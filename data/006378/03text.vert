<s>
Michail	Michail	k1gMnSc1	Michail
Jurjevič	Jurjevič	k1gMnSc1	Jurjevič
Lermontov	Lermontov	k1gInSc1	Lermontov
(	(	kIx(	(
<g/>
М	М	k?	М
Ю	Ю	k?	Ю
Л	Л	k?	Л
<g/>
;	;	kIx,	;
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1814	[number]	k4	1814
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1841	[number]	k4	1841
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ruský	ruský	k2eAgMnSc1d1	ruský
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
romantismu	romantismus	k1gInSc2	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
Lermontov	Lermontov	k1gInSc1	Lermontov
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
zemřela	zemřít	k5eAaPmAgFnS	zemřít
roku	rok	k1gInSc2	rok
1817	[number]	k4	1817
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
narození	narození	k1gNnSc6	narození
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Jurij	Jurij	k1gMnSc1	Jurij
Petrovič	Petrovič	k1gMnSc1	Petrovič
Lermontov	Lermontov	k1gInSc4	Lermontov
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
chudý	chudý	k2eAgMnSc1d1	chudý
vojenský	vojenský	k2eAgMnSc1d1	vojenský
důstojník	důstojník	k1gMnSc1	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
předal	předat	k5eAaPmAgMnS	předat
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
do	do	k7c2	do
výchovy	výchova	k1gFnSc2	výchova
jeho	jeho	k3xOp3gFnSc3	jeho
bohaté	bohatý	k2eAgFnSc3d1	bohatá
babičce	babička	k1gFnSc3	babička
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
domově	domov	k1gInSc6	domov
byl	být	k5eAaImAgMnS	být
Michail	Michail	k1gMnSc1	Michail
ohniskem	ohnisko	k1gNnSc7	ohnisko
rodinných	rodinný	k2eAgFnPc2d1	rodinná
hádek	hádka	k1gFnPc2	hádka
mezi	mezi	k7c7	mezi
svou	svůj	k3xOyFgFnSc7	svůj
babičkou	babička	k1gFnSc7	babička
a	a	k8xC	a
otcem	otec	k1gMnSc7	otec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
nesměl	smět	k5eNaImAgMnS	smět
na	na	k7c6	na
výchově	výchova	k1gFnSc6	výchova
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
podílet	podílet	k5eAaImF	podílet
<g/>
.	.	kIx.	.
</s>
<s>
Lermontovovi	Lermontova	k1gMnSc3	Lermontova
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
rozsáhlého	rozsáhlý	k2eAgNnSc2d1	rozsáhlé
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtrnácti	čtrnáct	k4xCc6	čtrnáct
letech	léto	k1gNnPc6	léto
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
moskevské	moskevský	k2eAgFnSc2d1	Moskevská
internátní	internátní	k2eAgFnSc2d1	internátní
školy	škola	k1gFnSc2	škola
pro	pro	k7c4	pro
chlapce	chlapec	k1gMnPc4	chlapec
z	z	k7c2	z
aristokratických	aristokratický	k2eAgFnPc2d1	aristokratická
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
šel	jít	k5eAaImAgMnS	jít
studovat	studovat	k5eAaImF	studovat
na	na	k7c4	na
Moskevskou	moskevský	k2eAgFnSc4d1	Moskevská
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgInS	věnovat
etice	etika	k1gFnSc3	etika
<g/>
,	,	kIx,	,
politologii	politologie	k1gFnSc6	politologie
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
také	také	k9	také
literatuře	literatura	k1gFnSc3	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kázeňských	kázeňský	k2eAgInPc2d1	kázeňský
důvodů	důvod	k1gInPc2	důvod
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1832	[number]	k4	1832
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Nato	nato	k6eAd1	nato
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
do	do	k7c2	do
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1834	[number]	k4	1834
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
školu	škola	k1gFnSc4	škola
s	s	k7c7	s
nejnižší	nízký	k2eAgFnSc7d3	nejnižší
důstojnickou	důstojnický	k2eAgFnSc7d1	důstojnická
hodností	hodnost	k1gFnSc7	hodnost
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
povolán	povolat	k5eAaPmNgMnS	povolat
ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
městě	město	k1gNnSc6	město
k	k	k7c3	k
regimentu	regiment	k1gInSc3	regiment
husarů	husar	k1gMnPc2	husar
imperiální	imperiální	k2eAgFnSc2d1	imperiální
stráže	stráž	k1gFnSc2	stráž
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
pozice	pozice	k1gFnSc2	pozice
u	u	k7c2	u
husarů	husar	k1gMnPc2	husar
Lermontov	Lermontov	k1gInSc4	Lermontov
pozoroval	pozorovat	k5eAaImAgInS	pozorovat
společenský	společenský	k2eAgInSc1d1	společenský
život	život	k1gInSc1	život
boháčů	boháč	k1gMnPc2	boháč
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zanícen	zanítit	k5eAaPmNgInS	zanítit
pro	pro	k7c4	pro
psaní	psaní	k1gNnSc4	psaní
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1832	[number]	k4	1832
měl	mít	k5eAaImAgMnS	mít
již	již	k6eAd1	již
napsáno	napsat	k5eAaPmNgNnS	napsat
200	[number]	k4	200
lyrických	lyrický	k2eAgFnPc2d1	lyrická
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
10	[number]	k4	10
delších	dlouhý	k2eAgFnPc2d2	delší
básní	báseň	k1gFnPc2	báseň
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Maškaráda	maškaráda	k1gFnSc1	maškaráda
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
Lermontovo	Lermontův	k2eAgNnSc4d1	Lermontův
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
drama	drama	k1gNnSc4	drama
<g/>
,	,	kIx,	,
točí	točit	k5eAaImIp3nS	točit
se	se	k3xPyFc4	se
okolo	okolo	k7c2	okolo
ztraceného	ztracený	k2eAgInSc2d1	ztracený
náramku	náramek	k1gInSc2	náramek
<g/>
,	,	kIx,	,
pochybných	pochybný	k2eAgFnPc2d1	pochybná
osobností	osobnost	k1gFnPc2	osobnost
a	a	k8xC	a
závisti	závist	k1gFnSc2	závist
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
je	být	k5eAaImIp3nS	být
věrná	věrný	k2eAgFnSc1d1	věrná
žena	žena	k1gFnSc1	žena
otrávena	otráven	k2eAgFnSc1d1	otrávena
zmrzlinou	zmrzlina	k1gFnSc7	zmrzlina
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc4	svůj
muže	muž	k1gMnSc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
Cenzura	cenzura	k1gFnSc1	cenzura
povolila	povolit	k5eAaPmAgFnS	povolit
první	první	k4xOgNnSc1	první
uvedení	uvedení	k1gNnSc1	uvedení
hry	hra	k1gFnSc2	hra
až	až	k8xS	až
roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1837	[number]	k4	1837
Lermontov	Lermontov	k1gInSc1	Lermontov
získal	získat	k5eAaPmAgInS	získat
širšího	široký	k2eAgNnSc2d2	širší
uznání	uznání	k1gNnSc2	uznání
jako	jako	k8xC	jako
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Alexandra	Alexandr	k1gMnSc2	Alexandr
Puškina	Puškino	k1gNnSc2	Puškino
publikoval	publikovat	k5eAaBmAgMnS	publikovat
elegii	elegie	k1gFnSc4	elegie
Smrt	smrt	k1gFnSc4	smrt
básníka	básník	k1gMnSc2	básník
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
báseň	báseň	k1gFnSc1	báseň
byla	být	k5eAaImAgFnS	být
nadšeně	nadšeně	k6eAd1	nadšeně
přijata	přijat	k2eAgFnSc1d1	přijata
v	v	k7c6	v
liberálních	liberální	k2eAgInPc6d1	liberální
kruzích	kruh	k1gInPc6	kruh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vadila	vadit	k5eAaImAgFnS	vadit
carovi	car	k1gMnSc3	car
Mikuláši	Mikuláš	k1gMnSc3	Mikuláš
I.	I.	kA	I.
Lermontov	Lermontov	k1gInSc1	Lermontov
byl	být	k5eAaImAgMnS	být
zadržen	zadržet	k5eAaPmNgMnS	zadržet
a	a	k8xC	a
musel	muset	k5eAaImAgMnS	muset
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
na	na	k7c4	na
Kavkaz	Kavkaz	k1gInSc4	Kavkaz
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
důstojník	důstojník	k1gMnSc1	důstojník
u	u	k7c2	u
dragounů	dragoun	k1gInPc2	dragoun
a	a	k8xC	a
musel	muset	k5eAaImAgMnS	muset
bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
horalům	horal	k1gMnPc3	horal
<g/>
.	.	kIx.	.
</s>
<s>
Díky	dík	k1gInPc1	dík
vlivu	vliv	k1gInSc2	vliv
jeho	jeho	k3xOp3gFnSc2	jeho
babičky	babička	k1gFnSc2	babička
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
byl	být	k5eAaImAgInS	být
umožněn	umožněn	k2eAgInSc1d1	umožněn
návrat	návrat	k1gInSc1	návrat
do	do	k7c2	do
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
postoj	postoj	k1gInSc1	postoj
k	k	k7c3	k
tehdejším	tehdejší	k2eAgFnPc3d1	tehdejší
státním	státní	k2eAgFnPc3d1	státní
záležitostem	záležitost	k1gFnPc3	záležitost
se	se	k3xPyFc4	se
však	však	k9	však
nijak	nijak	k6eAd1	nijak
nezměnil	změnit	k5eNaPmAgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1838	[number]	k4	1838
<g/>
–	–	k?	–
<g/>
1840	[number]	k4	1840
napsal	napsat	k5eAaBmAgInS	napsat
řadu	řada	k1gFnSc4	řada
příběhů	příběh	k1gInPc2	příběh
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
shromážděných	shromážděný	k2eAgMnPc2d1	shromážděný
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Hrdina	Hrdina	k1gMnSc1	Hrdina
naší	náš	k3xOp1gFnSc2	náš
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
klasické	klasický	k2eAgFnSc3d1	klasická
ruské	ruský	k2eAgFnSc3d1	ruská
literatuře	literatura	k1gFnSc3	literatura
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
charakterizováno	charakterizovat	k5eAaBmNgNnS	charakterizovat
jako	jako	k8xS	jako
novela	novela	k1gFnSc1	novela
psychologického	psychologický	k2eAgInSc2d1	psychologický
realismu	realismus	k1gInSc2	realismus
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
pěti	pět	k4xCc2	pět
oddělených	oddělený	k2eAgInPc2d1	oddělený
příběhů	příběh	k1gInPc2	příběh
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
spojovacím	spojovací	k2eAgInSc7d1	spojovací
prvkem	prvek	k1gInSc7	prvek
je	být	k5eAaImIp3nS	být
společný	společný	k2eAgMnSc1d1	společný
hrdina	hrdina	k1gMnSc1	hrdina
Grigorij	Grigorij	k1gFnSc2	Grigorij
Pečorin	Pečorin	k1gInSc1	Pečorin
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
mladý	mladý	k2eAgMnSc1d1	mladý
<g/>
,	,	kIx,	,
inteligentní	inteligentní	k2eAgMnSc1d1	inteligentní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
cítí	cítit	k5eAaImIp3nS	cítit
se	se	k3xPyFc4	se
zbytečný	zbytečný	k2eAgInSc4d1	zbytečný
(	(	kIx(	(
<g/>
literární	literární	k2eAgInSc4d1	literární
typ	typ	k1gInSc4	typ
tzv.	tzv.	kA	tzv.
zbytečného	zbytečný	k2eAgMnSc4d1	zbytečný
člověka	člověk	k1gMnSc4	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
tři	tři	k4xCgFnPc4	tři
<g/>
,	,	kIx,	,
chronologicky	chronologicky	k6eAd1	chronologicky
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
nenavazující	navazující	k2eNgFnPc1d1	nenavazující
vypravěčské	vypravěčský	k2eAgFnPc1d1	vypravěčská
linie	linie	k1gFnPc1	linie
<g/>
.	.	kIx.	.
</s>
<s>
Lermontovova	Lermontovův	k2eAgFnSc1d1	Lermontovova
největší	veliký	k2eAgFnSc1d3	veliký
báseň	báseň	k1gFnSc1	báseň
Démon	démon	k1gMnSc1	démon
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
andělovi	anděl	k1gMnSc6	anděl
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
do	do	k7c2	do
smrtelnice	smrtelnice	k1gFnSc2	smrtelnice
Tamary	Tamara	k1gFnSc2	Tamara
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
básníkovo	básníkův	k2eAgNnSc4d1	básníkovo
sebeposouzení	sebeposouzení	k1gNnSc4	sebeposouzení
jako	jako	k8xS	jako
démonického	démonický	k2eAgNnSc2d1	démonické
stvoření	stvoření	k1gNnSc2	stvoření
<g/>
.	.	kIx.	.
</s>
<s>
Melancholický	melancholický	k2eAgMnSc1d1	melancholický
Démon	démon	k1gMnSc1	démon
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
vyvržen	vyvrhnout	k5eAaPmNgMnS	vyvrhnout
z	z	k7c2	z
Ráje	rája	k1gFnSc2	rája
<g/>
,	,	kIx,	,
putuje	putovat	k5eAaImIp3nS	putovat
po	po	k7c6	po
Zemi	zem	k1gFnSc6	zem
v	v	k7c6	v
naději	naděje	k1gFnSc6	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
opět	opět	k6eAd1	opět
nalezne	naleznout	k5eAaPmIp3nS	naleznout
klid	klid	k1gInSc1	klid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
navštíví	navštívit	k5eAaPmIp3nS	navštívit
Tamaru	Tamara	k1gFnSc4	Tamara
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInSc1	jeho
polibek	polibek	k1gInSc1	polibek
ji	on	k3xPp3gFnSc4	on
zabije	zabít	k5eAaPmIp3nS	zabít
jako	jako	k9	jako
smrtící	smrtící	k2eAgInSc1d1	smrtící
jed	jed	k1gInSc1	jed
<g/>
.	.	kIx.	.
</s>
<s>
Démon	démon	k1gMnSc1	démon
proklíná	proklínat	k5eAaImIp3nS	proklínat
své	svůj	k3xOyFgInPc4	svůj
sny	sen	k1gInPc4	sen
o	o	k7c6	o
lepším	dobrý	k2eAgInSc6d2	lepší
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Lermontov	Lermontov	k1gInSc1	Lermontov
napsal	napsat	k5eAaBmAgInS	napsat
koncept	koncept	k1gInSc4	koncept
této	tento	k3xDgFnSc2	tento
melancholické	melancholický	k2eAgFnSc2d1	melancholická
a	a	k8xC	a
sebekritizující	sebekritizující	k2eAgFnSc2d1	sebekritizující
básně	báseň	k1gFnSc2	báseň
už	už	k9	už
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
14	[number]	k4	14
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Popularitu	popularita	k1gFnSc4	popularita
získal	získat	k5eAaPmAgMnS	získat
hlavně	hlavně	k9	hlavně
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gFnSc1	jeho
báseň	báseň	k1gFnSc1	báseň
Předpověď	předpověď	k1gFnSc1	předpověď
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
říká	říkat	k5eAaImIp3nS	říkat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Rok	rok	k1gInSc1	rok
přijde	přijít	k5eAaPmIp3nS	přijít
zlý	zlý	k2eAgMnSc1d1	zlý
<g/>
,	,	kIx,	,
ó	ó	k0	ó
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
v	v	k7c6	v
příští	příští	k2eAgFnSc6d1	příští
tvém	tvůj	k3xOp2gNnSc6	tvůj
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
s	s	k7c7	s
hlavy	hlava	k1gFnPc1	hlava
carů	car	k1gMnPc2	car
sletí	sletět	k5eAaPmIp3nP	sletět
diadém	diadém	k1gInSc4	diadém
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
luzy	luza	k1gFnSc2	luza
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
se	se	k3xPyFc4	se
změní	změnit	k5eAaPmIp3nS	změnit
v	v	k7c4	v
hněv	hněv	k1gInSc4	hněv
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
stravou	strava	k1gFnSc7	strava
mnohých	mnohé	k1gNnPc2	mnohé
bude	být	k5eAaImBp3nS	být
smrt	smrt	k1gFnSc1	smrt
a	a	k8xC	a
krev	krev	k1gFnSc1	krev
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Kvůli	kvůli	k7c3	kvůli
duelu	duel	k1gInSc3	duel
se	s	k7c7	s
synem	syn	k1gMnSc7	syn
francouzského	francouzský	k2eAgMnSc2d1	francouzský
vyslance	vyslanec	k1gMnSc2	vyslanec
musel	muset	k5eAaImAgInS	muset
Lermontov	Lermontov	k1gInSc4	Lermontov
opět	opět	k6eAd1	opět
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
k	k	k7c3	k
Černému	černý	k2eAgNnSc3d1	černé
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
<s>
Regiment	regiment	k1gInSc1	regiment
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
stálé	stálý	k2eAgFnSc6d1	stálá
aktivní	aktivní	k2eAgFnSc6d1	aktivní
službě	služba	k1gFnSc6	služba
<g/>
.	.	kIx.	.
</s>
<s>
Lermontov	Lermontov	k1gInSc1	Lermontov
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
odvahu	odvaha	k1gFnSc4	odvaha
obdivován	obdivován	k2eAgInSc4d1	obdivován
ostatními	ostatní	k2eAgMnPc7d1	ostatní
důstojníky	důstojník	k1gMnPc7	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Služba	služba	k1gFnSc1	služba
na	na	k7c6	na
frontě	fronta	k1gFnSc6	fronta
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
bránila	bránit	k5eAaImAgFnS	bránit
psát	psát	k5eAaImF	psát
a	a	k8xC	a
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
předstíraje	předstírat	k5eAaImSgMnS	předstírat
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
,	,	kIx,	,
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
lázní	lázeň	k1gFnPc2	lázeň
Pjatigorsk	Pjatigorsk	k1gInSc1	Pjatigorsk
<g/>
.	.	kIx.	.
</s>
<s>
Pohádal	pohádat	k5eAaPmAgMnS	pohádat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
s	s	k7c7	s
majorem	major	k1gMnSc7	major
N.	N.	kA	N.
S.	S.	kA	S.
Martynovem	Martynovo	k1gNnSc7	Martynovo
<g/>
,	,	kIx,	,
starým	starý	k2eAgMnSc7d1	starý
rodinným	rodinný	k2eAgMnSc7d1	rodinný
přítelem	přítel	k1gMnSc7	přítel
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
roku	rok	k1gInSc2	rok
1841	[number]	k4	1841
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
26	[number]	k4	26
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
našel	najít	k5eAaPmAgMnS	najít
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Korzár	korzár	k1gMnSc1	korzár
(	(	kIx(	(
<g/>
1828	[number]	k4	1828
<g/>
,	,	kIx,	,
К	К	k?	К
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poema	poema	k1gFnSc1	poema
<g/>
,	,	kIx,	,
Oleg	Oleg	k1gMnSc1	Oleg
(	(	kIx(	(
<g/>
1829	[number]	k4	1829
<g/>
,	,	kIx,	,
О	О	k?	О
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poema	poema	k1gNnSc1	poema
<g/>
,	,	kIx,	,
Poslední	poslední	k2eAgMnSc1d1	poslední
syn	syn	k1gMnSc1	syn
volnosti	volnost	k1gFnSc2	volnost
(	(	kIx(	(
<g/>
1830	[number]	k4	1830
<g/>
,	,	kIx,	,
П	П	k?	П
с	с	k?	с
в	в	k?	в
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poema	poema	k1gFnSc1	poema
<g/>
,	,	kIx,	,
Předpověď	předpověď	k1gFnSc1	předpověď
(	(	kIx(	(
<g/>
1830	[number]	k4	1830
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
П	П	k?	П
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
báseň	báseň	k1gFnSc1	báseň
<g/>
,	,	kIx,	,
Anděl	Anděl	k1gMnSc1	Anděl
(	(	kIx(	(
<g/>
1831	[number]	k4	1831
<g/>
,	,	kIx,	,
А	А	k?	А
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
báseň	báseň	k1gFnSc1	báseň
<g/>
,	,	kIx,	,
Izmail-Bej	Izmail-Bej	k1gInSc1	Izmail-Bej
(	(	kIx(	(
<g/>
1832	[number]	k4	1832
<g/>
,	,	kIx,	,
И	И	k?	И
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poema	poema	k1gFnSc1	poema
<g/>
,	,	kIx,	,
Plachta	plachta	k1gFnSc1	plachta
(	(	kIx(	(
<g/>
1832	[number]	k4	1832
<g/>
,	,	kIx,	,
П	П	k?	П
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
báseň	báseň	k1gFnSc1	báseň
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
uváděna	uvádět	k5eAaImNgFnS	uvádět
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Na	na	k7c6	na
obzoru	obzor	k1gInSc6	obzor
plachta	plachta	k1gFnSc1	plachta
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vadim	Vadim	k?	Vadim
(	(	kIx(	(
<g/>
1832	[number]	k4	1832
<g/>
–	–	k?	–
<g/>
1834	[number]	k4	1834
<g/>
,	,	kIx,	,
В	В	k?	В
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nedokončený	dokončený	k2eNgInSc1d1	nedokončený
román	román	k1gInSc4	román
odehrávající	odehrávající	k2eAgInSc4d1	odehrávající
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
povstání	povstání	k1gNnSc2	povstání
Jemeljana	Jemeljan	k1gMnSc4	Jemeljan
Pugačova	Pugačův	k2eAgMnSc4d1	Pugačův
<g/>
,	,	kIx,	,
Hadži	hadži	k1gMnSc1	hadži
Abrek	Abrek	k1gMnSc1	Abrek
(	(	kIx(	(
<g/>
1835	[number]	k4	1835
<g/>
,	,	kIx,	,
Х	Х	k?	Х
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
poema	poema	k1gFnSc1	poema
<g/>
,	,	kIx,	,
Bojar	bojar	k1gMnSc1	bojar
Orša	Orša	k1gMnSc1	Orša
(	(	kIx(	(
<g/>
1835	[number]	k4	1835
<g/>
–	–	k?	–
<g/>
1836	[number]	k4	1836
<g/>
,	,	kIx,	,
Б	Б	k?	Б
О	О	k?	О
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poema	poema	k1gFnSc1	poema
zobrazující	zobrazující	k2eAgFnSc2d1	zobrazující
konflikt	konflikt	k1gInSc4	konflikt
mezi	mezi	k7c7	mezi
nevolníkem	nevolník	k1gMnSc7	nevolník
a	a	k8xC	a
bojarem	bojar	k1gMnSc7	bojar
v	v	k7c6	v
době	doba	k1gFnSc6	doba
cara	car	k1gMnSc2	car
Ivana	Ivan	k1gMnSc2	Ivan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Maškaráda	maškaráda	k1gFnSc1	maškaráda
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
,	,	kIx,	,
Mа	Mа	k1gFnSc1	Mа
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
veršovaná	veršovaný	k2eAgFnSc1d1	veršovaná
<g />
.	.	kIx.	.
</s>
<s>
tragédie	tragédie	k1gFnPc1	tragédie
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
žárlivosti	žárlivost	k1gFnSc2	žárlivost
a	a	k8xC	a
msty	msta	k1gFnSc2	msta
<g/>
,	,	kIx,	,
Dva	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
,	,	kIx,	,
Д	Д	k?	Д
б	б	k?	б
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgInPc4	který
o	o	k7c4	o
mladou	mladý	k2eAgFnSc4d1	mladá
ženu	žena	k1gFnSc4	žena
soupeří	soupeřit	k5eAaImIp3nP	soupeřit
dva	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
jako	jako	k8xC	jako
naivní	naivní	k2eAgMnSc1d1	naivní
zamilovaný	zamilovaný	k1gMnSc1	zamilovaný
manžel	manžel	k1gMnSc1	manžel
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
dramatu	drama	k1gNnSc2	drama
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
démonský	démonský	k2eAgMnSc1d1	démonský
milenec	milenec	k1gMnSc1	milenec
<g/>
.	.	kIx.	.
</s>
<s>
Kněžna	kněžna	k1gFnSc1	kněžna
Ligovská	Ligovská	k1gFnSc1	Ligovská
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
,	,	kIx,	,
К	К	k?	К
Л	Л	k?	Л
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nedokončený	dokončený	k2eNgInSc4d1	nedokončený
román	román	k1gInSc4	román
řešící	řešící	k2eAgNnSc1d1	řešící
téma	téma	k1gNnSc1	téma
sociální	sociální	k2eAgFnSc2d1	sociální
nerovnosti	nerovnost	k1gFnSc2	nerovnost
<g/>
,	,	kIx,	,
Píseň	píseň	k1gFnSc1	píseň
o	o	k7c6	o
caru	car	k1gMnSc6	car
Ivanu	Ivan	k1gMnSc6	Ivan
Vasiljeviči	Vasiljevič	k1gMnSc6	Vasiljevič
<g/>
,	,	kIx,	,
mladém	mladý	k1gMnSc6	mladý
gardistovi	gardista	k1gMnSc6	gardista
a	a	k8xC	a
smělém	smělý	k2eAgMnSc6d1	smělý
kupci	kupec	k1gMnSc6	kupec
Kalašnikovu	kalašnikov	k1gInSc6	kalašnikov
(	(	kIx(	(
<g/>
1837	[number]	k4	1837
<g/>
,	,	kIx,	,
П	П	k?	П
п	п	k?	п
ц	ц	k?	ц
И	И	k?	И
В	В	k?	В
<g/>
,	,	kIx,	,
м	м	k?	м
о	о	k?	о
и	и	k?	и
у	у	k?	у
<g />
.	.	kIx.	.
</s>
<s>
к	к	k?	к
К	К	k?	К
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poema	poema	k1gFnSc1	poema
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
cara	car	k1gMnSc2	car
Ivana	Ivan	k1gMnSc2	Ivan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
napsaná	napsaný	k2eAgFnSc1d1	napsaná
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
lidových	lidový	k2eAgFnPc2d1	lidová
historických	historický	k2eAgFnPc2d1	historická
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
básník	básník	k1gMnSc1	básník
dává	dávat	k5eAaImIp3nS	dávat
za	za	k7c4	za
pravdu	pravda	k1gFnSc4	pravda
hrdinovi	hrdinův	k2eAgMnPc1d1	hrdinův
z	z	k7c2	z
lidu	lid	k1gInSc2	lid
proti	proti	k7c3	proti
carovi	car	k1gMnSc3	car
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc3	jeho
milci	milec	k1gMnSc3	milec
(	(	kIx(	(
<g/>
kupec	kupec	k1gMnSc1	kupec
Kalašnikov	kalašnikov	k1gInSc4	kalašnikov
<g/>
,	,	kIx,	,
hájící	hájící	k2eAgFnSc4d1	hájící
čest	čest	k1gFnSc4	čest
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
zabije	zabít	k5eAaPmIp3nS	zabít
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
mladého	mladý	k1gMnSc4	mladý
gardistu	gardista	k1gMnSc4	gardista
a	a	k8xC	a
hyne	hynout	k5eAaImIp3nS	hynout
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
na	na	k7c6	na
popravišti	popraviště	k1gNnSc6	popraviště
<g/>
,	,	kIx,	,
potrestán	potrestán	k2eAgInSc1d1	potrestán
carem	car	k1gMnSc7	car
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
básníka	básník	k1gMnSc2	básník
(	(	kIx(	(
<g/>
1837	[number]	k4	1837
<g/>
,	,	kIx,	,
С	С	k?	С
п	п	k?	п
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
elegie	elegie	k1gFnSc1	elegie
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
památce	památka	k1gFnSc3	památka
Alexanda	Alexando	k1gNnSc2	Alexando
Sergejeviče	Sergejevič	k1gMnSc2	Sergejevič
Puškina	Puškin	k1gMnSc2	Puškin
tvrdě	tvrdě	k6eAd1	tvrdě
odsuzující	odsuzující	k2eAgFnSc1d1	odsuzující
všechny	všechen	k3xTgMnPc4	všechen
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
básníka	básník	k1gMnSc4	básník
dotlačili	dotlačit	k5eAaPmAgMnP	dotlačit
k	k	k7c3	k
souboji	souboj	k1gInSc3	souboj
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
byl	být	k5eAaImAgInS	být
zabit	zabít	k5eAaPmNgInS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Borodino	Borodin	k2eAgNnSc1d1	Borodino
(	(	kIx(	(
<g/>
1837	[number]	k4	1837
<g/>
,	,	kIx,	,
Б	Б	k?	Б
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
báseň	báseň	k1gFnSc1	báseň
o	o	k7c4	o
hrdinství	hrdinství	k1gNnSc4	hrdinství
Rusů	Rus	k1gMnPc2	Rus
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Borodina	Borodin	k2eAgInSc2d1	Borodin
roku	rok	k1gInSc2	rok
1812	[number]	k4	1812
<g/>
,	,	kIx,	,
Hrdina	Hrdina	k1gMnSc1	Hrdina
naší	náš	k3xOp1gFnSc2	náš
doby	doba	k1gFnSc2	doba
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
–	–	k?	–
<g/>
1840	[number]	k4	1840
<g/>
,	,	kIx,	,
Г	Г	k?	Г
н	н	k?	н
в	в	k?	в
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
s	s	k7c7	s
hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
Grigorijem	Grigorij	k1gMnSc7	Grigorij
Alexandrovičem	Alexandrovič	k1gMnSc7	Alexandrovič
Pečorinem	Pečorin	k1gInSc7	Pečorin
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
cynickým	cynický	k2eAgMnSc7d1	cynický
bohémem	bohém	k1gMnSc7	bohém
bez	bez	k7c2	bez
jasného	jasný	k2eAgInSc2d1	jasný
životního	životní	k2eAgInSc2d1	životní
cíle	cíl	k1gInSc2	cíl
(	(	kIx(	(
<g/>
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
další	další	k2eAgInSc4d1	další
typ	typ	k1gInSc4	typ
tzv.	tzv.	kA	tzv.
zbytečného	zbytečný	k2eAgMnSc4d1	zbytečný
člověka	člověk	k1gMnSc4	člověk
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
literatuře	literatura	k1gFnSc6	literatura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skládající	skládající	k2eAgInPc4d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
pěti	pět	k4xCc2	pět
relativně	relativně	k6eAd1	relativně
nezávislých	závislý	k2eNgFnPc2d1	nezávislá
novel	novela	k1gFnPc2	novela
<g/>
:	:	kIx,	:
Bela	Bela	k1gFnSc1	Bela
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
,	,	kIx,	,
Б	Б	k?	Б
<g/>
)	)	kIx)	)
Maxim	Maxim	k1gMnSc1	Maxim
Maximyč	Maximyč	k1gMnSc1	Maximyč
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
,	,	kIx,	,
М	М	k?	М
М	М	k?	М
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tamaň	Tamaň	k1gMnSc1	Tamaň
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
,	,	kIx,	,
Т	Т	k?	Т
<g/>
)	)	kIx)	)
Komtesa	komtesa	k1gFnSc1	komtesa
Mary	Mary	k1gFnSc1	Mary
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
,	,	kIx,	,
К	К	k?	К
М	М	k?	М
<g/>
)	)	kIx)	)
Fatalista	fatalista	k1gMnSc1	fatalista
(	(	kIx(	(
<g/>
1839	[number]	k4	1839
<g/>
,	,	kIx,	,
Ф	Ф	k?	Ф
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Duma	duma	k1gFnSc1	duma
(	(	kIx(	(
<g/>
1839	[number]	k4	1839
<g/>
,	,	kIx,	,
Д	Д	k?	Д
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
báseň	báseň	k1gFnSc1	báseň
<g/>
,	,	kIx,	,
Paní	paní	k1gFnSc1	paní
důchodní	důchodní	k1gFnSc1	důchodní
z	z	k7c2	z
Tambova	Tambův	k2eAgInSc2d1	Tambův
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
,	,	kIx,	,
Т	Т	k?	Т
к	к	k?	к
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poema	poema	k1gNnSc4	poema
popisující	popisující	k2eAgInSc1d1	popisující
nudný	nudný	k2eAgInSc1d1	nudný
život	život	k1gInSc1	život
v	v	k7c6	v
ruském	ruský	k2eAgNnSc6d1	ruské
maloměstě	maloměsto	k1gNnSc6	maloměsto
<g/>
,	,	kIx,	,
Kozácká	kozácký	k2eAgFnSc1d1	kozácká
ukolébavka	ukolébavka	k1gFnSc1	ukolébavka
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
,	,	kIx,	,
К	К	k?	К
к	к	k?	к
п	п	k?	п
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
báseň	báseň	k1gFnSc1	báseň
<g/>
,	,	kIx,	,
Novic	novic	k1gMnSc1	novic
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
,	,	kIx,	,
М	М	k?	М
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poema	poema	k1gNnSc1	poema
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
,	,	kIx,	,
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
zavřený	zavřený	k2eAgInSc4d1	zavřený
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
<g/>
,	,	kIx,	,
touží	toužit	k5eAaImIp3nP	toužit
po	po	k7c6	po
svobodném	svobodný	k2eAgInSc6d1	svobodný
životě	život	k1gInSc6	život
v	v	k7c6	v
rodných	rodný	k2eAgFnPc6d1	rodná
kavkazských	kavkazský	k2eAgFnPc6d1	kavkazská
horách	hora	k1gFnPc6	hora
a	a	k8xC	a
protestuje	protestovat	k5eAaBmIp3nS	protestovat
proti	proti	k7c3	proti
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ho	on	k3xPp3gMnSc4	on
uvěznili	uvěznit	k5eAaPmAgMnP	uvěznit
<g/>
,	,	kIx,	,
Sen	sen	k1gInSc4	sen
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
,	,	kIx,	,
С	С	k?	С
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
báseň	báseň	k1gFnSc1	báseň
<g/>
,	,	kIx,	,
Sám	sám	k3xTgMnSc1	sám
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
si	se	k3xPyFc3	se
vyjdu	vyjít	k5eAaPmIp1nS	vyjít
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
,	,	kIx,	,
В	В	k?	В
о	о	k?	о
я	я	k?	я
н	н	k?	н
д	д	k?	д
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
báseň	báseň	k1gFnSc1	báseň
<g/>
,	,	kIx,	,
Démon	démon	k1gMnSc1	démon
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
,	,	kIx,	,
Д	Д	k?	Д
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poema	poema	k1gFnSc1	poema
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
první	první	k4xOgFnSc4	první
verzi	verze	k1gFnSc4	verze
napsal	napsat	k5eAaBmAgMnS	napsat
Lermontov	Lermontov	k1gInSc4	Lermontov
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1829	[number]	k4	1829
<g/>
,	,	kIx,	,
považovaná	považovaný	k2eAgFnSc1d1	považovaná
za	za	k7c4	za
druhý	druhý	k4xOgInSc1	druhý
největším	veliký	k2eAgMnSc7d3	veliký
počin	počin	k1gInSc1	počin
ruského	ruský	k2eAgInSc2d1	ruský
romantismu	romantismus	k1gInSc2	romantismus
hned	hned	k6eAd1	hned
po	po	k7c6	po
Puškinově	Puškinův	k2eAgMnSc6d1	Puškinův
Evženu	Evžen	k1gMnSc6	Evžen
Oněginovi	Oněgin	k1gMnSc6	Oněgin
<g/>
.	.	kIx.	.
</s>
<s>
Démon	démon	k1gMnSc1	démon
je	být	k5eAaImIp3nS	být
padlý	padlý	k1gMnSc1	padlý
anděl	anděl	k1gMnSc1	anděl
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
musí	muset	k5eAaImIp3nS	muset
pykat	pykat	k5eAaImF	pykat
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
vzpouru	vzpoura	k1gFnSc4	vzpoura
proti	proti	k7c3	proti
Bohu	bůh	k1gMnSc3	bůh
<g/>
,	,	kIx,	,
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
opuštěnosti	opuštěnost	k1gFnSc6	opuštěnost
se	se	k3xPyFc4	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
do	do	k7c2	do
smrtelné	smrtelný	k2eAgFnSc2d1	smrtelná
gruzínské	gruzínský	k2eAgFnSc2d1	gruzínská
princezny	princezna	k1gFnSc2	princezna
Tamary	Tamara	k1gFnSc2	Tamara
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
však	však	k9	však
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
polibku	polibek	k1gInSc6	polibek
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Básně	báseň	k1gFnPc1	báseň
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
Grégr	Grégr	k1gMnSc1	Grégr
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1872	[number]	k4	1872
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
A.	A.	kA	A.
Durdík	Durdík	k1gMnSc1	Durdík
<g/>
,	,	kIx,	,
Básně	báseň	k1gFnPc1	báseň
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
Grégr	Grégr	k1gMnSc1	Grégr
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1874	[number]	k4	1874
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
A.	A.	kA	A.
Durdík	Durdík	k1gMnSc1	Durdík
<g/>
,	,	kIx,	,
Hrdina	Hrdina	k1gMnSc1	Hrdina
naší	náš	k3xOp1gFnSc2	náš
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1879	[number]	k4	1879
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jan	Jan	k1gMnSc1	Jan
Žebro	žebro	k1gNnSc4	žebro
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
Básně	báseň	k1gFnPc1	báseň
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1828	[number]	k4	1828
<g/>
–	–	k?	–
<g/>
1832	[number]	k4	1832
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
František	František	k1gMnSc1	František
Táborský	Táborský	k1gMnSc1	Táborský
<g/>
,	,	kIx,	,
Bela	Bela	k1gFnSc1	Bela
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
V.	V.	kA	V.
Tobolka	tobolka	k1gFnSc1	tobolka
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
V.	V.	kA	V.
Tobolka	tobolka	k1gFnSc1	tobolka
<g/>
,	,	kIx,	,
Básně	báseň	k1gFnPc1	báseň
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1833	[number]	k4	1833
<g/>
–	–	k?	–
<g/>
1837	[number]	k4	1837
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
František	František	k1gMnSc1	František
Táborský	Táborský	k1gMnSc1	Táborský
<g/>
,	,	kIx,	,
Démon	démon	k1gMnSc1	démon
<g/>
,	,	kIx,	,
Svaz	svaz	k1gInSc1	svaz
českoslovanského	českoslovanský	k2eAgNnSc2d1	českoslovanské
studentstva	studentstvo	k1gNnSc2	studentstvo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
F.	F.	kA	F.
Hais	Hais	k1gInSc1	Hais
<g/>
,	,	kIx,	,
Hrdina	Hrdina	k1gMnSc1	Hrdina
naší	náš	k3xOp1gFnSc2	náš
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Čestmír	Čestmír	k1gMnSc1	Čestmír
Stehlík	Stehlík	k1gMnSc1	Stehlík
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
<s>
Fatalista	fatalista	k1gMnSc1	fatalista
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Vladimír	Vladimír	k1gMnSc1	Vladimír
Feršman	Feršman	k1gMnSc1	Feršman
<g/>
,	,	kIx,	,
Ašik-Kerib	Ašik-Kerib	k1gMnSc1	Ašik-Kerib
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Springer	Springer	k1gMnSc1	Springer
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jan	Jan	k1gMnSc1	Jan
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
Básně	báseň	k1gFnPc1	báseň
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
František	František	k1gMnSc1	František
Tropp	Tropp	k1gMnSc1	Tropp
<g/>
,	,	kIx,	,
Dvojí	dvojí	k4xRgMnSc1	dvojí
<g />
.	.	kIx.	.
</s>
<s>
dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
<g/>
,	,	kIx,	,
E.	E.	kA	E.
Šolc	Šolc	k1gMnSc1	Šolc
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Stanislav	Stanislav	k1gMnSc1	Stanislav
Minařík	Minařík	k1gMnSc1	Minařík
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Tamaň	Tamaň	k1gMnSc1	Tamaň
a	a	k8xC	a
Fatalista	fatalista	k1gMnSc1	fatalista
<g/>
,	,	kIx,	,
Básně	báseň	k1gFnPc1	báseň
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
–	–	k?	–
<g/>
1841	[number]	k4	1841
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
František	František	k1gMnSc1	František
Táborský	Táborský	k1gMnSc1	Táborský
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Vadím	vadit	k5eAaImIp1nS	vadit
<g/>
,	,	kIx,	,
Obrození	obrození	k1gNnSc1	obrození
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
V.	V.	kA	V.
Urban	Urban	k1gMnSc1	Urban
<g/>
,	,	kIx,	,
Maškarní	maškarní	k2eAgInSc1d1	maškarní
ples	ples	k1gInSc1	ples
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
František	František	k1gMnSc1	František
Táborský	Táborský	k1gMnSc1	Táborský
<g/>
,	,	kIx,	,
Ašik-Kerib	Ašik-Kerib	k1gMnSc1	Ašik-Kerib
<g/>
,	,	kIx,	,
Spolek	spolek	k1gInSc1	spolek
Radhošť	Radhošť	k1gFnSc1	Radhošť
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
František	František	k1gMnSc1	František
Táborský	Táborský	k1gMnSc1	Táborský
<g/>
,	,	kIx,	,
Démon	démon	k1gMnSc1	démon
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Josef	Josef	k1gMnSc1	Josef
Hora	Hora	k1gMnSc1	Hora
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1954	[number]	k4	1954
a	a	k8xC	a
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
Novic	novic	k1gMnSc1	novic
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Vladimír	Vladimír	k1gMnSc1	Vladimír
Holan	Holan	k1gMnSc1	Holan
<g/>
,	,	kIx,	,
Démonova	démonův	k2eAgFnSc1d1	démonova
přísaha	přísaha	k1gFnSc1	přísaha
Tamaře	Tamara	k1gFnSc6	Tamara
<g/>
,	,	kIx,	,
Otto	Otto	k1gMnSc1	Otto
Girgal	Girgal	k1gMnSc1	Girgal
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Josef	Josef	k1gMnSc1	Josef
Hora	Hora	k1gMnSc1	Hora
<g/>
,	,	kIx,	,
Píseň	píseň	k1gFnSc1	píseň
o	o	k7c6	o
caru	car	k1gMnSc6	car
Ivanu	Ivan	k1gMnSc6	Ivan
Vasiljeviči	Vasiljevič	k1gMnSc6	Vasiljevič
<g/>
,	,	kIx,	,
mladém	mladý	k1gMnSc6	mladý
gardistovi	gardista	k1gMnSc6	gardista
a	a	k8xC	a
smělém	smělý	k2eAgMnSc6d1	smělý
kupci	kupec	k1gMnSc6	kupec
Kalašnikovu	kalašnikov	k1gInSc3	kalašnikov
<g/>
,	,	kIx,	,
Evropský	evropský	k2eAgInSc1d1	evropský
literární	literární	k2eAgInSc1d1	literární
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Bohumil	Bohumil	k1gMnSc1	Bohumil
Mathesius	Mathesius	k1gMnSc1	Mathesius
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
<s>
Bratr	bratr	k1gMnSc1	bratr
smutek	smutek	k1gInSc1	smutek
(	(	kIx(	(
<g/>
vybrané	vybraný	k2eAgInPc4d1	vybraný
spisy	spis	k1gInPc4	spis
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Josef	Josef	k1gMnSc1	Josef
Hora	Hora	k1gMnSc1	Hora
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Marčanová	Marčanová	k1gFnSc1	Marčanová
a	a	k8xC	a
Bohumil	Bohumil	k1gMnSc1	Bohumil
Mathesius	Mathesius	k1gMnSc1	Mathesius
<g/>
,	,	kIx,	,
Dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
Grigorije	Grigorije	k1gMnSc2	Grigorije
Alexandroviče	Alexandrovič	k1gMnSc2	Alexandrovič
Pečorina	Pečorino	k1gNnSc2	Pečorino
(	(	kIx(	(
<g/>
vybrané	vybraný	k2eAgInPc4d1	vybraný
spisy	spis	k1gInPc4	spis
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Ivan	Ivan	k1gMnSc1	Ivan
Hálek	hálka	k1gFnPc2	hálka
a	a	k8xC	a
Bohuslav	Bohuslava	k1gFnPc2	Bohuslava
Ilek	Ileka	k1gFnPc2	Ileka
<g/>
,	,	kIx,	,
U	u	k7c2	u
hraběnky	hraběnka	k1gFnSc2	hraběnka
V.	V.	kA	V.
byl	být	k5eAaImAgInS	být
hudební	hudební	k2eAgInSc4d1	hudební
večírek	večírek	k1gInSc4	večírek
...	...	k?	...
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Anna	Anna	k1gFnSc1	Anna
Zahradníčková	Zahradníčková	k1gFnSc1	Zahradníčková
<g/>
,	,	kIx,	,
Kozácká	kozácký	k2eAgFnSc1d1	kozácká
ukolébavka	ukolébavka	k1gFnSc1	ukolébavka
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Pour	Pour	k1gMnSc1	Pour
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
<g />
.	.	kIx.	.
</s>
<s>
Bohumil	Bohumil	k1gMnSc1	Bohumil
Mathesius	Mathesius	k1gMnSc1	Mathesius
<g/>
,	,	kIx,	,
Borodino	Borodina	k1gFnSc5	Borodina
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Pour	Pour	k1gMnSc1	Pour
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Bohumil	Bohumil	k1gMnSc1	Bohumil
Mathesius	Mathesius	k1gMnSc1	Mathesius
<g/>
,	,	kIx,	,
Poemy	poema	k1gFnPc4	poema
(	(	kIx(	(
<g/>
vybrané	vybraný	k2eAgInPc4d1	vybraný
spisy	spis	k1gInPc4	spis
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Vladimír	Vladimír	k1gMnSc1	Vladimír
Holan	Holan	k1gMnSc1	Holan
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Hora	Hora	k1gMnSc1	Hora
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g />
.	.	kIx.	.
</s>
<s>
Marčanová	Marčanová	k1gFnSc1	Marčanová
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Mathesius	Mathesius	k1gMnSc1	Mathesius
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Víška	Víšek	k1gMnSc2	Víšek
<g/>
,	,	kIx,	,
Dopisy	dopis	k1gInPc1	dopis
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Šmidt	Šmidt	k1gInSc1	Šmidt
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Teichman	Teichman	k1gMnSc1	Teichman
<g/>
,	,	kIx,	,
Lyrika	lyrika	k1gFnSc1	lyrika
<g/>
,	,	kIx,	,
Svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Petr	Petr	k1gMnSc1	Petr
Křička	Křička	k1gMnSc1	Křička
<g/>
,	,	kIx,	,
Výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
I.	I.	kA	I.
–	–	k?	–
Lyrika	lyrik	k1gMnSc2	lyrik
a	a	k8xC	a
poemy	poema	k1gFnSc2	poema
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Emanuel	Emanuel	k1gMnSc1	Emanuel
Frynta	Frynta	k1gMnSc1	Frynta
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Holan	Holan	k1gMnSc1	Holan
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Hora	Hora	k1gMnSc1	Hora
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Křička	Křička	k1gMnSc1	Křička
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Marčanová	Marčanová	k1gFnSc1	Marčanová
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Mathesius	Mathesius	k1gMnSc1	Mathesius
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Teichman	Teichman	k1gMnSc1	Teichman
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Víška	Víšek	k1gMnSc2	Víšek
a	a	k8xC	a
Zdenka	Zdenka	k1gFnSc1	Zdenka
Vovsová	Vovsová	k1gFnSc1	Vovsová
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
básníkovu	básníkův	k2eAgFnSc4d1	básníkova
lyriku	lyrika	k1gFnSc4	lyrika
a	a	k8xC	a
poemy	poema	k1gFnSc2	poema
Píseň	píseň	k1gFnSc1	píseň
o	o	k7c6	o
caru	car	k1gMnSc6	car
Ivanu	Ivan	k1gMnSc6	Ivan
Vasiljeviči	Vasiljevič	k1gMnSc6	Vasiljevič
<g/>
,	,	kIx,	,
mladém	mladý	k1gMnSc6	mladý
gardistovi	gardista	k1gMnSc6	gardista
a	a	k8xC	a
smělém	smělý	k2eAgMnSc6d1	smělý
kupci	kupec	k1gMnSc6	kupec
Kalašnikovu	kalašnikov	k1gInSc3	kalašnikov
<g/>
,	,	kIx,	,
Bojar	bojar	k1gMnSc1	bojar
Orša	Orša	k1gMnSc1	Orša
<g/>
,	,	kIx,	,
Paní	paní	k1gFnSc1	paní
důchodní	důchodní	k1gFnSc1	důchodní
z	z	k7c2	z
Tambova	Tambův	k2eAgNnSc2d1	Tambův
<g/>
,	,	kIx,	,
Novic	novic	k1gMnSc1	novic
a	a	k8xC	a
Démon	démon	k1gMnSc1	démon
<g/>
.	.	kIx.	.
</s>
<s>
Výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
II	II	kA	II
<g/>
.	.	kIx.	.
–	–	k?	–
Kněžna	kněžna	k1gFnSc1	kněžna
Ligovská	Ligovská	k1gFnSc1	Ligovská
<g/>
,	,	kIx,	,
Hrdina	Hrdina	k1gMnSc1	Hrdina
naší	náš	k3xOp1gFnSc2	náš
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
Maškaráda	maškaráda	k1gFnSc1	maškaráda
<g/>
,	,	kIx,	,
Svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Emanuel	Emanuel	k1gMnSc1	Emanuel
Frynta	Frynta	k1gMnSc1	Frynta
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Hálek	hálka	k1gFnPc2	hálka
<g/>
,	,	kIx,	,
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Ilek	Ilek	k1gMnSc1	Ilek
a	a	k8xC	a
Zdenka	Zdenka	k1gFnSc1	Zdenka
Vovsová	Vovsová	k1gFnSc1	Vovsová
<g/>
,	,	kIx,	,
Maškaráda	maškaráda	k1gFnSc1	maškaráda
<g/>
,	,	kIx,	,
Osvěta	osvěta	k1gFnSc1	osvěta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Emanuel	Emanuel	k1gMnSc1	Emanuel
Frynta	Frynta	k1gMnSc1	Frynta
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1963	[number]	k4	1963
a	a	k8xC	a
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
<g/>
,	,	kIx,	,
ČLDJ	ČLDJ	kA	ČLDJ
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jiří	Jiří	k1gMnSc1	Jiří
Míka	Míka	k1gMnSc1	Míka
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
DILIA	DILIA	kA	DILIA
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
<s>
Hrdina	Hrdina	k1gMnSc1	Hrdina
naší	náš	k3xOp1gFnSc2	náš
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Ilek	Ilek	k1gMnSc1	Ilek
a	a	k8xC	a
Zdenka	Zdenka	k1gFnSc1	Zdenka
Bergrová	Bergrová	k1gFnSc1	Bergrová
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
SNKLU	SNKLU	kA	SNKLU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1966	[number]	k4	1966
a	a	k8xC	a
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Básně	báseň	k1gFnPc1	báseň
<g/>
,	,	kIx,	,
SNDK	SNDK	kA	SNDK
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Emanuel	Emanuel	k1gMnSc1	Emanuel
Frynta	Frynta	k1gMnSc1	Frynta
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Hora	Hora	k1gMnSc1	Hora
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Křička	Křička	k1gMnSc1	Křička
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Mathesius	Mathesius	k1gMnSc1	Mathesius
a	a	k8xC	a
Zdenka	Zdenka	k1gFnSc1	Zdenka
Vovsová	Vovsová	k1gFnSc1	Vovsová
<g/>
,	,	kIx,	,
Jak	jak	k8xC	jak
volný	volný	k2eAgInSc1d1	volný
vítr	vítr	k1gInSc1	vítr
<g/>
,	,	kIx,	,
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Petr	Petr	k1gMnSc1	Petr
Křička	Křička	k1gMnSc1	Křička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Hora	Hora	k1gMnSc1	Hora
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Teichman	Teichman	k1gMnSc1	Teichman
a	a	k8xC	a
Bohumil	Bohumil	k1gMnSc1	Bohumil
Mathesius	Mathesius	k1gMnSc1	Mathesius
<g/>
,	,	kIx,	,
Deset	deset	k4xCc1	deset
ruských	ruský	k2eAgFnPc2d1	ruská
novel	novela	k1gFnPc2	novela
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
také	také	k9	také
Tamaň	Tamaň	k1gFnSc4	Tamaň
(	(	kIx(	(
<g/>
překlad	překlad	k1gInSc1	překlad
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Ilek	Ilek	k1gMnSc1	Ilek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Samota	samota	k1gFnSc1	samota
a	a	k8xC	a
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
SNKLU	SNKLU	kA	SNKLU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1964	[number]	k4	1964
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Emanuel	Emanuel	k1gMnSc1	Emanuel
Frynta	Frynta	k1gMnSc1	Frynta
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Hora	Hora	k1gMnSc1	Hora
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
Marčanová	Marčanová	k1gFnSc1	Marčanová
<g/>
,	,	kIx,	,
Proměny	proměna	k1gFnPc1	proměna
démona	démon	k1gMnSc2	démon
<g/>
,	,	kIx,	,
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Zdenka	Zdenka	k1gFnSc1	Zdenka
Bergrová	Bergrová	k1gFnSc1	Bergrová
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
,	,	kIx,	,
Maškaráda	maškaráda	k1gFnSc1	maškaráda
<g/>
,	,	kIx,	,
Dva	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Emanuel	Emanuel	k1gMnSc1	Emanuel
Frynta	Frynta	k1gMnSc1	Frynta
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
Zdenka	Zdenka	k1gFnSc1	Zdenka
Bergrová	Bergrová	k1gFnSc1	Bergrová
<g/>
,	,	kIx,	,
Stesk	stesk	k1gInSc1	stesk
rozumu	rozum	k1gInSc2	rozum
<g/>
,	,	kIx,	,
Lidové	lidový	k2eAgNnSc1d1	lidové
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Zdenka	Zdenka	k1gFnSc1	Zdenka
Bergrová	Bergrová	k1gFnSc1	Bergrová
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Marčanová	Marčanová	k1gFnSc1	Marčanová
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
<g/>
,	,	kIx,	,
Z	z	k7c2	z
plamene	plamen	k1gInSc2	plamen
a	a	k8xC	a
jasu	jas	k1gInSc2	jas
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1978	[number]	k4	1978
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Zdenka	Zdenka	k1gFnSc1	Zdenka
Bergrová	Bergrová	k1gFnSc1	Bergrová
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Hora	Hora	k1gMnSc1	Hora
<g/>
,	,	kIx,	,
Emanuel	Emanuel	k1gMnSc1	Emanuel
Frynta	Frynta	k1gMnSc1	Frynta
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Marčanová	Marčanová	k1gFnSc1	Marčanová
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Křička	Křička	k1gMnSc1	Křička
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
Vrbová	Vrbová	k1gFnSc1	Vrbová
a	a	k8xC	a
Miloš	Miloš	k1gMnSc1	Miloš
Matula	Matula	k1gMnSc1	Matula
<g/>
,	,	kIx,	,
Strážný	strážný	k1gMnSc1	strážný
zvon	zvon	k1gInSc1	zvon
<g/>
,	,	kIx,	,
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Hana	Hana	k1gFnSc1	Hana
Vrbová	Vrbová	k1gFnSc1	Vrbová
<g/>
,	,	kIx,	,
Bludný	bludný	k2eAgInSc1d1	bludný
kruh	kruh	k1gInSc1	kruh
<g/>
,	,	kIx,	,
Astra	astra	k1gFnSc1	astra
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Zdenka	Zdenka	k1gFnSc1	Zdenka
Bergrová	Bergrová	k1gFnSc1	Bergrová
<g/>
.	.	kIx.	.
</s>
