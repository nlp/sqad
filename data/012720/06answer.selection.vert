<s>
Od	od	k7c2
roku	rok	k1gInSc2
1965	[number]	k4
se	se	k3xPyFc4
prodalo	prodat	k5eAaPmAgNnS
50	[number]	k4
milionů	milion	k4xCgInPc2
nosičů	nosič	k1gInPc2
desek	deska	k1gFnPc2
Karla	Karel	k1gMnSc4
Gotta	Gott	k1gMnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1
z	z	k7c2
něj	on	k3xPp3gMnSc2
dělá	dělat	k5eAaImIp3nS
nejúspěšnějšího	úspěšný	k2eAgMnSc4d3
českého	český	k2eAgMnSc4d1
interpreta	interpret	k1gMnSc4
dosud	dosud	k6eAd1
(	(	kIx(
<g/>
aktuální	aktuální	k2eAgInSc1d1
k	k	k7c3
roku	rok	k1gInSc3
2019	[number]	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>