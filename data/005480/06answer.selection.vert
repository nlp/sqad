<s>
Největší	veliký	k2eAgFnSc7d3	veliký
řekou	řeka	k1gFnSc7	řeka
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
je	být	k5eAaImIp3nS	být
Jenisej	Jenisej	k1gInSc1	Jenisej
<g/>
,	,	kIx,	,
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
Ob	Ob	k1gInSc1	Ob
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Irtyšem	Irtyš	k1gInSc7	Irtyš
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
společně	společně	k6eAd1	společně
tvoří	tvořit	k5eAaImIp3nP	tvořit
sedmou	sedmý	k4xOgFnSc4	sedmý
nejdelší	dlouhý	k2eAgFnSc4d3	nejdelší
řeku	řeka	k1gFnSc4	řeka
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
