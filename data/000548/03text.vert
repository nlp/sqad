<s>
Středa	středa	k1gFnSc1	středa
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
označení	označení	k1gNnSc2	označení
dne	den	k1gInSc2	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
mezi	mezi	k7c7	mezi
úterým	úterý	k1gNnSc7	úterý
a	a	k8xC	a
čtvrtkem	čtvrtek	k1gInSc7	čtvrtek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
střed	střed	k1gInSc4	střed
týdne	týden	k1gInSc2	týden
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
středy	středa	k1gFnSc2	středa
jako	jako	k8xC	jako
prostředního	prostřední	k2eAgMnSc2d1	prostřední
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
dne	den	k1gInSc2	den
týdne	týden	k1gInSc2	týden
v	v	k7c6	v
tradičním	tradiční	k2eAgMnSc6d1	tradiční
židovském	židovský	k2eAgMnSc6d1	židovský
a	a	k8xC	a
křesťanském	křesťanský	k2eAgInSc6d1	křesťanský
kalendáři	kalendář	k1gInSc6	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgInSc6d1	český
občanském	občanský	k2eAgInSc6d1	občanský
kalendáři	kalendář	k1gInSc6	kalendář
je	být	k5eAaImIp3nS	být
středa	středa	k1gFnSc1	středa
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
den	den	k1gInSc4	den
třetí	třetí	k4xOgFnSc2	třetí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
křesťanství	křesťanství	k1gNnSc6	křesťanství
je	být	k5eAaImIp3nS	být
středa	středa	k1gFnSc1	středa
vedle	vedle	k7c2	vedle
pátku	pátek	k1gInSc2	pátek
druhým	druhý	k4xOgInSc7	druhý
postním	postní	k2eAgInSc7d1	postní
dnem	den	k1gInSc7	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
byzantském	byzantský	k2eAgInSc6d1	byzantský
ritu	rit	k1gInSc6	rit
je	být	k5eAaImIp3nS	být
středa	středa	k1gFnSc1	středa
dnem	dnem	k7c2	dnem
připomínky	připomínka	k1gFnSc2	připomínka
svatého	svatý	k2eAgMnSc2d1	svatý
Kříže	Kříž	k1gMnSc2	Kříž
nebo	nebo	k8xC	nebo
Bohorodičky	Bohorodička	k1gFnSc2	Bohorodička
<g/>
.	.	kIx.	.
</s>
<s>
Středa	středa	k1gFnSc1	středa
se	se	k3xPyFc4	se
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
latině	latina	k1gFnSc6	latina
nazývala	nazývat	k5eAaImAgFnS	nazývat
Dies	Dies	k1gInSc1	Dies
Mercurii	Mercurie	k1gFnSc3	Mercurie
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
den	den	k1gInSc4	den
Merkurův	Merkurův	k2eAgInSc4d1	Merkurův
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
názvu	název	k1gInSc2	název
jsou	být	k5eAaImIp3nP	být
odvozeny	odvodit	k5eAaPmNgFnP	odvodit
jména	jméno	k1gNnPc4	jméno
středy	středa	k1gFnSc2	středa
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
románských	románský	k2eAgInPc2d1	románský
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
it.	it.	k?	it.
Mercoledì	Mercoledì	k1gMnSc1	Mercoledì
<g/>
,	,	kIx,	,
fr.	fr.	k?	fr.
Mercredi	Mercred	k1gMnPc1	Mercred
<g/>
,	,	kIx,	,
špan.	špan.	k?	špan.
Miercoles	Miercoles	k1gInSc1	Miercoles
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
románských	románský	k2eAgInPc6d1	románský
jazycích	jazyk	k1gInPc6	jazyk
převládl	převládnout	k5eAaPmAgInS	převládnout
středověký	středověký	k2eAgInSc1d1	středověký
latinský	latinský	k2eAgInSc1d1	latinský
termín	termín	k1gInSc1	termín
označující	označující	k2eAgFnSc4d1	označující
středu	středa	k1gFnSc4	středa
jako	jako	k8xC	jako
feria	feria	k1gFnSc1	feria
quarta	quarta	k1gFnSc1	quarta
"	"	kIx"	"
<g/>
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
den	den	k1gInSc1	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
neděle	neděle	k1gFnSc1	neděle
se	se	k3xPyFc4	se
počítala	počítat	k5eAaImAgFnS	počítat
za	za	k7c4	za
první	první	k4xOgInSc4	první
den	den	k1gInSc4	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Srov.	srov.	kA	srov.
např.	např.	kA	např.
portugalské	portugalský	k2eAgFnPc4d1	portugalská
quarta-feira	quartaeiro	k1gNnPc4	quarta-feiro
<g/>
.	.	kIx.	.
</s>
<s>
Angličtina	angličtina	k1gFnSc1	angličtina
označuje	označovat	k5eAaImIp3nS	označovat
středu	středa	k1gFnSc4	středa
jako	jako	k8xC	jako
Wednesday	Wednesdaa	k1gFnPc4	Wednesdaa
<g/>
,	,	kIx,	,
staroangl	staroangnout	k5eAaPmAgMnS	staroangnout
<g/>
.	.	kIx.	.
</s>
<s>
Wódnesdæ	Wódnesdæ	k?	Wódnesdæ
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
den	den	k1gInSc4	den
Ódinův	Ódinův	k2eAgInSc4d1	Ódinův
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
zasvěcený	zasvěcený	k2eAgInSc1d1	zasvěcený
bohu	bůh	k1gMnSc6	bůh
Ódinovi	Ódin	k1gMnSc6	Ódin
<g/>
.	.	kIx.	.
</s>
<s>
Němčina	němčina	k1gFnSc1	němčina
se	se	k3xPyFc4	se
v	v	k7c6	v
případě	případ	k1gInSc6	případ
středy	středa	k1gFnSc2	středa
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
od	od	k7c2	od
angličtiny	angličtina	k1gFnSc2	angličtina
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
čeština	čeština	k1gFnSc1	čeština
označuje	označovat	k5eAaImIp3nS	označovat
středu	středa	k1gFnSc4	středa
za	za	k7c4	za
prostřední	prostřední	k2eAgInSc4d1	prostřední
den	den	k1gInSc4	den
týdne	týden	k1gInSc2	týden
(	(	kIx(	(
<g/>
Mittwoch	Mittwoch	k1gInSc1	Mittwoch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
do	do	k7c2	do
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
však	však	k9	však
nazývala	nazývat	k5eAaImAgNnP	nazývat
středu	středa	k1gFnSc4	středa
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
angličtina	angličtina	k1gFnSc1	angličtina
Wodantag	Wodantaga	k1gFnPc2	Wodantaga
(	(	kIx(	(
<g/>
den	den	k1gInSc4	den
Ódinův	Ódinův	k2eAgInSc4d1	Ódinův
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Finský	finský	k2eAgInSc1d1	finský
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
podobný	podobný	k2eAgInSc1d1	podobný
<g/>
:	:	kIx,	:
Keskiviikko	Keskiviikko	k1gNnSc1	Keskiviikko
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
střed	střed	k1gInSc1	střed
týdne	týden	k1gInSc2	týden
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
islandské	islandský	k2eAgNnSc1d1	islandské
jméno	jméno	k1gNnSc1	jméno
<g/>
:	:	kIx,	:
Mið	Mið	k1gFnSc1	Mið
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
střední	střední	k2eAgInSc4d1	střední
den	den	k1gInSc4	den
týdne	týden	k1gInSc2	týden
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
hebrejské	hebrejský	k2eAgFnSc2d1	hebrejská
bible	bible	k1gFnSc2	bible
je	být	k5eAaImIp3nS	být
středa	středa	k1gFnSc1	středa
dnem	den	k1gInSc7	den
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
stvořeno	stvořen	k2eAgNnSc1d1	stvořeno
Slunce	slunce	k1gNnSc1	slunce
a	a	k8xC	a
Měsíc	měsíc	k1gInSc1	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Středa	středa	k1gFnSc1	středa
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
pozici	pozice	k1gFnSc4	pozice
uprostřed	uprostřed	k7c2	uprostřed
pěti	pět	k4xCc2	pět
pracovních	pracovní	k2eAgInPc2d1	pracovní
dnů	den	k1gInPc2	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
začínají	začínat	k5eAaImIp3nP	začínat
pondělím	pondělí	k1gNnSc7	pondělí
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
pátkem	pátek	k1gInSc7	pátek
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgInSc1d1	anglický
jazykový	jazykový	k2eAgInSc1d1	jazykový
idiom	idiom	k1gInSc1	idiom
pro	pro	k7c4	pro
středu	středa	k1gFnSc4	středa
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
hump	humpit	k5eAaPmRp2nS	humpit
day	day	k?	day
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
den	den	k1gInSc1	den
hrbu	hrb	k1gInSc2	hrb
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odkaz	odkaz	k1gInSc4	odkaz
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pracovní	pracovní	k2eAgInSc1d1	pracovní
týden	týden	k1gInSc1	týden
převalil	převalit	k5eAaPmAgInS	převalit
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tak	tak	k9	tak
o	o	k7c4	o
neformální	formální	k2eNgInSc4d1	neformální
odkaz	odkaz	k1gInSc4	odkaz
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
týdne	týden	k1gInSc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Kvakeři	kvaker	k1gMnPc1	kvaker
tradičně	tradičně	k6eAd1	tradičně
referují	referovat	k5eAaBmIp3nP	referovat
o	o	k7c6	o
středě	středa	k1gFnSc6	středa
jako	jako	k8xS	jako
o	o	k7c6	o
"	"	kIx"	"
<g/>
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
dnu	den	k1gInSc6	den
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vyvarovali	vyvarovat	k5eAaPmAgMnP	vyvarovat
pohanského	pohanský	k2eAgMnSc4d1	pohanský
původu	původa	k1gMnSc4	původa
anglického	anglický	k2eAgInSc2d1	anglický
názvu	název	k1gInSc2	název
tohoto	tento	k3xDgInSc2	tento
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
thajského	thajský	k2eAgInSc2d1	thajský
solárního	solární	k2eAgInSc2d1	solární
kalendáře	kalendář	k1gInSc2	kalendář
je	být	k5eAaImIp3nS	být
středa	středa	k1gFnSc1	středa
spojena	spojit	k5eAaPmNgFnS	spojit
se	s	k7c7	s
zelenou	zelená	k1gFnSc7	zelená
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Astrologické	astrologický	k2eAgNnSc1d1	astrologické
znamení	znamení	k1gNnSc1	znamení
planety	planeta	k1gFnSc2	planeta
Merkur	Merkur	k1gInSc1	Merkur
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
středu	středa	k1gFnSc4	středa
-	-	kIx~	-
Dies	Dies	k1gInSc4	Dies
Mercurii	Mercurie	k1gFnSc4	Mercurie
u	u	k7c2	u
Římanů	Říman	k1gMnPc2	Říman
<g/>
,	,	kIx,	,
s	s	k7c7	s
podobnými	podobný	k2eAgInPc7d1	podobný
názvy	název	k1gInPc7	název
odvozenými	odvozený	k2eAgInPc7d1	odvozený
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
v	v	k7c6	v
románských	románský	k2eAgInPc6d1	románský
jazycích	jazyk	k1gInPc6	jazyk
jako	jako	k8xC	jako
francouzsky	francouzsky	k6eAd1	francouzsky
Mercredi	Mercred	k1gMnPc1	Mercred
a	a	k8xC	a
španělsky	španělsky	k6eAd1	španělsky
Miércoles	Miércolesa	k1gFnPc2	Miércolesa
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
lidových	lidový	k2eAgFnPc2d1	lidová
tradic	tradice	k1gFnPc2	tradice
platila	platit	k5eAaImAgFnS	platit
středa	středa	k1gFnSc1	středa
za	za	k7c4	za
nešťastný	šťastný	k2eNgInSc4d1	nešťastný
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
např.	např.	kA	např.
dnem	den	k1gInSc7	den
pro	pro	k7c4	pro
tzv.	tzv.	kA	tzv.
tiché	tichý	k2eAgFnSc2d1	tichá
svatby	svatba	k1gFnSc2	svatba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pro	pro	k7c4	pro
padlé	padlý	k2eAgFnPc4d1	padlá
dívky	dívka	k1gFnPc4	dívka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
učení	učení	k1gNnSc2	učení
ortodoxní	ortodoxní	k2eAgFnSc2d1	ortodoxní
církve	církev	k1gFnSc2	církev
byla	být	k5eAaImAgFnS	být
středa	středa	k1gFnSc1	středa
dnem	den	k1gInSc7	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Jidáš	jidáš	k1gInSc1	jidáš
Iškariotský	iškariotský	k2eAgInSc1d1	iškariotský
zaprodal	zaprodat	k5eAaPmAgInS	zaprodat
Ježíše	Ježíš	k1gMnSc4	Ježíš
Krista	Kristus	k1gMnSc4	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
středa	středa	k1gFnSc1	středa
normálně	normálně	k6eAd1	normálně
v	v	k7c6	v
ortodoxní	ortodoxní	k2eAgFnSc6d1	ortodoxní
církvi	církev	k1gFnSc6	církev
postním	postní	k2eAgInSc7d1	postní
dnem	den	k1gInSc7	den
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
pátek	pátek	k1gInSc4	pátek
<g/>
.	.	kIx.	.
</s>
<s>
Popeleční	popeleční	k2eAgFnSc1d1	popeleční
středa	středa	k1gFnSc1	středa
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
den	den	k1gInSc4	den
půstu	půst	k1gInSc2	půst
<g/>
,	,	kIx,	,
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
středu	středa	k1gFnSc4	středa
40	[number]	k4	40
dní	den	k1gInPc2	den
před	před	k7c7	před
velikonocemi	velikonoce	k1gFnPc7	velikonoce
<g/>
,	,	kIx,	,
nepočítaje	nepočítaje	k7c4	nepočítaje
neděle	neděle	k1gFnPc4	neděle
<g/>
.	.	kIx.	.
</s>
<s>
Škaredá	škaredý	k2eAgFnSc1d1	škaredá
středa	středa	k1gFnSc1	středa
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
pro	pro	k7c4	pro
středu	středa	k1gFnSc4	středa
před	před	k7c7	před
velikonocemi	velikonoce	k1gFnPc7	velikonoce
<g/>
.	.	kIx.	.
černá	černý	k2eAgFnSc1d1	černá
středa	středa	k1gFnSc1	středa
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
středa	středa	k1gFnSc1	středa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
středa	středa	k1gFnSc1	středa
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
