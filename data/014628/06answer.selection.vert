<s>
Amenitní	Amenitní	k2eAgFnSc1d1
migrace	migrace	k1gFnSc1
(	(	kIx(
<g/>
též	též	k9
požitková	požitkový	k2eAgFnSc1d1
migrace	migrace	k1gFnSc1
nebo	nebo	k8xC
zelená	zelený	k2eAgFnSc1d1
migrace	migrace	k1gFnSc1
nebo	nebo	k8xC
také	také	k9
naturbanizace	naturbanizace	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
sociologický	sociologický	k2eAgInSc1d1
a	a	k8xC
urbanistický	urbanistický	k2eAgInSc1d1
pojem	pojem	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
označuje	označovat	k5eAaImIp3nS
stěhování	stěhování	k1gNnSc4
určité	určitý	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
městských	městský	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
na	na	k7c4
venkov	venkov	k1gInSc4
(	(	kIx(
<g/>
příp	příp	kA
<g/>
.	.	kIx.
též	též	k9
do	do	k7c2
starých	starý	k2eAgNnPc2d1
jader	jádro	k1gNnPc2
menších	malý	k2eAgNnPc2d2
měst	město	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>