<s>
Amenitní	Amenitní	k2eAgFnSc1d1
migrace	migrace	k1gFnSc1
</s>
<s>
Chatová	chatový	k2eAgFnSc1d1
osada	osada	k1gFnSc1
v	v	k7c6
přírodní	přírodní	k2eAgFnSc6d1
rezervaci	rezervace	k1gFnSc6
Hrbáčkovy	Hrbáčkův	k2eAgFnSc2d1
tůně	tůně	k1gFnSc2
v	v	k7c6
okresu	okres	k1gInSc6
Nymburk	Nymburk	k1gInSc1
</s>
<s>
Chatová	chatový	k2eAgFnSc1d1
osada	osada	k1gFnSc1
Felinka	Felinka	k1gFnSc1
u	u	k7c2
Ostré	ostrý	k2eAgFnSc2d1
–	–	k?
typický	typický	k2eAgInSc4d1
cíl	cíl	k1gInSc4
amenitních	amenitní	k2eAgMnPc2d1
migrantů	migrant	k1gMnPc2
</s>
<s>
Amenitní	Amenitní	k2eAgFnSc1d1
migrace	migrace	k1gFnSc1
(	(	kIx(
<g/>
též	též	k9
požitková	požitkový	k2eAgFnSc1d1
migrace	migrace	k1gFnSc1
nebo	nebo	k8xC
zelená	zelený	k2eAgFnSc1d1
migrace	migrace	k1gFnSc1
nebo	nebo	k8xC
také	také	k9
naturbanizace	naturbanizace	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
sociologický	sociologický	k2eAgInSc1d1
a	a	k8xC
urbanistický	urbanistický	k2eAgInSc1d1
pojem	pojem	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
označuje	označovat	k5eAaImIp3nS
stěhování	stěhování	k1gNnSc4
určité	určitý	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
městských	městský	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
na	na	k7c4
venkov	venkov	k1gInSc4
(	(	kIx(
<g/>
příp	příp	kA
<g/>
.	.	kIx.
též	též	k9
do	do	k7c2
starých	starý	k2eAgNnPc2d1
jader	jádro	k1gNnPc2
menších	malý	k2eAgNnPc2d2
měst	město	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
migrace	migrace	k1gFnSc1
není	být	k5eNaImIp3nS
podmíněna	podmínit	k5eAaPmNgFnS
ekonomickými	ekonomický	k2eAgInPc7d1
důvody	důvod	k1gInPc7
<g/>
,	,	kIx,
ale	ale	k8xC
zájmem	zájem	k1gInSc7
o	o	k7c4
jinou	jiný	k2eAgFnSc4d1
kvalitu	kvalita	k1gFnSc4
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
typ	typa	k1gFnPc2
migrantů	migrant	k1gMnPc2
hledá	hledat	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
žít	žít	k5eAaImF
v	v	k7c6
lepším	dobrý	k2eAgNnSc6d2
přírodním	přírodní	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
<g/>
,	,	kIx,
druhý	druhý	k4xOgMnSc1
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c4
kulturní	kulturní	k2eAgNnPc4d1
specifika	specifikon	k1gNnPc4
cílové	cílový	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
–	–	k?
místní	místní	k2eAgInPc4d1
obyčeje	obyčej	k1gInPc4
<g/>
,	,	kIx,
tradice	tradice	k1gFnPc4
apod.	apod.	kA
</s>
<s>
Jednou	jeden	k4xCgFnSc7
z	z	k7c2
dílčích	dílčí	k2eAgFnPc2d1
příčin	příčina	k1gFnPc2
tohoto	tento	k3xDgInSc2
jevu	jev	k1gInSc2
je	být	k5eAaImIp3nS
dostupnost	dostupnost	k1gFnSc1
informačních	informační	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
(	(	kIx(
<g/>
mobilní	mobilní	k2eAgInSc1d1
telefon	telefon	k1gInSc1
<g/>
,	,	kIx,
VoIP	VoIP	k1gFnSc1
<g/>
,	,	kIx,
internet	internet	k1gInSc1
a	a	k8xC
další	další	k2eAgMnPc1d1
<g/>
)	)	kIx)
a	a	k8xC
tedy	tedy	k9
menší	malý	k2eAgFnSc1d2
potřeba	potřeba	k1gFnSc1
cestovat	cestovat	k5eAaImF
každý	každý	k3xTgInSc4
den	den	k1gInSc4
do	do	k7c2
centra	centrum	k1gNnSc2
měst	město	k1gNnPc2
za	za	k7c4
prací	prací	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
se	se	k3xPyFc4
nejedná	jednat	k5eNaImIp3nS
o	o	k7c4
stěhování	stěhování	k1gNnSc4
do	do	k7c2
suburbií	suburbie	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
do	do	k7c2
rekreačních	rekreační	k2eAgFnPc2d1
chalup	chalupa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předstupněm	předstupeň	k1gInSc7
této	tento	k3xDgFnSc2
migrace	migrace	k1gFnSc2
proto	proto	k8xC
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
druhé	druhý	k4xOgNnSc4
bydlení	bydlení	k1gNnSc4
–	–	k?
chataření	chatařený	k2eAgMnPc1d1
a	a	k8xC
chalupaření	chalupařený	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Amenitní	Amenitní	k2eAgFnSc1d1
migrace	migrace	k1gFnSc1
přináší	přinášet	k5eAaImIp3nS
pozitiva	pozitivum	k1gNnPc4
i	i	k8xC
negativa	negativum	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
vesnice	vesnice	k1gFnPc4
je	být	k5eAaImIp3nS
přínosný	přínosný	k2eAgInSc1d1
příliv	příliv	k1gInSc1
nových	nový	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
rizikem	riziko	k1gNnSc7
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
nepřijetí	nepřijetí	k1gNnSc1
přistěhovalců	přistěhovalec	k1gMnPc2
ze	z	k7c2
strany	strana	k1gFnSc2
původních	původní	k2eAgMnPc2d1
vesničanů	vesničan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BARTOŠ	Bartoš	k1gMnSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
;	;	kIx,
KUŠOVÁ	KUŠOVÁ	kA
<g/>
,	,	kIx,
Drahomíra	drahomíra	k1gMnSc1
<g/>
;	;	kIx,
TĚŠITEL	těšitel	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amenitní	Amenitní	k2eAgFnSc1d1
migrace	migrace	k1gFnSc1
do	do	k7c2
venkovských	venkovský	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostelec	Kostelec	k1gInSc1
nad	nad	k7c7
Černými	černý	k2eAgInPc7d1
lesy	les	k1gInPc7
:	:	kIx,
Lesnická	lesnický	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
196	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87154	#num#	k4
<g/>
-	-	kIx~
<g/>
49	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Amišové	Amišové	k2eAgFnSc1d1
</s>
<s>
Druhé	druhý	k4xOgNnSc1
bydlení	bydlení	k1gNnSc1
</s>
<s>
Gentrifikace	Gentrifikace	k1gFnSc1
</s>
<s>
Suburbanizace	Suburbanizace	k1gFnSc1
</s>
<s>
Venkov	venkov	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Informace	informace	k1gFnPc1
o	o	k7c6
amenitní	amenitní	k2eAgFnSc6d1
migraci	migrace	k1gFnSc6
v	v	k7c6
časopisu	časopis	k1gInSc6
Urbanismus	urbanismus	k1gInSc4
a	a	k8xC
územní	územní	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc4d1
zdroj	zdroj	k1gInSc4
<g/>
]	]	kIx)
</s>
<s>
Eva	Eva	k1gFnSc1
Vernerová	Vernerová	k1gFnSc1
<g/>
:	:	kIx,
Formy	forma	k1gFnPc1
amenitní	amenitní	k2eAgFnPc1d1
migrace	migrace	k1gFnSc2
ve	v	k7c6
středních	střední	k2eAgFnPc6d1
Čechách	Čechy	k1gFnPc6
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
Jan	Jan	k1gMnSc1
Loquenz	Loquenz	k1gMnSc1
<g/>
:	:	kIx,
Formy	forma	k1gFnPc1
amenitní	amenitní	k2eAgFnPc1d1
migrace	migrace	k1gFnSc2
na	na	k7c6
Prachaticku	Prachaticko	k1gNnSc6
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
