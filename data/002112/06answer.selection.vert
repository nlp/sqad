<s>
Montréal	Montréal	k1gInSc1	Montréal
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
[	[	kIx(	[
<g/>
mõ	mõ	k?	mõ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
[	[	kIx(	[
<g/>
ˌ	ˌ	k?	ˌ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
kanadské	kanadský	k2eAgNnSc4d1	kanadské
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
provincie	provincie	k1gFnSc2	provincie
Québec	Québec	k1gMnSc1	Québec
a	a	k8xC	a
druhé	druhý	k4xOgNnSc1	druhý
největší	veliký	k2eAgNnSc1d3	veliký
frankofonní	frankofonní	k2eAgNnSc1d1	frankofonní
město	město	k1gNnSc1	město
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
