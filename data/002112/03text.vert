<s>
Montréal	Montréal	k1gInSc1	Montréal
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
[	[	kIx(	[
<g/>
mõ	mõ	k?	mõ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
[	[	kIx(	[
<g/>
ˌ	ˌ	k?	ˌ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
kanadské	kanadský	k2eAgNnSc4d1	kanadské
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
provincie	provincie	k1gFnSc2	provincie
Québec	Québec	k1gMnSc1	Québec
a	a	k8xC	a
druhé	druhý	k4xOgNnSc1	druhý
největší	veliký	k2eAgNnSc1d3	veliký
frankofonní	frankofonní	k2eAgNnSc1d1	frankofonní
město	město	k1gNnSc1	město
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
má	mít	k5eAaImIp3nS	mít
1	[number]	k4	1
886	[number]	k4	886
481	[number]	k4	481
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
aglomerace	aglomerace	k1gFnSc2	aglomerace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Montréal	Montréal	k1gInSc1	Montréal
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
provincie	provincie	k1gFnSc2	provincie
Québec	Québec	k1gInSc1	Québec
<g/>
,	,	kIx,	,
asi	asi	k9	asi
270	[number]	k4	270
kilometrů	kilometr	k1gInPc2	kilometr
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
provinčního	provinční	k2eAgNnSc2d1	provinční
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Québecu	Québecus	k1gInSc2	Québecus
a	a	k8xC	a
190	[number]	k4	190
kilometrů	kilometr	k1gInPc2	kilometr
východně	východně	k6eAd1	východně
od	od	k7c2	od
Ottawy	Ottawa	k1gFnSc2	Ottawa
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
Montréalském	Montréalský	k2eAgInSc6d1	Montréalský
ostrově	ostrov	k1gInSc6	ostrov
<g/>
,	,	kIx,	,
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
řeky	řeka	k1gFnSc2	řeka
Ottawy	Ottawa	k1gFnSc2	Ottawa
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Riviè	Riviè	k1gFnSc3	Riviè
des	des	k1gNnSc6	des
Outaouais	Outaouais	k1gFnPc2	Outaouais
<g/>
)	)	kIx)	)
a	a	k8xC	a
Řeky	řeka	k1gFnSc2	řeka
Svatého	svatý	k2eAgMnSc2d1	svatý
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Fleuve	Fleuev	k1gFnPc4	Fleuev
Saint-Laurent	Saint-Laurent	k1gInSc1	Saint-Laurent
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
zde	zde	k6eAd1	zde
největší	veliký	k2eAgInSc1d3	veliký
vnitrozemský	vnitrozemský	k2eAgInSc1d1	vnitrozemský
přístav	přístav	k1gInSc1	přístav
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Evropanem	Evropan	k1gMnSc7	Evropan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
území	území	k1gNnSc4	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Montréalu	Montréal	k1gInSc2	Montréal
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Jacques	Jacques	k1gMnSc1	Jacques
Cartier	Cartier	k1gMnSc1	Cartier
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1535	[number]	k4	1535
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
vesnice	vesnice	k1gFnSc2	vesnice
Hochelega	Hocheleg	k1gMnSc2	Hocheleg
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
sedmdesát	sedmdesát	k4xCc4	sedmdesát
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
sem	sem	k6eAd1	sem
přijel	přijet	k5eAaPmAgMnS	přijet
Samuel	Samuel	k1gMnSc1	Samuel
de	de	k?	de
Champlain	Champlain	k1gMnSc1	Champlain
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vesnice	vesnice	k1gFnSc1	vesnice
Hochelega	Hochelega	k1gFnSc1	Hochelega
už	už	k6eAd1	už
dávno	dávno	k6eAd1	dávno
neexistovala	existovat	k5eNaImAgFnS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1611	[number]	k4	1611
založil	založit	k5eAaPmAgMnS	založit
La	la	k1gNnSc3	la
Place	plac	k1gInSc5	plac
Royale	Royala	k1gFnSc6	Royala
<g/>
,	,	kIx,	,
kožešinové	kožešinový	k2eAgNnSc4d1	kožešinové
obchodní	obchodní	k2eAgNnSc4d1	obchodní
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
původní	původní	k2eAgMnPc1d1	původní
místní	místní	k2eAgMnPc1d1	místní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Irokézové	Irokéz	k1gMnPc1	Irokéz
své	svůj	k3xOyFgNnSc4	svůj
území	území	k1gNnSc4	území
úspěšně	úspěšně	k6eAd1	úspěšně
ubránili	ubránit	k5eAaPmAgMnP	ubránit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc4	první
trvalé	trvalý	k2eAgNnSc4d1	trvalé
evropské	evropský	k2eAgNnSc4d1	Evropské
osídlení	osídlení	k1gNnSc4	osídlení
Montréalského	Montréalský	k2eAgInSc2d1	Montréalský
ostrova	ostrov	k1gInSc2	ostrov
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
až	až	k9	až
roku	rok	k1gInSc2	rok
1639	[number]	k4	1639
francouzským	francouzský	k2eAgMnSc7d1	francouzský
daňovým	daňový	k2eAgMnSc7d1	daňový
výběrčím	výběrčí	k1gMnSc7	výběrčí
Jérômem	Jérôm	k1gMnSc7	Jérôm
Le	Le	k1gMnSc7	Le
Royerem	Royer	k1gMnSc7	Royer
<g/>
.	.	kIx.	.
</s>
<s>
Misionáři	misionář	k1gMnPc1	misionář
Paul	Paula	k1gFnPc2	Paula
Chomedey	Chomedea	k1gFnSc2	Chomedea
de	de	k?	de
Maisonneuve	Maisonneuv	k1gMnSc5	Maisonneuv
<g/>
,	,	kIx,	,
Jeanne	Jeann	k1gMnSc5	Jeann
Mance	Manka	k1gFnSc3	Manka
a	a	k8xC	a
několik	několik	k4yIc1	několik
francouzských	francouzský	k2eAgMnPc2d1	francouzský
kolonistů	kolonista	k1gMnPc2	kolonista
založili	založit	k5eAaPmAgMnP	založit
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1642	[number]	k4	1642
vyslanectví	vyslanectví	k1gNnSc2	vyslanectví
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
Ville	Ville	k1gFnSc2	Ville
Maria	Maria	k1gFnSc1	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
místo	místo	k1gNnSc1	místo
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
centrem	centrum	k1gNnSc7	centrum
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
kožešinou	kožešina	k1gFnSc7	kožešina
a	a	k8xC	a
katolického	katolický	k2eAgNnSc2d1	katolické
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Irokézové	Irokéz	k1gMnPc1	Irokéz
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
útocích	útok	k1gInPc6	útok
na	na	k7c4	na
osadu	osada	k1gFnSc4	osada
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1701	[number]	k4	1701
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
mír	mír	k1gInSc1	mír
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
pod	pod	k7c7	pod
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
nadvládou	nadvláda	k1gFnSc7	nadvláda
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1760	[number]	k4	1760
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Pierre	Pierr	k1gInSc5	Pierr
François	François	k1gFnSc6	François
de	de	k?	de
Rigaud	Rigauda	k1gFnPc2	Rigauda
a	a	k8xC	a
Marquis	Marquis	k1gFnPc2	Marquis
de	de	k?	de
Vaudreuil-Cavagnal	Vaudreuil-Cavagnal	k1gMnPc1	Vaudreuil-Cavagnal
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
britské	britský	k2eAgFnSc3d1	britská
armádě	armáda	k1gFnSc3	armáda
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Jefreyho	Jefrey	k1gMnSc2	Jefrey
Amhersta	Amherst	k1gMnSc2	Amherst
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1765	[number]	k4	1765
zničil	zničit	k5eAaPmAgInS	zničit
požár	požár	k1gInSc4	požár
jednu	jeden	k4xCgFnSc4	jeden
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Pařížský	pařížský	k2eAgInSc1d1	pařížský
mír	mír	k1gInSc1	mír
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1763	[number]	k4	1763
ukončil	ukončit	k5eAaPmAgInS	ukončit
sedmiletou	sedmiletý	k2eAgFnSc4d1	sedmiletá
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
území	území	k1gNnSc4	území
Nové	Nové	k2eAgFnSc2d1	Nové
Francie	Francie	k1gFnSc2	Francie
padlo	padnout	k5eAaImAgNnS	padnout
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
již	již	k6eAd1	již
jmenovalo	jmenovat	k5eAaBmAgNnS	jmenovat
Montréal	Montréal	k1gInSc4	Montréal
a	a	k8xC	a
rostlo	růst	k5eAaImAgNnS	růst
díky	díky	k7c3	díky
přistěhovalectví	přistěhovalectví	k1gNnSc3	přistěhovalectví
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Montréal	Montréal	k1gMnSc1	Montréal
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
město	město	k1gNnSc4	město
roku	rok	k1gInSc2	rok
1832	[number]	k4	1832
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
přísun	přísun	k1gInSc1	přísun
anglicky	anglicky	k6eAd1	anglicky
mluvících	mluvící	k2eAgMnPc2d1	mluvící
obyvatel	obyvatel	k1gMnPc2	obyvatel
způsobil	způsobit	k5eAaPmAgInS	způsobit
vznik	vznik	k1gInSc1	vznik
dvou	dva	k4xCgMnPc2	dva
zhruba	zhruba	k6eAd1	zhruba
stejně	stejně	k6eAd1	stejně
velkých	velký	k2eAgFnPc2d1	velká
jazykových	jazykový	k2eAgFnPc2d1	jazyková
komunit	komunita	k1gFnPc2	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rostoucí	rostoucí	k2eAgFnSc2d1	rostoucí
anglické	anglický	k2eAgFnSc2d1	anglická
komunity	komunita	k1gFnSc2	komunita
vyplynula	vyplynout	k5eAaPmAgFnS	vyplynout
stavba	stavba	k1gFnSc1	stavba
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
kanadských	kanadský	k2eAgFnPc2d1	kanadská
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
,	,	kIx,	,
McGillovy	McGillův	k2eAgFnSc2d1	McGillova
Univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1852	[number]	k4	1852
měl	mít	k5eAaImAgInS	mít
Montréal	Montréal	k1gInSc1	Montréal
58	[number]	k4	58
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
byl	být	k5eAaImAgInS	být
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
Britské	britský	k2eAgFnSc2d1	britská
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
bezesporu	bezesporu	k9	bezesporu
ekonomickým	ekonomický	k2eAgMnSc7d1	ekonomický
a	a	k8xC	a
kulturním	kulturní	k2eAgNnSc7d1	kulturní
centrem	centrum	k1gNnSc7	centrum
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
připojením	připojení	k1gNnSc7	připojení
sousedním	sousední	k2eAgNnSc7d1	sousední
měst	město	k1gNnPc2	město
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1883	[number]	k4	1883
až	až	k8xS	až
1918	[number]	k4	1918
se	se	k3xPyFc4	se
Montréal	Montréal	k1gInSc1	Montréal
opět	opět	k6eAd1	opět
stal	stát	k5eAaPmAgInS	stát
hlavně	hlavně	k6eAd1	hlavně
frankofonním	frankofonní	k2eAgNnSc7d1	frankofonní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
začala	začít	k5eAaPmAgFnS	začít
tradice	tradice	k1gFnSc1	tradice
střídání	střídání	k1gNnSc2	střídání
francouzsky	francouzsky	k6eAd1	francouzsky
a	a	k8xC	a
anglicky	anglicky	k6eAd1	anglicky
mluvících	mluvící	k2eAgMnPc2d1	mluvící
starostů	starosta	k1gMnPc2	starosta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
trvala	trvat	k5eAaImAgFnS	trvat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
během	během	k7c2	během
prohibice	prohibice	k1gFnSc2	prohibice
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Montréal	Montréal	k1gMnSc1	Montréal
změnil	změnit	k5eAaPmAgMnS	změnit
v	v	k7c4	v
útočiště	útočiště	k1gNnSc4	útočiště
pro	pro	k7c4	pro
Američany	Američan	k1gMnPc4	Američan
toužící	toužící	k2eAgFnSc1d1	toužící
po	po	k7c6	po
alkoholu	alkohol	k1gInSc6	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
rostoucímu	rostoucí	k2eAgInSc3d1	rostoucí
turismu	turismus	k1gInSc3	turismus
zůstala	zůstat	k5eAaPmAgFnS	zůstat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
vysoká	vysoký	k2eAgFnSc1d1	vysoká
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
prohloubená	prohloubený	k2eAgFnSc1d1	prohloubená
krachem	krach	k1gInSc7	krach
na	na	k7c6	na
burze	burza	k1gFnSc6	burza
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
však	však	k9	však
Kanada	Kanada	k1gFnSc1	Kanada
vzpamatovala	vzpamatovat	k5eAaPmAgFnS	vzpamatovat
ze	z	k7c2	z
Světové	světový	k2eAgFnSc2d1	světová
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
objevovat	objevovat	k5eAaImF	objevovat
první	první	k4xOgInPc4	první
mrakodrapy	mrakodrap	k1gInPc4	mrakodrap
jako	jako	k8xS	jako
například	například	k6eAd1	například
Sun	Sun	kA	Sun
Life	Life	k1gNnSc1	Life
Building	Building	k1gInSc1	Building
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
protestoval	protestovat	k5eAaBmAgMnS	protestovat
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
starosta	starosta	k1gMnSc1	starosta
Montréalu	Montréal	k1gInSc2	Montréal
Camillien	Camillina	k1gFnPc2	Camillina
Houde	Houd	k1gInSc5	Houd
proti	proti	k7c3	proti
branné	branný	k2eAgFnSc3d1	Branná
povinnosti	povinnost	k1gFnSc3	povinnost
a	a	k8xC	a
nutil	nutit	k5eAaImAgMnS	nutit
obyvatele	obyvatel	k1gMnPc4	obyvatel
svého	své	k1gNnSc2	své
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ignorovali	ignorovat	k5eAaImAgMnP	ignorovat
vládní	vládní	k2eAgNnSc4d1	vládní
nařízení	nařízení	k1gNnSc4	nařízení
o	o	k7c4	o
registraci	registrace	k1gFnSc4	registrace
všech	všecek	k3xTgMnPc2	všecek
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ottawě	Ottawa	k1gFnSc6	Ottawa
starostova	starostův	k2eAgFnSc1d1	starostova
neposlušnost	neposlušnost	k1gFnSc1	neposlušnost
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
vlnu	vlna	k1gFnSc4	vlna
nevole	nevole	k1gFnSc2	nevole
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
ho	on	k3xPp3gInSc4	on
umístili	umístit	k5eAaPmAgMnP	umístit
do	do	k7c2	do
vězeňského	vězeňský	k2eAgInSc2d1	vězeňský
tábora	tábor	k1gInSc2	tábor
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
vláda	vláda	k1gFnSc1	vláda
přinucena	přinutit	k5eAaPmNgFnS	přinutit
zavést	zavést	k5eAaPmF	zavést
brannou	branný	k2eAgFnSc4d1	Branná
povinnost	povinnost	k1gFnSc4	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
rostl	růst	k5eAaImAgInS	růst
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
zde	zde	k6eAd1	zde
postaveny	postaven	k2eAgFnPc1d1	postavena
stovky	stovka	k1gFnPc1	stovka
katolických	katolický	k2eAgInPc2d1	katolický
kostelů	kostel	k1gInPc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
několik	několik	k4yIc4	několik
někdejších	někdejší	k2eAgFnPc2d1	někdejší
přezdívek	přezdívka	k1gFnPc2	přezdívka
města	město	k1gNnSc2	město
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
the	the	k?	the
city	cit	k1gInPc1	cit
of	of	k?	of
Saints	Saints	k1gInSc1	Saints
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Město	město	k1gNnSc1	město
Svatých	svatá	k1gFnPc2	svatá
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
La	la	k1gNnSc1	la
ville	ville	k1gFnSc2	ville
aux	aux	k?	aux
cent	cent	k1gInSc1	cent
clochers	clochers	k1gInSc1	clochers
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Město	město	k1gNnSc1	město
sta	sto	k4xCgNnPc1	sto
věží	věžit	k5eAaImIp3nS	věžit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
názvů	název	k1gInPc2	název
ulic	ulice	k1gFnPc2	ulice
začínajících	začínající	k2eAgFnPc2d1	začínající
"	"	kIx"	"
<g/>
St	St	kA	St
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Ste	Ste	k?	Ste
<g/>
"	"	kIx"	"
připomíná	připomínat	k5eAaImIp3nS	připomínat
katolickou	katolický	k2eAgFnSc4d1	katolická
minulost	minulost	k1gFnSc4	minulost
i	i	k8xC	i
současnost	současnost	k1gFnSc4	současnost
Montréalu	Montréal	k1gInSc2	Montréal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
konala	konat	k5eAaImAgFnS	konat
velká	velký	k2eAgFnSc1d1	velká
světová	světový	k2eAgFnSc1d1	světová
výstava	výstava	k1gFnSc1	výstava
EXPO	Expo	k1gNnSc4	Expo
<g/>
'	'	kIx"	'
<g/>
67	[number]	k4	67
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
bývalé	bývalý	k2eAgNnSc1d1	bývalé
Československo	Československo	k1gNnSc1	Československo
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
též	též	k9	též
prezentovalo	prezentovat	k5eAaBmAgNnS	prezentovat
svým	svůj	k3xOyFgInSc7	svůj
projektem	projekt	k1gInSc7	projekt
Kinoautomatu	Kinoautomat	k1gInSc2	Kinoautomat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
samotné	samotný	k2eAgNnSc4d1	samotné
město	město	k1gNnSc4	město
Montréal	Montréal	k1gMnSc1	Montréal
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgMnS	být
bezesporu	bezesporu	k9	bezesporu
významný	významný	k2eAgInSc4d1	významný
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
o	o	k7c4	o
devět	devět	k4xCc4	devět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
zopakoval	zopakovat	k5eAaPmAgInS	zopakovat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
sportovní	sportovní	k2eAgFnSc7d1	sportovní
událostí	událost	k1gFnSc7	událost
Montréalu	Montréal	k1gInSc2	Montréal
byly	být	k5eAaImAgFnP	být
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
XXI	XXI	kA	XXI
<g/>
.	.	kIx.	.
letní	letní	k2eAgFnSc2d1	letní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Nejoblíbenějším	oblíbený	k2eAgInSc7d3	nejoblíbenější
sportem	sport	k1gInSc7	sport
v	v	k7c6	v
Montréalu	Montréal	k1gInSc6	Montréal
je	být	k5eAaImIp3nS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
lední	lední	k2eAgInSc4d1	lední
hokej	hokej	k1gInSc4	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgInSc1d1	místní
tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
Montréal	Montréal	k1gInSc1	Montréal
Canadiens	Canadiensa	k1gFnPc2	Canadiensa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
zakládajících	zakládající	k2eAgInPc2d1	zakládající
členů	člen	k1gInPc2	člen
kanadsko-americké	kanadskomerický	k2eAgFnSc2d1	kanadsko-americká
NHL	NHL	kA	NHL
(	(	kIx(	(
<g/>
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
Original	Original	k1gFnSc1	Original
Six	Six	k1gFnSc1	Six
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Montréal	Montréal	k1gInSc4	Montréal
Canadiens	Canadiensa	k1gFnPc2	Canadiensa
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
týmem	tým	k1gInSc7	tým
NHL	NHL	kA	NHL
<g/>
,	,	kIx,	,
získali	získat	k5eAaPmAgMnP	získat
Stanley	Stanle	k2eAgInPc4d1	Stanle
Cup	cup	k1gInSc4	cup
už	už	k9	už
24	[number]	k4	24
<g/>
×	×	k?	×
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgFnPc4	svůj
domácí	domácí	k2eAgInPc1d1	domácí
zápasy	zápas	k1gInPc1	zápas
hrávají	hrávat	k5eAaImIp3nP	hrávat
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
Bell	bell	k1gInSc1	bell
Centre	centr	k1gInSc5	centr
s	s	k7c7	s
oficiální	oficiální	k2eAgFnSc7d1	oficiální
kapacitou	kapacita	k1gFnSc7	kapacita
pro	pro	k7c4	pro
hokej	hokej	k1gInSc4	hokej
21	[number]	k4	21
273	[number]	k4	273
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
místním	místní	k2eAgInPc3d1	místní
sportovním	sportovní	k2eAgInPc3d1	sportovní
klubům	klub	k1gInPc3	klub
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Montréal	Montréal	k1gInSc1	Montréal
Alouettes	Alouettes	k1gInSc1	Alouettes
(	(	kIx(	(
<g/>
kanadský	kanadský	k2eAgInSc1d1	kanadský
fotbal	fotbal	k1gInSc1	fotbal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Montréal	Montréal	k1gMnSc1	Montréal
Impact	Impact	k1gMnSc1	Impact
(	(	kIx(	(
<g/>
fotbal	fotbal	k1gInSc1	fotbal
<g/>
)	)	kIx)	)
a	a	k8xC	a
Montréal	Montréal	k1gMnSc1	Montréal
Matrix	Matrix	k1gInSc1	Matrix
(	(	kIx(	(
<g/>
basketbal	basketbal	k1gInSc1	basketbal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
se	se	k3xPyFc4	se
v	v	k7c4	v
Montréalu	Montréala	k1gFnSc4	Montréala
konalo	konat	k5eAaImAgNnS	konat
první	první	k4xOgNnSc1	první
atletické	atletický	k2eAgNnSc1d1	atletické
mezikontinentální	mezikontinentální	k2eAgNnSc1d1	mezikontinentální
utkání	utkání	k1gNnSc1	utkání
Amerika-Evropa	Amerika-Evropa	k1gFnSc1	Amerika-Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
zde	zde	k6eAd1	zde
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
druhý	druhý	k4xOgInSc4	druhý
Světový	světový	k2eAgInSc4d1	světový
pohár	pohár	k1gInSc4	pohár
v	v	k7c6	v
atletice	atletika	k1gFnSc6	atletika
<g/>
,	,	kIx,	,
předchůdce	předchůdce	k1gMnSc2	předchůdce
dnešního	dnešní	k2eAgInSc2d1	dnešní
Kontinentálního	kontinentální	k2eAgInSc2d1	kontinentální
poháru	pohár	k1gInSc2	pohár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
hostil	hostit	k5eAaImAgInS	hostit
Montréal	Montréal	k1gInSc1	Montréal
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
plavání	plavání	k1gNnSc6	plavání
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
Montréalu	Montréal	k1gInSc2	Montréal
je	být	k5eAaImIp3nS	být
charakterizováno	charakterizovat	k5eAaBmNgNnS	charakterizovat
velikými	veliký	k2eAgInPc7d1	veliký
rozdíly	rozdíl	k1gInPc7	rozdíl
mezi	mezi	k7c7	mezi
létem	léto	k1gNnSc7	léto
a	a	k8xC	a
zimou	zima	k1gFnSc7	zima
<g/>
.	.	kIx.	.
</s>
<s>
Nejchladnějším	chladný	k2eAgInSc7d3	nejchladnější
měsícem	měsíc	k1gInSc7	měsíc
je	být	k5eAaImIp3nS	být
leden	leden	k1gInSc1	leden
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k6eAd1	kolem
-	-	kIx~	-
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
<g/>
,	,	kIx,	,
nejteplejším	teplý	k2eAgInSc6d3	nejteplejší
měsíci	měsíc	k1gInSc6	měsíc
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
vyhupuje	vyhupovat	k5eAaImIp3nS	vyhupovat
až	až	k9	až
k	k	k7c3	k
21	[number]	k4	21
°	°	k?	°
<g/>
C.	C.	kA	C.
Absolutní	absolutní	k2eAgInPc1d1	absolutní
extrémy	extrém	k1gInPc1	extrém
<g/>
:	:	kIx,	:
-	-	kIx~	-
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
leden	leden	k1gInSc1	leden
<g/>
,	,	kIx,	,
únor	únor	k1gInSc1	únor
<g/>
)	)	kIx)	)
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
40	[number]	k4	40
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
červenec	červenec	k1gInSc1	červenec
<g/>
,	,	kIx,	,
srpen	srpen	k1gInSc1	srpen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
dostatek	dostatek	k1gInSc4	dostatek
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
sněhové	sněhový	k2eAgFnPc1d1	sněhová
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
činí	činit	k5eAaImIp3nS	činit
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
000	[number]	k4	000
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Alžír	Alžír	k1gInSc1	Alžír
<g/>
,	,	kIx,	,
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
Dublin	Dublin	k1gInSc1	Dublin
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
Hirošima	Hirošima	k1gFnSc1	Hirošima
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Jerevan	Jerevan	k1gMnSc1	Jerevan
<g/>
,	,	kIx,	,
Arménie	Arménie	k1gFnSc1	Arménie
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Lakhnaú	Lakhnaú	k1gFnSc1	Lakhnaú
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Lyon	Lyon	k1gInSc1	Lyon
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
Manila	Manila	k1gFnSc1	Manila
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Filipíny	Filipíny	k1gFnPc4	Filipíny
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Milán	Milán	k1gInSc1	Milán
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Pusan	Pusan	k1gInSc1	Pusan
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
<g/>
,	,	kIx,	,
ČLR	ČLR	kA	ČLR
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Montréal	Montréal	k1gInSc1	Montréal
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Montreal	Montreal	k1gInSc1	Montreal
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Montrealu	Montreal	k1gInSc2	Montreal
</s>
