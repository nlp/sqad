<s desamb="1">
Roku	rok	k1gInSc2
1922	#num#	k4
objevil	objevit	k5eAaPmAgMnS
polarografii	polarografie	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
metoda	metoda	k1gFnSc1
používající	používající	k2eAgNnSc1d1
měření	měření	k1gNnSc1
elektrického	elektrický	k2eAgInSc2d1
proudu	proud	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
prochází	procházet	k5eAaImIp3nS
rtuťovou	rtuťový	k2eAgFnSc7d1
kapkou	kapka	k1gFnSc7
a	a	k8xC
roztokem	roztok	k1gInSc7
<g/>
,	,	kIx,
do	do	k7c2
něhož	jenž	k3xRgNnSc2
rtuť	rtuť	k1gFnSc1
odkapává	odkapávat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>