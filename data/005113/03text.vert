<s>
Cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
Le	Le	k1gFnSc1	Le
Voyage	Voyag	k1gFnSc2	Voyag
dans	dans	k6eAd1	dans
la	la	k1gNnSc3	la
Lune	Lun	k1gFnSc2	Lun
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
francouzský	francouzský	k2eAgInSc1d1	francouzský
němý	němý	k2eAgInSc1d1	němý
černobílý	černobílý	k2eAgInSc1d1	černobílý
vědeckofantastický	vědeckofantastický	k2eAgInSc1d1	vědeckofantastický
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
volně	volně	k6eAd1	volně
inspirován	inspirovat	k5eAaBmNgInS	inspirovat
dvěma	dva	k4xCgInPc7	dva
populárními	populární	k2eAgInPc7d1	populární
romány	román	k1gInPc7	román
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
Ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
od	od	k7c2	od
Julese	Julese	k1gFnSc2	Julese
Verna	Verno	k1gNnSc2	Verno
a	a	k8xC	a
První	první	k4xOgMnPc1	první
lidé	člověk	k1gMnPc1	člověk
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
od	od	k7c2	od
H.	H.	kA	H.
G.	G.	kA	G.
Wellse	Wells	k1gMnSc2	Wells
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc4	film
napsal	napsat	k5eAaBmAgMnS	napsat
i	i	k8xC	i
režíroval	režírovat	k5eAaImAgMnS	režírovat
Georges	Georges	k1gMnSc1	Georges
Méliè	Méliè	k1gMnSc1	Méliè
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
Gastona	Gaston	k1gMnSc2	Gaston
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
má	mít	k5eAaImIp3nS	mít
14	[number]	k4	14
minut	minuta	k1gFnPc2	minuta
při	při	k7c6	při
16	[number]	k4	16
snímcích	snímek	k1gInPc6	snímek
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
první	první	k4xOgFnSc4	první
široce	široko	k6eAd1	široko
distribuovaný	distribuovaný	k2eAgInSc4d1	distribuovaný
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
kompletní	kompletní	k2eAgInSc1d1	kompletní
příběh	příběh	k1gInSc1	příběh
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
za	za	k7c4	za
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
z	z	k7c2	z
kina	kino	k1gNnSc2	kino
stalo	stát	k5eAaPmAgNnS	stát
médium	médium	k1gNnSc4	médium
vyprávějící	vyprávějící	k2eAgInPc4d1	vyprávějící
příběhy	příběh	k1gInPc4	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Dřívější	dřívější	k2eAgInPc1d1	dřívější
filmy	film	k1gInPc1	film
byly	být	k5eAaImAgInP	být
většinou	většinou	k6eAd1	většinou
krátké	krátký	k2eAgInPc4d1	krátký
(	(	kIx(	(
<g/>
méně	málo	k6eAd2	málo
než	než	k8xS	než
jedna	jeden	k4xCgFnSc1	jeden
minuta	minuta	k1gFnSc1	minuta
<g/>
)	)	kIx)	)
klipy	klip	k1gInPc4	klip
z	z	k7c2	z
každodenního	každodenní	k2eAgInSc2d1	každodenní
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Dělníci	dělník	k1gMnPc1	dělník
odcházející	odcházející	k2eAgMnPc1d1	odcházející
z	z	k7c2	z
Lumiè	Lumiè	k1gFnSc2	Lumiè
továrny	továrna	k1gFnSc2	továrna
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Příjezd	příjezd	k1gInSc1	příjezd
vlaku	vlak	k1gInSc2	vlak
od	od	k7c2	od
bratrů	bratr	k1gMnPc2	bratr
Lumiè	Lumiè	k1gFnPc2	Lumiè
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
populární	populární	k2eAgFnPc1d1	populární
jen	jen	k9	jen
díky	díky	k7c3	díky
novotě	novota	k1gFnSc3	novota
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
první	první	k4xOgInSc4	první
vědeckofantastický	vědeckofantastický	k2eAgInSc4d1	vědeckofantastický
film	film	k1gInSc4	film
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
inovativní	inovativní	k2eAgFnPc4d1	inovativní
animace	animace	k1gFnPc4	animace
a	a	k8xC	a
speciální	speciální	k2eAgInPc4d1	speciální
efekty	efekt	k1gInPc4	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
setkání	setkání	k1gNnSc6	setkání
astronomů	astronom	k1gMnPc2	astronom
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
navrhne	navrhnout	k5eAaPmIp3nS	navrhnout
ostatním	ostatní	k2eAgMnPc3d1	ostatní
cestu	cesta	k1gFnSc4	cesta
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
určitý	určitý	k2eAgInSc4d1	určitý
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
šest	šest	k4xCc1	šest
statečných	statečný	k2eAgMnPc2d1	statečný
astronomů	astronom	k1gMnPc2	astronom
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
s	s	k7c7	s
plánem	plán	k1gInSc7	plán
<g/>
.	.	kIx.	.
</s>
<s>
Postaví	postavit	k5eAaPmIp3nS	postavit
kapsli	kapsle	k1gFnSc4	kapsle
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
projektilu	projektil	k1gInSc2	projektil
a	a	k8xC	a
obrovský	obrovský	k2eAgInSc4d1	obrovský
kanón	kanón	k1gInSc4	kanón
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
ji	on	k3xPp3gFnSc4	on
hodlají	hodlat	k5eAaImIp3nP	hodlat
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
vystřelit	vystřelit	k5eAaPmF	vystřelit
<g/>
.	.	kIx.	.
</s>
<s>
Astronomové	astronom	k1gMnPc1	astronom
nastoupí	nastoupit	k5eAaPmIp3nP	nastoupit
a	a	k8xC	a
kapsle	kapsle	k1gFnSc1	kapsle
je	být	k5eAaImIp3nS	být
vystřelena	vystřelit	k5eAaPmNgFnS	vystřelit
za	za	k7c2	za
asistence	asistence	k1gFnSc2	asistence
skupiny	skupina	k1gFnSc2	skupina
krásných	krásný	k2eAgFnPc2d1	krásná
žen	žena	k1gFnPc2	žena
(	(	kIx(	(
<g/>
hrají	hrát	k5eAaImIp3nP	hrát
je	on	k3xPp3gFnPc4	on
sboristky	sboristka	k1gFnPc4	sboristka
Folies	Folies	k1gMnSc1	Folies
Bergè	Bergè	k1gMnSc1	Bergè
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
sleduje	sledovat	k5eAaImIp3nS	sledovat
přibližující	přibližující	k2eAgNnSc4d1	přibližující
se	se	k3xPyFc4	se
kapsli	kapsle	k1gFnSc6	kapsle
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
ho	on	k3xPp3gMnSc4	on
zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
do	do	k7c2	do
oka	oko	k1gNnSc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bezpečném	bezpečný	k2eAgNnSc6d1	bezpečné
přistání	přistání	k1gNnSc6	přistání
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
astronomové	astronom	k1gMnPc1	astronom
vystoupí	vystoupit	k5eAaPmIp3nP	vystoupit
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
a	a	k8xC	a
v	v	k7c6	v
dáli	dál	k1gFnSc6	dál
sledují	sledovat	k5eAaImIp3nP	sledovat
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Něco	něco	k3yInSc1	něco
poté	poté	k6eAd1	poté
poblíž	poblíž	k7c2	poblíž
nich	on	k3xPp3gFnPc2	on
exploduje	explodovat	k5eAaBmIp3nS	explodovat
<g/>
.	.	kIx.	.
</s>
<s>
Astronomové	astronom	k1gMnPc1	astronom
rozbalí	rozbalit	k5eAaPmIp3nP	rozbalit
své	svůj	k3xOyFgFnPc4	svůj
deky	deka	k1gFnPc4	deka
a	a	k8xC	a
jdou	jít	k5eAaImIp3nP	jít
si	se	k3xPyFc3	se
zdřímnout	zdřímnout	k5eAaPmF	zdřímnout
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
o	o	k7c6	o
božských	božský	k2eAgFnPc6d1	božská
dívkách	dívka	k1gFnPc6	dívka
z	z	k7c2	z
Folies-Bergè	Folies-Bergè	k1gFnPc2	Folies-Bergè
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
tváře	tvář	k1gFnPc1	tvář
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
ve	v	k7c6	v
hvězdách	hvězda	k1gFnPc6	hvězda
Velkého	velký	k2eAgInSc2d1	velký
vozu	vůz	k1gInSc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
za	za	k7c4	za
chvíli	chvíle	k1gFnSc4	chvíle
objeví	objevit	k5eAaPmIp3nS	objevit
Saturn	Saturn	k1gInSc1	Saturn
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	on	k3xPp3gNnSc4	on
sleduje	sledovat	k5eAaImIp3nS	sledovat
z	z	k7c2	z
okna	okno	k1gNnSc2	okno
své	svůj	k3xOyFgFnSc2	svůj
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
Phoebe	Phoeb	k1gInSc5	Phoeb
<g/>
,	,	kIx,	,
bohyně	bohyně	k1gFnSc1	bohyně
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
sedící	sedící	k2eAgFnSc1d1	sedící
na	na	k7c6	na
srpku	srpek	k1gInSc6	srpek
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Phoebe	Phoebat	k5eAaPmIp3nS	Phoebat
přivolá	přivolat	k5eAaPmIp3nS	přivolat
sněžení	sněžení	k1gNnSc4	sněžení
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
astronomy	astronom	k1gMnPc4	astronom
probudí	probudit	k5eAaPmIp3nS	probudit
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
chtějí	chtít	k5eAaImIp3nP	chtít
ukrýt	ukrýt	k5eAaPmF	ukrýt
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
objeví	objevit	k5eAaPmIp3nS	objevit
obrovské	obrovský	k2eAgFnPc4d1	obrovská
houby	houba	k1gFnPc4	houba
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
astronom	astronom	k1gMnSc1	astronom
otevře	otevřít	k5eAaPmIp3nS	otevřít
svůj	svůj	k3xOyFgInSc4	svůj
deštník	deštník	k1gInSc4	deštník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
porovnal	porovnat	k5eAaPmAgMnS	porovnat
jeho	jeho	k3xOp3gFnSc4	jeho
velikost	velikost	k1gFnSc4	velikost
s	s	k7c7	s
houbami	houba	k1gFnPc7	houba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
deštníku	deštník	k1gInSc2	deštník
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
houba	houba	k1gFnSc1	houba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
chvíli	chvíle	k1gFnSc6	chvíle
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
Selenité	Selenitý	k2eAgNnSc1d1	Selenitý
<g/>
,	,	kIx,	,
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
hmyz	hmyz	k1gInSc1	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
astronomů	astronom	k1gMnPc2	astronom
prvního	první	k4xOgInSc2	první
Selenitu	Selenit	k1gInSc2	Selenit
zabije	zabít	k5eAaPmIp3nS	zabít
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
udeří	udeřit	k5eAaPmIp3nP	udeřit
svým	svůj	k3xOyFgInSc7	svůj
deštníkem	deštník	k1gInSc7	deštník
a	a	k8xC	a
ten	ten	k3xDgInSc4	ten
exploduje	explodovat	k5eAaBmIp3nS	explodovat
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
však	však	k9	však
další	další	k2eAgFnPc4d1	další
Selenité	Selenitý	k2eAgFnPc4d1	Selenitý
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
astronomy	astronom	k1gMnPc4	astronom
je	být	k5eAaImIp3nS	být
těžké	těžký	k2eAgNnSc1d1	těžké
se	se	k3xPyFc4	se
ubránit	ubránit	k5eAaPmF	ubránit
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
Selenity	Selenit	k2eAgFnPc1d1	Selenit
zajati	zajat	k2eAgMnPc1d1	zajat
a	a	k8xC	a
přivedeni	přiveden	k2eAgMnPc1d1	přiveden
k	k	k7c3	k
jejich	jejich	k3xOp3gMnSc3	jejich
králi	král	k1gMnSc3	král
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
astronomů	astronom	k1gMnPc2	astronom
zvedne	zvednout	k5eAaPmIp3nS	zvednout
krále	král	k1gMnSc4	král
z	z	k7c2	z
jeho	on	k3xPp3gInSc2	on
trůnu	trůn	k1gInSc2	trůn
<g/>
,	,	kIx,	,
udeří	udeřit	k5eAaPmIp3nP	udeřit
jím	on	k3xPp3gNnSc7	on
o	o	k7c4	o
zem	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
ho	on	k3xPp3gMnSc4	on
zabije	zabít	k5eAaPmIp3nS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Astronomové	astronom	k1gMnPc1	astronom
běží	běžet	k5eAaImIp3nP	běžet
zpět	zpět	k6eAd1	zpět
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
kapsli	kapsle	k1gFnSc3	kapsle
a	a	k8xC	a
pět	pět	k4xCc1	pět
se	se	k3xPyFc4	se
jich	on	k3xPp3gFnPc2	on
dostane	dostat	k5eAaPmIp3nS	dostat
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
.	.	kIx.	.
</s>
<s>
Šestý	šestý	k4xOgInSc1	šestý
použije	použít	k5eAaPmIp3nS	použít
lano	lano	k1gNnSc4	lano
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přetáhl	přetáhnout	k5eAaPmAgMnS	přetáhnout
kapsli	kapsle	k1gFnSc4	kapsle
přes	přes	k7c4	přes
okraj	okraj	k1gInSc4	okraj
Měsíce	měsíc	k1gInSc2	měsíc
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Selenité	Selenitý	k2eAgNnSc1d1	Selenitý
se	se	k3xPyFc4	se
pokusí	pokusit	k5eAaPmIp3nS	pokusit
kapsli	kapsle	k1gFnSc4	kapsle
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
získat	získat	k5eAaPmF	získat
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
drží	držet	k5eAaImIp3nS	držet
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
padá	padat	k5eAaImIp3nS	padat
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Astronom	astronom	k1gMnSc1	astronom
<g/>
,	,	kIx,	,
kapsle	kapsle	k1gFnPc1	kapsle
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
Selenita	Selenita	k1gFnSc1	Selenita
padají	padat	k5eAaImIp3nP	padat
vesmírem	vesmír	k1gInSc7	vesmír
a	a	k8xC	a
přistanou	přistat	k5eAaPmIp3nP	přistat
v	v	k7c6	v
oceánu	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
jsou	být	k5eAaImIp3nP	být
lodí	loď	k1gFnSc7	loď
zachráněni	zachránit	k5eAaPmNgMnP	zachránit
a	a	k8xC	a
odvezeni	odvézt	k5eAaPmNgMnP	odvézt
na	na	k7c4	na
břeh	břeh	k1gInSc4	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
scéna	scéna	k1gFnSc1	scéna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
astronomové	astronom	k1gMnPc1	astronom
oslavování	oslavování	k1gNnSc2	oslavování
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
součástí	součást	k1gFnPc2	součást
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
filmu	film	k1gInSc2	film
je	být	k5eAaImIp3nS	být
ztracena	ztratit	k5eAaPmNgFnS	ztratit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedávno	nedávno	k6eAd1	nedávno
byly	být	k5eAaImAgInP	být
fragmenty	fragment	k1gInPc1	fragment
poslední	poslední	k2eAgFnSc2d1	poslední
scény	scéna	k1gFnSc2	scéna
nalezeny	nalézt	k5eAaBmNgInP	nalézt
<g/>
.	.	kIx.	.
</s>
<s>
Méliè	Méliè	k?	Méliè
chtěl	chtít	k5eAaImAgMnS	chtít
vydat	vydat	k5eAaPmF	vydat
film	film	k1gInSc4	film
po	po	k7c6	po
celých	celý	k2eAgInPc6d1	celý
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
vydělat	vydělat	k5eAaPmF	vydělat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Thomas	Thomas	k1gMnSc1	Thomas
Alva	Alva	k1gMnSc1	Alva
Edison	Edison	k1gMnSc1	Edison
udělal	udělat	k5eAaPmAgMnS	udělat
tajně	tajně	k6eAd1	tajně
několik	několik	k4yIc4	několik
kopií	kopie	k1gFnPc2	kopie
a	a	k8xC	a
sám	sám	k3xTgInSc1	sám
je	být	k5eAaImIp3nS	být
distribuoval	distribuovat	k5eAaBmAgMnS	distribuovat
a	a	k8xC	a
vydělával	vydělávat	k5eAaImAgMnS	vydělávat
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
<g/>
.	.	kIx.	.
</s>
<s>
Méliè	Méliè	k?	Méliè
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
nikdy	nikdy	k6eAd1	nikdy
neprofitoval	profitovat	k5eNaBmAgMnS	profitovat
<g/>
.	.	kIx.	.
</s>
<s>
Scéna	scéna	k1gFnSc1	scéna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
Měsíc	měsíc	k1gInSc1	měsíc
zasažen	zasáhnout	k5eAaPmNgInS	zasáhnout
do	do	k7c2	do
oka	oko	k1gNnSc2	oko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
použití	použití	k1gNnSc1	použití
stop-motion	stopotion	k1gInSc4	stop-motion
animace	animace	k1gFnSc2	animace
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
film	film	k1gInSc1	film
inspiroval	inspirovat	k5eAaBmAgInS	inspirovat
videoklipy	videoklip	k1gInPc4	videoklip
Heaven	Heavna	k1gFnPc2	Heavna
for	forum	k1gNnPc2	forum
Everyone	Everyon	k1gInSc5	Everyon
od	od	k7c2	od
Queen	Quena	k1gFnPc2	Quena
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
a	a	k8xC	a
Tonight	Tonight	k1gMnSc1	Tonight
<g/>
,	,	kIx,	,
Tonight	Tonight	k1gMnSc1	Tonight
od	od	k7c2	od
Smashing	Smashing	k1gInSc4	Smashing
Pumpkins	Pumpkinsa	k1gFnPc2	Pumpkinsa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Záběr	záběr	k1gInSc1	záběr
z	z	k7c2	z
filmu	film	k1gInSc2	film
je	být	k5eAaImIp3nS	být
použit	použít	k5eAaPmNgInS	použít
ve	v	k7c6	v
videoklipu	videoklip	k1gInSc6	videoklip
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
"	"	kIx"	"
<g/>
Man	Man	k1gMnSc1	Man
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Moon	Moon	k1gNnSc1	Moon
<g/>
"	"	kIx"	"
od	od	k7c2	od
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
francouzské	francouzský	k2eAgFnSc6d1	francouzská
stodole	stodola	k1gFnSc6	stodola
nalezen	nalezen	k2eAgMnSc1d1	nalezen
zatím	zatím	k6eAd1	zatím
nejkompletnější	kompletní	k2eAgInSc4d3	nejkompletnější
sestřih	sestřih	k1gInSc4	sestřih
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
navíc	navíc	k6eAd1	navíc
ručně	ručně	k6eAd1	ručně
obarven	obarven	k2eAgInSc1d1	obarven
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
restaurován	restaurovat	k5eAaBmNgInS	restaurovat
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Le	Le	k1gFnSc2	Le
Giornate	Giornat	k1gInSc5	Giornat
del	del	k?	del
Cinema	Cinem	k1gMnSc4	Cinem
Muto	Muto	k1gMnSc1	Muto
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
parodován	parodovat	k5eAaImNgInS	parodovat
v	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
The	The	k1gMnSc1	The
Series	Series	k1gMnSc1	Series
Has	hasit	k5eAaImRp2nS	hasit
Landed	Landed	k1gMnSc1	Landed
seriálu	seriál	k1gInSc2	seriál
Futurama	Futurama	k?	Futurama
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Bender	Bender	k1gInSc1	Bender
vrazí	vrazit	k5eAaPmIp3nS	vrazit
láhev	láhev	k1gFnSc4	láhev
piva	pivo	k1gNnSc2	pivo
do	do	k7c2	do
oka	oko	k1gNnSc2	oko
maskota	maskot	k1gMnSc2	maskot
měsíčního	měsíční	k2eAgInSc2d1	měsíční
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Parodie	parodie	k1gFnSc1	parodie
filmu	film	k1gInSc2	film
se	se	k3xPyFc4	se
také	také	k9	také
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Simpsonovi	Simpson	k1gMnSc3	Simpson
v	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
Moe	Moe	k1gFnSc2	Moe
Letter	Lettra	k1gFnPc2	Lettra
Blues	blues	k1gNnSc1	blues
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Smrt	smrt	k1gFnSc1	smrt
kočky	kočka	k1gFnSc2	kočka
na	na	k7c6	na
měsíci	měsíc	k1gInSc6	měsíc
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
film	film	k1gInSc4	film
natočily	natočit	k5eAaBmAgFnP	natočit
hlavní	hlavní	k2eAgFnPc1d1	hlavní
postavičky	postavička	k1gFnPc1	postavička
z	z	k7c2	z
Itchy	Itcha	k1gFnSc2	Itcha
&	&	k?	&
Scratchy	Scratcha	k1gFnSc2	Scratcha
Show	show	k1gFnSc2	show
<g/>
.	.	kIx.	.
</s>
<s>
Záběry	záběr	k1gInPc1	záběr
přistání	přistání	k1gNnSc2	přistání
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
byly	být	k5eAaImAgFnP	být
použity	použít	k5eAaPmNgFnP	použít
ve	v	k7c6	v
filmu	film	k1gInSc6	film
The	The	k1gMnSc1	The
Adventures	Adventures	k1gMnSc1	Adventures
of	of	k?	of
Pluto	Pluto	k1gMnSc1	Pluto
Nash	Nash	k1gMnSc1	Nash
<g/>
.	.	kIx.	.
</s>
<s>
Natáčení	natáčení	k1gNnSc1	natáčení
tohoto	tento	k3xDgInSc2	tento
filmu	film	k1gInSc2	film
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
tvůrci	tvůrce	k1gMnPc1	tvůrce
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
byl	být	k5eAaImAgMnS	být
Georges	Georges	k1gMnSc1	Georges
Méliè	Méliè	k1gMnSc1	Méliè
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
film	film	k1gInSc1	film
Hugo	Hugo	k1gMnSc1	Hugo
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
velký	velký	k2eAgInSc1d1	velký
objev	objev	k1gInSc1	objev
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
filmu	film	k1gInSc2	film
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
George	George	k1gFnSc1	George
Meliés	Meliés	k1gInSc1	Meliés
<g/>
,	,	kIx,	,
Cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
-	-	kIx~	-
Le	Le	k1gFnSc1	Le
Voyage	Voyag	k1gFnSc2	Voyag
dans	dans	k6eAd1	dans
la	la	k1gNnSc2	la
Lune	Lune	k1gNnSc1	Lune
článek	článek	k1gInSc1	článek
na	na	k7c6	na
Neviditelném	viditelný	k2eNgMnSc6d1	Neviditelný
psu	pes	k1gMnSc6	pes
Cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
na	na	k7c4	na
IMDb	IMDb	k1gInSc4	IMDb
Kompletní	kompletní	k2eAgInSc1d1	kompletní
film	film	k1gInSc1	film
s	s	k7c7	s
anglickým	anglický	k2eAgInSc7d1	anglický
komentářem	komentář	k1gInSc7	komentář
Kompletní	kompletní	k2eAgInSc1d1	kompletní
film	film	k1gInSc1	film
s	s	k7c7	s
francouzským	francouzský	k2eAgInSc7d1	francouzský
komentářem	komentář	k1gInSc7	komentář
Online	Onlin	k1gInSc5	Onlin
Streaming	Streaming	k1gInSc1	Streaming
Video	video	k1gNnSc1	video
Voyage	Voyag	k1gMnSc2	Voyag
Homage	Homag	k1gMnSc2	Homag
Cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
měsíc	měsíc	k1gInSc4	měsíc
–	–	k?	–
(	(	kIx(	(
<g/>
Voyage	Voyag	k1gMnSc2	Voyag
dans	dans	k6eAd1	dans
la	la	k1gNnSc4	la
Lune	Lun	k1gFnSc2	Lun
<g/>
)	)	kIx)	)
–	–	k?	–
75	[number]	k4	75
%	%	kIx~	%
na	na	k7c6	na
FilmCZ	FilmCZ	k1gFnSc6	FilmCZ
-	-	kIx~	-
</s>
