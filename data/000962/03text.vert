<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hašek	Hašek	k1gMnSc1	Hašek
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1883	[number]	k4	1883
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1923	[number]	k4	1923
Lipnice	Lipnice	k1gFnSc1	Lipnice
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
publicista	publicista	k1gMnSc1	publicista
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
nejpřekládanější	překládaný	k2eAgFnSc2d3	nejpřekládanější
knihy	kniha	k1gFnSc2	kniha
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
(	(	kIx(	(
<g/>
58	[number]	k4	58
jazyků	jazyk	k1gInPc2	jazyk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proslulých	proslulý	k2eAgInPc2d1	proslulý
Osudů	osud	k1gInPc2	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc4	Švejk
za	za	k7c2	za
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Hašek	Hašek	k1gMnSc1	Hašek
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
učitele	učitel	k1gMnSc2	učitel
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
předčasně	předčasně	k6eAd1	předčasně
zesnul	zesnout	k5eAaPmAgMnS	zesnout
na	na	k7c4	na
intoxikaci	intoxikace	k1gFnSc4	intoxikace
alkoholem	alkohol	k1gInSc7	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Chudoba	chudoba	k1gFnSc1	chudoba
nutila	nutit	k5eAaImAgFnS	nutit
jeho	jeho	k3xOp3gFnSc4	jeho
matku	matka	k1gFnSc4	matka
Kateřinu	Kateřina	k1gFnSc4	Kateřina
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
dětmi	dítě	k1gFnPc7	dítě
stěhovat	stěhovat	k5eAaImF	stěhovat
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
než	než	k8xS	než
patnáctkrát	patnáctkrát	k6eAd1	patnáctkrát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nedokončených	dokončený	k2eNgNnPc6d1	nedokončené
studiích	studio	k1gNnPc6	studio
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
se	se	k3xPyFc4	se
vyučil	vyučit	k5eAaPmAgInS	vyučit
drogistou	drogista	k1gMnSc7	drogista
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
maturoval	maturovat	k5eAaBmAgMnS	maturovat
na	na	k7c6	na
obchodní	obchodní	k2eAgFnSc6d1	obchodní
akademii	akademie	k1gFnSc6	akademie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
jako	jako	k9	jako
školák	školák	k1gMnSc1	školák
antiněmeckých	antiněmecký	k2eAgMnPc2d1	antiněmecký
nepokojů	nepokoj	k1gInPc2	nepokoj
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
zaměstnancem	zaměstnanec	k1gMnSc7	zaměstnanec
banky	banka	k1gFnSc2	banka
Slavia	Slavia	k1gFnSc1	Slavia
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
ale	ale	k9	ale
začal	začít	k5eAaPmAgInS	začít
živit	živit	k5eAaImF	živit
výhradně	výhradně	k6eAd1	výhradně
novinařinou	novinařina	k1gFnSc7	novinařina
a	a	k8xC	a
literaturou	literatura	k1gFnSc7	literatura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
také	také	k9	také
seznámil	seznámit	k5eAaPmAgInS	seznámit
s	s	k7c7	s
českými	český	k2eAgMnPc7d1	český
anarchisty	anarchista	k1gMnPc7	anarchista
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
vést	vést	k5eAaImF	vést
bohémský	bohémský	k2eAgInSc4d1	bohémský
a	a	k8xC	a
tulácký	tulácký	k2eAgInSc4d1	tulácký
život	život	k1gInSc4	život
(	(	kIx(	(
<g/>
prošel	projít	k5eAaPmAgInS	projít
pěšky	pěšky	k6eAd1	pěšky
mj.	mj.	kA	mj.
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
,	,	kIx,	,
Halič	Halič	k1gFnSc1	Halič
<g/>
,	,	kIx,	,
Uhry	Uhry	k1gFnPc1	Uhry
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Povídky	povídka	k1gFnPc4	povídka
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
cest	cesta	k1gFnPc2	cesta
mu	on	k3xPp3gMnSc3	on
tehdy	tehdy	k6eAd1	tehdy
otiskovaly	otiskovat	k5eAaImAgInP	otiskovat
Národní	národní	k2eAgInPc1d1	národní
listy	list	k1gInPc1	list
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
redaktorem	redaktor	k1gMnSc7	redaktor
anarchistického	anarchistický	k2eAgInSc2d1	anarchistický
časopisu	časopis	k1gInSc2	časopis
Komuna	komuna	k1gFnSc1	komuna
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
krátce	krátce	k6eAd1	krátce
vězněn	věznit	k5eAaImNgMnS	věznit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
se	se	k3xPyFc4	se
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
do	do	k7c2	do
Jarmily	Jarmila	k1gFnSc2	Jarmila
Mayerové	Mayerová	k1gFnSc2	Mayerová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gInSc3	jeho
bohémskému	bohémský	k2eAgInSc3d1	bohémský
životu	život	k1gInSc3	život
ho	on	k3xPp3gNnSc4	on
její	její	k3xOp3gMnPc1	její
rodiče	rodič	k1gMnPc1	rodič
nepokládali	pokládat	k5eNaImAgMnP	pokládat
za	za	k7c4	za
vhodného	vhodný	k2eAgMnSc4d1	vhodný
partnera	partner	k1gMnSc4	partner
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
za	za	k7c4	za
znesvěcení	znesvěcení	k1gNnSc4	znesvěcení
vlajky	vlajka	k1gFnSc2	vlajka
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
rodiče	rodič	k1gMnPc1	rodič
Mayerovou	Mayerová	k1gFnSc4	Mayerová
vzali	vzít	k5eAaPmAgMnP	vzít
na	na	k7c4	na
venkov	venkov	k1gInSc4	venkov
v	v	k7c6	v
naději	naděje	k1gFnSc6	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
skončit	skončit	k5eAaPmF	skončit
jejich	jejich	k3xOp3gInSc4	jejich
vztah	vztah	k1gInSc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
Hašek	Hašek	k1gMnSc1	Hašek
pokusil	pokusit	k5eAaPmAgMnS	pokusit
vycouvat	vycouvat	k5eAaPmF	vycouvat
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
radikální	radikální	k2eAgFnSc2d1	radikální
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
trvalou	trvalý	k2eAgFnSc4d1	trvalá
práci	práce	k1gFnSc4	práce
jako	jako	k8xC	jako
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
redigoval	redigovat	k5eAaImAgInS	redigovat
Ženský	ženský	k2eAgInSc1d1	ženský
obzor	obzor	k1gInSc1	obzor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
měl	mít	k5eAaImAgInS	mít
šedesát	šedesát	k4xCc1	šedesát
čtyři	čtyři	k4xCgMnPc4	čtyři
publikovaných	publikovaný	k2eAgFnPc2d1	publikovaná
povídek	povídka	k1gFnPc2	povídka
a	a	k8xC	a
další	další	k2eAgInSc1d1	další
rok	rok	k1gInSc1	rok
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
redaktorem	redaktor	k1gMnSc7	redaktor
časopisu	časopis	k1gInSc2	časopis
Svět	svět	k1gInSc4	svět
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
práce	práce	k1gFnSc1	práce
netrvala	trvat	k5eNaImAgFnS	trvat
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
byl	být	k5eAaImAgInS	být
propuštěn	propustit	k5eAaPmNgInS	propustit
za	za	k7c4	za
publikování	publikování	k1gNnSc4	publikování
článků	článek	k1gInPc2	článek
o	o	k7c6	o
imaginárních	imaginární	k2eAgNnPc6d1	imaginární
zvířatech	zvíře	k1gNnPc6	zvíře
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc4	který
si	se	k3xPyFc3	se
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1910	[number]	k4	1910
se	se	k3xPyFc4	se
s	s	k7c7	s
Jarmilou	Jarmila	k1gFnSc7	Jarmila
Mayerovou	Mayerová	k1gFnSc7	Mayerová
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
manželství	manželství	k1gNnSc2	manželství
se	se	k3xPyFc4	se
ale	ale	k9	ale
Jarmila	Jarmila	k1gFnSc1	Jarmila
vrátila	vrátit	k5eAaPmAgFnS	vrátit
znovu	znovu	k6eAd1	znovu
žít	žít	k5eAaImF	žít
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k3yRnSc1	co
byl	být	k5eAaImAgMnS	být
Hašek	Hašek	k1gMnSc1	Hašek
zadržen	zadržet	k5eAaPmNgMnS	zadržet
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
předstírat	předstírat	k5eAaImF	předstírat
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
přispíval	přispívat	k5eAaImAgMnS	přispívat
do	do	k7c2	do
Českého	český	k2eAgNnSc2d1	české
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
do	do	k7c2	do
Pochodně	pochodeň	k1gFnSc2	pochodeň
<g/>
,	,	kIx,	,
Humoristických	humoristický	k2eAgInPc2d1	humoristický
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
Kopřiv	kopřiva	k1gFnPc2	kopřiva
<g/>
,	,	kIx,	,
Karikatur	karikatura	k1gFnPc2	karikatura
<g/>
,	,	kIx,	,
nějaký	nějaký	k3yIgInSc1	nějaký
čas	čas	k1gInSc1	čas
vedl	vést	k5eAaImAgInS	vést
též	též	k9	též
Kynologický	kynologický	k2eAgInSc1d1	kynologický
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ho	on	k3xPp3gNnSc4	on
později	pozdě	k6eAd2	pozdě
inspirovalo	inspirovat	k5eAaBmAgNnS	inspirovat
ke	k	k7c3	k
knize	kniha	k1gFnSc3	kniha
Můj	můj	k3xOp1gInSc1	můj
obchod	obchod	k1gInSc1	obchod
se	s	k7c7	s
psy	pes	k1gMnPc7	pes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
založil	založit	k5eAaPmAgMnS	založit
Stranu	strana	k1gFnSc4	strana
mírného	mírný	k2eAgInSc2d1	mírný
pokroku	pokrok	k1gInSc2	pokrok
v	v	k7c6	v
mezích	mez	k1gFnPc6	mez
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
politickou	politický	k2eAgFnSc4d1	politická
mystifikaci	mystifikace	k1gFnSc4	mystifikace
karikující	karikující	k2eAgInPc4d1	karikující
volební	volební	k2eAgInPc4d1	volební
poměry	poměr	k1gInPc4	poměr
<g/>
,	,	kIx,	,
a	a	k8xC	a
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
jako	jako	k9	jako
její	její	k3xOp3gMnSc1	její
kandidát	kandidát	k1gMnSc1	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
byl	být	k5eAaImAgInS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
F.	F.	kA	F.
Langrem	Langer	k1gMnSc7	Langer
<g/>
,	,	kIx,	,
E.	E.	kA	E.
A.	A.	kA	A.
Longenem	Longen	k1gInSc7	Longen
<g/>
,	,	kIx,	,
E.	E.	kA	E.
E.	E.	kA	E.
Kischem	Kisch	k1gMnSc7	Kisch
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
spoluautorem	spoluautor	k1gMnSc7	spoluautor
řady	řada	k1gFnSc2	řada
kabaretních	kabaretní	k2eAgNnPc2d1	kabaretní
vystoupení	vystoupení	k1gNnPc2	vystoupení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
i	i	k9	i
hlavním	hlavní	k2eAgMnSc7d1	hlavní
účinkujícím	účinkující	k1gMnPc3	účinkující
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
vypuknutí	vypuknutí	k1gNnSc2	vypuknutí
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
žil	žít	k5eAaImAgMnS	žít
Hašek	Hašek	k1gMnSc1	Hašek
periodicky	periodicky	k6eAd1	periodicky
s	s	k7c7	s
kreslířem	kreslíř	k1gMnSc7	kreslíř
Josefem	Josef	k1gMnSc7	Josef
Ladou	Lada	k1gFnSc7	Lada
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
později	pozdě	k6eAd2	pozdě
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
Dobrého	dobrý	k2eAgMnSc4d1	dobrý
vojáka	voják	k1gMnSc4	voják
Švejka	Švejk	k1gMnSc4	Švejk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
narukoval	narukovat	k5eAaPmAgMnS	narukovat
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
k	k	k7c3	k
91	[number]	k4	91
<g/>
.	.	kIx.	.
pluku	pluk	k1gInSc2	pluk
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
odjel	odjet	k5eAaPmAgMnS	odjet
na	na	k7c4	na
haličskou	haličský	k2eAgFnSc4d1	Haličská
frontu	fronta	k1gFnSc4	fronta
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
úmyslu	úmysl	k1gInSc6	úmysl
narukovat	narukovat	k5eAaPmF	narukovat
téměř	téměř	k6eAd1	téměř
nikomu	nikdo	k3yNnSc3	nikdo
neřekl	říct	k5eNaPmAgMnS	říct
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
proto	proto	k8xC	proto
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
hledaný	hledaný	k2eAgInSc1d1	hledaný
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
září	září	k1gNnSc2	září
1915	[number]	k4	1915
do	do	k7c2	do
léta	léto	k1gNnSc2	léto
1916	[number]	k4	1916
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
v	v	k7c6	v
táboře	tábor	k1gInSc6	tábor
Totskoje	Totskoj	k1gInSc2	Totskoj
a	a	k8xC	a
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
Československých	československý	k2eAgFnPc2d1	Československá
legií	legie	k1gFnPc2	legie
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
odveden	odvést	k5eAaPmNgInS	odvést
do	do	k7c2	do
čs	čs	kA	čs
<g/>
.	.	kIx.	.
pluku	pluk	k1gInSc2	pluk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
písař	písař	k1gMnSc1	písař
<g/>
,	,	kIx,	,
emisař	emisař	k1gMnSc1	emisař
náborové	náborový	k2eAgFnSc2d1	náborová
komise	komise	k1gFnSc2	komise
a	a	k8xC	a
střelec	střelec	k1gMnSc1	střelec
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
přeložen	přeložit	k5eAaPmNgInS	přeložit
ke	k	k7c3	k
spojovacímu	spojovací	k2eAgInSc3d1	spojovací
oddílu	oddíl	k1gInSc3	oddíl
<g/>
,	,	kIx,	,
kulometnému	kulometný	k2eAgInSc3d1	kulometný
oddílu	oddíl	k1gInSc3	oddíl
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Zborova	Zborov	k1gInSc2	Zborov
<g/>
)	)	kIx)	)
a	a	k8xC	a
do	do	k7c2	do
kanceláře	kancelář	k1gFnSc2	kancelář
1	[number]	k4	1
<g/>
.	.	kIx.	.
pluku	pluk	k1gInSc2	pluk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
do	do	k7c2	do
února	únor	k1gInSc2	únor
1918	[number]	k4	1918
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Čechoslovan	Čechoslovan	k?	Čechoslovan
a	a	k8xC	a
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
voják	voják	k1gMnSc1	voják
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
autorem	autor	k1gMnSc7	autor
řady	řada	k1gFnSc2	řada
protibolševických	protibolševický	k2eAgInPc2d1	protibolševický
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
února	únor	k1gInSc2	únor
1918	[number]	k4	1918
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
Českoslovanské	českoslovanský	k2eAgFnSc2d1	českoslovanská
sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
dělnické	dělnický	k2eAgFnSc2d1	Dělnická
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k9	co
vedlo	vést	k5eAaImAgNnS	vést
Haška	Hašek	k1gMnSc4	Hašek
k	k	k7c3	k
opuštění	opuštění	k1gNnSc3	opuštění
myšlenky	myšlenka	k1gFnSc2	myšlenka
anarchismu	anarchismus	k1gInSc2	anarchismus
a	a	k8xC	a
přijetí	přijetí	k1gNnSc4	přijetí
socialistických	socialistický	k2eAgFnPc2d1	socialistická
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
,	,	kIx,	,
nikde	nikde	k6eAd1	nikde
neobjasnil	objasnit	k5eNaPmAgMnS	objasnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
se	se	k3xPyFc4	se
Československé	československý	k2eAgFnSc2d1	Československá
legie	legie	k1gFnSc2	legie
vydaly	vydat	k5eAaPmAgFnP	vydat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
připojit	připojit	k5eAaPmF	připojit
se	se	k3xPyFc4	se
k	k	k7c3	k
Západní	západní	k2eAgFnSc3d1	západní
frontě	fronta	k1gFnSc3	fronta
přes	přes	k7c4	přes
Vladivostok	Vladivostok	k1gInSc4	Vladivostok
<g/>
,	,	kIx,	,
Hašek	Hašek	k1gMnSc1	Hašek
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
nesouhlasil	souhlasit	k5eNaImAgInS	souhlasit
a	a	k8xC	a
přijel	přijet	k5eAaPmAgInS	přijet
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začal	začít	k5eAaPmAgMnS	začít
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
bolševiky	bolševik	k1gMnPc7	bolševik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
z	z	k7c2	z
čs	čs	kA	čs
<g/>
.	.	kIx.	.
legií	legie	k1gFnPc2	legie
do	do	k7c2	do
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
přijel	přijet	k5eAaPmAgMnS	přijet
do	do	k7c2	do
Samary	Samara	k1gFnSc2	Samara
<g/>
,	,	kIx,	,
následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
byl	být	k5eAaImAgMnS	být
ředitelem	ředitel	k1gMnSc7	ředitel
armádní	armádní	k2eAgFnSc2d1	armádní
tiskárny	tiskárna	k1gFnSc2	tiskárna
v	v	k7c6	v
Ufě	Ufě	k1gFnSc6	Ufě
<g/>
,	,	kIx,	,
náčelníkem	náčelník	k1gInSc7	náčelník
oddělení	oddělení	k1gNnSc2	oddělení
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
cizinci	cizinec	k1gMnPc7	cizinec
aj.	aj.	kA	aj.
V	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
se	se	k3xPyFc4	se
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1920	[number]	k4	1920
v	v	k7c6	v
Krasnojarsku	Krasnojarsko	k1gNnSc6	Krasnojarsko
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
tiskárenskou	tiskárenský	k2eAgFnSc7d1	tiskárenská
dělnicí	dělnice	k1gFnSc7	dělnice
Alexandrou	Alexandra	k1gFnSc7	Alexandra
Grigorjevnou	Grigorjevný	k2eAgFnSc4d1	Grigorjevný
Lvovovou	Lvovová	k1gFnSc4	Lvovová
<g/>
,	,	kIx,	,
zvanou	zvaný	k2eAgFnSc4d1	zvaná
Šura	Šura	k1gMnSc1	Šura
(	(	kIx(	(
<g/>
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
nebyl	být	k5eNaImAgInS	být
souzen	soudit	k5eAaImNgInS	soudit
za	za	k7c4	za
mnohoženství	mnohoženství	k1gNnSc4	mnohoženství
jen	jen	k9	jen
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
nebyl	být	k5eNaImAgInS	být
zrovna	zrovna	k6eAd1	zrovna
pořádek	pořádek	k1gInSc4	pořádek
a	a	k8xC	a
neuznávaly	uznávat	k5eNaImAgInP	uznávat
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
různé	různý	k2eAgFnPc1d1	různá
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
smlouvy	smlouva	k1gFnPc1	smlouva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1920	[number]	k4	1920
přijel	přijet	k5eAaPmAgMnS	přijet
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
v	v	k7c6	v
karanténní	karanténní	k2eAgFnSc6d1	karanténní
stanici	stanice	k1gFnSc6	stanice
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
se	se	k3xPyFc4	se
Šurou	Šura	k1gFnSc7	Šura
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
bohémskému	bohémský	k2eAgInSc3d1	bohémský
způsobu	způsob	k1gInSc3	způsob
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
hostince	hostinec	k1gInSc2	hostinec
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
i	i	k8xC	i
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc2	jenž
psal	psát	k5eAaImAgMnS	psát
své	svůj	k3xOyFgFnSc2	svůj
povídky	povídka	k1gFnSc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
historek	historka	k1gFnPc2	historka
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
sepsal	sepsat	k5eAaPmAgMnS	sepsat
Haškův	Haškův	k2eAgMnSc1d1	Haškův
přítel	přítel	k1gMnSc1	přítel
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Matěj	Matěj	k1gMnSc1	Matěj
Kuděj	Kuděj	k1gMnSc1	Kuděj
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1921	[number]	k4	1921
odjel	odjet	k5eAaPmAgInS	odjet
s	s	k7c7	s
malířem	malíř	k1gMnSc7	malíř
J.	J.	kA	J.
Panuškou	panuška	k1gFnSc7	panuška
a	a	k8xC	a
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Šurou	Šurý	k2eAgFnSc7d1	Šurý
do	do	k7c2	do
Lipnice	Lipnice	k1gFnSc2	Lipnice
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgMnS	být
vážně	vážně	k6eAd1	vážně
nemocný	mocný	k2eNgMnSc1d1	nemocný
a	a	k8xC	a
nebezpečně	bezpečně	k6eNd1	bezpečně
obézní	obézní	k2eAgMnPc1d1	obézní
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
nepsal	psát	k5eNaImAgMnS	psát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
diktoval	diktovat	k5eAaImAgMnS	diktovat
kapitoly	kapitola	k1gFnSc2	kapitola
Švejka	Švejk	k1gMnSc2	Švejk
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
ložnici	ložnice	k1gFnSc6	ložnice
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1923	[number]	k4	1923
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ochrnutím	ochrnutí	k1gNnPc3	ochrnutí
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
fotografie	fotografie	k1gFnSc1	fotografie
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
prosince	prosinec	k1gInSc2	prosinec
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
a	a	k8xC	a
slovenské	slovenský	k2eAgFnSc3d1	slovenská
veřejnosti	veřejnost	k1gFnSc3	veřejnost
je	být	k5eAaImIp3nS	být
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hašek	Hašek	k1gMnSc1	Hašek
zafixován	zafixovat	k5eAaPmNgMnS	zafixovat
jako	jako	k9	jako
bohém	bohém	k1gMnSc1	bohém
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
dokonce	dokonce	k9	dokonce
jako	jako	k9	jako
prototyp	prototyp	k1gInSc1	prototyp
bohéma	bohém	k1gMnSc2	bohém
začátku	začátek	k1gInSc6	začátek
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jde	jít	k5eAaImIp3nS	jít
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
o	o	k7c4	o
legendu	legenda	k1gFnSc4	legenda
a	a	k8xC	a
možná	možná	k9	možná
dokonce	dokonce	k9	dokonce
o	o	k7c4	o
vlastní	vlastní	k2eAgFnSc4d1	vlastní
Haškovu	Haškův	k2eAgFnSc4d1	Haškova
sebestylizaci	sebestylizace	k1gFnSc4	sebestylizace
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
autor	autor	k1gMnSc1	autor
byl	být	k5eAaImAgMnS	být
Hašek	Hašek	k1gMnSc1	Hašek
velmi	velmi	k6eAd1	velmi
produktivní	produktivní	k2eAgMnSc1d1	produktivní
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přece	přece	k9	přece
jen	jen	k9	jen
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
přiměřenou	přiměřený	k2eAgFnSc4d1	přiměřená
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
disciplínu	disciplína	k1gFnSc4	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
je	být	k5eAaImIp3nS	být
také	také	k9	také
cítit	cítit	k5eAaImF	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgInS	mít
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
(	(	kIx(	(
<g/>
možná	možná	k9	možná
trochu	trochu	k6eAd1	trochu
nesystematické	systematický	k2eNgNnSc1d1	nesystematické
<g/>
)	)	kIx)	)
humanitní	humanitní	k2eAgNnSc1d1	humanitní
vzdělání	vzdělání	k1gNnSc1	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Nejzajímavější	zajímavý	k2eAgNnSc1d3	nejzajímavější
je	být	k5eAaImIp3nS	být
sledovat	sledovat	k5eAaImF	sledovat
Haškovo	Haškův	k2eAgNnSc4d1	Haškovo
působení	působení	k1gNnSc4	působení
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1916	[number]	k4	1916
<g/>
-	-	kIx~	-
<g/>
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgMnS	být
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
není	být	k5eNaImIp3nS	být
vnímán	vnímán	k2eAgMnSc1d1	vnímán
jako	jako	k8xS	jako
bohém	bohém	k1gMnSc1	bohém
a	a	k8xC	a
pouhý	pouhý	k2eAgMnSc1d1	pouhý
humoristický	humoristický	k2eAgMnSc1d1	humoristický
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
velmi	velmi	k6eAd1	velmi
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
bolševický	bolševický	k2eAgMnSc1d1	bolševický
armádní	armádní	k2eAgMnSc1d1	armádní
funkcionář	funkcionář	k1gMnSc1	funkcionář
a	a	k8xC	a
vážený	vážený	k2eAgMnSc1d1	vážený
intelektuál	intelektuál	k1gMnSc1	intelektuál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1916	[number]	k4	1916
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
v	v	k7c6	v
legionářských	legionářský	k2eAgInPc6d1	legionářský
časopisech	časopis	k1gInPc6	časopis
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
přeběhl	přeběhnout	k5eAaPmAgMnS	přeběhnout
k	k	k7c3	k
bolševikům	bolševik	k1gMnPc3	bolševik
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
se	se	k3xPyFc4	se
vyznamenal	vyznamenat	k5eAaPmAgMnS	vyznamenat
jako	jako	k9	jako
odvážný	odvážný	k2eAgMnSc1d1	odvážný
velitel	velitel	k1gMnSc1	velitel
oddílu	oddíl	k1gInSc2	oddíl
československých	československý	k2eAgMnPc2d1	československý
rudoarmějců	rudoarmějec	k1gMnPc2	rudoarmějec
při	při	k7c6	při
obraně	obrana	k1gFnSc6	obrana
Samary	Samara	k1gFnSc2	Samara
<g/>
.	.	kIx.	.
</s>
<s>
Samara	Samara	k1gFnSc1	Samara
byla	být	k5eAaImAgFnS	být
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
obsazována	obsazován	k2eAgFnSc1d1	obsazována
od	od	k7c2	od
směru	směr	k1gInSc2	směr
z	z	k7c2	z
Lipjagu	Lipjag	k1gInSc2	Lipjag
Československými	československý	k2eAgFnPc7d1	Československá
legiemi	legie	k1gFnPc7	legie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
bojovaly	bojovat	k5eAaImAgFnP	bojovat
po	po	k7c6	po
boku	bok	k1gInSc6	bok
bílých	bílý	k2eAgNnPc2d1	bílé
vojsk	vojsko	k1gNnPc2	vojsko
za	za	k7c4	za
obnovení	obnovení	k1gNnSc4	obnovení
carského	carský	k2eAgInSc2d1	carský
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Legionáři	legionář	k1gMnPc1	legionář
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
zachovávat	zachovávat	k5eAaImF	zachovávat
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
neutralitu	neutralita	k1gFnSc4	neutralita
a	a	k8xC	a
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
s	s	k7c7	s
bolševiky	bolševik	k1gMnPc7	bolševik
se	se	k3xPyFc4	se
pouštěli	pouštět	k5eAaImAgMnP	pouštět
jen	jen	k9	jen
v	v	k7c6	v
nevyhnutelných	vyhnutelný	k2eNgInPc6d1	nevyhnutelný
případech	případ	k1gInPc6	případ
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1918	[number]	k4	1918
byla	být	k5eAaImAgFnS	být
Samara	Samara	k1gFnSc1	Samara
legiemi	legie	k1gFnPc7	legie
dobyta	dobýt	k5eAaPmNgFnS	dobýt
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hašek	Hašek	k1gMnSc1	Hašek
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
"	"	kIx"	"
<g/>
bratry	bratr	k1gMnPc7	bratr
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
mohl	moct	k5eAaImAgMnS	moct
pobízet	pobízet	k5eAaImF	pobízet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
opustili	opustit	k5eAaPmAgMnP	opustit
demokratizační	demokratizační	k2eAgFnSc4d1	demokratizační
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgInPc4	který
bojovaly	bojovat	k5eAaImAgInP	bojovat
čs	čs	kA	čs
<g/>
.	.	kIx.	.
legie	legie	k1gFnSc2	legie
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
připojili	připojit	k5eAaPmAgMnP	připojit
se	se	k3xPyFc4	se
k	k	k7c3	k
bolševikům	bolševik	k1gMnPc3	bolševik
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Samary	Samara	k1gFnSc2	Samara
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
skrýval	skrývat	k5eAaImAgInS	skrývat
na	na	k7c6	na
území	území	k1gNnSc6	území
kontrolovaném	kontrolovaný	k2eAgNnSc6d1	kontrolované
bílými	bílý	k2eAgNnPc7d1	bílé
vojsky	vojsko	k1gNnPc7	vojsko
(	(	kIx(	(
<g/>
kontrolovaná	kontrolovaný	k2eAgNnPc1d1	kontrolované
také	také	k6eAd1	také
čs	čs	kA	čs
<g/>
.	.	kIx.	.
legiemi	legie	k1gFnPc7	legie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
velitel	velitel	k1gMnSc1	velitel
oddílu	oddíl	k1gInSc2	oddíl
Čuvašů	Čuvaš	k1gMnPc2	Čuvaš
v	v	k7c6	v
Rudé	rudý	k2eAgFnSc6d1	rudá
armádě	armáda	k1gFnSc6	armáda
a	a	k8xC	a
jako	jako	k9	jako
zástupce	zástupce	k1gMnSc1	zástupce
vojenského	vojenský	k2eAgMnSc2d1	vojenský
velitele	velitel	k1gMnSc2	velitel
bugulmského	bugulmský	k2eAgInSc2d1	bugulmský
okresu	okres	k1gInSc2	okres
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
všeho	všecek	k3xTgNnSc2	všecek
se	se	k3xPyFc4	se
choval	chovat	k5eAaImAgMnS	chovat
dosti	dosti	k6eAd1	dosti
brutálně	brutálně	k6eAd1	brutálně
a	a	k8xC	a
nařídil	nařídit	k5eAaPmAgMnS	nařídit
značný	značný	k2eAgInSc4d1	značný
počet	počet	k1gInSc4	počet
poprav	poprava	k1gFnPc2	poprava
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
zastřelit	zastřelit	k5eAaPmF	zastřelit
např.	např.	kA	např.
právníka	právník	k1gMnSc4	právník
Nikolaje	Nikolaj	k1gMnSc4	Nikolaj
Skalona	Skalon	k1gMnSc4	Skalon
<g/>
,	,	kIx,	,
otce	otec	k1gMnSc4	otec
známého	známý	k2eAgMnSc4d1	známý
sovětského	sovětský	k2eAgMnSc4d1	sovětský
biologa	biolog	k1gMnSc4	biolog
Vasilije	Vasilije	k1gMnSc4	Vasilije
Skalona	Skalon	k1gMnSc4	Skalon
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jako	jako	k8xS	jako
komisař	komisař	k1gMnSc1	komisař
sloužil	sloužit	k5eAaImAgMnS	sloužit
v	v	k7c6	v
politickém	politický	k2eAgNnSc6d1	politické
oddělení	oddělení	k1gNnSc6	oddělení
Tuchačevského	Tuchačevský	k2eAgNnSc2d1	Tuchačevský
5	[number]	k4	5
<g/>
.	.	kIx.	.
armády	armáda	k1gFnSc2	armáda
Východního	východní	k2eAgInSc2d1	východní
frontu	front	k1gInSc2	front
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
potom	potom	k6eAd1	potom
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vydával	vydávat	k5eAaImAgInS	vydávat
několik	několik	k4yIc4	několik
časopisů	časopis	k1gInPc2	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byl	být	k5eAaImAgInS	být
také	také	k9	také
první	první	k4xOgInSc1	první
časopis	časopis	k1gInSc1	časopis
v	v	k7c6	v
burjatštině	burjatština	k1gFnSc6	burjatština
"	"	kIx"	"
<g/>
Jur	jura	k1gFnPc2	jura
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
úsvit	úsvit	k1gInSc1	úsvit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Irkutsku	Irkutsk	k1gInSc6	Irkutsk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xS	jako
člen	člen	k1gMnSc1	člen
městského	městský	k2eAgInSc2d1	městský
sovětu	sovět	k1gInSc2	sovět
<g/>
,	,	kIx,	,
zraněn	zraněn	k2eAgMnSc1d1	zraněn
při	při	k7c6	při
vražedném	vražedný	k2eAgInSc6d1	vražedný
útoku	útok	k1gInSc6	útok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
podnikl	podniknout	k5eAaPmAgMnS	podniknout
v	v	k7c6	v
sovětských	sovětský	k2eAgFnPc6d1	sovětská
službách	služba	k1gFnPc6	služba
záhadnou	záhadný	k2eAgFnSc4d1	záhadná
misi	mise	k1gFnSc4	mise
do	do	k7c2	do
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
specifických	specifický	k2eAgFnPc6d1	specifická
revolučních	revoluční	k2eAgFnPc6d1	revoluční
ruských	ruský	k2eAgFnPc6d1	ruská
podmínkách	podmínka	k1gFnPc6	podmínka
dostal	dostat	k5eAaPmAgMnS	dostat
Hašek	Hašek	k1gMnSc1	Hašek
možnost	možnost	k1gFnSc4	možnost
uplatnit	uplatnit	k5eAaPmF	uplatnit
ty	ten	k3xDgFnPc4	ten
stránky	stránka	k1gFnPc4	stránka
svého	svůj	k3xOyFgInSc2	svůj
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nemohly	moct	k5eNaImAgFnP	moct
projevit	projevit	k5eAaPmF	projevit
ve	v	k7c6	v
stabilizovaných	stabilizovaný	k2eAgInPc6d1	stabilizovaný
a	a	k8xC	a
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
maloměstských	maloměstský	k2eAgInPc6d1	maloměstský
českých	český	k2eAgInPc6d1	český
poměrech	poměr	k1gInPc6	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
také	také	k9	také
určitě	určitě	k6eAd1	určitě
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hašek	Hašek	k1gMnSc1	Hašek
dostal	dostat	k5eAaPmAgMnS	dostat
jako	jako	k9	jako
stranický	stranický	k2eAgInSc1d1	stranický
úkol	úkol	k1gInSc1	úkol
zákaz	zákaz	k1gInSc1	zákaz
pití	pití	k1gNnSc2	pití
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
vyslán	vyslat	k5eAaPmNgMnS	vyslat
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
organizovat	organizovat	k5eAaBmF	organizovat
komunistické	komunistický	k2eAgNnSc4d1	komunistické
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
také	také	k9	také
podporuje	podporovat	k5eAaImIp3nS	podporovat
tezi	teze	k1gFnSc4	teze
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
sovětském	sovětský	k2eAgNnSc6d1	sovětské
Rusku	Rusko	k1gNnSc6	Rusko
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
vnímán	vnímat	k5eAaImNgInS	vnímat
jako	jako	k8xS	jako
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
schopný	schopný	k2eAgMnSc1d1	schopný
organizátor	organizátor	k1gMnSc1	organizátor
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
návratu	návrat	k1gInSc2	návrat
k	k	k7c3	k
alkoholu	alkohol	k1gInSc2	alkohol
a	a	k8xC	a
nezodpovědnosti	nezodpovědnost	k1gFnSc2	nezodpovědnost
mu	on	k3xPp3gMnSc3	on
ve	v	k7c6	v
splnění	splnění	k1gNnSc6	splnění
úkolu	úkol	k1gInSc2	úkol
zabránily	zabránit	k5eAaPmAgFnP	zabránit
i	i	k9	i
dvě	dva	k4xCgFnPc1	dva
okolnosti	okolnost	k1gFnPc1	okolnost
<g/>
:	:	kIx,	:
jednak	jednak	k8xC	jednak
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
kladenských	kladenský	k2eAgMnPc2d1	kladenský
nepokojů	nepokoj	k1gInPc2	nepokoj
dostal	dostat	k5eAaPmAgInS	dostat
od	od	k7c2	od
ruských	ruský	k2eAgInPc2d1	ruský
úřadů	úřad	k1gInPc2	úřad
částku	částka	k1gFnSc4	částka
1	[number]	k4	1
500	[number]	k4	500
marek	marka	k1gFnPc2	marka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
znehodnocena	znehodnotit	k5eAaPmNgFnS	znehodnotit
německou	německý	k2eAgFnSc7d1	německá
inflací	inflace	k1gFnSc7	inflace
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
Haškovým	Haškův	k2eAgInSc7d1	Haškův
příjezdem	příjezd	k1gInSc7	příjezd
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1920	[number]	k4	1920
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
zatčen	zatčen	k2eAgMnSc1d1	zatčen
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Handlíř	handlíř	k1gMnSc1	handlíř
<g/>
,	,	kIx,	,
spojka	spojka	k1gFnSc1	spojka
ruských	ruský	k2eAgMnPc2d1	ruský
agentů	agent	k1gMnPc2	agent
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
Hašek	Hašek	k1gMnSc1	Hašek
kontaktovat	kontaktovat	k5eAaImF	kontaktovat
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
psal	psát	k5eAaImAgInS	psát
především	především	k6eAd1	především
cestopisné	cestopisný	k2eAgFnPc4d1	cestopisná
povídky	povídka	k1gFnPc4	povídka
<g/>
,	,	kIx,	,
črty	črt	k1gInPc4	črt
a	a	k8xC	a
humoresky	humoreska	k1gFnPc4	humoreska
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
publikoval	publikovat	k5eAaBmAgMnS	publikovat
časopisecky	časopisecky	k6eAd1	časopisecky
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
děl	dělo	k1gNnPc2	dělo
napsal	napsat	k5eAaPmAgMnS	napsat
v	v	k7c6	v
pražských	pražský	k2eAgInPc6d1	pražský
hospodách	hospodách	k?	hospodách
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
jeho	jeho	k3xOp3gFnPc2	jeho
próz	próza	k1gFnPc2	próza
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gInPc1	jeho
skutečné	skutečný	k2eAgInPc1d1	skutečný
zážitky	zážitek	k1gInPc1	zážitek
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vnáší	vnášet	k5eAaImIp3nS	vnášet
do	do	k7c2	do
znalostí	znalost	k1gFnPc2	znalost
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
zmatek	zmatek	k1gInSc4	zmatek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
není	být	k5eNaImIp3nS	být
vždy	vždy	k6eAd1	vždy
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
je	být	k5eAaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
a	a	k8xC	a
co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
básnická	básnický	k2eAgFnSc1d1	básnická
nadsázka	nadsázka	k1gFnSc1	nadsázka
<g/>
.	.	kIx.	.
</s>
<s>
Hašek	Hašek	k1gMnSc1	Hašek
nenáviděl	návidět	k5eNaImAgMnS	návidět
přetvářku	přetvářka	k1gFnSc4	přetvářka
<g/>
,	,	kIx,	,
sentimentalitu	sentimentalita	k1gFnSc4	sentimentalita
<g/>
,	,	kIx,	,
usedlost	usedlost	k1gFnSc4	usedlost
<g/>
,	,	kIx,	,
ironicky	ironicky	k6eAd1	ironicky
reagoval	reagovat	k5eAaBmAgMnS	reagovat
na	na	k7c4	na
sociální	sociální	k2eAgInPc4d1	sociální
verše	verš	k1gInPc4	verš
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
typickým	typický	k2eAgInSc7d1	typický
znakem	znak	k1gInSc7	znak
jeho	jeho	k3xOp3gFnSc2	jeho
tvorby	tvorba	k1gFnSc2	tvorba
je	být	k5eAaImIp3nS	být
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
mravním	mravní	k2eAgFnPc3d1	mravní
a	a	k8xC	a
literárním	literární	k2eAgFnPc3d1	literární
konvencím	konvence	k1gFnPc3	konvence
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
je	být	k5eAaImIp3nS	být
roztroušena	roztroušen	k2eAgFnSc1d1	roztroušena
po	po	k7c6	po
různých	různý	k2eAgInPc6d1	různý
časopisech	časopis	k1gInPc6	časopis
a	a	k8xC	a
novinách	novina	k1gFnPc6	novina
<g/>
,	,	kIx,	,
jen	jen	k6eAd1	jen
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
do	do	k7c2	do
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
napsal	napsat	k5eAaPmAgMnS	napsat
asi	asi	k9	asi
1	[number]	k4	1
200	[number]	k4	200
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
textů	text	k1gInPc2	text
se	se	k3xPyFc4	se
ztratila	ztratit	k5eAaPmAgFnS	ztratit
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
román	román	k1gInSc1	román
Historie	historie	k1gFnSc1	historie
vola	vůl	k1gMnSc2	vůl
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
též	též	k9	též
řada	řada	k1gFnSc1	řada
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
existuje	existovat	k5eAaImIp3nS	existovat
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
Haškova	Haškův	k2eAgNnSc2d1	Haškovo
autorství	autorství	k1gNnSc2	autorství
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
však	však	k9	však
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Tvořil	tvořit	k5eAaImAgMnS	tvořit
velmi	velmi	k6eAd1	velmi
lehce	lehko	k6eAd1	lehko
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
mistrem	mistr	k1gMnSc7	mistr
mystifikace	mystifikace	k1gFnSc2	mystifikace
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholným	vrcholný	k2eAgInSc7d1	vrcholný
mystifikačním	mystifikační	k2eAgInSc7d1	mystifikační
a	a	k8xC	a
parodickým	parodický	k2eAgInSc7d1	parodický
kouskem	kousek	k1gInSc7	kousek
bylo	být	k5eAaImAgNnS	být
založení	založení	k1gNnSc1	založení
Strany	strana	k1gFnSc2	strana
mírného	mírný	k2eAgInSc2d1	mírný
pokroku	pokrok	k1gInSc2	pokrok
v	v	k7c6	v
mezích	mez	k1gFnPc6	mez
zákona	zákon	k1gInSc2	zákon
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
-	-	kIx~	-
tuto	tento	k3xDgFnSc4	tento
stranu	strana	k1gFnSc4	strana
Hašek	Hašek	k1gMnSc1	Hašek
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
kumpány	kumpány	k?	kumpány
založil	založit	k5eAaPmAgInS	založit
v	v	k7c6	v
hospodě	hospodě	k?	hospodě
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
parodovali	parodovat	k5eAaImAgMnP	parodovat
tehdejší	tehdejší	k2eAgInSc4d1	tehdejší
politický	politický	k2eAgInSc4d1	politický
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
ji	on	k3xPp3gFnSc4	on
ale	ale	k9	ale
opravdu	opravdu	k6eAd1	opravdu
zaregistrovali	zaregistrovat	k5eAaPmAgMnP	zaregistrovat
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
Hašek	Hašek	k1gMnSc1	Hašek
v	v	k7c6	v
Praze-Vinohradech	Praze-Vinohrad	k1gInPc6	Praze-Vinohrad
kandidoval	kandidovat	k5eAaImAgInS	kandidovat
na	na	k7c4	na
starostu	starosta	k1gMnSc4	starosta
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
strany	strana	k1gFnSc2	strana
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
jako	jako	k9	jako
samostatný	samostatný	k2eAgInSc4d1	samostatný
text	text	k1gInSc4	text
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
zdaleka	zdaleka	k6eAd1	zdaleka
nejslavnější	slavný	k2eAgInSc1d3	nejslavnější
text	text	k1gInSc1	text
<g/>
,	,	kIx,	,
čtyřdílný	čtyřdílný	k2eAgInSc1d1	čtyřdílný
humoristický	humoristický	k2eAgInSc1d1	humoristický
román	román	k1gInSc1	román
Osudy	osud	k1gInPc1	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc4	Švejk
za	za	k7c2	za
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
přeložen	přeložit	k5eAaPmNgInS	přeložit
do	do	k7c2	do
58	[number]	k4	58
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
několikrát	několikrát	k6eAd1	několikrát
zfilmován	zfilmován	k2eAgMnSc1d1	zfilmován
i	i	k8xC	i
zdramatizován	zdramatizován	k2eAgMnSc1d1	zdramatizován
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
díly	díl	k1gInPc1	díl
románu	román	k1gInSc2	román
mají	mít	k5eAaImIp3nP	mít
názvy	název	k1gInPc1	název
<g/>
:	:	kIx,	:
V	v	k7c6	v
zázemí	zázemí	k1gNnSc6	zázemí
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Na	na	k7c6	na
frontě	fronta	k1gFnSc6	fronta
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Slavný	slavný	k2eAgInSc1d1	slavný
výprask	výprask	k1gInSc1	výprask
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
a	a	k8xC	a
nedokončené	dokončený	k2eNgNnSc1d1	nedokončené
Pokračování	pokračování	k1gNnSc1	pokračování
slavného	slavný	k2eAgInSc2d1	slavný
výprasku	výprask	k1gInSc2	výprask
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejvýznamnější	významný	k2eAgNnSc4d3	nejvýznamnější
Haškovo	Haškův	k2eAgNnSc4d1	Haškovo
dílo	dílo	k1gNnSc4	dílo
mnoha	mnoho	k4c2	mnoho
lidmi	člověk	k1gMnPc7	člověk
spojované	spojovaný	k2eAgInPc1d1	spojovaný
s	s	k7c7	s
kongeniálními	kongeniální	k2eAgFnPc7d1	kongeniální
ilustracemi	ilustrace	k1gFnPc7	ilustrace
Josefa	Josef	k1gMnSc2	Josef
Lady	lady	k1gFnSc6	lady
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
dílo	dílo	k1gNnSc4	dílo
nestihl	stihnout	k5eNaPmAgMnS	stihnout
dokončit	dokončit	k5eAaPmF	dokončit
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
dokončil	dokončit	k5eAaPmAgMnS	dokončit
Karel	Karel	k1gMnSc1	Karel
Vaněk	Vaněk	k1gMnSc1	Vaněk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
vzdálil	vzdálit	k5eAaPmAgMnS	vzdálit
původní	původní	k2eAgFnSc3d1	původní
myšlence	myšlenka	k1gFnSc3	myšlenka
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
vycházet	vycházet	k5eAaImF	vycházet
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
kritizován	kritizován	k2eAgMnSc1d1	kritizován
(	(	kIx(	(
<g/>
Viktor	Viktor	k1gMnSc1	Viktor
Dyk	Dyk	k?	Dyk
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Durych	Durych	k1gMnSc1	Durych
<g/>
,	,	kIx,	,
F.	F.	kA	F.
X.	X.	kA	X.
Šalda	Šalda	k1gMnSc1	Šalda
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příznivců	příznivec	k1gMnPc2	příznivec
měl	mít	k5eAaImAgMnS	mít
román	román	k1gInSc4	román
zpočátku	zpočátku	k6eAd1	zpočátku
málo	málo	k6eAd1	málo
(	(	kIx(	(
<g/>
jako	jako	k9	jako
první	první	k4xOgFnSc7	první
ho	on	k3xPp3gNnSc4	on
za	za	k7c4	za
velké	velký	k2eAgNnSc4d1	velké
dílo	dílo	k1gNnSc4	dílo
označil	označit	k5eAaPmAgMnS	označit
zřejmě	zřejmě	k6eAd1	zřejmě
Ivan	Ivan	k1gMnSc1	Ivan
Olbracht	Olbracht	k1gMnSc1	Olbracht
v	v	k7c6	v
kulturní	kulturní	k2eAgFnSc6d1	kulturní
rubrice	rubrika	k1gFnSc6	rubrika
Rudého	rudý	k2eAgNnSc2d1	Rudé
Práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
zaujali	zaujmout	k5eAaPmAgMnP	zaujmout
i	i	k9	i
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
.	.	kIx.	.
</s>
<s>
Diskuse	diskuse	k1gFnSc1	diskuse
o	o	k7c6	o
hodnotě	hodnota	k1gFnSc6	hodnota
díla	dílo	k1gNnSc2	dílo
se	se	k3xPyFc4	se
vedly	vést	k5eAaImAgFnP	vést
i	i	k9	i
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
Švejkovi	Švejk	k1gMnSc3	Švejk
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
např.	např.	kA	např.
Václav	Václav	k1gMnSc1	Václav
Černý	Černý	k1gMnSc1	Černý
<g/>
,	,	kIx,	,
přihlásila	přihlásit	k5eAaPmAgFnS	přihlásit
se	se	k3xPyFc4	se
však	však	k9	však
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
celá	celý	k2eAgFnSc1d1	celá
plejáda	plejáda	k1gFnSc1	plejáda
českých	český	k2eAgMnPc2d1	český
literárních	literární	k2eAgMnPc2d1	literární
teoretiků	teoretik	k1gMnPc2	teoretik
<g/>
,	,	kIx,	,
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
či	či	k8xC	či
intelektuálů	intelektuál	k1gMnPc2	intelektuál
<g/>
.	.	kIx.	.
</s>
<s>
Švejk	Švejk	k1gMnSc1	Švejk
byl	být	k5eAaImAgMnS	být
několikrát	několikrát	k6eAd1	několikrát
dramatizován	dramatizován	k2eAgMnSc1d1	dramatizován
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
Hašek	Hašek	k1gMnSc1	Hašek
provedl	provést	k5eAaPmAgMnS	provést
první	první	k4xOgFnSc4	první
dramatizaci	dramatizace	k1gFnSc4	dramatizace
pro	pro	k7c4	pro
Revoluční	revoluční	k2eAgFnSc4d1	revoluční
scénu	scéna	k1gFnSc4	scéna
Emila	Emil	k1gMnSc2	Emil
Artura	Artur	k1gMnSc2	Artur
Longena	Longen	k1gMnSc2	Longen
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
ze	z	k7c2	z
Švejka	Švejk	k1gMnSc2	Švejk
divadelní	divadelní	k2eAgNnSc1d1	divadelní
představení	představení	k1gNnSc1	představení
Haškův	Haškův	k2eAgMnSc1d1	Haškův
přítel	přítel	k1gMnSc1	přítel
Max	Max	k1gMnSc1	Max
Brod	Brod	k1gInSc4	Brod
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
Pavel	Pavel	k1gMnSc1	Pavel
Kohout	Kohout	k1gMnSc1	Kohout
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
proslulosti	proslulost	k1gFnSc2	proslulost
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
divadelní	divadelní	k2eAgFnSc1d1	divadelní
adaptace	adaptace	k1gFnSc1	adaptace
Švejk	Švejk	k1gMnSc1	Švejk
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
německého	německý	k2eAgMnSc2d1	německý
dramatika	dramatik	k1gMnSc2	dramatik
a	a	k8xC	a
režiséra	režisér	k1gMnSc2	režisér
Bertolda	Bertold	k1gMnSc2	Bertold
Brechta	Brecht	k1gMnSc2	Brecht
<g/>
.	.	kIx.	.
</s>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Gan	Gan	k1gMnSc1	Gan
napsal	napsat	k5eAaBmAgMnS	napsat
román-koláž	románoláž	k1gFnSc4	román-koláž
Osudy	osud	k1gInPc7	osud
humoristy	humorista	k1gMnSc2	humorista
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Haška	Hašek	k1gMnSc2	Hašek
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
carů	car	k1gMnPc2	car
a	a	k8xC	a
komisařů	komisař	k1gMnPc2	komisař
i	i	k8xC	i
doma	doma	k6eAd1	doma
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
základě	základ	k1gInSc6	základ
korespondence	korespondence	k1gFnSc2	korespondence
a	a	k8xC	a
dokumentů	dokument	k1gInPc2	dokument
<g/>
,	,	kIx,	,
skutečných	skutečný	k2eAgMnPc2d1	skutečný
i	i	k8xC	i
fabulovaných	fabulovaný	k2eAgMnPc2d1	fabulovaný
<g/>
,	,	kIx,	,
z	z	k7c2	z
motivů	motiv	k1gInPc2	motiv
některých	některý	k3yIgFnPc2	některý
Haškových	Hašková	k1gFnPc2	Hašková
povídek	povídka	k1gFnPc2	povídka
a	a	k8xC	a
historek	historka	k1gFnPc2	historka
z	z	k7c2	z
Osudů	osud	k1gInPc2	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc2	Švejk
<g/>
,	,	kIx,	,
z	z	k7c2	z
dobových	dobový	k2eAgFnPc2d1	dobová
fotografií	fotografia	k1gFnPc2	fotografia
a	a	k8xC	a
kreseb	kresba	k1gFnPc2	kresba
Josefa	Josef	k1gMnSc2	Josef
Lady	Lada	k1gFnSc2	Lada
(	(	kIx(	(
<g/>
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
česky	česky	k6eAd1	česky
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Aleny	Alena	k1gFnSc2	Alena
Bláhové	Bláhová	k1gFnSc2	Bláhová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
filmové	filmový	k2eAgNnSc1d1	filmové
zpracování	zpracování	k1gNnSc1	zpracování
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
v	v	k7c6	v
němé	němý	k2eAgFnSc6d1	němá
éře	éra	k1gFnSc6	éra
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Švejka	Švejk	k1gMnSc4	Švejk
natočil	natočit	k5eAaBmAgMnS	natočit
Karel	Karel	k1gMnSc1	Karel
Lamač	lamač	k1gMnSc1	lamač
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Trnka	Trnka	k1gMnSc1	Trnka
natočil	natočit	k5eAaBmAgMnS	natočit
loutkový	loutkový	k2eAgInSc4d1	loutkový
film	film	k1gInSc4	film
o	o	k7c6	o
Švejkovi	Švejk	k1gMnSc6	Švejk
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
pak	pak	k6eAd1	pak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nejslavnější	slavný	k2eAgFnSc1d3	nejslavnější
<g/>
,	,	kIx,	,
dvoudílná	dvoudílný	k2eAgFnSc1d1	dvoudílná
filmová	filmový	k2eAgFnSc1d1	filmová
adaptace	adaptace	k1gFnSc1	adaptace
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
voják	voják	k1gMnSc1	voják
Švejk	Švejk	k1gMnSc1	Švejk
Karla	Karel	k1gMnSc2	Karel
Steklého	Steklý	k1gMnSc2	Steklý
s	s	k7c7	s
Rudolfem	Rudolf	k1gMnSc7	Rudolf
Hrušínským	Hrušínský	k2eAgMnSc7d1	Hrušínský
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Švejk	Švejk	k1gMnSc1	Švejk
lákal	lákat	k5eAaImAgMnS	lákat
i	i	k8xC	i
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
filmaře	filmař	k1gMnPc4	filmař
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
například	například	k6eAd1	například
Axel	Axel	k1gInSc1	Axel
von	von	k1gInSc1	von
Ambesser	Ambesser	k1gInSc1	Ambesser
natočil	natočit	k5eAaBmAgInS	natočit
v	v	k7c4	v
tehdejší	tehdejší	k2eAgInSc4d1	tehdejší
NSR	NSR	kA	NSR
snímek	snímek	k1gInSc4	snímek
Der	drát	k5eAaImRp2nS	drát
Brave	brav	k1gInSc5	brav
Soldat	Soldat	k1gMnPc4	Soldat
Schwejk	Schwejk	k1gInSc4	Schwejk
<g/>
,	,	kIx,	,
v	v	k7c6	v
polském	polský	k2eAgInSc6d1	polský
televizním	televizní	k2eAgInSc6d1	televizní
filmu	film	k1gInSc6	film
Przygody	Przygoda	k1gFnSc2	Przygoda
dobrego	dobrego	k1gNnSc1	dobrego
wojaka	wojak	k1gMnSc2	wojak
Szwejka	Szwejka	k1gFnSc1	Szwejka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
ztvárnil	ztvárnit	k5eAaPmAgInS	ztvárnit
Švejka	Švejk	k1gMnSc4	Švejk
populární	populární	k2eAgFnSc2d1	populární
Jerzy	Jerza	k1gFnSc2	Jerza
Stuhr	Stuhra	k1gFnPc2	Stuhra
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
britsko-ukrajinský	britskokrajinský	k2eAgInSc1d1	britsko-ukrajinský
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
voják	voják	k1gMnSc1	voják
Švejk	Švejk	k1gMnSc1	Švejk
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Robert	Roberta	k1gFnPc2	Roberta
Crombie	Crombie	k1gFnSc2	Crombie
a	a	k8xC	a
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
znění	znění	k1gNnSc6	znění
Ladislav	Ladislav	k1gMnSc1	Ladislav
Potměšil	potměšil	k1gMnSc1	potměšil
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
Švejka	Švejk	k1gMnSc2	Švejk
byly	být	k5eAaImAgFnP	být
zfilmovány	zfilmován	k2eAgFnPc1d1	zfilmována
i	i	k9	i
některé	některý	k3yIgFnPc1	některý
jeho	jeho	k3xOp3gFnPc1	jeho
povídky	povídka	k1gFnPc1	povídka
a	a	k8xC	a
humoresky	humoreska	k1gFnPc1	humoreska
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
natočil	natočit	k5eAaBmAgMnS	natočit
Miroslav	Miroslav	k1gMnSc1	Miroslav
Hubáček	Hubáček	k1gMnSc1	Hubáček
Haškovy	Haškův	k2eAgFnSc2d1	Haškova
povídky	povídka	k1gFnSc2	povídka
ze	z	k7c2	z
starého	starý	k2eAgNnSc2d1	staré
mocnářství	mocnářství	k1gNnSc2	mocnářství
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
Oldřich	Oldřich	k1gMnSc1	Oldřich
Lipský	lipský	k2eAgInSc4d1	lipský
film	film	k1gInSc4	film
Vzorný	vzorný	k2eAgInSc1d1	vzorný
kinematograf	kinematograf	k1gInSc1	kinematograf
Haška	Hašek	k1gMnSc2	Hašek
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
známý	známý	k2eAgInSc1d1	známý
je	být	k5eAaImIp3nS	být
snímek	snímek	k1gInSc1	snímek
Dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
s	s	k7c7	s
nahým	nahý	k2eAgMnSc7d1	nahý
klukem	kluk	k1gMnSc7	kluk
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Československou	československý	k2eAgFnSc4d1	Československá
televizi	televize	k1gFnSc4	televize
byly	být	k5eAaImAgFnP	být
bezpočetné	bezpočetný	k2eAgFnPc1d1	bezpočetný
Haškovy	Haškův	k2eAgFnPc1d1	Haškova
drobné	drobný	k2eAgFnPc1d1	drobná
prózy	próza	k1gFnPc1	próza
vděčným	vděčný	k2eAgInSc7d1	vděčný
inspiračním	inspirační	k2eAgInSc7d1	inspirační
zdrojem	zdroj	k1gInSc7	zdroj
pro	pro	k7c4	pro
televizní	televizní	k2eAgFnPc4d1	televizní
inscenace	inscenace	k1gFnPc4	inscenace
<g/>
:	:	kIx,	:
Zvony	zvon	k1gInPc4	zvon
pana	pan	k1gMnSc2	pan
Mlácena	mlácen	k2eAgFnSc1d1	mlácena
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Konec	konec	k1gInSc4	konec
agenta	agent	k1gMnSc2	agent
č.	č.	k?	č.
312	[number]	k4	312
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tři	tři	k4xCgMnPc1	tři
muži	muž	k1gMnPc1	muž
se	se	k3xPyFc4	se
žralokem	žralok	k1gMnSc7	žralok
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Odměna	odměna	k1gFnSc1	odměna
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Historky	historka	k1gFnPc1	historka
z	z	k7c2	z
Ražické	Ražický	k2eAgFnSc2d1	Ražický
bašty	bašta	k1gFnSc2	bašta
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Uličnictví	uličnictví	k1gNnSc4	uličnictví
pana	pan	k1gMnSc2	pan
Čabouna	Čaboun	k1gMnSc2	Čaboun
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ze	z	k7c2	z
staré	starý	k2eAgFnSc2d1	stará
drogerie	drogerie	k1gFnSc2	drogerie
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Šťastný	šťastný	k2eAgInSc1d1	šťastný
domov	domov	k1gInSc1	domov
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
či	či	k8xC	či
Můj	můj	k3xOp1gInSc1	můj
obchod	obchod	k1gInSc1	obchod
se	s	k7c7	s
psy	pes	k1gMnPc7	pes
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Haška	Hašek	k1gMnSc4	Hašek
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
Oldřich	Oldřich	k1gMnSc1	Oldřich
Kaiser	Kaiser	k1gMnSc1	Kaiser
<g/>
.	.	kIx.	.
</s>
<s>
Českou	český	k2eAgFnSc7d1	Česká
televizí	televize	k1gFnSc7	televize
byla	být	k5eAaImAgFnS	být
natočena	natočen	k2eAgFnSc1d1	natočena
inscenace	inscenace	k1gFnSc1	inscenace
Stavovské	stavovský	k2eAgInPc1d1	stavovský
rozdíly	rozdíl	k1gInPc7	rozdíl
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
je	být	k5eAaImIp3nS	být
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hašek	Hašek	k1gMnSc1	Hašek
vnímán	vnímán	k2eAgMnSc1d1	vnímán
jako	jako	k8xC	jako
geniální	geniální	k2eAgMnSc1d1	geniální
romanopisec	romanopisec	k1gMnSc1	romanopisec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dal	dát	k5eAaPmAgMnS	dát
světu	svět	k1gInSc3	svět
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejpůsobivějších	působivý	k2eAgInPc2d3	nejpůsobivější
satirických	satirický	k2eAgInPc2d1	satirický
popisů	popis	k1gInPc2	popis
dění	dění	k1gNnSc2	dění
na	na	k7c6	na
bojištích	bojiště	k1gNnPc6	bojiště
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
je	být	k5eAaImIp3nS	být
stavěna	stavěn	k2eAgFnSc1d1	stavěna
na	na	k7c4	na
roveň	roveň	k1gFnSc4	roveň
s	s	k7c7	s
dílem	díl	k1gInSc7	díl
Franze	Franze	k1gFnSc1	Franze
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
,	,	kIx,	,
jakkoliv	jakkoliv	k6eAd1	jakkoliv
se	se	k3xPyFc4	se
od	od	k7c2	od
něho	on	k3xPp3gMnSc2	on
liší	lišit	k5eAaImIp3nS	lišit
žánrem	žánr	k1gInSc7	žánr
a	a	k8xC	a
pohledem	pohled	k1gInSc7	pohled
na	na	k7c4	na
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
je	být	k5eAaImIp3nS	být
připomínáno	připomínán	k2eAgNnSc1d1	připomínáno
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
slavní	slavný	k2eAgMnPc1d1	slavný
rodáci	rodák	k1gMnPc1	rodák
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
narodili	narodit	k5eAaPmAgMnP	narodit
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
má	mít	k5eAaImIp3nS	mít
Hašek	Hašek	k1gMnSc1	Hašek
dodnes	dodnes	k6eAd1	dodnes
velmi	velmi	k6eAd1	velmi
mnoho	mnoho	k4c1	mnoho
příznivců	příznivec	k1gMnPc2	příznivec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
dokonce	dokonce	k9	dokonce
vlastní	vlastní	k2eAgFnSc4d1	vlastní
organizaci	organizace	k1gFnSc4	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
Bugulma	Bugulmum	k1gNnSc2	Bugulmum
má	mít	k5eAaImIp3nS	mít
Hašek	Hašek	k1gMnSc1	Hašek
vlastní	vlastní	k2eAgNnSc4d1	vlastní
muzeum	muzeum	k1gNnSc4	muzeum
<g/>
,	,	kIx,	,
v	v	k7c6	v
Sankt-Petěrburgu	Sankt-Petěrburg	k1gInSc6	Sankt-Petěrburg
a	a	k8xC	a
Moskvě	Moskva	k1gFnSc6	Moskva
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenovány	pojmenovat	k5eAaPmNgFnP	pojmenovat
ulice	ulice	k1gFnPc1	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
moskevská	moskevský	k2eAgFnSc1d1	Moskevská
leží	ležet	k5eAaImIp3nS	ležet
dokonce	dokonce	k9	dokonce
poblíž	poblíž	k6eAd1	poblíž
Velvyslanectví	velvyslanectví	k1gNnSc1	velvyslanectví
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Českého	český	k2eAgInSc2d1	český
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
Haškova	Haškův	k2eAgFnSc1d1	Haškova
100	[number]	k4	100
<g/>
.	.	kIx.	.
výroční	výroční	k2eAgNnPc1d1	výroční
narození	narození	k1gNnPc1	narození
byla	být	k5eAaImAgNnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
vydána	vydán	k2eAgFnSc1d1	vydána
stříbrná	stříbrná	k1gFnSc1	stříbrná
československá	československý	k2eAgFnSc1d1	Československá
100	[number]	k4	100
<g/>
korunová	korunový	k2eAgFnSc1d1	korunová
pamětní	pamětní	k2eAgFnSc1d1	pamětní
mince	mince	k1gFnSc1	mince
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
Štefan	Štefan	k1gMnSc1	Štefan
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byl	být	k5eAaImAgMnS	být
Jaroslavu	Jaroslav	k1gMnSc3	Jaroslav
Haškovi	Hašek	k1gMnSc3	Hašek
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
Žižkově	Žižkov	k1gInSc6	Žižkov
odhalen	odhalit	k5eAaPmNgInS	odhalit
jezdecký	jezdecký	k2eAgInSc1d1	jezdecký
pomník	pomník	k1gInSc1	pomník
od	od	k7c2	od
Karla	Karel	k1gMnSc2	Karel
Nepraše	Nepraš	k1gMnSc2	Nepraš
a	a	k8xC	a
Karolíny	Karolína	k1gFnSc2	Karolína
Neprašové	Neprašová	k1gFnSc2	Neprašová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2008	[number]	k4	2008
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Lipnici	Lipnice	k1gFnSc6	Lipnice
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
(	(	kIx(	(
<g/>
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Haškova	Haškův	k2eAgInSc2d1	Haškův
oblíbeného	oblíbený	k2eAgInSc2d1	oblíbený
hostince	hostinec	k1gInSc2	hostinec
U	u	k7c2	u
České	český	k2eAgFnSc2d1	Česká
koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
napsal	napsat	k5eAaPmAgMnS	napsat
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
Osudů	osud	k1gInPc2	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc2	Švejk
<g/>
)	)	kIx)	)
odhalen	odhalit	k5eAaPmNgInS	odhalit
druhý	druhý	k4xOgInSc1	druhý
pomník	pomník	k1gInSc1	pomník
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Haška	Hašek	k1gMnSc2	Hašek
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc1	dílo
akademického	akademický	k2eAgMnSc2d1	akademický
sochaře	sochař	k1gMnSc2	sochař
Josefa	Josef	k1gMnSc2	Josef
Malejovského	Malejovský	k1gMnSc2	Malejovský
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
umístění	umístění	k1gNnSc4	umístění
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
bylo	být	k5eAaImAgNnS	být
dříve	dříve	k6eAd2	dříve
odmítnuto	odmítnut	k2eAgNnSc1d1	odmítnuto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
nádraží	nádraží	k1gNnSc6	nádraží
ve	v	k7c4	v
Světlé	světlý	k2eAgNnSc4d1	světlé
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
odhalena	odhalen	k2eAgFnSc1d1	odhalena
bronzová	bronzový	k2eAgFnSc1d1	bronzová
deska	deska	k1gFnSc1	deska
sochaře	sochař	k1gMnSc2	sochař
Radomíra	Radomír	k1gMnSc2	Radomír
Dvořáka	Dvořák	k1gMnSc2	Dvořák
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
připomíná	připomínat	k5eAaImIp3nS	připomínat
příjezd	příjezd	k1gInSc4	příjezd
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Haška	Hašek	k1gMnSc2	Hašek
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc2	jeho
přítele	přítel	k1gMnSc2	přítel
malíře	malíř	k1gMnSc2	malíř
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Panušky	panuška	k1gFnSc2	panuška
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Jaroslavu	Jaroslav	k1gMnSc3	Jaroslav
Haškovi	Hašek	k1gMnSc3	Hašek
je	být	k5eAaImIp3nS	být
pojmenován	pojmenován	k2eAgInSc1d1	pojmenován
vlak	vlak	k1gInSc1	vlak
kategorie	kategorie	k1gFnSc2	kategorie
Eurocity	Eurocit	k1gInPc1	Eurocit
jezdící	jezdící	k2eAgInPc1d1	jezdící
mezi	mezi	k7c7	mezi
Prahou	Praha	k1gFnSc7	Praha
<g/>
,	,	kIx,	,
Brnem	Brno	k1gNnSc7	Brno
<g/>
,	,	kIx,	,
Bratislavou	Bratislava	k1gFnSc7	Bratislava
<g/>
,	,	kIx,	,
Budapeští	Budapešť	k1gFnSc7	Budapešť
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
číslo	číslo	k1gNnSc1	číslo
275	[number]	k4	275
<g/>
/	/	kIx~	/
<g/>
274	[number]	k4	274
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
130	[number]	k4	130
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
Haškova	Haškův	k2eAgNnSc2d1	Haškovo
narození	narození	k1gNnSc2	narození
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
louce	louka	k1gFnSc6	louka
u	u	k7c2	u
Lipnice	Lipnice	k1gFnSc2	Lipnice
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
odhalena	odhalen	k2eAgFnSc1d1	odhalena
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
velká	velký	k2eAgFnSc1d1	velká
žulová	žulový	k2eAgFnSc1d1	Žulová
hlava	hlava	k1gFnSc1	hlava
vysoká	vysoká	k1gFnSc1	vysoká
265	[number]	k4	265
cm	cm	kA	cm
s	s	k7c7	s
názvem	název	k1gInSc7	název
Hlava	hlava	k1gFnSc1	hlava
XXII	XXII	kA	XXII
<g/>
.	.	kIx.	.
</s>
<s>
Sochu	socha	k1gFnSc4	socha
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
umělec	umělec	k1gMnSc1	umělec
Radomír	Radomír	k1gMnSc1	Radomír
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Hlava	hlava	k1gFnSc1	hlava
XXII	XXII	kA	XXII
je	být	k5eAaImIp3nS	být
vzat	vzít	k5eAaPmNgInS	vzít
z	z	k7c2	z
názvu	název	k1gInSc2	název
knihy	kniha	k1gFnSc2	kniha
Josepha	Joseph	k1gMnSc2	Joseph
Hellera	Heller	k1gMnSc2	Heller
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
svědčí	svědčit	k5eAaImIp3nS	svědčit
Arnošt	Arnošt	k1gMnSc1	Arnošt
Lustig	Lustig	k1gMnSc1	Lustig
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gMnSc1	Joseph
Heller	Heller	k1gInSc4	Heller
mu	on	k3xPp3gMnSc3	on
osobně	osobně	k6eAd1	osobně
před	před	k7c7	před
lety	léto	k1gNnPc7	léto
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
by	by	kYmCp3nP	by
bez	bez	k7c2	bez
četby	četba	k1gFnSc2	četba
Dobrého	dobrý	k2eAgMnSc4d1	dobrý
vojáka	voják	k1gMnSc4	voják
Švejka	Švejk	k1gMnSc4	Švejk
svůj	svůj	k3xOyFgInSc4	svůj
americký	americký	k2eAgInSc4d1	americký
román	román	k1gInSc4	román
Hlava	Hlava	k1gMnSc1	Hlava
XXII	XXII	kA	XXII
nenapsal	napsat	k5eNaPmAgMnS	napsat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
soše	socha	k1gFnSc6	socha
je	být	k5eAaImIp3nS	být
vytesán	vytesán	k2eAgInSc4d1	vytesán
citát	citát	k1gInSc4	citát
ze	z	k7c2	z
Švejka	Švejk	k1gMnSc2	Švejk
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Život	život	k1gInSc1	život
lidský	lidský	k2eAgInSc1d1	lidský
<g/>
,	,	kIx,	,
poslušně	poslušně	k6eAd1	poslušně
hlásím	hlásit	k5eAaImIp1nS	hlásit
<g/>
,	,	kIx,	,
pane	pan	k1gMnSc5	pan
obrlajtnant	obrlajtnant	k1gInSc4	obrlajtnant
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
složitej	složitej	k?	složitej
<g/>
,	,	kIx,	,
že	že	k8xS	že
samotnej	samotnej	k?	samotnej
život	život	k1gInSc1	život
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
hadr	hadr	k1gInSc1	hadr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Haškovu	Haškův	k2eAgInSc3d1	Haškův
životu	život	k1gInSc3	život
byla	být	k5eAaImAgFnS	být
věnována	věnován	k2eAgFnSc1d1	věnována
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
Jana	Jan	k1gMnSc2	Jan
Schmida	Schmid	k1gMnSc2	Schmid
Voni	Voni	k?	Voni
sou	sou	k1gInSc2	sou
hodnej	hodnej	k?	hodnej
chlapec	chlapec	k1gMnSc1	chlapec
aneb	aneb	k?	aneb
Anabáze	anabáze	k1gFnSc2	anabáze
Jaroslava	Jaroslav	k1gMnSc4	Jaroslav
Haška	Hašek	k1gMnSc4	Hašek
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
úspěšně	úspěšně	k6eAd1	úspěšně
uváděla	uvádět	k5eAaImAgFnS	uvádět
pražská	pražský	k2eAgFnSc1d1	Pražská
Ypsilonka	Ypsilonka	k1gFnSc1	Ypsilonka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
vnikla	vniknout	k5eAaPmAgFnS	vniknout
koprodukční	koprodukční	k2eAgFnSc1d1	koprodukční
sovětsko-československá	sovětsko-československý	k2eAgFnSc1d1	sovětsko-československý
válečná	válečná	k1gFnSc1	válečná
komedie	komedie	k1gFnSc2	komedie
Velká	velký	k2eAgFnSc1d1	velká
cesta	cesta	k1gFnSc1	cesta
(	(	kIx(	(
<g/>
Bolšaja	Bolšaj	k2eAgFnSc1d1	Bolšaja
doroga	doroga	k1gFnSc1	doroga
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
Haškovu	Haškův	k2eAgInSc3d1	Haškův
pobytu	pobyt	k1gInSc3	pobyt
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
Jaroslava	Jaroslav	k1gMnSc4	Jaroslav
Haška	Hašek	k1gMnSc4	Hašek
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
Josef	Josef	k1gMnSc1	Josef
Abrhám	Abrha	k1gFnPc3	Abrha
<g/>
,	,	kIx,	,
vojáka	voják	k1gMnSc4	voják
Strašlipku	Strašlipek	k1gInSc2	Strašlipek
-	-	kIx~	-
předobraz	předobraz	k1gInSc1	předobraz
Švejka	Švejk	k1gMnSc2	Švejk
-	-	kIx~	-
hrál	hrát	k5eAaImAgMnS	hrát
Rudolf	Rudolf	k1gMnSc1	Rudolf
Hrušínský	Hrušínský	k2eAgMnSc1d1	Hrušínský
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Haška	Hašek	k1gMnSc2	Hašek
se	se	k3xPyFc4	se
v	v	k7c6	v
beletrii	beletrie	k1gFnSc6	beletrie
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
biografií	biografie	k1gFnPc2	biografie
a	a	k8xC	a
vzpomínkových	vzpomínkový	k2eAgFnPc2d1	vzpomínková
knih	kniha	k1gFnPc2	kniha
<g/>
)	)	kIx)	)
objevila	objevit	k5eAaPmAgFnS	objevit
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historickém	historický	k2eAgInSc6d1	historický
románu	román	k1gInSc6	román
Františky	Františka	k1gFnSc2	Františka
Vrbenské	Vrbenský	k2eAgFnSc2d1	Vrbenská
Naganty	Naganta	k1gFnSc2	Naganta
a	a	k8xC	a
vlčí	vlčí	k2eAgInPc1d1	vlčí
máky	mák	k1gInPc1	mák
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
;	;	kIx,	;
Triton	triton	k1gMnSc1	triton
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
s	s	k7c7	s
fantastickými	fantastický	k2eAgInPc7d1	fantastický
prvky	prvek	k1gInPc7	prvek
je	být	k5eAaImIp3nS	být
věnován	věnován	k2eAgInSc1d1	věnován
Haškově	Haškův	k2eAgNnSc6d1	Haškovo
působení	působení	k1gNnSc6	působení
v	v	k7c6	v
Irkutsku	Irkutsk	k1gInSc6	Irkutsk
a	a	k8xC	a
pravděpodobné	pravděpodobný	k2eAgFnSc6d1	pravděpodobná
misi	mise	k1gFnSc6	mise
do	do	k7c2	do
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
<g/>
.	.	kIx.	.
</s>
<s>
Filmový	filmový	k2eAgInSc4d1	filmový
dokument	dokument	k1gInSc4	dokument
Zrádce	zrádce	k1gMnSc2	zrádce
národa	národ	k1gInSc2	národ
v	v	k7c6	v
Chotěboři	Chotěboř	k1gFnSc6	Chotěboř
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
v	v	k7c6	v
Krátkém	krátký	k2eAgInSc6d1	krátký
filmu	film	k1gInSc6	film
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
r.	r.	kA	r.
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hovorka	Hovorka	k1gMnSc1	Hovorka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
neznámou	známý	k2eNgFnSc4d1	neznámá
epizodu	epizoda	k1gFnSc4	epizoda
ze	z	k7c2	z
života	život	k1gInSc2	život
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Haška	Hašek	k1gMnSc2	Hašek
a	a	k8xC	a
vzpomínky	vzpomínka	k1gFnSc2	vzpomínka
bratří	bratr	k1gMnPc2	bratr
Kubánků	Kubánek	k1gMnPc2	Kubánek
posledních	poslední	k2eAgInPc2d1	poslední
žijících	žijící	k2eAgInPc2d1	žijící
pamětníků	pamětník	k1gInPc2	pamětník
této	tento	k3xDgFnSc2	tento
události	událost	k1gFnSc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Májové	májový	k2eAgInPc1d1	májový
výkřiky	výkřik	k1gInPc1	výkřik
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
básní	básnit	k5eAaImIp3nS	básnit
Galerie	galerie	k1gFnSc1	galerie
karikatur	karikatura	k1gFnPc2	karikatura
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
Trampoty	trampota	k1gFnSc2	trampota
pana	pan	k1gMnSc4	pan
Tenkráta	Tenkrát	k1gMnSc4	Tenkrát
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
voják	voják	k1gMnSc1	voják
Švejk	Švejk	k1gMnSc1	Švejk
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
podivné	podivný	k2eAgFnPc4d1	podivná
historky	historka	k1gFnPc4	historka
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
Průvodčí	průvodčí	k1gFnPc1	průvodčí
cizinců	cizinec	k1gMnPc2	cizinec
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
satiry	satira	k1gFnSc2	satira
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
i	i	k9	i
z	z	k7c2	z
domova	domov	k1gInSc2	domov
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
Můj	můj	k3xOp1gInSc1	můj
obchod	obchod	k1gInSc1	obchod
se	s	k7c7	s
psy	pes	k1gMnPc7	pes
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
humoresky	humoreska	k1gFnPc1	humoreska
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
voják	voják	k1gMnSc1	voják
Švejk	Švejk	k1gMnSc1	Švejk
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
(	(	kIx(	(
<g/>
vydáno	vydat	k5eAaPmNgNnS	vydat
1917	[number]	k4	1917
ve	v	k7c6	v
Slovanském	slovanský	k2eAgNnSc6d1	slovanské
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
<g/>
)	)	kIx)	)
Dva	dva	k4xCgInPc4	dva
tucty	tucet	k1gInPc4	tucet
povídek	povídka	k1gFnPc2	povídka
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
Tři	tři	k4xCgMnPc1	tři
muži	muž	k1gMnPc1	muž
se	s	k7c7	s
žralokem	žralok	k1gMnSc7	žralok
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
poučné	poučný	k2eAgFnPc1d1	poučná
historky	historka	k1gFnPc1	historka
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
Pepíček	Pepíček	k1gMnSc1	Pepíček
Nový	nový	k2eAgMnSc1d1	nový
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
Velitelem	velitel	k1gMnSc7	velitel
města	město	k1gNnSc2	město
Bugulmy	Bugulma	k1gFnSc2	Bugulma
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
Mírová	mírový	k2eAgFnSc1d1	mírová
konference	konference	k1gFnSc1	konference
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
humoresky	humoreska	k1gFnPc1	humoreska
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
voják	voják	k1gMnSc1	voják
Švejk	Švejk	k1gMnSc1	Švejk
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
podivné	podivný	k2eAgFnSc2d1	podivná
historky	historka	k1gFnSc2	historka
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
Osudy	osud	k1gInPc1	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc4	Švejk
za	za	k7c2	za
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
-	-	kIx~	-
<g/>
1923	[number]	k4	1923
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Větrný	větrný	k2eAgMnSc1d1	větrný
mlynář	mlynář	k1gMnSc1	mlynář
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
(	(	kIx(	(
<g/>
knižně	knižně	k6eAd1	knižně
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
Červená	červený	k2eAgFnSc1d1	červená
sedma	sedma	k1gFnSc1	sedma
Paměti	paměť	k1gFnSc2	paměť
úctyhodné	úctyhodný	k2eAgFnSc2d1	úctyhodná
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
příběhy	příběh	k1gInPc4	příběh
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
Šťastný	šťastný	k2eAgInSc1d1	šťastný
domov	domov	k1gInSc1	domov
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
humoresky	humoreska	k1gFnPc1	humoreska
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
Za	za	k7c2	za
války	válka	k1gFnSc2	válka
i	i	k9	i
za	za	k7c2	za
sovětů	sovět	k1gInPc2	sovět
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
Zpověď	zpověď	k1gFnSc1	zpověď
starého	starý	k2eAgInSc2d1	starý
<g />
.	.	kIx.	.
</s>
<s>
mládence	mládenec	k1gMnSc4	mládenec
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
Všivá	všivý	k2eAgNnPc4d1	všivé
historie	historie	k1gFnSc2	historie
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
humoresky	humoreska	k1gFnSc2	humoreska
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
Podivuhodné	podivuhodný	k2eAgNnSc4d1	podivuhodné
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
kocoura	kocour	k1gMnSc2	kocour
Markuse	Markuse	k1gFnSc2	Markuse
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
humoresky	humoreska	k1gFnSc2	humoreska
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
Smějeme	smát	k5eAaImIp1nP	smát
se	se	k3xPyFc4	se
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Haškem	Hašek	k1gMnSc7	Hašek
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
díly	díl	k1gInPc4	díl
<g/>
)	)	kIx)	)
Škola	škola	k1gFnSc1	škola
humoru	humor	k1gInSc2	humor
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
Malá	malý	k2eAgNnPc4d1	malé
<g />
.	.	kIx.	.
</s>
<s>
zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
Veselé	Veselé	k2eAgFnSc2d1	Veselé
povídky	povídka	k1gFnSc2	povídka
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
také	také	k9	také
Historky	historka	k1gFnPc1	historka
z	z	k7c2	z
ražické	ražický	k2eAgFnSc2d1	ražický
bašty	bašta	k1gFnSc2	bašta
Aféra	aféra	k1gFnSc1	aféra
s	s	k7c7	s
křečkem	křeček	k1gMnSc7	křeček
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
Črty	črta	k1gFnPc1	črta
<g/>
,	,	kIx,	,
povídky	povídka	k1gFnPc1	povídka
a	a	k8xC	a
humoresky	humoreska	k1gFnPc1	humoreska
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
Fialový	fialový	k2eAgInSc1d1	fialový
hrom	hrom	k1gInSc1	hrom
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
Loupežný	loupežný	k2eAgMnSc1d1	loupežný
vrah	vrah	k1gMnSc1	vrah
před	před	k7c7	před
soudem	soud	k1gInSc7	soud
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
Terciánská	terciánský	k2eAgFnSc1d1	terciánský
vzpoura	vzpoura	k1gFnSc1	vzpoura
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
Dědictví	dědictví	k1gNnSc6	dědictví
po	po	k7c6	po
panu	pan	k1gMnSc6	pan
Šafránkovi	Šafránek	k1gMnSc6	Šafránek
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
Zrádce	zrádce	k1gMnSc1	zrádce
národa	národ	k1gInSc2	národ
v	v	k7c6	v
Chotěboři	Chotěboř	k1gFnSc6	Chotěboř
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
Politické	politický	k2eAgFnPc1d1	politická
a	a	k8xC	a
sociální	sociální	k2eAgFnPc1d1	sociální
dějiny	dějiny	k1gFnPc1	dějiny
strany	strana	k1gFnSc2	strana
mírného	mírný	k2eAgInSc2d1	mírný
pokroku	pokrok	k1gInSc2	pokrok
v	v	k7c6	v
mezích	mez	k1gFnPc6	mez
zákona	zákon	k1gInSc2	zákon
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
napsáno	napsat	k5eAaBmNgNnS	napsat
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
knižně	knižně	k6eAd1	knižně
vydáno	vydat	k5eAaPmNgNnS	vydat
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
Dekameron	Dekameron	k1gInSc1	Dekameron
humoru	humor	k1gInSc2	humor
a	a	k8xC	a
satiry	satira	k1gFnSc2	satira
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Moje	můj	k3xOp1gFnSc1	můj
zpověď	zpověď	k1gFnSc1	zpověď
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Zábavný	zábavný	k2eAgInSc1d1	zábavný
a	a	k8xC	a
poučný	poučný	k2eAgInSc1d1	poučný
koutek	koutek	k1gInSc1	koutek
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Haška	Hašek	k1gMnSc2	Hašek
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Oslí	oslí	k2eAgFnSc2d1	oslí
historie	historie	k1gFnSc2	historie
aneb	aneb	k?	aneb
Vojenské	vojenský	k2eAgInPc1d1	vojenský
články	článek	k1gInPc1	článek
do	do	k7c2	do
čítanek	čítanka	k1gFnPc2	čítanka
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc1	svět
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
Švejk	Švejk	k1gMnSc1	Švejk
před	před	k7c7	před
Švejkem	Švejk	k1gMnSc7	Švejk
(	(	kIx(	(
<g/>
neznámé	známý	k2eNgInPc1d1	neznámý
osudy	osud	k1gInPc1	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc2	Švejk
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
Tajemství	tajemství	k1gNnSc1	tajemství
mého	můj	k3xOp1gInSc2	můj
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
Povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
svazky	svazek	k1gInPc4	svazek
<g/>
)	)	kIx)	)
V	v	k7c6	v
polepšovně	polepšovna	k1gFnSc6	polepšovna
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Když	když	k8xS	když
bolševici	bolševik	k1gMnPc1	bolševik
zrušili	zrušit	k5eAaPmAgMnP	zrušit
Vánoce	Vánoce	k1gFnPc1	Vánoce
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
-	-	kIx~	-
neznámé	neznámá	k1gFnPc1	neznámá
<g/>
,	,	kIx,	,
utajené	utajený	k2eAgInPc1d1	utajený
a	a	k8xC	a
pravděpodobné	pravděpodobný	k2eAgInPc1d1	pravděpodobný
texty	text	k1gInPc1	text
Nešťastný	šťastný	k2eNgMnSc1d1	nešťastný
policejní	policejní	k2eAgMnSc1d1	policejní
ředitel	ředitel	k1gMnSc1	ředitel
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
-	-	kIx~	-
neznámé	známý	k2eNgNnSc1d1	neznámé
a	a	k8xC	a
nově	nově	k6eAd1	nově
nalezené	nalezený	k2eAgInPc1d1	nalezený
texty	text	k1gInPc1	text
Povídky	povídka	k1gFnSc2	povídka
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
-	-	kIx~	-
20	[number]	k4	20
vybraných	vybraný	k2eAgFnPc2d1	vybraná
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Román	román	k1gInSc1	román
novofundláka	novofundlák	k1gMnSc2	novofundlák
Ogly	Ogla	k1gMnSc2	Ogla
<g/>
,	,	kIx,	,
Aféra	aféra	k1gFnSc1	aféra
s	s	k7c7	s
křečkem	křeček	k1gMnSc7	křeček
<g/>
,	,	kIx,	,
Útrapy	útrap	k1gInPc1	útrap
vychovatele	vychovatel	k1gMnSc2	vychovatel
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc1	příběh
Z	z	k7c2	z
praktického	praktický	k2eAgInSc2d1	praktický
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
</s>
