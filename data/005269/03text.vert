<s>
Louvre	Louvre	k1gInSc1	Louvre
je	být	k5eAaImIp3nS	být
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
největším	veliký	k2eAgNnPc3d3	veliký
muzeím	muzeum	k1gNnPc3	muzeum
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
palácovém	palácový	k2eAgInSc6d1	palácový
komplexu	komplex	k1gInSc6	komplex
Palais	Palais	k1gFnSc2	Palais
du	du	k?	du
Louvre	Louvre	k1gInSc1	Louvre
<g/>
,	,	kIx,	,
bývalém	bývalý	k2eAgNnSc6d1	bývalé
sídle	sídlo	k1gNnSc6	sídlo
francouzských	francouzský	k2eAgMnPc2d1	francouzský
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
obvodu	obvod	k1gInSc2	obvod
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Seiny	Seina	k1gFnSc2	Seina
obklopený	obklopený	k2eAgMnSc1d1	obklopený
ulicí	ulice	k1gFnSc7	ulice
Rue	Rue	k1gFnSc2	Rue
de	de	k?	de
Rivoli	Rivole	k1gFnSc4	Rivole
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
<g/>
,	,	kIx,	,
Tuilerijskými	tuilerijský	k2eAgFnPc7d1	tuilerijský
zahradami	zahrada	k1gFnPc7	zahrada
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
a	a	k8xC	a
kostelem	kostel	k1gInSc7	kostel
Saint-Germain-l	Saint-Germain	k1gInSc1	Saint-Germain-l
<g/>
'	'	kIx"	'
<g/>
Auxerrois	Auxerrois	k1gInSc1	Auxerrois
z	z	k7c2	z
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
původního	původní	k2eAgInSc2d1	původní
loveckého	lovecký	k2eAgInSc2d1	lovecký
zámečku	zámeček	k1gInSc2	zámeček
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
kdysi	kdysi	k6eAd1	kdysi
stával	stávat	k5eAaImAgMnS	stávat
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
místech	místo	k1gNnPc6	místo
uprostřed	uprostřed	k7c2	uprostřed
lesa	les	k1gInSc2	les
zvaného	zvaný	k2eAgInSc2d1	zvaný
Louparie	Louparie	k1gFnPc4	Louparie
či	či	k8xC	či
Louverie	Louverie	k1gFnPc4	Louverie
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
možnost	možnost	k1gFnSc1	možnost
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
leovar	leovar	k1gInSc1	leovar
nebo	nebo	k8xC	nebo
leawer	leawer	k1gInSc1	leawer
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
opevněné	opevněný	k2eAgNnSc4d1	opevněné
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
tvrz	tvrz	k1gFnSc1	tvrz
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Karla	Karel	k1gMnSc2	Karel
V.	V.	kA	V.
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gFnSc7	jeho
rezidencí	rezidence	k1gFnSc7	rezidence
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
I.	I.	kA	I.
pak	pak	k6eAd1	pak
založil	založit	k5eAaPmAgMnS	založit
roku	rok	k1gInSc2	rok
1541	[number]	k4	1541
palác	palác	k1gInSc1	palác
nový	nový	k2eAgInSc1d1	nový
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gFnSc6	jehož
stavbě	stavba	k1gFnSc6	stavba
postupně	postupně	k6eAd1	postupně
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
další	další	k2eAgMnPc1d1	další
panovníci	panovník	k1gMnPc1	panovník
a	a	k8xC	a
stavitelé	stavitel	k1gMnPc1	stavitel
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
Pierre	Pierr	k1gInSc5	Pierr
Lescot	Lescot	k1gMnSc1	Lescot
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Jindřicha	Jindřich	k1gMnSc2	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
Kateřina	Kateřina	k1gFnSc1	Kateřina
Medicejská	Medicejský	k2eAgFnSc1d1	Medicejská
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
palác	palác	k1gInSc4	palác
o	o	k7c4	o
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gNnSc6	jehož
místě	místo	k1gNnSc6	místo
dnes	dnes	k6eAd1	dnes
stojí	stát	k5eAaImIp3nS	stát
Galerie	galerie	k1gFnPc4	galerie
Apollon	Apollon	k1gMnSc1	Apollon
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Jindřicha	Jindřich	k1gMnSc2	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1589	[number]	k4	1589
<g/>
–	–	k?	–
<g/>
1610	[number]	k4	1610
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pracovali	pracovat	k5eAaImAgMnP	pracovat
na	na	k7c6	na
dalších	další	k2eAgFnPc6d1	další
částech	část	k1gFnPc6	část
stavitelé	stavitel	k1gMnPc1	stavitel
Thibauld	Thibauld	k1gMnSc1	Thibauld
a	a	k8xC	a
Louis	Louis	k1gMnSc1	Louis
Métézeau	Métézeaus	k1gInSc2	Métézeaus
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
Baptiste	Baptist	k1gMnSc5	Baptist
a	a	k8xC	a
Jacques	Jacques	k1gMnSc1	Jacques
Androuet	Androuet	k1gMnSc1	Androuet
DuCerceau	DuCerceaa	k1gMnSc4	DuCerceaa
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
1623	[number]	k4	1623
uložil	uložit	k5eAaPmAgMnS	uložit
staviteli	stavitel	k1gMnSc3	stavitel
Lemercierovi	Lemercier	k1gMnSc3	Lemercier
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dokončil	dokončit	k5eAaPmAgMnS	dokončit
některé	některý	k3yIgFnPc4	některý
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
začal	začít	k5eAaPmAgMnS	začít
budovat	budovat	k5eAaImF	budovat
Lescot	Lescot	k1gInSc4	Lescot
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byl	být	k5eAaImAgMnS	být
původní	původní	k2eAgInSc4d1	původní
plán	plán	k1gInSc4	plán
stavby	stavba	k1gFnSc2	stavba
značně	značně	k6eAd1	značně
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
rokem	rok	k1gInSc7	rok
1660	[number]	k4	1660
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
v	v	k7c6	v
dostavbách	dostavba	k1gFnPc6	dostavba
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
B.	B.	kA	B.
Levaua	Levauum	k1gNnSc2	Levauum
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zbudována	zbudován	k2eAgFnSc1d1	zbudována
východní	východní	k2eAgFnSc1d1	východní
fasáda	fasáda	k1gFnSc1	fasáda
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
mnohokrát	mnohokrát	k6eAd1	mnohokrát
přerušována	přerušovat	k5eAaImNgFnS	přerušovat
<g/>
,	,	kIx,	,
od	od	k7c2	od
vlády	vláda	k1gFnSc2	vláda
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
až	až	k9	až
po	po	k7c4	po
Francouzskou	francouzský	k2eAgFnSc4d1	francouzská
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
Napoleona	Napoleon	k1gMnSc4	Napoleon
architekti	architekt	k1gMnPc1	architekt
I.	I.	kA	I.
Percier	Percier	k1gMnSc1	Percier
a	a	k8xC	a
L.	L.	kA	L.
Fontaine	Fontain	k1gInSc5	Fontain
vystavěli	vystavět	k5eAaPmAgMnP	vystavět
novou	nový	k2eAgFnSc4d1	nová
galerii	galerie	k1gFnSc4	galerie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
rovnoběžná	rovnoběžný	k2eAgFnSc1d1	rovnoběžná
s	s	k7c7	s
protější	protější	k2eAgFnSc7d1	protější
galerií	galerie	k1gFnSc7	galerie
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
Jindřicha	Jindřich	k1gMnSc2	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
po	po	k7c6	po
poslední	poslední	k2eAgFnSc6d1	poslední
přestávce	přestávka	k1gFnSc6	přestávka
za	za	k7c2	za
Napoleona	Napoleon	k1gMnSc2	Napoleon
III	III	kA	III
<g/>
.	.	kIx.	.
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1852	[number]	k4	1852
<g/>
–	–	k?	–
<g/>
1857	[number]	k4	1857
byl	být	k5eAaImAgInS	být
palác	palác	k1gInSc1	palác
dokončen	dokončit	k5eAaPmNgInS	dokončit
staviteli	stavitel	k1gMnPc7	stavitel
Lefuelem	Lefuel	k1gMnSc7	Lefuel
a	a	k8xC	a
Viscontim	Visconti	k1gNnSc7	Visconti
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
po	po	k7c6	po
300	[number]	k4	300
letech	let	k1gInPc6	let
Paříž	Paříž	k1gFnSc1	Paříž
přestala	přestat	k5eAaPmAgFnS	přestat
být	být	k5eAaImF	být
sídlem	sídlo	k1gNnSc7	sídlo
francouzských	francouzský	k2eAgMnPc2d1	francouzský
králů	král	k1gMnPc2	král
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
usídlili	usídlit	k5eAaPmAgMnP	usídlit
v	v	k7c6	v
Louvru	Louvre	k1gInSc6	Louvre
mnozí	mnohý	k2eAgMnPc1d1	mnohý
dvořané	dvořan	k1gMnPc1	dvořan
<g/>
,	,	kIx,	,
úředníci	úředník	k1gMnPc1	úředník
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
umělci	umělec	k1gMnPc1	umělec
a	a	k8xC	a
učenci	učenec	k1gMnPc1	učenec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
při	při	k7c6	při
požáru	požár	k1gInSc6	požár
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
shořela	shořet	k5eAaPmAgFnS	shořet
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
Louvru	Louvre	k1gInSc2	Louvre
-	-	kIx~	-
Tuilerijský	tuilerijský	k2eAgInSc1d1	tuilerijský
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
zůstal	zůstat	k5eAaPmAgInS	zůstat
obrovský	obrovský	k2eAgInSc1d1	obrovský
komplex	komplex	k1gInSc1	komplex
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
195	[number]	k4	195
000	[number]	k4	000
m2	m2	k4	m2
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Louvre	Louvre	k1gInSc1	Louvre
také	také	k6eAd1	také
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xS	jako
královské	královský	k2eAgFnPc1d1	královská
přijímací	přijímací	k2eAgFnPc1d1	přijímací
komnaty	komnata	k1gFnPc1	komnata
<g/>
,	,	kIx,	,
dvorní	dvorní	k2eAgInPc1d1	dvorní
byty	byt	k1gInPc1	byt
<g/>
,	,	kIx,	,
ministerstva	ministerstvo	k1gNnPc1	ministerstvo
<g/>
,	,	kIx,	,
jízdárny	jízdárna	k1gFnPc1	jízdárna
<g/>
,	,	kIx,	,
konírny	konírna	k1gFnPc1	konírna
atd.	atd.	kA	atd.
Celý	celý	k2eAgInSc1d1	celý
palác	palác	k1gInSc1	palác
se	se	k3xPyFc4	se
dělil	dělit	k5eAaImAgInS	dělit
na	na	k7c4	na
Starý	starý	k2eAgInSc4d1	starý
a	a	k8xC	a
Nový	nový	k2eAgInSc4d1	nový
a	a	k8xC	a
přes	přes	k7c4	přes
patrný	patrný	k2eAgInSc4d1	patrný
nedostatek	nedostatek	k1gInSc4	nedostatek
jednotného	jednotný	k2eAgInSc2d1	jednotný
plánu	plán	k1gInSc2	plán
budovy	budova	k1gFnSc2	budova
Louvru	Louvre	k1gInSc2	Louvre
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
velmi	velmi	k6eAd1	velmi
harmonický	harmonický	k2eAgInSc4d1	harmonický
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
skvost	skvost	k1gInSc4	skvost
světové	světový	k2eAgFnSc2d1	světová
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Staré	Staré	k2eAgFnSc2d1	Staré
části	část	k1gFnSc2	část
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
sloupová	sloupový	k2eAgFnSc1d1	sloupová
fasáda	fasáda	k1gFnSc1	fasáda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1674	[number]	k4	1674
od	od	k7c2	od
F.	F.	kA	F.
Peraulta	Peraulta	k1gFnSc1	Peraulta
a	a	k8xC	a
Nová	nový	k2eAgFnSc1d1	nová
část	část	k1gFnSc1	část
s	s	k7c7	s
vyčnívajícími	vyčnívající	k2eAgInPc7d1	vyčnívající
pavilony	pavilon	k1gInPc7	pavilon
Turgot	Turgota	k1gFnPc2	Turgota
<g/>
,	,	kIx,	,
Richelieu	Richelieu	k1gMnSc1	Richelieu
<g/>
,	,	kIx,	,
Colbert	Colbert	k1gMnSc1	Colbert
<g/>
,	,	kIx,	,
Daru	dar	k1gInSc2	dar
<g/>
,	,	kIx,	,
Denon	Denon	kA	Denon
a	a	k8xC	a
Mollieu	Molliea	k1gFnSc4	Molliea
s	s	k7c7	s
vnitřním	vnitřní	k2eAgInSc7d1	vnitřní
bohatým	bohatý	k2eAgInSc7d1	bohatý
sadem	sad	k1gInSc7	sad
s	s	k7c7	s
86	[number]	k4	86
sochami	socha	k1gFnPc7	socha
francouzských	francouzský	k2eAgFnPc2d1	francouzská
osobností	osobnost	k1gFnPc2	osobnost
a	a	k8xC	a
63	[number]	k4	63
alegorických	alegorický	k2eAgNnPc2d1	alegorické
sousoší	sousoší	k1gNnPc2	sousoší
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Place	plac	k1gInSc6	plac
du	du	k?	du
Carrousel	Carrousel	k1gInSc1	Carrousel
mezi	mezi	k7c7	mezi
budovami	budova	k1gFnPc7	budova
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
Jindřicha	Jindřich	k1gMnSc2	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
Napoleona	Napoleon	k1gMnSc4	Napoleon
III	III	kA	III
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
podél	podél	k7c2	podél
Seiny	Seina	k1gFnSc2	Seina
rozevírá	rozevírat	k5eAaImIp3nS	rozevírat
Vítězný	vítězný	k2eAgInSc4d1	vítězný
oblouk	oblouk	k1gInSc4	oblouk
Carrousel	Carrousel	k1gMnSc1	Carrousel
<g/>
,	,	kIx,	,
zbudovaný	zbudovaný	k2eAgMnSc1d1	zbudovaný
za	za	k7c4	za
Napoleona	Napoleon	k1gMnSc4	Napoleon
I.	I.	kA	I.
roku	rok	k1gInSc2	rok
1806	[number]	k4	1806
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
brány	brána	k1gFnSc2	brána
Septimia	Septimium	k1gNnSc2	Septimium
Severa	Severa	k1gMnSc1	Severa
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
čtyřspřežím	čtyřspřeží	k1gNnSc7	čtyřspřeží
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
sbírky	sbírka	k1gFnSc2	sbírka
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
založil	založit	k5eAaPmAgMnS	založit
František	František	k1gMnSc1	František
I.	I.	kA	I.
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
koupil	koupit	k5eAaPmAgMnS	koupit
sbírky	sbírka	k1gFnSc2	sbírka
bankéře	bankéř	k1gMnSc2	bankéř
Jabacha	Jabach	k1gMnSc2	Jabach
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
za	za	k7c2	za
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
roku	rok	k1gInSc2	rok
1793	[number]	k4	1793
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Louvru	Louvre	k1gInSc6	Louvre
oficiálně	oficiálně	k6eAd1	oficiálně
otevřeno	otevřen	k2eAgNnSc4d1	otevřeno
veřejné	veřejný	k2eAgNnSc4d1	veřejné
muzeum	muzeum	k1gNnSc4	muzeum
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
byla	být	k5eAaImAgNnP	být
zpřístupněna	zpřístupněn	k2eAgNnPc1d1	zpřístupněno
nejrůznější	různý	k2eAgNnPc1d3	nejrůznější
díla	dílo	k1gNnPc1	dílo
roztroušená	roztroušený	k2eAgNnPc1d1	roztroušené
po	po	k7c6	po
královských	královský	k2eAgInPc6d1	královský
zámcích	zámek	k1gInPc6	zámek
<g/>
,	,	kIx,	,
kostelech	kostel	k1gInPc6	kostel
a	a	k8xC	a
klášterech	klášter	k1gInPc6	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
různých	různý	k2eAgFnPc6d1	různá
vítězných	vítězný	k2eAgFnPc6d1	vítězná
bitvách	bitva	k1gFnPc6	bitva
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
francouzská	francouzský	k2eAgNnPc1d1	francouzské
vojska	vojsko	k1gNnPc1	vojsko
přinášela	přinášet	k5eAaImAgNnP	přinášet
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
válečnou	válečný	k2eAgFnSc4d1	válečná
kořist	kořist	k1gFnSc4	kořist
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Louvre	Louvre	k1gInSc1	Louvre
významným	významný	k2eAgNnSc7d1	významné
evropským	evropský	k2eAgNnSc7d1	Evropské
i	i	k8xC	i
světovým	světový	k2eAgNnSc7d1	světové
muzeem	muzeum	k1gNnSc7	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
sbírky	sbírka	k1gFnPc1	sbírka
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
rozšiřovány	rozšiřován	k2eAgFnPc4d1	rozšiřována
<g/>
,	,	kIx,	,
zákonem	zákon	k1gInSc7	zákon
ze	z	k7c2	z
dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1888	[number]	k4	1888
má	mít	k5eAaImIp3nS	mít
Muzeum	muzeum	k1gNnSc4	muzeum
Louvre	Louvre	k1gInSc1	Louvre
7	[number]	k4	7
oddělení	oddělení	k1gNnSc2	oddělení
<g/>
:	:	kIx,	:
Egyptské	egyptský	k2eAgFnSc2d1	egyptská
starožitnosti	starožitnost	k1gFnSc2	starožitnost
Stará	starat	k5eAaImIp3nS	starat
Antická	antický	k2eAgFnSc1d1	antická
a	a	k8xC	a
Orientální	orientální	k2eAgFnSc1d1	orientální
keramika	keramika	k1gFnSc1	keramika
Řecké	řecký	k2eAgFnSc2d1	řecká
a	a	k8xC	a
Římské	římský	k2eAgFnSc2d1	římská
starožitnosti	starožitnost	k1gFnSc2	starožitnost
Malby	malba	k1gFnSc2	malba
<g/>
,	,	kIx,	,
kresby	kresba	k1gFnSc2	kresba
<g/>
,	,	kIx,	,
mědirytiny	mědirytina	k1gFnSc2	mědirytina
-	-	kIx~	-
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
<g/>
-XIX	-XIX	k?	-XIX
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Skulptury	skulptura	k1gFnSc2	skulptura
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
renesance	renesance	k1gFnSc1	renesance
a	a	k8xC	a
Moderní	moderní	k2eAgFnPc1d1	moderní
doby	doba	k1gFnPc1	doba
Malé	Malé	k2eAgInPc4d1	Malé
umělecké	umělecký	k2eAgInPc4d1	umělecký
předměty	předmět	k1gInPc4	předmět
světové	světový	k2eAgFnSc2d1	světová
historie	historie	k1gFnSc2	historie
Námořnictví	námořnictví	k1gNnSc2	námořnictví
a	a	k8xC	a
etnografie	etnografie	k1gFnSc2	etnografie
Každé	každý	k3xTgNnSc1	každý
oddělení	oddělení	k1gNnSc1	oddělení
má	mít	k5eAaImIp3nS	mít
svého	svůj	k3xOyFgMnSc4	svůj
správce	správce	k1gMnSc4	správce
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
řídí	řídit	k5eAaImIp3nS	řídit
tým	tým	k1gInSc4	tým
znalců	znalec	k1gMnPc2	znalec
<g/>
,	,	kIx,	,
konzervátorů	konzervátor	k1gMnPc2	konzervátor
a	a	k8xC	a
historiků	historik	k1gMnPc2	historik
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
časech	čas	k1gInPc6	čas
přelomu	přelom	k1gInSc2	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byly	být	k5eAaImAgFnP	být
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
provoz	provoz	k1gInSc1	provoz
každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
zvyšovány	zvyšovat	k5eAaImNgFnP	zvyšovat
<g/>
,	,	kIx,	,
od	od	k7c2	od
450	[number]	k4	450
000	[number]	k4	000
Fr	fr	k0	fr
na	na	k7c4	na
personál	personál	k1gInSc4	personál
a	a	k8xC	a
300	[number]	k4	300
000	[number]	k4	000
Fr	fr	k0	fr
na	na	k7c4	na
chod	chod	k1gInSc4	chod
a	a	k8xC	a
údržbu	údržba	k1gFnSc4	údržba
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
cca	cca	kA	cca
4	[number]	k4	4
milionů	milion	k4xCgInPc2	milion
€	€	k?	€
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
nepočítaje	nepočítaje	k7c4	nepočítaje
mezi	mezi	k7c4	mezi
to	ten	k3xDgNnSc4	ten
zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
restaurátorské	restaurátorský	k2eAgFnPc1d1	restaurátorská
akce	akce	k1gFnPc1	akce
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nedávných	dávný	k2eNgFnPc2d1	nedávná
investic	investice	k1gFnPc2	investice
byla	být	k5eAaImAgFnS	být
známá	známý	k2eAgFnSc1d1	známá
dostavba	dostavba	k1gFnSc1	dostavba
hlavního	hlavní	k2eAgInSc2d1	hlavní
vchodu	vchod	k1gInSc2	vchod
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
tyčí	tyčit	k5eAaImIp3nS	tyčit
skleněná	skleněný	k2eAgFnSc1d1	skleněná
pyramida	pyramida	k1gFnSc1	pyramida
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
vedle	vedle	k7c2	vedle
jiných	jiný	k2eAgFnPc2d1	jiná
známých	známý	k2eAgFnPc2d1	známá
staveb	stavba	k1gFnPc2	stavba
stala	stát	k5eAaPmAgFnS	stát
novou	nový	k2eAgFnSc7d1	nová
dominantou	dominanta	k1gFnSc7	dominanta
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
nejbohatší	bohatý	k2eAgFnPc1d3	nejbohatší
literární	literární	k2eAgFnPc1d1	literární
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
založení	založení	k1gNnSc2	založení
muzea	muzeum	k1gNnSc2	muzeum
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
Paulem	Paul	k1gMnSc7	Paul
Gaultierem	Gaultier	k1gMnSc7	Gaultier
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
3	[number]	k4	3
svazcích	svazek	k1gInPc6	svazek
<g/>
.	.	kIx.	.
</s>
