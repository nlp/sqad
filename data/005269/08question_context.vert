<s>
Louvre	Louvre	k1gInSc1	Louvre
je	být	k5eAaImIp3nS	být
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
největším	veliký	k2eAgNnPc3d3	veliký
muzeím	muzeum	k1gNnPc3	muzeum
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
palácovém	palácový	k2eAgInSc6d1	palácový
komplexu	komplex	k1gInSc6	komplex
Palais	Palais	k1gFnSc2	Palais
du	du	k?	du
Louvre	Louvre	k1gInSc1	Louvre
<g/>
,	,	kIx,	,
bývalém	bývalý	k2eAgNnSc6d1	bývalé
sídle	sídlo	k1gNnSc6	sídlo
francouzských	francouzský	k2eAgMnPc2d1	francouzský
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
