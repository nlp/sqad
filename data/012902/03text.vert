<p>
<s>
Chmel	chmel	k1gInSc1	chmel
(	(	kIx(	(
<g/>
Humulus	Humulus	k1gInSc1	Humulus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc4	rod
rostlin	rostlina	k1gFnPc2	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
konopovité	konopovitý	k2eAgFnSc2d1	konopovitý
(	(	kIx(	(
<g/>
Cannabidaceae	Cannabidacea	k1gFnSc2	Cannabidacea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
chmelová	chmelový	k2eAgFnSc1d1	chmelová
hlávka	hlávka	k1gFnSc1	hlávka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
známé	známý	k2eAgMnPc4d1	známý
zástupce	zástupce	k1gMnPc4	zástupce
patří	patřit	k5eAaImIp3nS	patřit
chmel	chmel	k1gInSc1	chmel
otáčivý	otáčivý	k2eAgInSc1d1	otáčivý
(	(	kIx(	(
<g/>
Humulus	Humulus	k1gInSc1	Humulus
lupulus	lupulus	k1gInSc1	lupulus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
užívaný	užívaný	k2eAgInSc1d1	užívaný
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
výroby	výroba	k1gFnSc2	výroba
piva	pivo	k1gNnSc2	pivo
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
například	například	k6eAd1	například
chmel	chmel	k1gInSc1	chmel
japonský	japonský	k2eAgInSc1d1	japonský
(	(	kIx(	(
<g/>
Humulus	Humulus	k1gInSc1	Humulus
scandens	scandens	k1gInSc1	scandens
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Chmelem	chmel	k1gInSc7	chmel
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
chmelařství	chmelařství	k1gNnSc1	chmelařství
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgInPc4d1	mladý
chmelové	chmelový	k2eAgInPc4d1	chmelový
výhonky	výhonek	k1gInPc4	výhonek
pojídali	pojídat	k5eAaImAgMnP	pojídat
labužníci	labužník	k1gMnPc1	labužník
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
za	za	k7c2	za
dob	doba	k1gFnPc2	doba
Jakuba	Jakub	k1gMnSc2	Jakub
jako	jako	k9	jako
pochutinu	pochutina	k1gFnSc4	pochutina
se	s	k7c7	s
solí	sůl	k1gFnSc7	sůl
<g/>
,	,	kIx,	,
pepřem	pepř	k1gInSc7	pepř
<g/>
,	,	kIx,	,
octem	ocet	k1gInSc7	ocet
a	a	k8xC	a
olejem	olej	k1gInSc7	olej
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgInPc1d2	starší
výhonky	výhonek	k1gInPc1	výhonek
se	se	k3xPyFc4	se
vařily	vařit	k5eAaImAgInP	vařit
a	a	k8xC	a
používaly	používat	k5eAaImAgInP	používat
jako	jako	k9	jako
chřest	chřest	k1gInSc4	chřest
<g/>
.	.	kIx.	.
</s>
<s>
Chmel	chmel	k1gInSc1	chmel
byl	být	k5eAaImAgInS	být
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
zeleninu	zelenina	k1gFnSc4	zelenina
povzbuzující	povzbuzující	k2eAgFnSc4d1	povzbuzující
chuť	chuť	k1gFnSc4	chuť
k	k	k7c3	k
jídlu	jídlo	k1gNnSc3	jídlo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
uklidňuje	uklidňovat	k5eAaImIp3nS	uklidňovat
nervy	nerv	k1gInPc4	nerv
strávníků	strávník	k1gMnPc2	strávník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Čech	Čechy	k1gFnPc2	Čechy
byl	být	k5eAaImAgInS	být
znám	znám	k2eAgMnSc1d1	znám
a	a	k8xC	a
pěstován	pěstován	k2eAgMnSc1d1	pěstován
od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
i	i	k9	i
planě	planě	k6eAd1	planě
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
většinou	většina	k1gFnSc7	většina
je	být	k5eAaImIp3nS	být
pěstován	pěstovat	k5eAaImNgMnS	pěstovat
v	v	k7c6	v
tradičních	tradiční	k2eAgFnPc6d1	tradiční
chmelařských	chmelařský	k2eAgFnPc6d1	chmelařská
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Žatecká	žatecký	k2eAgNnPc1d1	žatecké
<g/>
,	,	kIx,	,
Úštěcká	úštěcký	k2eAgNnPc1d1	Úštěcké
a	a	k8xC	a
Tršická	Tršický	k2eAgNnPc1d1	Tršický
<g/>
.	.	kIx.	.
<g/>
Asi	asi	k9	asi
nejznámější	známý	k2eAgNnPc1d3	nejznámější
z	z	k7c2	z
místních	místní	k2eAgMnPc2d1	místní
chmelů	chmel	k1gInPc2	chmel
je	být	k5eAaImIp3nS	být
žatecký	žatecký	k2eAgInSc1d1	žatecký
poloraný	poloraný	k2eAgInSc1d1	poloraný
červeňák	červeňák	k1gInSc1	červeňák
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
také	také	k9	také
získal	získat	k5eAaPmAgInS	získat
chráněné	chráněný	k2eAgNnSc4d1	chráněné
označení	označení	k1gNnSc4	označení
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ZEITHAMMER	ZEITHAMMER	kA	ZEITHAMMER
<g/>
,	,	kIx,	,
Leopold	Leopold	k1gMnSc1	Leopold
Maria	Maria	k1gFnSc1	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Chmel	Chmel	k1gMnSc1	Chmel
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
pěstován	pěstován	k2eAgMnSc1d1	pěstován
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
1890	[number]	k4	1890
<g/>
.	.	kIx.	.
102	[number]	k4	102
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
chmel	chmel	k1gInSc1	chmel
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
chmel	chmel	k1gInSc1	chmel
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Humulus	Humulus	k1gInSc1	Humulus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
