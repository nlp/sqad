<s>
Velbloud	velbloud	k1gMnSc1	velbloud
je	být	k5eAaImIp3nS	být
společné	společný	k2eAgNnSc4d1	společné
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
druhy	druh	k1gInPc4	druh
sudokopytníků	sudokopytník	k1gMnPc2	sudokopytník
<g/>
:	:	kIx,	:
velblouda	velbloud	k1gMnSc2	velbloud
dvouhrbého	dvouhrbý	k2eAgMnSc2d1	dvouhrbý
(	(	kIx(	(
<g/>
Camelus	Camelus	k1gInSc1	Camelus
bactrianus	bactrianus	k1gInSc1	bactrianus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvaného	zvaný	k2eAgInSc2d1	zvaný
též	též	k9	též
drabař	drabař	k1gMnSc1	drabař
<g/>
,	,	kIx,	,
a	a	k8xC	a
velblouda	velbloud	k1gMnSc2	velbloud
jednohrbého	jednohrbý	k2eAgMnSc2d1	jednohrbý
(	(	kIx(	(
<g/>
Camelus	Camelus	k1gInSc1	Camelus
dromedarius	dromedarius	k1gInSc1	dromedarius
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
dromedára	dromedár	k1gMnSc4	dromedár
<g/>
.	.	kIx.	.
</s>
