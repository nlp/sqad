<s>
Velbloud	velbloud	k1gMnSc1	velbloud
je	být	k5eAaImIp3nS	být
společné	společný	k2eAgNnSc4d1	společné
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
druhy	druh	k1gInPc4	druh
sudokopytníků	sudokopytník	k1gMnPc2	sudokopytník
<g/>
:	:	kIx,	:
velblouda	velbloud	k1gMnSc2	velbloud
dvouhrbého	dvouhrbý	k2eAgMnSc2d1	dvouhrbý
(	(	kIx(	(
<g/>
Camelus	Camelus	k1gInSc1	Camelus
bactrianus	bactrianus	k1gInSc1	bactrianus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvaného	zvaný	k2eAgInSc2d1	zvaný
též	též	k9	též
drabař	drabař	k1gMnSc1	drabař
<g/>
,	,	kIx,	,
a	a	k8xC	a
velblouda	velbloud	k1gMnSc2	velbloud
jednohrbého	jednohrbý	k2eAgMnSc2d1	jednohrbý
(	(	kIx(	(
<g/>
Camelus	Camelus	k1gInSc1	Camelus
dromedarius	dromedarius	k1gInSc1	dromedarius
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
dromedára	dromedár	k1gMnSc4	dromedár
<g/>
.	.	kIx.	.
</s>
<s>
Arabové	Arab	k1gMnPc1	Arab
rozeznávají	rozeznávat	k5eAaImIp3nP	rozeznávat
přes	přes	k7c4	přes
dvacet	dvacet	k4xCc4	dvacet
plemen	plemeno	k1gNnPc2	plemeno
velblouda	velbloud	k1gMnSc2	velbloud
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
slovo	slovo	k1gNnSc1	slovo
velbloud	velbloud	k1gMnSc1	velbloud
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
zajímavou	zajímavý	k2eAgFnSc4d1	zajímavá
etymologii	etymologie	k1gFnSc4	etymologie
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
jej	on	k3xPp3gMnSc4	on
často	často	k6eAd1	často
spojují	spojovat	k5eAaImIp3nP	spojovat
s	s	k7c7	s
adjektivem	adjektivum	k1gNnSc7	adjektivum
velký	velký	k2eAgInSc1d1	velký
a	a	k8xC	a
slovy	slovo	k1gNnPc7	slovo
bloud	bloud	k1gMnSc1	bloud
<g/>
,	,	kIx,	,
bloudit	bloudit	k5eAaImF	bloudit
či	či	k8xC	či
obluda	obluda	k1gFnSc1	obluda
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c4	o
zcela	zcela	k6eAd1	zcela
mylnou	mylný	k2eAgFnSc4d1	mylná
lidovou	lidový	k2eAgFnSc4d1	lidová
etymologii	etymologie	k1gFnSc4	etymologie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
do	do	k7c2	do
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
dostalo	dostat	k5eAaPmAgNnS	dostat
v	v	k7c6	v
období	období	k1gNnSc6	období
Stěhování	stěhování	k1gNnPc2	stěhování
národů	národ	k1gInPc2	národ
z	z	k7c2	z
gótštiny	gótština	k1gFnSc2	gótština
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
gótské	gótský	k2eAgNnSc1d1	gótské
slovo	slovo	k1gNnSc1	slovo
ulbandus	ulbandus	k1gInSc1	ulbandus
<g/>
,	,	kIx,	,
odvozené	odvozený	k2eAgInPc1d1	odvozený
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
názvu	název	k1gInSc2	název
slona	slon	k1gMnSc2	slon
elefas	elefasa	k1gFnPc2	elefasa
(	(	kIx(	(
<g/>
genitiv	genitiv	k1gInSc1	genitiv
-antos	ntos	k1gInSc1	-antos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gótové	Gót	k1gMnPc1	Gót
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Černomoří	Černomoří	k1gNnSc2	Černomoří
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
setkávali	setkávat	k5eAaImAgMnP	setkávat
s	s	k7c7	s
východními	východní	k2eAgMnPc7d1	východní
kočovníky	kočovník	k1gMnPc7	kočovník
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Alany	Alan	k1gMnPc4	Alan
a	a	k8xC	a
Huny	Hun	k1gMnPc4	Hun
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
chovali	chovat	k5eAaImAgMnP	chovat
velbloudy	velbloud	k1gMnPc4	velbloud
<g/>
.	.	kIx.	.
</s>
<s>
Gótové	Gót	k1gMnPc1	Gót
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
zvíře	zvíře	k1gNnSc4	zvíře
neměli	mít	k5eNaImAgMnP	mít
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
antické	antický	k2eAgFnSc2d1	antická
kultury	kultura	k1gFnSc2	kultura
znali	znát	k5eAaImAgMnP	znát
slony	slon	k1gMnPc4	slon
<g/>
.	.	kIx.	.
</s>
<s>
Přenesení	přenesení	k1gNnSc4	přenesení
názvu	název	k1gInSc2	název
slona	slon	k1gMnSc2	slon
na	na	k7c6	na
velblouda	velbloud	k1gMnSc2	velbloud
bylo	být	k5eAaImAgNnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
motivováno	motivovat	k5eAaBmNgNnS	motivovat
setkáním	setkání	k1gNnSc7	setkání
s	s	k7c7	s
velkými	velký	k2eAgFnPc7d1	velká
neznámými	známý	k2eNgFnPc7d1	neznámá
dopravními	dopravní	k2eAgFnPc7d1	dopravní
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Slovenské	slovenský	k2eAgNnSc1d1	slovenské
slovo	slovo	k1gNnSc1	slovo
ťava	ťav	k1gInSc2	ťav
má	mít	k5eAaImIp3nS	mít
turkický	turkický	k2eAgInSc4d1	turkický
původ	původ	k1gInSc4	původ
a	a	k8xC	a
do	do	k7c2	do
slovenštiny	slovenština	k1gFnSc2	slovenština
proniklo	proniknout	k5eAaPmAgNnS	proniknout
přes	přes	k7c4	přes
maďarštinu	maďarština	k1gFnSc4	maďarština
<g/>
.	.	kIx.	.
</s>
<s>
Latinský	latinský	k2eAgInSc1d1	latinský
název	název	k1gInSc1	název
Camelus	Camelus	k1gInSc1	Camelus
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
semitských	semitský	k2eAgInPc2d1	semitský
jazyků	jazyk	k1gInPc2	jazyk
-	-	kIx~	-
např.	např.	kA	např.
v	v	k7c6	v
hebrejštině	hebrejština	k1gFnSc6	hebrejština
a	a	k8xC	a
aramejštinš	aramejštinš	k1gMnSc1	aramejštinš
gamal	gamal	k1gMnSc1	gamal
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
dromedár	dromedár	k1gMnSc1	dromedár
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
.	.	kIx.	.
</s>
<s>
Velbloud	velbloud	k1gMnSc1	velbloud
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
vysoký	vysoký	k2eAgMnSc1d1	vysoký
<g/>
,	,	kIx,	,
statný	statný	k2eAgMnSc1d1	statný
přežvýkavec	přežvýkavec	k1gMnSc1	přežvýkavec
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
hlavou	hlava	k1gFnSc7	hlava
na	na	k7c6	na
mohutném	mohutný	k2eAgInSc6d1	mohutný
krku	krk	k1gInSc6	krk
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnPc4	výška
do	do	k7c2	do
dvou	dva	k4xCgNnPc2	dva
a	a	k8xC	a
půl	půl	k1xP	půl
metru	metr	k1gInSc2	metr
<g/>
,	,	kIx,	,
délky	délka	k1gFnSc2	délka
do	do	k7c2	do
tří	tři	k4xCgNnPc2	tři
a	a	k8xC	a
půl	půl	k6eAd1	půl
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
nerovnoměrná	rovnoměrný	k2eNgFnSc1d1	nerovnoměrná
<g/>
,	,	kIx,	,
na	na	k7c6	na
krku	krk	k1gInSc6	krk
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
hřívy	hříva	k1gFnSc2	hříva
<g/>
.	.	kIx.	.
</s>
<s>
Velbloud	velbloud	k1gMnSc1	velbloud
je	být	k5eAaImIp3nS	být
zásadně	zásadně	k6eAd1	zásadně
býložravý	býložravý	k2eAgInSc1d1	býložravý
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
vybíravý	vybíravý	k2eAgMnSc1d1	vybíravý
<g/>
.	.	kIx.	.
</s>
<s>
Velbloud	velbloud	k1gMnSc1	velbloud
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
lama	lama	k1gFnSc1	lama
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
světě	svět	k1gInSc6	svět
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
15	[number]	k4	15
milionů	milion	k4xCgInPc2	milion
domácích	domácí	k2eAgMnPc2d1	domácí
velbloudů	velbloud	k1gMnPc2	velbloud
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
západní	západní	k2eAgFnSc6d1	západní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mongolských	mongolský	k2eAgFnPc6d1	mongolská
pouštích	poušť	k1gFnPc6	poušť
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
malé	malý	k2eAgFnPc1d1	malá
skupinky	skupinka	k1gFnPc1	skupinka
divokých	divoký	k2eAgMnPc2d1	divoký
drabařů	drabař	k1gMnPc2	drabař
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
najdeme	najít	k5eAaPmIp1nP	najít
asi	asi	k9	asi
25	[number]	k4	25
tisíc	tisíc	k4xCgInPc2	tisíc
zdivočelých	zdivočelý	k2eAgMnPc2d1	zdivočelý
dromedárů	dromedár	k1gMnPc2	dromedár
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
pramenů	pramen	k1gInPc2	pramen
až	až	k9	až
700	[number]	k4	700
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
potomků	potomek	k1gMnPc2	potomek
velbloudů	velbloud	k1gMnPc2	velbloud
dovezených	dovezený	k2eAgMnPc2d1	dovezený
z	z	k7c2	z
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
.	.	kIx.	.
</s>
<s>
Velbloudi	velbloud	k1gMnPc1	velbloud
jsou	být	k5eAaImIp3nP	být
pověstní	pověstný	k2eAgMnPc1d1	pověstný
svou	svůj	k3xOyFgFnSc7	svůj
schopností	schopnost	k1gFnSc7	schopnost
přetrvávat	přetrvávat	k5eAaImF	přetrvávat
dlouho	dlouho	k6eAd1	dlouho
bez	bez	k7c2	bez
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Velbloud	velbloud	k1gMnSc1	velbloud
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
především	především	k9	především
pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
břemen	břemeno	k1gNnPc2	břemeno
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
také	také	k9	také
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
a	a	k8xC	a
mléko	mléko	k1gNnSc4	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Hodí	hodit	k5eAaPmIp3nS	hodit
se	se	k3xPyFc4	se
ale	ale	k9	ale
i	i	k9	i
k	k	k7c3	k
jízdě	jízda	k1gFnSc3	jízda
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Velbloudí	velbloudí	k2eAgFnSc1d1	velbloudí
vlna	vlna	k1gFnSc1	vlna
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
značné	značný	k2eAgFnPc4d1	značná
kvality	kvalita	k1gFnPc4	kvalita
zejména	zejména	k9	zejména
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Užitečný	užitečný	k2eAgInSc1d1	užitečný
je	být	k5eAaImIp3nS	být
i	i	k9	i
velbloudí	velbloudí	k2eAgInSc1d1	velbloudí
trus	trus	k1gInSc1	trus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
coby	coby	k?	coby
palivo	palivo	k1gNnSc4	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
domestifikovali	domestifikovat	k5eAaImAgMnP	domestifikovat
velbloudy	velbloud	k1gMnPc4	velbloud
před	před	k7c7	před
mnoha	mnoho	k4c7	mnoho
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
Brehm	Brehm	k1gMnSc1	Brehm
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
starověkým	starověký	k2eAgMnPc3d1	starověký
Egypťanům	Egypťan	k1gMnPc3	Egypťan
sloužil	sloužit	k5eAaImAgInS	sloužit
již	již	k6eAd1	již
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
Nové	Nové	k2eAgFnSc2d1	Nové
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
získali	získat	k5eAaPmAgMnP	získat
je	on	k3xPp3gNnSc4	on
zřejmě	zřejmě	k6eAd1	zřejmě
od	od	k7c2	od
semitských	semitský	k2eAgMnPc2d1	semitský
kočovníků	kočovník	k1gMnPc2	kočovník
<g/>
.	.	kIx.	.
</s>
<s>
Egypťané	Egypťan	k1gMnPc1	Egypťan
prý	prý	k9	prý
také	také	k9	také
naučili	naučit	k5eAaPmAgMnP	naučit
velblouda	velbloud	k1gMnSc4	velbloud
i	i	k8xC	i
jednoduchému	jednoduchý	k2eAgInSc3d1	jednoduchý
tanci	tanec	k1gInSc3	tanec
zvanému	zvaný	k2eAgInSc3d1	zvaný
kenken	kenken	k1gInSc4	kenken
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
je	být	k5eAaImIp3nS	být
velbloud	velbloud	k1gMnSc1	velbloud
často	často	k6eAd1	často
zmiňován	zmiňován	k2eAgMnSc1d1	zmiňován
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
jej	on	k3xPp3gMnSc4	on
Izraelité	izraelita	k1gMnPc1	izraelita
pokládali	pokládat	k5eAaImAgMnP	pokládat
za	za	k7c4	za
nečisté	čistý	k2eNgNnSc4d1	nečisté
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
Arabové	Arab	k1gMnPc1	Arab
velblouda	velbloud	k1gMnSc2	velbloud
za	za	k7c4	za
nečistého	nečistý	k1gMnSc4	nečistý
nepokládají	pokládat	k5eNaImIp3nP	pokládat
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
maso	maso	k1gNnSc1	maso
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
muslimy	muslim	k1gMnPc4	muslim
halal	halal	k1gInSc4	halal
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
armáda	armáda	k1gFnSc1	armáda
zakoupila	zakoupit	k5eAaPmAgFnS	zakoupit
roku	rok	k1gInSc2	rok
1856	[number]	k4	1856
ve	v	k7c6	v
Smyrně	Smyrna	k1gFnSc6	Smyrna
75	[number]	k4	75
velbloudů	velbloud	k1gMnPc2	velbloud
se	s	k7c7	s
záměrem	záměr	k1gInSc7	záměr
o	o	k7c4	o
jejich	jejich	k3xOp3gNnSc4	jejich
vojenské	vojenský	k2eAgNnSc4d1	vojenské
využití	využití	k1gNnSc4	využití
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
sloužili	sloužit	k5eAaImAgMnP	sloužit
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Konfederace	konfederace	k1gFnSc2	konfederace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
zájem	zájem	k1gInSc1	zájem
armády	armáda	k1gFnSc2	armáda
opadl	opadnout	k5eAaPmAgInS	opadnout
a	a	k8xC	a
dromedáři	dromedár	k1gMnPc1	dromedár
byli	být	k5eAaImAgMnP	být
dílem	dílem	k6eAd1	dílem
prodáni	prodán	k2eAgMnPc1d1	prodán
<g/>
,	,	kIx,	,
dílem	dílem	k6eAd1	dílem
zdivočeli	zdivočet	k5eAaPmAgMnP	zdivočet
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnPc1d1	poslední
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
přežili	přežít	k5eAaPmAgMnP	přežít
asi	asi	k9	asi
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
</s>
<s>
Velbloudi	velbloud	k1gMnPc1	velbloud
byli	být	k5eAaImAgMnP	být
také	také	k6eAd1	také
<g/>
,	,	kIx,	,
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
jinými	jiný	k2eAgMnPc7d1	jiný
domácími	domácí	k1gMnPc7	domácí
zvířaty	zvíře	k1gNnPc7	zvíře
(	(	kIx(	(
<g/>
králík	králík	k1gMnSc1	králík
<g/>
,	,	kIx,	,
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
kočka	kočka	k1gFnSc1	kočka
<g/>
,	,	kIx,	,
kůň	kůň	k1gMnSc1	kůň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
introdukováni	introdukován	k2eAgMnPc1d1	introdukován
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Velbloud	velbloud	k1gMnSc1	velbloud
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
rychlý	rychlý	k2eAgInSc1d1	rychlý
a	a	k8xC	a
vytrvalý	vytrvalý	k2eAgMnSc1d1	vytrvalý
běžec	běžec	k1gMnSc1	běžec
<g/>
,	,	kIx,	,
dokáže	dokázat	k5eAaPmIp3nS	dokázat
uběhnout	uběhnout	k5eAaPmF	uběhnout
až	až	k9	až
175	[number]	k4	175
km	km	kA	km
za	za	k7c4	za
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Stoupání	stoupání	k1gNnSc1	stoupání
mu	on	k3xPp3gMnSc3	on
ale	ale	k8xC	ale
činí	činit	k5eAaImIp3nS	činit
obtíže	obtíž	k1gFnPc4	obtíž
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
není	být	k5eNaImIp3nS	být
často	často	k6eAd1	často
vidět	vidět	k5eAaImF	vidět
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
velbloudi	velbloud	k1gMnPc1	velbloud
dvouhrbí	dvouhrbý	k2eAgMnPc1d1	dvouhrbý
někdy	někdy	k6eAd1	někdy
do	do	k7c2	do
hor	hora	k1gFnPc2	hora
pronikají	pronikat	k5eAaImIp3nP	pronikat
<g/>
.	.	kIx.	.
</s>
<s>
Velbloudi	velbloud	k1gMnPc1	velbloud
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
používali	používat	k5eAaImAgMnP	používat
hlavně	hlavně	k9	hlavně
k	k	k7c3	k
dopravě	doprava	k1gFnSc3	doprava
nákladů	náklad	k1gInPc2	náklad
a	a	k8xC	a
k	k	k7c3	k
jízdě	jízda	k1gFnSc3	jízda
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
k	k	k7c3	k
tahu	tah	k1gInSc3	tah
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
pouštních	pouštní	k2eAgFnPc6d1	pouštní
oblastech	oblast	k1gFnPc6	oblast
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
šíří	šířit	k5eAaImIp3nS	šířit
užívání	užívání	k1gNnSc1	užívání
terénních	terénní	k2eAgNnPc2d1	terénní
a	a	k8xC	a
nákladních	nákladní	k2eAgNnPc2d1	nákladní
aut	auto	k1gNnPc2	auto
<g/>
,	,	kIx,	,
však	však	k9	však
význam	význam	k1gInSc1	význam
velbloudů	velbloud	k1gMnPc2	velbloud
v	v	k7c6	v
dopravě	doprava	k1gFnSc6	doprava
pomalu	pomalu	k6eAd1	pomalu
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Maso	maso	k1gNnSc1	maso
mladých	mladý	k2eAgMnPc2d1	mladý
velbloudů	velbloud	k1gMnPc2	velbloud
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
největší	veliký	k2eAgFnSc4d3	veliký
lahůdku	lahůdka	k1gFnSc4	lahůdka
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
na	na	k7c6	na
Arabském	arabský	k2eAgInSc6d1	arabský
poostrově	poostrově	k6eAd1	poostrově
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
spíše	spíše	k9	spíše
velbloudi	velbloud	k1gMnPc1	velbloud
jednohrbí	jednohrbý	k2eAgMnPc1d1	jednohrbý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
velbloudi	velbloud	k1gMnPc1	velbloud
dvouhrbí	dvouhrbý	k2eAgMnPc1d1	dvouhrbý
<g/>
,	,	kIx,	,
velbloudí	velbloudí	k2eAgNnSc1d1	velbloudí
maso	maso	k1gNnSc1	maso
obvykle	obvykle	k6eAd1	obvykle
není	být	k5eNaImIp3nS	být
konzumováno	konzumován	k2eAgNnSc1d1	konzumováno
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
velbloudi	velbloud	k1gMnPc1	velbloud
bývají	bývat	k5eAaImIp3nP	bývat
pojmenováni	pojmenován	k2eAgMnPc1d1	pojmenován
a	a	k8xC	a
majitelé	majitel	k1gMnPc1	majitel
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
mají	mít	k5eAaImIp3nP	mít
silný	silný	k2eAgInSc4d1	silný
citový	citový	k2eAgInSc4d1	citový
vztah	vztah	k1gInSc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
nevylučuje	vylučovat	k5eNaImIp3nS	vylučovat
rituální	rituální	k2eAgInPc4d1	rituální
důvody	důvod	k1gInPc4	důvod
a	a	k8xC	a
pojídání	pojídání	k1gNnSc4	pojídání
velbloudů	velbloud	k1gMnPc2	velbloud
ukradených	ukradený	k2eAgFnPc2d1	ukradená
jiným	jiný	k2eAgMnPc3d1	jiný
kmenům	kmen	k1gInPc3	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Maso	maso	k1gNnSc4	maso
z	z	k7c2	z
introdukovaných	introdukovaný	k2eAgMnPc2d1	introdukovaný
velbloudů	velbloud	k1gMnPc2	velbloud
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
také	také	k9	také
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Jateční	jateční	k2eAgFnSc1d1	jateční
hmotnost	hmotnost	k1gFnSc1	hmotnost
velblouda	velbloud	k1gMnSc2	velbloud
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
450	[number]	k4	450
kilogramů	kilogram	k1gInPc2	kilogram
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jateční	jateční	k2eAgFnSc1d1	jateční
výtěžnost	výtěžnost	k1gFnSc1	výtěžnost
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
.	.	kIx.	.
</s>
<s>
Velbloudí	velbloudí	k2eAgNnSc1d1	velbloudí
mléko	mléko	k1gNnSc1	mléko
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
tučné	tučný	k2eAgNnSc4d1	tučné
a	a	k8xC	a
husté	hustý	k2eAgNnSc4d1	husté
<g/>
,	,	kIx,	,
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
také	také	k6eAd1	také
máslo	máslo	k1gNnSc1	máslo
<g/>
,	,	kIx,	,
jogurty	jogurt	k1gInPc1	jogurt
a	a	k8xC	a
sýry	sýr	k1gInPc1	sýr
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
mléka	mléko	k1gNnSc2	mléko
převažují	převažovat	k5eAaImIp3nP	převažovat
velbloudi	velbloud	k1gMnPc1	velbloud
jednohrbí	jednohrbý	k2eAgMnPc1d1	jednohrbý
<g/>
.	.	kIx.	.
</s>
<s>
Červené	Červené	k2eAgFnPc1d1	Červené
krvinky	krvinka	k1gFnPc1	krvinka
velbloudů	velbloud	k1gMnPc2	velbloud
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
velké	velký	k2eAgFnPc1d1	velká
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgMnPc2d1	jiný
savců	savec	k1gMnPc2	savec
nemají	mít	k5eNaImIp3nP	mít
tvar	tvar	k1gInSc4	tvar
piškotu	piškot	k1gInSc2	piškot
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
kulaté	kulatý	k2eAgInPc1d1	kulatý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
tak	tak	k9	tak
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
buňka	buňka	k1gFnSc1	buňka
mohla	moct	k5eAaImAgFnS	moct
řídit	řídit	k5eAaImF	řídit
poměrně	poměrně	k6eAd1	poměrně
složitý	složitý	k2eAgInSc4d1	složitý
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgMnSc3	jenž
může	moct	k5eAaImIp3nS	moct
velbloud	velbloud	k1gMnSc1	velbloud
dlouho	dlouho	k6eAd1	dlouho
žít	žít	k5eAaImF	žít
bez	bez	k7c2	bez
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
principem	princip	k1gInSc7	princip
je	být	k5eAaImIp3nS	být
nabobtnávání	nabobtnávání	k1gNnSc1	nabobtnávání
červených	červený	k2eAgFnPc2d1	červená
krvinek	krvinka	k1gFnPc2	krvinka
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
koncentraci	koncentrace	k1gFnSc6	koncentrace
minerálů	minerál	k1gInPc2	minerál
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
(	(	kIx(	(
<g/>
čím	co	k3yInSc7	co
více	hodně	k6eAd2	hodně
přijal	přijmout	k5eAaPmAgMnS	přijmout
velbloud	velbloud	k1gMnSc1	velbloud
tekutin	tekutina	k1gFnPc2	tekutina
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
menší	malý	k2eAgFnSc1d2	menší
bude	být	k5eAaImBp3nS	být
koncentrace	koncentrace	k1gFnSc1	koncentrace
minerálů	minerál	k1gInPc2	minerál
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
nabobtnané	nabobtnaný	k2eAgFnSc2d1	nabobtnaná
krvinky	krvinka	k1gFnSc2	krvinka
vodu	voda	k1gFnSc4	voda
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
obsaženou	obsažený	k2eAgFnSc4d1	obsažená
udrží	udržet	k5eAaPmIp3nP	udržet
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
koncentrace	koncentrace	k1gFnSc1	koncentrace
minerálů	minerál	k1gInPc2	minerál
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
nezvedne	zvednout	k5eNaPmIp3nS	zvednout
nad	nad	k7c4	nad
normální	normální	k2eAgFnSc4d1	normální
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Velbloud	velbloud	k1gMnSc1	velbloud
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
velbloud	velbloud	k1gMnSc1	velbloud
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Taxon	taxon	k1gInSc1	taxon
camelus	camelus	k1gInSc1	camelus
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
Brožura	brožura	k1gFnSc1	brožura
OSN	OSN	kA	OSN
o	o	k7c6	o
velbloudech	velbloud	k1gMnPc6	velbloud
a	a	k8xC	a
velbloudím	velbloudí	k2eAgNnSc6d1	velbloudí
mléce	mléko	k1gNnSc6	mléko
<g/>
:	:	kIx,	:
http://www.fao.org/DOCREP/003/X6528E/X6528E00.htm#TOC	[url]	k4	http://www.fao.org/DOCREP/003/X6528E/X6528E00.htm#TOC
http://www.ingema.net/in2001/clanek.php?id=344	[url]	k4	http://www.ingema.net/in2001/clanek.php?id=344
</s>
