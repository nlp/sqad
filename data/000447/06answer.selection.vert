<s>
Léčil	léčit	k5eAaImAgInS	léčit
anglickou	anglický	k2eAgFnSc4d1	anglická
královnu	královna	k1gFnSc4	královna
Alžbětu	Alžběta	k1gFnSc4	Alžběta
I.	I.	kA	I.
a	a	k8xC	a
po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
smrti	smrt	k1gFnSc6	smrt
i	i	k8xC	i
krále	král	k1gMnSc2	král
Jakuba	Jakub	k1gMnSc2	Jakub
I.	I.	kA	I.
V	v	k7c6	v
odborných	odborný	k2eAgInPc6d1	odborný
kruzích	kruh	k1gInPc6	kruh
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
otce	otec	k1gMnSc4	otec
vědy	věda	k1gFnSc2	věda
o	o	k7c6	o
elektřině	elektřina	k1gFnSc6	elektřina
a	a	k8xC	a
magnetismu	magnetismus	k1gInSc6	magnetismus
<g/>
.	.	kIx.	.
</s>
