<s>
William	William	k1gInSc1	William
Gilbert	Gilbert	k1gMnSc1	Gilbert
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1544	[number]	k4	1544
<g/>
,	,	kIx,	,
Colchester	Colchester	k1gInSc1	Colchester
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1603	[number]	k4	1603
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
vědec	vědec	k1gMnSc1	vědec
<g/>
.	.	kIx.	.
</s>
<s>
Léčil	léčit	k5eAaImAgInS	léčit
anglickou	anglický	k2eAgFnSc4d1	anglická
královnu	královna	k1gFnSc4	královna
Alžbětu	Alžběta	k1gFnSc4	Alžběta
I.	I.	kA	I.
a	a	k8xC	a
po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
smrti	smrt	k1gFnSc6	smrt
i	i	k8xC	i
krále	král	k1gMnSc2	král
Jakuba	Jakub	k1gMnSc2	Jakub
I.	I.	kA	I.
V	v	k7c6	v
odborných	odborný	k2eAgInPc6d1	odborný
kruzích	kruh	k1gInPc6	kruh
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
otce	otec	k1gMnSc4	otec
vědy	věda	k1gFnSc2	věda
o	o	k7c6	o
elektřině	elektřina	k1gFnSc6	elektřina
a	a	k8xC	a
magnetismu	magnetismus	k1gInSc6	magnetismus
<g/>
.	.	kIx.	.
</s>
<s>
Názvy	název	k1gInPc1	název
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
elektřina	elektřina	k1gFnSc1	elektřina
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
elektrická	elektrický	k2eAgFnSc1d1	elektrická
síla	síla	k1gFnSc1	síla
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
přitažlivost	přitažlivost	k1gFnSc1	přitažlivost
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
"	"	kIx"	"
pocházejí	pocházet	k5eAaImIp3nP	pocházet
právě	právě	k9	právě
od	od	k7c2	od
něho	on	k3xPp3gMnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
také	také	k9	také
existenci	existence	k1gFnSc4	existence
vztahu	vztah	k1gInSc2	vztah
mezi	mezi	k7c7	mezi
statickou	statický	k2eAgFnSc7d1	statická
elektřinou	elektřina	k1gFnSc7	elektřina
a	a	k8xC	a
magnetismem	magnetismus	k1gInSc7	magnetismus
a	a	k8xC	a
publikoval	publikovat	k5eAaBmAgMnS	publikovat
první	první	k4xOgInSc4	první
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
knihu	kniha	k1gFnSc4	kniha
postihující	postihující	k2eAgInSc1d1	postihující
fenomén	fenomén	k1gInSc1	fenomén
elektřiny	elektřina	k1gFnSc2	elektřina
a	a	k8xC	a
magnetismu	magnetismus	k1gInSc2	magnetismus
-	-	kIx~	-
De	De	k?	De
Magnete	magnet	k1gInSc5	magnet
<g/>
,	,	kIx,	,
Magneticisque	Magneticisqu	k1gFnPc1	Magneticisqu
Corporibus	Corporibus	k1gInSc1	Corporibus
et	et	k?	et
de	de	k?	de
Magno	Magno	k6eAd1	Magno
Magnete	magnet	k1gInSc5	magnet
Tellure	tellur	k1gInSc5	tellur
(	(	kIx(	(
<g/>
O	o	k7c6	o
magnetu	magnet	k1gInSc6	magnet
<g/>
,	,	kIx,	,
magnetická	magnetický	k2eAgNnPc1d1	magnetické
tělesa	těleso	k1gNnPc1	těleso
a	a	k8xC	a
veliký	veliký	k2eAgInSc1d1	veliký
magnet	magnet	k1gInSc1	magnet
zemský	zemský	k2eAgInSc1d1	zemský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
princip	princip	k1gInSc1	princip
elektromagnetismu	elektromagnetismus	k1gInSc2	elektromagnetismus
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
základem	základ	k1gInSc7	základ
mnoha	mnoho	k4c2	mnoho
moderních	moderní	k2eAgFnPc2d1	moderní
komunikačních	komunikační	k2eAgFnPc2d1	komunikační
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
např.	např.	kA	např.
telefonu	telefon	k1gInSc2	telefon
<g/>
,	,	kIx,	,
rádia	rádio	k1gNnSc2	rádio
<g/>
,	,	kIx,	,
televize	televize	k1gFnSc2	televize
či	či	k8xC	či
mobilního	mobilní	k2eAgInSc2d1	mobilní
telefonu	telefon	k1gInSc2	telefon
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
současníkem	současník	k1gMnSc7	současník
Tychona	Tychon	k1gMnSc2	Tychon
Brahe	Brah	k1gFnSc2	Brah
a	a	k8xC	a
Johannese	Johannese	k1gFnSc2	Johannese
Keplera	Kepler	k1gMnSc2	Kepler
<g/>
.	.	kIx.	.
</s>
<s>
Značka	značka	k1gFnSc1	značka
Gb	Gb	k1gFnSc2	Gb
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
nepoužívaná	používaný	k2eNgFnSc1d1	nepoužívaná
jednotka	jednotka	k1gFnSc1	jednotka
magnetického	magnetický	k2eAgNnSc2d1	magnetické
či	či	k8xC	či
magnetomotorického	magnetomotorický	k2eAgNnSc2d1	magnetomotorický
napětí	napětí	k1gNnSc2	napětí
<g/>
,	,	kIx,	,
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
po	po	k7c6	po
Williamu	William	k1gInSc6	William
Gilbertovi	Gilbert	k1gMnSc3	Gilbert
<g/>
.	.	kIx.	.
1	[number]	k4	1
Gb	Gb	k1gFnPc2	Gb
=	=	kIx~	=
0,795775	[number]	k4	0,795775
A	a	k9	a
</s>
