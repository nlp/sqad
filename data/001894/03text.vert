<s>
Sinice	sinice	k1gFnPc1	sinice
(	(	kIx(	(
<g/>
Cyanobacteria	Cyanobacterium	k1gNnPc1	Cyanobacterium
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
Cyanophyta	Cyanophyta	k1gFnSc1	Cyanophyta
či	či	k8xC	či
Cyanoprokaryota	Cyanoprokaryota	k1gFnSc1	Cyanoprokaryota
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kmen	kmen	k1gInSc1	kmen
nebo	nebo	k8xC	nebo
oddělení	oddělení	k1gNnSc1	oddělení
(	(	kIx(	(
<g/>
záleží	záležet	k5eAaImIp3nS	záležet
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
bakteriologické	bakteriologický	k2eAgNnSc4d1	bakteriologické
či	či	k8xC	či
botanické	botanický	k2eAgNnSc4d1	botanické
pojetí	pojetí	k1gNnSc4	pojetí
<g/>
)	)	kIx)	)
gramnegativních	gramnegativní	k2eAgFnPc2d1	gramnegativní
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
se	se	k3xPyFc4	se
schopností	schopnost	k1gFnSc7	schopnost
fotosyntézy	fotosyntéza	k1gFnPc1	fotosyntéza
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
vzniká	vznikat	k5eAaImIp3nS	vznikat
kyslík	kyslík	k1gInSc1	kyslík
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
oxygenní	oxygenní	k2eAgInSc1d1	oxygenní
typ	typ	k1gInSc1	typ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
sinný	sinný	k1gMnSc1	sinný
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
modrý	modrý	k2eAgInSc1d1	modrý
<g/>
.	.	kIx.	.
</s>
<s>
Buňky	buňka	k1gFnPc1	buňka
sinic	sinice	k1gFnPc2	sinice
jsou	být	k5eAaImIp3nP	být
jednobuněčné	jednobuněčný	k2eAgFnPc1d1	jednobuněčná
či	či	k8xC	či
vláknité	vláknitý	k2eAgFnPc1d1	vláknitá
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
modrozeleně	modrozeleně	k6eAd1	modrozeleně
zbarvené	zbarvený	k2eAgFnPc1d1	zbarvená
a	a	k8xC	a
v	v	k7c6	v
mnohých	mnohý	k2eAgInPc6d1	mnohý
ohledech	ohled	k1gInPc6	ohled
typicky	typicky	k6eAd1	typicky
prokaryotické	prokaryotický	k2eAgFnPc1d1	prokaryotická
<g/>
:	:	kIx,	:
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
kruhovou	kruhový	k2eAgFnSc4d1	kruhová
molekulu	molekula	k1gFnSc4	molekula
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
bakteriální	bakteriální	k2eAgInSc1d1	bakteriální
typ	typ	k1gInSc1	typ
ribozomů	ribozom	k1gInPc2	ribozom
a	a	k8xC	a
chybí	chybět	k5eAaImIp3nS	chybět
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
složitější	složitý	k2eAgFnSc1d2	složitější
membránové	membránový	k2eAgFnPc4d1	membránová
struktury	struktura	k1gFnPc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Fotosyntetická	fotosyntetický	k2eAgNnPc1d1	fotosyntetické
barviva	barvivo	k1gNnPc1	barvivo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
ve	v	k7c6	v
speciálních	speciální	k2eAgInPc6d1	speciální
útvarech	útvar	k1gInPc6	útvar
<g/>
,	,	kIx,	,
fykobilizomech	fykobilizom	k1gInPc6	fykobilizom
nebo	nebo	k8xC	nebo
thylakoidech	thylakoid	k1gInPc6	thylakoid
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hlavním	hlavní	k2eAgInPc3d1	hlavní
pigmentům	pigment	k1gInPc3	pigment
účastnícím	účastnící	k2eAgInPc3d1	účastnící
se	se	k3xPyFc4	se
fotosyntézy	fotosyntéza	k1gFnPc1	fotosyntéza
patří	patřit	k5eAaImIp3nP	patřit
chlorofyl	chlorofyl	k1gInSc4	chlorofyl
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
typu	typ	k1gInSc2	typ
a	a	k8xC	a
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
b	b	k?	b
<g/>
,	,	kIx,	,
c	c	k0	c
nebo	nebo	k8xC	nebo
d	d	k?	d
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
allofykocyanin	allofykocyanin	k2eAgInSc1d1	allofykocyanin
<g/>
,	,	kIx,	,
fykocyanin	fykocyanin	k2eAgInSc1d1	fykocyanin
<g/>
,	,	kIx,	,
fykoerythrin	fykoerythrin	k1gInSc1	fykoerythrin
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Sinice	sinice	k1gFnPc1	sinice
se	se	k3xPyFc4	se
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
nepohlavně	pohlavně	k6eNd1	pohlavně
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buněčným	buněčný	k2eAgNnSc7d1	buněčné
dělením	dělení	k1gNnSc7	dělení
či	či	k8xC	či
fragmentací	fragmentace	k1gFnPc2	fragmentace
vláken	vlákno	k1gNnPc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
hojně	hojně	k6eAd1	hojně
ve	v	k7c6	v
vodním	vodní	k2eAgNnSc6d1	vodní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
a	a	k8xC	a
mnohdy	mnohdy	k6eAd1	mnohdy
také	také	k9	také
v	v	k7c6	v
extrémních	extrémní	k2eAgFnPc6d1	extrémní
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
pouště	poušť	k1gFnPc4	poušť
či	či	k8xC	či
polární	polární	k2eAgFnPc4d1	polární
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
také	také	k9	také
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
do	do	k7c2	do
symbiotických	symbiotický	k2eAgInPc2d1	symbiotický
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
endosymbioticky	endosymbioticky	k6eAd1	endosymbioticky
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
plastidů	plastid	k1gInPc2	plastid
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
případy	případ	k1gInPc7	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sinice	sinice	k1gFnPc1	sinice
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
svému	svůj	k1gMnSc3	svůj
hostiteli	hostitel	k1gMnSc3	hostitel
fixovat	fixovat	k5eAaImF	fixovat
dusík	dusík	k1gInSc4	dusík
či	či	k8xC	či
uhlík	uhlík	k1gInSc4	uhlík
<g/>
.	.	kIx.	.
</s>
<s>
Sinice	sinice	k1gFnSc1	sinice
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
z	z	k7c2	z
anaerobních	anaerobní	k2eAgFnPc2d1	anaerobní
fotosyntetizujících	fotosyntetizující	k2eAgFnPc2d1	fotosyntetizující
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
dnešní	dnešní	k2eAgFnPc1d1	dnešní
purpurové	purpurový	k2eAgFnPc1d1	purpurová
bakterie	bakterie	k1gFnPc1	bakterie
či	či	k8xC	či
chlorobakterie	chlorobakterie	k1gFnPc1	chlorobakterie
(	(	kIx(	(
<g/>
Chloroflexi	Chloroflexe	k1gFnSc4	Chloroflexe
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
známé	známý	k2eAgInPc1d1	známý
důkazy	důkaz	k1gInPc1	důkaz
o	o	k7c6	o
existenci	existence	k1gFnSc6	existence
sinic	sinice	k1gFnPc2	sinice
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
fosílií	fosílie	k1gFnPc2	fosílie
jsou	být	k5eAaImIp3nP	být
staré	stará	k1gFnSc2	stará
3,5	[number]	k4	3,5
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
a	a	k8xC	a
pochází	pocházet	k5eAaImIp3nS	pocházet
především	především	k6eAd1	především
z	z	k7c2	z
formace	formace	k1gFnSc2	formace
Apex	apex	k1gInSc1	apex
Chert	Chert	k1gInSc1	Chert
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
prekambrijské	prekambrijský	k2eAgInPc1d1	prekambrijský
nálezy	nález	k1gInPc1	nález
tzv.	tzv.	kA	tzv.
stromatolitů	stromatolit	k1gInPc2	stromatolit
možná	možná	k6eAd1	možná
představují	představovat	k5eAaImIp3nP	představovat
vůbec	vůbec	k9	vůbec
nejstarší	starý	k2eAgInPc1d3	nejstarší
nálezy	nález	k1gInPc1	nález
buněčných	buněčný	k2eAgInPc2d1	buněčný
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
studie	studie	k1gFnPc1	studie
však	však	k9	však
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc1	tento
nálezy	nález	k1gInPc1	nález
jsou	být	k5eAaImIp3nP	být
abiotického	abiotický	k2eAgInSc2d1	abiotický
původu	původ	k1gInSc2	původ
a	a	k8xC	a
představují	představovat	k5eAaImIp3nP	představovat
jen	jen	k9	jen
složité	složitý	k2eAgFnPc1d1	složitá
struktury	struktura	k1gFnPc1	struktura
vzniklé	vzniklý	k2eAgFnPc1d1	vzniklá
devitrifikací	devitrifikace	k1gFnSc7	devitrifikace
horniny	hornina	k1gFnSc2	hornina
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
.	.	kIx.	.
</s>
<s>
Lépe	dobře	k6eAd2	dobře
zachované	zachovaný	k2eAgInPc1d1	zachovaný
fosilní	fosilní	k2eAgInPc1d1	fosilní
nálezy	nález	k1gInPc1	nález
sinic	sinice	k1gFnPc2	sinice
pochází	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
období	období	k1gNnSc2	období
mezi	mezi	k7c7	mezi
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
0,9	[number]	k4	0,9
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhled	k1gInSc7	vzhled
jsou	být	k5eAaImIp3nP	být
identické	identický	k2eAgInPc1d1	identický
s	s	k7c7	s
dnešními	dnešní	k2eAgFnPc7d1	dnešní
sinicemi	sinice	k1gFnPc7	sinice
a	a	k8xC	a
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
nějaká	nějaký	k3yIgFnSc1	nějaký
fosílie	fosílie	k1gFnSc1	fosílie
zařazena	zařadit	k5eAaPmNgFnS	zařadit
do	do	k7c2	do
určitého	určitý	k2eAgInSc2d1	určitý
současného	současný	k2eAgInSc2d1	současný
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
před	před	k7c4	před
daný	daný	k2eAgInSc4d1	daný
rodový	rodový	k2eAgInSc4d1	rodový
název	název	k1gInSc4	název
se	se	k3xPyFc4	se
často	často	k6eAd1	často
dává	dávat	k5eAaImIp3nS	dávat
předpona	předpona	k1gFnSc1	předpona
"	"	kIx"	"
<g/>
paleo	palea	k1gFnSc5	palea
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Udává	udávat	k5eAaImIp3nS	udávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
sinice	sinice	k1gFnPc1	sinice
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
řádů	řád	k1gInPc2	řád
Chroococcales	Chroococcalesa	k1gFnPc2	Chroococcalesa
a	a	k8xC	a
Oscillatoriales	Oscillatorialesa	k1gFnPc2	Oscillatorialesa
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
kambria	kambrium	k1gNnSc2	kambrium
(	(	kIx(	(
<g/>
před	před	k7c7	před
cca	cca	kA	cca
600	[number]	k4	600
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
sinice	sinice	k1gFnSc2	sinice
dominantními	dominantní	k2eAgInPc7d1	dominantní
organismy	organismus	k1gInPc7	organismus
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
úsek	úsek	k1gInSc1	úsek
geologické	geologický	k2eAgFnSc2d1	geologická
historie	historie	k1gFnSc2	historie
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
věk	věk	k1gInSc1	věk
sinic	sinice	k1gFnPc2	sinice
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
také	také	k9	také
díky	díky	k7c3	díky
sinicím	sinice	k1gFnPc3	sinice
postupně	postupně	k6eAd1	postupně
zvyšoval	zvyšovat	k5eAaImAgInS	zvyšovat
obsah	obsah	k1gInSc1	obsah
kyslíku	kyslík	k1gInSc2	kyslík
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
evoluční	evoluční	k2eAgFnSc4d1	evoluční
historii	historie	k1gFnSc4	historie
sinic	sinice	k1gFnPc2	sinice
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vzhled	vzhled	k1gInSc1	vzhled
jejich	jejich	k3xOp3gFnPc2	jejich
buněk	buňka	k1gFnPc2	buňka
téměř	téměř	k6eAd1	téměř
nezměnil	změnit	k5eNaPmAgMnS	změnit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ke	k	k7c3	k
genetickým	genetický	k2eAgFnPc3d1	genetická
změnám	změna	k1gFnPc3	změna
dochází	docházet	k5eAaImIp3nS	docházet
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
označena	označit	k5eAaPmNgFnS	označit
jako	jako	k8xC	jako
stagnující	stagnující	k2eAgFnSc1d1	stagnující
evoluce	evoluce	k1gFnSc1	evoluce
<g/>
.	.	kIx.	.
</s>
<s>
Plastidy	plastid	k1gInPc1	plastid
<g/>
,	,	kIx,	,
organely	organela	k1gFnPc1	organela
mnohých	mnohý	k2eAgInPc2d1	mnohý
eukaryotických	eukaryotický	k2eAgInPc2d1	eukaryotický
organismů	organismus	k1gInPc2	organismus
(	(	kIx(	(
<g/>
především	především	k9	především
různé	různý	k2eAgFnPc1d1	různá
řasy	řasa	k1gFnPc1	řasa
a	a	k8xC	a
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnohých	mnohý	k2eAgInPc6d1	mnohý
ohledech	ohled	k1gInPc6	ohled
připomínají	připomínat	k5eAaImIp3nP	připomínat
sinice	sinice	k1gFnPc1	sinice
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tzv.	tzv.	kA	tzv.
endosymbiotická	endosymbiotický	k2eAgFnSc1d1	endosymbiotická
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
podpořena	podpořit	k5eAaPmNgFnS	podpořit
různými	různý	k2eAgFnPc7d1	různá
strukturálními	strukturální	k2eAgFnPc7d1	strukturální
a	a	k8xC	a
genetickými	genetický	k2eAgFnPc7d1	genetická
podobnostmi	podobnost	k1gFnPc7	podobnost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
plastidy	plastid	k1gInPc1	plastid
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
z	z	k7c2	z
sinic	sinice	k1gFnPc2	sinice
pohlcených	pohlcený	k2eAgFnPc2d1	pohlcená
eukaryotními	eukaryotní	k2eAgFnPc7d1	eukaryotní
buňkami	buňka	k1gFnPc7	buňka
asi	asi	k9	asi
před	před	k7c7	před
1,5	[number]	k4	1,5
miliardami	miliarda	k4xCgFnPc7	miliarda
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Sinice	sinice	k1gFnPc1	sinice
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jako	jako	k9	jako
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
shluky	shluk	k1gInPc1	shluk
buněk	buňka	k1gFnPc2	buňka
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
vlákna	vlákno	k1gNnPc1	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
stélka	stélka	k1gFnSc1	stélka
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
obvykle	obvykle	k6eAd1	obvykle
označuje	označovat	k5eAaImIp3nS	označovat
buď	buď	k8xC	buď
jako	jako	k9	jako
kokální	kokální	k2eAgMnSc1d1	kokální
či	či	k8xC	či
trichální	trichální	k2eAgMnSc1d1	trichální
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
vláknité	vláknitý	k2eAgFnPc1d1	vláknitá
formy	forma	k1gFnPc1	forma
mívají	mívat	k5eAaImIp3nP	mívat
specializované	specializovaný	k2eAgFnPc1d1	specializovaná
buňky	buňka	k1gFnPc1	buňka
heterocyty	heterocyt	k1gInPc4	heterocyt
(	(	kIx(	(
<g/>
či	či	k8xC	či
též	též	k9	též
heterocysty	heterocysta	k1gFnSc2	heterocysta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
fixace	fixace	k1gFnSc1	fixace
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
dusíku	dusík	k1gInSc2	dusík
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
akinety	akinet	k1gInPc4	akinet
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
klidové	klidový	k2eAgFnPc1d1	klidová
buňky	buňka	k1gFnPc1	buňka
určené	určený	k2eAgFnPc1d1	určená
k	k	k7c3	k
přetrvání	přetrvání	k1gNnSc3	přetrvání
nepříznivých	příznivý	k2eNgNnPc2d1	nepříznivé
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Buňka	buňka	k1gFnSc1	buňka
sinic	sinice	k1gFnPc2	sinice
je	být	k5eAaImIp3nS	být
prokaryotického	prokaryotický	k2eAgInSc2d1	prokaryotický
typu	typ	k1gInSc2	typ
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
velikosti	velikost	k1gFnSc2	velikost
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
mikrometrů	mikrometr	k1gInPc2	mikrometr
<g/>
,	,	kIx,	,
jen	jen	k9	jen
vzácně	vzácně	k6eAd1	vzácně
více	hodně	k6eAd2	hodně
(	(	kIx(	(
<g/>
např.	např.	kA	např.
buňka	buňka	k1gFnSc1	buňka
Chroococcus	Chroococcus	k1gMnSc1	Chroococcus
giganteus	giganteus	k1gMnSc1	giganteus
má	mít	k5eAaImIp3nS	mít
až	až	k9	až
60	[number]	k4	60
mikrometrů	mikrometr	k1gInPc2	mikrometr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
spatřit	spatřit	k5eAaPmF	spatřit
kolonie	kolonie	k1gFnPc4	kolonie
sinic	sinice	k1gFnPc2	sinice
<g/>
.	.	kIx.	.
</s>
<s>
Sinice	sinice	k1gFnPc1	sinice
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
kolonie	kolonie	k1gFnPc1	kolonie
jsou	být	k5eAaImIp3nP	být
zbarveny	zbarvit	k5eAaPmNgFnP	zbarvit
nejčastěji	často	k6eAd3	často
modrozeleně	modrozeleně	k6eAd1	modrozeleně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
barev	barva	k1gFnPc2	barva
od	od	k7c2	od
blankytně	blankytně	k6eAd1	blankytně
modré	modrý	k2eAgFnSc2d1	modrá
přes	přes	k7c4	přes
malachitově	malachitově	k6eAd1	malachitově
zelenou	zelený	k2eAgFnSc4d1	zelená
<g/>
,	,	kIx,	,
žlutou	žlutý	k2eAgFnSc4d1	žlutá
<g/>
,	,	kIx,	,
červenou	červený	k2eAgFnSc4d1	červená
až	až	k6eAd1	až
po	po	k7c4	po
černou	černá	k1gFnSc4	černá
<g/>
.	.	kIx.	.
</s>
<s>
Sinice	sinice	k1gFnPc1	sinice
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
gramnegativní	gramnegativní	k2eAgFnSc2d1	gramnegativní
bakterie	bakterie	k1gFnSc2	bakterie
a	a	k8xC	a
jako	jako	k9	jako
takové	takový	k3xDgMnPc4	takový
Gramovou	gramový	k2eAgFnSc7d1	gramová
metodou	metoda	k1gFnSc7	metoda
získávají	získávat	k5eAaImIp3nP	získávat
růžové	růžový	k2eAgNnSc4d1	růžové
zabarvení	zabarvení	k1gNnSc4	zabarvení
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tyto	tento	k3xDgFnPc1	tento
bakterie	bakterie	k1gFnPc1	bakterie
mají	mít	k5eAaImIp3nP	mít
specifický	specifický	k2eAgInSc4d1	specifický
typ	typ	k1gInSc4	typ
buněčné	buněčný	k2eAgFnSc2d1	buněčná
stěny	stěna	k1gFnSc2	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Sinice	sinice	k1gFnPc1	sinice
jako	jako	k8xC	jako
gramnegativní	gramnegativní	k2eAgFnPc1d1	gramnegativní
bakterie	bakterie	k1gFnPc1	bakterie
mají	mít	k5eAaImIp3nP	mít
totiž	totiž	k9	totiž
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
dvě	dva	k4xCgFnPc4	dva
plazmatické	plazmatický	k2eAgFnPc4d1	plazmatická
membrány	membrána	k1gFnPc4	membrána
(	(	kIx(	(
<g/>
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
a	a	k8xC	a
vnější	vnější	k2eAgInPc4d1	vnější
<g/>
)	)	kIx)	)
a	a	k8xC	a
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
poměrně	poměrně	k6eAd1	poměrně
tenkou	tenký	k2eAgFnSc4d1	tenká
vrstvu	vrstva	k1gFnSc4	vrstva
peptidoglykanu	peptidoglykan	k1gInSc2	peptidoglykan
(	(	kIx(	(
<g/>
murein	murein	k1gInSc1	murein
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
tvoří	tvořit	k5eAaImIp3nS	tvořit
pevnou	pevný	k2eAgFnSc4d1	pevná
složku	složka	k1gFnSc4	složka
celé	celý	k2eAgFnSc2d1	celá
buněčné	buněčný	k2eAgFnSc2d1	buněčná
stěny	stěna	k1gFnSc2	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
některé	některý	k3yIgFnPc4	některý
specifické	specifický	k2eAgFnPc4d1	specifická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
buněčné	buněčný	k2eAgFnSc2d1	buněčná
stěny	stěna	k1gFnSc2	stěna
sinic	sinice	k1gFnPc2	sinice
sinice	sinice	k1gFnSc2	sinice
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
gramnegativních	gramnegativní	k2eAgFnPc2d1	gramnegativní
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
a	a	k8xC	a
sinice	sinice	k1gFnSc1	sinice
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
zdají	zdát	k5eAaPmIp3nP	zdát
být	být	k5eAaImF	být
směsí	směs	k1gFnPc2	směs
vlastností	vlastnost	k1gFnPc2	vlastnost
gramnegativní	gramnegativní	k2eAgInSc1d1	gramnegativní
a	a	k8xC	a
grampozitivní	grampozitivní	k2eAgFnPc1d1	grampozitivní
buňky	buňka	k1gFnPc1	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
běžné	běžný	k2eAgFnPc1d1	běžná
gramnegativní	gramnegativní	k2eAgFnPc1d1	gramnegativní
bakterie	bakterie	k1gFnPc1	bakterie
mají	mít	k5eAaImIp3nP	mít
buněčnou	buněčný	k2eAgFnSc4d1	buněčná
stěnu	stěna	k1gFnSc4	stěna
o	o	k7c6	o
šířce	šířka	k1gFnSc6	šířka
jen	jen	k9	jen
asi	asi	k9	asi
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
nanometrů	nanometr	k1gInPc2	nanometr
<g/>
,	,	kIx,	,
u	u	k7c2	u
jednobuněčných	jednobuněčný	k2eAgFnPc2d1	jednobuněčná
sinic	sinice	k1gFnPc2	sinice
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
asi	asi	k9	asi
10	[number]	k4	10
nanometrů	nanometr	k1gInPc2	nanometr
<g/>
,	,	kIx,	,
u	u	k7c2	u
vláknitých	vláknitý	k2eAgFnPc2d1	vláknitá
sinic	sinice	k1gFnPc2	sinice
15	[number]	k4	15
<g/>
-	-	kIx~	-
<g/>
35	[number]	k4	35
nanometrů	nanometr	k1gInPc2	nanometr
a	a	k8xC	a
vzácně	vzácně	k6eAd1	vzácně
až	až	k9	až
700	[number]	k4	700
nanometrů	nanometr	k1gInPc2	nanometr
(	(	kIx(	(
<g/>
u	u	k7c2	u
sinice	sinice	k1gFnSc2	sinice
Oscillatoria	Oscillatorium	k1gNnSc2	Oscillatorium
princeps	princepsa	k1gFnPc2	princepsa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
chemické	chemický	k2eAgFnSc6d1	chemická
stránce	stránka	k1gFnSc6	stránka
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
peptidy	peptid	k1gInPc4	peptid
vytvářející	vytvářející	k2eAgInPc1d1	vytvářející
zmíněná	zmíněný	k2eAgNnPc4d1	zmíněné
příčná	příčný	k2eAgNnPc4d1	příčné
spojení	spojení	k1gNnPc4	spojení
spíše	spíše	k9	spíše
charakteristická	charakteristický	k2eAgNnPc1d1	charakteristické
pro	pro	k7c4	pro
gramnegativní	gramnegativní	k2eAgFnPc4d1	gramnegativní
bakterie	bakterie	k1gFnPc4	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
kyselina	kyselina	k1gFnSc1	kyselina
teichoová	teichoový	k2eAgFnSc1d1	teichoový
<g/>
,	,	kIx,	,
typická	typický	k2eAgFnSc1d1	typická
složka	složka	k1gFnSc1	složka
stěn	stěna	k1gFnPc2	stěna
grampozitivních	grampozitivní	k2eAgFnPc2d1	grampozitivní
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
u	u	k7c2	u
sinic	sinice	k1gFnPc2	sinice
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
.	.	kIx.	.
</s>
<s>
Membrána	membrána	k1gFnSc1	membrána
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnohé	mnohý	k2eAgInPc4d1	mnohý
transportní	transportní	k2eAgInPc4d1	transportní
kanály	kanál	k1gInPc4	kanál
umožňující	umožňující	k2eAgInSc4d1	umožňující
pasivní	pasivní	k2eAgInSc4d1	pasivní
či	či	k8xC	či
aktivní	aktivní	k2eAgInSc4d1	aktivní
transport	transport	k1gInSc4	transport
látek	látka	k1gFnPc2	látka
přes	přes	k7c4	přes
stěnu	stěna	k1gFnSc4	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
pasivní	pasivní	k2eAgInPc1d1	pasivní
kanály	kanál	k1gInPc1	kanál
z	z	k7c2	z
proteinu	protein	k1gInSc2	protein
porinu	porin	k1gInSc2	porin
či	či	k8xC	či
různé	různý	k2eAgInPc4d1	různý
specifické	specifický	k2eAgInPc4d1	specifický
kanály	kanál	k1gInPc4	kanál
určené	určený	k2eAgInPc4d1	určený
pro	pro	k7c4	pro
pomalu	pomalu	k6eAd1	pomalu
difundující	difundující	k2eAgFnPc4d1	difundující
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
vláknitých	vláknitý	k2eAgFnPc2d1	vláknitá
sinic	sinice	k1gFnPc2	sinice
byly	být	k5eAaImAgInP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
v	v	k7c6	v
příčných	příčný	k2eAgFnPc6d1	příčná
přehrádkách	přehrádka	k1gFnPc6	přehrádka
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
buňkami	buňka	k1gFnPc7	buňka
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
kanály	kanál	k1gInPc7	kanál
připomínající	připomínající	k2eAgFnSc2d1	připomínající
plazmodezmy	plazmodezma	k1gFnSc2	plazmodezma
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
ven	ven	k6eAd1	ven
od	od	k7c2	od
vnější	vnější	k2eAgFnSc2d1	vnější
plazmatické	plazmatický	k2eAgFnSc2d1	plazmatická
membrány	membrána	k1gFnSc2	membrána
navíc	navíc	k6eAd1	navíc
mají	mít	k5eAaImIp3nP	mít
sinice	sinice	k1gFnPc4	sinice
zpravidla	zpravidla	k6eAd1	zpravidla
slizovou	slizový	k2eAgFnSc4d1	slizová
vrstvu	vrstva	k1gFnSc4	vrstva
(	(	kIx(	(
<g/>
glykokalyx	glykokalyx	k1gInSc4	glykokalyx
<g/>
)	)	kIx)	)
složenou	složený	k2eAgFnSc4d1	složená
z	z	k7c2	z
lipopolysacharidů	lipopolysacharid	k1gInPc2	lipopolysacharid
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
mívá	mívat	k5eAaImIp3nS	mívat
fibrilární	fibrilární	k2eAgFnSc4d1	fibrilární
(	(	kIx(	(
<g/>
vláknitou	vláknitý	k2eAgFnSc4d1	vláknitá
<g/>
)	)	kIx)	)
strukturu	struktura	k1gFnSc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
více	hodně	k6eAd2	hodně
a	a	k8xC	a
jindy	jindy	k6eAd1	jindy
méně	málo	k6eAd2	málo
a	a	k8xC	a
v	v	k7c6	v
určitých	určitý	k2eAgInPc6d1	určitý
případech	případ	k1gInPc6	případ
může	moct	k5eAaImIp3nS	moct
tvořit	tvořit	k5eAaImF	tvořit
silnou	silný	k2eAgFnSc4d1	silná
homogenní	homogenní	k2eAgFnSc4d1	homogenní
či	či	k8xC	či
vrstevnatě	vrstevnatě	k6eAd1	vrstevnatě
uspořádanou	uspořádaný	k2eAgFnSc4d1	uspořádaná
pochvu	pochva	k1gFnSc4	pochva
kolem	kolem	k7c2	kolem
celé	celý	k2eAgFnSc2d1	celá
buňky	buňka	k1gFnSc2	buňka
(	(	kIx(	(
<g/>
a	a	k8xC	a
tento	tento	k3xDgInSc4	tento
obal	obal	k1gInSc4	obal
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
ještě	ještě	k6eAd1	ještě
navíc	navíc	k6eAd1	navíc
zbarvený	zbarvený	k2eAgInSc1d1	zbarvený
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
sinic	sinice	k1gFnPc2	sinice
nikdy	nikdy	k6eAd1	nikdy
nebyly	být	k5eNaImAgInP	být
nalezeny	nalézt	k5eAaBmNgInP	nalézt
bičíky	bičík	k1gInPc1	bičík
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
mnohé	mnohý	k2eAgFnPc1d1	mnohá
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
zástupci	zástupce	k1gMnPc1	zástupce
řádu	řád	k1gInSc2	řád
Oscillatoriales	Oscillatoriales	k1gInSc1	Oscillatoriales
<g/>
)	)	kIx)	)
dokáží	dokázat	k5eAaPmIp3nP	dokázat
po	po	k7c6	po
povrchu	povrch	k1gInSc6	povrch
aktivně	aktivně	k6eAd1	aktivně
pohybovat	pohybovat	k5eAaImF	pohybovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
je	být	k5eAaImIp3nS	být
produkováno	produkován	k2eAgNnSc1d1	produkováno
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
slizu	sliz	k1gInSc2	sliz
<g/>
,	,	kIx,	,
skutečný	skutečný	k2eAgInSc1d1	skutečný
hnací	hnací	k2eAgInSc1d1	hnací
motor	motor	k1gInSc1	motor
však	však	k9	však
představují	představovat	k5eAaImIp3nP	představovat
svazky	svazek	k1gInPc4	svazek
stažitelných	stažitelný	k2eAgFnPc2d1	stažitelná
bílkovinných	bílkovinný	k2eAgFnPc2d1	bílkovinná
vláken	vlákna	k1gFnPc2	vlákna
umístěných	umístěný	k2eAgFnPc2d1	umístěná
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
klouzají	klouzat	k5eAaImIp3nP	klouzat
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
však	však	k9	však
projevují	projevovat	k5eAaImIp3nP	projevovat
i	i	k9	i
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
typ	typ	k1gInSc4	typ
rotačního	rotační	k2eAgInSc2d1	rotační
pohybu	pohyb	k1gInSc2	pohyb
(	(	kIx(	(
<g/>
který	který	k3yIgMnSc1	který
dal	dát	k5eAaPmAgInS	dát
jméno	jméno	k1gNnSc4	jméno
například	například	k6eAd1	například
českému	český	k2eAgInSc3d1	český
rodu	rod	k1gInSc3	rod
Oscillatoria	Oscillatorium	k1gNnSc2	Oscillatorium
<g/>
,	,	kIx,	,
drkalka	drkalka	k1gFnSc1	drkalka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
protoplastu	protoplast	k1gInSc6	protoplast
(	(	kIx(	(
<g/>
vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
prostoru	prostor	k1gInSc6	prostor
<g/>
)	)	kIx)	)
sinic	sinice	k1gFnPc2	sinice
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
eukaryotickými	eukaryotický	k2eAgFnPc7d1	eukaryotická
buňkami	buňka	k1gFnPc7	buňka
takové	takový	k3xDgInPc1	takový
množství	množství	k1gNnSc4	množství
organel	organela	k1gFnPc2	organela
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
dva	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
cytoplazmy	cytoplazma	k1gFnSc2	cytoplazma
<g/>
:	:	kIx,	:
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
buněk	buňka	k1gFnPc2	buňka
bývá	bývat	k5eAaImIp3nS	bývat
výrazně	výrazně	k6eAd1	výrazně
barevná	barevný	k2eAgFnSc1d1	barevná
chromatoplazma	chromatoplazma	k1gFnSc1	chromatoplazma
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc1d1	obsahující
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
fotosyntetických	fotosyntetický	k2eAgNnPc2d1	fotosyntetické
barviv	barvivo	k1gNnPc2	barvivo
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
uvnitř	uvnitř	k6eAd1	uvnitř
je	být	k5eAaImIp3nS	být
centroplazma	centroplazma	k1gFnSc1	centroplazma
neboli	neboli	k8xC	neboli
centroplazmatická	centroplazmatický	k2eAgFnSc1d1	centroplazmatický
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
převládá	převládat	k5eAaImIp3nS	převládat
sinicová	sinicový	k2eAgNnPc4d1	sinicový
DNA	dno	k1gNnPc4	dno
<g/>
,	,	kIx,	,
ribozomy	ribozom	k1gInPc4	ribozom
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Ribozomy	Ribozom	k1gInPc1	Ribozom
sinic	sinice	k1gFnPc2	sinice
jsou	být	k5eAaImIp3nP	být
prokaryotního	prokaryotní	k2eAgInSc2d1	prokaryotní
typu	typ	k1gInSc2	typ
<g/>
:	:	kIx,	:
jejich	jejich	k3xOp3gFnSc1	jejich
malá	malý	k2eAgFnSc1d1	malá
podjednotka	podjednotka	k1gFnSc1	podjednotka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
16S	[number]	k4	16S
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
velká	velký	k2eAgFnSc1d1	velká
podjednotka	podjednotka	k1gFnSc1	podjednotka
23S	[number]	k4	23S
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
DNA	DNA	kA	DNA
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
jedinou	jediný	k2eAgFnSc4d1	jediná
kruhovitou	kruhovitý	k2eAgFnSc4d1	kruhovitá
molekulu	molekula	k1gFnSc4	molekula
DNA	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
nazývanou	nazývaný	k2eAgFnSc7d1	nazývaná
nukleoid	nukleoid	k1gInSc4	nukleoid
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
histony	histon	k1gInPc4	histon
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
genomu	genom	k1gInSc2	genom
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
asi	asi	k9	asi
1,7	[number]	k4	1,7
milionu	milion	k4xCgInSc2	milion
párů	pár	k1gInPc2	pár
bází	báze	k1gFnPc2	báze
(	(	kIx(	(
<g/>
Prochlorococcus	Prochlorococcus	k1gInSc1	Prochlorococcus
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c6	po
asi	asi	k9	asi
8,9	[number]	k4	8,9
milionu	milion	k4xCgInSc2	milion
párů	pár	k1gInPc2	pár
bází	báze	k1gFnPc2	báze
(	(	kIx(	(
<g/>
Nostoc	Nostoc	k1gFnSc1	Nostoc
punctiforme	punctiform	k1gInSc5	punctiform
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
sinice	sinice	k1gFnPc1	sinice
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
více	hodně	k6eAd2	hodně
kopií	kopie	k1gFnSc7	kopie
své	svůj	k3xOyFgFnPc4	svůj
genetické	genetický	k2eAgFnPc4d1	genetická
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
haploidní	haploidní	k2eAgMnPc1d1	haploidní
<g/>
.	.	kIx.	.
</s>
<s>
Dodatečný	dodatečný	k2eAgInSc4d1	dodatečný
genetický	genetický	k2eAgInSc4d1	genetický
materiál	materiál	k1gInSc4	materiál
mohou	moct	k5eAaImIp3nP	moct
také	také	k9	také
představovat	představovat	k5eAaImF	představovat
plazmidy	plazmida	k1gFnPc4	plazmida
<g/>
,	,	kIx,	,
malé	malý	k2eAgFnPc4d1	malá
kruhové	kruhový	k2eAgFnPc4d1	kruhová
molekuly	molekula	k1gFnPc4	molekula
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Sinice	sinice	k1gFnPc1	sinice
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgNnSc4d1	schopné
předávání	předávání	k1gNnSc4	předávání
částí	část	k1gFnSc7	část
své	svůj	k3xOyFgFnPc4	svůj
genetické	genetický	k2eAgFnPc4d1	genetická
informace	informace	k1gFnPc4	informace
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
buňky	buňka	k1gFnSc2	buňka
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
horizontální	horizontální	k2eAgInSc1d1	horizontální
genový	genový	k2eAgInSc1d1	genový
transfer	transfer	k1gInSc1	transfer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
buněčné	buněčný	k2eAgFnPc1d1	buněčná
struktury	struktura	k1gFnPc1	struktura
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
u	u	k7c2	u
sinic	sinice	k1gFnPc2	sinice
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
jejich	jejich	k3xOp3gInSc7	jejich
fotosyntetickým	fotosyntetický	k2eAgInSc7d1	fotosyntetický
způsobem	způsob	k1gInSc7	způsob
výživy	výživa	k1gFnSc2	výživa
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgInPc1d1	významný
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
thylakoidy	thylakoid	k1gInPc7	thylakoid
<g/>
,	,	kIx,	,
membránou	membrána	k1gFnSc7	membrána
obalené	obalený	k2eAgInPc1d1	obalený
měchýřky	měchýřek	k1gInPc1	měchýřek
uložené	uložený	k2eAgInPc1d1	uložený
podél	podél	k7c2	podél
cytoplazmatické	cytoplazmatický	k2eAgFnSc2d1	cytoplazmatická
membrány	membrána	k1gFnSc2	membrána
či	či	k8xC	či
prorůstající	prorůstající	k2eAgFnPc1d1	prorůstající
skrz	skrz	k6eAd1	skrz
naskrz	naskrz	k6eAd1	naskrz
celou	celý	k2eAgFnSc7d1	celá
buňkou	buňka	k1gFnSc7	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Fotosyntéze	fotosyntéza	k1gFnSc3	fotosyntéza
mimoto	mimoto	k6eAd1	mimoto
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
i	i	k9	i
tzv.	tzv.	kA	tzv.
fykobilizomy	fykobilizom	k1gInPc7	fykobilizom
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
vodní	vodní	k2eAgFnPc1d1	vodní
sinice	sinice	k1gFnPc1	sinice
tvoří	tvořit	k5eAaImIp3nP	tvořit
i	i	k9	i
válcovité	válcovitý	k2eAgInPc1d1	válcovitý
měchýřky	měchýřek	k1gInPc1	měchýřek
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
až	až	k9	až
1000	[number]	k4	1000
nanometrů	nanometr	k1gInPc2	nanometr
a	a	k8xC	a
naplněné	naplněný	k2eAgNnSc4d1	naplněné
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
vznášení	vznášení	k1gNnSc4	vznášení
buněk	buňka	k1gFnPc2	buňka
ve	v	k7c6	v
vodním	vodní	k2eAgInSc6d1	vodní
sloupci	sloupec	k1gInSc6	sloupec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
taxonů	taxon	k1gInPc2	taxon
sinic	sinice	k1gFnPc2	sinice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesný	přesný	k2eAgInSc1d1	přesný
počet	počet	k1gInSc1	počet
nelze	lze	k6eNd1	lze
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
.	.	kIx.	.
</s>
<s>
Kalina	Kalina	k1gMnSc1	Kalina
a	a	k8xC	a
Váňa	Váňa	k1gMnSc1	Váňa
například	například	k6eAd1	například
uvádí	uvádět	k5eAaImIp3nS	uvádět
150	[number]	k4	150
rodů	rod	k1gInPc2	rod
s	s	k7c7	s
2000	[number]	k4	2000
druhy	druh	k1gInPc7	druh
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
počet	počet	k1gInSc4	počet
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
silně	silně	k6eAd1	silně
podhodnocený	podhodnocený	k2eAgInSc1d1	podhodnocený
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
značné	značný	k2eAgFnSc3d1	značná
proměnlivosti	proměnlivost	k1gFnSc3	proměnlivost
(	(	kIx(	(
<g/>
fenoplasticitě	fenoplasticita	k1gFnSc3	fenoplasticita
<g/>
)	)	kIx)	)
sinic	sinice	k1gFnPc2	sinice
vůbec	vůbec	k9	vůbec
nepřistupuje	přistupovat	k5eNaImIp3nS	přistupovat
k	k	k7c3	k
popisování	popisování	k1gNnSc3	popisování
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
užívají	užívat	k5eAaImIp3nP	užívat
se	se	k3xPyFc4	se
jen	jen	k9	jen
rody	rod	k1gInPc1	rod
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
morfologické	morfologický	k2eAgFnPc4d1	morfologická
odchylky	odchylka	k1gFnPc4	odchylka
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rodu	rod	k1gInSc2	rod
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
používá	používat	k5eAaImIp3nS	používat
termín	termín	k1gInSc1	termín
morfotyp	morfotyp	k1gInSc1	morfotyp
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
byly	být	k5eAaImAgFnP	být
sinice	sinice	k1gFnPc1	sinice
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
nižší	nízký	k2eAgFnPc4d2	nižší
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
dosud	dosud	k6eAd1	dosud
upomíná	upomínat	k5eAaImIp3nS	upomínat
termín	termín	k1gInSc1	termín
blue-green	bluereen	k2eAgInSc1d1	blue-green
algae	algae	k1gInSc1	algae
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
historický	historický	k2eAgInSc1d1	historický
název	název	k1gInSc1	název
sinné	sinný	k2eAgFnSc2d1	sinný
řasy	řasa	k1gFnSc2	řasa
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
latinský	latinský	k2eAgInSc4d1	latinský
název	název	k1gInSc4	název
Cyanophyceae	Cyanophycea	k1gFnSc2	Cyanophycea
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
dnešní	dnešní	k2eAgInSc1d1	dnešní
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
sinice	sinice	k1gFnSc1	sinice
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
neutrální	neutrální	k2eAgFnSc1d1	neutrální
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
sinný	sinný	k1gMnSc1	sinný
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
modrý	modrý	k2eAgInSc1d1	modrý
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
určování	určování	k1gNnSc6	určování
sinic	sinice	k1gFnPc2	sinice
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
hledí	hledět	k5eAaImIp3nS	hledět
na	na	k7c4	na
typ	typ	k1gInSc4	typ
stélky	stélka	k1gFnSc2	stélka
<g/>
,	,	kIx,	,
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
velikost	velikost	k1gFnSc4	velikost
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
navíc	navíc	k6eAd1	navíc
uspořádání	uspořádání	k1gNnSc1	uspořádání
buněk	buňka	k1gFnPc2	buňka
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
a	a	k8xC	a
přítomnost	přítomnost	k1gFnSc4	přítomnost
slizu	sliz	k1gInSc2	sliz
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vláknitých	vláknitý	k2eAgFnPc2d1	vláknitá
kolonií	kolonie	k1gFnPc2	kolonie
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
přítomnost	přítomnost	k1gFnSc1	přítomnost
slizové	slizový	k2eAgFnSc2d1	slizová
pochvy	pochva	k1gFnSc2	pochva
<g/>
,	,	kIx,	,
způsob	způsob	k1gInSc1	způsob
propojení	propojení	k1gNnSc2	propojení
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
tvar	tvar	k1gInSc4	tvar
vlákna	vlákno	k1gNnSc2	vlákno
a	a	k8xC	a
případně	případně	k6eAd1	případně
typ	typ	k1gInSc1	typ
jeho	jeho	k3xOp3gNnSc2	jeho
větvení	větvení	k1gNnSc2	větvení
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
také	také	k9	také
přítomnost	přítomnost	k1gFnSc1	přítomnost
heterocytů	heterocyt	k1gInPc2	heterocyt
a	a	k8xC	a
akinet	akineta	k1gFnPc2	akineta
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
mikroskopickým	mikroskopický	k2eAgNnPc3d1	mikroskopické
a	a	k8xC	a
biochemických	biochemický	k2eAgFnPc2d1	biochemická
studiím	studio	k1gNnPc3	studio
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
sinice	sinice	k1gFnPc4	sinice
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
podskupin	podskupina	k1gFnPc2	podskupina
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
získaly	získat	k5eAaPmAgFnP	získat
nejen	nejen	k6eAd1	nejen
botanické	botanický	k2eAgFnPc1d1	botanická
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
druhé	druhý	k4xOgNnSc1	druhý
<g/>
,	,	kIx,	,
bakteriologické	bakteriologický	k2eAgNnSc1d1	bakteriologické
názvosloví	názvosloví	k1gNnSc1	názvosloví
<g/>
.	.	kIx.	.
</s>
<s>
Botanické	botanický	k2eAgNnSc1d1	botanické
názvosloví	názvosloví	k1gNnSc1	názvosloví
ustanovuje	ustanovovat	k5eAaImIp3nS	ustanovovat
tzv.	tzv.	kA	tzv.
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
kód	kód	k1gInSc4	kód
botanické	botanický	k2eAgFnSc2d1	botanická
nomenklatury	nomenklatura	k1gFnSc2	nomenklatura
(	(	kIx(	(
<g/>
ICBN	ICBN	kA	ICBN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
klasifikaci	klasifikace	k1gFnSc6	klasifikace
bakterií	bakterie	k1gFnPc2	bakterie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
bakteriologický	bakteriologický	k2eAgInSc1d1	bakteriologický
kód	kód	k1gInSc1	kód
(	(	kIx(	(
<g/>
ICNB	ICNB	kA	ICNB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
a	a	k8xC	a
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
přešlo	přejít	k5eAaPmAgNnS	přejít
z	z	k7c2	z
botanického	botanický	k2eAgInSc2d1	botanický
na	na	k7c4	na
bakteriologické	bakteriologický	k2eAgNnSc4d1	bakteriologické
názvosloví	názvosloví	k1gNnSc4	názvosloví
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dosud	dosud	k6eAd1	dosud
bylo	být	k5eAaImAgNnS	být
pod	pod	k7c7	pod
platným	platný	k2eAgNnSc7d1	platné
bakteriologickým	bakteriologický	k2eAgNnSc7d1	bakteriologické
názvoslovím	názvosloví	k1gNnSc7	názvosloví
publikováno	publikovat	k5eAaBmNgNnS	publikovat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k4c1	málo
druhů	druh	k1gInPc2	druh
sinic	sinice	k1gFnPc2	sinice
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
významnou	významný	k2eAgFnSc7d1	významná
změnou	změna	k1gFnSc7	změna
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
dřívější	dřívější	k2eAgInPc1d1	dřívější
prochlorofyty	prochlorofyt	k1gInPc1	prochlorofyt
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c2	za
sinice	sinice	k1gFnSc2	sinice
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
mají	mít	k5eAaImIp3nP	mít
některé	některý	k3yIgFnPc4	některý
specifické	specifický	k2eAgFnPc4d1	specifická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Klasifikace	klasifikace	k1gFnSc1	klasifikace
sinic	sinice	k1gFnPc2	sinice
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Cavalier-Smith	Cavalier-Smith	k1gMnSc1	Cavalier-Smith
zařadil	zařadit	k5eAaPmAgMnS	zařadit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
oddělení	oddělení	k1gNnPc2	oddělení
sinic	sinice	k1gFnPc2	sinice
do	do	k7c2	do
velké	velký	k2eAgFnSc2d1	velká
skupiny	skupina	k1gFnSc2	skupina
Glycobacteria	Glycobacterium	k1gNnSc2	Glycobacterium
a	a	k8xC	a
naznačil	naznačit	k5eAaPmAgMnS	naznačit
jejich	jejich	k3xOp3gFnSc4	jejich
příbuznost	příbuznost	k1gFnSc4	příbuznost
zejména	zejména	k9	zejména
s	s	k7c7	s
kmeny	kmen	k1gInPc7	kmen
Firmicutes	Firmicutesa	k1gFnPc2	Firmicutesa
a	a	k8xC	a
Actinobacteria	Actinobacterium	k1gNnSc2	Actinobacterium
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
sinice	sinice	k1gFnSc1	sinice
na	na	k7c4	na
skupiny	skupina	k1gFnPc4	skupina
Gloeobacteria	Gloeobacterium	k1gNnSc2	Gloeobacterium
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
rod	rod	k1gInSc4	rod
Gloeobacter	Gloeobactra	k1gFnPc2	Gloeobactra
<g/>
,	,	kIx,	,
neobsahující	obsahující	k2eNgFnPc4d1	neobsahující
thylakoidy	thylakoida	k1gFnPc4	thylakoida
<g/>
)	)	kIx)	)
a	a	k8xC	a
Phycobacteria	Phycobacterium	k1gNnSc2	Phycobacterium
(	(	kIx(	(
<g/>
ostatní	ostatní	k2eAgFnPc1d1	ostatní
sinice	sinice	k1gFnPc1	sinice
včetně	včetně	k7c2	včetně
prochlorofytů	prochlorofyt	k1gInPc2	prochlorofyt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
jmenovanou	jmenovaný	k2eAgFnSc4d1	jmenovaná
skupinu	skupina	k1gFnSc4	skupina
Cavalier-Smith	Cavalier-Smith	k1gInSc1	Cavalier-Smith
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
na	na	k7c4	na
pět	pět	k4xCc4	pět
obvyklých	obvyklý	k2eAgInPc2d1	obvyklý
řádů	řád	k1gInPc2	řád
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
jsou	být	k5eAaImIp3nP	být
uznávány	uznávat	k5eAaImNgInP	uznávat
i	i	k8xC	i
botaniky	botanika	k1gFnPc1	botanika
<g/>
:	:	kIx,	:
Chroobacteria	Chroobacterium	k1gNnPc1	Chroobacterium
Chroococcales	Chroococcalesa	k1gFnPc2	Chroococcalesa
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
prochlorofytů	prochlorofyt	k1gInPc2	prochlorofyt
<g/>
)	)	kIx)	)
Pleurocapsales	Pleurocapsales	k1gInSc1	Pleurocapsales
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
řazena	řadit	k5eAaImNgFnS	řadit
do	do	k7c2	do
Chroococcales	Chroococcalesa	k1gFnPc2	Chroococcalesa
<g/>
)	)	kIx)	)
Oscillatoriales	Oscillatorialesa	k1gFnPc2	Oscillatorialesa
Hormogoneae	Hormogonea	k1gInSc2	Hormogonea
Nostocales	Nostocales	k1gInSc4	Nostocales
Stigonematales	Stigonematales	k1gInSc1	Stigonematales
Systém	systém	k1gInSc1	systém
NCBI	NCBI	kA	NCBI
prakticky	prakticky	k6eAd1	prakticky
přejímá	přejímat	k5eAaImIp3nS	přejímat
Cavalier-Smithovo	Cavalier-Smithův	k2eAgNnSc4d1	Cavalier-Smithův
uspořádání	uspořádání	k1gNnSc4	uspořádání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
rozdílem	rozdíl	k1gInSc7	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
figuruje	figurovat	k5eAaImIp3nS	figurovat
skupina	skupina	k1gFnSc1	skupina
Prochlorales	Prochloralesa	k1gFnPc2	Prochloralesa
jako	jako	k9	jako
samostatný	samostatný	k2eAgInSc4d1	samostatný
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
velmi	velmi	k6eAd1	velmi
odlišné	odlišný	k2eAgNnSc1d1	odlišné
je	být	k5eAaImIp3nS	být
pojetí	pojetí	k1gNnSc1	pojetí
systému	systém	k1gInSc2	systém
sinic	sinice	k1gFnPc2	sinice
v	v	k7c6	v
publikaci	publikace	k1gFnSc6	publikace
Bergey	Bergea	k1gFnSc2	Bergea
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Manual	Manual	k1gInSc1	Manual
of	of	k?	of
Systematic	Systematice	k1gFnPc2	Systematice
Bacteriology	Bacteriolog	k1gMnPc4	Bacteriolog
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
knize	kniha	k1gFnSc6	kniha
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
kmen	kmen	k1gInSc1	kmen
sinice	sinice	k1gFnSc2	sinice
na	na	k7c4	na
pět	pět	k4xCc4	pět
sekcí	sekce	k1gFnPc2	sekce
(	(	kIx(	(
<g/>
I-V	I-V	k1gFnSc1	I-V
<g/>
)	)	kIx)	)
bez	bez	k7c2	bez
taxonomického	taxonomický	k2eAgNnSc2d1	taxonomické
označení	označení	k1gNnSc2	označení
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
určitých	určitý	k2eAgInPc6d1	určitý
ohledech	ohled	k1gInPc6	ohled
korespondují	korespondovat	k5eAaImIp3nP	korespondovat
se	s	k7c7	s
systémem	systém	k1gInSc7	systém
Cavalier-Smithe	Cavalier-Smith	k1gInSc2	Cavalier-Smith
<g/>
.	.	kIx.	.
</s>
<s>
Zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
systém	systém	k1gInSc1	systém
pěti	pět	k4xCc2	pět
sekcí	sekce	k1gFnPc2	sekce
vyjmenovává	vyjmenovávat	k5eAaImIp3nS	vyjmenovávat
jen	jen	k9	jen
rody	rod	k1gInPc1	rod
sinic	sinice	k1gFnPc2	sinice
<g/>
,	,	kIx,	,
druhy	druh	k1gInPc4	druh
podle	podle	k7c2	podle
autora	autor	k1gMnSc2	autor
u	u	k7c2	u
sinic	sinice	k1gFnPc2	sinice
nelze	lze	k6eNd1	lze
rozlišit	rozlišit	k5eAaPmF	rozlišit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
přiklání	přiklánět	k5eAaImIp3nS	přiklánět
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
typu	typ	k1gInSc3	typ
klasifikace	klasifikace	k1gFnSc2	klasifikace
i	i	k9	i
další	další	k2eAgMnPc1d1	další
autoři	autor	k1gMnPc1	autor
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
či	či	k8xC	či
onak	onak	k6eAd1	onak
<g/>
,	,	kIx,	,
molekulárně	molekulárně	k6eAd1	molekulárně
biologické	biologický	k2eAgFnSc2d1	biologická
studie	studie	k1gFnSc2	studie
některé	některý	k3yIgInPc1	některý
navrhované	navrhovaný	k2eAgInPc1d1	navrhovaný
taxony	taxon	k1gInPc1	taxon
nepotvrdily	potvrdit	k5eNaPmAgInP	potvrdit
a	a	k8xC	a
označují	označovat	k5eAaImIp3nP	označovat
je	on	k3xPp3gNnSc4	on
za	za	k7c4	za
nepřirozené	přirozený	k2eNgNnSc4d1	nepřirozené
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
vývoje	vývoj	k1gInSc2	vývoj
života	život	k1gInSc2	život
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
metabolického	metabolický	k2eAgNnSc2d1	metabolické
hlediska	hledisko	k1gNnSc2	hledisko
jsou	být	k5eAaImIp3nP	být
sinice	sinice	k1gFnPc1	sinice
fototrofní	fototrofní	k2eAgFnPc1d1	fototrofní
a	a	k8xC	a
autotrofní	autotrofní	k2eAgFnPc1d1	autotrofní
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
většina	většina	k1gFnSc1	většina
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
zkráceně	zkráceně	k6eAd1	zkráceně
fotoautotrofové	fotoautotrofový	k2eAgNnSc1d1	fotoautotrofový
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
se	se	k3xPyFc4	se
především	především	k6eAd1	především
fotosyntézou	fotosyntéza	k1gFnSc7	fotosyntéza
oxygenního	oxygenní	k2eAgInSc2d1	oxygenní
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
donorem	donor	k1gInSc7	donor
elektronů	elektron	k1gInPc2	elektron
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
je	být	k5eAaImIp3nS	být
fixován	fixován	k2eAgInSc1d1	fixován
na	na	k7c4	na
organické	organický	k2eAgFnPc4d1	organická
sloučeniny	sloučenina	k1gFnPc4	sloučenina
a	a	k8xC	a
jako	jako	k9	jako
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
produkt	produkt	k1gInSc1	produkt
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
kyslík	kyslík	k1gInSc1	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
reakce	reakce	k1gFnSc1	reakce
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
rovnicí	rovnice	k1gFnSc7	rovnice
<g/>
:	:	kIx,	:
6	[number]	k4	6
CO2	CO2	k1gFnPc2	CO2
+	+	kIx~	+
12	[number]	k4	12
H2O	H2O	k1gMnSc1	H2O
→	→	k?	→
C6H12O6	C6H12O6	k1gMnSc1	C6H12O6
+	+	kIx~	+
6	[number]	k4	6
O2	O2	k1gFnPc2	O2
+	+	kIx~	+
6	[number]	k4	6
H2O	H2O	k1gFnPc2	H2O
</s>
<s>
Centrem	centrum	k1gNnSc7	centrum
fotosyntetických	fotosyntetický	k2eAgFnPc2d1	fotosyntetická
reakcí	reakce	k1gFnPc2	reakce
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
sinic	sinice	k1gFnPc2	sinice
tzv.	tzv.	kA	tzv.
thylakoidy	thylakoida	k1gFnSc2	thylakoida
(	(	kIx(	(
<g/>
vyjma	vyjma	k7c2	vyjma
primitivní	primitivní	k2eAgFnSc2d1	primitivní
sinice	sinice	k1gFnSc2	sinice
Gloeobacter	Gloeobactra	k1gFnPc2	Gloeobactra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	on	k3xPp3gInPc4	on
nemá	mít	k5eNaImIp3nS	mít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
struktury	struktura	k1gFnPc1	struktura
totiž	totiž	k9	totiž
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
vlastní	vlastní	k2eAgNnPc4d1	vlastní
fotosyntetická	fotosyntetický	k2eAgNnPc4d1	fotosyntetické
barviva	barvivo	k1gNnPc4	barvivo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
nutná	nutný	k2eAgNnPc1d1	nutné
pro	pro	k7c4	pro
přeměnu	přeměna	k1gFnSc4	přeměna
světelné	světelný	k2eAgFnSc2d1	světelná
energie	energie	k1gFnSc2	energie
na	na	k7c4	na
chemickou	chemický	k2eAgFnSc4d1	chemická
<g/>
.	.	kIx.	.
</s>
<s>
Přehled	přehled	k1gInSc1	přehled
fotosyntetických	fotosyntetický	k2eAgNnPc2d1	fotosyntetické
barviv	barvivo	k1gNnPc2	barvivo
u	u	k7c2	u
sinic	sinice	k1gFnPc2	sinice
je	být	k5eAaImIp3nS	být
skutečně	skutečně	k6eAd1	skutečně
pestrý	pestrý	k2eAgInSc1d1	pestrý
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
sinic	sinice	k1gFnPc2	sinice
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
všechny	všechen	k3xTgInPc1	všechen
čtyři	čtyři	k4xCgInPc1	čtyři
známé	známý	k2eAgInPc1d1	známý
druhy	druh	k1gInPc1	druh
chlorofylu	chlorofyl	k1gInSc2	chlorofyl
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
nejen	nejen	k6eAd1	nejen
a	a	k8xC	a
(	(	kIx(	(
<g/>
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
zpravidla	zpravidla	k6eAd1	zpravidla
roli	role	k1gFnSc4	role
hlavního	hlavní	k2eAgInSc2d1	hlavní
fotosyntetického	fotosyntetický	k2eAgInSc2d1	fotosyntetický
pigmentu	pigment	k1gInSc2	pigment
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
zástupců	zástupce	k1gMnPc2	zástupce
navíc	navíc	k6eAd1	navíc
i	i	k9	i
b	b	k?	b
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
d.	d.	k?	d.
Chlorofyl	chlorofyl	k1gInSc1	chlorofyl
b	b	k?	b
byl	být	k5eAaImAgInS	být
nalezen	nalézt	k5eAaBmNgInS	nalézt
u	u	k7c2	u
prochlorofytů	prochlorofyt	k1gInPc2	prochlorofyt
(	(	kIx(	(
<g/>
Prochloron	Prochloron	k1gInSc1	Prochloron
<g/>
,	,	kIx,	,
Prochlorococcus	Prochlorococcus	k1gInSc1	Prochlorococcus
<g/>
,	,	kIx,	,
Prochlorothrix	Prochlorothrix	k1gInSc1	Prochlorothrix
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chlorofyl	chlorofyl	k1gInSc1	chlorofyl
c	c	k0	c
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
zástupců	zástupce	k1gMnPc2	zástupce
rodu	rod	k1gInSc2	rod
Prochlorococcus	Prochlorococcus	k1gMnSc1	Prochlorococcus
a	a	k8xC	a
chlorofyl	chlorofyl	k1gInSc1	chlorofyl
d	d	k?	d
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
být	být	k5eAaImF	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
fotosyntetickým	fotosyntetický	k2eAgInSc7d1	fotosyntetický
pigmentem	pigment	k1gInSc7	pigment
u	u	k7c2	u
sinice	sinice	k1gFnSc2	sinice
Acaryochloris	Acaryochloris	k1gFnSc1	Acaryochloris
marina	marina	k1gFnSc1	marina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
fykobilizomech	fykobilizom	k1gInPc6	fykobilizom
bývají	bývat	k5eAaImIp3nP	bývat
obsaženy	obsáhnout	k5eAaPmNgInP	obsáhnout
tři	tři	k4xCgInPc1	tři
fykobiliproteiny	fykobiliprotein	k1gInPc1	fykobiliprotein
<g/>
:	:	kIx,	:
modrý	modrý	k2eAgInSc1d1	modrý
allofykocyanin	allofykocyanin	k2eAgInSc1d1	allofykocyanin
a	a	k8xC	a
fykocyanin	fykocyanin	k2eAgInSc1d1	fykocyanin
a	a	k8xC	a
červený	červený	k2eAgInSc1d1	červený
fykoerytrin	fykoerytrin	k1gInSc1	fykoerytrin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
byla	být	k5eAaImAgFnS	být
nalezena	nalezen	k2eAgFnSc1d1	nalezena
i	i	k8xC	i
barviva	barvivo	k1gNnPc4	barvivo
β	β	k?	β
<g/>
,	,	kIx,	,
zeaxantin	zeaxantin	k1gMnSc1	zeaxantin
<g/>
,	,	kIx,	,
echinenon	echinenon	k1gMnSc1	echinenon
<g/>
,	,	kIx,	,
kantaxantin	kantaxantin	k1gMnSc1	kantaxantin
či	či	k8xC	či
myxoxantofyl	myxoxantofyl	k1gInSc1	myxoxantofyl
(	(	kIx(	(
<g/>
typ	typ	k1gInSc1	typ
xanthofylu	xanthofyl	k1gInSc2	xanthofyl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mnohé	mnohý	k2eAgFnPc1d1	mnohá
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
nemusí	muset	k5eNaImIp3nS	muset
zastávat	zastávat	k5eAaImF	zastávat
roli	role	k1gFnSc4	role
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
tylakoidech	tylakoid	k1gInPc6	tylakoid
přítomna	přítomen	k2eAgNnPc4d1	přítomno
reakční	reakční	k2eAgNnPc4d1	reakční
centra	centrum	k1gNnPc4	centrum
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
fotosystémy	fotosystém	k1gInPc4	fotosystém
I	i	k8xC	i
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
fotosystému	fotosystý	k2eAgInSc3d1	fotosystý
II	II	kA	II
probíhá	probíhat	k5eAaImIp3nS	probíhat
fotolýza	fotolýza	k1gFnSc1	fotolýza
vody	voda	k1gFnSc2	voda
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
kyslíku	kyslík	k1gInSc2	kyslík
a	a	k8xC	a
vodíkového	vodíkový	k2eAgInSc2d1	vodíkový
kationtu	kation	k1gInSc2	kation
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
uvolněnými	uvolněný	k2eAgInPc7d1	uvolněný
elektrony	elektron	k1gInPc7	elektron
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
reakcí	reakce	k1gFnPc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Elektrony	elektron	k1gInPc1	elektron
jsou	být	k5eAaImIp3nP	být
transportovány	transportovat	k5eAaBmNgFnP	transportovat
řetězcem	řetězec	k1gInSc7	řetězec
proteinových	proteinový	k2eAgInPc2d1	proteinový
komplexů	komplex	k1gInPc2	komplex
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
protonového	protonový	k2eAgInSc2d1	protonový
gradientu	gradient	k1gInSc2	gradient
napříč	napříč	k7c7	napříč
membránou	membrána	k1gFnSc7	membrána
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
podstatou	podstata	k1gFnSc7	podstata
vzniku	vznik	k1gInSc2	vznik
adenosintrifosfátu	adenosintrifosfát	k1gInSc2	adenosintrifosfát
(	(	kIx(	(
<g/>
ATP	atp	kA	atp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
tylakoidy	tylakoida	k1gFnPc4	tylakoida
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
při	při	k7c6	při
fotosyntéze	fotosyntéza	k1gFnSc6	fotosyntéza
ještě	ještě	k6eAd1	ještě
další	další	k2eAgFnPc4d1	další
struktury	struktura	k1gFnPc4	struktura
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
fykobilizomy	fykobilizom	k1gInPc4	fykobilizom
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
využít	využít	k5eAaPmF	využít
co	co	k9	co
nejširší	široký	k2eAgNnSc4d3	nejširší
spektrum	spektrum	k1gNnSc4	spektrum
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Karboxyzomy	Karboxyzom	k1gInPc1	Karboxyzom
jsou	být	k5eAaImIp3nP	být
tělíska	tělísko	k1gNnPc4	tělísko
obsahující	obsahující	k2eAgInSc4d1	obsahující
enzym	enzym	k1gInSc4	enzym
Rubisco	Rubisco	k1gMnSc1	Rubisco
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
syntézu	syntéza	k1gFnSc4	syntéza
cukrů	cukr	k1gInPc2	cukr
v	v	k7c6	v
Calvinově	Calvinův	k2eAgInSc6d1	Calvinův
cyklu	cyklus	k1gInSc6	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Zásobní	zásobní	k2eAgFnSc7d1	zásobní
látkou	látka	k1gFnSc7	látka
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
sinic	sinice	k1gFnPc2	sinice
především	především	k9	především
tzv.	tzv.	kA	tzv.
sinicový	sinicový	k2eAgInSc4d1	sinicový
škrob	škrob	k1gInSc4	škrob
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dusík	dusík	k1gInSc1	dusík
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
skladuje	skladovat	k5eAaImIp3nS	skladovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
cyanofycinu	cyanofycin	k1gInSc2	cyanofycin
a	a	k8xC	a
fosfor	fosfor	k1gInSc4	fosfor
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
zrn	zrno	k1gNnPc2	zrno
volutinu	volutin	k1gInSc2	volutin
<g/>
.	.	kIx.	.
</s>
<s>
Fotosyntetická	fotosyntetický	k2eAgFnSc1d1	fotosyntetická
schopnost	schopnost	k1gFnSc1	schopnost
sinic	sinice	k1gFnPc2	sinice
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
poměrně	poměrně	k6eAd1	poměrně
flexibilní	flexibilní	k2eAgNnSc1d1	flexibilní
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétním	konkrétní	k2eAgInSc7d1	konkrétní
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
chromatická	chromatický	k2eAgFnSc1d1	chromatická
adaptace	adaptace	k1gFnSc1	adaptace
probíhající	probíhající	k2eAgFnSc1d1	probíhající
ve	v	k7c6	v
ztížených	ztížený	k2eAgFnPc6d1	ztížená
světelných	světelný	k2eAgFnPc6d1	světelná
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jev	jev	k1gInSc4	jev
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
na	na	k7c6	na
fykobilizomech	fykobilizom	k1gInPc6	fykobilizom
upravuje	upravovat	k5eAaImIp3nS	upravovat
počet	počet	k1gInSc1	počet
fykocyaninových	fykocyaninový	k2eAgFnPc2d1	fykocyaninový
a	a	k8xC	a
fykoerytrinových	fykoerytrinův	k2eAgFnPc2d1	fykoerytrinův
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
posouvá	posouvat	k5eAaImIp3nS	posouvat
spektrum	spektrum	k1gNnSc1	spektrum
využitelného	využitelný	k2eAgNnSc2d1	využitelné
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Pokusy	pokus	k1gInPc4	pokus
bylo	být	k5eAaImAgNnS	být
dokázáno	dokázat	k5eAaPmNgNnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
buňka	buňka	k1gFnSc1	buňka
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
změnit	změnit	k5eAaPmF	změnit
svou	svůj	k3xOyFgFnSc4	svůj
barvu	barva	k1gFnSc4	barva
od	od	k7c2	od
ocelově	ocelově	k6eAd1	ocelově
šedé	šedý	k2eAgFnSc2d1	šedá
přes	přes	k7c4	přes
různé	různý	k2eAgInPc4d1	různý
stupně	stupeň	k1gInPc4	stupeň
zelené	zelená	k1gFnSc2	zelená
po	po	k7c4	po
červenavou	červenavý	k2eAgFnSc4d1	červenavá
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
příkladem	příklad	k1gInSc7	příklad
přizpůsobivosti	přizpůsobivost	k1gFnSc2	přizpůsobivost
je	být	k5eAaImIp3nS	být
schopnost	schopnost	k1gFnSc1	schopnost
přejít	přejít	k5eAaPmF	přejít
v	v	k7c6	v
anaerobním	anaerobní	k2eAgNnSc6d1	anaerobní
prostředí	prostředí	k1gNnSc6	prostředí
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
sirovodíku	sirovodík	k1gInSc2	sirovodík
na	na	k7c4	na
anaerobní	anaerobní	k2eAgFnSc4d1	anaerobní
fotosyntézu	fotosyntéza	k1gFnSc4	fotosyntéza
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
donor	donor	k1gInSc1	donor
elektronů	elektron	k1gInPc2	elektron
užíván	užíván	k2eAgMnSc1d1	užíván
místo	místo	k7c2	místo
vody	voda	k1gFnSc2	voda
právě	právě	k9	právě
sirovodík	sirovodík	k1gInSc4	sirovodík
<g/>
.	.	kIx.	.
</s>
<s>
Sinice	sinice	k1gFnPc1	sinice
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
významnými	významný	k2eAgInPc7d1	významný
fixátory	fixátor	k1gInPc7	fixátor
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
dusíku	dusík	k1gInSc2	dusík
v	v	k7c6	v
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
měřítku	měřítko	k1gNnSc6	měřítko
a	a	k8xC	a
významně	významně	k6eAd1	významně
tak	tak	k6eAd1	tak
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
koloběh	koloběh	k1gInSc4	koloběh
dusíku	dusík	k1gInSc2	dusík
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
reakce	reakce	k1gFnSc1	reakce
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
ve	v	k7c6	v
speciálních	speciální	k2eAgFnPc6d1	speciální
buňkách	buňka	k1gFnPc6	buňka
bez	bez	k7c2	bez
fotosyntetické	fotosyntetický	k2eAgFnSc2d1	fotosyntetická
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
zvaných	zvaný	k2eAgInPc2d1	zvaný
heterocyty	heterocyt	k1gInPc7	heterocyt
či	či	k8xC	či
heterocysty	heterocyst	k1gInPc7	heterocyst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
striktně	striktně	k6eAd1	striktně
anaerobních	anaerobní	k2eAgFnPc6d1	anaerobní
podmínkách	podmínka	k1gFnPc6	podmínka
uvnitř	uvnitř	k7c2	uvnitř
heterocytů	heterocyt	k1gInPc2	heterocyt
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
enzymu	enzym	k1gInSc2	enzym
nitrogenázy	nitrogenáza	k1gFnSc2	nitrogenáza
ze	z	k7c2	z
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
dusíku	dusík	k1gInSc2	dusík
(	(	kIx(	(
<g/>
N	N	kA	N
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
za	za	k7c2	za
spotřeby	spotřeba	k1gFnSc2	spotřeba
adenosintrifosfátu	adenosintrifosfát	k1gInSc2	adenosintrifosfát
(	(	kIx(	(
<g/>
ATP	atp	kA	atp
<g/>
)	)	kIx)	)
amonné	amonný	k2eAgFnPc1d1	amonná
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
sinice	sinice	k1gFnPc1	sinice
ani	ani	k8xC	ani
nemají	mít	k5eNaImIp3nP	mít
heterocyty	heterocyt	k1gInPc4	heterocyt
(	(	kIx(	(
<g/>
např.	např.	kA	např.
rod	rod	k1gInSc1	rod
Lyngbya	Lyngbya	k1gFnSc1	Lyngbya
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
pak	pak	k6eAd1	pak
fixují	fixovat	k5eAaImIp3nP	fixovat
dusík	dusík	k1gInSc4	dusík
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
neprobíhá	probíhat	k5eNaImIp3nS	probíhat
fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
a	a	k8xC	a
množství	množství	k1gNnSc1	množství
kyslíku	kyslík	k1gInSc2	kyslík
uvnitř	uvnitř	k7c2	uvnitř
buňky	buňka	k1gFnSc2	buňka
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
minimální	minimální	k2eAgMnSc1d1	minimální
<g/>
.	.	kIx.	.
</s>
<s>
Sinice	sinice	k1gFnPc1	sinice
produkují	produkovat	k5eAaImIp3nP	produkovat
množství	množství	k1gNnSc4	množství
dalších	další	k2eAgInPc2d1	další
sekundárních	sekundární	k2eAgInPc2d1	sekundární
metabolitů	metabolit	k1gInPc2	metabolit
a	a	k8xC	a
podpůrných	podpůrný	k2eAgFnPc2d1	podpůrná
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
různé	různý	k2eAgInPc1d1	různý
oligosacharidy	oligosacharid	k1gInPc1	oligosacharid
<g/>
,	,	kIx,	,
karboxylové	karboxylový	k2eAgFnPc1d1	karboxylová
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
,	,	kIx,	,
vitamíny	vitamín	k1gInPc1	vitamín
<g/>
,	,	kIx,	,
peptidy	peptid	k1gInPc1	peptid
<g/>
,	,	kIx,	,
atraktanty	atraktant	k1gMnPc7	atraktant
<g/>
,	,	kIx,	,
hormony	hormon	k1gInPc7	hormon
<g/>
,	,	kIx,	,
enzymy	enzym	k1gInPc1	enzym
<g/>
,	,	kIx,	,
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
<g/>
,	,	kIx,	,
polysacharidy	polysacharid	k1gInPc1	polysacharid
a	a	k8xC	a
toxiny	toxin	k1gInPc1	toxin
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
proti	proti	k7c3	proti
UV	UV	kA	UV
záření	záření	k1gNnSc2	záření
např.	např.	kA	např.
produkují	produkovat	k5eAaImIp3nP	produkovat
pigment	pigment	k1gInSc1	pigment
scytonemin	scytonemin	k2eAgInSc1d1	scytonemin
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
gloeocapsin	gloeocapsin	k1gMnSc1	gloeocapsin
<g/>
,	,	kIx,	,
β	β	k?	β
<g/>
,	,	kIx,	,
kantaxantin	kantaxantin	k1gInSc1	kantaxantin
či	či	k8xC	či
myxoxantofyl	myxoxantofyl	k1gInSc1	myxoxantofyl
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
látky	látka	k1gFnPc1	látka
odfiltrovávají	odfiltrovávat	k5eAaImIp3nP	odfiltrovávat
záření	záření	k1gNnSc4	záření
o	o	k7c6	o
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgFnSc6d1	nízká
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
<g/>
.	.	kIx.	.
</s>
<s>
Sinice	sinice	k1gFnPc1	sinice
rovněž	rovněž	k9	rovněž
produkují	produkovat	k5eAaImIp3nP	produkovat
široké	široký	k2eAgNnSc4d1	široké
spektrum	spektrum	k1gNnSc4	spektrum
jedů	jed	k1gInPc2	jed
<g/>
,	,	kIx,	,
souhrnně	souhrnně	k6eAd1	souhrnně
tzv.	tzv.	kA	tzv.
cyanotoxinů	cyanotoxin	k1gInPc2	cyanotoxin
<g/>
.	.	kIx.	.
</s>
<s>
Způsobují	způsobovat	k5eAaImIp3nP	způsobovat
kožní	kožní	k2eAgFnPc4d1	kožní
alergie	alergie	k1gFnPc4	alergie
<g/>
,	,	kIx,	,
zánět	zánět	k1gInSc4	zánět
spojivek	spojivka	k1gFnPc2	spojivka
<g/>
,	,	kIx,	,
bronchitidu	bronchitida	k1gFnSc4	bronchitida
<g/>
,	,	kIx,	,
u	u	k7c2	u
dobytka	dobytek	k1gInSc2	dobytek
napájeného	napájený	k2eAgInSc2d1	napájený
znečištěnou	znečištěný	k2eAgFnSc4d1	znečištěná
vodou	voda	k1gFnSc7	voda
i	i	k8xC	i
otravu	otrava	k1gFnSc4	otrava
<g/>
.	.	kIx.	.
</s>
<s>
Nebezpečné	bezpečný	k2eNgFnPc1d1	nebezpečná
koncentrace	koncentrace	k1gFnPc1	koncentrace
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
sinicové	sinicový	k2eAgInPc1d1	sinicový
jedy	jed	k1gInPc1	jed
především	především	k9	především
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
rozvinutý	rozvinutý	k2eAgMnSc1d1	rozvinutý
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
vodní	vodní	k2eAgInSc4d1	vodní
květ	květ	k1gInSc4	květ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
sinic	sinice	k1gFnPc2	sinice
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
pouze	pouze	k6eAd1	pouze
nepohlavní	pohlavní	k2eNgNnSc1d1	nepohlavní
rozmnožování	rozmnožování	k1gNnSc1	rozmnožování
<g/>
,	,	kIx,	,
žádné	žádný	k3yNgFnPc1	žádný
rozmnožovací	rozmnožovací	k2eAgFnPc1d1	rozmnožovací
buňky	buňka	k1gFnPc1	buňka
u	u	k7c2	u
sinic	sinice	k1gFnPc2	sinice
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Buňky	buňka	k1gFnPc1	buňka
jednobuněčných	jednobuněčný	k2eAgFnPc2d1	jednobuněčná
(	(	kIx(	(
<g/>
kokálních	kokální	k2eAgFnPc2d1	kokální
<g/>
)	)	kIx)	)
sinic	sinice	k1gFnPc2	sinice
se	se	k3xPyFc4	se
množí	množit	k5eAaImIp3nP	množit
pouze	pouze	k6eAd1	pouze
prostým	prostý	k2eAgNnSc7d1	prosté
dělením	dělení	k1gNnSc7	dělení
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dělení	dělení	k1gNnSc1	dělení
začíná	začínat	k5eAaImIp3nS	začínat
tvorbou	tvorba	k1gFnSc7	tvorba
příčné	příčný	k2eAgFnSc2d1	příčná
přehrádky	přehrádka	k1gFnSc2	přehrádka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
od	od	k7c2	od
krajů	kraj	k1gInPc2	kraj
buňky	buňka	k1gFnSc2	buňka
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
uzavírá	uzavírat	k5eAaImIp3nS	uzavírat
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
clona	clona	k1gFnSc1	clona
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
dělení	dělení	k1gNnSc1	dělení
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
rovinách	rovina	k1gFnPc6	rovina
<g/>
,	,	kIx,	,
vznikají	vznikat	k5eAaImIp3nP	vznikat
kolonie	kolonie	k1gFnPc1	kolonie
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
vzájemnou	vzájemný	k2eAgFnSc7d1	vzájemná
orientací	orientace	k1gFnSc7	orientace
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
sinice	sinice	k1gFnPc1	sinice
se	se	k3xPyFc4	se
však	však	k9	však
tomuto	tento	k3xDgInSc3	tento
klasickému	klasický	k2eAgInSc3d1	klasický
scénáři	scénář	k1gInSc3	scénář
vyhýbají	vyhýbat	k5eAaImIp3nP	vyhýbat
<g/>
:	:	kIx,	:
Chamaesiphon	Chamaesiphon	k1gInSc1	Chamaesiphon
se	se	k3xPyFc4	se
rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
pomocí	pomocí	k7c2	pomocí
exocytů	exocyt	k1gInPc2	exocyt
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
spor	spora	k1gFnPc2	spora
vznikajících	vznikající	k2eAgFnPc2d1	vznikající
na	na	k7c6	na
volném	volný	k2eAgInSc6d1	volný
konci	konec	k1gInSc6	konec
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
a	a	k8xC	a
Chroococcidiopsis	Chroococcidiopsis	k1gFnSc1	Chroococcidiopsis
se	se	k3xPyFc4	se
rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
mnohonásobným	mnohonásobný	k2eAgNnSc7d1	mnohonásobné
dělením	dělení	k1gNnSc7	dělení
tzv.	tzv.	kA	tzv.
baeocytu	baeocyt	k1gInSc6	baeocyt
<g/>
.	.	kIx.	.
</s>
<s>
Vláknité	vláknitý	k2eAgFnPc1d1	vláknitá
sinice	sinice	k1gFnPc1	sinice
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
štěpit	štěpit	k5eAaImF	štěpit
na	na	k7c4	na
dílčí	dílčí	k2eAgNnPc4d1	dílčí
pohyblivá	pohyblivý	k2eAgNnPc4d1	pohyblivé
vlákna	vlákno	k1gNnPc4	vlákno
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
hormogonie	hormogonie	k1gFnSc1	hormogonie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dlouhodobému	dlouhodobý	k2eAgNnSc3d1	dlouhodobé
přežívání	přežívání	k1gNnSc3	přežívání
slouží	sloužit	k5eAaImIp3nS	sloužit
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
sinic	sinice	k1gFnPc2	sinice
akinety	akinet	k1gInPc4	akinet
<g/>
,	,	kIx,	,
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
druh	druh	k1gInSc4	druh
tlustostěnných	tlustostěnný	k2eAgFnPc2d1	tlustostěnná
spor	spora	k1gFnPc2	spora
<g/>
.	.	kIx.	.
</s>
<s>
Sinice	sinice	k1gFnPc1	sinice
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
evoluční	evoluční	k2eAgInSc4d1	evoluční
úspěch	úspěch	k1gInSc4	úspěch
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
geologickou	geologický	k2eAgFnSc4d1	geologická
historii	historie	k1gFnSc4	historie
vděčí	vděčit	k5eAaImIp3nS	vděčit
kombinací	kombinace	k1gFnSc7	kombinace
několika	několik	k4yIc2	několik
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
charakteristických	charakteristický	k2eAgFnPc2d1	charakteristická
vlastností	vlastnost	k1gFnSc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prekambrickém	prekambrický	k2eAgNnSc6d1	prekambrický
období	období	k1gNnSc6	období
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
byly	být	k5eAaImAgFnP	být
schopny	schopen	k2eAgFnPc1d1	schopna
sinice	sinice	k1gFnPc1	sinice
tolerovat	tolerovat	k5eAaImF	tolerovat
nízký	nízký	k2eAgInSc4d1	nízký
obsah	obsah	k1gInSc4	obsah
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
vysokou	vysoký	k2eAgFnSc4d1	vysoká
míru	míra	k1gFnSc4	míra
UV	UV	kA	UV
záření	záření	k1gNnSc1	záření
a	a	k8xC	a
zvýšené	zvýšený	k2eAgFnPc1d1	zvýšená
koncentrace	koncentrace	k1gFnPc1	koncentrace
sirovodíku	sirovodík	k1gInSc2	sirovodík
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
sinice	sinice	k1gFnPc1	sinice
téměř	téměř	k6eAd1	téměř
všudypřítomné	všudypřítomný	k2eAgFnPc1d1	všudypřítomná
a	a	k8xC	a
mnohdy	mnohdy	k6eAd1	mnohdy
také	také	k9	také
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
do	do	k7c2	do
významných	významný	k2eAgInPc2d1	významný
symbiotických	symbiotický	k2eAgInPc2d1	symbiotický
svazků	svazek	k1gInPc2	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
přemnoží	přemnožit	k5eAaPmIp3nP	přemnožit
ve	v	k7c6	v
vodním	vodní	k2eAgNnSc6d1	vodní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nazývány	nazýván	k2eAgInPc1d1	nazýván
"	"	kIx"	"
<g/>
vodní	vodní	k2eAgInSc1d1	vodní
květ	květ	k1gInSc1	květ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sinice	sinice	k1gFnPc1	sinice
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
širokém	široký	k2eAgNnSc6d1	široké
spektru	spektrum	k1gNnSc6	spektrum
různých	různý	k2eAgNnPc2d1	různé
prostředí	prostředí	k1gNnPc2	prostředí
<g/>
:	:	kIx,	:
v	v	k7c6	v
sladkých	sladký	k2eAgFnPc6d1	sladká
i	i	k8xC	i
slaných	slaný	k2eAgFnPc6d1	slaná
vodách	voda	k1gFnPc6	voda
(	(	kIx(	(
<g/>
plankton	plankton	k1gInSc4	plankton
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
v	v	k7c6	v
tenké	tenký	k2eAgFnSc6d1	tenká
vrstvě	vrstva	k1gFnSc6	vrstva
pod	pod	k7c7	pod
ní	on	k3xPp3gFnSc7	on
<g/>
,	,	kIx,	,
na	na	k7c6	na
skalách	skála	k1gFnPc6	skála
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
vápencových	vápencový	k2eAgInPc2d1	vápencový
<g/>
)	)	kIx)	)
i	i	k9	i
uvnitř	uvnitř	k7c2	uvnitř
jeskyní	jeskyně	k1gFnPc2	jeskyně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nápadná	nápadný	k2eAgFnSc1d1	nápadná
je	být	k5eAaImIp3nS	být
schopnost	schopnost	k1gFnSc1	schopnost
sinic	sinice	k1gFnPc2	sinice
osídlovat	osídlovat	k5eAaImF	osídlovat
různá	různý	k2eAgNnPc4d1	různé
extrémní	extrémní	k2eAgNnPc4d1	extrémní
prostředí	prostředí	k1gNnPc4	prostředí
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
jiné	jiný	k2eAgFnPc4d1	jiná
skupiny	skupina	k1gFnPc4	skupina
organismů	organismus	k1gInPc2	organismus
nehostinná	hostinný	k2eNgFnSc1d1	nehostinná
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
sinice	sinice	k1gFnSc1	sinice
také	také	k6eAd1	také
často	často	k6eAd1	často
zcela	zcela	k6eAd1	zcela
dominují	dominovat	k5eAaImIp3nP	dominovat
<g/>
.	.	kIx.	.
</s>
<s>
Sinice	sinice	k1gFnPc1	sinice
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
primárními	primární	k2eAgMnPc7d1	primární
kolonizátory	kolonizátor	k1gMnPc7	kolonizátor
dosud	dosud	k6eAd1	dosud
neosídlených	osídlený	k2eNgFnPc2d1	neosídlená
skal	skála	k1gFnPc2	skála
či	či	k8xC	či
nově	nově	k6eAd1	nově
vzniklých	vzniklý	k2eAgFnPc2d1	vzniklá
půd	půda	k1gFnPc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
zaznamenány	zaznamenat	k5eAaPmNgFnP	zaznamenat
termofilní	termofilní	k2eAgFnPc1d1	termofilní
sinice	sinice	k1gFnPc1	sinice
rodu	rod	k1gInSc2	rod
Synechococcus	Synechococcus	k1gInSc1	Synechococcus
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgInPc1d1	žijící
v	v	k7c6	v
termálních	termální	k2eAgInPc6d1	termální
pramenech	pramen	k1gInPc6	pramen
v	v	k7c6	v
teplotě	teplota	k1gFnSc6	teplota
až	až	k9	až
73	[number]	k4	73
°	°	k?	°
<g/>
C.	C.	kA	C.
Ve	v	k7c6	v
špatných	špatný	k2eAgFnPc6d1	špatná
světelných	světelný	k2eAgFnPc6d1	světelná
a	a	k8xC	a
nutričních	nutriční	k2eAgFnPc6d1	nutriční
podmínkách	podmínka	k1gFnPc6	podmínka
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
žít	žít	k5eAaImF	žít
řada	řada	k1gFnSc1	řada
sinic	sinice	k1gFnPc2	sinice
podílejících	podílející	k2eAgFnPc2d1	podílející
se	se	k3xPyFc4	se
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
mořského	mořský	k2eAgInSc2d1	mořský
pikoplanktonu	pikoplankton	k1gInSc2	pikoplankton
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
halofilní	halofilní	k2eAgInPc1d1	halofilní
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgInPc1d1	schopný
odolávat	odolávat	k5eAaImF	odolávat
vysokým	vysoký	k2eAgFnPc3d1	vysoká
koncentracím	koncentrace	k1gFnPc3	koncentrace
soli	sůl	k1gFnSc2	sůl
v	v	k7c6	v
okolní	okolní	k2eAgFnSc6d1	okolní
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Mrtvém	mrtvý	k2eAgNnSc6d1	mrtvé
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Alkalofilní	Alkalofilní	k2eAgMnPc1d1	Alkalofilní
zástupci	zástupce	k1gMnPc1	zástupce
dokáží	dokázat	k5eAaPmIp3nP	dokázat
žít	žít	k5eAaImF	žít
i	i	k9	i
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
zásaditém	zásaditý	k2eAgNnSc6d1	zásadité
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
při	při	k7c6	při
pH	ph	kA	ph
13,5	[number]	k4	13,5
žije	žít	k5eAaImIp3nS	žít
sinice	sinice	k1gFnSc1	sinice
rodu	rod	k1gInSc2	rod
Leptolyngbya	Leptolyngbyum	k1gNnSc2	Leptolyngbyum
(	(	kIx(	(
<g/>
zřejmě	zřejmě	k6eAd1	zřejmě
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
zjištěná	zjištěný	k2eAgFnSc1d1	zjištěná
hodnota	hodnota	k1gFnSc1	hodnota
pH	ph	kA	ph
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
život	život	k1gInSc1	život
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
suchozemském	suchozemský	k2eAgNnSc6d1	suchozemské
prostředí	prostředí	k1gNnSc6	prostředí
úspěšně	úspěšně	k6eAd1	úspěšně
vzdorují	vzdorovat	k5eAaImIp3nP	vzdorovat
vyschnutí	vyschnutí	k1gNnPc4	vyschnutí
tzv.	tzv.	kA	tzv.
xerofilní	xerofilní	k2eAgFnPc4d1	xerofilní
sinice	sinice	k1gFnPc4	sinice
<g/>
:	:	kIx,	:
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
Negev	Negva	k1gFnPc2	Negva
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
sinice	sinice	k1gFnPc1	sinice
v	v	k7c6	v
drobných	drobný	k2eAgFnPc6d1	drobná
dutinkách	dutinka	k1gFnPc6	dutinka
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
stopové	stopový	k2eAgNnSc1d1	stopové
množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polárních	polární	k2eAgFnPc6d1	polární
oblastech	oblast	k1gFnPc6	oblast
jsou	být	k5eAaImIp3nP	být
sinice	sinice	k1gFnPc1	sinice
velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc7d1	důležitá
složkou	složka	k1gFnSc7	složka
zdejšího	zdejší	k2eAgInSc2d1	zdejší
koloběhu	koloběh	k1gInSc2	koloběh
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
kolonizují	kolonizovat	k5eAaBmIp3nP	kolonizovat
například	například	k6eAd1	například
dna	dna	k1gFnSc1	dna
antarktických	antarktický	k2eAgNnPc2d1	antarktické
jezer	jezero	k1gNnPc2	jezero
a	a	k8xC	a
ledovou	ledový	k2eAgFnSc4d1	ledová
krustu	krusta	k1gFnSc4	krusta
v	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
další	další	k2eAgFnPc1d1	další
sinice	sinice	k1gFnPc1	sinice
osídlují	osídlovat	k5eAaImIp3nP	osídlovat
póry	pór	k1gInPc4	pór
uvnitř	uvnitř	k7c2	uvnitř
kamenů	kámen	k1gInPc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Sinice	sinice	k1gFnSc1	sinice
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
povlaků	povlak	k1gInPc2	povlak
inkrustovaných	inkrustovaný	k2eAgInPc2d1	inkrustovaný
uhličitanem	uhličitan	k1gInSc7	uhličitan
vápenatým	vápenatý	k2eAgInSc7d1	vápenatý
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
mohou	moct	k5eAaImIp3nP	moct
vznikat	vznikat	k5eAaImF	vznikat
různé	různý	k2eAgInPc4d1	různý
travertiny	travertin	k1gInPc4	travertin
<g/>
,	,	kIx,	,
onkolity	onkolit	k1gInPc4	onkolit
a	a	k8xC	a
stromatolity	stromatolit	k1gInPc4	stromatolit
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
vodní	vodní	k2eAgInSc4d1	vodní
květ	květ	k1gInSc4	květ
<g/>
.	.	kIx.	.
</s>
<s>
Sinice	sinice	k1gFnPc1	sinice
jsou	být	k5eAaImIp3nP	být
častým	častý	k2eAgMnSc7d1	častý
původcem	původce	k1gMnSc7	původce
jevu	jev	k1gInSc2	jev
známého	známý	k2eAgInSc2d1	známý
jako	jako	k8xC	jako
vodní	vodní	k2eAgInSc4d1	vodní
květ	květ	k1gInSc4	květ
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgNnSc6	jenž
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přemnožení	přemnožení	k1gNnSc3	přemnožení
některých	některý	k3yIgInPc2	některý
vodních	vodní	k2eAgInPc2d1	vodní
fotosyntetizujících	fotosyntetizující	k2eAgInPc2d1	fotosyntetizující
organismů	organismus	k1gInPc2	organismus
nad	nad	k7c4	nad
určitou	určitý	k2eAgFnSc4d1	určitá
míru	míra	k1gFnSc4	míra
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
představuje	představovat	k5eAaImIp3nS	představovat
především	především	k9	především
vodní	vodní	k2eAgInSc4d1	vodní
květ	květ	k1gInSc4	květ
v	v	k7c6	v
sladkovodních	sladkovodní	k2eAgFnPc6d1	sladkovodní
nádržích	nádrž	k1gFnPc6	nádrž
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
využívány	využívat	k5eAaImNgFnP	využívat
jako	jako	k8xC	jako
zdroj	zdroj	k1gInSc1	zdroj
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
k	k	k7c3	k
rekreaci	rekreace	k1gFnSc3	rekreace
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rozvoji	rozvoj	k1gInSc6	rozvoj
vodního	vodní	k2eAgInSc2d1	vodní
květu	květ	k1gInSc2	květ
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
množství	množství	k1gNnSc6	množství
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
uvolňovány	uvolňován	k2eAgInPc4d1	uvolňován
cyanotoxiny	cyanotoxin	k1gInPc4	cyanotoxin
<g/>
.	.	kIx.	.
</s>
<s>
Rizikové	rizikový	k2eAgNnSc1d1	rizikové
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
sinicového	sinicový	k2eAgInSc2d1	sinicový
vodního	vodní	k2eAgInSc2d1	vodní
květu	květ	k1gInSc2	květ
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
vodní	vodní	k2eAgFnPc1d1	vodní
plochy	plocha	k1gFnPc1	plocha
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
hodnotou	hodnota	k1gFnSc7	hodnota
pH	ph	kA	ph
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgMnSc1d2	vyšší
teplotou	teplota	k1gFnSc7	teplota
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
živin	živina	k1gFnPc2	živina
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
fosforu	fosfor	k1gInSc3	fosfor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
vodní	vodní	k2eAgInSc1d1	vodní
květ	květ	k1gInSc1	květ
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
nadměrné	nadměrný	k2eAgNnSc4d1	nadměrné
vypouštění	vypouštění	k1gNnSc4	vypouštění
živin	živina	k1gFnPc2	živina
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
tzv.	tzv.	kA	tzv.
eutrofizace	eutrofizace	k1gFnSc1	eutrofizace
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
různé	různý	k2eAgInPc4d1	různý
způsoby	způsob	k1gInPc4	způsob
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
vodnímu	vodní	k2eAgInSc3d1	vodní
květu	květ	k1gInSc3	květ
<g/>
.	.	kIx.	.
</s>
<s>
Sinice	sinice	k1gFnPc1	sinice
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
do	do	k7c2	do
symbiotických	symbiotický	k2eAgInPc2d1	symbiotický
svazků	svazek	k1gInPc2	svazek
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
organismy	organismus	k1gInPc7	organismus
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
do	do	k7c2	do
mutualistických	mutualistický	k2eAgFnPc2d1	mutualistická
(	(	kIx(	(
<g/>
oboustranně	oboustranně	k6eAd1	oboustranně
prospěšných	prospěšný	k2eAgInPc2d1	prospěšný
<g/>
)	)	kIx)	)
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sinice	sinice	k1gFnSc1	sinice
zpravidla	zpravidla	k6eAd1	zpravidla
hraje	hrát	k5eAaImIp3nS	hrát
roli	role	k1gFnSc4	role
fotobionta	fotobiont	k1gInSc2	fotobiont
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
sinice	sinice	k1gFnPc4	sinice
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
cizích	cizí	k2eAgFnPc2d1	cizí
eukaryotních	eukaryotní	k2eAgFnPc2d1	eukaryotní
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
minimálně	minimálně	k6eAd1	minimálně
jedné	jeden	k4xCgFnSc2	jeden
takové	takový	k3xDgFnPc4	takový
události	událost	k1gFnPc4	událost
jsou	být	k5eAaImIp3nP	být
plastidy	plastid	k1gInPc1	plastid
-	-	kIx~	-
organely	organela	k1gFnPc1	organela
mnoha	mnoho	k4c2	mnoho
eukaryot	eukaryota	k1gFnPc2	eukaryota
připomínající	připomínající	k2eAgMnSc1d1	připomínající
v	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
své	svůj	k3xOyFgMnPc4	svůj
prapředky	prapředek	k1gMnPc4	prapředek
<g/>
,	,	kIx,	,
sinice	sinice	k1gFnSc1	sinice
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc7d1	jiná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podobnou	podobný	k2eAgFnSc4d1	podobná
<g/>
,	,	kIx,	,
událostí	událost	k1gFnPc2	událost
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
primitivní	primitivní	k2eAgInSc4d1	primitivní
plastidy	plastid	k1gInPc4	plastid
u	u	k7c2	u
prvoka	prvok	k1gMnSc2	prvok
Paulinella	Paulinell	k1gMnSc2	Paulinell
chromatophora	chromatophor	k1gMnSc2	chromatophor
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
dodnes	dodnes	k6eAd1	dodnes
patrnou	patrný	k2eAgFnSc4d1	patrná
peptidoglykanovou	peptidoglykanový	k2eAgFnSc4d1	peptidoglykanový
stěnu	stěna	k1gFnSc4	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
mezičlánky	mezičlánek	k1gInPc1	mezičlánek
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
mezi	mezi	k7c7	mezi
endosymbiontem	endosymbiont	k1gInSc7	endosymbiont
a	a	k8xC	a
organelou	organela	k1gFnSc7	organela
se	se	k3xPyFc4	se
často	často	k6eAd1	často
nazývají	nazývat	k5eAaImIp3nP	nazývat
cyanely	cyanela	k1gFnPc1	cyanela
<g/>
.	.	kIx.	.
</s>
<s>
Poněkud	poněkud	k6eAd1	poněkud
známějším	známý	k2eAgInSc7d2	známější
příkladem	příklad	k1gInSc7	příklad
symbiózy	symbióza	k1gFnSc2	symbióza
sinic	sinice	k1gFnPc2	sinice
jsou	být	k5eAaImIp3nP	být
lišejníky	lišejník	k1gInPc1	lišejník
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
cyanolišejníky	cyanolišejník	k1gInPc1	cyanolišejník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
asi	asi	k9	asi
v	v	k7c6	v
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
%	%	kIx~	%
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
sinice	sinice	k1gFnPc1	sinice
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
rodu	rod	k1gInSc3	rod
Nostoc	Nostoc	k1gInSc4	Nostoc
či	či	k8xC	či
Calothrix	Calothrix	k1gInSc4	Calothrix
<g/>
.	.	kIx.	.
</s>
<s>
Nostoc	Nostoc	k1gFnSc1	Nostoc
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
symbiotickém	symbiotický	k2eAgInSc6d1	symbiotický
svazku	svazek	k1gInSc6	svazek
také	také	k9	také
s	s	k7c7	s
houbou	houba	k1gFnSc7	houba
Geosiphon	Geosiphona	k1gFnPc2	Geosiphona
pyriforme	pyriform	k1gInSc5	pyriform
<g/>
,	,	kIx,	,
sinice	sinice	k1gFnSc1	sinice
Richelia	Richelia	k1gFnSc1	Richelia
uvnitř	uvnitř	k7c2	uvnitř
některých	některý	k3yIgFnPc2	některý
rozsivek	rozsivka	k1gFnPc2	rozsivka
(	(	kIx(	(
<g/>
Diatomeae	Diatomeae	k1gInSc1	Diatomeae
<g/>
)	)	kIx)	)
a	a	k8xC	a
jiný	jiný	k2eAgInSc1d1	jiný
rod	rod	k1gInSc1	rod
zase	zase	k9	zase
uvnitř	uvnitř	k7c2	uvnitř
buněk	buňka	k1gFnPc2	buňka
obrněnek	obrněnka	k1gFnPc2	obrněnka
(	(	kIx(	(
<g/>
Dinoflagellata	Dinoflagelle	k1gNnPc1	Dinoflagelle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
rostliny	rostlina	k1gFnPc1	rostlina
mohou	moct	k5eAaImIp3nP	moct
hostit	hostit	k5eAaImF	hostit
sinice	sinice	k1gFnSc1	sinice
<g/>
:	:	kIx,	:
Nostoc	Nostoc	k1gFnSc1	Nostoc
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
krytosemenných	krytosemenný	k2eAgFnPc6d1	krytosemenná
rostlinách	rostlina	k1gFnPc6	rostlina
rodu	rod	k1gInSc2	rod
Gunnera	Gunner	k1gMnSc2	Gunner
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
kořenech	kořen	k1gInPc6	kořen
mnohých	mnohý	k2eAgInPc2d1	mnohý
cykasů	cykas	k1gInPc2	cykas
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
sinice	sinice	k1gFnPc1	sinice
žijí	žít	k5eAaImIp3nP	žít
uvnitř	uvnitř	k7c2	uvnitř
těl	tělo	k1gNnPc2	tělo
kapradinek	kapradinka	k1gFnPc2	kapradinka
rodu	rod	k1gInSc2	rod
Azola	Azola	k1gFnSc1	Azola
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
uvnitř	uvnitř	k7c2	uvnitř
těl	tělo	k1gNnPc2	tělo
mořských	mořský	k2eAgFnPc2d1	mořská
hub	houba	k1gFnPc2	houba
(	(	kIx(	(
<g/>
Porifera	Porifera	k1gFnSc1	Porifera
<g/>
)	)	kIx)	)
a	a	k8xC	a
u	u	k7c2	u
pláštěnců	pláštěnec	k1gMnPc2	pláštěnec
(	(	kIx(	(
<g/>
Tunicata	Tunicata	k1gFnSc1	Tunicata
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
přítomny	přítomen	k2eAgFnPc1d1	přítomna
fotosyntetizující	fotosyntetizující	k2eAgFnPc1d1	fotosyntetizující
sinice	sinice	k1gFnPc1	sinice
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
pak	pak	k6eAd1	pak
jsou	být	k5eAaImIp3nP	být
nazývány	nazýván	k2eAgFnPc4d1	nazývána
zoocyanely	zoocyanela	k1gFnPc4	zoocyanela
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
sinic	sinice	k1gFnPc2	sinice
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
i	i	k8xC	i
jejich	jejich	k3xOp3gInPc1	jejich
virové	virový	k2eAgInPc1d1	virový
patogeny	patogen	k1gInPc1	patogen
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
cyanofágy	cyanofága	k1gFnPc1	cyanofága
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
genetickým	genetický	k2eAgInSc7d1	genetický
materiálem	materiál	k1gInSc7	materiál
je	být	k5eAaImIp3nS	být
zásadně	zásadně	k6eAd1	zásadně
dvouvláknová	dvouvláknový	k2eAgFnSc1d1	dvouvláknová
DNA	dna	k1gFnSc1	dna
<g/>
.	.	kIx.	.
</s>
<s>
Známými	známý	k2eAgInPc7d1	známý
viry	vir	k1gInPc7	vir
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
sladkovodní	sladkovodní	k2eAgInSc4d1	sladkovodní
as-	as-	k?	as-
<g/>
1	[number]	k4	1
a	a	k8xC	a
lpp-	lpp-	k?	lpp-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Viry	vir	k1gInPc1	vir
významně	významně	k6eAd1	významně
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
početnost	početnost	k1gFnSc4	početnost
sinic	sinice	k1gFnPc2	sinice
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
průzkumy	průzkum	k1gInPc1	průzkum
například	například	k6eAd1	například
objevily	objevit	k5eAaPmAgInP	objevit
viry	vir	k1gInPc1	vir
v	v	k7c6	v
téměř	téměř	k6eAd1	téměř
všech	všecek	k3xTgFnPc2	všecek
studovaných	studovaný	k2eAgFnPc2d1	studovaná
populací	populace	k1gFnPc2	populace
sinic	sinice	k1gFnPc2	sinice
<g/>
.	.	kIx.	.
</s>
<s>
Ekologické	ekologický	k2eAgInPc1d1	ekologický
výzkumy	výzkum	k1gInPc1	výzkum
v	v	k7c6	v
oceánu	oceán	k1gInSc6	oceán
také	také	k9	také
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
mají	mít	k5eAaImIp3nP	mít
sinice	sinice	k1gFnPc1	sinice
rodu	rod	k1gInSc2	rod
Synechococcus	Synechococcus	k1gInSc1	Synechococcus
daleko	daleko	k6eAd1	daleko
vyšší	vysoký	k2eAgFnSc4d2	vyšší
rezistenci	rezistence	k1gFnSc4	rezistence
vůči	vůči	k7c3	vůči
svým	svůj	k3xOyFgInPc3	svůj
virům	vir	k1gInPc3	vir
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
na	na	k7c6	na
mělčině	mělčina	k1gFnSc6	mělčina
častěji	často	k6eAd2	často
setkávají	setkávat	k5eAaImIp3nP	setkávat
a	a	k8xC	a
přírodní	přírodní	k2eAgInSc4d1	přírodní
výběr	výběr	k1gInSc4	výběr
zde	zde	k6eAd1	zde
tedy	tedy	k9	tedy
působí	působit	k5eAaImIp3nS	působit
daleko	daleko	k6eAd1	daleko
silněji	silně	k6eAd2	silně
<g/>
.	.	kIx.	.
</s>
<s>
Sinice	sinice	k1gFnPc1	sinice
mají	mít	k5eAaImIp3nP	mít
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
nezanedbatelný	zanedbatelný	k2eNgInSc4d1	nezanedbatelný
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Nepočítaje	nepočítaje	k7c4	nepočítaje
negativní	negativní	k2eAgInSc4d1	negativní
vliv	vliv	k1gInSc4	vliv
vodního	vodní	k2eAgInSc2d1	vodní
květu	květ	k1gInSc2	květ
na	na	k7c4	na
ekonomiku	ekonomika	k1gFnSc4	ekonomika
a	a	k8xC	a
lidské	lidský	k2eAgNnSc4d1	lidské
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
sinice	sinice	k1gFnPc1	sinice
mnoho	mnoho	k6eAd1	mnoho
kladných	kladný	k2eAgFnPc2d1	kladná
stránek	stránka	k1gFnPc2	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Sinice	sinice	k1gFnPc1	sinice
především	především	k9	především
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
v	v	k7c6	v
sušině	sušina	k1gFnSc6	sušina
vysoké	vysoký	k2eAgFnSc2d1	vysoká
koncentrace	koncentrace	k1gFnSc2	koncentrace
proteinů	protein	k1gInPc2	protein
(	(	kIx(	(
<g/>
až	až	k9	až
70	[number]	k4	70
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
než	než	k8xS	než
například	například	k6eAd1	například
zelené	zelený	k2eAgFnPc1d1	zelená
řasy	řasa	k1gFnPc1	řasa
<g/>
.	.	kIx.	.
</s>
<s>
Sinice	sinice	k1gFnSc1	sinice
rodu	rod	k1gInSc2	rod
Arthrospira	Arthrospira	k1gFnSc1	Arthrospira
(	(	kIx(	(
<g/>
známá	známá	k1gFnSc1	známá
pod	pod	k7c7	pod
komerčním	komerční	k2eAgInSc7d1	komerční
názvem	název	k1gInSc7	název
Spirulina	Spirulin	k2eAgFnSc1d1	Spirulina
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
vitamínových	vitamínový	k2eAgFnPc2d1	vitamínová
tablet	tableta	k1gFnPc2	tableta
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
vitamíny	vitamín	k1gInPc4	vitamín
(	(	kIx(	(
<g/>
vitamín	vitamín	k1gInSc4	vitamín
B	B	kA	B
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
a	a	k8xC	a
karoteny	karoten	k1gInPc1	karoten
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
v	v	k7c6	v
nezápadních	západní	k2eNgFnPc6d1	nezápadní
civilizacích	civilizace	k1gFnPc6	civilizace
byly	být	k5eAaImAgFnP	být
sinice	sinice	k1gFnPc1	sinice
dříve	dříve	k6eAd2	dříve
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
jídelníčku	jídelníček	k1gInSc2	jídelníček
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
(	(	kIx(	(
<g/>
Čad	Čad	k1gInSc1	Čad
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
pokrmů	pokrm	k1gInPc2	pokrm
užívají	užívat	k5eAaImIp3nP	užívat
dosud	dosud	k6eAd1	dosud
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
pigmenty	pigment	k1gInPc1	pigment
sinic	sinice	k1gFnPc2	sinice
(	(	kIx(	(
<g/>
fykobiliny	fykobilin	k1gInPc1	fykobilin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
fykocyanin	fykocyanin	k2eAgInSc1d1	fykocyanin
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
netoxická	toxický	k2eNgNnPc4d1	netoxické
barviva	barvivo	k1gNnPc4	barvivo
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
nimž	jenž	k3xRgFnPc3	jenž
se	se	k3xPyFc4	se
pozorují	pozorovat	k5eAaImIp3nP	pozorovat
metabolické	metabolický	k2eAgInPc4d1	metabolický
procesy	proces	k1gInPc4	proces
probíhající	probíhající	k2eAgInSc4d1	probíhající
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
různých	různý	k2eAgInPc2d1	různý
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
látky	látka	k1gFnPc1	látka
obsažené	obsažený	k2eAgFnPc1d1	obsažená
v	v	k7c6	v
sinicích	sinice	k1gFnPc6	sinice
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgInP	moct
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
stát	stát	k5eAaPmF	stát
výchozí	výchozí	k2eAgFnSc4d1	výchozí
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
protirakovinných	protirakovinný	k2eAgInPc2d1	protirakovinný
a	a	k8xC	a
protizánětlivých	protizánětlivý	k2eAgInPc2d1	protizánětlivý
léků	lék	k1gInPc2	lék
<g/>
,	,	kIx,	,
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
a	a	k8xC	a
antivirotik	antivirotika	k1gFnPc2	antivirotika
<g/>
.	.	kIx.	.
</s>
<s>
Kosmické	kosmický	k2eAgFnPc1d1	kosmická
agentury	agentura	k1gFnPc1	agentura
NASA	NASA	kA	NASA
a	a	k8xC	a
ESA	eso	k1gNnSc2	eso
uvažují	uvažovat	k5eAaImIp3nP	uvažovat
o	o	k7c6	o
zapojení	zapojení	k1gNnSc6	zapojení
sinic	sinice	k1gFnPc2	sinice
do	do	k7c2	do
stravy	strava	k1gFnSc2	strava
kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
na	na	k7c6	na
budoucích	budoucí	k2eAgFnPc6d1	budoucí
vesmírných	vesmírný	k2eAgFnPc6d1	vesmírná
misích	mise	k1gFnPc6	mise
na	na	k7c4	na
velkou	velký	k2eAgFnSc4d1	velká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
sinice	sinice	k1gFnPc1	sinice
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
růst	růst	k5eAaImF	růst
i	i	k9	i
na	na	k7c6	na
měsíční	měsíční	k2eAgFnSc6d1	měsíční
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
Uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
se	se	k3xPyFc4	se
o	o	k7c4	o
užití	užití	k1gNnSc4	užití
sinic	sinice	k1gFnPc2	sinice
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
biopaliv	biopalit	k5eAaPmDgMnS	biopalit
<g/>
.	.	kIx.	.
</s>
