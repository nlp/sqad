<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
známé	známý	k2eAgInPc1d1	známý
důkazy	důkaz	k1gInPc1	důkaz
o	o	k7c6	o
existenci	existence	k1gFnSc6	existence
sinic	sinice	k1gFnPc2	sinice
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
fosílií	fosílie	k1gFnPc2	fosílie
jsou	být	k5eAaImIp3nP	být
staré	stará	k1gFnSc2	stará
3,5	[number]	k4	3,5
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
a	a	k8xC	a
pochází	pocházet	k5eAaImIp3nS	pocházet
především	především	k6eAd1	především
z	z	k7c2	z
formace	formace	k1gFnSc2	formace
Apex	apex	k1gInSc1	apex
Chert	Chert	k1gInSc1	Chert
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
