<s>
Městský	městský	k2eAgInSc1d1	městský
prapor	prapor	k1gInSc1	prapor
města	město	k1gNnSc2	město
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
má	mít	k5eAaImIp3nS	mít
list	list	k1gInSc1	list
nakoso	nakoso	k6eAd1	nakoso
dělený	dělený	k2eAgInSc1d1	dělený
<g/>
,	,	kIx,	,
horní	horní	k2eAgFnSc6d1	horní
pole	pola	k1gFnSc6	pola
žluté	žlutý	k2eAgFnSc6d1	žlutá
<g/>
,	,	kIx,	,
dolní	dolní	k2eAgFnSc6d1	dolní
červené	červená	k1gFnSc6	červená
<g/>
,	,	kIx,	,
na	na	k7c6	na
třetinách	třetina	k1gFnPc6	třetina
listu	list	k1gInSc2	list
jsou	být	k5eAaImIp3nP	být
postaveny	postavit	k5eAaPmNgInP	postavit
odvrácené	odvrácený	k2eAgInPc1d1	odvrácený
vinařské	vinařský	k2eAgInPc1d1	vinařský
nože	nůž	k1gInPc1	nůž
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
čepele	čepel	k1gFnSc2	čepel
nožů	nůž	k1gInPc2	nůž
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
šířky	šířka	k1gFnSc2	šířka
listu	list	k1gInSc2	list
a	a	k8xC	a
střenky	střenka	k1gFnSc2	střenka
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
šířky	šířka	k1gFnPc4	šířka
listu	list	k1gInSc2	list
<g/>
,	,	kIx,	,
čepele	čepel	k1gFnPc1	čepel
jsou	být	k5eAaImIp3nP	být
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
,	,	kIx,	,
střenky	střenka	k1gFnPc1	střenka
hnědé	hnědý	k2eAgFnPc1d1	hnědá
<g/>
,	,	kIx,	,
poměr	poměr	k1gInSc1	poměr
délky	délka	k1gFnSc2	délka
k	k	k7c3	k
šířce	šířka	k1gFnSc3	šířka
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
:	:	kIx,	:
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
