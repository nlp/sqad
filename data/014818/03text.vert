<s>
Chipotle	Chipotle	k6eAd1
</s>
<s>
Chipotle	Chipotle	k1gFnSc1
<g/>
,	,	kIx,
kultivar	kultivar	k1gInSc1
’	’	k?
<g/>
morita	morita	k1gFnSc1
<g/>
’	’	k?
</s>
<s>
Pálivost	pálivost	k1gFnSc1
3000	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
000	#num#	k4
SHU	SHU	kA
</s>
<s>
Chipotle	Chipotlat	k5eAaPmIp3nS
(	(	kIx(
<g/>
čti	číst	k5eAaImRp2nS
čipotle	čipotle	k1gFnSc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
také	také	k9
chilpotle	chilpotle	k6eAd1
je	být	k5eAaImIp3nS
výraz	výraz	k1gInSc1
pocházející	pocházející	k2eAgInSc1d1
z	z	k7c2
jazyka	jazyk	k1gInSc2
nahuatl	nahuatnout	k5eAaPmAgInS
a	a	k8xC
znamenající	znamenající	k2eAgFnSc1d1
uzená	uzený	k2eAgFnSc1d1
chilli	chilli	k1gNnSc7
paprička	paprička	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
vyuzená	vyuzený	k2eAgFnSc1d1
a	a	k8xC
vysušená	vysušený	k2eAgFnSc1d1
paprička	paprička	k1gFnSc1
jalapeñ	jalapeñ	k6eAd1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
především	především	k9
v	v	k7c6
mexické	mexický	k2eAgFnSc6d1
kuchyni	kuchyně	k1gFnSc6
a	a	k8xC
v	v	k7c6
kuchyních	kuchyně	k1gFnPc6
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
jí	on	k3xPp3gFnSc7
jsou	být	k5eAaImIp3nP
inspirovány	inspirován	k2eAgFnPc1d1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
například	například	k6eAd1
Tex-Mex	Tex-Mex	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jejich	jejich	k3xOp3gFnSc6
chuti	chuť	k1gFnSc6
se	se	k3xPyFc4
pálivost	pálivost	k1gFnSc1
původních	původní	k2eAgFnPc2d1
papriček	paprička	k1gFnPc2
spojuje	spojovat	k5eAaImIp3nS
s	s	k7c7
uzenou	uzený	k2eAgFnSc7d1
příchutí	příchuť	k1gFnSc7
dýmu	dým	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1
odrůdy	odrůda	k1gFnPc1
papriček	paprička	k1gFnPc2
jalapeñ	jalapeñ	k6eAd1
se	se	k3xPyFc4
od	od	k7c2
sebe	se	k3xPyFc2
liší	lišit	k5eAaImIp3nP
velikostí	velikost	k1gFnPc2
a	a	k8xC
pálivostí	pálivost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Mexiku	Mexiko	k1gNnSc6
jsou	být	k5eAaImIp3nP
jalapeñ	jalapeñ	k6eAd1
známé	známý	k2eAgFnPc1d1
také	také	k9
jako	jako	k9
cuaresmeñ	cuaresmeñ	k1gNnSc1
nebo	nebo	k8xC
gordo	gordo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
donedávna	donedávna	k6eAd1
se	se	k3xPyFc4
uzené	uzený	k2eAgFnPc1d1
papričky	paprička	k1gFnPc1
chipotle	chipotle	k6eAd1
objevovaly	objevovat	k5eAaImAgFnP
převážně	převážně	k6eAd1
na	na	k7c6
trzích	trh	k1gInPc6
ve	v	k7c6
středním	střední	k2eAgNnSc6d1
a	a	k8xC
jižním	jižní	k2eAgNnSc6d1
Mexiku	Mexiko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
rostla	růst	k5eAaImAgFnS
jejich	jejich	k3xOp3gFnSc1
popularita	popularita	k1gFnSc1
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
,	,	kIx,
zejména	zejména	k9
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
a	a	k8xC
v	v	k7c6
Kanadě	Kanada	k1gFnSc6
<g/>
,	,	kIx,
začala	začít	k5eAaPmAgFnS
se	se	k3xPyFc4
výroba	výroba	k1gFnSc1
a	a	k8xC
zpracování	zpracování	k1gNnSc1
papriček	paprička	k1gFnPc2
jalapeñ	jalapeñ	k6eAd1
rozšiřovat	rozšiřovat	k5eAaImF
i	i	k9
do	do	k7c2
severních	severní	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
Mexika	Mexiko	k1gNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
lépe	dobře	k6eAd2
zásobován	zásobován	k2eAgInSc4d1
jihozápad	jihozápad	k1gInSc4
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
se	se	k3xPyFc4
tyto	tento	k3xDgFnPc1
papričky	paprička	k1gFnPc1
pěstují	pěstovat	k5eAaImIp3nP
a	a	k8xC
zpracovávají	zpracovávat	k5eAaImIp3nP
i	i	k9
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Chipotle	Chipotle	k6eAd1
má	mít	k5eAaImIp3nS
pálivost	pálivost	k1gFnSc1
mezi	mezi	k7c7
3000	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
000	#num#	k4
Scovilleových	Scovilleův	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
(	(	kIx(
<g/>
SHU	SHU	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tak	tak	k9
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
do	do	k7c2
pálivosti	pálivost	k1gFnSc2
srovnatelná	srovnatelný	k2eAgFnSc1d1
s	s	k7c7
francouzskou	francouzský	k2eAgFnSc7d1
papričkou	paprička	k1gFnSc7
Espelette	Espelett	k1gInSc5
<g/>
,	,	kIx,
mexickou	mexický	k2eAgFnSc4d1
Guajillo	Guajillo	k1gNnSc1
<g/>
,	,	kIx,
maďarskou	maďarský	k2eAgFnSc7d1
feferonkou	feferonka	k1gFnSc7
a	a	k8xC
s	s	k7c7
omáčkou	omáčka	k1gFnSc7
Tabasco	Tabasco	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Příprava	příprava	k1gFnSc1
</s>
<s>
Obvyklý	obvyklý	k2eAgInSc1d1
postup	postup	k1gInSc1
vypadá	vypadat	k5eAaImIp3nS,k5eAaPmIp3nS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
sběrači	sběrač	k1gMnPc1
často	často	k6eAd1
procházejí	procházet	k5eAaImIp3nP
po	po	k7c6
pozemku	pozemek	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
papričky	paprička	k1gFnPc1
jalapeñ	jalapeñ	k6eAd1
pěstují	pěstovat	k5eAaImIp3nP
<g/>
,	,	kIx,
a	a	k8xC
sklízejí	sklízet	k5eAaImIp3nP
přitom	přitom	k6eAd1
nezralé	zralý	k2eNgFnPc1d1
zelené	zelený	k2eAgFnPc1d1
papričky	paprička	k1gFnPc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
jsou	být	k5eAaImIp3nP
určeny	určit	k5eAaPmNgFnP
pro	pro	k7c4
trh	trh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
vegetačního	vegetační	k2eAgNnSc2d1
období	období	k1gNnSc2
papričky	paprička	k1gFnSc2
přirozeně	přirozeně	k6eAd1
dozrávají	dozrávat	k5eAaImIp3nP
a	a	k8xC
získávají	získávat	k5eAaImIp3nP
jasně	jasně	k6eAd1
červenou	červený	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Mexiku	Mexiko	k1gNnSc6
a	a	k8xC
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
mají	mít	k5eAaImIp3nP
zralé	zralý	k2eAgFnPc1d1
červené	červený	k2eAgFnPc1d1
papričky	paprička	k1gFnPc1
jalapeñ	jalapeñ	k6eAd1
zaručený	zaručený	k2eAgInSc4d1
odbyt	odbyt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Papričky	paprička	k1gFnPc4
určené	určený	k2eAgFnPc1d1
k	k	k7c3
sušení	sušení	k1gNnSc3
se	se	k3xPyFc4
nechávají	nechávat	k5eAaImIp3nP
na	na	k7c6
keřících	keřík	k1gInPc6
co	co	k9
nejdéle	dlouho	k6eAd3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
získají	získat	k5eAaPmIp3nP
temně	temně	k6eAd1
červenou	červený	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
a	a	k8xC
ztratí	ztratit	k5eAaPmIp3nS
značnou	značný	k2eAgFnSc4d1
část	část	k1gFnSc4
své	svůj	k3xOyFgFnSc2
vlhkosti	vlhkost	k1gFnSc2
<g/>
,	,	kIx,
přichází	přicházet	k5eAaImIp3nS
čas	čas	k1gInSc1
jejich	jejich	k3xOp3gFnSc2
sklizně	sklizeň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
se	se	k3xPyFc4
sklizená	sklizený	k2eAgNnPc1d1
jalapeñ	jalapeñ	k1gNnPc1
převezou	převézt	k5eAaPmIp3nP
do	do	k7c2
uzavřených	uzavřený	k2eAgFnPc2d1
udíren	udírna	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	on	k3xPp3gInPc4
dělníci	dělník	k1gMnPc1
rozmístí	rozmístit	k5eAaPmIp3nP
na	na	k7c4
kovové	kovový	k2eAgInPc4d1
rošty	rošt	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
topeniště	topeniště	k1gNnSc2
se	se	k3xPyFc4
naskládá	naskládat	k5eAaPmIp3nS
dřevo	dřevo	k1gNnSc1
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
kouř	kouř	k1gInSc1
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
veden	vést	k5eAaImNgInS
do	do	k7c2
uzavřeného	uzavřený	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
udírny	udírna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Papričky	paprička	k1gFnPc4
se	s	k7c7
vždy	vždy	k6eAd1
po	po	k7c6
několika	několik	k4yIc6
hodinách	hodina	k1gFnPc6
promíchají	promíchat	k5eAaPmIp3nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
lépe	dobře	k6eAd2
proudily	proudit	k5eAaPmAgInP,k5eAaImAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uzení	uzení	k1gNnSc1
probíhá	probíhat	k5eAaImIp3nS
po	po	k7c4
několik	několik	k4yIc4
dní	den	k1gInPc2
<g/>
,	,	kIx,
dokud	dokud	k8xS
se	se	k3xPyFc4
jalapeñ	jalapeñ	k6eAd1
nezbaví	zbavit	k5eNaPmIp3nS
většiny	většina	k1gFnSc2
vlhkosti	vlhkost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledek	výsledek	k1gInSc1
pak	pak	k6eAd1
vzhledově	vzhledově	k6eAd1
poněkud	poněkud	k6eAd1
připomíná	připomínat	k5eAaImIp3nS
sušené	sušený	k2eAgNnSc4d1
ovoce	ovoce	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
pěti	pět	k4xCc2
kilogramů	kilogram	k1gInPc2
papriček	paprička	k1gFnPc2
jalapeñ	jalapeñ	k6eAd1
vznikne	vzniknout	k5eAaPmIp3nS
asi	asi	k9
půl	půl	k1xP
kila	kilo	k1gNnSc2
uzených	uzené	k1gNnPc2
chipotle	chipotlat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odrůdy	odrůda	k1gFnPc1
</s>
<s>
Chipotle	Chipotle	k1gFnSc1
odrůdy	odrůda	k1gFnSc2
meco	meco	k6eAd1
</s>
<s>
Většina	většina	k1gFnSc1
chipotle	chipotle	k6eAd1
pochází	pocházet	k5eAaImIp3nS
ze	z	k7c2
severomexického	severomexický	k2eAgInSc2d1
státu	stát	k1gInSc2
Chihuahua	Chihuahu	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
odrůda	odrůda	k1gFnSc1
je	být	k5eAaImIp3nS
známa	znám	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
morita	morit	k2eAgFnSc1d1
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
malá	malý	k2eAgFnSc1d1
moruše	moruše	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
středním	střední	k2eAgNnSc6d1
a	a	k8xC
jižním	jižní	k2eAgNnSc6d1
Mexiku	Mexiko	k1gNnSc6
jsou	být	k5eAaImIp3nP
chipotle	chipotle	k6eAd1
známé	známý	k2eAgFnPc1d1
jako	jako	k8xS,k8xC
chile	chile	k6eAd1
meco	meco	k6eAd1
<g/>
,	,	kIx,
chile	chile	k6eAd1
ahumado	ahumada	k1gFnSc5
nebo	nebo	k8xC
típico	típico	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
moritas	moritas	k1gMnSc1
z	z	k7c2
Chihuahuy	Chihuahua	k1gFnSc2
mají	mít	k5eAaImIp3nP
purpurovou	purpurový	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
<g/>
,	,	kIx,
chile	chile	k6eAd1
meco	meco	k6eAd1
jsou	být	k5eAaImIp3nP
hnědé	hnědý	k2eAgFnPc1d1
až	až	k6eAd1
našedlé	našedlý	k2eAgFnPc1d1
a	a	k8xC
vypadají	vypadat	k5eAaImIp3nP,k5eAaPmIp3nP
jako	jako	k9
nedopalek	nedopalek	k1gInSc4
doutníku	doutník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
chipotle	chipotle	k1gFnSc2
v	v	k7c6
USA	USA	kA
jsou	být	k5eAaImIp3nP
papričky	paprička	k1gFnPc1
odrůdy	odrůda	k1gFnSc2
morita	morita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téměř	téměř	k6eAd1
všechny	všechen	k3xTgFnPc4
chipotle	chipotle	k6eAd1
meco	meco	k6eAd1
se	se	k3xPyFc4
spotřebují	spotřebovat	k5eAaPmIp3nP
v	v	k7c6
samotném	samotný	k2eAgNnSc6d1
Mexiku	Mexiko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Domácí	domácí	k2eAgInSc1d1
chipotles	chipotles	k1gInSc1
en	en	k?
adobo	adoba	k1gFnSc5
</s>
<s>
Použití	použití	k1gNnSc1
</s>
<s>
Uzené	uzený	k2eAgFnPc1d1
papričky	paprička	k1gFnPc1
chipotle	chipotle	k6eAd1
dodávají	dodávat	k5eAaImIp3nP
řadě	řada	k1gFnSc6
pokrmů	pokrm	k1gInPc2
mexické	mexický	k2eAgFnSc2d1
kuchyně	kuchyně	k1gFnSc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc7
hlavní	hlavní	k2eAgFnSc7d1
přísadou	přísada	k1gFnSc7
často	často	k6eAd1
bývají	bývat	k5eAaImIp3nP
<g/>
,	,	kIx,
poměrně	poměrně	k6eAd1
mírnou	mírný	k2eAgFnSc4d1
<g/>
,	,	kIx,
avšak	avšak	k8xC
zemitou	zemitý	k2eAgFnSc4d1
štiplavost	štiplavost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používají	používat	k5eAaImIp3nP
se	se	k3xPyFc4
také	také	k9
k	k	k7c3
přípravě	příprava	k1gFnSc3
různých	různý	k2eAgFnPc2d1
omáček	omáčka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohou	moct	k5eAaImIp3nP
se	se	k3xPyFc4
rozemlít	rozemlít	k5eAaPmF
a	a	k8xC
smíchané	smíchaný	k2eAgNnSc4d1
s	s	k7c7
dalším	další	k2eAgInSc7d1
kořením	kořenit	k5eAaImIp1nS
použít	použít	k5eAaPmF
pro	pro	k7c4
přípravu	příprava	k1gFnSc4
marinád	marináda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chipotle	Chipotle	k1gFnSc1
se	se	k3xPyFc4
často	často	k6eAd1
užívají	užívat	k5eAaImIp3nP
(	(	kIx(
<g/>
obvykle	obvykle	k6eAd1
v	v	k7c6
mleté	mletý	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
přísada	přísada	k1gFnSc1
pro	pro	k7c4
různé	různý	k2eAgMnPc4d1
domácí	domácí	k1gMnPc4
i	i	k9
průmyslově	průmyslově	k6eAd1
vyráběné	vyráběný	k2eAgFnPc1d1
omáčky	omáčka	k1gFnPc1
pro	pro	k7c4
barbecue	barbecue	k1gNnSc4
<g/>
,	,	kIx,
pro	pro	k7c4
běžné	běžný	k2eAgNnSc4d1
grilování	grilování	k1gNnSc4
<g/>
,	,	kIx,
případně	případně	k6eAd1
pro	pro	k7c4
chili	chile	k1gFnSc4
nebo	nebo	k8xC
dušené	dušený	k2eAgInPc4d1
pokrmy	pokrm	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průmyslově	průmyslově	k6eAd1
vyráběné	vyráběný	k2eAgFnPc4d1
pochutiny	pochutina	k1gFnPc4
mívají	mívat	k5eAaImIp3nP
na	na	k7c6
etiketě	etiketa	k1gFnSc6
obvykle	obvykle	k6eAd1
poznámku	poznámka	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
obsahují	obsahovat	k5eAaImIp3nP
chipotle	chipotle	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Tyto	tyt	k2eAgNnSc4d1
chilli	chilli	k1gNnSc4
papričky	paprička	k1gFnSc2
jsou	být	k5eAaImIp3nP
pálivé	pálivý	k2eAgFnPc1d1
a	a	k8xC
mají	mít	k5eAaImIp3nP
výraznou	výrazný	k2eAgFnSc4d1
kouřovou	kouřový	k2eAgFnSc4d1
chuť	chuť	k1gFnSc4
a	a	k8xC
vůni	vůně	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
silné	silný	k2eAgFnSc3d1
dužině	dužina	k1gFnSc3
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
používají	používat	k5eAaImIp3nP
do	do	k7c2
jídel	jídlo	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
se	se	k3xPyFc4
připravují	připravovat	k5eAaImIp3nP
na	na	k7c6
mírném	mírný	k2eAgInSc6d1
ohni	oheň	k1gInSc6
<g/>
,	,	kIx,
spíš	spíš	k9
než	než	k8xS
v	v	k7c6
syrovém	syrový	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celé	celý	k2eAgFnPc4d1
chipotle	chipotle	k1gFnPc4
se	se	k3xPyFc4
přidávají	přidávat	k5eAaImIp3nP
do	do	k7c2
polévek	polévka	k1gFnPc2
<g/>
,	,	kIx,
do	do	k7c2
dušených	dušený	k2eAgInPc2d1
pokrmů	pokrm	k1gInPc2
či	či	k8xC
do	do	k7c2
gulášů	guláš	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohou	moct	k5eAaImIp3nP
také	také	k9
okořenit	okořenit	k5eAaPmF
pokrmy	pokrm	k1gInPc4
z	z	k7c2
fazolí	fazole	k1gFnPc2
nebo	nebo	k8xC
z	z	k7c2
čočky	čočka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Chipotle	Chipotle	k1gFnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Processing	Processing	k1gInSc1
Peppers	Peppers	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Noticias	Noticias	k1gInSc1
al	ala	k1gFnPc2
momento	momento	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
ahoramismo	ahoramismo	k6eAd1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
<g/>
mx	mx	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Gastronomie	gastronomie	k1gFnSc1
</s>
