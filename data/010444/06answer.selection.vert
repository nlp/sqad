<s>
Trie	Trie	k6eAd1	Trie
nebo	nebo	k8xC	nebo
prefixový	prefixový	k2eAgInSc1d1	prefixový
strom	strom	k1gInSc1	strom
je	být	k5eAaImIp3nS	být
datová	datový	k2eAgFnSc1d1	datová
struktura	struktura	k1gFnSc1	struktura
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
uchovávání	uchovávání	k1gNnPc4	uchovávání
dvojic	dvojice	k1gFnPc2	dvojice
klíč-hodnota	klíčodnota	k1gFnSc1	klíč-hodnota
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
klíči	klíč	k1gInPc7	klíč
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
řetězce	řetězec	k1gInSc2	řetězec
<g/>
.	.	kIx.	.
</s>
