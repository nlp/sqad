<p>
<s>
Trie	Trie	k6eAd1	Trie
nebo	nebo	k8xC	nebo
prefixový	prefixový	k2eAgInSc1d1	prefixový
strom	strom	k1gInSc1	strom
je	být	k5eAaImIp3nS	být
datová	datový	k2eAgFnSc1d1	datová
struktura	struktura	k1gFnSc1	struktura
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
uchovávání	uchovávání	k1gNnPc4	uchovávání
dvojic	dvojice	k1gFnPc2	dvojice
klíč-hodnota	klíčodnota	k1gFnSc1	klíč-hodnota
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
klíči	klíč	k1gInPc7	klíč
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
řetězce	řetězec	k1gInSc2	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
binárního	binární	k2eAgInSc2d1	binární
vyhledávacího	vyhledávací	k2eAgInSc2d1	vyhledávací
stromu	strom	k1gInSc2	strom
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
hodnoty	hodnota	k1gFnSc2	hodnota
uzlu	uzel	k1gInSc2	uzel
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
větve	větev	k1gFnSc2	větev
sestoupit	sestoupit	k5eAaPmF	sestoupit
<g/>
,	,	kIx,	,
trie	triat	k5eAaPmIp3nS	triat
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
uzlu	uzel	k1gInSc6	uzel
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
všechny	všechen	k3xTgInPc4	všechen
podřetězce	podřetězec	k1gInPc4	podřetězec
<g/>
,	,	kIx,	,
kterými	který	k3yQgInPc7	který
může	moct	k5eAaImIp3nS	moct
pokračovat	pokračovat	k5eAaImF	pokračovat
řetězec	řetězec	k1gInSc4	řetězec
v	v	k7c6	v
dosud	dosud	k6eAd1	dosud
prohledané	prohledaný	k2eAgFnSc6d1	prohledaná
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
následníci	následník	k1gMnPc1	následník
uzlu	uzel	k1gInSc2	uzel
mají	mít	k5eAaImIp3nP	mít
společný	společný	k2eAgInSc4d1	společný
prefix	prefix	k1gInSc4	prefix
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
shodný	shodný	k2eAgInSc1d1	shodný
s	s	k7c7	s
řetězcem	řetězec	k1gInSc7	řetězec
přiřazeným	přiřazený	k2eAgInSc7d1	přiřazený
k	k	k7c3	k
danému	daný	k2eAgInSc3d1	daný
uzlu	uzel	k1gInSc3	uzel
<g/>
.	.	kIx.	.
</s>
<s>
Kořen	kořen	k1gInSc1	kořen
je	být	k5eAaImIp3nS	být
asociovaný	asociovaný	k2eAgInSc1d1	asociovaný
s	s	k7c7	s
prázdným	prázdný	k2eAgInSc7d1	prázdný
řetězcem	řetězec	k1gInSc7	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
Hodnoty	hodnota	k1gFnPc1	hodnota
obvykle	obvykle	k6eAd1	obvykle
nejsou	být	k5eNaImIp3nP	být
asociovány	asociován	k2eAgFnPc1d1	asociována
se	se	k3xPyFc4	se
všemi	všecek	k3xTgInPc7	všecek
uzly	uzel	k1gInPc7	uzel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
s	s	k7c7	s
listy	list	k1gInPc7	list
a	a	k8xC	a
některými	některý	k3yIgInPc7	některý
vnitřními	vnitřní	k2eAgInPc7d1	vnitřní
uzly	uzel	k1gInPc7	uzel
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
klíčům	klíč	k1gInPc3	klíč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
trie	tri	k1gInSc2	tri
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
"	"	kIx"	"
<g/>
retrieval	retrievat	k5eAaBmAgInS	retrievat
<g/>
"	"	kIx"	"
–	–	k?	–
získávání	získávání	k1gNnSc1	získávání
<g/>
,	,	kIx,	,
vyhledávání	vyhledávání	k1gNnSc1	vyhledávání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
uvedeném	uvedený	k2eAgInSc6d1	uvedený
příkladu	příklad	k1gInSc6	příklad
jsou	být	k5eAaImIp3nP	být
klíče	klíč	k1gInPc1	klíč
v	v	k7c6	v
uzlech	uzel	k1gInPc6	uzel
a	a	k8xC	a
hodnoty	hodnota	k1gFnPc1	hodnota
pod	pod	k7c7	pod
nimi	on	k3xPp3gInPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
trie	trie	k1gFnSc6	trie
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nazíráno	nazírat	k5eAaImNgNnS	nazírat
jako	jako	k8xS	jako
deterministický	deterministický	k2eAgInSc1d1	deterministický
konečný	konečný	k2eAgInSc1d1	konečný
automat	automat	k1gInSc1	automat
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
symbol	symbol	k1gInSc1	symbol
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
hraně	hrana	k1gFnSc6	hrana
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
implicitní	implicitní	k2eAgNnSc1d1	implicitní
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
větví	větev	k1gFnPc2	větev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
,	,	kIx,	,
trie	trie	k6eAd1	trie
nemusí	muset	k5eNaImIp3nS	muset
mít	mít	k5eAaImF	mít
jako	jako	k9	jako
klíče	klíč	k1gInPc1	klíč
textové	textový	k2eAgInPc1d1	textový
řetězce	řetězec	k1gInPc4	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
Stejné	stejný	k2eAgInPc1d1	stejný
algoritmy	algoritmus	k1gInPc1	algoritmus
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
snadno	snadno	k6eAd1	snadno
upraveny	upravit	k5eAaPmNgInP	upravit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
sloužily	sloužit	k5eAaImAgFnP	sloužit
podobným	podobný	k2eAgFnPc3d1	podobná
funkcím	funkce	k1gFnPc3	funkce
na	na	k7c6	na
seznamech	seznam	k1gInPc6	seznam
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
podoby	podoba	k1gFnSc2	podoba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
permutace	permutace	k1gFnSc1	permutace
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
číslic	číslice	k1gFnPc2	číslice
<g/>
,	,	kIx,	,
permutace	permutace	k1gFnSc1	permutace
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
tvarů	tvar	k1gInPc2	tvar
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
==	==	k?	==
Výhody	výhoda	k1gFnPc1	výhoda
a	a	k8xC	a
nevýhody	nevýhoda	k1gFnPc1	nevýhoda
trie	tri	k1gFnSc2	tri
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
binárnímu	binární	k2eAgInSc3d1	binární
vyhledávacímu	vyhledávací	k2eAgInSc3d1	vyhledávací
stromu	strom	k1gInSc3	strom
==	==	k?	==
</s>
</p>
<p>
<s>
Výhody	výhoda	k1gFnPc1	výhoda
oproti	oproti	k7c3	oproti
binárnímu	binární	k2eAgInSc3d1	binární
vyhledávacímu	vyhledávací	k2eAgInSc3d1	vyhledávací
stromu	strom	k1gInSc3	strom
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nalezení	nalezení	k1gNnSc1	nalezení
klíče	klíč	k1gInSc2	klíč
je	být	k5eAaImIp3nS	být
rychlejší	rychlý	k2eAgFnSc1d2	rychlejší
<g/>
.	.	kIx.	.
</s>
<s>
Nalezení	nalezení	k1gNnSc1	nalezení
klíče	klíč	k1gInSc2	klíč
délky	délka	k1gFnSc2	délka
m	m	kA	m
trvá	trvat	k5eAaImIp3nS	trvat
v	v	k7c6	v
nejhorším	zlý	k2eAgInSc6d3	Nejhorší
případě	případ	k1gInSc6	případ
O	O	kA	O
<g/>
(	(	kIx(	(
<g/>
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
BVS	BVS	kA	BVS
to	ten	k3xDgNnSc4	ten
trvá	trvat	k5eAaImIp3nS	trvat
O	O	kA	O
<g/>
(	(	kIx(	(
<g/>
log	log	kA	log
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k9	kde
n	n	k0	n
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
prvků	prvek	k1gInPc2	prvek
ve	v	k7c6	v
stromě	strom	k1gInSc6	strom
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vyhledávání	vyhledávání	k1gNnSc1	vyhledávání
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
hloubce	hloubka	k1gFnSc6	hloubka
stromu	strom	k1gInSc2	strom
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
logaritmická	logaritmický	k2eAgFnSc1d1	logaritmická
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
klíčů	klíč	k1gInPc2	klíč
<g/>
.	.	kIx.	.
</s>
<s>
Jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
operace	operace	k1gFnPc1	operace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
používá	používat	k5eAaImIp3nS	používat
trie	trie	k6eAd1	trie
během	během	k7c2	během
vyhledávání	vyhledávání	k1gNnSc2	vyhledávání
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
indexování	indexování	k1gNnSc1	indexování
pole	pole	k1gNnSc2	pole
pomocí	pomocí	k7c2	pomocí
znaku	znak	k1gInSc2	znak
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trie	Trie	k1gFnSc1	Trie
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
méně	málo	k6eAd2	málo
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
krátkých	krátký	k2eAgInPc2d1	krátký
řetězců	řetězec	k1gInPc2	řetězec
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
klíče	klíč	k1gInPc1	klíč
nejsou	být	k5eNaImIp3nP	být
uchovávány	uchovávat	k5eAaImNgInP	uchovávat
explicitně	explicitně	k6eAd1	explicitně
<g/>
,	,	kIx,	,
a	a	k8xC	a
uzly	uzel	k1gInPc1	uzel
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
sdíleny	sdílet	k5eAaImNgInP	sdílet
klíči	klíč	k1gInPc7	klíč
se	s	k7c7	s
společnými	společný	k2eAgInPc7d1	společný
začátky	začátek	k1gInPc7	začátek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trie	Trie	k6eAd1	Trie
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
nejdelšího	dlouhý	k2eAgInSc2d3	nejdelší
prefixu	prefix	k1gInSc2	prefix
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
úkolem	úkol	k1gInSc7	úkol
najít	najít	k5eAaPmF	najít
klíč	klíč	k1gInSc4	klíč
sdílející	sdílející	k2eAgInSc4d1	sdílející
nejdelší	dlouhý	k2eAgInSc4d3	nejdelší
možný	možný	k2eAgInSc4d1	možný
prefix	prefix	k1gInSc4	prefix
unikátních	unikátní	k2eAgInPc2d1	unikátní
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
<g/>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
trie	triat	k5eAaPmIp3nS	triat
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
dávková	dávkový	k2eAgFnSc1d1	dávková
nebo	nebo	k8xC	nebo
postupným	postupný	k2eAgNnSc7d1	postupné
přidáváním	přidávání	k1gNnSc7	přidávání
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
implementačně	implementačně	k6eAd1	implementačně
trochu	trochu	k6eAd1	trochu
složitější	složitý	k2eAgMnSc1d2	složitější
než	než	k8xS	než
u	u	k7c2	u
binárního	binární	k2eAgInSc2d1	binární
vyhledávacího	vyhledávací	k2eAgInSc2d1	vyhledávací
stromu	strom	k1gInSc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
zvláště	zvláště	k6eAd1	zvláště
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
optimalizací	optimalizace	k1gFnPc2	optimalizace
a	a	k8xC	a
optimalizovaných	optimalizovaný	k2eAgFnPc2d1	optimalizovaná
variant	varianta	k1gFnPc2	varianta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Užití	užití	k1gNnSc2	užití
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Jako	jako	k8xC	jako
náhrada	náhrada	k1gFnSc1	náhrada
jiných	jiný	k2eAgFnPc2d1	jiná
datových	datový	k2eAgFnPc2d1	datová
struktur	struktura	k1gFnPc2	struktura
===	===	k?	===
</s>
</p>
<p>
<s>
Trie	Trie	k1gFnSc1	Trie
může	moct	k5eAaImIp3nS	moct
nahradit	nahradit	k5eAaPmF	nahradit
hašovací	hašovací	k2eAgFnSc4d1	hašovací
tabulku	tabulka	k1gFnSc4	tabulka
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
které	který	k3yIgFnSc3	který
má	mít	k5eAaImIp3nS	mít
následující	následující	k2eAgMnSc1d1	následující
výhody	výhoda	k1gFnPc4	výhoda
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Vyhledání	vyhledání	k1gNnSc1	vyhledání
dat	datum	k1gNnPc2	datum
v	v	k7c4	v
trie	trie	k1gInSc4	trie
je	být	k5eAaImIp3nS	být
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
v	v	k7c6	v
nejhorším	zlý	k2eAgInSc6d3	Nejhorší
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
O	O	kA	O
<g/>
(	(	kIx(	(
<g/>
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
hašovací	hašovací	k2eAgFnSc3d1	hašovací
tabulce	tabulka	k1gFnSc3	tabulka
s	s	k7c7	s
kolizemi	kolize	k1gFnPc7	kolize
O	O	kA	O
<g/>
(	(	kIx(	(
<g/>
N	N	kA	N
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
však	však	k9	však
O	o	k7c6	o
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nenastávají	nastávat	k5eNaImIp3nP	nastávat
žádné	žádný	k3yNgFnPc4	žádný
kolize	kolize	k1gFnPc4	kolize
různých	různý	k2eAgInPc2d1	různý
klíčů	klíč	k1gInPc2	klíč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Datová	datový	k2eAgFnSc1d1	datová
struktura	struktura	k1gFnSc1	struktura
pro	pro	k7c4	pro
uchovávání	uchovávání	k1gNnSc4	uchovávání
kolizních	kolizní	k2eAgInPc2d1	kolizní
klíčů	klíč	k1gInPc2	klíč
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
více	hodně	k6eAd2	hodně
hodnot	hodnota	k1gFnPc2	hodnota
má	mít	k5eAaImIp3nS	mít
přiřazeno	přiřazen	k2eAgNnSc4d1	přiřazeno
totožný	totožný	k2eAgInSc4d1	totožný
klíč	klíč	k1gInSc4	klíč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
hašovací	hašovací	k2eAgFnSc1d1	hašovací
funkce	funkce	k1gFnSc1	funkce
nebo	nebo	k8xC	nebo
její	její	k3xOp3gFnSc1	její
změna	změna	k1gFnSc1	změna
při	při	k7c6	při
přidávání	přidávání	k1gNnSc6	přidávání
dalších	další	k2eAgInPc2d1	další
klíčů	klíč	k1gInPc2	klíč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trie	Trie	k1gFnSc1	Trie
může	moct	k5eAaImIp3nS	moct
poskytnout	poskytnout	k5eAaPmF	poskytnout
abecední	abecední	k2eAgNnSc1d1	abecední
řazení	řazení	k1gNnSc1	řazení
záznamů	záznam	k1gInPc2	záznam
podle	podle	k7c2	podle
klíče	klíč	k1gInSc2	klíč
<g/>
.	.	kIx.	.
<g/>
Má	mít	k5eAaImIp3nS	mít
také	také	k9	také
ale	ale	k9	ale
nevýhody	nevýhoda	k1gFnPc1	nevýhoda
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Trie	Trie	k1gFnSc1	Trie
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
pomalejší	pomalý	k2eAgFnSc1d2	pomalejší
než	než	k8xS	než
hašovací	hašovací	k2eAgFnSc1d1	hašovací
tabulka	tabulka	k1gFnSc1	tabulka
při	při	k7c6	při
vyhledávání	vyhledávání	k1gNnSc6	vyhledávání
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
přistupováno	přistupován	k2eAgNnSc1d1	přistupováno
na	na	k7c6	na
pevném	pevný	k2eAgInSc6d1	pevný
disku	disk	k1gInSc6	disk
nebo	nebo	k8xC	nebo
podobných	podobný	k2eAgNnPc6d1	podobné
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
doba	doba	k1gFnSc1	doba
náhodného	náhodný	k2eAgInSc2d1	náhodný
přístupu	přístup	k1gInSc2	přístup
velká	velký	k2eAgNnPc1d1	velké
vůči	vůči	k7c3	vůči
hlavní	hlavní	k2eAgFnSc3d1	hlavní
paměti	paměť	k1gFnSc3	paměť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trie	Trie	k1gFnSc1	Trie
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
méně	málo	k6eAd2	málo
paměťově	paměťově	k6eAd1	paměťově
efektivní	efektivní	k2eAgFnSc1d1	efektivní
než	než	k8xS	než
hašovací	hašovací	k2eAgFnSc1d1	hašovací
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reprezentace	reprezentace	k1gFnSc2	reprezentace
slovníku	slovník	k1gInSc2	slovník
===	===	k?	===
</s>
</p>
<p>
<s>
Častou	častý	k2eAgFnSc7d1	častá
aplikací	aplikace	k1gFnSc7	aplikace
trie	trie	k6eAd1	trie
je	být	k5eAaImIp3nS	být
uložení	uložení	k1gNnSc4	uložení
slovníku	slovník	k1gInSc2	slovník
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
mobilních	mobilní	k2eAgInPc6d1	mobilní
telefonech	telefon	k1gInPc6	telefon
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
využívá	využívat	k5eAaPmIp3nS	využívat
schopnosti	schopnost	k1gFnPc4	schopnost
trie	trie	k6eAd1	trie
rychle	rychle	k6eAd1	rychle
vyhledávat	vyhledávat	k5eAaImF	vyhledávat
<g/>
,	,	kIx,	,
vkládat	vkládat	k5eAaImF	vkládat
a	a	k8xC	a
odstraňovat	odstraňovat	k5eAaImF	odstraňovat
záznamy	záznam	k1gInPc4	záznam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trie	Trie	k6eAd1	Trie
je	být	k5eAaImIp3nS	být
také	také	k9	také
vhodné	vhodný	k2eAgNnSc1d1	vhodné
pro	pro	k7c4	pro
implementaci	implementace	k1gFnSc4	implementace
algoritmů	algoritmus	k1gInPc2	algoritmus
přibližného	přibližný	k2eAgNnSc2d1	přibližné
matchování	matchování	k1gNnSc2	matchování
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
ty	ten	k3xDgFnPc4	ten
používané	používaný	k2eAgFnPc4d1	používaná
v	v	k7c6	v
programech	program	k1gInPc6	program
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
pravopisu	pravopis	k1gInSc2	pravopis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Algoritmy	algoritmus	k1gInPc1	algoritmus
===	===	k?	===
</s>
</p>
<p>
<s>
Následující	následující	k2eAgInSc1d1	následující
pseudokód	pseudokód	k1gInSc1	pseudokód
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
obecný	obecný	k2eAgInSc1d1	obecný
algoritmus	algoritmus	k1gInSc1	algoritmus
zjišťující	zjišťující	k2eAgInSc1d1	zjišťující
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
je	být	k5eAaImIp3nS	být
daný	daný	k2eAgInSc1d1	daný
řetězec	řetězec	k1gInSc1	řetězec
v	v	k7c6	v
trie	trie	k1gFnSc6	trie
<g/>
.	.	kIx.	.
</s>
<s>
Pole	pola	k1gFnSc3	pola
naslednici	naslednice	k1gFnSc3	naslednice
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
následníky	následník	k1gMnPc4	následník
uzlu	uzel	k1gInSc2	uzel
<g/>
,	,	kIx,	,
koncový	koncový	k2eAgInSc4d1	koncový
uzel	uzel	k1gInSc4	uzel
je	být	k5eAaImIp3nS	být
ten	ten	k3xDgInSc1	ten
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
platné	platný	k2eAgNnSc4d1	platné
slovo	slovo	k1gNnSc4	slovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
function	function	k1gInSc1	function
jeVTrie	jeVTrie	k1gFnSc2	jeVTrie
<g/>
(	(	kIx(	(
<g/>
uzel	uzel	k1gInSc1	uzel
<g/>
,	,	kIx,	,
klic	klic	k1gFnSc1	klic
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
if	if	k?	if
(	(	kIx(	(
<g/>
klic	klic	k6eAd1	klic
je	on	k3xPp3gFnPc4	on
prazdny	prazdna	k1gFnPc4	prazdna
retezec	retezec	k1gInSc1	retezec
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
#	#	kIx~	#
výchozí	výchozí	k2eAgInSc1d1	výchozí
případ	případ	k1gInSc1	případ
</s>
</p>
<p>
<s>
return	return	k1gInSc1	return
je	být	k5eAaImIp3nS	být
uzel	uzel	k1gInSc4	uzel
koncový	koncový	k2eAgInSc4d1	koncový
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
else	elsat	k5eAaPmIp3nS	elsat
<g/>
:	:	kIx,	:
#	#	kIx~	#
<g/>
rekurzivní	rekurzivní	k2eAgInSc1d1	rekurzivní
případ	případ	k1gInSc1	případ
</s>
</p>
<p>
<s>
c	c	k0	c
=	=	kIx~	=
prvni	prvnit	k5eAaImRp2nS	prvnit
znak	znak	k1gInSc4	znak
v	v	k7c6	v
klici	klice	k1gFnSc6	klice
#	#	kIx~	#
<g/>
funguje	fungovat	k5eAaImIp3nS	fungovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
klíč	klíč	k1gInSc1	klíč
není	být	k5eNaImIp3nS	být
prázdný	prázdný	k2eAgInSc1d1	prázdný
</s>
</p>
<p>
<s>
sufix	sufix	k1gInSc1	sufix
=	=	kIx~	=
klic	klic	k1gInSc1	klic
minus	minus	k6eAd1	minus
prvni	prvň	k1gMnSc3	prvň
znak	znak	k1gInSc4	znak
</s>
</p>
<p>
<s>
naslednik	naslednik	k1gInSc4	naslednik
=	=	kIx~	=
uzel	uzel	k1gInSc4	uzel
<g/>
.	.	kIx.	.
<g/>
naslednici	naslednice	k1gFnSc4	naslednice
<g/>
[	[	kIx(	[
<g/>
c	c	k0	c
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
if	if	k?	if
(	(	kIx(	(
<g/>
naslednik	naslednik	k1gMnSc1	naslednik
je	být	k5eAaImIp3nS	být
prazdny	prazdna	k1gFnPc4	prazdna
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
#	#	kIx~	#
<g/>
nelze	lze	k6eNd1	lze
rekurze	rekurze	k1gFnSc1	rekurze
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
klíč	klíč	k1gInSc1	klíč
je	být	k5eAaImIp3nS	být
neprázdný	prázdný	k2eNgInSc1d1	neprázdný
</s>
</p>
<p>
<s>
return	return	k1gInSc1	return
false	false	k1gFnSc2	false
</s>
</p>
<p>
<s>
else	elsat	k5eAaPmIp3nS	elsat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
jeVTrie	jeVTrie	k1gFnSc1	jeVTrie
<g/>
(	(	kIx(	(
<g/>
naslednik	naslednik	k1gInSc1	naslednik
<g/>
,	,	kIx,	,
sufix	sufix	k1gInSc1	sufix
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Řazení	řazení	k1gNnSc2	řazení
===	===	k?	===
</s>
</p>
<p>
<s>
Lexikografické	lexikografický	k2eAgNnSc1d1	lexikografické
řazení	řazení	k1gNnSc1	řazení
skupiny	skupina	k1gFnSc2	skupina
prvků	prvek	k1gInPc2	prvek
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
provedeno	provést	k5eAaPmNgNnS	provést
jednoduchým	jednoduchý	k2eAgInSc7d1	jednoduchý
algoritmem	algoritmus	k1gInSc7	algoritmus
založeným	založený	k2eAgInSc7d1	založený
na	na	k7c4	na
trie	tri	k1gInPc4	tri
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Vlož	vložit	k5eAaPmRp2nS	vložit
všechny	všechen	k3xTgInPc1	všechen
klíče	klíč	k1gInPc1	klíč
do	do	k7c2	do
trie	tri	k1gFnSc2	tri
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vrať	vrátit	k5eAaPmRp2nS	vrátit
všechny	všechen	k3xTgInPc4	všechen
klíče	klíč	k1gInPc4	klíč
v	v	k7c4	v
trie	trie	k1gFnSc4	trie
průchodem	průchod	k1gInSc7	průchod
preorder	preordero	k1gNnPc2	preordero
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
způsobí	způsobit	k5eAaPmIp3nS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
výstup	výstup	k1gInSc1	výstup
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
lexikograficky	lexikograficky	k6eAd1	lexikograficky
vzestupném	vzestupný	k2eAgNnSc6d1	vzestupné
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
postorder	postorder	k1gInSc1	postorder
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgNnSc3	jenž
bude	být	k5eAaImBp3nS	být
výstup	výstup	k1gInSc4	výstup
v	v	k7c6	v
sestupném	sestupný	k2eAgNnSc6d1	sestupné
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
Průchod	průchod	k1gInSc1	průchod
preorder	preordra	k1gFnPc2	preordra
a	a	k8xC	a
postorder	postordra	k1gFnPc2	postordra
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc4	dva
druhy	druh	k1gInPc4	druh
průchodu	průchod	k1gInSc2	průchod
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
.	.	kIx.	.
<g/>
Tento	tento	k3xDgInSc1	tento
algoritmus	algoritmus	k1gInSc1	algoritmus
je	být	k5eAaImIp3nS	být
formou	forma	k1gFnSc7	forma
radix	radix	k1gInSc1	radix
sortu	sorta	k1gFnSc4	sorta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trie	Trie	k1gFnSc1	Trie
tvoří	tvořit	k5eAaImIp3nS	tvořit
základní	základní	k2eAgFnSc4d1	základní
datovou	datový	k2eAgFnSc4d1	datová
strukturu	struktura	k1gFnSc4	struktura
burstsortu	burstsort	k1gInSc2	burstsort
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
nejrychlejšího	rychlý	k2eAgInSc2d3	nejrychlejší
známého	známý	k2eAgInSc2d1	známý
algoritmu	algoritmus	k1gInSc2	algoritmus
pro	pro	k7c4	pro
řazení	řazení	k1gNnSc4	řazení
řetězců	řetězec	k1gInPc2	řetězec
založeném	založený	k2eAgInSc6d1	založený
na	na	k7c6	na
využití	využití	k1gNnSc4	využití
cache	cach	k1gFnSc2	cach
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Paralelní	paralelní	k2eAgInSc1d1	paralelní
algoritmus	algoritmus	k1gInSc1	algoritmus
pro	pro	k7c4	pro
řazení	řazení	k1gNnPc4	řazení
N	N	kA	N
klíčů	klíč	k1gInPc2	klíč
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
trie	trie	k1gFnSc6	trie
je	být	k5eAaImIp3nS	být
O	o	k7c6	o
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
N	N	kA	N
procesorů	procesor	k1gInPc2	procesor
a	a	k8xC	a
klíče	klíč	k1gInPc1	klíč
mají	mít	k5eAaImIp3nP	mít
konstantní	konstantní	k2eAgNnSc4d1	konstantní
horní	horní	k2eAgNnSc4d1	horní
omezení	omezení	k1gNnSc4	omezení
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
klíče	klíč	k1gInPc1	klíč
mohou	moct	k5eAaImIp3nP	moct
kolidovat	kolidovat	k5eAaImF	kolidovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
společné	společný	k2eAgInPc4d1	společný
prefixy	prefix	k1gInPc4	prefix
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
identické	identický	k2eAgFnPc1d1	identická
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
snižuje	snižovat	k5eAaImIp3nS	snižovat
výhodu	výhoda	k1gFnSc4	výhoda
rychlosti	rychlost	k1gFnSc2	rychlost
zpracování	zpracování	k1gNnSc2	zpracování
více	hodně	k6eAd2	hodně
paralelně	paralelně	k6eAd1	paralelně
pracujícími	pracující	k2eAgInPc7d1	pracující
procesory	procesor	k1gInPc7	procesor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fulltextové	fulltextový	k2eAgNnSc4d1	Fulltextové
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
===	===	k?	===
</s>
</p>
<p>
<s>
K	k	k7c3	k
indexování	indexování	k1gNnSc3	indexování
všech	všecek	k3xTgInPc2	všecek
sufixů	sufix	k1gInPc2	sufix
v	v	k7c6	v
textu	text	k1gInSc6	text
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
rychlého	rychlý	k2eAgNnSc2d1	rychlé
fulltexového	fulltexový	k2eAgNnSc2d1	fulltexový
vyhledávání	vyhledávání	k1gNnSc2	vyhledávání
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použit	použít	k5eAaPmNgInS	použít
speciální	speciální	k2eAgInSc1d1	speciální
druh	druh	k1gInSc1	druh
trie	trie	k1gInSc1	trie
<g/>
,	,	kIx,	,
sufixový	sufixový	k2eAgInSc1d1	sufixový
strom	strom	k1gInSc1	strom
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
využitím	využití	k1gNnSc7	využití
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
hledání	hledání	k1gNnSc4	hledání
nejdelšího	dlouhý	k2eAgInSc2d3	nejdelší
společného	společný	k2eAgInSc2d1	společný
prefixu	prefix	k1gInSc2	prefix
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
R.	R.	kA	R.
de	de	k?	de
la	la	k1gNnSc1	la
Briandais	Briandais	k1gFnSc1	Briandais
<g/>
:	:	kIx,	:
File	File	k1gFnSc1	File
Searching	Searching	k1gInSc1	Searching
Using	Using	k1gMnSc1	Using
Variable	Variable	k1gMnSc1	Variable
Length	Length	k1gMnSc1	Length
Keys	Keys	k1gInSc4	Keys
<g/>
.	.	kIx.	.
</s>
<s>
Proceedings	Proceedings	k1gInSc1	Proceedings
of	of	k?	of
the	the	k?	the
Western	Western	kA	Western
Joint	Joint	k1gInSc1	Joint
Computer	computer	k1gInSc1	computer
Conference	Conference	k1gFnSc1	Conference
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
295	[number]	k4	295
<g/>
–	–	k?	–
<g/>
298	[number]	k4	298
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
E.	E.	kA	E.
Fredkin	Fredkin	k1gMnSc1	Fredkin
<g/>
:	:	kIx,	:
Trie	Trie	k1gFnSc1	Trie
Memory	Memora	k1gFnSc2	Memora
<g/>
.	.	kIx.	.
</s>
<s>
Communications	Communications	k1gInSc1	Communications
of	of	k?	of
the	the	k?	the
ACM	ACM	kA	ACM
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
<g/>
490	[number]	k4	490
<g/>
-	-	kIx~	-
<g/>
499	[number]	k4	499
<g/>
,	,	kIx,	,
září	zářit	k5eAaImIp3nS	zářit
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Donald	Donald	k1gMnSc1	Donald
Knuth	Knuth	k1gMnSc1	Knuth
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Art	Art	k1gMnSc1	Art
of	of	k?	of
Computer	computer	k1gInSc1	computer
Programming	Programming	k1gInSc1	Programming
<g/>
,	,	kIx,	,
Volume	volum	k1gInSc5	volum
3	[number]	k4	3
<g/>
:	:	kIx,	:
Sorting	Sorting	k1gInSc1	Sorting
and	and	k?	and
Searching	Searching	k1gInSc1	Searching
<g/>
,	,	kIx,	,
Third	Third	k1gInSc1	Third
Edition	Edition	k1gInSc1	Edition
<g/>
.	.	kIx.	.
</s>
<s>
Addison-Wesley	Addison-Wesley	k1gInPc1	Addison-Wesley
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
201	[number]	k4	201
<g/>
-	-	kIx~	-
<g/>
89685	[number]	k4	89685
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Section	Section	k1gInSc1	Section
6.3	[number]	k4	6.3
<g/>
:	:	kIx,	:
Digital	Digital	kA	Digital
Searching	Searching	k1gInSc1	Searching
<g/>
,	,	kIx,	,
pp	pp	k?	pp
<g/>
.492	.492	k4	.492
<g/>
–	–	k?	–
<g/>
512	[number]	k4	512
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
trie	tri	k1gFnSc2	tri
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Animace	animace	k1gFnSc1	animace
trie	triat	k5eAaPmIp3nS	triat
ve	v	k7c6	v
flashi	flash	k1gFnSc6	flash
+	+	kIx~	+
implementace	implementace	k1gFnSc2	implementace
</s>
</p>
<p>
<s>
Cache-Efficient	Cache-Efficient	k1gInSc1	Cache-Efficient
String	String	k1gInSc1	String
Sorting	Sorting	k1gInSc1	Sorting
Using	Using	k1gInSc1	Using
Copying	Copying	k1gInSc4	Copying
</s>
</p>
