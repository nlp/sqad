<p>
<s>
Black	Black	k6eAd1	Black
metal	metal	k1gInSc1	metal
je	být	k5eAaImIp3nS	být
hudební	hudební	k2eAgInSc1d1	hudební
styl	styl	k1gInSc1	styl
<g/>
,	,	kIx,	,
subkultura	subkultura	k1gFnSc1	subkultura
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
80.	[number]	k4	80.
let	léto	k1gNnPc2	léto
20.	[number]	k4	20.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Námětem	námět	k1gInSc7	námět
black	black	k1gInSc4	black
metalu	metal	k1gInSc2	metal
je	být	k5eAaImIp3nS	být
hlavně	hlavně	k9	hlavně
temnota	temnota	k1gFnSc1	temnota
<g/>
,	,	kIx,	,
mizantropie	mizantropie	k1gFnSc1	mizantropie
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
rozebírá	rozebírat	k5eAaImIp3nS	rozebírat
stinné	stinný	k2eAgFnPc4d1	stinná
stránky	stránka	k1gFnPc4	stránka
lidské	lidský	k2eAgFnSc2d1	lidská
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k8xC	i
když	když	k8xS	když
mnoho	mnoho	k4c1	mnoho
kapel	kapela	k1gFnPc2	kapela
používá	používat	k5eAaImIp3nS	používat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
textech	text	k1gInPc6	text
satanismus	satanismus	k1gInSc4	satanismus
jako	jako	k8xC	jako
téma	téma	k1gNnSc4	téma
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
spíše	spíše	k9	spíše
o	o	k7c4	o
metaforické	metaforický	k2eAgNnSc4d1	metaforické
vyjádření	vyjádření	k1gNnSc4	vyjádření
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
pramení	pramenit	k5eAaImIp3nS	pramenit
z	z	k7c2	z
nenávisti	nenávist	k1gFnSc2	nenávist
vůči	vůči	k7c3	vůči
náboženstvím	náboženství	k1gNnPc3	náboženství
<g/>
,	,	kIx,	,
církvím	církev	k1gFnPc3	církev
a	a	k8xC	a
jiným	jiný	k1gMnPc3	jiný
<g/>
,	,	kIx,	,
z	z	k7c2	z
jejich	jejich	k3xOp3gInSc2	jejich
pohledu	pohled	k1gInSc2	pohled
<g/>
,	,	kIx,	,
pochybným	pochybný	k2eAgFnPc3d1	pochybná
hodnotám	hodnota	k1gFnPc3	hodnota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Hudební	hudební	k2eAgInPc4d1	hudební
rysy	rys	k1gInPc4	rys
==	==	k?	==
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
death	deatha	k1gFnPc2	deatha
metalu	metal	k1gInSc2	metal
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
podladění	podladění	k1gNnSc4	podladění
kytary	kytara	k1gFnSc2	kytara
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
až	až	k8xS	až
dva	dva	k4xCgInPc4	dva
tóny	tón	k1gInPc4	tón
(	(	kIx(	(
<g/>
dominuje	dominovat	k5eAaImIp3nS	dominovat
technika	technika	k1gFnSc1	technika
hry	hra	k1gFnSc2	hra
před	před	k7c7	před
melodikou	melodika	k1gFnSc7	melodika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
black	blacka	k1gFnPc2	blacka
metalu	metal	k1gInSc2	metal
je	být	k5eAaImIp3nS	být
častější	častý	k2eAgNnSc4d2	častější
klasické	klasický	k2eAgNnSc4d1	klasické
ladění	ladění	k1gNnSc4	ladění
(	(	kIx(	(
<g/>
dominuje	dominovat	k5eAaImIp3nS	dominovat
melodika	melodika	k1gFnSc1	melodika
<g/>
,	,	kIx,	,
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
minimalistické	minimalistický	k2eAgNnSc4d1	minimalistické
podání	podání	k1gNnSc4	podání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
rysem	rys	k1gInSc7	rys
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
hudební	hudební	k2eAgInSc4d1	hudební
žánr	žánr	k1gInSc4	žánr
je	být	k5eAaImIp3nS	být
zkreslený	zkreslený	k2eAgMnSc1d1	zkreslený
<g/>
,	,	kIx,	,
chraplavý	chraplavý	k2eAgInSc1d1	chraplavý
vokál	vokál	k1gInSc1	vokál
-	-	kIx~	-
False	False	k1gFnSc1	False
Chord	chorda	k1gFnPc2	chorda
Scream	Scream	k1gInSc4	Scream
<g/>
,	,	kIx,	,
screaming	screaming	k1gInSc4	screaming
a	a	k8xC	a
taky	taky	k9	taky
growling	growling	k1gInSc1	growling
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
docílení	docílení	k1gNnSc1	docílení
děsivé	děsivý	k2eAgFnSc2d1	děsivá
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
evokující	evokující	k2eAgInPc1d1	evokující
válečné	válečný	k2eAgInPc1d1	válečný
pohanské	pohanský	k2eAgInPc1d1	pohanský
zpěvy	zpěv	k1gInPc1	zpěv
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
měly	mít	k5eAaImAgInP	mít
původně	původně	k6eAd1	původně
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
zastrašení	zastrašení	k1gNnPc2	zastrašení
nepřítele	nepřítel	k1gMnSc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
se	s	k7c7	s
složením	složení	k1gNnSc7	složení
kapely	kapela	k1gFnSc2	kapela
bez	bez	k7c2	bez
baskytary	baskytara	k1gFnSc2	baskytara
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
nebo	nebo	k8xC	nebo
dvěma	dva	k4xCgFnPc7	dva
elektrickými	elektrický	k2eAgFnPc7d1	elektrická
kytarami	kytara	k1gFnPc7	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
klasického	klasický	k2eAgNnSc2d1	klasické
nástrojového	nástrojový	k2eAgNnSc2d1	nástrojové
obsazení	obsazení	k1gNnSc2	obsazení
(	(	kIx(	(
<g/>
old	old	k?	old
school	school	k1gInSc1	school
black	black	k1gMnSc1	black
metal	metal	k1gInSc1	metal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
často	často	k6eAd1	často
používaným	používaný	k2eAgInSc7d1	používaný
nástrojem	nástroj	k1gInSc7	nástroj
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
zvukové	zvukový	k2eAgInPc1d1	zvukový
vzorky	vzorek	k1gInPc1	vzorek
(	(	kIx(	(
<g/>
samply	sampnout	k5eAaPmAgFnP	sampnout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dotvářející	dotvářející	k2eAgFnSc4d1	dotvářející
temnou	temný	k2eAgFnSc4d1	temná
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Kytary	kytara	k1gFnSc2	kytara
===	===	k?	===
</s>
</p>
<p>
<s>
Rychlé	Rychlé	k2eAgFnPc1d1	Rychlé
<g/>
,	,	kIx,	,
velice	velice	k6eAd1	velice
zkreslené	zkreslený	k2eAgFnPc1d1	zkreslená
kytary	kytara	k1gFnPc1	kytara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Převažuje	převažovat	k5eAaImIp3nS	převažovat
použití	použití	k1gNnSc4	použití
tzv.	tzv.	kA	tzv.
silových	silový	k2eAgInPc2d1	silový
akordů	akord	k1gInPc2	akord
powerchords	powerchords	k6eAd1	powerchords
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
kytarista	kytarista	k1gMnSc1	kytarista
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
použil	použít	k5eAaPmAgMnS	použít
byl	být	k5eAaImAgInS	být
Tony	Tony	k1gFnSc1	Tony
Iommi	Iom	k1gFnPc7	Iom
(	(	kIx(	(
<g/>
Black	Black	k1gInSc1	Black
Sabbath	Sabbath	k1gInSc1	Sabbath
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Časté	častý	k2eAgNnSc1d1	časté
je	být	k5eAaImIp3nS	být
použití	použití	k1gNnSc1	použití
chromatických	chromatický	k2eAgInPc2d1	chromatický
tónů	tón	k1gInPc2	tón
posunutých	posunutý	k2eAgInPc2d1	posunutý
dolů	dol	k1gInPc2	dol
nebo	nebo	k8xC	nebo
nahoru	nahoru	k6eAd1	nahoru
o	o	k7c4	o
půltóny	půltón	k1gInPc4	půltón
<g/>
,	,	kIx,	,
vytvářející	vytvářející	k2eAgFnSc4d1	vytvářející
tónicky	tónicky	k6eAd1	tónicky
neklidnou	klidný	k2eNgFnSc4d1	neklidná
atmosféru	atmosféra	k1gFnSc4	atmosféra
(	(	kIx(	(
<g/>
běžné	běžný	k2eAgInPc1d1	běžný
rysy	rys	k1gInPc1	rys
třítónových	třítónový	k2eAgInPc2d1	třítónový
intervalů	interval	k1gInPc2	interval
<g/>
,	,	kIx,	,
také	také	k6eAd1	také
zpopularizované	zpopularizovaný	k2eAgFnPc1d1	zpopularizovaná
Black	Black	k1gInSc4	Black
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
také	také	k9	také
odklon	odklon	k1gInSc4	odklon
od	od	k7c2	od
již	již	k6eAd1	již
zavedené	zavedený	k2eAgFnSc2d1	zavedená
stupnice	stupnice	k1gFnSc2	stupnice
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
větších	veliký	k2eAgFnPc6d2	veliký
disonancích	disonance	k1gFnPc6	disonance
"	"	kIx"	"
<g/>
odpor	odpor	k1gInSc4	odpor
<g/>
"	"	kIx"	"
zvuku	zvuk	k1gInSc2	zvuk
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jako	jako	k9	jako
u	u	k7c2	u
harmonických	harmonický	k2eAgInPc2d1	harmonický
tónů	tón	k1gInPc2	tón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzácné	vzácný	k2eAgNnSc1d1	vzácné
je	být	k5eAaImIp3nS	být
využití	využití	k1gNnSc1	využití
kytarových	kytarový	k2eAgNnPc2d1	kytarové
sól	sólo	k1gNnPc2	sólo
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
heavy	heav	k1gInPc7	heav
a	a	k8xC	a
power	power	k1gInSc1	power
metalem	metal	k1gInSc7	metal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Bicí	bicí	k2eAgMnPc4d1	bicí
===	===	k?	===
</s>
</p>
<p>
<s>
Double	double	k2eAgInSc1d1	double
bass	bass	k1gInSc1	bass
<g/>
,	,	kIx,	,
blast	blast	k1gInSc1	blast
beat	beat	k1gInSc1	beat
<g/>
,	,	kIx,	,
a	a	k8xC	a
D-beat	Deat	k1gInSc1	D-beat
bubnování	bubnování	k1gNnSc2	bubnování
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
velmi	velmi	k6eAd1	velmi
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
a	a	k8xC	a
působivá	působivý	k2eAgFnSc1d1	působivá
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
bicí	bicí	k2eAgMnPc1d1	bicí
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
menší	malý	k2eAgFnSc4d2	menší
úlohu	úloha	k1gFnSc4	úloha
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
jen	jen	k9	jen
doprovázejí	doprovázet	k5eAaImIp3nP	doprovázet
velmi	velmi	k6eAd1	velmi
holými	holý	k2eAgInPc7d1	holý
a	a	k8xC	a
prázdnými	prázdný	k2eAgInPc7d1	prázdný
tóny	tón	k1gInPc7	tón
-	-	kIx~	-
využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
zvláště	zvláště	k6eAd1	zvláště
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
atmosféry	atmosféra	k1gFnSc2	atmosféra
black	black	k1gInSc4	black
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
jistě	jistě	k9	jistě
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
mnohé	mnohý	k2eAgFnPc1d1	mnohá
skupiny	skupina	k1gFnPc1	skupina
výhrady	výhrada	k1gFnSc2	výhrada
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
pasážích	pasáž	k1gFnPc6	pasáž
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
sóloví	sólový	k2eAgMnPc1d1	sólový
umělci	umělec	k1gMnPc1	umělec
<g/>
,	,	kIx,	,
využívají	využívat	k5eAaPmIp3nP	využívat
umělé	umělý	k2eAgNnSc4d1	umělé
bubenické	bubenický	k2eAgNnSc4d1	bubenické
zařízení	zařízení	k1gNnSc4	zařízení
namísto	namísto	k7c2	namísto
lidského	lidský	k2eAgMnSc2d1	lidský
bubeníka	bubeník	k1gMnSc2	bubeník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Texty	text	k1gInPc1	text
a	a	k8xC	a
vokály	vokál	k1gInPc1	vokál
===	===	k?	===
</s>
</p>
<p>
<s>
Zřetelný	zřetelný	k2eAgMnSc1d1	zřetelný
je	být	k5eAaImIp3nS	být
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
(	(	kIx(	(
<g/>
harsh	harsh	k1gInSc1	harsh
<g/>
)	)	kIx)	)
styl	styl	k1gInSc1	styl
vokálů	vokál	k1gInPc2	vokál
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
hrdelní	hrdelní	k2eAgInSc1d1	hrdelní
skřehot	skřehot	k1gInSc1	skřehot
(	(	kIx(	(
<g/>
False	False	k1gFnSc1	False
Chord	chorda	k1gFnPc2	chorda
Scream	Scream	k1gInSc4	Scream
<g/>
,	,	kIx,	,
growling	growling	k1gInSc4	growling
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
vysoko-intenzivní	vysokontenzivní	k2eAgInSc1d1	vysoko-intenzivní
vřískot	vřískot	k1gInSc1	vřískot
(	(	kIx(	(
<g/>
screaming	screaming	k1gInSc1	screaming
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vokál	vokál	k1gInSc1	vokál
současně	současně	k6eAd1	současně
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
hudbu	hudba	k1gFnSc4	hudba
svou	svůj	k3xOyFgFnSc7	svůj
dramatickou	dramatický	k2eAgFnSc7d1	dramatická
podstatou	podstata	k1gFnSc7	podstata
zkleslení	zkleslení	k1gNnSc2	zkleslení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
symfonickém	symfonický	k2eAgMnSc6d1	symfonický
black	black	k6eAd1	black
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
vkládají	vkládat	k5eAaImIp3nP	vkládat
do	do	k7c2	do
písní	píseň	k1gFnPc2	píseň
tradiční	tradiční	k2eAgInPc1d1	tradiční
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
uváděné	uváděný	k2eAgFnPc4d1	uváděná
jako	jako	k8xC	jako
'	'	kIx"	'
<g/>
čisté	čistý	k2eAgInPc1d1	čistý
<g/>
'	'	kIx"	'
vokály	vokál	k1gInPc1	vokál
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
některé	některý	k3yIgNnSc4	některý
blackmetalové	blackmetal	k1gMnPc1	blackmetal
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Arcturus	Arcturus	k1gInSc4	Arcturus
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
'	'	kIx"	'
<g/>
čisté	čistý	k2eAgInPc1d1	čistý
<g/>
'	'	kIx"	'
vokály	vokál	k1gInPc1	vokál
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
písně	píseň	k1gFnPc4	píseň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
efektu	efekt	k1gInSc3	efekt
ozvěny	ozvěna	k1gFnSc2	ozvěna
hlasu	hlas	k1gInSc2	hlas
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
hudby	hudba	k1gFnSc2	hudba
více	hodně	k6eAd2	hodně
plné	plný	k2eAgFnPc1d1	plná
a	a	k8xC	a
atmosférické	atmosférický	k2eAgFnPc1d1	atmosférická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
gregoriánský	gregoriánský	k2eAgInSc4d1	gregoriánský
chorál	chorál	k1gInSc4	chorál
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgFnPc4	některý
blackmetalové	blackmetal	k1gMnPc1	blackmetal
písně	píseň	k1gFnSc2	píseň
doplněny	doplněn	k2eAgFnPc1d1	doplněna
o	o	k7c4	o
ženský	ženský	k2eAgInSc4d1	ženský
nebo	nebo	k8xC	nebo
mužský	mužský	k2eAgInSc4d1	mužský
pěvecký	pěvecký	k2eAgInSc4d1	pěvecký
sbor	sbor	k1gInSc4	sbor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
textech	text	k1gInPc6	text
klade	klást	k5eAaImIp3nS	klást
black	black	k6eAd1	black
metal	metat	k5eAaImAgMnS	metat
velký	velký	k2eAgInSc4d1	velký
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
proti	proti	k7c3	proti
křesťanství	křesťanství	k1gNnSc3	křesťanství
(	(	kIx(	(
<g/>
satanismus	satanismus	k1gInSc4	satanismus
<g/>
,	,	kIx,	,
pohanství	pohanství	k1gNnSc1	pohanství
<g/>
,	,	kIx,	,
okultismus	okultismus	k1gInSc1	okultismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Texty	text	k1gInPc1	text
často	často	k6eAd1	často
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
zimu	zima	k1gFnSc4	zima
<g/>
,	,	kIx,	,
temnotu	temnota	k1gFnSc4	temnota
<g/>
,	,	kIx,	,
lesy	les	k1gInPc4	les
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
přírodní	přírodní	k2eAgInPc4d1	přírodní
elementy	element	k1gInPc4	element
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
mohou	moct	k5eAaImIp3nP	moct
opěvovat	opěvovat	k5eAaImF	opěvovat
folklór	folklór	k1gInSc4	folklór
a	a	k8xC	a
historii	historie	k1gFnSc4	historie
těchto	tento	k3xDgFnPc2	tento
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Texty	text	k1gInPc1	text
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
spíše	spíše	k9	spíše
temnými	temný	k2eAgFnPc7d1	temná
stránkami	stránka	k1gFnPc7	stránka
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
misantropie	misantropie	k1gFnSc2	misantropie
<g/>
,	,	kIx,	,
bídy	bída	k1gFnSc2	bída
a	a	k8xC	a
násilí	násilí	k1gNnSc2	násilí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
také	také	k6eAd1	také
inspirovány	inspirovat	k5eAaBmNgFnP	inspirovat
světem	svět	k1gInSc7	svět
fantasy	fantas	k1gInPc5	fantas
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k6eAd1	zejména
se	se	k3xPyFc4	se
mnoho	mnoho	k4c1	mnoho
skupin	skupina	k1gFnPc2	skupina
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
fiktivní	fiktivní	k2eAgInSc4d1	fiktivní
kontinent	kontinent	k1gInSc4	kontinent
Středozemě	Středozem	k1gFnSc2	Středozem
od	od	k7c2	od
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkiena	Tolkien	k1gMnSc2	Tolkien
<g/>
,	,	kIx,	,
např.	např.	kA	např.
rakouská	rakouský	k2eAgFnSc1d1	rakouská
skupina	skupina	k1gFnSc1	skupina
Summoning	Summoning	k1gInSc1	Summoning
<g/>
,	,	kIx,	,
či	či	k8xC	či
norský	norský	k2eAgInSc1d1	norský
Burzum	Burzum	k1gInSc1	Burzum
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
skupiny	skupina	k1gFnPc1	skupina
si	se	k3xPyFc3	se
mnohdy	mnohdy	k6eAd1	mnohdy
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
své	svůj	k3xOyFgFnSc2	svůj
vlastní	vlastní	k2eAgFnSc2d1	vlastní
smyšlené	smyšlený	k2eAgFnSc2d1	smyšlená
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
určité	určitý	k2eAgFnPc4d1	určitá
písně	píseň	k1gFnPc4	píseň
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Immortal	Immortal	k1gMnPc2	Immortal
vyobrazují	vyobrazovat	k5eAaImIp3nP	vyobrazovat
imaginární	imaginární	k2eAgNnSc4d1	imaginární
království	království	k1gNnSc4	království
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Blashyrkh	Blashyrkh	k1gInSc4	Blashyrkh
<g/>
.	.	kIx.	.
</s>
<s>
Bal-Sagoth	Bal-Sagoth	k1gMnSc1	Bal-Sagoth
si	se	k3xPyFc3	se
také	také	k9	také
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
fantasy	fantas	k1gInPc4	fantas
příběhy	příběh	k1gInPc4	příběh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
inspirovány	inspirován	k2eAgInPc1d1	inspirován
spisovateli	spisovatel	k1gMnSc6	spisovatel
Robert	Roberta	k1gFnPc2	Roberta
E.	E.	kA	E.
Howardem	Howard	k1gMnSc7	Howard
a	a	k8xC	a
H.	H.	kA	H.
P.	P.	kA	P.
Lovecraftem	Lovecraft	k1gInSc7	Lovecraft
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Atmosféra	atmosféra	k1gFnSc1	atmosféra
a	a	k8xC	a
stavební	stavební	k2eAgInPc1d1	stavební
prvky	prvek	k1gInPc1	prvek
hudby	hudba	k1gFnSc2	hudba
===	===	k?	===
</s>
</p>
<p>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
struktura	struktura	k1gFnSc1	struktura
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
často	často	k6eAd1	často
postrádá	postrádat	k5eAaImIp3nS	postrádat
veršování	veršování	k1gNnSc4	veršování
a	a	k8xC	a
refrén	refrén	k1gInSc4	refrén
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
a	a	k8xC	a
opakující	opakující	k2eAgNnSc1d1	opakující
se	se	k3xPyFc4	se
instrumentální	instrumentální	k2eAgFnPc1d1	instrumentální
pasáže	pasáž	k1gFnPc1	pasáž
s	s	k7c7	s
menším	malý	k2eAgNnSc7d2	menší
využitím	využití	k1gNnSc7	využití
vokálů	vokál	k1gInPc2	vokál
než	než	k8xS	než
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
stylech	styl	k1gInPc6	styl
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rychlý	rychlý	k2eAgInSc1d1	rychlý
rytmus	rytmus	k1gInSc1	rytmus
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
taktu	takt	k1gInSc6	takt
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
tříčtvrťovém	tříčtvrťový	k2eAgMnSc6d1	tříčtvrťový
se	se	k3xPyFc4	se
střídá	střídat	k5eAaImIp3nS	střídat
s	s	k7c7	s
klidnějšími	klidný	k2eAgFnPc7d2	klidnější
pasážemi	pasáž	k1gFnPc7	pasáž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příležitostně	příležitostně	k6eAd1	příležitostně
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
elektronické	elektronický	k2eAgFnSc2d1	elektronická
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
cembala	cembalo	k1gNnPc4	cembalo
<g/>
,	,	kIx,	,
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
varhany	varhany	k1gInPc4	varhany
a	a	k8xC	a
pěvecké	pěvecký	k2eAgInPc4d1	pěvecký
soubory	soubor	k1gInPc4	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
nástroje	nástroj	k1gInPc1	nástroj
dávají	dávat	k5eAaImIp3nP	dávat
hudbě	hudba	k1gFnSc3	hudba
orchestrální	orchestrální	k2eAgFnSc4d1	orchestrální
nebo	nebo	k8xC	nebo
chrámovou	chrámový	k2eAgFnSc4d1	chrámová
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
skupiny	skupina	k1gFnPc1	skupina
mají	mít	k5eAaImIp3nP	mít
sklon	sklon	k1gInSc4	sklon
k	k	k7c3	k
častému	častý	k2eAgNnSc3d1	časté
používání	používání	k1gNnSc3	používání
kláves	klávesa	k1gFnPc2	klávesa
<g/>
,	,	kIx,	,
kterých	který	k3yRgFnPc2	který
využívají	využívat	k5eAaImIp3nP	využívat
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgInSc1d1	hlavní
nástroj	nástroj	k1gInSc1	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
jsou	být	k5eAaImIp3nP	být
skupiny	skupina	k1gFnPc1	skupina
používající	používající	k2eAgFnSc2d1	používající
klávesy	klávesa	k1gFnSc2	klávesa
označovány	označován	k2eAgFnPc1d1	označována
jako	jako	k8xC	jako
hrající	hrající	k2eAgInSc1d1	hrající
symfonický	symfonický	k2eAgInSc1d1	symfonický
black	black	k1gInSc1	black
metal	metat	k5eAaImAgInS	metat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Dimmu	Dimma	k1gFnSc4	Dimma
Borgir	Borgira	k1gFnPc2	Borgira
<g/>
,	,	kIx,	,
nahrávají	nahrávat	k5eAaImIp3nP	nahrávat
svá	svůj	k3xOyFgNnPc4	svůj
alba	album	k1gNnPc4	album
se	s	k7c7	s
symfonickými	symfonický	k2eAgInPc7d1	symfonický
orchestry	orchestr	k1gInPc7	orchestr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
úmyslně	úmyslně	k6eAd1	úmyslně
vytvářena	vytvářet	k5eAaImNgFnS	vytvářet
technicky	technicky	k6eAd1	technicky
velmi	velmi	k6eAd1	velmi
chudá	chudý	k2eAgFnSc1d1	chudá
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
efekt	efekt	k1gInSc1	efekt
demonstruje	demonstrovat	k5eAaBmIp3nS	demonstrovat
album	album	k1gNnSc4	album
Transilvanian	Transilvanian	k1gMnSc1	Transilvanian
Hunger	Hunger	k1gMnSc1	Hunger
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Darkthrone	Darkthron	k1gMnSc5	Darkthron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Image	image	k1gFnPc4	image
a	a	k8xC	a
živé	živý	k2eAgNnSc4d1	živé
vystupování	vystupování	k1gNnSc4	vystupování
skupin	skupina	k1gFnPc2	skupina
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
žánrů	žánr	k1gInPc2	žánr
mnoho	mnoho	k6eAd1	mnoho
blackmetalových	blackmetalův	k2eAgFnPc2d1	blackmetalův
skupin	skupina	k1gFnPc2	skupina
nevystupuje	vystupovat	k5eNaImIp3nS	vystupovat
na	na	k7c4	na
živo	živ	k2eAgNnSc4d1	živo
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
o	o	k7c4	o
jednočlenné	jednočlenný	k2eAgFnPc4d1	jednočlenná
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
si	se	k3xPyFc3	se
vybraly	vybrat	k5eAaPmAgFnP	vybrat
kariéru	kariéra	k1gFnSc4	kariéra
bez	bez	k7c2	bez
živých	živý	k2eAgNnPc2d1	živé
vystupování	vystupování	k1gNnPc2	vystupování
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jejich	jejich	k3xOp3gFnSc7	jejich
přední	přední	k2eAgMnSc1d1	přední
zástupce	zástupce	k1gMnSc1	zástupce
patří	patřit	k5eAaImIp3nS	patřit
Burzum	Burzum	k1gInSc4	Burzum
<g/>
,	,	kIx,	,
Darkthrone	Darkthron	k1gInSc5	Darkthron
a	a	k8xC	a
Xasthur	Xasthura	k1gFnPc2	Xasthura
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
jiné	jiný	k2eAgNnSc4d1	jiné
jedno	jeden	k4xCgNnSc4	jeden
<g/>
-	-	kIx~	-
nebo	nebo	k8xC	nebo
dvoučlenné	dvoučlenný	k2eAgFnSc2d1	dvoučlenná
skupiny	skupina	k1gFnSc2	skupina
(	(	kIx(	(
<g/>
Satyricon	Satyricona	k1gFnPc2	Satyricona
<g/>
,	,	kIx,	,
Nargaroth	Nargarotha	k1gFnPc2	Nargarotha
<g/>
,	,	kIx,	,
Satanic	satanice	k1gFnPc2	satanice
Warmaster	Warmastra	k1gFnPc2	Warmastra
<g/>
)	)	kIx)	)
účinkují	účinkovat	k5eAaImIp3nP	účinkovat
často	často	k6eAd1	často
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
hudebníky	hudebník	k1gMnPc7	hudebník
na	na	k7c6	na
živých	živý	k2eAgNnPc6d1	živé
vystoupeních	vystoupení	k1gNnPc6	vystoupení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Živá	živý	k2eAgNnPc1d1	živé
vystoupení	vystoupení	k1gNnPc1	vystoupení
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
obohacena	obohatit	k5eAaPmNgNnP	obohatit
o	o	k7c4	o
divadelní	divadelní	k2eAgFnSc4d1	divadelní
techniky	technika	k1gFnPc4	technika
a	a	k8xC	a
jevištní	jevištní	k2eAgFnPc4d1	jevištní
rekvizity	rekvizita	k1gFnPc4	rekvizita
anebo	anebo	k8xC	anebo
ohnivou	ohnivý	k2eAgFnSc4d1	ohnivá
show	show	k1gFnSc4	show
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
plivání	plivání	k1gNnSc2	plivání
ohně	oheň	k1gInSc2	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Skupiny	skupina	k1gFnSc2	skupina
Mayhem	Mayh	k1gInSc7	Mayh
<g/>
,	,	kIx,	,
<g/>
Gorgoroth	Gorgoroth	k1gInSc1	Gorgoroth
<g/>
,	,	kIx,	,
Cradle	Cradle	k1gFnSc1	Cradle
of	of	k?	of
Filth	Filtha	k1gFnPc2	Filtha
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
proslulé	proslulý	k2eAgInPc1d1	proslulý
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
groteskních	groteskní	k2eAgFnPc6d1	groteskní
a	a	k8xC	a
kontroverzních	kontroverzní	k2eAgFnPc6d1	kontroverzní
jevištních	jevištní	k2eAgFnPc6d1	jevištní
show	show	k1gFnPc6	show
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
hudebníků	hudebník	k1gMnPc2	hudebník
si	se	k3xPyFc3	se
osvojilo	osvojit	k5eAaPmAgNnS	osvojit
neo-medieval	oedievat	k5eNaBmAgInS	o-medievat
(	(	kIx(	(
<g/>
napodobující	napodobující	k2eAgInSc1d1	napodobující
středověk	středověk	k1gInSc1	středověk
<g/>
)	)	kIx)	)
styl	styl	k1gInSc1	styl
oblékání	oblékání	k1gNnSc2	oblékání
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
kožené	kožený	k2eAgNnSc4d1	kožené
oblečení	oblečení	k1gNnSc4	oblečení
<g/>
,	,	kIx,	,
bodce	bodec	k1gInPc4	bodec
<g/>
,	,	kIx,	,
otrokářskou	otrokářský	k2eAgFnSc4d1	otrokářská
výbavu	výbava	k1gFnSc4	výbava
<g/>
,	,	kIx,	,
erby	erb	k1gInPc4	erb
a	a	k8xC	a
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
hudebníků	hudebník	k1gMnPc2	hudebník
používá	používat	k5eAaImIp3nS	používat
přezdívky	přezdívka	k1gFnSc2	přezdívka
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
inspirované	inspirovaný	k2eAgFnPc1d1	inspirovaná
mytologií	mytologie	k1gFnSc7	mytologie
nebo	nebo	k8xC	nebo
folklórem	folklór	k1gInSc7	folklór
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Bå	Bå	k1gMnSc1	Bå
Eithun	Eithun	k1gMnSc1	Eithun
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Emperor	Emperora	k1gFnPc2	Emperora
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
označil	označit	k5eAaPmAgMnS	označit
jako	jako	k8xS	jako
Faust	Faust	k1gMnSc1	Faust
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obaly	obal	k1gInPc1	obal
alb	alba	k1gFnPc2	alba
bývají	bývat	k5eAaImIp3nP	bývat
atmosférické	atmosférický	k2eAgFnPc1d1	atmosférická
nebo	nebo	k8xC	nebo
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
přírodní	přírodní	k2eAgFnPc4d1	přírodní
či	či	k8xC	či
fantastické	fantastický	k2eAgFnPc4d1	fantastická
krajiny	krajina	k1gFnPc4	krajina
(	(	kIx(	(
<g/>
album	album	k1gNnSc1	album
Filosofem	filosof	k1gMnSc7	filosof
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Burzum	Burzum	k1gNnSc1	Burzum
<g/>
,	,	kIx,	,
album	album	k1gNnSc1	album
In	In	k1gFnSc2	In
The	The	k1gFnSc2	The
Nightside	Nightsid	k1gInSc5	Nightsid
Eclipse	Eclips	k1gMnSc2	Eclips
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Emperor	Emperor	k1gInSc1	Emperor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiné	jiný	k2eAgFnPc1d1	jiná
skupiny	skupina	k1gFnPc1	skupina
používají	používat	k5eAaImIp3nP	používat
motivy	motiv	k1gInPc7	motiv
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
sexuálních	sexuální	k2eAgFnPc2d1	sexuální
zvrhlostí	zvrhlost	k1gFnPc2	zvrhlost
nebo	nebo	k8xC	nebo
obrazových	obrazový	k2eAgInPc2d1	obrazový
výjevů	výjev	k1gInPc2	výjev
(	(	kIx(	(
<g/>
album	album	k1gNnSc1	album
Opus	opus	k1gInSc1	opus
Nocturne	Nocturn	k1gInSc5	Nocturn
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Marduk	Marduk	k1gInSc1	Marduk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
běžné	běžný	k2eAgFnPc1d1	běžná
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
satanistické	satanistický	k2eAgInPc4d1	satanistický
motivy	motiv	k1gInPc4	motiv
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
obrácené	obrácený	k2eAgInPc4d1	obrácený
pentagramy	pentagram	k1gInPc4	pentagram
nebo	nebo	k8xC	nebo
obrácené	obrácený	k2eAgInPc4d1	obrácený
kříže	kříž	k1gInPc4	kříž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
rysů	rys	k1gInPc2	rys
blackmetalového	blackmetalový	k2eAgInSc2d1	blackmetalový
image	image	k1gInSc2	image
je	být	k5eAaImIp3nS	být
líčení	líčení	k1gNnSc1	líčení
corpsepaint	corpsepainta	k1gFnPc2	corpsepainta
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
napodobující	napodobující	k2eAgFnSc4d1	napodobující
mrtvolu	mrtvola	k1gFnSc4	mrtvola
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
warpaint	warpaint	k1gInSc1	warpaint
černý	černý	k2eAgInSc1d1	černý
a	a	k8xC	a
bílý	bílý	k2eAgInSc1d1	bílý
makeup	makeup	k1gInSc1	makeup
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
napodobující	napodobující	k2eAgFnPc1d1	napodobující
obličejové	obličejový	k2eAgFnPc1d1	obličejová
válečné	válečný	k2eAgFnPc1d1	válečná
barvy	barva	k1gFnPc1	barva
pohanských	pohanský	k2eAgMnPc2d1	pohanský
válečníků	válečník	k1gMnPc2	válečník
<g/>
,	,	kIx,	,
majících	mající	k2eAgMnPc2d1	mající
zastrašit	zastrašit	k5eAaPmF	zastrašit
nepřítele	nepřítel	k1gMnSc4	nepřítel
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
bitvy	bitva	k1gFnSc2	bitva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
falešnou	falešný	k2eAgFnSc7d1	falešná
krví	krev	k1gFnSc7	krev
<g/>
.	.	kIx.	.
</s>
<s>
Inspirované	inspirovaný	k2eAgNnSc1d1	inspirované
bývalým	bývalý	k2eAgInSc7d1	bývalý
frontmanem	frontman	k1gInSc7	frontman
skupiny	skupina	k1gFnSc2	skupina
Mayhem	Mayh	k1gInSc7	Mayh
Deadem	Deado	k1gNnSc7	Deado
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Začátek	začátek	k1gInSc1	začátek
tohoto	tento	k3xDgInSc2	tento
stylu	styl	k1gInSc2	styl
lze	lze	k6eAd1	lze
vystopovat	vystopovat	k5eAaPmF	vystopovat
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
Bathory	Bathora	k1gFnSc2	Bathora
<g/>
,	,	kIx,	,
založené	založený	k2eAgFnSc2d1	založená
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
po	po	k7c6	po
Alžbětě	Alžběta	k1gFnSc6	Alžběta
Báthoryové	Báthoryové	k2eAgFnSc6d1	Báthoryové
<g/>
,	,	kIx,	,
známé	známý	k2eAgFnSc6d1	známá
též	též	k6eAd1	též
jako	jako	k9	jako
Čachtická	čachtický	k2eAgFnSc1d1	Čachtická
paní	paní	k1gFnSc1	paní
<g/>
.	.	kIx.	.
</s>
<s>
Frontmanem	Frontman	k1gInSc7	Frontman
kapely	kapela	k1gFnSc2	kapela
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
hlavním	hlavní	k2eAgMnSc7d1	hlavní
skladatelem	skladatel	k1gMnSc7	skladatel
byl	být	k5eAaImAgMnS	být
Quorthon	Quorthon	k1gMnSc1	Quorthon
(	(	kIx(	(
<g/>
Tomas	Tomas	k1gMnSc1	Tomas
Forsberg	Forsberg	k1gMnSc1	Forsberg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
jeho	jeho	k3xOp3gInPc1	jeho
drsné	drsný	k2eAgInPc1d1	drsný
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
skřípavé	skřípavý	k2eAgInPc1d1	skřípavý
a	a	k8xC	a
vysoké	vysoký	k2eAgInPc4d1	vysoký
tóny	tón	k1gInPc4	tón
doprovázené	doprovázený	k2eAgInPc4d1	doprovázený
občasnými	občasný	k2eAgInPc7d1	občasný
výkřiky	výkřik	k1gInPc7	výkřik
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
jako	jako	k9	jako
texty	text	k1gInPc4	text
zaměřené	zaměřený	k2eAgInPc4d1	zaměřený
na	na	k7c4	na
temná	temný	k2eAgNnPc4d1	temné
témata	téma	k1gNnPc4	téma
a	a	k8xC	a
obsahující	obsahující	k2eAgFnPc4d1	obsahující
antikřesťanské	antikřesťanský	k2eAgFnPc4d1	antikřesťanská
a	a	k8xC	a
"	"	kIx"	"
<g/>
satanské	satanský	k2eAgInPc4d1	satanský
<g/>
"	"	kIx"	"
odkazy	odkaz	k1gInPc4	odkaz
<g/>
,	,	kIx,	,
definovaly	definovat	k5eAaBmAgInP	definovat
začátky	začátek	k1gInPc1	začátek
black	blacka	k1gFnPc2	blacka
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
tento	tento	k3xDgInSc4	tento
styl	styl	k1gInSc4	styl
použila	použít	k5eAaPmAgFnS	použít
u	u	k7c2	u
prvních	první	k4xOgInPc2	první
čtyř	čtyři	k4xCgInPc2	čtyři
alb	album	k1gNnPc2	album
<g/>
,	,	kIx,	,
pričemž	pričemž	k6eAd1	pričemž
třetí	třetí	k4xOgNnSc1	třetí
album	album	k1gNnSc1	album
Under	Under	k1gMnSc1	Under
the	the	k?	the
Sign	signum	k1gNnPc2	signum
of	of	k?	of
the	the	k?	the
Black	Black	k1gMnSc1	Black
Mark	Mark	k1gMnSc1	Mark
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgFnPc4d1	vydaná
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejvíce	nejvíce	k6eAd1	nejvíce
ovlivňující	ovlivňující	k2eAgFnSc4d1	ovlivňující
historii	historie	k1gFnSc4	historie
black	black	k1gMnSc1	black
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
tomuto	tento	k3xDgInSc3	tento
hudebního	hudební	k2eAgInSc2d1	hudební
stylu	styl	k1gInSc2	styl
však	však	k9	však
dala	dát	k5eAaPmAgFnS	dát
až	až	k9	až
anglická	anglický	k2eAgFnSc1d1	anglická
kapela	kapela	k1gFnSc1	kapela
Venom	Venom	k1gInSc4	Venom
albem	album	k1gNnSc7	album
Black	Blacka	k1gFnPc2	Blacka
Metal	metal	k1gInSc4	metal
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
kapelou	kapela	k1gFnSc7	kapela
patřící	patřící	k2eAgFnSc7d1	patřící
k	k	k7c3	k
NWOBHM	NWOBHM	kA	NWOBHM
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přispěla	přispět	k5eAaPmAgFnS	přispět
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
black	blacka	k1gFnPc2	blacka
metalu	metal	k1gInSc2	metal
je	být	k5eAaImIp3nS	být
kapela	kapela	k1gFnSc1	kapela
Witchfynde	Witchfynd	k1gInSc5	Witchfynd
(	(	kIx(	(
<g/>
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
wake	wake	k1gInSc1	wake
up	up	k?	up
screaming	screaming	k1gInSc1	screaming
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Death	Death	k1gMnSc1	Death
SS	SS	kA	SS
(	(	kIx(	(
<g/>
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Murder	Murder	k1gInSc1	Murder
Angels	Angels	k1gInSc1	Angels
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
definování	definování	k1gNnSc3	definování
typických	typický	k2eAgInPc2d1	typický
znaků	znak	k1gInPc2	znak
stylu	styl	k1gInSc2	styl
došlo	dojít	k5eAaPmAgNnS	dojít
díky	díky	k7c3	díky
kapelám	kapela	k1gFnPc3	kapela
Mercyful	Mercyfula	k1gFnPc2	Mercyfula
Fate	Fate	k1gFnPc2	Fate
<g/>
,	,	kIx,	,
Hellhammer	Hellhammra	k1gFnPc2	Hellhammra
a	a	k8xC	a
Celtic	Celtice	k1gFnPc2	Celtice
Frost	Frost	k1gFnSc1	Frost
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
první	první	k4xOgFnSc1	první
alba	alba	k1gFnSc1	alba
Slayer	Slayer	k1gInSc4	Slayer
a	a	k8xC	a
Possessed	Possessed	k1gInSc4	Possessed
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
blackmetalových	blackmetalův	k2eAgFnPc2d1	blackmetalův
kapel	kapela	k1gFnPc2	kapela
můžeme	moct	k5eAaImIp1nP	moct
zmínit	zmínit	k5eAaPmF	zmínit
např.	např.	kA	např.
Master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hammer	Hammer	k1gInSc1	Hammer
<g/>
,	,	kIx,	,
Root	Root	k1gInSc1	Root
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
polovina	polovina	k1gFnSc1	polovina
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Törr	Törr	k1gMnSc1	Törr
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
však	však	k9	však
stále	stále	k6eAd1	stále
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hrají	hrát	k5eAaImIp3nP	hrát
"	"	kIx"	"
<g/>
jen	jen	k9	jen
o	o	k7c4	o
něco	něco	k3yInSc4	něco
tvrdší	tvrdý	k2eAgInSc1d2	tvrdší
rock	rock	k1gInSc1	rock
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
roll	rollum	k1gNnPc2	rollum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
či	či	k8xC	či
již	již	k6eAd1	již
neexistující	existující	k2eNgFnSc4d1	neexistující
kapelu	kapela	k1gFnSc4	kapela
RITUAL	RITUAL	kA	RITUAL
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
potom	potom	k6eAd1	potom
Enochian	Enochian	k1gInSc4	Enochian
<g/>
,	,	kIx,	,
Maniac	Maniac	k1gInSc4	Maniac
Butcher	Butchra	k1gFnPc2	Butchra
<g/>
,	,	kIx,	,
Infernal	Infernal	k1gFnPc2	Infernal
Heretic	Heretice	k1gFnPc2	Heretice
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
Morrior	Morriora	k1gFnPc2	Morriora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgInSc1d1	další
rozmach	rozmach	k1gInSc1	rozmach
tento	tento	k3xDgInSc4	tento
styl	styl	k1gInSc4	styl
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
počátkem	počátkem	k7c2	počátkem
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
tzv.	tzv.	kA	tzv.
druhé	druhý	k4xOgFnSc2	druhý
vlny	vlna	k1gFnSc2	vlna
black	blacka	k1gFnPc2	blacka
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
v	v	k7c6	v
rozvoji	rozvoj	k1gInSc6	rozvoj
skandinávské	skandinávský	k2eAgFnSc2d1	skandinávská
black	black	k6eAd1	black
metalové	metalový	k2eAgFnSc2d1	metalová
scény	scéna	k1gFnSc2	scéna
hrála	hrát	k5eAaImAgFnS	hrát
skupina	skupina	k1gFnSc1	skupina
Mayhem	Mayh	k1gInSc7	Mayh
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
albem	album	k1gNnSc7	album
Deathcrush	Deathcrusha	k1gFnPc2	Deathcrusha
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
<g/>
.	.	kIx.	.
také	také	k6eAd1	také
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
Until	Until	k1gMnSc1	Until
the	the	k?	the
Light	Light	k1gMnSc1	Light
Takes	Takes	k1gMnSc1	Takes
Us	Us	k1gMnSc1	Us
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
dopad	dopad	k1gInSc1	dopad
na	na	k7c4	na
norskou	norský	k2eAgFnSc4d1	norská
black	black	k6eAd1	black
metalovou	metalový	k2eAgFnSc4d1	metalová
scénu	scéna	k1gFnSc4	scéna
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
kapela	kapela	k1gFnSc1	kapela
Burzum	Burzum	k1gInSc1	Burzum
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Varg	Varga	k1gFnPc2	Varga
"	"	kIx"	"
<g/>
Count	Count	k1gMnSc1	Count
Grishnackh	Grishnackh	k1gMnSc1	Grishnackh
<g/>
"	"	kIx"	"
Vikernesem	Vikernes	k1gMnSc7	Vikernes
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
k	k	k7c3	k
Mayhem	Mayh	k1gInSc7	Mayh
váže	vázat	k5eAaImIp3nS	vázat
velmi	velmi	k6eAd1	velmi
temná	temný	k2eAgFnSc1d1	temná
historie	historie	k1gFnSc1	historie
<g/>
.	.	kIx.	.
</s>
<s>
Vikernes	Vikernes	k1gMnSc1	Vikernes
byl	být	k5eAaImAgMnS	být
průkopníkem	průkopník	k1gMnSc7	průkopník
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
zvuku	zvuk	k1gInSc2	zvuk
black	black	k1gInSc1	black
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
tremolo	tremolo	k6eAd1	tremolo
efektu	efekt	k1gInSc2	efekt
v	v	k7c6	v
kytarových	kytarový	k2eAgNnPc6d1	kytarové
sólech	sólo	k1gNnPc6	sólo
a	a	k8xC	a
tvrdých	tvrdý	k2eAgInPc2d1	tvrdý
vokálů	vokál	k1gInPc2	vokál
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
kapely	kapela	k1gFnPc4	kapela
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
výrazně	výrazně	k6eAd1	výrazně
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
druhou	druhý	k4xOgFnSc4	druhý
vlnu	vlna	k1gFnSc4	vlna
black	blacka	k1gFnPc2	blacka
metalu	metal	k1gInSc2	metal
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Darkthrone	Darkthron	k1gMnSc5	Darkthron
<g/>
,	,	kIx,	,
Immortal	Immortal	k1gFnSc5	Immortal
<g/>
,	,	kIx,	,
Emperor	Emperor	k1gMnSc1	Emperor
<g/>
,	,	kIx,	,
Satyricon	Satyricon	k1gMnSc1	Satyricon
<g/>
,	,	kIx,	,
Dimmu	Dimm	k1gInSc2	Dimm
Borgir	Borgira	k1gFnPc2	Borgira
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
kapely	kapela	k1gFnPc1	kapela
bývají	bývat	k5eAaImIp3nP	bývat
označovány	označovat	k5eAaImNgFnP	označovat
jako	jako	k8xS	jako
True	True	k1gInSc1	True
Norwegian	Norwegiana	k1gFnPc2	Norwegiana
black	blacka	k1gFnPc2	blacka
metal	metal	k1gInSc1	metal
(	(	kIx(	(
<g/>
TNBM	TNBM	kA	TNBM
<g/>
,	,	kIx,	,
pravý	pravý	k2eAgMnSc1d1	pravý
norský	norský	k2eAgMnSc1d1	norský
black	black	k1gMnSc1	black
metal	metat	k5eAaImAgMnS	metat
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
true	true	k6eAd1	true
black	black	k6eAd1	black
metal	metat	k5eAaImAgMnS	metat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
současné	současný	k2eAgFnSc6d1	současná
české	český	k2eAgFnSc6d1	Česká
scéně	scéna	k1gFnSc6	scéna
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
kapely	kapela	k1gFnPc1	kapela
jako	jako	k8xC	jako
Lingam	Lingam	k1gInSc1	Lingam
<g/>
,	,	kIx,	,
Solfernus	Solfernus	k1gMnSc1	Solfernus
<g/>
,	,	kIx,	,
Oblomov	Oblomov	k1gMnSc1	Oblomov
<g/>
,	,	kIx,	,
Trollech	troll	k1gMnPc6	troll
<g/>
,	,	kIx,	,
Heiden	Heidna	k1gFnPc2	Heidna
<g/>
,	,	kIx,	,
Stíny	stín	k1gInPc7	stín
plamenů	plamen	k1gInPc2	plamen
<g/>
,	,	kIx,	,
Sezarbil	Sezarbil	k1gMnSc1	Sezarbil
<g/>
,	,	kIx,	,
Wyrm	Wyrm	k1gMnSc1	Wyrm
<g/>
,	,	kIx,	,
Svardenvyrd	Svardenvyrd	k1gMnSc1	Svardenvyrd
<g/>
,	,	kIx,	,
Hyperborean	Hyperborean	k1gMnSc1	Hyperborean
Desire	Desir	k1gInSc5	Desir
<g/>
,	,	kIx,	,
Forest	Forest	k1gFnSc1	Forest
<g/>
,	,	kIx,	,
Silva	Silva	k1gFnSc1	Silva
Nigra	nigr	k1gMnSc2	nigr
<g/>
,	,	kIx,	,
Sorath	Sorath	k1gInSc1	Sorath
<g/>
,	,	kIx,	,
Adultery	Adulter	k1gInPc1	Adulter
<g/>
,	,	kIx,	,
Born	Born	k1gInSc1	Born
in	in	k?	in
Morgue	Morgue	k1gInSc1	Morgue
<g/>
,	,	kIx,	,
Anubion	Anubion	k1gInSc1	Anubion
<g/>
,	,	kIx,	,
Bleeding	Bleeding	k1gInSc1	Bleeding
Death	Death	k1gInSc1	Death
<g/>
,	,	kIx,	,
Sekhmet	Sekhmet	k1gInSc1	Sekhmet
<g/>
,	,	kIx,	,
Inferno	inferno	k1gNnSc1	inferno
<g/>
,	,	kIx,	,
Tautenauer	Tautenauer	k1gInSc1	Tautenauer
666	[number]	k4	666
<g/>
,	,	kIx,	,
Surt	Surt	k1gInSc1	Surt
<g/>
,	,	kIx,	,
Naurrakar	Naurrakar	k1gInSc1	Naurrakar
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
narůstající	narůstající	k2eAgFnSc4d1	narůstající
rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
subžánrů	subžánr	k1gMnPc2	subžánr
black	black	k6eAd1	black
metalu	metal	k1gInSc2	metal
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
začíná	začínat	k5eAaImIp3nS	začínat
používat	používat	k5eAaImF	používat
označení	označení	k1gNnSc2	označení
tzv.	tzv.	kA	tzv.
třetí	třetí	k4xOgInSc4	třetí
vlny	vlna	k1gFnSc2	vlna
black	black	k1gInSc4	black
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Podžánry	Podžánra	k1gFnPc4	Podžánra
black	black	k6eAd1	black
metalu	metal	k1gInSc2	metal
==	==	k?	==
</s>
</p>
<p>
<s>
Depressive	Depressiev	k1gFnPc4	Depressiev
suicidal	suicidat	k5eAaImAgInS	suicidat
black	black	k1gInSc1	black
metal	metal	k1gInSc1	metal
(	(	kIx(	(
<g/>
DSBM	DSBM	kA	DSBM
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
depresivní	depresivní	k2eAgInSc1d1	depresivní
sebevražedný	sebevražedný	k2eAgInSc1d1	sebevražedný
black	black	k1gInSc1	black
metal	metat	k5eAaImAgInS	metat
<g/>
,	,	kIx,	,
představují	představovat	k5eAaImIp3nP	představovat
ho	on	k3xPp3gInSc4	on
kapely	kapela	k1gFnPc1	kapela
Xasthur	Xasthur	k1gMnSc1	Xasthur
<g/>
,	,	kIx,	,
Silencer	Silencer	k1gMnSc1	Silencer
<g/>
,	,	kIx,	,
Nargaroth	Nargaroth	k1gMnSc1	Nargaroth
<g/>
,	,	kIx,	,
Coldworld	Coldworld	k1gMnSc1	Coldworld
<g/>
,	,	kIx,	,
Anti	Anti	k1gNnSc1	Anti
<g/>
,	,	kIx,	,
Shining	Shining	k1gInSc1	Shining
<g/>
,	,	kIx,	,
Trist	Trist	k1gInSc1	Trist
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
<g/>
Blackgaze	Blackgaha	k1gFnSc6	Blackgaha
(	(	kIx(	(
<g/>
post-black	postlack	k1gInSc1	post-black
metal	metal	k1gInSc1	metal
<g/>
)	)	kIx)	)
<g/>
Ambient	Ambient	k1gMnSc1	Ambient
black	black	k1gMnSc1	black
metal	metat	k5eAaImAgMnS	metat
</s>
</p>
<p>
<s>
Ambient	Ambient	k1gInSc1	Ambient
black	black	k1gInSc1	black
metal	metal	k1gInSc1	metal
(	(	kIx(	(
<g/>
atmospheric	atmospheric	k1gMnSc1	atmospheric
black	black	k1gMnSc1	black
metal	metat	k5eAaImAgMnS	metat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
prvky	prvek	k1gInPc4	prvek
black	blacka	k1gFnPc2	blacka
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
ambientu	ambienta	k1gFnSc4	ambienta
nebo	nebo	k8xC	nebo
dark	dark	k1gInSc4	dark
ambientu	ambient	k1gInSc2	ambient
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
na	na	k7c4	na
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
kytaru	kytara	k1gFnSc4	kytara
<g/>
,	,	kIx,	,
syntetizátory	syntetizátor	k1gInPc4	syntetizátor
a	a	k8xC	a
klávesy	klávesa	k1gFnPc4	klávesa
v	v	k7c6	v
"	"	kIx"	"
<g/>
atmosférickém	atmosférický	k2eAgNnSc6d1	atmosférické
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
=	=	kIx~	=
ambientním	ambientní	k2eAgInSc6d1	ambientní
<g/>
)	)	kIx)	)
stylu	styl	k1gInSc6	styl
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
dozvuku	dozvuk	k1gInSc2	dozvuk
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejvýraznější	výrazný	k2eAgFnSc7d3	nejvýraznější
kapelou	kapela	k1gFnSc7	kapela
tohoto	tento	k3xDgInSc2	tento
subžánru	subžánr	k1gInSc2	subžánr
je	být	k5eAaImIp3nS	být
kapela	kapela	k1gFnSc1	kapela
Darkspace	Darkspace	k1gFnSc2	Darkspace
<g/>
.	.	kIx.	.
<g/>
Symphonic	Symphonice	k1gFnPc2	Symphonice
black	black	k6eAd1	black
metal	metat	k5eAaImAgMnS	metat
</s>
</p>
<p>
<s>
Symphonic	Symphonice	k1gFnPc2	Symphonice
black	black	k6eAd1	black
metal	metal	k1gInSc1	metal
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
black	black	k6eAd1	black
metal	metal	k1gInSc4	metal
a	a	k8xC	a
orchestrální	orchestrální	k2eAgInPc4d1	orchestrální
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgInPc4d1	jiný
klasické	klasický	k2eAgInPc4d1	klasický
nástroje	nástroj	k1gInPc4	nástroj
(	(	kIx(	(
<g/>
klávesy	klávesa	k1gFnPc4	klávesa
<g/>
,	,	kIx,	,
žesťové	žesťový	k2eAgInPc4d1	žesťový
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
podtrhující	podtrhující	k2eAgFnSc4d1	podtrhující
strašidelnou	strašidelný	k2eAgFnSc4d1	strašidelná
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
v	v	k7c6	v
symphonic	symphonice	k1gFnPc2	symphonice
black	black	k1gInSc1	black
metalu	metal	k1gInSc2	metal
se	se	k3xPyFc4	se
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
objevit	objevit	k5eAaPmF	objevit
čistý	čistý	k2eAgInSc4d1	čistý
ženský	ženský	k2eAgInSc4d1	ženský
vokál	vokál	k1gInSc4	vokál
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgFnPc4d3	nejznámější
kapely	kapela	k1gFnPc4	kapela
tohoto	tento	k3xDgInSc2	tento
subžánru	subžánr	k1gInSc2	subžánr
patří	patřit	k5eAaImIp3nS	patřit
Emperor	Emperor	k1gMnSc1	Emperor
<g/>
,	,	kIx,	,
Cradle	Cradle	k1gMnSc1	Cradle
of	of	k?	of
Filth	Filth	k1gMnSc1	Filth
<g/>
,	,	kIx,	,
Dimmu	Dimma	k1gFnSc4	Dimma
Borgir	Borgir	k1gInSc4	Borgir
<g/>
,	,	kIx,	,
Carach	Carach	k1gInSc4	Carach
Angren	Angrna	k1gFnPc2	Angrna
nebo	nebo	k8xC	nebo
méně	málo	k6eAd2	málo
známá	známý	k2eAgFnSc1d1	známá
kapela	kapela	k1gFnSc1	kapela
z	z	k7c2	z
Tchaj-wanu	Tchajan	k1gInSc2	Tchaj-wan
CHtHoniC	CHtHoniC	k1gFnSc2	CHtHoniC
<g/>
.	.	kIx.	.
</s>
<s>
Progressive	Progressivat	k5eAaPmIp3nS	Progressivat
black	black	k6eAd1	black
metal	metal	k1gInSc1	metal
</s>
</p>
<p>
<s>
kapely	kapela	k1gFnPc1	kapela
jako	jako	k8xS	jako
Enslaved	Enslaved	k1gInSc1	Enslaved
a	a	k8xC	a
Windir	Windir	k1gInSc1	Windir
nebo	nebo	k8xC	nebo
česká	český	k2eAgFnSc1d1	Česká
kapela	kapela	k1gFnSc1	kapela
Abstract	Abstract	k1gMnSc1	Abstract
EssenceBlackened	EssenceBlackened	k1gMnSc1	EssenceBlackened
death	death	k1gMnSc1	death
metal	metat	k5eAaImAgMnS	metat
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
mix	mix	k1gInSc4	mix
black	blacka	k1gFnPc2	blacka
metalu	metal	k1gInSc2	metal
s	s	k7c7	s
death	death	k1gInSc1	death
metalem	metal	k1gInSc7	metal
-	-	kIx~	-
např.	např.	kA	např.
kapela	kapela	k1gFnSc1	kapela
Behemoth	Behemoth	k1gMnSc1	Behemoth
<g/>
.	.	kIx.	.
<g/>
Unblack	Unblack	k1gMnSc1	Unblack
metal	metat	k5eAaImAgMnS	metat
</s>
</p>
<p>
<s>
Méně	málo	k6eAd2	málo
významný	významný	k2eAgInSc1d1	významný
subžánr	subžánr	k1gInSc1	subžánr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
působí	působit	k5eAaImIp3nS	působit
dojmem	dojem	k1gInSc7	dojem
stejně	stejně	k6eAd1	stejně
agresivním	agresivní	k2eAgInSc7d1	agresivní
<g/>
,	,	kIx,	,
odporným	odporný	k2eAgInSc7d1	odporný
<g/>
,	,	kIx,	,
mrazivým	mrazivý	k2eAgInSc7d1	mrazivý
a	a	k8xC	a
temným	temný	k2eAgInSc7d1	temný
jako	jako	k8xS	jako
běžný	běžný	k2eAgInSc4d1	běžný
black	black	k1gInSc4	black
metal	metal	k1gInSc1	metal
druhé	druhý	k4xOgFnSc2	druhý
vlny	vlna	k1gFnSc2	vlna
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
texty	text	k1gInPc1	text
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
zaměřují	zaměřovat	k5eAaImIp3nP	zaměřovat
na	na	k7c4	na
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
lásku	láska	k1gFnSc4	láska
a	a	k8xC	a
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
zlu	zlo	k1gNnSc3	zlo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
taková	takový	k3xDgFnSc1	takový
forma	forma	k1gFnSc1	forma
křesťanského	křesťanský	k2eAgInSc2d1	křesťanský
metalu	metal	k1gInSc2	metal
(	(	kIx(	(
<g/>
white	white	k5eAaPmIp2nP	white
metal	metal	k1gInSc4	metal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
po	po	k7c6	po
instrumentální	instrumentální	k2eAgFnSc6d1	instrumentální
stránce	stránka	k1gFnSc6	stránka
nejblíže	blízce	k6eAd3	blízce
black	black	k6eAd1	black
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
<g/>
Pagan	Pagan	k1gMnSc1	Pagan
black	black	k1gMnSc1	black
metal	metat	k5eAaImAgMnS	metat
</s>
</p>
<p>
<s>
Pagan	Pagan	k1gMnSc1	Pagan
black	black	k1gMnSc1	black
metal	metal	k1gInSc4	metal
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
black	black	k6eAd1	black
metal	metal	k1gInSc4	metal
s	s	k7c7	s
folkovými	folkový	k2eAgInPc7d1	folkový
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Podobá	podobat	k5eAaImIp3nS	podobat
se	se	k3xPyFc4	se
melodic	melodice	k1gFnPc2	melodice
black	black	k1gInSc1	black
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
předchůdcem	předchůdce	k1gMnSc7	předchůdce
folk	folk	k1gInSc1	folk
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
<g/>
Red	Red	k1gMnSc1	Red
and	and	k?	and
Anarchist	Anarchist	k1gMnSc1	Anarchist
Black	Black	k1gMnSc1	Black
Metal	metat	k5eAaImAgMnS	metat
</s>
</p>
<p>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jako	jako	k9	jako
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
NSBM	NSBM	kA	NSBM
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
<g/>
)	)	kIx)	)
prof.	prof.	kA	prof.
Simone	Simon	k1gMnSc5	Simon
Morabito	Morabita	k1gMnSc5	Morabita
<g/>
:	:	kIx,	:
Psychiatria	Psychiatrium	k1gNnPc1	Psychiatrium
all	all	k?	all
'	'	kIx"	'
<g/>
inferno	inferno	k1gNnSc1	inferno
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
blackmetalových	blackmetalův	k2eAgFnPc2d1	blackmetalův
skupin	skupina	k1gFnPc2	skupina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
black	black	k6eAd1	black
metal	metat	k5eAaImAgMnS	metat
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Black	Black	k1gInSc1	Black
Metal	metal	k1gInSc1	metal
+	+	kIx~	+
alba	alba	k1gFnSc1	alba
na	na	k7c4	na
heavymetalencyclopedia.com	heavymetalencyclopedia.com	k1gInSc4	heavymetalencyclopedia.com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
