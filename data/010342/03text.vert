<p>
<s>
Di	Di	k?	Di
varhajt	varhajt	k1gInSc1	varhajt
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Pravda	pravda	k9	pravda
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc4	první
komunistický	komunistický	k2eAgInSc4d1	komunistický
novinový	novinový	k2eAgInSc4d1	novinový
deník	deník	k1gInSc4	deník
<g/>
,	,	kIx,	,
psaným	psaný	k2eAgMnSc7d1	psaný
v	v	k7c6	v
židovském	židovský	k2eAgInSc6d1	židovský
jazyce	jazyk	k1gInSc6	jazyk
jidiš	jidiš	k1gNnSc2	jidiš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
číslo	číslo	k1gNnSc1	číslo
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
vydavatelem	vydavatel	k1gMnSc7	vydavatel
byl	být	k5eAaImAgInS	být
Lidový	lidový	k2eAgInSc1d1	lidový
komisariát	komisariát	k1gInSc1	komisariát
pro	pro	k7c4	pro
židovské	židovský	k2eAgFnPc4d1	židovská
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
přesunu	přesun	k1gInSc6	přesun
sídla	sídlo	k1gNnSc2	sídlo
deníku	deník	k1gInSc2	deník
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
nového	nový	k2eAgNnSc2d1	nové
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
k	k	k7c3	k
ukončení	ukončení	k1gNnSc3	ukončení
jeho	jeho	k3xOp3gFnSc2	jeho
činnosti	činnost	k1gFnSc2	činnost
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nedostatku	nedostatek	k1gInSc2	nedostatek
žurnalistů	žurnalist	k1gMnPc2	žurnalist
znalých	znalý	k2eAgMnPc2d1	znalý
jazyka	jazyk	k1gInSc2	jazyk
jidiš	jidiš	k1gNnSc2	jidiš
<g/>
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
novin	novina	k1gFnPc2	novina
Di	Di	k1gFnSc2	Di
varhajt	varhajt	k1gInSc1	varhajt
převzal	převzít	k5eAaPmAgInS	převzít
nově	nově	k6eAd1	nově
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
deník	deník	k1gInSc1	deník
Der	drát	k5eAaImRp2nS	drát
Emes	Emes	k1gInSc1	Emes
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
spadal	spadat	k5eAaImAgInS	spadat
pod	pod	k7c4	pod
přímou	přímý	k2eAgFnSc4d1	přímá
kontrolu	kontrola	k1gFnSc4	kontrola
Ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
