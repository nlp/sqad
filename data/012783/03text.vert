<p>
<s>
Fleshgod	Fleshgod	k1gInSc1	Fleshgod
Apocalypse	Apocalypse	k1gFnSc2	Apocalypse
je	být	k5eAaImIp3nS	být
italská	italský	k2eAgFnSc1d1	italská
symphonic	symphonice	k1gFnPc2	symphonice
death	deatha	k1gFnPc2	deatha
metalová	metalový	k2eAgFnSc1d1	metalová
kapela	kapela	k1gFnSc1	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Založena	založen	k2eAgFnSc1d1	založena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
podepsala	podepsat	k5eAaPmAgFnS	podepsat
smlouvy	smlouva	k1gFnPc4	smlouva
s	s	k7c7	s
nahrávacími	nahrávací	k2eAgFnPc7d1	nahrávací
společnostmi	společnost	k1gFnPc7	společnost
Willowtip	Willowtip	k1gInSc1	Willowtip
a	a	k8xC	a
Nuclear	Nuclear	k1gMnSc1	Nuclear
Blast	Blast	k1gMnSc1	Blast
<g/>
.	.	kIx.	.
</s>
<s>
Vydala	vydat	k5eAaPmAgFnS	vydat
čtyři	čtyři	k4xCgNnPc4	čtyři
studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc1d1	poslední
vydala	vydat	k5eAaPmAgFnS	vydat
dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
King	King	k1gMnSc1	King
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Fleshgod	Fleshgod	k1gInSc4	Fleshgod
Apocalypse	Apocalypse	k1gFnSc2	Apocalypse
vznikli	vzniknout	k5eAaPmAgMnP	vzniknout
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2007	[number]	k4	2007
a	a	k8xC	a
vydala	vydat	k5eAaPmAgFnS	vydat
své	svůj	k3xOyFgFnPc4	svůj
první	první	k4xOgFnPc4	první
demo	demo	k2eAgFnPc4d1	demo
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Promo	Proma	k1gFnSc5	Proma
'	'	kIx"	'
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Fleshgod	Fleshgod	k1gInSc1	Fleshgod
Apocalypse	Apocalypse	k1gFnSc2	Apocalypse
poté	poté	k6eAd1	poté
podepsali	podepsat	k5eAaPmAgMnP	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Neurotic	Neurotice	k1gFnPc2	Neurotice
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
skupina	skupina	k1gFnSc1	skupina
započala	započnout	k5eAaPmAgFnS	započnout
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
koncertovala	koncertovat	k5eAaImAgFnS	koncertovat
s	s	k7c7	s
kapelami	kapela	k1gFnPc7	kapela
jako	jako	k9	jako
Behemoth	Behemoth	k1gMnSc1	Behemoth
<g/>
,	,	kIx,	,
Origin	Origin	k1gMnSc1	Origin
<g/>
,	,	kIx,	,
Dying	Dying	k1gMnSc1	Dying
Fetus	Fetus	k1gMnSc1	Fetus
<g/>
,	,	kIx,	,
Hate	Hate	k1gFnSc1	Hate
Eternal	Eternal	k1gFnSc1	Eternal
<g/>
,	,	kIx,	,
Suffocation	Suffocation	k1gInSc1	Suffocation
<g/>
,	,	kIx,	,
Napalm	napalm	k1gInSc1	napalm
Death	Deatha	k1gFnPc2	Deatha
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2008	[number]	k4	2008
skupina	skupina	k1gFnSc1	skupina
nahrála	nahrát	k5eAaPmAgFnS	nahrát
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
Oracles	Oraclesa	k1gFnPc2	Oraclesa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
podepsat	podepsat	k5eAaPmF	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Willowtip	Willowtip	k1gMnSc1	Willowtip
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Oracles	Oraclesa	k1gFnPc2	Oraclesa
bylo	být	k5eAaImAgNnS	být
následně	následně	k6eAd1	následně
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
Oracles	Oraclesa	k1gFnPc2	Oraclesa
se	se	k3xPyFc4	se
Tommaso	Tommasa	k1gFnSc5	Tommasa
Riccardi	Riccard	k1gMnPc1	Riccard
ujal	ujmout	k5eAaPmAgMnS	ujmout
vokálů	vokál	k1gInPc2	vokál
a	a	k8xC	a
rytmické	rytmický	k2eAgFnSc2d1	rytmická
kytary	kytara	k1gFnSc2	kytara
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
předtím	předtím	k6eAd1	předtím
obstarával	obstarávat	k5eAaImAgInS	obstarávat
Francesco	Francesco	k6eAd1	Francesco
Paoli	Paole	k1gFnSc4	Paole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
EP	EP	kA	EP
Mafia	Mafium	k1gNnSc2	Mafium
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
čtyři	čtyři	k4xCgFnPc4	čtyři
nové	nový	k2eAgFnPc4d1	nová
skladby	skladba	k1gFnPc4	skladba
a	a	k8xC	a
coververzi	coververze	k1gFnSc4	coververze
"	"	kIx"	"
<g/>
Blinded	Blinded	k1gInSc1	Blinded
by	by	kYmCp3nS	by
Fear	Fear	k1gInSc4	Fear
<g/>
"	"	kIx"	"
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
At	At	k1gFnSc2	At
the	the	k?	the
Gates	Gates	k1gMnSc1	Gates
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
zahájila	zahájit	k5eAaPmAgFnS	zahájit
další	další	k2eAgNnSc4d1	další
evropské	evropský	k2eAgNnSc4d1	Evropské
turné	turné	k1gNnSc4	turné
se	se	k3xPyFc4	se
Suffocation	Suffocation	k1gInSc1	Suffocation
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
nahrávání	nahrávání	k1gNnSc2	nahrávání
Mafia	Mafium	k1gNnSc2	Mafium
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
cestě	cesta	k1gFnSc6	cesta
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
i	i	k9	i
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgMnS	být
bubeník	bubeník	k1gMnSc1	bubeník
Fleshgod	Fleshgod	k1gInSc4	Fleshgod
Apocalypse	Apocalypse	k1gFnSc2	Apocalypse
Francesco	Francesco	k6eAd1	Francesco
Paoli	Paole	k1gFnSc4	Paole
současně	současně	k6eAd1	současně
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
bývalé	bývalý	k2eAgFnSc6d1	bývalá
skupině	skupina	k1gFnSc6	skupina
Hour	Hour	k1gMnSc1	Hour
of	of	k?	of
Penance	Penance	k1gFnSc2	Penance
<g/>
.	.	kIx.	.
</s>
<s>
Tu	ten	k3xDgFnSc4	ten
následně	následně	k6eAd1	následně
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
plně	plně	k6eAd1	plně
věnovat	věnovat	k5eAaImF	věnovat
Fleshgod	Fleshgod	k1gInSc4	Fleshgod
Apocalypse	Apocalypse	k1gFnSc2	Apocalypse
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2010	[number]	k4	2010
kapela	kapela	k1gFnSc1	kapela
podepsala	podepsat	k5eAaPmAgFnS	podepsat
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
Extreme	Extrem	k1gInSc5	Extrem
Management	management	k1gInSc1	management
Group	Group	k1gInSc4	Group
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnSc4	Inc
<g/>
.	.	kIx.	.
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
skládat	skládat	k5eAaImF	skládat
druhé	druhý	k4xOgNnSc4	druhý
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2011	[number]	k4	2011
skupina	skupina	k1gFnSc1	skupina
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Nuclear	Nuclear	k1gMnSc1	Nuclear
Blast	Blast	k1gMnSc1	Blast
Records	Records	k1gInSc1	Records
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
ke	k	k7c3	k
kapele	kapela	k1gFnSc3	kapela
oficiálně	oficiálně	k6eAd1	oficiálně
na	na	k7c4	na
plný	plný	k2eAgInSc4d1	plný
úvazek	úvazek	k1gInSc4	úvazek
přidal	přidat	k5eAaPmAgMnS	přidat
nový	nový	k2eAgMnSc1d1	nový
člen	člen	k1gMnSc1	člen
<g/>
,	,	kIx,	,
Francesco	Francesco	k6eAd1	Francesco
Ferrini	Ferrin	k1gMnPc1	Ferrin
<g/>
,	,	kIx,	,
pianista	pianista	k1gMnSc1	pianista
a	a	k8xC	a
tvůrce	tvůrce	k1gMnSc1	tvůrce
symfonických	symfonický	k2eAgFnPc2d1	symfonická
částí	část	k1gFnPc2	část
na	na	k7c6	na
albech	album	k1gNnPc6	album
Oracles	Oraclesa	k1gFnPc2	Oraclesa
a	a	k8xC	a
Mafia	Mafium	k1gNnSc2	Mafium
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
podepsala	podepsat	k5eAaPmAgFnS	podepsat
na	na	k7c6	na
pozdějším	pozdní	k2eAgInSc6d2	pozdější
zvuku	zvuk	k1gInSc6	zvuk
Fleshgod	Fleshgod	k1gInSc1	Fleshgod
Apocalypse	Apocalypse	k1gFnSc2	Apocalypse
<g/>
,	,	kIx,	,
i	i	k9	i
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
nadcházejícím	nadcházející	k2eAgNnSc6d1	nadcházející
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
druhé	druhý	k4xOgNnSc4	druhý
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
Agony	agon	k1gInPc1	agon
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2011	[number]	k4	2011
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2011	[number]	k4	2011
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Bonusová	bonusový	k2eAgFnSc1d1	bonusová
verze	verze	k1gFnSc1	verze
alba	album	k1gNnPc4	album
také	také	k9	také
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
coververzi	coververze	k1gFnSc4	coververze
"	"	kIx"	"
<g/>
Heartwork	Heartwork	k1gInSc4	Heartwork
<g/>
"	"	kIx"	"
od	od	k7c2	od
deathmetalové	deathmetal	k1gMnPc1	deathmetal
skupiny	skupina	k1gFnSc2	skupina
Carcass	Carcass	k1gInSc1	Carcass
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fleshgod	Fleshgod	k1gInSc4	Fleshgod
Apocalypse	Apocalypse	k1gFnSc2	Apocalypse
vydala	vydat	k5eAaPmAgFnS	vydat
hudební	hudební	k2eAgInSc4d1	hudební
klip	klip	k1gInSc4	klip
pro	pro	k7c4	pro
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Forsaking	Forsaking	k1gInSc1	Forsaking
<g/>
"	"	kIx"	"
z	z	k7c2	z
alba	album	k1gNnSc2	album
Agony	agon	k1gInPc4	agon
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třetí	třetí	k4xOgNnSc1	třetí
album	album	k1gNnSc1	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Labyrinth	Labyrinth	k1gInSc1	Labyrinth
vydávají	vydávat	k5eAaPmIp3nP	vydávat
Fleshgod	Fleshgod	k1gInSc4	Fleshgod
Apocalypse	Apocalypse	k1gFnSc2	Apocalypse
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
se	se	k3xPyFc4	se
podílelo	podílet	k5eAaImAgNnS	podílet
několik	několik	k4yIc1	několik
hostujících	hostující	k2eAgMnPc2d1	hostující
umělců	umělec	k1gMnPc2	umělec
včetně	včetně	k7c2	včetně
operní	operní	k2eAgFnSc2d1	operní
pěvkyně	pěvkyně	k1gFnSc2	pěvkyně
Veronicy	Veronica	k1gFnSc2	Veronica
Bordacchini	Bordacchin	k1gMnPc1	Bordacchin
<g/>
.	.	kIx.	.
</s>
<s>
Koncept	koncept	k1gInSc1	koncept
alba	album	k1gNnSc2	album
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
řecké	řecký	k2eAgFnSc6d1	řecká
báji	báj	k1gFnSc6	báj
o	o	k7c6	o
Labyrintu	labyrint	k1gInSc6	labyrint
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Knossos	Knossosa	k1gFnPc2	Knossosa
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc2	jeho
přirovnání	přirovnání	k1gNnSc2	přirovnání
k	k	k7c3	k
moderní	moderní	k2eAgFnSc3d1	moderní
době	doba	k1gFnSc3	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fleshgod	Fleshgoda	k1gFnPc2	Fleshgoda
Apocalypse	Apocalypse	k1gFnSc1	Apocalypse
vydala	vydat	k5eAaPmAgFnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
King	King	k1gInSc4	King
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2017	[number]	k4	2017
skupina	skupina	k1gFnSc1	skupina
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Tommaso	Tommasa	k1gFnSc5	Tommasa
Riccardi	Riccard	k1gMnPc1	Riccard
opustil	opustit	k5eAaPmAgMnS	opustit
skupinu	skupina	k1gFnSc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Bude	být	k5eAaImBp3nS	být
nahrazen	nahradit	k5eAaPmNgMnS	nahradit
bubeníkem	bubeník	k1gMnSc7	bubeník
Francescem	Francesce	k1gNnSc7	Francesce
Paoli	Paole	k1gFnSc4	Paole
na	na	k7c6	na
vokálech	vokál	k1gInPc6	vokál
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Fabio	Fabio	k1gNnSc1	Fabio
Bartoletti	Bartoletť	k1gFnSc2	Bartoletť
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Deceptionist	Deceptionist	k1gInSc1	Deceptionist
obsadí	obsadit	k5eAaPmIp3nS	obsadit
místo	místo	k1gNnSc4	místo
kytaristy	kytarista	k1gMnSc2	kytarista
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
okamžik	okamžik	k1gInSc4	okamžik
bude	být	k5eAaImBp3nS	být
novým	nový	k2eAgMnSc7d1	nový
bubeníkem	bubeník	k1gMnSc7	bubeník
David	David	k1gMnSc1	David
Folchitto	Folchitto	k1gNnSc4	Folchitto
ze	z	k7c2	z
Stormlord	Stormlorda	k1gFnPc2	Stormlorda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Páté	pátý	k4xOgNnSc1	pátý
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
Veleno	velen	k2eAgNnSc1d1	veleno
je	být	k5eAaImIp3nS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
na	na	k7c4	na
květen	květen	k1gInSc4	květen
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
Oracles	Oracles	k1gInSc1	Oracles
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Agony	agon	k1gInPc1	agon
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Labyrinth	Labyrinth	k1gInSc1	Labyrinth
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
King	King	k1gMnSc1	King
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Veleno	velen	k2eAgNnSc1d1	veleno
(	(	kIx(	(
<g/>
2019	[number]	k4	2019
<g/>
)	)	kIx)	)
<g/>
DemoPromo	DemoProma	k1gFnSc5	DemoProma
'	'	kIx"	'
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
EPMafia	EPMafium	k1gNnSc2	EPMafium
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Členové	člen	k1gMnPc1	člen
kapely	kapela	k1gFnSc2	kapela
==	==	k?	==
</s>
</p>
<p>
<s>
Cristiano	Cristiana	k1gFnSc5	Cristiana
Trionfera	Trionfera	k1gFnSc1	Trionfera
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Paolo	Paonout	k5eAaPmAgNnS	Paonout
Rossi	Rosse	k1gFnSc3	Rosse
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Francesco	Francesco	k6eAd1	Francesco
Paoli	Paole	k1gFnSc4	Paole
–	–	k?	–
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Francesco	Francesco	k6eAd1	Francesco
Ferrini	Ferrin	k1gMnPc1	Ferrin
–	–	k?	–
klavír	klavír	k1gInSc4	klavír
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Fleshgod	Fleshgoda	k1gFnPc2	Fleshgoda
Apocalypse	Apocalypse	k1gFnSc2	Apocalypse
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Facebook	Facebook	k1gInSc1	Facebook
</s>
</p>
<p>
<s>
Fanouškovská	fanouškovský	k2eAgFnSc1d1	fanouškovská
stránka	stránka	k1gFnSc1	stránka
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Myspace	Myspace	k1gFnSc1	Myspace
</s>
</p>
<p>
<s>
Twitter	Twitter	k1gMnSc1	Twitter
</s>
</p>
<p>
<s>
YouTube	YouTubat	k5eAaPmIp3nS	YouTubat
</s>
</p>
