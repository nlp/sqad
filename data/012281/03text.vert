<p>
<s>
Ben	Ben	k1gInSc1	Ben
Haggerty	Haggert	k1gInPc1	Haggert
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1983	[number]	k4	1983
Seattle	Seattle	k1gFnSc2	Seattle
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
Macklemore	Macklemor	k1gInSc5	Macklemor
či	či	k8xC	či
dříve	dříve	k6eAd2	dříve
Professor	Professor	k1gInSc1	Professor
Macklemore	Macklemor	k1gMnSc5	Macklemor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
rapper	rapper	k1gInSc1	rapper
působící	působící	k2eAgInSc1d1	působící
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
vlastním	vlastní	k2eAgInSc6d1	vlastní
nezávislém	závislý	k2eNgInSc6d1	nezávislý
labelu	label	k1gInSc6	label
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samostatně	samostatně	k6eAd1	samostatně
začal	začít	k5eAaPmAgInS	začít
vydávat	vydávat	k5eAaImF	vydávat
hudbu	hudba	k1gFnSc4	hudba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
The	The	k1gMnSc1	The
Heist	Heist	k1gMnSc1	Heist
vydal	vydat	k5eAaPmAgMnS	vydat
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
nejlépe	dobře	k6eAd3	dobře
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
US	US	kA	US
žebříčku	žebříček	k1gInSc6	žebříček
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
se	s	k7c7	s
78	[number]	k4	78
000	[number]	k4	000
prodanými	prodaný	k2eAgFnPc7d1	prodaná
kopiemi	kopie	k1gFnPc7	kopie
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
týdne	týden	k1gInSc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
producentem	producent	k1gMnSc7	producent
Ryanem	Ryan	k1gMnSc7	Ryan
Lewisem	Lewis	k1gInSc7	Lewis
<g/>
,	,	kIx,	,
houslistou	houslista	k1gMnSc7	houslista
Andrew	Andrew	k1gMnSc7	Andrew
Joslynem	Joslyn	k1gMnSc7	Joslyn
a	a	k8xC	a
trumpetistou	trumpetista	k1gMnSc7	trumpetista
Owuorem	Owuor	k1gInSc7	Owuor
Arungou	Arunga	k1gFnSc7	Arunga
<g/>
.	.	kIx.	.
</s>
<s>
Vydal	vydat	k5eAaPmAgMnS	vydat
jeden	jeden	k4xCgMnSc1	jeden
mixtape	mixtapat	k5eAaPmIp3nS	mixtapat
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc1	tři
EP	EP	kA	EP
a	a	k8xC	a
tři	tři	k4xCgNnPc4	tři
alba	album	k1gNnPc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
hudební	hudební	k2eAgInSc1d1	hudební
klip	klip	k1gInSc1	klip
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
"	"	kIx"	"
<g/>
Thrift	Thrift	k2eAgInSc1d1	Thrift
Shop	shop	k1gInSc1	shop
<g/>
"	"	kIx"	"
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
zhlédlo	zhlédnout	k5eAaPmAgNnS	zhlédnout
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1,2	[number]	k4	1,2
miliardy	miliarda	k4xCgFnSc2	miliarda
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
album	album	k1gNnSc4	album
The	The	k1gFnSc2	The
Heist	Heist	k1gFnSc1	Heist
a	a	k8xC	a
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Thrift	Thrift	k2eAgInSc1d1	Thrift
Shop	shop	k1gInSc1	shop
<g/>
"	"	kIx"	"
obdržel	obdržet	k5eAaPmAgMnS	obdržet
čtyři	čtyři	k4xCgFnPc4	čtyři
ceny	cena	k1gFnPc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
dalšími	další	k2eAgMnPc7d1	další
alby	alba	k1gFnPc1	alba
jsou	být	k5eAaImIp3nP	být
This	This	k1gInSc4	This
unruly	unrula	k1gFnSc2	unrula
mess	mess	k6eAd1	mess
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
ve	v	k7c6	v
made	made	k1gNnSc6	made
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejnovější	nový	k2eAgNnSc1d3	nejnovější
album	album	k1gNnSc1	album
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
Gemini	Gemin	k1gMnPc1	Gemin
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgNnSc6	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nepodílel	podílet	k5eNaImAgMnS	podílet
Ryan	Ryan	k1gMnSc1	Ryan
Lewis	Lewis	k1gFnSc2	Lewis
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
producentem	producent	k1gMnSc7	producent
je	být	k5eAaImIp3nS	být
Tyler	Tyler	k1gMnSc1	Tyler
Dopps	Dopps	k1gInSc1	Dopps
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Seattlu	Seattl	k1gInSc6	Seattl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
vyrostl	vyrůst	k5eAaPmAgInS	vyrůst
<g/>
.	.	kIx.	.
</s>
<s>
Nepochází	pocházet	k5eNaImIp3nS	pocházet
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
muzikantů	muzikant	k1gMnPc2	muzikant
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oba	dva	k4xCgMnPc1	dva
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
ho	on	k3xPp3gInSc4	on
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
však	však	k9	však
podporovali	podporovat	k5eAaImAgMnP	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
hip	hip	k0	hip
hopem	hopem	k?	hopem
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
setkal	setkat	k5eAaPmAgMnS	setkat
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
skupiny	skupina	k1gFnSc2	skupina
Digital	Digital	kA	Digital
Underground	underground	k1gInSc1	underground
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtrnácti	čtrnáct	k4xCc6	čtrnáct
letech	let	k1gInPc6	let
začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
texty	text	k1gInPc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
začal	začít	k5eAaPmAgMnS	začít
rappovat	rappovat	k5eAaImF	rappovat
<g/>
,	,	kIx,	,
hodně	hodně	k6eAd1	hodně
se	se	k3xPyFc4	se
zajímal	zajímat	k5eAaImAgInS	zajímat
o	o	k7c4	o
West	West	k2eAgInSc4d1	West
Coast	Coast	k1gInSc4	Coast
underground	underground	k1gInSc1	underground
hip	hip	k0	hip
hop	hop	k0	hop
<g/>
,	,	kIx,	,
Freestyle	Freestyl	k1gInSc5	Freestyl
Fellowship	Fellowship	k1gInSc1	Fellowship
<g/>
,	,	kIx,	,
Aceyalone	Aceyalon	k1gInSc5	Aceyalon
<g/>
,	,	kIx,	,
Living	Living	k1gInSc4	Living
Legends	Legendsa	k1gFnPc2	Legendsa
<g/>
,	,	kIx,	,
Wu-Tang	Wu-Tang	k1gMnSc1	Wu-Tang
Clan	Clan	k1gMnSc1	Clan
<g/>
,	,	kIx,	,
Nas	Nas	k1gMnSc1	Nas
a	a	k8xC	a
Talib	Talib	k1gMnSc1	Talib
Kweli	Kwel	k1gMnPc1	Kwel
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c4	na
Garfield	Garfield	k1gInSc4	Garfield
High	High	k1gInSc4	High
School	Schoola	k1gFnPc2	Schoola
a	a	k8xC	a
Nathan	Nathan	k1gMnSc1	Nathan
High	Higha	k1gFnPc2	Higha
School	Schoola	k1gFnPc2	Schoola
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
poté	poté	k6eAd1	poté
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
na	na	k7c4	na
The	The	k1gFnSc4	The
Evergreen	evergreen	k1gInSc1	evergreen
State	status	k1gInSc5	status
College	College	k1gNnPc6	College
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgInS	získat
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
titul	titul	k1gInSc1	titul
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zajímal	zajímat	k5eAaImAgMnS	zajímat
o	o	k7c4	o
způsoby	způsob	k1gInPc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
oslovit	oslovit	k5eAaPmF	oslovit
mladší	mladý	k2eAgFnSc4d2	mladší
generaci	generace	k1gFnSc4	generace
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
své	svůj	k3xOyFgFnSc2	svůj
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
účastnil	účastnit	k5eAaImAgInS	účastnit
se	se	k3xPyFc4	se
programu	program	k1gInSc2	program
Gateways	Gatewaysa	k1gFnPc2	Gatewaysa
for	forum	k1gNnPc2	forum
Incarcerated	Incarcerated	k1gMnSc1	Incarcerated
Youth	Youth	k1gMnSc1	Youth
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
zaměřoval	zaměřovat	k5eAaImAgMnS	zaměřovat
na	na	k7c4	na
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc4d1	kulturní
identitu	identita	k1gFnSc4	identita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tohoto	tento	k3xDgInSc2	tento
programu	program	k1gInSc2	program
zprostředkovával	zprostředkovávat	k5eAaImAgMnS	zprostředkovávat
hudební	hudební	k2eAgInPc4d1	hudební
workshopy	workshop	k1gInPc4	workshop
<g/>
.	.	kIx.	.
</s>
<s>
Momentálně	momentálně	k6eAd1	momentálně
žije	žít	k5eAaImIp3nS	žít
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
<g/>
,	,	kIx,	,
Triciaou	Triciaa	k1gFnSc7	Triciaa
Davisovou	Davisový	k2eAgFnSc7d1	Davisová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
vydal	vydat	k5eAaPmAgMnS	vydat
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
nezávislém	závislý	k2eNgInSc6d1	nezávislý
labelu	label	k1gInSc6	label
album	album	k1gNnSc4	album
The	The	k1gFnSc2	The
Heist	Heist	k1gFnSc1	Heist
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc2	který
celé	celá	k1gFnSc2	celá
produkoval	produkovat	k5eAaImAgMnS	produkovat
Ryan	Ryan	k1gMnSc1	Ryan
Lewis	Lewis	k1gFnSc2	Lewis
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
Oh	oh	k0	oh
My	my	k3xPp1nPc1	my
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vydaný	vydaný	k2eAgInSc1d1	vydaný
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2010	[number]	k4	2010
v	v	k7c6	v
hitparádě	hitparáda	k1gFnSc6	hitparáda
propadl	propadnout	k5eAaPmAgMnS	propadnout
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
jen	jen	k9	jen
na	na	k7c4	na
deluxe	deluxe	k1gFnPc4	deluxe
edici	edice	k1gFnSc4	edice
alba	album	k1gNnPc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Wing	Wing	k1gInSc1	Wing
<g/>
$	$	kIx~	$
<g/>
"	"	kIx"	"
vydaný	vydaný	k2eAgInSc1d1	vydaný
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2011	[number]	k4	2011
se	se	k3xPyFc4	se
přiblížil	přiblížit	k5eAaPmAgMnS	přiblížit
ke	k	k7c3	k
vstupu	vstup	k1gInSc3	vstup
do	do	k7c2	do
hitparády	hitparáda	k1gFnSc2	hitparáda
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
112	[number]	k4	112
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
třetí	třetí	k4xOgInSc1	třetí
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Can	Can	k1gFnSc1	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Hold	hold	k1gInSc1	hold
Us	Us	k1gFnSc1	Us
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
si	se	k3xPyFc3	se
všimli	všimnout	k5eAaPmAgMnP	všimnout
v	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
The	The	k1gMnSc2	The
Source	Source	k1gMnSc2	Source
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
poté	poté	k6eAd1	poté
Macklemora	Macklemora	k1gFnSc1	Macklemora
zpropagovali	zpropagovat	k5eAaBmAgMnP	zpropagovat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
hitparády	hitparáda	k1gFnSc2	hitparáda
se	se	k3xPyFc4	se
však	však	k9	však
dostal	dostat	k5eAaPmAgMnS	dostat
až	až	k6eAd1	až
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
95	[number]	k4	95
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
vyhoupl	vyhoupnout	k5eAaPmAgMnS	vyhoupnout
až	až	k9	až
na	na	k7c4	na
příčku	příčka	k1gFnSc4	příčka
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
Same	Sam	k1gMnSc5	Sam
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
předchozí	předchozí	k2eAgInSc4d1	předchozí
singl	singl	k1gInSc4	singl
i	i	k8xC	i
tento	tento	k3xDgMnSc1	tento
se	se	k3xPyFc4	se
do	do	k7c2	do
hitparády	hitparáda	k1gFnSc2	hitparáda
dostal	dostat	k5eAaPmAgMnS	dostat
až	až	k6eAd1	až
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2013	[number]	k4	2013
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
vyšplhal	vyšplhat	k5eAaPmAgMnS	vyšplhat
až	až	k9	až
na	na	k7c4	na
11	[number]	k4	11
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
úspěch	úspěch	k1gInSc1	úspěch
umožnil	umožnit	k5eAaPmAgInS	umožnit
až	až	k9	až
pátý	pátý	k4xOgInSc1	pátý
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Thrift	Thrift	k2eAgInSc1d1	Thrift
Shop	shop	k1gInSc1	shop
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vydaný	vydaný	k2eAgInSc1d1	vydaný
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
ohromný	ohromný	k2eAgInSc4d1	ohromný
úspěch	úspěch	k1gInSc4	úspěch
a	a	k8xC	a
vyhoupl	vyhoupnout	k5eAaPmAgMnS	vyhoupnout
se	se	k3xPyFc4	se
na	na	k7c4	na
první	první	k4xOgFnSc4	první
příčku	příčka	k1gFnSc4	příčka
(	(	kIx(	(
<g/>
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
umožnil	umožnit	k5eAaPmAgInS	umožnit
ono	onen	k3xDgNnSc4	onen
zlepšení	zlepšení	k1gNnSc4	zlepšení
předchozích	předchozí	k2eAgInPc2d1	předchozí
singlů	singl	k1gInPc2	singl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc2	tento
písně	píseň	k1gFnSc2	píseň
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
pět	pět	k4xCc4	pět
milionů	milion	k4xCgInPc2	milion
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2013	[number]	k4	2013
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
alba	alba	k1gFnSc1	alba
The	The	k1gFnSc1	The
Heist	Heist	k1gFnSc1	Heist
<g/>
,	,	kIx,	,
i	i	k9	i
díky	díky	k7c3	díky
úspěchu	úspěch	k1gInSc2	úspěch
pátého	pátý	k4xOgInSc2	pátý
singlu	singl	k1gInSc2	singl
"	"	kIx"	"
<g/>
Thrift	Thrift	k2eAgInSc1d1	Thrift
Shop	shop	k1gInSc1	shop
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
prodalo	prodat	k5eAaPmAgNnS	prodat
1	[number]	k4	1
378	[number]	k4	378
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
56	[number]	k4	56
<g/>
.	.	kIx.	.
předávání	předávání	k1gNnSc4	předávání
hudebních	hudební	k2eAgFnPc2d1	hudební
cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
,	,	kIx,	,
konaném	konaný	k2eAgInSc6d1	konaný
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2014	[number]	k4	2014
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Ryanem	Ryan	k1gMnSc7	Ryan
Lewisem	Lewis	k1gInSc7	Lewis
čtyři	čtyři	k4xCgNnPc4	čtyři
ocenění	ocenění	k1gNnPc4	ocenění
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
za	za	k7c4	za
nejlepší	dobrý	k2eAgMnPc4d3	nejlepší
nováčky	nováček	k1gMnPc4	nováček
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
rapové	rapový	k2eAgNnSc4d1	rapové
album	album	k1gNnSc4	album
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Heist	Heist	k1gMnSc1	Heist
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
rapový	rapový	k2eAgInSc4d1	rapový
počin	počin	k1gInSc4	počin
a	a	k8xC	a
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
rapovou	rapový	k2eAgFnSc4d1	rapová
píseň	píseň	k1gFnSc4	píseň
(	(	kIx(	(
<g/>
obě	dva	k4xCgFnPc4	dva
za	za	k7c7	za
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
Thrift	Thrift	k2eAgInSc4d1	Thrift
Shop	shop	k1gInSc4	shop
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.26	.26	k4	.26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2016	[number]	k4	2016
vydal	vydat	k5eAaPmAgMnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
druhé	druhý	k4xOgNnSc4	druhý
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
This	This	k1gInSc1	This
Unruly	Unrula	k1gFnSc2	Unrula
Mess	Messa	k1gFnPc2	Messa
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
ve	v	k7c6	v
Made	Made	k1gFnSc6	Made
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
EP	EP	kA	EP
===	===	k?	===
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
-	-	kIx~	-
Open	Open	k1gNnSc1	Open
Your	Youra	k1gFnPc2	Youra
Eyes	Eyesa	k1gFnPc2	Eyesa
</s>
</p>
<p>
<s>
===	===	k?	===
Singly	singl	k1gInPc4	singl
===	===	k?	===
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
–	–	k?	–
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
Oh	oh	k0	oh
My	my	k3xPp1nPc1	my
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
s	s	k7c7	s
Ryanem	Ryan	k1gMnSc7	Ryan
Lewisem	Lewis	k1gInSc7	Lewis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
–	–	k?	–
"	"	kIx"	"
<g/>
Wing	Wing	k1gMnSc1	Wing
<g/>
$	$	kIx~	$
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
s	s	k7c7	s
Ryanem	Ryan	k1gMnSc7	Ryan
Lewisem	Lewis	k1gInSc7	Lewis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
–	–	k?	–
"	"	kIx"	"
<g/>
Can	Can	k1gMnSc1	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Hold	hold	k1gInSc1	hold
Us	Us	k1gFnSc1	Us
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
s	s	k7c7	s
Ryanem	Ryan	k1gMnSc7	Ryan
Lewisem	Lewis	k1gInSc7	Lewis
<g/>
,	,	kIx,	,
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Ray	Ray	k?	Ray
Dalton	Dalton	k1gInSc1	Dalton
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
–	–	k?	–
"	"	kIx"	"
<g/>
Same	Sam	k1gMnSc5	Sam
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
s	s	k7c7	s
Ryanem	Ryan	k1gMnSc7	Ryan
Lewisem	Lewis	k1gInSc7	Lewis
<g/>
,	,	kIx,	,
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Mary	Mary	k1gFnSc1	Mary
Lambert	Lambert	k1gMnSc1	Lambert
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
–	–	k?	–
"	"	kIx"	"
<g/>
Thrift	Thrift	k2eAgInSc1d1	Thrift
Shop	shop	k1gInSc1	shop
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
s	s	k7c7	s
Ryanem	Ryan	k1gMnSc7	Ryan
Lewisem	Lewis	k1gInSc7	Lewis
<g/>
,	,	kIx,	,
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Wanz	Wanz	k1gInSc1	Wanz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
–	–	k?	–
"	"	kIx"	"
<g/>
White	Whit	k1gInSc5	Whit
Walls	Walls	k1gInSc4	Walls
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
s	s	k7c7	s
Ryanem	Ryan	k1gMnSc7	Ryan
Lewisem	Lewis	k1gInSc7	Lewis
<g/>
,	,	kIx,	,
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Schoolboy	Schoolboa	k1gFnPc1	Schoolboa
Q	Q	kA	Q
a	a	k8xC	a
Hollis	Hollis	k1gFnSc1	Hollis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
–	–	k?	–
"	"	kIx"	"
<g/>
Downtown	Downtown	k1gMnSc1	Downtown
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
s	s	k7c7	s
Ryanem	Ryan	k1gMnSc7	Ryan
Lewisem	Lewis	k1gInSc7	Lewis
<g/>
,	,	kIx,	,
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Melle	Melle	k1gFnSc1	Melle
Mel	mlít	k5eAaImRp2nS	mlít
<g/>
,	,	kIx,	,
Kool	Kool	k1gMnSc1	Kool
Moe	Moe	k1gMnSc1	Moe
Dee	Dee	k1gMnSc1	Dee
<g/>
,	,	kIx,	,
Grandmaster	Grandmaster	k1gMnSc1	Grandmaster
Caz	Caz	k1gFnSc2	Caz
&	&	k?	&
Eric	Eric	k1gFnSc1	Eric
Nally	Nalla	k1gFnSc2	Nalla
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
–	–	k?	–
"	"	kIx"	"
<g/>
Glorious	Glorious	k1gMnSc1	Glorious
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
se	se	k3xPyFc4	se
Skylar	Skylar	k1gInSc1	Skylar
Grey	Grea	k1gFnSc2	Grea
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2018	[number]	k4	2018
–	–	k?	–
"	"	kIx"	"
<g/>
Corner	Corner	k1gMnSc1	Corner
Store	Stor	k1gInSc5	Stor
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Dave	Dav	k1gInSc5	Dav
B	B	kA	B
&	&	k?	&
Travis	Travis	k1gInSc1	Travis
Thompson	Thompson	k1gMnSc1	Thompson
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2018	[number]	k4	2018
–	–	k?	–
"	"	kIx"	"
<g/>
How	How	k1gMnSc2	How
To	ten	k3xDgNnSc1	ten
Play	play	k0	play
The	The	k1gMnSc5	The
Flute	Flut	k1gMnSc5	Flut
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
King	King	k1gInSc1	King
Draino	Draino	k1gNnSc1	Draino
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Macklemore	Macklemor	k1gInSc5	Macklemor
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Macklemore	Macklemor	k1gInSc5	Macklemor
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
