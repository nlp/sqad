<s>
Rubidium	rubidium	k1gNnSc1
</s>
<s>
Rubidium	rubidium	k1gNnSc1
</s>
<s>
[	[	kIx(
<g/>
Kr	Kr	k1gMnSc6
<g/>
]	]	kIx)
5	#num#	k4
<g/>
s	s	k7c7
<g/>
1	#num#	k4
</s>
<s>
85	#num#	k4
</s>
<s>
Rb	Rb	kA
</s>
<s>
37	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Rubidium	rubidium	k1gNnSc1
<g/>
,	,	kIx,
Rb	Rb	k1gFnSc1
<g/>
,	,	kIx,
37	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Rubidium	rubidium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
s	s	k7c7
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Koncentrace	koncentrace	k1gFnPc1
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
</s>
<s>
90	#num#	k4
až	až	k6eAd1
310	#num#	k4
ppm	ppm	k?
</s>
<s>
Koncentrace	koncentrace	k1gFnPc1
v	v	k7c6
mořské	mořský	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
</s>
<s>
0,12	0,12	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
l	l	kA
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
Měkký	měkký	k2eAgMnSc1d1
<g/>
,	,	kIx,
lehký	lehký	k2eAgInSc1d1
a	a	k8xC
stříbrolesklý	stříbrolesklý	k2eAgInSc1d1
kov	kov	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
lze	lze	k6eAd1
krájet	krájet	k5eAaImF
nožem	nůž	k1gInSc7
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7440-17-7	7440-17-7	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
85,467	85,467	k4
<g/>
8	#num#	k4
</s>
<s>
Atomový	atomový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
248	#num#	k4
pm	pm	k?
</s>
<s>
Kovalentní	kovalentní	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
220	#num#	k4
pm	pm	k?
</s>
<s>
Van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Waalsův	Waalsův	k2eAgInSc5d1
poloměr	poloměr	k1gInSc4
</s>
<s>
303	#num#	k4
pm	pm	k?
</s>
<s>
Iontový	iontový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
148	#num#	k4
pm	pm	k?
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
Kr	Kr	k1gMnSc6
<g/>
]	]	kIx)
5	#num#	k4
<g/>
s	s	k7c7
<g/>
1	#num#	k4
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
I	i	k9
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
0,82	0,82	k4
</s>
<s>
Ionizační	ionizační	k2eAgFnSc1d1
energie	energie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
</s>
<s>
403	#num#	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
</s>
<s>
2632,1	2632,1	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
</s>
<s>
3859,4	3859,4	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Látkové	látkový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Krystalografická	krystalografický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
Krychlová	krychlový	k2eAgFnSc1d1
<g/>
,	,	kIx,
prostorově	prostorově	k6eAd1
centrovaná	centrovaný	k2eAgFnSc1d1
</s>
<s>
Molární	molární	k2eAgInSc1d1
objem	objem	k1gInSc1
</s>
<s>
55,76	55,76	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
6	#num#	k4
m	m	kA
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
mol	mol	k1gInSc1
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
1,532	1,532	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
Pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Tvrdost	tvrdost	k1gFnSc1
</s>
<s>
0,3	0,3	k4
</s>
<s>
Tlak	tlak	k1gInSc1
syté	sytý	k2eAgFnSc2d1
páry	pára	k1gFnSc2
</s>
<s>
100	#num#	k4
Pa	Pa	kA
při	při	k7c6
552K	552K	k4
</s>
<s>
Rychlost	rychlost	k1gFnSc1
zvuku	zvuk	k1gInSc2
</s>
<s>
1300	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
</s>
<s>
Termické	termický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Tepelná	tepelný	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
58,2	58,2	k4
W	W	kA
<g/>
⋅	⋅	k?
<g/>
m	m	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
⋅	⋅	k?
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
39,31	39,31	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
312,46	312,46	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
687,85	687,85	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
961	#num#	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Skupenské	skupenský	k2eAgNnSc1d1
teplo	teplo	k1gNnSc1
tání	tání	k1gNnSc2
</s>
<s>
2,19	2,19	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Skupenské	skupenský	k2eAgNnSc1d1
teplo	teplo	k1gNnSc1
varu	var	k1gInSc2
</s>
<s>
75,77	75,77	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Měrná	měrný	k2eAgFnSc1d1
tepelná	tepelný	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
</s>
<s>
31,060	31,060	k4
Jmol	Jmol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
7,79	7,79	k4
<g/>
×	×	k?
<g/>
106	#num#	k4
S	s	k7c7
<g/>
/	/	kIx~
<g/>
m	m	kA
</s>
<s>
Měrný	měrný	k2eAgInSc4d1
elektrický	elektrický	k2eAgInSc4d1
odpor	odpor	k1gInSc4
</s>
<s>
128	#num#	k4
nΩ	nΩ	k?
<g/>
·	·	k?
<g/>
m	m	kA
</s>
<s>
Standardní	standardní	k2eAgInSc1d1
elektrodový	elektrodový	k2eAgInSc1d1
potenciál	potenciál	k1gInSc1
</s>
<s>
−	−	k?
V	v	k7c6
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1
chování	chování	k1gNnSc1
</s>
<s>
Paramagnetický	paramagnetický	k2eAgInSc1d1
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Vysoce	vysoce	k6eAd1
hořlavý	hořlavý	k2eAgMnSc1d1
(	(	kIx(
<g/>
F	F	kA
<g/>
)	)	kIx)
<g/>
Žíravý	žíravý	k2eAgMnSc1d1
(	(	kIx(
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
R-věty	R-věta	k1gFnPc1
</s>
<s>
R	R	kA
<g/>
14	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
,	,	kIx,
R34	R34	k1gFnSc1
</s>
<s>
S-věty	S-věta	k1gFnPc1
</s>
<s>
S	s	k7c7
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
,	,	kIx,
S	s	k7c7
<g/>
20	#num#	k4
<g/>
,	,	kIx,
S	s	k7c7
<g/>
26	#num#	k4
<g/>
,	,	kIx,
S	s	k7c7
<g/>
30	#num#	k4
<g/>
,	,	kIx,
S	s	k7c7
<g/>
33	#num#	k4
<g/>
,	,	kIx,
S	s	k7c7
<g/>
36	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
<g/>
/	/	kIx~
<g/>
39	#num#	k4
<g/>
,	,	kIx,
S	s	k7c7
<g/>
43	#num#	k4
<g/>
,	,	kIx,
S45	S45	k1gFnSc1
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
83	#num#	k4
<g/>
Rb	Rb	k1gFnSc1
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
86,2	86,2	k4
dne	den	k1gInSc2
</s>
<s>
ε	ε	k?
</s>
<s>
-	-	kIx~
</s>
<s>
83	#num#	k4
<g/>
Kr	Kr	k1gFnSc1
</s>
<s>
γ	γ	k?
</s>
<s>
0,52	0,52	k4
</s>
<s>
-	-	kIx~
</s>
<s>
84	#num#	k4
<g/>
Rb	Rb	k1gFnSc1
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
32,9	32,9	k4
dne	den	k1gInSc2
</s>
<s>
ε	ε	k?
</s>
<s>
-	-	kIx~
</s>
<s>
84	#num#	k4
<g/>
Kr	Kr	k1gFnSc1
</s>
<s>
β	β	k?
<g/>
+	+	kIx~
</s>
<s>
1,66	1,66	k4
</s>
<s>
84	#num#	k4
<g/>
Kr	Kr	k1gFnSc1
</s>
<s>
γ	γ	k?
</s>
<s>
0,881	0,881	k4
</s>
<s>
-	-	kIx~
</s>
<s>
β	β	k?
<g/>
−	−	k?
</s>
<s>
0,892	0,892	k4
</s>
<s>
84	#num#	k4
<g/>
Sr	Sr	k1gFnSc1
</s>
<s>
85	#num#	k4
<g/>
Rb	Rb	k1gFnSc1
</s>
<s>
72,168	72,168	k4
</s>
<s>
je	být	k5eAaImIp3nS
stabilní	stabilní	k2eAgMnSc1d1
s	s	k7c7
48	#num#	k4
neutrony	neutron	k1gInPc7
</s>
<s>
86	#num#	k4
<g/>
Rb	Rb	k1gFnSc1
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
18,65	18,65	k4
dne	den	k1gInSc2
</s>
<s>
β	β	k?
<g/>
−	−	k?
</s>
<s>
1,775	1,775	k4
</s>
<s>
86	#num#	k4
<g/>
Sr	Sr	k1gFnSc1
</s>
<s>
γ	γ	k?
</s>
<s>
1,076	1,076	k4
<g/>
7	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
87	#num#	k4
<g/>
Rb	Rb	k1gFnSc1
</s>
<s>
27,835	27,835	k4
</s>
<s>
4,88	4,88	k4
<g/>
×	×	k?
<g/>
1010	#num#	k4
roku	rok	k1gInSc2
</s>
<s>
β	β	k?
<g/>
−	−	k?
</s>
<s>
0.283	0.283	k4
</s>
<s>
87	#num#	k4
<g/>
Sr	Sr	k1gFnSc1
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
<g/>
⋏	⋏	k?
</s>
<s>
Rb	Rb	k?
<g/>
≻	≻	k?
Stroncium	stroncium	k1gNnSc4
</s>
<s>
⋎	⋎	k?
<g/>
Cs	Cs	k1gFnSc1
</s>
<s>
Rubidium	rubidium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Rb	Rb	k1gFnSc2
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Rubidium	rubidium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
prvkem	prvek	k1gInSc7
z	z	k7c2
řady	řada	k1gFnSc2
alkalických	alkalický	k2eAgInPc2d1
kovů	kov	k1gInPc2
<g/>
,	,	kIx,
vyznačuje	vyznačovat	k5eAaImIp3nS
se	se	k3xPyFc4
velkou	velký	k2eAgFnSc7d1
reaktivitou	reaktivita	k1gFnSc7
a	a	k8xC
mimořádně	mimořádně	k6eAd1
nízkým	nízký	k2eAgInSc7d1
redoxním	redoxní	k2eAgInSc7d1
potenciálem	potenciál	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnPc1d1
fyzikálně-chemické	fyzikálně-chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Kovové	kovový	k2eAgNnSc1d1
rubidium	rubidium	k1gNnSc1
</s>
<s>
Rubidium	rubidium	k1gNnSc1
je	být	k5eAaImIp3nS
měkký	měkký	k2eAgInSc1d1
<g/>
,	,	kIx,
lehký	lehký	k2eAgInSc1d1
a	a	k8xC
stříbrolesklý	stříbrolesklý	k2eAgInSc1d1
kov	kov	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
lze	lze	k6eAd1
krájet	krájet	k5eAaImF
nožem	nůž	k1gInSc7
<g/>
,	,	kIx,
asi	asi	k9
jako	jako	k9
vosk	vosk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
předchozích	předchozí	k2eAgInPc2d1
alkalických	alkalický	k2eAgInPc2d1
kovů	kov	k1gInPc2
má	mít	k5eAaImIp3nS
větší	veliký	k2eAgFnSc4d2
hustotu	hustota	k1gFnSc4
než	než	k8xS
voda	voda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
vede	vést	k5eAaImIp3nS
elektrický	elektrický	k2eAgInSc4d1
proud	proud	k1gInSc4
a	a	k8xC
teplo	teplo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
ostatními	ostatní	k2eAgInPc7d1
kovy	kov	k1gInPc7
má	mít	k5eAaImIp3nS
nízkou	nízký	k2eAgFnSc4d1
teplotu	teplota	k1gFnSc4
tání	tání	k1gNnSc2
a	a	k8xC
varu	var	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jeho	jeho	k3xOp3gFnPc6
parách	para	k1gFnPc6
se	se	k3xPyFc4
kromě	kromě	k7c2
jednoatomových	jednoatomový	k2eAgFnPc2d1
částic	částice	k1gFnPc2
vyskytují	vyskytovat	k5eAaImIp3nP
i	i	k9
dvouatomové	dvouatomový	k2eAgFnPc1d1
molekuly	molekula	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Páry	pára	k1gFnPc1
mají	mít	k5eAaImIp3nP
zelenomodrou	zelenomodrý	k2eAgFnSc4d1
až	až	k9
zelenou	zelený	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elementární	elementární	k2eAgFnSc1d1
kovové	kovový	k2eAgNnSc4d1
rubidium	rubidium	k1gNnSc4
lze	lze	k6eAd1
dlouhodobě	dlouhodobě	k6eAd1
uchovávat	uchovávat	k5eAaImF
pod	pod	k7c7
vrstvou	vrstva	k1gFnSc7
alifatických	alifatický	k2eAgInPc2d1
uhlovodíků	uhlovodík	k1gInPc2
jako	jako	k8xS,k8xC
petrolej	petrolej	k1gInSc1
nebo	nebo	k8xC
nafta	nafta	k1gFnSc1
<g/>
,	,	kIx,
s	s	k7c7
kterými	který	k3yIgFnPc7,k3yQgFnPc7,k3yRgFnPc7
nereaguje	reagovat	k5eNaBmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Rubidium	rubidium	k1gNnSc1
mimořádně	mimořádně	k6eAd1
rychle	rychle	k6eAd1
až	až	k9
explozivně	explozivně	k6eAd1
reaguje	reagovat	k5eAaBmIp3nS
s	s	k7c7
kyslíkem	kyslík	k1gInSc7
na	na	k7c4
superoxid	superoxid	k1gInSc4
rubidný	rubidný	k2eAgInSc4d1
a	a	k8xC
s	s	k7c7
vodou	voda	k1gFnSc7
na	na	k7c4
hydroxid	hydroxid	k1gInSc4
rubidný	rubidný	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reakce	reakce	k1gFnSc1
rubidia	rubidium	k1gNnSc2
s	s	k7c7
vodou	voda	k1gFnSc7
je	být	k5eAaImIp3nS
natolik	natolik	k6eAd1
exotermní	exotermní	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
unikající	unikající	k2eAgInSc1d1
vodík	vodík	k1gInSc1
reakčním	reakční	k2eAgNnSc7d1
teplem	teplo	k1gNnSc7
samovolně	samovolně	k6eAd1
explozivně	explozivně	k6eAd1
vzplane	vzplanout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
přírodě	příroda	k1gFnSc6
se	se	k3xPyFc4
proto	proto	k8xC
vyskytuje	vyskytovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
ve	v	k7c6
sloučenách	sloučena	k1gFnPc6
a	a	k8xC
jenom	jenom	k9
v	v	k7c6
oxidačním	oxidační	k2eAgInSc6d1
stupni	stupeň	k1gInSc6
Rb	Rb	k1gFnSc2
<g/>
+	+	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rubidium	rubidium	k1gNnSc1
se	se	k3xPyFc4
také	také	k9
za	za	k7c2
mírného	mírný	k2eAgNnSc2d1
zahřátí	zahřátí	k1gNnSc2
slučuje	slučovat	k5eAaImIp3nS
s	s	k7c7
vodíkem	vodík	k1gInSc7
na	na	k7c4
hydrid	hydrid	k1gInSc4
rubidný	rubidný	k2eAgMnSc1d1
RbH	RbH	k1gMnSc1
<g/>
,	,	kIx,
s	s	k7c7
dusíkem	dusík	k1gInSc7
na	na	k7c4
nitrid	nitrid	k1gInSc4
rubidný	rubidný	k2eAgInSc4d1
Rb	Rb	k1gFnSc7
<g/>
3	#num#	k4
<g/>
N	N	kA
nebo	nebo	k8xC
azid	azid	k1gInSc4
rubidný	rubidný	k2eAgInSc4d1
RbN	RbN	k1gFnSc7
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepřímo	přímo	k6eNd1
se	se	k3xPyFc4
také	také	k9
slučuje	slučovat	k5eAaImIp3nS
s	s	k7c7
uhlíkem	uhlík	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soli	sůl	k1gFnSc2
rubidia	rubidium	k1gNnSc2
barví	barvit	k5eAaImIp3nS
plamen	plamen	k1gInSc1
světle	světle	k6eAd1
fialově	fialově	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Historický	historický	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
</s>
<s>
Rubidium	rubidium	k1gNnSc1
bylo	být	k5eAaImAgNnS
objeveno	objevit	k5eAaPmNgNnS
roku	rok	k1gInSc2
1861	#num#	k4
německým	německý	k2eAgMnSc7d1
chemikem	chemik	k1gMnSc7
Robertem	Robert	k1gMnSc7
W.	W.	kA
Bunsenem	Bunsen	k1gMnSc7
a	a	k8xC
německým	německý	k2eAgMnSc7d1
fyzikem	fyzik	k1gMnSc7
Gustavem	Gustav	k1gMnSc7
R.	R.	kA
Kirchhoffem	Kirchhoff	k1gInSc7
za	za	k7c4
použití	použití	k1gNnSc4
jimi	on	k3xPp3gMnPc7
objevené	objevený	k2eAgFnPc1d1
spektrální	spektrální	k2eAgFnPc1d1
analýzy	analýza	k1gFnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
rubidium	rubidium	k1gNnSc4
našli	najít	k5eAaPmAgMnP
v	v	k7c6
dürkheimských	dürkheimský	k2eAgFnPc6d1
minerálních	minerální	k2eAgFnPc6d1
vodách	voda	k1gFnPc6
spolu	spolu	k6eAd1
s	s	k7c7
cesiem	cesium	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rubidium	rubidium	k1gNnSc1
bylo	být	k5eAaImAgNnS
pojmenováno	pojmenovat	k5eAaPmNgNnS
podle	podle	k7c2
svých	svůj	k3xOyFgFnPc2
dvou	dva	k4xCgFnPc2
červených	červený	k2eAgFnPc2d1
čar	čára	k1gFnPc2
ve	v	k7c6
spektru	spektrum	k1gNnSc6
jako	jako	k8xS,k8xC
tmavočervený	tmavočervený	k2eAgInSc1d1
–	–	k?
rubidus	rubidus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kovové	kovový	k2eAgNnSc1d1
rubidium	rubidium	k1gNnSc1
bylo	být	k5eAaImAgNnS
poprvé	poprvé	k6eAd1
získáno	získat	k5eAaPmNgNnS
jeho	jeho	k3xOp3gMnSc7
objevitelem	objevitel	k1gMnSc7
Robertem	Robert	k1gMnSc7
W.	W.	kA
Bunsenem	Bunsen	k1gMnSc7
elektrolýzou	elektrolýza	k1gFnSc7
roztaveného	roztavený	k2eAgInSc2d1
chloridu	chlorid	k1gInSc2
rubidného	rubidný	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
v	v	k7c6
přírodě	příroda	k1gFnSc6
</s>
<s>
Díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
velké	velký	k2eAgFnSc3d1
reaktivitě	reaktivita	k1gFnSc3
se	se	k3xPyFc4
v	v	k7c6
přírodě	příroda	k1gFnSc6
setkáváme	setkávat	k5eAaImIp1nP
pouze	pouze	k6eAd1
se	s	k7c7
sloučeninami	sloučenina	k1gFnPc7
rubidia	rubidium	k1gNnSc2
a	a	k8xC
to	ten	k3xDgNnSc4
pouze	pouze	k6eAd1
v	v	k7c6
mocenství	mocenství	k1gNnSc6
Rb	Rb	k1gFnSc2
<g/>
+	+	kIx~
<g/>
.	.	kIx.
</s>
<s>
Obsah	obsah	k1gInSc1
rubidia	rubidium	k1gNnSc2
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
vysoký	vysoký	k2eAgInSc1d1
<g/>
,	,	kIx,
předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
zemská	zemský	k2eAgFnSc1d1
kůra	kůra	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
100	#num#	k4
<g/>
–	–	k?
<g/>
300	#num#	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
kg	kg	kA
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
odpovídá	odpovídat	k5eAaImIp3nS
78	#num#	k4
ppm	ppm	k?
(	(	kIx(
<g/>
parts	partsa	k1gFnPc2
per	pero	k1gNnPc2
milion	milion	k4xCgInSc4
=	=	kIx~
počet	počet	k1gInSc4
částic	částice	k1gFnPc2
látky	látka	k1gFnSc2
na	na	k7c4
1	#num#	k4
milion	milion	k4xCgInSc4
všech	všecek	k3xTgFnPc2
částic	částice	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
ve	v	k7c6
výskytu	výskyt	k1gInSc6
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
na	na	k7c4
stejnou	stejný	k2eAgFnSc4d1
úroveň	úroveň	k1gFnSc4
jako	jako	k8xC,k8xS
nikl	nikl	k1gInSc4
<g/>
,	,	kIx,
měď	měď	k1gFnSc4
nebo	nebo	k8xC
zinek	zinek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrný	průměrný	k2eAgInSc1d1
obsah	obsah	k1gInSc1
v	v	k7c6
mořské	mořský	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
je	být	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
0,12	0,12	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
l.	l.	k?
Ve	v	k7c6
vesmíru	vesmír	k1gInSc6
se	se	k3xPyFc4
předpokládá	předpokládat	k5eAaImIp3nS
výskyt	výskyt	k1gInSc1
1	#num#	k4
atomu	atom	k1gInSc2
rubidia	rubidium	k1gNnSc2
na	na	k7c4
přibližně	přibližně	k6eAd1
6	#num#	k4
miliard	miliarda	k4xCgFnPc2
atomů	atom	k1gInPc2
vodíku	vodík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
minerálech	minerál	k1gInPc6
provází	provázet	k5eAaImIp3nS
rubidium	rubidium	k1gNnSc1
obvykle	obvykle	k6eAd1
ostatní	ostatní	k2eAgInPc1d1
alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patrně	patrně	k6eAd1
nejvýznamnější	významný	k2eAgInSc1d3
výskyt	výskyt	k1gInSc1
je	být	k5eAaImIp3nS
uváděn	uvádět	k5eAaImNgInS
v	v	k7c6
minerálu	minerál	k1gInSc6
lepidolitu	lepidolit	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
značně	značně	k6eAd1
komplikovaný	komplikovaný	k2eAgInSc4d1
hlinito-křemičitan	hlinito-křemičitan	k1gInSc4
lithno-draselný	lithno-draselný	k2eAgInSc4d1
KLi	KLi	k1gFnSc7
<g/>
2	#num#	k4
<g/>
[	[	kIx(
<g/>
AlSi	AlS	k1gInSc3
<g/>
3	#num#	k4
<g/>
O	o	k7c4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
,	,	kIx,
<g/>
F	F	kA
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
minerálu	minerál	k1gInSc6
se	se	k3xPyFc4
obsah	obsah	k1gInSc1
rubidia	rubidium	k1gNnSc2
pohybuje	pohybovat	k5eAaImIp3nS
kolem	kolem	k7c2
hodnoty	hodnota	k1gFnSc2
1,5	1,5	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
malých	malý	k2eAgNnPc6d1
množstvích	množství	k1gNnPc6
(	(	kIx(
<g/>
asi	asi	k9
okolo	okolo	k7c2
0,015	0,015	k4
%	%	kIx~
<g/>
)	)	kIx)
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
karnalitu	karnalit	k1gInSc6
KCl	KCl	k1gFnSc2
<g/>
·	·	k?
<g/>
MgCl	MgCl	k1gInSc1
<g/>
2	#num#	k4
<g/>
·	·	k?
<g/>
6	#num#	k4
<g/>
H	H	kA
<g/>
2	#num#	k4
<g/>
O.	O.	kA
</s>
<s>
Výroba	výroba	k1gFnSc1
</s>
<s>
Elementární	elementární	k2eAgNnSc1d1
rubidium	rubidium	k1gNnSc1
se	se	k3xPyFc4
průmyslově	průmyslově	k6eAd1
vyrábí	vyrábět	k5eAaImIp3nS
elektrolýzou	elektrolýza	k1gFnSc7
roztavené	roztavený	k2eAgFnSc2d1
směsi	směs	k1gFnSc2
60	#num#	k4
%	%	kIx~
chloridu	chlorid	k1gInSc6
vápenatého	vápenatý	k2eAgNnSc2d1
a	a	k8xC
40	#num#	k4
%	%	kIx~
chloridu	chlorid	k1gInSc2
rubidného	rubidný	k2eAgInSc2d1
při	při	k7c6
teplotě	teplota	k1gFnSc6
750	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Vápník	vápník	k1gInSc4
vzniklý	vzniklý	k2eAgInSc1d1
elektrolýzou	elektrolýza	k1gFnSc7
ve	v	k7c6
sběrné	sběrný	k2eAgFnSc6d1
nádobě	nádoba	k1gFnSc6
tuhne	tuhnout	k5eAaImIp3nS
<g/>
,	,	kIx,
protože	protože	k8xS
jeho	jeho	k3xOp3gFnSc1
teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
je	být	k5eAaImIp3nS
vyšší	vysoký	k2eAgFnSc1d2
než	než	k8xS
rubidia	rubidium	k1gNnPc4
a	a	k8xC
tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
od	od	k7c2
rubidia	rubidium	k1gNnSc2
odděluje	oddělovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elektrolýza	elektrolýza	k1gFnSc1
probíhá	probíhat	k5eAaImIp3nS
na	na	k7c6
železné	železný	k2eAgFnSc6d1
katodě	katoda	k1gFnSc6
a	a	k8xC
grafitové	grafitový	k2eAgFnSc3d1
anodě	anoda	k1gFnSc3
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yIgMnPc4,k3yQgMnPc4,k3yRgMnPc4
vzniká	vznikat	k5eAaImIp3nS
plynný	plynný	k2eAgInSc1d1
chlor	chlor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
způsob	způsob	k1gInSc4
pro	pro	k7c4
tento	tento	k3xDgInSc4
kov	kov	k1gInSc4
však	však	k9
není	být	k5eNaImIp3nS
úplně	úplně	k6eAd1
nejlepší	dobrý	k2eAgMnSc1d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
vyrábí	vyrábět	k5eAaImIp3nS
okolo	okolo	k7c2
5	#num#	k4
tun	tuna	k1gFnPc2
rubidia	rubidium	k1gNnSc2
ročně	ročně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Železná	železný	k2eAgFnSc1d1
katoda	katoda	k1gFnSc1
2	#num#	k4
Rb	Rb	k1gFnPc2
<g/>
+	+	kIx~
+	+	kIx~
2	#num#	k4
e	e	k0
<g/>
−	−	k?
→	→	k?
2	#num#	k4
Rb	Rb	k1gFnSc1
</s>
<s>
Grafitová	grafitový	k2eAgFnSc1d1
anoda	anoda	k1gFnSc1
2	#num#	k4
Cl	Cl	k1gMnSc1
<g/>
−	−	k?
→	→	k?
Cl	Cl	k1gFnSc7
<g/>
2	#num#	k4
+	+	kIx~
2	#num#	k4
e	e	k0
<g/>
−	−	k?
</s>
<s>
Lepší	dobrý	k2eAgFnSc1d2
je	být	k5eAaImIp3nS
příprava	příprava	k1gFnSc1
chemickou	chemický	k2eAgFnSc4d1
cestou	cesta	k1gFnSc7
<g/>
,	,	kIx,
zahříváním	zahřívání	k1gNnSc7
hydroxidu	hydroxid	k1gInSc2
rubidného	rubidný	k2eAgInSc2d1
nebo	nebo	k8xC
oxidu	oxid	k1gInSc2
rubidného	rubidný	k2eAgInSc2d1
s	s	k7c7
kovovým	kovový	k2eAgInSc7d1
hořčíkem	hořčík	k1gInSc7
v	v	k7c6
proudu	proud	k1gInSc6
vodíku	vodík	k1gInSc2
nebo	nebo	k8xC
s	s	k7c7
kovovým	kovový	k2eAgInSc7d1
vápníkem	vápník	k1gInSc7
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedno	jeden	k4xCgNnSc1
z	z	k7c2
nejlepších	dobrý	k2eAgNnPc2d3
redukovadel	redukovadlo	k1gNnPc2
je	být	k5eAaImIp3nS
zirkonium	zirkonium	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Malé	Malé	k2eAgNnSc4d1
množství	množství	k1gNnSc4
rubidia	rubidium	k1gNnSc2
lze	lze	k6eAd1
připravit	připravit	k5eAaPmF
zahříváním	zahřívání	k1gNnSc7
chloridu	chlorid	k1gInSc2
rubidného	rubidný	k2eAgInSc2d1
s	s	k7c7
azidem	azid	k1gInSc7
barnatým	barnatý	k2eAgInSc7d1
za	za	k7c2
vysokého	vysoký	k2eAgInSc2d1
tlaku	tlak	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Baryum	baryum	k1gNnSc1
vzniklé	vzniklý	k2eAgFnSc2d1
rozkladem	rozklad	k1gInSc7
azidu	azid	k1gInSc2
vytěsňuje	vytěsňovat	k5eAaImIp3nS
z	z	k7c2
chloridu	chlorid	k1gInSc2
rubidného	rubidný	k2eAgInSc2d1
rubidium	rubidium	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
v	v	k7c6
podobě	podoba	k1gFnSc6
svých	svůj	k3xOyFgFnPc2
par	para	k1gFnPc2
kondenzuje	kondenzovat	k5eAaImIp3nS
na	na	k7c6
chladnějších	chladný	k2eAgFnPc6d2
stěnách	stěna	k1gFnPc6
nádoby	nádoba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Vzhledem	vzhledem	k7c3
ke	k	k7c3
své	svůj	k3xOyFgFnSc3
mimořádné	mimořádný	k2eAgFnSc3d1
nestálosti	nestálost	k1gFnSc3
a	a	k8xC
reaktivitě	reaktivita	k1gFnSc3
má	mít	k5eAaImIp3nS
kovové	kovový	k2eAgNnSc4d1
rubidium	rubidium	k1gNnSc4
jen	jen	k6eAd1
minimální	minimální	k2eAgNnSc4d1
praktické	praktický	k2eAgNnSc4d1
využití	využití	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1
nízký	nízký	k2eAgInSc1d1
ionizační	ionizační	k2eAgInSc1d1
potenciál	potenciál	k1gInSc1
dává	dávat	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
uplatnění	uplatnění	k1gNnSc2
ve	v	k7c6
fotočláncích	fotočlánek	k1gInPc6
sloužících	sloužící	k2eAgInPc6d1
pro	pro	k7c4
přímou	přímý	k2eAgFnSc4d1
přeměnu	přeměna	k1gFnSc4
světelné	světelný	k2eAgFnSc2d1
energie	energie	k1gFnSc2
v	v	k7c4
elektrickou	elektrický	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
je	být	k5eAaImIp3nS
proto	proto	k8xC
perspektivním	perspektivní	k2eAgNnSc7d1
médiem	médium	k1gNnSc7
pro	pro	k7c4
iontové	iontový	k2eAgInPc4d1
motory	motor	k1gInPc4
jako	jako	k8xS,k8xC
pohonné	pohonný	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
kosmických	kosmický	k2eAgNnPc2d1
plavidel	plavidlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
výrobě	výroba	k1gFnSc6
katodových	katodový	k2eAgFnPc2d1
trubic	trubice	k1gFnPc2
<g/>
,	,	kIx,
pracujících	pracující	k2eAgInPc2d1
s	s	k7c7
nízkotlakou	nízkotlaký	k2eAgFnSc7d1
náplní	náplň	k1gFnSc7
inertního	inertní	k2eAgInSc2d1
plynu	plyn	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
užívá	užívat	k5eAaImIp3nS
rubidia	rubidium	k1gNnSc2
jako	jako	k8xS,k8xC
getru	getr	k1gInSc2
<g/>
,	,	kIx,
tj.	tj.	kA
látky	látka	k1gFnSc2
sloužící	sloužící	k1gFnSc2
k	k	k7c3
zachycení	zachycení	k1gNnSc3
a	a	k8xC
odstranění	odstranění	k1gNnSc3
posledních	poslední	k2eAgInPc2d1
zbytků	zbytek	k1gInPc2
přimíšených	přimíšený	k2eAgInPc2d1
reaktivních	reaktivní	k2eAgInPc2d1
plynů	plyn	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Soli	sůl	k1gFnPc1
rubidia	rubidium	k1gNnSc2
se	se	k3xPyFc4
přidávají	přidávat	k5eAaImIp3nP
do	do	k7c2
směsí	směs	k1gFnPc2
zábavní	zábavní	k2eAgFnSc2d1
pyrotechniky	pyrotechnika	k1gFnSc2
a	a	k8xC
barví	barvit	k5eAaImIp3nP
vzniklé	vzniklý	k2eAgInPc1d1
světelné	světelný	k2eAgInPc1d1
efekty	efekt	k1gInPc1
do	do	k7c2
fialova	fialovo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Izotop	izotop	k1gInSc1
87	#num#	k4
<g/>
Rb	Rb	k1gFnPc2
s	s	k7c7
přirozeným	přirozený	k2eAgInSc7d1
výskytem	výskyt	k1gInSc7
27,8	27,8	k4
%	%	kIx~
je	být	k5eAaImIp3nS
mírně	mírně	k6eAd1
radioaktivní	radioaktivní	k2eAgInSc1d1
<g/>
,	,	kIx,
rozpadá	rozpadat	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
s	s	k7c7
poločasem	poločas	k1gInSc7
4,92	4,92	k4
<g/>
×	×	k?
<g/>
1010	#num#	k4
roku	rok	k1gInSc2
za	za	k7c2
vzniku	vznik	k1gInSc2
izotopu	izotop	k1gInSc2
87	#num#	k4
<g/>
Sr	Sr	k1gFnPc2
a	a	k8xC
uvolnění	uvolnění	k1gNnSc4
β	β	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toho	ten	k3xDgMnSc4
se	se	k3xPyFc4
v	v	k7c6
geologii	geologie	k1gFnSc6
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
k	k	k7c3
datování	datování	k1gNnSc3
stáří	stáří	k1gNnSc2
hornin	hornina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc1
</s>
<s>
Anorganické	anorganický	k2eAgFnPc1d1
sloučeniny	sloučenina	k1gFnPc1
</s>
<s>
Hydrid	hydrid	k1gInSc1
rubidný	rubidný	k2eAgInSc1d1
RbH	RbH	k1gFnSc4
je	být	k5eAaImIp3nS
bílá	bílý	k2eAgFnSc1d1
krystalická	krystalický	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
lze	lze	k6eAd1
využít	využít	k5eAaPmF
jako	jako	k9
velmi	velmi	k6eAd1
silné	silný	k2eAgNnSc4d1
redukční	redukční	k2eAgNnSc4d1
činidlo	činidlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vzduchu	vzduch	k1gInSc6
je	být	k5eAaImIp3nS
nestálý	stálý	k2eNgInSc1d1
<g/>
,	,	kIx,
reaguje	reagovat	k5eAaBmIp3nS
s	s	k7c7
kyslíkem	kyslík	k1gInSc7
i	i	k8xC
se	s	k7c7
vzdušnou	vzdušný	k2eAgFnSc7d1
vlhkostí	vlhkost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připravuje	připravovat	k5eAaImIp3nS
se	se	k3xPyFc4
reakcí	reakce	k1gFnSc7
mírně	mírně	k6eAd1
zahřátého	zahřátý	k2eAgNnSc2d1
kovového	kovový	k2eAgNnSc2d1
rubidia	rubidium	k1gNnSc2
ve	v	k7c6
vodíkové	vodíkový	k2eAgFnSc6d1
atmosféře	atmosféra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Superoxid	superoxid	k1gInSc4
rubidný	rubidný	k2eAgInSc4d1
neboli	neboli	k8xC
hyperoxid	hyperoxid	k1gInSc4
rubidný	rubidný	k2eAgInSc4d1
RbO	RbO	k1gFnSc7
<g/>
2	#num#	k4
je	být	k5eAaImIp3nS
tmavěhnědý	tmavěhnědý	k2eAgInSc1d1
prášek	prášek	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
vlhkém	vlhký	k2eAgInSc6d1
vzduchu	vzduch	k1gInSc6
nestabilní	stabilní	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
ho	on	k3xPp3gMnSc4
využít	využít	k5eAaPmF
jako	jako	k9
silného	silný	k2eAgNnSc2d1
oxidačního	oxidační	k2eAgNnSc2d1
činidla	činidlo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
jemnou	jemný	k2eAgFnSc7d1
redukcí	redukce	k1gFnPc2
odštěpí	odštěpit	k5eAaPmIp3nS
jeden	jeden	k4xCgInSc4
kyslík	kyslík	k1gInSc4
a	a	k8xC
přejde	přejít	k5eAaPmIp3nS
v	v	k7c4
peroxid	peroxid	k1gInSc4
rubidný	rubidný	k2eAgInSc1d1
a	a	k8xC
silnější	silný	k2eAgFnSc1d2
redukcí	redukce	k1gFnSc7
odštěpí	odštěpit	k5eAaPmIp3nS
dva	dva	k4xCgInPc4
kyslíky	kyslík	k1gInPc4
a	a	k8xC
přejde	přejít	k5eAaPmIp3nS
v	v	k7c4
oxid	oxid	k1gInSc4
rubidný	rubidný	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reakcí	reakce	k1gFnSc7
rubidia	rubidium	k1gNnSc2
s	s	k7c7
kyslíkem	kyslík	k1gInSc7
vzniká	vznikat	k5eAaImIp3nS
superoxid	superoxid	k1gInSc1
rubidný	rubidný	k2eAgInSc1d1
–	–	k?
vzniká	vznikat	k5eAaImIp3nS
hořením	hoření	k1gNnSc7
rubidia	rubidium	k1gNnSc2
na	na	k7c6
vzduchu	vzduch	k1gInSc6
nebo	nebo	k8xC
i	i	k9
za	za	k7c4
pokojové	pokojový	k2eAgFnPc4d1
teploty	teplota	k1gFnPc4
při	při	k7c6
jeho	jeho	k3xOp3gFnSc6
samovolné	samovolný	k2eAgFnSc6d1
oxidaci	oxidace	k1gFnSc6
vzdušným	vzdušný	k2eAgInSc7d1
kyslíkem	kyslík	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Rb	Rb	k?
+	+	kIx~
O2	O2	k1gMnSc1
→	→	k?
RbO	RbO	k1gMnSc1
<g/>
2	#num#	k4
</s>
<s>
Hydroxid	hydroxid	k1gInSc1
rubidný	rubidný	k2eAgInSc1d1
RbOH	RbOH	k1gFnSc4
je	být	k5eAaImIp3nS
bílá	bílý	k2eAgFnSc1d1
krystalická	krystalický	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
analogických	analogický	k2eAgFnPc2d1
sloučenin	sloučenina	k1gFnPc2
sodíku	sodík	k1gInSc2
a	a	k8xC
draslíku	draslík	k1gInSc2
málo	málo	k6eAd1
hygroskopická	hygroskopický	k2eAgFnSc1d1
a	a	k8xC
je	být	k5eAaImIp3nS
jen	jen	k9
velmi	velmi	k6eAd1
omezeně	omezeně	k6eAd1
rozpustná	rozpustný	k2eAgFnSc1d1
ve	v	k7c6
vodě	voda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
velmi	velmi	k6eAd1
silná	silný	k2eAgFnSc1d1
zásada	zásada	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
má	mít	k5eAaImIp3nS
velmi	velmi	k6eAd1
silné	silný	k2eAgInPc4d1
žíravé	žíravý	k2eAgInPc4d1
účinky	účinek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připravuje	připravovat	k5eAaImIp3nS
se	se	k3xPyFc4
reakcí	reakce	k1gFnSc7
rubidia	rubidium	k1gNnSc2
<g/>
,	,	kIx,
oxidu	oxid	k1gInSc2
rubidného	rubidný	k2eAgInSc2d1
<g/>
,	,	kIx,
peroxidu	peroxid	k1gInSc2
rubidného	rubidný	k2eAgInSc2d1
nebo	nebo	k8xC
superoxidu	superoxid	k1gInSc2
rubidného	rubidný	k2eAgMnSc4d1
s	s	k7c7
vodou	voda	k1gFnSc7
nebo	nebo	k8xC
elektrolýzou	elektrolýza	k1gFnSc7
roztoku	roztok	k1gInSc2
chloridu	chlorid	k1gInSc2
rubidného	rubidný	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s>
Soli	sůl	k1gFnPc1
</s>
<s>
Rubidné	rubidný	k2eAgFnPc1d1
soli	sůl	k1gFnPc1
jsou	být	k5eAaImIp3nP
ve	v	k7c6
vodě	voda	k1gFnSc6
obecně	obecně	k6eAd1
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
rozpustné	rozpustný	k2eAgNnSc1d1
a	a	k8xC
jen	jen	k9
několik	několik	k4yIc1
je	být	k5eAaImIp3nS
nerozpustných	rozpustný	k2eNgMnPc6d1
<g/>
,	,	kIx,
všechny	všechen	k3xTgMnPc4
mají	mít	k5eAaImIp3nP
bílou	bílý	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
není	být	k5eNaImIp3nS
anion	anion	k1gInSc1
soli	sůl	k1gFnSc2
barevný	barevný	k2eAgMnSc1d1
(	(	kIx(
<g/>
manganistany	manganistan	k1gInPc1
<g/>
,	,	kIx,
chromany	chroman	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rubidné	rubidný	k2eAgFnPc4d1
soli	sůl	k1gFnPc4
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
snadno	snadno	k6eAd1
podvojné	podvojný	k2eAgFnPc4d1
soli	sůl	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
velmi	velmi	k6eAd1
nesnadno	snadno	k6eNd1
komplexy	komplex	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
před	před	k7c7
50	#num#	k4
lety	let	k1gInPc7
<g/>
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
nebyly	být	k5eNaImAgInP
známy	znám	k2eAgInPc1d1
žádné	žádný	k3yNgInPc1
komplexy	komplex	k1gInPc1
alkalických	alkalický	k2eAgInPc2d1
kovů	kov	k1gInPc2
<g/>
,	,	kIx,
o	o	k7c6
kterých	který	k3yQgFnPc6,k3yRgFnPc6,k3yIgFnPc6
se	se	k3xPyFc4
předpokládalo	předpokládat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
nejsou	být	k5eNaImIp3nP
vůbec	vůbec	k9
schopny	schopen	k2eAgFnPc1d1
tvořit	tvořit	k5eAaImF
komplexy	komplex	k1gInPc4
(	(	kIx(
<g/>
podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
se	se	k3xPyFc4
předpokládalo	předpokládat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
nejsou	být	k5eNaImIp3nP
schopny	schopen	k2eAgInPc1d1
tvořit	tvořit	k5eAaImF
sloučeniny	sloučenina	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Chlorid	chlorid	k1gInSc1
rubidný	rubidný	k2eAgInSc4d1
RbCl	RbCl	k1gInSc4
je	být	k5eAaImIp3nS
bílá	bílý	k2eAgFnSc1d1
krystalická	krystalický	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chlorid	chlorid	k1gInSc1
rubidný	rubidný	k2eAgInSc1d1
i	i	k8xC
ostatní	ostatní	k2eAgFnPc1d1
rubidné	rubidný	k2eAgFnPc1d1
halogenidy	halogenida	k1gFnPc1
mají	mít	k5eAaImIp3nP
silný	silný	k2eAgInSc4d1
sklon	sklon	k1gInSc4
k	k	k7c3
tvorbě	tvorba	k1gFnSc3
polyhalogenidů	polyhalogenid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chlorid	chlorid	k1gInSc1
rubidný	rubidný	k2eAgInSc1d1
se	se	k3xPyFc4
vyrábí	vyrábět	k5eAaImIp3nS
reakcí	reakce	k1gFnSc7
kyseliny	kyselina	k1gFnSc2
chlorovodíkové	chlorovodíkový	k2eAgFnPc4d1
s	s	k7c7
uhličitanem	uhličitan	k1gInSc7
rubidným	rubidný	k2eAgInSc7d1
nebo	nebo	k8xC
hydroxidem	hydroxid	k1gInSc7
rubidným	rubidný	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k2eAgInPc1d1
halogenidy	halogenid	k1gInPc1
až	až	k9
na	na	k7c4
jodid	jodid	k1gInSc4
rubidný	rubidný	k2eAgInSc4d1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
někdy	někdy	k6eAd1
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
někdy	někdy	k6eAd1
v	v	k7c6
lékařství	lékařství	k1gNnSc6
místo	místo	k7c2
více	hodně	k6eAd2
škodlivého	škodlivý	k2eAgInSc2d1
jodidu	jodid	k1gInSc2
draselného	draselný	k2eAgInSc2d1
<g/>
,	,	kIx,
nemají	mít	k5eNaImIp3nP
praktické	praktický	k2eAgNnSc4d1
využití	využití	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Dusičnan	dusičnan	k1gInSc4
rubidný	rubidný	k2eAgInSc4d1
RbNO	RbNO	k1gFnSc7
<g/>
3	#num#	k4
je	být	k5eAaImIp3nS
bílá	bílý	k2eAgFnSc1d1
krystalická	krystalický	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
svými	svůj	k3xOyFgFnPc7
vlastnostmi	vlastnost	k1gFnPc7
velmi	velmi	k6eAd1
podobá	podobat	k5eAaImIp3nS
dusičnanu	dusičnan	k1gInSc3
draselnému	draselný	k2eAgMnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyrábí	vyrábět	k5eAaImIp3nS
se	se	k3xPyFc4
reakcí	reakce	k1gFnSc7
kyseliny	kyselina	k1gFnPc1
dusičné	dusičný	k2eAgFnPc1d1
s	s	k7c7
hydroxidem	hydroxid	k1gInSc7
nebo	nebo	k8xC
uhličitanem	uhličitan	k1gInSc7
rubidným	rubidný	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s>
Uhličitan	uhličitan	k1gInSc4
rubidný	rubidný	k2eAgInSc4d1
Rb	Rb	k1gFnSc7
<g/>
2	#num#	k4
<g/>
CO	co	k9
<g/>
3	#num#	k4
je	být	k5eAaImIp3nS
bílá	bílý	k2eAgFnSc1d1
krystalická	krystalický	k2eAgFnSc1d1
<g/>
,	,	kIx,
silně	silně	k6eAd1
hygroskopická	hygroskopický	k2eAgFnSc1d1
<g/>
,	,	kIx,
látka	látka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snadno	snadno	k6eAd1
se	se	k3xPyFc4
rozpouští	rozpouštět	k5eAaImIp3nS
ve	v	k7c6
vodě	voda	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
ne	ne	k9
v	v	k7c6
ethanolu	ethanol	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejlépe	dobře	k6eAd3
se	se	k3xPyFc4
připravuje	připravovat	k5eAaImIp3nS
reakcí	reakce	k1gFnSc7
síranu	síran	k1gInSc2
rubidného	rubidný	k2eAgInSc2d1
s	s	k7c7
hydroxidem	hydroxid	k1gInSc7
barnatým	barnatý	k2eAgInSc7d1
a	a	k8xC
následným	následný	k2eAgNnSc7d1
odpařením	odpaření	k1gNnSc7
s	s	k7c7
uhličitanem	uhličitan	k1gInSc7
amonným	amonný	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dá	dát	k5eAaPmIp3nS
se	se	k3xPyFc4
také	také	k9
připravit	připravit	k5eAaPmF
reakcí	reakce	k1gFnSc7
hydroxidu	hydroxid	k1gInSc2
rubidného	rubidný	k2eAgInSc2d1
se	se	k3xPyFc4
vzdušným	vzdušný	k2eAgInSc7d1
oxidem	oxid	k1gInSc7
uhličitým	uhličitý	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s>
Síran	síran	k1gInSc4
rubidný	rubidný	k2eAgInSc4d1
Rb	Rb	k1gFnSc7
<g/>
2	#num#	k4
<g/>
SO	So	kA
<g/>
4	#num#	k4
je	být	k5eAaImIp3nS
bílá	bílý	k2eAgFnSc1d1
krystalická	krystalický	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
svými	svůj	k3xOyFgFnPc7
vlastnostmi	vlastnost	k1gFnPc7
podobá	podobat	k5eAaImIp3nS
síranu	síran	k1gInSc2
draselnému	draselný	k2eAgMnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
snadno	snadno	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
podvojné	podvojný	k2eAgFnPc1d1
sloučeniny	sloučenina	k1gFnPc1
<g/>
,	,	kIx,
popřípadě	popřípadě	k6eAd1
smíšené	smíšený	k2eAgFnPc4d1
soli	sůl	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připravuje	připravovat	k5eAaImIp3nS
se	se	k3xPyFc4
reakcí	reakce	k1gFnSc7
uhličitanu	uhličitan	k1gInSc2
rubidného	rubidný	k2eAgInSc2d1
nebo	nebo	k8xC
hydroxidu	hydroxid	k1gInSc2
rubidného	rubidný	k2eAgMnSc4d1
s	s	k7c7
kyselinou	kyselina	k1gFnSc7
sírovou	sírový	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s>
Organické	organický	k2eAgFnPc1d1
sloučeniny	sloučenina	k1gFnPc1
</s>
<s>
Mezi	mezi	k7c4
organické	organický	k2eAgFnPc4d1
sloučeniny	sloučenina	k1gFnPc4
rubidia	rubidium	k1gNnSc2
patří	patřit	k5eAaImIp3nP
zejména	zejména	k9
rubidné	rubidný	k2eAgFnPc1d1
soli	sůl	k1gFnPc1
organických	organický	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
a	a	k8xC
rubidné	rubidný	k2eAgInPc4d1
alkoholáty	alkoholát	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dalším	další	k2eAgFnPc3d1
rubidným	rubidný	k2eAgFnPc3d1
sloučeninám	sloučenina	k1gFnPc3
patří	patřit	k5eAaImIp3nS
organické	organický	k2eAgInPc4d1
komplexy	komplex	k1gInPc4
rubidných	rubidný	k2eAgFnPc2d1
sloučenin	sloučenina	k1gFnPc2
tzv.	tzv.	kA
crowny	crowna	k1gFnSc2
a	a	k8xC
kryptandy	kryptanda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zcela	zcela	k6eAd1
zvláštní	zvláštní	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
organických	organický	k2eAgFnPc2d1
rubidných	rubidný	k2eAgFnPc2d1
sloučenin	sloučenina	k1gFnPc2
tvoří	tvořit	k5eAaImIp3nP
organokovové	organokovový	k2eAgFnPc1d1
sloučeniny	sloučenina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
<g/>
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
<g/>
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
Holzbecher	Holzbechra	k1gFnPc2
Z.	Z.	kA
<g/>
:	:	kIx,
<g/>
Analytická	analytický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
SNTL	SNTL	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1974	#num#	k4
</s>
<s>
Jursík	Jursík	k1gMnSc1
F.	F.	kA
<g/>
:	:	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
nekovů	nekov	k1gInPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7080-504-8	80-7080-504-8	k4
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Heinrich	Heinrich	k1gMnSc1
Remy	remy	k1gNnSc2
<g/>
,	,	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc4
1961	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
rubidium	rubidium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
rubidium	rubidium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4178576-9	4178576-9	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
5747	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85115684	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85115684	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
