<s>
Československá	československý	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
názvu	název	k1gInSc6
a	a	k8xC
atributech	atribut	k1gInPc6
československého	československý	k2eAgInSc2d1
státu	stát	k1gInSc2
v	v	k7c6
letech	let	k1gInPc6
1960	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
všeobecných	všeobecný	k2eAgFnPc6d1
dějinách	dějiny	k1gFnPc6
tohoto	tento	k3xDgInSc2
útvaru	útvar	k1gInSc2
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Komunistický	komunistický	k2eAgInSc1d1
režim	režim	k1gInSc1
v	v	k7c6
Československu	Československo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Československá	československý	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
←	←	k?
</s>
<s>
1960	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
</s>
<s>
→	→	k?
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Hymna	hymna	k1gFnSc1
<g/>
:	:	kIx,
Kde	kde	k6eAd1
domov	domov	k1gInSc4
můj	můj	k3xOp1gInSc4
a	a	k8xC
Nad	nad	k7c7
Tatrou	Tatra	k1gFnSc7
sa	sa	k?
blýska	blýska	k1gFnSc1
</s>
<s>
Motto	motto	k1gNnSc1
<g/>
:	:	kIx,
Pravda	pravda	k9
vítězí	vítězit	k5eAaImIp3nS
/	/	kIx~
Pravda	pravda	k9
víťazí	víťazit	k5eAaPmIp3nS
</s>
<s>
geografie	geografie	k1gFnSc1
</s>
<s>
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Praha	Praha	k1gFnSc1
</s>
<s>
rozloha	rozloha	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
127	#num#	k4
900	#num#	k4
km²	km²	k?
</s>
<s>
nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Gerlachovský	Gerlachovský	k2eAgInSc1d1
štít	štít	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
655	#num#	k4
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
:	:	kIx,
</s>
<s>
13	#num#	k4
741	#num#	k4
529	#num#	k4
(	(	kIx(
<g/>
podle	podle	k7c2
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
z	z	k7c2
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1961	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dne	den	k1gInSc2
28	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1977	#num#	k4
dosáhl	dosáhnout	k5eAaPmAgInS
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
15	#num#	k4
miliónů	milión	k4xCgInPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
národnostní	národnostní	k2eAgNnSc1d1
složení	složení	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Češi	Čech	k1gMnPc1
<g/>
,	,	kIx,
Slováci	Slovák	k1gMnPc1
<g/>
,	,	kIx,
Maďaři	Maďar	k1gMnPc1
(	(	kIx(
<g/>
národnostní	národnostní	k2eAgFnSc1d1
menšina	menšina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
jazyky	jazyk	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
čeština	čeština	k1gFnSc1
<g/>
,	,	kIx,
slovenština	slovenština	k1gFnSc1
<g/>
,	,	kIx,
regionálně	regionálně	k6eAd1
maďarština	maďarština	k1gFnSc1
</s>
<s>
náboženství	náboženství	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
žádné	žádný	k3yNgNnSc1
státní	státní	k2eAgNnSc1d1
náboženství	náboženství	k1gNnSc1
<g/>
,	,	kIx,
státem	stát	k1gInSc7
podporovaný	podporovaný	k2eAgInSc1d1
ateismus	ateismus	k1gInSc1
</s>
<s>
státní	státní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
</s>
<s>
státní	státní	k2eAgNnSc1d1
zřízení	zřízení	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
–	–	k?
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
<g/>
federativní	federativní	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
měna	měna	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
koruna	koruna	k1gFnSc1
československá	československý	k2eAgFnSc1d1
</s>
<s>
vznik	vznik	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
1960	#num#	k4
(	(	kIx(
<g/>
Ústava	ústava	k1gFnSc1
Československé	československý	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
zánik	zánik	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
1990	#num#	k4
(	(	kIx(
<g/>
přejmenována	přejmenovat	k5eAaPmNgFnS
na	na	k7c4
Československá	československý	k2eAgFnSc1d1
federativní	federativní	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
státní	státní	k2eAgInPc4d1
útvary	útvar	k1gInPc4
a	a	k8xC
území	území	k1gNnSc4
</s>
<s>
předcházející	předcházející	k2eAgFnSc1d1
<g/>
:	:	kIx,
</s>
<s>
Československá	československý	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
1948	#num#	k4
<g/>
–	–	k?
<g/>
1960	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
následující	následující	k2eAgInPc4d1
<g/>
:	:	kIx,
</s>
<s>
Československá	československý	k2eAgFnSc1d1
federativní	federativní	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Československá	československý	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
ČSSR	ČSSR	kA
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
oficiální	oficiální	k2eAgInSc1d1
název	název	k1gInSc1
Československa	Československo	k1gNnSc2
od	od	k7c2
11	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1960	#num#	k4
do	do	k7c2
29	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
11	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1960	#num#	k4
byla	být	k5eAaImAgFnS
přijata	přijmout	k5eAaPmNgFnS
Ústava	ústava	k1gFnSc1
Československé	československý	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
nahrazující	nahrazující	k2eAgFnSc1d1
Ústavu	ústav	k1gInSc2
9	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1948	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc4
státu	stát	k1gInSc2
změnila	změnit	k5eAaPmAgFnS
z	z	k7c2
dosavadního	dosavadní	k2eAgInSc2d1
Československá	československý	k2eAgFnSc1d1
republika	republika	k1gFnSc1
na	na	k7c4
Československá	československý	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
název	název	k1gInSc4
již	již	k6eAd1
vydržel	vydržet	k5eAaPmAgMnS
po	po	k7c4
zbylé	zbylý	k2eAgNnSc4d1
trvání	trvání	k1gNnSc4
komunistického	komunistický	k2eAgInSc2d1
režimu	režim	k1gInSc2
v	v	k7c6
Československu	Československo	k1gNnSc6
a	a	k8xC
ještě	ještě	k6eAd1
několik	několik	k4yIc4
měsíců	měsíc	k1gInPc2
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
režim	režim	k1gInSc1
padl	padnout	k5eAaImAgInS,k5eAaPmAgInS
následkem	následkem	k7c2
sametové	sametový	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
zahájené	zahájený	k2eAgFnSc2d1
17	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
porevolučního	porevoluční	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
byl	být	k5eAaImAgInS
název	název	k1gInSc1
státu	stát	k1gInSc2
změněn	změnit	k5eAaPmNgInS
ústavním	ústavní	k2eAgInSc7d1
zákonem	zákon	k1gInSc7
Federálního	federální	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
ze	z	k7c2
dne	den	k1gInSc2
29	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1990	#num#	k4
„	„	k?
<g/>
O	o	k7c6
změně	změna	k1gFnSc6
názvu	název	k1gInSc2
Československé	československý	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
“	“	k?
na	na	k7c6
Československá	československý	k2eAgFnSc1d1
federativní	federativní	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ze	z	k7c2
státoprávního	státoprávní	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
prodělala	prodělat	k5eAaPmAgFnS
ČSSR	ČSSR	kA
během	běh	k1gInSc7
svého	svůj	k3xOyFgNnSc2
trvání	trvání	k1gNnSc2
hlubokou	hluboký	k2eAgFnSc4d1
změnu	změna	k1gFnSc4
v	v	k7c6
důsledku	důsledek	k1gInSc6
Pražského	pražský	k2eAgNnSc2d1
jara	jaro	k1gNnSc2
roku	rok	k1gInSc2
1968	#num#	k4
<g/>
:	:	kIx,
v	v	k7c6
říjnu	říjen	k1gInSc6
toho	ten	k3xDgInSc2
roku	rok	k1gInSc2
Ústavní	ústavní	k2eAgInSc1d1
zákon	zákon	k1gInSc1
o	o	k7c6
československé	československý	k2eAgFnSc6d1
federaci	federace	k1gFnSc6
stát	stát	k5eAaImF,k5eAaPmF
s	s	k7c7
účinností	účinnost	k1gFnSc7
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc3
1969	#num#	k4
změnil	změnit	k5eAaPmAgInS
na	na	k7c6
federací	federace	k1gFnPc2
skládající	skládající	k2eAgFnSc2d1
se	se	k3xPyFc4
z	z	k7c2
České	český	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
Slovenské	slovenský	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Federalizace	federalizace	k1gFnSc1
se	se	k3xPyFc4
tehdy	tehdy	k6eAd1
ovšem	ovšem	k9
do	do	k7c2
názvu	název	k1gInSc2
společného	společný	k2eAgInSc2d1
státu	stát	k1gInSc2
nepromítla	promítnout	k5eNaPmAgFnS
–	–	k?
to	ten	k3xDgNnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
až	až	k9
roku	rok	k1gInSc2
1990	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
již	již	k9
federace	federace	k1gFnSc1
začala	začít	k5eAaPmAgFnS
směřovat	směřovat	k5eAaImF
ke	k	k7c3
svému	svůj	k3xOyFgInSc3
zániku	zánik	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
hlediska	hledisko	k1gNnSc2
periodizace	periodizace	k1gFnSc2
dějin	dějiny	k1gFnPc2
Československa	Československo	k1gNnSc2
nejsou	být	k5eNaImIp3nP
vložení	vložení	k1gNnSc4
slova	slovo	k1gNnSc2
„	„	k?
<g/>
socialistický	socialistický	k2eAgInSc1d1
<g/>
“	“	k?
do	do	k7c2
názvu	název	k1gInSc2
státu	stát	k1gInSc2
ani	ani	k8xC
jeho	jeho	k3xOp3gNnSc1
odstranění	odstranění	k1gNnSc1
o	o	k7c4
necelých	celý	k2eNgNnPc2d1
30	#num#	k4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
příliš	příliš	k6eAd1
významnými	významný	k2eAgInPc7d1
zlomy	zlom	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obou	dva	k4xCgInPc6
případech	případ	k1gInPc6
šlo	jít	k5eAaImAgNnS
spíše	spíše	k9
o	o	k7c4
dodatečné	dodatečný	k2eAgNnSc4d1
stvrzení	stvrzení	k1gNnSc4
předchozího	předchozí	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
<g/>
,	,	kIx,
takže	takže	k8xS
do	do	k7c2
názvu	název	k1gInSc2
státu	stát	k1gInSc2
se	se	k3xPyFc4
promítla	promítnout	k5eAaPmAgFnS
politická	politický	k2eAgFnSc1d1
změna	změna	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
nastala	nastat	k5eAaPmAgFnS
již	již	k6eAd1
předtím	předtím	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Socialismus	socialismus	k1gInSc1
v	v	k7c6
názvu	název	k1gInSc6
republiky	republika	k1gFnSc2
upomínal	upomínat	k5eAaImAgInS
na	na	k7c4
konsolidaci	konsolidace	k1gFnSc4
komunistického	komunistický	k2eAgInSc2d1
režimu	režim	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
proběhla	proběhnout	k5eAaPmAgFnS
na	na	k7c6
přelomu	přelom	k1gInSc6
40	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
tedy	tedy	k9
v	v	k7c6
letech	let	k1gInPc6
po	po	k7c6
„	„	k?
<g/>
Vítězném	vítězný	k2eAgNnSc6d1
únoru	únor	k1gInSc6
<g/>
“	“	k?
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
převrat	převrat	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1948	#num#	k4
nazývali	nazývat	k5eAaImAgMnP
sami	sám	k3xTgMnPc1
komunisté	komunista	k1gMnPc1
<g/>
;	;	kIx,
a	a	k8xC
odstranění	odstranění	k1gNnSc1
tohoto	tento	k3xDgNnSc2
slova	slovo	k1gNnSc2
bylo	být	k5eAaImAgNnS
terminologickou	terminologický	k2eAgFnSc7d1
tečkou	tečka	k1gFnSc7
za	za	k7c7
sametovou	sametový	k2eAgFnSc7d1
revolucí	revoluce	k1gFnSc7
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
tento	tento	k3xDgInSc4
režim	režim	k1gInSc4
svrhla	svrhnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Politický	politický	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Státní	státní	k2eAgInPc1d1
orgány	orgán	k1gInPc1
</s>
<s>
Československá	československý	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
byla	být	k5eAaImAgFnS
socialistický	socialistický	k2eAgInSc4d1
stát	stát	k1gInSc4
s	s	k7c7
vládou	vláda	k1gFnSc7
jedné	jeden	k4xCgFnSc2
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Československa	Československo	k1gNnSc2
měla	mít	k5eAaImAgFnS
podle	podle	k7c2
Ústavy	ústava	k1gFnSc2
vedoucí	vedoucí	k2eAgFnSc4d1
úlohu	úloha	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Hlavou	hlava	k1gFnSc7
státu	stát	k1gInSc2
byl	být	k5eAaImAgMnS
prezident	prezident	k1gMnSc1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšším	vysoký	k2eAgInSc7d3
orgánem	orgán	k1gInSc7
státní	státní	k2eAgFnSc2d1
moci	moc	k1gFnSc2
a	a	k8xC
zákonodárným	zákonodárný	k2eAgInSc7d1
sborem	sbor	k1gInSc7
bylo	být	k5eAaImAgNnS
Národní	národní	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
Československé	československý	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1969	#num#	k4
bylo	být	k5eAaImAgNnS
Federální	federální	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
zákonodárným	zákonodárný	k2eAgInSc7d1
sborem	sbor	k1gInSc7
československé	československý	k2eAgFnSc2d1
federace	federace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Výkonnou	výkonný	k2eAgFnSc4d1
moc	moc	k1gFnSc4
měla	mít	k5eAaImAgFnS
vláda	vláda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Soudnictví	soudnictví	k1gNnSc1
</s>
<s>
Moc	moc	k6eAd1
soudní	soudní	k2eAgFnPc1d1
vykonávaly	vykonávat	k5eAaImAgFnP
volené	volený	k2eAgFnPc1d1
soudy	soud	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Národní	národní	k2eAgInPc1d1
výbory	výbor	k1gInPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Národní	národní	k2eAgInSc1d1
výbor	výbor	k1gInSc1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Národní	národní	k2eAgInPc4d1
výbory	výbor	k1gInPc4
byly	být	k5eAaImAgInP
orgány	orgán	k1gInPc1
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
na	na	k7c6
úrovni	úroveň	k1gFnSc6
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
města	město	k1gNnSc2
<g/>
,	,	kIx,
městského	městský	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
<g/>
,	,	kIx,
okresu	okres	k1gInSc2
a	a	k8xC
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
</s>
<s>
Zvláštní	zvláštní	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
měla	mít	k5eAaImAgFnS
Slovenská	slovenský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
zákonodárným	zákonodárný	k2eAgInSc7d1
sborem	sbor	k1gInSc7
Slovenska	Slovensko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1969	#num#	k4
byla	být	k5eAaImAgFnS
Slovenská	slovenský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
zákonodárným	zákonodárný	k2eAgInSc7d1
sborem	sbor	k1gInSc7
Slovenské	slovenský	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předsedové	předseda	k1gMnPc1
vlád	vláda	k1gFnPc2
a	a	k8xC
vlády	vláda	k1gFnSc2
</s>
<s>
Viliam	Viliam	k1gMnSc1
Široký	Široký	k1gMnSc1
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
–	–	k?
<g/>
1963	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Třetí	třetí	k4xOgFnSc1
vláda	vláda	k1gFnSc1
Viliama	Viliam	k1gMnSc4
Širokého	Široký	k1gMnSc4
</s>
<s>
Jozef	Jozef	k1gMnSc1
Lenárt	Lenárta	k1gFnPc2
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
–	–	k?
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vláda	vláda	k1gFnSc1
Jozefa	Jozef	k1gMnSc2
Lenárta	Lenárt	k1gMnSc2
</s>
<s>
Oldřich	Oldřich	k1gMnSc1
Černík	Černík	k1gMnSc1
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
–	–	k?
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
První	první	k4xOgFnSc1
vláda	vláda	k1gFnSc1
Oldřicha	Oldřich	k1gMnSc2
Černíka	Černík	k1gMnSc2
</s>
<s>
Druhá	druhý	k4xOgFnSc1
vláda	vláda	k1gFnSc1
Oldřicha	Oldřich	k1gMnSc2
Černíka	Černík	k1gMnSc2
</s>
<s>
Třetí	třetí	k4xOgFnSc1
vláda	vláda	k1gFnSc1
Oldřicha	Oldřich	k1gMnSc2
Černíka	Černík	k1gMnSc2
</s>
<s>
Lubomír	Lubomír	k1gMnSc1
Štrougal	Štrougal	k1gMnSc1
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
–	–	k?
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
První	první	k4xOgFnSc1
vláda	vláda	k1gFnSc1
Lubomíra	Lubomíra	k1gFnSc1
Štrougala	Štrougala	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
vláda	vláda	k1gFnSc1
Lubomíra	Lubomíra	k1gFnSc1
Štrougala	Štrougala	k1gFnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
vláda	vláda	k1gFnSc1
Lubomíra	Lubomíra	k1gFnSc1
Štrougala	Štrougala	k1gFnSc1
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1
vláda	vláda	k1gFnSc1
Lubomíra	Lubomíra	k1gFnSc1
Štrougala	Štrougala	k1gFnSc1
</s>
<s>
Pátá	pátý	k4xOgFnSc1
vláda	vláda	k1gFnSc1
Lubomíra	Lubomíra	k1gFnSc1
Štrougala	Štrougala	k1gFnSc1
</s>
<s>
Šestá	šestý	k4xOgFnSc1
vláda	vláda	k1gFnSc1
Lubomíra	Lubomíra	k1gFnSc1
Štrougala	Štrougala	k1gFnSc1
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Adamec	Adamec	k1gMnSc1
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vláda	vláda	k1gFnSc1
Ladislava	Ladislav	k1gMnSc2
Adamce	Adamec	k1gMnSc2
</s>
<s>
Marián	Marián	k1gMnSc1
Čalfa	Čalf	k1gMnSc2
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
První	první	k4xOgFnSc1
vláda	vláda	k1gFnSc1
Mariána	Marián	k1gMnSc2
Čalfy	Čalf	k1gInPc7
</s>
<s>
Druhá	druhý	k4xOgFnSc1
vláda	vláda	k1gFnSc1
Mariána	Marián	k1gMnSc2
Čalfy	Čalf	k1gInPc4
<g/>
,	,	kIx,
vláda	vláda	k1gFnSc1
národního	národní	k2eAgNnSc2d1
porozumění	porozumění	k1gNnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Demografie	demografie	k1gFnSc1
<g/>
:	:	kIx,
revue	revue	k1gFnSc1
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
populačního	populační	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
1989	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
7	#num#	k4
<g/>
↑	↑	k?
SKALICKÝ	Skalický	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovnoprávnost	rovnoprávnost	k1gFnSc1
s	s	k7c7
Čechy	Čech	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
50	#num#	k4
lety	let	k1gInPc7
se	se	k3xPyFc4
Československo	Československo	k1gNnSc1
stalo	stát	k5eAaPmAgNnS
federací	federace	k1gFnPc2
a	a	k8xC
Slováci	Slovák	k1gMnPc1
získali	získat	k5eAaPmAgMnP
vlastní	vlastní	k2eAgInSc4d1
stát	stát	k1gInSc4
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2019-01-02	2019-01-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ústavní	ústavní	k2eAgInSc1d1
zákon	zákon	k1gInSc1
č.	č.	k?
100	#num#	k4
<g/>
/	/	kIx~
<g/>
1960	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Ústava	ústava	k1gFnSc1
Československé	československý	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
↑	↑	k?
znění	znění	k1gNnSc1
ústavního	ústavní	k2eAgInSc2d1
zákona	zákon	k1gInSc2
o	o	k7c6
změně	změna	k1gFnSc6
názvu	název	k1gInSc2
Československé	československý	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
aplikace	aplikace	k1gFnSc1
<g/>
.	.	kIx.
<g/>
mvcr	mvcr	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
znění	znění	k1gNnSc1
ústavního	ústavní	k2eAgInSc2d1
zákona	zákon	k1gInSc2
o	o	k7c6
změně	změna	k1gFnSc6
názvu	název	k1gInSc2
Československé	československý	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
aplikace	aplikace	k1gFnSc1
<g/>
.	.	kIx.
<g/>
mvcr	mvcr	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
článek	článek	k1gInSc1
1	#num#	k4
ústavního	ústavní	k2eAgInSc2d1
zákona	zákon	k1gInSc2
o	o	k7c6
československé	československý	k2eAgFnSc6d1
federaci	federace	k1gFnSc6
<g/>
↑	↑	k?
článek	článek	k1gInSc1
4	#num#	k4
Ústavy	ústava	k1gFnSc2
Československé	československý	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
↑	↑	k?
článek	článek	k1gInSc1
39	#num#	k4
Ústavy	ústava	k1gFnSc2
Československé	československý	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
↑	↑	k?
článek	článek	k1gInSc1
20	#num#	k4
ústavního	ústavní	k2eAgInSc2d1
zákon	zákon	k1gInSc1
o	o	k7c6
československé	československý	k2eAgFnSc6d1
federaci	federace	k1gFnSc6
<g/>
↑	↑	k?
článek	článek	k1gInSc1
73	#num#	k4
Ústavy	ústava	k1gFnSc2
Československé	československý	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
↑	↑	k?
článek	článek	k1gInSc1
102	#num#	k4
ústavního	ústavní	k2eAgInSc2d1
zákona	zákon	k1gInSc2
o	o	k7c6
československé	československý	k2eAgFnSc6d1
federaci	federace	k1gFnSc6
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
RYCHLÍK	Rychlík	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Češi	Čech	k1gMnPc1
a	a	k8xC
Slováci	Slovák	k1gMnPc1
ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
:	:	kIx,
spolupráce	spolupráce	k1gFnSc1
a	a	k8xC
konflikty	konflikt	k1gInPc1
1914	#num#	k4
<g/>
-	-	kIx~
<g/>
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
688	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7429	#num#	k4
<g/>
-	-	kIx~
<g/>
133	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Normalizace	normalizace	k1gFnSc1
</s>
<s>
Pražské	pražský	k2eAgNnSc1d1
jaro	jaro	k1gNnSc1
</s>
<s>
Sametová	sametový	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1
socialistického	socialistický	k2eAgNnSc2d1
Československa	Československo	k1gNnSc2
</s>
<s>
Ústavní	ústavní	k2eAgInSc1d1
zákon	zákon	k1gInSc1
o	o	k7c6
československé	československý	k2eAgFnSc6d1
federaci	federace	k1gFnSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Československá	československý	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
ČSSR	ČSSR	kA
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Časová	časový	k2eAgFnSc1d1
osa	osa	k1gFnSc1
historie	historie	k1gFnSc2
Československa	Československo	k1gNnSc2
</s>
<s>
Časová	časový	k2eAgFnSc1d1
osa	osa	k1gFnSc1
</s>
<s>
Vznik	vznik	k1gInSc1
</s>
<s>
1918	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
</s>
<s>
1938	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
</s>
<s>
1945	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
</s>
<s>
1948	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
</s>
<s>
1989	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
</s>
<s>
Zánik	zánik	k1gInSc1
</s>
<s>
ČeskoČechyMoravaSlezsko	ČeskoČechyMoravaSlezsko	k6eAd1
</s>
<s>
Předlitavskozápadní	předlitavskozápadní	k2eAgInSc1d1
částR-U	částR-U	k?
</s>
<s>
Československá	československý	k2eAgFnSc1d1
republikaaPrvní	republikaaPrvní	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Sudetyb	Sudetyb	k1gMnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
republika	republika	k1gFnSc1
</s>
<s>
Československá	československý	k2eAgFnSc1d1
republikae	republikae	k1gFnSc1
<g/>
(	(	kIx(
<g/>
1948	#num#	k4
<g/>
–	–	k?
<g/>
1960	#num#	k4
<g/>
)	)	kIx)
<g/>
někdy	někdy	k6eAd1
také	také	k9
Čtvrtá	čtvrtý	k4xOgFnSc1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
do	do	k7c2
1969	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Československá	československý	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republikaf	republikaf	k1gInSc1
<g/>
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
někdy	někdy	k6eAd1
také	také	k9
Pátá	pátý	k4xOgFnSc1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
1969	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Česká	český	k2eAgFnSc1d1
a	a	k8xC
Slovenská	slovenský	k2eAgFnSc1d1
Federativní	federativní	k2eAgFnSc1d1
Republikag	Republikag	k1gInSc1
<g/>
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Českárepublika	Českárepublika	k1gFnSc1
<g/>
(	(	kIx(
<g/>
od	od	k7c2
1993	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Česko-SlovenskárepublikacDruhárepublika	Česko-SlovenskárepublikacDruhárepublika	k1gFnSc1
<g/>
(	(	kIx(
<g/>
1938	#num#	k4
<g/>
–	–	k?
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ProtektorátČechy	ProtektorátČech	k1gInPc1
a	a	k8xC
Morava	Morava	k1gFnSc1
<g/>
(	(	kIx(
<g/>
1939	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Zalitavskovýchodní	zalitavskovýchodní	k2eAgInSc1d1
částR-U	částR-U	k?
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
(	(	kIx(
<g/>
1939	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Slovenskárepublika	Slovenskárepublika	k1gFnSc1
<g/>
(	(	kIx(
<g/>
od	od	k7c2
1993	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
jižní	jižní	k2eAgFnSc1d1
Slovenskoa	Slovenskoa	k1gFnSc1
Karpatskod	Karpatskoda	k1gFnPc2
<g/>
(	(	kIx(
<g/>
1939	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podkarpatská	podkarpatský	k2eAgFnSc1d1
Rus	Rus	k1gFnSc1
</s>
<s>
Zakarpatská	zakarpatský	k2eAgFnSc1d1
oblasth	oblasth	k1gInSc1
<g/>
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
/	/	kIx~
<g/>
1946	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zakarpatská	zakarpatský	k2eAgFnSc1d1
oblasth	oblasth	k1gInSc1
<g/>
(	(	kIx(
<g/>
od	od	k7c2
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Exilová	exilový	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
v	v	k7c6
Londýně	Londýn	k1gInSc6
</s>
<s>
a	a	k8xC
Založena	založen	k2eAgFnSc1d1
Prozatímní	prozatímní	k2eAgFnSc7d1
ústavou	ústava	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukotvena	ukotven	k2eAgFnSc1d1
tzv.	tzv.	kA
Ústavou	ústava	k1gFnSc7
1920	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
b	b	k?
Anektovány	anektovat	k5eAaBmNgFnP
nacistickým	nacistický	k2eAgNnSc7d1
Německem	Německo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
c	c	k0
Včetně	včetně	k7c2
autonomních	autonomní	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
Slovenska	Slovensko	k1gNnSc2
a	a	k8xC
Podkarpatské	podkarpatský	k2eAgFnSc6d1
Rusi	Rus	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
d	d	k?
V	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
též	též	k9
krátce	krátce	k6eAd1
Karpatská	karpatský	k2eAgFnSc1d1
Ukrajina	Ukrajina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anektované	anektovaný	k2eAgInPc1d1
a	a	k8xC
připojené	připojený	k2eAgInPc1d1
k	k	k7c3
Maďarskému	maďarský	k2eAgNnSc3d1
království	království	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
e	e	k0
Deklarována	deklarován	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
"	"	kIx"
<g/>
lidově-demokratická	lidově-demokratický	k2eAgFnSc1d1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
bez	bez	k7c2
formální	formální	k2eAgFnSc2d1
změny	změna	k1gFnSc2
názvu	název	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zřízena	zřízen	k2eAgFnSc1d1
tzv.	tzv.	kA
Ústavou	ústava	k1gFnSc7
9	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
v	v	k7c6
návaznosti	návaznost	k1gFnSc6
na	na	k7c4
státní	státní	k2eAgInSc4d1
převrat	převrat	k1gInSc4
v	v	k7c6
únoru	únor	k1gInSc6
1948	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
f	f	k?
Po	po	k7c6
období	období	k1gNnSc6
tzv.	tzv.	kA
pražského	pražský	k2eAgNnSc2d1
jara	jaro	k1gNnSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1969	#num#	k4
k	k	k7c3
federalizaci	federalizace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
dosud	dosud	k6eAd1
unitárního	unitární	k2eAgNnSc2d1
Československa	Československo	k1gNnSc2
se	se	k3xPyFc4
v	v	k7c6
období	období	k1gNnSc6
tzv.	tzv.	kA
normalizace	normalizace	k1gFnSc1
stala	stát	k5eAaPmAgFnS
federace	federace	k1gFnSc2
dvou	dva	k4xCgInPc2
národních	národní	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
:	:	kIx,
ČSR	ČSR	kA
a	a	k8xC
SSR	SSR	kA
<g/>
.	.	kIx.
</s>
<s>
g	g	kA
Krátce	krátce	k6eAd1
též	též	k9
Československá	československý	k2eAgFnSc1d1
federativní	federativní	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Období	období	k1gNnSc1
tzv.	tzv.	kA
pomlčkové	pomlčkový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
h	h	k?
Součást	součást	k1gFnSc1
Ukrajinské	ukrajinský	k2eAgInPc4d1
SSR	SSR	kA
resp.	resp.	kA
Ukrajiny	Ukrajina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Komunistický	komunistický	k2eAgInSc1d1
režim	režim	k1gInSc1
v	v	k7c6
Československu	Československo	k1gNnSc6
|	|	kIx~
Politika	politika	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
124946369	#num#	k4
</s>
