<s>
Československá	československý	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
ČSSR	ČSSR	kA
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
oficiální	oficiální	k2eAgInSc1d1
název	název	k1gInSc1
Československa	Československo	k1gNnSc2
od	od	k7c2
11	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1960	#num#	k4
do	do	k7c2
29	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1990	#num#	k4
<g/>
.	.	kIx.
</s>