<s>
Lambda	lambda	k1gNnSc1	lambda
(	(	kIx(	(
<g/>
majuskulní	majuskulní	k2eAgFnSc1d1	majuskulní
podoba	podoba	k1gFnSc1	podoba
Λ	Λ	k?	Λ
<g/>
,	,	kIx,	,
minuskulní	minuskulní	k2eAgFnSc1d1	minuskulní
podoba	podoba	k1gFnSc1	podoba
λ	λ	k?	λ
<g/>
,	,	kIx,	,
řecký	řecký	k2eAgInSc1d1	řecký
název	název	k1gInSc1	název
Λ	Λ	k?	Λ
i	i	k8xC	i
Λ	Λ	k?	Λ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedenácté	jedenáctý	k4xOgNnSc1	jedenáctý
písmeno	písmeno	k1gNnSc1	písmeno
řecké	řecký	k2eAgFnSc2d1	řecká
abecedy	abeceda	k1gFnSc2	abeceda
a	a	k8xC	a
v	v	k7c6	v
systému	systém	k1gInSc6	systém
řeckých	řecký	k2eAgFnPc2d1	řecká
číslovek	číslovka	k1gFnPc2	číslovka
má	mít	k5eAaImIp3nS	mít
hodnotu	hodnota	k1gFnSc4	hodnota
30	[number]	k4	30
<g/>
.	.	kIx.	.
</s>
<s>
Majuskulní	majuskulní	k2eAgFnSc1d1	majuskulní
varianta	varianta	k1gFnSc1	varianta
písmena	písmeno	k1gNnSc2	písmeno
'	'	kIx"	'
<g/>
Λ	Λ	k?	Λ
<g/>
'	'	kIx"	'
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
<g/>
:	:	kIx,	:
elementární	elementární	k2eAgFnPc4d1	elementární
částice	částice	k1gFnPc4	částice
Λ	Λ	k?	Λ
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
částic	částice	k1gFnPc2	částice
symbol	symbol	k1gInSc1	symbol
starověkého	starověký	k2eAgInSc2d1	starověký
Lakedaimónu	Lakedaimón	k1gInSc2	Lakedaimón
<g/>
,	,	kIx,	,
vojenský	vojenský	k2eAgInSc1d1	vojenský
znak	znak	k1gInSc1	znak
spartských	spartský	k2eAgMnPc2d1	spartský
bojovníků	bojovník	k1gMnPc2	bojovník
minuskulní	minuskulní	k2eAgFnSc1d1	minuskulní
varianta	varianta	k1gFnSc1	varianta
písmena	písmeno	k1gNnSc2	písmeno
'	'	kIx"	'
<g/>
λ	λ	k?	λ
<g/>
'	'	kIx"	'
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
<g/>
:	:	kIx,	:
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
délku	délka	k1gFnSc4	délka
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
tepelná	tepelný	k2eAgFnSc1d1	tepelná
vodivost	vodivost	k1gFnSc1	vodivost
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
Laméův	Laméův	k2eAgInSc4d1	Laméův
<g />
.	.	kIx.	.
</s>
<s>
elastický	elastický	k2eAgInSc1d1	elastický
koeficient	koeficient	k1gInSc1	koeficient
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
označení	označení	k1gNnSc2	označení
prázdného	prázdný	k2eAgNnSc2d1	prázdné
slova	slovo	k1gNnSc2	slovo
ve	v	k7c6	v
formálních	formální	k2eAgInPc6d1	formální
jazycích	jazyk	k1gInPc6	jazyk
označení	označení	k1gNnSc2	označení
funkce	funkce	k1gFnSc2	funkce
v	v	k7c4	v
lambda	lambda	k1gNnSc4	lambda
kalkulu	kalkul	k1gInSc2	kalkul
vlastní	vlastní	k2eAgNnSc4d1	vlastní
číslo	číslo	k1gNnSc4	číslo
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
Lebesgueovu	Lebesgueův	k2eAgFnSc4d1	Lebesgueova
míru	míra	k1gFnSc4	míra
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
Lagrangeův	Lagrangeův	k2eAgInSc4d1	Lagrangeův
multiplikátor	multiplikátor	k1gInSc4	multiplikátor
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
článek	článek	k1gInSc4	článek
o	o	k7c6	o
vázaných	vázaný	k2eAgInPc6d1	vázaný
extrémech	extrém	k1gInPc6	extrém
<g/>
)	)	kIx)	)
zeměpisnou	zeměpisný	k2eAgFnSc4d1	zeměpisná
délku	délka	k1gFnSc4	délka
symbol	symbol	k1gInSc1	symbol
hnutí	hnutí	k1gNnSc2	hnutí
za	za	k7c4	za
práva	právo	k1gNnPc4	právo
homosexuálů	homosexuál	k1gMnPc2	homosexuál
symbol	symbol	k1gInSc1	symbol
her	hra	k1gFnPc2	hra
Counter	Countra	k1gFnPc2	Countra
Strike	Strik	k1gFnSc2	Strik
a	a	k8xC	a
Half	halfa	k1gFnPc2	halfa
<g />
.	.	kIx.	.
</s>
<s>
Life	Life	k6eAd1	Life
V	v	k7c6	v
Unicode	Unicod	k1gInSc5	Unicod
je	být	k5eAaImIp3nS	být
podporováno	podporován	k2eAgNnSc1d1	podporováno
jak	jak	k8xS	jak
majuskulní	majuskulní	k2eAgNnSc1d1	majuskulní
lambda	lambda	k1gNnSc1	lambda
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
39	[number]	k4	39
<g/>
B	B	kA	B
GREEK	GREEK	kA	GREEK
CAPITAL	CAPITAL	kA	CAPITAL
LETTER	LETTER	kA	LETTER
LAMDA	LAMDA	kA	LAMDA
tak	tak	k8xS	tak
minuskulní	minuskulní	k2eAgNnSc1d1	minuskulní
lambda	lambda	k1gNnSc1	lambda
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
BB	BB	kA	BB
GREEK	GREEK	kA	GREEK
SMALL	SMALL	kA	SMALL
LETTER	LETTER	kA	LETTER
LAMDA	LAMDA	kA	LAMDA
V	v	k7c6	v
HTML	HTML	kA	HTML
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
je	on	k3xPp3gInPc4	on
zapsat	zapsat	k5eAaPmF	zapsat
pomocí	pomocí	k7c2	pomocí
Λ	Λ	k?	Λ
respektive	respektive	k9	respektive
λ	λ	k?	λ
Lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
také	také	k9	také
zapsat	zapsat	k5eAaPmF	zapsat
pomocí	pomocí	k7c2	pomocí
HTML	HTML	kA	HTML
entit	entita	k1gFnPc2	entita
Λ	Λ	k?	Λ
respektive	respektive	k9	respektive
λ	λ	k?	λ
V	v	k7c6	v
LaTeXu	latex	k1gInSc6	latex
se	se	k3xPyFc4	se
majuskulní	majuskulní	k2eAgNnSc1d1	majuskulní
lambda	lambda	k1gNnSc1	lambda
píše	psát	k5eAaImIp3nS	psát
příkazem	příkaz	k1gInSc7	příkaz
\	\	kIx~	\
<g/>
Lambda	lambda	k1gNnSc1	lambda
a	a	k8xC	a
minuskulní	minuskulní	k2eAgNnSc1d1	minuskulní
lambda	lambda	k1gNnSc1	lambda
příkazem	příkaz	k1gInSc7	příkaz
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
<g/>
.	.	kIx.	.
</s>
