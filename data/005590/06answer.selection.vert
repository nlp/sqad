<s>
Lambda	lambda	k1gNnSc1	lambda
(	(	kIx(	(
<g/>
majuskulní	majuskulní	k2eAgFnSc1d1	majuskulní
podoba	podoba	k1gFnSc1	podoba
Λ	Λ	k?	Λ
<g/>
,	,	kIx,	,
minuskulní	minuskulní	k2eAgFnSc1d1	minuskulní
podoba	podoba	k1gFnSc1	podoba
λ	λ	k?	λ
<g/>
,	,	kIx,	,
řecký	řecký	k2eAgInSc1d1	řecký
název	název	k1gInSc1	název
Λ	Λ	k?	Λ
i	i	k8xC	i
Λ	Λ	k?	Λ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedenácté	jedenáctý	k4xOgNnSc1	jedenáctý
písmeno	písmeno	k1gNnSc1	písmeno
řecké	řecký	k2eAgFnSc2d1	řecká
abecedy	abeceda	k1gFnSc2	abeceda
a	a	k8xC	a
v	v	k7c6	v
systému	systém	k1gInSc6	systém
řeckých	řecký	k2eAgFnPc2d1	řecká
číslovek	číslovka	k1gFnPc2	číslovka
má	mít	k5eAaImIp3nS	mít
hodnotu	hodnota	k1gFnSc4	hodnota
30	[number]	k4	30
<g/>
.	.	kIx.	.
</s>
