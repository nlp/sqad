<s>
Eniwetok	Eniwetok	k1gInSc1	Eniwetok
(	(	kIx(	(
<g/>
jap.	jap.	k?	jap.
<g/>
:	:	kIx,	:
エ	エ	k?	エ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ostrov	ostrov	k1gInSc4	ostrov
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
(	(	kIx(	(
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
souostroví	souostroví	k1gNnSc2	souostroví
Marshallovy	Marshallův	k2eAgInPc1d1	Marshallův
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
40	[number]	k4	40
malých	malý	k2eAgInPc2d1	malý
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
spolu	spolu	k6eAd1	spolu
tvoří	tvořit	k5eAaImIp3nP	tvořit
atol	atol	k1gInSc4	atol
Eniwetok	Eniwetok	k1gInSc1	Eniwetok
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
asi	asi	k9	asi
525	[number]	k4	525
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
ostrova	ostrov	k1gInSc2	ostrov
Kwajalein	Kwajaleina	k1gFnPc2	Kwajaleina
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
rozloha	rozloha	k1gFnSc1	rozloha
všech	všecek	k3xTgInPc2	všecek
ostrovů	ostrov	k1gInPc2	ostrov
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
5,85	[number]	k4	5,85
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Laguna	laguna	k1gFnSc1	laguna
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
má	mít	k5eAaImIp3nS	mít
1	[number]	k4	1
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
4,89	[number]	k4	4,89
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
asi	asi	k9	asi
1	[number]	k4	1
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
ostrovy	ostrov	k1gInPc7	ostrov
jsou	být	k5eAaImIp3nP	být
Eniwetok	Eniwetok	k1gInSc4	Eniwetok
<g/>
,	,	kIx,	,
Engebi	Engebe	k1gFnSc4	Engebe
(	(	kIx(	(
<g/>
zvaný	zvaný	k2eAgMnSc1d1	zvaný
také	také	k9	také
Arthur	Arthur	k1gMnSc1	Arthur
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Parry	Parr	k1gInPc1	Parr
<g/>
,	,	kIx,	,
Muty	Muty	k?	Muty
a	a	k8xC	a
Igurin	Igurin	k1gInSc1	Igurin
<g/>
.	.	kIx.	.
</s>
<s>
Technicky	technicky	k6eAd1	technicky
vzato	vzít	k5eAaPmNgNnS	vzít
byl	být	k5eAaImAgInS	být
Eniwetok	Eniwetok	k1gInSc1	Eniwetok
původně	původně	k6eAd1	původně
španělskou	španělský	k2eAgFnSc7d1	španělská
kolonií	kolonie	k1gFnSc7	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Evropany	Evropan	k1gMnPc4	Evropan
jej	on	k3xPp3gInSc2	on
objevila	objevit	k5eAaPmAgFnS	objevit
roku	rok	k1gInSc2	rok
1794	[number]	k4	1794
britská	britský	k2eAgFnSc1d1	britská
šalupa	šalupa	k1gFnSc1	šalupa
Walpole	Walpole	k1gFnSc2	Walpole
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
německé	německý	k2eAgFnSc2d1	německá
kolonie	kolonie	k1gFnSc2	kolonie
Marshallových	Marshallův	k2eAgInPc2d1	Marshallův
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
obsadili	obsadit	k5eAaPmAgMnP	obsadit
ostrovy	ostrov	k1gInPc4	ostrov
Japonci	Japonec	k1gMnPc1	Japonec
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
je	on	k3xPp3gNnPc4	on
spravovali	spravovat	k5eAaImAgMnP	spravovat
jako	jako	k8xS	jako
své	svůj	k3xOyFgNnSc4	svůj
mandátní	mandátní	k2eAgNnSc4d1	mandátní
území	území	k1gNnSc4	území
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
Japonci	Japonec	k1gMnPc1	Japonec
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Engebi	Engeb	k1gFnSc2	Engeb
postavili	postavit	k5eAaPmAgMnP	postavit
letiště	letiště	k1gNnSc4	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Používali	používat	k5eAaImAgMnP	používat
je	on	k3xPp3gInPc4	on
pro	pro	k7c4	pro
letadla	letadlo	k1gNnPc1	letadlo
hlídkující	hlídkující	k2eAgNnPc1d1	hlídkující
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Karolín	Karolína	k1gFnPc2	Karolína
a	a	k8xC	a
zbytku	zbytek	k1gInSc2	zbytek
Marshallových	Marshallův	k2eAgInPc2d1	Marshallův
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1944	[number]	k4	1944
ostrovy	ostrov	k1gInPc1	ostrov
dobyly	dobýt	k5eAaPmAgInP	dobýt
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
evakuováno	evakuovat	k5eAaBmNgNnS	evakuovat
a	a	k8xC	a
atol	atol	k1gInSc1	atol
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
pro	pro	k7c4	pro
testování	testování	k1gNnSc4	testování
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Jaderné	jaderný	k2eAgInPc1d1	jaderný
testy	test	k1gInPc1	test
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1948	[number]	k4	1948
až	až	k9	až
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
se	se	k3xPyFc4	se
na	na	k7c6	na
atolu	atol	k1gInSc6	atol
uskutečnil	uskutečnit	k5eAaPmAgMnS	uskutečnit
test	test	k1gInSc4	test
první	první	k4xOgFnSc2	první
vodíkové	vodíkový	k2eAgFnSc2d1	vodíková
bomby	bomba	k1gFnSc2	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
za	za	k7c4	za
na	na	k7c4	na
atol	atol	k1gInSc4	atol
vrátili	vrátit	k5eAaPmAgMnP	vrátit
až	až	k9	až
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
začala	začít	k5eAaPmAgFnS	začít
americká	americký	k2eAgFnSc1d1	americká
vláda	vláda	k1gFnSc1	vláda
odstraňovat	odstraňovat	k5eAaImF	odstraňovat
z	z	k7c2	z
atolu	atol	k1gInSc2	atol
znečištěnou	znečištěný	k2eAgFnSc4d1	znečištěná
půdu	půda	k1gFnSc4	půda
a	a	k8xC	a
ostatní	ostatní	k2eAgInSc4d1	ostatní
materiál	materiál	k1gInSc4	materiál
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
ostrov	ostrov	k1gInSc4	ostrov
za	za	k7c4	za
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
k	k	k7c3	k
bydlení	bydlení	k1gNnSc3	bydlení
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1952	[number]	k4	1952
explodovala	explodovat	k5eAaBmAgFnS	explodovat
na	na	k7c6	na
atolu	atol	k1gInSc6	atol
první	první	k4xOgFnSc1	první
vodíková	vodíkový	k2eAgFnSc1d1	vodíková
bomba	bomba	k1gFnSc1	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Vážila	vážit	k5eAaImAgFnS	vážit
65	[number]	k4	65
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Korálový	korálový	k2eAgInSc1d1	korálový
ostrov	ostrov	k1gInSc1	ostrov
Elugelab	Elugelaba	k1gFnPc2	Elugelaba
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
výbuch	výbuch	k1gInSc1	výbuch
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
<g/>
,	,	kIx,	,
zmizel	zmizet	k5eAaPmAgMnS	zmizet
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
místě	místo	k1gNnSc6	místo
zůstal	zůstat	k5eAaPmAgInS	zůstat
pouze	pouze	k6eAd1	pouze
kráter	kráter	k1gInSc1	kráter
hluboký	hluboký	k2eAgInSc1d1	hluboký
60	[number]	k4	60
metrů	metr	k1gInPc2	metr
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
2	[number]	k4	2
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Výbuch	výbuch	k1gInSc1	výbuch
měl	mít	k5eAaImAgInS	mít
sílu	síla	k1gFnSc4	síla
10	[number]	k4	10
megatun	megatuna	k1gFnPc2	megatuna
TNT	TNT	kA	TNT
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
atolu	atol	k1gInSc6	atol
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
celkem	celkem	k6eAd1	celkem
43	[number]	k4	43
atomových	atomový	k2eAgFnPc2d1	atomová
explozí	exploze	k1gFnPc2	exploze
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
filmové	filmový	k2eAgInPc1d1	filmový
záběry	záběr	k1gInPc1	záběr
tří	tři	k4xCgFnPc2	tři
těchto	tento	k3xDgFnPc2	tento
zkoušek	zkouška	k1gFnPc2	zkouška
<g/>
.	.	kIx.	.
</s>
