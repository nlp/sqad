<s>
Optický	optický	k2eAgInSc1d1	optický
dalekohled	dalekohled	k1gInSc1	dalekohled
či	či	k8xC	či
teleskop	teleskop	k1gInSc1	teleskop
je	být	k5eAaImIp3nS	být
přístroj	přístroj	k1gInSc4	přístroj
k	k	k7c3	k
optickému	optický	k2eAgNnSc3d1	optické
přiblížení	přiblížení	k1gNnSc3	přiblížení
pomocí	pomoc	k1gFnPc2	pomoc
dvou	dva	k4xCgFnPc2	dva
soustav	soustava	k1gFnPc2	soustava
čoček	čočka	k1gFnPc2	čočka
nebo	nebo	k8xC	nebo
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
<g/>
:	:	kIx,	:
objektivu	objektiv	k1gInSc2	objektiv
a	a	k8xC	a
okuláru	okulár	k1gInSc2	okulár
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
obraz	obraz	k1gInSc1	obraz
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
<g/>
.	.	kIx.	.
</s>
