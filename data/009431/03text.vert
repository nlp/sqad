<p>
<s>
Ocet	ocet	k1gInSc1	ocet
je	být	k5eAaImIp3nS	být
potravinářská	potravinářský	k2eAgFnSc1d1	potravinářská
přísada	přísada	k1gFnSc1	přísada
a	a	k8xC	a
konzervační	konzervační	k2eAgNnSc1d1	konzervační
činidlo	činidlo	k1gNnSc1	činidlo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
hlavní	hlavní	k2eAgFnSc7d1	hlavní
složkou	složka	k1gFnSc7	složka
je	být	k5eAaImIp3nS	být
kyselina	kyselina	k1gFnSc1	kyselina
octová	octový	k2eAgFnSc1d1	octová
v	v	k7c6	v
koncentraci	koncentrace	k1gFnSc6	koncentrace
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
octa	ocet	k1gInSc2	ocet
od	od	k7c2	od
asi	asi	k9	asi
4	[number]	k4	4
do	do	k7c2	do
18	[number]	k4	18
%	%	kIx~	%
(	(	kIx(	(
<g/>
nejčastější	častý	k2eAgFnSc1d3	nejčastější
koncentrace	koncentrace	k1gFnSc1	koncentrace
je	být	k5eAaImIp3nS	být
8	[number]	k4	8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Potravinářský	potravinářský	k2eAgInSc1d1	potravinářský
ocet	ocet	k1gInSc1	ocet
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
oxidací	oxidace	k1gFnSc7	oxidace
tekutin	tekutina	k1gFnPc2	tekutina
obsahujících	obsahující	k2eAgFnPc2d1	obsahující
ethanol	ethanol	k1gInSc4	ethanol
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vinný	vinný	k2eAgInSc1d1	vinný
ocet	ocet	k1gInSc1	ocet
z	z	k7c2	z
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
oxidace	oxidace	k1gFnSc2	oxidace
bakterie	bakterie	k1gFnSc2	bakterie
rodu	rod	k1gInSc2	rod
Acetobacter	Acetobactrum	k1gNnPc2	Acetobactrum
oxidují	oxidovat	k5eAaBmIp3nP	oxidovat
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
kyslíku	kyslík	k1gInSc2	kyslík
ethanol	ethanol	k1gInSc1	ethanol
na	na	k7c4	na
kyselinu	kyselina	k1gFnSc4	kyselina
octovou	octový	k2eAgFnSc4d1	octová
<g/>
.	.	kIx.	.
</s>
<s>
Ocet	ocet	k1gInSc1	ocet
se	se	k3xPyFc4	se
v	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
užívá	užívat	k5eAaImIp3nS	užívat
jednak	jednak	k8xC	jednak
ke	k	k7c3	k
konzervaci	konzervace	k1gFnSc3	konzervace
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
jako	jako	k9	jako
okyselující	okyselující	k2eAgFnSc4d1	okyselující
součást	součást	k1gFnSc4	součást
pokrmů	pokrm	k1gInPc2	pokrm
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
kuchyní	kuchyně	k1gFnPc2	kuchyně
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
takto	takto	k6eAd1	takto
používán	používat	k5eAaImNgInS	používat
již	již	k6eAd1	již
v	v	k7c6	v
antice	antika	k1gFnSc6	antika
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
chuťové	chuťový	k2eAgFnPc4d1	chuťová
<g/>
,	,	kIx,	,
konzervační	konzervační	k2eAgFnPc4d1	konzervační
<g/>
,	,	kIx,	,
deodorační	deodorační	k2eAgFnPc4d1	deodorační
a	a	k8xC	a
čisticí	čisticí	k2eAgFnPc4d1	čisticí
vlastnosti	vlastnost	k1gFnPc4	vlastnost
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
nazýván	nazývat	k5eAaImNgInS	nazývat
pokladem	poklad	k1gInSc7	poklad
domácnosti	domácnost	k1gFnSc2	domácnost
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc1	druh
octa	ocet	k1gInSc2	ocet
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vinný	vinný	k2eAgInSc1d1	vinný
ocet	ocet	k1gInSc1	ocet
===	===	k?	===
</s>
</p>
<p>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
červeného	červený	k2eAgNnSc2d1	červené
nebo	nebo	k8xC	nebo
bílého	bílý	k2eAgNnSc2d1	bílé
vína	víno	k1gNnSc2	víno
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejobvyklejší	obvyklý	k2eAgInSc1d3	nejobvyklejší
druh	druh	k1gInSc1	druh
octa	ocet	k1gInSc2	ocet
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Středozemí	středozemí	k1gNnSc6	středozemí
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
víno	víno	k1gNnSc1	víno
se	se	k3xPyFc4	se
i	i	k9	i
vinné	vinný	k2eAgInPc1d1	vinný
octy	ocet	k1gInPc1	ocet
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
velkými	velký	k2eAgInPc7d1	velký
rozdíly	rozdíl	k1gInPc7	rozdíl
v	v	k7c6	v
kvalitě	kvalita	k1gFnSc6	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Lepší	dobrý	k2eAgInPc1d2	lepší
vinné	vinný	k2eAgInPc1d1	vinný
octy	ocet	k1gInPc1	ocet
zrají	zrát	k5eAaImIp3nP	zrát
v	v	k7c6	v
dřevěných	dřevěný	k2eAgInPc6d1	dřevěný
sudech	sud	k1gInPc6	sud
až	až	k9	až
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jablečný	jablečný	k2eAgInSc1d1	jablečný
ocet	ocet	k1gInSc1	ocet
===	===	k?	===
</s>
</p>
<p>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
jablečného	jablečný	k2eAgInSc2d1	jablečný
moštu	mošt	k1gInSc2	mošt
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
žlutohnědou	žlutohnědý	k2eAgFnSc4d1	žlutohnědá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Balzámový	balzámový	k2eAgInSc1d1	balzámový
ocet	ocet	k1gInSc1	ocet
===	===	k?	===
</s>
</p>
<p>
<s>
Balzámový	balzámový	k2eAgInSc1d1	balzámový
ocet	ocet	k1gInSc1	ocet
(	(	kIx(	(
<g/>
aceto	acet	k2eAgNnSc1d1	aceto
balsamico	balsamico	k1gNnSc1	balsamico
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
druh	druh	k1gInSc4	druh
octa	ocet	k1gInSc2	ocet
vyráběný	vyráběný	k2eAgInSc4d1	vyráběný
tradičně	tradičně	k6eAd1	tradičně
v	v	k7c6	v
italské	italský	k2eAgFnSc6d1	italská
Modeně	Modena	k1gFnSc6	Modena
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
vylisovaných	vylisovaný	k2eAgInPc2d1	vylisovaný
hroznů	hrozen	k1gInPc2	hrozen
odrůdy	odrůda	k1gFnSc2	odrůda
trebbiano	trebbiana	k1gFnSc5	trebbiana
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgInSc1d1	tradiční
balzámový	balzámový	k2eAgInSc1d1	balzámový
ocet	ocet	k1gInSc1	ocet
pak	pak	k6eAd1	pak
zraje	zrát	k5eAaImIp3nS	zrát
nejméně	málo	k6eAd3	málo
dvanáct	dvanáct	k4xCc4	dvanáct
let	léto	k1gNnPc2	léto
v	v	k7c6	v
dřevěných	dřevěný	k2eAgInPc6d1	dřevěný
sudech	sud	k1gInPc6	sud
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
cen	cena	k1gFnPc2	cena
srovnatelných	srovnatelný	k2eAgFnPc6d1	srovnatelná
s	s	k7c7	s
nejlepšími	dobrý	k2eAgNnPc7d3	nejlepší
víny	víno	k1gNnPc7	víno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rýžový	rýžový	k2eAgInSc1d1	rýžový
ocet	ocet	k1gInSc1	ocet
===	===	k?	===
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
oblíbený	oblíbený	k2eAgMnSc1d1	oblíbený
v	v	k7c6	v
kuchyních	kuchyně	k1gFnPc6	kuchyně
východní	východní	k2eAgFnSc2d1	východní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
světlý	světlý	k2eAgMnSc1d1	světlý
<g/>
,	,	kIx,	,
červený	červený	k2eAgMnSc1d1	červený
a	a	k8xC	a
tmavý	tmavý	k2eAgMnSc1d1	tmavý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
je	být	k5eAaImIp3nS	být
nejoblíbenější	oblíbený	k2eAgInSc1d3	nejoblíbenější
světlý	světlý	k2eAgInSc1d1	světlý
rýžový	rýžový	k2eAgInSc1d1	rýžový
ocet	ocet	k1gInSc1	ocet
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
na	na	k7c6	na
suši	suš	k1gFnSc6	suš
rýži	rýže	k1gFnSc6	rýže
a	a	k8xC	a
do	do	k7c2	do
salátových	salátový	k2eAgFnPc2d1	salátová
zálivek	zálivka	k1gFnPc2	zálivka
<g/>
.	.	kIx.	.
</s>
<s>
Tmavý	tmavý	k2eAgInSc1d1	tmavý
rýžový	rýžový	k2eAgInSc1d1	rýžový
ocet	ocet	k1gInSc1	ocet
je	být	k5eAaImIp3nS	být
nejpopulárnější	populární	k2eAgInSc1d3	nejpopulárnější
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
jihoasijských	jihoasijský	k2eAgFnPc6d1	jihoasijská
kuchyních	kuchyně	k1gFnPc6	kuchyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sladový	sladový	k2eAgInSc1d1	sladový
ocet	ocet	k1gInSc1	ocet
===	===	k?	===
</s>
</p>
<p>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
sladováním	sladování	k1gNnSc7	sladování
ječmene	ječmen	k1gInSc2	ječmen
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
se	se	k3xPyFc4	se
škrob	škrob	k1gInSc1	škrob
v	v	k7c6	v
zrnu	zrno	k1gNnSc6	zrno
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
na	na	k7c4	na
maltózu	maltóza	k1gFnSc4	maltóza
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
uvaří	uvařit	k5eAaPmIp3nS	uvařit
ale	ale	k9	ale
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
nechá	nechat	k5eAaPmIp3nS	nechat
zoctovatět	zoctovatět	k5eAaPmF	zoctovatět
<g/>
.	.	kIx.	.
</s>
<s>
Výsledný	výsledný	k2eAgInSc1d1	výsledný
ocet	ocet	k1gInSc1	ocet
posléze	posléze	k6eAd1	posléze
zraje	zrát	k5eAaImIp3nS	zrát
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
má	mít	k5eAaImIp3nS	mít
světle	světle	k6eAd1	světle
hnědou	hnědý	k2eAgFnSc4d1	hnědá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
zejména	zejména	k9	zejména
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lihový	lihový	k2eAgInSc4d1	lihový
(	(	kIx(	(
<g/>
kvasný	kvasný	k2eAgInSc4d1	kvasný
<g/>
)	)	kIx)	)
ocet	ocet	k1gInSc4	ocet
===	===	k?	===
</s>
</p>
<p>
<s>
Výroba	výroba	k1gFnSc1	výroba
probíhá	probíhat	k5eAaImIp3nS	probíhat
kvašením	kvašení	k1gNnSc7	kvašení
ředěného	ředěný	k2eAgInSc2d1	ředěný
etanolu	etanol	k1gInSc2	etanol
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodném	vodný	k2eAgInSc6d1	vodný
roztoku	roztok	k1gInSc6	roztok
je	být	k5eAaImIp3nS	být
kvasný	kvasný	k2eAgInSc1d1	kvasný
ocet	ocet	k1gInSc1	ocet
při	při	k7c6	při
velmi	velmi	k6eAd1	velmi
nízkých	nízký	k2eAgFnPc6d1	nízká
koncentracích	koncentrace	k1gFnPc6	koncentrace
vůní	vůně	k1gFnPc2	vůně
i	i	k9	i
chutí	chuť	k1gFnSc7	chuť
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc1d1	podobný
roztoku	roztoka	k1gFnSc4	roztoka
kyseliny	kyselina	k1gFnSc2	kyselina
citronové	citronový	k2eAgFnSc2d1	citronová
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemu	co	k3yQnSc3	co
jej	on	k3xPp3gInSc4	on
lze	lze	k6eAd1	lze
s	s	k7c7	s
úspěchem	úspěch	k1gInSc7	úspěch
použít	použít	k5eAaPmF	použít
v	v	k7c4	v
kuchyni	kuchyně	k1gFnSc4	kuchyně
do	do	k7c2	do
nápojů	nápoj	k1gInPc2	nápoj
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
prostředek	prostředek	k1gInSc4	prostředek
proti	proti	k7c3	proti
zápachům	zápach	k1gInPc3	zápach
<g/>
.	.	kIx.	.
</s>
<s>
Octová	octový	k2eAgFnSc1d1	octová
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
též	též	k9	též
výborný	výborný	k2eAgInSc4d1	výborný
prostředek	prostředek	k1gInSc4	prostředek
proti	proti	k7c3	proti
zatuchlině	zatuchlina	k1gFnSc3	zatuchlina
–	–	k?	–
kanalizace	kanalizace	k1gFnSc1	kanalizace
<g/>
,	,	kIx,	,
automatické	automatický	k2eAgFnPc1d1	automatická
pračky	pračka	k1gFnPc1	pračka
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Kvasný	kvasný	k2eAgInSc1d1	kvasný
ocet	ocet	k1gInSc1	ocet
nalezl	nalézt	k5eAaBmAgInS	nalézt
uplatnění	uplatnění	k1gNnSc3	uplatnění
také	také	k9	také
jako	jako	k9	jako
lék	lék	k1gInSc4	lék
proti	proti	k7c3	proti
plísňovým	plísňový	k2eAgNnPc3d1	plísňové
onemocněním	onemocnění	k1gNnPc3	onemocnění
nohou	noha	k1gFnPc2	noha
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
bazénová	bazénový	k2eAgFnSc1d1	bazénová
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Vinegar	Vinegara	k1gFnPc2	Vinegara
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ocet	ocet	k1gInSc1	ocet
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Ocet	ocet	k1gInSc1	ocet
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
ocet	ocet	k1gInSc1	ocet
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
