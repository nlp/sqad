<p>
<s>
Ulomené	ulomený	k2eAgNnSc1d1	ulomené
ucho	ucho	k1gNnSc1	ucho
je	být	k5eAaImIp3nS	být
šestá	šestý	k4xOgFnSc1	šestý
kniha	kniha	k1gFnSc1	kniha
z	z	k7c2	z
komiksového	komiksový	k2eAgInSc2d1	komiksový
cyklu	cyklus	k1gInSc2	cyklus
Tintinova	Tintinův	k2eAgNnSc2d1	Tintinovo
dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
poprvé	poprvé	k6eAd1	poprvé
vycházely	vycházet	k5eAaImAgInP	vycházet
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Petit	petit	k1gInSc1	petit
Vingtiéme	Vingtiém	k1gInSc5	Vingtiém
v	v	k7c6	v
letech	let	k1gInPc6	let
1935	[number]	k4	1935
<g/>
–	–	k?	–
<g/>
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Hergé	Hergé	k6eAd1	Hergé
tuto	tento	k3xDgFnSc4	tento
verzi	verze	k1gFnSc4	verze
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
upravil	upravit	k5eAaPmAgInS	upravit
na	na	k7c4	na
62	[number]	k4	62
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Celý	celý	k2eAgInSc1d1	celý
příběh	příběh	k1gInSc1	příběh
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
etnografickém	etnografický	k2eAgNnSc6d1	etnografické
muzeu	muzeum	k1gNnSc6	muzeum
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
začne	začít	k5eAaPmIp3nS	začít
správce	správce	k1gMnSc1	správce
uklízet	uklízet	k5eAaImF	uklízet
a	a	k8xC	a
všimne	všimnout	k5eAaPmIp3nS	všimnout
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
odcizena	odcizen	k2eAgFnSc1d1	odcizena
vzácná	vzácný	k2eAgFnSc1d1	vzácná
Arumbajská	Arumbajský	k2eAgFnSc1d1	Arumbajský
modla	modla	k1gFnSc1	modla
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
poté	poté	k6eAd1	poté
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
věc	věc	k1gFnSc1	věc
ohlášena	ohlásit	k5eAaPmNgFnS	ohlásit
v	v	k7c6	v
rádiu	rádius	k1gInSc6	rádius
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
i	i	k9	i
Tintin	Tintin	k1gInSc1	Tintin
<g/>
.	.	kIx.	.
</s>
<s>
Okamžitě	okamžitě	k6eAd1	okamžitě
běží	běžet	k5eAaImIp3nS	běžet
do	do	k7c2	do
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
již	již	k9	již
detektivové	detektiv	k1gMnPc1	detektiv
Tkadlec	tkadlec	k1gMnSc1	tkadlec
a	a	k8xC	a
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
případ	případ	k1gInSc4	případ
vyšetřují	vyšetřovat	k5eAaImIp3nP	vyšetřovat
<g/>
.	.	kIx.	.
</s>
<s>
Příštího	příští	k2eAgInSc2d1	příští
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
správce	správce	k1gMnSc1	správce
objeví	objevit	k5eAaPmIp3nS	objevit
modlu	modla	k1gFnSc4	modla
zase	zase	k9	zase
na	na	k7c6	na
stejném	stejný	k2eAgNnSc6d1	stejné
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
modly	modla	k1gFnSc2	modla
je	být	k5eAaImIp3nS	být
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
je	být	k5eAaImIp3nS	být
napsáno	napsat	k5eAaBmNgNnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
celá	celý	k2eAgFnSc1d1	celá
krádež	krádež	k1gFnSc1	krádež
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
kvůli	kvůli	k7c3	kvůli
jedné	jeden	k4xCgFnSc3	jeden
sázce	sázka	k1gFnSc3	sázka
<g/>
.	.	kIx.	.
</s>
<s>
Policie	policie	k1gFnSc1	policie
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
tímto	tento	k3xDgNnSc7	tento
je	být	k5eAaImIp3nS	být
případ	případ	k1gInSc4	případ
vyřešený	vyřešený	k2eAgInSc4d1	vyřešený
a	a	k8xC	a
odejde	odejít	k5eAaPmIp3nS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Tintin	Tintin	k1gInSc1	Tintin
však	však	k9	však
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
modla	modla	k1gFnSc1	modla
je	být	k5eAaImIp3nS	být
falešná	falešný	k2eAgFnSc1d1	falešná
<g/>
(	(	kIx(	(
<g/>
protože	protože	k8xS	protože
nemá	mít	k5eNaImIp3nS	mít
nalomené	nalomený	k2eAgNnSc1d1	nalomené
pravé	pravý	k2eAgNnSc1d1	pravé
ucho	ucho	k1gNnSc1	ucho
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
originál	originál	k1gInSc1	originál
má	mít	k5eAaImIp3nS	mít
kousek	kousek	k1gInSc4	kousek
poškozený	poškozený	k2eAgInSc4d1	poškozený
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
z	z	k7c2	z
novin	novina	k1gFnPc2	novina
se	se	k3xPyFc4	se
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Londýnské	londýnský	k2eAgFnSc6d1	londýnská
ulici	ulice	k1gFnSc6	ulice
prý	prý	k9	prý
zemřel	zemřít	k5eAaPmAgMnS	zemřít
známý	známý	k2eAgMnSc1d1	známý
sochař	sochař	k1gMnSc1	sochař
Balthazar	Balthazar	k1gMnSc1	Balthazar
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
specializoval	specializovat	k5eAaBmAgMnS	specializovat
na	na	k7c4	na
exotické	exotický	k2eAgFnPc4d1	exotická
sošky	soška	k1gFnPc4	soška
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
usilovném	usilovný	k2eAgNnSc6d1	usilovné
přemýšlení	přemýšlení	k1gNnSc6	přemýšlení
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
do	do	k7c2	do
ulice	ulice	k1gFnSc2	ulice
vypraví	vypravit	k5eAaPmIp3nS	vypravit
a	a	k8xC	a
po	po	k7c6	po
prohledání	prohledání	k1gNnSc6	prohledání
pokoje	pokoj	k1gInSc2	pokoj
sochaře	sochař	k1gMnSc2	sochař
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
sochař	sochař	k1gMnSc1	sochař
byl	být	k5eAaImAgMnS	být
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oficiální	oficiální	k2eAgFnSc1d1	oficiální
verze	verze	k1gFnSc1	verze
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sochař	sochař	k1gMnSc1	sochař
spáchal	spáchat	k5eAaPmAgMnS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
když	když	k8xS	když
vtom	vtom	k6eAd1	vtom
ho	on	k3xPp3gMnSc4	on
napadne	napadnout	k5eAaPmIp3nS	napadnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
koupit	koupit	k5eAaPmF	koupit
sochařova	sochařův	k2eAgMnSc4d1	sochařův
papouška	papoušek	k1gMnSc4	papoušek
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
jediného	jediný	k2eAgMnSc2d1	jediný
svědka	svědek	k1gMnSc2	svědek
sochařovy	sochařův	k2eAgFnSc2d1	sochařova
smrti	smrt	k1gFnSc2	smrt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
doběhnutí	doběhnutí	k1gNnSc6	doběhnutí
do	do	k7c2	do
ulice	ulice	k1gFnSc2	ulice
však	však	k9	však
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
papouška	papoušek	k1gMnSc2	papoušek
si	se	k3xPyFc3	se
před	před	k7c7	před
minutou	minuta	k1gFnSc7	minuta
koupil	koupit	k5eAaPmAgMnS	koupit
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
potkal	potkat	k5eAaPmAgInS	potkat
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
když	když	k8xS	když
odcházel	odcházet	k5eAaImAgMnS	odcházet
<g/>
.	.	kIx.	.
</s>
<s>
Rychle	rychle	k6eAd1	rychle
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
běží	běžet	k5eAaImIp3nS	běžet
<g/>
,	,	kIx,	,
muž	muž	k1gMnSc1	muž
se	se	k3xPyFc4	se
však	však	k9	však
popere	poprat	k5eAaPmIp3nS	poprat
s	s	k7c7	s
jedním	jeden	k4xCgMnSc7	jeden
pánem	pán	k1gMnSc7	pán
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
papoušek	papoušek	k1gMnSc1	papoušek
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
krákal	krákat	k5eAaImAgInS	krákat
neslušná	slušný	k2eNgNnPc4d1	neslušné
slova	slovo	k1gNnPc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rvačce	rvačka	k1gFnSc6	rvačka
se	se	k3xPyFc4	se
krabice	krabice	k1gFnSc1	krabice
rozpadne	rozpadnout	k5eAaPmIp3nS	rozpadnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
papoušek	papoušek	k1gMnSc1	papoušek
uletí	uletět	k5eAaPmIp3nS	uletět
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
Tintinovi	Tintin	k1gMnSc3	Tintin
poděkuje	poděkovat	k5eAaPmIp3nS	poděkovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Tintin	Tintin	k1gMnSc1	Tintin
si	se	k3xPyFc3	se
všimne	všimnout	k5eAaPmIp3nS	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
muž	muž	k1gMnSc1	muž
lže	lhát	k5eAaImIp3nS	lhát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
dárek	dárek	k1gInSc1	dárek
od	od	k7c2	od
strýčka	strýček	k1gMnSc2	strýček
<g/>
.	.	kIx.	.
</s>
<s>
Tintina	Tintina	k1gFnSc1	Tintina
překvapí	překvapit	k5eAaPmIp3nS	překvapit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
muž	muž	k1gMnSc1	muž
lže	lhát	k5eAaImIp3nS	lhát
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
neznámý	známý	k2eNgMnSc1d1	neznámý
muž	muž	k1gMnSc1	muž
i	i	k8xC	i
Tintin	Tintin	k1gInSc1	Tintin
podají	podat	k5eAaPmIp3nP	podat
inzerát	inzerát	k1gInSc4	inzerát
do	do	k7c2	do
novin	novina	k1gFnPc2	novina
"	"	kIx"	"
<g/>
ztratil	ztratit	k5eAaPmAgMnS	ztratit
se	se	k3xPyFc4	se
papoušek	papoušek	k1gMnSc1	papoušek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Papouška	Papoušek	k1gMnSc4	Papoušek
nakonec	nakonec	k6eAd1	nakonec
získá	získat	k5eAaPmIp3nS	získat
Tintin	Tintin	k1gInSc1	Tintin
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
onen	onen	k3xDgMnSc1	onen
neznámý	známý	k2eNgMnSc1d1	neznámý
muž	muž	k1gMnSc1	muž
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ho	on	k3xPp3gNnSc4	on
pokusí	pokusit	k5eAaPmIp3nS	pokusit
ukrást	ukrást	k5eAaPmF	ukrást
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
málem	málem	k6eAd1	málem
Tintina	Tintina	k1gFnSc1	Tintina
usmrtí	usmrtit	k5eAaPmIp3nS	usmrtit
nožem	nůž	k1gInSc7	nůž
<g/>
.	.	kIx.	.
</s>
<s>
Tintinovi	Tintin	k1gMnSc3	Tintin
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakém	jaký	k3yRgInSc6	jaký
domě	dům	k1gInSc6	dům
muž	muž	k1gMnSc1	muž
bydlí	bydlet	k5eAaImIp3nS	bydlet
<g/>
.	.	kIx.	.
</s>
<s>
Jedné	jeden	k4xCgFnSc6	jeden
noci	noc	k1gFnSc6	noc
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
vypraví	vypravit	k5eAaPmIp3nS	vypravit
<g/>
,	,	kIx,	,
a	a	k8xC	a
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
muž	muž	k1gMnSc1	muž
má	mít	k5eAaImIp3nS	mít
společníka	společník	k1gMnSc4	společník
jménem	jméno	k1gNnSc7	jméno
Alonso	Alonsa	k1gFnSc5	Alonsa
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Ramón	Ramón	k1gInSc1	Ramón
<g/>
.	.	kIx.	.
</s>
<s>
Zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
věhlasného	věhlasný	k2eAgMnSc2d1	věhlasný
sochaře	sochař	k1gMnSc2	sochař
zabil	zabít	k5eAaPmAgMnS	zabít
muž	muž	k1gMnSc1	muž
jménem	jméno	k1gNnSc7	jméno
Tortilla	Tortilla	k1gMnSc1	Tortilla
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
Alonsa	Alonsa	k1gFnSc1	Alonsa
s	s	k7c7	s
Ramónem	Ramón	k1gInSc7	Ramón
až	až	k9	až
do	do	k7c2	do
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
lodi	loď	k1gFnSc2	loď
Město	město	k1gNnSc1	město
Lyon	Lyon	k1gInSc1	Lyon
<g/>
.	.	kIx.	.
</s>
<s>
Alonso	Alonsa	k1gFnSc5	Alonsa
a	a	k8xC	a
Ramón	Ramón	k1gInSc4	Ramón
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
snaži	snazat	k5eAaPmIp1nS	snazat
Tintina	Tintina	k1gFnSc1	Tintina
objevit	objevit	k5eAaPmF	objevit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
spletou	splést	k5eAaPmIp3nP	splést
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
jednu	jeden	k4xCgFnSc4	jeden
noc	noc	k1gFnSc4	noc
zavraždí	zavraždit	k5eAaPmIp3nP	zavraždit
Tortillu	Tortill	k1gInSc3	Tortill
a	a	k8xC	a
hodí	hodit	k5eAaPmIp3nS	hodit
ho	on	k3xPp3gNnSc4	on
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
dej	dát	k5eAaPmRp2nS	dát
ale	ale	k9	ale
Tintin	Tintin	k1gInSc4	Tintin
odhalí	odhalit	k5eAaPmIp3nS	odhalit
<g/>
,	,	kIx,	,
a	a	k8xC	a
nechá	nechat	k5eAaPmIp3nS	nechat
je	on	k3xPp3gInPc4	on
zatknout	zatknout	k5eAaPmF	zatknout
vládním	vládní	k2eAgNnSc7d1	vládní
vojskem	vojsko	k1gNnSc7	vojsko
republiky	republika	k1gFnSc2	republika
San	San	k1gFnSc2	San
Teodoros	Teodorosa	k1gFnPc2	Teodorosa
<g/>
.	.	kIx.	.
</s>
<s>
Netuší	tušit	k5eNaImIp3nS	tušit
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
plukovník	plukovník	k1gMnSc1	plukovník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
přijede	přijet	k5eAaPmIp3nS	přijet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gMnSc1	jejich
dobrý	dobrý	k2eAgMnSc1d1	dobrý
přítel	přítel	k1gMnSc1	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Tintina	Tintin	k1gMnSc4	Tintin
je	být	k5eAaImIp3nS	být
nachystána	nachystán	k2eAgFnSc1d1	nachystána
léčka	léčka	k1gFnSc1	léčka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
skončí	skončit	k5eAaPmIp3nS	skončit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Tintin	Tintin	k1gInSc1	Tintin
je	být	k5eAaImIp3nS	být
zavřen	zavřít	k5eAaPmNgInS	zavřít
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
jako	jako	k8xC	jako
atentátník	atentátník	k1gMnSc1	atentátník
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
je	být	k5eAaImIp3nS	být
připravena	připraven	k2eAgFnSc1d1	připravena
poprava	poprava	k1gFnSc1	poprava
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
popravy	poprava	k1gFnSc2	poprava
je	být	k5eAaImIp3nS	být
však	však	k9	však
svržen	svržen	k2eAgMnSc1d1	svržen
vládnoucí	vládnoucí	k2eAgMnSc1d1	vládnoucí
generál	generál	k1gMnSc1	generál
Tapioca	Tapioca	k1gMnSc1	Tapioca
<g/>
,	,	kIx,	,
a	a	k8xC	a
vládu	vláda	k1gFnSc4	vláda
převezme	převzít	k5eAaPmIp3nS	převzít
generál	generál	k1gMnSc1	generál
Alcázar	Alcázar	k1gMnSc1	Alcázar
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jenom	jenom	k9	jenom
na	na	k7c4	na
půl	půl	k1xP	půl
hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
přichystána	přichystán	k2eAgFnSc1d1	přichystána
poprava	poprava	k1gFnSc1	poprava
<g/>
.	.	kIx.	.
</s>
<s>
Pušky	puška	k1gFnPc1	puška
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
porouchají	porouchat	k5eAaPmIp3nP	porouchat
<g/>
,	,	kIx,	,
a	a	k8xC	a
plukovník	plukovník	k1gMnSc1	plukovník
pozve	pozvat	k5eAaPmIp3nS	pozvat
Tintina	Tintin	k1gMnSc4	Tintin
na	na	k7c4	na
kapku	kapka	k1gFnSc4	kapka
whisky	whisky	k1gFnSc2	whisky
<g/>
.	.	kIx.	.
</s>
<s>
Tintin	Tintin	k1gMnSc1	Tintin
se	se	k3xPyFc4	se
i	i	k9	i
s	s	k7c7	s
plukovníkem	plukovník	k1gMnSc7	plukovník
opije	opít	k5eAaPmIp3nS	opít
a	a	k8xC	a
v	v	k7c6	v
opilosti	opilost	k1gFnSc6	opilost
začne	začít	k5eAaPmIp3nS	začít
při	při	k7c6	při
popravě	poprava	k1gFnSc6	poprava
vykřikovat	vykřikovat	k5eAaImF	vykřikovat
<g/>
:	:	kIx,	:
Ať	ať	k9	ať
žije	žít	k5eAaImIp3nS	žít
generál	generál	k1gMnSc1	generál
Alcázar	Alcázar	k1gMnSc1	Alcázar
<g/>
!	!	kIx.	!
</s>
<s>
V	v	k7c6	v
téže	tenže	k3xDgFnSc6	tenže
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
revoluce	revoluce	k1gFnSc1	revoluce
a	a	k8xC	a
Tintina	Tintina	k1gFnSc1	Tintina
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
generál	generál	k1gMnSc1	generál
svým	svůj	k1gMnSc7	svůj
pobočníkem	pobočník	k1gMnSc7	pobočník
místo	místo	k7c2	místo
plukovníka	plukovník	k1gMnSc2	plukovník
Díaze	Díaze	k1gFnSc2	Díaze
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
degraduje	degradovat	k5eAaBmIp3nS	degradovat
na	na	k7c4	na
desátníka	desátník	k1gMnSc4	desátník
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
přidá	přidat	k5eAaPmIp3nS	přidat
k	k	k7c3	k
hnutí	hnutí	k1gNnSc3	hnutí
odporu	odpor	k1gInSc2	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
Tintinově	Tintinův	k2eAgInSc6d1	Tintinův
pobytu	pobyt	k1gInSc6	pobyt
u	u	k7c2	u
generála	generál	k1gMnSc2	generál
<g/>
,	,	kIx,	,
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
několik	několik	k4yIc4	několik
atentátů	atentát	k1gInPc2	atentát
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
i	i	k9	i
na	na	k7c4	na
generála	generál	k1gMnSc4	generál
<g/>
(	(	kIx(	(
<g/>
atentátníkem	atentátník	k1gMnSc7	atentátník
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
Díaz	Díaza	k1gFnPc2	Díaza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všechny	všechen	k3xTgMnPc4	všechen
přežijí	přežít	k5eAaPmIp3nP	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Generál	generál	k1gMnSc1	generál
ale	ale	k9	ale
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
atentátů	atentát	k1gInPc2	atentát
dostane	dostat	k5eAaPmIp3nS	dostat
psotník	psotník	k1gInSc1	psotník
<g/>
,	,	kIx,	,
a	a	k8xC	a
Tintin	Tintin	k2eAgMnSc1d1	Tintin
de	de	k?	de
facto	facto	k1gNnSc1	facto
převezme	převzít	k5eAaPmIp3nS	převzít
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
zástupce	zástupce	k1gMnSc1	zástupce
společnosti	společnost	k1gFnSc2	společnost
General	General	k1gMnSc1	General
American	American	k1gMnSc1	American
Oil	Oil	k1gMnSc1	Oil
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
mu	on	k3xPp3gMnSc3	on
nabízí	nabízet	k5eAaImIp3nS	nabízet
10	[number]	k4	10
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
když	když	k8xS	když
přiměje	přimět	k5eAaPmIp3nS	přimět
generála	generál	k1gMnSc4	generál
zaútočit	zaútočit	k5eAaPmF	zaútočit
na	na	k7c4	na
sousední	sousední	k2eAgFnSc4d1	sousední
republiku	republika	k1gFnSc4	republika
Novoriko	Novorika	k1gFnSc5	Novorika
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
u	u	k7c2	u
hranic	hranice	k1gFnPc2	hranice
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Gran	Grana	k1gFnPc2	Grana
Chapo	Chapa	k1gFnSc5	Chapa
se	se	k3xPyFc4	se
údajně	údajně	k6eAd1	údajně
nachází	nacházet	k5eAaImIp3nS	nacházet
ropa	ropa	k1gFnSc1	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
příliš	příliš	k6eAd1	příliš
nelíbí	líbit	k5eNaImIp3nS	líbit
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
svému	svůj	k3xOyFgMnSc3	svůj
sluhovi	sluha	k1gMnSc3	sluha
Pablovi	Pabl	k1gMnSc3	Pabl
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c4	na
Tintina	Tintin	k1gMnSc4	Tintin
spáchal	spáchat	k5eAaPmAgInS	spáchat
atentát	atentát	k1gInSc1	atentát
<g/>
.	.	kIx.	.
</s>
<s>
Atentát	atentát	k1gInSc1	atentát
se	se	k3xPyFc4	se
nepovede	povést	k5eNaPmIp3nS	povést
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Tintin	Tintin	k1gMnSc1	Tintin
Pablovi	Pablův	k2eAgMnPc1d1	Pablův
odpustí	odpustit	k5eAaPmIp3nS	odpustit
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
stane	stanout	k5eAaPmIp3nS	stanout
jeho	jeho	k3xOp3gMnSc7	jeho
věrným	věrný	k2eAgMnSc7d1	věrný
přítelem	přítel	k1gMnSc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
Tintina	Tintin	k1gMnSc4	Tintin
unesou	unést	k5eAaPmIp3nP	unést
Alonso	Alonsa	k1gFnSc5	Alonsa
a	a	k8xC	a
Ramón	Ramón	k1gInSc4	Ramón
a	a	k8xC	a
chtějí	chtít	k5eAaImIp3nP	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jim	on	k3xPp3gFnPc3	on
vyzradil	vyzradit	k5eAaPmAgMnS	vyzradit
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
ukryta	ukryt	k2eAgFnSc1d1	ukryta
pravá	pravý	k2eAgFnSc1d1	pravá
modla	modla	k1gFnSc1	modla
<g/>
.	.	kIx.	.
</s>
<s>
Tintin	Tintin	k1gMnSc1	Tintin
jim	on	k3xPp3gFnPc3	on
nalže	nalhat	k5eAaPmIp3nS	nalhat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
lodi	loď	k1gFnSc2	loď
<g/>
(	(	kIx(	(
<g/>
sám	sám	k3xTgMnSc1	sám
netuší	tušit	k5eNaImIp3nS	tušit
kde	kde	k6eAd1	kde
modla	modla	k1gFnSc1	modla
je	být	k5eAaImIp3nS	být
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alonso	Alonsa	k1gFnSc5	Alonsa
a	a	k8xC	a
Ramón	Ramón	k1gInSc1	Ramón
ho	on	k3xPp3gNnSc4	on
chtějí	chtít	k5eAaImIp3nP	chtít
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
během	během	k7c2	během
toho	ten	k3xDgNnSc2	ten
prolétne	prolétnout	k5eAaPmIp3nS	prolétnout
místností	místnost	k1gFnPc2	místnost
kulový	kulový	k2eAgInSc4d1	kulový
blesk	blesk	k1gInSc4	blesk
a	a	k8xC	a
pomůže	pomoct	k5eAaPmIp3nS	pomoct
Tintinovi	Tintin	k1gMnSc3	Tintin
k	k	k7c3	k
útěku	útěk	k1gInSc3	útěk
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
zástupce	zástupce	k1gMnSc1	zástupce
společnosti	společnost	k1gFnSc2	společnost
General	General	k1gMnSc1	General
American	American	k1gMnSc1	American
Oil	Oil	k1gMnSc1	Oil
přemluví	přemluvit	k5eAaPmIp3nS	přemluvit
generála	generál	k1gMnSc4	generál
Alcázara	Alcázar	k1gMnSc4	Alcázar
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
na	na	k7c4	na
sousední	sousední	k2eAgFnSc4d1	sousední
republiku	republika	k1gFnSc4	republika
a	a	k8xC	a
nevěřil	věřit	k5eNaImAgMnS	věřit
Tintinovi	Tintin	k1gMnSc3	Tintin
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
vlády	vláda	k1gFnSc2	vláda
obou	dva	k4xCgInPc2	dva
dvou	dva	k4xCgInPc2	dva
států	stát	k1gInPc2	stát
obletí	obletět	k5eAaPmIp3nS	obletět
obchodník	obchodník	k1gMnSc1	obchodník
se	s	k7c7	s
zbraněmi	zbraň	k1gFnPc7	zbraň
Basil	Basil	k1gMnSc1	Basil
Bazaroff	Bazaroff	k1gMnSc1	Bazaroff
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
přítelem	přítel	k1gMnSc7	přítel
zástupce	zástupce	k1gMnSc2	zástupce
<g/>
,	,	kIx,	,
a	a	k8xC	a
podaří	podařit	k5eAaPmIp3nS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
prodat	prodat	k5eAaPmF	prodat
150	[number]	k4	150
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
120	[number]	k4	120
000	[number]	k4	000
niklovaných	niklovaný	k2eAgInPc2d1	niklovaný
granátů	granát	k1gInPc2	granát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
získá	získat	k5eAaPmIp3nS	získat
také	také	k9	také
listinu	listina	k1gFnSc4	listina
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
které	který	k3yRgFnSc3	který
je	být	k5eAaImIp3nS	být
Tintin	Tintin	k2eAgMnSc1d1	Tintin
generálem	generál	k1gMnSc7	generál
poslán	poslat	k5eAaPmNgMnS	poslat
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Pablo	Pablo	k1gNnSc1	Pablo
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
pomůže	pomoct	k5eAaPmIp3nS	pomoct
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
,	,	kIx,	,
a	a	k8xC	a
Tintin	Tintin	k1gInSc1	Tintin
prchá	prchat	k5eAaImIp3nS	prchat
automobilem	automobil	k1gInSc7	automobil
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
státní	státní	k2eAgFnSc3d1	státní
hranici	hranice	k1gFnSc3	hranice
s	s	k7c7	s
Novorikem	Novorik	k1gMnSc7	Novorik
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
ho	on	k3xPp3gMnSc4	on
začne	začít	k5eAaPmIp3nS	začít
pronásledovat	pronásledovat	k5eAaImF	pronásledovat
obrněný	obrněný	k2eAgInSc4d1	obrněný
vůz	vůz	k1gInSc4	vůz
<g/>
,	,	kIx,	,
s	s	k7c7	s
několika	několik	k4yIc7	několik
vojáky	voják	k1gMnPc7	voják
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
Tintin	Tintin	k1gInSc1	Tintin
obelstí	obelstít	k5eAaPmIp3nS	obelstít
a	a	k8xC	a
unikne	uniknout	k5eAaPmIp3nS	uniknout
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
autě	auto	k1gNnSc6	auto
<g/>
.	.	kIx.	.
</s>
<s>
Naneštěstí	naneštěstí	k9	naneštěstí
projede	projet	k5eAaPmIp3nS	projet
vojenským	vojenský	k2eAgNnSc7d1	vojenské
autem	auto	k1gNnSc7	auto
do	do	k7c2	do
sousední	sousední	k2eAgFnSc2d1	sousední
republiky	republika	k1gFnSc2	republika
Novoriko	Novorika	k1gFnSc5	Novorika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
zatknou	zatknout	k5eAaPmIp3nP	zatknout
<g/>
.	.	kIx.	.
</s>
<s>
Republika	republika	k1gFnSc1	republika
Novoriko	Novorika	k1gFnSc5	Novorika
<g/>
,	,	kIx,	,
domnívajíc	domnívat	k5eAaImSgFnS	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Tintin	Tintin	k1gInSc1	Tintin
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
<g/>
,	,	kIx,	,
vyhlásí	vyhlásit	k5eAaPmIp3nS	vyhlásit
válku	válka	k1gFnSc4	válka
San	San	k1gFnSc2	San
Teodorosu	Teodoros	k1gInSc2	Teodoros
a	a	k8xC	a
rozhoří	rozhořet	k5eAaPmIp3nS	rozhořet
se	se	k3xPyFc4	se
válka	válka	k1gFnSc1	válka
o	o	k7c4	o
Gran	Gran	k1gInSc4	Gran
Chapo	Chapa	k1gFnSc5	Chapa
<g/>
.	.	kIx.	.
</s>
<s>
Tintin	Tintin	k1gInSc1	Tintin
uteče	utéct	k5eAaPmIp3nS	utéct
z	z	k7c2	z
vozu	vůz	k1gInSc2	vůz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ho	on	k3xPp3gMnSc4	on
veze	vézt	k5eAaImIp3nS	vézt
do	do	k7c2	do
věznice	věznice	k1gFnSc2	věznice
a	a	k8xC	a
po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
nesnázích	nesnáz	k1gFnPc6	nesnáz
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
až	až	k9	až
k	k	k7c3	k
Arumbajům	Arumbaj	k1gMnPc3	Arumbaj
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jistý	jistý	k2eAgInSc4d1	jistý
mišenec	mišenec	k1gInSc4	mišenec
jménem	jméno	k1gNnSc7	jméno
Lopéz	Lopéza	k1gFnPc2	Lopéza
<g/>
,	,	kIx,	,
jim	on	k3xPp3gInPc3	on
ukradl	ukradnout	k5eAaPmAgInS	ukradnout
diamant	diamant	k1gInSc1	diamant
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ukryl	ukrýt	k5eAaPmAgInS	ukrýt
do	do	k7c2	do
modly	modla	k1gFnSc2	modla
<g/>
.	.	kIx.	.
</s>
<s>
Jede	jet	k5eAaImIp3nS	jet
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
civilizace	civilizace	k1gFnSc2	civilizace
na	na	k7c6	na
kánoi	kánoe	k1gFnSc6	kánoe
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
potká	potkat	k5eAaPmIp3nS	potkat
se	se	k3xPyFc4	se
s	s	k7c7	s
Alonsem	Alons	k1gInSc7	Alons
a	a	k8xC	a
Ramónem	Ramón	k1gInSc7	Ramón
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
sice	sice	k8xC	sice
porazí	porazit	k5eAaPmIp3nP	porazit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
špatně	špatně	k6eAd1	špatně
jim	on	k3xPp3gMnPc3	on
sváže	svázat	k5eAaPmIp3nS	svázat
ruce	ruka	k1gFnPc4	ruka
<g/>
,	,	kIx,	,
a	a	k8xC	a
Alonso	Alonsa	k1gFnSc5	Alonsa
ho	on	k3xPp3gMnSc4	on
svrhne	svrhnout	k5eAaPmIp3nS	svrhnout
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Tintin	Tintin	k1gInSc1	Tintin
nakonec	nakonec	k6eAd1	nakonec
dojde	dojít	k5eAaPmIp3nS	dojít
pěšky	pěšky	k6eAd1	pěšky
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
oba	dva	k4xCgMnPc1	dva
zločinci	zločinec	k1gMnPc1	zločinec
mají	mít	k5eAaImIp3nP	mít
týden	týden	k1gInSc4	týden
náskok	náskok	k1gInSc1	náskok
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zjistí	zjistit	k5eAaPmIp3nS	zjistit
že	že	k8xS	že
někdo	někdo	k3yInSc1	někdo
začal	začít	k5eAaPmAgMnS	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
sošky	soška	k1gFnPc4	soška
sériově	sériově	k6eAd1	sériově
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
po	po	k7c6	po
stopě	stopa	k1gFnSc6	stopa
a	a	k8xC	a
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
originál	originál	k1gInSc1	originál
sošky	soška	k1gFnSc2	soška
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
bohatý	bohatý	k2eAgMnSc1d1	bohatý
Američan	Američan	k1gMnSc1	Američan
Goldwood	Goldwooda	k1gFnPc2	Goldwooda
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
odjel	odjet	k5eAaPmAgMnS	odjet
zrovna	zrovna	k6eAd1	zrovna
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
Washington	Washington	k1gInSc1	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Doletí	doletět	k5eAaPmIp3nP	doletět
k	k	k7c3	k
lodi	loď	k1gFnSc3	loď
na	na	k7c6	na
hydroplánu	hydroplán	k1gInSc6	hydroplán
s	s	k7c7	s
poštou	pošta	k1gFnSc7	pošta
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
utíka	utíka	k6eAd1	utíka
do	do	k7c2	do
Goldwoodovy	Goldwoodův	k2eAgFnSc2d1	Goldwoodův
kajuty	kajuta	k1gFnSc2	kajuta
<g/>
.	.	kIx.	.
</s>
<s>
Setká	setkat	k5eAaPmIp3nS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
Alonsem	Alons	k1gMnSc7	Alons
a	a	k8xC	a
Ramónem	Ramón	k1gMnSc7	Ramón
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zrovna	zrovna	k6eAd1	zrovna
ukradli	ukradnout	k5eAaPmAgMnP	ukradnout
sošku	soška	k1gFnSc4	soška
<g/>
.	.	kIx.	.
</s>
<s>
Popere	poprat	k5eAaPmIp3nS	poprat
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
<g/>
,	,	kIx,	,
soška	soška	k1gFnSc1	soška
se	se	k3xPyFc4	se
rozbije	rozbít	k5eAaPmIp3nS	rozbít
a	a	k8xC	a
diamant	diamant	k1gInSc1	diamant
spadne	spadnout	k5eAaPmIp3nS	spadnout
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgMnPc1	tři
spadnou	spadnout	k5eAaPmIp3nP	spadnout
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Tintin	Tintin	k1gMnSc1	Tintin
přežije	přežít	k5eAaPmIp3nS	přežít
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
oba	dva	k4xCgMnPc1	dva
dva	dva	k4xCgMnPc1	dva
zločinci	zločinec	k1gMnPc1	zločinec
se	se	k3xPyFc4	se
utopí	utopit	k5eAaPmIp3nP	utopit
<g/>
.	.	kIx.	.
</s>
<s>
Vrátí	vrátit	k5eAaPmIp3nS	vrátit
sošku	soška	k1gFnSc4	soška
do	do	k7c2	do
muzea	muzeum	k1gNnSc2	muzeum
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
vstříc	vstříc	k6eAd1	vstříc
dalším	další	k2eAgNnSc7d1	další
dobrodružstvím	dobrodružství	k1gNnSc7	dobrodružství
<g/>
.	.	kIx.	.
</s>
</p>
