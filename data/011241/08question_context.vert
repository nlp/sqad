<s>
Berenguela	Berenguela	k1gFnSc1	Berenguela
Kastilská	kastilský	k2eAgFnSc1d1	Kastilská
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1180	[number]	k4	1180
<g/>
,	,	kIx,	,
Segovia	Segovia	k1gFnSc1	Segovia
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1246	[number]	k4	1246
<g/>
,	,	kIx,	,
Burgos	Burgos	k1gInSc1	Burgos
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
leónská	leónský	k2eAgFnSc1d1	leónský
a	a	k8xC	a
kastilská	kastilský	k2eAgFnSc1d1	Kastilská
královna	královna	k1gFnSc1	královna
<g/>
.	.	kIx.	.
</s>
<s>
Pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
burgundsko-Ivrejské	burgundsko-Ivrejský	k2eAgFnSc2d1	burgundsko-Ivrejský
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
Alfons	Alfons	k1gMnSc1	Alfons
IX	IX	kA	IX
<g/>
.	.	kIx.	.
</s>
