<p>
<s>
Berenguela	Berenguela	k1gFnSc1	Berenguela
Kastilská	kastilský	k2eAgFnSc1d1	Kastilská
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1180	[number]	k4	1180
<g/>
,	,	kIx,	,
Segovia	Segovia	k1gFnSc1	Segovia
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1246	[number]	k4	1246
<g/>
,	,	kIx,	,
Burgos	Burgos	k1gInSc1	Burgos
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
leónská	leónský	k2eAgFnSc1d1	leónský
a	a	k8xC	a
kastilská	kastilský	k2eAgFnSc1d1	Kastilská
královna	královna	k1gFnSc1	královna
<g/>
.	.	kIx.	.
</s>
<s>
Pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
burgundsko-Ivrejské	burgundsko-Ivrejský	k2eAgFnSc2d1	burgundsko-Ivrejský
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
Alfons	Alfons	k1gMnSc1	Alfons
IX	IX	kA	IX
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Berenguela	Berenguela	k1gFnSc1	Berenguela
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
potomek	potomek	k1gMnSc1	potomek
kastilského	kastilský	k2eAgMnSc2d1	kastilský
krále	král	k1gMnSc2	král
Alfonse	Alfons	k1gMnSc2	Alfons
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
a	a	k8xC	a
anglické	anglický	k2eAgFnSc2d1	anglická
princezny	princezna	k1gFnSc2	princezna
Eleonory	Eleonora	k1gFnSc2	Eleonora
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnSc2	dcera
slavné	slavný	k2eAgFnSc2d1	slavná
krásky	kráska	k1gFnSc2	kráska
Eleonory	Eleonora	k1gFnSc2	Eleonora
Akvitánské	Akvitánský	k2eAgFnSc2d1	Akvitánská
a	a	k8xC	a
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Plantageneta	Plantagenet	k1gMnSc2	Plantagenet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
manželství	manželství	k1gNnSc1	manželství
mladé	mladý	k2eAgFnSc2d1	mladá
princezny	princezna	k1gFnSc2	princezna
bylo	být	k5eAaImAgNnS	být
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1188	[number]	k4	1188
s	s	k7c7	s
Konrádem	Konrád	k1gMnSc7	Konrád
Švábským	švábský	k2eAgMnSc7d1	švábský
<g/>
,	,	kIx,	,
synem	syn	k1gMnSc7	syn
císaře	císař	k1gMnSc2	císař
Barbarossy	Barbarossa	k1gMnSc2	Barbarossa
<g/>
.	.	kIx.	.
</s>
<s>
Sňatek	sňatek	k1gInSc1	sňatek
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nevěstině	nevěstin	k2eAgFnSc3d1	nevěstina
věku	věk	k1gInSc6	věk
zkonzumován	zkonzumovat	k5eAaPmNgInS	zkonzumovat
a	a	k8xC	a
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodu	důvod	k1gInSc2	důvod
jej	on	k3xPp3gMnSc4	on
papež	papež	k1gMnSc1	papež
roku	rok	k1gInSc2	rok
1191	[number]	k4	1191
zrušil	zrušit	k5eAaPmAgMnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc4	druhý
manželství	manželství	k1gNnSc4	manželství
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
Berenguela	Berenguela	k1gFnSc1	Berenguela
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
osmnácti	osmnáct	k4xCc6	osmnáct
roku	rok	k1gInSc2	rok
1198	[number]	k4	1198
s	s	k7c7	s
čerstvě	čerstvě	k6eAd1	čerstvě
rozvedeným	rozvedený	k2eAgMnSc7d1	rozvedený
leonským	leonský	k2eAgMnSc7d1	leonský
králem	král	k1gMnSc7	král
Alfonsem	Alfons	k1gMnSc7	Alfons
<g/>
.	.	kIx.	.
</s>
<s>
Alfonsovo	Alfonsův	k2eAgNnSc1d1	Alfonsovo
manželství	manželství	k1gNnSc1	manželství
s	s	k7c7	s
Terezou	Tereza	k1gFnSc7	Tereza
Portugalskou	portugalský	k2eAgFnSc7d1	portugalská
bylo	být	k5eAaImAgNnS	být
rozloučeno	rozloučit	k5eAaPmNgNnS	rozloučit
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
blízké	blízký	k2eAgFnSc2d1	blízká
příbuznosti	příbuznost	k1gFnSc2	příbuznost
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
se	se	k3xPyFc4	se
však	však	k9	však
opakovala	opakovat	k5eAaImAgFnS	opakovat
a	a	k8xC	a
Berenguela	Berenguela	k1gFnSc1	Berenguela
byla	být	k5eAaImAgFnS	být
rozvedena	rozvést	k5eAaPmNgFnS	rozvést
z	z	k7c2	z
téhož	týž	k3xTgInSc2	týž
důvodu	důvod	k1gInSc2	důvod
pouhých	pouhý	k2eAgNnPc2d1	pouhé
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1217	[number]	k4	1217
Berenguelin	Berenguelina	k1gFnPc2	Berenguelina
bratr	bratr	k1gMnSc1	bratr
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Kastilie	Kastilie	k1gFnSc2	Kastilie
Jindřich	Jindřich	k1gMnSc1	Jindřich
I.	I.	kA	I.
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
při	při	k7c6	při
nehodě	nehoda	k1gFnSc6	nehoda
na	na	k7c6	na
lovu	lov	k1gInSc6	lov
a	a	k8xC	a
Berenguela	Berenguela	k1gFnSc1	Berenguela
jako	jako	k8xC	jako
prvorozená	prvorozený	k2eAgFnSc1d1	prvorozená
získala	získat	k5eAaPmAgFnS	získat
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
vzdala	vzdát	k5eAaPmAgFnS	vzdát
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
<g/>
.	.	kIx.	.
</s>
<s>
Zůstala	zůstat	k5eAaPmAgFnS	zůstat
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
jako	jako	k8xC	jako
synův	synův	k2eAgMnSc1d1	synův
rádce	rádce	k1gMnSc1	rádce
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1219	[number]	k4	1219
mu	on	k3xPp3gNnSc3	on
pomohla	pomoct	k5eAaPmAgFnS	pomoct
s	s	k7c7	s
ženitbou	ženitba	k1gFnSc7	ženitba
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
partii	partie	k1gFnSc4	partie
mu	on	k3xPp3gMnSc3	on
vyhledala	vyhledat	k5eAaPmAgFnS	vyhledat
Alžbětu	Alžběta	k1gFnSc4	Alžběta
Štaufskou	Štaufský	k2eAgFnSc4d1	Štaufská
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
zavražděného	zavražděný	k2eAgMnSc2d1	zavražděný
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
Filipa	Filip	k1gMnSc2	Filip
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
bývalého	bývalý	k2eAgMnSc2d1	bývalý
manžela	manžel	k1gMnSc2	manžel
roku	rok	k1gInSc2	rok
1230	[number]	k4	1230
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
angažovala	angažovat	k5eAaBmAgFnS	angažovat
při	při	k7c6	při
jednání	jednání	k1gNnSc6	jednání
s	s	k7c7	s
jeho	jeho	k3xOp3gFnPc7	jeho
dvěma	dva	k4xCgFnPc7	dva
dcerami	dcera	k1gFnPc7	dcera
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
manželství	manželství	k1gNnSc2	manželství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
za	za	k7c4	za
příslib	příslib	k1gInSc4	příslib
doživotní	doživotní	k2eAgFnSc2d1	doživotní
renty	renta	k1gFnSc2	renta
vzdaly	vzdát	k5eAaPmAgFnP	vzdát
nároku	nárok	k1gInSc2	nárok
na	na	k7c4	na
leonský	leonský	k2eAgInSc4d1	leonský
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgInS	stát
i	i	k9	i
králem	král	k1gMnSc7	král
Leónu	León	k1gInSc2	León
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konec	konec	k1gInSc4	konec
života	život	k1gInSc2	život
strávila	strávit	k5eAaPmAgFnS	strávit
v	v	k7c6	v
rodovém	rodový	k2eAgInSc6d1	rodový
klášteře	klášter	k1gInSc6	klášter
Las	laso	k1gNnPc2	laso
Huelgas	Huelgasa	k1gFnPc2	Huelgasa
společně	společně	k6eAd1	společně
s	s	k7c7	s
mladší	mladý	k2eAgFnSc7d2	mladší
sestrou	sestra	k1gFnSc7	sestra
Eleonorou	Eleonora	k1gFnSc7	Eleonora
a	a	k8xC	a
udržovala	udržovat	k5eAaImAgFnS	udržovat
i	i	k9	i
čilé	čilý	k2eAgInPc4d1	čilý
styky	styk	k1gInPc4	styk
s	s	k7c7	s
další	další	k2eAgFnSc7d1	další
sestrou	sestra	k1gFnSc7	sestra
Blankou	Blanka	k1gFnSc7	Blanka
<g/>
,	,	kIx,	,
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
regentkou	regentka	k1gFnSc7	regentka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
jí	jíst	k5eAaImIp3nS	jíst
také	také	k9	také
pomohla	pomoct	k5eAaPmAgFnS	pomoct
najít	najít	k5eAaPmF	najít
pro	pro	k7c4	pro
Ferdinanda	Ferdinand	k1gMnSc4	Ferdinand
novou	nový	k2eAgFnSc4d1	nová
choť	choť	k1gFnSc4	choť
–	–	k?	–
Johanku	Johanka	k1gFnSc4	Johanka
z	z	k7c2	z
Ponthieu	Ponthieus	k1gInSc2	Ponthieus
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1246	[number]	k4	1246
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
pohřbena	pohřbít	k5eAaPmNgFnS	pohřbít
společně	společně	k6eAd1	společně
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
v	v	k7c6	v
Las	laso	k1gNnPc2	laso
Huelgas	Huelgasa	k1gFnPc2	Huelgasa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Berenguela	Berenguela	k1gFnSc1	Berenguela
Kastilská	kastilský	k2eAgFnSc1d1	Kastilská
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Genealogie	genealogie	k1gFnSc1	genealogie
</s>
</p>
<p>
<s>
Polštář	polštář	k1gInSc1	polštář
z	z	k7c2	z
Berenguelina	Berenguelin	k2eAgInSc2d1	Berenguelin
hrobu	hrob	k1gInSc2	hrob
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc1	vývod
z	z	k7c2	z
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
