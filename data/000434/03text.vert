<s>
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Jiřího	Jiří	k1gMnSc2	Jiří
Ortena	Orten	k1gMnSc2	Orten
je	být	k5eAaImIp3nS	být
všeobecné	všeobecný	k2eAgNnSc1d1	všeobecné
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
,	,	kIx,	,
sídlící	sídlící	k2eAgFnSc1d1	sídlící
ve	v	k7c6	v
středočeském	středočeský	k2eAgNnSc6d1	Středočeské
městě	město	k1gNnSc6	město
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
Prvopočátky	prvopočátek	k1gInPc1	prvopočátek
gymnaziálního	gymnaziální	k2eAgNnSc2d1	gymnaziální
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
lze	lze	k6eAd1	lze
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
vysledovat	vysledovat	k5eAaPmF	vysledovat
až	až	k9	až
do	do	k7c2	do
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
pozdější	pozdní	k2eAgFnSc2d2	pozdější
existence	existence	k1gFnSc2	existence
jezuitského	jezuitský	k2eAgNnSc2d1	jezuitské
gymnázia	gymnázium	k1gNnSc2	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
jeho	jeho	k3xOp3gFnSc4	jeho
zrušení	zrušení	k1gNnSc1	zrušení
bylo	být	k5eAaImAgNnS	být
nahrazenou	nahrazený	k2eAgFnSc4d1	nahrazená
c.k.	c.k.	k?	c.k.
hlavní	hlavní	k2eAgFnSc7d1	hlavní
školou	škola	k1gFnSc7	škola
<g/>
,	,	kIx,	,
sídlící	sídlící	k2eAgFnSc7d1	sídlící
v	v	k7c6	v
historické	historický	k2eAgFnSc6d1	historická
budově	budova	k1gFnSc6	budova
Hrádku	Hrádok	k1gInSc2	Hrádok
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1857	[number]	k4	1857
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
vyšší	vysoký	k2eAgFnSc1d2	vyšší
reálná	reálný	k2eAgFnSc1d1	reálná
škola	škola	k1gFnSc1	škola
šestitřídní	šestitřídní	k2eAgFnSc1d1	šestitřídní
-	-	kIx~	-
předchůdce	předchůdce	k1gMnSc1	předchůdce
dnešního	dnešní	k2eAgNnSc2d1	dnešní
gymnázia	gymnázium	k1gNnSc2	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Stávající	stávající	k2eAgNnSc1d1	stávající
umístění	umístění	k1gNnSc1	umístění
školy	škola	k1gFnSc2	škola
na	na	k7c6	na
Hrádku	Hrádok	k1gInSc6	Hrádok
již	již	k6eAd1	již
nebylo	být	k5eNaImAgNnS	být
vyhovující	vyhovující	k2eAgNnSc1d1	vyhovující
a	a	k8xC	a
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
školy	škola	k1gFnSc2	škola
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
nová	nový	k2eAgFnSc1d1	nová
budova	budova	k1gFnSc1	budova
na	na	k7c6	na
Komenského	Komenského	k2eAgNnSc6d1	Komenského
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
za	za	k7c4	za
působení	působení	k1gNnSc4	působení
ředitele	ředitel	k1gMnSc2	ředitel
Josefa	Josef	k1gMnSc2	Josef
Webera	Weber	k1gMnSc2	Weber
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
výuky	výuka	k1gFnSc2	výuka
zavedena	zaveden	k2eAgFnSc1d1	zavedena
čeština	čeština	k1gFnSc1	čeština
a	a	k8xC	a
nahradila	nahradit	k5eAaPmAgFnS	nahradit
doposud	doposud	k6eAd1	doposud
používanou	používaný	k2eAgFnSc4d1	používaná
němčinu	němčina	k1gFnSc4	němčina
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dalším	další	k2eAgInSc7d1	další
růstem	růst	k1gInSc7	růst
školy	škola	k1gFnSc2	škola
městu	město	k1gNnSc3	město
stoupaly	stoupat	k5eAaImAgInP	stoupat
náklady	náklad	k1gInPc4	náklad
a	a	k8xC	a
stávaly	stávat	k5eAaImAgFnP	stávat
se	se	k3xPyFc4	se
neúnosnými	únosný	k2eNgFnPc7d1	neúnosná
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1874	[number]	k4	1874
přešla	přejít	k5eAaPmAgFnS	přejít
škola	škola	k1gFnSc1	škola
ze	z	k7c2	z
správy	správa	k1gFnSc2	správa
města	město	k1gNnSc2	město
pod	pod	k7c4	pod
kontrolu	kontrola	k1gFnSc4	kontrola
státu	stát	k1gInSc2	stát
a	a	k8xC	a
podléhala	podléhat	k5eAaImAgFnS	podléhat
c.k.	c.k.	k?	c.k.
zemské	zemský	k2eAgFnSc3d1	zemská
školní	školní	k2eAgFnSc3d1	školní
radě	rada	k1gFnSc3	rada
a	a	k8xC	a
zemskému	zemský	k2eAgMnSc3d1	zemský
školnímu	školní	k2eAgMnSc3d1	školní
inspektorovi	inspektor	k1gMnSc3	inspektor
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dalším	další	k2eAgNnSc7d1	další
rozšiřováním	rozšiřování	k1gNnSc7	rozšiřování
školy	škola	k1gFnSc2	škola
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
nedostatečnou	dostatečný	k2eNgFnSc7d1	nedostatečná
kapacitou	kapacita	k1gFnSc7	kapacita
tříd	třída	k1gFnPc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
vedení	vedení	k1gNnSc4	vedení
školy	škola	k1gFnSc2	škola
obrátilo	obrátit	k5eAaPmAgNnS	obrátit
na	na	k7c4	na
kutnohorskou	kutnohorský	k2eAgFnSc4d1	Kutnohorská
městskou	městský	k2eAgFnSc4d1	městská
radu	rada	k1gFnSc4	rada
s	s	k7c7	s
žádostí	žádost	k1gFnSc7	žádost
o	o	k7c4	o
rozšíření	rozšíření	k1gNnSc4	rozšíření
stávající	stávající	k2eAgFnSc2d1	stávající
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
výstavbu	výstavba	k1gFnSc4	výstavba
nové	nový	k2eAgFnSc2d1	nová
budovy	budova	k1gFnSc2	budova
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
se	s	k7c7	s
starostou	starosta	k1gMnSc7	starosta
Janem	Jan	k1gMnSc7	Jan
Macháčkem	Macháček	k1gMnSc7	Macháček
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
nového	nový	k2eAgNnSc2d1	nové
sídla	sídlo	k1gNnSc2	sídlo
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1898	[number]	k4	1898
<g/>
-	-	kIx~	-
<g/>
1900	[number]	k4	1900
byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
arch	arch	k1gInSc4	arch
<g/>
.	.	kIx.	.
</s>
<s>
Otakara	Otakar	k1gMnSc2	Otakar
Materny	Materna	k1gMnSc2	Materna
naproti	naproti	k7c3	naproti
chrámu	chrám	k1gInSc3	chrám
sv.	sv.	kA	sv.
Barbory	Barbora	k1gFnSc2	Barbora
postavena	postaven	k2eAgFnSc1d1	postavena
nová	nový	k2eAgFnSc1d1	nová
budova	budova	k1gFnSc1	budova
gymnázia	gymnázium	k1gNnSc2	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1900	[number]	k4	1900
byla	být	k5eAaImAgFnS	být
budova	budova	k1gFnSc1	budova
slavnostně	slavnostně	k6eAd1	slavnostně
vysvěcena	vysvěcen	k2eAgFnSc1d1	vysvěcena
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgInPc1d1	celkový
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
se	se	k3xPyFc4	se
vyšplhaly	vyšplhat	k5eAaPmAgInP	vyšplhat
na	na	k7c4	na
464.993	[number]	k4	464.993
Korun	koruna	k1gFnPc2	koruna
63	[number]	k4	63
haléřů	haléř	k1gInPc2	haléř
<g/>
.	.	kIx.	.
</s>
<s>
Gymnázium	gymnázium	k1gNnSc1	gymnázium
budovu	budova	k1gFnSc4	budova
využívalo	využívat	k5eAaImAgNnS	využívat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
budova	budova	k1gFnSc1	budova
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
běhu	běh	k1gInSc2	běh
školní	školní	k2eAgFnSc2d1	školní
výuky	výuka	k1gFnSc2	výuka
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
budova	budova	k1gFnSc1	budova
gymnázia	gymnázium	k1gNnSc2	gymnázium
obsazena	obsadit	k5eAaPmNgFnS	obsadit
vojsky	vojsky	k6eAd1	vojsky
Rakouska-Uherska	Rakouska-Uherska	k1gFnSc1	Rakouska-Uherska
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
zde	zde	k6eAd1	zde
nejprve	nejprve	k6eAd1	nejprve
umístěni	umístěn	k2eAgMnPc1d1	umístěn
noví	nový	k2eAgMnPc1d1	nový
vojenští	vojenský	k2eAgMnPc1d1	vojenský
rekruti	rekrut	k1gMnPc1	rekrut
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
vojenská	vojenský	k2eAgFnSc1d1	vojenská
nemocnice	nemocnice	k1gFnSc1	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
školy	škola	k1gFnSc2	škola
přesto	přesto	k8xC	přesto
výuku	výuka	k1gFnSc4	výuka
nezrušilo	zrušit	k5eNaPmAgNnS	zrušit
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
hledat	hledat	k5eAaImF	hledat
náhradní	náhradní	k2eAgNnSc4d1	náhradní
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
<g/>
.	.	kIx.	.
</s>
<s>
Studenti	student	k1gMnPc1	student
nakonec	nakonec	k6eAd1	nakonec
nalezli	naleznout	k5eAaPmAgMnP	naleznout
útočiště	útočiště	k1gNnSc4	útočiště
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Dívčí	dívčí	k2eAgFnSc2d1	dívčí
a	a	k8xC	a
obecné	obecný	k2eAgFnSc2d1	obecná
školy	škola	k1gFnSc2	škola
ve	v	k7c6	v
Vlašském	vlašský	k2eAgInSc6d1	vlašský
dvoře	dvůr	k1gInSc6	dvůr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
výuka	výuka	k1gFnSc1	výuka
v	v	k7c6	v
omezených	omezený	k2eAgFnPc6d1	omezená
podmínkách	podmínka	k1gFnPc6	podmínka
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
(	(	kIx(	(
<g/>
vyučovací	vyučovací	k2eAgFnSc1d1	vyučovací
hodina	hodina	k1gFnSc1	hodina
trvala	trvat	k5eAaImAgFnS	trvat
45	[number]	k4	45
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
výuka	výuka	k1gFnSc1	výuka
probíhala	probíhat	k5eAaImAgFnS	probíhat
od	od	k7c2	od
8	[number]	k4	8
hodin	hodina	k1gFnPc2	hodina
ráno	ráno	k6eAd1	ráno
do	do	k7c2	do
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
odpoledne	odpoledne	k1gNnSc2	odpoledne
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
císařská	císařský	k2eAgFnSc1d1	císařská
armáda	armáda	k1gFnSc1	armáda
budovu	budova	k1gFnSc4	budova
gymnázia	gymnázium	k1gNnSc2	gymnázium
opustila	opustit	k5eAaPmAgFnS	opustit
a	a	k8xC	a
vrátila	vrátit	k5eAaPmAgFnS	vrátit
ji	on	k3xPp3gFnSc4	on
vedení	vedení	k1gNnSc2	vedení
školy	škola	k1gFnPc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Výuka	výuka	k1gFnSc1	výuka
tedy	tedy	k9	tedy
mohla	moct	k5eAaImAgFnS	moct
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
původních	původní	k2eAgFnPc6d1	původní
prostorách	prostora	k1gFnPc6	prostora
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
byla	být	k5eAaImAgFnS	být
uzpůsobena	uzpůsobit	k5eAaPmNgFnS	uzpůsobit
probíhajícímu	probíhající	k2eAgInSc3d1	probíhající
válečnému	válečný	k2eAgInSc3d1	válečný
konfliktu	konflikt	k1gInSc3	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
osnov	osnova	k1gFnPc2	osnova
byl	být	k5eAaImAgInS	být
zaveden	zaveden	k2eAgInSc1d1	zaveden
předmět	předmět	k1gInSc1	předmět
"	"	kIx"	"
<g/>
Příprava	příprava	k1gFnSc1	příprava
k	k	k7c3	k
vojenské	vojenský	k2eAgFnSc3d1	vojenská
výchově	výchova	k1gFnSc3	výchova
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
zabrání	zabrání	k1gNnSc2	zabrání
kasáren	kasárny	k1gFnPc2	kasárny
vojskem	vojsko	k1gNnSc7	vojsko
znovu	znovu	k6eAd1	znovu
vyučován	vyučován	k2eAgInSc1d1	vyučován
tělocvik	tělocvik	k1gInSc1	tělocvik
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
předměty	předmět	k1gInPc1	předmět
byly	být	k5eAaImAgInP	být
taktéž	taktéž	k?	taktéž
využity	využít	k5eAaPmNgInP	využít
k	k	k7c3	k
popularizaci	popularizace	k1gFnSc3	popularizace
císařské	císařský	k2eAgFnSc2d1	císařská
rodiny	rodina	k1gFnSc2	rodina
<g/>
:	:	kIx,	:
v	v	k7c6	v
hodinách	hodina	k1gFnPc6	hodina
stylistiky	stylistika	k1gFnSc2	stylistika
například	například	k6eAd1	například
studenti	student	k1gMnPc1	student
zpracovávali	zpracovávat	k5eAaImAgMnP	zpracovávat
životopisy	životopis	k1gInPc4	životopis
členů	člen	k1gMnPc2	člen
císařské	císařský	k2eAgFnSc2d1	císařská
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
zpívala	zpívat	k5eAaImAgFnS	zpívat
rakouská	rakouský	k2eAgFnSc1d1	rakouská
hymna	hymna	k1gFnSc1	hymna
<g/>
.	.	kIx.	.
</s>
<s>
Cenzura	cenzura	k1gFnSc1	cenzura
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
vzestupu	vzestup	k1gInSc6	vzestup
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
mizely	mizet	k5eAaImAgFnP	mizet
české	český	k2eAgFnPc1d1	Česká
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
provedena	provést	k5eAaPmNgFnS	provést
byla	být	k5eAaImAgFnS	být
revize	revize	k1gFnSc1	revize
gymnazijní	gymnazijní	k2eAgFnSc2d1	gymnazijní
knihovny	knihovna	k1gFnSc2	knihovna
a	a	k8xC	a
škola	škola	k1gFnSc1	škola
obdržela	obdržet	k5eAaPmAgFnS	obdržet
nové	nový	k2eAgFnPc4d1	nová
učebnice	učebnice	k1gFnPc4	učebnice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyřešení	vyřešení	k1gNnSc6	vyřešení
počátečních	počáteční	k2eAgFnPc2d1	počáteční
poválečních	pováleční	k2eAgFnPc2d1	pováleční
potíží	potíž	k1gFnPc2	potíž
(	(	kIx(	(
<g/>
např.	např.	kA	např.
nedostatek	nedostatek	k1gInSc4	nedostatek
uhlí	uhlí	k1gNnSc2	uhlí
ve	v	k7c6	v
školním	školní	k2eAgInSc6d1	školní
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
-	-	kIx~	-
<g/>
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
oprava	oprava	k1gFnSc1	oprava
topení	topení	k1gNnSc2	topení
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
výuka	výuka	k1gFnSc1	výuka
obnovena	obnoven	k2eAgFnSc1d1	obnovena
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
poválečný	poválečný	k2eAgInSc4d1	poválečný
problém	problém	k1gInSc4	problém
představovalo	představovat	k5eAaImAgNnS	představovat
nedostatečné	dostatečný	k2eNgNnSc1d1	nedostatečné
personální	personální	k2eAgNnSc1d1	personální
obsazení	obsazení	k1gNnSc1	obsazení
profesorského	profesorský	k2eAgInSc2d1	profesorský
sboru	sbor	k1gInSc2	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
nedocházelo	docházet	k5eNaImAgNnS	docházet
ke	k	k7c3	k
jmenování	jmenování	k1gNnSc3	jmenování
středoškolských	středoškolský	k2eAgMnPc2d1	středoškolský
profesorů	profesor	k1gMnPc2	profesor
<g/>
,	,	kIx,	,
během	během	k7c2	během
následujících	následující	k2eAgNnPc2d1	následující
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
profesorský	profesorský	k2eAgInSc1d1	profesorský
sbor	sbor	k1gInSc1	sbor
doplněn	doplnit	k5eAaPmNgInS	doplnit
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgNnPc1d1	následující
poválečná	poválečný	k2eAgNnPc1d1	poválečné
léta	léto	k1gNnPc1	léto
probíhala	probíhat	k5eAaImAgNnP	probíhat
relativně	relativně	k6eAd1	relativně
klidně	klidně	k6eAd1	klidně
<g/>
,	,	kIx,	,
do	do	k7c2	do
osnov	osnova	k1gFnPc2	osnova
byl	být	k5eAaImAgInS	být
přidán	přidán	k2eAgInSc1d1	přidán
anglický	anglický	k2eAgInSc1d1	anglický
jazyk	jazyk	k1gInSc1	jazyk
a	a	k8xC	a
žáci	žák	k1gMnPc1	žák
se	se	k3xPyFc4	se
povinně	povinně	k6eAd1	povinně
učili	učít	k5eAaPmAgMnP	učít
německy	německy	k6eAd1	německy
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějšími	významný	k2eAgFnPc7d3	nejvýznamnější
událostmi	událost	k1gFnPc7	událost
byly	být	k5eAaImAgFnP	být
patrně	patrně	k6eAd1	patrně
návštěvy	návštěva	k1gFnPc1	návštěva
prezidenta	prezident	k1gMnSc2	prezident
ČSR	ČSR	kA	ČSR
T.G.	T.G.	k1gMnSc2	T.G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
a	a	k8xC	a
ministra	ministr	k1gMnSc2	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1922	[number]	k4	1922
a	a	k8xC	a
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Dosavadní	dosavadní	k2eAgInSc1d1	dosavadní
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
vývoj	vývoj	k1gInSc1	vývoj
gymnázia	gymnázium	k1gNnSc2	gymnázium
byl	být	k5eAaImAgInS	být
přerušen	přerušit	k5eAaPmNgInS	přerušit
obsazením	obsazení	k1gNnSc7	obsazení
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
vznikem	vznik	k1gInSc7	vznik
Protektorátu	protektorát	k1gInSc2	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
gymnázia	gymnázium	k1gNnSc2	gymnázium
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
zabrána	zabrán	k2eAgFnSc1d1	zabrána
vojskem	vojsko	k1gNnSc7	vojsko
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
školy	škola	k1gFnSc2	škola
muselo	muset	k5eAaImAgNnS	muset
hledat	hledat	k5eAaImF	hledat
náhradní	náhradní	k2eAgInPc4d1	náhradní
prostory	prostor	k1gInPc4	prostor
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgNnPc1	ten
probíhala	probíhat	k5eAaImAgNnP	probíhat
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
Voršilek	voršilka	k1gFnPc2	voršilka
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
obchodní	obchodní	k2eAgFnSc6d1	obchodní
škole	škola	k1gFnSc6	škola
a	a	k8xC	a
v	v	k7c6	v
Lindnerově	Lindnerův	k2eAgInSc6d1	Lindnerův
učitelském	učitelský	k2eAgInSc6d1	učitelský
ústavu	ústav	k1gInSc6	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Výuka	výuka	k1gFnSc1	výuka
musela	muset	k5eAaImAgFnS	muset
probíhat	probíhat	k5eAaImF	probíhat
s	s	k7c7	s
nedostačujícími	dostačující	k2eNgFnPc7d1	nedostačující
pomůckami	pomůcka	k1gFnPc7	pomůcka
<g/>
;	;	kIx,	;
obě	dva	k4xCgFnPc1	dva
školní	školní	k2eAgFnPc1d1	školní
knihovny	knihovna	k1gFnPc1	knihovna
musely	muset	k5eAaImAgFnP	muset
zůstat	zůstat	k5eAaPmF	zůstat
zapečetěny	zapečetit	k5eAaPmNgFnP	zapečetit
<g/>
.	.	kIx.	.
</s>
<s>
Studenti	student	k1gMnPc1	student
si	se	k3xPyFc3	se
učebnice	učebnice	k1gFnPc4	učebnice
půjčovali	půjčovat	k5eAaImAgMnP	půjčovat
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
knihovně	knihovna	k1gFnSc6	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Nábytek	nábytek	k1gInSc1	nábytek
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
ponechán	ponechat	k5eAaPmNgInS	ponechat
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
armádě	armáda	k1gFnSc3	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
v	v	k7c6	v
osnovách	osnova	k1gFnPc6	osnova
vyučovaných	vyučovaný	k2eAgInPc2d1	vyučovaný
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
byly	být	k5eAaImAgInP	být
zasaženy	zasažen	k2eAgInPc1d1	zasažen
předměty	předmět	k1gInPc1	předmět
jako	jako	k8xS	jako
český	český	k2eAgInSc1d1	český
jazyk	jazyk	k1gInSc1	jazyk
a	a	k8xC	a
dějepis	dějepis	k1gInSc1	dějepis
<g/>
.	.	kIx.	.
</s>
<s>
Svátky	svátek	k1gInPc1	svátek
jako	jako	k8xC	jako
byl	být	k5eAaImAgInS	být
28	[number]	k4	28
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc1	říjen
už	už	k6eAd1	už
nadále	nadále	k6eAd1	nadále
nebyly	být	k5eNaImAgFnP	být
volnými	volný	k2eAgInPc7d1	volný
dny	den	k1gInPc7	den
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
byly	být	k5eAaImAgInP	být
slaveny	slaven	k2eAgInPc1d1	slaven
dny	den	k1gInPc1	den
vzniku	vznik	k1gInSc2	vznik
Protektorátu	protektorát	k1gInSc2	protektorát
a	a	k8xC	a
studenti	student	k1gMnPc1	student
museli	muset	k5eAaImAgMnP	muset
projevovat	projevovat	k5eAaImF	projevovat
úctu	úcta	k1gFnSc4	úcta
symbolům	symbol	k1gInPc3	symbol
Třetí	třetí	k4xOgFnSc2	třetí
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
sice	sice	k8xC	sice
německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
budovu	budova	k1gFnSc4	budova
školy	škola	k1gFnSc2	škola
uvolnila	uvolnit	k5eAaPmAgFnS	uvolnit
a	a	k8xC	a
výuka	výuka	k1gFnSc1	výuka
mohla	moct	k5eAaImAgFnS	moct
v	v	k7c6	v
poničených	poničený	k2eAgFnPc6d1	poničená
učebnách	učebna	k1gFnPc6	učebna
pokračovat	pokračovat	k5eAaImF	pokračovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1943	[number]	k4	1943
však	však	k8xC	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
opětovnému	opětovný	k2eAgInSc3d1	opětovný
záboru	zábor	k1gInSc3	zábor
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
školy	škola	k1gFnSc2	škola
prošla	projít	k5eAaPmAgFnS	projít
mohutnou	mohutný	k2eAgFnSc7d1	mohutná
přestavbou	přestavba	k1gFnSc7	přestavba
a	a	k8xC	a
zbývající	zbývající	k2eAgFnPc1d1	zbývající
sbírky	sbírka	k1gFnPc1	sbírka
byly	být	k5eAaImAgFnP	být
odvezeny	odvézt	k5eAaPmNgFnP	odvézt
<g/>
.	.	kIx.	.
</s>
<s>
Učitelé	učitel	k1gMnPc1	učitel
se	se	k3xPyFc4	se
studenty	student	k1gMnPc7	student
museli	muset	k5eAaImAgMnP	muset
znovu	znovu	k6eAd1	znovu
stěhovat	stěhovat	k5eAaImF	stěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
usídlili	usídlit	k5eAaPmAgMnP	usídlit
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zabrání	zabrání	k1gNnSc3	zabrání
i	i	k8xC	i
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
archív	archív	k1gInSc1	archív
školy	škola	k1gFnSc2	škola
přestěhován	přestěhovat	k5eAaPmNgInS	přestěhovat
do	do	k7c2	do
Vlašského	vlašský	k2eAgInSc2d1	vlašský
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Velikonocích	Velikonoce	k1gFnPc6	Velikonoce
pak	pak	k6eAd1	pak
výuka	výuka	k1gFnSc1	výuka
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
sokolovně	sokolovna	k1gFnSc6	sokolovna
<g/>
,	,	kIx,	,
v	v	k7c6	v
hostinci	hostinec	k1gInSc6	hostinec
"	"	kIx"	"
<g/>
Kotor	Kotor	k1gInSc1	Kotor
<g/>
"	"	kIx"	"
a	a	k8xC	a
v	v	k7c6	v
Řemeslnické	řemeslnický	k2eAgFnSc6d1	řemeslnická
besedě	beseda	k1gFnSc6	beseda
<g/>
.	.	kIx.	.
</s>
<s>
Vyučovalo	vyučovat	k5eAaImAgNnS	vyučovat
se	se	k3xPyFc4	se
dopoledne	dopoledne	k6eAd1	dopoledne
i	i	k9	i
odpoledne	odpoledne	k6eAd1	odpoledne
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
nebyl	být	k5eNaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
<g/>
ročník	ročník	k1gInSc1	ročník
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nařízení	nařízení	k1gNnSc2	nařízení
protektorátního	protektorátní	k2eAgNnSc2d1	protektorátní
MŠ	MŠ	kA	MŠ
byli	být	k5eAaImAgMnP	být
studenti	student	k1gMnPc1	student
z	z	k7c2	z
bývalého	bývalý	k2eAgInSc2d1	bývalý
VII	VII	kA	VII
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
posláni	poslat	k5eAaPmNgMnP	poslat
do	do	k7c2	do
Technische	Technischus	k1gMnSc5	Technischus
Nothilfe	Nothilf	k1gMnSc5	Nothilf
<g/>
,	,	kIx,	,
na	na	k7c4	na
kopání	kopání	k1gNnSc4	kopání
zákopů	zákop	k1gInPc2	zákop
proti	proti	k7c3	proti
postupující	postupující	k2eAgFnSc3d1	postupující
frontě	fronta	k1gFnSc3	fronta
či	či	k8xC	či
ke	k	k7c3	k
vzdušné	vzdušný	k2eAgInPc4d1	vzdušný
obraně-Luftschutz	obraně-Luftschutz	k1gInSc4	obraně-Luftschutz
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
studentky	studentka	k1gFnPc1	studentka
pak	pak	k6eAd1	pak
pracovaly	pracovat	k5eAaImAgFnP	pracovat
v	v	k7c6	v
místních	místní	k2eAgInPc6d1	místní
průmyslových	průmyslový	k2eAgInPc6d1	průmyslový
podnicích	podnik	k1gInPc6	podnik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
sice	sice	k8xC	sice
německé	německý	k2eAgInPc1d1	německý
oddíly	oddíl	k1gInPc1	oddíl
budovu	budova	k1gFnSc4	budova
školy	škola	k1gFnPc1	škola
opustily	opustit	k5eAaPmAgFnP	opustit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gNnSc4	její
místo	místo	k1gNnSc4	místo
zabrala	zabrat	k5eAaPmAgFnS	zabrat
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
odchodu	odchod	k1gInSc6	odchod
zbývalo	zbývat	k5eAaImAgNnS	zbývat
z	z	k7c2	z
vybavení	vybavení	k1gNnSc2	vybavení
školy	škola	k1gFnSc2	škola
jen	jen	k9	jen
málo	málo	k4c1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgNnSc1	veškerý
vybavení	vybavení	k1gNnSc1	vybavení
kabinetů	kabinet	k1gInPc2	kabinet
bylo	být	k5eAaImAgNnS	být
zničeno	zničen	k2eAgNnSc1d1	zničeno
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
nábytek	nábytek	k1gInSc1	nábytek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
umučeno	umučit	k5eAaPmNgNnS	umučit
I2	I2	k1gFnPc4	I2
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
padl	padnout	k5eAaPmAgInS	padnout
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
zavládla	zavládnout	k5eAaPmAgFnS	zavládnout
uvolněná	uvolněný	k2eAgFnSc1d1	uvolněná
nálada	nálada	k1gFnSc1	nálada
a	a	k8xC	a
radost	radost	k1gFnSc1	radost
z	z	k7c2	z
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
ke	k	k7c3	k
slovu	slovo	k1gNnSc3	slovo
dostaly	dostat	k5eAaPmAgInP	dostat
studentské	studentský	k2eAgInPc1d1	studentský
a	a	k8xC	a
společenské	společenský	k2eAgInPc1d1	společenský
spolky	spolek	k1gInPc1	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
že	že	k8xS	že
budova	budova	k1gFnSc1	budova
školy	škola	k1gFnSc2	škola
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
nutnou	nutný	k2eAgFnSc4d1	nutná
a	a	k8xC	a
nákladnou	nákladný	k2eAgFnSc4d1	nákladná
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Následky	následek	k1gInPc1	následek
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
se	se	k3xPyFc4	se
ve	v	k7c4	v
školy	škola	k1gFnPc4	škola
neprojevily	projevit	k5eNaPmAgFnP	projevit
hned	hned	k6eAd1	hned
<g/>
.	.	kIx.	.
</s>
<s>
Vznikaly	vznikat	k5eAaImAgInP	vznikat
akční	akční	k2eAgInPc1d1	akční
výbory	výbor	k1gInPc1	výbor
a	a	k8xC	a
výuka	výuka	k1gFnSc1	výuka
se	se	k3xPyFc4	se
soustřeďovala	soustřeďovat	k5eAaImAgFnS	soustřeďovat
na	na	k7c6	na
vyzdvyhování	vyzdvyhování	k1gNnSc6	vyzdvyhování
role	role	k1gFnSc2	role
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Zaznamenány	zaznamenán	k2eAgInPc1d1	zaznamenán
jsou	být	k5eAaImIp3nP	být
konflikty	konflikt	k1gInPc1	konflikt
studentů	student	k1gMnPc2	student
s	s	k7c7	s
vedením	vedení	k1gNnSc7	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Studenti	student	k1gMnPc1	student
si	se	k3xPyFc3	se
především	především	k6eAd1	především
stěžovali	stěžovat	k5eAaImAgMnP	stěžovat
na	na	k7c4	na
povinnou	povinný	k2eAgFnSc4d1	povinná
výuku	výuka	k1gFnSc4	výuka
ruštiny	ruština	k1gFnSc2	ruština
a	a	k8xC	a
vyzdvihování	vyzdvihování	k1gNnSc2	vyzdvihování
SSSR	SSSR	kA	SSSR
jako	jako	k8xC	jako
jediného	jediný	k2eAgMnSc4d1	jediný
osvoboditele	osvoboditel	k1gMnSc4	osvoboditel
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
prohřešky	prohřešek	k1gInPc1	prohřešek
byly	být	k5eAaImAgInP	být
řešeny	řešit	k5eAaImNgInP	řešit
velmi	velmi	k6eAd1	velmi
tolerantně	tolerantně	k6eAd1	tolerantně
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
neohrozily	ohrozit	k5eNaPmAgInP	ohrozit
budoucnost	budoucnost	k1gFnSc4	budoucnost
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
gymnázium	gymnázium	k1gNnSc4	gymnázium
reforma	reforma	k1gFnSc1	reforma
českého	český	k2eAgInSc2d1	český
školského	školský	k2eAgInSc2d1	školský
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
sovětských	sovětský	k2eAgFnPc2d1	sovětská
škol	škola	k1gFnPc2	škola
bylo	být	k5eAaImAgNnS	být
základní	základní	k2eAgNnSc1d1	základní
a	a	k8xC	a
všeobecné	všeobecný	k2eAgNnSc1d1	všeobecné
střední	střední	k2eAgNnSc1d1	střední
vzdělání	vzdělání	k1gNnSc1	vzdělání
spojeno	spojit	k5eAaPmNgNnS	spojit
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jedenáctiletá	jedenáctiletý	k2eAgFnSc1d1	jedenáctiletá
střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
model	model	k1gInSc1	model
se	se	k3xPyFc4	se
však	však	k9	však
udržel	udržet	k5eAaPmAgInS	udržet
pouze	pouze	k6eAd1	pouze
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nového	nový	k2eAgInSc2d1	nový
školského	školský	k2eAgInSc2d1	školský
zákona	zákon	k1gInSc2	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
vznikaly	vznikat	k5eAaImAgInP	vznikat
rozdělením	rozdělení	k1gNnSc7	rozdělení
dosavadních	dosavadní	k2eAgFnPc2d1	dosavadní
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
základní	základní	k2eAgFnSc2d1	základní
devítileté	devítiletý	k2eAgFnSc2d1	devítiletá
a	a	k8xC	a
tříleté	tříletý	k2eAgFnSc2d1	tříletá
střední	střední	k2eAgFnSc2d1	střední
všeobecně	všeobecně	k6eAd1	všeobecně
vzdělávací	vzdělávací	k2eAgFnSc2d1	vzdělávací
školy	škola	k1gFnSc2	škola
(	(	kIx(	(
<g/>
SVVŠ	SVVŠ	kA	SVVŠ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
ale	ale	k9	ale
koncem	koncem	k7c2	koncem
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
zase	zase	k9	zase
změnily	změnit	k5eAaPmAgInP	změnit
v	v	k7c4	v
čtyřletá	čtyřletý	k2eAgNnPc4d1	čtyřleté
gymnázia	gymnázium	k1gNnPc4	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rozdělení	rozdělení	k1gNnSc6	rozdělení
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
školy	škola	k1gFnPc4	škola
nedopadla	dopadnout	k5eNaPmAgFnS	dopadnout
kutnohorská	kutnohorský	k2eAgFnSc1d1	Kutnohorská
SVVŠ	SVVŠ	kA	SVVŠ
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
její	její	k3xOp3gFnSc1	její
budova	budova	k1gFnSc1	budova
připadla	připadnout	k5eAaPmAgFnS	připadnout
městu	město	k1gNnSc3	město
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
<g/>
-	-	kIx~	-
<g/>
1968	[number]	k4	1968
škola	škola	k1gFnSc1	škola
sama	sám	k3xTgMnSc4	sám
počala	počnout	k5eAaPmAgFnS	počnout
vydávat	vydávat	k5eAaImF	vydávat
Výroční	výroční	k2eAgFnSc1d1	výroční
zprávy	zpráva	k1gFnPc1	zpráva
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vydává	vydávat	k5eAaImIp3nS	vydávat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1972	[number]	k4	1972
až	až	k8xS	až
1975	[number]	k4	1975
se	se	k3xPyFc4	se
jejími	její	k3xOp3gFnPc7	její
součástmi	součást	k1gFnPc7	součást
staly	stát	k5eAaPmAgInP	stát
i	i	k9	i
almanachy	almanach	k1gInPc1	almanach
studentských	studentský	k2eAgFnPc2d1	studentská
prací	práce	k1gFnPc2	práce
vzniklých	vzniklý	k2eAgFnPc2d1	vzniklá
v	v	k7c6	v
literárním	literární	k2eAgInSc6d1	literární
semináři	seminář	k1gInSc6	seminář
i	i	k8xC	i
v	v	k7c6	v
přírodovědných	přírodovědný	k2eAgInPc6d1	přírodovědný
předmětech	předmět	k1gInPc6	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Neustálým	neustálý	k2eAgMnPc3d1	neustálý
a	a	k8xC	a
stále	stále	k6eAd1	stále
se	s	k7c7	s
zvětšujícím	zvětšující	k2eAgInSc7d1	zvětšující
problémem	problém	k1gInSc7	problém
byl	být	k5eAaImAgInS	být
nedostatek	nedostatek	k1gInSc1	nedostatek
prostor	prostora	k1gFnPc2	prostora
ve	v	k7c6	v
školní	školní	k2eAgFnSc6d1	školní
budově	budova	k1gFnSc6	budova
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
tísnilo	tísnit	k5eAaImAgNnS	tísnit
gymnázium	gymnázium	k1gNnSc1	gymnázium
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
ZŠ	ZŠ	kA	ZŠ
Žižkov	Žižkov	k1gInSc1	Žižkov
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc4	tento
stav	stav	k1gInSc4	stav
nepodařilo	podařit	k5eNaPmAgNnS	podařit
vyřešit	vyřešit	k5eAaPmF	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc4	období
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
velkých	velký	k2eAgFnPc2d1	velká
změn	změna	k1gFnPc2	změna
ve	v	k7c6	v
výuce	výuka	k1gFnSc6	výuka
<g/>
.	.	kIx.	.
</s>
<s>
Pozornost	pozornost	k1gFnSc1	pozornost
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
více	hodně	k6eAd2	hodně
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
výuku	výuka	k1gFnSc4	výuka
cicích	cicí	k2eAgInPc2d1	cicí
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
zváni	zvát	k5eAaImNgMnP	zvát
do	do	k7c2	do
výuky	výuka	k1gFnSc2	výuka
zahraniční	zahraniční	k2eAgInSc1d1	zahraniční
lektoři	lektoř	k1gFnSc3	lektoř
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1992	[number]	k4	1992
získala	získat	k5eAaPmAgFnS	získat
škola	škola	k1gFnSc1	škola
právní	právní	k2eAgFnSc4d1	právní
subjektivitu	subjektivita	k1gFnSc4	subjektivita
<g/>
.	.	kIx.	.
</s>
<s>
Začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
pořádat	pořádat	k5eAaImF	pořádat
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
studentské	studentská	k1gFnSc2	studentská
výměnné	výměnný	k2eAgInPc4d1	výměnný
pobyty	pobyt	k1gInPc4	pobyt
<g/>
,	,	kIx,	,
škola	škola	k1gFnSc1	škola
vydává	vydávat	k5eAaImIp3nS	vydávat
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
časopis	časopis	k1gInSc4	časopis
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
divadelní	divadelní	k2eAgInSc4d1	divadelní
soubor	soubor	k1gInSc4	soubor
<g/>
,	,	kIx,	,
galerii	galerie	k1gFnSc4	galerie
a	a	k8xC	a
knihovnu	knihovna	k1gFnSc4	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c4	na
vyřešení	vyřešení	k1gNnSc4	vyřešení
problému	problém	k1gInSc2	problém
s	s	k7c7	s
nevyhovujícími	vyhovující	k2eNgInPc7d1	nevyhovující
prostory	prostor	k1gInPc7	prostor
školy	škola	k1gFnSc2	škola
-	-	kIx~	-
byl	být	k5eAaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
nové	nový	k2eAgFnSc2d1	nová
budovy	budova	k1gFnSc2	budova
v	v	k7c6	v
Jaselské	jaselský	k2eAgFnSc6d1	Jaselská
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
září	zářit	k5eAaImIp3nS	zářit
1997	[number]	k4	1997
zde	zde	k6eAd1	zde
probíhá	probíhat	k5eAaImIp3nS	probíhat
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
výuka	výuka	k1gFnSc1	výuka
<g/>
.	.	kIx.	.
</s>
