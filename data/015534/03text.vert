<s>
Gl	Gl	k?
833	#num#	k4
</s>
<s>
Gliese	Gliese	k1gFnSc1
833	#num#	k4
Astrometrická	Astrometrický	k2eAgNnPc4d1
data	datum	k1gNnPc4
<g/>
(	(	kIx(
<g/>
Ekvinokcium	ekvinokcium	k1gNnSc1
J	J	kA
<g/>
2000.0	2000.0	k4
<g/>
)	)	kIx)
Souhvězdí	souhvězdí	k1gNnSc2
</s>
<s>
Indián	Indián	k1gMnSc1
(	(	kIx(
<g/>
Indus	Indus	k1gInSc1
<g/>
)	)	kIx)
Rektascenze	rektascenze	k1gFnSc1
</s>
<s>
21	#num#	k4
<g/>
h	h	k?
<g/>
36	#num#	k4
<g/>
m	m	kA
<g/>
41,244	41,244	k4
16	#num#	k4
<g/>
s	s	k7c7
Deklinace	deklinace	k1gFnSc2
</s>
<s>
-50	-50	k4
<g/>
°	°	k?
<g/>
50	#num#	k4
<g/>
'	'	kIx"
<g/>
43,389	43,389	k4
1	#num#	k4
<g/>
"	"	kIx"
Paralaxa	paralaxa	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
68,40	68,40	k4
±	±	k?
0,58	0,58	k4
<g/>
)	)	kIx)
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
3	#num#	k4
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vzdálenost	vzdálenost	k1gFnSc1
</s>
<s>
47,7	47,7	k4
ly	ly	k?
<g/>
(	(	kIx(
<g/>
14,6	14,6	k4
pc	pc	k?
<g/>
)	)	kIx)
Barevný	barevný	k2eAgInSc1d1
index	index	k1gInSc1
(	(	kIx(
<g/>
U-B	U-B	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
0,559	0,559	k4
Barevný	barevný	k2eAgInSc4d1
index	index	k1gInSc4
(	(	kIx(
<g/>
B-V	B-V	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
0,887	0,887	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Barevný	barevný	k2eAgInSc1d1
index	index	k1gInSc1
(	(	kIx(
<g/>
V-R	V-R	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
0,50	0,50	k4
Barevný	barevný	k2eAgInSc4d1
index	index	k1gInSc4
(	(	kIx(
<g/>
R-I	R-I	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
0,45	0,45	k4
Zdánlivá	zdánlivý	k2eAgFnSc1d1
hvězdná	hvězdný	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
</s>
<s>
7,157	7,157	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Absolutní	absolutní	k2eAgFnSc1d1
hvězdná	hvězdný	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
</s>
<s>
6,33	6,33	k4
Radiální	radiální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
</s>
<s>
28,20	28,20	k4
<g/>
±	±	k?
<g/>
0,79	0,79	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vlastní	vlastní	k2eAgInSc1d1
pohyb	pohyb	k1gInSc1
v	v	k7c6
rektascenzi	rektascenze	k1gFnSc6
</s>
<s>
0,424	0,424	k4
24	#num#	k4
<g/>
"	"	kIx"
<g/>
/	/	kIx~
<g/>
rok	rok	k1gInSc4
Vlastní	vlastní	k2eAgInSc4d1
pohyb	pohyb	k1gInSc4
v	v	k7c6
deklinaci	deklinace	k1gFnSc6
</s>
<s>
-0,199	-0,199	k4
70	#num#	k4
<g/>
"	"	kIx"
<g/>
/	/	kIx~
<g/>
rok	rok	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Fyzikální	fyzikální	k2eAgFnSc2d1
charakteristiky	charakteristika	k1gFnSc2
Typ	typ	k1gInSc1
proměnnosti	proměnnost	k1gFnSc2
</s>
<s>
není	být	k5eNaImIp3nS
Spektrální	spektrální	k2eAgInSc1d1
typ	typ	k1gInSc1
</s>
<s>
K	k	k7c3
<g/>
1	#num#	k4
<g/>
V	V	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hmotnost	hmotnost	k1gFnSc1
</s>
<s>
</s>
<s desamb="1">
kg	kg	kA
(	(	kIx(
<g/>
?	?	kIx.
</s>
<s desamb="1">
M	M	kA
<g/>
☉	☉	k?
<g/>
)	)	kIx)
Poloměr	poloměr	k1gInSc1
</s>
<s>
</s>
<s desamb="1">
km	km	kA
(	(	kIx(
<g/>
?	?	kIx.
</s>
<s desamb="1">
R	R	kA
<g/>
☉	☉	k?
<g/>
)	)	kIx)
Zářivý	zářivý	k2eAgInSc1d1
výkon	výkon	k1gInSc1
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
</s>
<s>
</s>
<s desamb="1">
L	L	kA
<g/>
☉	☉	k?
Povrchová	povrchový	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
</s>
<s>
</s>
<s desamb="1">
K	k	k7c3
Stáří	stáří	k1gNnSc3
</s>
<s>
</s>
<s desamb="1">
r	r	kA
Rotační	rotační	k2eAgFnSc1d1
perioda	perioda	k1gFnSc1
</s>
<s>
</s>
<s desamb="1">
Oscilační	oscilační	k2eAgFnSc1d1
perioda	perioda	k1gFnSc1
</s>
<s>
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
označení	označení	k1gNnSc4
Henry	henry	k1gInSc2
Draper	Draper	k1gInSc4
Catalogue	Catalogue	k1gNnSc2
</s>
<s>
HD	HD	kA
205390	#num#	k4
2MASS	2MASS	k4
</s>
<s>
2MASS	2MASS	k4
J21364123-5050433	J21364123-5050433	k1gFnPc2
SAO	SAO	kA
katalog	katalog	k1gInSc1
</s>
<s>
SAO	SAO	kA
247109	#num#	k4
Katalog	katalog	k1gInSc1
Hipparcos	Hipparcos	k1gInSc4
</s>
<s>
HIP	hip	k0
106696	#num#	k4
Tychův	Tychův	k2eAgInSc4d1
katalog	katalog	k1gInSc4
</s>
<s>
TYC	TYC	kA
8436-504-1	8436-504-1	k4
General	General	k1gFnSc2
Catalogue	Catalogu	k1gFnSc2
</s>
<s>
GC	GC	kA
30234	#num#	k4
Glieseho	Gliese	k1gMnSc2
katalog	katalog	k1gInSc4
</s>
<s>
Gl	Gl	k?
833	#num#	k4
Synonyma	synonymum	k1gNnSc2
</s>
<s>
CD-51	CD-51	k4
12998	#num#	k4
<g/>
,	,	kIx,
<g/>
CPD	CPD	kA
-51	-51	k4
11696	#num#	k4
<g/>
,	,	kIx,
GCRV	GCRV	kA
72293	#num#	k4
<g/>
,	,	kIx,
GJ	GJ	kA
833	#num#	k4
<g/>
,	,	kIx,
IRAS	IRAS	kA
21332	#num#	k4
<g/>
-	-	kIx~
<g/>
5104	#num#	k4
<g/>
,	,	kIx,
LTT	LTT	kA
8595	#num#	k4
<g/>
,	,	kIx,
2MASS	2MASS	k4
J	J	kA
<g/>
21364123	#num#	k4
<g/>
-	-	kIx~
<g/>
5050433	#num#	k4
<g/>
,	,	kIx,
NLTT	NLTT	kA
51629	#num#	k4
<g/>
,	,	kIx,
PLX	PLX	kA
5198	#num#	k4
<g/>
,	,	kIx,
PPM	PPM	kA
349539	#num#	k4
<g/>
,	,	kIx,
1RXS	1RXS	k4
J	J	kA
<g/>
213642.6	213642.6	k4
<g/>
-	-	kIx~
<g/>
505033	#num#	k4
<g/>
,	,	kIx,
SPOCS	SPOCS	kA
935	#num#	k4
<g/>
,	,	kIx,
UBV	UBV	kA
18557	#num#	k4
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
–	–	k?
měření	měření	k1gNnSc2
provedena	proveden	k2eAgFnSc1d1
ve	v	k7c6
viditelném	viditelný	k2eAgInSc6d1
světleNěkterá	světleNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Gl	Gl	k?
833	#num#	k4
je	být	k5eAaImIp3nS
hvězda	hvězda	k1gFnSc1
spektrálního	spektrální	k2eAgInSc2d1
typu	typ	k1gInSc2
K2	K2	k1gFnSc2
vzdálená	vzdálený	k2eAgFnSc1d1
od	od	k7c2
Země	zem	k1gFnSc2
47,7	47,7	k4
ly	ly	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
oranžového	oranžový	k2eAgMnSc4d1
trpaslíka	trpaslík	k1gMnSc4
v	v	k7c6
souhvězdí	souhvězdí	k1gNnSc6
Indiána	Indián	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k1gNnPc7
označeními	označení	k1gNnPc7
hvězdy	hvězda	k1gFnPc1
jsou	být	k5eAaImIp3nP
HD	HD	kA
205390	#num#	k4
<g/>
,	,	kIx,
HIP	hip	k0
106696	#num#	k4
<g/>
,	,	kIx,
SAO	SAO	kA
247109	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radiální	radiální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
hvězdy	hvězda	k1gFnSc2
činí	činit	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
28	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
s.	s.	k?
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
http://simbad.u-strasbg.fr/simbad/sim-basic?Ident	http://simbad.u-strasbg.fr/simbad/sim-basic?Ident	k1gMnSc1
=	=	kIx~
Gl	Gl	k1gMnSc1
<g/>
+	+	kIx~
<g/>
833	#num#	k4
<g/>
&	&	k?
<g/>
submit	submit	k1gInSc1
=	=	kIx~
SIMBAD	SIMBAD	kA
<g/>
+	+	kIx~
<g/>
search	search	k1gMnSc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
Gl	Gl	k1gFnSc2
833	#num#	k4
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
,	,	kIx,
Gliese	Glies	k1gInSc6
833	#num#	k4
na	na	k7c6
nizozemské	nizozemský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
Gliese	Glies	k1gInSc6
833	#num#	k4
na	na	k7c6
italské	italský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Astronomie	astronomie	k1gFnSc1
</s>
