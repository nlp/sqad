<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
II	II	kA	II
<g/>
.	.	kIx.	.
Popel	popel	k1gInSc1	popel
z	z	k7c2	z
Lobkowicz	Lobkowicz	k1gMnSc1	Lobkowicz
(	(	kIx(	(
<g/>
1501	[number]	k4	1501
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1584	[number]	k4	1584
Praha-Malá	Praha-Malý	k2eAgFnSc1d1	Praha-Malá
Strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
příslušníkem	příslušník	k1gMnSc7	příslušník
chlumecké	chlumecký	k2eAgFnSc2d1	Chlumecká
větve	větev	k1gFnSc2	větev
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
rodu	rod	k1gInSc2	rod
Popelů	Popela	k1gMnPc2	Popela
z	z	k7c2	z
Lobkowicz	Lobkowicz	k1gMnSc1	Lobkowicz
<g/>
,	,	kIx,	,
vysoce	vysoce	k6eAd1	vysoce
vzdělaný	vzdělaný	k2eAgMnSc1d1	vzdělaný
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
také	také	k9	také
ovládal	ovládat	k5eAaImAgMnS	ovládat
mnoho	mnoho	k4c4	mnoho
cizích	cizí	k2eAgInPc2d1	cizí
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
