<p>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
II	II	kA	II
<g/>
.	.	kIx.	.
Popel	popel	k1gInSc1	popel
z	z	k7c2	z
Lobkowicz	Lobkowicz	k1gMnSc1	Lobkowicz
(	(	kIx(	(
<g/>
1501	[number]	k4	1501
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1584	[number]	k4	1584
Praha-Malá	Praha-Malý	k2eAgFnSc1d1	Praha-Malá
Strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
příslušníkem	příslušník	k1gMnSc7	příslušník
chlumecké	chlumecký	k2eAgFnSc2d1	Chlumecká
větve	větev	k1gFnSc2	větev
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
rodu	rod	k1gInSc2	rod
Popelů	Popela	k1gMnPc2	Popela
z	z	k7c2	z
Lobkowicz	Lobkowicz	k1gMnSc1	Lobkowicz
<g/>
,	,	kIx,	,
vysoce	vysoce	k6eAd1	vysoce
vzdělaný	vzdělaný	k2eAgMnSc1d1	vzdělaný
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
také	také	k9	také
ovládal	ovládat	k5eAaImAgMnS	ovládat
mnoho	mnoho	k4c4	mnoho
cizích	cizí	k2eAgInPc2d1	cizí
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Zastával	zastávat	k5eAaImAgMnS	zastávat
post	post	k1gInSc4	post
dvorského	dvorský	k2eAgMnSc2d1	dvorský
maršálka	maršálek	k1gMnSc2	maršálek
<g/>
,	,	kIx,	,
prezidenta	prezident	k1gMnSc2	prezident
apelačního	apelační	k2eAgInSc2d1	apelační
soudu	soud	k1gInSc2	soud
a	a	k8xC	a
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
hofmistra	hofmistr	k1gMnSc2	hofmistr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
a	a	k8xC	a
život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jako	jako	k9	jako
druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
potomků	potomek	k1gMnPc2	potomek
Ladislava	Ladislav	k1gMnSc2	Ladislav
I.	I.	kA	I.
Popela	Popela	k1gMnSc1	Popela
z	z	k7c2	z
Lobkowicz	Lobkowicz	k1gMnSc1	Lobkowicz
(	(	kIx(	(
<g/>
†	†	k?	†
1505	[number]	k4	1505
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
manželky	manželka	k1gFnPc1	manželka
Anny	Anna	k1gFnSc2	Anna
Krajířové	Krajířová	k1gFnSc2	Krajířová
z	z	k7c2	z
Krajku	krajek	k1gInSc2	krajek
(	(	kIx(	(
<g/>
†	†	k?	†
1520	[number]	k4	1520
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Habsburků	Habsburk	k1gMnPc2	Habsburk
spojil	spojit	k5eAaPmAgMnS	spojit
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
se	se	k3xPyFc4	se
službou	služba	k1gFnSc7	služba
tomuto	tento	k3xDgInSc3	tento
panovnickému	panovnický	k2eAgInSc3d1	panovnický
rodu	rod	k1gInSc3	rod
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
byl	být	k5eAaImAgInS	být
válečným	válečný	k2eAgMnSc7d1	válečný
komisařem	komisař	k1gMnSc7	komisař
Karla	Karel	k1gMnSc2	Karel
V.	V.	kA	V.
a	a	k8xC	a
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
ve	v	k7c6	v
vojenském	vojenský	k2eAgNnSc6d1	vojenské
tažení	tažení	k1gNnSc6	tažení
proti	proti	k7c3	proti
Turkům	turek	k1gInPc3	turek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1532	[number]	k4	1532
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1534	[number]	k4	1534
se	se	k3xPyFc4	se
s	s	k7c7	s
nevelkým	velký	k2eNgInSc7d1	nevelký
úspěchem	úspěch	k1gInSc7	úspěch
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
králova	králův	k2eAgNnSc2d1	královo
úsilí	úsilí	k1gNnSc2	úsilí
o	o	k7c4	o
zlomení	zlomení	k1gNnSc4	zlomení
odporu	odpor	k1gInSc2	odpor
měst	město	k1gNnPc2	město
proti	proti	k7c3	proti
nové	nový	k2eAgFnSc3d1	nová
dani	daň	k1gFnSc3	daň
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1536	[number]	k4	1536
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
královským	královský	k2eAgMnSc7d1	královský
radou	rada	k1gMnSc7	rada
a	a	k8xC	a
komořím	komoří	k1gMnSc7	komoří
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1542	[number]	k4	1542
dvorním	dvorní	k2eAgMnSc7d1	dvorní
maršálkem	maršálek	k1gMnSc7	maršálek
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
I.	I.	kA	I.
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
tažení	tažení	k1gNnSc4	tažení
proti	proti	k7c3	proti
Janu	Jan	k1gMnSc3	Jan
Fridrichovi	Fridrich	k1gMnSc3	Fridrich
Saskému	saský	k2eAgMnSc3d1	saský
a	a	k8xC	a
vojskům	vojsko	k1gNnPc3	vojsko
šmalkaldského	šmalkaldský	k2eAgInSc2d1	šmalkaldský
spolku	spolek	k1gInSc2	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Účastnil	účastnit	k5eAaImAgInS	účastnit
se	se	k3xPyFc4	se
soudu	soud	k1gInSc2	soud
nad	nad	k7c7	nad
královskými	královský	k2eAgNnPc7d1	královské
městy	město	k1gNnPc7	město
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1547	[number]	k4	1547
a	a	k8xC	a
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
jej	on	k3xPp3gNnSc4	on
také	také	k9	také
pověřil	pověřit	k5eAaPmAgMnS	pověřit
jako	jako	k8xS	jako
královského	královský	k2eAgMnSc2d1	královský
komisaře	komisař	k1gMnSc2	komisař
provedením	provedení	k1gNnSc7	provedení
konfiskace	konfiskace	k1gFnSc2	konfiskace
statků	statek	k1gInPc2	statek
Kašpara	Kašpar	k1gMnSc2	Kašpar
Pluha	Pluh	k1gMnSc2	Pluh
z	z	k7c2	z
Rabštejna	Rabštejn	k1gInSc2	Rabštejn
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1548	[number]	k4	1548
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
prezidenta	prezident	k1gMnSc2	prezident
nově	nově	k6eAd1	nově
zřízené	zřízený	k2eAgFnPc1d1	zřízená
rady	rada	k1gFnPc1	rada
nad	nad	k7c7	nad
apelacemi	apelace	k1gFnPc7	apelace
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1549	[number]	k4	1549
byl	být	k5eAaImAgInS	být
také	také	k9	také
přísedícím	přísedící	k1gMnSc7	přísedící
komorního	komorní	k2eAgInSc2d1	komorní
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
post	post	k1gInSc4	post
prezidenta	prezident	k1gMnSc2	prezident
apelačního	apelační	k2eAgInSc2d1	apelační
soudu	soud	k1gInSc2	soud
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
stát	stát	k1gInSc1	stát
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
zemským	zemský	k2eAgMnSc7d1	zemský
hofmistrem	hofmistr	k1gMnSc7	hofmistr
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
královským	královský	k2eAgMnSc7d1	královský
místodržícím	místodržící	k1gMnSc7	místodržící
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
byl	být	k5eAaImAgInS	být
pověřován	pověřovat	k5eAaImNgInS	pověřovat
i	i	k9	i
jinými	jiný	k2eAgInPc7d1	jiný
úkoly	úkol	k1gInPc7	úkol
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1573	[number]	k4	1573
stál	stát	k5eAaImAgMnS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
poselstva	poselstvo	k1gNnSc2	poselstvo
českého	český	k2eAgInSc2d1	český
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
do	do	k7c2	do
Varšavy	Varšava	k1gFnSc2	Varšava
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
doporučit	doporučit	k5eAaPmF	doporučit
pro	pro	k7c4	pro
volbu	volba	k1gFnSc4	volba
za	za	k7c2	za
polského	polský	k2eAgMnSc2d1	polský
krále	král	k1gMnSc2	král
syna	syn	k1gMnSc4	syn
Maxmiliána	Maxmilián	k1gMnSc2	Maxmilián
II	II	kA	II
<g/>
.	.	kIx.	.
arcivévodu	arcivévoda	k1gMnSc4	arcivévoda
Arnošta	Arnošt	k1gMnSc4	Arnošt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1584	[number]	k4	1584
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
83	[number]	k4	83
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pochován	pochován	k2eAgMnSc1d1	pochován
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Tomáše	Tomáš	k1gMnSc2	Tomáš
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Majetek	majetek	k1gInSc1	majetek
==	==	k?	==
</s>
</p>
<p>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Popel	popel	k1gInSc1	popel
z	z	k7c2	z
Lobkowicz	Lobkowicz	k1gMnSc1	Lobkowicz
také	také	k9	také
stál	stát	k5eAaImAgMnS	stát
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
a	a	k8xC	a
politického	politický	k2eAgInSc2d1	politický
vzestupu	vzestup	k1gInSc2	vzestup
chlumecké	chlumecký	k2eAgFnSc2d1	Chlumecká
větve	větev	k1gFnSc2	větev
lobkowiczkého	lobkowiczký	k2eAgInSc2d1	lobkowiczký
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
panství	panství	k1gNnSc4	panství
Chlumec	Chlumec	k1gInSc1	Chlumec
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
hrabství	hrabství	k1gNnSc3	hrabství
v	v	k7c6	v
Horní	horní	k2eAgFnSc6d1	horní
Falci	Falc	k1gFnSc6	Falc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
statcích	statek	k1gInPc6	statek
podporoval	podporovat	k5eAaImAgInS	podporovat
rozvoj	rozvoj	k1gInSc1	rozvoj
řemesel	řemeslo	k1gNnPc2	řemeslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlastnil	vlastnit	k5eAaImAgInS	vlastnit
Chlumec	Chlumec	k1gInSc1	Chlumec
<g/>
,	,	kIx,	,
Sedlčany	Sedlčany	k1gInPc1	Sedlčany
<g/>
,	,	kIx,	,
Jistebnice	Jistebnice	k1gFnPc1	Jistebnice
<g/>
,	,	kIx,	,
Zbiroh	Zbiroh	k1gInSc1	Zbiroh
<g/>
,	,	kIx,	,
Krásnou	krásný	k2eAgFnSc4d1	krásná
horu	hora	k1gFnSc4	hora
<g/>
,	,	kIx,	,
Rybníky	rybník	k1gInPc4	rybník
<g/>
,	,	kIx,	,
Borotín	Borotín	k1gInSc4	Borotín
<g/>
,	,	kIx,	,
Kamým	Kamé	k1gNnSc7	Kamé
<g/>
,	,	kIx,	,
Křivoklát	Křivoklát	k1gInSc1	Křivoklát
<g/>
,	,	kIx,	,
Králův	Králův	k2eAgInSc1d1	Králův
Dvůr	Dvůr	k1gInSc1	Dvůr
<g/>
,	,	kIx,	,
Neustadt	Neustadt	k1gInSc1	Neustadt
a.	a.	k?	a.
d.	d.	k?	d.
Waldnaab	Waldnaab	k1gInSc1	Waldnaab
a	a	k8xC	a
Störnsteint	Störnsteint	k1gInSc1	Störnsteint
(	(	kIx(	(
<g/>
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rodina	rodina	k1gFnSc1	rodina
==	==	k?	==
</s>
</p>
<p>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
třikrát	třikrát	k6eAd1	třikrát
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
potomci	potomek	k1gMnPc1	potomek
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
posledního	poslední	k2eAgNnSc2d1	poslední
manželství	manželství	k1gNnSc2	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
ve	v	k7c4	v
svazek	svazek	k1gInSc4	svazek
manželský	manželský	k2eAgInSc4d1	manželský
s	s	k7c7	s
vdovou	vdova	k1gFnSc7	vdova
Benignou	Benigna	k1gFnSc7	Benigna
ze	z	k7c2	z
Starhembergu	Starhemberg	k1gInSc2	Starhemberg
(	(	kIx(	(
<g/>
1499	[number]	k4	1499
<g/>
–	–	k?	–
<g/>
1557	[number]	k4	1557
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
ze	z	k7c2	z
Starhembergu	Starhemberg	k1gInSc2	Starhemberg
(	(	kIx(	(
<g/>
1460	[number]	k4	1460
<g/>
–	–	k?	–
<g/>
1531	[number]	k4	1531
<g/>
)	)	kIx)	)
a	a	k8xC	a
Magdaleny	Magdalena	k1gFnPc1	Magdalena
z	z	k7c2	z
Losensteinu	Losenstein	k1gInSc2	Losenstein
(	(	kIx(	(
<g/>
†	†	k?	†
1532	[number]	k4	1532
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
prvním	první	k4xOgMnSc7	první
manželem	manžel	k1gMnSc7	manžel
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
ze	z	k7c2	z
Švamberka	Švamberka	k1gFnSc1	Švamberka
na	na	k7c6	na
Boru	bor	k1gInSc6	bor
(	(	kIx(	(
<g/>
†	†	k?	†
1533	[number]	k4	1533
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Veronikou	Veronika	k1gFnSc7	Veronika
z	z	k7c2	z
Harrachu	Harrach	k1gInSc2	Harrach
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
Leonarda	Leonardo	k1gMnSc2	Leonardo
z	z	k7c2	z
Harrachu	Harrach	k1gInSc2	Harrach
(	(	kIx(	(
<g/>
†	†	k?	†
1570	[number]	k4	1570
<g/>
)	)	kIx)	)
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
Alžběty	Alžběta	k1gFnSc2	Alžběta
ze	z	k7c2	z
Schrattenbachu	Schrattenbach	k1gInSc2	Schrattenbach
(	(	kIx(	(
<g/>
†	†	k?	†
1573	[number]	k4	1573
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naposled	naposled	k6eAd1	naposled
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1563	[number]	k4	1563
s	s	k7c7	s
Johanou	Johana	k1gFnSc7	Johana
Berkovnou	Berkovna	k1gFnSc7	Berkovna
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1545	[number]	k4	1545
–	–	k?	–
19	[number]	k4	19
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1601	[number]	k4	1601
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Narodilo	narodit	k5eAaPmAgNnS	narodit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
osm	osm	k4xCc4	osm
dětí	dítě	k1gFnPc2	dítě
(	(	kIx(	(
<g/>
3	[number]	k4	3
synové	syn	k1gMnPc1	syn
a	a	k8xC	a
5	[number]	k4	5
dcer	dcera	k1gFnPc2	dcera
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
mladší	mladý	k2eAgMnSc1d2	mladší
<g/>
,	,	kIx,	,
Lacek	Lacek	k1gMnSc1	Lacek
<g/>
;	;	kIx,	;
15	[number]	k4	15
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1566	[number]	k4	1566
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1621	[number]	k4	1621
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgMnSc1d1	moravský
zemský	zemský	k2eAgMnSc1d1	zemský
hejtman	hejtman	k1gMnSc1	hejtman
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
∞	∞	k?	∞
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1597	[number]	k4	1597
<g/>
)	)	kIx)	)
Anna	Anna	k1gFnSc1	Anna
Alžběta	Alžběta	k1gFnSc1	Alžběta
ze	z	k7c2	z
Salm-Neuburgu	Salm-Neuburg	k1gInSc2	Salm-Neuburg
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1565	[number]	k4	1565
–	–	k?	–
1615	[number]	k4	1615
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
∞	∞	k?	∞
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1616	[number]	k4	1616
<g/>
)	)	kIx)	)
Anna	Anna	k1gFnSc1	Anna
Marie	Marie	k1gFnSc1	Marie
Alžběta	Alžběta	k1gFnSc1	Alžběta
ze	z	k7c2	z
Salm-Neuburg	Salm-Neuburg	k1gMnSc1	Salm-Neuburg
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1598	[number]	k4	1598
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1647	[number]	k4	1647
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1568	[number]	k4	1568
–	–	k?	–
16	[number]	k4	16
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1628	[number]	k4	1628
Vídeň	Vídeň	k1gFnSc4	Vídeň
nebo	nebo	k8xC	nebo
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
pohřben	pohřben	k2eAgInSc1d1	pohřben
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
kapucínů	kapucín	k1gMnPc2	kapucín
v	v	k7c6	v
Roudnici	Roudnice	k1gFnSc6	Roudnice
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
kancléř	kancléř	k1gMnSc1	kancléř
</s>
</p>
<p>
<s>
∞	∞	k?	∞
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1603	[number]	k4	1603
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
Polyxena	Polyxena	k1gFnSc1	Polyxena
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
(	(	kIx(	(
<g/>
1566	[number]	k4	1566
–	–	k?	–
24	[number]	k4	24
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1642	[number]	k4	1642
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
1570	[number]	k4	1570
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1596	[number]	k4	1596
Keresztés	Keresztésa	k1gFnPc2	Keresztésa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
1570	[number]	k4	1570
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1615	[number]	k4	1615
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
∞	∞	k?	∞
Jindřich	Jindřich	k1gMnSc1	Jindřich
Kurcpach	Kurcpach	k1gMnSc1	Kurcpach
z	z	k7c2	z
Trachenburka	Trachenburek	k1gMnSc2	Trachenburek
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
∞	∞	k?	∞
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1606	[number]	k4	1606
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Bedřich	Bedřich	k1gMnSc1	Bedřich
z	z	k7c2	z
Oppersdorfu	Oppersdorf	k1gInSc2	Oppersdorf
(	(	kIx(	(
<g/>
†	†	k?	†
18.5	[number]	k4	18.5
<g/>
.1632	.1632	k4	.1632
Žampach	Žampacha	k1gFnPc2	Žampacha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Benigna	Benigna	k1gFnSc1	Benigna
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1569	[number]	k4	1569
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1625	[number]	k4	1625
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
∞	∞	k?	∞
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1590	[number]	k4	1590
<g/>
)	)	kIx)	)
Seyfried	Seyfried	k1gMnSc1	Seyfried
(	(	kIx(	(
<g/>
Siegfried	Siegfried	k1gMnSc1	Siegfried
<g/>
)	)	kIx)	)
I.	I.	kA	I.
z	z	k7c2	z
Promnitz	Promnitza	k1gFnPc2	Promnitza
(	(	kIx(	(
<g/>
1534	[number]	k4	1534
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1597	[number]	k4	1597
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
∞	∞	k?	∞
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1606	[number]	k4	1606
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Kavka	Kavka	k1gMnSc1	Kavka
z	z	k7c2	z
Říčan	Říčany	k1gInPc2	Říčany
(	(	kIx(	(
<g/>
†	†	k?	†
1643	[number]	k4	1643
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Zbyňka	Zbyňka	k1gFnSc1	Zbyňka
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1571	[number]	k4	1571
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1591	[number]	k4	1591
<g/>
,	,	kIx,	,
pohřben	pohřben	k2eAgInSc1d1	pohřben
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Tomáše	Tomáš	k1gMnSc2	Tomáš
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
∞	∞	k?	∞
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1590	[number]	k4	1590
<g/>
)	)	kIx)	)
Václav	Václava	k1gFnPc2	Václava
Bezdružický	Bezdružický	k2eAgInSc1d1	Bezdružický
Ludvíkovský	ludvíkovský	k2eAgInSc1d1	ludvíkovský
z	z	k7c2	z
Kolowrat	Kolowrat	k1gMnSc1	Kolowrat
(	(	kIx(	(
<g/>
†	†	k?	†
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1604	[number]	k4	1604
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Anna	Anna	k1gFnSc1	Anna
</s>
</p>
<p>
<s>
∞	∞	k?	∞
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1598	[number]	k4	1598
<g/>
)	)	kIx)	)
Jiří	Jiří	k1gMnSc1	Jiří
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
z	z	k7c2	z
Oppersdorfu	Oppersdorf	k1gInSc2	Oppersdorf
(	(	kIx(	(
<g/>
†	†	k?	†
15	[number]	k4	15
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1607	[number]	k4	1607
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Johana	Johana	k1gFnSc1	Johana
(	(	kIx(	(
<g/>
1574	[number]	k4	1574
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1639	[number]	k4	1639
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
JUŘÍK	Juřík	k1gMnSc1	Juřík
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Lobkowiczové	Lobkowicz	k1gMnPc1	Lobkowicz
<g/>
:	:	kIx,	:
Popel	popel	k1gInSc1	popel
jsem	být	k5eAaImIp1nS	být
a	a	k8xC	a
popel	popel	k1gInSc1	popel
budu	být	k5eAaImBp1nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Euromedia	Euromedium	k1gNnSc2	Euromedium
Group	Group	k1gInSc1	Group
–	–	k?	–
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
160	[number]	k4	160
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
242	[number]	k4	242
<g/>
-	-	kIx~	-
<g/>
5429	[number]	k4	5429
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
31	[number]	k4	31
<g/>
–	–	k?	–
<g/>
32	[number]	k4	32
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KASÍK	KASÍK	kA	KASÍK
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
;	;	kIx,	;
MAŠEK	Mašek	k1gMnSc1	Mašek
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
MŽYKOVÁ	MŽYKOVÁ	kA	MŽYKOVÁ
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
.	.	kIx.	.
</s>
<s>
Lobkowiczové	Lobkowicz	k1gMnPc1	Lobkowicz
<g/>
,	,	kIx,	,
dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
genealogie	genealogie	k1gFnPc1	genealogie
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
:	:	kIx,	:
Bohumír	Bohumír	k1gMnSc1	Bohumír
Němec	Němec	k1gMnSc1	Němec
-	-	kIx~	-
Veduta	veduta	k1gFnSc1	veduta
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
240	[number]	k4	240
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
903040	[number]	k4	903040
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
109	[number]	k4	109
<g/>
,	,	kIx,	,
216	[number]	k4	216
<g/>
-	-	kIx~	-
<g/>
217	[number]	k4	217
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RYANTOVÁ	RYANTOVÁ	kA	RYANTOVÁ
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
.	.	kIx.	.
</s>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Popel	popel	k1gInSc1	popel
z	z	k7c2	z
Lobkovic	Lobkovice	k1gInPc2	Lobkovice
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
prezident	prezident	k1gMnSc1	prezident
apelačního	apelační	k2eAgInSc2d1	apelační
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
VOREL	Vorel	k1gMnSc1	Vorel
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stavovský	stavovský	k2eAgInSc1d1	stavovský
odboj	odboj	k1gInSc1	odboj
roku	rok	k1gInSc2	rok
1547	[number]	k4	1547
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
krize	krize	k1gFnSc1	krize
habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Sborník	sborník	k1gInSc1	sborník
příspěvků	příspěvek	k1gInPc2	příspěvek
z	z	k7c2	z
konference	konference	k1gFnSc2	konference
konané	konaný	k2eAgFnSc2d1	konaná
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
ve	v	k7c6	v
dnech	den	k1gInPc6	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
,	,	kIx,	,
185	[number]	k4	185
<g/>
–	–	k?	–
<g/>
204	[number]	k4	204
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Rodokmen	rodokmen	k1gInSc1	rodokmen
Lobkoviců	Lobkovice	k1gMnPc2	Lobkovice
</s>
</p>
