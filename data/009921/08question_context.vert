<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Popel	popel	k1gInSc1	popel
z	z	k7c2	z
Lobkowicz	Lobkowicz	k1gMnSc1	Lobkowicz
(	(	kIx(	(
<g/>
1501	[number]	k4	1501
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1584	[number]	k4	1584
Praha-Malá	Praha-Malý	k2eAgFnSc1d1	Praha-Malá
Strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
příslušníkem	příslušník	k1gMnSc7	příslušník
chlumecké	chlumecký	k2eAgFnSc2d1	Chlumecká
větve	větev	k1gFnSc2	větev
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
rodu	rod	k1gInSc2	rod
Popelů	Popela	k1gMnPc2	Popela
z	z	k7c2	z
Lobkowicz	Lobkowicz	k1gMnSc1	Lobkowicz
<g/>
,	,	kIx,	,
vysoce	vysoce	k6eAd1	vysoce
vzdělaný	vzdělaný	k2eAgMnSc1d1	vzdělaný
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
také	také	k9	také
ovládal	ovládat	k5eAaImAgMnS	ovládat
mnoho	mnoho	k4c4	mnoho
cizích	cizí	k2eAgInPc2d1	cizí
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Zastával	zastávat	k5eAaImAgMnS	zastávat
post	post	k1gInSc4	post
dvorského	dvorský	k2eAgMnSc2d1	dvorský
maršálka	maršálek	k1gMnSc2	maršálek
<g/>
,	,	kIx,	,
prezidenta	prezident	k1gMnSc2	prezident
apelačního	apelační	k2eAgInSc2d1	apelační
soudu	soud	k1gInSc2	soud
a	a	k8xC	a
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
hofmistra	hofmistr	k1gMnSc2	hofmistr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
a	a	k8xC	a
život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jako	jako	k9	jako
druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
potomků	potomek	k1gMnPc2	potomek
Ladislava	Ladislav	k1gMnSc2	Ladislav
I.	I.	kA	I.
Popela	Popela	k1gMnSc1	Popela
z	z	k7c2	z
Lobkowicz	Lobkowicz	k1gMnSc1	Lobkowicz
(	(	kIx(	(
<g/>
†	†	k?	†
1505	[number]	k4	1505
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
manželky	manželka	k1gFnPc1	manželka
Anny	Anna	k1gFnSc2	Anna
Krajířové	Krajířová	k1gFnSc2	Krajířová
z	z	k7c2	z
Krajku	krajek	k1gInSc2	krajek
(	(	kIx(	(
<g/>
†	†	k?	†
1520	[number]	k4	1520
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Habsburků	Habsburk	k1gMnPc2	Habsburk
spojil	spojit	k5eAaPmAgMnS	spojit
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
se	se	k3xPyFc4	se
službou	služba	k1gFnSc7	služba
tomuto	tento	k3xDgInSc3	tento
panovnickému	panovnický	k2eAgInSc3d1	panovnický
rodu	rod	k1gInSc3	rod
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
byl	být	k5eAaImAgInS	být
válečným	válečný	k2eAgMnSc7d1	válečný
komisařem	komisař	k1gMnSc7	komisař
Karla	Karel	k1gMnSc2	Karel
V.	V.	kA	V.
a	a	k8xC	a
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
ve	v	k7c6	v
vojenském	vojenský	k2eAgNnSc6d1	vojenské
tažení	tažení	k1gNnSc6	tažení
proti	proti	k7c3	proti
Turkům	turek	k1gInPc3	turek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1532	[number]	k4	1532
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1534	[number]	k4	1534
se	se	k3xPyFc4	se
s	s	k7c7	s
nevelkým	velký	k2eNgInSc7d1	nevelký
úspěchem	úspěch	k1gInSc7	úspěch
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
králova	králův	k2eAgNnSc2d1	královo
úsilí	úsilí	k1gNnSc2	úsilí
o	o	k7c4	o
zlomení	zlomení	k1gNnSc4	zlomení
odporu	odpor	k1gInSc2	odpor
měst	město	k1gNnPc2	město
proti	proti	k7c3	proti
nové	nový	k2eAgFnSc3d1	nová
dani	daň	k1gFnSc3	daň
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1536	[number]	k4	1536
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
královským	královský	k2eAgMnSc7d1	královský
radou	rada	k1gMnSc7	rada
a	a	k8xC	a
komořím	komoří	k1gMnSc7	komoří
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1542	[number]	k4	1542
dvorním	dvorní	k2eAgMnSc7d1	dvorní
maršálkem	maršálek	k1gMnSc7	maršálek
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
I.	I.	kA	I.
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
tažení	tažení	k1gNnSc4	tažení
proti	proti	k7c3	proti
Janu	Jan	k1gMnSc3	Jan
Fridrichovi	Fridrich	k1gMnSc3	Fridrich
Saskému	saský	k2eAgMnSc3d1	saský
a	a	k8xC	a
vojskům	vojsko	k1gNnPc3	vojsko
šmalkaldského	šmalkaldský	k2eAgInSc2d1	šmalkaldský
spolku	spolek	k1gInSc2	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Účastnil	účastnit	k5eAaImAgInS	účastnit
se	se	k3xPyFc4	se
soudu	soud	k1gInSc2	soud
nad	nad	k7c7	nad
královskými	královský	k2eAgNnPc7d1	královské
městy	město	k1gNnPc7	město
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1547	[number]	k4	1547
a	a	k8xC	a
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
jej	on	k3xPp3gNnSc4	on
také	také	k9	také
pověřil	pověřit	k5eAaPmAgMnS	pověřit
jako	jako	k8xS	jako
královského	královský	k2eAgMnSc2d1	královský
komisaře	komisař	k1gMnSc2	komisař
provedením	provedení	k1gNnSc7	provedení
konfiskace	konfiskace	k1gFnSc2	konfiskace
statků	statek	k1gInPc2	statek
Kašpara	Kašpar	k1gMnSc2	Kašpar
Pluha	Pluh	k1gMnSc2	Pluh
z	z	k7c2	z
Rabštejna	Rabštejn	k1gInSc2	Rabštejn
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1548	[number]	k4	1548
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
prezidenta	prezident	k1gMnSc2	prezident
nově	nově	k6eAd1	nově
zřízené	zřízený	k2eAgFnPc1d1	zřízená
rady	rada	k1gFnPc1	rada
nad	nad	k7c7	nad
apelacemi	apelace	k1gFnPc7	apelace
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1549	[number]	k4	1549
byl	být	k5eAaImAgInS	být
také	také	k9	také
přísedícím	přísedící	k1gMnSc7	přísedící
komorního	komorní	k2eAgInSc2d1	komorní
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>

