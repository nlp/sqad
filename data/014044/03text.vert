<s>
Americium	americium	k1gNnSc1
</s>
<s>
Americium	americium	k1gNnSc1
</s>
<s>
[	[	kIx(
<g/>
Rn	Rn	k1gMnSc6
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
7	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
243	#num#	k4
</s>
<s>
Am	Am	k?
</s>
<s>
95	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Americium	americium	k1gNnSc1
pod	pod	k7c7
mikroskopem	mikroskop	k1gInSc7
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Americium	americium	k1gNnSc1
<g/>
,	,	kIx,
Am	Am	k1gFnSc1
<g/>
,	,	kIx,
95	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Americium	americium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
f	f	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Koncentrace	koncentrace	k1gFnPc1
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
</s>
<s>
7	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
13	#num#	k4
ppm	ppm	k?
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
stříbřitě	stříbřitě	k6eAd1
bílé	bílý	k2eAgFnPc1d1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7440-35-9	7440-35-9	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
241,056	241,056	k4
7	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
243,061	243,061	k4
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Atomový	atomový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
173	#num#	k4
pm	pm	k?
</s>
<s>
Kovalentní	kovalentní	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
184	#num#	k4
pm	pm	k?
</s>
<s>
Van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Waalsův	Waalsův	k2eAgMnSc1d1
poloměr	poloměr	k1gInSc4
</s>
<s>
228,5	228,5	k4
pm	pm	k?
</s>
<s>
Iontový	iontový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
(	(	kIx(
<g/>
Am	Am	k1gFnPc2
<g/>
3	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
107	#num#	k4
pm	pm	k?
<g/>
(	(	kIx(
<g/>
Am	Am	k1gFnSc1
<g/>
4	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
92	#num#	k4
pm	pm	k?
<g/>
(	(	kIx(
<g/>
Am	Am	k1gFnSc1
<g/>
6	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
71	#num#	k4
pm	pm	k?
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
Rn	Rn	k1gMnSc6
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
7	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
II	II	kA
<g/>
,	,	kIx,
III	III	kA
<g/>
,	,	kIx,
IV	IV	kA
<g/>
,	,	kIx,
V	V	kA
<g/>
,	,	kIx,
VI	VI	kA
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1,3	1,3	k4
</s>
<s>
Ionizační	ionizační	k2eAgFnSc1d1
energie	energie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
</s>
<s>
5,5	5,5	k4
eV	eV	k?
</s>
<s>
Druhá	druhý	k4xOgFnSc1
</s>
<s>
10,9	10,9	k4
eV	eV	k?
</s>
<s>
Třetí	třetí	k4xOgFnSc1
</s>
<s>
23,9	23,9	k4
eV	eV	k?
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1
</s>
<s>
41,0	41,0	k4
eV	eV	k?
</s>
<s>
Látkové	látkový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Krystalografická	krystalografický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
α	α	k1gFnSc1
hexagonální	hexagonální	k2eAgFnSc1d1
a	a	k8xC
<g/>
=	=	kIx~
346,81	346,81	k4
pm	pm	k?
c	c	k0
<g/>
=	=	kIx~
1	#num#	k4
124,1	124,1	k4
pm	pm	k?
β	β	k1gFnSc1
kubická	kubický	k2eAgFnSc1d1
(	(	kIx(
<g/>
stálá	stálý	k2eAgFnSc1d1
nad	nad	k7c4
600	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
a	a	k8xC
<g/>
=	=	kIx~
489,4	489,4	k4
pm	pm	k?
</s>
<s>
Molární	molární	k2eAgInSc1d1
objem	objem	k1gInSc1
</s>
<s>
17,78	17,78	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
6	#num#	k4
m	m	kA
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
mol	mol	k1gInSc1
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
13,78	13,78	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
α	α	k?
-	-	kIx~
mod.	mod.	k?
<g/>
)	)	kIx)
13,67	13,67	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
β	β	k?
-	-	kIx~
mod.	mod.	k?
<g/>
)	)	kIx)
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Tlak	tlak	k1gInSc1
syté	sytý	k2eAgFnSc2d1
páry	pára	k1gFnSc2
</s>
<s>
1	#num#	k4
Pa	Pa	kA
(	(	kIx(
<g/>
966	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
10	#num#	k4
Pa	Pa	kA
(	(	kIx(
<g/>
1	#num#	k4
083	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Termické	termický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Tepelná	tepelný	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
10	#num#	k4
W	W	kA
<g/>
⋅	⋅	k?
<g/>
m	m	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
⋅	⋅	k?
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Molární	molární	k2eAgFnSc1d1
atomizační	atomizační	k2eAgFnSc1d1
entalpie	entalpie	k1gFnSc1
</s>
<s>
252	#num#	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1
molární	molární	k2eAgFnSc1d1
entropie	entropie	k1gFnSc1
S	s	k7c7
<g/>
°	°	k?
</s>
<s>
54	#num#	k4
J	J	kA
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
994	#num#	k4
±	±	k?
4	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
1	#num#	k4
267,15	267,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
2	#num#	k4
460	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
2	#num#	k4
733,15	733,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Specifické	specifický	k2eAgNnSc4d1
teplo	teplo	k1gNnSc4
tání	tání	k1gNnSc2
</s>
<s>
41	#num#	k4
J	J	kA
<g/>
/	/	kIx~
<g/>
K	k	k7c3
</s>
<s>
Specifické	specifický	k2eAgNnSc4d1
teplo	teplo	k1gNnSc4
varu	var	k1gInSc2
</s>
<s>
890	#num#	k4
J	J	kA
<g/>
/	/	kIx~
<g/>
K	k	k7c3
</s>
<s>
Měrná	měrný	k2eAgFnSc1d1
tepelná	tepelný	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
</s>
<s>
62,7	62,7	k4
J	J	kA
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Měrný	měrný	k2eAgInSc4d1
elektrický	elektrický	k2eAgInSc4d1
odpor	odpor	k1gInSc4
</s>
<s>
0,69	0,69	k4
µ	µ	k?
m	m	kA
</s>
<s>
Standardní	standardní	k2eAgInSc1d1
elektrodový	elektrodový	k2eAgInSc1d1
potenciál	potenciál	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
Am	Am	k1gFnPc2
<g/>
3	#num#	k4
<g/>
+	+	kIx~
+	+	kIx~
3	#num#	k4
e	e	k0
<g/>
−	−	k?
→	→	k?
Am	Am	k1gFnSc1
<g/>
)	)	kIx)
−	−	k?
V	v	k7c6
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1
chování	chování	k1gNnSc1
</s>
<s>
paramagnetický	paramagnetický	k2eAgInSc1d1
</s>
<s>
Měrná	měrný	k2eAgFnSc1d1
magnetická	magnetický	k2eAgFnSc1d1
susceptibilita	susceptibilita	k1gFnSc1
</s>
<s>
4	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
6	#num#	k4
cm	cm	kA
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
g	g	kA
(	(	kIx(
<g/>
27	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Radioaktivní	radioaktivní	k2eAgFnSc1d1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
{{{	{{{	k?
<g/>
izotopy	izotop	k1gInPc1
<g/>
}}}	}}}	k?
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Eu	Eu	k?
<g/>
⋏	⋏	k?
</s>
<s>
Plutonium	plutonium	k1gNnSc1
≺	≺	k?
<g/>
Am	Am	k1gMnSc2
<g/>
≻	≻	k?
Curium	curium	k1gNnSc1
</s>
<s>
Americium	americium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Am	Am	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
sedmým	sedmý	k4xOgInSc7
členem	člen	k1gInSc7
z	z	k7c2
řady	řada	k1gFnSc2
aktinoidů	aktinoid	k1gInPc2
<g/>
,	,	kIx,
třetím	třetí	k4xOgInSc7
transuranem	transuran	k1gInSc7
<g/>
,	,	kIx,
silně	silně	k6eAd1
radioaktivní	radioaktivní	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
připravovaný	připravovaný	k2eAgInSc1d1
uměle	uměle	k6eAd1
v	v	k7c6
jaderných	jaderný	k2eAgInPc6d1
reaktorech	reaktor	k1gInPc6
především	především	k9
z	z	k7c2
plutonia	plutonium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnPc1d1
fyzikálně-chemické	fyzikálně-chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Americium	americium	k1gNnSc1
je	být	k5eAaImIp3nS
radioaktivní	radioaktivní	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
stříbřitě	stříbřitě	k6eAd1
bílé	bílý	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
působením	působení	k1gNnSc7
vzdušného	vzdušný	k2eAgInSc2d1
kyslíku	kyslík	k1gInSc2
mění	měnit	k5eAaImIp3nS
na	na	k7c4
šedavou	šedavý	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
mechanické	mechanický	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
je	být	k5eAaImIp3nS
tvárnější	tvárný	k2eAgInSc1d2
než	než	k8xS
příbuzný	příbuzný	k2eAgInSc1d1
uran	uran	k1gInSc1
nebo	nebo	k8xC
neptunium	neptunium	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyzařuje	vyzařovat	k5eAaImIp3nS
α	α	k?
a	a	k8xC
γ	γ	k?
záření	záření	k1gNnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
proto	proto	k8xC
nutno	nutno	k6eAd1
s	s	k7c7
ním	on	k3xPp3gMnSc7
manipulovat	manipulovat	k5eAaImF
za	za	k7c4
dodržování	dodržování	k1gNnSc4
bezpečnostních	bezpečnostní	k2eAgNnPc2d1
opatření	opatření	k1gNnPc2
pro	pro	k7c4
práci	práce	k1gFnSc4
s	s	k7c7
radioaktivními	radioaktivní	k2eAgInPc7d1
materiály	materiál	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
sloučeninách	sloučenina	k1gFnPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
mocenství	mocenství	k1gNnSc6
od	od	k7c2
Am	Am	k1gFnSc2
<g/>
2	#num#	k4
<g/>
+	+	kIx~
po	po	k7c6
Am	Am	k1gFnSc6
<g/>
6	#num#	k4
<g/>
+	+	kIx~
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
nejstálejší	stálý	k2eAgFnPc1d3
jsou	být	k5eAaImIp3nP
sloučeniny	sloučenina	k1gFnPc1
v	v	k7c6
oxidačním	oxidační	k2eAgNnSc6d1
čísle	číslo	k1gNnSc6
+3	+3	k4
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
<g/>
,	,	kIx,
výroba	výroba	k1gFnSc1
<g/>
,	,	kIx,
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Americium	americium	k1gNnSc1
se	se	k3xPyFc4
v	v	k7c6
přírodě	příroda	k1gFnSc6
přirozeně	přirozeně	k6eAd1
nevyskytuje	vyskytovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
uměle	uměle	k6eAd1
připravený	připravený	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
tzv.	tzv.	kA
transuran	transuran	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
množství	množství	k1gNnSc1
americia	americium	k1gNnSc2
bylo	být	k5eAaImAgNnS
uvolněno	uvolnit	k5eAaPmNgNnS
do	do	k7c2
prostředí	prostředí	k1gNnSc2
v	v	k7c6
důsledku	důsledek	k1gInSc6
černobylské	černobylský	k2eAgFnSc2d1
havárie	havárie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
především	především	k9
v	v	k7c6
oblasti	oblast	k1gFnSc6
Uzavřené	uzavřený	k2eAgFnSc2d1
zóny	zóna	k1gFnSc2
Černobylské	černobylský	k2eAgFnSc2d1
jaderné	jaderný	k2eAgFnSc2d1
elektrárny	elektrárna	k1gFnSc2
(	(	kIx(
<g/>
Ukrajina	Ukrajina	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
Poleské	Poleský	k2eAgFnPc1d1
státní	státní	k2eAgFnPc1d1
radiačně-ekologické	radiačně-ekologický	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
(	(	kIx(
<g/>
Bělorusko	Bělorusko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izotop	izotop	k1gInSc1
241	#num#	k4
<g/>
Am	Am	k1gFnPc2
s	s	k7c7
poločasem	poločas	k1gInSc7
rozpadu	rozpad	k1gInSc2
432,6	432,6	k4
let	léto	k1gNnPc2
vzniká	vznikat	k5eAaImIp3nS
rozpadem	rozpad	k1gInSc7
241	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
s	s	k7c7
poločasem	poločas	k1gInSc7
rozpadu	rozpad	k1gInSc2
14,4	14,4	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
241	#num#	k4
<g/>
Am	Am	k1gFnPc2
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
jediný	jediný	k2eAgInSc1d1
radioizotop	radioizotop	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc4
koncentrace	koncentrace	k1gFnSc1
v	v	k7c6
půdě	půda	k1gFnSc6
se	se	k3xPyFc4
s	s	k7c7
časem	čas	k1gInSc7
zvyšuje	zvyšovat	k5eAaImIp3nS
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
je	být	k5eAaImIp3nS
jako	jako	k8xS,k8xC
alfa	alfa	k1gFnSc1
zářič	zářič	k1gInSc1
vzhledem	vzhledem	k7c3
k	k	k7c3
241	#num#	k4
<g/>
Pu	Pu	k1gFnSc2
(	(	kIx(
<g/>
beta	beta	k1gNnSc1
zářič	zářič	k1gInSc1
<g/>
)	)	kIx)
mnohem	mnohem	k6eAd1
toxičtější	toxický	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Očekává	očekávat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gFnSc1
koncentrace	koncentrace	k1gFnSc1
v	v	k7c6
Poleské	Poleský	k2eAgFnSc6d1
státní	státní	k2eAgFnSc6d1
radiačně-ekologické	radiačně-ekologický	k2eAgFnSc6d1
rezervaci	rezervace	k1gFnSc6
se	se	k3xPyFc4
bude	být	k5eAaImBp3nS
zvyšovat	zvyšovat	k5eAaImF
přibližně	přibližně	k6eAd1
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
2060	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Americium	americium	k1gNnSc1
bylo	být	k5eAaImAgNnS
poprvé	poprvé	k6eAd1
připraveno	připravit	k5eAaPmNgNnS
roku	rok	k1gInSc2
1944	#num#	k4
bombardováním	bombardování	k1gNnSc7
239	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
neutrony	neutron	k1gInPc1
v	v	k7c6
jaderné	jaderný	k2eAgFnSc6d1
laboratoři	laboratoř	k1gFnSc6
chicagské	chicagský	k2eAgFnSc2d1
university	universita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
jeho	jeho	k3xOp3gMnPc4
objevitele	objevitel	k1gMnPc4
jsou	být	k5eAaImIp3nP
označováni	označovat	k5eAaImNgMnP
Glenn	Glenna	k1gFnPc2
T.	T.	kA
Seaborg	Seaborg	k1gInSc1
<g/>
,	,	kIx,
Leon	Leona	k1gFnPc2
O.	O.	kA
Morgan	morgan	k1gMnSc1
<g/>
,	,	kIx,
Ralph	Ralph	k1gMnSc1
A.	A.	kA
James	James	k1gMnSc1
a	a	k8xC
Albert	Albert	k1gMnSc1
Ghiorso	Ghiorsa	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvek	prvek	k1gInSc1
byl	být	k5eAaImAgInS
poté	poté	k6eAd1
pojmenován	pojmenovat	k5eAaPmNgInS
podle	podle	k7c2
světadílu	světadíl	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
byl	být	k5eAaImAgInS
vyroben	vyrobit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
Kovové	kovový	k2eAgNnSc1d1
americium	americium	k1gNnSc1
se	se	k3xPyFc4
připravuje	připravovat	k5eAaImIp3nS
redukcí	redukce	k1gFnSc7
fluoridu	fluorid	k1gInSc2
americitého	americitý	k2eAgMnSc4d1
AmF	AmF	k1gMnSc4
<g/>
3	#num#	k4
parami	para	k1gFnPc7
barya	baryum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
charakterizováno	charakterizovat	k5eAaBmNgNnS
20	#num#	k4
izotopů	izotop	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
jsou	být	k5eAaImIp3nP
nejstabilnější	stabilní	k2eAgInSc4d3
243	#num#	k4
<g/>
Am	Am	k1gFnPc2
s	s	k7c7
poločasem	poločas	k1gInSc7
rozpadu	rozpad	k1gInSc2
7364	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
241	#num#	k4
<g/>
Am	Am	k1gFnPc2
s	s	k7c7
poločasem	poločas	k1gInSc7
432,6	432,6	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc4
zbývající	zbývající	k2eAgInPc1d1
radioaktivní	radioaktivní	k2eAgInPc1d1
izotopy	izotop	k1gInPc1
mají	mít	k5eAaImIp3nP
poločas	poločas	k1gInSc4
rozpadu	rozpad	k1gInSc2
méně	málo	k6eAd2
než	než	k8xS
51	#num#	k4
hodin	hodina	k1gFnPc2
a	a	k8xC
většina	většina	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
dokonce	dokonce	k9
méně	málo	k6eAd2
než	než	k8xS
100	#num#	k4
minut	minuta	k1gFnPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
:	:	kIx,
</s>
<s>
IzotopPoločas	IzotopPoločas	k1gMnSc1
přeměnyDruh	přeměnyDruh	k1gMnSc1
přeměnyProdukt	přeměnyProdukt	k1gInSc4
přeměny	přeměna	k1gFnSc2
</s>
<s>
230	#num#	k4
<g/>
Am	Am	k1gFnSc1
<g/>
≈	≈	k?
<g/>
17	#num#	k4
sε	sε	k?
<g/>
230	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
</s>
<s>
231	#num#	k4
<g/>
Am	Am	k1gFnPc2
?	?	kIx.
</s>
<s desamb="1">
<g/>
ε	ε	k?
(	(	kIx(
<g/>
?	?	kIx.
</s>
<s desamb="1">
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
?	?	kIx.
</s>
<s desamb="1">
%	%	kIx~
<g/>
)	)	kIx)
<g/>
231	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
/	/	kIx~
227	#num#	k4
<g/>
Np	Np	k1gFnPc2
</s>
<s>
232	#num#	k4
<g/>
Am	Am	k1gFnSc1
<g/>
79	#num#	k4
sε	sε	k?
(	(	kIx(
<g/>
97	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
3	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
232	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
(	(	kIx(
<g/>
ε	ε	k?
)	)	kIx)
<g/>
/	/	kIx~
228	#num#	k4
<g/>
Np	Np	k1gFnPc2
(	(	kIx(
<g/>
α	α	k?
<g/>
)	)	kIx)
</s>
<s>
233	#num#	k4
<g/>
Am	Am	k1gFnSc1
<g/>
3,2	3,2	k4
mε	mε	k?
(	(	kIx(
<g/>
<	<	kIx(
<g/>
97	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
>	>	kIx)
<g/>
3	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
233	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
(	(	kIx(
<g/>
ε	ε	k?
<g/>
)	)	kIx)
/	/	kIx~
229	#num#	k4
<g/>
Np	Np	k1gFnPc2
(	(	kIx(
<g/>
α	α	k?
<g/>
)	)	kIx)
</s>
<s>
234	#num#	k4
<g/>
Am	Am	k1gFnSc1
<g/>
2,32	2,32	k4
mε	mε	k?
(	(	kIx(
<g/>
100	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
?	?	kIx.
</s>
<s desamb="1">
%	%	kIx~
<g/>
)	)	kIx)
<g/>
234	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
/	/	kIx~
230	#num#	k4
<g/>
Np	Np	k1gFnPc2
</s>
<s>
235	#num#	k4
<g/>
Am	Am	k1gFnSc1
<g/>
10,3	10,3	k4
mε	mε	k?
(	(	kIx(
<g/>
99,6	99,6	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
0,4	0,4	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
235	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
/	/	kIx~
231	#num#	k4
<g/>
Np	Np	k1gFnPc2
</s>
<s>
236	#num#	k4
<g/>
Am	Am	k1gFnSc1
<g/>
3,6	3,6	k4
mε	mε	k?
(	(	kIx(
<g/>
?	?	kIx.
</s>
<s desamb="1">
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
?	?	kIx.
</s>
<s desamb="1">
%	%	kIx~
<g/>
)	)	kIx)
<g/>
236	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
/	/	kIx~
232	#num#	k4
<g/>
Np	Np	k1gFnPc2
</s>
<s>
237	#num#	k4
<g/>
Am	Am	k1gFnSc1
<g/>
73,6	73,6	k4
mε	mε	k?
(	(	kIx(
<g/>
99,97	99,97	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
0,03	0,03	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
237	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
/	/	kIx~
233	#num#	k4
<g/>
Np	Np	k1gFnPc2
</s>
<s>
238	#num#	k4
<g/>
Am	Am	k1gFnSc1
<g/>
98	#num#	k4
mε	mε	k?
(	(	kIx(
<g/>
100,00	100,00	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
4	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
238	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
/	/	kIx~
234	#num#	k4
<g/>
Np	Np	k1gFnPc2
</s>
<s>
239	#num#	k4
<g/>
Am	Am	k1gFnSc1
<g/>
11,9	11,9	k4
hε	hε	k?
(	(	kIx(
<g/>
99,99	99,99	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
0,01	0,01	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
239	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
/	/	kIx~
235	#num#	k4
<g/>
Np	Np	k1gFnPc2
</s>
<s>
240	#num#	k4
<g/>
Am	Am	k1gFnSc1
<g/>
50,8	50,8	k4
hε	hε	k?
(	(	kIx(
<g/>
100,00	100,00	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
1,9	1,9	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
4	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
240	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
/	/	kIx~
236	#num#	k4
<g/>
Np	Np	k1gFnPc2
</s>
<s>
241	#num#	k4
<g/>
Am	Am	k1gFnSc1
<g/>
432,6	432,6	k4
rα	rα	k?
(	(	kIx(
<g/>
100,00	100,00	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
<g/>
SF	SF	kA
(	(	kIx(
<g/>
4	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
10	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
237	#num#	k4
<g/>
Np	Np	k1gFnPc2
/	/	kIx~
různé	různý	k2eAgInPc1d1
</s>
<s>
242	#num#	k4
<g/>
Am	Am	k1gFnSc1
<g/>
16,02	16,02	k4
hβ	hβ	k?
<g/>
−	−	k?
(	(	kIx(
<g/>
82,7	82,7	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
ε	ε	k?
(	(	kIx(
<g/>
17,3	17,3	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
242	#num#	k4
<g/>
Cm	cm	kA
/	/	kIx~
242	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
</s>
<s>
243	#num#	k4
<g/>
Am	Am	k1gFnSc1
<g/>
7364	#num#	k4
rα	rα	k?
(	(	kIx(
<g/>
100,00	100,00	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
SF	SF	kA
(	(	kIx(
<g/>
3,7	3,7	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
9	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
239	#num#	k4
<g/>
Np	Np	k1gFnPc2
/	/	kIx~
různé	různý	k2eAgInPc1d1
</s>
<s>
244	#num#	k4
<g/>
Am	Am	k1gFnSc1
<g/>
10,1	10,1	k4
hβ	hβ	k?
<g/>
−	−	k?
<g/>
244	#num#	k4
<g/>
Cm	cm	kA
</s>
<s>
245	#num#	k4
<g/>
Am	Am	k1gFnSc1
<g/>
2,05	2,05	k4
hβ	hβ	k?
<g/>
−	−	k?
<g/>
245	#num#	k4
<g/>
Cm	cm	kA
</s>
<s>
246	#num#	k4
<g/>
Am	Am	k1gFnSc1
<g/>
39	#num#	k4
mβ	mβ	k?
<g/>
−	−	k?
<g/>
246	#num#	k4
<g/>
Cm	cm	kA
</s>
<s>
247	#num#	k4
<g/>
Am	Am	k1gFnSc1
<g/>
23,0	23,0	k4
mβ	mβ	k?
<g/>
−	−	k?
<g/>
247	#num#	k4
<g/>
Cm	cm	kA
</s>
<s>
248	#num#	k4
<g/>
Am	Am	k1gFnSc1
<g/>
≈	≈	k?
<g/>
10	#num#	k4
mβ	mβ	k?
<g/>
−	−	k?
<g/>
248	#num#	k4
<g/>
Cm	cm	kA
</s>
<s>
249	#num#	k4
<g/>
Am	Am	k1gFnPc2
?	?	kIx.
</s>
<s desamb="1">
<g/>
β	β	k?
<g/>
−	−	k?
<g/>
249	#num#	k4
<g/>
Cm	cm	kA
</s>
<s>
Všechny	všechen	k3xTgInPc1
izotopy	izotop	k1gInPc1
americia	americium	k1gNnSc2
jsou	být	k5eAaImIp3nP
radioaktivní	radioaktivní	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
<g/>
,	,	kIx,
sloučeniny	sloučenina	k1gFnPc1
</s>
<s>
Americium	americium	k1gNnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
v	v	k7c6
přesných	přesný	k2eAgInPc6d1
měřících	měřící	k2eAgInPc6d1
přístrojích	přístroj	k1gInPc6
a	a	k8xC
v	v	k7c6
detektorech	detektor	k1gInPc6
kouře	kouř	k1gInSc2
jako	jako	k8xC,k8xS
zdroj	zdroj	k1gInSc4
α	α	k1gFnPc2
nebo	nebo	k8xC
γ	γ	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lékařství	lékařství	k1gNnSc6
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
při	při	k7c6
léčbě	léčba	k1gFnSc6
nádorů	nádor	k1gInPc2
štítné	štítný	k2eAgFnSc2d1
žlázy	žláza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnSc7d3
sloučeninou	sloučenina	k1gFnSc7
americia	americium	k1gNnSc2
je	být	k5eAaImIp3nS
oxid	oxid	k1gInSc4
americičitý	americičitý	k2eAgInSc4d1
AmO	AmO	k1gFnSc7
<g/>
2	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
výchozí	výchozí	k2eAgFnSc7d1
surovinou	surovina	k1gFnSc7
pro	pro	k7c4
přípravu	příprava	k1gFnSc4
ostatních	ostatní	k2eAgFnPc2d1
sloučenin	sloučenina	k1gFnPc2
tohoto	tento	k3xDgInSc2
prvku	prvek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k1gNnSc1
v	v	k7c6
praxi	praxe	k1gFnSc6
významnou	významný	k2eAgFnSc7d1
sloučeninou	sloučenina	k1gFnSc7
je	být	k5eAaImIp3nS
fluorid	fluorid	k1gInSc4
americitý	americitý	k2eAgInSc4d1
AmF	AmF	k1gFnSc7
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dalším	další	k2eAgNnSc7d1
využitím	využití	k1gNnSc7
jsou	být	k5eAaImIp3nP
externí	externí	k2eAgInPc4d1
neutronové	neutronový	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
pro	pro	k7c4
startování	startování	k1gNnSc4
jaderných	jaderný	k2eAgInPc2d1
reaktorů	reaktor	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
reaktoru	reaktor	k1gInSc2
LR-	LR-	k1gFnSc2
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
CV	CV	kA
Řež	řež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
О	О	k?
з	з	k?
<g/>
.	.	kIx.
www.zapovednik.by	www.zapovednik.ba	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.nndc.bnl.gov/chart/	http://www.nndc.bnl.gov/chart/	k?
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
cvrez	cvrez	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
II	II	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnPc1
americium	americium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
americium	americium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
americium	americium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
VOHLÍDAL	VOHLÍDAL	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
ŠTULÍK	štulík	k1gInSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
;	;	kIx,
JULÁK	JULÁK	kA
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemické	chemický	k2eAgFnPc4d1
a	a	k8xC
analytické	analytický	k2eAgFnPc4d1
tabulky	tabulka	k1gFnPc4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7169	#num#	k4
<g/>
-	-	kIx~
<g/>
855	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4191405-3	4191405-3	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85004443	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85004443	#num#	k4
</s>
