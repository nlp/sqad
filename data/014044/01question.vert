<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
jmenuje	jmenovat	k5eAaImIp3nS
silně	silně	k6eAd1
radioaktivní	radioaktivní	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
se	se	k3xPyFc4
připravuje	připravovat	k5eAaImIp3nS
uměle	uměle	k6eAd1
v	v	k7c6
jaderných	jaderný	k2eAgInPc6d1
reaktorech	reaktor	k1gInPc6
především	především	k6eAd1
z	z	k7c2
plutonia	plutonium	k1gNnSc2
<g/>
?	?	kIx.
</s>