<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
federace	federace	k1gFnSc1	federace
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
následnickým	následnický	k2eAgInSc7d1	následnický
státem	stát	k1gInSc7	stát
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
rozpadl	rozpadnout	k5eAaPmAgInS	rozpadnout
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
;	;	kIx,	;
převzala	převzít	k5eAaPmAgFnS	převzít
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vůdčím	vůdčí	k2eAgInSc7d1	vůdčí
členem	člen	k1gInSc7	člen
Společenství	společenství	k1gNnSc2	společenství
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
