<s>
Vojenský	vojenský	k2eAgInSc1d1
újezd	újezd	k1gInSc1
Březina	Březina	k1gMnSc1
</s>
<s>
Vojenský	vojenský	k2eAgInSc1d1
újezd	újezd	k1gInSc1
Březina	Březina	k1gMnSc1
Les	les	k1gInSc1
a	a	k8xC
vojenská	vojenský	k2eAgFnSc1d1
silnice	silnice	k1gFnSc1
ve	v	k7c6
Vojenském	vojenský	k2eAgInSc6d1
újezdu	újezd	k1gInSc6
Březina	Březina	k1gMnSc1
</s>
<s>
znakvlajka	znakvlajka	k1gFnSc1
Lokalita	lokalita	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
vojenský	vojenský	k2eAgInSc1d1
újezd	újezd	k1gInSc1
LAU	LAU	kA
2	#num#	k4
(	(	kIx(
<g/>
obec	obec	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
CZ0646	CZ0646	k4
592935	#num#	k4
Pověřená	pověřený	k2eAgFnSc1d1
obec	obec	k1gFnSc1
</s>
<s>
vojenský	vojenský	k2eAgInSc1d1
újezd	újezd	k1gInSc1
Březina	Březina	k1gFnSc1
Obec	obec	k1gFnSc1
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
</s>
<s>
Vyškov	Vyškov	k1gInSc1
Okres	okres	k1gInSc1
(	(	kIx(
<g/>
LAU	LAU	kA
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vyškov	Vyškov	k1gInSc1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
646	#num#	k4
<g/>
)	)	kIx)
Kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
NUTS	NUTS	kA
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jihomoravský	jihomoravský	k2eAgMnSc1d1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
64	#num#	k4
<g/>
)	)	kIx)
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Morava	Morava	k1gFnSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
22	#num#	k4
<g/>
′	′	k?
<g/>
6	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
57	#num#	k4
<g/>
′	′	k?
<g/>
39	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
0	#num#	k4
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rozloha	rozloha	k1gFnSc1
</s>
<s>
158,206	158,206	k4
km²	km²	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
674	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
PSČ	PSČ	kA
</s>
<s>
682	#num#	k4
01	#num#	k4
Počet	počet	k1gInSc1
částí	část	k1gFnPc2
obce	obec	k1gFnSc2
</s>
<s>
1	#num#	k4
Počet	počet	k1gInSc1
k.	k.	k?
ú.	ú.	k?
</s>
<s>
6	#num#	k4
Počet	počet	k1gInSc1
ZSJ	ZSJ	kA
</s>
<s>
6	#num#	k4
Kontakt	kontakt	k1gInSc1
Adresa	adresa	k1gFnSc1
újezdního	újezdní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
</s>
<s>
Víta	Víta	k1gFnSc1
Nejedlého	Nejedlého	k2eAgInPc2d1
<g/>
682	#num#	k4
01	#num#	k4
Vyškov	Vyškov	k1gInSc1
uurvu.brezina@army.cz	uurvu.brezina@army.cz	k1gMnSc1
Přednosta	přednosta	k1gMnSc1
</s>
<s>
pplk.	pplk.	kA
Ing.	ing.	kA
Radek	Radek	k1gMnSc1
Malaník	Malaník	k1gMnSc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
:	:	kIx,
www.vojujezd-brezina.cz	www.vojujezd-brezina.cz	k1gInSc1
</s>
<s>
Vojenský	vojenský	k2eAgInSc1d1
újezd	újezd	k1gInSc1
Březina	Březina	k1gMnSc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Kód	kód	k1gInSc4
části	část	k1gFnPc1
obce	obec	k1gFnSc2
</s>
<s>
14125	#num#	k4
Geodata	Geodata	k1gFnSc1
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vojenský	vojenský	k2eAgInSc1d1
újezd	újezd	k1gInSc1
Březina	Březina	k1gFnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Birkicht	Birkicht	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
též	též	k9
vojenský	vojenský	k2eAgInSc4d1
výcvikový	výcvikový	k2eAgInSc4d1
prostor	prostor	k1gInSc4
Dědice	dědic	k1gMnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
vojenský	vojenský	k2eAgInSc1d1
újezd	újezd	k1gInSc1
na	na	k7c6
severu	sever	k1gInSc6
okresu	okres	k1gInSc2
Vyškov	Vyškov	k1gInSc1
v	v	k7c6
lesnatém	lesnatý	k2eAgNnSc6d1
území	území	k1gNnSc6
Drahanské	Drahanský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Území	území	k1gNnSc1
újezdu	újezd	k1gInSc2
zahrnuje	zahrnovat	k5eAaImIp3nS
158,206	158,206	k4
km²	km²	k?
lesů	les	k1gInPc2
<g/>
,	,	kIx,
újezd	újezd	k1gInSc1
byl	být	k5eAaImAgInS
vytvořen	vytvořit	k5eAaPmNgInS
vládou	vláda	k1gFnSc7
ČSR	ČSR	kA
dne	den	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1951	#num#	k4
s	s	k7c7
platností	platnost	k1gFnSc7
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1951	#num#	k4
na	na	k7c6
základě	základ	k1gInSc6
zákona	zákon	k1gInSc2
č.	č.	k?
169	#num#	k4
<g/>
/	/	kIx~
<g/>
1949	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
vojenských	vojenský	k2eAgInPc6d1
újezdech	újezd	k1gInPc6
<g/>
,	,	kIx,
na	na	k7c6
území	území	k1gNnSc6
dosavadního	dosavadní	k2eAgInSc2d1
vojenského	vojenský	k2eAgInSc2d1
tábora	tábor	k1gInSc2
Dědice	dědic	k1gMnSc2
zřízeného	zřízený	k2eAgMnSc2d1
vládou	vláda	k1gFnSc7
ČSR	ČSR	kA
v	v	k7c6
roce	rok	k1gInSc6
1936	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
případů	případ	k1gInPc2
jiných	jiný	k2eAgInPc2d1
vojenských	vojenský	k2eAgInPc2d1
újezdů	újezd	k1gInPc2
nebyla	být	k5eNaImAgFnS
při	při	k7c6
jeho	jeho	k3xOp3gNnSc6
zřízení	zřízení	k1gNnSc6
vysídlena	vysídlen	k2eAgFnSc1d1
žádná	žádný	k3yNgFnSc1
obec	obec	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Újezdní	újezdní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
sídlí	sídlet	k5eAaImIp3nS
ve	v	k7c6
Vyškově	Vyškov	k1gInSc6
a	a	k8xC
vykonává	vykonávat	k5eAaImIp3nS
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
také	také	k9
funkci	funkce	k1gFnSc4
stavebního	stavební	k2eAgInSc2d1
dozoru	dozor	k1gInSc2
a	a	k8xC
matriky	matrika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téměř	téměř	k6eAd1
celé	celý	k2eAgNnSc4d1
území	území	k1gNnSc4
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
je	být	k5eAaImIp3nS
zalesněno	zalesněn	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nezalesněné	zalesněný	k2eNgFnPc4d1
a	a	k8xC
odlesněné	odlesněný	k2eAgFnPc4d1
plochy	plocha	k1gFnPc4
tvoří	tvořit	k5eAaImIp3nP
především	především	k9
různé	různý	k2eAgInPc4d1
vojenské	vojenský	k2eAgInPc4d1
areály	areál	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
území	území	k1gNnSc6
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
i	i	k9
Myslejovická	Myslejovický	k2eAgFnSc1d1
nádrž	nádrž	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
roku	rok	k1gInSc3
2016	#num#	k4
není	být	k5eNaImIp3nS
na	na	k7c6
území	území	k1gNnSc6
újezdu	újezd	k1gInSc2
nikdo	nikdo	k3yNnSc1
hlášen	hlásit	k5eAaImNgInS
k	k	k7c3
trvalému	trvalý	k2eAgInSc3d1
pobytu	pobyt	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Součástí	součást	k1gFnSc7
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
Březina	Březina	k1gMnSc1
jsou	být	k5eAaImIp3nP
katastrální	katastrální	k2eAgNnSc4d1
území	území	k1gNnSc4
Doubrava	Doubrava	k1gFnSc1
u	u	k7c2
Březiny	Březina	k1gFnSc2
<g/>
,	,	kIx,
Kotáry	kotár	k1gInPc1
<g/>
,	,	kIx,
Osina	osina	k1gFnSc1
<g/>
,	,	kIx,
Pulkava	Pulkava	k1gFnSc1
<g/>
,	,	kIx,
Stříbrná	stříbrný	k2eAgFnSc1d1
u	u	k7c2
Březiny	Březina	k1gFnSc2
a	a	k8xC
Žbánov	Žbánov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Památky	památka	k1gFnPc1
na	na	k7c6
území	území	k1gNnSc6
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
v	v	k7c6
okrese	okres	k1gInSc6
Vyškov	Vyškov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
kulturní	kulturní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
Zámek	zámek	k1gInSc1
Ferdinandsko	Ferdinandsko	k1gNnSc1
</s>
<s>
Hrad	hrad	k1gInSc1
Horní	horní	k2eAgInSc1d1
Melice	melika	k1gFnSc3
</s>
<s>
Hrad	hrad	k1gInSc1
Vícov	Vícov	k1gInSc1
(	(	kIx(
<g/>
Ježův	Ježův	k2eAgInSc1d1
Hrad	hrad	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hradiště	Hradiště	k1gNnSc1
Obrova	obrův	k2eAgMnSc2d1
noha	noh	k1gMnSc2
</s>
<s>
ostatní	ostatní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
Hrad	hrad	k1gInSc1
Starý	starý	k2eAgInSc4d1
Plumlov	Plumlov	k1gInSc4
(	(	kIx(
<g/>
Drahans	Drahans	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Hrad	hrad	k1gInSc4
Smilovo	Smilův	k2eAgNnSc4d1
hradisko	hradisko	k1gNnSc4
(	(	kIx(
<g/>
též	též	k9
Smilův	Smilův	k2eAgInSc4d1
hrad	hrad	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Hrad	hrad	k1gInSc1
Myslejovice	Myslejovice	k1gFnSc2
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Počátkem	počátkem	k7c2
30	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
Německu	Německo	k1gNnSc6
nástupem	nástup	k1gInSc7
Adolfa	Adolf	k1gMnSc2
Hitlera	Hitler	k1gMnSc2
k	k	k7c3
moci	moc	k1gFnSc3
ke	k	k7c3
změně	změna	k1gFnSc3
bezpečnostní	bezpečnostní	k2eAgFnSc2d1
situace	situace	k1gFnSc2
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
přijata	přijat	k2eAgFnSc1d1
řada	řada	k1gFnSc1
opatření	opatření	k1gNnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimiž	jenž	k3xRgMnPc7
bylo	být	k5eAaImAgNnS
i	i	k9
rozhodnutí	rozhodnutí	k1gNnSc1
vlády	vláda	k1gFnSc2
ČSR	ČSR	kA
ze	z	k7c2
dne	den	k1gInSc2
7.2	7.2	k4
<g/>
.1936	.1936	k4
o	o	k7c4
vybudování	vybudování	k1gNnSc4
vojenského	vojenský	k2eAgInSc2d1
tábora	tábor	k1gInSc2
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
části	část	k1gFnSc6
lesního	lesní	k2eAgInSc2d1
komplexu	komplex	k1gInSc2
Drahanské	Drahanský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
majetkem	majetek	k1gInSc7
olomouckého	olomoucký	k2eAgNnSc2d1
arcibiskupství	arcibiskupství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lesy	les	k1gInPc1
byly	být	k5eAaImAgFnP
postupně	postupně	k6eAd1
státem	stát	k1gInSc7
vyvlastněny	vyvlastněn	k2eAgFnPc4d1
a	a	k8xC
již	již	k6eAd1
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1935	#num#	k4
bylo	být	k5eAaImAgNnS
zřízeno	zřízen	k2eAgNnSc1d1
Velitelství	velitelství	k1gNnSc1
výcvikového	výcvikový	k2eAgInSc2d1
tábora	tábor	k1gInSc2
v	v	k7c6
Dědicích	Dědice	k1gFnPc6
u	u	k7c2
Vyškova	Vyškov	k1gInSc2
<g/>
,	,	kIx,
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
zde	zde	k6eAd1
přišla	přijít	k5eAaPmAgFnS
osádka	osádka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgFnP
prováděny	prováděn	k2eAgFnPc1d1
intenzívní	intenzívní	k2eAgFnPc1d1
práce	práce	k1gFnPc1
<g/>
,	,	kIx,
postupně	postupně	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
vybudování	vybudování	k1gNnSc3
dělostřelecké	dělostřelecký	k2eAgFnPc1d1
<g/>
,	,	kIx,
pěchotní	pěchotní	k2eAgFnPc1d1
a	a	k8xC
letecké	letecký	k2eAgFnPc1d1
střelnice	střelnice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kasárnách	kasárny	k1gFnPc6
byl	být	k5eAaImAgInS
umístěn	umístit	k5eAaPmNgInS
pluk	pluk	k1gInSc1
útočné	útočný	k2eAgFnSc2d1
vozby	vozba	k1gFnSc2
<g/>
,	,	kIx,
vyzbrojený	vyzbrojený	k2eAgMnSc1d1
tanky	tank	k1gInPc7
LT-35	LT-35	k1gFnPc2
a	a	k8xC
obrněnými	obrněný	k2eAgNnPc7d1
vozidly	vozidlo	k1gNnPc7
<g/>
;	;	kIx,
ve	v	k7c6
Vyškově	Vyškov	k1gInSc6
bylo	být	k5eAaImAgNnS
zřízeno	zřízen	k2eAgNnSc1d1
Učiliště	učiliště	k1gNnSc1
útočné	útočný	k2eAgFnSc2d1
vozby	vozba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prostoru	prostor	k1gInSc6
byly	být	k5eAaImAgFnP
od	od	k7c2
roku	rok	k1gInSc2
1937	#num#	k4
stavěny	stavěn	k2eAgInPc4d1
cvičné	cvičný	k2eAgInPc4d1
železobetonové	železobetonový	k2eAgInPc4d1
objekty	objekt	k1gInPc4
pro	pro	k7c4
výcvik	výcvik	k1gInSc4
osádek	osádka	k1gFnPc2
československého	československý	k2eAgNnSc2d1
pohraničního	pohraniční	k2eAgNnSc2d1
opevnění	opevnění	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
tomtéž	týž	k3xTgInSc6
roce	rok	k1gInSc6
bylo	být	k5eAaImAgNnS
zřízeno	zřídit	k5eAaPmNgNnS
vyškovské	vyškovský	k2eAgNnSc1d1
vojenské	vojenský	k2eAgNnSc1d1
letiště	letiště	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
obsazení	obsazení	k1gNnSc6
státu	stát	k1gInSc2
nacistickým	nacistický	k2eAgNnSc7d1
Německem	Německo	k1gNnSc7
a	a	k8xC
vzniku	vznik	k1gInSc3
Protektorátu	protektorát	k1gInSc2
Čechy	Čechy	k1gFnPc1
a	a	k8xC
Morava	Morava	k1gFnSc1
byl	být	k5eAaImAgInS
26	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1939	#num#	k4
vojenský	vojenský	k2eAgInSc1d1
prostor	prostor	k1gInSc1
převzat	převzat	k2eAgInSc1d1
Němci	Němec	k1gMnPc7
a	a	k8xC
přejmenován	přejmenován	k2eAgInSc4d1
na	na	k7c4
Truppenübungsplatz	Truppenübungsplatz	k1gInSc4
Wischau	Wischaus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kasárnách	kasárny	k1gFnPc6
na	na	k7c6
Kozí	kozí	k2eAgFnSc6d1
Horce	horka	k1gFnSc6
byla	být	k5eAaImAgFnS
umístěna	umístit	k5eAaPmNgFnS
Německá	německý	k2eAgFnSc1d1
praporčická	praporčický	k2eAgFnSc1d1
škola	škola	k1gFnSc1
tankových	tankový	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1940	#num#	k4
vydal	vydat	k5eAaPmAgMnS
říšský	říšský	k2eAgMnSc1d1
protektor	protektor	k1gMnSc1
Konstantin	Konstantin	k1gMnSc1
von	von	k1gInSc4
Neurath	Neurath	k1gInSc1
výnos	výnos	k1gInSc1
o	o	k7c6
vystěhování	vystěhování	k1gNnSc6
33	#num#	k4
obcí	obec	k1gFnPc2
Drahanské	Drahanský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účelem	účel	k1gInSc7
toho	ten	k3xDgNnSc2
bylo	být	k5eAaImAgNnS
jednak	jednak	k8xC
dočasně	dočasně	k6eAd1
rozšířit	rozšířit	k5eAaPmF
vojenskou	vojenský	k2eAgFnSc4d1
střelnici	střelnice	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
hlavně	hlavně	k9
vytvořit	vytvořit	k5eAaPmF
německý	německý	k2eAgInSc4d1
koridor	koridor	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
měl	mít	k5eAaImAgInS
propojit	propojit	k5eAaPmF
německé	německý	k2eAgInPc4d1
ostrůvky	ostrůvek	k1gInPc4
na	na	k7c6
Litovelsku	Litovelsko	k1gNnSc6
a	a	k8xC
Olomoucku	Olomoucko	k1gNnSc6
přes	přes	k7c4
německý	německý	k2eAgInSc4d1
ostrůvek	ostrůvek	k1gInSc4
u	u	k7c2
Vyškova	Vyškov	k1gInSc2
s	s	k7c7
německými	německý	k2eAgFnPc7d1
obcemi	obec	k1gFnPc7
na	na	k7c6
Brněnsku	Brněnsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
takto	takto	k6eAd1
rozšířeném	rozšířený	k2eAgInSc6d1
vojenském	vojenský	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
bylo	být	k5eAaImAgNnS
vybudováno	vybudovat	k5eAaPmNgNnS
množství	množství	k1gNnSc4
pevnůstek	pevnůstka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
sloužily	sloužit	k5eAaImAgFnP
k	k	k7c3
tahání	tahání	k1gNnSc3
cvičných	cvičný	k2eAgInPc2d1
cílů	cíl	k1gInPc2
či	či	k8xC
jako	jako	k9
pozorovatelny	pozorovatelna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystěhované	vystěhovaný	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
byly	být	k5eAaImAgFnP
poškozeny	poškozen	k2eAgFnPc1d1
<g/>
,	,	kIx,
nejvíce	nejvíce	k6eAd1,k6eAd3
utrpěly	utrpět	k5eAaPmAgFnP
ty	ten	k3xDgFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
sloužily	sloužit	k5eAaImAgFnP
jako	jako	k9
cvičné	cvičný	k2eAgInPc4d1
cíle	cíl	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
ukončení	ukončení	k1gNnSc6
bojů	boj	k1gInPc2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
se	se	k3xPyFc4
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
byli	být	k5eAaImAgMnP
násilně	násilně	k6eAd1
vystěhováni	vystěhovat	k5eAaPmNgMnP
<g/>
,	,	kIx,
začali	začít	k5eAaPmAgMnP
vracet	vracet	k5eAaImF
zpět	zpět	k6eAd1
do	do	k7c2
svých	svůj	k3xOyFgInPc2
domovů	domov	k1gInPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
nebylo	být	k5eNaImAgNnS
zcela	zcela	k6eAd1
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
zda	zda	k8xS
střelnice	střelnice	k1gFnSc1
ve	v	k7c6
stávajícím	stávající	k2eAgInSc6d1
rozsahu	rozsah	k1gInSc6
bude	být	k5eAaImBp3nS
zachována	zachovat	k5eAaPmNgFnS
<g/>
.	.	kIx.
29	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1945	#num#	k4
bylo	být	k5eAaImAgNnS
rozhodnuto	rozhodnout	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
Vyškovská	vyškovský	k2eAgFnSc1d1
vojenská	vojenský	k2eAgFnSc1d1
střelnice	střelnice	k1gFnSc1
zůstane	zůstat	k5eAaPmIp3nS
v	v	k7c6
rozsahu	rozsah	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
jakém	jaký	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
byla	být	k5eAaImAgNnP
před	před	k7c7
okupací	okupace	k1gFnSc7
v	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Zmenšení	zmenšení	k1gNnSc1
</s>
<s>
Ministr	ministr	k1gMnSc1
obrany	obrana	k1gFnSc2
Alexandr	Alexandr	k1gMnSc1
Vondra	Vondra	k1gMnSc1
8	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2011	#num#	k4
informoval	informovat	k5eAaBmAgMnS
starosty	starosta	k1gMnSc2
obcí	obec	k1gFnPc2
Vyškovska	Vyškovsko	k1gNnSc2
<g/>
,	,	kIx,
že	že	k8xS
ministerstvo	ministerstvo	k1gNnSc1
obrany	obrana	k1gFnSc2
počítá	počítat	k5eAaImIp3nS
s	s	k7c7
redukcí	redukce	k1gFnSc7
rozlohy	rozloha	k1gFnSc2
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
Březina	Březina	k1gMnSc1
o	o	k7c6
cca	cca	kA
5	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Jihomoravský	jihomoravský	k2eAgMnSc1d1
hejtman	hejtman	k1gMnSc1
Michal	Michal	k1gMnSc1
Hašek	Hašek	k1gMnSc1
při	při	k7c6
tomto	tento	k3xDgInSc6
jednání	jednání	k1gNnSc6
dohodl	dohodnout	k5eAaPmAgInS
rozšíření	rozšíření	k1gNnSc4
využitelnosti	využitelnost	k1gFnSc2
vojenského	vojenský	k2eAgInSc2d1
výcvikového	výcvikový	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
pro	pro	k7c4
<g />
.	.	kIx.
</s>
<s hack="1">
výcvik	výcvik	k1gInSc1
složek	složka	k1gFnPc2
Integrovaného	integrovaný	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
systému	systém	k1gInSc2
Jihomoravského	jihomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
,	,	kIx,
zejména	zejména	k9
Policie	policie	k1gFnSc1
ČR	ČR	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
O	o	k7c6
zrušení	zrušení	k1gNnSc6
VÚ	VÚ	kA
Brdy	Brdy	k1gInPc4
a	a	k8xC
zmenšení	zmenšení	k1gNnSc4
dalších	další	k2eAgInPc2d1
4	#num#	k4
vojenských	vojenský	k2eAgInPc2d1
újezdů	újezd	k1gInPc2
včetně	včetně	k7c2
Březiny	Březina	k1gFnSc2
nejpozději	pozdě	k6eAd3
k	k	k7c3
roku	rok	k1gInSc3
2015	#num#	k4
rozhodla	rozhodnout	k5eAaPmAgFnS
vláda	vláda	k1gFnSc1
na	na	k7c6
začátku	začátek	k1gInSc6
ledna	leden	k1gInSc2
2012	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Vojenský	vojenský	k2eAgInSc1d1
újezd	újezd	k1gInSc1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
zmenšen	zmenšit	k5eAaPmNgInS
o	o	k7c4
750	#num#	k4
ha	ha	kA
<g/>
,	,	kIx,
tedy	tedy	k9
asi	asi	k9
5	#num#	k4
%	%	kIx~
své	svůj	k3xOyFgFnSc2
rozlohy	rozloha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
újezdu	újezd	k1gInSc2
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
vyjmuta	vyjmout	k5eAaPmNgFnS
nejvýchodnější	východní	k2eAgFnSc1d3
část	část	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
přiléhá	přiléhat	k5eAaImIp3nS
ke	k	k7c3
katastru	katastr	k1gInSc3
městyse	městys	k1gInSc2
Brodek	brodek	k1gInSc4
u	u	k7c2
Prostějova	Prostějov	k1gInSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
okolí	okolí	k1gNnSc2
obce	obec	k1gFnSc2
Podivice	Podivice	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
dosud	dosud	k6eAd1
je	být	k5eAaImIp3nS
újezdem	újezd	k1gInSc7
zcela	zcela	k6eAd1
obklopena	obklopit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
jí	on	k3xPp3gFnSc3
bránilo	bránit	k5eAaImAgNnS
ve	v	k7c6
využití	využití	k1gNnSc6
evropských	evropský	k2eAgInPc2d1
fondů	fond	k1gInPc2
<g/>
,	,	kIx,
plné	plný	k2eAgFnSc6d1
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
mikroregiony	mikroregion	k1gInPc7
a	a	k8xC
rozvoji	rozvoj	k1gInSc3
cykloturistiky	cykloturistika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Změna	změna	k1gFnSc1
má	mít	k5eAaImIp3nS
vyřešit	vyřešit	k5eAaPmF
i	i	k9
restituční	restituční	k2eAgInPc4d1
nároky	nárok	k1gInPc4
rodiny	rodina	k1gFnSc2
Belcrediů	Belcredi	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podivice	Podivice	k1gFnSc2
dosud	dosud	k6eAd1
nemají	mít	k5eNaImIp3nP
zaveden	zaveden	k2eAgInSc4d1
plyn	plyn	k1gInSc4
a	a	k8xC
k	k	k7c3
dovozu	dovoz	k1gInSc2
dříví	dříví	k1gNnSc2
z	z	k7c2
okolních	okolní	k2eAgInPc2d1
lesů	les	k1gInPc2
potřebují	potřebovat	k5eAaImIp3nP
obyvatelé	obyvatel	k1gMnPc1
povolenky	povolenka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zákonem	zákon	k1gInSc7
č.	č.	k?
15	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
hranicích	hranice	k1gFnPc6
vojenských	vojenský	k2eAgInPc2d1
újezdů	újezd	k1gInPc2
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2016	#num#	k4
ke	k	k7c3
zmenšení	zmenšení	k1gNnSc3
rozlohy	rozloha	k1gFnSc2
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
budoucím	budoucí	k2eAgNnSc7d1
zmenšením	zmenšení	k1gNnSc7
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
proto	proto	k8xC
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
letech	léto	k1gNnPc6
2013	#num#	k4
a	a	k8xC
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
k	k	k7c3
vyčlenění	vyčlenění	k1gNnSc3
těchto	tento	k3xDgNnPc2
nových	nový	k2eAgNnPc2d1
katastrálních	katastrální	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
:	:	kIx,
Cihelny	cihelna	k1gFnPc1
u	u	k7c2
Podivic	Podivice	k1gFnPc2
(	(	kIx(
<g/>
vyčleněno	vyčlenit	k5eAaPmNgNnS
k	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
říjnu	říjen	k1gInSc6
2013	#num#	k4
z	z	k7c2
dosavadního	dosavadní	k2eAgInSc2d1
k.	k.	k?
ú.	ú.	k?
Kotáry	kotár	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Chaloupky	chaloupka	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
u	u	k7c2
Otaslavic	Otaslavice	k1gFnPc2
(	(	kIx(
<g/>
vyčleněno	vyčlenit	k5eAaPmNgNnS
k	k	k7c3
3	#num#	k4
<g/>
.	.	kIx.
březnu	březen	k1gInSc6
2014	#num#	k4
z	z	k7c2
dosavadního	dosavadní	k2eAgInSc2d1
k.	k.	k?
ú.	ú.	k?
Pulkava	Pulkava	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Občiny	občina	k1gFnPc1
u	u	k7c2
Drahan	Drahana	k1gFnPc2
(	(	kIx(
<g/>
vyčleněno	vyčlenit	k5eAaPmNgNnS
k	k	k7c3
29	#num#	k4
<g/>
.	.	kIx.
říjnu	říjen	k1gInSc6
2013	#num#	k4
z	z	k7c2
dosavadního	dosavadní	k2eAgInSc2d1
k.	k.	k?
ú.	ú.	k?
Osina	osina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Osinky	osinek	k1gInPc1
u	u	k7c2
Krumsína	Krumsín	k1gInSc2
(	(	kIx(
<g/>
vyčleněno	vyčlenit	k5eAaPmNgNnS
k	k	k7c3
29	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
říjnu	říjen	k1gInSc6
2013	#num#	k4
z	z	k7c2
dosavadního	dosavadní	k2eAgInSc2d1
k.	k.	k?
ú.	ú.	k?
Osina	osina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Ostatky	ostatek	k1gInPc1
u	u	k7c2
Křenůvek	Křenůvka	k1gFnPc2
(	(	kIx(
<g/>
vyčleněno	vyčlenit	k5eAaPmNgNnS
k	k	k7c3
29	#num#	k4
<g/>
.	.	kIx.
říjnu	říjen	k1gInSc6
2013	#num#	k4
z	z	k7c2
dosavadního	dosavadní	k2eAgInSc2d1
k.	k.	k?
ú.	ú.	k?
Osina	osina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Žleb	žleb	k1gInSc1
u	u	k7c2
Prostějoviček	Prostějovička	k1gFnPc2
(	(	kIx(
<g/>
vyčleněno	vyčlenit	k5eAaPmNgNnS
k	k	k7c3
6	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2013	#num#	k4
z	z	k7c2
dosavadního	dosavadní	k2eAgInSc2d1
k.	k.	k?
ú.	ú.	k?
Osina	osina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
téhož	týž	k3xTgInSc2
zákona	zákon	k1gInSc2
se	se	k3xPyFc4
Cihelny	cihelna	k1gFnPc1
u	u	k7c2
Podivic	Podivice	k1gInPc2
připojily	připojit	k5eAaPmAgInP
k	k	k7c3
obci	obec	k1gFnSc3
Podivice	Podivice	k1gFnSc2
<g/>
,	,	kIx,
Chaloupky	chaloupka	k1gFnSc2
u	u	k7c2
Otaslavic	Otaslavice	k1gFnPc2
k	k	k7c3
obci	obec	k1gFnSc3
Otaslavice	Otaslavice	k1gFnSc2
<g/>
,	,	kIx,
Občiny	občina	k1gFnPc1
u	u	k7c2
Drahan	Drahana	k1gFnPc2
k	k	k7c3
obci	obec	k1gFnSc3
Drahany	Drahana	k1gFnSc2
<g/>
,	,	kIx,
Osinky	osinka	k1gFnSc2
u	u	k7c2
Krumsína	Krumsín	k1gInSc2
k	k	k7c3
obci	obec	k1gFnSc3
Krumsín	Krumsín	k1gInSc1
<g/>
,	,	kIx,
Ostatky	ostatek	k1gInPc1
u	u	k7c2
Křenůvek	Křenůvka	k1gFnPc2
k	k	k7c3
obci	obec	k1gFnSc3
Myslejovice	Myslejovice	k1gFnSc2
a	a	k8xC
Žleb	žleb	k1gInSc1
u	u	k7c2
Prostějoviček	Prostějovička	k1gFnPc2
k	k	k7c3
obci	obec	k1gFnSc3
Prostějovičky	Prostějovička	k1gFnSc2
(	(	kIx(
<g/>
kromě	kromě	k7c2
Cihelen	cihelna	k1gFnPc2
u	u	k7c2
Podivic	Podivice	k1gFnPc2
tedy	tedy	k8xC
všechny	všechen	k3xTgFnPc4
zároveň	zároveň	k6eAd1
přešly	přejít	k5eAaPmAgInP
do	do	k7c2
Olomouckého	olomoucký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbývající	zbývající	k2eAgInSc1d1
území	území	k1gNnSc2
i	i	k8xC
nadále	nadále	k6eAd1
spravuje	spravovat	k5eAaImIp3nS
újezdní	újezdní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
ve	v	k7c6
Vyškově	Vyškov	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Územní	územní	k2eAgNnSc1d1
členění	členění	k1gNnSc1
</s>
<s>
Vojenský	vojenský	k2eAgInSc1d1
újezd	újezd	k1gInSc1
byl	být	k5eAaImAgInS
utvořen	utvořit	k5eAaPmNgInS
na	na	k7c6
území	území	k1gNnSc6
původně	původně	k6eAd1
patřícím	patřící	k2eAgNnSc6d1
ke	k	k7c3
katastrálním	katastrální	k2eAgNnSc7d1
územím	území	k1gNnSc7
těchto	tento	k3xDgFnPc2
obcí	obec	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
z	z	k7c2
někdejšího	někdejší	k2eAgInSc2d1
politického	politický	k2eAgInSc2d1
okresu	okres	k1gInSc2
Prostějov	Prostějov	k1gInSc1
</s>
<s>
Bousín	Bousín	k1gMnSc1
</s>
<s>
Drahany	Drahana	k1gFnPc1
</s>
<s>
Hamry	Hamry	k1gInPc1
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
část	část	k1gFnSc1
Plumlova	Plumlův	k2eAgFnSc1d1
</s>
<s>
Kobylničky	Kobylnička	k1gFnPc1
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
část	část	k1gFnSc1
Myslejovic	Myslejovice	k1gFnPc2
</s>
<s>
Krumsín	Krumsín	k1gMnSc1
</s>
<s>
Malé	Malé	k2eAgNnSc4d1
Hradisko	hradisko	k1gNnSc4
</s>
<s>
Myslejovice	Myslejovice	k1gFnSc1
</s>
<s>
Otaslavice	Otaslavice	k1gFnSc1
</s>
<s>
Prostějovičky	Prostějovička	k1gFnPc1
</s>
<s>
Sněhotice	Sněhotice	k1gFnSc1
</s>
<s>
Stínava	Stínava	k1gFnSc1
</s>
<s>
Vícov	Vícov	k1gInSc1
</s>
<s>
Žárovice	Žárovice	k1gFnSc1
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
část	část	k1gFnSc1
Plumlova	Plumlův	k2eAgFnSc1d1
</s>
<s>
z	z	k7c2
někdejšího	někdejší	k2eAgInSc2d1
politického	politický	k2eAgInSc2d1
okresu	okres	k1gInSc2
Boskovice	Boskovice	k1gInPc1
</s>
<s>
Repechy	Repech	k1gInPc1
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
část	část	k1gFnSc1
Bousína	Bousína	k1gFnSc1
</s>
<s>
z	z	k7c2
někdejšího	někdejší	k2eAgInSc2d1
politického	politický	k2eAgInSc2d1
okresu	okres	k1gInSc2
Vyškov	Vyškov	k1gInSc1
</s>
<s>
Podivice	Podivice	k1gFnSc1
</s>
<s>
Radslavice	Radslavice	k1gFnSc1
(	(	kIx(
<g/>
katastrální	katastrální	k2eAgNnSc1d1
území	území	k1gNnSc1
Radslavice	Radslavice	k1gFnSc2
a	a	k8xC
Radslavičky	Radslavička	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Dědice	Dědice	k1gFnPc1
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
část	část	k1gFnSc1
Vyškova	Vyškov	k1gInSc2
</s>
<s>
Rychtářov	Rychtářov	k1gInSc1
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
část	část	k1gFnSc1
Vyškova	Vyškov	k1gInSc2
</s>
<s>
Krásensko	Krásensko	k6eAd1
</s>
<s>
Studnice	studnice	k1gFnSc1
</s>
<s>
Nové	Nové	k2eAgInPc1d1
Sady	sad	k1gInPc1
</s>
<s>
Pustiměř	Pustiměř	k1gFnSc1
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
Části	část	k1gFnSc3
uvedených	uvedený	k2eAgNnPc2d1
katastrálních	katastrální	k2eAgNnPc2d1
území	území	k1gNnPc2
byly	být	k5eAaImAgInP
v	v	k7c6
rámci	rámec	k1gInSc6
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
červnu	červen	k1gInSc3
1977	#num#	k4
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
sloučeny	sloučit	k5eAaPmNgInP
do	do	k7c2
šesti	šest	k4xCc2
nových	nový	k2eAgNnPc2d1
katastrálních	katastrální	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
se	se	k3xPyFc4
vojenský	vojenský	k2eAgInSc1d1
újezd	újezd	k1gInSc1
člení	členit	k5eAaImIp3nS
i	i	k9
po	po	k7c6
svém	svůj	k3xOyFgNnSc6
zmenšení	zmenšení	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Doubrava	Doubrava	k1gFnSc1
u	u	k7c2
Březiny	Březina	k1gFnSc2
</s>
<s>
Kotáry	kotár	k1gInPc1
</s>
<s>
Osina	osina	k1gFnSc1
</s>
<s>
Pulkava	Pulkava	k1gFnSc1
</s>
<s>
Stříbrná	stříbrná	k1gFnSc1
u	u	k7c2
Březiny	Březina	k1gFnSc2
</s>
<s>
Žbánov	Žbánov	k1gInSc1
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
k	k	k7c3
jednotlivým	jednotlivý	k2eAgFnPc3d1
datům	datum	k1gNnPc3
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2001	#num#	k4
–	–	k?
7	#num#	k4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2002	#num#	k4
–	–	k?
9	#num#	k4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2003	#num#	k4
–	–	k?
6	#num#	k4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2004	#num#	k4
–	–	k?
3	#num#	k4
obyvatelé	obyvatel	k1gMnPc1
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2004	#num#	k4
–	–	k?
2	#num#	k4
obyvatelé	obyvatel	k1gMnPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2006	#num#	k4
–	–	k?
5	#num#	k4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2012	#num#	k4
–	–	k?
2	#num#	k4
obyvatelé	obyvatel	k1gMnPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2016	#num#	k4
–	–	k?
0	#num#	k4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Plocha	plocha	k1gFnSc1
s	s	k7c7
mladými	mladý	k2eAgInPc7d1
stromy	strom	k1gInPc7
v	v	k7c6
k.	k.	k?
ú.	ú.	k?
Doubrava	Doubrava	k1gMnSc1
u	u	k7c2
Březiny	Březina	k1gFnSc2
</s>
<s>
Chemické	chemický	k2eAgNnSc1d1
cvičiště	cvičiště	k1gNnSc1
v	v	k7c6
k.ú.	k.ú.	k?
Doubrava	Doubrava	k1gMnSc1
u	u	k7c2
Březiny	Březina	k1gFnSc2
</s>
<s>
Stádo	stádo	k1gNnSc1
ovcí	ovce	k1gFnPc2
u	u	k7c2
obce	obec	k1gFnSc2
Březina	Březina	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
pozadí	pozadí	k1gNnSc6
stejnojmenný	stejnojmenný	k2eAgInSc1d1
vojenský	vojenský	k2eAgInSc1d1
újezd	újezd	k1gInSc1
</s>
<s>
Výhled	výhled	k1gInSc1
na	na	k7c4
Krumsín	Krumsín	k1gInSc4
od	od	k7c2
východu	východ	k1gInSc2
<g/>
,	,	kIx,
za	za	k7c2
obcí	obec	k1gFnPc2
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
újezd	újezd	k1gInSc1
</s>
<s>
Značka	značka	k1gFnSc1
začátku	začátek	k1gInSc2
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
</s>
<s>
Zřícenina	zřícenina	k1gFnSc1
Ježova	Ježův	k2eAgInSc2d1
hradu	hrad	k1gInSc2
</s>
<s>
Panorama	panorama	k1gNnSc1
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
severní	severní	k2eAgFnSc4d1
část	část	k1gFnSc4
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
Březina	Březina	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Zde	zde	k6eAd1
je	být	k5eAaImIp3nS
soutisk	soutisk	k1gInSc4
map	mapa	k1gFnPc2
z	z	k7c2
let	léto	k1gNnPc2
1959	#num#	k4
a	a	k8xC
2009	#num#	k4
zobrazující	zobrazující	k2eAgFnSc4d1
katastrální	katastrální	k2eAgNnSc4d1
členění	členění	k1gNnSc4
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
před	před	k7c7
a	a	k8xC
po	po	k7c6
této	tento	k3xDgFnSc6
úpravě	úprava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
:	:	kIx,
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
obcích	obec	k1gFnPc6
-	-	kIx~
k	k	k7c3
1.1	1.1	k4
<g/>
.2021	.2021	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Armáda	armáda	k1gFnSc1
zmenší	zmenšit	k5eAaPmIp3nS
vojenský	vojenský	k2eAgInSc4d1
újezd	újezd	k1gInSc4
Březina	Březina	k1gMnSc1
<g/>
,	,	kIx,
vojáci	voják	k1gMnPc1
na	na	k7c6
něm	on	k3xPp3gInSc6
budou	být	k5eAaImBp3nP
cvičit	cvičit	k5eAaImF
dál	daleko	k6eAd2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-11-08	2011-11-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Hejtman	hejtman	k1gMnSc1
Michal	Michal	k1gMnSc1
Hašek	Hašek	k1gMnSc1
jednal	jednat	k5eAaImAgMnS
s	s	k7c7
ministrem	ministr	k1gMnSc7
obrany	obrana	k1gFnSc2
o	o	k7c6
budoucnosti	budoucnost	k1gFnSc6
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
Březina	Březina	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Jihomoravský	jihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vláda	vláda	k1gFnSc1
zrušila	zrušit	k5eAaPmAgFnS
vojenský	vojenský	k2eAgInSc4d1
újezd	újezd	k1gInSc4
Brdy	Brdy	k1gInPc7
<g/>
,	,	kIx,
další	další	k2eAgInPc4d1
čtyři	čtyři	k4xCgInPc4
cvičiště	cvičiště	k1gNnPc1
se	se	k3xPyFc4
zmenší	zmenšit	k5eAaPmIp3nP
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-01-04	2012-01-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
TESAŘ	Tesař	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Újezd	Újezd	k1gInSc1
Březina	Březina	k1gFnSc1
bude	být	k5eAaImBp3nS
menší	malý	k2eAgFnSc1d2
<g/>
,	,	kIx,
radost	radost	k1gFnSc1
z	z	k7c2
toho	ten	k3xDgNnSc2
mají	mít	k5eAaImIp3nP
hlavně	hlavně	k9
v	v	k7c6
Podivicích	Podivice	k1gFnPc6
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-01-07	2012-01-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Změny	změna	k1gFnSc2
názvů	název	k1gInPc2
katastrálních	katastrální	k2eAgNnPc2d1
území	území	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
úřad	úřad	k1gInSc1
zeměměřický	zeměměřický	k2eAgInSc1d1
a	a	k8xC
katastrální	katastrální	k2eAgMnSc1d1
<g/>
,	,	kIx,
2015-04-01	2015-04-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
15	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c4
zrušení	zrušení	k1gNnSc4
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
Brdy	Brdy	k1gInPc4
<g/>
,	,	kIx,
o	o	k7c6
stanovení	stanovení	k1gNnSc6
hranic	hranice	k1gFnPc2
vojenských	vojenský	k2eAgInPc2d1
újezdů	újezd	k1gInPc2
<g/>
,	,	kIx,
o	o	k7c6
změně	změna	k1gFnSc6
hranic	hranice	k1gFnPc2
krajů	kraj	k1gInPc2
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
souvisejících	související	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
o	o	k7c6
hranicích	hranice	k1gFnPc6
vojenských	vojenský	k2eAgInPc2d1
újezdů	újezd	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kronika	kronika	k1gFnSc1
obce	obec	k1gFnSc2
Podivice	Podivice	k1gFnSc1
<g/>
,	,	kIx,
díl	díl	k1gInSc1
pátý	pátý	k4xOgInSc1
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
vojenský	vojenský	k2eAgInSc4d1
újezd	újezd	k1gInSc4
Březina	Březina	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Vojenský	vojenský	k2eAgInSc1d1
újezd	újezd	k1gInSc1
Březina	Březina	k1gMnSc1
</s>
<s>
Územní	územní	k2eAgInSc1d1
plán	plán	k1gInSc1
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
Březina	Březina	k1gMnSc1
</s>
<s>
Viktoria	Viktoria	k1gFnSc1
Höhe	Höh	k1gFnSc2
-	-	kIx~
stránky	stránka	k1gFnSc2
o	o	k7c6
opomíjeném	opomíjený	k2eAgInSc6d1
koutu	kout	k1gInSc2
Drahanské	Drahanský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
</s>
<s>
vvpdedice	vvpdedice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
-	-	kIx~
stránky	stránka	k1gFnPc4
věnované	věnovaný	k2eAgFnPc4d1
zejména	zejména	k9
historii	historie	k1gFnSc4
vojenského	vojenský	k2eAgInSc2d1
výcvikového	výcvikový	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
Dědice	dědic	k1gMnSc2
</s>
<s>
Parlamentní	parlamentní	k2eAgFnSc2d1
volby	volba	k1gFnSc2
2002	#num#	k4
ve	v	k7c6
volebním	volební	k2eAgInSc6d1
okrsku	okrsek	k1gInSc6
Březina	Březina	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Města	město	k1gNnSc2
<g/>
,	,	kIx,
městyse	městys	k1gInSc2
<g/>
,	,	kIx,
obce	obec	k1gFnSc2
a	a	k8xC
vojenský	vojenský	k2eAgInSc4d1
újezd	újezd	k1gInSc4
okresu	okres	k1gInSc2
Vyškov	Vyškov	k1gInSc1
</s>
<s>
Bohaté	bohatý	k2eAgFnPc4d1
Málkovice	Málkovice	k1gFnPc4
•	•	k?
Bohdalice-Pavlovice	Bohdalice-Pavlovice	k1gFnPc4
•	•	k?
Bošovice	Bošovice	k1gFnPc4
•	•	k?
Brankovice	Brankovice	k1gFnPc4
•	•	k?
Bučovice	Bučovice	k1gFnPc4
•	•	k?
Dětkovice	Dětkovice	k1gFnSc2
•	•	k?
Dobročkovice	Dobročkovice	k1gFnSc2
•	•	k?
Dražovice	Dražovice	k1gFnSc1
•	•	k?
Drnovice	Drnovice	k1gInPc1
•	•	k?
Drysice	Drysice	k1gFnSc2
•	•	k?
Habrovany	Habrovan	k1gMnPc4
•	•	k?
Heršpice	Heršpice	k1gFnPc4
•	•	k?
Hlubočany	Hlubočan	k1gMnPc4
•	•	k?
Hodějice	Hodějice	k1gFnSc1
•	•	k?
Holubice	holubice	k1gFnSc2
•	•	k?
Hostěrádky-Rešov	Hostěrádky-Rešov	k1gInSc1
•	•	k?
Hoštice-Heroltice	Hoštice-Heroltice	k1gFnSc2
•	•	k?
Hrušky	hruška	k1gFnSc2
•	•	k?
Hvězdlice	Hvězdlice	k1gFnSc2
•	•	k?
Chvalkovice	Chvalkovice	k1gFnSc2
•	•	k?
Ivanovice	Ivanovice	k1gFnPc1
na	na	k7c6
Hané	Haná	k1gFnSc6
•	•	k?
Ježkovice	Ježkovice	k1gFnSc2
•	•	k?
Kobeřice	Kobeřice	k1gFnSc2
u	u	k7c2
Brna	Brno	k1gNnSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Kojátky	Kojátka	k1gFnSc2
•	•	k?
Komořany	Komořan	k1gMnPc4
•	•	k?
Kozlany	Kozlana	k1gFnPc4
•	•	k?
Kožušice	Kožušice	k1gFnPc4
•	•	k?
Krásensko	Krásensko	k1gNnSc1
•	•	k?
Křenovice	Křenovice	k1gFnSc2
•	•	k?
Křižanovice	Křižanovice	k1gFnSc2
•	•	k?
Křižanovice	Křižanovice	k1gFnSc2
u	u	k7c2
Vyškova	Vyškov	k1gInSc2
•	•	k?
Kučerov	Kučerov	k1gInSc1
•	•	k?
Letonice	Letonice	k1gFnSc2
•	•	k?
Lovčičky	Lovčička	k1gFnSc2
•	•	k?
Luleč	Luleč	k1gMnSc1
•	•	k?
Lysovice	Lysovice	k1gFnSc2
•	•	k?
Malínky	Malínka	k1gFnSc2
•	•	k?
Medlovice	Medlovice	k1gFnSc2
•	•	k?
Milešovice	Milešovice	k1gFnSc2
•	•	k?
Milonice	Milonice	k1gFnSc2
•	•	k?
Moravské	moravský	k2eAgFnSc2d1
Málkovice	Málkovice	k1gFnSc2
•	•	k?
Mouřínov	Mouřínov	k1gInSc1
•	•	k?
Němčany	Němčan	k1gMnPc4
•	•	k?
Nemochovice	Nemochovice	k1gFnSc1
•	•	k?
Nemojany	Nemojana	k1gFnSc2
•	•	k?
Nemotice	Nemotice	k1gFnSc2
•	•	k?
Nesovice	Nesovice	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Nevojice	Nevojice	k1gFnSc2
•	•	k?
Nížkovice	Nížkovice	k1gFnSc2
•	•	k?
Nové	Nové	k2eAgInPc1d1
Sady	sad	k1gInPc1
•	•	k?
Olšany	Olšany	k1gInPc4
•	•	k?
Orlovice	Orlovice	k1gFnSc2
•	•	k?
Otnice	Otnice	k1gFnSc2
•	•	k?
Podbřežice	Podbřežice	k1gFnSc2
•	•	k?
Podivice	Podivice	k1gFnSc1
•	•	k?
Podomí	Podomí	k1gNnPc2
•	•	k?
Prusy-Boškůvky	Prusy-Boškůvka	k1gFnSc2
•	•	k?
Pustiměř	Pustiměř	k1gMnSc1
•	•	k?
Račice-Pístovice	Račice-Pístovice	k1gFnSc2
•	•	k?
Radslavice	Radslavice	k1gFnSc2
•	•	k?
Rašovice	Rašovice	k1gFnSc2
•	•	k?
Rostěnice-Zvonovice	Rostěnice-Zvonovice	k1gFnSc2
•	•	k?
Rousínov	Rousínov	k1gInSc1
•	•	k?
Ruprechtov	Ruprechtov	k1gInSc1
•	•	k?
Rybníček	rybníček	k1gInSc1
•	•	k?
Slavkov	Slavkov	k1gInSc1
u	u	k7c2
Brna	Brno	k1gNnSc2
•	•	k?
Snovídky	Snovídka	k1gFnSc2
•	•	k?
Studnice	studnice	k1gFnSc2
•	•	k?
Šaratice	šaratice	k1gFnSc2
•	•	k?
Švábenice	Švábenice	k1gFnSc2
•	•	k?
Topolany	Topolana	k1gFnSc2
•	•	k?
Tučapy	Tučapa	k1gFnSc2
•	•	k?
Uhřice	Uhřice	k1gFnSc2
•	•	k?
Vážany	Vážana	k1gFnSc2
•	•	k?
Vážany	Vážana	k1gFnSc2
nad	nad	k7c7
Litavou	Litava	k1gFnSc7
•	•	k?
Velešovice	Velešovice	k1gFnSc2
•	•	k?
Vyškov	Vyškov	k1gInSc1
•	•	k?
Zbýšov	Zbýšov	k1gInSc1
•	•	k?
Zelená	zelenat	k5eAaImIp3nS
Hora	hora	k1gFnSc1
•	•	k?
vojenský	vojenský	k2eAgInSc4d1
újezd	újezd	k1gInSc4
Březina	Březina	k1gFnSc1
legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
město	město	k1gNnSc1
<g/>
,	,	kIx,
městys	městys	k1gInSc1
.	.	kIx.
</s>
