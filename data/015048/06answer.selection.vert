<s desamb="1">
Území	území	k1gNnSc1
újezdu	újezd	k1gInSc2
zahrnuje	zahrnovat	k5eAaImIp3nS
158,206	158,206	k4
km²	km²	k?
lesů	les	k1gInPc2
<g/>
,	,	kIx,
újezd	újezd	k1gInSc1
byl	být	k5eAaImAgInS
vytvořen	vytvořit	k5eAaPmNgInS
vládou	vláda	k1gFnSc7
ČSR	ČSR	kA
dne	den	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1951	#num#	k4
s	s	k7c7
platností	platnost	k1gFnSc7
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1951	#num#	k4
na	na	k7c6
základě	základ	k1gInSc6
zákona	zákon	k1gInSc2
č.	č.	k?
169	#num#	k4
<g/>
/	/	kIx~
<g/>
1949	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
vojenských	vojenský	k2eAgInPc6d1
újezdech	újezd	k1gInPc6
<g/>
,	,	kIx,
na	na	k7c6
území	území	k1gNnSc6
dosavadního	dosavadní	k2eAgInSc2d1
vojenského	vojenský	k2eAgInSc2d1
tábora	tábor	k1gInSc2
Dědice	dědic	k1gMnSc2
zřízeného	zřízený	k2eAgMnSc2d1
vládou	vláda	k1gFnSc7
ČSR	ČSR	kA
v	v	k7c6
roce	rok	k1gInSc6
1936	#num#	k4
<g/>
.	.	kIx.
</s>