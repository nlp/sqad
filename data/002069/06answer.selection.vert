<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
zničeno	zničen	k2eAgNnSc1d1	zničeno
(	(	kIx(	(
<g/>
např.	např.	kA	např.
40	[number]	k4	40
procent	procento	k1gNnPc2	procento
obytné	obytný	k2eAgFnSc2d1	obytná
plochy	plocha	k1gFnSc2	plocha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Berlín	Berlín	k1gInSc1	Berlín
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
čtyř	čtyři	k4xCgInPc2	čtyři
okupačních	okupační	k2eAgInPc2d1	okupační
sektorů	sektor	k1gInPc2	sektor
<g/>
.	.	kIx.	.
</s>
