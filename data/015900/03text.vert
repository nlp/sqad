<s>
Vilém	Vilém	k1gMnSc1
Dušan	Dušan	k1gMnSc1
Lambl	Lambl	k1gMnSc1
</s>
<s>
Vilém	Vilém	k1gMnSc1
Dušan	Dušan	k1gMnSc1
Lambl	Lambl	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1824	#num#	k4
nebo	nebo	k8xC
1824	#num#	k4
<g/>
Letiny	Letin	k1gInPc1
Rakouské	rakouský	k2eAgInPc1d1
císařství	císařství	k1gNnSc4
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1895	#num#	k4
nebo	nebo	k8xC
25	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1895	#num#	k4
<g/>
Varšava	Varšava	k1gFnSc1
Polsko	Polsko	k1gNnSc1
Polsko	Polsko	k1gNnSc4
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnSc2
</s>
<s>
astma	astma	k1gFnSc1
Národnost	národnost	k1gFnSc1
</s>
<s>
Češi	Čech	k1gMnPc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgNnSc2d1
Povolání	povolání	k1gNnSc2
</s>
<s>
lékař-spisovatel	lékař-spisovatel	k1gMnSc1
<g/>
,	,	kIx,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
<g/>
,	,	kIx,
lékař	lékař	k1gMnSc1
<g/>
,	,	kIx,
etnograf	etnograf	k1gMnSc1
a	a	k8xC
překladatel	překladatel	k1gMnSc1
Zaměstnavatelé	zaměstnavatel	k1gMnPc1
</s>
<s>
Varšavská	varšavský	k2eAgFnSc1d1
univerzitaCharkovská	univerzitaCharkovský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vilém	Vilém	k1gMnSc1
Dušan	Dušan	k1gMnSc1
Lambl	Lambl	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1824	#num#	k4
Letiny	Letina	k1gFnSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
12	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1895	#num#	k4
<g/>
,	,	kIx,
Varšava	Varšava	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
biolog	biolog	k1gMnSc1
<g/>
,	,	kIx,
lékař	lékař	k1gMnSc1
<g/>
,	,	kIx,
průkopník	průkopník	k1gMnSc1
moderního	moderní	k2eAgNnSc2d1
dětského	dětský	k2eAgNnSc2d1
lékařství	lékařství	k1gNnSc2
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
a	a	k8xC
politický	politický	k2eAgMnSc1d1
aktivista	aktivista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
Letinách	Letina	k1gFnPc6
u	u	k7c2
Plzně	Plzeň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystudoval	vystudovat	k5eAaPmAgMnS
gymnázium	gymnázium	k1gNnSc4
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
a	a	k8xC
posléze	posléze	k6eAd1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
na	na	k7c6
Univerzitě	univerzita	k1gFnSc6
Karlově	Karlův	k2eAgFnSc6d1
studoval	studovat	k5eAaImAgMnS
slovanské	slovanský	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
a	a	k8xC
medicínu	medicína	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
za	za	k7c7
studií	studie	k1gFnSc7
hojně	hojně	k6eAd1
překládal	překládat	k5eAaImAgMnS
z	z	k7c2
jihoslovanských	jihoslovanský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
tak	tak	k6eAd1
ještě	ještě	k6eAd1
za	za	k7c7
studií	studie	k1gFnSc7
psal	psát	k5eAaImAgInS
články	článek	k1gInPc7
do	do	k7c2
Časopisu	časopis	k1gInSc2
českého	český	k2eAgNnSc2d1
musea	museum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prosadil	prosadit	k5eAaPmAgMnS
v	v	k7c6
českém	český	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
užívání	užívání	k1gNnSc2
pojmu	pojem	k1gInSc2
řasy	řasa	k1gFnSc2
namísto	namísto	k7c2
slova	slovo	k1gNnSc2
žabinec	žabinec	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
razil	razit	k5eAaImAgMnS
Jan	Jan	k1gMnSc1
Svatopluk	Svatopluk	k1gMnSc1
Presl	Presl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studia	studio	k1gNnSc2
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
ale	ale	k9
protáhla	protáhnout	k5eAaPmAgFnS
kvůli	kvůli	k7c3
politické	politický	k2eAgFnSc3d1
aktivitě	aktivita	k1gFnSc3
a	a	k8xC
angažmá	angažmá	k1gNnSc4
v	v	k7c6
revolučním	revoluční	k2eAgNnSc6d1
dění	dění	k1gNnSc6
roku	rok	k1gInSc2
1848	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
straně	strana	k1gFnSc6
liberálů	liberál	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
tehdy	tehdy	k6eAd1
členem	člen	k1gMnSc7
Národního	národní	k2eAgInSc2d1
výboru	výbor	k1gInSc2
a	a	k8xC
výboru	výbor	k1gInSc2
Slovanské	slovanský	k2eAgFnSc2d1
lípy	lípa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spřátelil	spřátelit	k5eAaPmAgMnS
se	se	k3xPyFc4
s	s	k7c7
Karlem	Karel	k1gMnSc7
Havlíčkem	Havlíček	k1gMnSc7
Borovským	Borovský	k1gMnSc7
a	a	k8xC
přispíval	přispívat	k5eAaImAgMnS
do	do	k7c2
jeho	jeho	k3xOp3gFnPc2
Národních	národní	k2eAgFnPc2d1
novin	novina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Promoval	promovat	k5eAaBmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1851	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politiky	politikum	k1gNnPc7
se	se	k3xPyFc4
vzdal	vzdát	k5eAaPmAgMnS
ve	v	k7c4
prospěch	prospěch	k1gInSc4
vědy	věda	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gInPc4
širší	široký	k2eAgInPc4d2
zájmy	zájem	k1gInPc4
prozrazuje	prozrazovat	k5eAaImIp3nS
kupříkladu	kupříkladu	k6eAd1
spis	spis	k1gInSc4
Evropa	Evropa	k1gFnSc1
v	v	k7c6
ohledu	ohled	k1gInSc6
národopisném	národopisný	k2eAgInSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bádal	bádat	k5eAaImAgMnS
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
a	a	k8xC
objevil	objevit	k5eAaPmAgMnS
přitom	přitom	k6eAd1
výrůstky	výrůstek	k1gInPc4
na	na	k7c6
srdečních	srdeční	k2eAgFnPc6d1
chlopních	chlopeň	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
pak	pak	k6eAd1
nějaký	nějaký	k3yIgInSc4
čas	čas	k1gInSc4
nazývaly	nazývat	k5eAaImAgInP
Lamblovy	Lamblův	k2eAgInPc1d1
výrůstky	výrůstek	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1856	#num#	k4
se	se	k3xPyFc4
habilitoval	habilitovat	k5eAaBmAgInS
jako	jako	k9
vůbec	vůbec	k9
první	první	k4xOgMnSc1
soukromý	soukromý	k2eAgMnSc1d1
docent	docent	k1gMnSc1
patologické	patologický	k2eAgFnSc2d1
anatomie	anatomie	k1gFnSc2
a	a	k8xC
histologie	histologie	k1gFnSc2
na	na	k7c6
Karlově	Karlův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
lékařem	lékař	k1gMnSc7
v	v	k7c6
Dětské	dětský	k2eAgFnSc6d1
nemocnici	nemocnice	k1gFnSc6
císaře	císař	k1gMnSc2
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
jako	jako	k8xC,k8xS
první	první	k4xOgInSc4
na	na	k7c6
světě	svět	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1859	#num#	k4
popsal	popsat	k5eAaPmAgMnS
bičíkovce	bičíkovec	k1gMnPc4
ve	v	k7c6
stolici	stolice	k1gFnSc6
dětí	dítě	k1gFnPc2
s	s	k7c7
dysenterií	dysenterie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nazval	nazvat	k5eAaPmAgMnS,k5eAaBmAgMnS
ho	on	k3xPp3gNnSc4
Cercominas	Cercominas	k1gMnSc1
intestinalis	intestinalis	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1888	#num#	k4
jméno	jméno	k1gNnSc1
na	na	k7c4
Lamblia	Lamblius	k1gMnSc4
intestinalis	intestinalis	k1gFnSc2
změnil	změnit	k5eAaPmAgMnS
zoolog	zoolog	k1gMnSc1
Émile	Émile	k1gFnSc2
Blanchard	Blanchard	k1gMnSc1
(	(	kIx(
<g/>
1819	#num#	k4
<g/>
–	–	k?
<g/>
1900	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1915	#num#	k4
byl	být	k5eAaImAgMnS
druh	druh	k1gMnSc1
přejmenován	přejmenovat	k5eAaPmNgMnS
na	na	k7c4
Giardia	Giardium	k1gNnPc4
lamblia	lamblia	k1gFnSc1
americkým	americký	k2eAgMnSc7d1
zoologem	zoolog	k1gMnSc7
C.	C.	kA
W.	W.	kA
Stilesem	Stiles	k1gMnSc7
(	(	kIx(
<g/>
1867	#num#	k4
<g/>
–	–	k?
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
na	na	k7c4
počest	počest	k1gFnSc4
Lambla	Lambla	k1gMnSc2
a	a	k8xC
francouzského	francouzský	k2eAgMnSc2d1
biologa	biolog	k1gMnSc2
A.	A.	kA
M.	M.	kA
Giarda	Giarda	k1gMnSc1
(	(	kIx(
<g/>
1846	#num#	k4
<g/>
–	–	k?
<g/>
1908	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1860	#num#	k4
získal	získat	k5eAaPmAgMnS
Vilém	Vilém	k1gMnSc1
Dušan	Dušan	k1gMnSc1
Lambl	Lambl	k1gMnSc1
profesuru	profesura	k1gFnSc4
na	na	k7c6
Univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Charkově	Charkov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
působil	působit	k5eAaImAgInS
i	i	k9
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
ve	v	k7c6
Varšavě	Varšava	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
také	také	k9
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vztah	vztah	k1gInSc1
s	s	k7c7
Boženu	Božena	k1gFnSc4
Němcovou	Němcův	k2eAgFnSc7d1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1851	#num#	k4
se	se	k3xPyFc4
Lambl	Lambl	k1gMnSc1
seznámil	seznámit	k5eAaPmAgMnS
Boženu	Božena	k1gFnSc4
Němcovou	Němcová	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
během	během	k7c2
její	její	k3xOp3gFnSc2
nemoci	nemoc	k1gFnSc2
ošetřoval	ošetřovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patřil	patřit	k5eAaImAgMnS
ke	k	k7c3
spisovatelčiným	spisovatelčin	k2eAgMnPc3d1
literárním	literární	k2eAgMnPc3d1
důvěrníkům	důvěrník	k1gMnPc3
<g/>
,	,	kIx,
podporoval	podporovat	k5eAaImAgInS
její	její	k3xOp3gNnSc4
slovanské	slovanský	k2eAgNnSc4d1
uvědomění	uvědomění	k1gNnSc4
a	a	k8xC
byl	být	k5eAaImAgMnS
lékařem	lékař	k1gMnSc7
celé	celá	k1gFnSc2
její	její	k3xOp3gFnSc2
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Intimní	intimní	k2eAgInSc1d1
vztah	vztah	k1gInSc1
s	s	k7c7
Němcové	Němcové	k2eAgInSc7d1
s	s	k7c7
Lamblem	Lamblo	k1gNnSc7
trval	trvalo	k1gNnPc2
od	od	k7c2
podzimu	podzim	k1gInSc2
1851	#num#	k4
do	do	k7c2
jara	jaro	k1gNnSc2
1853	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Matriční	matriční	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c6
narození	narození	k1gNnSc6
a	a	k8xC
křtu	křest	k1gInSc6
farnost	farnost	k1gFnSc1
Letiny	Letina	k1gFnSc2
↑	↑	k?
http://lekarske.slovniky.cz/pojem/lambl-vilem-dusan	http://lekarske.slovniky.cz/pojem/lambl-vilem-dusan	k1gMnSc1
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.muzeumbn.cz	www.muzeumbn.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
NĚMCOVÁ	Němcová	k1gFnSc1
<g/>
,	,	kIx,
Božena	Božena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korespondence	korespondence	k1gFnSc1
II	II	kA
<g/>
..	..	k?
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
697	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Jmenný	jmenný	k2eAgInSc1d1
rejstřík	rejstřík	k1gInSc1
s	s	k7c7
anotacemi	anotace	k1gFnPc7
<g/>
,	,	kIx,
s.	s.	k?
545	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Dílo	dílo	k1gNnSc4
Mravní	mravní	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
milostného	milostný	k2eAgInSc2d1
života	život	k1gInSc2
Boženy	Božena	k1gFnSc2
Němcové	Němcová	k1gFnSc2
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Vilém	Vilém	k1gMnSc1
Dušan	Dušan	k1gMnSc1
Lambl	Lambl	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jk	jk	k?
<g/>
0	#num#	k4
<g/>
1071438	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1017567646	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5876	#num#	k4
7352	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
83804080	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Biologie	biologie	k1gFnSc1
|	|	kIx~
Medicína	medicína	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
