<s desamb="1">
Patřil	patřit	k5eAaImAgMnS
ke	k	k7c3
spisovatelčiným	spisovatelčin	k2eAgMnPc3d1
literárním	literární	k2eAgMnPc3d1
důvěrníkům	důvěrník	k1gMnPc3
<g/>
,	,	kIx,
podporoval	podporovat	k5eAaImAgInS
její	její	k3xOp3gNnSc4
slovanské	slovanský	k2eAgNnSc4d1
uvědomění	uvědomění	k1gNnSc4
a	a	k8xC
byl	být	k5eAaImAgMnS
lékařem	lékař	k1gMnSc7
celé	celá	k1gFnSc2
její	její	k3xOp3gFnSc2
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Intimní	intimní	k2eAgInSc1d1
vztah	vztah	k1gInSc1
s	s	k7c7
Němcové	Němcové	k2eAgInSc7d1
s	s	k7c7
Lamblem	Lamblo	k1gNnSc7
trval	trvalo	k1gNnPc2
od	od	k7c2
podzimu	podzim	k1gInSc2
1851	#num#	k4
do	do	k7c2
jara	jaro	k1gNnSc2
1853	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>