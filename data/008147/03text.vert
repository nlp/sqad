<s>
Džoser	Džoser	k1gInSc1	Džoser
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Posvátný	posvátný	k2eAgMnSc1d1	posvátný
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Nádherný	nádherný	k2eAgMnSc1d1	nádherný
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
zakladatel	zakladatel	k1gMnSc1	zakladatel
a	a	k8xC	a
první	první	k4xOgMnSc1	první
panovník	panovník	k1gMnSc1	panovník
3	[number]	k4	3
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Egyptě	Egypt	k1gInSc6	Egypt
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
Staré	Staré	k2eAgFnSc2d1	Staré
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
znám	znám	k2eAgInSc1d1	znám
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Necerichet	Necericheta	k1gFnPc2	Necericheta
(	(	kIx(	(
<g/>
jméno	jméno	k1gNnSc1	jméno
Džoser	Džosra	k1gFnPc2	Džosra
je	být	k5eAaImIp3nS	být
doloženo	doložit	k5eAaPmNgNnS	doložit
až	až	k9	až
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
říši	říš	k1gFnSc6	říš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
Tosorthros	Tosorthrosa	k1gFnPc2	Tosorthrosa
<g/>
.	.	kIx.	.
</s>
<s>
Vládl	vládnout	k5eAaImAgMnS	vládnout
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2690	[number]	k4	2690
<g/>
/	/	kIx~	/
<g/>
2640	[number]	k4	2640
<g/>
–	–	k?	–
<g/>
2670	[number]	k4	2670
<g/>
/	/	kIx~	/
<g/>
2620	[number]	k4	2620
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Panováním	panování	k1gNnSc7	panování
krále	král	k1gMnSc2	král
Džosera	Džoser	k1gMnSc2	Džoser
začíná	začínat	k5eAaImIp3nS	začínat
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejskvělejších	skvělý	k2eAgNnPc2d3	nejskvělejší
období	období	k1gNnPc2	období
Staré	Stará	k1gFnSc2	Stará
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
"	"	kIx"	"
<g/>
Džoserovo	Džoserův	k2eAgNnSc1d1	Džoserův
století	století	k1gNnSc1	století
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
faraon	faraon	k1gMnSc1	faraon
vládl	vládnout	k5eAaImAgMnS	vládnout
jako	jako	k9	jako
teokratický	teokratický	k2eAgMnSc1d1	teokratický
despota	despota	k1gMnSc1	despota
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nápisu	nápis	k1gInSc2	nápis
ve	v	k7c6	v
Vádí	vádí	k1gNnSc6	vádí
Hammámat	Hammámat	k1gFnSc2	Hammámat
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyslal	vyslat	k5eAaPmAgMnS	vyslat
expedici	expedice	k1gFnSc4	expedice
na	na	k7c4	na
Sinaj	Sinaj	k1gInSc4	Sinaj
(	(	kIx(	(
<g/>
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
byl	být	k5eAaImAgMnS	být
velitel	velitel	k1gMnSc1	velitel
králových	králův	k2eAgMnPc2d1	králův
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
správce	správce	k1gMnSc2	správce
pouštní	pouštní	k2eAgFnSc2d1	pouštní
oblasti	oblast	k1gFnSc2	oblast
Negtanch	Negtanch	k1gMnSc1	Negtanch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
docílil	docílit	k5eAaPmAgMnS	docílit
četných	četný	k2eAgInPc2d1	četný
územních	územní	k2eAgInPc2d1	územní
zisků	zisk	k1gInPc2	zisk
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
ho	on	k3xPp3gMnSc4	on
zajímaly	zajímat	k5eAaImAgInP	zajímat
hlavně	hlavně	k9	hlavně
měděné	měděný	k2eAgInPc1d1	měděný
a	a	k8xC	a
tyrkysové	tyrkysový	k2eAgInPc1d1	tyrkysový
doly	dol	k1gInPc1	dol
ve	v	k7c6	v
Vádí	vádí	k1gNnSc6	vádí
Magháře	Maghář	k1gMnSc2	Maghář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
ochraně	ochrana	k1gFnSc3	ochrana
jižních	jižní	k2eAgFnPc2d1	jižní
hranic	hranice	k1gFnPc2	hranice
Egypta	Egypt	k1gInSc2	Egypt
dal	dát	k5eAaPmAgInS	dát
údajně	údajně	k6eAd1	údajně
vybudovat	vybudovat	k5eAaPmF	vybudovat
hradbu	hradba	k1gFnSc4	hradba
u	u	k7c2	u
prvního	první	k4xOgInSc2	první
nilského	nilský	k2eAgInSc2d1	nilský
kataraktu	katarakt	k1gInSc2	katarakt
za	za	k7c7	za
Asuánem	Asuán	k1gInSc7	Asuán
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
přesunul	přesunout	k5eAaPmAgMnS	přesunout
královskou	královský	k2eAgFnSc4d1	královská
rezidenci	rezidence	k1gFnSc4	rezidence
z	z	k7c2	z
Ceneje	Cenej	k1gInSc2	Cenej
(	(	kIx(	(
<g/>
Thinis	Thinis	k1gFnSc1	Thinis
<g/>
)	)	kIx)	)
do	do	k7c2	do
Mennoferu	Mennofer	k1gInSc2	Mennofer
<g/>
,	,	kIx,	,
města	město	k1gNnSc2	město
nacházejícího	nacházející	k2eAgInSc2d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
Horního	horní	k2eAgInSc2d1	horní
a	a	k8xC	a
Dolního	dolní	k2eAgInSc2d1	dolní
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	daleko	k6eNd1	daleko
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
malou	malý	k2eAgFnSc4d1	malá
svatyni	svatyně	k1gFnSc4	svatyně
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
jen	jen	k9	jen
ruiny	ruina	k1gFnSc2	ruina
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
prý	prý	k9	prý
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
veliký	veliký	k2eAgInSc1d1	veliký
hlad	hlad	k1gInSc1	hlad
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
Nil	Nil	k1gInSc1	Nil
už	už	k6eAd1	už
nevyléval	vylévat	k5eNaImAgInS	vylévat
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
koryta	koryto	k1gNnSc2	koryto
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
víme	vědět	k5eAaImIp1nP	vědět
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
Ptolemaiovské	Ptolemaiovský	k2eAgFnSc2d1	Ptolemaiovská
desky	deska	k1gFnSc2	deska
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
hladová	hladový	k2eAgFnSc1d1	hladová
stéla	stéla	k1gFnSc1	stéla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
říká	říkat	k5eAaImIp3nS	říkat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
7	[number]	k4	7
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
bída	bída	k1gFnSc1	bída
a	a	k8xC	a
hlad	hlad	k1gInSc1	hlad
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
nevěděl	vědět	k5eNaImAgMnS	vědět
proč	proč	k6eAd1	proč
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
obrátil	obrátit	k5eAaPmAgMnS	obrátit
ke	k	k7c3	k
kněžím	kněz	k1gMnPc3	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
po	po	k7c6	po
usilovném	usilovný	k2eAgNnSc6d1	usilovné
pátrání	pátrání	k1gNnSc6	pátrání
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
hněvá	hněvat	k5eAaImIp3nS	hněvat
bůh	bůh	k1gMnSc1	bůh
Chnum	Chnum	k1gInSc1	Chnum
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInPc1	jeho
sandály	sandál	k1gInPc1	sandál
brání	bránit	k5eAaImIp3nP	bránit
toku	tok	k1gInSc2	tok
Nilu	Nil	k1gInSc2	Nil
<g/>
.	.	kIx.	.
</s>
<s>
Chnumovi	Chnum	k1gMnSc3	Chnum
byl	být	k5eAaImAgMnS	být
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
obětován	obětován	k2eAgInSc4d1	obětován
chléb	chléb	k1gInSc4	chléb
<g/>
,	,	kIx,	,
pivo	pivo	k1gNnSc4	pivo
a	a	k8xC	a
drůbež	drůbež	k1gFnSc4	drůbež
a	a	k8xC	a
záplavy	záplava	k1gFnPc1	záplava
začaly	začít	k5eAaPmAgFnP	začít
<g/>
.	.	kIx.	.
</s>
<s>
Imhotep	Imhotep	k1gMnSc1	Imhotep
<g/>
,	,	kIx,	,
vezír	vezír	k1gMnSc1	vezír
Hesire	Hesir	k1gInSc5	Hesir
<g/>
,	,	kIx,	,
vrchní	vrchní	k2eAgMnSc1d1	vrchní
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
písař	písař	k1gMnSc1	písař
a	a	k8xC	a
lékař	lékař	k1gMnSc1	lékař
Související	související	k2eAgFnPc4d1	související
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Džoserova	Džoserův	k2eAgFnSc1d1	Džoserova
pyramida	pyramida	k1gFnSc1	pyramida
<g/>
.	.	kIx.	.
</s>
<s>
Džoser	Džoser	k1gMnSc1	Džoser
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
symbolickou	symbolický	k2eAgFnSc4d1	symbolická
hrobku	hrobka	k1gFnSc4	hrobka
na	na	k7c6	na
královském	královský	k2eAgNnSc6d1	královské
pohřebišti	pohřebiště	k1gNnSc6	pohřebiště
v	v	k7c6	v
Abydu	Abydo	k1gNnSc6	Abydo
(	(	kIx(	(
<g/>
Ebozev	Ebozev	k1gMnSc1	Ebozev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
zvaném	zvaný	k2eAgNnSc6d1	zvané
Bét-Challáf	Bét-Challáf	k1gInSc1	Bét-Challáf
a	a	k8xC	a
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
všechny	všechen	k3xTgMnPc4	všechen
ostatní	ostatní	k2eAgMnPc4d1	ostatní
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
z	z	k7c2	z
nepálených	pálený	k2eNgFnPc2d1	nepálená
sušených	sušený	k2eAgFnPc2d1	sušená
cihel	cihla	k1gFnPc2	cihla
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
skutečná	skutečný	k2eAgFnSc1d1	skutečná
hrobka	hrobka	k1gFnSc1	hrobka
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
Sakkáře	Sakkář	k1gMnSc4	Sakkář
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
sakkárský	sakkárský	k2eAgInSc1d1	sakkárský
komplex	komplex	k1gInSc1	komplex
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
měřící	měřící	k2eAgFnSc1d1	měřící
přibližně	přibližně	k6eAd1	přibližně
15	[number]	k4	15
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stavitelem	stavitel	k1gMnSc7	stavitel
této	tento	k3xDgFnSc2	tento
první	první	k4xOgFnSc2	první
egyptské	egyptský	k2eAgFnSc2d1	egyptská
pyramidy	pyramida	k1gFnSc2	pyramida
byl	být	k5eAaImAgInS	být
Imhotep	Imhotep	k1gInSc1	Imhotep
<g/>
.	.	kIx.	.
</s>
<s>
Imhotep	Imhotep	k1gMnSc1	Imhotep
dal	dát	k5eAaPmAgMnS	dát
odvést	odvést	k5eAaPmF	odvést
písek	písek	k1gInSc4	písek
a	a	k8xC	a
vyrovnat	vyrovnat	k5eAaPmF	vyrovnat
na	na	k7c6	na
staveništi	staveniště	k1gNnSc6	staveniště
vápencový	vápencový	k2eAgInSc4d1	vápencový
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
kameni	kámen	k1gInSc6	kámen
vyhloubeny	vyhlouben	k2eAgInPc4d1	vyhlouben
asi	asi	k9	asi
28	[number]	k4	28
m	m	kA	m
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
šachty	šachta	k1gFnSc2	šachta
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
dno	dno	k1gNnSc1	dno
bylo	být	k5eAaImAgNnS	být
obloženo	obložen	k2eAgNnSc1d1	obloženo
žulou	žula	k1gFnSc7	žula
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
netěžila	těžit	k5eNaImAgFnS	těžit
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
dovážet	dovážet	k5eAaImF	dovážet
z	z	k7c2	z
lomů	lom	k1gInPc2	lom
800	[number]	k4	800
km	km	kA	km
od	od	k7c2	od
Sakkáry	Sakkár	k1gInPc1	Sakkár
a	a	k8xC	a
kvádry	kvádr	k1gInPc1	kvádr
byly	být	k5eAaImAgInP	být
dopravovány	dopravovat	k5eAaImNgInP	dopravovat
po	po	k7c6	po
Nilu	Nil	k1gInSc6	Nil
<g/>
.	.	kIx.	.
</s>
<s>
Stupňovitá	stupňovitý	k2eAgFnSc1d1	stupňovitá
Džoserova	Džoserův	k2eAgFnSc1d1	Džoserova
pyramida	pyramida	k1gFnSc1	pyramida
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
Velké	velký	k2eAgFnSc2d1	velká
pyramidy	pyramida	k1gFnSc2	pyramida
v	v	k7c6	v
Gíze	Gíze	k1gFnSc6	Gíze
vzdálena	vzdáleno	k1gNnSc2	vzdáleno
asi	asi	k9	asi
14	[number]	k4	14
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
jen	jen	k9	jen
mastabou	mastaba	k1gFnSc7	mastaba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
původně	původně	k6eAd1	původně
patrně	patrně	k6eAd1	patrně
patřila	patřit	k5eAaImAgFnS	patřit
královu	králův	k2eAgMnSc3d1	králův
pravděpodobnému	pravděpodobný	k2eAgMnSc3d1	pravděpodobný
předchůdci	předchůdce	k1gMnSc3	předchůdce
Sanachtovi	Sanacht	k1gMnSc3	Sanacht
(	(	kIx(	(
<g/>
Nebka	Nebka	k1gMnSc1	Nebka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Imhotep	Imhotep	k1gMnSc1	Imhotep
tuto	tento	k3xDgFnSc4	tento
základní	základní	k2eAgFnSc4d1	základní
stavbu	stavba	k1gFnSc4	stavba
navýšil	navýšit	k5eAaPmAgMnS	navýšit
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
stupně	stupeň	k1gInPc4	stupeň
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
přidal	přidat	k5eAaPmAgMnS	přidat
ještě	ještě	k9	ještě
tři	tři	k4xCgFnPc4	tři
další	další	k2eAgFnPc4d1	další
–	–	k?	–
tak	tak	k6eAd1	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
první	první	k4xOgFnSc1	první
pyramida	pyramida	k1gFnSc1	pyramida
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
úpravách	úprava	k1gFnPc6	úprava
získala	získat	k5eAaPmAgFnS	získat
stavba	stavba	k1gFnSc1	stavba
svůj	svůj	k3xOyFgInSc4	svůj
dnešní	dnešní	k2eAgInSc4d1	dnešní
šestistupňový	šestistupňový	k2eAgInSc4d1	šestistupňový
vzhled	vzhled	k1gInSc4	vzhled
<g/>
.	.	kIx.	.
</s>
