<s>
V	v	k7c6	v
normálním	normální	k2eAgNnSc6d1	normální
lidském	lidský	k2eAgNnSc6d1	lidské
oku	oko	k1gNnSc6	oko
existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgInPc1	tři
druhy	druh	k1gInPc1	druh
čípků	čípek	k1gInPc2	čípek
<g/>
,	,	kIx,	,
lišící	lišící	k2eAgMnPc4d1	lišící
se	se	k3xPyFc4	se
barevnými	barevný	k2eAgInPc7d1	barevný
pigmenty	pigment	k1gInPc7	pigment
a	a	k8xC	a
citlivostí	citlivost	k1gFnSc7	citlivost
k	k	k7c3	k
vlnovým	vlnový	k2eAgFnPc3d1	vlnová
délkám	délka	k1gFnPc3	délka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
určují	určovat	k5eAaImIp3nP	určovat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
