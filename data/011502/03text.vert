<p>
<s>
Kolibřík	kolibřík	k1gMnSc1	kolibřík
rubínohrdlý	rubínohrdlý	k2eAgMnSc1d1	rubínohrdlý
(	(	kIx(	(
<g/>
Archilochus	Archilochus	k1gInSc1	Archilochus
colubris	colubris	k1gFnPc2	colubris
Linné	Linné	k1gNnSc1	Linné
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tažný	tažný	k2eAgMnSc1d1	tažný
pták	pták	k1gMnSc1	pták
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
kolibříkovitých	kolibříkovitý	k2eAgFnPc2d1	kolibříkovitý
(	(	kIx(	(
<g/>
Trochilidae	Trochilidae	k1gFnPc2	Trochilidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Zbarven	zbarven	k2eAgMnSc1d1	zbarven
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
zeleně	zeleně	k6eAd1	zeleně
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
šedé	šedý	k2eAgNnSc4d1	šedé
bříško	bříško	k1gNnSc4	bříško
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
obdržel	obdržet	k5eAaPmAgMnS	obdržet
od	od	k7c2	od
rubínově	rubínově	k6eAd1	rubínově
zbarvené	zbarvený	k2eAgFnSc2d1	zbarvená
skvrny	skvrna	k1gFnSc2	skvrna
na	na	k7c6	na
hrdle	hrdla	k1gFnSc6	hrdla
samce	samec	k1gInSc2	samec
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
má	mít	k5eAaImIp3nS	mít
tmavý	tmavý	k2eAgMnSc1d1	tmavý
<g/>
,	,	kIx,	,
vidličnatý	vidličnatý	k2eAgInSc1d1	vidličnatý
ocas	ocas	k1gInSc1	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Samici	samice	k1gFnSc4	samice
schází	scházet	k5eAaImIp3nS	scházet
rubínová	rubínový	k2eAgFnSc1d1	rubínová
skvrna	skvrna	k1gFnSc1	skvrna
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
tupě	tupě	k6eAd1	tupě
zakončený	zakončený	k2eAgInSc1d1	zakončený
ocas	ocas	k1gInSc1	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgNnPc1	tři
krajní	krajní	k2eAgNnPc1d1	krajní
ocasní	ocasní	k2eAgNnPc1d1	ocasní
pírka	pírko	k1gNnPc1	pírko
mají	mít	k5eAaImIp3nP	mít
navíc	navíc	k6eAd1	navíc
na	na	k7c6	na
konci	konec	k1gInSc6	konec
velkou	velká	k1gFnSc4	velká
<g/>
,	,	kIx,	,
bílou	bílý	k2eAgFnSc4d1	bílá
skvrnu	skvrna	k1gFnSc4	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
samici	samice	k1gFnSc3	samice
<g/>
.	.	kIx.	.
</s>
<s>
Měří	měřit	k5eAaImIp3nS	měřit
9	[number]	k4	9
cm	cm	kA	cm
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
má	mít	k5eAaImIp3nS	mít
10	[number]	k4	10
-	-	kIx~	-
12	[number]	k4	12
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
3	[number]	k4	3
-	-	kIx~	-
3,5	[number]	k4	3,5
g.	g.	k?	g.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
květním	květní	k2eAgInSc7d1	květní
nektarem	nektar	k1gInSc7	nektar
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
drobným	drobný	k2eAgInSc7d1	drobný
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
kolibříci	kolibřík	k1gMnPc1	kolibřík
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
trubicovitý	trubicovitý	k2eAgInSc4d1	trubicovitý
zobák	zobák	k1gInSc4	zobák
a	a	k8xC	a
nektar	nektar	k1gInSc4	nektar
nasává	nasávat	k5eAaImIp3nS	nasávat
pomocí	pomocí	k7c2	pomocí
zvláštně	zvláštně	k6eAd1	zvláštně
utvářeného	utvářený	k2eAgInSc2d1	utvářený
<g/>
,	,	kIx,	,
trubicovitého	trubicovitý	k2eAgInSc2d1	trubicovitý
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
květů	květ	k1gInPc2	květ
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
u	u	k7c2	u
krmítek	krmítko	k1gNnPc2	krmítko
pro	pro	k7c4	pro
kolibříky	kolibřík	k1gMnPc4	kolibřík
se	se	k3xPyFc4	se
kolibříci	kolibřík	k1gMnPc1	kolibřík
navzájem	navzájem	k6eAd1	navzájem
zahánějí	zahánět	k5eAaImIp3nP	zahánět
<g/>
.	.	kIx.	.
</s>
<s>
Kolibřík	kolibřík	k1gMnSc1	kolibřík
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
lovu	lov	k1gInSc2	lov
hmyzu	hmyz	k1gInSc2	hmyz
za	za	k7c2	za
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
,	,	kIx,	,
spředeného	spředený	k2eAgNnSc2d1	spředené
pavoučím	pavoučí	k1gNnSc7	pavoučí
vláknem	vlákno	k1gNnSc7	vlákno
z	z	k7c2	z
listí	listí	k1gNnSc2	listí
a	a	k8xC	a
mechu	mech	k1gInSc2	mech
<g/>
,	,	kIx,	,
klade	klást	k5eAaImIp3nS	klást
samička	samička	k1gFnSc1	samička
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
vajíčkách	vajíčko	k1gNnPc6	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
mláďata	mládě	k1gNnPc4	mládě
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
pouze	pouze	k6eAd1	pouze
samička	samička	k1gFnSc1	samička
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jednom	jeden	k4xCgInSc6	jeden
příletu	přílet	k1gInSc6	přílet
nakrmí	nakrmit	k5eAaPmIp3nS	nakrmit
samička	samička	k1gFnSc1	samička
obě	dva	k4xCgNnPc4	dva
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
malý	malý	k2eAgInSc1d1	malý
ptáček	ptáček	k1gInSc1	ptáček
je	být	k5eAaImIp3nS	být
neohroženým	ohrožený	k2eNgMnSc7d1	neohrožený
bojovníkem	bojovník	k1gMnSc7	bojovník
a	a	k8xC	a
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
soky	sok	k1gMnPc7	sok
svádí	svádět	k5eAaImIp3nP	svádět
hlučné	hlučný	k2eAgInPc4d1	hlučný
vzdušné	vzdušný	k2eAgInPc4d1	vzdušný
souboje	souboj	k1gInPc4	souboj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
zalétá	zalétat	k5eAaPmIp3nS	zalétat
až	až	k9	až
do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
zimuje	zimovat	k5eAaImIp3nS	zimovat
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
a	a	k8xC	a
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
rychlosti	rychlost	k1gFnSc6	rychlost
mávne	mávnout	k5eAaPmIp3nS	mávnout
kolibřík	kolibřík	k1gMnSc1	kolibřík
rubínohrdlý	rubínohrdlý	k2eAgInSc4d1	rubínohrdlý
křídly	křídlo	k1gNnPc7	křídlo
až	až	k6eAd1	až
70	[number]	k4	70
<g/>
×	×	k?	×
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
křídla	křídlo	k1gNnSc2	křídlo
při	při	k7c6	při
takové	takový	k3xDgFnSc6	takový
rychlosti	rychlost	k1gFnSc6	rychlost
vidíme	vidět	k5eAaImIp1nP	vidět
pouze	pouze	k6eAd1	pouze
obláček	obláček	k1gInSc4	obláček
<g/>
.	.	kIx.	.
</s>
<s>
Obrovskou	obrovský	k2eAgFnSc4d1	obrovská
frekvenci	frekvence	k1gFnSc4	frekvence
mávání	mávání	k1gNnSc6	mávání
křídel	křídlo	k1gNnPc2	křídlo
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
díky	díky	k7c3	díky
přímému	přímý	k2eAgNnSc3d1	přímé
spalování	spalování	k1gNnSc3	spalování
naposledy	naposledy	k6eAd1	naposledy
zkonzumovaného	zkonzumovaný	k2eAgInSc2d1	zkonzumovaný
cukru	cukr	k1gInSc2	cukr
ve	v	k7c6	v
svalstvu	svalstvo	k1gNnSc6	svalstvo
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vyřazují	vyřazovat	k5eAaImIp3nP	vyřazovat
z	z	k7c2	z
bilance	bilance	k1gFnSc2	bilance
energetické	energetický	k2eAgInPc1d1	energetický
náklady	náklad	k1gInPc1	náklad
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
by	by	kYmCp3nP	by
je	on	k3xPp3gNnSc4	on
stály	stát	k5eAaImAgFnP	stát
<g/>
,	,	kIx,	,
kdyby	kdyby	k9	kdyby
dotyčný	dotyčný	k2eAgInSc4d1	dotyčný
cukr	cukr	k1gInSc4	cukr
nejprve	nejprve	k6eAd1	nejprve
proměnili	proměnit	k5eAaPmAgMnP	proměnit
na	na	k7c4	na
tuk	tuk	k1gInSc4	tuk
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kolibřík	kolibřík	k1gMnSc1	kolibřík
rubínohrdlý	rubínohrdlý	k2eAgMnSc1d1	rubínohrdlý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
https://web.archive.org/web/20140903132230/http://nasazem.eu/kolibrik-majster-latkovej-vymeny/	[url]	k4	https://web.archive.org/web/20140903132230/http://nasazem.eu/kolibrik-majster-latkovej-vymeny/
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
kolibřík	kolibřík	k1gMnSc1	kolibřík
rubínohrdlý	rubínohrdlý	k2eAgMnSc1d1	rubínohrdlý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
