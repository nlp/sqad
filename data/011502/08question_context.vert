<s>
Kolibřík	kolibřík	k1gMnSc1	kolibřík
rubínohrdlý	rubínohrdlý	k2eAgMnSc1d1	rubínohrdlý
(	(	kIx(	(
<g/>
Archilochus	Archilochus	k1gInSc1	Archilochus
colubris	colubris	k1gFnPc2	colubris
Linné	Linné	k1gNnSc1	Linné
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tažný	tažný	k2eAgMnSc1d1	tažný
pták	pták	k1gMnSc1	pták
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
kolibříkovitých	kolibříkovitý	k2eAgFnPc2d1	kolibříkovitý
(	(	kIx(	(
<g/>
Trochilidae	Trochilidae	k1gFnPc2	Trochilidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Zbarven	zbarven	k2eAgMnSc1d1	zbarven
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
zeleně	zeleně	k6eAd1	zeleně
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
šedé	šedý	k2eAgNnSc4d1	šedé
bříško	bříško	k1gNnSc4	bříško
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
obdržel	obdržet	k5eAaPmAgMnS	obdržet
od	od	k7c2	od
rubínově	rubínově	k6eAd1	rubínově
zbarvené	zbarvený	k2eAgFnSc2d1	zbarvená
skvrny	skvrna	k1gFnSc2	skvrna
na	na	k7c6	na
hrdle	hrdla	k1gFnSc6	hrdla
samce	samec	k1gInSc2	samec
<g/>
.	.	kIx.	.
</s>

