<s>
Michaela	Michaela	k1gFnSc1
Burdová	Burdová	k1gFnSc1
</s>
<s>
Michaela	Michaela	k1gFnSc1
Burdová	Burdová	k1gFnSc1
Narození	narození	k1gNnSc4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1989	#num#	k4
Příbram	Příbram	k1gFnSc4
Pseudonym	pseudonym	k1gInSc1
</s>
<s>
Nelien	Nelien	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
spisovatelka	spisovatelka	k1gFnSc1
<g/>
,	,	kIx,
malířka	malířka	k1gFnSc1
Národnost	národnost	k1gFnSc1
</s>
<s>
česká	český	k2eAgNnPc1d1
Vzdělání	vzdělání	k1gNnPc1
</s>
<s>
maturita	maturita	k1gFnSc1
-	-	kIx~
Střední	střední	k2eAgFnSc1d1
odborná	odborný	k2eAgFnSc1d1
škola	škola	k1gFnSc1
ekonomická	ekonomický	k2eAgFnSc1d1
Významná	významný	k2eAgFnSc1d1
díla	dílo	k1gNnPc4
</s>
<s>
trilogie	trilogie	k1gFnSc1
Poselství	poselství	k1gNnSc2
jednorožcůtetralogie	jednorožcůtetralogie	k1gFnSc2
Křišťály	křišťál	k1gInPc1
moci	moc	k1gFnSc2
</s>
<s>
„	„	k?
<g/>
Náhoda	náhoda	k1gFnSc1
neexistuje	existovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náhoda	náhoda	k1gFnSc1
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
převlečený	převlečený	k2eAgInSc1d1
osud	osud	k1gInSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Michaela	Michaela	k1gFnSc1
Burdová	Burdová	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
8	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1989	#num#	k4
<g/>
,	,	kIx,
Příbram	Příbram	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
občanským	občanský	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Michaela	Michael	k1gMnSc2
Prošková	Prošková	k1gFnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
česká	český	k2eAgFnSc1d1
spisovatelka	spisovatelka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
napsala	napsat	k5eAaBmAgFnS,k5eAaPmAgFnS
fantasy	fantas	k1gInPc4
trilogii	trilogie	k1gFnSc6
Poselství	poselství	k1gNnSc1
jednorožců	jednorožec	k1gMnPc2
<g/>
,	,	kIx,
tetralogii	tetralogie	k1gFnSc6
Křišťály	křišťál	k1gInPc4
moci	moc	k1gFnSc2
a	a	k8xC
dilogie	dilogie	k1gFnSc2
Syn	syn	k1gMnSc1
pekel	peklo	k1gNnPc2
a	a	k8xC
Volání	volání	k1gNnSc1
sirény	siréna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystupuje	vystupovat	k5eAaImIp3nS
občas	občas	k6eAd1
také	také	k9
pod	pod	k7c7
pseudonymem	pseudonym	k1gInSc7
Nelien	Nelina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
se	se	k3xPyFc4
provdala	provdat	k5eAaPmAgFnS
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
knihy	kniha	k1gFnPc4
nadále	nadále	k6eAd1
hodlá	hodlat	k5eAaImIp3nS
publikovat	publikovat	k5eAaBmF
pod	pod	k7c7
svým	svůj	k3xOyFgNnSc7
dívčím	dívčí	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Vyrostla	vyrůst	k5eAaPmAgFnS
v	v	k7c6
Sestrouni	Sestroun	k1gMnPc1
u	u	k7c2
Příbrami	Příbram	k1gFnSc2
<g/>
,	,	kIx,
absolvovala	absolvovat	k5eAaPmAgFnS
Střední	střední	k2eAgFnSc4d1
ekonomickou	ekonomický	k2eAgFnSc4d1
školu	škola	k1gFnSc4
v	v	k7c6
Sedlčanech	Sedlčany	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
psaní	psaní	k1gNnSc2
se	se	k3xPyFc4
také	také	k9
věnuje	věnovat	k5eAaPmIp3nS,k5eAaImIp3nS
kreslení	kreslení	k1gNnSc4
a	a	k8xC
malbě	malba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Miluje	milovat	k5eAaImIp3nS
zvířata	zvíře	k1gNnPc1
<g/>
,	,	kIx,
ráda	rád	k2eAgFnSc1d1
čte	číst	k5eAaImIp3nS
a	a	k8xC
to	ten	k3xDgNnSc1
především	především	k9
fantasy	fantas	k1gInPc1
literaturu	literatura	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgInSc1
dílo	dílo	k1gNnSc4
Poselství	poselství	k1gNnPc2
jednorožců	jednorožec	k1gMnPc2
začala	začít	k5eAaPmAgFnS
psát	psát	k5eAaImF
už	už	k6eAd1
v	v	k7c6
16	#num#	k4
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Její	její	k3xOp3gFnSc1
tvorba	tvorba	k1gFnSc1
se	se	k3xPyFc4
vyznačuje	vyznačovat	k5eAaImIp3nS
typickými	typický	k2eAgInPc7d1
prvky	prvek	k1gInPc7
fantasy	fantas	k1gInPc1
literatury	literatura	k1gFnSc2
<g/>
:	:	kIx,
fiktivními	fiktivní	k2eAgInPc7d1
světy	svět	k1gInPc7
<g/>
,	,	kIx,
fantazijními	fantazijní	k2eAgFnPc7d1
bytostmi	bytost	k1gFnPc7
(	(	kIx(
<g/>
draci	drak	k1gMnPc1
<g/>
,	,	kIx,
elfové	elf	k1gMnPc1
atd.	atd.	kA
<g/>
)	)	kIx)
a	a	k8xC
kouzly	kouzlo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
jejímu	její	k3xOp3gNnSc3
vegetariánskému	vegetariánský	k2eAgNnSc3d1
přesvědčení	přesvědčení	k1gNnSc3
se	se	k3xPyFc4
v	v	k7c6
jejích	její	k3xOp3gFnPc6
knihách	kniha	k1gFnPc6
objevují	objevovat	k5eAaImIp3nP
také	také	k9
i	i	k9
vegetariánské	vegetariánský	k2eAgInPc4d1
podtexty	podtext	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Koncem	koncem	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
vyšel	vyjít	k5eAaPmAgInS
první	první	k4xOgInSc4
díl	díl	k1gInSc4
trilogie	trilogie	k1gFnSc2
Poselství	poselství	k1gNnSc2
jednorožců	jednorožec	k1gMnPc2
v	v	k7c6
srbském	srbský	k2eAgNnSc6d1
nakladatelství	nakladatelství	k1gNnSc6
Evro	Evro	k1gNnSc1
Giunti	Giunti	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
ceně	cena	k1gFnSc6
dětí	dítě	k1gFnPc2
ankety	anketa	k1gFnSc2
SUK	suk	k1gInSc1
za	za	k7c4
rok	rok	k1gInSc4
2009	#num#	k4
za	za	k7c4
knihu	kniha	k1gFnSc4
Záchrana	záchrana	k1gFnSc1
Lilandgarie	Lilandgarie	k1gFnSc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
nominace	nominace	k1gFnSc1
na	na	k7c4
Zlatou	zlatý	k2eAgFnSc4d1
knihu	kniha	k1gFnSc4
2009	#num#	k4
-	-	kIx~
trilogie	trilogie	k1gFnSc1
Poselství	poselství	k1gNnSc2
jednorožců	jednorožec	k1gMnPc2
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
nominace	nominace	k1gFnSc1
na	na	k7c4
Zlatou	zlatý	k2eAgFnSc4d1
knihu	kniha	k1gFnSc4
2009	#num#	k4
-	-	kIx~
kategorie	kategorie	k1gFnSc2
Autoři	autor	k1gMnPc1
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Poselství	poselství	k1gNnSc1
jednorožců	jednorožec	k1gMnPc2
</s>
<s>
Strážci	strážce	k1gMnPc1
dobra	dobro	k1gNnSc2
<g/>
,	,	kIx,
Fragment	fragment	k1gInSc1
<g/>
,	,	kIx,
2008	#num#	k4
</s>
<s>
Zrádné	zrádný	k2eAgFnPc1d1
hory	hora	k1gFnPc1
Dragor	Dragor	k1gInSc1
<g/>
,	,	kIx,
Fragment	fragment	k1gInSc1
<g/>
,	,	kIx,
2008	#num#	k4
</s>
<s>
Záchrana	záchrana	k1gFnSc1
Lilandgarie	Lilandgarie	k1gFnSc1
<g/>
,	,	kIx,
Fragment	fragment	k1gInSc1
<g/>
,	,	kIx,
2009	#num#	k4
</s>
<s>
Snížkova	Snížkův	k2eAgNnPc1d1
dobrodružství	dobrodružství	k1gNnPc1
<g/>
,	,	kIx,
Fragment	fragment	k1gInSc1
<g/>
,	,	kIx,
2016	#num#	k4
</s>
<s>
Křišťály	křišťál	k1gInPc1
moci	moc	k1gFnSc2
</s>
<s>
Zrada	zrada	k1gFnSc1
temného	temný	k2eAgMnSc2d1
elfa	elf	k1gMnSc2
<g/>
,	,	kIx,
Fragment	fragment	k1gInSc1
<g/>
,	,	kIx,
2010	#num#	k4
</s>
<s>
Hněv	hněv	k1gInSc4
Pána	pán	k1gMnSc2
ohně	oheň	k1gInSc2
<g/>
,	,	kIx,
Fragment	fragment	k1gInSc1
<g/>
,	,	kIx,
2011	#num#	k4
</s>
<s>
Věž	věž	k1gFnSc1
zkázy	zkáza	k1gFnSc2
<g/>
,	,	kIx,
Fragment	fragment	k1gInSc1
<g/>
,	,	kIx,
2011	#num#	k4
</s>
<s>
Minotaurus	Minotaurus	k1gInSc1
<g/>
,	,	kIx,
Fragment	fragment	k1gInSc1
<g/>
,	,	kIx,
2012	#num#	k4
</s>
<s>
Terienina	Terienin	k2eAgFnSc1d1
minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
e-kniha	e-kniha	k1gFnSc1
<g/>
,	,	kIx,
2013	#num#	k4
</s>
<s>
Syn	syn	k1gMnSc1
pekel	peklo	k1gNnPc2
</s>
<s>
Vlčí	vlčí	k2eAgFnSc1d1
krev	krev	k1gFnSc1
<g/>
,	,	kIx,
Fragment	fragment	k1gInSc1
<g/>
,	,	kIx,
2013	#num#	k4
</s>
<s>
V	v	k7c6
moci	moc	k1gFnSc6
démonů	démon	k1gMnPc2
<g/>
,	,	kIx,
Fragment	fragment	k1gInSc1
<g/>
,	,	kIx,
2013	#num#	k4
</s>
<s>
Volání	volání	k1gNnSc1
sirény	siréna	k1gFnSc2
</s>
<s>
Prokletí	prokletí	k1gNnSc1
<g/>
,	,	kIx,
Fragment	fragment	k1gInSc1
<g/>
,	,	kIx,
2014	#num#	k4
</s>
<s>
Pomsta	pomsta	k1gFnSc1
<g/>
,	,	kIx,
Fragment	fragment	k1gInSc1
<g/>
,	,	kIx,
2015	#num#	k4
</s>
<s>
Další	další	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
</s>
<s>
Dcera	dcera	k1gFnSc1
hvězd	hvězda	k1gFnPc2
<g/>
,	,	kIx,
Klika	klika	k1gFnSc1
<g/>
,	,	kIx,
2017	#num#	k4
</s>
<s>
4	#num#	k4
kroky	krok	k1gInPc4
jak	jak	k8xS,k8xC
napsat	napsat	k5eAaPmF,k5eAaBmF
knihu	kniha	k1gFnSc4
<g/>
,	,	kIx,
2017	#num#	k4
</s>
<s>
Rozpracované	rozpracovaný	k2eAgNnSc1d1
dílo	dílo	k1gNnSc1
</s>
<s>
Znamení	znamení	k1gNnSc1
draka	drak	k1gMnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.michaelaburdova.com/autorka-/	http://www.michaelaburdova.com/autorka-/	k?
<g/>
↑	↑	k?
http://miluju-knihy.blogspot.cz/2014/01/rc-recenze-poselstvi-jednorozcu.html	http://miluju-knihy.blogspot.cz/2014/01/rc-recenze-poselstvi-jednorozcu.html	k1gInSc1
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
burdova-michaela	burdova-michaela	k1gFnSc1
<g/>
.	.	kIx.
<g/>
blog	blog	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.evro-giunti.com	www.evro-giunti.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.npkk.cz	www.npkk.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
http://www.cbdb.cz/novinka-42-zlata-kniha-2010-vysledky	http://www.cbdb.cz/novinka-42-zlata-kniha-2010-vysledek	k1gInPc7
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Michaela	Michaela	k1gFnSc1
Burdová	Burdová	k1gFnSc1
</s>
<s>
burdova-michaela	burdova-michaela	k1gFnSc1
<g/>
.	.	kIx.
<g/>
blog	blog	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
Archivováno	archivován	k2eAgNnSc1d1
26	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
</s>
<s>
michaela-burdova-galerie	michaela-burdova-galerie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
blog	blog	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
výtvarná	výtvarný	k2eAgFnSc1d1
tvorba	tvorba	k1gFnSc1
</s>
<s>
michaelaburdova	michaelaburdův	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
com	com	k?
-	-	kIx~
nové	nový	k2eAgFnSc2d1
oficiální	oficiální	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
84341	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
85858057	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Literatura	literatura	k1gFnSc1
</s>
