<s>
Norsko	Norsko	k1gNnSc1	Norsko
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
hornatá	hornatý	k2eAgFnSc1d1	hornatá
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Galdhø	Galdhø	k1gFnSc2	Galdhø
měří	měřit	k5eAaImIp3nS	měřit
2469	[number]	k4	2469
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
