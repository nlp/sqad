<p>
<s>
Písek	Písek	k1gInSc1	Písek
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
:	:	kIx,	:
<g/>
Pisek	Pisek	k6eAd1	Pisek
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
ve	v	k7c6	v
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
okrese	okres	k1gInSc6	okres
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
44	[number]	k4	44
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
,	,	kIx,	,
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Otava	Otava	k1gFnSc1	Otava
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Písek	Písek	k1gInSc1	Písek
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1243	[number]	k4	1243
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
zónou	zóna	k1gFnSc7	zóna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
městem	město	k1gNnSc7	město
krajským	krajský	k2eAgNnSc7d1	krajské
<g/>
,	,	kIx,	,
sídelním	sídelní	k2eAgNnSc7d1	sídelní
městem	město	k1gNnSc7	město
Prácheňského	prácheňský	k2eAgInSc2d1	prácheňský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
kamenný	kamenný	k2eAgInSc1d1	kamenný
most	most	k1gInSc1	most
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
nejstarším	starý	k2eAgMnSc7d3	nejstarší
českým	český	k2eAgInSc7d1	český
stojícím	stojící	k2eAgInSc7d1	stojící
mostem	most	k1gInSc7	most
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
pamětihodnostmi	pamětihodnost	k1gFnPc7	pamětihodnost
jsou	být	k5eAaImIp3nP	být
královský	královský	k2eAgInSc4d1	královský
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
gotický	gotický	k2eAgInSc4d1	gotický
kostel	kostel	k1gInSc4	kostel
Narození	narození	k1gNnSc2	narození
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
s	s	k7c7	s
hodinovou	hodinový	k2eAgFnSc7d1	hodinová
věží	věž	k1gFnSc7	věž
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
dominanta	dominanta	k1gFnSc1	dominanta
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
několik	několik	k4yIc4	několik
úseků	úsek	k1gInPc2	úsek
zachovaných	zachovaný	k2eAgFnPc2d1	zachovaná
hradeb	hradba	k1gFnPc2	hradba
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
však	však	k9	však
byla	být	k5eAaImAgFnS	být
během	během	k7c2	během
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
revoluce	revoluce	k1gFnSc2	revoluce
zbořena	zbořen	k2eAgFnSc1d1	zbořena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
Písku	Písek	k1gInSc6	Písek
se	s	k7c7	s
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
relativně	relativně	k6eAd1	relativně
vysokému	vysoký	k2eAgInSc3d1	vysoký
počtu	počet	k1gInSc3	počet
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
někdy	někdy	k6eAd1	někdy
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
město	město	k1gNnSc1	město
univerzitního	univerzitní	k2eAgInSc2d1	univerzitní
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Skutečná	skutečný	k2eAgFnSc1d1	skutečná
univerzita	univerzita	k1gFnSc1	univerzita
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
nenachází	nacházet	k5eNaImIp3nS	nacházet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
zde	zde	k6eAd1	zde
působí	působit	k5eAaImIp3nS	působit
soukromá	soukromý	k2eAgFnSc1d1	soukromá
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
Filmová	filmový	k2eAgFnSc1d1	filmová
akademie	akademie	k1gFnSc1	akademie
Miroslava	Miroslav	k1gMnSc2	Miroslav
Ondříčka	Ondříček	k1gMnSc2	Ondříček
<g/>
,	,	kIx,	,
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
z	z	k7c2	z
VOŠ	VOŠ	kA	VOŠ
<g/>
.	.	kIx.	.
</s>
<s>
Písek	Písek	k1gInSc1	Písek
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgNnPc4	dva
divadla	divadlo	k1gNnPc4	divadlo
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgInPc4d1	kulturní
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
nemocnici	nemocnice	k1gFnSc4	nemocnice
<g/>
,	,	kIx,	,
fotbalový	fotbalový	k2eAgInSc4d1	fotbalový
stadion	stadion	k1gInSc4	stadion
a	a	k8xC	a
důležitý	důležitý	k2eAgInSc4d1	důležitý
zemský	zemský	k2eAgInSc4d1	zemský
plemenný	plemenný	k2eAgInSc4d1	plemenný
hřebčinec	hřebčinec	k1gInSc4	hřebčinec
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
širším	široký	k2eAgNnSc7d2	širší
okolím	okolí	k1gNnSc7	okolí
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
rekonstruované	rekonstruovaný	k2eAgFnSc2d1	rekonstruovaná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
podporu	podpor	k1gInSc6	podpor
rozvíjejícího	rozvíjející	k2eAgMnSc2d1	rozvíjející
se	se	k3xPyFc4	se
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
se	se	k3xPyFc4	se
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
města	město	k1gNnSc2	město
nachází	nacházet	k5eAaImIp3nS	nacházet
infocentrum	infocentrum	k1gNnSc1	infocentrum
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
v	v	k7c6	v
objektu	objekt	k1gInSc6	objekt
bývalé	bývalý	k2eAgFnSc2d1	bývalá
sladovny	sladovna	k1gFnSc2	sladovna
společně	společně	k6eAd1	společně
se	s	k7c7	s
zařízením	zařízení	k1gNnSc7	zařízení
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Pravěk	pravěk	k1gInSc1	pravěk
a	a	k8xC	a
středověk	středověk	k1gInSc1	středověk
===	===	k?	===
</s>
</p>
<p>
<s>
Lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
v	v	k7c6	v
písecké	písecký	k2eAgFnSc6d1	Písecká
kotlině	kotlina	k1gFnSc6	kotlina
poprvé	poprvé	k6eAd1	poprvé
objevili	objevit	k5eAaPmAgMnP	objevit
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
starší	starý	k2eAgFnSc2d2	starší
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgNnSc4d1	kamenné
a	a	k8xC	a
usadili	usadit	k5eAaPmAgMnP	usadit
se	se	k3xPyFc4	se
severně	severně	k6eAd1	severně
od	od	k7c2	od
dnešního	dnešní	k2eAgNnSc2d1	dnešní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgMnPc7	první
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
pozdějšího	pozdní	k2eAgNnSc2d2	pozdější
středověkého	středověký	k2eAgNnSc2d1	středověké
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
Keltové	Kelt	k1gMnPc1	Kelt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
poli	pole	k1gNnSc6	pole
mezi	mezi	k7c7	mezi
Hradištěm	Hradiště	k1gNnSc7	Hradiště
a	a	k8xC	a
Putimí	Putim	k1gFnSc7	Putim
byly	být	k5eAaImAgFnP	být
nalezeny	nalezen	k2eAgInPc1d1	nalezen
dvě	dva	k4xCgFnPc1	dva
mohyly	mohyla	k1gFnPc1	mohyla
s	s	k7c7	s
keltským	keltský	k2eAgInSc7d1	keltský
pokladem	poklad	k1gInSc7	poklad
<g/>
.	.	kIx.	.
</s>
<s>
Nejvzácnější	vzácný	k2eAgFnSc1d3	nejvzácnější
byla	být	k5eAaImAgFnS	být
kovová	kovový	k2eAgFnSc1d1	kovová
tepaná	tepaný	k2eAgFnSc1d1	tepaná
zobákovitá	zobákovitý	k2eAgFnSc1d1	zobákovitá
konvice	konvice	k1gFnSc1	konvice
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
několik	několik	k4yIc1	několik
zlatých	zlatý	k2eAgInPc2d1	zlatý
prutů	prut	k1gInPc2	prut
a	a	k8xC	a
zlaté	zlatý	k2eAgFnPc4d1	zlatá
a	a	k8xC	a
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
ozdoby	ozdoba	k1gFnPc4	ozdoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podnětem	podnět	k1gInSc7	podnět
pro	pro	k7c4	pro
založení	založení	k1gNnSc4	založení
Písku	Písek	k1gInSc2	Písek
bylo	být	k5eAaImAgNnS	být
nerostné	nerostný	k2eAgNnSc4d1	nerostné
bohatství	bohatství	k1gNnSc4	bohatství
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
zlatonosného	zlatonosný	k2eAgInSc2d1	zlatonosný
písku	písek	k1gInSc2	písek
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
již	již	k6eAd1	již
od	od	k7c2	od
konce	konec	k1gInSc2	konec
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
stála	stát	k5eAaImAgFnS	stát
rýžovnická	rýžovnický	k2eAgFnSc1d1	rýžovnická
vesnice	vesnice	k1gFnSc1	vesnice
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Otavy	Otava	k1gFnSc2	Otava
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
osada	osada	k1gFnSc1	osada
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
"	"	kIx"	"
<g/>
Na	na	k7c6	na
Písku	Písek	k1gInSc6	Písek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
z	z	k7c2	z
osady	osada	k1gFnSc2	osada
stala	stát	k5eAaPmAgFnS	stát
trhová	trhový	k2eAgFnSc1d1	trhová
ves	ves	k1gFnSc1	ves
s	s	k7c7	s
kostelem	kostel	k1gInSc7	kostel
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Písecká	písecký	k2eAgFnSc1d1	Písecká
těžba	těžba	k1gFnSc1	těžba
zlata	zlato	k1gNnSc2	zlato
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
její	její	k3xOp3gFnSc4	její
vzkříšení	vzkříšení	k1gNnSc1	vzkříšení
nesplnilo	splnit	k5eNaPmAgNnS	splnit
očekávání	očekávání	k1gNnPc4	očekávání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
rýžování	rýžování	k1gNnSc2	rýžování
zlatonosného	zlatonosný	k2eAgInSc2d1	zlatonosný
písku	písek	k1gInSc2	písek
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Písek	Písek	k1gInSc1	Písek
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
v	v	k7c6	v
datační	datační	k2eAgFnSc6d1	datační
formuli	formule	k1gFnSc6	formule
listiny	listina	k1gFnSc2	listina
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
I.	I.	kA	I.
vydané	vydaný	k2eAgFnPc4d1	vydaná
roku	rok	k1gInSc2	rok
1243	[number]	k4	1243
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
I.	I.	kA	I.
začal	začít	k5eAaPmAgInS	začít
nedaleko	nedaleko	k7c2	nedaleko
osady	osada	k1gFnSc2	osada
na	na	k7c6	na
skalnatém	skalnatý	k2eAgInSc6d1	skalnatý
břehu	břeh	k1gInSc6	břeh
stavět	stavět	k5eAaImF	stavět
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lokace	lokace	k1gFnSc1	lokace
města	město	k1gNnSc2	město
mohla	moct	k5eAaImAgFnS	moct
proběhnout	proběhnout	k5eAaPmF	proběhnout
již	již	k6eAd1	již
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
vlády	vláda	k1gFnSc2	vláda
Václava	Václav	k1gMnSc2	Václav
I.	I.	kA	I.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vlastní	vlastní	k2eAgNnSc1d1	vlastní
budování	budování	k1gNnSc1	budování
bylo	být	k5eAaImAgNnS	být
zásluhou	zásluha	k1gFnSc7	zásluha
až	až	k8xS	až
jeho	on	k3xPp3gMnSc2	on
syna	syn	k1gMnSc2	syn
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ve	v	k7c6	v
městě	město	k1gNnSc6	město
často	často	k6eAd1	často
přebýval	přebývat	k5eAaImAgMnS	přebývat
a	a	k8xC	a
který	který	k3yQgMnSc1	který
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1254	[number]	k4	1254
založil	založit	k5eAaPmAgMnS	založit
hrazené	hrazený	k2eAgNnSc4d1	hrazené
královské	královský	k2eAgNnSc4d1	královské
město	město	k1gNnSc4	město
s	s	k7c7	s
královskou	královský	k2eAgFnSc7d1	královská
mincovnou	mincovna	k1gFnSc7	mincovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
nebylo	být	k5eNaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
jen	jen	k9	jen
pro	pro	k7c4	pro
těžbu	těžba	k1gFnSc4	těžba
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
zároveň	zároveň	k6eAd1	zároveň
ochraňovat	ochraňovat	k5eAaImF	ochraňovat
obchodní	obchodní	k2eAgNnSc4d1	obchodní
</s>
</p>
<p>
<s>
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
stezku	stezka	k1gFnSc4	stezka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
místech	místo	k1gNnPc6	místo
překračovala	překračovat	k5eAaImAgFnS	překračovat
Otavu	Otava	k1gFnSc4	Otava
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
být	být	k5eAaImF	být
základnou	základna	k1gFnSc7	základna
královské	královský	k2eAgFnSc2d1	královská
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
hlavní	hlavní	k2eAgFnPc1d1	hlavní
budovy	budova	k1gFnPc1	budova
(	(	kIx(	(
<g/>
městský	městský	k2eAgInSc1d1	městský
hrad	hrad	k1gInSc1	hrad
<g/>
,	,	kIx,	,
kamenný	kamenný	k2eAgInSc1d1	kamenný
most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
dominikánský	dominikánský	k2eAgInSc1d1	dominikánský
klášter	klášter	k1gInSc1	klášter
<g/>
,	,	kIx,	,
rychta	rychta	k1gFnSc1	rychta
<g/>
)	)	kIx)	)
vyrostly	vyrůst	k5eAaPmAgFnP	vyrůst
společně	společně	k6eAd1	společně
s	s	k7c7	s
opevněním	opevnění	k1gNnSc7	opevnění
snad	snad	k9	snad
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
pouhých	pouhý	k2eAgInPc2d1	pouhý
dvou	dva	k4xCgInPc2	dva
desetiletí	desetiletí	k1gNnPc2	desetiletí
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterých	který	k3yRgFnPc2	který
tu	tu	k6eAd1	tu
král	král	k1gMnSc1	král
vícekrát	vícekrát	k6eAd1	vícekrát
pobýval	pobývat	k5eAaImAgMnS	pobývat
<g/>
;	;	kIx,	;
v	v	k7c6	v
letech	let	k1gInPc6	let
1258	[number]	k4	1258
<g/>
–	–	k?	–
<g/>
1265	[number]	k4	1265
s	s	k7c7	s
jedinou	jediný	k2eAgFnSc7d1	jediná
výjimkou	výjimka	k1gFnSc7	výjimka
každoročně	každoročně	k6eAd1	každoročně
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
doba	doba	k1gFnSc1	doba
rozkvětu	rozkvět	k1gInSc2	rozkvět
Písku	Písek	k1gInSc2	Písek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tehdy	tehdy	k6eAd1	tehdy
patřil	patřit	k5eAaImAgInS	patřit
k	k	k7c3	k
nejpřednějším	přední	k2eAgNnPc3d3	nejpřednější
městům	město	k1gNnPc3	město
v	v	k7c6	v
království	království	k1gNnSc6	království
a	a	k8xC	a
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
významu	význam	k1gInSc3	význam
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
neopakoval	opakovat	k5eNaImAgMnS	opakovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
král	král	k1gMnSc1	král
založil	založit	k5eAaPmAgMnS	založit
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Nový	nový	k2eAgInSc1d1	nový
Písek	Písek	k1gInSc1	Písek
<g/>
,	,	kIx,	,
osada	osada	k1gFnSc1	osada
Starý	Starý	k1gMnSc1	Starý
Písek	Písek	k1gInSc1	Písek
si	se	k3xPyFc3	se
výslovně	výslovně	k6eAd1	výslovně
nepřála	přát	k5eNaImAgFnS	přát
být	být	k5eAaImF	být
s	s	k7c7	s
Novým	nový	k2eAgInSc7d1	nový
Pískem	Písek	k1gInSc7	Písek
spojována	spojován	k2eAgFnSc1d1	spojována
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
král	král	k1gMnSc1	král
obyvatelům	obyvatel	k1gMnPc3	obyvatel
Starého	Starého	k2eAgInSc2d1	Starého
Písku	Písek	k1gInSc2	Písek
garantoval	garantovat	k5eAaBmAgMnS	garantovat
samostatnost	samostatnost	k1gFnSc4	samostatnost
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
Písek	Písek	k1gInSc1	Písek
však	však	k9	však
ve	v	k7c4	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
převzal	převzít	k5eAaPmAgInS	převzít
roli	role	k1gFnSc4	role
sídla	sídlo	k1gNnSc2	sídlo
Prácheňského	prácheňský	k2eAgInSc2d1	prácheňský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
plnil	plnit	k5eAaImAgInS	plnit
hrad	hrad	k1gInSc1	hrad
Prácheň	Prácheň	k1gFnSc4	Prácheň
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgInSc1d1	starý
a	a	k8xC	a
Nový	nový	k2eAgInSc1d1	nový
Písek	Písek	k1gInSc1	Písek
splynuly	splynout	k5eAaPmAgInP	splynout
až	až	k6eAd1	až
po	po	k7c6	po
staletích	staletí	k1gNnPc6	staletí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1327	[number]	k4	1327
(	(	kIx(	(
<g/>
jiný	jiný	k2eAgInSc1d1	jiný
zdroj	zdroj	k1gInSc1	zdroj
uvádí	uvádět	k5eAaImIp3nS	uvádět
rok	rok	k1gInSc4	rok
1308	[number]	k4	1308
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
propůjčil	propůjčit	k5eAaPmAgInS	propůjčit
městu	město	k1gNnSc3	město
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
městská	městský	k2eAgFnSc1d1	městská
práva	právo	k1gNnPc4	právo
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
pražského	pražský	k2eAgNnSc2d1	Pražské
(	(	kIx(	(
<g/>
právo	právo	k1gNnSc1	právo
vybírat	vybírat	k5eAaImF	vybírat
mýto	mýto	k1gNnSc4	mýto
<g/>
,	,	kIx,	,
osvobození	osvobození	k1gNnSc4	osvobození
od	od	k7c2	od
cla	clo	k1gNnSc2	clo
a	a	k8xC	a
mýta	mýto	k1gNnSc2	mýto
<g/>
,	,	kIx,	,
mílové	mílový	k2eAgNnSc4d1	mílové
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
zřídit	zřídit	k5eAaPmF	zřídit
solný	solný	k2eAgInSc4d1	solný
sklad	sklad	k1gInSc4	sklad
<g/>
,	,	kIx,	,
obilnice	obilnice	k1gFnSc1	obilnice
–	–	k?	–
největší	veliký	k2eAgFnSc7d3	veliký
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
krátce	krátce	k6eAd1	krátce
pobýval	pobývat	k5eAaImAgMnS	pobývat
jak	jak	k8xC	jak
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
Nový	nový	k2eAgInSc1d1	nový
Písek	Písek	k1gInSc1	Písek
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Prácheňského	prácheňský	k2eAgInSc2d1	prácheňský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
do	do	k7c2	do
města	město	k1gNnSc2	město
často	často	k6eAd1	často
zajížděl	zajíždět	k5eAaImAgInS	zajíždět
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
posledním	poslední	k2eAgMnSc7d1	poslední
panovníkem	panovník	k1gMnSc7	panovník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pobýval	pobývat	k5eAaImAgMnS	pobývat
na	na	k7c6	na
zdejším	zdejší	k2eAgInSc6d1	zdejší
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
hrál	hrát	k5eAaImAgInS	hrát
Písek	Písek	k1gInSc1	Písek
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
a	a	k8xC	a
stál	stát	k5eAaImAgInS	stát
již	již	k6eAd1	již
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
reformního	reformní	k2eAgNnSc2d1	reformní
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Písečtí	písecký	k2eAgMnPc1d1	písecký
obyvatelé	obyvatel	k1gMnPc1	obyvatel
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
jedni	jeden	k4xCgMnPc1	jeden
z	z	k7c2	z
prvních	první	k4xOgNnPc6	první
hlásili	hlásit	k5eAaImAgMnP	hlásit
k	k	k7c3	k
husitskému	husitský	k2eAgNnSc3d1	husitské
hnutí	hnutí	k1gNnSc3	hnutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Husitské	husitský	k2eAgNnSc4d1	husitské
období	období	k1gNnSc4	období
===	===	k?	===
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1419	[number]	k4	1419
úspěšně	úspěšně	k6eAd1	úspěšně
zaútočili	zaútočit	k5eAaPmAgMnP	zaútočit
Písečtí	písecký	k2eAgMnPc1d1	písecký
na	na	k7c4	na
dominikánský	dominikánský	k2eAgInSc4d1	dominikánský
klášter	klášter	k1gInSc4	klášter
a	a	k8xC	a
následně	následně	k6eAd1	následně
jako	jako	k8xC	jako
jedni	jeden	k4xCgMnPc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
se	se	k3xPyFc4	se
připojili	připojit	k5eAaPmAgMnP	připojit
k	k	k7c3	k
Jednotě	jednota	k1gFnSc3	jednota
táborské	táborský	k2eAgFnSc3d1	táborská
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
Táboře	Tábor	k1gInSc6	Tábor
i	i	k8xC	i
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
byly	být	k5eAaImAgFnP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
kádě	káď	k1gFnPc1	káď
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yQgMnPc2	který
občané	občan	k1gMnPc1	občan
odevzdávali	odevzdávat	k5eAaImAgMnP	odevzdávat
své	svůj	k3xOyFgNnSc4	svůj
bohatství	bohatství	k1gNnSc4	bohatství
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Písek	Písek	k1gInSc1	Písek
má	mít	k5eAaImIp3nS	mít
před	před	k7c7	před
Táborem	Tábor	k1gInSc7	Tábor
prvenství	prvenství	k1gNnSc2	prvenství
ohledně	ohledně	k7c2	ohledně
těchto	tento	k3xDgFnPc2	tento
kádí	káď	k1gFnPc2	káď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
husitství	husitství	k1gNnSc2	husitství
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
častým	častý	k2eAgMnSc7d1	častý
hostem	host	k1gMnSc7	host
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
<g/>
.	.	kIx.	.
</s>
<s>
Vládcem	vládce	k1gMnSc7	vládce
Písku	Písek	k1gInSc2	Písek
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
stal	stát	k5eAaPmAgMnS	stát
husitský	husitský	k2eAgMnSc1d1	husitský
hejtman	hejtman	k1gMnSc1	hejtman
Matěj	Matěj	k1gMnSc1	Matěj
Louda	Louda	k1gMnSc1	Louda
z	z	k7c2	z
Chlumčan	Chlumčan	k1gMnSc1	Chlumčan
<g/>
.	.	kIx.	.
</s>
<s>
Písečtí	písecký	k2eAgMnPc1d1	písecký
zůstali	zůstat	k5eAaPmAgMnP	zůstat
myšlenkám	myšlenka	k1gFnPc3	myšlenka
husitství	husitství	k1gNnSc2	husitství
věrni	věren	k2eAgMnPc1d1	věren
až	až	k8xS	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
na	na	k7c6	na
faře	fara	k1gFnSc6	fara
působil	působit	k5eAaImAgMnS	působit
také	také	k9	také
první	první	k4xOgMnSc1	první
a	a	k8xC	a
poslední	poslední	k2eAgMnSc1d1	poslední
biskup	biskup	k1gMnSc1	biskup
bratrstva	bratrstvo	k1gNnSc2	bratrstvo
Mikuláš	mikuláš	k1gInSc1	mikuláš
z	z	k7c2	z
Pelhřimova	Pelhřimov	k1gInSc2	Pelhřimov
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vojenské	vojenský	k2eAgFnSc6d1	vojenská
porážce	porážka	k1gFnSc6	porážka
Tábora	Tábor	k1gInSc2	Tábor
Jiřím	Jiří	k1gMnPc3	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
výhrůžce	výhrůžka	k1gFnSc6	výhrůžka
Píseckým	písecký	k2eAgInSc7d1	písecký
k	k	k7c3	k
dohodě	dohoda	k1gFnSc3	dohoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
období	období	k1gNnSc6	období
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
nebývalému	nebývalý	k2eAgInSc3d1	nebývalý
rozkvětu	rozkvět	k1gInSc3	rozkvět
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
umožnil	umožnit	k5eAaPmAgMnS	umožnit
městu	město	k1gNnSc3	město
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1509	[number]	k4	1509
koupit	koupit	k5eAaPmF	koupit
královský	královský	k2eAgInSc4d1	královský
hrad	hrad	k1gInSc4	hrad
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
a	a	k8xC	a
královské	královský	k2eAgNnSc1d1	královské
panství	panství	k1gNnSc1	panství
<g/>
.	.	kIx.	.
</s>
<s>
Písecký	písecký	k2eAgInSc1d1	písecký
majetek	majetek	k1gInSc1	majetek
a	a	k8xC	a
vliv	vliv	k1gInSc1	vliv
sahal	sahat	k5eAaImAgInS	sahat
od	od	k7c2	od
Mirotic	Mirotice	k1gFnPc2	Mirotice
až	až	k9	až
k	k	k7c3	k
Protivínu	Protivín	k1gInSc3	Protivín
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1532	[number]	k4	1532
zachvátil	zachvátit	k5eAaPmAgInS	zachvátit
město	město	k1gNnSc4	město
veliký	veliký	k2eAgInSc1d1	veliký
požár	požár	k1gInSc1	požár
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
poničil	poničit	k5eAaPmAgInS	poničit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pod	pod	k7c7	pod
Habsburským	habsburský	k2eAgNnSc7d1	habsburské
žezlem	žezlo	k1gNnSc7	žezlo
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
pražské	pražský	k2eAgFnSc6d1	Pražská
defenestraci	defenestrace	k1gFnSc6	defenestrace
roku	rok	k1gInSc2	rok
1618	[number]	k4	1618
se	se	k3xPyFc4	se
Písek	Písek	k1gInSc1	Písek
přidal	přidat	k5eAaPmAgInS	přidat
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
vypálení	vypálení	k1gNnSc4	vypálení
píseckých	písecký	k2eAgNnPc2d1	písecké
předměstí	předměstí	k1gNnPc2	předměstí
a	a	k8xC	a
okolních	okolní	k2eAgFnPc2d1	okolní
vesnic	vesnice	k1gFnPc2	vesnice
císařskými	císařský	k2eAgMnPc7d1	císařský
oddíly	oddíl	k1gInPc1	oddíl
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1619	[number]	k4	1619
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
několikrát	několikrát	k6eAd1	několikrát
obléháno	obléhat	k5eAaImNgNnS	obléhat
<g/>
,	,	kIx,	,
ostřelováno	ostřelovat	k5eAaImNgNnS	ostřelovat
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
vydrancováno	vydrancován	k2eAgNnSc1d1	vydrancováno
postupně	postupně	k6eAd1	postupně
generálem	generál	k1gMnSc7	generál
Buquoyem	Buquoy	k1gMnSc7	Buquoy
<g/>
,	,	kIx,	,
Mansfeldem	Mansfeld	k1gMnSc7	Mansfeld
a	a	k8xC	a
rok	rok	k1gInSc4	rok
nato	nato	k6eAd1	nato
Maxmiliánem	Maxmilián	k1gMnSc7	Maxmilián
Bavorským	bavorský	k2eAgMnSc7d1	bavorský
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
každého	každý	k3xTgNnSc2	každý
dobytí	dobytí	k1gNnSc2	dobytí
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
vypáleno	vypálit	k5eAaPmNgNnS	vypálit
a	a	k8xC	a
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
téměř	téměř	k6eAd1	téměř
vyvražděno	vyvraždit	k5eAaPmNgNnS	vyvraždit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1623	[number]	k4	1623
byl	být	k5eAaImAgMnS	být
dosazen	dosadit	k5eAaPmNgMnS	dosadit
jako	jako	k8xC	jako
správce	správce	k1gMnSc1	správce
města	město	k1gNnSc2	město
císařský	císařský	k2eAgMnSc1d1	císařský
generál	generál	k1gMnSc1	generál
Martin	Martin	k1gMnSc1	Martin
de	de	k?	de
Huerta	Huerta	k1gFnSc1	Huerta
<g/>
,	,	kIx,	,
lidově	lidově	k6eAd1	lidově
přezdívaný	přezdívaný	k2eAgMnSc1d1	přezdívaný
"	"	kIx"	"
<g/>
Poberta	poberta	k1gMnSc1	poberta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pomocí	pomocí	k7c2	pomocí
tyranizování	tyranizování	k1gNnSc2	tyranizování
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
začal	začít	k5eAaPmAgMnS	začít
Písecké	písecký	k2eAgNnSc1d1	písecké
obracet	obracet	k5eAaImF	obracet
na	na	k7c4	na
katolickou	katolický	k2eAgFnSc4d1	katolická
víru	víra	k1gFnSc4	víra
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
ožebračení	ožebračení	k1gNnSc2	ožebračení
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
po	po	k7c6	po
správcově	správcův	k2eAgFnSc6d1	správcova
smrti	smrt	k1gFnSc6	smrt
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
opětovně	opětovně	k6eAd1	opětovně
ustanoveno	ustanovit	k5eAaPmNgNnS	ustanovit
jako	jako	k8xS	jako
krajské	krajský	k2eAgNnSc1d1	krajské
město	město	k1gNnSc1	město
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1641	[number]	k4	1641
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgFnP	být
navráceny	navrácen	k2eAgFnPc1d1	navrácena
některé	některý	k3yIgFnPc1	některý
královské	královský	k2eAgFnPc1d1	královská
výsady	výsada	k1gFnPc1	výsada
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
výslovným	výslovný	k2eAgInSc7d1	výslovný
dodatkem	dodatek	k1gInSc7	dodatek
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gMnPc4	on
může	moct	k5eAaImIp3nS	moct
využívat	využívat	k5eAaImF	využívat
jen	jen	k9	jen
katolické	katolický	k2eAgNnSc4d1	katolické
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
finančním	finanční	k2eAgInPc3d1	finanční
problémům	problém	k1gInPc3	problém
města	město	k1gNnSc2	město
a	a	k8xC	a
k	k	k7c3	k
řádění	řádění	k1gNnSc3	řádění
moru	mor	k1gInSc2	mor
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
projev	projev	k1gInSc4	projev
díku	dík	k1gInSc2	dík
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc4	město
uchráněno	uchráněn	k2eAgNnSc4d1	uchráněno
morové	morový	k2eAgFnPc4d1	morová
vlny	vlna	k1gFnPc4	vlna
<g/>
,	,	kIx,	,
nechali	nechat	k5eAaPmAgMnP	nechat
Písečtí	písecký	k2eAgMnPc1d1	písecký
vystavět	vystavět	k5eAaPmF	vystavět
na	na	k7c6	na
Malém	malý	k2eAgNnSc6d1	malé
náměstí	náměstí	k1gNnSc6	náměstí
mariánské	mariánský	k2eAgNnSc4d1	Mariánské
sousoší	sousoší	k1gNnSc4	sousoší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Znovu	znovu	k6eAd1	znovu
byl	být	k5eAaImAgInS	být
Písek	Písek	k1gInSc1	Písek
postižen	postihnout	k5eAaPmNgInS	postihnout
válkou	válka	k1gFnSc7	válka
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1741	[number]	k4	1741
až	až	k9	až
1742	[number]	k4	1742
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
francouzská	francouzský	k2eAgFnSc1d1	francouzská
posádka	posádka	k1gFnSc1	posádka
<g/>
,	,	kIx,	,
vracející	vracející	k2eAgFnSc1d1	vracející
se	se	k3xPyFc4	se
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
které	který	k3yQgFnSc3	který
vyrazilo	vyrazit	k5eAaPmAgNnS	vyrazit
rakouské	rakouský	k2eAgNnSc1d1	rakouské
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Písečtí	písecký	k2eAgMnPc1d1	písecký
se	se	k3xPyFc4	se
báli	bát	k5eAaImAgMnP	bát
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
město	město	k1gNnSc1	město
opětovně	opětovně	k6eAd1	opětovně
dobyto	dobýt	k5eAaPmNgNnS	dobýt
<g/>
,	,	kIx,	,
vypáleno	vypálen	k2eAgNnSc1d1	vypáleno
a	a	k8xC	a
zničeno	zničen	k2eAgNnSc1d1	zničeno
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
slíbili	slíbit	k5eAaPmAgMnP	slíbit
Panně	Panna	k1gFnSc3	Panna
Marii	Maria	k1gFnSc3	Maria
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
bude	být	k5eAaImBp3nS	být
město	město	k1gNnSc1	město
ušetřeno	ušetřen	k2eAgNnSc1d1	ušetřeno
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
každoročně	každoročně	k6eAd1	každoročně
provozovat	provozovat	k5eAaImF	provozovat
městskou	městský	k2eAgFnSc4d1	městská
slavnost	slavnost	k1gFnSc4	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
konala	konat	k5eAaImAgFnS	konat
po	po	k7c4	po
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
století	století	k1gNnSc4	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
éře	éra	k1gFnSc6	éra
socialismu	socialismus	k1gInSc2	socialismus
byla	být	k5eAaImAgFnS	být
zakázána	zakázat	k5eAaPmNgFnS	zakázat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
obnovena	obnoven	k2eAgFnSc1d1	obnovena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1778	[number]	k4	1778
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
zřízeno	zřízen	k2eAgNnSc1d1	zřízeno
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
přibyla	přibýt	k5eAaPmAgFnS	přibýt
česká	český	k2eAgFnSc1d1	Česká
reálka	reálka	k1gFnSc1	reálka
a	a	k8xC	a
roku	rok	k1gInSc3	rok
1866	[number]	k4	1866
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
radnici	radnice	k1gFnSc6	radnice
zřízeno	zřízen	k2eAgNnSc1d1	zřízeno
české	český	k2eAgNnSc1d1	české
úřadování	úřadování	k1gNnSc1	úřadování
<g/>
.	.	kIx.	.
1861	[number]	k4	1861
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
první	první	k4xOgFnSc1	první
česká	český	k2eAgFnSc1d1	Česká
vyšší	vysoký	k2eAgFnSc1d2	vyšší
dívčí	dívčí	k2eAgFnSc1d1	dívčí
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
1870	[number]	k4	1870
první	první	k4xOgFnSc1	první
škola	škola	k1gFnSc1	škola
rolnická	rolnický	k2eAgFnSc1d1	rolnická
<g/>
,	,	kIx,	,
1884	[number]	k4	1884
škola	škola	k1gFnSc1	škola
revírnická	revírnický	k2eAgFnSc1d1	revírnický
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
první	první	k4xOgFnSc1	první
škola	škola	k1gFnSc1	škola
lesnická	lesnický	k2eAgFnSc1d1	lesnická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Národní	národní	k2eAgNnSc1d1	národní
obrození	obrození	k1gNnSc1	obrození
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
probíhalo	probíhat	k5eAaImAgNnS	probíhat
zásluhou	zásluhou	k7c2	zásluhou
vynikajících	vynikající	k2eAgFnPc2d1	vynikající
osobností	osobnost	k1gFnPc2	osobnost
velmi	velmi	k6eAd1	velmi
intenzivně	intenzivně	k6eAd1	intenzivně
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zde	zde	k6eAd1	zde
dokonce	dokonce	k9	dokonce
vydáván	vydáván	k2eAgInSc4d1	vydáván
časopis	časopis	k1gInSc4	časopis
Poutník	poutník	k1gMnSc1	poutník
od	od	k7c2	od
Otavy	Otava	k1gFnSc2	Otava
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pak	pak	k6eAd1	pak
nahradil	nahradit	k5eAaPmAgInS	nahradit
Otavan	Otavan	k1gInSc1	Otavan
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
povzbuzení	povzbuzení	k1gNnSc3	povzbuzení
národního	národní	k2eAgNnSc2d1	národní
vědomí	vědomí	k1gNnSc2	vědomí
přispělo	přispět	k5eAaPmAgNnS	přispět
i	i	k9	i
založení	založení	k1gNnSc4	založení
tělocvičné	tělocvičný	k2eAgFnSc2d1	Tělocvičná
jednoty	jednota	k1gFnSc2	jednota
Sokol	Sokol	k1gInSc4	Sokol
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
prošel	projít	k5eAaPmAgInS	projít
Písek	Písek	k1gInSc4	Písek
průmyslovým	průmyslový	k2eAgInSc7d1	průmyslový
rozvojem	rozvoj	k1gInSc7	rozvoj
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zde	zde	k6eAd1	zde
založena	založit	k5eAaPmNgFnS	založit
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
výroba	výroba	k1gFnSc1	výroba
fezů	fez	k1gInPc2	fez
<g/>
,	,	kIx,	,
papírna	papírna	k1gFnSc1	papírna
<g/>
,	,	kIx,	,
tabáková	tabákový	k2eAgFnSc1d1	tabáková
továrna	továrna	k1gFnSc1	továrna
a	a	k8xC	a
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
stavěly	stavět	k5eAaImAgFnP	stavět
komunikace	komunikace	k1gFnPc1	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1875	[number]	k4	1875
byl	být	k5eAaImAgInS	být
Písek	Písek	k1gInSc1	Písek
spojen	spojit	k5eAaPmNgInS	spojit
železnicí	železnice	k1gFnSc7	železnice
s	s	k7c7	s
Prahou	Praha	k1gFnSc7	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
městské	městský	k2eAgNnSc1d1	Městské
muzeum	muzeum	k1gNnSc1	muzeum
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
zavedl	zavést	k5eAaPmAgInS	zavést
Písek	Písek	k1gInSc1	Písek
jako	jako	k8xC	jako
třetí	třetí	k4xOgNnSc4	třetí
město	město	k1gNnSc4	město
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
elektrické	elektrický	k2eAgNnSc1d1	elektrické
osvětlení	osvětlení	k1gNnSc1	osvětlení
obloukovými	obloukový	k2eAgFnPc7d1	oblouková
lampami	lampa	k1gFnPc7	lampa
Františka	František	k1gMnSc2	František
Křižíka	Křižík	k1gMnSc2	Křižík
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
městská	městský	k2eAgFnSc1d1	městská
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
byl	být	k5eAaImAgInS	být
Písek	Písek	k1gInSc1	Písek
krajským	krajský	k2eAgNnSc7d1	krajské
městem	město	k1gNnSc7	město
Prácheňského	prácheňský	k2eAgInSc2d1	prácheňský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vinou	vina	k1gFnSc7	vina
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
od	od	k7c2	od
hlavní	hlavní	k2eAgFnSc2d1	hlavní
železniční	železniční	k2eAgFnSc2d1	železniční
tratě	trať	k1gFnSc2	trať
se	se	k3xPyFc4	se
nestal	stát	k5eNaPmAgInS	stát
významným	významný	k2eAgNnSc7d1	významné
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
se	se	k3xPyFc4	se
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gFnPc2	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
stal	stát	k5eAaPmAgMnS	stát
s	s	k7c7	s
přispěním	přispění	k1gNnSc7	přispění
píseckých	písecký	k2eAgMnPc2d1	písecký
voličů	volič	k1gMnPc2	volič
poslancem	poslanec	k1gMnSc7	poslanec
rakouské	rakouský	k2eAgFnSc2d1	rakouská
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
K	k	k7c3	k
vyhlášení	vyhlášení	k1gNnSc3	vyhlášení
samostatné	samostatný	k2eAgFnSc2d1	samostatná
republiky	republika	k1gFnSc2	republika
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
dalších	další	k2eAgNnPc6d1	další
městech	město	k1gNnPc6	město
již	již	k9	již
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
probíhala	probíhat	k5eAaImAgFnS	probíhat
generální	generální	k2eAgFnSc1d1	generální
stávka	stávka	k1gFnSc1	stávka
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
ale	ale	k8xC	ale
železničáři	železničář	k1gMnPc1	železničář
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
přivezli	přivézt	k5eAaPmAgMnP	přivézt
informaci	informace	k1gFnSc4	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
republika	republika	k1gFnSc1	republika
ještě	ještě	k6eAd1	ještě
nebyla	být	k5eNaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
Písečtí	písecký	k2eAgMnPc1d1	písecký
rozešli	rozejít	k5eAaPmAgMnP	rozejít
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
města	město	k1gNnSc2	město
byla	být	k5eAaImAgFnS	být
přemístěna	přemístit	k5eAaPmNgFnS	přemístit
uherská	uherský	k2eAgFnSc1d1	uherská
(	(	kIx(	(
<g/>
maďarská	maďarský	k2eAgFnSc1d1	maďarská
<g/>
)	)	kIx)	)
vojenská	vojenský	k2eAgFnSc1d1	vojenská
posádka	posádka	k1gFnSc1	posádka
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
pokusu	pokus	k1gInSc2	pokus
o	o	k7c4	o
převrat	převrat	k1gInSc4	převrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
trvání	trvání	k1gNnSc2	trvání
první	první	k4xOgFnSc2	první
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
navázal	navázat	k5eAaPmAgInS	navázat
Písek	Písek	k1gInSc1	Písek
na	na	k7c4	na
své	svůj	k3xOyFgFnPc4	svůj
předválečné	předválečný	k2eAgFnPc4d1	předválečná
tradice	tradice	k1gFnPc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Zůstal	zůstat	k5eAaPmAgInS	zůstat
vyhledávaným	vyhledávaný	k2eAgNnSc7d1	vyhledávané
místem	místo	k1gNnSc7	místo
pro	pro	k7c4	pro
studenty	student	k1gMnPc4	student
<g/>
,	,	kIx,	,
letní	letní	k2eAgMnPc4d1	letní
hosty	host	k1gMnPc4	host
i	i	k9	i
pro	pro	k7c4	pro
důchodce	důchodce	k1gMnPc4	důchodce
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zde	zde	k6eAd1	zde
hodlali	hodlat	k5eAaImAgMnP	hodlat
strávit	strávit	k5eAaPmF	strávit
klidné	klidný	k2eAgNnSc4d1	klidné
stáří	stáří	k1gNnSc4	stáří
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nacistická	nacistický	k2eAgFnSc1d1	nacistická
okupace	okupace	k1gFnSc1	okupace
a	a	k8xC	a
zřízení	zřízení	k1gNnSc1	zřízení
Protektorátu	protektorát	k1gInSc2	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
přinesly	přinést	k5eAaPmAgFnP	přinést
Písku	Písek	k1gInSc6	Písek
velké	velký	k2eAgNnSc4d1	velké
strádání	strádání	k1gNnSc4	strádání
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
na	na	k7c6	na
popravištích	popraviště	k1gNnPc6	popraviště
a	a	k8xC	a
v	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
<g/>
;	;	kIx,	;
někteří	některý	k3yIgMnPc1	některý
obyvatelé	obyvatel	k1gMnPc1	obyvatel
se	se	k3xPyFc4	se
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
domácího	domácí	k2eAgInSc2d1	domácí
i	i	k8xC	i
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
odboje	odboj	k1gInSc2	odboj
<g/>
.	.	kIx.	.
</s>
<s>
Okupaci	okupace	k1gFnSc4	okupace
ukončil	ukončit	k5eAaPmAgInS	ukončit
příchod	příchod	k1gInSc1	příchod
amerických	americký	k2eAgNnPc2d1	americké
vojsk	vojsko	k1gNnPc2	vojsko
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
večer	večer	k6eAd1	večer
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Sovětské	sovětský	k2eAgFnSc2d1	sovětská
jednotky	jednotka	k1gFnSc2	jednotka
dorazily	dorazit	k5eAaPmAgInP	dorazit
do	do	k7c2	do
města	město	k1gNnSc2	město
až	až	k6eAd1	až
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
po	po	k7c4	po
uchopení	uchopení	k1gNnSc4	uchopení
moci	moc	k1gFnSc2	moc
Komunistickou	komunistický	k2eAgFnSc7d1	komunistická
stranou	strana	k1gFnSc7	strana
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tvář	tvář	k1gFnSc1	tvář
Písku	Písek	k1gInSc2	Písek
začala	začít	k5eAaPmAgFnS	začít
měnit	měnit	k5eAaImF	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
byla	být	k5eAaImAgFnS	být
založeny	založen	k2eAgInPc4d1	založen
národní	národní	k2eAgInPc4d1	národní
podniky	podnik	k1gInPc4	podnik
jako	jako	k8xC	jako
textilní	textilní	k2eAgInSc1d1	textilní
závod	závod	k1gInSc1	závod
Jitex	Jitex	k1gInSc1	Jitex
<g/>
,	,	kIx,	,
Kovosvit	Kovosvit	k1gInSc1	Kovosvit
a	a	k8xC	a
Elektropřístroj	elektropřístroj	k1gInSc1	elektropřístroj
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
jatka	jatka	k1gFnSc1	jatka
<g/>
,	,	kIx,	,
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
se	se	k3xPyFc4	se
dřevařský	dřevařský	k2eAgInSc1d1	dřevařský
průmysl	průmysl	k1gInSc1	průmysl
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Začala	začít	k5eAaPmAgFnS	začít
také	také	k9	také
stavba	stavba	k1gFnSc1	stavba
nových	nový	k2eAgFnPc2d1	nová
budov	budova	k1gFnPc2	budova
a	a	k8xC	a
sídlišť	sídliště	k1gNnPc2	sídliště
(	(	kIx(	(
<g/>
Jih	jih	k1gInSc1	jih
a	a	k8xC	a
Dukla	Dukla	k1gFnSc1	Dukla
<g/>
,	,	kIx,	,
zástavba	zástavba	k1gFnSc1	zástavba
na	na	k7c4	na
Nábřeží	nábřeží	k1gNnSc4	nábřeží
1	[number]	k4	1
<g/>
.	.	kIx.	.
máje	máj	k1gInSc2	máj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tzv.	tzv.	kA	tzv.
socialistické	socialistický	k2eAgFnSc2d1	socialistická
éry	éra	k1gFnSc2	éra
byla	být	k5eAaImAgFnS	být
vztyčena	vztyčen	k2eAgFnSc1d1	vztyčena
socha	socha	k1gFnSc1	socha
Rýžování	rýžování	k1gNnSc2	rýžování
zlata	zlato	k1gNnSc2	zlato
na	na	k7c4	na
Nábřeží	nábřeží	k1gNnSc4	nábřeží
1	[number]	k4	1
.	.	kIx.	.
<g/>
máje	máj	k1gInSc2	máj
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
lávka	lávka	k1gFnSc1	lávka
spojující	spojující	k2eAgNnSc4d1	spojující
sídliště	sídliště	k1gNnSc4	sídliště
Dukla	Dukla	k1gFnSc1	Dukla
a	a	k8xC	a
zástavbu	zástavba	k1gFnSc4	zástavba
na	na	k7c4	na
nábřeží	nábřeží	k1gNnSc4	nábřeží
<g/>
,	,	kIx,	,
zkonstruovaná	zkonstruovaný	k2eAgFnSc1d1	zkonstruovaná
z	z	k7c2	z
betonových	betonový	k2eAgInPc2d1	betonový
panelů	panel	k1gInPc2	panel
a	a	k8xC	a
koncipovaná	koncipovaný	k2eAgFnSc1d1	koncipovaná
jako	jako	k8xC	jako
samonosná	samonosný	k2eAgFnSc1d1	samonosná
zavěšená	zavěšený	k2eAgFnSc1d1	zavěšená
lávka	lávka	k1gFnSc1	lávka
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Otavu	Otava	k1gFnSc4	Otava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
povodních	povodeň	k1gFnPc6	povodeň
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2002	[number]	k4	2002
byl	být	k5eAaImAgInS	být
značně	značně	k6eAd1	značně
poničen	poničen	k2eAgInSc1d1	poničen
kamenný	kamenný	k2eAgInSc1d1	kamenný
středověký	středověký	k2eAgInSc1d1	středověký
most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
ukotven	ukotvit	k5eAaPmNgInS	ukotvit
do	do	k7c2	do
skály	skála	k1gFnSc2	skála
pod	pod	k7c7	pod
řekou	řeka	k1gFnSc7	řeka
<g/>
,	,	kIx,	,
nápor	nápor	k1gInSc1	nápor
vody	voda	k1gFnSc2	voda
celkově	celkově	k6eAd1	celkově
vydržel	vydržet	k5eAaPmAgMnS	vydržet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nebyl	být	k5eNaImAgInS	být
zasažen	zasáhnout	k5eAaPmNgInS	zasáhnout
jenom	jenom	k9	jenom
Kamenný	kamenný	k2eAgInSc1d1	kamenný
most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
lávka	lávka	k1gFnSc1	lávka
u	u	k7c2	u
sídliště	sídliště	k1gNnSc2	sídliště
<g/>
:	:	kIx,	:
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
při	při	k7c6	při
povodních	povodeň	k1gFnPc6	povodeň
voda	voda	k1gFnSc1	voda
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
konstrukci	konstrukce	k1gFnSc3	konstrukce
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
proud	proud	k1gInSc1	proud
vody	voda	k1gFnSc2	voda
strhl	strhnout	k5eAaPmAgInS	strhnout
zábradlí	zábradlí	k1gNnSc4	zábradlí
a	a	k8xC	a
lávku	lávka	k1gFnSc4	lávka
nebezpečně	bezpečně	k6eNd1	bezpečně
překroutil	překroutit	k5eAaPmAgMnS	překroutit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
povodních	povodeň	k1gFnPc6	povodeň
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
postavena	postaven	k2eAgFnSc1d1	postavena
<g/>
,	,	kIx,	,
dostalo	dostat	k5eAaPmAgNnS	dostat
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c4	na
několik	několik	k4yIc4	několik
kosmetických	kosmetický	k2eAgFnPc2d1	kosmetická
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgNnSc2d1	dnešní
parkoviště	parkoviště	k1gNnSc2	parkoviště
na	na	k7c6	na
sídlišti	sídliště	k1gNnSc6	sídliště
Dukla	Dukla	k1gFnSc1	Dukla
stála	stát	k5eAaImAgFnS	stát
stará	starý	k2eAgFnSc1d1	stará
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
fezy	fez	k1gInPc4	fez
(	(	kIx(	(
<g/>
přezdívaná	přezdívaný	k2eAgFnSc1d1	přezdívaná
"	"	kIx"	"
<g/>
fezovka	fezovka	k1gFnSc1	fezovka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
kvůli	kvůli	k7c3	kvůli
porušené	porušený	k2eAgFnSc3d1	porušená
statice	statika	k1gFnSc3	statika
stržena	stržen	k2eAgFnSc1d1	stržena
<g/>
;	;	kIx,	;
před	před	k7c7	před
povodněmi	povodeň	k1gFnPc7	povodeň
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
míry	míra	k1gFnSc2	míra
nevyužívaná	využívaný	k2eNgFnSc1d1	nevyužívaná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
odhalen	odhalit	k5eAaPmNgInS	odhalit
památník	památník	k1gInSc1	památník
českým	český	k2eAgMnPc3d1	český
letcům	letec	k1gMnPc3	letec
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
rodáků	rodák	k1gMnPc2	rodák
z	z	k7c2	z
Písku	Písek	k1gInSc2	Písek
<g/>
,	,	kIx,	,
či	či	k8xC	či
z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
blízkého	blízký	k2eAgNnSc2d1	blízké
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Písek	Písek	k1gInSc1	Písek
má	mít	k5eAaImIp3nS	mít
dobrou	dobrý	k2eAgFnSc4d1	dobrá
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
vybudován	vybudován	k2eAgInSc4d1	vybudován
ucelený	ucelený	k2eAgInSc4d1	ucelený
kamerový	kamerový	k2eAgInSc4d1	kamerový
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
Písek	Písek	k1gInSc1	Písek
plánuje	plánovat	k5eAaImIp3nS	plánovat
přebudovat	přebudovat	k5eAaPmF	přebudovat
nábřeží	nábřeží	k1gNnSc4	nábřeží
Otavy	Otava	k1gFnSc2	Otava
<g/>
,	,	kIx,	,
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
mezi	mezi	k7c7	mezi
Starým	starý	k2eAgInSc7d1	starý
a	a	k8xC	a
Novým	nový	k2eAgInSc7d1	nový
mostem	most	k1gInSc7	most
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
kdysi	kdysi	k6eAd1	kdysi
bylo	být	k5eAaImAgNnS	být
zastavěno	zastavěn	k2eAgNnSc1d1	zastavěno
ulicí	ulice	k1gFnSc7	ulice
Fischergasse	Fischergasse	k1gFnSc2	Fischergasse
(	(	kIx(	(
<g/>
Rybářská	rybářský	k2eAgFnSc1d1	rybářská
ulice	ulice	k1gFnSc1	ulice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
rekreačně-kulturní	rekreačněulturní	k2eAgNnSc4d1	rekreačně-kulturní
centrum	centrum	k1gNnSc4	centrum
města	město	k1gNnSc2	město
pod	pod	k7c7	pod
širým	širý	k2eAgNnSc7d1	širé
nebem	nebe	k1gNnSc7	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
nábřeží	nábřeží	k1gNnSc4	nábřeží
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
naproti	naproti	k7c3	naproti
nově	nově	k6eAd1	nově
zastavěnému	zastavěný	k2eAgNnSc3d1	zastavěné
pravému	pravý	k2eAgNnSc3d1	pravé
nábřeží	nábřeží	k1gNnSc3	nábřeží
v	v	k7c6	v
Portyči	Portyč	k1gFnSc6	Portyč
(	(	kIx(	(
<g/>
název	název	k1gInSc1	název
lokality	lokalita	k1gFnSc2	lokalita
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
jí	on	k3xPp3gFnSc7	on
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgNnSc4d1	podobné
severoitalské	severoitalský	k2eAgNnSc4d1	severoitalské
město	město	k1gNnSc4	město
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
frekventované	frekventovaný	k2eAgNnSc1d1	frekventované
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepříliš	příliš	k6eNd1	příliš
upravené	upravený	k2eAgNnSc1d1	upravené
místo	místo	k1gNnSc1	místo
<g/>
.	.	kIx.	.
</s>
<s>
Radní	radní	k1gMnPc1	radní
města	město	k1gNnSc2	město
ale	ale	k8xC	ale
vítězný	vítězný	k2eAgInSc4d1	vítězný
návrh	návrh	k1gInSc4	návrh
komise	komise	k1gFnSc2	komise
nepodporují	podporovat	k5eNaImIp3nP	podporovat
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
pravděpodobný	pravděpodobný	k2eAgMnSc1d1	pravděpodobný
vítěz	vítěz	k1gMnSc1	vítěz
vzejde	vzejít	k5eAaPmIp3nS	vzejít
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
třech	tři	k4xCgInPc2	tři
míst	místo	k1gNnPc2	místo
ve	v	k7c6	v
veřejné	veřejný	k2eAgFnSc6d1	veřejná
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
bylo	být	k5eAaImAgNnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
Fügnerova	Fügnerův	k2eAgNnSc2d1	Fügnerovo
náměstí	náměstí	k1gNnSc2	náměstí
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
nákladu	náklad	k1gInSc2	náklad
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
finančně	finančně	k6eAd1	finančně
podpořila	podpořit	k5eAaPmAgFnS	podpořit
i	i	k9	i
firma	firma	k1gFnSc1	firma
AISIN	AISIN	kA	AISIN
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c6	v
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
zóně	zóna	k1gFnSc6	zóna
Písek-Sever	Písek-Sevra	k1gFnPc2	Písek-Sevra
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Písek	Písek	k1gInSc1	Písek
si	se	k3xPyFc3	se
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vede	vést	k5eAaImIp3nS	vést
poměrně	poměrně	k6eAd1	poměrně
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
udělených	udělený	k2eAgNnPc2d1	udělené
uznání	uznání	k1gNnPc2	uznání
byla	být	k5eAaImAgFnS	být
diamantová	diamantový	k2eAgFnSc1d1	Diamantová
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
podmínky	podmínka	k1gFnPc4	podmínka
k	k	k7c3	k
životu	život	k1gInSc3	život
v	v	k7c6	v
průzkumu	průzkum	k1gInSc6	průzkum
společnosti	společnost	k1gFnSc2	společnost
KPMG	KPMG	kA	KPMG
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
silné	silný	k2eAgFnSc6d1	silná
konkurenci	konkurence	k1gFnSc6	konkurence
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
středně	středně	k6eAd1	středně
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
konaných	konaný	k2eAgInPc2d1	konaný
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2014	[number]	k4	2014
se	s	k7c7	s
starostkou	starostka	k1gFnSc7	starostka
města	město	k1gNnSc2	město
stala	stát	k5eAaPmAgFnS	stát
Eva	Eva	k1gFnSc1	Eva
Vanžurová	Vanžurový	k2eAgFnSc1d1	Vanžurová
z	z	k7c2	z
uskupení	uskupení	k1gNnSc2	uskupení
Jihočeši	Jihočech	k1gMnPc1	Jihočech
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
postu	post	k1gInSc6	post
Ondřeje	Ondřej	k1gMnSc2	Ondřej
Veselého	Veselý	k1gMnSc2	Veselý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Židé	Žid	k1gMnPc1	Žid
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
===	===	k?	===
</s>
</p>
<p>
<s>
Židé	Žid	k1gMnPc1	Žid
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
od	od	k7c2	od
konce	konec	k1gInSc2	konec
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1424	[number]	k4	1424
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byli	být	k5eAaImAgMnP	být
vypovězeni	vypovědět	k5eAaPmNgMnP	vypovědět
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
začali	začít	k5eAaPmAgMnP	začít
usídlovat	usídlovat	k5eAaImF	usídlovat
až	až	k9	až
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc4d3	veliký
rozkvět	rozkvět	k1gInSc4	rozkvět
zažili	zažít	k5eAaPmAgMnP	zažít
po	po	k7c6	po
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
jich	on	k3xPp3gNnPc2	on
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
449	[number]	k4	449
a	a	k8xC	a
tvořili	tvořit	k5eAaImAgMnP	tvořit
tak	tak	k6eAd1	tak
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
městské	městský	k2eAgFnSc2d1	městská
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1872	[number]	k4	1872
postavili	postavit	k5eAaPmAgMnP	postavit
místo	místo	k1gNnSc4	místo
původní	původní	k2eAgFnSc2d1	původní
modlitebny	modlitebna	k1gFnSc2	modlitebna
<g/>
,	,	kIx,	,
doložené	doložený	k2eAgFnSc2d1	doložená
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
synagogu	synagoga	k1gFnSc4	synagoga
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dodnes	dodnes	k6eAd1	dodnes
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
Soukenické	soukenický	k2eAgFnSc6d1	Soukenická
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Synagoga	synagoga	k1gFnSc1	synagoga
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
pseudomaurském	pseudomaurský	k2eAgInSc6d1	pseudomaurský
stylu	styl	k1gInSc6	styl
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
1953	[number]	k4	1953
<g/>
–	–	k?	–
<g/>
1996	[number]	k4	1996
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
komunismu	komunismus	k1gInSc2	komunismus
budova	budova	k1gFnSc1	budova
chátrala	chátrat	k5eAaImAgFnS	chátrat
jako	jako	k9	jako
skladiště	skladiště	k1gNnSc4	skladiště
velkoobchodu	velkoobchod	k1gInSc2	velkoobchod
s	s	k7c7	s
textilem	textil	k1gInSc7	textil
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
navrácení	navrácení	k1gNnSc6	navrácení
Židovské	židovská	k1gFnPc4	židovská
obci	obec	k1gFnSc3	obec
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
postupně	postupně	k6eAd1	postupně
opravována	opravován	k2eAgFnSc1d1	opravována
<g/>
,	,	kIx,	,
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
poměrně	poměrně	k6eAd1	poměrně
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
ukončení	ukončení	k1gNnSc3	ukončení
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
také	také	k9	také
židovský	židovský	k2eAgInSc4d1	židovský
hřbitov	hřbitov	k1gInSc4	hřbitov
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1876	[number]	k4	1876
<g/>
,	,	kIx,	,
náhrobků	náhrobek	k1gInPc2	náhrobek
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
asi	asi	k9	asi
60	[number]	k4	60
<g/>
,	,	kIx,	,
budovy	budova	k1gFnPc1	budova
byly	být	k5eAaImAgFnP	být
zbořeny	zbořit	k5eAaPmNgFnP	zbořit
v	v	k7c6	v
letech	let	k1gInPc6	let
1968	[number]	k4	1968
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
starší	starý	k2eAgInSc1d2	starší
hřbitov	hřbitov	k1gInSc1	hřbitov
se	se	k3xPyFc4	se
nezachoval	zachovat	k5eNaPmAgInS	zachovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1921	[number]	k4	1921
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c4	v
1	[number]	k4	1
178	[number]	k4	178
domech	dům	k1gInPc6	dům
15	[number]	k4	15
303	[number]	k4	303
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
bylo	být	k5eAaImAgNnS	být
7	[number]	k4	7
809	[number]	k4	809
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
14	[number]	k4	14
914	[number]	k4	914
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
<g/>
,	,	kIx,	,
153	[number]	k4	153
k	k	k7c3	k
německé	německý	k2eAgFnSc3d1	německá
a	a	k8xC	a
7	[number]	k4	7
k	k	k7c3	k
židovské	židovská	k1gFnSc3	židovská
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
12	[number]	k4	12
922	[number]	k4	922
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
492	[number]	k4	492
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
322	[number]	k4	322
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
217	[number]	k4	217
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1930	[number]	k4	1930
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c4	v
1	[number]	k4	1
560	[number]	k4	560
domech	dům	k1gInPc6	dům
15	[number]	k4	15
783	[number]	k4	783
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
15	[number]	k4	15
208	[number]	k4	208
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
a	a	k8xC	a
157	[number]	k4	157
k	k	k7c3	k
německé	německý	k2eAgFnSc2d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
12	[number]	k4	12
178	[number]	k4	178
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
1	[number]	k4	1
097	[number]	k4	097
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
1	[number]	k4	1
137	[number]	k4	137
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
254	[number]	k4	254
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Členění	členění	k1gNnSc1	členění
města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
Písek	Písek	k1gInSc1	Písek
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
devíti	devět	k4xCc2	devět
částí	část	k1gFnPc2	část
na	na	k7c6	na
pěti	pět	k4xCc6	pět
katastrálních	katastrální	k2eAgNnPc6d1	katastrální
územích	území	k1gNnPc6	území
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Písek	Písek	k1gInSc1	Písek
<g/>
,	,	kIx,	,
části	část	k1gFnPc1	část
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Budějovické	budějovický	k2eAgNnSc1d1	Budějovické
Předměstí	předměstí	k1gNnSc1	předměstí
<g/>
,	,	kIx,	,
Pražské	pražský	k2eAgNnSc1d1	Pražské
Předměstí	předměstí	k1gNnSc1	předměstí
<g/>
,	,	kIx,	,
Václavské	václavský	k2eAgNnSc1d1	Václavské
Předměstí	předměstí	k1gNnSc1	předměstí
a	a	k8xC	a
Purkratice	Purkratice	k1gFnSc1	Purkratice
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Hradiště	Hradiště	k1gNnSc2	Hradiště
u	u	k7c2	u
Písku	Písek	k1gInSc2	Písek
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
Hradiště	Hradiště	k1gNnSc2	Hradiště
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Nový	nový	k2eAgInSc4d1	nový
Dvůr	Dvůr	k1gInSc4	Dvůr
u	u	k7c2	u
Písku	Písek	k1gInSc2	Písek
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
Nový	Nový	k1gMnSc1	Nový
Dvůr	Dvůr	k1gInSc1	Dvůr
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Semice	Semice	k1gFnSc2	Semice
u	u	k7c2	u
Písku	Písek	k1gInSc2	Písek
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
Semice	Semice	k1gFnSc1	Semice
</s>
</p>
<p>
<s>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Smrkovice	Smrkovice	k1gFnSc1	Smrkovice
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
SmrkoviceAž	SmrkoviceAž	k1gFnSc1	SmrkoviceAž
do	do	k7c2	do
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
města	město	k1gNnSc2	město
také	také	k9	také
osada	osada	k1gFnSc1	osada
Horní	horní	k2eAgFnSc1d1	horní
Novosedly	Novosedlo	k1gNnPc7	Novosedlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Současný	současný	k2eAgInSc1d1	současný
průmysl	průmysl	k1gInSc1	průmysl
===	===	k?	===
</s>
</p>
<p>
<s>
Písek	Písek	k1gInSc1	Písek
<g/>
,	,	kIx,	,
historicky	historicky	k6eAd1	historicky
ekonomicky	ekonomicky	k6eAd1	ekonomicky
velmi	velmi	k6eAd1	velmi
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bohatlo	bohatnout	k5eAaImAgNnS	bohatnout
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
na	na	k7c6	na
rýžování	rýžování	k1gNnSc6	rýžování
zlata	zlato	k1gNnSc2	zlato
z	z	k7c2	z
Otavy	Otava	k1gFnSc2	Otava
<g/>
,	,	kIx,	,
sběru	sběr	k1gInSc2	sběr
perel	perla	k1gFnPc2	perla
či	či	k8xC	či
obchodu	obchod	k1gInSc2	obchod
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
svědkem	svědek	k1gMnSc7	svědek
prudké	prudký	k2eAgFnSc2d1	prudká
industrializace	industrializace	k1gFnSc2	industrializace
jako	jako	k8xC	jako
mnohá	mnohý	k2eAgNnPc4d1	mnohé
okolní	okolní	k2eAgNnPc4d1	okolní
města	město	k1gNnPc4	město
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Strakonice	Strakonice	k1gFnPc1	Strakonice
či	či	k8xC	či
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
byl	být	k5eAaImAgInS	být
odklon	odklon	k1gInSc1	odklon
hlavní	hlavní	k2eAgFnSc2d1	hlavní
železnice	železnice	k1gFnSc2	železnice
<g/>
,	,	kIx,	,
Dráhy	dráha	k1gFnPc1	dráha
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
190	[number]	k4	190
<g/>
,	,	kIx,	,
do	do	k7c2	do
výhodnější	výhodný	k2eAgFnSc2d2	výhodnější
a	a	k8xC	a
přímější	přímý	k2eAgFnSc2d2	přímější
polohy	poloha	k1gFnSc2	poloha
–	–	k?	–
nicméně	nicméně	k8xC	nicméně
daleko	daleko	k6eAd1	daleko
mimo	mimo	k7c4	mimo
Písek	Písek	k1gInSc4	Písek
jižním	jižní	k2eAgInSc7d1	jižní
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Průmysl	průmysl	k1gInSc1	průmysl
ve	v	k7c6	v
městě	město	k1gNnSc6	město
existoval	existovat	k5eAaImAgInS	existovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedosáhl	dosáhnout	k5eNaPmAgMnS	dosáhnout
většího	veliký	k2eAgInSc2d2	veliký
významu	význam	k1gInSc2	význam
a	a	k8xC	a
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
vyústil	vyústit	k5eAaPmAgInS	vyústit
ve	v	k7c6	v
zbrzdění	zbrzdění	k1gNnSc6	zbrzdění
rozvoje	rozvoj	k1gInSc2	rozvoj
krajského	krajský	k2eAgNnSc2d1	krajské
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
významné	významný	k2eAgNnSc1d1	významné
spíše	spíše	k9	spíše
jako	jako	k8xS	jako
centrum	centrum	k1gNnSc1	centrum
vzdělání	vzdělání	k1gNnSc2	vzdělání
a	a	k8xC	a
administrativy	administrativa	k1gFnSc2	administrativa
<g/>
.	.	kIx.	.
</s>
<s>
Vystěhovalectví	vystěhovalectví	k1gNnSc1	vystěhovalectví
nezaměstnaných	nezaměstnaný	k1gMnPc2	nezaměstnaný
do	do	k7c2	do
průmyslovějších	průmyslový	k2eAgNnPc2d2	průmyslovější
sídel	sídlo	k1gNnPc2	sídlo
<g/>
,	,	kIx,	,
i	i	k9	i
za	za	k7c4	za
moře	moře	k1gNnSc4	moře
do	do	k7c2	do
USA	USA	kA	USA
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
osad	osada	k1gFnPc2	osada
Pisek	Pisko	k1gNnPc2	Pisko
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Dakotě	Dakota	k1gFnSc6	Dakota
a	a	k8xC	a
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgNnPc4d1	východní
úbočí	úbočí	k1gNnPc4	úbočí
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
příměstská	příměstský	k2eAgFnSc1d1	příměstská
rekreační	rekreační	k2eAgFnSc1d1	rekreační
zóna	zóna	k1gFnSc1	zóna
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
připomíná	připomínat	k5eAaImIp3nS	připomínat
pokus	pokus	k1gInSc1	pokus
města	město	k1gNnSc2	město
o	o	k7c4	o
zamezení	zamezení	k1gNnSc4	zamezení
vystěhovalectví	vystěhovalectví	k1gNnSc2	vystěhovalectví
nabídnutím	nabídnutí	k1gNnSc7	nabídnutí
využívání	využívání	k1gNnSc2	využívání
půdy	půda	k1gFnSc2	půda
chudším	chudý	k2eAgFnPc3d2	chudší
vrstvám	vrstva	k1gFnPc3	vrstva
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Nižší	nízký	k2eAgFnSc1d2	nižší
industrializace	industrializace	k1gFnSc1	industrializace
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
dopad	dopad	k1gInSc4	dopad
v	v	k7c6	v
pozdějším	pozdní	k2eAgInSc6d2	pozdější
rozvoji	rozvoj	k1gInSc6	rozvoj
turistického	turistický	k2eAgInSc2d1	turistický
ruchu	ruch	k1gInSc2	ruch
v	v	k7c6	v
málo	málo	k6eAd1	málo
dotčené	dotčený	k2eAgFnSc6d1	dotčená
krajině	krajina	k1gFnSc6	krajina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průmysl	průmysl	k1gInSc1	průmysl
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
významu	význam	k1gInSc3	význam
až	až	k9	až
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
řada	řada	k1gFnSc1	řada
potravinářských	potravinářský	k2eAgMnPc2d1	potravinářský
<g/>
,	,	kIx,	,
textilních	textilní	k2eAgInPc2d1	textilní
<g/>
,	,	kIx,	,
elektrotechnických	elektrotechnický	k2eAgInPc2d1	elektrotechnický
koncernů	koncern	k1gInPc2	koncern
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Jitex	Jitex	k1gInSc1	Jitex
či	či	k8xC	či
Kovosvit	Kovosvit	k1gInSc1	Kovosvit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
Písek	Písek	k1gInSc1	Písek
zažívá	zažívat	k5eAaImIp3nS	zažívat
pozvolný	pozvolný	k2eAgInSc4d1	pozvolný
rozvoj	rozvoj	k1gInSc4	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
struktura	struktura	k1gFnSc1	struktura
se	se	k3xPyFc4	se
v	v	k7c6	v
porevolučních	porevoluční	k2eAgNnPc6d1	porevoluční
letech	léto	k1gNnPc6	léto
změnila	změnit	k5eAaPmAgFnS	změnit
více	hodně	k6eAd2	hodně
k	k	k7c3	k
elektrotechnickému	elektrotechnický	k2eAgNnSc3d1	elektrotechnické
a	a	k8xC	a
strojnímu	strojní	k2eAgNnSc3d1	strojní
–	–	k?	–
automotive	automotiv	k1gInSc5	automotiv
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
vliv	vliv	k1gInSc1	vliv
předrevolučních	předrevoluční	k2eAgInPc2d1	předrevoluční
velkopodniků	velkopodnik	k1gInPc2	velkopodnik
utlumil	utlumit	k5eAaPmAgInS	utlumit
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
mnohem	mnohem	k6eAd1	mnohem
nižšího	nízký	k2eAgInSc2d2	nižší
podílu	podíl	k1gInSc2	podíl
v	v	k7c6	v
ekonomice	ekonomika	k1gFnSc6	ekonomika
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Čížovou	Čížová	k1gFnSc4	Čížová
je	být	k5eAaImIp3nS	být
stavěna	stavit	k5eAaImNgFnS	stavit
od	od	k7c2	od
konce	konec	k1gInSc2	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
rozlehlá	rozlehlý	k2eAgFnSc1d1	rozlehlá
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
zóna	zóna	k1gFnSc1	zóna
Písek-Sever	Písek-Sever	k1gInSc1	Písek-Sever
<g/>
,	,	kIx,	,
využívaná	využívaný	k2eAgFnSc1d1	využívaná
rostoucím	rostoucí	k2eAgNnSc7d1	rostoucí
množstvím	množství	k1gNnSc7	množství
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zóna	zóna	k1gFnSc1	zóna
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
80	[number]	k4	80
ha	ha	kA	ha
největší	veliký	k2eAgFnSc1d3	veliký
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
polovina	polovina	k1gFnSc1	polovina
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
volná	volný	k2eAgFnSc1d1	volná
i	i	k9	i
přes	přes	k7c4	přes
velkou	velký	k2eAgFnSc4d1	velká
podporu	podpora	k1gFnSc4	podpora
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgFnSc1d1	zdejší
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
zóna	zóna	k1gFnSc1	zóna
je	být	k5eAaImIp3nS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
převážně	převážně	k6eAd1	převážně
na	na	k7c4	na
strojírenský	strojírenský	k2eAgInSc4d1	strojírenský
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
logistiku	logistika	k1gFnSc4	logistika
a	a	k8xC	a
sofistikovaný	sofistikovaný	k2eAgInSc4d1	sofistikovaný
elektro-průmysl	elektrorůmysl	k1gInSc4	elektro-průmysl
<g/>
;	;	kIx,	;
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgInPc4d3	veliký
zdejší	zdejší	k2eAgInPc4d1	zdejší
nové	nový	k2eAgInPc4d1	nový
závody	závod	k1gInPc4	závod
lze	lze	k6eAd1	lze
zahrnout	zahrnout	k5eAaPmF	zahrnout
firmu	firma	k1gFnSc4	firma
Faurecia	Faurecium	k1gNnSc2	Faurecium
Components	Components	k1gInSc1	Components
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
(	(	kIx(	(
<g/>
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
autosoučástky	autosoučástka	k1gFnPc4	autosoučástka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
sedačky	sedačka	k1gFnSc2	sedačka
či	či	k8xC	či
výfuky	výfuk	k1gInPc4	výfuk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
AISIN	AISIN	kA	AISIN
Europe	Europ	k1gInSc5	Europ
Manufacturing	Manufacturing	k1gInSc1	Manufacturing
Czech	Czech	k1gMnSc1	Czech
<g />
.	.	kIx.	.
</s>
<s>
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
(	(	kIx(	(
<g/>
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
součástky	součástka	k1gFnPc4	součástka
a	a	k8xC	a
nástroje	nástroj	k1gInPc4	nástroj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
S.	S.	kA	S.
<g/>
n.	n.	k?	n.
<g/>
o.	o.	k?	o.
<g/>
p.	p.	k?	p.
cz	cz	k?	cz
a.	a.	k?	a.
s.	s.	k?	s.
Písek	Písek	k1gInSc1	Písek
<g/>
,	,	kIx,	,
Amtek	Amtek	k1gInSc1	Amtek
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
,	,	kIx,	,
Heyco	Heyco	k1gMnSc1	Heyco
Werk	Werk	k1gMnSc1	Werk
<g/>
,	,	kIx,	,
KUNSTSTOFF	KUNSTSTOFF	kA	KUNSTSTOFF
FRÖHLICH	FRÖHLICH	kA	FRÖHLICH
GmbH	GmbH	k1gMnSc1	GmbH
o.s.	o.s.	k?	o.s.
CZ	CZ	kA	CZ
<g/>
,	,	kIx,	,
I.	I.	kA	I.
<g/>
n.	n.	k?	n.
<g/>
p.	p.	k?	p.
<g />
.	.	kIx.	.
</s>
<s>
Písek	Písek	k1gInSc1	Písek
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
SMOZ	SMOZ	kA	SMOZ
IMMO	IMMO	kA	IMMO
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
(	(	kIx(	(
<g/>
kovovýroba	kovovýroba	k1gFnSc1	kovovýroba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
CSS	CSS	kA	CSS
SPEDITION	SPEDITION	kA	SPEDITION
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
BROTEX	BROTEX	kA	BROTEX
Z	z	k7c2	z
<g/>
&	&	k?	&
<g/>
J	J	kA	J
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
(	(	kIx(	(
<g/>
textilní	textilní	k2eAgInSc1d1	textilní
průmysl	průmysl	k1gInSc1	průmysl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ČESKÉ	český	k2eAgFnSc2d1	Česká
A	a	k8xC	a
MORAVSKÉ	moravský	k2eAgFnSc2d1	Moravská
OBALOVNY	obalovna	k1gFnSc2	obalovna
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
STRABAG	STRABAG	kA	STRABAG
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
a	a	k8xC	a
Grosshauser	Grosshauser	k1gMnSc1	Grosshauser
Invest	Invest	k1gMnSc1	Invest
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
zóny	zóna	k1gFnSc2	zóna
stále	stále	k6eAd1	stále
funguje	fungovat	k5eAaImIp3nS	fungovat
původní	původní	k2eAgFnSc1d1	původní
zástavba	zástavba	k1gFnSc1	zástavba
školního	školní	k2eAgInSc2d1	školní
statku	statek	k1gInSc2	statek
Střední	střední	k2eAgFnSc2d1	střední
Zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
níž	jenž	k3xRgFnSc2	jenž
funguje	fungovat	k5eAaImIp3nS	fungovat
ubytovna	ubytovna	k1gFnSc1	ubytovna
pro	pro	k7c4	pro
dělníky	dělník	k1gMnPc4	dělník
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zde	zde	k6eAd1	zde
bioplynová	bioplynový	k2eAgFnSc1d1	bioplynová
stanice	stanice	k1gFnSc1	stanice
Bioenergy	Bioenerg	k1gInPc1	Bioenerg
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
</s>
</p>
<p>
<s>
Nedávné	dávný	k2eNgNnSc4d1	nedávné
rozšíření	rozšíření	k1gNnSc4	rozšíření
přinesla	přinést	k5eAaPmAgFnS	přinést
do	do	k7c2	do
zóny	zóna	k1gFnSc2	zóna
italskou	italský	k2eAgFnSc4d1	italská
firmu	firma	k1gFnSc4	firma
Lovato	Lovat	k2eAgNnSc1d1	Lovato
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
a	a	k8xC	a
jejích	její	k3xOp3gNnPc2	její
asi	asi	k9	asi
sto	sto	k4xCgNnSc1	sto
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
se	se	k3xPyFc4	se
soustředí	soustředit	k5eAaPmIp3nS	soustředit
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
komponentů	komponent	k1gInPc2	komponent
a	a	k8xC	a
silnoproudých	silnoproudý	k2eAgInPc2d1	silnoproudý
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nových	nový	k2eAgFnPc2d1	nová
investic	investice	k1gFnPc2	investice
je	být	k5eAaImIp3nS	být
vývojová	vývojový	k2eAgFnSc1d1	vývojová
laboratoř	laboratoř	k1gFnSc1	laboratoř
a	a	k8xC	a
výrobní	výrobní	k2eAgInSc1d1	výrobní
závod	závod	k1gInSc1	závod
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
na	na	k7c4	na
LED	led	k1gInSc4	led
osvětlení	osvětlení	k1gNnSc2	osvětlení
firmy	firma	k1gFnSc2	firma
Industrial	Industrial	k1gMnSc1	Industrial
Light	Light	k1gMnSc1	Light
&	&	k?	&
Control	Control	k1gInSc1	Control
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
plánuje	plánovat	k5eAaImIp3nS	plánovat
zaměstnat	zaměstnat	k5eAaPmF	zaměstnat
70	[number]	k4	70
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
a	a	k8xC	a
140	[number]	k4	140
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
fázi	fáze	k1gFnSc6	fáze
investice	investice	k1gFnSc1	investice
<g/>
;	;	kIx,	;
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2013	[number]	k4	2013
oznámila	oznámit	k5eAaPmAgFnS	oznámit
firma	firma	k1gFnSc1	firma
JIRI	JIRI	kA	JIRI
MODELS	MODELS	kA	MODELS
úmysl	úmysl	k1gInSc1	úmysl
investovat	investovat	k5eAaBmF	investovat
zde	zde	k6eAd1	zde
celkem	celek	k1gInSc7	celek
120	[number]	k4	120
milionů	milion	k4xCgInPc2	milion
a	a	k8xC	a
zaměstnat	zaměstnat	k5eAaPmF	zaměstnat
celkem	celkem	k6eAd1	celkem
až	až	k9	až
70	[number]	k4	70
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
domluvy	domluva	k1gFnSc2	domluva
s	s	k7c7	s
městem	město	k1gNnSc7	město
Písek	Písek	k1gInSc1	Písek
ohledně	ohledně	k7c2	ohledně
cen	cena	k1gFnPc2	cena
pozemků	pozemek	k1gInPc2	pozemek
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
ovšem	ovšem	k9	ovšem
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
prodat	prodat	k5eAaPmF	prodat
pozemky	pozemek	k1gInPc4	pozemek
v	v	k7c6	v
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
zóně	zóna	k1gFnSc6	zóna
Písek-Sever	Písek-Sever	k1gInSc4	Písek-Sever
pod	pod	k7c7	pod
cenou	cena	k1gFnSc7	cena
<g/>
,	,	kIx,	,
budoucnost	budoucnost	k1gFnSc1	budoucnost
investice	investice	k1gFnSc1	investice
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nejistá	jistý	k2eNgFnSc1d1	nejistá
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
nabídlo	nabídnout	k5eAaPmAgNnS	nabídnout
levnější	levný	k2eAgInPc4d2	levnější
pozemky	pozemek	k1gInPc4	pozemek
poblíž	poblíž	k7c2	poblíž
lokality	lokalita	k1gFnSc2	lokalita
a	a	k8xC	a
jednání	jednání	k1gNnSc1	jednání
patrně	patrně	k6eAd1	patrně
neskončilo	skončit	k5eNaPmAgNnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Dceřiná	dceřiný	k2eAgFnSc1d1	dceřiná
firma	firma	k1gFnSc1	firma
AISIN	AISIN	kA	AISIN
<g/>
,	,	kIx,	,
ADVICS	ADVICS	kA	ADVICS
Europe	Europ	k1gMnSc5	Europ
GmbH	GmbH	k1gMnSc5	GmbH
<g/>
,	,	kIx,	,
vyrábějící	vyrábějící	k2eAgInPc1d1	vyrábějící
brzdové	brzdový	k2eAgInPc1d1	brzdový
systémy	systém	k1gInPc1	systém
pro	pro	k7c4	pro
automotive	automotiv	k1gInSc5	automotiv
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
využije	využít	k5eAaPmIp3nS	využít
část	část	k1gFnSc1	část
areálu	areál	k1gInSc2	areál
mateřské	mateřský	k2eAgFnSc2d1	mateřská
firmy	firma	k1gFnSc2	firma
a	a	k8xC	a
zaměstná	zaměstnat	k5eAaPmIp3nS	zaměstnat
celkem	celkem	k6eAd1	celkem
36	[number]	k4	36
lidí	člověk	k1gMnPc2	člověk
od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Hala	hala	k1gFnSc1	hala
pro	pro	k7c4	pro
firmu	firma	k1gFnSc4	firma
ADVICS	ADVICS	kA	ADVICS
a	a	k8xC	a
logistické	logistický	k2eAgNnSc1d1	logistické
centrum	centrum	k1gNnSc1	centrum
firmy	firma	k1gFnSc2	firma
AEM-C	AEM-C	k1gFnSc2	AEM-C
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
zaměstná	zaměstnat	k5eAaPmIp3nS	zaměstnat
dalších	další	k2eAgMnPc2d1	další
25	[number]	k4	25
lidí	člověk	k1gMnPc2	člověk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stavěna	stavit	k5eAaImNgFnS	stavit
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
začala	začít	k5eAaPmAgFnS	začít
jistá	jistý	k2eAgFnSc1d1	jistá
firma	firma	k1gFnSc1	firma
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
recyklací	recyklace	k1gFnSc7	recyklace
hliníku	hliník	k1gInSc2	hliník
sondovat	sondovat	k5eAaImF	sondovat
možnost	možnost	k1gFnSc4	možnost
svého	svůj	k3xOyFgNnSc2	svůj
přesídlení	přesídlení	k1gNnSc2	přesídlení
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
zóny	zóna	k1gFnSc2	zóna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celkově	celkově	k6eAd1	celkově
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zaměstnáno	zaměstnat	k5eAaPmNgNnS	zaměstnat
přibližně	přibližně	k6eAd1	přibližně
3,300	[number]	k4	3,300
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
<g/>
Pomocí	pomocí	k7c2	pomocí
dotace	dotace	k1gFnSc2	dotace
od	od	k7c2	od
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
zóny	zóna	k1gFnSc2	zóna
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
cyklostezka	cyklostezka	k1gFnSc1	cyklostezka
<g/>
,	,	kIx,	,
do	do	k7c2	do
zóny	zóna	k1gFnSc2	zóna
zajíždí	zajíždět	k5eAaImIp3nP	zajíždět
pravidelné	pravidelný	k2eAgInPc1d1	pravidelný
spoje	spoj	k1gInPc1	spoj
městské	městský	k2eAgFnSc2d1	městská
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
zde	zde	k6eAd1	zde
funguje	fungovat	k5eAaImIp3nS	fungovat
železniční	železniční	k2eAgFnSc1d1	železniční
zastávka	zastávka	k1gFnSc1	zastávka
Písek-Dobešice	Písek-Dobešice	k1gFnSc2	Písek-Dobešice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
areálu	areál	k1gInSc6	areál
bývalých	bývalý	k2eAgFnPc2d1	bývalá
kasáren	kasárny	k1gFnPc2	kasárny
sousedících	sousedící	k2eAgFnPc2d1	sousedící
se	s	k7c7	s
zónou	zóna	k1gFnSc7	zóna
sídlí	sídlet	k5eAaImIp3nS	sídlet
firma	firma	k1gFnSc1	firma
BaK	bak	k0	bak
<g/>
.	.	kIx.	.
</s>
<s>
Tradičními	tradiční	k2eAgInPc7d1	tradiční
píseckými	písecký	k2eAgInPc7d1	písecký
podniky	podnik	k1gInPc7	podnik
jsou	být	k5eAaImIp3nP	být
Schneider	Schneider	k1gMnSc1	Schneider
Electric	Electric	k1gMnSc1	Electric
<g/>
,	,	kIx,	,
MASO	maso	k1gNnSc1	maso
UZENINY	uzenina	k1gFnSc2	uzenina
Písek	Písek	k1gInSc1	Písek
a	a	k8xC	a
firma	firma	k1gFnSc1	firma
Agpi	Agp	k1gFnSc2	Agp
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnPc1d1	zabývající
se	se	k3xPyFc4	se
velkovýrobou	velkovýroba	k1gFnSc7	velkovýroba
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
známou	známý	k2eAgFnSc7d1	známá
textilní	textilní	k2eAgFnSc7d1	textilní
firmou	firma	k1gFnSc7	firma
je	být	k5eAaImIp3nS	být
Jitex	Jitex	k1gInSc1	Jitex
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
areál	areál	k1gInSc1	areál
Jitexu	Jitex	k1gInSc2	Jitex
zhruba	zhruba	k6eAd1	zhruba
padesátka	padesátka	k1gFnSc1	padesátka
firem	firma	k1gFnPc2	firma
a	a	k8xC	a
živnostníků	živnostník	k1gMnPc2	živnostník
rozmanitého	rozmanitý	k2eAgNnSc2d1	rozmanité
zaměření	zaměření	k1gNnSc2	zaměření
na	na	k7c6	na
8	[number]	k4	8
hektarech	hektar	k1gInPc6	hektar
plochy	plocha	k1gFnSc2	plocha
a	a	k8xC	a
stále	stále	k6eAd1	stále
nabízí	nabízet	k5eAaImIp3nS	nabízet
volné	volný	k2eAgFnPc4d1	volná
plochy	plocha	k1gFnPc4	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
městem	město	k1gNnSc7	město
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Smrkovice	Smrkovice	k1gFnPc4	Smrkovice
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
teplárna	teplárna	k1gFnSc1	teplárna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tradičním	tradiční	k2eAgInSc7d1	tradiční
lze	lze	k6eAd1	lze
nazvat	nazvat	k5eAaPmF	nazvat
zpracování	zpracování	k1gNnSc3	zpracování
dřeva	dřevo	k1gNnSc2	dřevo
z	z	k7c2	z
městských	městský	k2eAgInPc2d1	městský
lesů	les	k1gInPc2	les
či	či	k8xC	či
kamene	kámen	k1gInSc2	kámen
těženého	těžený	k2eAgInSc2d1	těžený
v	v	k7c6	v
blízkém	blízký	k2eAgNnSc6d1	blízké
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Tábor	Tábor	k1gInSc4	Tábor
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
kamenolom	kamenolom	k1gInSc1	kamenolom
Na	na	k7c6	na
Ptáčkovně	Ptáčkovna	k1gFnSc6	Ptáčkovna
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
produkuje	produkovat	k5eAaImIp3nS	produkovat
drcený	drcený	k2eAgInSc1d1	drcený
stavební	stavební	k2eAgInSc1d1	stavební
kámen	kámen	k1gInSc1	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
zpracování	zpracování	k1gNnSc1	zpracování
probíhá	probíhat	k5eAaImIp3nS	probíhat
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
lomu	lom	k1gInSc2	lom
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
lomu	lom	k1gInSc2	lom
je	být	k5eAaImIp3nS	být
také	také	k9	také
závod	závod	k1gInSc4	závod
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
dekoračního	dekorační	k2eAgInSc2d1	dekorační
kamene	kámen	k1gInSc2	kámen
Jihokámen	Jihokámen	k1gInSc1	Jihokámen
<g/>
,	,	kIx,	,
v.d.	v.d.	k?	v.d.
<g/>
;	;	kIx,	;
ve	v	k7c6	v
městě	město	k1gNnSc6	město
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
kámen	kámen	k1gInSc1	kámen
i	i	k8xC	i
UNIGRANIT	UNIGRANIT	kA	UNIGRANIT
Písek	Písek	k1gInSc1	Písek
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
Písek	Písek	k1gInSc1	Písek
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
výskytem	výskyt	k1gInSc7	výskyt
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
Pisekitu	Pisekit	k1gInSc2	Pisekit
<g/>
,	,	kIx,	,
nerostu	nerost	k1gInSc2	nerost
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Blízké	blízký	k2eAgFnPc1d1	blízká
Písecké	písecký	k2eAgFnPc1d1	Písecká
hory	hora	k1gFnPc1	hora
mají	mít	k5eAaImIp3nP	mít
několik	několik	k4yIc4	několik
lokalit	lokalita	k1gFnPc2	lokalita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
těženo	těžen	k2eAgNnSc4d1	těženo
zlato	zlato	k1gNnSc4	zlato
a	a	k8xC	a
drahokamy	drahokam	k1gInPc4	drahokam
v	v	k7c6	v
průmyslovém	průmyslový	k2eAgNnSc6d1	průmyslové
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Cestovní	cestovní	k2eAgInSc1d1	cestovní
ruch	ruch	k1gInSc1	ruch
===	===	k?	===
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
též	též	k9	též
snaží	snažit	k5eAaImIp3nS	snažit
orientovat	orientovat	k5eAaBmF	orientovat
na	na	k7c4	na
turistický	turistický	k2eAgInSc4d1	turistický
ruch	ruch	k1gInSc4	ruch
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
za	za	k7c2	za
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2	Rakousko-Uhersk
a	a	k8xC	a
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
Písek	Písek	k1gInSc1	Písek
znám	znám	k2eAgInSc1d1	znám
jako	jako	k8xC	jako
lázeňské	lázeňský	k2eAgNnSc1d1	lázeňské
centrum	centrum	k1gNnSc1	centrum
v	v	k7c6	v
Prácheňském	prácheňský	k2eAgInSc6d1	prácheňský
kraji	kraj	k1gInSc6	kraj
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
zdravé	zdravá	k1gFnPc4	zdravá
ovzduší	ovzduší	k1gNnSc2	ovzduší
ovlivněné	ovlivněný	k2eAgFnPc4d1	ovlivněná
okolními	okolní	k2eAgInPc7d1	okolní
lesy	les	k1gInPc7	les
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Turisté	turist	k1gMnPc1	turist
přijíždějí	přijíždět	k5eAaImIp3nP	přijíždět
na	na	k7c6	na
folklorní	folklorní	k2eAgFnSc6d1	folklorní
<g/>
,	,	kIx,	,
městské	městský	k2eAgFnSc6d1	městská
a	a	k8xC	a
hudební	hudební	k2eAgFnSc6d1	hudební
slavnosti	slavnost	k1gFnSc6	slavnost
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jsou	být	k5eAaImIp3nP	být
přitahováni	přitahován	k2eAgMnPc1d1	přitahován
památkově	památkově	k6eAd1	památkově
chráněným	chráněný	k2eAgNnSc7d1	chráněné
středověkým	středověký	k2eAgNnSc7d1	středověké
jádrem	jádro	k1gNnSc7	jádro
města	město	k1gNnSc2	město
včetně	včetně	k7c2	včetně
nejstaršího	starý	k2eAgInSc2d3	nejstarší
českého	český	k2eAgInSc2d1	český
mostu	most	k1gInSc2	most
či	či	k8xC	či
prohlídky	prohlídka	k1gFnSc2	prohlídka
věže	věž	k1gFnSc2	věž
Děkanského	děkanský	k2eAgInSc2d1	děkanský
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
novověkých	novověký	k2eAgFnPc2d1	novověká
zajímavostí	zajímavost	k1gFnPc2	zajímavost
návštěvníky	návštěvník	k1gMnPc4	návštěvník
láká	lákat	k5eAaImIp3nS	lákat
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
elektráren	elektrárna	k1gFnPc2	elektrárna
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
zprovozněná	zprovozněný	k2eAgFnSc1d1	zprovozněná
r.	r.	kA	r.
1887	[number]	k4	1887
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Otavě	Otava	k1gFnSc6	Otava
<g/>
,	,	kIx,	,
Městská	městský	k2eAgFnSc1d1	městská
elektrárna	elektrárna	k1gFnSc1	elektrárna
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgInSc1d3	nejstarší
funkční	funkční	k2eAgFnSc7d1	funkční
vodní	vodní	k2eAgFnSc7d1	vodní
elektrárnou	elektrárna	k1gFnSc7	elektrárna
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
před	před	k7c7	před
náročnou	náročný	k2eAgFnSc7d1	náročná
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
fungovala	fungovat	k5eAaImAgFnS	fungovat
po	po	k7c6	po
99	[number]	k4	99
let	léto	k1gNnPc2	léto
až	až	k9	až
do	do	k7c2	do
r.	r.	kA	r.
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
magnetem	magnet	k1gInSc7	magnet
je	být	k5eAaImIp3nS	být
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
galerie-centrum	galerieentrum	k1gNnSc4	galerie-centrum
dětské	dětský	k2eAgFnSc2d1	dětská
kresby	kresba	k1gFnSc2	kresba
s	s	k7c7	s
workshopy	workshop	k1gInPc7	workshop
<g/>
,	,	kIx,	,
expozicemi	expozice	k1gFnPc7	expozice
a	a	k8xC	a
výstavami	výstava	k1gFnPc7	výstava
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
v	v	k7c6	v
objektu	objekt	k1gInSc6	objekt
sladovny	sladovna	k1gFnSc2	sladovna
z	z	k7c2	z
r.	r.	kA	r.
1864	[number]	k4	1864
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
objektu	objekt	k1gInSc2	objekt
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
infocentrum	infocentrum	k1gNnSc1	infocentrum
s	s	k7c7	s
celoročním	celoroční	k2eAgInSc7d1	celoroční
provozem	provoz	k1gInSc7	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
propaguje	propagovat	k5eAaImIp3nS	propagovat
též	též	k9	též
770	[number]	k4	770
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
první	první	k4xOgFnSc2	první
písemné	písemný	k2eAgFnSc2d1	písemná
zmínky	zmínka	k1gFnSc2	zmínka
<g/>
,	,	kIx,	,
připadající	připadající	k2eAgInSc4d1	připadající
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
přitáhnout	přitáhnout	k5eAaPmF	přitáhnout
více	hodně	k6eAd2	hodně
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
i	i	k8xC	i
okolím	okolí	k1gNnSc7	okolí
vede	vést	k5eAaImIp3nS	vést
hustá	hustý	k2eAgFnSc1d1	hustá
síť	síť	k1gFnSc1	síť
cyklostezek	cyklostezka	k1gFnPc2	cyklostezka
a	a	k8xC	a
tras	trasa	k1gFnPc2	trasa
pro	pro	k7c4	pro
pěší	pěší	k2eAgFnSc4d1	pěší
turistiku	turistika	k1gFnSc4	turistika
<g/>
,	,	kIx,	,
končí	končit	k5eAaImIp3nS	končit
zde	zde	k6eAd1	zde
vodácká	vodácký	k2eAgFnSc1d1	vodácká
trasa	trasa	k1gFnSc1	trasa
po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
Otavě	Otava	k1gFnSc6	Otava
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
účelu	účel	k1gInSc3	účel
propagace	propagace	k1gFnSc2	propagace
slouží	sloužit	k5eAaImIp3nS	sloužit
i	i	k9	i
turistické	turistický	k2eAgFnPc4d1	turistická
informační	informační	k2eAgFnPc4d1	informační
stránky	stránka	k1gFnPc4	stránka
online	onlinout	k5eAaPmIp3nS	onlinout
www.icpisek.cz	www.icpisek.cz	k1gInSc1	www.icpisek.cz
v	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
získaly	získat	k5eAaPmAgInP	získat
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
cenu	cena	k1gFnSc4	cena
ministra	ministr	k1gMnSc2	ministr
pro	pro	k7c4	pro
místní	místní	k2eAgInSc4d1	místní
rozvoj	rozvoj	k1gInSc4	rozvoj
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
soutěže	soutěž	k1gFnSc2	soutěž
Zlatý	zlatý	k2eAgInSc4d1	zlatý
Erb	erb	k1gInSc4	erb
2011	[number]	k4	2011
za	za	k7c4	za
vysokou	vysoký	k2eAgFnSc4d1	vysoká
kvalitu	kvalita	k1gFnSc4	kvalita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Současný	současný	k2eAgInSc1d1	současný
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
rozvoj	rozvoj	k1gInSc1	rozvoj
===	===	k?	===
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc4d1	velký
rozkvět	rozkvět	k1gInSc4	rozkvět
zaznamenaly	zaznamenat	k5eAaPmAgFnP	zaznamenat
nové	nový	k2eAgFnPc1d1	nová
obchodní	obchodní	k2eAgFnPc1d1	obchodní
zóny	zóna	k1gFnPc1	zóna
<g/>
;	;	kIx,	;
jedna	jeden	k4xCgFnSc1	jeden
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pod	pod	k7c7	pod
Zemským	zemský	k2eAgInSc7d1	zemský
hřebčincem	hřebčinec	k1gInSc7	hřebčinec
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
poblíž	poblíž	k6eAd1	poblíž
železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
Písek	Písek	k1gInSc1	Písek
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
zde	zde	k6eAd1	zde
mnoho	mnoho	k4c4	mnoho
řetězců	řetězec	k1gInPc2	řetězec
včetně	včetně	k7c2	včetně
McDonald	McDonalda	k1gFnPc2	McDonalda
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
Tesco	Tesco	k1gNnSc1	Tesco
<g/>
,	,	kIx,	,
Kaufland	Kaufland	k1gInSc1	Kaufland
<g/>
,	,	kIx,	,
Hypernova	Hypernův	k2eAgFnSc1d1	Hypernova
<g/>
,	,	kIx,	,
OBI	OBI	kA	OBI
či	či	k8xC	či
KFC	KFC	kA	KFC
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
Písek	Písek	k1gInSc1	Písek
velký	velký	k2eAgInSc1d1	velký
rozmach	rozmach	k1gInSc1	rozmach
v	v	k7c6	v
soukromém	soukromý	k2eAgNnSc6d1	soukromé
podnikání	podnikání	k1gNnSc6	podnikání
<g/>
;	;	kIx,	;
mnoho	mnoho	k4c4	mnoho
menších	malý	k2eAgFnPc2d2	menší
firem	firma	k1gFnPc2	firma
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
IT	IT	kA	IT
<g/>
/	/	kIx~	/
<g/>
ITC	ITC	kA	ITC
<g/>
,	,	kIx,	,
vývojem	vývoj	k1gInSc7	vývoj
softwaru	software	k1gInSc2	software
<g/>
,	,	kIx,	,
potravinářskou	potravinářský	k2eAgFnSc7d1	potravinářská
výrobou	výroba	k1gFnSc7	výroba
<g/>
,	,	kIx,	,
řemeslnými	řemeslný	k2eAgInPc7d1	řemeslný
výrobky	výrobek	k1gInPc7	výrobek
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
službami	služba	k1gFnPc7	služba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
areálu	areál	k1gInSc6	areál
bývalých	bývalý	k2eAgFnPc2d1	bývalá
píseckých	písecký	k2eAgFnPc2d1	Písecká
Žižkových	Žižkových	k2eAgFnPc2d1	Žižkových
kasáren	kasárny	k1gFnPc2	kasárny
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
za	za	k7c4	za
investici	investice	k1gFnSc4	investice
450	[number]	k4	450
milionů	milion	k4xCgInPc2	milion
CZK	CZK	kA	CZK
Technologické	technologický	k2eAgNnSc1d1	Technologické
Centrum	centrum	k1gNnSc1	centrum
Písek	Písek	k1gInSc1	Písek
(	(	kIx(	(
<g/>
zkr.	zkr.	kA	zkr.
TCP	TCP	kA	TCP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
opravený	opravený	k2eAgInSc4d1	opravený
areál	areál	k1gInSc4	areál
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k8xC	jako
multifunkční	multifunkční	k2eAgNnSc1d1	multifunkční
centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
,	,	kIx,	,
vývoj	vývoj	k1gInSc4	vývoj
technologií	technologie	k1gFnPc2	technologie
a	a	k8xC	a
zázemí	zázemí	k1gNnSc2	zázemí
pro	pro	k7c4	pro
administrativu	administrativa	k1gFnSc4	administrativa
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
kromě	kromě	k7c2	kromě
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
centrálního	centrální	k2eAgInSc2d1	centrální
objektu	objekt	k1gInSc2	objekt
počítá	počítat	k5eAaImIp3nS	počítat
s	s	k7c7	s
revitalizací	revitalizace	k1gFnSc7	revitalizace
svého	svůj	k3xOyFgNnSc2	svůj
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
pokročilé	pokročilý	k2eAgNnSc1d1	pokročilé
počítačového	počítačový	k2eAgMnSc4d1	počítačový
centrum	centrum	k1gNnSc1	centrum
s	s	k7c7	s
CLOUD	CLOUD	kA	CLOUD
datovým	datový	k2eAgInSc7d1	datový
centrem	centr	k1gInSc7	centr
a	a	k8xC	a
připojením	připojení	k1gNnSc7	připojení
k	k	k7c3	k
internetu	internet	k1gInSc3	internet
2	[number]	k4	2
x	x	k?	x
10	[number]	k4	10
GB	GB	kA	GB
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
již	již	k6eAd1	již
přilákalo	přilákat	k5eAaPmAgNnS	přilákat
6	[number]	k4	6
firem	firma	k1gFnPc2	firma
a	a	k8xC	a
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2013	[number]	k4	2013
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
třetina	třetina	k1gFnSc1	třetina
kapacity	kapacita	k1gFnSc2	kapacita
centra	centrum	k1gNnSc2	centrum
zaplněna	zaplněn	k2eAgFnSc1d1	zaplněna
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
centrem	centr	k1gInSc7	centr
projevilo	projevit	k5eAaPmAgNnS	projevit
zájem	zájem	k1gInSc4	zájem
několik	několik	k4yIc1	několik
institucí	instituce	k1gFnPc2	instituce
vyššího	vysoký	k2eAgNnSc2d2	vyšší
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Jihočeská	jihočeský	k2eAgFnSc1d1	Jihočeská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
podnikání	podnikání	k1gNnSc2	podnikání
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
spolupráci	spolupráce	k1gFnSc4	spolupráce
již	již	k6eAd1	již
navázalo	navázat	k5eAaPmAgNnS	navázat
TCP	TCP	kA	TCP
s	s	k7c7	s
ČVUT	ČVUT	kA	ČVUT
či	či	k8xC	či
Univerzitou	univerzita	k1gFnSc7	univerzita
Hradec	Hradec	k1gInSc4	Hradec
Králové	Králové	k2eAgInSc4d1	Králové
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
bylo	být	k5eAaImAgNnS	být
5,700	[number]	k4	5,700
m2	m2	k4	m2
prostoru	prostor	k1gInSc2	prostor
pro	pro	k7c4	pro
až	až	k6eAd1	až
20	[number]	k4	20
firem	firma	k1gFnPc2	firma
s	s	k7c7	s
ca	ca	kA	ca
<g/>
.	.	kIx.	.
300	[number]	k4	300
nově	nově	k6eAd1	nově
vytvořenými	vytvořený	k2eAgFnPc7d1	vytvořená
pracovními	pracovní	k2eAgNnPc7d1	pracovní
místy	místo	k1gNnPc7	místo
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
firmami	firma	k1gFnPc7	firma
IBM	IBM	kA	IBM
<g/>
,	,	kIx,	,
Scheider	Scheider	k1gMnSc1	Scheider
Electric	Electric	k1gMnSc1	Electric
<g/>
,	,	kIx,	,
Juniper	Juniper	k1gMnSc1	Juniper
<g/>
,	,	kIx,	,
Arow	Arow	k1gMnSc1	Arow
ECS	ECS	kA	ECS
<g/>
,	,	kIx,	,
Microsoft	Microsoft	kA	Microsoft
či	či	k8xC	či
Conteg	Conteg	k1gInSc1	Conteg
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
jiného	jiný	k2eAgNnSc2d1	jiné
TCP	TCP	kA	TCP
nabízí	nabízet	k5eAaImIp3nS	nabízet
kongresové	kongresový	k2eAgInPc4d1	kongresový
prostory	prostor	k1gInPc4	prostor
a	a	k8xC	a
konferenční	konferenční	k2eAgInPc1d1	konferenční
sály	sál	k1gInPc1	sál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Areál	areál	k1gInSc1	areál
stejných	stejný	k2eAgFnPc2d1	stejná
kasáren	kasárny	k1gFnPc2	kasárny
byl	být	k5eAaImAgInS	být
též	též	k9	též
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
novinové	novinový	k2eAgFnSc6d1	novinová
anketě	anketa	k1gFnSc6	anketa
vybrán	vybrat	k5eAaPmNgMnS	vybrat
jako	jako	k8xS	jako
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
postavení	postavení	k1gNnSc4	postavení
nového	nový	k2eAgNnSc2d1	nové
aquacentra	aquacentrum	k1gNnSc2	aquacentrum
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
písečtí	písecký	k2eAgMnPc1d1	písecký
občané	občan	k1gMnPc1	občan
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
v	v	k7c6	v
místním	místní	k2eAgNnSc6d1	místní
referendu	referendum	k1gNnSc6	referendum
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
většinou	většina	k1gFnSc7	většina
88	[number]	k4	88
<g/>
%	%	kIx~	%
snahu	snaha	k1gFnSc4	snaha
radnice	radnice	k1gFnSc2	radnice
o	o	k7c4	o
rozšíření	rozšíření	k1gNnSc4	rozšíření
a	a	k8xC	a
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
stávajícího	stávající	k2eAgInSc2d1	stávající
městského	městský	k2eAgInSc2d1	městský
bazénu	bazén	k1gInSc2	bazén
v	v	k7c6	v
přímém	přímý	k2eAgNnSc6d1	přímé
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Stávající	stávající	k2eAgInSc4d1	stávající
bazén	bazén	k1gInSc4	bazén
ovšem	ovšem	k9	ovšem
bez	bez	k7c2	bez
podstatných	podstatný	k2eAgFnPc2d1	podstatná
investic	investice	k1gFnPc2	investice
vydrží	vydržet	k5eAaPmIp3nS	vydržet
až	až	k9	až
další	další	k2eAgNnPc4d1	další
dvě	dva	k4xCgNnPc4	dva
desetiletí	desetiletí	k1gNnPc4	desetiletí
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
píseckém	písecký	k2eAgInSc6d1	písecký
aquacentru	aquacentr	k1gInSc6	aquacentr
město	město	k1gNnSc1	město
uspořádá	uspořádat	k5eAaPmIp3nS	uspořádat
novou	nový	k2eAgFnSc4d1	nová
anketu	anketa	k1gFnSc4	anketa
<g/>
;	;	kIx,	;
nabízeny	nabízen	k2eAgFnPc1d1	nabízena
budou	být	k5eAaImBp3nP	být
lokality	lokalita	k1gFnPc1	lokalita
Výstaviště	výstaviště	k1gNnSc1	výstaviště
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
lesnickou	lesnický	k2eAgFnSc7d1	lesnická
školou	škola	k1gFnSc7	škola
či	či	k8xC	či
soukromé	soukromý	k2eAgInPc4d1	soukromý
pozemky	pozemek	k1gInPc4	pozemek
bývalého	bývalý	k2eAgInSc2d1	bývalý
Jitexu	Jitex	k1gInSc2	Jitex
<g/>
,	,	kIx,	,
u	u	k7c2	u
teplárny	teplárna	k1gFnSc2	teplárna
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vojenském	vojenský	k2eAgInSc6d1	vojenský
areálu	areál	k1gInSc6	areál
na	na	k7c4	na
Pražské	pražský	k2eAgNnSc4d1	Pražské
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nemocnicí	nemocnice	k1gFnSc7	nemocnice
a	a	k8xC	a
obchodní	obchodní	k2eAgFnSc7d1	obchodní
zónou	zóna	k1gFnSc7	zóna
a	a	k8xC	a
další	další	k2eAgFnSc7d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
areálu	areál	k1gInSc2	areál
kasáren	kasárny	k1gFnPc2	kasárny
bude	být	k5eAaImBp3nS	být
řešen	řešit	k5eAaImNgInS	řešit
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
i	i	k9	i
soudní	soudní	k2eAgFnSc7d1	soudní
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celkově	celkově	k6eAd1	celkově
je	být	k5eAaImIp3nS	být
Písek	Písek	k1gInSc1	Písek
jako	jako	k8xC	jako
místo	místo	k1gNnSc1	místo
podnikání	podnikání	k1gNnSc2	podnikání
vysoce	vysoce	k6eAd1	vysoce
hodnocen	hodnotit	k5eAaImNgMnS	hodnotit
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
týdeník	týdeník	k1gInSc1	týdeník
EKONOM	ekonom	k1gMnSc1	ekonom
udělil	udělit	k5eAaPmAgMnS	udělit
městu	město	k1gNnSc3	město
Písek	Písek	k1gInSc1	Písek
první	první	k4xOgNnPc1	první
místo	místo	k1gNnSc1	místo
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2013	[number]	k4	2013
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jihočeského	jihočeský	k2eAgInSc2d1	jihočeský
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
===	===	k?	===
</s>
</p>
<p>
<s>
Písek	Písek	k1gInSc1	Písek
a	a	k8xC	a
okres	okres	k1gInSc1	okres
Písek	Písek	k1gInSc1	Písek
měl	mít	k5eAaImAgInS	mít
k	k	k7c3	k
31.12	[number]	k4	31.12
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
7,84	[number]	k4	7,84
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
žen	žena	k1gFnPc2	žena
bez	bez	k7c2	bez
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
je	být	k5eAaImIp3nS	být
9,13	[number]	k4	9,13
<g/>
%	%	kIx~	%
a	a	k8xC	a
mužů	muž	k1gMnPc2	muž
6,91	[number]	k4	6,91
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
byla	být	k5eAaImAgFnS	být
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
5,0	[number]	k4	5,0
<g/>
%	%	kIx~	%
a	a	k8xC	a
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
5,3	[number]	k4	5,3
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
I	i	k9	i
přes	přes	k7c4	přes
relativně	relativně	k6eAd1	relativně
nízkou	nízký	k2eAgFnSc4d1	nízká
nezaměstnanost	nezaměstnanost	k1gFnSc4	nezaměstnanost
jsou	být	k5eAaImIp3nP	být
nová	nový	k2eAgNnPc1d1	nové
pracovní	pracovní	k2eAgNnPc1d1	pracovní
místa	místo	k1gNnPc1	místo
vzácná	vzácný	k2eAgNnPc1d1	vzácné
a	a	k8xC	a
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
připadalo	připadat	k5eAaPmAgNnS	připadat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
27	[number]	k4	27
uchazečů	uchazeč	k1gMnPc2	uchazeč
(	(	kIx(	(
<g/>
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
údaje	údaj	k1gInPc1	údaj
z	z	k7c2	z
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c4	o
zlepšení	zlepšení	k1gNnSc4	zlepšení
a	a	k8xC	a
snížení	snížení	k1gNnSc4	snížení
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
na	na	k7c4	na
6,7	[number]	k4	6,7
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
Data	datum	k1gNnPc4	datum
za	za	k7c4	za
první	první	k4xOgInSc4	první
měsíc	měsíc	k1gInSc4	měsíc
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
pozici	pozice	k1gFnSc4	pozice
Písku	Písek	k1gInSc2	Písek
jako	jako	k8xC	jako
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
měst	město	k1gNnPc2	město
s	s	k7c7	s
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
zaměstnaností	zaměstnanost	k1gFnSc7	zaměstnanost
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
celé	celý	k2eAgFnSc2d1	celá
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
novém	nový	k2eAgInSc6d1	nový
roce	rok	k1gInSc6	rok
k	k	k7c3	k
rekordnímu	rekordní	k2eAgInSc3d1	rekordní
růstu	růst	k1gInSc3	růst
nezaměstnaných	nezaměstnaný	k1gMnPc2	nezaměstnaný
o	o	k7c4	o
+12,1	+12,1	k4	+12,1
<g/>
%	%	kIx~	%
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
okresu	okres	k1gInSc2	okres
Písek	Písek	k1gInSc1	Písek
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
je	být	k5eAaImIp3nS	být
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
okresu	okres	k1gInSc2	okres
na	na	k7c6	na
hodnotě	hodnota	k1gFnSc6	hodnota
7,3	[number]	k4	7,3
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
7,1	[number]	k4	7,1
<g/>
%	%	kIx~	%
a	a	k8xC	a
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
7,5	[number]	k4	7,5
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výsledek	výsledek	k1gInSc1	výsledek
je	být	k5eAaImIp3nS	být
20	[number]	k4	20
<g/>
.	.	kIx.	.
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
celkem	celkem	k6eAd1	celkem
3600	[number]	k4	3600
nezaměstnaných	nezaměstnaný	k1gMnPc2	nezaměstnaný
připadá	připadat	k5eAaPmIp3nS	připadat
485	[number]	k4	485
volných	volný	k2eAgFnPc2d1	volná
pracovních	pracovní	k2eAgFnPc2d1	pracovní
pozic	pozice	k1gFnPc2	pozice
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
7,4	[number]	k4	7,4
uchazeče	uchazeč	k1gMnSc4	uchazeč
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
pozici	pozice	k1gFnSc4	pozice
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
pátý	pátý	k4xOgInSc4	pátý
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
výsledek	výsledek	k1gInSc4	výsledek
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
celé	celá	k1gFnSc2	celá
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Brownfields	Brownfields	k1gInSc4	Brownfields
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
aglomeraci	aglomerace	k1gFnSc4	aglomerace
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
brownfields	brownfieldsa	k1gFnPc2	brownfieldsa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
větších	veliký	k2eAgFnPc2d2	veliký
částí	část	k1gFnPc2	část
nevyužité	využitý	k2eNgFnPc1d1	nevyužitá
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc4	tento
plochy	plocha	k1gFnPc4	plocha
často	často	k6eAd1	často
v	v	k7c6	v
atraktivní	atraktivní	k2eAgFnSc6d1	atraktivní
blízkosti	blízkost	k1gFnSc6	blízkost
dopravních	dopravní	k2eAgInPc2d1	dopravní
uzlů	uzel	k1gInPc2	uzel
či	či	k8xC	či
sídelních	sídelní	k2eAgNnPc2d1	sídelní
center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
nabízejí	nabízet	k5eAaImIp3nP	nabízet
hodně	hodně	k6eAd1	hodně
volného	volný	k2eAgInSc2d1	volný
prostoru	prostor	k1gInSc2	prostor
k	k	k7c3	k
využití	využití	k1gNnSc3	využití
<g/>
;	;	kIx,	;
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
mála	málo	k1gNnSc2	málo
nevýhod	nevýhoda	k1gFnPc2	nevýhoda
jsou	být	k5eAaImIp3nP	být
nutné	nutný	k2eAgFnPc4d1	nutná
vyšší	vysoký	k2eAgFnPc4d2	vyšší
investice	investice	k1gFnPc4	investice
a	a	k8xC	a
možné	možný	k2eAgFnPc4d1	možná
skryté	skrytý	k2eAgFnPc4d1	skrytá
zátěže	zátěž	k1gFnPc4	zátěž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Otavská	otavský	k2eAgNnPc4d1	otavský
kasárna	kasárna	k1gNnPc4	kasárna
–	–	k?	–
0.5	[number]	k4	0.5
ha	ha	kA	ha
–	–	k?	–
leží	ležet	k5eAaImIp3nS	ležet
nedaleko	nedaleko	k7c2	nedaleko
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
tři	tři	k4xCgFnPc1	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
jsou	být	k5eAaImIp3nP	být
volná	volný	k2eAgFnSc1d1	volná
plocha	plocha	k1gFnSc1	plocha
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
nevyužito	využit	k2eNgNnSc1d1	nevyužito
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Purkratice	Purkratice	k1gFnSc1	Purkratice
–	–	k?	–
Nová	nový	k2eAgFnSc1d1	nová
kasárna	kasárna	k1gNnPc4	kasárna
–	–	k?	–
7.6	[number]	k4	7.6
ha	ha	kA	ha
–	–	k?	–
14	[number]	k4	14
<g/>
%	%	kIx~	%
zastavěno	zastavěn	k2eAgNnSc1d1	zastavěno
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
nevyžito	vyžit	k2eNgNnSc1d1	vyžit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bývalá	bývalý	k2eAgFnSc1d1	bývalá
slévárna	slévárna	k1gFnSc1	slévárna
Remar	Remara	k1gFnPc2	Remara
v	v	k7c4	v
Pražské	pražský	k2eAgNnSc4d1	Pražské
ul	ul	kA	ul
<g/>
.	.	kIx.	.
–	–	k?	–
0.3	[number]	k4	0.3
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kulturní	kulturní	k2eAgFnPc4d1	kulturní
a	a	k8xC	a
vzdělávací	vzdělávací	k2eAgFnPc4d1	vzdělávací
instituce	instituce	k1gFnPc4	instituce
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Divadlo	divadlo	k1gNnSc1	divadlo
Fráni	Fráňa	k1gMnSc2	Fráňa
Šrámka	Šrámek	k1gMnSc2	Šrámek
===	===	k?	===
</s>
</p>
<p>
<s>
Dnešní	dnešní	k2eAgNnSc1d1	dnešní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nese	nést	k5eAaImIp3nS	nést
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
od	od	k7c2	od
r.	r.	kA	r.
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
Palackého	Palackého	k2eAgInPc2d1	Palackého
sadů	sad	k1gInPc2	sad
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
víceúčelového	víceúčelový	k2eAgInSc2d1	víceúčelový
objektu	objekt	k1gInSc2	objekt
"	"	kIx"	"
<g/>
Na	na	k7c4	na
střelnici	střelnice	k1gFnSc4	střelnice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
r.	r.	kA	r.
1868	[number]	k4	1868
vybudován	vybudován	k2eAgInSc4d1	vybudován
divadelní	divadelní	k2eAgInSc4d1	divadelní
sál	sál	k1gInSc4	sál
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
současnou	současný	k2eAgFnSc4d1	současná
podobu	podoba	k1gFnSc4	podoba
divadlo	divadlo	k1gNnSc1	divadlo
získalo	získat	k5eAaPmAgNnS	získat
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
komplexní	komplexní	k2eAgFnSc7d1	komplexní
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
zahájení	zahájení	k1gNnSc1	zahájení
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
uvedením	uvedení	k1gNnSc7	uvedení
"	"	kIx"	"
Prodané	prodaný	k2eAgFnSc2d1	prodaná
nevěsty	nevěsta	k1gFnSc2	nevěsta
<g/>
"	"	kIx"	"
v	v	k7c6	v
provedení	provedení	k1gNnSc6	provedení
ND	ND	kA	ND
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
s	s	k7c7	s
dirigentem	dirigent	k1gMnSc7	dirigent
Václavem	Václav	k1gMnSc7	Václav
Talichem	Talich	k1gMnSc7	Talich
<g/>
.	.	kIx.	.
</s>
<s>
Novou	nový	k2eAgFnSc7d1	nová
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
prošla	projít	k5eAaPmAgFnS	projít
celá	celý	k2eAgFnSc1d1	celá
budova	budova	k1gFnSc1	budova
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2008	[number]	k4	2008
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
probíhají	probíhat	k5eAaImIp3nP	probíhat
divadelní	divadelní	k2eAgFnSc4d1	divadelní
představení	představení	k1gNnSc4	představení
všech	všecek	k3xTgInPc2	všecek
žánrů	žánr	k1gInPc2	žánr
<g/>
,	,	kIx,	,
koncerty	koncert	k1gInPc4	koncert
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgInPc4d1	kulturní
festivaly	festival	k1gInPc4	festival
a	a	k8xC	a
výstavy	výstava	k1gFnPc4	výstava
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
divadla	divadlo	k1gNnSc2	divadlo
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
r.	r.	kA	r.
2003	[number]	k4	2003
rovněž	rovněž	k6eAd1	rovněž
koncertní	koncertní	k2eAgFnSc4d1	koncertní
síň	síň	k1gFnSc4	síň
TROJICE	trojice	k1gFnSc2	trojice
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
pořádají	pořádat	k5eAaImIp3nP	pořádat
zejména	zejména	k9	zejména
koncerty	koncert	k1gInPc4	koncert
vážné	vážná	k1gFnSc3	vážná
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
slavností	slavnost	k1gFnSc7	slavnost
akce	akce	k1gFnSc2	akce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Městská	městský	k2eAgFnSc1d1	městská
knihovna	knihovna	k1gFnSc1	knihovna
Písek	Písek	k1gInSc1	Písek
===	===	k?	===
</s>
</p>
<p>
<s>
Písecká	písecký	k2eAgFnSc1d1	Písecká
městská	městský	k2eAgFnSc1d1	městská
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1841	[number]	k4	1841
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgFnPc3d3	nejstarší
veřejným	veřejný	k2eAgFnPc3d1	veřejná
městským	městský	k2eAgFnPc3d1	městská
knihovnám	knihovna	k1gFnPc3	knihovna
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
její	její	k3xOp3gInSc1	její
fond	fond	k1gInSc1	fond
více	hodně	k6eAd2	hodně
než	než	k8xS	než
135	[number]	k4	135
000	[number]	k4	000
knihovních	knihovní	k2eAgFnPc2d1	knihovní
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Nabízí	nabízet	k5eAaImIp3nS	nabízet
komplexní	komplexní	k2eAgFnPc4d1	komplexní
knihovnicko-informační	knihovnickonformační	k2eAgFnPc4d1	knihovnicko-informační
služby	služba	k1gFnPc4	služba
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
meziknihovní	meziknihovní	k2eAgFnSc2d1	meziknihovní
výpůjční	výpůjční	k2eAgFnSc2d1	výpůjční
služby	služba	k1gFnSc2	služba
a	a	k8xC	a
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
</s>
</p>
<p>
<s>
Internetu	Internet	k1gInSc3	Internet
<g/>
.	.	kIx.	.
</s>
<s>
Pořádá	pořádat	k5eAaImIp3nS	pořádat
pravidelné	pravidelný	k2eAgFnPc4d1	pravidelná
měsíční	měsíční	k2eAgFnPc4d1	měsíční
výstavy	výstava	k1gFnPc4	výstava
a	a	k8xC	a
vzdělávací	vzdělávací	k2eAgInPc4d1	vzdělávací
pořady	pořad	k1gInPc4	pořad
a	a	k8xC	a
besedy	beseda	k1gFnPc4	beseda
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
r.	r.	kA	r.
1970	[number]	k4	1970
knihovna	knihovna	k1gFnSc1	knihovna
sídlí	sídlet	k5eAaImIp3nS	sídlet
na	na	k7c6	na
Alšově	Alšův	k2eAgNnSc6d1	Alšovo
náměstí	náměstí	k1gNnSc6	náměstí
85	[number]	k4	85
v	v	k7c6	v
adaptované	adaptovaný	k2eAgFnSc6d1	adaptovaná
historické	historický	k2eAgFnSc6d1	historická
budově	budova	k1gFnSc6	budova
s	s	k7c7	s
dochovaným	dochovaný	k2eAgInSc7d1	dochovaný
raně	raně	k6eAd1	raně
gotickým	gotický	k2eAgNnSc7d1	gotické
zdivem	zdivo	k1gNnSc7	zdivo
a	a	k8xC	a
empírovou	empírový	k2eAgFnSc7d1	empírová
fasádou	fasáda	k1gFnSc7	fasáda
z	z	k7c2	z
r.	r.	kA	r.
1800	[number]	k4	1800
zdobenou	zdobený	k2eAgFnSc4d1	zdobená
keramickými	keramický	k2eAgFnPc7d1	keramická
vázami	váza	k1gFnPc7	váza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Prácheňské	prácheňský	k2eAgNnSc1d1	Prácheňské
muzeum	muzeum	k1gNnSc1	muzeum
===	===	k?	===
</s>
</p>
<p>
<s>
Prácheňské	prácheňský	k2eAgNnSc1d1	Prácheňské
muzeum	muzeum	k1gNnSc1	muzeum
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
muzeum	muzeum	k1gNnSc1	muzeum
dřívějšího	dřívější	k2eAgInSc2d1	dřívější
Prácheňského	prácheňský	k2eAgInSc2d1	prácheňský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
části	část	k1gFnSc2	část
nestrženého	stržený	k2eNgInSc2d1	stržený
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Divadlo	divadlo	k1gNnSc1	divadlo
Pod	pod	k7c7	pod
čarou	čára	k1gFnSc7	čára
===	===	k?	===
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
čarou	čára	k1gFnSc7	čára
z.	z.	k?	z.
<g/>
s.	s.	k?	s.
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
provozuje	provozovat	k5eAaImIp3nS	provozovat
Divadlo	divadlo	k1gNnSc4	divadlo
Pod	pod	k7c7	pod
čarou	čára	k1gFnSc7	čára
-	-	kIx~	-
alternativní	alternativní	k2eAgInSc1d1	alternativní
hudebně-	hudebně-	k?	hudebně-
divadelní	divadelní	k2eAgInSc4d1	divadelní
klub	klub	k1gInSc4	klub
-	-	kIx~	-
v	v	k7c6	v
objektu	objekt	k1gInSc6	objekt
bývalého	bývalý	k2eAgNnSc2d1	bývalé
kina	kino	k1gNnSc2	kino
Svět	svět	k1gInSc1	svět
<g/>
.	.	kIx.	.
</s>
<s>
Pořádá	pořádat	k5eAaImIp3nS	pořádat
koncerty	koncert	k1gInPc1	koncert
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgNnPc1d1	divadelní
představení	představení	k1gNnPc1	představení
<g/>
,	,	kIx,	,
dramatické	dramatický	k2eAgFnPc1d1	dramatická
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgFnPc1d1	hudební
<g/>
,	,	kIx,	,
tvůrčí	tvůrčí	k2eAgFnPc1d1	tvůrčí
dílny	dílna	k1gFnPc1	dílna
<g/>
,	,	kIx,	,
programy	program	k1gInPc1	program
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
je	být	k5eAaImIp3nS	být
využívaný	využívaný	k2eAgInSc1d1	využívaný
také	také	k9	také
na	na	k7c4	na
plesy	ples	k1gInPc4	ples
a	a	k8xC	a
divadelní	divadelní	k2eAgFnSc2d1	divadelní
přehlídky	přehlídka	k1gFnSc2	přehlídka
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
Šrámkův	Šrámkův	k2eAgInSc4d1	Šrámkův
Písek	Písek	k1gInSc4	Písek
či	či	k8xC	či
Duhové	duhový	k2eAgNnSc4d1	duhové
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
..	..	k?	..
Kulturních	kulturní	k2eAgFnPc2d1	kulturní
akcí	akce	k1gFnPc2	akce
uspořádá	uspořádat	k5eAaPmIp3nS	uspořádat
každoročně	každoročně	k6eAd1	každoročně
okolo	okolo	k7c2	okolo
150	[number]	k4	150
<g/>
.	.	kIx.	.
</s>
<s>
Poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
zázemí	zázemí	k1gNnSc4	zázemí
pro	pro	k7c4	pro
využití	využití	k1gNnSc4	využití
volného	volný	k2eAgInSc2d1	volný
času	čas	k1gInSc2	čas
obyvatel	obyvatel	k1gMnPc2	obyvatel
regionu	region	k1gInSc2	region
-	-	kIx~	-
zkoušky	zkouška	k1gFnPc1	zkouška
kapel	kapela	k1gFnPc2	kapela
<g/>
,	,	kIx,	,
dramatických	dramatický	k2eAgInPc2d1	dramatický
souborů	soubor	k1gInPc2	soubor
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
s	s	k7c7	s
minimálními	minimální	k2eAgInPc7d1	minimální
náklady	náklad	k1gInPc7	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
čarou	čára	k1gFnSc7	čára
z.	z.	k?	z.
<g/>
s.	s.	k?	s.
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c4	na
dobrovolnictví	dobrovolnictví	k1gNnSc4	dobrovolnictví
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
20	[number]	k4	20
členů	člen	k1gInPc2	člen
-	-	kIx~	-
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
a	a	k8xC	a
jen	jen	k9	jen
tři	tři	k4xCgMnPc4	tři
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
je	být	k5eAaImIp3nS	být
stanice	stanice	k1gFnPc4	stanice
Písek	Písek	k1gInSc4	Písek
město	město	k1gNnSc1	město
a	a	k8xC	a
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
Písek	Písek	k1gInSc1	Písek
zastávka	zastávka	k1gFnSc1	zastávka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Písku	Písek	k1gInSc2	Písek
vedou	vést	k5eAaImIp3nP	vést
železniční	železniční	k2eAgFnPc4d1	železniční
tratě	trať	k1gFnPc4	trať
č.	č.	k?	č.
201	[number]	k4	201
do	do	k7c2	do
Tábora	Tábor	k1gInSc2	Tábor
<g/>
,	,	kIx,	,
200	[number]	k4	200
přes	přes	k7c4	přes
Březnice	Březnice	k1gFnPc4	Březnice
a	a	k8xC	a
Příbram	Příbram	k1gFnSc4	Příbram
do	do	k7c2	do
Zdic	Zdice	k1gFnPc2	Zdice
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Plzeň	Plzeň	k1gFnSc1	Plzeň
a	a	k8xC	a
elektrifikovaná	elektrifikovaný	k2eAgFnSc1d1	elektrifikovaná
spojka	spojka	k1gFnSc1	spojka
přes	přes	k7c4	přes
Putim	Putim	k1gFnSc4	Putim
do	do	k7c2	do
Ražic	Ražice	k1gFnPc2	Ražice
nebo	nebo	k8xC	nebo
Protivína	Protivín	k1gInSc2	Protivín
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
–	–	k?	–
<g/>
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
Písku	Písek	k1gInSc2	Písek
vychází	vycházet	k5eAaImIp3nS	vycházet
silnice	silnice	k1gFnSc1	silnice
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
č.	č.	k?	č.
29	[number]	k4	29
do	do	k7c2	do
Tábora	Tábor	k1gInSc2	Tábor
a	a	k8xC	a
podél	podél	k7c2	podél
jižního	jižní	k2eAgInSc2d1	jižní
okraje	okraj	k1gInSc2	okraj
města	město	k1gNnSc2	město
vede	vést	k5eAaImIp3nS	vést
obchvat	obchvat	k1gInSc1	obchvat
silnice	silnice	k1gFnSc2	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
20	[number]	k4	20
z	z	k7c2	z
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
do	do	k7c2	do
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
zhruba	zhruba	k6eAd1	zhruba
10	[number]	k4	10
km	km	kA	km
úsek	úsek	k1gInSc1	úsek
na	na	k7c4	na
křižovatku	křižovatka	k1gFnSc4	křižovatka
se	s	k7c7	s
silnicí	silnice	k1gFnSc7	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
u	u	k7c2	u
Nové	Nová	k1gFnSc2	Nová
Hospody	Hospody	k?	Hospody
slouží	sloužit	k5eAaImIp3nS	sloužit
i	i	k9	i
pro	pro	k7c4	pro
spojení	spojení	k1gNnSc4	spojení
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Úsek	úsek	k1gInSc1	úsek
je	být	k5eAaImIp3nS	být
čtyřproudý	čtyřproudý	k2eAgMnSc1d1	čtyřproudý
a	a	k8xC	a
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
dostavěna	dostavěn	k2eAgFnSc1d1	dostavěna
mimoúrovňová	mimoúrovňový	k2eAgFnSc1d1	mimoúrovňová
křižovatka	křižovatka	k1gFnSc1	křižovatka
<g/>
;	;	kIx,	;
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
se	se	k3xPyFc4	se
napojuje	napojovat	k5eAaImIp3nS	napojovat
dálnice	dálnice	k1gFnSc1	dálnice
D4	D4	k1gFnSc1	D4
od	od	k7c2	od
Příbrami	Příbram	k1gFnSc2	Příbram
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Písek	Písek	k1gInSc1	Písek
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
městskou	městský	k2eAgFnSc4d1	městská
autobusovou	autobusový	k2eAgFnSc4d1	autobusová
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čtyři	čtyři	k4xCgInPc4	čtyři
kilometry	kilometr	k1gInPc4	kilometr
od	od	k7c2	od
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vnitrostátní	vnitrostátní	k2eAgNnSc1d1	vnitrostátní
letiště	letiště	k1gNnSc1	letiště
Písek	Písek	k1gInSc1	Písek
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
jednu	jeden	k4xCgFnSc4	jeden
600	[number]	k4	600
metrů	metr	k1gInPc2	metr
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
ranvej	ranvej	k1gFnSc4	ranvej
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
zde	zde	k6eAd1	zde
téměř	téměř	k6eAd1	téměř
žádný	žádný	k3yNgInSc4	žádný
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Kamenný	kamenný	k2eAgInSc1d1	kamenný
most	most	k1gInSc1	most
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
</s>
</p>
<p>
<s>
Festival	festival	k1gInSc1	festival
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
</s>
</p>
<p>
<s>
Písek	Písek	k1gInSc1	Písek
(	(	kIx(	(
<g/>
hrad	hrad	k1gInSc1	hrad
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Prácheňské	prácheňský	k2eAgNnSc1d1	Prácheňské
muzeum	muzeum	k1gNnSc1	muzeum
</s>
</p>
<p>
<s>
Sladovna	sladovna	k1gFnSc1	sladovna
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Narození	narození	k1gNnSc2	narození
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Povýšení	povýšení	k1gNnSc2	povýšení
svatého	svatý	k2eAgInSc2d1	svatý
Kříže	kříž	k1gInSc2	kříž
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Karla	Karel	k1gMnSc2	Karel
</s>
</p>
<p>
<s>
Městská	městský	k2eAgFnSc1d1	městská
elektrárna	elektrárna	k1gFnSc1	elektrárna
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
</s>
</p>
<p>
<s>
Zemský	zemský	k2eAgInSc1d1	zemský
hřebčinec	hřebčinec	k1gInSc1	hřebčinec
</s>
</p>
<p>
<s>
Putimská	putimský	k2eAgFnSc1d1	Putimská
brána	brána	k1gFnSc1	brána
</s>
</p>
<p>
<s>
Hotel	hotel	k1gInSc1	hotel
Otava	Otava	k1gFnSc1	Otava
</s>
</p>
<p>
<s>
Památník	památník	k1gInSc1	památník
Adolfa	Adolf	k1gMnSc2	Adolf
Heyduka	Heyduk	k1gMnSc2	Heyduk
</s>
</p>
<p>
<s>
Mariánské	mariánský	k2eAgNnSc1d1	Mariánské
sousoší	sousoší	k1gNnSc1	sousoší
</s>
</p>
<p>
<s>
Pomník	pomník	k1gInSc4	pomník
padlým	padlý	k1gMnPc3	padlý
u	u	k7c2	u
Melegnana	Melegnan	k1gMnSc2	Melegnan
a	a	k8xC	a
Solferina	Solferin	k1gMnSc2	Solferin
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
1	[number]	k4	1
-	-	kIx~	-
Dům	dům	k1gInSc1	dům
Městské	městský	k2eAgFnSc2d1	městská
kompanie	kompanie	k1gFnSc2	kompanie
</s>
</p>
<p>
<s>
Reinerovský	Reinerovský	k2eAgInSc1d1	Reinerovský
dům	dům	k1gInSc1	dům
</s>
</p>
<p>
<s>
Palackého	Palackého	k2eAgFnPc1d1	Palackého
sady	sada	k1gFnPc1	sada
</s>
</p>
<p>
<s>
Písecké	písecký	k2eAgFnPc1d1	Písecká
hory	hora	k1gFnPc1	hora
</s>
</p>
<p>
<s>
Lesnická	lesnický	k2eAgFnSc1d1	lesnická
naučná	naučný	k2eAgFnSc1d1	naučná
stezka	stezka	k1gFnSc1	stezka
</s>
</p>
<p>
<s>
Židovský	židovský	k2eAgInSc1d1	židovský
hřbitov	hřbitov	k1gInSc1	hřbitov
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Svatohorské	svatohorský	k2eAgFnSc2d1	Svatohorská
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Svatohorské	svatohorský	k2eAgFnSc2d1	Svatohorská
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgFnPc1d1	další
stavby	stavba	k1gFnPc1	stavba
===	===	k?	===
</s>
</p>
<p>
<s>
Vojenský	vojenský	k2eAgInSc1d1	vojenský
hřbitov	hřbitov	k1gInSc1	hřbitov
</s>
</p>
<p>
<s>
==	==	k?	==
Panorama	panorama	k1gNnSc1	panorama
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Narozené	narozený	k2eAgFnPc4d1	narozená
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
===	===	k?	===
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Ausobský	Ausobský	k2eAgMnSc1d1	Ausobský
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
–	–	k?	–
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Brukner	Brukner	k1gMnSc1	Brukner
(	(	kIx(	(
<g/>
*	*	kIx~	*
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Čabrádek	Čabrádka	k1gFnPc2	Čabrádka
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
scenárista	scenárista	k1gMnSc1	scenárista
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Daneš	Daneš	k1gMnSc1	Daneš
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
ředitel	ředitel	k1gMnSc1	ředitel
Ústavu	ústav	k1gInSc2	ústav
pro	pro	k7c4	pro
jazyk	jazyk	k1gInSc4	jazyk
český	český	k2eAgInSc4d1	český
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Roman	Roman	k1gMnSc1	Roman
Dragoun	dragoun	k1gMnSc1	dragoun
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
portrétista	portrétista	k1gMnSc1	portrétista
</s>
</p>
<p>
<s>
Roman	Roman	k1gMnSc1	Roman
Dragoun	dragoun	k1gMnSc1	dragoun
(	(	kIx(	(
<g/>
*	*	kIx~	*
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
klavírista	klavírista	k1gMnSc1	klavírista
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Dvořák	Dvořák	k1gMnSc1	Dvořák
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moderátor	moderátor	k1gMnSc1	moderátor
<g/>
,	,	kIx,	,
textař	textař	k1gMnSc1	textař
<g/>
,	,	kIx,	,
bavič	bavič	k1gMnSc1	bavič
</s>
</p>
<p>
<s>
Dominik	Dominik	k1gMnSc1	Dominik
Fey	Fey	k1gMnSc1	Fey
(	(	kIx(	(
<g/>
1863	[number]	k4	1863
<g/>
–	–	k?	–
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
</s>
</p>
<p>
<s>
Moric	Moric	k1gMnSc1	Moric
Fialka	fialka	k1gFnSc1	fialka
(	(	kIx(	(
<g/>
1809	[number]	k4	1809
<g/>
–	–	k?	–
<g/>
1869	[number]	k4	1869
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
důstojník	důstojník	k1gMnSc1	důstojník
rakouské	rakouský	k2eAgFnSc2d1	rakouská
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Filip	Filip	k1gMnSc1	Filip
(	(	kIx(	(
<g/>
*	*	kIx~	*
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
a	a	k8xC	a
televizní	televizní	k2eAgMnSc1d1	televizní
režisér	režisér	k1gMnSc1	režisér
</s>
</p>
<p>
<s>
Dominik	Dominik	k1gMnSc1	Dominik
Halmoši	Halmoš	k1gMnSc3	Halmoš
(	(	kIx(	(
<g/>
*	*	kIx~	*
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejový	hokejový	k2eAgMnSc1d1	hokejový
brankář	brankář	k1gMnSc1	brankář
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Hála	Hála	k1gMnSc1	Hála
(	(	kIx(	(
<g/>
*	*	kIx~	*
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
klavírista	klavírista	k1gMnSc1	klavírista
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
pedagog	pedagog	k1gMnSc1	pedagog
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Hanzal	Hanzal	k1gMnSc1	Hanzal
(	(	kIx(	(
<g/>
*	*	kIx~	*
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Chochol	Chochol	k1gMnSc1	Chochol
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
–	–	k?	–
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
projektant	projektant	k1gMnSc1	projektant
a	a	k8xC	a
urbanista	urbanista	k1gMnSc1	urbanista
</s>
</p>
<p>
<s>
Otakar	Otakar	k1gMnSc1	Otakar
Jeremiáš	Jeremiáš	k1gMnSc1	Jeremiáš
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
–	–	k?	–
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
dirigent	dirigent	k1gMnSc1	dirigent
</s>
</p>
<p>
<s>
Jana	Jana	k1gFnSc1	Jana
Klusáková	Klusáková	k1gFnSc1	Klusáková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
a	a	k8xC	a
televizní	televizní	k2eAgFnSc1d1	televizní
moderátorka	moderátorka	k1gFnSc1	moderátorka
<g/>
,	,	kIx,	,
publicistka	publicistka	k1gFnSc1	publicistka
a	a	k8xC	a
překladatelka	překladatelka	k1gFnSc1	překladatelka
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Kotrba	kotrba	k1gFnSc1	kotrba
(	(	kIx(	(
<g/>
*	*	kIx~	*
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
trenér	trenér	k1gMnSc1	trenér
</s>
</p>
<p>
<s>
Iveta	Iveta	k1gFnSc1	Iveta
Luzumová	Luzumová	k1gFnSc1	Luzumová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
házenkářka	házenkářka	k1gFnSc1	házenkářka
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
házenkářka	házenkářka	k1gFnSc1	házenkářka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Kouba	Kouba	k1gMnSc1	Kouba
(	(	kIx(	(
<g/>
*	*	kIx~	*
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
Ústavu	ústav	k1gInSc2	ústav
filozofie	filozofie	k1gFnSc2	filozofie
a	a	k8xC	a
religionistiky	religionistika	k1gFnSc2	religionistika
na	na	k7c4	na
FF	ff	kA	ff
UK	UK	kA	UK
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Krška	Krška	k1gMnSc1	Krška
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
prozaik	prozaik	k1gMnSc1	prozaik
</s>
</p>
<p>
<s>
Iva	Iva	k1gFnSc1	Iva
Kubelková	Kubelková	k1gFnSc1	Kubelková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
modelka	modelka	k1gFnSc1	modelka
<g/>
,	,	kIx,	,
moderátorka	moderátorka	k1gFnSc1	moderátorka
a	a	k8xC	a
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Malý	Malý	k1gMnSc1	Malý
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
–	–	k?	–
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
regionalista	regionalista	k1gMnSc1	regionalista
<g/>
,	,	kIx,	,
redaktor	redaktor	k1gMnSc1	redaktor
</s>
</p>
<p>
<s>
Bohuš	Bohuš	k1gMnSc1	Bohuš
Matuš	Matuš	k1gMnSc1	Matuš
(	(	kIx(	(
<g/>
*	*	kIx~	*
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
</s>
</p>
<p>
<s>
Dana	Dana	k1gFnSc1	Dana
Morávková	Morávková	k1gFnSc1	Morávková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
moderátorka	moderátorka	k1gFnSc1	moderátorka
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Mráz	Mráz	k1gMnSc1	Mráz
(	(	kIx(	(
<g/>
*	*	kIx~	*
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
také	také	k9	také
jako	jako	k8xC	jako
George	George	k1gFnSc1	George
Mraz	mrazit	k5eAaImRp2nS	mrazit
<g/>
,	,	kIx,	,
jazzový	jazzový	k2eAgMnSc1d1	jazzový
kontrabasista	kontrabasista	k1gMnSc1	kontrabasista
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
USA	USA	kA	USA
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Mukařovský	Mukařovský	k1gMnSc1	Mukařovský
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
–	–	k?	–
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
estetik	estetik	k1gMnSc1	estetik
a	a	k8xC	a
literární	literární	k2eAgMnSc1d1	literární
teoretik	teoretik	k1gMnSc1	teoretik
<g/>
,	,	kIx,	,
čelný	čelný	k2eAgMnSc1d1	čelný
představitel	představitel	k1gMnSc1	představitel
české	český	k2eAgFnSc2d1	Česká
strukturalistické	strukturalistický	k2eAgFnSc2d1	strukturalistická
školy	škola	k1gFnSc2	škola
</s>
</p>
<p>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Neckář	Neckář	k1gMnSc1	Neckář
(	(	kIx(	(
<g/>
*	*	kIx~	*
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Neumannová	Neumannová	k1gFnSc1	Neumannová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
běžkařka	běžkařka	k1gFnSc1	běžkařka
<g/>
,	,	kIx,	,
olympijská	olympijský	k2eAgFnSc1d1	olympijská
vítězka	vítězka	k1gFnSc1	vítězka
a	a	k8xC	a
mistryně	mistryně	k1gFnSc1	mistryně
světa	svět	k1gInSc2	svět
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Onufer	Onufer	k1gMnSc1	Onufer
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anglista	anglista	k1gMnSc1	anglista
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
a	a	k8xC	a
literární	literární	k2eAgMnSc1d1	literární
kritik	kritik	k1gMnSc1	kritik
</s>
</p>
<p>
<s>
Arnošt	Arnošt	k1gMnSc1	Arnošt
Petráček	Petráček	k1gMnSc1	Petráček
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
paralympijský	paralympijský	k2eAgMnSc1d1	paralympijský
plavec	plavec	k1gMnSc1	plavec
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
zlaté	zlatý	k2eAgFnSc2d1	zlatá
medaile	medaile	k1gFnSc2	medaile
z	z	k7c2	z
Paralympijských	paralympijský	k2eAgFnPc2d1	paralympijská
her	hra	k1gFnPc2	hra
</s>
</p>
<p>
<s>
Radek	Radek	k1gMnSc1	Radek
Pilař	Pilař	k1gMnSc1	Pilař
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Hladič	hladič	k1gMnSc1	hladič
Písecký	písecký	k2eAgMnSc1d1	písecký
(	(	kIx(	(
<g/>
1482	[number]	k4	1482
<g/>
–	–	k?	–
<g/>
1511	[number]	k4	1511
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
,	,	kIx,	,
děkan	děkan	k1gMnSc1	děkan
pražské	pražský	k2eAgFnSc2d1	Pražská
univerzity	univerzita	k1gFnSc2	univerzita
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Pleskot	Pleskot	k1gMnSc1	Pleskot
(	(	kIx(	(
<g/>
*	*	kIx~	*
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
C.	C.	kA	C.
Putna	putna	k1gFnSc1	putna
(	(	kIx(	(
<g/>
*	*	kIx~	*
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
literární	literární	k2eAgMnSc1d1	literární
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
kritik	kritik	k1gMnSc1	kritik
<g/>
,	,	kIx,	,
komparatista	komparatista	k1gMnSc1	komparatista
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
Fakulty	fakulta	k1gFnSc2	fakulta
humanitních	humanitní	k2eAgFnPc2d1	humanitní
studií	studio	k1gNnPc2	studio
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Ryantová	Ryantový	k2eAgFnSc1d1	Ryantová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
archivářka	archivářka	k1gFnSc1	archivářka
<g/>
,	,	kIx,	,
historička	historička	k1gFnSc1	historička
<g/>
,	,	kIx,	,
pedagožka	pedagožka	k1gFnSc1	pedagožka
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Rybák	Rybák	k1gMnSc1	Rybák
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komunistický	komunistický	k2eAgMnSc1d1	komunistický
publicista	publicista	k1gMnSc1	publicista
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Stropnický	stropnický	k2eAgMnSc1d1	stropnický
(	(	kIx(	(
<g/>
1796	[number]	k4	1796
<g/>
–	–	k?	–
<g/>
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
občanské	občanský	k2eAgFnSc2d1	občanská
záložny	záložna	k1gFnSc2	záložna
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
</s>
</p>
<p>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Šebánek	Šebánek	k1gMnSc1	Šebánek
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
–	–	k?	–
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
archivář	archivář	k1gMnSc1	archivář
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
</s>
</p>
<p>
<s>
Dagmar	Dagmar	k1gFnSc1	Dagmar
Šimková	Šimková	k1gFnSc1	Šimková
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politická	politický	k2eAgFnSc1d1	politická
vězeňkyně	vězeňkyně	k1gFnSc1	vězeňkyně
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Špirhanzl	Špirhanzl	k1gMnSc1	Špirhanzl
(	(	kIx(	(
<g/>
1857	[number]	k4	1857
<g/>
–	–	k?	–
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stavební	stavební	k2eAgMnSc1d1	stavební
podnikatel	podnikatel	k1gMnSc1	podnikatel
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
starosta	starosta	k1gMnSc1	starosta
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Štětka	Štětka	k1gMnSc1	Štětka
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
starého	starý	k2eAgInSc2d1	starý
Písku	Písek	k1gInSc2	Písek
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Švejcar	Švejcar	k1gMnSc1	Švejcar
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
meditativních	meditativní	k2eAgInPc2d1	meditativní
obrazů	obraz	k1gInPc2	obraz
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Velenovský	Velenovský	k2eAgMnSc1d1	Velenovský
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
–	–	k?	–
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
fotograf	fotograf	k1gMnSc1	fotograf
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Verner	Verner	k1gMnSc1	Verner
(	(	kIx(	(
<g/>
*	*	kIx~	*
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krasobruslař	krasobruslař	k1gMnSc1	krasobruslař
<g/>
,	,	kIx,	,
mistr	mistr	k1gMnSc1	mistr
Evropy	Evropa	k1gFnSc2	Evropa
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Weiner	Weiner	k1gMnSc1	Weiner
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
–	–	k?	–
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
expresionistický	expresionistický	k2eAgMnSc1d1	expresionistický
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
prozaik	prozaik	k1gMnSc1	prozaik
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Otakar	Otakar	k1gMnSc1	Otakar
Zeithammer	Zeithammer	k1gMnSc1	Zeithammer
(	(	kIx(	(
<g/>
1832	[number]	k4	1832
<g/>
–	–	k?	–
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
staročeský	staročeský	k2eAgMnSc1d1	staročeský
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Zíb	Zíb	k1gMnSc1	Zíb
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenista	tenista	k1gMnSc1	tenista
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Škoch	Škoch	k1gMnSc1	Škoch
(	(	kIx(	(
<g/>
*	*	kIx~	*
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotograf	fotograf	k1gMnSc1	fotograf
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Kayser	Kayser	k1gMnSc1	Kayser
(	(	kIx(	(
<g/>
Kaiser	Kaiser	k1gMnSc1	Kaiser
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
</s>
</p>
<p>
<s>
===	===	k?	===
Působící	působící	k2eAgMnSc1d1	působící
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
===	===	k?	===
</s>
</p>
<p>
<s>
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
Aleš	Aleš	k1gMnSc1	Aleš
(	(	kIx(	(
<g/>
1852	[number]	k4	1852
<g/>
–	–	k?	–
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
</s>
</p>
<p>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Borecký	borecký	k2eAgMnSc1d1	borecký
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
–	–	k?	–
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
,	,	kIx,	,
literární	literární	k2eAgMnSc1d1	literární
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
kritik	kritik	k1gMnSc1	kritik
<g/>
,	,	kIx,	,
orientalista	orientalista	k1gMnSc1	orientalista
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
knihovny	knihovna	k1gFnSc2	knihovna
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Cumpfe	Cumpf	k1gInSc5	Cumpf
(	(	kIx(	(
<g/>
1853	[number]	k4	1853
<g/>
–	–	k?	–
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
klasický	klasický	k2eAgMnSc1d1	klasický
filolog	filolog	k1gMnSc1	filolog
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
gymnázia	gymnázium	k1gNnSc2	gymnázium
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Doubek	Doubek	k1gMnSc1	Doubek
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
</s>
</p>
<p>
<s>
Bedřich	Bedřich	k1gMnSc1	Bedřich
Dubský	Dubský	k1gMnSc1	Dubský
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
–	–	k?	–
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
archeolog	archeolog	k1gMnSc1	archeolog
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Gregora	Gregor	k1gMnSc2	Gregor
(	(	kIx(	(
<g/>
1819	[number]	k4	1819
<g/>
–	–	k?	–
<g/>
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
</s>
</p>
<p>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
Heyduk	Heyduk	k1gMnSc1	Heyduk
(	(	kIx(	(
<g/>
1835	[number]	k4	1835
<g/>
–	–	k?	–
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Holeček	Holeček	k1gMnSc1	Holeček
(	(	kIx(	(
<g/>
1853	[number]	k4	1853
<g/>
–	–	k?	–
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Kodl	Kodl	k1gMnSc1	Kodl
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
-	-	kIx~	-
<g/>
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
</s>
</p>
<p>
<s>
Alois	Alois	k1gMnSc1	Alois
Laub	Laub	k1gMnSc1	Laub
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ruský	ruský	k2eAgMnSc1d1	ruský
legionář	legionář	k1gMnSc1	legionář
<g/>
,	,	kIx,	,
důstojník	důstojník	k1gMnSc1	důstojník
čs	čs	kA	čs
<g/>
.	.	kIx.	.
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
odbojář	odbojář	k1gMnSc1	odbojář
<g/>
;	;	kIx,	;
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
sloužil	sloužit	k5eAaImAgMnS	sloužit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1928	[number]	k4	1928
až	až	k9	až
1937	[number]	k4	1937
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Zachariáš	Zachariáš	k1gMnSc1	Zachariáš
Quast	Quast	k1gMnSc1	Quast
(	(	kIx(	(
<g/>
1814	[number]	k4	1814
<g/>
–	–	k?	–
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
proslulý	proslulý	k2eAgMnSc1d1	proslulý
malbami	malba	k1gFnPc7	malba
miniatur	miniatura	k1gFnPc2	miniatura
na	na	k7c6	na
skle	sklo	k1gNnSc6	sklo
a	a	k8xC	a
porcelánu	porcelán	k1gInSc6	porcelán
</s>
</p>
<p>
<s>
August	August	k1gMnSc1	August
Sedláček	Sedláček	k1gMnSc1	Sedláček
(	(	kIx(	(
<g/>
1843	[number]	k4	1843
<g/>
–	–	k?	–
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Věnceslav	Věnceslava	k1gFnPc2	Věnceslava
Soukup	Soukup	k1gMnSc1	Soukup
(	(	kIx(	(
<g/>
1819	[number]	k4	1819
<g/>
–	–	k?	–
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
odborný	odborný	k2eAgMnSc1d1	odborný
publicista	publicista	k1gMnSc1	publicista
</s>
</p>
<p>
<s>
Otakar	Otakar	k1gMnSc1	Otakar
Ševčík	Ševčík	k1gMnSc1	Ševčík
(	(	kIx(	(
<g/>
1852	[number]	k4	1852
<g/>
–	–	k?	–
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
houslový	houslový	k2eAgMnSc1d1	houslový
pedagog	pedagog	k1gMnSc1	pedagog
</s>
</p>
<p>
<s>
Fráňa	Fráňa	k1gMnSc1	Fráňa
Šrámek	Šrámek	k1gMnSc1	Šrámek
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
–	–	k?	–
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
prozaik	prozaik	k1gMnSc1	prozaik
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
</s>
</p>
<p>
<s>
Alois	Alois	k1gMnSc1	Alois
Ladislav	Ladislav	k1gMnSc1	Ladislav
Vymetal	vymetat	k5eAaImAgMnS	vymetat
(	(	kIx(	(
<g/>
1865	[number]	k4	1865
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
</s>
</p>
<p>
<s>
===	===	k?	===
Pobývající	pobývající	k2eAgMnSc1d1	pobývající
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
===	===	k?	===
</s>
</p>
<p>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gNnSc2	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
</s>
</p>
<p>
<s>
Emil	Emil	k1gMnSc1	Emil
Hácha	Hácha	k1gMnSc1	Hácha
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
</s>
</p>
<p>
<s>
Caerphilly	Caerphilla	k1gFnPc1	Caerphilla
<g/>
,	,	kIx,	,
Wales	Wales	k1gInSc1	Wales
</s>
</p>
<p>
<s>
Deggendorf	Deggendorf	k1gInSc1	Deggendorf
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
</s>
</p>
<p>
<s>
Lemvig	Lemvig	k1gInSc1	Lemvig
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
</s>
</p>
<p>
<s>
Smiltene	Smilten	k1gMnSc5	Smilten
<g/>
,	,	kIx,	,
Lotyšsko	Lotyšsko	k1gNnSc5	Lotyšsko
</s>
</p>
<p>
<s>
Veľký	Veľký	k2eAgMnSc1d1	Veľký
Krtíš	Krtíš	k1gMnSc1	Krtíš
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
</s>
</p>
<p>
<s>
Wetzlar	Wetzlar	k1gInSc1	Wetzlar
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
DUŠEK	Dušek	k1gMnSc1	Dušek
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
Písek	Písek	k1gInSc1	Písek
:	:	kIx,	:
staré	starý	k2eAgNnSc1d1	staré
a	a	k8xC	a
nové	nový	k2eAgNnSc1d1	nové
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Písek	Písek	k1gInSc1	Písek
:	:	kIx,	:
Praam	Praam	k1gInSc1	Praam
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
ca	ca	kA	ca
110	[number]	k4	110
s.	s.	k?	s.
<g/>
,	,	kIx,	,
fot	fot	k1gInSc1	fot
<g/>
,	,	kIx,	,
il	il	k?	il
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86616	[number]	k4	86616
<g/>
-	-	kIx~	-
<g/>
28	[number]	k4	28
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HOLKUP	HOLKUP	kA	HOLKUP
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Památník	památník	k1gInSc1	památník
Občanské	občanský	k2eAgFnSc2d1	občanská
záložny	záložna	k1gFnSc2	záložna
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HLADKÝ	Hladký	k1gMnSc1	Hladký
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Kamenný	kamenný	k2eAgInSc1d1	kamenný
most	most	k1gInSc1	most
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
<g/>
.	.	kIx.	.
</s>
<s>
Písek	Písek	k1gInSc1	Písek
:	:	kIx,	:
Město	město	k1gNnSc1	město
Písek	Písek	k1gInSc1	Písek
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
144	[number]	k4	144
s.	s.	k?	s.
<g/>
.	.	kIx.	.
fot	fot	k1gInSc1	fot
<g/>
.	.	kIx.	.
il	il	k?	il
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HLADKÝ	Hladký	k1gMnSc1	Hladký
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
-	-	kIx~	-
PRÁŠEK	Prášek	k1gMnSc1	Prášek
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Průvodce	průvodce	k1gMnSc1	průvodce
po	po	k7c6	po
píseckých	písecký	k2eAgFnPc6d1	Písecká
památkách	památka	k1gFnPc6	památka
<g/>
.	.	kIx.	.
</s>
<s>
Fot	fot	k1gInSc1	fot
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Vávra	Vávra	k1gMnSc1	Vávra
<g/>
.	.	kIx.	.
</s>
<s>
Písek	Písek	k1gInSc1	Písek
:	:	kIx,	:
Městský	městský	k2eAgInSc1d1	městský
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Nestr	Nestr	k1gMnSc1	Nestr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JAVŮREK	Javůrek	k1gMnSc1	Javůrek
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
-	-	kIx~	-
KOTALÍK	KOTALÍK	kA	KOTALÍK
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Písek	Písek	k1gInSc1	Písek
ve	v	k7c6	v
fotografii	fotografia	k1gFnSc6	fotografia
1860	[number]	k4	1860
–	–	k?	–
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Písek	Písek	k1gInSc1	Písek
<g/>
:	:	kIx,	:
Město	město	k1gNnSc1	město
Písek	Písek	k1gInSc1	Písek
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
240	[number]	k4	240
s.	s.	k?	s.
</s>
</p>
<p>
<s>
KOLÁŘ	Kolář	k1gMnSc1	Kolář
<g/>
,	,	kIx,	,
Ondřej	Ondřej	k1gMnSc1	Ondřej
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
v	v	k7c6	v
času	čas	k1gInSc2	čas
proměn	proměna	k1gFnPc2	proměna
:	:	kIx,	:
Z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
města	město	k1gNnSc2	město
Písku	Písek	k1gInSc2	Písek
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Il	Il	k?	Il
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Doubek	Doubek	k1gMnSc1	Doubek
<g/>
.	.	kIx.	.
</s>
<s>
Písek	Písek	k1gInSc1	Písek
:	:	kIx,	:
Městské	městský	k2eAgNnSc1d1	Městské
kult	kult	k1gInSc1	kult
<g/>
.	.	kIx.	.
středisko	středisko	k1gNnSc1	středisko
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
32	[number]	k4	32
s.	s.	k?	s.
</s>
</p>
<p>
<s>
KRÝZL	KRÝZL	kA	KRÝZL
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Turistický	turistický	k2eAgMnSc1d1	turistický
průvodce	průvodce	k1gMnSc1	průvodce
po	po	k7c6	po
městě	město	k1gNnSc6	město
Písku	Písek	k1gInSc2	Písek
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Písek	Písek	k1gInSc1	Písek
:	:	kIx,	:
Podhajský	Podhajský	k2eAgMnSc1d1	Podhajský
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
80	[number]	k4	80
s.	s.	k?	s.
<g/>
,	,	kIx,	,
fot	fot	k1gInSc1	fot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NADEMLEJNSKÁ	NADEMLEJNSKÁ	kA	NADEMLEJNSKÁ
<g/>
,	,	kIx,	,
Dana	Dana	k1gFnSc1	Dana
<g/>
.	.	kIx.	.
</s>
<s>
Písek	Písek	k1gInSc1	Písek
<g/>
.	.	kIx.	.
</s>
<s>
Fot	fot	k1gInSc1	fot
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Třeský	Třeský	k1gMnSc1	Třeský
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
ALE	ale	k8xC	ale
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
nestr	nestr	k1gInSc1	nestr
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
900793	[number]	k4	900793
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PÍSEK	Písek	k1gInSc1	Písek
-	-	kIx~	-
almanach	almanach	k1gInSc1	almanach
:	:	kIx,	:
750	[number]	k4	750
let	léto	k1gNnPc2	léto
města	město	k1gNnSc2	město
Písku	Písek	k1gInSc6	Písek
<g/>
.	.	kIx.	.
</s>
<s>
Písek	Písek	k1gInSc1	Písek
:	:	kIx,	:
Městský	městský	k2eAgInSc1d1	městský
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
363	[number]	k4	363
s.	s.	k?	s.
:	:	kIx,	:
il	il	k?	il
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-85618-08-7	[number]	k4	80-85618-08-7
.	.	kIx.	.
</s>
</p>
<p>
<s>
PÍSEK	Písek	k1gInSc1	Písek
-	-	kIx~	-
řeka	řeka	k1gFnSc1	řeka
vypravuje	vypravovat	k5eAaImIp3nS	vypravovat
<g/>
.	.	kIx.	.
</s>
<s>
Úvod	úvod	k1gInSc1	úvod
Jiří	Jiří	k1gMnSc1	Jiří
Prášek	Prášek	k1gMnSc1	Prášek
<g/>
,	,	kIx,	,
fot	fot	k1gInSc1	fot
<g/>
.	.	kIx.	.
</s>
<s>
Kate	kat	k1gMnSc5	kat
Bliss	Bliss	k1gInSc1	Bliss
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Bouček	Bouček	k1gMnSc1	Bouček
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hübl	Hübl	k1gMnSc1	Hübl
aj.	aj.	kA	aj.
Písek	Písek	k1gInSc1	Písek
:	:	kIx,	:
Městský	městský	k2eAgInSc1d1	městský
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
136	[number]	k4	136
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
238	[number]	k4	238
<g/>
-	-	kIx~	-
<g/>
7611	[number]	k4	7611
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PRÁŠEK	Prášek	k1gMnSc1	Prášek
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Písecké	písecký	k2eAgInPc1d1	písecký
XX	XX	kA	XX
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
<g/>
.	.	kIx.	.
</s>
<s>
Písek	Písek	k1gInSc1	Písek
<g/>
:	:	kIx,	:
J	J	kA	J
&	&	k?	&
M	M	kA	M
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
193	[number]	k4	193
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86154	[number]	k4	86154
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PRÁŠEK	Prášek	k1gMnSc1	Prášek
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Písecké	písecký	k2eAgNnSc4d1	písecké
2	[number]	k4	2
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
<g/>
.	.	kIx.	.
</s>
<s>
Písek	Písek	k1gInSc1	Písek
:	:	kIx,	:
J	J	kA	J
&	&	k?	&
M	M	kA	M
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
221	[number]	k4	221
s.	s.	k?	s.
Bibl.	Bibl.	k1gFnPc2	Bibl.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86154	[number]	k4	86154
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PRÁŠEK	Prášek	k1gMnSc1	Prášek
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Písek-procházka	Písekrocházka	k1gFnSc1	Písek-procházka
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Písek	Písek	k1gInSc1	Písek
:	:	kIx,	:
Městský	městský	k2eAgInSc1d1	městský
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
42	[number]	k4	42
s.	s.	k?	s.
<g/>
,	,	kIx,	,
il	il	k?	il
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
fot	fot	k1gInSc1	fot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RYŠLAVÝ	ryšlavý	k2eAgMnSc1d1	ryšlavý
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Písek	Písek	k1gInSc1	Písek
-	-	kIx~	-
jihočeské	jihočeský	k2eAgFnSc2d1	Jihočeská
Athény	Athéna	k1gFnSc2	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
:	:	kIx,	:
Vyd	Vyd	k1gFnSc1	Vyd
<g/>
.	.	kIx.	.
</s>
<s>
MCU	MCU	kA	MCU
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
62	[number]	k4	62
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7339	[number]	k4	7339
<g/>
-	-	kIx~	-
<g/>
134	[number]	k4	134
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SEDLÁČEK	Sedláček	k1gMnSc1	Sedláček
<g/>
,	,	kIx,	,
August	August	k1gMnSc1	August
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
královského	královský	k2eAgNnSc2d1	královské
krajského	krajský	k2eAgNnSc2d1	krajské
města	město	k1gNnSc2	město
Písku	Písek	k1gInSc2	Písek
nad	nad	k7c7	nad
Otavou	Otava	k1gFnSc7	Otava
:	:	kIx,	:
Od	od	k7c2	od
nejstarší	starý	k2eAgFnSc2d3	nejstarší
doby	doba	k1gFnSc2	doba
až	až	k9	až
do	do	k7c2	do
zřízení	zřízení	k1gNnSc2	zřízení
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
úřadu	úřad	k1gInSc2	úřad
;	;	kIx,	;
1	[number]	k4	1
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
Písek	Písek	k1gInSc1	Písek
:	:	kIx,	:
Obec	obec	k1gFnSc1	obec
písecká	písecký	k2eAgFnSc1d1	Písecká
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
461	[number]	k4	461
s.	s.	k?	s.
<g/>
,	,	kIx,	,
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
fot	fot	k1gInSc1	fot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SEDLÁČEK	Sedláček	k1gMnSc1	Sedláček
<g/>
,	,	kIx,	,
August	August	k1gMnSc1	August
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
královského	královský	k2eAgNnSc2d1	královské
krajského	krajský	k2eAgNnSc2d1	krajské
města	město	k1gNnSc2	město
Písku	Písek	k1gInSc2	Písek
nad	nad	k7c7	nad
Otavou	Otava	k1gFnSc7	Otava
:	:	kIx,	:
Od	od	k7c2	od
zřízení	zřízení	k1gNnSc2	zřízení
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
úřadu	úřad	k1gInSc2	úřad
až	až	k9	až
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
;	;	kIx,	;
2	[number]	k4	2
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
Písek	Písek	k1gInSc1	Písek
:	:	kIx,	:
Obec	obec	k1gFnSc1	obec
písecká	písecký	k2eAgFnSc1d1	Písecká
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
462	[number]	k4	462
s.	s.	k?	s.
<g/>
,	,	kIx,	,
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
fot	fot	k1gInSc1	fot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SEDLÁČEK	Sedláček	k1gMnSc1	Sedláček
<g/>
,	,	kIx,	,
August	August	k1gMnSc1	August
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
královského	královský	k2eAgNnSc2d1	královské
krajského	krajský	k2eAgNnSc2d1	krajské
města	město	k1gNnSc2	město
Písku	Písek	k1gInSc2	Písek
nad	nad	k7c7	nad
Otavou	Otava	k1gFnSc7	Otava
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
zvláštních	zvláštní	k2eAgFnPc2d1	zvláštní
částí	část	k1gFnPc2	část
;	;	kIx,	;
3	[number]	k4	3
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
Písek	Písek	k1gInSc1	Písek
:	:	kIx,	:
Obec	obec	k1gFnSc1	obec
písecká	písecký	k2eAgFnSc1d1	Písecká
<g/>
,	,	kIx,	,
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
482	[number]	k4	482
s.	s.	k?	s.
<g/>
,	,	kIx,	,
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
fot	fot	k1gInSc1	fot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠRŮTEK	šrůtek	k1gInSc1	šrůtek
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
:	:	kIx,	:
Písek	Písek	k1gInSc1	Písek
<g/>
.	.	kIx.	.
</s>
<s>
Fot	fot	k1gInSc1	fot
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Šechtlová	Šechtlová	k1gFnSc1	Šechtlová
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Škoch	Škoch	k1gInSc1	Škoch
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
:	:	kIx,	:
Krajské	krajský	k2eAgNnSc4d1	krajské
nakl	naknout	k5eAaPmAgMnS	naknout
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
46	[number]	k4	46
s.	s.	k?	s.
</s>
</p>
<p>
<s>
ZEMANOVÁ	Zemanová	k1gFnSc1	Zemanová
<g/>
,	,	kIx,	,
Blanka	Blanka	k1gFnSc1	Blanka
<g/>
.	.	kIx.	.
</s>
<s>
Písecké	písecký	k2eAgFnPc4d1	Písecká
procházky	procházka	k1gFnPc4	procházka
pro	pro	k7c4	pro
začátečníky	začátečník	k1gMnPc4	začátečník
i	i	k8xC	i
pokročilé	pokročilý	k2eAgMnPc4d1	pokročilý
<g/>
.	.	kIx.	.
</s>
<s>
Ilustr	Ilustr	k1gInSc1	Ilustr
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Doubek	Doubek	k1gMnSc1	Doubek
<g/>
.	.	kIx.	.
</s>
<s>
Písek	Písek	k1gInSc1	Písek
:	:	kIx,	:
J	J	kA	J
&	&	k?	&
M	M	kA	M
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
183	[number]	k4	183
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86154	[number]	k4	86154
<g/>
-	-	kIx~	-
<g/>
52	[number]	k4	52
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Písek	Písek	k1gInSc1	Písek
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Písek	Písek	k1gInSc1	Písek
(	(	kIx(	(
<g/>
obce	obec	k1gFnPc1	obec
<g/>
)	)	kIx)	)
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Písek	Písek	k1gInSc1	Písek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Písek	Písek	k1gInSc1	Písek
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vybrané	vybraný	k2eAgInPc1d1	vybraný
statistické	statistický	k2eAgInPc1d1	statistický
údaje	údaj	k1gInPc1	údaj
za	za	k7c4	za
obec	obec	k1gFnSc4	obec
Písek	Písek	k1gInSc1	Písek
ve	v	k7c6	v
Veřejné	veřejný	k2eAgFnSc6d1	veřejná
databázi	databáze	k1gFnSc6	databáze
ČSÚ	ČSÚ	kA	ČSÚ
</s>
</p>
<p>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
o	o	k7c6	o
píseckém	písecký	k2eAgInSc6d1	písecký
mostě	most	k1gInSc6	most
<g/>
,	,	kIx,	,
s	s	k7c7	s
fotografiemi	fotografia	k1gFnPc7	fotografia
</s>
</p>
<p>
<s>
Statistické	statistický	k2eAgFnPc1d1	statistická
informace	informace	k1gFnPc1	informace
na	na	k7c6	na
serveru	server	k1gInSc6	server
Regionservis	Regionservis	k1gFnSc2	Regionservis
</s>
</p>
<p>
<s>
Městská	městský	k2eAgFnSc1d1	městská
knihovna	knihovna	k1gFnSc1	knihovna
Písek	Písek	k1gInSc1	Písek
</s>
</p>
