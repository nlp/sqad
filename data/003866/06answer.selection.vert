<s>
Roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Velká	velký	k2eAgFnSc1d1	velká
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
byla	být	k5eAaImAgFnS	být
zahrnuta	zahrnut	k2eAgFnSc1d1	zahrnuta
všechna	všechen	k3xTgNnPc4	všechen
předměstí	předměstí	k1gNnSc4	předměstí
včetně	včetně	k7c2	včetně
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
samostatných	samostatný	k2eAgNnPc2d1	samostatné
měst	město	k1gNnPc2	město
jako	jako	k8xC	jako
Královské	královský	k2eAgInPc1d1	královský
Vinohrady	Vinohrady	k1gInPc1	Vinohrady
<g/>
,	,	kIx,	,
Nusle	Nusle	k1gFnPc1	Nusle
nebo	nebo	k8xC	nebo
Košíře	Košíře	k1gInPc1	Košíře
<g/>
.	.	kIx.	.
</s>
