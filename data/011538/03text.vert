<p>
<s>
Hvězdokupa	hvězdokupa	k1gFnSc1	hvězdokupa
je	být	k5eAaImIp3nS	být
seskupení	seskupení	k1gNnSc4	seskupení
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
pohromadě	pohromadě	k6eAd1	pohromadě
udržuje	udržovat	k5eAaImIp3nS	udržovat
gravitace	gravitace	k1gFnSc1	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdokupy	hvězdokupa	k1gFnPc1	hvězdokupa
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
základních	základní	k2eAgInPc2d1	základní
typů	typ	k1gInPc2	typ
<g/>
:	:	kIx,	:
otevřené	otevřený	k2eAgFnPc1d1	otevřená
a	a	k8xC	a
kulové	kulový	k2eAgFnPc1d1	kulová
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Plejády	Plejáda	k1gFnSc2	Plejáda
(	(	kIx(	(
<g/>
Kuřátka	kuřátko	k1gNnSc2	kuřátko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
viditelné	viditelný	k2eAgFnPc1d1	viditelná
i	i	k8xC	i
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Otevřené	otevřený	k2eAgFnPc1d1	otevřená
hvězdokupy	hvězdokupa	k1gFnPc1	hvězdokupa
==	==	k?	==
</s>
</p>
<p>
<s>
Otevřené	otevřený	k2eAgFnPc1d1	otevřená
hvězdokupy	hvězdokupa	k1gFnPc1	hvězdokupa
nemají	mít	k5eNaImIp3nP	mít
žádný	žádný	k3yNgInSc4	žádný
specifický	specifický	k2eAgInSc4d1	specifický
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
až	až	k6eAd1	až
několik	několik	k4yIc4	několik
stovek	stovka	k1gFnPc2	stovka
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hvězdy	hvězda	k1gFnPc1	hvězda
v	v	k7c6	v
otevřených	otevřený	k2eAgFnPc6d1	otevřená
hvězdokupách	hvězdokupa	k1gFnPc6	hvězdokupa
jsou	být	k5eAaImIp3nP	být
stejně	stejně	k6eAd1	stejně
staré	starý	k2eAgFnPc1d1	stará
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
tedy	tedy	k9	tedy
také	také	k9	také
společně	společně	k6eAd1	společně
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
důvodu	důvod	k1gInSc2	důvod
jsou	být	k5eAaImIp3nP	být
jasné	jasný	k2eAgFnPc4d1	jasná
hvězdy	hvězda	k1gFnPc4	hvězda
v	v	k7c6	v
otevřených	otevřený	k2eAgFnPc6d1	otevřená
hvězdokupách	hvězdokupa	k1gFnPc6	hvězdokupa
horké	horký	k2eAgFnSc2d1	horká
a	a	k8xC	a
bílé	bílý	k2eAgFnSc2d1	bílá
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
množství	množství	k1gNnSc1	množství
mlhovin	mlhovina	k1gFnPc2	mlhovina
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
třeba	třeba	k6eAd1	třeba
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Plejád	Plejáda	k1gFnPc2	Plejáda
<g/>
.	.	kIx.	.
</s>
<s>
Otevřené	otevřený	k2eAgFnPc1d1	otevřená
hvězdokupy	hvězdokupa	k1gFnPc1	hvězdokupa
bývají	bývat	k5eAaImIp3nP	bývat
ovlivněny	ovlivnit	k5eAaPmNgFnP	ovlivnit
gravitací	gravitace	k1gFnSc7	gravitace
okolních	okolní	k2eAgFnPc2d1	okolní
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nakonec	nakonec	k6eAd1	nakonec
způsobí	způsobit	k5eAaPmIp3nS	způsobit
rozpad	rozpad	k1gInSc1	rozpad
hvězdokupy	hvězdokupa	k1gFnSc2	hvězdokupa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kulové	kulový	k2eAgFnSc2d1	kulová
hvězdokupy	hvězdokupa	k1gFnSc2	hvězdokupa
==	==	k?	==
</s>
</p>
<p>
<s>
Kulové	kulový	k2eAgFnPc1d1	kulová
hvězdokupy	hvězdokupa	k1gFnPc1	hvězdokupa
jsou	být	k5eAaImIp3nP	být
odlišné	odlišný	k2eAgFnPc1d1	odlišná
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
zhruba	zhruba	k6eAd1	zhruba
kulový	kulový	k2eAgInSc4d1	kulový
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
je	být	k5eAaImIp3nS	být
až	až	k6eAd1	až
milion	milion	k4xCgInSc1	milion
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Kulové	kulový	k2eAgFnPc1d1	kulová
hvězdokupy	hvězdokupa	k1gFnPc1	hvězdokupa
měří	měřit	k5eAaImIp3nP	měřit
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
asi	asi	k9	asi
150	[number]	k4	150
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
v	v	k7c6	v
nich	on	k3xPp3gNnPc6	on
jsou	být	k5eAaImIp3nP	být
načervenalé	načervenalý	k2eAgInPc1d1	načervenalý
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	on	k3xPp3gMnPc4	on
tvoří	tvořit	k5eAaImIp3nP	tvořit
nejstarší	starý	k2eAgFnPc1d3	nejstarší
známé	známý	k2eAgFnPc1d1	známá
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kulové	kulový	k2eAgFnPc1d1	kulová
hvězdokupy	hvězdokupa	k1gFnPc1	hvězdokupa
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
naši	náš	k3xOp1gFnSc4	náš
Galaxii	galaxie	k1gFnSc4	galaxie
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
tak	tak	k9	tak
tzv.	tzv.	kA	tzv.
galaktické	galaktický	k2eAgNnSc1d1	Galaktické
halo	halo	k1gNnSc1	halo
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
známých	známý	k2eAgFnPc2d1	známá
kulových	kulový	k2eAgFnPc2d1	kulová
hvězdokup	hvězdokupa	k1gFnPc2	hvězdokupa
v	v	k7c6	v
Galaxii	galaxie	k1gFnSc6	galaxie
jsou	být	k5eAaImIp3nP	být
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
viditelné	viditelný	k2eAgFnSc2d1	viditelná
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgMnPc4	tři
<g/>
.	.	kIx.	.
</s>
<s>
Kulové	kulový	k2eAgFnPc1d1	kulová
hvězdokupy	hvězdokupa	k1gFnPc1	hvězdokupa
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
také	také	k9	také
cefeidy	cefeida	k1gFnPc1	cefeida
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
měřit	měřit	k5eAaImF	měřit
jejich	jejich	k3xOp3gFnSc4	jejich
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kdyby	kdyby	kYmCp3nS	kdyby
bylo	být	k5eAaImAgNnS	být
naše	náš	k3xOp1gNnSc1	náš
Slunce	slunce	k1gNnSc1	slunce
součástí	součást	k1gFnSc7	součást
kulové	kulový	k2eAgFnPc1d1	kulová
hvězdokupy	hvězdokupa	k1gFnPc1	hvězdokupa
<g/>
,	,	kIx,	,
blízké	blízký	k2eAgFnPc1d1	blízká
hvězdy	hvězda	k1gFnPc1	hvězda
by	by	kYmCp3nP	by
zářily	zářit	k5eAaImAgFnP	zářit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
obloha	obloha	k1gFnSc1	obloha
nebyla	být	k5eNaImAgFnS	být
nikdy	nikdy	k6eAd1	nikdy
temná	temný	k2eAgFnSc1d1	temná
<g/>
,	,	kIx,	,
předměty	předmět	k1gInPc1	předmět
by	by	kYmCp3nP	by
i	i	k9	i
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
vrhaly	vrhat	k5eAaImAgInP	vrhat
stíny	stín	k1gInPc1	stín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozložení	rozložení	k1gNnSc1	rozložení
hvězdokup	hvězdokupa	k1gFnPc2	hvězdokupa
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
Galaxii	galaxie	k1gFnSc6	galaxie
==	==	k?	==
</s>
</p>
<p>
<s>
Kulové	kulový	k2eAgFnPc1d1	kulová
hvězdokupy	hvězdokupa	k1gFnPc1	hvězdokupa
tvoří	tvořit	k5eAaImIp3nP	tvořit
kulové	kulový	k2eAgNnSc4d1	kulové
halo	halo	k1gNnSc4	halo
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
otevřené	otevřený	k2eAgFnPc1d1	otevřená
hvězdokupy	hvězdokupa	k1gFnPc1	hvězdokupa
jsou	být	k5eAaImIp3nP	být
roztroušeny	roztroušet	k5eAaImNgFnP	roztroušet
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
rovině	rovina	k1gFnSc6	rovina
galaktického	galaktický	k2eAgInSc2d1	galaktický
disku	disk	k1gInSc2	disk
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
mladé	mladý	k2eAgFnPc1d1	mladá
hvězdy	hvězda	k1gFnPc1	hvězda
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
ztotožnit	ztotožnit	k5eAaPmF	ztotožnit
s	s	k7c7	s
oblastmi	oblast	k1gFnPc7	oblast
plynu	plyn	k1gInSc2	plyn
a	a	k8xC	a
prachu	prach	k1gInSc2	prach
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
představují	představovat	k5eAaImIp3nP	představovat
rodiště	rodiště	k1gNnSc4	rodiště
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hvězdokupa	hvězdokupa	k1gFnSc1	hvězdokupa
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hvězdokupa	hvězdokupa	k1gFnSc1	hvězdokupa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Hvězdokupa	hvězdokupa	k1gFnSc1	hvězdokupa
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
