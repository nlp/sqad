<s>
Třetí	třetí	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
</s>
<s>
Richard	Richard	k1gMnSc1
Lví	lví	k2eAgNnSc4d1
srdce	srdce	k1gNnSc4
a	a	k8xC
Filip	Filip	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
August	August	k1gMnSc1
před	před	k7c7
hradbami	hradba	k1gFnPc7
Acré	Acrá	k1gFnSc2
(	(	kIx(
<g/>
obraz	obraz	k1gInSc1
Předání	předání	k1gNnSc2
Akkonu	Akkon	k1gMnSc3
Filipu	Filip	k1gMnSc3
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Merry-Joseph	Merry-Joseph	k1gInSc1
Blondel	blondel	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
1189	#num#	k4
<g/>
–	–	k?
<g/>
1192	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Levanta	Levanta	k1gFnSc1
<g/>
,	,	kIx,
Anatolie	Anatolie	k1gFnSc1
<g/>
,	,	kIx,
Kypr	Kypr	k1gInSc1
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Udržení	udržení	k1gNnSc1
křižáckých	křižácký	k2eAgFnPc2d1
držav	država	k1gFnPc2
v	v	k7c6
Palestině	Palestina	k1gFnSc6
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Anglie	Anglie	k1gFnSc1
a	a	k8xC
Plantagenetská	Plantagenetský	k2eAgFnSc1d1
Francie	Francie	k1gFnSc1
</s>
<s>
Anglie	Anglie	k1gFnSc1
</s>
<s>
Normandie	Normandie	k1gFnSc1
</s>
<s>
Akvitánie	Akvitánie	k1gFnSc1
</s>
<s>
Anjou	Anjou	k6eAd1
</s>
<s>
Poitou	Poiíst	k5eAaPmIp3nP
</s>
<s>
Vasconie	Vasconie	k1gFnSc1
</s>
<s>
Bretaň	Bretaň	k1gFnSc1
</s>
<s>
velští	velský	k2eAgMnPc1d1
křižáci	křižák	k1gMnPc1
</s>
<s>
skotští	skotský	k2eAgMnPc1d1
křižáci	křižák	k1gMnPc1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
korunní	korunní	k2eAgFnPc1d1
země	zem	k1gFnPc1
</s>
<s>
Burgundsko	Burgundsko	k1gNnSc1
</s>
<s>
Champagne	Champagnout	k5eAaImIp3nS,k5eAaPmIp3nS
</s>
<s>
Blois	Blois	k1gFnSc1
</s>
<s>
Flandry	Flandry	k1gInPc1
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1
říše	říše	k1gFnSc1
římská	římský	k2eAgFnSc1d1
Svatá	svatý	k2eAgFnSc1d1
říše	říše	k1gFnSc1
římská	římský	k2eAgFnSc1d1
</s>
<s>
Švábsko	Švábsko	k1gNnSc1
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Čechy	Čechy	k1gFnPc1
</s>
<s>
Durynsko	Durynsko	k1gNnSc1
</s>
<s>
Braniborsko	Braniborsko	k1gNnSc1
</s>
<s>
Montferrat	Montferrat	k1gMnSc1
</s>
<s>
Bádensko	Bádensko	k1gNnSc1
</s>
<s>
Holandsko	Holandsko	k1gNnSc1
</s>
<s>
Holštýnsko	Holštýnsko	k1gNnSc1
</s>
<s>
Mohuč	Mohuč	k1gFnSc1
</s>
<s>
Cagliari	Cagliari	k6eAd1
</s>
<s>
italští	italský	k2eAgMnPc1d1
křižáci	křižák	k1gMnPc1
</s>
<s>
Janovská	janovský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Pisánská	pisánský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Uhersko	Uhersko	k1gNnSc1
</s>
<s>
ostatní	ostatní	k2eAgMnPc1d1
křižáci	křižák	k1gMnPc1
</s>
<s>
Sicilané	Sicilan	k1gMnPc1
</s>
<s>
Dánové	Dán	k1gMnPc1
</s>
<s>
Norové	Nor	k1gMnPc1
</s>
<s>
křižácké	křižácký	k2eAgInPc1d1
státy	stát	k1gInPc1
Outremeru	Outremer	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
Jeruzalémské	jeruzalémský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Tripolis	Tripolis	k1gInSc1
</s>
<s>
Antiochijské	antiochijský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
</s>
<s>
rytířsko-vojenské	rytířsko-vojenský	k2eAgInPc4d1
řády	řád	k1gInPc4
<g/>
:	:	kIx,
</s>
<s>
Řád	řád	k1gInSc1
templářů	templář	k1gMnPc2
</s>
<s>
Řád	řád	k1gInSc1
johanitů	johanita	k1gMnPc2
</s>
<s>
německé	německý	k2eAgNnSc4d1
hospitální	hospitální	k2eAgNnSc4d1
bratrstvo	bratrstvo	k1gNnSc4
</s>
<s>
Řád	řád	k1gInSc1
Božího	boží	k2eAgInSc2d1
Hrobu	hrob	k1gInSc2
</s>
<s>
Řád	řád	k1gInSc1
sv.	sv.	kA
Lazara	Lazar	k1gMnSc2
</s>
<s>
Řád	řád	k1gInSc1
rytířů	rytíř	k1gMnPc2
sv.	sv.	kA
Tomáše	Tomáš	k1gMnSc4
</s>
<s>
východní	východní	k2eAgFnSc1d1
spojenci	spojenec	k1gMnPc7
<g/>
:	:	kIx,
</s>
<s>
Arménské	arménský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
v	v	k7c6
Kilíkii	Kilíkie	k1gFnSc6
</s>
<s>
sunnitští	sunnitský	k2eAgMnPc1d1
muslimové	muslim	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
Ajjúbovský	Ajjúbovský	k2eAgInSc1d1
sultanát	sultanát	k1gInSc1
</s>
<s>
Egyptský	egyptský	k2eAgInSc1d1
sultanát	sultanát	k1gInSc1
</s>
<s>
Damašský	damašský	k2eAgInSc1d1
emirát	emirát	k1gInSc1
</s>
<s>
Chamátský	Chamátský	k2eAgInSc1d1
emirát	emirát	k1gInSc1
(	(	kIx(
<g/>
Hamá	hamat	k5eAaImIp3nS
<g/>
)	)	kIx)
</s>
<s>
Mezopotámský	Mezopotámský	k2eAgInSc1d1
emirát	emirát	k1gInSc1
</s>
<s>
Rúmský	Rúmský	k2eAgInSc1d1
sultanát	sultanát	k1gInSc1
Rúmský	Rúmský	k2eAgInSc4d1
sultanát	sultanát	k1gInSc4
</s>
<s>
ší	ší	k?
<g/>
'	'	kIx"
<g/>
itští	itštit	k5eAaImIp3nS,k5eAaPmIp3nS
ismá	ismá	k1gFnSc1
<g/>
'	'	kIx"
<g/>
ílité	ílitý	k2eAgNnSc1d1
<g/>
:	:	kIx,
</s>
<s>
syrští	syrský	k2eAgMnPc1d1
asasíni	asasín	k1gMnPc1
</s>
<s>
křesťanští	křesťanský	k2eAgMnPc1d1
protivníci	protivník	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
Byzantská	byzantský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
Byzantská	byzantský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Kyperské	kyperský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Kyperské	kyperský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Richard	Richard	k1gMnSc1
Lví	lví	k2eAgNnSc4d1
srdce	srdce	k1gNnSc4
</s>
<s>
Filip	Filip	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
August	August	k1gMnSc1
</s>
<s>
Hugo	Hugo	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Theobald	Theobald	k1gMnSc1
V.	V.	kA
</s>
<s>
Filip	Filip	k1gMnSc1
I.	I.	kA
</s>
<s>
Jakub	Jakub	k1gMnSc1
z	z	k7c2
Avesnes	Avesnes	k1gMnSc1
†	†	k?
</s>
<s>
Hugo	Hugo	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
Robert	Robert	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
z	z	k7c2
Bar	bar	k1gInSc1
</s>
<s>
Érard	Érard	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
Barbarossa	Barbarossa	k1gMnSc1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
V.	V.	kA
</s>
<s>
Leopold	Leopold	k1gMnSc1
V.	V.	kA
</s>
<s>
Konrád	Konrád	k1gMnSc1
z	z	k7c2
Montferratu	Montferrat	k1gInSc2
</s>
<s>
Děpolt	Děpolt	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Albert	Albert	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Floris	Floris	k1gFnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Heřman	Heřman	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
Adolf	Adolf	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
ze	z	k7c2
Zähringenu	Zähringen	k1gInSc2
</s>
<s>
Vilém	Vilém	k1gMnSc1
z	z	k7c2
Cagliari	Cagliar	k1gFnSc2
</s>
<s>
Markvart	Markvart	k1gInSc1
z	z	k7c2
Annweileru	Annweiler	k1gInSc2
</s>
<s>
Gejza	Gejza	k1gFnSc1
Uherský	uherský	k2eAgMnSc1d1
</s>
<s>
Margaritus	Margaritus	k1gMnSc1
z	z	k7c2
Brindisi	Brindisi	k1gNnSc2
</s>
<s>
křižácké	křižácký	k2eAgInPc1d1
státy	stát	k1gInPc1
Outremeru	Outremer	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
Guy	Guy	k?
de	de	k?
Lusignan	Lusignan	k1gInSc1
</s>
<s>
Homfroi	Homfroi	k1gNnSc1
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Raimond	Raimond	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
Balian	Balian	k1gInSc1
z	z	k7c2
Ibelinu	Ibelin	k1gInSc2
</s>
<s>
Joscelin	Joscelina	k1gFnPc2
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Renaud	Renaud	k6eAd1
ze	z	k7c2
Châtillonu	Châtillon	k1gInSc2
</s>
<s>
rytířsko-vojenské	rytířsko-vojenský	k2eAgInPc4d1
řády	řád	k1gInPc4
<g/>
:	:	kIx,
</s>
<s>
Gérard	Gérard	k1gMnSc1
z	z	k7c2
Ridefortu	Ridefort	k1gInSc2
</s>
<s>
Robert	Robert	k1gMnSc1
ze	z	k7c2
Sablé	Sablý	k2eAgFnSc2d1
</s>
<s>
Hermangard	Hermangard	k1gInSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Asp	Asp	k1gMnSc1
</s>
<s>
Garnier	Garnier	k1gMnSc1
de	de	k?
Naplous	Naplous	k1gMnSc1
</s>
<s>
Sibrand	Sibrand	k1gInSc1
</s>
<s>
východní	východní	k2eAgFnSc1d1
spojenci	spojenec	k1gMnPc7
<g/>
:	:	kIx,
</s>
<s>
Lev	Lev	k1gMnSc1
Arménský	arménský	k2eAgMnSc1d1
</s>
<s>
Saladin	Saladin	k2eAgInSc1d1
</s>
<s>
Al-Ádil	Al-Ádit	k5eAaImAgMnS,k5eAaPmAgMnS
(	(	kIx(
<g/>
Safadin	Safadin	k2eAgInSc4d1
<g/>
)	)	kIx)
</s>
<s>
Al-Afdal	Al-Afdat	k5eAaImAgMnS,k5eAaPmAgMnS
</s>
<s>
Al-Muzaffar	Al-Muzaffar	k1gMnSc1
Umar	Umar	k1gMnSc1
</s>
<s>
Muzaffar	Muzaffar	k1gMnSc1
ad-Dín	ad-Dín	k1gMnSc1
Gökböri	Gökböre	k1gFnSc4
</s>
<s>
Asad	Asad	k1gMnSc1
ad-Dín	ad-Dín	k1gMnSc1
Šírkúh	Šírkúh	k1gMnSc1
</s>
<s>
Aladdin	Aladdin	k2eAgInSc1d1
Mosulský	mosulský	k2eAgInSc1d1
</s>
<s>
Musek	Musek	k1gMnSc1
<g/>
,	,	kIx,
velkoemír	velkoemír	k1gMnSc1
Kurdů	Kurd	k1gMnPc2
†	†	k?
</s>
<s>
Robert	Robert	k1gMnSc1
ze	z	k7c2
St.	st.	kA
Albans	Albans	k1gInSc1
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kilič	Kilič	k1gMnSc1
Arslan	Arslan	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Qutb	Qutb	k1gMnSc1
al-Dín	al-Dín	k1gMnSc1
</s>
<s>
Rašíd	Rašíd	k1gMnSc1
ad-Dín	ad-Dín	k1gMnSc1
Sinan	Sinan	k1gMnSc1
Izák	Izák	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Angelos	Angelos	k1gMnSc1
Izák	Izák	k1gMnSc1
Komnenos	Komnenos	k1gMnSc1
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
35	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
50	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
</s>
<s>
12	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
–	–	k?
<g/>
25	#num#	k4
000	#num#	k4
křižáků	křižák	k1gInPc2
ze	z	k7c2
států	stát	k1gInPc2
Svaté	svatý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
římské	římský	k2eAgFnSc2d1
</s>
<s>
8	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
–	–	k?
<g/>
9	#num#	k4
000	#num#	k4
Angličanů	Angličan	k1gMnPc2
<g/>
,	,	kIx,
Normanů	Norman	k1gMnPc2
<g/>
,	,	kIx,
<g/>
Velšanů	Velšan	k1gMnPc2
</s>
<s>
7	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
Francouzů	Francouz	k1gMnPc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2	#num#	k4
000	#num#	k4
Janovanůa	Janovanůum	k1gNnPc4
Pisánců	Pisánec	k1gMnPc2
</s>
<s>
2	#num#	k4
000	#num#	k4
Uhrů	Uher	k1gMnPc2
</s>
<s>
2	#num#	k4
000	#num#	k4
zkřižáckých	zkřižácký	k2eAgInPc2d1
států	stát	k1gInPc2
</s>
<s>
1	#num#	k4
000	#num#	k4
templářů	templář	k1gMnPc2
a	a	k8xC
johanitů	johanita	k1gMnPc2
</s>
<s>
2	#num#	k4
000	#num#	k4
ostatních	ostatní	k1gNnPc2
<g/>
(	(	kIx(
<g/>
Dánové	Dán	k1gMnPc1
<g/>
,	,	kIx,
Norové	Nor	k1gMnPc1
<g/>
,	,	kIx,
<g/>
turkopolové	turkopol	k1gMnPc1
aj.	aj.	kA
<g/>
)	)	kIx)
</s>
<s>
25	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
19	#num#	k4
300	#num#	k4
padlých	padlý	k1gMnPc2
</s>
<s>
10	#num#	k4
300	#num#	k4
padlých	padlý	k1gMnPc2
</s>
<s>
9	#num#	k4
000	#num#	k4
padlých	padlý	k1gMnPc2
</s>
<s>
Třetí	třetí	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
(	(	kIx(
<g/>
1189	#num#	k4
<g/>
–	–	k?
<g/>
1192	#num#	k4
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
velké	velký	k2eAgNnSc1d1
vojenské	vojenský	k2eAgNnSc1d1
tažení	tažení	k1gNnSc1
vyhlášené	vyhlášený	k2eAgNnSc1d1
papežem	papež	k1gMnSc7
Řehořem	Řehoř	k1gMnSc7
VIII	VIII	kA
<g/>
.	.	kIx.
za	za	k7c4
znovudobytí	znovudobytí	k1gNnSc4
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
roku	rok	k1gInSc2
1187	#num#	k4
dobyl	dobýt	k5eAaPmAgInS
Saladin	Saladin	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
fiasku	fiasko	k1gNnSc6
druhé	druhý	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
ovládla	ovládnout	k5eAaPmAgFnS
a	a	k8xC
sjednotila	sjednotit	k5eAaPmAgFnS
zengíovská	zengíovský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
Sýrii	Sýrie	k1gFnSc3
a	a	k8xC
po	po	k7c6
několika	několik	k4yIc6
válkách	válka	k1gFnPc6
s	s	k7c7
křižáky	křižák	k1gMnPc7
a	a	k8xC
Fátimovci	Fátimovec	k1gMnPc1
také	také	k6eAd1
Egypt	Egypt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zengíovské	Zengíovská	k1gFnSc6
impérium	impérium	k1gNnSc4
v	v	k7c4
70	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
postupně	postupně	k6eAd1
dobyl	dobýt	k5eAaPmAgInS
Saladin	Saladin	k2eAgInSc1d1
<g/>
,	,	kIx,
původně	původně	k6eAd1
vazal	vazal	k1gMnSc1
a	a	k8xC
vojevůdce	vojevůdce	k1gMnSc1
ve	v	k7c6
službách	služba	k1gFnPc6
Zengíovce	Zengíovec	k1gMnSc2
Núr	Núr	k1gMnSc2
ad-Dína	ad-Dín	k1gMnSc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
se	se	k3xPyFc4
palestinské	palestinský	k2eAgInPc1d1
křižácké	křižácký	k2eAgInPc1d1
státy	stát	k1gInPc1
dostaly	dostat	k5eAaPmAgInP
do	do	k7c2
sevření	sevření	k1gNnSc2
jednotného	jednotný	k2eAgInSc2d1
muslimského	muslimský	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sultán	sultána	k1gFnPc2
Saladin	Saladina	k1gFnPc2
poté	poté	k6eAd1
zahájil	zahájit	k5eAaPmAgMnS
válku	válka	k1gFnSc4
proti	proti	k7c3
křižákům	křižák	k1gMnPc3
<g/>
;	;	kIx,
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
tažení	tažení	k1gNnSc2
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS
roku	rok	k1gInSc2
1187	#num#	k4
vítěznou	vítězný	k2eAgFnSc7d1
bitvou	bitva	k1gFnSc7
u	u	k7c2
Hattínu	Hattín	k1gInSc2
a	a	k8xC
poté	poté	k6eAd1
dobytím	dobytí	k1gNnSc7
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Papež	Papež	k1gMnSc1
Řehoř	Řehoř	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
reagoval	reagovat	k5eAaBmAgMnS
vyhlášením	vyhlášení	k1gNnSc7
třetí	třetí	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
<g/>
,	,	kIx,
k	k	k7c3
níž	jenž	k3xRgFnSc3
se	se	k3xPyFc4
přihlásili	přihlásit	k5eAaPmAgMnP
i	i	k9
nejmocnější	mocný	k2eAgMnPc1d3
panovníci	panovník	k1gMnPc1
křesťanského	křesťanský	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
:	:	kIx,
římsko-německý	římsko-německý	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
Barbarossa	Barbarossa	k1gMnSc1
<g/>
,	,	kIx,
francouzský	francouzský	k2eAgMnSc1d1
král	král	k1gMnSc1
Filip	Filip	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
August	August	k1gMnSc1
a	a	k8xC
anglický	anglický	k2eAgMnSc1d1
král	král	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
Richard	Richard	k1gMnSc1
a	a	k8xC
s	s	k7c7
nimi	on	k3xPp3gMnPc7
i	i	k9
mnoho	mnoho	k4c1
dalších	další	k2eAgMnPc2d1
mocných	mocný	k2eAgMnPc2d1
evropských	evropský	k2eAgMnPc2d1
feudálů	feudál	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Německá	německý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
se	se	k3xPyFc4
vydala	vydat	k5eAaPmAgFnS
na	na	k7c4
cestu	cesta	k1gFnSc4
roku	rok	k1gInSc2
1188	#num#	k4
<g/>
,	,	kIx,
prošla	projít	k5eAaPmAgFnS
Uhrami	Uhry	k1gFnPc7
a	a	k8xC
Byzantskou	byzantský	k2eAgFnSc7d1
říší	říš	k1gFnSc7
a	a	k8xC
v	v	k7c6
květnu	květen	k1gInSc6
1190	#num#	k4
zničila	zničit	k5eAaPmAgFnS
seldžuckou	seldžucký	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
u	u	k7c2
Iconia	Iconium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
měsíc	měsíc	k1gInSc4
později	pozdě	k6eAd2
však	však	k9
císař	císař	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
utonul	utonout	k5eAaPmAgMnS
v	v	k7c6
říčce	říčka	k1gFnSc6
Salef	Salef	k1gInSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
výprava	výprava	k1gFnSc1
se	se	k3xPyFc4
rozpadla	rozpadnout	k5eAaPmAgFnS
<g/>
;	;	kIx,
část	část	k1gFnSc4
armády	armáda	k1gFnSc2
se	se	k3xPyFc4
rozhodla	rozhodnout	k5eAaPmAgFnS
pro	pro	k7c4
návrat	návrat	k1gInSc4
do	do	k7c2
Evropy	Evropa	k1gFnSc2
a	a	k8xC
jen	jen	k9
zlomek	zlomek	k1gInSc4
pod	pod	k7c7
velením	velení	k1gNnSc7
Barbarossova	Barbarossův	k2eAgMnSc2d1
syna	syn	k1gMnSc2
Fridricha	Fridrich	k1gMnSc2
Švábského	švábský	k2eAgMnSc2d1
dorazil	dorazit	k5eAaPmAgMnS
do	do	k7c2
Antiochie	Antiochie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1190	#num#	k4
vyrazili	vyrazit	k5eAaPmAgMnP
také	také	k9
Francouzi	Francouz	k1gMnPc1
a	a	k8xC
Angličané	Angličan	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
si	se	k3xPyFc3
zvolili	zvolit	k5eAaPmAgMnP
trasu	trasa	k1gFnSc4
do	do	k7c2
Palestiny	Palestina	k1gFnSc2
po	po	k7c6
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
přezimování	přezimování	k1gNnSc6
na	na	k7c6
Sicílii	Sicílie	k1gFnSc6
v	v	k7c6
polovině	polovina	k1gFnSc6
roku	rok	k1gInSc2
křižáci	křižák	k1gMnPc1
dorazili	dorazit	k5eAaPmAgMnP
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
;	;	kIx,
předtím	předtím	k6eAd1
ještě	ještě	k9
anglický	anglický	k2eAgMnSc1d1
král	král	k1gMnSc1
Richard	Richard	k1gMnSc1
stihl	stihnout	k5eAaPmAgMnS
dobýt	dobýt	k5eAaPmF
odpadlickou	odpadlický	k2eAgFnSc4d1
byzantskou	byzantský	k2eAgFnSc4d1
provincii	provincie	k1gFnSc4
Kypr	Kypr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evropané	Evropan	k1gMnPc1
se	se	k3xPyFc4
připojili	připojit	k5eAaPmAgMnP
k	k	k7c3
místním	místní	k2eAgMnPc3d1
křižáckým	křižácký	k2eAgMnPc3d1
baronům	baron	k1gMnPc3
a	a	k8xC
společně	společně	k6eAd1
s	s	k7c7
nimi	on	k3xPp3gInPc7
dobyli	dobýt	k5eAaPmAgMnP
mocné	mocný	k2eAgNnSc4d1
přístavní	přístavní	k2eAgNnSc4d1
město	město	k1gNnSc4
Akkon	Akkona	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
na	na	k7c6
dalších	další	k2eAgInPc6d1
sto	sto	k4xCgNnSc4
let	léto	k1gNnPc2
stalo	stát	k5eAaPmAgNnS
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
Jeruzalémského	jeruzalémský	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
Filip	Filip	k1gMnSc1
se	se	k3xPyFc4
poté	poté	k6eAd1
rozhodl	rozhodnout	k5eAaPmAgInS
pro	pro	k7c4
návrat	návrat	k1gInSc4
do	do	k7c2
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Richard	Richard	k1gMnSc1
Lví	lví	k2eAgNnSc4d1
srdce	srdce	k1gNnSc4
zůstal	zůstat	k5eAaPmAgMnS
po	po	k7c4
celý	celý	k2eAgInSc4d1
další	další	k2eAgInSc4d1
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
tažení	tažení	k1gNnSc1
bylo	být	k5eAaImAgNnS
korunováno	korunovat	k5eAaBmNgNnS
několika	několik	k4yIc7
úspěchy	úspěch	k1gInPc7
<g/>
,	,	kIx,
ale	ale	k8xC
znovu	znovu	k6eAd1
dobýt	dobýt	k5eAaPmF
Jeruzalém	Jeruzalém	k1gInSc4
se	se	k3xPyFc4
mu	on	k3xPp3gNnSc3
nepodařilo	podařit	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížová	Křížová	k1gFnSc1
výprava	výprava	k1gFnSc1
nakonec	nakonec	k6eAd1
skončila	skončit	k5eAaPmAgFnS
příměřím	příměří	k1gNnSc7
se	s	k7c7
Saladinem	Saladin	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Pád	Pád	k1gInSc1
Jeruzalémského	jeruzalémský	k2eAgNnSc2d1
království	království	k1gNnSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Bitva	bitva	k1gFnSc1
u	u	k7c2
Hattínu	Hattín	k1gInSc2
a	a	k8xC
Obléhání	obléhání	k1gNnSc2
Jeruzaléma	Jeruzalém	k1gInSc2
(	(	kIx(
<g/>
1187	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Hattínu	Hattín	k1gInSc2
<g/>
,	,	kIx,
obrázek	obrázek	k1gInSc1
ze	z	k7c2
středověkého	středověký	k2eAgInSc2d1
rukopisu	rukopis	k1gInSc2
</s>
<s>
Dne	den	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1187	#num#	k4
vojska	vojsko	k1gNnSc2
sultána	sultán	k1gMnSc2
Egypta	Egypt	k1gInSc2
a	a	k8xC
Sýrie	Sýrie	k1gFnSc1
Saladina	Saladina	k1gFnSc1
rozdrtila	rozdrtit	k5eAaPmAgFnS
jeruzalémské	jeruzalémský	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Hattínu	Hattín	k1gInSc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
zajala	zajmout	k5eAaPmAgFnS
hlavní	hlavní	k2eAgNnSc4d1
křesťanské	křesťanský	k2eAgNnSc4d1
velitele	velitel	k1gMnSc4
včetně	včetně	k7c2
jeruzalémského	jeruzalémský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Guye	Guy	k1gMnSc2
de	de	k?
Lusignan	Lusignan	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
zničeném	zničený	k2eAgNnSc6d1
vojsku	vojsko	k1gNnSc6
byla	být	k5eAaImAgFnS
přítomna	přítomen	k2eAgFnSc1d1
většina	většina	k1gFnSc1
posádek	posádka	k1gFnPc2
křižáckých	křižácký	k2eAgInPc2d1
hradů	hrad	k1gInPc2
a	a	k8xC
měst	město	k1gNnPc2
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
Palestině	Palestina	k1gFnSc6
tak	tak	k9
již	již	k6eAd1
nebyla	být	k5eNaImAgFnS
síla	síla	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
by	by	kYmCp3nP
Saladinovu	Saladinův	k2eAgFnSc4d1
invazi	invaze	k1gFnSc4
dokázala	dokázat	k5eAaPmAgFnS
zastavit	zastavit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
polovině	polovina	k1gFnSc6
září	září	k1gNnSc2
roku	rok	k1gInSc2
1187	#num#	k4
měl	mít	k5eAaImAgInS
již	již	k6eAd1
Saladin	Saladin	k2eAgMnSc1d1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
moci	moc	k1gFnSc6
města	město	k1gNnSc2
Akkon	Akkona	k1gFnPc2
<g/>
,	,	kIx,
Nábulus	Nábulus	k1gInSc4
<g/>
,	,	kIx,
Jaffu	Jaffa	k1gFnSc4
<g/>
,	,	kIx,
Sidón	Sidón	k1gInSc4
<g/>
,	,	kIx,
Bejrút	Bejrút	k1gInSc4
i	i	k8xC
Askalon	Askalon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
2	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
kapituloval	kapitulovat	k5eAaBmAgInS
i	i	k9
Jeruzalém	Jeruzalém	k1gInSc1
<g/>
,	,	kIx,
hájený	hájený	k2eAgInSc1d1
Balianem	Balian	k1gMnSc7
z	z	k7c2
Ibelinu	Ibelin	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Muslimům	muslim	k1gMnPc3
odolalo	odolat	k5eAaPmAgNnS
jen	jen	k9
několik	několik	k4yIc1
ohnisek	ohnisko	k1gNnPc2
odporu	odpor	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
své	svůj	k3xOyFgFnPc4
síly	síla	k1gFnPc4
soustředili	soustředit	k5eAaPmAgMnP
jeruzalémští	jeruzalémský	k2eAgMnPc1d1
baroni	baron	k1gMnPc1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
přístav	přístav	k1gInSc1
Týros	Týrosa	k1gFnPc2
a	a	k8xC
Tripolis	Tripolis	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Mnohé	mnohý	k2eAgInPc1d1
křižácké	křižácký	k2eAgInPc1d1
hrady	hrad	k1gInPc1
ve	v	k7c6
vnitrozemí	vnitrozemí	k1gNnSc6
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
Krak	Krak	k?
des	des	k1gNnSc1
Moabites	Moabites	k1gMnSc1
<g/>
,	,	kIx,
Belvoir	Belvoir	k1gMnSc1
či	či	k8xC
Krak	Krak	k?
de	de	k?
Montréal	Montréal	k1gInSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
postavily	postavit	k5eAaPmAgFnP
na	na	k7c4
odpor	odpor	k1gInSc4
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
obklíčeny	obklíčit	k5eAaPmNgInP
a	a	k8xC
obléhání	obléhání	k1gNnPc1
vzdorovaly	vzdorovat	k5eAaImAgFnP
několik	několik	k4yIc4
měsíců	měsíc	k1gInPc2
i	i	k8xC
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
v	v	k7c6
křižáckých	křižácký	k2eAgFnPc6d1
rukou	ruka	k1gFnPc6
zůstala	zůstat	k5eAaPmAgFnS
jen	jen	k6eAd1
Antiochie	Antiochie	k1gFnSc1
<g/>
,	,	kIx,
Tripolis	Tripolis	k1gInSc1
a	a	k8xC
Tyros	Tyros	k1gInSc1
a	a	k8xC
dále	daleko	k6eAd2
mocné	mocný	k2eAgInPc1d1
hrady	hrad	k1gInPc1
řádu	řád	k1gInSc2
johanitů	johanita	k1gMnPc2
–	–	k?
Krak	Krak	k?
des	des	k1gNnSc2
Chevaliers	Chevaliersa	k1gFnPc2
a	a	k8xC
Marqab	Marqaba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Přístav	přístav	k1gInSc1
Tyros	Tyrosa	k1gFnPc2
několikrát	několikrát	k6eAd1
Saladin	Saladin	k2eAgMnSc1d1
oblehl	oblehnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
neuspěl	uspět	k5eNaPmAgMnS
díky	díky	k7c3
skvělé	skvělý	k2eAgFnSc3d1
obraně	obrana	k1gFnSc3
italského	italský	k2eAgMnSc2d1
dobrodruha	dobrodruh	k1gMnSc2
Konráda	Konrád	k1gMnSc2
z	z	k7c2
Montferratu	Montferrat	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
do	do	k7c2
Tyru	Tyrus	k1gInSc2
dorazil	dorazit	k5eAaPmAgMnS
deset	deset	k4xCc1
dnů	den	k1gInPc2
po	po	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Hattínu	Hattín	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Konrád	Konrád	k1gMnSc1
převzal	převzít	k5eAaPmAgMnS
velení	velení	k1gNnSc4
nad	nad	k7c7
zbytky	zbytek	k1gInPc7
křižácké	křižácký	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
a	a	k8xC
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
úsilí	úsilí	k1gNnSc2
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS
2	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1188	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Saladinovy	Saladinův	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
obléhající	obléhající	k2eAgFnPc1d1
přístav	přístav	k1gInSc4
Tyros	Tyrosa	k1gFnPc2
obléhání	obléhání	k1gNnSc2
vzdaly	vzdát	k5eAaPmAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konrád	Konrád	k1gMnSc1
tak	tak	k9
pro	pro	k7c4
křižáky	křižák	k1gInPc4
udržel	udržet	k5eAaPmAgInS
hlavní	hlavní	k2eAgInSc1d1
palestinský	palestinský	k2eAgInSc1d1
opěrný	opěrný	k2eAgInSc1d1
bod	bod	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
muslimském	muslimský	k2eAgNnSc6d1
vítězství	vítězství	k1gNnSc6
se	se	k3xPyFc4
značně	značně	k6eAd1
změnila	změnit	k5eAaPmAgFnS
demografie	demografie	k1gFnSc1
Palestiny	Palestina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Města	město	k1gNnSc2
museli	muset	k5eAaImAgMnP
opustit	opustit	k5eAaPmF
křesťané	křesťan	k1gMnPc1
západního	západní	k2eAgInSc2d1
ritu	rit	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c4
jejich	jejich	k3xOp3gNnSc4
místo	místo	k1gNnSc4
začali	začít	k5eAaPmAgMnP
přicházet	přicházet	k5eAaImF
muslimové	muslim	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
židů	žid	k1gMnPc2
však	však	k9
směli	smět	k5eAaImAgMnP
zůstat	zůstat	k5eAaPmF
pravoslavní	pravoslavný	k2eAgMnPc1d1
křesťané	křesťan	k1gMnPc1
a	a	k8xC
syrští	syrský	k2eAgMnPc1d1
jakobité	jakobita	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
také	také	k9
tvořili	tvořit	k5eAaImAgMnP
menšinu	menšina	k1gFnSc4
křesťanského	křesťanský	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
na	na	k7c6
venkově	venkov	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
zůstával	zůstávat	k5eAaImAgInS
i	i	k9
za	za	k7c2
vlády	vláda	k1gFnSc2
křižáků	křižák	k1gInPc2
osídlen	osídlen	k2eAgInSc4d1
z	z	k7c2
větší	veliký	k2eAgFnSc2d2
části	část	k1gFnSc2
muslimy	muslim	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Přestože	přestože	k8xS
se	se	k3xPyFc4
Saladin	Saladin	k2eAgMnSc1d1
ocitl	ocitnout	k5eAaPmAgMnS
na	na	k7c6
vrcholu	vrchol	k1gInSc6
moci	moc	k1gFnSc2
<g/>
,	,	kIx,
prováděl	provádět	k5eAaImAgInS
na	na	k7c6
dobytých	dobytý	k2eAgNnPc6d1
územích	území	k1gNnPc6
velmi	velmi	k6eAd1
uvážlivou	uvážlivý	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
měla	mít	k5eAaImAgFnS
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
nejméně	málo	k6eAd3
narušit	narušit	k5eAaPmF
každodenní	každodenní	k2eAgInSc4d1
život	život	k1gInSc4
a	a	k8xC
vést	vést	k5eAaImF
k	k	k7c3
hospodářskému	hospodářský	k2eAgInSc3d1
rozkvětu	rozkvět	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obchodní	obchodní	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
zdejších	zdejší	k2eAgInPc2d1
přístavů	přístav	k1gInPc2
se	se	k3xPyFc4
západní	západní	k2eAgFnSc7d1
Evropou	Evropa	k1gFnSc7
zůstaly	zůstat	k5eAaPmAgInP
zachovány	zachovat	k5eAaPmNgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byzantský	byzantský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Izák	Izák	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
po	po	k7c6
dobytí	dobytí	k1gNnSc6
Jeruzaléma	Jeruzalém	k1gInSc2
Saladinovi	Saladin	k1gMnSc3
poblahopřál	poblahopřát	k5eAaPmAgMnS
a	a	k8xC
požádal	požádat	k5eAaPmAgMnS
ho	on	k3xPp3gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
svěřil	svěřit	k5eAaPmAgInS
svatá	svatý	k2eAgNnPc4d1
místa	místo	k1gNnPc4
do	do	k7c2
ochrany	ochrana	k1gFnSc2
východnímu	východní	k2eAgInSc3d1
kléru	klér	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Zároveň	zároveň	k6eAd1
mu	on	k3xPp3gMnSc3
nabídl	nabídnout	k5eAaPmAgInS
spojenectví	spojenectví	k1gNnSc4
proti	proti	k7c3
Latinům	Latin	k1gMnPc3
<g/>
,	,	kIx,
které	který	k3yQgMnPc4,k3yIgMnPc4,k3yRgMnPc4
Saladin	Saladin	k2eAgMnSc1d1
přijal	přijmout	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Když	když	k8xS
se	se	k3xPyFc4
o	o	k7c6
tomto	tento	k3xDgInSc6
paktu	pakt	k1gInSc6
dozvěděli	dozvědět	k5eAaPmAgMnP
Evropané	Evropan	k1gMnPc1
<g/>
,	,	kIx,
brali	brát	k5eAaImAgMnP
to	ten	k3xDgNnSc1
jako	jako	k9
zradu	zrada	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
Byzantincům	Byzantinec	k1gMnPc3
dlouho	dlouho	k6eAd1
nemohli	moct	k5eNaImAgMnP
zapomenout	zapomenout	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyhlášení	vyhlášení	k1gNnSc1
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
</s>
<s>
Po	po	k7c6
neúspěchu	neúspěch	k1gInSc6
druhé	druhý	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
ztratila	ztratit	k5eAaPmAgFnS
západní	západní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
na	na	k7c4
mnoho	mnoho	k4c4
let	léto	k1gNnPc2
o	o	k7c6
tažení	tažení	k1gNnSc6
na	na	k7c4
pomoc	pomoc	k1gFnSc4
křižáckým	křižácký	k2eAgInPc3d1
státům	stát	k1gInPc3
zájem	zájem	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Bojovná	bojovný	k2eAgFnSc1d1
šlechta	šlechta	k1gFnSc1
i	i	k8xC
panovníci	panovník	k1gMnPc1
nacházeli	nacházet	k5eAaImAgMnP
totiž	totiž	k9
dost	dost	k6eAd1
příležitostí	příležitost	k1gFnPc2
k	k	k7c3
válce	válka	k1gFnSc3
na	na	k7c6
evropské	evropský	k2eAgFnSc6d1
půdě	půda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
se	se	k3xPyFc4
němečtí	německý	k2eAgMnPc1d1
feudálové	feudál	k1gMnPc1
zajímali	zajímat	k5eAaImAgMnP
o	o	k7c4
výboje	výboj	k1gInPc4
do	do	k7c2
oblastí	oblast	k1gFnPc2
ležících	ležící	k2eAgFnPc2d1
za	za	k7c7
východními	východní	k2eAgFnPc7d1
hranicemi	hranice	k1gFnPc7
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
byly	být	k5eAaImAgFnP
nyní	nyní	k6eAd1
rovněž	rovněž	k9
vedeny	veden	k2eAgFnPc1d1
pod	pod	k7c7
heslem	heslo	k1gNnSc7
svaté	svatý	k2eAgFnSc2d1
války	válka	k1gFnSc2
s	s	k7c7
nevěřícími	nevěřící	k1gFnPc7
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
císař	císař	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
plně	plně	k6eAd1
zaměstnán	zaměstnat	k5eAaPmNgInS
italskou	italský	k2eAgFnSc7d1
expanzí	expanze	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
Anglií	Anglie	k1gFnSc7
a	a	k8xC
Francií	Francie	k1gFnSc7
probíhal	probíhat	k5eAaImAgMnS
vleklý	vleklý	k2eAgInSc4d1
konflikt	konflikt	k1gInSc4
o	o	k7c4
kontinentální	kontinentální	k2eAgNnPc4d1
léna	léno	k1gNnPc4
Plantagenetů	Plantagenet	k1gInPc2
<g/>
,	,	kIx,
komplikovaný	komplikovaný	k2eAgInSc1d1
sporem	spor	k1gInSc7
Jindřicha	Jindřich	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
s	s	k7c7
jeho	jeho	k3xOp3gMnPc7
syny	syn	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anglický	anglický	k2eAgInSc1d1
panovník	panovník	k1gMnSc1
však	však	k9
nezapomínal	zapomínat	k5eNaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
vnukem	vnuk	k1gMnSc7
někdejšího	někdejší	k2eAgMnSc2d1
jeruzalémského	jeruzalémský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Fulka	Fulek	k1gInSc2
z	z	k7c2
Anjou	Anja	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
posílal	posílat	k5eAaImAgMnS
do	do	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
štědré	štědrý	k2eAgInPc1d1
finanční	finanční	k2eAgInPc1d1
dary	dar	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Jeruzalémští	jeruzalémský	k2eAgMnPc1d1
vyslanci	vyslanec	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
navštívili	navštívit	k5eAaPmAgMnP
v	v	k7c6
letech	let	k1gInPc6
1184	#num#	k4
<g/>
–	–	k?
<g/>
1185	#num#	k4
dvory	dvůr	k1gInPc4
západoevropských	západoevropský	k2eAgMnPc2d1
vládců	vládce	k1gMnPc2
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
aby	aby	kYmCp3nP
varovali	varovat	k5eAaImAgMnP
před	před	k7c4
šířící	šířící	k2eAgInSc4d1
se	se	k3xPyFc4
Saladinovou	Saladinový	k2eAgFnSc7d1
mocí	moc	k1gFnSc7
a	a	k8xC
žádali	žádat	k5eAaImAgMnP
o	o	k7c4
pomoc	pomoc	k1gFnSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
vrátili	vrátit	k5eAaPmAgMnP
na	na	k7c4
Východ	východ	k1gInSc4
roztrpčeni	roztrpčit	k5eAaPmNgMnP
jejich	jejich	k3xOp3gFnSc7
lhostejností	lhostejnost	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Pozornost	pozornost	k1gFnSc1
vzbudily	vzbudit	k5eAaPmAgFnP
teprve	teprve	k6eAd1
zprávy	zpráva	k1gFnPc1
o	o	k7c6
katastrofální	katastrofální	k2eAgFnSc6d1
porážce	porážka	k1gFnSc6
křesťanů	křesťan	k1gMnPc2
u	u	k7c2
Hattínu	Hattín	k1gInSc2
a	a	k8xC
dobytí	dobytí	k1gNnSc4
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgNnPc1
přinášela	přinášet	k5eAaImAgNnP
nová	nový	k2eAgNnPc1d1
poselstva	poselstvo	k1gNnPc1
i	i	k8xC
uprchlíci	uprchlík	k1gMnPc1
vracející	vracející	k2eAgFnSc2d1
se	se	k3xPyFc4
ze	z	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
na	na	k7c4
Západ	západ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Křižácké	křižácký	k2eAgInPc1d1
státy	stát	k1gInPc1
v	v	k7c6
Levantě	Levanta	k1gFnSc6
roku	rok	k1gInSc2
1190	#num#	k4
</s>
<s>
Zprávy	zpráva	k1gFnPc1
o	o	k7c6
porážce	porážka	k1gFnSc6
křesťanů	křesťan	k1gMnPc2
u	u	k7c2
Hattínu	Hattín	k1gInSc2
otřásly	otřást	k5eAaPmAgFnP
Evropou	Evropa	k1gFnSc7
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
říká	říkat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
papež	papež	k1gMnSc1
Urban	Urban	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
zemřel	zemřít	k5eAaPmAgMnS
žalem	žal	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gMnSc1
nástupce	nástupce	k1gMnSc1
Řehoř	Řehoř	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
vydal	vydat	k5eAaPmAgInS
29	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1187	#num#	k4
bulu	bula	k1gFnSc4
Audita	Audita	k1gMnSc1
tremendi	tremendit	k5eAaPmRp2nS
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc3
vyhlásil	vyhlásit	k5eAaPmAgInS
třetí	třetí	k4xOgFnSc4
křížovou	křížový	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
a	a	k8xC
podnítil	podnítit	k5eAaPmAgInS
křižácká	křižácký	k2eAgNnPc4d1
kázání	kázání	k1gNnSc4
v	v	k7c6
německých	německý	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
své	svůj	k3xOyFgFnSc6
bule	bula	k1gFnSc6
papež	papež	k1gMnSc1
vyzval	vyzvat	k5eAaPmAgMnS
evropské	evropský	k2eAgMnPc4d1
panovníky	panovník	k1gMnPc4
k	k	k7c3
sedmiletému	sedmiletý	k2eAgNnSc3d1
příměří	příměří	k1gNnSc3
a	a	k8xC
podle	podle	k7c2
spisů	spis	k1gInPc2
Bernarda	Bernard	k1gMnSc2
z	z	k7c2
Clairvaux	Clairvaux	k1gInSc1
dodal	dodat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
úspěch	úspěch	k1gInSc1
výpravy	výprava	k1gFnSc2
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
jejím	její	k3xOp3gInSc6
duchovním	duchovní	k2eAgInSc6d1
rozměru	rozměr	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Papežským	papežský	k2eAgMnPc3d1
diplomatům	diplomat	k1gMnPc3
se	se	k3xPyFc4
dokonce	dokonce	k9
podařilo	podařit	k5eAaPmAgNnS
utlumit	utlumit	k5eAaPmF
konflikt	konflikt	k1gInSc4
mezi	mezi	k7c7
francouzským	francouzský	k2eAgMnSc7d1
králem	král	k1gMnSc7
Filipem	Filip	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
<g />
.	.	kIx.
</s>
<s hack="1">
anglickým	anglický	k2eAgInSc7d1
Jidřichem	Jidřich	k1gInSc7
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
tak	tak	k6eAd1
21	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1188	#num#	k4
podepsali	podepsat	k5eAaPmAgMnP
mírovou	mírový	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
získat	získat	k5eAaPmF
je	on	k3xPp3gMnPc4
pro	pro	k7c4
křížovou	křížový	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Sicilský	sicilský	k2eAgMnSc1d1
král	král	k1gMnSc1
Vilém	Vilém	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
si	se	k3xPyFc3
sice	sice	k8xC
uvědomoval	uvědomovat	k5eAaImAgMnS
důležitost	důležitost	k1gFnSc4
rychlého	rychlý	k2eAgInSc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
zásahu	zásah	k1gInSc2
ve	v	k7c4
prospěch	prospěch	k1gInSc4
palestinských	palestinský	k2eAgMnPc2d1
křižáků	křižák	k1gMnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
byl	být	k5eAaImAgInS
však	však	k9
od	od	k7c2
roku	rok	k1gInSc2
1185	#num#	k4
zapleten	zaplést	k5eAaPmNgMnS
ve	v	k7c6
válce	válka	k1gFnSc6
s	s	k7c7
Byzancí	Byzanc	k1gFnSc7
v	v	k7c6
Řecku	Řecko	k1gNnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
musel	muset	k5eAaImAgMnS
proto	proto	k8xC
nejprve	nejprve	k6eAd1
s	s	k7c7
císařem	císař	k1gMnSc7
Izákem	Izák	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
uzavřít	uzavřít	k5eAaPmF
příměří	příměří	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
19	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
jaře	jaro	k1gNnSc6
1188	#num#	k4
na	na	k7c4
Východ	východ	k1gInSc4
dorazilo	dorazit	k5eAaPmAgNnS
normanské	normanský	k2eAgNnSc1d1
loďstvo	loďstvo	k1gNnSc1
s	s	k7c7
dvěma	dva	k4xCgNnPc7
sty	sto	k4xCgNnPc7
rytíři	rytíř	k1gMnSc3
na	na	k7c6
palubě	paluba	k1gFnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
významným	významný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
podílela	podílet	k5eAaImAgFnS
na	na	k7c6
obraně	obrana	k1gFnSc6
Tyru	Tyrus	k1gInSc2
a	a	k8xC
Tripolisu	Tripolis	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Jenže	jenže	k8xC
<g />
.	.	kIx.
</s>
<s hack="1">
již	již	k6eAd1
následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
Vilém	Vilém	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
zemřel	zemřít	k5eAaPmAgMnS
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
armáda	armáda	k1gFnSc1
se	se	k3xPyFc4
musela	muset	k5eAaImAgFnS
kvůli	kvůli	k7c3
politické	politický	k2eAgFnSc3d1
nestabilitě	nestabilita	k1gFnSc3
na	na	k7c6
Sicílii	Sicílie	k1gFnSc6
vrátit	vrátit	k5eAaPmF
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
sicilští	sicilský	k2eAgMnPc1d1
Normani	Norman	k1gMnPc1
tak	tak	k6eAd1
již	již	k6eAd1
na	na	k7c6
výpravě	výprava	k1gFnSc6
nehráli	hrát	k5eNaImAgMnP
významnější	významný	k2eAgFnSc4d2
úlohu	úloha	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Dokázali	dokázat	k5eAaPmAgMnP
je	on	k3xPp3gInPc4
nahradit	nahradit	k5eAaPmF
křižáci	křižák	k1gMnPc1
z	z	k7c2
Dánska	Dánsko	k1gNnSc2
a	a	k8xC
Anglie	Anglie	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
na	na	k7c4
vlastní	vlastní	k2eAgFnSc4d1
pěst	pěst	k1gFnSc4
dorazili	dorazit	k5eAaPmAgMnP
do	do	k7c2
východního	východní	k2eAgNnSc2d1
Středomoří	středomoří	k1gNnSc2
ze	z	k7c2
Severního	severní	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Pomohla	pomoct	k5eAaPmAgFnS
i	i	k8xC
loďstva	loďstvo	k1gNnPc1
italských	italský	k2eAgFnPc2d1
republik	republika	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
zásobovala	zásobovat	k5eAaImAgFnS
obránce	obránce	k1gMnPc4
posilami	posila	k1gFnPc7
i	i	k8xC
materiálem	materiál	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1188	#num#	k4
Řehoř	Řehoř	k1gMnSc1
VIII	VIII	kA
<g/>
.	.	kIx.
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
udělil	udělit	k5eAaPmAgInS
každé	každý	k3xTgFnPc4
ze	z	k7c2
zúčastněných	zúčastněný	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
vlastní	vlastní	k2eAgFnSc4d1
variantu	varianta	k1gFnSc4
svatojiřské	svatojiřský	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
s	s	k7c7
křížem	kříž	k1gInSc7
vyznačující	vyznačující	k2eAgFnSc2d1
se	se	k3xPyFc4
každá	každý	k3xTgFnSc1
odlišnou	odlišný	k2eAgFnSc7d1
barvou	barva	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kronika	kronika	k1gFnSc1
angličana	angličan	k1gMnSc2
Rogera	Roger	k1gMnSc2
z	z	k7c2
Howdenu	Howden	k1gInSc2
zmiňuje	zmiňovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
tři	tři	k4xCgInPc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
konkrétně	konkrétně	k6eAd1
zelený	zelený	k2eAgInSc1d1
kříž	kříž	k1gInSc1
pro	pro	k7c4
Flandry	Flandry	k1gInPc4
<g/>
,	,	kIx,
červený	červený	k2eAgInSc4d1
pro	pro	k7c4
Francii	Francie	k1gFnSc4
(	(	kIx(
<g/>
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
už	už	k6eAd1
Francouzi	Francouz	k1gMnPc1
použili	použít	k5eAaPmAgMnP
na	na	k7c6
předchozích	předchozí	k2eAgFnPc6d1
výpravách	výprava	k1gFnPc6
<g/>
)	)	kIx)
a	a	k8xC
bílý	bílý	k2eAgInSc1d1
na	na	k7c6
červeném	červený	k2eAgInSc6d1
podkladě	podklad	k1gInSc6
pro	pro	k7c4
Anglii	Anglie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
legendy	legenda	k1gFnSc2
Řehořův	Řehořův	k2eAgMnSc1d1
nástupce	nástupce	k1gMnSc1
Klement	Klement	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
udělil	udělit	k5eAaPmAgInS
bílý	bílý	k2eAgInSc1d1
ondřejský	ondřejský	k2eAgInSc1d1
kříž	kříž	k1gInSc1
na	na	k7c6
červeném	červený	k2eAgInSc6d1
podkladě	podklad	k1gInSc6
Gaskoňsku	Gaskoňsko	k1gNnSc6
<g/>
,	,	kIx,
zřejmě	zřejmě	k6eAd1
na	na	k7c6
základě	základ	k1gInSc6
patronace	patronace	k1gFnSc2
světce	světec	k1gMnSc2
nad	nad	k7c7
městem	město	k1gNnSc7
Bordeaux	Bordeaux	k1gNnPc2
a	a	k8xC
barvami	barva	k1gFnPc7
shodnými	shodný	k2eAgInPc7d1
s	s	k7c7
Anglií	Anglie	k1gFnSc7
<g/>
,	,	kIx,
k	k	k7c3
níž	jenž	k3xRgFnSc3
země	zem	k1gFnSc2
náležela	náležet	k5eAaImAgFnS
jako	jako	k9
léno	léno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
Bretaně	Bretaň	k1gFnSc2
se	se	k3xPyFc4
vžil	vžít	k5eAaPmAgInS
černý	černý	k2eAgInSc1d1
kříž	kříž	k1gInSc1
a	a	k8xC
žlutá	žlutý	k2eAgFnSc1d1
varianta	varianta	k1gFnSc1
náležela	náležet	k5eAaImAgFnS
zřejmě	zřejmě	k6eAd1
Italům	Ital	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaké	jaký	k3yQgFnSc2,k3yIgFnSc2,k3yRgFnSc2
barvy	barva	k1gFnSc2
charakterizovaly	charakterizovat	k5eAaBmAgFnP
nejpočetnější	početní	k2eAgMnPc4d3
účastníky	účastník	k1gMnPc4
výpravy	výprava	k1gFnSc2
<g/>
,	,	kIx,
tedy	tedy	k9
Němce	Němec	k1gMnPc4
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
zřejmé	zřejmý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohl	moct	k5eAaImAgMnS
to	ten	k3xDgNnSc4
být	být	k5eAaImF
jako	jako	k9
u	u	k7c2
Bretonců	Bretonec	k1gMnPc2
černý	černý	k2eAgInSc4d1
kříž	kříž	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
tak	tak	k6eAd1
stal	stát	k5eAaPmAgInS
předlohou	předloha	k1gFnSc7
Řádu	řád	k1gInSc2
německých	německý	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
pro	pro	k7c4
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
vlastní	vlastní	k2eAgInSc4d1
znak	znak	k1gInSc4
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
bílý	bílý	k2eAgInSc1d1
kříž	kříž	k1gInSc1
na	na	k7c6
červeném	červený	k2eAgInSc6d1
(	(	kIx(
<g/>
jako	jako	k9
u	u	k7c2
Angličanů	Angličan	k1gMnPc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
v	v	k7c6
průběhu	průběh	k1gInSc6
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
zaveden	zavést	k5eAaPmNgInS
v	v	k7c6
podobě	podoba	k1gFnSc6
císařského	císařský	k2eAgInSc2d1
vojenského	vojenský	k2eAgInSc2d1
praporce	praporec	k1gInSc2
(	(	kIx(
<g/>
Reichsrennfahne	Reichsrennfahne	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
avšak	avšak	k8xC
podle	podle	k7c2
jiného	jiný	k2eAgInSc2d1
zdroje	zdroj	k1gInSc2
nesl	nést	k5eAaImAgMnS
císař	císař	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
žlutý	žlutý	k2eAgInSc4d1
kříž	kříž	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzi	Francouz	k1gMnPc1
i	i	k8xC
Angličané	Angličan	k1gMnPc1
pokračovali	pokračovat	k5eAaImAgMnP
v	v	k7c6
užívání	užívání	k1gNnSc6
křížů	kříž	k1gInPc2
i	i	k9
nadále	nadále	k6eAd1
a	a	k8xC
teprve	teprve	k6eAd1
na	na	k7c6
začátku	začátek	k1gInSc6
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
si	se	k3xPyFc3
je	on	k3xPp3gNnSc4
měli	mít	k5eAaImAgMnP
vzájemně	vzájemně	k6eAd1
prohodit	prohodit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
zástavy	zástava	k1gFnPc1
účastníků	účastník	k1gMnPc2
třetí	třetí	k4xOgFnSc3
kruciáty	kruciáta	k1gFnPc5
</s>
<s>
Anglie	Anglie	k1gFnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Flandry	Flandry	k1gInPc1
</s>
<s>
Vasconie	Vasconie	k1gFnSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Bretaň	Bretaň	k1gFnSc1
</s>
<s>
Tažení	tažení	k1gNnSc1
Fridricha	Fridrich	k1gMnSc2
Barbarossy	Barbarossa	k1gMnSc2
</s>
<s>
Jako	jako	k9
první	první	k4xOgMnSc1
evropský	evropský	k2eAgMnSc1d1
panovník	panovník	k1gMnSc1
vytáhl	vytáhnout	k5eAaPmAgMnS
na	na	k7c4
křížovou	křížový	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
Fridrich	Fridrich	k1gMnSc1
Barbarossa	Barbarossa	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
přijal	přijmout	k5eAaPmAgMnS
znamení	znamení	k1gNnSc4
kříže	kříž	k1gInSc2
na	na	k7c6
slavnostním	slavnostní	k2eAgInSc6d1
sněmu	sněm	k1gInSc6
v	v	k7c6
Mohuči	Mohuč	k1gFnSc6
27	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1188	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Jelikož	jelikož	k8xS
se	se	k3xPyFc4
Fridrich	Fridrich	k1gMnSc1
účastnil	účastnit	k5eAaImAgMnS
již	již	k9
druhé	druhý	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
<g/>
,	,	kIx,
věděl	vědět	k5eAaImAgMnS
jak	jak	k8xS,k8xC
obtížný	obtížný	k2eAgMnSc1d1
bude	být	k5eAaImBp3nS
pochod	pochod	k1gInSc1
na	na	k7c4
Blízký	blízký	k2eAgInSc4d1
<g />
.	.	kIx.
</s>
<s hack="1">
východ	východ	k1gInSc1
přes	přes	k7c4
Anatolii	Anatolie	k1gFnSc4
za	za	k7c4
cenu	cena	k1gFnSc4
co	co	k9
nejmenších	malý	k2eAgFnPc2d3
možných	možný	k2eAgFnPc2d1
ztrát	ztráta	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Císař	Císař	k1gMnSc1
se	se	k3xPyFc4
proto	proto	k8xC
na	na	k7c6
tažení	tažení	k1gNnSc6
důkladně	důkladně	k6eAd1
připravoval	připravovat	k5eAaImAgMnS
<g/>
;	;	kIx,
křižákům	křižák	k1gMnPc3
nařídil	nařídit	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
finančně	finančně	k6eAd1
zabezpečili	zabezpečit	k5eAaPmAgMnP
na	na	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
pochodů	pochod	k1gInPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
vyjednat	vyjednat	k5eAaPmF
volný	volný	k2eAgInSc4d1
průchod	průchod	k1gInSc4
své	svůj	k3xOyFgFnSc2
armády	armáda	k1gFnSc2
s	s	k7c7
vládci	vládce	k1gMnPc7
<g />
.	.	kIx.
</s>
<s hack="1">
území	území	k1gNnSc1
<g/>
,	,	kIx,
jimiž	jenž	k3xRgNnPc7
hodlal	hodlat	k5eAaImAgMnS
protáhnout	protáhnout	k5eAaPmF
<g/>
,	,	kIx,
tedy	tedy	k8xC
Uherskem	Uhersko	k1gNnSc7
<g/>
,	,	kIx,
Srbskem	Srbsko	k1gNnSc7
<g/>
,	,	kIx,
Byzancí	Byzanc	k1gFnSc7
i	i	k8xC
Ikonyjským	Ikonyjský	k2eAgInSc7d1
sultanátem	sultanát	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
U	u	k7c2
uherského	uherský	k2eAgMnSc2d1
krále	král	k1gMnSc2
a	a	k8xC
byzantského	byzantský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
se	se	k3xPyFc4
Fridrichovi	Fridrich	k1gMnSc3
dostalo	dostat	k5eAaPmAgNnS
podpory	podpora	k1gFnSc2
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
císař	císař	k1gMnSc1
Izák	Izák	k1gMnSc1
byl	být	k5eAaImAgMnS
vůči	vůči	k7c3
<g />
.	.	kIx.
</s>
<s hack="1">
Fridrichovi	Fridrichův	k2eAgMnPc5d1
podezřívavý	podezřívavý	k2eAgInSc1d1
kvůli	kvůli	k7c3
nedávnému	dávný	k2eNgNnSc3d1
uzavření	uzavření	k1gNnSc3
spojenectví	spojenectví	k1gNnSc2
se	s	k7c7
sicilskými	sicilský	k2eAgMnPc7d1
Normany	Norman	k1gMnPc7
<g/>
,	,	kIx,
nepřáteli	nepřítel	k1gMnPc7
Byzance	Byzanc	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
sultán	sultán	k1gMnSc1
Kilič	Kilič	k1gMnSc1
Arslan	Arslan	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
žádost	žádost	k1gFnSc4
neodpověděl	odpovědět	k5eNaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
Fridrich	Fridrich	k1gMnSc1
Barbarossa	Barbarossa	k1gMnSc1
zaslal	zaslat	k5eAaPmAgMnS
dopis	dopis	k1gInSc4
také	také	k9
Saladinovi	Saladinův	k2eAgMnPc1d1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
mu	on	k3xPp3gNnSc3
<g />
.	.	kIx.
</s>
<s hack="1">
poslal	poslat	k5eAaPmAgMnS
zdvořilou	zdvořilý	k2eAgFnSc4d1
odpověď	odpověď	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
Barbarossovi	Barbarossa	k1gMnSc3
dal	dát	k5eAaPmAgMnS
najevo	najevo	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
příchodu	příchod	k1gInSc2
nebojí	bát	k5eNaImIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Německá	německý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
se	se	k3xPyFc4
shromažďovala	shromažďovat	k5eAaImAgFnS
v	v	k7c6
Řeznu	Řezno	k1gNnSc6
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
podle	podle	k7c2
mínění	mínění	k1gNnSc2
současníků	současník	k1gMnPc2
měla	mít	k5eAaImAgFnS
100	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
150	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
včetně	včetně	k7c2
20	#num#	k4
000	#num#	k4
rytířů	rytíř	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Toto	tento	k3xDgNnSc1
číslo	číslo	k1gNnSc1
je	být	k5eAaImIp3nS
značně	značně	k6eAd1
přehnané	přehnaný	k2eAgNnSc1d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
německá	německý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
mohla	moct	k5eAaImAgFnS
čítat	čítat	k5eAaImF
asi	asi	k9
15	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
z	z	k7c2
toho	ten	k3xDgNnSc2
3000	#num#	k4
rytířů	rytíř	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c4
dobu	doba	k1gFnSc4
císařovy	císařův	k2eAgFnSc2d1
nepřítomnosti	nepřítomnost	k1gFnSc2
v	v	k7c6
říši	říš	k1gFnSc6
byl	být	k5eAaImAgMnS
ustanoven	ustanoven	k2eAgMnSc1d1
Fridrichův	Fridrichův	k2eAgMnSc1d1
nejstarší	starý	k2eAgMnSc1d3
syn	syn	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
regentem	regens	k1gMnSc7
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
jeho	jeho	k3xOp3gFnSc1
pozice	pozice	k1gFnSc1
byla	být	k5eAaImAgFnS
vratká	vratký	k2eAgFnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
Svatá	svatý	k2eAgFnSc1d1
říše	říše	k1gFnSc1
římská	římský	k2eAgFnSc1d1
nebyla	být	k5eNaImAgFnS
monarchií	monarchie	k1gFnSc7
dědičnou	dědičný	k2eAgFnSc7d1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
nýbrž	nýbrž	k8xC
volitelnou	volitelný	k2eAgFnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
Jindřich	Jindřich	k1gMnSc1
tedy	tedy	k9
nemohl	moct	k5eNaImAgMnS
být	být	k5eAaImF
Fridrichem	Fridrich	k1gMnSc7
prohlášen	prohlásit	k5eAaPmNgMnS
za	za	k7c4
jasného	jasný	k2eAgMnSc4d1
nástupce	nástupce	k1gMnSc4
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
Barbarossa	Barbarossa	k1gMnSc1
snažil	snažit	k5eAaImAgMnS
nejistou	jistý	k2eNgFnSc4d1
situaci	situace	k1gFnSc4
zmírnit	zmírnit	k5eAaPmF
utlumením	utlumení	k1gNnSc7
domácí	domácí	k2eAgFnSc2d1
opozice	opozice	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
nechal	nechat	k5eAaPmAgMnS
poslat	poslat	k5eAaPmF
do	do	k7c2
vyhnanství	vyhnanství	k1gNnSc2
svého	svůj	k3xOyFgMnSc2
největšího	veliký	k2eAgMnSc2d3
rivala	rival	k1gMnSc2
<g/>
,	,	kIx,
bývalého	bývalý	k2eAgMnSc4d1
saského	saský	k2eAgMnSc4d1
vévodu	vévoda	k1gMnSc4
Jindřicha	Jindřich	k1gMnSc4
Lva	Lev	k1gMnSc4
z	z	k7c2
rodu	rod	k1gInSc2
Welfů	Welf	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
Barbarossa	Barbarossa	k1gMnSc1
vyobrazen	vyobrazen	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
křižák	křižák	k1gInSc1
<g/>
,	,	kIx,
rukopis	rukopis	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1188	#num#	k4
<g/>
,	,	kIx,
Vatikánská	vatikánský	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
</s>
<s>
Křižácké	křižácký	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
vedené	vedený	k2eAgFnSc2d1
osobně	osobně	k6eAd1
starým	starý	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
se	se	k3xPyFc4
vydalo	vydat	k5eAaPmAgNnS
z	z	k7c2
Řezna	Řezno	k1gNnSc2
na	na	k7c4
cestu	cesta	k1gFnSc4
na	na	k7c4
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1189	#num#	k4
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
byli	být	k5eAaImAgMnP
v	v	k7c6
něm	on	k3xPp3gInSc6
přítomni	přítomen	k2eAgMnPc1d1
velmoži	velmož	k1gMnPc1
z	z	k7c2
celé	celý	k2eAgFnSc2d1
Svaté	svatý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
římské	římský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc1
součástí	součást	k1gFnSc7
byl	být	k5eAaImAgInS
i	i	k9
český	český	k2eAgInSc1d1
oddíl	oddíl	k1gInSc1
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
knížetem	kníže	k1gMnSc7
Děpoltem	Děpolt	k1gInSc7
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barbarossovy	Barbarossův	k2eAgFnPc4d1
účasti	účast	k1gFnPc4
na	na	k7c6
výpravě	výprava	k1gFnSc6
se	se	k3xPyFc4
pokusili	pokusit	k5eAaPmAgMnP
využít	využít	k5eAaPmF
panovníci	panovník	k1gMnPc1
slovanských	slovanský	k2eAgInPc2d1
států	stát	k1gInPc2
nedávno	nedávno	k6eAd1
se	se	k3xPyFc4
osvobodivších	osvobodivší	k2eAgFnPc2d1
zpod	zpod	k7c2
byzantské	byzantský	k2eAgFnSc2d1
nadvlády	nadvláda	k1gFnSc2
<g/>
,	,	kIx,
Srbska	Srbsko	k1gNnSc2
a	a	k8xC
Bulharska	Bulharsko	k1gNnSc2
<g/>
,	,	kIx,
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
ho	on	k3xPp3gMnSc4
získali	získat	k5eAaPmAgMnP
za	za	k7c4
spojence	spojenec	k1gMnPc4
proti	proti	k7c3
Byzanci	Byzanc	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srbský	srbský	k2eAgInSc1d1
župan	župan	k1gInSc1
Štěpán	Štěpán	k1gMnSc1
Nemanja	Nemanja	k1gMnSc1
vypravil	vypravit	k5eAaPmAgMnS
za	za	k7c7
tím	ten	k3xDgInSc7
účelem	účel	k1gInSc7
roku	rok	k1gInSc2
1188	#num#	k4
poselstvo	poselstvo	k1gNnSc4
do	do	k7c2
Norimberka	Norimberk	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
Barbarossa	Barbarossa	k1gMnSc1
jeho	jeho	k3xOp3gFnSc4
nabídku	nabídka	k1gFnSc4
odmítl	odmítnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Císař	Císař	k1gMnSc1
Izák	Izák	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
naopak	naopak	k6eAd1
doufal	doufat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
křižáky	křižák	k1gInPc4
<g/>
,	,	kIx,
jichž	jenž	k3xRgInPc2
se	se	k3xPyFc4
v	v	k7c6
Byzanci	Byzanc	k1gFnSc6
obávali	obávat	k5eAaImAgMnP
více	hodně	k6eAd2
než	než	k8xS
Saladinových	Saladinový	k2eAgMnPc2d1
bojovníků	bojovník	k1gMnPc2
<g/>
,	,	kIx,
oslabí	oslabit	k5eAaPmIp3nS
během	během	k7c2
tažení	tažení	k1gNnSc2
Balkánem	Balkán	k1gInSc7
konflikty	konflikt	k1gInPc1
s	s	k7c7
místním	místní	k2eAgNnSc7d1
obyvatelstvem	obyvatelstvo	k1gNnSc7
a	a	k8xC
záměrně	záměrně	k6eAd1
nedodržoval	dodržovat	k5eNaImAgMnS
dohody	dohoda	k1gFnPc4
o	o	k7c4
zásobování	zásobování	k1gNnSc4
vojska	vojsko	k1gNnSc2
<g/>
;	;	kIx,
byzantský	byzantský	k2eAgInSc1d1
názor	názor	k1gInSc1
na	na	k7c4
přicházející	přicházející	k2eAgInPc4d1
křižáky	křižák	k1gInPc4
vystihuje	vystihovat	k5eAaImIp3nS
byzantský	byzantský	k2eAgMnSc1d1
kronikář	kronikář	k1gMnSc1
Niketas	Niketas	k1gMnSc1
Choniates	Choniates	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Z	z	k7c2
velké	velký	k2eAgFnSc2d1
dálky	dálka	k1gFnSc2
se	se	k3xPyFc4
blížilo	blížit	k5eAaImAgNnS
nové	nový	k2eAgNnSc1d1
neštěstí	neštěstí	k1gNnSc1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
by	by	kYmCp3nP
bojovní	bojovný	k2eAgMnPc1d1
barbaři	barbar	k1gMnPc1
nebyli	být	k5eNaImAgMnP
dostatečným	dostatečný	k2eAgNnSc7d1
soužením	soužení	k1gNnSc7
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
to	ten	k3xDgNnSc1
Friderichos	Friderichos	k1gInSc1
<g/>
,	,	kIx,
král	král	k1gMnSc1
Alamanů	Alaman	k1gMnPc2
<g/>
...	...	k?
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Niketas	Niketas	k1gMnSc1
Choniates	Choniates	k1gMnSc1
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přestože	přestože	k8xS
se	s	k7c7
vztahy	vztah	k1gInPc7
mezi	mezi	k7c7
císaři	císař	k1gMnPc7
povážlivě	povážlivě	k6eAd1
zhoršily	zhoršit	k5eAaPmAgFnP
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
nevyužil	využít	k5eNaPmAgMnS
Fridrich	Fridrich	k1gMnSc1
další	další	k2eAgFnSc2d1
nabídky	nabídka	k1gFnSc2
srbského	srbský	k2eAgMnSc2d1
župana	župan	k1gMnSc2
<g/>
,	,	kIx,
s	s	k7c7
nímž	jenž	k3xRgMnSc7
se	se	k3xPyFc4
osobně	osobně	k6eAd1
setkal	setkat	k5eAaPmAgMnS
v	v	k7c6
Niši	Niš	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
stane	stanout	k5eAaPmIp3nS
jeho	jeho	k3xOp3gMnSc7
vazalem	vazal	k1gMnSc7
<g/>
,	,	kIx,
a	a	k8xC
odmítl	odmítnout	k5eAaPmAgMnS
také	také	k9
bratry	bratr	k1gMnPc4
Asena	Aseen	k2eAgFnSc1d1
a	a	k8xC
Petra	Petra	k1gFnSc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
byli	být	k5eAaImAgMnP
ochotni	ochoten	k2eAgMnPc1d1
poskytnout	poskytnout	k5eAaPmF
mu	on	k3xPp3gMnSc3
vojenskou	vojenský	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
a	a	k8xC
uznat	uznat	k5eAaPmF
lenní	lenní	k2eAgFnSc4d1
svrchovanost	svrchovanost	k1gFnSc4
říše	říš	k1gFnSc2
nad	nad	k7c7
Bulharskem	Bulharsko	k1gNnSc7
<g/>
,	,	kIx,
přizná	přiznat	k5eAaPmIp3nS
<g/>
-li	-li	k?
jeho	jeho	k3xOp3gMnSc3
vládci	vládce	k1gMnSc3
carský	carský	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fridrichovým	Fridrichův	k2eAgInSc7d1
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
osvobodit	osvobodit	k5eAaPmF
Jeruzalém	Jeruzalém	k1gInSc4
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
nechtěl	chtít	k5eNaImAgMnS
angažovat	angažovat	k5eAaBmF
ve	v	k7c6
složitých	složitý	k2eAgInPc6d1
balkánských	balkánský	k2eAgInPc6d1
poměrech	poměr	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
ale	ale	k9
Izáku	Izák	k1gMnSc3
Angelovi	angel	k1gMnSc3
donesly	donést	k5eAaPmAgInP
zprávy	zpráva	k1gFnSc2
o	o	k7c6
vyjednávání	vyjednávání	k1gNnSc6
Němců	Němec	k1gMnPc2
se	s	k7c7
Srby	Srb	k1gMnPc7
<g/>
,	,	kIx,
začal	začít	k5eAaPmAgInS
to	ten	k3xDgNnSc1
považovat	považovat	k5eAaImF
za	za	k7c4
jednání	jednání	k1gNnSc4
proti	proti	k7c3
Byzantské	byzantský	k2eAgFnSc3d1
říši	říš	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
Naproti	naproti	k7c3
tomu	ten	k3xDgMnSc3
sám	sám	k3xTgMnSc1
Izák	Izák	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
jednal	jednat	k5eAaImAgMnS
se	s	k7c7
Saladinem	Saladin	k1gMnSc7
a	a	k8xC
slíbil	slíbit	k5eAaPmAgMnS
mu	on	k3xPp3gMnSc3
<g/>
,	,	kIx,
že	že	k8xS
pochodu	pochod	k1gInSc2
Němců	Němec	k1gMnPc2
zabrání	zabránit	k5eAaPmIp3nS
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Když	když	k8xS
se	se	k3xPyFc4
o	o	k7c6
tomto	tento	k3xDgInSc6
paktu	pakt	k1gInSc6
mezi	mezi	k7c7
Byzantinci	Byzantinec	k1gMnPc7
a	a	k8xC
muslimy	muslim	k1gMnPc7
doslechli	doslechnout	k5eAaPmAgMnP
křižáci	křižák	k1gMnPc1
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
hluboce	hluboko	k6eAd1
pohoršeni	pohoršen	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Barbarossa	Barbarossa	k1gMnSc1
postupoval	postupovat	k5eAaImAgInS
směrem	směr	k1gInSc7
na	na	k7c4
Konstantinopol	Konstantinopol	k1gInSc4
a	a	k8xC
obsadil	obsadit	k5eAaPmAgMnS
Filippopolis	Filippopolis	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Izák	Izák	k1gMnSc1
Angelos	Angelos	k1gMnSc1
se	s	k7c7
<g />
.	.	kIx.
</s>
<s hack="1">
obával	obávat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Fridrich	Fridrich	k1gMnSc1
napadne	napadnout	k5eAaPmIp3nS
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
Byzance	Byzanc	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Když	když	k8xS
do	do	k7c2
Konstantinopole	Konstantinopol	k1gInSc2
dorazili	dorazit	k5eAaPmAgMnP
Fridrichovi	Fridrichův	k2eAgMnPc1d1
vyslanci	vyslanec	k1gMnPc1
<g/>
,	,	kIx,
nechal	nechat	k5eAaPmAgMnS
je	být	k5eAaImIp3nS
císař	císař	k1gMnSc1
uvěznit	uvěznit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
Barbarossa	Barbarossa	k1gMnSc1
to	ten	k3xDgNnSc4
považoval	považovat	k5eAaImAgMnS
za	za	k7c4
jasné	jasný	k2eAgNnSc4d1
vyhlášení	vyhlášení	k1gNnSc4
války	válka	k1gFnSc2
<g/>
;	;	kIx,
Fridrichův	Fridrichův	k2eAgMnSc1d1
syn	syn	k1gMnSc1
s	s	k7c7
menší	malý	k2eAgFnSc7d2
<g />
.	.	kIx.
</s>
<s hack="1">
armádou	armáda	k1gFnSc7
podnikl	podniknout	k5eAaPmAgMnS
demonstrativní	demonstrativní	k2eAgInSc4d1
útok	útok	k1gInSc4
na	na	k7c4
Konstantinopol	Konstantinopol	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
zatímco	zatímco	k8xS
sám	sám	k3xTgMnSc1
Fridrich	Fridrich	k1gMnSc1
okamžitě	okamžitě	k6eAd1
vyslal	vyslat	k5eAaPmAgMnS
své	svůj	k3xOyFgMnPc4
posly	posel	k1gMnPc4
do	do	k7c2
německých	německý	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ze	z	k7c2
Severního	severní	k2eAgNnSc2d1
moře	moře	k1gNnSc2
přivolal	přivolat	k5eAaPmAgInS
německou	německý	k2eAgFnSc4d1
flotilu	flotila	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
mu	on	k3xPp3gMnSc3
pomohla	pomoct	k5eAaPmAgFnS
dobýt	dobýt	k5eAaPmF
Konstantinopol	Konstantinopol	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Zároveň	zároveň	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
papeži	papež	k1gMnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
cíle	cíl	k1gInPc1
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
rozšířil	rozšířit	k5eAaPmAgInS
i	i	k9
proti	proti	k7c3
Byzantské	byzantský	k2eAgFnSc3d1
říši	říš	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Když	když	k8xS
se	se	k3xPyFc4
Izák	Izák	k1gMnSc1
dozvěděl	dozvědět	k5eAaPmAgMnS
o	o	k7c6
Barbarossových	Barbarossův	k2eAgInPc6d1
plánech	plán	k1gInPc6
<g/>
,	,	kIx,
propustil	propustit	k5eAaPmAgInS
německé	německý	k2eAgMnPc4d1
zajatce	zajatec	k1gMnPc4
a	a	k8xC
uzavřel	uzavřít	k5eAaPmAgMnS
s	s	k7c7
jejich	jejich	k3xOp3gMnSc7
pánem	pán	k1gMnSc7
příměří	příměří	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Fridrich	Fridrich	k1gMnSc1
po	po	k7c6
Byzantincích	Byzantinec	k1gMnPc6
<g />
.	.	kIx.
</s>
<s hack="1">
požadoval	požadovat	k5eAaImAgMnS
zásobování	zásobování	k1gNnSc4
přes	přes	k7c4
zimu	zima	k1gFnSc4
a	a	k8xC
na	na	k7c4
cestu	cesta	k1gFnSc4
Anatolií	Anatolie	k1gFnPc2
poskytnutí	poskytnutí	k1gNnSc2
průvodce	průvodce	k1gMnSc1
<g/>
,	,	kIx,
za	za	k7c4
což	což	k3yRnSc4,k3yQnSc4
byl	být	k5eAaImAgInS
ochoten	ochoten	k2eAgInSc1d1
do	do	k7c2
Anatolie	Anatolie	k1gFnSc2
přejít	přejít	k5eAaPmF
přes	přes	k7c4
Helespont	Helespont	k1gInSc4
a	a	k8xC
vyhnout	vyhnout	k5eAaPmF
se	se	k3xPyFc4
tak	tak	k9
přímo	přímo	k6eAd1
Konstantinopoli	Konstantinopol	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Protože	protože	k8xS
již	již	k6eAd1
byl	být	k5eAaImAgMnS
podzim	podzim	k1gInSc4
<g/>
,	,	kIx,
přezimovala	přezimovat	k5eAaBmAgFnS
německá	německý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
v	v	k7c6
Adrianopoli	Adrianopole	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Barbarossova	Barbarossův	k2eAgFnSc1d1
smrt	smrt	k1gFnSc1
v	v	k7c6
řece	řeka	k1gFnSc6
Salef	Salef	k1gInSc1
(	(	kIx(
<g/>
Saxon	Saxon	k1gInSc1
Chronicle	Chronicle	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
březnu	březen	k1gInSc6
1190	#num#	k4
křižáci	křižák	k1gMnPc1
překročili	překročit	k5eAaPmAgMnP
Helespont	Helespont	k1gInSc4
a	a	k8xC
25	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
vstoupili	vstoupit	k5eAaPmAgMnP
na	na	k7c4
turecké	turecký	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Němci	Němec	k1gMnPc1
zamířili	zamířit	k5eAaPmAgMnP
na	na	k7c4
jih	jih	k1gInSc4
podél	podél	k7c2
Egejského	egejský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
na	na	k7c6
Filadelfii	Filadelfie	k1gFnSc6
a	a	k8xC
Laodikeju	Laodikeju	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Poté	poté	k6eAd1
se	se	k3xPyFc4
stočili	stočit	k5eAaPmAgMnP
na	na	k7c4
východ	východ	k1gInSc4
do	do	k7c2
vnitrozemí	vnitrozemí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vyprahlých	vyprahlý	k2eAgFnPc6d1
anatolských	anatolský	k2eAgFnPc6d1
pustinách	pustina	k1gFnPc6
trpěli	trpět	k5eAaImAgMnP
hladem	hlad	k1gInSc7
i	i	k8xC
žízní	žízeň	k1gFnSc7
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
byli	být	k5eAaImAgMnP
neustále	neustále	k6eAd1
napadáni	napadat	k5eAaBmNgMnP,k5eAaPmNgMnP
tureckými	turecký	k2eAgMnPc7d1
vojáky	voják	k1gMnPc7
<g/>
,	,	kIx,
které	který	k3yQgMnPc4,k3yRgMnPc4,k3yIgMnPc4
vedl	vést	k5eAaImAgMnS
syn	syn	k1gMnSc1
sultána	sultán	k1gMnSc2
Kilič	Kilič	k1gMnSc1
Arslana	Arslan	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Qutb	Qutb	k1gMnSc1
ad-Dín	ad-Dín	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
...	...	k?
<g/>
někteří	některý	k3yIgMnPc1
pili	pít	k5eAaImAgMnP
vlastní	vlastní	k2eAgFnSc4d1
moč	moč	k1gFnSc4
<g/>
,	,	kIx,
někteří	některý	k3yIgMnPc1
krev	krev	k1gFnSc4
koní	kůň	k1gMnPc2
<g/>
,	,	kIx,
jiní	jiný	k1gMnPc1
žvýkali	žvýkat	k5eAaImAgMnP
kvůli	kvůli	k7c3
šťávě	šťáva	k1gFnSc3
koňský	koňský	k2eAgInSc4d1
hnůj	hnůj	k1gInSc4
a	a	k8xC
mnozí	mnohý	k2eAgMnPc1d1
přežvykovali	přežvykovat	k5eAaImAgMnP
travnaté	travnatý	k2eAgInPc4d1
drny	drn	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Ansbert	Ansbert	k1gInSc1
</s>
<s>
Křižáci	křižák	k1gMnPc1
dorazili	dorazit	k5eAaPmAgMnP
až	až	k6eAd1
k	k	k7c3
Iconiu	Iconium	k1gNnSc3
<g/>
,	,	kIx,
hlavnímu	hlavní	k2eAgNnSc3d1
městu	město	k1gNnSc3
Ikonyjského	Ikonyjský	k2eAgInSc2d1
sultanátu	sultanát	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
do	do	k7c2
cesty	cesta	k1gFnSc2
postavilo	postavit	k5eAaPmAgNnS
hlavní	hlavní	k2eAgNnSc1d1
seldžucké	seldžucký	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
Němci	Němec	k1gMnPc7
Turky	Turek	k1gMnPc4
na	na	k7c4
hlavu	hlava	k1gFnSc4
porazili	porazit	k5eAaPmAgMnP
a	a	k8xC
obsadili	obsadit	k5eAaPmAgMnP
Iconium	Iconium	k1gNnSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
čímž	což	k3yQnSc7,k3yRnSc7
si	se	k3xPyFc3
otevřeli	otevřít	k5eAaPmAgMnP
cestu	cesta	k1gFnSc4
na	na	k7c4
východ	východ	k1gInSc4
do	do	k7c2
oblasti	oblast	k1gFnSc2
Kilíkie	Kilíkie	k1gFnSc2
<g/>
,	,	kIx,
ovládané	ovládaný	k2eAgNnSc1d1
křesťanskými	křesťanský	k2eAgMnPc7d1
arménskými	arménský	k2eAgMnPc7d1
knížaty	kníže	k1gMnPc7wR
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Tady	tady	k6eAd1
našel	najít	k5eAaPmAgInS
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1190	#num#	k4
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
Barbarossa	Barbarossa	k1gMnSc1
při	při	k7c6
přechodu	přechod	k1gInSc6
řeky	řeka	k1gFnSc2
Saléf	Saléf	k1gMnSc1
svoji	svůj	k3xOyFgFnSc4
smrt	smrt	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
Císař	Císař	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
si	se	k3xPyFc3
postoupil	postoupit	k5eAaPmAgMnS
všechna	všechen	k3xTgNnPc4
nebezpečí	nebezpečí	k1gNnPc4
<g/>
,	,	kIx,
si	se	k3xPyFc3
přál	přát	k5eAaImAgMnS
uniknout	uniknout	k5eAaPmF
mimořádnému	mimořádný	k2eAgNnSc3d1
vedru	vedro	k1gNnSc3
a	a	k8xC
vyhnout	vyhnout	k5eAaPmF
se	se	k3xPyFc4
stoupání	stoupání	k1gNnSc1
na	na	k7c4
horský	horský	k2eAgInSc4d1
vrch	vrch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
pokoušel	pokoušet	k5eAaImAgInS
přeplavat	přeplavat	k5eAaPmF
přes	přes	k7c4
velice	velice	k6eAd1
prudkou	prudký	k2eAgFnSc4d1
řeku	řeka	k1gFnSc4
Kalycadmus	Kalycadmus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
jakýsi	jakýsi	k3yIgMnSc1
rozumný	rozumný	k2eAgMnSc1d1
muž	muž	k1gMnSc1
řekl	říct	k5eAaPmAgMnS
<g/>
:	:	kIx,
<g/>
"	"	kIx"
<g/>
Nemůžeš	moct	k5eNaImIp2nS
plavat	plavat	k5eAaImF
proti	proti	k7c3
toku	tok	k1gInSc3
řeky	řeka	k1gFnSc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
Jinak	jinak	k6eAd1
vždy	vždy	k6eAd1
velmi	velmi	k6eAd1
rozumný	rozumný	k2eAgMnSc1d1
císař	císař	k1gMnSc1
bláznivě	bláznivě	k6eAd1
zkoušel	zkoušet	k5eAaImAgMnS
svoje	svůj	k3xOyFgFnPc4
síly	síla	k1gFnPc4
proti	proti	k7c3
proudu	proud	k1gInSc3
a	a	k8xC
proti	proti	k7c3
síle	síla	k1gFnSc3
řeky	řeka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
každý	každý	k3xTgMnSc1
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
ho	on	k3xPp3gMnSc4
zastavit	zastavit	k5eAaPmF
<g/>
,	,	kIx,
vstoupil	vstoupit	k5eAaPmAgMnS
do	do	k7c2
vody	voda	k1gFnSc2
a	a	k8xC
ponořil	ponořit	k5eAaPmAgInS
se	se	k3xPyFc4
do	do	k7c2
vodního	vodní	k2eAgInSc2d1
víru	vír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
tak	tak	k9
ten	ten	k3xDgMnSc1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
podstoupil	podstoupit	k5eAaPmAgInS
mnohá	mnohý	k2eAgNnPc4d1
nebezpečí	nebezpečí	k1gNnPc4
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
uboze	uboze	k6eAd1
zahynul	zahynout	k5eAaPmAgMnS
<g/>
...	...	k?
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Ansbert	Ansbert	k1gInSc1
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Německá	německý	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
se	se	k3xPyFc4
po	po	k7c6
císařově	císařův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
rozpadla	rozpadnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
Mnoho	mnoho	k4c1
bojovníků	bojovník	k1gMnPc2
se	se	k3xPyFc4
poté	poté	k6eAd1
vrátilo	vrátit	k5eAaPmAgNnS
domů	domů	k6eAd1
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
menší	malý	k2eAgFnSc4d2
část	část	k1gFnSc4
dovedl	dovést	k5eAaPmAgMnS
císařův	císařův	k2eAgMnSc1d1
syn	syn	k1gMnSc1
švábský	švábský	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
do	do	k7c2
Akkonu	Akkon	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výprava	výprava	k1gFnSc1
Francouzů	Francouz	k1gMnPc2
a	a	k8xC
Angličanů	Angličan	k1gMnPc2
</s>
<s>
Cesta	cesta	k1gFnSc1
na	na	k7c6
Sicílii	Sicílie	k1gFnSc6
</s>
<s>
Richard	Richard	k1gMnSc1
a	a	k8xC
Filip	Filip	k1gMnSc1
se	se	k3xPyFc4
setkávají	setkávat	k5eAaImIp3nP
v	v	k7c6
Messině	Messina	k1gFnSc6
(	(	kIx(
<g/>
Les	les	k1gInSc1
Chroniques	Chroniques	k1gInSc1
de	de	k?
France	Franc	k1gMnSc2
ou	ou	k0
de	de	k?
St-Denis	St-Denis	k1gFnPc6
<g/>
)	)	kIx)
</s>
<s>
Společná	společný	k2eAgFnSc1d1
francouzsko-anglická	francouzsko-anglický	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
se	se	k3xPyFc4
měla	mít	k5eAaImAgFnS
uskutečnit	uskutečnit	k5eAaPmF
o	o	k7c6
Velikonocích	Velikonoce	k1gFnPc6
roku	rok	k1gInSc2
1189	#num#	k4
<g/>
,	,	kIx,
zatím	zatím	k6eAd1
však	však	k9
mezi	mezi	k7c7
Francií	Francie	k1gFnSc7
a	a	k8xC
Anglií	Anglie	k1gFnSc7
znovu	znovu	k6eAd1
vypuklo	vypuknout	k5eAaPmAgNnS
nepřátelství	nepřátelství	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
6	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1189	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
anglický	anglický	k2eAgMnSc1d1
král	král	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
jeho	jeho	k3xOp3gNnSc4
místo	místo	k1gNnSc4
zaujal	zaujmout	k5eAaPmAgMnS
syn	syn	k1gMnSc1
Richard	Richard	k1gMnSc1
I.	I.	kA
Ten	ten	k3xDgMnSc1
s	s	k7c7
Filipem	Filip	k1gMnSc7
Augustem	August	k1gMnSc7
uzavřel	uzavřít	k5eAaPmAgMnS
mír	mír	k1gInSc4
a	a	k8xC
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1190	#num#	k4
<g/>
,	,	kIx,
přesně	přesně	k6eAd1
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
po	po	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Hattínu	Hattín	k1gInSc2
<g/>
,	,	kIx,
vyrazili	vyrazit	k5eAaPmAgMnP
z	z	k7c2
Vézelay	Vézelaa	k1gFnSc2
oba	dva	k4xCgMnPc1
králové	král	k1gMnPc1
směrem	směr	k1gInSc7
na	na	k7c4
Lyon	Lyon	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Lyonu	Lyon	k1gInSc6
se	se	k3xPyFc4
Angličané	Angličan	k1gMnPc1
a	a	k8xC
Francouzi	Francouz	k1gMnPc1
rozdělili	rozdělit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Richard	Richard	k1gMnSc1
táhl	táhnout	k5eAaImAgMnS
na	na	k7c4
Marseille	Marseille	k1gFnPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
jej	on	k3xPp3gNnSc4
mělo	mít	k5eAaImAgNnS
očekávat	očekávat	k5eAaImF
anglické	anglický	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Filip	Filip	k1gMnSc1
si	se	k3xPyFc3
přepravu	přeprava	k1gFnSc4
sjednal	sjednat	k5eAaPmAgInS
s	s	k7c7
Janovany	Janovan	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Oba	dva	k4xCgMnPc1
však	však	k9
měli	mít	k5eAaImAgMnP
v	v	k7c6
plánu	plán	k1gInSc6
se	se	k3xPyFc4
znovu	znovu	k6eAd1
setkat	setkat	k5eAaPmF
na	na	k7c6
Sicílii	Sicílie	k1gFnSc6
<g/>
,	,	kIx,
kam	kam	k6eAd1
je	on	k3xPp3gMnPc4
pozval	pozvat	k5eAaPmAgMnS
král	král	k1gMnSc1
Vilém	Vilém	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Richardův	Richardův	k2eAgMnSc1d1
švagr	švagr	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Cesta	cesta	k1gFnSc1
přes	přes	k7c4
Sicílii	Sicílie	k1gFnSc4
byla	být	k5eAaImAgFnS
velmi	velmi	k6eAd1
nákladná	nákladný	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
nejbezpečnější	bezpečný	k2eAgMnSc1d3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Vilém	Vilém	k1gMnSc1
však	však	k9
roku	rok	k1gInSc2
1189	#num#	k4
nečekaně	nečekaně	k6eAd1
zemřel	zemřít	k5eAaPmAgMnS
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
bratranec	bratranec	k1gMnSc1
a	a	k8xC
nástupce	nástupce	k1gMnSc1
Tankred	Tankred	k1gMnSc1
královně	královna	k1gFnSc3
vdově	vdova	k1gFnSc3
Johaně	Johana	k1gFnSc3
<g/>
,	,	kIx,
Richardově	Richardův	k2eAgFnSc3d1
sestře	sestra	k1gFnSc3
<g/>
,	,	kIx,
ukradl	ukradnout	k5eAaPmAgMnS
věno	věno	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yRgNnSc4,k3yIgNnSc4
jí	on	k3xPp3gFnSc3
mělo	mít	k5eAaImAgNnS
po	po	k7c6
Vilémově	Vilémův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
patřit	patřit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nechal	nechat	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
také	také	k6eAd1
uvrhnout	uvrhnout	k5eAaPmF
do	do	k7c2
domácího	domácí	k2eAgNnSc2d1
vězení	vězení	k1gNnSc2
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
obavě	obava	k1gFnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nepodporovala	podporovat	k5eNaImAgFnS
Tankredovu	Tankredův	k2eAgFnSc4d1
tetu	teta	k1gFnSc4
<g/>
,	,	kIx,
princeznu	princezna	k1gFnSc4
Konstancii	Konstancie	k1gFnSc4
a	a	k8xC
jejího	její	k3xOp3gMnSc2
manžela	manžel	k1gMnSc2
Jindřicha	Jindřich	k1gMnSc2
<g/>
,	,	kIx,
nejstaršího	starý	k2eAgMnSc2d3
syna	syn	k1gMnSc2
Fridricha	Fridrich	k1gMnSc2
Barbarossy	Barbarossa	k1gMnSc2
a	a	k8xC
římsko-německého	římsko-německý	k2eAgMnSc2d1
krále	král	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
Messiny	Messina	k1gFnSc2
na	na	k7c6
Sicílii	Sicílie	k1gFnSc6
dorazil	dorazit	k5eAaPmAgMnS
jako	jako	k8xS,k8xC
první	první	k4xOgMnSc1
v	v	k7c6
září	září	k1gNnSc6
král	král	k1gMnSc1
Filip	Filip	k1gMnSc1
a	a	k8xC
Richard	Richard	k1gMnSc1
několik	několik	k4yIc4
dnů	den	k1gInPc2
po	po	k7c6
něm.	něm.	k?
Richard	Richard	k1gMnSc1
se	se	k3xPyFc4
vzápětí	vzápětí	k6eAd1
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
sporu	spor	k1gInSc2
s	s	k7c7
králem	král	k1gMnSc7
Tankredem	Tankred	k1gMnSc7
a	a	k8xC
když	když	k8xS
vypuklo	vypuknout	k5eAaPmAgNnS
v	v	k7c6
Messině	Messin	k2eAgNnSc6d1
povstání	povstání	k1gNnSc6
proti	proti	k7c3
Angličanům	Angličan	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
byli	být	k5eAaImAgMnP
neukáznění	ukázněný	k2eNgMnPc1d1
a	a	k8xC
obtěžovali	obtěžovat	k5eAaImAgMnP
obyvatelstvo	obyvatelstvo	k1gNnSc4
<g/>
,	,	kIx,
dal	dát	k5eAaPmAgMnS
rozzuřený	rozzuřený	k2eAgMnSc1d1
Richard	Richard	k1gMnSc1
rozkaz	rozkaz	k1gInSc1
město	město	k1gNnSc4
napadnout	napadnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tankred	Tankred	k1gInSc1
pak	pak	k6eAd1
požádal	požádat	k5eAaPmAgInS
Richarda	Richard	k1gMnSc4
o	o	k7c4
příměří	příměří	k1gNnSc4
a	a	k8xC
vyplatil	vyplatit	k5eAaPmAgMnS
okamžitě	okamžitě	k6eAd1
Richardovi	Richard	k1gMnSc3
Johannino	Johannin	k2eAgNnSc1d1
věno	věno	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Filip	Filip	k1gMnSc1
August	August	k1gMnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
Richardova	Richardův	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
akce	akce	k1gFnSc1
pobouřila	pobouřit	k5eAaPmAgFnS
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
neprodleně	prodleně	k6eNd1
zasáhl	zasáhnout	k5eAaPmAgMnS
a	a	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podařilo	podařit	k5eAaPmAgNnS
spor	spor	k1gInSc4
mezi	mezi	k7c7
Richardem	Richard	k1gMnSc7
a	a	k8xC
Tankredem	Tankred	k1gMnSc7
urovnat	urovnat	k5eAaPmF
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Příměří	příměří	k1gNnSc1
bylo	být	k5eAaImAgNnS
obnoveno	obnovit	k5eAaPmNgNnS
a	a	k8xC
králové	král	k1gMnPc1
se	se	k3xPyFc4
rozhodli	rozhodnout	k5eAaPmAgMnP
na	na	k7c6
Sicílii	Sicílie	k1gFnSc6
přezimovat	přezimovat	k5eAaBmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Byla	být	k5eAaImAgFnS
také	také	k9
uzavřena	uzavřen	k2eAgFnSc1d1
formální	formální	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
mezi	mezi	k7c7
Richardem	Richard	k1gMnSc7
a	a	k8xC
Filipem	Filip	k1gMnSc7
<g/>
;	;	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
stanovila	stanovit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
veškerá	veškerý	k3xTgFnSc1
kořist	kořist	k1gFnSc1
získaná	získaný	k2eAgFnSc1d1
na	na	k7c6
křižácké	křižácký	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
bude	být	k5eAaImBp3nS
rovným	rovný	k2eAgInSc7d1
dílem	díl	k1gInSc7
rozdělena	rozdělit	k5eAaPmNgFnS
mezi	mezi	k7c4
Angličany	Angličan	k1gMnPc4
a	a	k8xC
Francouze	Francouz	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Proto	proto	k8xC
Filip	Filip	k1gMnSc1
August	August	k1gMnSc1
vznesl	vznést	k5eAaPmAgMnS
formální	formální	k2eAgInSc4d1
nárok	nárok	k1gInSc4
na	na	k7c4
polovinu	polovina	k1gFnSc4
Jonanina	Jonanin	k2eAgNnSc2d1
věna	věno	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
Richard	Richard	k1gMnSc1
získal	získat	k5eAaPmAgMnS
od	od	k7c2
Tankreda	Tankred	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ač	ač	k8xS
Filip	Filip	k1gMnSc1
fakticky	fakticky	k6eAd1
na	na	k7c4
část	část	k1gFnSc4
věna	věno	k1gNnSc2
nárok	nárok	k1gInSc1
neměl	mít	k5eNaImAgMnS
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
mu	on	k3xPp3gMnSc3
v	v	k7c6
zájmu	zájem	k1gInSc6
dobrých	dobrý	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
vyplatil	vyplatit	k5eAaPmAgInS
třetinu	třetina	k1gFnSc4
částky	částka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Richard	Richard	k1gMnSc1
poté	poté	k6eAd1
oznámil	oznámit	k5eAaPmAgMnS
zrušení	zrušení	k1gNnSc4
svého	svůj	k3xOyFgNnSc2
zasnoubení	zasnoubení	k1gNnSc2
s	s	k7c7
Filipovou	Filipová	k1gFnSc7
sestrou	sestra	k1gFnSc7
Adélou	Adéla	k1gFnSc7
a	a	k8xC
naopak	naopak	k6eAd1
se	se	k3xPyFc4
zasnoubil	zasnoubit	k5eAaPmAgMnS
s	s	k7c7
navarrskou	navarrský	k2eAgFnSc7d1
princeznou	princezna	k1gFnSc7
Berengarií	Berengarie	k1gFnPc2
<g/>
;	;	kIx,
byl	být	k5eAaImAgInS
to	ten	k3xDgNnSc1
politický	politický	k2eAgInSc1d1
tah	tah	k1gInSc1
<g/>
,	,	kIx,
kterým	který	k3yQgFnPc3,k3yRgFnPc3,k3yIgFnPc3
Richard	Richard	k1gMnSc1
sledoval	sledovat	k5eAaImAgMnS
posílení	posílení	k1gNnSc4
anglických	anglický	k2eAgFnPc2d1
pozic	pozice	k1gFnPc2
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dobytí	dobytí	k1gNnSc1
Kypru	Kypr	k1gInSc2
</s>
<s>
Dne	den	k1gInSc2
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1191	#num#	k4
opustili	opustit	k5eAaPmAgMnP
francouzští	francouzský	k2eAgMnPc1d1
křižáci	křižák	k1gMnPc1
se	se	k3xPyFc4
svým	svůj	k3xOyFgMnSc7
králem	král	k1gMnSc7
Sicílii	Sicílie	k1gFnSc6
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
přistáli	přistát	k5eAaImAgMnP,k5eAaPmAgMnP
v	v	k7c6
Palestině	Palestina	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Angličané	Angličan	k1gMnPc1
vyrazili	vyrazit	k5eAaPmAgMnP
deset	deset	k4xCc4
dnů	den	k1gInPc2
po	po	k7c6
Francouzích	Francouzy	k1gInPc6
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
s	s	k7c7
králem	král	k1gMnSc7
Richardem	Richard	k1gMnSc7
<g />
.	.	kIx.
</s>
<s hack="1">
odplula	odplout	k5eAaPmAgFnS
i	i	k9
sestra	sestra	k1gFnSc1
Johana	Johana	k1gFnSc1
a	a	k8xC
snoubenka	snoubenka	k1gFnSc1
Berengarrie	Berengarrie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Anglické	anglický	k2eAgFnSc2d1
lodě	loď	k1gFnSc2
pronásledovalo	pronásledovat	k5eAaImAgNnS
špatné	špatný	k2eAgNnSc1d1
počasí	počasí	k1gNnSc1
<g/>
,	,	kIx,
u	u	k7c2
jižního	jižní	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
se	se	k3xPyFc4
flotila	flotila	k1gFnSc1
rozpadla	rozpadnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
mnohé	mnohý	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
se	se	k3xPyFc4
potopily	potopit	k5eAaPmAgFnP
<g/>
,	,	kIx,
některé	některý	k3yIgFnPc1
změnily	změnit	k5eAaPmAgFnP
kurs	kurs	k1gInSc4
na	na	k7c4
Kypr	Kypr	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
Sám	sám	k3xTgMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Richard	Richard	k1gMnSc1
zamířil	zamířit	k5eAaPmAgMnS
nejprve	nejprve	k6eAd1
na	na	k7c4
Krétu	Kréta	k1gFnSc4
a	a	k8xC
poté	poté	k6eAd1
zakotvil	zakotvit	k5eAaPmAgInS
na	na	k7c6
Rhodu	Rhodos	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
zůstal	zůstat	k5eAaPmAgInS
až	až	k6eAd1
do	do	k7c2
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
jedné	jeden	k4xCgFnSc6
z	z	k7c2
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
byly	být	k5eAaImAgFnP
zahnány	zahnat	k5eAaPmNgFnP
na	na	k7c4
Kypr	Kypr	k1gInSc4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
i	i	k9
Richardova	Richardův	k2eAgFnSc1d1
sestra	sestra	k1gFnSc1
a	a	k8xC
snoubenka	snoubenka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Kypr	Kypr	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
oficiálně	oficiálně	k6eAd1
byzantskou	byzantský	k2eAgFnSc7d1
provincií	provincie	k1gFnSc7
<g/>
,	,	kIx,
fakticky	fakticky	k6eAd1
mu	on	k3xPp3gInSc3
ale	ale	k9
nezávisle	závisle	k6eNd1
vládl	vládnout	k5eAaImAgMnS
Izák	Izák	k1gMnSc1
Komnenos	Komnenos	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
prohlásil	prohlásit	k5eAaPmAgMnS
nezávislým	závislý	k2eNgMnSc7d1
kyperským	kyperský	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
Byzantská	byzantský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
v	v	k7c6
Konstantinopoli	Konstantinopol	k1gInSc6
neměla	mít	k5eNaImAgFnS
sílu	síla	k1gFnSc4
vzpurného	vzpurný	k2eAgMnSc4d1
despotického	despotický	k2eAgMnSc4d1
vládce	vládce	k1gMnSc4
sesadit	sesadit	k5eAaPmF
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Izáka	Izák	k1gMnSc2
zpráva	zpráva	k1gFnSc1
o	o	k7c6
křižáckých	křižácký	k2eAgFnPc6d1
lodích	loď	k1gFnPc6
zděsila	zděsit	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Mnohé	mnohý	k2eAgMnPc4d1
anglické	anglický	k2eAgMnPc4d1
trosečníky	trosečník	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
přistáli	přistát	k5eAaPmAgMnP,k5eAaImAgMnP
na	na	k7c6
ostrově	ostrov	k1gInSc6
<g/>
,	,	kIx,
dal	dát	k5eAaPmAgMnS
okamžitě	okamžitě	k6eAd1
uvěznit	uvěznit	k5eAaPmF
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
lodě	loď	k1gFnSc2
vydrancoval	vydrancovat	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Kapitán	kapitán	k1gMnSc1
lodě	loď	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
níž	jenž	k3xRgFnSc6
cestovala	cestovat	k5eAaImAgFnS
Johana	Johana	k1gFnSc1
s	s	k7c7
Berengarrií	Berengarrie	k1gFnSc7
<g/>
,	,	kIx,
zakotvil	zakotvit	k5eAaPmAgMnS
v	v	k7c6
Limassolské	Limassolský	k2eAgFnSc6d1
zátoce	zátoka	k1gFnSc6
a	a	k8xC
požádal	požádat	k5eAaPmAgMnS
Komnena	Komnen	k1gMnSc4
o	o	k7c4
zásoby	zásoba	k1gFnPc4
<g/>
,	,	kIx,
ten	ten	k3xDgInSc1
ale	ale	k9
jakoukoliv	jakýkoliv	k3yIgFnSc4
pomoc	pomoc	k1gFnSc4
odmítl	odmítnout	k5eAaPmAgMnS
a	a	k8xC
zakázal	zakázat	k5eAaPmAgMnS
posádce	posádka	k1gFnSc3
se	se	k3xPyFc4
vylodit	vylodit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Když	když	k8xS
jen	jen	k9
o	o	k7c4
několik	několik	k4yIc4
dnů	den	k1gInPc2
později	pozdě	k6eAd2
dorazil	dorazit	k5eAaPmAgInS
s	s	k7c7
hlavními	hlavní	k2eAgFnPc7d1
silami	síla	k1gFnPc7
Richard	Richarda	k1gFnPc2
<g/>
,	,	kIx,
císařovo	císařův	k2eAgNnSc1d1
jednání	jednání	k1gNnSc1
ho	on	k3xPp3gMnSc4
značně	značně	k6eAd1
pobouřilo	pobouřit	k5eAaPmAgNnS
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
požadoval	požadovat	k5eAaImAgMnS
okamžité	okamžitý	k2eAgNnSc4d1
propuštění	propuštění	k1gNnSc4
anglických	anglický	k2eAgMnPc2d1
zajatců	zajatec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Izák	Izák	k1gMnSc1
Komnenos	Komnenos	k1gMnSc1
odmítl	odmítnout	k5eAaPmAgMnS
a	a	k8xC
začal	začít	k5eAaPmAgMnS
mobilizovat	mobilizovat	k5eAaBmF
obranu	obrana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Richard	Richard	k1gMnSc1
vzal	vzít	k5eAaPmAgMnS
útokem	útok	k1gInSc7
přístav	přístav	k1gInSc1
Limassol	Limassola	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
okamžitě	okamžitě	k6eAd1
kapituloval	kapitulovat	k5eAaBmAgMnS
<g/>
,	,	kIx,
a	a	k8xC
Izáka	Izák	k1gMnSc2
odtud	odtud	k6eAd1
vypudil	vypudit	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Ten	ten	k3xDgInSc1
později	pozdě	k6eAd2
přišel	přijít	k5eAaPmAgInS
za	za	k7c7
Richardem	Richard	k1gMnSc7
s	s	k7c7
nabídkou	nabídka	k1gFnSc7
příměří	příměří	k1gNnSc2
a	a	k8xC
slíbil	slíbit	k5eAaPmAgMnS
křižákům	křižák	k1gMnPc3
pomoc	pomoc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Izák	Izák	k1gMnSc1
však	však	k9
po	po	k7c6
zjištění	zjištění	k1gNnSc6
stavu	stav	k1gInSc2
poškození	poškození	k1gNnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
flotily	flotila	k1gFnSc2
a	a	k8xC
unaveného	unavený	k2eAgNnSc2d1
křižáckého	křižácký	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
hned	hned	k6eAd1
po	po	k7c6
opuštění	opuštění	k1gNnSc6
města	město	k1gNnSc2
své	svůj	k3xOyFgInPc4
sliby	slib	k1gInPc4
odvolal	odvolat	k5eAaPmAgInS
a	a	k8xC
rozhodl	rozhodnout	k5eAaPmAgInS
se	se	k3xPyFc4
pro	pro	k7c4
válku	válka	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Richardovi	Richardův	k2eAgMnPc1d1
dorazily	dorazit	k5eAaPmAgFnP
nečekané	čekaný	k2eNgFnPc1d1
posily	posila	k1gFnPc1
v	v	k7c6
podobě	podoba	k1gFnSc6
kontingentu	kontingent	k1gInSc2
palestinských	palestinský	k2eAgMnPc2d1
křižáků	křižák	k1gMnPc2
vedených	vedený	k2eAgFnPc2d1
jeruzalémským	jeruzalémský	k2eAgMnSc7d1
králem	král	k1gMnSc7
Guyem	Guy	k1gMnSc7
de	de	k?
Lusignan	Lusignan	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
anglického	anglický	k2eAgMnSc2d1
krále	král	k1gMnSc2
připluli	připlout	k5eAaPmAgMnP
přivítat	přivítat	k5eAaPmF
a	a	k8xC
doufali	doufat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
získají	získat	k5eAaPmIp3nP
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
stranu	strana	k1gFnSc4
ve	v	k7c6
sporu	spor	k1gInSc6
mezi	mezi	k7c7
Guyem	Guy	k1gMnSc7
a	a	k8xC
Konrádem	Konrád	k1gMnSc7
z	z	k7c2
Montferratu	Montferrat	k1gInSc2
o	o	k7c4
jeruzalémskou	jeruzalémský	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teď	teď	k6eAd1
již	již	k9
převaha	převaha	k1gFnSc1
křižáků	křižák	k1gMnPc2
byla	být	k5eAaImAgFnS
zřejmá	zřejmý	k2eAgFnSc1d1
a	a	k8xC
Richard	Richard	k1gMnSc1
po	po	k7c6
svatbě	svatba	k1gFnSc6
s	s	k7c7
Berengarií	Berengarie	k1gFnSc7
vyrazil	vyrazit	k5eAaPmAgInS
za	za	k7c7
Izákem	Izák	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Richard	Richard	k1gMnSc1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgMnS
podrobit	podrobit	k5eAaPmF
si	se	k3xPyFc3
celý	celý	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obyvatelstvo	obyvatelstvo	k1gNnSc1
se	se	k3xPyFc4
nepostavilo	postavit	k5eNaPmAgNnS
na	na	k7c4
odpor	odpor	k1gInSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
kyperským	kyperský	k2eAgMnPc3d1
Řekům	Řek	k1gMnPc3
bylo	být	k5eAaImAgNnS
lhostejné	lhostejný	k2eAgNnSc1d1
<g/>
,	,	kIx,
zda	zda	k8xS
jim	on	k3xPp3gMnPc3
budou	být	k5eAaImBp3nP
vládnout	vládnout	k5eAaImF
křižáci	křižák	k1gMnPc1
nebo	nebo	k8xC
nenáviděný	nenáviděný	k2eAgInSc4d1
Komnenos	Komnenos	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
demoralizovaní	demoralizovaný	k2eAgMnPc1d1
Komnenovi	Komnenův	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
kladli	klást	k5eAaImAgMnP
proti	proti	k7c3
přesile	přesila	k1gFnSc3
jen	jen	k6eAd1
slabý	slabý	k2eAgInSc4d1
odpor	odpor	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
května	květen	k1gInSc2
křižáci	křižák	k1gMnPc1
ovládli	ovládnout	k5eAaPmAgMnP
celý	celý	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
a	a	k8xC
Richard	Richard	k1gMnSc1
jmenoval	jmenovat	k5eAaImAgMnS,k5eAaBmAgMnS
dva	dva	k4xCgInPc4
anglické	anglický	k2eAgInPc4d1
barony	baron	k1gMnPc4
správci	správce	k1gMnPc7
ostrova	ostrov	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Angličané	Angličan	k1gMnPc1
získali	získat	k5eAaPmAgMnP
značnou	značný	k2eAgFnSc4d1
kořist	kořist	k1gFnSc4
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Kypr	Kypr	k1gInSc1
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
stal	stát	k5eAaPmAgInS
nejdůležitější	důležitý	k2eAgFnSc7d3
křižáckou	křižácký	k2eAgFnSc7d1
pozicí	pozice	k1gFnSc7
ve	v	k7c6
východním	východní	k2eAgNnSc6d1
Středomoří	středomoří	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obléhání	obléhání	k1gNnSc1
Akkonu	Akkon	k1gInSc2
</s>
<s>
Obléhání	obléhání	k1gNnSc1
Akkonu	Akkon	k1gInSc2
<g/>
,	,	kIx,
středověká	středověký	k2eAgFnSc1d1
iluminace	iluminace	k1gFnSc1
</s>
<s>
V	v	k7c6
létě	léto	k1gNnSc6
roku	rok	k1gInSc2
1188	#num#	k4
byl	být	k5eAaImAgMnS
ze	z	k7c2
Saladinova	Saladinův	k2eAgNnSc2d1
zajetí	zajetí	k1gNnSc2
propuštěn	propustit	k5eAaPmNgMnS
jeruzalémský	jeruzalémský	k2eAgMnSc1d1
král	král	k1gMnSc1
Guy	Guy	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
vzápětí	vzápětí	k6eAd1
dostavil	dostavit	k5eAaPmAgMnS
do	do	k7c2
Tyru	Tyrus	k1gInSc2
a	a	k8xC
požadoval	požadovat	k5eAaImAgMnS
na	na	k7c6
Konrádovi	Konrád	k1gMnSc6
z	z	k7c2
Montferratu	Montferrat	k1gInSc2
vydání	vydání	k1gNnSc2
města	město	k1gNnSc2
a	a	k8xC
uznání	uznání	k1gNnSc2
královské	královský	k2eAgFnSc2d1
hodnosti	hodnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konrád	Konrád	k1gMnSc1
odmítl	odmítnout	k5eAaPmAgMnS
a	a	k8xC
dokonce	dokonce	k9
zakázal	zakázat	k5eAaPmAgMnS
Guyovi	Guya	k1gMnSc3
i	i	k8xC
jeho	jeho	k3xOp3gInSc6
doprovodu	doprovod	k1gInSc6
vstup	vstup	k1gInSc4
do	do	k7c2
města	město	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
Palestinští	palestinský	k2eAgMnPc1d1
baroni	baron	k1gMnPc1
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
rozdělili	rozdělit	k5eAaPmAgMnP
na	na	k7c4
příznivce	příznivec	k1gMnSc4
Konráda	Konrád	k1gMnSc4
z	z	k7c2
Montferratu	Montferrat	k1gInSc2
a	a	k8xC
Guye	Guy	k1gFnSc2
de	de	k?
Lusignan	Lusignan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Guy	Guy	k1gMnSc1
si	se	k3xPyFc3
uvědomoval	uvědomovat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
nečinností	nečinnost	k1gFnSc7
pod	pod	k7c7
hradbami	hradba	k1gFnPc7
Tyru	Tyrus	k1gInSc2
svou	svůj	k3xOyFgFnSc4
pozici	pozice	k1gFnSc4
zpět	zpět	k6eAd1
nezíská	získat	k5eNaPmIp3nS
a	a	k8xC
v	v	k7c6
srpnu	srpen	k1gInSc6
1189	#num#	k4
s	s	k7c7
nepočetným	početný	k2eNgInSc7d1
sborem	sbor	k1gInSc7
svých	svůj	k3xOyFgMnPc2
stoupenců	stoupenec	k1gMnPc2
a	a	k8xC
válečným	válečný	k2eAgNnSc7d1
loďstvem	loďstvo	k1gNnSc7
Pisánské	pisánský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
zaútočil	zaútočit	k5eAaPmAgInS
na	na	k7c4
Akkon	Akkon	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
dříve	dříve	k6eAd2
jedno	jeden	k4xCgNnSc4
z	z	k7c2
největších	veliký	k2eAgNnPc2d3
měst	město	k1gNnPc2
Jeruzalémského	jeruzalémský	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
v	v	k7c6
rukou	ruka	k1gFnPc6
Saladina	Saladin	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obráncům	obránce	k1gMnPc3
<g/>
,	,	kIx,
kterých	který	k3yIgMnPc2,k3yRgMnPc2,k3yQgMnPc2
bylo	být	k5eAaImAgNnS
mnohem	mnohem	k6eAd1
více	hodně	k6eAd2
než	než	k8xS
křižáků	křižák	k1gMnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
se	se	k3xPyFc4
Guye	Guye	k1gFnSc7
podařilo	podařit	k5eAaPmAgNnS
odrazit	odrazit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
i	i	k9
útok	útok	k1gInSc4
Saladina	Saladina	k1gFnSc1
ve	v	k7c6
snaze	snaha	k1gFnSc6
útočníky	útočník	k1gMnPc4
zahnat	zahnat	k5eAaPmF
selhal	selhat	k5eAaPmAgMnS
<g/>
,	,	kIx,
a	a	k8xC
Guyovi	Guyův	k2eAgMnPc1d1
křižáci	křižák	k1gMnPc1
tak	tak	k9
byli	být	k5eAaImAgMnP
sami	sám	k3xTgMnPc1
obklíčeni	obklíčen	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
se	se	k3xPyFc4
k	k	k7c3
obléhání	obléhání	k1gNnSc3
přidal	přidat	k5eAaPmAgMnS
i	i	k8xC
Konrád	Konrád	k1gMnSc1
z	z	k7c2
Montferratu	Montferrat	k1gInSc2
<g/>
,	,	kIx,
hrstka	hrstka	k1gFnSc1
účastníků	účastník	k1gMnPc2
výpravy	výprava	k1gFnSc2
Fridricha	Fridrich	k1gMnSc2
Barbarossy	Barbarossa	k1gMnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
500	#num#	k4
Normanů	Norman	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
10	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
dorazili	dorazit	k5eAaPmAgMnP
ze	z	k7c2
Sicílie	Sicílie	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
další	další	k2eAgMnPc1d1
křižáci	křižák	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
dorazili	dorazit	k5eAaPmAgMnP
z	z	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zimě	zima	k1gFnSc6
1189	#num#	k4
již	již	k6eAd1
byl	být	k5eAaImAgInS
Akkon	Akkon	k1gInSc1
bez	bez	k7c2
zásob	zásoba	k1gFnPc2
a	a	k8xC
v	v	k7c6
listopadu	listopad	k1gInSc6
1190	#num#	k4
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
od	od	k7c2
města	město	k1gNnSc2
zapudit	zapudit	k5eAaPmF
i	i	k9
Saladinovu	Saladinův	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Děpolt	Děpolt	k1gInSc1
u	u	k7c2
Akkonu	Akkon	k1gInSc2
na	na	k7c4
následky	následek	k1gInPc4
zranění	zranění	k1gNnPc2
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
či	či	k8xC
v	v	k7c6
důsledku	důsledek	k1gInSc6
epidemie	epidemie	k1gFnSc2
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příjezd	příjezd	k1gInSc1
krále	král	k1gMnSc2
Filipa	Filip	k1gMnSc2
Augusta	Augusta	k1gMnSc1
znamenal	znamenat	k5eAaImAgMnS
pro	pro	k7c4
křižáky	křižák	k1gInPc4
velkou	velký	k2eAgFnSc4d1
posilu	posila	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1191	#num#	k4
Richardových	Richardův	k2eAgInPc2d1
25	#num#	k4
lodí	loď	k1gFnPc2
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
dorazilo	dorazit	k5eAaPmAgNnS
k	k	k7c3
Tyru	Tyrum	k1gNnSc3
<g/>
,	,	kIx,
odmítl	odmítnout	k5eAaPmAgMnS
je	být	k5eAaImIp3nS
Konrád	Konrád	k1gMnSc1
vpustit	vpustit	k5eAaPmF
do	do	k7c2
přístavu	přístav	k1gInSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
mu	on	k3xPp3gMnSc3
již	již	k6eAd1
bylo	být	k5eAaImAgNnS
známo	znám	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
Richard	Richard	k1gMnSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
sporu	spor	k1gInSc6
o	o	k7c4
jeruzalémskou	jeruzalémský	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
na	na	k7c6
straně	strana	k1gFnSc6
Lusignana	Lusignan	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konrádovi	Konrádův	k2eAgMnPc5d1
stranil	stranit	k5eAaImAgInS
pro	pro	k7c4
změnu	změna	k1gFnSc4
Filip	Filip	k1gMnSc1
August	August	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Richard	Richard	k1gMnSc1
proto	proto	k8xC
vyplul	vyplout	k5eAaPmAgMnS
na	na	k7c4
sever	sever	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	s	k7c7
8	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1191	#num#	k4
vylodil	vylodit	k5eAaPmAgInS
před	před	k7c7
Akkonem	Akkon	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
Společně	společně	k6eAd1
s	s	k7c7
evropskými	evropský	k2eAgFnPc7d1
posilami	posila	k1gFnPc7
k	k	k7c3
překvapení	překvapení	k1gNnSc3
arabského	arabský	k2eAgMnSc2d1
kronikáře	kronikář	k1gMnSc2
připlulo	připlout	k5eAaPmAgNnS
i	i	k9
množství	množství	k1gNnSc1
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
podle	podle	k7c2
něj	on	k3xPp3gNnSc2
povětrných	povětrný	k2eAgNnPc2d1
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
</s>
<s>
...	...	k?
<g/>
připlulo	připlout	k5eAaPmAgNnS
tři	tři	k4xCgNnPc1
sta	sto	k4xCgNnPc1
krásných	krásný	k2eAgFnPc2d1
franckých	francký	k2eAgFnPc2d1
žen	žena	k1gFnPc2
v	v	k7c6
květu	květ	k1gInSc6
mládí	mládí	k1gNnSc2
a	a	k8xC
líbeznosti	líbeznost	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
shromáždily	shromáždit	k5eAaPmAgFnP
za	za	k7c7
mořem	moře	k1gNnSc7
a	a	k8xC
upsaly	upsat	k5eAaPmAgInP
se	se	k3xPyFc4
hříchu	hřích	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opustily	opustit	k5eAaPmAgFnP
svoji	svůj	k3xOyFgFnSc4
vlast	vlast	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
učinili	učinit	k5eAaImAgMnP,k5eAaPmAgMnP
nešťastné	šťastný	k2eNgNnSc4d1
šťastnými	šťastný	k2eAgMnPc7d1
<g/>
...	...	k?
Hořely	hořet	k5eAaImAgFnP
touhou	touha	k1gFnSc7
po	po	k7c6
soužití	soužití	k1gNnSc6
s	s	k7c7
muži	muž	k1gMnPc7
a	a	k8xC
po	po	k7c6
tělesném	tělesný	k2eAgNnSc6d1
spojení	spojení	k1gNnSc6
s	s	k7c7
nimi	on	k3xPp3gMnPc7
<g/>
...	...	k?
Kráčely	kráčet	k5eAaImAgInP
domýšlivě	domýšlivě	k6eAd1
s	s	k7c7
křížem	kříž	k1gInSc7
na	na	k7c6
prsou	prsa	k1gNnPc6
<g/>
...	...	k?
a	a	k8xC
říkaly	říkat	k5eAaImAgFnP
<g/>
,	,	kIx,
že	že	k8xS
touto	tento	k3xDgFnSc7
cestou	cesta	k1gFnSc7
se	se	k3xPyFc4
obětují	obětovat	k5eAaBmIp3nP
<g/>
,	,	kIx,
a	a	k8xC
věřily	věřit	k5eAaImAgFnP
<g/>
,	,	kIx,
že	že	k8xS
nemohou	moct	k5eNaImIp3nP
získat	získat	k5eAaPmF
boží	boží	k2eAgFnSc1d1
přízeň	přízeň	k1gFnSc1
žádnou	žádný	k3yNgFnSc7
lepší	dobrý	k2eAgFnSc7d2
obětí	oběť	k1gFnSc7
<g/>
,	,	kIx,
než	než	k8xS
je	být	k5eAaImIp3nS
právě	právě	k6eAd1
tato	tento	k3xDgFnSc1
<g/>
...	...	k?
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Baha	Baha	k1gMnSc1
ad-Dín	ad-Dín	k1gMnSc1
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Merry-Joseph	Merry-Joseph	k1gInSc1
Blondel	blondel	k1gInSc1
<g/>
:	:	kIx,
Předání	předání	k1gNnSc1
Akkonu	Akkon	k1gInSc6
Filipu	Filip	k1gMnSc3
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Křižáci	křižák	k1gMnPc1
zkonstruovali	zkonstruovat	k5eAaPmAgMnP
před	před	k7c7
městem	město	k1gNnSc7
mohutné	mohutný	k2eAgFnSc2d1
obléhací	obléhací	k2eAgInPc4d1
stroje	stroj	k1gInPc1
a	a	k8xC
anglické	anglický	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
pomohlo	pomoct	k5eAaPmAgNnS
přístav	přístav	k1gInSc4
z	z	k7c2
moře	moře	k1gNnSc2
dokonale	dokonale	k6eAd1
uzavřít	uzavřít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obránci	obránce	k1gMnPc1
Akkonu	Akkon	k1gInSc2
se	se	k3xPyFc4
bránili	bránit	k5eAaImAgMnP
za	za	k7c4
použití	použití	k1gNnSc4
řeckého	řecký	k2eAgInSc2d1
ohně	oheň	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
Kdykoliv	kdykoliv	k6eAd1
křižáci	křižák	k1gMnPc1
zaútočili	zaútočit	k5eAaPmAgMnP
na	na	k7c4
hradby	hradba	k1gFnPc4
<g/>
,	,	kIx,
vpadl	vpadnout	k5eAaPmAgMnS
Saladin	Saladin	k2eAgMnSc1d1
ze	z	k7c2
svého	svůj	k3xOyFgMnSc2
tábora	tábor	k1gMnSc2
do	do	k7c2
křižáckého	křižácký	k2eAgInSc2d1
týlu	týl	k1gInSc2
a	a	k8xC
donutil	donutit	k5eAaPmAgMnS
je	být	k5eAaImIp3nS
se	se	k3xPyFc4
stáhnout	stáhnout	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
Vojenské	vojenský	k2eAgFnSc2d1
akce	akce	k1gFnSc2
pokračovaly	pokračovat	k5eAaImAgFnP
i	i	k9
přes	přes	k7c4
horečnaté	horečnatý	k2eAgNnSc4d1
onemocnění	onemocnění	k1gNnSc4
obou	dva	k4xCgMnPc2
evropských	evropský	k2eAgMnPc2d1
panovníků	panovník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
malárii	malárie	k1gFnSc4
zemřely	zemřít	k5eAaPmAgFnP
i	i	k9
mnohé	mnohý	k2eAgFnPc1d1
významné	významný	k2eAgFnPc1d1
osobnosti	osobnost	k1gFnPc1
<g/>
;	;	kIx,
jeruzalémský	jeruzalémský	k2eAgMnSc1d1
patriarcha	patriarcha	k1gMnSc1
Heraklios	Heraklios	k1gMnSc1
<g/>
,	,	kIx,
hrabě	hrabě	k1gMnSc1
Filip	Filip	k1gMnSc1
Flanderský	flanderský	k2eAgMnSc1d1
<g/>
,	,	kIx,
či	či	k8xC
manželka	manželka	k1gFnSc1
Guye	Guye	k1gFnPc2
de	de	k?
Lusignan	Lusignana	k1gFnPc2
královna	královna	k1gFnSc1
Sibyla	Sibyla	k1gFnSc1
i	i	k8xC
jejich	jejich	k3xOp3gFnSc2
dcery	dcera	k1gFnSc2
Alix	Alix	k1gInSc1
a	a	k8xC
Marie	Marie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flanderský	flanderský	k2eAgMnSc1d1
hrabě	hrabě	k1gMnSc1
nezanechal	zanechat	k5eNaPmAgMnS
přímého	přímý	k2eAgMnSc4d1
dědice	dědic	k1gMnSc4
svého	svůj	k1gMnSc2
panství	panství	k1gNnSc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
anglickým	anglický	k2eAgMnSc7d1
a	a	k8xC
francouzským	francouzský	k2eAgMnSc7d1
králem	král	k1gMnSc7
tak	tak	k8xS,k8xC
vyvstal	vyvstat	k5eAaPmAgInS
spor	spor	k1gInSc4
<g/>
;	;	kIx,
podle	podle	k7c2
feudálních	feudální	k2eAgFnPc2d1
zásad	zásada	k1gFnPc2
mělo	mít	k5eAaImAgNnS
Flanderské	flanderský	k2eAgNnSc4d1
hrabství	hrabství	k1gNnSc4
připadnou	připadnout	k5eAaPmIp3nP
Filipu	Filip	k1gMnSc3
Augustovi	August	k1gMnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
Richard	Richard	k1gMnSc1
mu	on	k3xPp3gMnSc3
připomněl	připomnět	k5eAaPmAgMnS
messinskou	messinský	k2eAgFnSc4d1
dohodu	dohoda	k1gFnSc4
<g/>
,	,	kIx,
podle	podle	k7c2
níž	jenž	k3xRgFnSc2
si	se	k3xPyFc3
Richard	Richard	k1gMnSc1
nárokoval	nárokovat	k5eAaImAgMnS
polovinu	polovina	k1gFnSc4
Flander	Flandry	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
55	#num#	k4
<g/>
]	]	kIx)
Filip	Filip	k1gMnSc1
August	August	k1gMnSc1
se	se	k3xPyFc4
naopak	naopak	k6eAd1
ohradil	ohradit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
tím	ten	k3xDgNnSc7
by	by	kYmCp3nS
si	se	k3xPyFc3
mohl	moct	k5eAaImAgMnS
nárokovat	nárokovat	k5eAaImF
i	i	k9
polovinu	polovina	k1gFnSc4
dobytého	dobytý	k2eAgInSc2d1
Kypru	Kypr	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
však	však	k9
Richard	Richard	k1gMnSc1
odmítal	odmítat	k5eAaImAgMnS
<g/>
,	,	kIx,
a	a	k8xC
celý	celý	k2eAgInSc1d1
spor	spor	k1gInSc1
tak	tak	k9
nakonec	nakonec	k6eAd1
utichl	utichnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
Přiostřil	přiostřit	k5eAaPmAgMnS
se	se	k3xPyFc4
také	také	k9
spor	spor	k1gInSc1
mezi	mezi	k7c7
Konrádem	Konrád	k1gMnSc7
a	a	k8xC
Guyem	Guy	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
smrti	smrt	k1gFnSc6
královny	královna	k1gFnSc2
Sibyly	Sibyla	k1gFnSc2
ztratil	ztratit	k5eAaPmAgInS
Guy	Guy	k1gFnSc4
nárok	nárok	k1gInSc1
na	na	k7c4
korunu	koruna	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
přímou	přímý	k2eAgFnSc7d1
dědičkou	dědička	k1gFnSc7
byla	být	k5eAaImAgFnS
Sibylina	Sibylin	k2eAgFnSc1d1
sestra	sestra	k1gFnSc1
Isabela	Isabela	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konrád	Konrád	k1gMnSc1
toho	ten	k3xDgMnSc4
využil	využít	k5eAaPmAgMnS
<g/>
,	,	kIx,
nechal	nechat	k5eAaPmAgMnS
mladou	mladý	k2eAgFnSc4d1
Isabelu	Isabela	k1gFnSc4
rozvést	rozvést	k5eAaPmF
s	s	k7c7
jejím	její	k3xOp3gMnSc7
manželem	manžel	k1gMnSc7
Onfroyem	Onfroy	k1gMnSc7
z	z	k7c2
Toronu	toron	k1gInSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
Guyových	Guyův	k2eAgMnPc2d1
nejbližších	blízký	k2eAgMnPc2d3
spojenců	spojenec	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
pak	pak	k6eAd1
se	se	k3xPyFc4
s	s	k7c7
ní	on	k3xPp3gFnSc7
sám	sám	k3xTgMnSc1
oženil	oženit	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
Konrád	Konrád	k1gMnSc1
se	se	k3xPyFc4
tak	tak	k9
stal	stát	k5eAaPmAgInS
oficiálním	oficiální	k2eAgMnSc7d1
uchazečem	uchazeč	k1gMnSc7
o	o	k7c4
královský	královský	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
Obléhací	obléhací	k2eAgInPc1d1
stroje	stroj	k1gInPc1
krále	král	k1gMnSc2
Francie	Francie	k1gFnSc2
rozbořily	rozbořit	k5eAaPmAgFnP
hradby	hradba	k1gFnPc1
města	město	k1gNnSc2
do	do	k7c2
té	ten	k3xDgFnSc2
míry	míra	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
prorazit	prorazit	k5eAaPmF
dovnitř	dovnitř	k6eAd1
a	a	k8xC
pustit	pustit	k5eAaPmF
se	se	k3xPyFc4
boje	boj	k1gInSc2
muže	muž	k1gMnPc4
proti	proti	k7c3
muži	muž	k1gMnSc3
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
věhlas	věhlas	k1gInSc4
krále	král	k1gMnSc2
Anglie	Anglie	k1gFnSc2
a	a	k8xC
jeho	jeho	k3xOp3gInPc2
činů	čin	k1gInPc2
tak	tak	k6eAd1
[	[	kIx(
<g/>
muslimy	muslim	k1gMnPc7
<g/>
]	]	kIx)
vyděsil	vyděsit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
vzdávali	vzdávat	k5eAaImAgMnP
naděje	naděje	k1gFnSc1
na	na	k7c4
záchranu	záchrana	k1gFnSc4
svých	svůj	k3xOyFgInPc2
životů	život	k1gInPc2
<g/>
...	...	k?
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
z	z	k7c2
kroniky	kronika	k1gFnSc2
Viléma	Vilém	k1gMnSc2
z	z	k7c2
Tyru	Tyrus	k1gInSc2
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Králové	Králové	k2eAgMnSc1d1
Richard	Richard	k1gMnSc1
Lví	lví	k2eAgNnSc4d1
srdce	srdce	k1gNnSc4
a	a	k8xC
Filip	Filip	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
přijímají	přijímat	k5eAaImIp3nP
kapitulaci	kapitulace	k1gFnSc4
Akkonu	Akkon	k1gInSc2
<g/>
(	(	kIx(
<g/>
Grandes	Grandes	k1gInSc1
Chroniques	Chroniques	k1gInSc1
de	de	k?
France	Franc	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1191	#num#	k4
Akkon	Akkon	k1gMnSc1
kapituloval	kapitulovat	k5eAaBmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Emírové	emír	k1gMnPc1
z	z	k7c2
Akkonu	Akkon	k1gInSc2
přislíbili	přislíbit	k5eAaPmAgMnP
zaplatit	zaplatit	k5eAaPmF
200	#num#	k4
000	#num#	k4
zlaťáků	zlaťák	k1gInPc2
a	a	k8xC
vrátit	vrátit	k5eAaPmF
1500	#num#	k4
křesťanských	křesťanský	k2eAgMnPc2d1
zajatců	zajatec	k1gMnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
stovky	stovka	k1gFnSc2
urozených	urozený	k2eAgMnPc2d1
a	a	k8xC
navrátit	navrátit	k5eAaPmF
relikvii	relikvie	k1gFnSc4
Pravý	pravý	k2eAgInSc1d1
kříž	kříž	k1gInSc1
<g/>
,	,	kIx,
ukořistěný	ukořistěný	k2eAgInSc1d1
u	u	k7c2
Hattínu	Hattín	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
Guy	Guy	k1gMnSc1
de	de	k?
Lusignan	Lusignan	k1gMnSc1
se	se	k3xPyFc4
nyní	nyní	k6eAd1
podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
Konrád	Konrád	k1gMnSc1
mohl	moct	k5eAaImAgMnS
opírat	opírat	k5eAaImF
o	o	k7c4
silné	silný	k2eAgNnSc4d1
město	město	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
Evropští	evropský	k2eAgMnPc1d1
králové	král	k1gMnPc1
spor	spor	k1gInSc4
o	o	k7c4
jeruzalémský	jeruzalémský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
museli	muset	k5eAaImAgMnP
vyřešit	vyřešit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konráda	Konrád	k1gMnSc4
podpořil	podpořit	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
bratranec	bratranec	k1gMnSc1
Filip	Filip	k1gMnSc1
August	August	k1gMnSc1
i	i	k8xC
rakouský	rakouský	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
Leopold	Leopold	k1gMnSc1
Babenberský	babenberský	k2eAgMnSc1d1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
po	po	k7c6
smrti	smrt	k1gFnSc6
Fridricha	Fridrich	k1gMnSc2
Švábského	švábský	k2eAgMnSc2d1
vedl	vést	k5eAaImAgInS
zbytek	zbytek	k1gInSc4
Barbarossových	Barbarossův	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Guye	Guye	k1gInSc4
podporoval	podporovat	k5eAaImAgMnS
Richard	Richard	k1gMnSc1
Lví	lví	k2eAgNnSc4d1
srdce	srdce	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
bylo	být	k5eAaImAgNnS
ujednáno	ujednat	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
Guy	Guy	k1gMnSc1
zůstane	zůstat	k5eAaPmIp3nS
králem	král	k1gMnSc7
a	a	k8xC
Konrád	Konrád	k1gMnSc1
s	s	k7c7
Isabelou	Isabela	k1gFnSc7
nastoupí	nastoupit	k5eAaPmIp3nP
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
Filip	Filip	k1gMnSc1
August	August	k1gMnSc1
se	se	k3xPyFc4
poté	poté	k6eAd1
začal	začít	k5eAaPmAgInS
chystat	chystat	k5eAaImF
na	na	k7c4
návrat	návrat	k1gInSc4
do	do	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
tak	tak	k9
i	i	k9
vévoda	vévoda	k1gMnSc1
Leopold	Leopold	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
Richard	Richard	k1gMnSc1
se	se	k3xPyFc4
Filipa	Filip	k1gMnSc2
snažil	snažit	k5eAaImAgMnS
přemluvit	přemluvit	k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zůstal	zůstat	k5eAaPmAgMnS
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
obával	obávat	k5eAaImAgMnS
o	o	k7c4
anglické	anglický	k2eAgFnPc4d1
kontinentální	kontinentální	k2eAgFnPc4d1
državy	država	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
nepodařilo	podařit	k5eNaPmAgNnS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
to	ten	k3xDgNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
Filip	Filip	k1gMnSc1
sice	sice	k8xC
odpřisáhl	odpřisáhnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
anglická	anglický	k2eAgNnPc4d1
území	území	k1gNnPc4
neohrozí	ohrozit	k5eNaPmIp3nP
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
ale	ale	k8xC
návrat	návrat	k1gInSc4
Filipovi	Filip	k1gMnSc3
umožnil	umožnit	k5eAaPmAgMnS
uspořádat	uspořádat	k5eAaPmF
tažení	tažení	k1gNnSc4
proti	proti	k7c3
Flandrám	Flandra	k1gFnPc3
a	a	k8xC
Normandii	Normandie	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
Francouzský	francouzský	k2eAgMnSc1d1
král	král	k1gMnSc1
sice	sice	k8xC
v	v	k7c6
Palestině	Palestina	k1gFnSc6
ponechal	ponechat	k5eAaPmAgMnS
pod	pod	k7c7
velením	velení	k1gNnSc7
burgundského	burgundský	k2eAgMnSc2d1
vévody	vévoda	k1gMnSc2
Huga	Hugo	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
silný	silný	k2eAgInSc1d1
oddíl	oddíl	k1gInSc1
francouzských	francouzský	k2eAgMnPc2d1
křižáků	křižák	k1gMnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
anglickými	anglický	k2eAgMnPc7d1
kronikáři	kronikář	k1gMnPc7
však	však	k8xC
byl	být	k5eAaImAgInS
jeho	jeho	k3xOp3gNnSc1
odjezd	odjezd	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
interpretován	interpretovat	k5eAaBmNgInS
jako	jako	k8xC,k8xS
zrada	zrada	k1gFnSc1
křesťanstva	křesťanstvo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
Palestinští	palestinský	k2eAgMnPc1d1
baroni	baron	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
ve	v	k7c6
Svaté	svatý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
již	již	k6eAd1
narodili	narodit	k5eAaPmAgMnP
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
francouzského	francouzský	k2eAgMnSc4d1
původu	původa	k1gMnSc4
a	a	k8xC
Filipa	Filip	k1gMnSc4
Augusta	August	k1gMnSc4
coby	coby	k?
krále	král	k1gMnSc4
Francie	Francie	k1gFnSc2
považovali	považovat	k5eAaImAgMnP
za	za	k7c4
svého	svůj	k3xOyFgMnSc4
nejvyššího	vysoký	k2eAgMnSc4d3
vládce	vládce	k1gMnSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
anglický	anglický	k2eAgMnSc1d1
král	král	k1gMnSc1
Richard	Richard	k1gMnSc1
byl	být	k5eAaImAgMnS
pro	pro	k7c4
ně	on	k3xPp3gInPc4
jako	jako	k9
vévoda	vévoda	k1gMnSc1
Akvitánie	Akvitánie	k1gFnSc2
a	a	k8xC
Normandie	Normandie	k1gFnSc2
pouhý	pouhý	k2eAgMnSc1d1
vazal	vazal	k1gMnSc1
francouzského	francouzský	k2eAgMnSc2d1
krále	král	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
Nyní	nyní	k6eAd1
po	po	k7c6
odjezdu	odjezd	k1gInSc6
Filipa	Filip	k1gMnSc2
Augusta	August	k1gMnSc2
se	se	k3xPyFc4
Richard	Richard	k1gMnSc1
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc7
mužem	muž	k1gMnSc7
ve	v	k7c6
Svaté	svatý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tažení	tažení	k1gNnSc1
Richarda	Richard	k1gMnSc2
Lví	lví	k2eAgNnSc1d1
srdce	srdce	k1gNnSc1
</s>
<s>
Poprava	poprava	k1gFnSc1
zajatců	zajatec	k1gMnPc2
</s>
<s>
Hromadná	hromadný	k2eAgFnSc1d1
poprava	poprava	k1gFnSc1
muslimských	muslimský	k2eAgMnPc2d1
zajatců	zajatec	k1gMnPc2
<g/>
,	,	kIx,
The	The	k1gFnSc1
History	Histor	k1gInPc1
of	of	k?
France	Franc	k1gMnSc4
from	from	k1gMnSc1
the	the	k?
Earliest	Earliest	k1gMnSc1
Times	Times	k1gMnSc1
to	ten	k3xDgNnSc1
the	the	k?
Year	Year	k1gInSc1
1789	#num#	k4
</s>
<s>
Richardovou	Richardův	k2eAgFnSc7d1
první	první	k4xOgFnSc6
starostí	starost	k1gFnPc2
bylo	být	k5eAaImAgNnS
zajištění	zajištění	k1gNnSc1
splnění	splnění	k1gNnSc2
podmínek	podmínka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
slíbila	slíbit	k5eAaPmAgFnS
akkonská	akkonský	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
při	při	k7c6
kapitulaci	kapitulace	k1gFnSc6
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saladin	Saladina	k1gFnPc2
byl	být	k5eAaImAgMnS
ochoten	ochoten	k2eAgMnSc1d1
podmínky	podmínka	k1gFnPc4
vyjednané	vyjednaný	k2eAgFnPc1d1
svými	svůj	k3xOyFgMnPc7
emíry	emír	k1gMnPc7
splnit	splnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přijal	přijmout	k5eAaPmAgMnS
Richardovi	Richard	k1gMnSc3
posly	posel	k1gMnPc7
<g/>
,	,	kIx,
propustil	propustit	k5eAaPmAgInS
část	část	k1gFnSc4
zajatců	zajatec	k1gMnPc2
a	a	k8xC
zaplatil	zaplatit	k5eAaPmAgMnS
část	část	k1gFnSc4
slíbeného	slíbený	k2eAgNnSc2d1
výkupného	výkupné	k1gNnSc2
<g/>
,	,	kIx,
rovněž	rovněž	k9
jim	on	k3xPp3gMnPc3
ukázal	ukázat	k5eAaPmAgMnS
Pravý	pravý	k2eAgInSc4d1
kříž	kříž	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c7
křesťanskými	křesťanský	k2eAgMnPc7d1
zajatci	zajatec	k1gMnPc7
však	však	k8xC
nebyl	být	k5eNaImAgMnS
ani	ani	k8xC
jediný	jediný	k2eAgMnSc1d1
rytíř	rytíř	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
Saladin	Saladin	k2eAgInSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
žádné	žádný	k3yNgMnPc4
rytíře	rytíř	k1gMnSc4
v	v	k7c6
zajetí	zajetí	k1gNnSc6
neměl	mít	k5eNaImAgMnS
<g/>
,	,	kIx,
neboť	neboť	k8xC
urozené	urozený	k2eAgMnPc4d1
zajatce	zajatec	k1gMnPc4
buď	buď	k8xC
brzy	brzy	k6eAd1
propustil	propustit	k5eAaPmAgInS
za	za	k7c4
výkupné	výkupné	k1gNnSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
dal	dát	k5eAaPmAgMnS
popravit	popravit	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
zajetí	zajetí	k1gNnSc6
je	být	k5eAaImIp3nS
dlouho	dlouho	k6eAd1
nedržel	držet	k5eNaImAgMnS
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
však	však	k9
akkonská	akkonský	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
s	s	k7c7
křižáky	křižák	k1gMnPc7
vyjednávala	vyjednávat	k5eAaImAgFnS
<g/>
,	,	kIx,
nevěděla	vědět	k5eNaImAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
]	]	kIx)
Richard	Richard	k1gMnSc1
okamžitě	okamžitě	k6eAd1
požadoval	požadovat	k5eAaImAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Saladin	Saladin	k2eAgInSc1d1
rytíře	rytíř	k1gMnSc4
propustil	propustit	k5eAaPmAgInS
a	a	k8xC
když	když	k8xS
sultán	sultán	k1gMnSc1
odmítl	odmítnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
to	ten	k3xDgNnSc4
prohlásil	prohlásit	k5eAaPmAgMnS
za	za	k7c4
porušení	porušení	k1gNnSc4
mírových	mírový	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
20	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1191	#num#	k4
bylo	být	k5eAaImAgNnS
2700	#num#	k4
muslimských	muslimský	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
Akkonu	Akkon	k1gInSc2
vyvedeno	vyveden	k2eAgNnSc1d1
za	za	k7c4
město	město	k1gNnSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
nahnáno	nahnán	k2eAgNnSc1d1
do	do	k7c2
ohrady	ohrada	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byli	být	k5eAaImAgMnP
všichni	všechen	k3xTgMnPc1
<g/>
,	,	kIx,
včetně	včetně	k7c2
žen	žena	k1gFnPc2
a	a	k8xC
dětí	dítě	k1gFnPc2
<g/>
,	,	kIx,
anglickými	anglický	k2eAgInPc7d1
vojáky	voják	k1gMnPc4
zmasakrováni	zmasakrován	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
Saladinovy	Saladinův	k2eAgFnPc1d1
předsunuté	předsunutý	k2eAgFnPc1d1
hlídky	hlídka	k1gFnPc1
se	se	k3xPyFc4
pokusily	pokusit	k5eAaPmAgFnP
zajatce	zajatec	k1gMnPc4
osvobodit	osvobodit	k5eAaPmF
<g/>
,	,	kIx,
muslimští	muslimský	k2eAgMnPc1d1
jezdci	jezdec	k1gMnPc1
však	však	k9
byli	být	k5eAaImAgMnP
odraženi	odrazit	k5eAaPmNgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
Nepřítel	nepřítel	k1gMnSc1
pak	pak	k6eAd1
vyvedl	vyvést	k5eAaPmAgMnS
muslimské	muslimský	k2eAgMnPc4d1
zajatce	zajatec	k1gMnPc4
<g/>
...	...	k?
asi	asi	k9
3000	#num#	k4
osob	osoba	k1gFnPc2
svázaných	svázaný	k2eAgFnPc2d1
provazy	provaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
je	být	k5eAaImIp3nS
jako	jako	k9
jednoho	jeden	k4xCgMnSc4
muže	muž	k1gMnSc4
obvinili	obvinit	k5eAaPmAgMnP
a	a	k8xC
chladnokrevně	chladnokrevně	k6eAd1
zavraždili	zavraždit	k5eAaPmAgMnP
bodáním	bodání	k1gNnSc7
a	a	k8xC
ranami	rána	k1gFnPc7
mečem	meč	k1gInSc7
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
muslimské	muslimský	k2eAgFnPc1d1
stráže	stráž	k1gFnPc1
přihlížely	přihlížet	k5eAaImAgFnP
a	a	k8xC
nevěděly	vědět	k5eNaImAgFnP
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
dělat	dělat	k5eAaImF
<g/>
,	,	kIx,
neboť	neboť	k8xC
byly	být	k5eAaImAgFnP
příliš	příliš	k6eAd1
daleko	daleko	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Baha	Baha	k1gMnSc1
ad-Dín	ad-Dín	k1gMnSc1
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Arsufu	Arsuf	k1gInSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Bitva	bitva	k1gFnSc1
u	u	k7c2
Arsufu	Arsuf	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
vyrazili	vyrazit	k5eAaPmAgMnP
z	z	k7c2
Akkonu	Akkon	k1gInSc2
angličtí	anglický	k2eAgMnPc1d1
a	a	k8xC
francouzští	francouzský	k2eAgMnPc1d1
křižáci	křižák	k1gMnPc1
<g/>
,	,	kIx,
společně	společně	k6eAd1
s	s	k7c7
oddíly	oddíl	k1gInPc7
řádu	řád	k1gInSc2
templářů	templář	k1gMnPc2
a	a	k8xC
johanitů	johanita	k1gMnPc2
na	na	k7c4
jih	jih	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
zabezpečili	zabezpečit	k5eAaPmAgMnP
pobřežní	pobřežní	k1gMnPc1
města	město	k1gNnSc2
pro	pro	k7c4
tažení	tažení	k1gNnSc4
na	na	k7c4
Jeruzalém	Jeruzalém	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
Společně	společně	k6eAd1
s	s	k7c7
vojskem	vojsko	k1gNnSc7
postupovala	postupovat	k5eAaImAgFnS
i	i	k9
anglická	anglický	k2eAgFnSc1d1
flotila	flotila	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
z	z	k7c2
moře	moře	k1gNnSc2
křižákům	křižák	k1gInPc3
kryla	krýt	k5eAaImAgFnS
bok	bok	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
Saladin	Saladina	k1gFnPc2
svou	svůj	k3xOyFgFnSc4
hlavní	hlavní	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
shromáždil	shromáždit	k5eAaPmAgInS
v	v	k7c6
Arsufu	Arsuf	k1gInSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
svedl	svést	k5eAaPmAgInS
rozhodnou	rozhodný	k2eAgFnSc4d1
bitvu	bitva	k1gFnSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
jeho	jeho	k3xOp3gFnSc1
jízda	jízda	k1gFnSc1
podnikala	podnikat	k5eAaImAgFnS
drobné	drobný	k2eAgInPc4d1
nájezdy	nájezd	k1gInPc4
na	na	k7c4
křižácké	křižácký	k2eAgInPc4d1
útvary	útvar	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
vedru	vedro	k1gNnSc3
křižákům	křižák	k1gInPc3
trvalo	trvat	k5eAaImAgNnS
plné	plný	k2eAgInPc4d1
dva	dva	k4xCgInPc4
týdny	týden	k1gInPc4
<g/>
,	,	kIx,
než	než	k8xS
dorazili	dorazit	k5eAaPmAgMnP
od	od	k7c2
Akkonu	Akkon	k1gInSc2
k	k	k7c3
Arsufu	Arsuf	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
utkali	utkat	k5eAaPmAgMnP
se	s	k7c7
Saladinem	Saladin	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Eloi	Eloi	k6eAd1
Firmin	Firmin	k2eAgInSc1d1
Feron	Feron	k1gInSc1
<g/>
:	:	kIx,
Bitva	bitva	k1gFnSc1
u	u	k7c2
Arsufu	Arsuf	k1gInSc2
</s>
<s>
Dne	den	k1gInSc2
5	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
se	se	k3xPyFc4
Richard	Richard	k1gMnSc1
sešel	sejít	k5eAaPmAgMnS
se	s	k7c7
Saladinovým	Saladinový	k2eAgMnSc7d1
diplomatem	diplomat	k1gMnSc7
al-Ádilem	al-Ádil	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Richard	Richard	k1gMnSc1
požadoval	požadovat	k5eAaImAgMnS
odstoupení	odstoupení	k1gNnSc4
celé	celý	k2eAgFnSc2d1
Palestiny	Palestina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Al-Ádil	Al-Ádil	k1gMnSc1
to	ten	k3xDgNnSc4
odmítl	odmítnout	k5eAaPmAgMnS
a	a	k8xC
jednání	jednání	k1gNnSc1
tak	tak	k6eAd1
zkrachovalo	zkrachovat	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
Bitva	bitva	k1gFnSc1
se	se	k3xPyFc4
odehrála	odehrát	k5eAaPmAgFnS
o	o	k7c4
dva	dva	k4xCgInPc4
dny	den	k1gInPc4
později	pozdě	k6eAd2
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1191	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
Richard	Richard	k1gMnSc1
proti	proti	k7c3
Saladinovi	Saladin	k1gMnSc3
použil	použít	k5eAaPmAgMnS
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
předchozích	předchozí	k2eAgFnPc2d1
bitev	bitva	k1gFnPc2
křižáků	křižák	k1gMnPc2
s	s	k7c7
muslimy	muslim	k1gMnPc7
novou	nový	k2eAgFnSc4d1
taktiku	taktika	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
67	#num#	k4
<g/>
]	]	kIx)
Zaujal	zaujmout	k5eAaPmAgInS
obrannou	obranný	k2eAgFnSc4d1
pozici	pozice	k1gFnSc4
a	a	k8xC
odrážel	odrážet	k5eAaImAgMnS
muslimské	muslimský	k2eAgInPc4d1
útoky	útok	k1gInPc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
mezi	mezi	k7c7
jednotlivými	jednotlivý	k2eAgFnPc7d1
vlnami	vlna	k1gFnPc7
angličtí	anglický	k2eAgMnPc1d1
lučištníci	lučištník	k1gMnPc1
–	–	k?
v	v	k7c6
křižáckém	křižácký	k2eAgNnSc6d1
vojsku	vojsko	k1gNnSc6
naprostá	naprostý	k2eAgFnSc1d1
novinka	novinka	k1gFnSc1
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
–	–	k?
tvrdě	tvrdě	k6eAd1
zasahovali	zasahovat	k5eAaImAgMnP
muslimskou	muslimský	k2eAgFnSc4d1
jízdu	jízda	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Richard	Richard	k1gMnSc1
oddaloval	oddalovat	k5eAaImAgMnS
konečný	konečný	k2eAgInSc4d1
výpad	výpad	k1gInSc4
<g/>
,	,	kIx,
až	až	k9
nakonec	nakonec	k6eAd1
špitálníci	špitálník	k1gMnPc1
vyrazili	vyrazit	k5eAaPmAgMnP
bez	bez	k7c2
rozkazu	rozkaz	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
...	...	k?
<g/>
začali	začít	k5eAaPmAgMnP
svůj	svůj	k3xOyFgInSc4
útok	útok	k1gInSc4
se	s	k7c7
jménem	jméno	k1gNnSc7
Všemohoucího	Všemohoucí	k1gMnSc2
na	na	k7c6
rtech	ret	k1gInPc6
a	a	k8xC
křičeli	křičet	k5eAaImAgMnP
hlasitě	hlasitě	k6eAd1
Svatý	svatý	k1gMnSc1
Jiří	Jiří	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
Boží	božit	k5eAaImIp3nP
pak	pak	k6eAd1
obrátili	obrátit	k5eAaPmAgMnP
své	svůj	k3xOyFgMnPc4
koně	kůň	k1gMnSc4
proti	proti	k7c3
nepříteli	nepřítel	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Ambroise	Ambroise	k1gFnSc1
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Richard	Richard	k1gMnSc1
nemohl	moct	k5eNaImAgMnS
masu	masa	k1gFnSc4
svého	svůj	k3xOyFgNnSc2
vojska	vojsko	k1gNnSc2
zadržet	zadržet	k5eAaPmF
<g/>
,	,	kIx,
vyrazil	vyrazit	k5eAaPmAgMnS
tedy	tedy	k9
také	také	k9
<g/>
,	,	kIx,
ale	ale	k8xC
útok	útok	k1gInSc1
byl	být	k5eAaImAgInS
předčasný	předčasný	k2eAgMnSc1d1
a	a	k8xC
mnoho	mnoho	k4c1
Saladinových	Saladinový	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
před	před	k7c7
křižáky	křižák	k1gMnPc7
stačilo	stačit	k5eAaBmAgNnS
uniknout	uniknout	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
Saladin	Saladina	k1gFnPc2
utrpěl	utrpět	k5eAaPmAgMnS
porážku	porážka	k1gFnSc4
a	a	k8xC
i	i	k9
když	když	k8xS
ztráty	ztráta	k1gFnSc2
na	na	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
byly	být	k5eAaImAgFnP
malé	malý	k2eAgFnPc1d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
Saladinova	Saladinův	k2eAgFnSc1d1
pověst	pověst	k1gFnSc1
o	o	k7c6
neporazitelnosti	neporazitelnost	k1gFnSc6
byla	být	k5eAaImAgFnS
pryč	pryč	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Křižáci	křižák	k1gMnPc1
poté	poté	k6eAd1
obsadili	obsadit	k5eAaPmAgMnP
přístav	přístav	k1gInSc4
Jaffa	Jaffa	k1gFnSc1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Saladin	Saladina	k1gFnPc2
v	v	k7c6
očekávání	očekávání	k1gNnSc6
dalšího	další	k2eAgInSc2d1
křižáckého	křižácký	k2eAgInSc2d1
postupu	postup	k1gInSc2
na	na	k7c4
Jeruzalém	Jeruzalém	k1gInSc4
obsadil	obsadit	k5eAaPmAgMnS
Ramlu	Ramla	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
Když	když	k8xS
se	se	k3xPyFc4
Saladin	Saladin	k2eAgMnSc1d1
však	však	k9
dozvěděl	dozvědět	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
křižáci	křižák	k1gMnPc1
obsadili	obsadit	k5eAaPmAgMnP
Jaffu	Jaffa	k1gFnSc4
<g/>
,	,	kIx,
vydal	vydat	k5eAaPmAgInS
rozkaz	rozkaz	k1gInSc1
zničit	zničit	k5eAaPmF
bohaté	bohatý	k2eAgNnSc4d1
město	město	k1gNnSc4
Askalon	Askalon	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
pro	pro	k7c4
křižáky	křižák	k1gInPc4
ztratilo	ztratit	k5eAaPmAgNnS
svůj	svůj	k3xOyFgInSc4
vojenský	vojenský	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
Richard	Richard	k1gMnSc1
ale	ale	k8xC
netáhl	táhnout	k5eNaImAgMnS
ani	ani	k8xC
na	na	k7c4
Askalon	Askalon	k1gInSc4
<g/>
,	,	kIx,
ani	ani	k8xC
na	na	k7c4
Jeruzalém	Jeruzalém	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Tyru	Tyrus	k1gInSc6
začal	začít	k5eAaPmAgInS
růst	růst	k1gInSc1
vliv	vliv	k1gInSc4
opozice	opozice	k1gFnSc2
Konráda	Konrád	k1gMnSc2
z	z	k7c2
Montferratu	Montferrat	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
odmítl	odmítnout	k5eAaPmAgInS
účastnit	účastnit	k5eAaImF
Richardovy	Richardův	k2eAgFnPc4d1
ofenzivy	ofenziva	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
Richard	Richard	k1gMnSc1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgMnS
začít	začít	k5eAaPmF
se	s	k7c7
Saladinem	Saladino	k1gNnSc7
vyjednávat	vyjednávat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Vyjednávání	vyjednávání	k1gNnSc1
a	a	k8xC
boj	boj	k1gInSc1
za	za	k7c4
znovudobytí	znovudobytí	k1gNnSc4
Jeruzaléma	Jeruzalém	k1gInSc2
</s>
<s>
Saladinovi	Saladinův	k2eAgMnPc1d1
jezdci	jezdec	k1gMnPc1
<g/>
,	,	kIx,
francouzská	francouzský	k2eAgFnSc1d1
iluminace	iluminace	k1gFnSc1
<g/>
,	,	kIx,
1337	#num#	k4
</s>
<s>
Vyjednávání	vyjednávání	k1gNnSc1
mezi	mezi	k7c7
Richardem	Richard	k1gMnSc7
a	a	k8xC
Saladinem	Saladino	k1gNnSc7
byla	být	k5eAaImAgFnS
složitá	složitý	k2eAgFnSc1d1
a	a	k8xC
trvala	trvat	k5eAaImAgFnS
téměř	téměř	k6eAd1
celý	celý	k2eAgInSc4d1
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saladin	Saladina	k1gFnPc2
se	se	k3xPyFc4
za	za	k7c4
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
s	s	k7c7
Richardem	Richard	k1gMnSc7
osobně	osobně	k6eAd1
nesetkal	setkat	k5eNaPmAgMnS
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
Saladina	Saladin	k2eAgInSc2d1
zastupoval	zastupovat	k5eAaImAgMnS
jeho	jeho	k3xOp3gMnSc1
bratr	bratr	k1gMnSc1
al-Ádil	al-Ádit	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
,	,	kIx,
křižáky	křižák	k1gMnPc4
přezdívaný	přezdívaný	k2eAgInSc4d1
Safadin	Safadin	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Richardovu	Richardův	k2eAgFnSc4d1
diplomacii	diplomacie	k1gFnSc4
vedl	vést	k5eAaImAgMnS
jeruzalémský	jeruzalémský	k2eAgMnSc1d1
baron	baron	k1gMnSc1
Onfroy	Onfroa	k1gFnSc2
z	z	k7c2
Toronu	toron	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
Onfroy	Onfroa	k1gFnSc2
al-Ádilovi	al-Ádilův	k2eAgMnPc1d1
předložil	předložit	k5eAaPmAgMnS
první	první	k4xOgInSc4
návrh	návrh	k1gInSc4
<g/>
:	:	kIx,
navrácení	navrácení	k1gNnSc2
území	území	k1gNnSc2
Jeruzalémského	jeruzalémský	k2eAgNnSc2d1
království	království	k1gNnSc2
včetně	včetně	k7c2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
a	a	k8xC
Pravý	pravý	k2eAgInSc4d1
kříž	kříž	k1gInSc4
výměnou	výměna	k1gFnSc7
za	za	k7c4
konečný	konečný	k2eAgInSc4d1
mír	mír	k1gInSc4
a	a	k8xC
stažení	stažení	k1gNnSc4
cizích	cizí	k2eAgInPc2d1
křižáků	křižák	k1gInPc2
zpět	zpět	k6eAd1
do	do	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
Al-Ádil	Al-Ádil	k1gMnSc1
tyto	tento	k3xDgFnPc4
podmínky	podmínka	k1gFnPc4
odmítl	odmítnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
načež	načež	k6eAd1
Richard	Richard	k1gMnSc1
předložil	předložit	k5eAaPmAgMnS
nový	nový	k2eAgInSc4d1
kompromisní	kompromisní	k2eAgInSc4d1
a	a	k8xC
odvážný	odvážný	k2eAgInSc4d1
návrh	návrh	k1gInSc4
<g/>
:	:	kIx,
al-Ádil	al-Ádit	k5eAaImAgMnS,k5eAaPmAgMnS
se	se	k3xPyFc4
měl	mít	k5eAaImAgMnS
oženit	oženit	k5eAaPmF
s	s	k7c7
Richardovou	Richardův	k2eAgFnSc7d1
sestrou	sestra	k1gFnSc7
Johannou	Johanný	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnSc7
sídelním	sídelní	k2eAgNnSc7d1
městem	město	k1gNnSc7
se	se	k3xPyFc4
měl	mít	k5eAaImAgMnS
stát	stát	k5eAaImF,k5eAaPmF
Jeruzalém	Jeruzalém	k1gInSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
al-Ádil	al-Ádit	k5eAaImAgMnS,k5eAaPmAgMnS
by	by	kYmCp3nS
do	do	k7c2
svazku	svazek	k1gInSc2
přinesl	přinést	k5eAaPmAgInS
území	území	k1gNnSc4
Palestiny	Palestina	k1gFnSc2
a	a	k8xC
Johanna	Johann	k1gMnSc2
palestinské	palestinský	k2eAgNnSc1d1
pobřeží	pobřeží	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc4,k3yRgNnSc4,k3yQgNnSc4
ovládali	ovládat	k5eAaImAgMnP
křižáci	křižák	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeruzalém	Jeruzalém	k1gInSc4
a	a	k8xC
celá	celý	k2eAgFnSc1d1
Svatá	svatý	k2eAgFnSc1d1
země	země	k1gFnSc1
měla	mít	k5eAaImAgFnS
zůstat	zůstat	k5eAaPmF
přístupná	přístupný	k2eAgFnSc1d1
křesťanským	křesťanský	k2eAgMnPc3d1
poutníkům	poutník	k1gMnPc3
a	a	k8xC
muslimové	muslim	k1gMnPc1
a	a	k8xC
křesťané	křesťan	k1gMnPc1
měli	mít	k5eAaImAgMnP
žít	žít	k5eAaImF
v	v	k7c6
míru	mír	k1gInSc6
a	a	k8xC
jednotě	jednota	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
Rovněž	rovněž	k9
území	území	k1gNnSc1
mělo	mít	k5eAaImAgNnS
bývalá	bývalý	k2eAgNnPc1d1
léna	léno	k1gNnPc1
Jeruzalémského	jeruzalémský	k2eAgNnSc2d1
království	království	k1gNnSc2
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
rozdělena	rozdělit	k5eAaPmNgFnS
pod	pod	k7c4
muslimské	muslimský	k2eAgMnPc4d1
a	a	k8xC
křesťanské	křesťanský	k2eAgMnPc4d1
pány	pan	k1gMnPc4
<g/>
;	;	kIx,
hrady	hrad	k1gInPc1
a	a	k8xC
pevnosti	pevnost	k1gFnPc1
rytířských	rytířský	k2eAgInPc2d1
řádů	řád	k1gInPc2
<g />
.	.	kIx.
</s>
<s hack="1">
měli	mít	k5eAaImAgMnP
připadnout	připadnout	k5eAaPmF
řádům	řád	k1gInPc3
zpět	zpět	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
pozemky	pozemek	k1gInPc1
jeruzalémských	jeruzalémský	k2eAgMnPc2d1
baronů	baron	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
sympatizovali	sympatizovat	k5eAaImAgMnP
s	s	k7c7
Konrádem	Konrád	k1gMnSc7
z	z	k7c2
Montferratu	Montferrat	k1gInSc2
<g/>
,	,	kIx,
měly	mít	k5eAaImAgFnP
zůstat	zůstat	k5eAaPmF
v	v	k7c6
rukách	ruka	k1gFnPc6
muslimů	muslim	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
Svůj	svůj	k3xOyFgInSc4
nápad	nápad	k1gInSc4
Richard	Richard	k1gMnSc1
předem	předem	k6eAd1
nezkonzultoval	zkonzultovat	k5eNaPmAgMnS
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
sestrou	sestra	k1gFnSc7
a	a	k8xC
ta	ten	k3xDgFnSc1
jej	on	k3xPp3gMnSc4
odmítla	odmítnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
72	#num#	k4
<g/>
]	]	kIx)
Také	také	k9
barony	baron	k1gMnPc7
Richardův	Richardův	k2eAgInSc4d1
návrh	návrh	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
zcela	zcela	k6eAd1
opomíjel	opomíjet	k5eAaImAgInS
náboženskou	náboženský	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
svaté	svatý	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
pobouřil	pobouřit	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
Saladin	Saladin	k2eAgInSc1d1
Richardův	Richardův	k2eAgInSc1d1
návrh	návrh	k1gInSc1
nebral	brát	k5eNaImAgInS
vážně	vážně	k6eAd1
a	a	k8xC
když	když	k8xS
se	se	k3xPyFc4
otázal	otázat	k5eAaPmAgMnS
svého	svůj	k3xOyFgMnSc4
bratra	bratr	k1gMnSc4
<g/>
,	,	kIx,
zda	zda	k8xS
by	by	kYmCp3nS
byl	být	k5eAaImAgInS
ochoten	ochoten	k2eAgInSc1d1
konvertovat	konvertovat	k5eAaBmF
ke	k	k7c3
křesťanství	křesťanství	k1gNnSc3
<g/>
,	,	kIx,
al-Ádil	al-Ádit	k5eAaImAgMnS,k5eAaPmAgMnS
odpověděl	odpovědět	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
bylo	být	k5eAaImAgNnS
obtížné	obtížný	k2eAgNnSc1d1
být	být	k5eAaImF
dobrým	dobrý	k2eAgMnSc7d1
křesťanem	křesťan	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Se	s	k7c7
Saladinem	Saladin	k1gInSc7
začal	začít	k5eAaPmAgInS
vyjednávat	vyjednávat	k5eAaImF
také	také	k9
Konrád	Konrád	k1gMnSc1
z	z	k7c2
Montferratu	Montferrat	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k8xC,k8xS
Konrád	Konrád	k1gMnSc1
<g/>
,	,	kIx,
tak	tak	k6eAd1
al-Ádil	al-Ádit	k5eAaPmAgInS,k5eAaImAgInS
však	však	k9
jednání	jednání	k1gNnSc4
drželi	držet	k5eAaImAgMnP
v	v	k7c4
tajnosti	tajnost	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
Konrádovi	Konrádův	k2eAgMnPc1d1
vyslanci	vyslanec	k1gMnPc1
vedení	vedení	k1gNnSc2
Reginaldem	Reginaldo	k1gNnSc7
ze	z	k7c2
Sidonu	Sidon	k1gInSc2
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
Saladinovi	Saladinův	k2eAgMnPc1d1
předložili	předložit	k5eAaPmAgMnP
protinávrh	protinávrh	k1gInSc4
<g/>
:	:	kIx,
Pokud	pokud	k8xS
Saladin	Saladin	k2eAgMnSc1d1
uzná	uznat	k5eAaPmIp3nS
Konrádovi	Konrádův	k2eAgMnPc1d1
nároky	nárok	k1gInPc4
na	na	k7c4
jeruzalémskou	jeruzalémský	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
a	a	k8xC
ponechá	ponechat	k5eAaPmIp3nS
mu	on	k3xPp3gMnSc3
vládu	vláda	k1gFnSc4
nad	nad	k7c7
Tyrem	Tyr	k1gInSc7
<g/>
,	,	kIx,
Sidónem	Sidón	k1gInSc7
a	a	k8xC
Bejrútem	Bejrút	k1gInSc7
<g/>
,	,	kIx,
Konrád	Konrád	k1gMnSc1
nebude	být	k5eNaImBp3nS
držet	držet	k5eAaImF
dobytý	dobytý	k2eAgInSc4d1
Akkon	Akkon	k1gInSc4
a	a	k8xC
odmítne	odmítnout	k5eAaPmIp3nS
se	se	k3xPyFc4
dále	daleko	k6eAd2
podílet	podílet	k5eAaImF
na	na	k7c6
Richardově	Richardův	k2eAgFnSc6d1
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
však	však	k9
Saladin	Saladin	k2eAgInSc1d1
Konráda	Konrád	k1gMnSc2
zeptal	zeptat	k5eAaPmAgMnS
<g/>
,	,	kIx,
zda	zda	k8xS
by	by	kYmCp3nS
byl	být	k5eAaImAgInS
ochoten	ochoten	k2eAgInSc1d1
bojovat	bojovat	k5eAaImF
proti	proti	k7c3
Richardovi	Richard	k1gMnSc3
<g/>
,	,	kIx,
Konrád	Konrád	k1gMnSc1
ustoupil	ustoupit	k5eAaPmAgMnS
a	a	k8xC
jednání	jednání	k1gNnSc4
ztroskotala	ztroskotat	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
Al-Ádil	Al-Ádil	k1gMnSc1
vyjednávání	vyjednávání	k1gNnSc2
udržoval	udržovat	k5eAaImAgMnS
až	až	k6eAd1
do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1191	#num#	k4
uspořádal	uspořádat	k5eAaPmAgInS
al-Ádil	al-Ádit	k5eAaImAgMnS,k5eAaPmAgMnS
v	v	k7c6
Lyddě	Lydda	k1gFnSc6
velkolepou	velkolepý	k2eAgFnSc4d1
hostinu	hostina	k1gFnSc4
na	na	k7c4
Richardovu	Richardův	k2eAgFnSc4d1
počest	počest	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Richard	Richard	k1gMnSc1
Lví	lví	k2eAgNnSc1d1
srdce	srdce	k1gNnSc1
táhne	táhnout	k5eAaImIp3nS
k	k	k7c3
Jeruzalému	Jeruzalém	k1gInSc3
<g/>
,	,	kIx,
James	James	k1gInSc4
William	William	k1gInSc1
Glass	Glass	k1gInSc1
(	(	kIx(
<g/>
1850	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Boj	boj	k1gInSc1
Saladina	Saladina	k1gFnSc1
s	s	k7c7
Richardem	Richard	k1gMnSc7
<g/>
,	,	kIx,
vyobrazení	vyobrazení	k1gNnPc2
ze	z	k7c2
středověkého	středověký	k2eAgInSc2d1
rukopisu	rukopis	k1gInSc2
</s>
<s>
Na	na	k7c4
zimu	zima	k1gFnSc4
Saladin	Saladina	k1gFnPc2
rozpustil	rozpustit	k5eAaPmAgMnS
polovinu	polovina	k1gFnSc4
své	svůj	k3xOyFgFnSc2
armády	armáda	k1gFnSc2
<g/>
;	;	kIx,
vycházel	vycházet	k5eAaImAgMnS
z	z	k7c2
přesvědčení	přesvědčení	k1gNnSc2
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
zimní	zimní	k2eAgInPc4d1
deště	dešť	k1gInPc4
křesťanská	křesťanský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
nepodnikne	podniknout	k5eNaPmIp3nS
žádnou	žádný	k3yNgFnSc4
novou	nový	k2eAgFnSc4d1
ofenzivu	ofenziva	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
Zbytek	zbytek	k1gInSc1
muslimských	muslimský	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
se	se	k3xPyFc4
z	z	k7c2
Ramly	Ramla	k1gFnSc2
stáhl	stáhnout	k5eAaPmAgMnS
do	do	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Richard	Richard	k1gMnSc1
naopak	naopak	k6eAd1
dostal	dostat	k5eAaPmAgMnS
posily	posila	k1gFnPc4
z	z	k7c2
Akkonu	Akkon	k1gInSc2
a	a	k8xC
v	v	k7c6
listopadu	listopad	k1gInSc6
1191	#num#	k4
vyrazil	vyrazit	k5eAaPmAgMnS
na	na	k7c4
Jeruzalém	Jeruzalém	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
Saladin	Saladina	k1gFnPc2
si	se	k3xPyFc3
přivolal	přivolat	k5eAaPmAgMnS
posily	posila	k1gFnPc4
z	z	k7c2
Egypta	Egypt	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
utábořily	utábořit	k5eAaPmAgFnP
v	v	k7c6
okolí	okolí	k1gNnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
křižáky	křižák	k1gInPc4
mohli	moct	k5eAaImAgMnP
napadnout	napadnout	k5eAaPmF
z	z	k7c2
týlu	týl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navzdory	navzdory	k6eAd1
špatnému	špatný	k2eAgNnSc3d1
počasí	počasí	k1gNnSc3
křižáci	křižák	k1gMnPc1
dospěli	dochvít	k5eAaPmAgMnP
k	k	k7c3
pevnosti	pevnost	k1gFnSc3
Bejt	Bejt	k?
Núbá	Núbá	k1gFnSc1
<g/>
,	,	kIx,
asi	asi	k9
20	#num#	k4
kilometrů	kilometr	k1gInPc2
od	od	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
Richard	Richard	k1gMnSc1
si	se	k3xPyFc3
však	však	k9
brzy	brzy	k6eAd1
uvědomil	uvědomit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
omyl	omyl	k1gInSc4
<g/>
;	;	kIx,
nemohl	moct	k5eNaImAgInS
obléhat	obléhat	k5eAaImF
město	město	k1gNnSc4
s	s	k7c7
nepřítelem	nepřítel	k1gMnSc7
v	v	k7c6
zádech	záda	k1gNnPc6
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
křižáků	křižák	k1gMnPc2
nebylo	být	k5eNaImAgNnS
tolik	tolik	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
si	se	k3xPyFc3
<g />
.	.	kIx.
</s>
<s hack="1">
mohli	moct	k5eAaImAgMnP
dovolit	dovolit	k5eAaPmF
město	město	k1gNnSc4
hájit	hájit	k5eAaImF
po	po	k7c6
Richardově	Richardův	k2eAgInSc6d1
odjezdu	odjezd	k1gInSc6
ze	z	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
Svou	svůj	k3xOyFgFnSc4
armádu	armáda	k1gFnSc4
stočil	stočit	k5eAaPmAgMnS
na	na	k7c4
jih	jih	k1gInSc4
k	k	k7c3
pobřeží	pobřeží	k1gNnSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
obsadil	obsadit	k5eAaPmAgMnS
zničený	zničený	k2eAgInSc4d1
Askalon	Askalon	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
jeho	jeho	k3xOp3gFnSc4
vojáci	voják	k1gMnPc1
začali	začít	k5eAaPmAgMnP
okamžitě	okamžitě	k6eAd1
znovu	znovu	k6eAd1
opevňovat	opevňovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
Opevňovací	opevňovací	k2eAgFnSc2d1
práce	práce	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
trvaly	trvat	k5eAaImAgInP
čtyři	čtyři	k4xCgInPc1
měsíce	měsíc	k1gInPc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
již	již	k6eAd1
v	v	k7c6
květnu	květen	k1gInSc6
se	se	k3xPyFc4
Richardovi	Richard	k1gMnSc3
podařilo	podařit	k5eAaPmAgNnS
Saladina	Saladina	k1gFnSc1
vytlačit	vytlačit	k5eAaPmF
z	z	k7c2
Daronu	Daron	k1gInSc2
jižně	jižně	k6eAd1
od	od	k7c2
Gazy	Gaza	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
7	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
vyrazili	vyrazit	k5eAaPmAgMnP
křižáci	křižák	k1gMnPc1
znovu	znovu	k6eAd1
na	na	k7c4
Jeruzalém	Jeruzalém	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
také	také	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
donesly	donést	k5eAaPmAgInP
zprávy	zpráva	k1gFnSc2
o	o	k7c6
vzpouře	vzpoura	k1gFnSc6
v	v	k7c6
Džezíře	Džezíra	k1gFnSc6
a	a	k8xC
údajné	údajný	k2eAgFnSc6d1
Saladinově	Saladinův	k2eAgFnSc6d1
nepřítomnosti	nepřítomnost	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
Nicméně	nicméně	k8xC
Saladin	Saladin	k2eAgMnSc1d1
byl	být	k5eAaImAgInS
v	v	k7c6
Jeruzalémě	Jeruzalém	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gFnSc1
pozice	pozice	k1gFnSc1
byla	být	k5eAaImAgFnS
vratká	vratký	k2eAgFnSc1d1
<g/>
;	;	kIx,
armáda	armáda	k1gFnSc1
již	již	k6eAd1
nebyla	být	k5eNaImAgFnS
tak	tak	k6eAd1
silná	silný	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
ale	ale	k8xC
hlavně	hlavně	k6eAd1
sílilo	sílit	k5eAaImAgNnS
napětí	napětí	k1gNnSc1
mezi	mezi	k7c7
muslimskými	muslimský	k2eAgMnPc7d1
vojáky	voják	k1gMnPc7
různých	různý	k2eAgFnPc2d1
národností	národnost	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Saladin	Saladin	k2eAgMnSc1d1
dokonce	dokonce	k9
zvažoval	zvažovat	k5eAaImAgMnS
ústup	ústup	k1gInSc4
z	z	k7c2
města	město	k1gNnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
si	se	k3xPyFc3
nebyl	být	k5eNaImAgMnS
jist	jist	k2eAgMnSc1d1
loajalitou	loajalita	k1gFnSc7
svých	svůj	k3xOyFgNnPc2
vojsk	vojsko	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
Přesto	přesto	k8xC
nakonec	nakonec	k6eAd1
nechal	nechat	k5eAaPmAgMnS
otrávit	otrávit	k5eAaPmF
studny	studna	k1gFnPc4
v	v	k7c6
okolí	okolí	k1gNnSc6
Jeruzaléma	Jeruzalém	k1gInSc2
a	a	k8xC
připravit	připravit	k5eAaPmF
město	město	k1gNnSc4
na	na	k7c4
obléhání	obléhání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Richard	Richarda	k1gFnPc2
zatím	zatím	k6eAd1
11	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
dospěl	dochvít	k5eAaPmAgMnS
opět	opět	k6eAd1
k	k	k7c3
pevnosti	pevnost	k1gFnSc3
Bejt	Bejt	k?
Núbá	Núbá	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
rozbil	rozbít	k5eAaPmAgInS
ležení	ležení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
město	město	k1gNnSc4
nezaútočil	zaútočit	k5eNaPmAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
vyčkával	vyčkávat	k5eAaImAgMnS
na	na	k7c6
místě	místo	k1gNnSc6
a	a	k8xC
podnikal	podnikat	k5eAaImAgMnS
drobné	drobný	k2eAgMnPc4d1
přepady	přepad	k1gInPc1
muslimských	muslimský	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
Lví	lví	k2eAgNnPc4d1
srdce	srdce	k1gNnPc4
netušil	tušit	k5eNaImAgMnS
o	o	k7c6
Saladinových	Saladinový	k2eAgInPc6d1
problémech	problém	k1gInPc6
ve	v	k7c6
městě	město	k1gNnSc6
<g/>
,	,	kIx,
neboť	neboť	k8xC
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Arabů	Arab	k1gMnPc2
si	se	k3xPyFc3
Evropané	Evropan	k1gMnPc1
moc	moc	k6eAd1
necenili	cenit	k5eNaImAgMnP
zrádců	zrádce	k1gMnPc2
a	a	k8xC
špehů	špeh	k1gMnPc2
<g/>
;	;	kIx,
Saladin	Saladin	k2eAgInSc1d1
měl	mít	k5eAaImAgInS
proto	proto	k8xC
mnohem	mnohem	k6eAd1
lepší	dobrý	k2eAgFnPc4d2
informace	informace	k1gFnPc4
o	o	k7c6
situaci	situace	k1gFnSc6
nepřítele	nepřítel	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
téměř	téměř	k6eAd1
tří	tři	k4xCgFnPc2
týdenní	týdenní	k2eAgFnSc6d1
nečinnosti	nečinnost	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
dostali	dostat	k5eAaPmAgMnP
křižáci	křižák	k1gMnPc1
zprávu	zpráva	k1gFnSc4
o	o	k7c6
postupu	postup	k1gInSc6
velké	velký	k2eAgFnSc2d1
egyptské	egyptský	k2eAgFnSc2d1
karavany	karavana	k1gFnSc2
směrem	směr	k1gInSc7
k	k	k7c3
Jeruzalému	Jeruzalém	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arabský	arabský	k2eAgMnSc1d1
kronikář	kronikář	k1gMnSc1
Baha	Baha	k1gMnSc1
ad-Dín	ad-Dín	k1gMnSc1
píše	psát	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Když	když	k8xS
to	ten	k3xDgNnSc4
jacísi	jakýsi	k3yIgMnPc1
Arabové	Arab	k1gMnPc1
hlásili	hlásit	k5eAaImAgMnP
anglickému	anglický	k2eAgMnSc3d1
králi	král	k1gMnSc3
<g/>
,	,	kIx,
nevěřil	věřit	k5eNaImAgMnS
jim	on	k3xPp3gMnPc3
<g/>
,	,	kIx,
ale	ale	k8xC
vsedl	vsednout	k5eAaPmAgMnS
na	na	k7c4
koně	kůň	k1gMnSc4
a	a	k8xC
vydal	vydat	k5eAaPmAgMnS
se	se	k3xPyFc4
s	s	k7c7
Araby	Arab	k1gMnPc7
a	a	k8xC
malou	malý	k2eAgFnSc7d1
eskortou	eskorta	k1gFnSc7
na	na	k7c4
cestu	cesta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
přiblížil	přiblížit	k5eAaPmAgMnS
ke	k	k7c3
karavaně	karavana	k1gFnSc3
<g/>
,	,	kIx,
převlíkl	převlíknout	k5eAaPmAgMnS
se	se	k3xPyFc4
za	za	k7c4
Araba	Arab	k1gMnSc4
a	a	k8xC
objel	objet	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
ze	z	k7c2
všech	všecek	k3xTgFnPc2
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakmile	jakmile	k8xS
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
v	v	k7c6
táboře	tábor	k1gInSc6
klid	klid	k1gInSc1
a	a	k8xC
všichni	všechen	k3xTgMnPc1
spí	spát	k5eAaImIp3nP
<g/>
,	,	kIx,
vrátil	vrátit	k5eAaPmAgMnS
se	se	k3xPyFc4
a	a	k8xC
dal	dát	k5eAaPmAgMnS
svým	svůj	k3xOyFgMnPc3
mužům	muž	k1gMnPc3
povel	povel	k1gInSc1
vsednout	vsednout	k5eAaPmF
na	na	k7c4
koně	kůň	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Baha	Baha	k1gMnSc1
ad-Dín	ad-Dín	k1gMnSc1
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Saladin	Saladin	k2eAgMnSc1d1
obléhá	obléhat	k5eAaImIp3nS
Jaffu	Jaffa	k1gFnSc4
<g/>
,	,	kIx,
středověké	středověký	k2eAgNnSc4d1
vyobrazení	vyobrazení	k1gNnSc4
</s>
<s>
Křižáci	křižák	k1gMnPc1
karavanu	karavana	k1gFnSc4
napadli	napadnout	k5eAaPmAgMnP
a	a	k8xC
bez	bez	k7c2
větších	veliký	k2eAgFnPc2d2
potíží	potíž	k1gFnPc2
se	se	k3xPyFc4
zmocnili	zmocnit	k5eAaPmAgMnP
velké	velký	k2eAgFnPc4d1
kořisti	kořist	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
Richard	Richard	k1gMnSc1
nakonec	nakonec	k6eAd1
rezignoval	rezignovat	k5eAaBmAgMnS
a	a	k8xC
navzdory	navzdory	k6eAd1
protestům	protest	k1gInPc3
francouzských	francouzský	k2eAgInPc2d1
křižáků	křižák	k1gInPc2
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
ustoupil	ustoupit	k5eAaPmAgMnS
od	od	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
zpět	zpět	k6eAd1
k	k	k7c3
Jaffě	Jaffa	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyjednávání	vyjednávání	k1gNnSc1
začalo	začít	k5eAaPmAgNnS
nanovo	nanovo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
značná	značný	k2eAgFnSc1d1
překážka	překážka	k1gFnSc1
konečné	konečná	k1gFnSc3
dohodě	dohoda	k1gFnSc3
se	se	k3xPyFc4
ukázal	ukázat	k5eAaPmAgMnS
být	být	k5eAaImF
znovu	znovu	k6eAd1
vybudovaný	vybudovaný	k2eAgInSc4d1
Askalon	Askalon	k1gInSc4
v	v	k7c6
rukách	ruka	k1gFnPc6
křižáků	křižák	k1gMnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
protože	protože	k8xS
zdrojem	zdroj	k1gInSc7
Saladinových	Saladinový	k2eAgInPc2d1
příjmů	příjem	k1gInPc2
byl	být	k5eAaImAgInS
Egypt	Egypt	k1gInSc1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
většina	většina	k1gFnSc1
vojáků	voják	k1gMnPc2
se	se	k3xPyFc4
rekrutovala	rekrutovat	k5eAaImAgFnS
ze	z	k7c2
Sýrie	Sýrie	k1gFnSc2
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
pokud	pokud	k8xS
by	by	kYmCp3nP
křižáci	křižák	k1gMnPc1
kontrolovali	kontrolovat	k5eAaImAgMnP
Askalon	Askalon	k1gInSc4
<g/>
,	,	kIx,
mohli	moct	k5eAaImAgMnP
ohrožovat	ohrožovat	k5eAaImF
životně	životně	k6eAd1
důležitou	důležitý	k2eAgFnSc4d1
trasu	trasa	k1gFnSc4
přes	přes	k7c4
Sinaj	Sinaj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saladin	Saladin	k2eAgInSc4d1
jako	jako	k8xS,k8xC
protinávrh	protinávrh	k1gInSc4
za	za	k7c4
Askalon	Askalon	k1gInSc4
nabízel	nabízet	k5eAaImAgInS
odstoupení	odstoupení	k1gNnSc4
Lyddy	Lydda	k1gFnSc2
<g/>
,	,	kIx,
toleranci	tolerance	k1gFnSc4
latinského	latinský	k2eAgNnSc2d1
duchovenstva	duchovenstvo	k1gNnSc2
v	v	k7c6
Jeruzalémě	Jeruzalém	k1gInSc6
a	a	k8xC
dobré	dobrý	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
k	k	k7c3
Jeruzalémskému	jeruzalémský	k2eAgNnSc3d1
království	království	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
Richard	Richard	k1gMnSc1
ale	ale	k9
Askalon	Askalon	k1gInSc4
postoupit	postoupit	k5eAaPmF
odmítl	odmítnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
přerušil	přerušit	k5eAaPmAgInS
jaffská	jaffský	k2eAgNnPc4d1
jednání	jednání	k1gNnPc4
a	a	k8xC
odplul	odplout	k5eAaPmAgMnS
do	do	k7c2
Akkonu	Akkon	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
odkud	odkud	k6eAd1
se	se	k3xPyFc4
chtěl	chtít	k5eAaImAgMnS
pokusit	pokusit	k5eAaPmF
zmocnit	zmocnit	k5eAaPmF
Bejrútu	Bejrút	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
Saladin	Saladina	k1gFnPc2
podnikl	podniknout	k5eAaPmAgInS
náhlý	náhlý	k2eAgInSc1d1
protiútok	protiútok	k1gInSc1
<g/>
.	.	kIx.
27	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
jeho	jeho	k3xOp3gNnPc1
vojska	vojsko	k1gNnPc1
vytáhla	vytáhnout	k5eAaPmAgNnP
z	z	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
,	,	kIx,
napadla	napadnout	k5eAaPmAgFnS
Jaffu	Jaffa	k1gFnSc4
a	a	k8xC
během	během	k7c2
tří	tři	k4xCgInPc2
dnů	den	k1gInPc2
se	se	k3xPyFc4
muslimům	muslim	k1gMnPc3
podařilo	podařit	k5eAaPmAgNnS
obranu	obrana	k1gFnSc4
prorazit	prorazit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Den	den	k1gInSc1
poté	poté	k6eAd1
z	z	k7c2
Akkonu	Akkon	k1gInSc2
přispěchal	přispěchat	k5eAaPmAgMnS
Richard	Richard	k1gMnSc1
s	s	k7c7
padesáti	padesát	k4xCc7
janovskými	janovský	k2eAgFnPc7d1
a	a	k8xC
pisánskými	pisánský	k2eAgFnPc7d1
galérami	galéra	k1gFnPc7
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
hlavní	hlavní	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
se	se	k3xPyFc4
vydala	vydat	k5eAaPmAgFnS
po	po	k7c6
souši	souš	k1gFnSc6
na	na	k7c4
jih	jih	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
Přesto	přesto	k8xC
se	se	k3xPyFc4
Richard	Richard	k1gMnSc1
jen	jen	k9
s	s	k7c7
malým	malý	k2eAgInSc7d1
oddílem	oddíl	k1gInSc7
vylodil	vylodit	k5eAaPmAgInS
<g/>
,	,	kIx,
pronikl	proniknout	k5eAaPmAgInS
do	do	k7c2
města	město	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
muslimy	muslim	k1gMnPc7
překvapil	překvapit	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
Saladin	Saladin	k2eAgMnSc1d1
byl	být	k5eAaImAgMnS
vytlačen	vytlačen	k2eAgMnSc1d1
z	z	k7c2
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saladin	Saladin	k2eAgInSc4d1
ale	ale	k8xC
brzy	brzy	k6eAd1
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Richard	Richard	k1gMnSc1
Jaffu	Jaffa	k1gFnSc4
dobyl	dobýt	k5eAaPmAgMnS
jen	jen	k9
s	s	k7c7
80	#num#	k4
rytíři	rytíř	k1gMnPc7
<g/>
,	,	kIx,
200	#num#	k4
pěšáky	pěšák	k1gMnPc7
a	a	k8xC
2000	#num#	k4
italskými	italský	k2eAgMnPc7d1
námořníky	námořník	k1gMnPc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
zatímco	zatímco	k8xS
hlavní	hlavní	k2eAgNnSc1d1
křižácké	křižácký	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
se	se	k3xPyFc4
zatím	zatím	k6eAd1
nacházelo	nacházet	k5eAaImAgNnS
severně	severně	k6eAd1
od	od	k7c2
Caesareje	Caesarej	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
4	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
Saladin	Saladin	k2eAgMnSc1d1
tiše	tiš	k1gFnPc4
přitáhl	přitáhnout	k5eAaPmAgMnS
pod	pod	k7c4
Jaffu	Jaffa	k1gFnSc4
k	k	k7c3
Richardovu	Richardův	k2eAgInSc3d1
táboru	tábor	k1gInSc3
a	a	k8xC
pokusil	pokusit	k5eAaPmAgMnS
se	se	k3xPyFc4
ho	on	k3xPp3gMnSc4
překvapit	překvapit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saladin	Saladina	k1gFnPc2
disponoval	disponovat	k5eAaBmAgInS
až	až	k9
7000	#num#	k4
mamlúky	mamlúko	k1gNnPc7
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
křižáci	křižák	k1gMnPc1
mohli	moct	k5eAaImAgMnP
do	do	k7c2
boje	boj	k1gInSc2
postavit	postavit	k5eAaPmF
jen	jen	k9
54	#num#	k4
rytířů	rytíř	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
mělo	mít	k5eAaImAgNnS
jen	jen	k9
15	#num#	k4
k	k	k7c3
dispozici	dispozice	k1gFnSc3
koně	kůň	k1gMnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
2000	#num#	k4
pěších	pěší	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
Richard	Richard	k1gMnSc1
zaujal	zaujmout	k5eAaPmAgMnS
obranné	obranný	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
a	a	k8xC
téměř	téměř	k6eAd1
celý	celý	k2eAgInSc4d1
den	den	k1gInSc4
se	se	k3xPyFc4
křižákům	křižák	k1gMnPc3
dařilo	dařit	k5eAaImAgNnS
odrážet	odrážet	k5eAaImF
muslimské	muslimský	k2eAgInPc4d1
výpady	výpad	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
Nakonec	nakonec	k6eAd1
muslimské	muslimský	k2eAgInPc4d1
šiky	šik	k1gInPc4
rozbili	rozbít	k5eAaPmAgMnP
angličtí	anglický	k2eAgMnPc1d1
lučištníci	lučištník	k1gMnPc1
a	a	k8xC
janovští	janovský	k2eAgMnPc1d1
kušičníci	kušičník	k1gMnPc1
a	a	k8xC
Richard	Richard	k1gMnSc1
podnikl	podniknout	k5eAaPmAgMnS
výpad	výpad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Večer	večer	k6eAd1
dal	dát	k5eAaPmAgInS
Saladin	Saladin	k2eAgInSc1d1
rozkaz	rozkaz	k1gInSc1
k	k	k7c3
ústupu	ústup	k1gInSc3
zpět	zpět	k6eAd1
do	do	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
okamžitě	okamžitě	k6eAd1
připravil	připravit	k5eAaPmAgInS
na	na	k7c4
možný	možný	k2eAgInSc4d1
Richardův	Richardův	k2eAgInSc4d1
útok	útok	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Richard	Richard	k1gMnSc1
již	již	k6eAd1
na	na	k7c4
Jeruzalém	Jeruzalém	k1gInSc4
netáhl	táhnout	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znovudobytí	znovudobytí	k1gNnSc3
Jaffy	Jaffa	k1gFnSc2
a	a	k8xC
následná	následný	k2eAgFnSc1d1
bitva	bitva	k1gFnSc1
byly	být	k5eAaImAgFnP
jeho	jeho	k3xOp3gMnSc7
posledním	poslední	k2eAgInSc7d1
vojenským	vojenský	k2eAgInSc7d1
střetem	střet	k1gInSc7
ve	v	k7c6
Svaté	svatý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
Brzy	brzy	k6eAd1
poté	poté	k6eAd1
těžce	těžce	k6eAd1
onemocněl	onemocnět	k5eAaPmAgMnS
a	a	k8xC
navíc	navíc	k6eAd1
již	již	k6eAd1
věděl	vědět	k5eAaImAgMnS
o	o	k7c6
nestabilní	stabilní	k2eNgFnSc6d1
politické	politický	k2eAgFnSc6d1
situaci	situace	k1gFnSc6
<g/>
,	,	kIx,
které	který	k3yIgNnSc4,k3yRgNnSc4,k3yQgNnSc4
využíval	využívat	k5eAaImAgMnS,k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
bratr	bratr	k1gMnSc1
Jan	Jan	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
Anglii	Anglie	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
si	se	k3xPyFc3
žádala	žádat	k5eAaImAgFnS
jeho	jeho	k3xOp3gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
návrat	návrat	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
Konečná	konečný	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
se	se	k3xPyFc4
Saladinem	Saladino	k1gNnSc7
byla	být	k5eAaImAgFnS
stvrzena	stvrzen	k2eAgFnSc1d1
2	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1192	#num#	k4
a	a	k8xC
zahrnovala	zahrnovat	k5eAaImAgFnS
pětileté	pětiletý	k2eAgNnSc4d1
příměří	příměří	k1gNnSc4
<g/>
,	,	kIx,
křižácké	křižácký	k2eAgFnPc4d1
državy	država	k1gFnPc4
na	na	k7c6
palestinském	palestinský	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
od	od	k7c2
Bejrútu	Bejrút	k1gInSc2
po	po	k7c4
Jaffu	Jaffa	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
Jeruzalém	Jeruzalém	k1gInSc1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
zůstal	zůstat	k5eAaPmAgMnS
pod	pod	k7c7
vládou	vláda	k1gFnSc7
muslimů	muslim	k1gMnPc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
zpřístupněn	zpřístupnit	k5eAaPmNgInS
pro	pro	k7c4
křesťanské	křesťanský	k2eAgMnPc4d1
poutníky	poutník	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
si	se	k3xPyFc3
ovšem	ovšem	k9
do	do	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
nesměli	smět	k5eNaImAgMnP
přinést	přinést	k5eAaPmF
zbraně	zbraň	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
Křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
skončila	skončit	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spor	spor	k1gInSc1
o	o	k7c4
jeruzalémskou	jeruzalémský	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
</s>
<s>
Richard	Richard	k1gMnSc1
Lví	lví	k2eAgNnSc1d1
srdce	srdce	k1gNnSc1
opouští	opouštět	k5eAaImIp3nS
Svatou	svatý	k2eAgFnSc4d1
zemi	zem	k1gFnSc4
<g/>
,	,	kIx,
The	The	k1gFnSc1
History	Histor	k1gInPc1
of	of	k?
France	Franc	k1gMnSc4
from	from	k1gMnSc1
the	the	k?
Earliest	Earliest	k1gMnSc1
Times	Times	k1gMnSc1
to	ten	k3xDgNnSc1
the	the	k?
Year	Year	k1gInSc1
1789	#num#	k4
</s>
<s>
Spor	spor	k1gInSc1
o	o	k7c4
jeruzalémskou	jeruzalémský	k2eAgFnSc4d1
královskou	královský	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
zažehnán	zažehnán	k2eAgInSc1d1
nebyl	být	k5eNaImAgInS
a	a	k8xC
mezi	mezi	k7c7
Guyem	Guy	k1gMnSc7
a	a	k8xC
Konrádem	Konrád	k1gMnSc7
i	i	k8xC
po	po	k7c6
červenci	červenec	k1gInSc6
1191	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Richard	Richard	k1gMnSc1
zvolil	zvolit	k5eAaPmAgMnS
řešení	řešení	k1gNnSc4
Konrádova	Konrádův	k2eAgNnSc2d1
nástupnictví	nástupnictví	k1gNnSc2
po	po	k7c6
Guyovi	Guya	k1gMnSc6
<g/>
,	,	kIx,
rostlo	růst	k5eAaImAgNnS
napětí	napětí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS
na	na	k7c4
podzim	podzim	k1gInSc4
roku	rok	k1gInSc2
1191	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
do	do	k7c2
dynastických	dynastický	k2eAgInPc2d1
sporů	spor	k1gInPc2
promítla	promítnout	k5eAaPmAgFnS
i	i	k9
tradiční	tradiční	k2eAgFnSc1d1
rivalita	rivalita	k1gFnSc1
mezi	mezi	k7c7
italskými	italský	k2eAgInPc7d1
státy	stát	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
se	se	k3xPyFc4
na	na	k7c6
výpravě	výprava	k1gFnSc6
podílely	podílet	k5eAaImAgFnP
<g/>
;	;	kIx,
Italové	Ital	k1gMnPc1
spolu	spolu	k6eAd1
začali	začít	k5eAaPmAgMnP
válčit	válčit	k5eAaImF
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
Janov	Janov	k1gInSc1
podporoval	podporovat	k5eAaImAgInS
Konráda	Konrád	k1gMnSc2
a	a	k8xC
Pisa	Pisa	k1gFnSc1
Guye	Guy	k1gFnSc2
de	de	k?
Lusignan	Lusignan	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
Na	na	k7c6
jaře	jaro	k1gNnSc6
1192	#num#	k4
Richard	Richard	k1gMnSc1
začal	začít	k5eAaPmAgMnS
znovu	znovu	k6eAd1
otevřený	otevřený	k2eAgInSc4d1
spor	spor	k1gInSc4
řešit	řešit	k5eAaImF
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
tím	ten	k3xDgNnSc7
odvrátil	odvrátit	k5eAaPmAgMnS
občanskou	občanský	k2eAgFnSc4d1
válku	válka	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
dubnu	duben	k1gInSc6
bylo	být	k5eAaImAgNnS
Richardem	Richard	k1gMnSc7
svoláno	svolán	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
palestinských	palestinský	k2eAgMnPc2d1
baronů	baron	k1gMnPc2
do	do	k7c2
Akkonu	Akkon	k1gInSc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
měli	mít	k5eAaImAgMnP
mezi	mezi	k7c7
oběma	dva	k4xCgMnPc7
uchazeči	uchazeč	k1gMnPc7
zvolit	zvolit	k5eAaPmF
krále	král	k1gMnPc4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
Guy	Guy	k1gMnSc1
měl	mít	k5eAaImAgMnS
s	s	k7c7
většinou	většina	k1gFnSc7
křižáckých	křižácký	k2eAgMnPc2d1
baronů	baron	k1gMnPc2
špatné	špatný	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
a	a	k8xC
ti	ten	k3xDgMnPc1
jej	on	k3xPp3gNnSc4
rovněž	rovněž	k9
od	od	k7c2
bitvy	bitva	k1gFnSc2
u	u	k7c2
Hattínu	Hattín	k1gInSc2
považovali	považovat	k5eAaImAgMnP
za	za	k7c4
neschopného	schopný	k2eNgMnSc4d1
velitele	velitel	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
Králem	Král	k1gMnSc7
byl	být	k5eAaImAgMnS
jednomyslně	jednomyslně	k6eAd1
zvolen	zvolen	k2eAgMnSc1d1
Konrád	Konrád	k1gMnSc1
z	z	k7c2
Montferratu	Montferrat	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
Guy	Guy	k1gMnSc1
musel	muset	k5eAaImAgMnS
uznat	uznat	k5eAaPmF
svou	svůj	k3xOyFgFnSc4
porážku	porážka	k1gFnSc4
a	a	k8xC
Richard	Richard	k1gMnSc1
mu	on	k3xPp3gMnSc3
jako	jako	k8xC,k8xS
odškodné	odškodné	k1gNnSc4
vzápětí	vzápětí	k6eAd1
nabídl	nabídnout	k5eAaPmAgInS
Kypr	Kypr	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
Jenže	jenže	k8xC
Kypr	Kypr	k1gInSc1
již	již	k6eAd1
od	od	k7c2
podzimu	podzim	k1gInSc2
drželi	držet	k5eAaImAgMnP
templáři	templář	k1gMnPc1
<g/>
,	,	kIx,
kterým	který	k3yRgMnPc3,k3yIgMnPc3,k3yQgMnPc3
ostrov	ostrov	k1gInSc1
prodal	prodat	k5eAaPmAgInS
Richard	Richard	k1gMnSc1
<g/>
,	,	kIx,
neboť	neboť	k8xC
držba	držba	k1gFnSc1
ostrova	ostrov	k1gInSc2
pro	pro	k7c4
něho	on	k3xPp3gInSc4
byla	být	k5eAaImAgFnS
nákladná	nákladný	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
Templáři	templář	k1gMnSc3
Guyovi	Guya	k1gMnSc3
ostrov	ostrov	k1gInSc4
ochotně	ochotně	k6eAd1
odprodali	odprodat	k5eAaPmAgMnP
<g/>
,	,	kIx,
neboť	neboť	k8xC
již	již	k6eAd1
za	za	k7c2
těch	ten	k3xDgInPc2
několik	několik	k4yIc4
měsíců	měsíc	k1gInPc2
museli	muset	k5eAaImAgMnP
potlačovat	potlačovat	k5eAaImF
vzpouru	vzpoura	k1gFnSc4
kyperských	kyperský	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
povstali	povstat	k5eAaPmAgMnP
proti	proti	k7c3
vysokým	vysoký	k2eAgFnPc3d1
daním	daň	k1gFnPc3
<g/>
,	,	kIx,
kterými	který	k3yRgInPc7,k3yQgInPc7,k3yIgInPc7
je	on	k3xPp3gMnPc4
templáři	templář	k1gMnPc1
zatížili	zatížit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Guy	Guy	k1gMnSc1
odplul	odplout	k5eAaPmAgMnS
na	na	k7c4
Kypr	Kypr	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
si	se	k3xPyFc3
založil	založit	k5eAaPmAgMnS
své	svůj	k3xOyFgNnSc4
vlastní	vlastní	k2eAgNnSc4d1
panství	panství	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
Konrád	Konrád	k1gMnSc1
byl	být	k5eAaImAgMnS
slavnostně	slavnostně	k6eAd1
korunován	korunovat	k5eAaBmNgMnS
v	v	k7c6
Akkonu	Akkon	k1gInSc6
a	a	k8xC
poté	poté	k6eAd1
se	se	k3xPyFc4
měl	mít	k5eAaImAgMnS
s	s	k7c7
Richardem	Richard	k1gMnSc7
setkat	setkat	k5eAaPmF
v	v	k7c6
Askalonu	Askalon	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
28	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
byl	být	k5eAaImAgMnS
ale	ale	k8xC
Konrád	Konrád	k1gMnSc1
v	v	k7c6
Tyru	Tyrus	k1gInSc6
zavražděn	zavraždit	k5eAaPmNgMnS
dvěma	dva	k4xCgFnPc7
příslušníky	příslušník	k1gMnPc7
muslimské	muslimský	k2eAgFnSc2d1
sekty	sekta	k1gFnSc2
assassinů	assassin	k1gInPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
na	na	k7c4
Konráda	Konrád	k1gMnSc4
vyslal	vyslat	k5eAaPmAgMnS
jejich	jejich	k3xOp3gMnSc1
šejk	šejk	k1gMnSc1
Rašíd	Rašíd	k1gMnSc1
ad-Dín	ad-Dín	k1gMnSc1
Sinan	Sinan	k1gMnSc1
<g/>
,	,	kIx,
známý	známý	k1gMnSc1
jako	jako	k8xS,k8xC
Stařec	stařec	k1gMnSc1
z	z	k7c2
hor.	hor.	k?
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1
současníci	současník	k1gMnPc1
obviňovali	obviňovat	k5eAaImAgMnP
Richarda	Richard	k1gMnSc4
<g/>
,	,	kIx,
jiní	jiný	k2eAgMnPc1d1
palestinské	palestinský	k2eAgMnPc4d1
barony	baron	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
Současní	současný	k2eAgMnPc1d1
historikové	historik	k1gMnPc1
však	však	k9
Richardovu	Richardův	k2eAgFnSc4d1
vinu	vina	k1gFnSc4
zpochybňují	zpochybňovat	k5eAaImIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
Dědičkou	dědička	k1gFnSc7
koruny	koruna	k1gFnSc2
byla	být	k5eAaImAgFnS
vdova	vdova	k1gFnSc1
jedenadvacetiletá	jedenadvacetiletý	k2eAgFnSc1d1
Isabela	Isabela	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gFnSc1
malá	malý	k2eAgFnSc1d1
dcera	dcera	k1gFnSc1
Marie	Maria	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
týden	týden	k1gInSc4
po	po	k7c6
Konrádově	Konrádův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
byla	být	k5eAaImAgFnS
Isabela	Isabela	k1gFnSc1
znovu	znovu	k6eAd1
provdána	provdán	k2eAgFnSc1d1
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
za	za	k7c4
Richardova	Richardův	k2eAgMnSc4d1
synovce	synovec	k1gMnSc4
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
byl	být	k5eAaImAgMnS
zároveň	zároveň	k6eAd1
i	i	k8xC
synovcem	synovec	k1gMnSc7
krále	král	k1gMnSc2
Filipa	Filip	k1gMnSc2
Augusta	August	k1gMnSc2
<g/>
,	,	kIx,
Jindřicha	Jindřich	k1gMnSc2
ze	z	k7c2
Champagne	Champagn	k1gInSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Důsledky	důsledek	k1gInPc1
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
království	království	k1gNnSc2
úspěšně	úspěšně	k6eAd1
vládl	vládnout	k5eAaImAgInS
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
jeruzalémským	jeruzalémský	k2eAgMnSc7d1
králem	král	k1gMnSc7
nebyl	být	k5eNaImAgMnS
nikdy	nikdy	k6eAd1
formálně	formálně	k6eAd1
korunován	korunován	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
Úspěch	úspěch	k1gInSc4
mu	on	k3xPp3gMnSc3
usnadnila	usnadnit	k5eAaPmAgFnS
dvě	dva	k4xCgNnPc4
úmrtí	úmrtí	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1193	#num#	k4
<g/>
,	,	kIx,
pět	pět	k4xCc4
měsíců	měsíc	k1gInPc2
po	po	k7c6
skončení	skončení	k1gNnSc6
války	válka	k1gFnSc2
<g/>
,	,	kIx,
zemřel	zemřít	k5eAaPmAgMnS
sultán	sultán	k1gMnSc1
Saladin	Saladina	k1gFnPc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
smrt	smrt	k1gFnSc1
vyvolala	vyvolat	k5eAaPmAgFnS
mezi	mezi	k7c7
dědici	dědic	k1gMnPc7
spory	spora	k1gFnSc2
o	o	k7c4
trůn	trůn	k1gInSc4
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
nakonec	nakonec	k6eAd1
vzešel	vzejít	k5eAaPmAgInS
jako	jako	k9
vítěz	vítěz	k1gMnSc1
Saladinův	Saladinův	k2eAgMnSc1d1
bratr	bratr	k1gMnSc1
al-Ádil	al-Ádit	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
Rok	rok	k1gInSc1
nato	nato	k6eAd1
ho	on	k3xPp3gInSc4
následoval	následovat	k5eAaImAgMnS
Guy	Guy	k1gMnSc1
de	de	k?
Lusignan	Lusignan	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
Guyovým	Guyův	k2eAgMnSc7d1
nástupcem	nástupce	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
bratr	bratr	k1gMnSc1
Amaury	Amaura	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
roku	rok	k1gInSc2
1197	#num#	k4
získal	získat	k5eAaPmAgMnS
od	od	k7c2
římsko-německého	římsko-německý	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
Jindřicha	Jindřich	k1gMnSc2
VI	VI	kA
<g/>
.	.	kIx.
královskou	královský	k2eAgFnSc4d1
hodnost	hodnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrov	ostrov	k1gInSc1
se	se	k3xPyFc4
brzy	brzy	k6eAd1
stal	stát	k5eAaPmAgMnS
nejvýznamnější	významný	k2eAgFnSc7d3
křižáckou	křižácký	k2eAgFnSc7d1
základnou	základna	k1gFnSc7
a	a	k8xC
Lusignanové	Lusignan	k1gMnPc1
zde	zde	k6eAd1
vládli	vládnout	k5eAaImAgMnP
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1473	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
z	z	k7c2
pobočných	pobočný	k2eAgFnPc2d1
větví	větev	k1gFnPc2
tohoto	tento	k3xDgInSc2
rodu	rod	k1gInSc2
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1343	#num#	k4
dostala	dostat	k5eAaPmAgFnS
také	také	k9
na	na	k7c4
trůn	trůn	k1gInSc4
Malé	Malé	k2eAgFnSc2d1
Arménie	Arménie	k1gFnSc2
(	(	kIx(
<g/>
do	do	k7c2
roku	rok	k1gInSc2
1375	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Území	území	k1gNnSc1
pod	pod	k7c7
kontrolou	kontrola	k1gFnSc7
křižáckých	křižácký	k2eAgInPc2d1
států	stát	k1gInPc2
po	po	k7c6
skončení	skončení	k1gNnSc6
třetí	třetí	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
</s>
<s>
Proklamovaného	proklamovaný	k2eAgInSc2d1
cíle	cíl	k1gInSc2
–	–	k?
dobytí	dobytí	k1gNnSc1
Jeruzaléma	Jeruzalém	k1gInSc2
–	–	k?
dosaženo	dosáhnout	k5eAaPmNgNnS
nebylo	být	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
Richardova	Richardův	k2eAgFnSc1d1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
pomohla	pomoct	k5eAaPmAgFnS
zachránit	zachránit	k5eAaPmF
křižácká	křižácký	k2eAgNnPc4d1
panství	panství	k1gNnPc4
na	na	k7c6
Východě	východ	k1gInSc6
před	před	k7c7
úplnou	úplný	k2eAgFnSc7d1
zkázou	zkáza	k1gFnSc7
a	a	k8xC
i	i	k9
když	když	k8xS
se	se	k3xPyFc4
Jeruzalémské	jeruzalémský	k2eAgNnSc1d1
království	království	k1gNnSc1
již	již	k6eAd1
nikdy	nikdy	k6eAd1
nepozvedlo	pozvednout	k5eNaPmAgNnS
ke	k	k7c3
svému	svůj	k3xOyFgNnSc3
postavení	postavení	k1gNnSc3
před	před	k7c7
rokem	rok	k1gInSc7
1187	#num#	k4
–	–	k?
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
také	také	k9
díky	díky	k7c3
dobytí	dobytí	k1gNnSc3
Kypru	Kypr	k1gInSc2
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
–	–	k?
přežilo	přežít	k5eAaPmAgNnS
dalších	další	k2eAgMnPc2d1
sto	sto	k4xCgNnSc1
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Účast	účast	k1gFnSc1
Čechů	Čech	k1gMnPc2
na	na	k7c6
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
</s>
<s>
Součástí	součást	k1gFnSc7
výpravy	výprava	k1gFnSc2
Fridricha	Fridrich	k1gMnSc2
Barbarossy	Barbarossa	k1gMnSc2
byl	být	k5eAaImAgInS
i	i	k9
český	český	k2eAgInSc1d1
oddíl	oddíl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
český	český	k2eAgMnSc1d1
kníže	kníže	k1gMnSc1
Bedřich	Bedřich	k1gMnSc1
se	se	k3xPyFc4
ke	k	k7c3
křížové	křížový	k2eAgFnSc3d1
výpravě	výprava	k1gFnSc3
zavázal	zavázat	k5eAaPmAgMnS
<g/>
,	,	kIx,
zemřel	zemřít	k5eAaPmAgMnS
ještě	ještě	k6eAd1
před	před	k7c7
zahájením	zahájení	k1gNnSc7
výpravy	výprava	k1gFnSc2
a	a	k8xC
Bedřichův	Bedřichův	k2eAgMnSc1d1
nástupce	nástupce	k1gMnSc1
Konrád	Konrád	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ota	Ota	k1gMnSc1
Bedřichův	Bedřichův	k2eAgInSc4d1
slib	slib	k1gInSc4
nevzal	vzít	k5eNaPmAgMnS
za	za	k7c4
svůj	svůj	k3xOyFgInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Českému	český	k2eAgInSc3d1
kontingentu	kontingent	k1gInSc3
tedy	tedy	k9
nakonec	nakonec	k6eAd1
velel	velet	k5eAaImAgMnS
Bedřichův	Bedřichův	k2eAgMnSc1d1
věrný	věrný	k2eAgMnSc1d1
spojenec	spojenec	k1gMnSc1
Děpolt	Děpolt	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
Česká	český	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
se	se	k3xPyFc4
skládala	skládat	k5eAaImAgFnS
nejen	nejen	k6eAd1
ze	z	k7c2
šlechtických	šlechtický	k2eAgInPc2d1
oddílů	oddíl	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gFnSc4
značnou	značný	k2eAgFnSc4d1
část	část	k1gFnSc4
tvořili	tvořit	k5eAaImAgMnP
„	„	k?
<g/>
lotrasové	lotras	k1gMnPc1
<g/>
“	“	k?
<g/>
,	,	kIx,
zločinci	zločinec	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
byli	být	k5eAaImAgMnP
pod	pod	k7c7
slibem	slib	k1gInSc7
účasti	účast	k1gFnSc2
na	na	k7c6
kruciátě	kruciáta	k1gFnSc6
propuštěni	propuštěn	k2eAgMnPc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
z	z	k7c2
českých	český	k2eAgInPc2d1
žalářů	žalář	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
Češi	česat	k5eAaImIp1nS
se	se	k3xPyFc4
k	k	k7c3
císařské	císařský	k2eAgFnSc3d1
výpravě	výprava	k1gFnSc3
připojili	připojit	k5eAaPmAgMnP
v	v	k7c6
Ostřihomi	Ostřiho	k1gFnPc7
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Děpoltova	Děpoltův	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
posílená	posílená	k1gFnSc1
o	o	k7c4
uherské	uherský	k2eAgInPc4d1
oddíly	oddíl	k1gInPc4
<g/>
,	,	kIx,
cestou	cesta	k1gFnSc7
Balkánem	Balkán	k1gInSc7
tvořila	tvořit	k5eAaImAgFnS
předvoj	předvoj	k1gInSc4
císařského	císařský	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
Když	když	k8xS
křižáci	křižák	k1gMnPc1
dorazili	dorazit	k5eAaPmAgMnP
na	na	k7c4
byzantské	byzantský	k2eAgNnSc4d1
území	území	k1gNnSc4
a	a	k8xC
střetli	střetnout	k5eAaPmAgMnP
se	se	k3xPyFc4
místním	místní	k2eAgNnSc7d1
obyvatelstvem	obyvatelstvo	k1gNnSc7
<g/>
,	,	kIx,
Češi	česat	k5eAaImIp1nS
se	se	k3xPyFc4
aktivně	aktivně	k6eAd1
zapojili	zapojit	k5eAaPmAgMnP
do	do	k7c2
drancování	drancování	k1gNnSc2
<g/>
;	;	kIx,
když	když	k8xS
křižáci	křižák	k1gMnPc1
procházeli	procházet	k5eAaImAgMnP
kolem	kolem	k7c2
Naissu	Naiss	k1gInSc2
(	(	kIx(
<g/>
Niše	Niše	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nechal	nechat	k5eAaPmAgMnS
Fridrich	Fridrich	k1gMnSc1
kvůli	kvůli	k7c3
násilnostem	násilnost	k1gFnPc3
osm	osm	k4xCc1
českých	český	k2eAgMnPc2d1
křižáků	křižák	k1gMnPc2
popravit	popravit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Češi	česat	k5eAaImIp1nS
se	se	k3xPyFc4
také	také	k9
podíleli	podílet	k5eAaImAgMnP
na	na	k7c4
vyplenění	vyplenění	k1gNnSc4
Adrianopole	Adrianopole	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
kde	kde	k6eAd1
německá	německý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
zimovala	zimovat	k5eAaImAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
únoru	únor	k1gInSc6
1190	#num#	k4
vydrancovali	vydrancovat	k5eAaPmAgMnP
neznámé	známý	k2eNgNnSc4d1
byzantské	byzantský	k2eAgNnSc4d1
přístavní	přístavní	k2eAgNnSc4d1
město	město	k1gNnSc4
a	a	k8xC
když	když	k8xS
výprava	výprava	k1gFnSc1
překročila	překročit	k5eAaPmAgFnS
v	v	k7c6
březnu	březen	k1gInSc6
1190	#num#	k4
Helespont	Helespont	k1gInSc4
a	a	k8xC
přiblížila	přiblížit	k5eAaPmAgFnS
se	se	k3xPyFc4
k	k	k7c3
Filadelfii	Filadelfie	k1gFnSc3
<g/>
,	,	kIx,
Češi	Čech	k1gMnPc1
město	město	k1gNnSc4
napadli	napadnout	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fridrich	Fridrich	k1gMnSc1
Barbarossa	Barbarossa	k1gMnSc1
okamžitě	okamžitě	k6eAd1
vyslal	vyslat	k5eAaPmAgMnS
svého	svůj	k3xOyFgMnSc4
syna	syn	k1gMnSc4
Fridricha	Fridrich	k1gMnSc4
spolu	spolu	k6eAd1
s	s	k7c7
řezenským	řezenský	k2eAgMnSc7d1
biskupem	biskup	k1gMnSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Děpolta	Děpolta	k1gFnSc1
zastavili	zastavit	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
Když	když	k8xS
se	se	k3xPyFc4
křižáci	křižák	k1gMnPc1
dali	dát	k5eAaPmAgMnP
na	na	k7c4
ústup	ústup	k1gInSc4
od	od	k7c2
Filadelfie	Filadelfie	k1gFnSc2
dále	daleko	k6eAd2
na	na	k7c4
východ	východ	k1gInSc4
<g/>
,	,	kIx,
z	z	k7c2
města	město	k1gNnSc2
vyrazilo	vyrazit	k5eAaPmAgNnS
pět	pět	k4xCc1
set	sto	k4xCgNnPc2
mladých	mladý	k1gMnPc2
řeckých	řecký	k2eAgMnPc2d1
jezdců	jezdec	k1gMnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
pomstili	pomstít	k5eAaPmAgMnP,k5eAaImAgMnP
za	za	k7c4
útok	útok	k1gInSc4
na	na	k7c6
Filadelfii	Filadelfie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
Řekové	Řek	k1gMnPc1
křižáky	křižák	k1gInPc4
dohnali	dohnat	k5eAaPmAgMnP
<g/>
,	,	kIx,
narazili	narazit	k5eAaPmAgMnP
právě	právě	k9
na	na	k7c4
Čechy	Čech	k1gMnPc4
<g/>
;	;	kIx,
Niketas	Niketas	k1gMnSc1
Choniates	Choniates	k1gMnSc1
následný	následný	k2eAgInSc4d1
střet	střet	k1gInSc4
popisuje	popisovat	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Jakmile	jakmile	k8xS
zjistili	zjistit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
narazili	narazit	k5eAaPmAgMnP
do	do	k7c2
značné	značný	k2eAgFnSc2d1
míry	míra	k1gFnSc2
spíše	spíše	k9
na	na	k7c4
železné	železný	k2eAgFnPc4d1
sochy	socha	k1gFnPc4
a	a	k8xC
nezranitelné	zranitelný	k2eNgInPc4d1
giganty	gigant	k1gInPc4
než	než	k8xS
na	na	k7c4
smrtelníky	smrtelník	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
je	on	k3xPp3gMnPc4
navíc	navíc	k6eAd1
přivítali	přivítat	k5eAaPmAgMnP
hromovým	hromový	k2eAgInSc7d1
smíchem	smích	k1gInSc7
nad	nad	k7c7
jejich	jejich	k3xOp3gFnSc7
pošetilostí	pošetilost	k1gFnSc7
<g/>
,	,	kIx,
obrátili	obrátit	k5eAaPmAgMnP
se	se	k3xPyFc4
tito	tento	k3xDgMnPc1
zmatení	zmatený	k2eAgMnPc1d1
mladí	mladý	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
na	na	k7c6
místě	místo	k1gNnSc6
a	a	k8xC
hledali	hledat	k5eAaImAgMnP
v	v	k7c6
útěku	útěk	k1gInSc6
spásu	spása	k1gFnSc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
mnohým	mnohé	k1gNnSc7
z	z	k7c2
nich	on	k3xPp3gMnPc2
způsobili	způsobit	k5eAaPmAgMnP
tito	tento	k3xDgMnPc1
obři	obr	k1gMnPc1
protáhlá	protáhlý	k2eAgNnPc4d1
zranění	zranění	k1gNnPc4
na	na	k7c6
citlivém	citlivý	k2eAgNnSc6d1
místě	místo	k1gNnSc6
zadku	zadek	k1gInSc2
-	-	kIx~
na	na	k7c6
oněch	onen	k3xDgFnPc6
polokoulích	polokoule	k1gFnPc6
-	-	kIx~
svými	svůj	k3xOyFgInPc7
zvláštními	zvláštní	k2eAgInPc7d1
dlouhými	dlouhý	k2eAgInPc7d1
biči	bič	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Niketas	Niketas	k1gMnSc1
Choniates	Choniates	k1gMnSc1
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čeští	český	k2eAgMnPc1d1
křižáci	křižák	k1gMnPc1
dále	daleko	k6eAd2
tvořili	tvořit	k5eAaImAgMnP
předvoj	předvoj	k1gInSc4
a	a	k8xC
když	když	k8xS
Turci	Turek	k1gMnPc1
v	v	k7c6
květnu	květen	k1gInSc6
1190	#num#	k4
u	u	k7c2
Filomelionu	Filomelion	k1gInSc2
napadli	napadnout	k5eAaPmAgMnP
Barbarossův	Barbarossův	k2eAgInSc4d1
předvoj	předvoj	k1gInSc4
<g/>
,	,	kIx,
Děpoltovi	Děpoltův	k2eAgMnPc1d1
rytíři	rytíř	k1gMnPc1
byli	být	k5eAaImAgMnP
schopni	schopen	k2eAgMnPc1d1
Turky	turek	k1gInPc4
porazit	porazit	k5eAaPmF
a	a	k8xC
5000	#num#	k4
jich	on	k3xPp3gFnPc2
pobít	pobít	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
Český	český	k2eAgInSc1d1
oddíl	oddíl	k1gInSc1
se	se	k3xPyFc4
dále	daleko	k6eAd2
vyznamenal	vyznamenat	k5eAaPmAgInS
13	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
v	v	k7c6
bitvě	bitva	k1gFnSc6
s	s	k7c7
Turky	Turek	k1gMnPc7
u	u	k7c2
Axarate	Axarat	k1gInSc5
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
o	o	k7c4
pět	pět	k4xCc4
dní	den	k1gInPc2
později	pozdě	k6eAd2
při	při	k7c6
dobytí	dobytí	k1gNnSc6
Iconia	Iconium	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
císařově	císařův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
německá	německý	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
prakticky	prakticky	k6eAd1
rozpadla	rozpadnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
část	část	k1gFnSc1
českých	český	k2eAgMnPc2d1
účastníků	účastník	k1gMnPc2
se	se	k3xPyFc4
obrátila	obrátit	k5eAaPmAgFnS
na	na	k7c4
cestu	cesta	k1gFnSc4
domů	dům	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
většina	většina	k1gFnSc1
<g/>
,	,	kIx,
již	již	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
značně	značně	k6eAd1
oslabeného	oslabený	k2eAgInSc2d1
oddílu	oddíl	k1gInSc2
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Děpoltem	Děpolt	k1gInSc7
pravděpodobně	pravděpodobně	k6eAd1
následovala	následovat	k5eAaImAgFnS
Fridricha	Fridrich	k1gMnSc4
Švábského	švábský	k2eAgMnSc4d1
do	do	k7c2
Sýrie	Sýrie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
7	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
pak	pak	k6eAd1
zbytek	zbytek	k1gInSc4
Barbarossových	Barbarossův	k2eAgInPc2d1
křižáků	křižák	k1gInPc2
stanul	stanout	k5eAaPmAgInS
pod	pod	k7c7
Akkonem	Akkon	k1gInSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
99	#num#	k4
<g/>
]	]	kIx)
kde	kde	k6eAd1
posílili	posílit	k5eAaPmAgMnP
armádu	armáda	k1gFnSc4
krále	král	k1gMnSc2
Guye	Guy	k1gMnSc2
de	de	k?
Lusignan	Lusignan	k1gMnSc1
a	a	k8xC
Konráda	Konrád	k1gMnSc4
z	z	k7c2
Montferratu	Montferrat	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
obléhání	obléhání	k1gNnSc2
Akkonu	Akkon	k1gInSc2
se	se	k3xPyFc4
během	během	k7c2
bitvy	bitva	k1gFnSc2
na	na	k7c6
řece	řeka	k1gFnSc6
Belus	Belus	k1gInSc1
naposledy	naposledy	k6eAd1
vyznamenala	vyznamenat	k5eAaPmAgFnS
česká	český	k2eAgFnSc1d1
družina	družina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
</s>
<s>
Nebylo	být	k5eNaImAgNnS
jich	on	k3xPp3gMnPc2
více	hodně	k6eAd2
než	než	k8xS
tři	tři	k4xCgNnPc4
sta	sto	k4xCgNnPc4
<g/>
,	,	kIx,
když	když	k9
na	na	k7c6
svých	svůj	k3xOyFgMnPc6
koních	kůň	k1gMnPc6
<g/>
,	,	kIx,
vyjíce	výt	k5eAaImSgFnP
přitom	přitom	k6eAd1
spíše	spíše	k9
jako	jako	k9
ďáblové	ďábel	k1gMnPc1
než	než	k8xS
dítka	dítko	k1gNnPc1
Boží	božit	k5eAaImIp3nP
<g/>
,	,	kIx,
vpadli	vpadnout	k5eAaPmAgMnP
na	na	k7c4
onen	onen	k3xDgInSc4
most	most	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
předtím	předtím	k6eAd1
tolik	tolik	k4xDc4,k4yIc4
dobrých	dobrý	k2eAgMnPc2d1
křesťanů	křesťan	k1gMnPc2
své	svůj	k3xOyFgInPc4
životy	život	k1gInPc4
nechati	nechat	k5eAaPmF
muselo	muset	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k4c1
jich	on	k3xPp3gMnPc2
padlo	padnout	k5eAaPmAgNnS,k5eAaImAgNnS
<g/>
,	,	kIx,
sestřeleno	sestřelit	k5eAaPmNgNnS
z	z	k7c2
koní	kůň	k1gMnPc2
<g/>
,	,	kIx,
jiní	jiný	k2eAgMnPc1d1
zahynuli	zahynout	k5eAaPmAgMnP
skláceni	sklácet	k5eAaImNgMnP,k5eAaPmNgMnP
šavlemi	šavle	k1gFnPc7
<g/>
,	,	kIx,
ale	ale	k8xC
oni	onen	k3xDgMnPc1,k3xPp3gMnPc1
neustali	ustat	k5eNaPmAgMnP
rubat	rubat	k5eAaImF
Saracény	Saracén	k1gMnPc4
<g/>
,	,	kIx,
dokud	dokud	k8xS
je	být	k5eAaImIp3nS
všechny	všechen	k3xTgInPc4
nezdolali	zdolat	k5eNaPmAgMnP
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1190	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
i	i	k9
vůdce	vůdce	k1gMnSc1
české	český	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
kníže	kníže	k1gMnSc1
Děpolt	Děpolt	k1gMnSc1
a	a	k8xC
zbytek	zbytek	k1gInSc1
Čechů	Čech	k1gMnPc2
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
služeb	služba	k1gFnPc2
Richarda	Richard	k1gMnSc2
Lví	lví	k2eAgNnSc4d1
srdce	srdce	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
osud	osud	k1gInSc1
Čechů	Čech	k1gMnPc2
na	na	k7c6
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
není	být	k5eNaImIp3nS
znám	znám	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
většina	většina	k1gFnSc1
jich	on	k3xPp3gFnPc2
na	na	k7c6
křížové	křížový	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
zemřela	zemřít	k5eAaPmAgFnS
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
existují	existovat	k5eAaImIp3nP
zprávy	zpráva	k1gFnPc1
o	o	k7c6
veteránech	veterán	k1gMnPc6
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
do	do	k7c2
Čech	Čechy	k1gFnPc2
nakonec	nakonec	k6eAd1
vrátili	vrátit	k5eAaPmAgMnP
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
už	už	k6eAd1
z	z	k7c2
Anatolie	Anatolie	k1gFnSc2
po	po	k7c6
smrti	smrt	k1gFnSc6
Fridricha	Fridrich	k1gMnSc2
Barbarossy	Barbarossa	k1gMnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
i	i	k9
z	z	k7c2
Palestiny	Palestina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Robert	Robert	k1gMnSc1
ze	z	k7c2
St.	st.	kA
Albans	Albans	k1gInSc1
byl	být	k5eAaImAgMnS
templářský	templářský	k2eAgMnSc1d1
rytíř	rytíř	k1gMnSc1
anglického	anglický	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
konvertoval	konvertovat	k5eAaBmAgInS
k	k	k7c3
islámu	islám	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oženil	oženit	k5eAaPmAgMnS
se	se	k3xPyFc4
se	s	k7c7
Saladinovou	Saladinový	k2eAgFnSc7d1
neteří	neteř	k1gFnSc7
a	a	k8xC
roku	rok	k1gInSc2
1185	#num#	k4
působil	působit	k5eAaImAgMnS
jako	jako	k9
velitel	velitel	k1gMnSc1
v	v	k7c6
Saladinově	Saladinův	k2eAgNnSc6d1
vojsku	vojsko	k1gNnSc6
během	během	k7c2
vojenského	vojenský	k2eAgNnSc2d1
tažení	tažení	k1gNnSc2
proti	proti	k7c3
křižákům	křižák	k1gInPc3
a	a	k8xC
Jeruzalémskému	jeruzalémský	k2eAgNnSc3d1
království	království	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřel	zemřít	k5eAaPmAgMnS
roku	rok	k1gInSc2
1187	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
KONSTAM	KONSTAM	kA
<g/>
,	,	kIx,
Angus	Angus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historical	Historical	k1gMnSc1
Atlas	Atlas	k1gMnSc1
of	of	k?
the	the	k?
Crusades	Crusades	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Londýn	Londýn	k1gInSc1
<g/>
:	:	kIx,
Mercury	Mercur	k1gInPc1
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1904668003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
124	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CHISHOLM	CHISHOLM	kA
<g/>
,	,	kIx,
Hugh	Hugh	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Encyclopaedia	Encyclopaedium	k1gNnSc2
Britannica	Britannic	k1gInSc2
:	:	kIx,
A	a	k8xC
Dictionary	Dictionar	k1gInPc1
of	of	k?
Arts	Arts	k1gInSc1
<g/>
,	,	kIx,
Sciences	Sciences	k1gInSc1
<g/>
,	,	kIx,
Literature	Literatur	k1gMnSc5
and	and	k?
General	General	k1gMnSc4
Information	Information	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Encyclopæ	Encyclopæ	k1gNnSc2
Britannica	Britannic	k1gInSc2
Company	Compana	k1gFnSc2
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
294	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
PHILLIPS	PHILLIPS	kA
<g/>
,	,	kIx,
Jonathan	Jonathan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Fourth	Fourth	k1gMnSc1
Crusade	Crusad	k1gInSc5
and	and	k?
the	the	k?
Sack	Sack	k1gMnSc1
of	of	k?
Constantinople	Constantinople	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Londýn	Londýn	k1gInSc1
<g/>
:	:	kIx,
Pimlico	Pimlico	k1gNnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
670033502	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
66	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Phillips	Phillips	k1gInSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
MAALOUF	MAALOUF	kA
<g/>
,	,	kIx,
Amin	amin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Les	les	k1gInSc1
croisades	croisades	k1gMnSc1
vues	vuesa	k1gFnPc2
par	para	k1gFnPc2
les	les	k1gInSc1
Arabes	Arabes	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paříž	Paříž	k1gFnSc1
<g/>
:	:	kIx,
J.	J.	kA
<g/>
-	-	kIx~
<g/>
C.	C.	kA
Lattè	Lattè	k1gInSc1
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
217	#num#	k4
<g/>
–	–	k?
<g/>
225	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Maalouf	Maalouf	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
KOVAŘÍK	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Meč	meč	k1gInSc4
a	a	k8xC
kříž	kříž	k1gInSc4
<g/>
,	,	kIx,
rytířské	rytířský	k2eAgFnPc1d1
bitvy	bitva	k1gFnPc1
a	a	k8xC
osudy	osud	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
1289	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
236	#num#	k4
<g/>
-	-	kIx~
<g/>
237	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Kovařík	Kovařík	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
168	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Maalouf	Maalouf	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
225	#num#	k4
<g/>
–	–	k?
<g/>
232.1	232.1	k4
2	#num#	k4
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
170	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GROUSSET	GROUSSET	kA
<g/>
,	,	kIx,
René	René	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Histoire	Histoir	k1gInSc5
des	des	k1gNnSc7
croisades	croisades	k1gInSc1
et	et	k?
du	du	k?
royaume	royaum	k1gInSc5
franc	franc	k6eAd1
de	de	k?
Jérusalem	Jérusal	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
III	III	kA
–	–	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
anarchie	anarchie	k1gFnSc1
franque	franque	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paříž	Paříž	k1gFnSc1
<g/>
:	:	kIx,
Plon	Plon	k1gNnSc1
<g/>
,	,	kIx,
1936	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
61	#num#	k4
<g/>
–	–	k?
<g/>
66	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Grousset	Grousset	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
DUGGAN	DUGGAN	kA
<g/>
,	,	kIx,
Alfred	Alfred	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižácké	křižácký	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Orbis	orbis	k1gInSc1
<g/>
,	,	kIx,
1973	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
142	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Duggan	Duggan	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Grousset	Grousset	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
45	#num#	k4
<g/>
–	–	k?
<g/>
52.1	52.1	k4
2	#num#	k4
3	#num#	k4
Duggan	Duggana	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
139	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
171	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
215.1	215.1	k4
2	#num#	k4
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
172.1	172.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
MADDEN	MADDEN	kA
<g/>
,	,	kIx,
Thomas	Thomas	k1gMnSc1
F.	F.	kA
The	The	k1gMnSc1
new	new	k?
concise	concise	k1gFnSc2
history	histor	k1gInPc4
of	of	k?
the	the	k?
Crusades	Crusades	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Barnes	Barnes	k1gInSc1
&	&	k?
Noble	Noble	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
742538222	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
79	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k6eAd1
Madden	Maddna	k1gFnPc2
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
MAYER	Mayer	k1gMnSc1
<g/>
,	,	kIx,
Hans	Hans	k1gMnSc1
E.	E.	kA
The	The	k1gMnSc1
Crusades	Crusades	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxford	Oxford	k1gInSc1
<g/>
:	:	kIx,
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
139	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
172	#num#	k4
<g/>
–	–	k?
<g/>
173.1	173.1	k4
2	#num#	k4
3	#num#	k4
Duggan	Duggana	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
143.1	143.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
173	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SOUKUP	Soukup	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
dle	dle	k7c2
kronikáře	kronikář	k1gMnSc2
Ansberta	Ansbert	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příbram	Příbram	k1gFnSc1
<g/>
:	:	kIx,
Knihovna	knihovna	k1gFnSc1
Jana	Jan	k1gMnSc2
Drdy	Drda	k1gMnSc2
v	v	k7c6
Příbrami	Příbram	k1gFnSc6
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86240	#num#	k4
<g/>
-	-	kIx~
<g/>
67	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
50	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Soukup	Soukup	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Madden	Maddna	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
80.1	80.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
174.1	174.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Duggan	Duggana	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
149	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BRIDGE	BRIDGE	kA
<g/>
,	,	kIx,
Antony	anton	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížové	Křížové	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
512	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
151	#num#	k4
<g/>
-	-	kIx~
<g/>
152	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Bridge	Bridge	k1gFnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
Bridge	Bridge	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
152	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Soukup	Soukup	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HROCHOVÁ	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
Věra	Věra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížové	Křížové	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
ve	v	k7c6
světle	světlo	k1gNnSc6
soudobých	soudobý	k2eAgFnPc2d1
kronik	kronika	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgNnSc1d1
pedagogické	pedagogický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
149	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
ve	v	k7c6
světle	světlo	k1gNnSc6
soudobých	soudobý	k2eAgFnPc2d1
kronik	kronika	k1gFnPc2
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Madden	Maddna	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
82	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Duggan	Duggan	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
149	#num#	k4
<g/>
–	–	k?
<g/>
150.1	150.1	k4
2	#num#	k4
3	#num#	k4
Duggan	Duggana	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
150	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
152	#num#	k4
<g/>
–	–	k?
<g/>
153	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
153	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
ve	v	k7c6
světle	světlo	k1gNnSc6
soudobých	soudobý	k2eAgFnPc2d1
kronik	kronika	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
155	#num#	k4
<g/>
↑	↑	k?
WIBECK	WIBECK	kA
<g/>
,	,	kIx,
Sören	Sören	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korstå	Korstå	k1gInSc1
<g/>
:	:	kIx,
Västerlandets	Västerlandets	k1gInSc1
heliga	helig	k1gMnSc2
krig	krig	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lund	Lund	k1gInSc1
<g/>
:	:	kIx,
Historiska	Historiska	k1gFnSc1
Media	medium	k1gNnSc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9185507520	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
184	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
švédsky	švédsky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Wibeck	Wibeck	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Bridge	Bridge	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
154.1	154.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Madden	Maddna	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
86.1	86.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Duggan	Duggana	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
152.1	152.1	k4
<g />
.	.	kIx.
</s>
<s hack="1">
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Bridge	Bridge	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
155.1	155.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
Duggan	Duggana	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
153	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
155	#num#	k4
<g/>
–	–	k?
<g/>
156	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
OSTROGORSKY	OSTROGORSKY	kA
<g/>
,	,	kIx,
Georg	Georg	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Storia	Storium	k1gNnPc1
dell	della	k1gFnPc2
<g/>
'	'	kIx"
<g/>
Impero	Impero	k1gNnSc1
bizantino	bizantin	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Milán	Milán	k1gInSc1
<g/>
:	:	kIx,
Einaudi	Einaud	k1gMnPc1
<g/>
,	,	kIx,
1968	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
8806173626	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
360	#num#	k4
<g/>
–	–	k?
<g/>
361	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
Bridge	Bridge	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
156.1	156.1	k4
2	#num#	k4
3	#num#	k4
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
177.1	177.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
176.1	176.1	k4
2	#num#	k4
3	#num#	k4
Bridge	Bridge	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
157	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
WETZKY	WETZKY	kA
<g/>
,	,	kIx,
Karl	Karl	k1gMnSc1
von	von	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bůh	bůh	k1gMnSc1
tomu	ten	k3xDgNnSc3
chce	chtít	k5eAaImIp3nS
<g/>
:	:	kIx,
Češi	Čech	k1gMnPc1
a	a	k8xC
Moravané	Moravan	k1gMnPc1
na	na	k7c4
3	#num#	k4
<g/>
.	.	kIx.
křížové	křížový	k2eAgFnSc2d1
výpravě	výprava	k1gFnSc3
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Moba	Moba	k1gMnSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7173	#num#	k4
<g/>
-	-	kIx~
<g/>
917	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
157	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Wetzky	Wetzka	k1gFnSc2
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Wibeck	Wibeck	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
181	#num#	k4
<g/>
–	–	k?
<g/>
184	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
176	#num#	k4
<g/>
-	-	kIx~
<g/>
177	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Wetzky	Wetzka	k1gFnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
164.1	164.1	k4
2	#num#	k4
3	#num#	k4
Duggan	Duggana	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
154	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
ve	v	k7c6
světle	světlo	k1gNnSc6
soudobých	soudobý	k2eAgFnPc2d1
kronik	kronika	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
161	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
ve	v	k7c6
světle	světlo	k1gNnSc6
soudobých	soudobý	k2eAgFnPc2d1
kronik	kronika	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
159	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
158	#num#	k4
<g/>
–	–	k?
<g/>
159.1	159.1	k4
2	#num#	k4
Duggan	Duggana	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
155	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MCGLYNN	MCGLYNN	kA
<g/>
,	,	kIx,
Sean	Sean	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mečem	meč	k1gInSc7
a	a	k8xC
ohněm	oheň	k1gInSc7
<g/>
:	:	kIx,
Ukrutnosti	ukrutnost	k1gFnSc2
a	a	k8xC
zvěrstva	zvěrstvo	k1gNnSc2
středověkého	středověký	k2eAgNnSc2d1
válečnictví	válečnictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
2211	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
81	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Duggan	Duggan	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
155	#num#	k4
<g/>
–	–	k?
<g/>
156.1	156.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
178.1	178.1	k4
2	#num#	k4
3	#num#	k4
Bridge	Bridge	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
160.1	160.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Duggan	Duggana	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
156	#num#	k4
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
↑	↑	k?
Bridge	Bridge	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
160	#num#	k4
<g/>
–	–	k?
<g/>
161	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Duggan	Duggan	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
156	#num#	k4
<g/>
–	–	k?
<g/>
157.1	157.1	k4
2	#num#	k4
Bridge	Bridge	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
161.1	161.1	k4
2	#num#	k4
3	#num#	k4
Duggan	Duggana	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
157	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
NICOLLE	NICOLLE	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
1191	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
247	#num#	k4
<g/>
-	-	kIx~
<g/>
2382	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
55	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Nicolle	Nicolle	k1gFnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
178	#num#	k4
<g/>
-	-	kIx~
<g/>
179.1	179.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
179	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
162	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
OBERMEIER	OBERMEIER	kA
<g/>
,	,	kIx,
Siegfried	Siegfried	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Richard	Richard	k1gMnSc1
Lví	lví	k2eAgNnSc1d1
srdce	srdce	k1gNnSc1
–	–	k?
Král	Král	k1gMnSc1
<g/>
,	,	kIx,
rytíř	rytíř	k1gMnSc1
a	a	k8xC
dobrodruh	dobrodruh	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paha	Paha	k1gMnSc1
<g/>
:	:	kIx,
Ikar	Ikar	k1gMnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
242	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
39	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
108	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Nicolle	Nicolle	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
76.1	76.1	k4
2	#num#	k4
3	#num#	k4
Duggan	Duggana	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
158.1	158.1	k4
2	#num#	k4
Bridge	Bridge	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
163.1	163.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Duggan	Duggana	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
159.1	159.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
str	str	kA
<g/>
.	.	kIx.
180.1	180.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Bridge	Bridge	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
164.1	164.1	k4
2	#num#	k4
Duggan	Duggana	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
160.1	160.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Bridge	Bridge	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
165.1	165.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Duggan	Duggana	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
163.1	163.1	k4
2	#num#	k4
3	#num#	k4
Bridge	Bridge	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
166.1	166.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Bridge	Bridge	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
167.1	167.1	k4
2	#num#	k4
3	#num#	k4
Duggan	Duggana	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
164	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
167	#num#	k4
<g/>
–	–	k?
<g/>
168.1	168.1	k4
2	#num#	k4
Bridge	Bridge	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
168	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
180	#num#	k4
<g/>
–	–	k?
<g/>
181.1	181.1	k4
2	#num#	k4
TAUER	TAUER	kA
<g/>
,	,	kIx,
Felix	Felix	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svět	svět	k1gInSc1
islámu	islám	k1gInSc2
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
a	a	k8xC
kultura	kultura	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
702	#num#	k4
<g/>
-	-	kIx~
<g/>
1828	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
185	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
181.1	181.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Duggan	Duggana	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
161	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
241	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bridge	Bridge	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
164	#num#	k4
<g/>
–	–	k?
<g/>
165.1	165.1	k4
2	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
3	#num#	k4
4	#num#	k4
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
182	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Duggan	Duggan	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
165.1	165.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Hrochová	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
175	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Wetzky	Wetzka	k1gFnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
162	#num#	k4
<g/>
–	–	k?
<g/>
163.1	163.1	k4
2	#num#	k4
ŠOLLE	ŠOLLE	kA
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
stopách	stopa	k1gFnPc6
přemyslovských	přemyslovský	k2eAgInPc2d1
Děpolticů	Děpoltiec	k1gInPc2
<g/>
:	:	kIx,
Příspěvek	příspěvek	k1gInSc1
ke	k	k7c3
genezi	geneze	k1gFnSc3
města	město	k1gNnSc2
Kouřimě	Kouřim	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7021	#num#	k4
<g/>
-	-	kIx~
<g/>
343	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
64	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Wetzky	Wetzka	k1gFnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
80	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Wetzky	Wetzka	k1gFnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
81	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Wetzky	Wetzka	k1gFnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
100	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Wetzky	Wetzka	k1gFnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
109	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Wetzky	Wetzka	k1gFnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
158	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Wetzky	Wetzka	k1gFnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
162	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BARBER	BARBER	kA
<g/>
,	,	kIx,
Malcolm	Malcolm	k1gMnSc1
<g/>
;	;	kIx,
BATE	BATE	kA
<g/>
,	,	kIx,
Keith	Keith	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letters	Letters	k1gInSc1
from	from	k1gInSc4
the	the	k?
East	East	k1gInSc1
:	:	kIx,
crusaders	crusaders	k1gInSc1
<g/>
,	,	kIx,
pilgrims	pilgrims	k1gInSc1
and	and	k?
settlers	settlers	k1gInSc1
in	in	k?
the	the	k?
12	#num#	k4
<g/>
th-	th-	k?
<g/>
13	#num#	k4
<g/>
th	th	k?
centuries	centuries	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Farnham	Farnham	k1gInSc4
<g/>
:	:	kIx,
Ashgate	Ashgat	k1gMnSc5
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
188	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7546	#num#	k4
<g/>
-	-	kIx~
<g/>
6356	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
BRIDGE	BRIDGE	kA
<g/>
,	,	kIx,
Antony	anton	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížové	Křížové	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
228	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
512	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
BÜHLER	BÜHLER	kA
<g/>
,	,	kIx,
Arnold	Arnold	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Der	drát	k5eAaImRp2nS
Kreuzzug	Kreuzzug	k1gMnSc1
Friedrich	Friedrich	k1gMnSc1
Barbarossas	Barbarossas	k1gMnSc1
<g/>
,	,	kIx,
1187-1190	1187-1190	k4
:	:	kIx,
Bericht	Bericht	k2eAgInSc1d1
eines	eines	k1gInSc1
Augenzeugen	Augenzeugen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stuttgart	Stuttgart	k1gInSc1
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Thorbecke	Thorbeck	k1gFnSc2
Verlag	Verlag	k1gMnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
191	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
79950612	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
DUGGAN	DUGGAN	kA
<g/>
,	,	kIx,
Alfred	Alfred	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižácké	křižácký	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Orbis	orbis	k1gInSc1
<g/>
,	,	kIx,
1973	#num#	k4
<g/>
.	.	kIx.
214	#num#	k4
s.	s.	k?
</s>
<s>
EDBURY	EDBURY	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
W.	W.	kA
The	The	k1gMnSc1
conquest	conquest	k1gMnSc1
of	of	k?
Jerusalem	Jerusalem	k1gInSc1
and	and	k?
the	the	k?
Third	Third	k1gInSc1
Crusade	Crusad	k1gInSc5
:	:	kIx,
sources	sources	k1gInSc1
in	in	k?
translation	translation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aldershot	Aldershot	k1gInSc4
<g/>
:	:	kIx,
Ashgate	Ashgat	k1gMnSc5
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
196	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
84014	#num#	k4
<g/>
-	-	kIx~
<g/>
676	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
GABRIELI	Gabriel	k1gMnSc5
<g/>
,	,	kIx,
Francesco	Francesco	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížové	Křížové	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
očima	oko	k1gNnPc7
arabských	arabský	k2eAgInPc2d1
kronikářů	kronikář	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
344	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
257	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
333	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HROCHOVÁ	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
Věra	Věra	k1gFnSc1
<g/>
;	;	kIx,
HROCH	Hroch	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižáci	křižák	k1gMnPc1
ve	v	k7c6
Svaté	svatý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
289	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
621	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HROCHOVÁ	Hrochová	k1gFnSc1
<g/>
,	,	kIx,
Věra	Věra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křížové	Křížové	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
ve	v	k7c6
světle	světlo	k1gNnSc6
soudobých	soudobý	k2eAgFnPc2d1
kronik	kronika	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgNnSc1d1
pedagogické	pedagogický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
255	#num#	k4
s.	s.	k?
</s>
<s>
MAALOUF	MAALOUF	kA
<g/>
,	,	kIx,
Amin	amin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
crusades	crusades	k1gMnSc1
through	through	k1gMnSc1
Arab	Arab	k1gMnSc1
eyes	eyes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Schocken	Schocken	k2eAgInSc1d1
Books	Books	k1gInSc1
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
.	.	kIx.
293	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8052	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
898	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
NICOLLE	NICOLLE	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
1191	#num#	k4
:	:	kIx,
Richard	Richard	k1gMnSc1
Lví	lví	k2eAgNnSc4d1
srdce	srdce	k1gNnSc4
<g/>
,	,	kIx,
Saladin	Saladin	k2eAgInSc4d1
a	a	k8xC
zápas	zápas	k1gInSc4
o	o	k7c4
Jeruzalém	Jeruzalém	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
247	#num#	k4
<g/>
-	-	kIx~
<g/>
2382	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
OBERMEIER	OBERMEIER	kA
<g/>
,	,	kIx,
Siegfried	Siegfried	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Richard	Richard	k1gMnSc1
Lví	lví	k2eAgNnSc1d1
srdce	srdce	k1gNnSc1
:	:	kIx,
král	král	k1gMnSc1
<g/>
,	,	kIx,
rytíř	rytíř	k1gMnSc1
<g/>
,	,	kIx,
dobrodruh	dobrodruh	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ikar	Ikar	k1gMnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7202	#num#	k4
<g/>
-	-	kIx~
<g/>
481	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
RUNCIMAN	RUNCIMAN	kA
<g/>
,	,	kIx,
Steven	Steven	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
history	histor	k1gInPc1
of	of	k?
the	the	k?
Crusades	Crusades	k1gInSc1
<g/>
.	.	kIx.
vol	vol	k6eAd1
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
The	The	k1gFnSc1
kingdom	kingdom	k1gInSc1
of	of	k?
Acre	Acre	k1gInSc1
and	and	k?
the	the	k?
later	later	k1gMnSc1
crusades	crusades	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
;	;	kIx,
New-York	New-York	k1gInSc1
;	;	kIx,
Toronto	Toronto	k1gNnSc1
<g/>
:	:	kIx,
Penguin	Penguin	k2eAgInSc1d1
Books	Books	k1gInSc1
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
.	.	kIx.
529	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
13705	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
SMAIL	SMAIL	kA
<g/>
,	,	kIx,
Raymond	Raymond	k1gMnSc1
Charles	Charles	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Crusading	Crusading	k1gInSc1
Warfare	Warfar	k1gMnSc5
(	(	kIx(
<g/>
1097	#num#	k4
<g/>
-	-	kIx~
<g/>
1193	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Barnes	Barnes	k1gInSc1
&	&	k?
Noble	Noble	k1gInSc1
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
1956	#num#	k4
<g/>
.	.	kIx.
272	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
56619	#num#	k4
<g/>
-	-	kIx~
<g/>
769	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
WETZKY	WETZKY	kA
<g/>
,	,	kIx,
Karl	Karl	k1gMnSc1
von	von	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bůh	bůh	k1gMnSc1
tomu	ten	k3xDgNnSc3
chce	chtít	k5eAaImIp3nS
:	:	kIx,
Češi	Čech	k1gMnPc1
a	a	k8xC
Moravané	Moravan	k1gMnPc1
na	na	k7c4
3	#num#	k4
<g/>
.	.	kIx.
křížové	křížový	k2eAgFnSc2d1
výpravě	výprava	k1gFnSc3
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Moba	Moba	k1gMnSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7173	#num#	k4
<g/>
-	-	kIx~
<g/>
917	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
SOUKUP	Soukup	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
dle	dle	k7c2
kronikáře	kronikář	k1gMnSc2
Ansberta	Ansbert	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příbram	Příbram	k1gFnSc1
<g/>
:	:	kIx,
Knihovna	knihovna	k1gFnSc1
Jana	Jan	k1gMnSc2
Drdy	Drda	k1gMnSc2
v	v	k7c6
Příbrami	Příbram	k1gFnSc6
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
151	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86240	#num#	k4
<g/>
-	-	kIx~
<g/>
67	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TATE	TATE	kA
<g/>
,	,	kIx,
Georges	Georges	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižáci	křižák	k1gMnPc1
v	v	k7c6
Orientu	Orient	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Slovart	Slovarta	k1gFnPc2
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
192	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85871	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TYERMAN	TYERMAN	kA
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svaté	svatý	k2eAgInPc1d1
války	válek	k1gInPc1
:	:	kIx,
dějiny	dějiny	k1gFnPc1
křížových	křížový	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
926	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7422	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
91	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
WOLFF	WOLFF	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
L.	L.	kA
<g/>
;	;	kIx,
HAZARD	hazard	k1gMnSc1
<g/>
,	,	kIx,
Harry	Harr	k1gMnPc4
W.	W.	kA
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
History	Histor	k1gInPc1
of	of	k?
the	the	k?
Crusades	Crusades	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vol	vol	k6eAd1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
The	The	k1gMnSc1
later	later	k1gMnSc1
Crusades	Crusades	k1gMnSc1
<g/>
,	,	kIx,
1189	#num#	k4
<g/>
-	-	kIx~
<g/>
1311	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Madison	Madison	k1gInSc1
<g/>
:	:	kIx,
University	universita	k1gFnSc2
of	of	k?
Wisconsin	Wisconsin	k2eAgInSc1d1
Press	Press	k1gInSc1
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
.	.	kIx.
871	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Křížové	Křížové	k2eAgFnPc1d1
výpravy	výprava	k1gFnPc1
</s>
<s>
Křižácké	křižácký	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Outremer	Outremer	k1gMnSc1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
Barbarossa	Barbarossa	k1gMnSc1
</s>
<s>
Saladin	Saladin	k2eAgInSc1d1
</s>
<s>
Usáma	Usáma	k1gNnSc1
Ibn	Ibn	k1gMnSc1
Munkiz	Munkiz	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Třetí	třetí	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravyproti	výpravyprot	k1gMnPc5
muslimům	muslim	k1gMnPc3
</s>
<s>
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
(	(	kIx(
<g/>
1095	#num#	k4
<g/>
–	–	k?
<g/>
1291	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lidová	lidový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1096	#num#	k4
<g/>
)	)	kIx)
•	•	k?
První	první	k4xOgFnSc2
(	(	kIx(
<g/>
1095	#num#	k4
<g/>
–	–	k?
<g/>
1099	#num#	k4
<g/>
)	)	kIx)
•	•	k?
výprava	výprava	k1gFnSc1
roku	rok	k1gInSc2
1101	#num#	k4
(	(	kIx(
<g/>
1101	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Norská	norský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1107	#num#	k4
<g/>
–	–	k?
<g/>
1110	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Benátská	benátský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1122	#num#	k4
<g/>
–	–	k?
<g/>
1124	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Damašská	damašský	k2eAgFnSc1d1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1129	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Druhá	druhý	k4xOgFnSc1
(	(	kIx(
<g/>
1147	#num#	k4
<g/>
–	–	k?
<g/>
1149	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Amauryho	Amaury	k1gMnSc2
tažení	tažení	k1gNnSc1
do	do	k7c2
Egypta	Egypt	k1gInSc2
(	(	kIx(
<g/>
1154	#num#	k4
<g/>
–	–	k?
<g/>
1169	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Třetí	třetí	k4xOgFnSc2
(	(	kIx(
<g/>
1189	#num#	k4
<g/>
–	–	k?
<g/>
1192	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Německá	německý	k2eAgFnSc1d1
(	(	kIx(
<g/>
1197	#num#	k4
<g/>
–	–	k?
<g/>
1198	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Čtvrtá	čtvrtá	k1gFnSc1
(	(	kIx(
<g/>
1202	#num#	k4
<g/>
–	–	k?
<g/>
1204	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dětské	dětský	k2eAgNnSc1d1
(	(	kIx(
<g/>
1212	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pátá	pátá	k1gFnSc1
(	(	kIx(
<g/>
1217	#num#	k4
<g/>
–	–	k?
<g/>
1221	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Šestá	šestý	k4xOgFnSc1
(	(	kIx(
<g/>
1228	#num#	k4
<g/>
–	–	k?
<g/>
1229	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Baronská	baronský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1239	#num#	k4
<g/>
–	–	k?
<g/>
1241	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Pastýřská	pastýřská	k1gFnSc1
(	(	kIx(
<g/>
1251	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sedmá	sedmý	k4xOgFnSc1
(	(	kIx(
<g/>
1248	#num#	k4
<g/>
–	–	k?
<g/>
1254	#num#	k4
<g/>
)	)	kIx)
•	•	k?
výprava	výprava	k1gFnSc1
roku	rok	k1gInSc2
1267	#num#	k4
•	•	k?
Osmá	osmý	k4xOgFnSc1
(	(	kIx(
<g/>
1270	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Devátá	devátý	k4xOgFnSc1
(	(	kIx(
<g/>
1271	#num#	k4
<g/>
–	–	k?
<g/>
1272	#num#	k4
<g/>
)	)	kIx)
Reconquista	Reconquista	k1gMnSc1
<g/>
(	(	kIx(
<g/>
718	#num#	k4
<g/>
–	–	k?
<g/>
1492	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Reconquista	Reconquista	k1gMnSc1
(	(	kIx(
<g/>
718	#num#	k4
<g/>
–	–	k?
<g/>
1492	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Barbastro	Barbastro	k1gNnSc4
(	(	kIx(
<g/>
1063	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mallorka	Mallorka	k1gFnSc1
(	(	kIx(
<g/>
1313	#num#	k4
<g/>
–	–	k?
<g/>
1315	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Las	laso	k1gNnPc2
Navas	Navas	k1gInSc1
de	de	k?
Tolosa	Tolosa	k1gFnSc1
(	(	kIx(
<g/>
1212	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pastýřská	pastýřská	k1gFnSc1
(	(	kIx(
<g/>
1320	#num#	k4
<g/>
)	)	kIx)
po	po	k7c6
roce	rok	k1gInSc6
1291	#num#	k4
</s>
<s>
Chudinská	chudinský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1309	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Smyrenské	smyrenský	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
(	(	kIx(
<g/>
1343	#num#	k4
<g/>
–	–	k?
<g/>
1351	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Alexandrijská	alexandrijský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1365	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mahdijská	Mahdijský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1390	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Modlící	modlící	k2eAgMnSc1d1
se	se	k3xPyFc4
křižácký	křižácký	k2eAgMnSc1d1
rytíř	rytíř	k1gMnSc1
na	na	k7c6
dobové	dobový	k2eAgFnSc6d1
miniatuře	miniatura	k1gFnSc6
(	(	kIx(
<g/>
BL	BL	kA
MS	MS	kA
Royal	Royal	k1gInSc4
2A	2A	k4
XXII	XXII	kA
f.	f.	k?
220	#num#	k4
<g/>
)	)	kIx)
severní	severní	k2eAgFnSc2d1
křížové	křížový	k2eAgFnSc2d1
výpravyproti	výpravyprot	k1gMnPc5
pohanům	pohan	k1gMnPc3
</s>
<s>
Kalmarská	Kalmarský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1123	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Venedská	Venedský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1147	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Švédské	švédský	k2eAgNnSc1d1
(	(	kIx(
<g/>
tři	tři	k4xCgFnPc1
<g/>
)	)	kIx)
•	•	k?
Livonská	Livonský	k2eAgFnSc1d1
(	(	kIx(
<g/>
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Pruská	pruský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1217	#num#	k4
<g/>
–	–	k?
<g/>
1274	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Litevská	litevský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1283	#num#	k4
<g/>
–	–	k?
<g/>
1410	#num#	k4
<g/>
)	)	kIx)
křížové	křížový	k2eAgFnSc2d1
výpravyproti	výpravyprot	k1gMnPc5
kacířům	kacíř	k1gMnPc3
</s>
<s>
proti	proti	k7c3
kacířům	kacíř	k1gMnPc3
aproti	aprot	k1gMnPc1
neposlušnýmpanovníkům	neposlušnýmpanovník	k1gMnPc3
</s>
<s>
Albigenská	albigenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1209	#num#	k4
<g/>
–	–	k?
<g/>
1229	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Drentská	Drentský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1228	#num#	k4
<g/>
–	–	k?
<g/>
1232	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Stedingerská	Stedingerský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1233	#num#	k4
<g/>
–	–	k?
<g/>
1234	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bosenská	bosenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1235	#num#	k4
<g/>
–	–	k?
<g/>
1241	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Novgorodská	novgorodský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1241	#num#	k4
<g/>
–	–	k?
<g/>
1242	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Aragonská	aragonský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1284	#num#	k4
<g/>
–	–	k?
<g/>
1285	#num#	k4
<g/>
)	)	kIx)
•	•	k?
do	do	k7c2
Čech	Čechy	k1gFnPc2
proti	proti	k7c3
valdenským	valdenští	k1gMnPc3
(	(	kIx(
<g/>
1340	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Despenserova	Despenserův	k2eAgInSc2d1
(	(	kIx(
<g/>
1382	#num#	k4
<g/>
–	–	k?
<g/>
1383	#num#	k4
<g/>
)	)	kIx)
proti	proti	k7c3
husitům	husita	k1gMnPc3
</s>
<s>
První	první	k4xOgMnSc1
(	(	kIx(
<g/>
1420	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Druhá	druhý	k4xOgFnSc1
(	(	kIx(
<g/>
1421	#num#	k4
<g/>
–	–	k?
<g/>
1422	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Třetí	třetí	k4xOgFnSc2
(	(	kIx(
<g/>
1427	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Čtvrtá	čtvrtá	k1gFnSc1
(	(	kIx(
<g/>
1431	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Česko-uherské	česko-uherský	k2eAgFnSc2d1
války	válka	k1gFnSc2
(	(	kIx(
<g/>
1468	#num#	k4
<g/>
-	-	kIx~
<g/>
1478	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
pozdní	pozdní	k2eAgInPc1d1
křížové	křížový	k2eAgInPc1d1
výpravyproti	výpravyprot	k1gMnPc1
Osmanským	osmanský	k2eAgInSc7d1
Turkům	turek	k1gInPc3
</s>
<s>
Savojská	savojský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1366	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Nikopolská	Nikopolský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1396	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Varnská	Varnská	k1gFnSc1
(	(	kIx(
<g/>
1444	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Projekt	projekt	k1gInSc1
Evžena	Evžen	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1444	#num#	k4
<g/>
–	–	k?
<g/>
1445	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Projekt	projekt	k1gInSc1
Kalixta	Kalixta	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1455	#num#	k4
<g/>
–	–	k?
<g/>
1458	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Projekt	projekt	k1gInSc1
Pia	Pius	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1463	#num#	k4
<g/>
–	–	k?
<g/>
1464	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Portugalská	portugalský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1481	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
456961	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4139116-0	4139116-0	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85034383	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85034383	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k5eAaPmF
<g/>
:	:	kIx,
<g/>
bold	bold	k6eAd1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc7
<g/>
:	:	kIx,
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
|	|	kIx~
Středověk	středověk	k1gInSc1
|	|	kIx~
Válka	válka	k1gFnSc1
</s>
