<p>
<s>
Radomilický	Radomilický	k2eAgInSc1d1	Radomilický
potok	potok	k1gInSc1	potok
je	být	k5eAaImIp3nS	být
pravostranný	pravostranný	k2eAgInSc1d1	pravostranný
přítok	přítok	k1gInSc1	přítok
řeky	řeka	k1gFnSc2	řeka
Blanice	Blanice	k1gFnSc2	Blanice
protékající	protékající	k2eAgInPc1d1	protékající
okresy	okres	k1gInPc1	okres
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
,	,	kIx,	,
Strakonice	Strakonice	k1gFnPc1	Strakonice
a	a	k8xC	a
Písek	Písek	k1gInSc1	Písek
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
délkou	délka	k1gFnSc7	délka
21,3	[number]	k4	21,3
km	km	kA	km
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
Zlatém	zlatý	k2eAgInSc6d1	zlatý
potoce	potok	k1gInSc6	potok
druhým	druhý	k4xOgInSc7	druhý
nejdelším	dlouhý	k2eAgInSc7d3	nejdelší
přítokem	přítok	k1gInSc7	přítok
Blanice	Blanice	k1gFnSc2	Blanice
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
jeho	on	k3xPp3gNnSc2	on
povodí	povodí	k1gNnSc2	povodí
měří	měřit	k5eAaImIp3nS	měřit
91,2	[number]	k4	91,2
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc1	průběh
toku	tok	k1gInSc2	tok
==	==	k?	==
</s>
</p>
<p>
<s>
Potok	potok	k1gInSc1	potok
pramení	pramenit	k5eAaImIp3nS	pramenit
u	u	k7c2	u
Kaliště	kaliště	k1gNnSc2	kaliště
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
503	[number]	k4	503
m.	m.	k?	m.
Protéká	protékat	k5eAaImIp3nS	protékat
Blatskou	blatský	k2eAgFnSc7d1	Blatská
pánví	pánev	k1gFnSc7	pánev
<g/>
,	,	kIx,	,
do	do	k7c2	do
Blanice	Blanice	k1gFnSc2	Blanice
ústí	ústit	k5eAaImIp3nS	ústit
zprava	zprava	k6eAd1	zprava
u	u	k7c2	u
Milenovic	Milenovice	k1gFnPc2	Milenovice
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
386	[number]	k4	386
m.	m.	k?	m.
Jméno	jméno	k1gNnSc4	jméno
Radomilický	Radomilický	k2eAgInSc1d1	Radomilický
potok	potok	k1gInSc1	potok
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
až	až	k9	až
u	u	k7c2	u
Radomilic	Radomilice	k1gFnPc2	Radomilice
<g/>
,	,	kIx,	,
na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Bílý	bílý	k2eAgInSc1d1	bílý
potok	potok	k1gInSc1	potok
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgInSc1d1	bílý
potok	potok	k1gInSc1	potok
se	se	k3xPyFc4	se
východně	východně	k6eAd1	východně
od	od	k7c2	od
Záblatí	Záblatí	k1gNnSc2	Záblatí
rozvětvuje	rozvětvovat	k5eAaImIp3nS	rozvětvovat
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
jeho	jeho	k3xOp3gFnSc4	jeho
větev	větev	k1gFnSc1	větev
teče	téct	k5eAaImIp3nS	téct
na	na	k7c4	na
západ	západ	k1gInSc4	západ
jako	jako	k8xC	jako
Radomilický	Radomilický	k2eAgInSc1d1	Radomilický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
teče	téct	k5eAaImIp3nS	téct
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
a	a	k8xC	a
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
Bezdrevského	bezdrevský	k2eAgInSc2d1	bezdrevský
potoka	potok	k1gInSc2	potok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Radomilickém	Radomilický	k2eAgInSc6d1	Radomilický
potoce	potok	k1gInSc6	potok
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
řada	řada	k1gFnSc1	řada
rybníků	rybník	k1gInPc2	rybník
-	-	kIx~	-
např.	např.	kA	např.
Strpský	Strpský	k2eAgInSc1d1	Strpský
rybník	rybník	k1gInSc1	rybník
<g/>
,	,	kIx,	,
Skornice	Skornice	k1gFnSc1	Skornice
<g/>
,	,	kIx,	,
Mlýnský	mlýnský	k2eAgInSc1d1	mlýnský
rybník	rybník	k1gInSc1	rybník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vodní	vodní	k2eAgInSc1d1	vodní
režim	režim	k1gInSc1	režim
==	==	k?	==
</s>
</p>
<p>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
Radomilického	Radomilický	k2eAgInSc2d1	Radomilický
potoka	potok	k1gInSc2	potok
u	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
činí	činit	k5eAaImIp3nS	činit
0,15	[number]	k4	0,15
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
</s>
</p>
<p>
<s>
==	==	k?	==
Příroda	příroda	k1gFnSc1	příroda
==	==	k?	==
</s>
</p>
<p>
<s>
Poblíž	poblíž	k7c2	poblíž
Radomilic	Radomilice	k1gFnPc2	Radomilice
potok	potok	k1gInSc4	potok
napájí	napájet	k5eAaImIp3nS	napájet
mokřad	mokřad	k1gInSc1	mokřad
zvaný	zvaný	k2eAgInSc1d1	zvaný
Radomilická	Radomilický	k2eAgFnSc1d1	Radomilická
mokřina	mokřina	k1gFnSc1	mokřina
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
za	za	k7c4	za
přírodní	přírodní	k2eAgFnSc4d1	přírodní
rezervaci	rezervace	k1gFnSc4	rezervace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Radomilický	Radomilický	k2eAgInSc4d1	Radomilický
potok	potok	k1gInSc4	potok
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
