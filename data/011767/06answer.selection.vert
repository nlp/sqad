<s>
Radomilický	Radomilický	k2eAgInSc1d1	Radomilický
potok	potok	k1gInSc1	potok
je	být	k5eAaImIp3nS	být
pravostranný	pravostranný	k2eAgInSc1d1	pravostranný
přítok	přítok	k1gInSc1	přítok
řeky	řeka	k1gFnSc2	řeka
Blanice	Blanice	k1gFnSc2	Blanice
protékající	protékající	k2eAgInPc1d1	protékající
okresy	okres	k1gInPc1	okres
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
,	,	kIx,	,
Strakonice	Strakonice	k1gFnPc1	Strakonice
a	a	k8xC	a
Písek	Písek	k1gInSc1	Písek
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
