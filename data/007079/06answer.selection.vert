<s>
Čedič	čedič	k1gInSc1	čedič
neboli	neboli	k8xC	neboli
bazalt	bazalt	k1gInSc1	bazalt
(	(	kIx(	(
<g/>
starší	starý	k2eAgInPc1d2	starší
<g/>
,	,	kIx,	,
paleozoické	paleozoický	k2eAgInPc1d1	paleozoický
bazalty	bazalt	k1gInPc1	bazalt
se	se	k3xPyFc4	se
nesprávně	správně	k6eNd1	správně
nazývají	nazývat	k5eAaImIp3nP	nazývat
diabas	diabas	k1gInSc4	diabas
nebo	nebo	k8xC	nebo
melafyr	melafyr	k1gInSc4	melafyr
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
četná	četný	k2eAgFnSc1d1	četná
tmavá	tmavý	k2eAgFnSc1d1	tmavá
výlevná	výlevný	k2eAgFnSc1d1	výlevná
vyvřelá	vyvřelý	k2eAgFnSc1d1	vyvřelá
hornina	hornina	k1gFnSc1	hornina
s	s	k7c7	s
charakteristickou	charakteristický	k2eAgFnSc7d1	charakteristická
porfyrickou	porfyrický	k2eAgFnSc7d1	porfyrická
nebo	nebo	k8xC	nebo
sklovitou	sklovitý	k2eAgFnSc7d1	sklovitá
strukturou	struktura	k1gFnSc7	struktura
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
vyrostlicemi	vyrostlice	k1gFnPc7	vyrostlice
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
minerálů	minerál	k1gInPc2	minerál
<g/>
.	.	kIx.	.
</s>
