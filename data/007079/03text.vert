<s>
Čedič	čedič	k1gInSc1	čedič
neboli	neboli	k8xC	neboli
bazalt	bazalt	k1gInSc1	bazalt
(	(	kIx(	(
<g/>
starší	starý	k2eAgInPc1d2	starší
<g/>
,	,	kIx,	,
paleozoické	paleozoický	k2eAgInPc1d1	paleozoický
bazalty	bazalt	k1gInPc1	bazalt
se	se	k3xPyFc4	se
nesprávně	správně	k6eNd1	správně
nazývají	nazývat	k5eAaImIp3nP	nazývat
diabas	diabas	k1gInSc4	diabas
nebo	nebo	k8xC	nebo
melafyr	melafyr	k1gInSc4	melafyr
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
četná	četný	k2eAgFnSc1d1	četná
tmavá	tmavý	k2eAgFnSc1d1	tmavá
výlevná	výlevný	k2eAgFnSc1d1	výlevná
vyvřelá	vyvřelý	k2eAgFnSc1d1	vyvřelá
hornina	hornina	k1gFnSc1	hornina
s	s	k7c7	s
charakteristickou	charakteristický	k2eAgFnSc7d1	charakteristická
porfyrickou	porfyrický	k2eAgFnSc7d1	porfyrická
nebo	nebo	k8xC	nebo
sklovitou	sklovitý	k2eAgFnSc7d1	sklovitá
strukturou	struktura	k1gFnSc7	struktura
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
vyrostlicemi	vyrostlice	k1gFnPc7	vyrostlice
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
minerálů	minerál	k1gInPc2	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
škváry	škvára	k1gFnSc2	škvára
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
viditelných	viditelný	k2eAgInPc2d1	viditelný
krystalů	krystal	k1gInPc2	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
má	mít	k5eAaImIp3nS	mít
šedou	šedý	k2eAgFnSc4d1	šedá
či	či	k8xC	či
černou	černý	k2eAgFnSc4d1	černá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
zvětralý	zvětralý	k2eAgInSc4d1	zvětralý
čedič	čedič	k1gInSc4	čedič
má	mít	k5eAaImIp3nS	mít
barvu	barva	k1gFnSc4	barva
spíše	spíše	k9	spíše
šedou	šedý	k2eAgFnSc4d1	šedá
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
čedič	čedič	k1gInSc1	čedič
(	(	kIx(	(
<g/>
bazalt	bazalt	k1gInSc1	bazalt
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
jemně	jemně	k6eAd1	jemně
zrnitých	zrnitý	k2eAgFnPc2d1	zrnitá
extruzivních	extruzivní	k2eAgFnPc2d1	extruzivní
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
mělce	mělce	k6eAd1	mělce
uložených	uložený	k2eAgFnPc2d1	uložená
intruzivních	intruzivní	k2eAgFnPc2d1	intruzivní
hornin	hornina	k1gFnPc2	hornina
<g/>
,	,	kIx,	,
hrubě	hrubě	k6eAd1	hrubě
zrnité	zrnitý	k2eAgFnSc2d1	zrnitá
hlubinné	hlubinný	k2eAgFnSc2d1	hlubinná
horniny	hornina	k1gFnSc2	hornina
daného	daný	k2eAgNnSc2d1	dané
složení	složení	k1gNnSc2	složení
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
názvy	název	k1gInPc1	název
dolerit	dolerit	k1gInSc1	dolerit
a	a	k8xC	a
gabro	gabro	k1gNnSc1	gabro
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
bazalt	bazalt	k1gInSc1	bazalt
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
používaný	používaný	k2eAgInSc1d1	používaný
už	už	k9	už
ve	v	k7c6	v
starém	starý	k2eAgNnSc6d1	staré
Řecku	Řecko	k1gNnSc6	Řecko
a	a	k8xC	a
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
zavedl	zavést	k5eAaPmAgMnS	zavést
v	v	k7c6	v
současném	současný	k2eAgNnSc6d1	současné
pojetí	pojetí	k1gNnSc6	pojetí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1546	[number]	k4	1546
Georgius	Georgius	k1gInSc1	Georgius
Agricola	Agricola	k1gFnSc1	Agricola
<g/>
..	..	k?	..
Starověké	starověký	k2eAgNnSc1d1	starověké
použití	použití	k1gNnSc1	použití
termínu	termín	k1gInSc2	termín
je	být	k5eAaImIp3nS	být
připisováno	připisovat	k5eAaImNgNnS	připisovat
římskému	římský	k2eAgMnSc3d1	římský
přírodovědci	přírodovědec	k1gMnSc3	přírodovědec
Pliniovi	Plinius	k1gMnSc3	Plinius
staršímu	starší	k1gMnSc3	starší
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
čedič	čedič	k1gInSc4	čedič
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
jemnozrnná	jemnozrnný	k2eAgFnSc1d1	jemnozrnná
stavba	stavba	k1gFnSc1	stavba
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
rychlým	rychlý	k2eAgNnSc7d1	rychlé
utuhnutím	utuhnutí	k1gNnSc7	utuhnutí
lávy	láva	k1gFnSc2	láva
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
stavbě	stavba	k1gFnSc6	stavba
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
často	často	k6eAd1	často
velké	velký	k2eAgInPc1d1	velký
krystaly	krystal	k1gInPc1	krystal
minerálů	minerál	k1gInPc2	minerál
či	či	k8xC	či
vesikule	vesikule	k1gFnPc1	vesikule
<g/>
,	,	kIx,	,
drobné	drobný	k2eAgFnPc1d1	drobná
bublinky	bublinka	k1gFnPc1	bublinka
vyplněné	vyplněný	k2eAgFnPc1d1	vyplněná
plynem	plyn	k1gInSc7	plyn
či	či	k8xC	či
druhotnou	druhotný	k2eAgFnSc7d1	druhotná
mineralizací	mineralizace	k1gFnSc7	mineralizace
<g/>
,	,	kIx,	,
a	a	k8xC	a
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
strusky	struska	k1gFnSc2	struska
<g/>
.	.	kIx.	.
</s>
<s>
Odlučnost	odlučnost	k1gFnSc1	odlučnost
čediče	čedič	k1gInSc2	čedič
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
sloupcovitá	sloupcovitý	k2eAgFnSc1d1	sloupcovitá
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
chladnutí	chladnutí	k1gNnSc6	chladnutí
magmatu	magma	k1gNnSc2	magma
a	a	k8xC	a
pozdějším	pozdní	k2eAgNnSc6d2	pozdější
zvětrávání	zvětrávání	k1gNnSc6	zvětrávání
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
pěti	pět	k4xCc2	pět
<g/>
,	,	kIx,	,
šesti	šest	k4xCc2	šest
či	či	k8xC	či
sedmiúhelníkové	sedmiúhelníkový	k2eAgInPc4d1	sedmiúhelníkový
bloky	blok	k1gInPc4	blok
<g/>
.	.	kIx.	.
</s>
<s>
Textura	textura	k1gFnSc1	textura
bývá	bývat	k5eAaImIp3nS	bývat
proudovitá	proudovitý	k2eAgFnSc1d1	proudovitý
nebo	nebo	k8xC	nebo
všesměrná	všesměrný	k2eAgFnSc1d1	všesměrná
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
oficiální	oficiální	k2eAgFnSc2d1	oficiální
definice	definice	k1gFnSc2	definice
vycházející	vycházející	k2eAgFnSc2d1	vycházející
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
čediče	čedič	k1gInSc2	čedič
v	v	k7c6	v
diagramu	diagram	k1gInSc6	diagram
QAPF	QAPF	kA	QAPF
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
hornina	hornina	k1gFnSc1	hornina
jemnozrnná	jemnozrnný	k2eAgFnSc1d1	jemnozrnná
vyvřelá	vyvřelý	k2eAgFnSc1d1	vyvřelá
hornina	hornina	k1gFnSc1	hornina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
méně	málo	k6eAd2	málo
než	než	k8xS	než
20	[number]	k4	20
objemových	objemový	k2eAgNnPc2d1	objemové
procent	procento	k1gNnPc2	procento
křemene	křemen	k1gInSc2	křemen
a	a	k8xC	a
méně	málo	k6eAd2	málo
než	než	k8xS	než
10	[number]	k4	10
%	%	kIx~	%
foidů	foid	k1gInPc2	foid
a	a	k8xC	a
minimálně	minimálně	k6eAd1	minimálně
65	[number]	k4	65
%	%	kIx~	%
živců	živec	k1gInPc2	živec
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
plagioklasu	plagioklas	k1gInSc2	plagioklas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
vzniká	vznikat	k5eAaImIp3nS	vznikat
většina	většina	k1gFnSc1	většina
čedičových	čedičový	k2eAgNnPc2d1	čedičové
magmat	magma	k1gNnPc2	magma
dekompresním	dekompresní	k2eAgNnSc7d1	dekompresní
tavením	tavení	k1gNnSc7	tavení
pláště	plášť	k1gInSc2	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
Země	zem	k1gFnPc1	zem
jsou	být	k5eAaImIp3nP	být
čediče	čedič	k1gInPc4	čedič
známé	známý	k2eAgFnSc2d1	známá
taktéž	taktéž	k?	taktéž
z	z	k7c2	z
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
Marsu	Mars	k1gInSc2	Mars
<g/>
,	,	kIx,	,
Venuše	Venuše	k1gFnSc2	Venuše
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
z	z	k7c2	z
asteroidu	asteroid	k1gInSc2	asteroid
Vesta	vesta	k1gFnSc1	vesta
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
zdrojovými	zdrojový	k2eAgFnPc7d1	zdrojová
horninami	hornina	k1gFnPc7	hornina
pro	pro	k7c4	pro
částečné	částečný	k2eAgNnSc4d1	částečné
tavení	tavení	k1gNnSc4	tavení
jsou	být	k5eAaImIp3nP	být
jak	jak	k6eAd1	jak
peridotity	peridotit	k1gInPc1	peridotit
tak	tak	k9	tak
i	i	k9	i
pyroxenity	pyroxenit	k1gInPc1	pyroxenit
<g/>
..	..	k?	..
Čediče	čedič	k1gInPc1	čedič
tvoří	tvořit	k5eAaImIp3nP	tvořit
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
oceánskou	oceánský	k2eAgFnSc4d1	oceánská
kůru	kůra	k1gFnSc4	kůra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
na	na	k7c6	na
středooceánských	středooceánský	k2eAgInPc6d1	středooceánský
hřbetech	hřbet	k1gInPc6	hřbet
výstupem	výstup	k1gInSc7	výstup
(	(	kIx(	(
<g/>
upwelling	upwelling	k1gInSc1	upwelling
<g/>
)	)	kIx)	)
plášťového	plášťový	k2eAgInSc2d1	plášťový
materiálu	materiál	k1gInSc2	materiál
z	z	k7c2	z
hlavní	hlavní	k2eAgFnSc2d1	hlavní
části	část	k1gFnSc2	část
tvoří	tvořit	k5eAaImIp3nP	tvořit
<g/>
.	.	kIx.	.
</s>
<s>
Čedič	čedič	k1gInSc1	čedič
<g/>
,	,	kIx,	,
lidově	lidově	k6eAd1	lidově
"	"	kIx"	"
<g/>
čadič	čadič	k1gInSc1	čadič
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
nářeční	nářeční	k2eAgInSc1d1	nářeční
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
očazený	očazený	k2eAgInSc4d1	očazený
černý	černý	k2eAgInSc4d1	černý
kámen	kámen	k1gInSc4	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
národním	národní	k2eAgNnSc6d1	národní
obrození	obrození	k1gNnSc6	obrození
přijat	přijat	k2eAgMnSc1d1	přijat
jako	jako	k8xC	jako
termín	termín	k1gInSc1	termín
pro	pro	k7c4	pro
bazalt	bazalt	k1gInSc4	bazalt
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
složkou	složka	k1gFnSc7	složka
bazaltů	bazalt	k1gInPc2	bazalt
jsou	být	k5eAaImIp3nP	být
zásadité	zásaditý	k2eAgFnPc1d1	zásaditá
(	(	kIx(	(
<g/>
sodno-vápenaté	sodnoápenatý	k2eAgInPc4d1	sodno-vápenatý
<g/>
)	)	kIx)	)
živce	živec	k1gInPc4	živec
-	-	kIx~	-
plagioklasy	plagioklas	k1gInPc4	plagioklas
(	(	kIx(	(
<g/>
labradorit	labradorit	k1gInSc4	labradorit
<g/>
,	,	kIx,	,
anortit	anortit	k5eAaPmF	anortit
až	až	k8xS	až
bytownit	bytownit	k5eAaPmF	bytownit
<g/>
)	)	kIx)	)
a	a	k8xC	a
pyroxeny	pyroxen	k1gInPc4	pyroxen
(	(	kIx(	(
<g/>
rombické	rombický	k2eAgFnPc4d1	rombická
i	i	k8xC	i
monoklinické	monoklinický	k2eAgFnPc4d1	monoklinická
<g/>
,	,	kIx,	,
augit	augit	k1gInSc1	augit
<g/>
,	,	kIx,	,
titanaugit	titanaugit	k1gInSc1	titanaugit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
značném	značný	k2eAgNnSc6d1	značné
množství	množství	k1gNnSc6	množství
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
i	i	k8xC	i
ilmenit	ilmenit	k1gInSc1	ilmenit
a	a	k8xC	a
magnetit	magnetit	k1gInSc1	magnetit
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
některé	některý	k3yIgInPc4	některý
typy	typ	k1gInPc4	typ
bazaltů	bazalt	k1gInPc2	bazalt
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgInPc1d1	typický
vyšší	vysoký	k2eAgInPc1d2	vyšší
obsahy	obsah	k1gInPc1	obsah
minerálů	minerál	k1gInPc2	minerál
skupiny	skupina	k1gFnSc2	skupina
olivínu	olivín	k1gInSc2	olivín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
meziprostorech	meziprostor	k1gInPc6	meziprostor
mezi	mezi	k7c4	mezi
minerály	minerál	k1gInPc4	minerál
je	být	k5eAaImIp3nS	být
vulkanické	vulkanický	k2eAgNnSc1d1	vulkanické
sklo	sklo	k1gNnSc1	sklo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
druhá	druhý	k4xOgFnSc1	druhý
generace	generace	k1gFnSc1	generace
stejných	stejný	k2eAgInPc2d1	stejný
minerálů	minerál	k1gInPc2	minerál
v	v	k7c6	v
drobnozrnném	drobnozrnný	k2eAgInSc6d1	drobnozrnný
vývoji	vývoj	k1gInSc6	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
typu	typ	k1gInSc6	typ
erupce	erupce	k1gFnSc2	erupce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c6	na
vlastnostech	vlastnost	k1gFnPc6	vlastnost
magmatu	magma	k1gNnSc2	magma
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
bazaltové	bazaltový	k2eAgFnPc1d1	bazaltová
lávy	láva	k1gFnPc1	láva
různé	různý	k2eAgFnPc4d1	různá
formy	forma	k1gFnPc4	forma
<g/>
:	:	kIx,	:
lávové	lávový	k2eAgFnPc4d1	lávová
prody	proda	k1gFnPc4	proda
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
strukturou	struktura	k1gFnSc7	struktura
-	-	kIx~	-
pahoehoe	pahoehoe	k1gFnSc7	pahoehoe
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Aa	Aa	k1gFnSc1	Aa
lávy	láva	k1gFnSc2	láva
(	(	kIx(	(
<g/>
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
viskozitě	viskozita	k1gFnSc6	viskozita
<g/>
)	)	kIx)	)
vzácně	vzácně	k6eAd1	vzácně
tvoří	tvořit	k5eAaImIp3nP	tvořit
i	i	k9	i
různé	různý	k2eAgInPc1d1	různý
vulkanoklasty	vulkanoklast	k1gInPc1	vulkanoklast
a	a	k8xC	a
tufy	tuf	k1gInPc1	tuf
podmořskými	podmořský	k2eAgFnPc7d1	podmořská
erupcemi	erupce	k1gFnPc7	erupce
vznikají	vznikat	k5eAaImIp3nP	vznikat
polštářové	polštářový	k2eAgNnSc1d1	polštářové
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
pillow	pillow	k?	pillow
<g/>
)	)	kIx)	)
lávy	láva	k1gFnSc2	láva
<g/>
.	.	kIx.	.
</s>
<s>
Tholeitické	Tholeitický	k2eAgInPc1d1	Tholeitický
bazalty	bazalt	k1gInPc1	bazalt
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
typem	typ	k1gInSc7	typ
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
spadají	spadat	k5eAaPmIp3nP	spadat
všechny	všechen	k3xTgInPc4	všechen
bazalty	bazalt	k1gInPc4	bazalt
oceánského	oceánský	k2eAgNnSc2d1	oceánské
dna	dno	k1gNnSc2	dno
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
mid	mid	k?	mid
ocean	ocean	k1gInSc1	ocean
ridge	ridge	k1gInSc1	ridge
basalt	basalt	k1gInSc1	basalt
-	-	kIx~	-
MORB	MORB	kA	MORB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velkých	velký	k2eAgInPc2d1	velký
vulkanických	vulkanický	k2eAgInPc2d1	vulkanický
ostrovů	ostrov	k1gInPc2	ostrov
i	i	k8xC	i
kontinentálních	kontinentální	k2eAgInPc2d1	kontinentální
bazaltů	bazalt	k1gInPc2	bazalt
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristický	charakteristický	k2eAgMnSc1d1	charakteristický
je	být	k5eAaImIp3nS	být
nízký	nízký	k2eAgInSc1d1	nízký
obsah	obsah	k1gInSc1	obsah
sodíku	sodík	k1gInSc2	sodík
a	a	k8xC	a
průměrný	průměrný	k2eAgInSc4d1	průměrný
obsah	obsah	k1gInSc4	obsah
křemíku	křemík	k1gInSc2	křemík
<g/>
.	.	kIx.	.
</s>
<s>
Mineralogicky	mineralogicky	k6eAd1	mineralogicky
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
typ	typ	k1gInSc4	typ
charakterizován	charakterizován	k2eAgInSc4d1	charakterizován
obsahem	obsah	k1gInSc7	obsah
pyroxenů	pyroxen	k1gInPc2	pyroxen
(	(	kIx(	(
<g/>
augit	augit	k1gInSc1	augit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plagioklasu	plagioklas	k1gInSc2	plagioklas
a	a	k8xC	a
magnetitu	magnetit	k1gInSc2	magnetit
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
akcesorie	akcesorie	k1gFnSc1	akcesorie
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
oxid	oxid	k1gInSc1	oxid
křemičitý	křemičitý	k2eAgInSc1d1	křemičitý
(	(	kIx(	(
<g/>
křemen	křemen	k1gInSc1	křemen
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
tridymit	tridymit	k1gInSc1	tridymit
<g/>
)	)	kIx)	)
a	a	k8xC	a
olivín	olivín	k1gInSc4	olivín
<g/>
.	.	kIx.	.
</s>
<s>
Olivinické	olivinický	k2eAgFnPc1d1	olivinický
tholeity	tholeita	k1gFnPc1	tholeita
jsou	být	k5eAaImIp3nP	být
dost	dost	k6eAd1	dost
podobné	podobný	k2eAgFnPc1d1	podobná
<g/>
,	,	kIx,	,
jen	jen	k9	jen
mají	mít	k5eAaImIp3nP	mít
zvýšený	zvýšený	k2eAgInSc4d1	zvýšený
obsah	obsah	k1gInSc4	obsah
olivínu	olivín	k1gInSc2	olivín
<g/>
.	.	kIx.	.
</s>
<s>
Vysoce	vysoce	k6eAd1	vysoce
hlinité	hlinitý	k2eAgInPc1d1	hlinitý
bazalty	bazalt	k1gInPc1	bazalt
-	-	kIx~	-
mají	mít	k5eAaImIp3nP	mít
zvýšený	zvýšený	k2eAgInSc4d1	zvýšený
obsah	obsah	k1gInSc4	obsah
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
až	až	k9	až
17	[number]	k4	17
%	%	kIx~	%
a	a	k8xC	a
nižší	nízký	k2eAgInSc1d2	nižší
obsah	obsah	k1gInSc1	obsah
TiO	TiO	k1gFnSc2	TiO
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1	alkalický
bazalty	bazalt	k1gInPc1	bazalt
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
produktem	produkt	k1gInSc7	produkt
vulkanismu	vulkanismus	k1gInSc2	vulkanismus
divergentních	divergentní	k2eAgInPc2d1	divergentní
deskových	deskový	k2eAgInPc2d1	deskový
okrajů	okraj	k1gInPc2	okraj
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
vyšší	vysoký	k2eAgInSc4d2	vyšší
obsah	obsah	k1gInSc4	obsah
foidů	foid	k1gInPc2	foid
a	a	k8xC	a
flogopitu	flogopit	k1gInSc2	flogopit
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
bazaltového	bazaltový	k2eAgNnSc2d1	bazaltové
magmatu	magma	k1gNnSc2	magma
je	být	k5eAaImIp3nS	být
okolo	okolo	k7c2	okolo
1	[number]	k4	1
100	[number]	k4	100
až	až	k9	až
1	[number]	k4	1
250	[number]	k4	250
°	°	k?	°
<g/>
C.	C.	kA	C.
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
díky	díky	k7c3	díky
nízkému	nízký	k2eAgInSc3d1	nízký
obsahu	obsah	k1gInSc3	obsah
rozpuštěných	rozpuštěný	k2eAgInPc2d1	rozpuštěný
plynů	plyn	k1gInPc2	plyn
je	být	k5eAaImIp3nS	být
magma	magma	k1gNnSc1	magma
značně	značně	k6eAd1	značně
pohyblivé	pohyblivý	k2eAgFnSc2d1	pohyblivá
(	(	kIx(	(
<g/>
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
až	až	k9	až
20	[number]	k4	20
km	km	kA	km
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
lávové	lávový	k2eAgInPc4d1	lávový
proudy	proud	k1gInPc4	proud
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
erupce	erupce	k1gFnPc1	erupce
bývají	bývat	k5eAaImIp3nP	bývat
zpravidla	zpravidla	k6eAd1	zpravidla
klidné	klidný	k2eAgFnPc1d1	klidná
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
větších	veliký	k2eAgFnPc2d2	veliký
explozí	exploze	k1gFnPc2	exploze
<g/>
.	.	kIx.	.
</s>
<s>
Bazalty	bazalt	k1gInPc1	bazalt
jsou	být	k5eAaImIp3nP	být
pevnější	pevný	k2eAgInPc1d2	pevnější
než	než	k8xS	než
granitoidy	granitoid	k1gInPc1	granitoid
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
třetinu	třetina	k1gFnSc4	třetina
(	(	kIx(	(
<g/>
pevnost	pevnost	k1gFnSc4	pevnost
v	v	k7c6	v
tlaku	tlak	k1gInSc6	tlak
za	za	k7c2	za
sucha	sucho	k1gNnSc2	sucho
180	[number]	k4	180
až	až	k9	až
380	[number]	k4	380
MPa	MPa	k1gFnPc2	MPa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
hustota	hustota	k1gFnSc1	hustota
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
2,95	[number]	k4	2,95
do	do	k7c2	do
3,15	[number]	k4	3,15
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm3	cm3	k4	cm3
Bazalty	bazalt	k1gInPc7	bazalt
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
porovnaní	porovnaný	k2eAgMnPc1d1	porovnaný
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
běžnými	běžný	k2eAgFnPc7d1	běžná
horninami	hornina	k1gFnPc7	hornina
malý	malý	k2eAgInSc4d1	malý
obsah	obsah	k1gInSc4	obsah
oxidu	oxid	k1gInSc2	oxid
křemičitého	křemičitý	k2eAgInSc2d1	křemičitý
(	(	kIx(	(
<g/>
SiO	SiO	k1gFnSc1	SiO
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
-	-	kIx~	-
od	od	k7c2	od
48	[number]	k4	48
do	do	k7c2	do
52	[number]	k4	52
hm	hm	k?	hm
<g/>
.	.	kIx.	.
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
převážně	převážně	k6eAd1	převážně
oxidem	oxid	k1gInSc7	oxid
hořečnatým	hořečnatý	k2eAgInSc7d1	hořečnatý
(	(	kIx(	(
<g/>
MgO	MgO	k1gMnSc6	MgO
-	-	kIx~	-
5	[number]	k4	5
až	až	k9	až
12	[number]	k4	12
hm	hm	k?	hm
<g/>
.	.	kIx.	.
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oxidem	oxid	k1gInSc7	oxid
vápenatým	vápenatý	k2eAgInSc7d1	vápenatý
(	(	kIx(	(
<g/>
CaO	CaO	k1gMnPc2	CaO
-	-	kIx~	-
okolo	okolo	k7c2	okolo
10	[number]	k4	10
hm	hm	k?	hm
<g/>
.	.	kIx.	.
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oxidy	oxid	k1gInPc1	oxid
železa	železo	k1gNnSc2	železo
(	(	kIx(	(
<g/>
FeO	FeO	k1gFnSc1	FeO
a	a	k8xC	a
Fe	Fe	k1gFnSc1	Fe
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
-	-	kIx~	-
5	[number]	k4	5
až	až	k9	až
14	[number]	k4	14
hm	hm	k?	hm
<g/>
.	.	kIx.	.
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
oxidem	oxid	k1gInSc7	oxid
hlinitým	hlinitý	k2eAgInSc7d1	hlinitý
(	(	kIx(	(
<g/>
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
-	-	kIx~	-
více	hodně	k6eAd2	hodně
než	než	k8xS	než
14	[number]	k4	14
hm	hm	k?	hm
<g/>
.	.	kIx.	.
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
světlejším	světlý	k2eAgFnPc3d2	světlejší
horninám	hornina	k1gFnPc3	hornina
mají	mít	k5eAaImIp3nP	mít
menší	malý	k2eAgInSc4d2	menší
obsah	obsah	k1gInSc4	obsah
CaO	CaO	k1gFnSc2	CaO
<g/>
,	,	kIx,	,
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
K	K	kA	K
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
Asi	asi	k9	asi
2	[number]	k4	2
-	-	kIx~	-
6	[number]	k4	6
hmotnostních	hmotnostní	k2eAgFnPc2d1	hmotnostní
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nS	tvořit
alkálie	alkálie	k1gFnSc1	alkálie
<g/>
,	,	kIx,	,
0,5	[number]	k4	0,5
<g/>
-	-	kIx~	-
<g/>
2,0	[number]	k4	2,0
%	%	kIx~	%
TiO	TiO	k1gFnSc2	TiO
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1	alkalický
bazalty	bazalt	k1gInPc1	bazalt
mají	mít	k5eAaImIp3nP	mít
obsah	obsah	k1gInSc4	obsah
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
3	[number]	k4	3
od	od	k7c2	od
17	[number]	k4	17
do	do	k7c2	do
19	[number]	k4	19
%	%	kIx~	%
<g/>
;	;	kIx,	;
boninity	boninita	k1gFnPc1	boninita
mají	mít	k5eAaImIp3nP	mít
obsah	obsah	k1gInSc4	obsah
MgO	MgO	k1gFnSc2	MgO
do	do	k7c2	do
15	[number]	k4	15
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
foidy	foida	k1gFnPc4	foida
bohaté	bohatý	k2eAgFnSc2d1	bohatá
mafické	mafický	k2eAgFnSc2d1	mafický
horniny	hornina	k1gFnSc2	hornina
až	až	k9	až
alkalické	alkalický	k2eAgInPc1d1	alkalický
bazalty	bazalt	k1gInPc1	bazalt
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
obsah	obsah	k1gInSc4	obsah
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
a	a	k8xC	a
K2O	K2O	k1gFnSc1	K2O
aj	aj	kA	aj
12	[number]	k4	12
<g/>
%	%	kIx~	%
a	a	k8xC	a
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
MORB	MORB	kA	MORB
bazalty	bazalt	k1gInPc4	bazalt
považovány	považován	k2eAgInPc4d1	považován
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
klíčových	klíčový	k2eAgInPc2d1	klíčový
faktorů	faktor	k1gInPc2	faktor
chápání	chápání	k1gNnSc2	chápání
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc4	jejich
chemické	chemický	k2eAgNnSc1d1	chemické
složení	složení	k1gNnSc1	složení
bylo	být	k5eAaImAgNnS	být
poměrně	poměrně	k6eAd1	poměrně
podrobně	podrobně	k6eAd1	podrobně
prozkoumáno	prozkoumán	k2eAgNnSc1d1	prozkoumáno
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc4	složení
MORB	MORB	kA	MORB
bazaltů	bazalt	k1gInPc2	bazalt
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
odlišitelné	odlišitelný	k2eAgNnSc1d1	odlišitelné
od	od	k7c2	od
bazaltů	bazalt	k1gInPc2	bazalt
vznikajících	vznikající	k2eAgInPc2d1	vznikající
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
prostředích	prostředí	k1gNnPc6	prostředí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
netvoří	tvořit	k5eNaImIp3nS	tvořit
homogenní	homogenní	k2eAgFnSc4d1	homogenní
skupinu	skupina	k1gFnSc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
složení	složení	k1gNnSc1	složení
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jejich	jejich	k3xOp3gNnSc2	jejich
umístění	umístění	k1gNnSc2	umístění
v	v	k7c6	v
středooceánském	středooceánský	k2eAgInSc6d1	středooceánský
hřbetu	hřbet	k1gInSc6	hřbet
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
závislé	závislý	k2eAgNnSc1d1	závislé
i	i	k9	i
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
lokalitách	lokalita	k1gFnPc6	lokalita
a	a	k8xC	a
oceánských	oceánský	k2eAgInPc6d1	oceánský
bazénech	bazén	k1gInPc6	bazén
<g/>
.	.	kIx.	.
</s>
<s>
MORB	MORB	kA	MORB
bazalty	bazalt	k1gInPc4	bazalt
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
intruzivní	intruzivní	k2eAgInPc1d1	intruzivní
ekvivalenty	ekvivalent	k1gInPc1	ekvivalent
<g/>
,	,	kIx,	,
gabra	gabro	k1gNnPc1	gabro
jsou	být	k5eAaImIp3nP	být
typickými	typický	k2eAgFnPc7d1	typická
horninami	hornina	k1gFnPc7	hornina
středooceánských	středooceánský	k2eAgMnPc2d1	středooceánský
hřbetů	hřbet	k1gMnPc2	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Josu	Josu	k6eAd1	Josu
též	též	k9	též
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k8xS	jako
tholeitické	tholeitický	k2eAgInPc1d1	tholeitický
bazalty	bazalt	k1gInPc1	bazalt
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
poměrně	poměrně	k6eAd1	poměrně
nízký	nízký	k2eAgInSc4d1	nízký
obsah	obsah	k1gInSc4	obsah
alkálií	alkálie	k1gFnPc2	alkálie
a	a	k8xC	a
nekompatibilních	kompatibilní	k2eNgInPc2d1	nekompatibilní
stopových	stopový	k2eAgInPc2d1	stopový
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
MORB	MORB	kA	MORB
bazalty	bazalt	k1gInPc1	bazalt
také	také	k9	také
mají	mít	k5eAaImIp3nP	mít
poměrně	poměrně	k6eAd1	poměrně
vyrovnanou	vyrovnaný	k2eAgFnSc4d1	vyrovnaná
křivku	křivka	k1gFnSc4	křivka
obsahu	obsah	k1gInSc2	obsah
kovů	kov	k1gInPc2	kov
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
(	(	kIx(	(
<g/>
REE	Rea	k1gFnSc6	Rea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
normalizovanou	normalizovaný	k2eAgFnSc4d1	normalizovaná
na	na	k7c4	na
plášťové	plášťový	k2eAgFnPc4d1	plášťová
chondritické	chondritický	k2eAgFnPc4d1	chondritický
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
alkalické	alkalický	k2eAgInPc1d1	alkalický
bazalty	bazalt	k1gInPc1	bazalt
mají	mít	k5eAaImIp3nP	mít
normalizované	normalizovaný	k2eAgFnPc1d1	normalizovaná
hodnoty	hodnota	k1gFnPc1	hodnota
obohaceny	obohacen	k2eAgFnPc1d1	obohacena
o	o	k7c4	o
lehké	lehký	k2eAgNnSc4d1	lehké
prvky	prvek	k1gInPc1	prvek
REE	Rea	k1gFnSc3	Rea
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
celkově	celkově	k6eAd1	celkově
vyšší	vysoký	k2eAgInSc4d2	vyšší
obsah	obsah	k1gInSc4	obsah
REE	Rea	k1gFnSc3	Rea
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
nekompatibilních	kompatibilní	k2eNgInPc2d1	nekompatibilní
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Místa	místo	k1gNnPc1	místo
výskytu	výskyt	k1gInSc2	výskyt
bazaltových	bazaltový	k2eAgNnPc2d1	bazaltové
magmat	magma	k1gNnPc2	magma
jsou	být	k5eAaImIp3nP	být
různá	různý	k2eAgNnPc1d1	různé
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
riftová	riftový	k2eAgNnPc4d1	riftový
údolí	údolí	k1gNnPc4	údolí
(	(	kIx(	(
<g/>
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
středooceánských	středooceánský	k2eAgInPc6d1	středooceánský
hřbetech	hřbet	k1gInPc6	hřbet
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c6	na
kontinentech	kontinent	k1gInPc6	kontinent
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
horké	horký	k2eAgFnSc2d1	horká
skvrny	skvrna	k1gFnSc2	skvrna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bazaltový	bazaltový	k2eAgInSc1d1	bazaltový
vulkanismus	vulkanismus	k1gInSc1	vulkanismus
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
v	v	k7c6	v
konvergentních	konvergentní	k2eAgInPc6d1	konvergentní
okrajích	okraj	k1gInPc6	okraj
střetávajících	střetávající	k2eAgFnPc2d1	střetávající
se	se	k3xPyFc4	se
tektonických	tektonický	k2eAgFnPc2d1	tektonická
desek	deska	k1gFnPc2	deska
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
obě	dva	k4xCgFnPc1	dva
desky	deska	k1gFnPc1	deska
tvořeny	tvořit	k5eAaImNgFnP	tvořit
oceánskou	oceánský	k2eAgFnSc7d1	oceánská
kůrou	kůra	k1gFnSc7	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Čedič	čedič	k1gInSc1	čedič
je	být	k5eAaImIp3nS	být
nejhojnější	hojný	k2eAgFnSc1d3	nejhojnější
magmatická	magmatický	k2eAgFnSc1d1	magmatická
hornina	hornina	k1gFnSc1	hornina
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
prakticky	prakticky	k6eAd1	prakticky
celé	celý	k2eAgNnSc4d1	celé
oceánské	oceánský	k2eAgNnSc4d1	oceánské
dno	dno	k1gNnSc4	dno
a	a	k8xC	a
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
zemskou	zemský	k2eAgFnSc4d1	zemská
kůru	kůra	k1gFnSc4	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
obrovské	obrovský	k2eAgInPc1d1	obrovský
výlevy	výlev	k1gInPc1	výlev
bazaltů	bazalt	k1gInPc2	bazalt
z	z	k7c2	z
triasu	trias	k1gInSc2	trias
<g/>
:	:	kIx,	:
Dekánská	Dekánský	k2eAgFnSc1d1	Dekánská
plošina	plošina	k1gFnSc1	plošina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
Sibiřský	sibiřský	k2eAgInSc4d1	sibiřský
trap	trap	k1gInSc4	trap
-	-	kIx~	-
obrovské	obrovský	k2eAgFnPc1d1	obrovská
plošiny	plošina	k1gFnPc1	plošina
tvořené	tvořený	k2eAgFnPc1d1	tvořená
bazalty	bazalt	k1gInPc4	bazalt
<g/>
,	,	kIx,	,
vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
jen	jen	k6eAd1	jen
několika	několik	k4yIc2	několik
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
v	v	k7c6	v
geologickém	geologický	k2eAgNnSc6d1	geologické
měřítku	měřítko	k1gNnSc6	měřítko
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
enormní	enormní	k2eAgFnSc4d1	enormní
vulkanickou	vulkanický	k2eAgFnSc4d1	vulkanická
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
příkladem	příklad	k1gInSc7	příklad
plošinových	plošinový	k2eAgInPc2d1	plošinový
kontinentálních	kontinentální	k2eAgInPc2d1	kontinentální
bazaltů	bazalt	k1gInPc2	bazalt
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
řeky	řeka	k1gFnSc2	řeka
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgFnSc1d1	podobná
vulkanická	vulkanický	k2eAgFnSc1d1	vulkanická
aktivita	aktivita	k1gFnSc1	aktivita
byla	být	k5eAaImAgFnS	být
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
jury	jura	k1gFnSc2	jura
kolem	kolem	k7c2	kolem
nově	nově	k6eAd1	nově
vznikajícího	vznikající	k2eAgInSc2d1	vznikající
Atlantiku	Atlantik	k1gInSc2	Atlantik
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
velká	velký	k2eAgFnSc1d1	velká
akumulace	akumulace	k1gFnSc1	akumulace
bazaltů	bazalt	k1gInPc2	bazalt
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
vulkanicky	vulkanicky	k6eAd1	vulkanicky
aktivní	aktivní	k2eAgMnSc1d1	aktivní
i	i	k8xC	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
Havajských	havajský	k2eAgInPc6d1	havajský
ostrovech	ostrov	k1gInPc6	ostrov
a	a	k8xC	a
také	také	k9	také
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
<g/>
.	.	kIx.	.
</s>
<s>
Bazaltové	bazaltový	k2eAgFnPc1d1	bazaltová
horniny	hornina	k1gFnPc1	hornina
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
i	i	k9	i
z	z	k7c2	z
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
typy	typ	k1gInPc1	typ
bazaltů	bazalt	k1gInPc2	bazalt
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
hlavní	hlavní	k2eAgFnSc7d1	hlavní
horninou	hornina	k1gFnSc7	hornina
tvořící	tvořící	k2eAgInPc1d1	tvořící
povrchové	povrchový	k2eAgInPc1d1	povrchový
útvary	útvar	k1gInPc1	útvar
na	na	k7c4	na
Venuši	Venuše	k1gFnSc4	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Bazalty	bazalt	k1gInPc1	bazalt
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
středohoří	středohoří	k1gNnSc6	středohoří
<g/>
,	,	kIx,	,
např.	např.	kA	např.
kopce	kopec	k1gInSc2	kopec
Tlustec	Tlustec	k1gInSc1	Tlustec
<g/>
,	,	kIx,	,
Radobýl	Radobýl	k1gInSc1	Radobýl
nebo	nebo	k8xC	nebo
hora	hora	k1gFnSc1	hora
Říp	Říp	k1gInSc1	Říp
<g/>
.	.	kIx.	.
</s>
<s>
Doupovské	Doupovský	k2eAgFnPc1d1	Doupovská
hory	hora	k1gFnPc1	hora
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
čediče	čedič	k1gInSc2	čedič
(	(	kIx(	(
<g/>
48	[number]	k4	48
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgFnPc1d1	známá
lokality	lokalita	k1gFnPc1	lokalita
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
republice	republika	k1gFnSc6	republika
jsou	být	k5eAaImIp3nP	být
chráněny	chránit	k5eAaImNgFnP	chránit
jako	jako	k9	jako
přírodní	přírodní	k2eAgFnPc1d1	přírodní
památky	památka	k1gFnPc1	památka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Panská	panský	k2eAgFnSc1d1	Panská
skála	skála	k1gFnSc1	skála
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Kamenický	kamenický	k2eAgInSc1d1	kamenický
Šenov	Šenov	k1gInSc1	Šenov
<g/>
,	,	kIx,	,
čedičová	čedičový	k2eAgFnSc1d1	čedičová
žíla	žíla	k1gFnSc1	žíla
Boč	bočit	k5eAaImRp2nS	bočit
<g/>
,	,	kIx,	,
čedičové	čedičový	k2eAgFnPc4d1	čedičová
varhany	varhany	k1gFnPc4	varhany
u	u	k7c2	u
Hlinek	hlinka	k1gFnPc2	hlinka
nebo	nebo	k8xC	nebo
Rotava	Rotava	k1gFnSc1	Rotava
<g/>
.	.	kIx.	.
</s>
<s>
Čedič	čedič	k1gInSc1	čedič
se	se	k3xPyFc4	se
už	už	k6eAd1	už
po	po	k7c6	po
tisíciletí	tisíciletí	k1gNnSc6	tisíciletí
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
stavební	stavební	k2eAgInSc1d1	stavební
kámen	kámen	k1gInSc1	kámen
<g/>
,	,	kIx,	,
na	na	k7c4	na
štěrk	štěrk	k1gInSc4	štěrk
a	a	k8xC	a
kamenivo	kamenivo	k1gNnSc4	kamenivo
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc4	součást
betonů	beton	k1gInPc2	beton
i	i	k8xC	i
směsí	směs	k1gFnPc2	směs
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
silnic	silnice	k1gFnPc2	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Novodobé	novodobý	k2eAgNnSc1d1	novodobé
využití	využití	k1gNnSc1	využití
našel	najít	k5eAaPmAgInS	najít
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
petrurgie	petrurgie	k1gFnSc2	petrurgie
<g/>
.	.	kIx.	.
</s>
<s>
Taví	tavit	k5eAaImIp3nS	tavit
se	se	k3xPyFc4	se
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
kolem	kolem	k7c2	kolem
1300	[number]	k4	1300
°	°	k?	°
<g/>
C.	C.	kA	C.
Slévárny	slévárna	k1gFnSc2	slévárna
čediče	čedič	k1gInSc2	čedič
produkují	produkovat	k5eAaImIp3nP	produkovat
např.	např.	kA	např.
dlažby	dlažba	k1gFnPc1	dlažba
<g/>
,	,	kIx,	,
žlaby	žlab	k1gInPc1	žlab
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
otěruvzdorná	otěruvzdorný	k2eAgNnPc4d1	otěruvzdorné
potrubí	potrubí	k1gNnPc4	potrubí
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
pneumatickou	pneumatický	k2eAgFnSc4d1	pneumatická
nebo	nebo	k8xC	nebo
hydraulickou	hydraulický	k2eAgFnSc4d1	hydraulická
dopravu	doprava	k1gFnSc4	doprava
abrazivních	abrazivní	k2eAgInPc2d1	abrazivní
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Dlažby	dlažba	k1gFnPc1	dlažba
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
pro	pro	k7c4	pro
odolné	odolný	k2eAgFnPc4d1	odolná
průmyslové	průmyslový	k2eAgFnPc4d1	průmyslová
podlahy	podlaha	k1gFnPc4	podlaha
<g/>
.	.	kIx.	.
</s>
<s>
Žlaby	žlab	k1gInPc1	žlab
<g/>
,	,	kIx,	,
cihly	cihla	k1gFnPc1	cihla
a	a	k8xC	a
trouby	trouba	k1gFnPc1	trouba
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
vlastnosti	vlastnost	k1gFnPc4	vlastnost
používány	používat	k5eAaImNgFnP	používat
i	i	k9	i
pro	pro	k7c4	pro
kanalizace	kanalizace	k1gFnPc4	kanalizace
<g/>
.	.	kIx.	.
</s>
<s>
Čedičová	čedičový	k2eAgFnSc1d1	čedičová
tavenina	tavenina	k1gFnSc1	tavenina
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
také	také	k9	také
rozvláknit	rozvláknit	k5eAaPmF	rozvláknit
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vznikají	vznikat	k5eAaImIp3nP	vznikat
vysoce	vysoce	k6eAd1	vysoce
pevná	pevný	k2eAgNnPc1d1	pevné
a	a	k8xC	a
ohebná	ohebný	k2eAgNnPc1d1	ohebné
vlákna	vlákno	k1gNnPc1	vlákno
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
předčí	předčit	k5eAaBmIp3nP	předčit
svými	svůj	k3xOyFgFnPc7	svůj
fyzikálními	fyzikální	k2eAgFnPc7d1	fyzikální
<g/>
,	,	kIx,	,
mechanickými	mechanický	k2eAgFnPc7d1	mechanická
a	a	k8xC	a
chemickými	chemický	k2eAgFnPc7d1	chemická
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
vlákna	vlákno	k1gNnPc1	vlákno
skleněná	skleněný	k2eAgNnPc1d1	skleněné
i	i	k8xC	i
azbestová	azbestový	k2eAgNnPc1d1	azbestové
<g/>
.	.	kIx.	.
</s>
<s>
Tkaniny	tkanina	k1gFnPc4	tkanina
z	z	k7c2	z
čedičových	čedičový	k2eAgNnPc2d1	čedičové
vláken	vlákno	k1gNnPc2	vlákno
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
použít	použít	k5eAaPmF	použít
ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
<g/>
,	,	kIx,	,
v	v	k7c6	v
leteckém	letecký	k2eAgInSc6d1	letecký
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
tepelných	tepelný	k2eAgFnPc2d1	tepelná
<g/>
,	,	kIx,	,
zvukových	zvukový	k2eAgFnPc2d1	zvuková
a	a	k8xC	a
chemických	chemický	k2eAgFnPc2d1	chemická
izolací	izolace	k1gFnPc2	izolace
<g/>
,	,	kIx,	,
kompozitních	kompozitní	k2eAgFnPc2d1	kompozitní
výztuží	výztuž	k1gFnPc2	výztuž
atd.	atd.	kA	atd.
Tato	tento	k3xDgNnPc1	tento
vlákna	vlákno	k1gNnPc1	vlákno
jsou	být	k5eAaImIp3nP	být
zpracovávána	zpracováván	k2eAgNnPc1d1	zpracováváno
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
vysokopevnostních	vysokopevnostní	k2eAgInPc2d1	vysokopevnostní
<g/>
,	,	kIx,	,
tepelně	tepelně	k6eAd1	tepelně
odolných	odolný	k2eAgFnPc6d1	odolná
Hi-Tech	Hi-T	k1gFnPc6	Hi-T
šňůr	šňůra	k1gFnPc2	šňůra
a	a	k8xC	a
sítí	síť	k1gFnPc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
čedič	čedič	k1gInSc1	čedič
zpracováván	zpracovávat	k5eAaImNgInS	zpracovávat
tavením	tavení	k1gNnSc7	tavení
ve	v	k7c6	v
Staré	Staré	k2eAgFnSc6d1	Staré
Vodě	voda	k1gFnSc6	voda
u	u	k7c2	u
Mariánských	mariánský	k2eAgFnPc2d1	Mariánská
Lázní	lázeň	k1gFnPc2	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
čedič	čedič	k1gInSc1	čedič
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
čedič	čedič	k1gInSc1	čedič
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
