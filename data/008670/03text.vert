<p>
<s>
Bospor	Bospor	k1gInSc1	Bospor
(	(	kIx(	(
<g/>
v	v	k7c6	v
antice	antika	k1gFnSc6	antika
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
Bospor	Bospor	k1gInSc1	Bospor
Thrácký	thrácký	k2eAgInSc1d1	thrácký
nebo	nebo	k8xC	nebo
Bospor	Bospor	k1gInSc1	Bospor
Cařihradský	cařihradský	k2eAgInSc1d1	cařihradský
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
průliv	průliv	k1gInSc4	průliv
mezi	mezi	k7c7	mezi
Černým	černý	k2eAgMnSc7d1	černý
a	a	k8xC	a
Marmarským	Marmarský	k2eAgNnSc7d1	Marmarské
mořem	moře	k1gNnSc7	moře
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
také	také	k9	také
část	část	k1gFnSc1	část
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
Asií	Asie	k1gFnSc7	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
31	[number]	k4	31
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
v	v	k7c6	v
nejužším	úzký	k2eAgNnSc6d3	nejužší
místě	místo	k1gNnSc6	místo
široký	široký	k2eAgInSc4d1	široký
700	[number]	k4	700
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
s	s	k7c7	s
nejmělčí	mělký	k2eAgFnSc7d3	nejmělčí
hloubkou	hloubka	k1gFnSc7	hloubka
13	[number]	k4	13
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obou	dva	k4xCgInPc6	dva
březích	břeh	k1gInPc6	břeh
se	se	k3xPyFc4	se
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
město	město	k1gNnSc1	město
Istanbul	Istanbul	k1gInSc1	Istanbul
<g/>
.	.	kIx.	.
</s>
<s>
Průliv	průliv	k1gInSc1	průliv
je	být	k5eAaImIp3nS	být
překlenut	překlenout	k5eAaPmNgInS	překlenout
třemi	tři	k4xCgInPc7	tři
silničními	silniční	k2eAgInPc7d1	silniční
mosty	most	k1gInPc7	most
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
řeckého	řecký	k2eAgInSc2d1	řecký
Bosporos	Bosporosa	k1gFnPc2	Bosporosa
(	(	kIx(	(
<g/>
Β	Β	k?	Β
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
složenina	složenina	k1gFnSc1	složenina
slov	slovo	k1gNnPc2	slovo
β	β	k?	β
(	(	kIx(	(
<g/>
Bous	Bous	k1gInSc1	Bous
–	–	k?	–
kráva	kráva	k1gFnSc1	kráva
<g/>
)	)	kIx)	)
a	a	k8xC	a
π	π	k?	π
(	(	kIx(	(
<g/>
Poros	porosit	k5eAaPmRp2nS	porosit
–	–	k?	–
brod	brod	k1gInSc1	brod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Kravský	kravský	k2eAgInSc1d1	kravský
brod	brod	k1gInSc1	brod
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
se	se	k3xPyFc4	se
bůh	bůh	k1gMnSc1	bůh
Zeus	Zeusa	k1gFnPc2	Zeusa
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
do	do	k7c2	do
Íó	Íó	k1gFnSc2	Íó
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	on	k3xPp3gFnPc4	on
přistihla	přistihnout	k5eAaPmAgFnS	přistihnout
manželka	manželka	k1gFnSc1	manželka
Héra	Héra	k1gFnSc1	Héra
<g/>
,	,	kIx,	,
proměnil	proměnit	k5eAaPmAgMnS	proměnit
Zeus	Zeus	k1gInSc4	Zeus
Íó	Íó	k1gMnPc2	Íó
v	v	k7c4	v
krávu	kráva	k1gFnSc4	kráva
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
během	během	k7c2	během
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
cesty	cesta	k1gFnSc2	cesta
překročila	překročit	k5eAaPmAgFnS	překročit
i	i	k9	i
průliv	průliv	k1gInSc4	průliv
mezi	mezi	k7c7	mezi
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
Asií	Asie	k1gFnSc7	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
je	být	k5eAaImIp3nS	být
také	také	k9	také
pojmenováno	pojmenován	k2eAgNnSc1d1	pojmenováno
Jónské	jónský	k2eAgNnSc1d1	Jónské
moře	moře	k1gNnSc1	moře
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
i	i	k9	i
název	název	k1gInSc1	název
Thrácký	thrácký	k2eAgInSc4d1	thrácký
Bospor	Bospor	k1gInSc4	Bospor
pro	pro	k7c4	pro
odlišení	odlišení	k1gNnSc4	odlišení
od	od	k7c2	od
Kimmerského	Kimmerský	k2eAgInSc2d1	Kimmerský
Bosporu	Bospor	k1gInSc2	Bospor
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Kerčský	Kerčský	k2eAgInSc1d1	Kerčský
průliv	průliv	k1gInSc1	průliv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Bosporu	Bospor	k1gInSc2	Bospor
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
průliv	průliv	k1gInSc1	průliv
Východní	východní	k2eAgInSc1d1	východní
Bospor	Bospor	k1gInSc4	Bospor
u	u	k7c2	u
Vladivostoku	Vladivostok	k1gInSc2	Vladivostok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
teorie	teorie	k1gFnSc1	teorie
amerických	americký	k2eAgMnPc2d1	americký
geologů	geolog	k1gMnPc2	geolog
Waltera	Walter	k1gMnSc4	Walter
Pitmana	Pitman	k1gMnSc4	Pitman
a	a	k8xC	a
Williama	William	k1gMnSc4	William
Ryana	Ryan	k1gMnSc4	Ryan
z	z	k7c2	z
Kolumbijské	kolumbijský	k2eAgFnSc2d1	kolumbijská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
že	že	k8xS	že
průliv	průliv	k1gInSc1	průliv
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
asi	asi	k9	asi
5600	[number]	k4	5600
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vysoká	vysoký	k2eAgFnSc1d1	vysoká
hladina	hladina	k1gFnSc1	hladina
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
dostala	dostat	k5eAaPmAgFnS	dostat
přes	přes	k7c4	přes
Bospor	Bospor	k1gInSc4	Bospor
do	do	k7c2	do
níže	nízce	k6eAd2	nízce
položeného	položený	k2eAgNnSc2d1	položené
velkého	velký	k2eAgNnSc2d1	velké
sladkovodního	sladkovodní	k2eAgNnSc2d1	sladkovodní
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
Černé	Černé	k2eAgNnSc4d1	Černé
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
hladinami	hladina	k1gFnPc7	hladina
byl	být	k5eAaImAgMnS	být
140	[number]	k4	140
<g/>
–	–	k?	–
<g/>
160	[number]	k4	160
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
vyrovnání	vyrovnání	k1gNnSc1	vyrovnání
trvalo	trvat	k5eAaImAgNnS	trvat
asi	asi	k9	asi
40	[number]	k4	40
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
souvislosti	souvislost	k1gFnSc6	souvislost
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
i	i	k8xC	i
hladina	hladina	k1gFnSc1	hladina
světových	světový	k2eAgNnPc2d1	světové
moří	moře	k1gNnPc2	moře
o	o	k7c4	o
30	[number]	k4	30
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Průliv	průliv	k1gInSc1	průliv
byl	být	k5eAaImAgInS	být
vždy	vždy	k6eAd1	vždy
významným	významný	k2eAgNnSc7d1	významné
strategickým	strategický	k2eAgNnSc7d1	strategické
místem	místo	k1gNnSc7	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
667	[number]	k4	667
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zde	zde	k6eAd1	zde
kolonisté	kolonista	k1gMnPc1	kolonista
z	z	k7c2	z
řecké	řecký	k2eAgFnSc2d1	řecká
Megary	Megara	k1gFnSc2	Megara
založili	založit	k5eAaPmAgMnP	založit
město	město	k1gNnSc4	město
Byzantion	Byzantion	k1gNnSc1	Byzantion
a	a	k8xC	a
kontrolovali	kontrolovat	k5eAaImAgMnP	kontrolovat
tak	tak	k6eAd1	tak
veškerý	veškerý	k3xTgInSc4	veškerý
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
Černomořím	Černomoří	k1gNnSc7	Černomoří
a	a	k8xC	a
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
i	i	k8xC	i
mezi	mezi	k7c7	mezi
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
Asií	Asie	k1gFnSc7	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
řecko-perských	řeckoerský	k2eAgFnPc2d1	řecko-perský
válek	válka	k1gFnPc2	válka
město	město	k1gNnSc1	město
obsadil	obsadit	k5eAaPmAgMnS	obsadit
perský	perský	k2eAgMnSc1d1	perský
král	král	k1gMnSc1	král
Dareios	Dareiosa	k1gFnPc2	Dareiosa
I.	I.	kA	I.
a	a	k8xC	a
při	při	k7c6	při
tažení	tažení	k1gNnSc6	tažení
proti	proti	k7c3	proti
Skytům	Skyt	k1gMnPc3	Skyt
nechal	nechat	k5eAaPmAgInS	nechat
podle	podle	k7c2	podle
Hérodota	Hérodot	k1gMnSc2	Hérodot
přes	přes	k7c4	přes
Bospor	Bospor	k1gInSc4	Bospor
z	z	k7c2	z
lodí	loď	k1gFnPc2	loď
postavit	postavit	k5eAaPmF	postavit
provizorní	provizorní	k2eAgInSc4d1	provizorní
most	most	k1gInSc4	most
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
který	který	k3yRgInSc4	který
údajně	údajně	k6eAd1	údajně
přešlo	přejít	k5eAaPmAgNnS	přejít
až	až	k9	až
700	[number]	k4	700
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
jeho	jeho	k3xOp3gFnSc2	jeho
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
478	[number]	k4	478
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Byzantion	Byzantion	k1gNnSc4	Byzantion
zpět	zpět	k6eAd1	zpět
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Řekové	Řek	k1gMnPc1	Řek
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
340	[number]	k4	340
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
dobytí	dobytí	k1gNnSc4	dobytí
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
Filip	Filip	k1gMnSc1	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Makedonský	makedonský	k2eAgInSc1d1	makedonský
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
samostatné	samostatný	k2eAgNnSc1d1	samostatné
a	a	k8xC	a
po	po	k7c6	po
makedonských	makedonský	k2eAgFnPc6d1	makedonská
válkách	válka	k1gFnPc6	válka
se	se	k3xPyFc4	se
připojilo	připojit	k5eAaPmAgNnS	připojit
k	k	k7c3	k
Římské	římský	k2eAgFnSc3d1	římská
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
330	[number]	k4	330
zde	zde	k6eAd1	zde
císař	císař	k1gMnSc1	císař
Konstantin	Konstantin	k1gMnSc1	Konstantin
I.	I.	kA	I.
Veliký	veliký	k2eAgMnSc1d1	veliký
začal	začít	k5eAaPmAgMnS	začít
budovat	budovat	k5eAaImF	budovat
nové	nový	k2eAgNnSc4d1	nové
město	město	k1gNnSc4	město
Konstantinopol	Konstantinopol	k1gInSc1	Konstantinopol
jako	jako	k9	jako
druhé	druhý	k4xOgNnSc4	druhý
hlavní	hlavní	k2eAgNnSc4d1	hlavní
sídlo	sídlo	k1gNnSc4	sídlo
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
395	[number]	k4	395
se	se	k3xPyFc4	se
Konstantinopol	Konstantinopol	k1gInSc1	Konstantinopol
stala	stát	k5eAaPmAgFnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Strategický	strategický	k2eAgInSc1d1	strategický
význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Bospor	Bospor	k1gInSc1	Bospor
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
námořní	námořní	k2eAgFnSc7d1	námořní
přístupovou	přístupový	k2eAgFnSc7d1	přístupová
cestou	cesta	k1gFnSc7	cesta
Ruska	Rusko	k1gNnSc2	Rusko
z	z	k7c2	z
černomořských	černomořský	k2eAgInPc2d1	černomořský
přístavů	přístav	k1gInPc2	přístav
do	do	k7c2	do
Středozemí	středozemí	k1gNnSc2	středozemí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novodobé	novodobý	k2eAgFnSc6d1	novodobá
historii	historie	k1gFnSc6	historie
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
několikrát	několikrát	k6eAd1	několikrát
pokoušelo	pokoušet	k5eAaImAgNnS	pokoušet
získat	získat	k5eAaPmF	získat
nad	nad	k7c7	nad
ním	on	k3xPp3gNnSc7	on
alespoň	alespoň	k9	alespoň
částečnou	částečný	k2eAgFnSc4d1	částečná
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
rusko-turecké	ruskourecký	k2eAgFnSc2d1	rusko-turecká
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1878	[number]	k4	1878
podepsána	podepsán	k2eAgFnSc1d1	podepsána
Sanstefanská	Sanstefanský	k2eAgFnSc1d1	Sanstefanský
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zaručovala	zaručovat	k5eAaImAgFnS	zaručovat
svobodnou	svobodný	k2eAgFnSc4d1	svobodná
plavbu	plavba	k1gFnSc4	plavba
průlivem	průliv	k1gInSc7	průliv
všem	všecek	k3xTgFnPc3	všecek
neutrálním	neutrální	k2eAgFnPc3d1	neutrální
lodím	loď	k1gFnPc3	loď
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Sè	Sè	k1gFnSc2	Sè
smlouvy	smlouva	k1gFnSc2	smlouva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
Bospor	Bospor	k1gInSc1	Bospor
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Dardanelami	Dardanely	k1gFnPc7	Dardanely
demilitarizovanou	demilitarizovaný	k2eAgFnSc7d1	demilitarizovaná
zónou	zóna	k1gFnSc7	zóna
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
upravena	upravit	k5eAaPmNgFnS	upravit
Lausannskou	lausannský	k2eAgFnSc7d1	Lausannská
smlouvou	smlouva	k1gFnSc7	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
Turecku	Turecko	k1gNnSc3	Turecko
vrátila	vrátit	k5eAaPmAgFnS	vrátit
svrchovanost	svrchovanost	k1gFnSc1	svrchovanost
nad	nad	k7c7	nad
průlivy	průliv	k1gInPc7	průliv
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
muselo	muset	k5eAaImAgNnS	muset
umožnit	umožnit	k5eAaPmF	umožnit
proplutí	proplutí	k1gNnSc1	proplutí
všem	všecek	k3xTgFnPc3	všecek
cizím	cizí	k2eAgFnPc3d1	cizí
lodím	loď	k1gFnPc3	loď
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
tuto	tento	k3xDgFnSc4	tento
smlouvu	smlouva	k1gFnSc4	smlouva
Turecko	Turecko	k1gNnSc4	Turecko
neakceptovalo	akceptovat	k5eNaBmAgNnS	akceptovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
režim	režim	k1gInSc1	režim
v	v	k7c6	v
průlivech	průliv	k1gInPc6	průliv
řídí	řídit	k5eAaImIp3nS	řídit
dohodou	dohoda	k1gFnSc7	dohoda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
z	z	k7c2	z
Montreaux	Montreaux	k1gInSc4	Montreaux
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
uznávána	uznáván	k2eAgFnSc1d1	uznávána
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
lodní	lodní	k2eAgFnSc1d1	lodní
trasa	trasa	k1gFnSc1	trasa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Turecko	Turecko	k1gNnSc1	Turecko
má	mít	k5eAaImIp3nS	mít
možnost	možnost	k1gFnSc4	možnost
částečně	částečně	k6eAd1	částečně
omezit	omezit	k5eAaPmF	omezit
námořní	námořní	k2eAgInSc4d1	námořní
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konferencích	konference	k1gFnPc6	konference
během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
Josif	Josif	k1gMnSc1	Josif
Stalin	Stalin	k1gMnSc1	Stalin
dožadoval	dožadovat	k5eAaImAgInS	dožadovat
poválečného	poválečný	k2eAgNnSc2d1	poválečné
zřízení	zřízení	k1gNnSc2	zřízení
sovětské	sovětský	k2eAgFnSc2d1	sovětská
základny	základna	k1gFnSc2	základna
v	v	k7c6	v
průlivu	průliv	k1gInSc6	průliv
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
Turecko	Turecko	k1gNnSc1	Turecko
bylo	být	k5eAaImAgNnS	být
neutrální	neutrální	k2eAgFnPc4d1	neutrální
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
Turecko	Turecko	k1gNnSc1	Turecko
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
vzdát	vzdát	k5eAaPmF	vzdát
své	svůj	k3xOyFgFnPc4	svůj
neutrality	neutralita	k1gFnPc4	neutralita
a	a	k8xC	a
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1945	[number]	k4	1945
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
válku	válka	k1gFnSc4	válka
Německu	Německo	k1gNnSc3	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
žádných	žádný	k3yNgFnPc2	žádný
bojových	bojový	k2eAgFnPc2d1	bojová
operací	operace	k1gFnPc2	operace
se	se	k3xPyFc4	se
však	však	k9	však
nezapojilo	zapojit	k5eNaPmAgNnS	zapojit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mosty	most	k1gInPc1	most
a	a	k8xC	a
tunely	tunel	k1gInPc1	tunel
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
most	most	k1gInSc1	most
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
1	[number]	k4	1
074	[number]	k4	074
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc4d1	zvaný
také	také	k6eAd1	také
Bosporský	bosporský	k2eAgInSc4d1	bosporský
most	most	k1gInSc4	most
nebo	nebo	k8xC	nebo
První	první	k4xOgInSc4	první
Bosporský	bosporský	k2eAgInSc4d1	bosporský
most	most	k1gInSc4	most
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
přes	přes	k7c4	přes
Bospor	Bospor	k1gInSc4	Bospor
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1988	[number]	k4	1988
byl	být	k5eAaImAgInS	být
asi	asi	k9	asi
5	[number]	k4	5
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
prvního	první	k4xOgInSc2	první
mostu	most	k1gInSc2	most
otevřen	otevřít	k5eAaPmNgInS	otevřít
most	most	k1gInSc1	most
sultána	sultán	k1gMnSc2	sultán
Mehmeda	Mehmed	k1gMnSc2	Mehmed
Dobyvatele	dobyvatel	k1gMnSc2	dobyvatel
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
1	[number]	k4	1
090	[number]	k4	090
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2016	[number]	k4	2016
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
třetí	třetí	k4xOgInSc4	třetí
a	a	k8xC	a
nejsevernější	severní	k2eAgInSc4d3	nejsevernější
most	most	k1gInSc4	most
sultána	sultána	k1gFnSc1	sultána
Selima	Selimum	k1gNnSc2	Selimum
Hrozného	hrozný	k2eAgNnSc2d1	hrozné
s	s	k7c7	s
osmiproudou	osmiproudý	k2eAgFnSc7d1	osmiproudá
silnicí	silnice	k1gFnSc7	silnice
a	a	k8xC	a
železnicí	železnice	k1gFnSc7	železnice
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
2	[number]	k4	2
164	[number]	k4	164
metrů	metr	k1gInPc2	metr
pojmenovaný	pojmenovaný	k2eAgInSc4d1	pojmenovaný
po	po	k7c6	po
sultánu	sultán	k1gMnSc6	sultán
Selimu	Selim	k1gMnSc6	Selim
I.V	I.V	k1gFnSc1	I.V
říjnu	říjen	k1gInSc6	říjen
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
13,7	[number]	k4	13,7
km	km	kA	km
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
železniční	železniční	k2eAgInSc1d1	železniční
tunel	tunel	k1gInSc1	tunel
Marmaray	Marmaraa	k1gFnSc2	Marmaraa
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
55	[number]	k4	55
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
propojuje	propojovat	k5eAaImIp3nS	propojovat
dříve	dříve	k6eAd2	dříve
oddělené	oddělený	k2eAgFnPc4d1	oddělená
části	část	k1gFnPc4	část
istanbulského	istanbulský	k2eAgNnSc2d1	istanbulské
metra	metro	k1gNnSc2	metro
a	a	k8xC	a
železniční	železniční	k2eAgFnSc2d1	železniční
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
Bosporský	bosporský	k2eAgInSc1d1	bosporský
vodní	vodní	k2eAgInSc1d1	vodní
tunel	tunel	k1gInSc1	tunel
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
5	[number]	k4	5
551	[number]	k4	551
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zásobuje	zásobovat	k5eAaImIp3nS	zásobovat
evropskou	evropský	k2eAgFnSc4d1	Evropská
část	část	k1gFnSc4	část
Istanbulu	Istanbul	k1gInSc2	Istanbul
vodou	voda	k1gFnSc7	voda
z	z	k7c2	z
Düzcské	Düzcský	k2eAgFnSc2d1	Düzcský
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Námořní	námořní	k2eAgFnSc1d1	námořní
doprava	doprava	k1gFnSc1	doprava
a	a	k8xC	a
nehody	nehoda	k1gFnPc1	nehoda
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
je	být	k5eAaImIp3nS	být
Bospor	Bospor	k1gInSc1	Bospor
důležitý	důležitý	k2eAgInSc1d1	důležitý
pro	pro	k7c4	pro
transport	transport	k1gInSc4	transport
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
exportuje	exportovat	k5eAaBmIp3nS	exportovat
z	z	k7c2	z
Novorossijska	Novorossijsk	k1gInSc2	Novorossijsk
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
přístavů	přístav	k1gInPc2	přístav
do	do	k7c2	do
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
propluje	proplout	k5eAaPmIp3nS	proplout
průlivem	průliv	k1gInSc7	průliv
až	až	k9	až
53	[number]	k4	53
000	[number]	k4	000
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Silné	silný	k2eAgInPc1d1	silný
proudy	proud	k1gInPc1	proud
jsou	být	k5eAaImIp3nP	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
dopravních	dopravní	k2eAgFnPc2d1	dopravní
nehod	nehoda	k1gFnPc2	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
proplouvání	proplouvání	k1gNnSc6	proplouvání
musí	muset	k5eAaImIp3nS	muset
každá	každý	k3xTgFnSc1	každý
loď	loď	k1gFnSc1	loď
12	[number]	k4	12
<g/>
krát	krát	k6eAd1	krát
prudce	prudko	k6eAd1	prudko
změnit	změnit	k5eAaPmF	změnit
kurz	kurz	k1gInSc4	kurz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1953	[number]	k4	1953
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
461	[number]	k4	461
lodních	lodní	k2eAgFnPc2d1	lodní
nehod	nehoda	k1gFnPc2	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
největšímu	veliký	k2eAgNnSc3d3	veliký
neštěstí	neštěstí	k1gNnSc3	neštěstí
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kyperský	kyperský	k2eAgInSc1d1	kyperský
tanker	tanker	k1gInSc1	tanker
Nassia	Nassium	k1gNnSc2	Nassium
přepravoval	přepravovat	k5eAaImAgInS	přepravovat
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
56	[number]	k4	56
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
průlivu	průliv	k1gInSc2	průliv
se	se	k3xPyFc4	se
střetl	střetnout	k5eAaPmAgMnS	střetnout
s	s	k7c7	s
lodí	loď	k1gFnSc7	loď
Ship	Ship	k1gMnSc1	Ship
Broke	Brok	k1gInPc4	Brok
a	a	k8xC	a
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
20	[number]	k4	20
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nehodě	nehoda	k1gFnSc6	nehoda
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
28	[number]	k4	28
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ropa	ropa	k1gFnSc1	ropa
hořela	hořet	k5eAaImAgFnS	hořet
pět	pět	k4xCc4	pět
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
způsobila	způsobit	k5eAaPmAgFnS	způsobit
ekologickou	ekologický	k2eAgFnSc4d1	ekologická
katastrofu	katastrofa	k1gFnSc4	katastrofa
a	a	k8xC	a
Bospor	Bospor	k1gInSc1	Bospor
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nehodě	nehoda	k1gFnSc6	nehoda
rumunského	rumunský	k2eAgInSc2d1	rumunský
ropného	ropný	k2eAgInSc2d1	ropný
tankeru	tanker	k1gInSc2	tanker
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
41	[number]	k4	41
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výstavba	výstavba	k1gFnSc1	výstavba
průplavu	průplav	k1gInSc2	průplav
==	==	k?	==
</s>
</p>
<p>
<s>
Bosporský	bosporský	k2eAgInSc1d1	bosporský
průliv	průliv	k1gInSc1	průliv
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
námořní	námořní	k2eAgFnSc4d1	námořní
dopravu	doprava	k1gFnSc4	doprava
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejnebezpečnějších	bezpečný	k2eNgInPc2d3	nejnebezpečnější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
turecký	turecký	k2eAgMnSc1d1	turecký
premiér	premiér	k1gMnSc1	premiér
Erdoğ	Erdoğ	k1gMnSc1	Erdoğ
oznámil	oznámit	k5eAaPmAgMnS	oznámit
záměr	záměr	k1gInSc4	záměr
vybudovat	vybudovat	k5eAaPmF	vybudovat
bezpečnější	bezpečný	k2eAgInSc4d2	bezpečnější
průplav	průplav	k1gInSc4	průplav
podél	podél	k7c2	podél
Bosporu	Bospor	k1gInSc2	Bospor
na	na	k7c6	na
evropské	evropský	k2eAgFnSc6d1	Evropská
části	část	k1gFnSc6	část
pevniny	pevnina	k1gFnSc2	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
km	km	kA	km
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
průplav	průplav	k1gInSc4	průplav
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
150	[number]	k4	150
metrů	metr	k1gInPc2	metr
široký	široký	k2eAgInSc1d1	široký
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
výstavba	výstavba	k1gFnSc1	výstavba
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2023	[number]	k4	2023
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
100	[number]	k4	100
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
založení	založení	k1gNnSc2	založení
Turecké	turecký	k2eAgFnSc2d1	turecká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Bospor	Bospor	k1gInSc4	Bospor
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bospor	Bospor	k1gInSc1	Bospor
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Bospor	Bospor	k1gInSc1	Bospor
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Zrádný	zrádný	k2eAgInSc1d1	zrádný
Bospor	Bospor	k1gInSc1	Bospor
</s>
</p>
