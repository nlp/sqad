<s>
Junák	junák	k1gMnSc1	junák
–	–	k?	–
český	český	k2eAgMnSc1d1	český
skaut	skaut	k1gMnSc1	skaut
<g/>
,	,	kIx,	,
z.	z.	k?	z.
s.	s.	k?	s.
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
skautská	skautský	k2eAgFnSc1d1	skautská
organizace	organizace	k1gFnSc1	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Junák	junák	k1gMnSc1	junák
je	být	k5eAaImIp3nS	být
původně	původně	k6eAd1	původně
český	český	k2eAgInSc1d1	český
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
označení	označení	k1gNnSc4	označení
skaut	skaut	k1gMnSc1	skaut
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
název	název	k1gInSc4	název
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Junák	junák	k1gMnSc1	junák
–	–	k?	–
český	český	k2eAgMnSc1d1	český
skaut	skaut	k1gMnSc1	skaut
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
organizací	organizace	k1gFnSc7	organizace
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
světového	světový	k2eAgNnSc2d1	světové
skautského	skautský	k2eAgNnSc2d1	skautské
hnutí	hnutí	k1gNnSc2	hnutí
a	a	k8xC	a
členem	člen	k1gMnSc7	člen
Světové	světový	k2eAgFnSc2d1	světová
organizace	organizace	k1gFnSc2	organizace
skautského	skautský	k2eAgNnSc2d1	skautské
hnutí	hnutí	k1gNnSc2	hnutí
(	(	kIx(	(
<g/>
WOSM	WOSM	kA	WOSM
<g/>
)	)	kIx)	)
a	a	k8xC	a
Světové	světový	k2eAgFnSc2d1	světová
asociace	asociace	k1gFnSc2	asociace
skautek	skautka	k1gFnPc2	skautka
(	(	kIx(	(
<g/>
WAGGGS	WAGGGS	kA	WAGGGS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2015	[number]	k4	2015
nesl	nést	k5eAaImAgInS	nést
název	název	k1gInSc1	název
Junák	junák	k1gMnSc1	junák
–	–	k?	–
svaz	svaz	k1gInSc1	svaz
skautů	skaut	k1gMnPc2	skaut
a	a	k8xC	a
skautek	skautka	k1gFnPc2	skautka
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
registroval	registrovat	k5eAaBmAgInS	registrovat
2113	[number]	k4	2113
oddílů	oddíl	k1gInPc2	oddíl
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vyvíjely	vyvíjet	k5eAaImAgInP	vyvíjet
celoroční	celoroční	k2eAgFnSc4d1	celoroční
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
pořádá	pořádat	k5eAaImIp3nS	pořádat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
dětských	dětský	k2eAgInPc2d1	dětský
táborů	tábor	k1gInPc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
992	[number]	k4	992
táborů	tábor	k1gInPc2	tábor
<g/>
,	,	kIx,	,
kterých	který	k3yRgInPc2	který
se	se	k3xPyFc4	se
účastnilo	účastnit	k5eAaImAgNnS	účastnit
23	[number]	k4	23
tisíc	tisíc	k4xCgInPc2	tisíc
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Junák	junák	k1gMnSc1	junák
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
skaut	skaut	k1gMnSc1	skaut
se	se	k3xPyFc4	se
dynamicky	dynamicky	k6eAd1	dynamicky
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
stále	stále	k6eAd1	stále
aktuální	aktuální	k2eAgInSc1d1	aktuální
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
dětem	dítě	k1gFnPc3	dítě
i	i	k8xC	i
jejich	jejich	k3xOp3gMnPc3	jejich
rodičům	rodič	k1gMnPc3	rodič
co	co	k9	co
nabídnout	nabídnout	k5eAaPmF	nabídnout
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
nový	nový	k2eAgInSc1d1	nový
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
změnil	změnit	k5eAaPmAgInS	změnit
se	se	k3xPyFc4	se
systém	systém	k1gInSc1	systém
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
vedoucích	vedoucí	k1gMnPc2	vedoucí
<g/>
,	,	kIx,	,
novým	nový	k2eAgMnSc7d1	nový
je	být	k5eAaImIp3nS	být
i	i	k9	i
projekt	projekt	k1gInSc1	projekt
dětské	dětský	k2eAgFnSc2d1	dětská
kategorie	kategorie	k1gFnSc2	kategorie
pro	pro	k7c4	pro
předškolní	předškolní	k2eAgInSc4d1	předškolní
věk	věk	k1gInSc4	věk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hodnoty	hodnota	k1gFnPc1	hodnota
obsažené	obsažený	k2eAgFnPc1d1	obsažená
ve	v	k7c6	v
skautském	skautský	k2eAgInSc6d1	skautský
slibu	slib	k1gInSc6	slib
a	a	k8xC	a
skautských	skautský	k2eAgInPc6d1	skautský
zákonech	zákon	k1gInPc6	zákon
se	se	k3xPyFc4	se
však	však	k9	však
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
v	v	k7c6	v
Junáku	junák	k1gMnSc6	junák
-	-	kIx~	-
českém	český	k2eAgMnSc6d1	český
skautu	skaut	k1gMnSc6	skaut
registrováno	registrovat	k5eAaBmNgNnS	registrovat
55	[number]	k4	55
533	[number]	k4	533
skautů	skaut	k1gMnPc2	skaut
a	a	k8xC	a
skautek	skautka	k1gFnPc2	skautka
<g/>
.	.	kIx.	.
</s>
<s>
Aktuálním	aktuální	k2eAgMnSc7d1	aktuální
náčelníkem	náčelník	k1gMnSc7	náčelník
je	být	k5eAaImIp3nS	být
Marek	Marek	k1gMnSc1	Marek
Baláš	Baláš	k1gMnSc1	Baláš
<g/>
,	,	kIx,	,
náčelní	náčelní	k2eAgFnSc1d1	náčelní
Eva	Eva	k1gFnSc1	Eva
Měřínská	Měřínský	k2eAgFnSc1d1	Měřínská
a	a	k8xC	a
starostou	starosta	k1gMnSc7	starosta
Josef	Josef	k1gMnSc1	Josef
Výprachtický	Výprachtický	k2eAgInSc1d1	Výprachtický
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
kapitánem	kapitán	k1gMnSc7	kapitán
vodních	vodní	k2eAgMnPc2d1	vodní
skautů	skaut	k1gMnPc2	skaut
je	být	k5eAaImIp3nS	být
David	David	k1gMnSc1	David
Svoboda	Svoboda	k1gMnSc1	Svoboda
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Organizační	organizační	k2eAgFnSc1d1	organizační
struktura	struktura	k1gFnSc1	struktura
Junáka	junák	k1gMnSc2	junák
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
výchovnou	výchovný	k2eAgFnSc7d1	výchovná
jednotkou	jednotka	k1gFnSc7	jednotka
je	být	k5eAaImIp3nS	být
skautský	skautský	k2eAgInSc1d1	skautský
oddíl	oddíl	k1gInSc1	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
obvykle	obvykle	k6eAd1	obvykle
tvoří	tvořit	k5eAaImIp3nS	tvořit
12	[number]	k4	12
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
větší	veliký	k2eAgInPc1d2	veliký
oddíly	oddíl	k1gInPc1	oddíl
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
Oddíly	oddíl	k1gInPc1	oddíl
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
zpravidla	zpravidla	k6eAd1	zpravidla
člení	členit	k5eAaImIp3nS	členit
do	do	k7c2	do
družin	družina	k1gFnPc2	družina
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
posádka	posádka	k1gFnSc1	posádka
u	u	k7c2	u
vodních	vodní	k2eAgMnPc2d1	vodní
skautů	skaut	k1gMnPc2	skaut
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgFnPc1d2	menší
skupinky	skupinka	k1gFnPc1	skupinka
cca	cca	kA	cca
šesti	šest	k4xCc2	šest
až	až	k9	až
osmi	osm	k4xCc2	osm
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
fungují	fungovat	k5eAaImIp3nP	fungovat
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
Družinu	družina	k1gFnSc4	družina
vede	vést	k5eAaImIp3nS	vést
pod	pod	k7c7	pod
dozorem	dozor	k1gInSc7	dozor
vedoucího	vedoucí	k2eAgInSc2d1	vedoucí
oddílu	oddíl	k1gInSc2	oddíl
někdo	někdo	k3yInSc1	někdo
z	z	k7c2	z
jejích	její	k3xOp3gMnPc2	její
starších	starý	k2eAgMnPc2d2	starší
členů	člen	k1gMnPc2	člen
–	–	k?	–
označovaný	označovaný	k2eAgInSc4d1	označovaný
jako	jako	k8xC	jako
rádce	rádce	k1gMnSc4	rádce
družiny	družina	k1gFnSc2	družina
<g/>
.	.	kIx.	.
</s>
<s>
Družina	družina	k1gFnSc1	družina
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
užší	úzký	k2eAgInSc1d2	užší
kolektiv	kolektiv	k1gInSc1	kolektiv
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
poznávají	poznávat	k5eAaImIp3nP	poznávat
své	svůj	k3xOyFgNnSc4	svůj
chování	chování	k1gNnSc4	chování
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
než	než	k8xS	než
školním	školní	k2eAgInSc6d1	školní
kolektivu	kolektiv	k1gInSc6	kolektiv
<g/>
,	,	kIx,	,
učí	učit	k5eAaImIp3nS	učit
se	se	k3xPyFc4	se
žít	žít	k5eAaImF	žít
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
<g/>
,	,	kIx,	,
předcházet	předcházet	k5eAaImF	předcházet
konfliktům	konflikt	k1gInPc3	konflikt
a	a	k8xC	a
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
oddílů	oddíl	k1gInPc2	oddíl
společně	společně	k6eAd1	společně
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
středisko	středisko	k1gNnSc1	středisko
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
přístav	přístav	k1gInSc1	přístav
u	u	k7c2	u
vodních	vodní	k2eAgMnPc2d1	vodní
skautů	skaut	k1gMnPc2	skaut
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
zajišťovat	zajišťovat	k5eAaImF	zajišťovat
servis	servis	k1gInSc4	servis
a	a	k8xC	a
zázemí	zázemí	k1gNnSc4	zázemí
oddílům	oddíl	k1gInPc3	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
střediskem	středisko	k1gNnSc7	středisko
stojí	stát	k5eAaImIp3nS	stát
vyšší	vysoký	k2eAgFnPc4d2	vyšší
organizační	organizační	k2eAgFnPc4d1	organizační
jednotky	jednotka	k1gFnPc4	jednotka
-	-	kIx~	-
okresní	okresní	k2eAgFnPc4d1	okresní
a	a	k8xC	a
krajské	krajský	k2eAgFnPc4d1	krajská
rady	rada	k1gFnPc4	rada
Junáka	junák	k1gMnSc2	junák
a	a	k8xC	a
ústřední	ústřední	k2eAgInPc4d1	ústřední
orgány	orgán	k1gInPc4	orgán
Junáka	junák	k1gMnSc2	junák
<g/>
.	.	kIx.	.
</s>
<s>
Oddíly	oddíl	k1gInPc1	oddíl
a	a	k8xC	a
přístavy	přístav	k1gInPc1	přístav
vodních	vodní	k2eAgMnPc2d1	vodní
skautů	skaut	k1gMnPc2	skaut
jsou	být	k5eAaImIp3nP	být
sdruženy	sdružen	k2eAgInPc1d1	sdružen
do	do	k7c2	do
výchovné	výchovný	k2eAgFnSc2d1	výchovná
sítě	síť	k1gFnSc2	síť
řízené	řízený	k2eAgNnSc1d1	řízené
Hlavním	hlavní	k2eAgInSc7d1	hlavní
kapitanátem	kapitanát	k1gInSc7	kapitanát
vodních	vodní	k2eAgMnPc2d1	vodní
skautů	skaut	k1gMnPc2	skaut
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Junáku	junák	k1gMnSc3	junák
v	v	k7c6	v
členské	členský	k2eAgFnSc3d1	členská
základně	základna	k1gFnSc3	základna
daří	dařit	k5eAaImIp3nS	dařit
stále	stále	k6eAd1	stále
růst	růst	k5eAaImF	růst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
opět	opět	k6eAd1	opět
překonal	překonat	k5eAaPmAgMnS	překonat
hranici	hranice	k1gFnSc4	hranice
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
55	[number]	k4	55
tisíc	tisíc	k4xCgInSc1	tisíc
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Věkové	věkový	k2eAgFnSc2d1	věková
kategorie	kategorie	k1gFnSc2	kategorie
v	v	k7c6	v
Junáku	junák	k1gMnSc6	junák
<g/>
.	.	kIx.	.
</s>
<s>
Junák	junák	k1gMnSc1	junák
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
několika	několik	k4yIc7	několik
věkovými	věkový	k2eAgFnPc7d1	věková
kategoriemi	kategorie	k1gFnPc7	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
se	se	k3xPyFc4	se
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
kategorii	kategorie	k1gFnSc4	kategorie
na	na	k7c6	na
základě	základ	k1gInSc6	základ
potřeb	potřeba	k1gFnPc2	potřeba
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc6	jejich
věku	věk	k1gInSc6	věk
<g/>
.	.	kIx.	.
</s>
<s>
Věkové	věkový	k2eAgNnSc1d1	věkové
rozmezí	rozmezí	k1gNnSc1	rozmezí
uvedené	uvedený	k2eAgNnSc1d1	uvedené
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
kategorií	kategorie	k1gFnPc2	kategorie
není	být	k5eNaImIp3nS	být
striktní	striktní	k2eAgFnSc1d1	striktní
<g/>
.	.	kIx.	.
benjamínci	benjamínek	k1gMnPc1	benjamínek
–	–	k?	–
předškoláci	předškolák	k1gMnPc1	předškolák
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
vlčata	vlče	k1gNnPc1	vlče
(	(	kIx(	(
<g/>
chlapci	chlapec	k1gMnSc6	chlapec
<g/>
)	)	kIx)	)
a	a	k8xC	a
světlušky	světluška	k1gFnPc1	světluška
<g/>
/	/	kIx~	/
<g/>
žabičky	žabička	k1gFnPc1	žabička
(	(	kIx(	(
<g/>
dívky	dívka	k1gFnPc1	dívka
<g/>
)	)	kIx)	)
–	–	k?	–
obvykle	obvykle	k6eAd1	obvykle
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
let	léto	k1gNnPc2	léto
skauti	skaut	k1gMnPc1	skaut
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
skautky	skautka	k1gFnPc1	skautka
–	–	k?	–
přibližně	přibližně	k6eAd1	přibližně
11	[number]	k4	11
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
let	léto	k1gNnPc2	léto
roveři	rover	k1gMnPc1	rover
(	(	kIx(	(
<g/>
chlapci	chlapec	k1gMnPc1	chlapec
<g/>
)	)	kIx)	)
a	a	k8xC	a
rangers	rangers	k1gInSc1	rangers
(	(	kIx(	(
<g/>
dívky	dívka	k1gFnPc1	dívka
<g/>
)	)	kIx)	)
–	–	k?	–
přibližně	přibližně	k6eAd1	přibližně
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
21	[number]	k4	21
let	léto	k1gNnPc2	léto
oldskauti	oldskauť	k1gFnSc2	oldskauť
–	–	k?	–
dospělí	dospělí	k1gMnPc1	dospělí
Oddíl	oddíl	k1gInSc1	oddíl
tvoří	tvořit	k5eAaImIp3nS	tvořit
jedna	jeden	k4xCgFnSc1	jeden
kategorie	kategorie	k1gFnSc1	kategorie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
společné	společný	k2eAgInPc1d1	společný
oddíly	oddíl	k1gInPc1	oddíl
vlčat	vlče	k1gNnPc2	vlče
a	a	k8xC	a
skautů	skaut	k1gMnPc2	skaut
či	či	k8xC	či
světlušek	světluška	k1gFnPc2	světluška
a	a	k8xC	a
skautek	skautka	k1gFnPc2	skautka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
koedukované	koedukovaný	k2eAgInPc1d1	koedukovaný
oddíly	oddíl	k1gInPc1	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Roverské	Roverský	k2eAgInPc1d1	Roverský
kmeny	kmen	k1gInPc1	kmen
jsou	být	k5eAaImIp3nP	být
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
kategorií	kategorie	k1gFnSc7	kategorie
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
odlišností	odlišnost	k1gFnPc2	odlišnost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
práci	práce	k1gFnSc6	práce
s	s	k7c7	s
nejmenšími	malý	k2eAgFnPc7d3	nejmenší
dětmi	dítě	k1gFnPc7	dítě
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
symbolický	symbolický	k2eAgInSc4d1	symbolický
rámec	rámec	k1gInSc4	rámec
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
veškeré	veškerý	k3xTgNnSc4	veškerý
dění	dění	k1gNnSc4	dění
je	být	k5eAaImIp3nS	být
spojeno	spojen	k2eAgNnSc1d1	spojeno
nějakým	nějaký	k3yIgInSc7	nějaký
jednotícím	jednotící	k2eAgInSc7d1	jednotící
příběhem	příběh	k1gInSc7	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
mladší	mladý	k2eAgFnSc2d2	mladší
kategorie	kategorie	k1gFnSc2	kategorie
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k8xS	jako
vlčata	vlče	k1gNnPc1	vlče
a	a	k8xC	a
světlušky	světluška	k1gFnPc1	světluška
(	(	kIx(	(
<g/>
u	u	k7c2	u
chlapců	chlapec	k1gMnPc2	chlapec
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Kniha	kniha	k1gFnSc1	kniha
džunglí	džungle	k1gFnPc2	džungle
Rudyarda	Rudyard	k1gMnSc2	Rudyard
Kiplinga	Kipling	k1gMnSc2	Kipling
<g/>
;	;	kIx,	;
u	u	k7c2	u
dívek	dívka	k1gFnPc2	dívka
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Kouzelná	kouzelný	k2eAgFnSc1d1	kouzelná
lucerna	lucerna	k1gFnSc1	lucerna
Radka	Radek	k1gMnSc2	Radek
Kučery	Kučera	k1gMnSc2	Kučera
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
skautů	skaut	k1gMnPc2	skaut
a	a	k8xC	a
skautek	skautka	k1gFnPc2	skautka
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
družinovým	družinový	k2eAgInSc7d1	družinový
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
potřebě	potřeba	k1gFnSc3	potřeba
tvořit	tvořit	k5eAaImF	tvořit
kamarádské	kamarádský	k2eAgInPc4d1	kamarádský
užší	úzký	k2eAgInPc4d2	užší
kolektivy	kolektiv	k1gInPc4	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Klade	klást	k5eAaImIp3nS	klást
se	se	k3xPyFc4	se
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
samostatnost	samostatnost	k1gFnSc4	samostatnost
a	a	k8xC	a
charakterové	charakterový	k2eAgFnPc4d1	charakterová
kvality	kvalita	k1gFnPc4	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Roveři	rover	k1gMnPc1	rover
a	a	k8xC	a
rangers	rangers	k6eAd1	rangers
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
činnosti	činnost	k1gFnSc6	činnost
značnou	značný	k2eAgFnSc4d1	značná
dávku	dávka	k1gFnSc4	dávka
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
podporuje	podporovat	k5eAaImIp3nS	podporovat
jejich	jejich	k3xOp3gFnSc4	jejich
touhu	touha	k1gFnSc4	touha
po	po	k7c6	po
samostatnosti	samostatnost	k1gFnSc6	samostatnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
jim	on	k3xPp3gMnPc3	on
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
věku	věk	k1gInSc6	věk
vlastní	vlastní	k2eAgFnSc4d1	vlastní
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
prostor	prostor	k1gInSc4	prostor
k	k	k7c3	k
seberealizaci	seberealizace	k1gFnSc3	seberealizace
a	a	k8xC	a
hledání	hledání	k1gNnSc4	hledání
své	svůj	k3xOyFgFnSc2	svůj
další	další	k2eAgFnSc2d1	další
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
veškeré	veškerý	k3xTgFnSc2	veškerý
činnosti	činnost	k1gFnSc2	činnost
Junáka	junák	k1gMnSc2	junák
je	být	k5eAaImIp3nS	být
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
oddílech	oddíl	k1gInPc6	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Mimoto	mimoto	k6eAd1	mimoto
Junák	junák	k1gMnSc1	junák
pořádá	pořádat	k5eAaImIp3nS	pořádat
řadu	řada	k1gFnSc4	řada
akcí	akce	k1gFnPc2	akce
pro	pro	k7c4	pro
členy	člen	k1gMnPc4	člen
i	i	k9	i
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
cílů	cíl	k1gInPc2	cíl
skautingu	skauting	k1gInSc2	skauting
je	být	k5eAaImIp3nS	být
propojovat	propojovat	k5eAaImF	propojovat
mladé	mladý	k2eAgMnPc4d1	mladý
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelně	pravidelně	k6eAd1	pravidelně
se	se	k3xPyFc4	se
pořádají	pořádat	k5eAaImIp3nP	pořádat
setkávací	setkávací	k2eAgFnPc1d1	setkávací
akce	akce	k1gFnPc1	akce
prakticky	prakticky	k6eAd1	prakticky
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
úrovních	úroveň	k1gFnPc6	úroveň
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgMnPc1d1	místní
<g/>
,	,	kIx,	,
okresní	okresní	k2eAgFnSc6d1	okresní
<g/>
,	,	kIx,	,
krajské	krajský	k2eAgFnSc6d1	krajská
<g/>
,	,	kIx,	,
celostátní	celostátní	k2eAgFnSc6d1	celostátní
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
i	i	k8xC	i
světové	světový	k2eAgFnSc6d1	světová
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
stráví	strávit	k5eAaPmIp3nS	strávit
vždy	vždy	k6eAd1	vždy
tisícovka	tisícovka	k1gFnSc1	tisícovka
skautů	skaut	k1gMnPc2	skaut
a	a	k8xC	a
skautek	skautka	k1gFnPc2	skautka
starších	starší	k1gMnPc2	starší
15	[number]	k4	15
let	léto	k1gNnPc2	léto
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
na	na	k7c6	na
celorepublikovém	celorepublikový	k2eAgNnSc6d1	celorepublikové
setkání	setkání	k1gNnSc6	setkání
Obrok	obrok	k1gInSc1	obrok
<g/>
.	.	kIx.	.
</s>
<s>
Potkávají	potkávat	k5eAaImIp3nP	potkávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
diskutují	diskutovat	k5eAaImIp3nP	diskutovat
s	s	k7c7	s
odborníky	odborník	k1gMnPc7	odborník
<g/>
,	,	kIx,	,
navazují	navazovat	k5eAaImIp3nP	navazovat
kontakty	kontakt	k1gInPc4	kontakt
<g/>
,	,	kIx,	,
realizují	realizovat	k5eAaBmIp3nP	realizovat
workshopy	workshop	k1gInPc4	workshop
i	i	k8xC	i
službu	služba	k1gFnSc4	služba
pro	pro	k7c4	pro
místní	místní	k2eAgFnSc4d1	místní
komunitu	komunita	k1gFnSc4	komunita
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Večery	večer	k1gInPc1	večer
pak	pak	k6eAd1	pak
vyplní	vyplnit	k5eAaPmIp3nP	vyplnit
zábavou	zábava	k1gFnSc7	zábava
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
nebo	nebo	k8xC	nebo
posezením	posezení	k1gNnSc7	posezení
v	v	k7c6	v
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
postavených	postavený	k2eAgInPc2d1	postavený
čajovnách	čajovna	k1gFnPc6	čajovna
nebo	nebo	k8xC	nebo
kavárnách	kavárna	k1gFnPc6	kavárna
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
ročník	ročník	k1gInSc1	ročník
akce	akce	k1gFnSc2	akce
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
Obrok	obrok	k1gInSc1	obrok
koná	konat	k5eAaImIp3nS	konat
každé	každý	k3xTgInPc4	každý
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
Sklené	sklený	k2eAgNnSc1d1	Sklené
nad	nad	k7c7	nad
Oslavou	oslava	k1gFnSc7	oslava
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
Potštejn	Potštejn	k1gInSc1	Potštejn
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
Bílá	bílý	k2eAgFnSc1d1	bílá
skála	skála	k1gFnSc1	skála
<g/>
,	,	kIx,	,
Číměř	Číměř	k1gFnSc1	Číměř
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
Jaroměř	Jaroměř	k1gFnSc1	Jaroměř
–	–	k?	–
Josefov	Josefov	k1gInSc1	Josefov
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
Blansko	Blansko	k1gNnSc1	Blansko
–	–	k?	–
Češkovice	Češkovice	k1gFnSc1	Češkovice
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
Švihov	Švihov	k1gInSc1	Švihov
–	–	k?	–
vodní	vodní	k2eAgInSc1d1	vodní
hrad	hrad	k1gInSc1	hrad
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
Liberec	Liberec	k1gInSc1	Liberec
–	–	k?	–
Vesec	Vesec	k1gInSc1	Vesec
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
Trutnov	Trutnov	k1gInSc1	Trutnov
–	–	k?	–
Bojiště	bojiště	k1gNnSc1	bojiště
Navigamus	Navigamus	k1gMnSc1	Navigamus
je	být	k5eAaImIp3nS	být
celostátní	celostátní	k2eAgNnSc4d1	celostátní
setkání	setkání	k1gNnSc4	setkání
vodních	vodní	k2eAgMnPc2d1	vodní
skautů	skaut	k1gMnPc2	skaut
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
každé	každý	k3xTgInPc4	každý
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
akce	akce	k1gFnSc2	akce
"	"	kIx"	"
<g/>
Navigamus	Navigamus	k1gInSc1	Navigamus
<g/>
"	"	kIx"	"
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
a	a	k8xC	a
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
my	my	k3xPp1nPc1	my
plujeme	plout	k5eAaImIp1nP	plout
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Námětem	námět	k1gInSc7	námět
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
významná	významný	k2eAgFnSc1d1	významná
historická	historický	k2eAgFnSc1d1	historická
námořní	námořní	k2eAgFnSc1d1	námořní
událost	událost	k1gFnSc1	událost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mohou	moct	k5eAaImIp3nP	moct
skauti	skaut	k1gMnPc1	skaut
zažít	zažít	k5eAaPmF	zažít
díky	díky	k7c3	díky
týmovým	týmový	k2eAgFnPc3d1	týmová
a	a	k8xC	a
skupinovým	skupinový	k2eAgFnPc3d1	skupinová
hrám	hra	k1gFnPc3	hra
<g/>
,	,	kIx,	,
outdoorovým	outdoorův	k2eAgFnPc3d1	outdoorův
aktivitám	aktivita	k1gFnPc3	aktivita
a	a	k8xC	a
závodům	závod	k1gInPc3	závod
<g/>
.	.	kIx.	.
</s>
<s>
Posláním	poslání	k1gNnSc7	poslání
akce	akce	k1gFnSc2	akce
je	být	k5eAaImIp3nS	být
setkávání	setkávání	k1gNnSc4	setkávání
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
navázání	navázání	k1gNnSc4	navázání
nových	nový	k2eAgInPc2d1	nový
kontaktů	kontakt	k1gInPc2	kontakt
a	a	k8xC	a
přátelství	přátelství	k1gNnSc4	přátelství
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
skauty	skaut	k1gMnPc7	skaut
z	z	k7c2	z
Česka	Česko	k1gNnSc2	Česko
i	i	k8xC	i
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
akce	akce	k1gFnSc1	akce
presentuje	presentovat	k5eAaBmIp3nS	presentovat
veřejnosti	veřejnost	k1gFnSc3	veřejnost
nabídku	nabídka	k1gFnSc4	nabídka
volnočasových	volnočasův	k2eAgFnPc2d1	volnočasův
aktivit	aktivita	k1gFnPc2	aktivita
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mládež	mládež	k1gFnSc4	mládež
<g/>
,	,	kIx,	,
umožní	umožnit	k5eAaPmIp3nS	umožnit
jim	on	k3xPp3gMnPc3	on
přímou	přímý	k2eAgFnSc4d1	přímá
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
a	a	k8xC	a
vodní	vodní	k2eAgInSc4d1	vodní
skauting	skauting	k1gInSc4	skauting
je	být	k5eAaImIp3nS	být
veřejnosti	veřejnost	k1gFnSc2	veřejnost
představen	představit	k5eAaPmNgInS	představit
jako	jako	k9	jako
mezinárodní	mezinárodní	k2eAgNnPc4d1	mezinárodní
hnutí	hnutí	k1gNnPc4	hnutí
s	s	k7c7	s
výchovným	výchovný	k2eAgNnSc7d1	výchovné
posláním	poslání	k1gNnSc7	poslání
<g/>
.	.	kIx.	.
</s>
<s>
Akce	akce	k1gFnSc1	akce
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
u	u	k7c2	u
velkých	velký	k2eAgFnPc2d1	velká
vodních	vodní	k2eAgFnPc2d1	vodní
ploch	plocha	k1gFnPc2	plocha
umožňujících	umožňující	k2eAgInPc2d1	umožňující
společné	společný	k2eAgInPc4d1	společný
programy	program	k1gInPc4	program
pro	pro	k7c4	pro
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tisíc	tisíc	k4xCgInSc1	tisíc
účastníků	účastník	k1gMnPc2	účastník
a	a	k8xC	a
stovky	stovka	k1gFnSc2	stovka
lodí	loď	k1gFnPc2	loď
<g/>
:	:	kIx,	:
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
Orlická	orlický	k2eAgFnSc1d1	Orlická
přehrada	přehrada	k1gFnSc1	přehrada
–	–	k?	–
téma	téma	k1gNnSc1	téma
<g/>
:	:	kIx,	:
50	[number]	k4	50
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
invaze	invaze	k1gFnSc2	invaze
Spojeneckých	spojenecký	k2eAgNnPc2d1	spojenecké
vojsk	vojsko	k1gNnPc2	vojsko
v	v	k7c6	v
Normandii	Normandie	k1gFnSc6	Normandie
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
Orlická	orlický	k2eAgFnSc1d1	Orlická
přehrada	přehrada	k1gFnSc1	přehrada
–	–	k?	–
téma	téma	k1gNnSc1	téma
<g/>
:	:	kIx,	:
500	[number]	k4	500
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
výročí	výročí	k1gNnSc1	výročí
obeplutí	obeplutí	k1gNnSc2	obeplutí
Afriky	Afrika	k1gFnSc2	Afrika
portugalským	portugalský	k2eAgMnSc7d1	portugalský
mořeplavcem	mořeplavec	k1gMnSc7	mořeplavec
Vasco	Vasco	k6eAd1	Vasco
de	de	k?	de
Gamou	Gamá	k1gFnSc4	Gamá
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
Seč	seč	k6eAd1	seč
–	–	k?	–
téma	téma	k1gNnSc4	téma
<g/>
:	:	kIx,	:
300	[number]	k4	300
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
pirátské	pirátský	k2eAgFnSc2d1	pirátská
republiky	republika	k1gFnSc2	republika
Tortuga	Tortuga	k1gFnSc1	Tortuga
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
Břehy	břeh	k1gInPc4	břeh
u	u	k7c2	u
Přelouče	Přelouč	k1gFnSc2	Přelouč
–	–	k?	–
téma	téma	k1gNnSc1	téma
<g/>
:	:	kIx,	:
60	[number]	k4	60
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
bitvy	bitva	k1gFnSc2	bitva
o	o	k7c6	o
Guadalcanal	Guadalcanal	k1gFnSc6	Guadalcanal
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
Hlučínské	hlučínský	k2eAgFnPc1d1	Hlučínská
štěrkovna	štěrkovna	k1gFnSc1	štěrkovna
–	–	k?	–
téma	téma	k1gNnSc1	téma
<g/>
:	:	kIx,	:
650	[number]	k4	650
<g />
.	.	kIx.	.
</s>
<s>
<g/>
výročí	výročí	k1gNnSc1	výročí
založení	založení	k1gNnSc2	založení
Velké	velký	k2eAgFnSc2d1	velká
Hanzy	hanza	k1gFnSc2	hanza
<g/>
,	,	kIx,	,
kupeckého	kupecký	k2eAgInSc2d1	kupecký
spolku	spolek	k1gInSc2	spolek
středověkých	středověký	k2eAgNnPc2d1	středověké
měst	město	k1gNnPc2	město
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
Rozkoš	rozkoš	k1gFnSc1	rozkoš
–	–	k?	–
téma	téma	k1gNnSc1	téma
<g/>
:	:	kIx,	:
plavby	plavba	k1gFnPc1	plavba
námořníka	námořník	k1gMnSc2	námořník
Sindibáda	Sindibáda	k1gFnSc1	Sindibáda
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
Velký	velký	k2eAgInSc1d1	velký
Bolevecký	Bolevecký	k2eAgInSc1d1	Bolevecký
rybník	rybník	k1gInSc1	rybník
–	–	k?	–
téma	téma	k1gNnSc1	téma
<g/>
:	:	kIx,	:
100	[number]	k4	100
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
Titaniku	Titanic	k1gInSc2	Titanic
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
Seč	seč	k6eAd1	seč
–	–	k?	–
téma	téma	k1gNnSc4	téma
<g/>
:	:	kIx,	:
210	[number]	k4	210
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Trafalgaru	Trafalgar	k1gInSc2	Trafalgar
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
Lipno	Lipno	k1gNnSc1	Lipno
-	-	kIx~	-
téma	téma	k1gNnSc4	téma
<g/>
:	:	kIx,	:
cesty	cesta	k1gFnSc2	cesta
admirála	admirál	k1gMnSc4	admirál
Čheng	Čheng	k1gMnSc1	Čheng
Che	che	k0	che
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
národní	národní	k2eAgNnSc4d1	národní
jamboree	jamboree	k1gNnSc4	jamboree
s	s	k7c7	s
názvem	název	k1gInSc7	název
Klíč	klíč	k1gInSc4	klíč
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
sjelo	sjet	k5eAaPmAgNnS	sjet
na	na	k7c4	na
2300	[number]	k4	2300
dětí	dítě	k1gFnPc2	dítě
skautského	skautský	k2eAgInSc2d1	skautský
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
rozměr	rozměr	k1gInSc1	rozměr
se	se	k3xPyFc4	se
ve	v	k7c6	v
skautingu	skauting	k1gInSc6	skauting
vnímá	vnímat	k5eAaImIp3nS	vnímat
jako	jako	k9	jako
důležitý	důležitý	k2eAgInSc1d1	důležitý
prvek	prvek	k1gInSc1	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
skauti	skaut	k1gMnPc1	skaut
tradičně	tradičně	k6eAd1	tradičně
vysílají	vysílat	k5eAaImIp3nP	vysílat
silné	silný	k2eAgInPc1d1	silný
kontingenty	kontingent	k1gInPc1	kontingent
na	na	k7c4	na
světové	světový	k2eAgNnSc4d1	světové
jamboree	jamboree	k1gNnSc4	jamboree
–	–	k?	–
největší	veliký	k2eAgNnSc1d3	veliký
setkání	setkání	k1gNnSc1	setkání
skautů	skaut	k1gMnPc2	skaut
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
jamboree	jamboree	k1gNnSc1	jamboree
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
a	a	k8xC	a
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
285	[number]	k4	285
českých	český	k2eAgMnPc2d1	český
skautů	skaut	k1gMnPc2	skaut
a	a	k8xC	a
skautek	skautka	k1gFnPc2	skautka
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
skauti	skaut	k1gMnPc1	skaut
se	se	k3xPyFc4	se
také	také	k9	také
pravidelně	pravidelně	k6eAd1	pravidelně
účastní	účastnit	k5eAaImIp3nS	účastnit
středoevropských	středoevropský	k2eAgNnPc2d1	středoevropské
jamboree	jamboree	k1gNnPc2	jamboree
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
v	v	k7c6	v
novodobé	novodobý	k2eAgFnSc6d1	novodobá
historii	historie	k1gFnSc6	historie
konalo	konat	k5eAaImAgNnS	konat
už	už	k6eAd1	už
třikrát	třikrát	k6eAd1	třikrát
-	-	kIx~	-
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
v	v	k7c6	v
Mariánském	mariánský	k2eAgNnSc6d1	Mariánské
údolí	údolí	k1gNnSc6	údolí
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
v	v	k7c6	v
Doksech	Doksy	k1gInPc6	Doksy
nedaleko	nedaleko	k7c2	nedaleko
Máchova	Máchův	k2eAgNnSc2d1	Máchovo
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
středoevrospkého	středoevrospký	k2eAgNnSc2d1	středoevrospký
jamboree	jamboree	k1gNnSc2	jamboree
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
už	už	k6eAd1	už
třikrát	třikrát	k6eAd1	třikrát
také	také	k9	také
konal	konat	k5eAaImAgInS	konat
Intercamp	Intercamp	k1gInSc1	Intercamp
<g/>
,	,	kIx,	,
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
v	v	k7c6	v
pevnosti	pevnost	k1gFnSc6	pevnost
Josefov	Josefov	k1gInSc4	Josefov
v	v	k7c6	v
Jaroměři	Jaroměř	k1gFnSc6	Jaroměř
<g/>
.	.	kIx.	.
</s>
<s>
Junák	junák	k1gMnSc1	junák
je	být	k5eAaImIp3nS	být
dlouholetým	dlouholetý	k2eAgMnSc7d1	dlouholetý
spojencem	spojenec	k1gMnSc7	spojenec
sbírky	sbírka	k1gFnSc2	sbírka
Pomozte	pomoct	k5eAaPmRp2nPwC	pomoct
dětem	dítě	k1gFnPc3	dítě
<g/>
,	,	kIx,	,
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Nadací	nadace	k1gFnSc7	nadace
pro	pro	k7c4	pro
transplantace	transplantace	k1gFnPc4	transplantace
kostní	kostní	k2eAgFnSc2d1	kostní
dřeně	dřeň	k1gFnSc2	dřeň
pořádá	pořádat	k5eAaImIp3nS	pořádat
též	též	k9	též
sbírkovou	sbírkový	k2eAgFnSc4d1	sbírková
akci	akce	k1gFnSc4	akce
Společně	společně	k6eAd1	společně
proti	proti	k7c3	proti
leukémii	leukémie	k1gFnSc3	leukémie
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
Člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
pořádá	pořádat	k5eAaImIp3nS	pořádat
akci	akce	k1gFnSc4	akce
Postavme	postavit	k5eAaPmRp1nP	postavit
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
šesti	šest	k4xCc2	šest
ročníků	ročník	k1gInPc2	ročník
sbírky	sbírka	k1gFnSc2	sbírka
již	již	k9	již
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
vybraných	vybraný	k2eAgInPc2d1	vybraný
prostředků	prostředek	k1gInPc2	prostředek
postaveno	postavit	k5eAaPmNgNnS	postavit
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
devět	devět	k4xCc4	devět
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2000	[number]	k4	2000
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Posledních	poslední	k2eAgNnPc2d1	poslední
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
skauti	skaut	k1gMnPc1	skaut
zapojují	zapojovat	k5eAaImIp3nP	zapojovat
do	do	k7c2	do
akce	akce	k1gFnSc2	akce
Skautský	skautský	k2eAgInSc4d1	skautský
dobrý	dobrý	k2eAgInSc4d1	dobrý
skutek	skutek	k1gInSc4	skutek
<g/>
.	.	kIx.	.
</s>
<s>
Betlémské	betlémský	k2eAgNnSc1d1	Betlémské
světlo	světlo	k1gNnSc1	světlo
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
novodobým	novodobý	k2eAgInSc7d1	novodobý
symbolem	symbol	k1gInSc7	symbol
Vánoc	Vánoce	k1gFnPc2	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
zapaluje	zapalovat	k5eAaImIp3nS	zapalovat
v	v	k7c6	v
Betlémě	Betlém	k1gInSc6	Betlém
<g/>
,	,	kIx,	,
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
putuje	putovat	k5eAaImIp3nS	putovat
napříč	napříč	k7c7	napříč
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Česka	Česko	k1gNnSc2	Česko
betlémské	betlémský	k2eAgNnSc1d1	Betlémské
světlo	světlo	k1gNnSc1	světlo
putuje	putovat	k5eAaImIp3nS	putovat
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
rakouští	rakouský	k2eAgMnPc1d1	rakouský
skauti	skaut	k1gMnPc1	skaut
předávají	předávat	k5eAaImIp3nP	předávat
delegacím	delegace	k1gFnPc3	delegace
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
Česko	Česko	k1gNnSc4	Česko
jej	on	k3xPp3gInSc2	on
přebírají	přebírat	k5eAaImIp3nP	přebírat
brněnští	brněnský	k2eAgMnPc1d1	brněnský
skauti	skaut	k1gMnPc1	skaut
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
jej	on	k3xPp3gMnSc4	on
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
před	před	k7c7	před
4	[number]	k4	4
<g/>
.	.	kIx.	.
nedělí	dělit	k5eNaImIp3nP	dělit
adventní	adventní	k2eAgMnPc1d1	adventní
skauti	skaut	k1gMnPc1	skaut
rozvážejí	rozvážet	k5eAaImIp3nP	rozvážet
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nechávají	nechávat	k5eAaImIp3nP	nechávat
jej	on	k3xPp3gInSc4	on
pak	pak	k6eAd1	pak
zdarma	zdarma	k6eAd1	zdarma
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
v	v	k7c6	v
kostelích	kostel	k1gInPc6	kostel
<g/>
,	,	kIx,	,
na	na	k7c6	na
náměstích	náměstí	k1gNnPc6	náměstí
<g/>
,	,	kIx,	,
vánočních	vánoční	k2eAgInPc6d1	vánoční
trzích	trh	k1gInPc6	trh
<g/>
,	,	kIx,	,
knihovnách	knihovna	k1gFnPc6	knihovna
na	na	k7c6	na
stovkách	stovka	k1gFnPc6	stovka
míst	místo	k1gNnPc2	místo
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
symbol	symbol	k1gInSc1	symbol
původního	původní	k2eAgInSc2d1	původní
významu	význam	k1gInSc2	význam
Vánoc	Vánoce	k1gFnPc2	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Aktuálně	aktuálně	k6eAd1	aktuálně
se	se	k3xPyFc4	se
diskutuje	diskutovat	k5eAaImIp3nS	diskutovat
o	o	k7c6	o
reformách	reforma	k1gFnPc6	reforma
skautingu	skauting	k1gInSc2	skauting
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
nového	nový	k2eAgInSc2d1	nový
výchovného	výchovný	k2eAgInSc2d1	výchovný
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dělí	dělit	k5eAaImIp3nS	dělit
děti	dítě	k1gFnPc4	dítě
do	do	k7c2	do
více	hodně	k6eAd2	hodně
skupin	skupina	k1gFnPc2	skupina
podle	podle	k7c2	podle
věku	věk	k1gInSc2	věk
a	a	k8xC	a
více	hodně	k6eAd2	hodně
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
koedukovanými	koedukovaný	k2eAgInPc7d1	koedukovaný
oddíly	oddíl	k1gInPc7	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
zavádějí	zavádět	k5eAaImIp3nP	zavádět
nové	nový	k2eAgFnSc2d1	nová
skautské	skautský	k2eAgFnSc2d1	skautská
a	a	k8xC	a
vlčácké	vlčácký	k2eAgFnSc2d1	vlčácká
stezky	stezka	k1gFnSc2	stezka
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
jsou	být	k5eAaImIp3nP	být
publikace	publikace	k1gFnPc1	publikace
provádějící	provádějící	k2eAgNnSc1d1	provádějící
dítě	dítě	k1gNnSc1	dítě
skautským	skautský	k2eAgInSc7d1	skautský
rokem	rok	k1gInSc7	rok
a	a	k8xC	a
sledující	sledující	k2eAgInSc4d1	sledující
jeho	on	k3xPp3gInSc4	on
osobnostní	osobnostní	k2eAgInSc4d1	osobnostní
růst	růst	k1gInSc4	růst
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
podoba	podoba	k1gFnSc1	podoba
stezek	stezka	k1gFnPc2	stezka
měnila	měnit	k5eAaImAgFnS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgFnSc1	první
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
stezka	stezka	k1gFnSc1	stezka
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
fantasy	fantas	k1gInPc4	fantas
obrázkové	obrázkový	k2eAgFnSc2d1	obrázková
knížky	knížka	k1gFnSc2	knížka
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
kritice	kritika	k1gFnSc6	kritika
představena	představen	k2eAgFnSc1d1	představena
i	i	k8xC	i
verze	verze	k1gFnSc1	verze
bez	bez	k7c2	bez
fantasy	fantas	k1gInPc7	fantas
motivů	motiv	k1gInPc2	motiv
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
výchovný	výchovný	k2eAgInSc1d1	výchovný
program	program	k1gInSc1	program
je	být	k5eAaImIp3nS	být
projektem	projekt	k1gInSc7	projekt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
inovovat	inovovat	k5eAaBmF	inovovat
současné	současný	k2eAgInPc4d1	současný
výchovné	výchovný	k2eAgInPc4d1	výchovný
prostředky	prostředek	k1gInPc4	prostředek
na	na	k7c6	na
národní	národní	k2eAgFnSc6d1	národní
úrovni	úroveň	k1gFnSc6	úroveň
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
odpovídaly	odpovídat	k5eAaImAgFnP	odpovídat
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
a	a	k8xC	a
zájmům	zájem	k1gInPc3	zájem
dnešních	dnešní	k2eAgFnPc2d1	dnešní
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mladých	mladý	k2eAgMnPc2d1	mladý
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Zadání	zadání	k1gNnSc1	zadání
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
poskytl	poskytnout	k5eAaPmAgInS	poskytnout
dokument	dokument	k1gInSc1	dokument
X.	X.	kA	X.
Valného	valný	k2eAgInSc2d1	valný
sněmu	sněm	k1gInSc2	sněm
Junáka	junák	k1gMnSc4	junák
–	–	k?	–
Charta	charta	k1gFnSc1	charta
českého	český	k2eAgInSc2d1	český
skautingu	skauting	k1gInSc2	skauting
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c4	pod
nový	nový	k2eAgInSc4d1	nový
výchovný	výchovný	k2eAgInSc4d1	výchovný
program	program	k1gInSc4	program
Junáka	junák	k1gMnSc2	junák
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
skautská	skautský	k2eAgFnSc1d1	skautská
metoda	metoda	k1gFnSc1	metoda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
metodický	metodický	k2eAgInSc4d1	metodický
rámec	rámec	k1gInSc4	rámec
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
vede	vést	k5eAaImIp3nS	vést
mladého	mladý	k2eAgMnSc4d1	mladý
člověka	člověk	k1gMnSc4	člověk
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc2	jeho
stezce	stezka	k1gFnSc6	stezka
osobního	osobní	k2eAgInSc2d1	osobní
růstu	růst	k1gInSc2	růst
(	(	kIx(	(
<g/>
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
prvky	prvek	k1gInPc4	prvek
<g/>
:	:	kIx,	:
zákon	zákon	k1gInSc4	zákon
a	a	k8xC	a
slib	slib	k1gInSc4	slib
<g/>
;	;	kIx,	;
učím	učit	k5eAaImIp1nS	učit
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
dělám	dělat	k5eAaImIp1nS	dělat
<g/>
;	;	kIx,	;
družinový	družinový	k2eAgInSc1d1	družinový
systém	systém	k1gInSc1	systém
<g />
.	.	kIx.	.
</s>
<s>
<g/>
;	;	kIx,	;
symbolický	symbolický	k2eAgInSc4d1	symbolický
rámec	rámec	k1gInSc4	rámec
<g/>
;	;	kIx,	;
program	program	k1gInSc1	program
osobního	osobní	k2eAgInSc2d1	osobní
růstu	růst	k1gInSc2	růst
<g/>
;	;	kIx,	;
příroda	příroda	k1gFnSc1	příroda
<g/>
;	;	kIx,	;
podpora	podpora	k1gFnSc1	podpora
dospělými	dospělý	k2eAgMnPc7d1	dospělý
<g/>
)	)	kIx)	)
výchovné	výchovný	k2eAgInPc1d1	výchovný
cíle	cíl	k1gInPc1	cíl
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
kompetence	kompetence	k1gFnSc2	kompetence
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
konkrétně	konkrétně	k6eAd1	konkrétně
rozpracovávají	rozpracovávat	k5eAaImIp3nP	rozpracovávat
poslání	poslání	k1gNnSc4	poslání
skautingu	skauting	k1gInSc2	skauting
a	a	k8xC	a
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
naší	náš	k3xOp1gFnSc2	náš
představy	představa	k1gFnSc2	představa
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
projdou	projít	k5eAaPmIp3nP	projít
skautskou	skautský	k2eAgFnSc7d1	skautská
výchovou	výchova	k1gFnSc7	výchova
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Charta	charta	k1gFnSc1	charta
českého	český	k2eAgInSc2d1	český
<g />
.	.	kIx.	.
</s>
<s>
skautingu	skauting	k1gInSc2	skauting
<g/>
)	)	kIx)	)
věkové	věkový	k2eAgFnSc2d1	věková
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
určují	určovat	k5eAaImIp3nP	určovat
pro	pro	k7c4	pro
jaký	jaký	k3yQgInSc4	jaký
věk	věk	k1gInSc4	věk
a	a	k8xC	a
v	v	k7c6	v
jakých	jaký	k3yIgNnPc6	jaký
věkových	věkový	k2eAgNnPc6d1	věkové
rozmezích	rozmezí	k1gNnPc6	rozmezí
organizovat	organizovat	k5eAaBmF	organizovat
výchovu	výchova	k1gFnSc4	výchova
výchovná	výchovný	k2eAgFnSc1d1	výchovná
nabídka	nabídka	k1gFnSc1	nabídka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
konkrétních	konkrétní	k2eAgInPc2d1	konkrétní
zájmů	zájem	k1gInPc2	zájem
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mladých	mladý	k2eAgMnPc2d1	mladý
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
právě	právě	k6eAd1	právě
jim	on	k3xPp3gMnPc3	on
může	moct	k5eAaImIp3nS	moct
skauting	skauting	k1gInSc1	skauting
nabídnout	nabídnout	k5eAaPmF	nabídnout
výchovné	výchovný	k2eAgInPc4d1	výchovný
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
<g />
.	.	kIx.	.
</s>
<s>
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
konkrétních	konkrétní	k2eAgInPc2d1	konkrétní
prostředků	prostředek	k1gInPc2	prostředek
pro	pro	k7c4	pro
oddílovou	oddílový	k2eAgFnSc4d1	oddílová
činnost	činnost	k1gFnSc4	činnost
–	–	k?	–
stezky	stezka	k1gFnPc1	stezka
<g/>
,	,	kIx,	,
odborky	odborek	k1gInPc1	odborek
<g/>
,	,	kIx,	,
zkoušky	zkouška	k1gFnPc1	zkouška
<g/>
,	,	kIx,	,
závody	závod	k1gInPc1	závod
<g/>
,	,	kIx,	,
celostátní	celostátní	k2eAgInPc1d1	celostátní
projekty	projekt	k1gInPc1	projekt
a	a	k8xC	a
akce	akce	k1gFnSc1	akce
apod.	apod.	kA	apod.
metodická	metodický	k2eAgFnSc1d1	metodická
podpora	podpora	k1gFnSc1	podpora
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
napomáhá	napomáhat	k5eAaBmIp3nS	napomáhat
vedení	vedení	k1gNnSc4	vedení
oddílů	oddíl	k1gInPc2	oddíl
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
s	s	k7c7	s
výchovných	výchovný	k2eAgFnPc2d1	výchovná
programem	program	k1gInSc7	program
–	–	k?	–
semináře	seminář	k1gInSc2	seminář
<g/>
,	,	kIx,	,
kursy	kurs	k1gInPc1	kurs
<g/>
,	,	kIx,	,
časopisy	časopis	k1gInPc1	časopis
<g/>
,	,	kIx,	,
příručky	příručka	k1gFnPc1	příručka
První	první	k4xOgMnSc1	první
<g />
.	.	kIx.	.
</s>
<s>
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
skautingu	skauting	k1gInSc6	skauting
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
vyšla	vyjít	k5eAaPmAgFnS	vyjít
první	první	k4xOgFnSc1	první
brožurka	brožurka	k1gFnSc1	brožurka
od	od	k7c2	od
A.	A.	kA	A.
B.	B.	kA	B.
Svojsíka	Svojsík	k1gMnSc2	Svojsík
(	(	kIx(	(
<g/>
s	s	k7c7	s
názvem	název	k1gInSc7	název
Základy	základ	k1gInPc1	základ
Junáctví	junáctví	k1gNnSc1	junáctví
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
samotným	samotný	k2eAgInSc7d1	samotný
Svojsíkem	Svojsík	k1gInSc7	Svojsík
veden	vést	k5eAaImNgInS	vést
první	první	k4xOgInSc1	první
český	český	k2eAgInSc1d1	český
skautský	skautský	k2eAgInSc1d1	skautský
tábor	tábor	k1gInSc1	tábor
(	(	kIx(	(
<g/>
nedaleko	nedaleko	k7c2	nedaleko
Lipnice	Lipnice	k1gFnSc2	Lipnice
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
první	první	k4xOgFnSc6	první
kurz	kurz	k1gInSc1	kurz
pro	pro	k7c4	pro
rádce	rádce	k1gMnPc4	rádce
a	a	k8xC	a
vůdce	vůdce	k1gMnPc4	vůdce
a	a	k8xC	a
v	v	k7c6	v
září	září	k1gNnSc6	září
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Královské	královský	k2eAgFnSc6d1	královská
oboře	obora	k1gFnSc6	obora
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
zahájen	zahájit	k5eAaPmNgInS	zahájit
první	první	k4xOgInSc1	první
propagační	propagační	k2eAgInSc1d1	propagační
skautský	skautský	k2eAgInSc1d1	skautský
tábor	tábor	k1gInSc1	tábor
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
přesně	přesně	k6eAd1	přesně
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
spolek	spolek	k1gInSc1	spolek
s	s	k7c7	s
názvem	název	k1gInSc7	název
Junák	junák	k1gMnSc1	junák
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
skaut	skaut	k1gMnSc1	skaut
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
význam	význam	k1gInSc1	význam
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
Junák	junák	k1gMnSc1	junák
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgMnS	být
zhruba	zhruba	k6eAd1	zhruba
statný	statný	k2eAgMnSc1d1	statný
mládenec	mládenec	k1gMnSc1	mládenec
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
české	český	k2eAgNnSc4d1	české
označení	označení	k1gNnSc4	označení
skauta	skaut	k1gMnSc2	skaut
použil	použít	k5eAaPmAgMnS	použít
toto	tento	k3xDgNnSc4	tento
slovo	slovo	k1gNnSc4	slovo
zakladatel	zakladatel	k1gMnSc1	zakladatel
českého	český	k2eAgMnSc2d1	český
Junáka	junák	k1gMnSc2	junák
A.	A.	kA	A.
B.	B.	kA	B.
Svojsík	Svojsík	k1gInSc1	Svojsík
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
Františka	František	k1gMnSc2	František
Bílého	bílý	k1gMnSc2	bílý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
Vlasty	Vlasta	k1gFnSc2	Vlasta
Štěpánové-Koseové	Štěpánové-Koseová	k1gFnSc2	Štěpánové-Koseová
Odbor	odbor	k1gInSc1	odbor
pro	pro	k7c4	pro
dívčí	dívčí	k2eAgFnSc4d1	dívčí
výchovu	výchova	k1gFnSc4	výchova
skautskou	skautský	k2eAgFnSc7d1	skautská
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
první	první	k4xOgInSc1	první
tábor	tábor	k1gInSc1	tábor
skautek	skautka	k1gFnPc2	skautka
u	u	k7c2	u
Živohoště	Živohošť	k1gFnSc2	Živohošť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
byla	být	k5eAaImAgFnS	být
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Československá	československý	k2eAgFnSc1d1	Československá
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
skautská	skautský	k2eAgFnSc1d1	skautská
pošta	pošta	k1gFnSc1	pošta
a	a	k8xC	a
vydány	vydán	k2eAgFnPc1d1	vydána
první	první	k4xOgFnPc1	první
známky	známka	k1gFnPc1	známka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
s	s	k7c7	s
tematikou	tematika	k1gFnSc7	tematika
skautingu	skauting	k1gInSc2	skauting
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
se	se	k3xPyFc4	se
skauti	skaut	k1gMnPc1	skaut
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
návratu	návrat	k1gInSc3	návrat
Tomáše	Tomáš	k1gMnSc2	Tomáš
Garrigue	Garrigu	k1gMnSc2	Garrigu
Masaryka	Masaryk	k1gMnSc2	Masaryk
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
(	(	kIx(	(
<g/>
do	do	k7c2	do
ČSR	ČSR	kA	ČSR
-	-	kIx~	-
Horního	horní	k2eAgNnSc2d1	horní
Dvořiště	dvořiště	k1gNnSc2	dvořiště
a	a	k8xC	a
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
přijel	přijet	k5eAaPmAgMnS	přijet
vlakem	vlak	k1gInSc7	vlak
Masaryk	Masaryk	k1gMnSc1	Masaryk
o	o	k7c4	o
den	den	k1gInSc4	den
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
ten	ten	k3xDgInSc4	ten
den	den	k1gInSc4	den
fungovala	fungovat	k5eAaImAgFnS	fungovat
skautská	skautský	k2eAgFnSc1d1	skautská
pošta	pošta	k1gFnSc1	pošta
s	s	k7c7	s
přetiskem	přetisk	k1gInSc7	přetisk
skautských	skautský	k2eAgFnPc2d1	skautská
známek	známka	k1gFnPc2	známka
"	"	kIx"	"
<g/>
Příjezd	příjezd	k1gInSc1	příjezd
presidenta	president	k1gMnSc2	president
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
ustanoven	ustanoven	k2eAgInSc1d1	ustanoven
Svaz	svaz	k1gInSc1	svaz
junáků	junák	k1gMnPc2	junák
–	–	k?	–
skautů	skaut	k1gMnPc2	skaut
a	a	k8xC	a
skautek	skautka	k1gFnPc2	skautka
RČS	RČS	kA	RČS
<g/>
,	,	kIx,	,
starostou	starosta	k1gMnSc7	starosta
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Josef	Josef	k1gMnSc1	Josef
Rössler	Rössler	k1gMnSc1	Rössler
<g/>
,	,	kIx,	,
náčelníkem	náčelník	k1gInSc7	náčelník
A.	A.	kA	A.
B.	B.	kA	B.
Svojsík	Svojsík	k1gInSc1	Svojsík
a	a	k8xC	a
náčelnicí	náčelnice	k1gFnSc7	náčelnice
Emilie	Emilie	k1gFnSc2	Emilie
Milčicová	Milčicová	k1gFnSc1	Milčicová
<g/>
,	,	kIx,	,
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
byla	být	k5eAaImAgFnS	být
za	za	k7c4	za
znak	znak	k1gInSc4	znak
junáka	junák	k1gMnSc2	junák
zvolena	zvolen	k2eAgFnSc1d1	zvolena
zlatá	zlatá	k1gFnSc1	zlatá
(	(	kIx(	(
<g/>
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
)	)	kIx)	)
lilie	lilie	k1gFnSc1	lilie
se	s	k7c7	s
stříbrným	stříbrný	k2eAgInSc7d1	stříbrný
(	(	kIx(	(
<g/>
bílým	bílý	k2eAgInSc7d1	bílý
<g/>
)	)	kIx)	)
štítkem	štítek	k1gInSc7	štítek
a	a	k8xC	a
černou	černý	k2eAgFnSc7d1	černá
psí	psí	k2eAgFnSc7d1	psí
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
první	první	k4xOgNnSc1	první
slovanské	slovanský	k2eAgNnSc1d1	slovanské
jamboree	jamboree	k1gNnSc1	jamboree
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
předchozí	předchozí	k2eAgNnPc4d1	předchozí
léta	léto	k1gNnPc4	léto
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
pouze	pouze	k6eAd1	pouze
lesní	lesní	k2eAgFnSc2d1	lesní
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
dívčí	dívčí	k2eAgFnSc6d1	dívčí
i	i	k8xC	i
chlapecké	chlapecký	k2eAgFnSc6d1	chlapecká
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
byli	být	k5eAaImAgMnP	být
rozpuštěni	rozpuštěn	k2eAgMnPc1d1	rozpuštěn
němečtí	německý	k2eAgMnPc1d1	německý
skauti	skaut	k1gMnPc1	skaut
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
ustavující	ustavující	k2eAgInSc1d1	ustavující
sněm	sněm	k1gInSc1	sněm
Junáka	junák	k1gMnSc2	junák
(	(	kIx(	(
<g/>
sjednocení	sjednocení	k1gNnSc1	sjednocení
dílčích	dílčí	k2eAgFnPc2d1	dílčí
skautských	skautský	k2eAgFnPc2d1	skautská
organizací	organizace	k1gFnPc2	organizace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velitelem	velitel	k1gMnSc7	velitel
byl	být	k5eAaImAgMnS	být
jmenován	jmenován	k2eAgMnSc1d1	jmenován
V.	V.	kA	V.
Vlček	vlček	k1gInSc4	vlček
a	a	k8xC	a
náčelníkem	náčelník	k1gInSc7	náčelník
B.	B.	kA	B.
Řehák	Řehák	k1gMnSc1	Řehák
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
byla	být	k5eAaImAgFnS	být
česká	český	k2eAgFnSc1d1	Česká
skautská	skautský	k2eAgFnSc1d1	skautská
organizace	organizace	k1gFnSc1	organizace
zlikvidována	zlikvidovat	k5eAaPmNgFnS	zlikvidovat
nacisty	nacista	k1gMnPc7	nacista
<g/>
,	,	kIx,	,
nařízeno	nařídit	k5eAaPmNgNnS	nařídit
to	ten	k3xDgNnSc4	ten
bylo	být	k5eAaImAgNnS	být
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Skauting	skauting	k1gInSc1	skauting
byl	být	k5eAaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
již	již	k6eAd1	již
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
další	další	k2eAgInSc1d1	další
rok	rok	k1gInSc1	rok
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
odstartován	odstartován	k2eAgInSc4d1	odstartován
první	první	k4xOgInSc4	první
ročník	ročník	k1gInSc4	ročník
Svojsíkova	Svojsíkův	k2eAgInSc2d1	Svojsíkův
závodu	závod	k1gInSc2	závod
a	a	k8xC	a
založena	založen	k2eAgFnSc1d1	založena
mohyla	mohyla	k1gFnSc1	mohyla
Ivančena	Ivančen	k2eAgFnSc1d1	Ivančena
v	v	k7c6	v
Beskydech	Beskyd	k1gInPc6	Beskyd
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
byl	být	k5eAaImAgMnS	být
Junák	junák	k1gMnSc1	junák
začleněn	začlenit	k5eAaPmNgMnS	začlenit
do	do	k7c2	do
Svazu	svaz	k1gInSc2	svaz
československé	československý	k2eAgFnSc2d1	Československá
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1950	[number]	k4	1950
byl	být	k5eAaImAgInS	být
oznámen	oznámen	k2eAgInSc1d1	oznámen
zánik	zánik	k1gInSc1	zánik
Junáka	junák	k1gMnSc2	junák
a	a	k8xC	a
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
jeho	jeho	k3xOp3gFnSc4	jeho
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
obnovení	obnovení	k1gNnSc1	obnovení
skautingu	skauting	k1gInSc2	skauting
přišlo	přijít	k5eAaPmAgNnS	přijít
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
pražským	pražský	k2eAgInSc7d1	pražský
jarem	jar	k1gInSc7	jar
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
byla	být	k5eAaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
činnost	činnost	k1gFnSc1	činnost
a	a	k8xC	a
starostou	starosta	k1gMnSc7	starosta
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Antonín	Antonín	k1gMnSc1	Antonín
Sum	suma	k1gFnPc2	suma
<g/>
,	,	kIx,	,
náčelníkem	náčelník	k1gMnSc7	náčelník
Rudolf	Rudolfa	k1gFnPc2	Rudolfa
Plajner	Plajner	k1gInSc1	Plajner
a	a	k8xC	a
náčelní	náčelní	k2eAgFnSc1d1	náčelní
Vlasta	Vlasta	k1gFnSc1	Vlasta
Koseová	Koseová	k1gFnSc1	Koseová
<g/>
,	,	kIx,	,
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1970	[number]	k4	1970
byl	být	k5eAaImAgInS	být
skauting	skauting	k1gInSc1	skauting
opět	opět	k6eAd1	opět
zakázán	zakázat	k5eAaPmNgInS	zakázat
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
jednotná	jednotný	k2eAgFnSc1d1	jednotná
dětská	dětský	k2eAgFnSc1d1	dětská
organizace	organizace	k1gFnSc1	organizace
Pionýrská	pionýrský	k2eAgFnSc1d1	Pionýrská
organizace	organizace	k1gFnSc1	organizace
Socialistického	socialistický	k2eAgInSc2d1	socialistický
svazu	svaz	k1gInSc2	svaz
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byla	být	k5eAaImAgFnS	být
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
opět	opět	k6eAd1	opět
obnovena	obnoven	k2eAgFnSc1d1	obnovena
činnost	činnost	k1gFnSc1	činnost
Junáka	junák	k1gMnSc2	junák
a	a	k8xC	a
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byly	být	k5eAaImAgFnP	být
schváleny	schválen	k2eAgFnPc1d1	schválena
stanovy	stanova	k1gFnPc1	stanova
organizace	organizace	k1gFnSc2	organizace
Český	český	k2eAgMnSc1d1	český
Junák	junák	k1gMnSc1	junák
-	-	kIx~	-
svaz	svaz	k1gInSc1	svaz
skautů	skaut	k1gMnPc2	skaut
a	a	k8xC	a
skautek	skautka	k1gFnPc2	skautka
<g/>
,	,	kIx,	,
na	na	k7c6	na
IV	IV	kA	IV
<g/>
.	.	kIx.	.
řádném	řádný	k2eAgInSc6d1	řádný
sněmu	sněm	k1gInSc6	sněm
Junáka	junák	k1gMnSc2	junák
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
starostkou	starostka	k1gFnSc7	starostka
Junáka	junák	k1gMnSc2	junák
zvolena	zvolen	k2eAgFnSc1d1	zvolena
Dagmar	Dagmar	k1gFnSc1	Dagmar
Burešová	Burešová	k1gFnSc1	Burešová
<g/>
,	,	kIx,	,
starostou	starosta	k1gMnSc7	starosta
Jarmil	Jarmila	k1gFnPc2	Jarmila
Burghauser	Burghauser	k1gInSc1	Burghauser
a	a	k8xC	a
náčelníkem	náčelník	k1gInSc7	náčelník
Václav	Václav	k1gMnSc1	Václav
Břicháček	Břicháček	k?	Břicháček
<g/>
,	,	kIx,	,
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byly	být	k5eAaImAgFnP	být
československé	československý	k2eAgFnPc1d1	Československá
skautky	skautka	k1gFnPc1	skautka
opět	opět	k6eAd1	opět
přijaty	přijmout	k5eAaPmNgFnP	přijmout
do	do	k7c2	do
světové	světový	k2eAgFnSc2d1	světová
organizace	organizace	k1gFnSc2	organizace
WAGGGS	WAGGGS	kA	WAGGGS
a	a	k8xC	a
skauti	skaut	k1gMnPc1	skaut
do	do	k7c2	do
WOSM	WOSM	kA	WOSM
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
byly	být	k5eAaImAgFnP	být
vytvořeny	vytvořit	k5eAaPmNgFnP	vytvořit
nové	nový	k2eAgFnPc1d1	nová
organizace	organizace	k1gFnPc1	organizace
pro	pro	k7c4	pro
rozdělenou	rozdělený	k2eAgFnSc4d1	rozdělená
republiku	republika	k1gFnSc4	republika
a	a	k8xC	a
opět	opět	k6eAd1	opět
byly	být	k5eAaImAgInP	být
přijaty	přijmout	k5eAaPmNgInP	přijmout
do	do	k7c2	do
světových	světový	k2eAgFnPc2d1	světová
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Junák	junák	k1gMnSc1	junák
je	být	k5eAaImIp3nS	být
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
České	český	k2eAgFnSc2d1	Česká
rady	rada	k1gFnSc2	rada
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnPc1	mládež
založené	založený	k2eAgFnPc1d1	založená
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
a	a	k8xC	a
2012	[number]	k4	2012
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
oslavy	oslava	k1gFnPc1	oslava
100	[number]	k4	100
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
vzniku	vznik	k1gInSc2	vznik
skautingu	skauting	k1gInSc2	skauting
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
</s>
<s>
Valném	valný	k2eAgInSc6d1	valný
sněmu	sněm	k1gInSc6	sněm
Junáka	junák	k1gMnSc2	junák
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
od	od	k7c2	od
28	[number]	k4	28
<g/>
.	.	kIx.	.
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2014	[number]	k4	2014
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
delegátky	delegátka	k1gFnPc1	delegátka
a	a	k8xC	a
delegáti	delegát	k1gMnPc1	delegát
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
novému	nový	k2eAgInSc3d1	nový
občanskému	občanský	k2eAgInSc3d1	občanský
zákoníku	zákoník	k1gInSc3	zákoník
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
změnit	změnit	k5eAaPmF	změnit
název	název	k1gInSc4	název
spolku	spolek	k1gInSc2	spolek
na	na	k7c4	na
Junák	junák	k1gMnSc1	junák
–	–	k?	–
český	český	k2eAgMnSc1d1	český
skaut	skaut	k1gMnSc1	skaut
<g/>
,	,	kIx,	,
z.	z.	k?	z.
s.	s.	k?	s.
Po	po	k7c4	po
sto	sto	k4xCgNnSc4	sto
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
tak	tak	k9	tak
čeští	český	k2eAgMnPc1d1	český
skauti	skaut	k1gMnPc1	skaut
vrátili	vrátit	k5eAaPmAgMnP	vrátit
ke	k	k7c3	k
stejnému	stejný	k2eAgInSc3d1	stejný
názvu	název	k1gInSc3	název
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
věkové	věkový	k2eAgFnPc4d1	věková
kategorie	kategorie	k1gFnPc4	kategorie
vydává	vydávat	k5eAaPmIp3nS	vydávat
5	[number]	k4	5
<g/>
x	x	k?	x
ročně	ročně	k6eAd1	ročně
Tiskové	tiskový	k2eAgNnSc1d1	tiskové
a	a	k8xC	a
distribuční	distribuční	k2eAgNnSc1d1	distribuční
centrum	centrum	k1gNnSc1	centrum
Junáka	junák	k1gMnSc2	junák
časopisy	časopis	k1gInPc1	časopis
<g/>
:	:	kIx,	:
Ben	Ben	k1gInSc1	Ben
Já	já	k3xPp1nSc1	já
Mína	Mína	k1gFnSc1	Mína
–	–	k?	–
benjamínci	benjamínek	k1gMnPc1	benjamínek
(	(	kIx(	(
<g/>
předškolní	předškolní	k2eAgFnPc4d1	předškolní
děti	dítě	k1gFnPc4	dítě
<g/>
)	)	kIx)	)
Světýlko	světýlko	k1gNnSc1	světýlko
–	–	k?	–
vlčata	vlče	k1gNnPc4	vlče
a	a	k8xC	a
světlušky	světluška	k1gFnPc4	světluška
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
Skaut	Skaut	k1gInSc1	Skaut
–	–	k?	–
skauti	skaut	k1gMnPc1	skaut
a	a	k8xC	a
skautky	skautka	k1gFnPc1	skautka
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
11	[number]	k4	11
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
Roverský	Roverský	k2eAgInSc1d1	Roverský
kmen	kmen	k1gInSc1	kmen
–	–	k?	–
roveři	rover	k1gMnPc1	rover
a	a	k8xC	a
rangers	rangers	k6eAd1	rangers
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
–	–	k?	–
<g/>
24	[number]	k4	24
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
Skauting	skauting	k1gInSc1	skauting
–	–	k?	–
metodická	metodický	k2eAgFnSc1d1	metodická
podpora	podpora	k1gFnSc1	podpora
dobrovolníkům	dobrovolník	k1gMnPc3	dobrovolník
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
+	+	kIx~	+
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
Skautský	skautský	k2eAgInSc1d1	skautský
svět	svět	k1gInSc1	svět
–	–	k?	–
dospělí	dospělí	k1gMnPc1	dospělí
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gMnPc1	vedoucí
<g/>
,	,	kIx,	,
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
příznivci	příznivec	k1gMnPc1	příznivec
skautingu	skauting	k1gInSc2	skauting
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
+	+	kIx~	+
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
Skaut	Skaut	k1gInSc1	Skaut
–	–	k?	–
český	český	k2eAgInSc1d1	český
skauting	skauting	k1gInSc1	skauting
ABS	ABS	kA	ABS
YMCA	YMCA	kA	YMCA
<g/>
–	–	k?	–
<g/>
SKAUT	Skaut	k1gInSc1	Skaut
ČR	ČR	kA	ČR
Klub	klub	k1gInSc1	klub
Pathfinder	Pathfinder	k1gInSc1	Pathfinder
Svaz	svaz	k1gInSc1	svaz
skautů	skaut	k1gMnPc2	skaut
a	a	k8xC	a
skautek	skautka	k1gFnPc2	skautka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Asociace	asociace	k1gFnSc2	asociace
skautů	skaut	k1gMnPc2	skaut
a	a	k8xC	a
skautek	skautka	k1gFnPc2	skautka
Evropy	Evropa	k1gFnSc2	Evropa
Skautský	skautský	k2eAgInSc4d1	skautský
oddíl	oddíl	k1gInSc4	oddíl
Velena	velen	k2eAgFnSc1d1	Velena
Fanderlika	Fanderlika	k1gFnSc1	Fanderlika
Harcerstwo	Harcerstwo	k6eAd1	Harcerstwo
Polskie	Polskie	k1gFnSc2	Polskie
w	w	k?	w
Republice	republika	k1gFnSc3	republika
Czeskiej	Czeskiej	k1gInSc4	Czeskiej
</s>
