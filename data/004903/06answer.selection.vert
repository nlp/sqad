<s>
Významné	významný	k2eAgInPc1d1	významný
nárůsty	nárůst	k1gInPc1	nárůst
počtu	počet	k1gInSc2	počet
sebevražd	sebevražda	k1gFnPc2	sebevražda
jsou	být	k5eAaImIp3nP	být
svázány	svázán	k2eAgInPc1d1	svázán
s	s	k7c7	s
obdobím	období	k1gNnSc7	období
zásadních	zásadní	k2eAgFnPc2d1	zásadní
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
již	již	k6eAd1	již
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
třicátá	třicátý	k4xOgNnPc4	třicátý
léta	léto	k1gNnPc4	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
začátek	začátek	k1gInSc1	začátek
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
přelom	přelom	k1gInSc1	přelom
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
normalizace	normalizace	k1gFnSc2	normalizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
