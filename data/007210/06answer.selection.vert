<s>
Sekt	sekt	k1gInSc1	sekt
je	být	k5eAaImIp3nS	být
šumivý	šumivý	k2eAgInSc4d1	šumivý
nápoj	nápoj	k1gInSc4	nápoj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
skladován	skladovat	k5eAaImNgInS	skladovat
ve	v	k7c6	v
skleněných	skleněný	k2eAgFnPc6d1	skleněná
láhvích	láhev	k1gFnPc6	láhev
s	s	k7c7	s
korkovým	korkový	k2eAgNnSc7d1	korkové
a	a	k8xC	a
nebo	nebo	k8xC	nebo
plastovým	plastový	k2eAgInSc7d1	plastový
uzávěrem	uzávěr	k1gInSc7	uzávěr
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
tlakem	tlak	k1gInSc7	tlak
sektu	sekta	k1gFnSc4	sekta
vytlačit	vytlačit	k5eAaPmF	vytlačit
z	z	k7c2	z
láhve	láhev	k1gFnSc2	láhev
<g/>
.	.	kIx.	.
</s>
