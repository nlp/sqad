<s>
Sekt	sekt	k1gInSc1	sekt
či	či	k8xC	či
někdy	někdy	k6eAd1	někdy
také	také	k9	také
šumivé	šumivý	k2eAgNnSc1d1	šumivé
víno	víno	k1gNnSc1	víno
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
alkoholického	alkoholický	k2eAgInSc2d1	alkoholický
nápoje	nápoj	k1gInSc2	nápoj
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
vyráběno	vyrábět	k5eAaImNgNnS	vyrábět
primárním	primární	k2eAgNnSc7d1	primární
či	či	k8xC	či
sekundárním	sekundární	k2eAgNnSc7d1	sekundární
kvašením	kvašení	k1gNnSc7	kvašení
vína	víno	k1gNnSc2	víno
a	a	k8xC	a
nebo	nebo	k8xC	nebo
moštu	mošt	k1gInSc2	mošt
<g/>
.	.	kIx.	.
</s>
<s>
Sekt	sekt	k1gInSc1	sekt
je	být	k5eAaImIp3nS	být
šumivý	šumivý	k2eAgInSc4d1	šumivý
nápoj	nápoj	k1gInSc4	nápoj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
skladován	skladovat	k5eAaImNgInS	skladovat
ve	v	k7c6	v
skleněných	skleněný	k2eAgFnPc6d1	skleněná
láhvích	láhev	k1gFnPc6	láhev
s	s	k7c7	s
korkovým	korkový	k2eAgNnSc7d1	korkové
a	a	k8xC	a
nebo	nebo	k8xC	nebo
plastovým	plastový	k2eAgInSc7d1	plastový
uzávěrem	uzávěr	k1gInSc7	uzávěr
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
tlakem	tlak	k1gInSc7	tlak
sektu	sekta	k1gFnSc4	sekta
vytlačit	vytlačit	k5eAaPmF	vytlačit
z	z	k7c2	z
láhve	láhev	k1gFnSc2	láhev
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
sekt	sekta	k1gFnPc2	sekta
je	být	k5eAaImIp3nS	být
převzato	převzít	k5eAaPmNgNnS	převzít
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
používáno	používat	k5eAaImNgNnS	používat
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
sekt	sekta	k1gFnPc2	sekta
je	být	k5eAaImIp3nS	být
občas	občas	k6eAd1	občas
zaměňován	zaměňován	k2eAgInSc1d1	zaměňován
s	s	k7c7	s
názvem	název	k1gInSc7	název
šampaňské	šampaňský	k2eAgNnSc4d1	šampaňské
víno	víno	k1gNnSc4	víno
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
se	se	k3xPyFc4	se
ale	ale	k9	ale
mohou	moct	k5eAaImIp3nP	moct
honosit	honosit	k5eAaImF	honosit
pouze	pouze	k6eAd1	pouze
vína	víno	k1gNnPc4	víno
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
francouzské	francouzský	k2eAgFnSc2d1	francouzská
oblasti	oblast	k1gFnSc2	oblast
Champagne	Champagn	k1gMnSc5	Champagn
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
chráněnou	chráněný	k2eAgFnSc4d1	chráněná
značku	značka	k1gFnSc4	značka
<g/>
.	.	kIx.	.
brut	brut	k2eAgInSc1d1	brut
nature	natur	k1gMnSc5	natur
-	-	kIx~	-
pod	pod	k7c4	pod
3	[number]	k4	3
gramů	gram	k1gInPc2	gram
cukru	cukr	k1gInSc2	cukr
na	na	k7c4	na
litr	litr	k1gInSc4	litr
brut	brut	k1gInSc4	brut
-	-	kIx~	-
do	do	k7c2	do
15	[number]	k4	15
gramů	gram	k1gInPc2	gram
cukru	cukr	k1gInSc2	cukr
na	na	k7c4	na
litr	litr	k1gInSc4	litr
extra	extra	k2eAgMnSc2d1	extra
dry	dry	k?	dry
-	-	kIx~	-
12	[number]	k4	12
až	až	k9	až
20	[number]	k4	20
gramů	gram	k1gInPc2	gram
cukru	cukr	k1gInSc2	cukr
na	na	k7c4	na
litr	litr	k1gInSc4	litr
sec	sec	kA	sec
-	-	kIx~	-
17	[number]	k4	17
až	až	k9	až
35	[number]	k4	35
gramů	gram	k1gInPc2	gram
cukru	cukr	k1gInSc2	cukr
na	na	k7c4	na
litr	litr	k1gInSc4	litr
demi-sec	demiec	k1gInSc1	demi-sec
-	-	kIx~	-
33	[number]	k4	33
až	až	k9	až
50	[number]	k4	50
gramů	gram	k1gInPc2	gram
cukru	cukr	k1gInSc2	cukr
na	na	k7c4	na
litr	litr	k1gInSc4	litr
doux	doux	k1gInSc4	doux
-	-	kIx~	-
nad	nad	k7c4	nad
50	[number]	k4	50
gramů	gram	k1gInPc2	gram
cukru	cukr	k1gInSc2	cukr
na	na	k7c4	na
litr	litr	k1gInSc4	litr
Sekt	sekt	k1gInSc1	sekt
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
dvojí	dvojí	k4xRgFnSc7	dvojí
cestou	cesta	k1gFnSc7	cesta
z	z	k7c2	z
vína	víno	k1gNnSc2	víno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
suchého	suchý	k2eAgNnSc2d1	suché
kyselejšího	kyselý	k2eAgNnSc2d2	kyselejší
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
lisování	lisování	k1gNnSc3	lisování
hroznů	hrozen	k1gInPc2	hrozen
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
zabrání	zabránit	k5eAaPmIp3nS	zabránit
vzniku	vznik	k1gInSc2	vznik
oxidantních	oxidantní	k2eAgFnPc2d1	oxidantní
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
tříslovin	tříslovina	k1gFnPc2	tříslovina
<g/>
.	.	kIx.	.
</s>
<s>
Klasickou	klasický	k2eAgFnSc7d1	klasická
a	a	k8xC	a
původní	původní	k2eAgFnSc7d1	původní
metodou	metoda	k1gFnSc7	metoda
je	být	k5eAaImIp3nS	být
kvašení	kvašení	k1gNnSc1	kvašení
vína	víno	k1gNnSc2	víno
v	v	k7c6	v
láhvi	láhev	k1gFnSc6	láhev
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
nazýváno	nazývat	k5eAaImNgNnS	nazývat
jako	jako	k8xS	jako
Méthode	Méthod	k1gInSc5	Méthod
Champenoise	Champenoise	k1gFnSc1	Champenoise
<g/>
.	.	kIx.	.
</s>
<s>
Méthode	Méthod	k1gMnSc5	Méthod
Champenoise	Champenoise	k1gFnPc4	Champenoise
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgInSc1d1	tradiční
postup	postup	k1gInSc1	postup
výroby	výroba	k1gFnSc2	výroba
šumivých	šumivý	k2eAgNnPc2d1	šumivé
vín	víno	k1gNnPc2	víno
v	v	k7c6	v
Champagni	Champageň	k1gFnSc6	Champageň
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
druhotném	druhotný	k2eAgNnSc6d1	druhotné
kvašení	kvašení	k1gNnSc6	kvašení
v	v	k7c6	v
láhvi	láhev	k1gFnSc6	láhev
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ručnímu	ruční	k2eAgInSc3d1	ruční
sběru	sběr	k1gInSc3	sběr
hroznů	hrozen	k1gInPc2	hrozen
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
nižším	nízký	k2eAgInSc7d2	nižší
obsahem	obsah	k1gInSc7	obsah
cukrů	cukr	k1gInPc2	cukr
a	a	k8xC	a
výraznou	výrazný	k2eAgFnSc7d1	výrazná
kyselinkou	kyselinka	k1gFnSc7	kyselinka
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
již	již	k6eAd1	již
100	[number]	k4	100
dní	den	k1gInPc2	den
po	po	k7c6	po
odkvětu	odkvět	k1gInSc6	odkvět
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
má	mít	k5eAaImIp3nS	mít
hrozen	hrozen	k1gInSc1	hrozen
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
kvalitu	kvalita	k1gFnSc4	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Hrozny	hrozen	k1gInPc1	hrozen
jsou	být	k5eAaImIp3nP	být
opatrně	opatrně	k6eAd1	opatrně
lisovány	lisovat	k5eAaImNgInP	lisovat
<g/>
,	,	kIx,	,
do	do	k7c2	do
moštu	mošt	k1gInSc2	mošt
přidány	přidán	k2eAgFnPc1d1	přidána
kvasinky	kvasinka	k1gFnPc1	kvasinka
a	a	k8xC	a
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
první	první	k4xOgNnSc4	první
kvašení	kvašení	k1gNnSc4	kvašení
v	v	k7c6	v
kádích	káď	k1gFnPc6	káď
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
v	v	k7c6	v
dubových	dubový	k2eAgInPc6d1	dubový
sudech	sud	k1gInPc6	sud
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
scelování	scelování	k1gNnSc1	scelování
a	a	k8xC	a
stáčení	stáčení	k1gNnSc1	stáčení
vína	víno	k1gNnSc2	víno
do	do	k7c2	do
lahví	lahev	k1gFnPc2	lahev
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
tirážní	tirážní	k2eAgInSc1d1	tirážní
likér	likér	k1gInSc1	likér
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
směs	směs	k1gFnSc4	směs
třtinového	třtinový	k2eAgInSc2d1	třtinový
cukru	cukr	k1gInSc2	cukr
<g/>
,	,	kIx,	,
kvasinkové	kvasinkový	k2eAgFnSc2d1	kvasinková
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
"	"	kIx"	"
<g/>
starého	starý	k2eAgNnSc2d1	staré
<g/>
"	"	kIx"	"
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
Láhev	láhev	k1gFnSc1	láhev
se	se	k3xPyFc4	se
uzavře	uzavřít	k5eAaPmIp3nS	uzavřít
korunkovým	korunkový	k2eAgInSc7d1	korunkový
uzávěrem	uzávěr	k1gInSc7	uzávěr
a	a	k8xC	a
uloží	uložit	k5eAaPmIp3nS	uložit
do	do	k7c2	do
vodorovné	vodorovný	k2eAgFnSc2d1	vodorovná
polohy	poloha	k1gFnSc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Nastává	nastávat	k5eAaImIp3nS	nastávat
druhotné	druhotný	k2eAgNnSc4d1	druhotné
kvašení	kvašení	k1gNnSc4	kvašení
a	a	k8xC	a
vzniklý	vzniklý	k2eAgMnSc1d1	vzniklý
CO2	CO2	k1gMnSc1	CO2
proměňuje	proměňovat	k5eAaImIp3nS	proměňovat
tiché	tichý	k2eAgNnSc4d1	tiché
víno	víno	k1gNnSc4	víno
v	v	k7c4	v
šumivé	šumivý	k2eAgFnPc4d1	šumivá
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
nejméně	málo	k6eAd3	málo
9	[number]	k4	9
měsíců	měsíc	k1gInPc2	měsíc
následného	následný	k2eAgNnSc2d1	následné
vyzrávání	vyzrávání	k1gNnSc2	vyzrávání
vína	víno	k1gNnSc2	víno
před	před	k7c7	před
odstraněním	odstranění	k1gNnSc7	odstranění
sedimentů	sediment	k1gInPc2	sediment
<g/>
.	.	kIx.	.
</s>
<s>
Odumírající	odumírající	k2eAgFnPc1d1	odumírající
kvasinky	kvasinka	k1gFnPc1	kvasinka
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
a	a	k8xC	a
obohacují	obohacovat	k5eAaImIp3nP	obohacovat
víno	víno	k1gNnSc4	víno
o	o	k7c4	o
specifickou	specifický	k2eAgFnSc4d1	specifická
chuť	chuť	k1gFnSc4	chuť
i	i	k8xC	i
vůni	vůně	k1gFnSc4	vůně
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholná	vrcholný	k2eAgFnSc1d1	vrcholná
fáze	fáze	k1gFnSc1	fáze
výroby	výroba	k1gFnSc2	výroba
představuje	představovat	k5eAaImIp3nS	představovat
setřásání	setřásání	k1gNnSc1	setřásání
sedliny	sedlina	k1gFnSc2	sedlina
na	na	k7c4	na
zátku	zátka	k1gFnSc4	zátka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
lahve	lahev	k1gFnPc1	lahev
jsou	být	k5eAaImIp3nP	být
obrácené	obrácený	k2eAgFnPc1d1	obrácená
zátkou	zátka	k1gFnSc7	zátka
dolů	dolů	k6eAd1	dolů
<g/>
,	,	kIx,	,
nakloněné	nakloněný	k2eAgFnPc1d1	nakloněná
a	a	k8xC	a
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
přetřásají	přetřásat	k5eAaImIp3nP	přetřásat
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
jsou	být	k5eAaImIp3nP	být
hrdla	hrdlo	k1gNnPc4	hrdlo
lahví	lahev	k1gFnPc2	lahev
ochlazena	ochlazen	k2eAgFnSc1d1	ochlazena
<g/>
,	,	kIx,	,
po	po	k7c6	po
otevření	otevření	k1gNnSc6	otevření
láhve	láhev	k1gFnSc2	láhev
vystřelí	vystřelit	k5eAaPmIp3nP	vystřelit
zmrzlé	zmrzlý	k2eAgInPc1d1	zmrzlý
sedimenty	sediment	k1gInPc1	sediment
ven	ven	k6eAd1	ven
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
končí	končit	k5eAaImIp3nS	končit
fáze	fáze	k1gFnPc4	fáze
odkalení	odkalení	k1gNnSc2	odkalení
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
láhve	láhev	k1gFnSc2	láhev
se	se	k3xPyFc4	se
doplní	doplnit	k5eAaPmIp3nS	doplnit
buď	buď	k8xC	buď
suchým	suchý	k2eAgNnSc7d1	suché
vínem	víno	k1gNnSc7	víno
–	–	k?	–
tak	tak	k8xS	tak
vznikají	vznikat	k5eAaImIp3nP	vznikat
vyhlášená	vyhlášený	k2eAgNnPc4d1	vyhlášené
šampaňská	šampaňský	k2eAgNnPc4d1	šampaňské
vína	víno	k1gNnPc4	víno
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
brut	bruta	k1gFnPc2	bruta
nature	natur	k1gMnSc5	natur
bez	bez	k7c2	bez
přidaného	přidaný	k2eAgInSc2d1	přidaný
cukru	cukr	k1gInSc2	cukr
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
doplní	doplnit	k5eAaPmIp3nS	doplnit
expedičním	expediční	k2eAgInSc7d1	expediční
likérem	likér	k1gInSc7	likér
–	–	k?	–
směsí	směs	k1gFnPc2	směs
vína	víno	k1gNnSc2	víno
a	a	k8xC	a
cukru	cukr	k1gInSc2	cukr
na	na	k7c6	na
škále	škála	k1gFnSc6	škála
extra	extra	k6eAd1	extra
brut	brut	k5eAaPmF	brut
až	až	k9	až
doux	doux	k1gInSc4	doux
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
reguluje	regulovat	k5eAaImIp3nS	regulovat
sladkost	sladkost	k1gFnSc1	sladkost
výsledného	výsledný	k2eAgNnSc2d1	výsledné
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
Lahve	lahev	k1gFnPc1	lahev
jsou	být	k5eAaImIp3nP	být
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
korkovou	korkový	k2eAgFnSc7d1	korková
zátkou	zátka	k1gFnSc7	zátka
a	a	k8xC	a
zabezpečeny	zabezpečen	k2eAgMnPc4d1	zabezpečen
drátěným	drátěný	k2eAgInSc7d1	drátěný
košíčkem	košíček	k1gInSc7	košíček
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ostatních	ostatní	k2eAgNnPc2d1	ostatní
šumivých	šumivý	k2eAgNnPc2d1	šumivé
vín	víno	k1gNnPc2	víno
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
vyrobených	vyrobený	k2eAgInPc2d1	vyrobený
týmž	týž	k3xTgInSc7	týž
postupem	postup	k1gInSc7	postup
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
povoleno	povolen	k2eAgNnSc1d1	povoleno
používat	používat	k5eAaImF	používat
označení	označení	k1gNnSc4	označení
méthode	méthod	k1gInSc5	méthod
traditionnelle	traditionnell	k1gMnSc4	traditionnell
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
označení	označení	k1gNnSc4	označení
šampaňského	šampaňské	k1gNnSc2	šampaňské
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
jen	jen	k9	jen
vína	víno	k1gNnPc4	víno
vyrobená	vyrobený	k2eAgNnPc4d1	vyrobené
v	v	k7c6	v
Champagne	Champagn	k1gInSc5	Champagn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
metodou	metoda	k1gFnSc7	metoda
je	být	k5eAaImIp3nS	být
kvašení	kvašení	k1gNnSc4	kvašení
v	v	k7c6	v
tanku	tank	k1gInSc6	tank
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
charmat	charmat	k1gInSc1	charmat
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
výrobních	výrobní	k2eAgInPc2d1	výrobní
nákladů	náklad	k1gInPc2	náklad
značně	značně	k6eAd1	značně
levnější	levný	k2eAgMnSc1d2	levnější
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
větší	veliký	k2eAgInSc4d2	veliký
objem	objem	k1gInSc4	objem
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Charmat-Martinotti	Charmat-Martinotť	k1gFnPc1	Charmat-Martinotť
nebo	nebo	k8xC	nebo
také	také	k9	také
Charmatova	Charmatův	k2eAgFnSc1d1	Charmatova
metoda	metoda	k1gFnSc1	metoda
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejrozšířenějších	rozšířený	k2eAgFnPc2d3	nejrozšířenější
a	a	k8xC	a
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
lehká	lehký	k2eAgFnSc1d1	lehká
<g/>
,	,	kIx,	,
jemná	jemný	k2eAgFnSc1d1	jemná
šumivá	šumivý	k2eAgFnSc1d1	šumivá
vína	vína	k1gFnSc1	vína
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
principu	princip	k1gInSc6	princip
druhotného	druhotný	k2eAgNnSc2d1	druhotné
kvašení	kvašení	k1gNnSc2	kvašení
v	v	k7c6	v
nerezových	rezový	k2eNgInPc6d1	nerezový
ocelových	ocelový	k2eAgInPc6d1	ocelový
tancích	tanec	k1gInPc6	tanec
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
kapalina	kapalina	k1gFnSc1	kapalina
stáčí	stáčet	k5eAaImIp3nS	stáčet
do	do	k7c2	do
lahví	lahev	k1gFnPc2	lahev
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
rychlá	rychlý	k2eAgFnSc1d1	rychlá
a	a	k8xC	a
trvá	trvat	k5eAaImIp3nS	trvat
"	"	kIx"	"
<g/>
jen	jen	k9	jen
<g/>
"	"	kIx"	"
několik	několik	k4yIc1	několik
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
především	především	k9	především
odrůdy	odrůda	k1gFnPc1	odrůda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
tak	tak	k6eAd1	tak
drahé	drahý	k2eAgInPc1d1	drahý
jako	jako	k8xC	jako
Pinot	Pinot	k1gInSc1	Pinot
noir	noira	k1gFnPc2	noira
nebo	nebo	k8xC	nebo
Chardonnay	Chardonnaa	k1gFnSc2	Chardonnaa
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
se	se	k3xPyFc4	se
v	v	k7c6	v
uzavřených	uzavřený	k2eAgFnPc6d1	uzavřená
nádobách	nádoba	k1gFnPc6	nádoba
do	do	k7c2	do
přetlaku	přetlak	k1gInSc2	přetlak
0,25	[number]	k4	0,25
MPa	MPa	k1gFnPc2	MPa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
nejznámější	známý	k2eAgInSc1d3	nejznámější
a	a	k8xC	a
nejprodávanější	prodávaný	k2eAgInSc1d3	nejprodávanější
sekt	sekt	k1gInSc1	sekt
vystupující	vystupující	k2eAgInSc1d1	vystupující
pod	pod	k7c7	pod
značkou	značka	k1gFnSc7	značka
Bohemia	bohemia	k1gFnSc1	bohemia
Sekt	sekt	k1gInSc1	sekt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
Plzenci	Plzenec	k1gInSc6	Plzenec
nedaleko	nedaleko	k7c2	nedaleko
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
výrobou	výroba	k1gFnSc7	výroba
sektu	sekt	k1gInSc2	sekt
zabývá	zabývat	k5eAaImIp3nS	zabývat
například	například	k6eAd1	například
vinařství	vinařství	k1gNnSc1	vinařství
Tanzberg	Tanzberg	k1gInSc1	Tanzberg
<g/>
.	.	kIx.	.
</s>
