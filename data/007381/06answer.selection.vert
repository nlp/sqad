<s>
Veltlínské	veltlínský	k2eAgNnSc1d1	Veltlínské
červené	červený	k2eAgNnSc1d1	červené
rané	raný	k2eAgNnSc1d1	rané
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
VČR	VČR	kA	VČR
<g/>
,	,	kIx,	,
lidový	lidový	k2eAgInSc4d1	lidový
název	název	k1gInSc4	název
Večerka	večerka	k1gFnSc1	večerka
<g/>
,	,	kIx,	,
název	název	k1gInSc1	název
dle	dle	k7c2	dle
VIVC	VIVC	kA	VIVC
Veltliner	Veltliner	k1gInSc1	Veltliner
Frührot	Frührot	k1gInSc1	Frührot
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stará	starý	k2eAgFnSc1d1	stará
odrůda	odrůda	k1gFnSc1	odrůda
révy	réva	k1gFnSc2	réva
vinné	vinný	k2eAgFnSc2d1	vinná
(	(	kIx(	(
<g/>
Vitis	Vitis	k1gFnSc2	Vitis
vinifera	vinifer	k1gMnSc2	vinifer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
určená	určený	k2eAgFnSc1d1	určená
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
bílých	bílý	k2eAgFnPc2d1	bílá
vín	vína	k1gFnPc2	vína
<g/>
.	.	kIx.	.
</s>
