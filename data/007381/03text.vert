<s>
Veltlínské	veltlínský	k2eAgNnSc1d1	Veltlínské
červené	červený	k2eAgNnSc1d1	červené
rané	raný	k2eAgNnSc1d1	rané
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
VČR	VČR	kA	VČR
<g/>
,	,	kIx,	,
lidový	lidový	k2eAgInSc4d1	lidový
název	název	k1gInSc4	název
Večerka	večerka	k1gFnSc1	večerka
<g/>
,	,	kIx,	,
název	název	k1gInSc1	název
dle	dle	k7c2	dle
VIVC	VIVC	kA	VIVC
Veltliner	Veltliner	k1gInSc1	Veltliner
Frührot	Frührot	k1gInSc1	Frührot
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stará	starý	k2eAgFnSc1d1	stará
odrůda	odrůda	k1gFnSc1	odrůda
révy	réva	k1gFnSc2	réva
vinné	vinný	k2eAgFnSc2d1	vinná
(	(	kIx(	(
<g/>
Vitis	Vitis	k1gFnSc2	Vitis
vinifera	vinifer	k1gMnSc2	vinifer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
určená	určený	k2eAgFnSc1d1	určená
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
bílých	bílý	k2eAgFnPc2d1	bílá
vín	vína	k1gFnPc2	vína
<g/>
.	.	kIx.	.
</s>
<s>
Autochtonní	Autochtonní	k2eAgFnSc1d1	Autochtonní
odrůda	odrůda	k1gFnSc1	odrůda
pochází	pocházet	k5eAaImIp3nS	pocházet
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
Dolního	dolní	k2eAgNnSc2d1	dolní
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
z	z	k7c2	z
vinařského	vinařský	k2eAgInSc2d1	vinařský
regionu	region	k1gInSc2	region
Thermenregion	Thermenregion	k1gInSc1	Thermenregion
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
pěstována	pěstován	k2eAgFnSc1d1	pěstována
zejména	zejména	k9	zejména
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
Gumpoldskirchen	Gumpoldskirchna	k1gFnPc2	Gumpoldskirchna
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
samovolným	samovolný	k2eAgNnSc7d1	samovolné
křížením	křížení	k1gNnSc7	křížení
odrůd	odrůda	k1gFnPc2	odrůda
Sylvánské	sylvánské	k1gNnSc4	sylvánské
zelené	zelený	k2eAgInPc4d1	zelený
a	a	k8xC	a
Veltlínské	veltlínský	k2eAgFnPc4d1	veltlínský
červené	červená	k1gFnPc4	červená
<g/>
.	.	kIx.	.
</s>
<s>
Réva	réva	k1gFnSc1	réva
vinná	vinný	k2eAgFnSc1d1	vinná
(	(	kIx(	(
<g/>
Vitis	Vitis	k1gFnSc1	Vitis
vinifera	vinifera	k1gFnSc1	vinifera
<g/>
)	)	kIx)	)
odrůda	odrůda	k1gFnSc1	odrůda
Veltlínské	veltlínský	k2eAgFnSc2d1	veltlínský
červené	červená	k1gFnSc2	červená
rané	raný	k2eAgFnSc2d1	raná
je	být	k5eAaImIp3nS	být
jednodomá	jednodomý	k2eAgFnSc1d1	jednodomá
dřevitá	dřevitý	k2eAgFnSc1d1	dřevitá
pnoucí	pnoucí	k2eAgFnSc1d1	pnoucí
liána	liána	k1gFnSc1	liána
<g/>
,	,	kIx,	,
dorůstající	dorůstající	k2eAgFnSc1d1	dorůstající
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
až	až	k9	až
několika	několik	k4yIc2	několik
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Kmen	kmen	k1gInSc1	kmen
tloušťky	tloušťka	k1gFnSc2	tloušťka
až	až	k9	až
několik	několik	k4yIc1	několik
centimetrů	centimetr	k1gInPc2	centimetr
je	být	k5eAaImIp3nS	být
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
světlou	světlý	k2eAgFnSc7d1	světlá
borkou	borka	k1gFnSc7	borka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
loupe	loupat	k5eAaImIp3nS	loupat
v	v	k7c6	v
pruzích	pruh	k1gInPc6	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Úponky	úponek	k1gInPc1	úponek
révy	réva	k1gFnSc2	réva
jsou	být	k5eAaImIp3nP	být
středně	středně	k6eAd1	středně
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
až	až	k8xS	až
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
této	tento	k3xDgFnSc3	tento
rostlině	rostlina	k1gFnSc3	rostlina
pnout	pnout	k5eAaImF	pnout
se	se	k3xPyFc4	se
po	po	k7c6	po
pevných	pevný	k2eAgInPc6d1	pevný
předmětech	předmět	k1gInPc6	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
bujný	bujný	k2eAgMnSc1d1	bujný
až	až	k8xS	až
bujný	bujný	k2eAgMnSc1d1	bujný
<g/>
,	,	kIx,	,
letorosty	letorost	k1gInPc1	letorost
jsou	být	k5eAaImIp3nP	být
silné	silný	k2eAgInPc1d1	silný
<g/>
,	,	kIx,	,
polovzpřímené	polovzpřímený	k2eAgInPc1d1	polovzpřímený
<g/>
.	.	kIx.	.
</s>
<s>
Včelka	včelka	k1gFnSc1	včelka
je	být	k5eAaImIp3nS	být
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
bělavě	bělavě	k6eAd1	bělavě
ochlupená	ochlupený	k2eAgFnSc1d1	ochlupená
s	s	k7c7	s
jemně	jemně	k6eAd1	jemně
načervenalými	načervenalý	k2eAgInPc7d1	načervenalý
okraji	okraj	k1gInPc7	okraj
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholek	vrcholek	k1gInSc1	vrcholek
letorostu	letorost	k1gInSc2	letorost
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
hustě	hustě	k6eAd1	hustě
až	až	k9	až
hustě	hustě	k6eAd1	hustě
bíle	bíle	k6eAd1	bíle
vlnatě	vlnatě	k6eAd1	vlnatě
ochmýřený	ochmýřený	k2eAgInSc1d1	ochmýřený
<g/>
,	,	kIx,	,
světle	světle	k6eAd1	světle
zelený	zelený	k2eAgInSc1d1	zelený
s	s	k7c7	s
vínově	vínově	k6eAd1	vínově
červeným	červený	k2eAgInSc7d1	červený
nádechem	nádech	k1gInSc7	nádech
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
<g/>
,	,	kIx,	,
středně	středně	k6eAd1	středně
silně	silně	k6eAd1	silně
pigmentovaný	pigmentovaný	k2eAgInSc1d1	pigmentovaný
antokyaniny	antokyanin	k2eAgInPc1d1	antokyanin
<g/>
.	.	kIx.	.
</s>
<s>
Internodia	internodium	k1gNnPc1	internodium
a	a	k8xC	a
nodia	nodium	k1gNnPc1	nodium
jsou	být	k5eAaImIp3nP	být
zelenovínově	zelenovínově	k6eAd1	zelenovínově
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
s	s	k7c7	s
hnědým	hnědý	k2eAgInSc7d1	hnědý
odstínem	odstín	k1gInSc7	odstín
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ochmýření	ochmýření	k1gNnSc2	ochmýření
<g/>
,	,	kIx,	,
pupeny	pupen	k1gInPc1	pupen
jsou	být	k5eAaImIp3nP	být
silně	silně	k6eAd1	silně
pigmentované	pigmentovaný	k2eAgFnPc4d1	pigmentovaná
antokyaniny	antokyanina	k1gFnPc4	antokyanina
<g/>
.	.	kIx.	.
mladé	mladý	k2eAgInPc4d1	mladý
lístky	lístek	k1gInPc4	lístek
jsou	být	k5eAaImIp3nP	být
zelené	zelený	k2eAgInPc1d1	zelený
s	s	k7c7	s
bronzovými	bronzový	k2eAgFnPc7d1	bronzová
skvrnami	skvrna	k1gFnPc7	skvrna
<g/>
,	,	kIx,	,
středně	středně	k6eAd1	středně
silně	silně	k6eAd1	silně
pigmentované	pigmentovaný	k2eAgFnPc4d1	pigmentovaná
antokyaniny	antokyanina	k1gFnPc4	antokyanina
<g/>
,	,	kIx,	,
slabě	slabě	k6eAd1	slabě
pavučinovitě	pavučinovitě	k6eAd1	pavučinovitě
ochmýřené	ochmýřený	k2eAgNnSc1d1	ochmýřené
<g/>
.	.	kIx.	.
</s>
<s>
Jednoleté	jednoletý	k2eAgNnSc1d1	jednoleté
réví	réví	k1gNnSc1	réví
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
tenké	tenký	k2eAgInPc1d1	tenký
<g/>
,	,	kIx,	,
tmavohnědé	tmavohnědý	k2eAgInPc1d1	tmavohnědý
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
fialovým	fialový	k2eAgInSc7d1	fialový
nádechem	nádech	k1gInSc7	nádech
<g/>
,	,	kIx,	,
po	po	k7c6	po
délce	délka	k1gFnSc6	délka
čárkované	čárkovaný	k2eAgFnSc6d1	čárkovaná
<g/>
,	,	kIx,	,
eliptického	eliptický	k2eAgInSc2d1	eliptický
průřezu	průřez	k1gInSc2	průřez
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
špatně	špatně	k6eAd1	špatně
vyzrává	vyzrávat	k5eAaImIp3nS	vyzrávat
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgInPc1d1	zimní
pupeny	pupen	k1gInPc1	pupen
jsou	být	k5eAaImIp3nP	být
středně	středně	k6eAd1	středně
velké	velký	k2eAgInPc1d1	velký
<g/>
,	,	kIx,	,
zahrocené	zahrocený	k2eAgInPc1d1	zahrocený
<g/>
.	.	kIx.	.
</s>
<s>
List	list	k1gInSc1	list
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgInSc1d1	velký
až	až	k8xS	až
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
pětilaločnatý	pětilaločnatý	k2eAgInSc1d1	pětilaločnatý
se	s	k7c7	s
středně	středně	k6eAd1	středně
hlubokými	hluboký	k2eAgInPc7d1	hluboký
<g/>
,	,	kIx,	,
výraznými	výrazný	k2eAgInPc7d1	výrazný
<g/>
,	,	kIx,	,
lyrovitými	lyrovitý	k2eAgInPc7d1	lyrovitý
výkroji	výkroj	k1gInPc7	výkroj
s	s	k7c7	s
oblým	oblý	k2eAgInSc7d1	oblý
až	až	k8xS	až
plochým	plochý	k2eAgInSc7d1	plochý
dnem	den	k1gInSc7	den
<g/>
,	,	kIx,	,
světle	světle	k6eAd1	světle
zelené	zelený	k2eAgFnPc4d1	zelená
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
na	na	k7c6	na
svrchní	svrchní	k2eAgFnSc6d1	svrchní
straně	strana	k1gFnSc6	strana
průměrně	průměrně	k6eAd1	průměrně
silně	silně	k6eAd1	silně
puchýřnatý	puchýřnatý	k2eAgInSc1d1	puchýřnatý
<g/>
,	,	kIx,	,
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
straně	strana	k1gFnSc6	strana
hladký	hladký	k2eAgInSc1d1	hladký
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
jemně	jemně	k6eAd1	jemně
plstnatý	plstnatý	k2eAgInSc1d1	plstnatý
<g/>
.	.	kIx.	.
</s>
<s>
Řapíkový	řapíkový	k2eAgInSc1d1	řapíkový
výkroj	výkroj	k1gInSc1	výkroj
je	být	k5eAaImIp3nS	být
lyrovitý	lyrovitý	k2eAgInSc1d1	lyrovitý
s	s	k7c7	s
ostrým	ostrý	k2eAgInSc7d1	ostrý
dnem	den	k1gInSc7	den
až	až	k9	až
šípovitý	šípovitý	k2eAgMnSc1d1	šípovitý
<g/>
,	,	kIx,	,
úzce	úzko	k6eAd1	úzko
otevřený	otevřený	k2eAgInSc1d1	otevřený
až	až	k6eAd1	až
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
s	s	k7c7	s
průsvitem	průsvit	k1gInSc7	průsvit
<g/>
,	,	kIx,	,
řapík	řapík	k1gInSc1	řapík
je	být	k5eAaImIp3nS	být
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
kratší	krátký	k2eAgInSc1d2	kratší
než	než	k8xS	než
medián	medián	k1gInSc1	medián
listu	list	k1gInSc2	list
<g/>
,	,	kIx,	,
narůžovělý	narůžovělý	k2eAgInSc1d1	narůžovělý
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
žilnatina	žilnatina	k1gFnSc1	žilnatina
listu	list	k1gInSc2	list
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
napojení	napojení	k1gNnSc2	napojení
řapíku	řapík	k1gInSc2	řapík
<g/>
.	.	kIx.	.
</s>
<s>
Podzimní	podzimní	k2eAgFnSc1d1	podzimní
barva	barva	k1gFnSc1	barva
listů	list	k1gInPc2	list
je	být	k5eAaImIp3nS	být
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
.	.	kIx.	.
</s>
<s>
Oboupohlavní	oboupohlavní	k2eAgInPc4d1	oboupohlavní
pětičetné	pětičetný	k2eAgInPc4d1	pětičetný
květy	květ	k1gInPc4	květ
v	v	k7c6	v
hroznovitých	hroznovitý	k2eAgNnPc6d1	hroznovité
květenstvích	květenství	k1gNnPc6	květenství
jsou	být	k5eAaImIp3nP	být
žlutozelené	žlutozelený	k2eAgInPc1d1	žlutozelený
<g/>
,	,	kIx,	,
samosprašné	samosprašný	k2eAgInPc1d1	samosprašný
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgInPc1d1	malý
až	až	k9	až
středně	středně	k6eAd1	středně
velké	velký	k2eAgNnSc1d1	velké
(	(	kIx(	(
<g/>
13	[number]	k4	13
x	x	k?	x
12	[number]	k4	12
mm	mm	kA	mm
<g/>
,	,	kIx,	,
1,34	[number]	k4	1,34
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vejčité	vejčitý	k2eAgInPc1d1	vejčitý
až	až	k9	až
protáhle	protáhle	k6eAd1	protáhle
eliptické	eliptický	k2eAgFnPc4d1	eliptická
bobule	bobule	k1gFnPc4	bobule
<g/>
,	,	kIx,	,
náchylné	náchylný	k2eAgNnSc4d1	náchylné
k	k	k7c3	k
hnilobě	hniloba	k1gFnSc3	hniloba
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
mají	mít	k5eAaImIp3nP	mít
růžovou	růžový	k2eAgFnSc4d1	růžová
až	až	k9	až
světle	světle	k6eAd1	světle
fialovočervenou	fialovočervený	k2eAgFnSc4d1	fialovočervená
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
středně	středně	k6eAd1	středně
pevnou	pevný	k2eAgFnSc4d1	pevná
<g/>
,	,	kIx,	,
průměrně	průměrně	k6eAd1	průměrně
voskovitě	voskovitě	k6eAd1	voskovitě
ojíněnou	ojíněný	k2eAgFnSc4d1	ojíněná
slupku	slupka	k1gFnSc4	slupka
a	a	k8xC	a
nezbarvenou	zbarvený	k2eNgFnSc4d1	nezbarvená
<g/>
,	,	kIx,	,
rozplývavou	rozplývavý	k2eAgFnSc4d1	rozplývavá
<g/>
,	,	kIx,	,
šťavnatou	šťavnatý	k2eAgFnSc4d1	šťavnatá
dužinu	dužina	k1gFnSc4	dužina
sladké	sladký	k2eAgFnPc1d1	sladká
<g/>
,	,	kIx,	,
mírně	mírně	k6eAd1	mírně
kořenité	kořenitý	k2eAgFnSc2d1	kořenitá
chuti	chuť	k1gFnSc2	chuť
a	a	k8xC	a
nízké	nízký	k2eAgFnSc2d1	nízká
acidity	acidita	k1gFnSc2	acidita
<g/>
.	.	kIx.	.
</s>
<s>
Bobule	bobule	k1gFnPc1	bobule
jsou	být	k5eAaImIp3nP	být
nejednotné	jednotný	k2eNgFnPc4d1	nejednotná
velikosti	velikost	k1gFnPc4	velikost
a	a	k8xC	a
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
stopečka	stopečka	k1gFnSc1	stopečka
je	být	k5eAaImIp3nS	být
krátká	krátký	k2eAgFnSc1d1	krátká
<g/>
,	,	kIx,	,
obtížně	obtížně	k6eAd1	obtížně
oddělitelná	oddělitelný	k2eAgFnSc1d1	oddělitelná
<g/>
.	.	kIx.	.
</s>
<s>
Semeno	semeno	k1gNnSc1	semeno
je	být	k5eAaImIp3nS	být
kulovité	kulovitý	k2eAgNnSc1d1	kulovité
až	až	k6eAd1	až
elipsoidní	elipsoidní	k2eAgNnSc1d1	elipsoidní
<g/>
,	,	kIx,	,
středně	středně	k6eAd1	středně
velké	velký	k2eAgInPc1d1	velký
<g/>
,	,	kIx,	,
zobáček	zobáček	k1gInSc1	zobáček
je	být	k5eAaImIp3nS	být
krátký	krátký	k2eAgInSc1d1	krátký
<g/>
,	,	kIx,	,
tupý	tupý	k2eAgInSc1d1	tupý
<g/>
.	.	kIx.	.
</s>
<s>
Hrozen	hrozen	k1gInSc1	hrozen
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgMnSc1d1	velký
až	až	k8xS	až
velký	velký	k2eAgMnSc1d1	velký
(	(	kIx(	(
<g/>
120	[number]	k4	120
mm	mm	kA	mm
<g/>
,	,	kIx,	,
110-160	[number]	k4	110-160
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kratší	krátký	k2eAgFnSc1d2	kratší
<g/>
,	,	kIx,	,
kompaktní	kompaktní	k2eAgFnSc1d1	kompaktní
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
hustý	hustý	k2eAgInSc1d1	hustý
<g/>
,	,	kIx,	,
válcovitě-kuželovitý	válcovitěuželovitý	k2eAgInSc1d1	válcovitě-kuželovitý
<g/>
,	,	kIx,	,
s	s	k7c7	s
krátkou	krátká	k1gFnSc7	krátká
<g/>
,	,	kIx,	,
průměrně	průměrně	k6eAd1	průměrně
lignifikovanou	lignifikovaný	k2eAgFnSc7d1	lignifikovaný
stopkou	stopka	k1gFnSc7	stopka
<g/>
.	.	kIx.	.
</s>
<s>
Veltlínské	veltlínský	k2eAgFnSc2d1	veltlínský
červené	červená	k1gFnSc2	červená
rané	raný	k2eAgFnSc2d1	raná
je	být	k5eAaImIp3nS	být
raná	raný	k2eAgFnSc1d1	raná
až	až	k9	až
středně	středně	k6eAd1	středně
pozdní	pozdní	k2eAgInPc1d1	pozdní
<g/>
,	,	kIx,	,
moštová	moštový	k2eAgNnPc1d1	moštový
a	a	k8xC	a
místy	místo	k1gNnPc7	místo
i	i	k8xC	i
stolní	stolní	k2eAgFnSc1d1	stolní
odrůda	odrůda	k1gFnSc1	odrůda
révy	réva	k1gFnSc2	réva
vinné	vinný	k2eAgFnSc2d1	vinná
(	(	kIx(	(
<g/>
Vitis	Vitis	k1gFnSc2	Vitis
vinifera	vinifer	k1gMnSc2	vinifer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Genetické	genetický	k2eAgFnPc1d1	genetická
studie	studie	k1gFnPc1	studie
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
blízce	blízce	k6eAd1	blízce
příbuzné	příbuzný	k2eAgFnSc2d1	příbuzná
odrůdy	odrůda	k1gFnSc2	odrůda
Neuburské	Neuburský	k2eAgFnSc2d1	Neuburský
<g/>
,	,	kIx,	,
o	o	k7c4	o
spontánní	spontánní	k2eAgNnSc4d1	spontánní
křížení	křížení	k1gNnSc4	křížení
odrůd	odrůda	k1gFnPc2	odrůda
Sylvánské	sylvánské	k1gNnSc4	sylvánské
zelené	zelený	k2eAgInPc4d1	zelený
a	a	k8xC	a
Veltlínské	veltlínský	k2eAgFnPc4d1	veltlínský
červené	červená	k1gFnPc4	červená
<g/>
,	,	kIx,	,
u	u	k7c2	u
odrůdy	odrůda	k1gFnSc2	odrůda
Neuburské	Neuburský	k2eAgNnSc1d1	Neuburské
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
mateřskou	mateřský	k2eAgFnSc7d1	mateřská
odrůdou	odrůda	k1gFnSc7	odrůda
Veltlínské	veltlínský	k2eAgFnSc2d1	veltlínský
červené	červená	k1gFnSc2	červená
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
starších	starý	k2eAgInPc2d2	starší
zdrojů	zdroj	k1gInPc2	zdroj
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
odrůda	odrůda	k1gFnSc1	odrůda
pocházet	pocházet	k5eAaImF	pocházet
ze	z	k7c2	z
severoitalského	severoitalský	k2eAgNnSc2d1	severoitalské
údolí	údolí	k1gNnSc2	údolí
Valtelina	Valtelin	k2eAgFnSc1d1	Valtelin
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
nepěstuje	pěstovat	k5eNaImIp3nS	pěstovat
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
soudí	soudit	k5eAaImIp3nS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
pouze	pouze	k6eAd1	pouze
název	název	k1gInSc1	název
celé	celý	k2eAgFnSc2d1	celá
skupiny	skupina	k1gFnSc2	skupina
odrůd	odrůda	k1gFnPc2	odrůda
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
místo	místo	k1gNnSc4	místo
původu	původ	k1gInSc2	původ
odrůdy	odrůda	k1gFnSc2	odrůda
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
označováno	označován	k2eAgNnSc1d1	označováno
Dolní	dolní	k2eAgNnSc1d1	dolní
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
okolí	okolí	k1gNnSc1	okolí
města	město	k1gNnSc2	město
Gumpodskirchen	Gumpodskirchna	k1gFnPc2	Gumpodskirchna
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Thermenregion	Thermenregion	k1gInSc4	Thermenregion
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
Dolním	dolní	k2eAgNnSc6d1	dolní
Rakousku	Rakousko	k1gNnSc6	Rakousko
pod	pod	k7c7	pod
názvy	název	k1gInPc7	název
Frühroter	Frühroter	k1gMnSc1	Frühroter
Veltliner	Veltliner	k1gMnSc1	Veltliner
či	či	k8xC	či
Malvasier	Malvasier	k1gMnSc1	Malvasier
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
pěstována	pěstován	k2eAgFnSc1d1	pěstována
na	na	k7c4	na
424	[number]	k4	424
hektarech	hektar	k1gInPc6	hektar
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
asi	asi	k9	asi
na	na	k7c4	na
1	[number]	k4	1
%	%	kIx~	%
plochy	plocha	k1gFnSc2	plocha
vinic	vinice	k1gFnPc2	vinice
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
Thermenregion	Thermenregion	k1gInSc4	Thermenregion
ještě	ještě	k6eAd1	ještě
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
oblasti	oblast	k1gFnSc2	oblast
Weinviertel	Weinviertela	k1gFnPc2	Weinviertela
a	a	k8xC	a
v	v	k7c6	v
regionech	region	k1gInPc6	region
Kamptal	Kamptal	k1gMnSc1	Kamptal
a	a	k8xC	a
Kremstal	Kremstal	k1gMnSc1	Kremstal
<g/>
.	.	kIx.	.
</s>
<s>
Hojná	hojný	k2eAgFnSc1d1	hojná
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Wagramu	Wagram	k1gInSc2	Wagram
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
odrůdu	odrůda	k1gFnSc4	odrůda
najdeme	najít	k5eAaPmIp1nP	najít
na	na	k7c6	na
malých	malý	k2eAgFnPc6d1	malá
plochách	plocha	k1gFnPc6	plocha
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
v	v	k7c4	v
Německo	Německo	k1gNnSc4	Německo
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
na	na	k7c4	na
6	[number]	k4	6
ha	ha	kA	ha
<g/>
,	,	kIx,	,
region	region	k1gInSc4	region
Rheinhessen	Rheinhessna	k1gFnPc2	Rheinhessna
<g/>
,	,	kIx,	,
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
je	být	k5eAaImIp3nS	být
poprvé	poprvé	k6eAd1	poprvé
zmíněna	zmínit	k5eAaPmNgFnS	zmínit
již	již	k9	již
roku	rok	k1gInSc2	rok
1521	[number]	k4	1521
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
(	(	kIx(	(
<g/>
roku	rok	k1gInSc3	rok
2008	[number]	k4	2008
na	na	k7c4	na
4	[number]	k4	4
ha	ha	kA	ha
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
zde	zde	k6eAd1	zde
součást	součást	k1gFnSc1	součást
vín	vína	k1gFnPc2	vína
AOC	AOC	kA	AOC
Vin	vina	k1gFnPc2	vina
de	de	k?	de
Savoie	Savoie	k1gFnSc2	Savoie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
(	(	kIx(	(
<g/>
okolí	okolí	k1gNnSc6	okolí
Šoproně	Šoproň	k1gFnSc2	Šoproň
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
(	(	kIx(	(
<g/>
tvoří	tvořit	k5eAaImIp3nP	tvořit
zde	zde	k6eAd1	zde
2	[number]	k4	2
%	%	kIx~	%
plochy	plocha	k1gFnSc2	plocha
vinic	vinice	k1gFnPc2	vinice
<g/>
,	,	kIx,	,
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
v	v	k7c6	v
Nitrianské	Nitrianský	k2eAgFnSc6d1	Nitrianská
a	a	k8xC	a
Malokarpatské	malokarpatský	k2eAgFnSc6d1	Malokarpatská
oblasti	oblast	k1gFnSc6	oblast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětově	celosvětově	k6eAd1	celosvětově
nepřesahují	přesahovat	k5eNaImIp3nP	přesahovat
plochy	plocha	k1gFnPc4	plocha
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
odrůdou	odrůda	k1gFnSc7	odrůda
1.000	[number]	k4	1.000
hektarů	hektar	k1gInPc2	hektar
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Státní	státní	k2eAgFnSc6d1	státní
odrůdové	odrůdový	k2eAgFnSc6d1	odrůdová
knize	kniha	k1gFnSc6	kniha
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
odrůda	odrůda	k1gFnSc1	odrůda
zapsána	zapsat	k5eAaPmNgFnS	zapsat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
registrována	registrován	k2eAgFnSc1d1	registrována
i	i	k9	i
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
ČR	ČR	kA	ČR
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
pět	pět	k4xCc1	pět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
vybraných	vybraný	k2eAgInPc2d1	vybraný
a	a	k8xC	a
povolených	povolený	k2eAgInPc2d1	povolený
klonů	klon	k1gInPc2	klon
:	:	kIx,	:
PO-	PO-	k1gFnPc2	PO-
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
PO-	PO-	k1gFnSc1	PO-
<g/>
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
<g/>
,	,	kIx,	,
PO-	PO-	k1gFnSc1	PO-
<g/>
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
<g/>
,	,	kIx,	,
PO-	PO-	k1gFnSc1	PO-
<g/>
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
30	[number]	k4	30
a	a	k8xC	a
PO-	PO-	k1gMnSc3	PO-
<g/>
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
rozšířena	rozšířen	k2eAgFnSc1d1	rozšířena
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
v	v	k7c6	v
Mikulovské	mikulovský	k2eAgFnSc6d1	Mikulovská
a	a	k8xC	a
Znojemské	znojemský	k2eAgFnSc6d1	Znojemská
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
Dolních	dolní	k2eAgFnPc6d1	dolní
Dunajovicích	Dunajovice	k1gFnPc6	Dunajovice
<g/>
,	,	kIx,	,
daří	dařit	k5eAaImIp3nS	dařit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
na	na	k7c6	na
písčitých	písčitý	k2eAgFnPc6d1	písčitá
půdách	půda	k1gFnPc6	půda
na	na	k7c6	na
Bzenecku	Bzeneck	k1gInSc6	Bzeneck
a	a	k8xC	a
na	na	k7c6	na
Strážnicku	Strážnick	k1gInSc6	Strážnick
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tvoří	tvořit	k5eAaImIp3nS	tvořit
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
odrůd	odrůda	k1gFnPc2	odrůda
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
tvořil	tvořit	k5eAaImAgInS	tvořit
v	v	k7c6	v
ČR	ČR	kA	ČR
podíl	podíl	k1gInSc4	podíl
vinic	vinice	k1gFnPc2	vinice
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
odrůdou	odrůda	k1gFnSc7	odrůda
3,5	[number]	k4	3,5
%	%	kIx~	%
a	a	k8xC	a
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
již	již	k6eAd1	již
pouze	pouze	k6eAd1	pouze
1,3	[number]	k4	1,3
%	%	kIx~	%
plochy	plocha	k1gFnPc1	plocha
veškerých	veškerý	k3xTgFnPc2	veškerý
vinic	vinice	k1gFnPc2	vinice
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
vysazena	vysadit	k5eAaPmNgFnS	vysadit
na	na	k7c4	na
281	[number]	k4	281
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
nově	nově	k6eAd1	nově
nevysazuje	vysazovat	k5eNaImIp3nS	vysazovat
a	a	k8xC	a
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
vinice	vinice	k1gFnPc4	vinice
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
odrůdou	odrůda	k1gFnSc7	odrůda
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
nejstarší	starý	k2eAgFnSc1d3	nejstarší
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
32	[number]	k4	32
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
našich	náš	k3xOp1gInPc2	náš
regionů	region	k1gInPc2	region
se	s	k7c7	s
VČR	VČR	kA	VČR
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jeho	jeho	k3xOp3gNnSc4	jeho
pěstování	pěstování	k1gNnSc4	pěstování
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
někdejší	někdejší	k2eAgMnSc1d1	někdejší
ředitel	ředitel	k1gMnSc1	ředitel
známé	známý	k2eAgFnSc2d1	známá
vinařské	vinařský	k2eAgFnSc2d1	vinařská
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Klosterneuburgu	Klosterneuburg	k1gInSc6	Klosterneuburg
u	u	k7c2	u
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
rakouský	rakouský	k2eAgMnSc1d1	rakouský
šlechtitel	šlechtitel	k1gMnSc1	šlechtitel
August	August	k1gMnSc1	August
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
von	von	k1gInSc4	von
Babo	baba	k1gFnSc5	baba
<g/>
.	.	kIx.	.
</s>
<s>
Odrůdě	odrůda	k1gFnSc3	odrůda
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
něho	on	k3xPp3gMnSc2	on
říkalo	říkat	k5eAaImAgNnS	říkat
"	"	kIx"	"
<g/>
Babovina	Babovina	k1gFnSc1	Babovina
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Babotraube	Babotraub	k1gInSc5	Babotraub
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vína	vína	k1gFnSc1	vína
(	(	kIx(	(
<g/>
nejenom	nejenom	k6eAd1	nejenom
<g/>
)	)	kIx)	)
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
odrůdy	odrůda	k1gFnSc2	odrůda
se	se	k3xPyFc4	se
k	k	k7c3	k
nám	my	k3xPp1nPc3	my
ovšem	ovšem	k9	ovšem
dovážela	dovážet	k5eAaImAgFnS	dovážet
již	již	k6eAd1	již
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
Malvasia	Malvasium	k1gNnSc2	Malvasium
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc4	ten
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vína	vína	k1gFnSc1	vína
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
zbytkem	zbytek	k1gInSc7	zbytek
cukru	cukr	k1gInSc2	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
památka	památka	k1gFnSc1	památka
po	po	k7c6	po
nich	on	k3xPp3gMnPc6	on
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
rčení	rčení	k1gNnSc1	rčení
"	"	kIx"	"
<g/>
sladké	sladký	k2eAgFnPc1d1	sladká
jako	jako	k8xS	jako
malvaz	malvaz	k1gInSc1	malvaz
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vína	vína	k1gFnSc1	vína
odrůdy	odrůda	k1gFnSc2	odrůda
Veltlínské	veltlínský	k2eAgNnSc4d1	Veltlínské
červené	červené	k1gNnSc4	červené
rané	raný	k2eAgNnSc4d1	rané
ostatně	ostatně	k6eAd1	ostatně
chválili	chválit	k5eAaImAgMnP	chválit
například	například	k6eAd1	například
i	i	k9	i
Martin	Martin	k1gMnSc1	Martin
Luther	Luthra	k1gFnPc2	Luthra
a	a	k8xC	a
Johann	Johanna	k1gFnPc2	Johanna
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Goethe	Goeth	k1gFnSc2	Goeth
<g/>
.	.	kIx.	.
</s>
<s>
Johann	Johann	k1gMnSc1	Johann
Bauhin	Bauhin	k1gMnSc1	Bauhin
mu	on	k3xPp3gMnSc3	on
říkal	říkat	k5eAaImAgMnS	říkat
rhaetica	rhaetica	k6eAd1	rhaetica
podle	podle	k7c2	podle
starověkého	starověký	k2eAgInSc2d1	starověký
kmene	kmen	k1gInSc2	kmen
Rétů	rét	k1gInPc2	rét
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Veltlínské	veltlínský	k2eAgNnSc1d1	Veltlínské
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
názvu	název	k1gInSc2	název
severoitalského	severoitalský	k2eAgNnSc2d1	severoitalské
alpského	alpský	k2eAgNnSc2d1	alpské
údolí	údolí	k1gNnSc2	údolí
Valtellina	Valtellina	k1gFnSc1	Valtellina
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Valteline	Valtelin	k1gInSc5	Valtelin
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Veltlin	Veltlin	k2eAgInSc1d1	Veltlin
<g/>
)	)	kIx)	)
v	v	k7c6	v
Lombardii	Lombardie	k1gFnSc6	Lombardie
<g/>
,	,	kIx,	,
poblíž	poblíž	k6eAd1	poblíž
hranic	hranice	k1gFnPc2	hranice
se	s	k7c7	s
Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
hypotéz	hypotéza	k1gFnPc2	hypotéza
by	by	kYmCp3nS	by
odsud	odsud	k6eAd1	odsud
měla	mít	k5eAaImAgFnS	mít
pocházet	pocházet	k5eAaImF	pocházet
odrůda	odrůda	k1gFnSc1	odrůda
Veltlínské	veltlínský	k2eAgFnSc2d1	veltlínský
červené	červená	k1gFnSc2	červená
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
"	"	kIx"	"
<g/>
zakladatelem	zakladatel	k1gMnSc7	zakladatel
<g/>
"	"	kIx"	"
rodiny	rodina	k1gFnSc2	rodina
odrůd	odrůda	k1gFnPc2	odrůda
Veltlínské	veltlínský	k2eAgFnPc4d1	veltlínský
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnPc4d1	ostatní
odrůdy	odrůda	k1gFnPc4	odrůda
této	tento	k3xDgFnSc2	tento
rodiny	rodina	k1gFnSc2	rodina
jsou	být	k5eAaImIp3nP	být
její	její	k3xOp3gFnPc4	její
mutace	mutace	k1gFnPc4	mutace
či	či	k8xC	či
kříženci	kříženec	k1gMnPc1	kříženec
této	tento	k3xDgFnSc2	tento
odrůdy	odrůda	k1gFnSc2	odrůda
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
<g/>
,	,	kIx,	,
lokálně	lokálně	k6eAd1	lokálně
používaná	používaný	k2eAgNnPc1d1	používané
synonyma	synonymum	k1gNnPc1	synonymum
odrůdy	odrůda	k1gFnSc2	odrůda
Veltlínské	veltlínský	k2eAgFnSc2d1	veltlínský
červené	červená	k1gFnSc2	červená
rané	raný	k2eAgFnSc2d1	raná
jsou	být	k5eAaImIp3nP	být
:	:	kIx,	:
Babotraube	Babotraub	k1gInSc5	Babotraub
<g/>
,	,	kIx,	,
Babovina	Babovina	k1gFnSc1	Babovina
<g/>
,	,	kIx,	,
Babův	Babův	k2eAgInSc1d1	Babův
Hrozen	hrozen	k1gInSc1	hrozen
<g/>
,	,	kIx,	,
Červený	červený	k2eAgInSc1d1	červený
cinyfál	cinyfál	k1gInSc1	cinyfál
<g/>
,	,	kIx,	,
Crvena	Crven	k2eAgFnSc1d1	Crvena
Babovina	Babovina	k1gFnSc1	Babovina
<g/>
,	,	kIx,	,
Eperizue	Eperizue	k1gFnSc1	Eperizue
<g/>
,	,	kIx,	,
Eperpiros	Eperpirosa	k1gFnPc2	Eperpirosa
<g/>
,	,	kIx,	,
Eperszölö	Eperszölö	k1gFnPc2	Eperszölö
<g/>
,	,	kIx,	,
Feldinger	Feldingra	k1gFnPc2	Feldingra
<g/>
,	,	kIx,	,
Früher	Frühra	k1gFnPc2	Frühra
Roter	Rotra	k1gFnPc2	Rotra
Malvasier	Malvasier	k1gInSc1	Malvasier
(	(	kIx(	(
<g/>
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
F.	F.	kA	F.
R.	R.	kA	R.
Veltliner	Veltliner	k1gMnSc1	Veltliner
<g/>
,	,	kIx,	,
Frührot	Frührot	k1gMnSc1	Frührot
<g/>
,	,	kIx,	,
Frühroter	Frühroter	k1gMnSc1	Frühroter
Malvasier	Malvasier	k1gMnSc1	Malvasier
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Veltliner	Veltliner	k1gInSc1	Veltliner
(	(	kIx(	(
<g/>
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hartheunisch	Hartheunisch	k1gInSc4	Hartheunisch
Rot	rota	k1gFnPc2	rota
<g/>
,	,	kIx,	,
Hellroter	Hellrotra	k1gFnPc2	Hellrotra
Velteliner	Veltelinra	k1gFnPc2	Veltelinra
<g/>
,	,	kIx,	,
Italienischer	Italienischra	k1gFnPc2	Italienischra
Malvasier	Malvasira	k1gFnPc2	Malvasira
<g/>
,	,	kIx,	,
I.	I.	kA	I.
Rother	Rothra	k1gFnPc2	Rothra
Malvasier	Malvasira	k1gFnPc2	Malvasira
<g/>
,	,	kIx,	,
Kalebstraube	Kalebstraub	k1gInSc5	Kalebstraub
<g/>
,	,	kIx,	,
Kis	Kis	k1gMnPc1	Kis
Veltelini	Veltelin	k2eAgMnPc1d1	Veltelin
<g/>
,	,	kIx,	,
Korai	Kora	k1gMnPc1	Kora
piros	piros	k1gInSc4	piros
veltelini	veltelin	k2eAgMnPc1d1	veltelin
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Mährer	Mährer	k1gMnSc1	Mährer
Roter	Roter	k1gMnSc1	Roter
<g/>
,	,	kIx,	,
Malvasia	Malvasia	k1gFnSc1	Malvasia
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Nera	Nero	k1gMnSc2	Nero
Precoce	Precoce	k1gMnSc2	Precoce
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Rossa	Rossa	k1gFnSc1	Rossa
<g/>
,	,	kIx,	,
Malvasie	Malvasie	k1gFnSc1	Malvasie
Rouge	rouge	k1gFnSc1	rouge
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Italie	Italie	k1gFnSc1	Italie
<g/>
,	,	kIx,	,
Malvasier	Malvasier	k1gInSc1	Malvasier
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Frührot	Frührot	k1gInSc1	Frührot
(	(	kIx(	(
<g/>
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Italienisch	Italienisch	k1gInSc1	Italienisch
<g/>
,	,	kIx,	,
Malvazia	Malvazia	k1gFnSc1	Malvazia
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Krasnaja	Krasnaj	k1gInSc2	Krasnaj
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
Rozovaja	Rozovaja	k1gFnSc1	Rozovaja
<g/>
,	,	kIx,	,
Malvazinka	malvazinka	k1gFnSc1	malvazinka
<g/>
,	,	kIx,	,
Malvazija	Malvazij	k2eAgFnSc1d1	Malvazij
Krasna	Krasna	k1gFnSc1	Krasna
(	(	kIx(	(
<g/>
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Malvazské	Malvazský	k2eAgFnPc1d1	Malvazský
Rané	raný	k2eAgFnPc1d1	raná
<g/>
,	,	kIx,	,
Malvoisie	Malvoisie	k1gFnPc1	Malvoisie
de	de	k?	de
Lasseraz	Lasseraz	k1gInSc1	Lasseraz
<g/>
,	,	kIx,	,
M.	M.	kA	M.
du	du	k?	du
Chautagne	Chautagn	k1gMnSc5	Chautagn
<g/>
,	,	kIx,	,
M.	M.	kA	M.
du	du	k?	du
Po	Po	kA	Po
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Rosé	Rosé	k1gNnSc1	Rosé
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Rouge	rouge	k1gFnSc1	rouge
(	(	kIx(	(
<g/>
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Masový	masový	k2eAgInSc1d1	masový
Hrozen	hrozen	k1gInSc1	hrozen
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Morillon	Morillon	k1gInSc4	Morillon
Rouge	rouge	k1gFnSc2	rouge
<g/>
,	,	kIx,	,
M.	M.	kA	M.
R.	R.	kA	R.
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Italie	Italie	k1gFnSc2	Italie
<g/>
,	,	kIx,	,
Ostitalienischer	Ostitalienischra	k1gFnPc2	Ostitalienischra
Malvasier	Malvasira	k1gFnPc2	Malvasira
<g/>
,	,	kIx,	,
Piros	Piros	k1gInSc1	Piros
Malvazia	Malvazia	k1gFnSc1	Malvazia
<g/>
,	,	kIx,	,
Printschtraube	Printschtraub	k1gInSc5	Printschtraub
<g/>
,	,	kIx,	,
Rani	Ran	k1gMnPc1	Ran
Crveni	Crven	k2eAgMnPc1d1	Crven
Veltlinac	Veltlinac	k1gFnSc1	Veltlinac
(	(	kIx(	(
<g/>
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ranna	Ranen	k2eAgFnSc1d1	Ranen
Babovina	Babovina	k1gFnSc1	Babovina
<g/>
,	,	kIx,	,
Rojal	Rojal	k1gInSc1	Rojal
(	(	kIx(	(
<g/>
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
R.	R.	kA	R.
Rose	Rose	k1gMnSc1	Rose
<g />
.	.	kIx.	.
</s>
<s>
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Italie	Italie	k1gFnSc2	Italie
<g/>
,	,	kIx,	,
Rose	Rose	k1gMnSc1	Rose
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Espagne	Espagn	k1gMnSc5	Espagn
<g/>
,	,	kIx,	,
Roter	Roter	k1gInSc4	Roter
Harteinisch	Harteinischa	k1gFnPc2	Harteinischa
<g/>
,	,	kIx,	,
Rother	Rothra	k1gFnPc2	Rothra
Malvasier	Malvasira	k1gFnPc2	Malvasira
<g/>
,	,	kIx,	,
R.	R.	kA	R.
Rampfler	Rampfler	k1gInSc1	Rampfler
<g/>
,	,	kIx,	,
R.	R.	kA	R.
Zierfahnler	Zierfahnler	k1gInSc1	Zierfahnler
<g/>
,	,	kIx,	,
R.	R.	kA	R.
Zierfandler	Zierfandler	k1gInSc1	Zierfandler
<g/>
,	,	kIx,	,
Skorý	Skorý	k?	Skorý
Červený	Červený	k1gMnSc1	Červený
Muskatel	Muskatel	k1gMnSc1	Muskatel
<g/>
,	,	kIx,	,
Uva	Uva	k1gMnSc1	Uva
Rosa	Rosa	k1gMnSc1	Rosa
<g/>
,	,	kIx,	,
Valteliner	Valteliner	k1gMnSc1	Valteliner
(	(	kIx(	(
<g/>
Rouge	rouge	k1gFnSc1	rouge
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Précoce	Précoce	k1gMnSc1	Précoce
<g/>
,	,	kIx,	,
Veltliner	Veltliner	k1gMnSc1	Veltliner
Frührot	Frührot	k1gMnSc1	Frührot
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Mahrer	Mahrer	k1gMnSc1	Mahrer
Roter	Roter	k1gMnSc1	Roter
<g/>
,	,	kIx,	,
V.	V.	kA	V.
(	(	kIx(	(
<g/>
Rouge	rouge	k1gFnSc1	rouge
<g/>
)	)	kIx)	)
Precoce	Precoce	k1gFnSc1	Precoce
<g/>
,	,	kIx,	,
Veltelini	Veltelin	k2eAgMnPc1d1	Veltelin
Korai	Kora	k1gMnPc1	Kora
Piros	Pirosa	k1gFnPc2	Pirosa
(	(	kIx(	(
<g/>
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Veltlinac	Veltlinac	k1gFnSc1	Veltlinac
(	(	kIx(	(
<g/>
Crveni	Crven	k2eAgMnPc1d1	Crven
<g/>
)	)	kIx)	)
Rani	Ran	k1gMnPc1	Ran
<g/>
,	,	kIx,	,
Velteliner	Velteliner	k1gMnSc1	Velteliner
Frührot	Frührot	k1gMnSc1	Frührot
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Veltliner	Veltliner	k1gMnSc1	Veltliner
Rosso	Rossa	k1gFnSc5	Rossa
Precoce	Precoec	k1gInPc1	Precoec
(	(	kIx(	(
<g/>
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Veltlínské	veltlínský	k2eAgFnSc2d1	veltlínský
červené	červená	k1gFnSc2	červená
skoré	skoré	k?	skoré
<g/>
,	,	kIx,	,
Večerka	večerka	k1gFnSc1	večerka
<g/>
.	.	kIx.	.
</s>
<s>
Odrůdy	odrůda	k1gFnPc1	odrůda
Veltlínské	veltlínský	k2eAgFnSc2d1	veltlínský
červené	červená	k1gFnSc2	červená
rané	raný	k2eAgFnSc2d1	raná
<g/>
,	,	kIx,	,
lidově	lidově	k6eAd1	lidově
zvané	zvaný	k2eAgNnSc1d1	zvané
"	"	kIx"	"
<g/>
Večerka	večerka	k1gFnSc1	večerka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Veltlínské	veltlínský	k2eAgFnSc2d1	veltlínský
červené	červený	k2eAgFnSc2d1	červená
a	a	k8xC	a
Veltlínské	veltlínský	k2eAgFnSc2d1	veltlínský
červenobílé	červenobílý	k2eAgFnSc2d1	červenobílá
jsou	být	k5eAaImIp3nP	být
nejen	nejen	k6eAd1	nejen
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
často	často	k6eAd1	často
i	i	k9	i
navzájem	navzájem	k6eAd1	navzájem
zaměňované	zaměňovaný	k2eAgFnPc4d1	zaměňovaná
odrůdy	odrůda	k1gFnPc4	odrůda
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
lze	lze	k6eAd1	lze
na	na	k7c6	na
vinicích	vinice	k1gFnPc6	vinice
objevit	objevit	k5eAaPmF	objevit
velmi	velmi	k6eAd1	velmi
vzácně	vzácně	k6eAd1	vzácně
ještě	ještě	k9	ještě
odrůdy	odrůda	k1gFnSc2	odrůda
Veltlínské	veltlínský	k2eAgFnSc2d1	veltlínský
hnědé	hnědý	k2eAgFnSc2d1	hnědá
a	a	k8xC	a
Veltlínské	veltlínský	k2eAgFnSc2d1	veltlínský
šedé	šedá	k1gFnSc2	šedá
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
jsou	být	k5eAaImIp3nP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
mutacemi	mutace	k1gFnPc7	mutace
odrůdy	odrůda	k1gFnSc2	odrůda
Veltlínské	veltlínský	k2eAgFnSc2d1	veltlínský
červené	červená	k1gFnSc2	červená
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
<g/>
,	,	kIx,	,
v	v	k7c6	v
katalogu	katalog	k1gInSc6	katalog
VIVC	VIVC	kA	VIVC
uvedená	uvedený	k2eAgFnSc1d1	uvedená
odrůda	odrůda	k1gFnSc1	odrůda
Veltliner	Veltliner	k1gMnSc1	Veltliner
Weiss	Weiss	k1gMnSc1	Weiss
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
identická	identický	k2eAgFnSc1d1	identická
s	s	k7c7	s
odr	odr	k1gInSc4	odr
<g/>
.	.	kIx.	.
</s>
<s>
Veltlínské	veltlínský	k2eAgFnPc1d1	veltlínský
červenobílé	červenobílý	k2eAgFnPc1d1	červenobílá
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
těchto	tento	k3xDgFnPc2	tento
odrůd	odrůda	k1gFnPc2	odrůda
nepatří	patřit	k5eNaImIp3nS	patřit
odrůda	odrůda	k1gFnSc1	odrůda
Veltlínské	veltlínský	k2eAgFnSc2d1	veltlínský
zelené	zelená	k1gFnSc2	zelená
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
považovaná	považovaný	k2eAgFnSc1d1	považovaná
za	za	k7c4	za
mutaci	mutace	k1gFnSc4	mutace
odrůdy	odrůda	k1gFnSc2	odrůda
Veltlínské	veltlínský	k2eAgFnSc2d1	veltlínský
červené	červená	k1gFnSc2	červená
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
odrůda	odrůda	k1gFnSc1	odrůda
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
dle	dle	k7c2	dle
genetických	genetický	k2eAgInPc2d1	genetický
výzkumů	výzkum	k1gInPc2	výzkum
<g/>
,	,	kIx,	,
publikovaných	publikovaný	k2eAgFnPc2d1	publikovaná
roku	rok	k1gInSc3	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
spontánním	spontánní	k2eAgNnSc7d1	spontánní
křížením	křížení	k1gNnSc7	křížení
odrůd	odrůda	k1gFnPc2	odrůda
Tramín	tramín	k1gInSc4	tramín
a	a	k8xC	a
St.	st.	kA	st.
Georgen	Georgen	k1gInSc1	Georgen
<g/>
.	.	kIx.	.
</s>
<s>
Odrůda	odrůda	k1gFnSc1	odrůda
Jubiläumsrebe	Jubiläumsreb	k1gInSc5	Jubiläumsreb
<g/>
,	,	kIx,	,
vyšlechtěná	vyšlechtěný	k2eAgFnSc1d1	vyšlechtěná
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
Fritzem	Fritz	k1gMnSc7	Fritz
Zweigeltem	Zweigelt	k1gInSc7	Zweigelt
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
výsledků	výsledek	k1gInPc2	výsledek
genové	genový	k2eAgFnSc2d1	genová
analýzy	analýza	k1gFnSc2	analýza
<g/>
,	,	kIx,	,
provedené	provedený	k2eAgNnSc1d1	provedené
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
Regnerem	Regner	k1gMnSc7	Regner
v	v	k7c6	v
rakouském	rakouský	k2eAgInSc6d1	rakouský
Klosterneuburgu	Klosterneuburg	k1gInSc6	Klosterneuburg
<g/>
,	,	kIx,	,
křížencem	kříženec	k1gMnSc7	kříženec
odrůd	odrůda	k1gFnPc2	odrůda
Veltlínské	veltlínský	k2eAgNnSc4d1	Veltlínské
červené	červené	k1gNnSc4	červené
rané	raný	k2eAgNnSc4d1	rané
×	×	k?	×
Portugal	portugal	k1gInSc4	portugal
šedý	šedý	k2eAgInSc4d1	šedý
<g/>
.	.	kIx.	.
</s>
<s>
Šlechtitelem	šlechtitel	k1gMnSc7	šlechtitel
původně	původně	k6eAd1	původně
udávaný	udávaný	k2eAgInSc4d1	udávaný
údaj	údaj	k1gInSc4	údaj
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
křížení	křížení	k1gNnSc4	křížení
odrůd	odrůda	k1gFnPc2	odrůda
Modrý	modrý	k2eAgInSc1d1	modrý
Portugal	portugal	k1gInSc1	portugal
a	a	k8xC	a
Frankovka	frankovka	k1gFnSc1	frankovka
byl	být	k5eAaImAgInS	být
tak	tak	k6eAd1	tak
podstatně	podstatně	k6eAd1	podstatně
pozměněn	pozměněn	k2eAgInSc1d1	pozměněn
<g/>
.	.	kIx.	.
</s>
<s>
Réví	réví	k1gNnSc1	réví
vyzrává	vyzrávat	k5eAaImIp3nS	vyzrávat
pouze	pouze	k6eAd1	pouze
průměrně	průměrně	k6eAd1	průměrně
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Odolnost	odolnost	k1gFnSc1	odolnost
proti	proti	k7c3	proti
zimním	zimní	k2eAgInPc3d1	zimní
mrazům	mráz	k1gInPc3	mráz
je	být	k5eAaImIp3nS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
jarním	jarní	k2eAgInPc3d1	jarní
mrazům	mráz	k1gInPc3	mráz
dobrá	dobrý	k2eAgFnSc1d1	dobrá
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
bujně	bujně	k6eAd1	bujně
roste	růst	k5eAaImIp3nS	růst
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jí	on	k3xPp3gFnSc3	on
vyhovují	vyhovovat	k5eAaImIp3nP	vyhovovat
širší	široký	k2eAgFnSc4d2	širší
spony	spona	k1gFnPc4	spona
a	a	k8xC	a
vysoké	vysoký	k2eAgNnSc4d1	vysoké
vedení	vedení	k1gNnSc4	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
řez	řez	k1gInSc1	řez
Sylvoz	Sylvoz	k1gInSc1	Sylvoz
anebo	anebo	k8xC	anebo
srdcový	srdcový	k2eAgInSc1d1	srdcový
řez	řez	k1gInSc1	řez
(	(	kIx(	(
<g/>
tvar	tvar	k1gInSc1	tvar
<g/>
)	)	kIx)	)
se	s	k7c7	s
zatížením	zatížení	k1gNnSc7	zatížení
8-10	[number]	k4	8-10
oček	očko	k1gNnPc2	očko
na	na	k7c4	na
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahrádkách	zahrádka	k1gFnPc6	zahrádka
prospívá	prospívat	k5eAaImIp3nS	prospívat
na	na	k7c6	na
stěnách	stěna	k1gFnPc6	stěna
nebo	nebo	k8xC	nebo
pergolách	pergola	k1gFnPc6	pergola
<g/>
.	.	kIx.	.
</s>
<s>
Vhodné	vhodný	k2eAgInPc1d1	vhodný
jsou	být	k5eAaImIp3nP	být
podnože	podnož	k1gInPc1	podnož
T	T	kA	T
5	[number]	k4	5
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
SO-	SO-	k1gFnSc1	SO-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
125	[number]	k4	125
AA	AA	kA	AA
<g/>
,	,	kIx,	,
Cr	cr	k0	cr
2	[number]	k4	2
<g/>
,	,	kIx,	,
zásadně	zásadně	k6eAd1	zásadně
nevyhovuje	vyhovovat	k5eNaImIp3nS	vyhovovat
K	k	k7c3	k
5	[number]	k4	5
<g/>
BB	BB	kA	BB
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
hlinitopísčité	hlinitopísčitý	k2eAgFnPc4d1	hlinitopísčitá
půdy	půda	k1gFnPc4	půda
se	se	k3xPyFc4	se
osvědčila	osvědčit	k5eAaPmAgFnS	osvědčit
podnož	podnož	k1gFnSc1	podnož
Schwarzmann	Schwarzmanna	k1gFnPc2	Schwarzmanna
<g/>
.	.	kIx.	.
</s>
<s>
Výnos	výnos	k1gInSc1	výnos
je	být	k5eAaImIp3nS	být
střední	střední	k2eAgMnSc1d1	střední
<g/>
,	,	kIx,	,
7-12	[number]	k4	7-12
t	t	k?	t
<g/>
/	/	kIx~	/
<g/>
ha	ha	kA	ha
při	při	k7c6	při
cukernatosti	cukernatost	k1gFnSc6	cukernatost
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
)	)	kIx)	)
18-20	[number]	k4	18-20
°	°	k?	°
<g/>
NM	NM	kA	NM
a	a	k8xC	a
aciditě	acidita	k1gFnSc6	acidita
7-9	[number]	k4	7-9
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Raší	rašit	k5eAaImIp3nS	rašit
a	a	k8xC	a
kvete	kvést	k5eAaImIp3nS	kvést
středně	středně	k6eAd1	středně
pozdně	pozdně	k6eAd1	pozdně
<g/>
,	,	kIx,	,
raší	rašit	k5eAaImIp3nS	rašit
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
dekádě	dekáda	k1gFnSc6	dekáda
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
kvete	kvést	k5eAaImIp3nS	kvést
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
květy	květ	k1gInPc4	květ
neopadávají	opadávat	k5eNaImIp3nP	opadávat
<g/>
.	.	kIx.	.
</s>
<s>
Zaměkat	zaměkat	k5eAaImF	zaměkat
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
období	období	k1gNnSc1	období
sklizně	sklizeň	k1gFnSc2	sklizeň
většinou	většina	k1gFnSc7	většina
začíná	začínat	k5eAaImIp3nS	začínat
od	od	k7c2	od
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
září	září	k1gNnSc2	září
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
do	do	k7c2	do
konce	konec	k1gInSc2	konec
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
napadána	napadat	k5eAaPmNgFnS	napadat
plísní	plíseň	k1gFnSc7	plíseň
révovou	révový	k2eAgFnSc7d1	révová
(	(	kIx(	(
<g/>
Plasmopara	Plasmopara	k1gFnSc1	Plasmopara
viticola	viticola	k1gFnSc1	viticola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
padlím	padlí	k1gNnSc7	padlí
révovým	révový	k2eAgFnPc3d1	révová
(	(	kIx(	(
<g/>
Uncinula	Uncinula	k1gFnSc1	Uncinula
necator	necator	k1gMnSc1	necator
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlhkého	vlhký	k2eAgNnSc2d1	vlhké
počasí	počasí	k1gNnSc2	počasí
husté	hustý	k2eAgInPc4d1	hustý
hrozny	hrozen	k1gInPc4	hrozen
rychle	rychle	k6eAd1	rychle
infikuje	infikovat	k5eAaBmIp3nS	infikovat
plíseň	plíseň	k1gFnSc1	plíseň
šedá	šedá	k1gFnSc1	šedá
(	(	kIx(	(
<g/>
Botrytis	Botrytis	k1gInSc1	Botrytis
cinerea	cinere	k1gInSc2	cinere
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Netrpí	trpět	k5eNaImIp3nS	trpět
černou	černý	k2eAgFnSc7d1	černá
skvrnitostí	skvrnitost	k1gFnSc7	skvrnitost
(	(	kIx(	(
<g/>
Phomopsis	Phomopsis	k1gFnSc1	Phomopsis
viticola	viticola	k1gFnSc1	viticola
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hrozny	hrozen	k1gInPc1	hrozen
jsou	být	k5eAaImIp3nP	být
vyhledávány	vyhledáván	k2eAgFnPc1d1	vyhledávána
vosami	vosa	k1gFnPc7	vosa
<g/>
,	,	kIx,	,
včelami	včela	k1gFnPc7	včela
a	a	k8xC	a
ptáky	pták	k1gMnPc4	pták
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
polohu	poloha	k1gFnSc4	poloha
a	a	k8xC	a
půdu	půda	k1gFnSc4	půda
je	být	k5eAaImIp3nS	být
odrůda	odrůda	k1gFnSc1	odrůda
nenáročná	náročný	k2eNgFnSc1d1	nenáročná
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
jí	on	k3xPp3gFnSc3	on
nejvíce	hodně	k6eAd3	hodně
vyhovují	vyhovovat	k5eAaImIp3nP	vyhovovat
středně	středně	k6eAd1	středně
těžké	těžký	k2eAgFnPc1d1	těžká
půdy	půda	k1gFnPc1	půda
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
roste	růst	k5eAaImIp3nS	růst
i	i	k9	i
v	v	k7c6	v
půdách	půda	k1gFnPc6	půda
kamenitých	kamenitý	k2eAgFnPc6d1	kamenitá
a	a	k8xC	a
propustných	propustný	k2eAgFnPc6d1	propustná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
půdách	půda	k1gFnPc6	půda
s	s	k7c7	s
vyšším	vysoký	k2eAgInSc7d2	vyšší
obsahem	obsah	k1gInSc7	obsah
dusíku	dusík	k1gInSc2	dusík
však	však	k9	však
roste	růst	k5eAaImIp3nS	růst
velmi	velmi	k6eAd1	velmi
bujně	bujně	k6eAd1	bujně
a	a	k8xC	a
zmrzá	zmrzat	k5eAaImIp3nS	zmrzat
<g/>
.	.	kIx.	.
</s>
<s>
Nevhodné	vhodný	k2eNgInPc1d1	nevhodný
jsou	být	k5eAaImIp3nP	být
vlhké	vlhký	k2eAgInPc1d1	vlhký
<g/>
,	,	kIx,	,
nížinné	nížinný	k2eAgInPc1d1	nížinný
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
vápenité	vápenitý	k2eAgFnPc4d1	vápenitá
půdy	půda	k1gFnPc4	půda
<g/>
,	,	kIx,	,
hrozny	hrozen	k1gInPc4	hrozen
totiž	totiž	k9	totiž
snadno	snadno	k6eAd1	snadno
hnijí	hnít	k5eAaImIp3nP	hnít
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
v	v	k7c6	v
severnějších	severní	k2eAgFnPc6d2	severnější
vinařských	vinařský	k2eAgFnPc6d1	vinařská
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
využívá	využívat	k5eAaPmIp3nS	využívat
méně	málo	k6eAd2	málo
úrodné	úrodný	k2eAgFnPc1d1	úrodná
<g/>
,	,	kIx,	,
písčité	písčitý	k2eAgFnPc1d1	písčitá
půdy	půda	k1gFnPc1	půda
a	a	k8xC	a
terasy	terasa	k1gFnPc1	terasa
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
snáší	snášet	k5eAaImIp3nS	snášet
sucho	sucho	k1gNnSc1	sucho
<g/>
.	.	kIx.	.
</s>
<s>
Technologii	technologie	k1gFnSc4	technologie
výroby	výroba	k1gFnSc2	výroba
vína	víno	k1gNnSc2	víno
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zaměřit	zaměřit	k5eAaPmF	zaměřit
na	na	k7c4	na
zvýraznění	zvýraznění	k1gNnSc4	zvýraznění
aromatického	aromatický	k2eAgInSc2d1	aromatický
projevu	projev	k1gInSc2	projev
a	a	k8xC	a
zachování	zachování	k1gNnSc2	zachování
svěží	svěží	k2eAgFnSc2d1	svěží
kyseliny	kyselina	k1gFnSc2	kyselina
k	k	k7c3	k
chuťovému	chuťový	k2eAgInSc3d1	chuťový
dojmu	dojem	k1gInSc3	dojem
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc4d1	vhodné
důkladnější	důkladný	k2eAgNnSc4d2	důkladnější
odkalení	odkalení	k1gNnSc4	odkalení
moštu	mošt	k1gInSc2	mošt
<g/>
,	,	kIx,	,
aplikace	aplikace	k1gFnSc2	aplikace
čisté	čistý	k2eAgFnSc2d1	čistá
kultury	kultura	k1gFnSc2	kultura
kvasinek	kvasinka	k1gFnPc2	kvasinka
a	a	k8xC	a
kvašení	kvašení	k1gNnSc2	kvašení
při	při	k7c6	při
nižších	nízký	k2eAgFnPc6d2	nižší
teplotách	teplota	k1gFnPc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
pomůže	pomoct	k5eAaPmIp3nS	pomoct
získat	získat	k5eAaPmF	získat
svěží	svěží	k2eAgFnSc1d1	svěží
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
pitelné	pitelný	k2eAgNnSc4d1	pitelné
víno	víno	k1gNnSc4	víno
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nízkému	nízký	k2eAgInSc3d1	nízký
obsahu	obsah	k1gInSc2	obsah
kyselin	kyselina	k1gFnPc2	kyselina
zraje	zrát	k5eAaImIp3nS	zrát
víno	víno	k1gNnSc1	víno
poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
často	často	k6eAd1	často
využívá	využívat	k5eAaImIp3nS	využívat
k	k	k7c3	k
urychlení	urychlení	k1gNnSc3	urychlení
zrání	zrání	k1gNnSc2	zrání
cuvée	cuvé	k1gInSc2	cuvé
vín	víno	k1gNnPc2	víno
s	s	k7c7	s
odrůdami	odrůda	k1gFnPc7	odrůda
pozdními	pozdní	k2eAgFnPc7d1	pozdní
(	(	kIx(	(
<g/>
Ryzlink	ryzlink	k1gInSc1	ryzlink
vlašský	vlašský	k2eAgInSc1d1	vlašský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
vína	vína	k1gFnSc1	vína
postrádají	postrádat	k5eAaImIp3nP	postrádat
osobitý	osobitý	k2eAgInSc4d1	osobitý
výraz	výraz	k1gInSc4	výraz
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
vína	víno	k1gNnSc2	víno
ze	z	k7c2	z
severněji	severně	k6eAd2	severně
položených	položený	k2eAgFnPc2d1	položená
vinic	vinice	k1gFnPc2	vinice
jižní	jižní	k2eAgFnSc2d1	jižní
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
na	na	k7c6	na
určité	určitý	k2eAgFnSc6d1	určitá
osobitosti	osobitost	k1gFnSc6	osobitost
nechybí	chybět	k5eNaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgNnPc1d1	typické
vína	víno	k1gNnPc1	víno
jsou	být	k5eAaImIp3nP	být
zlatožlutá	zlatožlutý	k2eAgNnPc1d1	zlatožluté
<g/>
,	,	kIx,	,
neutrální	neutrální	k2eAgFnPc1d1	neutrální
vůně	vůně	k1gFnPc1	vůně
<g/>
,	,	kIx,	,
při	při	k7c6	při
vyšší	vysoký	k2eAgFnSc6d2	vyšší
cukernatosti	cukernatost	k1gFnSc6	cukernatost
moštů	mošt	k1gInPc2	mošt
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
mandlové	mandlový	k2eAgInPc1d1	mandlový
až	až	k8xS	až
medové	medový	k2eAgInPc1d1	medový
tóny	tón	k1gInPc1	tón
<g/>
.	.	kIx.	.
</s>
<s>
Chuť	chuť	k1gFnSc1	chuť
je	být	k5eAaImIp3nS	být
plná	plný	k2eAgFnSc1d1	plná
<g/>
,	,	kIx,	,
jemně	jemně	k6eAd1	jemně
ovocitá	ovocitat	k5eAaPmIp3nS	ovocitat
až	až	k9	až
kořenitá	kořenitý	k2eAgFnSc1d1	kořenitá
<g/>
,	,	kIx,	,
širší	široký	k2eAgFnSc1d2	širší
a	a	k8xC	a
extraktivní	extraktivní	k2eAgFnSc1d1	extraktivní
<g/>
,	,	kIx,	,
s	s	k7c7	s
jemnou	jemný	k2eAgFnSc7d1	jemná
kyselinkou	kyselinka	k1gFnSc7	kyselinka
ba	ba	k9	ba
se	s	k7c7	s
středním	střední	k2eAgInSc7d1	střední
až	až	k8xS	až
vyšším	vysoký	k2eAgInSc7d2	vyšší
obsahem	obsah	k1gInSc7	obsah
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vůni	vůně	k1gFnSc6	vůně
a	a	k8xC	a
chuti	chuť	k1gFnSc6	chuť
můžeme	moct	k5eAaImIp1nP	moct
hledat	hledat	k5eAaImF	hledat
směs	směs	k1gFnSc4	směs
zahradního	zahradní	k2eAgNnSc2d1	zahradní
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
,	,	kIx,	,
mandle	mandle	k1gFnSc2	mandle
<g/>
,	,	kIx,	,
med	med	k1gInSc4	med
<g/>
,	,	kIx,	,
chlebnatost	chlebnatost	k1gFnSc4	chlebnatost
<g/>
.	.	kIx.	.
</s>
<s>
Vhodnost	vhodnost	k1gFnSc1	vhodnost
k	k	k7c3	k
archivaci	archivace	k1gFnSc3	archivace
je	být	k5eAaImIp3nS	být
malá	malá	k1gFnSc1	malá
<g/>
,	,	kIx,	,
vína	víno	k1gNnPc1	víno
se	se	k3xPyFc4	se
pijí	pít	k5eAaImIp3nP	pít
většinou	většina	k1gFnSc7	většina
mladá	mladý	k2eAgFnSc1d1	mladá
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
buketní	buketní	k2eAgFnPc4d1	buketní
látky	látka	k1gFnPc4	látka
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
sklon	sklon	k1gInSc4	sklon
k	k	k7c3	k
madeirizaci	madeirizace	k1gFnSc3	madeirizace
<g/>
.	.	kIx.	.
</s>
<s>
Zrání	zrání	k1gNnSc1	zrání
vína	víno	k1gNnSc2	víno
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
brzy	brzy	k6eAd1	brzy
lahvovat	lahvovat	k5eAaImF	lahvovat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
časné	časný	k2eAgNnSc4d1	časné
vyzrávání	vyzrávání	k1gNnSc4	vyzrávání
a	a	k8xC	a
nízký	nízký	k2eAgInSc4d1	nízký
obsah	obsah	k1gInSc4	obsah
kyselin	kyselina	k1gFnPc2	kyselina
je	být	k5eAaImIp3nS	být
VČR	VČR	kA	VČR
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
odrůdou	odrůda	k1gFnSc7	odrůda
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
burčáku	burčák	k1gInSc2	burčák
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
"	"	kIx"	"
<g/>
bílých	bílý	k2eAgFnPc2d1	bílá
<g/>
"	"	kIx"	"
odrůd	odrůda	k1gFnPc2	odrůda
<g/>
,	,	kIx,	,
určených	určený	k2eAgFnPc6d1	určená
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
Svatomartinského	svatomartinský	k2eAgInSc2d1	svatomartinský
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
vína	vína	k1gFnSc1	vína
Veltlínského	veltlínský	k2eAgInSc2d1	veltlínský
červeného	červený	k2eAgInSc2d1	červený
raného	raný	k2eAgInSc2d1	raný
doporučují	doporučovat	k5eAaImIp3nP	doporučovat
k	k	k7c3	k
běžné	běžný	k2eAgFnSc3d1	běžná
denní	denní	k2eAgFnSc3d1	denní
stravě	strava	k1gFnSc3	strava
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
k	k	k7c3	k
masu	maso	k1gNnSc3	maso
s	s	k7c7	s
omáčkami	omáčka	k1gFnPc7	omáčka
<g/>
,	,	kIx,	,
hutným	hutný	k2eAgFnPc3d1	hutná
polévkám	polévka	k1gFnPc3	polévka
a	a	k8xC	a
studeným	studený	k2eAgFnPc3d1	studená
mísám	mísa	k1gFnPc3	mísa
<g/>
.	.	kIx.	.
</s>
