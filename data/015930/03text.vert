<s>
Truckee	Truckee	k6eAd1
</s>
<s>
Truckee	Truckee	k1gFnSc1
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
39	#num#	k4
<g/>
°	°	k?
<g/>
20	#num#	k4
<g/>
′	′	k?
<g/>
32	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
120	#num#	k4
<g/>
°	°	k?
<g/>
12	#num#	k4
<g/>
′	′	k?
<g/>
13	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
1773	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
UTC-	UTC-	k?
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
-	-	kIx~
<g/>
7	#num#	k4
(	(	kIx(
<g/>
letní	letní	k2eAgInSc1d1
čas	čas	k1gInSc1
<g/>
)	)	kIx)
Stát	stát	k1gInSc1
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
stát	stát	k5eAaPmF,k5eAaImF
</s>
<s>
Kalifornie	Kalifornie	k1gFnSc1
okres	okres	k1gInSc1
</s>
<s>
Nevada	Nevada	k1gFnSc1
Country	country	k2eAgFnSc2d1
</s>
<s>
Truckee	Truckee	k6eAd1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
87,2	87,2	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
16	#num#	k4
180	#num#	k4
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
185,6	185,6	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Richard	Richard	k1gMnSc1
Anderson	Anderson	k1gMnSc1
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
www.townoftruckee.com	www.townoftruckee.com	k1gInSc1
PSČ	PSČ	kA
</s>
<s>
96160	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Truckee	Truckee	k6eAd1
je	být	k5eAaImIp3nS
město	město	k1gNnSc1
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
okresu	okres	k1gInSc2
Nevada	Nevada	k1gFnSc1
Country	country	k2eAgMnPc2d1
ve	v	k7c6
státě	stát	k1gInSc6
Kalifornie	Kalifornie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
16	#num#	k4
180	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
o	o	k7c4
více	hodně	k6eAd2
než	než	k8xS
2	#num#	k4
000	#num#	k4
více	hodně	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
leží	ležet	k5eAaImIp3nS
ve	v	k7c6
vysoké	vysoký	k2eAgFnSc6d1
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
1773	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
proto	proto	k8xC
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
průměrná	průměrný	k2eAgFnSc1d1
roční	roční	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
jen	jen	k9
6,9	6,9	k4
°	°	k?
<g/>
C.	C.	kA
</s>
<s>
Řeka	řeka	k1gFnSc1
Truckee	Trucke	k1gFnSc2
River	Rivra	k1gFnPc2
východně	východně	k6eAd1
od	od	k7c2
Truckee	Trucke	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Truckee	Trucke	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
82041168	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
133710284	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
82041168	#num#	k4
</s>
