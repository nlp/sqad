<s desamb="1">
Město	město	k1gNnSc1
leží	ležet	k5eAaImIp3nS
ve	v	k7c6
vysoké	vysoký	k2eAgFnSc6d1
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
1773	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
proto	proto	k8xC
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
průměrná	průměrný	k2eAgFnSc1d1
roční	roční	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
jen	jen	k9
6,9	6,9	k4
°	°	k?
<g/>
C.	C.	kA
</s>