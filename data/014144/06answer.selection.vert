<s>
Atomizér	atomizér	k1gInSc1
je	být	k5eAaImIp3nS
zařízení	zařízení	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
aerosolový	aerosolový	k2eAgInSc1d1
rozprašovač	rozprašovač	k1gInSc1
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
aerosol	aerosol	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
místo	místo	k7c2
stlačeného	stlačený	k2eAgInSc2d1
vzduchu	vzduch	k1gInSc2
používá	používat	k5eAaImIp3nS
mechanickou	mechanický	k2eAgFnSc4d1
pumpu	pumpa	k1gFnSc4
<g/>
.	.	kIx.
</s>