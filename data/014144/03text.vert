<s>
Atomizér	atomizér	k1gInSc1
</s>
<s>
Atomizér	atomizér	k1gInSc1
je	být	k5eAaImIp3nS
zařízení	zařízení	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
aerosolový	aerosolový	k2eAgInSc1d1
rozprašovač	rozprašovač	k1gInSc1
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
aerosol	aerosol	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
místo	místo	k7c2
stlačeného	stlačený	k2eAgInSc2d1
vzduchu	vzduch	k1gInSc2
používá	používat	k5eAaImIp3nS
mechanickou	mechanický	k2eAgFnSc4d1
pumpu	pumpa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
je	být	k5eAaImIp3nS
odvozen	odvodit	k5eAaPmNgInS
ze	z	k7c2
slova	slovo	k1gNnSc2
atomizace	atomizace	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
znamená	znamenat	k5eAaImIp3nS
drobení	drobení	k1gNnSc4
<g/>
,	,	kIx,
rozdrobení	rozdrobení	k1gNnSc4
<g/>
,	,	kIx,
rozložení	rozložení	k1gNnSc4
na	na	k7c4
drobné	drobný	k2eAgFnPc4d1
<g/>
,	,	kIx,
nepatrné	nepatrný	k2eAgFnPc4d1,k2eNgFnPc4d1
částečky	částečka	k1gFnPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Toto	tento	k3xDgNnSc1
označení	označení	k1gNnSc1
se	se	k3xPyFc4
rovněž	rovněž	k9
používá	používat	k5eAaImIp3nS
pro	pro	k7c4
součástku	součástka	k1gFnSc4
elektronické	elektronický	k2eAgFnSc2d1
cigarety	cigareta	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
zařízení	zařízení	k1gNnSc4
s	s	k7c7
funkcí	funkce	k1gFnSc7
vaporizéru	vaporizér	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
někdy	někdy	k6eAd1
kuřáky	kuřák	k1gInPc4
používáno	používán	k2eAgNnSc4d1
místo	místo	k1gNnSc4
klasických	klasický	k2eAgFnPc2d1
cigaret	cigareta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Atomizér	atomizér	k1gInSc1
v	v	k7c6
elektronické	elektronický	k2eAgFnSc6d1
cigaretě	cigareta	k1gFnSc6
</s>
<s>
Atomizér	atomizér	k1gInSc1
v	v	k7c6
elektronické	elektronický	k2eAgFnSc6d1
cigaretě	cigareta	k1gFnSc6
se	se	k3xPyFc4
většinou	většinou	k6eAd1
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
topné	topný	k2eAgFnSc2d1
spirálky	spirálka	k1gFnSc2
vytvořené	vytvořený	k2eAgFnSc2d1
z	z	k7c2
drátu	drát	k1gInSc2
o	o	k7c6
odporu	odpor	k1gInSc6
mezi	mezi	k7c7
0.2	0.2	k4
až	až	k9
3	#num#	k4
Ω	Ω	k?
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
ovíjí	ovíjet	k5eAaImIp3nS
kolem	kolem	k7c2
savého	savý	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
(	(	kIx(
<g/>
knotu	knot	k1gInSc2
<g/>
)	)	kIx)
napuštěného	napuštěný	k2eAgInSc2d1
e-liquidem	e-liquid	k1gInSc7
(	(	kIx(
<g/>
tekutá	tekutý	k2eAgFnSc1d1
náplň	náplň	k1gFnSc4
obsahující	obsahující	k2eAgInSc4d1
propylenglykol	propylenglykol	k1gInSc4
nebo	nebo	k8xC
glycerin	glycerin	k1gInSc4
<g/>
,	,	kIx,
další	další	k2eAgFnPc1d1
příměsi	příměs	k1gFnPc1
a	a	k8xC
různé	různý	k2eAgNnSc1d1
množství	množství	k1gNnSc1
nikotinu	nikotin	k1gInSc2
<g/>
)	)	kIx)
.	.	kIx.
</s>
<s desamb="1">
Atomizér	atomizér	k1gInSc1
je	být	k5eAaImIp3nS
připojen	připojit	k5eAaPmNgInS
ke	k	k7c3
kladnému	kladný	k2eAgMnSc3d1
a	a	k8xC
zápornému	záporný	k2eAgInSc3d1
pólu	pól	k1gInSc3
baterie	baterie	k1gFnSc1
e-cigarety	e-cigareta	k1gFnSc2
a	a	k8xC
při	při	k7c6
aktivaci	aktivace	k1gFnSc6
zařízení	zařízení	k1gNnSc2
se	se	k3xPyFc4
drát	drát	k1gInSc1
rychle	rychle	k6eAd1
ohřívá	ohřívat	k5eAaImIp3nS
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
přeměňuje	přeměňovat	k5eAaImIp3nS
kapalinu	kapalina	k1gFnSc4
na	na	k7c4
páru	pára	k1gFnSc4
vdechovanou	vdechovaný	k2eAgFnSc4d1
uživatelem	uživatel	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Knot	knot	k1gInSc1
atomizéru	atomizér	k1gInSc2
bývá	bývat	k5eAaImIp3nS
vyroben	vyrobit	k5eAaPmNgInS
z	z	k7c2
křemičitých	křemičitý	k2eAgNnPc2d1
vláken	vlákno	k1gNnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
bavlny	bavlna	k1gFnSc2
<g/>
,	,	kIx,
konopí	konopí	k1gNnSc2
<g/>
,	,	kIx,
bambusové	bambusový	k2eAgFnSc2d1
příze	příz	k1gFnSc2
apod.	apod.	kA
</s>
<s>
Trvanlivost	trvanlivost	k1gFnSc1
atomizéru	atomizér	k1gInSc2
</s>
<s>
Trvanlivost	trvanlivost	k1gFnSc1
atomizéru	atomizér	k1gInSc2
v	v	k7c6
e-cigaretě	e-cigaret	k1gInSc6
je	být	k5eAaImIp3nS
omezená	omezený	k2eAgFnSc1d1
a	a	k8xC
pohybuje	pohybovat	k5eAaImIp3nS
se	se	k3xPyFc4
mezi	mezi	k7c7
2	#num#	k4
až	až	k8xS
6	#num#	k4
týdny	týden	k1gInPc7
<g/>
,	,	kIx,
někteří	některý	k3yIgMnPc1
výrobci	výrobce	k1gMnPc1
uvádějí	uvádět	k5eAaImIp3nP
výdrž	výdrž	k1gFnSc4
až	až	k9
3	#num#	k4
měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
atomizéry	atomizér	k1gInPc1
typu	typ	k1gInSc2
tzv.	tzv.	kA
DIY	DIY	kA
(	(	kIx(
<g/>
„	„	k?
<g/>
do	do	k7c2
it	it	k?
yourself	yourself	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
si	se	k3xPyFc3
jejich	jejich	k3xOp3gFnSc2
části	část	k1gFnSc2
–	–	k?
knot	knot	k1gInSc1
a	a	k8xC
žhavící	žhavící	k2eAgFnSc4d1
spirálu	spirála	k1gFnSc4
–	–	k?
uživatel	uživatel	k1gMnSc1
může	moct	k5eAaImIp3nS
sám	sám	k3xTgMnSc1
vyměňovat	vyměňovat	k5eAaImF
za	za	k7c4
nové	nový	k2eAgInPc4d1
náhradní	náhradní	k2eAgInPc4d1
díly	díl	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
v	v	k7c6
atomizéru	atomizér	k1gInSc6
dojde	dojít	k5eAaPmIp3nS
nasátý	nasátý	k2eAgInSc1d1
e-liquid	e-liquid	k1gInSc1
a	a	k8xC
atomizér	atomizér	k1gInSc1
začne	začít	k5eAaPmIp3nS
pálit	pálit	k5eAaImF
na	na	k7c4
sucho	sucho	k1gNnSc4
<g/>
,	,	kIx,
vzniká	vznikat	k5eAaImIp3nS
nepříjemný	příjemný	k2eNgInSc1d1
štiplavý	štiplavý	k2eAgInSc1d1
dým	dým	k1gInSc1
a	a	k8xC
trvanlivost	trvanlivost	k1gFnSc1
součástky	součástka	k1gFnSc2
se	se	k3xPyFc4
rychle	rychle	k6eAd1
snižuje	snižovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Druhy	druh	k1gInPc1
atomizéru	atomizér	k1gInSc2
</s>
<s>
Existují	existovat	k5eAaImIp3nP
dva	dva	k4xCgInPc1
základní	základní	k2eAgInPc1d1
typy	typ	k1gInPc1
atomizérů	atomizér	k1gInPc2
podle	podle	k7c2
stylu	styl	k1gInSc2
potahu	potah	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
MTL	MTL	kA
(	(	kIx(
<g/>
mouth	mouth	k1gMnSc1
to	ten	k3xDgNnSc4
lungs	lungs	k6eAd1
<g/>
)	)	kIx)
neboli	neboli	k8xC
určené	určený	k2eAgFnPc4d1
na	na	k7c4
šlukování	šlukování	k?
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
u	u	k7c2
klasické	klasický	k2eAgFnSc2d1
cigarety	cigareta	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
DL	DL	kA
(	(	kIx(
<g/>
direct	direct	k2eAgInSc1d1
lungs	lungs	k1gInSc1
<g/>
)	)	kIx)
neboli	neboli	k8xC
určené	určený	k2eAgNnSc1d1
pro	pro	k7c4
přímý	přímý	k2eAgInSc4d1
potah	potah	k1gInSc4
do	do	k7c2
plic	plíce	k1gFnPc2
(	(	kIx(
<g/>
obdobně	obdobně	k6eAd1
jako	jako	k8xC,k8xS
u	u	k7c2
vodní	vodní	k2eAgFnSc2d1
dýmky	dýmka	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Někdy	někdy	k6eAd1
se	se	k3xPyFc4
jako	jako	k9
třetí	třetí	k4xOgFnSc1
kategorie	kategorie	k1gFnSc1
uvádí	uvádět	k5eAaImIp3nS
CC	CC	kA
(	(	kIx(
<g/>
Cloudchasing	Cloudchasing	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
tento	tento	k3xDgInSc1
styl	styl	k1gInSc1
kouření	kouření	k1gNnSc2
má	mít	k5eAaImIp3nS
za	za	k7c4
cíl	cíl	k1gInSc4
vytvořit	vytvořit	k5eAaPmF
co	co	k9
nejvíce	hodně	k6eAd3,k6eAd1
páry	pár	k1gInPc4
<g/>
,	,	kIx,
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
často	často	k6eAd1
na	na	k7c4
triky	trik	k1gInPc4
s	s	k7c7
kouřem	kouř	k1gInSc7
</s>
<s>
Atomizéry	atomizér	k1gInPc1
se	se	k3xPyFc4
pak	pak	k6eAd1
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
další	další	k2eAgInPc4d1
dva	dva	k4xCgInPc4
typy	typ	k1gInPc4
<g/>
:	:	kIx,
</s>
<s>
Atomizér	atomizér	k1gInSc1
na	na	k7c4
tovární	tovární	k2eAgFnPc4d1
hlavy	hlava	k1gFnPc4
-	-	kIx~
vyměňuje	vyměňovat	k5eAaImIp3nS
se	se	k3xPyFc4
celá	celý	k2eAgFnSc1d1
žhavící	žhavící	k2eAgFnSc1d1
hlava	hlava	k1gFnSc1
obsahující	obsahující	k2eAgFnSc1d1
jak	jak	k6eAd1
žhavící	žhavící	k2eAgFnSc4d1
spirálku	spirálka	k1gFnSc4
tak	tak	k8xC,k8xS
vatu	vata	k1gFnSc4
</s>
<s>
DIY	DIY	kA
(	(	kIx(
<g/>
z	z	k7c2
anglického	anglický	k2eAgMnSc2d1
do	do	k7c2
it	it	k?
yourself	yourself	k1gInSc1
<g/>
)	)	kIx)
-	-	kIx~
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
je	být	k5eAaImIp3nS
složitější	složitý	k2eAgMnSc1d2
<g/>
,	,	kIx,
zde	zde	k6eAd1
se	se	k3xPyFc4
vkládá	vkládat	k5eAaImIp3nS
zvolená	zvolený	k2eAgFnSc1d1
spirálka	spirálka	k1gFnSc1
<g/>
,	,	kIx,
do	do	k7c2
které	který	k3yRgFnSc2,k3yIgFnSc2,k3yQgFnSc2
se	se	k3xPyFc4
provleče	provléct	k5eAaPmIp3nS
vata	vata	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
styl	styl	k1gInSc4
je	být	k5eAaImIp3nS
pro	pro	k7c4
zkušenější	zkušený	k2eAgMnPc4d2
e-kuřáky	e-kuřák	k1gMnPc4
a	a	k8xC
poskytuje	poskytovat	k5eAaImIp3nS
lepší	dobrý	k2eAgFnSc4d2
chuť	chuť	k1gFnSc4
a	a	k8xC
je	být	k5eAaImIp3nS
levnější	levný	k2eAgMnSc1d2
než	než	k8xS
tovární	tovární	k2eAgFnPc1d1
hlavy	hlava	k1gFnPc1
</s>
<s>
DIY	DIY	kA
Atomizéry	atomizér	k1gInPc1
pak	pak	k9
můžeme	moct	k5eAaImIp1nP
rozdělit	rozdělit	k5eAaPmF
na	na	k7c4
další	další	k2eAgInPc4d1
tři	tři	k4xCgInPc4
typy	typ	k1gInPc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
<g/>
:	:	kIx,
</s>
<s>
RDA	RDA	kA
-	-	kIx~
(	(	kIx(
<g/>
rebuildable	rebuildable	k6eAd1
dripping	dripping	k1gInSc1
atomizer	atomizra	k1gFnPc2
<g/>
)	)	kIx)
-	-	kIx~
tento	tento	k3xDgInSc1
atomizér	atomizér	k1gInSc1
nemá	mít	k5eNaImIp3nS
nádržku	nádržka	k1gFnSc4
na	na	k7c4
eliquid	eliquid	k1gInSc4
a	a	k8xC
ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
tedy	tedy	k9
musí	muset	k5eAaImIp3nS
nakapávat	nakapávat	k5eAaImF
vždy	vždy	k6eAd1
po	po	k7c6
pár	pár	k4xCyI
potazích	potah	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
atomizéry	atomizér	k1gInPc4
mají	mít	k5eAaImIp3nP
většinou	většinou	k6eAd1
možnost	možnost	k1gFnSc4
„	„	k?
<g/>
Squonku	Squonek	k1gInSc3
<g/>
´´	´´	k?
což	což	k3yRnSc1,k3yQnSc1
znamená	znamenat	k5eAaImIp3nS
že	že	k8xS
v	v	k7c6
konektoru	konektor	k1gInSc6
je	být	k5eAaImIp3nS
malá	malý	k2eAgFnSc1d1
díra	díra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kombinaci	kombinace	k1gFnSc6
se	s	k7c7
Squonkovým	Squonkův	k2eAgNnSc7d1
zařízením	zařízení	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
vyznačuje	vyznačovat	k5eAaImIp3nS
gumovou	gumový	k2eAgFnSc7d1
lahvičkou	lahvička	k1gFnSc7
<g/>
,	,	kIx,
po	po	k7c6
jejím	její	k3xOp3gNnSc6
zmáčknutí	zmáčknutí	k1gNnSc6
se	se	k3xPyFc4
liquid	liquid	k1gInSc1
doplní	doplnit	k5eAaPmIp3nS
zespodu	zespodu	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
atomizéry	atomizér	k1gInPc1
nabízejí	nabízet	k5eAaImIp3nP
nejlepší	dobrý	k2eAgFnSc4d3
chuť	chuť	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
RTA	RTA	kA
-	-	kIx~
(	(	kIx(
<g/>
rebuildable	rebuildable	k6eAd1
tank	tank	k1gInSc1
atomizer	atomizra	k1gFnPc2
<g/>
)	)	kIx)
-	-	kIx~
klasický	klasický	k2eAgInSc1d1
atomizér	atomizér	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
má	mít	k5eAaImIp3nS
nádržku	nádržka	k1gFnSc4
na	na	k7c4
eliquid	eliquid	k1gInSc4
a	a	k8xC
dírami	díra	k1gFnPc7
se	se	k3xPyFc4
vsakuje	vsakovat	k5eAaImIp3nS
do	do	k7c2
vaty	vata	k1gFnSc2
ze	z	k7c2
které	který	k3yIgFnSc2,k3yRgFnSc2,k3yQgFnSc2
se	se	k3xPyFc4
pak	pak	k6eAd1
odpařuje	odpařovat	k5eAaImIp3nS
<g/>
,	,	kIx,
tento	tento	k3xDgInSc1
typ	typ	k1gInSc1
atomizéru	atomizér	k1gInSc2
je	být	k5eAaImIp3nS
nejpoužívanější	používaný	k2eAgInSc1d3
</s>
<s>
RDTA	RDTA	kA
-	-	kIx~
(	(	kIx(
<g/>
rebuildable	rebuildable	k6eAd1
driping	driping	k1gInSc1
tank	tank	k1gInSc1
atomizer	atomizer	k1gInSc4
<g/>
)	)	kIx)
-	-	kIx~
je	být	k5eAaImIp3nS
kombinací	kombinace	k1gFnPc2
RDA	RDA	kA
a	a	k8xC
RTA	RTA	kA
atomizéru	atomizér	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nádržka	nádržka	k1gFnSc1
je	být	k5eAaImIp3nS
uložená	uložený	k2eAgFnSc1d1
ve	v	k7c6
spodu	spod	k1gInSc6
atomizéru	atomizér	k1gInSc2
a	a	k8xC
podtlakem	podtlak	k1gInSc7
se	se	k3xPyFc4
vtahuje	vtahovat	k5eAaImIp3nS
do	do	k7c2
žhavící	žhavící	k2eAgFnSc2d1
spirálky	spirálka	k1gFnSc2
v	v	k7c6
horní	horní	k2eAgFnSc6d1
části	část	k1gFnSc6
atomizéru	atomizér	k1gInSc2
</s>
<s>
Existuje	existovat	k5eAaImIp3nS
více	hodně	k6eAd2
typů	typ	k1gInPc2
atomizérů	atomizér	k1gInPc2
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
jaké	jaký	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
mají	mít	k5eAaImIp3nP
vlastnosti	vlastnost	k1gFnPc4
<g/>
,	,	kIx,
tj.	tj.	kA
jak	jak	k8xS,k8xC
jsou	být	k5eAaImIp3nP
různě	různě	k6eAd1
upraveny	upravit	k5eAaPmNgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zásadní	zásadní	k2eAgFnSc1d1
funkce	funkce	k1gFnSc1
vždy	vždy	k6eAd1
zůstává	zůstávat	k5eAaImIp3nS
stejná	stejný	k2eAgFnSc1d1
<g/>
,	,	kIx,
tj.	tj.	kA
vytváření	vytváření	k1gNnSc4
dýmu	dým	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atomizér	atomizér	k1gInSc1
s	s	k7c7
velkou	velký	k2eAgFnSc7d1
nádobou	nádoba	k1gFnSc7
pro	pro	k7c4
náplň	náplň	k1gFnSc4
<g/>
,	,	kIx,
tzv.	tzv.	kA
tankem	tank	k1gInSc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
giantomizér	giantomizér	k1gInSc1
nebo	nebo	k8xC
také	také	k9
liquinátor	liquinátor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tzv.	tzv.	kA
cartomizér	cartomizér	k1gMnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
sobě	sebe	k3xPyFc6
spojuje	spojovat	k5eAaImIp3nS
cartrige	cartrig	k1gInPc4
a	a	k8xC
atomizér	atomizér	k1gInSc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
tak	tak	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
jeden	jeden	k4xCgInSc4
celek	celek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
e-cigareta	e-cigareta	k1gFnSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
neskládá	skládat	k5eNaImIp3nS
ze	z	k7c2
tří	tři	k4xCgFnPc2
částí	část	k1gFnPc2
jako	jako	k8xC,k8xS
klasická	klasický	k2eAgFnSc1d1
e-cigareta	e-cigareta	k1gFnSc1
(	(	kIx(
<g/>
atomizér	atomizér	k1gInSc1
<g/>
,	,	kIx,
cartrige	cartrige	k1gFnSc1
<g/>
,	,	kIx,
baterie	baterie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
dvou	dva	k4xCgFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cartomizéry	Cartomizéra	k1gFnPc1
jsou	být	k5eAaImIp3nP
modernější	moderní	k2eAgNnPc4d2
a	a	k8xC
také	také	k9
úspornější	úsporný	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
výměnou	výměna	k1gFnSc7
cartomizéru	cartomizér	k1gInSc2
uživatel	uživatel	k1gMnSc1
mění	měnit	k5eAaImIp3nS
současně	současně	k6eAd1
i	i	k9
atomizér	atomizér	k1gInSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
udržuje	udržovat	k5eAaImIp3nS
elektronickou	elektronický	k2eAgFnSc4d1
cigaretu	cigareta	k1gFnSc4
dostatečně	dostatečně	k6eAd1
dýmivou	dýmivý	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Clearomizér	Clearomizér	k1gInSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
označuje	označovat	k5eAaImIp3nS
typ	typ	k1gInSc4
atomizéru	atomizér	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
průhledný	průhledný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nemusí	muset	k5eNaImIp3nS
být	být	k5eAaImF
průhledný	průhledný	k2eAgInSc1d1
celý	celý	k2eAgInSc1d1
<g/>
,	,	kIx,
často	často	k6eAd1
obsahuje	obsahovat	k5eAaImIp3nS
jen	jen	k9
průhledné	průhledný	k2eAgFnPc4d1
části	část	k1gFnPc4
a	a	k8xC
zbytek	zbytek	k1gInSc4
součástky	součástka	k1gFnSc2
je	být	k5eAaImIp3nS
proveden	provést	k5eAaPmNgInS
například	například	k6eAd1
ve	v	k7c6
stříbrné	stříbrná	k1gFnSc6
<g/>
,	,	kIx,
ocelové	ocelový	k2eAgFnSc3d1
nebo	nebo	k8xC
černé	černý	k2eAgFnSc3d1
barvě	barva	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
uživatele	uživatel	k1gMnPc4
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
ověřit	ověřit	k5eAaPmF
kompatibilitu	kompatibilita	k1gFnSc4
určitého	určitý	k2eAgInSc2d1
atomizéru	atomizér	k1gInSc2
s	s	k7c7
baterií	baterie	k1gFnSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgInS
přístroj	přístroj	k1gInSc1
po	po	k7c6
smontování	smontování	k1gNnSc6
součástek	součástka	k1gFnPc2
funkční	funkční	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Srovnání	srovnání	k1gNnSc1
3	#num#	k4
základních	základní	k2eAgInPc2d1
druhů	druh	k1gInPc2
atomizérů	atomizér	k1gInPc2
</s>
<s>
Klasický	klasický	k2eAgInSc1d1
atomizér	atomizér	k1gInSc1
má	mít	k5eAaImIp3nS
jednoduchou	jednoduchý	k2eAgFnSc4d1
konstrukci	konstrukce	k1gFnSc4
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
plnění	plnění	k1gNnSc1
je	být	k5eAaImIp3nS
snadné	snadný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stav	stav	k1gInSc1
kapaliny	kapalina	k1gFnSc2
v	v	k7c6
něm	on	k3xPp3gMnSc6
je	být	k5eAaImIp3nS
však	však	k9
obtížně	obtížně	k6eAd1
kontrolovatelný	kontrolovatelný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpravidla	zpravidla	k6eAd1
je	být	k5eAaImIp3nS
také	také	k9
náročné	náročný	k2eAgNnSc1d1
tuto	tento	k3xDgFnSc4
součástku	součástka	k1gFnSc4
rozebrat	rozebrat	k5eAaPmF
a	a	k8xC
vyměnit	vyměnit	k5eAaPmF
náhradní	náhradní	k2eAgInPc4d1
díly	díl	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cartomizéry	Cartomizéra	k1gFnPc1
obsahují	obsahovat	k5eAaImIp3nP
již	již	k6eAd1
zmíněný	zmíněný	k2eAgInSc1d1
válcový	válcový	k2eAgInSc1d1
zásobník	zásobník	k1gInSc1
e-liquidu	e-liquid	k1gInSc2
a	a	k8xC
jsou	být	k5eAaImIp3nP
také	také	k6eAd1
doplněny	doplnit	k5eAaPmNgFnP
o	o	k7c4
materiál	materiál	k1gInSc4
zvaný	zvaný	k2eAgInSc4d1
[	[	kIx(
<g/>
polyfil	polyfil	k1gInSc4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
nasává	nasávat	k5eAaImIp3nS
kapalinu	kapalina	k1gFnSc4
a	a	k8xC
umožňuje	umožňovat	k5eAaImIp3nS
delší	dlouhý	k2eAgFnSc4d2
dobu	doba	k1gFnSc4
kouření	kouření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cartomizér	Cartomizér	k1gInSc1
lze	lze	k6eAd1
poměrně	poměrně	k6eAd1
snadno	snadno	k6eAd1
upravovat	upravovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
clearomizéru	clearomizér	k1gInSc2
je	být	k5eAaImIp3nS
díky	díky	k7c3
jeho	jeho	k3xOp3gFnSc3
průhlednosti	průhlednost	k1gFnSc3
velmi	velmi	k6eAd1
snadné	snadný	k2eAgNnSc1d1
kontrolovat	kontrolovat	k5eAaImF
hladinu	hladina	k1gFnSc4
kapaliny	kapalina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průhledná	průhledný	k2eAgFnSc1d1
část	část	k1gFnSc1
clearomizéru	clearomizér	k1gInSc2
bývá	bývat	k5eAaImIp3nS
vyrobena	vyroben	k2eAgFnSc1d1
z	z	k7c2
plastového	plastový	k2eAgInSc2d1
nebo	nebo	k8xC
pyrexového	pyrexový	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc1
modely	model	k1gInPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
uživatelem	uživatel	k1gMnSc7
upraveny	upravit	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
E-cigarettes	E-cigarettes	k1gInSc1
<g/>
:	:	kIx,
No	no	k9
smoke	smoke	k1gInSc1
<g/>
,	,	kIx,
but	but	k?
fiery	fiera	k1gFnSc2
debate	debat	k1gInSc5
over	over	k1gInSc4
safety	safet	k2eAgFnPc1d1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
USA	USA	kA
Today	Todaa	k1gFnSc2
<g/>
.	.	kIx.
↑	↑	k?
http://vaperanks.com/e-cig-basics-what-is-a-cartomizer/	http://vaperanks.com/e-cig-basics-what-is-a-cartomizer/	k?
↑	↑	k?
Greg	Greg	k1gMnSc1
Olson	Olson	k1gMnSc1
(	(	kIx(
<g/>
29	#num#	k4
January	Januara	k1gFnSc2
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
“	“	k?
<g/>
Smoking	smoking	k1gInSc1
going	going	k1gMnSc1
electronic	electronice	k1gFnPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thetelegraph	Thetelegraph	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
https://www.rcplondon.ac.uk/commentary/what-you-need-know-about-electronic-cigarettes	https://www.rcplondon.ac.uk/commentary/what-you-need-know-about-electronic-cigarettes	k1gMnSc1
</s>
<s>
http://bjgp.org/content/64/626/442	http://bjgp.org/content/64/626/442	k4
</s>
<s>
http://www.thejournal.ie/electronic-cigarette-e-cigs-ban-research-1656347-Sep2014/	http://www.thejournal.ie/electronic-cigarette-e-cigs-ban-research-1656347-Sep2014/	k4
</s>
<s>
http://www.bbc.com/news/health-29061169	http://www.bbc.com/news/health-29061169	k4
</s>
<s>
http://www.bbc.com/news/health-27161965	http://www.bbc.com/news/health-27161965	k4
</s>
<s>
https://www.gov.uk/government/uploads/system/uploads/attachment_data/file/311887/Ecigarettes_report.pdf	https://www.gov.uk/government/uploads/system/uploads/attachment_data/file/311887/Ecigarettes_report.pdf	k1gMnSc1
</s>
