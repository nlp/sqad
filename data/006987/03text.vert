<s>
Louis	Louis	k1gMnSc1	Louis
Eugè	Eugè	k1gFnSc2	Eugè
Félix	Félix	k1gInSc1	Félix
Néel	Néel	k1gMnSc1	Néel
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc4	listopad
1904	[number]	k4	1904
Lyon	Lyon	k1gInSc4	Lyon
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
na	na	k7c6	na
École	Écola	k1gFnSc6	Écola
normale	normal	k1gMnSc5	normal
supérieure	supérieur	k1gMnSc5	supérieur
<g/>
.	.	kIx.	.
</s>
<s>
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
obdržel	obdržet	k5eAaPmAgInS	obdržet
společně	společně	k6eAd1	společně
se	s	k7c7	s
švédským	švédský	k2eAgMnSc7d1	švédský
astrofyzikem	astrofyzik	k1gMnSc7	astrofyzik
H.	H.	kA	H.
Alfvénem	Alfvén	k1gMnSc7	Alfvén
<g/>
)	)	kIx)	)
za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
magnetických	magnetický	k2eAgFnPc2d1	magnetická
vlastností	vlastnost	k1gFnPc2	vlastnost
pevných	pevný	k2eAgFnPc2d1	pevná
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
Néel	Néel	k1gMnSc1	Néel
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
domněnku	domněnka	k1gFnSc4	domněnka
existence	existence	k1gFnSc2	existence
magnetického	magnetický	k2eAgNnSc2d1	magnetické
chování	chování	k1gNnSc2	chování
zvaného	zvaný	k2eAgNnSc2d1	zvané
antiferomagnetismus	antiferomagnetismus	k1gInSc4	antiferomagnetismus
v	v	k7c6	v
protikladu	protiklad	k1gInSc6	protiklad
k	k	k7c3	k
feromagnetismu	feromagnetismus	k1gInSc3	feromagnetismus
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
chování	chování	k1gNnSc1	chování
mizí	mizet	k5eAaImIp3nS	mizet
nad	nad	k7c7	nad
určitou	určitý	k2eAgFnSc7d1	určitá
teplotou	teplota	k1gFnSc7	teplota
zvanou	zvaný	k2eAgFnSc7d1	zvaná
Néelova	Néelův	k2eAgFnSc1d1	Néelův
teplota	teplota	k1gFnSc1	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Néel	Néel	k1gMnSc1	Néel
také	také	k9	také
podal	podat	k5eAaPmAgMnS	podat
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
slabého	slabý	k2eAgNnSc2d1	slabé
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
určitých	určitý	k2eAgFnPc2d1	určitá
hornin	hornina	k1gFnPc2	hornina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
příčin	příčina	k1gFnPc2	příčina
vzniku	vznik	k1gInSc2	vznik
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Louis	louis	k1gInSc2	louis
Eugè	Eugè	k1gFnSc2	Eugè
Félix	Félix	k1gInSc1	Félix
Néel	Néel	k1gInSc1	Néel
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
