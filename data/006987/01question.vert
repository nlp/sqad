<s>
Kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
Louis	Louis	k1gMnSc1	Louis
Eugè	Eugè	k1gFnSc2	Eugè
Félix	Félix	k1gInSc1	Félix
Néel	Néel	k1gInSc1	Néel
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
?	?	kIx.	?
</s>
