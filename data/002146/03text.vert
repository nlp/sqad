<s>
Superman	superman	k1gMnSc1	superman
je	být	k5eAaImIp3nS	být
fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
postava	postava	k1gFnSc1	postava
komiksových	komiksový	k2eAgInPc2d1	komiksový
příběhů	příběh	k1gInPc2	příběh
vydávaných	vydávaný	k2eAgInPc2d1	vydávaný
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
DC	DC	kA	DC
Comics	comics	k1gInSc1	comics
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
výtvorem	výtvor	k1gInSc7	výtvor
tvůrčího	tvůrčí	k2eAgNnSc2d1	tvůrčí
dua	duo	k1gNnSc2	duo
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
tvořili	tvořit	k5eAaImAgMnP	tvořit
Jerry	Jerro	k1gNnPc7	Jerro
Siegel	Siegel	k1gMnSc1	Siegel
a	a	k8xC	a
Joe	Joe	k1gMnSc1	Joe
Shuster	Shuster	k1gMnSc1	Shuster
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
komiksovém	komiksový	k2eAgInSc6d1	komiksový
sešitu	sešit	k1gInSc6	sešit
Action	Action	k1gInSc1	Action
Comics	comics	k1gInSc1	comics
#	#	kIx~	#
<g/>
1	[number]	k4	1
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
byla	být	k5eAaImAgFnS	být
vymyšlena	vymyslet	k5eAaPmNgFnS	vymyslet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
kanadským	kanadský	k2eAgMnSc7d1	kanadský
kreslířem	kreslíř	k1gMnSc7	kreslíř
Joe	Joe	k1gMnSc7	Joe
Shusterem	Shuster	k1gMnSc7	Shuster
a	a	k8xC	a
americkým	americký	k2eAgMnSc7d1	americký
spisovatelem	spisovatel	k1gMnSc7	spisovatel
Jerry	Jerra	k1gFnSc2	Jerra
Siegelem	Siegel	k1gMnSc7	Siegel
během	běh	k1gInSc7	běh
jejich	jejich	k3xOp3gNnSc2	jejich
mládí	mládí	k1gNnSc2	mládí
v	v	k7c6	v
Clevelandu	Cleveland	k1gInSc6	Cleveland
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státě	stát	k1gInSc6	stát
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
nápad	nápad	k1gInSc4	nápad
prodali	prodat	k5eAaPmAgMnP	prodat
do	do	k7c2	do
Detective	Detectiv	k1gInSc5	Detectiv
Comics	comics	k1gInSc1	comics
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
a	a	k8xC	a
Superman	superman	k1gMnSc1	superman
debutoval	debutovat	k5eAaBmAgMnS	debutovat
v	v	k7c6	v
komiksu	komiks	k1gInSc6	komiks
"	"	kIx"	"
<g/>
Action	Action	k1gInSc1	Action
Comics	comics	k1gInSc1	comics
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
"	"	kIx"	"
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
postava	postava	k1gFnSc1	postava
Supermana	superman	k1gMnSc2	superman
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
rozhlasovém	rozhlasový	k2eAgInSc6d1	rozhlasový
seriálu	seriál	k1gInSc6	seriál
<g/>
,	,	kIx,	,
televizních	televizní	k2eAgInPc6d1	televizní
seriálech	seriál	k1gInPc6	seriál
<g/>
,	,	kIx,	,
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
novinových	novinový	k2eAgInPc6d1	novinový
stripech	strip	k1gInPc6	strip
a	a	k8xC	a
videohrách	videohra	k1gFnPc6	videohra
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
byly	být	k5eAaImAgInP	být
příběhy	příběh	k1gInPc1	příběh
Supermana	superman	k1gMnSc2	superman
vydány	vydat	k5eAaPmNgInP	vydat
v	v	k7c6	v
mnoha	mnoho	k4c2	mnoho
komiksech	komiks	k1gInPc6	komiks
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
syna	syn	k1gMnSc4	syn
vědce	vědec	k1gMnSc2	vědec
Jor-Ela	Jor-El	k1gMnSc2	Jor-El
a	a	k8xC	a
Lary	Lara	k1gMnSc2	Lara
Lor-Van	Lor-Vana	k1gFnPc2	Lor-Vana
z	z	k7c2	z
planety	planeta	k1gFnSc2	planeta
Krypton	krypton	k1gInSc1	krypton
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
rodné	rodný	k2eAgNnSc4d1	rodné
jméno	jméno	k1gNnSc4	jméno
je	být	k5eAaImIp3nS	být
Kal-	Kal-	k1gMnSc1	Kal-
<g/>
El.	El.	k1gMnSc1	El.
Jako	jako	k8xC	jako
malé	malý	k2eAgNnSc4d1	malé
dítě	dítě	k1gNnSc4	dítě
byl	být	k5eAaImAgInS	být
odeslán	odeslat	k5eAaPmNgInS	odeslat
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
svým	svůj	k1gMnSc7	svůj
otcem	otec	k1gMnSc7	otec
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
věděl	vědět	k5eAaImAgMnS	vědět
o	o	k7c4	o
blížící	blížící	k2eAgNnSc4d1	blížící
se	se	k3xPyFc4	se
zkáze	zkáza	k1gFnSc3	zkáza
jejich	jejich	k3xOp3gFnSc2	jejich
domovské	domovský	k2eAgFnSc2d1	domovská
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
ujali	ujmout	k5eAaPmAgMnP	ujmout
postarší	postarší	k2eAgMnPc1d1	postarší
bezdětní	bezdětný	k2eAgMnPc1d1	bezdětný
manželé	manžel	k1gMnPc1	manžel
Jonathan	Jonathan	k1gMnSc1	Jonathan
a	a	k8xC	a
Martha	Martha	k1gMnSc1	Martha
Kentovi	Kentův	k2eAgMnPc1d1	Kentův
a	a	k8xC	a
dali	dát	k5eAaPmAgMnP	dát
mu	on	k3xPp3gMnSc3	on
jméno	jméno	k1gNnSc4	jméno
Clark	Clark	k1gInSc4	Clark
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
fyzikálním	fyzikální	k2eAgInPc3d1	fyzikální
rozdílům	rozdíl	k1gInPc3	rozdíl
mezi	mezi	k7c7	mezi
planetami	planeta	k1gFnPc7	planeta
a	a	k8xC	a
evolučnímu	evoluční	k2eAgInSc3d1	evoluční
předstihu	předstih	k1gInSc3	předstih
Kryptoňanů	Kryptoňan	k1gMnPc2	Kryptoňan
oproti	oproti	k7c3	oproti
lidem	člověk	k1gMnPc3	člověk
má	mít	k5eAaImIp3nS	mít
Clark	Clark	k1gInSc4	Clark
neobyčejné	obyčejný	k2eNgFnSc2d1	neobyčejná
(	(	kIx(	(
<g/>
nadpřirozené	nadpřirozený	k2eAgFnSc2d1	nadpřirozená
<g/>
)	)	kIx)	)
schopnosti	schopnost	k1gFnSc2	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
jeho	jeho	k3xOp3gFnSc2	jeho
síly	síla	k1gFnSc2	síla
je	být	k5eAaImIp3nS	být
naše	náš	k3xOp1gNnSc1	náš
žluté	žlutý	k2eAgNnSc1d1	žluté
Slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Dokáže	dokázat	k5eAaPmIp3nS	dokázat
běžet	běžet	k5eAaImF	běžet
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
kulka	kulka	k1gFnSc1	kulka
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
nadlidskou	nadlidský	k2eAgFnSc4d1	nadlidská
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nezničitelný	zničitelný	k2eNgMnSc1d1	nezničitelný
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
citlivější	citlivý	k2eAgInPc4d2	citlivější
smysly	smysl	k1gInPc4	smysl
<g/>
,	,	kIx,	,
reflexy	reflex	k1gInPc4	reflex
<g/>
,	,	kIx,	,
výdrž	výdrž	k1gFnSc4	výdrž
a	a	k8xC	a
dokonalou	dokonalý	k2eAgFnSc4d1	dokonalá
regenerační	regenerační	k2eAgFnSc4d1	regenerační
schopnost	schopnost	k1gFnSc4	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Svýma	svůj	k3xOyFgNnPc7	svůj
rentgenovýma	rentgenový	k2eAgNnPc7d1	rentgenové
očima	oko	k1gNnPc7	oko
dokáže	dokázat	k5eAaPmIp3nS	dokázat
prohlédnout	prohlédnout	k5eAaPmF	prohlédnout
jakýkoli	jakýkoli	k3yIgInSc4	jakýkoli
materiál	materiál	k1gInSc4	materiál
kromě	kromě	k7c2	kromě
olova	olovo	k1gNnSc2	olovo
<g/>
.	.	kIx.	.
<g/>
Oči	oko	k1gNnPc1	oko
mu	on	k3xPp3gMnSc3	on
slouží	sloužit	k5eAaImIp3nP	sloužit
i	i	k9	i
jako	jako	k9	jako
dalekohled	dalekohled	k1gInSc1	dalekohled
<g/>
,	,	kIx,	,
teleskop	teleskop	k1gInSc1	teleskop
nebo	nebo	k8xC	nebo
mikroskop	mikroskop	k1gInSc1	mikroskop
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
očima	oko	k1gNnPc7	oko
metat	metat	k5eAaImF	metat
oheň	oheň	k1gInSc1	oheň
<g/>
,	,	kIx,	,
laserový	laserový	k2eAgInSc1d1	laserový
paprsek	paprsek	k1gInSc1	paprsek
nebo	nebo	k8xC	nebo
různé	různý	k2eAgFnPc1d1	různá
variace	variace	k1gFnPc1	variace
tepelného	tepelný	k2eAgNnSc2d1	tepelné
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
sluch	sluch	k1gInSc1	sluch
mu	on	k3xPp3gMnSc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
slyšet	slyšet	k5eAaImF	slyšet
<g/>
,	,	kIx,	,
volání	volání	k1gNnSc1	volání
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
i	i	k9	i
z	z	k7c2	z
hodně	hodně	k6eAd1	hodně
vzdáleného	vzdálený	k2eAgNnSc2d1	vzdálené
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Létání	létání	k1gNnSc1	létání
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc7	jeho
další	další	k2eAgFnSc7d1	další
schopností	schopnost	k1gFnSc7	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
za	za	k7c2	za
letu	let	k1gInSc2	let
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
takřka	takřka	k6eAd1	takřka
neomezená	omezený	k2eNgFnSc1d1	neomezená
a	a	k8xC	a
blíží	blížit	k5eAaImIp3nS	blížit
se	se	k3xPyFc4	se
rychlosti	rychlost	k1gFnSc3	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
dechem	dech	k1gInSc7	dech
dokáže	dokázat	k5eAaPmIp3nS	dokázat
cokoliv	cokoliv	k3yInSc4	cokoliv
zmrazit	zmrazit	k5eAaPmF	zmrazit
nebo	nebo	k8xC	nebo
odfouknout	odfouknout	k5eAaPmF	odfouknout
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc7	jeho
hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
energie	energie	k1gFnSc2	energie
je	být	k5eAaImIp3nS	být
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
nemusí	muset	k5eNaImIp3nS	muset
jíst	jíst	k5eAaImF	jíst
ani	ani	k8xC	ani
pít	pít	k5eAaImF	pít
a	a	k8xC	a
prakticky	prakticky	k6eAd1	prakticky
vůbec	vůbec	k9	vůbec
nestárne	stárnout	k5eNaImIp3nS	stárnout
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
omezená	omezený	k2eAgFnSc1d1	omezená
délkou	délka	k1gFnSc7	délka
života	život	k1gInSc2	život
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
jeho	jeho	k3xOp3gFnSc7	jeho
slabostí	slabost	k1gFnSc7	slabost
je	být	k5eAaImIp3nS	být
kryptonit	kryptonit	k1gInSc1	kryptonit
<g/>
,	,	kIx,	,
meteorit	meteorit	k1gInSc1	meteorit
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
radioaktivním	radioaktivní	k2eAgInSc7d1	radioaktivní
úlomkem	úlomek	k1gInSc7	úlomek
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
zničené	zničený	k2eAgFnSc2d1	zničená
rodné	rodný	k2eAgFnSc2d1	rodná
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
zelená	zelený	k2eAgFnSc1d1	zelená
forma	forma	k1gFnSc1	forma
může	moct	k5eAaImIp3nS	moct
Supermana	superman	k1gMnSc4	superman
i	i	k8xC	i
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
druhy	druh	k1gInPc1	druh
kryptonitu	kryptonit	k1gInSc2	kryptonit
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
ho	on	k3xPp3gNnSc4	on
mohou	moct	k5eAaImIp3nP	moct
nějak	nějak	k6eAd1	nějak
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
červený	červený	k2eAgInSc4d1	červený
<g/>
,	,	kIx,	,
černý	černý	k2eAgInSc4d1	černý
<g/>
,	,	kIx,	,
modrý	modrý	k2eAgInSc4d1	modrý
<g/>
,	,	kIx,	,
zlatý	zlatý	k2eAgInSc4d1	zlatý
a	a	k8xC	a
stříbrný	stříbrný	k2eAgInSc4d1	stříbrný
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
adoptivního	adoptivní	k2eAgMnSc2d1	adoptivní
otce	otec	k1gMnSc2	otec
se	se	k3xPyFc4	se
vydává	vydávat	k5eAaImIp3nS	vydávat
do	do	k7c2	do
města	město	k1gNnSc2	město
Metropolis	Metropolis	k1gFnSc2	Metropolis
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
pracuje	pracovat	k5eAaImIp3nS	pracovat
inkognito	inkognito	k6eAd1	inkognito
jako	jako	k9	jako
nesmělý	smělý	k2eNgMnSc1d1	nesmělý
a	a	k8xC	a
bojácný	bojácný	k2eAgMnSc1d1	bojácný
reportér	reportér	k1gMnSc1	reportér
deníku	deník	k1gInSc2	deník
Daily	Daila	k1gFnSc2	Daila
Planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
vzbuzující	vzbuzující	k2eAgInSc4d1	vzbuzující
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
neuměl	umět	k5eNaImAgMnS	umět
ani	ani	k8xC	ani
napočítat	napočítat	k5eAaPmF	napočítat
do	do	k7c2	do
pěti	pět	k4xCc2	pět
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
kdykoliv	kdykoliv	k6eAd1	kdykoliv
je	být	k5eAaImIp3nS	být
připraven	připravit	k5eAaPmNgMnS	připravit
proměnit	proměnit	k5eAaPmF	proměnit
se	se	k3xPyFc4	se
v	v	k7c4	v
muže	muž	k1gMnPc4	muž
v	v	k7c6	v
modrém	modrý	k2eAgNnSc6d1	modré
kostýmu	kostým	k1gInSc6	kostým
s	s	k7c7	s
červenou	červený	k2eAgFnSc7d1	červená
pláštěnkou	pláštěnka	k1gFnSc7	pláštěnka
-	-	kIx~	-
Supermana	superman	k1gMnSc2	superman
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vždy	vždy	k6eAd1	vždy
bojuje	bojovat	k5eAaImIp3nS	bojovat
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
a	a	k8xC	a
dobra	dobro	k1gNnSc2	dobro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zaměstnání	zaměstnání	k1gNnSc6	zaměstnání
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
atraktivní	atraktivní	k2eAgFnSc7d1	atraktivní
žurnalistkou	žurnalistka	k1gFnSc7	žurnalistka
Lois	Loisa	k1gFnPc2	Loisa
Laneovou	Laneův	k2eAgFnSc7d1	Laneova
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
ženským	ženský	k2eAgInSc7d1	ženský
doplňkem	doplněk	k1gInSc7	doplněk
respektive	respektive	k9	respektive
protějškem	protějšek	k1gInSc7	protějšek
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
komiksová	komiksový	k2eAgFnSc1d1	komiksová
postava	postava	k1gFnSc1	postava
Supergirl	Supergirla	k1gFnPc2	Supergirla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
vydává	vydávat	k5eAaImIp3nS	vydávat
komiksové	komiksový	k2eAgFnSc2d1	komiksová
knihy	kniha	k1gFnSc2	kniha
Superman	superman	k1gMnSc1	superman
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
BB	BB	kA	BB
<g/>
/	/	kIx~	/
<g/>
art	art	k?	art
a	a	k8xC	a
CREW	CREW	kA	CREW
<g/>
.	.	kIx.	.
</s>
<s>
Superman	superman	k1gMnSc1	superman
-	-	kIx~	-
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
s	s	k7c7	s
Mužem	muž	k1gMnSc7	muž
zítřka	zítřek	k1gInSc2	zítřek
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
-	-	kIx~	-
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
Alan	Alan	k1gMnSc1	Alan
Moore	Moor	k1gInSc5	Moor
a	a	k8xC	a
Curt	Curt	k2eAgMnSc1d1	Curt
Swan	Swan	k1gMnSc1	Swan
<g/>
:	:	kIx,	:
Superman	superman	k1gMnSc1	superman
#	#	kIx~	#
<g/>
423	[number]	k4	423
a	a	k8xC	a
Action	Action	k1gInSc1	Action
Comics	comics	k1gInSc1	comics
#	#	kIx~	#
<g/>
583	[number]	k4	583
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
Superman	superman	k1gMnSc1	superman
-	-	kIx~	-
Rudá	rudý	k2eAgFnSc1d1	rudá
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
-	-	kIx~	-
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
Mark	Mark	k1gMnSc1	Mark
Millar	Millar	k1gMnSc1	Millar
a	a	k8xC	a
Dave	Dav	k1gInSc5	Dav
Johnson	Johnson	k1gMnSc1	Johnson
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Superman	superman	k1gMnSc1	superman
<g/>
:	:	kIx,	:
Red	Red	k1gFnSc1	Red
Son	son	k1gInSc1	son
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Superman	superman	k1gMnSc1	superman
pro	pro	k7c4	pro
zítřek	zítřek	k1gInSc4	zítřek
-	-	kIx~	-
kniha	kniha	k1gFnSc1	kniha
první	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
-	-	kIx~	-
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
Brian	Brian	k1gMnSc1	Brian
Azzarello	Azzarello	k1gNnSc4	Azzarello
a	a	k8xC	a
Jim	on	k3xPp3gMnPc3	on
Lee	Lea	k1gFnSc3	Lea
<g/>
:	:	kIx,	:
Superman	superman	k1gMnSc1	superman
#	#	kIx~	#
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
209	[number]	k4	209
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
2004	[number]	k4	2004
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
Superman	superman	k1gMnSc1	superman
pro	pro	k7c4	pro
zítřek	zítřek	k1gInSc4	zítřek
-	-	kIx~	-
kniha	kniha	k1gFnSc1	kniha
druhá	druhý	k4xOgFnSc1	druhý
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
-	-	kIx~	-
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
Brian	Brian	k1gMnSc1	Brian
Azzarello	Azzarello	k1gNnSc4	Azzarello
a	a	k8xC	a
Jim	on	k3xPp3gMnPc3	on
Lee	Lea	k1gFnSc3	Lea
<g/>
:	:	kIx,	:
Superman	superman	k1gMnSc1	superman
#	#	kIx~	#
<g/>
210	[number]	k4	210
<g/>
-	-	kIx~	-
<g/>
215	[number]	k4	215
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Superman	superman	k1gMnSc1	superman
-	-	kIx~	-
Poslední	poslední	k2eAgMnSc1d1	poslední
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
-	-	kIx~	-
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
Geoff	Geoff	k1gInSc1	Geoff
Johns	Johns	k1gInSc1	Johns
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Donner	Donner	k1gMnSc1	Donner
a	a	k8xC	a
Adam	Adam	k1gMnSc1	Adam
Kubert	Kubert	k1gMnSc1	Kubert
<g/>
:	:	kIx,	:
Action	Action	k1gInSc1	Action
Comics	comics	k1gInSc1	comics
#	#	kIx~	#
<g/>
844	[number]	k4	844
<g/>
-	-	kIx~	-
<g/>
847	[number]	k4	847
<g/>
,	,	kIx,	,
851	[number]	k4	851
a	a	k8xC	a
Action	Action	k1gInSc1	Action
Comics	comics	k1gInSc1	comics
Annual	Annual	k1gInSc1	Annual
#	#	kIx~	#
<g/>
11	[number]	k4	11
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g />
.	.	kIx.	.
</s>
<s>
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
Superman	superman	k1gMnSc1	superman
-	-	kIx~	-
Utajený	utajený	k2eAgInSc1d1	utajený
počátek	počátek	k1gInSc1	počátek
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
-	-	kIx~	-
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
Geoff	Geoff	k1gInSc1	Geoff
Johns	Johnsa	k1gFnPc2	Johnsa
a	a	k8xC	a
Gary	Gara	k1gFnSc2	Gara
Frank	Frank	k1gMnSc1	Frank
<g/>
:	:	kIx,	:
Superman	superman	k1gMnSc1	superman
<g/>
:	:	kIx,	:
Secret	Secret	k1gMnSc1	Secret
Origin	Origin	k1gMnSc1	Origin
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
Superman	superman	k1gMnSc1	superman
1	[number]	k4	1
<g/>
:	:	kIx,	:
Cena	cena	k1gFnSc1	cena
<g />
.	.	kIx.	.
</s>
<s>
zítřka	zítřek	k1gInSc2	zítřek
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
-	-	kIx~	-
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
George	George	k1gInSc1	George
Pérez	Pérez	k1gInSc1	Pérez
a	a	k8xC	a
Jesús	Jesús	k1gInSc1	Jesús
Merino	merino	k1gNnSc2	merino
<g/>
:	:	kIx,	:
Superman	superman	k1gMnSc1	superman
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
Superman	superman	k1gMnSc1	superman
2	[number]	k4	2
<g/>
:	:	kIx,	:
Tajnosti	tajnost	k1gFnPc4	tajnost
a	a	k8xC	a
lži	lež	k1gFnPc4	lež
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
-	-	kIx~	-
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
Dan	Dan	k1gMnSc1	Dan
Jurgens	Jurgens	k1gInSc4	Jurgens
<g/>
,	,	kIx,	,
Keith	Keith	k1gInSc4	Keith
Giffen	Giffna	k1gFnPc2	Giffna
a	a	k8xC	a
Jesús	Jesús	k1gInSc1	Jesús
Merino	merino	k1gNnSc2	merino
<g/>
:	:	kIx,	:
Superman	superman	k1gMnSc1	superman
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
#	#	kIx~	#
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
a	a	k8xC	a
Annual	Annual	k1gMnSc1	Annual
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Superman	superman	k1gMnSc1	superman
/	/	kIx~	/
Action	Action	k1gInSc1	Action
comics	comics	k1gInSc1	comics
1	[number]	k4	1
<g/>
:	:	kIx,	:
Superman	superman	k1gMnSc1	superman
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
oceli	ocel	k1gFnSc2	ocel
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
2013	[number]	k4	2013
-	-	kIx~	-
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
Grant	grant	k1gInSc1	grant
Morrison	Morrisona	k1gFnPc2	Morrisona
<g/>
,	,	kIx,	,
Andy	Anda	k1gFnSc2	Anda
Kubert	Kubert	k1gInSc1	Kubert
a	a	k8xC	a
Rags	Rags	k1gInSc1	Rags
Morales	Morales	k1gMnSc1	Morales
<g/>
:	:	kIx,	:
Superman	superman	k1gMnSc1	superman
and	and	k?	and
the	the	k?	the
Men	Men	k1gMnSc1	Men
of	of	k?	of
Steel	Steel	k1gMnSc1	Steel
(	(	kIx(	(
<g/>
Action	Action	k1gInSc1	Action
Comics	comics	k1gInSc1	comics
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Superman	superman	k1gMnSc1	superman
/	/	kIx~	/
Action	Action	k1gInSc1	Action
comics	comics	k1gInSc1	comics
2	[number]	k4	2
<g/>
:	:	kIx,	:
Neprůstřelný	průstřelný	k2eNgMnSc1d1	neprůstřelný
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
-	-	kIx~	-
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
Grant	grant	k1gInSc1	grant
Morrison	Morrisona	k1gFnPc2	Morrisona
a	a	k8xC	a
Rags	Ragsa	k1gFnPc2	Ragsa
Morales	Moralesa	k1gFnPc2	Moralesa
<g/>
:	:	kIx,	:
Bulletproof	Bulletproof	k1gInSc1	Bulletproof
(	(	kIx(	(
<g/>
Action	Action	k1gInSc1	Action
Comics	comics	k1gInSc1	comics
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
a	a	k8xC	a
Annual	Annual	k1gMnSc1	Annual
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Superman	superman	k1gMnSc1	superman
/	/	kIx~	/
Action	Action	k1gInSc1	Action
comics	comics	k1gInSc1	comics
3	[number]	k4	3
<g/>
:	:	kIx,	:
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
-	-	kIx~	-
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
<g/>
:	:	kIx,	:
Grant	grant	k1gInSc1	grant
Morrison	Morrison	k1gInSc1	Morrison
<g/>
,	,	kIx,	,
Rags	Rags	k1gInSc1	Rags
Morales	Moralesa	k1gFnPc2	Moralesa
a	a	k8xC	a
Brad	brada	k1gFnPc2	brada
Walker	Walker	k1gMnSc1	Walker
<g/>
:	:	kIx,	:
At	At	k1gMnSc1	At
The	The	k1gMnSc1	The
End	End	k1gMnSc1	End
of	of	k?	of
Days	Days	k1gInSc1	Days
(	(	kIx(	(
<g/>
Action	Action	k1gInSc1	Action
Comics	comics	k1gInSc1	comics
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
#	#	kIx~	#
<g/>
13	[number]	k4	13
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
)	)	kIx)	)
Superman	superman	k1gMnSc1	superman
Nespoutaný	spoutaný	k2eNgMnSc1d1	nespoutaný
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
-	-	kIx~	-
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
Scott	Scott	k1gMnSc1	Scott
Snyder	Snyder	k1gMnSc1	Snyder
a	a	k8xC	a
Jim	on	k3xPp3gMnPc3	on
Lee	Lea	k1gFnSc3	Lea
<g/>
:	:	kIx,	:
Superman	superman	k1gMnSc1	superman
Unchained	Unchained	k1gMnSc1	Unchained
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Superman	superman	k1gMnSc1	superman
<g />
.	.	kIx.	.
</s>
<s>
Nespoutaný	spoutaný	k2eNgMnSc1d1	nespoutaný
2	[number]	k4	2
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
-	-	kIx~	-
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
Scott	Scott	k1gMnSc1	Scott
Snyder	Snyder	k1gMnSc1	Snyder
a	a	k8xC	a
Jim	on	k3xPp3gMnPc3	on
Lee	Lea	k1gFnSc3	Lea
<g/>
:	:	kIx,	:
Superman	superman	k1gMnSc1	superman
Unchained	Unchained	k1gMnSc1	Unchained
#	#	kIx~	#
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Superman	superman	k1gMnSc1	superman
<g/>
:	:	kIx,	:
Země	země	k1gFnSc1	země
jedna	jeden	k4xCgFnSc1	jeden
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
-	-	kIx~	-
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
J.	J.	kA	J.
Michael	Michael	k1gMnSc1	Michael
Straczynski	Straczynsk	k1gFnSc2	Straczynsk
a	a	k8xC	a
Shane	Shan	k1gInSc5	Shan
Davis	Davis	k1gFnSc1	Davis
<g/>
:	:	kIx,	:
Superman	superman	k1gMnSc1	superman
<g/>
:	:	kIx,	:
Earth	Earth	k1gMnSc1	Earth
One	One	k1gMnSc1	One
(	(	kIx(	(
<g/>
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
1948	[number]	k4	1948
Superman	superman	k1gMnSc1	superman
-	-	kIx~	-
filmový	filmový	k2eAgInSc1d1	filmový
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Spencer	Spencer	k1gMnSc1	Spencer
Gordon	Gordon	k1gMnSc1	Gordon
Bennet	Bennet	k1gMnSc1	Bennet
a	a	k8xC	a
Thomas	Thomas	k1gMnSc1	Thomas
Carr	Carr	k1gMnSc1	Carr
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Kirk	Kirk	k1gMnSc1	Kirk
Alyn	Alyn	k1gMnSc1	Alyn
1950	[number]	k4	1950
Atom	atom	k1gInSc1	atom
Man	Man	k1gMnSc1	Man
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Superman	superman	k1gMnSc1	superman
-	-	kIx~	-
filmový	filmový	k2eAgInSc1d1	filmový
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Spencer	Spencer	k1gMnSc1	Spencer
Gordon	Gordon	k1gMnSc1	Gordon
Bennet	Bennet	k1gMnSc1	Bennet
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Kirk	Kirk	k1gMnSc1	Kirk
Alyn	Alyn	k1gInSc4	Alyn
1951	[number]	k4	1951
Superman	superman	k1gMnSc1	superman
and	and	k?	and
the	the	k?	the
Mole	mol	k1gMnSc5	mol
Men	Men	k1gMnSc5	Men
-	-	kIx~	-
režie	režie	k1gFnSc1	režie
Lee	Lea	k1gFnSc3	Lea
Sholem	Sholo	k1gNnSc7	Sholo
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
George	George	k1gNnSc2	George
Reeves	Reeves	k1gInSc4	Reeves
1954	[number]	k4	1954
Stamp	Stamp	k1gInSc4	Stamp
Day	Day	k1gFnSc2	Day
for	forum	k1gNnPc2	forum
Superman	superman	k1gMnSc1	superman
-	-	kIx~	-
krátký	krátký	k2eAgInSc1d1	krátký
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Thomas	Thomas	k1gMnSc1	Thomas
Carr	Carr	k1gMnSc1	Carr
<g/>
,	,	kIx,	,
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
George	Georg	k1gFnSc2	Georg
Reeves	Reeves	k1gInSc4	Reeves
1978	[number]	k4	1978
Superman	superman	k1gMnSc1	superman
-	-	kIx~	-
režie	režie	k1gFnSc1	režie
Richard	Richard	k1gMnSc1	Richard
Donner	Donner	k1gMnSc1	Donner
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Christopher	Christophra	k1gFnPc2	Christophra
Reeve	Reeev	k1gFnSc2	Reeev
1980	[number]	k4	1980
Superman	superman	k1gMnSc1	superman
2	[number]	k4	2
-	-	kIx~	-
režie	režie	k1gFnSc1	režie
Richard	Richard	k1gMnSc1	Richard
Lester	Lester	k1gMnSc1	Lester
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Christopher	Christophra	k1gFnPc2	Christophra
Reeve	Reeev	k1gFnSc2	Reeev
2006	[number]	k4	2006
Superman	superman	k1gMnSc1	superman
II	II	kA	II
<g/>
:	:	kIx,	:
Verze	verze	k1gFnSc1	verze
Richarda	Richard	k1gMnSc2	Richard
Donnera	Donner	k1gMnSc2	Donner
-	-	kIx~	-
upravený	upravený	k2eAgInSc1d1	upravený
režisérský	režisérský	k2eAgInSc1d1	režisérský
sestřih	sestřih	k1gInSc1	sestřih
podle	podle	k7c2	podle
původního	původní	k2eAgInSc2d1	původní
scénáře	scénář	k1gInSc2	scénář
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Richard	Richard	k1gMnSc1	Richard
<g />
.	.	kIx.	.
</s>
<s>
Donner	Donner	k1gInSc1	Donner
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Christopher	Christophra	k1gFnPc2	Christophra
Reeve	Reeev	k1gFnSc2	Reeev
1983	[number]	k4	1983
Superman	superman	k1gMnSc1	superman
3	[number]	k4	3
-	-	kIx~	-
režie	režie	k1gFnSc1	režie
Richard	Richard	k1gMnSc1	Richard
Lester	Lester	k1gMnSc1	Lester
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Christopher	Christophra	k1gFnPc2	Christophra
Reeve	Reeev	k1gFnSc2	Reeev
1984	[number]	k4	1984
Superdívka	Superdívka	k1gFnSc1	Superdívka
-	-	kIx~	-
odvozený	odvozený	k2eAgInSc1d1	odvozený
film	film	k1gInSc1	film
o	o	k7c6	o
Supermanově	supermanův	k2eAgFnSc6d1	Supermanova
sestřenici	sestřenice	k1gFnSc6	sestřenice
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Jeannot	Jeannota	k1gFnPc2	Jeannota
Szwarc	Szwarc	k1gFnSc1	Szwarc
<g/>
,	,	kIx,	,
v	v	k7c4	v
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
Helen	Helena	k1gFnPc2	Helena
Slater	Slatrum	k1gNnPc2	Slatrum
1987	[number]	k4	1987
Superman	superman	k1gMnSc1	superman
4	[number]	k4	4
-	-	kIx~	-
režie	režie	k1gFnSc1	režie
Sidney	Sidnea	k1gFnSc2	Sidnea
J.	J.	kA	J.
Furie	Furie	k1gFnSc2	Furie
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Christopher	Christophra	k1gFnPc2	Christophra
Reeve	Reeev	k1gFnSc2	Reeev
2006	[number]	k4	2006
Superman	superman	k1gMnSc1	superman
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
-	-	kIx~	-
režie	režie	k1gFnSc1	režie
Bryan	Bryan	k1gMnSc1	Bryan
Singer	Singer	k1gMnSc1	Singer
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Brandon	Brandon	k1gMnSc1	Brandon
Routh	Routh	k1gInSc4	Routh
2013	[number]	k4	2013
Muž	muž	k1gMnSc1	muž
z	z	k7c2	z
oceli	ocel	k1gFnSc2	ocel
-	-	kIx~	-
režie	režie	k1gFnSc1	režie
Zack	Zack	k1gMnSc1	Zack
Snyder	Snyder	k1gMnSc1	Snyder
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Henry	Henry	k1gMnSc1	Henry
Cavill	Cavill	k1gInSc4	Cavill
2016	[number]	k4	2016
Batman	Batman	k1gMnSc1	Batman
v	v	k7c6	v
Superman	superman	k1gMnSc1	superman
<g/>
:	:	kIx,	:
Úsvit	úsvit	k1gInSc1	úsvit
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
-	-	kIx~	-
režie	režie	k1gFnSc1	režie
Zack	Zack	k1gMnSc1	Zack
Snyder	Snyder	k1gMnSc1	Snyder
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Henry	Henry	k1gMnSc1	Henry
Cavill	Cavill	k1gMnSc1	Cavill
1952	[number]	k4	1952
<g/>
-	-	kIx~	-
<g/>
1952	[number]	k4	1952
Adventures	Adventures	k1gMnSc1	Adventures
of	of	k?	of
Superman	superman	k1gMnSc1	superman
-	-	kIx~	-
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
104	[number]	k4	104
dílů	díl	k1gInPc2	díl
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
George	George	k1gNnSc2	George
Reeves	Reeves	k1gInSc4	Reeves
1958	[number]	k4	1958
The	The	k1gMnSc1	The
Adventures	Adventures	k1gMnSc1	Adventures
of	of	k?	of
Superpup	Superpup	k1gInSc1	Superpup
-	-	kIx~	-
neodvysílaný	odvysílaný	k2eNgInSc1d1	neodvysílaný
pilotní	pilotní	k2eAgInSc1d1	pilotní
díl	díl	k1gInSc1	díl
odvozeného	odvozený	k2eAgInSc2d1	odvozený
seriálu	seriál	k1gInSc2	seriál
o	o	k7c6	o
Superpupovi	Superpup	k1gMnSc6	Superpup
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Billy	Bill	k1gMnPc4	Bill
Curtis	Curtis	k1gFnSc2	Curtis
1961	[number]	k4	1961
The	The	k1gMnSc1	The
Adventures	Adventures	k1gMnSc1	Adventures
<g />
.	.	kIx.	.
</s>
<s>
of	of	k?	of
Superboy	Superboa	k1gFnPc1	Superboa
-	-	kIx~	-
neodvysílaný	odvysílaný	k2eNgInSc4d1	neodvysílaný
pilotní	pilotní	k2eAgInSc4d1	pilotní
díl	díl	k1gInSc4	díl
seriálu	seriál	k1gInSc2	seriál
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Johnny	Johnna	k1gFnSc2	Johnna
Rockwell	Rockwell	k1gInSc4	Rockwell
1975	[number]	k4	1975
It	It	k1gFnPc2	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
a	a	k8xC	a
Bird	Bird	k1gMnSc1	Bird
<g/>
...	...	k?	...
<g/>
It	It	k1gMnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
a	a	k8xC	a
Plane	planout	k5eAaImIp3nS	planout
<g/>
...	...	k?	...
<g/>
It	It	k1gFnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Superman	superman	k1gMnSc1	superman
-	-	kIx~	-
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
adaptace	adaptace	k1gFnSc1	adaptace
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
broadwayského	broadwayský	k2eAgInSc2d1	broadwayský
muzikálu	muzikál	k1gInSc2	muzikál
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
David	David	k1gMnSc1	David
Wilson	Wilson	k1gMnSc1	Wilson
1988	[number]	k4	1988
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
Superboy	Superboa	k1gFnPc1	Superboa
-	-	kIx~	-
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
100	[number]	k4	100
dílů	díl	k1gInPc2	díl
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
John	John	k1gMnSc1	John
Newton	Newton	k1gMnSc1	Newton
a	a	k8xC	a
Gerard	Gerard	k1gMnSc1	Gerard
Christopher	Christophra	k1gFnPc2	Christophra
1993	[number]	k4	1993
<g/>
-	-	kIx~	-
<g/>
1997	[number]	k4	1997
Superman	superman	k1gMnSc1	superman
-	-	kIx~	-
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
88	[number]	k4	88
dílů	díl	k1gInPc2	díl
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Dean	Dean	k1gMnSc1	Dean
Cain	Cain	k1gMnSc1	Cain
2001	[number]	k4	2001
<g/>
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
Smallville	Smallville	k1gInSc1	Smallville
-	-	kIx~	-
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
218	[number]	k4	218
dílů	díl	k1gInPc2	díl
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Tom	Tom	k1gMnSc1	Tom
Welling	Welling	k1gInSc4	Welling
2015	[number]	k4	2015
<g/>
-	-	kIx~	-
<g/>
dosud	dosud	k6eAd1	dosud
Supergirl	Supergirl	k1gInSc1	Supergirl
-	-	kIx~	-
odvozený	odvozený	k2eAgInSc1d1	odvozený
seriál	seriál	k1gInSc1	seriál
o	o	k7c6	o
Supermanově	supermanův	k2eAgFnSc6d1	Supermanova
sestřenici	sestřenice	k1gFnSc6	sestřenice
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Melissa	Melissa	k1gFnSc1	Melissa
Benoist	Benoist	k1gInSc4	Benoist
<g/>
,	,	kIx,	,
v	v	k7c6	v
roli	role	k1gFnSc6	role
Supermana	superman	k1gMnSc2	superman
Tyler	Tyler	k1gInSc4	Tyler
Hoechlin	Hoechlina	k1gFnPc2	Hoechlina
1966	[number]	k4	1966
<g/>
-	-	kIx~	-
<g/>
1970	[number]	k4	1970
The	The	k1gMnSc1	The
New	New	k1gMnSc1	New
<g />
.	.	kIx.	.
</s>
<s>
Adventures	Adventures	k1gMnSc1	Adventures
of	of	k?	of
Superman	superman	k1gMnSc1	superman
<g/>
,	,	kIx,	,
68	[number]	k4	68
epizod	epizoda	k1gFnPc2	epizoda
1988	[number]	k4	1988
Superman	superman	k1gMnSc1	superman
<g/>
,	,	kIx,	,
13	[number]	k4	13
epizod	epizoda	k1gFnPc2	epizoda
1996	[number]	k4	1996
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
Superman	superman	k1gMnSc1	superman
(	(	kIx(	(
<g/>
též	též	k9	též
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
Superman	superman	k1gMnSc1	superman
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Animated	Animated	k1gMnSc1	Animated
Series	Series	k1gMnSc1	Series
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
54	[number]	k4	54
epizod	epizoda	k1gFnPc2	epizoda
2006	[number]	k4	2006
-	-	kIx~	-
Superman	superman	k1gMnSc1	superman
<g/>
:	:	kIx,	:
Brainiac	Brainiac	k1gFnSc1	Brainiac
Attacks	Attacks	k1gInSc1	Attacks
<g/>
,	,	kIx,	,
TV	TV	kA	TV
film	film	k1gInSc4	film
2007	[number]	k4	2007
-	-	kIx~	-
Superman	superman	k1gMnSc1	superman
<g/>
:	:	kIx,	:
Soudný	soudný	k2eAgInSc1d1	soudný
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
TV	TV	kA	TV
film	film	k1gInSc4	film
2009	[number]	k4	2009
-	-	kIx~	-
Superman	superman	k1gMnSc1	superman
<g/>
/	/	kIx~	/
<g/>
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Veřejní	veřejný	k2eAgMnPc1d1	veřejný
nepřátelé	nepřítel	k1gMnPc1	nepřítel
<g/>
,	,	kIx,	,
TV	TV	kA	TV
film	film	k1gInSc4	film
2010	[number]	k4	2010
-	-	kIx~	-
Superman	superman	k1gMnSc1	superman
<g/>
/	/	kIx~	/
<g/>
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Apokalypsa	apokalypsa	k1gFnSc1	apokalypsa
<g/>
,	,	kIx,	,
TV	TV	kA	TV
film	film	k1gInSc4	film
2010	[number]	k4	2010
-	-	kIx~	-
Superman	superman	k1gMnSc1	superman
<g/>
/	/	kIx~	/
<g/>
Shazam	Shazam	k1gInSc1	Shazam
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
:	:	kIx,	:
Návrat	návrat	k1gInSc1	návrat
černého	černý	k1gMnSc2	černý
Adama	Adam	k1gMnSc2	Adam
<g/>
,	,	kIx,	,
TV	TV	kA	TV
film	film	k1gInSc4	film
2011	[number]	k4	2011
-	-	kIx~	-
Superhvězda	superhvězda	k1gFnSc1	superhvězda
Superman	superman	k1gMnSc1	superman
<g/>
,	,	kIx,	,
TV	TV	kA	TV
film	film	k1gInSc4	film
2012	[number]	k4	2012
-	-	kIx~	-
Superman	superman	k1gMnSc1	superman
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Elita	elita	k1gFnSc1	elita
<g/>
,	,	kIx,	,
TV	TV	kA	TV
film	film	k1gInSc4	film
2013	[number]	k4	2013
-	-	kIx~	-
Superman	superman	k1gMnSc1	superman
<g/>
:	:	kIx,	:
Unbound	Unbound	k1gMnSc1	Unbound
<g/>
,	,	kIx,	,
TV	TV	kA	TV
film	film	k1gInSc4	film
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Superman	superman	k1gMnSc1	superman
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
