<p>
<s>
Kazeta	kazeta	k1gFnSc1	kazeta
(	(	kIx(	(
<g/>
z	z	k7c2	z
ital	itala	k1gFnPc2	itala
<g/>
.	.	kIx.	.
cassetto	cassetto	k1gNnSc1	cassetto
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
přihrádka	přihrádka	k1gFnSc1	přihrádka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ozdobný	ozdobný	k2eAgInSc4d1	ozdobný
architektonický	architektonický	k2eAgInSc4d1	architektonický
prvek	prvek	k1gInSc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vpadlý	vpadlý	k2eAgInSc4d1	vpadlý
panel	panel	k1gInSc4	panel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
čtverce	čtverec	k1gInSc2	čtverec
<g/>
,	,	kIx,	,
obdélníku	obdélník	k1gInSc2	obdélník
nebo	nebo	k8xC	nebo
osmiúhelníku	osmiúhelník	k1gInSc2	osmiúhelník
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
výrazný	výrazný	k2eAgInSc4d1	výrazný
dekorativní	dekorativní	k2eAgInSc4d1	dekorativní
prvek	prvek	k1gInSc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Kazety	kazeta	k1gFnPc1	kazeta
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
řadí	řadit	k5eAaImIp3nS	řadit
vedle	vedle	k7c2	vedle
sebe	se	k3xPyFc2	se
do	do	k7c2	do
pásu	pás	k1gInSc2	pás
nebo	nebo	k8xC	nebo
mřížky	mřížka	k1gFnSc2	mřížka
a	a	k8xC	a
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
výzdobě	výzdoba	k1gFnSc3	výzdoba
stropu	strop	k1gInSc2	strop
<g/>
,	,	kIx,	,
klenby	klenba	k1gFnSc2	klenba
nebo	nebo	k8xC	nebo
oblouku	oblouk	k1gInSc2	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Kazetový	kazetový	k2eAgInSc1d1	kazetový
strop	strop	k1gInSc1	strop
může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
například	například	k6eAd1	například
ze	z	k7c2	z
štuku	štuk	k1gInSc2	štuk
či	či	k8xC	či
betonu	beton	k1gInSc2	beton
<g/>
,	,	kIx,	,
křížením	křížení	k1gNnSc7	křížení
trámů	trám	k1gInPc2	trám
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
jako	jako	k9	jako
dřevěné	dřevěný	k2eAgNnSc4d1	dřevěné
obložení	obložení	k1gNnSc4	obložení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
W.	W.	kA	W.
Koch	Koch	k1gMnSc1	Koch
<g/>
,	,	kIx,	,
Evropská	evropský	k2eAgFnSc1d1	Evropská
architektura	architektura	k1gFnSc1	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Universum	universum	k1gNnSc1	universum
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
452	[number]	k4	452
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Kupole	kupole	k1gFnSc1	kupole
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kazeta	kazeta	k1gFnSc1	kazeta
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
