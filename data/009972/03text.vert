<p>
<s>
Sv.	sv.	kA	sv.
Lucius	Lucius	k1gMnSc1	Lucius
I.	I.	kA	I.
(	(	kIx(	(
<g/>
zemřel	zemřít	k5eAaPmAgMnS	zemřít
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
254	[number]	k4	254
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
22	[number]	k4	22
<g/>
.	.	kIx.	.
papežem	papež	k1gMnSc7	papež
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
v	v	k7c6	v
letech	let	k1gInPc6	let
253	[number]	k4	253
<g/>
–	–	k?	–
<g/>
254	[number]	k4	254
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
rodině	rodina	k1gFnSc6	rodina
není	být	k5eNaImIp3nS	být
nic	nic	k3yNnSc1	nic
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
údajně	údajně	k6eAd1	údajně
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Porfyrián	Porfyrián	k1gMnSc1	Porfyrián
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Papežem	Papež	k1gMnSc7	Papež
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
253	[number]	k4	253
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
nové	nový	k2eAgFnSc2d1	nová
vlny	vlna	k1gFnSc2	vlna
pronásledování	pronásledování	k1gNnSc1	pronásledování
křesťanů	křesťan	k1gMnPc2	křesťan
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Treboniana	Trebonian	k1gMnSc2	Trebonian
Galla	Gall	k1gMnSc2	Gall
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgNnSc3	tento
pronásledování	pronásledování	k1gNnSc3	pronásledování
předcházelo	předcházet	k5eAaImAgNnS	předcházet
vyhoštění	vyhoštění	k1gNnSc1	vyhoštění
Luciova	Luciův	k2eAgMnSc2d1	Luciův
předchůdce	předchůdce	k1gMnSc2	předchůdce
<g/>
,	,	kIx,	,
papeže	papež	k1gMnSc2	papež
Kornelia	Kornelius	k1gMnSc2	Kornelius
<g/>
.	.	kIx.	.
</s>
<s>
Lucius	Lucius	k1gInSc1	Lucius
I.	I.	kA	I.
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
vyhoštěn	vyhoštěn	k2eAgInSc1d1	vyhoštěn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
římským	římský	k2eAgMnSc7d1	římský
císařem	císař	k1gMnSc7	císař
stal	stát	k5eAaPmAgMnS	stát
Valerianus	Valerianus	k1gMnSc1	Valerianus
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
povolení	povolení	k1gNnPc4	povolení
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Návrat	návrat	k1gInSc1	návrat
z	z	k7c2	z
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
je	být	k5eAaImIp3nS	být
počítán	počítat	k5eAaImNgMnS	počítat
k	k	k7c3	k
zázrakům	zázrak	k1gInPc3	zázrak
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
Luciově	Luciův	k2eAgFnSc3d1	Luciova
svatořečení	svatořečení	k1gNnSc2	svatořečení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lucius	Lucius	k1gInSc1	Lucius
I.	I.	kA	I.
byl	být	k5eAaImAgInS	být
římským	římský	k2eAgMnSc7d1	římský
biskupem	biskup	k1gMnSc7	biskup
pouhých	pouhý	k2eAgInPc2d1	pouhý
8	[number]	k4	8
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Dochovalo	dochovat	k5eAaPmAgNnS	dochovat
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
dopisů	dopis	k1gInPc2	dopis
svatému	svatý	k1gMnSc3	svatý
Cypriánovi	Cyprián	k1gMnSc3	Cyprián
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
zastává	zastávat	k5eAaImIp3nS	zastávat
mírné	mírný	k2eAgInPc4d1	mírný
názory	názor	k1gInPc4	názor
svých	svůj	k3xOyFgMnPc2	svůj
předchůdců	předchůdce	k1gMnPc2	předchůdce
na	na	k7c4	na
přijímání	přijímání	k1gNnSc4	přijímání
odpadlíků	odpadlík	k1gMnPc2	odpadlík
a	a	k8xC	a
hříšníků	hříšník	k1gMnPc2	hříšník
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
254	[number]	k4	254
<g/>
.	.	kIx.	.
</s>
<s>
Tradice	tradice	k1gFnSc1	tradice
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
umučen	umučit	k5eAaPmNgMnS	umučit
během	během	k7c2	během
pronásledování	pronásledování	k1gNnSc2	pronásledování
křesťanů	křesťan	k1gMnPc2	křesťan
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Valeriana	Valerian	k1gMnSc2	Valerian
<g/>
.	.	kIx.	.
</s>
<s>
Nezdá	zdát	k5eNaPmIp3nS	zdát
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
však	však	k9	však
být	být	k5eAaImF	být
příliš	příliš	k6eAd1	příliš
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
první	první	k4xOgInSc4	první
edikt	edikt	k1gInSc4	edikt
k	k	k7c3	k
pronásledování	pronásledování	k1gNnSc3	pronásledování
křesťanů	křesťan	k1gMnPc2	křesťan
císař	císař	k1gMnSc1	císař
vydal	vydat	k5eAaPmAgMnS	vydat
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
257	[number]	k4	257
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
Lucius	Lucius	k1gInSc1	Lucius
již	již	k6eAd1	již
dávno	dávno	k6eAd1	dávno
mrtev	mrtev	k2eAgInSc1d1	mrtev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Luciova	Luciův	k2eAgFnSc1d1	Luciova
hrobka	hrobka	k1gFnSc1	hrobka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Kalixtových	Kalixtův	k2eAgFnPc6d1	Kalixtův
katakombách	katakomby	k1gFnPc6	katakomby
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
byly	být	k5eAaImAgInP	být
odtud	odtud	k6eAd1	odtud
odvezeny	odvezen	k2eAgInPc1d1	odvezen
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nejrozšířenější	rozšířený	k2eAgFnSc2d3	nejrozšířenější
verze	verze	k1gFnSc2	verze
byly	být	k5eAaImAgInP	být
přesunuty	přesunout	k5eAaPmNgInP	přesunout
do	do	k7c2	do
chrámu	chrám	k1gInSc2	chrám
sv.	sv.	kA	sv.
Cecílie	Cecílie	k1gFnSc1	Cecílie
(	(	kIx(	(
<g/>
Santa	Santa	k1gFnSc1	Santa
Cecilia	Cecilia	k1gFnSc1	Cecilia
in	in	k?	in
Trastevere	Trastever	k1gInSc5	Trastever
<g/>
)	)	kIx)	)
společně	společně	k6eAd1	společně
s	s	k7c7	s
s	s	k7c7	s
ostatky	ostatek	k1gInPc7	ostatek
sv.	sv.	kA	sv.
Cecílie	Cecílie	k1gFnSc2	Cecílie
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
světců	světec	k1gMnPc2	světec
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
prameny	pramen	k1gInPc1	pramen
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nechal	nechat	k5eAaPmAgMnS	nechat
přenést	přenést	k5eAaPmF	přenést
papež	papež	k1gMnSc1	papež
Pavel	Pavel	k1gMnSc1	Pavel
I.	I.	kA	I.
(	(	kIx(	(
<g/>
757	[number]	k4	757
–	–	k?	–
767	[number]	k4	767
<g/>
)	)	kIx)	)
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
San	San	k1gFnSc4	San
Silvestro	Silvestro	k1gNnSc1	Silvestro
in	in	k?	in
Capite	Capit	k1gMnSc5	Capit
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
podle	podle	k7c2	podle
další	další	k2eAgFnSc2d1	další
verze	verze	k1gFnSc2	verze
je	on	k3xPp3gInPc4	on
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
papež	papež	k1gMnSc1	papež
Paschalis	Paschalis	k1gFnSc2	Paschalis
I.	I.	kA	I.
(	(	kIx(	(
<g/>
817	[number]	k4	817
–	–	k?	–
824	[number]	k4	824
<g/>
)	)	kIx)	)
do	do	k7c2	do
baziliky	bazilika	k1gFnSc2	bazilika
sv.	sv.	kA	sv.
Praxedy	Praxeda	k1gMnSc2	Praxeda
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
hlava	hlava	k1gFnSc1	hlava
je	být	k5eAaImIp3nS	být
však	však	k9	však
uchovávána	uchovávat	k5eAaImNgFnS	uchovávat
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
relikviáři	relikviář	k1gInSc6	relikviář
katedrály	katedrála	k1gFnSc2	katedrála
sv.	sv.	kA	sv.
Ansgara	Ansgar	k1gMnSc2	Ansgar
v	v	k7c6	v
Kodani	Kodaň	k1gFnSc6	Kodaň
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
jedny	jeden	k4xCgInPc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
ostatků	ostatek	k1gInPc2	ostatek
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
přečkaly	přečkat	k5eAaPmAgFnP	přečkat
reformaci	reformace	k1gFnSc4	reformace
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1100	[number]	k4	1100
byl	být	k5eAaImAgMnS	být
Lucius	Lucius	k1gMnSc1	Lucius
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
patronem	patron	k1gMnSc7	patron
dánské	dánský	k2eAgFnSc2d1	dánská
provincie	provincie	k1gFnSc2	provincie
Sjæ	Sjæ	k1gFnSc2	Sjæ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Památku	památka	k1gFnSc4	památka
svatého	svatý	k2eAgMnSc2d1	svatý
Lucia	Lucius	k1gMnSc2	Lucius
si	se	k3xPyFc3	se
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
připomíná	připomínat	k5eAaImIp3nS	připomínat
v	v	k7c4	v
den	den	k1gInSc4	den
jeho	jeho	k3xOp3gNnSc2	jeho
úmrtí	úmrtí	k1gNnSc2	úmrtí
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
GELMI	GELMI	kA	GELMI
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Papežové	Papež	k1gMnPc1	Papež
:	:	kIx,	:
Od	od	k7c2	od
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
po	po	k7c4	po
Jana	Jan	k1gMnSc4	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
328	[number]	k4	328
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
457	[number]	k4	457
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MAXWELL-STUART	MAXWELL-STUART	k?	MAXWELL-STUART
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
G.	G.	kA	G.
Papežové	Papež	k1gMnPc1	Papež
<g/>
,	,	kIx,	,
život	život	k1gInSc1	život
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
k	k	k7c3	k
Janu	Jan	k1gMnSc3	Jan
Pavlu	Pavel	k1gMnSc3	Pavel
II	II	kA	II
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
servis	servis	k1gInSc1	servis
<g/>
)	)	kIx)	)
240	[number]	k4	240
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902300	[number]	k4	902300
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RENDINA	RENDINA	kA	RENDINA
<g/>
,	,	kIx,	,
Claudio	Claudio	k6eAd1	Claudio
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
papežů	papež	k1gMnPc2	papež
:	:	kIx,	:
dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
tajemství	tajemství	k1gNnSc1	tajemství
:	:	kIx,	:
životopisy	životopis	k1gInPc4	životopis
265	[number]	k4	265
římských	římský	k2eAgMnPc2d1	římský
papežů	papež	k1gMnPc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
714	[number]	k4	714
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
574	[number]	k4	574
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VONDRUŠKA	Vondruška	k1gMnSc1	Vondruška
<g/>
,	,	kIx,	,
Isidor	Isidor	k1gMnSc1	Isidor
<g/>
.	.	kIx.	.
</s>
<s>
Životopisy	životopis	k1gInPc1	životopis
svatých	svatá	k1gFnPc2	svatá
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
dějin	dějiny	k1gFnPc2	dějiny
církevních	církevní	k2eAgFnPc2d1	církevní
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ladislav	Ladislav	k1gMnSc1	Ladislav
Kuncíř	Kuncíř	k1gMnSc1	Kuncíř
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lucius	Lucius	k1gInSc1	Lucius
I.	I.	kA	I.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
</s>
</p>
