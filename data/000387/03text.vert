<s>
World	World	k1gMnSc1	World
of	of	k?	of
Warcraft	Warcraft	k1gMnSc1	Warcraft
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Burning	Burning	k1gInSc1	Burning
Crusade	Crusad	k1gInSc5	Crusad
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
TBC	TBC	kA	TBC
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgInSc7	první
datadiskem	datadisk	k1gInSc7	datadisk
hry	hra	k1gFnSc2	hra
World	World	k1gInSc1	World
of	of	k?	of
Warcraft	Warcraft	k1gInSc1	Warcraft
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
maximální	maximální	k2eAgInSc1d1	maximální
level	level	k1gInSc1	level
na	na	k7c4	na
70	[number]	k4	70
<g/>
,	,	kIx,	,
přidal	přidat	k5eAaPmAgMnS	přidat
nové	nový	k2eAgFnSc2d1	nová
hratelné	hratelný	k2eAgFnSc2d1	hratelná
rasy	rasa	k1gFnSc2	rasa
-	-	kIx~	-
Krvaví	krvavit	k5eAaImIp3nP	krvavit
Elfové	elf	k1gMnPc1	elf
a	a	k8xC	a
Draeneiové	Draeneius	k1gMnPc1	Draeneius
<g/>
,	,	kIx,	,
oblasti	oblast	k1gFnPc1	oblast
(	(	kIx(	(
<g/>
polorozpadlé	polorozpadlý	k2eAgInPc1d1	polorozpadlý
zbytky	zbytek	k1gInPc1	zbytek
světa	svět	k1gInSc2	svět
Draenor	Draenora	k1gFnPc2	Draenora
-	-	kIx~	-
nyní	nyní	k6eAd1	nyní
nazývané	nazývaný	k2eAgInPc4d1	nazývaný
Outland	Outland	k1gInSc4	Outland
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profese	profese	k1gFnSc1	profese
(	(	kIx(	(
<g/>
Šperkařství	šperkařství	k1gNnSc1	šperkařství
-	-	kIx~	-
Jewelcrafting	Jewelcrafting	k1gInSc1	Jewelcrafting
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
mnohé	mnohý	k2eAgFnPc4d1	mnohá
změny	změna	k1gFnPc4	změna
jak	jak	k6eAd1	jak
herního	herní	k2eAgInSc2d1	herní
světa	svět	k1gInSc2	svět
tak	tak	k6eAd1	tak
samotného	samotný	k2eAgInSc2d1	samotný
systému	systém	k1gInSc2	systém
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
dalším	další	k2eAgInSc7d1	další
datadiskem	datadisk	k1gInSc7	datadisk
World	World	k1gMnSc1	World
of	of	k?	of
Warcraft	Warcraft	k1gMnSc1	Warcraft
<g/>
:	:	kIx,	:
Wrath	Wrath	k1gMnSc1	Wrath
of	of	k?	of
the	the	k?	the
Lich	Lich	k?	Lich
King	King	k1gMnSc1	King
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
hráčů	hráč	k1gMnPc2	hráč
však	však	k9	však
považuje	považovat	k5eAaImIp3nS	považovat
tento	tento	k3xDgInSc4	tento
datadisk	datadisk	k1gInSc4	datadisk
za	za	k7c4	za
nejlepší	dobrý	k2eAgMnPc4d3	nejlepší
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Historie	historie	k1gFnSc1	historie
Outlandu	Outland	k1gInSc2	Outland
Lokace	lokace	k1gFnSc2	lokace
Hellfire	Hellfir	k1gInSc5	Hellfir
Peninsula	Peninsulum	k1gNnPc4	Peninsulum
-	-	kIx~	-
(	(	kIx(	(
<g/>
57	[number]	k4	57
<g/>
-	-	kIx~	-
<g/>
63	[number]	k4	63
<g/>
)	)	kIx)	)
Zanganmarsh	Zanganmarsh	k1gInSc1	Zanganmarsh
-	-	kIx~	-
(	(	kIx(	(
<g/>
60	[number]	k4	60
<g/>
-	-	kIx~	-
<g/>
64	[number]	k4	64
<g/>
)	)	kIx)	)
Blede	Bled	k1gInSc5	Bled
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Edge	Edge	k1gFnSc7	Edge
Mountain	Mountain	k1gMnSc1	Mountain
-	-	kIx~	-
(	(	kIx(	(
<g/>
65	[number]	k4	65
<g/>
-	-	kIx~	-
<g/>
68	[number]	k4	68
<g/>
)	)	kIx)	)
Netherstorm	Netherstorm	k1gInSc1	Netherstorm
-	-	kIx~	-
(	(	kIx(	(
<g/>
67	[number]	k4	67
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
70	[number]	k4	70
<g/>
)	)	kIx)	)
Nagrand	Nagrand	k1gInSc1	Nagrand
-	-	kIx~	-
(	(	kIx(	(
<g/>
64	[number]	k4	64
<g/>
-	-	kIx~	-
<g/>
67	[number]	k4	67
<g/>
)	)	kIx)	)
Terokkar	Terokkar	k1gMnSc1	Terokkar
Forest	Forest	k1gMnSc1	Forest
-	-	kIx~	-
(	(	kIx(	(
<g/>
62	[number]	k4	62
<g/>
-	-	kIx~	-
<g/>
65	[number]	k4	65
<g/>
)	)	kIx)	)
Shadowmoon	Shadowmoon	k1gNnSc4	Shadowmoon
Valey	Valea	k1gFnSc2	Valea
-	-	kIx~	-
(	(	kIx(	(
<g/>
67	[number]	k4	67
<g/>
-	-	kIx~	-
<g/>
70	[number]	k4	70
<g/>
)	)	kIx)	)
Nové	Nové	k2eAgNnSc1d1	Nové
město	město	k1gNnSc1	město
Shattrath	Shattratha	k1gFnPc2	Shattratha
City	City	k1gFnSc2	City
Nové	Nové	k2eAgFnSc2d1	Nové
Rasy	rasa	k1gFnSc2	rasa
Draenei	Draenei	k1gNnSc4	Draenei
-	-	kIx~	-
Výborní	výborný	k2eAgMnPc1d1	výborný
šamani	šaman	k1gMnPc1	šaman
a	a	k8xC	a
paladinové	paladin	k1gMnPc1	paladin
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
na	na	k7c4	na
Azeroth	Azeroth	k1gInSc4	Azeroth
docestovali	docestovat	k5eAaPmAgMnP	docestovat
pomocí	pomocí	k7c2	pomocí
jejich	jejich	k3xOp3gFnSc2	jejich
mezidimenzionální	mezidimenzionální	k2eAgFnSc2d1	mezidimenzionální
lodi	loď	k1gFnSc2	loď
Exodar	Exodara	k1gFnPc2	Exodara
(	(	kIx(	(
<g/>
momentální	momentální	k2eAgNnSc1d1	momentální
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Draenei	Draene	k1gFnSc2	Draene
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zde	zde	k6eAd1	zde
ztroskotala	ztroskotat	k5eAaPmAgFnS	ztroskotat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
přidali	přidat	k5eAaPmAgMnP	přidat
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
Aliance	aliance	k1gFnSc2	aliance
<g/>
.	.	kIx.	.
</s>
<s>
Krvaví	krvavý	k2eAgMnPc1d1	krvavý
Elfové	elf	k1gMnPc1	elf
-	-	kIx~	-
Staronová	staronový	k2eAgFnSc1d1	staronová
rasa	rasa	k1gFnSc1	rasa
za	za	k7c4	za
Hordu	horda	k1gFnSc4	horda
<g/>
,	,	kIx,	,
vedená	vedený	k2eAgFnSc1d1	vedená
princem	princ	k1gMnSc7	princ
Kael	Kael	k1gMnSc1	Kael
<g/>
'	'	kIx"	'
<g/>
thase	thase	k1gFnSc1	thase
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
vynikající	vynikající	k2eAgMnPc1d1	vynikající
mágové	mág	k1gMnPc1	mág
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
Profese	profese	k1gFnSc1	profese
Klenotnictví	klenotnictví	k1gNnSc2	klenotnictví
(	(	kIx(	(
<g/>
Jewelcrafting	Jewelcrafting	k1gInSc1	Jewelcrafting
<g/>
)	)	kIx)	)
-	-	kIx~	-
Primární	primární	k2eAgFnPc4d1	primární
<g/>
,	,	kIx,	,
výrobní	výrobní	k2eAgFnPc4d1	výrobní
profese	profes	k1gFnPc4	profes
zabívající	zabívající	k2eAgFnPc4d1	zabívající
se	s	k7c7	s
zpracováním	zpracování	k1gNnSc7	zpracování
surovin	surovina	k1gFnPc2	surovina
z	z	k7c2	z
těžení	těžení	k1gNnSc2	těžení
rud	ruda	k1gFnPc2	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
hráči	hráč	k1gMnSc3	hráč
vyrábět	vyrábět	k5eAaImF	vyrábět
prsteny	prsten	k1gInPc4	prsten
<g/>
,	,	kIx,	,
náhrdelníky	náhrdelník	k1gInPc1	náhrdelník
<g/>
,	,	kIx,	,
náušnice	náušnice	k1gFnPc1	náušnice
a	a	k8xC	a
šperky	šperk	k1gInPc1	šperk
<g/>
,	,	kIx,	,
vylepšující	vylepšující	k2eAgFnPc1d1	vylepšující
ostatní	ostatní	k2eAgFnPc1d1	ostatní
části	část	k1gFnPc1	část
zbroje	zbroj	k1gFnSc2	zbroj
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
World	Worlda	k1gFnPc2	Worlda
of	of	k?	of
Warcraft	Warcraft	k1gMnSc1	Warcraft
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Burning	Burning	k1gInSc1	Burning
Crusade	Crusad	k1gInSc5	Crusad
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Burning	Burning	k1gInSc1	Burning
Crusade	Crusad	k1gInSc5	Crusad
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
Burning	Burning	k1gInSc4	Burning
Crusade	Crusad	k1gInSc5	Crusad
</s>
