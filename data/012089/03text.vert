<p>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Ryšavý	Ryšavý	k1gMnSc1	Ryšavý
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
965	[number]	k4	965
–	–	k?	–
1037	[number]	k4	1037
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
v	v	k7c6	v
letech	let	k1gInPc6	let
999	[number]	k4	999
<g/>
–	–	k?	–
<g/>
1002	[number]	k4	1002
a	a	k8xC	a
nakrátko	nakrátko	k6eAd1	nakrátko
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1003	[number]	k4	1003
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
už	už	k6eAd1	už
souvěkým	souvěký	k2eAgMnSc7d1	souvěký
kronikářem	kronikář	k1gMnSc7	kronikář
Dětmarem	Dětmar	k1gMnSc7	Dětmar
z	z	k7c2	z
Merseburku	Merseburk	k1gInSc2	Merseburk
Ryšavý	ryšavý	k2eAgMnSc1d1	ryšavý
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
syn	syn	k1gMnSc1	syn
Boleslava	Boleslav	k1gMnSc2	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Pobožného	pobožný	k2eAgMnSc4d1	pobožný
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
knížat	kníže	k1gMnPc2wR	kníže
Jaromíra	Jaromír	k1gMnSc2	Jaromír
a	a	k8xC	a
Oldřicha	Oldřich	k1gMnSc2	Oldřich
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Kosmas	Kosmas	k1gMnSc1	Kosmas
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
Boleslavovou	Boleslavův	k2eAgFnSc7d1	Boleslavova
matkou	matka	k1gFnSc7	matka
byla	být	k5eAaImAgFnS	být
údajně	údajně	k6eAd1	údajně
jako	jako	k8xS	jako
u	u	k7c2	u
jeho	jeho	k3xOp3gMnPc2	jeho
mladších	mladý	k2eAgMnPc2d2	mladší
bratrů	bratr	k1gMnPc2	bratr
kněžna	kněžna	k1gFnSc1	kněžna
Emma	Emma	k1gFnSc1	Emma
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
mají	mít	k5eAaImIp3nP	mít
historici	historik	k1gMnPc1	historik
většinou	většinou	k6eAd1	většinou
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
manželka	manželka	k1gFnSc1	manželka
Boleslava	Boleslav	k1gMnSc4	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Emma	Emma	k1gFnSc1	Emma
byla	být	k5eAaImAgFnS	být
manželkou	manželka	k1gFnSc7	manželka
druhou	druhý	k4xOgFnSc4	druhý
nebo	nebo	k8xC	nebo
spíše	spíše	k9	spíše
až	až	k9	až
třetí	třetí	k4xOgMnSc1	třetí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Boleslavova	Boleslavův	k2eAgFnSc1d1	Boleslavova
násilnická	násilnický	k2eAgFnSc1d1	násilnická
vláda	vláda	k1gFnSc1	vláda
zahájila	zahájit	k5eAaPmAgFnS	zahájit
první	první	k4xOgFnSc4	první
vážnou	vážná	k1gFnSc4	vážná
krizi	krize	k1gFnSc4	krize
přemyslovského	přemyslovský	k2eAgInSc2d1	přemyslovský
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
jeho	jeho	k3xOp3gNnSc4	jeho
nástupnické	nástupnický	k2eAgNnSc4d1	nástupnické
právo	právo	k1gNnSc4	právo
coby	coby	k?	coby
nejstaršího	starý	k2eAgMnSc4d3	nejstarší
syna	syn	k1gMnSc4	syn
nejspíš	nejspíš	k9	nejspíš
nikdo	nikdo	k3yNnSc1	nikdo
nezpochybňoval	zpochybňovat	k5eNaImAgMnS	zpochybňovat
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
pokus	pokus	k1gInSc4	pokus
zbavit	zbavit	k5eAaPmF	zbavit
se	se	k3xPyFc4	se
svých	svůj	k3xOyFgNnPc2	svůj
bratrů	bratr	k1gMnPc2	bratr
coby	coby	k?	coby
konkurentů	konkurent	k1gMnPc2	konkurent
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podpořenou	podpořený	k2eAgFnSc4d1	podpořená
vznikem	vznik	k1gInSc7	vznik
nových	nový	k2eAgInPc2d1	nový
státních	státní	k2eAgInPc2d1	státní
útvarů	útvar	k1gInPc2	útvar
<g/>
,	,	kIx,	,
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
999	[number]	k4	999
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Boleslav	Boleslav	k1gMnSc1	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
ujal	ujmout	k5eAaPmAgInS	ujmout
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
patřila	patřit	k5eAaImAgFnS	patřit
z	z	k7c2	z
říše	říš	k1gFnSc2	říš
jeho	on	k3xPp3gMnSc2	on
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
děda	děd	k1gMnSc2	děd
k	k	k7c3	k
českému	český	k2eAgInSc3d1	český
státu	stát	k1gInSc3	stát
už	už	k6eAd1	už
jen	jen	k9	jen
Čechy	Čech	k1gMnPc4	Čech
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgNnSc4d1	ostatní
území	území	k1gNnSc4	území
patřila	patřit	k5eAaImAgFnS	patřit
už	už	k9	už
k	k	k7c3	k
Polsku	Polsko	k1gNnSc3	Polsko
a	a	k8xC	a
Uhersku	Uhersko	k1gNnSc3	Uhersko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
krizi	krize	k1gFnSc3	krize
kromě	kromě	k7c2	kromě
územních	územní	k2eAgFnPc2d1	územní
ztrát	ztráta	k1gFnPc2	ztráta
přispělo	přispět	k5eAaPmAgNnS	přispět
i	i	k9	i
spojenectví	spojenectví	k1gNnSc1	spojenectví
císaře	císař	k1gMnSc2	císař
Oty	Ota	k1gMnSc2	Ota
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
polského	polský	k2eAgMnSc2d1	polský
knížete	kníže	k1gMnSc2	kníže
<g/>
,	,	kIx,	,
bratrance	bratranec	k1gMnSc2	bratranec
Boleslava	Boleslav	k1gMnSc2	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
bratří	bratr	k1gMnPc2	bratr
<g/>
,	,	kIx,	,
Boleslava	Boleslav	k1gMnSc2	Boleslav
Chrabrého	chrabrý	k2eAgMnSc2d1	chrabrý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Boje	boj	k1gInPc1	boj
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
==	==	k?	==
</s>
</p>
<p>
<s>
1001	[number]	k4	1001
–	–	k?	–
útěk	útěk	k1gInSc4	útěk
Jaromíra	Jaromír	k1gMnSc4	Jaromír
(	(	kIx(	(
<g/>
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
knížete	kníže	k1gMnSc2	kníže
vykastrován	vykastrovat	k5eAaPmNgInS	vykastrovat
<g/>
)	)	kIx)	)
a	a	k8xC	a
Oldřicha	Oldřich	k1gMnSc2	Oldřich
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
na	na	k7c4	na
Boleslavův	Boleslavův	k2eAgInSc4d1	Boleslavův
příkaz	příkaz	k1gInSc4	příkaz
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
<g/>
,	,	kIx,	,
do	do	k7c2	do
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
<g/>
;	;	kIx,	;
<g/>
1002	[number]	k4	1002
–	–	k?	–
povstání	povstání	k1gNnSc4	povstání
proti	proti	k7c3	proti
Boleslavovi	Boleslav	k1gMnSc3	Boleslav
(	(	kIx(	(
<g/>
vedeno	vést	k5eAaImNgNnS	vést
Vršovci	Vršovec	k1gMnSc6	Vršovec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Boleslav	Boleslav	k1gMnSc1	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
k	k	k7c3	k
Jindřichu	Jindřich	k1gMnSc3	Jindřich
Nordgavskému	Nordgavský	k2eAgInSc3d1	Nordgavský
(	(	kIx(	(
<g/>
otci	otec	k1gMnSc3	otec
Jitky	Jitka	k1gFnSc2	Jitka
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgFnSc2d2	pozdější
manželky	manželka	k1gFnSc2	manželka
knížete	kníže	k1gMnSc2	kníže
Břetislava	Břetislav	k1gMnSc2	Břetislav
I.	I.	kA	I.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
jím	jíst	k5eAaImIp1nS	jíst
byl	být	k5eAaImAgMnS	být
krátce	krátce	k6eAd1	krátce
zajat	zajat	k2eAgMnSc1d1	zajat
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
k	k	k7c3	k
Boleslavu	Boleslav	k1gMnSc3	Boleslav
Chrabrému	chrabrý	k2eAgInSc3d1	chrabrý
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
;	;	kIx,	;
<g/>
1003	[number]	k4	1003
(	(	kIx(	(
<g/>
leden	leden	k1gInSc4	leden
<g/>
)	)	kIx)	)
–	–	k?	–
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Vladivoje	Vladivoj	k1gInSc2	Vladivoj
byl	být	k5eAaImAgInS	být
Boleslav	Boleslav	k1gMnSc1	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Boleslavem	Boleslav	k1gMnSc7	Boleslav
Chrabrým	chrabrý	k2eAgInSc7d1	chrabrý
znovu	znovu	k6eAd1	znovu
dosazen	dosazen	k2eAgInSc1d1	dosazen
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
knížecí	knížecí	k2eAgInSc4d1	knížecí
stolec	stolec	k1gInSc4	stolec
<g/>
.	.	kIx.	.
</s>
<s>
Slíbil	slíbit	k5eAaPmAgMnS	slíbit
Boleslavu	Boleslav	k1gMnSc3	Boleslav
Chrabrému	chrabrý	k2eAgMnSc3d1	chrabrý
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zúčastní	zúčastnit	k5eAaPmIp3nP	zúčastnit
povstání	povstání	k1gNnSc2	povstání
proti	proti	k7c3	proti
římskoněmeckému	římskoněmecký	k2eAgMnSc3d1	římskoněmecký
králi	král	k1gMnSc3	král
Jindřichovi	Jindřich	k1gMnSc3	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
;	;	kIx,	;
<g/>
1003	[number]	k4	1003
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
únor	únor	k1gInSc4	únor
<g/>
)	)	kIx)	)
–	–	k?	–
kníže	kníže	k1gMnSc1	kníže
slíbil	slíbit	k5eAaPmAgMnS	slíbit
odpouštění	odpouštění	k1gNnSc4	odpouštění
svým	svůj	k3xOyFgMnPc3	svůj
dřívějším	dřívější	k2eAgMnPc3d1	dřívější
odpůrcům	odpůrce	k1gMnPc3	odpůrce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nařídil	nařídit	k5eAaPmAgMnS	nařídit
vyvraždění	vyvraždění	k1gNnPc4	vyvraždění
Vršovců	Vršovec	k1gMnPc2	Vršovec
a	a	k8xC	a
svého	svůj	k3xOyFgMnSc2	svůj
zetě	zeť	k1gMnSc2	zeť
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
prý	prý	k9	prý
zabil	zabít	k5eAaPmAgMnS	zabít
vlastníma	vlastní	k2eAgFnPc7d1	vlastní
rukama	ruka	k1gFnPc7	ruka
(	(	kIx(	(
<g/>
tuto	tento	k3xDgFnSc4	tento
příhodu	příhoda	k1gFnSc4	příhoda
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
saský	saský	k2eAgMnSc1d1	saský
letopisec	letopisec	k1gMnSc1	letopisec
Dětmar	Dětmar	k1gMnSc1	Dětmar
z	z	k7c2	z
Merseburku	Merseburk	k1gInSc2	Merseburk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
čin	čin	k1gInSc1	čin
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
povstání	povstání	k1gNnSc4	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
Chrabrý	chrabrý	k2eAgMnSc1d1	chrabrý
povolal	povolat	k5eAaPmAgMnS	povolat
svého	svůj	k3xOyFgMnSc4	svůj
chráněnce	chráněnec	k1gMnSc4	chráněnec
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
oslepil	oslepit	k5eAaPmAgMnS	oslepit
jej	on	k3xPp3gMnSc4	on
a	a	k8xC	a
uvěznil	uvěznit	k5eAaPmAgMnS	uvěznit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1003	[number]	k4	1003
vpadl	vpadnout	k5eAaPmAgMnS	vpadnout
Boleslav	Boleslav	k1gMnSc1	Boleslav
Chrabrý	chrabrý	k2eAgMnSc1d1	chrabrý
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
ovládal	ovládat	k5eAaImAgInS	ovládat
do	do	k7c2	do
podzimu	podzim	k1gInSc2	podzim
1004	[number]	k4	1004
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
vypuzen	vypudit	k5eAaPmNgMnS	vypudit
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
Jindřichem	Jindřich	k1gMnSc7	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
císařem	císař	k1gMnSc7	císař
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgFnPc4d1	zbylá
tři	tři	k4xCgFnPc4	tři
desítky	desítka	k1gFnPc4	desítka
let	let	k1gInSc4	let
života	život	k1gInSc2	život
prožil	prožít	k5eAaPmAgMnS	prožít
svržený	svržený	k2eAgMnSc1d1	svržený
Boleslav	Boleslav	k1gMnSc1	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
v	v	k7c6	v
polském	polský	k2eAgNnSc6d1	polské
vězení	vězení	k1gNnSc6	vězení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
roku	rok	k1gInSc2	rok
1037	[number]	k4	1037
"	"	kIx"	"
<g/>
od	od	k7c2	od
nikoho	nikdo	k3yNnSc2	nikdo
neželen	želen	k2eNgMnSc1d1	želen
i	i	k9	i
umřel	umřít	k5eAaPmAgMnS	umřít
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Boleslav	Boleslav	k1gMnSc1	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
neměl	mít	k5eNaImAgMnS	mít
mužského	mužský	k2eAgMnSc4d1	mužský
dědice	dědic	k1gMnSc4	dědic
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
ani	ani	k8xC	ani
jméno	jméno	k1gNnSc1	jméno
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
měl	mít	k5eAaImAgInS	mít
jen	jen	k9	jen
dceru	dcera	k1gFnSc4	dcera
<g/>
,	,	kIx,	,
manželku	manželka	k1gFnSc4	manželka
výše	výše	k1gFnSc2	výše
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
Vršovce	Vršovec	k1gInSc2	Vršovec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Boleslav	Boleslav	k1gMnSc1	Boleslav
měl	mít	k5eAaImAgMnS	mít
vnuka	vnuk	k1gMnSc4	vnuk
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
kníže	kníže	k1gMnSc1	kníže
Oldřich	Oldřich	k1gMnSc1	Oldřich
nařídil	nařídit	k5eAaPmAgMnS	nařídit
druhé	druhý	k4xOgNnSc4	druhý
vraždění	vraždění	k1gNnSc4	vraždění
Vršovců	Vršovec	k1gInPc2	Vršovec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
Dětmat	Dětmat	k1gInSc1	Dětmat
z	z	k7c2	z
Merseburku	Merseburk	k1gInSc2	Merseburk
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Vršovci	Vršovec	k1gMnPc1	Vršovec
podporovali	podporovat	k5eAaImAgMnP	podporovat
prostředního	prostřední	k2eAgInSc2d1	prostřední
z	z	k7c2	z
bratrů	bratr	k1gMnPc2	bratr
Jaromíra	Jaromír	k1gMnSc2	Jaromír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Genealogie	genealogie	k1gFnSc2	genealogie
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
BLÁHOVÁ	Bláhová	k1gFnSc1	Bláhová
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
;	;	kIx,	;
FROLÍK	FROLÍK	kA	FROLÍK
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
PROFANTOVÁ	PROFANTOVÁ	kA	PROFANTOVÁ
<g/>
,	,	kIx,	,
Naďa	Naďa	k1gFnSc1	Naďa
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
I.	I.	kA	I.
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1197	[number]	k4	1197
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
800	[number]	k4	800
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
265	[number]	k4	265
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CHARVÁT	Charvát	k1gMnSc1	Charvát
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Zrod	zrod	k1gInSc1	zrod
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
568	[number]	k4	568
<g/>
-	-	kIx~	-
<g/>
1055	[number]	k4	1055
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
263	[number]	k4	263
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
845	[number]	k4	845
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kdo	kdo	k3yInSc1	kdo
byl	být	k5eAaImAgMnS	být
kdo	kdo	k3yQnSc1	kdo
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
dějinách	dějiny	k1gFnPc6	dějiny
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
/	/	kIx~	/
(	(	kIx(	(
<g/>
Pavel	Pavel	k1gMnSc1	Pavel
Augusta	Augusta	k1gMnSc1	Augusta
...	...	k?	...
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
571	[number]	k4	571
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85983	[number]	k4	85983
<g/>
-	-	kIx~	-
<g/>
94	[number]	k4	94
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
S.	S.	kA	S.
42	[number]	k4	42
<g/>
–	–	k?	–
<g/>
43	[number]	k4	43
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LUTOVSKÝ	LUTOVSKÝ	kA	LUTOVSKÝ
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
prvních	první	k4xOgInPc2	první
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Léta	léto	k1gNnPc1	léto
krize	krize	k1gFnSc2	krize
a	a	k8xC	a
obnovy	obnova	k1gFnSc2	obnova
972	[number]	k4	972
<g/>
-	-	kIx~	-
<g/>
1012	[number]	k4	1012
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Boleslava	Boleslav	k1gMnSc2	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
po	po	k7c4	po
Jaromíra	Jaromír	k1gMnSc4	Jaromír
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
271	[number]	k4	271
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
231	[number]	k4	231
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LUTOVSKÝ	LUTOVSKÝ	kA	LUTOVSKÝ
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
prvních	první	k4xOgInPc2	první
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Správa	správa	k1gFnSc1	správa
a	a	k8xC	a
obrana	obrana	k1gFnSc1	obrana
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
1012	[number]	k4	1012
<g/>
-	-	kIx~	-
<g/>
1055	[number]	k4	1055
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Oldřicha	Oldřich	k1gMnSc2	Oldřich
po	po	k7c4	po
Břetislava	Břetislav	k1gMnSc4	Břetislav
I.	I.	kA	I.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
282	[number]	k4	282
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
365	[number]	k4	365
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MATLA-KOZŁOWSKA	MATLA-KOZŁOWSKA	k?	MATLA-KOZŁOWSKA
<g/>
,	,	kIx,	,
Marzena	Marzen	k2eAgFnSc1d1	Marzena
<g/>
.	.	kIx.	.
</s>
<s>
Pierwsi	Pierwse	k1gFnSc4	Pierwse
Przemyślidzi	Przemyślidze	k1gFnSc4	Przemyślidze
i	i	k9	i
ich	ich	k?	ich
państwo	państwo	k6eAd1	państwo
(	(	kIx(	(
<g/>
od	od	k7c2	od
X	X	kA	X
do	do	k7c2	do
połowy	połowa	k1gFnSc2	połowa
XI	XI	kA	XI
wieku	wieku	k6eAd1	wieku
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ekspansja	Ekspansj	k2eAgFnSc1d1	Ekspansj
terytorialna	terytorialna	k1gFnSc1	terytorialna
i	i	k8xC	i
jej	on	k3xPp3gMnSc4	on
polityczne	politycznout	k5eAaPmIp3nS	politycznout
uwarunkowania	uwarunkowanium	k1gNnPc4	uwarunkowanium
<g/>
.	.	kIx.	.
</s>
<s>
Poznań	Poznań	k?	Poznań
<g/>
:	:	kIx,	:
Wydawnictwo	Wydawnictwo	k1gMnSc1	Wydawnictwo
Poznańskie	Poznańskie	k1gFnSc2	Poznańskie
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
576	[number]	k4	576
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
7177	[number]	k4	7177
<g/>
-	-	kIx~	-
<g/>
547	[number]	k4	547
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
NOVOTNÝ	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dějiny	dějiny	k1gFnPc1	dějiny
I.	I.	kA	I.
<g/>
/	/	kIx~	/
<g/>
I.	I.	kA	I.
Od	od	k7c2	od
nejstarších	starý	k2eAgFnPc2d3	nejstarší
dob	doba	k1gFnPc2	doba
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
knížete	kníže	k1gMnSc2	kníže
Oldřicha	Oldřich	k1gMnSc2	Oldřich
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
782	[number]	k4	782
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Osobnosti	osobnost	k1gFnPc1	osobnost
-	-	kIx~	-
Česko	Česko	k1gNnSc1	Česko
:	:	kIx,	:
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
823	[number]	k4	823
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7360	[number]	k4	7360
<g/>
-	-	kIx~	-
<g/>
796	[number]	k4	796
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
61	[number]	k4	61
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SOMMER	Sommer	k1gMnSc1	Sommer
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
TŘEŠTÍK	TŘEŠTÍK	kA	TŘEŠTÍK
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
;	;	kIx,	;
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Budování	budování	k1gNnSc4	budování
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
779	[number]	k4	779
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
352	[number]	k4	352
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TŘEŠTÍK	TŘEŠTÍK	kA	TŘEŠTÍK
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
;	;	kIx,	;
ČECHURA	Čechura	k1gMnSc1	Čechura
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
;	;	kIx,	;
PECHAR	PECHAR	kA	PECHAR
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Králové	Král	k1gMnPc1	Král
a	a	k8xC	a
knížata	kníže	k1gMnPc1wR	kníže
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Rybka	Rybka	k1gMnSc1	Rybka
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
413	[number]	k4	413
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86182	[number]	k4	86182
<g/>
-	-	kIx~	-
<g/>
55	[number]	k4	55
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
VOŠAHLÍKOVÁ	Vošahlíková	k1gFnSc1	Vošahlíková
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
:	:	kIx,	:
6	[number]	k4	6
<g/>
.	.	kIx.	.
sešit	sešit	k1gInSc1	sešit
:	:	kIx,	:
Boh	Boh	k1gFnSc1	Boh
<g/>
–	–	k?	–
<g/>
Bož	Boža	k1gFnPc2	Boža
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
109	[number]	k4	109
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
239	[number]	k4	239
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
37	[number]	k4	37
<g/>
–	–	k?	–
<g/>
38	[number]	k4	38
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
žili	žít	k5eAaImAgMnP	žít
<g/>
,	,	kIx,	,
vládli	vládnout	k5eAaImAgMnP	vládnout
<g/>
,	,	kIx,	,
umírali	umírat	k5eAaImAgMnP	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
497	[number]	k4	497
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
759	[number]	k4	759
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Boleslav	Boleslav	k1gMnSc1	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
na	na	k7c6	na
e-stredovek	etredovka	k1gFnPc2	e-stredovka
</s>
</p>
