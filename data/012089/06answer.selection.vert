<s>
Boleslavova	Boleslavův	k2eAgFnSc1d1	Boleslavova
násilnická	násilnický	k2eAgFnSc1d1	násilnická
vláda	vláda	k1gFnSc1	vláda
zahájila	zahájit	k5eAaPmAgFnS	zahájit
první	první	k4xOgFnSc4	první
vážnou	vážná	k1gFnSc4	vážná
krizi	krize	k1gFnSc4	krize
přemyslovského	přemyslovský	k2eAgInSc2d1	přemyslovský
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
jeho	jeho	k3xOp3gNnSc4	jeho
nástupnické	nástupnický	k2eAgNnSc4d1	nástupnické
právo	právo	k1gNnSc4	právo
coby	coby	k?	coby
nejstaršího	starý	k2eAgMnSc4d3	nejstarší
syna	syn	k1gMnSc4	syn
nejspíš	nejspíš	k9	nejspíš
nikdo	nikdo	k3yNnSc1	nikdo
nezpochybňoval	zpochybňovat	k5eNaImAgMnS	zpochybňovat
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
pokus	pokus	k1gInSc4	pokus
zbavit	zbavit	k5eAaPmF	zbavit
se	se	k3xPyFc4	se
svých	svůj	k3xOyFgNnPc2	svůj
bratrů	bratr	k1gMnPc2	bratr
coby	coby	k?	coby
konkurentů	konkurent	k1gMnPc2	konkurent
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podpořenou	podpořený	k2eAgFnSc4d1	podpořená
vznikem	vznik	k1gInSc7	vznik
nových	nový	k2eAgInPc2d1	nový
státních	státní	k2eAgInPc2d1	státní
útvarů	útvar	k1gInPc2	útvar
<g/>
,	,	kIx,	,
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
</s>
