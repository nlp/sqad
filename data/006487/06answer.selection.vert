<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
negativní	negativní	k2eAgFnSc4d1	negativní
metodu	metoda	k1gFnSc4	metoda
vylučování	vylučování	k1gNnSc2	vylučování
hypotéz	hypotéza	k1gFnPc2	hypotéza
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
lepší	dobrý	k2eAgFnPc1d2	lepší
hypotézy	hypotéza	k1gFnPc1	hypotéza
jsou	být	k5eAaImIp3nP	být
zastávány	zastáván	k2eAgFnPc1d1	zastávána
a	a	k8xC	a
horší	zlý	k2eAgFnPc1d2	horší
vylučovány	vylučován	k2eAgFnPc1d1	vylučována
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
vedou	vést	k5eAaImIp3nP	vést
ke	k	k7c3	k
sporu	spor	k1gInSc3	spor
<g/>
.	.	kIx.	.
</s>
