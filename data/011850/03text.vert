<p>
<s>
Trias	trias	k1gInSc1	trias
je	být	k5eAaImIp3nS	být
geologická	geologický	k2eAgFnSc1d1	geologická
perioda	perioda	k1gFnSc1	perioda
druhohorního	druhohorní	k2eAgNnSc2d1	druhohorní
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Trval	trvat	k5eAaImAgInS	trvat
od	od	k7c2	od
zhruba	zhruba	k6eAd1	zhruba
252	[number]	k4	252
do	do	k7c2	do
201,3	[number]	k4	201,3
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Trias	trias	k1gInSc1	trias
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc4	první
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
period	perioda	k1gFnPc2	perioda
druhohor	druhohory	k1gFnPc2	druhohory
a	a	k8xC	a
následuje	následovat	k5eAaImIp3nS	následovat
po	po	k7c6	po
poslední	poslední	k2eAgFnSc6d1	poslední
periodě	perioda	k1gFnSc6	perioda
prvohorního	prvohorní	k2eAgNnSc2d1	prvohorní
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
permu	perm	k1gInSc2	perm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
Lehmannem	Lehmanno	k1gNnSc7	Lehmanno
a	a	k8xC	a
Füchselem	Füchsel	k1gInSc7	Füchsel
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
rozlišována	rozlišován	k2eAgNnPc4d1	rozlišováno
dvě	dva	k4xCgNnPc4	dva
souvrství	souvrství	k1gNnPc4	souvrství
<g/>
,	,	kIx,	,
Buntsandstein	Buntsandstein	k1gInSc1	Buntsandstein
–	–	k?	–
pestrý	pestrý	k2eAgInSc1d1	pestrý
pískovec	pískovec	k1gInSc1	pískovec
a	a	k8xC	a
Muschelkalk	Muschelkalk	k1gInSc1	Muschelkalk
–	–	k?	–
lasturnatý	lasturnatý	k2eAgInSc1d1	lasturnatý
vápenec	vápenec	k1gInSc1	vápenec
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1822	[number]	k4	1822
oddělil	oddělit	k5eAaPmAgInS	oddělit
Buch	buch	k1gInSc1	buch
svrchní	svrchní	k2eAgFnSc4d1	svrchní
část	část	k1gFnSc4	část
lasturnatého	lasturnatý	k2eAgInSc2d1	lasturnatý
vápence	vápenec	k1gInSc2	vápenec
a	a	k8xC	a
nazval	nazvat	k5eAaPmAgMnS	nazvat
ho	on	k3xPp3gInSc4	on
Keuper	keuper	k1gInSc4	keuper
–	–	k?	–
pestrý	pestrý	k2eAgInSc1d1	pestrý
slín	slín	k1gInSc1	slín
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
roku	rok	k1gInSc2	rok
1834	[number]	k4	1834
Friedrich	Friedrich	k1gMnSc1	Friedrich
von	von	k1gInSc1	von
Alberti	Albert	k1gMnPc1	Albert
zahrnul	zahrnout	k5eAaPmAgInS	zahrnout
všechna	všechen	k3xTgNnPc4	všechen
tři	tři	k4xCgNnPc4	tři
souvrství	souvrství	k1gNnPc4	souvrství
pod	pod	k7c4	pod
jeden	jeden	k4xCgInSc4	jeden
útvar	útvar	k1gInSc4	útvar
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
dal	dát	k5eAaPmAgInS	dát
název	název	k1gInSc1	název
trias	trias	k1gInSc1	trias
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
trojice	trojice	k1gFnSc2	trojice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
klasický	klasický	k2eAgInSc1d1	klasický
nezvrásněný	zvrásněný	k2eNgInSc1d1	zvrásněný
kontinentální	kontinentální	k2eAgInSc1d1	kontinentální
vývoj	vývoj	k1gInSc1	vývoj
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
převládají	převládat	k5eAaImIp3nP	převládat
klastika	klastik	k1gMnSc4	klastik
nad	nad	k7c7	nad
karbonáty	karbonát	k1gInPc7	karbonát
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
germánský	germánský	k2eAgInSc1d1	germánský
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
alpínský	alpínský	k2eAgInSc4d1	alpínský
vývoj	vývoj	k1gInSc4	vývoj
s	s	k7c7	s
převahou	převaha	k1gFnSc7	převaha
karbonátů	karbonát	k1gInPc2	karbonát
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
v	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
etapách	etapa	k1gFnPc6	etapa
silnému	silný	k2eAgInSc3d1	silný
vrásnění	vrásnění	k1gNnSc2	vrásnění
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
těchto	tento	k3xDgInPc2	tento
vývojů	vývoj	k1gInPc2	vývoj
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
představovala	představovat	k5eAaImAgFnS	představovat
tzv.	tzv.	kA	tzv.
vindelická	vindelický	k2eAgFnSc1d1	vindelický
pevnina	pevnina	k1gFnSc1	pevnina
(	(	kIx(	(
<g/>
též	též	k9	též
vindelický	vindelický	k2eAgInSc1d1	vindelický
val	val	k1gInSc1	val
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
probíhající	probíhající	k2eAgInSc4d1	probíhající
od	od	k7c2	od
Českého	český	k2eAgInSc2d1	český
masívu	masív	k1gInSc2	masív
přes	přes	k7c4	přes
jižní	jižní	k2eAgNnSc4d1	jižní
Bavorsko	Bavorsko	k1gNnSc4	Bavorsko
k	k	k7c3	k
Ženevskému	ženevský	k2eAgNnSc3d1	Ženevské
jezeru	jezero	k1gNnSc3	jezero
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
na	na	k7c4	na
Korsiku	Korsika	k1gFnSc4	Korsika
a	a	k8xC	a
Sardinii	Sardinie	k1gFnSc4	Sardinie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členění	členění	k1gNnSc1	členění
triasu	trias	k1gInSc2	trias
==	==	k?	==
</s>
</p>
<p>
<s>
Základní	základní	k2eAgNnSc1d1	základní
členění	členění	k1gNnSc1	členění
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
spodní	spodní	k2eAgInSc4d1	spodní
<g/>
,	,	kIx,	,
střední	střední	k2eAgInSc4d1	střední
a	a	k8xC	a
svrchní	svrchní	k2eAgInSc4d1	svrchní
trias	trias	k1gInSc4	trias
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgNnSc1d2	podrobnější
zonální	zonální	k2eAgNnSc1d1	zonální
členění	členění	k1gNnSc1	členění
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
na	na	k7c6	na
základě	základ	k1gInSc6	základ
amonitové	amonitový	k2eAgFnSc2d1	amonitový
<g/>
,	,	kIx,	,
konodontové	konodontový	k2eAgFnSc2d1	konodontový
a	a	k8xC	a
foraminiferové	foraminiferový	k2eAgFnSc2d1	foraminiferový
fauny	fauna	k1gFnSc2	fauna
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
novějších	nový	k2eAgFnPc2d2	novější
studií	studie	k1gFnPc2	studie
se	s	k7c7	s
zpřesněným	zpřesněný	k2eAgNnSc7d1	zpřesněné
datováním	datování	k1gNnSc7	datování
hornin	hornina	k1gFnPc2	hornina
trvalo	trvat	k5eAaImAgNnS	trvat
toto	tento	k3xDgNnSc1	tento
druhohorní	druhohorní	k2eAgNnSc1d1	druhohorní
období	období	k1gNnSc1	období
asi	asi	k9	asi
50,5	[number]	k4	50,5
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Paleogeografie	paleogeografie	k1gFnSc2	paleogeografie
==	==	k?	==
</s>
</p>
<p>
<s>
Trias	trias	k1gInSc1	trias
se	se	k3xPyFc4	se
vyznačoval	vyznačovat	k5eAaImAgInS	vyznačovat
celosvětově	celosvětově	k6eAd1	celosvětově
teplým	teplý	k2eAgNnSc7d1	teplé
klimatem	klima	k1gNnSc7	klima
<g/>
.	.	kIx.	.
</s>
<s>
Subtropické	subtropický	k2eAgInPc1d1	subtropický
pásy	pás	k1gInPc1	pás
s	s	k7c7	s
aridním	aridní	k2eAgNnSc7d1	aridní
podnebím	podnebí	k1gNnSc7	podnebí
doprovázené	doprovázený	k2eAgInPc1d1	doprovázený
červenými	červený	k2eAgInPc7d1	červený
sedimenty	sediment	k1gInPc7	sediment
a	a	k8xC	a
ložisky	ložisko	k1gNnPc7	ložisko
evaporitů	evaporit	k1gInPc2	evaporit
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Arizony	Arizona	k1gFnSc2	Arizona
či	či	k8xC	či
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
klima	klima	k1gNnSc1	klima
nebylo	být	k5eNaImAgNnS	být
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
(	(	kIx(	(
<g/>
Lutz	Lutz	k1gInSc1	Lutz
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
vhodné	vhodný	k2eAgNnSc1d1	vhodné
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
ložisek	ložisko	k1gNnPc2	ložisko
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
dílčímu	dílčí	k2eAgNnSc3d1	dílčí
ochlazení	ochlazení	k1gNnSc3	ochlazení
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vlhčím	vlhký	k2eAgNnSc7d2	vlhčí
podnebím	podnebí	k1gNnSc7	podnebí
jsou	být	k5eAaImIp3nP	být
opět	opět	k6eAd1	opět
vytvořeny	vytvořit	k5eAaPmNgFnP	vytvořit
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
ložisek	ložisko	k1gNnPc2	ložisko
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spodním	spodní	k2eAgInSc6d1	spodní
triasu	trias	k1gInSc6	trias
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozsáhlé	rozsáhlý	k2eAgFnSc3d1	rozsáhlá
mořské	mořský	k2eAgFnSc3d1	mořská
transgresi	transgrese	k1gFnSc3	transgrese
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
opakovala	opakovat	k5eAaImAgFnS	opakovat
i	i	k9	i
ve	v	k7c6	v
svrchním	svrchní	k2eAgInSc6d1	svrchní
triasu	trias	k1gInSc6	trias
<g/>
.	.	kIx.	.
</s>
<s>
Podstatnými	podstatný	k2eAgFnPc7d1	podstatná
událostmi	událost	k1gFnPc7	událost
jsou	být	k5eAaImIp3nP	být
počínající	počínající	k2eAgInSc1d1	počínající
rozpad	rozpad	k1gInSc1	rozpad
Gondwany	Gondwana	k1gFnSc2	Gondwana
a	a	k8xC	a
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
otevírání	otevírání	k1gNnSc4	otevírání
Atlantického	atlantický	k2eAgInSc2d1	atlantický
oceánu	oceán	k1gInSc2	oceán
podél	podél	k7c2	podél
středooceánského	středooceánský	k2eAgInSc2d1	středooceánský
hřbetu	hřbet	k1gInSc2	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgInSc1d1	severní
pól	pól	k1gInSc1	pól
se	se	k3xPyFc4	se
v	v	k7c6	v
triasu	trias	k1gInSc6	trias
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Kamčatky	Kamčatka	k1gFnSc2	Kamčatka
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
mohutným	mohutný	k2eAgInPc3d1	mohutný
výlevům	výlev	k1gInPc3	výlev
láv	láva	k1gFnPc2	láva
<g/>
,	,	kIx,	,
označovaných	označovaný	k2eAgFnPc2d1	označovaná
jako	jako	k8xS	jako
trapy	trap	k1gInPc4	trap
na	na	k7c6	na
sibiřské	sibiřský	k2eAgFnSc6d1	sibiřská
platformě	platforma	k1gFnSc6	platforma
(	(	kIx(	(
<g/>
s	s	k7c7	s
diamantonosnými	diamantonosný	k2eAgInPc7d1	diamantonosný
kimberlity	kimberlit	k1gInPc7	kimberlit
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Vulkanická	vulkanický	k2eAgFnSc1d1	vulkanická
činnost	činnost	k1gFnSc1	činnost
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
i	i	k9	i
z	z	k7c2	z
jižních	jižní	k2eAgFnPc2d1	jižní
částí	část	k1gFnPc2	část
alpínské	alpínský	k2eAgFnSc2d1	alpínská
geosynklinály	geosynklinála	k1gFnSc2	geosynklinála
(	(	kIx(	(
<g/>
Dinaridy	Dinarid	k1gInPc1	Dinarid
<g/>
,	,	kIx,	,
pohoří	pohoří	k1gNnSc1	pohoří
Bükk	Bükka	k1gFnPc2	Bükka
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Začínají	začínat	k5eAaImIp3nP	začínat
se	se	k3xPyFc4	se
též	též	k9	též
projevovat	projevovat	k5eAaImF	projevovat
první	první	k4xOgFnSc1	první
fáze	fáze	k1gFnSc1	fáze
alpínského	alpínský	k2eAgNnSc2d1	alpínské
vrásnění	vrásnění	k1gNnSc2	vrásnění
–	–	k?	–
labinská	labinský	k2eAgFnSc1d1	labinský
fáze	fáze	k1gFnSc1	fáze
v	v	k7c6	v
carnu	carn	k1gInSc6	carn
a	a	k8xC	a
starokimerská	starokimerský	k2eAgFnSc1d1	starokimerský
fáze	fáze	k1gFnSc1	fáze
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
triasu	trias	k1gInSc2	trias
a	a	k8xC	a
jury	jura	k1gFnSc2	jura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
v	v	k7c6	v
triasu	trias	k1gInSc6	trias
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Fauna	fauna	k1gFnSc1	fauna
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
spodním	spodní	k2eAgInSc6d1	spodní
triasu	trias	k1gInSc6	trias
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
praví	pravý	k2eAgMnPc1d1	pravý
amoniti	amonit	k5eAaImF	amonit
a	a	k8xC	a
nastává	nastávat	k5eAaImIp3nS	nastávat
jejich	jejich	k3xOp3gInSc4	jejich
krátký	krátký	k2eAgInSc4d1	krátký
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bouřlivý	bouřlivý	k2eAgInSc1d1	bouřlivý
vývoj	vývoj	k1gInSc1	vývoj
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
popsáno	popsat	k5eAaPmNgNnS	popsat
přes	přes	k7c4	přes
300	[number]	k4	300
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ukončený	ukončený	k2eAgInSc1d1	ukončený
ve	v	k7c6	v
svrchním	svrchní	k2eAgInSc6d1	svrchní
triasu	trias	k1gInSc6	trias
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc1	všechen
rody	rod	k1gInPc1	rod
vymírají	vymírat	k5eAaImIp3nP	vymírat
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k2eAgFnSc7d1	vedoucí
skupinou	skupina	k1gFnSc7	skupina
byli	být	k5eAaImAgMnP	být
Ceratitida	Ceratitida	k1gFnSc1	Ceratitida
(	(	kIx(	(
<g/>
Ceratites	Ceratites	k1gMnSc1	Ceratites
nodosus	nodosus	k1gMnSc1	nodosus
<g/>
)	)	kIx)	)
se	s	k7c7	s
spirálovitě	spirálovitě	k6eAd1	spirálovitě
stočenými	stočený	k2eAgFnPc7d1	stočená
schránkami	schránka	k1gFnPc7	schránka
a	a	k8xC	a
jednoduchými	jednoduchý	k2eAgInPc7d1	jednoduchý
<g/>
,	,	kIx,	,
silnými	silný	k2eAgInPc7d1	silný
žebry	žebr	k1gInPc7	žebr
<g/>
.	.	kIx.	.
</s>
<s>
Vymírají	vymírat	k5eAaImIp3nP	vymírat
zástupci	zástupce	k1gMnPc1	zástupce
ortocerů	ortocer	k1gInPc2	ortocer
<g/>
,	,	kIx,	,
nastupují	nastupovat	k5eAaImIp3nP	nastupovat
dvoužábří	dvoužábrý	k2eAgMnPc1d1	dvoužábrý
hlavonožci	hlavonožec	k1gMnPc1	hlavonožec
–	–	k?	–
belemniti	belemnit	k5eAaPmF	belemnit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mlžů	mlž	k1gMnPc2	mlž
jsou	být	k5eAaImIp3nP	být
vůdčími	vůdčí	k2eAgInPc7d1	vůdčí
druhy	druh	k1gInPc7	druh
Claraia	Claraium	k1gNnSc2	Claraium
<g/>
,	,	kIx,	,
Costatoria	Costatorium	k1gNnSc2	Costatorium
<g/>
,	,	kIx,	,
Halobyia	Halobyium	k1gNnSc2	Halobyium
<g/>
,	,	kIx,	,
Megalodon	Megalodona	k1gFnPc2	Megalodona
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgMnPc3d1	další
zástupcům	zástupce	k1gMnPc3	zástupce
fauny	fauna	k1gFnSc2	fauna
patří	patřit	k5eAaImIp3nS	patřit
brachiopodi	brachiopod	k1gMnPc1	brachiopod
<g/>
,	,	kIx,	,
krinoidi	krinoid	k1gMnPc1	krinoid
<g/>
,	,	kIx,	,
vápnité	vápnitý	k2eAgFnPc1d1	vápnitá
houby	houba	k1gFnPc1	houba
<g/>
,	,	kIx,	,
šestičetní	šestičetný	k2eAgMnPc1d1	šestičetný
koráli	korál	k1gMnPc1	korál
<g/>
,	,	kIx,	,
ježovky	ježovka	k1gFnPc1	ježovka
<g/>
,	,	kIx,	,
foraminifery	foraminifera	k1gFnPc1	foraminifera
či	či	k8xC	či
konodonti	konodonť	k1gFnPc1	konodonť
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
triasu	trias	k1gInSc2	trias
už	už	k6eAd1	už
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
také	také	k9	také
první	první	k4xOgMnPc1	první
motýli	motýl	k1gMnPc1	motýl
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
zástupci	zástupce	k1gMnPc1	zástupce
skupiny	skupina	k1gFnSc2	skupina
Lepidoptera	Lepidopter	k1gMnSc2	Lepidopter
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
ryb	ryba	k1gFnPc2	ryba
dominují	dominovat	k5eAaImIp3nP	dominovat
Teleostei	Teleostee	k1gFnSc4	Teleostee
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Austrálie	Austrálie	k1gFnSc2	Austrálie
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
nálezy	nález	k1gInPc1	nález
posledních	poslední	k2eAgMnPc2d1	poslední
sladkovodních	sladkovodní	k2eAgMnPc2d1	sladkovodní
žraloků	žralok	k1gMnPc2	žralok
<g/>
.	.	kIx.	.
</s>
<s>
Paprskoploutvé	Paprskoploutvý	k2eAgFnPc1d1	Paprskoploutvý
ryby	ryba	k1gFnPc1	ryba
začínají	začínat	k5eAaImIp3nP	začínat
pronikat	pronikat	k5eAaImF	pronikat
i	i	k9	i
do	do	k7c2	do
slaných	slaný	k2eAgFnPc2d1	slaná
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
hlavní	hlavní	k2eAgFnSc7d1	hlavní
skupinou	skupina	k1gFnSc7	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
obojživelníky	obojživelník	k1gMnPc7	obojživelník
vymírají	vymírat	k5eAaImIp3nP	vymírat
Stegocephali	Stegocephali	k1gFnPc1	Stegocephali
<g/>
,	,	kIx,	,
k	k	k7c3	k
předchůdcům	předchůdce	k1gMnPc3	předchůdce
moderních	moderní	k2eAgFnPc2d1	moderní
žab	žába	k1gFnPc2	žába
řadíme	řadit	k5eAaImIp1nP	řadit
druh	druh	k1gMnSc1	druh
Triadobatrachus	Triadobatrachus	k1gMnSc1	Triadobatrachus
massinoti	massinot	k1gMnPc1	massinot
s	s	k7c7	s
lebkou	lebka	k1gFnSc7	lebka
charakteru	charakter	k1gInSc2	charakter
dnešních	dnešní	k2eAgFnPc2d1	dnešní
žab	žába	k1gFnPc2	žába
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
plazů	plaz	k1gMnPc2	plaz
existovalo	existovat	k5eAaImAgNnS	existovat
asi	asi	k9	asi
14	[number]	k4	14
řádů	řád	k1gInPc2	řád
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
zástupci	zástupce	k1gMnPc1	zástupce
čtyř	čtyři	k4xCgMnPc2	čtyři
(	(	kIx(	(
<g/>
např.	např.	kA	např.
želvy	želva	k1gFnSc2	želva
<g/>
)	)	kIx)	)
přežili	přežít	k5eAaPmAgMnP	přežít
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
Therapsida	Therapsida	k1gFnSc1	Therapsida
<g/>
,	,	kIx,	,
dělená	dělený	k2eAgFnSc1d1	dělená
na	na	k7c4	na
převážně	převážně	k6eAd1	převážně
býložravé	býložravý	k2eAgNnSc4d1	býložravé
Anomodontia	Anomodontium	k1gNnPc4	Anomodontium
a	a	k8xC	a
dravé	dravý	k2eAgFnPc4d1	dravá
Theriodontia	Theriodontium	k1gNnPc4	Theriodontium
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
skupiny	skupina	k1gFnPc1	skupina
Dicynodontia	Dicynodontius	k1gMnSc2	Dicynodontius
s	s	k7c7	s
progresivními	progresivní	k2eAgInPc7d1	progresivní
savčími	savčí	k2eAgInPc7d1	savčí
znaky	znak	k1gInPc7	znak
(	(	kIx(	(
<g/>
ustrnuli	ustrnout	k5eAaPmAgMnP	ustrnout
však	však	k9	však
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nálezy	nález	k1gInPc1	nález
rodu	rod	k1gInSc2	rod
Lystrosaurus	Lystrosaurus	k1gInSc4	Lystrosaurus
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
částečně	částečně	k6eAd1	částečně
žijícího	žijící	k2eAgInSc2d1	žijící
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
z	z	k7c2	z
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
jsou	být	k5eAaImIp3nP	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
dokladů	doklad	k1gInPc2	doklad
existence	existence	k1gFnSc2	existence
Gondwany	Gondwana	k1gFnSc2	Gondwana
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
skupinou	skupina	k1gFnSc7	skupina
byly	být	k5eAaImAgFnP	být
Dinocephalia	Dinocephalia	k1gFnSc1	Dinocephalia
(	(	kIx(	(
<g/>
rod	rod	k1gInSc1	rod
Jonkeria	Jonkerium	k1gNnSc2	Jonkerium
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
až	až	k9	až
5	[number]	k4	5
m	m	kA	m
a	a	k8xC	a
Moschops	Moschops	k1gInSc1	Moschops
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
přes	přes	k7c4	přes
3	[number]	k4	3
m	m	kA	m
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Theriodontia	Theriodontium	k1gNnPc1	Theriodontium
mají	mít	k5eAaImIp3nP	mít
již	již	k6eAd1	již
zřetelně	zřetelně	k6eAd1	zřetelně
rozlišený	rozlišený	k2eAgInSc4d1	rozlišený
chrup	chrup	k1gInSc4	chrup
na	na	k7c4	na
řezáky	řezák	k1gInPc4	řezák
<g/>
,	,	kIx,	,
špičáky	špičák	k1gInPc4	špičák
a	a	k8xC	a
stoličky	stolička	k1gFnPc4	stolička
<g/>
.	.	kIx.	.
</s>
<s>
Uzavřením	uzavření	k1gNnSc7	uzavření
spodiny	spodina	k1gFnSc2	spodina
lebeční	lebeční	k2eAgFnSc2d1	lebeční
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
pevné	pevný	k2eAgNnSc1d1	pevné
patro	patro	k1gNnSc1	patro
<g/>
,	,	kIx,	,
končetiny	končetina	k1gFnPc1	končetina
se	se	k3xPyFc4	se
posunují	posunovat	k5eAaImIp3nP	posunovat
pod	pod	k7c4	pod
tělo	tělo	k1gNnSc4	tělo
do	do	k7c2	do
svislé	svislý	k2eAgFnSc2d1	svislá
polohy	poloha	k1gFnSc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Vzorec	vzorec	k1gInSc4	vzorec
prstních	prstní	k2eAgInPc2d1	prstní
článků	článek	k1gInPc2	článek
2,3	[number]	k4	2,3
<g/>
,3	,3	k4	,3
<g/>
,3	,3	k4	,3
<g/>
,3	,3	k4	,3
máme	mít	k5eAaImIp1nP	mít
dnes	dnes	k6eAd1	dnes
i	i	k8xC	i
my	my	k3xPp1nPc1	my
<g/>
.	.	kIx.	.
</s>
<s>
Pokračoval	pokračovat	k5eAaImAgInS	pokračovat
vývoj	vývoj	k1gInSc1	vývoj
izolační	izolační	k2eAgFnSc2d1	izolační
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
představovala	představovat	k5eAaImAgFnS	představovat
srst	srst	k1gFnSc1	srst
a	a	k8xC	a
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
první	první	k4xOgFnPc1	první
mléčné	mléčný	k2eAgFnPc1d1	mléčná
žlázy	žláza	k1gFnPc1	žláza
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
z	z	k7c2	z
potních	potní	k2eAgFnPc2d1	potní
žláz	žláza	k1gFnPc2	žláza
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
primitivním	primitivní	k2eAgInPc3d1	primitivní
plazům	plaz	k1gInPc3	plaz
patřili	patřit	k5eAaImAgMnP	patřit
ichtyosauři	ichtyosaurus	k1gMnPc1	ichtyosaurus
(	(	kIx(	(
<g/>
ryboještěři	ryboještěr	k1gMnPc1	ryboještěr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
velcí	velký	k2eAgMnPc1d1	velký
zástupci	zástupce	k1gMnPc1	zástupce
jako	jako	k8xS	jako
Leptopterygius	Leptopterygius	k1gInSc1	Leptopterygius
acutirostris	acutirostris	k1gFnPc2	acutirostris
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
délky	délka	k1gFnSc2	délka
až	až	k9	až
12	[number]	k4	12
m	m	kA	m
a	a	k8xC	a
Placodontia	Placodontia	k1gFnSc1	Placodontia
(	(	kIx(	(
<g/>
nálezy	nález	k1gInPc1	nález
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
)	)	kIx)	)
se	s	k7c7	s
silnými	silný	k2eAgInPc7d1	silný
knoflíkovými	knoflíkový	k2eAgInPc7d1	knoflíkový
zuby	zub	k1gInPc7	zub
k	k	k7c3	k
drcení	drcení	k1gNnSc3	drcení
schránek	schránka	k1gFnPc2	schránka
a	a	k8xC	a
ojedinělým	ojedinělý	k2eAgInSc7d1	ojedinělý
břišním	břišní	k2eAgInSc7d1	břišní
krunýřem	krunýř	k1gInSc7	krunýř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
triasu	trias	k1gInSc6	trias
se	se	k3xPyFc4	se
objevující	objevující	k2eAgFnSc1d1	objevující
skupina	skupina	k1gFnSc1	skupina
Thecodontia	Thecodontia	k1gFnSc1	Thecodontia
(	(	kIx(	(
<g/>
jamkozubí	jamkozubí	k1gNnSc1	jamkozubí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
reprezentována	reprezentován	k2eAgFnSc1d1	reprezentována
malými	malý	k2eAgMnPc7d1	malý
masožravci	masožravec	k1gMnPc7	masožravec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
evolučně	evolučně	k6eAd1	evolučně
významným	významný	k2eAgMnSc7d1	významný
předchůdcem	předchůdce	k1gMnSc7	předchůdce
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
,	,	kIx,	,
krokodýlů	krokodýl	k1gMnPc2	krokodýl
i	i	k8xC	i
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
rétu	rét	k1gInSc2	rét
jsou	být	k5eAaImIp3nP	být
nálezy	nález	k1gInPc1	nález
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
doloženi	doložen	k2eAgMnPc1d1	doložen
i	i	k8xC	i
první	první	k4xOgMnSc1	první
zástupci	zástupce	k1gMnPc7	zástupce
primitivních	primitivní	k2eAgMnPc2d1	primitivní
savců	savec	k1gMnPc2	savec
(	(	kIx(	(
<g/>
hmyzožravci	hmyzožravec	k1gMnPc1	hmyzožravec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
triasu	trias	k1gInSc6	trias
(	(	kIx(	(
<g/>
asi	asi	k9	asi
před	před	k7c7	před
235	[number]	k4	235
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
také	také	k9	také
objevují	objevovat	k5eAaImIp3nP	objevovat
první	první	k4xOgMnPc1	první
praví	pravý	k2eAgMnPc1d1	pravý
dinosauři	dinosaurus	k1gMnPc1	dinosaurus
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
dominantní	dominantní	k2eAgFnSc4d1	dominantní
formu	forma	k1gFnSc4	forma
suchozemské	suchozemský	k2eAgFnSc2d1	suchozemská
megafauny	megafauna	k1gFnSc2	megafauna
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
následujících	následující	k2eAgInPc2d1	následující
170	[number]	k4	170
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
křídy	křída	k1gFnSc2	křída
před	před	k7c7	před
66	[number]	k4	66
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Flóra	Flóra	k1gFnSc1	Flóra
===	===	k?	===
</s>
</p>
<p>
<s>
Značný	značný	k2eAgInSc4d1	značný
rozvoj	rozvoj	k1gInSc4	rozvoj
zaznamenaly	zaznamenat	k5eAaPmAgFnP	zaznamenat
mořské	mořský	k2eAgFnPc1d1	mořská
zelené	zelený	k2eAgFnPc1d1	zelená
řasy	řasa	k1gFnPc1	řasa
(	(	kIx(	(
<g/>
Dasycladaceae	Dasycladacea	k1gFnPc1	Dasycladacea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
teplá	teplý	k2eAgNnPc4d1	teplé
mělká	mělký	k2eAgNnPc4d1	mělké
moře	moře	k1gNnPc4	moře
s	s	k7c7	s
hloubkou	hloubka	k1gFnSc7	hloubka
do	do	k7c2	do
20	[number]	k4	20
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
porosty	porost	k1gInPc1	porost
tvořily	tvořit	k5eAaImAgInP	tvořit
zejména	zejména	k9	zejména
v	v	k7c6	v
lagunách	laguna	k1gFnPc6	laguna
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgFnP	být
chráněny	chránit	k5eAaImNgInP	chránit
rify	rif	k1gInPc1	rif
<g/>
.	.	kIx.	.
</s>
<s>
Suchozemské	suchozemský	k2eAgFnSc3d1	suchozemská
flóře	flóra	k1gFnSc3	flóra
dominují	dominovat	k5eAaImIp3nP	dominovat
nahosemenné	nahosemenný	k2eAgInPc4d1	nahosemenný
–	–	k?	–
jehličnany	jehličnan	k1gInPc4	jehličnan
(	(	kIx(	(
<g/>
Voltzia	Voltzia	k1gFnSc1	Voltzia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cykasy	cykas	k1gInPc1	cykas
(	(	kIx(	(
<g/>
Cycadaceae	Cycadaceae	k1gFnSc1	Cycadaceae
<g/>
)	)	kIx)	)
s	s	k7c7	s
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
<g/>
,	,	kIx,	,
kožovitými	kožovitý	k2eAgInPc7d1	kožovitý
listy	list	k1gInPc7	list
<g/>
,	,	kIx,	,
stromovitého	stromovitý	k2eAgInSc2d1	stromovitý
habitu	habitus	k1gInSc2	habitus
popř.	popř.	kA	popř.
se	s	k7c7	s
soudkovitým	soudkovitý	k2eAgInSc7d1	soudkovitý
<g/>
,	,	kIx,	,
nízkým	nízký	k2eAgInSc7d1	nízký
kmenem	kmen	k1gInSc7	kmen
či	či	k8xC	či
gingovité	gingovitý	k2eAgNnSc1d1	gingovitý
(	(	kIx(	(
<g/>
Baiera	Baier	k1gMnSc4	Baier
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
výtrusných	výtrusný	k2eAgMnPc2d1	výtrusný
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
přesličky	přeslička	k1gFnPc1	přeslička
(	(	kIx(	(
<g/>
Equisetites	Equisetites	k1gInSc1	Equisetites
<g/>
)	)	kIx)	)
a	a	k8xC	a
kapradiny	kapradina	k1gFnSc2	kapradina
(	(	kIx(	(
<g/>
Gleicheniaceae	Gleicheniaceae	k1gInSc1	Gleicheniaceae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Germánský	germánský	k2eAgInSc1d1	germánský
vývoj	vývoj	k1gInSc1	vývoj
triasu	trias	k1gInSc2	trias
==	==	k?	==
</s>
</p>
<p>
<s>
Zastoupen	zastoupen	k2eAgMnSc1d1	zastoupen
na	na	k7c6	na
území	území	k1gNnSc6	území
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
mocnost	mocnost	k1gFnSc1	mocnost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
3000	[number]	k4	3000
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgInSc1d1	spodní
trias	trias	k1gInSc1	trias
(	(	kIx(	(
<g/>
pestrý	pestrý	k2eAgInSc1d1	pestrý
pískovec	pískovec	k1gInSc1	pískovec
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
kontinentální	kontinentální	k2eAgFnSc4d1	kontinentální
facii	facie	k1gFnSc4	facie
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnPc4d1	střední
mořské	mořský	k2eAgFnPc4d1	mořská
a	a	k8xC	a
svrchní	svrchní	k2eAgFnPc4d1	svrchní
lagunární	lagunární	k2eAgNnPc4d1	lagunární
<g/>
.	.	kIx.	.
</s>
<s>
Pestrý	pestrý	k2eAgInSc1d1	pestrý
pískovec	pískovec	k1gInSc1	pískovec
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
vložky	vložka	k1gFnPc1	vložka
vápenců	vápenec	k1gInPc2	vápenec
<g/>
,	,	kIx,	,
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
jsou	být	k5eAaImIp3nP	být
hojné	hojný	k2eAgFnPc1d1	hojná
stopy	stopa	k1gFnPc1	stopa
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Využíván	využívat	k5eAaImNgInS	využívat
byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
stavební	stavební	k2eAgInSc1d1	stavební
kámen	kámen	k1gInSc1	kámen
(	(	kIx(	(
<g/>
katedrála	katedrála	k1gFnSc1	katedrála
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
triasu	trias	k1gInSc6	trias
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zalití	zalití	k1gNnSc3	zalití
většiny	většina	k1gFnSc2	většina
prostoru	prostor	k1gInSc2	prostor
mělkým	mělký	k2eAgNnSc7d1	mělké
epikontinentálním	epikontinentální	k2eAgNnSc7d1	epikontinentální
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
při	při	k7c6	při
okrajích	okraj	k1gInPc6	okraj
místy	místy	k6eAd1	místy
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
sedimentace	sedimentace	k1gFnSc1	sedimentace
kontinentálně-terestrických	kontinentálněerestrický	k2eAgFnPc2d1	kontinentálně-terestrický
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
občasném	občasný	k2eAgNnSc6d1	občasné
uzavření	uzavření	k1gNnSc6	uzavření
lagun	laguna	k1gFnPc2	laguna
se	se	k3xPyFc4	se
namísto	namísto	k7c2	namísto
vápenců	vápenec	k1gInPc2	vápenec
ukládaly	ukládat	k5eAaImAgFnP	ukládat
evapority	evaporita	k1gFnPc1	evaporita
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svrchní	svrchní	k2eAgFnSc6d1	svrchní
partiích	partie	k1gFnPc6	partie
jsou	být	k5eAaImIp3nP	být
hojné	hojný	k2eAgFnPc1d1	hojná
zkameněliny	zkamenělina	k1gFnPc1	zkamenělina
(	(	kIx(	(
<g/>
krinodi	krinod	k1gMnPc1	krinod
<g/>
,	,	kIx,	,
Ceratites	Ceratites	k1gMnSc1	Ceratites
nodusus	nodusus	k1gMnSc1	nodusus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
u	u	k7c2	u
Bytomi	Byto	k1gFnPc7	Byto
je	on	k3xPp3gMnPc4	on
znám	znám	k2eAgInSc1d1	znám
rudonosný	rudonosný	k2eAgInSc1d1	rudonosný
dolomit	dolomit	k1gInSc1	dolomit
s	s	k7c7	s
rudami	ruda	k1gFnPc7	ruda
Pb-	Pb-	k1gFnPc2	Pb-
<g/>
Zn.	zn.	kA	zn.
Po	po	k7c6	po
regresi	regrese	k1gFnSc6	regrese
moře	moře	k1gNnSc2	moře
sedimentují	sedimentovat	k5eAaImIp3nP	sedimentovat
břidlice	břidlice	k1gFnPc1	břidlice
<g/>
,	,	kIx,	,
pískovce	pískovec	k1gInPc1	pískovec
a	a	k8xC	a
dolomity	dolomit	k1gInPc1	dolomit
keuperu	keuper	k1gInSc2	keuper
<g/>
,	,	kIx,	,
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
místy	místy	k6eAd1	místy
se	s	k7c7	s
slojkami	slojka	k1gFnPc7	slojka
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
transgresi	transgrese	k1gFnSc6	transgrese
vznikají	vznikat	k5eAaImIp3nP	vznikat
polohy	poloha	k1gFnPc1	poloha
sádrovce	sádrovec	k1gInSc2	sádrovec
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
partie	partie	k1gFnSc1	partie
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
mořská	mořský	k2eAgFnSc1d1	mořská
transgrese	transgrese	k1gFnSc1	transgrese
s	s	k7c7	s
napojením	napojení	k1gNnSc7	napojení
na	na	k7c4	na
oceán	oceán	k1gInSc4	oceán
Tethys	Tethys	k1gInSc4	Tethys
(	(	kIx(	(
<g/>
společná	společný	k2eAgFnSc1d1	společná
fauna	fauna	k1gFnSc1	fauna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Alpínský	alpínský	k2eAgInSc4d1	alpínský
vývoj	vývoj	k1gInSc4	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
Východních	východní	k2eAgFnPc2d1	východní
Alp	Alpy	k1gFnPc2	Alpy
se	se	k3xPyFc4	se
rozkládala	rozkládat	k5eAaImAgFnS	rozkládat
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
mělčina	mělčina	k1gFnSc1	mělčina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ukládalo	ukládat	k5eAaImAgNnS	ukládat
vápnité	vápnitý	k2eAgNnSc1d1	vápnité
bahno	bahno	k1gNnSc1	bahno
za	za	k7c2	za
podmínek	podmínka	k1gFnPc2	podmínka
podobných	podobný	k2eAgFnPc2d1	podobná
dnes	dnes	k6eAd1	dnes
na	na	k7c6	na
Bahamské	bahamský	k2eAgFnSc6d1	bahamská
platformě	platforma	k1gFnSc6	platforma
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
postupném	postupný	k2eAgNnSc6d1	postupné
poklesávání	poklesávání	k1gNnSc6	poklesávání
pánve	pánev	k1gFnSc2	pánev
se	se	k3xPyFc4	se
tvořily	tvořit	k5eAaImAgInP	tvořit
tisíce	tisíc	k4xCgInPc1	tisíc
metrů	metr	k1gMnPc2	metr
mocné	mocný	k2eAgFnSc2d1	mocná
vrstvy	vrstva	k1gFnSc2	vrstva
vápenců	vápenec	k1gInPc2	vápenec
a	a	k8xC	a
dolomitů	dolomit	k1gInPc2	dolomit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bavorské	bavorský	k2eAgInPc1d1	bavorský
příkrovy	příkrov	k1gInPc1	příkrov
===	===	k?	===
</s>
</p>
<p>
<s>
Anisu	Anisa	k1gFnSc4	Anisa
patří	patřit	k5eAaImIp3nS	patřit
gutensteinské	gutensteinský	k2eAgInPc4d1	gutensteinský
vápence	vápenec	k1gInPc4	vápenec
<g/>
,	,	kIx,	,
ladinu	ladinout	k5eAaPmIp1nS	ladinout
wettersteinské	wettersteinský	k2eAgInPc4d1	wettersteinský
vápence	vápenec	k1gInPc4	vápenec
<g/>
,	,	kIx,	,
partnachské	partnachský	k2eAgInPc4d1	partnachský
lupky	lupek	k1gInPc4	lupek
<g/>
,	,	kIx,	,
reiflinské	reiflinský	k2eAgInPc4d1	reiflinský
vápence	vápenec	k1gInPc4	vápenec
s	s	k7c7	s
polohami	poloha	k1gFnPc7	poloha
tufů	tuf	k1gInPc2	tuf
<g/>
,	,	kIx,	,
karnu	karnout	k5eAaPmIp1nS	karnout
lunzské	lunzský	k2eAgFnPc4d1	lunzský
vrstvy	vrstva	k1gFnPc4	vrstva
se	s	k7c7	s
slojemi	sloj	k1gFnPc7	sloj
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
hauptdolomit	hauptdolomit	k5eAaPmF	hauptdolomit
až	až	k9	až
3000	[number]	k4	3000
metrů	metr	k1gInPc2	metr
mocný	mocný	k2eAgMnSc1d1	mocný
<g/>
,	,	kIx,	,
rétu	rét	k1gInSc6	rét
kössenské	kössenský	k2eAgFnPc4d1	kössenský
vrstvy	vrstva	k1gFnPc4	vrstva
</s>
</p>
<p>
<s>
===	===	k?	===
Hallštattský	Hallštattský	k2eAgInSc1d1	Hallštattský
příkrov	příkrov	k1gInSc1	příkrov
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
jsou	být	k5eAaImIp3nP	být
solná	solný	k2eAgNnPc1d1	solné
ložiska	ložisko	k1gNnPc1	ložisko
stáří	stáří	k1gNnSc2	stáří
perm-verfen	permerfna	k1gFnPc2	perm-verfna
a	a	k8xC	a
červené	červený	k2eAgInPc4d1	červený
hlíznaté	hlíznatý	k2eAgInPc4d1	hlíznatý
vápence	vápenec	k1gInPc4	vápenec
<g/>
,	,	kIx,	,
anis	anis	k6eAd1	anis
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
schreyeralmské	schreyeralmský	k2eAgInPc4d1	schreyeralmský
vápence	vápenec	k1gInPc4	vápenec
<g/>
,	,	kIx,	,
ladin-norik	ladinorik	k1gMnSc1	ladin-norik
hallštattský	hallštattský	k2eAgInSc4d1	hallštattský
vápenec	vápenec	k1gInSc4	vápenec
<g/>
,	,	kIx,	,
rét	rét	k1gInSc4	rét
zlambachské	zlambachský	k2eAgFnSc2d1	zlambachský
břidlice	břidlice	k1gFnSc2	břidlice
</s>
</p>
<p>
<s>
===	===	k?	===
Dachsteinský	dachsteinský	k2eAgInSc1d1	dachsteinský
příkrov	příkrov	k1gInSc1	příkrov
===	===	k?	===
</s>
</p>
<p>
<s>
Stáří	stáří	k1gNnSc1	stáří
střední	střední	k2eAgFnSc2d1	střední
trias	trias	k1gInSc4	trias
mají	mít	k5eAaImIp3nP	mít
ramsauské	ramsauský	k2eAgInPc1d1	ramsauský
dolomity	dolomit	k1gInPc1	dolomit
<g/>
,	,	kIx,	,
karn	karn	k1gNnSc1	karn
karditové	karditový	k2eAgFnSc2d1	karditový
vrstvy	vrstva	k1gFnSc2	vrstva
a	a	k8xC	a
svrchní	svrchní	k2eAgInSc1d1	svrchní
trias	trias	k1gInSc1	trias
pak	pak	k6eAd1	pak
dachsteinské	dachsteinský	k2eAgInPc4d1	dachsteinský
vápence	vápenec	k1gInPc4	vápenec
</s>
</p>
<p>
<s>
===	===	k?	===
Dinaridy	Dinarid	k1gInPc7	Dinarid
(	(	kIx(	(
<g/>
Jižní	jižní	k2eAgFnSc2d1	jižní
Alpy	alpa	k1gFnSc2	alpa
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
spodním	spodní	k2eAgInSc6d1	spodní
triasu	trias	k1gInSc6	trias
se	se	k3xPyFc4	se
ukládala	ukládat	k5eAaImAgFnS	ukládat
klastika	klastika	k1gFnSc1	klastika
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středním-svrchním	střednímvrchní	k1gMnSc6	středním-svrchní
vápence	vápenec	k1gInSc2	vápenec
<g/>
,	,	kIx,	,
časté	častý	k2eAgInPc1d1	častý
jsou	být	k5eAaImIp3nP	být
výlevy	výlev	k1gInPc1	výlev
bazických	bazický	k2eAgInPc2d1	bazický
i	i	k8xC	i
kyselých	kyselý	k2eAgInPc2d1	kyselý
vulkanitů	vulkanit	k1gInPc2	vulkanit
(	(	kIx(	(
<g/>
buchensteinské	buchensteinský	k2eAgFnPc1d1	buchensteinský
vrstvy	vrstva	k1gFnPc1	vrstva
<g/>
,	,	kIx,	,
wengenské	wengenský	k2eAgFnPc1d1	wengenský
vrstvy	vrstva	k1gFnPc1	vrstva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zastoupeny	zastoupen	k2eAgInPc1d1	zastoupen
jsou	být	k5eAaImIp3nP	být
útesové	útesový	k2eAgInPc4d1	útesový
vápence	vápenec	k1gInPc4	vápenec
(	(	kIx(	(
<g/>
schlernský	schlernský	k2eAgInSc4d1	schlernský
dolomit	dolomit	k1gInSc4	dolomit
<g/>
,	,	kIx,	,
rabeljský	rabeljský	k2eAgInSc4d1	rabeljský
dolomit	dolomit	k1gInSc4	dolomit
s	s	k7c7	s
Pb-Zn	Pb-Zna	k1gFnPc2	Pb-Zna
zrudněním	zrudnění	k1gNnSc7	zrudnění
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Trias	trias	k1gInSc4	trias
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
masívu	masív	k1gInSc6	masív
==	==	k?	==
</s>
</p>
<p>
<s>
Spodnotriasové	Spodnotriasový	k2eAgInPc1d1	Spodnotriasový
sedimenty	sediment	k1gInPc1	sediment
jsou	být	k5eAaImIp3nP	být
kontinentálního	kontinentální	k2eAgMnSc4d1	kontinentální
původu	původa	k1gMnSc4	původa
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
stupni	stupeň	k1gInSc3	stupeň
pestrý	pestrý	k2eAgInSc1d1	pestrý
pískovec	pískovec	k1gInSc1	pískovec
<g/>
.	.	kIx.	.
</s>
<s>
Zastoupeny	zastoupen	k2eAgInPc1d1	zastoupen
jsou	být	k5eAaImIp3nP	být
arkózové	arkózový	k2eAgInPc1d1	arkózový
pískovce	pískovec	k1gInPc1	pískovec
<g/>
,	,	kIx,	,
polymiktní	polymiktní	k2eAgInPc1d1	polymiktní
slepence	slepenec	k1gInPc1	slepenec
a	a	k8xC	a
kaolinizované	kaolinizovaný	k2eAgInPc1d1	kaolinizovaný
křemenné	křemenný	k2eAgInPc1d1	křemenný
pískovce	pískovec	k1gInPc1	pískovec
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgFnPc4d1	tvořící
bohuslavické	bohuslavická	k1gFnPc4	bohuslavická
souvrství	souvrství	k1gNnSc2	souvrství
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
spodních	spodní	k2eAgFnPc2d1	spodní
partií	partie	k1gFnPc2	partie
je	být	k5eAaImIp3nS	být
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
svrchních	svrchní	k2eAgFnPc6d1	svrchní
bílá	bílý	k2eAgNnPc4d1	bílé
<g/>
.	.	kIx.	.
</s>
<s>
Sedimenty	sediment	k1gInPc1	sediment
se	se	k3xPyFc4	se
ukládaly	ukládat	k5eAaImAgInP	ukládat
v	v	k7c6	v
průtočných	průtočný	k2eAgNnPc6d1	průtočné
jezerech	jezero	k1gNnPc6	jezero
nebo	nebo	k8xC	nebo
periodických	periodický	k2eAgInPc6d1	periodický
tocích	tok	k1gInPc6	tok
<g/>
.	.	kIx.	.
</s>
<s>
Výchozy	výchoz	k1gInPc1	výchoz
hornin	hornina	k1gFnPc2	hornina
triasu	trias	k1gInSc2	trias
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
Podkrkonoší	Podkrkonoší	k1gNnSc6	Podkrkonoší
jižně	jižně	k6eAd1	jižně
a	a	k8xC	a
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Trutnova	Trutnov	k1gInSc2	Trutnov
(	(	kIx(	(
<g/>
až	až	k9	až
65	[number]	k4	65
metrů	metr	k1gInPc2	metr
mocné	mocný	k2eAgFnPc1d1	mocná
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
obou	dva	k4xCgNnPc6	dva
křídlech	křídlo	k1gNnPc6	křídlo
dolnoslezské	dolnoslezský	k2eAgFnSc2d1	Dolnoslezská
pánve	pánev	k1gFnSc2	pánev
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
120	[number]	k4	120
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
všude	všude	k6eAd1	všude
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
pod	pod	k7c4	pod
sedimenty	sediment	k1gInPc4	sediment
svrchní	svrchní	k2eAgFnSc2d1	svrchní
křídy	křída	k1gFnSc2	křída
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
byly	být	k5eAaImAgFnP	být
těženy	těžen	k2eAgInPc1d1	těžen
pro	pro	k7c4	pro
stavební	stavební	k2eAgInPc4d1	stavební
účely	účel	k1gInPc4	účel
(	(	kIx(	(
<g/>
lom	lom	k1gInSc4	lom
"	"	kIx"	"
<g/>
U	u	k7c2	u
devíti	devět	k4xCc2	devět
křížů	kříž	k1gInPc2	kříž
<g/>
"	"	kIx"	"
západně	západně	k6eAd1	západně
od	od	k7c2	od
Červeného	Červeného	k2eAgInSc2d1	Červeného
Kostelce	Kostelec	k1gInSc2	Kostelec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Jinnan	Jinnan	k1gMnSc1	Jinnan
Tong	Tong	k1gMnSc1	Tong
<g/>
;	;	kIx,	;
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Triassic	Triassic	k1gMnSc1	Triassic
integrative	integrativ	k1gInSc5	integrativ
stratigraphy	stratigraph	k1gMnPc7	stratigraph
and	and	k?	and
timescale	timescale	k6eAd1	timescale
of	of	k?	of
China	China	k1gFnSc1	China
<g/>
.	.	kIx.	.
</s>
<s>
Science	Science	k1gFnSc1	Science
China	China	k1gFnSc1	China
Earth	Earth	k1gInSc4	Earth
Sciences	Sciences	k1gInSc1	Sciences
<g/>
.	.	kIx.	.
doi	doi	k?	doi
<g/>
:	:	kIx,	:
https://doi.org/10.1007/s11430-018-9278-0	[url]	k4	https://doi.org/10.1007/s11430-018-9278-0
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
trias	trias	k1gInSc1	trias
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
