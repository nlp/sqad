<p>
<s>
Ján	Ján	k1gMnSc1	Ján
Pivarník	Pivarník	k1gMnSc1	Pivarník
(	(	kIx(	(
<g/>
*	*	kIx~	*
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
Cejkov	Cejkov	k1gInSc1	Cejkov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
československý	československý	k2eAgMnSc1d1	československý
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
reprezentant	reprezentant	k1gMnSc1	reprezentant
<g/>
,	,	kIx,	,
slovenský	slovenský	k2eAgMnSc1d1	slovenský
fotbalista	fotbalista	k1gMnSc1	fotbalista
(	(	kIx(	(
<g/>
krajní	krajní	k2eAgMnSc1d1	krajní
obránce	obránce	k1gMnSc1	obránce
<g/>
)	)	kIx)	)
a	a	k8xC	a
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
trenér	trenér	k1gMnSc1	trenér
<g/>
.	.	kIx.	.
</s>
<s>
Mistr	mistr	k1gMnSc1	mistr
Evropy	Evropa	k1gFnSc2	Evropa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
manželem	manžel	k1gMnSc7	manžel
herečky	herečka	k1gFnSc2	herečka
Jarmily	Jarmila	k1gFnSc2	Jarmila
Koleničové	Kolenič	k1gMnPc1	Kolenič
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
synovcem	synovec	k1gMnSc7	synovec
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
fotbalista	fotbalista	k1gMnSc1	fotbalista
a	a	k8xC	a
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
trenér	trenér	k1gMnSc1	trenér
Roman	Roman	k1gMnSc1	Roman
Pivarník	Pivarník	k1gMnSc1	Pivarník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
fotbalem	fotbal	k1gInSc7	fotbal
začínal	začínat	k5eAaImAgMnS	začínat
v	v	k7c6	v
nedalekém	daleký	k2eNgInSc6d1	nedaleký
Trebišově	Trebišov	k1gInSc6	Trebišov
relativně	relativně	k6eAd1	relativně
pozdě	pozdě	k6eAd1	pozdě
<g/>
,	,	kIx,	,
až	až	k9	až
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
16	[number]	k4	16
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kariéra	kariéra	k1gFnSc1	kariéra
měla	mít	k5eAaImAgFnS	mít
však	však	k9	však
stoupající	stoupající	k2eAgFnSc4d1	stoupající
křivku	křivka	k1gFnSc4	křivka
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
jako	jako	k8xC	jako
20	[number]	k4	20
<g/>
letý	letý	k2eAgInSc1d1	letý
debutoval	debutovat	k5eAaBmAgInS	debutovat
za	za	k7c4	za
národní	národní	k2eAgInSc4d1	národní
tým	tým	k1gInSc4	tým
na	na	k7c6	na
prestižním	prestižní	k2eAgInSc6d1	prestižní
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
(	(	kIx(	(
<g/>
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
proti	proti	k7c3	proti
Santos	Santos	k1gInSc4	Santos
FC	FC	kA	FC
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
klubový	klubový	k2eAgInSc4d1	klubový
tým	tým	k1gInSc4	tým
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejprve	nejprve	k6eAd1	nejprve
hrál	hrát	k5eAaImAgInS	hrát
za	za	k7c4	za
domovský	domovský	k2eAgInSc4d1	domovský
Slavoj	Slavoj	k1gInSc4	Slavoj
Trebišov	Trebišov	k1gInSc1	Trebišov
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
–	–	k?	–
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
ve	v	k7c6	v
VSS	VSS	kA	VSS
Košice	Košice	k1gInPc1	Košice
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
–	–	k?	–
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
ve	v	k7c6	v
Slovanu	Slovan	k1gInSc6	Slovan
Bratislava	Bratislava	k1gFnSc1	Bratislava
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
–	–	k?	–
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgInSc7	který
získal	získat	k5eAaPmAgMnS	získat
celkem	celkem	k6eAd1	celkem
dva	dva	k4xCgInPc1	dva
tituly	titul	k1gInPc1	titul
mistra	mistr	k1gMnSc2	mistr
ČSSR	ČSSR	kA	ČSSR
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednou	jednou	k6eAd1	jednou
Československý	československý	k2eAgInSc4d1	československý
pohár	pohár	k1gInSc4	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
jedna	jeden	k4xCgFnSc1	jeden
sezóna	sezóna	k1gFnSc1	sezóna
za	za	k7c4	za
Duklu	Dukla	k1gFnSc4	Dukla
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
–	–	k?	–
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
29	[number]	k4	29
letech	let	k1gInPc6	let
úspěšného	úspěšný	k2eAgMnSc4d1	úspěšný
reprezentanta	reprezentant	k1gMnSc4	reprezentant
"	"	kIx"	"
<g/>
zradily	zradit	k5eAaPmAgFnP	zradit
<g/>
"	"	kIx"	"
oba	dva	k4xCgInPc1	dva
menisky	meniskus	k1gInPc1	meniskus
<g/>
,	,	kIx,	,
navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
ještě	ještě	k6eAd1	ještě
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xS	jako
hrající	hrající	k2eAgMnSc1d1	hrající
trenér	trenér	k1gMnSc1	trenér
v	v	k7c6	v
Kittsee	Kittsee	k1gFnSc6	Kittsee
(	(	kIx(	(
<g/>
II	II	kA	II
<g/>
.	.	kIx.	.
rakouská	rakouský	k2eAgFnSc1d1	rakouská
liga	liga	k1gFnSc1	liga
<g/>
)	)	kIx)	)
i	i	k9	i
ve	v	k7c6	v
španělském	španělský	k2eAgInSc6d1	španělský
Cadizu	Cadiz	k1gInSc6	Cadiz
(	(	kIx(	(
<g/>
I.	I.	kA	I.
liga	liga	k1gFnSc1	liga
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
potkal	potkat	k5eAaPmAgInS	potkat
s	s	k7c7	s
krajanem	krajan	k1gMnSc7	krajan
Dušanem	Dušan	k1gMnSc7	Dušan
Galisem	Galis	k1gInSc7	Galis
a	a	k8xC	a
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
španělské	španělský	k2eAgFnSc6d1	španělská
soutěži	soutěž	k1gFnSc6	soutěž
ve	v	k7c6	v
3	[number]	k4	3
utkáních	utkání	k1gNnPc6	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
když	když	k8xS	když
definitivně	definitivně	k6eAd1	definitivně
pověsil	pověsit	k5eAaPmAgMnS	pověsit
kopačky	kopačka	k1gFnPc4	kopačka
na	na	k7c4	na
hřebíček	hřebíček	k1gInSc4	hřebíček
<g/>
,	,	kIx,	,
dělal	dělat	k5eAaImAgMnS	dělat
asistenta	asistent	k1gMnSc4	asistent
trenéra	trenér	k1gMnSc4	trenér
v	v	k7c6	v
Austrii	Austrie	k1gFnSc6	Austrie
Memphis	Memphis	k1gFnSc2	Memphis
(	(	kIx(	(
<g/>
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
Austria	Austrium	k1gNnSc2	Austrium
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
v	v	k7c6	v
Sportingu	Sporting	k1gInSc6	Sporting
Lisabon	Lisabon	k1gInSc1	Lisabon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hlavním	hlavní	k2eAgMnSc7d1	hlavní
trenérem	trenér	k1gMnSc7	trenér
byl	být	k5eAaImAgMnS	být
Jozef	Jozef	k1gMnSc1	Jozef
Vengloš	Vengloš	k1gMnSc1	Vengloš
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
však	však	k9	však
měl	mít	k5eAaImAgMnS	mít
ambice	ambice	k1gFnPc4	ambice
stát	stát	k5eAaPmF	stát
se	s	k7c7	s
prvním	první	k4xOgMnSc7	první
trenérem	trenér	k1gMnSc7	trenér
na	na	k7c6	na
lavičce	lavička	k1gFnSc6	lavička
<g/>
.	.	kIx.	.
</s>
<s>
Štěstí	štěstí	k1gNnSc1	štěstí
našel	najít	k5eAaPmAgInS	najít
v	v	k7c6	v
exotické	exotický	k2eAgFnSc6d1	exotická
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgInS	působit
téměř	téměř	k6eAd1	téměř
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
trenér	trenér	k1gMnSc1	trenér
působil	působit	k5eAaImAgMnS	působit
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
arabských	arabský	k2eAgFnPc6d1	arabská
zemích	zem	k1gFnPc6	zem
–	–	k?	–
v	v	k7c6	v
Kuvajtu	Kuvajt	k1gInSc6	Kuvajt
<g/>
,	,	kIx,	,
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
<g/>
,	,	kIx,	,
Kataru	katar	k1gInSc6	katar
<g/>
,	,	kIx,	,
Ománu	Omán	k1gInSc6	Omán
i	i	k8xC	i
Spojených	spojený	k2eAgInPc6d1	spojený
arabských	arabský	k2eAgInPc6d1	arabský
emirátech	emirát	k1gInPc6	emirát
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
týmem	tým	k1gInSc7	tým
Al	ala	k1gFnPc2	ala
Qadisiya	Qadisiya	k1gMnSc1	Qadisiya
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
Asijský	asijský	k2eAgInSc4d1	asijský
pohár	pohár	k1gInSc4	pohár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
československé	československý	k2eAgFnSc6d1	Československá
reprezentaci	reprezentace	k1gFnSc6	reprezentace
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
ve	v	k7c6	v
39	[number]	k4	39
utkáních	utkání	k1gNnPc6	utkání
<g/>
,	,	kIx,	,
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
lize	liga	k1gFnSc6	liga
dal	dát	k5eAaPmAgMnS	dát
v	v	k7c6	v
267	[number]	k4	267
zápasech	zápas	k1gInPc6	zápas
14	[number]	k4	14
gólů	gól	k1gInPc2	gól
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
československý	československý	k2eAgMnSc1d1	československý
fotbalista	fotbalista	k1gMnSc1	fotbalista
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Poháru	pohár	k1gInSc6	pohár
mistrů	mistr	k1gMnPc2	mistr
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
ve	v	k7c6	v
2	[number]	k4	2
utkáních	utkání	k1gNnPc6	utkání
a	a	k8xC	a
v	v	k7c6	v
Poháru	pohár	k1gInSc6	pohár
UEFA	UEFA	kA	UEFA
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
ve	v	k7c6	v
3	[number]	k4	3
utkáních	utkání	k1gNnPc6	utkání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavost	zajímavost	k1gFnSc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1970	[number]	k4	1970
ho	on	k3xPp3gInSc4	on
pražská	pražský	k2eAgFnSc1d1	Pražská
Sparta	Sparta	k1gFnSc1	Sparta
pozvala	pozvat	k5eAaPmAgFnS	pozvat
na	na	k7c4	na
velký	velký	k2eAgInSc4d1	velký
zájezd	zájezd	k1gInSc4	zájezd
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
VSS	VSS	kA	VSS
Košice	Košice	k1gInPc1	Košice
odmítly	odmítnout	k5eAaPmAgInP	odmítnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
považovaly	považovat	k5eAaImAgFnP	považovat
za	za	k7c4	za
první	první	k4xOgInSc4	první
krok	krok	k1gInSc4	krok
Sparty	Sparta	k1gFnSc2	Sparta
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
získání	získání	k1gNnSc3	získání
<g/>
.	.	kIx.	.
</s>
<s>
Spartě	Sparta	k1gFnSc3	Sparta
to	ten	k3xDgNnSc1	ten
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
trenér	trenér	k1gMnSc1	trenér
Jozef	Jozef	k1gMnSc1	Jozef
Marko	Marko	k1gMnSc1	Marko
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mohl	moct	k5eAaImAgMnS	moct
před	před	k7c7	před
MS	MS	kA	MS
1970	[number]	k4	1970
získat	získat	k5eAaPmF	získat
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Pivarník	Pivarník	k1gMnSc1	Pivarník
chtěl	chtít	k5eAaImAgMnS	chtít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
studovat	studovat	k5eAaImF	studovat
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
se	s	k7c7	s
Spartou	Sparta	k1gFnSc7	Sparta
už	už	k6eAd1	už
trénoval	trénovat	k5eAaImAgMnS	trénovat
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
dojížděl	dojíždět	k5eAaImAgMnS	dojíždět
do	do	k7c2	do
Košic	Košice	k1gInPc2	Košice
k	k	k7c3	k
zápasům	zápas	k1gInPc3	zápas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
VSS	VSS	kA	VSS
mu	on	k3xPp3gMnSc3	on
přestup	přestup	k1gInSc4	přestup
nepovolily	povolit	k5eNaPmAgFnP	povolit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Pivarník	Pivarník	k1gMnSc1	Pivarník
později	pozdě	k6eAd2	pozdě
"	"	kIx"	"
<g/>
přiženil	přiženit	k5eAaPmAgInS	přiženit
<g/>
"	"	kIx"	"
do	do	k7c2	do
Bratislavy	Bratislava	k1gFnSc2	Bratislava
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
ho	on	k3xPp3gInSc4	on
"	"	kIx"	"
<g/>
bez	bez	k1gInSc4	bez
boje	boj	k1gInSc2	boj
<g/>
"	"	kIx"	"
Slovan	Slovan	k1gMnSc1	Slovan
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ligová	ligový	k2eAgFnSc1d1	ligová
bilance	bilance	k1gFnSc1	bilance
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Ján	Ján	k1gMnSc1	Ján
Pivarník	Pivarník	k1gMnSc1	Pivarník
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Player	Player	k1gInSc1	Player
History	Histor	k1gInPc1	Histor
</s>
</p>
<p>
<s>
Worldfootball	Worldfootball	k1gInSc1	Worldfootball
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
</s>
</p>
