<s>
Dopravní	dopravní	k2eAgInSc1d1	dopravní
letoun	letoun	k1gInSc1	letoun
je	být	k5eAaImIp3nS	být
letadlo	letadlo	k1gNnSc4	letadlo
s	s	k7c7	s
pevnými	pevný	k2eAgFnPc7d1	pevná
nosnými	nosný	k2eAgFnPc7d1	nosná
plochami	plocha	k1gFnPc7	plocha
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc4	jehož
hlavní	hlavní	k2eAgFnPc4d1	hlavní
funkce	funkce	k1gFnPc4	funkce
je	být	k5eAaImIp3nS	být
přeprava	přeprava	k1gFnSc1	přeprava
platících	platící	k2eAgMnPc2d1	platící
pasažérů	pasažér	k1gMnPc2	pasažér
či	či	k8xC	či
nákladu	náklad	k1gInSc2	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Letouny	letoun	k1gInPc1	letoun
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
vlastněny	vlastněn	k2eAgInPc1d1	vlastněn
leteckou	letecký	k2eAgFnSc7d1	letecká
společností	společnost	k1gFnSc7	společnost
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
si	se	k3xPyFc3	se
je	on	k3xPp3gInPc4	on
společnost	společnost	k1gFnSc1	společnost
pronajímá	pronajímat	k5eAaImIp3nS	pronajímat
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
letecké	letecký	k2eAgFnSc6d1	letecká
dopravě	doprava	k1gFnSc6	doprava
je	být	k5eAaImIp3nS	být
kladen	klást	k5eAaImNgInS	klást
velký	velký	k2eAgInSc1d1	velký
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
ostatně	ostatně	k6eAd1	ostatně
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
jiné	jiný	k2eAgFnSc6d1	jiná
veřejné	veřejný	k2eAgFnSc6d1	veřejná
dopravě	doprava	k1gFnSc6	doprava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
první	první	k4xOgMnPc1	první
cestující	cestující	k1gMnPc1	cestující
přepraveni	přepraven	k2eAgMnPc1d1	přepraven
letouny	letoun	k1gInPc4	letoun
až	až	k8xS	až
jedenáct	jedenáct	k4xCc4	jedenáct
let	léto	k1gNnPc2	léto
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
letu	let	k1gInSc6	let
bratří	bratr	k1gMnPc2	bratr
Wrightů	Wright	k1gInPc2	Wright
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
jen	jen	k9	jen
o	o	k7c4	o
letouny	letoun	k1gInPc4	letoun
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
mezi	mezi	k7c4	mezi
letadla	letadlo	k1gNnPc4	letadlo
započteme	započíst	k5eAaPmIp1nP	započíst
i	i	k9	i
aerostaty	aerostat	k1gInPc1	aerostat
<g/>
,	,	kIx,	,
doprava	doprava	k1gFnSc1	doprava
cestujících	cestující	k1gMnPc2	cestující
probíhala	probíhat	k5eAaImAgFnS	probíhat
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
již	již	k6eAd1	již
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
takovým	takový	k3xDgInSc7	takový
strojem	stroj	k1gInSc7	stroj
byl	být	k5eAaImAgMnS	být
Ilja	Ilja	k1gMnSc1	Ilja
Muromec	Muromec	k1gMnSc1	Muromec
z	z	k7c2	z
konstrukční	konstrukční	k2eAgFnSc2d1	konstrukční
kanceláře	kancelář	k1gFnSc2	kancelář
Igora	Igor	k1gMnSc2	Igor
Sikorského	Sikorský	k2eAgMnSc2d1	Sikorský
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
První	první	k4xOgInSc1	první
let	let	k1gInSc1	let
s	s	k7c7	s
pasažéry	pasažér	k1gMnPc7	pasažér
uskutečnil	uskutečnit	k5eAaPmAgMnS	uskutečnit
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
</s>
<s>
Kapacita	kapacita	k1gFnSc1	kapacita
tohoto	tento	k3xDgInSc2	tento
letounu	letoun	k1gInSc2	letoun
byla	být	k5eAaImAgFnS	být
8	[number]	k4	8
až	až	k9	až
16	[number]	k4	16
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Československé	československý	k2eAgFnPc1d1	Československá
aerolinie	aerolinie	k1gFnPc1	aerolinie
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
let	let	k1gInSc4	let
s	s	k7c7	s
cestujícími	cestující	k1gMnPc7	cestující
teprve	teprve	k6eAd1	teprve
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1923	[number]	k4	1923
se	s	k7c7	s
strojem	stroj	k1gInSc7	stroj
Aero	aero	k1gNnSc1	aero
A-14	A-14	k1gFnSc2	A-14
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
nasadily	nasadit	k5eAaPmAgInP	nasadit
letoun	letoun	k1gInSc4	letoun
Aero	aero	k1gNnSc4	aero
A-	A-	k1gFnSc2	A-
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
mnohem	mnohem	k6eAd1	mnohem
pohodlnější	pohodlný	k2eAgInSc1d2	pohodlnější
s	s	k7c7	s
již	již	k6eAd1	již
krytou	krytý	k2eAgFnSc7d1	krytá
kajutou	kajuta	k1gFnSc7	kajuta
pro	pro	k7c4	pro
cestující	cestující	k1gMnPc4	cestující
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
se	se	k3xPyFc4	se
létalo	létat	k5eAaImAgNnS	létat
pouze	pouze	k6eAd1	pouze
dvouplošníky	dvouplošník	k1gInPc4	dvouplošník
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
celodřevěné	celodřevěný	k2eAgFnSc2d1	celodřevěná
konstrukce	konstrukce	k1gFnSc2	konstrukce
s	s	k7c7	s
plátěným	plátěný	k2eAgInSc7d1	plátěný
potahem	potah	k1gInSc7	potah
<g/>
.	.	kIx.	.
</s>
<s>
Lety	let	k1gInPc1	let
byly	být	k5eAaImAgInP	být
možné	možný	k2eAgMnPc4d1	možný
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
příznivých	příznivý	k2eAgFnPc2d1	příznivá
povětrnostních	povětrnostní	k2eAgFnPc2d1	povětrnostní
podmínek	podmínka	k1gFnPc2	podmínka
a	a	k8xC	a
viditelnosti	viditelnost	k1gFnSc2	viditelnost
<g/>
.	.	kIx.	.
</s>
<s>
Pokrokovými	pokrokový	k2eAgInPc7d1	pokrokový
letouny	letoun	k1gInPc7	letoun
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
byly	být	k5eAaImAgInP	být
americké	americký	k2eAgInPc1d1	americký
stroje	stroj	k1gInPc1	stroj
Ford	ford	k1gInSc1	ford
Trimotor	Trimotor	k1gInSc1	Trimotor
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
již	již	k9	již
o	o	k7c4	o
celokovové	celokovový	k2eAgInPc4d1	celokovový
hornoplošníky	hornoplošník	k1gInPc4	hornoplošník
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
hvězdicovými	hvězdicový	k2eAgInPc7d1	hvězdicový
motory	motor	k1gInPc7	motor
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
8	[number]	k4	8
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
jich	on	k3xPp3gMnPc2	on
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
celkem	celek	k1gInSc7	celek
199	[number]	k4	199
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
pouze	pouze	k6eAd1	pouze
jediný	jediný	k2eAgMnSc1d1	jediný
létal	létat	k5eAaImAgInS	létat
mimo	mimo	k7c4	mimo
Ameriku	Amerika	k1gFnSc4	Amerika
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
ČSA	ČSA	kA	ČSA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
na	na	k7c4	na
trh	trh	k1gInSc4	trh
dvoumotorový	dvoumotorový	k2eAgInSc1d1	dvoumotorový
dolnoplošník	dolnoplošník	k1gInSc1	dolnoplošník
Douglas	Douglasa	k1gFnPc2	Douglasa
DC-	DC-	k1gMnPc2	DC-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Výkonové	výkonový	k2eAgInPc1d1	výkonový
parametry	parametr	k1gInPc1	parametr
byly	být	k5eAaImAgInP	být
vynikající	vynikající	k2eAgInPc4d1	vynikající
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
provozavatelé	provozavatel	k1gMnPc1	provozavatel
požadovali	požadovat	k5eAaImAgMnP	požadovat
vyšší	vysoký	k2eAgFnSc4d2	vyšší
kapacitu	kapacita	k1gFnSc4	kapacita
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
původních	původní	k2eAgNnPc2d1	původní
14	[number]	k4	14
míst	místo	k1gNnPc2	místo
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
na	na	k7c4	na
32	[number]	k4	32
a	a	k8xC	a
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
tím	ten	k3xDgNnSc7	ten
nejúspěšnější	úspěšný	k2eAgNnSc1d3	nejúspěšnější
letadlo	letadlo	k1gNnSc1	letadlo
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
:	:	kIx,	:
Douglas	Douglas	k1gInSc1	Douglas
DC-	DC-	k1gFnSc2	DC-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
známé	známý	k2eAgNnSc1d1	známé
též	též	k9	též
jako	jako	k8xS	jako
Dakota	Dakota	k1gFnSc1	Dakota
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
a	a	k8xC	a
čtyřicátých	čtyřicátý	k4xOgNnPc6	čtyřicátý
letech	léto	k1gNnPc6	léto
jich	on	k3xPp3gFnPc2	on
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
USA	USA	kA	USA
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
10692	[number]	k4	10692
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
asi	asi	k9	asi
2000	[number]	k4	2000
v	v	k7c6	v
licenci	licence	k1gFnSc6	licence
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
jako	jako	k8xS	jako
Lisunov	Lisunov	k1gInSc1	Lisunov
Li-	Li-	k1gFnSc2	Li-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Létaly	létat	k5eAaImAgFnP	létat
prakticky	prakticky	k6eAd1	prakticky
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
provoz	provoz	k1gInSc1	provoz
byl	být	k5eAaImAgInS	být
ukončen	ukončit	k5eAaPmNgInS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
asi	asi	k9	asi
400	[number]	k4	400
kusů	kus	k1gInPc2	kus
je	být	k5eAaImIp3nS	být
nasazováno	nasazovat	k5eAaImNgNnS	nasazovat
na	na	k7c4	na
mimořádné	mimořádný	k2eAgInPc4d1	mimořádný
lety	let	k1gInPc4	let
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
mezníků	mezník	k1gInPc2	mezník
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
letectví	letectví	k1gNnSc2	letectví
bylo	být	k5eAaImAgNnS	být
použití	použití	k1gNnSc4	použití
dvouproudových	dvouproudový	k2eAgInPc2d1	dvouproudový
motorů	motor	k1gInPc2	motor
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
letounem	letoun	k1gInSc7	letoun
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
pohonem	pohon	k1gInSc7	pohon
byl	být	k5eAaImAgInS	být
britský	britský	k2eAgInSc1d1	britský
de	de	k?	de
Havilland	Havilland	k1gInSc1	Havilland
Comet	Comet	k1gInSc1	Comet
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
let	let	k1gInSc1	let
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
s	s	k7c7	s
cestujícími	cestující	k1gMnPc7	cestující
až	až	k9	až
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgInS	být
ovšem	ovšem	k9	ovšem
po	po	k7c6	po
sérii	série	k1gFnSc6	série
tragických	tragický	k2eAgFnPc2d1	tragická
nehod	nehoda	k1gFnPc2	nehoda
<g/>
,	,	kIx,	,
způsobených	způsobený	k2eAgInPc2d1	způsobený
únavou	únava	k1gFnSc7	únava
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
stažen	stažen	k2eAgMnSc1d1	stažen
z	z	k7c2	z
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
vyřazen	vyřadit	k5eAaPmNgMnS	vyřadit
z	z	k7c2	z
civilního	civilní	k2eAgInSc2d1	civilní
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
fungoval	fungovat	k5eAaImAgInS	fungovat
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
armádní	armádní	k2eAgInSc1d1	armádní
transportní	transportní	k2eAgInSc1d1	transportní
letoun	letoun	k1gInSc1	letoun
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
poté	poté	k6eAd1	poté
následovali	následovat	k5eAaImAgMnP	následovat
další	další	k2eAgMnPc1d1	další
letečtí	letecký	k2eAgMnPc1d1	letecký
výrobci	výrobce	k1gMnPc1	výrobce
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
již	již	k6eAd1	již
byli	být	k5eAaImAgMnP	být
úspěšnější	úspěšný	k2eAgMnPc1d2	úspěšnější
<g/>
.	.	kIx.	.
</s>
<s>
Ruský	ruský	k2eAgMnSc1d1	ruský
Tupolev	Tupolev	k1gMnSc1	Tupolev
Tu-	Tu-	k1gMnSc1	Tu-
<g/>
104	[number]	k4	104
létal	létat	k5eAaImAgMnS	létat
v	v	k7c6	v
pravidelném	pravidelný	k2eAgInSc6d1	pravidelný
provozu	provoz	k1gInSc6	provoz
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1956	[number]	k4	1956
a	a	k8xC	a
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
jej	on	k3xPp3gInSc4	on
využívaly	využívat	k5eAaPmAgFnP	využívat
ČSA	ČSA	kA	ČSA
jakožto	jakožto	k8xS	jakožto
první	první	k4xOgFnSc4	první
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Vlajkovou	vlajkový	k2eAgFnSc7d1	vlajková
lodí	loď	k1gFnSc7	loď
západního	západní	k2eAgInSc2d1	západní
bloku	blok	k1gInSc2	blok
byl	být	k5eAaImAgInS	být
americký	americký	k2eAgInSc1d1	americký
Boeing	boeing	k1gInSc1	boeing
707	[number]	k4	707
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
cestujícími	cestující	k1gMnPc7	cestující
poprvé	poprvé	k6eAd1	poprvé
vzlétl	vzlétnout	k5eAaPmAgMnS	vzlétnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
a	a	k8xC	a
dosloužil	dosloužit	k5eAaPmAgMnS	dosloužit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
Boeing	boeing	k1gInSc1	boeing
720	[number]	k4	720
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
nastal	nastat	k5eAaPmAgInS	nastat
konkurenční	konkurenční	k2eAgInSc1d1	konkurenční
boj	boj	k1gInSc1	boj
mezi	mezi	k7c7	mezi
výrobci	výrobce	k1gMnPc7	výrobce
a	a	k8xC	a
boom	boom	k1gInSc4	boom
nových	nový	k2eAgInPc2d1	nový
typů	typ	k1gInPc2	typ
letadel	letadlo	k1gNnPc2	letadlo
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
se	s	k7c7	s
zvyšující	zvyšující	k2eAgFnSc7d1	zvyšující
kapacitou	kapacita	k1gFnSc7	kapacita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
SSSR	SSSR	kA	SSSR
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
Tupolev	Tupolev	k1gFnPc1	Tupolev
Tu-	Tu-	k1gFnSc1	Tu-
<g/>
154	[number]	k4	154
<g/>
,	,	kIx,	,
Iljušin	iljušin	k1gInSc1	iljušin
Il-	Il-	k1gFnSc1	Il-
<g/>
62	[number]	k4	62
<g/>
,	,	kIx,	,
v	v	k7c6	v
USA	USA	kA	USA
pak	pak	k9	pak
Boeing	boeing	k1gInSc4	boeing
727	[number]	k4	727
<g/>
,	,	kIx,	,
McDonnell	McDonnell	k1gMnSc1	McDonnell
Douglas	Douglas	k1gMnSc1	Douglas
DC-	DC-	k1gMnSc1	DC-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
největším	veliký	k2eAgInSc7d3	veliký
dopravním	dopravní	k2eAgInSc7d1	dopravní
letounem	letoun	k1gInSc7	letoun
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
Boeingem	boeing	k1gInSc7	boeing
747	[number]	k4	747
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
až	až	k9	až
524	[number]	k4	524
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
revoluční	revoluční	k2eAgFnSc7d1	revoluční
myšlenkou	myšlenka	k1gFnSc7	myšlenka
byl	být	k5eAaImAgInS	být
nadzvukový	nadzvukový	k2eAgInSc1d1	nadzvukový
dopravní	dopravní	k2eAgInSc1d1	dopravní
letoun	letoun	k1gInSc1	letoun
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
desetiletí	desetiletí	k1gNnSc6	desetiletí
vývoje	vývoj	k1gInSc2	vývoj
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
představil	představit	k5eAaPmAgInS	představit
hmatatelný	hmatatelný	k2eAgInSc1d1	hmatatelný
výsledek	výsledek	k1gInSc1	výsledek
ruský	ruský	k2eAgMnSc1d1	ruský
Tupolev	Tupolev	k1gMnSc1	Tupolev
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
strojem	stroj	k1gInSc7	stroj
Tupolev	Tupolev	k1gFnSc1	Tupolev
Tu-	Tu-	k1gFnSc1	Tu-
<g/>
144	[number]	k4	144
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1968	[number]	k4	1968
(	(	kIx(	(
<g/>
s	s	k7c7	s
cestujícími	cestující	k1gMnPc7	cestující
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
jej	on	k3xPp3gNnSc4	on
následoval	následovat	k5eAaImAgInS	následovat
britsko-francouzský	britskorancouzský	k2eAgInSc1d1	britsko-francouzský
projekt	projekt	k1gInSc1	projekt
Concorde	Concord	k1gInSc5	Concord
(	(	kIx(	(
<g/>
první	první	k4xOgInSc1	první
let	let	k1gInSc1	let
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
s	s	k7c7	s
cestujícími	cestující	k2eAgNnPc7d1	cestující
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stroje	stroj	k1gInPc1	stroj
byly	být	k5eAaImAgInP	být
natolik	natolik	k6eAd1	natolik
podobné	podobný	k2eAgInPc1d1	podobný
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
ruští	ruský	k2eAgMnPc1d1	ruský
konstruktéři	konstruktér	k1gMnPc1	konstruktér
podezíráni	podezírat	k5eAaImNgMnP	podezírat
ze	z	k7c2	z
špionáže	špionáž	k1gFnSc2	špionáž
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
o	o	k7c6	o
podobnosti	podobnost	k1gFnSc6	podobnost
úspěchů	úspěch	k1gInPc2	úspěch
nelze	lze	k6eNd1	lze
hovořit	hovořit	k5eAaImF	hovořit
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Tu-	Tu-	k1gMnSc1	Tu-
<g/>
144	[number]	k4	144
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
haváriích	havárie	k1gFnPc6	havárie
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
k	k	k7c3	k
vyřazení	vyřazení	k1gNnSc3	vyřazení
<g/>
,	,	kIx,	,
Concorde	Concord	k1gInSc5	Concord
vykazoval	vykazovat	k5eAaImAgInS	vykazovat
zisky	zisk	k1gInPc4	zisk
i	i	k8xC	i
přes	přes	k7c4	přes
velmi	velmi	k6eAd1	velmi
značnou	značný	k2eAgFnSc4d1	značná
spotřebu	spotřeba	k1gFnSc4	spotřeba
paliva	palivo	k1gNnSc2	palivo
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
související	související	k2eAgFnPc1d1	související
cenou	cena	k1gFnSc7	cena
letenky	letenka	k1gFnSc2	letenka
(	(	kIx(	(
<g/>
až	až	k9	až
řádově	řádově	k6eAd1	řádově
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
běžným	běžný	k2eAgNnSc7d1	běžné
letadlem	letadlo	k1gNnSc7	letadlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nehodě	nehoda	k1gFnSc6	nehoda
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
ovšem	ovšem	k9	ovšem
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
nadzvukové	nadzvukový	k2eAgInPc4d1	nadzvukový
lety	let	k1gInPc4	let
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
jejich	jejich	k3xOp3gMnPc4	jejich
provozovatele	provozovatel	k1gMnPc4	provozovatel
<g/>
,	,	kIx,	,
Air	Air	k1gMnPc4	Air
France	Franc	k1gMnSc4	Franc
a	a	k8xC	a
British	British	k1gInSc4	British
Airways	Airwaysa	k1gFnPc2	Airwaysa
k	k	k7c3	k
ukončení	ukončení	k1gNnSc3	ukončení
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
let	let	k1gInSc1	let
s	s	k7c7	s
cestujícími	cestující	k1gMnPc7	cestující
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ovládání	ovládání	k1gNnSc2	ovládání
pohyblivých	pohyblivý	k2eAgFnPc2d1	pohyblivá
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
detekce	detekce	k1gFnSc1	detekce
poškození	poškození	k1gNnSc1	poškození
a	a	k8xC	a
navigace	navigace	k1gFnSc1	navigace
čím	čí	k3xOyRgNnSc7	čí
dál	daleko	k6eAd2	daleko
tím	ten	k3xDgNnSc7	ten
více	hodně	k6eAd2	hodně
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
elektronika	elektronika	k1gFnSc1	elektronika
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
velikost	velikost	k1gFnSc4	velikost
letounů	letoun	k1gInPc2	letoun
nebyl	být	k5eNaImAgMnS	být
zaznamenám	zaznamenat	k5eAaPmIp1nS	zaznamenat
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
třicet	třicet	k4xCc4	třicet
let	léto	k1gNnPc2	léto
výraznější	výrazný	k2eAgInSc1d2	výraznější
posun	posun	k1gInSc1	posun
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
velkokapacitní	velkokapacitní	k2eAgInSc1d1	velkokapacitní
letoun	letoun	k1gInSc1	letoun
Airbus	airbus	k1gInSc4	airbus
A	a	k9	a
<g/>
380	[number]	k4	380
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
po	po	k7c6	po
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
36	[number]	k4	36
letech	léto	k1gNnPc6	léto
překonal	překonat	k5eAaPmAgMnS	překonat
ve	v	k7c4	v
velikosti	velikost	k1gFnPc4	velikost
Boeing	boeing	k1gInSc4	boeing
747	[number]	k4	747
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
patrech	patro	k1gNnPc6	patro
pojme	pojmout	k5eAaPmIp3nS	pojmout
až	až	k9	až
853	[number]	k4	853
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
rozměry	rozměr	k1gInPc1	rozměr
(	(	kIx(	(
<g/>
73	[number]	k4	73
<g/>
×	×	k?	×
<g/>
80	[number]	k4	80
<g/>
m	m	kA	m
<g/>
)	)	kIx)	)
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
překážkou	překážka	k1gFnSc7	překážka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zdaleka	zdaleka	k6eAd1	zdaleka
ne	ne	k9	ne
každé	každý	k3xTgNnSc1	každý
letiště	letiště	k1gNnSc1	letiště
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
takové	takový	k3xDgMnPc4	takový
kolosy	kolos	k1gMnPc4	kolos
dimenzováno	dimenzován	k2eAgNnSc1d1	dimenzováno
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
budoucnosti	budoucnost	k1gFnSc2	budoucnost
se	se	k3xPyFc4	se
jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
ambiciózní	ambiciózní	k2eAgFnSc7d1	ambiciózní
jeví	jevit	k5eAaImIp3nS	jevit
evropský	evropský	k2eAgInSc1d1	evropský
projekt	projekt	k1gInSc1	projekt
Reaction	Reaction	k1gInSc1	Reaction
Engines	Engines	k1gInSc4	Engines
A	a	k9	a
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Znamenalo	znamenat	k5eAaImAgNnS	znamenat
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
obnovení	obnovení	k1gNnSc1	obnovení
nadzvukového	nadzvukový	k2eAgNnSc2d1	nadzvukové
létání	létání	k1gNnSc2	létání
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
mnohem	mnohem	k6eAd1	mnohem
rychlejšího	rychlý	k2eAgNnSc2d2	rychlejší
(	(	kIx(	(
<g/>
až	až	k9	až
5	[number]	k4	5
M	M	kA	M
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
bude	být	k5eAaImBp3nS	být
použita	použít	k5eAaPmNgFnS	použít
zcela	zcela	k6eAd1	zcela
nová	nový	k2eAgFnSc1d1	nová
koncepce	koncepce	k1gFnSc1	koncepce
hybridního	hybridní	k2eAgInSc2d1	hybridní
motoru	motor	k1gInSc2	motor
-	-	kIx~	-
SABRE	SABRE	kA	SABRE
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bude	být	k5eAaImBp3nS	být
kombinace	kombinace	k1gFnSc1	kombinace
klasického	klasický	k2eAgInSc2d1	klasický
proudového	proudový	k2eAgInSc2d1	proudový
<g/>
,	,	kIx,	,
náporového	náporový	k2eAgInSc2d1	náporový
a	a	k8xC	a
raketového	raketový	k2eAgInSc2d1	raketový
motoru	motor	k1gInSc2	motor
<g/>
.	.	kIx.	.
</s>
<s>
Ceny	cena	k1gFnPc1	cena
letenek	letenka	k1gFnPc2	letenka
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
současných	současný	k2eAgInPc2d1	současný
předpokladů	předpoklad	k1gInPc2	předpoklad
neměly	mít	k5eNaImAgInP	mít
lišit	lišit	k5eAaImF	lišit
od	od	k7c2	od
konvenčních	konvenční	k2eAgInPc2d1	konvenční
letů	let	k1gInPc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
délky	délka	k1gFnSc2	délka
doletu	dolet	k1gInSc2	dolet
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
<g/>
:	:	kIx,	:
Letadla	letadlo	k1gNnPc4	letadlo
na	na	k7c4	na
krátké	krátký	k2eAgFnPc4d1	krátká
tratě	trať	k1gFnPc4	trať
s	s	k7c7	s
doletem	dolet	k1gInSc7	dolet
do	do	k7c2	do
cca	cca	kA	cca
1000	[number]	k4	1000
km	km	kA	km
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
označované	označovaný	k2eAgFnSc6d1	označovaná
jako	jako	k8xS	jako
regionální	regionální	k2eAgFnSc6d1	regionální
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
pohonná	pohonný	k2eAgFnSc1d1	pohonná
jednotka	jednotka	k1gFnSc1	jednotka
slouží	sloužit	k5eAaImIp3nS	sloužit
obvykle	obvykle	k6eAd1	obvykle
turbovrtulový	turbovrtulový	k2eAgInSc1d1	turbovrtulový
motor	motor	k1gInSc1	motor
umístěný	umístěný	k2eAgInSc1d1	umístěný
v	v	k7c6	v
gondolách	gondola	k1gFnPc6	gondola
na	na	k7c6	na
křídlech	křídlo	k1gNnPc6	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
ČSA	ČSA	kA	ČSA
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
kategorii	kategorie	k1gFnSc6	kategorie
používá	používat	k5eAaImIp3nS	používat
letadla	letadlo	k1gNnSc2	letadlo
ATR	ATR	kA	ATR
42	[number]	k4	42
a	a	k8xC	a
ATR	ATR	kA	ATR
72	[number]	k4	72
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
typy	typ	k1gInPc7	typ
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
Bombardier	Bombardier	k1gInSc4	Bombardier
Dash	Dash	k1gInSc1	Dash
8	[number]	k4	8
<g/>
,	,	kIx,	,
Fokker	Fokker	k1gInSc1	Fokker
50	[number]	k4	50
<g/>
,	,	kIx,	,
Saab	Saab	k1gInSc1	Saab
340	[number]	k4	340
nebo	nebo	k8xC	nebo
Saab	Saab	k1gInSc1	Saab
2000	[number]	k4	2000
Letadla	letadlo	k1gNnPc1	letadlo
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
tratě	trata	k1gFnSc6	trata
s	s	k7c7	s
doletem	dolet	k1gInSc7	dolet
1000	[number]	k4	1000
až	až	k8xS	až
3000	[number]	k4	3000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
letadla	letadlo	k1gNnPc1	letadlo
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
především	především	k9	především
na	na	k7c6	na
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
linkách	linka	k1gFnPc6	linka
<g/>
.	.	kIx.	.
</s>
<s>
Očekáváme	očekávat	k5eAaImIp1nP	očekávat
zde	zde	k6eAd1	zde
vyšší	vysoký	k2eAgInSc4d2	vyšší
komfort	komfort	k1gInSc4	komfort
a	a	k8xC	a
úroveň	úroveň	k1gFnSc4	úroveň
servisu	servis	k1gInSc2	servis
během	během	k7c2	během
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Kapacita	kapacita	k1gFnSc1	kapacita
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
cca	cca	kA	cca
40	[number]	k4	40
do	do	k7c2	do
160	[number]	k4	160
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Pohonnou	pohonný	k2eAgFnSc4d1	pohonná
jednotku	jednotka	k1gFnSc4	jednotka
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
obvykle	obvykle	k6eAd1	obvykle
představuje	představovat	k5eAaImIp3nS	představovat
dvojice	dvojice	k1gFnSc1	dvojice
proudových	proudový	k2eAgInPc2d1	proudový
motorů	motor	k1gInPc2	motor
<g/>
.	.	kIx.	.
</s>
<s>
Letadla	letadlo	k1gNnPc1	letadlo
na	na	k7c4	na
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
tratě	trať	k1gFnPc4	trať
s	s	k7c7	s
doletem	dolet	k1gInSc7	dolet
nad	nad	k7c4	nad
3000	[number]	k4	3000
km	km	kA	km
se	se	k3xPyFc4	se
nasazují	nasazovat	k5eAaImIp3nP	nasazovat
na	na	k7c4	na
transkontinentální	transkontinentální	k2eAgInPc4d1	transkontinentální
lety	let	k1gInPc4	let
<g/>
.	.	kIx.	.
</s>
<s>
Sem	sem	k6eAd1	sem
spadají	spadat	k5eAaPmIp3nP	spadat
největší	veliký	k2eAgNnPc1d3	veliký
a	a	k8xC	a
nejpohodlnější	pohodlný	k2eAgNnPc1d3	nejpohodlnější
letadla	letadlo	k1gNnPc1	letadlo
současnosti	současnost	k1gFnSc2	současnost
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
až	až	k9	až
880	[number]	k4	880
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
Airbus	airbus	k1gInSc4	airbus
A	a	k9	a
<g/>
340	[number]	k4	340
<g/>
,	,	kIx,	,
Airbus	airbus	k1gInSc1	airbus
A	A	kA	A
<g/>
350	[number]	k4	350
<g/>
,	,	kIx,	,
Airbus	airbus	k1gInSc1	airbus
A	A	kA	A
<g/>
380	[number]	k4	380
<g/>
,	,	kIx,	,
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
</s>
<s>
Pohon	pohon	k1gInSc4	pohon
většinou	většinou	k6eAd1	většinou
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
dva	dva	k4xCgInPc1	dva
až	až	k9	až
čtyři	čtyři	k4xCgInPc1	čtyři
proudové	proudový	k2eAgInPc1d1	proudový
motory	motor	k1gInPc1	motor
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
největších	veliký	k2eAgNnPc2d3	veliký
letadel	letadlo	k1gNnPc2	letadlo
je	být	k5eAaImIp3nS	být
nemožnost	nemožnost	k1gFnSc1	nemožnost
využít	využít	k5eAaPmF	využít
většinu	většina	k1gFnSc4	většina
letišť	letiště	k1gNnPc2	letiště
(	(	kIx(	(
<g/>
nejsou	být	k5eNaImIp3nP	být
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
rozměrově	rozměrově	k6eAd1	rozměrově
dimenzována	dimenzován	k2eAgFnSc1d1	dimenzována
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
(	(	kIx(	(
<g/>
kapacity	kapacita	k1gFnPc1	kapacita
<g/>
)	)	kIx)	)
aerotaxi	aerotaxi	k1gNnSc1	aerotaxi
<g/>
:	:	kIx,	:
3	[number]	k4	3
až	až	k9	až
10	[number]	k4	10
cestujících	cestující	k1gMnPc2	cestující
malá	malý	k2eAgNnPc1d1	malé
dopravní	dopravní	k2eAgNnPc1d1	dopravní
letadla	letadlo	k1gNnPc1	letadlo
<g/>
:	:	kIx,	:
10	[number]	k4	10
až	až	k9	až
30	[number]	k4	30
cestujících	cestující	k1gMnPc2	cestující
střední	střední	k2eAgFnSc2d1	střední
dopravní	dopravní	k2eAgNnPc1d1	dopravní
letadla	letadlo	k1gNnPc1	letadlo
<g/>
:	:	kIx,	:
30	[number]	k4	30
až	až	k9	až
100	[number]	k4	100
cestujících	cestující	k1gMnPc2	cestující
velká	velký	k2eAgNnPc1d1	velké
dopravní	dopravní	k2eAgNnPc1d1	dopravní
letadla	letadlo	k1gNnPc1	letadlo
<g/>
:	:	kIx,	:
100	[number]	k4	100
až	až	k9	až
200	[number]	k4	200
cestujících	cestující	k1gMnPc2	cestující
velkokapacitní	velkokapacitní	k2eAgInPc4d1	velkokapacitní
aerobusy	aerobus	k1gInPc4	aerobus
<g/>
:	:	kIx,	:
nad	nad	k7c4	nad
200	[number]	k4	200
cestujících	cestující	k1gMnPc2	cestující
Podle	podle	k7c2	podle
uspořádání	uspořádání	k1gNnSc2	uspořádání
<g />
.	.	kIx.	.
</s>
<s>
sedadel	sedadlo	k1gNnPc2	sedadlo
úzký	úzký	k2eAgInSc1d1	úzký
trup	trup	k1gInSc1	trup
(	(	kIx(	(
<g/>
narrow-body	narrowoda	k1gFnPc1	narrow-boda
<g/>
)	)	kIx)	)
s	s	k7c7	s
1	[number]	k4	1
uličkou	ulička	k1gFnSc7	ulička
<g/>
,	,	kIx,	,
2-2	[number]	k4	2-2
nebo	nebo	k8xC	nebo
3-3	[number]	k4	3-3
sedadla	sedadlo	k1gNnSc2	sedadlo
vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
široký	široký	k2eAgInSc1d1	široký
trup	trup	k1gInSc1	trup
(	(	kIx(	(
<g/>
wide-body	wideoda	k1gFnPc1	wide-boda
<g/>
)	)	kIx)	)
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
uličkami	ulička	k1gFnPc7	ulička
a	a	k8xC	a
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
3-3-3	[number]	k4	3-3-3
nebo	nebo	k8xC	nebo
3-4-3	[number]	k4	3-4-3
sedadly	sedadlo	k1gNnPc7	sedadlo
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
motorů	motor	k1gInPc2	motor
vrtulová	vrtulový	k2eAgNnPc4d1	vrtulové
(	(	kIx(	(
<g/>
pístová	pístový	k2eAgNnPc4d1	pístové
<g/>
)	)	kIx)	)
letadla	letadlo	k1gNnPc4	letadlo
turbovrtulová	turbovrtulový	k2eAgNnPc4d1	turbovrtulové
letadla	letadlo	k1gNnPc4	letadlo
proudová	proudový	k2eAgNnPc4d1	proudové
letadla	letadlo	k1gNnPc4	letadlo
Další	další	k2eAgNnPc4d1	další
dělení	dělení	k1gNnSc4	dělení
poloha	poloha	k1gFnSc1	poloha
nosných	nosný	k2eAgFnPc2d1	nosná
ploch	plocha	k1gFnPc2	plocha
(	(	kIx(	(
<g/>
dolnoplošníky	dolnoplošník	k1gInPc1	dolnoplošník
<g/>
,	,	kIx,	,
středoplošníky	středoplošník	k1gInPc1	středoplošník
<g/>
,	,	kIx,	,
hornoplošníky	hornoplošník	k1gInPc1	hornoplošník
<g/>
)	)	kIx)	)
počet	počet	k1gInSc1	počet
motorů	motor	k1gInPc2	motor
umístění	umístění	k1gNnSc2	umístění
motorů	motor	k1gInPc2	motor
lak	laka	k1gFnPc2	laka
(	(	kIx(	(
<g/>
členství	členství	k1gNnSc2	členství
v	v	k7c6	v
nějaké	nějaký	k3yIgFnSc6	nějaký
organizaci	organizace	k1gFnSc6	organizace
-	-	kIx~	-
Oneworld	Oneworld	k1gInSc1	Oneworld
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
použité	použitý	k2eAgFnPc4d1	použitá
<g />
.	.	kIx.	.
</s>
<s>
materiály	materiál	k1gInPc4	materiál
(	(	kIx(	(
<g/>
celokovová	celokovový	k2eAgNnPc4d1	celokovové
<g/>
,	,	kIx,	,
kompozitní	kompozitní	k2eAgNnPc4d1	kompozitní
<g/>
,	,	kIx,	,
smíšená	smíšený	k2eAgNnPc4d1	smíšené
...	...	k?	...
<g/>
)	)	kIx)	)
tvar	tvar	k1gInSc4	tvar
křídel	křídlo	k1gNnPc2	křídlo
(	(	kIx(	(
<g/>
přímá	přímý	k2eAgNnPc4d1	přímé
<g/>
,	,	kIx,	,
šípová	šípový	k2eAgNnPc4d1	šípové
<g/>
,	,	kIx,	,
lichoběžníková	lichoběžníkový	k2eAgNnPc4d1	lichoběžníkové
<g/>
)	)	kIx)	)
typ	typ	k1gInSc4	typ
podvozku	podvozek	k1gInSc2	podvozek
(	(	kIx(	(
<g/>
příďové	příďový	k2eAgFnSc2d1	příďová
<g/>
,	,	kIx,	,
tandemové	tandemový	k2eAgFnSc2d1	tandemová
<g/>
)	)	kIx)	)
Vnější	vnější	k2eAgFnSc2d1	vnější
části	část	k1gFnSc2	část
konstrukce	konstrukce	k1gFnSc2	konstrukce
letounu	letoun	k1gInSc2	letoun
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
motorů	motor	k1gInPc2	motor
<g/>
)	)	kIx)	)
nazýváme	nazývat	k5eAaImIp1nP	nazývat
souhrnně	souhrnně	k6eAd1	souhrnně
drak	drak	k1gInSc4	drak
letadla	letadlo	k1gNnSc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
základní	základní	k2eAgFnPc1d1	základní
části	část	k1gFnPc1	část
jsou	být	k5eAaImIp3nP	být
trup	trup	k1gInSc4	trup
<g/>
,	,	kIx,	,
křídla	křídlo	k1gNnPc4	křídlo
<g/>
,	,	kIx,	,
podvozek	podvozek	k1gInSc1	podvozek
<g/>
,	,	kIx,	,
vodorovné	vodorovný	k2eAgFnPc1d1	vodorovná
a	a	k8xC	a
svislé	svislý	k2eAgFnPc1d1	svislá
ocasní	ocasní	k2eAgFnPc1d1	ocasní
plochy	plocha	k1gFnPc1	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
draku	drak	k1gInSc2	drak
je	být	k5eAaImIp3nS	být
optimalizován	optimalizovat	k5eAaBmNgInS	optimalizovat
na	na	k7c4	na
aerodynamické	aerodynamický	k2eAgFnPc4d1	aerodynamická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
při	při	k7c6	při
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
rychlosti	rychlost	k1gFnSc6	rychlost
letu	let	k1gInSc2	let
800	[number]	k4	800
-	-	kIx~	-
1000	[number]	k4	1000
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Trup	trup	k1gInSc1	trup
tvoří	tvořit	k5eAaImIp3nS	tvořit
centrální	centrální	k2eAgFnSc4d1	centrální
část	část	k1gFnSc4	část
draku	drak	k1gInSc2	drak
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
umístěny	umístit	k5eAaPmNgInP	umístit
veškeré	veškerý	k3xTgInPc1	veškerý
prostory	prostor	k1gInPc1	prostor
a	a	k8xC	a
vybavení	vybavení	k1gNnSc1	vybavení
pro	pro	k7c4	pro
cestující	cestující	k1gMnPc4	cestující
a	a	k8xC	a
posádku	posádka	k1gFnSc4	posádka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
nákladový	nákladový	k2eAgInSc4d1	nákladový
a	a	k8xC	a
zavazadlový	zavazadlový	k2eAgInSc4d1	zavazadlový
prostor	prostor	k1gInSc4	prostor
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
palivové	palivový	k2eAgFnPc1d1	palivová
nádrže	nádrž	k1gFnPc1	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgFnSc1d1	přední
část	část	k1gFnSc1	část
trupu	trup	k1gInSc2	trup
<g/>
,	,	kIx,	,
před	před	k7c7	před
kabinou	kabina	k1gFnSc7	kabina
pilota	pilota	k1gFnSc1	pilota
(	(	kIx(	(
<g/>
kokpitem	kokpit	k1gInSc7	kokpit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nazýváme	nazývat	k5eAaImIp1nP	nazývat
přídí	příď	k1gFnSc7	příď
<g/>
,	,	kIx,	,
zadní	zadní	k2eAgInSc1d1	zadní
pak	pak	k6eAd1	pak
zádí	zádí	k1gNnSc4	zádí
(	(	kIx(	(
<g/>
nese	nést	k5eAaImIp3nS	nést
ocasní	ocasní	k2eAgFnPc4d1	ocasní
plochy	plocha	k1gFnPc4	plocha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prostorovou	prostorový	k2eAgFnSc4d1	prostorová
tuhost	tuhost	k1gFnSc4	tuhost
trupu	trup	k1gInSc2	trup
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
podélné	podélný	k2eAgFnPc4d1	podélná
výztuhy	výztuha	k1gFnPc4	výztuha
a	a	k8xC	a
kolmo	kolmo	k6eAd1	kolmo
na	na	k7c4	na
ně	on	k3xPp3gFnPc4	on
trupové	trupový	k2eAgFnPc4d1	trupová
přepážky	přepážka	k1gFnPc4	přepážka
<g/>
.	.	kIx.	.
</s>
<s>
Trupové	trupový	k2eAgFnPc1d1	trupová
přepážky	přepážka	k1gFnPc1	přepážka
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
většího	veliký	k2eAgNnSc2d2	veliký
namáhání	namáhání	k1gNnSc2	namáhání
(	(	kIx(	(
<g/>
závěsy	závěsa	k1gFnPc1	závěsa
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
podvozek	podvozek	k1gInSc1	podvozek
<g/>
,	,	kIx,	,
ocasní	ocasní	k2eAgFnPc1d1	ocasní
plochy	plocha	k1gFnPc1	plocha
<g/>
)	)	kIx)	)
zesílené	zesílený	k2eAgFnPc1d1	zesílená
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
zpravidla	zpravidla	k6eAd1	zpravidla
prstencový	prstencový	k2eAgInSc4d1	prstencový
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
však	však	k9	však
být	být	k5eAaImF	být
i	i	k9	i
plnostěnné	plnostěnný	k2eAgInPc1d1	plnostěnný
(	(	kIx(	(
<g/>
např	např	kA	např
v	v	k7c6	v
zádi	záď	k1gFnSc6	záď
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
se	se	k3xPyFc4	se
vkládají	vkládat	k5eAaImIp3nP	vkládat
izolační	izolační	k2eAgFnPc1d1	izolační
desky	deska	k1gFnPc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
trupu	trup	k1gInSc2	trup
tvoří	tvořit	k5eAaImIp3nS	tvořit
tzv.	tzv.	kA	tzv.
potah	potah	k1gInSc1	potah
z	z	k7c2	z
lehkých	lehký	k2eAgFnPc2d1	lehká
kovových	kovový	k2eAgFnPc2d1	kovová
slitin	slitina	k1gFnPc2	slitina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
tloušťka	tloušťka	k1gFnSc1	tloušťka
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
3	[number]	k4	3
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Křídla	křídlo	k1gNnPc1	křídlo
představují	představovat	k5eAaImIp3nP	představovat
dominantní	dominantní	k2eAgFnSc4d1	dominantní
složku	složka	k1gFnSc4	složka
nosných	nosný	k2eAgFnPc2d1	nosná
ploch	plocha	k1gFnPc2	plocha
letounu	letoun	k1gInSc2	letoun
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgFnPc4d3	nejdůležitější
charakteristiky	charakteristika	k1gFnPc4	charakteristika
křídel	křídlo	k1gNnPc2	křídlo
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
poloha	poloha	k1gFnSc1	poloha
vůči	vůči	k7c3	vůči
trupu	trup	k1gInSc3	trup
(	(	kIx(	(
<g/>
u	u	k7c2	u
dopravních	dopravní	k2eAgInPc2d1	dopravní
letounů	letoun	k1gInPc2	letoun
většinou	většinou	k6eAd1	většinou
dolnoplošníky	dolnoplošník	k1gInPc7	dolnoplošník
<g/>
,	,	kIx,	,
vzácněji	vzácně	k6eAd2	vzácně
u	u	k7c2	u
turbovrtulových	turbovrtulový	k2eAgNnPc2d1	turbovrtulové
letadel	letadlo	k1gNnPc2	letadlo
hornoplošníky	hornoplošník	k1gInPc7	hornoplošník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
půdorysný	půdorysný	k2eAgInSc1d1	půdorysný
tvar	tvar	k1gInSc1	tvar
(	(	kIx(	(
<g/>
šípová	šípový	k2eAgFnSc1d1	šípová
<g/>
,	,	kIx,	,
přímá	přímý	k2eAgFnSc1d1	přímá
nebo	nebo	k8xC	nebo
lichoběžníková	lichoběžníkový	k2eAgFnSc1d1	lichoběžníková
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozpětí	rozpětí	k1gNnSc4	rozpětí
<g/>
,	,	kIx,	,
úhel	úhel	k1gInSc4	úhel
šípu	šíp	k1gInSc2	šíp
<g/>
,	,	kIx,	,
úhel	úhel	k1gInSc1	úhel
vzepětí	vzepětí	k1gNnSc2	vzepětí
<g/>
,	,	kIx,	,
štíhlost	štíhlost	k1gFnSc1	štíhlost
<g/>
,	,	kIx,	,
zúžení	zúžení	k1gNnSc1	zúžení
a	a	k8xC	a
zkroucení	zkroucení	k1gNnSc1	zkroucení
<g/>
.	.	kIx.	.
</s>
<s>
Profil	profil	k1gInSc1	profil
křídla	křídlo	k1gNnSc2	křídlo
je	být	k5eAaImIp3nS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
podle	podle	k7c2	podle
principu	princip	k1gInSc2	princip
Bernoulliho	Bernoulli	k1gMnSc2	Bernoulli
rovnice	rovnice	k1gFnSc2	rovnice
vznikal	vznikat	k5eAaImAgInS	vznikat
vztlak	vztlak	k1gInSc1	vztlak
(	(	kIx(	(
<g/>
nahoře	nahoře	k6eAd1	nahoře
podtlak	podtlak	k1gInSc1	podtlak
<g/>
,	,	kIx,	,
dole	dole	k6eAd1	dole
přetlak	přetlak	k1gInSc1	přetlak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vztlak	vztlak	k1gInSc1	vztlak
lze	lze	k6eAd1	lze
regulovat	regulovat	k5eAaImF	regulovat
pomocí	pomocí	k7c2	pomocí
tzv.	tzv.	kA	tzv.
mechanizace	mechanizace	k1gFnSc2	mechanizace
křídla	křídlo	k1gNnSc2	křídlo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
soustava	soustava	k1gFnSc1	soustava
pohyblivých	pohyblivý	k2eAgInPc2d1	pohyblivý
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
ovládány	ovládat	k5eAaImNgInP	ovládat
buď	buď	k8xC	buď
pilotem	pilot	k1gInSc7	pilot
nebo	nebo	k8xC	nebo
automaticky	automaticky	k6eAd1	automaticky
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
náběžné	náběžný	k2eAgFnSc6d1	náběžná
hraně	hrana	k1gFnSc6	hrana
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
soustava	soustava	k1gFnSc1	soustava
slotů	slot	k1gInPc2	slot
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
díky	díky	k7c3	díky
možnosti	možnost	k1gFnSc3	možnost
předsunutí	předsunutí	k1gNnSc1	předsunutí
při	při	k7c6	při
vyšším	vysoký	k2eAgInSc6d2	vyšší
úhlu	úhel	k1gInSc6	úhel
náběhu	náběh	k1gInSc2	náběh
zamezí	zamezit	k5eAaPmIp3nS	zamezit
odtrhávání	odtrhávání	k1gNnSc1	odtrhávání
proudnic	proudnice	k1gFnPc2	proudnice
(	(	kIx(	(
<g/>
vzduch	vzduch	k1gInSc1	vzduch
ze	z	k7c2	z
spodní	spodní	k2eAgFnSc2d1	spodní
strany	strana	k1gFnSc2	strana
křídla	křídlo	k1gNnSc2	křídlo
proudí	proudit	k5eAaImIp3nS	proudit
vzniklou	vzniklý	k2eAgFnSc7d1	vzniklá
mezerou	mezera	k1gFnSc7	mezera
na	na	k7c4	na
horní	horní	k2eAgFnSc4d1	horní
stranu	strana	k1gFnSc4	strana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vztlakové	vztlakový	k2eAgFnPc1d1	vztlaková
klapky	klapka	k1gFnPc1	klapka
na	na	k7c6	na
odtokové	odtokový	k2eAgFnSc6d1	odtoková
hraně	hrana	k1gFnSc6	hrana
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
napovídá	napovídat	k5eAaBmIp3nS	napovídat
název	název	k1gInSc4	název
jsou	být	k5eAaImIp3nP	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
prvkem	prvek	k1gInSc7	prvek
upravujícím	upravující	k2eAgInSc7d1	upravující
vztlak	vztlak	k1gInSc4	vztlak
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
vysouvat	vysouvat	k5eAaImF	vysouvat
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
zvětší	zvětšit	k5eAaPmIp3nS	zvětšit
plocha	plocha	k1gFnSc1	plocha
křídla	křídlo	k1gNnSc2	křídlo
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
i	i	k9	i
vztlak	vztlak	k1gInSc1	vztlak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
od	od	k7c2	od
trupu	trup	k1gInSc2	trup
jsou	být	k5eAaImIp3nP	být
umístěna	umístěn	k2eAgNnPc1d1	umístěno
křidélka	křidélko	k1gNnPc1	křidélko
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
směrovým	směrový	k2eAgNnSc7d1	směrové
kormidlem	kormidlo	k1gNnSc7	kormidlo
(	(	kIx(	(
<g/>
na	na	k7c6	na
svislé	svislý	k2eAgFnSc6d1	svislá
ocasní	ocasní	k2eAgFnSc6d1	ocasní
ploše	plocha	k1gFnSc6	plocha
<g/>
)	)	kIx)	)
nezbytná	zbytný	k2eNgFnSc1d1	zbytný
pro	pro	k7c4	pro
směrové	směrový	k2eAgNnSc4d1	směrové
řízení	řízení	k1gNnSc4	řízení
a	a	k8xC	a
zachování	zachování	k1gNnSc4	zachování
stability	stabilita	k1gFnSc2	stabilita
při	při	k7c6	při
náklonu	náklon	k1gInSc6	náklon
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
spoilery	spoiler	k1gInPc4	spoiler
(	(	kIx(	(
<g/>
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
straně	strana	k1gFnSc6	strana
interceptor	interceptor	k1gInSc1	interceptor
<g/>
)	)	kIx)	)
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
rušení	rušení	k1gNnSc3	rušení
vztlaku	vztlak	k1gInSc2	vztlak
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
výhradně	výhradně	k6eAd1	výhradně
při	při	k7c6	při
brzdění	brzdění	k1gNnSc6	brzdění
na	na	k7c6	na
přistávací	přistávací	k2eAgFnSc6d1	přistávací
ploše	plocha	k1gFnSc6	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Středem	středem	k7c2	středem
křídla	křídlo	k1gNnSc2	křídlo
prochází	procházet	k5eAaImIp3nS	procházet
jeden	jeden	k4xCgInSc1	jeden
hlavní	hlavní	k2eAgInSc1d1	hlavní
nosník	nosník	k1gInSc1	nosník
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gFnPc6	jeho
stranách	strana	k1gFnPc6	strana
pomocné	pomocný	k2eAgInPc4d1	pomocný
nosníky	nosník	k1gInPc4	nosník
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
vzájemně	vzájemně	k6eAd1	vzájemně
spojeny	spojen	k2eAgInPc4d1	spojen
kolmými	kolmý	k2eAgInPc7d1	kolmý
nebo	nebo	k8xC	nebo
šikmými	šikmý	k2eAgInPc7d1	šikmý
žebry	žebr	k1gInPc7	žebr
s	s	k7c7	s
odlehčovacími	odlehčovací	k2eAgInPc7d1	odlehčovací
otvory	otvor	k1gInPc7	otvor
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
tvoří	tvořit	k5eAaImIp3nS	tvořit
horní	horní	k2eAgInSc1d1	horní
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
dolní	dolní	k2eAgFnSc2d1	dolní
potah	potah	k1gInSc4	potah
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
křídel	křídlo	k1gNnPc2	křídlo
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
palivové	palivový	k2eAgFnPc1d1	palivová
nádrže	nádrž	k1gFnPc1	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Ocasní	ocasní	k2eAgFnPc1d1	ocasní
plochy	plocha	k1gFnPc1	plocha
jsou	být	k5eAaImIp3nP	být
nezbytné	zbytný	k2eNgFnPc1d1	zbytný
pro	pro	k7c4	pro
zajištění	zajištění	k1gNnSc4	zajištění
stability	stabilita	k1gFnSc2	stabilita
a	a	k8xC	a
řiditelnosti	řiditelnost	k1gFnSc2	řiditelnost
letadla	letadlo	k1gNnSc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
tvar	tvar	k1gInSc1	tvar
a	a	k8xC	a
konstrukce	konstrukce	k1gFnSc1	konstrukce
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
křídlům	křídlo	k1gNnPc3	křídlo
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
křídla	křídlo	k1gNnSc2	křídlo
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
pevných	pevný	k2eAgFnPc2d1	pevná
a	a	k8xC	a
pohyblivých	pohyblivý	k2eAgFnPc2d1	pohyblivá
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Vodorovné	vodorovný	k2eAgFnPc1d1	vodorovná
ocasní	ocasní	k2eAgFnPc1d1	ocasní
plochy	plocha	k1gFnPc1	plocha
(	(	kIx(	(
<g/>
VOP	VOP	kA	VOP
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
celé	celá	k1gFnPc4	celá
natáčet	natáčet	k5eAaImF	natáčet
vůči	vůči	k7c3	vůči
draku	drak	k1gInSc3	drak
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
o	o	k7c4	o
9	[number]	k4	9
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Pevnou	pevný	k2eAgFnSc4d1	pevná
část	část	k1gFnSc4	část
nazýváme	nazývat	k5eAaImIp1nP	nazývat
stabilizátorem	stabilizátor	k1gInSc7	stabilizátor
a	a	k8xC	a
pohyblivou	pohyblivý	k2eAgFnSc4d1	pohyblivá
výškovým	výškový	k2eAgNnSc7d1	výškové
kormidlem	kormidlo	k1gNnSc7	kormidlo
(	(	kIx(	(
<g/>
tvoří	tvořit	k5eAaImIp3nP	tvořit
odtokovou	odtokový	k2eAgFnSc4d1	odtoková
hranu	hrana	k1gFnSc4	hrana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svislá	svislý	k2eAgFnSc1d1	svislá
ocasní	ocasní	k2eAgFnSc1d1	ocasní
plocha	plocha	k1gFnSc1	plocha
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
směrové	směrový	k2eAgFnSc2d1	směrová
stability	stabilita	k1gFnSc2	stabilita
a	a	k8xC	a
ke	k	k7c3	k
směrovému	směrový	k2eAgNnSc3d1	směrové
řízení	řízení	k1gNnSc3	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Pevná	pevný	k2eAgFnSc1d1	pevná
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kýlová	kýlový	k2eAgFnSc1d1	kýlová
plocha	plocha	k1gFnSc1	plocha
a	a	k8xC	a
pohyblivá	pohyblivý	k2eAgFnSc1d1	pohyblivá
směrové	směrový	k2eAgNnSc4d1	směrové
kormidlo	kormidlo	k1gNnSc4	kormidlo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
pohyby	pohyb	k1gInPc1	pohyb
jsou	být	k5eAaImIp3nP	být
přesně	přesně	k6eAd1	přesně
zkoordinovány	zkoordinovat	k5eAaPmNgFnP	zkoordinovat
s	s	k7c7	s
křidélky	křidélko	k1gNnPc7	křidélko
a	a	k8xC	a
vztlakovými	vztlakový	k2eAgFnPc7d1	vztlaková
klapkami	klapka	k1gFnPc7	klapka
na	na	k7c6	na
křídlech	křídlo	k1gNnPc6	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Pohyblivé	pohyblivý	k2eAgFnPc1d1	pohyblivá
části	část	k1gFnPc1	část
jsou	být	k5eAaImIp3nP	být
ovládány	ovládán	k2eAgInPc4d1	ovládán
hydraulickými	hydraulický	k2eAgInPc7d1	hydraulický
válci	válec	k1gInPc7	válec
<g/>
.	.	kIx.	.
</s>
<s>
Povely	povel	k1gInPc1	povel
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
starších	starý	k2eAgInPc2d2	starší
typů	typ	k1gInPc2	typ
letadel	letadlo	k1gNnPc2	letadlo
(	(	kIx(	(
<g/>
Airbus	airbus	k1gInSc1	airbus
A	A	kA	A
<g/>
310	[number]	k4	310
<g/>
)	)	kIx)	)
přenášeny	přenášet	k5eAaImNgFnP	přenášet
mechanicky	mechanicky	k6eAd1	mechanicky
u	u	k7c2	u
nových	nový	k2eAgInPc2d1	nový
typů	typ	k1gInPc2	typ
elektronicky	elektronicky	k6eAd1	elektronicky
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Podvozek	podvozek	k1gInSc1	podvozek
letadla	letadlo	k1gNnSc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Podvozek	podvozek	k1gInSc1	podvozek
tvoří	tvořit	k5eAaImIp3nS	tvořit
nezbytnou	nezbytný	k2eAgFnSc4d1	nezbytná
část	část	k1gFnSc4	část
letounu	letoun	k1gInSc2	letoun
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
pojíždění	pojíždění	k1gNnSc2	pojíždění
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
tlumení	tlumení	k1gNnSc1	tlumení
nárazu	náraz	k1gInSc2	náraz
při	při	k7c6	při
dosednutí	dosednutí	k1gNnSc6	dosednutí
<g/>
.	.	kIx.	.
</s>
<s>
Tlumicí	tlumice	k1gFnSc7	tlumice
účinek	účinek	k1gInSc1	účinek
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
tlumičem	tlumič	k1gInSc7	tlumič
umístěným	umístěný	k2eAgInSc7d1	umístěný
vně	vně	k7c2	vně
podvozkové	podvozkový	k2eAgFnSc2d1	podvozková
nohy	noha	k1gFnSc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Podvozky	podvozek	k1gInPc1	podvozek
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
zatahovací	zatahovací	k2eAgFnSc4d1	zatahovací
díky	díky	k7c3	díky
pákovému	pákový	k2eAgNnSc3d1	pákové
<g/>
,	,	kIx,	,
či	či	k8xC	či
teleskopickému	teleskopický	k2eAgNnSc3d1	teleskopické
uspořádání	uspořádání	k1gNnSc3	uspořádání
<g/>
.	.	kIx.	.
</s>
<s>
Brzděním	brzdění	k1gNnSc7	brzdění
kol	kola	k1gFnPc2	kola
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
zkrátit	zkrátit	k5eAaPmF	zkrátit
brzdná	brzdný	k2eAgFnSc1d1	brzdná
dráha	dráha	k1gFnSc1	dráha
až	až	k9	až
o	o	k7c4	o
50	[number]	k4	50
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
vlastností	vlastnost	k1gFnSc7	vlastnost
konstrukčních	konstrukční	k2eAgFnPc2d1	konstrukční
částí	část	k1gFnPc2	část
letounů	letoun	k1gInPc2	letoun
je	být	k5eAaImIp3nS	být
pevnost	pevnost	k1gFnSc4	pevnost
a	a	k8xC	a
odolnost	odolnost	k1gFnSc4	odolnost
vůči	vůči	k7c3	vůči
mimořádně	mimořádně	k6eAd1	mimořádně
silným	silný	k2eAgFnPc3d1	silná
aerodynamickým	aerodynamický	k2eAgFnPc3d1	aerodynamická
silám	síla	k1gFnPc3	síla
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
konstruktéři	konstruktér	k1gMnPc1	konstruktér
snaží	snažit	k5eAaImIp3nP	snažit
snižovat	snižovat	k5eAaImF	snižovat
hmotnost	hmotnost	k1gFnSc4	hmotnost
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
šetření	šetření	k1gNnSc2	šetření
na	na	k7c6	na
pohonných	pohonný	k2eAgFnPc6d1	pohonná
hmotách	hmota	k1gFnPc6	hmota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
nejvíce	hodně	k6eAd3	hodně
používají	používat	k5eAaImIp3nP	používat
slitiny	slitina	k1gFnPc4	slitina
lehkých	lehký	k2eAgInPc2d1	lehký
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
hliníku	hliník	k1gInSc2	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
prakticky	prakticky	k6eAd1	prakticky
na	na	k7c4	na
veškeré	veškerý	k3xTgInPc4	veškerý
potahy	potah	k1gInPc4	potah
a	a	k8xC	a
některé	některý	k3yIgInPc4	některý
méně	málo	k6eAd2	málo
namáhané	namáhaný	k2eAgInPc4d1	namáhaný
spoje	spoj	k1gInPc4	spoj
(	(	kIx(	(
<g/>
mez	mez	k1gFnSc1	mez
pevnosti	pevnost	k1gFnSc2	pevnost
do	do	k7c2	do
500	[number]	k4	500
MPa	MPa	k1gFnPc2	MPa
<g/>
,	,	kIx,	,
objemová	objemový	k2eAgFnSc1d1	objemová
hmotnost	hmotnost	k1gFnSc1	hmotnost
2700	[number]	k4	2700
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nosná	nosný	k2eAgFnSc1d1	nosná
část	část	k1gFnSc1	část
konstrukce	konstrukce	k1gFnSc2	konstrukce
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
titanových	titanový	k2eAgFnPc2d1	titanová
slitin	slitina	k1gFnPc2	slitina
(	(	kIx(	(
<g/>
mez	mez	k1gFnSc1	mez
pevnosti	pevnost	k1gFnSc2	pevnost
až	až	k9	až
1200	[number]	k4	1200
MPa	MPa	k1gMnPc2	MPa
<g/>
,	,	kIx,	,
objemová	objemový	k2eAgFnSc1d1	objemová
hmotnost	hmotnost	k1gFnSc1	hmotnost
4500	[number]	k4	4500
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
svařovaných	svařovaný	k2eAgInPc2d1	svařovaný
spojů	spoj	k1gInPc2	spoj
se	se	k3xPyFc4	se
titan	titan	k1gInSc1	titan
nahradí	nahradit	k5eAaPmIp3nS	nahradit
ocelí	ocel	k1gFnSc7	ocel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
čím	co	k3yInSc7	co
dál	daleko	k6eAd2	daleko
tím	ten	k3xDgNnSc7	ten
více	hodně	k6eAd2	hodně
používají	používat	k5eAaImIp3nP	používat
kompozitní	kompozitní	k2eAgInPc4d1	kompozitní
materiály	materiál	k1gInPc4	materiál
a	a	k8xC	a
plasty	plast	k1gInPc4	plast
<g/>
.	.	kIx.	.
</s>
<s>
Spoje	spoj	k1gInPc1	spoj
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
provádějí	provádět	k5eAaImIp3nP	provádět
převážně	převážně	k6eAd1	převážně
lepené	lepený	k2eAgInPc1d1	lepený
(	(	kIx(	(
<g/>
i	i	k9	i
u	u	k7c2	u
velmi	velmi	k6eAd1	velmi
namáhaných	namáhaný	k2eAgFnPc2d1	namáhaná
částí	část	k1gFnPc2	část
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
opouští	opouštět	k5eAaImIp3nS	opouštět
nýtování	nýtování	k1gNnSc4	nýtování
<g/>
.	.	kIx.	.
</s>
<s>
Letecké	letecký	k2eAgInPc1d1	letecký
motory	motor	k1gInPc1	motor
jsou	být	k5eAaImIp3nP	být
jedinými	jediný	k2eAgFnPc7d1	jediná
částmi	část	k1gFnPc7	část
letounu	letoun	k1gInSc2	letoun
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
samotný	samotný	k2eAgInSc4d1	samotný
pohyb	pohyb	k1gInSc4	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
v	v	k7c6	v
gondolách	gondola	k1gFnPc6	gondola
pod	pod	k7c7	pod
křídly	křídlo	k1gNnPc7	křídlo
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
trupu	trup	k1gInSc2	trup
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
optimálně	optimálně	k6eAd1	optimálně
vyrovnávají	vyrovnávat	k5eAaImIp3nP	vyrovnávat
moment	moment	k1gInSc4	moment
vyvolaný	vyvolaný	k2eAgInSc4d1	vyvolaný
hmotností	hmotnost	k1gFnSc7	hmotnost
křídel	křídlo	k1gNnPc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
běžné	běžný	k2eAgInPc1d1	běžný
jsou	být	k5eAaImIp3nP	být
motory	motor	k1gInPc1	motor
na	na	k7c6	na
zádi	záď	k1gFnSc6	záď
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
tohoto	tento	k3xDgNnSc2	tento
uspořádání	uspořádání	k1gNnSc2	uspořádání
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
lepší	dobrý	k2eAgFnSc1d2	lepší
aerodynamika	aerodynamika	k1gFnSc1	aerodynamika
křídla	křídlo	k1gNnSc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Letecké	letecký	k2eAgInPc1d1	letecký
motory	motor	k1gInPc1	motor
dělíme	dělit	k5eAaImIp1nP	dělit
podle	podle	k7c2	podle
principu	princip	k1gInSc2	princip
na	na	k7c4	na
<g/>
:	:	kIx,	:
Pístové	pístový	k2eAgInPc4d1	pístový
-	-	kIx~	-
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
malých	malý	k2eAgNnPc2d1	malé
dopravních	dopravní	k2eAgNnPc2d1	dopravní
letadel	letadlo	k1gNnPc2	letadlo
a	a	k8xC	a
aerotaxi	aerotaxi	k1gNnSc2	aerotaxi
řadové	řadový	k2eAgFnSc2d1	řadová
hvězdicové	hvězdicový	k2eAgFnSc2d1	hvězdicová
Lopatkové	lopatkový	k2eAgFnSc2d1	lopatková
turbovrtulové	turbovrtulový	k2eAgFnSc2d1	turbovrtulová
proudové	proudový	k2eAgFnSc2d1	proudová
Nádrže	nádrž	k1gFnSc2	nádrž
leteckých	letecký	k2eAgFnPc2d1	letecká
pohonných	pohonný	k2eAgFnPc2d1	pohonná
hmot	hmota	k1gFnPc2	hmota
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
současných	současný	k2eAgNnPc2d1	současné
dopravních	dopravní	k2eAgNnPc2d1	dopravní
letadel	letadlo	k1gNnPc2	letadlo
umístěny	umístit	k5eAaPmNgFnP	umístit
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
křídlech	křídlo	k1gNnPc6	křídlo
a	a	k8xC	a
v	v	k7c6	v
centroplánu	centroplán	k1gInSc6	centroplán
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
přispívá	přispívat	k5eAaImIp3nS	přispívat
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
hmotnosti	hmotnost	k1gFnSc2	hmotnost
trupu	trup	k1gInSc2	trup
na	na	k7c4	na
minimum	minimum	k1gNnSc4	minimum
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
letu	let	k1gInSc2	let
je	být	k5eAaImIp3nS	být
palivo	palivo	k1gNnSc4	palivo
vystaveno	vystaven	k2eAgNnSc1d1	vystaveno
působení	působení	k1gNnSc1	působení
setrvačných	setrvačný	k2eAgFnPc2d1	setrvačná
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	k9	aby
za	za	k7c2	za
všech	všecek	k3xTgFnPc2	všecek
okolností	okolnost	k1gFnPc2	okolnost
byla	být	k5eAaImAgFnS	být
zajištěna	zajistit	k5eAaPmNgFnS	zajistit
spolehlivá	spolehlivý	k2eAgFnSc1d1	spolehlivá
dodávka	dodávka	k1gFnSc1	dodávka
do	do	k7c2	do
motorů	motor	k1gInPc2	motor
jsou	být	k5eAaImIp3nP	být
letadla	letadlo	k1gNnPc1	letadlo
vybavena	vybavit	k5eAaPmNgNnP	vybavit
elektronickým	elektronický	k2eAgMnSc7d1	elektronický
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
plně	plně	k6eAd1	plně
automatickým	automatický	k2eAgInSc7d1	automatický
<g/>
)	)	kIx)	)
systémem	systém	k1gInSc7	systém
přečerpávání	přečerpávání	k1gNnSc2	přečerpávání
paliva	palivo	k1gNnSc2	palivo
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
mezi	mezi	k7c7	mezi
nádrží	nádrž	k1gFnSc7	nádrž
a	a	k8xC	a
motorem	motor	k1gInSc7	motor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
nádržemi	nádrž	k1gFnPc7	nádrž
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
zachování	zachování	k1gNnSc2	zachování
polohy	poloha	k1gFnSc2	poloha
těžiště	těžiště	k1gNnSc2	těžiště
<g/>
.	.	kIx.	.
</s>
<s>
Objem	objem	k1gInSc1	objem
palivových	palivový	k2eAgFnPc2d1	palivová
nádrží	nádrž	k1gFnPc2	nádrž
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
desítek	desítka	k1gFnPc2	desítka
tisíc	tisíc	k4xCgInSc1	tisíc
litrů	litr	k1gInPc2	litr
(	(	kIx(	(
<g/>
Boeing	boeing	k1gInSc1	boeing
737	[number]	k4	737
<g/>
-	-	kIx~	-
<g/>
500	[number]	k4	500
<g/>
:	:	kIx,	:
23800	[number]	k4	23800
l	l	kA	l
<g/>
;	;	kIx,	;
Airbus	airbus	k1gInSc1	airbus
A	A	kA	A
<g/>
310	[number]	k4	310
<g/>
:	:	kIx,	:
61071	[number]	k4	61071
l	l	kA	l
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
až	až	k6eAd1	až
stovek	stovka	k1gFnPc2	stovka
tisíc	tisíc	k4xCgInSc1	tisíc
litrů	litr	k1gInPc2	litr
(	(	kIx(	(
<g/>
Airbus	airbus	k1gInSc4	airbus
A	a	k9	a
<g/>
380	[number]	k4	380
<g/>
:	:	kIx,	:
310000	[number]	k4	310000
l	l	kA	l
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
