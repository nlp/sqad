<s>
Resolutio	Resolutio	k6eAd1
Carolina	Carolin	k2eAgFnSc1d1
</s>
<s>
Resolutio	Resolutio	k1gNnSc1
Carolina	Carolina	k1gFnSc1
bylo	být	k5eAaImAgNnS
nařízení	nařízení	k1gNnSc1
císaře	císař	k1gMnSc2
a	a	k8xC
uherského	uherský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Karla	Karel	k1gMnSc2
VI	VI	kA
<g/>
.	.	kIx.
z	z	k7c2
roku	rok	k1gInSc2
1731	#num#	k4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
upravovalo	upravovat	k5eAaImAgNnS
náboženské	náboženský	k2eAgInPc4d1
poměry	poměr	k1gInPc4
v	v	k7c6
Uhrách	Uhry	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
tohoto	tento	k3xDgNnSc2
nařízení	nařízení	k1gNnSc2
bylo	být	k5eAaImAgNnS
státem	stát	k1gInSc7
uznané	uznaný	k2eAgNnSc1d1
náboženství	náboženství	k1gNnSc1
římskokatolické	římskokatolický	k2eAgNnSc1d1
vyznání	vyznání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protestanti	protestant	k1gMnPc1
mohli	moct	k5eAaImAgMnP
na	na	k7c6
základě	základ	k1gInSc6
nařízení	nařízení	k1gNnSc2
svobodně	svobodně	k6eAd1
a	a	k8xC
veřejně	veřejně	k6eAd1
vyznávat	vyznávat	k5eAaImF
svou	svůj	k3xOyFgFnSc4
víru	víra	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
na	na	k7c6
tzv.	tzv.	kA
artikulárních	artikulární	k2eAgInPc6d1
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
se	se	k3xPyFc4
protestantští	protestantský	k2eAgMnPc1d1
kněží	kněz	k1gMnPc1
nesměli	smět	k5eNaImAgMnP
bez	bez	k7c2
povolení	povolení	k1gNnSc2
vzdálit	vzdálit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
křtu	křest	k1gInSc6
dětí	dítě	k1gFnPc2
protestanti	protestant	k1gMnPc1
podléhali	podléhat	k5eAaImAgMnP
dozoru	dozor	k1gInSc2
katolického	katolický	k2eAgNnSc2d1
duchovenstva	duchovenstvo	k1gNnSc2
a	a	k8xC
v	v	k7c6
otázkách	otázka	k1gFnPc6
manželství	manželství	k1gNnSc2
římskokatolickému	římskokatolický	k2eAgInSc3d1
tribunálu	tribunál	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smíšená	smíšený	k2eAgFnSc1d1
manželství	manželství	k1gNnPc1
se	se	k3xPyFc4
mohla	moct	k5eAaImAgNnP
uzavírat	uzavírat	k5eAaImF,k5eAaPmF
pouze	pouze	k6eAd1
před	před	k7c7
římskokatolickým	římskokatolický	k2eAgMnSc7d1
knězem	kněz	k1gMnSc7
<g/>
,	,	kIx,
děti	dítě	k1gFnPc4
narozené	narozený	k2eAgFnPc4d1
v	v	k7c6
takových	takový	k3xDgNnPc6
manželstvích	manželství	k1gNnPc6
musely	muset	k5eAaImAgFnP
být	být	k5eAaImF
římskokatolického	římskokatolický	k2eAgNnSc2d1
vyznání	vyznání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Resolutio	Resolutio	k6eAd1
Carolina	Carolina	k1gFnSc1
byla	být	k5eAaImAgFnS
upravena	upravit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1734	#num#	k4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1781	#num#	k4
zrušena	zrušit	k5eAaPmNgFnS
a	a	k8xC
nahrazena	nahradit	k5eAaPmNgFnS
Tolerančním	toleranční	k2eAgInSc7d1
patentem	patent	k1gInSc7
Josefa	Josef	k1gMnSc2
II	II	kA
<g/>
..	..	k?
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Resolutio	Resolutio	k6eAd1
Carolina	Carolin	k2eAgFnSc1d1
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
