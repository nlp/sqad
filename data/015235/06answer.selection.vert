<s desamb="1">
Při	při	k7c6
sčítání	sčítání	k1gNnSc6
obyvatelstva	obyvatelstvo	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
se	s	k7c7
37	#num#	k4
092	#num#	k4
obyvatel	obyvatel	k1gMnPc2
provincie	provincie	k1gFnSc2
přihlásilo	přihlásit	k5eAaPmAgNnS
k	k	k7c3
indiánskému	indiánský	k2eAgInSc3d1
původu	původ	k1gInSc3
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
9	#num#	k4
319	#num#	k4
lidí	člověk	k1gMnPc2
k	k	k7c3
africkému	africký	k2eAgInSc3d1
původu	původ	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
území	území	k1gNnSc6
provincie	provincie	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
Panamy	Panama	k1gFnSc2
-	-	kIx~
Volcán	Volcán	k2eAgMnSc1d1
Barú	Barú	k1gMnSc1
<g/>
.	.	kIx.
</s>