<s>
Chiriquí	Chiriquí	k6eAd1
</s>
<s>
Chiriquí	Chiriquí	k2eAgFnSc1d1
Provincia	Provincia	k1gFnSc1
de	de	k?
Chiriquí	Chiriquí	k1gFnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
David	David	k1gMnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
8	#num#	k4
<g/>
°	°	k?
<g/>
26	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
82	#num#	k4
<g/>
°	°	k?
<g/>
26	#num#	k4
<g/>
′	′	k?
z.	z.	k?
d.	d.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
6	#num#	k4
490,9	490,9	k4
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
km²	km²	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
Obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
416	#num#	k4
873	#num#	k4
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
Šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Zastaralý	zastaralý	k2eAgInSc1d1
parametr	parametr	k1gInSc1
infoboxu	infobox	k1gInSc2
<g/>
}}	}}	k?
prohlašuje	prohlašovat	k5eAaImIp3nS
parametr	parametr	k1gInSc1
"	"	kIx"
<g/>
hustota	hustota	k1gFnSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
s	s	k7c7
hodnotou	hodnota	k1gFnSc7
"	"	kIx"
<g/>
64,2	64,2	k4
<g/>
"	"	kIx"
<g/>
)	)	kIx)
šablony	šablona	k1gFnPc1
"	"	kIx"
<g/>
Infobox	Infobox	k1gInSc1
-	-	kIx~
region	region	k1gInSc1
<g/>
"	"	kIx"
za	za	k7c4
zastaralý	zastaralý	k2eAgInSc4d1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
64,2	64,2	k4
obyv	obyva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
regionu	region	k1gInSc2
Nadřazený	nadřazený	k2eAgInSc1d1
celek	celek	k1gInSc1
</s>
<s>
Panama	panama	k2eAgInPc1d1
Panama	panama	k2eAgInPc1d1
Podřízené	podřízený	k2eAgInPc1d1
celky	celek	k1gInPc1
</s>
<s>
13	#num#	k4
distriktů	distrikt	k1gInPc2
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Chiriquí	Chiriquí	k1gFnSc1
je	být	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
10	#num#	k4
panamských	panamský	k2eAgFnPc2d1
provincií	provincie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
ve	v	k7c6
západě	západ	k1gInSc6
státu	stát	k1gInSc2
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
Pacifiku	Pacifik	k1gInSc2
při	při	k7c6
hranicích	hranice	k1gFnPc6
s	s	k7c7
Kostarikou	Kostarika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zabírá	zabírat	k5eAaImIp3nS
8,75	8,75	k4
%	%	kIx~
rozlohy	rozloha	k1gFnSc2
celé	celý	k2eAgFnSc2d1
Panamy	Panama	k1gFnSc2
a	a	k8xC
žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
12,24	12,24	k4
%	%	kIx~
panamské	panamský	k2eAgFnSc2d1
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
sčítání	sčítání	k1gNnSc6
obyvatelstva	obyvatelstvo	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
se	s	k7c7
37	#num#	k4
092	#num#	k4
obyvatel	obyvatel	k1gMnPc2
provincie	provincie	k1gFnSc2
přihlásilo	přihlásit	k5eAaPmAgNnS
k	k	k7c3
indiánskému	indiánský	k2eAgInSc3d1
původu	původ	k1gInSc3
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
9	#num#	k4
319	#num#	k4
lidí	člověk	k1gMnPc2
k	k	k7c3
africkému	africký	k2eAgInSc3d1
původu	původ	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
území	území	k1gNnSc6
provincie	provincie	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
Panamy	Panama	k1gFnSc2
-	-	kIx~
Volcán	Volcán	k2eAgMnSc1d1
Barú	Barú	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Provincie	provincie	k1gFnSc1
je	být	k5eAaImIp3nS
dále	daleko	k6eAd2
dělena	dělit	k5eAaImNgFnS
na	na	k7c4
14	#num#	k4
distriktů	distrikt	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
Alanje	Alanje	k1gFnSc1
(	(	kIx(
<g/>
Santiago	Santiago	k1gNnSc1
de	de	k?
Alanje	Alanj	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Barú	Barú	k?
(	(	kIx(
<g/>
Puerto	Puerta	k1gFnSc5
Tomás	Tomás	k1gInSc4
Armuelles	Armuelles	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Boquerón	Boquerón	k1gMnSc1
(	(	kIx(
<g/>
Boquerón	Boquerón	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Boquete	Boquat	k5eAaImIp2nP,k5eAaPmIp2nP,k5eAaBmIp2nP
(	(	kIx(
<g/>
Bajo	Baja	k1gMnSc5
Boquete	Boquet	k1gMnSc5
<g/>
)	)	kIx)
</s>
<s>
Bugaba	Bugaba	k1gFnSc1
(	(	kIx(
<g/>
La	la	k1gNnSc1
Concepción	Concepción	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
David	David	k1gMnSc1
(	(	kIx(
<g/>
San	San	k1gMnSc1
José	Josá	k1gFnSc2
de	de	k?
David	David	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Dolega	Dolega	k1gFnSc1
(	(	kIx(
<g/>
San	San	k1gMnSc1
Francisco	Francisco	k1gMnSc1
de	de	k?
Dolega	Dolega	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Gualaca	Gualaca	k1gFnSc1
(	(	kIx(
<g/>
Gualaca	Gualaca	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Remedios	Remedios	k1gInSc1
(	(	kIx(
<g/>
Nuestra	Nuestra	k1gFnSc1
Señ	Señ	k1gFnSc1
de	de	k?
los	los	k1gInSc1
Remedios	Remedios	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Renacimiento	Renacimiento	k1gNnSc1
(	(	kIx(
<g/>
Río	Río	k1gFnSc1
Sereno	Seren	k2eAgNnSc1d1
<g/>
)	)	kIx)
</s>
<s>
San	San	k?
Félix	Félix	k1gInSc1
(	(	kIx(
<g/>
Las	laso	k1gNnPc2
Lajas	Lajasa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
San	San	k?
Lorenzo	Lorenza	k1gFnSc5
(	(	kIx(
<g/>
Horconcitos	Horconcitos	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Tierras	Tierras	k1gMnSc1
Altas	Altas	k1gMnSc1
(	(	kIx(
<g/>
Volcán	Volcán	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Tolé	Tolé	k1gNnSc1
(	(	kIx(
<g/>
Tolé	Tolé	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Chiriquí	Chiriquí	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
1	#num#	k4
2	#num#	k4
údaj	údaj	k1gInSc1
ze	z	k7c2
sčítání	sčítání	k1gNnSc2
obyvatelstva	obyvatelstvo	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
SUPERFICIE	SUPERFICIE	kA
<g/>
,	,	kIx,
POBLACIÓN	POBLACIÓN	kA
Y	Y	kA
DENSIDAD	DENSIDAD	kA
DE	DE	k?
POBLACIÓN	POBLACIÓN	kA
EN	EN	kA
LA	la	k1gNnPc2
REPÚBLICA	REPÚBLICA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Instituto	Institut	k2eAgNnSc1d1
Nacional	Nacional	k1gMnSc3
de	de	k?
Estadísticas	Estadísticas	k1gMnSc1
y	y	k?
Censos	Censos	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
údaj	údaj	k1gInSc1
ze	z	k7c2
sčítání	sčítání	k1gNnSc2
obyvatelstva	obyvatelstvo	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
POBLACIÓN	POBLACIÓN	kA
INDÍGENA	INDÍGENA	kA
EN	EN	kA
LA	la	k1gNnPc2
REPÚBLICA	REPÚBLICA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Instituto	Institut	k2eAgNnSc1d1
Nacional	Nacional	k1gMnSc3
de	de	k?
Estadísticas	Estadísticas	k1gMnSc1
y	y	k?
Censos	Censos	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
údaj	údaj	k1gInSc1
ze	z	k7c2
sčítání	sčítání	k1gNnSc2
obyvatelstva	obyvatelstvo	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
POBLACIÓN	POBLACIÓN	kA
TOTAL	totat	k5eAaImAgMnS
Y	Y	kA
AFRODESCENDIENTE	AFRODESCENDIENTE	kA
EN	EN	kA
LA	la	k1gNnPc2
REPÚBLICA	REPÚBLICA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Instituto	Institut	k2eAgNnSc1d1
Nacional	Nacional	k1gMnSc3
de	de	k?
Estadísticas	Estadísticas	k1gMnSc1
y	y	k?
Censos	Censos	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Panama	panama	k1gNnSc1
–	–	k?
República	Repúblic	k1gInSc2
de	de	k?
Panamá	Panamý	k2eAgFnSc1d1
–	–	k?
(	(	kIx(
<g/>
PA	Pa	kA
<g/>
)	)	kIx)
Provincie	provincie	k1gFnSc1
</s>
<s>
Bocas	Bocas	k1gInSc1
del	del	k?
Toro	Toro	k?
•	•	k?
Coclé	Coclý	k2eAgFnSc2d1
•	•	k?
Colón	colón	k1gInSc1
•	•	k?
Darién	Darién	k1gInSc1
•	•	k?
Herrera	Herrera	k1gFnSc1
•	•	k?
Chiriquí	Chiriquí	k1gFnSc2
•	•	k?
Los	los	k1gInSc1
Santos	Santos	k1gInSc1
•	•	k?
Panamá	Panamý	k2eAgFnSc1d1
•	•	k?
Veraguas	Veraguas	k1gInSc1
•	•	k?
Západní	západní	k2eAgFnSc1d1
Panamá	Panamý	k2eAgFnSc1d1
Indiánské	indiánský	k2eAgFnPc4d1
comarcy	comarca	k1gFnPc4
</s>
<s>
Emberá-Wounaan	Emberá-Wounaan	k1gMnSc1
•	•	k?
Guna	Guna	k1gMnSc1
Yala	Yala	k1gMnSc1
•	•	k?
Naso	Naso	k1gMnSc1
Tjër	Tjër	k1gMnSc1
Di	Di	k1gMnSc1
•	•	k?
Ngöbe-Buglé	Ngöbe-Buglý	k2eAgNnSc1d1
•	•	k?
(	(	kIx(
<g/>
Madugandí	Madugandí	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
(	(	kIx(
<g/>
Wargandí	Wargandí	k1gNnSc1
<g/>
)	)	kIx)
</s>
