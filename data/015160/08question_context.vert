<s>
Salpn	Salpn	k1gInSc1
je	být	k5eAaImIp3nS
běžně	běžně	k6eAd1
používaný	používaný	k2eAgInSc1d1
zkrácený	zkrácený	k2eAgInSc1d1
název	název	k1gInSc1
chelatačního	chelatační	k2eAgNnSc2d1
činidla	činidlo	k1gNnSc2
N	N	kA
<g/>
,	,	kIx,
<g/>
N	N	kA
<g/>
′	′	k?
<g/>
-bis	-bis	k1gInSc1
<g/>
(	(	kIx(
<g/>
salicyliden	salicylidno	k1gNnPc2
<g/>
)	)	kIx)
<g/>
-	-	kIx~
<g/>
1,2	1,2	k4
<g/>
-propandiaminu	-propandiamin	k1gInSc2
(	(	kIx(
<g/>
systematicky	systematicky	k6eAd1
nazývaného	nazývaný	k2eAgInSc2d1
2,2	2,2	k4
<g/>
'	'	kIx"
<g/>
-	-	kIx~
<g/>
{	{	kIx(
<g/>
1,2	1,2	k4
<g/>
-propandiylbis	-propandiylbis	k1gFnPc2
<g/>
[	[	kIx(
<g/>
nitrilo	nitrít	k5eAaPmAgNnS,k5eAaBmAgNnS,k5eAaImAgNnS
<g/>
(	(	kIx(
<g/>
E	E	kA
<g/>
)	)	kIx)
<g/>
methylyliden	methylylidna	k1gFnPc2
<g/>
]	]	kIx)
<g/>
}	}	kIx)
<g/>
difenol	difenol	k1gInSc1
<g/>
.	.	kIx.
</s>
