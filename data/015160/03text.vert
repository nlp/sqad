<s>
Salpn	Salpn	k1gMnSc1
</s>
<s>
Salpn	Salpn	k1gMnSc1
</s>
<s>
Strukturní	strukturní	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Systematický	systematický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
2,2	2,2	k4
<g/>
'	'	kIx"
<g/>
-	-	kIx~
<g/>
{	{	kIx(
<g/>
1,2	1,2	k4
<g/>
-propandiylbis	-propandiylbis	k1gFnPc2
<g/>
[	[	kIx(
<g/>
nitrilo	nitrit	k5eAaImAgNnS,k5eAaPmAgNnS,k5eAaBmAgNnS
<g/>
(	(	kIx(
<g/>
E	E	kA
<g/>
)	)	kIx)
<g/>
methylyliden	methylylidna	k1gFnPc2
<g/>
]	]	kIx)
<g/>
}	}	kIx)
<g/>
difenol	difenol	k1gInSc1
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
N	N	kA
<g/>
,	,	kIx,
<g/>
N	N	kA
<g/>
′	′	k?
<g/>
-ethylenbis	-ethylenbis	k1gInSc1
<g/>
(	(	kIx(
<g/>
salicylimin	salicylimin	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Sumární	sumární	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
C17H18N2O2	C17H18N2O2	k4
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
257636-52-5	257636-52-5	k4
</s>
<s>
PubChem	PubCh	k1gInSc7
</s>
<s>
7210	#num#	k4
</s>
<s>
SMILES	SMILES	kA
</s>
<s>
OC	OC	kA
<g/>
1	#num#	k4
<g/>
=	=	kIx~
<g/>
CC	CC	kA
<g/>
=	=	kIx~
<g/>
CC	CC	kA
<g/>
=	=	kIx~
<g/>
C	C	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
C	C	kA
<g/>
=	=	kIx~
<g/>
N	N	kA
<g/>
/	/	kIx~
<g/>
CC	CC	kA
<g/>
(	(	kIx(
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
/	/	kIx~
<g/>
N	N	kA
<g/>
=	=	kIx~
<g/>
C	C	kA
<g/>
/	/	kIx~
<g/>
C	C	kA
<g/>
2	#num#	k4
<g/>
=	=	kIx~
<g/>
CC	CC	kA
<g/>
=	=	kIx~
<g/>
CC	CC	kA
<g/>
=	=	kIx~
<g/>
C	C	kA
<g/>
2	#num#	k4
<g/>
O	o	k7c6
</s>
<s>
InChI	InChI	k?
</s>
<s>
1	#num#	k4
<g/>
S	s	k7c7
<g/>
/	/	kIx~
<g/>
C	C	kA
<g/>
17	#num#	k4
<g/>
H	H	kA
<g/>
18	#num#	k4
<g/>
N	N	kA
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
c	c	k0
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
(	(	kIx(
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
(	(	kIx(
<g/>
15	#num#	k4
<g/>
)	)	kIx)
<g/>
21	#num#	k4
<g/>
)	)	kIx)
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
(	(	kIx(
<g/>
14	#num#	k4
<g/>
)	)	kIx)
<g/>
20	#num#	k4
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
9,11	9,11	k4
<g/>
-	-	kIx~
<g/>
13,20	13,20	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
H	H	kA
<g/>
,10	,10	k4
<g/>
H	H	kA
<g/>
2,1	2,1	k4
<g/>
H	H	kA
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
b	b	k?
<g/>
18	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
+	+	kIx~
<g/>
,19	,19	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Molární	molární	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
282,34	282,34	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Salpn	Salpn	k1gInSc1
je	být	k5eAaImIp3nS
běžně	běžně	k6eAd1
používaný	používaný	k2eAgInSc1d1
zkrácený	zkrácený	k2eAgInSc1d1
název	název	k1gInSc1
chelatačního	chelatační	k2eAgNnSc2d1
činidla	činidlo	k1gNnSc2
N	N	kA
<g/>
,	,	kIx,
<g/>
N	N	kA
<g/>
′	′	k?
<g/>
-bis	-bis	k1gInSc1
<g/>
(	(	kIx(
<g/>
salicyliden	salicylidno	k1gNnPc2
<g/>
)	)	kIx)
<g/>
-	-	kIx~
<g/>
1,2	1,2	k4
<g/>
-propandiaminu	-propandiamin	k1gInSc2
(	(	kIx(
<g/>
systematicky	systematicky	k6eAd1
nazývaného	nazývaný	k2eAgInSc2d1
2,2	2,2	k4
<g/>
'	'	kIx"
<g/>
-	-	kIx~
<g/>
{	{	kIx(
<g/>
1,2	1,2	k4
<g/>
-propandiylbis	-propandiylbis	k1gFnPc2
<g/>
[	[	kIx(
<g/>
nitrilo	nitrít	k5eAaPmAgNnS,k5eAaBmAgNnS,k5eAaImAgNnS
<g/>
(	(	kIx(
<g/>
E	E	kA
<g/>
)	)	kIx)
<g/>
methylyliden	methylylidna	k1gFnPc2
<g/>
]	]	kIx)
<g/>
}	}	kIx)
<g/>
difenol	difenol	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
jako	jako	k9
aditivum	aditivum	k1gNnSc1
do	do	k7c2
motorových	motorový	k2eAgInPc2d1
olejů	olej	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Strukturu	struktura	k1gFnSc4
čistého	čistý	k2eAgInSc2d1
salpnu	salpnout	k5eAaPmIp1nS
je	on	k3xPp3gMnPc4
možné	možný	k2eAgNnSc1d1
popsat	popsat	k5eAaPmF
jako	jako	k9
salen	salen	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c4
jehož	jehož	k3xOyRp3gInSc4
ethylenový	ethylenový	k2eAgInSc4d1
můstek	můstek	k1gInSc4
je	být	k5eAaImIp3nS
navázána	navázán	k2eAgFnSc1d1
methylová	methylový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Podobně	podobně	k6eAd1
jako	jako	k9
salen	salen	k1gInSc1
často	často	k6eAd1
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
konjugovanou	konjugovaný	k2eAgFnSc4d1
zásadu	zásada	k1gFnSc4
salpn	salpn	k1gInSc1
<g/>
2	#num#	k4
<g/>
−	−	k?
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
vzniká	vznikat	k5eAaImIp3nS
odtržením	odtržení	k1gNnSc7
dvou	dva	k4xCgInPc2
protonů	proton	k1gInPc2
z	z	k7c2
hydroxylové	hydroxylový	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vzorcích	vzorec	k1gInPc6
komplexů	komplex	k1gInPc2
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
anion	anion	k1gInSc1
často	často	k6eAd1
značí	značit	k5eAaImIp3nS
„	„	k?
<g/>
(	(	kIx(
<g/>
salpn	salpn	k1gMnSc1
<g/>
)	)	kIx)
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Zkratka	zkratka	k1gFnSc1
„	„	k?
<g/>
salpn	salpn	k1gInSc1
<g/>
“	“	k?
se	se	k3xPyFc4
také	také	k9
někdy	někdy	k6eAd1
používá	používat	k5eAaImIp3nS
pro	pro	k7c4
strukturní	strukturní	k2eAgInSc4d1
izomer	izomer	k1gInSc4
N	N	kA
<g/>
,	,	kIx,
<g/>
N	N	kA
<g/>
‘	‘	k?
<g/>
-bis	-bis	k1gInSc1
<g/>
(	(	kIx(
<g/>
salicyliden	salicylidno	k1gNnPc2
<g/>
)	)	kIx)
<g/>
-	-	kIx~
<g/>
1,3	1,3	k4
<g/>
-diaminopropan	-diaminopropana	k1gFnPc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
konjugovanou	konjugovaný	k2eAgFnSc4d1
zásadu	zásada	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výroba	výroba	k1gFnSc1
a	a	k8xC
příprava	příprava	k1gFnSc1
</s>
<s>
Salpn	Salpn	k1gInSc1
se	se	k3xPyFc4
vyrábí	vyrábět	k5eAaImIp3nS
kondenzační	kondenzační	k2eAgFnSc7d1
reakcí	reakce	k1gFnSc7
1,2	1,2	k4
<g/>
-diaminopropanu	-diaminopropan	k1gInSc2
se	s	k7c7
salicylaldehydem	salicylaldehyd	k1gInSc7
<g/>
:	:	kIx,
</s>
<s>
2	#num#	k4
<g/>
C	C	kA
<g/>
6	#num#	k4
<g/>
H	H	kA
<g/>
4	#num#	k4
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
CHO	cho	k0
+	+	kIx~
CH	Ch	kA
<g/>
3	#num#	k4
<g/>
CH	Ch	kA
<g/>
(	(	kIx(
<g/>
NH	NH	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
CH	Ch	kA
<g/>
2	#num#	k4
<g/>
NH	NH	kA
<g/>
2	#num#	k4
→	→	k?
[	[	kIx(
<g/>
C	C	kA
<g/>
6	#num#	k4
<g/>
H	H	kA
<g/>
4	#num#	k4
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
CH	Ch	kA
<g/>
]	]	kIx)
<g/>
2	#num#	k4
<g/>
CH	Ch	kA
<g/>
3	#num#	k4
<g/>
CHNCH	CHNCH	kA
<g/>
2	#num#	k4
<g/>
N	N	kA
+	+	kIx~
2H2O	2H2O	k4
</s>
<s>
Použití	použití	k1gNnSc1
</s>
<s>
Salpn	Salpn	k1gInSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
jako	jako	k9
aditivum	aditivum	k1gNnSc4
v	v	k7c6
motorových	motorový	k2eAgInPc6d1
olejích	olej	k1gInPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
zachytává	zachytávat	k5eAaImIp3nS
ionty	ion	k1gInPc4
kovů	kov	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
by	by	kYmCp3nP
znehodnocoavqaly	znehodnocoavqat	k5eAaPmAgInP,k5eAaImAgInP,k5eAaBmAgInP
palivo	palivo	k1gNnSc4
katalyzováním	katalyzování	k1gNnSc7
růzmých	růzmý	k2eAgFnPc2d1
oxidačních	oxidační	k2eAgFnPc2d1
reakcí	reakce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Látky	látka	k1gFnPc1
jako	jako	k8xS,k8xC
je	on	k3xPp3gFnPc4
salpn	salpn	k1gMnSc1
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
s	s	k7c7
kovy	kov	k1gInPc7
stabilní	stabilní	k2eAgFnSc1d1
komplexy	komplex	k1gInPc4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
snižují	snižovat	k5eAaImIp3nP
jejich	jejich	k3xOp3gFnSc4
katalkytickou	katalkytický	k2eAgFnSc4d1
aktivitu	aktivita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
salpn	salpn	k1gNnSc1
tvoří	tvořit	k5eAaImIp3nP
stabilní	stabilní	k2eAgInPc1d1
komplexy	komplex	k1gInPc1
s	s	k7c7
mnoha	mnoho	k4c7
kovy	kov	k1gInPc7
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
mědí	mědit	k5eAaImIp3nP
<g/>
,	,	kIx,
železem	železo	k1gNnSc7
<g/>
,	,	kIx,
chromem	chrom	k1gInSc7
a	a	k8xC
niklem	nikl	k1gInSc7
<g/>
,	,	kIx,
tak	tak	k6eAd1
je	být	k5eAaImIp3nS
oblíbenou	oblíbený	k2eAgFnSc7d1
přísadou	přísada	k1gFnSc7
do	do	k7c2
motorových	motorový	k2eAgInPc2d1
olejů	olej	k1gInPc2
především	především	k9
díky	díky	k7c3
schopnosti	schopnost	k1gFnSc3
zachytávat	zachytávat	k5eAaImF
měď	měď	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měď	měď	k1gFnSc1
má	mít	k5eAaImIp3nS
v	v	k7c6
palivu	palivo	k1gNnSc6
největší	veliký	k2eAgFnSc4d3
katalytickou	katalytický	k2eAgFnSc4d1
aktivitu	aktivita	k1gFnSc4
a	a	k8xC
salpn	salpn	k1gNnSc4
s	s	k7c7
ní	on	k3xPp3gFnSc7
tvoří	tvořit	k5eAaImIp3nP
velmi	velmi	k6eAd1
stabilní	stabilní	k2eAgInSc4d1
komplex	komplex	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Salpn	Salpn	k1gInSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
častěji	často	k6eAd2
než	než	k8xS
salen	salen	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
díky	díky	k7c3
lepší	dobrý	k2eAgFnSc3d2
rozpustnosti	rozpustnost	k1gFnSc3
v	v	k7c6
nepolárních	polární	k2eNgNnPc6d1
rozpouštědlech	rozpouštědlo	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Salpn	Salpna	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
K.	K.	kA
Rajender	Rajender	k1gInSc4
Reddy	Redda	k1gFnSc2
<g/>
,	,	kIx,
M.	M.	kA
V.	V.	kA
Rajasekharan	Rajasekharana	k1gFnPc2
<g/>
,	,	kIx,
and	and	k?
J.	J.	kA
<g/>
-	-	kIx~
<g/>
P.	P.	kA
Tuchagues	Tuchagues	k1gMnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Synthesis	Synthesis	k1gInSc1
<g/>
,	,	kIx,
Structure	Structur	k1gMnSc5
<g/>
,	,	kIx,
and	and	k?
Magnetic	Magnetice	k1gFnPc2
Properties	Properties	k1gMnSc1
of	of	k?
Mn	Mn	k1gMnSc1
<g/>
(	(	kIx(
<g/>
salpn	salpn	k1gMnSc1
<g/>
)	)	kIx)
<g/>
N	N	kA
<g/>
3	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
<g />
.	.	kIx.
</s>
<s hack="1">
Helical	Helical	k1gMnSc2
Polymer	polymer	k1gInSc4
<g/>
,	,	kIx,
and	and	k?
Fe	Fe	k1gMnSc1
<g/>
(	(	kIx(
<g/>
salpn	salpn	k1gMnSc1
<g/>
)	)	kIx)
<g/>
N	N	kA
<g/>
3	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
Ferromagnetically	Ferromagneticalla	k1gFnSc2
Coupled	Coupled	k1gMnSc1
Dimer	Dimer	k1gMnSc1
<g/>
(	(	kIx(
<g/>
salpnH	salpnH	k?
<g/>
2	#num#	k4
=	=	kIx~
N	N	kA
<g/>
,	,	kIx,
<g/>
N	N	kA
<g/>
‘	‘	k?
<g/>
-bis	-bis	k1gInSc1
<g/>
(	(	kIx(
<g/>
Salicylidene	Salicyliden	k1gInSc5
<g/>
)	)	kIx)
<g/>
-	-	kIx~
<g/>
1,3	1,3	k4
<g/>
-diaminopropane	-diaminopropanout	k5eAaPmIp3nS
<g/>
)	)	kIx)
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inorganic	Inorganice	k1gFnPc2
Chemistry	Chemistr	k1gMnPc4
<g/>
,	,	kIx,
volume	volum	k1gInSc5
37	#num#	k4
<g/>
,	,	kIx,
issue	issue	k1gFnSc1
23	#num#	k4
<g/>
,	,	kIx,
pages	pages	k1gInSc1
5978	#num#	k4
<g/>
–	–	k?
<g/>
5982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
ic	ic	k?
<g/>
971592	#num#	k4
<g/>
yJe	yJe	k?
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
DOI	DOI	kA
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
D.	D.	kA
A.	A.	kA
Evans	Evansa	k1gFnPc2
<g/>
;	;	kIx,
S.	S.	kA
J.	J.	kA
Miller	Miller	k1gMnSc1
<g/>
;	;	kIx,
T.	T.	kA
Lectka	Lectka	k1gMnSc1
<g/>
;	;	kIx,
P.	P.	kA
von	von	k1gInSc1
Matt	Matt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chiral	Chiral	k1gMnSc1
Bis	Bis	k1gMnSc1
<g/>
(	(	kIx(
<g/>
oxazoline	oxazolin	k1gInSc5
<g/>
)	)	kIx)
<g/>
copper	copper	k1gMnSc1
<g/>
(	(	kIx(
<g/>
II	II	kA
<g/>
)	)	kIx)
Complexes	Complexes	k1gMnSc1
as	as	k9
Lewis	Lewis	k1gInSc4
Acid	Acid	k1gInSc1
Catalysts	Catalystsa	k1gFnPc2
for	forum	k1gNnPc2
Enantioselective	Enantioselectiv	k1gInSc5
Diels-Alder	Diels-Alder	k1gMnSc1
Reaction	Reaction	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
American	American	k1gInSc4
Chemical	Chemical	k1gMnSc2
Society	societa	k1gFnSc2
<g/>
.	.	kIx.
1999	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
7559	#num#	k4
<g/>
–	–	k?
<g/>
7573	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
