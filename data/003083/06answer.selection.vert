<s>
Do	do	k7c2	do
čeledě	čeleď	k1gFnSc2	čeleď
kakaduovití	kakaduovití	k1gMnPc1	kakaduovití
jsou	být	k5eAaImIp3nP	být
řazeni	řazen	k2eAgMnPc1d1	řazen
papoušci	papoušek	k1gMnPc1	papoušek
obývající	obývající	k2eAgFnSc4d1	obývající
Austrálii	Austrálie	k1gFnSc4	Austrálie
<g/>
,	,	kIx,	,
Indonésii	Indonésie	k1gFnSc4	Indonésie
<g/>
,	,	kIx,	,
Oceánii	Oceánie	k1gFnSc4	Oceánie
a	a	k8xC	a
Filipíny	Filipíny	k1gFnPc4	Filipíny
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
typickým	typický	k2eAgInSc7d1	typický
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
chocholka	chocholka	k1gFnSc1	chocholka
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
.	.	kIx.	.
</s>
