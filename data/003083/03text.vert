<s>
Do	do	k7c2	do
čeledě	čeleď	k1gFnSc2	čeleď
kakaduovití	kakaduovití	k1gMnPc1	kakaduovití
jsou	být	k5eAaImIp3nP	být
řazeni	řazen	k2eAgMnPc1d1	řazen
papoušci	papoušek	k1gMnPc1	papoušek
obývající	obývající	k2eAgFnSc4d1	obývající
Austrálii	Austrálie	k1gFnSc4	Austrálie
<g/>
,	,	kIx,	,
Indonésii	Indonésie	k1gFnSc4	Indonésie
<g/>
,	,	kIx,	,
Oceánii	Oceánie	k1gFnSc4	Oceánie
a	a	k8xC	a
Filipíny	Filipíny	k1gFnPc4	Filipíny
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
typickým	typický	k2eAgInSc7d1	typický
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
chocholka	chocholka	k1gFnSc1	chocholka
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenším	malý	k2eAgMnSc7d3	nejmenší
zástupcem	zástupce	k1gMnSc7	zástupce
je	být	k5eAaImIp3nS	být
korela	korela	k1gFnSc1	korela
chocholatá	chocholatý	k2eAgFnSc1d1	chocholatá
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
kakadu	kakadu	k1gMnPc3	kakadu
je	být	k5eAaImIp3nS	být
kakadu	kakadu	k1gMnSc1	kakadu
arový	arový	k2eAgMnSc1d1	arový
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
hmotnosti	hmotnost	k1gFnSc3	hmotnost
až	až	k9	až
1	[number]	k4	1
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
kakaduovití	kakaduovití	k1gMnPc1	kakaduovití
kromě	kromě	k7c2	kromě
korely	korela	k1gFnSc2	korela
jsou	být	k5eAaImIp3nP	být
zapsáni	zapsán	k2eAgMnPc1d1	zapsán
v	v	k7c6	v
příloze	příloha	k1gFnSc6	příloha
CITES	CITES	kA	CITES
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
je	být	k5eAaImIp3nS	být
chráněna	chráněn	k2eAgFnSc1d1	chráněna
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
vyhubení	vyhubení	k1gNnSc2	vyhubení
nebo	nebo	k8xC	nebo
zranitelní	zranitelný	k2eAgMnPc1d1	zranitelný
<g/>
.	.	kIx.	.
</s>
<s>
Jediná	jediný	k2eAgFnSc1d1	jediná
skupina	skupina	k1gFnSc1	skupina
papoušků	papoušek	k1gMnPc2	papoušek
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyměnila	vyměnit	k5eAaPmAgFnS	vyměnit
pestré	pestrý	k2eAgFnPc4d1	pestrá
barvy	barva	k1gFnPc4	barva
za	za	k7c4	za
bílou	bílý	k2eAgFnSc4d1	bílá
<g/>
,	,	kIx,	,
růžovou	růžový	k2eAgFnSc4d1	růžová
a	a	k8xC	a
černou	černý	k2eAgFnSc4d1	černá
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
ztratili	ztratit	k5eAaPmAgMnP	ztratit
své	svůj	k3xOyFgNnSc4	svůj
kouzlo	kouzlo	k1gNnSc4	kouzlo
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
kakaduové	kakadu	k1gMnPc1	kakadu
mají	mít	k5eAaImIp3nP	mít
krásnou	krásný	k2eAgFnSc4d1	krásná
ozdobu	ozdoba	k1gFnSc4	ozdoba
hlavy	hlava	k1gFnSc2	hlava
-	-	kIx~	-
pohyblivou	pohyblivý	k2eAgFnSc4d1	pohyblivá
chocholku	chocholka	k1gFnSc4	chocholka
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jejichž	jejichž	k3xOyRp3gInPc2	jejichž
pohybů	pohyb	k1gInPc2	pohyb
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
nálada	nálada	k1gFnSc1	nálada
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
papoušků	papoušek	k1gMnPc2	papoušek
nejdéle	dlouho	k6eAd3	dlouho
<g/>
,	,	kIx,	,
kakadu	kakadu	k1gMnSc1	kakadu
žlutočečelatý	žlutočečelatý	k2eAgMnSc1d1	žlutočečelatý
se	se	k3xPyFc4	se
dožívá	dožívat	k5eAaImIp3nS	dožívat
až	až	k9	až
119	[number]	k4	119
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Kakaduové	kakadu	k1gMnPc1	kakadu
jsou	být	k5eAaImIp3nP	být
ptáci	pták	k1gMnPc1	pták
vysloveně	vysloveně	k6eAd1	vysloveně
společenští	společenský	k2eAgMnPc1d1	společenský
<g/>
,	,	kIx,	,
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
hejna	hejno	k1gNnPc4	hejno
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
rozpadají	rozpadat	k5eAaPmIp3nP	rozpadat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
době	doba	k1gFnSc6	doba
hnízdění	hnízdění	k1gNnSc2	hnízdění
<g/>
.	.	kIx.	.
</s>
<s>
Čeleď	čeleď	k1gFnSc1	čeleď
kakaduovití	kakaduovitý	k2eAgMnPc1d1	kakaduovitý
se	se	k3xPyFc4	se
rozpadá	rozpadat	k5eAaPmIp3nS	rozpadat
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
podčeledi	podčeleď	k1gFnPc4	podčeleď
-	-	kIx~	-
kakaduové	kakadu	k1gMnPc1	kakadu
(	(	kIx(	(
<g/>
Cacatuinae	Cacatuinae	k1gInSc1	Cacatuinae
<g/>
)	)	kIx)	)
a	a	k8xC	a
korely	korela	k1gFnSc2	korela
(	(	kIx(	(
<g/>
Nymphicinae	Nymphicinae	k1gFnSc1	Nymphicinae
<g/>
)	)	kIx)	)
:	:	kIx,	:
rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Callocephalon	Callocephalon	k1gInSc1	Callocephalon
kakadu	kakadu	k1gMnSc1	kakadu
přilbový	přilbový	k2eAgMnSc1d1	přilbový
(	(	kIx(	(
<g/>
Callocephalon	Callocephalon	k1gInSc1	Callocephalon
fimbriatum	fimbriatum	k1gNnSc1	fimbriatum
<g/>
)	)	kIx)	)
rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Probosciger	Probosciger	k1gMnSc1	Probosciger
kakadu	kakadu	k1gMnSc1	kakadu
arový	arový	k2eAgMnSc1d1	arový
(	(	kIx(	(
<g/>
Probosciger	Probosciger	k1gMnSc1	Probosciger
aterrimus	aterrimus	k1gMnSc1	aterrimus
<g/>
)	)	kIx)	)
rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Eolophus	Eolophus	k1gInSc1	Eolophus
<g />
.	.	kIx.	.
</s>
<s>
kakadu	kakadu	k1gMnSc1	kakadu
růžový	růžový	k2eAgMnSc1d1	růžový
(	(	kIx(	(
<g/>
Eolophus	Eolophus	k1gMnSc1	Eolophus
roseicapillus	roseicapillus	k1gMnSc1	roseicapillus
<g/>
)	)	kIx)	)
rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Cacatua	Cacatua	k1gFnSc1	Cacatua
kakadu	kakadu	k1gMnSc1	kakadu
žlutolící	žlutolící	k2eAgMnSc1d1	žlutolící
(	(	kIx(	(
<g/>
Cacatua	Cacatua	k1gMnSc1	Cacatua
sulphurea	sulphurea	k1gMnSc1	sulphurea
<g/>
)	)	kIx)	)
kakadu	kakadu	k1gMnSc1	kakadu
žlutočečelatý	žlutočečelatý	k2eAgMnSc1d1	žlutočečelatý
(	(	kIx(	(
<g/>
Cacatua	Cacatu	k2eAgFnSc1d1	Cacatua
galerita	galerita	k1gFnSc1	galerita
<g/>
)	)	kIx)	)
kakadu	kakadu	k1gMnSc1	kakadu
brýlový	brýlový	k2eAgMnSc1d1	brýlový
(	(	kIx(	(
<g/>
Cacatua	Cacatua	k1gMnSc1	Cacatua
ophthalmica	ophthalmica	k1gMnSc1	ophthalmica
<g/>
)	)	kIx)	)
kakadu	kakadu	k1gMnSc1	kakadu
molucký	molucký	k2eAgMnSc1d1	molucký
(	(	kIx(	(
<g/>
Cacatua	Cacatu	k2eAgFnSc1d1	Cacatua
moluccensis	moluccensis	k1gFnSc1	moluccensis
<g/>
)	)	kIx)	)
kakadu	kakadu	k1gMnSc1	kakadu
bílý	bílý	k1gMnSc1	bílý
(	(	kIx(	(
<g/>
Cacatua	Cacatua	k1gMnSc1	Cacatua
alba	album	k1gNnSc2	album
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
kakadu	kakadu	k1gMnSc1	kakadu
filipínský	filipínský	k2eAgMnSc1d1	filipínský
(	(	kIx(	(
<g/>
Cacatua	Cacatu	k2eAgFnSc1d1	Cacatua
haematuropygia	haematuropygia	k1gFnSc1	haematuropygia
<g/>
)	)	kIx)	)
kakadu	kakadu	k1gMnSc1	kakadu
Goffinův	Goffinův	k2eAgMnSc1d1	Goffinův
(	(	kIx(	(
<g/>
Cacatua	Cacatua	k1gMnSc1	Cacatua
goffini	goffin	k2eAgMnPc1d1	goffin
<g/>
)	)	kIx)	)
kakadu	kakadu	k1gMnSc1	kakadu
naholící	naholící	k2eAgMnSc1d1	naholící
(	(	kIx(	(
<g/>
Cacatua	Cacatua	k1gMnSc1	Cacatua
sanguinea	sanguinea	k1gMnSc1	sanguinea
<g/>
)	)	kIx)	)
kakadu	kakadu	k1gMnSc1	kakadu
šalamounský	šalamounský	k2eAgMnSc1d1	šalamounský
(	(	kIx(	(
<g/>
Cacatua	Cacatua	k1gFnSc1	Cacatua
dukorps	dukorps	k1gInSc1	dukorps
<g/>
)	)	kIx)	)
kakadu	kakadu	k1gMnSc1	kakadu
tenkozobý	tenkozobý	k2eAgMnSc1d1	tenkozobý
(	(	kIx(	(
<g/>
Cacatua	Cacatu	k2eAgFnSc1d1	Cacatua
tenuirostris	tenuirostris	k1gFnSc1	tenuirostris
<g/>
)	)	kIx)	)
kakadu	kakadu	k1gMnSc1	kakadu
inka	inka	k1gMnSc1	inka
(	(	kIx(	(
<g/>
Cacatua	Cacatua	k1gMnSc1	Cacatua
leadbeateri	leadbeater	k1gFnSc2	leadbeater
<g/>
)	)	kIx)	)
rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Calyptorhynchus	Calyptorhynchus	k1gMnSc1	Calyptorhynchus
kakadu	kakadu	k1gMnSc1	kakadu
černý	černý	k1gMnSc1	černý
(	(	kIx(	(
<g/>
Calyptorhynchus	Calyptorhynchus	k1gMnSc1	Calyptorhynchus
funereus	funereus	k1gMnSc1	funereus
<g/>
)	)	kIx)	)
kakadu	kakadu	k1gMnSc1	kakadu
havraní	havraní	k2eAgMnSc1d1	havraní
(	(	kIx(	(
<g/>
Calyptorhynchus	Calyptorhynchus	k1gMnSc1	Calyptorhynchus
magnificus	magnificus	k1gMnSc1	magnificus
<g/>
)	)	kIx)	)
kakadu	kakadu	k1gMnSc1	kakadu
hnědohlavý	hnědohlavý	k2eAgMnSc1d1	hnědohlavý
(	(	kIx(	(
<g/>
Calyptorhynchus	Calyptorhynchus	k1gMnSc1	Calyptorhynchus
lathami	latha	k1gFnPc7	latha
<g/>
)	)	kIx)	)
kakadu	kakadu	k1gMnSc1	kakadu
palmový	palmový	k2eAgMnSc1d1	palmový
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Probosciger	Probosciger	k1gMnSc1	Probosciger
aterrimus	aterrimus	k1gMnSc1	aterrimus
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Jediným	jediný	k2eAgMnSc7d1	jediný
zástupcem	zástupce	k1gMnSc7	zástupce
je	být	k5eAaImIp3nS	být
korela	korela	k1gFnSc1	korela
chocholatá	chocholatý	k2eAgFnSc1d1	chocholatá
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
běžný	běžný	k2eAgMnSc1d1	běžný
<g/>
,	,	kIx,	,
travními	travní	k2eAgNnPc7d1	travní
semeny	semeno	k1gNnPc7	semeno
živící	živící	k2eAgFnSc1d1	živící
se	se	k3xPyFc4	se
pták	pták	k1gMnSc1	pták
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnSc1d1	žijící
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
populární	populární	k2eAgMnSc1d1	populární
klecový	klecový	k2eAgMnSc1d1	klecový
pták	pták	k1gMnSc1	pták
<g/>
.	.	kIx.	.
rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Nymphicus	Nymphicus	k1gInSc1	Nymphicus
korela	korela	k1gFnSc1	korela
chocholatá	chocholatý	k2eAgFnSc1d1	chocholatá
(	(	kIx(	(
<g/>
Nymphicus	Nymphicus	k1gMnSc1	Nymphicus
hollandicus	hollandicus	k1gMnSc1	hollandicus
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kakaduovití	Kakaduovitý	k2eAgMnPc1d1	Kakaduovitý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
City	City	k1gFnSc7	City
Parrots	Parrotsa	k1gFnPc2	Parrotsa
kakaduovití	kakaduovití	k1gMnPc1	kakaduovití
na	na	k7c6	na
Biolibu	Biolib	k1gInSc6	Biolib
</s>
