Obec	obec	k1gFnSc1
má	mít	k5eAaImIp3nS
jméno	jméno	k1gNnSc4
zřejmě	zřejmě	k6eAd1
podle	podle	k7c2
zakladatele	zakladatel	k1gMnSc2
Heralta	Heralt	k1gMnSc2
či	či	k8xC
Heralda	Herald	k1gMnSc2
<g/>
,	,	kIx,
čemuž	což	k3yRnSc3,k3yQnSc3
napovídá	napovídat	k5eAaBmIp3nS
německý	německý	k2eAgInSc1d1
název	název	k1gInSc1
Herautz	Herautza	k1gFnPc2
a	a	k8xC
dřívější	dřívější	k2eAgNnSc4d1
české	český	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
Heraltice	Heraltice	k1gFnSc2
<g/>
.	.	kIx.
