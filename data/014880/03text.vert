<s>
Heroltice	Heroltice	k1gFnSc1
(	(	kIx(
<g/>
Štíty	štít	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Heroltice	Heroltika	k1gFnSc3
Pohled	pohled	k1gInSc4
na	na	k7c4
obec	obec	k1gFnSc4
HerolticeLokalita	HerolticeLokalita	k1gFnSc1
Charakter	charakter	k1gInSc1
</s>
<s>
vesnice	vesnice	k1gFnSc1
Obec	obec	k1gFnSc1
</s>
<s>
Štíty	štít	k1gInPc1
Okres	okres	k1gInSc1
</s>
<s>
Šumperk	Šumperk	k1gInSc1
Kraj	kraj	k1gInSc1
</s>
<s>
Olomoucký	olomoucký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Morava	Morava	k1gFnSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
59	#num#	k4
<g/>
′	′	k?
<g/>
18	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
44	#num#	k4
<g/>
′	′	k?
<g/>
15	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
95	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Katastrální	katastrální	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Heroltice	Heroltice	k1gFnPc1
u	u	k7c2
Štítů	štít	k1gInPc2
(	(	kIx(
<g/>
6,2	6,2	k4
km²	km²	k?
<g/>
)	)	kIx)
PSČ	PSČ	kA
</s>
<s>
789	#num#	k4
91	#num#	k4
Počet	počet	k1gInSc1
domů	domů	k6eAd1
</s>
<s>
66	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Heroltice	Heroltice	k1gFnSc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Web	web	k1gInSc4
</s>
<s>
www.heroltice.eu	www.heroltice.eu	k6eAd1
Kód	kód	k1gInSc1
části	část	k1gFnSc2
obce	obec	k1gFnSc2
</s>
<s>
38431	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Heroltice	Heroltice	k1gFnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Herautz	Herautz	k1gInSc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
vesnice	vesnice	k1gFnPc4
v	v	k7c6
okrese	okres	k1gInSc6
Šumperk	Šumperk	k1gInSc1
<g/>
,	,	kIx,
součást	součást	k1gFnSc1
města	město	k1gNnSc2
Štíty	štít	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	s	k7c7
zhruba	zhruba	k6eAd1
3,5	3,5	k4
km	km	kA
ssz	ssz	k?
<g/>
.	.	kIx.
od	od	k7c2
Štítů	štít	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídlo	sídlo	k1gNnSc1
je	být	k5eAaImIp3nS
uličního	uliční	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
,	,	kIx,
roztažené	roztažený	k2eAgNnSc1d1
podél	podél	k7c2
Heroltického	Heroltický	k2eAgInSc2d1
potoka	potok	k1gInSc2
<g/>
,	,	kIx,
drobného	drobný	k2eAgInSc2d1
toku	tok	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
stéká	stékat	k5eAaImIp3nS
z	z	k7c2
východního	východní	k2eAgInSc2d1
okraje	okraj	k1gInSc2
Orlických	orlický	k2eAgFnPc2d1
hor	hora	k1gFnPc2
do	do	k7c2
Kladské	kladský	k2eAgFnSc2d1
kotliny	kotlina	k1gFnSc2
a	a	k8xC
pod	pod	k7c7
Herolticemi	Heroltice	k1gFnPc7
zprava	zprava	k6eAd1
ústí	ústit	k5eAaImIp3nP
do	do	k7c2
řeky	řeka	k1gFnSc2
Březná	Březný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vesnička	vesnička	k1gFnSc1
Heroltice	Heroltice	k1gFnSc1
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
v	v	k7c6
severním	severní	k2eAgInSc6d1
cípu	cíp	k1gInSc6
Olomouckého	olomoucký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
nedaleko	nedaleko	k7c2
česko-polských	česko-polský	k2eAgFnPc2d1
hranic	hranice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malebné	malebný	k2eAgNnSc1d1
údolí	údolí	k1gNnSc3
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
přirozenou	přirozený	k2eAgFnSc7d1
hranicí	hranice	k1gFnSc7
mezi	mezi	k7c7
Jeseníky	Jeseník	k1gInPc7
a	a	k8xC
Orlickými	orlický	k2eAgFnPc7d1
horami	hora	k1gFnPc7
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
450	#num#	k4
<g/>
–	–	k?
<g/>
750	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Vesnička	vesnička	k1gFnSc1
správně	správně	k6eAd1
spadá	spadat	k5eAaImIp3nS,k5eAaPmIp3nS
pod	pod	k7c4
město	město	k1gNnSc4
Štíty	štít	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Historický	historický	k2eAgInSc1d1
znak	znak	k1gInSc1
Heroltic	Heroltice	k1gFnPc2
</s>
<s>
Obec	obec	k1gFnSc1
má	mít	k5eAaImIp3nS
jméno	jméno	k1gNnSc4
zřejmě	zřejmě	k6eAd1
podle	podle	k7c2
zakladatele	zakladatel	k1gMnSc2
Heralta	Heralt	k1gMnSc2
či	či	k8xC
Heralda	Herald	k1gMnSc2
<g/>
,	,	kIx,
čemuž	což	k3yRnSc3,k3yQnSc3
napovídá	napovídat	k5eAaBmIp3nS
německý	německý	k2eAgInSc1d1
název	název	k1gInSc1
Herautz	Herautza	k1gFnPc2
a	a	k8xC
dřívější	dřívější	k2eAgNnSc4d1
české	český	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
Heraltice	Heraltice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heroltice	Heroltika	k1gFnSc6
se	se	k3xPyFc4
připomínají	připomínat	k5eAaImIp3nP
již	již	k6eAd1
roku	rok	k1gInSc2
1278	#num#	k4
u	u	k7c2
tehdejšího	tehdejší	k2eAgInSc2d1
Šilperka	Šilperka	k1gFnSc1
a	a	k8xC
byly	být	k5eAaImAgFnP
někdy	někdy	k6eAd1
označovány	označovat	k5eAaImNgInP
jako	jako	k9
Velké	velký	k2eAgInPc1d1
nebo	nebo	k8xC
též	též	k9
Dolní	dolní	k2eAgFnPc1d1
Heroltice	Heroltice	k1gFnPc1
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Malých	Malých	k2eAgFnPc2d1
či	či	k8xC
Horních	horní	k2eAgFnPc2d1
Heroltic	Heroltice	k1gFnPc2
<g/>
,	,	kIx,
tj.	tj.	kA
pozdější	pozdní	k2eAgFnSc2d2
Červené	Červené	k2eAgFnSc2d1
Vody	voda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1623	#num#	k4
se	se	k3xPyFc4
Heroltice	Heroltice	k1gFnPc1
dostaly	dostat	k5eAaPmAgFnP
se	s	k7c7
šilperským	šilperský	k2eAgNnSc7d1
panstvím	panství	k1gNnSc7
pod	pod	k7c4
patrimoniální	patrimoniální	k2eAgFnSc4d1
správu	správa	k1gFnSc4
v	v	k7c6
Rudě	ruda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patřily	patřit	k5eAaImAgInP
k	k	k7c3
největším	veliký	k2eAgFnPc3d3
vsím	ves	k1gFnPc3
tohoto	tento	k3xDgNnSc2
panství	panství	k1gNnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
podle	podle	k7c2
lánového	lánový	k2eAgInSc2d1
rejstříku	rejstřík	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1677	#num#	k4
měly	mít	k5eAaImAgInP
49	#num#	k4
usedlých	usedlý	k2eAgInPc2d1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1834	#num#	k4
dokonce	dokonce	k9
již	již	k9
156	#num#	k4
domů	dům	k1gInPc2
a	a	k8xC
1026	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Škola	škola	k1gFnSc1
zde	zde	k6eAd1
existovala	existovat	k5eAaImAgFnS
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1828	#num#	k4
a	a	k8xC
roku	rok	k1gInSc2
1854	#num#	k4
tu	ten	k3xDgFnSc4
byla	být	k5eAaImAgFnS
zřízena	zřízen	k2eAgFnSc1d1
kuracie	kuracie	k1gFnSc1
při	při	k7c6
zdejším	zdejší	k2eAgInSc6d1
kostele	kostel	k1gInSc6
<g/>
,	,	kIx,
původně	původně	k6eAd1
kapli	kaple	k1gFnSc4
sv.	sv.	kA
Jana	Jan	k1gMnSc2
Křtitele	křtitel	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1718	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1848	#num#	k4
připadly	připadnout	k5eAaPmAgFnP
Heroltice	Heroltice	k1gFnPc1
k	k	k7c3
soudnímu	soudní	k2eAgInSc3d1
okresu	okres	k1gInSc3
Šilperk	Šilperk	k1gInSc4
a	a	k8xC
politickému	politický	k2eAgInSc3d1
okresu	okres	k1gInSc3
Zábřeh	Zábřeh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
sousedních	sousední	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
Červená	červený	k2eAgFnSc1d1
a	a	k8xC
Bílá	bílý	k2eAgFnSc1d1
Voda	voda	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
zaznamenaly	zaznamenat	k5eAaPmAgFnP
velký	velký	k2eAgInSc4d1
rozmach	rozmach	k1gInSc4
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
rozvojem	rozvoj	k1gInSc7
textilního	textilní	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
<g/>
,	,	kIx,
zůstaly	zůstat	k5eAaPmAgFnP
téměř	téměř	k6eAd1
beze	beze	k7c2
zbytku	zbytek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
vesnice	vesnice	k1gFnSc2
soustavně	soustavně	k6eAd1
upadala	upadat	k5eAaImAgFnS,k5eAaPmAgFnS
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
platilo	platit	k5eAaImAgNnS
o	o	k7c6
zástavbě	zástavba	k1gFnSc6
i	i	k8xC
zemědělství	zemědělství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozemky	pozemka	k1gFnPc4
heroltického	heroltický	k2eAgInSc2d1
katastru	katastr	k1gInSc2
převzal	převzít	k5eAaPmAgInS
nakonec	nakonec	k6eAd1
k	k	k7c3
obhospodařování	obhospodařování	k1gNnSc3
Státní	státní	k2eAgInSc4d1
statek	statek	k1gInSc4
ve	v	k7c6
Štítech	štít	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1976	#num#	k4
byla	být	k5eAaImAgFnS
obec	obec	k1gFnSc1
Heroltice	Heroltice	k1gFnSc1
připojena	připojen	k2eAgFnSc1d1
ke	k	k7c3
Štítům	štít	k1gInPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kulturní	kulturní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
V	v	k7c6
katastru	katastr	k1gInSc6
obce	obec	k1gFnSc2
jsou	být	k5eAaImIp3nP
evidovány	evidovat	k5eAaImNgFnP
tyto	tento	k3xDgFnPc1
kulturní	kulturní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dům	dům	k1gInSc1
čp.	čp.	k?
158	#num#	k4
(	(	kIx(
<g/>
bývalá	bývalý	k2eAgFnSc1d1
fara	fara	k1gFnSc1
<g/>
,	,	kIx,
severně	severně	k6eAd1
od	od	k7c2
kostela	kostel	k1gInSc2
<g/>
)	)	kIx)
-	-	kIx~
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
v	v	k7c6
seznamu	seznam	k1gInSc6
chráněných	chráněný	k2eAgFnPc2d1
památek	památka	k1gFnPc2
uveden	uvést	k5eAaPmNgInS
jako	jako	k9
doklad	doklad	k1gInSc1
lidové	lidový	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
jeden	jeden	k4xCgMnSc1
empírový	empírový	k2eAgInSc1d1
dům	dům	k1gInSc1
č.	č.	k?
p.	p.	k?
158	#num#	k4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
dříve	dříve	k6eAd2
sloužil	sloužit	k5eAaImAgMnS
jako	jako	k8xS,k8xC
fara	fara	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
postaven	postavit	k5eAaPmNgMnS
v	v	k7c6
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Mimo	mimo	k7c4
chráněné	chráněný	k2eAgFnPc4d1
památky	památka	k1gFnPc4
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
např.	např.	kA
<g/>
:	:	kIx,
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Jana	Jan	k1gMnSc2
Křtitele	křtitel	k1gMnSc2
-	-	kIx~
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
Křtitele	křtitel	k1gMnSc2
v	v	k7c6
Herolticích	Heroltice	k1gFnPc6
je	být	k5eAaImIp3nS
původně	původně	k6eAd1
barokní	barokní	k2eAgFnSc7d1
kaplí	kaple	k1gFnSc7
z	z	k7c2
roku	rok	k1gInSc2
1718	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stojí	stát	k5eAaImIp3nS
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
537	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
těsně	těsně	k6eAd1
u	u	k7c2
okraje	okraj	k1gInSc2
silnice	silnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2007	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
kostele	kostel	k1gInSc6
sv.	sv.	kA
Jana	Jan	k1gMnSc2
Křtitele	křtitel	k1gMnSc2
opět	opět	k6eAd1
provádí	provádět	k5eAaImIp3nS
nedělní	nedělní	k2eAgNnSc4d1
polední	polední	k2eAgNnSc4d1
zvonění	zvonění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostel	kostel	k1gInSc1
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
chráněnou	chráněný	k2eAgFnSc7d1
kulturní	kulturní	k2eAgFnSc7d1
památkou	památka	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Jana	Jan	k1gMnSc2
Křtitele	křtitel	k1gMnSc2
(	(	kIx(
<g/>
Heroltice	Heroltice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Kříž	Kříž	k1gMnSc1
u	u	k7c2
domu	dům	k1gInSc2
čp.	čp.	k?
158	#num#	k4
</s>
<s>
Pískovcový	pískovcový	k2eAgInSc1d1
kříž	kříž	k1gInSc1
s	s	k7c7
korpusem	korpus	k1gInSc7
Krista	Kristus	k1gMnSc2
u	u	k7c2
kostela	kostel	k1gInSc2
v	v	k7c6
Herolticích	Heroltice	k1gFnPc6
</s>
<s>
Turistika	turistika	k1gFnSc1
</s>
<s>
Sídlo	sídlo	k1gNnSc1
Heroltická	Heroltický	k2eAgFnSc1d1
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
</s>
<s>
Rekreační	rekreační	k2eAgInSc4d1
a	a	k8xC
sportovní	sportovní	k2eAgInSc4d1
areál	areál	k1gInSc4
Heroltická	Heroltický	k2eAgFnSc1d1
s.	s.	k?
r.	r.	kA
o.	o.	k?
na	na	k7c6
rozhraní	rozhraní	k1gNnSc6
Orlických	orlický	k2eAgFnPc2d1
hor	hora	k1gFnPc2
a	a	k8xC
Kralického	kralický	k2eAgInSc2d1
Sněžníku	Sněžník	k1gInSc2
umožňuje	umožňovat	k5eAaImIp3nS
ubytování	ubytování	k1gNnSc4
v	v	k7c6
pokojích	pokoj	k1gInPc6
i	i	k8xC
v	v	k7c6
chatkách	chatka	k1gFnPc6
a	a	k8xC
maringotkách	maringotka	k1gFnPc6
v	v	k7c6
klidné	klidný	k2eAgFnSc6d1
části	část	k1gFnSc6
zahrady	zahrada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
postavení	postavení	k1gNnSc2
karavanů	karavan	k1gInPc2
na	na	k7c6
zahradě	zahrada	k1gFnSc6
areálu	areál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karavany	karavan	k1gInPc7
lze	lze	k6eAd1
napojit	napojit	k5eAaPmF
na	na	k7c4
elektrickou	elektrický	k2eAgFnSc4d1
energii	energie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
každém	každý	k3xTgInSc6
období	období	k1gNnSc6
mohou	moct	k5eAaImIp3nP
lidé	člověk	k1gMnPc1
čerpat	čerpat	k5eAaImF
z	z	k7c2
mnoha	mnoho	k4c2
doprovodných	doprovodný	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
<g/>
,	,	kIx,
využít	využít	k5eAaPmF
stravování	stravování	k1gNnSc4
a	a	k8xC
vyrazit	vyrazit	k5eAaPmF
do	do	k7c2
malebného	malebný	k2eAgNnSc2d1
okolí	okolí	k1gNnSc2
<g/>
,	,	kIx,
s	s	k7c7
příležitostmi	příležitost	k1gFnPc7
k	k	k7c3
zimním	zimní	k2eAgInPc3d1
i	i	k8xC
letním	letní	k2eAgInPc3d1
sportům	sport	k1gInPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kulturní	kulturní	k2eAgFnSc1d1
akce	akce	k1gFnSc1
</s>
<s>
Výstava	výstava	k1gFnSc1
jiřin	jiřina	k1gFnPc2
-	-	kIx~
již	již	k6eAd1
tradičně	tradičně	k6eAd1
začátkem	začátek	k1gInSc7
září	zářit	k5eAaImIp3nS
pestrobarevnými	pestrobarevný	k2eAgFnPc7d1
jiřinami	jiřina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiřiny	Jiřina	k1gFnPc4
vystavuje	vystavovat	k5eAaImIp3nS
pěstitel	pěstitel	k1gMnSc1
Zdeněk	Zdeněk	k1gMnSc1
Beran	Beran	k1gMnSc1
z	z	k7c2
Horních	horní	k2eAgFnPc2d1
Heřmanic	Heřmanice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výstava	výstava	k1gFnSc1
bývá	bývat	k5eAaImIp3nS
doplněna	doplnit	k5eAaPmNgFnS
o	o	k7c4
řezbářské	řezbářský	k2eAgFnPc4d1
práce	práce	k1gFnPc4
či	či	k8xC
zajímavé	zajímavý	k2eAgFnPc4d1
fotografie	fotografia	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Hasičský	hasičský	k2eAgInSc1d1
ples	ples	k1gInSc1
-	-	kIx~
každoročně	každoročně	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
Herolticích	Heroltice	k1gFnPc6
pořádá	pořádat	k5eAaImIp3nS
hasičský	hasičský	k2eAgInSc1d1
ples	ples	k1gInSc1
pod	pod	k7c7
záštitou	záštita	k1gFnSc7
tamního	tamní	k2eAgInSc2d1
spolku	spolek	k1gInSc2
dobrovolných	dobrovolný	k2eAgMnPc2d1
hasičů	hasič	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
RC	RC	kA
modely	model	k1gInPc4
-	-	kIx~
tato	tento	k3xDgFnSc1
akce	akce	k1gFnSc1
s	s	k7c7
dlouholetou	dlouholetý	k2eAgFnSc7d1
tradicí	tradice	k1gFnSc7
se	se	k3xPyFc4
zaryla	zarýt	k5eAaPmAgFnS
do	do	k7c2
povědomí	povědomí	k1gNnSc2
všech	všecek	k3xTgMnPc2
stavitelů	stavitel	k1gMnPc2
a	a	k8xC
konstruktérů	konstruktér	k1gMnPc2
RC	RC	kA
modelů	model	k1gInPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
sem	sem	k6eAd1
sjíždějí	sjíždět	k5eAaImIp3nP
z	z	k7c2
celého	celý	k2eAgNnSc2d1
okolí	okolí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Osobnosti	osobnost	k1gFnPc1
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Felzmann	Felzmann	k1gMnSc1
(	(	kIx(
<g/>
1866	#num#	k4
–	–	k?
1937	#num#	k4
<g/>
)	)	kIx)
český	český	k2eAgMnSc1d1
a	a	k8xC
rakouský	rakouský	k2eAgMnSc1d1
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
poslanec	poslanec	k1gMnSc1
Moravského	moravský	k2eAgInSc2d1
zemského	zemský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
a	a	k8xC
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
–	–	k?
1869	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
online	onlinout	k5eAaPmIp3nS
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
MELZER	MELZER	kA
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
;	;	kIx,
SCHULZ	Schulz	k1gMnSc1
<g/>
,	,	kIx,
Jindřich	Jindřich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastivěda	vlastivěda	k1gFnSc1
šumperského	šumperský	k2eAgInSc2d1
okresu	okres	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šumperk	Šumperk	k1gInSc1
<g/>
:	:	kIx,
Okresní	okresní	k2eAgNnSc1d1
vlastivědné	vlastivědný	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85083	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
PERŮTKA	PERŮTKA	kA
<g/>
,	,	kIx,
M.	M.	kA
(	(	kIx(
<g/>
red	red	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
nemovitých	movitý	k2eNgFnPc2d1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
okresu	okres	k1gInSc2
Šumperk	Šumperk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
a	a	k8xC
OÚ	OÚ	kA
Šumperk	Šumperk	k1gInSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
901473	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Herolticka	Herolticka	k1gFnSc1
<g/>
.	.	kIx.
www.herolticka.cz	www.herolticka.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Heroltice	Heroltice	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Aktuální	aktuální	k2eAgFnPc1d1
informace	informace	k1gFnPc1
obce	obec	k1gFnSc2
Heroltice	Heroltice	k1gFnSc2
a	a	k8xC
její	její	k3xOp3gFnSc2
webové	webový	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
</s>
<s>
Heroltice	Heroltice	k1gFnSc1
v	v	k7c6
Registru	registrum	k1gNnSc6
územní	územní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
<g/>
,	,	kIx,
adres	adresa	k1gFnPc2
a	a	k8xC
nemovitostí	nemovitost	k1gFnPc2
(	(	kIx(
<g/>
RÚIAN	RÚIAN	kA
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Části	část	k1gFnSc3
města	město	k1gNnSc2
Štíty	štít	k1gInPc4
Části	část	k1gFnSc3
obce	obec	k1gFnSc2
</s>
<s>
BřeznáCrhovHerolticeŠtíty	BřeznáCrhovHerolticeŠtíta	k1gFnPc1
</s>
