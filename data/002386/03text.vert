<s>
Níkajský	Níkajský	k2eAgInSc1d1	Níkajský
(	(	kIx(	(
<g/>
též	též	k9	též
nicejský	nicejský	k2eAgInSc1d1	nicejský
<g/>
)	)	kIx)	)
koncil	koncil	k1gInSc1	koncil
svolal	svolat	k5eAaPmAgMnS	svolat
císař	císař	k1gMnSc1	císař
Kónstantínos	Kónstantínos	k1gMnSc1	Kónstantínos
v	v	k7c6	v
roce	rok	k1gInSc6	rok
325	[number]	k4	325
do	do	k7c2	do
maloasijského	maloasijský	k2eAgNnSc2d1	maloasijské
města	město	k1gNnSc2	město
Nikaia	Nikaium	k1gNnSc2	Nikaium
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgInSc4	první
ekumenický	ekumenický	k2eAgInSc4d1	ekumenický
koncil	koncil	k1gInSc4	koncil
-	-	kIx~	-
shromáždění	shromáždění	k1gNnSc1	shromáždění
biskupů	biskup	k1gInPc2	biskup
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
sněm	sněm	k1gInSc4	sněm
přijeli	přijet	k5eAaPmAgMnP	přijet
biskupové	biskup	k1gMnPc1	biskup
především	především	k6eAd1	především
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
poloviny	polovina	k1gFnSc2	polovina
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
sněmu	sněm	k1gInSc2	sněm
se	se	k3xPyFc4	se
osobně	osobně	k6eAd1	osobně
neúčastnil	účastnit	k5eNaImAgMnS	účastnit
biskup	biskup	k1gMnSc1	biskup
sídlící	sídlící	k2eAgMnSc1d1	sídlící
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Římě	Řím	k1gInSc6	Řím
(	(	kIx(	(
<g/>
papež	papež	k1gMnSc1	papež
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poslal	poslat	k5eAaPmAgInS	poslat
ovšem	ovšem	k9	ovšem
své	svůj	k3xOyFgMnPc4	svůj
zástupce	zástupce	k1gMnPc4	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Koncil	koncil	k1gInSc1	koncil
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
sporem	spor	k1gInSc7	spor
o	o	k7c4	o
nauku	nauka	k1gFnSc4	nauka
Areia	Areium	k1gNnSc2	Areium
z	z	k7c2	z
Alexandrie	Alexandrie	k1gFnSc2	Alexandrie
(	(	kIx(	(
<g/>
srv	srv	k?	srv
<g/>
.	.	kIx.	.
heslo	heslo	k1gNnSc4	heslo
ariánská	ariánský	k2eAgFnSc1d1	ariánská
hereze	hereze	k1gFnSc1	hereze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
týkal	týkat	k5eAaImAgMnS	týkat
povahy	povaha	k1gFnSc2	povaha
Kristovy	Kristův	k2eAgFnSc2d1	Kristova
osoby	osoba	k1gFnSc2	osoba
a	a	k8xC	a
vztahu	vztah	k1gInSc2	vztah
Božího	boží	k2eAgMnSc2d1	boží
Syna	syn	k1gMnSc2	syn
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
Otci	otec	k1gMnSc3	otec
<g/>
.	.	kIx.	.
</s>
<s>
Koncil	koncil	k1gInSc1	koncil
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
výrazy	výraz	k1gInPc7	výraz
"	"	kIx"	"
<g/>
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Syn	syn	k1gMnSc1	syn
nebyl	být	k5eNaImAgMnS	být
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Syn	syn	k1gMnSc1	syn
není	být	k5eNaImIp3nS	být
z	z	k7c2	z
jiné	jiný	k2eAgFnSc2d1	jiná
usie	usi	k1gFnSc2	usi
nebo	nebo	k8xC	nebo
hypostaze	hypostaze	k1gFnSc2	hypostaze
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
Syn	syn	k1gMnSc1	syn
<g/>
)	)	kIx)	)
nebyl	být	k5eNaImAgInS	být
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
byl	být	k5eAaImAgInS	být
zrozen	zrozen	k2eAgMnSc1d1	zrozen
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
jasně	jasně	k6eAd1	jasně
vymezit	vymezit	k5eAaPmF	vymezit
proti	proti	k7c3	proti
Areiovi	Areius	k1gMnSc3	Areius
(	(	kIx(	(
<g/>
či	či	k8xC	či
latinsky	latinsky	k6eAd1	latinsky
Ariovi	Ariův	k2eAgMnPc1d1	Ariův
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
nauce	nauka	k1gFnSc3	nauka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
koncilu	koncil	k1gInSc6	koncil
bylo	být	k5eAaImAgNnS	být
zformulováno	zformulován	k2eAgNnSc1d1	zformulováno
nicejské	nicejský	k2eAgNnSc1d1	nicejské
<g/>
/	/	kIx~	/
<g/>
nikájské	nikájský	k2eAgNnSc1d1	nikájský
vyznání	vyznání	k1gNnSc1	vyznání
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
především	především	k9	především
výrazy	výraz	k1gInPc1	výraz
"	"	kIx"	"
<g/>
Syn	syn	k1gMnSc1	syn
je	být	k5eAaImIp3nS	být
soupodstatný	soupodstatný	k2eAgMnSc1d1	soupodstatný
s	s	k7c7	s
Otcem	otec	k1gMnSc7	otec
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Syn	syn	k1gMnSc1	syn
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
Otcovy	otcův	k2eAgFnSc2d1	otcova
podstaty	podstata	k1gFnSc2	podstata
<g/>
"	"	kIx"	"
znovu	znovu	k6eAd1	znovu
arianismus	arianismus	k1gInSc4	arianismus
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
z	z	k7c2	z
výsledků	výsledek	k1gInPc2	výsledek
koncilu	koncil	k1gInSc2	koncil
byla	být	k5eAaImAgFnS	být
shoda	shoda	k1gFnSc1	shoda
církví	církev	k1gFnPc2	církev
na	na	k7c6	na
způsobu	způsob	k1gInSc6	způsob
výpočtu	výpočet	k1gInSc2	výpočet
data	datum	k1gNnSc2	datum
pro	pro	k7c4	pro
slavení	slavení	k1gNnSc4	slavení
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
svátky	svátek	k1gInPc1	svátek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
připomíná	připomínat	k5eAaImIp3nS	připomínat
smrt	smrt	k1gFnSc1	smrt
Kristova	Kristův	k2eAgFnSc1d1	Kristova
a	a	k8xC	a
slaví	slavit	k5eAaImIp3nS	slavit
jeho	jeho	k3xOp3gNnSc4	jeho
zmrtvýchvstání	zmrtvýchvstání	k1gNnSc4	zmrtvýchvstání
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
ústřední	ústřední	k2eAgNnSc4d1	ústřední
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
a	a	k8xC	a
církev	církev	k1gFnSc1	církev
proto	proto	k8xC	proto
považovala	považovat	k5eAaImAgFnS	považovat
za	za	k7c4	za
důležité	důležitý	k2eAgNnSc4d1	důležité
Ježíšovo	Ježíšův	k2eAgNnSc4d1	Ježíšovo
vzkříšení	vzkříšení	k1gNnSc4	vzkříšení
slavit	slavit	k5eAaImF	slavit
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Koncil	koncil	k1gInSc1	koncil
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
325	[number]	k4	325
v	v	k7c6	v
ústřední	ústřední	k2eAgFnSc6d1	ústřední
budově	budova	k1gFnSc6	budova
císařského	císařský	k2eAgInSc2d1	císařský
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
předběžně	předběžně	k6eAd1	předběžně
projednána	projednán	k2eAgFnSc1d1	projednána
otázka	otázka	k1gFnSc1	otázka
ariánství	ariánství	k1gNnSc2	ariánství
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
diskusi	diskuse	k1gFnSc6	diskuse
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
alexandrijský	alexandrijský	k2eAgMnSc1d1	alexandrijský
kněz	kněz	k1gMnSc1	kněz
Arius	Arius	k1gMnSc1	Arius
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
egyptští	egyptský	k2eAgMnPc1d1	egyptský
přívrženci	přívrženec	k1gMnPc1	přívrženec
nejspíše	nejspíše	k9	nejspíše
neúčastnili	účastnit	k5eNaImAgMnP	účastnit
<g/>
,	,	kIx,	,
hráli	hrát	k5eAaImAgMnP	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
Areiovi	Areiův	k2eAgMnPc1d1	Areiův
zastánci	zastánce	k1gMnPc1	zastánce
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
biskupů	biskup	k1gMnPc2	biskup
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
biskup	biskup	k1gMnSc1	biskup
Eusebios	Eusebios	k1gMnSc1	Eusebios
z	z	k7c2	z
Nikomédie	Nikomédie	k1gFnSc2	Nikomédie
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelná	pravidelný	k2eAgNnPc4d1	pravidelné
zasedání	zasedání	k1gNnPc4	zasedání
však	však	k9	však
započala	započnout	k5eAaPmAgFnS	započnout
teprve	teprve	k6eAd1	teprve
s	s	k7c7	s
příjezdem	příjezd	k1gInSc7	příjezd
císaře	císař	k1gMnSc2	císař
Constantina	Constantin	k1gMnSc2	Constantin
I.	I.	kA	I.
Po	po	k7c6	po
úvodní	úvodní	k2eAgFnSc6d1	úvodní
řeči	řeč	k1gFnSc6	řeč
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
císař	císař	k1gMnSc1	císař
též	též	k6eAd1	též
koncilní	koncilní	k2eAgInSc4d1	koncilní
pořádek	pořádek	k1gInSc4	pořádek
-	-	kIx~	-
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
při	při	k7c6	při
jednání	jednání	k1gNnSc6	jednání
postupovat	postupovat	k5eAaImF	postupovat
<g/>
.	.	kIx.	.
</s>
<s>
Předsedajícím	předsedající	k1gMnSc7	předsedající
koncilu	koncil	k1gInSc2	koncil
byl	být	k5eAaImAgMnS	být
nejspíše	nejspíše	k9	nejspíše
biskup	biskup	k1gMnSc1	biskup
Hosius	Hosius	k1gMnSc1	Hosius
z	z	k7c2	z
Córdoby	Córdoba	k1gFnSc2	Córdoba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zasedání	zasedání	k1gNnSc6	zasedání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
trvalo	trvat	k5eAaImAgNnS	trvat
celý	celý	k2eAgInSc4d1	celý
měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
koncil	koncil	k1gInSc4	koncil
slavnostně	slavnostně	k6eAd1	slavnostně
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
Nicejské	Nicejský	k2eAgNnSc4d1	Nicejské
vyznání	vyznání	k1gNnSc4	vyznání
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vyznání	vyznání	k1gNnSc1	vyznání
víry	víra	k1gFnSc2	víra
přijala	přijmout	k5eAaPmAgFnS	přijmout
většina	většina	k1gFnSc1	většina
přítomných	přítomný	k2eAgMnPc2d1	přítomný
biskupů	biskup	k1gMnPc2	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jednání	jednání	k1gNnSc2	jednání
o	o	k7c4	o
vyznání	vyznání	k1gNnSc4	vyznání
víry	víra	k1gFnSc2	víra
aktivně	aktivně	k6eAd1	aktivně
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
Eusebios	Eusebios	k1gInSc1	Eusebios
z	z	k7c2	z
Kaisareie	Kaisareie	k1gFnSc2	Kaisareie
<g/>
.	.	kIx.	.
</s>
<s>
Přednesl	přednést	k5eAaPmAgMnS	přednést
vlastní	vlastní	k2eAgNnSc4d1	vlastní
vyznání	vyznání	k1gNnSc4	vyznání
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
císař	císař	k1gMnSc1	císař
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
biskupů	biskup	k1gMnPc2	biskup
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
Eusebiovou	Eusebiový	k2eAgFnSc7d1	Eusebiový
snahou	snaha	k1gFnSc7	snaha
byl	být	k5eAaImAgInS	být
pokoj	pokoj	k1gInSc1	pokoj
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
a	a	k8xC	a
Eusebios	Eusebios	k1gInSc1	Eusebios
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
věci	věc	k1gFnSc6	věc
snažil	snažit	k5eAaImAgMnS	snažit
vyjít	vyjít	k5eAaPmF	vyjít
císaři	císař	k1gMnSc3	císař
vstříc	vstříc	k6eAd1	vstříc
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Kónstantínos	Kónstantínos	k1gMnSc1	Kónstantínos
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
připojit	připojit	k5eAaPmF	připojit
k	k	k7c3	k
Eusebiovu	Eusebiův	k2eAgNnSc3d1	Eusebiův
vyznání	vyznání	k1gNnSc3	vyznání
pojem	pojem	k1gInSc1	pojem
homoúsios	homoúsios	k1gInSc1	homoúsios
<g/>
.	.	kIx.	.
</s>
<s>
Nejspíše	nejspíše	k9	nejspíše
tato	tento	k3xDgFnSc1	tento
iniciativa	iniciativa	k1gFnSc1	iniciativa
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
nového	nový	k2eAgNnSc2d1	nové
kréda	krédo	k1gNnSc2	krédo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
lišilo	lišit	k5eAaImAgNnS	lišit
od	od	k7c2	od
Eusebiova	Eusebiův	k2eAgNnSc2d1	Eusebiův
vyznání	vyznání	k1gNnSc2	vyznání
nejen	nejen	k6eAd1	nejen
přijetím	přijetí	k1gNnSc7	přijetí
obratu	obrat	k1gInSc2	obrat
homoúsios	homoúsiosa	k1gFnPc2	homoúsiosa
tó	tó	k?	tó
patri	patri	k1gNnSc4	patri
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
soupodstatný	soupodstatný	k2eAgMnSc1d1	soupodstatný
s	s	k7c7	s
Otcem	otec	k1gMnSc7	otec
<g/>
"	"	kIx"	"
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
sněmem	sněm	k1gInSc7	sněm
v	v	k7c6	v
Níkaii	Níkaie	k1gFnSc6	Níkaie
přijato	přijmout	k5eAaPmNgNnS	přijmout
jako	jako	k8xC	jako
Níkajské	Níkajský	k2eAgNnSc1d1	Níkajský
vyznání	vyznání	k1gNnSc1	vyznání
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
325	[number]	k4	325
byl	být	k5eAaImAgInS	být
koncil	koncil	k1gInSc1	koncil
ukončen	ukončit	k5eAaPmNgInS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
řeči	řeč	k1gFnSc6	řeč
Constantinus	Constantinus	k1gMnSc1	Constantinus
opět	opět	k6eAd1	opět
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
mu	on	k3xPp3gMnSc3	on
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
pokoji	pokoj	k1gInSc6	pokoj
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
i	i	k8xC	i
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okružním	okružní	k2eAgInSc6d1	okružní
listu	list	k1gInSc6	list
pak	pak	k6eAd1	pak
informoval	informovat	k5eAaBmAgMnS	informovat
o	o	k7c6	o
výsledcích	výsledek	k1gInPc6	výsledek
jednání	jednání	k1gNnSc2	jednání
a	a	k8xC	a
shodě	shoda	k1gFnSc3	shoda
ohledně	ohledně	k7c2	ohledně
slavení	slavení	k1gNnSc2	slavení
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
<g/>
.	.	kIx.	.
</s>
<s>
Synoda	synoda	k1gFnSc1	synoda
roku	rok	k1gInSc2	rok
327	[number]	k4	327
zpochybnila	zpochybnit	k5eAaPmAgFnS	zpochybnit
usnesení	usnesení	k1gNnSc4	usnesení
1	[number]	k4	1
<g/>
.	.	kIx.	.
nikajského	nikajský	k2eAgInSc2d1	nikajský
koncilu	koncil	k1gInSc2	koncil
týkající	týkající	k2eAgFnSc1d1	týkající
se	se	k3xPyFc4	se
vyloučení	vyloučení	k1gNnSc1	vyloučení
některých	některý	k3yIgMnPc2	některý
zastánců	zastánce	k1gMnPc2	zastánce
Areia	Areius	k1gMnSc2	Areius
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
nauky	nauka	k1gFnSc2	nauka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Eusebia	Eusebia	k1gFnSc1	Eusebia
z	z	k7c2	z
Níkomédie	Níkomédie	k1gFnSc2	Níkomédie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
také	také	k9	také
teologické	teologický	k2eAgInPc1d1	teologický
spory	spor	k1gInPc1	spor
o	o	k7c4	o
pojetí	pojetí	k1gNnSc4	pojetí
a	a	k8xC	a
vyjádření	vyjádření	k1gNnSc4	vyjádření
vztahu	vztah	k1gInSc2	vztah
mezi	mezi	k7c7	mezi
osobami	osoba	k1gFnPc7	osoba
v	v	k7c6	v
Trojici	trojice	k1gFnSc6	trojice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zapříčiňovaly	zapříčiňovat	k5eAaImAgFnP	zapříčiňovat
rozdělení	rozdělení	k1gNnSc4	rozdělení
církve	církev	k1gFnSc2	církev
po	po	k7c6	po
celé	celá	k1gFnSc6	celá
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
koncilu	koncil	k1gInSc6	koncil
byla	být	k5eAaImAgFnS	být
také	také	k9	také
vybrána	vybrat	k5eAaPmNgFnS	vybrat
metoda	metoda	k1gFnSc1	metoda
pro	pro	k7c4	pro
výpočet	výpočet	k1gInSc4	výpočet
data	datum	k1gNnSc2	datum
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgInP	používat
dva	dva	k4xCgInPc1	dva
výpočty	výpočet	k1gInPc1	výpočet
<g/>
,	,	kIx,	,
římský	římský	k2eAgInSc1d1	římský
a	a	k8xC	a
alexandrijský	alexandrijský	k2eAgInSc1d1	alexandrijský
a	a	k8xC	a
nikajský	nikajský	k2eAgInSc1d1	nikajský
koncil	koncil	k1gInSc1	koncil
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pro	pro	k7c4	pro
ten	ten	k3xDgInSc4	ten
alexandrijský	alexandrijský	k2eAgInSc4d1	alexandrijský
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
až	až	k9	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
