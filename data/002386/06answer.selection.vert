<s>
Níkajský	Níkajský	k2eAgInSc1d1	Níkajský
(	(	kIx(	(
<g/>
též	též	k9	též
nicejský	nicejský	k2eAgInSc1d1	nicejský
<g/>
)	)	kIx)	)
koncil	koncil	k1gInSc1	koncil
svolal	svolat	k5eAaPmAgMnS	svolat
císař	císař	k1gMnSc1	císař
Kónstantínos	Kónstantínos	k1gMnSc1	Kónstantínos
v	v	k7c6	v
roce	rok	k1gInSc6	rok
325	[number]	k4	325
do	do	k7c2	do
maloasijského	maloasijský	k2eAgNnSc2d1	maloasijské
města	město	k1gNnSc2	město
Nikaia	Nikaium	k1gNnSc2	Nikaium
<g/>
.	.	kIx.	.
</s>
