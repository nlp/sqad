<p>
<s>
První	první	k4xOgFnSc1	první
búrská	búrský	k2eAgFnSc1d1	búrská
válka	válka	k1gFnSc1	válka
probíhala	probíhat	k5eAaImAgFnS	probíhat
na	na	k7c6	na
území	území	k1gNnSc6	území
Transvaalu	Transvaal	k1gInSc2	Transvaal
mezi	mezi	k7c7	mezi
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosincem	prosinec	k1gInSc7	prosinec
1880	[number]	k4	1880
a	a	k8xC	a
23	[number]	k4	23
<g/>
.	.	kIx.	.
březnem	březen	k1gInSc7	březen
1881	[number]	k4	1881
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
první	první	k4xOgInSc4	první
střet	střet	k1gInSc4	střet
mezi	mezi	k7c7	mezi
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
a	a	k8xC	a
Transvaalskou	Transvaalský	k2eAgFnSc7d1	Transvaalský
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Počátky	počátek	k1gInPc4	počátek
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1815	[number]	k4	1815
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
dohod	dohoda	k1gFnPc2	dohoda
vídeňského	vídeňský	k2eAgInSc2d1	vídeňský
kongresu	kongres	k1gInSc2	kongres
připadla	připadnout	k5eAaPmAgFnS	připadnout
bývalá	bývalý	k2eAgFnSc1d1	bývalá
nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
kolonie	kolonie	k1gFnSc1	kolonie
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
cípu	cíp	k1gInSc6	cíp
Afriky	Afrika	k1gFnSc2	Afrika
do	do	k7c2	do
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
Britů	Brit	k1gMnPc2	Brit
<g/>
.	.	kIx.	.
</s>
<s>
Búrové	Búr	k1gMnPc1	Búr
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zde	zde	k6eAd1	zde
žili	žít	k5eAaImAgMnP	žít
byli	být	k5eAaImAgMnP	být
od	od	k7c2	od
nynějška	nynějšek	k1gInSc2	nynějšek
systematicky	systematicky	k6eAd1	systematicky
vyháněni	vyhánět	k5eAaImNgMnP	vyhánět
anglickými	anglický	k2eAgMnPc7d1	anglický
osadníky	osadník	k1gMnPc7	osadník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
jejich	jejich	k3xOp3gInSc4	jejich
neprospěch	neprospěch	k1gInSc4	neprospěch
britské	britský	k2eAgInPc1d1	britský
koloniální	koloniální	k2eAgInPc1d1	koloniální
úřady	úřad	k1gInPc1	úřad
prosadily	prosadit	k5eAaPmAgInP	prosadit
zrušení	zrušení	k1gNnSc4	zrušení
otroctví	otroctví	k1gNnPc2	otroctví
(	(	kIx(	(
<g/>
1834	[number]	k4	1834
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zavedly	zavést	k5eAaPmAgFnP	zavést
angličtinu	angličtina	k1gFnSc4	angličtina
jako	jako	k8xS	jako
jediný	jediný	k2eAgInSc4d1	jediný
oficiální	oficiální	k2eAgInSc4d1	oficiální
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
zásadu	zásada	k1gFnSc4	zásada
<g/>
,	,	kIx,	,
že	že	k8xS	že
nové	nový	k2eAgFnPc1d1	nová
oblasti	oblast	k1gFnPc1	oblast
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
osidlovat	osidlovat	k5eAaImF	osidlovat
pouze	pouze	k6eAd1	pouze
po	po	k7c6	po
obdržení	obdržení	k1gNnSc6	obdržení
souhlasu	souhlas	k1gInSc2	souhlas
správy	správa	k1gFnSc2	správa
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
První	první	k4xOgFnSc1	první
búrská	búrský	k2eAgFnSc1d1	búrská
válka	válka	k1gFnSc1	válka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
