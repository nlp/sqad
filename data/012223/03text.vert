<p>
<s>
Paraguayské	paraguayský	k2eAgNnSc1d1	paraguayské
letectvo	letectvo	k1gNnSc1	letectvo
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
Fuerza	Fuerza	k1gFnSc1	Fuerza
Aérea	Aérea	k1gFnSc1	Aérea
Paraguaya	Paraguaya	k1gFnSc1	Paraguaya
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vzdušná	vzdušný	k2eAgFnSc1d1	vzdušná
složka	složka	k1gFnSc1	složka
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
Paraguaye	Paraguay	k1gFnSc2	Paraguay
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc4	přehled
letecké	letecký	k2eAgFnSc2d1	letecká
techniky	technika	k1gFnSc2	technika
==	==	k?	==
</s>
</p>
<p>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
přehled	přehled	k1gInSc4	přehled
letecké	letecký	k2eAgFnSc2d1	letecká
techniky	technika	k1gFnSc2	technika
Paraguayského	paraguayský	k2eAgNnSc2d1	paraguayské
letectva	letectvo	k1gNnSc2	letectvo
podle	podle	k7c2	podle
Flightglobal	Flightglobal	k1gFnSc2	Flightglobal
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Force	force	k1gFnSc2	force
aérienne	aériennout	k5eAaPmIp3nS	aériennout
paraguayenne	paraguayennout	k5eAaImIp3nS	paraguayennout
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
