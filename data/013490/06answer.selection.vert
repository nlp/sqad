<s>
Leží	ležet	k5eAaImIp3nS
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
127	#num#	k4
metrů	metr	k1gInPc2
na	na	k7c6
zalesněných	zalesněný	k2eAgInPc6d1
západních	západní	k2eAgInPc6d1
svazích	svah	k1gInPc6
pohoří	pohořet	k5eAaPmIp3nS
Karmel	Karmel	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
hřbetu	hřbet	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
jižně	jižně	k6eAd1
od	od	k7c2
osady	osada	k1gFnSc2
prudce	prudce	k6eAd1
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
do	do	k7c2
údolí	údolí	k1gNnSc2
Nachal	Nachal	k1gInSc1
Hod	Hod	k1gInSc1
a	a	k8xC
na	na	k7c6
severu	sever	k1gInSc6
do	do	k7c2
údolí	údolí	k1gNnSc2
Nachal	Nachal	k1gInSc1
Bustan	Bustan	k1gInSc1
<g/>
.	.	kIx.
</s>