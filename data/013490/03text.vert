<s>
Ejn	Ejn	k?
Hod	hod	k1gInSc4
</s>
<s>
O	o	k7c6
arabské	arabský	k2eAgFnSc6d1
vesnici	vesnice	k1gFnSc6
v	v	k7c6
Izraeli	Izrael	k1gInSc6
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yQgFnSc6,k3yIgFnSc6,k3yRgFnSc6
se	se	k3xPyFc4
usídlili	usídlit	k5eAaPmAgMnP
někteří	některý	k3yIgMnPc1
Arabové	Arab	k1gMnPc1
vysídlení	vysídlení	k1gNnSc4
roku	rok	k1gInSc2
1948	#num#	k4
z	z	k7c2
nynějšího	nynější	k2eAgMnSc2d1
Ejn	Ejn	k1gMnSc2
Hod	hod	k1gInSc4
<g/>
,	,	kIx,
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Ejn	Ejn	k1gFnSc2
Chaud	Chauda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Ejn	Ejn	k?
Hod	hod	k1gInSc1
ע	ע	k?
ה	ה	k?
Výhled	výhled	k1gInSc1
z	z	k7c2
Ejn	Ejn	k1gFnSc2
Hod	hod	k1gInSc4
na	na	k7c4
okolní	okolní	k2eAgInPc4d1
svahy	svah	k1gInPc4
KarmeluPoloha	KarmeluPoloha	k1gMnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
32	#num#	k4
<g/>
°	°	k?
<g/>
42	#num#	k4
<g/>
′	′	k?
<g/>
1	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
34	#num#	k4
<g/>
°	°	k?
<g/>
58	#num#	k4
<g/>
′	′	k?
<g/>
53	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
127	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Izrael	Izrael	k1gInSc1
Izrael	Izrael	k1gInSc1
distrikt	distrikt	k1gInSc4
</s>
<s>
Haifský	haifský	k2eAgMnSc1d1
oblastní	oblastní	k2eAgMnSc1d1
rada	rada	k1gMnSc1
</s>
<s>
Chof	Chof	k1gMnSc1
ha-Karmel	ha-Karmel	k1gMnSc1
</s>
<s>
Ejn	Ejn	k?
Hod	hod	k1gInSc4
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
583	#num#	k4
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
Správa	správa	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
1954	#num#	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.ein-hod.org	www.ein-hod.org	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ejn	Ejn	k1gInSc1
Hod	hod	k1gInSc1
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
ע	ע	kA
<g/>
ֵ	ֵ	kA
<g/>
י	י	kA
ה	ה	kA
<g/>
ֹ	ֹ	kA
<g/>
ד	ד	kA
<g/>
,	,	kIx,
v	v	k7c6
oficiálním	oficiální	k2eAgInSc6d1
přepisu	přepis	k1gInSc6
do	do	k7c2
angličtiny	angličtina	k1gFnSc2
En	En	k1gInSc1
Hod	Hod	k1gInSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
přepisováno	přepisován	k2eAgNnSc1d1
též	též	k9
Ein	Ein	k1gInSc1
Hod	Hod	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vesnice	vesnice	k1gFnSc1
typu	typ	k1gInSc2
společná	společný	k2eAgFnSc1d1
osada	osada	k1gFnSc1
(	(	kIx(
<g/>
jišuv	jišuv	k1gInSc1
kehilati	kehilati	k1gInSc1
<g/>
)	)	kIx)
v	v	k7c6
Izraeli	Izrael	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
Haifském	haifský	k2eAgInSc6d1
distriktu	distrikt	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
Oblastní	oblastní	k2eAgFnSc6d1
radě	rada	k1gFnSc6
Chof	Chof	k1gInSc1
ha-Karmel	ha-Karmel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Leží	ležet	k5eAaImIp3nS
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
127	#num#	k4
metrů	metr	k1gInPc2
na	na	k7c6
zalesněných	zalesněný	k2eAgInPc6d1
západních	západní	k2eAgInPc6d1
svazích	svah	k1gInPc6
pohoří	pohořet	k5eAaPmIp3nS
Karmel	Karmel	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
hřbetu	hřbet	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
jižně	jižně	k6eAd1
od	od	k7c2
osady	osada	k1gFnSc2
prudce	prudce	k6eAd1
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
do	do	k7c2
údolí	údolí	k1gNnSc2
Nachal	Nachal	k1gInSc1
Hod	Hod	k1gInSc1
a	a	k8xC
na	na	k7c6
severu	sever	k1gInSc6
do	do	k7c2
údolí	údolí	k1gNnSc2
Nachal	Nachal	k1gInSc1
Bustan	Bustan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Obec	obec	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
cca	cca	kA
4	#num#	k4
kilometry	kilometr	k1gInPc7
od	od	k7c2
břehů	břeh	k1gInPc2
Středozemního	středozemní	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
cca	cca	kA
72	#num#	k4
kilometrů	kilometr	k1gInPc2
severoseverovýchodně	severoseverovýchodně	k6eAd1
od	od	k7c2
centra	centrum	k1gNnSc2
Tel	tel	kA
Avivu	Aviv	k1gInSc2
a	a	k8xC
cca	cca	kA
12	#num#	k4
kilometrů	kilometr	k1gInPc2
jižně	jižně	k6eAd1
od	od	k7c2
centra	centrum	k1gNnSc2
Haify	Haifa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ejn	Ejn	k1gFnPc2
Hod	hod	k1gInSc4
obývají	obývat	k5eAaImIp3nP
Židé	Žid	k1gMnPc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
osídlení	osídlení	k1gNnSc1
v	v	k7c6
tomto	tento	k3xDgInSc6
regionu	region	k1gInSc6
je	být	k5eAaImIp3nS
převážně	převážně	k6eAd1
židovské	židovský	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
6	#num#	k4
kilometrů	kilometr	k1gInPc2
na	na	k7c4
východ	východ	k1gInSc4
od	od	k7c2
vesnice	vesnice	k1gFnSc2
leží	ležet	k5eAaImIp3nS
na	na	k7c6
hřbetu	hřbet	k1gMnSc6
Karmelu	Karmel	k1gMnSc6
skupina	skupina	k1gFnSc1
sídel	sídlo	k1gNnPc2
obývaných	obývaný	k2eAgMnPc2d1
arabsky	arabsky	k6eAd1
mluvícími	mluvící	k2eAgMnPc7d1
Drúzy	drúza	k1gFnSc2
a	a	k8xC
cca	cca	kA
2	#num#	k4
kilometry	kilometr	k1gInPc7
jihovýchodně	jihovýchodně	k6eAd1
od	od	k7c2
Ejn	Ejn	k1gFnSc2
Hod	hod	k1gInSc4
také	také	k6eAd1
stojí	stát	k5eAaImIp3nS
vesnice	vesnice	k1gFnSc1
Ejn	Ejn	k1gFnPc2
Chaud	Chauda	k1gFnPc2
osídlená	osídlený	k2eAgFnSc1d1
izraelskými	izraelský	k2eAgMnPc7d1
Araby	Arab	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Ejn	Ejn	k?
Hod	hod	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c4
dopravní	dopravní	k2eAgFnSc4d1
síť	síť	k1gFnSc4
napojen	napojen	k2eAgMnSc1d1
pomocí	pomocí	k7c2
lokální	lokální	k2eAgFnSc2d1
silnice	silnice	k1gFnSc2
číslo	číslo	k1gNnSc1
7111	#num#	k4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
sem	sem	k6eAd1
odbočuje	odbočovat	k5eAaImIp3nS
z	z	k7c2
dálnice	dálnice	k1gFnSc2
číslo	číslo	k1gNnSc1
4	#num#	k4
v	v	k7c6
pobřežní	pobřežní	k2eAgFnSc6d1
nížině	nížina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
Muzeum	muzeum	k1gNnSc4
Marcela	Marcel	k1gMnSc2
Janko	Janko	k1gMnSc1
v	v	k7c6
Ejn	Ejn	k1gFnSc6
Hod	hod	k1gInSc4
</s>
<s>
Na	na	k7c6
místě	místo	k1gNnSc6
současné	současný	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
Ejn	Ejn	k1gFnSc1
Hod	hod	k1gInSc1
se	se	k3xPyFc4
do	do	k7c2
roku	rok	k1gInSc2
1948	#num#	k4
rozkládala	rozkládat	k5eAaImAgFnS
arabská	arabský	k2eAgFnSc1d1
vesnice	vesnice	k1gFnSc1
Ejn	Ejn	k1gMnSc1
Chaud	Chaud	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stála	stát	k5eAaImAgFnS
v	v	k7c6
ní	on	k3xPp3gFnSc6
mešita	mešita	k1gFnSc1
a	a	k8xC
chlapecká	chlapecký	k2eAgFnSc1d1
základní	základní	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1931	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
459	#num#	k4
lidí	člověk	k1gMnPc2
v	v	k7c6
81	#num#	k4
domech	dům	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
války	válka	k1gFnSc2
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
byla	být	k5eAaImAgFnS
vesnice	vesnice	k1gFnSc1
dobyta	dobýt	k5eAaPmNgFnS
izraelskými	izraelský	k2eAgFnPc7d1
silami	síla	k1gFnPc7
a	a	k8xC
arabské	arabský	k2eAgNnSc4d1
osídlení	osídlení	k1gNnSc4
tu	tu	k6eAd1
skončilo	skončit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
mnoha	mnoho	k4c2
jiných	jiný	k2eAgNnPc2d1
arabských	arabský	k2eAgNnPc2d1
sídel	sídlo	k1gNnPc2
ovládnutých	ovládnutý	k2eAgFnPc2d1
Izraelci	Izraelec	k1gMnPc7
ale	ale	k8xC
nedošlo	dojít	k5eNaPmAgNnS
k	k	k7c3
její	její	k3xOp3gFnSc3
demolici	demolice	k1gFnSc3
a	a	k8xC
zdejší	zdejší	k2eAgFnSc1d1
zástavba	zástavba	k1gFnSc1
byla	být	k5eAaImAgFnS
zachována	zachovat	k5eAaPmNgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nynější	nynější	k2eAgMnSc1d1
židovský	židovský	k2eAgMnSc1d1
Ejn	Ejn	k1gMnSc1
Hod	hod	k1gInSc4
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1954	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gMnPc1
obyvatelé	obyvatel	k1gMnPc1
se	se	k3xPyFc4
nastěhovali	nastěhovat	k5eAaPmAgMnP
do	do	k7c2
opuštěné	opuštěný	k2eAgFnSc2d1
arabské	arabský	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
uměleckou	umělecký	k2eAgFnSc4d1
kolonii	kolonie	k1gFnSc4
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yIgMnPc4,k3yQgMnPc4,k3yRgMnPc4
žijí	žít	k5eAaImIp3nP
a	a	k8xC
prodávají	prodávat	k5eAaImIp3nP
své	svůj	k3xOyFgInPc4
výrobky	výrobek	k1gInPc4
řemeslníci	řemeslník	k1gMnPc1
<g/>
,	,	kIx,
malíři	malíř	k1gMnPc1
<g/>
,	,	kIx,
keramici	keramik	k1gMnPc1
apod.	apod.	kA
Nachází	nacházet	k5eAaImIp3nP
se	se	k3xPyFc4
zde	zde	k6eAd1
i	i	k9
muzeum	muzeum	k1gNnSc1
dadaistického	dadaistický	k2eAgMnSc2d1
malíře	malíř	k1gMnSc2
Marcela	Marcela	k1gFnSc1
Janco	Janco	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
patřil	patřit	k5eAaImAgMnS
mezi	mezi	k7c4
zakladatele	zakladatel	k1gMnSc4
kolonie	kolonie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Část	část	k1gFnSc1
Arabů	Arab	k1gMnPc2
vysídlených	vysídlený	k2eAgInPc2d1
roku	rok	k1gInSc2
1948	#num#	k4
z	z	k7c2
Ejn	Ejn	k1gFnSc2
Chaud	Chauda	k1gFnPc2
se	se	k3xPyFc4
v	v	k7c6
následujících	následující	k2eAgFnPc6d1
dekádách	dekáda	k1gFnPc6
snažila	snažit	k5eAaImAgFnS
domoci	domoct	k5eAaPmF
návratu	návrat	k1gInSc2
do	do	k7c2
svých	svůj	k3xOyFgInPc2
domovů	domov	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
založili	založit	k5eAaPmAgMnP
provizorní	provizorní	k2eAgFnSc4d1
osadu	osada	k1gFnSc4
cca	cca	kA
2	#num#	k4
kilometry	kilometr	k1gInPc7
jihovýchodně	jihovýchodně	k6eAd1
odtud	odtud	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
byla	být	k5eAaImAgFnS
nakonec	nakonec	k6eAd1
uznána	uznat	k5eAaPmNgFnS
za	za	k7c4
oficiální	oficiální	k2eAgNnSc4d1
sídlo	sídlo	k1gNnSc4
pod	pod	k7c7
jménem	jméno	k1gNnSc7
Ejn	Ejn	k1gFnPc2
Chaud	Chaud	k1gInSc4
a	a	k8xC
stala	stát	k5eAaPmAgFnS
se	s	k7c7
členskou	členský	k2eAgFnSc7d1
obcí	obec	k1gFnSc7
Oblastní	oblastní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
Chof	Chof	k1gMnSc1
ha-Karmel	ha-Karmel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Demografie	demografie	k1gFnSc1
</s>
<s>
Podle	podle	k7c2
údajů	údaj	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
tvořili	tvořit	k5eAaImAgMnP
naprostou	naprostý	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
Ejn	Ejn	k1gFnSc6
Hod	hod	k1gInSc4
Židé	Žid	k1gMnPc1
(	(	kIx(
<g/>
včetně	včetně	k7c2
statistické	statistický	k2eAgFnSc2d1
kategorie	kategorie	k1gFnSc2
"	"	kIx"
<g/>
ostatní	ostatní	k2eAgFnPc1d1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
nearabské	arabský	k2eNgMnPc4d1
obyvatele	obyvatel	k1gMnPc4
židovského	židovský	k2eAgInSc2d1
původu	původ	k1gInSc2
ale	ale	k8xC
bez	bez	k7c2
formální	formální	k2eAgFnSc2d1
příslušnosti	příslušnost	k1gFnSc2
k	k	k7c3
židovskému	židovský	k2eAgNnSc3d1
náboženství	náboženství	k1gNnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jde	jít	k5eAaImIp3nS
o	o	k7c4
menší	malý	k2eAgNnSc4d2
sídlo	sídlo	k1gNnSc4
vesnického	vesnický	k2eAgInSc2d1
typu	typ	k1gInSc2
s	s	k7c7
dlouhodobě	dlouhodobě	k6eAd1
rostoucí	rostoucí	k2eAgFnSc7d1
populací	populace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
prosinci	prosinec	k1gInSc6
2014	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
583	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
populace	populace	k1gFnSc1
klesla	klesnout	k5eAaPmAgFnS
o	o	k7c4
0,5	0,5	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
Ejn	Ejn	k1gFnSc2
Hod	hod	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
1961	#num#	k4
</s>
<s>
1972	#num#	k4
</s>
<s>
1983	#num#	k4
</s>
<s>
1995	#num#	k4
</s>
<s>
2001	#num#	k4
</s>
<s>
2003	#num#	k4
</s>
<s>
2004	#num#	k4
</s>
<s>
2005	#num#	k4
</s>
<s>
2006	#num#	k4
</s>
<s>
2007	#num#	k4
</s>
<s>
2008	#num#	k4
</s>
<s>
2009	#num#	k4
</s>
<s>
2010	#num#	k4
</s>
<s>
2011	#num#	k4
</s>
<s>
2012	#num#	k4
</s>
<s>
2013	#num#	k4
</s>
<s>
2014	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
27168191336432464472486519546559522537556586583	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
י	י	k?
2014	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izraelský	izraelský	k2eAgInSc1d1
centrální	centrální	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
י	י	k?
2013	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izraelský	izraelský	k2eAgInSc1d1
centrální	centrální	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Welcome	Welcom	k1gInSc5
To	to	k9
'	'	kIx"
<g/>
Ayn	Ayn	k1gMnSc1
Hawd	Hawd	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palestine	Palestin	k1gInSc5
Remembered	Remembered	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ע	ע	k?
ה	ה	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
hof-hacarmel	hof-hacarmel	k1gInSc1
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
il	il	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ע	ע	k?
ה	ה	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
www.nirezion.com	www.nirezion.com	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ר	ר	k?
ה	ה	k?
<g/>
,	,	kIx,
מ	מ	k?
ג	ג	k?
ו	ו	k?
1948,1961	1948,1961	k4
<g/>
,1972	,1972	k4
<g/>
,1983	,1983	k4
<g/>
,	,	kIx,
1995	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izraelský	izraelský	k2eAgInSc1d1
centrální	centrální	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ש	ש	k?
י	י	k?
א	א	k?
a	a	k8xC
další	další	k2eAgInPc1d1
seznamy	seznam	k1gInPc1
demografického	demografický	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
sídel	sídlo	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izraelský	izraelský	k2eAgInSc1d1
centrální	centrální	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
společné	společný	k2eAgFnSc2d1
osady	osada	k1gFnSc2
</s>
<s>
Ejn	Ejn	k?
Chaud	Chaud	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Ejn	Ejn	k1gFnSc2
Hod	hod	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
obce	obec	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Oblastní	oblastní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
Chof	Chof	k1gMnSc1
ha-Karmel	ha-Karmel	k1gMnSc1
Kibucy	kibuc	k1gInPc7
</s>
<s>
Bejt	Bejt	k?
Oren	Oren	k1gMnSc1
</s>
<s>
Ejn	Ejn	k?
Karmel	Karmel	k1gInSc1
</s>
<s>
ha-Chotrim	ha-Chotrim	k6eAd1
</s>
<s>
Ma	Ma	k?
<g/>
'	'	kIx"
<g/>
agan	agan	k1gMnSc1
Micha	Micha	k1gMnSc1
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
</s>
<s>
Ma	Ma	k?
<g/>
'	'	kIx"
<g/>
ajan	ajan	k1gMnSc1
Cvi	Cvi	k1gMnSc1
</s>
<s>
Nachšolim	Nachšolim	k6eAd1
</s>
<s>
Neve	Neve	k1gFnSc1
Jam	jáma	k1gFnPc2
</s>
<s>
Sdot	Sdot	k2eAgInSc1d1
Jam	jam	k1gInSc1
Mošavy	Mošava	k1gFnSc2
</s>
<s>
Bat	Bat	k?
Šlomo	Šloma	k1gFnSc5
</s>
<s>
Bejt	Bejt	k?
Chananja	Chananja	k1gFnSc1
</s>
<s>
Crufa	Cruf	k1gMnSc4
</s>
<s>
Dor	Dora	k1gFnPc2
</s>
<s>
Ejn	Ejn	k?
Ajala	Ajala	k1gFnSc1
</s>
<s>
Geva	Geva	k1gMnSc1
Karmel	Karmel	k1gMnSc1
</s>
<s>
ha-Bonim	ha-Bonim	k6eAd1
</s>
<s>
Kerem	Kerem	k1gInSc1
Maharal	Maharal	k1gFnSc2
</s>
<s>
Megadim	Megadim	k6eAd1
</s>
<s>
Nir	Nir	k?
Ecjon	Ecjon	k1gInSc1
</s>
<s>
Ofer	Ofer	k1gInSc1
Společné	společný	k2eAgFnSc2d1
osady	osada	k1gFnSc2
</s>
<s>
Atlit	Atlit	k1gMnSc1
</s>
<s>
Caesarea	Caesarea	k6eAd1
</s>
<s>
Ejn	Ejn	k?
Hod	hod	k1gInSc4
Arabská	arabský	k2eAgFnSc1d1
vesnice	vesnice	k1gFnSc1
</s>
<s>
Ejn	Ejn	k?
Chaud	Chaud	k1gMnSc1
Ostatní	ostatní	k2eAgMnSc1d1
sídla	sídlo	k1gNnSc2
</s>
<s>
Kfar	Kfar	k1gMnSc1
Galim	Galim	k1gMnSc1
</s>
<s>
Kfar	Kfar	k1gMnSc1
Cvi	Cvi	k1gMnSc1
Sitrin	Sitrin	k1gInSc4
</s>
<s>
Me	Me	k?
<g/>
'	'	kIx"
<g/>
ir	ir	k?
Šfeja	Šfeja	k1gFnSc1
</s>
<s>
Geografie	geografie	k1gFnSc1
pohoří	pohořet	k5eAaPmIp3nS
Karmel	Karmel	k1gInSc4
Hory	hora	k1gFnSc2
</s>
<s>
Cukej	cukat	k5eAaImRp2nS
Chotem	Chot	k1gInSc7
</s>
<s>
Giv	Giv	k?
<g/>
'	'	kIx"
<g/>
at	at	k?
Eden	Eden	k1gInSc1
</s>
<s>
Giv	Giv	k?
<g/>
'	'	kIx"
<g/>
at	at	k?
Šana	Šana	k1gFnSc1
</s>
<s>
Giv	Giv	k?
<g/>
'	'	kIx"
<g/>
at	at	k?
Šluchit	Šluchit	k1gInSc1
</s>
<s>
Giv	Giv	k?
<g/>
'	'	kIx"
<g/>
ot	ot	k1gMnSc1
Telimon	Telimon	k1gMnSc1
</s>
<s>
Har	Har	k?
Alon	Alon	k1gInSc1
</s>
<s>
Har	Har	k?
Arkan	Arkan	k1gInSc1
</s>
<s>
Har	Har	k?
Ela	Ela	k1gFnSc1
</s>
<s>
Har	Har	k?
Mehalel	Mehalel	k1gFnSc1
</s>
<s>
Har	Har	k?
Oren	Oren	k1gMnSc1
</s>
<s>
Har	Har	k?
Sumak	Sumak	k1gInSc1
</s>
<s>
Har	Har	k?
Šokef	Šokef	k1gInSc1
</s>
<s>
Har	Har	k?
Tlali	Tlale	k1gFnSc4
</s>
<s>
Chotem	Choto	k1gNnSc7
ha-Karmel	ha-Karmela	k1gFnPc2
</s>
<s>
Keren	keren	k1gInSc1
Karmel	Karmela	k1gFnPc2
</s>
<s>
Micpe	Micpat	k5eAaPmIp3nS
Chusejfa	Chusejf	k1gMnSc4
</s>
<s>
Ramat	Ramat	k1gInSc1
ha-Nadiv	ha-Nadit	k5eAaPmDgInS
</s>
<s>
Reches	Reches	k1gMnSc1
Mitla	Mitla	k1gMnSc1
</s>
<s>
Rom	Rom	k1gMnSc1
Karmel	Karmel	k1gMnSc1
Vodní	vodní	k2eAgFnSc2d1
toky	toka	k1gFnSc2
</s>
<s>
Achuza	Achuza	k1gFnSc1
</s>
<s>
Alija	Alija	k1gMnSc1
</s>
<s>
Alon	Alon	k1gMnSc1
</s>
<s>
Amik	Amik	k6eAd1
</s>
<s>
Amiram	Amiram	k1gInSc1
</s>
<s>
Ben	Ben	k1gInSc1
Dor	Dora	k1gFnPc2
</s>
<s>
Bustan	Bustan	k1gInSc1
</s>
<s>
Dalija	Dalija	k6eAd1
</s>
<s>
Duchan	Duchan	k1gMnSc1
</s>
<s>
Durim	Durim	k6eAd1
</s>
<s>
Elkana	Elkana	k1gFnSc1
</s>
<s>
Elro	Elro	k1gNnSc1
<g/>
'	'	kIx"
<g/>
i	i	k9
</s>
<s>
Even	Even	k1gMnSc1
</s>
<s>
Ezov	Ezov	k1gMnSc1
</s>
<s>
Galim	Galim	k6eAd1
</s>
<s>
Giborim	Giborim	k6eAd1
</s>
<s>
Hod	hod	k1gInSc4
</s>
<s>
Chanilon	Chanilon	k1gInSc1
</s>
<s>
Charuvim	Charuvim	k6eAd1
</s>
<s>
Chejk	Chejk	k6eAd1
</s>
<s>
Chotem	Chot	k1gInSc7
</s>
<s>
Chusejfa	Chusejf	k1gMnSc4
</s>
<s>
Jagur	Jagur	k1gMnSc1
</s>
<s>
Jokne	Joknout	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
'	'	kIx"
<g/>
am	am	k?
</s>
<s>
Katija	Katija	k6eAd1
</s>
<s>
Kelach	Kelach	k1gMnSc1
</s>
<s>
Kevara	Kevara	k1gFnSc1
</s>
<s>
Lotem	lot	k1gInSc7
</s>
<s>
Ma	Ma	k?
<g/>
'	'	kIx"
<g/>
apilim	apilim	k1gMnSc1
</s>
<s>
Maharal	Maharat	k5eAaImAgMnS,k5eAaPmAgMnS
</s>
<s>
Me	Me	k?
<g/>
'	'	kIx"
<g/>
arot	arot	k1gMnSc1
</s>
<s>
Megadim	Megadim	k6eAd1
</s>
<s>
Mitla	Mitla	k6eAd1
</s>
<s>
Nec	Nec	k?
</s>
<s>
Neder	drát	k5eNaImRp2nS
</s>
<s>
Nešer	Nešer	k1gMnSc1
</s>
<s>
Oranit	Oranit	k1gInSc1
</s>
<s>
Oren	Oren	k1gMnSc1
</s>
<s>
Ovadja	Ovadja	k6eAd1
</s>
<s>
Rakefet	Rakefet	k1gMnSc1
</s>
<s>
Rakit	Rakit	k1gMnSc1
</s>
<s>
Sevach	Sevach	k1gMnSc1
</s>
<s>
Sfunim	Sfunim	k6eAd1
</s>
<s>
Siach	Siach	k1gMnSc1
</s>
<s>
Šikmona	Šikmona	k1gFnSc1
</s>
<s>
Šimri	Šimri	k6eAd1
</s>
<s>
Tan	Tan	k?
</s>
<s>
Taninim	Taninim	k6eAd1
</s>
<s>
Timsach	Timsach	k1gMnSc1
</s>
<s>
Tira	Tira	k6eAd1
</s>
<s>
Telimon	Telimon	k1gMnSc1
</s>
<s>
Tut	tut	k1gInSc1
</s>
<s>
Vardija	Vardija	k6eAd1
</s>
<s>
Zichrona	Zichrona	k1gFnSc1
Údolí	údolí	k1gNnSc2
</s>
<s>
Bik	bika	k1gFnPc2
<g/>
'	'	kIx"
<g/>
at	at	k?
ha-Nadiv	ha-Nadit	k5eAaPmDgInS
</s>
<s>
Bik	bika	k1gFnPc2
<g/>
'	'	kIx"
<g/>
at	at	k?
Šir	širo	k1gNnPc2
</s>
<s>
Emek	Emek	k1gMnSc1
Maharal	Maharal	k1gMnSc1
</s>
<s>
Izraelská	izraelský	k2eAgFnSc1d1
pobřežní	pobřežní	k2eAgFnSc1d1
planina	planina	k1gFnSc1
</s>
<s>
Jizre	Jizr	k1gMnSc5
<g/>
'	'	kIx"
<g/>
elské	elská	k1gFnSc3
údolí	údolí	k1gNnSc2
</s>
<s>
Zevulunské	Zevulunská	k1gFnPc1
údolí	údolí	k1gNnSc2
Města	město	k1gNnSc2
a	a	k8xC
vesnice	vesnice	k1gFnSc2
</s>
<s>
Bat	Bat	k?
Šlomo	Šloma	k1gFnSc5
</s>
<s>
Bejt	Bejt	k?
Oren	Oren	k1gMnSc1
</s>
<s>
Dalijat	Dalijat	k2eAgInSc1d1
al-Karmel	al-Karmel	k1gInSc1
</s>
<s>
Ejn	Ejn	k?
Hod	hod	k1gInSc4
</s>
<s>
Ejn	Ejn	k?
Chaud	Chaud	k1gInSc1
</s>
<s>
Furejdis	Furejdis	k1gFnSc1
</s>
<s>
Haifa	Haifa	k1gFnSc1
</s>
<s>
Isfija	Isfija	k6eAd1
</s>
<s>
Jagur	Jagur	k1gMnSc1
</s>
<s>
Jokne	Joknout	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
'	'	kIx"
<g/>
am	am	k?
</s>
<s>
Kerem	Kerem	k1gInSc1
Maharal	Maharal	k1gFnSc2
</s>
<s>
Me	Me	k?
<g/>
'	'	kIx"
<g/>
ir	ir	k?
Šfeja	Šfeja	k1gFnSc1
</s>
<s>
Nešer	Nešer	k1gMnSc1
</s>
<s>
Nir	Nir	k?
Ecjon	Ecjon	k1gInSc1
</s>
<s>
Ofer	Ofer	k1gMnSc1
</s>
<s>
Tirat	Tirat	k2eAgInSc1d1
Karmel	Karmel	k1gInSc1
</s>
<s>
Zichron	Zichron	k1gInSc1
Ja	Ja	k?
<g/>
'	'	kIx"
<g/>
akov	akov	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Izrael	Izrael	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
540928	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
nr	nr	k?
<g/>
93051819	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
148195460	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-nr	lccn-nr	k1gInSc1
<g/>
93051819	#num#	k4
</s>
