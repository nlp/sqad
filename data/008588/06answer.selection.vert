<s>
Požár	požár	k1gInSc1	požár
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1666	[number]	k4	1666
<g/>
,	,	kIx,	,
ráno	ráno	k6eAd1	ráno
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Pudding	Pudding	k1gInSc4	Pudding
Lane	Lane	k1gNnSc2	Lane
<g/>
,	,	kIx,	,
v	v	k7c6	v
domě	dům	k1gInSc6	dům
Tomase	Tomasa	k1gFnSc3	Tomasa
Farrynora	Farrynor	k1gMnSc2	Farrynor
<g/>
,	,	kIx,	,
pekaře	pekař	k1gMnSc2	pekař
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
