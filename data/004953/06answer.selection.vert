<s>
Pašto	Pašto	k1gNnSc1	Pašto
je	být	k5eAaImIp3nS	být
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
<g/>
,	,	kIx,	,
mluví	mluvit	k5eAaImIp3nP	mluvit
jim	on	k3xPp3gFnPc3	on
asi	asi	k9	asi
60	[number]	k4	60
%	%	kIx~	%
Afghánců	Afghánec	k1gMnPc2	Afghánec
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
část	část	k1gFnSc1	část
obyvatel	obyvatel	k1gMnPc2	obyvatel
Pákistánu	Pákistán	k1gInSc2	Pákistán
<g/>
.	.	kIx.	.
</s>
