<p>
<s>
Waterloo	Waterloo	k1gNnSc1	Waterloo
je	být	k5eAaImIp3nS	být
stanice	stanice	k1gFnSc1	stanice
metra	metro	k1gNnSc2	metro
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
otevřená	otevřený	k2eAgFnSc1d1	otevřená
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
linkách	linka	k1gFnPc6	linka
:	:	kIx,	:
</s>
</p>
<p>
<s>
Bakerloo	Bakerloo	k6eAd1	Bakerloo
Line	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
stanicemi	stanice	k1gFnPc7	stanice
Embankment	Embankment	k1gMnSc1	Embankment
a	a	k8xC	a
Lambeth	Lambeth	k1gMnSc1	Lambeth
North	North	k1gMnSc1	North
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jubilee	Jubilee	k6eAd1	Jubilee
Line	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
stanicemi	stanice	k1gFnPc7	stanice
Westminster	Westminstra	k1gFnPc2	Westminstra
a	a	k8xC	a
Southwark	Southwark	k1gInSc1	Southwark
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Northern	Northern	k1gInSc1	Northern
Line	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
stanicemi	stanice	k1gFnPc7	stanice
Embankment	Embankment	k1gInSc1	Embankment
a	a	k8xC	a
Kennington	Kennington	k1gInSc1	Kennington
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Waterloo	Waterloo	k1gNnSc1	Waterloo
&	&	k?	&
City	city	k1gNnSc1	city
line	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
linka	linka	k1gFnSc1	linka
končí	končit	k5eAaImIp3nS	končit
<g/>
,	,	kIx,	,
před	před	k7c7	před
touto	tento	k3xDgFnSc7	tento
stanicí	stanice	k1gFnSc7	stanice
je	být	k5eAaImIp3nS	být
Bank	bank	k1gInSc1	bank
<g/>
)	)	kIx)	)
</s>
</p>
