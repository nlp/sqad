<p>
<s>
Dabbajra	Dabbajra	k1gFnSc1	Dabbajra
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
neaktivní	aktivní	k2eNgFnSc1d1	neaktivní
štítová	štítový	k2eAgFnSc1d1	štítová
sopka	sopka	k1gFnSc1	sopka
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Afarské	Afarský	k2eAgFnSc2d1	Afarská
pánve	pánev	k1gFnSc2	pánev
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejzápadněji	západně	k6eAd3	západně
umístěný	umístěný	k2eAgInSc1d1	umístěný
vulkán	vulkán	k1gInSc1	vulkán
Afarské	Afarský	k2eAgFnSc2d1	Afarská
pánve	pánev	k1gFnSc2	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Sopka	sopka	k1gFnSc1	sopka
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
převážně	převážně	k6eAd1	převážně
čedičovými	čedičový	k2eAgFnPc7d1	čedičová
horninami	hornina	k1gFnPc7	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
prokřemeněné	prokřemeněný	k2eAgFnPc1d1	prokřemeněný
(	(	kIx(	(
<g/>
horniny	hornina	k1gFnPc1	hornina
s	s	k7c7	s
vyšším	vysoký	k2eAgInSc7d2	vyšší
obsahem	obsah	k1gInSc7	obsah
SiO	SiO	k1gFnSc1	SiO
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
vulkanické	vulkanický	k2eAgInPc1d1	vulkanický
produkty	produkt	k1gInPc1	produkt
(	(	kIx(	(
<g/>
dómy	dóm	k1gInPc1	dóm
a	a	k8xC	a
lávové	lávový	k2eAgInPc1d1	lávový
proudy	proud	k1gInPc1	proud
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
soustředěné	soustředěný	k2eAgNnSc1d1	soustředěné
podél	podél	k7c2	podél
hřbetu	hřbet	k1gInSc2	hřbet
sopky	sopka	k1gFnSc2	sopka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Dabbajra	Dabbajr	k1gInSc2	Dabbajr
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Www.volcano.si.edu	Www.volcano.si.et	k5eAaPmIp1nS	Www.volcano.si.et
-	-	kIx~	-
štítová	štítový	k2eAgFnSc1d1	štítová
sopka	sopka	k1gFnSc1	sopka
Dabbayra	Dabbayr	k1gInSc2	Dabbayr
na	na	k7c4	na
Global	globat	k5eAaImAgInS	globat
Volcanism	Volcanis	k1gNnSc7	Volcanis
Program	program	k1gInSc1	program
</s>
</p>
