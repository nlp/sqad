<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Holan	Holan	k1gMnSc1	Holan
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1905	[number]	k4	1905
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1980	[number]	k4	1980
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
českých	český	k2eAgMnPc2d1	český
básníků	básník	k1gMnPc2	básník
a	a	k8xC	a
překladatelů	překladatel	k1gMnPc2	překladatel
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
intelektuálně	intelektuálně	k6eAd1	intelektuálně
náročné	náročný	k2eAgFnSc2d1	náročná
reflexivní	reflexivní	k2eAgFnSc2d1	reflexivní
a	a	k8xC	a
meditativní	meditativní	k2eAgFnSc2d1	meditativní
poezie	poezie	k1gFnSc2	poezie
osobitých	osobitý	k2eAgInPc2d1	osobitý
imaginativních	imaginativní	k2eAgInPc2d1	imaginativní
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
nezřídka	nezřídka	k6eAd1	nezřídka
používající	používající	k2eAgInPc4d1	používající
vlastní	vlastní	k2eAgInPc4d1	vlastní
jazykové	jazykový	k2eAgInPc4d1	jazykový
novotvary	novotvar	k1gInPc4	novotvar
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
typická	typický	k2eAgFnSc1d1	typická
enigmatičnost	enigmatičnost	k1gFnSc1	enigmatičnost
<g/>
,	,	kIx,	,
spekulativnost	spekulativnost	k1gFnSc1	spekulativnost
<g/>
,	,	kIx,	,
meditativnost	meditativnost	k1gFnSc1	meditativnost
<g/>
,	,	kIx,	,
spirituálnost	spirituálnost	k1gFnSc1	spirituálnost
<g/>
,	,	kIx,	,
vědomí	vědomí	k1gNnSc1	vědomí
tragiky	tragika	k1gFnSc2	tragika
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
pohrdání	pohrdání	k1gNnSc4	pohrdání
hedonismem	hedonismus	k1gInSc7	hedonismus
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
lapidární	lapidární	k2eAgFnSc1d1	lapidární
gnómičnost	gnómičnost	k1gFnSc1	gnómičnost
<g/>
;	;	kIx,	;
přístupnost	přístupnost	k1gFnSc1	přístupnost
veršů	verš	k1gInPc2	verš
je	být	k5eAaImIp3nS	být
snížena	snížit	k5eAaPmNgFnS	snížit
častými	častý	k2eAgFnPc7d1	častá
intertextovými	intertextová	k1gFnPc7	intertextová
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
kulturněhistorickými	kulturněhistorický	k2eAgFnPc7d1	Kulturněhistorická
aluzemi	aluze	k1gFnPc7	aluze
<g/>
.	.	kIx.	.
</s>
<s>
Básník	básník	k1gMnSc1	básník
Josef	Josef	k1gMnSc1	Josef
Hora	Hora	k1gMnSc1	Hora
jej	on	k3xPp3gMnSc4	on
nazval	nazvat	k5eAaPmAgMnS	nazvat
"	"	kIx"	"
<g/>
alchymikem	alchymik	k1gInSc7	alchymik
slova	slovo	k1gNnSc2	slovo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Používal	používat	k5eAaImAgMnS	používat
pseudonymy	pseudonym	k1gInPc4	pseudonym
a	a	k8xC	a
šifry	šifra	k1gFnPc4	šifra
Ivo	Ivo	k1gMnSc1	Ivo
Holan	Holan	k1gMnSc1	Holan
<g/>
,	,	kIx,	,
Ivo	Ivo	k1gMnSc1	Ivo
Rol	rola	k1gFnPc2	rola
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Scarlet	Scarlet	k1gInSc1	Scarlet
<g/>
,	,	kIx,	,
-Dt	-Dt	k?	-Dt
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
-h-	-	k?	-h-
<g/>
,	,	kIx,	,
H.	H.	kA	H.
V.	V.	kA	V.
<g/>
,	,	kIx,	,
V.	V.	kA	V.
H.	H.	kA	H.
Holan	Holan	k1gMnSc1	Holan
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
se	se	k3xPyFc4	se
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
Bělé	Bělá	k1gFnSc2	Bělá
pod	pod	k7c7	pod
Bezdězem	Bezděz	k1gInSc7	Bezděz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1926	[number]	k4	1926
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Truhlářské	truhlářský	k2eAgFnSc6d1	truhlářská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začaly	začít	k5eAaPmAgInP	začít
jeho	jeho	k3xOp3gInPc1	jeho
první	první	k4xOgInPc1	první
literární	literární	k2eAgInPc1d1	literární
pokusy	pokus	k1gInPc1	pokus
-	-	kIx~	-
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
básnickou	básnický	k2eAgFnSc4d1	básnická
sbírku	sbírka	k1gFnSc4	sbírka
Blouznivý	blouznivý	k2eAgInSc4d1	blouznivý
vějíř	vějíř	k1gInSc4	vějíř
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
vydává	vydávat	k5eAaImIp3nS	vydávat
jako	jako	k9	jako
septimán	septimán	k1gMnSc1	septimán
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
této	tento	k3xDgFnSc6	tento
sbírce	sbírka	k1gFnSc6	sbírka
<g/>
,	,	kIx,	,
poznamenané	poznamenaný	k2eAgFnSc6d1	poznamenaná
poetismem	poetismus	k1gInSc7	poetismus
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
hovořil	hovořit	k5eAaImAgInS	hovořit
jako	jako	k9	jako
o	o	k7c4	o
"	"	kIx"	"
<g/>
hříchu	hřích	k1gInSc2	hřích
mládí	mládí	k1gNnSc2	mládí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
gymnázia	gymnázium	k1gNnSc2	gymnázium
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
přijat	přijmout	k5eAaPmNgMnS	přijmout
jako	jako	k8xC	jako
úředník	úředník	k1gMnSc1	úředník
do	do	k7c2	do
pražského	pražský	k2eAgInSc2d1	pražský
Penzijního	penzijní	k2eAgInSc2d1	penzijní
ústavu	ústav	k1gInSc2	ústav
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
zaměstnán	zaměstnat	k5eAaPmNgMnS	zaměstnat
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
odešel	odejít	k5eAaPmAgMnS	odejít
v	v	k7c6	v
28	[number]	k4	28
letech	let	k1gInPc6	let
pro	pro	k7c4	pro
nevhodnost	nevhodnost	k1gFnSc4	nevhodnost
povolání	povolání	k1gNnSc2	povolání
a	a	k8xC	a
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
předčasně	předčasně	k6eAd1	předčasně
do	do	k7c2	do
penze	penze	k1gFnSc2	penze
a	a	k8xC	a
věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
literatuře	literatura	k1gFnSc3	literatura
a	a	k8xC	a
poezii	poezie	k1gFnSc3	poezie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1934	[number]	k4	1934
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
bydlel	bydlet	k5eAaImAgInS	bydlet
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
v	v	k7c6	v
podkrovním	podkrovní	k2eAgInSc6d1	podkrovní
bytě	byt	k1gInSc6	byt
Hlavovy	Hlavův	k2eAgFnSc2d1	Hlavova
vily	vila	k1gFnSc2	vila
ve	v	k7c6	v
Strašnicích	Strašnice	k1gFnPc6	Strašnice
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
V	v	k7c6	v
Úžlabině	úžlabina	k1gFnSc6	úžlabina
884	[number]	k4	884
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měli	mít	k5eAaImAgMnP	mít
kuchyň	kuchyň	k1gFnSc4	kuchyň
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
pokoj	pokoj	k1gInSc4	pokoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1933	[number]	k4	1933
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
redaktor	redaktor	k1gMnSc1	redaktor
Umělecké	umělecký	k2eAgFnSc2d1	umělecká
besedy	beseda	k1gFnSc2	beseda
v	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
Život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1940	[number]	k4	1940
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
dramaturg	dramaturg	k1gMnSc1	dramaturg
Divadla	divadlo	k1gNnSc2	divadlo
E.	E.	kA	E.
F.	F.	kA	F.
Buriana	Burian	k1gMnSc2	Burian
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
katolickou	katolický	k2eAgFnSc4d1	katolická
církev	církev	k1gFnSc4	církev
a	a	k8xC	a
v	v	k7c6	v
euforii	euforie	k1gFnSc6	euforie
z	z	k7c2	z
ruského	ruský	k2eAgNnSc2d1	ruské
osvobození	osvobození	k1gNnSc2	osvobození
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
důvody	důvod	k1gInPc1	důvod
tohoto	tento	k3xDgInSc2	tento
kroku	krok	k1gInSc2	krok
v	v	k7c6	v
Rudém	rudý	k2eAgNnSc6d1	Rudé
právu	právo	k1gNnSc6	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
dopisu	dopis	k1gInSc6	dopis
anglickým	anglický	k2eAgInSc7d1	anglický
intelektuálům	intelektuál	k1gMnPc3	intelektuál
obhajoval	obhajovat	k5eAaImAgInS	obhajovat
únorový	únorový	k2eAgInSc4d1	únorový
převrat	převrat	k1gInSc4	převrat
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
převratu	převrat	k1gInSc6	převrat
však	však	k9	však
prohlédl	prohlédnout	k5eAaPmAgMnS	prohlédnout
a	a	k8xC	a
znechucen	znechutit	k5eAaPmNgInS	znechutit
novými	nový	k2eAgInPc7d1	nový
poměry	poměr	k1gInPc7	poměr
vrací	vracet	k5eAaImIp3nP	vracet
se	se	k3xPyFc4	se
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1950	[number]	k4	1950
do	do	k7c2	do
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
KSČ	KSČ	kA	KSČ
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
však	však	k9	však
předcházela	předcházet	k5eAaImAgFnS	předcházet
pověstná	pověstný	k2eAgFnSc1d1	pověstná
hádka	hádka	k1gFnSc1	hádka
v	v	k7c6	v
Goldhammerově	Goldhammerův	k2eAgFnSc6d1	Goldhammerův
vinárně	vinárna	k1gFnSc6	vinárna
v	v	k7c6	v
Křemencové	křemencový	k2eAgFnSc6d1	křemencová
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
mezi	mezi	k7c7	mezi
Seifertem	Seifert	k1gMnSc7	Seifert
a	a	k8xC	a
Tauferem	Taufer	k1gMnSc7	Taufer
o	o	k7c4	o
význam	význam	k1gInSc4	význam
Majakovského	Majakovský	k2eAgMnSc2d1	Majakovský
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
Holan	Holan	k1gMnSc1	Holan
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
básníky	básník	k1gMnPc7	básník
také	také	k6eAd1	také
účastnil	účastnit	k5eAaImAgMnS	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
udavačské	udavačský	k2eAgFnSc6d1	udavačská
atmosféře	atmosféra	k1gFnSc6	atmosféra
těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
únoru	únor	k1gInSc6	únor
to	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
pro	pro	k7c4	pro
Holana	Holan	k1gMnSc4	Holan
i	i	k9	i
Seiferta	Seifert	k1gMnSc4	Seifert
za	za	k7c4	za
následek	následek	k1gInSc4	následek
zákaz	zákaz	k1gInSc4	zákaz
publikace	publikace	k1gFnSc2	publikace
nových	nový	k2eAgNnPc2d1	nové
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
omezené	omezený	k2eAgNnSc1d1	omezené
publikování	publikování	k1gNnSc1	publikování
starších	starý	k2eAgFnPc2d2	starší
prací	práce	k1gFnPc2	práce
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
hlavně	hlavně	k9	hlavně
Dík	dík	k7c3	dík
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
a	a	k8xC	a
Rudoarmějci	rudoarmějec	k1gMnPc7	rudoarmějec
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
po	po	k7c4	po
léta	léto	k1gNnPc4	léto
hrubě	hrubě	k6eAd1	hrubě
zkreslovalo	zkreslovat	k5eAaImAgNnS	zkreslovat
dílo	dílo	k1gNnSc1	dílo
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
českých	český	k2eAgMnPc2d1	český
básníků	básník	k1gMnPc2	básník
vůbec	vůbec	k9	vůbec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zastává	zastávat	k5eAaImIp3nS	zastávat
se	se	k3xPyFc4	se
jich	on	k3xPp3gFnPc2	on
František	Františka	k1gFnPc2	Františka
Halas	halas	k1gInSc1	halas
a	a	k8xC	a
píše	psát	k5eAaImIp3nS	psát
dopis	dopis	k1gInSc4	dopis
ministru	ministr	k1gMnSc3	ministr
informací	informace	k1gFnPc2	informace
a	a	k8xC	a
členu	člen	k1gInSc2	člen
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
Václavu	Václav	k1gMnSc3	Václav
Kopeckému	Kopecký	k1gMnSc3	Kopecký
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
věc	věc	k1gFnSc1	věc
nevídaná	vídaný	k2eNgFnSc1d1	nevídaná
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yIgFnSc3	který
nemohu	moct	k5eNaImIp1nS	moct
mlčet	mlčet	k5eAaImF	mlčet
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
přinejmenším	přinejmenším	k6eAd1	přinejmenším
bych	by	kYmCp1nS	by
se	se	k3xPyFc4	se
solidarizoval	solidarizovat	k5eAaImAgMnS	solidarizovat
alespoň	alespoň	k9	alespoň
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
nesvolím	svolit	k5eNaPmIp1nS	svolit
k	k	k7c3	k
vydávání	vydávání	k1gNnSc3	vydávání
svých	svůj	k3xOyFgFnPc2	svůj
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nebude	být	k5eNaImBp3nS	být
vše	všechen	k3xTgNnSc1	všechen
skončeno	skončen	k2eAgNnSc1d1	skončeno
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
<g/>
,	,	kIx,	,
o	o	k7c4	o
pár	pár	k4xCyI	pár
měsíců	měsíc	k1gInPc2	měsíc
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc4	tento
čestný	čestný	k2eAgMnSc1d1	čestný
člověk	člověk	k1gMnSc1	člověk
upřímně	upřímně	k6eAd1	upřímně
slibuje	slibovat	k5eAaImIp3nS	slibovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
tak	tak	k9	tak
nenechá	nechat	k5eNaPmIp3nS	nechat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
marné	marný	k2eAgNnSc1d1	marné
<g/>
,	,	kIx,	,
Holan	Holan	k1gMnSc1	Holan
se	se	k3xPyFc4	se
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
uzavírá	uzavírat	k5eAaImIp3nS	uzavírat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
tuskulu	tuskulum	k1gNnSc6	tuskulum
na	na	k7c6	na
Kampě	Kampa	k1gFnSc6	Kampa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
později	pozdě	k6eAd2	pozdě
najde	najít	k5eAaPmIp3nS	najít
editor	editor	k1gInSc1	editor
a	a	k8xC	a
oddaný	oddaný	k2eAgMnSc1d1	oddaný
přítel	přítel	k1gMnSc1	přítel
Vladimír	Vladimír	k1gMnSc1	Vladimír
Justl	Justl	k1gMnSc1	Justl
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
léty	léto	k1gNnPc7	léto
svou	svůj	k3xOyFgFnSc4	svůj
klauzuru	klauzura	k1gFnSc4	klauzura
opouští	opouštět	k5eAaImIp3nS	opouštět
stále	stále	k6eAd1	stále
zřídkavěji	zřídkavě	k6eAd2	zřídkavě
<g/>
,	,	kIx,	,
na	na	k7c4	na
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
na	na	k7c4	na
celé	celý	k2eAgNnSc4d1	celé
léto	léto	k1gNnSc4	léto
<g/>
,	,	kIx,	,
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
pravidelně	pravidelně	k6eAd1	pravidelně
jen	jen	k9	jen
za	za	k7c7	za
maminkou	maminka	k1gFnSc7	maminka
do	do	k7c2	do
Všenor	Všenora	k1gFnPc2	Všenora
<g/>
.	.	kIx.	.
</s>
<s>
Publikační	publikační	k2eAgInSc1d1	publikační
zákaz	zákaz	k1gInSc1	zákaz
s	s	k7c7	s
několika	několik	k4yIc7	několik
spíše	spíše	k9	spíše
bibliofilskými	bibliofilský	k2eAgFnPc7d1	bibliofilská
výjimkami	výjimka	k1gFnPc7	výjimka
trvá	trvat	k5eAaImIp3nS	trvat
přes	přes	k7c4	přes
celá	celý	k2eAgFnSc1d1	celá
50	[number]	k4	50
<g/>
.	.	kIx.	.
léta	léto	k1gNnPc4	léto
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začínají	začínat	k5eAaImIp3nP	začínat
vycházet	vycházet	k5eAaImF	vycházet
sbírky	sbírka	k1gFnPc4	sbírka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
mezitím	mezitím	k6eAd1	mezitím
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Holan	Holan	k1gMnSc1	Holan
nikdy	nikdy	k6eAd1	nikdy
nepřestal	přestat	k5eNaPmAgMnS	přestat
tvořit	tvořit	k5eAaImF	tvořit
<g/>
,	,	kIx,	,
nezpronevěřil	zpronevěřit	k5eNaPmAgMnS	zpronevěřit
se	se	k3xPyFc4	se
svému	svůj	k3xOyFgNnSc3	svůj
básnickému	básnický	k2eAgNnSc3d1	básnické
poslání	poslání	k1gNnSc3	poslání
<g/>
,	,	kIx,	,
psal	psát	k5eAaImAgMnS	psát
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
údajně	údajně	k6eAd1	údajně
nenapsal	napsat	k5eNaPmAgMnS	napsat
po	po	k7c4	po
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
jedinou	jediný	k2eAgFnSc4d1	jediná
báseň	báseň	k1gFnSc4	báseň
<g/>
,	,	kIx,	,
znepokojovalo	znepokojovat	k5eAaImAgNnS	znepokojovat
ho	on	k3xPp3gMnSc4	on
to	ten	k3xDgNnSc1	ten
<g/>
.	.	kIx.	.
</s>
<s>
Roky	rok	k1gInPc1	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
div	div	k1gInSc1	div
tu	tu	k6eAd1	tu
nežebral	žebrat	k5eNaImAgInS	žebrat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
komentuje	komentovat	k5eAaBmIp3nS	komentovat
verši	verš	k1gInPc7	verš
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Proč	proč	k6eAd1	proč
těžký	těžký	k2eAgInSc1d1	těžký
je	být	k5eAaImIp3nS	být
tvůj	tvůj	k3xOp2gInSc1	tvůj
let	let	k1gInSc1	let
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
pozdí	pozdit	k5eAaImIp3nS	pozdit
<g/>
?	?	kIx.	?
</s>
<s>
Mluvil	mluvit	k5eAaImAgMnS	mluvit
jsem	být	k5eAaImIp1nS	být
patnáct	patnáct	k4xCc4	patnáct
let	léto	k1gNnPc2	léto
do	do	k7c2	do
zdi	zeď	k1gFnSc2	zeď
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Dobrovského	Dobrovského	k2eAgFnSc6d1	Dobrovského
vile	vila	k1gFnSc6	vila
<g/>
,	,	kIx,	,
bývalé	bývalý	k2eAgFnSc6d1	bývalá
koželužně	koželužna	k1gFnSc6	koželužna
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
hrabě	hrabě	k1gMnSc1	hrabě
Nostic	Nostice	k1gFnPc2	Nostice
věnoval	věnovat	k5eAaPmAgMnS	věnovat
svému	svůj	k3xOyFgMnSc3	svůj
věrnému	věrný	k2eAgMnSc3d1	věrný
služebníku	služebník	k1gMnSc3	služebník
"	"	kIx"	"
<g/>
modrému	modré	k1gNnSc3	modré
abbé	abbé	k1gMnPc1	abbé
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
Josefu	Josef	k1gMnSc3	Josef
Dobrovskému	Dobrovský	k1gMnSc3	Dobrovský
přezdívali	přezdívat	k5eAaImAgMnP	přezdívat
(	(	kIx(	(
<g/>
po	po	k7c6	po
emigraci	emigrace	k1gFnSc6	emigrace
Jiřího	Jiří	k1gMnSc2	Jiří
Voskovce	Voskovec	k1gMnSc2	Voskovec
získal	získat	k5eAaPmAgMnS	získat
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
jeho	jeho	k3xOp3gInSc4	jeho
byt	byt	k1gInSc4	byt
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
<g/>
,	,	kIx,	,
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
patře	patro	k1gNnSc6	patro
téhož	týž	k3xTgInSc2	týž
domu	dům	k1gInSc2	dům
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
Kampě	Kampa	k1gFnSc6	Kampa
s	s	k7c7	s
Holanem	Holan	k1gMnSc7	Holan
bydlel	bydlet	k5eAaImAgMnS	bydlet
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
jejich	jejich	k3xOp3gNnSc4	jejich
soužití	soužití	k1gNnSc4	soužití
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nebylo	být	k5eNaImAgNnS	být
zrovna	zrovna	k6eAd1	zrovna
harmonické	harmonický	k2eAgFnPc4d1	harmonická
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyprávějí	vyprávět	k5eAaImIp3nP	vyprávět
anekdoty	anekdota	k1gFnPc1	anekdota
<g/>
.	.	kIx.	.
</s>
<s>
Holan	Holan	k1gMnSc1	Holan
si	se	k3xPyFc3	se
prý	prý	k9	prý
stěžoval	stěžovat	k5eAaImAgMnS	stěžovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zatímco	zatímco	k8xS	zatímco
nemá	mít	k5eNaImIp3nS	mít
z	z	k7c2	z
čeho	co	k3yRnSc2	co
žít	žít	k5eAaImF	žít
<g/>
,	,	kIx,	,
o	o	k7c4	o
jeho	on	k3xPp3gInSc4	on
strop	strop	k1gInSc4	strop
buší	bušit	k5eAaImIp3nP	bušit
kosti	kost	k1gFnPc1	kost
odhozené	odhozený	k2eAgFnPc1d1	odhozená
z	z	k7c2	z
Werichova	Werichův	k2eAgInSc2d1	Werichův
stolu	stol	k1gInSc2	stol
při	při	k7c6	při
bujarých	bujarý	k2eAgInPc6d1	bujarý
večírcích	večírek	k1gInPc6	večírek
apod.	apod.	kA	apod.
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
o	o	k7c6	o
Werichově	Werichův	k2eAgFnSc6d1	Werichova
dobré	dobrý	k2eAgFnSc6d1	dobrá
vůli	vůle	k1gFnSc6	vůle
být	být	k5eAaImF	být
přítelem	přítel	k1gMnSc7	přítel
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
zpočátku	zpočátku	k6eAd1	zpočátku
<g/>
,	,	kIx,	,
a	a	k8xC	a
snaze	snaha	k1gFnSc3	snaha
pomoci	pomoc	k1gFnSc2	pomoc
nejsou	být	k5eNaImIp3nP	být
pochybnosti	pochybnost	k1gFnPc1	pochybnost
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
života	život	k1gInSc2	život
pak	pak	k6eAd1	pak
Holanovi	Holanův	k2eAgMnPc1d1	Holanův
prožili	prožít	k5eAaPmAgMnP	prožít
v	v	k7c6	v
"	"	kIx"	"
<g/>
pražských	pražský	k2eAgFnPc6d1	Pražská
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
"	"	kIx"	"
u	u	k7c2	u
Lužického	lužický	k2eAgInSc2d1	lužický
semináře	seminář	k1gInSc2	seminář
18	[number]	k4	18
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
Mistrova	mistrův	k2eAgFnSc1d1	Mistrova
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
národním	národní	k2eAgMnSc7d1	národní
umělcem	umělec	k1gMnSc7	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Psát	psát	k5eAaImF	psát
přestal	přestat	k5eAaPmAgInS	přestat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zemřela	zemřít	k5eAaPmAgFnS	zemřít
jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
Kateřina	Kateřina	k1gFnSc1	Kateřina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
postižena	postihnout	k5eAaPmNgFnS	postihnout
Downovým	Downův	k2eAgInSc7d1	Downův
syndromem	syndrom	k1gInSc7	syndrom
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
raná	raný	k2eAgFnSc1d1	raná
tvorba	tvorba	k1gFnSc1	tvorba
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
citátem	citát	k1gInSc7	citát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Poezie	poezie	k1gFnSc1	poezie
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
probíjet	probíjet	k5eAaImF	probíjet
pod	pod	k7c4	pod
povrch	povrch	k1gInSc4	povrch
a	a	k8xC	a
luštit	luštit	k5eAaImF	luštit
tajemství	tajemství	k1gNnSc4	tajemství
skrytého	skrytý	k2eAgInSc2d1	skrytý
smyslu	smysl	k1gInSc2	smysl
věci	věc	k1gFnSc2	věc
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
F.	F.	kA	F.
X.	X.	kA	X.
Šalda	Šalda	k1gMnSc1	Šalda
jeho	jeho	k3xOp3gFnSc4	jeho
tvorbu	tvorba	k1gFnSc4	tvorba
charakterizoval	charakterizovat	k5eAaBmAgMnS	charakterizovat
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Abstrakce	abstrakce	k1gFnSc1	abstrakce
umocněná	umocněný	k2eAgFnSc1d1	umocněná
abstrakcí	abstrakce	k1gFnSc7	abstrakce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
básně	báseň	k1gFnPc1	báseň
byly	být	k5eAaImAgFnP	být
otiskovány	otiskovat	k5eAaImNgFnP	otiskovat
také	také	k9	také
v	v	k7c6	v
kulturních	kulturní	k2eAgFnPc6d1	kulturní
a	a	k8xC	a
literárních	literární	k2eAgFnPc6d1	literární
revuích	revue	k1gFnPc6	revue
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byl	být	k5eAaImAgInS	být
Blok	blok	k1gInSc1	blok
<g/>
,	,	kIx,	,
Orientace	orientace	k1gFnSc1	orientace
<g/>
,	,	kIx,	,
Host	host	k1gMnSc1	host
do	do	k7c2	do
domu	dům	k1gInSc2	dům
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
–	–	k?	–
<g/>
69	[number]	k4	69
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
méně	málo	k6eAd2	málo
známý	známý	k2eAgInSc1d1	známý
Kvart	kvart	k1gInSc1	kvart
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
–	–	k?	–
<g/>
48	[number]	k4	48
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Červený	červený	k2eAgInSc1d1	červený
květ	květ	k1gInSc1	květ
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
–	–	k?	–
<g/>
69	[number]	k4	69
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dlouholetým	dlouholetý	k2eAgInSc7d1	dlouholetý
editorem	editor	k1gInSc7	editor
Holanova	Holanův	k2eAgNnSc2d1	Holanovo
díla	dílo	k1gNnSc2	dílo
byl	být	k5eAaImAgMnS	být
Vladimír	Vladimír	k1gMnSc1	Vladimír
Justl	Justl	k1gMnSc1	Justl
<g/>
.	.	kIx.	.
</s>
<s>
Blouznivý	blouznivý	k2eAgInSc1d1	blouznivý
vějíř	vějíř	k1gInSc1	vějíř
–	–	k?	–
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
juvenilní	juvenilní	k2eAgFnSc1d1	juvenilní
prvotina	prvotina	k1gFnSc1	prvotina
ovlivněná	ovlivněný	k2eAgFnSc1d1	ovlivněná
dobovým	dobový	k2eAgInSc7d1	dobový
poetismem	poetismus	k1gInSc7	poetismus
<g/>
.	.	kIx.	.
</s>
<s>
Triumf	triumf	k1gInSc1	triumf
smrti	smrt	k1gFnSc2	smrt
–	–	k?	–
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
–	–	k?	–
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sbírce	sbírka	k1gFnSc6	sbírka
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
naznačen	naznačit	k5eAaPmNgInS	naznačit
další	další	k2eAgInSc1d1	další
Holanův	Holanův	k2eAgInSc1d1	Holanův
vývoj	vývoj	k1gInSc1	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1936	[number]	k4	1936
a	a	k8xC	a
1948	[number]	k4	1948
přepracována	přepracován	k2eAgFnSc1d1	přepracována
a	a	k8xC	a
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgNnPc7d1	jednotlivé
vydáními	vydání	k1gNnPc7	vydání
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
výrazné	výrazný	k2eAgFnPc4d1	výrazná
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Holan	Holan	k1gMnSc1	Holan
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
na	na	k7c4	na
hledání	hledání	k1gNnSc4	hledání
nové	nový	k2eAgFnSc2d1	nová
poetiky	poetika	k1gFnSc2	poetika
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
rozporuplnost	rozporuplnost	k1gFnSc4	rozporuplnost
a	a	k8xC	a
složitost	složitost	k1gFnSc4	složitost
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
došel	dojít	k5eAaPmAgInS	dojít
k	k	k7c3	k
meditativní	meditativní	k2eAgFnSc3d1	meditativní
poezii	poezie	k1gFnSc3	poezie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
odrážela	odrážet	k5eAaImAgFnS	odrážet
ambivalentnost	ambivalentnost	k1gFnSc4	ambivalentnost
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
i	i	k8xC	i
vnějšího	vnější	k2eAgInSc2d1	vnější
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
střetávání	střetávání	k1gNnSc2	střetávání
dobra	dobro	k1gNnSc2	dobro
a	a	k8xC	a
zla	zlo	k1gNnSc2	zlo
<g/>
,	,	kIx,	,
bytí	bytí	k1gNnSc2	bytí
a	a	k8xC	a
nicoty	nicota	k1gFnSc2	nicota
<g/>
,	,	kIx,	,
plynutí	plynutí	k1gNnSc1	plynutí
času	čas	k1gInSc2	čas
a	a	k8xC	a
věčnosti	věčnost	k1gFnSc2	věčnost
<g/>
.	.	kIx.	.
</s>
<s>
Básně	báseň	k1gFnPc1	báseň
jsou	být	k5eAaImIp3nP	být
psané	psaný	k2eAgFnPc1d1	psaná
v	v	k7c6	v
pravidelném	pravidelný	k2eAgInSc6d1	pravidelný
veršovém	veršový	k2eAgInSc6d1	veršový
rytmu	rytmus	k1gInSc6	rytmus
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
jambu	jamb	k1gInSc6	jamb
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
zdánlivá	zdánlivý	k2eAgFnSc1d1	zdánlivá
"	"	kIx"	"
<g/>
nesrozumitelnost	nesrozumitelnost	k1gFnSc1	nesrozumitelnost
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
vykoupena	vykoupit	k5eAaPmNgFnS	vykoupit
uchvacující	uchvacující	k2eAgFnSc7d1	uchvacující
melodičností	melodičnost	k1gFnSc7	melodičnost
a	a	k8xC	a
navozenou	navozený	k2eAgFnSc7d1	navozená
náladou	nálada	k1gFnSc7	nálada
<g/>
,	,	kIx,	,
dojmem	dojem	k1gInSc7	dojem
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
zanechají	zanechat	k5eAaPmIp3nP	zanechat
tyto	tento	k3xDgInPc1	tento
verše	verš	k1gInPc1	verš
ve	v	k7c6	v
vnímavém	vnímavý	k2eAgInSc6d1	vnímavý
čtenáři	čtenář	k1gMnSc3	čtenář
po	po	k7c4	po
jejich	jejich	k3xOp3gNnSc4	jejich
přečtení	přečtení	k1gNnSc4	přečtení
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
jsou	být	k5eAaImIp3nP	být
blízké	blízký	k2eAgMnPc4d1	blízký
expresionismu	expresionismus	k1gInSc2	expresionismus
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tyto	tento	k3xDgFnPc4	tento
sbírky	sbírka	k1gFnPc4	sbírka
<g/>
:	:	kIx,	:
Vanutí	vanutí	k1gNnSc4	vanutí
–	–	k?	–
1932	[number]	k4	1932
Oblouk	oblouk	k1gInSc1	oblouk
–	–	k?	–
1934	[number]	k4	1934
Kameni	kámen	k1gInSc6	kámen
<g/>
,	,	kIx,	,
přicházíš	přicházet	k5eAaImIp2nS	přicházet
<g/>
...	...	k?	...
–	–	k?	–
1937	[number]	k4	1937
Koncem	koncem	k7c2	koncem
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
Holan	Holan	k1gMnSc1	Holan
reagoval	reagovat	k5eAaBmAgMnS	reagovat
na	na	k7c4	na
společensko-politickou	společenskoolitický	k2eAgFnSc4d1	společensko-politická
realitu	realita	k1gFnSc4	realita
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
především	především	k9	především
ve	v	k7c6	v
sbírkách	sbírka	k1gFnPc6	sbírka
reagujících	reagující	k2eAgFnPc6d1	reagující
na	na	k7c4	na
události	událost	k1gFnPc4	událost
kolem	kolem	k7c2	kolem
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
a	a	k8xC	a
počínající	počínající	k2eAgFnSc2d1	počínající
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okoupace	okoupace	k1gFnSc2	okoupace
<g/>
.	.	kIx.	.
</s>
<s>
Září	září	k1gNnSc1	září
–	–	k?	–
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
název	název	k1gInSc1	název
Září	září	k1gNnSc1	září
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
je	být	k5eAaImIp3nS	být
báseň	báseň	k1gFnSc1	báseň
Noc	noc	k1gFnSc1	noc
z	z	k7c2	z
Iliady	Iliada	k1gFnSc2	Iliada
Odpověď	odpověď	k1gFnSc4	odpověď
Francii	Francie	k1gFnSc4	Francie
–	–	k?	–
zde	zde	k6eAd1	zde
přímo	přímo	k6eAd1	přímo
obviňuje	obviňovat	k5eAaImIp3nS	obviňovat
Francii	Francie	k1gFnSc4	Francie
z	z	k7c2	z
mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
zrady	zrada	k1gFnSc2	zrada
<g/>
,	,	kIx,	,
nazývá	nazývat	k5eAaImIp3nS	nazývat
ji	on	k3xPp3gFnSc4	on
přímo	přímo	k6eAd1	přímo
"	"	kIx"	"
<g/>
kurvou	kurva	k1gFnSc7	kurva
<g/>
"	"	kIx"	"
a	a	k8xC	a
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
dopadne	dopadnout	k5eAaPmIp3nS	dopadnout
trest	trest	k1gInSc1	trest
<g/>
.	.	kIx.	.
</s>
<s>
Zabaveno	zabaven	k2eAgNnSc4d1	zabaveno
cenzurou	cenzura	k1gFnSc7	cenzura
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
nemohl	moct	k5eNaImAgInS	moct
svobodně	svobodně	k6eAd1	svobodně
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
jinotajů	jinotaj	k1gInPc2	jinotaj
a	a	k8xC	a
dvojsmyslů	dvojsmysl	k1gInPc2	dvojsmysl
<g/>
,	,	kIx,	,
některá	některý	k3yIgFnSc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
mohla	moct	k5eAaImAgFnS	moct
vyjít	vyjít	k5eAaPmF	vyjít
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
:	:	kIx,	:
Sen	sen	k1gInSc1	sen
–	–	k?	–
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
báseň	báseň	k1gFnSc1	báseň
líčící	líčící	k2eAgFnSc1d1	líčící
v	v	k7c6	v
ponurých	ponurý	k2eAgFnPc6d1	ponurá
vidinách	vidina	k1gFnPc6	vidina
"	"	kIx"	"
<g/>
zaživa	zaživa	k6eAd1	zaživa
pohřbené	pohřbený	k2eAgNnSc1d1	pohřbené
<g/>
"	"	kIx"	"
temné	temný	k2eAgNnSc1d1	temné
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
Prahu	Praha	k1gFnSc4	Praha
<g/>
;	;	kIx,	;
věnováno	věnovat	k5eAaImNgNnS	věnovat
památce	památka	k1gFnSc3	památka
ruského	ruský	k2eAgMnSc2d1	ruský
básníka	básník	k1gMnSc2	básník
V.	V.	kA	V.
Chlebnikova	Chlebnikův	k2eAgInSc2d1	Chlebnikův
<g/>
.	.	kIx.	.
</s>
<s>
Záhřmotí	Záhřmotit	k5eAaPmIp3nS	Záhřmotit
–	–	k?	–
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
věnováno	věnovat	k5eAaImNgNnS	věnovat
Mozartovi	Mozart	k1gMnSc6	Mozart
<g/>
,	,	kIx,	,
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
na	na	k7c4	na
dětství	dětství	k1gNnSc4	dětství
a	a	k8xC	a
matku	matka	k1gFnSc4	matka
<g/>
;	;	kIx,	;
jazykově	jazykově	k6eAd1	jazykově
experimentující	experimentující	k2eAgFnSc1d1	experimentující
lyrika	lyrika	k1gFnSc1	lyrika
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
testament	testament	k1gInSc1	testament
–	–	k?	–
1940	[number]	k4	1940
Zpěv	zpěv	k1gInSc1	zpěv
tříkrálový	tříkrálový	k2eAgInSc1d1	tříkrálový
–	–	k?	–
sbírka	sbírka	k1gFnSc1	sbírka
plná	plný	k2eAgFnSc1d1	plná
jinotajů	jinotaj	k1gInPc2	jinotaj
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
symbol	symbol	k1gInSc1	symbol
velikosti	velikost	k1gFnSc2	velikost
je	být	k5eAaImIp3nS	být
použit	použít	k5eAaPmNgMnS	použít
Karel	Karel	k1gMnSc1	Karel
Hynek	Hynek	k1gMnSc1	Hynek
Mácha	Mácha	k1gMnSc1	Mácha
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
ostatky	ostatek	k1gInPc1	ostatek
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
převezeny	převezen	k2eAgInPc4d1	převezen
z	z	k7c2	z
Litoměřic	Litoměřice	k1gInPc2	Litoměřice
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Chór	chór	k1gInSc1	chór
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
oslavil	oslavit	k5eAaPmAgInS	oslavit
osvobození	osvobození	k1gNnSc4	osvobození
Československa	Československo	k1gNnSc2	Československo
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
obyčejné	obyčejný	k2eAgNnSc1d1	obyčejné
lidské	lidský	k2eAgNnSc1d1	lidské
hrdinství	hrdinství	k1gNnSc1	hrdinství
jejích	její	k3xOp3gMnPc2	její
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Dík	dík	k7c3	dík
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
–	–	k?	–
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
značně	značně	k6eAd1	značně
patetické	patetický	k2eAgNnSc4d1	patetické
poděkování	poděkování	k1gNnSc4	poděkování
za	za	k7c4	za
osvobození	osvobození	k1gNnSc4	osvobození
<g/>
.	.	kIx.	.
</s>
<s>
Panychida	panychida	k1gFnSc1	panychida
–	–	k?	–
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
tryzna	tryzna	k1gFnSc1	tryzna
za	za	k7c4	za
padlé	padlý	k2eAgNnSc4d1	padlé
a	a	k8xC	a
umučené	umučený	k2eAgNnSc4d1	umučené
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Tobě	ty	k3xPp2nSc3	ty
–	–	k?	–
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc1	soubor
básní	báseň	k1gFnPc2	báseň
reagujících	reagující	k2eAgFnPc2d1	reagující
reportážní	reportážní	k2eAgFnSc7d1	reportážní
formou	forma	k1gFnSc7	forma
na	na	k7c6	na
válečné	válečný	k2eAgFnSc6d1	válečná
a	a	k8xC	a
poválečné	poválečný	k2eAgFnSc6d1	poválečná
události	událost	k1gFnSc6	událost
<g/>
.	.	kIx.	.
</s>
<s>
Rudoarmějci	rudoarmějec	k1gMnPc1	rudoarmějec
–	–	k?	–
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
portréty	portrét	k1gInPc4	portrét
prostých	prostý	k2eAgMnPc2d1	prostý
sovětských	sovětský	k2eAgMnPc2d1	sovětský
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
uvedené	uvedený	k2eAgFnPc1d1	uvedená
sbírky	sbírka	k1gFnPc1	sbírka
vyšly	vyjít	k5eAaPmAgFnP	vyjít
později	pozdě	k6eAd2	pozdě
jako	jako	k8xS	jako
komplet	komplet	k6eAd1	komplet
v	v	k7c6	v
souboru	soubor	k1gInSc6	soubor
Dokument	dokument	k1gInSc1	dokument
–	–	k?	–
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
nemohly	moct	k5eNaImAgFnP	moct
Holanovy	Holanův	k2eAgFnPc1d1	Holanova
sbírky	sbírka	k1gFnPc1	sbírka
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
vycházet	vycházet	k5eAaImF	vycházet
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
dílo	dílo	k1gNnSc4	dílo
se	se	k3xPyFc4	se
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
literatuře	literatura	k1gFnSc6	literatura
objevilo	objevit	k5eAaPmAgNnS	objevit
až	až	k9	až
po	po	k7c6	po
r.	r.	kA	r.
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
Básnické	básnický	k2eAgFnPc1d1	básnická
sbírky	sbírka	k1gFnPc1	sbírka
60	[number]	k4	60
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
i	i	k9	i
sbírky	sbírka	k1gFnPc1	sbírka
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
Holan	Holan	k1gMnSc1	Holan
napsal	napsat	k5eAaPmAgMnS	napsat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgInSc2	svůj
zákazu	zákaz	k1gInSc2	zákaz
<g/>
.	.	kIx.	.
</s>
<s>
Zamýšlí	zamýšlet	k5eAaImIp3nS	zamýšlet
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
nedokonalostí	nedokonalost	k1gFnSc7	nedokonalost
lidského	lidský	k2eAgNnSc2d1	lidské
bytí	bytí	k1gNnSc2	bytí
a	a	k8xC	a
základními	základní	k2eAgFnPc7d1	základní
existenciálními	existenciální	k2eAgFnPc7d1	existenciální
otázkami	otázka	k1gFnPc7	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Lyrické	lyrický	k2eAgFnPc1d1	lyrická
sbírky	sbírka	k1gFnPc1	sbírka
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
odpoutané	odpoutaný	k2eAgInPc1d1	odpoutaný
od	od	k7c2	od
reality	realita	k1gFnSc2	realita
a	a	k8xC	a
zaměřené	zaměřený	k2eAgNnSc1d1	zaměřené
na	na	k7c4	na
lidské	lidský	k2eAgNnSc4d1	lidské
nitro	nitro	k1gNnSc4	nitro
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgInPc1d1	častý
jsou	být	k5eAaImIp3nP	být
motivy	motiv	k1gInPc1	motiv
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
absurdity	absurdita	k1gFnSc2	absurdita
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
nepříjemné	příjemný	k2eNgFnSc2d1	nepříjemná
a	a	k8xC	a
živočišné	živočišný	k2eAgFnSc2d1	živočišná
erotiky	erotika	k1gFnSc2	erotika
kontrastující	kontrastující	k2eAgFnSc1d1	kontrastující
s	s	k7c7	s
čistou	čistý	k2eAgFnSc7d1	čistá
mateřskou	mateřský	k2eAgFnSc7d1	mateřská
a	a	k8xC	a
dětskou	dětský	k2eAgFnSc7d1	dětská
láskou	láska	k1gFnSc7	láska
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
tvorba	tvorba	k1gFnSc1	tvorba
velmi	velmi	k6eAd1	velmi
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
generaci	generace	k1gFnSc4	generace
básníků	básník	k1gMnPc2	básník
vstupujících	vstupující	k2eAgMnPc2d1	vstupující
do	do	k7c2	do
literatury	literatura	k1gFnSc2	literatura
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Bolest	bolest	k1gFnSc1	bolest
–	–	k?	–
psáno	psán	k2eAgNnSc1d1	psáno
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
věnováno	věnovat	k5eAaImNgNnS	věnovat
jeho	jeho	k3xOp3gMnPc3	jeho
mrtvým	mrtvý	k2eAgMnPc3d1	mrtvý
přátelům	přítel	k1gMnPc3	přítel
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Hora	Hora	k1gMnSc1	Hora
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Halas	Halas	k1gMnSc1	Halas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Strach	strach	k1gInSc1	strach
Toskána	Toskána	k1gFnSc1	Toskána
1963	[number]	k4	1963
Mozartiana	Mozartiana	k1gFnSc1	Mozartiana
1963	[number]	k4	1963
Noc	noc	k1gFnSc1	noc
s	s	k7c7	s
Hamletem	Hamlet	k1gMnSc7	Hamlet
–	–	k?	–
vydáno	vydat	k5eAaPmNgNnS	vydat
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
napsáno	napsat	k5eAaPmNgNnS	napsat
v	v	k7c6	v
letech	let	k1gInPc6	let
1949	[number]	k4	1949
<g/>
–	–	k?	–
<g/>
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
přepracováno	přepracován	k2eAgNnSc1d1	přepracováno
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
<g/>
;	;	kIx,	;
Holan	Holan	k1gMnSc1	Holan
zde	zde	k6eAd1	zde
konfrontuje	konfrontovat	k5eAaBmIp3nS	konfrontovat
své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
s	s	k7c7	s
Hamletem	Hamlet	k1gMnSc7	Hamlet
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Co	co	k3yQnSc1	co
nás	my	k3xPp1nPc4	my
nyní	nyní	k6eAd1	nyní
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
nás	my	k3xPp1nPc4	my
jednou	jednou	k6eAd1	jednou
zavalí	zavalit	k5eAaPmIp3nS	zavalit
<g/>
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Justl	Justl	k1gMnSc1	Justl
uvedl	uvést	k5eAaPmAgMnS	uvést
tuto	tento	k3xDgFnSc4	tento
skladbu	skladba	k1gFnSc4	skladba
ještě	ještě	k9	ještě
před	před	k7c7	před
knižním	knižní	k2eAgNnSc7d1	knižní
vydáním	vydání	k1gNnSc7	vydání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
vinárně	vinárna	k1gFnSc6	vinárna
Viola	Viola	k1gFnSc1	Viola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
do	do	k7c2	do
konce	konec	k1gInSc2	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
dočkala	dočkat	k5eAaPmAgFnS	dočkat
asi	asi	k9	asi
150	[number]	k4	150
repríz	repríza	k1gFnPc2	repríza
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Noc	noc	k1gFnSc1	noc
s	s	k7c7	s
Ofélií	Ofélie	k1gFnSc7	Ofélie
1970	[number]	k4	1970
–	–	k?	–
bibliofilie	bibliofilie	k1gFnSc2	bibliofilie
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
fragment	fragment	k1gInSc1	fragment
<g/>
,	,	kIx,	,
pendant	pendant	k1gInSc1	pendant
k	k	k7c3	k
Noci	noc	k1gFnSc3	noc
s	s	k7c7	s
Hamletem	Hamlet	k1gMnSc7	Hamlet
<g/>
)	)	kIx)	)
Bajaja	Bajaja	k1gFnSc1	Bajaja
–	–	k?	–
verše	verš	k1gInPc1	verš
psané	psaný	k2eAgInPc1d1	psaný
pro	pro	k7c4	pro
dceru	dcera	k1gFnSc4	dcera
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc1	první
vydání	vydání	k1gNnSc1	vydání
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
názvu	název	k1gInSc2	název
1963	[number]	k4	1963
Na	na	k7c6	na
postupu	postup	k1gInSc6	postup
1964	[number]	k4	1964
Na	na	k7c4	na
sotnách	sotnách	k?	sotnách
1967	[number]	k4	1967
Asklépiovi	Asklépiův	k2eAgMnPc1d1	Asklépiův
kohouta	kohout	k1gMnSc2	kohout
1970	[number]	k4	1970
Předposlední	předposlední	k2eAgInSc1d1	předposlední
1982	[number]	k4	1982
–	–	k?	–
nevyšlo	vyjít	k5eNaPmAgNnS	vyjít
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
Sbohem	sbohem	k0	sbohem
<g/>
?	?	kIx.	?
</s>
<s>
v	v	k7c6	v
pátém	pátý	k4xOgInSc6	pátý
svazku	svazek	k1gInSc6	svazek
Spisů	spis	k1gInPc2	spis
V.	V.	kA	V.
H.	H.	kA	H.
Sbohem	sbohem	k0	sbohem
<g/>
?	?	kIx.	?
</s>
<s>
1982	[number]	k4	1982
Holan	Holan	k1gMnSc1	Holan
napsal	napsat	k5eAaBmAgMnS	napsat
několik	několik	k4yIc4	několik
rozsáhlejších	rozsáhlý	k2eAgFnPc2d2	rozsáhlejší
epických	epický	k2eAgFnPc2d1	epická
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
básnické	básnický	k2eAgInPc4d1	básnický
příběhy	příběh	k1gInPc4	příběh
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
lidské	lidský	k2eAgFnSc2d1	lidská
tragédie	tragédie	k1gFnSc2	tragédie
<g/>
;	;	kIx,	;
ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
symbolem	symbol	k1gInSc7	symbol
lidského	lidský	k2eAgInSc2d1	lidský
údělu	úděl	k1gInSc2	úděl
<g/>
:	:	kIx,	:
Terezka	Terezka	k1gFnSc1	Terezka
Planetová	planetový	k2eAgFnSc1d1	planetová
–	–	k?	–
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc1	příběh
inspirovaný	inspirovaný	k2eAgInSc1d1	inspirovaný
Janáčkovou	Janáčkův	k2eAgFnSc7d1	Janáčkova
črtou	črta	k1gFnSc7	črta
Moje	můj	k3xOp1gNnSc1	můj
děvče	děvče	k1gNnSc1	děvče
z	z	k7c2	z
Tater	Tatra	k1gFnPc2	Tatra
<g/>
.	.	kIx.	.
</s>
<s>
Navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
První	první	k4xOgInSc4	první
testament	testament	k1gInSc4	testament
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
mraku	mrak	k1gInSc2	mrak
–	–	k?	–
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
tragický	tragický	k2eAgInSc1d1	tragický
baladický	baladický	k2eAgInSc1d1	baladický
osud	osud	k1gInSc1	osud
kováře	kovář	k1gMnSc2	kovář
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
–	–	k?	–
první	první	k4xOgNnSc1	první
vydání	vydání	k1gNnSc1	vydání
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
výše	vysoce	k6eAd2	vysoce
uvedené	uvedený	k2eAgFnPc4d1	uvedená
epické	epický	k2eAgFnPc4d1	epická
básně	báseň	k1gFnPc4	báseň
a	a	k8xC	a
11	[number]	k4	11
dalších	další	k2eAgInPc2d1	další
příběhů	příběh	k1gInPc2	příběh
volným	volný	k2eAgInSc7d1	volný
veršem	verš	k1gInSc7	verš
<g/>
.	.	kIx.	.
</s>
<s>
Kolury	kolur	k1gInPc1	kolur
Lemuria	Lemurium	k1gNnSc2	Lemurium
Hadry	hadr	k1gInPc4	hadr
<g/>
,	,	kIx,	,
kosti	kost	k1gFnPc4	kost
<g/>
,	,	kIx,	,
kůže	kůže	k1gFnSc1	kůže
Torzo	torzo	k1gNnSc1	torzo
Sebrané	sebraný	k2eAgInPc1d1	sebraný
spisy	spis	k1gInPc1	spis
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Holana	Holan	k1gMnSc2	Holan
vycházely	vycházet	k5eAaImAgInP	vycházet
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Odeon	odeon	k1gInSc1	odeon
v	v	k7c6	v
letech	let	k1gInPc6	let
1965	[number]	k4	1965
<g/>
–	–	k?	–
<g/>
1988	[number]	k4	1988
péčí	péče	k1gFnPc2	péče
Holanova	Holanův	k2eAgMnSc2d1	Holanův
dvorního	dvorní	k2eAgMnSc2d1	dvorní
editora	editor	k1gMnSc2	editor
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Justla	Justla	k1gMnSc2	Justla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedenácti	jedenáct	k4xCc6	jedenáct
svazcích	svazek	k1gInPc6	svazek
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
autorovo	autorův	k2eAgNnSc4d1	autorovo
základní	základní	k2eAgNnSc4d1	základní
básnické	básnický	k2eAgNnSc4d1	básnické
a	a	k8xC	a
prozaické	prozaický	k2eAgNnSc4d1	prozaické
dílo	dílo	k1gNnSc4	dílo
(	(	kIx(	(
<g/>
sv.	sv.	kA	sv.
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
práce	práce	k1gFnSc2	práce
nezařazené	zařazený	k2eNgFnSc6d1	nezařazená
do	do	k7c2	do
základního	základní	k2eAgNnSc2d1	základní
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
marginálie	marginálie	k1gFnSc2	marginálie
a	a	k8xC	a
publicistiku	publicistika	k1gFnSc4	publicistika
(	(	kIx(	(
<g/>
sv.	sv.	kA	sv.
10	[number]	k4	10
<g/>
)	)	kIx)	)
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
básníkův	básníkův	k2eAgInSc4d1	básníkův
životopis	životopis	k1gInSc4	životopis
z	z	k7c2	z
pera	pero	k1gNnSc2	pero
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Justla	Justla	k1gMnSc2	Justla
a	a	k8xC	a
bibliografii	bibliografie	k1gFnSc4	bibliografie
(	(	kIx(	(
<g/>
sv.	sv.	kA	sv.
11	[number]	k4	11
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spisy	spis	k1gInPc1	spis
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Holana	Holan	k1gMnSc2	Holan
vycházejí	vycházet	k5eAaImIp3nP	vycházet
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Paseka	paseka	k1gFnSc1	paseka
v	v	k7c6	v
ediční	ediční	k2eAgFnSc6d1	ediční
spolupráci	spolupráce	k1gFnSc6	spolupráce
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Justla	Justla	k1gMnSc2	Justla
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
Chalupy	Chalupa	k1gMnSc2	Chalupa
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
koncept	koncept	k1gInSc1	koncept
Sebraných	sebraný	k2eAgInPc2d1	sebraný
spisů	spis	k1gInPc2	spis
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
o	o	k7c4	o
básníkovy	básníkův	k2eAgInPc4d1	básníkův
překlady	překlad	k1gInPc4	překlad
z	z	k7c2	z
cizích	cizí	k2eAgInPc2d1	cizí
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
sv.	sv.	kA	sv.
11	[number]	k4	11
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ediční	ediční	k2eAgInPc4d1	ediční
komentáře	komentář	k1gInPc4	komentář
a	a	k8xC	a
úplnou	úplný	k2eAgFnSc4d1	úplná
bibliografii	bibliografie	k1gFnSc4	bibliografie
(	(	kIx(	(
<g/>
sv.	sv.	kA	sv.
15	[number]	k4	15
<g/>
;	;	kIx,	;
tento	tento	k3xDgInSc1	tento
poslední	poslední	k2eAgInSc1d1	poslední
svazek	svazek	k1gInSc1	svazek
vyjde	vyjít	k5eAaPmIp3nS	vyjít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výbory	výbor	k1gInPc1	výbor
poezie	poezie	k1gFnSc2	poezie
<g/>
:	:	kIx,	:
Noční	noční	k2eAgFnSc1d1	noční
hlídka	hlídka	k1gFnSc1	hlídka
srdce	srdce	k1gNnSc1	srdce
–	–	k?	–
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
po	po	k7c6	po
15	[number]	k4	15
letech	léto	k1gNnPc6	léto
zákazu	zákaz	k1gInSc2	zákaz
vydávání	vydávání	k1gNnSc2	vydávání
<g/>
,	,	kIx,	,
vychází	vycházet	k5eAaImIp3nS	vycházet
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
básníkovy	básníkův	k2eAgFnSc2d1	básníkova
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
tvorby	tvorba	k1gFnSc2	tvorba
Smrt	smrt	k1gFnSc1	smrt
a	a	k8xC	a
sen	sen	k1gInSc1	sen
a	a	k8xC	a
slovo	slovo	k1gNnSc1	slovo
–	–	k?	–
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
podtitul	podtitul	k1gInSc1	podtitul
Z	z	k7c2	z
Máchova	Máchův	k2eAgInSc2d1	Máchův
kraje	kraj	k1gInSc2	kraj
Ptala	ptat	k5eAaImAgFnS	ptat
se	se	k3xPyFc4	se
tě	ty	k3xPp2nSc4	ty
<g/>
...	...	k?	...
–	–	k?	–
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
milostné	milostný	k2eAgFnSc2d1	milostná
lyriky	lyrika	k1gFnSc2	lyrika
Strom	strom	k1gInSc1	strom
kůru	kůra	k1gFnSc4	kůra
shazuje	shazovat	k5eAaImIp3nS	shazovat
–	–	k?	–
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc1	výbor
sestavený	sestavený	k2eAgInSc1d1	sestavený
V.	V.	kA	V.
Rzounkem	Rzounek	k1gMnSc7	Rzounek
<g/>
,	,	kIx,	,
dogmaticky	dogmaticky	k6eAd1	dogmaticky
smýšlejícím	smýšlející	k2eAgMnSc7d1	smýšlející
literárním	literární	k2eAgMnSc7d1	literární
historikem	historik	k1gMnSc7	historik
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ve	v	k7c6	v
výboru	výbor	k1gInSc6	výbor
kladl	klást	k5eAaImAgMnS	klást
důraz	důraz	k1gInSc4	důraz
především	především	k9	především
na	na	k7c4	na
Holanovu	Holanův	k2eAgFnSc4d1	Holanova
tvorbu	tvorba	k1gFnSc4	tvorba
tematicky	tematicky	k6eAd1	tematicky
spjatou	spjatý	k2eAgFnSc4d1	spjatá
s	s	k7c7	s
osvobozením	osvobození	k1gNnSc7	osvobození
Československa	Československo	k1gNnSc2	Československo
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
na	na	k7c4	na
protiválečné	protiválečný	k2eAgInPc4d1	protiválečný
verše	verš	k1gInPc4	verš
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc4	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Mušle	mušle	k1gFnSc2	mušle
<g/>
,	,	kIx,	,
lastury	lastura	k1gFnSc2	lastura
a	a	k8xC	a
škeble	škeble	k1gFnSc2	škeble
<g/>
...	...	k?	...
–	–	k?	–
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
reprezentativní	reprezentativní	k2eAgInSc1d1	reprezentativní
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
Holanova	Holanův	k2eAgNnSc2d1	Holanovo
díla	dílo	k1gNnSc2	dílo
Propast	propast	k1gFnSc4	propast
propasti	propast	k1gFnSc2	propast
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
sbírky	sbírka	k1gFnPc4	sbírka
Sbohem	sbohem	k0	sbohem
<g/>
?	?	kIx.	?
</s>
<s>
a	a	k8xC	a
Předposlední	předposlední	k2eAgInSc4d1	předposlední
Les	les	k1gInSc4	les
bez	bez	k7c2	bez
stromů	strom	k1gInPc2	strom
–	–	k?	–
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc1	výbor
básní	báseň	k1gFnPc2	báseň
psaných	psaný	k2eAgFnPc2d1	psaná
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1949	[number]	k4	1949
–	–	k?	–
1955	[number]	k4	1955
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
Strach	strach	k1gInSc1	strach
<g/>
,	,	kIx,	,
sbírek	sbírka	k1gFnPc2	sbírka
Bolest	bolest	k1gFnSc1	bolest
a	a	k8xC	a
Příběhy	příběh	k1gInPc1	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Básně	báseň	k1gFnPc1	báseň
manifestují	manifestovat	k5eAaBmIp3nP	manifestovat
básníka	básník	k1gMnSc4	básník
pod	pod	k7c7	pod
odpovídajícím	odpovídající	k2eAgNnSc7d1	odpovídající
přízviskem	přízvisko	k1gNnSc7	přízvisko
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
básníka	básník	k1gMnSc2	básník
z	z	k7c2	z
nejtemnějších	temný	k2eAgNnPc2d3	nejtemnější
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
epiky	epika	k1gFnSc2	epika
<g/>
:	:	kIx,	:
Pojď	jít	k5eAaImRp2nS	jít
se	se	k3xPyFc4	se
mnou	já	k3xPp1nSc7	já
do	do	k7c2	do
noci	noc	k1gFnSc2	noc
Bagately	bagatela	k1gFnSc2	bagatela
–	–	k?	–
básně	báseň	k1gFnPc1	báseň
<g/>
,	,	kIx,	,
prózy	próza	k1gFnPc1	próza
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
z	z	k7c2	z
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
eseje	esej	k1gFnPc1	esej
<g/>
,	,	kIx,	,
úvahy	úvaha	k1gFnPc1	úvaha
<g/>
,	,	kIx,	,
vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
<g/>
,	,	kIx,	,
referáty	referát	k1gInPc1	referát
a	a	k8xC	a
glosy	glosa	k1gFnPc1	glosa
<g/>
.	.	kIx.	.
</s>
<s>
Překládal	překládat	k5eAaImAgMnS	překládat
poezii	poezie	k1gFnSc4	poezie
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
ho	on	k3xPp3gMnSc4	on
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
Horův	Horův	k2eAgInSc1d1	Horův
překlad	překlad	k1gInSc1	překlad
Oněgina	Oněgino	k1gNnSc2	Oněgino
<g/>
.	.	kIx.	.
</s>
<s>
Překládal	překládat	k5eAaImAgMnS	překládat
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
(	(	kIx(	(
<g/>
R.	R.	kA	R.
M.	M.	kA	M.
Rilkeho	Rilke	k1gMnSc2	Rilke
<g/>
,	,	kIx,	,
N.	N.	kA	N.
Lenaua	Lenaua	k1gFnSc1	Lenaua
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzštiny	francouzština	k1gFnSc2	francouzština
(	(	kIx(	(
<g/>
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
Baudelaira	Baudelair	k1gMnSc2	Baudelair
<g/>
,	,	kIx,	,
La	la	k1gNnSc4	la
Fontaina	Fontain	k1gMnSc2	Fontain
<g/>
,	,	kIx,	,
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
Vildraca	Vildraca	k1gMnSc1	Vildraca
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
španělštiny	španělština	k1gFnSc2	španělština
(	(	kIx(	(
<g/>
L.	L.	kA	L.
de	de	k?	de
Góngoru	Góngor	k1gInSc2	Góngor
y	y	k?	y
<g />
.	.	kIx.	.
</s>
<s>
Argote	argot	k1gInSc5	argot
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polštiny	polština	k1gFnSc2	polština
(	(	kIx(	(
<g/>
J.	J.	kA	J.
Słowackého	Słowackého	k2eAgMnSc1d1	Słowackého
a	a	k8xC	a
A.	A.	kA	A.
Mickiewicze	Mickiewicze	k1gFnSc1	Mickiewicze
–	–	k?	–
jazyková	jazykový	k2eAgFnSc1d1	jazyková
spolupráce	spolupráce	k1gFnSc1	spolupráce
Josef	Josef	k1gMnSc1	Josef
Matouš	Matouš	k1gMnSc1	Matouš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ruštiny	ruština	k1gFnSc2	ruština
(	(	kIx(	(
<g/>
M.	M.	kA	M.
J.	J.	kA	J.
Lermontova	Lermontův	k2eAgFnSc1d1	Lermontova
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
perštiny	perština	k1gFnSc2	perština
(	(	kIx(	(
<g/>
Nizámího	Nizámí	k2eAgInSc2d1	Nizámí
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
čínštiny	čínština	k1gFnPc1	čínština
<g/>
,	,	kIx,	,
u	u	k7c2	u
exotičtějších	exotický	k2eAgInPc2d2	exotičtější
jazyků	jazyk	k1gInPc2	jazyk
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c6	o
zbásnění	zbásnění	k1gNnSc6	zbásnění
doslovných	doslovný	k2eAgInPc2d1	doslovný
překladů	překlad	k1gInPc2	překlad
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
pořízeny	pořídit	k5eAaPmNgInP	pořídit
jazykovědci	jazykovědec	k1gMnSc6	jazykovědec
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
překlady	překlad	k1gInPc1	překlad
vycházely	vycházet	k5eAaImAgInP	vycházet
často	často	k6eAd1	často
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
malých	malý	k2eAgInPc6d1	malý
nákladech	náklad	k1gInPc6	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Překládal	překládat	k5eAaImAgMnS	překládat
asi	asi	k9	asi
120	[number]	k4	120
básníků	básník	k1gMnPc2	básník
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
160	[number]	k4	160
<g/>
)	)	kIx)	)
ze	z	k7c2	z
16	[number]	k4	16
literatur	literatura	k1gFnPc2	literatura
<g/>
,	,	kIx,	,
překlady	překlad	k1gInPc4	překlad
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
se	se	k3xPyFc4	se
cítil	cítit	k5eAaImAgMnS	cítit
duchovně	duchovně	k6eAd1	duchovně
spřízněn	spřízněn	k2eAgMnSc1d1	spřízněn
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k9	jako
kongeniální	kongeniální	k2eAgInPc1d1	kongeniální
<g/>
.	.	kIx.	.
</s>
<s>
Výbor	výbor	k1gInSc1	výbor
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
Holanových	Holanových	k2eAgInPc2d1	Holanových
překladů	překlad	k1gInPc2	překlad
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Cestou	cesta	k1gFnSc7	cesta
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
úplnosti	úplnost	k1gFnSc6	úplnost
vyšly	vyjít	k5eAaPmAgInP	vyjít
Holanovy	Holanův	k2eAgInPc1d1	Holanův
překlady	překlad	k1gInPc1	překlad
v	v	k7c4	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
svazku	svazek	k1gInSc2	svazek
jeho	jeho	k3xOp3gInPc2	jeho
Spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Paseka	paseka	k1gFnSc1	paseka
v	v	k7c6	v
letech	let	k1gInPc6	let
2007	[number]	k4	2007
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
FORST	FORST	kA	FORST
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Lexikon	lexikon	k1gInSc1	lexikon
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
:	:	kIx,	:
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
,	,	kIx,	,
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
instituce	instituce	k1gFnPc1	instituce
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
I.	I.	kA	I.
H-	H-	k1gFnPc2	H-
<g/>
J.	J.	kA	J.
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
589	[number]	k4	589
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
468	[number]	k4	468
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
231	[number]	k4	231
<g/>
-	-	kIx~	-
<g/>
234	[number]	k4	234
<g/>
.	.	kIx.	.
</s>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Pelc	Pelc	k1gMnSc1	Pelc
<g/>
:	:	kIx,	:
Strom	strom	k1gInSc1	strom
kůru	kůr	k1gInSc2	kůr
shazuje	shazovat	k5eAaImIp3nS	shazovat
<g/>
...	...	k?	...
In	In	k1gFnSc1	In
<g/>
:	:	kIx,	:
Psáno	psán	k2eAgNnSc1d1	psáno
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
</s>
<s>
BMSS-START	BMSS-START	k?	BMSS-START
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
s.	s.	k?	s.
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-86140-28-8	[number]	k4	80-86140-28-8
PUTNA	putna	k1gFnSc1	putna
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
C.	C.	kA	C.
Česká	český	k2eAgFnSc1d1	Česká
katolická	katolický	k2eAgFnSc1d1	katolická
literatura	literatura	k1gFnSc1	literatura
v	v	k7c6	v
kontextech	kontext	k1gInPc6	kontext
:	:	kIx,	:
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Torst	Torst	k1gMnSc1	Torst
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
1390	[number]	k4	1390
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
721	[number]	k4	721
<g/>
-	-	kIx~	-
<g/>
5391	[number]	k4	5391
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
spisovatelé	spisovatel	k1gMnPc1	spisovatel
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
s.	s.	k?	s.
179-183	[number]	k4	179-183
Česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
od	od	k7c2	od
počátků	počátek	k1gInPc2	počátek
k	k	k7c3	k
dnešku	dnešek	k1gInSc3	dnešek
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
NLN	NLN	kA	NLN
1998	[number]	k4	1998
Slovník	slovník	k1gInSc4	slovník
zakázaných	zakázaný	k2eAgMnPc2d1	zakázaný
autorů	autor	k1gMnPc2	autor
1948	[number]	k4	1948
<g/>
-	-	kIx~	-
<g/>
90	[number]	k4	90
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SPN	SPN	kA	SPN
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
s.	s.	k?	s.
142-145	[number]	k4	142-145
seznam	seznam	k1gInSc4	seznam
českých	český	k2eAgMnPc2d1	český
spisovatelů	spisovatel	k1gMnPc2	spisovatel
česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
v	v	k7c6	v
letech	let	k1gInPc6	let
1900	[number]	k4	1900
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
v	v	k7c6	v
letech	let	k1gInPc6	let
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
meditativní	meditativní	k2eAgFnSc1d1	meditativní
poezie	poezie	k1gFnSc1	poezie
spirituální	spirituální	k2eAgFnSc2d1	spirituální
poezie	poezie	k1gFnSc2	poezie
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vladimír	Vladimír	k1gMnSc1	Vladimír
Holan	Holan	k1gMnSc1	Holan
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Vladimír	Vladimír	k1gMnSc1	Vladimír
Holan	Holan	k1gMnSc1	Holan
ve	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Vladimír	Vladimír	k1gMnSc1	Vladimír
Holan	Holan	k1gMnSc1	Holan
Seznam	seznam	k1gInSc4	seznam
děl	dít	k5eAaImAgMnS	dít
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Vladimír	Vladimír	k1gMnSc1	Vladimír
Holan	Holan	k1gMnSc1	Holan
Heslo	heslo	k1gNnSc1	heslo
Vladimír	Vladimír	k1gMnSc1	Vladimír
Holan	Holan	k1gMnSc1	Holan
na	na	k7c6	na
stránce	stránka	k1gFnSc6	stránka
Databazeknih	Databazekniha	k1gFnPc2	Databazekniha
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Vladimír	Vladimír	k1gMnSc1	Vladimír
Holan	Holan	k1gMnSc1	Holan
v	v	k7c6	v
Slovníku	slovník	k1gInSc6	slovník
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
Vladimír	Vladimír	k1gMnSc1	Vladimír
Holan	Holan	k1gMnSc1	Holan
<g/>
:	:	kIx,	:
Básník	básník	k1gMnSc1	básník
v	v	k7c6	v
temných	temný	k2eAgInPc6d1	temný
časech	čas	k1gInPc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Příloha	příloha	k1gFnSc1	příloha
KT	KT	kA	KT
Perspektivy	perspektiva	k1gFnSc2	perspektiva
49	[number]	k4	49
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
Vladimír	Vladimír	k1gMnSc1	Vladimír
Holan	Holan	k1gMnSc1	Holan
-	-	kIx~	-
rejstřík	rejstřík	k1gInSc1	rejstřík
a	a	k8xC	a
frekvenční	frekvenční	k2eAgInSc1d1	frekvenční
slovník	slovník	k1gInSc1	slovník
substantiv	substantivum	k1gNnPc2	substantivum
Zora	Zora	k1gFnSc1	Zora
Dvořáková	Dvořáková	k1gFnSc1	Dvořáková
<g/>
:	:	kIx,	:
Vladimír	Vladimír	k1gMnSc1	Vladimír
Holan	Holan	k1gMnSc1	Holan
<g/>
,	,	kIx,	,
článek	článek	k1gInSc1	článek
<g/>
,	,	kIx,	,
strana	strana	k1gFnSc1	strana
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtletník	čtvrtletník	k1gInSc1	čtvrtletník
nezávislého	závislý	k2eNgInSc2d1	nezávislý
diskusního	diskusní	k2eAgInSc2d1	diskusní
klubu	klub	k1gInSc2	klub
Milady	Milada	k1gFnSc2	Milada
Horákové	Horáková	k1gFnSc2	Horáková
<g/>
:	:	kIx,	:
Masarykův	Masarykův	k2eAgInSc1d1	Masarykův
lid	lid	k1gInSc1	lid
<g/>
,	,	kIx,	,
miladahorakova	miladahorakův	k2eAgFnSc1d1	miladahorakův
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgInS	vydat
Klub	klub	k1gInSc1	klub
Milady	Milada	k1gFnSc2	Milada
Horákové	Horáková	k1gFnSc2	Horáková
<g/>
,	,	kIx,	,
ZŠ	ZŠ	kA	ZŠ
Na	na	k7c6	na
Planině	planina	k1gFnSc6	planina
1393	[number]	k4	1393
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
4	[number]	k4	4
<g/>
;	;	kIx,	;
produkce	produkce	k1gFnSc1	produkce
a	a	k8xC	a
expedice	expedice	k1gFnSc1	expedice
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Eva-Milan	Eva-Milana	k1gFnPc2	Eva-Milana
Nevole	nevole	k1gFnSc2	nevole
<g/>
,	,	kIx,	,
K	k	k7c3	k
Údolí	údolí	k1gNnSc3	údolí
2	[number]	k4	2
<g/>
,	,	kIx,	,
143	[number]	k4	143
00	[number]	k4	00
Praha	Praha	k1gFnSc1	Praha
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Registrováno	registrován	k2eAgNnSc1d1	registrováno
MK	MK	kA	MK
ČR	ČR	kA	ČR
E	E	kA	E
11026	[number]	k4	11026
<g/>
.	.	kIx.	.
</s>
<s>
Ročník	ročník	k1gInSc1	ročník
XXI	XXI	kA	XXI
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
září	září	k1gNnSc1	září
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
PDF	PDF	kA	PDF
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
,	,	kIx,	,
verze	verze	k1gFnSc1	verze
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
2.9	[number]	k4	2.9
<g/>
.2015	.2015	k4	.2015
17	[number]	k4	17
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Stran	stran	k7c2	stran
24	[number]	k4	24
<g/>
.	.	kIx.	.
</s>
<s>
Navštíveno	navštíven	k2eAgNnSc4d1	navštíveno
2017	[number]	k4	2017
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
