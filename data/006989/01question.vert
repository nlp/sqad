<s>
Kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
Vladimír	Vladimír	k1gMnSc1	Vladimír
Holan	Holan	k1gMnSc1	Holan
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
českých	český	k2eAgMnPc2d1	český
básníků	básník	k1gMnPc2	básník
a	a	k8xC	a
překladatelů	překladatel	k1gMnPc2	překladatel
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
často	často	k6eAd1	často
používal	používat	k5eAaImAgMnS	používat
vlastní	vlastní	k2eAgInPc4d1	vlastní
novotvary	novotvar	k1gInPc4	novotvar
<g/>
?	?	kIx.	?
</s>
