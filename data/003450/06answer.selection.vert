<s>
Moták	moták	k1gMnSc1
pochop	pochop	k1gMnSc1
je	být	k5eAaImIp3nS
tažný	tažný	k2eAgInSc4d1
druh	druh	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1
na	na	k7c4
zimoviště	zimoviště	k1gNnSc4
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
a	a	k8xC
Africe	Afrika	k1gFnSc6
odlétá	odlétat	k5eAaPmIp3nS
v	v	k7c6
srpnu	srpen	k1gInSc6
až	až	k8xS
říjnu	říjen	k1gInSc6
a	a	k8xC
ze	z	k7c2
zimovišť	zimoviště	k1gNnPc2
přilétá	přilétat	k5eAaImIp3nS
během	během	k7c2
března	březen	k1gInSc2
a	a	k8xC
dubna	duben	k1gInSc2
<g/>
.	.	kIx.
</s>