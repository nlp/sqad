<p>
<s>
Samarium	samarium	k1gNnSc1	samarium
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Sm	Sm	k1gFnSc2	Sm
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Samarium	samarium	k1gNnSc1	samarium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
měkký	měkký	k2eAgInSc1d1	měkký
stříbřitě	stříbřitě	k6eAd1	stříbřitě
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
přechodný	přechodný	k2eAgInSc1d1	přechodný
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
šestý	šestý	k4xOgInSc1	šestý
člen	člen	k1gInSc1	člen
skupiny	skupina	k1gFnSc2	skupina
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
uplatnění	uplatnění	k1gNnSc1	uplatnění
nalézá	nalézat	k5eAaImIp3nS	nalézat
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
mimořádně	mimořádně	k6eAd1	mimořádně
silných	silný	k2eAgInPc2d1	silný
permanentních	permanentní	k2eAgInPc2d1	permanentní
magnetů	magnet	k1gInPc2	magnet
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
také	také	k9	také
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
speciálních	speciální	k2eAgNnPc2d1	speciální
skel	sklo	k1gNnPc2	sklo
a	a	k8xC	a
keramiky	keramika	k1gFnSc2	keramika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc1d1	základní
fyzikálně-chemické	fyzikálněhemický	k2eAgFnPc1d1	fyzikálně-chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Samarium	samarium	k1gNnSc1	samarium
je	být	k5eAaImIp3nS	být
stříbřitě	stříbřitě	k6eAd1	stříbřitě
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
měkký	měkký	k2eAgInSc1d1	měkký
<g/>
,	,	kIx,	,
vnitřně	vnitřně	k6eAd1	vnitřně
přechodný	přechodný	k2eAgInSc4d1	přechodný
kov	kov	k1gInSc4	kov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chemicky	chemicky	k6eAd1	chemicky
je	být	k5eAaImIp3nS	být
samarium	samarium	k1gNnSc4	samarium
méně	málo	k6eAd2	málo
reaktivní	reaktivní	k2eAgInPc4d1	reaktivní
než	než	k8xS	než
předchozí	předchozí	k2eAgInPc4d1	předchozí
prvky	prvek	k1gInPc4	prvek
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
stálé	stálý	k2eAgNnSc1d1	stálé
a	a	k8xC	a
ke	k	k7c3	k
vzplanutí	vzplanutí	k1gNnSc3	vzplanutí
dochází	docházet	k5eAaImIp3nS	docházet
až	až	k9	až
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
nad	nad	k7c7	nad
150	[number]	k4	150
°	°	k?	°
<g/>
C.	C.	kA	C.
S	s	k7c7	s
vodou	voda	k1gFnSc7	voda
reaguje	reagovat	k5eAaBmIp3nS	reagovat
samarium	samarium	k1gNnSc1	samarium
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
plynného	plynný	k2eAgInSc2d1	plynný
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
běžných	běžný	k2eAgFnPc6d1	běžná
minerálních	minerální	k2eAgFnPc6d1	minerální
kyselinách	kyselina	k1gFnPc6	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
prakticky	prakticky	k6eAd1	prakticky
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
Sm	Sm	k1gFnSc2	Sm
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
jeho	jeho	k3xOp3gFnPc2	jeho
solí	sůl	k1gFnPc2	sůl
jsou	být	k5eAaImIp3nP	být
značně	značně	k6eAd1	značně
podobné	podobný	k2eAgInPc1d1	podobný
sloučeninám	sloučenina	k1gFnPc3	sloučenina
ostatních	ostatní	k2eAgInPc2d1	ostatní
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
a	a	k8xC	a
hliníku	hliník	k1gInSc2	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
prvky	prvek	k1gInPc1	prvek
tvoří	tvořit	k5eAaImIp3nP	tvořit
například	například	k6eAd1	například
vysoce	vysoce	k6eAd1	vysoce
stabilní	stabilní	k2eAgInPc1d1	stabilní
oxidy	oxid	k1gInPc1	oxid
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nereagují	reagovat	k5eNaBmIp3nP	reagovat
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
obtížně	obtížně	k6eAd1	obtížně
se	se	k3xPyFc4	se
redukují	redukovat	k5eAaBmIp3nP	redukovat
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
solí	sůl	k1gFnPc2	sůl
anorganických	anorganický	k2eAgFnPc2d1	anorganická
kyselin	kyselina	k1gFnPc2	kyselina
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgInPc1d1	důležitý
především	především	k9	především
fluoridy	fluorid	k1gInPc1	fluorid
a	a	k8xC	a
fosforečnany	fosforečnan	k1gInPc1	fosforečnan
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
nerozpustnost	nerozpustnost	k1gFnSc1	nerozpustnost
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
oddělení	oddělení	k1gNnSc3	oddělení
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
kovových	kovový	k2eAgInPc2d1	kovový
iontů	ion	k1gInPc2	ion
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
nerozpustnou	rozpustný	k2eNgFnSc7d1	nerozpustná
sloučeninou	sloučenina	k1gFnSc7	sloučenina
je	být	k5eAaImIp3nS	být
šťavelan	šťavelan	k1gInSc1	šťavelan
samaritý	samaritý	k2eAgInSc1d1	samaritý
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
použít	použít	k5eAaPmF	použít
ke	k	k7c3	k
gravimetrickému	gravimetrický	k2eAgNnSc3d1	gravimetrické
stanovení	stanovení	k1gNnSc3	stanovení
těchto	tento	k3xDgInPc2	tento
prvků	prvek	k1gInPc2	prvek
po	po	k7c6	po
jejich	jejich	k3xOp3gFnSc6	jejich
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
separaci	separace	k1gFnSc6	separace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
objevu	objev	k1gInSc2	objev
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1853	[number]	k4	1853
objevil	objevit	k5eAaPmAgMnS	objevit
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
chemik	chemik	k1gMnSc1	chemik
Jean	Jean	k1gMnSc1	Jean
Charles	Charles	k1gMnSc1	Charles
Galissard	Galissard	k1gMnSc1	Galissard
de	de	k?	de
Marignac	Marignac	k1gFnSc4	Marignac
neznámé	známý	k2eNgFnSc2d1	neznámá
emisní	emisní	k2eAgFnSc2d1	emisní
linie	linie	k1gFnSc2	linie
ve	v	k7c6	v
spektru	spektrum	k1gNnSc6	spektrum
didymia	didymium	k1gNnSc2	didymium
a	a	k8xC	a
přiřadil	přiřadit	k5eAaPmAgMnS	přiřadit
je	být	k5eAaImIp3nS	být
doposud	doposud	k6eAd1	doposud
neobjevenému	objevený	k2eNgInSc3d1	neobjevený
prvku	prvek	k1gInSc3	prvek
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Izolaci	izolace	k1gFnSc4	izolace
čistého	čistý	k2eAgInSc2d1	čistý
prvku	prvek	k1gInSc2	prvek
provedl	provést	k5eAaPmAgMnS	provést
teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
francouzský	francouzský	k2eAgMnSc1d1	francouzský
chemik	chemik	k1gMnSc1	chemik
Paul	Paul	k1gMnSc1	Paul
Émile	Émile	k1gFnSc2	Émile
Lecoq	Lecoq	k1gMnSc1	Lecoq
de	de	k?	de
Boisbaudran	Boisbaudran	k1gInSc1	Boisbaudran
<g/>
.	.	kIx.	.
</s>
<s>
Vycházel	vycházet	k5eAaImAgInS	vycházet
přitom	přitom	k6eAd1	přitom
z	z	k7c2	z
minerálu	minerál	k1gInSc2	minerál
samarskitu	samarskit	k1gInSc2	samarskit
o	o	k7c6	o
složení	složení	k1gNnSc6	složení
((	((	k?	((
<g/>
Y	Y	kA	Y
<g/>
,	,	kIx,	,
<g/>
Ce	Ce	k1gMnSc1	Ce
<g/>
,	,	kIx,	,
<g/>
U	U	kA	U
<g/>
,	,	kIx,	,
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
Nb	Nb	k1gFnSc1	Nb
<g/>
,	,	kIx,	,
<g/>
Ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
<g/>
Ti	ty	k3xPp2nSc3	ty
<g/>
)	)	kIx)	)
<g/>
5	[number]	k4	5
<g/>
O	o	k7c4	o
<g/>
16	[number]	k4	16
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvek	prvek	k1gInSc1	prvek
byl	být	k5eAaImAgInS	být
poté	poté	k6eAd1	poté
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
minerálu	minerál	k1gInSc2	minerál
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nesl	nést	k5eAaImAgMnS	nést
jméno	jméno	k1gNnSc4	jméno
ruského	ruský	k2eAgMnSc2d1	ruský
důlního	důlní	k2eAgMnSc2d1	důlní
specialisty	specialista	k1gMnSc2	specialista
<g/>
,	,	kIx,	,
plukovníka	plukovník	k1gMnSc2	plukovník
Samarského	samarský	k2eAgMnSc2d1	samarský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Samarium	samarium	k1gNnSc1	samarium
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
v	v	k7c6	v
koncentraci	koncentrace	k1gFnSc6	koncentrace
asi	asi	k9	asi
6	[number]	k4	6
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
,	,	kIx,	,
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
obsahu	obsah	k1gInSc6	obsah
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
údaje	údaj	k1gInPc1	údaj
chybí	chybit	k5eAaPmIp3nP	chybit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaPmIp3nS	připadat
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
samaria	samarium	k1gNnSc2	samarium
na	na	k7c4	na
100	[number]	k4	100
miliard	miliarda	k4xCgFnPc2	miliarda
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
samarium	samarium	k1gNnSc1	samarium
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Neexistují	existovat	k5eNaImIp3nP	existovat
však	však	k9	však
ani	ani	k9	ani
minerály	minerál	k1gInPc1	minerál
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
některé	některý	k3yIgFnPc1	některý
lanthanoidy	lanthanoida	k1gFnPc1	lanthanoida
(	(	kIx(	(
<g/>
prvky	prvek	k1gInPc1	prvek
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
<g/>
)	)	kIx)	)
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
minerály	minerál	k1gInPc4	minerál
směsné	směsný	k2eAgInPc4d1	směsný
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
prakticky	prakticky	k6eAd1	prakticky
všechny	všechen	k3xTgInPc1	všechen
prvky	prvek	k1gInPc1	prvek
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nejznámější	známý	k2eAgFnSc7d3	nejznámější
patří	patřit	k5eAaImIp3nP	patřit
monazity	monazit	k1gInPc1	monazit
(	(	kIx(	(
<g/>
Ce	Ce	k1gFnSc1	Ce
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
<g/>
,	,	kIx,	,
Th	Th	k1gFnSc1	Th
<g/>
,	,	kIx,	,
Nd	Nd	k1gFnSc1	Nd
<g/>
,	,	kIx,	,
Y	Y	kA	Y
<g/>
)	)	kIx)	)
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
a	a	k8xC	a
xenotim	xenotim	k1gInSc1	xenotim
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
fosforečnany	fosforečnan	k1gInPc1	fosforečnan
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
bastnäsity	bastnäsit	k1gInPc1	bastnäsit
(	(	kIx(	(
<g/>
Ce	Ce	k1gFnSc1	Ce
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
<g/>
,	,	kIx,	,
Y	Y	kA	Y
<g/>
)	)	kIx)	)
<g/>
CO	co	k9	co
<g/>
3	[number]	k4	3
<g/>
F	F	kA	F
<g/>
–	–	k?	–
směsné	směsný	k2eAgInPc4d1	směsný
flourouhličitany	flourouhličitan	k1gInPc4	flourouhličitan
prvků	prvek	k1gInPc2	prvek
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
samarium	samarium	k1gNnSc4	samarium
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
již	již	k6eAd1	již
výše	vysoce	k6eAd2	vysoce
uvedený	uvedený	k2eAgInSc1d1	uvedený
minerál	minerál	k1gInSc1	minerál
samarskit	samarskit	k1gInSc1	samarskit
((	((	k?	((
<g/>
Y	Y	kA	Y
<g/>
,	,	kIx,	,
<g/>
Ce	Ce	k1gMnSc1	Ce
<g/>
,	,	kIx,	,
<g/>
U	U	kA	U
<g/>
,	,	kIx,	,
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
Nb	Nb	k1gFnSc1	Nb
<g/>
,	,	kIx,	,
<g/>
Ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
<g/>
Ti	ty	k3xPp2nSc3	ty
<g/>
)	)	kIx)	)
<g/>
5	[number]	k4	5
<g/>
O	o	k7c4	o
<g/>
16	[number]	k4	16
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velká	velký	k2eAgNnPc1d1	velké
ložiska	ložisko	k1gNnPc1	ložisko
těchto	tento	k3xDgFnPc2	tento
rud	ruda	k1gFnPc2	ruda
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
ve	v	k7c4	v
Skandinávii	Skandinávie	k1gFnSc4	Skandinávie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
zdrojem	zdroj	k1gInSc7	zdroj
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
fosfátové	fosfátový	k2eAgFnPc1d1	fosfátová
suroviny	surovina	k1gFnPc1	surovina
–	–	k?	–
apatity	apatit	k1gInPc1	apatit
z	z	k7c2	z
poloostrova	poloostrov	k1gInSc2	poloostrov
Kola	kolo	k1gNnSc2	kolo
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
</s>
</p>
<p>
<s>
Při	při	k7c6	při
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
výrobě	výroba	k1gFnSc6	výroba
prvků	prvek	k1gInPc2	prvek
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnPc1	jejich
rudy	ruda	k1gFnPc1	ruda
nejprve	nejprve	k6eAd1	nejprve
louží	loužit	k5eAaImIp3nP	loužit
směsí	směs	k1gFnSc7	směs
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
a	a	k8xC	a
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1	chlorovodíková
a	a	k8xC	a
ze	z	k7c2	z
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
roztoku	roztok	k1gInSc2	roztok
solí	solit	k5eAaImIp3nS	solit
se	s	k7c7	s
přídavkem	přídavek	k1gInSc7	přídavek
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
vysráží	vysrážet	k5eAaPmIp3nS	vysrážet
hydroxidy	hydroxid	k1gInPc7	hydroxid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Separace	separace	k1gFnSc1	separace
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
prvků	prvek	k1gInPc2	prvek
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
řadou	řada	k1gFnSc7	řada
různých	různý	k2eAgInPc2d1	různý
postupů	postup	k1gInPc2	postup
–	–	k?	–
kapalinovou	kapalinový	k2eAgFnSc7d1	kapalinová
extrakcí	extrakce	k1gFnSc7	extrakce
<g/>
,	,	kIx,	,
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
ionexových	ionexový	k2eAgFnPc2d1	ionexová
kolon	kolona	k1gFnPc2	kolona
nebo	nebo	k8xC	nebo
selektivním	selektivní	k2eAgNnSc7d1	selektivní
srážením	srážení	k1gNnSc7	srážení
nerozpustných	rozpustný	k2eNgFnPc2d1	nerozpustná
komplexních	komplexní	k2eAgFnPc2d1	komplexní
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příprava	příprava	k1gFnSc1	příprava
čistého	čistý	k2eAgInSc2d1	čistý
kovu	kov	k1gInSc2	kov
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
provádí	provádět	k5eAaImIp3nS	provádět
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
směsi	směs	k1gFnSc2	směs
roztavených	roztavený	k2eAgInPc2d1	roztavený
chloridů	chlorid	k1gInPc2	chlorid
samaritého	samaritý	k2eAgInSc2d1	samaritý
SmCl	SmCla	k1gFnPc2	SmCla
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
CaCl	CaCl	k1gMnSc1	CaCl
<g/>
2	[number]	k4	2
a	a	k8xC	a
sodného	sodný	k2eAgInSc2d1	sodný
NaCl	NaCl	k1gMnSc1	NaCl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
postupech	postup	k1gInPc6	postup
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
i	i	k9	i
redukce	redukce	k1gFnSc1	redukce
oxidu	oxid	k1gInSc2	oxid
samaritého	samaritý	k2eAgMnSc2d1	samaritý
Sm	Sm	k1gMnSc2	Sm
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
3	[number]	k4	3
elementárním	elementární	k2eAgInSc7d1	elementární
lanthanem	lanthan	k1gInSc7	lanthan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sm	Sm	k?	Sm
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
+	+	kIx~	+
2	[number]	k4	2
La	la	k1gNnPc2	la
→	→	k?	→
2	[number]	k4	2
Sm	Sm	k1gFnSc1	Sm
+	+	kIx~	+
La	la	k1gNnSc1	la
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc1	použití
a	a	k8xC	a
sloučeniny	sloučenina	k1gFnPc1	sloučenina
==	==	k?	==
</s>
</p>
<p>
<s>
Malá	Malá	k1gFnSc1	Malá
množství	množství	k1gNnSc2	množství
samaria	samarium	k1gNnSc2	samarium
jsou	být	k5eAaImIp3nP	být
obsažena	obsáhnout	k5eAaPmNgNnP	obsáhnout
v	v	k7c6	v
didymu	didymum	k1gNnSc6	didymum
<g/>
,	,	kIx,	,
směsi	směs	k1gFnSc6	směs
praseodymu	praseodym	k1gInSc2	praseodym
a	a	k8xC	a
neodymu	neodym	k1gInSc2	neodym
<g/>
,	,	kIx,	,
požívané	požívaný	k2eAgFnSc2d1	požívaná
pro	pro	k7c4	pro
odkysličování	odkysličování	k1gNnSc4	odkysličování
tavenin	tavenina	k1gFnPc2	tavenina
kovů	kov	k1gInPc2	kov
díky	díky	k7c3	díky
vysoké	vysoký	k2eAgFnSc3d1	vysoká
afinitě	afinita	k1gFnSc3	afinita
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
ke	k	k7c3	k
kyslíku	kyslík	k1gInSc3	kyslík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
sklářském	sklářský	k2eAgInSc6d1	sklářský
průmyslu	průmysl	k1gInSc6	průmysl
slouží	sloužit	k5eAaImIp3nS	sloužit
přídavky	přídavka	k1gFnPc4	přídavka
samaria	samarium	k1gNnSc2	samarium
do	do	k7c2	do
skloviny	sklovina	k1gFnSc2	sklovina
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
absorpce	absorpce	k1gFnSc2	absorpce
skla	sklo	k1gNnSc2	sklo
pro	pro	k7c4	pro
světlo	světlo	k1gNnSc4	světlo
v	v	k7c6	v
infračervené	infračervený	k2eAgFnSc6d1	infračervená
oblasti	oblast	k1gFnSc6	oblast
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
optických	optický	k2eAgInPc2d1	optický
laserů	laser	k1gInPc2	laser
a	a	k8xC	a
maserů	maser	k1gInPc2	maser
se	se	k3xPyFc4	se
často	často	k6eAd1	často
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
samariem	samarium	k1gNnSc7	samarium
dopované	dopovaný	k2eAgInPc1d1	dopovaný
krystaly	krystal	k1gInPc1	krystal
fluoridu	fluorid	k1gInSc2	fluorid
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
CaF	CaF	k1gFnPc7	CaF
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Katalyzátory	katalyzátor	k1gInPc1	katalyzátor
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
oxidu	oxid	k1gInSc2	oxid
samaria	samarium	k1gNnSc2	samarium
se	se	k3xPyFc4	se
v	v	k7c6	v
chemickém	chemický	k2eAgInSc6d1	chemický
průmyslu	průmysl	k1gInSc6	průmysl
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
dehydrataci	dehydratace	k1gFnSc4	dehydratace
a	a	k8xC	a
dehydrogenaci	dehydrogenace	k1gFnSc4	dehydrogenace
ethanolu	ethanol	k1gInSc2	ethanol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
jaderné	jaderný	k2eAgFnSc6d1	jaderná
energetice	energetika	k1gFnSc6	energetika
se	se	k3xPyFc4	se
slitiny	slitina	k1gFnPc1	slitina
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
samaria	samarium	k1gNnSc2	samarium
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
pro	pro	k7c4	pro
zachycování	zachycování	k1gNnSc4	zachycování
neutronů	neutron	k1gInPc2	neutron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obloukové	obloukový	k2eAgFnPc1d1	oblouková
lampy	lampa	k1gFnPc1	lampa
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
především	především	k6eAd1	především
jako	jako	k8xC	jako
světelné	světelný	k2eAgInPc1d1	světelný
zdroje	zdroj	k1gInPc1	zdroj
při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
filmů	film	k1gInPc2	film
používají	používat	k5eAaImIp3nP	používat
elektrody	elektroda	k1gFnPc4	elektroda
ze	z	k7c2	z
slitin	slitina	k1gFnPc2	slitina
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
samaria	samarium	k1gNnSc2	samarium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Permanentní	permanentní	k2eAgInPc1d1	permanentní
magnety	magnet	k1gInPc1	magnet
===	===	k?	===
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
nejsilnějšími	silný	k2eAgMnPc7d3	nejsilnější
známými	známý	k1gMnPc7	známý
permanentní	permanentní	k2eAgFnSc2d1	permanentní
magnety	magnet	k1gInPc1	magnet
materiály	materiál	k1gInPc1	materiál
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
neodymu	neodym	k1gInSc2	neodym
o	o	k7c6	o
složení	složení	k1gNnSc6	složení
Nd	Nd	k1gFnSc2	Nd
<g/>
2	[number]	k4	2
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
14	[number]	k4	14
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
magnety	magnet	k1gInPc1	magnet
složené	složený	k2eAgInPc1d1	složený
ze	z	k7c2	z
samaria	samarium	k1gNnSc2	samarium
a	a	k8xC	a
kobaltu	kobalt	k1gInSc2	kobalt
stále	stále	k6eAd1	stále
prakticky	prakticky	k6eAd1	prakticky
nejvíce	nejvíce	k6eAd1	nejvíce
vyráběnými	vyráběný	k2eAgInPc7d1	vyráběný
extrémně	extrémně	k6eAd1	extrémně
silnými	silný	k2eAgInPc7d1	silný
permanentními	permanentní	k2eAgInPc7d1	permanentní
magnety	magnet	k1gInPc7	magnet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Složení	složení	k1gNnSc1	složení
těchto	tento	k3xDgInPc2	tento
magnetů	magnet	k1gInPc2	magnet
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
uváděno	uvádět	k5eAaImNgNnS	uvádět
jako	jako	k8xC	jako
SmCo	SmCo	k1gNnSc1	SmCo
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
i	i	k9	i
materiál	materiál	k1gInSc1	materiál
Sm	Sm	k1gFnSc2	Sm
<g/>
2	[number]	k4	2
<g/>
Co	co	k9	co
<g/>
17	[number]	k4	17
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
mít	mít	k5eAaImF	mít
ještě	ještě	k9	ještě
lepší	dobrý	k2eAgFnPc4d2	lepší
magnetické	magnetický	k2eAgFnPc4d1	magnetická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
předností	přednost	k1gFnSc7	přednost
Sm-Co	Sm-Co	k6eAd1	Sm-Co
magnetů	magnet	k1gInPc2	magnet
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc4	jejich
použitelnost	použitelnost	k1gFnSc4	použitelnost
v	v	k7c6	v
širokém	široký	k2eAgInSc6d1	široký
oboru	obor	k1gInSc6	obor
teplot	teplota	k1gFnPc2	teplota
<g/>
,	,	kIx,	,
prakticky	prakticky	k6eAd1	prakticky
jsou	být	k5eAaImIp3nP	být
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
účinné	účinný	k2eAgFnSc2d1	účinná
i	i	k8xC	i
za	za	k7c2	za
teplot	teplota	k1gFnPc2	teplota
kolem	kolem	k7c2	kolem
300	[number]	k4	300
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Curieova	Curieův	k2eAgFnSc1d1	Curieova
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
až	až	k9	až
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
700	[number]	k4	700
–	–	k?	–
800	[number]	k4	800
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
Praktická	praktický	k2eAgFnSc1d1	praktická
výroba	výroba	k1gFnSc1	výroba
těchto	tento	k3xDgInPc2	tento
magnetů	magnet	k1gInPc2	magnet
započala	započnout	k5eAaPmAgFnS	započnout
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
magnety	magnet	k1gInPc1	magnet
prakticky	prakticky	k6eAd1	prakticky
používány	používat	k5eAaImNgInP	používat
v	v	k7c6	v
počítačové	počítačový	k2eAgFnSc6d1	počítačová
technice	technika	k1gFnSc6	technika
v	v	k7c6	v
záznamových	záznamový	k2eAgFnPc6d1	záznamová
hlavách	hlava	k1gFnPc6	hlava
pevných	pevný	k2eAgInPc2d1	pevný
disků	disk	k1gInPc2	disk
nebo	nebo	k8xC	nebo
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
malých	malý	k2eAgInPc2d1	malý
mikrofonů	mikrofon	k1gInPc2	mikrofon
a	a	k8xC	a
reproduktorů	reproduktor	k1gInPc2	reproduktor
ve	v	k7c6	v
sluchátkách	sluchátko	k1gNnPc6	sluchátko
a	a	k8xC	a
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgFnPc6d1	další
aplikacích	aplikace	k1gFnPc6	aplikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nevýhody	nevýhoda	k1gFnPc1	nevýhoda
a	a	k8xC	a
rizika	riziko	k1gNnPc1	riziko
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Výrobní	výrobní	k2eAgFnSc1d1	výrobní
cena	cena	k1gFnSc1	cena
samariových	samariový	k2eAgInPc2d1	samariový
magnetů	magnet	k1gInPc2	magnet
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
u	u	k7c2	u
neodymových	neodymová	k1gFnPc2	neodymová
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
pro	pro	k7c4	pro
vyšší	vysoký	k2eAgFnSc4d2	vyšší
cenu	cena	k1gFnSc4	cena
samaria	samarium	k1gNnSc2	samarium
i	i	k8xC	i
kobaltu	kobalt	k1gInSc2	kobalt
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
neodymem	neodym	k1gInSc7	neodym
a	a	k8xC	a
železem	železo	k1gNnSc7	železo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Materiál	materiál	k1gInSc1	materiál
těchto	tento	k3xDgInPc2	tento
magnetů	magnet	k1gInPc2	magnet
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
křehký	křehký	k2eAgInSc1d1	křehký
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
rozbít	rozbít	k5eAaPmF	rozbít
nejen	nejen	k6eAd1	nejen
mechanickým	mechanický	k2eAgInSc7d1	mechanický
úderem	úder	k1gInSc7	úder
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
při	při	k7c6	při
náhlém	náhlý	k2eAgNnSc6d1	náhlé
vystavení	vystavení	k1gNnSc6	vystavení
silnému	silný	k2eAgNnSc3d1	silné
magnetickému	magnetický	k2eAgNnSc3d1	magnetické
poli	pole	k1gNnSc3	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
vysoká	vysoký	k2eAgFnSc1d1	vysoká
magnetická	magnetický	k2eAgFnSc1d1	magnetická
síla	síla	k1gFnSc1	síla
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
vymazání	vymazání	k1gNnSc2	vymazání
dat	datum	k1gNnPc2	datum
na	na	k7c6	na
magnetických	magnetický	k2eAgNnPc6d1	magnetické
záznamových	záznamový	k2eAgNnPc6d1	záznamové
mediích	medium	k1gNnPc6	medium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přitažlivá	přitažlivý	k2eAgFnSc1d1	přitažlivá
síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
náhlém	náhlý	k2eAgNnSc6d1	náhlé
přiblížení	přiblížení	k1gNnSc6	přiblížení
magnetu	magnet	k1gInSc2	magnet
k	k	k7c3	k
ferromagnetickému	ferromagnetický	k2eAgInSc3d1	ferromagnetický
materiálu	materiál	k1gInSc3	materiál
dokáže	dokázat	k5eAaPmIp3nS	dokázat
způsobit	způsobit	k5eAaPmF	způsobit
citlivá	citlivý	k2eAgNnPc1d1	citlivé
poranění	poranění	k1gNnPc1	poranění
pokožky	pokožka	k1gFnSc2	pokožka
nebo	nebo	k8xC	nebo
svalové	svalový	k2eAgFnSc2d1	svalová
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mezi	mezi	k7c7	mezi
magnetem	magnet	k1gInSc7	magnet
a	a	k8xC	a
přitahovaným	přitahovaný	k2eAgInSc7d1	přitahovaný
předmětem	předmět	k1gInSc7	předmět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
</s>
</p>
<p>
<s>
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
1961	[number]	k4	1961
</s>
</p>
<p>
<s>
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwood	k1gInSc1	Greenwood
–	–	k?	–
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc2	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
samarium	samarium	k1gNnSc4	samarium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
samarium	samarium	k1gNnSc4	samarium
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
