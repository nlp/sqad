<s desamb="1">
Nejsou	být	k5eNaImIp3nP
plnovýznamovým	plnovýznamový	k2eAgInSc7d1
slovním	slovní	k2eAgInSc7d1
druhem	druh	k1gInSc7
(	(	kIx(
<g/>
odborně	odborně	k6eAd1
se	se	k3xPyFc4
označují	označovat	k5eAaImIp3nP
jako	jako	k8xS
synsémantika	synsémantikum	k1gNnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
pomáhají	pomáhat	k5eAaImIp3nP
vytvářet	vytvářet	k5eAaImF
fráze	fráze	k1gFnPc4
a	a	k8xC
modifikovat	modifikovat	k5eAaBmF
vztahy	vztah	k1gInPc4
mezi	mezi	k7c7
větnými	větný	k2eAgMnPc7d1
členy	člen	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předložky	předložka	k1gFnPc1
stojí	stát	k5eAaImIp3nP
před	před	k7c7
podstatnými	podstatný	k2eAgNnPc7d1
jmény	jméno	k1gNnPc7
<g/>
,	,	kIx,
zájmeny	zájmeno	k1gNnPc7
a	a	k8xC
číslovkami	číslovka	k1gFnPc7
<g/>
.	.	kIx.
</s>