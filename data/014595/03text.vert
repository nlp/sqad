<s>
Předložka	předložka	k1gFnSc1
</s>
<s>
Předložka	předložka	k1gFnSc1
(	(	kIx(
<g/>
lat.	lat.	kA
praepositio	praepositio	k1gMnSc1
<g/>
;	;	kIx,
v	v	k7c6
některých	některý	k3yIgInPc6
jazycích	jazyk	k1gInPc6
záložka	záložka	k1gFnSc1
<g/>
,	,	kIx,
lat.	lat.	k?
postpositio	postpositio	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
neohebný	ohebný	k2eNgInSc1d1
slovní	slovní	k2eAgInSc1d1
druh	druh	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předložky	předložka	k1gFnPc1
stojí	stát	k5eAaImIp3nP
před	před	k7c7
podstatnými	podstatný	k2eAgNnPc7d1
jmény	jméno	k1gNnPc7
<g/>
,	,	kIx,
zájmeny	zájmeno	k1gNnPc7
a	a	k8xC
číslovkami	číslovka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejsou	být	k5eNaImIp3nP
plnovýznamovým	plnovýznamový	k2eAgInSc7d1
slovním	slovní	k2eAgInSc7d1
druhem	druh	k1gInSc7
(	(	kIx(
<g/>
odborně	odborně	k6eAd1
se	se	k3xPyFc4
označují	označovat	k5eAaImIp3nP
jako	jako	k8xS
synsémantika	synsémantikum	k1gNnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
pomáhají	pomáhat	k5eAaImIp3nP
vytvářet	vytvářet	k5eAaImF
fráze	fráze	k1gFnPc4
a	a	k8xC
modifikovat	modifikovat	k5eAaBmF
vztahy	vztah	k1gInPc4
mezi	mezi	k7c7
větnými	větný	k2eAgMnPc7d1
členy	člen	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předložka	předložka	k1gFnSc1
sama	sám	k3xTgFnSc1
o	o	k7c6
sobě	sebe	k3xPyFc6
není	být	k5eNaImIp3nS
větným	větný	k2eAgInSc7d1
členem	člen	k1gInSc7
<g/>
,	,	kIx,
stává	stávat	k5eAaImIp3nS
se	se	k3xPyFc4
jím	on	k3xPp3gNnSc7
teprve	teprve	k6eAd1
ve	v	k7c6
spojení	spojení	k1gNnSc6
s	s	k7c7
příslušným	příslušný	k2eAgInSc7d1
výrazem	výraz	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
flektivních	flektivní	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
čeština	čeština	k1gFnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
každá	každý	k3xTgFnSc1
předložka	předložka	k1gFnSc1
pojí	pojíst	k5eAaPmIp3nS,k5eAaImIp3nS
s	s	k7c7
určitým	určitý	k2eAgInSc7d1
pádem	pád	k1gInSc7
nebo	nebo	k8xC
více	hodně	k6eAd2
pády	pád	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jiných	jiný	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
mohou	moct	k5eAaImIp3nP
předložky	předložka	k1gFnPc1
systém	systém	k1gInSc4
pádů	pád	k1gInPc2
plně	plně	k6eAd1
nahradit	nahradit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
původu	původ	k1gInSc2
rozlišujeme	rozlišovat	k5eAaImIp1nP
předložky	předložka	k1gFnPc1
primární	primární	k2eAgFnSc2d1
(	(	kIx(
<g/>
též	též	k9
původní	původní	k2eAgFnSc1d1
<g/>
,	,	kIx,
v	v	k7c6
češtině	čeština	k1gFnSc6
např.	např.	kA
na	na	k7c6
<g/>
,	,	kIx,
v	v	k7c6
<g/>
,	,	kIx,
do	do	k7c2
<g/>
,	,	kIx,
z	z	k7c2
<g/>
,	,	kIx,
k	k	k7c3
,	,	kIx,
u	u	k7c2
<g/>
)	)	kIx)
a	a	k8xC
sekundární	sekundární	k2eAgMnSc1d1
(	(	kIx(
<g/>
též	též	k9
nepůvodní	původní	k2eNgMnSc1d1
<g/>
,	,	kIx,
např.	např.	kA
kolem	kolo	k1gNnSc7
<g/>
,	,	kIx,
díky	dík	k1gInPc7
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
vznikly	vzniknout	k5eAaPmAgFnP
z	z	k7c2
jiných	jiný	k2eAgInPc2d1
slovních	slovní	k2eAgInPc2d1
druhů	druh	k1gInPc2
a	a	k8xC
ustálených	ustálený	k2eAgFnPc2d1
frází	fráze	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Postavení	postavení	k1gNnSc1
ve	v	k7c6
větě	věta	k1gFnSc6
</s>
<s>
Předložka	předložka	k1gFnSc1
stojí	stát	k5eAaImIp3nS
před	před	k7c7
podstatným	podstatný	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
<g/>
,	,	kIx,
zájmenem	zájmeno	k1gNnSc7
nebo	nebo	k8xC
číslovkou	číslovka	k1gFnSc7
<g/>
,	,	kIx,
ke	k	k7c3
kterým	který	k3yIgMnPc3,k3yRgMnPc3,k3yQgMnPc3
se	se	k3xPyFc4
vztahuje	vztahovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
podstatné	podstatný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
rozvito	rozvit	k2eAgNnSc4d1
přívlastky	přívlastek	k1gInPc1
<g/>
,	,	kIx,
předložka	předložka	k1gFnSc1
se	se	k3xPyFc4
klade	klást	k5eAaImIp3nS
před	před	k7c4
celý	celý	k2eAgInSc4d1
takto	takto	k6eAd1
rozvitý	rozvitý	k2eAgInSc4d1
větný	větný	k2eAgInSc4d1
člen	člen	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příklady	příklad	k1gInPc4
<g/>
:	:	kIx,
</s>
<s>
o	o	k7c6
městě	město	k1gNnSc6
<g/>
,	,	kIx,
o	o	k7c6
něm	on	k3xPp3gNnSc6
<g/>
,	,	kIx,
o	o	k7c6
pěti	pět	k4xCc6
</s>
<s>
o	o	k7c6
velmi	velmi	k6eAd1
starém	starý	k2eAgNnSc6d1
městě	město	k1gNnSc6
</s>
<s>
V	v	k7c6
některých	některý	k3yIgInPc6
jazycích	jazyk	k1gInPc6
je	být	k5eAaImIp3nS
pociťována	pociťován	k2eAgFnSc1d1
silná	silný	k2eAgFnSc1d1
vazba	vazba	k1gFnSc1
mezi	mezi	k7c7
předložkou	předložka	k1gFnSc7
a	a	k8xC
slovesem	sloveso	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
např.	např.	kA
v	v	k7c6
anglické	anglický	k2eAgFnSc6d1
vedlejší	vedlejší	k2eAgFnSc6d1
větě	věta	k1gFnSc6
nebo	nebo	k8xC
otázce	otázka	k1gFnSc6
obvykle	obvykle	k6eAd1
nestojí	stát	k5eNaImIp3nS
předložka	předložka	k1gFnSc1
před	před	k7c7
vztažným	vztažný	k2eAgInSc7d1
<g/>
/	/	kIx~
<g/>
tázacím	tázací	k2eAgNnSc7d1
zájmenem	zájmeno	k1gNnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
za	za	k7c7
slovesem	sloveso	k1gNnSc7
na	na	k7c6
konci	konec	k1gInSc6
věty	věta	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
What	What	k1gInSc1
is	is	k?
it	it	k?
about	about	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
–	–	k?
O	o	k7c4
čem	co	k3yInSc6,k3yRnSc6,k3yQnSc6
to	ten	k3xDgNnSc4
je	být	k5eAaImIp3nS
<g/>
?	?	kIx.
</s>
<s>
the	the	k?
house	house	k1gNnSc1
I	i	k9
live	liv	k1gInSc2
in	in	k?
/	/	kIx~
the	the	k?
house	house	k1gNnSc1
which	which	k1gMnSc1
I	i	k8xC
live	live	k1gFnSc1
in	in	k?
<g/>
–	–	k?
dům	dům	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
bydlím	bydlet	k5eAaImIp1nS
</s>
<s>
V	v	k7c6
některých	některý	k3yIgInPc6
jazycích	jazyk	k1gInPc6
(	(	kIx(
<g/>
např.	např.	kA
v	v	k7c6
maďarštině	maďarština	k1gFnSc6
<g/>
)	)	kIx)
existují	existovat	k5eAaImIp3nP
tzv.	tzv.	kA
záložky	záložek	k1gInPc1
(	(	kIx(
<g/>
postpozice	postpozice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
plní	plnit	k5eAaImIp3nP
stejnou	stejný	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
jako	jako	k8xC,k8xS
předložky	předložka	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
stojí	stát	k5eAaImIp3nS
až	až	k9
za	za	k7c7
podstatným	podstatný	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
<g/>
:	:	kIx,
sampon	sampon	k1gNnSc1
korpásodás	korpásodása	k1gFnPc2
ellen	ellna	k1gFnPc2
–	–	k?
šampón	šampón	k?
proti	proti	k7c3
lupům	lup	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
latině	latina	k1gFnSc6
mohou	moct	k5eAaImIp3nP
mít	mít	k5eAaImF
některé	některý	k3yIgInPc4
záložky	záložek	k1gInPc4
formu	forma	k1gFnSc4
přípony	přípona	k1gFnSc2
<g/>
,	,	kIx,
například	například	k6eAd1
nobiscum	nobiscum	k1gNnSc1
(	(	kIx(
<g/>
s	s	k7c7
námi	my	k3xPp1nPc7
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
alternativa	alternativa	k1gFnSc1
k	k	k7c3
cum	cum	k?
nobis	nobis	k1gFnSc3
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
i	i	k9
v	v	k7c6
turečtině	turečtina	k1gFnSc6
<g/>
,	,	kIx,
finštině	finština	k1gFnSc6
atd.	atd.	kA
</s>
<s>
Vazby	vazba	k1gFnPc1
předložek	předložka	k1gFnPc2
a	a	k8xC
záložek	záložka	k1gFnPc2
</s>
<s>
Ve	v	k7c6
flektivních	flektivní	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
(	(	kIx(
<g/>
např.	např.	kA
v	v	k7c6
češtině	čeština	k1gFnSc6
<g/>
)	)	kIx)
se	se	k3xPyFc4
předložka	předložka	k1gFnSc1
obvykle	obvykle	k6eAd1
pojí	pojíst	k5eAaPmIp3nS,k5eAaImIp3nS
s	s	k7c7
určitým	určitý	k2eAgInSc7d1
pádem	pád	k1gInSc7
<g/>
:	:	kIx,
</s>
<s>
na	na	k7c6
ulici	ulice	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
domě	dům	k1gInSc6
<g/>
,	,	kIx,
do	do	k7c2
města	město	k1gNnSc2
<g/>
,	,	kIx,
z	z	k7c2
okna	okno	k1gNnSc2
</s>
<s>
Předložka	předložka	k1gFnSc1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
pojit	pojit	k5eAaImF
s	s	k7c7
více	hodně	k6eAd2
než	než	k8xS
jedním	jeden	k4xCgInSc7
pádem	pád	k1gInSc7
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
však	však	k9
vyjadřují	vyjadřovat	k5eAaImIp3nP
různé	různý	k2eAgInPc4d1
významy	význam	k1gInPc4
–	–	k?
typicky	typicky	k6eAd1
např.	např.	kA
rozlišení	rozlišení	k1gNnSc1
místa	místo	k1gNnSc2
a	a	k8xC
směru	směr	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
na	na	k7c4
stůl	stůl	k1gInSc4
(	(	kIx(
<g/>
ak.	ak.	k?
<g/>
,	,	kIx,
kam	kam	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
x	x	k?
na	na	k7c6
stole	stol	k1gInSc6
(	(	kIx(
<g/>
lok.	lok.	k?
<g/>
,	,	kIx,
kde	kde	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
</s>
<s>
Obdobně	obdobně	k6eAd1
například	například	k6eAd1
i	i	k9
v	v	k7c6
němčině	němčina	k1gFnSc6
<g/>
:	:	kIx,
</s>
<s>
in	in	k?
der	drát	k5eAaImRp2nS
Stadt	Stadt	k1gInSc1
(	(	kIx(
<g/>
dat	datum	k1gNnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
–	–	k?
ve	v	k7c6
městě	město	k1gNnSc6
x	x	k?
in	in	k?
die	die	k?
Stadt	Stadt	k1gInSc1
(	(	kIx(
<g/>
ak.	ak.	k?
<g/>
)	)	kIx)
–	–	k?
do	do	k7c2
města	město	k1gNnSc2
</s>
<s>
V	v	k7c6
maďarštině	maďarština	k1gFnSc6
se	se	k3xPyFc4
záložky	záložka	k1gFnPc1
pojí	pojit	k5eAaImIp3nP
nejčastěji	často	k6eAd3
se	s	k7c7
základním	základní	k2eAgInSc7d1
pádem	pád	k1gInSc7
(	(	kIx(
<g/>
nominativem	nominativ	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
systémem	systém	k1gInSc7
pádů	pád	k1gInPc2
se	se	k3xPyFc4
zpravidla	zpravidla	k6eAd1
nekombinují	kombinovat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záložky	záložka	k1gFnPc1
plní	plnit	k5eAaImIp3nP
podobnou	podobný	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
jako	jako	k8xC,k8xS
pádové	pádový	k2eAgFnPc4d1
koncovky	koncovka	k1gFnPc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
s	s	k7c7
tím	ten	k3xDgInSc7
rozdílem	rozdíl	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
nepodléhají	podléhat	k5eNaImIp3nP
samohláskové	samohláskový	k2eAgFnSc3d1
harmonii	harmonie	k1gFnSc3
(	(	kIx(
<g/>
mají	mít	k5eAaImIp3nP
pouze	pouze	k6eAd1
jeden	jeden	k4xCgInSc4
tvar	tvar	k1gInSc4
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
písmu	písmo	k1gNnSc6
stojí	stát	k5eAaImIp3nS
samostatně	samostatně	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
az	az	k?
asztalon	asztalon	k1gInSc1
–	–	k?
na	na	k7c6
stole	stol	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
széken	széken	k1gInSc1
–	–	k?
na	na	k7c6
židli	židle	k1gFnSc6
(	(	kIx(
<g/>
pádové	pádový	k2eAgFnSc2d1
koncovky	koncovka	k1gFnSc2
superessivu	superessiv	k1gInSc2
<g/>
;	;	kIx,
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
az	az	k?
asztalra	asztalra	k1gFnSc1
–	–	k?
na	na	k7c4
stůl	stůl	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
székre	székr	k1gInSc5
–	–	k?
na	na	k7c6
židli	židle	k1gFnSc6
(	(	kIx(
<g/>
pádové	pádový	k2eAgFnSc2d1
koncovky	koncovka	k1gFnSc2
sublativu	sublativ	k1gInSc2
<g/>
;	;	kIx,
směr	směr	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
az	az	k?
astal	astal	k1gInSc1
alatt	alatt	k1gInSc1
–	–	k?
pod	pod	k7c7
stolem	stol	k1gInSc7
<g/>
,	,	kIx,
a	a	k8xC
szék	szék	k6eAd1
alatt	alatt	k2eAgInSc1d1
–	–	k?
pod	pod	k7c7
židlí	židle	k1gFnSc7
(	(	kIx(
<g/>
záložka	záložka	k1gFnSc1
<g/>
;	;	kIx,
místo	místo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
az	az	k?
astal	astal	k1gInSc1
alá	alá	k?
–	–	k?
pod	pod	k7c4
stůl	stůl	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
szék	szék	k1gInSc1
alá	alá	k?
–	–	k?
pod	pod	k7c4
židli	židle	k1gFnSc4
(	(	kIx(
<g/>
záložka	záložka	k1gFnSc1
<g/>
;	;	kIx,
směr	směr	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
některých	některý	k3yIgInPc6
jazycích	jazyk	k1gInPc6
(	(	kIx(
<g/>
angličtina	angličtina	k1gFnSc1
<g/>
,	,	kIx,
švédština	švédština	k1gFnSc1
<g/>
,	,	kIx,
francouzština	francouzština	k1gFnSc1
<g/>
)	)	kIx)
systém	systém	k1gInSc1
předložek	předložka	k1gFnPc2
zcela	zcela	k6eAd1
nahrazuje	nahrazovat	k5eAaImIp3nS
systém	systém	k1gInSc1
pádů	pád	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příklad	příklad	k1gInSc1
z	z	k7c2
angličtiny	angličtina	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
of	of	k?
a	a	k8xC
house	house	k1gNnSc1
–	–	k?
domu	dům	k1gInSc2
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
the	the	k?
house	house	k1gNnSc1
–	–	k?
k	k	k7c3
domu	dům	k1gInSc3
<g/>
,	,	kIx,
about	about	k1gInSc4
a	a	k8xC
house	house	k1gNnSc4
–	–	k?
o	o	k7c6
domě	dům	k1gInSc6
<g/>
,	,	kIx,
with	with	k1gInSc1
a	a	k8xC
house	house	k1gNnSc1
–	–	k?
s	s	k7c7
domem	dům	k1gInSc7
<g/>
,	,	kIx,
by	by	kYmCp3nS
a	a	k9
house	house	k1gNnSc1
–	–	k?
domem	dům	k1gInSc7
</s>
<s>
Volba	volba	k1gFnSc1
předložky	předložka	k1gFnSc2
primárně	primárně	k6eAd1
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
slovese	sloveso	k1gNnSc6
<g/>
,	,	kIx,
tj.	tj.	kA
sloveso	sloveso	k1gNnSc1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
pojit	pojit	k5eAaImF
jen	jen	k9
s	s	k7c7
určitými	určitý	k2eAgFnPc7d1
předložkami	předložka	k1gFnPc7
<g/>
:	:	kIx,
počítat	počítat	k5eAaImF
s	s	k7c7
něčím	něco	k3yInSc7
<g/>
,	,	kIx,
spoléhat	spoléhat	k5eAaImF
na	na	k7c4
něco	něco	k3yInSc4
<g/>
,	,	kIx,
uvažovat	uvažovat	k5eAaImF
o	o	k7c6
něčem	něco	k3yInSc6
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
české	český	k2eAgFnPc1d1
předložky	předložka	k1gFnPc1
</s>
<s>
seznam	seznam	k1gInSc1
latinských	latinský	k2eAgInPc2d1
gramatických	gramatický	k2eAgInPc2d1
pojmů	pojem	k1gInPc2
</s>
<s>
nezlomitelná	zlomitelný	k2eNgFnSc1d1
mezera	mezera	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
předložka	předložka	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Kategorie	kategorie	k1gFnPc1
Předložky	předložka	k1gFnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Slovní	slovní	k2eAgInPc1d1
druhy	druh	k1gInPc1
</s>
<s>
podstatné	podstatný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
•	•	k?
přídavné	přídavný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
•	•	k?
zájmeno	zájmeno	k1gNnSc4
•	•	k?
číslovka	číslovka	k1gFnSc1
•	•	k?
sloveso	sloveso	k1gNnSc4
•	•	k?
příslovce	příslovce	k1gNnSc2
•	•	k?
předložka	předložka	k1gFnSc1
•	•	k?
spojka	spojka	k1gFnSc1
•	•	k?
částice	částice	k1gFnSc1
•	•	k?
citoslovce	citoslovce	k1gNnSc1
jméno	jméno	k1gNnSc4
</s>
<s>
obecné	obecný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
vlastní	vlastní	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
bionymum	bionymum	k1gInSc1
</s>
<s>
antroponymum	antroponymum	k1gNnSc1
(	(	kIx(
<g/>
rodné	rodný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
/	/	kIx~
křestní	křestní	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
•	•	k?
hypokoristikon	hypokoristikon	k1gNnSc1
•	•	k?
příjmí	příjmí	k?
•	•	k?
příjmení	příjmení	k1gNnSc2
•	•	k?
přezdívka	přezdívka	k1gFnSc1
•	•	k?
jméno	jméno	k1gNnSc1
po	po	k7c6
chalupě	chalupa	k1gFnSc6
•	•	k?
fiktonymum	fiktonymum	k1gInSc1
(	(	kIx(
<g/>
pseudonym	pseudonym	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
obyvatelské	obyvatelský	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
•	•	k?
rodinné	rodinný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
•	•	k?
rodové	rodový	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
•	•	k?
etnonymum	etnonymum	k1gNnSc1
•	•	k?
theonymum	theonymum	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
zoonymum	zoonymum	k1gInSc1
•	•	k?
fytonymum	fytonymum	k1gInSc1
abionymum	abionymum	k1gInSc1
</s>
<s>
toponymum	toponymum	k1gNnSc1
(	(	kIx(
<g/>
choronymum	choronymum	k1gInSc1
•	•	k?
oikonymum	oikonymum	k1gInSc1
•	•	k?
anoikonymum	anoikonymum	k1gInSc1
–	–	k?
urbanonymum	urbanonymum	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
kosmonymum	kosmonymum	k1gInSc1
/	/	kIx~
astronymum	astronymum	k1gInSc1
•	•	k?
chrématonymum	chrématonymum	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
exonymum	exonymum	k1gInSc1
•	•	k?
endonymum	endonymum	k1gInSc1
•	•	k?
cizí	cizit	k5eAaImIp3nS
jméno	jméno	k1gNnSc4
•	•	k?
standardizované	standardizovaný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Jazyk	jazyk	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
PSH	PSH	kA
<g/>
:	:	kIx,
6859	#num#	k4
</s>
