<s>
Diova	Diův	k2eAgFnSc1d1	Diova
hudební	hudební	k2eAgFnSc1d1	hudební
kariéra	kariéra	k1gFnSc1	kariéra
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
když	když	k8xS	když
několik	několik	k4yIc1	několik
Cortlandských	Cortlandský	k2eAgMnPc2d1	Cortlandský
hudebníků	hudebník	k1gMnPc2	hudebník
založilo	založit	k5eAaPmAgNnS	založit
skupinu	skupina	k1gFnSc4	skupina
The	The	k1gMnPc2	The
Vegas	Vegas	k1gInSc4	Vegas
Kings	Kingsa	k1gFnPc2	Kingsa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
založení	založení	k1gNnSc6	založení
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
Ronnie	Ronnie	k1gFnPc4	Ronnie
and	and	k?	and
the	the	k?	the
Rumblers	Rumblers	k1gInSc1	Rumblers
<g/>
.	.	kIx.	.
</s>
