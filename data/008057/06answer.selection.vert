<s>
iPhone	iPhon	k1gMnSc5	iPhon
je	být	k5eAaImIp3nS	být
produkt	produkt	k1gInSc1	produkt
společnosti	společnost	k1gFnSc2	společnost
Apple	Apple	kA	Apple
<g/>
,	,	kIx,	,
smartphone	smartphon	k1gMnSc5	smartphon
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
chytrý	chytrý	k2eAgInSc4d1	chytrý
mobilní	mobilní	k2eAgInSc4d1	mobilní
telefon	telefon	k1gInSc4	telefon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
spojuje	spojovat	k5eAaImIp3nS	spojovat
funkce	funkce	k1gFnSc2	funkce
mobilního	mobilní	k2eAgInSc2d1	mobilní
telefonu	telefon	k1gInSc2	telefon
s	s	k7c7	s
kapesním	kapesní	k2eAgInSc7d1	kapesní
počítačem	počítač	k1gInSc7	počítač
<g/>
.	.	kIx.	.
</s>
