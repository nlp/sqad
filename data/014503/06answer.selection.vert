<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
obnovena	obnoven	k2eAgFnSc1d1	obnovena
branná	branný	k2eAgFnSc1d1	Branná
povinnost	povinnost	k1gFnSc1	povinnost
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
bylo	být	k5eAaImAgNnS	být
urychleno	urychlen	k2eAgNnSc4d1	urychleno
obnovení	obnovení	k1gNnSc4	obnovení
německé	německý	k2eAgFnSc2d1	německá
armády	armáda	k1gFnSc2	armáda
(	(	kIx(	(
<g/>
Wehrmacht	wehrmacht	k1gInSc1	wehrmacht
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
bylo	být	k5eAaImAgNnS	být
připojeno	připojen	k2eAgNnSc1d1	připojeno
Sársko	Sársko	k1gNnSc1	Sársko
<g/>
.	.	kIx.	.
</s>
