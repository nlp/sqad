<s>
Pravidlům	pravidlo	k1gNnPc3	pravidlo
správného	správný	k2eAgNnSc2d1	správné
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
,	,	kIx,	,
usuzování	usuzování	k1gNnSc2	usuzování
a	a	k8xC	a
argumentace	argumentace	k1gFnSc2	argumentace
je	být	k5eAaImIp3nS	být
věnována	věnován	k2eAgFnSc1d1	věnována
řada	řada	k1gFnSc1	řada
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
označovaných	označovaný	k2eAgNnPc2d1	označované
jako	jako	k8xS	jako
Organon	organon	k1gNnSc4	organon
čili	čili	k8xC	čili
nástroj	nástroj	k1gInSc4	nástroj
<g/>
,	,	kIx,	,
rozumí	rozumět	k5eAaImIp3nS	rozumět
se	se	k3xPyFc4	se
právě	právě	k9	právě
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
