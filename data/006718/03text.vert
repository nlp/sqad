<s>
Vietnam	Vietnam	k1gInSc1	Vietnam
(	(	kIx(	(
<g/>
vietnamsky	vietnamsky	k6eAd1	vietnamsky
Việ	Việ	k1gMnSc1	Việ
Nam	Nam	k1gMnSc1	Nam
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celým	celý	k2eAgInSc7d1	celý
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
Vietnamská	vietnamský	k2eAgFnSc1d1	vietnamská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
vietnamsky	vietnamsky	k6eAd1	vietnamsky
Cộ	Cộ	k1gMnSc2	Cộ
hò	hò	k?	hò
Xã	Xã	k1gMnSc2	Xã
hộ	hộ	k?	hộ
chủ	chủ	k?	chủ
nghĩ	nghĩ	k?	nghĩ
Việ	Việ	k1gMnSc1	Việ
Nam	Nam	k1gMnSc1	Nam
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
zkratka	zkratka	k1gFnSc1	zkratka
VSR	VSR	kA	VSR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
poloostrova	poloostrov	k1gInSc2	poloostrov
Zadní	zadní	k2eAgFnSc2d1	zadní
Indie	Indie	k1gFnSc2	Indie
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Jihočínského	jihočínský	k2eAgNnSc2d1	Jihočínské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
sousedé	soused	k1gMnPc1	soused
jsou	být	k5eAaImIp3nP	být
Kambodža	Kambodža	k1gFnSc1	Kambodža
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Laos	Laos	k1gInSc1	Laos
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
90	[number]	k4	90
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Hanoj	Hanoj	k1gFnSc1	Hanoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
vládne	vládnout	k5eAaImIp3nS	vládnout
komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
soukromé	soukromý	k2eAgNnSc4d1	soukromé
podnikání	podnikání	k1gNnSc4	podnikání
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
vietnamština	vietnamština	k1gFnSc1	vietnamština
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k9	jako
Vietnam	Vietnam	k1gInSc1	Vietnam
byla	být	k5eAaImAgFnS	být
osidlována	osidlovat	k5eAaImNgFnS	osidlovat
již	již	k6eAd1	již
od	od	k7c2	od
paleolitu	paleolit	k1gInSc2	paleolit
(	(	kIx(	(
<g/>
kultura	kultura	k1gFnSc1	kultura
Hò	Hò	k1gFnSc1	Hò
Bì	Bì	k1gFnSc1	Bì
<g/>
,	,	kIx,	,
kultura	kultura	k1gFnSc1	kultura
Bắ	Bắ	k1gFnSc1	Bắ
Sơ	Sơ	k1gFnSc1	Sơ
<g/>
)	)	kIx)	)
a	a	k8xC	a
některá	některý	k3yIgNnPc1	některý
archeologická	archeologický	k2eAgNnPc1d1	Archeologické
naleziště	naleziště	k1gNnPc1	naleziště
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Thanh	Thanha	k1gFnPc2	Thanha
Hoa	Hoa	k1gFnSc2	Hoa
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
údajně	údajně	k6eAd1	údajně
stará	starý	k2eAgFnSc1d1	stará
až	až	k6eAd1	až
několik	několik	k4yIc1	několik
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Archeologové	archeolog	k1gMnPc1	archeolog
spojují	spojovat	k5eAaImIp3nP	spojovat
začátky	začátek	k1gInPc4	začátek
vietnamské	vietnamský	k2eAgFnSc2d1	vietnamská
civilizace	civilizace	k1gFnSc2	civilizace
s	s	k7c7	s
pozdním	pozdní	k2eAgInSc7d1	pozdní
neolitem	neolit	k1gInSc7	neolit
<g/>
,	,	kIx,	,
ranou	raný	k2eAgFnSc7d1	raná
dobou	doba	k1gFnSc7	doba
bronzovou	bronzový	k2eAgFnSc7d1	bronzová
a	a	k8xC	a
s	s	k7c7	s
kulturou	kultura	k1gFnSc7	kultura
Phung-nguyen	Phungguyna	k1gFnPc2	Phung-nguyna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sídlila	sídlit	k5eAaImAgFnS	sídlit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
provincie	provincie	k1gFnSc2	provincie
Vinh	Vinh	k1gInSc4	Vinh
Phu	Phu	k1gFnSc2	Phu
současného	současný	k2eAgInSc2d1	současný
Vietnamu	Vietnam	k1gInSc2	Vietnam
někdy	někdy	k6eAd1	někdy
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2000	[number]	k4	2000
a	a	k8xC	a
1400	[number]	k4	1400
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Někdy	někdy	k6eAd1	někdy
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1200	[number]	k4	1200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
pak	pak	k6eAd1	pak
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
kultury	kultura	k1gFnSc2	kultura
Dong	dong	k1gInSc1	dong
Son	son	k1gInSc1	son
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
pozoruhodná	pozoruhodný	k2eAgFnSc1d1	pozoruhodná
především	především	k6eAd1	především
díky	díky	k7c3	díky
svým	svůj	k3xOyFgInPc3	svůj
propracovaným	propracovaný	k2eAgInPc3d1	propracovaný
kotlům	kotel	k1gInPc3	kotel
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k6eAd1	právě
bronzové	bronzový	k2eAgFnPc4d1	bronzová
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
nářadí	nářadí	k1gNnPc4	nářadí
a	a	k8xC	a
bubínky	bubínek	k1gInPc4	bubínek
nám	my	k3xPp1nPc3	my
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
na	na	k7c4	na
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
signalizuje	signalizovat	k5eAaImIp3nS	signalizovat
původní	původní	k2eAgInSc1d1	původní
zdroj	zdroj	k1gInSc1	zdroj
technologie	technologie	k1gFnSc2	technologie
bronzových	bronzový	k2eAgInPc2d1	bronzový
odlitků	odlitek	k1gInPc2	odlitek
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
nalezeno	naleznout	k5eAaPmNgNnS	naleznout
mnoho	mnoho	k4c1	mnoho
malých	malý	k2eAgInPc2d1	malý
starověkých	starověký	k2eAgInPc2d1	starověký
měděných	měděný	k2eAgInPc2d1	měděný
dolů	dol	k1gInPc2	dol
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známý	k2eAgFnSc1d1	známá
je	být	k5eAaImIp3nS	být
také	také	k9	také
jistá	jistý	k2eAgFnSc1d1	jistá
podobnost	podobnost	k1gFnSc1	podobnost
mezi	mezi	k7c7	mezi
místy	místo	k1gNnPc7	místo
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Don	dona	k1gFnPc2	dona
Son	son	k1gInSc1	son
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
podobné	podobný	k2eAgInPc4d1	podobný
znaky	znak	k1gInPc4	znak
můžeme	moct	k5eAaImIp1nP	moct
zahrnout	zahrnout	k5eAaPmF	zahrnout
např.	např.	kA	např.
výskyt	výskyt	k1gInSc4	výskyt
rakví	rakev	k1gFnSc7	rakev
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
lodě	loď	k1gFnPc1	loď
<g/>
,	,	kIx,	,
podobné	podobný	k2eAgFnPc1d1	podobná
pohřební	pohřební	k2eAgFnPc1d1	pohřební
nádoby	nádoba	k1gFnPc1	nádoba
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
také	také	k6eAd1	také
např.	např.	kA	např.
černání	černání	k1gNnSc1	černání
zubů	zub	k1gInPc2	zub
nebo	nebo	k8xC	nebo
žvýkání	žvýkání	k1gNnSc3	žvýkání
arakových	arakový	k2eAgInPc2d1	arakový
ořechů	ořech	k1gInPc2	ořech
<g/>
.	.	kIx.	.
</s>
<s>
Legendární	legendární	k2eAgFnSc1d1	legendární
dynastie	dynastie	k1gFnSc1	dynastie
Hồ	Hồ	k1gFnSc1	Hồ
Bà	Bà	k1gFnSc1	Bà
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
mnoha	mnoho	k4c3	mnoho
Vietnamci	Vietnamec	k1gMnPc1	Vietnamec
za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
prvního	první	k4xOgInSc2	první
vietnamského	vietnamský	k2eAgInSc2d1	vietnamský
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
známého	známý	k1gMnSc4	známý
jako	jako	k8xC	jako
Văn	Văn	k1gMnSc1	Văn
Lang	Lang	k1gMnSc1	Lang
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
257	[number]	k4	257
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Thụ	Thụ	k1gMnSc1	Thụ
Phán	Phán	k1gMnSc1	Phán
porazil	porazit	k5eAaPmAgMnS	porazit
posledního	poslední	k2eAgMnSc4d1	poslední
krále	král	k1gMnSc4	král
této	tento	k3xDgFnSc2	tento
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
,	,	kIx,	,
sjednotil	sjednotit	k5eAaPmAgInS	sjednotit
původní	původní	k2eAgInPc4d1	původní
kmeny	kmen	k1gInPc4	kmen
Lạ	Lạ	k1gFnSc2	Lạ
Việ	Việ	k1gFnPc2	Việ
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
kmeny	kmen	k1gInPc7	kmen
Âu	Âu	k1gFnSc4	Âu
Việ	Việ	k1gFnSc2	Việ
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
nový	nový	k2eAgInSc1d1	nový
kmen	kmen	k1gInSc1	kmen
Âu	Âu	k1gMnSc2	Âu
Lạ	Lạ	k1gMnSc2	Lạ
<g/>
,	,	kIx,	,
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
novým	nový	k2eAgMnSc7d1	nový
panovníkem	panovník	k1gMnSc7	panovník
jménem	jméno	k1gNnSc7	jméno
An	An	k1gMnSc1	An
Dư	Dư	k1gMnSc1	Dư
Vư	Vư	k1gMnSc1	Vư
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
207	[number]	k4	207
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
pak	pak	k6eAd1	pak
čínský	čínský	k2eAgMnSc1d1	čínský
generál	generál	k1gMnSc1	generál
jménem	jméno	k1gNnSc7	jméno
Čao	čao	k0	čao
Tchuo	Tchuo	k6eAd1	Tchuo
tohoto	tento	k3xDgMnSc4	tento
panovníka	panovník	k1gMnSc4	panovník
porazil	porazit	k5eAaPmAgInS	porazit
a	a	k8xC	a
původní	původní	k2eAgInSc1d1	původní
kmen	kmen	k1gInSc1	kmen
Âu	Âu	k1gMnSc1	Âu
Lạ	Lạ	k1gMnSc1	Lạ
přetransformoval	přetransformovat	k5eAaPmAgMnS	přetransformovat
v	v	k7c4	v
kmen	kmen	k1gInSc4	kmen
Nanyue	Nanyu	k1gFnSc2	Nanyu
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
111	[number]	k4	111
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
kmen	kmen	k1gInSc1	kmen
spojen	spojen	k2eAgInSc1d1	spojen
s	s	k7c7	s
čínskou	čínský	k2eAgFnSc7d1	čínská
říší	říš	k1gFnSc7	říš
pod	pod	k7c7	pod
nadvládou	nadvláda	k1gFnSc7	nadvláda
dynastie	dynastie	k1gFnSc2	dynastie
Chan	Chana	k1gFnPc2	Chana
<g/>
.	.	kIx.	.
</s>
<s>
Číňané	Číňan	k1gMnPc1	Číňan
vládli	vládnout	k5eAaImAgMnP	vládnout
poměrně	poměrně	k6eAd1	poměrně
tvrdě	tvrdě	k6eAd1	tvrdě
a	a	k8xC	a
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
se	se	k3xPyFc4	se
o	o	k7c4	o
sinizaci	sinizace	k1gFnSc4	sinizace
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
vzpourám	vzpoura	k1gFnPc3	vzpoura
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Vietnam	Vietnam	k1gInSc1	Vietnam
na	na	k7c4	na
přechodnou	přechodný	k2eAgFnSc4d1	přechodná
dobu	doba	k1gFnSc4	doba
osamostatnil	osamostatnit	k5eAaPmAgMnS	osamostatnit
<g/>
.	.	kIx.	.
</s>
<s>
Tisícileté	tisíciletý	k2eAgNnSc1d1	tisícileté
období	období	k1gNnSc1	období
čínské	čínský	k2eAgFnSc2d1	čínská
nadvlády	nadvláda	k1gFnSc2	nadvláda
nad	nad	k7c7	nad
územím	území	k1gNnSc7	území
Vietnamu	Vietnam	k1gInSc2	Vietnam
trvalo	trvat	k5eAaImAgNnS	trvat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
938	[number]	k4	938
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Ngô	Ngô	k1gMnPc1	Ngô
Quyề	Quyề	k1gFnSc2	Quyề
taktickým	taktický	k2eAgInSc7d1	taktický
bojem	boj	k1gInSc7	boj
porazil	porazit	k5eAaPmAgMnS	porazit
čínskou	čínský	k2eAgFnSc4d1	čínská
armádu	armáda	k1gFnSc4	armáda
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Bach	Bach	k1gMnSc1	Bach
Dang	Dang	k1gMnSc1	Dang
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
musel	muset	k5eAaImAgInS	muset
Vietnam	Vietnam	k1gInSc1	Vietnam
čelit	čelit	k5eAaImF	čelit
třem	tři	k4xCgInPc3	tři
mongolským	mongolský	k2eAgInPc3d1	mongolský
vpádům	vpád	k1gInPc3	vpád
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všechny	všechen	k3xTgInPc4	všechen
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
odrazit	odrazit	k5eAaPmF	odrazit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1406	[number]	k4	1406
<g/>
–	–	k?	–
<g/>
1407	[number]	k4	1407
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
čínská	čínský	k2eAgFnSc1d1	čínská
invaze	invaze	k1gFnSc1	invaze
do	do	k7c2	do
Vietnamu	Vietnam	k1gInSc2	Vietnam
a	a	k8xC	a
z	z	k7c2	z
dobytého	dobytý	k2eAgInSc2d1	dobytý
Vietnamu	Vietnam	k1gInSc2	Vietnam
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
provincie	provincie	k1gFnSc1	provincie
Ťiao-č	Ťiao-č	k1gFnSc1	Ťiao-č
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Nadvládu	nadvláda	k1gFnSc4	nadvláda
čínské	čínský	k2eAgFnSc2d1	čínská
dynastie	dynastie	k1gFnSc2	dynastie
Ming	Ming	k1gInSc1	Ming
ukončil	ukončit	k5eAaPmAgInS	ukončit
roku	rok	k1gInSc2	rok
1428	[number]	k4	1428
vůdce	vůdce	k1gMnSc1	vůdce
povstání	povstání	k1gNnSc2	povstání
Lé	Lé	k1gMnSc1	Lé
Loi	Loi	k1gMnSc1	Loi
<g/>
,	,	kIx,	,
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
vietnamskou	vietnamský	k2eAgFnSc4d1	vietnamská
dynastii	dynastie	k1gFnSc4	dynastie
Lé	Lé	k1gFnSc2	Lé
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
formálně	formálně	k6eAd1	formálně
vládla	vládnout	k5eAaImAgFnS	vládnout
zemi	zem	k1gFnSc4	zem
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1788	[number]	k4	1788
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
čínské	čínský	k2eAgFnSc2d1	čínská
okupace	okupace	k1gFnSc2	okupace
Vietnamu	Vietnam	k1gInSc2	Vietnam
byl	být	k5eAaImAgInS	být
obnovený	obnovený	k2eAgInSc1d1	obnovený
vietnamský	vietnamský	k2eAgInSc1d1	vietnamský
stát	stát	k1gInSc1	stát
mnohem	mnohem	k6eAd1	mnohem
pokročilejší	pokročilý	k2eAgMnSc1d2	pokročilejší
ve	v	k7c6	v
vojenské	vojenský	k2eAgFnSc6d1	vojenská
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
kvality	kvalita	k1gFnSc2	kvalita
střelných	střelný	k2eAgFnPc2d1	střelná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
během	během	k7c2	během
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
staletí	staletí	k1gNnSc4	staletí
trvající	trvající	k2eAgFnSc2d1	trvající
války	válka	k1gFnSc2	válka
s	s	k7c7	s
Čampou	Čampa	k1gFnSc7	Čampa
ve	v	k7c4	v
svůj	svůj	k3xOyFgInSc4	svůj
prospěch	prospěch	k1gInSc4	prospěch
<g/>
.	.	kIx.	.
</s>
<s>
Francouzští	francouzský	k2eAgMnPc1d1	francouzský
misionáři	misionář	k1gMnPc1	misionář
do	do	k7c2	do
Vietnamu	Vietnam	k1gInSc2	Vietnam
přijížděli	přijíždět	k5eAaImAgMnP	přijíždět
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Panovníci	panovník	k1gMnPc1	panovník
dynastie	dynastie	k1gFnSc2	dynastie
Nguyen	Nguyen	k1gInSc1	Nguyen
<g/>
,	,	kIx,	,
vládnoucí	vládnoucí	k2eAgFnSc1d1	vládnoucí
na	na	k7c6	na
území	území	k1gNnSc6	území
jižního	jižní	k2eAgInSc2d1	jižní
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
,	,	kIx,	,
začali	začít	k5eAaPmAgMnP	začít
s	s	k7c7	s
Francouzi	Francouz	k1gMnPc7	Francouz
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1854	[number]	k4	1854
-	-	kIx~	-
84	[number]	k4	84
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
Francie	Francie	k1gFnSc1	Francie
Vietnam	Vietnam	k1gInSc4	Vietnam
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
silou	síla	k1gFnSc7	síla
a	a	k8xC	a
učinila	učinit	k5eAaImAgFnS	učinit
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
svou	svůj	k3xOyFgFnSc4	svůj
kolonii	kolonie	k1gFnSc4	kolonie
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
Indočína	Indočína	k1gFnSc1	Indočína
rozdělenou	rozdělená	k1gFnSc4	rozdělená
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
oblastí	oblast	k1gFnPc2	oblast
<g/>
:	:	kIx,	:
Tonkin	Tonkin	k1gInSc1	Tonkin
<g/>
,	,	kIx,	,
Annam	Annam	k1gInSc1	Annam
<g/>
,	,	kIx,	,
Kočinčína	Kočinčína	k1gFnSc1	Kočinčína
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
sílil	sílit	k5eAaImAgMnS	sílit
protifrancouzský	protifrancouzský	k2eAgInSc4d1	protifrancouzský
odboj	odboj	k1gInSc4	odboj
(	(	kIx(	(
<g/>
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
vedl	vést	k5eAaImAgMnS	vést
Phan	Phan	k1gMnSc1	Phan
Đì	Đì	k1gMnSc1	Đì
Phù	Phù	k1gMnSc1	Phù
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
Vietnam	Vietnam	k1gInSc4	Vietnam
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
po	po	k7c6	po
jehož	jehož	k3xOyRp3gFnSc6	jehož
kapitulaci	kapitulace	k1gFnSc6	kapitulace
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
Ho	on	k3xPp3gNnSc4	on
Či	či	k9wB	či
Min	min	kA	min
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Následná	následný	k2eAgFnSc1d1	následná
osmiletá	osmiletý	k2eAgFnSc1d1	osmiletá
Indočínská	indočínský	k2eAgFnSc1d1	indočínská
válka	válka	k1gFnSc1	válka
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
bitvou	bitva	k1gFnSc7	bitva
u	u	k7c2	u
Dien	dien	k1gInSc1	dien
Bien	biena	k1gFnPc2	biena
Phu	Phu	k1gFnSc2	Phu
a	a	k8xC	a
rozdělením	rozdělení	k1gNnSc7	rozdělení
Vietnamu	Vietnam	k1gInSc2	Vietnam
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Ženevských	ženevský	k2eAgFnPc2d1	Ženevská
dohod	dohoda	k1gFnPc2	dohoda
s	s	k7c7	s
demilitarizovanou	demilitarizovaný	k2eAgFnSc7d1	demilitarizovaná
zónou	zóna	k1gFnSc7	zóna
podél	podél	k7c2	podél
17	[number]	k4	17
<g/>
.	.	kIx.	.
rovnoběžky	rovnoběžka	k1gFnSc2	rovnoběžka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vietnamské	vietnamský	k2eAgFnSc6d1	vietnamská
válce	válka	k1gFnSc6	válka
byly	být	k5eAaImAgFnP	být
obě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
opět	opět	k6eAd1	opět
spojeny	spojen	k2eAgInPc4d1	spojen
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1976	[number]	k4	1976
do	do	k7c2	do
společného	společný	k2eAgInSc2d1	společný
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Vietnam	Vietnam	k1gInSc1	Vietnam
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
Ženevských	ženevský	k2eAgFnPc2d1	Ženevská
dohod	dohoda	k1gFnPc2	dohoda
rozdělen	rozdělen	k2eAgMnSc1d1	rozdělen
na	na	k7c4	na
Ho	on	k3xPp3gNnSc4	on
Či	či	k9wB	či
Minovu	Minův	k2eAgFnSc4d1	Minova
komunistickou	komunistický	k2eAgFnSc4d1	komunistická
severní	severní	k2eAgFnSc4d1	severní
Vietnamskou	vietnamský	k2eAgFnSc4d1	vietnamská
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
republiku	republika	k1gFnSc4	republika
a	a	k8xC	a
profrancouzskou	profrancouzský	k2eAgFnSc4d1	profrancouzská
jižní	jižní	k2eAgFnSc4d1	jižní
Vietnamskou	vietnamský	k2eAgFnSc4d1	vietnamská
republiku	republika	k1gFnSc4	republika
císaře	císař	k1gMnSc2	císař
Bao	Bao	k1gMnSc2	Bao
Daie	Dai	k1gMnSc2	Dai
a	a	k8xC	a
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
problematicky	problematicky	k6eAd1	problematicky
zvoleného	zvolený	k2eAgMnSc2d1	zvolený
prezidenta	prezident	k1gMnSc2	prezident
Ngô	Ngô	k1gMnSc2	Ngô
Ð	Ð	k?	Ð
Diệ	Diệ	k1gMnSc2	Diệ
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
nadcházejícího	nadcházející	k2eAgMnSc2d1	nadcházející
300	[number]	k4	300
denního	denní	k2eAgNnSc2d1	denní
období	období	k1gNnSc2	období
otevřených	otevřený	k2eAgFnPc2d1	otevřená
hranic	hranice	k1gFnPc2	hranice
téměř	téměř	k6eAd1	téměř
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
většinou	většinou	k6eAd1	většinou
katolických	katolický	k2eAgMnPc2d1	katolický
obyvatel	obyvatel	k1gMnPc2	obyvatel
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
do	do	k7c2	do
Jižního	jižní	k2eAgInSc2d1	jižní
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
severu	sever	k1gInSc6	sever
probíhalo	probíhat	k5eAaImAgNnS	probíhat
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
vraždění	vraždění	k1gNnSc1	vraždění
majitelů	majitel	k1gMnPc2	majitel
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
komunistické	komunistický	k2eAgFnSc2d1	komunistická
jednotky	jednotka	k1gFnSc2	jednotka
Vietcongu	Vietcong	k1gInSc2	Vietcong
zahájily	zahájit	k5eAaPmAgFnP	zahájit
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
partyzánské	partyzánský	k2eAgFnSc2d1	Partyzánská
operace	operace	k1gFnSc2	operace
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
svrhnout	svrhnout	k5eAaPmF	svrhnout
režim	režim	k1gInSc4	režim
prezidenta	prezident	k1gMnSc2	prezident
Diệ	Diệ	k1gMnSc2	Diệ
<g/>
.	.	kIx.	.
</s>
<s>
Vojenské	vojenský	k2eAgFnPc1d1	vojenská
operace	operace	k1gFnPc1	operace
partyzánského	partyzánský	k2eAgInSc2d1	partyzánský
komunistického	komunistický	k2eAgInSc2d1	komunistický
Vietcongu	Vietcong	k1gInSc2	Vietcong
a	a	k8xC	a
jednotek	jednotka	k1gFnPc2	jednotka
Vietminhu	Vietminh	k1gInSc2	Vietminh
probíhaly	probíhat	k5eAaImAgInP	probíhat
i	i	k9	i
na	na	k7c6	na
území	území	k1gNnSc6	území
sousedního	sousední	k2eAgInSc2d1	sousední
Laosu	Laos	k1gInSc2	Laos
a	a	k8xC	a
Kambodži	Kambodža	k1gFnSc6	Kambodža
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
Vietnam	Vietnam	k1gInSc1	Vietnam
již	již	k6eAd1	již
spojený	spojený	k2eAgInSc4d1	spojený
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1976	[number]	k4	1976
po	po	k7c6	po
konci	konec	k1gInSc6	konec
Vietnamské	vietnamský	k2eAgFnSc2d1	vietnamská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
zde	zde	k6eAd1	zde
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Válka	válka	k1gFnSc1	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
mezi	mezi	k7c7	mezi
Jižním	jižní	k2eAgInSc7d1	jižní
a	a	k8xC	a
Severním	severní	k2eAgInSc7d1	severní
Vietnamem	Vietnam	k1gInSc7	Vietnam
konflikt	konflikt	k1gInSc1	konflikt
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Jižního	jižní	k2eAgInSc2d1	jižní
Vietnamu	Vietnam	k1gInSc2	Vietnam
silně	silně	k6eAd1	silně
angažovaly	angažovat	k5eAaBmAgFnP	angažovat
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Válkou	válka	k1gFnSc7	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
prošlo	projít	k5eAaPmAgNnS	projít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2,6	[number]	k4	2,6
milionů	milion	k4xCgInPc2	milion
Američanů	Američan	k1gMnPc2	Američan
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
byla	být	k5eAaImAgFnS	být
vedena	vést	k5eAaImNgFnS	vést
se	s	k7c7	s
značnou	značný	k2eAgFnSc7d1	značná
krutostí	krutost	k1gFnSc7	krutost
<g/>
;	;	kIx,	;
během	během	k7c2	během
bojů	boj	k1gInPc2	boj
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
přes	přes	k7c4	přes
2	[number]	k4	2
miliony	milion	k4xCgInPc1	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
civilistů	civilista	k1gMnPc2	civilista
<g/>
,	,	kIx,	,
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
ničeno	ničen	k2eAgNnSc4d1	ničeno
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgInSc1d1	severní
Vietnam	Vietnam	k1gInSc1	Vietnam
byl	být	k5eAaImAgInS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
bombardován	bombardován	k2eAgInSc1d1	bombardován
americkým	americký	k2eAgNnSc7d1	americké
letectvem	letectvo	k1gNnSc7	letectvo
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
operace	operace	k1gFnSc1	operace
Rolling	Rolling	k1gInSc1	Rolling
Thunder	Thunder	k1gInSc1	Thunder
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
v	v	k7c6	v
boji	boj	k1gInSc6	boj
s	s	k7c7	s
partyzány	partyzán	k1gMnPc7	partyzán
z	z	k7c2	z
Vietcongu	Vietcong	k1gInSc2	Vietcong
použily	použít	k5eAaPmAgFnP	použít
proti	proti	k7c3	proti
rostlinným	rostlinný	k2eAgInPc3d1	rostlinný
porostům	porost	k1gInPc3	porost
jedovatou	jedovatý	k2eAgFnSc4d1	jedovatá
chemikálii	chemikálie	k1gFnSc4	chemikálie
Agent	agent	k1gMnSc1	agent
Orange	Orang	k1gInSc2	Orang
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
kontaminovala	kontaminovat	k5eAaBmAgFnS	kontaminovat
půdu	půda	k1gFnSc4	půda
a	a	k8xC	a
zmrzačila	zmrzačit	k5eAaPmAgFnS	zmrzačit
miliony	milion	k4xCgInPc4	milion
Vietnamců	Vietnamec	k1gMnPc2	Vietnamec
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
se	se	k3xPyFc4	se
stáhly	stáhnout	k5eAaPmAgInP	stáhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
válka	válka	k1gFnSc1	válka
skončila	skončit	k5eAaPmAgFnS	skončit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
padl	padnout	k5eAaImAgInS	padnout
Saigon	Saigon	k1gInSc1	Saigon
a	a	k8xC	a
Severní	severní	k2eAgInSc1d1	severní
Vietnam	Vietnam	k1gInSc1	Vietnam
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
zbytek	zbytek	k1gInSc4	zbytek
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vietnamské	vietnamský	k2eAgFnSc2d1	vietnamská
invaze	invaze	k1gFnSc2	invaze
do	do	k7c2	do
Kambodže	Kambodža	k1gFnSc2	Kambodža
proti	proti	k7c3	proti
režimu	režim	k1gInSc3	režim
Rudých	rudý	k2eAgMnPc2d1	rudý
Khmerů	Khmer	k1gMnPc2	Khmer
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zhoršení	zhoršení	k1gNnSc3	zhoršení
čínsko-vietnamských	čínskoietnamský	k2eAgInPc2d1	čínsko-vietnamský
vztahů	vztah	k1gInPc2	vztah
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
čínsko-vietnamská	čínskoietnamský	k2eAgFnSc1d1	čínsko-vietnamský
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
vládne	vládnout	k5eAaImIp3nS	vládnout
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
omezuje	omezovat	k5eAaImIp3nS	omezovat
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
povoluje	povolovat	k5eAaImIp3nS	povolovat
soukromé	soukromý	k2eAgNnSc4d1	soukromé
podnikání	podnikání	k1gNnSc4	podnikání
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
úzké	úzký	k2eAgFnPc1d1	úzká
nížiny	nížina	k1gFnPc1	nížina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vypínají	vypínat	k5eAaImIp3nP	vypínat
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
výšky	výška	k1gFnPc4	výška
až	až	k9	až
přes	přes	k7c4	přes
3000	[number]	k4	3000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
(	(	kIx(	(
<g/>
Fan	Fana	k1gFnPc2	Fana
Si	se	k3xPyFc3	se
Pan	Pan	k1gMnSc1	Pan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Vietnam	Vietnam	k1gInSc4	Vietnam
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
Rudá	rudý	k2eAgFnSc1d1	rudá
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
řeka	řeka	k1gFnSc1	řeka
-	-	kIx~	-
protéká	protékat	k5eAaImIp3nS	protékat
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
450	[number]	k4	450
km	km	kA	km
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
nekonečná	konečný	k2eNgNnPc4d1	nekonečné
rýžová	rýžový	k2eAgNnPc4d1	rýžové
pole	pole	k1gNnPc4	pole
a	a	k8xC	a
stále	stále	k6eAd1	stále
zelené	zelený	k2eAgInPc1d1	zelený
tropické	tropický	k2eAgInPc1d1	tropický
lesy	les	k1gInPc1	les
<g/>
.	.	kIx.	.
</s>
<s>
Zemi	zem	k1gFnSc4	zem
často	často	k6eAd1	často
ohrožují	ohrožovat	k5eAaImIp3nP	ohrožovat
tajfuny	tajfun	k1gInPc1	tajfun
<g/>
.	.	kIx.	.
</s>
<s>
Vietnam	Vietnam	k1gInSc1	Vietnam
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
63	[number]	k4	63
administrativních	administrativní	k2eAgFnPc2d1	administrativní
oblastí	oblast	k1gFnPc2	oblast
<g/>
;	;	kIx,	;
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
58	[number]	k4	58
provincií	provincie	k1gFnPc2	provincie
(	(	kIx(	(
<g/>
vietnamsky	vietnamsky	k6eAd1	vietnamsky
tỉ	tỉ	k?	tỉ
<g/>
)	)	kIx)	)
a	a	k8xC	a
5	[number]	k4	5
měst	město	k1gNnPc2	město
"	"	kIx"	"
<g/>
pod	pod	k7c7	pod
ústřední	ústřední	k2eAgFnSc7d1	ústřední
správou	správa	k1gFnSc7	správa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
viet	viet	k1gInSc1	viet
<g/>
.	.	kIx.	.
thà	thà	k?	thà
phố	phố	k?	phố
trự	trự	k?	trự
thuộ	thuộ	k?	thuộ
trung	trung	k1gInSc1	trung
ư	ư	k?	ư
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
provincie	provincie	k1gFnPc1	provincie
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
okresy	okres	k1gInPc4	okres
(	(	kIx(	(
<g/>
viet	viet	k2eAgInSc4d1	viet
<g/>
.	.	kIx.	.
huyệ	huyệ	k?	huyệ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
střediskové	střediskový	k2eAgFnPc1d1	středisková
obce	obec	k1gFnPc1	obec
(	(	kIx(	(
<g/>
viet	viet	k1gInSc1	viet
<g/>
.	.	kIx.	.
thị	thị	k?	thị
xã	xã	k?	xã
<g/>
)	)	kIx)	)
a	a	k8xC	a
města	město	k1gNnSc2	město
"	"	kIx"	"
<g/>
pod	pod	k7c7	pod
provinční	provinční	k2eAgFnSc7d1	provinční
správou	správa	k1gFnSc7	správa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
viet	viet	k1gInSc1	viet
<g/>
.	.	kIx.	.
thà	thà	k?	thà
phố	phố	k?	phố
trự	trự	k?	trự
thuộ	thuộ	k?	thuộ
tỉ	tỉ	k?	tỉ
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
na	na	k7c6	na
téže	tenže	k3xDgFnSc6	tenže
úrovni	úroveň	k1gFnSc6	úroveň
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
také	také	k9	také
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zmíněných	zmíněný	k2eAgNnPc6d1	zmíněné
5	[number]	k4	5
městech	město	k1gNnPc6	město
<g/>
)	)	kIx)	)
městské	městský	k2eAgInPc1d1	městský
obvody	obvod	k1gInPc1	obvod
(	(	kIx(	(
<g/>
viet	viet	k1gInSc1	viet
<g/>
.	.	kIx.	.
quậ	quậ	k?	quậ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
národním	národní	k2eAgNnSc6d1	národní
hospodářství	hospodářství	k1gNnSc6	hospodářství
hraje	hrát	k5eAaImIp3nS	hrát
zemědělství	zemědělství	k1gNnSc1	zemědělství
-	-	kIx~	-
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
48	[number]	k4	48
%	%	kIx~	%
ekonomicky	ekonomicky	k6eAd1	ekonomicky
aktivního	aktivní	k2eAgNnSc2d1	aktivní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgInPc4d1	hlavní
produkty	produkt	k1gInPc4	produkt
patří	patřit	k5eAaImIp3nS	patřit
rýže	rýže	k1gFnSc1	rýže
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
v	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
vývozu	vývoz	k1gInSc6	vývoz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
káva	káva	k1gFnSc1	káva
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
v	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pepř	pepř	k1gInSc1	pepř
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
v	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ořechy	ořech	k1gInPc1	ořech
kešu	kešus	k1gInSc2	kešus
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
v	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čaj	čaj	k1gInSc1	čaj
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
v	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krevety	kreveta	k1gFnSc2	kreveta
<g/>
.	.	kIx.	.
</s>
<s>
Zvolna	zvolna	k6eAd1	zvolna
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
cestovní	cestovní	k2eAgInSc1d1	cestovní
ruch	ruch	k1gInSc1	ruch
-	-	kIx~	-
země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
bohatá	bohatý	k2eAgFnSc1d1	bohatá
jak	jak	k8xS	jak
na	na	k7c4	na
kulturní	kulturní	k2eAgNnSc4d1	kulturní
<g/>
,	,	kIx,	,
tak	tak	k9	tak
na	na	k7c4	na
přírodní	přírodní	k2eAgNnPc4d1	přírodní
bohatství	bohatství	k1gNnPc4	bohatství
<g/>
,	,	kIx,	,
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
si	se	k3xPyFc3	se
přijdou	přijít	k5eAaPmIp3nP	přijít
i	i	k9	i
milovníci	milovník	k1gMnPc1	milovník
sportu	sport	k1gInSc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
Vietnam	Vietnam	k1gInSc1	Vietnam
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
prioritní	prioritní	k2eAgFnPc4d1	prioritní
země	zem	k1gFnPc4	zem
české	český	k2eAgFnSc2d1	Česká
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
rozvojové	rozvojový	k2eAgFnSc2d1	rozvojová
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
projekty	projekt	k1gInPc1	projekt
se	se	k3xPyFc4	se
soustředí	soustředit	k5eAaPmIp3nP	soustředit
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
hlavní	hlavní	k2eAgInPc4d1	hlavní
sektory	sektor	k1gInPc4	sektor
<g/>
:	:	kIx,	:
ochrana	ochrana	k1gFnSc1	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnSc1d1	sociální
ochrana	ochrana	k1gFnSc1	ochrana
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
rozvoj	rozvoj	k1gInSc4	rozvoj
venkova	venkov	k1gInSc2	venkov
a	a	k8xC	a
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
boj	boj	k1gInSc1	boj
proti	proti	k7c3	proti
kriminalitě	kriminalita	k1gFnSc3	kriminalita
(	(	kIx(	(
<g/>
zejména	zejména	k6eAd1	zejména
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ekonomicko	ekonomicko	k6eAd1	ekonomicko
průmyslový	průmyslový	k2eAgInSc4d1	průmyslový
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
náboženstvím	náboženství	k1gNnSc7	náboženství
je	být	k5eAaImIp3nS	být
buddhismus	buddhismus	k1gInSc1	buddhismus
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
velká	velký	k2eAgNnPc4d1	velké
města	město	k1gNnPc4	město
patří	patřit	k5eAaImIp3nS	patřit
Hanoj	Hanoj	k1gFnSc1	Hanoj
(	(	kIx(	(
<g/>
Hà	Hà	k1gMnSc1	Hà
Nộ	Nộ	k1gMnSc1	Nộ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ho	on	k3xPp3gInSc4	on
Či	či	k9wB	či
Minovo	Minův	k2eAgNnSc1d1	Minovo
Město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
Thà	Thà	k1gFnSc1	Thà
phố	phố	k?	phố
Hồ	Hồ	k1gFnSc2	Hồ
Chí	chí	k1gNnSc2	chí
Minh	Minha	k1gFnPc2	Minha
<g/>
,	,	kIx,	,
též	též	k9	též
Sà	Sà	k1gMnSc1	Sà
Gò	Gò	k1gMnSc1	Gò
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Haiphong	Haiphong	k1gMnSc1	Haiphong
(	(	kIx(	(
<g/>
Hả	Hả	k1gMnSc1	Hả
Phò	Phò	k1gMnSc1	Phò
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Huế	Huế	k1gMnSc1	Huế
<g/>
,	,	kIx,	,
Danang	Danang	k1gMnSc1	Danang
(	(	kIx(	(
<g/>
Đà	Đà	k1gMnSc1	Đà
Nã	Nã	k1gMnSc1	Nã
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Vietové	Vietová	k1gFnPc1	Vietová
tvoří	tvořit	k5eAaImIp3nP	tvořit
přibližně	přibližně	k6eAd1	přibližně
85	[number]	k4	85
%	%	kIx~	%
Vietnamců	Vietnamec	k1gMnPc2	Vietnamec
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
obyvatel	obyvatel	k1gMnSc1	obyvatel
celého	celý	k2eAgInSc2d1	celý
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
žije	žít	k5eAaImIp3nS	žít
celkem	celkem	k6eAd1	celkem
přes	přes	k7c4	přes
50	[number]	k4	50
menšinových	menšinový	k2eAgFnPc2d1	menšinová
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Khmerové	Khmer	k1gMnPc1	Khmer
<g/>
,	,	kIx,	,
Hmongové	Hmong	k1gMnPc1	Hmong
<g/>
,	,	kIx,	,
etničtí	etnický	k2eAgMnPc1d1	etnický
Číňané	Číňan	k1gMnPc1	Číňan
nebo	nebo	k8xC	nebo
Jaové	Jaová	k1gFnPc1	Jaová
<g/>
.	.	kIx.	.
</s>
<s>
Horské	Horské	k2eAgInPc1d1	Horské
kmeny	kmen	k1gInPc1	kmen
obývající	obývající	k2eAgInPc1d1	obývající
převážně	převážně	k6eAd1	převážně
hraniční	hraniční	k2eAgFnSc6d1	hraniční
oblasti	oblast	k1gFnSc6	oblast
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
chudobě	chudoba	k1gFnSc6	chudoba
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
čelí	čelit	k5eAaImIp3nS	čelit
diskriminaci	diskriminace	k1gFnSc4	diskriminace
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
většinové	většinový	k2eAgFnSc2d1	většinová
vietnamské	vietnamský	k2eAgFnSc2d1	vietnamská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
vietnamsko-čínského	vietnamsko-čínský	k2eAgInSc2d1	vietnamsko-čínský
konfliktu	konflikt	k1gInSc2	konflikt
Vietnam	Vietnam	k1gInSc1	Vietnam
opustilo	opustit	k5eAaPmAgNnS	opustit
téměř	téměř	k6eAd1	téměř
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
etnických	etnický	k2eAgMnPc2d1	etnický
Číňanů	Číňan	k1gMnPc2	Číňan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
žilo	žít	k5eAaImAgNnS	žít
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
necelých	celý	k2eNgInPc2d1	necelý
53	[number]	k4	53
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
téměř	téměř	k6eAd1	téměř
zdvojnásobil	zdvojnásobit	k5eAaPmAgInS	zdvojnásobit
na	na	k7c4	na
90,5	[number]	k4	90,5
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
