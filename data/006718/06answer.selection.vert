<s>
Vietnam	Vietnam	k1gInSc1	Vietnam
(	(	kIx(	(
<g/>
vietnamsky	vietnamsky	k6eAd1	vietnamsky
Việ	Việ	k1gMnSc1	Việ
Nam	Nam	k1gMnSc1	Nam
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celým	celý	k2eAgInSc7d1	celý
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
Vietnamská	vietnamský	k2eAgFnSc1d1	vietnamská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
vietnamsky	vietnamsky	k6eAd1	vietnamsky
Cộ	Cộ	k1gMnSc2	Cộ
hò	hò	k?	hò
Xã	Xã	k1gMnSc2	Xã
hộ	hộ	k?	hộ
chủ	chủ	k?	chủ
nghĩ	nghĩ	k?	nghĩ
Việ	Việ	k1gMnSc1	Việ
Nam	Nam	k1gMnSc1	Nam
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
zkratka	zkratka	k1gFnSc1	zkratka
VSR	VSR	kA	VSR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
poloostrova	poloostrov	k1gInSc2	poloostrov
Zadní	zadní	k2eAgFnSc2d1	zadní
Indie	Indie	k1gFnSc2	Indie
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Jihočínského	jihočínský	k2eAgNnSc2d1	Jihočínské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
