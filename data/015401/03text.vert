<s>
Modrá	modrý	k2eAgFnSc1d1
stuha	stuha	k1gFnSc1
</s>
<s>
Možná	možná	k9
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
fancouzské	fancouzský	k2eAgNnSc1d1
kuchařské	kuchařský	k2eAgNnSc1d1
vyznamenání	vyznamenání	k1gNnSc1
Cordon	Cordona	k1gFnPc2
bleu	bleus	k1gInSc2
a	a	k8xC
stejnojmenný	stejnojmenný	k2eAgInSc4d1
pokrm	pokrm	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
prvního	první	k4xOgMnSc4
držitele	držitel	k1gMnSc4
Modré	modrý	k2eAgFnSc2d1
stuhy	stuha	k1gFnSc2
je	být	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
kolesový	kolesový	k2eAgInSc1d1
parník	parník	k1gInSc1
SS	SS	kA
Sirius	Sirius	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1
stuha	stuha	k1gFnSc1
při	při	k7c6
plavbě	plavba	k1gFnSc6
přes	přes	k7c4
Atlantský	atlantský	k2eAgInSc4d1
oceán	oceán	k1gInSc4
bylo	být	k5eAaImAgNnS
zvláštní	zvláštní	k2eAgNnSc1d1
prestižní	prestižní	k2eAgNnSc1d1
putovní	putovní	k2eAgNnSc1d1
ocenění	ocenění	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
bylo	být	k5eAaImAgNnS
udělováno	udělován	k2eAgNnSc1d1
jednotlivým	jednotlivý	k2eAgFnPc3d1
lodím	loď	k1gFnPc3
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
nejrychleji	rychle	k6eAd3
přepluly	přeplout	k5eAaPmAgFnP
severní	severní	k2eAgInSc4d1
Atlantský	atlantský	k2eAgInSc4d1
oceán	oceán	k1gInSc4
na	na	k7c6
trase	trasa	k1gFnSc6
z	z	k7c2
New	New	k1gFnSc2
Yorku	York	k1gInSc2
do	do	k7c2
Evropy	Evropa	k1gFnSc2
(	(	kIx(
<g/>
obvykle	obvykle	k6eAd1
do	do	k7c2
Liverpoolu	Liverpool	k1gInSc2
nebo	nebo	k8xC
i	i	k9
jiného	jiný	k2eAgInSc2d1
evropského	evropský	k2eAgInSc2d1
přístavu	přístav	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
zavedena	zavést	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1838	#num#	k4
<g/>
,	,	kIx,
tedy	tedy	k9
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
existuje	existovat	k5eAaImIp3nS
vlastně	vlastně	k9
až	až	k9
do	do	k7c2
dnešních	dnešní	k2eAgFnPc2d1
dob	doba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
držitelem	držitel	k1gMnSc7
této	tento	k3xDgFnSc2
ceny	cena	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1838	#num#	k4
stal	stát	k5eAaPmAgInS
kolesový	kolesový	k2eAgInSc1d1
parník	parník	k1gInSc1
SS	SS	kA
Sirius	Sirius	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
vykonal	vykonat	k5eAaPmAgInS
cestu	cesta	k1gFnSc4
z	z	k7c2
Corku	Cork	k1gInSc2
do	do	k7c2
New	New	k1gFnSc2
Yorku	York	k1gInSc2
rychlostí	rychlost	k1gFnSc7
8,03	8,03	k4
uzlu	uzel	k1gInSc2
(	(	kIx(
<g/>
14,87	14,87	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
za	za	k7c4
18	#num#	k4
dní	den	k1gInPc2
<g/>
,	,	kIx,
14	#num#	k4
hodin	hodina	k1gFnPc2
a	a	k8xC
22	#num#	k4
minut	minuta	k1gFnPc2
a	a	k8xC
cestu	cesta	k1gFnSc4
z	z	k7c2
New	New	k1gFnSc2
Yorku	York	k1gInSc2
do	do	k7c2
Falmouthu	Falmouth	k1gInSc2
rychlostí	rychlost	k1gFnSc7
7,31	7,31	k4
uzlu	uzel	k1gInSc2
za	za	k7c4
18	#num#	k4
dní	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
držení	držení	k1gNnSc6
této	tento	k3xDgFnSc2
ceny	cena	k1gFnSc2
vystřídala	vystřídat	k5eAaPmAgFnS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
dalších	další	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
z	z	k7c2
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
USA	USA	kA
a	a	k8xC
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
i	i	k9
z	z	k7c2
Německa	Německo	k1gNnSc2
či	či	k8xC
Itálie	Itálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
slavné	slavný	k2eAgMnPc4d1
držitele	držitel	k1gMnPc4
modré	modrý	k2eAgFnSc2d1
stuhy	stuha	k1gFnSc2
patří	patřit	k5eAaImIp3nS
mimo	mimo	k7c4
jiné	jiný	k2eAgInPc4d1
i	i	k8xC
známé	známý	k2eAgInPc4d1
parníky	parník	k1gInPc4
Lusitania	Lusitanium	k1gNnSc2
a	a	k8xC
Mauretania	Mauretanium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Jednou	jeden	k4xCgFnSc7
z	z	k7c2
posledních	poslední	k2eAgFnPc2d1
držitelek	držitelka	k1gFnPc2
modré	modrý	k2eAgFnSc2d1
stuhy	stuha	k1gFnSc2
byla	být	k5eAaImAgFnS
americká	americký	k2eAgFnSc1d1
loď	loď	k1gFnSc1
United	United	k1gMnSc1
States	States	k1gMnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
tuto	tento	k3xDgFnSc4
vzdálenost	vzdálenost	k1gFnSc4
překonala	překonat	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1952	#num#	k4
za	za	k7c4
3	#num#	k4
dny	den	k1gInPc4
plus	plus	k1gInSc1
10	#num#	k4
hodin	hodina	k1gFnPc2
a	a	k8xC
40	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Ocenění	ocenění	k1gNnSc1
bylo	být	k5eAaImAgNnS
zejména	zejména	k9
v	v	k7c6
dobách	doba	k1gFnPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
neexistovala	existovat	k5eNaImAgFnS
dálková	dálkový	k2eAgFnSc1d1
trasoceánská	trasoceánský	k2eAgFnSc1d1
letecká	letecký	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
velmi	velmi	k6eAd1
prestižní	prestižní	k2eAgFnSc1d1
i	i	k9
z	z	k7c2
komerčních	komerční	k2eAgInPc2d1
respektive	respektive	k9
ekonomických	ekonomický	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotliví	jednotlivý	k2eAgMnPc1d1
dopravci	dopravce	k1gMnPc1
se	se	k3xPyFc4
na	na	k7c4
svoje	svůj	k3xOyFgFnPc4
luxusní	luxusní	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
snažili	snažit	k5eAaImAgMnP
nalákat	nalákat	k5eAaPmF
bohatou	bohatý	k2eAgFnSc4d1
pasažérskou	pasažérský	k2eAgFnSc4d1
klientelu	klientela	k1gFnSc4
i	i	k8xC
nákladní	nákladní	k2eAgMnPc4d1
přepravce	přepravce	k1gMnPc4
(	(	kIx(
<g/>
například	například	k6eAd1
přepravce	přepravce	k1gMnSc1
poštovních	poštovní	k2eAgFnPc2d1
zásilek	zásilka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
závody	závod	k1gInPc4
o	o	k7c4
nejrychlejší	rychlý	k2eAgFnSc4d3
loď	loď	k1gFnSc4
vedly	vést	k5eAaImAgFnP
také	také	k9
k	k	k7c3
několika	několik	k4yIc3
námořním	námořní	k2eAgFnPc3d1
katastrofám	katastrofa	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
k	k	k7c3
potopení	potopení	k1gNnSc3
Titaniku	Titanic	k1gInSc2
15	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1912	#num#	k4
<g/>
:	:	kIx,
J.	J.	kA
Bruce	Bruce	k1gFnSc1
Ismay	Ismaa	k1gFnSc2
prý	prý	k9
nabádal	nabádat	k5eAaBmAgMnS,k5eAaImAgMnS
kapitána	kapitán	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zvyšoval	zvyšovat	k5eAaImAgInS
rychlost	rychlost	k1gFnSc4
a	a	k8xC
neměnil	měnit	k5eNaImAgMnS
směr	směr	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byly	být	k5eAaImAgInP
zřejmě	zřejmě	k6eAd1
důvody	důvod	k1gInPc1
<g/>
,	,	kIx,
proč	proč	k6eAd1
se	se	k3xPyFc4
Titanic	Titanic	k1gInSc1
nedokázal	dokázat	k5eNaPmAgInS
vyhnout	vyhnout	k5eAaPmF
ledovci	ledovec	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
Název	název	k1gInSc1
Modrá	modrý	k2eAgFnSc1d1
stuha	stuha	k1gFnSc1
dal	dát	k5eAaPmAgMnS
jméno	jméno	k1gNnSc4
i	i	k8xC
různým	různý	k2eAgFnPc3d1
českým	český	k2eAgFnPc3d1
vodáckým	vodácký	k2eAgFnPc3d1
a	a	k8xC
jachtařským	jachtařský	k2eAgFnPc3d1
akcím	akce	k1gFnPc3
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1
stuha	stuha	k1gFnSc1
Vltavy	Vltava	k1gFnSc2
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1
stuha	stuha	k1gFnSc1
Ohře	Ohře	k1gFnSc2
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1
stuha	stuha	k1gFnSc1
Svratky	Svratka	k1gFnSc2
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1
stuha	stuha	k1gFnSc1
Lipna	Lipno	k1gNnSc2
</s>
<s>
Držitelé	držitel	k1gMnPc1
modré	modrý	k2eAgFnSc2d1
stuhy	stuha	k1gFnSc2
</s>
<s>
Směrem	směr	k1gInSc7
na	na	k7c4
západ	západ	k1gInSc4
</s>
<s>
Jméno	jméno	k1gNnSc1
lodi	loď	k1gFnSc2
</s>
<s>
Datum	datum	k1gNnSc1
</s>
<s>
Rejdařství	rejdařství	k1gNnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
Do	do	k7c2
</s>
<s>
Námořních	námořní	k2eAgFnPc2d1
mil	míle	k1gFnPc2
</s>
<s>
Dny	den	k1gInPc1
<g/>
/	/	kIx~
<g/>
Hodiny	hodina	k1gFnPc1
<g/>
/	/	kIx~
<g/>
Minuty	minuta	k1gFnPc1
</s>
<s>
Rychlost	rychlost	k1gFnSc1
v	v	k7c6
uzlech	uzel	k1gInPc6
</s>
<s>
SS	SS	kA
Sirius	Sirius	k1gMnSc1
(	(	kIx(
<g/>
1837	#num#	k4
<g/>
)	)	kIx)
<g/>
1838	#num#	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
B	B	kA
<g/>
&	&	k?
<g/>
A	a	k9
</s>
<s>
CorkSandy	CorkSand	k1gInPc1
Hook	Hook	k1gInSc1
<g/>
358318	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
/	/	kIx~
<g/>
228.03	228.03	k4
</s>
<s>
Great	Great	k2eAgInSc1d1
Western	western	k1gInSc1
</s>
<s>
1838	#num#	k4
(	(	kIx(
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
GWAvonmouthNew	GWAvonmouthNew	k1gFnSc1
York	York	k1gInSc1
<g/>
3220	#num#	k4
</s>
<s>
15	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8.66	8.66	k4
</s>
<s>
Great	Great	k2eAgInSc1d1
Western	western	k1gInSc1
</s>
<s>
1838	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
GWAvonmouthNew	GWAvonmouthNew	k1gFnSc1
York	York	k1gInSc1
<g/>
3140	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8.92	8.92	k4
</s>
<s>
Great	Great	k2eAgInSc1d1
Western	western	k1gInSc1
</s>
<s>
1839	#num#	k4
(	(	kIx(
<g/>
18	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
GWAvonmouthNew	GWAvonmouthNew	k1gFnSc1
York	York	k1gInSc1
<g/>
3086	#num#	k4
</s>
<s>
13	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9.52	9.52	k4
</s>
<s>
Columbia	Columbia	k1gFnSc1
<g/>
1841	#num#	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
Cunard	Cunard	k1gInSc1
</s>
<s>
LiverpoolHalifax	LiverpoolHalifax	k1gInSc1
<g/>
(	(	kIx(
<g/>
2534	#num#	k4
<g/>
)	)	kIx)
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
(	(	kIx(
<g/>
9.78	9.78	k4
<g/>
)	)	kIx)
</s>
<s>
Great	Great	k2eAgInSc1d1
Western	western	k1gInSc1
</s>
<s>
1843	#num#	k4
(	(	kIx(
<g/>
29	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
GWLiverpoolNew	GWLiverpoolNew	k1gFnSc1
York	York	k1gInSc1
<g/>
3068	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
10.03	10.03	k4
</s>
<s>
Cambria	Cambrium	k1gNnPc4
<g/>
1845	#num#	k4
(	(	kIx(
<g/>
19	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
Cunard	Cunard	k1gInSc1
</s>
<s>
LiverpoolHalifax	LiverpoolHalifax	k1gInSc1
<g/>
(	(	kIx(
<g/>
2534	#num#	k4
<g/>
)	)	kIx)
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
<g/>
(	(	kIx(
<g/>
10.71	10.71	k4
<g/>
)	)	kIx)
</s>
<s>
America	America	k6eAd1
<g/>
1848	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
Cunard	Cunard	k1gInSc1
</s>
<s>
LiverpoolHalifax	LiverpoolHalifax	k1gInSc1
<g/>
(	(	kIx(
<g/>
2534	#num#	k4
<g/>
)	)	kIx)
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
(	(	kIx(
<g/>
11.71	11.71	k4
<g/>
)	)	kIx)
</s>
<s>
Europa	Europa	k1gFnSc1
<g/>
1848	#num#	k4
(	(	kIx(
<g/>
14	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
<g/>
Cunard	Cunard	k1gInSc1
</s>
<s>
LiverpoolHalifax	LiverpoolHalifax	k1gInSc1
<g/>
(	(	kIx(
<g/>
2534	#num#	k4
<g/>
)	)	kIx)
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
23	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
(	(	kIx(
<g/>
11.79	11.79	k4
<g/>
)	)	kIx)
</s>
<s>
Asia	Asia	k6eAd1
<g/>
1850	#num#	k4
(	(	kIx(
<g/>
18	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
Cunard	Cunard	k1gInSc1
</s>
<s>
LiverpoolHalifax	LiverpoolHalifax	k1gInSc1
<g/>
(	(	kIx(
<g/>
2534	#num#	k4
<g/>
)	)	kIx)
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
<g/>
(	(	kIx(
<g/>
12.25	12.25	k4
<g/>
)	)	kIx)
</s>
<s>
Pacific	Pacific	k1gMnSc1
<g/>
1850	#num#	k4
(	(	kIx(
<g/>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
)	)	kIx)
<g/>
Collins	Collins	k1gInSc1
</s>
<s>
LiverpoolNew	LiverpoolNew	k?
York	York	k1gInSc1
<g/>
(	(	kIx(
<g/>
3050	#num#	k4
<g/>
)	)	kIx)
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
45	#num#	k4
<g/>
(	(	kIx(
<g/>
12.46	12.46	k4
<g/>
)	)	kIx)
</s>
<s>
Baltic	Baltice	k1gFnPc2
<g/>
1851	#num#	k4
(	(	kIx(
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
Collins	Collins	k1gInSc1
</s>
<s>
LiverpoolNew	LiverpoolNew	k?
York	York	k1gInSc1
<g/>
30399	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
/	/	kIx~
<g/>
2612.91	2612.91	k4
</s>
<s>
Baltic	Baltice	k1gFnPc2
<g/>
1854	#num#	k4
(	(	kIx(
<g/>
28	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
Collins	Collins	k1gInSc1
</s>
<s>
LiverpoolNew	LiverpoolNew	k?
York	York	k1gInSc1
<g/>
30379	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
/	/	kIx~
<g/>
5213.04	5213.04	k4
</s>
<s>
Persia	Persia	k1gFnSc1
<g/>
1856	#num#	k4
(	(	kIx(
<g/>
19	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
Cunard	Cunard	k1gInSc1
</s>
<s>
LiverpoolSandy	LiverpoolSand	k1gInPc1
Hook	Hooka	k1gFnPc2
<g/>
(	(	kIx(
<g/>
3045	#num#	k4
<g/>
)	)	kIx)
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
(	(	kIx(
<g/>
13.11	13.11	k4
<g/>
)	)	kIx)
</s>
<s>
Scotia	Scotia	k1gFnSc1
<g/>
1863	#num#	k4
(	(	kIx(
<g/>
19	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
Cunard	Cunard	k1gInSc1
</s>
<s>
QueenstownNew	QueenstownNew	k?
York	York	k1gInSc1
<g/>
(	(	kIx(
<g/>
2820	#num#	k4
<g/>
)	)	kIx)
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
(	(	kIx(
<g/>
14.46	14.46	k4
<g/>
)	)	kIx)
</s>
<s>
Adriatic	Adriatice	k1gFnPc2
<g/>
1872	#num#	k4
(	(	kIx(
<g/>
17	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
W.	W.	kA
<g/>
StarQueenstownSandy	StarQueenstownSanda	k1gFnPc1
Hook	Hook	k1gMnSc1
</s>
<s>
27787	#num#	k4
<g/>
/	/	kIx~
<g/>
23	#num#	k4
<g/>
/	/	kIx~
<g/>
1714.53	1714.53	k4
</s>
<s>
Germanic	Germanice	k1gFnPc2
<g/>
1875	#num#	k4
(	(	kIx(
<g/>
30	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
W.	W.	kA
<g/>
Star	Star	kA
</s>
<s>
QueenstownSandy	QueenstownSand	k1gInPc1
Hook	Hook	k1gInSc1
<g/>
28007	#num#	k4
<g/>
/	/	kIx~
<g/>
23	#num#	k4
<g/>
/	/	kIx~
<g/>
714.65	714.65	k4
</s>
<s>
City	city	k1gNnSc1
Of	Of	k1gFnSc2
Berlin	berlina	k1gFnPc2
<g/>
1875	#num#	k4
(	(	kIx(
<g/>
17	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
InmanQueenstownSandy	InmanQueenstownSand	k1gInPc1
Bank	bank	k1gInSc1
<g/>
2829	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
/	/	kIx~
<g/>
215.21	215.21	k4
</s>
<s>
Britannic	Britannice	k1gFnPc2
<g/>
1876	#num#	k4
(	(	kIx(
<g/>
27	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
W.	W.	kA
<g/>
StarQueenstownSandy	StarQueenstownSanda	k1gFnPc1
Hook	Hook	k1gMnSc1
</s>
<s>
27957	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
/	/	kIx~
<g/>
1115.43	1115.43	k4
</s>
<s>
Germanic	Germanice	k1gFnPc2
<g/>
1877	#num#	k4
(	(	kIx(
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
W.	W.	kA
<g/>
Star	Star	kA
</s>
<s>
QueenstownSandy	QueenstownSand	k1gInPc1
Hook	Hook	k1gInSc1
<g/>
28307	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
3715.76	3715.76	k4
</s>
<s>
Alaska	Alaska	k1gFnSc1
<g/>
1882	#num#	k4
(	(	kIx(
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
Guion	Guion	k1gInSc1
</s>
<s>
QueenstownSandy	QueenstownSand	k1gInPc1
Hook	Hook	k1gInSc1
<g/>
28027	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2016.07	2016.07	k4
</s>
<s>
Alaska	Alaska	k1gFnSc1
<g/>
1882	#num#	k4
(	(	kIx(
<g/>
14	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
Guion	Guion	k1gInSc1
</s>
<s>
QueenstownSandy	QueenstownSand	k1gInPc1
Hook	Hook	k1gInSc1
<g/>
28717	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
1216.67	1216.67	k4
</s>
<s>
Alaska	Alaska	k1gFnSc1
<g/>
1882	#num#	k4
(	(	kIx(
<g/>
18	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
Guion	Guion	k1gInSc1
</s>
<s>
QueenstownSandy	QueenstownSand	k1gInPc1
Hook	Hook	k1gInSc1
<g/>
28867	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
5816.98	5816.98	k4
</s>
<s>
Alaska	Alaska	k1gFnSc1
<g/>
1883	#num#	k4
(	(	kIx(
<g/>
29	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
Guion	Guion	k1gInSc1
</s>
<s>
QueenstownSandy	QueenstownSand	k1gInPc1
Hook	Hook	k1gInSc1
<g/>
28446	#num#	k4
<g/>
/	/	kIx~
<g/>
23	#num#	k4
<g/>
/	/	kIx~
<g/>
4817.05	4817.05	k4
</s>
<s>
Oregon	Oregon	k1gInSc1
<g/>
1884	#num#	k4
(	(	kIx(
<g/>
13	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
Guion	Guion	k1gInSc1
</s>
<s>
QueenstownSandy	QueenstownSand	k1gInPc1
Hook	Hook	k1gInSc1
<g/>
28616	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
1018.56	1018.56	k4
</s>
<s>
Etruria	Etrurium	k1gNnPc4
<g/>
1885	#num#	k4
(	(	kIx(
<g/>
16	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
22	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
Cunard	Cunard	k1gInSc1
</s>
<s>
QueenstownSandy	QueenstownSand	k1gInPc1
Hook	Hook	k1gInSc1
<g/>
28016	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
3118.73	3118.73	k4
</s>
<s>
RMS	RMS	kA
Umbria	Umbrium	k1gNnPc4
<g/>
1887	#num#	k4
(	(	kIx(
<g/>
29	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CunardQueenstownSandy	CunardQueenstownSanda	k1gFnPc1
Hook	Hooka	k1gFnPc2
</s>
<s>
28486	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
1219.22	1219.22	k4
</s>
<s>
Etruria	Etrurium	k1gNnPc4
<g/>
1888	#num#	k4
(	(	kIx(
<g/>
27	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
Cunard	Cunard	k1gInSc1
</s>
<s>
QueenstownSandy	QueenstownSand	k1gInPc1
Hook	Hook	k1gInSc1
<g/>
28546	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
5519.56	5519.56	k4
</s>
<s>
City	city	k1gNnSc1
of	of	k?
Paris	Paris	k1gMnSc1
</s>
<s>
1889	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
I	i	k9
<g/>
&	&	k?
<g/>
IQueenstown	IQueenstown	k1gInSc1
</s>
<s>
Sandy	Sandy	k6eAd1
Hook	Hook	k1gInSc1
<g/>
28555	#num#	k4
<g/>
/	/	kIx~
<g/>
23	#num#	k4
<g/>
/	/	kIx~
<g/>
719.95	719.95	k4
</s>
<s>
City	city	k1gNnSc1
of	of	k?
Paris	Paris	k1gMnSc1
</s>
<s>
1889	#num#	k4
(	(	kIx(
<g/>
22	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
I	i	k9
<g/>
&	&	k?
<g/>
IQueenstown	IQueenstown	k1gInSc1
</s>
<s>
Sandy	Sandy	k6eAd1
Hook	Hook	k1gInSc1
<g/>
27885	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
/	/	kIx~
<g/>
1820.01	1820.01	k4
</s>
<s>
Majestic	Majestice	k1gFnPc2
<g/>
1891	#num#	k4
(	(	kIx(
<g/>
30	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
W.	W.	kA
<g/>
StarQueenstownSandy	StarQueenstownSanda	k1gFnPc1
Hook	Hook	k1gMnSc1
</s>
<s>
27775	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
/	/	kIx~
<g/>
820.10	820.10	k4
</s>
<s>
Teutonic	Teutonice	k1gFnPc2
<g/>
1891	#num#	k4
(	(	kIx(
<g/>
13	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
W.	W.	kA
<g/>
StarQueenstownSandy	StarQueenstownSanda	k1gFnPc1
Hook	Hook	k1gMnSc1
</s>
<s>
27785	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
/	/	kIx~
<g/>
3120.35	3120.35	k4
</s>
<s>
City	city	k1gNnSc1
of	of	k?
Paris	Paris	k1gMnSc1
</s>
<s>
1892	#num#	k4
(	(	kIx(
<g/>
20	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
I	i	k9
<g/>
&	&	k?
<g/>
IQueenstown	IQueenstown	k1gInSc1
</s>
<s>
Sandy	Sandy	k6eAd1
Book	Book	k1gInSc1
<g/>
27855	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
/	/	kIx~
<g/>
5820.48	5820.48	k4
</s>
<s>
City	city	k1gNnSc1
of	of	k?
Paris	Paris	k1gMnSc1
</s>
<s>
1892	#num#	k4
(	(	kIx(
<g/>
13	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
<g/>
I	i	k9
<g/>
&	&	k?
<g/>
IQueenstown	IQueenstown	k1gInSc1
</s>
<s>
Sandy	Sandy	k6eAd1
Hook	Hook	k1gInSc1
<g/>
27825	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
/	/	kIx~
<g/>
2420.70	2420.70	k4
</s>
<s>
Campania	Campanium	k1gNnPc4
<g/>
1893	#num#	k4
(	(	kIx(
<g/>
18	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
Cunard	Cunard	k1gInSc1
</s>
<s>
QueenstownSandy	QueenstownSand	k1gInPc1
Hook	Hook	k1gInSc1
<g/>
28645	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
/	/	kIx~
<g/>
3721.12	3721.12	k4
</s>
<s>
Campania	Campanium	k1gNnPc4
<g/>
1894	#num#	k4
(	(	kIx(
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
Cunard	Cunard	k1gInSc1
</s>
<s>
QueenstownSandy	QueenstownSand	k1gInPc1
Hook	Hook	k1gInSc1
<g/>
27765	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
2921.44	2921.44	k4
</s>
<s>
Lucania	Lucanium	k1gNnPc4
<g/>
1894	#num#	k4
(	(	kIx(
<g/>
26	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CunardQueenstownSandy	CunardQueenstownSanda	k1gFnPc1
Hook	Hooka	k1gFnPc2
</s>
<s>
27875	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
3821.65	3821.65	k4
</s>
<s>
Lucania	Lucanium	k1gNnPc4
<g/>
1894	#num#	k4
(	(	kIx(
<g/>
23	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CunardQueenstownSandy	CunardQueenstownSanda	k1gFnPc1
Hook	Hooka	k1gFnPc2
</s>
<s>
27825	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
4821.75	4821.75	k4
</s>
<s>
Lucania	Lucanium	k1gNnPc4
<g/>
1894	#num#	k4
(	(	kIx(
<g/>
21	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CunardQueenstownSandy	CunardQueenstownSanda	k1gFnPc1
Hook	Hooka	k1gFnPc2
</s>
<s>
27795	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
2321.81	2321.81	k4
</s>
<s>
Kaiser	Kaiser	k1gMnSc1
Wilhelm	Wilhelm	k1gMnSc1
der	drát	k5eAaImRp2nS
Große	Groß	k1gMnSc2
</s>
<s>
1898	#num#	k4
(	(	kIx(
<g/>
30	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
NDLNeedlesSandy	NDLNeedlesSanda	k1gFnSc2
Hook	Hook	k1gInSc1
<g/>
3120	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
22.29	22.29	k4
</s>
<s>
Deutschland	Deutschland	k1gInSc1
</s>
<s>
1900	#num#	k4
(	(	kIx(
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
HapagEddystone	HapagEddyston	k1gInSc5
</s>
<s>
Sandy	Sandy	k6eAd1
Hook	Hook	k1gInSc1
<g/>
30445	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
/	/	kIx~
<g/>
4622.42	4622.42	k4
</s>
<s>
Deutschland	Deutschland	k1gInSc1
</s>
<s>
1900	#num#	k4
(	(	kIx(
<g/>
26	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
)	)	kIx)
<g/>
HapagCherbourg	HapagCherbourg	k1gInSc1
</s>
<s>
Sandy	Sandy	k6eAd1
Hook	Hook	k1gInSc1
<g/>
30505	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
2923.02	2923.02	k4
</s>
<s>
Deutschland	Deutschland	k1gInSc1
</s>
<s>
1901	#num#	k4
(	(	kIx(
<g/>
26	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
HapagCherbourg	HapagCherbourg	k1gInSc1
</s>
<s>
Sandy	Sandy	k6eAd1
Hook	Hook	k1gInSc1
<g/>
31415	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
/	/	kIx~
<g/>
1223.06	1223.06	k4
</s>
<s>
Kronprinz	Kronprinz	k1gMnSc1
Wilhelm	Wilhelm	k1gMnSc1
</s>
<s>
1902	#num#	k4
(	(	kIx(
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
)	)	kIx)
<g/>
NDLCherbourg	NDLCherbourg	k1gInSc1
</s>
<s>
Sandy	Sandy	k6eAd1
Hook	Hook	k1gInSc1
<g/>
30475	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
5723.09	5723.09	k4
</s>
<s>
Deutschland	Deutschland	k1gInSc1
</s>
<s>
1903	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
)	)	kIx)
<g/>
HapagCherbourg	HapagCherbourg	k1gInSc1
</s>
<s>
Sandy	Sandy	k6eAd1
Hook	Hook	k1gInSc1
<g/>
30545	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
5423.15	5423.15	k4
</s>
<s>
Lusitania	Lusitanium	k1gNnPc4
<g/>
1907	#num#	k4
(	(	kIx(
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CunardQueenstownSandy	CunardQueenstownSanda	k1gFnPc1
Hook	Hooka	k1gFnPc2
</s>
<s>
27804	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
/	/	kIx~
<g/>
5223.99	5223.99	k4
</s>
<s>
Lusitania	Lusitanium	k1gNnPc4
<g/>
1908	#num#	k4
(	(	kIx(
<g/>
17	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CunardQueenstownSandy	CunardQueenstownSanda	k1gFnPc1
Hook	Hooka	k1gFnPc2
</s>
<s>
28894	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
/	/	kIx~
<g/>
2224.83	2224.83	k4
</s>
<s>
Lusitania	Lusitanium	k1gNnPc4
<g/>
1908	#num#	k4
(	(	kIx(
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CunardQueenstownSandy	CunardQueenstownSanda	k1gFnPc1
Hook	Hooka	k1gFnPc2
</s>
<s>
28914	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
/	/	kIx~
<g/>
3625.01	3625.01	k4
</s>
<s>
Lusitania	Lusitanium	k1gNnPc4
<g/>
1909	#num#	k4
(	(	kIx(
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CunardQueenstownAmbrose	CunardQueenstownAmbrosa	k1gFnSc3
</s>
<s>
28904	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
/	/	kIx~
<g/>
4025.65	4025.65	k4
</s>
<s>
Mauretania	Mauretanium	k1gNnPc1
</s>
<s>
1909	#num#	k4
(	(	kIx(
<g/>
26	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
)	)	kIx)
<g/>
Cunard	Cunard	k1gInSc1
</s>
<s>
QueenstownAmbrose	QueenstownAmbrosa	k1gFnSc6
<g/>
27844	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
51	#num#	k4
</s>
<s>
26.06	26.06	k4
</s>
<s>
Bremen	Bremen	k1gInSc1
<g/>
1929	#num#	k4
(	(	kIx(
<g/>
17	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
NDLCherbourgAmbrose	NDLCherbourgAmbrosa	k1gFnSc6
<g/>
31644	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
/	/	kIx~
<g/>
4227.83	4227.83	k4
</s>
<s>
Europa	Europa	k1gFnSc1
<g/>
1930	#num#	k4
(	(	kIx(
<g/>
20	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
NDLCherbourgAmbrose	NDLCherbourgAmbrosa	k1gFnSc6
<g/>
31574	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
/	/	kIx~
<g/>
627.91	627.91	k4
</s>
<s>
Europa	Europa	k1gFnSc1
<g/>
1933	#num#	k4
(	(	kIx(
<g/>
27	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
NDLCherbourgAmbrose	NDLCherbourgAmbrosa	k1gFnSc6
<g/>
31494	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
/	/	kIx~
<g/>
4827.92	4827.92	k4
</s>
<s>
Rex	Rex	k?
<g/>
1933	#num#	k4
(	(	kIx(
<g/>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ItalianGibraltarAmbrose	ItalianGibraltarAmbrosa	k1gFnSc3
<g/>
3181	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
/	/	kIx~
<g/>
5828.92	5828.92	k4
</s>
<s>
Normandie	Normandie	k1gFnSc1
<g/>
1935	#num#	k4
(	(	kIx(
<g/>
30	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CGTBishop	CGTBishop	k1gInSc1
RockAmbrose	RockAmbrosa	k1gFnSc6
<g/>
2971	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
229.98	229.98	k4
</s>
<s>
Queen	Queen	k1gInSc1
Mary	Mary	k1gFnSc1
<g/>
1936	#num#	k4
(	(	kIx(
<g/>
20	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
C-WSBishop	C-WSBishop	k1gInSc1
RockAmbrose	RockAmbrosa	k1gFnSc3
<g/>
2907	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
2730.14	2730.14	k4
</s>
<s>
Normandie	Normandie	k1gFnSc1
<g/>
1937	#num#	k4
(	(	kIx(
<g/>
29	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CGTBishop	CGTBishop	k1gInSc1
RockAmbrose	RockAmbrosa	k1gFnSc6
<g/>
2906	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
23	#num#	k4
<g/>
/	/	kIx~
<g/>
230.58	230.58	k4
</s>
<s>
Queen	Queen	k1gInSc1
Mary	Mary	k1gFnSc1
<g/>
1938	#num#	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
C-WSBishop	C-WSBishop	k1gInSc1
RockAmbrose	RockAmbrosa	k1gFnSc3
<g/>
2907	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
<g/>
/	/	kIx~
<g/>
4830.99	4830.99	k4
</s>
<s>
United	United	k1gMnSc1
States	States	k1gMnSc1
</s>
<s>
1952	#num#	k4
(	(	kIx(
<g/>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
USLBishop	USLBishop	k1gInSc1
Rock	rock	k1gInSc1
</s>
<s>
Ambrose	Ambrosa	k1gFnSc6
<g/>
29063	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
1234.51	1234.51	k4
</s>
<s>
Směrem	směr	k1gInSc7
na	na	k7c4
východ	východ	k1gInSc4
</s>
<s>
Jméno	jméno	k1gNnSc1
lodi	loď	k1gFnSc2
</s>
<s>
Datum	datum	k1gNnSc1
</s>
<s>
Rejdařství	rejdařství	k1gNnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
Do	do	k7c2
</s>
<s>
Námořních	námořní	k2eAgFnPc2d1
mil	míle	k1gFnPc2
</s>
<s>
Dny	den	k1gInPc1
<g/>
/	/	kIx~
<g/>
Hodiny	hodina	k1gFnPc1
<g/>
/	/	kIx~
<g/>
Minuty	minuta	k1gFnPc1
</s>
<s>
Rychlost	rychlost	k1gFnSc1
v	v	k7c6
uzlech	uzel	k1gInPc6
</s>
<s>
Sirius	Sirius	k1gInSc1
<g/>
1838	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
B	B	kA
<g/>
&	&	k?
<g/>
A	a	k9
</s>
<s>
New	New	k?
YorkFalmouth	YorkFalmouth	k1gInSc1
<g/>
(	(	kIx(
<g/>
3159	#num#	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
18	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
7.31	7.31	k4
<g/>
)	)	kIx)
</s>
<s>
Great	Great	k2eAgInSc1d1
Western	western	k1gInSc1
</s>
<s>
1838	#num#	k4
(	(	kIx(
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
GWNew	GWNew	k1gFnSc1
YorkAvonmouth	YorkAvonmouth	k1gInSc1
<g/>
3218	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
/	/	kIx~
<g/>
599.14	599.14	k4
</s>
<s>
Great	Great	k2eAgInSc1d1
Western	western	k1gInSc1
</s>
<s>
1838	#num#	k4
(	(	kIx(
<g/>
25	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
GWNew	GWNew	k1gFnSc1
YorkAvonmouth	YorkAvonmouth	k1gInSc1
<g/>
3099	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
/	/	kIx~
<g/>
3410.17	3410.17	k4
</s>
<s>
Britannia	Britannium	k1gNnPc4
<g/>
1840	#num#	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
Cunard	Cunard	k1gInSc1
</s>
<s>
HalifaxLiverpool	HalifaxLiverpool	k1gInSc1
<g/>
(	(	kIx(
<g/>
2610	#num#	k4
<g/>
)	)	kIx)
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
<g/>
/	/	kIx~
<g/>
44	#num#	k4
<g/>
(	(	kIx(
<g/>
10.98	10.98	k4
<g/>
)	)	kIx)
</s>
<s>
Great	Great	k2eAgInSc1d1
Western	western	k1gInSc1
</s>
<s>
1842	#num#	k4
(	(	kIx(
<g/>
28	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
GWNew	GWNew	k1gFnSc1
YorkLiverpool	YorkLiverpool	k1gInSc1
<g/>
3248	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
3010.99	3010.99	k4
</s>
<s>
Columbia	Columbia	k1gFnSc1
<g/>
1843	#num#	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
Cunard	Cunard	k1gInSc1
</s>
<s>
HalifaxLiverpool	HalifaxLiverpool	k1gInSc1
<g/>
(	(	kIx(
<g/>
2534	#num#	k4
<g/>
)	)	kIx)
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
(	(	kIx(
<g/>
11.11	11.11	k4
<g/>
)	)	kIx)
</s>
<s>
Hibernia	Hibernium	k1gNnPc4
<g/>
1843	#num#	k4
(	(	kIx(
<g/>
18	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
Cunard	Cunard	k1gInSc1
</s>
<s>
HalifaxLiverpool	HalifaxLiverpool	k1gInSc1
<g/>
(	(	kIx(
<g/>
2534	#num#	k4
<g/>
)	)	kIx)
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
44	#num#	k4
<g/>
(	(	kIx(
<g/>
11.18	11.18	k4
<g/>
)	)	kIx)
</s>
<s>
Hibernia	Hibernium	k1gNnPc4
<g/>
1843	#num#	k4
(	(	kIx(
<g/>
18	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
Cunard	Cunard	k1gInSc1
</s>
<s>
HalifaxLiverpool	HalifaxLiverpool	k1gInSc1
<g/>
(	(	kIx(
<g/>
2534	#num#	k4
<g/>
)	)	kIx)
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
<g/>
/	/	kIx~
<g/>
44	#num#	k4
<g/>
(	(	kIx(
<g/>
11.80	11.80	k4
<g/>
)	)	kIx)
</s>
<s>
Canada	Canada	k1gFnSc1
<g/>
1849	#num#	k4
(	(	kIx(
<g/>
19	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
Cunard	Cunard	k1gInSc1
</s>
<s>
HalifaxLiverpool	HalifaxLiverpool	k1gInSc1
<g/>
(	(	kIx(
<g/>
2534	#num#	k4
<g/>
)	)	kIx)
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
44	#num#	k4
<g/>
(	(	kIx(
<g/>
12.38	12.38	k4
<g/>
)	)	kIx)
</s>
<s>
Pacific	Pacific	k1gMnSc1
<g/>
1851	#num#	k4
(	(	kIx(
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
Collins	Collins	k1gInSc1
</s>
<s>
New	New	k?
YorkLiverpool	YorkLiverpool	k1gInSc1
<g/>
(	(	kIx(
<g/>
3078	#num#	k4
<g/>
)	)	kIx)
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
(	(	kIx(
<g/>
13.03	13.03	k4
<g/>
)	)	kIx)
</s>
<s>
Arctic	Arctice	k1gFnPc2
<g/>
1852	#num#	k4
(	(	kIx(
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
Collins	Collins	k1gInSc1
</s>
<s>
New	New	k?
YorkLiverpool	YorkLiverpool	k1gInSc1
<g/>
30519	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
/	/	kIx~
<g/>
1513.06	1513.06	k4
</s>
<s>
Persia	Persia	k1gFnSc1
<g/>
1856	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
Cunard	Cunard	k1gInSc1
</s>
<s>
Sandy	Sand	k1gInPc1
HookLiverpool	HookLiverpoola	k1gFnPc2
<g/>
(	(	kIx(
<g/>
3048	#num#	k4
<g/>
)	)	kIx)
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
<g/>
(	(	kIx(
<g/>
13.46	13.46	k4
<g/>
)	)	kIx)
</s>
<s>
Persia	Persia	k1gFnSc1
<g/>
1856	#num#	k4
(	(	kIx(
<g/>
14	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
Cunard	Cunard	k1gInSc1
</s>
<s>
Sandy	Sand	k1gInPc1
HookLiverpool	HookLiverpoola	k1gFnPc2
<g/>
(	(	kIx(
<g/>
3048	#num#	k4
<g/>
)	)	kIx)
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
24	#num#	k4
<g/>
(	(	kIx(
<g/>
13.89	13.89	k4
<g/>
)	)	kIx)
</s>
<s>
Persia	Persia	k1gFnSc1
<g/>
1856	#num#	k4
(	(	kIx(
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
Cunard	Cunard	k1gInSc1
</s>
<s>
Sandy	Sand	k1gInPc1
HookLiverpool	HookLiverpoola	k1gFnPc2
<g/>
(	(	kIx(
<g/>
3046	#num#	k4
<g/>
)	)	kIx)
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
23	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
(	(	kIx(
<g/>
14.15	14.15	k4
<g/>
)	)	kIx)
</s>
<s>
Scotia	Scotia	k1gFnSc1
<g/>
1863	#num#	k4
(	(	kIx(
<g/>
16	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
)	)	kIx)
<g/>
Cunard	Cunard	k1gInSc1
</s>
<s>
New	New	k?
YorkQueenstown	YorkQueenstown	k1gInSc1
<g/>
(	(	kIx(
<g/>
2800	#num#	k4
<g/>
)	)	kIx)
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
42	#num#	k4
<g/>
(	(	kIx(
<g/>
14.16	14.16	k4
<g/>
)	)	kIx)
</s>
<s>
City	city	k1gNnSc1
of	of	k?
Brussels	Brusselsa	k1gFnPc2
<g/>
1869	#num#	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
InmanSandy	InmanSand	k1gInPc1
HookQueenstown	HookQueenstowna	k1gFnPc2
<g/>
(	(	kIx(
<g/>
2780	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
/	/	kIx~
<g/>
33	#num#	k4
<g/>
(	(	kIx(
<g/>
14.74	14.74	k4
<g/>
)	)	kIx)
</s>
<s>
Baltic	Baltice	k1gFnPc2
<g/>
1873	#num#	k4
(	(	kIx(
<g/>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
W.	W.	kA
<g/>
Star	Star	kA
</s>
<s>
Sandy	Sanda	k1gFnPc1
HookQueenstown	HookQueenstown	k1gNnSc1
<g/>
28407	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
/	/	kIx~
<g/>
915.09	915.09	k4
</s>
<s>
City	city	k1gNnSc1
of	of	k?
Berlin	berlina	k1gFnPc2
<g/>
1875	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
InmanSandy	InmanSanda	k1gFnPc1
HookQueenstown	HookQueenstown	k1gNnSc1
<g/>
2820	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
/	/	kIx~
<g/>
2815.37	2815.37	k4
</s>
<s>
Germanic	Germanice	k1gFnPc2
<g/>
1876	#num#	k4
(	(	kIx(
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
W.	W.	kA
<g/>
Star	Star	kA
</s>
<s>
Sandy	Sanda	k1gFnPc1
HookQueenstown	HookQueenstown	k1gNnSc1
<g/>
28947	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
/	/	kIx~
<g/>
1715.79	1715.79	k4
</s>
<s>
Britannic	Britannice	k1gFnPc2
<g/>
1876	#num#	k4
(	(	kIx(
<g/>
16	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
W.	W.	kA
<g/>
StarSandy	StarSanda	k1gFnPc1
HookQueenstown	HookQueenstown	k1gMnSc1
</s>
<s>
28927	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
4115.94	4115.94	k4
</s>
<s>
Arizona	Arizona	k1gFnSc1
<g/>
1879	#num#	k4
(	(	kIx(
<g/>
22	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
Guion	Guion	k1gInSc1
</s>
<s>
Sandy	Sanda	k1gFnPc1
HookQueenstown	HookQueenstown	k1gNnSc1
<g/>
28107	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
1115.96	1115.96	k4
</s>
<s>
Alaska	Alaska	k1gFnSc1
<g/>
1882	#num#	k4
(	(	kIx(
<g/>
30	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
Guion	Guion	k1gInSc1
</s>
<s>
Sandy	Sandy	k6eAd1
HookQueenstown	HookQueenstown	k1gNnSc1
<g/>
(	(	kIx(
<g/>
2791	#num#	k4
<g/>
)	)	kIx)
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
(	(	kIx(
<g/>
16.81	16.81	k4
<g/>
)	)	kIx)
</s>
<s>
Alaska	Alaska	k1gFnSc1
<g/>
1882	#num#	k4
(	(	kIx(
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
)	)	kIx)
<g/>
Guion	Guion	k1gInSc1
</s>
<s>
Sandy	Sanda	k1gFnPc1
HookQueenstown	HookQueenstown	k1gNnSc1
<g/>
27816	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
/	/	kIx~
<g/>
3717.10	3717.10	k4
</s>
<s>
Oregon	Oregon	k1gInSc1
<g/>
1884	#num#	k4
(	(	kIx(
<g/>
29	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
Guion	Guion	k1gInSc1
</s>
<s>
Sandy	Sanda	k1gFnPc1
HookQueenstown	HookQueenstown	k1gNnSc1
<g/>
29167	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
1817.12	1817.12	k4
</s>
<s>
Oregon	Oregon	k1gInSc1
<g/>
1884	#num#	k4
(	(	kIx(
<g/>
26	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
Guion	Guion	k1gInSc1
</s>
<s>
Sandy	Sanda	k1gFnPc1
HookQueenstown	HookQueenstown	k1gNnSc1
<g/>
2916	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
/	/	kIx~
<g/>
5718.09	5718.09	k4
</s>
<s>
Oregon	Oregon	k1gInSc1
<g/>
1884	#num#	k4
(	(	kIx(
<g/>
30	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
Cunard	Cunard	k1gInSc1
</s>
<s>
Sandy	Sanda	k1gFnPc1
HookQueenstown	HookQueenstown	k1gNnSc1
<g/>
28536	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
5418.18	5418.18	k4
</s>
<s>
Oregon	Oregon	k1gInSc1
<g/>
1884	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
)	)	kIx)
<g/>
Cunard	Cunard	k1gInSc1
</s>
<s>
Sandy	Sanda	k1gFnPc1
HookQueenstown	HookQueenstown	k1gNnSc1
<g/>
28536	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
918.39	918.39	k4
</s>
<s>
Etruria	Etrurium	k1gNnPc4
<g/>
1885	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
Cunard	Cunard	k1gInSc1
</s>
<s>
Sandy	Sanda	k1gFnPc1
HookQueenstown	HookQueenstown	k1gNnSc1
<g/>
28226	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
18.44	18.44	k4
</s>
<s>
Etruria	Etrurium	k1gNnPc4
<g/>
1888	#num#	k4
(	(	kIx(
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
Cunard	Cunard	k1gInSc1
</s>
<s>
Sandy	Sanda	k1gFnPc1
HookQueenstown	HookQueenstown	k1gNnSc1
<g/>
29816	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
5019.36	5019.36	k4
</s>
<s>
City	city	k1gNnSc1
of	of	k?
Paris	Paris	k1gMnSc1
</s>
<s>
1889	#num#	k4
(	(	kIx(
<g/>
15	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
I	i	k9
<g/>
&	&	k?
<g/>
ISandy	ISanda	k1gFnPc1
Hook	Hooka	k1gFnPc2
</s>
<s>
Queenstown	Queenstown	k1gInSc1
<g/>
28946	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
2920.03	2920.03	k4
</s>
<s>
City	city	k1gNnSc1
of	of	k?
New	New	k1gFnSc1
York	York	k1gInSc1
</s>
<s>
1892	#num#	k4
(	(	kIx(
<g/>
17	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
I	i	k9
<g/>
&	&	k?
<g/>
ISandy	ISanda	k1gFnPc1
Hook	Hooka	k1gFnPc2
</s>
<s>
Queenstown	Queenstown	k1gInSc1
<g/>
28145	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
/	/	kIx~
<g/>
5720.11	5720.11	k4
</s>
<s>
Campania	Campanium	k1gNnPc4
<g/>
1893	#num#	k4
(	(	kIx(
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
Cunard	Cunard	k1gInSc1
</s>
<s>
Sandy	Sanda	k1gFnPc1
HookQueenstown	HookQueenstown	k1gNnSc1
<g/>
29285	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
/	/	kIx~
<g/>
2721.30	2721.30	k4
</s>
<s>
Lucania	Lucanium	k1gNnPc4
<g/>
1894	#num#	k4
(	(	kIx(
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CunardSandy	CunardSanda	k1gFnPc1
HookQueenstown	HookQueenstowna	k1gFnPc2
</s>
<s>
29113	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
/	/	kIx~
<g/>
2821.81	2821.81	k4
</s>
<s>
Lucania	Lucanium	k1gNnPc4
<g/>
1894	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CunardSandy	CunardSanda	k1gFnPc1
HookQueenstown	HookQueenstowna	k1gFnPc2
</s>
<s>
29115	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
5921.90	5921.90	k4
</s>
<s>
Lucania	Lucanium	k1gNnPc4
<g/>
1895	#num#	k4
(	(	kIx(
<g/>
18	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CunardSandy	CunardSanda	k1gFnPc1
HookQueenstown	HookQueenstowna	k1gFnPc2
</s>
<s>
28975	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
4022.00	4022.00	k4
</s>
<s>
Kaiser	Kaiser	k1gMnSc1
Wilhelm	Wilhelm	k1gMnSc1
der	drát	k5eAaImRp2nS
Große	Große	k1gNnSc2
</s>
<s>
1897	#num#	k4
(	(	kIx(
<g/>
23	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
)	)	kIx)
<g/>
NDLSandy	NDLSanda	k1gFnSc2
Hook	Hook	k1gInSc1
</s>
<s>
Needles	Needles	k1gInSc1
<g/>
30655	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
/	/	kIx~
<g/>
2322.33	2322.33	k4
</s>
<s>
Deutschland	Deutschland	k1gInSc1
</s>
<s>
1900	#num#	k4
(	(	kIx(
<g/>
18	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
HapagSandy	HapagSanda	k1gFnSc2
Hook	Hook	k1gInSc1
</s>
<s>
Eddystone	Eddyston	k1gInSc5
<g/>
30855	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
/	/	kIx~
<g/>
522.84	522.84	k4
</s>
<s>
Deutschland	Deutschland	k1gInSc1
</s>
<s>
1900	#num#	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
)	)	kIx)
<g/>
HapagSandy	HapagSanda	k1gFnSc2
Hook	Hook	k1gMnSc1
</s>
<s>
Eddystone	Eddyston	k1gInSc5
<g/>
29815	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
3823.36	3823.36	k4
</s>
<s>
Deutschland	Deutschland	k1gInSc1
</s>
<s>
1901	#num#	k4
(	(	kIx(
<g/>
13	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
HapagSandy	HapagSanda	k1gFnSc2
Hook	Hook	k1gMnSc1
</s>
<s>
Eddystone	Eddyston	k1gInSc5
<g/>
30835	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
5123.38	5123.38	k4
</s>
<s>
Deutschland	Deutschland	k1gInSc1
</s>
<s>
1901	#num#	k4
(	(	kIx(
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
HapagSandy	HapagSanda	k1gFnSc2
Hook	Hook	k1gMnSc1
</s>
<s>
Eddystone	Eddyston	k1gInSc5
<g/>
30825	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
523.51	523.51	k4
</s>
<s>
Kaiser	Kaiser	k1gMnSc1
Wilhelm	Wilhelm	k1gMnSc1
II	II	kA
</s>
<s>
1904	#num#	k4
(	(	kIx(
<g/>
14	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
NDLSandy	NDLSanda	k1gFnSc2
Hook	Hook	k1gInSc1
</s>
<s>
Eddystone	Eddyston	k1gInSc5
<g/>
31125	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
5823.58	5823.58	k4
</s>
<s>
Lusitania	Lusitanium	k1gNnPc4
<g/>
1907	#num#	k4
(	(	kIx(
<g/>
19	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CunardSandy	CunardSanda	k1gFnPc1
HookQueenstown	HookQueenstown	k1gNnSc1
<g/>
2807	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
<g/>
/	/	kIx~
<g/>
5323.61	5323.61	k4
</s>
<s>
Mauretania	Mauretanium	k1gNnPc1
</s>
<s>
1907	#num#	k4
(	(	kIx(
<g/>
30	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
)	)	kIx)
<g/>
CunardBeady	CunardBeada	k1gFnSc2
Hook	Hook	k1gMnSc1
</s>
<s>
Queenstown	Queenstown	k1gInSc1
<g/>
28074	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
<g/>
/	/	kIx~
<g/>
3323.69	3323.69	k4
</s>
<s>
Mauretania	Mauretanium	k1gNnPc1
</s>
<s>
1908	#num#	k4
(	(	kIx(
<g/>
25	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
CunardSandy	CunardSanda	k1gFnSc2
Hook	Hook	k1gMnSc1
</s>
<s>
Queenstown	Queenstown	k1gInSc1
<g/>
29325	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
4123.90	4123.90	k4
</s>
<s>
Mauretania	Mauretanium	k1gNnPc1
</s>
<s>
1908	#num#	k4
(	(	kIx(
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
CunardSandy	CunardSanda	k1gFnSc2
Hook	Hook	k1gMnSc1
</s>
<s>
Queenstown	Queenstown	k1gInSc1
<g/>
29325	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
524.42	524.42	k4
</s>
<s>
Mauretania	Mauretanium	k1gNnPc1
</s>
<s>
1909	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
CunardAmbroseQueenstown	CunardAmbroseQueenstown	k1gInSc1
<g/>
,2930	,2930	k4
</s>
<s>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
/	/	kIx~
<g/>
2725.16	2725.16	k4
</s>
<s>
Mauretania	Mauretanium	k1gNnPc1
</s>
<s>
1909	#num#	k4
(	(	kIx(
<g/>
17	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
CunardAmbrose	CunardAmbrosa	k1gFnSc6
</s>
<s>
Queenstown	Queenstown	k1gInSc1
<g/>
29344	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
/	/	kIx~
<g/>
3525.61	3525.61	k4
</s>
<s>
Mauretania	Mauretanium	k1gNnPc1
</s>
<s>
1909	#num#	k4
(	(	kIx(
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
CunardAmbrose	CunardAmbrosa	k1gFnSc6
</s>
<s>
Queenstown	Queenstown	k1gInSc1
<g/>
29344	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
/	/	kIx~
<g/>
1125.70	1125.70	k4
</s>
<s>
Mauretania	Mauretanium	k1gNnPc1
</s>
<s>
1909	#num#	k4
(	(	kIx(
<g/>
16	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
CunardAmbrose	CunardAmbrosa	k1gFnSc6
</s>
<s>
Queenstown	Queenstown	k1gInSc1
<g/>
29334	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
/	/	kIx~
<g/>
2125.88	2125.88	k4
</s>
<s>
Mauretania	Mauretanium	k1gNnPc1
</s>
<s>
1924	#num#	k4
(	(	kIx(
<g/>
20	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
CunardAmbrose	CunardAmbrosa	k1gFnSc6
</s>
<s>
Cherbourg	Cherbourg	k1gInSc1
<g/>
31985	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
4926.25	4926.25	k4
</s>
<s>
Bremen	Bremen	k1gInSc1
<g/>
1929	#num#	k4
(	(	kIx(
<g/>
27	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
NDLAmbroseEddystone	NDLAmbroseEddyston	k1gInSc5
<g/>
30844	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
/	/	kIx~
<g/>
3027.91	3027.91	k4
</s>
<s>
Bremen	Bremen	k1gInSc1
<g/>
1933	#num#	k4
(	(	kIx(
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
NDLAmbroseCherbourg	NDLAmbroseCherbourg	k1gInSc1
<g/>
31994	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
/	/	kIx~
<g/>
1528.51	1528.51	k4
</s>
<s>
Normandie	Normandie	k1gFnSc1
<g/>
1935	#num#	k4
(	(	kIx(
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CGTAmbroseBishop	CGTAmbroseBishop	k1gInSc1
Rock	rock	k1gInSc1
<g/>
3015	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
2530.31	2530.31	k4
</s>
<s>
Queen	Queen	k1gInSc1
Mary	Mary	k1gFnSc1
<g/>
1936	#num#	k4
(	(	kIx(
<g/>
26	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
C-WSAmbroseBishop	C-WSAmbroseBishop	k1gInSc1
Rock	rock	k1gInSc1
<g/>
2939	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
23	#num#	k4
<g/>
/	/	kIx~
<g/>
5730.63	5730.63	k4
</s>
<s>
Normandie	Normandie	k1gFnSc1
<g/>
1937	#num#	k4
(	(	kIx(
<g/>
18	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CGTAmbroseBishop	CGTAmbroseBishop	k1gInSc1
Rock	rock	k1gInSc1
<g/>
2967	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
630.99	630.99	k4
</s>
<s>
Normandie	Normandie	k1gFnSc1
<g/>
1937	#num#	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
CGTAmbroseBishop	CGTAmbroseBishop	k1gInSc1
Rock	rock	k1gInSc1
<g/>
2936	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
22	#num#	k4
<g/>
/	/	kIx~
<g/>
731.20	731.20	k4
</s>
<s>
Queen	Queen	k1gInSc1
Mary	Mary	k1gFnSc1
<g/>
1938	#num#	k4
(	(	kIx(
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
C-WSAmbroseBishop	C-WSAmbroseBishop	k1gInSc1
Rock	rock	k1gInSc1
<g/>
2938	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
/	/	kIx~
<g/>
4231.69	4231.69	k4
</s>
<s>
United	United	k1gMnSc1
States	States	k1gMnSc1
</s>
<s>
1952	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
USLAmbroseBishop	USLAmbroseBishop	k1gInSc1
Rock	rock	k1gInSc1
<g/>
2942	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
4035.59	4035.59	k4
</s>
<s>
Hoverspeed	Hoverspeed	k1gInSc1
Great	Great	k2eAgInSc4d1
Britain	Britain	k1gInSc4
<g/>
1990	#num#	k4
Jun	jun	k1gMnSc1
23	#num#	k4
</s>
<s>
Aegean	Aegean	k1gMnSc1
Speedlines	Speedlines	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
5436.6	5436.6	k4
</s>
<s>
Catalonia	Catalonium	k1gNnPc1
<g/>
1998	#num#	k4
Jun	jun	k1gMnSc1
9	#num#	k4
<g/>
Buquebus	Buquebus	k1gInSc1
</s>
<s>
ManhattanTarifa	ManhattanTarif	k1gMnSc2
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
<g/>
31253	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
4038.9	4038.9	k4
</s>
<s>
Cat-Link	Cat-Link	k1gInSc1
V	V	kA
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Master	master	k1gMnSc1
Cat	Cat	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1998	#num#	k4
Jul	Jula	k1gFnPc2
20	#num#	k4
<g/>
Master	master	k1gMnSc1
Ferries	Ferries	k1gMnSc1
</s>
<s>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
/	/	kIx~
<g/>
941.3	941.3	k4
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Stejný	stejný	k2eAgInSc1d1
význam	význam	k1gInSc1
„	„	k?
<g/>
modrá	modrat	k5eAaImIp3nS
stuha	stuha	k1gFnSc1
<g/>
“	“	k?
má	mít	k5eAaImIp3nS
i	i	k9
francouzské	francouzský	k2eAgNnSc1d1
kuchařské	kuchařský	k2eAgNnSc1d1
vyznamenání	vyznamenání	k1gNnSc1
Cordon	Cordona	k1gFnPc2
bleu	bleus	k1gInSc2
a	a	k8xC
stejnojmenný	stejnojmenný	k2eAgInSc4d1
pokrm	pokrm	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
M.	M.	kA
<g/>
Michajlov	Michajlov	k1gInSc1
<g/>
,	,	kIx,
O.	O.	kA
<g/>
Sokolov	Sokolov	k1gInSc1
-	-	kIx~
Od	od	k7c2
drakkaru	drakkar	k1gInSc2
ke	k	k7c3
křižníku	křižník	k1gInSc2
<g/>
,	,	kIx,
vydal	vydat	k5eAaPmAgMnS
Albatros	albatros	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Článek	článek	k1gInSc1
o	o	k7c6
Modré	modrý	k2eAgFnSc6d1
stuze	stuha	k1gFnSc6
Atlantiku	Atlantik	k1gInSc2
<g/>
,	,	kIx,
yin	yin	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
