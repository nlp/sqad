<s>
Modrá	modrý	k2eAgFnSc1d1
stuha	stuha	k1gFnSc1
při	při	k7c6
plavbě	plavba	k1gFnSc6
přes	přes	k7c4
Atlantský	atlantský	k2eAgInSc4d1
oceán	oceán	k1gInSc4
bylo	být	k5eAaImAgNnS
zvláštní	zvláštní	k2eAgNnSc1d1
prestižní	prestižní	k2eAgNnSc1d1
putovní	putovní	k2eAgNnSc1d1
ocenění	ocenění	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
bylo	být	k5eAaImAgNnS
udělováno	udělován	k2eAgNnSc1d1
jednotlivým	jednotlivý	k2eAgFnPc3d1
lodím	loď	k1gFnPc3
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
nejrychleji	rychle	k6eAd3
přepluly	přeplout	k5eAaPmAgFnP
severní	severní	k2eAgInSc4d1
Atlantský	atlantský	k2eAgInSc4d1
oceán	oceán	k1gInSc4
na	na	k7c6
trase	trasa	k1gFnSc6
z	z	k7c2
New	New	k1gFnSc2
Yorku	York	k1gInSc2
do	do	k7c2
Evropy	Evropa	k1gFnSc2
(	(	kIx(
<g/>
obvykle	obvykle	k6eAd1
do	do	k7c2
Liverpoolu	Liverpool	k1gInSc2
nebo	nebo	k8xC
i	i	k9
jiného	jiný	k2eAgInSc2d1
evropského	evropský	k2eAgInSc2d1
přístavu	přístav	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>