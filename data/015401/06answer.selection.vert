<s desamb="1">
Prvním	první	k4xOgMnSc7
držitelem	držitel	k1gMnSc7
této	tento	k3xDgFnSc2
ceny	cena	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1838	#num#	k4
stal	stát	k5eAaPmAgInS
kolesový	kolesový	k2eAgInSc1d1
parník	parník	k1gInSc1
SS	SS	kA
Sirius	Sirius	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
vykonal	vykonat	k5eAaPmAgInS
cestu	cesta	k1gFnSc4
z	z	k7c2
Corku	Cork	k1gInSc2
do	do	k7c2
New	New	k1gFnSc2
Yorku	York	k1gInSc2
rychlostí	rychlost	k1gFnSc7
8,03	8,03	k4
uzlu	uzel	k1gInSc2
(	(	kIx(
<g/>
14,87	14,87	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
za	za	k7c4
18	#num#	k4
dní	den	k1gInPc2
<g/>
,	,	kIx,
14	#num#	k4
hodin	hodina	k1gFnPc2
a	a	k8xC
22	#num#	k4
minut	minuta	k1gFnPc2
a	a	k8xC
cestu	cesta	k1gFnSc4
z	z	k7c2
New	New	k1gFnSc2
Yorku	York	k1gInSc2
do	do	k7c2
Falmouthu	Falmouth	k1gInSc2
rychlostí	rychlost	k1gFnSc7
7,31	7,31	k4
uzlu	uzel	k1gInSc2
za	za	k7c4
18	#num#	k4
dní	den	k1gInPc2
<g/>
.	.	kIx.
</s>