<s>
Roger	Roger	k1gMnSc1	Roger
Federer	Federer	k1gMnSc1	Federer
(	(	kIx(	(
<g/>
výslovnost	výslovnost	k1gFnSc1	výslovnost
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
;	;	kIx,	;
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1981	[number]	k4	1981
Basilej	Basilej	k1gFnSc1	Basilej
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
profesionální	profesionální	k2eAgMnSc1d1	profesionální
tenista	tenista	k1gMnSc1	tenista
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
řada	řada	k1gFnSc1	řada
komentátorů	komentátor	k1gMnPc2	komentátor
<g/>
,	,	kIx,	,
odborníků	odborník	k1gMnPc2	odborník
a	a	k8xC	a
tenistů	tenista	k1gMnPc2	tenista
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
hráče	hráč	k1gMnSc4	hráč
historie	historie	k1gFnSc2	historie
tenisu	tenis	k1gInSc2	tenis
<g/>
.	.	kIx.	.
</s>
