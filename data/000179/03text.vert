<s>
Jakub	Jakub	k1gMnSc1	Jakub
Uličník	uličník	k1gMnSc1	uličník
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
divadelní	divadelní	k2eAgMnSc1d1	divadelní
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
All	All	k1gMnSc1	All
X	X	kA	X
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
Konzervatoře	konzervatoř	k1gFnSc2	konzervatoř
Brno	Brno	k1gNnSc4	Brno
byl	být	k5eAaImAgMnS	být
přijat	přijmout	k5eAaPmNgMnS	přijmout
na	na	k7c6	na
JAMU	jam	k1gInSc6	jam
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
má	mít	k5eAaImIp3nS	mít
stálé	stálý	k2eAgNnSc4d1	stálé
angažmá	angažmá	k1gNnSc4	angažmá
v	v	k7c6	v
Městském	městský	k2eAgNnSc6d1	Městské
divadle	divadlo	k1gNnSc6	divadlo
Brno	Brno	k1gNnSc1	Brno
Jeho	jeho	k3xOp3gNnSc1	jeho
ženou	žena	k1gFnSc7	žena
je	být	k5eAaImIp3nS	být
podnikatelka	podnikatelka	k1gFnSc1	podnikatelka
Kateřina	Kateřina	k1gFnSc1	Kateřina
Uličník	uličník	k1gInSc4	uličník
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
dcery	dcera	k1gFnPc4	dcera
<g/>
:	:	kIx,	:
Rozálii	Rozálie	k1gFnSc4	Rozálie
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
a	a	k8xC	a
Anastázii	Anastázie	k1gFnSc4	Anastázie
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
provozují	provozovat	k5eAaImIp3nP	provozovat
restauraci	restaurace	k1gFnSc4	restaurace
Středověká	středověký	k2eAgFnSc1d1	středověká
krčma	krčma	k1gFnSc1	krčma
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Bubulja	Bubulja	k1gFnSc1	Bubulja
-	-	kIx~	-
Cikáni	cikán	k1gMnPc1	cikán
jdou	jít	k5eAaImIp3nP	jít
do	do	k7c2	do
nebe	nebe	k1gNnSc2	nebe
Riff	Riff	k1gInSc1	Riff
-	-	kIx~	-
West	West	k2eAgInSc1d1	West
Side	Side	k1gInSc1	Side
Story	story	k1gFnSc1	story
Josef	Josef	k1gMnSc1	Josef
-	-	kIx~	-
Josef	Josef	k1gMnSc1	Josef
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
úžasný	úžasný	k2eAgInSc1d1	úžasný
pestrobarevný	pestrobarevný	k2eAgInSc1d1	pestrobarevný
plášť	plášť	k1gInSc1	plášť
Kejchal	kejchat	k5eAaImAgInS	kejchat
-	-	kIx~	-
Sněhurka	Sněhurka	k1gFnSc1	Sněhurka
a	a	k8xC	a
sedm	sedm	k4xCc1	sedm
trpaslíků	trpaslík	k1gMnPc2	trpaslík
Petr	Petr	k1gMnSc1	Petr
-	-	kIx~	-
Jesus	Jesus	k1gMnSc1	Jesus
Christ	Christ	k1gMnSc1	Christ
Superstar	superstar	k1gFnPc2	superstar
Marius	Marius	k1gMnSc1	Marius
-	-	kIx~	-
Les	les	k1gInSc1	les
Misérables	Misérables	k1gInSc1	Misérables
(	(	kIx(	(
<g/>
Bídníci	bídník	k1gMnPc1	bídník
<g/>
)	)	kIx)	)
Michael	Michael	k1gMnSc1	Michael
-	-	kIx~	-
Čarodějky	čarodějka	k1gFnPc1	čarodějka
z	z	k7c2	z
Eastwicku	Eastwick	k1gInSc2	Eastwick
(	(	kIx(	(
<g/>
muzikál	muzikál	k1gInSc1	muzikál
<g/>
)	)	kIx)	)
Seymour	Seymour	k1gMnSc1	Seymour
-	-	kIx~	-
Kvítek	kvítek	k1gInSc1	kvítek
z	z	k7c2	z
horrroru	horrror	k1gInSc2	horrror
Biondello	Biondello	k1gNnSc1	Biondello
-	-	kIx~	-
Zkrocení	zkrocení	k1gNnSc1	zkrocení
zlé	zlý	k2eAgFnSc2d1	zlá
ženy	žena	k1gFnSc2	žena
Dave	Dav	k1gInSc5	Dav
Bukatinsky	Bukatinsky	k1gMnPc1	Bukatinsky
-	-	kIx~	-
Donaha	donaha	k6eAd1	donaha
<g/>
!	!	kIx.	!
</s>
<s>
Nemotorus	Nemotorus	k1gInSc1	Nemotorus
-	-	kIx~	-
Let	léto	k1gNnPc2	léto
snů	sen	k1gInPc2	sen
LILI	lít	k5eAaImAgMnP	lít
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jakub	Jakub	k1gMnSc1	Jakub
Uličník	uličník	k1gInSc1	uličník
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Jakub	Jakub	k1gMnSc1	Jakub
Uličník	uličník	k1gMnSc1	uličník
na	na	k7c6	na
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
stránkách	stránka	k1gFnPc6	stránka
Městského	městský	k2eAgNnSc2d1	Městské
divadla	divadlo	k1gNnSc2	divadlo
Brno	Brno	k1gNnSc1	Brno
</s>
