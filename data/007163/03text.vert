<s>
Židle	židle	k1gFnSc1	židle
(	(	kIx(	(
<g/>
zastarale	zastarale	k6eAd1	zastarale
nebo	nebo	k8xC	nebo
nářečně	nářečně	k6eAd1	nářečně
sesle	sesle	k1gFnSc1	sesle
z	z	k7c2	z
něm.	něm.	k?	něm.
Sessel	Sessel	k1gInSc1	Sessel
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nábytek	nábytek	k1gInSc1	nábytek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
k	k	k7c3	k
sezení	sezení	k1gNnSc3	sezení
a	a	k8xC	a
k	k	k7c3	k
podpírání	podpírání	k1gNnSc3	podpírání
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
židle	židle	k1gFnSc1	židle
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
opěrátka	opěrátko	k1gNnSc2	opěrátko
<g/>
,	,	kIx,	,
sedáku	sedák	k1gInSc2	sedák
a	a	k8xC	a
nosné	nosný	k2eAgFnPc1d1	nosná
části	část	k1gFnPc1	část
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
4	[number]	k4	4
nohy	noha	k1gFnPc4	noha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výjimkou	výjimka	k1gFnSc7	výjimka
nejsou	být	k5eNaImIp3nP	být
více-	více-	k?	více-
či	či	k8xC	či
méněnohé	méněnohý	k2eAgFnPc4d1	méněnohý
varianty	varianta	k1gFnPc4	varianta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Doplňkem	doplněk	k1gInSc7	doplněk
židle	židle	k1gFnSc2	židle
je	být	k5eAaImIp3nS	být
područka	područka	k1gFnSc1	područka
(	(	kIx(	(
<g/>
opěrátko	opěrátko	k1gNnSc1	opěrátko
na	na	k7c6	na
ruce	ruka	k1gFnSc6	ruka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgMnSc7	který
se	se	k3xPyFc4	se
už	už	k6eAd1	už
zařazuje	zařazovat	k5eAaImIp3nS	zařazovat
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
nábytku	nábytek	k1gInSc2	nábytek
sloužícího	sloužící	k2eAgNnSc2d1	sloužící
především	především	k9	především
k	k	k7c3	k
odpočinku	odpočinek	k1gInSc3	odpočinek
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
křeslo	křeslo	k1gNnSc4	křeslo
<g/>
.	.	kIx.	.
</s>
<s>
Židle	židle	k1gFnSc1	židle
může	moct	k5eAaImIp3nS	moct
a	a	k8xC	a
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
polstrovaná	polstrovaný	k2eAgFnSc1d1	polstrovaná
<g/>
;	;	kIx,	;
křeslo	křeslo	k1gNnSc1	křeslo
je	být	k5eAaImIp3nS	být
polstrované	polstrovaný	k2eAgNnSc1d1	polstrované
vždy	vždy	k6eAd1	vždy
<g/>
.	.	kIx.	.
</s>
<s>
Židle	židle	k1gFnSc1	židle
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
jednu	jeden	k4xCgFnSc4	jeden
osobu	osoba	k1gFnSc4	osoba
a	a	k8xC	a
neslouží	sloužit	k5eNaImIp3nP	sloužit
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
relaxačním	relaxační	k2eAgInPc3d1	relaxační
účelům	účel	k1gInPc3	účel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
například	například	k6eAd1	například
i	i	k9	i
při	při	k7c6	při
stolování	stolování	k1gNnSc6	stolování
<g/>
,	,	kIx,	,
k	k	k7c3	k
úlevové	úlevový	k2eAgFnSc3d1	úlevová
poloze	poloha	k1gFnSc3	poloha
nebo	nebo	k8xC	nebo
při	při	k7c6	při
různé	různý	k2eAgFnSc6d1	různá
práci	práce	k1gFnSc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
Pohovka	pohovka	k1gFnSc1	pohovka
<g/>
,	,	kIx,	,
lavička	lavička	k1gFnSc1	lavička
<g/>
,	,	kIx,	,
gauč	gauč	k1gInSc4	gauč
atd.	atd.	kA	atd.
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgFnP	určit
pro	pro	k7c4	pro
více	hodně	k6eAd2	hodně
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
využití	využití	k1gNnSc1	využití
nacházejí	nacházet	k5eAaImIp3nP	nacházet
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
odpočinkové	odpočinkový	k2eAgFnSc6d1	odpočinková
činnosti	činnost	k1gFnSc6	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Židle	židle	k1gFnSc1	židle
je	být	k5eAaImIp3nS	být
vyrobena	vyrobit	k5eAaPmNgFnS	vyrobit
téměř	téměř	k6eAd1	téměř
z	z	k7c2	z
libovolného	libovolný	k2eAgInSc2d1	libovolný
pevného	pevný	k2eAgInSc2d1	pevný
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mezi	mezi	k7c4	mezi
nejčastější	častý	k2eAgNnSc4d3	nejčastější
patří	patřit	k5eAaImIp3nS	patřit
židle	židle	k1gFnPc4	židle
vyrobené	vyrobený	k2eAgFnPc4d1	vyrobená
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ratan	ratany	k1gInPc2	ratany
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plastu	plast	k1gInSc2	plast
<g/>
,	,	kIx,	,
kovu	kov	k1gInSc2	kov
<g/>
,	,	kIx,	,
pleteného	pletený	k2eAgNnSc2d1	pletené
proutí	proutí	k1gNnSc2	proutí
atd.	atd.	kA	atd.
Výška	výška	k1gFnSc1	výška
sedáku	sedák	k1gInSc2	sedák
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
40	[number]	k4	40
až	až	k9	až
50	[number]	k4	50
cm	cm	kA	cm
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
sedící	sedící	k2eAgMnSc1d1	sedící
člověk	člověk	k1gMnSc1	člověk
měl	mít	k5eAaImAgMnS	mít
nohy	noha	k1gFnPc4	noha
v	v	k7c6	v
kolenou	koleno	k1gNnPc6	koleno
ohnuté	ohnutý	k2eAgFnSc2d1	ohnutá
v	v	k7c6	v
tupém	tupý	k2eAgInSc6d1	tupý
úhlu	úhel	k1gInSc6	úhel
<g/>
.	.	kIx.	.
</s>
<s>
Úhel	úhel	k1gInSc1	úhel
opěradla	opěradlo	k1gNnSc2	opěradlo
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
takový	takový	k3xDgInSc1	takový
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
osoba	osoba	k1gFnSc1	osoba
při	při	k7c6	při
opření	opření	k1gNnSc6	opření
v	v	k7c6	v
mírném	mírný	k2eAgInSc6d1	mírný
záklonu	záklon	k1gInSc6	záklon
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
opěradla	opěradlo	k1gNnSc2	opěradlo
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
přibližně	přibližně	k6eAd1	přibližně
taková	takový	k3xDgNnPc1	takový
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
pod	pod	k7c4	pod
lopatky	lopatka	k1gFnPc4	lopatka
nebo	nebo	k8xC	nebo
k	k	k7c3	k
ramenům	rameno	k1gNnPc3	rameno
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Kancelářská	kancelářský	k2eAgFnSc1d1	kancelářská
židle	židle	k1gFnSc1	židle
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
sedícímu	sedící	k2eAgMnSc3d1	sedící
vyšší	vysoký	k2eAgInSc4d2	vyšší
komfort	komfort	k1gInSc4	komfort
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
prvky	prvek	k1gInPc4	prvek
aktivního	aktivní	k2eAgNnSc2d1	aktivní
sezení	sezení	k1gNnSc2	sezení
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
typ	typ	k1gInSc4	typ
ergonomické	ergonomický	k2eAgFnSc2d1	ergonomická
židle	židle	k1gFnSc2	židle
pro	pro	k7c4	pro
dlouhodobé	dlouhodobý	k2eAgNnSc4d1	dlouhodobé
sezení	sezení	k1gNnSc4	sezení
<g/>
.	.	kIx.	.
</s>
<s>
Artefakt	artefakt	k1gInSc1	artefakt
Židle	židle	k1gFnSc2	židle
Magdaleny	Magdalena	k1gFnSc2	Magdalena
Jetelové	jetelový	k2eAgFnSc2d1	Jetelová
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
symbolem	symbol	k1gInSc7	symbol
povodně	povodeň	k1gFnSc2	povodeň
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
