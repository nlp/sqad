<s>
Kůň	kůň	k1gMnSc1	kůň
domácí	domácí	k1gMnSc1	domácí
(	(	kIx(	(
<g/>
Equus	Equus	k1gMnSc1	Equus
caballus	caballus	k1gMnSc1	caballus
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
pouze	pouze	k6eAd1	pouze
kůň	kůň	k1gMnSc1	kůň
je	být	k5eAaImIp3nS	být
domestikované	domestikovaný	k2eAgNnSc4d1	domestikované
zvíře	zvíře	k1gNnSc4	zvíře
patřící	patřící	k2eAgNnSc4d1	patřící
mezi	mezi	k7c4	mezi
lichokopytníky	lichokopytník	k1gMnPc4	lichokopytník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
koně	kůň	k1gMnPc1	kůň
používali	používat	k5eAaImAgMnP	používat
především	především	k6eAd1	především
k	k	k7c3	k
přepravě	přeprava	k1gFnSc3	přeprava
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
na	na	k7c6	na
nich	on	k3xPp3gMnPc6	on
jezdí	jezdit	k5eAaImIp3nP	jezdit
hlavně	hlavně	k9	hlavně
rekreačně	rekreačně	k6eAd1	rekreačně
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
kůň	kůň	k1gMnSc1	kůň
označuje	označovat	k5eAaImIp3nS	označovat
obecně	obecně	k6eAd1	obecně
jak	jak	k6eAd1	jak
samce	samec	k1gInSc2	samec
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
samici	samice	k1gFnSc4	samice
koně	kůň	k1gMnSc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
klisna	klisna	k1gFnSc1	klisna
nebo	nebo	k8xC	nebo
kobyla	kobyla	k1gFnSc1	kobyla
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
kobylou	kobyla	k1gFnSc7	kobyla
a	a	k8xC	a
klisnou	klisna	k1gFnSc7	klisna
je	být	k5eAaImIp3nS	být
rozdíl	rozdíl	k1gInSc1	rozdíl
takový	takový	k3xDgInSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
kobyla	kobyla	k1gFnSc1	kobyla
hříbě	hříbě	k1gNnSc4	hříbě
měla	mít	k5eAaImAgFnS	mít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
klisna	klisna	k1gFnSc1	klisna
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
v	v	k7c6	v
říji	říje	k1gFnSc6	říje
se	se	k3xPyFc4	se
říjí	říjet	k5eAaImIp3nS	říjet
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
říjná	říjný	k2eAgFnSc1d1	říjná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
říjnou	říjný	k2eAgFnSc4d1	říjná
klisnu	klisna	k1gFnSc4	klisna
hřebec	hřebec	k1gMnSc1	hřebec
reaguje	reagovat	k5eAaBmIp3nS	reagovat
flémováním	flémování	k1gNnSc7	flémování
<g/>
.	.	kIx.	.
</s>
<s>
Gravidní	gravidní	k2eAgFnSc1d1	gravidní
samice	samice	k1gFnSc1	samice
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
pojmem	pojem	k1gInSc7	pojem
březí	březí	k1gNnSc2	březí
<g/>
.	.	kIx.	.
</s>
<s>
Porod	porod	k1gInSc1	porod
je	být	k5eAaImIp3nS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
hřebením	hřebení	k1gNnSc7	hřebení
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
je	být	k5eAaImIp3nS	být
hřebec	hřebec	k1gMnSc1	hřebec
<g/>
,	,	kIx,	,
vykastrovaný	vykastrovaný	k2eAgMnSc1d1	vykastrovaný
pak	pak	k6eAd1	pak
valach	valach	k1gMnSc1	valach
<g/>
.	.	kIx.	.
</s>
<s>
Mládě	mládě	k1gNnSc1	mládě
koně	kůň	k1gMnSc2	kůň
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
hříbě	hříbě	k1gNnSc1	hříbě
<g/>
.	.	kIx.	.
</s>
<s>
Věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	se	k3xPyFc4	se
koňmi	kůň	k1gMnPc7	kůň
je	být	k5eAaImIp3nS	být
hipologie	hipologie	k1gFnSc1	hipologie
<g/>
.	.	kIx.	.
</s>
<s>
Předci	předek	k1gMnPc1	předek
dnešního	dnešní	k2eAgMnSc2d1	dnešní
koně	kůň	k1gMnSc2	kůň
jsou	být	k5eAaImIp3nP	být
zařazeni	zařadit	k5eAaPmNgMnP	zařadit
do	do	k7c2	do
období	období	k1gNnSc2	období
před	před	k7c7	před
60	[number]	k4	60
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Kůň	kůň	k1gMnSc1	kůň
se	se	k3xPyFc4	se
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
přecházel	přecházet	k5eAaImAgInS	přecházet
přes	přes	k7c4	přes
tzv.	tzv.	kA	tzv.
pevninské	pevninský	k2eAgInPc1d1	pevninský
mosty	most	k1gInPc4	most
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
několika	několik	k4yIc7	několik
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
kůň	kůň	k1gMnSc1	kůň
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
z	z	k7c2	z
neznámého	známý	k2eNgInSc2d1	neznámý
důvodu	důvod	k1gInSc2	důvod
zcela	zcela	k6eAd1	zcela
vyhynul	vyhynout	k5eAaPmAgInS	vyhynout
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
koně	kůň	k1gMnPc1	kůň
vrátili	vrátit	k5eAaPmAgMnP	vrátit
až	až	k6eAd1	až
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
osadníky	osadník	k1gMnPc7	osadník
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
;	;	kIx,	;
proto	proto	k8xC	proto
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
nejsou	být	k5eNaImIp3nP	být
koně	kůň	k1gMnPc1	kůň
divocí	divoký	k2eAgMnPc1d1	divoký
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
koně	kůň	k1gMnPc4	kůň
zdivočelí	zdivočelý	k2eAgMnPc1d1	zdivočelý
<g/>
.	.	kIx.	.
</s>
<s>
Dobu	doba	k1gFnSc4	doba
ledovou	ledový	k2eAgFnSc4d1	ledová
přežili	přežít	k5eAaPmAgMnP	přežít
dva	dva	k4xCgMnPc1	dva
předkové	předek	k1gMnPc1	předek
koně	kůň	k1gMnPc1	kůň
-	-	kIx~	-
kůň	kůň	k1gMnSc1	kůň
Převalského	převalský	k2eAgInSc2d1	převalský
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
přežívá	přežívat	k5eAaImIp3nS	přežívat
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
a	a	k8xC	a
Tarpan	tarpan	k1gMnSc1	tarpan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
lidmi	člověk	k1gMnPc7	člověk
vyhuben	vyhuben	k2eAgInSc1d1	vyhuben
<g/>
.	.	kIx.	.
</s>
<s>
Eohippus	Eohippus	k1gInSc1	Eohippus
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
Hyracotherium	Hyracotherium	k1gNnSc4	Hyracotherium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xS	jako
prakoník	prakoník	k1gMnSc1	prakoník
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
pralesích	prales	k1gInPc6	prales
starších	starý	k2eAgFnPc2d2	starší
třetihor	třetihory	k1gFnPc2	třetihory
-	-	kIx~	-
v	v	k7c6	v
eocénu	eocén	k1gInSc6	eocén
-	-	kIx~	-
asi	asi	k9	asi
před	před	k7c7	před
60	[number]	k4	60
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
končetinách	končetina	k1gFnPc6	končetina
měl	mít	k5eAaImAgInS	mít
pět	pět	k4xCc4	pět
prstů	prst	k1gInPc2	prst
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
chůzi	chůze	k1gFnSc3	chůze
byly	být	k5eAaImAgFnP	být
uzpůsobeny	uzpůsoben	k2eAgInPc1d1	uzpůsoben
jenom	jenom	k9	jenom
čtyři	čtyři	k4xCgMnPc1	čtyři
na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
končetinách	končetina	k1gFnPc6	končetina
a	a	k8xC	a
tři	tři	k4xCgInPc4	tři
prsty	prst	k1gInPc4	prst
na	na	k7c6	na
zadních	zadní	k2eAgFnPc6d1	zadní
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
malé	malý	k2eAgInPc4d1	malý
a	a	k8xC	a
ostré	ostrý	k2eAgInPc4d1	ostrý
zuby	zub	k1gInPc4	zub
<g/>
;	;	kIx,	;
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgInS	živit
listím	listí	k1gNnSc7	listí
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
velký	velký	k2eAgInSc1d1	velký
asi	asi	k9	asi
jako	jako	k8xC	jako
liška	liška	k1gFnSc1	liška
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
předek	předek	k1gInSc1	předek
koně	kůň	k1gMnSc4	kůň
žil	žít	k5eAaImAgInS	žít
ve	v	k7c6	v
stepích	step	k1gFnPc6	step
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
nohách	noha	k1gFnPc6	noha
měl	mít	k5eAaImAgInS	mít
jen	jen	k6eAd1	jen
čtyři	čtyři	k4xCgInPc4	čtyři
prsty	prst	k1gInPc4	prst
<g/>
,	,	kIx,	,
na	na	k7c4	na
zadních	zadní	k2eAgNnPc2d1	zadní
tři	tři	k4xCgNnPc4	tři
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
tyto	tento	k3xDgInPc1	tento
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
Mesohippus	Mesohippus	k1gInSc1	Mesohippus
je	být	k5eAaImIp3nS	být
starší	starý	k2eAgMnSc1d2	starší
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
jejich	jejich	k3xOp3gMnSc1	jejich
předchůdce	předchůdce	k1gMnSc1	předchůdce
Eohippus	Eohippus	k1gMnSc1	Eohippus
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
zuby	zub	k1gInPc1	zub
byly	být	k5eAaImAgInP	být
již	již	k6eAd1	již
také	také	k9	také
lépe	dobře	k6eAd2	dobře
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
<g/>
,	,	kIx,	,
mohli	moct	k5eAaImAgMnP	moct
proto	proto	k8xC	proto
přijímat	přijímat	k5eAaImF	přijímat
více	hodně	k6eAd2	hodně
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
nohou	noha	k1gFnPc6	noha
měli	mít	k5eAaImAgMnP	mít
tři	tři	k4xCgInPc4	tři
prsty	prst	k1gInPc4	prst
<g/>
.	.	kIx.	.
</s>
<s>
Mesohippus	Mesohippus	k1gMnSc1	Mesohippus
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
oligocénu	oligocén	k1gInSc6	oligocén
<g/>
,	,	kIx,	,
Miohippus	Miohippus	k1gInSc1	Miohippus
v	v	k7c6	v
miocénu	miocén	k1gInSc6	miocén
<g/>
.	.	kIx.	.
</s>
<s>
Parahippus	Parahippus	k1gMnSc1	Parahippus
se	se	k3xPyFc4	se
na	na	k7c6	na
tvrdém	tvrdý	k2eAgInSc6d1	tvrdý
povrchu	povrch	k1gInSc6	povrch
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
pouze	pouze	k6eAd1	pouze
po	po	k7c6	po
prostředním	prostřední	k2eAgInSc6d1	prostřední
prstu	prst	k1gInSc6	prst
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
dva	dva	k4xCgMnPc1	dva
používal	používat	k5eAaImAgInS	používat
jen	jen	k9	jen
v	v	k7c6	v
měkkém	měkký	k2eAgInSc6d1	měkký
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
<s>
Stál	stát	k5eAaImAgMnS	stát
u	u	k7c2	u
vzniku	vznik	k1gInSc2	vznik
několika	několik	k4yIc2	několik
různých	různý	k2eAgFnPc2d1	různá
odlišných	odlišný	k2eAgFnPc2d1	odlišná
linií	linie	k1gFnPc2	linie
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
je	být	k5eAaImIp3nS	být
linie	linie	k1gFnSc1	linie
Pliohippa	Pliohippa	k1gFnSc1	Pliohippa
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
z	z	k7c2	z
tohoto	tento	k3xDgMnSc2	tento
předchůdce	předchůdce	k1gMnSc2	předchůdce
se	se	k3xPyFc4	se
před	před	k7c4	před
milionem	milion	k4xCgInSc7	milion
let	léto	k1gNnPc2	léto
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
Equus	Equus	k1gMnSc1	Equus
caballus	caballus	k1gMnSc1	caballus
-	-	kIx~	-
dnešní	dnešní	k2eAgMnSc1d1	dnešní
kůň	kůň	k1gMnSc1	kůň
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
důkazy	důkaz	k1gInPc4	důkaz
o	o	k7c4	o
domestikaci	domestikace	k1gFnSc4	domestikace
koně	kůň	k1gMnSc2	kůň
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Centrální	centrální	k2eAgFnSc2d1	centrální
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
asi	asi	k9	asi
3000	[number]	k4	3000
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Domestikovaní	domestikovaný	k2eAgMnPc1d1	domestikovaný
koně	kůň	k1gMnPc1	kůň
se	se	k3xPyFc4	se
chovali	chovat	k5eAaImAgMnP	chovat
zprvu	zprvu	k6eAd1	zprvu
pro	pro	k7c4	pro
mléko	mléko	k1gNnSc4	mléko
a	a	k8xC	a
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
nákladů	náklad	k1gInPc2	náklad
i	i	k9	i
přepravu	přeprava	k1gFnSc4	přeprava
osobní	osobní	k2eAgFnSc4d1	osobní
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
zachovaly	zachovat	k5eAaPmAgInP	zachovat
primitivní	primitivní	k2eAgInSc1d1	primitivní
udidla	udidlo	k1gNnSc2	udidlo
a	a	k8xC	a
sedla	sedlo	k1gNnSc2	sedlo
<g/>
.	.	kIx.	.
</s>
<s>
Horké	Horké	k2eAgNnSc1d1	Horké
podnebí	podnebí	k1gNnSc1	podnebí
dávalo	dávat	k5eAaImAgNnS	dávat
podnět	podnět	k1gInSc4	podnět
k	k	k7c3	k
vývoji	vývoj	k1gInSc3	vývoj
lehčích	lehký	k2eAgMnPc2d2	lehčí
<g/>
,	,	kIx,	,
vyšších	vysoký	k2eAgMnPc2d2	vyšší
koní	kůň	k1gMnPc2	kůň
s	s	k7c7	s
užšími	úzký	k2eAgNnPc7d2	užší
kopyty	kopyto	k1gNnPc7	kopyto
(	(	kIx(	(
<g/>
typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
achaltekinský	achaltekinský	k2eAgMnSc1d1	achaltekinský
kůň	kůň	k1gMnSc1	kůň
a	a	k8xC	a
arabský	arabský	k2eAgMnSc1d1	arabský
plnokrevník	plnokrevník	k1gMnSc1	plnokrevník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
horských	horský	k2eAgFnPc6d1	horská
či	či	k8xC	či
chladnějších	chladný	k2eAgFnPc6d2	chladnější
oblastech	oblast	k1gFnPc6	oblast
žili	žít	k5eAaImAgMnP	žít
koně	kůň	k1gMnPc1	kůň
menší	malý	k2eAgFnSc2d2	menší
a	a	k8xC	a
robustnější	robustní	k2eAgFnSc2d2	robustnější
(	(	kIx(	(
<g/>
především	především	k9	především
ponyové	pony	k1gMnPc1	pony
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
suchých	suchý	k2eAgFnPc6d1	suchá
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
vyvíjeli	vyvíjet	k5eAaImAgMnP	vyvíjet
lehčí	lehký	k2eAgMnPc1d2	lehčí
koně	kůň	k1gMnPc1	kůň
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vlhčích	vlhký	k2eAgFnPc6d2	vlhčí
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
bohatou	bohatý	k2eAgFnSc7d1	bohatá
pastvou	pastva	k1gFnSc7	pastva
koně	kůň	k1gMnPc4	kůň
těžcí	těžký	k2eAgMnPc1d1	těžký
(	(	kIx(	(
<g/>
chladnokrevní	chladnokrevný	k2eAgMnPc1d1	chladnokrevný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Koně	kůň	k1gMnPc1	kůň
byli	být	k5eAaImAgMnP	být
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
vždy	vždy	k6eAd1	vždy
velmi	velmi	k6eAd1	velmi
důležití	důležitý	k2eAgMnPc1d1	důležitý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pravěku	pravěk	k1gInSc6	pravěk
mu	on	k3xPp3gMnSc3	on
sloužili	sloužit	k5eAaImAgMnP	sloužit
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
potrava	potrava	k1gFnSc1	potrava
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
člověk	člověk	k1gMnSc1	člověk
koně	kůň	k1gMnSc4	kůň
domestikoval	domestikovat	k5eAaBmAgMnS	domestikovat
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
ho	on	k3xPp3gMnSc4	on
využívat	využívat	k5eAaPmF	využívat
k	k	k7c3	k
tahu	tah	k1gInSc3	tah
a	a	k8xC	a
jízdě	jízda	k1gFnSc3	jízda
na	na	k7c6	na
něm.	něm.	k?	něm.
Koně	kůň	k1gMnPc1	kůň
sloužili	sloužit	k5eAaImAgMnP	sloužit
v	v	k7c6	v
dopravě	doprava	k1gFnSc6	doprava
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
,	,	kIx,	,
nezbytní	zbytnět	k5eNaPmIp3nP	zbytnět
byli	být	k5eAaImAgMnP	být
ve	v	k7c6	v
vojenství	vojenství	k1gNnSc6	vojenství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poskytovali	poskytovat	k5eAaImAgMnP	poskytovat
lidem	lid	k1gInSc7	lid
i	i	k8xC	i
zábavu	zábav	k1gInSc2	zábav
při	při	k7c6	při
lovech	lov	k1gInPc6	lov
<g/>
,	,	kIx,	,
různých	různý	k2eAgFnPc6d1	různá
soutěžích	soutěž	k1gFnPc6	soutěž
a	a	k8xC	a
dostizích	dostih	k1gInPc6	dostih
<g/>
.	.	kIx.	.
</s>
<s>
Koňské	koňský	k2eAgFnPc1d1	koňská
žíně	žíně	k1gFnPc1	žíně
byly	být	k5eAaImAgFnP	být
využívány	využívat	k5eAaImNgFnP	využívat
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
matrací	matrace	k1gFnPc2	matrace
<g/>
,	,	kIx,	,
kartáčů	kartáč	k1gInPc2	kartáč
<g/>
,	,	kIx,	,
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
ve	v	k7c6	v
vyspělých	vyspělý	k2eAgFnPc6d1	vyspělá
zemích	zem	k1gFnPc6	zem
využívá	využívat	k5eAaImIp3nS	využívat
kůň	kůň	k1gMnSc1	kůň
převážně	převážně	k6eAd1	převážně
pro	pro	k7c4	pro
zábavu	zábava	k1gFnSc4	zábava
a	a	k8xC	a
v	v	k7c6	v
podstatně	podstatně	k6eAd1	podstatně
menší	malý	k2eAgFnSc3d2	menší
míře	míra	k1gFnSc3	míra
než	než	k8xS	než
tomu	ten	k3xDgNnSc3	ten
bývalo	bývat	k5eAaImAgNnS	bývat
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
i	i	k9	i
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
<g/>
:	:	kIx,	:
například	například	k6eAd1	například
pro	pro	k7c4	pro
stahování	stahování	k1gNnSc4	stahování
dřeva	dřevo	k1gNnSc2	dřevo
v	v	k7c6	v
lesích	les	k1gInPc6	les
(	(	kIx(	(
<g/>
zvlášť	zvlášť	k6eAd1	zvlášť
při	při	k7c6	při
kalamitních	kalamitní	k2eAgFnPc6d1	kalamitní
situacích	situace	k1gFnPc6	situace
<g/>
,	,	kIx,	,
v	v	k7c6	v
obtížném	obtížný	k2eAgInSc6d1	obtížný
terénu	terén	k1gInSc6	terén
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
provoz	provoz	k1gInSc4	provoz
turistických	turistický	k2eAgInPc2d1	turistický
povozů	povoz	k1gInPc2	povoz
(	(	kIx(	(
<g/>
fiakr	fiakr	k1gInSc1	fiakr
<g/>
,	,	kIx,	,
<g/>
drožka	drožka	k1gFnSc1	drožka
<g/>
)	)	kIx)	)
v	v	k7c6	v
centrech	centrum	k1gNnPc6	centrum
historických	historický	k2eAgNnPc6d1	historické
městech	město	k1gNnPc6	město
nebo	nebo	k8xC	nebo
při	při	k7c6	při
slavnostních	slavnostní	k2eAgFnPc6d1	slavnostní
příležitostech	příležitost	k1gFnPc6	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
kůň	kůň	k1gMnSc1	kůň
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
stále	stále	k6eAd1	stále
běžným	běžný	k2eAgNnSc7d1	běžné
tažným	tažný	k2eAgNnSc7d1	tažné
zvířetem	zvíře	k1gNnSc7	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Setkat	setkat	k5eAaPmF	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
lze	lze	k6eAd1	lze
i	i	k9	i
v	v	k7c6	v
cirkusech	cirkus	k1gInPc6	cirkus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
koně	kůň	k1gMnPc1	kůň
domácí	domácí	k2eAgMnPc1d1	domácí
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
v	v	k7c6	v
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
a	a	k8xC	a
chová	chovat	k5eAaImIp3nS	chovat
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Zoo	zoo	k1gNnSc1	zoo
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Zoo	zoo	k1gNnPc7	zoo
Liberec	Liberec	k1gInSc1	Liberec
či	či	k8xC	či
Zoo	zoo	k1gFnSc1	zoo
Tábor	Tábor	k1gInSc1	Tábor
<g/>
.	.	kIx.	.
</s>
<s>
Koně	kůň	k1gMnPc1	kůň
se	se	k3xPyFc4	se
využívali	využívat	k5eAaImAgMnP	využívat
a	a	k8xC	a
využívají	využívat	k5eAaImIp3nP	využívat
i	i	k9	i
ve	v	k7c6	v
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
-	-	kIx~	-
jednak	jednak	k8xC	jednak
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
očkovacích	očkovací	k2eAgFnPc2d1	očkovací
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
sér	sérum	k1gNnPc2	sérum
<g/>
.	.	kIx.	.
</s>
<s>
Kůň	kůň	k1gMnSc1	kůň
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
v	v	k7c6	v
hiporehabilitaci	hiporehabilitace	k1gFnSc6	hiporehabilitace
<g/>
,	,	kIx,	,
léčebném	léčebný	k2eAgInSc6d1	léčebný
a	a	k8xC	a
pedagogickém	pedagogický	k2eAgNnSc6d1	pedagogické
využití	využití	k1gNnSc6	využití
v	v	k7c6	v
oborech	obor	k1gInPc6	obor
hipoterapie	hipoterapie	k1gFnSc2	hipoterapie
<g/>
,	,	kIx,	,
psychoterapie	psychoterapie	k1gFnSc1	psychoterapie
pomocí	pomocí	k7c2	pomocí
koní	kůň	k1gMnPc2	kůň
<g/>
,	,	kIx,	,
aktivity	aktivita	k1gFnPc1	aktivita
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
koní	kůň	k1gMnPc2	kůň
a	a	k8xC	a
parajezdectví	parajezdectví	k1gNnSc2	parajezdectví
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
využití	využití	k1gNnSc1	využití
koní	kůň	k1gMnPc2	kůň
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
ve	v	k7c6	v
sportu	sport	k1gInSc6	sport
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yQnSc7	co
dál	daleko	k6eAd2	daleko
víc	hodně	k6eAd2	hodně
roste	růst	k5eAaImIp3nS	růst
sportovní	sportovní	k2eAgNnSc1d1	sportovní
využití	využití	k1gNnSc1	využití
koní	kůň	k1gMnPc2	kůň
-	-	kIx~	-
existuje	existovat	k5eAaImIp3nS	existovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
jezdeckých	jezdecký	k2eAgFnPc2d1	jezdecká
i	i	k8xC	i
vozatajských	vozatajský	k2eAgFnPc2d1	vozatajská
disciplín	disciplína	k1gFnPc2	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějšími	důležitý	k2eAgFnPc7d3	nejdůležitější
soutěžemi	soutěž	k1gFnPc7	soutěž
v	v	k7c6	v
jezdeckém	jezdecký	k2eAgInSc6d1	jezdecký
sportu	sport	k1gInSc6	sport
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Drezura	drezura	k1gFnSc1	drezura
je	být	k5eAaImIp3nS	být
sportovní	sportovní	k2eAgFnSc1d1	sportovní
disciplina	disciplina	k1gFnSc1	disciplina
postavená	postavený	k2eAgFnSc1d1	postavená
na	na	k7c6	na
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
harmonii	harmonie	k1gFnSc6	harmonie
a	a	k8xC	a
souhře	souhra	k1gFnSc6	souhra
koně	kůň	k1gMnSc2	kůň
a	a	k8xC	a
jezdce	jezdec	k1gMnSc2	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
drezury	drezura	k1gFnSc2	drezura
je	být	k5eAaImIp3nS	být
co	co	k9	co
nejvíce	nejvíce	k6eAd1	nejvíce
zdokonalit	zdokonalit	k5eAaPmF	zdokonalit
pohyb	pohyb	k1gInSc4	pohyb
koně	kůň	k1gMnSc2	kůň
<g/>
,	,	kIx,	,
připravit	připravit	k5eAaPmF	připravit
(	(	kIx(	(
<g/>
přijezdit	přijezdit	k5eAaPmF	přijezdit
<g/>
)	)	kIx)	)
koně	kůň	k1gMnSc4	kůň
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
nést	nést	k5eAaImF	nést
svého	svůj	k3xOyFgMnSc4	svůj
jezdce	jezdec	k1gMnSc4	jezdec
lehce	lehko	k6eAd1	lehko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
pružný	pružný	k2eAgInSc1d1	pružný
<g/>
,	,	kIx,	,
vyvážený	vyvážený	k2eAgInSc1d1	vyvážený
<g/>
,	,	kIx,	,
správně	správně	k6eAd1	správně
osvalený	osvalený	k2eAgMnSc1d1	osvalený
a	a	k8xC	a
reagoval	reagovat	k5eAaBmAgMnS	reagovat
na	na	k7c4	na
i	i	k8xC	i
na	na	k7c4	na
ty	ten	k3xDgFnPc4	ten
nejjemnější	jemný	k2eAgFnPc4d3	nejjemnější
pobídky	pobídka	k1gFnPc4	pobídka
svého	svůj	k3xOyFgMnSc2	svůj
jezdce	jezdec	k1gMnSc2	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
závody	závod	k1gInPc4	závod
jsou	být	k5eAaImIp3nP	být
vypsány	vypsán	k2eAgFnPc4d1	vypsána
drezurní	drezurní	k2eAgFnPc4d1	drezurní
úlohy	úloha	k1gFnPc4	úloha
rozdělené	rozdělený	k2eAgFnPc4d1	rozdělená
do	do	k7c2	do
několika	několik	k4yIc2	několik
stupňů	stupeň	k1gInPc2	stupeň
obtížnosti	obtížnost	k1gFnSc2	obtížnost
<g/>
.	.	kIx.	.
</s>
<s>
Drezurní	drezurní	k2eAgFnSc1d1	drezurní
úloha	úloha	k1gFnSc1	úloha
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
cviků	cvik	k1gInPc2	cvik
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
musí	muset	k5eAaImIp3nS	muset
jezdec	jezdec	k1gMnSc1	jezdec
s	s	k7c7	s
koněm	kůň	k1gMnSc7	kůň
postupně	postupně	k6eAd1	postupně
udělat	udělat	k5eAaPmF	udělat
co	co	k9	co
nejpřesněji	přesně	k6eAd3	přesně
v	v	k7c6	v
určeném	určený	k2eAgNnSc6d1	určené
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
olympijská	olympijský	k2eAgFnSc1d1	olympijská
sportovní	sportovní	k2eAgFnSc1d1	sportovní
disciplina	disciplina	k1gFnSc1	disciplina
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejtěžších	těžký	k2eAgInPc2d3	nejtěžší
cviků	cvik	k1gInPc2	cvik
je	být	k5eAaImIp3nS	být
piaffa	piaff	k1gMnSc4	piaff
<g/>
.	.	kIx.	.
</s>
<s>
Parkur	parkur	k1gInSc1	parkur
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
jezdecké	jezdecký	k2eAgNnSc1d1	jezdecké
odvětví	odvětví	k1gNnSc1	odvětví
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
kůň	kůň	k1gMnSc1	kůň
překonává	překonávat	k5eAaImIp3nS	překonávat
překážky	překážka	k1gFnPc4	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
na	na	k7c4	na
překážky	překážka	k1gFnPc4	překážka
výškové	výškový	k2eAgFnSc2d1	výšková
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kolmý	kolmý	k2eAgInSc1d1	kolmý
skok	skok	k1gInSc1	skok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šířkové	šířkový	k2eAgInPc1d1	šířkový
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vodní	vodní	k2eAgInSc1d1	vodní
příkop	příkop	k1gInSc1	příkop
<g/>
)	)	kIx)	)
a	a	k8xC	a
výškově-šířkové	výškově-šířková	k1gFnSc2	výškově-šířková
(	(	kIx(	(
<g/>
např.	např.	kA	např.
oxer	oxer	k1gInSc1	oxer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
se	se	k3xPyFc4	se
také	také	k9	také
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
úrovní	úroveň	k1gFnPc2	úroveň
nejjednodušší	jednoduchý	k2eAgFnSc2d3	nejjednodušší
ZM	ZM	kA	ZM
<g/>
,	,	kIx,	,
Z	Z	kA	Z
<g/>
,	,	kIx,	,
ZL	ZL	kA	ZL
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
,	,	kIx,	,
SM	SM	kA	SM
<g/>
,	,	kIx,	,
S	s	k7c7	s
<g/>
,	,	kIx,	,
ST	St	kA	St
<g/>
,	,	kIx,	,
T	T	kA	T
<g/>
,	,	kIx,	,
nejtěžší	těžký	k2eAgMnSc1d3	nejtěžší
TT	TT	kA	TT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
sportovní	sportovní	k2eAgFnSc4d1	sportovní
disciplínu	disciplína	k1gFnSc4	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Soutěž	soutěž	k1gFnSc1	soutěž
jezdecké	jezdecký	k2eAgFnSc2d1	jezdecká
všestrannosti	všestrannost	k1gFnSc2	všestrannost
neboli	neboli	k8xC	neboli
military	militara	k1gFnSc2	militara
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
třídenní	třídenní	k2eAgFnSc4d1	třídenní
soutěž	soutěž	k1gFnSc4	soutěž
skládající	skládající	k2eAgFnSc2d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
drezury	drezura	k1gFnSc2	drezura
<g/>
,	,	kIx,	,
terénní	terénní	k2eAgFnSc2d1	terénní
jízdy	jízda	k1gFnSc2	jízda
a	a	k8xC	a
parkuru	parkur	k1gInSc2	parkur
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
olympijská	olympijský	k2eAgFnSc1d1	olympijská
sportovní	sportovní	k2eAgFnSc1d1	sportovní
disciplina	disciplina	k1gFnSc1	disciplina
a	a	k8xC	a
pro	pro	k7c4	pro
koně	kůň	k1gMnSc4	kůň
i	i	k8xC	i
jezdce	jezdec	k1gMnSc4	jezdec
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
náročná	náročný	k2eAgFnSc1d1	náročná
<g/>
.	.	kIx.	.
</s>
<s>
Dostihy	dostih	k1gInPc1	dostih
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
rovinové	rovinový	k2eAgInPc4d1	rovinový
dostihy	dostih	k1gInPc4	dostih
<g/>
,	,	kIx,	,
překážkové	překážkový	k2eAgInPc1d1	překážkový
dostihy	dostih	k1gInPc1	dostih
a	a	k8xC	a
klusácké	klusácký	k2eAgInPc1d1	klusácký
dostihy	dostih	k1gInPc1	dostih
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
podstatě	podstata	k1gFnSc6	podstata
se	se	k3xPyFc4	se
u	u	k7c2	u
dostihů	dostih	k1gInPc2	dostih
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
sport	sport	k1gInSc4	sport
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
výkonnostní	výkonnostní	k2eAgFnSc4d1	výkonnostní
zkoušku	zkouška	k1gFnSc4	zkouška
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
výsledkem	výsledek	k1gInSc7	výsledek
na	na	k7c6	na
konci	konec	k1gInSc6	konec
kariéry	kariéra	k1gFnSc2	kariéra
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
generální	generální	k2eAgInSc1d1	generální
handicap	handicap	k1gInSc1	handicap
-	-	kIx~	-
GH	GH	kA	GH
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Dostihy	dostih	k1gInPc1	dostih
nejsou	být	k5eNaImIp3nP	být
a	a	k8xC	a
nebyly	být	k5eNaImAgFnP	být
nikdy	nikdy	k6eAd1	nikdy
řízeny	řídit	k5eAaImNgInP	řídit
tělovýchovnými	tělovýchovný	k2eAgFnPc7d1	Tělovýchovná
organizacemi	organizace	k1gFnPc7	organizace
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nP	řídit
je	on	k3xPp3gInPc4	on
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
např.	např.	kA	např.
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Jockey	jockey	k1gMnSc1	jockey
clubu	club	k1gInSc2	club
<g/>
.	.	kIx.	.
</s>
<s>
Výše	výše	k1gFnSc1	výše
GH	GH	kA	GH
určuje	určovat	k5eAaImIp3nS	určovat
vlastně	vlastně	k9	vlastně
kvalitu	kvalita	k1gFnSc4	kvalita
koně	kůň	k1gMnSc2	kůň
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
jeho	on	k3xPp3gNnSc2	on
potomstva	potomstvo	k1gNnSc2	potomstvo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rovinných	rovinný	k2eAgInPc6d1	rovinný
dostizích	dostih	k1gInPc6	dostih
běží	běžet	k5eAaImIp3nS	běžet
kůň	kůň	k1gMnSc1	kůň
po	po	k7c6	po
dráze	dráha	k1gFnSc6	dráha
bez	bez	k7c2	bez
překážek	překážka	k1gFnPc2	překážka
(	(	kIx(	(
<g/>
nejznámější	známý	k2eAgInPc1d3	nejznámější
jsou	být	k5eAaImIp3nP	být
Derby	derby	k1gNnSc7	derby
a	a	k8xC	a
St.	st.	kA	st.
Leger	Leger	k1gInSc1	Leger
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
překážkových	překážkový	k2eAgInPc6d1	překážkový
dostizích	dostih	k1gInPc6	dostih
kůň	kůň	k1gMnSc1	kůň
překonává	překonávat	k5eAaImIp3nS	překonávat
proutěné	proutěný	k2eAgFnPc4d1	proutěná
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pevné	pevný	k2eAgFnPc4d1	pevná
překážky	překážka	k1gFnPc4	překážka
<g/>
,	,	kIx,	,
příkopy	příkop	k1gInPc4	příkop
či	či	k8xC	či
vodní	vodní	k2eAgInPc4d1	vodní
skoky	skok	k1gInPc4	skok
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
Grand	grand	k1gMnSc1	grand
National	National	k1gMnSc1	National
(	(	kIx(	(
<g/>
Velká	velká	k1gFnSc1	velká
národní	národní	k2eAgFnSc1d1	národní
<g/>
)	)	kIx)	)
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
pardubická	pardubický	k2eAgFnSc1d1	pardubická
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
klusáckých	klusácký	k2eAgInPc6d1	klusácký
dostizích	dostih	k1gInPc6	dostih
kůň	kůň	k1gMnSc1	kůň
táhne	táhnout	k5eAaImIp3nS	táhnout
jezdce	jezdec	k1gMnSc4	jezdec
na	na	k7c6	na
lehkém	lehký	k2eAgInSc6d1	lehký
vozíku	vozík	k1gInSc6	vozík
zvaném	zvaný	k2eAgInSc6d1	zvaný
sulka	sulka	k1gFnSc1	sulka
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
obvyklé	obvyklý	k2eAgInPc1d1	obvyklý
jsou	být	k5eAaImIp3nP	být
klusácké	klusácký	k2eAgInPc1d1	klusácký
dostihy	dostih	k1gInPc1	dostih
pod	pod	k7c7	pod
sedlem	sedlo	k1gNnSc7	sedlo
<g/>
.	.	kIx.	.
</s>
<s>
Kůň	kůň	k1gMnSc1	kůň
zde	zde	k6eAd1	zde
musí	muset	k5eAaImIp3nS	muset
pouze	pouze	k6eAd1	pouze
klusat	klusat	k5eAaImF	klusat
(	(	kIx(	(
<g/>
nesmí	smět	k5eNaImIp3nS	smět
cválat	cválat	k5eAaImF	cválat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klusácké	klusácký	k2eAgInPc1d1	klusácký
dostihy	dostih	k1gInPc1	dostih
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Cross-country	Crossountr	k1gInPc4	Cross-countr
je	být	k5eAaImIp3nS	být
jízda	jízda	k1gFnSc1	jízda
terénem	terén	k1gInSc7	terén
<g/>
.	.	kIx.	.
</s>
<s>
Kůň	kůň	k1gMnSc1	kůň
musí	muset	k5eAaImIp3nS	muset
skákat	skákat	k5eAaImF	skákat
překážky	překážka	k1gFnPc4	překážka
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
mít	mít	k5eAaImF	mít
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
další	další	k2eAgNnPc4d1	další
sportovní	sportovní	k2eAgNnPc4d1	sportovní
odvětví	odvětví	k1gNnPc4	odvětví
a	a	k8xC	a
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
soutěže	soutěž	k1gFnPc4	soutěž
provozované	provozovaný	k2eAgNnSc1d1	provozované
s	s	k7c7	s
koňmi	kůň	k1gMnPc7	kůň
<g/>
:	:	kIx,	:
Kočárové	kočárový	k2eAgFnSc2d1	Kočárová
soutěže	soutěž	k1gFnSc2	soutěž
resp.	resp.	kA	resp.
vozatajské	vozatajský	k2eAgFnSc2d1	vozatajská
soutěže	soutěž	k1gFnSc2	soutěž
jsou	být	k5eAaImIp3nP	být
specializovaným	specializovaný	k2eAgInSc7d1	specializovaný
sportovní	sportovní	k2eAgFnSc4d1	sportovní
odvětvím	odvětví	k1gNnSc7	odvětví
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
soutěžící	soutěžící	k1gMnPc1	soutěžící
-	-	kIx~	-
vozatajové	vozataj	k1gMnPc1	vozataj
-	-	kIx~	-
nejedou	jet	k5eNaImIp3nP	jet
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vezou	vézt	k5eAaImIp3nP	vézt
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
na	na	k7c6	na
povozu	povoz	k1gInSc6	povoz
<g/>
)	)	kIx)	)
na	na	k7c6	na
speciálním	speciální	k2eAgNnSc6d1	speciální
vozidle	vozidlo	k1gNnSc6	vozidlo
taženém	tažený	k2eAgNnSc6d1	tažené
koňmi	kůň	k1gMnPc7	kůň
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
jedou	jet	k5eAaImIp3nP	jet
v	v	k7c6	v
kočáru	kočár	k1gInSc6	kočár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
soutěže	soutěž	k1gFnPc4	soutěž
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
testuje	testovat	k5eAaImIp3nS	testovat
vozatajská	vozatajský	k2eAgFnSc1d1	vozatajská
zručnost	zručnost	k1gFnSc1	zručnost
na	na	k7c6	na
vymezené	vymezený	k2eAgFnSc6d1	vymezená
dráze	dráha	k1gFnSc6	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Vůz	vůz	k1gInSc1	vůz
musí	muset	k5eAaImIp3nS	muset
překonávat	překonávat	k5eAaImF	překonávat
různé	různý	k2eAgFnPc4d1	různá
umělé	umělý	k2eAgFnPc4d1	umělá
překážky	překážka	k1gFnPc4	překážka
a	a	k8xC	a
speciální	speciální	k2eAgFnPc4d1	speciální
vozatajské	vozatajský	k2eAgFnPc4d1	vozatajská
branky	branka	k1gFnPc4	branka
<g/>
.	.	kIx.	.
</s>
<s>
Koňské	koňský	k2eAgNnSc1d1	koňské
pólo	pólo	k1gNnSc1	pólo
je	být	k5eAaImIp3nS	být
kolektivní	kolektivní	k2eAgFnSc1d1	kolektivní
sportovní	sportovní	k2eAgFnSc1d1	sportovní
hra	hra	k1gFnSc1	hra
s	s	k7c7	s
míčkem	míček	k1gInSc7	míček
na	na	k7c6	na
branky	branka	k1gFnSc2	branka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
hrají	hrát	k5eAaImIp3nP	hrát
dvě	dva	k4xCgFnPc1	dva
skupiny	skupina	k1gFnPc1	skupina
jezdců	jezdec	k1gMnPc2	jezdec
na	na	k7c6	na
koních	kůň	k1gMnPc6	kůň
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
západoevropských	západoevropský	k2eAgFnPc6d1	západoevropská
zemích	zem	k1gFnPc6	zem
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
<g/>
.	.	kIx.	.
</s>
<s>
Westernový	westernový	k2eAgInSc1d1	westernový
jezdecký	jezdecký	k2eAgInSc1d1	jezdecký
sport	sport	k1gInSc1	sport
se	se	k3xPyFc4	se
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
na	na	k7c6	na
dobytkářských	dobytkářský	k2eAgFnPc6d1	dobytkářská
rančích	ranč	k1gFnPc6	ranč
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Klade	klást	k5eAaImIp3nS	klást
důraz	důraz	k1gInSc4	důraz
především	především	k9	především
na	na	k7c4	na
všestrannost	všestrannost	k1gFnSc4	všestrannost
koně	kůň	k1gMnSc2	kůň
i	i	k8xC	i
jezdce	jezdec	k1gMnSc2	jezdec
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vyšlechtěna	vyšlechtit	k5eAaPmNgFnS	vyšlechtit
i	i	k9	i
některá	některý	k3yIgNnPc4	některý
plemena	plemeno	k1gNnPc4	plemeno
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
hodí	hodit	k5eAaPmIp3nP	hodit
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
quarter	quarter	k1gInSc1	quarter
horse	horse	k1gFnSc2	horse
<g/>
,	,	kIx,	,
american	american	k1gMnSc1	american
paint	paint	k1gMnSc1	paint
horse	horse	k6eAd1	horse
a	a	k8xC	a
appaloosa	appaloosa	k1gFnSc1	appaloosa
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
disciplíny	disciplína	k1gFnPc1	disciplína
se	se	k3xPyFc4	se
odvíjejí	odvíjet	k5eAaImIp3nP	odvíjet
od	od	k7c2	od
každodenní	každodenní	k2eAgFnSc2d1	každodenní
práce	práce	k1gFnSc2	práce
kovbojů	kovboj	k1gMnPc2	kovboj
<g/>
.	.	kIx.	.
trail	trail	k1gMnSc1	trail
-	-	kIx~	-
trasa	trasa	k1gFnSc1	trasa
s	s	k7c7	s
překážkami	překážka	k1gFnPc7	překážka
jako	jako	k8xC	jako
můstek	můstek	k1gInSc1	můstek
<g/>
,	,	kIx,	,
kavalety	kavalet	k1gInPc1	kavalet
nebo	nebo	k8xC	nebo
branka	branka	k1gFnSc1	branka
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
simulací	simulace	k1gFnSc7	simulace
obtížného	obtížný	k2eAgInSc2d1	obtížný
terénu	terén	k1gInSc2	terén
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
překonány	překonat	k5eAaPmNgInP	překonat
s	s	k7c7	s
rozvahou	rozvaha	k1gFnSc7	rozvaha
<g/>
,	,	kIx,	,
v	v	k7c6	v
určeném	určený	k2eAgNnSc6d1	určené
pořadí	pořadí	k1gNnSc6	pořadí
a	a	k8xC	a
chodu	chod	k1gInSc6	chod
cutting	cutting	k1gInSc1	cutting
-	-	kIx~	-
dobytkářská	dobytkářský	k2eAgFnSc1d1	dobytkářská
disciplína	disciplína	k1gFnSc1	disciplína
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
musí	muset	k5eAaImIp3nS	muset
jezdec	jezdec	k1gMnSc1	jezdec
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
jezdců	jezdec	k1gMnPc2	jezdec
oddělit	oddělit	k5eAaPmF	oddělit
jednoho	jeden	k4xCgMnSc2	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
předem	předem	k6eAd1	předem
určených	určený	k2eAgMnPc2d1	určený
býčků	býček	k1gMnPc2	býček
od	od	k7c2	od
stáda	stádo	k1gNnSc2	stádo
a	a	k8xC	a
označit	označit	k5eAaPmF	označit
je	on	k3xPp3gMnPc4	on
v	v	k7c6	v
co	co	k9	co
nejkratším	krátký	k2eAgInSc6d3	nejkratší
čase	čas	k1gInSc6	čas
barrel	barret	k5eAaImAgInS	barret
racing	racing	k1gInSc1	racing
-	-	kIx~	-
rychlostní	rychlostní	k2eAgFnSc1d1	rychlostní
disciplína	disciplína	k1gFnSc1	disciplína
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
<g />
.	.	kIx.	.
</s>
<s>
o	o	k7c4	o
projetí	projetí	k1gNnSc4	projetí
kolem	kolem	k7c2	kolem
tří	tři	k4xCgInPc2	tři
barelů	barel	k1gInPc2	barel
<g/>
,	,	kIx,	,
rozestavených	rozestavený	k2eAgFnPc2d1	rozestavená
do	do	k7c2	do
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ani	ani	k8xC	ani
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
schozen	schozen	k2eAgMnSc1d1	schozen
pole	pole	k1gNnSc4	pole
banding	banding	k1gInSc1	banding
-	-	kIx~	-
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
projetí	projetí	k1gNnSc4	projetí
slalomu	slalom	k1gInSc2	slalom
bez	bez	k7c2	bez
shození	shození	k1gNnSc2	shození
tyče	tyč	k1gFnSc2	tyč
v	v	k7c6	v
co	co	k9	co
nejkratším	krátký	k2eAgInSc6d3	nejkratší
čase	čas	k1gInSc6	čas
western	western	k1gInSc4	western
pleasure	pleasur	k1gMnSc5	pleasur
-	-	kIx~	-
soutěžící	soutěžící	k1gMnPc1	soutěžící
jsou	být	k5eAaImIp3nP	být
předvedeni	předveden	k2eAgMnPc1d1	předveden
všichni	všechen	k3xTgMnPc1	všechen
zároveň	zároveň	k6eAd1	zároveň
a	a	k8xC	a
na	na	k7c4	na
pokyn	pokyn	k1gInSc4	pokyn
rozhodčích	rozhodčí	k1gMnPc2	rozhodčí
předvádějí	předvádět	k5eAaImIp3nP	předvádět
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
cviky	cvik	k1gInPc1	cvik
ve	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
stanovených	stanovený	k2eAgInPc6d1	stanovený
chodech	chod	k1gInPc6	chod
<g/>
,	,	kIx,	,
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
se	se	k3xPyFc4	se
sed	sed	k1gInSc1	sed
jezdce	jezdec	k1gMnSc2	jezdec
<g/>
,	,	kIx,	,
pobídky	pobídka	k1gFnSc2	pobídka
<g/>
,	,	kIx,	,
připravenost	připravenost	k1gFnSc1	připravenost
a	a	k8xC	a
ochota	ochota	k1gFnSc1	ochota
koně	kůň	k1gMnSc2	kůň
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
i	i	k9	i
celkový	celkový	k2eAgInSc4d1	celkový
vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
upravenost	upravenost	k1gFnSc4	upravenost
dvojice	dvojice	k1gFnSc2	dvojice
reining	reining	k1gInSc1	reining
-	-	kIx~	-
rychlostní	rychlostní	k2eAgFnSc1d1	rychlostní
disciplína	disciplína	k1gFnSc1	disciplína
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgInPc4	který
však	však	k9	však
kůň	kůň	k1gMnSc1	kůň
musí	muset	k5eAaImIp3nS	muset
prokázat	prokázat	k5eAaPmF	prokázat
vysoký	vysoký	k2eAgInSc4d1	vysoký
stupeň	stupeň	k1gInSc4	stupeň
ovladatelnosti	ovladatelnost	k1gFnSc2	ovladatelnost
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
složitější	složitý	k2eAgInPc4d2	složitější
westernové	westernový	k2eAgInPc4d1	westernový
prvky	prvek	k1gInPc4	prvek
jako	jako	k8xS	jako
klouzavé	klouzavý	k2eAgNnSc4d1	klouzavé
zastavení	zastavení	k1gNnSc4	zastavení
ze	z	k7c2	z
cvalu	cval	k1gInSc2	cval
<g/>
,	,	kIx,	,
obraty	obrat	k1gInPc1	obrat
<g/>
,	,	kIx,	,
couvání	couvání	k1gNnPc1	couvání
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nehybné	hybný	k2eNgNnSc1d1	nehybné
stání	stání	k1gNnSc1	stání
po	po	k7c4	po
několik	několik	k4yIc4	několik
sekund	sekunda	k1gFnPc2	sekunda
western	western	k1gInSc1	western
horsemanship	horsemanship	k1gMnSc1	horsemanship
-	-	kIx~	-
drezurní	drezurní	k2eAgFnSc1d1	drezurní
soutěž	soutěž	k1gFnSc1	soutěž
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
se	se	k3xPyFc4	se
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
zejména	zejména	k9	zejména
jezdecká	jezdecký	k2eAgFnSc1d1	jezdecká
vyspělost	vyspělost	k1gFnSc1	vyspělost
<g/>
,	,	kIx,	,
sed	sed	k1gInSc1	sed
a	a	k8xC	a
udělování	udělování	k1gNnSc4	udělování
pobídek	pobídka	k1gFnPc2	pobídka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
sezóny	sezóna	k1gFnSc2	sezóna
se	se	k3xPyFc4	se
pořádá	pořádat	k5eAaImIp3nS	pořádat
tzv.	tzv.	kA	tzv.
Hubertova	Hubertův	k2eAgFnSc1d1	Hubertova
jízda	jízda	k1gFnSc1	jízda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
konec	konec	k1gInSc1	konec
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Jezdec	jezdec	k1gMnSc1	jezdec
při	při	k7c6	při
ní	on	k3xPp3gFnSc6	on
musí	muset	k5eAaImIp3nS	muset
chytit	chytit	k5eAaPmF	chytit
pírko	pírko	k1gNnSc4	pírko
<g/>
,	,	kIx,	,
být	být	k5eAaImF	být
nejrychlejší	rychlý	k2eAgNnSc4d3	nejrychlejší
a	a	k8xC	a
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
skákat	skákat	k5eAaImF	skákat
překážky	překážka	k1gFnPc4	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
nemoci	nemoc	k1gFnSc2	nemoc
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Zdravý	zdravý	k2eAgMnSc1d1	zdravý
kůň	kůň	k1gMnSc1	kůň
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
teplotu	teplota	k1gFnSc4	teplota
37,5	[number]	k4	37,5
<g/>
-	-	kIx~	-
<g/>
38,5	[number]	k4	38,5
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
hříbata	hříbě	k1gNnPc1	hříbě
asi	asi	k9	asi
o	o	k7c6	o
1	[number]	k4	1
°	°	k?	°
<g/>
C	C	kA	C
vyšší	vysoký	k2eAgInPc1d2	vyšší
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
klidová	klidový	k2eAgFnSc1d1	klidová
frekvence	frekvence	k1gFnSc1	frekvence
dýchání	dýchání	k1gNnSc2	dýchání
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
dechů	dech	k1gInPc2	dech
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
(	(	kIx(	(
<g/>
hříbata	hříbě	k1gNnPc4	hříbě
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tepová	tepový	k2eAgFnSc1d1	tepová
klidová	klidový	k2eAgFnSc1d1	klidová
frekvence	frekvence	k1gFnSc1	frekvence
je	být	k5eAaImIp3nS	být
35	[number]	k4	35
<g/>
-	-	kIx~	-
<g/>
45	[number]	k4	45
tepů	tep	k1gInPc2	tep
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
(	(	kIx(	(
<g/>
hříbata	hříbě	k1gNnPc4	hříbě
100	[number]	k4	100
<g/>
-	-	kIx~	-
<g/>
120	[number]	k4	120
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nemoci	nemoc	k1gFnPc1	nemoc
a	a	k8xC	a
úrazy	úraz	k1gInPc1	úraz
koní	kůň	k1gMnPc2	kůň
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
onemocnění	onemocnění	k1gNnSc4	onemocnění
kopyt	kopyto	k1gNnPc2	kopyto
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
otlaky	otlak	k1gInPc4	otlak
<g/>
,	,	kIx,	,
schvácení	schvácení	k1gNnSc1	schvácení
kopyt	kopyto	k1gNnPc2	kopyto
<g/>
,	,	kIx,	,
podotrochlóza	podotrochlóza	k1gFnSc1	podotrochlóza
<g/>
,	,	kIx,	,
abscesy	absces	k1gInPc1	absces
<g/>
,	,	kIx,	,
hniloba	hniloba	k1gFnSc1	hniloba
rohového	rohový	k2eAgInSc2d1	rohový
střelu	střel	k1gInSc2	střel
<g/>
,	,	kIx,	,
rozštěp	rozštěp	k1gInSc1	rozštěp
kopyta	kopyto	k1gNnSc2	kopyto
<g/>
,	,	kIx,	,
rakovina	rakovina	k1gFnSc1	rakovina
kopyt	kopyto	k1gNnPc2	kopyto
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nemoci	nemoc	k1gFnPc4	nemoc
kloubů	kloub	k1gInPc2	kloub
a	a	k8xC	a
končetin	končetina	k1gFnPc2	končetina
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
nálevky	nálevka	k1gFnPc1	nálevka
<g/>
,	,	kIx,	,
špánek	špánek	k1gInSc1	špánek
<g/>
,	,	kIx,	,
kostnatění	kostnatění	k1gNnSc1	kostnatění
kopytních	kopytní	k2eAgFnPc2d1	kopytní
chrupavek	chrupavka	k1gFnPc2	chrupavka
<g/>
,	,	kIx,	,
natažení	natažení	k1gNnSc3	natažení
šlachy	šlacha	k1gFnSc2	šlacha
<g/>
,	,	kIx,	,
zášlapy	zášlap	k1gInPc4	zášlap
a	a	k8xC	a
odřeniny	odřenina	k1gFnPc4	odřenina
<g/>
,	,	kIx,	,
návní	návní	k2eAgFnSc4d1	návní
kost	kost	k1gFnSc4	kost
<g/>
,	,	kIx,	,
zlomeniny	zlomenina	k1gFnPc4	zlomenina
<g/>
,	,	kIx,	,
šinbajny	šinbajna	k1gFnPc4	šinbajna
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nemoci	nemoc	k1gFnPc4	nemoc
očí	oko	k1gNnPc2	oko
(	(	kIx(	(
<g/>
zákal	zákal	k1gInSc1	zákal
<g/>
,	,	kIx,	,
zánět	zánět	k1gInSc1	zánět
spojivek	spojivka	k1gFnPc2	spojivka
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
nemoci	nemoc	k1gFnPc4	nemoc
dutiny	dutina	k1gFnSc2	dutina
ústní	ústní	k2eAgFnSc2d1	ústní
(	(	kIx(	(
<g/>
řádky	řádka	k1gFnSc2	řádka
(	(	kIx(	(
<g/>
otok	otok	k1gInSc1	otok
horního	horní	k2eAgNnSc2d1	horní
patra	patro	k1gNnSc2	patro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nemoci	nemoc	k1gFnPc4	nemoc
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
onemocnění	onemocnění	k1gNnSc1	onemocnění
kůže	kůže	k1gFnSc2	kůže
(	(	kIx(	(
<g/>
podlom	podlom	k1gInSc1	podlom
<g/>
,	,	kIx,	,
dermatofilóza	dermatofilóza	k1gFnSc1	dermatofilóza
<g/>
,	,	kIx,	,
otlaky	otlak	k1gInPc1	otlak
<g/>
,	,	kIx,	,
letní	letní	k2eAgFnSc1d1	letní
vyrážka	vyrážka	k1gFnSc1	vyrážka
<g/>
,	,	kIx,	,
plísňová	plísňový	k2eAgNnPc1d1	plísňové
onemocnění	onemocnění	k1gNnPc1	onemocnění
<g/>
,	,	kIx,	,
svrab	svrab	k1gInSc1	svrab
<g/>
,	,	kIx,	,
vši	veš	k1gFnPc1	veš
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
střečkovitost	střečkovitost	k1gFnSc4	střečkovitost
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poruchy	poruch	k1gInPc1	poruch
trávicí	trávicí	k2eAgFnSc2d1	trávicí
soustavy	soustava	k1gFnSc2	soustava
(	(	kIx(	(
<g/>
kolika	kolika	k1gFnSc1	kolika
<g/>
,	,	kIx,	,
průjem	průjem	k1gInSc1	průjem
<g/>
,	,	kIx,	,
otrava	otrava	k1gFnSc1	otrava
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poruchy	porucha	k1gFnPc1	porucha
oběhového	oběhový	k2eAgInSc2d1	oběhový
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
anémie	anémie	k1gFnSc1	anémie
<g/>
,	,	kIx,	,
lymfangitida	lymfangitida	k1gFnSc1	lymfangitida
<g/>
,	,	kIx,	,
africký	africký	k2eAgInSc1d1	africký
mor	mor	k1gInSc1	mor
koní	kůň	k1gMnPc2	kůň
<g/>
,	,	kIx,	,
dehydratace	dehydratace	k1gFnSc1	dehydratace
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nemoci	nemoc	k1gFnPc4	nemoc
dýchací	dýchací	k2eAgFnSc2d1	dýchací
soustavy	soustava	k1gFnSc2	soustava
(	(	kIx(	(
<g/>
chřipka	chřipka	k1gFnSc1	chřipka
koní	kůň	k1gMnPc2	kůň
<g/>
,	,	kIx,	,
hříběcí	hříběcí	k2eAgFnPc1d1	hříběcí
<g/>
,	,	kIx,	,
obrna	obrna	k1gFnSc1	obrna
hrtanu	hrtan	k1gInSc2	hrtan
<g/>
,	,	kIx,	,
COPD	COPD	kA	COPD
<g/>
,	,	kIx,	,
kašel	kašel	k1gInSc1	kašel
<g/>
,	,	kIx,	,
vozhřivka	vozhřivka	k1gFnSc1	vozhřivka
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pohlavní	pohlavní	k2eAgNnSc1d1	pohlavní
onemocnění	onemocnění	k1gNnSc1	onemocnění
(	(	kIx(	(
<g/>
metritida	metritida	k1gFnSc1	metritida
<g/>
,	,	kIx,	,
infekční	infekční	k2eAgNnSc1d1	infekční
zmetání	zmetání	k1gNnSc1	zmetání
klisen	klisna	k1gFnPc2	klisna
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
závažným	závažný	k2eAgNnSc7d1	závažné
onemocněním	onemocnění	k1gNnSc7	onemocnění
je	být	k5eAaImIp3nS	být
tetanus	tetanus	k1gInSc1	tetanus
a	a	k8xC	a
vzteklina	vzteklina	k1gFnSc1	vzteklina
<g/>
,	,	kIx,	,
koně	kůň	k1gMnPc1	kůň
můžou	můžou	k?	můžou
trpět	trpět	k5eAaImF	trpět
i	i	k9	i
alergiemi	alergie	k1gFnPc7	alergie
<g/>
.	.	kIx.	.
</s>
<s>
Koně	kůň	k1gMnPc1	kůň
často	často	k6eAd1	často
napadají	napadat	k5eAaPmIp3nP	napadat
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
i	i	k9	i
vnější	vnější	k2eAgMnSc1d1	vnější
paraziti	parazit	k1gMnPc1	parazit
-	-	kIx~	-
vši	veš	k1gFnPc1	veš
<g/>
,	,	kIx,	,
svrab	svrab	k1gInSc1	svrab
<g/>
,	,	kIx,	,
klíšťata	klíště	k1gNnPc1	klíště
<g/>
,	,	kIx,	,
škrkavky	škrkavka	k1gFnPc1	škrkavka
<g/>
,	,	kIx,	,
tasemnice	tasemnice	k1gFnPc1	tasemnice
<g/>
,	,	kIx,	,
roupi	roup	k1gMnPc1	roup
<g/>
,	,	kIx,	,
velcí	velký	k2eAgMnPc1d1	velký
a	a	k8xC	a
malí	malý	k2eAgMnPc1d1	malý
strongylidi	strongylid	k1gMnPc1	strongylid
<g/>
,	,	kIx,	,
střečci	střeček	k1gMnPc1	střeček
<g/>
.	.	kIx.	.
</s>
<s>
Zlozvyky	zlozvyk	k1gInPc1	zlozvyk
koní	kůň	k1gMnPc2	kůň
jsou	být	k5eAaImIp3nP	být
poruchy	porucha	k1gFnPc1	porucha
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
opakují	opakovat	k5eAaImIp3nP	opakovat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
kůň	kůň	k1gMnSc1	kůň
nemá	mít	k5eNaImIp3nS	mít
důvod	důvod	k1gInSc4	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
klkání	klkání	k1gNnSc4	klkání
<g/>
,	,	kIx,	,
hodinaření	hodinařený	k2eAgMnPc1d1	hodinařený
<g/>
,	,	kIx,	,
vzpurnost	vzpurnost	k1gFnSc1	vzpurnost
<g/>
,	,	kIx,	,
útočnost	útočnost	k1gFnSc1	útočnost
<g/>
,	,	kIx,	,
drbání	drbání	k1gNnSc1	drbání
ocasu	ocas	k1gInSc2	ocas
<g/>
,	,	kIx,	,
lechtivost	lechtivost	k1gFnSc1	lechtivost
<g/>
,	,	kIx,	,
vyplazování	vyplazování	k1gNnSc1	vyplazování
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
neochota	neochota	k1gFnSc1	neochota
<g/>
,	,	kIx,	,
trhání	trhání	k1gNnSc1	trhání
dek	deka	k1gFnPc2	deka
a	a	k8xC	a
ohlávek	ohlávka	k1gFnPc2	ohlávka
<g/>
,	,	kIx,	,
skřípání	skřípání	k1gNnSc1	skřípání
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
...	...	k?	...
Špatná	špatný	k2eAgFnSc1d1	špatná
nebo	nebo	k8xC	nebo
nedostatečně	dostatečně	k6eNd1	dostatečně
vyvinutá	vyvinutý	k2eAgFnSc1d1	vyvinutá
stavba	stavba	k1gFnSc1	stavba
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
sražená	sražený	k2eAgFnSc1d1	sražená
záď	záď	k1gFnSc1	záď
<g/>
,	,	kIx,	,
jelení	jelení	k2eAgInSc1d1	jelení
krk	krk	k1gInSc1	krk
<g/>
,	,	kIx,	,
úzká	úzký	k2eAgNnPc1d1	úzké
kopyta	kopyto	k1gNnPc1	kopyto
<g/>
,	,	kIx,	,
kravský	kravský	k2eAgInSc1d1	kravský
postoj	postoj	k1gInSc1	postoj
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
problémy	problém	k1gInPc4	problém
a	a	k8xC	a
snižují	snižovat	k5eAaImIp3nP	snižovat
pohyblivost	pohyblivost	k1gFnSc4	pohyblivost
<g/>
.	.	kIx.	.
</s>
<s>
Nohy	noha	k1gFnPc1	noha
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
rovné	rovný	k2eAgFnPc4d1	rovná
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
vypoulené	vypoulený	k2eAgNnSc1d1	vypoulené
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
klenuté	klenutý	k2eAgNnSc1d1	klenuté
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Jezdečtí	jezdecký	k2eAgMnPc1d1	jezdecký
koně	kůň	k1gMnPc1	kůň
váží	vážit	k5eAaImIp3nP	vážit
od	od	k7c2	od
300	[number]	k4	300
po	po	k7c4	po
700	[number]	k4	700
kg	kg	kA	kg
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
plemenu	plemeno	k1gNnSc6	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
Tažní	tažní	k2eAgMnPc1d1	tažní
koně	kůň	k1gMnPc1	kůň
váží	vážit	k5eAaImIp3nP	vážit
od	od	k7c2	od
700	[number]	k4	700
kilogramů	kilogram	k1gInPc2	kilogram
do	do	k7c2	do
1	[number]	k4	1
tuny	tuna	k1gFnSc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
kůň	kůň	k1gMnSc1	kůň
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
byl	být	k5eAaImAgMnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
Shirský	Shirský	k2eAgMnSc1d1	Shirský
kůň	kůň	k1gMnSc1	kůň
jménem	jméno	k1gNnSc7	jméno
Mammoth	Mammoth	k1gMnSc1	Mammoth
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
220	[number]	k4	220
cm	cm	kA	cm
vysoký	vysoký	k2eAgMnSc1d1	vysoký
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
vrcholná	vrcholný	k2eAgFnSc1d1	vrcholná
váha	váha	k1gFnSc1	váha
byla	být	k5eAaImAgFnS	být
odhadnuta	odhadnout	k5eAaPmNgFnS	odhadnout
na	na	k7c4	na
1500	[number]	k4	1500
kg	kg	kA	kg
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgMnSc1d1	současný
nejmenší	malý	k2eAgMnSc1d3	nejmenší
kůň	kůň	k1gMnSc1	kůň
na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
Thumbelina	Thumbelina	k1gFnSc1	Thumbelina
<g/>
,	,	kIx,	,
plně	plně	k6eAd1	plně
dospělý	dospělý	k2eAgMnSc1d1	dospělý
miniaturní	miniaturní	k2eAgMnSc1d1	miniaturní
kůň	kůň	k1gMnSc1	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
43	[number]	k4	43
cm	cm	kA	cm
vysoká	vysoká	k1gFnSc1	vysoká
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
26	[number]	k4	26
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
přiměřená	přiměřený	k2eAgFnSc1d1	přiměřená
velikosti	velikost	k1gFnSc6	velikost
těla	tělo	k1gNnSc2	tělo
koně	kůň	k1gMnSc4	kůň
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
by	by	kYmCp3nS	by
mít	mít	k5eAaImF	mít
správný	správný	k2eAgInSc4d1	správný
úhel	úhel	k1gInSc4	úhel
připojení	připojení	k1gNnSc4	připojení
krku	krk	k1gInSc2	krk
kvůli	kvůli	k7c3	kvůli
funkci	funkce	k1gFnSc3	funkce
dýchání	dýchání	k1gNnSc2	dýchání
a	a	k8xC	a
pohyblivosti	pohyblivost	k1gFnSc2	pohyblivost
<g/>
.	.	kIx.	.
</s>
<s>
Nozdry	nozdra	k1gFnPc1	nozdra
jsou	být	k5eAaImIp3nP	být
velké	velký	k2eAgFnPc1d1	velká
a	a	k8xC	a
široké	široký	k2eAgFnPc1d1	široká
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
profilu	profil	k1gInSc2	profil
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
hlava	hlava	k1gFnSc1	hlava
rovná	rovný	k2eAgFnSc1d1	rovná
<g/>
,	,	kIx,	,
klabonosá	klabonosý	k2eAgFnSc1d1	klabonosá
(	(	kIx(	(
<g/>
vypouklá	vypouklý	k2eAgFnSc1d1	vypouklá
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
u	u	k7c2	u
starokladrubských	starokladrubský	k2eAgMnPc2d1	starokladrubský
koní	kůň	k1gMnPc2	kůň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
štičí	štičí	k2eAgFnSc7d1	štičí
(	(	kIx(	(
<g/>
konkávní	konkávní	k2eAgFnSc7d1	konkávní
<g/>
,	,	kIx,	,
s	s	k7c7	s
miskovitým	miskovitý	k2eAgInSc7d1	miskovitý
profilem	profil	k1gInSc7	profil
<g/>
,	,	kIx,	,
u	u	k7c2	u
arabských	arabský	k2eAgMnPc2d1	arabský
koní	kůň	k1gMnPc2	kůň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
volská	volský	k2eAgFnSc1d1	volská
<g/>
,	,	kIx,	,
babská	babský	k2eAgFnSc1d1	babská
a	a	k8xC	a
klínovitá	klínovitý	k2eAgFnSc1d1	klínovitá
<g/>
.	.	kIx.	.
</s>
<s>
Uši	ucho	k1gNnPc1	ucho
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgFnPc1d1	důležitá
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
sluch	sluch	k1gInSc4	sluch
koně	kůň	k1gMnSc2	kůň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnSc1	jejich
pozice	pozice	k1gFnSc1	pozice
slouží	sloužit	k5eAaImIp3nS	sloužit
i	i	k9	i
ke	k	k7c3	k
komunikaci	komunikace	k1gFnSc3	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
uši	ucho	k1gNnPc4	ucho
sklopené	sklopený	k2eAgFnSc2d1	sklopená
dozadu	dozadu	k6eAd1	dozadu
znamenají	znamenat	k5eAaImIp3nP	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kůň	kůň	k1gMnSc1	kůň
je	být	k5eAaImIp3nS	být
podrážděn	podrážděn	k2eAgMnSc1d1	podrážděn
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc1	oko
jsou	být	k5eAaImIp3nP	být
velké	velký	k2eAgInPc1d1	velký
a	a	k8xC	a
jasné	jasný	k2eAgInPc1d1	jasný
<g/>
,	,	kIx,	,
posazeny	posazen	k2eAgInPc1d1	posazen
jsou	být	k5eAaImIp3nP	být
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Zrakové	zrakový	k2eAgNnSc1d1	zrakové
pole	pole	k1gNnSc1	pole
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
asi	asi	k9	asi
340	[number]	k4	340
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Žuchvy	žuchva	k1gFnPc1	žuchva
neboli	neboli	k8xC	neboli
ganaše	ganaše	k1gFnPc1	ganaše
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
hlavně	hlavně	k9	hlavně
žvýkacími	žvýkací	k2eAgInPc7d1	žvýkací
svaly	sval	k1gInPc7	sval
<g/>
.	.	kIx.	.
</s>
<s>
Kůň	kůň	k1gMnSc1	kůň
má	mít	k5eAaImIp3nS	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
hlavu	hlava	k1gFnSc4	hlava
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
při	při	k7c6	při
pasení	pasení	k1gNnSc6	pasení
mohl	moct	k5eAaImAgInS	moct
mít	mít	k5eAaImF	mít
oči	oko	k1gNnPc4	oko
dostatečně	dostatečně	k6eAd1	dostatečně
vysoko	vysoko	k6eAd1	vysoko
a	a	k8xC	a
viděl	vidět	k5eAaImAgMnS	vidět
nad	nad	k7c7	nad
vrcholky	vrcholek	k1gInPc7	vrcholek
trav	tráva	k1gFnPc2	tráva
případné	případný	k2eAgNnSc4d1	případné
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
krku	krk	k1gInSc2	krk
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
všech	všecek	k3xTgMnPc2	všecek
ostatních	ostatní	k2eAgMnPc2d1	ostatní
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
7	[number]	k4	7
krčních	krční	k2eAgInPc2d1	krční
obratlů	obratel	k1gInPc2	obratel
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
a	a	k8xC	a
tvar	tvar	k1gInSc4	tvar
krku	krk	k1gInSc2	krk
mají	mít	k5eAaImIp3nP	mít
značný	značný	k2eAgInSc4d1	značný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
pohyb	pohyb	k1gInSc4	pohyb
koně	kůň	k1gMnSc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgFnSc1d1	přední
noha	noha	k1gFnSc1	noha
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
<g/>
:	:	kIx,	:
plec	plec	k1gFnSc1	plec
-	-	kIx~	-
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
lopatky	lopatka	k1gFnSc2	lopatka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
svírat	svírat	k5eAaImF	svírat
asi	asi	k9	asi
45	[number]	k4	45
<g/>
°	°	k?	°
se	se	k3xPyFc4	se
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
ramenní	ramenní	k2eAgFnSc2d1	ramenní
kosti	kost	k1gFnSc2	kost
předloktí	předloktí	k1gNnSc4	předloktí
-	-	kIx~	-
tvoří	tvořit	k5eAaImIp3nS	tvořit
ho	on	k3xPp3gMnSc4	on
kost	kost	k1gFnSc1	kost
loketní	loketní	k2eAgMnPc1d1	loketní
a	a	k8xC	a
vřetenní	vřetenní	k2eAgMnPc1d1	vřetenní
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
jsou	být	k5eAaImIp3nP	být
vzadu	vzadu	k6eAd1	vzadu
srostlé	srostlý	k2eAgFnPc1d1	srostlá
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
dobře	dobře	k6eAd1	dobře
osvalené	osvalený	k2eAgNnSc4d1	osvalené
<g/>
.	.	kIx.	.
zápěstí	zápěstí	k1gNnSc4	zápěstí
(	(	kIx(	(
<g/>
carpus	carpus	k1gInSc1	carpus
<g/>
)	)	kIx)	)
-	-	kIx~	-
kloub	kloub	k1gInSc1	kloub
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
malých	malý	k2eAgFnPc2d1	malá
kůstek	kůstka	k1gFnPc2	kůstka
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
na	na	k7c6	na
ose	osa	k1gFnSc6	osa
přední	přední	k2eAgFnSc2d1	přední
končetiny	končetina	k1gFnSc2	končetina
<g/>
.	.	kIx.	.
záprstí	záprstí	k1gNnSc1	záprstí
spěnka	spěnka	k1gFnSc1	spěnka
-	-	kIx~	-
má	mít	k5eAaImIp3nS	mít
svírat	svírat	k5eAaImF	svírat
úhel	úhel	k1gInSc4	úhel
asi	asi	k9	asi
45	[number]	k4	45
<g/>
°	°	k?	°
se	se	k3xPyFc4	se
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
spěnková	spěnkový	k2eAgFnSc1d1	spěnková
kost	kost	k1gFnSc1	kost
a	a	k8xC	a
kosti	kost	k1gFnPc1	kost
sezamské	sezamský	k2eAgFnPc1d1	sezamská
korunka	korunka	k1gFnSc1	korunka
-	-	kIx~	-
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
kost	kost	k1gFnSc1	kost
korunková	korunkový	k2eAgFnSc1d1	korunková
kopyto	kopyto	k1gNnSc4	kopyto
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
kožním	kožní	k2eAgInSc7d1	kožní
derivátem	derivát	k1gInSc7	derivát
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
kost	kost	k1gFnSc1	kost
kopytní	kopytní	k2eAgFnSc1d1	kopytní
a	a	k8xC	a
střelková	střelkový	k2eAgFnSc1d1	Střelková
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
potaženy	potažen	k2eAgFnPc1d1	potažena
škárou	škára	k1gFnSc7	škára
a	a	k8xC	a
celé	celý	k2eAgNnSc1d1	celé
kopyto	kopyto	k1gNnSc1	kopyto
chrání	chránit	k5eAaImIp3nS	chránit
rohové	rohový	k2eAgNnSc4d1	rohové
pouzdro	pouzdro	k1gNnSc4	pouzdro
<g/>
.	.	kIx.	.
</s>
<s>
Kopyto	kopyto	k1gNnSc4	kopyto
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c4	na
část	část	k1gFnSc4	část
chodidlovou	chodidlový	k2eAgFnSc4d1	chodidlová
<g/>
,	,	kIx,	,
stěnovou	stěnový	k2eAgFnSc4d1	stěnová
<g/>
,	,	kIx,	,
střelovou	střelový	k2eAgFnSc4d1	střelový
a	a	k8xC	a
větve	větev	k1gFnPc1	větev
na	na	k7c4	na
které	který	k3yIgMnPc4	který
dosedá	dosedat	k5eAaImIp3nS	dosedat
kopytní	kopytní	k2eAgFnSc1d1	kopytní
chrupavka	chrupavka	k1gFnSc1	chrupavka
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
by	by	kYmCp3nS	by
svírat	svírat	k5eAaImF	svírat
úhel	úhel	k1gInSc4	úhel
asi	asi	k9	asi
55	[number]	k4	55
<g/>
°	°	k?	°
s	s	k7c7	s
horizontálou	horizontála	k1gFnSc7	horizontála
<g/>
.	.	kIx.	.
</s>
<s>
Kopyta	kopyto	k1gNnPc1	kopyto
se	se	k3xPyFc4	se
před	před	k7c7	před
opotřebováním	opotřebování	k1gNnSc7	opotřebování
chrání	chránit	k5eAaImIp3nP	chránit
podkovami	podkova	k1gFnPc7	podkova
<g/>
.	.	kIx.	.
</s>
<s>
Zadní	zadní	k2eAgFnSc1d1	zadní
noha	noha	k1gFnSc1	noha
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
<g/>
:	:	kIx,	:
stehno	stehno	k1gNnSc1	stehno
-	-	kIx~	-
stehenní	stehenní	k2eAgFnSc1d1	stehenní
kost	kost	k1gFnSc1	kost
<g/>
,	,	kIx,	,
stehenní	stehenní	k2eAgInSc1d1	stehenní
sval	sval	k1gInSc1	sval
bérec	bérec	k1gInSc1	bérec
-	-	kIx~	-
holenní	holenní	k2eAgFnSc1d1	holenní
a	a	k8xC	a
lýtková	lýtkový	k2eAgFnSc1d1	lýtková
kost	kost	k1gFnSc1	kost
<g/>
.	.	kIx.	.
</s>
<s>
Bérce	bérec	k1gInPc1	bérec
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
a	a	k8xC	a
svalnaté	svalnatý	k2eAgInPc4d1	svalnatý
hlezenní	hlezenní	k2eAgInPc4d1	hlezenní
kloub	kloub	k1gInSc4	kloub
-	-	kIx~	-
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
kloubů	kloub	k1gInPc2	kloub
<g/>
,	,	kIx,	,
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
6	[number]	k4	6
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
by	by	kYmCp3nP	by
být	být	k5eAaImF	být
pod	pod	k7c7	pod
hrbolem	hrbol	k1gInSc7	hrbol
sedací	sedací	k2eAgFnSc2d1	sedací
kosti	kost	k1gFnSc2	kost
nárt	nárt	k1gInSc1	nárt
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
delší	dlouhý	k2eAgMnSc1d2	delší
než	než	k8xS	než
záprstí	záprstí	k1gNnSc1	záprstí
<g/>
,	,	kIx,	,
čím	co	k3yInSc7	co
je	být	k5eAaImIp3nS	být
delší	dlouhý	k2eAgNnSc1d2	delší
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
rychleji	rychle	k6eAd2	rychle
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
kůň	kůň	k1gMnSc1	kůň
pohybovat	pohybovat	k5eAaImF	pohybovat
spěnka	spěnka	k1gFnSc1	spěnka
-	-	kIx~	-
má	mít	k5eAaImIp3nS	mít
svírat	svírat	k5eAaImF	svírat
asi	asi	k9	asi
45	[number]	k4	45
<g/>
°	°	k?	°
se	se	k3xPyFc4	se
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
spěnková	spěnkový	k2eAgFnSc1d1	spěnková
kost	kost	k1gFnSc1	kost
korunka	korunka	k1gFnSc1	korunka
-	-	kIx~	-
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
kost	kost	k1gFnSc1	kost
korunková	korunkový	k2eAgFnSc1d1	korunková
kopyto	kopyto	k1gNnSc4	kopyto
-	-	kIx~	-
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
kost	kost	k1gFnSc1	kost
kopytní	kopytní	k2eAgFnSc1d1	kopytní
a	a	k8xC	a
střelková	střelkový	k2eAgFnSc1d1	Střelková
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
potaženy	potažen	k2eAgFnPc1d1	potažena
škárou	škára	k1gFnSc7	škára
a	a	k8xC	a
celé	celý	k2eAgNnSc1d1	celé
kopyto	kopyto	k1gNnSc1	kopyto
chrání	chránit	k5eAaImIp3nS	chránit
rohové	rohový	k2eAgNnSc4d1	rohové
pouzdro	pouzdro	k1gNnSc4	pouzdro
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
by	by	kYmCp3nS	by
svírat	svírat	k5eAaImF	svírat
asi	asi	k9	asi
45	[number]	k4	45
<g/>
°	°	k?	°
s	s	k7c7	s
horizontálou	horizontála	k1gFnSc7	horizontála
<g/>
.	.	kIx.	.
</s>
<s>
Kopyta	kopyto	k1gNnPc1	kopyto
se	se	k3xPyFc4	se
před	před	k7c7	před
opotřebováním	opotřebování	k1gNnSc7	opotřebování
chrání	chránit	k5eAaImIp3nP	chránit
podkovami	podkova	k1gFnPc7	podkova
<g/>
.	.	kIx.	.
</s>
<s>
Hřbet	hřbet	k1gInSc1	hřbet
tvoří	tvořit	k5eAaImIp3nS	tvořit
18	[number]	k4	18
obratlů	obratel	k1gInPc2	obratel
<g/>
,	,	kIx,	,
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
se	se	k3xPyFc4	se
upíná	upínat	k5eAaImIp3nS	upínat
18	[number]	k4	18
párů	pár	k1gInPc2	pár
žeber	žebro	k1gNnPc2	žebro
<g/>
,	,	kIx,	,
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
párů	pár	k1gInPc2	pár
nepravých	pravý	k2eNgInPc2d1	nepravý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
hřbetu	hřbet	k1gInSc2	hřbet
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
kohoutek	kohoutek	k1gInSc1	kohoutek
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yRnSc7	co
je	být	k5eAaImIp3nS	být
hrudník	hrudník	k1gInSc1	hrudník
prostornější	prostorný	k2eAgInSc1d2	prostornější
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
vyšší	vysoký	k2eAgInPc4d2	vyšší
výkony	výkon	k1gInPc4	výkon
může	moct	k5eAaImIp3nS	moct
kůň	kůň	k1gMnSc1	kůň
podávat	podávat	k5eAaImF	podávat
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
beder	bedra	k1gNnPc2	bedra
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
bederních	bederní	k2eAgInPc2d1	bederní
obratlů	obratel	k1gInPc2	obratel
<g/>
,	,	kIx,	,
u	u	k7c2	u
arabských	arabský	k2eAgMnPc2d1	arabský
koní	kůň	k1gMnPc2	kůň
jich	on	k3xPp3gFnPc2	on
bývá	bývat	k5eAaImIp3nS	bývat
jen	jen	k9	jen
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
široká	široký	k2eAgNnPc4d1	široké
a	a	k8xC	a
pevná	pevný	k2eAgNnPc4d1	pevné
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
pánev	pánev	k1gFnSc4	pánev
a	a	k8xC	a
svalstvo	svalstvo	k1gNnSc4	svalstvo
<g/>
,	,	kIx,	,
připojuje	připojovat	k5eAaImIp3nS	připojovat
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
ohon	ohon	k1gInSc1	ohon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
odhánění	odhánění	k1gNnSc3	odhánění
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
17	[number]	k4	17
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
ocasních	ocasní	k2eAgInPc2d1	ocasní
obratlů	obratel	k1gInPc2	obratel
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
porostlý	porostlý	k2eAgMnSc1d1	porostlý
žíněmi	žíně	k1gFnPc7	žíně
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
ocas	ocas	k1gInSc1	ocas
svěšený	svěšený	k2eAgInSc1d1	svěšený
mezi	mezi	k7c7	mezi
zadníma	zadní	k2eAgFnPc7d1	zadní
nohama	noha	k1gFnPc7	noha
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
to	ten	k3xDgNnSc1	ten
známka	známka	k1gFnSc1	známka
onemocnění	onemocnění	k1gNnPc2	onemocnění
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgFnPc2d1	jiná
stresových	stresový	k2eAgFnPc2d1	stresová
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Srst	srst	k1gFnSc1	srst
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
krycích	krycí	k2eAgInPc2d1	krycí
a	a	k8xC	a
podsadových	podsadový	k2eAgInPc2d1	podsadový
chlupů	chlup	k1gInPc2	chlup
<g/>
.	.	kIx.	.
</s>
<s>
Podsadové	Podsadový	k2eAgInPc1d1	Podsadový
chlupy	chlup	k1gInPc1	chlup
jsou	být	k5eAaImIp3nP	být
duté	dutý	k2eAgInPc1d1	dutý
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
tepelné	tepelný	k2eAgFnSc3d1	tepelná
izolaci	izolace	k1gFnSc3	izolace
<g/>
,	,	kIx,	,
narůstají	narůstat	k5eAaImIp3nP	narůstat
na	na	k7c4	na
zimu	zima	k1gFnSc4	zima
<g/>
.	.	kIx.	.
</s>
<s>
Hříbata	hříbě	k1gNnPc1	hříbě
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
s	s	k7c7	s
podsadovými	podsadový	k2eAgInPc7d1	podsadový
chlupy	chlup	k1gInPc7	chlup
<g/>
,	,	kIx,	,
krycí	krycí	k2eAgInSc1d1	krycí
jim	on	k3xPp3gInPc3	on
začínají	začínat	k5eAaImIp3nP	začínat
růst	růst	k5eAaImF	růst
až	až	k9	až
po	po	k7c6	po
asi	asi	k9	asi
3	[number]	k4	3
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Výměna	výměna	k1gFnSc1	výměna
chlupů	chlup	k1gInPc2	chlup
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
línání	línání	k1gNnSc1	línání
<g/>
.	.	kIx.	.
</s>
<s>
Kůň	kůň	k1gMnSc1	kůň
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
ochranné	ochranný	k2eAgInPc4d1	ochranný
chlupy	chlup	k1gInPc4	chlup
-	-	kIx~	-
žíně	žíně	k1gFnPc4	žíně
a	a	k8xC	a
rousy	rous	k1gInPc4	rous
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
hrubší	hrubý	k2eAgFnPc1d2	hrubší
<g/>
,	,	kIx,	,
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
a	a	k8xC	a
pevněji	pevně	k6eAd2	pevně
zapuštěné	zapuštěný	k2eAgInPc4d1	zapuštěný
do	do	k7c2	do
škáry	škára	k1gFnSc2	škára
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
kštici	kštice	k1gFnSc4	kštice
<g/>
,	,	kIx,	,
hřívu	hříva	k1gFnSc4	hříva
a	a	k8xC	a
rousy	rous	k1gInPc4	rous
<g/>
.	.	kIx.	.
</s>
<s>
Vysoce	vysoce	k6eAd1	vysoce
citlivé	citlivý	k2eAgInPc1d1	citlivý
jsou	být	k5eAaImIp3nP	být
hmatové	hmatový	k2eAgInPc1d1	hmatový
chlupy	chlup	k1gInPc1	chlup
na	na	k7c6	na
pyscích	pysk	k1gInPc6	pysk
a	a	k8xC	a
nad	nad	k7c7	nad
očima	oko	k1gNnPc7	oko
<g/>
.	.	kIx.	.
<g/>
Chlupové	Chlupové	k2eAgInPc1d1	Chlupové
víry	vír	k1gInPc1	vír
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgInPc1d1	důležitý
pro	pro	k7c4	pro
identifikaci	identifikace	k1gFnSc4	identifikace
<g/>
,	,	kIx,	,
vznikají	vznikat	k5eAaImIp3nP	vznikat
různým	různý	k2eAgNnSc7d1	různé
napětím	napětí	k1gNnSc7	napětí
podkožních	podkožní	k2eAgInPc2d1	podkožní
svalů	sval	k1gInPc2	sval
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
<g/>
,	,	kIx,	,
krku	krk	k1gInSc6	krk
<g/>
,	,	kIx,	,
prsou	prsa	k1gNnPc2	prsa
<g/>
,	,	kIx,	,
ve	v	k7c6	v
slabinách	slabina	k1gFnPc6	slabina
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
srsti	srst	k1gFnSc2	srst
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
barvou	barva	k1gFnSc7	barva
kůže	kůže	k1gFnSc2	kůže
určuje	určovat	k5eAaImIp3nS	určovat
barvu	barva	k1gFnSc4	barva
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yRnSc7	co
víc	hodně	k6eAd2	hodně
vzduchu	vzduch	k1gInSc2	vzduch
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
dřeni	dřeň	k1gFnSc6	dřeň
chlupu	chlup	k1gInSc2	chlup
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
světleji	světle	k6eAd2	světle
srst	srst	k1gFnSc1	srst
působí	působit	k5eAaImIp3nS	působit
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
barev	barva	k1gFnPc2	barva
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
nejzákladnější	základní	k2eAgInPc1d3	nejzákladnější
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Hnědák	hnědák	k1gMnSc1	hnědák
-	-	kIx~	-
od	od	k7c2	od
červenohnědé	červenohnědý	k2eAgFnSc2d1	červenohnědá
až	až	k9	až
po	po	k7c4	po
zlatou	zlatý	k2eAgFnSc4d1	zlatá
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
má	mít	k5eAaImIp3nS	mít
černou	černý	k2eAgFnSc4d1	černá
hřívu	hříva	k1gFnSc4	hříva
<g/>
,	,	kIx,	,
ocas	ocas	k1gInSc4	ocas
a	a	k8xC	a
dolní	dolní	k2eAgFnSc4d1	dolní
část	část	k1gFnSc4	část
nohou	noha	k1gFnPc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Vraník	vraník	k1gMnSc1	vraník
je	být	k5eAaImIp3nS	být
celý	celý	k2eAgInSc4d1	celý
černý	černý	k2eAgInSc4d1	černý
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
tzv.	tzv.	kA	tzv.
letní	letní	k2eAgMnPc1d1	letní
vraníci	vraník	k1gMnPc1	vraník
s	s	k7c7	s
hnědočernou	hnědočerný	k2eAgFnSc7d1	hnědočerná
srstí	srst	k1gFnSc7	srst
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
a	a	k8xC	a
černou	černý	k2eAgFnSc7d1	černá
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Bělouš	bělouš	k1gInSc1	bělouš
vybělující	vybělující	k2eAgInSc1d1	vybělující
má	mít	k5eAaImIp3nS	mít
jako	jako	k9	jako
hříbě	hříbě	k1gNnSc4	hříbě
většinou	většinou	k6eAd1	většinou
hnědou	hnědý	k2eAgFnSc4d1	hnědá
nebo	nebo	k8xC	nebo
černou	černý	k2eAgFnSc4d1	černá
srst	srst	k1gFnSc4	srst
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
věkem	věk	k1gInSc7	věk
vyběluje	vybělovat	k5eAaImIp3nS	vybělovat
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
hlavy	hlava	k1gFnSc2	hlava
až	až	k9	až
po	po	k7c4	po
úplně	úplně	k6eAd1	úplně
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Bělouš	bělouš	k1gInSc1	bělouš
nevybělující	vybělující	k2eNgInSc1d1	nevybělující
má	mít	k5eAaImIp3nS	mít
bílou	bílý	k2eAgFnSc4d1	bílá
srst	srst	k1gFnSc4	srst
smíšenou	smíšený	k2eAgFnSc4d1	smíšená
s	s	k7c7	s
nějakou	nějaký	k3yIgFnSc7	nějaký
další	další	k2eAgFnSc7d1	další
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
vzniká	vznikat	k5eAaImIp3nS	vznikat
červený	červený	k2eAgInSc1d1	červený
bělouš	bělouš	k1gInSc1	bělouš
-	-	kIx~	-
jablečňák	jablečňák	k1gMnSc1	jablečňák
<g/>
,	,	kIx,	,
hnědý	hnědý	k2eAgMnSc1d1	hnědý
bělouš	bělouš	k1gMnSc1	bělouš
<g/>
,	,	kIx,	,
šedý	šedý	k2eAgMnSc1d1	šedý
bělouš	bělouš	k1gMnSc1	bělouš
-	-	kIx~	-
mourek	mourek	k1gMnSc1	mourek
atd.	atd.	kA	atd.
Nevybělující	Nevybělující	k2eAgMnPc1d1	Nevybělující
bělouši	bělouš	k1gMnPc1	bělouš
mají	mít	k5eAaImIp3nP	mít
vždy	vždy	k6eAd1	vždy
hlavu	hlava	k1gFnSc4	hlava
tmavší	tmavý	k2eAgFnSc4d2	tmavší
<g/>
,	,	kIx,	,
než	než	k8xS	než
zbytek	zbytek	k1gInSc1	zbytek
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Albín	albín	k1gInSc1	albín
má	mít	k5eAaImIp3nS	mít
úplně	úplně	k6eAd1	úplně
bílou	bílý	k2eAgFnSc4d1	bílá
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
krémovější	krémový	k2eAgFnSc4d2	krémovější
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc1	oko
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
červené	červený	k2eAgFnPc1d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
Albín	albín	k1gInSc1	albín
má	mít	k5eAaImIp3nS	mít
kůži	kůže	k1gFnSc4	kůže
pod	pod	k7c7	pod
srstí	srst	k1gFnSc7	srst
růžovou	růžový	k2eAgFnSc7d1	růžová
<g/>
,	,	kIx,	,
bělouš	bělouš	k1gInSc1	bělouš
černou	černá	k1gFnSc4	černá
<g/>
.	.	kIx.	.
</s>
<s>
Ryzák	ryzák	k1gMnSc1	ryzák
-	-	kIx~	-
srst	srst	k1gFnSc1	srst
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
odstínech	odstín	k1gInPc6	odstín
zlaté	zlatá	k1gFnSc2	zlatá
<g/>
,	,	kIx,	,
stejnou	stejný	k2eAgFnSc4d1	stejná
barvu	barva	k1gFnSc4	barva
nebo	nebo	k8xC	nebo
lehce	lehko	k6eAd1	lehko
rozdílnou	rozdílný	k2eAgFnSc7d1	rozdílná
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
hříva	hříva	k1gFnSc1	hříva
a	a	k8xC	a
ocas	ocas	k1gInSc1	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Plavák	plavák	k1gInSc1	plavák
má	mít	k5eAaImIp3nS	mít
barvu	barva	k1gFnSc4	barva
do	do	k7c2	do
žlutošeda	žlutošed	k1gMnSc2	žlutošed
<g/>
,	,	kIx,	,
tmavou	tmavý	k2eAgFnSc4d1	tmavá
hřívu	hříva	k1gFnSc4	hříva
<g/>
,	,	kIx,	,
ocas	ocas	k1gInSc4	ocas
i	i	k8xC	i
nohy	noha	k1gFnPc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
vždy	vždy	k6eAd1	vždy
tzv.	tzv.	kA	tzv.
úhoří	úhořit	k5eAaImIp3nS	úhořit
pruh	pruh	k1gInSc1	pruh
(	(	kIx(	(
<g/>
pruh	pruh	k1gInSc1	pruh
tmavé	tmavý	k2eAgFnSc2d1	tmavá
srsti	srst	k1gFnSc2	srst
vedoucí	vedoucí	k1gMnSc1	vedoucí
podél	podél	k7c2	podél
páteře	páteř	k1gFnSc2	páteř
od	od	k7c2	od
hřívy	hříva	k1gFnSc2	hříva
k	k	k7c3	k
ocasu	ocas	k1gInSc3	ocas
<g/>
)	)	kIx)	)
a	a	k8xC	a
zebrování	zebrování	k1gNnSc1	zebrování
na	na	k7c6	na
nohou	noha	k1gFnPc6	noha
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nemá	mít	k5eNaImIp3nS	mít
úhoří	úhoří	k2eAgInSc4d1	úhoří
pruh	pruh	k1gInSc4	pruh
<g/>
,	,	kIx,	,
uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
odchylka	odchylka	k1gFnSc1	odchylka
<g/>
.	.	kIx.	.
</s>
<s>
Koně	kůň	k1gMnPc1	kůň
mají	mít	k5eAaImIp3nP	mít
velice	velice	k6eAd1	velice
často	často	k6eAd1	často
vrozené	vrozený	k2eAgInPc4d1	vrozený
odznaky	odznak	k1gInPc4	odznak
(	(	kIx(	(
<g/>
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
např.	např.	kA	např.
hvězdu	hvězda	k1gFnSc4	hvězda
nebo	nebo	k8xC	nebo
lysinu	lysina	k1gFnSc4	lysina
<g/>
,	,	kIx,	,
či	či	k8xC	či
nohou	noha	k1gFnSc7	noha
např.	např.	kA	např.
bílou	bílý	k2eAgFnSc4d1	bílá
korunku	korunka	k1gFnSc4	korunka
<g/>
)	)	kIx)	)
a	a	k8xC	a
různé	různý	k2eAgFnPc4d1	různá
barvy	barva	k1gFnPc4	barva
kopyt	kopyto	k1gNnPc2	kopyto
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
tzv.	tzv.	kA	tzv.
získané	získaný	k2eAgInPc4d1	získaný
odznaky	odznak	k1gInPc4	odznak
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yIgInPc4	který
patří	patřit	k5eAaImIp3nP	patřit
například	například	k6eAd1	například
bílé	bílý	k2eAgFnPc1d1	bílá
skvrny	skvrna	k1gFnPc1	skvrna
po	po	k7c4	po
zranění	zranění	k1gNnPc4	zranění
<g/>
,	,	kIx,	,
jizvy	jizva	k1gFnPc4	jizva
<g/>
,	,	kIx,	,
výžehy	výžeh	k1gInPc4	výžeh
nebo	nebo	k8xC	nebo
tetování	tetování	k1gNnPc4	tetování
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
poslední	poslední	k2eAgInPc4d1	poslední
příklady	příklad	k1gInPc4	příklad
<g/>
,	,	kIx,	,
vytvořené	vytvořený	k2eAgFnPc4d1	vytvořená
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
identifikaci	identifikace	k1gFnSc3	identifikace
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
buňky	buňka	k1gFnSc2	buňka
koně	kůň	k1gMnSc2	kůň
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
32	[number]	k4	32
párů	pár	k1gInPc2	pár
chromozomů	chromozom	k1gInPc2	chromozom
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc1d1	základní
dělení	dělení	k1gNnSc1	dělení
plemen	plemeno	k1gNnPc2	plemeno
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
stupně	stupeň	k1gInSc2	stupeň
prošlechtění	prošlechtění	k1gNnSc2	prošlechtění
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
<g/>
:	:	kIx,	:
primitivní	primitivní	k2eAgNnPc1d1	primitivní
plemena	plemeno	k1gNnPc1	plemeno
-	-	kIx~	-
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
si	se	k3xPyFc3	se
původní	původní	k2eAgInSc4d1	původní
způsob	způsob	k1gInSc4	způsob
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
konstituci	konstituce	k1gFnSc4	konstituce
<g/>
,	,	kIx,	,
dospívají	dospívat	k5eAaImIp3nP	dospívat
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
dlouhověká	dlouhověký	k2eAgNnPc1d1	dlouhověké
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
mívají	mívat	k5eAaImIp3nP	mívat
menší	malý	k2eAgInSc4d2	menší
vzrůst	vzrůst	k1gInSc4	vzrůst
a	a	k8xC	a
bývají	bývat	k5eAaImIp3nP	bývat
velmi	velmi	k6eAd1	velmi
přizpůsobivá	přizpůsobivý	k2eAgNnPc1d1	přizpůsobivé
kulturní	kulturní	k2eAgNnPc1d1	kulturní
plemena	plemeno	k1gNnPc1	plemeno
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
<g />
.	.	kIx.	.
</s>
<s>
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
šlechtěna	šlechtěn	k2eAgFnSc1d1	šlechtěna
podle	podle	k7c2	podle
chovných	chovný	k2eAgInPc2d1	chovný
cílů	cíl	k1gInPc2	cíl
Podle	podle	k7c2	podle
původu	původ	k1gInSc2	původ
se	se	k3xPyFc4	se
koně	kůň	k1gMnPc1	kůň
dají	dát	k5eAaPmIp3nP	dát
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
Skupina	skupina	k1gFnSc1	skupina
koní	kůň	k1gMnPc2	kůň
mongolských	mongolský	k2eAgMnPc2d1	mongolský
(	(	kIx(	(
<g/>
stepních	stepní	k2eAgMnPc2d1	stepní
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
jejich	jejich	k3xOp3gInSc7	jejich
předkem	předek	k1gInSc7	předek
je	být	k5eAaImIp3nS	být
kůň	kůň	k1gMnSc1	kůň
Převalského	převalský	k2eAgInSc2d1	převalský
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nP	patřit
sem	sem	k6eAd1	sem
např.	např.	kA	např.
kirgizský	kirgizský	k2eAgMnSc1d1	kirgizský
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
baškirský	baškirský	k2eAgMnSc1d1	baškirský
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
sibiřský	sibiřský	k2eAgMnSc1d1	sibiřský
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
kazašský	kazašský	k2eAgMnSc1d1	kazašský
kůň	kůň	k1gMnSc1	kůň
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
kabardinský	kabardinský	k2eAgMnSc1d1	kabardinský
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
altajský	altajský	k2eAgMnSc1d1	altajský
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
Skupina	skupina	k1gFnSc1	skupina
koní	kůň	k1gMnPc2	kůň
východních	východní	k2eAgMnPc2d1	východní
(	(	kIx(	(
<g/>
orientálních	orientální	k2eAgMnPc2d1	orientální
<g/>
)	)	kIx)	)
-	-	kIx~	-
jejich	jejich	k3xOp3gInSc7	jejich
předkem	předek	k1gInSc7	předek
je	být	k5eAaImIp3nS	být
tarpan	tarpan	k1gMnSc1	tarpan
podskupina	podskupina	k1gFnSc1	podskupina
koní	kůň	k1gMnPc2	kůň
íránských	íránský	k2eAgMnPc2d1	íránský
-	-	kIx~	-
achatelkinský	achatelkinský	k2eAgMnSc1d1	achatelkinský
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
jomutský	jomutský	k2eAgMnSc1d1	jomutský
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
lokajský	lokajský	k2eAgMnSc1d1	lokajský
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
karabašský	karabašský	k2eAgMnSc1d1	karabašský
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
perský	perský	k2eAgMnSc1d1	perský
arab	arab	k1gMnSc1	arab
<g/>
,	,	kIx,	,
podskupina	podskupina	k1gFnSc1	podskupina
<g />
.	.	kIx.	.
</s>
<s>
koní	koní	k2eAgMnPc1d1	koní
tarpani	tarpan	k1gMnPc1	tarpan
-	-	kIx~	-
konik	konik	k1gMnSc1	konik
<g/>
,	,	kIx,	,
hucul	hucul	k1gMnSc1	hucul
<g/>
,	,	kIx,	,
...	...	k?	...
podskupina	podskupina	k1gFnSc1	podskupina
koní	kůň	k1gMnPc2	kůň
arabského	arabský	k2eAgInSc2d1	arabský
typu	typ	k1gInSc2	typ
-	-	kIx~	-
arabský	arabský	k2eAgMnSc1d1	arabský
plnokrevník	plnokrevník	k1gMnSc1	plnokrevník
<g/>
,	,	kIx,	,
berberský	berberský	k2eAgMnSc1d1	berberský
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
Shagya	Shagya	k1gMnSc1	Shagya
arab	arab	k1gMnSc1	arab
<g/>
,	,	kIx,	,
těrský	těrský	k1gMnSc1	těrský
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
španělský	španělský	k2eAgMnSc1d1	španělský
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
andaluský	andaluský	k2eAgMnSc1d1	andaluský
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
lipicán	lipicán	k1gMnSc1	lipicán
<g/>
,	,	kIx,	,
starokladrubský	starokladrubský	k2eAgMnSc1d1	starokladrubský
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
orlovský	orlovský	k2eAgMnSc1d1	orlovský
klusák	klusák	k1gMnSc1	klusák
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
quarter	quarter	k1gInSc1	quarter
horse	horse	k1gFnSc2	horse
<g/>
,	,	kIx,	,
american	american	k1gMnSc1	american
paint	paint	k1gMnSc1	paint
horse	horse	k6eAd1	horse
<g/>
,	,	kIx,	,
pinto	pinta	k1gFnSc5	pinta
<g/>
,	,	kIx,	,
appaloosa	appaloosa	k1gFnSc1	appaloosa
<g/>
,	,	kIx,	,
criollo	criolla	k1gFnSc5	criolla
<g/>
,	,	kIx,	,
passo	passa	k1gFnSc5	passa
fino	fino	k1gMnSc1	fino
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
jezdecký	jezdecký	k2eAgMnSc1d1	jezdecký
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
trakénský	trakénský	k1gMnSc1	trakénský
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
norfolk	norfolk	k1gMnSc1	norfolk
<g/>
,	,	kIx,	,
yorkshirský	yorkshirský	k2eAgMnSc1d1	yorkshirský
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
hunter	hunter	k1gMnSc1	hunter
<g/>
,	,	kIx,	,
morgan	morgan	k1gMnSc1	morgan
<g/>
,	,	kIx,	,
švédský	švédský	k2eAgMnSc1d1	švédský
teplokrevník	teplokrevník	k1gMnSc1	teplokrevník
<g/>
,	,	kIx,	,
brumby	brumb	k1gMnPc4	brumb
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
<g/>
...	...	k?	...
podskupina	podskupina	k1gFnSc1	podskupina
koní	kůň	k1gMnPc2	kůň
anglického	anglický	k2eAgInSc2d1	anglický
typu	typ	k1gInSc2	typ
-	-	kIx~	-
anglický	anglický	k2eAgMnSc1d1	anglický
plnokrevník	plnokrevník	k1gMnSc1	plnokrevník
<g/>
,	,	kIx,	,
angloarab	angloarab	k1gMnSc1	angloarab
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
teplokrevník	teplokrevník	k1gMnSc1	teplokrevník
<g/>
,	,	kIx,	,
hannoverský	hannoverský	k2eAgMnSc1d1	hannoverský
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
Skupina	skupina	k1gFnSc1	skupina
koní	kůň	k1gMnPc2	kůň
západních	západní	k2eAgMnPc2d1	západní
-	-	kIx~	-
jejich	jejich	k3xOp3gInSc7	jejich
předkem	předek	k1gInSc7	předek
je	být	k5eAaImIp3nS	být
kůň	kůň	k1gMnSc1	kůň
západního	západní	k2eAgInSc2d1	západní
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
již	již	k6eAd1	již
vyhynul	vyhynout	k5eAaPmAgInS	vyhynout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
sem	sem	k6eAd1	sem
např.	např.	kA	např.
norik	norik	k1gMnSc1	norik
<g/>
,	,	kIx,	,
hafling	hafling	k1gInSc1	hafling
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
belgický	belgický	k2eAgMnSc1d1	belgický
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
peršeron	peršeron	k1gMnSc1	peršeron
<g/>
,	,	kIx,	,
clydesdalský	clydesdalský	k2eAgMnSc1d1	clydesdalský
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
shirský	shirský	k2eAgMnSc1d1	shirský
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
torijský	torijský	k2eAgMnSc1d1	torijský
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
bitjug	bitjug	k1gMnSc1	bitjug
<g/>
,	,	kIx,	,
...	...	k?	...
Skupina	skupina	k1gFnSc1	skupina
koní	kůň	k1gMnPc2	kůň
severských	severský	k2eAgMnPc2d1	severský
(	(	kIx(	(
<g/>
nordických	nordický	k2eAgMnPc2d1	nordický
<g/>
)	)	kIx)	)
-	-	kIx~	-
např.	např.	kA	např.
fjordský	fjordský	k2eAgMnSc1d1	fjordský
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
pony	pony	k1gMnSc1	pony
(	(	kIx(	(
<g/>
shetlandský	shetlandský	k2eAgMnSc1d1	shetlandský
pony	pony	k1gMnSc1	pony
<g/>
,	,	kIx,	,
dartmoorský	dartmoorský	k2eAgMnSc1d1	dartmoorský
pony	pony	k1gMnSc1	pony
<g/>
,	,	kIx,	,
exmoorský	exmoorský	k2eAgMnSc1d1	exmoorský
pony	pony	k1gMnSc1	pony
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
velšský	velšský	k2eAgMnSc1d1	velšský
pony	pony	k1gMnSc1	pony
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
connemara	connemara	k1gFnSc1	connemara
<g/>
,	,	kIx,	,
...	...	k?	...
Polde	Pold	k1gInSc5	Pold
temperamentu	temperament	k1gInSc3	temperament
<g/>
:	:	kIx,	:
Chladnokrevní	chladnokrevný	k2eAgMnPc1d1	chladnokrevný
koně	kůň	k1gMnPc1	kůň
-	-	kIx~	-
odvozený	odvozený	k2eAgMnSc1d1	odvozený
od	od	k7c2	od
koně	kůň	k1gMnSc4	kůň
západního	západní	k2eAgMnSc4d1	západní
<g/>
,	,	kIx,	,
s	s	k7c7	s
mohutnou	mohutný	k2eAgFnSc7d1	mohutná
stavbou	stavba	k1gFnSc7	stavba
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
klidní	klidnit	k5eAaImIp3nS	klidnit
<g/>
,	,	kIx,	,
s	s	k7c7	s
dobrým	dobrý	k2eAgInSc7d1	dobrý
charakterem	charakter	k1gInSc7	charakter
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
užíváni	užíván	k2eAgMnPc1d1	užíván
jako	jako	k8xS	jako
tažní	tažní	k2eAgMnPc1d1	tažní
-	-	kIx~	-
belgický	belgický	k2eAgMnSc1d1	belgický
<g/>
,	,	kIx,	,
norický	norický	k2eAgMnSc1d1	norický
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
peršeronský	peršeronský	k2eAgMnSc1d1	peršeronský
<g/>
,	,	kIx,	,
shirský	shirský	k2eAgMnSc1d1	shirský
Teplokrevní	teplokrevný	k2eAgMnPc1d1	teplokrevný
koně	kůň	k1gMnPc1	kůň
-	-	kIx~	-
odvozený	odvozený	k2eAgMnSc1d1	odvozený
od	od	k7c2	od
koně	kůň	k1gMnSc4	kůň
východního	východní	k2eAgMnSc4d1	východní
<g/>
,	,	kIx,	,
s	s	k7c7	s
ušlechtilejší	ušlechtilý	k2eAgFnSc7d2	ušlechtilejší
stavbou	stavba	k1gFnSc7	stavba
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
živý	živý	k1gMnSc1	živý
<g/>
,	,	kIx,	,
užívaní	užívaný	k2eAgMnPc1d1	užívaný
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
pod	pod	k7c7	pod
sedlem	sedlo	k1gNnSc7	sedlo
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
všestranně	všestranně	k6eAd1	všestranně
užitkoví	užitkový	k2eAgMnPc1d1	užitkový
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
sport	sport	k1gInSc4	sport
-	-	kIx~	-
<g/>
český	český	k2eAgMnSc1d1	český
teplokrevník	teplokrevník	k1gMnSc1	teplokrevník
<g/>
,	,	kIx,	,
hannoverský	hannoverský	k2eAgMnSc1d1	hannoverský
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
huculský	huculský	k2eAgMnSc1d1	huculský
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
starokladrubský	starokladrubský	k2eAgMnSc1d1	starokladrubský
<g />
.	.	kIx.	.
</s>
<s>
kůň	kůň	k1gMnSc1	kůň
Plnokrevní	plnokrevný	k2eAgMnPc1d1	plnokrevný
koně	kůň	k1gMnPc1	kůň
-	-	kIx~	-
staré	starý	k2eAgNnSc4d1	staré
ušlechtilé	ušlechtilý	k2eAgNnSc4d1	ušlechtilé
plemeno	plemeno	k1gNnSc4	plemeno
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
si	se	k3xPyFc3	se
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
svůj	svůj	k3xOyFgInSc4	svůj
původní	původní	k2eAgInSc4d1	původní
ráz	ráz	k1gInSc4	ráz
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
příměsi	příměs	k1gFnSc2	příměs
cizí	cizí	k2eAgFnSc2d1	cizí
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
teplokrevník	teplokrevník	k1gMnSc1	teplokrevník
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
šlechtěný	šlechtěný	k2eAgMnSc1d1	šlechtěný
na	na	k7c4	na
určitou	určitý	k2eAgFnSc4d1	určitá
užitkovost	užitkovost	k1gFnSc4	užitkovost
s	s	k7c7	s
takovým	takový	k3xDgInSc7	takový
stupněm	stupeň	k1gInSc7	stupeň
prošlechtění	prošlechtění	k1gNnPc2	prošlechtění
<g/>
,	,	kIx,	,
že	že	k8xS	že
požadované	požadovaný	k2eAgFnPc1d1	požadovaná
vlastnosti	vlastnost	k1gFnPc1	vlastnost
už	už	k6eAd1	už
nejdou	jít	k5eNaImIp3nP	jít
dalším	další	k2eAgNnSc7d1	další
přikřižováním	přikřižování	k1gNnSc7	přikřižování
zlepšovat-	zlepšovat-	k?	zlepšovat-
arabský	arabský	k2eAgMnSc1d1	arabský
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
plnokrevník	plnokrevník	k1gMnSc1	plnokrevník
<g/>
,	,	kIx,	,
achaltekinský	achaltekinský	k2eAgMnSc1d1	achaltekinský
kůň	kůň	k1gMnSc1	kůň
Polokrevní	polokrevný	k2eAgMnPc1d1	polokrevný
koně	kůň	k1gMnPc4	kůň
-	-	kIx~	-
kříženci	kříženec	k1gMnPc7	kříženec
plnokrevních	plnokrevní	k2eAgMnPc2d1	plnokrevní
koní	kůň	k1gMnPc2	kůň
s	s	k7c7	s
teplokrevnými	teplokrevný	k2eAgMnPc7d1	teplokrevný
-	-	kIx~	-
arabofríský	arabofríský	k2eAgMnSc1d1	arabofríský
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
Hispanoarab	Hispanoarab	k1gInSc1	Hispanoarab
Sedlo	sedlo	k1gNnSc1	sedlo
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
jezdcovu	jezdcův	k2eAgFnSc4d1	jezdcova
váhu	váha	k1gFnSc4	váha
po	po	k7c6	po
koňském	koňský	k2eAgInSc6d1	koňský
hřbetu	hřbet	k1gInSc6	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Různá	různý	k2eAgNnPc1d1	různé
sedla	sedlo	k1gNnPc1	sedlo
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
různým	různý	k2eAgInPc3d1	různý
účelům	účel	k1gInPc3	účel
(	(	kIx(	(
<g/>
Univerzální	univerzální	k2eAgNnSc1d1	univerzální
sedlo	sedlo	k1gNnSc1	sedlo
<g/>
,	,	kIx,	,
skokové	skokový	k2eAgNnSc1d1	skokové
sedlo	sedlo	k1gNnSc1	sedlo
<g/>
,	,	kIx,	,
dostihové	dostihový	k2eAgNnSc1d1	dostihové
sedlo	sedlo	k1gNnSc1	sedlo
<g/>
,	,	kIx,	,
drezurní	drezurní	k2eAgNnSc1d1	drezurní
sedlo	sedlo	k1gNnSc1	sedlo
<g/>
,	,	kIx,	,
westernové	westernový	k2eAgNnSc1d1	westernové
sedlo	sedlo	k1gNnSc1	sedlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c4	pod
sedlo	sedlo	k1gNnSc4	sedlo
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
umisťuje	umisťovat	k5eAaImIp3nS	umisťovat
sedlová	sedlový	k2eAgFnSc1d1	sedlová
poduška	poduška	k1gFnSc1	poduška
<g/>
.	.	kIx.	.
</s>
<s>
Podbřišník	podbřišník	k1gInSc1	podbřišník
sedlo	sedlo	k1gNnSc1	sedlo
udržuje	udržovat	k5eAaImIp3nS	udržovat
na	na	k7c6	na
hřbetu	hřbet	k1gInSc6	hřbet
koně	kůň	k1gMnSc2	kůň
<g/>
,	,	kIx,	,
probíhá	probíhat	k5eAaImIp3nS	probíhat
za	za	k7c7	za
předníma	přední	k2eAgFnPc7d1	přední
nohama	noha	k1gFnPc7	noha
koně	kůň	k1gMnSc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Třmeny	třmen	k1gInPc1	třmen
jsou	být	k5eAaImIp3nP	být
připevněny	připevněn	k2eAgInPc1d1	připevněn
k	k	k7c3	k
sedlu	sedlo	k1gNnSc3	sedlo
<g/>
,	,	kIx,	,
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
stabilitu	stabilita	k1gFnSc4	stabilita
jezdce	jezdec	k1gMnSc2	jezdec
a	a	k8xC	a
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
při	při	k7c6	při
tzv.	tzv.	kA	tzv.
lehkých	lehký	k2eAgInPc6d1	lehký
sedech	sed	k1gInPc6	sed
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
krocích	krok	k1gInPc6	krok
<g/>
.	.	kIx.	.
</s>
<s>
Uzdečka	uzdečka	k1gFnSc1	uzdečka
se	se	k3xPyFc4	se
připevňuje	připevňovat	k5eAaImIp3nS	připevňovat
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
koně	kůň	k1gMnSc2	kůň
a	a	k8xC	a
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
jezdci	jezdec	k1gMnSc3	jezdec
nad	nad	k7c7	nad
koněm	kůň	k1gMnSc7	kůň
udržet	udržet	k5eAaPmF	udržet
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
nátylníku	nátylník	k1gInSc2	nátylník
<g/>
,	,	kIx,	,
podhrdelníku	podhrdelník	k1gInSc2	podhrdelník
<g/>
,	,	kIx,	,
čelenky	čelenka	k1gFnSc2	čelenka
<g/>
,	,	kIx,	,
lícnic	lícnice	k1gFnPc2	lícnice
<g/>
,	,	kIx,	,
otěží	otěž	k1gFnPc2	otěž
<g/>
,	,	kIx,	,
nánosníku	nánosník	k1gInSc2	nánosník
a	a	k8xC	a
udidla	udidlo	k1gNnSc2	udidlo
<g/>
.	.	kIx.	.
</s>
<s>
Uzda	uzda	k1gFnSc1	uzda
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
přehlídkách	přehlídka	k1gFnPc6	přehlídka
a	a	k8xC	a
drezurách	drezura	k1gFnPc6	drezura
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
uzdečky	uzdečka	k1gFnSc2	uzdečka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
využívána	využívat	k5eAaPmNgFnS	využívat
mnohem	mnohem	k6eAd1	mnohem
častěji	často	k6eAd2	často
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
navíc	navíc	k6eAd1	navíc
jednu	jeden	k4xCgFnSc4	jeden
lícnici	lícnice	k1gFnSc4	lícnice
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
udidlu	udidlo	k1gNnSc3	udidlo
(	(	kIx(	(
<g/>
uzdečka	uzdečka	k1gFnSc1	uzdečka
má	mít	k5eAaImIp3nS	mít
udidlo	udidlo	k1gNnSc4	udidlo
jen	jen	k9	jen
jedno	jeden	k4xCgNnSc1	jeden
<g/>
,	,	kIx,	,
uzda	uzda	k1gFnSc1	uzda
dvě	dva	k4xCgFnPc4	dva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Udidlo	udidlo	k1gNnSc1	udidlo
se	se	k3xPyFc4	se
umisťuje	umisťovat	k5eAaImIp3nS	umisťovat
do	do	k7c2	do
huby	huba	k1gFnSc2	huba
koně	kůň	k1gMnSc2	kůň
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
jich	on	k3xPp3gMnPc2	on
několik	několik	k4yIc1	několik
druhů	druh	k1gMnPc2	druh
(	(	kIx(	(
<g/>
jednou	jednou	k6eAd1	jednou
<g/>
,	,	kIx,	,
dvakrát	dvakrát	k6eAd1	dvakrát
lomené	lomený	k2eAgNnSc1d1	lomené
udidlo	udidlo	k1gNnSc1	udidlo
<g/>
,	,	kIx,	,
rovné	rovný	k2eAgNnSc1d1	rovné
udidlo	udidlo	k1gNnSc1	udidlo
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
varianty	varianta	k1gFnPc4	varianta
bez	bez	k7c2	bez
udidel	udidla	k1gNnPc2	udidla
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
kůň	kůň	k1gMnSc1	kůň
ovládá	ovládat	k5eAaImIp3nS	ovládat
tlakem	tlak	k1gInSc7	tlak
na	na	k7c4	na
nos	nos	k1gInSc4	nos
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
udidla	udidla	k1gNnPc1	udidla
s	s	k7c7	s
pákami	páka	k1gFnPc7	páka
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
martingaly	martingal	k1gInPc7	martingal
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
koni	kůň	k1gMnPc1	kůň
brání	bránit	k5eAaImIp3nP	bránit
ve	v	k7c6	v
zdvihání	zdvihání	k1gNnSc6	zdvihání
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
upevněny	upevnit	k5eAaPmNgInP	upevnit
nad	nad	k7c7	nad
kohoutkem	kohoutek	k1gInSc7	kohoutek
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
krku	krk	k1gInSc2	krk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
podbřišníku	podbřišník	k1gInSc3	podbřišník
a	a	k8xC	a
k	k	k7c3	k
otěžím	otěž	k1gFnPc3	otěž
<g/>
.	.	kIx.	.
</s>
<s>
Kamaše	kamaše	k1gFnPc1	kamaše
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
nohou	noha	k1gFnPc2	noha
koně	kůň	k1gMnPc1	kůň
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
<g/>
,	,	kIx,	,
skákání	skákání	k1gNnSc6	skákání
i	i	k8xC	i
přepravě	přeprava	k1gFnSc6	přeprava
<g/>
.	.	kIx.	.
</s>
<s>
Koně	kůň	k1gMnPc1	kůň
jako	jako	k9	jako
prapůvodní	prapůvodní	k2eAgMnPc4d1	prapůvodní
obyvatele	obyvatel	k1gMnPc4	obyvatel
stepí	step	k1gFnPc2	step
a	a	k8xC	a
lesostepí	lesostep	k1gFnPc2	lesostep
byli	být	k5eAaImAgMnP	být
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
ustájeni	ustájen	k2eAgMnPc1d1	ustájen
ve	v	k7c6	v
stájích	stáj	k1gFnPc6	stáj
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
boxech	box	k1gInPc6	box
nebo	nebo	k8xC	nebo
stáních	stání	k1gNnPc6	stání
<g/>
.	.	kIx.	.
</s>
<s>
Box	box	k1gInSc1	box
je	být	k5eAaImIp3nS	být
nejvhodnější	vhodný	k2eAgMnSc1d3	nejvhodnější
ze	z	k7c2	z
stájových	stájový	k2eAgInPc2d1	stájový
typů	typ	k1gInPc2	typ
ustájení	ustájení	k1gNnSc2	ustájení
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
kůň	kůň	k1gMnSc1	kůň
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
vyhrazený	vyhrazený	k2eAgInSc4d1	vyhrazený
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
v	v	k7c6	v
boxu	box	k1gInSc6	box
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
lehání	lehání	k1gNnSc4	lehání
a	a	k8xC	a
volný	volný	k2eAgInSc4d1	volný
pohyb	pohyb	k1gInSc4	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Umístění	umístění	k1gNnSc1	umístění
ve	v	k7c6	v
stáních	stání	k1gNnPc6	stání
není	být	k5eNaImIp3nS	být
tak	tak	k9	tak
pohodlné	pohodlný	k2eAgNnSc1d1	pohodlné
jako	jako	k8xS	jako
v	v	k7c6	v
boxech	box	k1gInPc6	box
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
etologického	etologický	k2eAgNnSc2d1	etologické
hlediska	hledisko	k1gNnSc2	hledisko
šetří	šetřit	k5eAaImIp3nS	šetřit
psychické	psychický	k2eAgNnSc4d1	psychické
zdraví	zdraví	k1gNnSc4	zdraví
koní	kůň	k1gMnPc2	kůň
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
blíž	blízce	k6eAd2	blízce
u	u	k7c2	u
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
vidí	vidět	k5eAaImIp3nS	vidět
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
slyší	slyšet	k5eAaImIp3nP	slyšet
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
možné	možný	k2eAgNnSc1d1	možné
i	i	k9	i
u	u	k7c2	u
boxového	boxový	k2eAgInSc2d1	boxový
typu	typ	k1gInSc2	typ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Frekvence	frekvence	k1gFnSc1	frekvence
stájových	stájový	k2eAgInPc2d1	stájový
zlozvyků	zlozvyk	k1gInPc2	zlozvyk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
okusování	okusování	k1gNnSc1	okusování
žlabu	žlab	k1gInSc2	žlab
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
však	však	k9	však
ve	v	k7c6	v
vazném	vazný	k2eAgNnSc6d1	vazné
ustájení	ustájení	k1gNnSc6	ustájení
větší	veliký	k2eAgFnPc1d2	veliký
než	než	k8xS	než
v	v	k7c6	v
ustájení	ustájení	k1gNnSc6	ustájení
boxovém	boxový	k2eAgNnSc6d1	boxové
<g/>
.	.	kIx.	.
</s>
<s>
Koně	kůň	k1gMnPc1	kůň
jsou	být	k5eAaImIp3nP	být
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
jsou	být	k5eAaImIp3nP	být
přivázáni	přivázán	k2eAgMnPc1d1	přivázán
hlavou	hlava	k1gFnSc7	hlava
ke	k	k7c3	k
zdi	zeď	k1gFnSc3	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Stání	stání	k1gNnSc1	stání
není	být	k5eNaImIp3nS	být
tak	tak	k9	tak
prostorově	prostorově	k6eAd1	prostorově
náročné	náročný	k2eAgInPc4d1	náročný
jako	jako	k9	jako
boxy	box	k1gInPc4	box
a	a	k8xC	a
ušetří	ušetřit	k5eAaPmIp3nS	ušetřit
se	se	k3xPyFc4	se
práce	práce	k1gFnSc1	práce
a	a	k8xC	a
podestýlka	podestýlka	k1gFnSc1	podestýlka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
trpí	trpět	k5eAaImIp3nP	trpět
tu	tu	k6eAd1	tu
zdraví	zdravý	k2eAgMnPc1d1	zdravý
koně	kůň	k1gMnPc1	kůň
díky	díky	k7c3	díky
nedostatku	nedostatek	k1gInSc3	nedostatek
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Podestýlkou	podestýlka	k1gFnSc7	podestýlka
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
sláma	sláma	k1gFnSc1	sláma
<g/>
,	,	kIx,	,
hobliny	hoblina	k1gFnPc1	hoblina
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
i	i	k9	i
rašelina	rašelina	k1gFnSc1	rašelina
<g/>
,	,	kIx,	,
krouhaný	krouhaný	k2eAgInSc1d1	krouhaný
papír	papír	k1gInSc1	papír
<g/>
,	,	kIx,	,
piliny	pilina	k1gFnPc1	pilina
či	či	k8xC	či
jen	jen	k9	jen
gumová	gumový	k2eAgFnSc1d1	gumová
podložka	podložka	k1gFnSc1	podložka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
venkovním	venkovní	k2eAgNnSc6d1	venkovní
ustájení	ustájení	k1gNnSc6	ustájení
se	se	k3xPyFc4	se
kůň	kůň	k1gMnSc1	kůň
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
volně	volně	k6eAd1	volně
po	po	k7c6	po
výběhu	výběh	k1gInSc6	výběh
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nS	bát
<g/>
,	,	kIx,	,
že	že	k8xS	že
koně	kůň	k1gMnPc1	kůň
nastydnou	nastydnout	k5eAaPmIp3nP	nastydnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kůň	kůň	k1gMnSc1	kůň
uvyklý	uvyklý	k2eAgMnSc1d1	uvyklý
pobytu	pobyt	k1gInSc2	pobyt
venku	venku	k6eAd1	venku
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
mění	měnit	k5eAaImIp3nS	měnit
srst	srst	k1gFnSc4	srst
(	(	kIx(	(
<g/>
v	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
případě	případ	k1gInSc6	případ
mu	on	k3xPp3gMnSc3	on
majitel	majitel	k1gMnSc1	majitel
pořídí	pořídit	k5eAaPmIp3nS	pořídit
venkovní	venkovní	k2eAgFnSc4d1	venkovní
pokrývku	pokrývka	k1gFnSc4	pokrývka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
kůň	kůň	k1gMnSc1	kůň
je	být	k5eAaImIp3nS	být
zvíře	zvíře	k1gNnSc4	zvíře
žijící	žijící	k2eAgNnSc4d1	žijící
ve	v	k7c6	v
stepích	step	k1gFnPc6	step
<g/>
,	,	kIx,	,
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
životních	životní	k2eAgFnPc2d1	životní
podmínek	podmínka	k1gFnPc2	podmínka
by	by	kYmCp3nS	by
musel	muset	k5eAaImAgInS	muset
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
nachodit	nachodit	k5eAaBmF	nachodit
mnoho	mnoho	k4c4	mnoho
kilometrů	kilometr	k1gInPc2	kilometr
(	(	kIx(	(
<g/>
za	za	k7c7	za
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
potravou	potrava	k1gFnSc7	potrava
<g/>
,	,	kIx,	,
závětřím	závětří	k1gNnSc7	závětří
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
koně	kůň	k1gMnPc1	kůň
alespoň	alespoň	k9	alespoň
několikrát	několikrát	k6eAd1	několikrát
týdně	týdně	k6eAd1	týdně
pouštějí	pouštět	k5eAaImIp3nP	pouštět
do	do	k7c2	do
výběhu	výběh	k1gInSc2	výběh
s	s	k7c7	s
přístřeškem	přístřešek	k1gInSc7	přístřešek
<g/>
,	,	kIx,	,
nejlépe	dobře	k6eAd3	dobře
se	s	k7c7	s
zpevněnou	zpevněný	k2eAgFnSc7d1	zpevněná
podlahou	podlaha	k1gFnSc7	podlaha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
kůň	kůň	k1gMnSc1	kůň
může	moct	k5eAaImIp3nS	moct
ukrýt	ukrýt	k5eAaPmF	ukrýt
před	před	k7c7	před
nepříznivým	příznivý	k2eNgNnSc7d1	nepříznivé
počasím	počasí	k1gNnSc7	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Koně	kůň	k1gMnPc1	kůň
je	on	k3xPp3gInPc4	on
třeba	třeba	k6eAd1	třeba
čistit	čistit	k5eAaImF	čistit
vícekrát	vícekrát	k6eAd1	vícekrát
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
především	především	k9	především
před	před	k7c7	před
ježděním	ježdění	k1gNnSc7	ježdění
a	a	k8xC	a
po	po	k7c6	po
něm.	něm.	k?	něm.
Čistění	čistění	k1gNnSc6	čistění
má	mít	k5eAaImIp3nS	mít
většinou	většinou	k6eAd1	většinou
několik	několik	k4yIc4	několik
fází	fáze	k1gFnPc2	fáze
<g/>
:	:	kIx,	:
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
hrubým	hrubý	k2eAgNnSc7d1	hrubé
očištěním	očištění	k1gNnSc7	očištění
rýžákem	rýžák	k1gInSc7	rýžák
či	či	k8xC	či
hřbílkem	hřbílek	k1gInSc7	hřbílek
<g/>
,	,	kIx,	,
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
se	se	k3xPyFc4	se
měkkým	měkký	k2eAgInSc7d1	měkký
kartáčem	kartáč	k1gInSc7	kartáč
a	a	k8xC	a
dokončuje	dokončovat	k5eAaImIp3nS	dokončovat
vyčesáním	vyčesání	k1gNnSc7	vyčesání
hřívy	hříva	k1gFnSc2	hříva
a	a	k8xC	a
ocasu	ocas	k1gInSc2	ocas
či	či	k8xC	či
natřením	natření	k1gNnPc3	natření
kopyt	kopyto	k1gNnPc2	kopyto
olejem	olej	k1gInSc7	olej
<g/>
.	.	kIx.	.
</s>
<s>
Čištění	čištění	k1gNnSc1	čištění
se	se	k3xPyFc4	se
zahajuje	zahajovat	k5eAaImIp3nS	zahajovat
hřbílkem	hřbílek	k1gInSc7	hřbílek
(	(	kIx(	(
<g/>
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
destička	destička	k1gFnSc1	destička
s	s	k7c7	s
výstupky	výstupek	k1gInPc7	výstupek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
nástrojem	nástroj	k1gInSc7	nástroj
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
kruzích	kruh	k1gInPc6	kruh
i	i	k9	i
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
růstu	růst	k1gInSc2	růst
srsti	srst	k1gFnSc2	srst
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
se	se	k3xPyFc4	se
od	od	k7c2	od
krku	krk	k1gInSc2	krk
a	a	k8xC	a
postupuje	postupovat	k5eAaImIp3nS	postupovat
k	k	k7c3	k
ocasu	ocas	k1gInSc3	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Hřbílkem	hřbílko	k1gNnSc7	hřbílko
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
nemělo	mít	k5eNaImAgNnS	mít
přejíždět	přejíždět	k5eAaImF	přejíždět
přes	přes	k7c4	přes
klouby	kloub	k1gInPc4	kloub
končetin	končetina	k1gFnPc2	končetina
a	a	k8xC	a
kohoutek	kohoutek	k1gInSc1	kohoutek
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
nečistota	nečistota	k1gFnSc1	nečistota
<g/>
,	,	kIx,	,
stačí	stačit	k5eAaBmIp3nS	stačit
ji	on	k3xPp3gFnSc4	on
jen	jen	k9	jen
jemně	jemně	k6eAd1	jemně
pomocí	pomocí	k7c2	pomocí
hřbílka	hřbílko	k1gNnSc2	hřbílko
odrolit	odrolit	k5eAaPmF	odrolit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
čištění	čištění	k1gNnSc1	čištění
hrubým	hrubý	k2eAgInSc7d1	hrubý
kartáčem	kartáč	k1gInSc7	kartáč
(	(	kIx(	(
<g/>
rýžákem	rýžák	k1gInSc7	rýžák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
se	se	k3xPyFc4	se
postupuje	postupovat	k5eAaImIp3nS	postupovat
od	od	k7c2	od
krku	krk	k1gInSc2	krk
koně	kůň	k1gMnPc4	kůň
k	k	k7c3	k
ocasu	ocas	k1gInSc2	ocas
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
kloubů	kloub	k1gInPc2	kloub
<g/>
,	,	kIx,	,
kohoutku	kohoutek	k1gInSc2	kohoutek
a	a	k8xC	a
břicha	břicho	k1gNnSc2	břicho
koně	kůň	k1gMnSc2	kůň
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
po	po	k7c6	po
směru	směr	k1gInSc6	směr
růstu	růst	k1gInSc2	růst
srsti	srst	k1gFnSc3	srst
a	a	k8xC	a
od	od	k7c2	od
shora	shora	k6eAd1	shora
dolů	dol	k1gInPc2	dol
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc4d1	jediná
výjimku	výjimka	k1gFnSc4	výjimka
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
pravidla	pravidlo	k1gNnSc2	pravidlo
tvoří	tvořit	k5eAaImIp3nS	tvořit
místo	místo	k1gNnSc4	místo
nad	nad	k7c7	nad
slabinami	slabina	k1gFnPc7	slabina
koně	kůň	k1gMnSc2	kůň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
srst	srst	k1gFnSc1	srst
tvoří	tvořit	k5eAaImIp3nS	tvořit
hvězdicovitý	hvězdicovitý	k2eAgInSc4d1	hvězdicovitý
útvar	útvar	k1gInSc4	útvar
-	-	kIx~	-
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
čistí	čistit	k5eAaImIp3nS	čistit
srst	srst	k1gFnSc1	srst
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
směrech	směr	k1gInPc6	směr
a	a	k8xC	a
na	na	k7c4	na
nástroj	nástroj	k1gInSc4	nástroj
se	se	k3xPyFc4	se
netlačí	tlačit	k5eNaImIp3nS	tlačit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
citlivé	citlivý	k2eAgNnSc4d1	citlivé
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
mohlo	moct	k5eAaImAgNnS	moct
by	by	kYmCp3nP	by
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
poplašení	poplašení	k1gNnSc3	poplašení
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Tahy	tah	k1gInPc1	tah
rýžákem	rýžák	k1gInSc7	rýžák
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
pohyby	pohyb	k1gInPc1	pohyb
vedené	vedený	k2eAgInPc1d1	vedený
celou	celý	k2eAgFnSc7d1	celá
paží	paže	k1gFnSc7	paže
rytmicky	rytmicky	k6eAd1	rytmicky
<g/>
,	,	kIx,	,
plynule	plynule	k6eAd1	plynule
a	a	k8xC	a
silně	silně	k6eAd1	silně
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
tazích	tag	k1gInPc6	tag
se	se	k3xPyFc4	se
rýžák	rýžák	k1gInSc1	rýžák
vždy	vždy	k6eAd1	vždy
vyčistí	vyčistit	k5eAaPmIp3nS	vyčistit
o	o	k7c4	o
připravené	připravený	k2eAgNnSc4d1	připravené
kovové	kovový	k2eAgNnSc4d1	kovové
hřbílko	hřbílko	k1gNnSc4	hřbílko
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
se	se	k3xPyFc4	se
rýžákem	rýžák	k1gInSc7	rýžák
nečistí	čistit	k5eNaImIp3nS	čistit
<g/>
,	,	kIx,	,
v	v	k7c6	v
břišní	břišní	k2eAgFnSc6d1	břišní
oblasti	oblast	k1gFnSc6	oblast
mezi	mezi	k7c7	mezi
zadními	zadní	k2eAgFnPc7d1	zadní
končetinami	končetina	k1gFnPc7	končetina
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
čistit	čistit	k5eAaImF	čistit
opatrně	opatrně	k6eAd1	opatrně
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k1gNnSc7	další
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
je	být	k5eAaImIp3nS	být
měkký	měkký	k2eAgInSc1d1	měkký
kartáč	kartáč	k1gInSc1	kartáč
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
od	od	k7c2	od
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
růstu	růst	k1gInSc2	růst
srsti	srst	k1gFnSc2	srst
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
očí	oko	k1gNnPc2	oko
se	se	k3xPyFc4	se
čistí	čistit	k5eAaImIp3nS	čistit
opatrně	opatrně	k6eAd1	opatrně
<g/>
,	,	kIx,	,
nozdry	nozdra	k1gFnPc1	nozdra
se	se	k3xPyFc4	se
vynechávají	vynechávat	k5eAaImIp3nP	vynechávat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
hlavy	hlava	k1gFnSc2	hlava
se	se	k3xPyFc4	se
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
k	k	k7c3	k
ocasu	ocas	k1gInSc3	ocas
koně	kůň	k1gMnSc2	kůň
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tahy	tah	k1gInPc4	tah
tímto	tento	k3xDgInSc7	tento
kartáčem	kartáč	k1gInSc7	kartáč
jsou	být	k5eAaImIp3nP	být
krátké	krátká	k1gFnPc1	krátká
s	s	k7c7	s
vytočením	vytočení	k1gNnSc7	vytočení
zápěstí	zápěstí	k1gNnSc2	zápěstí
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
postupuje	postupovat	k5eAaImIp3nS	postupovat
zepředu	zepředu	k6eAd1	zepředu
dozadu	dozadu	k6eAd1	dozadu
a	a	k8xC	a
shora	shora	k6eAd1	shora
dolů	dolů	k6eAd1	dolů
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nečistota	nečistota	k1gFnSc1	nečistota
a	a	k8xC	a
prach	prach	k1gInSc4	prach
nepadaly	padat	k5eNaImAgFnP	padat
na	na	k7c4	na
již	již	k6eAd1	již
vyčištěné	vyčištěný	k2eAgFnPc4d1	vyčištěná
plochy	plocha	k1gFnPc4	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Měkký	měkký	k2eAgInSc1d1	měkký
kartáč	kartáč	k1gInSc1	kartáč
se	se	k3xPyFc4	se
čistí	čistit	k5eAaImIp3nS	čistit
o	o	k7c4	o
kovové	kovový	k2eAgNnSc4d1	kovové
hřbílko	hřbílko	k1gNnSc4	hřbílko
častěji	často	k6eAd2	často
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
kůň	kůň	k1gMnSc1	kůň
takto	takto	k6eAd1	takto
vyčištěn	vyčistit	k5eAaPmNgMnS	vyčistit
<g/>
,	,	kIx,	,
použije	použít	k5eAaPmIp3nS	použít
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
hadřík	hadřík	k1gInSc1	hadřík
na	na	k7c4	na
vytření	vytření	k1gNnSc4	vytření
nozder	nozdra	k1gFnPc2	nozdra
a	a	k8xC	a
jiný	jiný	k2eAgInSc1d1	jiný
na	na	k7c4	na
podocasní	podocasní	k2eAgFnSc4d1	podocasní
část	část	k1gFnSc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
rozčeše	rozčesat	k5eAaPmIp3nS	rozčesat
hřebenem	hřeben	k1gInSc7	hřeben
hříva	hříva	k1gFnSc1	hříva
a	a	k8xC	a
ocas	ocas	k1gInSc1	ocas
a	a	k8xC	a
na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
se	se	k3xPyFc4	se
kopytním	kopytní	k2eAgInSc7d1	kopytní
háčkem	háček	k1gInSc7	háček
pročistí	pročistit	k5eAaPmIp3nP	pročistit
kopyta	kopyto	k1gNnPc1	kopyto
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jeho	jeho	k3xOp3gNnSc6	jeho
používání	používání	k1gNnSc6	používání
se	se	k3xPyFc4	se
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
tvar	tvar	k1gInSc1	tvar
podkovy	podkova	k1gFnSc2	podkova
a	a	k8xC	a
práce	práce	k1gFnSc2	práce
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
velmi	velmi	k6eAd1	velmi
opatrně	opatrně	k6eAd1	opatrně
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
při	při	k7c6	při
ní	on	k3xPp3gFnSc6	on
kůň	kůň	k1gMnSc1	kůň
nezranil	zranit	k5eNaPmAgMnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
dávat	dávat	k5eAaImF	dávat
pozor	pozor	k1gInSc4	pozor
na	na	k7c4	na
střelku	střelka	k1gFnSc4	střelka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
kůň	kůň	k1gMnSc1	kůň
velmi	velmi	k6eAd1	velmi
citlivý	citlivý	k2eAgMnSc1d1	citlivý
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
úplný	úplný	k2eAgInSc4d1	úplný
lesk	lesk	k1gInSc4	lesk
lze	lze	k6eAd1	lze
na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
koně	kůň	k1gMnSc4	kůň
přetřít	přetřít	k5eAaPmF	přetřít
hadříkem	hadřík	k1gInSc7	hadřík
a	a	k8xC	a
vazelínou	vazelína	k1gFnSc7	vazelína
nebo	nebo	k8xC	nebo
olejem	olej	k1gInSc7	olej
natřít	natřít	k5eAaPmF	natřít
kopyta	kopyto	k1gNnPc4	kopyto
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgInSc1d1	anglický
styl	styl	k1gInSc1	styl
ježdění	ježdění	k1gNnSc2	ježdění
používá	používat	k5eAaImIp3nS	používat
anglické	anglický	k2eAgNnSc1d1	anglické
sedlo	sedlo	k1gNnSc1	sedlo
<g/>
,	,	kIx,	,
chody	chod	k1gInPc1	chod
jsou	být	k5eAaImIp3nP	být
krok	krok	k1gInSc1	krok
<g/>
,	,	kIx,	,
klus	klus	k1gInSc1	klus
<g/>
,	,	kIx,	,
cval	cval	k1gInSc1	cval
.	.	kIx.	.
</s>
<s>
Westernový	westernový	k2eAgInSc1d1	westernový
styl	styl	k1gInSc1	styl
je	být	k5eAaImIp3nS	být
využívaný	využívaný	k2eAgInSc1d1	využívaný
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
westernové	westernový	k2eAgNnSc1d1	westernové
sedlo	sedlo	k1gNnSc1	sedlo
<g/>
,	,	kIx,	,
chody	chod	k1gInPc1	chod
jsou	být	k5eAaImIp3nP	být
krok	krok	k1gInSc4	krok
<g/>
,	,	kIx,	,
jog	jog	k?	jog
a	a	k8xC	a
lope	lop	k1gFnSc2	lop
<g/>
.	.	kIx.	.
</s>
<s>
Otěže	otěž	k1gFnPc1	otěž
se	se	k3xPyFc4	se
drží	držet	k5eAaImIp3nP	držet
jednou	jeden	k4xCgFnSc7	jeden
rukou	ruka	k1gFnSc7	ruka
<g/>
,	,	kIx,	,
třmeny	třmen	k1gInPc1	třmen
jsou	být	k5eAaImIp3nP	být
delší	dlouhý	k2eAgInPc1d2	delší
než	než	k8xS	než
u	u	k7c2	u
anglického	anglický	k2eAgNnSc2d1	anglické
ježdění	ježdění	k1gNnSc2	ježdění
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInPc1d1	základní
chody	chod	k1gInPc1	chod
koně	kůň	k1gMnSc2	kůň
jsou	být	k5eAaImIp3nP	být
krok	krok	k1gInSc4	krok
<g/>
,	,	kIx,	,
klus	klus	k1gInSc1	klus
<g/>
,	,	kIx,	,
cval	cval	k1gInSc1	cval
<g/>
;	;	kIx,	;
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
chod	chod	k1gInSc1	chod
<g/>
:	:	kIx,	:
trysk	trysk	k1gInSc1	trysk
Krok	krok	k1gInSc1	krok
Krok	krok	k1gInSc1	krok
má	mít	k5eAaImIp3nS	mít
čtyřdobý	čtyřdobý	k2eAgInSc1d1	čtyřdobý
takt	takt	k1gInSc1	takt
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pohyb	pohyb	k1gInSc1	pohyb
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
raterální	raterální	k2eAgNnSc1d1	raterální
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
sobě	se	k3xPyFc3	se
jdou	jít	k5eAaImIp3nP	jít
vždy	vždy	k6eAd1	vždy
nejprve	nejprve	k6eAd1	nejprve
dvě	dva	k4xCgFnPc1	dva
končetiny	končetina	k1gFnPc1	končetina
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
(	(	kIx(	(
<g/>
levé	levá	k1gFnSc6	levá
či	či	k8xC	či
pravé	pravý	k2eAgFnSc6d1	pravá
<g/>
)	)	kIx)	)
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Kůň	kůň	k1gMnSc1	kůň
má	mít	k5eAaImIp3nS	mít
vždy	vždy	k6eAd1	vždy
nejméně	málo	k6eAd3	málo
dvě	dva	k4xCgFnPc1	dva
končetiny	končetina	k1gFnPc1	končetina
v	v	k7c6	v
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
podložkou	podložka	k1gFnSc7	podložka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
krok	krok	k1gInSc1	krok
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
,	,	kIx,	,
krk	krk	k1gInSc1	krk
má	mít	k5eAaImIp3nS	mít
uvolněný	uvolněný	k2eAgInSc1d1	uvolněný
<g/>
.	.	kIx.	.
</s>
<s>
Nohosled	Nohosled	k1gMnSc1	Nohosled
<g/>
:	:	kIx,	:
LZ	LZ	kA	LZ
<g/>
,	,	kIx,	,
LP	LP	kA	LP
<g/>
,	,	kIx,	,
PZ	PZ	kA	PZ
<g/>
,	,	kIx,	,
PP	PP	kA	PP
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
koně	kůň	k1gMnSc2	kůň
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kroku	krok	k1gInSc6	krok
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
</s>
<s>
Klus	klus	k1gInSc1	klus
Klus	klus	k1gInSc1	klus
je	být	k5eAaImIp3nS	být
chod	chod	k1gInSc4	chod
koně	kůň	k1gMnSc4	kůň
v	v	k7c6	v
dvoudobém	dvoudobý	k2eAgInSc6d1	dvoudobý
taktu	takt	k1gInSc6	takt
<g/>
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
je	být	k5eAaImIp3nS	být
diagonální	diagonální	k2eAgInSc1d1	diagonální
s	s	k7c7	s
krátkou	krátký	k2eAgFnSc7d1	krátká
fází	fáze	k1gFnSc7	fáze
vnosu	vnos	k1gInSc2	vnos
<g/>
.	.	kIx.	.
</s>
<s>
Klusající	klusající	k2eAgMnSc1d1	klusající
kůň	kůň	k1gMnSc1	kůň
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
maximální	maximální	k2eAgFnPc4d1	maximální
rovnováhy	rovnováha	k1gFnPc4	rovnováha
a	a	k8xC	a
současně	současně	k6eAd1	současně
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
možný	možný	k2eAgInSc1d1	možný
krok	krok	k1gInSc1	krok
<g/>
.	.	kIx.	.
</s>
<s>
Nohosled	Nohosled	k1gMnSc1	Nohosled
<g/>
:	:	kIx,	:
LZ	LZ	kA	LZ
<g/>
+	+	kIx~	+
<g/>
PP	PP	kA	PP
<g/>
,	,	kIx,	,
PZ	PZ	kA	PZ
<g/>
+	+	kIx~	+
<g/>
LP	LP	kA	LP
Rychlost	rychlost	k1gFnSc1	rychlost
koně	kůň	k1gMnSc2	kůň
v	v	k7c6	v
klusu	klus	k1gInSc6	klus
je	být	k5eAaImIp3nS	být
4	[number]	k4	4
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
15	[number]	k4	15
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klusáci	Klusák	k1gMnPc1	Klusák
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
vyvinout	vyvinout	k5eAaPmF	vyvinout
42	[number]	k4	42
<g/>
-	-	kIx~	-
<g/>
50	[number]	k4	50
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
na	na	k7c6	na
kratší	krátký	k2eAgFnSc6d2	kratší
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
klusu	klus	k1gInSc2	klus
<g/>
:	:	kIx,	:
lehký	lehký	k2eAgInSc1d1	lehký
a	a	k8xC	a
pracovní	pracovní	k2eAgInSc1d1	pracovní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lehkém	lehký	k2eAgInSc6d1	lehký
klusu	klus	k1gInSc6	klus
se	se	k3xPyFc4	se
vysedává	vysedávat	k5eAaImIp3nS	vysedávat
a	a	k8xC	a
v	v	k7c6	v
pracovním	pracovní	k2eAgMnSc6d1	pracovní
se	se	k3xPyFc4	se
sedí	sedit	k5eAaImIp3nP	sedit
pevně	pevně	k6eAd1	pevně
v	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
<g/>
.	.	kIx.	.
</s>
<s>
Cval	cval	k1gInSc4	cval
<g/>
:	:	kIx,	:
Cval	cval	k1gInSc4	cval
rozpoznáváme	rozpoznávat	k5eAaImIp1nP	rozpoznávat
na	na	k7c4	na
levou	levý	k2eAgFnSc4d1	levá
nebo	nebo	k8xC	nebo
na	na	k7c4	na
pravou	pravý	k2eAgFnSc4d1	pravá
ruku	ruka	k1gFnSc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jízdárně	jízdárna	k1gFnSc6	jízdárna
se	se	k3xPyFc4	se
cválá	cválat	k5eAaImIp3nS	cválat
tzv.	tzv.	kA	tzv.
na	na	k7c4	na
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
nohu	noha	k1gFnSc4	noha
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
že	že	k8xS	že
vedoucí	vedoucí	k1gFnSc1	vedoucí
je	být	k5eAaImIp3nS	být
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
přední	přední	k2eAgFnSc1d1	přední
noha	noha	k1gFnSc1	noha
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
nacválá	nacválat	k5eAaImIp3nS	nacválat
vždy	vždy	k6eAd1	vždy
zadní	zadní	k2eAgFnSc1d1	zadní
vnější	vnější	k2eAgFnSc1d1	vnější
noha	noha	k1gFnSc1	noha
<g/>
.	.	kIx.	.
</s>
<s>
Nohosled	Nohosled	k1gMnSc1	Nohosled
<g/>
:	:	kIx,	:
LZ	LZ	kA	LZ
<g/>
,	,	kIx,	,
<g/>
LP	LP	kA	LP
<g/>
+	+	kIx~	+
<g/>
PZ	PZ	kA	PZ
<g/>
,	,	kIx,	,
<g/>
PP	PP	kA	PP
(	(	kIx(	(
<g/>
na	na	k7c4	na
pravou	pravý	k2eAgFnSc4d1	pravá
ruku	ruka	k1gFnSc4	ruka
<g/>
)	)	kIx)	)
a	a	k8xC	a
PZ	PZ	kA	PZ
<g/>
,	,	kIx,	,
<g/>
PP	PP	kA	PP
<g/>
+	+	kIx~	+
<g/>
LZ	LZ	kA	LZ
<g/>
,	,	kIx,	,
<g/>
LP	LP	kA	LP
(	(	kIx(	(
<g/>
na	na	k7c4	na
levou	levý	k2eAgFnSc4d1	levá
ruku	ruka	k1gFnSc4	ruka
<g/>
)	)	kIx)	)
rychlost	rychlost	k1gFnSc4	rychlost
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
je	být	k5eAaImIp3nS	být
až	až	k9	až
30	[number]	k4	30
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Plnokrevníci	plnokrevník	k1gMnPc1	plnokrevník
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
běží	běžet	k5eAaImIp3nS	běžet
rychlostí	rychlost	k1gFnSc7	rychlost
až	až	k9	až
60	[number]	k4	60
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
(	(	kIx(	(
<g/>
na	na	k7c4	na
kratší	krátký	k2eAgInSc4d2	kratší
úsek	úsek	k1gInSc4	úsek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trysk	trysk	k1gInSc1	trysk
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
nejrychlejším	rychlý	k2eAgInSc7d3	nejrychlejší
<g/>
,	,	kIx,	,
čtyřdobým	čtyřdobý	k2eAgInSc7d1	čtyřdobý
chodem	chod	k1gInSc7	chod
<g/>
.	.	kIx.	.
</s>
<s>
Přechod	přechod	k1gInSc4	přechod
mezi	mezi	k7c7	mezi
cvalem	cval	k1gInSc7	cval
a	a	k8xC	a
tryskem	trysk	k1gInSc7	trysk
nazýváme	nazývat	k5eAaImIp1nP	nazývat
rychlý	rychlý	k2eAgInSc4d1	rychlý
cval	cval	k1gInSc4	cval
neboli	neboli	k8xC	neboli
galop	galop	k1gInSc4	galop
<g/>
.	.	kIx.	.
</s>
