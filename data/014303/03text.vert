<s>
GNU	gnu	k1gMnSc1
</s>
<s>
GNU	gnu	k1gMnSc1
</s>
<s>
Operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
GNU	gnu	k1gNnSc2
<g/>
/	/	kIx~
<g/>
Linux	linux	k1gInSc1
UbuntuWeb	UbuntuWba	k1gFnPc2
</s>
<s>
www.gnu.org	www.gnu.org	k1gInSc1
Vyvíjí	vyvíjet	k5eAaImIp3nS
</s>
<s>
Free	Free	k6eAd1
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
Rodina	rodina	k1gFnSc1
OS	OS	kA
</s>
<s>
Unix-like	Unix-like	k6eAd1
Typ	typ	k1gInSc1
jádra	jádro	k1gNnSc2
</s>
<s>
mikrojádro	mikrojádro	k6eAd1
(	(	kIx(
<g/>
GNU	gnu	k1gNnPc1
Hurd	hurda	k1gFnPc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
monolitické	monolitický	k2eAgNnSc4d1
(	(	kIx(
<g/>
GNU	gnu	k1gNnSc4
Linux-libre	Linux-libr	k1gMnSc5
<g/>
,	,	kIx,
fork	fork	k6eAd1
Linuxu	linux	k1gInSc3
<g/>
)	)	kIx)
Programovací	programovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
</s>
<s>
různé	různý	k2eAgInPc4d1
(	(	kIx(
<g/>
zejménaC	zejménaC	k?
a	a	k8xC
assembler	assembler	k1gInSc4
<g/>
)	)	kIx)
Výchozí	výchozí	k2eAgNnSc4d1
uživatelské	uživatelský	k2eAgNnSc4d1
rozhraní	rozhraní	k1gNnSc4
</s>
<s>
GNOME	GNOME	kA
Licence	licence	k1gFnSc1
</s>
<s>
GNU	gnu	k1gMnSc1
GPL	GPL	kA
<g/>
,	,	kIx,
GNU	gnu	k1gNnSc1
LGPL	LGPL	kA
<g/>
,	,	kIx,
GNU	gnu	k1gNnSc1
AGPL	AGPL	kA
<g/>
,	,	kIx,
GNU	gnu	k1gNnSc1
FDL	FDL	kA
<g/>
,	,	kIx,
GNU	gnu	k1gMnSc1
FSDG	FSDG	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Stav	stav	k1gInSc1
</s>
<s>
Aktivní	aktivní	k2eAgFnSc1d1
</s>
<s>
GNU	gnu	k1gNnSc1
(	(	kIx(
<g/>
výslovnost	výslovnost	k1gFnSc1
[	[	kIx(
<g/>
ɡ	ɡ	k?
<g/>
]	]	kIx)
<g/>
IPA	IPA	kA
<g/>
;	;	kIx,
anglicky	anglicky	k6eAd1
také	také	k9
pakůň	pakůň	k1gMnSc1
<g/>
,	,	kIx,
odtud	odtud	k6eAd1
logo	logo	k1gNnSc4
systému	systém	k1gInSc2
i	i	k8xC
projektu	projekt	k1gInSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
tuto	tento	k3xDgFnSc4
chvíli	chvíle	k1gFnSc6
nekompletní	kompletní	k2eNgInSc1d1
počítačový	počítačový	k2eAgInSc1d1
svobodný	svobodný	k2eAgInSc1d1
operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
projektu	projekt	k1gInSc2
GNU	gnu	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
jméno	jméno	k1gNnSc1
je	být	k5eAaImIp3nS
rekurzivní	rekurzivní	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
pro	pro	k7c4
anglické	anglický	k2eAgInPc4d1
„	„	k?
<g/>
GNU	gnu	k1gNnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Not	nota	k1gFnPc2
Unix	Unix	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
(	(	kIx(
<g/>
GNU	gnu	k1gMnSc1
Není	být	k5eNaImIp3nS
Unix	Unix	k1gInSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
UNIX-like	UNIX-like	k1gInSc1
a	a	k8xC
neobsahuje	obsahovat	k5eNaImIp3nS
žádný	žádný	k3yNgInSc4
originální	originální	k2eAgInSc4d1
kód	kód	k1gInSc4
Unixu	Unix	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
ještě	ještě	k6eAd1
nebylo	být	k5eNaImAgNnS
dopsáno	dopsat	k5eAaPmNgNnS
jeho	jeho	k3xOp3gNnSc1
oficiální	oficiální	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
GNU	gnu	k1gNnSc2
Hurd	hurda	k1gFnPc2
(	(	kIx(
<g/>
[	[	kIx(
<g/>
ɡ	ɡ	k?
ˈ	ˈ	k1gInSc1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
používá	používat	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
jádro	jádro	k1gNnSc1
Linux	Linux	kA
nebo	nebo	k8xC
Linux-libre	Linux-libr	k1gMnSc5
<g/>
,	,	kIx,
případně	případně	k6eAd1
jádro	jádro	k1gNnSc1
OpenSolaris	OpenSolaris	k1gFnSc2
či	či	k8xC
další	další	k2eAgNnPc1d1
svobodná	svobodný	k2eAgNnPc1d1
jádra	jádro	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
distribuce	distribuce	k1gFnPc4
tohoto	tento	k3xDgInSc2
systému	systém	k1gInSc2
patří	patřit	k5eAaImIp3nS
např.	např.	kA
GNU	gnu	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Linux	linux	k1gInSc1
nebo	nebo	k8xC
Nexenta	Nexenta	k1gFnSc1
OS	OS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vývoj	vývoj	k1gInSc1
tohoto	tento	k3xDgInSc2
systému	systém	k1gInSc2
byl	být	k5eAaImAgInS
iniciován	iniciovat	k5eAaBmNgInS
v	v	k7c6
projektu	projekt	k1gInSc6
GNU	gnu	k1gNnSc2
Richardem	Richard	k1gMnSc7
Stallmanem	Stallman	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
započal	započnout	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1984	#num#	k4
a	a	k8xC
byl	být	k5eAaImAgInS
hlavním	hlavní	k2eAgInSc7d1
záměrem	záměr	k1gInSc7
společnosti	společnost	k1gFnSc2
Free	Fre	k1gInSc2
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
(	(	kIx(
<g/>
FSF	FSF	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
stále	stále	k6eAd1
není	být	k5eNaImIp3nS
stabilní	stabilní	k2eAgNnSc4d1
vydání	vydání	k1gNnSc4
GNU	gnu	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jádra	jádro	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
nemají	mít	k5eNaImIp3nP
nic	nic	k3yNnSc1
společného	společný	k2eAgNnSc2d1
s	s	k7c7
GNU	gnu	k1gNnSc7
<g/>
,	,	kIx,
například	například	k6eAd1
známé	známý	k2eAgNnSc1d1
linuxové	linuxový	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
využito	využit	k2eAgNnSc1d1
s	s	k7c7
GNU	gnu	k1gNnSc7
softwarem	software	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1983	#num#	k4
byl	být	k5eAaImAgMnS
Richardem	Richard	k1gMnSc7
Stallmanem	Stallman	k1gMnSc7
na	na	k7c6
stránkách	stránka	k1gFnPc6
net	net	k?
<g/>
.	.	kIx.
<g/>
unix-wizards	unix-wizards	k1gInSc1
a	a	k8xC
net	net	k?
<g/>
.	.	kIx.
<g/>
usoft	usoft	k2eAgInSc1d1
uveřejněn	uveřejněn	k2eAgInSc1d1
záměr	záměr	k1gInSc1
pro	pro	k7c4
vytvoření	vytvoření	k1gNnSc4
GNU	gnu	k1gMnSc1
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Vývoj	vývoj	k1gInSc1
softwaru	software	k1gInSc2
započal	započnout	k5eAaPmAgInS
5	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1984	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
když	když	k8xS
Stallman	Stallman	k1gMnSc1
ukončil	ukončit	k5eAaPmAgMnS
práci	práce	k1gFnSc4
na	na	k7c6
Massachusettském	massachusettský	k2eAgInSc6d1
technologickém	technologický	k2eAgInSc6d1
institutu	institut	k1gInSc6
(	(	kIx(
<g/>
MIT	MIT	kA
<g/>
)	)	kIx)
v	v	k7c6
laboratoři	laboratoř	k1gFnSc6
umělé	umělý	k2eAgFnSc2d1
inteligence	inteligence	k1gFnSc2
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
právu	právo	k1gNnSc3
na	na	k7c6
vlastnictví	vlastnictví	k1gNnSc6
a	a	k8xC
aby	aby	kYmCp3nP
nemohli	moct	k5eNaImAgMnP
zasahovat	zasahovat	k5eAaImF
do	do	k7c2
distribuce	distribuce	k1gFnSc2
GNU	gnu	k1gNnSc2
jako	jako	k8xC,k8xS
svobodného	svobodný	k2eAgInSc2d1
softwaru	software	k1gInSc2
(	(	kIx(
<g/>
free	free	k1gInSc1
software	software	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Název	název	k1gInSc1
GNU	gnu	k1gNnSc2
byl	být	k5eAaImAgInS
vybrán	vybrat	k5eAaPmNgInS
Richardem	Richard	k1gMnSc7
Stallmanem	Stallman	k1gMnSc7
výběrem	výběr	k1gInSc7
z	z	k7c2
různých	různý	k2eAgFnPc2d1
slovních	slovní	k2eAgFnPc2d1
hříček	hříčka	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
písně	píseň	k1gFnSc2
The	The	k1gMnSc1
Gnu	gnu	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1
myšlenkou	myšlenka	k1gFnSc7
bylo	být	k5eAaImAgNnS
vytvořit	vytvořit	k5eAaPmF
kompletně	kompletně	k6eAd1
svobodný	svobodný	k2eAgInSc1d1
operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stallman	Stallman	k1gMnSc1
chtěl	chtít	k5eAaImAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
uživatelé	uživatel	k1gMnPc1
byli	být	k5eAaImAgMnP
„	„	k?
<g/>
free	fre	k1gInSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
variace	variace	k1gFnSc1
angl.	angl.	k?
slova	slovo	k1gNnSc2
–	–	k?
znamená	znamenat	k5eAaImIp3nS
volný	volný	k2eAgInSc1d1
<g/>
/	/	kIx~
<g/>
svobodný	svobodný	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chtěl	chtít	k5eAaImAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
mohli	moct	k5eAaImAgMnP
<g/>
:	:	kIx,
</s>
<s>
volně	volně	k6eAd1
studovat	studovat	k5eAaImF
zdrojový	zdrojový	k2eAgInSc4d1
kód	kód	k1gInSc4
softwaru	software	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
používají	používat	k5eAaImIp3nP
</s>
<s>
volně	volně	k6eAd1
sdílet	sdílet	k5eAaImF
software	software	k1gInSc4
s	s	k7c7
jinými	jiný	k2eAgMnPc7d1
uživateli	uživatel	k1gMnPc7
</s>
<s>
volně	volně	k6eAd1
upravovat	upravovat	k5eAaImF
chování	chování	k1gNnSc4
programu	program	k1gInSc2
</s>
<s>
volně	volně	k6eAd1
zveřejňovat	zveřejňovat	k5eAaImF
upravené	upravený	k2eAgFnPc4d1
verze	verze	k1gFnPc4
softwaru	software	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
myšlenka	myšlenka	k1gFnSc1
byla	být	k5eAaImAgFnS
později	pozdě	k6eAd2
uveřejněna	uveřejnit	k5eAaPmNgFnS
jako	jako	k8xC,k8xS
GNU	gnu	k1gNnSc1
Manifest	manifest	k1gInSc1
v	v	k7c6
dubnu	duben	k1gInSc6
roku	rok	k1gInSc2
1985	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stallmanovy	Stallmanův	k2eAgFnPc1d1
zkušenosti	zkušenost	k1gFnPc1
s	s	k7c7
Incompatible	Incompatible	k1gFnSc7
Timesharing	Timesharing	k1gInSc4
Systemem	System	k1gInSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
což	což	k3yRnSc1,k3yQnSc1
byl	být	k5eAaImAgMnS
raný	raný	k2eAgInSc4d1
operační	operační	k2eAgInSc4d1
systém	systém	k1gInSc4
napsaný	napsaný	k2eAgInSc4d1
v	v	k7c6
jazyku	jazyk	k1gInSc6
symbolických	symbolický	k2eAgFnPc2d1
adres	adresa	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
zastaralým	zastaralý	k2eAgFnPc3d1
kvůli	kvůli	k7c3
přerušení	přerušení	k1gNnSc3
výroby	výroba	k1gFnSc2
minipočítače	minipočítač	k1gInSc2
PDP-	PDP-	k1gFnSc2
<g/>
10	#num#	k4
<g/>
,	,	kIx,
pro	pro	k7c4
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
byl	být	k5eAaImAgInS
napsán	napsat	k5eAaBmNgInS,k5eAaPmNgInS
<g/>
,	,	kIx,
vedly	vést	k5eAaImAgFnP
k	k	k7c3
rozhodnutí	rozhodnutí	k1gNnSc3
<g/>
,	,	kIx,
že	že	k8xS
<g />
.	.	kIx.
</s>
<s hack="1">
je	být	k5eAaImIp3nS
zapotřebí	zapotřebí	k6eAd1
přenositelného	přenositelný	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Bylo	být	k5eAaImAgNnS
proto	proto	k8xC
rozhodnuto	rozhodnout	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
GNU	gnu	k1gNnSc1
bude	být	k5eAaImBp3nS
ponejvíce	ponejvíce	k6eAd1
kompatibilní	kompatibilní	k2eAgMnSc1d1
s	s	k7c7
Unixem	Unix	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Již	již	k6eAd1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
byl	být	k5eAaImAgInS
Unix	Unix	k1gInSc4
populárním	populární	k2eAgInSc7d1
komerčním	komerční	k2eAgInSc7d1
softwarem	software	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unix	Unix	k1gInSc1
byl	být	k5eAaImAgInS
navržen	navrhnout	k5eAaPmNgInS
modulárně	modulárně	k6eAd1
<g/>
,	,	kIx,
takže	takže	k8xS
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
reimplementován	reimplementován	k2eAgInSc1d1
po	po	k7c6
částech	část	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
S	s	k7c7
většinou	většina	k1gFnSc7
potřebného	potřebný	k2eAgInSc2d1
softwaru	software	k1gInSc2
bylo	být	k5eAaImAgNnS
potřeba	potřeba	k6eAd1
začít	začít	k5eAaPmF
od	od	k7c2
píky	píka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
svobodný	svobodný	k2eAgInSc1d1
software	software	k1gInSc1
třetích	třetí	k4xOgFnPc2
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
TeX	TeX	k1gFnSc1
(	(	kIx(
<g/>
výslovnost	výslovnost	k1gFnSc1
[	[	kIx(
<g/>
tech	tech	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
angl.	angl.	k?
[	[	kIx(
<g/>
tek	teka	k1gFnPc2
<g/>
]	]	kIx)
<g/>
)	)	kIx)
sázecí	sázecí	k2eAgInSc1d1
software	software	k1gInSc1
<g/>
,	,	kIx,
X	X	kA
Window	Window	k1gFnSc1
System	Syst	k1gInSc7
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
jádro	jádro	k1gNnSc1
Mach	macha	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
formuje	formovat	k5eAaImIp3nS
základ	základ	k1gInSc1
GNU	gnu	k1gNnSc2
Mach	macha	k1gFnPc2
jádra	jádro	k1gNnSc2
GNU	gnu	k1gMnSc1
Hurd	hurda	k1gFnPc2
(	(	kIx(
<g/>
oficiální	oficiální	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
GNU	gnu	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
S	s	k7c7
výjimkou	výjimka	k1gFnSc7
posledně	posledně	k6eAd1
zmíněných	zmíněný	k2eAgInPc2d1
komponentů	komponent	k1gInPc2
třetích	třetí	k4xOgFnPc2
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
většina	většina	k1gFnSc1
GNU	gnu	k1gNnSc2
napsána	napsat	k5eAaPmNgFnS,k5eAaBmNgFnS
dobrovolníky	dobrovolník	k1gMnPc7
z	z	k7c2
GNU	gnu	k1gNnSc2
Projektu	projekt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
psali	psát	k5eAaImAgMnP
GNU	gnu	k1gNnSc4
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
volném	volný	k2eAgInSc6d1
čase	čas	k1gInSc6
<g/>
,	,	kIx,
někteří	některý	k3yIgMnPc1
byli	být	k5eAaImAgMnP
placeni	platit	k5eAaImNgMnP
společnostmi	společnost	k1gFnPc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
vzdělávacími	vzdělávací	k2eAgFnPc7d1
institucemi	instituce	k1gFnPc7
a	a	k8xC
neziskovými	ziskový	k2eNgFnPc7d1
společnostmi	společnost	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
říjnu	říjen	k1gInSc6
roku	rok	k1gInSc2
1985	#num#	k4
Stallman	Stallman	k1gMnSc1
vytvořil	vytvořit	k5eAaPmAgMnS
Free	Free	k1gInSc4
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
(	(	kIx(
<g/>
FSF	FSF	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
přelomu	přelom	k1gInSc6
let	léto	k1gNnPc2
1980	#num#	k4
a	a	k8xC
1990	#num#	k4
si	se	k3xPyFc3
FSF	FSF	kA
najmulo	najmout	k5eAaPmAgNnS
softwarové	softwarový	k2eAgNnSc1d1
vývojáře	vývojář	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
jim	on	k3xPp3gMnPc3
napsali	napsat	k5eAaBmAgMnP,k5eAaPmAgMnP
potřebný	potřebný	k2eAgInSc4d1
software	software	k1gInSc4
pro	pro	k7c4
GNU	gnu	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
GNU	gnu	k1gNnSc1
dostávalo	dostávat	k5eAaImAgNnS
do	do	k7c2
popředí	popředí	k1gNnSc2
<g/>
,	,	kIx,
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
měly	mít	k5eAaImAgFnP
zájem	zájem	k1gInSc4
<g/>
,	,	kIx,
začaly	začít	k5eAaPmAgInP
přispívat	přispívat	k5eAaImF
k	k	k7c3
vývoji	vývoj	k1gInSc3
<g/>
,	,	kIx,
nebo	nebo	k8xC
k	k	k7c3
prodeji	prodej	k1gInSc3
GNU	gnu	k1gNnSc2
softwaru	software	k1gInSc2
a	a	k8xC
technické	technický	k2eAgFnSc2d1
podpory	podpora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýznačnější	význačný	k2eAgFnSc1d3
a	a	k8xC
nejúspěšnější	úspěšný	k2eAgFnSc1d3
byla	být	k5eAaImAgFnS
firma	firma	k1gFnSc1
Cygnus	Cygnus	k1gMnSc1
Solutions	Solutions	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
nyní	nyní	k6eAd1
součástí	součást	k1gFnSc7
Red	Red	k1gFnSc2
Hat	hat	k0
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Komponenty	komponenta	k1gFnPc1
</s>
<s>
Základní	základní	k2eAgFnPc4d1
systémové	systémový	k2eAgFnPc4d1
komponenty	komponenta	k1gFnPc4
obsahují	obsahovat	k5eAaImIp3nP
GNU	gnu	k1gNnSc7
Compiler	Compiler	k1gInSc1
Collection	Collection	k1gInSc1
(	(	kIx(
<g/>
GCC	GCC	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
GNU	gnu	k1gMnSc1
C	C	kA
Library	Librar	k1gInPc1
<g/>
(	(	kIx(
<g/>
glibc	glibc	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
GNU	gnu	k1gMnSc1
Core	Cor	k1gFnSc2
Utilities	Utilities	k1gMnSc1
(	(	kIx(
<g/>
coreutils	coreutils	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
GNU	gnu	k1gMnSc1
Debugger	Debugger	k1gMnSc1
(	(	kIx(
<g/>
GDB	GDB	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
GNU	gnu	k1gMnSc1
binutils	binutils	k1gInSc1
(	(	kIx(
<g/>
binutils	binutils	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Bash	Bash	k1gMnSc1
shell	shell	k1gMnSc1
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
GNOME	GNOME	kA
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Desktopové	desktopový	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vývojáři	vývojář	k1gMnPc1
GNU	gnu	k1gNnSc4
přispěli	přispět	k5eAaPmAgMnP
Linuxu	linux	k1gInSc2
<g/>
,	,	kIx,
přenesením	přenesení	k1gNnSc7
GNU	gnu	k1gNnSc2
aplikací	aplikace	k1gFnPc2
a	a	k8xC
nástrojů	nástroj	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
nyní	nyní	k6eAd1
široce	široko	k6eAd1
využívané	využívaný	k2eAgFnPc1d1
na	na	k7c6
jiných	jiný	k2eAgInPc6d1
operačních	operační	k2eAgInPc6d1
systémech	systém	k1gInPc6
jako	jako	k9
varianty	varianta	k1gFnPc4
BSD	BSD	kA
<g/>
,	,	kIx,
Solaris	Solaris	k1gFnSc1
a	a	k8xC
macOS	macOS	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mnoho	mnoho	k6eAd1
GNU	gnu	k1gNnSc1
programů	program	k1gInPc2
bylo	být	k5eAaImAgNnS
přeneseno	přenést	k5eAaPmNgNnS
na	na	k7c4
jiné	jiný	k2eAgInPc4d1
operační	operační	k2eAgInPc4d1
systémy	systém	k1gInPc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
komerčních	komerční	k2eAgFnPc2d1
platforem	platforma	k1gFnPc2
jako	jako	k8xC,k8xS
MS	MS	kA
Windows	Windows	kA
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
macOS	macOS	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
jejich	jejich	k3xOp3gInPc7
komerčními	komerční	k2eAgInPc7d1
protějšky	protějšek	k1gInPc7
se	s	k7c7
GNU	gnu	k1gNnSc7
prokázaly	prokázat	k5eAaPmAgFnP
být	být	k5eAaImF
také	také	k9
více	hodně	k6eAd2
spolehlivé	spolehlivý	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
bylo	být	k5eAaImAgNnS
hostováno	hostován	k2eAgNnSc4d1
319	#num#	k4
GNU	gnu	k1gNnSc4
balíčků	balíček	k1gInPc2
na	na	k7c6
oficiálních	oficiální	k2eAgFnPc6d1
stránkách	stránka	k1gFnPc6
vývoje	vývoj	k1gInSc2
GNU	gnu	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Varianty	varianta	k1gFnPc1
GNU	gnu	k1gNnSc2
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
Projektu	projekt	k1gInSc2
GNU	gnu	k1gNnSc2
je	být	k5eAaImIp3nS
GNU	gnu	k1gNnSc4
Hurd	hurda	k1gFnPc2
mikrojádro	mikrojádro	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
je	být	k5eAaImIp3nS
linuxové	linuxový	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
oficiálně	oficiálně	k6eAd1
částí	část	k1gFnSc7
Projektu	projekt	k1gInSc2
GNU	gnu	k1gNnSc2
ve	v	k7c6
formě	forma	k1gFnSc6
Linux-libre	Linux-libr	k1gInSc5
–	–	k?
varianta	varianta	k1gFnSc1
linuxového	linuxový	k2eAgNnSc2d1
jádra	jádro	k1gNnSc2
bez	bez	k7c2
jakýchkoliv	jakýkoliv	k3yIgFnPc2
proprietárních	proprietární	k2eAgFnPc2d1
součástí	součást	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ostatní	ostatní	k2eAgNnPc4d1
jádra	jádro	k1gNnPc4
se	se	k3xPyFc4
také	také	k9
dají	dát	k5eAaPmIp3nP
zakomponovat	zakomponovat	k5eAaPmF
do	do	k7c2
GNU	gnu	k1gNnSc2
softwaru	software	k1gInSc2
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
fungujícího	fungující	k2eAgInSc2d1
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
např.	např.	kA
FreeBSD	FreeBSD	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
FSF	FSF	kA
(	(	kIx(
<g/>
Free	Fre	k1gInSc2
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
<g/>
)	)	kIx)
prohlašuje	prohlašovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Linux	linux	k1gInSc4
použitý	použitý	k2eAgInSc4d1
s	s	k7c7
GNU	gnu	k1gNnPc2
nástroji	nástroj	k1gInPc7
a	a	k8xC
pomůckami	pomůcka	k1gFnPc7
<g/>
,	,	kIx,
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
považován	považován	k2eAgInSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
za	za	k7c4
variantu	varianta	k1gFnSc4
GNU	gnu	k1gNnSc2
a	a	k8xC
dále	daleko	k6eAd2
proklamuje	proklamovat	k5eAaBmIp3nS
pro	pro	k7c4
tyto	tento	k3xDgInPc4
systémy	systém	k1gInPc4
termín	termín	k1gInSc4
GNU	gnu	k1gNnPc7
<g/>
/	/	kIx~
<g/>
Linux	Linux	kA
(	(	kIx(
<g/>
Tato	tento	k3xDgFnSc1
okolnost	okolnost	k1gFnSc1
vede	vést	k5eAaImIp3nS
k	k	k7c3
GNU	gnu	k1gNnSc3
<g/>
/	/	kIx~
<g/>
Linux	Linux	kA
kontroverzi	kontroverze	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
GNU	gnu	k1gNnPc4
<g />
.	.	kIx.
</s>
<s hack="1">
Projekt	projekt	k1gInSc1
takto	takto	k6eAd1
schválil	schválit	k5eAaPmAgInS
varianty	varianta	k1gFnSc2
GNU	gnu	k1gNnSc2
obsahující	obsahující	k2eAgNnSc4d1
linuxové	linuxový	k2eAgNnSc4d1
jádro	jádro	k1gNnSc4
zejména	zejména	k9
gNewSense	gNewSense	k6eAd1
<g/>
,	,	kIx,
Trisquel	Trisquel	k1gInSc1
a	a	k8xC
Parabola	parabola	k1gFnSc1
GNU	gnu	k1gNnSc2
<g/>
/	/	kIx~
<g/>
Linux	Linux	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Ostatní	ostatní	k2eAgFnSc2d1
varianty	varianta	k1gFnSc2
GNU	gnu	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
nepoužívají	používat	k5eNaImIp3nP
Hurd	hurda	k1gFnPc2
jako	jako	k8xC,k8xS
jádro	jádro	k1gNnSc4
obsahují	obsahovat	k5eAaImIp3nP
Nexenta	Nexenta	k1gFnSc1
Core	Core	k1gFnSc1
(	(	kIx(
<g/>
GNU	gnu	k1gNnSc1
a	a	k8xC
jádro	jádro	k1gNnSc1
OpenSolaris	OpenSolaris	k1gFnSc2
<g/>
)	)	kIx)
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
GNU-Darwin	GNU-Darwin	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
GNU	gnu	k1gNnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Licenses	Licenses	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Free	Fre	k1gInSc2
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
<g/>
,	,	kIx,
Inc	Inc	k1gFnSc1
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2016-11-18	2016-11-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GNU	gnu	k1gMnSc1
Project	Project	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Free	Free	k1gInSc1
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
<g/>
,	,	kIx,
Inc	Inc	k1gFnSc1
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2018-01-01	2018-01-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GNU	gnu	k1gMnSc1
Project	Project	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Richard	Richard	k1gMnSc1
Stallman	Stallman	k1gMnSc1
<g/>
.	.	kIx.
new	new	k?
UNIX	Unix	k1gInSc1
implementation	implementation	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1983-09-27	1983-09-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Net	Net	k1gFnSc1
<g/>
.	.	kIx.
<g/>
unix-wizards	unix-wizards	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
LAMBERT	LAMBERT	kA
<g/>
,	,	kIx,
Laura	Laura	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Internet	Internet	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
Historical	Historical	k1gFnSc4
Encyclopedia	Encyclopedium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biographies	Biographiesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Editor	editor	k1gInSc1
<g/>
:	:	kIx,
Hilary	Hilara	k1gFnSc2
Poole	Poole	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volume	volum	k1gInSc5
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Santa	Santa	k1gFnSc1
Barbara	Barbara	k1gFnSc1
<g/>
,	,	kIx,
California	Californium	k1gNnPc1
<g/>
:	:	kIx,
ABC-CLIO	ABC-CLIO	k1gFnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
85109	#num#	k4
<g/>
-	-	kIx~
<g/>
664	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
215	#num#	k4
<g/>
–	–	k?
<g/>
216	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
HOLMEVIK	HOLMEVIK	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Rune	run	k1gInSc5
<g/>
;	;	kIx,
BOGOST	BOGOST	kA
<g/>
,	,	kIx,
Ian	Ian	k1gMnSc1
<g/>
;	;	kIx,
ULMER	ULMER	kA
<g/>
,	,	kIx,
Gregory	Gregor	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inter	Inter	k1gInSc1
<g/>
/	/	kIx~
<g/>
vention	vention	k1gInSc1
<g/>
:	:	kIx,
Free	Free	k1gInSc1
Play	play	k0
in	in	k?
the	the	k?
Age	Age	k1gFnSc2
of	of	k?
Electracy	Electraca	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
MIT	MIT	kA
Press	Press	k1gInSc1
<g/>
,	,	kIx,
březen	březen	k1gInSc1
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
262	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1705	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
69	#num#	k4
<g/>
–	–	k?
<g/>
71	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
The	The	k1gFnPc2
Free	Free	k1gFnPc2
Software	software	k1gInSc1
Movement	Movement	k1gMnSc1
and	and	k?
the	the	k?
Future	Futur	k1gMnSc5
of	of	k?
Freedom	Freedom	k1gInSc1
<g/>
,	,	kIx,
ffzg	ffzg	k1gInSc1
<g/>
.	.	kIx.
<g/>
hr	hr	k6eAd1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
DIBONA	DIBONA	kA
<g/>
,	,	kIx,
Chris	Chris	k1gFnSc1
<g/>
;	;	kIx,
STONE	ston	k1gInSc5
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
<g/>
;	;	kIx,
COOPER	COOPER	kA
<g/>
,	,	kIx,
Danese	Danese	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Open	Open	k1gMnSc1
Sources	Sources	k1gMnSc1
2.0	2.0	k4
<g/>
:	:	kIx,
The	The	k1gFnSc1
Continuing	Continuing	k1gInSc1
Evolution	Evolution	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
říjen	říjen	k1gInSc1
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780596008024	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
38	#num#	k4
<g/>
–	–	k?
<g/>
40	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SEEBACH	SEEBACH	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Beginning	Beginning	k1gInSc1
Portable	portable	k1gInSc4
Shell	Shell	k1gInSc1
Scripting	Scripting	k1gInSc1
<g/>
:	:	kIx,
From	From	k1gMnSc1
Novice	novice	k1gFnSc2
to	ten	k3xDgNnSc4
Professional	Professional	k1gMnSc1
(	(	kIx(
<g/>
Expert	expert	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Voice	Voice	k1gMnSc1
in	in	k?
Open	Open	k1gMnSc1
Source	Source	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781430210436	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
177	#num#	k4
<g/>
–	–	k?
<g/>
178	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
KERRISK	KERRISK	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
Linux	Linux	kA
Programming	Programming	k1gInSc1
Interface	interface	k1gInSc1
<g/>
:	:	kIx,
A	A	kA
Linux	Linux	kA
and	and	k?
UNIX	UNIX	kA
System	Syst	k1gInSc7
Programming	Programming	k1gInSc1
Handbook	handbook	k1gInSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781593272203	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
5	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Open	Open	k1gMnSc1
Sources	Sources	k1gMnSc1
<g/>
:	:	kIx,
Voices	Voices	k1gMnSc1
from	from	k1gMnSc1
the	the	k?
Open	Open	k1gInSc1
Source	Source	k1gMnSc1
Revolution	Revolution	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
O	o	k7c4
<g/>
'	'	kIx"
<g/>
Reilly	Reilla	k1gFnSc2
&	&	k?
Associates	Associates	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
leden	leden	k1gInSc4
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
56592	#num#	k4
<g/>
-	-	kIx~
<g/>
582	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BUXMANN	BUXMANN	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
;	;	kIx,
DIEFENBACH	DIEFENBACH	kA
<g/>
,	,	kIx,
Heiner	Heiner	k1gMnSc1
<g/>
;	;	kIx,
HESS	HESS	kA
<g/>
,	,	kIx,
Thomas	Thomas	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Software	software	k1gInSc1
Industry	Industra	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9783642315091	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
187	#num#	k4
<g/>
–	–	k?
<g/>
196	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Practical	Practical	k1gMnSc1
UNIX	UNIX	kA
and	and	k?
Internet	Internet	k1gInSc1
Security	Securita	k1gFnSc2
<g/>
,	,	kIx,
3	#num#	k4
<g/>
rd	rd	k?
Edition	Edition	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
O	o	k7c4
<g/>
'	'	kIx"
<g/>
Reilly	Reilla	k1gFnSc2
&	&	k?
Associates	Associates	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
únor	únor	k1gInSc4
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781449310127	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
18	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SHANKLAND	SHANKLAND	kA
<g/>
,	,	kIx,
Stephen	Stephen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Red	Red	k1gFnSc1
Hat	hat	k0
buys	buys	k1gInSc4
software	software	k1gInSc1
firm	firm	k1gInSc1
<g/>
,	,	kIx,
shuffles	shuffles	k1gInSc1
CEO	CEO	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CBS	CBS	kA
Interactive	Interactiv	k1gInSc5
Inc	Inc	k1gMnPc7
<g/>
,	,	kIx,
2002-01-02	2002-01-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Tech	Tech	k?
Industry	Industra	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNET	CNET	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MATTHEW	MATTHEW	kA
<g/>
,	,	kIx,
Neil	Neil	k1gMnSc1
<g/>
;	;	kIx,
STONES	STONES	kA
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Beginning	Beginning	k1gInSc1
Linux	Linux	kA
Programming	Programming	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781118058619	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SOWE	SOWE	kA
<g/>
,	,	kIx,
Sulayman	Sulayman	k1gMnSc1
K	K	kA
<g/>
;	;	kIx,
STAMELOS	STAMELOS	kA
<g/>
,	,	kIx,
Ioannis	Ioannis	k1gFnPc1
G	G	kA
<g/>
;	;	kIx,
SAMOLADAS	SAMOLADAS	kA
<g/>
,	,	kIx,
Ioannis	Ioannis	k1gInSc1
M.	M.	kA
Emerging	Emerging	k1gInSc1
Free	Free	k1gFnSc1
and	and	k?
Open	Open	k1gNnSc1
Source	Sourec	k1gInSc2
Software	software	k1gInSc1
Practices	Practices	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
May	May	k1gMnSc1
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781599042107	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
262	#num#	k4
<g/>
–	–	k?
<g/>
264	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Linux	linux	k1gInSc1
<g/>
:	:	kIx,
History	Histor	k1gInPc1
and	and	k?
Introduction	Introduction	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Buzzle	Buzzle	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
1991-08-25	1991-08-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MCCUNE	MCCUNE	kA
<g/>
,	,	kIx,
Mike	Mike	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Integrating	Integrating	k1gInSc1
Linux	Linux	kA
and	and	k?
Windows	Windows	kA
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
prosinec	prosinec	k1gInSc1
2000	#num#	k4
<g/>
.	.	kIx.
30	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780130306708	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SOBELL	SOBELL	kA
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
G	G	kA
<g/>
;	;	kIx,
SEEBACH	SEEBACH	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Practical	Practical	k1gMnSc1
Guide	Guid	k1gInSc5
To	ten	k3xDgNnSc1
Unix	Unix	k1gInSc1
For	forum	k1gNnPc2
Mac	Mac	kA
Os	osa	k1gFnPc2
X	X	kA
Users	Users	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780131863330	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MILLER	Miller	k1gMnSc1
<g/>
,	,	kIx,
Barton	Barton	k1gInSc1
P	P	kA
<g/>
;	;	kIx,
KOSKI	KOSKI	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
;	;	kIx,
LEE	Lea	k1gFnSc6
<g/>
,	,	kIx,
Cjin	Cjin	k1gMnSc1
Pheow	Pheow	k1gMnSc1
<g/>
;	;	kIx,
MAGANTY	MAGANTY	kA
<g/>
,	,	kIx,
Vivekananda	Vivekananda	k1gFnSc1
<g/>
;	;	kIx,
MURTHY	MURTHY	kA
<g/>
,	,	kIx,
Ravi	Ravi	k1gNnSc1
<g/>
;	;	kIx,
NATARAJAN	NATARAJAN	kA
<g/>
,	,	kIx,
Ajitkumar	Ajitkumar	k1gMnSc1
<g/>
;	;	kIx,
STEIDL	STEIDL	kA
<g/>
,	,	kIx,
Jeff	Jeff	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fuzz	Fuzz	k1gMnSc1
Revisited	Revisited	k1gMnSc1
<g/>
:	:	kIx,
A	A	kA
Re-examination	Re-examination	k1gInSc1
of	of	k?
the	the	k?
Reliability	Reliabilita	k1gFnSc2
of	of	k?
UNIX	UNIX	kA
Utilities	Utilities	k1gMnSc1
and	and	k?
Services	Services	k1gMnSc1
[	[	kIx(
<g/>
ps	ps	k0
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Computer	computer	k1gInSc1
Sciences	Sciences	k1gMnSc1
Department	department	k1gInSc1
<g/>
,	,	kIx,
University	universita	k1gFnPc1
of	of	k?
Wisconsin	Wisconsin	k1gMnSc1
<g/>
,	,	kIx,
1995-10	1995-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Statistics	Statistics	k1gInSc1
[	[	kIx(
<g/>
Savannah	Savannah	k1gInSc1
<g/>
]	]	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Free	Fre	k1gInSc2
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
<g/>
,	,	kIx,
Inc	Inc	k1gFnSc1
<g/>
,	,	kIx,
2011-02-13	2011-02-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Powered	Powered	k1gInSc1
by	by	kYmCp3nS
Savane	Savan	k1gMnSc5
3.1	3.1	k4
<g/>
-cleanup	-cleanup	k1gInSc1
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
GNU	gnu	k1gMnSc1
Linux-libre	Linux-libr	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Free	Fre	k1gInSc2
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
<g/>
,	,	kIx,
Inc	Inc	k1gFnSc1
<g/>
,	,	kIx,
2012-12-17	2012-12-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KAVANAGH	KAVANAGH	kA
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Open	Open	k1gNnSc1
Source	Sourec	k1gInSc2
Software	software	k1gInSc1
<g/>
:	:	kIx,
Implementation	Implementation	k1gInSc1
And	Anda	k1gFnPc2
Management	management	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781555583200	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
129	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Linux	linux	k1gInSc1
is	is	k?
a	a	k8xC
GNU	gnu	k1gMnSc1
system	syst	k1gMnSc7
and	and	k?
the	the	k?
DWARF	DWARF	kA
support	support	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1994-09-05	1994-09-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Comp	Comp	k1gInSc4
<g/>
.	.	kIx.
<g/>
os	osa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
linux	linux	k1gInSc1
<g/>
.	.	kIx.
<g/>
misc	misc	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PROFFITT	PROFFITT	kA
<g/>
,	,	kIx,
Brian	Brian	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Debian	Debian	k1gMnSc1
GNU	gnu	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Linux	Linux	kA
seeks	seeks	k1gInSc1
alignment	alignment	k1gInSc1
with	witha	k1gFnPc2
Free	Fre	k1gFnSc2
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
IDG	IDG	kA
Communications	Communicationsa	k1gFnPc2
<g/>
,	,	kIx,
Inc	Inc	k1gFnPc2
<g/>
,	,	kIx,
2012-07-12	2012-07-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
1.1	1.1	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Linux	linux	k1gInSc1
or	or	k?
GNU	gnu	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Linux	linux	k1gInSc1
<g/>
,	,	kIx,
that	that	k1gInSc1
is	is	k?
the	the	k?
question	question	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tldp	Tldp	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
List	list	k1gInSc1
of	of	k?
Free	Free	k1gInSc1
GNU	gnu	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Linux	Linux	kA
Distributions	Distributions	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Free	Fre	k1gInSc2
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2017-12-22	2017-12-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GNU	gnu	k1gMnSc1
Project	Project	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SOLTER	SOLTER	kA
<g/>
,	,	kIx,
Nicholas	Nicholas	k1gMnSc1
A	A	kA
<g/>
;	;	kIx,
JELINEK	JELINEK	kA
<g/>
,	,	kIx,
Jerry	Jerra	k1gFnSc2
<g/>
;	;	kIx,
MINER	MINER	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781118080313	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
The	The	k1gMnSc2
GNU-Darwin	GNU-Darwin	k2eAgInSc4d1
Distribution	Distribution	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Projekt	projekt	k1gInSc1
GNU	gnu	k1gNnSc2
</s>
<s>
Hnutí	hnutí	k1gNnSc1
za	za	k7c4
svobodný	svobodný	k2eAgInSc4d1
software	software	k1gInSc4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
GNU	gnu	k1gNnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
GNU	gnu	k1gNnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Projekt	projekt	k1gInSc1
GNU	gnu	k1gNnSc2
Historie	historie	k1gFnSc1
</s>
<s>
GNU	gnu	k1gNnSc1
Manifest	manifest	k1gInSc1
•	•	k?
Projekt	projekt	k1gInSc1
GNU	gnu	k1gMnSc1
•	•	k?
Free	Free	k1gInSc1
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
•	•	k?
Historie	historie	k1gFnSc1
svobodného	svobodný	k2eAgInSc2d1
softwaru	software	k1gInSc2
</s>
<s>
Licence	licence	k1gFnSc1
</s>
<s>
GNU	gnu	k1gMnSc1
General	General	k1gMnSc1
Public	publicum	k1gNnPc2
License	Licens	k1gMnSc2
•	•	k?
GNU	gnu	k1gMnSc1
Lesser	Lesser	k1gMnSc1
General	General	k1gMnSc1
Public	publicum	k1gNnPc2
License	Licens	k1gMnSc2
•	•	k?
Affero	Affero	k1gNnSc1
General	General	k1gFnSc1
Public	publicum	k1gNnPc2
License	License	k1gFnSc2
•	•	k?
GNU	gnu	k1gMnSc1
Free	Fre	k1gFnSc2
Documentation	Documentation	k1gInSc1
License	License	k1gFnSc2
•	•	k?
GPL	GPL	kA
linking	linking	k1gInSc1
exception	exception	k1gInSc1
</s>
<s>
Software	software	k1gInSc1
</s>
<s>
GNU	gnu	k1gMnSc1
•	•	k?
GNU	gnu	k1gMnSc1
Health	Health	k1gMnSc1
•	•	k?
GNU	gnu	k1gNnSc2
Hurd	hurda	k1gFnPc2
•	•	k?
GNU	gnu	k1gNnSc2
Hurd	hurda	k1gFnPc2
NG	NG	kA
•	•	k?
Linux-libre	Linux-libr	k1gInSc5
•	•	k?
GNOME	GNOME	kA
•	•	k?
Gnuzilla	Gnuzilla	k1gMnSc1
•	•	k?
IceCat	IceCat	k1gInSc1
•	•	k?
Gnash	Gnash	k1gInSc1
•	•	k?
Bash	Bash	k1gInSc1
•	•	k?
GCC	GCC	kA
•	•	k?
GNU	gnu	k1gNnSc2
Emacs	Emacsa	k1gFnPc2
•	•	k?
GNU	gnu	k1gMnSc1
C	C	kA
Library	Librara	k1gFnPc1
•	•	k?
Coreutils	Coreutils	k1gInSc1
•	•	k?
GNU	gnu	k1gMnSc1
build	build	k1gMnSc1
system	syst	k1gMnSc7
•	•	k?
gettext	gettext	k1gMnSc1
•	•	k?
Ostatní	ostatní	k2eAgMnSc1d1
GNU	gnu	k1gMnSc1
balíčky	balíček	k1gInPc4
a	a	k8xC
programy	program	k1gInPc4
</s>
<s>
Stoupenci	stoupenec	k1gMnPc1
</s>
<s>
Robert	Robert	k1gMnSc1
J.	J.	kA
Chassell	Chassell	k1gMnSc1
•	•	k?
Loï	Loï	k1gFnSc1
Dachary	Dachara	k1gFnSc2
•	•	k?
Ricardo	Ricardo	k1gNnSc4
Galli	Galle	k1gFnSc3
•	•	k?
Georg	Georg	k1gInSc1
C.	C.	kA
F.	F.	kA
Greve	Greev	k1gFnPc1
•	•	k?
Federico	Federico	k1gMnSc1
Heinz	Heinz	k1gMnSc1
•	•	k?
Benjamin	Benjamin	k1gMnSc1
Mako	mako	k1gNnSc1
Hill	Hill	k1gMnSc1
•	•	k?
Bradley	Bradlea	k1gFnSc2
M.	M.	kA
Kuhn	Kuhn	k1gMnSc1
•	•	k?
Eben	eben	k1gInSc1
Moglen	Moglen	k1gInSc1
•	•	k?
Brett	Brett	k1gMnSc1
Smith	Smith	k1gMnSc1
•	•	k?
Richard	Richard	k1gMnSc1
Stallman	Stallman	k1gMnSc1
•	•	k?
John	John	k1gMnSc1
Sullivan	Sullivan	k1gMnSc1
•	•	k?
Leonard	Leonard	k1gMnSc1
H.	H.	kA
Tower	Tower	k1gMnSc1
ml.	ml.	kA
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
GNU	gnu	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Linux	linux	k1gInSc1
-	-	kIx~
spor	spor	k1gInSc1
o	o	k7c4
jméno	jméno	k1gNnSc4
•	•	k?
Revolution	Revolution	k1gInSc1
OS	OS	kA
(	(	kIx(
<g/>
film	film	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
The	The	k1gFnSc1
Code	Code	k1gFnSc1
(	(	kIx(
<g/>
film	film	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Operační	operační	k2eAgInPc1d1
systémy	systém	k1gInPc1
BSD	BSD	kA
</s>
<s>
FreeBSD	FreeBSD	k?
•	•	k?
DragonFly	DragonFla	k1gFnSc2
BSD	BSD	kA
•	•	k?
NetBSD	NetBSD	k1gMnSc1
•	•	k?
OpenBSD	OpenBSD	k1gMnSc1
—	—	k?
Minix	Minix	k1gInSc1
3	#num#	k4
Linux	Linux	kA
(	(	kIx(
<g/>
distribuce	distribuce	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
Deb	Deb	k1gMnPc5
<g/>
)	)	kIx)
</s>
<s>
Debian	Debian	k1gInSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Knoppix	Knoppix	k1gInSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ubuntu	Ubunt	k1gInSc2
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
RPM	RPM	kA
<g/>
)	)	kIx)
</s>
<s>
Red	Red	k?
Hat	hat	k0
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Fedora	Fedora	k1gFnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mandriva	Mandriva	k1gFnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
Portage	Portage	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Gentoo	Gentoo	k1gNnSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
AUR	aura	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Arch	arch	k1gInSc1
Linux	Linux	kA
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Manjaro	Manjara	k1gFnSc5
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
další	další	k2eAgFnSc2d1
</s>
<s>
Slackware	Slackwar	k1gMnSc5
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Slax	Slax	k1gInSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SUSE	SUSE	kA
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
•	•	k?
další	další	k2eAgInSc4d1
<g/>
…	…	k?
</s>
<s>
GNU	gnu	k1gMnSc1
</s>
<s>
GNU	gnu	k1gMnSc1
Hurd	hurda	k1gFnPc2
•	•	k?
Linux-libre	Linux-libr	k1gInSc5
—	—	k?
seL	sít	k5eAaImAgInS
<g/>
4	#num#	k4
(	(	kIx(
<g/>
Mac	Mac	kA
<g/>
)	)	kIx)
OS	OS	kA
X	X	kA
•	•	k?
macOS	macOS	k?
</s>
<s>
10.6	10.6	k4
(	(	kIx(
<g/>
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
10.7	10.7	k4
(	(	kIx(
<g/>
Lion	Lion	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
10.8	10.8	k4
(	(	kIx(
<g/>
Mountain	Mountain	k1gMnSc1
Lion	Lion	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
10.9	10.9	k4
(	(	kIx(
<g/>
Mavericks	Mavericksa	k1gFnPc2
<g/>
)	)	kIx)
•	•	k?
OS	OS	kA
X	X	kA
10.10	10.10	k4
Yosemite	Yosemit	k1gInSc5
•	•	k?
OS	OS	kA
X	X	kA
10.11	10.11	k4
El	Ela	k1gFnPc2
Capitan	Capitan	k1gInSc4
•	•	k?
macOS	macOS	k?
10.12	10.12	k4
Sierra	Sierra	k1gFnSc1
•	•	k?
macOS	macOS	k?
10.13	10.13	k4
High	Higha	k1gFnPc2
Sierra	Sierra	k1gFnSc1
•	•	k?
macOS	macOS	k?
10.14	10.14	k4
Mojave	Mojav	k1gInSc5
•	•	k?
macOS	macOS	k?
10.15	10.15	k4
Catalina	Catalina	k1gFnSc1
•	•	k?
macOS	macOS	k?
11	#num#	k4
Big	Big	k1gFnPc7
Sur	Sur	k1gMnSc2
DOS	DOS	kA
</s>
<s>
MS-DOS	MS-DOS	k?
•	•	k?
DR-DOS	DR-DOS	k1gMnSc1
•	•	k?
FreeDOS	FreeDOS	k1gMnSc1
•	•	k?
PTS-DOS	PTS-DOS	k1gMnSc1
Windows	Windows	kA
</s>
<s>
Windows	Windows	kA
pro	pro	k7c4
MS-DOS	MS-DOS	k1gFnSc4
</s>
<s>
Windows	Windows	kA
1.0	1.0	k4
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
2.0	2.0	k4
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
3.0	3.0	k4
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
3.1	3.1	k4
<g/>
x	x	k?
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
95	#num#	k4
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
98	#num#	k4
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
ME	ME	kA
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
Windows	Windows	kA
NT	NT	kA
</s>
<s>
Windows	Windows	kA
NT	NT	kA
3.1	3.1	k4
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
NT	NT	kA
3.5	3.5	k4
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
NT	NT	kA
3.51	3.51	k4
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
NT	NT	kA
4.0	4.0	k4
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
2000	#num#	k4
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
XP	XP	kA
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
→	→	k?
Windows	Windows	kA
Server	server	k1gInSc4
2003	#num#	k4
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
Vista	vista	k2eAgFnSc1d1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
Server	server	k1gInSc4
2008	#num#	k4
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
7	#num#	k4
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
Server	server	k1gInSc4
2008	#num#	k4
R2	R2	k1gFnPc2
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
8	#num#	k4
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
Server	server	k1gInSc4
2012	#num#	k4
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
8.1	8.1	k4
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
Server	server	k1gInSc4
2012	#num#	k4
R2	R2	k1gFnPc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
10	#num#	k4
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
Windows	Windows	kA
CE	CE	kA
</s>
<s>
Windows	Windows	kA
CE	CE	kA
→	→	k?
Windows	Windows	kA
Mobile	mobile	k1gNnSc1
→	→	k?
Windows	Windows	kA
Phone	Phon	k1gInSc5
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
→	→	k?
Windows	Windows	kA
10	#num#	k4
Mobile	mobile	k1gNnPc2
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
Vývoj	vývoj	k1gInSc1
zrušen	zrušit	k5eAaPmNgInS
</s>
<s>
Cairo	Cairo	k6eAd1
•	•	k?
Nashville	Nashville	k1gInSc1
•	•	k?
Neptune	Neptun	k1gInSc5
•	•	k?
Odyssey	Odyssea	k1gFnPc5
</s>
<s>
Mobilní	mobilní	k2eAgInPc1d1
telefony	telefon	k1gInPc1
a	a	k8xC
PDA	PDA	kA
</s>
<s>
Android	android	k1gInSc1
•	•	k?
Bada	Bada	k1gFnSc1
•	•	k?
BlackBerry	BlackBerra	k1gFnSc2
OS	OS	kA
•	•	k?
Firefox	Firefox	k1gInSc1
OS	OS	kA
•	•	k?
iOS	iOS	k?
•	•	k?
Maemo	Maema	k1gFnSc5
•	•	k?
Palm	Palmo	k1gNnPc2
OS	OS	kA
•	•	k?
Symbian	Symbian	k1gMnSc1
OS	OS	kA
•	•	k?
Tizen	Tizen	k1gInSc1
•	•	k?
Ubuntu	Ubunt	k1gInSc2
Touch	Touch	k1gMnSc1
•	•	k?
webOS	webOS	k?
•	•	k?
Windows	Windows	kA
Phone	Phon	k1gInSc5
další	další	k2eAgNnPc1d1
</s>
<s>
QNX	QNX	kA
•	•	k?
Solaris	Solaris	k1gInSc1
•	•	k?
BeOS	BeOS	k1gFnSc2
•	•	k?
OpenVMS	OpenVMS	k1gFnSc2
•	•	k?
Mac	Mac	kA
OS	OS	kA
•	•	k?
NeXTSTEP	NeXTSTEP	k1gMnSc1
•	•	k?
Syllable	Syllable	k1gMnSc1
•	•	k?
ReactOS	ReactOS	k1gMnSc1
•	•	k?
Haiku	Haika	k1gFnSc4
•	•	k?
Chrome	chromat	k5eAaImIp3nS
OS	OS	kA
•	•	k?
AmigaOS	AmigaOS	k1gMnSc7
historické	historický	k2eAgFnSc2d1
</s>
<s>
Mac	Mac	kA
OS	OS	kA
(	(	kIx(
<g/>
Classic	Classic	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Multics	Multics	k1gInSc1
•	•	k?
OS	OS	kA
<g/>
/	/	kIx~
<g/>
2	#num#	k4
•	•	k?
Plan	plan	k1gInSc1
9	#num#	k4
from	from	k6eAd1
Bell	bell	k1gInSc1
Labs	Labs	k1gInSc1
•	•	k?
UNIX	UNIX	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Svobodný	svobodný	k2eAgInSc1d1
software	software	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4405655-2	4405655-2	k4
</s>
