<s>
Chief	Chief	k1gMnSc1
executive	executiv	k1gInSc5
officer	officer	k1gMnSc1
</s>
<s>
Chief	Chief	k1gMnSc1
executive	executive	k2eAgMnSc1d1
officer	officer	k1gMnSc1
(	(	kIx(
<g/>
CEO	CEO	kA
<g/>
,	,	kIx,
chief	chief	k1gMnSc1
executive	executive	k2eAgMnSc1d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
angloamerickém	angloamerický	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
výkonný	výkonný	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
obchodní	obchodní	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
a	a	k8xC
jiných	jiný	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
akciových	akciový	k2eAgFnPc6d1
společnostech	společnost	k1gFnPc6
odpovídá	odpovídat	k5eAaImIp3nS
post	post	k1gInSc4
CEO	CEO	kA
funkci	funkce	k1gFnSc6
předsedy	předseda	k1gMnSc2
představenstva	představenstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
tedy	tedy	k9
o	o	k7c4
manažera	manažer	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
má	mít	k5eAaImIp3nS
veškeré	veškerý	k3xTgFnPc4
jednatelské	jednatelský	k2eAgFnPc4d1
pravomoci	pravomoc	k1gFnPc4
vůči	vůči	k7c3
třetím	třetí	k4xOgFnPc3
osobám	osoba	k1gFnPc3
i	i	k8xC
veškeré	veškerý	k3xTgFnPc4
jednatelské	jednatelský	k2eAgFnPc4d1
pravomoci	pravomoc	k1gFnPc4
uvnitř	uvnitř	k7c2
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
českého	český	k2eAgNnSc2d1
práva	právo	k1gNnSc2
jde	jít	k5eAaImIp3nS
o	o	k7c4
statutární	statutární	k2eAgInSc4d1
orgán	orgán	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
existují	existovat	k5eAaImIp3nP
i	i	k9
manažeři	manažer	k1gMnPc1
s	s	k7c7
obdobnými	obdobný	k2eAgInPc7d1
tituly	titul	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
např.	např.	kA
o	o	k7c4
chief	chief	k1gInSc4
financial	financial	k1gMnSc1
officer	officer	k1gMnSc1
(	(	kIx(
<g/>
CFO	CFO	kA
<g/>
)	)	kIx)
–	–	k?
finančního	finanční	k2eAgMnSc2d1
ředitele	ředitel	k1gMnSc2
<g/>
;	;	kIx,
ten	ten	k3xDgMnSc1
má	mít	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnSc4d3
odpovědnost	odpovědnost	k1gFnSc4
za	za	k7c4
finance	finance	k1gFnPc4
společnosti	společnost	k1gFnSc2
a	a	k8xC
zodpovídá	zodpovídat	k5eAaPmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
dozorčí	dozorčí	k2eAgFnSc3d1
radě	rada	k1gFnSc3
–	–	k?
supervisory	supervisor	k1gMnPc4
board	boarda	k1gFnPc2
(	(	kIx(
<g/>
nebo	nebo	k8xC
také	také	k9
board	board	k1gInSc1
of	of	k?
directors	directors	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
pak	pak	k6eAd1
chief	chief	k1gInSc1
operating	operating	k1gInSc1
officer	officer	k1gInSc1
(	(	kIx(
<g/>
COO	COO	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
chief	chief	k1gInSc1
information	information	k1gInSc1
officer	officer	k1gInSc1
(	(	kIx(
<g/>
CIO	CIO	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
chief	chief	k1gInSc1
marketing	marketing	k1gInSc1
officer	officer	k1gInSc1
(	(	kIx(
<g/>
CMO	CMO	kA
<g/>
)	)	kIx)
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4071766-5	4071766-5	k4
</s>
