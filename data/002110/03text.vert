<s>
Calgary	Calgary	k1gNnSc1	Calgary
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jedním	jeden	k4xCgInSc7	jeden
milionem	milion	k4xCgInSc7	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
1,097	[number]	k4	1,097
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
aglomeraci	aglomerace	k1gFnSc6	aglomerace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
kanadské	kanadský	k2eAgFnSc2d1	kanadská
prérijní	prérijní	k2eAgFnSc2d1	prérijní
provincie	provincie	k1gFnSc2	provincie
Alberta	Albert	k1gMnSc2	Albert
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
provincie	provincie	k1gFnSc2	provincie
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
úrodné	úrodný	k2eAgFnSc2d1	úrodná
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
prérií	prérie	k1gFnPc2	prérie
<g/>
.	.	kIx.	.
</s>
<s>
Calgary	Calgary	k1gNnSc1	Calgary
je	být	k5eAaImIp3nS	být
kvůli	kvůli	k7c3	kvůli
okolním	okolní	k2eAgNnPc3d1	okolní
horským	horský	k2eAgNnPc3d1	horské
střediskům	středisko	k1gNnPc3	středisko
významným	významný	k2eAgMnSc7d1	významný
centrem	centr	k1gMnSc7	centr
zimních	zimní	k2eAgInPc2d1	zimní
sportů	sport	k1gInPc2	sport
a	a	k8xC	a
ekoturismu	ekoturismus	k1gInSc2	ekoturismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
město	město	k1gNnSc1	město
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
veliký	veliký	k2eAgInSc4d1	veliký
rozmach	rozmach	k1gInSc4	rozmach
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
kvůli	kvůli	k7c3	kvůli
těžbě	těžba	k1gFnSc3	těžba
a	a	k8xC	a
zpracování	zpracování	k1gNnSc3	zpracování
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
významnými	významný	k2eAgMnPc7d1	významný
odvětvími	odvětví	k1gNnPc7	odvětví
jsou	být	k5eAaImIp3nP	být
turistický	turistický	k2eAgInSc1d1	turistický
ruch	ruch	k1gInSc1	ruch
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc1	zemědělství
a	a	k8xC	a
průmysl	průmysl	k1gInSc1	průmysl
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
na	na	k7c4	na
moderní	moderní	k2eAgFnSc4d1	moderní
technologie	technologie	k1gFnSc1	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
dějištěm	dějiště	k1gNnSc7	dějiště
mnoha	mnoho	k4c2	mnoho
festivalů	festival	k1gInPc2	festival
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
Calgary	Calgary	k1gNnSc1	Calgary
Stampede	Stamped	k1gMnSc5	Stamped
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
789,9	[number]	k4	789,9
km2	km2	k4	km2
asi	asi	k9	asi
80	[number]	k4	80
km	km	kA	km
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
Skalistých	skalistý	k2eAgFnPc2d1	skalistý
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
300	[number]	k4	300
km	km	kA	km
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Edmontonu	Edmonton	k1gInSc2	Edmonton
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
5083	[number]	k4	5083
km2	km2	k4	km2
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
1,162	[number]	k4	1,162
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
jména	jméno	k1gNnSc2	jméno
Calgary	Calgary	k1gNnSc2	Calgary
(	(	kIx(	(
<g/>
Cala-ghearraidh	Calahearraidh	k1gInSc1	Cala-ghearraidh
<g/>
,	,	kIx,	,
pláž	pláž	k1gFnSc1	pláž
<g/>
,	,	kIx,	,
pastviny	pastvina	k1gFnPc1	pastvina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
skotský	skotský	k2eAgMnSc1d1	skotský
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
město	město	k1gNnSc1	město
na	na	k7c6	na
louce	louka	k1gFnSc6	louka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgInSc1d1	vlastní
hokejový	hokejový	k2eAgInSc1d1	hokejový
tým	tým	k1gInSc1	tým
Calgary	Calgary	k1gNnSc2	Calgary
Flames	Flamesa	k1gFnPc2	Flamesa
<g/>
.	.	kIx.	.
</s>
<s>
Bret	Bret	k2eAgInSc1d1	Bret
Hart	Hart	k1gInSc1	Hart
(	(	kIx(	(
<g/>
*	*	kIx~	*
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
wrestler	wrestler	k1gMnSc1	wrestler
Erica	Eric	k1gInSc2	Eric
Durance	durance	k1gFnSc2	durance
(	(	kIx(	(
<g/>
*	*	kIx~	*
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Owen	Owen	k1gMnSc1	Owen
Hargreaves	Hargreaves	k1gMnSc1	Hargreaves
(	(	kIx(	(
<g/>
*	*	kIx~	*
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
Elisha	Elisha	k1gMnSc1	Elisha
Cuthbertová	Cuthbertová	k1gFnSc1	Cuthbertová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Cory	Cora	k1gFnSc2	Cora
Monteith	Monteitha	k1gFnPc2	Monteitha
(	(	kIx(	(
<g/>
*	*	kIx~	*
1982	[number]	k4	1982
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
John	John	k1gMnSc1	John
Kucera	Kucera	k1gFnSc1	Kucera
(	(	kIx(	(
<g/>
*	*	kIx~	*
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sjezdový	sjezdový	k2eAgMnSc1d1	sjezdový
lyžař	lyžař	k1gMnSc1	lyžař
Džajpur	Džajpur	k1gMnSc1	Džajpur
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
Naucalpan	Naucalpan	k1gInSc1	Naucalpan
de	de	k?	de
Juárez	Juárez	k1gInSc1	Juárez
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
Phoenix	Phoenix	k1gInSc1	Phoenix
<g/>
,	,	kIx,	,
Arizona	Arizona	k1gFnSc1	Arizona
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
Québec	Québec	k1gInSc1	Québec
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
Ta-čching	Ta-čching	k1gInSc1	Ta-čching
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
Tedžon	Tedžona	k1gFnPc2	Tedžona
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
Calgary	Calgary	k1gNnSc2	Calgary
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Calgary	Calgary	k1gNnSc2	Calgary
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
