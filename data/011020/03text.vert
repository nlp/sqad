<p>
<s>
Lžíce	lžíce	k1gFnSc1	lžíce
je	být	k5eAaImIp3nS	být
nástroj	nástroj	k1gInSc4	nástroj
sloužící	sloužící	k2eAgInSc4d1	sloužící
k	k	k7c3	k
nabírání	nabírání	k1gNnSc3	nabírání
potravy	potrava	k1gFnSc2	potrava
z	z	k7c2	z
jídelní	jídelní	k2eAgFnSc2d1	jídelní
nádoby	nádoba	k1gFnSc2	nádoba
a	a	k8xC	a
jejímu	její	k3xOp3gNnSc3	její
podávání	podávání	k1gNnSc1	podávání
do	do	k7c2	do
úst	ústa	k1gNnPc2	ústa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
víceúčelová	víceúčelový	k2eAgFnSc1d1	víceúčelová
pomůcka	pomůcka	k1gFnSc1	pomůcka
v	v	k7c6	v
kuchyni	kuchyně	k1gFnSc6	kuchyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
tak	tak	k9	tak
i	i	k9	i
pracovní	pracovní	k2eAgFnSc4d1	pracovní
část	část	k1gFnSc4	část
stavebního	stavební	k2eAgInSc2d1	stavební
či	či	k8xC	či
důlního	důlní	k2eAgInSc2d1	důlní
stroje	stroj	k1gInSc2	stroj
(	(	kIx(	(
<g/>
bagr	bagr	k1gInSc1	bagr
<g/>
,	,	kIx,	,
rypadlo	rypadlo	k1gNnSc1	rypadlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výraz	výraz	k1gInSc1	výraz
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
pro	pro	k7c4	pro
nástroj	nástroj	k1gInSc4	nástroj
zvaný	zvaný	k2eAgInSc4d1	zvaný
zednická	zednický	k2eAgFnSc1d1	zednická
lžíce	lžíce	k1gFnSc1	lžíce
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
nese	nést	k5eAaImIp3nS	nést
i	i	k9	i
pomůcka	pomůcka	k1gFnSc1	pomůcka
při	při	k7c6	při
obouvání	obouvání	k1gNnSc6	obouvání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jídelní	jídelní	k2eAgFnPc4d1	jídelní
a	a	k8xC	a
kuchyňské	kuchyňský	k2eAgFnPc4d1	kuchyňská
lžíce	lžíce	k1gFnPc4	lžíce
==	==	k?	==
</s>
</p>
<p>
<s>
Lžíce	lžíce	k1gFnSc1	lžíce
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Antická	antický	k2eAgFnSc1d1	antická
lžíce	lžíce	k1gFnSc1	lžíce
měla	mít	k5eAaImAgFnS	mít
oblý	oblý	k2eAgInSc4d1	oblý
téměř	téměř	k6eAd1	téměř
kruhový	kruhový	k2eAgInSc4d1	kruhový
tvar	tvar	k1gInSc4	tvar
listu	list	k1gInSc2	list
a	a	k8xC	a
rovnou	rovnou	k6eAd1	rovnou
úzkou	úzký	k2eAgFnSc4d1	úzká
rukojeť	rukojeť	k1gFnSc4	rukojeť
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
tvar	tvar	k1gInSc1	tvar
jídelní	jídelní	k2eAgFnSc2d1	jídelní
a	a	k8xC	a
kuchyňské	kuchyňský	k2eAgFnSc2d1	kuchyňská
lžíce	lžíce	k1gFnSc2	lžíce
je	být	k5eAaImIp3nS	být
mělká	mělký	k2eAgFnSc1d1	mělká
oválná	oválný	k2eAgFnSc1d1	oválná
miska	miska	k1gFnSc1	miska
(	(	kIx(	(
<g/>
list	list	k1gInSc1	list
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dřík	dřík	k1gInSc1	dřík
je	být	k5eAaImIp3nS	být
plochý	plochý	k2eAgInSc1d1	plochý
a	a	k8xC	a
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
se	se	k3xPyFc4	se
v	v	k7c4	v
rukojeť	rukojeť	k1gFnSc4	rukojeť
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
misky	miska	k1gFnSc2	miska
(	(	kIx(	(
<g/>
list	list	k1gInSc1	list
<g/>
)	)	kIx)	)
i	i	k8xC	i
rukojeti	rukojeť	k1gFnPc1	rukojeť
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nP	měnit
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
dobách	doba	k1gFnPc6	doba
měnil	měnit	k5eAaImAgMnS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Rukojeť	rukojeť	k1gFnSc1	rukojeť
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
zdobena	zdoben	k2eAgFnSc1d1	zdobena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Materiál	materiál	k1gInSc1	materiál
===	===	k?	===
</s>
</p>
<p>
<s>
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
hojně	hojně	k6eAd1	hojně
používané	používaný	k2eAgNnSc1d1	používané
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
cín	cín	k1gInSc1	cín
<g/>
,	,	kIx,	,
stříbro	stříbro	k1gNnSc1	stříbro
či	či	k8xC	či
(	(	kIx(	(
<g/>
v	v	k7c6	v
nejvyšších	vysoký	k2eAgFnPc6d3	nejvyšší
vrstvách	vrstva	k1gFnPc6	vrstva
společnosti	společnost	k1gFnSc2	společnost
<g/>
)	)	kIx)	)
zlato	zlato	k1gNnSc1	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
převládly	převládnout	k5eAaPmAgInP	převládnout
obecné	obecný	k2eAgInPc1d1	obecný
kovy	kov	k1gInPc1	kov
<g/>
,	,	kIx,	,
poniklované	poniklovaný	k2eAgNnSc1d1	poniklované
železo	železo	k1gNnSc1	železo
<g/>
,	,	kIx,	,
slitiny	slitina	k1gFnPc1	slitina
jako	jako	k8xC	jako
(	(	kIx(	(
<g/>
alpaka	alpaka	k1gFnSc1	alpaka
<g/>
,	,	kIx,	,
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
také	také	k9	také
hliník	hliník	k1gInSc4	hliník
<g/>
,	,	kIx,	,
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
i	i	k9	i
plast	plast	k1gInSc4	plast
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
i	i	k9	i
používání	používání	k1gNnSc1	používání
nerezové	rezový	k2eNgFnSc2d1	nerezová
oceli	ocel	k1gFnSc2	ocel
<g/>
;	;	kIx,	;
tyto	tento	k3xDgInPc1	tento
tři	tři	k4xCgInPc1	tři
materiály	materiál	k1gInPc1	materiál
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nejpoužívanější	používaný	k2eAgInSc1d3	nejpoužívanější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jídelní	jídelní	k2eAgFnPc1d1	jídelní
lžíce	lžíce	k1gFnPc1	lžíce
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
buď	buď	k8xC	buď
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
součástí	součást	k1gFnSc7	součást
jídelního	jídelní	k2eAgInSc2d1	jídelní
příboru	příbor	k1gInSc2	příbor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Typy	typ	k1gInPc4	typ
<g/>
,	,	kIx,	,
tvary	tvar	k1gInPc4	tvar
a	a	k8xC	a
velikosti	velikost	k1gFnPc4	velikost
===	===	k?	===
</s>
</p>
<p>
<s>
Polévková	polévkový	k2eAgFnSc1d1	polévková
lžíce	lžíce	k1gFnSc1	lžíce
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
jenom	jenom	k9	jenom
"	"	kIx"	"
<g/>
lžíce	lžíce	k1gFnPc4	lžíce
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
objem	objem	k1gInSc1	objem
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
mililitrů	mililitr	k1gInPc2	mililitr
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
<g/>
)	)	kIx)	)
–	–	k?	–
osobní	osobní	k2eAgInSc1d1	osobní
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
ke	k	k7c3	k
konzumaci	konzumace	k1gFnSc3	konzumace
polévek	polévka	k1gFnPc2	polévka
a	a	k8xC	a
kašovité	kašovitý	k2eAgFnSc2d1	kašovitá
stravy	strava	k1gFnSc2	strava
jednomu	jeden	k4xCgNnSc3	jeden
strávníkovi	strávníkův	k2eAgMnPc1d1	strávníkův
</s>
</p>
<p>
<s>
Dětská	dětský	k2eAgFnSc1d1	dětská
lžíce	lžíce	k1gFnSc1	lžíce
-	-	kIx~	-
zmenšená	zmenšený	k2eAgFnSc1d1	zmenšená
lžíce	lžíce	k1gFnSc1	lžíce
polévková	polévkový	k2eAgFnSc1d1	polévková
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
kulatý	kulatý	k2eAgInSc1d1	kulatý
list	list	k1gInSc1	list
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dítě	dítě	k1gNnSc1	dítě
nezranilo	zranit	k5eNaPmAgNnS	zranit
</s>
</p>
<p>
<s>
Podávací	podávací	k2eAgFnPc4d1	podávací
(	(	kIx(	(
<g/>
servírovací	servírovací	k2eAgFnPc4d1	servírovací
<g/>
)	)	kIx)	)
lžíce	lžíce	k1gFnPc4	lžíce
-	-	kIx~	-
na	na	k7c6	na
míse	mísa	k1gFnSc6	mísa
<g/>
,	,	kIx,	,
společná	společný	k2eAgFnSc1d1	společná
pro	pro	k7c4	pro
více	hodně	k6eAd2	hodně
stolovníků	stolovník	k1gMnPc2	stolovník
</s>
</p>
<p>
<s>
Čajová	čajový	k2eAgFnSc1d1	čajová
lžička	lžička	k1gFnSc1	lžička
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
jenom	jenom	k9	jenom
"	"	kIx"	"
<g/>
lžička	lžička	k1gFnSc1	lžička
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
objem	objem	k1gInSc1	objem
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
mililitrů	mililitr	k1gInPc2	mililitr
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
<g/>
)	)	kIx)	)
–	–	k?	–
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
na	na	k7c4	na
míchání	míchání	k1gNnPc4	míchání
a	a	k8xC	a
ochutnávání	ochutnávání	k1gNnPc4	ochutnávání
čaje	čaj	k1gInSc2	čaj
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgInPc2d1	jiný
horkých	horký	k2eAgInPc2d1	horký
nápojů	nápoj	k1gInPc2	nápoj
<g/>
,	,	kIx,	,
eventuálně	eventuálně	k6eAd1	eventuálně
i	i	k9	i
ke	k	k7c3	k
konzumaci	konzumace	k1gFnSc3	konzumace
dezertů	dezert	k1gInPc2	dezert
(	(	kIx(	(
<g/>
i	i	k8xC	i
když	když	k8xS	když
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
dezertů	dezert	k1gInPc2	dezert
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
vhodnější	vhodný	k2eAgNnSc1d2	vhodnější
použít	použít	k5eAaPmF	použít
speciální	speciální	k2eAgFnSc4d1	speciální
vidličku	vidlička	k1gFnSc4	vidlička
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kávová	kávový	k2eAgFnSc1d1	kávová
lžička	lžička	k1gFnSc1	lžička
–	–	k?	–
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
na	na	k7c4	na
míchání	míchání	k1gNnSc4	míchání
a	a	k8xC	a
ochutnávání	ochutnávání	k1gNnSc4	ochutnávání
kávy	káva	k1gFnSc2	káva
</s>
</p>
<p>
<s>
Mocca	Mocc	k2eAgFnSc1d1	Mocc
lžička	lžička	k1gFnSc1	lžička
–	–	k?	–
drobná	drobný	k2eAgFnSc1d1	drobná
lžička	lžička	k1gFnSc1	lžička
o	o	k7c6	o
objemu	objem	k1gInSc6	objem
cca	cca	kA	cca
3	[number]	k4	3
mililitrů	mililitr	k1gInPc2	mililitr
používaná	používaný	k2eAgFnSc1d1	používaná
při	při	k7c6	při
podávání	podávání	k1gNnSc6	podávání
mocca	mocc	k1gInSc2	mocc
kávy	káva	k1gFnSc2	káva
v	v	k7c6	v
malých	malý	k2eAgInPc6d1	malý
šálcích	šálek	k1gInPc6	šálek
</s>
</p>
<p>
<s>
Dezertní	dezertní	k2eAgFnSc1d1	dezertní
lžička	lžička	k1gFnSc1	lžička
–	–	k?	–
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
ke	k	k7c3	k
konzumaci	konzumace	k1gFnSc3	konzumace
dortů	dort	k1gInPc2	dort
i	i	k8xC	i
jiných	jiný	k2eAgInPc2d1	jiný
dezertů	dezert	k1gInPc2	dezert
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
typický	typický	k2eAgInSc1d1	typický
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
oválný	oválný	k2eAgInSc1d1	oválný
nebo	nebo	k8xC	nebo
vejčitý	vejčitý	k2eAgInSc1d1	vejčitý
tvar	tvar	k1gInSc1	tvar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
rovná	rovnat	k5eAaImIp3nS	rovnat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jí	on	k3xPp3gFnSc2	on
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
odkrajovat	odkrajovat	k5eAaImF	odkrajovat
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
sousta	sousto	k1gNnPc4	sousto
</s>
</p>
<p>
<s>
Zmrzlinovíá	Zmrzlinovíá	k1gFnSc1	Zmrzlinovíá
lžička	lžička	k1gFnSc1	lžička
-	-	kIx~	-
plochá	plochý	k2eAgFnSc1d1	plochá
téměř	téměř	k6eAd1	téměř
čtvercová	čtvercový	k2eAgFnSc1d1	čtvercová
</s>
</p>
<p>
<s>
Salátová	salátový	k2eAgFnSc1d1	salátová
lžíce	lžíce	k1gFnSc1	lžíce
-	-	kIx~	-
dříve	dříve	k6eAd2	dříve
s	s	k7c7	s
listem	list	k1gInSc7	list
z	z	k7c2	z
kosti	kost	k1gFnSc2	kost
(	(	kIx(	(
<g/>
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
plastová	plastový	k2eAgNnPc1d1	plastové
<g/>
,	,	kIx,	,
zásadně	zásadně	k6eAd1	zásadně
ne	ne	k9	ne
kovová	kovový	k2eAgFnSc1d1	kovová
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
povrch	povrch	k1gInSc4	povrch
nereagoval	reagovat	k5eNaBmAgMnS	reagovat
na	na	k7c4	na
ocet	ocet	k1gInSc4	ocet
či	či	k8xC	či
citronovou	citronový	k2eAgFnSc4d1	citronová
šťávu	šťáva	k1gFnSc4	šťáva
</s>
</p>
<p>
<s>
Sypací	sypací	k2eAgFnPc4d1	sypací
lžíce	lžíce	k1gFnPc4	lžíce
-	-	kIx~	-
s	s	k7c7	s
otvory	otvor	k1gInPc7	otvor
v	v	k7c6	v
listu	list	k1gInSc6	list
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
sypání	sypání	k1gNnSc3	sypání
cukru	cukr	k1gInSc2	cukr
na	na	k7c4	na
moučníkyPoužívání	moučníkyPoužívání	k1gNnSc4	moučníkyPoužívání
lžíce	lžíce	k1gFnSc2	lžíce
během	během	k7c2	během
konzumace	konzumace	k1gFnSc2	konzumace
jídla	jídlo	k1gNnSc2	jídlo
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
hygienickou	hygienický	k2eAgFnSc4d1	hygienická
konzumaci	konzumace	k1gFnSc4	konzumace
tekuté	tekutý	k2eAgFnSc2d1	tekutá
<g/>
,	,	kIx,	,
kašovité	kašovitý	k2eAgFnSc2d1	kašovitá
či	či	k8xC	či
měkké	měkký	k2eAgFnSc2d1	měkká
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
konzumovaná	konzumovaný	k2eAgFnSc1d1	konzumovaná
potrava	potrava	k1gFnSc1	potrava
příliš	příliš	k6eAd1	příliš
horká	horký	k2eAgFnSc1d1	horká
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
na	na	k7c4	na
lžíci	lžíce	k1gFnSc4	lžíce
nabrané	nabraný	k2eAgNnSc1d1	nabrané
sousto	sousto	k1gNnSc1	sousto
před	před	k7c7	před
spolknutím	spolknutí	k1gNnSc7	spolknutí
ochladit	ochladit	k5eAaPmF	ochladit
foukáním	foukání	k1gNnSc7	foukání
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgNnSc1d1	časté
je	být	k5eAaImIp3nS	být
též	též	k9	též
její	její	k3xOp3gNnSc4	její
použití	použití	k1gNnSc4	použití
k	k	k7c3	k
míchání	míchání	k1gNnSc3	míchání
horkých	horký	k2eAgInPc2d1	horký
nápojů	nápoj	k1gInPc2	nápoj
či	či	k8xC	či
polévek	polévka	k1gFnPc2	polévka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
jejich	jejich	k3xOp3gNnSc1	jejich
promíchání	promíchání	k1gNnSc1	promíchání
nebo	nebo	k8xC	nebo
rychlejšího	rychlý	k2eAgNnSc2d2	rychlejší
ochlazení	ochlazení	k1gNnSc2	ochlazení
<g/>
.	.	kIx.	.
</s>
<s>
Lžíci	lžíce	k1gFnSc4	lžíce
lze	lze	k6eAd1	lze
při	při	k7c6	při
vaření	vaření	k1gNnSc6	vaření
použít	použít	k5eAaPmF	použít
i	i	k9	i
k	k	k7c3	k
míchání	míchání	k1gNnSc3	míchání
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
vařečku	vařečka	k1gFnSc4	vařečka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Měrná	měrný	k2eAgFnSc1d1	měrná
jednotka	jednotka	k1gFnSc1	jednotka
==	==	k?	==
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgFnPc1	tento
lžíce	lžíce	k1gFnPc1	lžíce
se	se	k3xPyFc4	se
též	též	k9	též
používají	používat	k5eAaImIp3nP	používat
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
stravy	strava	k1gFnSc2	strava
jako	jako	k8xC	jako
odměrka	odměrka	k1gFnSc1	odměrka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
kuchařských	kuchařský	k2eAgInPc6d1	kuchařský
receptech	recept	k1gInPc6	recept
bývá	bývat	k5eAaImIp3nS	bývat
uvedeno	uveden	k2eAgNnSc1d1	uvedeno
množství	množství	k1gNnSc1	množství
surovin	surovina	k1gFnPc2	surovina
ve	v	k7c6	v
lžících	lžíce	k1gFnPc6	lžíce
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Přidáme	přidat	k5eAaPmIp1nP	přidat
do	do	k7c2	do
hrnce	hrnec	k1gInSc2	hrnec
dvě	dva	k4xCgFnPc4	dva
polévkové	polévkový	k2eAgFnPc4d1	polévková
lžíce	lžíce	k1gFnPc4	lžíce
mléka	mléko	k1gNnSc2	mléko
<g/>
,	,	kIx,	,
Přidáme	přidat	k5eAaPmIp1nP	přidat
čajovou	čajový	k2eAgFnSc4d1	čajová
lžičku	lžička	k1gFnSc4	lžička
soli	sůl	k1gFnSc2	sůl
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Polévkovou	polévkový	k2eAgFnSc4d1	polévková
lžíci	lžíce	k1gFnSc4	lžíce
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
nouzově	nouzově	k6eAd1	nouzově
jako	jako	k8xC	jako
odměrku	odměrka	k1gFnSc4	odměrka
i	i	k9	i
při	při	k7c6	při
absenci	absence	k1gFnSc6	absence
kuchyňské	kuchyňský	k2eAgFnSc2d1	kuchyňská
váhy	váha	k1gFnSc2	váha
–	–	k?	–
vrchovatá	vrchovatý	k2eAgFnSc1d1	vrchovatá
lžíce	lžíce	k1gFnSc1	lžíce
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
lžíce	lžíce	k1gFnSc2	lžíce
"	"	kIx"	"
<g/>
s	s	k7c7	s
čepicí	čepice	k1gFnSc7	čepice
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
představuje	představovat	k5eAaImIp3nS	představovat
cca	cca	kA	cca
25	[number]	k4	25
g	g	kA	g
sypkého	sypký	k2eAgInSc2d1	sypký
materiálu	materiál	k1gInSc2	materiál
(	(	kIx(	(
<g/>
mouky	mouka	k1gFnSc2	mouka
<g/>
,	,	kIx,	,
cukru	cukr	k1gInSc2	cukr
ap.	ap.	kA	ap.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Ostatní	ostatní	k2eAgFnPc4d1	ostatní
lžíce	lžíce	k1gFnPc4	lžíce
==	==	k?	==
</s>
</p>
<p>
<s>
zednická	zednický	k2eAgFnSc1d1	zednická
lžíce	lžíce	k1gFnSc1	lžíce
–	–	k?	–
zednický	zednický	k2eAgInSc4d1	zednický
nástroj	nástroj	k1gInSc4	nástroj
sloužící	sloužící	k2eAgInSc4d1	sloužící
k	k	k7c3	k
míchání	míchání	k1gNnSc3	míchání
a	a	k8xC	a
nanášení	nanášení	k1gNnSc3	nanášení
malty	malta	k1gFnSc2	malta
na	na	k7c4	na
zeď	zeď	k1gFnSc4	zeď
</s>
</p>
<p>
<s>
Obuvnická	obuvnický	k2eAgFnSc1d1	obuvnická
lžíce	lžíce	k1gFnSc1	lžíce
–	–	k?	–
lžíce	lžíce	k1gFnSc1	lžíce
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
snadnější	snadný	k2eAgNnSc4d2	snazší
obouvání	obouvání	k1gNnSc4	obouvání
bot	bota	k1gFnPc2	bota
</s>
</p>
<p>
<s>
strojní	strojní	k2eAgFnPc4d1	strojní
lžíce	lžíce	k1gFnPc4	lžíce
–	–	k?	–
lžíce	lžíce	k1gFnPc4	lžíce
u	u	k7c2	u
bagru	bagr	k1gInSc2	bagr
či	či	k8xC	či
jiného	jiný	k2eAgNnSc2d1	jiné
rypadla	rypadlo	k1gNnSc2	rypadlo
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
pomocí	pomoc	k1gFnSc7	pomoc
se	se	k3xPyFc4	se
nabírá	nabírat	k5eAaImIp3nS	nabírat
a	a	k8xC	a
přenáší	přenášet	k5eAaImIp3nS	přenášet
zemina	zemina	k1gFnSc1	zemina
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Lžičník	lžičník	k1gInSc1	lžičník
</s>
</p>
<p>
<s>
Nůž	nůž	k1gInSc1	nůž
</s>
</p>
<p>
<s>
Vaření	vaření	k1gNnSc1	vaření
</s>
</p>
<p>
<s>
Vařečka	vařečka	k1gFnSc1	vařečka
</s>
</p>
<p>
<s>
Vidlička	vidlička	k1gFnSc1	vidlička
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Věra	Věra	k1gFnSc1	Věra
Vokáčová	Vokáčová	k1gFnSc1	Vokáčová
<g/>
:	:	kIx,	:
Nože	nůž	k1gInPc1	nůž
lžíce	lžíce	k1gFnSc2	lžíce
vidličky	vidlička	k1gFnSc2	vidlička
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
UPM	UPM	kA	UPM
<g/>
.	.	kIx.	.
</s>
<s>
Katalog	katalog	k1gInSc1	katalog
výstavy	výstava	k1gFnSc2	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Uměleckoprůmyslové	uměleckoprůmyslový	k2eAgNnSc1d1	Uměleckoprůmyslové
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1981	[number]	k4	1981
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
lžíce	lžíce	k1gFnSc2	lžíce
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
lžíce	lžíce	k1gFnSc2	lžíce
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
