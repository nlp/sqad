<p>
<s>
Ayrton	Ayrton	k1gInSc1	Ayrton
Senna	Senn	k1gInSc2	Senn
da	da	k?	da
Silva	Silva	k1gFnSc1	Silva
<g/>
,	,	kIx,	,
(	(	kIx(	(
[	[	kIx(	[
<g/>
aˈ	aˈ	k?	aˈ
ˈ	ˈ	k?	ˈ
da	da	k?	da
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1960	[number]	k4	1960
Sã	Sã	k1gFnPc2	Sã
Paulo	Paula	k1gFnSc5	Paula
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1994	[number]	k4	1994
Bologna	Bologna	k1gFnSc1	Bologna
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
brazilský	brazilský	k2eAgMnSc1d1	brazilský
automobilový	automobilový	k2eAgMnSc1d1	automobilový
závodník	závodník	k1gMnSc1	závodník
a	a	k8xC	a
pilot	pilot	k1gMnSc1	pilot
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
získal	získat	k5eAaPmAgInS	získat
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
celkem	celkem	k6eAd1	celkem
třikrát	třikrát	k6eAd1	třikrát
<g/>
.	.	kIx.	.
</s>
<s>
Zahynul	zahynout	k5eAaPmAgInS	zahynout
ve	v	k7c4	v
svých	svůj	k3xOyFgNnPc2	svůj
34	[number]	k4	34
letech	léto	k1gNnPc6	léto
během	během	k7c2	během
tragické	tragický	k2eAgFnSc2d1	tragická
nehody	nehoda	k1gFnSc2	nehoda
při	při	k7c6	při
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
San	San	k1gFnSc2	San
Marina	Marina	k1gFnSc1	Marina
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
závod	závod	k1gInSc1	závod
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
ve	v	k7c6	v
13	[number]	k4	13
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1973	[number]	k4	1973
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
první	první	k4xOgInSc4	první
závod	závod	k1gInSc4	závod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
motokárovým	motokárový	k2eAgMnSc7d1	motokárový
mistrem	mistr	k1gMnSc7	mistr
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc4d1	světový
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
titul	titul	k1gInSc4	titul
však	však	k9	však
v	v	k7c6	v
motokáře	motokára	k1gFnSc6	motokára
nikdy	nikdy	k6eAd1	nikdy
nezískal	získat	k5eNaPmAgMnS	získat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
v	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
formuli	formule	k1gFnSc6	formule
Ford	ford	k1gInSc1	ford
1600	[number]	k4	1600
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
mu	on	k3xPp3gMnSc3	on
nabídnul	nabídnout	k5eAaPmAgMnS	nabídnout
šéf	šéf	k1gMnSc1	šéf
stáje	stáj	k1gFnSc2	stáj
McLaren	McLarno	k1gNnPc2	McLarno
Ron	Ron	k1gMnSc1	Ron
Dennis	Dennis	k1gFnSc2	Dennis
šanci	šance	k1gFnSc4	šance
debutovat	debutovat	k5eAaBmF	debutovat
ve	v	k7c6	v
F	F	kA	F
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Senna	Senna	k6eAd1	Senna
tehdy	tehdy	k6eAd1	tehdy
nabídku	nabídka	k1gFnSc4	nabídka
odmítnul	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
šest	šest	k4xCc4	šest
roků	rok	k1gInPc2	rok
později	pozdě	k6eAd2	pozdě
získal	získat	k5eAaPmAgInS	získat
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
týmem	tým	k1gInSc7	tým
první	první	k4xOgMnSc1	první
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
tří	tři	k4xCgInPc2	tři
mistrovských	mistrovský	k2eAgInPc2d1	mistrovský
titulů	titul	k1gInPc2	titul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Alainem	Alain	k1gMnSc7	Alain
Prostem	Prost	k1gMnSc7	Prost
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Ayrton	Ayrton	k1gInSc4	Ayrton
Senna	Senn	k1gInSc2	Senn
až	až	k9	až
do	do	k7c2	do
VC	VC	kA	VC
Belgie	Belgie	k1gFnSc1	Belgie
2000	[number]	k4	2000
historicky	historicky	k6eAd1	historicky
nejúspěšnější	úspěšný	k2eAgFnSc4d3	nejúspěšnější
dvojici	dvojice	k1gFnSc4	dvojice
jezdců	jezdec	k1gInPc2	jezdec
jedné	jeden	k4xCgFnSc2	jeden
stáje	stáj	k1gFnSc2	stáj
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
sezónách	sezóna	k1gFnPc6	sezóna
1988	[number]	k4	1988
a	a	k8xC	a
1989	[number]	k4	1989
společně	společně	k6eAd1	společně
získali	získat	k5eAaPmAgMnP	získat
celkem	celkem	k6eAd1	celkem
25	[number]	k4	25
vítězství	vítězství	k1gNnPc2	vítězství
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
14	[number]	k4	14
Senna	Senn	k1gInSc2	Senn
a	a	k8xC	a
11	[number]	k4	11
Prost	prost	k2eAgInSc1d1	prost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1984	[number]	k4	1984
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
startoval	startovat	k5eAaBmAgInS	startovat
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
F1	F1	k1gFnSc6	F1
za	za	k7c7	za
volantem	volant	k1gInSc7	volant
vozu	vůz	k1gInSc2	vůz
Toleman	Toleman	k1gMnSc1	Toleman
TG183B	TG183B	k1gMnSc1	TG183B
na	na	k7c6	na
velké	velký	k2eAgFnSc6d1	velká
ceně	cena	k1gFnSc6	cena
Brazílie	Brazílie	k1gFnSc2	Brazílie
v	v	k7c6	v
Rio	Rio	k1gFnSc6	Rio
de	de	k?	de
Janeiru	Janeir	k1gInSc2	Janeir
<g/>
.	.	kIx.	.
</s>
<s>
Kvalifikoval	kvalifikovat	k5eAaBmAgInS	kvalifikovat
se	se	k3xPyFc4	se
na	na	k7c6	na
výborném	výborný	k2eAgNnSc6d1	výborné
šestnáctém	šestnáctý	k4xOgNnSc6	šestnáctý
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
vydržel	vydržet	k5eAaPmAgMnS	vydržet
jen	jen	k9	jen
do	do	k7c2	do
osmého	osmý	k4xOgNnSc2	osmý
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
Toleman	Toleman	k1gMnSc1	Toleman
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
týmy	tým	k1gInPc7	tým
poměrně	poměrně	k6eAd1	poměrně
malý	malý	k2eAgMnSc1d1	malý
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
sestavit	sestavit	k5eAaPmF	sestavit
slušný	slušný	k2eAgInSc4d1	slušný
vůz	vůz	k1gInSc4	vůz
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
body	bod	k1gInPc4	bod
se	se	k3xPyFc4	se
Sennovi	Senn	k1gMnSc3	Senn
podařilo	podařit	k5eAaPmAgNnS	podařit
nasbírat	nasbírat	k5eAaPmF	nasbírat
během	během	k7c2	během
GP	GP	kA	GP
Afriky	Afrika	k1gFnSc2	Afrika
v	v	k7c6	v
Kyalami	Kyala	k1gFnPc7	Kyala
<g/>
,	,	kIx,	,
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
podařilo	podařit	k5eAaPmAgNnS	podařit
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
zapůsobit	zapůsobit	k5eAaPmF	zapůsobit
i	i	k9	i
v	v	k7c6	v
GP	GP	kA	GP
Monaka	Monako	k1gNnSc2	Monako
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
deštěm	dešť	k1gInSc7	dešť
skrápěném	skrápěný	k2eAgInSc6d1	skrápěný
závodě	závod	k1gInSc6	závod
sice	sice	k8xC	sice
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
až	až	k9	až
z	z	k7c2	z
třináctého	třináctý	k4xOgNnSc2	třináctý
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokázal	dokázat	k5eAaPmAgMnS	dokázat
se	se	k3xPyFc4	se
v	v	k7c6	v
krátkém	krátký	k2eAgInSc6d1	krátký
čase	čas	k1gInSc6	čas
propracovat	propracovat	k5eAaPmF	propracovat
až	až	k9	až
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
pozici	pozice	k1gFnSc4	pozice
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
přibližoval	přibližovat	k5eAaImAgMnS	přibližovat
k	k	k7c3	k
prvnímu	první	k4xOgMnSc3	první
Alainu	Alain	k1gMnSc3	Alain
Prostovi	Prosta	k1gMnSc3	Prosta
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
závod	závod	k1gInSc4	závod
pro	pro	k7c4	pro
prudký	prudký	k2eAgInSc4d1	prudký
déšť	déšť	k1gInSc4	déšť
(	(	kIx(	(
a	a	k8xC	a
žádosti	žádost	k1gFnPc4	žádost
vedoucího	vedoucí	k2eAgMnSc2d1	vedoucí
A.	A.	kA	A.
Prosta	prost	k2eAgFnSc1d1	prosta
)	)	kIx)	)
ukončen	ukončit	k5eAaPmNgInS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
šestnácti	šestnáct	k4xCc6	šestnáct
závodech	závod	k1gInPc6	závod
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
devátém	devátý	k4xOgNnSc6	devátý
místě	místo	k1gNnSc6	místo
se	s	k7c7	s
třinácti	třináct	k4xCc7	třináct
body	bod	k1gInPc7	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1985	[number]	k4	1985
===	===	k?	===
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
přestup	přestup	k1gInSc1	přestup
do	do	k7c2	do
týmu	tým	k1gInSc2	tým
Lotus	Lotus	kA	Lotus
nebyl	být	k5eNaImAgInS	být
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
Tolemanem	Toleman	k1gMnSc7	Toleman
měl	mít	k5eAaImAgMnS	mít
Senna	Senn	k1gMnSc4	Senn
za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
stáj	stáj	k1gFnSc4	stáj
ještě	ještě	k9	ještě
rok	rok	k1gInSc4	rok
jezdit	jezdit	k5eAaImF	jezdit
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
podařilo	podařit	k5eAaPmAgNnS	podařit
ze	z	k7c2	z
smlouvy	smlouva	k1gFnSc2	smlouva
vykoupit	vykoupit	k5eAaPmF	vykoupit
a	a	k8xC	a
přestup	přestup	k1gInSc1	přestup
byl	být	k5eAaImAgInS	být
možný	možný	k2eAgInSc1d1	možný
<g/>
.	.	kIx.	.
</s>
<s>
Lotus	Lotus	kA	Lotus
97T	[number]	k4	97T
poháněný	poháněný	k2eAgInSc4d1	poháněný
motorem	motor	k1gInSc7	motor
Renault	renault	k1gInSc1	renault
byl	být	k5eAaImAgInS	být
nejlepším	dobrý	k2eAgInSc7d3	nejlepší
vozem	vůz	k1gInSc7	vůz
této	tento	k3xDgFnSc2	tento
stáje	stáj	k1gFnSc2	stáj
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
neprojektoval	projektovat	k5eNaBmAgMnS	projektovat
Colin	Colin	k2eAgMnSc1d1	Colin
Chapman	Chapman	k1gMnSc1	Chapman
a	a	k8xC	a
Senna	Senna	k1gFnSc1	Senna
jeho	jeho	k3xOp3gFnSc2	jeho
kvality	kvalita	k1gFnSc2	kvalita
dokázal	dokázat	k5eAaPmAgInS	dokázat
patřičně	patřičně	k6eAd1	patřičně
zúročit	zúročit	k5eAaPmF	zúročit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
závodě	závod	k1gInSc6	závod
na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
trati	trať	k1gFnSc6	trať
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
sice	sice	k8xC	sice
závod	závod	k1gInSc4	závod
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
pro	pro	k7c4	pro
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
elektronikou	elektronika	k1gFnSc7	elektronika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
GP	GP	kA	GP
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
jela	jet	k5eAaImAgFnS	jet
na	na	k7c6	na
Portugalském	portugalský	k2eAgInSc6d1	portugalský
Estorilu	Estoril	k1gInSc6	Estoril
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
sezóny	sezóna	k1gFnSc2	sezóna
ho	on	k3xPp3gInSc2	on
trápily	trápit	k5eAaImAgFnP	trápit
mechanické	mechanický	k2eAgFnPc1d1	mechanická
závady	závada	k1gFnPc1	závada
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mu	on	k3xPp3gMnSc3	on
mnohokrát	mnohokrát	k6eAd1	mnohokrát
nedovolily	dovolit	k5eNaPmAgInP	dovolit
obhájit	obhájit	k5eAaPmF	obhájit
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
z	z	k7c2	z
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
také	také	k9	také
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
sezony	sezona	k1gFnSc2	sezona
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
vyhrát	vyhrát	k5eAaPmF	vyhrát
již	již	k9	již
pouze	pouze	k6eAd1	pouze
jednu	jeden	k4xCgFnSc4	jeden
velkou	velký	k2eAgFnSc4d1	velká
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
GP	GP	kA	GP
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ta	ten	k3xDgFnSc1	ten
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
jela	jet	k5eAaImAgFnS	jet
na	na	k7c6	na
mokru	mokro	k1gNnSc3	mokro
<g/>
.	.	kIx.	.
</s>
<s>
Sezonu	sezona	k1gFnSc4	sezona
pak	pak	k6eAd1	pak
dokončil	dokončit	k5eAaPmAgMnS	dokončit
na	na	k7c6	na
čtvrtém	čtvrtý	k4xOgNnSc6	čtvrtý
místě	místo	k1gNnSc6	místo
s	s	k7c7	s
38	[number]	k4	38
body	bod	k1gInPc7	bod
na	na	k7c6	na
kontě	konto	k1gNnSc6	konto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1986	[number]	k4	1986
===	===	k?	===
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
sezona	sezona	k1gFnSc1	sezona
s	s	k7c7	s
Lotusem	Lotus	k1gMnSc7	Lotus
byla	být	k5eAaImAgFnS	být
ještě	ještě	k6eAd1	ještě
lepší	dobrý	k2eAgFnSc1d2	lepší
než	než	k8xS	než
ta	ten	k3xDgFnSc1	ten
první	první	k4xOgFnSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
GP	GP	kA	GP
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
jela	jet	k5eAaImAgFnS	jet
opět	opět	k6eAd1	opět
na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
trati	trať	k1gFnSc6	trať
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
dojel	dojet	k5eAaPmAgMnS	dojet
Senna	Senn	k1gMnSc4	Senn
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
za	za	k7c7	za
svým	svůj	k3xOyFgMnSc7	svůj
krajanem	krajan	k1gMnSc7	krajan
Nelsonem	Nelson	k1gMnSc7	Nelson
Piquetem	Piquet	k1gMnSc7	Piquet
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
závod	závod	k1gInSc1	závod
sezony	sezona	k1gFnSc2	sezona
ve	v	k7c6	v
španělském	španělský	k2eAgInSc6d1	španělský
Jerezu	Jerez	k1gInSc6	Jerez
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
před	před	k7c7	před
Nigelem	Nigel	k1gMnSc7	Nigel
Mansellem	Mansell	k1gMnSc7	Mansell
<g/>
,	,	kIx,	,
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
dokázal	dokázat	k5eAaPmAgMnS	dokázat
porazit	porazit	k5eAaPmF	porazit
o	o	k7c4	o
pár	pár	k4xCyI	pár
decimetrů	decimetr	k1gInPc2	decimetr
na	na	k7c6	na
cílové	cílový	k2eAgFnSc6d1	cílová
pásce	páska	k1gFnSc6	páska
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kariéře	kariéra	k1gFnSc6	kariéra
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
ujal	ujmout	k5eAaPmAgInS	ujmout
vedení	vedení	k1gNnSc4	vedení
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1	mechanická
závady	závada	k1gFnPc1	závada
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
voze	vůz	k1gInSc6	vůz
však	však	k9	však
zavinily	zavinit	k5eAaPmAgFnP	zavinit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc4	tento
rok	rok	k1gInSc4	rok
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
nezasáhl	zasáhnout	k5eNaPmAgMnS	zasáhnout
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
vyhrát	vyhrát	k5eAaPmF	vyhrát
GP	GP	kA	GP
USA	USA	kA	USA
a	a	k8xC	a
několikrát	několikrát	k6eAd1	několikrát
dojet	dojet	k5eAaPmF	dojet
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
a	a	k8xC	a
třetím	třetí	k4xOgInSc6	třetí
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
na	na	k7c4	na
Alaina	Alain	k1gMnSc4	Alain
Prosta	prost	k2eAgMnSc4d1	prost
v	v	k7c6	v
McLarenu	McLaren	k1gInSc6	McLaren
a	a	k8xC	a
na	na	k7c4	na
dvojici	dvojice	k1gFnSc4	dvojice
od	od	k7c2	od
Williamsu	Williams	k1gInSc2	Williams
Mansell	Mansell	k1gInSc1	Mansell
<g/>
,	,	kIx,	,
Piquet	Piquet	k1gInSc1	Piquet
prostě	prostě	k9	prostě
neměl	mít	k5eNaImAgInS	mít
<g/>
.	.	kIx.	.
<g/>
Sezonu	sezona	k1gFnSc4	sezona
tedy	tedy	k9	tedy
opět	opět	k6eAd1	opět
dokončil	dokončit	k5eAaPmAgMnS	dokončit
na	na	k7c6	na
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
však	však	k9	však
již	již	k9	již
s	s	k7c7	s
55	[number]	k4	55
body	bod	k1gInPc7	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1987	[number]	k4	1987
===	===	k?	===
</s>
</p>
<p>
<s>
Start	start	k1gInSc1	start
sezony	sezona	k1gFnSc2	sezona
se	se	k3xPyFc4	se
Sennovi	Senn	k1gMnSc3	Senn
příliš	příliš	k6eAd1	příliš
nepodařil	podařit	k5eNaPmAgMnS	podařit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
dokázal	dokázat	k5eAaPmAgMnS	dokázat
vyhrát	vyhrát	k5eAaPmF	vyhrát
čtvrtou	čtvrtá	k1gFnSc4	čtvrtá
a	a	k8xC	a
pátou	pátá	k1gFnSc4	pátá
GP	GP	kA	GP
v	v	k7c6	v
Monaku	Monako	k1gNnSc6	Monako
a	a	k8xC	a
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
ujal	ujmout	k5eAaPmAgMnS	ujmout
se	se	k3xPyFc4	se
vedení	vedení	k1gNnSc4	vedení
v	v	k7c6	v
šampionátu	šampionát	k1gInSc6	šampionát
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
sváděl	svádět	k5eAaImAgMnS	svádět
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
krajanem	krajan	k1gMnSc7	krajan
Piquetem	Piquet	k1gMnSc7	Piquet
a	a	k8xC	a
Britem	Brit	k1gMnSc7	Brit
Mansellem	Mansell	k1gMnSc7	Mansell
<g/>
,	,	kIx,	,
jedoucími	jedoucí	k2eAgMnPc7d1	jedoucí
s	s	k7c7	s
vozy	vůz	k1gInPc7	vůz
Williams	Williamsa	k1gFnPc2	Williamsa
Honda	honda	k1gFnSc1	honda
<g/>
.	.	kIx.	.
</s>
<s>
Naději	naděje	k1gFnSc4	naděje
na	na	k7c4	na
výhru	výhra	k1gFnSc4	výhra
v	v	k7c6	v
šampionátu	šampionát	k1gInSc6	šampionát
držel	držet	k5eAaImAgMnS	držet
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
definitivně	definitivně	k6eAd1	definitivně
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
ztratil	ztratit	k5eAaPmAgMnS	ztratit
po	po	k7c6	po
GP	GP	kA	GP
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
kontrole	kontrola	k1gFnSc6	kontrola
Sennova	Sennův	k2eAgInSc2d1	Sennův
vozu	vůz	k1gInSc2	vůz
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
brzdové	brzdový	k2eAgInPc1d1	brzdový
bubny	buben	k1gInPc1	buben
jeho	on	k3xPp3gInSc2	on
Lotusu	Lotus	k1gInSc2	Lotus
jsou	být	k5eAaImIp3nP	být
širší	široký	k2eAgInSc4d2	širší
než	než	k8xS	než
legálně	legálně	k6eAd1	legálně
povolené	povolený	k2eAgInPc1d1	povolený
a	a	k8xC	a
ze	z	k7c2	z
závodu	závod	k1gInSc2	závod
byl	být	k5eAaImAgInS	být
diskvalifikován	diskvalifikován	k2eAgInSc1d1	diskvalifikován
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
diskvalifikaci	diskvalifikace	k1gFnSc6	diskvalifikace
skončil	skončit	k5eAaPmAgMnS	skončit
v	v	k7c6	v
konečném	konečný	k2eAgNnSc6d1	konečné
pořadí	pořadí	k1gNnSc6	pořadí
na	na	k7c6	na
třetím	třetí	k4xOgNnSc6	třetí
místě	místo	k1gNnSc6	místo
s	s	k7c7	s
57	[number]	k4	57
body	bod	k1gInPc7	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1988	[number]	k4	1988
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vypršení	vypršení	k1gNnSc6	vypršení
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
Lotusem	Lotus	k1gMnSc7	Lotus
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
Senna	Senn	k1gMnSc4	Senn
do	do	k7c2	do
týmu	tým	k1gInSc2	tým
McLaren	McLarna	k1gFnPc2	McLarna
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
týmovým	týmový	k2eAgMnSc7d1	týmový
kolegou	kolega	k1gMnSc7	kolega
dvojnásobného	dvojnásobný	k2eAgMnSc2d1	dvojnásobný
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
Alaina	Alain	k1gMnSc2	Alain
Prosta	prost	k2eAgFnSc1d1	prosta
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
dvěma	dva	k4xCgInPc7	dva
jezdci	jezdec	k1gInPc7	jezdec
nebyl	být	k5eNaImAgInS	být
nijak	nijak	k6eAd1	nijak
idylický	idylický	k2eAgInSc1d1	idylický
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
potvrdit	potvrdit	k5eAaPmF	potvrdit
především	především	k9	především
následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Úchvatné	úchvatný	k2eAgNnSc1d1	úchvatné
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
souboji	souboj	k1gInSc6	souboj
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
McLaren	McLarna	k1gFnPc2	McLarna
nechal	nechat	k5eAaPmAgInS	nechat
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
závodit	závodit	k5eAaImF	závodit
dva	dva	k4xCgInPc4	dva
z	z	k7c2	z
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
pilotů	pilot	k1gInPc2	pilot
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
nebyl	být	k5eNaImAgMnS	být
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Professor	Professor	k1gMnSc1	Professor
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
přezdívalo	přezdívat	k5eAaImAgNnS	přezdívat
Prostovi	Prosta	k1gMnSc3	Prosta
vůbec	vůbec	k9	vůbec
nadšený	nadšený	k2eAgInSc4d1	nadšený
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
sezony	sezona	k1gFnSc2	sezona
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
daleko	daleko	k6eAd1	daleko
lépe	dobře	k6eAd2	dobře
Prost	prost	k2eAgInSc1d1	prost
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Senna	Senn	k1gMnSc4	Senn
dokázal	dokázat	k5eAaPmAgInS	dokázat
bodovat	bodovat	k5eAaImF	bodovat
pouze	pouze	k6eAd1	pouze
vítězstvím	vítězství	k1gNnSc7	vítězství
v	v	k7c6	v
San	San	k1gFnSc6	San
Marinu	Marina	k1gFnSc4	Marina
<g/>
.	.	kIx.	.
</s>
<s>
Zlom	zlom	k1gInSc1	zlom
přišel	přijít	k5eAaPmAgInS	přijít
v	v	k7c6	v
GP	GP	kA	GP
Monaka	Monako	k1gNnPc4	Monako
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Senna	Senno	k1gNnPc4	Senno
chyboval	chybovat	k5eAaImAgMnS	chybovat
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
když	když	k8xS	když
měl	mít	k5eAaImAgInS	mít
náskok	náskok	k1gInSc4	náskok
téměř	téměř	k6eAd1	téměř
minutu	minuta	k1gFnSc4	minuta
na	na	k7c4	na
druhého	druhý	k4xOgMnSc4	druhý
Prosta	prost	k2eAgFnSc1d1	prosta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Monaku	Monako	k1gNnSc6	Monako
se	se	k3xPyFc4	se
Ayrton	Ayrton	k1gInSc1	Ayrton
schoval	schovat	k5eAaPmAgInS	schovat
před	před	k7c7	před
světem	svět	k1gInSc7	svět
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
a	a	k8xC	a
ukázal	ukázat	k5eAaPmAgMnS	ukázat
se	se	k3xPyFc4	se
až	až	k9	až
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
při	při	k7c6	při
dalším	další	k2eAgInSc6d1	další
závodě	závod	k1gInSc6	závod
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
dlouho	dlouho	k6eAd1	dlouho
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zastávka	zastávka	k1gFnSc1	zastávka
v	v	k7c6	v
boxech	box	k1gInPc6	box
navíc	navíc	k6eAd1	navíc
ho	on	k3xPp3gNnSc4	on
opět	opět	k6eAd1	opět
stála	stát	k5eAaImAgFnS	stát
vítězství	vítězství	k1gNnSc4	vítězství
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
Prosta	prost	k2eAgFnSc1d1	prosta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
závodě	závod	k1gInSc6	závod
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
byl	být	k5eAaImAgInS	být
bodový	bodový	k2eAgInSc1d1	bodový
stav	stav	k1gInSc1	stav
šampionátu	šampionát	k1gInSc2	šampionát
Prost	prost	k2eAgMnSc1d1	prost
-	-	kIx~	-
33	[number]	k4	33
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
Senna	Senn	k1gInSc2	Senn
-15	-15	k4	-15
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
přišla	přijít	k5eAaPmAgFnS	přijít
neskutečná	skutečný	k2eNgFnSc1d1	neskutečná
série	série	k1gFnSc1	série
Ayrtonových	Ayrtonový	k2eAgNnPc2d1	Ayrtonový
prvenství	prvenství	k1gNnPc2	prvenství
<g/>
,	,	kIx,	,
když	když	k8xS	když
z	z	k7c2	z
následujících	následující	k2eAgInPc2d1	následující
6	[number]	k4	6
závodů	závod	k1gInPc2	závod
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
plných	plný	k2eAgNnPc2d1	plné
5	[number]	k4	5
a	a	k8xC	a
na	na	k7c4	na
Prosta	prost	k2eAgNnPc4d1	prosto
nestačil	stačit	k5eNaBmAgInS	stačit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
domácím	domácí	k2eAgInSc6d1	domácí
závodě	závod	k1gInSc6	závod
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
Paul	Paul	k1gMnSc1	Paul
Ricard	Ricard	k1gMnSc1	Ricard
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
vítězství	vítězství	k1gNnSc2	vítězství
McLarenu	McLaren	k1gInSc2	McLaren
se	se	k3xPyFc4	se
přerušila	přerušit	k5eAaPmAgFnS	přerušit
až	až	k9	až
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
na	na	k7c4	na
slavné	slavný	k2eAgFnPc4d1	slavná
Monze	Monze	k1gFnPc4	Monze
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
19	[number]	k4	19
kol	kolo	k1gNnPc2	kolo
před	před	k7c7	před
cílem	cíl	k1gInSc7	cíl
po	po	k7c6	po
explozi	exploze	k1gFnSc6	exploze
motoru	motor	k1gInSc2	motor
odstoupil	odstoupit	k5eAaPmAgInS	odstoupit
Alain	Alain	k2eAgInSc1d1	Alain
Prost	prost	k2eAgInSc1d1	prost
a	a	k8xC	a
2	[number]	k4	2
kola	kolo	k1gNnSc2	kolo
před	před	k7c7	před
cílem	cíl	k1gInSc7	cíl
po	po	k7c6	po
kolizi	kolize	k1gFnSc6	kolize
s	s	k7c7	s
Jean-Louis	Jean-Louis	k1gFnSc7	Jean-Louis
Schlesserem	Schlesser	k1gMnSc7	Schlesser
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
o	o	k7c4	o
kolo	kolo	k1gNnSc4	kolo
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
i	i	k9	i
Senna	Senn	k1gMnSc4	Senn
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Ferrari	ferrari	k1gNnSc4	ferrari
to	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
double	double	k1gInSc4	double
pro	pro	k7c4	pro
zesnulého	zesnulý	k1gMnSc4	zesnulý
zakladatele	zakladatel	k1gMnSc2	zakladatel
Enza	Enzus	k1gMnSc2	Enzus
Ferrariho	Ferrari	k1gMnSc2	Ferrari
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
startu	start	k1gInSc2	start
do	do	k7c2	do
VC	VC	kA	VC
Japonska	Japonsko	k1gNnSc2	Japonsko
měl	mít	k5eAaImAgInS	mít
Senna	Senna	k1gFnSc1	Senna
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
motorem	motor	k1gInSc7	motor
a	a	k8xC	a
musel	muset	k5eAaImAgMnS	muset
dohánět	dohánět	k5eAaImF	dohánět
velkou	velký	k2eAgFnSc4d1	velká
ztrátu	ztráta	k1gFnSc4	ztráta
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
jako	jako	k9	jako
zdánlivě	zdánlivě	k6eAd1	zdánlivě
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
"	"	kIx"	"
<g/>
RainManovi	RainManův	k2eAgMnPc1d1	RainManův
<g/>
"	"	kIx"	"
doslova	doslova	k6eAd1	doslova
zapršelo	zapršet	k5eAaPmAgNnS	zapršet
štěstí	štěstí	k1gNnSc1	štěstí
a	a	k8xC	a
i	i	k9	i
právě	právě	k9	právě
díky	díky	k7c3	díky
dešti	dešť	k1gInSc3	dešť
Prosta	prost	k2eAgFnSc1d1	prosta
dokázal	dokázat	k5eAaPmAgInS	dokázat
dojet	dojet	k2eAgInSc4d1	dojet
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
zdolat	zdolat	k5eAaPmF	zdolat
<g/>
.	.	kIx.	.
</s>
<s>
Senna	Senna	k6eAd1	Senna
tak	tak	k9	tak
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kariéře	kariéra	k1gFnSc6	kariéra
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
celý	celý	k2eAgInSc1d1	celý
šampionát	šampionát	k1gInSc1	šampionát
F	F	kA	F
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
s	s	k7c7	s
90	[number]	k4	90
dosaženými	dosažený	k2eAgInPc7d1	dosažený
body	bod	k1gInPc7	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1989	[number]	k4	1989
===	===	k?	===
</s>
</p>
<p>
<s>
I	i	k9	i
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
boj	boj	k1gInSc1	boj
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgMnPc7	dva
jezdci	jezdec	k1gMnPc7	jezdec
jedoucími	jedoucí	k2eAgMnPc7d1	jedoucí
s	s	k7c7	s
monoposty	monopost	k1gInPc7	monopost
McLaren	McLarna	k1gFnPc2	McLarna
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vyrovnaný	vyrovnaný	k2eAgInSc1d1	vyrovnaný
boj	boj	k1gInSc1	boj
pak	pak	k6eAd1	pak
vyvrcholil	vyvrcholit	k5eAaPmAgInS	vyvrcholit
během	během	k7c2	během
předposlední	předposlední	k2eAgFnSc2d1	předposlední
GP	GP	kA	GP
v	v	k7c4	v
japonské	japonský	k2eAgFnPc4d1	japonská
Suzuce	Suzuce	k1gFnPc4	Suzuce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
titul	titul	k1gInSc4	titul
získal	získat	k5eAaPmAgInS	získat
Prost	prost	k2eAgInSc1d1	prost
<g/>
,	,	kIx,	,
po	po	k7c6	po
neblaze	blaze	k6eNd1	blaze
proslulém	proslulý	k2eAgInSc6d1	proslulý
incidentu	incident	k1gInSc6	incident
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
šikan	šikana	k1gFnPc2	šikana
se	se	k3xPyFc4	se
Senna	Senno	k1gNnSc2	Senno
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c6	o
předjetí	předjetí	k1gNnSc6	předjetí
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
srazil	srazit	k5eAaPmAgMnS	srazit
s	s	k7c7	s
Prostem	Prost	k1gMnSc7	Prost
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
však	však	k9	však
nepatrně	patrně	k6eNd1	patrně
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
nelogicky	logicky	k6eNd1	logicky
zatočil	zatočit	k5eAaPmAgMnS	zatočit
k	k	k7c3	k
Sennovi	Senn	k1gMnSc3	Senn
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
ven	ven	k6eAd1	ven
z	z	k7c2	z
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
manévr	manévr	k1gInSc1	manévr
byl	být	k5eAaImAgInS	být
velice	velice	k6eAd1	velice
riskantní	riskantní	k2eAgMnSc1d1	riskantní
a	a	k8xC	a
Senna	Senna	k1gFnSc1	Senna
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
velice	velice	k6eAd1	velice
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
<g/>
,	,	kIx,	,
při	při	k7c6	při
hlubším	hluboký	k2eAgInSc6d2	hlubší
pohledu	pohled	k1gInSc6	pohled
se	se	k3xPyFc4	se
však	však	k9	však
dá	dát	k5eAaPmIp3nS	dát
pochopit	pochopit	k5eAaPmF	pochopit
<g/>
.	.	kIx.	.
</s>
<s>
Prost	prost	k2eAgInSc1d1	prost
měl	mít	k5eAaImAgInS	mít
předpoklad	předpoklad	k1gInSc1	předpoklad
získat	získat	k5eAaPmF	získat
titul	titul	k1gInSc4	titul
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
Senna	Senna	k1gFnSc1	Senna
tento	tento	k3xDgInSc4	tento
závod	závod	k1gInSc4	závod
nevyhraje	vyhrát	k5eNaPmIp3nS	vyhrát
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
byl	být	k5eAaImAgInS	být
důvod	důvod	k1gInSc1	důvod
Sennova	Sennův	k2eAgInSc2d1	Sennův
kroku	krok	k1gInSc2	krok
a	a	k8xC	a
možná	možná	k9	možná
i	i	k9	i
důvod	důvod	k1gInSc4	důvod
Prostova	Prostův	k2eAgNnSc2d1	Prostův
nepatrného	patrný	k2eNgNnSc2d1	nepatrné
zatočení	zatočení	k1gNnSc2	zatočení
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
však	však	k9	však
můžeme	moct	k5eAaImIp1nP	moct
jen	jen	k9	jen
spekulovat	spekulovat	k5eAaImF	spekulovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sennovi	Senn	k1gMnSc3	Senn
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
podařilo	podařit	k5eAaPmAgNnS	podařit
dojet	dojet	k5eAaPmF	dojet
do	do	k7c2	do
depa	depo	k1gNnSc2	depo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgMnS	nechat
svůj	svůj	k3xOyFgInSc4	svůj
vůz	vůz	k1gInSc4	vůz
opravit	opravit	k5eAaPmF	opravit
a	a	k8xC	a
opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
zařadil	zařadit	k5eAaPmAgMnS	zařadit
do	do	k7c2	do
závodu	závod	k1gInSc2	závod
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
závod	závod	k1gInSc1	závod
dokonce	dokonce	k9	dokonce
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
nepochopitelně	pochopitelně	k6eNd1	pochopitelně
diskvalifikován	diskvalifikován	k2eAgInSc1d1	diskvalifikován
pro	pro	k7c4	pro
nedovolené	dovolený	k2eNgNnSc4d1	nedovolené
zkrácení	zkrácení	k1gNnSc4	zkrácení
zatáčky	zatáčka	k1gFnSc2	zatáčka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
šampionátu	šampionát	k1gInSc2	šampionát
tak	tak	k6eAd1	tak
nakonec	nakonec	k6eAd1	nakonec
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
s	s	k7c7	s
šedesáti	šedesát	k4xCc7	šedesát
body	bod	k1gInPc7	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1990	[number]	k4	1990
===	===	k?	===
</s>
</p>
<p>
<s>
Boj	boj	k1gInSc1	boj
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
dvěma	dva	k4xCgInPc7	dva
závodníky	závodník	k1gMnPc4	závodník
neustal	ustat	k5eNaPmAgMnS	ustat
ani	ani	k8xC	ani
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
a	a	k8xC	a
incidenty	incident	k1gInPc1	incident
<g/>
,	,	kIx,	,
podobné	podobný	k2eAgInPc1d1	podobný
tomu	ten	k3xDgInSc3	ten
ze	z	k7c2	z
Suzuky	Suzuk	k1gInPc1	Suzuk
89	[number]	k4	89
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
v	v	k7c6	v
pozměněné	pozměněný	k2eAgFnSc6d1	pozměněná
verzi	verze	k1gFnSc6	verze
opakovaly	opakovat	k5eAaImAgInP	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
nedaroval	darovat	k5eNaPmAgMnS	darovat
druhému	druhý	k4xOgInSc3	druhý
nic	nic	k3yNnSc1	nic
zadarmo	zadarmo	k6eAd1	zadarmo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgMnS	být
týmovým	týmový	k2eAgMnSc7d1	týmový
kolegou	kolega	k1gMnSc7	kolega
Ayrtona	Ayrton	k1gMnSc2	Ayrton
Senny	Senny	k?	Senny
Rakušan	Rakušan	k1gMnSc1	Rakušan
Gerhard	Gerhard	k1gMnSc1	Gerhard
Berger	Berger	k1gMnSc1	Berger
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Prosta	prost	k2eAgMnSc4d1	prost
<g/>
,	,	kIx,	,
jezdícího	jezdící	k2eAgMnSc4d1	jezdící
tento	tento	k3xDgInSc1	tento
rok	rok	k1gInSc4	rok
za	za	k7c7	za
Ferrari	Ferrari	k1gMnSc7	Ferrari
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
blízkým	blízký	k2eAgMnSc7d1	blízký
přítelem	přítel	k1gMnSc7	přítel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Šampionát	šampionát	k1gInSc1	šampionát
nakonec	nakonec	k6eAd1	nakonec
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
Senna	Senn	k1gInSc2	Senn
se	s	k7c7	s
78	[number]	k4	78
body	bod	k1gInPc7	bod
před	před	k7c7	před
druhým	druhý	k4xOgInSc7	druhý
Prostem	Prost	k1gInSc7	Prost
</s>
</p>
<p>
<s>
===	===	k?	===
1991	[number]	k4	1991
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
byl	být	k5eAaImAgInS	být
Sennovým	Sennův	k2eAgMnSc7d1	Sennův
stájovým	stájový	k2eAgMnSc7d1	stájový
kolegou	kolega	k1gMnSc7	kolega
opět	opět	k6eAd1	opět
Gerhard	Gerhard	k1gMnSc1	Gerhard
Berger	Berger	k1gMnSc1	Berger
<g/>
.	.	kIx.	.
</s>
<s>
Prost	prost	k2eAgMnSc1d1	prost
nadále	nadále	k6eAd1	nadále
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
ve	v	k7c6	v
Ferrari	ferrari	k1gNnSc6	ferrari
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
rok	rok	k1gInSc4	rok
zde	zde	k6eAd1	zde
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
jako	jako	k8xS	jako
ten	ten	k3xDgInSc1	ten
loňský	loňský	k2eAgInSc1d1	loňský
a	a	k8xC	a
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
tak	tak	k6eAd1	tak
více	hodně	k6eAd2	hodně
než	než	k8xS	než
on	on	k3xPp3gMnSc1	on
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
Brit	Brit	k1gMnSc1	Brit
Nigel	Nigel	k1gMnSc1	Nigel
Mansell	Mansell	k1gMnSc1	Mansell
jezdící	jezdící	k2eAgInSc4d1	jezdící
za	za	k7c4	za
tým	tým	k1gInSc4	tým
Williams	Williamsa	k1gFnPc2	Williamsa
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
také	také	k9	také
nakonec	nakonec	k6eAd1	nakonec
obsazuje	obsazovat	k5eAaImIp3nS	obsazovat
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
za	za	k7c7	za
Ayrtonem	Ayrton	k1gInSc7	Ayrton
Sennou	senný	k2eAgFnSc4d1	senná
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vítězí	vítězit	k5eAaImIp3nS	vítězit
s	s	k7c7	s
96	[number]	k4	96
body	bod	k1gInPc7	bod
<g/>
.	.	kIx.	.
</s>
<s>
Alain	Alain	k1gInSc1	Alain
Prost	prost	k2eAgInSc1d1	prost
končí	končit	k5eAaImIp3nS	končit
tento	tento	k3xDgInSc1	tento
rok	rok	k1gInSc1	rok
až	až	k9	až
na	na	k7c6	na
pátém	pátý	k4xOgNnSc6	pátý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1992	[number]	k4	1992
===	===	k?	===
</s>
</p>
<p>
<s>
Sezona	sezona	k1gFnSc1	sezona
1992	[number]	k4	1992
neprobíhala	probíhat	k5eNaImAgFnS	probíhat
podle	podle	k7c2	podle
Sennových	Sennův	k2eAgFnPc2d1	Sennova
představ	představa	k1gFnPc2	představa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šampionátu	šampionát	k1gInSc6	šampionát
skončil	skončit	k5eAaPmAgInS	skončit
až	až	k9	až
na	na	k7c6	na
čtvrtém	čtvrtý	k4xOgNnSc6	čtvrtý
místě	místo	k1gNnSc6	místo
s	s	k7c7	s
pouhými	pouhý	k2eAgInPc7d1	pouhý
50	[number]	k4	50
body	bod	k1gInPc7	bod
<g/>
.	.	kIx.	.
</s>
<s>
Znechucení	znechucení	k1gNnSc1	znechucení
a	a	k8xC	a
zklamání	zklamání	k1gNnSc1	zklamání
z	z	k7c2	z
neschopnosti	neschopnost	k1gFnSc2	neschopnost
McLarenu	McLarena	k1gFnSc4	McLarena
soupeřit	soupeřit	k5eAaImF	soupeřit
s	s	k7c7	s
Williamsem	Williams	k1gInSc7	Williams
se	se	k3xPyFc4	se
také	také	k9	také
nepochybně	pochybně	k6eNd1	pochybně
projevilo	projevit	k5eAaPmAgNnS	projevit
na	na	k7c6	na
nerozhodnosti	nerozhodnost	k1gFnSc6	nerozhodnost
při	při	k7c6	při
uzavíraní	uzavíraný	k2eAgMnPc1d1	uzavíraný
kontraktu	kontrakt	k1gInSc3	kontrakt
na	na	k7c4	na
rok	rok	k1gInSc4	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
roku	rok	k1gInSc2	rok
testoval	testovat	k5eAaImAgMnS	testovat
Senna	Senn	k1gMnSc4	Senn
pro	pro	k7c4	pro
tým	tým	k1gInSc4	tým
Marlboro	Marlboro	k1gNnSc1	Marlboro
Penske	Pensk	k1gInSc2	Pensk
v	v	k7c4	v
IndyCar	IndyCar	k1gInSc4	IndyCar
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
zvažoval	zvažovat	k5eAaImAgMnS	zvažovat
dočasný	dočasný	k2eAgInSc4d1	dočasný
odchod	odchod	k1gInSc4	odchod
z	z	k7c2	z
F	F	kA	F
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
McLaren	McLarna	k1gFnPc2	McLarna
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
potýkal	potýkat	k5eAaImAgMnS	potýkat
s	s	k7c7	s
problémem	problém	k1gInSc7	problém
týkajícím	týkající	k2eAgInSc7d1	týkající
se	se	k3xPyFc4	se
dodávky	dodávka	k1gFnPc4	dodávka
motorů	motor	k1gInPc2	motor
<g/>
,	,	kIx,	,
Honda	honda	k1gFnSc1	honda
ukončila	ukončit	k5eAaPmAgFnS	ukončit
svojí	svojit	k5eAaImIp3nP	svojit
dodávku	dodávka	k1gFnSc4	dodávka
<g/>
.	.	kIx.	.
</s>
<s>
Šéf	šéf	k1gMnSc1	šéf
týmu	tým	k1gInSc2	tým
McLaren	McLarna	k1gFnPc2	McLarna
Ron	Ron	k1gMnSc1	Ron
Dennis	Dennis	k1gFnSc2	Dennis
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
zajistit	zajistit	k5eAaPmF	zajistit
dodání	dodání	k1gNnSc4	dodání
motorů	motor	k1gInPc2	motor
Renault	renault	k1gInSc1	renault
<g/>
,	,	kIx,	,
montovaných	montovaný	k2eAgFnPc6d1	montovaná
do	do	k7c2	do
dominantních	dominantní	k2eAgInPc2d1	dominantní
vozů	vůz	k1gInPc2	vůz
Williams	Williamsa	k1gFnPc2	Williamsa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
tento	tento	k3xDgInSc1	tento
obchod	obchod	k1gInSc1	obchod
ztroskotal	ztroskotat	k5eAaPmAgInS	ztroskotat
<g/>
,	,	kIx,	,
zajistil	zajistit	k5eAaPmAgMnS	zajistit
Dennis	Dennis	k1gFnSc4	Dennis
dodávku	dodávka	k1gFnSc4	dodávka
motorů	motor	k1gInPc2	motor
Ford	ford	k1gInSc1	ford
-	-	kIx~	-
avšak	avšak	k8xC	avšak
s	s	k7c7	s
nižším	nízký	k2eAgInSc7d2	nižší
výkonem	výkon	k1gInSc7	výkon
než	než	k8xS	než
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
používal	používat	k5eAaImAgInS	používat
tým	tým	k1gInSc1	tým
Benetton	Benetton	k1gInSc1	Benetton
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1993	[number]	k4	1993
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
předsezónních	předsezónní	k2eAgInPc2d1	předsezónní
testů	test	k1gInPc2	test
nového	nový	k2eAgInSc2d1	nový
vozu	vůz	k1gInSc2	vůz
McLaren	McLarna	k1gFnPc2	McLarna
došel	dojít	k5eAaPmAgMnS	dojít
Senna	Senn	k1gInSc2	Senn
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
šasi	šasi	k1gNnSc1	šasi
je	být	k5eAaImIp3nS	být
opravdu	opravdu	k6eAd1	opravdu
velice	velice	k6eAd1	velice
dobré	dobrý	k2eAgNnSc1d1	dobré
<g/>
.	.	kIx.	.
</s>
<s>
Věděl	vědět	k5eAaImAgMnS	vědět
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
motoru	motor	k1gInSc2	motor
bude	být	k5eAaImBp3nS	být
chybět	chybět	k5eAaImF	chybět
potřebný	potřebný	k2eAgInSc4d1	potřebný
výkon	výkon	k1gInSc4	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
proto	proto	k8xC	proto
podepsat	podepsat	k5eAaPmF	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
sezonu	sezona	k1gFnSc4	sezona
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
jezdit	jezdit	k5eAaImF	jezdit
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
závod	závod	k1gInSc4	závod
za	za	k7c7	za
závodem	závod	k1gInSc7	závod
za	za	k7c4	za
milion	milion	k4xCgInSc4	milion
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
každý	každý	k3xTgInSc4	každý
odjetý	odjetý	k2eAgInSc4d1	odjetý
závod	závod	k1gInSc4	závod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sezona	sezona	k1gFnSc1	sezona
nedopadla	dopadnout	k5eNaPmAgFnS	dopadnout
pro	pro	k7c4	pro
McLaren	McLarna	k1gFnPc2	McLarna
tak	tak	k9	tak
tragicky	tragicky	k6eAd1	tragicky
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
vypadalo	vypadat	k5eAaImAgNnS	vypadat
<g/>
.	.	kIx.	.
</s>
<s>
Sennovi	Senn	k1gMnSc3	Senn
se	se	k3xPyFc4	se
celkem	celkem	k6eAd1	celkem
povedl	povést	k5eAaPmAgInS	povést
úvod	úvod	k1gInSc4	úvod
sezony	sezona	k1gFnSc2	sezona
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
závodě	závod	k1gInSc6	závod
skončil	skončit	k5eAaPmAgMnS	skončit
druhý	druhý	k4xOgMnSc1	druhý
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
dvou	dva	k4xCgNnPc6	dva
dokázal	dokázat	k5eAaPmAgMnS	dokázat
dokonce	dokonce	k9	dokonce
vyhrát	vyhrát	k5eAaPmF	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
prestižní	prestižní	k2eAgFnSc7d1	prestižní
GP	GP	kA	GP
Monaka	Monako	k1gNnPc1	Monako
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gFnSc4	jeho
kořistí	kořistit	k5eAaImIp3nP	kořistit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
závodě	závod	k1gInSc6	závod
<g/>
,	,	kIx,	,
šestém	šestý	k4xOgNnSc6	šestý
v	v	k7c6	v
šampionátu	šampionát	k1gInSc6	šampionát
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgMnS	vést
Senna	Senn	k1gMnSc4	Senn
celý	celý	k2eAgInSc1d1	celý
šampionát	šampionát	k1gInSc1	šampionát
před	před	k7c7	před
svým	svůj	k3xOyFgMnSc7	svůj
hlavním	hlavní	k2eAgMnSc7d1	hlavní
sokem	sok	k1gMnSc7	sok
Alainem	Alain	k1gMnSc7	Alain
Prostem	Prost	k1gMnSc7	Prost
<g/>
,	,	kIx,	,
jedoucímu	jedoucí	k2eAgInSc3d1	jedoucí
ve	v	k7c6	v
voze	vůz	k1gInSc6	vůz
Williams	Williams	k1gInSc1	Williams
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
také	také	k9	také
Senna	Senno	k1gNnSc2	Senno
upsal	upsat	k5eAaPmAgMnS	upsat
McLarenu	McLarena	k1gFnSc4	McLarena
k	k	k7c3	k
dokončení	dokončení	k1gNnSc3	dokončení
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ani	ani	k9	ani
on	on	k3xPp3gMnSc1	on
však	však	k9	však
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
vyhrát	vyhrát	k5eAaPmF	vyhrát
tento	tento	k3xDgInSc4	tento
nerovný	rovný	k2eNgInSc4d1	nerovný
boj	boj	k1gInSc4	boj
s	s	k7c7	s
přesilou	přesila	k1gFnSc7	přesila
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mistrovský	mistrovský	k2eAgInSc1d1	mistrovský
titul	titul	k1gInSc1	titul
nakonec	nakonec	k6eAd1	nakonec
získal	získat	k5eAaPmAgInS	získat
Prost	prost	k2eAgInSc1d1	prost
<g/>
.	.	kIx.	.
</s>
<s>
Senna	Senna	k6eAd1	Senna
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
73	[number]	k4	73
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1994	[number]	k4	1994
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
týmu	tým	k1gInSc2	tým
Williams	Williamsa	k1gFnPc2	Williamsa
<g/>
.	.	kIx.	.
</s>
<s>
Nevyhrál	vyhrát	k5eNaPmAgMnS	vyhrát
již	již	k9	již
ani	ani	k9	ani
jednu	jeden	k4xCgFnSc4	jeden
velkou	velký	k2eAgFnSc4d1	velká
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
dvě	dva	k4xCgFnPc4	dva
GP	GP	kA	GP
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
a	a	k8xC	a
v	v	k7c6	v
Pacifiku	Pacifik	k1gInSc6	Pacifik
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
GP	GP	kA	GP
San	San	k1gFnSc1	San
Marina	Marina	k1gFnSc1	Marina
v	v	k7c6	v
Imole	Imola	k1gFnSc6	Imola
1.5	[number]	k4	1.5
<g/>
.1994	.1994	k4	.1994
jeho	jeho	k3xOp3gNnSc2	jeho
monopost	monopost	k1gInSc1	monopost
vyjel	vyjet	k5eAaPmAgInS	vyjet
z	z	k7c2	z
trati	trať	k1gFnSc2	trať
a	a	k8xC	a
při	při	k7c6	při
následném	následný	k2eAgInSc6d1	následný
nárazu	náraz	k1gInSc6	náraz
do	do	k7c2	do
betonové	betonový	k2eAgFnSc2d1	betonová
zdi	zeď	k1gFnSc2	zeď
Ayrton	Ayrton	k1gInSc1	Ayrton
Senna	Senn	k1gInSc2	Senn
okamžitě	okamžitě	k6eAd1	okamžitě
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
pilotů	pilot	k1gInPc2	pilot
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jezdci	jezdec	k1gMnPc7	jezdec
dokonce	dokonce	k9	dokonce
anketu	anketa	k1gFnSc4	anketa
o	o	k7c4	o
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
jezdce	jezdec	k1gMnSc4	jezdec
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tragická	tragický	k2eAgFnSc1d1	tragická
nehoda	nehoda	k1gFnSc1	nehoda
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
příběh	příběh	k1gInSc1	příběh
má	mít	k5eAaImIp3nS	mít
zdánlivou	zdánlivý	k2eAgFnSc4d1	zdánlivá
předehru	předehra	k1gFnSc4	předehra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prvním	první	k4xOgInSc6	první
kvalifikačním	kvalifikační	k2eAgInSc6d1	kvalifikační
tréninku	trénink	k1gInSc6	trénink
na	na	k7c6	na
VC	VC	kA	VC
San	San	k1gFnSc1	San
Marína	Marína	k1gFnSc1	Marína
se	se	k3xPyFc4	se
dvojice	dvojice	k1gFnSc1	dvojice
týmových	týmový	k2eAgMnPc2d1	týmový
kolegů	kolega	k1gMnPc2	kolega
od	od	k7c2	od
McLarenu	McLaren	k1gInSc2	McLaren
tj.	tj.	kA	tj.
Ayrton	Ayrton	k1gInSc1	Ayrton
Senna	Senno	k1gNnSc2	Senno
a	a	k8xC	a
Gerhard	Gerhard	k1gMnSc1	Gerhard
Berger	Berger	k1gMnSc1	Berger
šla	jít	k5eAaImAgFnS	jít
podívat	podívat	k5eAaPmF	podívat
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
nehody	nehoda	k1gFnSc2	nehoda
druhého	druhý	k4xOgMnSc2	druhý
jmenovaného	jmenovaný	k2eAgMnSc2d1	jmenovaný
při	při	k7c6	při
závodě	závod	k1gInSc6	závod
v	v	k7c6	v
minulém	minulý	k2eAgInSc6d1	minulý
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Berger	Berger	k1gMnSc1	Berger
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
Scuderie	Scuderie	k1gFnSc2	Scuderie
Ferrari	Ferrari	k1gMnSc2	Ferrari
boural	bourat	k5eAaImAgMnS	bourat
na	na	k7c6	na
vjezdu	vjezd	k1gInSc6	vjezd
zatáčky	zatáčka	k1gFnSc2	zatáčka
Tamburello	Tamburello	k1gNnSc1	Tamburello
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
vedou	vést	k5eAaImIp3nP	vést
neúnavnou	únavný	k2eNgFnSc4d1	neúnavná
diskuzi	diskuze	k1gFnSc4	diskuze
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
by	by	kYmCp3nS	by
nešla	jít	k5eNaImAgFnS	jít
posunout	posunout	k5eAaPmF	posunout
zeď	zeď	k1gFnSc1	zeď
v	v	k7c6	v
zatáčce	zatáčka	k1gFnSc6	zatáčka
Tamburello	Tamburello	k1gNnSc4	Tamburello
o	o	k7c4	o
něco	něco	k3yInSc4	něco
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
právě	právě	k6eAd1	právě
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
za	za	k7c7	za
zdí	zeď	k1gFnSc7	zeď
nachází	nacházet	k5eAaImIp3nS	nacházet
dodnes	dodnes	k6eAd1	dodnes
říčka	říčka	k1gFnSc1	říčka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
oba	dva	k4xCgMnPc1	dva
odchází	odcházet	k5eAaImIp3nP	odcházet
do	do	k7c2	do
paddocku	paddock	k1gInSc2	paddock
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Bergera	Berger	k1gMnSc2	Berger
se	se	k3xPyFc4	se
nacházeli	nacházet	k5eAaImAgMnP	nacházet
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
inkriminovaného	inkriminovaný	k2eAgNnSc2d1	inkriminované
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
o	o	k7c4	o
4	[number]	k4	4
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
stalo	stát	k5eAaPmAgNnS	stát
Sennovi	Sennův	k2eAgMnPc1d1	Sennův
osudným	osudný	k2eAgInSc7d1	osudný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
neděle	neděle	k1gFnSc1	neděle
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
na	na	k7c4	na
okruh	okruh	k1gInSc4	okruh
Ayrton	Ayrton	k1gInSc1	Ayrton
Senna	Senn	k1gInSc2	Senn
dorazil	dorazit	k5eAaPmAgInS	dorazit
před	před	k7c7	před
osmou	osmý	k4xOgFnSc7	osmý
hodinou	hodina	k1gFnSc7	hodina
ranní	ranní	k2eAgFnSc7d1	ranní
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příjezdu	příjezd	k1gInSc6	příjezd
krátce	krátce	k6eAd1	krátce
hovořil	hovořit	k5eAaImAgMnS	hovořit
s	s	k7c7	s
Niki	Niki	k1gNnSc7	Niki
Laudou	Lauda	k1gMnSc7	Lauda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c4	v
půl	půl	k1xP	půl
desáté	desátá	k1gFnSc2	desátá
pak	pak	k6eAd1	pak
začal	začít	k5eAaPmAgInS	začít
zahřívací	zahřívací	k2eAgInSc4d1	zahřívací
trénink	trénink	k1gInSc4	trénink
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
za	za	k7c4	za
celý	celý	k2eAgInSc4d1	celý
závodní	závodní	k2eAgInSc4d1	závodní
víkend	víkend	k1gInSc4	víkend
byl	být	k5eAaImAgMnS	být
Senna	Senn	k1gInSc2	Senn
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
než	než	k8xS	než
celý	celý	k2eAgInSc1d1	celý
zbytek	zbytek	k1gInSc1	zbytek
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
pak	pak	k9	pak
skončil	skončit	k5eAaPmAgMnS	skončit
jeho	jeho	k3xOp3gMnSc1	jeho
týmový	týmový	k2eAgMnSc1d1	týmový
kolega	kolega	k1gMnSc1	kolega
Damon	Damon	k1gMnSc1	Damon
Hill	Hill	k1gMnSc1	Hill
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
ztráta	ztráta	k1gFnSc1	ztráta
činila	činit	k5eAaImAgFnS	činit
téměř	téměř	k6eAd1	téměř
jednu	jeden	k4xCgFnSc4	jeden
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Michael	Michael	k1gMnSc1	Michael
Schumacher	Schumachra	k1gFnPc2	Schumachra
zůstal	zůstat	k5eAaPmAgMnS	zůstat
na	na	k7c6	na
jedenáctém	jedenáctý	k4xOgInSc6	jedenáctý
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
sekundy	sekunda	k1gFnPc4	sekunda
pozadu	pozadu	k6eAd1	pozadu
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
patrně	patrně	k6eAd1	patrně
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
jízdou	jízda	k1gFnSc7	jízda
s	s	k7c7	s
plnou	plný	k2eAgFnSc7d1	plná
nádrží	nádrž	k1gFnSc7	nádrž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c4	v
jedenáct	jedenáct	k4xCc4	jedenáct
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
45	[number]	k4	45
minut	minuta	k1gFnPc2	minuta
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
byl	být	k5eAaImAgInS	být
warm-up	warmp	k1gInSc1	warm-up
zakončen	zakončit	k5eAaPmNgInS	zakončit
<g/>
,	,	kIx,	,
dorazil	dorazit	k5eAaPmAgMnS	dorazit
Ayrton	Ayrton	k1gInSc4	Ayrton
Senna	Senn	k1gInSc2	Senn
na	na	k7c4	na
jezdecký	jezdecký	k2eAgInSc4d1	jezdecký
brífink	brífink	k1gInSc4	brífink
pořádaný	pořádaný	k2eAgInSc4d1	pořádaný
v	v	k7c6	v
kontrolní	kontrolní	k2eAgFnSc6d1	kontrolní
věži	věž	k1gFnSc6	věž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
programu	program	k1gInSc6	program
byla	být	k5eAaImAgFnS	být
i	i	k9	i
minuta	minuta	k1gFnSc1	minuta
ticha	ticho	k1gNnSc2	ticho
za	za	k7c2	za
Rolanda	Rolando	k1gNnSc2	Rolando
Ratzenbergra	Ratzenbergr	k1gMnSc2	Ratzenbergr
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
tragicky	tragicky	k6eAd1	tragicky
zahynul	zahynout	k5eAaPmAgMnS	zahynout
během	během	k7c2	během
sobotní	sobotní	k2eAgFnSc2d1	sobotní
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
minutě	minuta	k1gFnSc6	minuta
ticha	ticho	k1gNnSc2	ticho
začala	začít	k5eAaPmAgFnS	začít
rutinní	rutinní	k2eAgFnSc1d1	rutinní
diskuse	diskuse	k1gFnSc1	diskuse
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
brífinku	brífink	k1gInSc2	brífink
hovořil	hovořit	k5eAaImAgMnS	hovořit
Senna	Senn	k1gMnSc4	Senn
se	s	k7c7	s
Schumacherem	Schumacher	k1gMnSc7	Schumacher
<g/>
,	,	kIx,	,
Bergerem	Berger	k1gMnSc7	Berger
a	a	k8xC	a
Alboretem	Alboret	k1gMnSc7	Alboret
<g/>
.	.	kIx.	.
</s>
<s>
Senna	Senna	k6eAd1	Senna
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
uspořádat	uspořádat	k5eAaPmF	uspořádat
schůzku	schůzka	k1gFnSc4	schůzka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
větší	veliký	k2eAgFnSc1d2	veliký
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
jezdců	jezdec	k1gMnPc2	jezdec
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Předběžně	předběžně	k6eAd1	předběžně
se	se	k3xPyFc4	se
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c6	na
setkání	setkání	k1gNnSc6	setkání
na	na	k7c6	na
příští	příští	k2eAgFnSc6d1	příští
velké	velký	k2eAgFnSc6d1	velká
ceně	cena	k1gFnSc6	cena
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gNnSc7	jejíž
dějištěm	dějiště	k1gNnSc7	dějiště
bylo	být	k5eAaImAgNnS	být
Monako	Monako	k1gNnSc1	Monako
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Senna	Senno	k1gNnSc2	Senno
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
motorhomu	motorhom	k1gInSc2	motorhom
týmu	tým	k1gInSc2	tým
Williams	Williamsa	k1gFnPc2	Williamsa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
setkání	setkání	k1gNnSc1	setkání
s	s	k7c7	s
hosty	host	k1gMnPc4	host
sponzorů	sponzor	k1gMnPc2	sponzor
<g/>
,	,	kIx,	,
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
také	také	k9	také
jeho	jeho	k3xOp3gMnSc1	jeho
týmový	týmový	k2eAgMnSc1d1	týmový
kolega	kolega	k1gMnSc1	kolega
Hill	Hill	k1gMnSc1	Hill
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c4	v
jednu	jeden	k4xCgFnSc4	jeden
hodinu	hodina	k1gFnSc4	hodina
pak	pak	k6eAd1	pak
začala	začít	k5eAaPmAgFnS	začít
předstartovní	předstartovní	k2eAgFnSc1d1	předstartovní
procedura	procedura	k1gFnSc1	procedura
<g/>
.	.	kIx.	.
</s>
<s>
Doktor	doktor	k1gMnSc1	doktor
Watkins	Watkinsa	k1gFnPc2	Watkinsa
ještě	ještě	k9	ještě
jednou	jednou	k6eAd1	jednou
projel	projet	k5eAaPmAgMnS	projet
okruh	okruh	k1gInSc4	okruh
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zkontroloval	zkontrolovat	k5eAaPmAgMnS	zkontrolovat
rozestavení	rozestavení	k1gNnSc4	rozestavení
zásahových	zásahový	k2eAgInPc2d1	zásahový
a	a	k8xC	a
sanitních	sanitní	k2eAgInPc2d1	sanitní
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c4	v
tři	tři	k4xCgFnPc4	tři
čtvrtě	čtvrt	k1gFnPc4	čtvrt
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
startuje	startovat	k5eAaBmIp3nS	startovat
Senna	Senno	k1gNnSc2	Senno
v	v	k7c6	v
garáži	garáž	k1gFnSc6	garáž
svůj	svůj	k3xOyFgInSc4	svůj
monopost	monopost	k1gInSc4	monopost
<g/>
,	,	kIx,	,
vyjíždí	vyjíždět	k5eAaImIp3nS	vyjíždět
ven	ven	k6eAd1	ven
a	a	k8xC	a
za	za	k7c2	za
asistence	asistence	k1gFnSc2	asistence
dvou	dva	k4xCgMnPc2	dva
mechaniků	mechanik	k1gMnPc2	mechanik
umisťuje	umisťovat	k5eAaImIp3nS	umisťovat
svůj	svůj	k3xOyFgInSc4	svůj
Williams	Williams	k1gInSc4	Williams
FW16	FW16	k1gFnSc2	FW16
na	na	k7c4	na
určenou	určený	k2eAgFnSc4d1	určená
startovní	startovní	k2eAgFnSc4d1	startovní
pozici	pozice	k1gFnSc4	pozice
–	–	k?	–
zcela	zcela	k6eAd1	zcela
vpředu	vpředu	k6eAd1	vpředu
na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Hlasatel	hlasatel	k1gMnSc1	hlasatel
mezitím	mezitím	k6eAd1	mezitím
představil	představit	k5eAaPmAgMnS	představit
všechny	všechen	k3xTgMnPc4	všechen
jezdce	jezdec	k1gMnPc4	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Senna	Senna	k6eAd1	Senna
si	se	k3xPyFc3	se
nasadil	nasadit	k5eAaPmAgMnS	nasadit
nehořlavou	hořlavý	k2eNgFnSc4d1	nehořlavá
kuklu	kukla	k1gFnSc4	kukla
<g/>
,	,	kIx,	,
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
nasadil	nasadit	k5eAaPmAgMnS	nasadit
helmu	helma	k1gFnSc4	helma
a	a	k8xC	a
na	na	k7c4	na
pokyn	pokyn	k1gInSc4	pokyn
startéra	startér	k1gMnSc2	startér
prudce	prudko	k6eAd1	prudko
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
do	do	k7c2	do
zahřívacího	zahřívací	k2eAgNnSc2d1	zahřívací
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
objetí	objetí	k1gNnSc1	objetí
jednoho	jeden	k4xCgNnSc2	jeden
kola	kolo	k1gNnSc2	kolo
se	se	k3xPyFc4	se
had	had	k1gMnSc1	had
pětadvaceti	pětadvacet	k4xCc2	pětadvacet
aut	auto	k1gNnPc2	auto
opět	opět	k6eAd1	opět
seřadil	seřadit	k5eAaPmAgInS	seřadit
na	na	k7c6	na
startovním	startovní	k2eAgInSc6d1	startovní
roštu	rošt	k1gInSc6	rošt
připraven	připravit	k5eAaPmNgMnS	připravit
ke	k	k7c3	k
startu	start	k1gInSc3	start
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c4	v
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
se	se	k3xPyFc4	se
rozsvítila	rozsvítit	k5eAaPmAgNnP	rozsvítit
zelená	zelený	k2eAgNnPc1d1	zelené
světla	světlo	k1gNnPc1	světlo
a	a	k8xC	a
Senna	Senno	k1gNnPc4	Senno
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
vpřed	vpřed	k6eAd1	vpřed
<g/>
,	,	kIx,	,
předvedl	předvést	k5eAaPmAgMnS	předvést
mistrný	mistrný	k2eAgInSc4d1	mistrný
start	start	k1gInSc4	start
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
první	první	k4xOgFnSc6	první
zatáčce	zatáčka	k1gFnSc6	zatáčka
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
vedení	vedení	k1gNnSc2	vedení
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
řady	řada	k1gFnPc4	řada
dále	daleko	k6eAd2	daleko
startujícímu	startující	k2eAgMnSc3d1	startující
Jyrki	Jyrk	k1gMnSc3	Jyrk
Järvilehtovi	Järvileht	k1gMnSc3	Järvileht
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
odstartovat	odstartovat	k5eAaPmF	odstartovat
a	a	k8xC	a
do	do	k7c2	do
jeho	jeho	k3xOp3gInSc2	jeho
vozu	vůz	k1gInSc2	vůz
<g/>
,	,	kIx,	,
stojícího	stojící	k2eAgInSc2d1	stojící
na	na	k7c6	na
startovní	startovní	k2eAgFnSc6d1	startovní
rovince	rovinka	k1gFnSc6	rovinka
<g/>
,	,	kIx,	,
naráží	narážet	k5eAaImIp3nS	narážet
z	z	k7c2	z
předposlední	předposlední	k2eAgFnSc2d1	předposlední
řady	řada	k1gFnSc2	řada
startující	startující	k2eAgMnSc1d1	startující
Portugalec	Portugalec	k1gMnSc1	Portugalec
Pedro	Pedro	k1gNnSc4	Pedro
Lamy	lama	k1gMnSc2	lama
<g/>
.	.	kIx.	.
</s>
<s>
Benetton	Benetton	k1gInSc1	Benetton
Lehta	Leht	k1gInSc2	Leht
je	být	k5eAaImIp3nS	být
odhozen	odhozen	k2eAgInSc1d1	odhozen
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
dopředu	dopředu	k6eAd1	dopředu
<g/>
,	,	kIx,	,
trosky	troska	k1gFnSc2	troska
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
kol	kolo	k1gNnPc2	kolo
jsou	být	k5eAaImIp3nP	být
vymrštěny	vymrštit	k5eAaPmNgInP	vymrštit
přes	přes	k7c4	přes
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
bariéru	bariéra	k1gFnSc4	bariéra
a	a	k8xC	a
dopadají	dopadat	k5eAaImIp3nP	dopadat
mezi	mezi	k7c7	mezi
diváky	divák	k1gMnPc7	divák
<g/>
.	.	kIx.	.
</s>
<s>
Třem	tři	k4xCgFnPc3	tři
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
zranění	zranění	k1gNnPc1	zranění
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
jednomu	jeden	k4xCgMnSc3	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
vážné	vážná	k1gFnSc2	vážná
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
piloti	pilot	k1gMnPc1	pilot
jsou	být	k5eAaImIp3nP	být
nezraněni	zraněn	k2eNgMnPc1d1	nezraněn
<g/>
,	,	kIx,	,
všeobecně	všeobecně	k6eAd1	všeobecně
se	se	k3xPyFc4	se
očekává	očekávat	k5eAaImIp3nS	očekávat
zastavení	zastavení	k1gNnSc1	zastavení
závodu	závod	k1gInSc2	závod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
se	se	k3xPyFc4	se
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
trať	trať	k1gFnSc4	trať
však	však	k9	však
vyjíždí	vyjíždět	k5eAaImIp3nP	vyjíždět
safety	safeta	k1gFnPc1	safeta
car	car	k1gMnSc1	car
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgNnSc3	jenž
mohou	moct	k5eAaImIp3nP	moct
traťoví	traťový	k2eAgMnPc1d1	traťový
komisaři	komisar	k1gMnPc1	komisar
odklidit	odklidit	k5eAaPmF	odklidit
trosky	troska	k1gFnPc4	troska
ležící	ležící	k2eAgFnPc4d1	ležící
na	na	k7c6	na
startovní	startovní	k2eAgFnSc6d1	startovní
rovince	rovinka	k1gFnSc6	rovinka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
deseti	deset	k4xCc6	deset
minutách	minuta	k1gFnPc6	minuta
je	být	k5eAaImIp3nS	být
připraven	připraven	k2eAgInSc1d1	připraven
restart	restart	k1gInSc1	restart
<g/>
.	.	kIx.	.
</s>
<s>
Box	box	k1gInSc1	box
Williamsu	Williams	k1gInSc2	Williams
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
skutečnosti	skutečnost	k1gFnSc6	skutečnost
Sennu	Senn	k1gMnSc3	Senn
informuje	informovat	k5eAaBmIp3nS	informovat
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
příjem	příjem	k1gInSc4	příjem
<g/>
,	,	kIx,	,
žádné	žádný	k3yNgInPc4	žádný
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
vozem	vůz	k1gInSc7	vůz
nehlásí	hlásit	k5eNaImIp3nP	hlásit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Závod	závod	k1gInSc1	závod
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
šestým	šestý	k4xOgNnSc7	šestý
kolem	kolo	k1gNnSc7	kolo
<g/>
,	,	kIx,	,
Senna	Senna	k1gFnSc1	Senna
předvádí	předvádět	k5eAaImIp3nS	předvádět
vynikající	vynikající	k2eAgInSc4d1	vynikající
start	start	k1gInSc4	start
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Schumacherem	Schumachero	k1gNnSc7	Schumachero
unikají	unikat	k5eAaImIp3nP	unikat
zbytku	zbytek	k1gInSc3	zbytek
startovního	startovní	k2eAgNnSc2d1	startovní
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sedmnáct	sedmnáct	k4xCc1	sedmnáct
minut	minuta	k1gFnPc2	minuta
po	po	k7c6	po
startu	start	k1gInSc6	start
VC	VC	kA	VC
San	San	k1gFnPc2	San
Marína	Marína	k1gFnSc1	Marína
dvojice	dvojice	k1gFnSc1	dvojice
jezdců	jezdec	k1gMnPc2	jezdec
protíná	protínat	k5eAaImIp3nS	protínat
cílovou	cílový	k2eAgFnSc4d1	cílová
pásku	páska	k1gFnSc4	páska
6	[number]	k4	6
<g/>
.	.	kIx.	.
kola	kolo	k1gNnSc2	kolo
závodu	závod	k1gInSc2	závod
a	a	k8xC	a
Senna	Senn	k1gInSc2	Senn
se	se	k3xPyFc4	se
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
zatáčce	zatáčka	k1gFnSc3	zatáčka
Tamburello	Tamburello	k1gNnSc1	Tamburello
<g/>
,	,	kIx,	,
zatáčka	zatáčka	k1gFnSc1	zatáčka
se	se	k3xPyFc4	se
stáčí	stáčet	k5eAaImIp3nS	stáčet
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
vlevo	vlevo	k6eAd1	vlevo
<g/>
,	,	kIx,	,
Senna	Senna	k1gFnSc1	Senna
přejíždí	přejíždět	k5eAaImIp3nS	přejíždět
příčné	příčný	k2eAgInPc4d1	příčný
pásy	pás	k1gInPc4	pás
nově	nově	k6eAd1	nově
položeného	položený	k2eAgInSc2d1	položený
asfaltu	asfalt	k1gInSc2	asfalt
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
předchozím	předchozí	k2eAgNnSc6d1	předchozí
kole	kolo	k1gNnSc6	kolo
reaguje	reagovat	k5eAaBmIp3nS	reagovat
jeho	on	k3xPp3gInSc4	on
monopost	monopost	k1gInSc4	monopost
na	na	k7c4	na
náhlou	náhlý	k2eAgFnSc4d1	náhlá
změnu	změna	k1gFnSc4	změna
povrchu	povrch	k1gInSc2	povrch
rojem	roj	k1gInSc7	roj
jisker	jiskra	k1gFnPc2	jiskra
vyletujícím	vyletující	k2eAgFnPc3d1	vyletující
ze	z	k7c2	z
zádi	záď	k1gFnSc2	záď
jeho	on	k3xPp3gInSc2	on
Williamsu	Williams	k1gInSc2	Williams
<g/>
.	.	kIx.	.
</s>
<s>
Jede	jet	k5eAaImIp3nS	jet
rychlostí	rychlost	k1gFnSc7	rychlost
310	[number]	k4	310
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
O	o	k7c4	o
několik	několik	k4yIc4	několik
sekund	sekunda	k1gFnPc2	sekunda
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
dráha	dráha	k1gFnSc1	dráha
jeho	jeho	k3xOp3gInSc2	jeho
monopostu	monopost	k1gInSc2	monopost
narovnává	narovnávat	k5eAaImIp3nS	narovnávat
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
zeď	zeď	k1gFnSc1	zeď
lemující	lemující	k2eAgFnSc4d1	lemující
zatáčku	zatáčka	k1gFnSc4	zatáčka
se	se	k3xPyFc4	se
nezadržitelně	zadržitelně	k6eNd1	zadržitelně
blíží	blížit	k5eAaImIp3nS	blížit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
necelých	celý	k2eNgFnPc2d1	necelá
dvou	dva	k4xCgFnPc2	dva
sekund	sekunda	k1gFnPc2	sekunda
přejel	přejet	k5eAaPmAgInS	přejet
jeho	jeho	k3xOp3gInSc1	jeho
monopost	monopost	k1gInSc1	monopost
travnatý	travnatý	k2eAgInSc1d1	travnatý
pás	pás	k1gInSc4	pás
<g/>
,	,	kIx,	,
krátkou	krátký	k2eAgFnSc4d1	krátká
únikovou	únikový	k2eAgFnSc4d1	úniková
zónu	zóna	k1gFnSc4	zóna
a	a	k8xC	a
přesně	přesně	k6eAd1	přesně
17	[number]	k4	17
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
23	[number]	k4	23
sekund	sekunda	k1gFnPc2	sekunda
po	po	k7c6	po
startu	start	k1gInSc6	start
závodu	závod	k1gInSc2	závod
přichází	přicházet	k5eAaImIp3nS	přicházet
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
impakt	impakt	k1gInSc4	impakt
do	do	k7c2	do
nechráněné	chráněný	k2eNgFnSc2d1	nechráněná
betonové	betonový	k2eAgFnSc2d1	betonová
zdi	zeď	k1gFnSc2	zeď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Necelou	celý	k2eNgFnSc4d1	necelá
půl	půl	k6eAd1	půl
minutu	minuta	k1gFnSc4	minuta
po	po	k7c6	po
nárazu	náraz	k1gInSc6	náraz
přibíhá	přibíhat	k5eAaImIp3nS	přibíhat
k	k	k7c3	k
Ayrtonovi	Ayrton	k1gMnSc3	Ayrton
první	první	k4xOgMnSc1	první
traťový	traťový	k2eAgMnSc1d1	traťový
komisař	komisař	k1gMnSc1	komisař
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
chvilce	chvilka	k1gFnSc6	chvilka
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
další	další	k2eAgMnPc1d1	další
záchranáři	záchranář	k1gMnPc1	záchranář
s	s	k7c7	s
hasicími	hasicí	k2eAgInPc7d1	hasicí
přístroji	přístroj	k1gInPc7	přístroj
<g/>
,	,	kIx,	,
k	k	k7c3	k
monopostu	monopost	k1gInSc3	monopost
se	se	k3xPyFc4	se
však	však	k9	však
nepřibližují	přibližovat	k5eNaImIp3nP	přibližovat
a	a	k8xC	a
čekají	čekat	k5eAaImIp3nP	čekat
na	na	k7c4	na
příjezd	příjezd	k1gInSc4	příjezd
lékařského	lékařský	k2eAgInSc2d1	lékařský
vozu	vůz	k1gInSc2	vůz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Závod	závod	k1gInSc1	závod
je	být	k5eAaImIp3nS	být
červenými	červený	k2eAgFnPc7d1	červená
vlajkami	vlajka	k1gFnPc7	vlajka
zastaven	zastavit	k5eAaPmNgInS	zastavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Minutu	minuta	k1gFnSc4	minuta
a	a	k8xC	a
čtvrt	čtvrt	k1gFnSc4	čtvrt
po	po	k7c6	po
srážce	srážka	k1gFnSc6	srážka
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
lékařské	lékařský	k2eAgNnSc4d1	lékařské
vozidlo	vozidlo	k1gNnSc4	vozidlo
s	s	k7c7	s
profesorem	profesor	k1gMnSc7	profesor
Watkinsem	Watkins	k1gMnSc7	Watkins
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
lékařům	lékař	k1gMnPc3	lékař
podaří	podařit	k5eAaPmIp3nS	podařit
sundat	sundat	k5eAaPmF	sundat
přilbu	přilba	k1gFnSc4	přilba
a	a	k8xC	a
speciálním	speciální	k2eAgInSc7d1	speciální
límcem	límec	k1gInSc7	límec
zafixovat	zafixovat	k5eAaPmF	zafixovat
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
vyprošťují	vyprošťovat	k5eAaImIp3nP	vyprošťovat
Sennu	Senna	k1gFnSc4	Senna
z	z	k7c2	z
vraku	vrak	k1gInSc2	vrak
a	a	k8xC	a
pokládají	pokládat	k5eAaImIp3nP	pokládat
ho	on	k3xPp3gMnSc4	on
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
kousek	kousek	k1gInSc4	kousek
dál	daleko	k6eAd2	daleko
právě	právě	k6eAd1	právě
na	na	k7c6	na
závodní	závodní	k2eAgFnSc6d1	závodní
dráze	dráha	k1gFnSc6	dráha
přistává	přistávat	k5eAaImIp3nS	přistávat
helikoptéra	helikoptéra	k1gFnSc1	helikoptéra
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
je	být	k5eAaImIp3nS	být
Senna	Senen	k2eAgFnSc1d1	Senna
po	po	k7c6	po
chvíli	chvíle	k1gFnSc6	chvíle
naložen	naložen	k2eAgInSc1d1	naložen
a	a	k8xC	a
za	za	k7c2	za
potlesku	potlesk	k1gInSc2	potlesk
tribun	tribuna	k1gFnPc2	tribuna
odlétá	odlétat	k5eAaPmIp3nS	odlétat
do	do	k7c2	do
boloňské	boloňský	k2eAgFnSc2d1	Boloňská
nemocnice	nemocnice	k1gFnSc2	nemocnice
Maggiore	Maggior	k1gMnSc5	Maggior
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
dorazí	dorazit	k5eAaPmIp3nS	dorazit
za	za	k7c4	za
dalších	další	k2eAgFnPc2d1	další
18	[number]	k4	18
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
letu	let	k1gInSc2	let
Sennu	Senn	k1gInSc2	Senn
postihne	postihnout	k5eAaPmIp3nS	postihnout
srdeční	srdeční	k2eAgFnSc1d1	srdeční
zástava	zástava	k1gFnSc1	zástava
<g/>
,	,	kIx,	,
činnost	činnost	k1gFnSc1	činnost
srdce	srdce	k1gNnSc2	srdce
však	však	k8xC	však
lékaři	lékař	k1gMnPc1	lékař
dokážou	dokázat	k5eAaPmIp3nP	dokázat
<g/>
,	,	kIx,	,
alespoň	alespoň	k9	alespoň
prozatím	prozatím	k6eAd1	prozatím
<g/>
,	,	kIx,	,
obnovit	obnovit	k5eAaPmF	obnovit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Záběry	záběr	k1gInPc1	záběr
televizních	televizní	k2eAgFnPc2d1	televizní
kamer	kamera	k1gFnPc2	kamera
mezitím	mezitím	k6eAd1	mezitím
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
otisk	otisk	k1gInSc4	otisk
dvou	dva	k4xCgFnPc2	dva
pneumatik	pneumatika	k1gFnPc2	pneumatika
na	na	k7c4	na
bílé	bílý	k2eAgFnPc4d1	bílá
zdi	zeď	k1gFnPc4	zeď
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
nárazu	náraz	k1gInSc2	náraz
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k4xCyI	pár
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
trosky	troska	k1gFnSc2	troska
monopostu	monopost	k1gInSc2	monopost
nelze	lze	k6eNd1	lze
na	na	k7c6	na
betonové	betonový	k2eAgFnSc6d1	betonová
zemi	zem	k1gFnSc6	zem
přehlédnout	přehlédnout	k5eAaPmF	přehlédnout
velkou	velký	k2eAgFnSc4d1	velká
červenou	červený	k2eAgFnSc4d1	červená
skvrnu	skvrna	k1gFnSc4	skvrna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Helikoptéra	helikoptéra	k1gFnSc1	helikoptéra
zatím	zatím	k6eAd1	zatím
doletěla	doletět	k5eAaPmAgFnS	doletět
k	k	k7c3	k
nemocnici	nemocnice	k1gFnSc3	nemocnice
Maggiore	Maggior	k1gMnSc5	Maggior
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Senna	Senna	k1gFnSc1	Senna
prodělává	prodělávat	k5eAaImIp3nS	prodělávat
další	další	k2eAgFnSc4d1	další
zástavu	zástava	k1gFnSc4	zástava
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
lékaři	lékař	k1gMnPc1	lékař
i	i	k9	i
tentokrát	tentokrát	k6eAd1	tentokrát
dokázali	dokázat	k5eAaPmAgMnP	dokázat
srdeční	srdeční	k2eAgInSc4d1	srdeční
rytmus	rytmus	k1gInSc4	rytmus
obnovit	obnovit	k5eAaPmF	obnovit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Primářka	primářka	k1gFnSc1	primářka
jednotky	jednotka	k1gFnSc2	jednotka
intenzivní	intenzivní	k2eAgFnSc2d1	intenzivní
péče	péče	k1gFnSc2	péče
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Marie	Marie	k1gFnSc1	Marie
Teresa	Teresa	k1gFnSc1	Teresa
Fiandri	Fiandr	k1gFnSc2	Fiandr
později	pozdě	k6eAd2	pozdě
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
zástavy	zástava	k1gFnSc2	zástava
srdce	srdce	k1gNnSc1	srdce
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
nevratnému	vratný	k2eNgNnSc3d1	nevratné
poškození	poškození	k1gNnSc3	poškození
mozku	mozek	k1gInSc2	mozek
způsobeného	způsobený	k2eAgInSc2d1	způsobený
nedostatkem	nedostatek	k1gInSc7	nedostatek
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
u	u	k7c2	u
Senny	Senny	k?	Senny
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
několikanásobnou	několikanásobný	k2eAgFnSc4d1	několikanásobná
frakturu	fraktura	k1gFnSc4	fraktura
lebky	lebka	k1gFnSc2	lebka
<g/>
,	,	kIx,	,
prasklou	prasklý	k2eAgFnSc4d1	prasklá
spánkovou	spánkový	k2eAgFnSc4d1	spánková
tepnu	tepna	k1gFnSc4	tepna
a	a	k8xC	a
krvácení	krvácení	k1gNnSc4	krvácení
ze	z	k7c2	z
spodiny	spodina	k1gFnSc2	spodina
lebeční	lebeční	k2eAgFnSc2d1	lebeční
<g/>
.	.	kIx.	.
</s>
<s>
Řekla	říct	k5eAaPmAgFnS	říct
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ayrton	Ayrton	k1gInSc1	Ayrton
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
čas	čas	k1gInSc4	čas
v	v	k7c6	v
bezvědomí	bezvědomí	k1gNnSc6	bezvědomí
a	a	k8xC	a
že	že	k8xS	že
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
nárazu	náraz	k1gInSc6	náraz
upadl	upadnout	k5eAaPmAgMnS	upadnout
do	do	k7c2	do
těžkého	těžký	k2eAgNnSc2d1	těžké
kómatu	kóma	k1gNnSc2	kóma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
závodní	závodní	k2eAgFnSc6d1	závodní
dráze	dráha	k1gFnSc6	dráha
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
připravuje	připravovat	k5eAaImIp3nS	připravovat
restart	restart	k1gInSc4	restart
závodu	závod	k1gInSc2	závod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jakmile	jakmile	k8xS	jakmile
bylo	být	k5eAaImAgNnS	být
znovu	znovu	k6eAd1	znovu
odstartováno	odstartován	k2eAgNnSc4d1	odstartováno
<g/>
,	,	kIx,	,
vedení	vedení	k1gNnSc4	vedení
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
Gerhard	Gerhard	k1gMnSc1	Gerhard
Berger	Berger	k1gMnSc1	Berger
ve	v	k7c6	v
Ferrari	ferrari	k1gNnSc6	ferrari
<g/>
,	,	kIx,	,
po	po	k7c6	po
devíti	devět	k4xCc6	devět
kolech	kolo	k1gNnPc6	kolo
však	však	k9	však
neodolal	odolat	k5eNaPmAgMnS	odolat
náporu	nápor	k1gInSc3	nápor
Schumachera	Schumacher	k1gMnSc2	Schumacher
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jedenácti	jedenáct	k4xCc6	jedenáct
kolech	kolo	k1gNnPc6	kolo
pak	pak	k6eAd1	pak
Berger	Berger	k1gMnSc1	Berger
pro	pro	k7c4	pro
technickou	technický	k2eAgFnSc4d1	technická
závadu	závada	k1gFnSc4	závada
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c4	v
čtyři	čtyři	k4xCgFnPc4	čtyři
hodiny	hodina	k1gFnPc4	hodina
doktorka	doktorka	k1gFnSc1	doktorka
Fiandri	Fiandre	k1gFnSc4	Fiandre
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
Sennu	Senna	k1gFnSc4	Senna
za	za	k7c4	za
klinicky	klinicky	k6eAd1	klinicky
mrtvého	mrtvý	k2eAgMnSc4d1	mrtvý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
pak	pak	k6eAd1	pak
Schumacher	Schumachra	k1gFnPc2	Schumachra
vítězně	vítězně	k6eAd1	vítězně
protíná	protínat	k5eAaImIp3nS	protínat
cílovou	cílový	k2eAgFnSc4d1	cílová
čáru	čára	k1gFnSc4	čára
<g/>
,	,	kIx,	,
následován	následován	k2eAgInSc4d1	následován
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
Nicolou	Nicola	k1gMnSc7	Nicola
Larinim	Larinim	k1gInSc4	Larinim
<g/>
,	,	kIx,	,
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
místě	místo	k1gNnSc6	místo
pak	pak	k6eAd1	pak
dojel	dojet	k5eAaPmAgMnS	dojet
Fin	Fin	k1gMnSc1	Fin
Mika	Mik	k1gMnSc2	Mik
Häkkinen	Häkkinen	k2eAgInSc1d1	Häkkinen
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vyhlašování	vyhlašování	k1gNnSc6	vyhlašování
vítězná	vítězný	k2eAgFnSc1d1	vítězná
trojice	trojice	k1gFnSc1	trojice
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
netuší	tušit	k5eNaImIp3nP	tušit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
Senna	Senna	k1gFnSc1	Senna
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Ceremoniál	ceremoniál	k1gInSc1	ceremoniál
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
obešel	obejít	k5eAaPmAgMnS	obejít
bez	bez	k7c2	bez
tradičního	tradiční	k2eAgInSc2d1	tradiční
stříkaní	stříkaný	k2eAgMnPc1d1	stříkaný
šampaňským	šampaňské	k1gNnSc7	šampaňské
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
až	až	k9	až
poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
jezdce	jezdec	k1gInPc4	jezdec
odvedli	odvést	k5eAaPmAgMnP	odvést
týmoví	týmový	k2eAgMnPc1d1	týmový
manažeři	manažer	k1gMnPc1	manažer
stranou	strana	k1gFnSc7	strana
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jim	on	k3xPp3gMnPc3	on
oznámili	oznámit	k5eAaPmAgMnP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Senna	Senn	k1gInSc2	Senn
následky	následek	k1gInPc1	následek
své	svůj	k3xOyFgFnPc4	svůj
nehody	nehoda	k1gFnPc4	nehoda
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nepřežije	přežít	k5eNaPmIp3nS	přežít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
40	[number]	k4	40
doktorka	doktorka	k1gFnSc1	doktorka
Fiandri	Fiandr	k1gFnSc2	Fiandr
oznamuje	oznamovat	k5eAaImIp3nS	oznamovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ayrton	Ayrton	k1gInSc1	Ayrton
Senna	Senn	k1gInSc2	Senn
před	před	k7c7	před
pár	pár	k4xCyI	pár
okamžiky	okamžik	k1gInPc7	okamžik
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ještě	ještě	k9	ještě
v	v	k7c4	v
podvečer	podvečer	k1gInSc4	podvečer
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Sao	Sao	k1gMnSc6	Sao
Paulu	Paul	k1gMnSc6	Paul
sloužena	sloužen	k2eAgFnSc1d1	sloužena
za	za	k7c2	za
Ayrtona	Ayrton	k1gMnSc2	Ayrton
Sennu	Senn	k1gInSc2	Senn
smuteční	smuteční	k2eAgFnSc2d1	smuteční
mše	mše	k1gFnSc2	mše
pod	pod	k7c7	pod
širým	širý	k2eAgNnSc7d1	širé
nebem	nebe	k1gNnSc7	nebe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prezident	prezident	k1gMnSc1	prezident
Brazílie	Brazílie	k1gFnSc2	Brazílie
Itamar	Itamar	k1gMnSc1	Itamar
Franco	Franco	k1gMnSc1	Franco
zaslal	zaslat	k5eAaPmAgMnS	zaslat
rodině	rodina	k1gFnSc3	rodina
zesnulého	zesnulý	k1gMnSc2	zesnulý
oficiální	oficiální	k2eAgFnSc4d1	oficiální
kondolenci	kondolence	k1gFnSc4	kondolence
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
znamení	znamení	k1gNnSc6	znamení
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
úcty	úcta	k1gFnSc2	úcta
udělil	udělit	k5eAaPmAgInS	udělit
Sennovi	Sennův	k2eAgMnPc1d1	Sennův
in	in	k?	in
memoriam	memoriam	k6eAd1	memoriam
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
státní	státní	k2eAgNnSc4d1	státní
vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
Národní	národní	k2eAgInSc1d1	národní
řád	řád	k1gInSc1	řád
za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
a	a	k8xC	a
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
třídenní	třídenní	k2eAgInSc1d1	třídenní
státní	státní	k2eAgInSc1d1	státní
smutek	smutek	k1gInSc1	smutek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
pondělního	pondělní	k2eAgNnSc2d1	pondělní
rána	ráno	k1gNnSc2	ráno
byl	být	k5eAaImAgInS	být
soudním	soudní	k2eAgNnSc7d1	soudní
nařízením	nařízení	k1gNnSc7	nařízení
okruh	okruh	k1gInSc4	okruh
v	v	k7c6	v
Imole	Imol	k1gInSc6	Imol
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
nad	nad	k7c7	nad
ochranným	ochranný	k2eAgInSc7d1	ochranný
plotem	plot	k1gInSc7	plot
u	u	k7c2	u
zatáčky	zatáčka	k1gFnSc2	zatáčka
Tamburello	Tamburello	k1gNnSc4	Tamburello
nechali	nechat	k5eAaPmAgMnP	nechat
lidé	člověk	k1gMnPc1	člověk
spoustu	spousta	k1gFnSc4	spousta
květin	květina	k1gFnPc2	květina
a	a	k8xC	a
transparentů	transparent	k1gInPc2	transparent
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc4d1	další
květiny	květina	k1gFnPc4	květina
lidé	člověk	k1gMnPc1	člověk
nechávali	nechávat	k5eAaImAgMnP	nechávat
před	před	k7c7	před
institutem	institut	k1gInSc7	institut
soudního	soudní	k2eAgNnSc2d1	soudní
lékařství	lékařství	k1gNnSc2	lékařství
Di	Di	k1gFnSc2	Di
medicina	medicin	k2eAgFnSc1d1	medicina
Legace	legace	k1gFnSc1	legace
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byl	být	k5eAaImAgInS	být
Senna	Senn	k1gMnSc4	Senn
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
večer	večer	k6eAd1	večer
převezen	převézt	k5eAaPmNgMnS	převézt
k	k	k7c3	k
soudem	soud	k1gInSc7	soud
nařízené	nařízený	k2eAgFnSc3d1	nařízená
pitvě	pitva	k1gFnSc3	pitva
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pitvě	pitva	k1gFnSc6	pitva
státní	státní	k2eAgMnSc1d1	státní
zástupce	zástupce	k1gMnSc1	zástupce
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
příčinou	příčina	k1gFnSc7	příčina
Sennovy	Sennův	k2eAgFnSc2d1	Sennova
smrti	smrt	k1gFnSc2	smrt
byla	být	k5eAaImAgNnP	být
četná	četný	k2eAgNnPc1d1	četné
poranění	poranění	k1gNnPc1	poranění
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
způsobená	způsobený	k2eAgNnPc4d1	způsobené
uraženým	uražený	k2eAgInSc7d1	uražený
závěsem	závěs	k1gInSc7	závěs
pravého	pravý	k2eAgNnSc2d1	pravé
předního	přední	k2eAgNnSc2d1	přední
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
prorazil	prorazit	k5eAaPmAgInS	prorazit
hledí	hledí	k1gNnSc4	hledí
přilby	přilba	k1gFnSc2	přilba
a	a	k8xC	a
nad	nad	k7c7	nad
pravým	pravý	k2eAgNnSc7d1	pravé
obočím	obočí	k1gNnSc7	obočí
vnikl	vniknout	k5eAaPmAgMnS	vniknout
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
úmrtím	úmrtí	k1gNnSc7	úmrtí
Senny	Senny	k?	Senny
bylo	být	k5eAaImAgNnS	být
zahájeno	zahájen	k2eAgNnSc1d1	zahájeno
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
17	[number]	k4	17
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
všech	všecek	k3xTgFnPc2	všecek
nezbytných	nezbytný	k2eAgFnPc2d1	nezbytná
lékařských	lékařský	k2eAgFnPc2d1	lékařská
procedur	procedura	k1gFnPc2	procedura
byla	být	k5eAaImAgFnS	být
rakev	rakev	k1gFnSc1	rakev
se	s	k7c7	s
Sennovým	Sennův	k2eAgNnSc7d1	Sennův
tělem	tělo	k1gNnSc7	tělo
<g/>
,	,	kIx,	,
zahalená	zahalený	k2eAgFnSc1d1	zahalená
do	do	k7c2	do
brazilské	brazilský	k2eAgFnSc2d1	brazilská
národní	národní	k2eAgFnSc2d1	národní
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
převezena	převézt	k5eAaPmNgFnS	převézt
na	na	k7c4	na
boloňské	boloňský	k2eAgNnSc4d1	boloňské
letiště	letiště	k1gNnSc4	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
letecky	letecky	k6eAd1	letecky
transportována	transportován	k2eAgFnSc1d1	transportována
do	do	k7c2	do
brazilského	brazilský	k2eAgMnSc2d1	brazilský
Sao	Sao	k1gMnSc2	Sao
Paula	Paul	k1gMnSc2	Paul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
letadlo	letadlo	k1gNnSc1	letadlo
přistálo	přistát	k5eAaPmAgNnS	přistát
v	v	k7c6	v
Sao	Sao	k1gFnSc6	Sao
Paulu	Paula	k1gFnSc4	Paula
<g/>
,	,	kIx,	,
cestu	cesta	k1gFnSc4	cesta
z	z	k7c2	z
letiště	letiště	k1gNnSc2	letiště
do	do	k7c2	do
Monumental	Monumental	k1gMnSc1	Monumental
Hall	Hall	k1gInSc1	Hall
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byla	být	k5eAaImAgFnS	být
rakev	rakev	k1gFnSc1	rakev
převážena	převážet	k5eAaImNgFnS	převážet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
bylo	být	k5eAaImAgNnS	být
lidem	člověk	k1gMnPc3	člověk
umožněno	umožnit	k5eAaPmNgNnS	umožnit
vzdát	vzdát	k5eAaPmF	vzdát
zesnulému	zesnulý	k1gMnSc3	zesnulý
poslední	poslední	k2eAgFnSc4d1	poslední
úctu	úcta	k1gFnSc4	úcta
<g/>
,	,	kIx,	,
lemovaly	lemovat	k5eAaImAgFnP	lemovat
desítky	desítka	k1gFnPc1	desítka
tisíc	tisíc	k4xCgInSc1	tisíc
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
vlajkami	vlajka	k1gFnPc7	vlajka
a	a	k8xC	a
transparenty	transparent	k1gInPc7	transparent
vyjadřujícími	vyjadřující	k2eAgInPc7d1	vyjadřující
poslední	poslední	k2eAgFnSc4d1	poslední
úctu	úcta	k1gFnSc4	úcta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
rakev	rakev	k1gFnSc1	rakev
dorazila	dorazit	k5eAaPmAgFnS	dorazit
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zde	zde	k6eAd1	zde
nejdříve	dříve	k6eAd3	dříve
sloužena	sloužen	k2eAgFnSc1d1	sloužena
soukromá	soukromý	k2eAgFnSc1d1	soukromá
mše	mše	k1gFnSc1	mše
v	v	k7c6	v
kruhu	kruh	k1gInSc6	kruh
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
otevření	otevření	k1gNnSc6	otevření
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
fronta	fronta	k1gFnSc1	fronta
čekajících	čekající	k2eAgMnPc2d1	čekající
lidí	člověk	k1gMnPc2	člověk
několika	několik	k4yIc2	několik
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgInSc4d1	další
den	den	k1gInSc4	den
bylo	být	k5eAaImAgNnS	být
Sennovo	Sennův	k2eAgNnSc1d1	Sennův
tělo	tělo	k1gNnSc1	tělo
převezeno	převézt	k5eAaPmNgNnS	převézt
na	na	k7c4	na
hřbitov	hřbitov	k1gInSc4	hřbitov
Morumbi	Morumb	k1gFnSc2	Morumb
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
poslední	poslední	k2eAgFnSc4d1	poslední
cestu	cesta	k1gFnSc4	cesta
doprovázelo	doprovázet	k5eAaImAgNnS	doprovázet
něco	něco	k6eAd1	něco
přes	přes	k7c4	přes
jeden	jeden	k4xCgInSc4	jeden
a	a	k8xC	a
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ceremoniálu	ceremoniál	k1gInSc3	ceremoniál
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
něco	něco	k6eAd1	něco
přes	přes	k7c4	přes
500	[number]	k4	500
lidí	člověk	k1gMnPc2	člověk
včetně	včetně	k7c2	včetně
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
známých	známý	k1gMnPc2	známý
a	a	k8xC	a
spolujezdců	spolujezdec	k1gMnPc2	spolujezdec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obřad	obřad	k1gInSc1	obřad
konaný	konaný	k2eAgInSc1d1	konaný
pod	pod	k7c7	pod
otevřeným	otevřený	k2eAgNnSc7d1	otevřené
nebem	nebe	k1gNnSc7	nebe
trval	trvat	k5eAaImAgInS	trvat
půl	půl	k6eAd1	půl
hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
hrobě	hrob	k1gInSc6	hrob
Ayrtona	Ayrtona	k1gFnSc1	Ayrtona
Senny	Senny	k?	Senny
je	být	k5eAaImIp3nS	být
vsazená	vsazený	k2eAgFnSc1d1	vsazená
mosazná	mosazný	k2eAgFnSc1d1	mosazná
deska	deska	k1gFnSc1	deska
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
Ayrton	Ayrton	k1gInSc1	Ayrton
Senna	Senn	k1gInSc2	Senn
de	de	k?	de
Silva	Silva	k1gFnSc1	Silva
21.3	[number]	k4	21.3
<g/>
.1960	.1960	k4	.1960
-	-	kIx~	-
1.5	[number]	k4	1.5
<g/>
.1994	.1994	k4	.1994
<g/>
.	.	kIx.	.
</s>
<s>
Nada	Nada	k1gFnSc1	Nada
pode	pod	k7c7	pod
me	me	k?	me
separar	separar	k1gMnSc1	separar
do	do	k7c2	do
Amor	Amor	k1gMnSc1	Amor
de	de	k?	de
Deus	Deus	k1gInSc1	Deus
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Nic	nic	k3yNnSc1	nic
mě	já	k3xPp1nSc4	já
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
oddělit	oddělit	k5eAaPmF	oddělit
od	od	k7c2	od
lásky	láska	k1gFnSc2	láska
Boží	boží	k2eAgFnSc2d1	boží
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Kompletní	kompletní	k2eAgInPc4d1	kompletní
výsledky	výsledek	k1gInPc4	výsledek
ve	v	k7c6	v
Formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
==	==	k?	==
</s>
</p>
<p>
<s>
tučně	tučně	k6eAd1	tučně
vyznačené	vyznačený	k2eAgInPc1d1	vyznačený
závody	závod	k1gInPc1	závod
znamenají	znamenat	k5eAaImIp3nP	znamenat
zisk	zisk	k1gInSc4	zisk
pole	pole	k1gNnSc2	pole
position	position	k1gInSc1	position
<g/>
,	,	kIx,	,
závody	závod	k1gInPc1	závod
vyznačené	vyznačený	k2eAgInPc1d1	vyznačený
kurzívou	kurzíva	k1gFnSc7	kurzíva
označují	označovat	k5eAaImIp3nP	označovat
zajetí	zajetí	k1gNnPc1	zajetí
nejrychlejšího	rychlý	k2eAgNnSc2d3	nejrychlejší
kola	kolo	k1gNnSc2	kolo
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
</s>
</p>
<p>
<s>
==	==	k?	==
Památka	památka	k1gFnSc1	památka
==	==	k?	==
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
:	:	kIx,	:
Ayrton	Ayrton	k1gInSc1	Ayrton
Senna	Senn	k1gInSc2	Senn
</s>
</p>
<p>
<s>
Souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
<g/>
:	:	kIx,	:
Auriga	Auriga	k1gFnSc1	Auriga
(	(	kIx(	(
<g/>
Vozka	vozka	k1gMnSc1	vozka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hvězdné	hvězdný	k2eAgFnPc4d1	hvězdná
souřadnice	souřadnice	k1gFnPc4	souřadnice
<g/>
:	:	kIx,	:
RA	ra	k0	ra
6	[number]	k4	6
<g/>
h	h	k?	h
53	[number]	k4	53
<g/>
'	'	kIx"	'
55	[number]	k4	55
<g/>
,	,	kIx,	,
43	[number]	k4	43
<g/>
'	'	kIx"	'
SD	SD	kA	SD
+37	+37	k4	+37
<g/>
'	'	kIx"	'
56	[number]	k4	56
<g/>
'	'	kIx"	'
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
astronomické	astronomický	k2eAgFnSc2d1	astronomická
unie	unie	k1gFnSc2	unie
IAU	IAU	kA	IAU
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ayrton	Ayrton	k1gInSc1	Ayrton
Senna	Senno	k1gNnSc2	Senno
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Ayrton	Ayrton	k1gInSc1	Ayrton
Senna	Senn	k1gInSc2	Senn
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Ayrton	Ayrton	k1gInSc1	Ayrton
Senna	Senn	k1gInSc2	Senn
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Ayrton	Ayrton	k1gInSc1	Ayrton
Senna	Senn	k1gInSc2	Senn
<g/>
:	:	kIx,	:
Smrť	smrtit	k5eAaImRp2nS	smrtit
v	v	k7c6	v
priamom	priamom	k1gInSc1	priamom
prenose	prenosa	k1gFnSc3	prenosa
</s>
</p>
<p>
<s>
Mohol	Mohol	k1gInSc1	Mohol
žiť	žiť	k?	žiť
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
(	(	kIx(	(
<g/>
portugalsky	portugalsky	k6eAd1	portugalsky
<g/>
)	)	kIx)	)
</s>
</p>
