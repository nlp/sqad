<p>
<s>
Obdélník	obdélník	k1gInSc1	obdélník
je	být	k5eAaImIp3nS	být
rovnoběžník	rovnoběžník	k1gInSc4	rovnoběžník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
všechny	všechen	k3xTgInPc1	všechen
úhly	úhel	k1gInPc1	úhel
pravé	pravý	k2eAgInPc1d1	pravý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Vzájemně	vzájemně	k6eAd1	vzájemně
protilehlé	protilehlý	k2eAgFnPc1d1	protilehlá
strany	strana	k1gFnPc1	strana
jsou	být	k5eAaImIp3nP	být
rovnoběžné	rovnoběžný	k2eAgFnPc1d1	rovnoběžná
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
shodnou	shodný	k2eAgFnSc4d1	shodná
délku	délka	k1gFnSc4	délka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úhlopříčky	úhlopříčka	k1gFnPc1	úhlopříčka
obdélníka	obdélník	k1gInSc2	obdélník
se	se	k3xPyFc4	se
půlí	půlit	k5eAaImIp3nP	půlit
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
stejně	stejně	k6eAd1	stejně
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obdélník	obdélník	k1gInSc1	obdélník
má	mít	k5eAaImIp3nS	mít
kružnici	kružnice	k1gFnSc4	kružnice
opsanou	opsaný	k2eAgFnSc4d1	opsaná
se	s	k7c7	s
středem	střed	k1gInSc7	střed
v	v	k7c6	v
průsečíku	průsečík	k1gInSc6	průsečík
úhlopříček	úhlopříčka	k1gFnPc2	úhlopříčka
a	a	k8xC	a
poloměrem	poloměr	k1gInSc7	poloměr
rovným	rovný	k2eAgInSc7d1	rovný
polovině	polovina	k1gFnSc6	polovina
délky	délka	k1gFnSc2	délka
úhlopříčky	úhlopříčka	k1gFnSc2	úhlopříčka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obdélník	obdélník	k1gInSc1	obdélník
obecně	obecně	k6eAd1	obecně
nemá	mít	k5eNaImIp3nS	mít
kružnici	kružnice	k1gFnSc4	kružnice
vepsanou	vepsaný	k2eAgFnSc4d1	vepsaná
–	–	k?	–
výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
speciální	speciální	k2eAgInSc1d1	speciální
případ	případ	k1gInSc1	případ
obdélníka	obdélník	k1gInSc2	obdélník
–	–	k?	–
čtverec	čtverec	k1gInSc1	čtverec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obdélník	obdélník	k1gInSc1	obdélník
je	být	k5eAaImIp3nS	být
středově	středově	k6eAd1	středově
souměrný	souměrný	k2eAgInSc1d1	souměrný
podle	podle	k7c2	podle
průsečíku	průsečík	k1gInSc2	průsečík
úhlopříček	úhlopříčka	k1gFnPc2	úhlopříčka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obdélník	obdélník	k1gInSc1	obdélník
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
obecném	obecný	k2eAgInSc6d1	obecný
případě	případ	k1gInSc6	případ
osově	osově	k6eAd1	osově
souměrný	souměrný	k2eAgMnSc1d1	souměrný
podle	podle	k7c2	podle
dvou	dva	k4xCgFnPc2	dva
os	osa	k1gFnPc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Osami	osa	k1gFnPc7	osa
souměrnosti	souměrnost	k1gFnPc4	souměrnost
jsou	být	k5eAaImIp3nP	být
rovnoběžky	rovnoběžka	k1gFnPc1	rovnoběžka
se	s	k7c7	s
stranami	strana	k1gFnPc7	strana
procházející	procházející	k2eAgMnPc1d1	procházející
průsečíkem	průsečík	k1gInSc7	průsečík
úhlopříček	úhlopříčka	k1gFnPc2	úhlopříčka
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgInSc1d1	speciální
případ	případ	k1gInSc1	případ
obdélníka	obdélník	k1gInSc2	obdélník
-	-	kIx~	-
čtverec	čtverec	k1gInSc1	čtverec
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
osově	osově	k6eAd1	osově
souměrný	souměrný	k2eAgMnSc1d1	souměrný
podle	podle	k7c2	podle
čtyř	čtyři	k4xCgFnPc2	čtyři
os	osa	k1gFnPc2	osa
<g/>
,	,	kIx,	,
dalšími	další	k2eAgFnPc7d1	další
dvěma	dva	k4xCgFnPc7	dva
osami	osa	k1gFnPc7	osa
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gFnPc1	jeho
úhlopříčky	úhlopříčka	k1gFnPc1	úhlopříčka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzorce	vzorec	k1gInPc1	vzorec
==	==	k?	==
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
označíme	označit	k5eAaPmIp1nP	označit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
délky	délka	k1gFnSc2	délka
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
u	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
u	u	k7c2	u
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
délku	délka	k1gFnSc4	délka
úhlopříčky	úhlopříčka	k1gFnSc2	úhlopříčka
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r	r	kA	r
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
poloměr	poloměr	k1gInSc1	poloměr
kružnice	kružnice	k1gFnSc2	kružnice
opsané	opsaný	k2eAgFnSc2d1	opsaná
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
obsah	obsah	k1gInSc1	obsah
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
o	o	k7c4	o
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
obvod	obvod	k1gInSc1	obvod
obdélníka	obdélník	k1gInSc2	obdélník
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
platí	platit	k5eAaImIp3nS	platit
následující	následující	k2eAgInPc4d1	následující
vztahy	vztah	k1gInPc4	vztah
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
=	=	kIx~	=
<g/>
ab	ab	k?	ab
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
u	u	k7c2	u
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
u	u	k7c2	u
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
b	b	k?	b
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Pythagorova	Pythagorův	k2eAgFnSc1d1	Pythagorova
věta	věta	k1gFnSc1	věta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
u	u	k7c2	u
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r	r	kA	r
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
u	u	k7c2	u
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
o	o	k0	o
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
a	a	k8xC	a
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Geometrický	geometrický	k2eAgInSc1d1	geometrický
útvar	útvar	k1gInSc1	útvar
</s>
</p>
<p>
<s>
Čtverec	čtverec	k1gInSc1	čtverec
</s>
</p>
<p>
<s>
Rovnoběžník	rovnoběžník	k1gInSc1	rovnoběžník
</s>
</p>
<p>
<s>
Deltoid	deltoid	k1gInSc1	deltoid
</s>
</p>
<p>
<s>
Kvádr	kvádr	k1gInSc1	kvádr
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
obdélník	obdélník	k1gInSc1	obdélník
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
obdélník	obdélník	k1gInSc1	obdélník
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Kalkulátor	kalkulátor	k1gInSc1	kalkulátor
pro	pro	k7c4	pro
řešení	řešení	k1gNnSc4	řešení
výpočtů	výpočet	k1gInPc2	výpočet
týkajících	týkající	k2eAgInPc2d1	týkající
se	se	k3xPyFc4	se
obdélníku	obdélník	k1gInSc2	obdélník
</s>
</p>
