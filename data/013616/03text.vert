<s>
Tyterský	Tyterský	k2eAgInSc1d1
potok	potok	k1gInSc1
</s>
<s>
Tyterský	Tyterský	k2eAgInSc1d1
potok	potok	k1gInSc1
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Délka	délka	k1gFnSc1
toku	tok	k1gInSc2
</s>
<s>
13,8	13,8	k4
km	km	kA
Plocha	plocha	k1gFnSc1
povodí	povodit	k5eAaPmIp3nS
</s>
<s>
42,9	42,9	k4
km²	km²	k?
Průměrný	průměrný	k2eAgInSc1d1
průtok	průtok	k1gInSc1
</s>
<s>
0,009	0,009	k4
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s	s	k7c7
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Hydrologické	hydrologický	k2eAgNnSc1d1
pořadí	pořadí	k1gNnSc4
</s>
<s>
1-11-02-149	1-11-02-149	k4
Pramen	pramen	k1gInSc1
</s>
<s>
západně	západně	k6eAd1
od	od	k7c2
Hvozdu	hvozd	k1gInSc2
50	#num#	k4
<g/>
°	°	k?
<g/>
3	#num#	k4
<g/>
′	′	k?
<g/>
1,68	1,68	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
13	#num#	k4
<g/>
°	°	k?
<g/>
41	#num#	k4
<g/>
′	′	k?
<g/>
32,63	32,63	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
495	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Ústí	ústí	k1gNnSc1
</s>
<s>
do	do	k7c2
Berounky	Berounka	k1gFnSc2
u	u	k7c2
Nezabudic	Nezabudice	k1gInPc2
50	#num#	k4
<g/>
°	°	k?
<g/>
0	#num#	k4
<g/>
′	′	k?
<g/>
14,85	14,85	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
13	#num#	k4
<g/>
°	°	k?
<g/>
49	#num#	k4
<g/>
′	′	k?
<g/>
21,06	21,06	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
241	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Protéká	protékat	k5eAaImIp3nS
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
(	(	kIx(
<g/>
Středočeský	středočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
–	–	k?
okres	okres	k1gInSc1
Rakovník	Rakovník	k1gInSc1
<g/>
)	)	kIx)
Úmoří	úmoří	k1gNnSc1
<g/>
,	,	kIx,
povodí	povodí	k1gNnSc1
</s>
<s>
Atlantský	atlantský	k2eAgInSc1d1
oceán	oceán	k1gInSc1
<g/>
,	,	kIx,
Severní	severní	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
,	,	kIx,
Labe	Labe	k1gNnSc1
<g/>
,	,	kIx,
Vltava	Vltava	k1gFnSc1
<g/>
,	,	kIx,
Berounka	Berounka	k1gFnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tomuto	tento	k3xDgInSc3
článku	článek	k1gInSc3
chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
obrázky	obrázek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Víte	vědět	k5eAaImIp2nP
<g/>
-li	-li	k?
o	o	k7c6
nějakých	nějaký	k3yIgFnPc6
svobodně	svobodně	k6eAd1
šiřitelných	šiřitelný	k2eAgFnPc6d1
<g/>
,	,	kIx,
neváhejte	váhat	k5eNaImRp2nP
je	on	k3xPp3gMnPc4
načíst	načíst	k5eAaBmF,k5eAaPmF
a	a	k8xC
přidat	přidat	k5eAaPmF
do	do	k7c2
článku	článek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
rychlejší	rychlý	k2eAgNnSc4d2
přidání	přidání	k1gNnSc4
obrázku	obrázek	k1gInSc2
můžete	moct	k5eAaImIp2nP
přidat	přidat	k5eAaPmF
žádost	žádost	k1gFnSc4
i	i	k9
sem	sem	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
WikiProjekt	WikiProjekt	k1gInSc1
Fotografování	fotografování	k1gNnSc2
</s>
<s>
Tyterský	Tyterský	k2eAgInSc1d1
potok	potok	k1gInSc1
je	být	k5eAaImIp3nS
drobný	drobný	k2eAgInSc1d1
vodní	vodní	k2eAgInSc1d1
tok	tok	k1gInSc1
v	v	k7c6
Plaské	Plaský	k2eAgFnSc6d1
pahorkatině	pahorkatina	k1gFnSc6
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
okrese	okres	k1gInSc6
Rakovník	Rakovník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
dlouhý	dlouhý	k2eAgInSc1d1
13,8	13,8	k4
km	km	kA
<g/>
,	,	kIx,
plocha	plocha	k1gFnSc1
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
povodí	povodí	k1gNnSc2
měří	měřit	k5eAaImIp3nS
42,9	42,9	k4
km²	km²	kA
a	a	k8xC
průměrný	průměrný	k2eAgInSc1d1
průtok	průtok	k1gInSc1
v	v	k7c6
ústí	ústí	k1gNnSc6
je	být	k5eAaImIp3nS
0,009	0,009	k4
m³	m³	kA
<g/>
/	/	kIx~
<g/>
s.	s.	kA
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Potok	potok	k1gInSc1
pramení	pramenit	k5eAaImIp3nS
asi	asi	k9
500	#num#	k4
m	m	kA
západně	západně	k6eAd1
od	od	k7c2
obce	obec	k1gFnSc2
Hvozd	hvozd	k1gInSc1
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
495	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Protéká	protékat	k5eAaImIp3nS
vesnicí	vesnice	k1gFnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
napájí	napájet	k5eAaImIp3nS
malý	malý	k2eAgInSc4d1
rybník	rybník	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
stáčí	stáčet	k5eAaImIp3nS
se	se	k3xPyFc4
k	k	k7c3
Panošímu	panoší	k2eAgInSc3d1
Újezdu	Újezd	k1gInSc3
<g/>
,	,	kIx,
před	před	k7c7
kterým	který	k3yIgNnSc7,k3yQgNnSc7,k3yRgNnSc7
protéká	protékat	k5eAaImIp3nS
dalším	další	k2eAgInSc7d1
rybníkem	rybník	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potom	potom	k6eAd1
pokračuje	pokračovat	k5eAaImIp3nS
dále	daleko	k6eAd2
k	k	k7c3
jihovýchodu	jihovýchod	k1gInSc2
a	a	k8xC
pod	pod	k7c7
vesnicí	vesnice	k1gFnSc7
Tytry	Tytra	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
vtéká	vtékat	k5eAaImIp3nS
do	do	k7c2
zalesněného	zalesněný	k2eAgNnSc2d1
údolí	údolí	k1gNnSc2
<g/>
,	,	kIx,
přijímá	přijímat	k5eAaImIp3nS
menší	malý	k2eAgInPc4d2
bezejmenné	bezejmenný	k2eAgInPc4d1
přítoky	přítok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihovýchodně	jihovýchodně	k6eAd1
od	od	k7c2
vesnice	vesnice	k1gFnSc2
Skřivaň	Skřivaň	k1gFnSc2
zleva	zleva	k6eAd1
obtéká	obtékat	k5eAaImIp3nS
přírodní	přírodní	k2eAgFnSc4d1
památku	památka	k1gFnSc4
Valachov	Valachov	k1gInSc4
na	na	k7c6
západním	západní	k2eAgNnSc6d1
úbočí	úbočí	k1gNnSc6
stejnojmenného	stejnojmenný	k2eAgInSc2d1
vrchu	vrch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východně	východně	k6eAd1
od	od	k7c2
něj	on	k3xPp3gMnSc2
zleva	zleva	k6eAd1
přijímá	přijímat	k5eAaImIp3nS
Všetatský	Všetatský	k2eAgInSc4d1
potok	potok	k1gInSc4
a	a	k8xC
po	po	k7c6
dalších	další	k2eAgInPc6d1
dvou	dva	k4xCgInPc6
kilometrech	kilometr	k1gInPc6
menší	malý	k2eAgInSc1d2
potok	potok	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
přitéká	přitékat	k5eAaImIp3nS
bočním	boční	k2eAgNnSc7d1
údolím	údolí	k1gNnSc7
Sibův	Sibův	k2eAgInSc4d1
luh	luh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jižně	jižně	k6eAd1
od	od	k7c2
Nezabudic	Nezabudice	k1gFnPc2
<g/>
,	,	kIx,
nedaleko	nedaleko	k7c2
restaurace	restaurace	k1gFnSc2
U	u	k7c2
Rozvědčíka	rozvědčík	k1gMnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
241	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
vlévá	vlévat	k5eAaImIp3nS
zleva	zleva	k6eAd1
do	do	k7c2
Berounky	Berounka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
Novosedly	Novosedlo	k1gNnPc7
a	a	k8xC
Nezabudicemi	Nezabudice	k1gFnPc7
vede	vést	k5eAaImIp3nS
údolím	údolí	k1gNnSc7
potoka	potok	k1gInSc2
žlutě	žlutě	k6eAd1
značená	značený	k2eAgFnSc1d1
turistická	turistický	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
CENIA	CENIA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geomorfologická	geomorfologický	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Národní	národní	k2eAgInSc1d1
geoportál	geoportál	k1gInSc1
INSPIRE	INSPIRE	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
VLČEK	Vlček	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeměpisný	zeměpisný	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
ČSR	ČSR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vodní	vodní	k2eAgInPc1d1
toky	tok	k1gInPc1
a	a	k8xC
nádrže	nádrž	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
.	.	kIx.
316	#num#	k4
s.	s.	k?
Heslo	heslo	k1gNnSc4
Tyterský	Tyterský	k2eAgInSc1d1
potok	potok	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
279	#num#	k4
<g/>
.	.	kIx.
</s>
