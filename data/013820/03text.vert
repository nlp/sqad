<s>
František	František	k1gMnSc1
Halas	Halas	k1gMnSc1*xF
</s>
<s>
František	František	k1gMnSc1
Halas	Halas	k1gMnSc1
František	František	k1gMnSc1
Halas	Halas	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1901	#num#	k4
Brno	Brno	k1gNnSc4
<g/>
,	,	kIx,
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1949	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
48	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Československo	Československo	k1gNnSc1
Československo	Československo	k1gNnSc4
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnSc2
</s>
<s>
nemoc	nemoc	k1gFnSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Kunštát	Kunštát	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
jazykovědec	jazykovědec	k1gMnSc1
<g/>
,	,	kIx,
básník	básník	k1gMnSc1
<g/>
,	,	kIx,
překladatel	překladatel	k1gMnSc1
a	a	k8xC
redaktor	redaktor	k1gMnSc1
Národnost	národnost	k1gFnSc4
</s>
<s>
Češi	Čech	k1gMnPc1
Ocenění	ocenění	k1gNnSc2
</s>
<s>
Řád	řád	k1gInSc1
Tomáše	Tomáš	k1gMnSc2
Garrigua	Garriguus	k1gMnSc2
Masaryka	Masaryk	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
<g/>
,	,	kIx,
in	in	k?
memoriam	memoriam	k1gInSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
Politická	politický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
</s>
<s>
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Československa	Československo	k1gNnSc2
Děti	dítě	k1gFnPc1
</s>
<s>
Jan	Jan	k1gMnSc1
HalasFrantišek	HalasFrantišek	k1gMnSc1
X.	X.	kA
Halas	Halas	k1gMnSc1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Dagmar	Dagmar	k1gFnSc1
Halasová	Halasová	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
původní	původní	k2eAgInPc4d1
texty	text	k1gInPc4
na	na	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
citáty	citát	k1gInPc1
na	na	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Halas	halas	k1gInSc1
roku	rok	k1gInSc2
1923	#num#	k4
</s>
<s>
František	František	k1gMnSc1
Halas	Halas	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1901	#num#	k4
Brno	Brno	k1gNnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
27	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1949	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
básník	básník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
dětství	dětství	k1gNnSc6
prožil	prožít	k5eAaPmAgMnS
na	na	k7c6
Českomoravské	českomoravský	k2eAgFnSc6d1
vrchovině	vrchovina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
rodiče	rodič	k1gMnPc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Halas	Halas	k1gMnSc1
a	a	k8xC
Leopoldina	Leopoldin	k2eAgFnSc1d1
Pelikánová	Pelikánová	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
byli	být	k5eAaImAgMnP
textilní	textilní	k2eAgMnPc1d1
dělníci	dělník	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
ukončení	ukončení	k1gNnSc6
základní	základní	k2eAgFnSc2d1
školy	škola	k1gFnSc2
se	se	k3xPyFc4
vyučil	vyučit	k5eAaPmAgMnS
knihkupcem	knihkupec	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otec	otec	k1gMnSc1
ho	on	k3xPp3gMnSc4
přivedl	přivést	k5eAaPmAgMnS
mezi	mezi	k7c4
komunistickou	komunistický	k2eAgFnSc4d1
mládež	mládež	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
ho	on	k3xPp3gMnSc4
velmi	velmi	k6eAd1
ovlivnila	ovlivnit	k5eAaPmAgFnS
<g/>
,	,	kIx,
později	pozdě	k6eAd2
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1921	#num#	k4
<g/>
)	)	kIx)
pracoval	pracovat	k5eAaImAgInS
pro	pro	k7c4
komunistický	komunistický	k2eAgInSc4d1
tisk	tisk	k1gInSc4
(	(	kIx(
<g/>
Rovnost	rovnost	k1gFnSc1
a	a	k8xC
Sršatec	sršatec	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
vydával	vydávat	k5eAaPmAgInS,k5eAaImAgInS
avantgardní	avantgardní	k2eAgInPc4d1
časopisy	časopis	k1gInPc4
<g/>
,	,	kIx,
na	na	k7c6
kterých	který	k3yRgFnPc6,k3yIgFnPc6,k3yQgFnPc6
spolupracoval	spolupracovat	k5eAaImAgMnS
s	s	k7c7
Bedřichem	Bedřich	k1gMnSc7
Václavkem	Václavek	k1gMnSc7
(	(	kIx(
<g/>
Pásmo	pásmo	k1gNnSc1
a	a	k8xC
Fronta	fronta	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
tvorbu	tvorba	k1gFnSc4
ovlivnil	ovlivnit	k5eAaPmAgInS
Jiří	Jiří	k1gMnSc1
Mahen	Mahna	k1gFnPc2
a	a	k8xC
pobyt	pobyt	k1gInSc1
ve	v	k7c6
Španělsku	Španělsko	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
v	v	k7c6
průběhu	průběh	k1gInSc6
španělské	španělský	k2eAgFnSc2d1
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
přispíval	přispívat	k5eAaImAgInS
do	do	k7c2
ilegálně	ilegálně	k6eAd1
vydávaného	vydávaný	k2eAgNnSc2d1
Rudého	rudý	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
revolučního	revoluční	k2eAgInSc2d1
výboru	výbor	k1gInSc2
spisovatelů	spisovatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
různost	různost	k1gFnSc4
názorů	názor	k1gInPc2
byl	být	k5eAaImAgInS
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
blízkým	blízký	k2eAgFnPc3d1
přítelem	přítel	k1gMnSc7
katolický	katolický	k2eAgMnSc1d1
básník	básník	k1gMnSc1
Jan	Jan	k1gMnSc1
Zahradníček	Zahradníček	k1gMnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
předsedou	předseda	k1gMnSc7
Syndikátu	syndikát	k1gInSc2
českých	český	k2eAgMnPc2d1
spisovatelů	spisovatel	k1gMnPc2
<g/>
,	,	kIx,
pracoval	pracovat	k5eAaImAgMnS
na	na	k7c6
ministerstvu	ministerstvo	k1gNnSc6
informací	informace	k1gFnPc2
a	a	k8xC
choval	chovat	k5eAaImAgMnS
se	se	k3xPyFc4
jako	jako	k8xC,k8xS
aktivní	aktivní	k2eAgMnSc1d1
komunista	komunista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1945	#num#	k4
<g/>
–	–	k?
<g/>
1946	#num#	k4
byl	být	k5eAaImAgInS
poslancem	poslanec	k1gMnSc7
Prozatímního	prozatímní	k2eAgNnSc2d1
Národního	národní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
za	za	k7c4
KSČ	KSČ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
parlamentu	parlament	k1gInSc6
zasedal	zasedat	k5eAaImAgInS
do	do	k7c2
parlamentních	parlamentní	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1946	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
–	–	k?
především	především	k6eAd1
po	po	k7c6
svém	svůj	k3xOyFgInSc6
návratu	návrat	k1gInSc6
z	z	k7c2
návštěvy	návštěva	k1gFnSc2
v	v	k7c6
Sovětském	sovětský	k2eAgInSc6d1
svazu	svaz	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
jím	jíst	k5eAaImIp1nS
otřásla	otřást	k5eAaPmAgFnS
–	–	k?
byl	být	k5eAaImAgInS
však	však	k9
i	i	k9
novým	nový	k2eAgInSc7d1
režimem	režim	k1gInSc7
zklamán	zklamán	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
Praze	Praha	k1gFnSc6
dne	den	k1gInSc2
27	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1949	#num#	k4
na	na	k7c4
selhání	selhání	k1gNnSc4
srdce	srdce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohřben	pohřbít	k5eAaPmNgInS
je	být	k5eAaImIp3nS
v	v	k7c6
Kunštátě	Kunštáta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnPc7
syny	syn	k1gMnPc7
jsou	být	k5eAaImIp3nP
diplomat	diplomat	k1gMnSc1
<g/>
,	,	kIx,
církevní	církevní	k2eAgMnSc1d1
historik	historik	k1gMnSc1
a	a	k8xC
překladatel	překladatel	k1gMnSc1
František	František	k1gMnSc1
X.	X.	kA
Halas	Halas	k1gMnSc1
a	a	k8xC
rozhlasový	rozhlasový	k2eAgMnSc1d1
redaktor	redaktor	k1gMnSc1
a	a	k8xC
publicista	publicista	k1gMnSc1
Jan	Jan	k1gMnSc1
Halas	Halas	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1997	#num#	k4
mu	on	k3xPp3gMnSc3
byl	být	k5eAaImAgInS
in	in	k?
memoriam	memoriam	k6eAd1
propůjčen	propůjčen	k2eAgInSc1d1
Řád	řád	k1gInSc1
T.	T.	kA
G.	G.	kA
Masaryka	Masaryk	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Halasova	Halasův	k2eAgFnSc1d1
poetika	poetika	k1gFnSc1
bývá	bývat	k5eAaImIp3nS
popisována	popisován	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
nelíbivá	líbivý	k2eNgFnSc1d1
<g/>
,	,	kIx,
nemelodická	melodický	k2eNgFnSc1d1
<g/>
,	,	kIx,
Vítězslav	Vítězslav	k1gMnSc1
Nezval	Nezval	k1gMnSc1
mu	on	k3xPp3gMnSc3
vyčítal	vyčítat	k5eAaImAgMnS
opomíjení	opomíjení	k1gNnSc4
básnického	básnický	k2eAgNnSc2d1
řemesla	řemeslo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyznačuje	vyznačovat	k5eAaImIp3nS
se	s	k7c7
však	však	k9
jazykovým	jazykový	k2eAgNnSc7d1
experimentováním	experimentování	k1gNnSc7
a	a	k8xC
výraznou	výrazný	k2eAgFnSc7d1
metaforikou	metaforika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
pólem	pólo	k1gNnSc7
jeho	jeho	k3xOp3gFnSc2
poezie	poezie	k1gFnSc2
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
zapojit	zapojit	k5eAaPmF
své	svůj	k3xOyFgNnSc4
umění	umění	k1gNnSc4
do	do	k7c2
služby	služba	k1gFnSc2
celku	celek	k1gInSc2
<g/>
,	,	kIx,
odtud	odtud	k6eAd1
snaha	snaha	k1gFnSc1
o	o	k7c4
prosté	prostý	k2eAgNnSc4d1
psaní	psaní	k1gNnSc4
a	a	k8xC
akcentování	akcentování	k1gNnSc4
sociální	sociální	k2eAgFnSc2d1
problematiky	problematika	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
druhým	druhý	k4xOgNnSc7
pólem	pólo	k1gNnSc7
je	být	k5eAaImIp3nS
potřeba	potřeba	k1gFnSc1
vyjádřit	vyjádřit	k5eAaPmF
své	svůj	k3xOyFgNnSc4
intimní	intimní	k2eAgNnSc4d1
<g/>
,	,	kIx,
niterné	niterný	k2eAgNnSc4d1
prožívání	prožívání	k1gNnSc4
světa	svět	k1gInSc2
<g/>
,	,	kIx,
vědomí	vědomí	k1gNnSc2
nicotnosti	nicotnost	k1gFnSc2
člověka	člověk	k1gMnSc2
a	a	k8xC
tušení	tušení	k1gNnSc1
blízkosti	blízkost	k1gFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
a	a	k8xC
v	v	k7c6
odvážných	odvážný	k2eAgFnPc6d1
polohách	poloha	k1gFnPc6
se	se	k3xPyFc4
v	v	k7c6
jeho	jeho	k3xOp3gFnPc6
básních	báseň	k1gFnPc6
objevují	objevovat	k5eAaImIp3nP
erotické	erotický	k2eAgInPc1d1
motivy	motiv	k1gInPc1
<g/>
,	,	kIx,
opakovaně	opakovaně	k6eAd1
se	se	k3xPyFc4
také	také	k9
vrací	vracet	k5eAaImIp3nS
motivy	motiv	k1gInPc4
slova	slovo	k1gNnSc2
<g/>
,	,	kIx,
řeči	řeč	k1gFnSc2
<g/>
,	,	kIx,
poezie	poezie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Sepie	Sepie	k1gFnSc1
–	–	k?
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
poetistická	poetistický	k2eAgFnSc1d1
sbírka	sbírka	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
s	s	k7c7
hravostí	hravost	k1gFnSc7
poetismu	poetismus	k1gInSc2
kontrastuje	kontrastovat	k5eAaImIp3nS
melancholie	melancholie	k1gFnSc1
a	a	k8xC
hořkost	hořkost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Kohout	Kohout	k1gMnSc1
plaší	plašit	k5eAaImIp3nS
smrt	smrt	k1gFnSc4
(	(	kIx(
<g/>
1930	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tvář	tvář	k1gFnSc1
(	(	kIx(
<g/>
1930	#num#	k4
<g/>
,	,	kIx,
od	od	k7c2
1940	#num#	k4
spolu	spolu	k6eAd1
s	s	k7c7
Hořcem	hořec	k1gInSc7
a	a	k8xC
Tiše	tiš	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Thyrsos	thyrsos	k1gInSc1
(	(	kIx(
<g/>
1932	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
soukromý	soukromý	k2eAgInSc1d1
tisk	tisk	k1gInSc1
v	v	k7c6
počtu	počet	k1gInSc6
130	#num#	k4
kusů	kus	k1gInPc2
<g/>
,	,	kIx,
erotické	erotický	k2eAgFnSc2d1
básně	báseň	k1gFnSc2
</s>
<s>
Hořec	hořec	k1gInSc1
(	(	kIx(
<g/>
1934	#num#	k4
<g/>
)	)	kIx)
–	–	k?
k	k	k7c3
výše	vysoce	k6eAd2
zmiňované	zmiňovaný	k2eAgFnSc3d1
charakteristice	charakteristika	k1gFnSc3
se	se	k3xPyFc4
přidává	přidávat	k5eAaImIp3nS
barokní	barokní	k2eAgFnSc1d1
konstrukce	konstrukce	k1gFnSc1
verše	verš	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Staré	Staré	k2eAgFnPc1d1
ženy	žena	k1gFnPc1
(	(	kIx(
<g/>
1935	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sugestivní	sugestivní	k2eAgFnSc4d1
litanické	litanický	k2eAgNnSc1d1
líčení	líčení	k1gNnSc1
osudu	osud	k1gInSc2
starých	starý	k2eAgFnPc2d1
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
vystavěné	vystavěný	k2eAgNnSc1d1
na	na	k7c6
řetězcích	řetězec	k1gInPc6
metafor	metafora	k1gFnPc2
a	a	k8xC
refrénech	refrén	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
dílo	dílo	k1gNnSc1
nicméně	nicméně	k8xC
označil	označit	k5eAaPmAgMnS
S.	S.	kA
K.	K.	kA
Neumann	Neumann	k1gMnSc1
v	v	k7c6
dopise	dopis	k1gInSc6
Bedřichu	Bedřich	k1gMnSc3
Václavkovi	Václavek	k1gMnSc3
za	za	k7c2
maloměšťácké	maloměšťácký	k2eAgFnSc2d1
a	a	k8xC
za	za	k7c4
projev	projev	k1gInSc4
Halasova	Halasův	k2eAgNnSc2d1
„	„	k?
<g/>
kňouravého	kňouravý	k2eAgInSc2d1
pesimismu	pesimismus	k1gInSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
dokonce	dokonce	k9
ho	on	k3xPp3gMnSc4
to	ten	k3xDgNnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
napsání	napsání	k1gNnSc3
polemické	polemický	k2eAgFnSc2d1
básně	báseň	k1gFnPc4
Staří	starý	k2eAgMnPc1d1
dělníci	dělník	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Dělnice	dělnice	k1gFnSc1
–	–	k?
reakce	reakce	k1gFnSc2
na	na	k7c4
Neumannovy	Neumannův	k2eAgMnPc4d1
Staré	Staré	k2eAgMnPc4d1
dělníky	dělník	k1gMnPc4
<g/>
;	;	kIx,
spojuje	spojovat	k5eAaImIp3nS
vzpomínku	vzpomínka	k1gFnSc4
na	na	k7c4
předčasně	předčasně	k6eAd1
zemřelou	zemřelý	k2eAgFnSc4d1
matku	matka	k1gFnSc4
s	s	k7c7
typickým	typický	k2eAgInSc7d1
osudem	osud	k1gInSc7
dělnic	dělnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Dokořán	dokořán	k6eAd1
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
–	–	k?
antifašistická	antifašistický	k2eAgFnSc1d1
sbírka	sbírka	k1gFnSc1
</s>
<s>
Torzo	torzo	k1gNnSc1
naděje	naděje	k1gFnSc2
(	(	kIx(
<g/>
1938	#num#	k4
<g/>
)	)	kIx)
–	–	kIx~ 
emotivní	emotivní	k2eAgFnSc1d1
reakce	reakce	k1gFnSc1
na	na	k7c4
mnichovskou	mnichovský	k2eAgFnSc4d1
dohodu	dohoda	k1gFnSc4
a	a	k8xC
marnou	marný	k2eAgFnSc4d1
československou	československý	k2eAgFnSc4d1
mobilizaci	mobilizace	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1
má	mít	k5eAaImIp3nS
čtenáře	čtenář	k1gMnPc4
vyburcovat	vyburcovat	k5eAaPmF
a	a	k8xC
posílit	posílit	k5eAaPmF
jeho	jeho	k3xOp3gMnSc4
víru	víra	k1gFnSc4
a	a	k8xC
vzdor	vzdor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Staví	stavit	k5eAaBmIp3nS,k5eAaPmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
proti	proti	k7c3
Spojenému	spojený	k2eAgNnSc3d1
království	království	k1gNnSc3
a	a	k8xC
Francii	Francie	k1gFnSc3
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
podle	podle	k7c2
něj	on	k3xPp3gMnSc2
zavinily	zavinit	k5eAaPmAgFnP
Mnichovskou	mnichovský	k2eAgFnSc4d1
dohodu	dohoda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Označuje	označovat	k5eAaImIp3nS
je	být	k5eAaImIp3nS
za	za	k7c4
zrádce	zrádce	k1gMnSc4
a	a	k8xC
volá	volat	k5eAaImIp3nS
po	po	k7c6
odplatě	odplata	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejznámější	známý	k2eAgFnPc4d3
básně	báseň	k1gFnPc4
sbírky	sbírka	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
Praze	Praha	k1gFnSc3
</s>
<s>
Zpěv	zpěv	k1gInSc1
úzkosti	úzkost	k1gFnSc2
</s>
<s>
Mobilizace	mobilizace	k1gFnSc1
</s>
<s>
Naše	náš	k3xOp1gFnSc1
paní	paní	k1gFnSc1
Božena	Božena	k1gFnSc1
Němcová	Němcová	k1gFnSc1
(	(	kIx(
<g/>
1940	#num#	k4
<g/>
)	)	kIx)
–	–	k?
oslavný	oslavný	k2eAgInSc4d1
cyklus	cyklus	k1gInSc4
básní	báseň	k1gFnPc2
ke	k	k7c3
120	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc3
narození	narození	k1gNnSc2
Boženy	Božena	k1gFnSc2
Němcové	Němcová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tragický	tragický	k2eAgInSc1d1
osud	osud	k1gInSc1
Boženy	Božena	k1gFnSc2
Němcové	Němcová	k1gFnSc2
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
paralelou	paralela	k1gFnSc7
k	k	k7c3
situaci	situace	k1gFnSc3
českého	český	k2eAgInSc2d1
národa	národ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Ladění	ladění	k1gNnSc1
(	(	kIx(
<g/>
1942	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
básně	báseň	k1gFnPc4
z	z	k7c2
let	léto	k1gNnPc2
1937	#num#	k4
<g/>
–	–	k?
<g/>
1941	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c6
sbírce	sbírka	k1gFnSc6
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
též	též	k9
oddíl	oddíl	k1gInSc1
veršů	verš	k1gInPc2
pro	pro	k7c4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Já	já	k3xPp1nSc1
se	se	k3xPyFc4
tam	tam	k6eAd1
vrátím	vrátit	k5eAaPmIp1nS
(	(	kIx(
<g/>
1939	#num#	k4
<g/>
,	,	kIx,
vydáno	vydat	k5eAaPmNgNnS
1947	#num#	k4
<g/>
)	)	kIx)
–	–	k?
o	o	k7c6
kraji	kraj	k1gInSc6
Halasova	Halasův	k2eAgNnSc2d1
mládí	mládí	k1gNnSc2
(	(	kIx(
<g/>
Kunštát	Kunštát	k1gInSc1
<g/>
,	,	kIx,
Českomoravská	českomoravský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
lyrizovanou	lyrizovaný	k2eAgFnSc4d1
prózu	próza	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
řadě	řada	k1gFnSc6
(	(	kIx(
<g/>
1948	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
A	a	k9
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
vydáno	vydat	k5eAaPmNgNnS
1957	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
době	doba	k1gFnSc6
tzv.	tzv.	kA
Tání	tání	k1gNnSc4
<g/>
)	)	kIx)
–	–	k?
zklamání	zklamání	k1gNnSc4
z	z	k7c2
vývoje	vývoj	k1gInSc2
po	po	k7c6
únoru	únor	k1gInSc6
1948	#num#	k4
i	i	k8xC
tušení	tušení	k1gNnSc4
vlastní	vlastní	k2eAgFnSc2d1
smrti	smrt	k1gFnSc2
vedly	vést	k5eAaImAgFnP
ke	k	k7c3
změně	změna	k1gFnSc3
autorovy	autorův	k2eAgFnSc2d1
poetiky	poetika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
básně	báseň	k1gFnPc1
sbírky	sbírka	k1gFnSc2
se	se	k3xPyFc4
vyznačují	vyznačovat	k5eAaImIp3nP
syrovostí	syrovost	k1gFnSc7
<g/>
,	,	kIx,
snahou	snaha	k1gFnSc7
o	o	k7c4
maximální	maximální	k2eAgFnSc4d1
úspornost	úspornost	k1gFnSc4
<g/>
,	,	kIx,
sevřenost	sevřenost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reakce	reakce	k1gFnSc1
na	na	k7c4
poválečný	poválečný	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
jen	jen	k9
v	v	k7c6
náznaku	náznak	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
umožňuje	umožňovat	k5eAaImIp3nS
autorovi	autor	k1gMnSc3
jeho	jeho	k3xOp3gInPc4
obrazy	obraz	k1gInPc4
zobecnit	zobecnit	k5eAaPmF
a	a	k8xC
vyslovit	vyslovit	k5eAaPmF
obavy	obava	k1gFnPc4
o	o	k7c4
osud	osud	k1gInSc4
nejen	nejen	k6eAd1
českého	český	k2eAgInSc2d1
národa	národ	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
celé	celý	k2eAgFnSc2d1
civilizace	civilizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevují	objevovat	k5eAaImIp3nP
se	se	k3xPyFc4
autobiografické	autobiografický	k2eAgInPc1d1
motivy	motiv	k1gInPc1
<g/>
,	,	kIx,
úvahy	úvaha	k1gFnPc1
o	o	k7c6
smyslu	smysl	k1gInSc6
a	a	k8xC
možnostech	možnost	k1gFnPc6
poezie	poezie	k1gFnSc2
<g/>
,	,	kIx,
závěrečná	závěrečný	k2eAgFnSc1d1
báseň	báseň	k1gFnSc1
A	a	k8xC
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
básník	básník	k1gMnSc1
vyslovuje	vyslovovat	k5eAaImIp3nS
básníkovo	básníkův	k2eAgNnSc4d1
krédo	krédo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Potopa	potopa	k1gFnSc1
–	–	k?
dlouhodobý	dlouhodobý	k2eAgInSc1d1
básnický	básnický	k2eAgInSc1d1
projekt	projekt	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
zůstal	zůstat	k5eAaPmAgInS
nedokončen	dokončit	k5eNaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
Hlad	hlad	k1gInSc1
–	–	k?
dlouhodobý	dlouhodobý	k2eAgInSc1d1
básnický	básnický	k2eAgInSc1d1
projekt	projekt	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
zůstal	zůstat	k5eAaPmAgInS
nedokončen	dokončit	k5eNaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
Brzy	brzy	k6eAd1
po	po	k7c6
smrti	smrt	k1gFnSc6
zaútočil	zaútočit	k5eAaPmAgMnS
na	na	k7c4
Halasovo	Halasův	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
komunistický	komunistický	k2eAgMnSc1d1
kritik	kritik	k1gMnSc1
Ladislav	Ladislav	k1gMnSc1
Štoll	Štoll	k1gMnSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnPc1
odsudek	odsudek	k1gInSc4
Halasova	Halasův	k2eAgNnSc2d1
díla	dílo	k1gNnSc2
byl	být	k5eAaImAgInS
platným	platný	k2eAgInSc7d1
hodnotícím	hodnotící	k2eAgInSc7d1
soudem	soud	k1gInSc7
až	až	k9
do	do	k7c2
konce	konec	k1gInSc2
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
mohla	moct	k5eAaImAgFnS
konečně	konečně	k6eAd1
vyjít	vyjít	k5eAaPmF
Halasova	Halasův	k2eAgFnSc1d1
poslední	poslední	k2eAgFnSc1d1
sbírka	sbírka	k1gFnSc1
A	a	k9
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
i	i	k8xC
první	první	k4xOgInSc1
úplný	úplný	k2eAgInSc1d1
soubor	soubor	k1gInSc1
jeho	jeho	k3xOp3gFnPc2
básnických	básnický	k2eAgFnPc2d1
knih	kniha	k1gFnPc2
(	(	kIx(
<g/>
obojí	oboj	k1gFnPc2
1957	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
začaly	začít	k5eAaPmAgInP
vycházet	vycházet	k5eAaImF
kriticky	kriticky	k6eAd1
připravené	připravený	k2eAgInPc4d1
sebrané	sebraný	k2eAgInPc4d1
spisy	spis	k1gInPc4
<g/>
,	,	kIx,
na	na	k7c6
jejich	jejich	k3xOp3gNnSc6
pořádání	pořádání	k1gNnSc6
se	se	k3xPyFc4
podílel	podílet	k5eAaImAgMnS
básníkův	básníkův	k2eAgMnSc1d1
syn	syn	k1gMnSc1
František	František	k1gMnSc1
X.	X.	kA
Halas	Halas	k1gMnSc1
<g/>
,	,	kIx,
literární	literární	k2eAgMnSc1d1
historik	historik	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Brabec	Brabec	k1gMnSc1
a	a	k8xC
básník	básník	k1gMnSc1
a	a	k8xC
překladatel	překladatel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Kundera	Kundera	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krom	krom	k7c2
textů	text	k1gInPc2
publikovaných	publikovaný	k2eAgInPc2d1
za	za	k7c2
básníkova	básníkův	k2eAgInSc2d1
života	život	k1gInSc2
se	se	k3xPyFc4
ve	v	k7c6
spisech	spis	k1gInPc6
objevuje	objevovat	k5eAaImIp3nS
množství	množství	k1gNnSc1
rukopisného	rukopisný	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
svazek	svazek	k1gInSc1
nese	nést	k5eAaImIp3nS
název	název	k1gInSc4
Krásné	krásný	k2eAgNnSc1d1
neštěstí	neštěstí	k1gNnSc1
a	a	k8xC
shrnuje	shrnovat	k5eAaImIp3nS
tvorbu	tvorba	k1gFnSc4
do	do	k7c2
roku	rok	k1gInSc2
1933	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc4d1
svazek	svazek	k1gInSc4
<g/>
,	,	kIx,
Časy	čas	k1gInPc4
(	(	kIx(
<g/>
léta	léto	k1gNnSc2
1934	#num#	k4
<g/>
–	–	k?
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
vyšel	vyjít	k5eAaPmAgMnS
až	až	k6eAd1
roku	rok	k1gInSc2
1981	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Závěrečnou	závěrečný	k2eAgFnSc4d1
etapu	etapa	k1gFnSc4
básníkovy	básníkův	k2eAgFnSc2d1
tvorby	tvorba	k1gFnSc2
shrnuje	shrnovat	k5eAaImIp3nS
svazek	svazek	k1gInSc1
A	a	k9
co	co	k9
básník	básník	k1gMnSc1
z	z	k7c2
roku	rok	k1gInSc2
1983	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krom	krom	k7c2
básnické	básnický	k2eAgFnSc2d1
tvorby	tvorba	k1gFnSc2
se	se	k3xPyFc4
do	do	k7c2
spisů	spis	k1gInPc2
dostalo	dostat	k5eAaPmAgNnS
i	i	k9
sebrání	sebrání	k1gNnSc1
Halasových	Halasových	k2eAgInPc2d1
textů	text	k1gInPc2
o	o	k7c6
literatuře	literatura	k1gFnSc6
(	(	kIx(
<g/>
Imagena	Imagena	k1gFnSc1
<g/>
,	,	kIx,
1971	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
výtvarném	výtvarný	k2eAgNnSc6d1
umění	umění	k1gNnSc6
(	(	kIx(
<g/>
Obrazy	obraz	k1gInPc1
<g/>
,	,	kIx,
1968	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
byl	být	k5eAaImAgMnS
v	v	k7c6
rámci	rámec	k1gInSc6
spisů	spis	k1gInPc2
vydán	vydán	k2eAgInSc1d1
výbor	výbor	k1gInSc1
z	z	k7c2
básníkovy	básníkův	k2eAgFnSc2d1
korespondence	korespondence	k1gFnSc2
(	(	kIx(
<g/>
Dopisy	dopis	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Překlady	překlad	k1gInPc4
</s>
<s>
Halas	Halas	k1gMnSc1
rovněž	rovněž	k9
překládal	překládat	k5eAaImAgMnS
za	za	k7c2
jazykové	jazykový	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
s	s	k7c7
dalšími	další	k2eAgMnPc7d1
autory	autor	k1gMnPc7
<g/>
:	:	kIx,
</s>
<s>
z	z	k7c2
polštiny	polština	k1gFnSc2
(	(	kIx(
<g/>
spolupráce	spolupráce	k1gFnSc1
Josef	Josef	k1gMnSc1
Matouš	Matouš	k1gMnSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Adam	Adam	k1gMnSc1
Mickiewicz	Mickiewicz	k1gMnSc1
<g/>
,	,	kIx,
Juliusz	Juliusz	k1gMnSc1
Słowacki	Słowack	k1gFnSc2
</s>
<s>
z	z	k7c2
ruštiny	ruština	k1gFnSc2
(	(	kIx(
<g/>
spolupráce	spolupráce	k1gFnSc1
Ladislav	Ladislav	k1gMnSc1
Fikar	Fikar	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Hrubín	Hrubín	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
Eisner	Eisner	k1gMnSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Alexandr	Alexandr	k1gMnSc1
Sergejevič	Sergejevič	k1gMnSc1
Puškin	Puškin	k1gMnSc1
</s>
<s>
z	z	k7c2
maďarštiny	maďarština	k1gFnSc2
(	(	kIx(
<g/>
spolupráce	spolupráce	k1gFnSc2
Vilém	Vilém	k1gMnSc1
Závada	závada	k1gFnSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Endre	Endr	k1gMnSc5
Ady	Ady	k1gMnSc5
</s>
<s>
další	další	k2eAgInPc1d1
překlady	překlad	k1gInPc1
z	z	k7c2
bulharštiny	bulharština	k1gFnSc2
<g/>
,	,	kIx,
slovinštiny	slovinština	k1gFnSc2
a	a	k8xC
srbochorvatštiny	srbochorvatština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Hrob	hrob	k1gInSc1
Františka	František	k1gMnSc2
Halase	Halas	k1gMnSc2
v	v	k7c6
Kunštátě	Kunštáta	k1gFnSc6
</s>
<s>
František	František	k1gMnSc1
Halas	Halas	k1gMnSc1
<g/>
,	,	kIx,
hrob	hrob	k1gInSc1
na	na	k7c6
hřbitově	hřbitov	k1gInSc6
v	v	k7c6
Kunštátě	Kunštáta	k1gFnSc6
</s>
<s>
Detail	detail	k1gInSc1
náhrobního	náhrobní	k2eAgInSc2d1
kamene	kámen	k1gInSc2
</s>
<s>
Detail	detail	k1gInSc1
nápisu	nápis	k1gInSc2
na	na	k7c6
hrobě	hrob	k1gInSc6
</s>
<s>
Dům	dům	k1gInSc1
Františka	František	k1gMnSc2
Halase	Halas	k1gMnSc2
v	v	k7c6
Kunštátě	Kunštáta	k1gFnSc6
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Matriční	matriční	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c6
narození	narození	k1gNnSc6
a	a	k8xC
křtu	křest	k1gInSc6
zemská	zemský	k2eAgFnSc1d1
porodnice	porodnice	k1gFnSc1
Brno	Brno	k1gNnSc1
<g/>
↑	↑	k?
Matriční	matriční	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c6
sňatku	sňatek	k1gInSc6
rodičů	rodič	k1gMnPc2
Františka	František	k1gMnSc2
Halase	Halas	k1gMnSc2
farnosti	farnost	k1gFnSc2
Brno-Zábrdovice	Brno-Zábrdovice	k1gFnSc1
(	(	kIx(
<g/>
kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
)	)	kIx)
<g/>
↑	↑	k?
HALAS	Halas	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
;	;	kIx,
ZAHRADNÍČEK	Zahradníček	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
dálky	dálka	k1gFnPc4
<g/>
…	…	k?
:	:	kIx,
Vzájemná	vzájemný	k2eAgFnSc1d1
korespondence	korespondence	k1gFnSc1
Františka	František	k1gMnSc2
Halase	Halas	k1gMnSc2
a	a	k8xC
Jana	Jan	k1gMnSc2
Zahradníčka	Zahradníček	k1gMnSc2
z	z	k7c2
let	léto	k1gNnPc2
1930	#num#	k4
<g/>
–	–	k?
<g/>
1949	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Jan	Jan	k1gMnSc1
Weindl	Weindl	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Komárek	Komárek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
;	;	kIx,
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
28	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
.	.	kIx.
136	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
591	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
Recenze	recenze	k1gFnSc1
<g/>
:	:	kIx,
Karel	Karel	k1gMnSc1
Komárek	Komárek	k1gMnSc1
Archivováno	archivován	k2eAgNnSc4d1
26	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
jmenný	jmenný	k2eAgInSc1d1
rejstřík	rejstřík	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Čeští	český	k2eAgMnPc1d1
spisovatelé	spisovatel	k1gMnPc1
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Československý	československý	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
156	#num#	k4
<g/>
–	–	k?
<g/>
160	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Čeští	český	k2eAgMnPc1d1
spisovatelé	spisovatel	k1gMnPc1
literatury	literatura	k1gFnSc2
pro	pro	k7c4
děti	dítě	k1gFnPc4
a	a	k8xC
mládež	mládež	k1gFnSc4
/	/	kIx~
redakce	redakce	k1gFnSc2
Otakar	Otakar	k1gMnSc1
Chaloupka	Chaloupka	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Albatros	albatros	k1gMnSc1
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
102	#num#	k4
<g/>
–	–	k?
<g/>
106	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
české	český	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Literatura	literatura	k1gFnSc1
od	od	k7c2
konce	konec	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
do	do	k7c2
roku	rok	k1gInSc2
1945	#num#	k4
/	/	kIx~
hlavní	hlavní	k2eAgMnSc1d1
redaktor	redaktor	k1gMnSc1
Jan	Jan	k1gMnSc1
Mukařovský	Mukařovský	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Victoria	Victorium	k1gNnSc2
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
714	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85865	#num#	k4
<g/>
-	-	kIx~
<g/>
48	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
529	#num#	k4
<g/>
–	–	k?
<g/>
553	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
FORST	FORST	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lexikon	lexikon	k1gNnSc1
české	český	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
:	:	kIx,
osobnosti	osobnost	k1gFnPc1
<g/>
,	,	kIx,
díla	dílo	k1gNnPc1
<g/>
,	,	kIx,
instituce	instituce	k1gFnPc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
/	/	kIx~
<g/>
I.	I.	kA
H	H	kA
<g/>
–	–	k?
<g/>
J.	J.	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
589	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
468	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
37	#num#	k4
<g/>
–	–	k?
<g/>
43	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MED	med	k1gInSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Literární	literární	k2eAgInSc4d1
život	život	k1gInSc4
ve	v	k7c6
stínu	stín	k1gInSc6
Mnichova	Mnichov	k1gInSc2
(	(	kIx(
<g/>
1938	#num#	k4
<g/>
–	–	k?
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
340	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
1823	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Osobnosti	osobnost	k1gFnPc1
–	–	k?
Česko	Česko	k1gNnSc1
:	:	kIx,
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ottovo	Ottův	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
823	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7360	#num#	k4
<g/>
-	-	kIx~
<g/>
796	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
198	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PUTNA	putna	k1gFnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
C.	C.	kA
Česká	český	k2eAgFnSc1d1
katolická	katolický	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
v	v	k7c6
kontextech	kontext	k1gInPc6
:	:	kIx,
1918	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Torst	Torst	k1gMnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
1390	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
721	#num#	k4
<g/>
-	-	kIx~
<g/>
5391	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
BAUER	Bauer	k1gMnSc1
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tíseň	tíseň	k1gFnSc1
tmy	tma	k1gFnSc2
aneb	aneb	k?
Halasovské	Halasovský	k2eAgFnSc2d1
interpretace	interpretace	k1gFnSc2
po	po	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Akropolis	Akropolis	k1gFnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
470	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86903	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HEJDÁNEK	HEJDÁNEK	kA
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Básník	básník	k1gMnSc1
a	a	k8xC
slovo	slovo	k1gNnSc1
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Setkání	setkání	k1gNnSc1
a	a	k8xC
odstup	odstup	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
OIKOYMENH	OIKOYMENH	kA
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
230	#num#	k4
<g/>
–	–	k?
<g/>
252	#num#	k4
<g/>
,	,	kIx,
elektronicky	elektronicky	k6eAd1
zde	zde	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
českých	český	k2eAgMnPc2d1
spisovatelů	spisovatel	k1gMnPc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
v	v	k7c6
letech	let	k1gInPc6
1900	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
</s>
<s>
Česká	český	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
v	v	k7c6
letech	let	k1gInPc6
1945	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
</s>
<s>
Existencialismus	existencialismus	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Osoba	osoba	k1gFnSc1
František	František	k1gMnSc1
Halas	Halas	k1gMnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Autor	autor	k1gMnSc1
František	František	k1gMnSc1
Halas	Halas	k1gMnSc1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
František	františek	k1gInSc1
Halas	halas	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
František	František	k1gMnSc1
Halas	Halas	k1gMnSc1
</s>
<s>
Digitalizovaná	digitalizovaný	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
Františka	František	k1gMnSc2
Halase	Halas	k1gMnSc2
v	v	k7c6
digitální	digitální	k2eAgFnSc6d1
knihovně	knihovna	k1gFnSc6
Kramerius	Kramerius	k1gMnSc1
NK	NK	kA
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s>
Halas	halas	k1gInSc1
na	na	k7c6
Literárním	literární	k2eAgNnSc6d1
doupěti	doupě	k1gNnSc6
</s>
<s>
Halasovy	Halasův	k2eAgInPc1d1
překlady	překlad	k1gInPc1
</s>
<s>
František	František	k1gMnSc1
Halas	Halas	k1gMnSc1
ve	v	k7c6
Slovníku	slovník	k1gInSc6
české	český	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
po	po	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Devětsil	Devětsil	k1gInSc1
(	(	kIx(
<g/>
1920	#num#	k4
<g/>
–	–	k?
<g/>
1930	#num#	k4
<g/>
)	)	kIx)
zakladatelé	zakladatel	k1gMnPc1
</s>
<s>
Adolf	Adolf	k1gMnSc1
Hoffmeister	Hoffmeister	k1gMnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Seifert	Seifert	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
Teige	Teig	k1gFnSc2
•	•	k?
Vladislav	Vladislav	k1gMnSc1
Vančura	Vančura	k1gMnSc1
vůdčí	vůdčí	k2eAgFnSc3d1
osobnosti	osobnost	k1gFnSc3
</s>
<s>
Vítězslav	Vítězslav	k1gMnSc1
Nezval	Nezval	k1gMnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Seifert	Seifert	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
Teige	Teig	k1gFnSc2
architekti	architekt	k1gMnPc1
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Fragner	Fragner	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Gillar	Gillar	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Havlíček	Havlíček	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
Honzík	Honzík	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Chochol	Chochol	k1gMnSc1
•	•	k?
Jaromír	Jaromír	k1gMnSc1
Krejcar	krejcar	k1gInSc1
•	•	k?
Evžen	Evžen	k1gMnSc1
Linhart	Linhart	k1gMnSc1
•	•	k?
Pavel	Pavel	k1gMnSc1
Smetana	Smetana	k1gMnSc1
básníci	básník	k1gMnPc1
</s>
<s>
Vítězslav	Vítězslav	k1gMnSc1
Nezval	Nezval	k1gMnSc1
•	•	k?
Konstantin	Konstantin	k1gMnSc1
Biebl	Biebl	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Halas	Halas	k1gMnSc1
•	•	k?
Jindřich	Jindřich	k1gMnSc1
Hořejší	Hořejší	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Wolker	Wolker	k1gMnSc1
hudebníci	hudebník	k1gMnPc1
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Ježek	Ježek	k1gMnSc1
režiséři	režisér	k1gMnPc1
</s>
<s>
Emil	Emil	k1gMnSc1
František	František	k1gMnSc1
Burian	Burian	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Frejka	Frejka	k1gFnSc1
•	•	k?
Jindřich	Jindřich	k1gMnSc1
Honzl	Honzl	k1gMnSc1
•	•	k?
Vladislav	Vladislav	k1gMnSc1
Vančura	Vančura	k1gMnSc1
spisovatelé	spisovatel	k1gMnPc1
</s>
<s>
Julius	Julius	k1gMnSc1
Fučík	Fučík	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
Konrád	Konrád	k1gMnSc1
•	•	k?
Vladislav	Vladislav	k1gMnSc1
Vančura	Vančura	k1gMnSc1
výtvarníci	výtvarník	k1gMnPc1
</s>
<s>
Adolf	Adolf	k1gMnSc1
Hoffmeister	Hoffmeistra	k1gFnPc2
•	•	k?
Otakar	Otakara	k1gFnPc2
Mrkvička	mrkvička	k1gFnSc1
•	•	k?
František	františek	k1gInSc1
Muzika	muzika	k1gFnSc1
•	•	k?
Jindřich	Jindřich	k1gMnSc1
Štyrský	Štyrský	k2eAgMnSc1d1
•	•	k?
Toyen	Toyen	k1gInSc1
fotografové	fotograf	k1gMnPc5
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Rössler	Rössler	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
Teige	Teig	k1gFnSc2
•	•	k?
Evžen	Evžen	k1gMnSc1
Markalous	Markalous	k1gMnSc1
•	•	k?
Jindřich	Jindřich	k1gMnSc1
Štyrský	Štyrský	k2eAgMnSc1d1
kritici	kritik	k1gMnPc1
<g/>
,	,	kIx,
teoretici	teoretik	k1gMnPc1
<g/>
,	,	kIx,
vědci	vědec	k1gMnPc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Frejka	Frejka	k1gFnSc1
•	•	k?
Julius	Julius	k1gMnSc1
Fučík	Fučík	k1gMnSc1
•	•	k?
Jindřich	Jindřich	k1gMnSc1
Štyrský	Štyrský	k2eAgMnSc1d1
•	•	k?
Karel	Karel	k1gMnSc1
Teige	Teig	k1gFnSc2
•	•	k?
Bedřich	Bedřich	k1gMnSc1
Václavek	Václavek	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jk	jk	k?
<g/>
0	#num#	k4
<g/>
1033200	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
119470438	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0881	#num#	k4
6483	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80164796	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
27099101	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80164796	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Literatura	literatura	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
