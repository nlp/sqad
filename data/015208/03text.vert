<s>
Ann	Ann	k?
Rosener	Rosener	k1gInSc1
</s>
<s>
Ann	Ann	k?
Rosener	Rosener	k1gInSc1
Narození	narození	k1gNnSc2
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1914	#num#	k4
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2012	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
97	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Menlo	Menlo	k1gNnSc1
Park	park	k1gInSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Smith	Smith	k1gMnSc1
College	Colleg	k1gMnSc2
Povolání	povolání	k1gNnSc2
</s>
<s>
fotografka	fotografka	k1gFnSc1
<g/>
,	,	kIx,
fotoreportérka	fotoreportérka	k1gFnSc1
a	a	k8xC
novinářka	novinářka	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Typická	typický	k2eAgFnSc1d1
fotografie	fotografie	k1gFnSc1
autorky	autorka	k1gFnSc2
</s>
<s>
Ann	Ann	k?
Rosener	Rosener	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
25	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1914	#num#	k4
–	–	k?
19	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2012	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
americká	americký	k2eAgFnSc1d1
fotografka	fotografka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
specializovala	specializovat	k5eAaBmAgFnS
na	na	k7c4
snímky	snímek	k1gInPc4
obyčejných	obyčejný	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
v	v	k7c6
každodenním	každodenní	k2eAgInSc6d1
životě	život	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pracovala	pracovat	k5eAaImAgFnS
jako	jako	k9
fotografka	fotografka	k1gFnSc1
pro	pro	k7c4
Works	Works	kA
Progress	Progress	k1gInSc1
Administration	Administration	k1gInSc1
a	a	k8xC
ve	v	k7c6
společnosti	společnost	k1gFnSc6
Office	Office	kA
of	of	k?
War	War	k1gFnSc2
Informations	Informationsa	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
z	z	k7c2
instituce	instituce	k1gFnSc2
Farm	Farma	k1gFnPc2
Security	Securita	k1gFnSc2
Administration	Administration	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
a	a	k8xC
dílo	dílo	k1gNnSc1
</s>
<s>
Patřila	patřit	k5eAaImAgFnS
do	do	k7c2
skupiny	skupina	k1gFnSc2
talentovaných	talentovaný	k2eAgMnPc2d1
fotografů	fotograf	k1gMnPc2
najatých	najatý	k2eAgMnPc2d1
Royem	Roy	k1gMnSc7
Strykerem	Stryker	k1gInSc7
právě	právě	k9
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
často	často	k6eAd1
nazývána	nazývat	k5eAaImNgFnS
"	"	kIx"
<g/>
zlatým	zlatý	k2eAgInSc7d1
věkem	věk	k1gInSc7
dokumentární	dokumentární	k2eAgFnSc2d1
fotografie	fotografia	k1gFnSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Společnost	společnost	k1gFnSc1
FSA	FSA	kA
byla	být	k5eAaImAgFnS
vytvořena	vytvořit	k5eAaPmNgFnS
v	v	k7c6
USA	USA	kA
v	v	k7c6
roce	rok	k1gInSc6
1935	#num#	k4
v	v	k7c6
rámci	rámec	k1gInSc6
New	New	k1gMnSc1
Deal	Deal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gInSc7
úkolem	úkol	k1gInSc7
bylo	být	k5eAaImAgNnS
v	v	k7c6
krizi	krize	k1gFnSc6
pomáhat	pomáhat	k5eAaImF
proti	proti	k7c3
americké	americký	k2eAgFnSc3d1
venkovské	venkovský	k2eAgFnSc3d1
chudobě	chudoba	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
FSA	FSA	kA
podporovalo	podporovat	k5eAaImAgNnS
skupování	skupování	k1gNnSc1
okrajových	okrajový	k2eAgInPc2d1
pozemků	pozemek	k1gInPc2
<g/>
,	,	kIx,
práci	práce	k1gFnSc4
na	na	k7c6
velkých	velký	k2eAgInPc6d1
pozemcích	pozemek	k1gInPc6
s	s	k7c7
moderními	moderní	k2eAgInPc7d1
stroji	stroj	k1gInPc7
a	a	k8xC
kolektivizaci	kolektivizace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
vstupem	vstup	k1gInSc7
USA	USA	kA
do	do	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
však	však	k9
nastala	nastat	k5eAaPmAgFnS
změna	změna	k1gFnSc1
<g/>
:	:	kIx,
projekt	projekt	k1gInSc1
FSA	FSA	kA
dostal	dostat	k5eAaPmAgInS
jiné	jiný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
–	–	k?
Office	Office	kA
of	of	k?
War	War	k1gFnSc1
Informations	Informations	k1gInSc1
(	(	kIx(
<g/>
OWI	OWI	kA
<g/>
)	)	kIx)
–	–	k?
a	a	k8xC
také	také	k9
jiný	jiný	k2eAgInSc4d1
program	program	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
válce	válka	k1gFnSc6
musela	muset	k5eAaImAgFnS
propaganda	propaganda	k1gFnSc1
ukazovat	ukazovat	k5eAaImF
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
jsou	být	k5eAaImIp3nP
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
silné	silný	k2eAgInPc1d1
a	a	k8xC
ne	ne	k9
jaké	jaký	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
mají	mít	k5eAaImIp3nP
potíže	potíž	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
organizace	organizace	k1gFnSc1
zanikla	zaniknout	k5eAaPmAgFnS
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
dokonce	dokonce	k9
snaha	snaha	k1gFnSc1
pořízené	pořízený	k2eAgInPc4d1
dokumenty	dokument	k1gInPc4
zničit	zničit	k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
nemohly	moct	k5eNaImAgFnP
být	být	k5eAaImF
použity	použít	k5eAaPmNgFnP
pro	pro	k7c4
propagandu	propaganda	k1gFnSc4
komunistickou	komunistický	k2eAgFnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Ženy	žena	k1gFnPc1
ruční	ruční	k2eAgFnPc1d1
sklizní	sklizeň	k1gFnSc7
chřestu	chřest	k1gInSc2
pomáhají	pomáhat	k5eAaImIp3nP
udržet	udržet	k5eAaPmF
národní	národní	k2eAgInSc4d1
blahobyt	blahobyt	k1gInSc4
<g/>
,	,	kIx,
Rochelle	Rochelle	k1gInSc4
<g/>
,	,	kIx,
Illinois	Illinois	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Řezníci	řezník	k1gMnPc1
vykupují	vykupovat	k5eAaImIp3nP
od	od	k7c2
domácích	domácí	k2eAgMnPc2d1
chovatelů	chovatel	k1gMnPc2
tuk	tuk	k1gInSc4
a	a	k8xC
prodávají	prodávat	k5eAaImIp3nP
je	on	k3xPp3gFnPc4
do	do	k7c2
kafilérie	kafilérie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
bude	být	k5eAaImBp3nS
zpracován	zpracovat	k5eAaPmNgInS
na	na	k7c4
střelivo	střelivo	k1gNnSc4
pro	pro	k7c4
muže	muž	k1gMnPc4
bojující	bojující	k2eAgInSc1d1
za	za	k7c4
Ameriku	Amerika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Voják	voják	k1gMnSc1
na	na	k7c6
domácí	domácí	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
ukládá	ukládat	k5eAaImIp3nS
všechny	všechen	k3xTgInPc1
odpadní	odpadní	k2eAgInPc4d1
tuky	tuk	k1gInPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
mohly	moct	k5eAaImAgFnP
být	být	k5eAaImF
zpracovány	zpracovat	k5eAaPmNgFnP
do	do	k7c2
munice	munice	k1gFnSc2
pro	pro	k7c4
americké	americký	k2eAgMnPc4d1
vojáky	voják	k1gMnPc4
<g/>
...	...	k?
</s>
<s>
Sběr	sběr	k1gInSc1
starého	starý	k2eAgInSc2d1
papíru	papír	k1gInSc2
ušetří	ušetřit	k5eAaPmIp3nS
Strýčku	strýček	k1gMnSc3
Samovi	Sam	k1gMnSc3
miliony	milion	k4xCgInPc4
dolarů	dolar	k1gInPc2
ročně	ročně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Fotografie	fotografia	k1gFnPc1
propagující	propagující	k2eAgFnSc4d1
recyklaci	recyklace	k1gFnSc4
kovů	kov	k1gInPc2
během	během	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
Slečna	slečna	k1gFnSc1
Anna	Anna	k1gFnSc1
Bland	Bland	k1gInSc1
<g/>
,	,	kIx,
svářečka	svářečka	k1gFnSc1
<g/>
,	,	kIx,
afroamerická	afroamerický	k2eAgFnSc1d1
pracovnice	pracovnice	k1gFnSc1
v	v	k7c6
richmondských	richmondský	k2eAgFnPc6d1
loděnicích	loděnice	k1gFnPc6
<g/>
,	,	kIx,
Richmond	Richmond	k1gInSc1
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spěšné	spěšný	k2eAgNnSc1d1
dokončování	dokončování	k1gNnPc1
lodě	loď	k1gFnSc2
SS	SS	kA
George	George	k1gInSc1
Washington	Washington	k1gInSc1
Carver	Carver	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvalifikovaní	kvalifikovaný	k2eAgMnPc1d1
černošští	černošský	k2eAgMnPc1d1
dělníci	dělník	k1gMnPc1
hrají	hrát	k5eAaImIp3nP
důležitou	důležitý	k2eAgFnSc4d1
roli	role	k1gFnSc4
při	při	k7c6
konstruování	konstruování	k1gNnSc6
SS	SS	kA
George	George	k1gFnSc1
Washington	Washington	k1gInSc1
Carver	Carver	k1gInSc1
<g/>
,	,	kIx,
druhé	druhý	k4xOgFnSc2
lodi	loď	k1gFnSc2
pojmenované	pojmenovaný	k2eAgFnSc2d1
po	po	k7c6
černochovi	černochův	k2eAgMnPc1d1
v	v	k7c6
loděnicích	loděnice	k1gFnPc6
společnosti	společnost	k1gFnSc2
Kaiser	Kaiser	k1gInSc1
v	v	k7c6
Richmondu	Richmond	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
4	#num#	k4
loděnicích	loděnice	k1gFnPc6
Kaiser	Kaisra	k1gFnPc2
pracuje	pracovat	k5eAaImIp3nS
asi	asi	k9
1000	#num#	k4
černošek	černoška	k1gFnPc2
z	z	k7c2
více	hodně	k6eAd2
než	než	k8xS
6000	#num#	k4
"	"	kIx"
<g/>
barevných	barevný	k2eAgInPc2d1
<g/>
"	"	kIx"
dělníků	dělník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
poníženy	ponížen	k2eAgInPc4d1
body	bod	k1gInPc4
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
ponížena	ponížit	k5eAaPmNgFnS
i	i	k9
cena	cena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
přeškrtlé	přeškrtlý	k2eAgInPc4d1
body	bod	k1gInPc4
bez	bez	k7c2
přeškrtlých	přeškrtlý	k2eAgFnPc2d1
cen	cena	k1gFnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jeden	jeden	k4xCgInSc1
z	z	k7c2
nejjistějších	jistý	k2eAgInPc2d3
náznaků	náznak	k1gInPc2
masa	maso	k1gNnSc2
z	z	k7c2
černého	černý	k2eAgInSc2d1
trhu	trh	k1gInSc2
<g/>
,	,	kIx,
asi	asi	k9
1943	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Portrét	portrét	k1gInSc1
autorky	autorka	k1gFnSc2
na	na	k7c6
stránkách	stránka	k1gFnPc6
Library	Librara	k1gFnSc2
of	of	k?
Congress	Congress	k1gInSc1
<g/>
↑	↑	k?
CDS	CDS	kA
Exhibits	Exhibits	k1gInSc1
<g/>
.	.	kIx.
cds	cds	k?
<g/>
.	.	kIx.
<g/>
aas	aas	k?
<g/>
.	.	kIx.
<g/>
duke	duke	k1gFnSc1
<g/>
.	.	kIx.
<g/>
edu	edu	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
12	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
2008	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
PETÁK	PETÁK	kA
<g/>
.	.	kIx.
diplomová	diplomový	k2eAgFnSc1d1
práce	práce	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opava	Opava	k1gFnSc1
<g/>
:	:	kIx,
Slezská	slezský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
Filozoficko-přírodovědecká	filozoficko-přírodovědecký	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
,	,	kIx,
Institut	institut	k1gInSc1
tvůrčí	tvůrčí	k2eAgFnSc2d1
fotografie	fotografia	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
amerických	americký	k2eAgFnPc2d1
fotografek	fotografka	k1gFnPc2
</s>
<s>
Fotografie	fotografia	k1gFnPc1
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgInPc6d1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Ann	Ann	k1gFnSc2
Rosener	Rosener	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Fotografie	fotografia	k1gFnPc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
nr	nr	k?
<g/>
96038683	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
305051742	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-nr	lccn-nr	k1gInSc1
<g/>
96038683	#num#	k4
</s>
