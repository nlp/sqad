<p>
<s>
Operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
označení	označení	k1gNnSc2	označení
pro	pro	k7c4	pro
základní	základní	k2eAgNnSc4d1	základní
programové	programový	k2eAgNnSc4d1	programové
vybavení	vybavení	k1gNnSc4	vybavení
počítače	počítač	k1gInSc2	počítač
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
software	software	k1gInSc1	software
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
je	být	k5eAaImIp3nS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
počítače	počítač	k1gInSc2	počítač
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
startu	start	k1gInSc6	start
a	a	k8xC	a
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
v	v	k7c6	v
činnosti	činnost	k1gFnSc6	činnost
až	až	k9	až
do	do	k7c2	do
jeho	jeho	k3xOp3gNnSc2	jeho
vypnutí	vypnutí	k1gNnSc2	vypnutí
<g/>
.	.	kIx.	.
</s>
<s>
Operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
(	(	kIx(	(
<g/>
též	též	k9	též
označovaného	označovaný	k2eAgMnSc2d1	označovaný
jako	jako	k8xC	jako
kernel	kernel	k1gInSc1	kernel
<g/>
)	)	kIx)	)
a	a	k8xC	a
pomocných	pomocný	k2eAgInPc2d1	pomocný
systémových	systémový	k2eAgInPc2d1	systémový
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
úkolem	úkol	k1gInSc7	úkol
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
zajistit	zajistit	k5eAaPmF	zajistit
uživateli	uživatel	k1gMnSc3	uživatel
možnost	možnost	k1gFnSc4	možnost
ovládat	ovládat	k5eAaImF	ovládat
počítač	počítač	k1gInSc4	počítač
<g/>
,	,	kIx,	,
vytvořit	vytvořit	k5eAaPmF	vytvořit
pro	pro	k7c4	pro
procesy	proces	k1gInPc4	proces
aplikační	aplikační	k2eAgNnSc4d1	aplikační
rozhraní	rozhraní	k1gNnSc4	rozhraní
(	(	kIx(	(
<g/>
API	API	kA	API
<g/>
)	)	kIx)	)
a	a	k8xC	a
přidělovat	přidělovat	k5eAaImF	přidělovat
jim	on	k3xPp3gMnPc3	on
systémové	systémový	k2eAgInPc1d1	systémový
zdroje	zdroj	k1gInPc1	zdroj
(	(	kIx(	(
<g/>
procesor	procesor	k1gInSc1	procesor
<g/>
,	,	kIx,	,
paměť	paměť	k1gFnSc1	paměť
<g/>
,	,	kIx,	,
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
souborům	soubor	k1gInPc3	soubor
atd	atd	kA	atd
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vývoj	vývoj	k1gInSc1	vývoj
je	být	k5eAaImIp3nS	být
náročný	náročný	k2eAgInSc1d1	náročný
na	na	k7c4	na
bezchybnost	bezchybnost	k1gFnSc4	bezchybnost
a	a	k8xC	a
efektivitu	efektivita	k1gFnSc4	efektivita
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
"	"	kIx"	"
<g/>
počítač	počítač	k1gInSc1	počítač
<g/>
"	"	kIx"	"
a	a	k8xC	a
provádějí	provádět	k5eAaImIp3nP	provádět
různé	různý	k2eAgInPc4d1	různý
úkoly	úkol	k1gInPc4	úkol
najednou	najednou	k6eAd1	najednou
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
mobilní	mobilní	k2eAgInSc4d1	mobilní
telefon	telefon	k1gInSc4	telefon
<g/>
,	,	kIx,	,
herní	herní	k2eAgFnSc3d1	herní
konzole	konzola	k1gFnSc3	konzola
<g/>
,	,	kIx,	,
server	server	k1gInSc1	server
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
zástupce	zástupce	k1gMnPc4	zástupce
patří	patřit	k5eAaImIp3nS	patřit
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
Linux	Linux	kA	Linux
<g/>
,	,	kIx,	,
macOS	macOS	k?	macOS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInPc4	první
počítače	počítač	k1gInPc4	počítač
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
neměly	mít	k5eNaImAgFnP	mít
<g/>
.	.	kIx.	.
</s>
<s>
Zárodky	zárodek	k1gInPc1	zárodek
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
lze	lze	k6eAd1	lze
vysledovat	vysledovat	k5eAaImF	vysledovat
v	v	k7c6	v
knihovnách	knihovna	k1gFnPc6	knihovna
pro	pro	k7c4	pro
obsluhu	obsluha	k1gFnSc4	obsluha
vstupních	vstupní	k2eAgFnPc2d1	vstupní
a	a	k8xC	a
výstupních	výstupní	k2eAgFnPc2d1	výstupní
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
výrobci	výrobce	k1gMnPc1	výrobce
počítačů	počítač	k1gInPc2	počítač
dodávali	dodávat	k5eAaImAgMnP	dodávat
propracované	propracovaný	k2eAgInPc4d1	propracovaný
nástroje	nástroj	k1gInPc4	nástroj
pro	pro	k7c4	pro
řízení	řízení	k1gNnSc4	řízení
dávkového	dávkový	k2eAgNnSc2d1	dávkové
zpracování	zpracování	k1gNnSc2	zpracování
spouštěných	spouštěný	k2eAgInPc2d1	spouštěný
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
operační	operační	k2eAgInPc1d1	operační
systémy	systém	k1gInPc1	systém
byly	být	k5eAaImAgInP	být
dodávány	dodávat	k5eAaImNgInP	dodávat
k	k	k7c3	k
sálovým	sálový	k2eAgMnPc3d1	sálový
počítačům	počítač	k1gMnPc3	počítač
(	(	kIx(	(
<g/>
mainframe	mainframe	k1gInSc1	mainframe
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
byl	být	k5eAaImAgInS	být
firmou	firma	k1gFnSc7	firma
IBM	IBM	kA	IBM
vydán	vydat	k5eAaPmNgInS	vydat
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
MFT	MFT	kA	MFT
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
podporoval	podporovat	k5eAaImAgInS	podporovat
v	v	k7c6	v
omezené	omezený	k2eAgFnSc6d1	omezená
míře	míra	k1gFnSc6	míra
multitasking	multitasking	k1gInSc4	multitasking
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
Multics	Multicsa	k1gFnPc2	Multicsa
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
myšlenky	myšlenka	k1gFnSc2	myšlenka
dodávky	dodávka	k1gFnSc2	dodávka
výpočetního	výpočetní	k2eAgInSc2d1	výpočetní
výkonu	výkon	k1gInSc2	výkon
podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
realizovány	realizovat	k5eAaBmNgFnP	realizovat
dodávky	dodávka	k1gFnPc1	dodávka
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
,	,	kIx,	,
plynu	plyn	k1gInSc2	plyn
nebo	nebo	k8xC	nebo
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Multics	Multics	k1gInSc1	Multics
přinesl	přinést	k5eAaPmAgInS	přinést
řadu	řada	k1gFnSc4	řada
nových	nový	k2eAgFnPc2d1	nová
myšlenek	myšlenka	k1gFnPc2	myšlenka
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
inspirací	inspirace	k1gFnPc2	inspirace
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
Unix	Unix	k1gInSc1	Unix
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
stal	stát	k5eAaPmAgMnS	stát
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
pozdější	pozdní	k2eAgInPc4d2	pozdější
systémy	systém	k1gInPc4	systém
(	(	kIx(	(
<g/>
CP	CP	kA	CP
<g/>
/	/	kIx~	/
<g/>
M	M	kA	M
<g/>
,	,	kIx,	,
DOS	DOS	kA	DOS
<g/>
,	,	kIx,	,
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
macOS	macOS	k?	macOS
<g/>
,	,	kIx,	,
Linux	Linux	kA	Linux
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
==	==	k?	==
</s>
</p>
<p>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
univerzální	univerzální	k2eAgInSc4d1	univerzální
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
splnit	splnit	k5eAaPmF	splnit
všechny	všechen	k3xTgInPc4	všechen
požadavky	požadavek	k1gInPc4	požadavek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
požadavků	požadavek	k1gInPc2	požadavek
je	být	k5eAaImIp3nS	být
protichůdných	protichůdný	k2eAgFnPc2d1	protichůdná
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgNnPc2	některý
zařízení	zařízení	k1gNnPc2	zařízení
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
nežádoucí	žádoucí	k2eNgInSc4d1	nežádoucí
(	(	kIx(	(
<g/>
např.	např.	kA	např.
řízení	řízení	k1gNnSc1	řízení
kávovaru	kávovar	k1gInSc2	kávovar
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
vestavěné	vestavěný	k2eAgInPc1d1	vestavěný
systémy	systém	k1gInPc1	systém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Desktop	desktop	k1gInSc1	desktop
===	===	k?	===
</s>
</p>
<p>
<s>
Desktop	desktop	k1gInSc1	desktop
je	být	k5eAaImIp3nS	být
počítač	počítač	k1gInSc1	počítač
sloužící	sloužící	k2eAgInSc1d1	sloužící
uživateli	uživatel	k1gMnPc7	uživatel
na	na	k7c6	na
stole	stol	k1gInSc6	stol
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
osobní	osobní	k2eAgInPc4d1	osobní
počítače	počítač	k1gInPc4	počítač
(	(	kIx(	(
<g/>
PC	PC	kA	PC
<g/>
,	,	kIx,	,
Mac	Mac	kA	Mac
Pro	pro	k7c4	pro
<g/>
,	,	kIx,	,
notebooky	notebook	k1gInPc4	notebook
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
nabídnout	nabídnout	k5eAaPmF	nabídnout
jednoduchost	jednoduchost	k1gFnSc4	jednoduchost
<g/>
,	,	kIx,	,
univerzálnost	univerzálnost	k1gFnSc4	univerzálnost
<g/>
,	,	kIx,	,
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
aplikacemi	aplikace	k1gFnPc7	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
operační	operační	k2eAgInPc4d1	operační
systémy	systém	k1gInPc4	systém
pro	pro	k7c4	pro
desktopové	desktopový	k2eAgInPc4d1	desktopový
počítače	počítač	k1gInPc4	počítač
patří	patřit	k5eAaImIp3nS	patřit
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
MacOS	MacOS	k1gFnPc1	MacOS
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Apple	Apple	kA	Apple
<g/>
,	,	kIx,	,
Linux	Linux	kA	Linux
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Mobilní	mobilní	k2eAgNnPc1d1	mobilní
zařízení	zařízení	k1gNnPc1	zařízení
===	===	k?	===
</s>
</p>
<p>
<s>
Mobilní	mobilní	k2eAgNnPc1d1	mobilní
zařízení	zařízení	k1gNnPc1	zařízení
jsou	být	k5eAaImIp3nP	být
konstruována	konstruován	k2eAgNnPc1d1	konstruováno
jako	jako	k8xC	jako
přenosné	přenosný	k2eAgInPc4d1	přenosný
počítače	počítač	k1gInPc4	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
systému	systém	k1gInSc2	systém
pro	pro	k7c4	pro
mobilní	mobilní	k2eAgNnSc4d1	mobilní
zařízení	zařízení	k1gNnSc4	zařízení
je	být	k5eAaImIp3nS	být
snadnost	snadnost	k1gFnSc1	snadnost
obsluhy	obsluha	k1gFnSc2	obsluha
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
(	(	kIx(	(
<g/>
z	z	k7c2	z
ruky	ruka	k1gFnSc2	ruka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
úspora	úspora	k1gFnSc1	úspora
energie	energie	k1gFnSc2	energie
akumulátoru	akumulátor	k1gInSc2	akumulátor
<g/>
,	,	kIx,	,
univerzálnost	univerzálnost	k1gFnSc1	univerzálnost
<g/>
:	:	kIx,	:
telefonování	telefonování	k1gNnSc1	telefonování
<g/>
,	,	kIx,	,
e-mail	eail	k1gInSc1	e-mail
<g/>
,	,	kIx,	,
Internet	Internet	k1gInSc1	Internet
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnPc1d1	sociální
sítě	síť	k1gFnPc1	síť
<g/>
,	,	kIx,	,
specializované	specializovaný	k2eAgFnPc1d1	specializovaná
aplikace	aplikace	k1gFnPc1	aplikace
(	(	kIx(	(
<g/>
bankovní	bankovní	k2eAgFnSc1d1	bankovní
<g/>
,	,	kIx,	,
navigační	navigační	k2eAgFnSc1d1	navigační
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
krádeže	krádež	k1gFnSc2	krádež
<g/>
,	,	kIx,	,
útoku	útok	k1gInSc2	útok
<g/>
)	)	kIx)	)
atd.	atd.	kA	atd.
Mobilních	mobilní	k2eAgNnPc2d1	mobilní
zařízení	zařízení	k1gNnPc2	zařízení
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
světě	svět	k1gInSc6	svět
už	už	k6eAd1	už
více	hodně	k6eAd2	hodně
než	než	k8xS	než
klasických	klasický	k2eAgInPc2d1	klasický
desktopových	desktopový	k2eAgInPc2d1	desktopový
počítačů	počítač	k1gInPc2	počítač
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
důležitost	důležitost	k1gFnSc4	důležitost
systémů	systém	k1gInPc2	systém
pro	pro	k7c4	pro
takové	takový	k3xDgInPc4	takový
počítače	počítač	k1gInPc4	počítač
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yRgInPc4	který
patří	patřit	k5eAaImIp3nS	patřit
Android	android	k1gInSc1	android
a	a	k8xC	a
iOS	iOS	k?	iOS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Servery	server	k1gInPc1	server
===	===	k?	===
</s>
</p>
<p>
<s>
Servery	server	k1gInPc1	server
obsluhují	obsluhovat	k5eAaImIp3nP	obsluhovat
uživatele	uživatel	k1gMnSc4	uživatel
Internetu	Internet	k1gInSc2	Internet
a	a	k8xC	a
provádějí	provádět	k5eAaImIp3nP	provádět
složité	složitý	k2eAgInPc4d1	složitý
výpočty	výpočet	k1gInPc4	výpočet
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
nabídnout	nabídnout	k5eAaPmF	nabídnout
vysoký	vysoký	k2eAgInSc4d1	vysoký
výpočetní	výpočetní	k2eAgInSc4d1	výpočetní
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
snadnou	snadný	k2eAgFnSc4d1	snadná
údržbu	údržba	k1gFnSc4	údržba
<g/>
,	,	kIx,	,
odolnost	odolnost	k1gFnSc1	odolnost
proti	proti	k7c3	proti
počítačovým	počítačový	k2eAgInPc3d1	počítačový
útokům	útok	k1gInPc3	útok
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
úpravy	úprava	k1gFnSc2	úprava
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
škálování	škálování	k1gNnSc2	škálování
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejpoužívanější	používaný	k2eAgInPc4d3	nejpoužívanější
systémy	systém	k1gInPc4	systém
patří	patřit	k5eAaImIp3nP	patřit
Linux	linux	k1gInSc4	linux
a	a	k8xC	a
servery	server	k1gInPc4	server
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
Windows	Windows	kA	Windows
NT	NT	kA	NT
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Řízení	řízení	k1gNnSc1	řízení
v	v	k7c6	v
reálném	reálný	k2eAgInSc6d1	reálný
čase	čas	k1gInSc6	čas
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
řízení	řízení	k1gNnSc4	řízení
chodu	chod	k1gInSc2	chod
některých	některý	k3yIgInPc2	některý
strojů	stroj	k1gInPc2	stroj
(	(	kIx(	(
<g/>
např.	např.	kA	např.
motor	motor	k1gInSc1	motor
automobilu	automobil	k1gInSc2	automobil
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
používány	používán	k2eAgInPc4d1	používán
počítače	počítač	k1gInPc4	počítač
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
co	co	k9	co
nejrychlejší	rychlý	k2eAgFnSc1d3	nejrychlejší
reakce	reakce	k1gFnSc1	reakce
na	na	k7c6	na
probíhající	probíhající	k2eAgFnSc6d1	probíhající
události	událost	k1gFnSc6	událost
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
ně	on	k3xPp3gInPc4	on
vyvíjeny	vyvíjen	k2eAgInPc4d1	vyvíjen
speciální	speciální	k2eAgInPc4d1	speciální
operační	operační	k2eAgInPc4d1	operační
systémy	systém	k1gInPc4	systém
reálného	reálný	k2eAgInSc2d1	reálný
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
patřit	patřit	k5eAaImF	patřit
i	i	k9	i
systémy	systém	k1gInPc1	systém
řídící	řídící	k2eAgFnSc4d1	řídící
virtuální	virtuální	k2eAgFnSc4d1	virtuální
realitu	realita	k1gFnSc4	realita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
počítačové	počítačový	k2eAgInPc4d1	počítačový
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgMnPc4	který
jsou	být	k5eAaImIp3nP	být
kladeny	klást	k5eAaImNgInP	klást
vysoké	vysoký	k2eAgInPc1d1	vysoký
bezpečnostní	bezpečnostní	k2eAgInPc1d1	bezpečnostní
požadavky	požadavek	k1gInPc1	požadavek
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
speciální	speciální	k2eAgInPc4d1	speciální
operační	operační	k2eAgInPc4d1	operační
systémy	systém	k1gInPc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
jsou	být	k5eAaImIp3nP	být
založeny	založit	k5eAaPmNgFnP	založit
na	na	k7c6	na
mikrojádře	mikrojádra	k1gFnSc6	mikrojádra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Funkce	funkce	k1gFnPc4	funkce
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
==	==	k?	==
</s>
</p>
<p>
<s>
Operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
plní	plnit	k5eAaImIp3nS	plnit
tři	tři	k4xCgFnPc4	tři
základní	základní	k2eAgFnPc4d1	základní
funkce	funkce	k1gFnPc4	funkce
<g/>
:	:	kIx,	:
ovládání	ovládání	k1gNnSc4	ovládání
počítače	počítač	k1gInSc2	počítač
<g/>
,	,	kIx,	,
abstrakce	abstrakce	k1gFnSc1	abstrakce
hardware	hardware	k1gInSc1	hardware
a	a	k8xC	a
správa	správa	k1gFnSc1	správa
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Ovládání	ovládání	k1gNnSc1	ovládání
počítače	počítač	k1gInSc2	počítač
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
definici	definice	k1gFnSc6	definice
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
omezuje	omezovat	k5eAaImIp3nS	omezovat
ovládání	ovládání	k1gNnSc1	ovládání
počítače	počítač	k1gInSc2	počítač
na	na	k7c4	na
schopnost	schopnost	k1gFnSc4	schopnost
spustit	spustit	k5eAaPmF	spustit
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
předat	předat	k5eAaPmF	předat
mu	on	k3xPp3gMnSc3	on
vstupní	vstupní	k2eAgNnSc1d1	vstupní
data	datum	k1gNnPc4	datum
a	a	k8xC	a
umožnit	umožnit	k5eAaPmF	umožnit
výstup	výstup	k1gInSc4	výstup
výsledků	výsledek	k1gInPc2	výsledek
na	na	k7c4	na
výstupní	výstupní	k2eAgNnSc4d1	výstupní
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
však	však	k9	však
pojem	pojem	k1gInSc1	pojem
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
i	i	k9	i
na	na	k7c4	na
grafické	grafický	k2eAgNnSc4d1	grafické
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
marketingových	marketingový	k2eAgInPc2d1	marketingový
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
problému	problém	k1gInSc3	problém
nejasné	jasný	k2eNgFnSc2d1	nejasná
hranice	hranice	k1gFnSc2	hranice
mezi	mezi	k7c7	mezi
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
a	a	k8xC	a
aplikacemi	aplikace	k1gFnPc7	aplikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
disponují	disponovat	k5eAaBmIp3nP	disponovat
jediným	jediný	k2eAgNnSc7d1	jediné
grafickým	grafický	k2eAgNnSc7d1	grafické
rozhraním	rozhraní	k1gNnSc7	rozhraní
(	(	kIx(	(
<g/>
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
Symbian	Symbian	k1gInSc1	Symbian
OS	OS	kA	OS
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
grafické	grafický	k2eAgNnSc1d1	grafické
rozhraní	rozhraní	k1gNnSc1	rozhraní
zahrnováno	zahrnovat	k5eAaImNgNnS	zahrnovat
do	do	k7c2	do
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
uživatelské	uživatelský	k2eAgNnSc1d1	Uživatelské
rozhraní	rozhraní	k1gNnSc1	rozhraní
možné	možný	k2eAgNnSc1d1	možné
vytvořit	vytvořit	k5eAaPmF	vytvořit
několika	několik	k4yIc7	několik
nezávislými	závislý	k2eNgInPc7d1	nezávislý
způsoby	způsob	k1gInPc7	způsob
nebo	nebo	k8xC	nebo
různými	různý	k2eAgFnPc7d1	různá
aplikacemi	aplikace	k1gFnPc7	aplikace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc1d1	běžné
nepovažovat	považovat	k5eNaImF	považovat
ho	on	k3xPp3gNnSc4	on
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
unixové	unixový	k2eAgInPc1d1	unixový
systémy	systém	k1gInPc1	systém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Abstrakce	abstrakce	k1gFnSc1	abstrakce
hardware	hardware	k1gInSc1	hardware
===	===	k?	===
</s>
</p>
<p>
<s>
Operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
skrývá	skrývat	k5eAaImIp3nS	skrývat
detaily	detail	k1gInPc1	detail
ovládání	ovládání	k1gNnSc2	ovládání
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
zařízení	zařízení	k1gNnPc2	zařízení
v	v	k7c6	v
počítači	počítač	k1gInSc6	počítač
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
hardware	hardware	k1gInSc1	hardware
<g/>
)	)	kIx)	)
a	a	k8xC	a
definuje	definovat	k5eAaBmIp3nS	definovat
standardní	standardní	k2eAgNnSc4d1	standardní
rozhraní	rozhraní	k1gNnSc4	rozhraní
pro	pro	k7c4	pro
volání	volání	k1gNnSc4	volání
systémových	systémový	k2eAgFnPc2d1	systémová
služeb	služba	k1gFnPc2	služba
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
abstraktní	abstraktní	k2eAgFnSc4d1	abstraktní
vrstvu	vrstva	k1gFnSc4	vrstva
s	s	k7c7	s
jednoduchými	jednoduchý	k2eAgFnPc7d1	jednoduchá
funkcemi	funkce	k1gFnPc7	funkce
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
API	API	kA	API
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
využívají	využívat	k5eAaPmIp3nP	využívat
programátoři	programátor	k1gMnPc1	programátor
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
nejen	nejen	k6eAd1	nejen
zjednodušuje	zjednodušovat	k5eAaImIp3nS	zjednodušovat
programátorům	programátor	k1gMnPc3	programátor
vytváření	vytváření	k1gNnSc4	vytváření
programů	program	k1gInPc2	program
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
programům	program	k1gInPc3	program
pracovat	pracovat	k5eAaImF	pracovat
i	i	k9	i
se	s	k7c7	s
zařízeními	zařízení	k1gNnPc7	zařízení
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
programu	program	k1gInSc2	program
neexistovaly	existovat	k5eNaImAgFnP	existovat
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
programátora	programátor	k1gMnSc2	programátor
není	být	k5eNaImIp3nS	být
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
otevřením	otevření	k1gNnSc7	otevření
souboru	soubor	k1gInSc2	soubor
na	na	k7c6	na
pevném	pevný	k2eAgInSc6d1	pevný
disku	disk	k1gInSc6	disk
<g/>
,	,	kIx,	,
CD	CD	kA	CD
<g/>
,	,	kIx,	,
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
flash	flash	k1gInSc1	flash
<g/>
,	,	kIx,	,
síťovém	síťový	k2eAgInSc6d1	síťový
disku	disk	k1gInSc6	disk
nebo	nebo	k8xC	nebo
Blu-ray	Bluaa	k1gFnSc2	Blu-raa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
uvnitř	uvnitř	k7c2	uvnitř
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
vytvářena	vytvářit	k5eAaPmNgFnS	vytvářit
podobná	podobný	k2eAgFnSc1d1	podobná
abstraktní	abstraktní	k2eAgFnSc1d1	abstraktní
mezivrstva	mezivrstva	k1gFnSc1	mezivrstva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
programování	programování	k1gNnSc4	programování
ovladačů	ovladač	k1gInPc2	ovladač
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
zařízení	zařízení	k1gNnPc2	zařízení
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
HAL	hala	k1gFnPc2	hala
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Hardware	hardware	k1gInSc1	hardware
Abstraction	Abstraction	k1gInSc1	Abstraction
Layer	Layer	k1gInSc1	Layer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Správa	správa	k1gFnSc1	správa
prostředků	prostředek	k1gInPc2	prostředek
===	===	k?	===
</s>
</p>
<p>
<s>
Operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
přiděluje	přidělovat	k5eAaImIp3nS	přidělovat
spuštěným	spuštěný	k2eAgInPc3d1	spuštěný
procesům	proces	k1gInPc3	proces
systémové	systémový	k2eAgInPc1d1	systémový
prostředky	prostředek	k1gInPc7	prostředek
(	(	kIx(	(
<g/>
operační	operační	k2eAgFnSc4d1	operační
paměť	paměť	k1gFnSc4	paměť
<g/>
,	,	kIx,	,
procesor	procesor	k1gInSc4	procesor
<g/>
,	,	kIx,	,
pevný	pevný	k2eAgInSc4d1	pevný
disk	disk	k1gInSc4	disk
<g/>
,	,	kIx,	,
vstupně-výstupní	vstupněýstupní	k2eAgNnPc1d1	vstupně-výstupní
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
může	moct	k5eAaImIp3nS	moct
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
procesům	proces	k1gInPc3	proces
přidělené	přidělený	k2eAgMnPc4d1	přidělený
prostředky	prostředek	k1gInPc4	prostředek
násilně	násilně	k6eAd1	násilně
odebrat	odebrat	k5eAaPmF	odebrat
(	(	kIx(	(
<g/>
preempce	preempce	k1gFnSc1	preempce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
využívá	využívat	k5eAaImIp3nS	využívat
schopnosti	schopnost	k1gFnSc2	schopnost
procesoru	procesor	k1gInSc2	procesor
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
sebe	sebe	k3xPyFc4	sebe
samého	samý	k3xTgNnSc2	samý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
k	k	k7c3	k
oddělení	oddělení	k1gNnSc3	oddělení
pracovního	pracovní	k2eAgInSc2d1	pracovní
prostoru	prostor	k1gInSc2	prostor
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
procesů	proces	k1gInPc2	proces
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
operačních	operační	k2eAgInPc2d1	operační
systému	systém	k1gInSc2	systém
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
Unix	Unix	k1gInSc1	Unix
a	a	k8xC	a
systémy	systém	k1gInPc1	systém
založené	založený	k2eAgInPc1d1	založený
na	na	k7c4	na
Unix	Unix	k1gInSc4	Unix
===	===	k?	===
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
na	na	k7c4	na
<g/>
:	:	kIx,	:
Unix	Unix	k1gInSc4	Unix
</s>
</p>
<p>
<s>
Unix	Unix	k1gInSc1	Unix
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
napsán	napsat	k5eAaPmNgInS	napsat
v	v	k7c6	v
nízko	nízko	k6eAd1	nízko
úrovňovém	úrovňový	k2eAgInSc6d1	úrovňový
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Ken	Ken	k?	Ken
Thompson	Thompson	k1gNnSc1	Thompson
napsal	napsat	k5eAaBmAgInS	napsat
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
B	B	kA	B
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
především	především	k9	především
na	na	k7c6	na
BCPL	BCPL	kA	BCPL
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
svých	svůj	k3xOyFgFnPc2	svůj
zkušeností	zkušenost	k1gFnPc2	zkušenost
s	s	k7c7	s
projektem	projekt	k1gInSc7	projekt
MULTICS	MULTICS	kA	MULTICS
<g/>
.	.	kIx.	.
</s>
<s>
Programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
B	B	kA	B
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
programovacím	programovací	k2eAgInSc7d1	programovací
jazykem	jazyk	k1gInSc7	jazyk
C	C	kA	C
a	a	k8xC	a
Unixem	Unix	k1gInSc7	Unix
<g/>
,	,	kIx,	,
přepsaný	přepsaný	k2eAgInSc1d1	přepsaný
v	v	k7c6	v
C	C	kA	C
<g/>
,	,	kIx,	,
<g/>
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
vzájemně	vzájemně	k6eAd1	vzájemně
propojených	propojený	k2eAgInPc2d1	propojený
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
moderní	moderní	k2eAgInSc4d1	moderní
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rodina	rodina	k1gFnSc1	rodina
Unix	Unix	k1gInSc1	Unix
je	být	k5eAaImIp3nS	být
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
skupina	skupina	k1gFnSc1	skupina
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
s	s	k7c7	s
několika	několik	k4yIc7	několik
hlavními	hlavní	k2eAgFnPc7d1	hlavní
podskupinami	podskupina	k1gFnPc7	podskupina
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
systémem	systém	k1gInSc7	systém
Unix	Unix	k1gInSc4	Unix
V	V	kA	V
<g/>
,	,	kIx,	,
BSD	BSD	kA	BSD
či	či	k8xC	či
Linuxem	linux	k1gInSc7	linux
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
UNIX	UNIX	kA	UNIX
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
vlajková	vlajkový	k2eAgFnSc1d1	vlajková
loď	loď	k1gFnSc1	loď
společnosti	společnost	k1gFnSc2	společnost
The	The	k1gMnSc1	The
Open	Open	k1gMnSc1	Open
Group	Group	k1gMnSc1	Group
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
licencuje	licencovat	k5eAaBmIp3nS	licencovat
použití	použití	k1gNnSc3	použití
v	v	k7c6	v
jakémkoliv	jakýkoliv	k3yIgInSc6	jakýkoliv
operačním	operační	k2eAgInSc6d1	operační
systému	systém	k1gInSc6	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
prokázán	prokázat	k5eAaPmNgInS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
jejich	jejich	k3xOp3gFnSc4	jejich
distribuci	distribuce	k1gFnSc4	distribuce
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
UNIX-like	UNIXike	k1gInSc1	UNIX-like
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
velké	velký	k2eAgFnSc2d1	velká
sady	sada	k1gFnSc2	sada
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
původnímu	původní	k2eAgInSc3d1	původní
systému	systém	k1gInSc3	systém
UNIX	UNIX	kA	UNIX
<g/>
.	.	kIx.	.
</s>
<s>
Systémy	systém	k1gInPc1	systém
typu	typ	k1gInSc2	typ
Unix	Unix	k1gInSc1	Unix
běží	běžet	k5eAaImIp3nS	běžet
na	na	k7c6	na
široké	široký	k2eAgFnSc6d1	široká
škále	škála	k1gFnSc6	škála
počítačových	počítačový	k2eAgFnPc2d1	počítačová
architektur	architektura	k1gFnPc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
používaná	používaný	k2eAgFnSc1d1	používaná
pro	pro	k7c4	pro
servery	server	k1gInPc4	server
při	při	k7c6	při
podnikání	podnikání	k1gNnSc6	podnikání
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
počítače	počítač	k1gInPc4	počítač
v	v	k7c6	v
akademickém	akademický	k2eAgMnSc6d1	akademický
a	a	k8xC	a
technickém	technický	k2eAgNnSc6d1	technické
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Bezplatné	bezplatný	k2eAgFnPc1d1	bezplatná
varianty	varianta	k1gFnPc1	varianta
systému	systém	k1gInSc2	systém
UNIX	UNIX	kA	UNIX
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Linux	Linux	kA	Linux
a	a	k8xC	a
BSD	BSD	kA	BSD
jsou	být	k5eAaImIp3nP	být
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
poměrně	poměrně	k6eAd1	poměrně
hodně	hodně	k6eAd1	hodně
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čtyři	čtyři	k4xCgInPc1	čtyři
z	z	k7c2	z
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
jsou	být	k5eAaImIp3nP	být
certifikovány	certifikovat	k5eAaImNgInP	certifikovat
společností	společnost	k1gFnSc7	společnost
Open	Open	k1gMnSc1	Open
Group	Group	k1gMnSc1	Group
jako	jako	k8xS	jako
Unix	Unix	k1gInSc1	Unix
<g/>
.	.	kIx.	.
</s>
<s>
Systémy	systém	k1gInPc1	systém
firem	firma	k1gFnPc2	firma
HP	HP	kA	HP
HP-UX	HP-UX	k1gFnSc2	HP-UX
a	a	k8xC	a
IBM	IBM	kA	IBM
AIX	AIX	kA	AIX
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
původního	původní	k2eAgMnSc2d1	původní
Unix	Unix	k1gInSc4	Unix
V	V	kA	V
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
navrženy	navrhnout	k5eAaPmNgInP	navrhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
fungovaly	fungovat	k5eAaImAgFnP	fungovat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
hardwaru	hardware	k1gInSc6	hardware
jejich	jejich	k3xOp3gMnSc2	jejich
dodavatele	dodavatel	k1gMnSc2	dodavatel
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
Solaris	Solaris	k1gInSc1	Solaris
společnosti	společnost	k1gFnSc2	společnost
Sun	Sun	kA	Sun
Microsystems	Microsystems	k1gInSc1	Microsystems
může	moct	k5eAaImIp3nS	moct
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
typech	typ	k1gInPc6	typ
hardwaru	hardware	k1gInSc2	hardware
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
serverů	server	k1gInPc2	server
x	x	k?	x
<g/>
86	[number]	k4	86
a	a	k8xC	a
Sparc	Sparc	k1gInSc1	Sparc
<g/>
,	,	kIx,	,
a	a	k8xC	a
počítačů	počítač	k1gMnPc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
varianta	varianta	k1gFnSc1	varianta
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
Apple	Apple	kA	Apple
MacOS	MacOS	k1gMnSc1	MacOS
<g/>
,	,	kIx,	,
nahrazující	nahrazující	k2eAgMnSc1d1	nahrazující
starší	starší	k1gMnSc1	starší
Apple	Apple	kA	Apple
(	(	kIx(	(
<g/>
non-Unix	non-Unix	k1gInSc1	non-Unix
<g/>
)	)	kIx)	)
Mac	Mac	kA	Mac
OS	OS	kA	OS
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hybridní	hybridní	k2eAgNnSc1d1	hybridní
jádro	jádro	k1gNnSc1	jádro
<g/>
,	,	kIx,	,
založené	založený	k2eAgNnSc1d1	založené
na	na	k7c6	na
BSD	BSD	kA	BSD
s	s	k7c7	s
variantou	varianta	k1gFnSc7	varianta
odvozenou	odvozený	k2eAgFnSc7d1	odvozená
z	z	k7c2	z
NeXTSTEP	NeXTSTEP	k1gFnSc2	NeXTSTEP
<g/>
,	,	kIx,	,
Mach	macha	k1gFnPc2	macha
a	a	k8xC	a
FreeBSD	FreeBSD	k1gFnPc2	FreeBSD
<g/>
.	.	kIx.	.
</s>
<s>
Unixová	unixový	k2eAgFnSc1d1	unixová
interoperabilita	interoperabilita	k1gFnSc1	interoperabilita
byla	být	k5eAaImAgFnS	být
vyhledávána	vyhledávat	k5eAaImNgFnS	vyhledávat
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
zavedení	zavedení	k1gNnSc6	zavedení
standardu	standard	k1gInSc2	standard
POSIX	POSIX	kA	POSIX
<g/>
.	.	kIx.	.
</s>
<s>
Standard	standard	k1gInSc1	standard
POSIX	POSIX	kA	POSIX
lze	lze	k6eAd1	lze
aplikovat	aplikovat	k5eAaBmF	aplikovat
na	na	k7c4	na
libovolný	libovolný	k2eAgInSc4d1	libovolný
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
varianty	varianta	k1gFnPc4	varianta
Unixu	Unix	k1gInSc2	Unix
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
BSD	BSD	kA	BSD
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
distribuce	distribuce	k1gFnSc1	distribuce
===	===	k?	===
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
na	na	k7c6	na
<g/>
:	:	kIx,	:
BSD	BSD	kA	BSD
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
server	server	k1gInSc1	server
pro	pro	k7c4	pro
World	World	k1gInSc4	World
Wide	Wid	k1gInSc2	Wid
Web	web	k1gInSc1	web
běžel	běžet	k5eAaImAgMnS	běžet
na	na	k7c6	na
platformě	platforma	k1gFnSc6	platforma
NeXTSTEP	NeXTSTEP	k1gFnPc4	NeXTSTEP
založené	založený	k2eAgFnPc4d1	založená
na	na	k7c6	na
BSD	BSD	kA	BSD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podskupina	podskupina	k1gFnSc1	podskupina
Unix	Unix	k1gInSc1	Unix
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
Berkeley	Berkeley	k1gInPc7	Berkeley
Software	software	k1gInSc1	software
Distribution	Distribution	k1gInSc1	Distribution
(	(	kIx(	(
<g/>
BSD	BSD	kA	BSD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
FreeBSD	FreeBSD	k1gMnSc3	FreeBSD
<g/>
,	,	kIx,	,
NetBSD	NetBSD	k1gMnSc3	NetBSD
a	a	k8xC	a
OpenBSD	OpenBSD	k1gMnSc3	OpenBSD
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
operační	operační	k2eAgInPc1d1	operační
systémy	systém	k1gInPc1	systém
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
webových	webový	k2eAgInPc6d1	webový
serverech	server	k1gInPc6	server
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
mohou	moct	k5eAaImIp3nP	moct
fungovat	fungovat	k5eAaImF	fungovat
také	také	k9	také
jako	jako	k9	jako
počítače	počítač	k1gInPc1	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Internet	Internet	k1gInSc1	Internet
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
postaven	postavit	k5eAaPmNgInS	postavit
na	na	k7c6	na
BSD	BSD	kA	BSD
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mnoho	mnoho	k4c4	mnoho
protokolů	protokol	k1gInPc2	protokol
běžně	běžně	k6eAd1	běžně
používaných	používaný	k2eAgInPc2d1	používaný
počítači	počítač	k1gInSc3	počítač
pro	pro	k7c4	pro
připojení	připojení	k1gNnSc4	připojení
<g/>
,	,	kIx,	,
odesílání	odesílání	k1gNnSc4	odesílání
a	a	k8xC	a
přijímání	přijímání	k1gNnSc4	přijímání
dat	datum	k1gNnPc2	datum
přes	přes	k7c4	přes
síť	síť	k1gFnSc4	síť
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
rozvinuto	rozvinout	k5eAaPmNgNnS	rozvinout
v	v	k7c6	v
BSD	BSD	kA	BSD
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
síť	síť	k1gFnSc1	síť
byla	být	k5eAaImAgFnS	být
také	také	k9	také
poprvé	poprvé	k6eAd1	poprvé
demonstrována	demonstrován	k2eAgFnSc1d1	demonstrována
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
počítačů	počítač	k1gMnPc2	počítač
s	s	k7c7	s
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
založeným	založený	k2eAgInSc7d1	založený
na	na	k7c4	na
BSD	BSD	kA	BSD
nazvaném	nazvaný	k2eAgMnSc6d1	nazvaný
NeXTSTEP	NeXTSTEP	k1gMnSc6	NeXTSTEP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
Kalifornská	kalifornský	k2eAgFnSc1d1	kalifornská
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c4	v
Berkeley	Berkelea	k1gFnPc4	Berkelea
nainstalovala	nainstalovat	k5eAaPmAgFnS	nainstalovat
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
Unix	Unix	k1gInSc4	Unix
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
studenti	student	k1gMnPc1	student
a	a	k8xC	a
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
v	v	k7c6	v
oddělení	oddělení	k1gNnSc6	oddělení
informatiky	informatika	k1gFnSc2	informatika
začali	začít	k5eAaPmAgMnP	začít
přidávat	přidávat	k5eAaImF	přidávat
nové	nový	k2eAgInPc4d1	nový
programy	program	k1gInPc4	program
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
usnadňují	usnadňovat	k5eAaImIp3nP	usnadňovat
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
textové	textový	k2eAgInPc1d1	textový
editory	editor	k1gInPc1	editor
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
univerzita	univerzita	k1gFnSc1	univerzita
Berkeley	Berkelea	k1gFnSc2	Berkelea
obdržela	obdržet	k5eAaPmAgFnS	obdržet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
nové	nový	k2eAgInPc4d1	nový
počítače	počítač	k1gInPc4	počítač
VAX	VAX	kA	VAX
s	s	k7c7	s
instalovaným	instalovaný	k2eAgInSc7d1	instalovaný
Unixem	Unix	k1gInSc7	Unix
<g/>
,	,	kIx,	,
vysokoškolští	vysokoškolský	k2eAgMnPc1d1	vysokoškolský
studenti	student	k1gMnPc1	student
modifikovali	modifikovat	k5eAaBmAgMnP	modifikovat
Unix	Unix	k1gInSc4	Unix
ještě	ještě	k6eAd1	ještě
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
využili	využít	k5eAaPmAgMnP	využít
možnosti	možnost	k1gFnPc4	možnost
hardwaru	hardware	k1gInSc2	hardware
počítače	počítač	k1gInPc1	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Agentura	agentura	k1gFnSc1	agentura
pro	pro	k7c4	pro
obranné	obranný	k2eAgInPc4d1	obranný
výzkumy	výzkum	k1gInPc4	výzkum
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
obrany	obrana	k1gFnSc2	obrana
v	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
tento	tento	k3xDgInSc4	tento
projekt	projekt	k1gInSc4	projekt
financovat	financovat	k5eAaBmF	financovat
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
korporací	korporace	k1gFnPc2	korporace
a	a	k8xC	a
vládních	vládní	k2eAgFnPc2d1	vládní
organizací	organizace	k1gFnPc2	organizace
si	se	k3xPyFc3	se
této	tento	k3xDgFnSc2	tento
skutečnosti	skutečnost	k1gFnSc2	skutečnost
všimlo	všimnout	k5eAaPmAgNnS	všimnout
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
používat	používat	k5eAaImF	používat
verzi	verze	k1gFnSc4	verze
Berkeley	Berkelea	k1gFnSc2	Berkelea
Unix	Unix	k1gInSc1	Unix
místo	místo	k7c2	místo
oficiální	oficiální	k2eAgFnSc2d1	oficiální
verze	verze	k1gFnSc2	verze
distribuované	distribuovaný	k2eAgFnSc2d1	distribuovaná
společností	společnost	k1gFnSc7	společnost
AT	AT	kA	AT
<g/>
&	&	k?	&
<g/>
T.	T.	kA	T.
</s>
</p>
<p>
<s>
Steve	Steve	k1gMnSc1	Steve
Jobs	Jobsa	k1gFnPc2	Jobsa
<g/>
,	,	kIx,	,
po	po	k7c4	po
opuštění	opuštění	k1gNnSc4	opuštění
společnosti	společnost	k1gFnSc2	společnost
Apple	Apple	kA	Apple
Inc	Inc	k1gMnSc1	Inc
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
společnost	společnost	k1gFnSc4	společnost
NeXT	NeXT	k1gFnSc2	NeXT
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
špičkové	špičkový	k2eAgInPc4d1	špičkový
počítače	počítač	k1gInPc4	počítač
běžící	běžící	k2eAgInPc4d1	běžící
na	na	k7c4	na
BSD	BSD	kA	BSD
s	s	k7c7	s
názvem	název	k1gInSc7	název
NeXTSTEP	NeXTSTEP	k1gFnSc2	NeXTSTEP
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
počítačů	počítač	k1gInPc2	počítač
používal	používat	k5eAaImAgInS	používat
Tim	Tim	k?	Tim
Berners-Lee	Berners-Lee	k1gFnSc4	Berners-Lee
jako	jako	k8xS	jako
první	první	k4xOgInSc4	první
webový	webový	k2eAgInSc4d1	webový
server	server	k1gInSc4	server
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
World	Worlda	k1gFnPc2	Worlda
Wide	Wid	k1gFnSc2	Wid
Web	web	k1gInSc1	web
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vývojáři	vývojář	k1gMnPc1	vývojář
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Keith	Keith	k1gInSc1	Keith
Bostic	Bostice	k1gFnPc2	Bostice
<g/>
,	,	kIx,	,
posunuli	posunout	k5eAaPmAgMnP	posunout
projekt	projekt	k1gInSc4	projekt
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nahradil	nahradit	k5eAaPmAgMnS	nahradit
non-free	nonre	k1gMnSc4	non-fre
code	cod	k1gMnSc4	cod
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Bell	bell	k1gInSc4	bell
Labs	Labsa	k1gFnPc2	Labsa
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
<g/>
,	,	kIx,	,
AT	AT	kA	AT
<g/>
&	&	k?	&
<g/>
T	T	kA	T
žaloval	žalovat	k5eAaImAgMnS	žalovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
letech	let	k1gInPc6	let
soudních	soudní	k2eAgInPc2d1	soudní
sporů	spor	k1gInPc2	spor
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
projekt	projekt	k1gInSc1	projekt
BSD	BSD	kA	BSD
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
bezplatných	bezplatný	k2eAgNnPc2d1	bezplatné
rozšíření	rozšíření	k1gNnPc2	rozšíření
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
NetBSD	NetBSD	k1gFnSc4	NetBSD
a	a	k8xC	a
FreeBSD	FreeBSD	k1gFnSc4	FreeBSD
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
OpenBSD	OpenBSD	k1gFnSc1	OpenBSD
(	(	kIx(	(
<g/>
od	od	k7c2	od
NetBSD	NetBSD	k1gFnSc2	NetBSD
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
MacOS	MacOS	k1gMnPc5	MacOS
===	===	k?	===
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
na	na	k7c6	na
<g/>
:	:	kIx,	:
MacOS	MacOS	k1gFnSc6	MacOS
</s>
</p>
<p>
<s>
MacOS	MacOS	k?	MacOS
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
"	"	kIx"	"
<g/>
Mac	Mac	kA	Mac
OS	OS	kA	OS
X	X	kA	X
<g/>
"	"	kIx"	"
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
"	"	kIx"	"
<g/>
OS	OS	kA	OS
X	X	kA	X
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
otevřených	otevřený	k2eAgInPc2d1	otevřený
grafických	grafický	k2eAgInPc2d1	grafický
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
vyvinutých	vyvinutý	k2eAgInPc2d1	vyvinutý
firmou	firma	k1gFnSc7	firma
Apple	Apple	kA	Apple
<g/>
,	,	kIx,	,
nejnovější	nový	k2eAgMnSc1d3	Nejnovější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
počítačích	počítač	k1gInPc6	počítač
Macintosh	Macintosh	kA	Macintosh
<g/>
.	.	kIx.	.
</s>
<s>
MacOS	MacOS	k?	MacOS
je	být	k5eAaImIp3nS	být
nástupcem	nástupce	k1gMnSc7	nástupce
původního	původní	k2eAgNnSc2d1	původní
klasického	klasický	k2eAgNnSc2d1	klasické
Mac	Mac	kA	Mac
OS	OS	kA	OS
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
primárním	primární	k2eAgInSc7d1	primární
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
Applu	Appl	k1gInSc2	Appl
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
předchůdce	předchůdce	k1gMnSc2	předchůdce
<g/>
,	,	kIx,	,
MacOS	MacOS	k1gFnSc7	MacOS
je	být	k5eAaImIp3nS	být
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
UNIX	UNIX	kA	UNIX
postaven	postavit	k5eAaPmNgInS	postavit
na	na	k7c6	na
technologii	technologie	k1gFnSc6	technologie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
pro	pro	k7c4	pro
NeXT	NeXT	k1gFnSc4	NeXT
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Apple	Apple	kA	Apple
koupila	koupit	k5eAaPmAgFnS	koupit
firmu	firma	k1gFnSc4	firma
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
jako	jako	k8xS	jako
Mac	Mac	kA	Mac
OS	OS	kA	OS
X	X	kA	X
Server	server	k1gInSc4	server
1.0	[number]	k4	1.0
<g/>
,	,	kIx,	,
následovaný	následovaný	k2eAgInSc1d1	následovaný
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2001	[number]	k4	2001
klientskou	klientský	k2eAgFnSc7d1	klientská
verzí	verze	k1gFnSc7	verze
(	(	kIx(	(
<g/>
Mac	Mac	kA	Mac
OS	OS	kA	OS
X	X	kA	X
v	v	k7c6	v
<g/>
10.0	[number]	k4	10.0
"	"	kIx"	"
<g/>
Cheetah	Cheetah	k1gInSc1	Cheetah
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
šest	šest	k4xCc1	šest
dalších	další	k2eAgFnPc2d1	další
"	"	kIx"	"
<g/>
klientských	klientský	k2eAgFnPc2d1	klientská
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
serverových	serverový	k2eAgFnPc2d1	serverová
<g/>
"	"	kIx"	"
edic	edice	k1gFnPc2	edice
MacOS	MacOS	k1gMnPc2	MacOS
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nebyly	být	k5eNaImAgInP	být
tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
sloučeny	sloučit	k5eAaPmNgInP	sloučit
v	v	k7c6	v
systému	systém	k1gInSc6	systém
OS	OS	kA	OS
X	X	kA	X
10.7	[number]	k4	10.7
"	"	kIx"	"
<g/>
Lion	Lion	k1gMnSc1	Lion
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
spojením	spojení	k1gNnSc7	spojení
s	s	k7c7	s
macOS	macOS	k?	macOS
byla	být	k5eAaImAgFnS	být
serverová	serverový	k2eAgFnSc1d1	serverová
edice	edice	k1gFnSc1	edice
-	-	kIx~	-
macOS	macOS	k?	macOS
Server	server	k1gInSc1	server
-	-	kIx~	-
architektonicky	architektonicky	k6eAd1	architektonicky
identická	identický	k2eAgFnSc1d1	identická
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
uživatelským	uživatelský	k2eAgInSc7d1	uživatelský
protějškem	protějšek	k1gInSc7	protějšek
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
běžela	běžet	k5eAaImAgFnS	běžet
na	na	k7c6	na
počítačích	počítač	k1gInPc6	počítač
Apple	Apple	kA	Apple
Macintosh	Macintosh	kA	Macintosh
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
macOS	macOS	k?	macOS
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
nástroje	nástroj	k1gInPc4	nástroj
pro	pro	k7c4	pro
správu	správa	k1gFnSc4	správa
a	a	k8xC	a
nastavení	nastavení	k1gNnSc4	nastavení
práv	právo	k1gNnPc2	právo
pro	pro	k7c4	pro
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
zjednodušený	zjednodušený	k2eAgInSc4d1	zjednodušený
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3	k
klíčovým	klíčový	k2eAgFnPc3d1	klíčová
síťovým	síťový	k2eAgFnPc3d1	síťová
službám	služba	k1gFnPc3	služba
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
přenosu	přenos	k1gInSc2	přenos
pošty	pošta	k1gFnSc2	pošta
<g/>
,	,	kIx,	,
serveru	server	k1gInSc2	server
Samba	samba	k1gFnSc1	samba
<g/>
,	,	kIx,	,
serveru	server	k1gInSc2	server
LDAP	LDAP	kA	LDAP
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
Mac	Mac	kA	Mac
OS	OS	kA	OS
X	X	kA	X
v	v	k7c6	v
<g/>
10.7	[number]	k4	10.7
Lion	Lion	k1gMnSc1	Lion
byly	být	k5eAaImAgInP	být
všechny	všechen	k3xTgInPc1	všechen
aspekty	aspekt	k1gInPc1	aspekt
serveru	server	k1gInSc2	server
Mac	Mac	kA	Mac
OS	OS	kA	OS
X	X	kA	X
Server	server	k1gInSc4	server
integrovány	integrován	k2eAgInPc4d1	integrován
do	do	k7c2	do
verze	verze	k1gFnSc2	verze
klienta	klient	k1gMnSc2	klient
a	a	k8xC	a
produkt	produkt	k1gInSc1	produkt
byl	být	k5eAaImAgInS	být
přeznačen	přeznačit	k5eAaPmNgInS	přeznačit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
OS	OS	kA	OS
X	X	kA	X
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Serverové	serverový	k2eAgInPc1d1	serverový
nástroje	nástroj	k1gInPc1	nástroj
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
nabízeny	nabízet	k5eAaImNgInP	nabízet
jako	jako	k8xS	jako
aplikace	aplikace	k1gFnPc1	aplikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Linux	Linux	kA	Linux
===	===	k?	===
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
na	na	k7c4	na
<g/>
:	:	kIx,	:
Linux	linux	k1gInSc4	linux
</s>
</p>
<p>
<s>
Linuxové	linuxový	k2eAgNnSc1d1	linuxové
jádro	jádro	k1gNnSc1	jádro
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
jako	jako	k8xS	jako
projekt	projekt	k1gInSc1	projekt
Linuse	Linuse	k1gFnSc2	Linuse
Torvaldse	Torvaldse	k1gFnSc2	Torvaldse
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
studoval	studovat	k5eAaImAgMnS	studovat
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
projektu	projekt	k1gInSc6	projekt
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
v	v	k7c6	v
diskuzní	diskuzní	k2eAgFnSc6d1	diskuzní
skupině	skupina	k1gFnSc6	skupina
pro	pro	k7c4	pro
studenty	student	k1gMnPc4	student
a	a	k8xC	a
programátory	programátor	k1gMnPc4	programátor
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
pomoc	pomoc	k1gFnSc4	pomoc
od	od	k7c2	od
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
vytvořit	vytvořit	k5eAaPmF	vytvořit
kompletní	kompletní	k2eAgNnSc1d1	kompletní
a	a	k8xC	a
funkční	funkční	k2eAgNnSc1d1	funkční
jádro	jádro	k1gNnSc1	jádro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Linux	linux	k1gInSc1	linux
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
Unixu	Unix	k1gInSc6	Unix
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
unixového	unixový	k2eAgInSc2d1	unixový
kódu	kód	k1gInSc2	kód
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
BSD	BSD	kA	BSD
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc2	jeho
variant	varianta	k1gFnPc2	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
otevřenému	otevřený	k2eAgInSc3d1	otevřený
licenčnímu	licenční	k2eAgInSc3d1	licenční
modelu	model	k1gInSc3	model
je	být	k5eAaImIp3nS	být
kód	kód	k1gInSc4	kód
jádra	jádro	k1gNnSc2	jádro
Linuxu	linux	k1gInSc2	linux
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
a	a	k8xC	a
modifikaci	modifikace	k1gFnSc4	modifikace
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
použití	použití	k1gNnSc3	použití
na	na	k7c6	na
širokém	široký	k2eAgNnSc6d1	široké
spektru	spektrum	k1gNnSc6	spektrum
zařízení	zařízení	k1gNnPc2	zařízení
od	od	k7c2	od
superpočítačů	superpočítač	k1gInPc2	superpočítač
až	až	k9	až
po	po	k7c4	po
chytré	chytrý	k2eAgFnPc4d1	chytrá
hodinky	hodinka	k1gFnPc4	hodinka
(	(	kIx(	(
<g/>
smart	smart	k1gInSc1	smart
watch	watch	k1gInSc1	watch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
odhady	odhad	k1gInPc1	odhad
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Linux	linux	k1gInSc1	linux
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
1,82	[number]	k4	1,82
<g/>
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
počítačů	počítač	k1gInPc2	počítač
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
široce	široko	k6eAd1	široko
přijat	přijmout	k5eAaPmNgInS	přijmout
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
na	na	k7c6	na
serverech	server	k1gInPc6	server
<g/>
,	,	kIx,	,
na	na	k7c6	na
mobilních	mobilní	k2eAgInPc6d1	mobilní
telefonech	telefon	k1gInPc6	telefon
a	a	k8xC	a
dalších	další	k2eAgNnPc6d1	další
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Linux	linux	k1gInSc1	linux
nahradil	nahradit	k5eAaPmAgInS	nahradit
Unix	Unix	k1gInSc4	Unix
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
platformách	platforma	k1gFnPc6	platforma
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
superpočítačů	superpočítač	k1gInPc2	superpočítač
<g/>
.	.	kIx.	.
</s>
<s>
Linux	linux	k1gInSc1	linux
je	být	k5eAaImIp3nS	být
běžně	běžně	k6eAd1	běžně
používán	používat	k5eAaImNgInS	používat
i	i	k9	i
na	na	k7c6	na
jiných	jiný	k2eAgNnPc6d1	jiné
malých	malý	k2eAgNnPc6d1	malé
energeticky	energeticky	k6eAd1	energeticky
úsporných	úsporný	k2eAgNnPc6d1	úsporné
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
smartphony	smartphona	k1gFnPc4	smartphona
a	a	k8xC	a
chytré	chytrý	k2eAgFnPc4d1	chytrá
hodinky	hodinka	k1gFnPc4	hodinka
(	(	kIx(	(
<g/>
smart	smart	k1gInSc1	smart
watch	watch	k1gInSc1	watch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Linuxové	linuxový	k2eAgNnSc1d1	linuxové
jádro	jádro	k1gNnSc1	jádro
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
populárních	populární	k2eAgFnPc6d1	populární
distribucích	distribuce	k1gFnPc6	distribuce
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Red	Red	k1gFnSc4	Red
Hat	hat	k0	hat
<g/>
,	,	kIx,	,
Debian	Debian	k1gMnSc1	Debian
<g/>
,	,	kIx,	,
Ubuntu	Ubunta	k1gFnSc4	Ubunta
<g/>
,	,	kIx,	,
Linux	linux	k1gInSc4	linux
Mint	Mint	k2eAgInSc4d1	Mint
<g/>
,	,	kIx,	,
Android	android	k1gInSc4	android
a	a	k8xC	a
Chrome	chromat	k5eAaImIp3nS	chromat
OS	OS	kA	OS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
===	===	k?	===
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
na	na	k7c6	na
<g/>
:	:	kIx,	:
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
</s>
</p>
<p>
<s>
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
proprietárních	proprietární	k2eAgInPc2d1	proprietární
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
navržených	navržený	k2eAgFnPc2d1	navržená
společností	společnost	k1gFnPc2	společnost
Microsoft	Microsoft	kA	Microsoft
a	a	k8xC	a
primárně	primárně	k6eAd1	primárně
zaměřených	zaměřený	k2eAgFnPc2d1	zaměřená
na	na	k7c4	na
počítače	počítač	k1gInPc4	počítač
založené	založený	k2eAgInPc4d1	založený
na	na	k7c6	na
architektuře	architektura	k1gFnSc6	architektura
Intel	Intel	kA	Intel
s	s	k7c7	s
odhadovaným	odhadovaný	k2eAgInSc7d1	odhadovaný
podílem	podíl	k1gInSc7	podíl
na	na	k7c6	na
celkovém	celkový	k2eAgInSc6d1	celkový
počtu	počet	k1gInSc6	počet
zařízení	zařízení	k1gNnPc2	zařízení
88,9	[number]	k4	88,9
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgFnSc1d3	nejnovější
verze	verze	k1gFnSc1	verze
Windows	Windows	kA	Windows
je	být	k5eAaImIp3nS	být
Windows	Windows	kA	Windows
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
Windows	Windows	kA	Windows
7	[number]	k4	7
překonal	překonat	k5eAaPmAgMnS	překonat
Windows	Windows	kA	Windows
XP	XP	kA	XP
coby	coby	k?	coby
nejpoužívanější	používaný	k2eAgFnSc6d3	nejpoužívanější
verzi	verze	k1gFnSc6	verze
OS	OS	kA	OS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
jako	jako	k8xC	jako
operační	operační	k2eAgNnSc4d1	operační
prostředí	prostředí	k1gNnSc4	prostředí
běžící	běžící	k2eAgNnSc4d1	běžící
na	na	k7c4	na
MS-DOS	MS-DOS	k1gFnSc4	MS-DOS
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgMnS	být
standardní	standardní	k2eAgInSc4d1	standardní
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
používaný	používaný	k2eAgInSc4d1	používaný
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
osobních	osobní	k2eAgMnPc2d1	osobní
počítačů	počítač	k1gMnPc2	počítač
architektury	architektura	k1gFnSc2	architektura
Intel	Intel	kA	Intel
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
Windows	Windows	kA	Windows
95	[number]	k4	95
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
používal	používat	k5eAaImAgInS	používat
pouze	pouze	k6eAd1	pouze
systém	systém	k1gInSc1	systém
MS-DOS	MS-DOS	k1gFnSc2	MS-DOS
jako	jako	k8xC	jako
bootstrap	bootstrap	k1gInSc1	bootstrap
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
ME	ME	kA	ME
<g/>
,	,	kIx,	,
vydaný	vydaný	k2eAgInSc1d1	vydaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
poslední	poslední	k2eAgFnSc7d1	poslední
distribucí	distribuce	k1gFnSc7	distribuce
používající	používající	k2eAgFnSc4d1	používající
Win	Win	k1gFnSc4	Win
<g/>
9	[number]	k4	9
<g/>
x.	x.	k?	x.
Pozdější	pozdní	k2eAgFnPc1d2	pozdější
verze	verze	k1gFnPc1	verze
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
na	na	k7c6	na
jádře	jádro	k1gNnSc6	jádro
systému	systém	k1gInSc2	systém
Windows	Windows	kA	Windows
NT	NT	kA	NT
<g/>
.	.	kIx.	.
</s>
<s>
Aktuální	aktuální	k2eAgFnPc1d1	aktuální
verze	verze	k1gFnPc1	verze
systému	systém	k1gInSc2	systém
Windows	Windows	kA	Windows
běží	běžet	k5eAaImIp3nS	běžet
na	na	k7c6	na
IA-	IA-	k1gFnSc6	IA-
<g/>
32	[number]	k4	32
<g/>
,	,	kIx,	,
x	x	k?	x
<g/>
86	[number]	k4	86
<g/>
-	-	kIx~	-
<g/>
64	[number]	k4	64
a	a	k8xC	a
32	[number]	k4	32
<g/>
bitových	bitový	k2eAgFnPc2d1	bitová
ARM	ARM	kA	ARM
mikroprocesorech	mikroprocesor	k1gInPc6	mikroprocesor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Serverové	serverový	k2eAgFnPc4d1	serverová
edice	edice	k1gFnPc4	edice
systému	systém	k1gInSc2	systém
Windows	Windows	kA	Windows
jsou	být	k5eAaImIp3nP	být
hodně	hodně	k6eAd1	hodně
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
společnost	společnost	k1gFnSc1	společnost
Microsoft	Microsoft	kA	Microsoft
vynaložila	vynaložit	k5eAaPmAgFnS	vynaložit
hodně	hodně	k6eAd1	hodně
peněz	peníze	k1gInPc2	peníze
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
podpořit	podpořit	k5eAaPmF	podpořit
používání	používání	k1gNnSc3	používání
systému	systém	k1gInSc2	systém
Windows	Windows	kA	Windows
coby	coby	k?	coby
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
pro	pro	k7c4	pro
servery	server	k1gInPc4	server
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
systém	systém	k1gInSc1	systém
Windows	Windows	kA	Windows
snaží	snažit	k5eAaImIp3nS	snažit
konkurovat	konkurovat	k5eAaImF	konkurovat
Linuxu	linux	k1gInSc2	linux
a	a	k8xC	a
BSD	BSD	kA	BSD
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
systému	systém	k1gInSc2	systém
Windows	Windows	kA	Windows
na	na	k7c6	na
serverech	server	k1gInPc6	server
však	však	k9	však
není	být	k5eNaImIp3nS	být
tak	tak	k9	tak
rozšířené	rozšířený	k2eAgFnPc4d1	rozšířená
jako	jako	k9	jako
u	u	k7c2	u
klasických	klasický	k2eAgNnPc2d1	klasické
PC	PC	kA	PC
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ReactOS	ReactOS	k?	ReactOS
je	být	k5eAaImIp3nS	být
alternativní	alternativní	k2eAgInSc4d1	alternativní
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
vyvíjen	vyvíjet	k5eAaImNgInS	vyvíjet
na	na	k7c6	na
principu	princip	k1gInSc6	princip
Windows	Windows	kA	Windows
-	-	kIx~	-
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
kódu	kód	k1gInSc2	kód
společnosti	společnost	k1gFnSc2	společnost
Microsoft	Microsoft	kA	Microsoft
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Jiné	jiný	k2eAgFnPc1d1	jiná
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
mnoho	mnoho	k4c1	mnoho
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nejsou	být	k5eNaImIp3nP	být
tak	tak	k6eAd1	tak
známé	známý	k2eAgFnPc1d1	známá
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
AmigaOS	AmigaOS	k1gFnSc1	AmigaOS
<g/>
,	,	kIx,	,
OS	OS	kA	OS
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
od	od	k7c2	od
firem	firma	k1gFnPc2	firma
IBM	IBM	kA	IBM
a	a	k8xC	a
Microsoft	Microsoft	kA	Microsoft
<g/>
,	,	kIx,	,
klasický	klasický	k2eAgInSc4d1	klasický
Mac	Mac	kA	Mac
OS	OS	kA	OS
<g/>
,	,	kIx,	,
non-Unix	non-Unix	k1gInSc1	non-Unix
odvozený	odvozený	k2eAgInSc1d1	odvozený
od	od	k7c2	od
Apple	Apple	kA	Apple
MacOS	MacOS	k1gFnSc2	MacOS
<g/>
,	,	kIx,	,
BeOS	BeOS	k1gFnSc1	BeOS
<g/>
,	,	kIx,	,
XTS-	XTS-	k1gFnSc1	XTS-
<g/>
300	[number]	k4	300
<g/>
,	,	kIx,	,
RISC	RISC	kA	RISC
OS	OS	kA	OS
<g/>
,	,	kIx,	,
MorphOS	MorphOS	k1gFnSc4	MorphOS
<g/>
,	,	kIx,	,
Haiku	Haika	k1gFnSc4	Haika
<g/>
,	,	kIx,	,
BareMetal	BareMetal	k1gFnSc4	BareMetal
a	a	k8xC	a
FreeMint	FreeMint	k1gInSc4	FreeMint
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
používány	používat	k5eAaImNgInP	používat
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
se	se	k3xPyFc4	se
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
jako	jako	k9	jako
menší	malý	k2eAgFnPc4d2	menší
platformy	platforma	k1gFnPc4	platforma
pro	pro	k7c4	pro
komunity	komunita	k1gFnPc4	komunita
nadšenců	nadšenec	k1gMnPc2	nadšenec
a	a	k8xC	a
pro	pro	k7c4	pro
specializované	specializovaný	k2eAgFnPc4d1	specializovaná
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
OpenVMS	OpenVMS	k?	OpenVMS
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
od	od	k7c2	od
DEC	DEC	kA	DEC
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vyvíjen	vyvíjen	k2eAgInSc1d1	vyvíjen
stále	stále	k6eAd1	stále
aktivní	aktivní	k2eAgFnSc7d1	aktivní
společností	společnost	k1gFnSc7	společnost
Hewlett-Packard	Hewlett-Packarda	k1gFnPc2	Hewlett-Packarda
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
operační	operační	k2eAgInPc1d1	operační
systémy	systém	k1gInPc1	systém
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
téměř	téměř	k6eAd1	téměř
jedině	jedině	k6eAd1	jedině
v	v	k7c6	v
akademické	akademický	k2eAgFnSc6d1	akademická
sféře	sféra	k1gFnSc6	sféra
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
konceptů	koncept	k1gInPc2	koncept
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
splňuje	splňovat	k5eAaImIp3nS	splňovat
obě	dva	k4xCgFnPc4	dva
role	role	k1gFnPc4	role
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
MINIX	MINIX	kA	MINIX
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
například	například	k6eAd1	například
Singularity	singularita	k1gFnPc1	singularita
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
výhradně	výhradně	k6eAd1	výhradně
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc1	systém
Oberon	Oberon	k1gInSc1	Oberon
navržený	navržený	k2eAgInSc1d1	navržený
v	v	k7c6	v
ETH	ETH	kA	ETH
Zürich	Zürich	k1gMnSc1	Zürich
Niklaus	Niklaus	k1gMnSc1	Niklaus
Wirth	Wirth	k1gMnSc1	Wirth
<g/>
,	,	kIx,	,
Jürgem	Jürg	k1gMnSc7	Jürg
Gutknechtem	Gutknecht	k1gMnSc7	Gutknecht
a	a	k8xC	a
skupinou	skupina	k1gFnSc7	skupina
studentů	student	k1gMnPc2	student
bývalého	bývalý	k2eAgMnSc2d1	bývalý
Computer	computer	k1gInSc1	computer
Systems	Systems	k1gInSc1	Systems
Institute	institut	k1gInSc5	institut
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
<g/>
,	,	kIx,	,
výuku	výuka	k1gFnSc4	výuka
a	a	k8xC	a
každodenní	každodenní	k2eAgFnSc4d1	každodenní
práci	práce	k1gFnSc4	práce
ve	v	k7c6	v
Wirthově	Wirthův	k2eAgFnSc6d1	Wirthův
skupině	skupina	k1gFnSc6	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
operační	operační	k2eAgInPc1d1	operační
systémy	systém	k1gInPc1	systém
nedokázaly	dokázat	k5eNaPmAgInP	dokázat
získat	získat	k5eAaPmF	získat
významný	významný	k2eAgInSc4d1	významný
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zavedly	zavést	k5eAaPmAgFnP	zavést
inovace	inovace	k1gFnPc1	inovace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
dnešní	dnešní	k2eAgInPc4d1	dnešní
operační	operační	k2eAgInPc4d1	operační
systémy	systém	k1gInPc4	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Komponenty	komponent	k1gInPc4	komponent
==	==	k?	==
</s>
</p>
<p>
<s>
Komponenty	komponenta	k1gFnPc1	komponenta
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
různé	různý	k2eAgFnPc1d1	různá
části	část	k1gFnPc1	část
počítače	počítač	k1gInSc2	počítač
spolupracovaly	spolupracovat	k5eAaImAgFnP	spolupracovat
<g/>
.	.	kIx.	.
</s>
<s>
Veškerý	veškerý	k3xTgInSc1	veškerý
uživatelský	uživatelský	k2eAgInSc1d1	uživatelský
software	software	k1gInSc1	software
musí	muset	k5eAaImIp3nS	muset
procházet	procházet	k5eAaImF	procházet
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
používat	používat	k5eAaImF	používat
hardware	hardware	k1gInSc1	hardware
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
jako	jako	k8xS	jako
myš	myš	k1gFnSc1	myš
nebo	nebo	k8xC	nebo
klávesnice	klávesnice	k1gFnSc1	klávesnice
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
složitý	složitý	k2eAgInSc1d1	složitý
jako	jako	k8xS	jako
internetové	internetový	k2eAgFnPc1d1	internetová
komponenty	komponenta	k1gFnPc1	komponenta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Kernel	kernel	k1gInSc1	kernel
(	(	kIx(	(
<g/>
Jádro	jádro	k1gNnSc1	jádro
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
na	na	k7c4	na
<g/>
:	:	kIx,	:
Jádro	jádro	k1gNnSc4	jádro
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
</s>
</p>
<p>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
ovladačů	ovladač	k1gInPc2	ovladač
firmwaru	firmware	k1gInSc2	firmware
a	a	k8xC	a
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
,	,	kIx,	,
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
jádro	jádro	k1gNnSc1	jádro
nejzákladnější	základní	k2eAgFnSc4d3	nejzákladnější
úroveň	úroveň	k1gFnSc4	úroveň
kontroly	kontrola	k1gFnSc2	kontrola
všech	všecek	k3xTgNnPc2	všecek
hardwarových	hardwarový	k2eAgNnPc2d1	hardwarové
zařízení	zařízení	k1gNnPc2	zařízení
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Spravuje	spravovat	k5eAaImIp3nS	spravovat
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
paměti	paměť	k1gFnSc3	paměť
pro	pro	k7c4	pro
programy	program	k1gInPc4	program
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
RAM	RAM	kA	RAM
<g/>
,	,	kIx,	,
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
programy	program	k1gInPc1	program
získají	získat	k5eAaPmIp3nP	získat
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3	k
zdrojům	zdroj	k1gInPc3	zdroj
hardwaru	hardware	k1gInSc2	hardware
<g/>
,	,	kIx,	,
nastavuje	nastavovat	k5eAaImIp3nS	nastavovat
nebo	nebo	k8xC	nebo
obnovuje	obnovovat	k5eAaImIp3nS	obnovovat
provozní	provozní	k2eAgInPc4d1	provozní
stavy	stav	k1gInPc4	stav
procesoru	procesor	k1gInSc2	procesor
pro	pro	k7c4	pro
optimální	optimální	k2eAgInSc4d1	optimální
provoz	provoz	k1gInSc4	provoz
a	a	k8xC	a
organizuje	organizovat	k5eAaBmIp3nS	organizovat
data	datum	k1gNnPc4	datum
pro	pro	k7c4	pro
dlouhodobé	dlouhodobý	k2eAgFnPc4d1	dlouhodobá
<g/>
,	,	kIx,	,
energeticky	energeticky	k6eAd1	energeticky
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
<g/>
,	,	kIx,	,
ukládání	ukládání	k1gNnSc1	ukládání
dat	datum	k1gNnPc2	datum
se	s	k7c7	s
systémy	systém	k1gInPc7	systém
souborů	soubor	k1gInPc2	soubor
na	na	k7c6	na
takových	takový	k3xDgNnPc6	takový
médiích	médium	k1gNnPc6	médium
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
disky	disk	k1gInPc1	disk
<g/>
,	,	kIx,	,
pásky	pásek	k1gInPc1	pásek
<g/>
,	,	kIx,	,
flash	flash	k1gInSc1	flash
paměť	paměť	k1gFnSc1	paměť
atd.	atd.	kA	atd.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Spuštění	spuštění	k1gNnSc1	spuštění
programu	program	k1gInSc2	program
===	===	k?	===
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
na	na	k7c4	na
<g/>
:	:	kIx,	:
Proces	proces	k1gInSc4	proces
(	(	kIx(	(
<g/>
informatika	informatika	k1gFnSc1	informatika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
rozhraní	rozhraní	k1gNnSc4	rozhraní
mezi	mezi	k7c7	mezi
aplikačním	aplikační	k2eAgInSc7d1	aplikační
programem	program	k1gInSc7	program
a	a	k8xC	a
počítačovým	počítačový	k2eAgInSc7d1	počítačový
hardwarem	hardware	k1gInSc7	hardware
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
aplikační	aplikační	k2eAgInSc1d1	aplikační
program	program	k1gInSc1	program
může	moct	k5eAaImIp3nS	moct
komunikovat	komunikovat	k5eAaImF	komunikovat
s	s	k7c7	s
hardwarem	hardware	k1gInSc7	hardware
pouze	pouze	k6eAd1	pouze
dodržováním	dodržování	k1gNnSc7	dodržování
pravidel	pravidlo	k1gNnPc2	pravidlo
a	a	k8xC	a
postupů	postup	k1gInPc2	postup
naprogramovaných	naprogramovaný	k2eAgInPc2d1	naprogramovaný
v	v	k7c6	v
operačním	operační	k2eAgInSc6d1	operační
systému	systém	k1gInSc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
také	také	k9	také
soubor	soubor	k1gInSc1	soubor
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zjednodušují	zjednodušovat	k5eAaImIp3nP	zjednodušovat
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
spouštění	spouštění	k1gNnSc4	spouštění
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Spuštění	spuštění	k1gNnSc1	spuštění
programu	program	k1gInSc2	program
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
vytvoření	vytvoření	k1gNnSc4	vytvoření
procesu	proces	k1gInSc2	proces
jádrem	jádro	k1gNnSc7	jádro
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
přiřazuje	přiřazovat	k5eAaImIp3nS	přiřazovat
paměťový	paměťový	k2eAgInSc4d1	paměťový
prostor	prostor	k1gInSc4	prostor
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
zdroje	zdroj	k1gInPc4	zdroj
<g/>
,	,	kIx,	,
stanoví	stanovit	k5eAaPmIp3nS	stanovit
prioritu	priorita	k1gFnSc4	priorita
procesu	proces	k1gInSc2	proces
ve	v	k7c6	v
víceúlohových	víceúlohův	k2eAgInPc6d1	víceúlohův
systémech	systém	k1gInPc6	systém
<g/>
,	,	kIx,	,
načte	načíst	k5eAaBmIp3nS	načíst
programový	programový	k2eAgInSc4d1	programový
binární	binární	k2eAgInSc4d1	binární
kód	kód	k1gInSc4	kód
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
a	a	k8xC	a
spustí	spustit	k5eAaPmIp3nS	spustit
provádění	provádění	k1gNnSc4	provádění
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pak	pak	k6eAd1	pak
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
uživatelem	uživatel	k1gMnSc7	uživatel
a	a	k8xC	a
hardwarovými	hardwarový	k2eAgNnPc7d1	hardwarové
zařízeními	zařízení	k1gNnPc7	zařízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Přerušení	přerušení	k1gNnSc2	přerušení
===	===	k?	===
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
na	na	k7c4	na
<g/>
:	:	kIx,	:
Přerušení	přerušení	k1gNnSc4	přerušení
</s>
</p>
<p>
<s>
Přerušení	přerušení	k1gNnPc1	přerušení
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
operační	operační	k2eAgInPc4d1	operační
systémy	systém	k1gInPc4	systém
klíčová	klíčový	k2eAgFnSc1d1	klíčová
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
efektivní	efektivní	k2eAgInSc4d1	efektivní
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
může	moct	k5eAaImIp3nS	moct
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
komunikovat	komunikovat	k5eAaImF	komunikovat
a	a	k8xC	a
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c6	na
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Alternativa	alternativa	k1gFnSc1	alternativa
-	-	kIx~	-
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
"	"	kIx"	"
<g/>
sledovat	sledovat	k5eAaImF	sledovat
<g/>
"	"	kIx"	"
různé	různý	k2eAgInPc4d1	různý
zdroje	zdroj	k1gInPc4	zdroj
vstupů	vstup	k1gInPc2	vstup
pro	pro	k7c4	pro
události	událost	k1gFnPc4	událost
(	(	kIx(	(
<g/>
polling	polling	k1gInSc1	polling
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
akci	akce	k1gFnSc4	akce
-	-	kIx~	-
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
ve	v	k7c6	v
starších	starý	k2eAgInPc6d2	starší
systémech	systém	k1gInPc6	systém
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
malými	malý	k2eAgInPc7d1	malý
stacky	stack	k1gInPc7	stack
(	(	kIx(	(
<g/>
50	[number]	k4	50
nebo	nebo	k8xC	nebo
60	[number]	k4	60
bajtů	bajt	k1gInPc2	bajt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
neobvyklý	obvyklý	k2eNgInSc1d1	neobvyklý
v	v	k7c6	v
moderních	moderní	k2eAgInPc6d1	moderní
systémech	systém	k1gInPc6	systém
s	s	k7c7	s
velkými	velký	k2eAgInPc7d1	velký
stacky	stack	k1gInPc7	stack
<g/>
.	.	kIx.	.
</s>
<s>
Programování	programování	k1gNnSc1	programování
založené	založený	k2eAgNnSc1d1	založené
na	na	k7c6	na
přerušení	přerušení	k1gNnSc6	přerušení
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
podporováno	podporovat	k5eAaImNgNnS	podporovat
většinou	většinou	k6eAd1	většinou
moderních	moderní	k2eAgInPc2d1	moderní
procesorů	procesor	k1gInPc2	procesor
<g/>
.	.	kIx.	.
</s>
<s>
Přerušení	přerušení	k1gNnSc1	přerušení
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
počítači	počítač	k1gInSc3	počítač
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
automaticky	automaticky	k6eAd1	automaticky
ukládat	ukládat	k5eAaImF	ukládat
místní	místní	k2eAgInPc4d1	místní
kontexty	kontext	k1gInPc4	kontext
registru	registr	k1gInSc2	registr
a	a	k8xC	a
spouštět	spouštět	k5eAaImF	spouštět
určitý	určitý	k2eAgInSc4d1	určitý
kód	kód	k1gInSc4	kód
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c6	na
události	událost	k1gFnSc6	událost
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
velmi	velmi	k6eAd1	velmi
základní	základní	k2eAgInPc1d1	základní
počítače	počítač	k1gInPc1	počítač
podporují	podporovat	k5eAaImIp3nP	podporovat
přerušení	přerušení	k1gNnPc4	přerušení
hardwaru	hardware	k1gInSc2	hardware
a	a	k8xC	a
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
programátorovi	programátor	k1gMnSc3	programátor
zadat	zadat	k5eAaPmF	zadat
kód	kód	k1gInSc4	kód
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
spuštěn	spustit	k5eAaPmNgInS	spustit
<g/>
,	,	kIx,	,
když	když	k8xS	když
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
dojde	dojít	k5eAaPmIp3nS	dojít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
přijetí	přijetí	k1gNnSc6	přijetí
přerušení	přerušení	k1gNnSc2	přerušení
se	se	k3xPyFc4	se
hardware	hardware	k1gInSc1	hardware
počítače	počítač	k1gInSc2	počítač
automaticky	automaticky	k6eAd1	automaticky
pozastaví	pozastavit	k5eAaPmIp3nS	pozastavit
<g/>
.	.	kIx.	.
</s>
<s>
Jakýkoli	jakýkoli	k3yIgInSc1	jakýkoli
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
aktuálně	aktuálně	k6eAd1	aktuálně
spuštěn	spustit	k5eAaPmNgInS	spustit
<g/>
,	,	kIx,	,
uloží	uložit	k5eAaPmIp3nS	uložit
svůj	svůj	k3xOyFgInSc4	svůj
stav	stav	k1gInSc4	stav
a	a	k8xC	a
spustí	spustit	k5eAaPmIp3nS	spustit
počítačový	počítačový	k2eAgInSc1d1	počítačový
kód	kód	k1gInSc1	kód
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
přidružený	přidružený	k2eAgMnSc1d1	přidružený
k	k	k7c3	k
přerušení	přerušení	k1gNnSc3	přerušení
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
analogické	analogický	k2eAgNnSc1d1	analogické
k	k	k7c3	k
umístění	umístění	k1gNnSc3	umístění
záložky	záložka	k1gFnSc2	záložka
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
odpověď	odpověď	k1gFnSc1	odpověď
na	na	k7c4	na
telefonní	telefonní	k2eAgInSc4d1	telefonní
hovor	hovor	k1gInSc4	hovor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderních	moderní	k2eAgInPc6d1	moderní
operačních	operační	k2eAgInPc6d1	operační
systémech	systém	k1gInPc6	systém
jsou	být	k5eAaImIp3nP	být
přerušení	přerušení	k1gNnSc3	přerušení
zpracovávány	zpracováván	k2eAgFnPc1d1	zpracovávána
jádrem	jádro	k1gNnSc7	jádro
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Přerušení	přerušení	k1gNnPc1	přerušení
mohou	moct	k5eAaImIp3nP	moct
pocházet	pocházet	k5eAaImF	pocházet
buď	buď	k8xC	buď
z	z	k7c2	z
hardwaru	hardware	k1gInSc2	hardware
počítače	počítač	k1gInSc2	počítač
nebo	nebo	k8xC	nebo
z	z	k7c2	z
běžícího	běžící	k2eAgInSc2d1	běžící
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
hardware	hardware	k1gInSc1	hardware
zařízení	zařízení	k1gNnSc2	zařízení
spustí	spustit	k5eAaPmIp3nS	spustit
přerušení	přerušení	k1gNnSc1	přerušení
<g/>
,	,	kIx,	,
jádro	jádro	k1gNnSc1	jádro
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
událostí	událost	k1gFnSc7	událost
vypořádat	vypořádat	k5eAaPmF	vypořádat
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
spustí	spustit	k5eAaPmIp3nS	spustit
nějaký	nějaký	k3yIgInSc4	nějaký
kód	kód	k1gInSc4	kód
zpracování	zpracování	k1gNnSc2	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
spuštěného	spuštěný	k2eAgInSc2d1	spuštěný
kódu	kód	k1gInSc2	kód
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
prioritě	priorita	k1gFnSc6	priorita
přerušení	přerušení	k1gNnSc2	přerušení
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
osoba	osoba	k1gFnSc1	osoba
obvykle	obvykle	k6eAd1	obvykle
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
alarm	alarm	k1gInSc4	alarm
detektoru	detektor	k1gInSc2	detektor
kouře	kouř	k1gInSc2	kouř
před	před	k7c7	před
odpovědí	odpověď	k1gFnSc7	odpověď
na	na	k7c4	na
telefon	telefon	k1gInSc4	telefon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpracování	zpracování	k1gNnSc1	zpracování
přerušení	přerušení	k1gNnSc2	přerušení
hardwaru	hardware	k1gInSc2	hardware
je	být	k5eAaImIp3nS	být
úloha	úloha	k1gFnSc1	úloha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
delegována	delegovat	k5eAaBmNgFnS	delegovat
na	na	k7c4	na
software	software	k1gInSc4	software
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
ovladač	ovladač	k1gInSc1	ovladač
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
součástí	součást	k1gFnSc7	součást
jádra	jádro	k1gNnSc2	jádro
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
jiného	jiný	k2eAgInSc2d1	jiný
programu	program	k1gInSc2	program
nebo	nebo	k8xC	nebo
obojího	obojí	k4xRgMnSc2	obojí
<g/>
.	.	kIx.	.
</s>
<s>
Ovladače	ovladač	k1gInPc4	ovladač
zařízení	zařízení	k1gNnSc2	zařízení
pak	pak	k6eAd1	pak
mohou	moct	k5eAaImIp3nP	moct
přenášet	přenášet	k5eAaImF	přenášet
informace	informace	k1gFnPc4	informace
do	do	k7c2	do
běžícího	běžící	k2eAgInSc2d1	běžící
programu	program	k1gInSc2	program
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Program	program	k1gInSc1	program
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
spustit	spustit	k5eAaPmF	spustit
přerušení	přerušení	k1gNnSc2	přerušení
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
chce	chtít	k5eAaImIp3nS	chtít
program	program	k1gInSc4	program
například	například	k6eAd1	například
přistupovat	přistupovat	k5eAaImF	přistupovat
k	k	k7c3	k
hardwaru	hardware	k1gInSc3	hardware
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
přerušit	přerušit	k5eAaPmF	přerušit
jádro	jádro	k1gNnSc1	jádro
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobí	způsobit	k5eAaPmIp3nS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
kontrola	kontrola	k1gFnSc1	kontrola
přenese	přenést	k5eAaPmIp3nS	přenést
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
potom	potom	k6eAd1	potom
zpracuje	zpracovat	k5eAaPmIp3nS	zpracovat
požadavek	požadavek	k1gInSc4	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
některý	některý	k3yIgInSc1	některý
program	program	k1gInSc1	program
přeje	přát	k5eAaImIp3nS	přát
další	další	k2eAgInPc4d1	další
zdroje	zdroj	k1gInPc4	zdroj
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
si	se	k3xPyFc3	se
přeje	přát	k5eAaImIp3nS	přát
zbavit	zbavit	k5eAaPmF	zbavit
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
paměť	paměť	k1gFnSc4	paměť
<g/>
,	,	kIx,	,
spustí	spustit	k5eAaPmIp3nS	spustit
přerušení	přerušení	k1gNnSc1	přerušení
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
získalo	získat	k5eAaPmAgNnS	získat
pozornost	pozornost	k1gFnSc4	pozornost
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Režimy	režim	k1gInPc4	režim
===	===	k?	===
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
na	na	k7c6	na
<g/>
:	:	kIx,	:
User	usrat	k5eAaPmRp2nS	usrat
space	space	k1gFnSc2	space
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
mikroprocesory	mikroprocesor	k1gInPc1	mikroprocesor
(	(	kIx(	(
<g/>
CPU	CPU	kA	CPU
nebo	nebo	k8xC	nebo
MPU	MPU	kA	MPU
<g/>
)	)	kIx)	)
podporují	podporovat	k5eAaImIp3nP	podporovat
více	hodně	k6eAd2	hodně
režimů	režim	k1gInPc2	režim
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
CPU	cpát	k5eAaImIp1nS	cpát
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
schopností	schopnost	k1gFnSc7	schopnost
nabízejí	nabízet	k5eAaImIp3nP	nabízet
alespoň	alespoň	k9	alespoň
dva	dva	k4xCgInPc4	dva
režimy	režim	k1gInPc4	režim
<g/>
:	:	kIx,	:
uživatelský	uživatelský	k2eAgInSc4d1	uživatelský
režim	režim	k1gInSc4	režim
a	a	k8xC	a
režim	režim	k1gInSc4	režim
supervizora	supervizor	k1gMnSc2	supervizor
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
režim	režim	k1gInSc1	režim
supervizora	supervizor	k1gMnSc2	supervizor
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
neomezený	omezený	k2eNgInSc4d1	neomezený
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3	k
všem	všecek	k3xTgInPc3	všecek
strojním	strojní	k2eAgInPc3d1	strojní
zdrojům	zdroj	k1gInPc3	zdroj
včetně	včetně	k7c2	včetně
všech	všecek	k3xTgFnPc2	všecek
instrukcí	instrukce	k1gFnPc2	instrukce
MPU	MPU	kA	MPU
<g/>
.	.	kIx.	.
</s>
<s>
Režim	režim	k1gInSc1	režim
uživatelského	uživatelský	k2eAgInSc2d1	uživatelský
režimu	režim	k1gInSc2	režim
nastavuje	nastavovat	k5eAaImIp3nS	nastavovat
limity	limit	k1gInPc4	limit
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
instrukcí	instrukce	k1gFnPc2	instrukce
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
přímý	přímý	k2eAgInSc4d1	přímý
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
strojním	strojní	k2eAgInPc3d1	strojní
zdrojům	zdroj	k1gInPc3	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
CPU	CPU	kA	CPU
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
režimy	režim	k1gInPc1	režim
podobné	podobný	k2eAgInPc1d1	podobný
uživatelskému	uživatelský	k2eAgInSc3d1	uživatelský
režimu	režim	k1gInSc3	režim
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
virtuální	virtuální	k2eAgInPc4d1	virtuální
režimy	režim	k1gInPc4	režim
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
emulovali	emulovat	k5eAaImAgMnP	emulovat
starší	starý	k2eAgInPc4d2	starší
typy	typ	k1gInPc4	typ
procesorů	procesor	k1gInPc2	procesor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
16	[number]	k4	16
<g/>
bitové	bitový	k2eAgInPc1d1	bitový
procesory	procesor	k1gInPc1	procesor
na	na	k7c6	na
32	[number]	k4	32
<g/>
bitovém	bitový	k2eAgInSc6d1	bitový
nebo	nebo	k8xC	nebo
32	[number]	k4	32
<g/>
bitové	bitový	k2eAgInPc1d1	bitový
procesory	procesor	k1gInPc1	procesor
na	na	k7c4	na
64	[number]	k4	64
<g/>
bitové	bitový	k2eAgInPc1d1	bitový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
zapnutí	zapnutí	k1gNnSc6	zapnutí
nebo	nebo	k8xC	nebo
resetování	resetování	k1gNnSc6	resetování
začne	začít	k5eAaPmIp3nS	začít
systém	systém	k1gInSc1	systém
v	v	k7c6	v
režimu	režim	k1gInSc6	režim
supervizora	supervizor	k1gMnSc2	supervizor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
načtení	načtení	k1gNnSc6	načtení
a	a	k8xC	a
spuštění	spuštění	k1gNnSc6	spuštění
jádra	jádro	k1gNnSc2	jádro
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
lze	lze	k6eAd1	lze
vytvořit	vytvořit	k5eAaPmF	vytvořit
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
režimem	režim	k1gInSc7	režim
uživatele	uživatel	k1gMnSc2	uživatel
a	a	k8xC	a
režimem	režim	k1gInSc7	režim
supervizora	supervizor	k1gMnSc2	supervizor
(	(	kIx(	(
<g/>
také	také	k9	také
známým	známý	k1gMnSc7	známý
jako	jako	k8xC	jako
režim	režim	k1gInSc4	režim
jádra	jádro	k1gNnSc2	jádro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Režim	režim	k1gInSc1	režim
supervizora	supervizor	k1gMnSc2	supervizor
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jádrem	jádro	k1gNnSc7	jádro
pro	pro	k7c4	pro
úkoly	úkol	k1gInPc4	úkol
s	s	k7c7	s
nízkou	nízký	k2eAgFnSc7d1	nízká
úrovní	úroveň	k1gFnSc7	úroveň
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
neomezený	omezený	k2eNgInSc4d1	neomezený
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
hardwaru	hardware	k1gInSc3	hardware
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
kontrola	kontrola	k1gFnSc1	kontrola
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
paměti	paměť	k1gFnSc3	paměť
a	a	k8xC	a
komunikace	komunikace	k1gFnSc1	komunikace
se	s	k7c7	s
zařízeními	zařízení	k1gNnPc7	zařízení
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
diskové	diskový	k2eAgFnPc4d1	disková
jednotky	jednotka	k1gFnPc4	jednotka
a	a	k8xC	a
zařízení	zařízení	k1gNnPc4	zařízení
pro	pro	k7c4	pro
zobrazování	zobrazování	k1gNnSc4	zobrazování
videa	video	k1gNnSc2	video
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelský	uživatelský	k2eAgInSc1d1	uživatelský
režim	režim	k1gInSc1	režim
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
používá	používat	k5eAaImIp3nS	používat
téměř	téměř	k6eAd1	téměř
pro	pro	k7c4	pro
všechno	všechen	k3xTgNnSc4	všechen
ostatní	ostatní	k1gNnSc4	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Aplikační	aplikační	k2eAgInPc1d1	aplikační
programy	program	k1gInPc1	program
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
textové	textový	k2eAgInPc1d1	textový
procesory	procesor	k1gInPc1	procesor
a	a	k8xC	a
správci	správce	k1gMnPc1	správce
databází	databáze	k1gFnPc2	databáze
<g/>
,	,	kIx,	,
pracují	pracovat	k5eAaImIp3nP	pracovat
v	v	k7c6	v
uživatelském	uživatelský	k2eAgInSc6d1	uživatelský
režimu	režim	k1gInSc6	režim
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
přístup	přístup	k1gInSc4	přístup
pouze	pouze	k6eAd1	pouze
ke	k	k7c3	k
strojním	strojní	k2eAgInPc3d1	strojní
prostředkům	prostředek	k1gInPc3	prostředek
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
převádějí	převádět	k5eAaImIp3nP	převádět
ovládání	ovládání	k1gNnSc4	ovládání
na	na	k7c4	na
jádro	jádro	k1gNnSc4	jádro
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
způsobí	způsobit	k5eAaPmIp3nS	způsobit
přepnutí	přepnutí	k1gNnSc4	přepnutí
do	do	k7c2	do
režimu	režim	k1gInSc2	režim
supervizora	supervizor	k1gMnSc2	supervizor
<g/>
.	.	kIx.	.
</s>
<s>
Přenos	přenos	k1gInSc1	přenos
řízení	řízení	k1gNnSc2	řízení
do	do	k7c2	do
jádra	jádro	k1gNnSc2	jádro
je	být	k5eAaImIp3nS	být
typicky	typicky	k6eAd1	typicky
dosažen	dosáhnout	k5eAaPmNgInS	dosáhnout
provedením	provedení	k1gNnSc7	provedení
instrukce	instrukce	k1gFnSc2	instrukce
pro	pro	k7c4	pro
přerušení	přerušení	k1gNnSc4	přerušení
softwaru	software	k1gInSc2	software
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
instrukce	instrukce	k1gFnPc4	instrukce
Motorola	Motorola	kA	Motorola
68000	[number]	k4	68000
TRAP	trap	k1gInSc4	trap
<g/>
.	.	kIx.	.
</s>
<s>
Přerušení	přerušení	k1gNnSc1	přerušení
softwaru	software	k1gInSc2	software
způsobí	způsobit	k5eAaPmIp3nS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mikroprocesor	mikroprocesor	k1gInSc1	mikroprocesor
přepne	přepnout	k5eAaPmIp3nS	přepnout
z	z	k7c2	z
uživatelského	uživatelský	k2eAgInSc2d1	uživatelský
režimu	režim	k1gInSc2	režim
do	do	k7c2	do
režimu	režim	k1gInSc2	režim
supervizora	supervizor	k1gMnSc2	supervizor
a	a	k8xC	a
spustí	spustit	k5eAaPmIp3nS	spustit
provádění	provádění	k1gNnSc4	provádění
kódu	kód	k1gInSc2	kód
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
umožní	umožnit	k5eAaPmIp3nS	umožnit
jádru	jádro	k1gNnSc6	jádro
převzít	převzít	k5eAaPmF	převzít
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
uživatelském	uživatelský	k2eAgInSc6d1	uživatelský
režimu	režim	k1gInSc6	režim
mají	mít	k5eAaImIp3nP	mít
programy	program	k1gInPc1	program
obvykle	obvykle	k6eAd1	obvykle
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
omezené	omezený	k2eAgFnSc3d1	omezená
sadě	sada	k1gFnSc3	sada
instrukcí	instrukce	k1gFnPc2	instrukce
mikroprocesoru	mikroprocesor	k1gInSc2	mikroprocesor
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
nemohou	moct	k5eNaImIp3nP	moct
provádět	provádět	k5eAaImF	provádět
žádné	žádný	k3yNgInPc1	žádný
pokyny	pokyn	k1gInPc1	pokyn
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
potenciálně	potenciálně	k6eAd1	potenciálně
způsobit	způsobit	k5eAaPmF	způsobit
narušení	narušení	k1gNnSc4	narušení
provozu	provoz	k1gInSc2	provoz
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
režimu	režim	k1gInSc6	režim
supervizora	supervizor	k1gMnSc2	supervizor
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
odstraněny	odstraněn	k2eAgInPc1d1	odstraněn
pokyny	pokyn	k1gInPc1	pokyn
k	k	k7c3	k
provádění	provádění	k1gNnSc3	provádění
instrukcí	instrukce	k1gFnPc2	instrukce
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
jádru	jádro	k1gNnSc6	jádro
neomezený	omezený	k2eNgInSc4d1	neomezený
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3	k
všem	všecek	k3xTgInPc3	všecek
strojním	strojní	k2eAgInPc3d1	strojní
zdrojům	zdroj	k1gInPc3	zdroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
prostředek	prostředek	k1gInSc1	prostředek
uživatelského	uživatelský	k2eAgInSc2d1	uživatelský
módu	mód	k1gInSc2	mód
<g/>
"	"	kIx"	"
obecně	obecně	k6eAd1	obecně
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
registrů	registr	k1gInPc2	registr
CPU	CPU	kA	CPU
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
informace	informace	k1gFnPc1	informace
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
běžící	běžící	k2eAgInSc4d1	běžící
program	program	k1gInSc4	program
nesmí	smět	k5eNaImIp3nS	smět
měnit	měnit	k5eAaImF	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
změnu	změna	k1gFnSc4	změna
těchto	tento	k3xDgInPc2	tento
zdrojů	zdroj	k1gInPc2	zdroj
obecně	obecně	k6eAd1	obecně
způsobí	způsobit	k5eAaPmIp3nS	způsobit
přepnutí	přepnutí	k1gNnSc4	přepnutí
do	do	k7c2	do
režimu	režim	k1gInSc2	režim
supervizora	supervizor	k1gMnSc2	supervizor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
může	moct	k5eAaImIp3nS	moct
vypořádat	vypořádat	k5eAaPmF	vypořádat
s	s	k7c7	s
nelegální	legální	k2eNgFnSc7d1	nelegální
činností	činnost	k1gFnSc7	činnost
<g/>
,	,	kIx,	,
o	o	k7c4	o
kterou	který	k3yRgFnSc4	který
se	se	k3xPyFc4	se
program	program	k1gInSc4	program
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
násilným	násilný	k2eAgNnSc7d1	násilné
ukončením	ukončení	k1gNnSc7	ukončení
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
zabíjením	zabíjení	k1gNnSc7	zabíjení
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
programu	program	k1gInSc2	program
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Správa	správa	k1gFnSc1	správa
paměti	paměť	k1gFnSc2	paměť
===	===	k?	===
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
na	na	k7c4	na
<g/>
:	:	kIx,	:
Správa	správa	k1gFnSc1	správa
paměti	paměť	k1gFnSc2	paměť
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
jiného	jiný	k2eAgNnSc2d1	jiné
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
jádro	jádro	k1gNnSc1	jádro
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
s	s	k7c7	s
více	hodně	k6eAd2	hodně
programy	program	k1gInPc7	program
<g/>
,	,	kIx,	,
programem	program	k1gInSc7	program
odpovědným	odpovědný	k2eAgInSc7d1	odpovědný
za	za	k7c4	za
správu	správa	k1gFnSc4	správa
všech	všecek	k3xTgFnPc2	všecek
systémových	systémový	k2eAgFnPc2d1	systémová
pamětí	paměť	k1gFnPc2	paměť
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
používány	používán	k2eAgInPc1d1	používán
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zajistí	zajistit	k5eAaPmIp3nS	zajistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
program	program	k1gInSc1	program
nenaruší	narušit	k5eNaPmIp3nS	narušit
paměť	paměť	k1gFnSc4	paměť
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
již	již	k6eAd1	již
používá	používat	k5eAaImIp3nS	používat
jiný	jiný	k2eAgInSc4d1	jiný
program	program	k1gInSc4	program
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
programy	program	k1gInPc1	program
sdílejí	sdílet	k5eAaImIp3nP	sdílet
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
program	program	k1gInSc1	program
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
nezávislý	závislý	k2eNgInSc1d1	nezávislý
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
paměti	paměť	k1gFnSc3	paměť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kooperativní	kooperativní	k2eAgFnSc1d1	kooperativní
správa	správa	k1gFnSc1	správa
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
používaná	používaný	k2eAgFnSc1d1	používaná
mnoha	mnoho	k4c7	mnoho
ranými	raný	k2eAgInPc7d1	raný
operačními	operační	k2eAgInPc7d1	operační
systémy	systém	k1gInPc7	systém
<g/>
,	,	kIx,	,
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgInPc1	všechen
programy	program	k1gInPc1	program
využívají	využívat	k5eAaImIp3nP	využívat
správce	správce	k1gMnPc4	správce
paměti	paměť	k1gFnSc2	paměť
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
nepřekračují	překračovat	k5eNaImIp3nP	překračovat
alokovanou	alokovaný	k2eAgFnSc4d1	alokovaná
paměť	paměť	k1gFnSc4	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
správy	správa	k1gFnSc2	správa
paměti	paměť	k1gFnSc2	paměť
není	být	k5eNaImIp3nS	být
moc	moc	k6eAd1	moc
používán	používat	k5eAaImNgInS	používat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
programy	program	k1gInPc1	program
často	často	k6eAd1	často
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
chyby	chyba	k1gFnPc1	chyba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
způsobit	způsobit	k5eAaPmF	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
překročí	překročit	k5eAaPmIp3nS	překročit
alokovanou	alokovaný	k2eAgFnSc4d1	alokovaná
paměť	paměť	k1gFnSc4	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
program	program	k1gInSc1	program
selže	selhat	k5eAaPmIp3nS	selhat
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
ovlivnění	ovlivnění	k1gNnSc3	ovlivnění
nebo	nebo	k8xC	nebo
přepsání	přepsání	k1gNnSc3	přepsání
používané	používaný	k2eAgFnSc2d1	používaná
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Škodlivé	škodlivý	k2eAgInPc1d1	škodlivý
programy	program	k1gInPc1	program
nebo	nebo	k8xC	nebo
viry	vir	k1gInPc1	vir
mohou	moct	k5eAaImIp3nP	moct
pozměnit	pozměnit	k5eAaPmF	pozměnit
data	datum	k1gNnPc4	datum
v	v	k7c4	v
paměť	paměť	k1gFnSc4	paměť
jiného	jiný	k2eAgInSc2d1	jiný
programu	program	k1gInSc2	program
nebo	nebo	k8xC	nebo
mohou	moct	k5eAaImIp3nP	moct
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
provoz	provoz	k1gInSc4	provoz
samotného	samotný	k2eAgInSc2d1	samotný
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ochrana	ochrana	k1gFnSc1	ochrana
paměti	paměť	k1gFnSc2	paměť
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
jádru	jádro	k1gNnSc6	jádro
omezit	omezit	k5eAaPmF	omezit
přístup	přístup	k1gInSc1	přístup
procesu	proces	k1gInSc2	proces
k	k	k7c3	k
paměti	paměť	k1gFnSc3	paměť
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
různé	různý	k2eAgInPc4d1	různý
způsoby	způsob	k1gInPc4	způsob
ochrany	ochrana	k1gFnSc2	ochrana
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
segmentace	segmentace	k1gFnSc2	segmentace
paměti	paměť	k1gFnSc2	paměť
a	a	k8xC	a
stránkování	stránkování	k1gNnSc2	stránkování
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
metody	metoda	k1gFnPc1	metoda
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
určitou	určitý	k2eAgFnSc4d1	určitá
úroveň	úroveň	k1gFnSc4	úroveň
hardwarové	hardwarový	k2eAgFnSc2d1	hardwarová
podpory	podpora	k1gFnSc2	podpora
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
80286	[number]	k4	80286
MMU	MMU	kA	MMU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
počítačích	počítač	k1gInPc6	počítač
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
segmentaci	segmentace	k1gFnSc6	segmentace
a	a	k8xC	a
stránkování	stránkování	k1gNnSc6	stránkování
<g/>
,	,	kIx,	,
chráněný	chráněný	k2eAgInSc1d1	chráněný
režim	režim	k1gInSc1	režim
registrů	registr	k1gInPc2	registr
určuje	určovat	k5eAaImIp3nS	určovat
CPU	CPU	kA	CPU
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc4	jaký
adresu	adresa	k1gFnSc4	adresa
paměti	paměť	k1gFnSc2	paměť
má	mít	k5eAaImIp3nS	mít
běžícím	běžící	k2eAgInPc3d1	běžící
programům	program	k1gInPc3	program
zpřístupnit	zpřístupnit	k5eAaPmF	zpřístupnit
<g/>
.	.	kIx.	.
</s>
<s>
Pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
jiným	jiný	k2eAgFnPc3d1	jiná
adresám	adresa	k1gFnPc3	adresa
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
přerušení	přerušení	k1gNnSc3	přerušení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
způsobí	způsobit	k5eAaPmIp3nS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
procesor	procesor	k1gInSc1	procesor
znovu	znovu	k6eAd1	znovu
vstoupí	vstoupit	k5eAaPmIp3nS	vstoupit
do	do	k7c2	do
režimu	režim	k1gInSc2	režim
supervizora	supervizor	k1gMnSc2	supervizor
a	a	k8xC	a
přesune	přesunout	k5eAaPmIp3nS	přesunout
jádro	jádro	k1gNnSc1	jádro
do	do	k7c2	do
primárního	primární	k2eAgInSc2d1	primární
módu	mód	k1gInSc2	mód
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
porušení	porušení	k1gNnSc1	porušení
ochrany	ochrana	k1gFnSc2	ochrana
paměti	paměť	k1gFnSc2	paměť
(	(	kIx(	(
<g/>
segmentation	segmentation	k1gInSc1	segmentation
violation	violation	k1gInSc1	violation
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
Seg-V	Seg-V	k1gFnSc1	Seg-V
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
přiřadit	přiřadit	k5eAaPmF	přiřadit
k	k	k7c3	k
takové	takový	k3xDgFnSc3	takový
operaci	operace	k1gFnSc3	operace
smysluplný	smysluplný	k2eAgInSc1d1	smysluplný
výsledek	výsledek	k1gInSc1	výsledek
a	a	k8xC	a
jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
obvykle	obvykle	k6eAd1	obvykle
známka	známka	k1gFnSc1	známka
chybného	chybný	k2eAgInSc2d1	chybný
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
jádro	jádro	k1gNnSc1	jádro
ukončí	ukončit	k5eAaPmIp3nS	ukončit
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
porušil	porušit	k5eAaPmAgMnS	porušit
předpisy	předpis	k1gInPc7	předpis
a	a	k8xC	a
nahlásí	nahlásit	k5eAaPmIp3nP	nahlásit
chybu	chyba	k1gFnSc4	chyba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Windows	Windows	kA	Windows
verze	verze	k1gFnSc1	verze
3.1	[number]	k4	3.1
až	až	k8xS	až
ME	ME	kA	ME
měla	mít	k5eAaImAgFnS	mít
jistou	jistý	k2eAgFnSc4d1	jistá
úroveň	úroveň	k1gFnSc4	úroveň
ochrany	ochrana	k1gFnSc2	ochrana
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
programy	program	k1gInPc1	program
mohly	moct	k5eAaImAgInP	moct
snadno	snadno	k6eAd1	snadno
obcházet	obcházet	k5eAaImF	obcházet
přiřazení	přiřazení	k1gNnSc4	přiřazení
potřebné	potřebný	k2eAgFnSc2d1	potřebná
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
obecná	obecný	k2eAgFnSc1d1	obecná
chyba	chyba	k1gFnSc1	chyba
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
narušení	narušení	k1gNnSc3	narušení
segmentace	segmentace	k1gFnSc2	segmentace
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
i	i	k9	i
tak	tak	k6eAd1	tak
často	často	k6eAd1	často
spadnul	spadnout	k5eAaPmAgMnS	spadnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Virtuální	virtuální	k2eAgFnSc4d1	virtuální
paměť	paměť	k1gFnSc4	paměť
===	===	k?	===
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
na	na	k7c4	na
<g/>
:	:	kIx,	:
Virtuální	virtuální	k2eAgFnSc4d1	virtuální
paměť	paměť	k1gFnSc4	paměť
</s>
</p>
<p>
<s>
Použití	použití	k1gNnSc1	použití
adresování	adresování	k1gNnSc2	adresování
virtuální	virtuální	k2eAgFnSc2d1	virtuální
paměti	paměť	k1gFnSc2	paměť
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
stránkování	stránkování	k1gNnSc1	stránkování
nebo	nebo	k8xC	nebo
segmentace	segmentace	k1gFnSc1	segmentace
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jádro	jádro	k1gNnSc1	jádro
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
zvolit	zvolit	k5eAaPmF	zvolit
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc4	jaký
paměť	paměť	k1gFnSc4	paměť
může	moct	k5eAaImIp3nS	moct
používat	používat	k5eAaImF	používat
každý	každý	k3xTgInSc4	každý
program	program	k1gInSc4	program
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
operačnímu	operační	k2eAgInSc3d1	operační
systému	systém	k1gInSc3	systém
používat	používat	k5eAaImF	používat
stejné	stejné	k1gNnSc4	stejné
paměti	paměť	k1gFnSc2	paměť
pro	pro	k7c4	pro
více	hodně	k6eAd2	hodně
úkolů	úkol	k1gInPc2	úkol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
program	program	k1gInSc1	program
pokusí	pokusit	k5eAaPmIp3nS	pokusit
přistupovat	přistupovat	k5eAaImF	přistupovat
k	k	k7c3	k
paměti	paměť	k1gFnSc3	paměť
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
dosavadním	dosavadní	k2eAgInSc6d1	dosavadní
rozsahu	rozsah	k1gInSc6	rozsah
dostupné	dostupný	k2eAgFnSc2d1	dostupná
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
byla	být	k5eAaImAgFnS	být
přidělena	přidělen	k2eAgFnSc1d1	přidělena
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jádrem	jádro	k1gNnSc7	jádro
přerušen	přerušit	k5eAaPmNgInS	přerušit
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
kdyby	kdyby	kYmCp3nS	kdyby
program	program	k1gInSc1	program
překročil	překročit	k5eAaPmAgInS	překročit
přidělenou	přidělený	k2eAgFnSc4d1	přidělená
paměť	paměť	k1gFnSc4	paměť
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Viz	vidět	k5eAaImRp2nS	vidět
část	část	k1gFnSc1	část
týkající	týkající	k2eAgFnSc1d1	týkající
se	se	k3xPyFc4	se
správy	správa	k1gFnSc2	správa
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Pod	pod	k7c7	pod
UNIXem	Unix	k1gInSc7	Unix
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
přerušení	přerušení	k1gNnSc2	přerušení
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
chyba	chyba	k1gFnSc1	chyba
stránky	stránka	k1gFnSc2	stránka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
jádro	jádro	k1gNnSc1	jádro
zjistí	zjistit	k5eAaPmIp3nS	zjistit
chybu	chyba	k1gFnSc4	chyba
stránky	stránka	k1gFnSc2	stránka
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
upraví	upravit	k5eAaPmIp3nS	upravit
rozsah	rozsah	k1gInSc4	rozsah
virtuální	virtuální	k2eAgFnSc2d1	virtuální
paměti	paměť	k1gFnSc2	paměť
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ji	on	k3xPp3gFnSc4	on
spustil	spustit	k5eAaPmAgInS	spustit
<g/>
,	,	kIx,	,
a	a	k8xC	a
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
jí	on	k3xPp3gFnSc3	on
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
požadované	požadovaný	k2eAgFnSc3d1	požadovaná
paměti	paměť	k1gFnSc3	paměť
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
dává	dávat	k5eAaImIp3nS	dávat
jádru	jádro	k1gNnSc3	jádro
diskrétní	diskrétní	k2eAgFnSc1d1	diskrétní
pravomoc	pravomoc	k1gFnSc1	pravomoc
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
uložena	uložen	k2eAgFnSc1d1	uložena
paměť	paměť	k1gFnSc1	paměť
konkrétní	konkrétní	k2eAgFnSc2d1	konkrétní
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
byla	být	k5eAaImAgFnS	být
nebo	nebo	k8xC	nebo
nebyla	být	k5eNaImAgFnS	být
dosud	dosud	k6eAd1	dosud
přidělena	přidělen	k2eAgFnSc1d1	přidělena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
moderních	moderní	k2eAgInPc6d1	moderní
operačních	operační	k2eAgInPc6d1	operační
systémech	systém	k1gInPc6	systém
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
paměť	paměť	k1gFnSc4	paměť
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yRgFnSc3	který
se	se	k3xPyFc4	se
přistupuje	přistupovat	k5eAaImIp3nS	přistupovat
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
dočasně	dočasně	k6eAd1	dočasně
uložena	uložit	k5eAaPmNgNnP	uložit
na	na	k7c6	na
disku	disk	k1gInSc6	disk
nebo	nebo	k8xC	nebo
na	na	k7c6	na
jiném	jiný	k2eAgNnSc6d1	jiné
médiu	médium	k1gNnSc6	médium
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
prostor	prostor	k1gInSc4	prostor
dostupný	dostupný	k2eAgInSc4d1	dostupný
pro	pro	k7c4	pro
jiné	jiný	k2eAgInPc4d1	jiný
programy	program	k1gInPc4	program
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
výměna	výměna	k1gFnSc1	výměna
<g/>
(	(	kIx(	(
<g/>
swapping	swapping	k1gInSc1	swapping
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
oblast	oblast	k1gFnSc1	oblast
paměti	paměť	k1gFnSc2	paměť
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
používána	používán	k2eAgFnSc1d1	používána
více	hodně	k6eAd2	hodně
programy	program	k1gInPc7	program
<g/>
,	,	kIx,	,
a	a	k8xC	a
co	co	k9	co
tato	tento	k3xDgFnSc1	tento
paměťová	paměťový	k2eAgFnSc1d1	paměťová
oblast	oblast	k1gFnSc1	oblast
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
"	"	kIx"	"
<g/>
swappnout	swappnout	k5eAaPmF	swappnout
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
vyměnit	vyměnit	k5eAaPmF	vyměnit
na	na	k7c4	na
požádání	požádání	k1gNnSc4	požádání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Virtuální	virtuální	k2eAgFnSc4d1	virtuální
paměť	paměť	k1gFnSc4	paměť
<g/>
"	"	kIx"	"
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
programátorovi	programátor	k1gMnSc3	programátor
nebo	nebo	k8xC	nebo
uživateli	uživatel	k1gMnSc3	uživatel
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
počítači	počítač	k1gInSc6	počítač
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
(	(	kIx(	(
<g/>
RAM	RAM	kA	RAM
<g/>
)	)	kIx)	)
než	než	k8xS	než
tam	tam	k6eAd1	tam
skutečně	skutečně	k6eAd1	skutečně
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Multitasking	multitasking	k1gInSc4	multitasking
===	===	k?	===
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
na	na	k7c4	na
<g/>
:	:	kIx,	:
Multitasking	multitasking	k1gInSc4	multitasking
<g/>
,	,	kIx,	,
Změna	změna	k1gFnSc1	změna
kontextu	kontext	k1gInSc2	kontext
<g/>
,	,	kIx,	,
Preempce	Preempce	k1gFnSc1	Preempce
(	(	kIx(	(
<g/>
informatika	informatika	k1gFnSc1	informatika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Multitasking	multitasking	k1gInSc1	multitasking
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
provozování	provozování	k1gNnSc4	provozování
více	hodně	k6eAd2	hodně
nezávislých	závislý	k2eNgInPc2d1	nezávislý
počítačových	počítačový	k2eAgInPc2d1	počítačový
programů	program	k1gInPc2	program
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
počítači	počítač	k1gInSc6	počítač
<g/>
;	;	kIx,	;
což	což	k3yRnSc1	což
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
provádí	provádět	k5eAaImIp3nS	provádět
úkoly	úkol	k1gInPc4	úkol
současně	současně	k6eAd1	současně
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
počítačů	počítač	k1gMnPc2	počítač
může	moct	k5eAaImIp3nS	moct
dělat	dělat	k5eAaImF	dělat
nejvýše	vysoce	k6eAd3	vysoce
jednu	jeden	k4xCgFnSc4	jeden
nebo	nebo	k8xC	nebo
dvě	dva	k4xCgFnPc4	dva
věci	věc	k1gFnPc4	věc
najednou	najednou	k6eAd1	najednou
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
obvykle	obvykle	k6eAd1	obvykle
prováděno	provádět	k5eAaImNgNnS	provádět
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
sdílení	sdílení	k1gNnSc2	sdílení
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc1	každý
program	program	k1gInSc1	program
používá	používat	k5eAaImIp3nS	používat
ke	k	k7c3	k
spouštění	spouštění	k1gNnSc3	spouštění
část	část	k1gFnSc1	část
"	"	kIx"	"
<g/>
času	čas	k1gInSc2	čas
<g/>
"	"	kIx"	"
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jádro	jádro	k1gNnSc1	jádro
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
plánovací	plánovací	k2eAgInSc1d1	plánovací
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
kolik	kolik	k4yIc4	kolik
času	čas	k1gInSc2	čas
každý	každý	k3xTgInSc1	každý
proces	proces	k1gInSc1	proces
využije	využít	k5eAaPmIp3nS	využít
při	při	k7c6	při
provádění	provádění	k1gNnSc6	provádění
a	a	k8xC	a
v	v	k7c6	v
jakém	jaký	k3yRgNnSc6	jaký
pořadí	pořadí	k1gNnSc6	pořadí
budou	být	k5eAaImBp3nP	být
procesy	proces	k1gInPc1	proces
prováděny	provádět	k5eAaImNgInP	provádět
<g/>
.	.	kIx.	.
</s>
<s>
Řízení	řízení	k1gNnSc1	řízení
předává	předávat	k5eAaImIp3nS	předávat
proces	proces	k1gInSc4	proces
jádru	jádro	k1gNnSc6	jádro
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
programu	program	k1gInSc2	program
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
CPU	CPU	kA	CPU
a	a	k8xC	a
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
kontrola	kontrola	k1gFnSc1	kontrola
vrácena	vrácen	k2eAgFnSc1d1	vrácena
do	do	k7c2	do
jádra	jádro	k1gNnSc2	jádro
pomocí	pomoc	k1gFnPc2	pomoc
nějakého	nějaký	k3yIgInSc2	nějaký
mechanismu	mechanismus	k1gInSc2	mechanismus
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
povoleno	povolen	k2eAgNnSc1d1	povoleno
použít	použít	k5eAaPmF	použít
jiný	jiný	k2eAgInSc4d1	jiný
program	program	k1gInSc4	program
CPU	CPU	kA	CPU
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
tzv.	tzv.	kA	tzv.
Předávání	předávání	k1gNnSc4	předávání
kontroly	kontrola	k1gFnSc2	kontrola
mezi	mezi	k7c7	mezi
jádrem	jádro	k1gNnSc7	jádro
a	a	k8xC	a
aplikacemi	aplikace	k1gFnPc7	aplikace
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kontextový	kontextový	k2eAgInSc1d1	kontextový
přepínač	přepínač	k1gInSc1	přepínač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvotní	prvotní	k2eAgInSc1d1	prvotní
model	model	k1gInSc1	model
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
řídil	řídit	k5eAaImAgInS	řídit
přidělování	přidělování	k1gNnSc4	přidělování
času	čas	k1gInSc2	čas
na	na	k7c4	na
programy	program	k1gInPc4	program
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazýval	nazývat	k5eAaImAgInS	nazývat
kooperativní	kooperativní	k2eAgInSc1d1	kooperativní
multitasking	multitasking	k1gInSc1	multitasking
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
operační	operační	k2eAgInPc1d1	operační
systémy	systém	k1gInPc1	systém
rozšiřují	rozšiřovat	k5eAaImIp3nP	rozšiřovat
koncepty	koncept	k1gInPc7	koncept
předběžné	předběžný	k2eAgFnSc2d1	předběžná
aplikace	aplikace	k1gFnSc2	aplikace
na	na	k7c4	na
ovladače	ovladač	k1gInPc4	ovladač
zařízení	zařízení	k1gNnSc2	zařízení
a	a	k8xC	a
"	"	kIx"	"
<g/>
jádrový	jádrový	k2eAgInSc4d1	jádrový
<g/>
"	"	kIx"	"
kód	kód	k1gInSc4	kód
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
má	mít	k5eAaImIp3nS	mít
předběžnou	předběžný	k2eAgFnSc4d1	předběžná
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
interními	interní	k2eAgFnPc7d1	interní
run-times	runimes	k1gInSc4	run-times
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Filozofií	filozofie	k1gFnSc7	filozofie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
řídí	řídit	k5eAaImIp3nS	řídit
předběžný	předběžný	k2eAgInSc4d1	předběžný
multitasking	multitasking	k1gInSc4	multitasking
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zajistit	zajistit	k5eAaPmF	zajistit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
všechny	všechen	k3xTgInPc1	všechen
programy	program	k1gInPc1	program
byly	být	k5eAaImAgInP	být
na	na	k7c4	na
CPU	CPU	kA	CPU
poskytovány	poskytovat	k5eAaImNgInP	poskytovat
řádně	řádně	k6eAd1	řádně
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgInPc1	všechen
programy	program	k1gInPc1	program
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
omezeny	omezen	k2eAgInPc1d1	omezen
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kolik	kolik	k9	kolik
času	čas	k1gInSc2	čas
mohou	moct	k5eAaImIp3nP	moct
trávit	trávit	k5eAaImF	trávit
na	na	k7c6	na
CPU	CPU	kA	CPU
bez	bez	k7c2	bez
přerušení	přerušení	k1gNnSc2	přerušení
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
využívají	využívat	k5eAaImIp3nP	využívat
moderní	moderní	k2eAgNnPc1d1	moderní
jádra	jádro	k1gNnPc1	jádro
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
časované	časovaný	k2eAgNnSc1d1	časované
přerušení	přerušení	k1gNnSc1	přerušení
<g/>
.	.	kIx.	.
</s>
<s>
Časovač	časovač	k1gInSc1	časovač
chráněného	chráněný	k2eAgInSc2d1	chráněný
režimu	režim	k1gInSc2	režim
je	být	k5eAaImIp3nS	být
nastaven	nastavit	k5eAaPmNgInS	nastavit
jádrem	jádro	k1gNnSc7	jádro
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
po	po	k7c6	po
uplynutí	uplynutí	k1gNnSc6	uplynutí
nastaveného	nastavený	k2eAgInSc2d1	nastavený
času	čas	k1gInSc2	čas
spouští	spouštět	k5eAaImIp3nS	spouštět
návrat	návrat	k1gInSc1	návrat
do	do	k7c2	do
režimu	režim	k1gInSc2	režim
superuživatele	superuživatel	k1gMnSc2	superuživatel
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
uvedené	uvedený	k2eAgFnPc4d1	uvedená
části	část	k1gFnPc4	část
o	o	k7c6	o
přerušení	přerušení	k1gNnSc6	přerušení
a	a	k8xC	a
provozu	provoz	k1gInSc6	provoz
s	s	k7c7	s
duálním	duální	k2eAgInSc7d1	duální
režimem	režim	k1gInSc7	režim
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
operačních	operační	k2eAgInPc6d1	operační
systémech	systém	k1gInPc6	systém
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgMnPc4d1	jednotlivý
uživatele	uživatel	k1gMnPc4	uživatel
je	být	k5eAaImIp3nS	být
kooperativní	kooperativní	k2eAgInSc1d1	kooperativní
multitasking	multitasking	k1gInSc1	multitasking
dokonale	dokonale	k6eAd1	dokonale
adekvátní	adekvátní	k2eAgInSc1d1	adekvátní
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
domácí	domácí	k2eAgInPc1d1	domácí
počítače	počítač	k1gInPc1	počítač
obecně	obecně	k6eAd1	obecně
provozují	provozovat	k5eAaImIp3nP	provozovat
malý	malý	k2eAgInSc4d1	malý
počet	počet	k1gInSc4	počet
dobře	dobře	k6eAd1	dobře
testovaných	testovaný	k2eAgMnPc2d1	testovaný
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
AmigaOS	AmigaOS	k?	AmigaOS
je	být	k5eAaImIp3nS	být
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
předběžný	předběžný	k2eAgInSc4d1	předběžný
multitasking	multitasking	k1gInSc4	multitasking
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
první	první	k4xOgFnSc2	první
verze	verze	k1gFnSc2	verze
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
NT	NT	kA	NT
byla	být	k5eAaImAgFnS	být
první	první	k4xOgInSc4	první
verzí	verze	k1gFnPc2	verze
systému	systém	k1gInSc2	systém
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vynucovala	vynucovat	k5eAaImAgFnS	vynucovat
předběžný	předběžný	k2eAgInSc4d1	předběžný
multitasking	multitasking	k1gInSc4	multitasking
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedosáhla	dosáhnout	k5eNaPmAgFnS	dosáhnout
na	na	k7c4	na
trh	trh	k1gInSc4	trh
s	s	k7c7	s
domácími	domácí	k2eAgMnPc7d1	domácí
uživateli	uživatel	k1gMnPc7	uživatel
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nevyšel	vyjít	k5eNaPmAgMnS	vyjít
Windows	Windows	kA	Windows
XP	XP	kA	XP
(	(	kIx(	(
<g/>
protože	protože	k8xS	protože
Windows	Windows	kA	Windows
NT	NT	kA	NT
byl	být	k5eAaImAgInS	být
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
na	na	k7c4	na
profesionály	profesionál	k1gMnPc4	profesionál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
diskům	disk	k1gInPc3	disk
a	a	k8xC	a
souborovým	souborový	k2eAgInPc3d1	souborový
systémům	systém	k1gInPc3	systém
===	===	k?	===
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
na	na	k7c4	na
<g/>
:	:	kIx,	:
Virtuální	virtuální	k2eAgInSc1d1	virtuální
souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
</s>
</p>
<p>
<s>
Přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
datům	datum	k1gNnPc3	datum
uloženým	uložený	k2eAgFnPc3d1	uložená
na	na	k7c6	na
discích	disk	k1gInPc6	disk
je	být	k5eAaImIp3nS	být
ústředním	ústřední	k2eAgInSc7d1	ústřední
prvkem	prvek	k1gInSc7	prvek
všech	všecek	k3xTgInPc2	všecek
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Počítače	počítač	k1gInPc1	počítač
uchovávají	uchovávat	k5eAaImIp3nP	uchovávat
data	datum	k1gNnPc4	datum
na	na	k7c6	na
discích	disk	k1gInPc6	disk
pomocí	pomocí	k7c2	pomocí
souborů	soubor	k1gInPc2	soubor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
strukturovány	strukturován	k2eAgInPc1d1	strukturován
specifickými	specifický	k2eAgInPc7d1	specifický
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
umožnily	umožnit	k5eAaPmAgFnP	umožnit
rychlejší	rychlý	k2eAgInSc4d2	rychlejší
přístup	přístup	k1gInSc4	přístup
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgFnSc4d2	vyšší
spolehlivost	spolehlivost	k1gFnSc4	spolehlivost
a	a	k8xC	a
lépe	dobře	k6eAd2	dobře
využívaly	využívat	k5eAaImAgFnP	využívat
dostupný	dostupný	k2eAgInSc4d1	dostupný
prostor	prostor	k1gInSc4	prostor
disku	disk	k1gInSc2	disk
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétní	konkrétní	k2eAgInSc1d1	konkrétní
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
jsou	být	k5eAaImIp3nP	být
soubory	soubor	k1gInPc1	soubor
uloženy	uložen	k2eAgInPc1d1	uložen
na	na	k7c6	na
disku	disk	k1gInSc6	disk
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
souborům	soubor	k1gInPc3	soubor
mít	mít	k5eAaImF	mít
jména	jméno	k1gNnPc4	jméno
a	a	k8xC	a
atributy	atribut	k1gInPc4	atribut
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
také	také	k9	také
ukládání	ukládání	k1gNnSc4	ukládání
do	do	k7c2	do
hierarchie	hierarchie	k1gFnSc2	hierarchie
adresářů	adresář	k1gInPc2	adresář
nebo	nebo	k8xC	nebo
složek	složka	k1gFnPc2	složka
uspořádaných	uspořádaný	k2eAgFnPc2d1	uspořádaná
ve	v	k7c6	v
stromovém	stromový	k2eAgInSc6d1	stromový
adresáři	adresář	k1gInSc6	adresář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Včasné	včasný	k2eAgInPc1d1	včasný
operační	operační	k2eAgInPc1d1	operační
systémy	systém	k1gInPc1	systém
obecně	obecně	k6eAd1	obecně
podporují	podporovat	k5eAaImIp3nP	podporovat
jeden	jeden	k4xCgInSc4	jeden
typ	typ	k1gInSc4	typ
diskové	diskový	k2eAgFnSc2d1	disková
jednotky	jednotka	k1gFnSc2	jednotka
a	a	k8xC	a
jediný	jediný	k2eAgInSc1d1	jediný
druh	druh	k1gInSc1	druh
souborového	souborový	k2eAgInSc2d1	souborový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Systémy	systém	k1gInPc1	systém
časných	časný	k2eAgInPc2d1	časný
souborů	soubor	k1gInPc2	soubor
byly	být	k5eAaImAgFnP	být
omezeny	omezit	k5eAaPmNgFnP	omezit
kapacitou	kapacita	k1gFnSc7	kapacita
<g/>
,	,	kIx,	,
rychlostí	rychlost	k1gFnSc7	rychlost
a	a	k8xC	a
druhy	druh	k1gInPc1	druh
názvů	název	k1gInPc2	název
souborů	soubor	k1gInPc2	soubor
a	a	k8xC	a
adresářových	adresářový	k2eAgFnPc2d1	adresářová
struktur	struktura	k1gFnPc2	struktura
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
používat	používat	k5eAaImF	používat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
omezení	omezení	k1gNnPc1	omezení
často	často	k6eAd1	často
odrážejí	odrážet	k5eAaImIp3nP	odrážet
omezení	omezení	k1gNnSc4	omezení
v	v	k7c6	v
operačních	operační	k2eAgInPc6d1	operační
systémech	systém	k1gInPc6	systém
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgFnPc4	který
byly	být	k5eAaImAgFnP	být
navrženy	navrhnout	k5eAaPmNgFnP	navrhnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgNnSc1d1	obtížné
podporovat	podporovat	k5eAaImF	podporovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jeden	jeden	k4xCgInSc1	jeden
souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
mnoho	mnoho	k4c1	mnoho
jednodušších	jednoduchý	k2eAgInPc2d2	jednodušší
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
podporuje	podporovat	k5eAaImIp3nS	podporovat
omezený	omezený	k2eAgInSc1d1	omezený
rozsah	rozsah	k1gInSc1	rozsah
možností	možnost	k1gFnPc2	možnost
pro	pro	k7c4	pro
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
úložným	úložný	k2eAgInPc3d1	úložný
systémům	systém	k1gInPc3	systém
<g/>
,	,	kIx,	,
operační	operační	k2eAgInPc4d1	operační
systémy	systém	k1gInPc4	systém
jako	jako	k8xC	jako
UNIX	Unix	k1gInSc4	Unix
a	a	k8xC	a
Linux	linux	k1gInSc4	linux
podporují	podporovat	k5eAaImIp3nP	podporovat
technologii	technologie	k1gFnSc4	technologie
známou	známá	k1gFnSc4	známá
jako	jako	k8xC	jako
virtuální	virtuální	k2eAgInSc1d1	virtuální
souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
nebo	nebo	k8xC	nebo
VFS	VFS	kA	VFS
<g/>
.	.	kIx.	.
</s>
<s>
Operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
UNIX	Unix	k1gInSc4	Unix
<g/>
,	,	kIx,	,
podporuje	podporovat	k5eAaImIp3nS	podporovat
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
paměťových	paměťový	k2eAgNnPc2d1	paměťové
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
design	design	k1gInSc4	design
nebo	nebo	k8xC	nebo
souborové	souborový	k2eAgInPc4d1	souborový
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jim	on	k3xPp3gMnPc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přístup	přístup	k1gInSc1	přístup
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
společného	společný	k2eAgNnSc2d1	společné
aplikačního	aplikační	k2eAgNnSc2d1	aplikační
programovacího	programovací	k2eAgNnSc2d1	programovací
rozhraní	rozhraní	k1gNnSc2	rozhraní
(	(	kIx(	(
<g/>
API	API	kA	API
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
zbytečné	zbytečný	k2eAgNnSc1d1	zbytečné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
programy	program	k1gInPc1	program
měly	mít	k5eAaImAgInP	mít
nějaké	nějaký	k3yIgFnPc4	nějaký
znalosti	znalost	k1gFnPc4	znalost
o	o	k7c4	o
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yQgNnSc3	který
přistupují	přistupovat	k5eAaImIp3nP	přistupovat
<g/>
.	.	kIx.	.
</s>
<s>
VFS	VFS	kA	VFS
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
operačnímu	operační	k2eAgInSc3d1	operační
systému	systém	k1gInSc3	systém
poskytovat	poskytovat	k5eAaImF	poskytovat
programy	program	k1gInPc7	program
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
neomezenému	omezený	k2eNgInSc3d1	neomezený
počtu	počet	k1gInSc3	počet
zařízení	zařízení	k1gNnSc2	zařízení
s	s	k7c7	s
nekonečnou	konečný	k2eNgFnSc7d1	nekonečná
řadou	řada	k1gFnSc7	řada
souborových	souborový	k2eAgInPc2d1	souborový
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
instalovány	instalován	k2eAgInPc1d1	instalován
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
konkrétních	konkrétní	k2eAgInPc2d1	konkrétní
ovladačů	ovladač	k1gInPc2	ovladač
zařízení	zařízení	k1gNnPc2	zařízení
a	a	k8xC	a
ovladačů	ovladač	k1gInPc2	ovladač
souborového	souborový	k2eAgInSc2d1	souborový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Připojené	připojený	k2eAgNnSc1d1	připojené
úložné	úložný	k2eAgNnSc1d1	úložné
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pevný	pevný	k2eAgInSc4d1	pevný
disk	disk	k1gInSc4	disk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přístupné	přístupný	k2eAgNnSc1d1	přístupné
pomocí	pomocí	k7c2	pomocí
ovladače	ovladač	k1gInSc2	ovladač
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Ovladač	ovladač	k1gInSc1	ovladač
zařízení	zařízení	k1gNnSc1	zařízení
rozumí	rozumět	k5eAaImIp3nS	rozumět
konkrétnímu	konkrétní	k2eAgInSc3d1	konkrétní
jazyku	jazyk	k1gInSc3	jazyk
jednotky	jednotka	k1gFnSc2	jednotka
a	a	k8xC	a
dokáže	dokázat	k5eAaPmIp3nS	dokázat
tento	tento	k3xDgInSc4	tento
jazyk	jazyk	k1gInSc4	jazyk
přeložit	přeložit	k5eAaPmF	přeložit
do	do	k7c2	do
standardního	standardní	k2eAgInSc2d1	standardní
jazyka	jazyk	k1gInSc2	jazyk
používaného	používaný	k2eAgInSc2d1	používaný
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
pro	pro	k7c4	pro
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3	k
všem	všecek	k3xTgFnPc3	všecek
diskovým	diskový	k2eAgFnPc3d1	disková
jednotkám	jednotka	k1gFnPc3	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
systému	systém	k1gInSc6	systém
UNIX	UNIX	kA	UNIX
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jazyk	jazyk	k1gInSc1	jazyk
blokových	blokový	k2eAgNnPc2d1	Blokové
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
jádro	jádro	k1gNnSc4	jádro
správný	správný	k2eAgInSc1d1	správný
ovladač	ovladač	k1gInSc1	ovladač
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
přistupovat	přistupovat	k5eAaImF	přistupovat
k	k	k7c3	k
obsahu	obsah	k1gInSc3	obsah
disku	disk	k1gInSc2	disk
v	v	k7c6	v
syrovém	syrový	k2eAgInSc6d1	syrový
formátu	formát	k1gInSc6	formát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
souborových	souborový	k2eAgInPc2d1	souborový
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Ovladač	ovladač	k1gInSc1	ovladač
souborového	souborový	k2eAgInSc2d1	souborový
systému	systém	k1gInSc2	systém
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
překládání	překládání	k1gNnSc3	překládání
příkazů	příkaz	k1gInPc2	příkaz
používaných	používaný	k2eAgInPc2d1	používaný
pro	pro	k7c4	pro
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3	k
každému	každý	k3xTgInSc3	každý
konkrétnímu	konkrétní	k2eAgInSc3d1	konkrétní
souborovému	souborový	k2eAgInSc3d1	souborový
systému	systém	k1gInSc3	systém
do	do	k7c2	do
standardní	standardní	k2eAgFnSc2d1	standardní
sady	sada	k1gFnSc2	sada
příkazů	příkaz	k1gInPc2	příkaz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
může	moct	k5eAaImIp3nS	moct
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
používat	používat	k5eAaImF	používat
ke	k	k7c3	k
komunikaci	komunikace	k1gFnSc3	komunikace
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
systémy	systém	k1gInPc7	systém
souborů	soubor	k1gInPc2	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Programy	program	k1gInPc1	program
pak	pak	k6eAd1	pak
mohou	moct	k5eAaImIp3nP	moct
zpracovávat	zpracovávat	k5eAaImF	zpracovávat
tyto	tento	k3xDgInPc4	tento
systémy	systém	k1gInPc4	systém
souborů	soubor	k1gInPc2	soubor
na	na	k7c6	na
základě	základ	k1gInSc6	základ
názvů	název	k1gInPc2	název
souborů	soubor	k1gInPc2	soubor
a	a	k8xC	a
adresářů	adresář	k1gInPc2	adresář
/	/	kIx~	/
složek	složka	k1gFnPc2	složka
obsažených	obsažený	k2eAgFnPc2d1	obsažená
v	v	k7c6	v
hierarchické	hierarchický	k2eAgFnSc6d1	hierarchická
struktuře	struktura	k1gFnSc6	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
<g/>
,	,	kIx,	,
odstraňovat	odstraňovat	k5eAaImF	odstraňovat
<g/>
,	,	kIx,	,
otevírat	otevírat	k5eAaImF	otevírat
a	a	k8xC	a
zavírat	zavírat	k5eAaImF	zavírat
soubory	soubor	k1gInPc4	soubor
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
shromažďovat	shromažďovat	k5eAaImF	shromažďovat
různé	různý	k2eAgFnPc1d1	různá
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
nich	on	k3xPp3gMnPc6	on
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
oprávnění	oprávnění	k1gNnSc2	oprávnění
k	k	k7c3	k
přístupu	přístup	k1gInSc3	přístup
<g/>
,	,	kIx,	,
velikosti	velikost	k1gFnPc4	velikost
<g/>
,	,	kIx,	,
volného	volný	k2eAgNnSc2d1	volné
místa	místo	k1gNnSc2	místo
a	a	k8xC	a
údajů	údaj	k1gInPc2	údaj
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
a	a	k8xC	a
úpravách	úprava	k1gFnPc6	úprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Různé	různý	k2eAgInPc1d1	různý
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
souborovými	souborový	k2eAgInPc7d1	souborový
systémy	systém	k1gInPc7	systém
ztěžují	ztěžovat	k5eAaImIp3nP	ztěžovat
podporu	podpora	k1gFnSc4	podpora
všech	všecek	k3xTgMnPc2	všecek
souborových	souborový	k2eAgMnPc2d1	souborový
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Povolené	povolený	k2eAgInPc1d1	povolený
znaky	znak	k1gInPc1	znak
v	v	k7c6	v
názvech	název	k1gInPc6	název
souborů	soubor	k1gInPc2	soubor
<g/>
,	,	kIx,	,
citlivost	citlivost	k1gFnSc1	citlivost
případů	případ	k1gInPc2	případ
a	a	k8xC	a
přítomnost	přítomnost	k1gFnSc1	přítomnost
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
atributů	atribut	k1gInPc2	atribut
souborů	soubor	k1gInPc2	soubor
činí	činit	k5eAaImIp3nP	činit
implementaci	implementace	k1gFnSc4	implementace
jediného	jediný	k2eAgNnSc2d1	jediné
rozhraní	rozhraní	k1gNnSc2	rozhraní
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
souborový	souborový	k2eAgInSc4d1	souborový
systém	systém	k1gInSc4	systém
náročnou	náročný	k2eAgFnSc7d1	náročná
úlohou	úloha	k1gFnSc7	úloha
<g/>
.	.	kIx.	.
</s>
<s>
Operační	operační	k2eAgInPc1d1	operační
systémy	systém	k1gInPc1	systém
obvykle	obvykle	k6eAd1	obvykle
doporučují	doporučovat	k5eAaImIp3nP	doporučovat
používat	používat	k5eAaImF	používat
(	(	kIx(	(
<g/>
a	a	k8xC	a
tak	tak	k6eAd1	tak
podporovat	podporovat	k5eAaImF	podporovat
nativně	nativně	k6eAd1	nativně
<g/>
)	)	kIx)	)
souborové	souborový	k2eAgInPc1d1	souborový
systémy	systém	k1gInPc1	systém
speciálně	speciálně	k6eAd1	speciálně
určené	určený	k2eAgInPc1d1	určený
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
NTFS	NTFS	kA	NTFS
v	v	k7c6	v
systému	systém	k1gInSc6	systém
Windows	Windows	kA	Windows
a	a	k8xC	a
ext	ext	k?	ext
<g/>
3	[number]	k4	3
a	a	k8xC	a
ReiserFS	ReiserFS	k1gMnSc1	ReiserFS
v	v	k7c6	v
Linuxu	linux	k1gInSc6	linux
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
ovladače	ovladač	k1gInPc1	ovladač
třetích	třetí	k4xOgFnPc2	třetí
stran	strana	k1gFnPc2	strana
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
nejpoužívanějších	používaný	k2eAgInPc2d3	nejpoužívanější
souborových	souborový	k2eAgInPc2d1	souborový
systémů	systém	k1gInPc2	systém
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
s	s	k7c7	s
obecným	obecný	k2eAgInSc7d1	obecný
účelem	účel	k1gInSc7	účel
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
NTFS	NTFS	kA	NTFS
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
v	v	k7c6	v
Linuxu	linux	k1gInSc6	linux
přes	přes	k7c4	přes
NTFS-	NTFS-	k1gFnSc4	NTFS-
<g/>
3	[number]	k4	3
<g/>
g	g	kA	g
a	a	k8xC	a
ext	ext	k?	ext
<g/>
2	[number]	k4	2
/	/	kIx~	/
3	[number]	k4	3
a	a	k8xC	a
ReiserFS	ReiserFS	k1gFnPc1	ReiserFS
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
v	v	k7c6	v
systému	systém	k1gInSc6	systém
Windows	Windows	kA	Windows
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
softwaru	software	k1gInSc2	software
třetí	třetí	k4xOgFnSc2	třetí
strany	strana	k1gFnSc2	strana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podpora	podpora	k1gFnSc1	podpora
souborových	souborový	k2eAgInPc2d1	souborový
systémů	systém	k1gInPc2	systém
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
moderními	moderní	k2eAgInPc7d1	moderní
operačními	operační	k2eAgInPc7d1	operační
systémy	systém	k1gInPc7	systém
velmi	velmi	k6eAd1	velmi
rozmanitá	rozmanitý	k2eAgNnPc4d1	rozmanité
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
běžných	běžný	k2eAgInPc2d1	běžný
souborových	souborový	k2eAgInPc2d1	souborový
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc4	všechen
operační	operační	k2eAgInPc4d1	operační
systémy	systém	k1gInPc4	systém
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
ovladače	ovladač	k1gInPc4	ovladač
<g/>
.	.	kIx.	.
</s>
<s>
Operační	operační	k2eAgInPc1d1	operační
systémy	systém	k1gInPc1	systém
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
podpory	podpora	k1gFnSc2	podpora
souborového	souborový	k2eAgInSc2d1	souborový
systému	systém	k1gInSc2	systém
a	a	k8xC	a
formátů	formát	k1gInPc2	formát
disků	disk	k1gInPc2	disk
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgFnPc6	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
nainstalovány	nainstalován	k2eAgInPc1d1	nainstalován
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
systému	systém	k1gInSc6	systém
Windows	Windows	kA	Windows
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgInSc4	každý
systém	systém	k1gInSc4	systém
souborů	soubor	k1gInPc2	soubor
obvykle	obvykle	k6eAd1	obvykle
omezen	omezit	k5eAaPmNgInS	omezit
v	v	k7c6	v
aplikaci	aplikace	k1gFnSc6	aplikace
na	na	k7c4	na
určitá	určitý	k2eAgNnPc4d1	určité
média	médium	k1gNnPc4	médium
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
CD	CD	kA	CD
musí	muset	k5eAaImIp3nS	muset
používat	používat	k5eAaImF	používat
ISO	ISO	kA	ISO
9660	[number]	k4	9660
nebo	nebo	k8xC	nebo
UDF	UDF	kA	UDF
a	a	k8xC	a
od	od	k7c2	od
systému	systém	k1gInSc2	systém
Windows	Windows	kA	Windows
Vista	vista	k2eAgInPc6d1	vista
je	být	k5eAaImIp3nS	být
NTFS	NTFS	kA	NTFS
jediným	jediný	k2eAgInSc7d1	jediný
souborovým	souborový	k2eAgInSc7d1	souborový
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nainstalován	nainstalován	k2eAgInSc1d1	nainstalován
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
instalovat	instalovat	k5eAaBmF	instalovat
Linux	linux	k1gInSc4	linux
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
typů	typ	k1gInPc2	typ
souborových	souborový	k2eAgInPc2d1	souborový
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
Linux	linux	k1gInSc4	linux
a	a	k8xC	a
UNIX	Unix	k1gInSc4	Unix
používat	používat	k5eAaImF	používat
libovolný	libovolný	k2eAgInSc4d1	libovolný
souborový	souborový	k2eAgInSc4d1	souborový
systém	systém	k1gInSc4	systém
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
médium	médium	k1gNnSc4	médium
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
uloženo	uložit	k5eAaPmNgNnS	uložit
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pevný	pevný	k2eAgInSc4d1	pevný
disk	disk	k1gInSc4	disk
<g/>
,	,	kIx,	,
disk	disk	k1gInSc4	disk
(	(	kIx(	(
<g/>
CD	CD	kA	CD
<g/>
,	,	kIx,	,
DVD	DVD	kA	DVD
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USB	USB	kA	USB
flash	flash	k1gInSc1	flash
disk	disk	k1gInSc1	disk
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
souboru	soubor	k1gInSc2	soubor
umístěného	umístěný	k2eAgInSc2d1	umístěný
na	na	k7c6	na
jiném	jiný	k2eAgInSc6d1	jiný
souborovém	souborový	k2eAgInSc6d1	souborový
systému	systém	k1gInSc6	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Ovladače	ovladač	k1gInPc1	ovladač
zařízení	zařízení	k1gNnSc1	zařízení
===	===	k?	===
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
na	na	k7c4	na
<g/>
:	:	kIx,	:
Ovladač	ovladač	k1gInSc4	ovladač
zařízení	zařízení	k1gNnSc2	zařízení
</s>
</p>
<p>
<s>
Ovladač	ovladač	k1gInSc1	ovladač
zařízení	zařízení	k1gNnSc2	zařízení
je	být	k5eAaImIp3nS	být
specifický	specifický	k2eAgInSc1d1	specifický
typ	typ	k1gInSc1	typ
počítačového	počítačový	k2eAgInSc2d1	počítačový
softwaru	software	k1gInSc2	software
výhradně	výhradně	k6eAd1	výhradně
pro	pro	k7c4	pro
umožnění	umožnění	k1gNnSc4	umožnění
interakce	interakce	k1gFnSc2	interakce
s	s	k7c7	s
hardwarovými	hardwarový	k2eAgNnPc7d1	hardwarové
zařízeními	zařízení	k1gNnPc7	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Typicky	typicky	k6eAd1	typicky
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c6	o
rozhraní	rozhraní	k1gNnSc6	rozhraní
pro	pro	k7c4	pro
komunikaci	komunikace	k1gFnSc4	komunikace
se	s	k7c7	s
zařízením	zařízení	k1gNnSc7	zařízení
přes	přes	k7c4	přes
počítačovou	počítačový	k2eAgFnSc4d1	počítačová
sběrnici	sběrnice	k1gFnSc4	sběrnice
nebo	nebo	k8xC	nebo
komunikační	komunikační	k2eAgInSc4d1	komunikační
subsystém	subsystém	k1gInSc4	subsystém
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
podsystém	podsystém	k1gInSc1	podsystém
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgMnSc3	který
je	být	k5eAaImIp3nS	být
hardware	hardware	k1gInSc1	hardware
připojen	připojit	k5eAaPmNgInS	připojit
<g/>
,	,	kIx,	,
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
příkazy	příkaz	k1gInPc4	příkaz
nebo	nebo	k8xC	nebo
přijímá	přijímat	k5eAaImIp3nS	přijímat
data	datum	k1gNnPc4	datum
ze	z	k7c2	z
zařízení	zařízení	k1gNnSc2	zařízení
a	a	k8xC	a
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
konci	konec	k1gInSc6	konec
rozhraní	rozhraní	k1gNnSc2	rozhraní
-	-	kIx~	-
z	z	k7c2	z
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc2	jeho
softwarových	softwarový	k2eAgFnPc2d1	softwarová
aplikací	aplikace	k1gFnPc2	aplikace
(	(	kIx(	(
<g/>
programů	program	k1gInPc2	program
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvotním	prvotní	k2eAgInSc7d1	prvotní
cílem	cíl	k1gInSc7	cíl
návrhu	návrh	k1gInSc2	návrh
ovladačů	ovladač	k1gInPc2	ovladač
zařízení	zařízení	k1gNnSc1	zařízení
je	být	k5eAaImIp3nS	být
abstrakce	abstrakce	k1gFnSc1	abstrakce
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
model	model	k1gInSc1	model
hardwaru	hardware	k1gInSc2	hardware
(	(	kIx(	(
<g/>
i	i	k9	i
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
"	"	kIx"	"
<g/>
stejného	stejný	k2eAgNnSc2d1	stejné
<g/>
"	"	kIx"	"
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgInPc1d2	novější
modely	model	k1gInPc1	model
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
vydávány	vydáván	k2eAgFnPc4d1	vydávána
výrobci	výrobce	k1gMnPc7	výrobce
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
například	například	k6eAd1	například
vyšší	vysoký	k2eAgFnSc4d2	vyšší
spolehlivost	spolehlivost	k1gFnSc4	spolehlivost
nebo	nebo	k8xC	nebo
lepší	dobrý	k2eAgInSc4d2	lepší
výkon	výkon	k1gInSc4	výkon
a	a	k8xC	a
tyto	tento	k3xDgInPc4	tento
novější	nový	k2eAgInPc4d2	novější
modely	model	k1gInPc4	model
mají	mít	k5eAaImIp3nP	mít
často	často	k6eAd1	často
odlišný	odlišný	k2eAgInSc4d1	odlišný
způsob	způsob	k1gInSc4	způsob
ovládání	ovládání	k1gNnSc2	ovládání
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počítačů	počítač	k1gMnPc2	počítač
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc2	jejich
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
nemůžeme	moct	k5eNaImIp1nP	moct
očekávat	očekávat	k5eAaImF	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
ovládat	ovládat	k5eAaImF	ovládat
všechny	všechen	k3xTgFnPc1	všechen
zařízení	zařízení	k1gNnPc1	zařízení
(	(	kIx(	(
<g/>
periferie	periferie	k1gFnSc1	periferie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vyřešení	vyřešení	k1gNnSc3	vyřešení
tohoto	tento	k3xDgInSc2	tento
problému	problém	k1gInSc2	problém
operační	operační	k2eAgInPc4d1	operační
systémy	systém	k1gInPc4	systém
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
"	"	kIx"	"
<g/>
diktují	diktovat	k5eAaImIp3nP	diktovat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
ovládáno	ovládat	k5eAaImNgNnS	ovládat
každé	každý	k3xTgNnSc1	každý
zařízení	zařízení	k1gNnSc1	zařízení
(	(	kIx(	(
<g/>
periferie	periferie	k1gFnSc1	periferie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teoreticky	teoreticky	k6eAd1	teoreticky
by	by	kYmCp3nS	by
nové	nový	k2eAgNnSc1d1	nové
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
řízeno	řídit	k5eAaImNgNnS	řídit
odlišným	odlišný	k2eAgInSc7d1	odlišný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
fungovat	fungovat	k5eAaImF	fungovat
správně	správně	k6eAd1	správně
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
vhodný	vhodný	k2eAgInSc4d1	vhodný
ovladač	ovladač	k1gInSc4	ovladač
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nový	nový	k2eAgInSc1d1	nový
ovladač	ovladač	k1gInSc1	ovladač
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zařízení	zařízení	k1gNnSc1	zařízení
(	(	kIx(	(
<g/>
periferie	periferie	k1gFnSc1	periferie
<g/>
)	)	kIx)	)
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
verzích	verze	k1gFnPc6	verze
systému	systém	k1gInSc2	systém
Windows	Windows	kA	Windows
před	před	k7c7	před
verzí	verze	k1gFnSc7	verze
Vista	vista	k2eAgMnSc2d1	vista
a	a	k8xC	a
u	u	k7c2	u
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
Linux	Linux	kA	Linux
před	před	k7c7	před
verzí	verze	k1gFnSc7	verze
2.6	[number]	k4	2.6
byla	být	k5eAaImAgFnS	být
veškerá	veškerý	k3xTgFnSc1	veškerý
práce	práce	k1gFnSc1	práce
s	s	k7c7	s
ovladači	ovladač	k1gInPc7	ovladač
kooperativní	kooperativní	k2eAgFnSc2d1	kooperativní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Sítě	síť	k1gFnSc2	síť
===	===	k?	===
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
na	na	k7c4	na
<g/>
:	:	kIx,	:
Počítačová	počítačový	k2eAgFnSc1d1	počítačová
síť	síť	k1gFnSc1	síť
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
podporuje	podporovat	k5eAaImIp3nS	podporovat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
síťových	síťový	k2eAgInPc2d1	síťový
protokolů	protokol	k1gInPc2	protokol
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
počítače	počítač	k1gInSc2	počítač
odlišnými	odlišný	k2eAgInPc7d1	odlišný
operačními	operační	k2eAgInPc7d1	operační
systémy	systém	k1gInPc7	systém
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
napojeny	napojit	k5eAaPmNgInP	napojit
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
společnou	společný	k2eAgFnSc4d1	společná
síť	síť	k1gFnSc4	síť
pro	pro	k7c4	pro
sdílení	sdílení	k1gNnSc4	sdílení
periferií	periferie	k1gFnPc2	periferie
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
počítačů	počítač	k1gMnPc2	počítač
<g/>
,	,	kIx,	,
souborů	soubor	k1gInPc2	soubor
<g/>
,	,	kIx,	,
tiskáren	tiskárna	k1gFnPc2	tiskárna
a	a	k8xC	a
skenerů	skener	k1gInPc2	skener
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
pomocí	pomocí	k7c2	pomocí
kabelového	kabelový	k2eAgNnSc2d1	kabelové
nebo	nebo	k8xC	nebo
bezdrátového	bezdrátový	k2eAgNnSc2d1	bezdrátové
připojení	připojení	k1gNnSc2	připojení
<g/>
.	.	kIx.	.
</s>
<s>
Sítě	síť	k1gFnPc1	síť
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
umožnit	umožnit	k5eAaPmF	umožnit
operačnímu	operační	k2eAgInSc3d1	operační
systému	systém	k1gInSc3	systém
počítače	počítač	k1gInSc2	počítač
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
vzdálenému	vzdálený	k2eAgInSc3d1	vzdálený
počítači	počítač	k1gInSc3	počítač
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
vše	všechen	k3xTgNnSc1	všechen
od	od	k7c2	od
jednoduché	jednoduchý	k2eAgFnSc2d1	jednoduchá
komunikace	komunikace	k1gFnSc2	komunikace
až	až	k9	až
po	po	k7c6	po
používání	používání	k1gNnSc6	používání
síťových	síťový	k2eAgInPc2d1	síťový
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
souborů	soubor	k1gInPc2	soubor
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
sdílení	sdílení	k1gNnSc4	sdílení
grafického	grafický	k2eAgNnSc2d1	grafické
nebo	nebo	k8xC	nebo
zvukového	zvukový	k2eAgNnSc2d1	zvukové
vybavení	vybavení	k1gNnSc2	vybavení
jiného	jiný	k2eAgInSc2d1	jiný
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
síťové	síťový	k2eAgFnPc1d1	síťová
služby	služba	k1gFnPc1	služba
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
transparentní	transparentní	k2eAgInSc4d1	transparentní
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
prostředkům	prostředek	k1gInPc3	prostředek
počítače	počítač	k1gInSc2	počítač
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
SSH	SSH	kA	SSH
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
uživatelům	uživatel	k1gMnPc3	uživatel
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
přímý	přímý	k2eAgInSc4d1	přímý
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
rozhraní	rozhraní	k1gNnSc3	rozhraní
formou	forma	k1gFnSc7	forma
příkazového	příkazový	k2eAgInSc2d1	příkazový
řádku	řádek	k1gInSc2	řádek
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Síť	síť	k1gFnSc1	síť
klient	klient	k1gMnSc1	klient
/	/	kIx~	/
server	server	k1gInSc1	server
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
programu	program	k1gInSc3	program
v	v	k7c6	v
počítači	počítač	k1gInSc3	počítač
<g/>
,	,	kIx,	,
připojit	připojit	k5eAaPmF	připojit
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
síť	síť	k1gFnSc4	síť
k	k	k7c3	k
jinému	jiný	k2eAgInSc3d1	jiný
počítači	počítač	k1gInSc3	počítač
nazvanému	nazvaný	k2eAgInSc3d1	nazvaný
server	server	k1gInSc1	server
<g/>
.	.	kIx.	.
</s>
<s>
Servery	server	k1gInPc1	server
nabízejí	nabízet	k5eAaImIp3nP	nabízet
různé	různý	k2eAgFnPc4d1	různá
služby	služba	k1gFnPc4	služba
ostatním	ostatní	k2eAgInPc3d1	ostatní
počítačům	počítač	k1gInPc3	počítač
připojeným	připojený	k2eAgInPc3d1	připojený
přes	přes	k7c4	přes
síť	síť	k1gFnSc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
služby	služba	k1gFnPc1	služba
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
poskytovány	poskytovat	k5eAaImNgFnP	poskytovat
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
portů	port	k1gInPc2	port
nebo	nebo	k8xC	nebo
očíslovaných	očíslovaný	k2eAgInPc2d1	očíslovaný
přístupových	přístupový	k2eAgInPc2d1	přístupový
bodů	bod	k1gInPc2	bod
mimo	mimo	k7c4	mimo
IP	IP	kA	IP
adresu	adresa	k1gFnSc4	adresa
serveru	server	k1gInSc2	server
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
číslo	číslo	k1gNnSc1	číslo
portu	port	k1gInSc2	port
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
přiděleno	přidělit	k5eAaPmNgNnS	přidělit
maximálně	maximálně	k6eAd1	maximálně
jednomu	jeden	k4xCgNnSc3	jeden
běžícímu	běžící	k2eAgInSc3d1	běžící
programu	program	k1gInSc3	program
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
zodpovědný	zodpovědný	k2eAgInSc1d1	zodpovědný
za	za	k7c4	za
zpracování	zpracování	k1gNnSc4	zpracování
požadavků	požadavek	k1gInPc2	požadavek
na	na	k7c6	na
daném	daný	k2eAgInSc6d1	daný
portu	port	k1gInSc6	port
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
podporuje	podporovat	k5eAaImIp3nS	podporovat
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
dodavatelských	dodavatelský	k2eAgInPc2d1	dodavatelský
nebo	nebo	k8xC	nebo
otevřených	otevřený	k2eAgInPc2d1	otevřený
síťových	síťový	k2eAgInPc2d1	síťový
protokolů	protokol	k1gInPc2	protokol
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
SNA	sen	k1gInSc2	sen
v	v	k7c6	v
systémech	systém	k1gInPc6	systém
IBM	IBM	kA	IBM
<g/>
,	,	kIx,	,
DECnet	DECnet	k1gInSc4	DECnet
na	na	k7c6	na
systémech	systém	k1gInPc6	systém
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
Digital	Digital	kA	Digital
Equipment	Equipment	k1gInSc1	Equipment
Corporation	Corporation	k1gInSc1	Corporation
a	a	k8xC	a
protokolů	protokol	k1gInPc2	protokol
specifických	specifický	k2eAgInPc2d1	specifický
pro	pro	k7c4	pro
Microsoft	Microsoft	kA	Microsoft
(	(	kIx(	(
<g/>
SMB	SMB	kA	SMB
<g/>
)	)	kIx)	)
v	v	k7c6	v
systému	systém	k1gInSc6	systém
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
podporovány	podporovat	k5eAaImNgInP	podporovat
také	také	k9	také
specifické	specifický	k2eAgInPc1d1	specifický
protokoly	protokol	k1gInPc1	protokol
pro	pro	k7c4	pro
konkrétní	konkrétní	k2eAgInPc4d1	konkrétní
úkoly	úkol	k1gInPc4	úkol
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
NFS	NFS	kA	NFS
(	(	kIx(	(
<g/>
Network	network	k1gInSc1	network
File	File	k1gNnSc2	File
System	Syst	k1gInSc7	Syst
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
souborům	soubor	k1gInPc3	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Protokoly	protokol	k1gInPc1	protokol
jako	jako	k8xS	jako
ESound	ESound	k1gInSc1	ESound
nebo	nebo	k8xC	nebo
esd	esd	k?	esd
lze	lze	k6eAd1	lze
snadno	snadno	k6eAd1	snadno
nahrát	nahrát	k5eAaBmF	nahrát
na	na	k7c4	na
síť	síť	k1gFnSc4	síť
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
poskytovaly	poskytovat	k5eAaImAgFnP	poskytovat
zvuk	zvuk	k1gInSc4	zvuk
z	z	k7c2	z
lokálních	lokální	k2eAgFnPc2d1	lokální
aplikací	aplikace	k1gFnPc2	aplikace
na	na	k7c6	na
zvukovém	zvukový	k2eAgInSc6d1	zvukový
hardwaru	hardware	k1gInSc6	hardware
vzdáleného	vzdálený	k2eAgNnSc2d1	vzdálené
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
===	===	k?	===
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
na	na	k7c4	na
<g/>
:	:	kIx,	:
Počítačová	počítačový	k2eAgFnSc1d1	počítačová
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
</s>
</p>
<p>
<s>
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
počítače	počítač	k1gInSc2	počítač
<g/>
,	,	kIx,	,
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
správném	správný	k2eAgNnSc6d1	správné
fungování	fungování	k1gNnSc6	fungování
mnoha	mnoho	k4c2	mnoho
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
schopen	schopen	k2eAgMnSc1d1	schopen
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
žádosti	žádost	k1gFnPc4	žádost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
mají	mít	k5eAaImIp3nP	mít
provést	provést	k5eAaPmF	provést
a	a	k8xC	a
žádosti	žádost	k1gFnSc3	žádost
které	který	k3yRgFnSc2	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
zamítnout	zamítnout	k5eAaPmF	zamítnout
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
některé	některý	k3yIgInPc1	některý
systémy	systém	k1gInPc1	systém
mohou	moct	k5eAaImIp3nP	moct
jednoduše	jednoduše	k6eAd1	jednoduše
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
mezi	mezi	k7c7	mezi
"	"	kIx"	"
<g/>
privilegovanými	privilegovaný	k2eAgMnPc7d1	privilegovaný
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
ne	ne	k9	ne
privilegovanými	privilegovaný	k2eAgFnPc7d1	privilegovaná
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
ostatní	ostatní	k2eAgMnPc1d1	ostatní
musí	muset	k5eAaImIp3nP	muset
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
některých	některý	k3yIgMnPc2	některý
údajů	údaj	k1gInPc2	údaj
například	například	k6eAd1	například
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kontrole	kontrola	k1gFnSc3	kontrola
totožnosti	totožnost	k1gFnSc2	totožnost
může	moct	k5eAaImIp3nS	moct
existovat	existovat	k5eAaImF	existovat
proces	proces	k1gInSc4	proces
autentizace	autentizace	k1gFnSc2	autentizace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
probíhá	probíhat	k5eAaImIp3nS	probíhat
proces	proces	k1gInSc1	proces
autentizace	autentizace	k1gFnSc2	autentizace
formou	forma	k1gFnSc7	forma
zadání	zadání	k1gNnSc2	zadání
uživatelského	uživatelský	k2eAgNnSc2d1	Uživatelské
jména	jméno	k1gNnSc2	jméno
popřípadě	popřípadě	k6eAd1	popřípadě
hesla	heslo	k1gNnSc2	heslo
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
možnosti	možnost	k1gFnPc1	možnost
autentizace	autentizace	k1gFnSc2	autentizace
jako	jako	k8xC	jako
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
magnetické	magnetický	k2eAgFnPc1d1	magnetická
karty	karta	k1gFnPc1	karta
nebo	nebo	k8xC	nebo
biometrické	biometrický	k2eAgInPc1d1	biometrický
údaje	údaj	k1gInPc1	údaj
(	(	kIx(	(
<g/>
otisky	otisk	k1gInPc1	otisk
prstů	prst	k1gInPc2	prst
<g/>
,	,	kIx,	,
snímání	snímání	k1gNnSc4	snímání
oční	oční	k2eAgFnSc2d1	oční
duhovky	duhovka	k1gFnSc2	duhovka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
aplikace	aplikace	k1gFnPc1	aplikace
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
omezeny	omezen	k2eAgFnPc1d1	omezena
pro	pro	k7c4	pro
některé	některý	k3yIgMnPc4	některý
uživatele	uživatel	k1gMnPc4	uživatel
i	i	k8xC	i
po	po	k7c4	po
jejich	jejich	k3xOp3gFnSc4	jejich
autentizaci	autentizace	k1gFnSc4	autentizace
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nedostatečných	dostatečný	k2eNgNnPc2d1	nedostatečné
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Externí	externí	k2eAgNnSc1d1	externí
zabezpečení	zabezpečení	k1gNnSc1	zabezpečení
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
žádost	žádost	k1gFnSc4	žádost
mimo	mimo	k7c4	mimo
počítač	počítač	k1gInSc4	počítač
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
přihlášení	přihlášení	k1gNnSc1	přihlášení
na	na	k7c6	na
připojené	připojený	k2eAgFnSc6d1	připojená
konzoli	konzole	k1gFnSc6	konzole
nebo	nebo	k8xC	nebo
nějaké	nějaký	k3yIgNnSc4	nějaký
síťové	síťový	k2eAgNnSc4d1	síťové
připojení	připojení	k1gNnSc4	připojení
<g/>
.	.	kIx.	.
</s>
<s>
Externí	externí	k2eAgInPc1d1	externí
požadavky	požadavek	k1gInPc1	požadavek
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
předávány	předávat	k5eAaImNgFnP	předávat
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
ovladačů	ovladač	k1gInPc2	ovladač
zařízení	zařízení	k1gNnSc2	zařízení
k	k	k7c3	k
jádru	jádro	k1gNnSc3	jádro
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
předávány	předávat	k5eAaImNgFnP	předávat
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
aplikací	aplikace	k1gFnPc2	aplikace
nebo	nebo	k8xC	nebo
prováděny	provádět	k5eAaImNgInP	provádět
přímo	přímo	k6eAd1	přímo
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
dlouho	dlouho	k6eAd1	dlouho
velké	velký	k2eAgNnSc4d1	velké
téma	téma	k1gNnSc4	téma
jelikož	jelikož	k8xS	jelikož
na	na	k7c6	na
počítačích	počítač	k1gInPc6	počítač
uchováváme	uchovávat	k5eAaImIp1nP	uchovávat
i	i	k9	i
dost	dost	k6eAd1	dost
citlivé	citlivý	k2eAgFnPc1d1	citlivá
informace	informace	k1gFnPc1	informace
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
komerční	komerční	k2eAgNnSc1d1	komerční
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
vojenské	vojenský	k2eAgFnPc1d1	vojenská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Síťové	síťový	k2eAgFnPc1d1	síťová
služby	služba	k1gFnPc1	služba
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
nabídky	nabídka	k1gFnPc4	nabídka
jako	jako	k8xS	jako
sdílení	sdílení	k1gNnSc4	sdílení
souborů	soubor	k1gInPc2	soubor
<g/>
,	,	kIx,	,
tiskové	tiskový	k2eAgFnSc2d1	tisková
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
e-mail	eail	k1gInSc4	e-mail
<g/>
,	,	kIx,	,
webové	webový	k2eAgFnPc4d1	webová
stránky	stránka	k1gFnPc4	stránka
a	a	k8xC	a
protokoly	protokol	k1gInPc4	protokol
přenosu	přenos	k1gInSc2	přenos
souborů	soubor	k1gInPc2	soubor
(	(	kIx(	(
<g/>
FTP	FTP	kA	FTP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
těchto	tento	k3xDgFnPc2	tento
služeb	služba	k1gFnPc2	služba
může	moct	k5eAaImIp3nS	moct
znamenat	znamenat	k5eAaImF	znamenat
bezpečnosti	bezpečnost	k1gFnPc4	bezpečnost
"	"	kIx"	"
<g/>
díru	díra	k1gFnSc4	díra
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgNnSc7d1	základní
systémovým	systémový	k2eAgNnSc7d1	systémové
zabezpečením	zabezpečení	k1gNnSc7	zabezpečení
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
firewall	firewall	k1gInSc1	firewall
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
řada	řada	k1gFnSc1	řada
firewallů	firewall	k1gInPc2	firewall
<g/>
,	,	kIx,	,
antivirů	antivir	k1gInPc2	antivir
a	a	k8xC	a
pod	pod	k7c7	pod
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
moderních	moderní	k2eAgInPc2d1	moderní
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
firewall	firewall	k1gInSc1	firewall
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
základním	základní	k2eAgNnSc6d1	základní
nastavení	nastavení	k1gNnSc6	nastavení
PC	PC	kA	PC
zapnut	zapnout	k5eAaPmNgInS	zapnout
<g/>
.	.	kIx.	.
</s>
<s>
Firewall	Firewall	k1gMnSc1	Firewall
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nastaven	nastavit	k5eAaPmNgMnS	nastavit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
blokoval	blokovat	k5eAaImAgInS	blokovat
určitý	určitý	k2eAgInSc1d1	určitý
typ	typ	k1gInSc1	typ
síťové	síťový	k2eAgFnSc2d1	síťová
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
například	například	k6eAd1	například
i	i	k9	i
některé	některý	k3yIgInPc1	některý
softwary	software	k1gInPc1	software
běžící	běžící	k2eAgInPc1d1	běžící
na	na	k7c6	na
operačním	operační	k2eAgInSc6d1	operační
systému	systém	k1gInSc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
instalovat	instalovat	k5eAaBmF	instalovat
a	a	k8xC	a
používat	používat	k5eAaImF	používat
nezabezpečenou	zabezpečený	k2eNgFnSc4d1	nezabezpečená
síťovou	síťový	k2eAgFnSc4d1	síťová
službu	služba	k1gFnSc4	služba
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Telnet	Telnet	k1gInSc1	Telnet
nebo	nebo	k8xC	nebo
FTP	FTP	kA	FTP
a	a	k8xC	a
nemusíme	muset	k5eNaImIp1nP	muset
se	se	k3xPyFc4	se
bát	bát	k5eAaImF	bát
nežádaného	žádaný	k2eNgNnSc2d1	nežádané
narušení	narušení	k1gNnSc2	narušení
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
zapnutá	zapnutý	k2eAgFnSc1d1	zapnutá
brána	brána	k1gFnSc1	brána
firewall	firewalla	k1gFnPc2	firewalla
zablokuje	zablokovat	k5eAaPmIp3nS	zablokovat
všechnu	všechen	k3xTgFnSc4	všechen
nevyžádanou	vyžádaný	k2eNgFnSc4d1	nevyžádaná
komunikaci	komunikace	k1gFnSc4	komunikace
na	na	k7c6	na
daném	daný	k2eAgInSc6d1	daný
portu	port	k1gInSc6	port
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Interní	interní	k2eAgNnSc1d1	interní
zabezpečení	zabezpečení	k1gNnSc1	zabezpečení
v	v	k7c6	v
systémech	systém	k1gInPc6	systém
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
více	hodně	k6eAd2	hodně
uživatelů	uživatel	k1gMnPc2	uživatel
na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
zařízení	zařízení	k1gNnSc6	zařízení
<g/>
,	,	kIx,	,
na	na	k7c4	na
příklad	příklad	k1gInSc4	příklad
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
každému	každý	k3xTgMnSc3	každý
uživateli	uživatel	k1gMnSc3	uživatel
mít	mít	k5eAaImF	mít
soukromé	soukromý	k2eAgInPc4d1	soukromý
soubory	soubor	k1gInPc4	soubor
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgMnPc7	který
ostatní	ostatní	k2eAgMnSc1d1	ostatní
uživatelé	uživatel	k1gMnPc1	uživatel
nemohou	moct	k5eNaImIp3nP	moct
manipulovat	manipulovat	k5eAaImF	manipulovat
<g/>
,	,	kIx,	,
upravovat	upravovat	k5eAaImF	upravovat
nebo	nebo	k8xC	nebo
i	i	k9	i
číst	číst	k5eAaImF	číst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Uživatelské	uživatelský	k2eAgNnSc1d1	Uživatelské
rozhraní	rozhraní	k1gNnSc1	rozhraní
(	(	kIx(	(
<g/>
Shell	Shell	k1gInSc1	Shell
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
na	na	k7c4	na
<g/>
:	:	kIx,	:
Shell	Shell	k1gInSc4	Shell
</s>
</p>
<p>
<s>
Každý	každý	k3xTgInSc1	každý
počítač	počítač	k1gInSc1	počítač
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
ovládán	ovládat	k5eAaImNgInS	ovládat
uživatelem	uživatel	k1gMnSc7	uživatel
<g/>
,	,	kIx,	,
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelské	uživatelský	k2eAgNnSc1d1	Uživatelské
rozhraní	rozhraní	k1gNnSc1	rozhraní
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xC	jako
shell	shell	k1gInSc1	shell
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nezbytné	nezbytný	k2eAgNnSc1d1	nezbytný
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
se	s	k7c7	s
zařízením	zařízení	k1gNnSc7	zařízení
pracovat	pracovat	k5eAaImF	pracovat
uživatel	uživatel	k1gMnSc1	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
nejčastější	častý	k2eAgNnSc1d3	nejčastější
uživatelské	uživatelský	k2eAgNnSc1d1	Uživatelské
rozhraní	rozhraní	k1gNnSc1	rozhraní
jsou	být	k5eAaImIp3nP	být
rozhraní	rozhraní	k1gNnSc1	rozhraní
příkazového	příkazový	k2eAgInSc2d1	příkazový
řádku	řádek	k1gInSc2	řádek
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
není	být	k5eNaImIp3nS	být
skoro	skoro	k6eAd1	skoro
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
napsány	napsán	k2eAgInPc4d1	napsán
příkazy	příkaz	k1gInPc4	příkaz
počítače	počítač	k1gInSc2	počítač
po	po	k7c6	po
řádcích	řádek	k1gInPc6	řádek
a	a	k8xC	a
grafické	grafický	k2eAgNnSc4d1	grafické
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
přítomno	přítomen	k2eAgNnSc1d1	přítomno
vizuální	vizuální	k2eAgNnSc1d1	vizuální
prostředí	prostředí	k1gNnSc1	prostředí
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
WIMP	WIMP	kA	WIMP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
moderních	moderní	k2eAgInPc2d1	moderní
počítačových	počítačový	k2eAgInPc2d1	počítačový
systémů	systém	k1gInPc2	systém
podporuje	podporovat	k5eAaImIp3nS	podporovat
grafické	grafický	k2eAgNnSc4d1	grafické
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
(	(	kIx(	(
<g/>
GUI	GUI	kA	GUI
-	-	kIx~	-
"	"	kIx"	"
<g/>
Graphical	Graphical	k1gFnSc1	Graphical
user	usrat	k5eAaPmRp2nS	usrat
interface	interface	k1gInSc1	interface
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
počítačových	počítačový	k2eAgInPc6d1	počítačový
systémech	systém	k1gInPc6	systém
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
u	u	k7c2	u
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
Mac	Mac	kA	Mac
OS	OS	kA	OS
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
grafické	grafický	k2eAgNnSc1d1	grafické
rozhraní	rozhraní	k1gNnSc1	rozhraní
integrováno	integrovat	k5eAaBmNgNnS	integrovat
do	do	k7c2	do
jádra	jádro	k1gNnSc2	jádro
OS	OS	kA	OS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Grafické	grafický	k2eAgNnSc1d1	grafické
rozhraní	rozhraní	k1gNnSc1	rozhraní
se	se	k3xPyFc4	se
z	z	k7c2	z
technického	technický	k2eAgNnSc2d1	technické
hlediska	hledisko	k1gNnSc2	hledisko
nedá	dát	k5eNaPmIp3nS	dát
bát	bát	k5eAaImF	bát
jako	jako	k9	jako
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
začleněný	začleněný	k2eAgInSc4d1	začleněný
do	do	k7c2	do
podpory	podpora	k1gFnSc2	podpora
jádra	jádro	k1gNnSc2	jádro
OS	osa	k1gFnPc2	osa
který	který	k3yIgInSc4	který
může	moct	k5eAaImIp3nS	moct
umožnit	umožnit	k5eAaPmF	umožnit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
grafické	grafický	k2eAgNnSc1d1	grafické
rozhraní	rozhraní	k1gNnSc1	rozhraní
více	hodně	k6eAd2	hodně
"	"	kIx"	"
<g/>
citlivé	citlivý	k2eAgFnSc3d1	citlivá
<g/>
"	"	kIx"	"
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
sníží	snížit	k5eAaPmIp3nS	snížit
počet	počet	k1gInSc1	počet
kontextových	kontextový	k2eAgInPc2d1	kontextový
přepínačů	přepínač	k1gInPc2	přepínač
(	(	kIx(	(
<g/>
context	context	k2eAgInSc4d1	context
switch	switch	k1gInSc4	switch
<g/>
)	)	kIx)	)
potřebných	potřebný	k2eAgFnPc2d1	potřebná
pro	pro	k7c4	pro
výstupní	výstupní	k2eAgFnPc4d1	výstupní
funkce	funkce	k1gFnPc4	funkce
rozhraní	rozhraní	k1gNnSc2	rozhraní
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
operační	operační	k2eAgInPc1d1	operační
systémy	systém	k1gInPc1	systém
jsou	být	k5eAaImIp3nP	být
modulární	modulární	k2eAgInPc1d1	modulární
<g/>
,	,	kIx,	,
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
grafický	grafický	k2eAgInSc4d1	grafický
subsystém	subsystém	k1gInSc4	subsystém
od	od	k7c2	od
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
UNIX	UNIX	kA	UNIX
<g/>
,	,	kIx,	,
VMS	VMS	kA	VMS
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
měli	mít	k5eAaImAgMnP	mít
operační	operační	k2eAgInPc4d1	operační
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Linux	linux	k1gInSc1	linux
a	a	k8xC	a
macOS	macOS	k?	macOS
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
postaveny	postavit	k5eAaPmNgInP	postavit
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Windows	Windows	kA	Windows
Vista	vista	k2eAgFnSc1d1	vista
<g/>
,	,	kIx,	,
implementují	implementovat	k5eAaImIp3nP	implementovat
grafický	grafický	k2eAgInSc4d1	grafický
subsystém	subsystém	k1gInSc4	subsystém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
podporují	podporovat	k5eAaImIp3nP	podporovat
uživatele	uživatel	k1gMnPc4	uživatel
v	v	k7c6	v
instalovaní	instalovaný	k2eAgMnPc1d1	instalovaný
nebo	nebo	k8xC	nebo
vytvářet	vytvářet	k5eAaImF	vytvářet
uživatelských	uživatelský	k2eAgFnPc2d1	Uživatelská
rozhraní	rozhraní	k1gNnSc4	rozhraní
dle	dle	k7c2	dle
jejich	jejich	k3xOp3gFnPc2	jejich
představ	představa	k1gFnPc2	představa
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
X	X	kA	X
Window	Window	k1gMnPc2	Window
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
GNOME	GNOME	kA	GNOME
nebo	nebo	k8xC	nebo
KDE	kde	k6eAd1	kde
Plasma	plasma	k1gFnSc1	plasma
5	[number]	k4	5
je	být	k5eAaImIp3nS	být
běžně	běžně	k6eAd1	běžně
instalovaným	instalovaný	k2eAgInSc7d1	instalovaný
systémem	systém	k1gInSc7	systém
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
systémů	systém	k1gInPc2	systém
Unix	Unix	k1gInSc1	Unix
(	(	kIx(	(
<g/>
BSD	BSD	kA	BSD
<g/>
,	,	kIx,	,
Linux	Linux	kA	Linux
<g/>
,	,	kIx,	,
Solaris	Solaris	k1gFnSc1	Solaris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
systému	systém	k1gInSc6	systém
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
řada	řada	k1gFnSc1	řada
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
shellu	shello	k1gNnSc6	shello
-	-	kIx~	-
v	v	k7c6	v
systému	systém	k1gInSc6	systém
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nabízejí	nabízet	k5eAaImIp3nP	nabízet
alternativy	alternativa	k1gFnPc1	alternativa
k	k	k7c3	k
zahrnutému	zahrnutý	k2eAgNnSc3d1	zahrnuté
shellu	shello	k1gNnSc3	shello
systému	systém	k1gInSc2	systém
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
samotný	samotný	k2eAgInSc1d1	samotný
shell	shell	k1gInSc1	shell
nelze	lze	k6eNd1	lze
oddělit	oddělit	k5eAaPmF	oddělit
od	od	k7c2	od
systému	systém	k1gInSc2	systém
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Grafické	grafický	k2eAgNnSc4d1	grafické
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
systém	systém	k1gInSc1	systém
Windows	Windows	kA	Windows
změnil	změnit	k5eAaPmAgInS	změnit
své	svůj	k3xOyFgNnSc4	svůj
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
při	při	k7c6	při
vydání	vydání	k1gNnSc6	vydání
nové	nový	k2eAgFnSc2d1	nová
hlavní	hlavní	k2eAgFnSc2d1	hlavní
verze	verze	k1gFnSc2	verze
systému	systém	k1gInSc2	systém
Windows	Windows	kA	Windows
a	a	k8xC	a
grafické	grafický	k2eAgNnSc1d1	grafické
uživatelské	uživatelský	k2eAgNnSc1d1	Uživatelské
rozhraní	rozhraní	k1gNnSc1	rozhraní
Mac	Mac	kA	Mac
OS	OS	kA	OS
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
dramaticky	dramaticky	k6eAd1	dramaticky
změnilo	změnit	k5eAaPmAgNnS	změnit
zavedením	zavedení	k1gNnSc7	zavedení
systému	systém	k1gInSc2	systém
Mac	Mac	kA	Mac
OS	OS	kA	OS
X.	X.	kA	X.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
reálného	reálný	k2eAgInSc2d1	reálný
času	čas	k1gInSc2	čas
==	==	k?	==
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
na	na	k7c4	na
<g/>
:	:	kIx,	:
Operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
reálného	reálný	k2eAgInSc2d1	reálný
času	čas	k1gInSc2	čas
</s>
</p>
<p>
<s>
Operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
reálného	reálný	k2eAgInSc2d1	reálný
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
RTOS	RTOS	kA	RTOS
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
určený	určený	k2eAgInSc1d1	určený
pro	pro	k7c4	pro
aplikace	aplikace	k1gFnPc4	aplikace
s	s	k7c7	s
pevnými	pevný	k2eAgFnPc7d1	pevná
lhůtami	lhůta	k1gFnPc7	lhůta
(	(	kIx(	(
<g/>
výpočty	výpočet	k1gInPc7	výpočet
v	v	k7c6	v
reálném	reálný	k2eAgInSc6d1	reálný
čase	čas	k1gInSc6	čas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
takové	takový	k3xDgFnPc4	takový
aplikace	aplikace	k1gFnPc4	aplikace
patří	patřit	k5eAaImIp3nS	patřit
některé	některý	k3yIgInPc1	některý
malé	malý	k2eAgInPc1d1	malý
vestavěné	vestavěný	k2eAgInPc1d1	vestavěný
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
regulátory	regulátor	k1gInPc1	regulátor
automobilových	automobilový	k2eAgInPc2d1	automobilový
motorů	motor	k1gInPc2	motor
<g/>
,	,	kIx,	,
průmysloví	průmyslovět	k5eAaImIp3nP	průmyslovět
roboti	robot	k1gMnPc1	robot
<g/>
,	,	kIx,	,
kosmické	kosmický	k2eAgFnPc1d1	kosmická
lodě	loď	k1gFnPc1	loď
<g/>
,	,	kIx,	,
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
kontrola	kontrola	k1gFnSc1	kontrola
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
výpočetní	výpočetní	k2eAgInPc1d1	výpočetní
systémy	systém	k1gInPc1	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvním	první	k4xOgInSc7	první
příkladem	příklad	k1gInSc7	příklad
rozsáhlého	rozsáhlý	k2eAgInSc2d1	rozsáhlý
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
reálného	reálný	k2eAgInSc2d1	reálný
času	čas	k1gInSc2	čas
byla	být	k5eAaImAgFnS	být
Transaction	Transaction	k1gInSc4	Transaction
Processing	Processing	k1gInSc1	Processing
Facility	Facilita	k1gFnSc2	Facilita
vyvinutá	vyvinutý	k2eAgFnSc1d1	vyvinutá
společnostmi	společnost	k1gFnPc7	společnost
American	American	k1gMnSc1	American
Airlines	Airlines	k1gMnSc1	Airlines
a	a	k8xC	a
IBM	IBM	kA	IBM
pro	pro	k7c4	pro
Saber	Saber	k1gInSc4	Saber
Airline	Airlin	k1gInSc5	Airlin
Reservations	Reservations	k1gInSc1	Reservations
System	Syst	k1gInSc7	Syst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vestavěné	vestavěný	k2eAgInPc1d1	vestavěný
systémy	systém	k1gInPc1	systém
s	s	k7c7	s
pevnými	pevný	k2eAgInPc7d1	pevný
termíny	termín	k1gInPc7	termín
používají	používat	k5eAaImIp3nP	používat
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
reálného	reálný	k2eAgInSc2d1	reálný
času	čas	k1gInSc2	čas
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
VxWorks	VxWorksa	k1gFnPc2	VxWorksa
<g/>
,	,	kIx,	,
PikeOS	PikeOS	k1gFnPc2	PikeOS
<g/>
,	,	kIx,	,
eCos	eCosa	k1gFnPc2	eCosa
<g/>
,	,	kIx,	,
QNX	QNX	kA	QNX
<g/>
,	,	kIx,	,
MontaVista	MontaVista	k1gMnSc1	MontaVista
Linux	linux	k1gInSc1	linux
a	a	k8xC	a
RTLinux	RTLinux	k1gInSc1	RTLinux
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
CE	CE	kA	CE
je	být	k5eAaImIp3nS	být
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
pracující	pracující	k1gFnSc2	pracující
v	v	k7c6	v
reálném	reálný	k2eAgInSc6d1	reálný
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Symbian	Symbian	k1gInSc1	Symbian
OS	OS	kA	OS
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
RTOS	RTOS	kA	RTOS
jádro	jádro	k1gNnSc4	jádro
(	(	kIx(	(
<g/>
EKA	EKA	kA	EKA
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
od	od	k7c2	od
verze	verze	k1gFnSc2	verze
8.0	[number]	k4	8.0
<g/>
b.	b.	k?	b.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
vestavěné	vestavěný	k2eAgInPc1d1	vestavěný
systémy	systém	k1gInPc1	systém
používají	používat	k5eAaImIp3nP	používat
operační	operační	k2eAgInPc1d1	operační
systémy	systém	k1gInPc1	systém
jako	jako	k8xS	jako
Palm	Palm	k1gInSc1	Palm
OS	OS	kA	OS
<g/>
,	,	kIx,	,
BSD	BSD	kA	BSD
a	a	k8xC	a
Linux	Linux	kA	Linux
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
tyto	tento	k3xDgInPc1	tento
operační	operační	k2eAgInPc1d1	operační
systémy	systém	k1gInPc1	systém
nepodporují	podporovat	k5eNaImIp3nP	podporovat
výpočet	výpočet	k1gInSc4	výpočet
v	v	k7c6	v
reálném	reálný	k2eAgInSc6d1	reálný
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
jako	jako	k8xC	jako
hobby	hobby	k1gNnSc4	hobby
==	==	k?	==
</s>
</p>
<p>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejkomplikovanějších	komplikovaný	k2eAgFnPc2d3	nejkomplikovanější
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
počítačový	počítačový	k2eAgMnSc1d1	počítačový
fanoušek	fanoušek	k1gMnSc1	fanoušek
zapojit	zapojit	k5eAaPmF	zapojit
<g/>
.	.	kIx.	.
</s>
<s>
Takovýto	takovýto	k3xDgInSc1	takovýto
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
klasifikován	klasifikovat	k5eAaImNgMnS	klasifikovat
jako	jako	k9	jako
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
kód	kód	k1gInSc4	kód
nebyl	být	k5eNaImAgMnS	být
přímo	přímo	k6eAd1	přímo
odvozený	odvozený	k2eAgMnSc1d1	odvozený
od	od	k7c2	od
existujícího	existující	k2eAgInSc2d1	existující
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
málo	málo	k4c1	málo
uživatelů	uživatel	k1gMnPc2	uživatel
a	a	k8xC	a
aktivních	aktivní	k2eAgMnPc2d1	aktivní
vývojářů	vývojář	k1gMnPc2	vývojář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
hobby	hobby	k1gNnSc4	hobby
vývoj	vývoj	k1gInSc1	vývoj
<g/>
"	"	kIx"	"
považován	považován	k2eAgInSc1d1	považován
jako	jako	k8xS	jako
podpora	podpora	k1gFnSc1	podpora
podomácku	podomácku	k6eAd1	podomácku
sestaveného	sestavený	k2eAgInSc2d1	sestavený
počítače	počítač	k1gInSc2	počítač
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
jednodeskový	jednodeskový	k2eAgInSc4d1	jednodeskový
počítač	počítač	k1gInSc4	počítač
poháněný	poháněný	k2eAgInSc4d1	poháněný
mikroprocesorem	mikroprocesor	k1gInSc7	mikroprocesor
6502	[number]	k4	6502
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vývoj	vývoj	k1gInSc1	vývoj
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
široké	široký	k2eAgNnSc4d1	široké
využití	využití	k1gNnSc4	využití
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
architektury	architektura	k1gFnPc4	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
může	moct	k5eAaImIp3nS	moct
pocházet	pocházet	k5eAaImF	pocházet
ze	z	k7c2	z
zcela	zcela	k6eAd1	zcela
nových	nový	k2eAgInPc2d1	nový
nápadů	nápad	k1gInPc2	nápad
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
upravováním	upravování	k1gNnSc7	upravování
stávajícího	stávající	k2eAgInSc2d1	stávající
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
fanoušek	fanoušek	k1gMnSc1	fanoušek
jeho	jeho	k3xOp3gMnSc7	jeho
vlastním	vlastní	k2eAgMnSc7d1	vlastní
vývojářem	vývojář	k1gMnSc7	vývojář
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
<g/>
,	,	kIx,	,
neorganizovanou	organizovaný	k2eNgFnSc7d1	neorganizovaná
skupinou	skupina	k1gFnSc7	skupina
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
podobné	podobný	k2eAgInPc4d1	podobný
zájmy	zájem	k1gInPc4	zájem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
příklady	příklad	k1gInPc4	příklad
takového	takový	k3xDgInSc2	takový
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
patří	patřit	k5eAaImIp3nS	patřit
Syllable	Syllable	k1gFnSc1	Syllable
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
a	a	k8xC	a
přenositelnost	přenositelnost	k1gFnSc1	přenositelnost
==	==	k?	==
</s>
</p>
<p>
<s>
Aplikační	aplikační	k2eAgInSc1d1	aplikační
software	software	k1gInSc1	software
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
psán	psán	k2eAgInSc1d1	psán
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
konkrétním	konkrétní	k2eAgInSc6d1	konkrétní
operačním	operační	k2eAgInSc6d1	operační
systému	systém	k1gInSc6	systém
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
dokonce	dokonce	k9	dokonce
i	i	k9	i
pro	pro	k7c4	pro
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
hardware	hardware	k1gInSc4	hardware
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přenosu	přenos	k1gInSc6	přenos
aplikace	aplikace	k1gFnSc2	aplikace
na	na	k7c4	na
jiný	jiný	k2eAgInSc4d1	jiný
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
funkce	funkce	k1gFnPc4	funkce
vyžadované	vyžadovaný	k2eAgFnPc4d1	vyžadovaná
touto	tento	k3xDgFnSc7	tento
aplikací	aplikace	k1gFnPc2	aplikace
implementovány	implementovat	k5eAaImNgInP	implementovat
odlišně	odlišně	k6eAd1	odlišně
tímto	tento	k3xDgInSc7	tento
systémem	systém	k1gInSc7	systém
(	(	kIx(	(
<g/>
názvy	název	k1gInPc1	název
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
význam	význam	k1gInSc1	význam
argumentů	argument	k1gInPc2	argument
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Které	který	k3yIgFnPc1	který
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
úpravu	úprava	k1gFnSc4	úprava
<g/>
,	,	kIx,	,
změnu	změna	k1gFnSc4	změna
nebo	nebo	k8xC	nebo
jinou	jiný	k2eAgFnSc4d1	jiná
údržbu	údržba	k1gFnSc4	údržba
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Unix	Unix	k1gInSc1	Unix
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc4	první
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nebyl	být	k5eNaImAgInS	být
napsán	napsat	k5eAaPmNgInS	napsat
v	v	k7c6	v
assemblerovém	assemblerový	k2eAgInSc6d1	assemblerový
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
přenosný	přenosný	k2eAgMnSc1d1	přenosný
pro	pro	k7c4	pro
systémy	systém	k1gInPc4	systém
odlišné	odlišný	k2eAgNnSc1d1	odlišné
od	od	k7c2	od
jeho	on	k3xPp3gInSc2	on
původního	původní	k2eAgInSc2d1	původní
PDP-	PDP-	k1gFnPc7	PDP-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
cena	cena	k1gFnSc1	cena
při	při	k7c6	při
podpoře	podpora	k1gFnSc6	podpora
rozmanitosti	rozmanitost	k1gFnSc2	rozmanitost
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
aplikace	aplikace	k1gFnPc1	aplikace
namísto	namísto	k7c2	namísto
zápisu	zápis	k1gInSc2	zápis
do	do	k7c2	do
softwarových	softwarový	k2eAgFnPc2d1	softwarová
platforem	platforma	k1gFnPc2	platforma
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Java	Java	k1gFnSc1	Java
nebo	nebo	k8xC	nebo
Qt	Qt	k1gFnSc1	Qt
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
abstrakce	abstrakce	k1gFnPc1	abstrakce
již	již	k6eAd1	již
vznášely	vznášet	k5eAaImAgFnP	vznášet
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
přizpůsobení	přizpůsobení	k1gNnSc4	přizpůsobení
konkrétním	konkrétní	k2eAgInPc3d1	konkrétní
operačním	operační	k2eAgInPc3d1	operační
systémům	systém	k1gInPc3	systém
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc3	jejich
systémovým	systémový	k2eAgFnPc3d1	systémová
knihovnám	knihovna	k1gFnPc3	knihovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
přístupem	přístup	k1gInSc7	přístup
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
dodavatelé	dodavatel	k1gMnPc1	dodavatel
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
přijali	přijmout	k5eAaPmAgMnP	přijmout
standardy	standard	k1gInPc4	standard
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
vrstvy	vrstva	k1gFnSc2	vrstva
abstrakce	abstrakce	k1gFnSc2	abstrakce
POSIX	POSIX	kA	POSIX
a	a	k8xC	a
OS	OS	kA	OS
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
společné	společný	k2eAgFnPc4d1	společná
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
snižují	snižovat	k5eAaImIp3nP	snižovat
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
přenos	přenos	k1gInSc4	přenos
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Stavba	stavba	k1gFnSc1	stavba
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
(	(	kIx(	(
<g/>
též	též	k9	též
označovaného	označovaný	k2eAgMnSc2d1	označovaný
jako	jako	k8xS	jako
kernel	kernel	k1gInSc1	kernel
<g/>
)	)	kIx)	)
a	a	k8xC	a
pomocných	pomocný	k2eAgInPc2d1	pomocný
systémových	systémový	k2eAgInPc2d1	systémový
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
kamenem	kámen	k1gInSc7	kámen
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Zavádí	zavádět	k5eAaImIp3nS	zavádět
se	se	k3xPyFc4	se
do	do	k7c2	do
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
počítače	počítač	k1gInSc2	počítač
při	při	k7c6	při
startu	start	k1gInSc6	start
a	a	k8xC	a
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
v	v	k7c6	v
činnosti	činnost	k1gFnSc6	činnost
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
běhu	běh	k1gInSc2	běh
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
naprogramováno	naprogramovat	k5eAaPmNgNnS	naprogramovat
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
a	a	k8xC	a
podle	podle	k7c2	podle
toho	ten	k3xDgInSc2	ten
rozeznáváme	rozeznávat	k5eAaImIp1nP	rozeznávat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
monolitické	monolitický	k2eAgNnSc1d1	monolitické
jádro	jádro	k1gNnSc1	jádro
–	–	k?	–
jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
funkčním	funkční	k2eAgInSc7d1	funkční
celkem	celek	k1gInSc7	celek
</s>
</p>
<p>
<s>
mikrojádro	mikrojádro	k6eAd1	mikrojádro
–	–	k?	–
jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgFnPc1d1	malá
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
oddělitelné	oddělitelný	k2eAgFnPc1d1	oddělitelná
části	část	k1gFnPc1	část
pracují	pracovat	k5eAaImIp3nP	pracovat
samostatně	samostatně	k6eAd1	samostatně
jako	jako	k8xC	jako
běžné	běžný	k2eAgInPc1d1	běžný
procesy	proces	k1gInPc1	proces
</s>
</p>
<p>
<s>
hybridní	hybridní	k2eAgNnSc1d1	hybridní
jádro	jádro	k1gNnSc1	jádro
–	–	k?	–
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
monolitického	monolitický	k2eAgNnSc2d1	monolitické
jádra	jádro	k1gNnSc2	jádro
i	i	k8xC	i
mikrojádra	mikrojádra	k1gFnSc1	mikrojádra
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Grafické	grafický	k2eAgNnSc4d1	grafické
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
===	===	k?	===
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
již	již	k9	již
většina	většina	k1gFnSc1	většina
moderních	moderní	k2eAgInPc2d1	moderní
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
grafické	grafický	k2eAgNnSc4d1	grafické
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
systémech	systém	k1gInPc6	systém
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
integrované	integrovaný	k2eAgNnSc1d1	integrované
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
systému	systém	k1gInSc2	systém
–	–	k?	–
například	například	k6eAd1	například
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
implementaci	implementace	k1gFnSc6	implementace
MS	MS	kA	MS
Windows	Windows	kA	Windows
a	a	k8xC	a
Mac	Mac	kA	Mac
OS	OS	kA	OS
byl	být	k5eAaImAgInS	být
grafický	grafický	k2eAgInSc1d1	grafický
podsystém	podsystém	k1gInSc1	podsystém
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
částí	část	k1gFnPc2	část
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
operační	operační	k2eAgInPc1d1	operační
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
starší	starý	k2eAgInPc1d2	starší
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
novější	nový	k2eAgMnPc1d2	novější
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
modulární	modulární	k2eAgInSc4d1	modulární
–	–	k?	–
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
grafický	grafický	k2eAgInSc4d1	grafický
podsystém	podsystém	k1gInSc4	podsystém
od	od	k7c2	od
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
existovaly	existovat	k5eAaImAgInP	existovat
systémy	systém	k1gInPc1	systém
UNIX	UNIX	kA	UNIX
<g/>
,	,	kIx,	,
VMS	VMS	kA	VMS
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
jiných	jiný	k2eAgMnPc2d1	jiný
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
byly	být	k5eAaImAgFnP	být
vybudovány	vybudovat	k5eAaPmNgInP	vybudovat
právě	právě	k9	právě
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
principu	princip	k1gInSc6	princip
funguje	fungovat	k5eAaImIp3nS	fungovat
také	také	k9	také
Linux	linux	k1gInSc1	linux
a	a	k8xC	a
macOS	macOS	k?	macOS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
OS	osa	k1gFnPc2	osa
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
uživateli	uživatel	k1gMnSc3	uživatel
nainstalovat	nainstalovat	k5eAaPmF	nainstalovat
nebo	nebo	k8xC	nebo
vytvořit	vytvořit	k5eAaPmF	vytvořit
grafické	grafický	k2eAgNnSc4d1	grafické
rozhraní	rozhraní	k1gNnSc4	rozhraní
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gInPc2	jeho
požadavků	požadavek	k1gInPc2	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
X	X	kA	X
Window	Window	k1gFnSc1	Window
System	Syst	k1gInSc7	Syst
<g/>
,	,	kIx,	,
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
GNOME	GNOME	kA	GNOME
nebo	nebo	k8xC	nebo
KDE	kde	k6eAd1	kde
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
běžně	běžně	k6eAd1	běžně
dostupný	dostupný	k2eAgInSc1d1	dostupný
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
UN	UN	kA	UN
<g/>
*	*	kIx~	*
<g/>
Xových	Xových	k2eAgInPc2d1	Xových
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Mnohá	mnohé	k1gNnPc1	mnohé
na	na	k7c6	na
Unixu	Unix	k1gInSc6	Unix
založená	založený	k2eAgFnSc1d1	založená
grafická	grafický	k2eAgFnSc1d1	grafická
uživatelská	uživatelský	k2eAgFnSc1d1	Uživatelská
rozhraní	rozhraní	k1gNnSc1	rozhraní
existují	existovat	k5eAaImIp3nP	existovat
již	již	k6eAd1	již
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
jsou	být	k5eAaImIp3nP	být
zděděná	zděděný	k2eAgNnPc1d1	zděděné
od	od	k7c2	od
X	X	kA	X
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Soutěžení	soutěžení	k1gNnPc1	soutěžení
mezi	mezi	k7c7	mezi
různými	různý	k2eAgMnPc7d1	různý
prodejci	prodejce	k1gMnPc7	prodejce
Unixu	Unix	k1gInSc2	Unix
(	(	kIx(	(
<g/>
HP	HP	kA	HP
<g/>
,	,	kIx,	,
IBM	IBM	kA	IBM
<g/>
,	,	kIx,	,
Sun	Sun	kA	Sun
<g/>
)	)	kIx)	)
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
mnoha	mnoho	k4c3	mnoho
rozdílům	rozdíl	k1gInPc3	rozdíl
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
selhání	selhání	k1gNnSc4	selhání
snahy	snaha	k1gFnSc2	snaha
o	o	k7c6	o
standardizaci	standardizace	k1gFnSc6	standardizace
podle	podle	k7c2	podle
COSE	COSE	kA	COSE
a	a	k8xC	a
CDE	CDE	kA	CDE
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Grafická	grafický	k2eAgNnPc1d1	grafické
uživatelská	uživatelský	k2eAgNnPc1d1	Uživatelské
rozhraní	rozhraní	k1gNnPc1	rozhraní
se	se	k3xPyFc4	se
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Windows	Windows	kA	Windows
modifikuje	modifikovat	k5eAaBmIp3nS	modifikovat
své	své	k1gNnSc4	své
GUI	GUI	kA	GUI
vždy	vždy	k6eAd1	vždy
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
vydána	vydán	k2eAgFnSc1d1	vydána
nová	nový	k2eAgFnSc1d1	nová
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
a	a	k8xC	a
rozhraní	rozhraní	k1gNnSc1	rozhraní
Mac	Mac	kA	Mac
OS	OS	kA	OS
bylo	být	k5eAaImAgNnS	být
dramaticky	dramaticky	k6eAd1	dramaticky
změněno	změnit	k5eAaPmNgNnS	změnit
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
Mac	Mac	kA	Mac
OS	OS	kA	OS
X	X	kA	X
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Vymezení	vymezení	k1gNnSc6	vymezení
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
obvykle	obvykle	k6eAd1	obvykle
zahrnujeme	zahrnovat	k5eAaImIp1nP	zahrnovat
i	i	k9	i
základní	základní	k2eAgInPc4d1	základní
systémové	systémový	k2eAgInPc4d1	systémový
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
ke	k	k7c3	k
správě	správa	k1gFnSc3	správa
počítače	počítač	k1gInSc2	počítač
(	(	kIx(	(
<g/>
formátování	formátování	k1gNnSc4	formátování
disků	disk	k1gInPc2	disk
<g/>
,	,	kIx,	,
kontrola	kontrola	k1gFnSc1	kontrola
integrity	integrita	k1gFnSc2	integrita
souborového	souborový	k2eAgInSc2d1	souborový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
nastavení	nastavení	k1gNnSc4	nastavení
systémového	systémový	k2eAgInSc2d1	systémový
času	čas	k1gInSc2	čas
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
doplňující	doplňující	k2eAgFnPc1d1	doplňující
aplikace	aplikace	k1gFnPc1	aplikace
se	se	k3xPyFc4	se
však	však	k9	však
těmto	tento	k3xDgInPc3	tento
nástrojům	nástroj	k1gInPc3	nástroj
velmi	velmi	k6eAd1	velmi
blíží	blížit	k5eAaImIp3nS	blížit
nebo	nebo	k8xC	nebo
je	on	k3xPp3gFnPc4	on
dokonce	dokonce	k9	dokonce
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
součástí	součást	k1gFnSc7	součást
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
není	být	k5eNaImIp3nS	být
diagnostika	diagnostika	k1gFnSc1	diagnostika
pevných	pevný	k2eAgInPc2d1	pevný
disků	disk	k1gInPc2	disk
<g/>
,	,	kIx,	,
detailní	detailní	k2eAgInSc1d1	detailní
nástroj	nástroj	k1gInSc1	nástroj
na	na	k7c4	na
sledování	sledování	k1gNnSc4	sledování
procesů	proces	k1gInPc2	proces
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
interních	interní	k2eAgInPc2d1	interní
pochodů	pochod	k1gInPc2	pochod
v	v	k7c6	v
systému	systém	k1gInSc6	systém
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
není	být	k5eNaImIp3nS	být
vždy	vždy	k6eAd1	vždy
možné	možný	k2eAgInPc4d1	možný
systémové	systémový	k2eAgInPc4d1	systémový
nástroje	nástroj	k1gInPc4	nástroj
a	a	k8xC	a
aplikace	aplikace	k1gFnPc4	aplikace
jednoznačně	jednoznačně	k6eAd1	jednoznačně
rozlišit	rozlišit	k5eAaPmF	rozlišit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aplikace	aplikace	k1gFnSc1	aplikace
a	a	k8xC	a
jádro	jádro	k1gNnSc1	jádro
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
můžeme	moct	k5eAaImIp1nP	moct
rozlišit	rozlišit	k5eAaPmF	rozlišit
podle	podle	k7c2	podle
výše	výše	k1gFnSc2	výše
uvedených	uvedený	k2eAgFnPc2d1	uvedená
základních	základní	k2eAgFnPc2d1	základní
funkcí	funkce	k1gFnPc2	funkce
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
je	být	k5eAaImIp3nS	být
daný	daný	k2eAgInSc4d1	daný
spuštěný	spuštěný	k2eAgInSc4d1	spuštěný
proces	proces	k1gInSc4	proces
zpracováván	zpracováván	k2eAgInSc4d1	zpracováván
v	v	k7c6	v
uživatelském	uživatelský	k2eAgInSc6d1	uživatelský
nebo	nebo	k8xC	nebo
jaderném	jaderný	k2eAgInSc6d1	jaderný
režimu	režim	k1gInSc6	režim
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
privilegovaný	privilegovaný	k2eAgInSc1d1	privilegovaný
režim	režim	k1gInSc1	režim
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
s	s	k7c7	s
monolitickým	monolitický	k2eAgNnSc7d1	monolitické
jádrem	jádro	k1gNnSc7	jádro
(	(	kIx(	(
<g/>
např.	např.	kA	např.
unixové	unixový	k2eAgInPc1d1	unixový
systémy	systém	k1gInPc1	systém
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jasná	jasný	k2eAgFnSc1d1	jasná
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
systémovým	systémový	k2eAgNnSc7d1	systémové
voláním	volání	k1gNnSc7	volání
<g/>
,	,	kIx,	,
knihovnami	knihovna	k1gFnPc7	knihovna
a	a	k8xC	a
procesy	proces	k1gInPc7	proces
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
nich	on	k3xPp3gMnPc6	on
typická	typický	k2eAgFnSc1d1	typická
součást	součást	k1gFnSc1	součást
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
</s>
</p>
<p>
<s>
systémy	systém	k1gInPc1	systém
s	s	k7c7	s
mikrojádrem	mikrojádr	k1gInSc7	mikrojádr
(	(	kIx(	(
<g/>
např.	např.	kA	např.
systémy	systém	k1gInPc1	systém
Windows	Windows	kA	Windows
NT	NT	kA	NT
<g/>
)	)	kIx)	)
tuto	tento	k3xDgFnSc4	tento
hranici	hranice	k1gFnSc4	hranice
jasnou	jasný	k2eAgFnSc4d1	jasná
nemají	mít	k5eNaImIp3nP	mít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
výše	vysoce	k6eAd2	vysoce
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
obsluha	obsluha	k1gFnSc1	obsluha
souborového	souborový	k2eAgInSc2d1	souborový
systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
realizována	realizován	k2eAgFnSc1d1	realizována
jako	jako	k8xS	jako
samostatný	samostatný	k2eAgInSc1d1	samostatný
proces	proces	k1gInSc1	proces
v	v	k7c6	v
uživatelském	uživatelský	k2eAgInSc6d1	uživatelský
prostoru	prostor	k1gInSc6	prostor
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
serverem	server	k1gInSc7	server
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Windows	Windows	kA	Windows
API	API	kA	API
slučuje	slučovat	k5eAaImIp3nS	slučovat
systémová	systémový	k2eAgNnPc4d1	systémové
volání	volání	k1gNnPc4	volání
<g/>
,	,	kIx,	,
ovládání	ovládání	k1gNnSc4	ovládání
uživatelského	uživatelský	k2eAgNnSc2d1	Uživatelské
rozhraní	rozhraní	k1gNnSc2	rozhraní
i	i	k8xC	i
různé	různý	k2eAgFnSc2d1	různá
knihovní	knihovní	k2eAgFnSc2d1	knihovní
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
je	být	k5eAaImIp3nS	být
knihovní	knihovní	k2eAgFnSc1d1	knihovní
funkce	funkce	k1gFnSc1	funkce
a	a	k8xC	a
co	co	k3yInSc4	co
je	být	k5eAaImIp3nS	být
obdoba	obdoba	k1gFnSc1	obdoba
systémového	systémový	k2eAgNnSc2d1	systémové
volání	volání	k1gNnSc2	volání
monolitického	monolitický	k2eAgNnSc2d1	monolitické
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlastní	vlastní	k2eAgNnSc4d1	vlastní
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
počítače	počítač	k1gInSc2	počítač
(	(	kIx(	(
<g/>
příkazový	příkazový	k2eAgInSc1d1	příkazový
řádek	řádek	k1gInSc1	řádek
<g/>
,	,	kIx,	,
textové	textový	k2eAgNnSc4d1	textové
nebo	nebo	k8xC	nebo
grafické	grafický	k2eAgNnSc4d1	grafické
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
do	do	k7c2	do
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
zahrnováno	zahrnován	k2eAgNnSc1d1	zahrnováno
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
kvůli	kvůli	k7c3	kvůli
zvýšení	zvýšení	k1gNnSc3	zvýšení
výkonu	výkon	k1gInSc6	výkon
některé	některý	k3yIgInPc4	některý
typicky	typicky	k6eAd1	typicky
aplikační	aplikační	k2eAgInPc4d1	aplikační
úkoly	úkol	k1gInPc4	úkol
přenést	přenést	k5eAaPmF	přenést
do	do	k7c2	do
jádra	jádro	k1gNnSc2	jádro
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
webový	webový	k2eAgInSc1d1	webový
server	server	k1gInSc1	server
<g/>
,	,	kIx,	,
grafické	grafický	k2eAgNnSc4d1	grafické
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
,	,	kIx,	,
akcelerované	akcelerovaný	k2eAgFnPc4d1	akcelerovaná
funkce	funkce	k1gFnPc4	funkce
grafických	grafický	k2eAgFnPc2d1	grafická
karet	kareta	k1gFnPc2	kareta
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Tržní	tržní	k2eAgInSc1d1	tržní
podíl	podíl	k1gInSc1	podíl
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
na	na	k7c6	na
stolních	stolní	k2eAgInPc6d1	stolní
počítačích	počítač	k1gInPc6	počítač
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Operating	Operating	k1gInSc1	Operating
system	syst	k1gInSc7	syst
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Mobilní	mobilní	k2eAgInSc1d1	mobilní
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
</s>
</p>
