<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
základní	základní	k2eAgNnSc1d1	základní
programové	programový	k2eAgNnSc1d1	programové
vybavení	vybavení	k1gNnSc1	vybavení
počítače	počítač	k1gInSc2	počítač
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
startu	start	k1gInSc6	start
<g/>
?	?	kIx.	?
</s>
