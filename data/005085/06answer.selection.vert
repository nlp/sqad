<s>
Operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
označení	označení	k1gNnSc2	označení
pro	pro	k7c4	pro
základní	základní	k2eAgNnSc4d1	základní
programové	programový	k2eAgNnSc4d1	programové
vybavení	vybavení	k1gNnSc4	vybavení
počítače	počítač	k1gInSc2	počítač
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
software	software	k1gInSc1	software
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
je	být	k5eAaImIp3nS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
počítače	počítač	k1gInSc2	počítač
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
startu	start	k1gInSc6	start
a	a	k8xC	a
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
v	v	k7c6	v
činnosti	činnost	k1gFnSc6	činnost
až	až	k9	až
do	do	k7c2	do
jeho	jeho	k3xOp3gNnSc2	jeho
vypnutí	vypnutí	k1gNnSc2	vypnutí
<g/>
.	.	kIx.	.
</s>
