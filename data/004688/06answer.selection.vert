<s>
Rajhrad	Rajhrad	k1gInSc1	Rajhrad
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Groß	Groß	k1gMnSc1	Groß
Raigern	Raigern	k1gMnSc1	Raigern
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
ležící	ležící	k2eAgNnSc1d1	ležící
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Svratky	Svratka	k1gFnSc2	Svratka
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
asi	asi	k9	asi
12	[number]	k4	12
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
v	v	k7c6	v
Dyjsko-svrateckém	dyjskovratecký	k2eAgInSc6d1	dyjsko-svratecký
úvalu	úval	k1gInSc6	úval
<g/>
.	.	kIx.	.
</s>
