<s>
Bon	bon	k1gInSc1	bon
Jovi	Jov	k1gFnSc2	Jov
je	být	k5eAaImIp3nS	být
debutové	debutový	k2eAgNnSc1d1	debutové
album	album	k1gNnSc1	album
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
americké	americký	k2eAgFnSc2d1	americká
hard	hard	k6eAd1	hard
rockové	rockový	k2eAgFnSc2d1	rocková
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
Bon	bona	k1gFnPc2	bona
Jovi	Jovi	k1gNnSc1	Jovi
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
produkovali	produkovat	k5eAaImAgMnP	produkovat
Tony	Tony	k1gFnSc3	Tony
Bongiovi	Bongiův	k2eAgMnPc1d1	Bongiův
<g/>
,	,	kIx,	,
bratranec	bratranec	k1gMnSc1	bratranec
Jona	Jon	k1gInSc2	Jon
Bon	bon	k1gInSc1	bon
Joviho	Jovi	k1gMnSc2	Jovi
a	a	k8xC	a
Lance	lance	k1gNnPc1	lance
Quinn	Quinn	k1gNnSc1	Quinn
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jediné	jediný	k2eAgNnSc1d1	jediné
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
píseň	píseň	k1gFnSc1	píseň
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
She	She	k1gMnSc1	She
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Know	Know	k1gMnSc1	Know
Me	Me	k1gMnSc1	Me
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
textu	text	k1gInSc6	text
se	se	k3xPyFc4	se
nijak	nijak	k6eAd1	nijak
nepodíleli	podílet	k5eNaImAgMnP	podílet
členové	člen	k1gMnPc1	člen
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c4	na
43	[number]	k4	43
<g/>
.	.	kIx.	.
pozici	pozice	k1gFnSc6	pozice
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tři	tři	k4xCgInPc4	tři
singly	singl	k1gInPc4	singl
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc2	první
"	"	kIx"	"
<g/>
Runaway	Runawaa	k1gFnSc2	Runawaa
<g/>
"	"	kIx"	"
vydala	vydat	k5eAaPmAgFnS	vydat
kapela	kapela	k1gFnSc1	kapela
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
a	a	k8xC	a
již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
byl	být	k5eAaImAgInS	být
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
<g/>
,	,	kIx,	,
umístil	umístit	k5eAaPmAgMnS	umístit
se	se	k3xPyFc4	se
na	na	k7c4	na
39	[number]	k4	39
<g/>
.	.	kIx.	.
pozici	pozice	k1gFnSc4	pozice
amerického	americký	k2eAgInSc2d1	americký
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
zmiňovaná	zmiňovaný	k2eAgFnSc1d1	zmiňovaná
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
She	She	k1gMnSc1	She
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Know	Know	k1gMnSc1	Know
Me	Me	k1gMnSc1	Me
<g/>
"	"	kIx"	"
vyšla	vyjít	k5eAaPmAgFnS	vyjít
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1984	[number]	k4	1984
jako	jako	k8xS	jako
druhý	druhý	k4xOgInSc1	druhý
singl	singl	k1gInSc1	singl
alba	album	k1gNnSc2	album
a	a	k8xC	a
umístila	umístit	k5eAaPmAgFnS	umístit
se	se	k3xPyFc4	se
na	na	k7c4	na
48	[number]	k4	48
<g/>
.	.	kIx.	.
pozici	pozice	k1gFnSc6	pozice
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Skladbu	skladba	k1gFnSc4	skladba
napsal	napsat	k5eAaBmAgMnS	napsat
Marc	Marc	k1gFnSc4	Marc
Avsec	Avsec	k1gMnSc1	Avsec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xS	jako
člen	člen	k1gMnSc1	člen
uskupení	uskupení	k1gNnSc2	uskupení
Donnie	Donnie	k1gFnSc2	Donnie
Iris	iris	k1gFnSc2	iris
&	&	k?	&
The	The	k1gFnSc2	The
Cruisers	Cruisersa	k1gFnPc2	Cruisersa
<g/>
.	.	kIx.	.
</s>
<s>
Třetím	třetí	k4xOgInSc7	třetí
singlem	singl	k1gInSc7	singl
alba	album	k1gNnSc2	album
byla	být	k5eAaImAgFnS	být
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Burning	Burning	k1gInSc1	Burning
for	forum	k1gNnPc2	forum
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
jako	jako	k9	jako
singl	singl	k1gInSc4	singl
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
se	se	k3xPyFc4	se
Bon	bon	k1gInSc1	bon
Jovi	Jov	k1gFnSc2	Jov
umístilo	umístit	k5eAaPmAgNnS	umístit
také	také	k9	také
v	v	k7c4	v
Australian	Australian	k1gInSc4	Australian
Albums	Albums	k1gInSc4	Albums
Chart	charta	k1gFnPc2	charta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skončilo	skončit	k5eAaPmAgNnS	skončit
na	na	k7c4	na
39	[number]	k4	39
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
New	New	k1gFnSc6	New
Zealand	Zealand	k1gInSc1	Zealand
Albums	Albumsa	k1gFnPc2	Albumsa
Chart	charta	k1gFnPc2	charta
na	na	k7c6	na
osmnácté	osmnáctý	k4xOgFnSc6	osmnáctý
pozici	pozice	k1gFnSc6	pozice
a	a	k8xC	a
UK	UK	kA	UK
Albums	Albums	k1gInSc4	Albums
Chart	charta	k1gFnPc2	charta
na	na	k7c4	na
71	[number]	k4	71
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
získalo	získat	k5eAaPmAgNnS	získat
dvě	dva	k4xCgNnPc1	dva
ocenění	ocenění	k1gNnPc1	ocenění
platinové	platinový	k2eAgFnSc2d1	platinová
desky	deska	k1gFnSc2	deska
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
a	a	k8xC	a
zlaté	zlatý	k2eAgNnSc1d1	Zlaté
ocenění	ocenění	k1gNnSc1	ocenění
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
časopisu	časopis	k1gInSc2	časopis
Kerrang	Kerranga	k1gFnPc2	Kerranga
<g/>
!	!	kIx.	!
</s>
<s>
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
11	[number]	k4	11
<g/>
.	.	kIx.	.
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
rockové	rockový	k2eAgNnSc1d1	rockové
album	album	k1gNnSc1	album
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Jon	Jon	k?	Jon
Bon	bon	k1gInSc1	bon
Jovi	Jovi	k1gNnSc1	Jovi
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
Richie	Richie	k1gFnSc1	Richie
Sambora	Sambora	k1gFnSc1	Sambora
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěv	zpěv	k1gInSc1	zpěv
Alec	Alec	k1gFnSc1	Alec
John	John	k1gMnSc1	John
Such	sucho	k1gNnPc2	sucho
-	-	kIx~	-
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěv	zpěv	k1gInSc1	zpěv
Tico	Tico	k1gMnSc1	Tico
Torres	Torres	k1gMnSc1	Torres
-	-	kIx~	-
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
,	,	kIx,	,
perkuse	perkuse	k1gFnSc1	perkuse
David	David	k1gMnSc1	David
Bryan	Bryan	k1gInSc1	Bryan
-	-	kIx~	-
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěv	zpěv	k1gInSc1	zpěv
Roy	Roy	k1gMnSc1	Roy
Bittan	Bittan	k1gInSc1	Bittan
-	-	kIx~	-
klávesy	klávesa	k1gFnSc2	klávesa
Chuck	Chuck	k1gInSc1	Chuck
Burgi	Burge	k1gFnSc4	Burge
-	-	kIx~	-
doplňující	doplňující	k2eAgMnSc1d1	doplňující
bicí	bicí	k2eAgMnSc1d1	bicí
David	David	k1gMnSc1	David
Grahmme	Grahmme	k1gMnSc1	Grahmme
-	-	kIx~	-
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěv	zpěv	k1gInSc1	zpěv
Doug	Douga	k1gFnPc2	Douga
Katsaros	Katsarosa	k1gFnPc2	Katsarosa
-	-	kIx~	-
klávesy	klávesa	k1gFnSc2	klávesa
Frankie	Frankie	k1gFnSc2	Frankie
LaRocka	LaRocka	k1gFnSc1	LaRocka
-	-	kIx~	-
bicí	bicí	k2eAgNnSc1d1	bicí
Aldo	Aldo	k1gNnSc1	Aldo
Nova	nova	k1gFnSc1	nova
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
Tim	Tim	k?	Tim
Pierce	Pierec	k1gInSc2	Pierec
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
Mick	Micka	k1gFnPc2	Micka
Seeley	Seelea	k1gFnSc2	Seelea
-	-	kIx~	-
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěv	zpěv	k1gInSc1	zpěv
</s>
