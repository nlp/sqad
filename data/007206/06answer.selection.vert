<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
pec	pec	k1gFnSc1	pec
je	být	k5eAaImIp3nS	být
metalurgické	metalurgický	k2eAgNnSc4d1	metalurgické
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
surového	surový	k2eAgNnSc2d1	surové
železa	železo	k1gNnSc2	železo
redukcí	redukce	k1gFnPc2	redukce
z	z	k7c2	z
železných	železný	k2eAgFnPc2d1	železná
rud	ruda	k1gFnPc2	ruda
<g/>
.	.	kIx.	.
</s>
