<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
pec	pec	k1gFnSc1	pec
je	být	k5eAaImIp3nS	být
metalurgické	metalurgický	k2eAgNnSc4d1	metalurgické
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
surového	surový	k2eAgNnSc2d1	surové
železa	železo	k1gNnSc2	železo
redukcí	redukce	k1gFnPc2	redukce
z	z	k7c2	z
železných	železný	k2eAgFnPc2d1	železná
rud	ruda	k1gFnPc2	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
zavedením	zavedení	k1gNnSc7	zavedení
vysokých	vysoký	k2eAgFnPc2d1	vysoká
pecí	pec	k1gFnPc2	pec
bylo	být	k5eAaImAgNnS	být
železo	železo	k1gNnSc1	železo
získáváno	získávat	k5eAaImNgNnS	získávat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
houbovitého	houbovitý	k2eAgInSc2d1	houbovitý
polotovaru	polotovar	k1gInSc2	polotovar
v	v	k7c6	v
dýmačce	dýmačka	k1gFnSc6	dýmačka
<g/>
,	,	kIx,	,
jednoduché	jednoduchý	k2eAgFnSc3d1	jednoduchá
redukční	redukční	k2eAgFnSc3d1	redukční
peci	pec	k1gFnSc3	pec
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nebyla	být	k5eNaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
výrobek	výrobek	k1gInSc4	výrobek
roztavit	roztavit	k5eAaPmF	roztavit
<g/>
.	.	kIx.	.
</s>
<s>
Polotovar	polotovar	k1gInSc1	polotovar
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
zpracovával	zpracovávat	k5eAaImAgInS	zpracovávat
kováním	kování	k1gNnSc7	kování
v	v	k7c6	v
hamru	hamr	k1gInSc6	hamr
<g/>
.	.	kIx.	.
</s>
<s>
Zvětšováním	zvětšování	k1gNnSc7	zvětšování
objemu	objem	k1gInSc2	objem
a	a	k8xC	a
zejména	zejména	k9	zejména
výšky	výška	k1gFnSc2	výška
pece	pec	k1gFnSc2	pec
se	se	k3xPyFc4	se
zvyšovala	zvyšovat	k5eAaImAgFnS	zvyšovat
nejen	nejen	k6eAd1	nejen
účinnost	účinnost	k1gFnSc4	účinnost
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
teplota	teplota	k1gFnSc1	teplota
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dosažení	dosažení	k1gNnSc6	dosažení
výšky	výška	k1gFnSc2	výška
pece	pec	k1gFnSc2	pec
cca	cca	kA	cca
4	[number]	k4	4
m	m	kA	m
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
teplota	teplota	k1gFnSc1	teplota
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
pece	pec	k1gFnSc2	pec
přes	přes	k7c4	přes
900	[number]	k4	900
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
nauhličování	nauhličování	k1gNnSc3	nauhličování
železa	železo	k1gNnSc2	železo
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
karbidu	karbid	k1gInSc2	karbid
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Zvyšováním	zvyšování	k1gNnSc7	zvyšování
obsahu	obsah	k1gInSc2	obsah
karbidu	karbid	k1gInSc2	karbid
železa	železo	k1gNnSc2	železo
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
snižování	snižování	k1gNnSc3	snižování
teploty	teplota	k1gFnSc2	teplota
tavení	tavení	k1gNnSc2	tavení
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Binární	binární	k2eAgInSc1d1	binární
diagram	diagram	k1gInSc1	diagram
železo-uhlík	železohlík	k1gInSc1	železo-uhlík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
při	při	k7c6	při
dosažení	dosažení	k1gNnSc6	dosažení
teploty	teplota	k1gFnSc2	teplota
nad	nad	k7c7	nad
1150	[number]	k4	1150
°	°	k?	°
<g/>
C	C	kA	C
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
roztavení	roztavení	k1gNnSc3	roztavení
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
tak	tak	k6eAd1	tak
surové	surový	k2eAgNnSc4d1	surové
železo	železo	k1gNnSc4	železo
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
technologie	technologie	k1gFnSc1	technologie
zkujňování	zkujňování	k1gNnSc2	zkujňování
surového	surový	k2eAgNnSc2d1	surové
železa	železo	k1gNnSc2	železo
dejlováním	dejlování	k1gNnSc7	dejlování
později	pozdě	k6eAd2	pozdě
pudlováním	pudlování	k1gNnPc3	pudlování
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
železa	železo	k1gNnSc2	železo
vysoké	vysoký	k2eAgFnSc2d1	vysoká
pece	pec	k1gFnSc2	pec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgFnPc6	první
vysokých	vysoký	k2eAgFnPc6d1	vysoká
pecích	pec	k1gFnPc6	pec
bylo	být	k5eAaImAgNnS	být
-	-	kIx~	-
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
dýmačkách	dýmačka	k1gFnPc6	dýmačka
-	-	kIx~	-
používáno	používán	k2eAgNnSc4d1	používáno
dřevěné	dřevěný	k2eAgNnSc4d1	dřevěné
uhlí	uhlí	k1gNnSc4	uhlí
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
přešlo	přejít	k5eAaPmAgNnS	přejít
na	na	k7c4	na
koksové	koksový	k2eAgFnPc4d1	koksová
pece	pec	k1gFnPc4	pec
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
následujícímu	následující	k2eAgInSc3d1	následující
popisu	popis	k1gInSc3	popis
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
pec	pec	k1gFnSc1	pec
je	být	k5eAaImIp3nS	být
typově	typově	k6eAd1	typově
pec	pec	k1gFnSc1	pec
šachtová	šachtový	k2eAgFnSc1d1	šachtová
vysoká	vysoký	k2eAgFnSc1d1	vysoká
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
m	m	kA	m
.	.	kIx.
</s>
<s>
Profil	profil	k1gInSc1	profil
pece	pec	k1gFnSc2	pec
je	být	k5eAaImIp3nS	být
přizpůsoben	přizpůsoben	k2eAgInSc1d1	přizpůsoben
technologii	technologie	k1gFnSc4	technologie
provozu	provoz	k1gInSc2	provoz
a	a	k8xC	a
měnícímu	měnící	k2eAgInSc3d1	měnící
se	se	k3xPyFc4	se
objemu	objem	k1gInSc3	objem
vsázky	vsázka	k1gFnSc2	vsázka
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
kuželovité	kuželovitý	k2eAgFnSc2d1	kuželovitá
<g/>
,	,	kIx,	,
k	k	k7c3	k
základně	základna	k1gFnSc3	základna
se	se	k3xPyFc4	se
rozšiřující	rozšiřující	k2eAgFnSc2d1	rozšiřující
vlastní	vlastní	k2eAgFnSc2d1	vlastní
šachty	šachta	k1gFnSc2	šachta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
největšího	veliký	k2eAgInSc2d3	veliký
objemu	objem	k1gInSc2	objem
vsázky	vsázka	k1gFnSc2	vsázka
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
(	(	kIx(	(
<g/>
který	který	k3yIgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
zadržení	zadržení	k1gNnSc3	zadržení
celé	celý	k2eAgFnSc2d1	celá
náplně	náplň	k1gFnSc2	náplň
v	v	k7c6	v
peci	pec	k1gFnSc6	pec
<g/>
)	)	kIx)	)
v	v	k7c4	v
komolý	komolý	k2eAgInSc4d1	komolý
kužel	kužel	k1gInSc4	kužel
opačný	opačný	k2eAgInSc4d1	opačný
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
sedlo	sedlo	k1gNnSc4	sedlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
zužuje	zužovat	k5eAaImIp3nS	zužovat
k	k	k7c3	k
základně	základna	k1gFnSc3	základna
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
obr	obr	k1gMnSc1	obr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgInSc4d1	vnější
povrch	povrch	k1gInSc4	povrch
šachty	šachta	k1gFnSc2	šachta
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
silným	silný	k2eAgInSc7d1	silný
ocelovým	ocelový	k2eAgInSc7d1	ocelový
pancířem	pancíř	k1gInSc7	pancíř
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
strany	strana	k1gFnSc2	strana
chlazen	chladit	k5eAaImNgMnS	chladit
vodou	voda	k1gFnSc7	voda
pomocí	pomoc	k1gFnPc2	pomoc
litinových	litinový	k2eAgFnPc2d1	litinová
a	a	k8xC	a
měděných	měděný	k2eAgFnPc2d1	měděná
chladnic	chladnice	k1gFnPc2	chladnice
a	a	k8xC	a
chráněn	chránit	k5eAaImNgInS	chránit
žáruvzdornou	žáruvzdorný	k2eAgFnSc7d1	žáruvzdorná
vyzdívkou	vyzdívka	k1gFnSc7	vyzdívka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
konci	konec	k1gInSc6	konec
šachty	šachta	k1gFnSc2	šachta
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
sazebna	sazebna	k1gFnSc1	sazebna
sloužící	sloužící	k2eAgFnSc1d1	sloužící
pro	pro	k7c4	pro
doplňování	doplňování	k1gNnSc4	doplňování
materiálu	materiál	k1gInSc2	materiál
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
železa	železo	k1gNnSc2	železo
-	-	kIx~	-
vsázky	vsázka	k1gFnSc2	vsázka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
býval	bývat	k5eAaImAgInS	bývat
plnící	plnící	k2eAgInSc1d1	plnící
otvor	otvor	k1gInSc1	otvor
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
dvojitým	dvojitý	k2eAgInSc7d1	dvojitý
kuželovým	kuželový	k2eAgInSc7d1	kuželový
neboli	neboli	k8xC	neboli
zvonovým	zvonový	k2eAgInSc7d1	zvonový
sazebním	sazební	k2eAgInSc7d1	sazební
(	(	kIx(	(
<g/>
kychtovým	kychtový	k2eAgInSc7d1	kychtový
<g/>
)	)	kIx)	)
uzávěrem	uzávěr	k1gInSc7	uzávěr
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
jednak	jednak	k8xC	jednak
zavážení	zavážení	k1gNnSc4	zavážení
pece	pec	k1gFnSc2	pec
vsázkovým	vsázkový	k2eAgInSc7d1	vsázkový
materiálem	materiál	k1gInSc7	materiál
a	a	k8xC	a
jednak	jednak	k8xC	jednak
jej	on	k3xPp3gMnSc4	on
utěsňoval	utěsňovat	k5eAaImAgMnS	utěsňovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
odvádět	odvádět	k5eAaImF	odvádět
vysokopecní	vysokopecní	k2eAgInSc4d1	vysokopecní
plyn	plyn	k1gInSc4	plyn
do	do	k7c2	do
plynojemu	plynojem	k1gInSc2	plynojem
na	na	k7c4	na
ohřev	ohřev	k1gInSc4	ohřev
dmychaného	dmychaný	k2eAgInSc2d1	dmychaný
větru	vítr	k1gInSc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
již	již	k6eAd1	již
v	v	k7c6	v
drtivé	drtivý	k2eAgFnSc6d1	drtivá
většině	většina	k1gFnSc6	většina
používá	používat	k5eAaImIp3nS	používat
tzv.	tzv.	kA	tzv.
bezzvonová	bezzvonová	k1gFnSc1	bezzvonová
sazebna	sazebna	k1gFnSc1	sazebna
(	(	kIx(	(
<g/>
BZS	BZS	kA	BZS
<g/>
)	)	kIx)	)
tvořená	tvořený	k2eAgFnSc1d1	tvořená
dvěma	dva	k4xCgFnPc7	dva
materiálovými	materiálový	k2eAgFnPc7d1	materiálová
komorami	komora	k1gFnPc7	komora
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
nimi	on	k3xPp3gInPc7	on
je	být	k5eAaImIp3nS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
pojízdná	pojízdný	k2eAgFnSc1d1	pojízdná
násypka	násypka	k1gFnSc1	násypka
určující	určující	k2eAgFnSc1d1	určující
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
materiálová	materiálový	k2eAgFnSc1d1	materiálová
komora	komora	k1gFnSc1	komora
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
plnit	plnit	k5eAaImF	plnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
komoře	komora	k1gFnSc6	komora
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
horní	horní	k2eAgInSc1d1	horní
a	a	k8xC	a
dolní	dolní	k2eAgInSc1d1	dolní
klapový	klapový	k2eAgInSc1d1	klapový
uzávěr	uzávěr	k1gInSc1	uzávěr
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
segmentový	segmentový	k2eAgInSc4d1	segmentový
uzávěr	uzávěr	k1gInSc4	uzávěr
určující	určující	k2eAgFnSc1d1	určující
rychlost	rychlost	k1gFnSc1	rychlost
vysypávání	vysypávání	k1gNnSc2	vysypávání
komory	komora	k1gFnSc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
komorami	komora	k1gFnPc7	komora
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vsázka	vsázka	k1gFnSc1	vsázka
sype	sypat	k5eAaImIp3nS	sypat
do	do	k7c2	do
otočného	otočný	k2eAgInSc2d1	otočný
rozdělovače	rozdělovač	k1gInSc2	rozdělovač
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pomocí	pomocí	k7c2	pomocí
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
otočného	otočný	k2eAgInSc2d1	otočný
žlabu	žlab	k1gInSc2	žlab
vysypává	vysypávat	k5eAaImIp3nS	vysypávat
vsázku	vsázka	k1gFnSc4	vsázka
na	na	k7c4	na
předem	předem	k6eAd1	předem
zadané	zadaný	k2eAgFnPc4d1	zadaná
"	"	kIx"	"
<g/>
kružnice	kružnice	k1gFnPc4	kružnice
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
rozporu	rozpor	k1gInSc2	rozpor
pece	pec	k1gFnSc2	pec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
kruhové	kruhový	k2eAgNnSc4d1	kruhové
potrubí	potrubí	k1gNnSc4	potrubí
rozdělovače	rozdělovač	k1gInSc2	rozdělovač
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
okružní	okružní	k2eAgFnSc2d1	okružní
větrovod	větrovod	k1gInSc4	větrovod
<g/>
.	.	kIx.	.
</s>
<s>
Předehřátý	předehřátý	k2eAgInSc1d1	předehřátý
vzduch	vzduch	k1gInSc1	vzduch
(	(	kIx(	(
<g/>
vítr	vítr	k1gInSc1	vítr
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
odsud	odsud	k6eAd1	odsud
přiváděn	přivádět	k5eAaImNgInS	přivádět
pomocí	pomocí	k7c2	pomocí
výfučen	výfučna	k1gFnPc2	výfučna
do	do	k7c2	do
tavicího	tavicí	k2eAgInSc2d1	tavicí
prostoru	prostor	k1gInSc2	prostor
vysoké	vysoký	k2eAgFnSc2d1	vysoká
pece	pec	k1gFnSc2	pec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výfučnách	výfučna	k1gFnPc6	výfučna
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
umístěny	umístit	k5eAaPmNgFnP	umístit
zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
trysky	tryska	k1gFnPc1	tryska
pro	pro	k7c4	pro
přidávání	přidávání	k1gNnSc4	přidávání
topného	topný	k2eAgInSc2d1	topný
oleje	olej	k1gInSc2	olej
<g/>
,	,	kIx,	,
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
či	či	k8xC	či
mletého	mletý	k2eAgNnSc2d1	mleté
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nejnižší	nízký	k2eAgFnSc6d3	nejnižší
rovině	rovina	k1gFnSc6	rovina
nístěje	nístěj	k1gFnSc2	nístěj
(	(	kIx(	(
<g/>
dna	dno	k1gNnSc2	dno
pece	pec	k1gFnSc2	pec
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
výtok	výtok	k1gInSc1	výtok
roztaveného	roztavený	k2eAgNnSc2d1	roztavené
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
odpichový	odpichový	k2eAgInSc1d1	odpichový
otvor	otvor	k1gInSc1	otvor
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
úrovní	úroveň	k1gFnSc7	úroveň
odpichového	odpichový	k2eAgInSc2d1	odpichový
otvoru	otvor	k1gInSc2	otvor
pro	pro	k7c4	pro
surové	surový	k2eAgNnSc4d1	surové
železo	železo	k1gNnSc4	železo
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
na	na	k7c6	na
jiné	jiný	k2eAgFnSc6d1	jiná
straně	strana	k1gFnSc6	strana
pece	pec	k1gFnSc2	pec
obvykle	obvykle	k6eAd1	obvykle
ještě	ještě	k6eAd1	ještě
výtokové	výtokový	k2eAgInPc4d1	výtokový
otvory	otvor	k1gInPc4	otvor
strusky	struska	k1gFnSc2	struska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
však	však	k9	však
od	od	k7c2	od
vypouštění	vypouštění	k1gNnSc2	vypouštění
strusky	struska	k1gFnSc2	struska
přes	přes	k7c4	přes
tyto	tento	k3xDgInPc4	tento
otvory	otvor	k1gInPc4	otvor
upustilo	upustit	k5eAaPmAgNnS	upustit
a	a	k8xC	a
veškerá	veškerý	k3xTgFnSc1	veškerý
tavenina	tavenina	k1gFnSc1	tavenina
z	z	k7c2	z
pece	pec	k1gFnSc2	pec
je	být	k5eAaImIp3nS	být
vypouštěna	vypouštěn	k2eAgFnSc1d1	vypouštěna
pouze	pouze	k6eAd1	pouze
odpichovým	odpichový	k2eAgInSc7d1	odpichový
otvorem	otvor	k1gInSc7	otvor
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
pec	pec	k1gFnSc1	pec
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
úrovni	úroveň	k1gFnSc6	úroveň
víc	hodně	k6eAd2	hodně
odpichových	odpichový	k2eAgInPc2d1	odpichový
otvorů	otvor	k1gInPc2	otvor
<g/>
.	.	kIx.	.
</s>
<s>
Struska	struska	k1gFnSc1	struska
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
surového	surový	k2eAgNnSc2d1	surové
železa	železo	k1gNnSc2	železo
oddělována	oddělován	k2eAgFnSc1d1	oddělována
v	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
odpichovém	odpichový	k2eAgInSc6d1	odpichový
žlabu	žlab	k1gInSc6	žlab
pomocí	pomocí	k7c2	pomocí
přepážky	přepážka	k1gFnSc2	přepážka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
lehčí	lehčit	k5eAaImIp3nS	lehčit
strusku	struska	k1gFnSc4	struska
odvádí	odvádět	k5eAaImIp3nS	odvádět
do	do	k7c2	do
struskového	struskový	k2eAgInSc2d1	struskový
žlabu	žlab	k1gInSc2	žlab
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
něhož	jenž	k3xRgNnSc2	jenž
je	být	k5eAaImIp3nS	být
dopravována	dopravovat	k5eAaImNgFnS	dopravovat
do	do	k7c2	do
struskových	struskový	k2eAgFnPc2d1	Strusková
pánví	pánev	k1gFnPc2	pánev
nebo	nebo	k8xC	nebo
k	k	k7c3	k
přímému	přímý	k2eAgNnSc3d1	přímé
zpracování	zpracování	k1gNnSc3	zpracování
<g/>
,	,	kIx,	,
např.	např.	kA	např.
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
granulačních	granulační	k2eAgFnPc2d1	granulační
komor	komora	k1gFnPc2	komora
(	(	kIx(	(
<g/>
výroba	výroba	k1gFnSc1	výroba
granulačního	granulační	k2eAgInSc2d1	granulační
písku	písek	k1gInSc2	písek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zavážka	zavážka	k1gFnSc1	zavážka
vysoké	vysoký	k2eAgFnSc2d1	vysoká
pece	pec	k1gFnSc2	pec
<g/>
,	,	kIx,	,
t.j.	t.j.	k?	t.j.
doprava	doprava	k1gFnSc1	doprava
železné	železný	k2eAgFnSc2d1	železná
rudy	ruda	k1gFnSc2	ruda
<g/>
,	,	kIx,	,
koksu	koks	k1gInSc2	koks
a	a	k8xC	a
struskotvorných	struskotvorný	k2eAgFnPc2d1	struskotvorná
přísad	přísada	k1gFnPc2	přísada
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
buďto	buďto	k8xC	buďto
šikmým	šikmý	k2eAgInSc7d1	šikmý
výtahem	výtah	k1gInSc7	výtah
(	(	kIx(	(
<g/>
skipem	skip	k1gInSc7	skip
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
pomocí	pomocí	k7c2	pomocí
velkých	velký	k2eAgFnPc2d1	velká
zavážecích	zavážecí	k2eAgFnPc2d1	zavážecí
nádob	nádoba	k1gFnPc2	nádoba
(	(	kIx(	(
<g/>
košů	koš	k1gInPc2	koš
<g/>
)	)	kIx)	)
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
kolmých	kolmý	k2eAgInPc2d1	kolmý
výtahů	výtah	k1gInPc2	výtah
<g/>
.	.	kIx.	.
</s>
<s>
Ohřívače	ohřívač	k1gInPc1	ohřívač
větru	vítr	k1gInSc2	vítr
(	(	kIx(	(
<g/>
Cowper	Cowper	k1gInSc1	Cowper
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
vysoké	vysoký	k2eAgFnPc1d1	vysoká
ocelové	ocelový	k2eAgFnPc1d1	ocelová
válcovité	válcovitý	k2eAgFnPc1d1	válcovitá
stavby	stavba	k1gFnPc1	stavba
uvnitř	uvnitř	k6eAd1	uvnitř
vyzděné	vyzděný	k2eAgInPc1d1	vyzděný
ohnivzdornou	ohnivzdorný	k2eAgFnSc7d1	ohnivzdorná
kanálkovou	kanálkový	k2eAgFnSc7d1	Kanálková
vyzdívkou	vyzdívka	k1gFnSc7	vyzdívka
a	a	k8xC	a
vybavené	vybavený	k2eAgInPc4d1	vybavený
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
výšce	výška	k1gFnSc6	výška
sahající	sahající	k2eAgFnSc7d1	sahající
spalnou	spalný	k2eAgFnSc7d1	spalná
komorou	komora	k1gFnSc7	komora
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
konci	konec	k1gInSc6	konec
jsou	být	k5eAaImIp3nP	být
uzavřeny	uzavřít	k5eAaPmNgInP	uzavřít
kopulí	kopule	k1gFnSc7	kopule
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
pod	pod	k7c7	pod
kopulí	kopule	k1gFnSc7	kopule
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
1	[number]	k4	1
600	[number]	k4	600
°	°	k?	°
<g/>
C.	C.	kA	C.
Ke	k	k7c3	k
každé	každý	k3xTgFnSc3	každý
vysoké	vysoký	k2eAgFnSc3d1	vysoká
peci	pec	k1gFnSc3	pec
patří	patřit	k5eAaImIp3nS	patřit
3	[number]	k4	3
až	až	k9	až
5	[number]	k4	5
ohřívačů	ohřívač	k1gInPc2	ohřívač
větru	vítr	k1gInSc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Pracují	pracovat	k5eAaImIp3nP	pracovat
střídavě	střídavě	k6eAd1	střídavě
<g/>
,	,	kIx,	,
přerušovaně	přerušovaně	k6eAd1	přerušovaně
t.j.	t.j.	k?	t.j.
jeden	jeden	k4xCgInSc4	jeden
předává	předávat	k5eAaImIp3nS	předávat
teplo	teplo	k6eAd1	teplo
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
další	další	k2eAgMnSc1d1	další
je	být	k5eAaImIp3nS	být
předehříván	předehřívat	k5eAaImNgInS	předehřívat
atd.	atd.	kA	atd.
Vytápěny	vytápěn	k2eAgFnPc1d1	vytápěna
jsou	být	k5eAaImIp3nP	být
vysokopecním	vysokopecní	k2eAgInSc7d1	vysokopecní
plynem	plyn	k1gInSc7	plyn
zpravidla	zpravidla	k6eAd1	zpravidla
s	s	k7c7	s
přídavkem	přídavek	k1gInSc7	přídavek
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
nebo	nebo	k8xC	nebo
topného	topný	k2eAgInSc2d1	topný
oleje	olej	k1gInSc2	olej
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
vytápění	vytápění	k1gNnSc2	vytápění
pouze	pouze	k6eAd1	pouze
vysokopecním	vysokopecní	k2eAgInSc7d1	vysokopecní
plynem	plyn	k1gInSc7	plyn
se	se	k3xPyFc4	se
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
účinnost	účinnost	k1gFnSc1	účinnost
vytápění	vytápění	k1gNnSc2	vytápění
předehřevem	předehřev	k1gInSc7	předehřev
spalovacího	spalovací	k2eAgInSc2d1	spalovací
plynu	plyn	k1gInSc2	plyn
a	a	k8xC	a
vzduchu	vzduch	k1gInSc2	vzduch
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
výměníků	výměník	k1gInPc2	výměník
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
využívají	využívat	k5eAaImIp3nP	využívat
teplotu	teplota	k1gFnSc4	teplota
spalin	spaliny	k1gFnPc2	spaliny
(	(	kIx(	(
<g/>
cca	cca	kA	cca
300	[number]	k4	300
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
odcházejících	odcházející	k2eAgMnPc2d1	odcházející
z	z	k7c2	z
vytápěných	vytápěný	k2eAgInPc2d1	vytápěný
ohřívačů	ohřívač	k1gInPc2	ohřívač
větrů	vítr	k1gInPc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Vysokopecní	vysokopecní	k2eAgInSc1d1	vysokopecní
(	(	kIx(	(
<g/>
kychtový	kychtový	k2eAgInSc1d1	kychtový
<g/>
)	)	kIx)	)
plyn	plyn	k1gInSc1	plyn
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
%	%	kIx~	%
jedovatého	jedovatý	k2eAgInSc2d1	jedovatý
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgInSc2d1	uhelnatý
(	(	kIx(	(
<g/>
CO	co	k8xS	co
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k6eAd1	málo
výhřevný	výhřevný	k2eAgMnSc1d1	výhřevný
(	(	kIx(	(
<g/>
cca	cca	kA	cca
15000-18500	[number]	k4	15000-18500
kJ	kJ	k?	kJ
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
vysokou	vysoký	k2eAgFnSc7d1	vysoká
pecí	pec	k1gFnSc7	pec
a	a	k8xC	a
ohřívači	ohřívač	k1gInSc6	ohřívač
vzduchu	vzduch	k1gInSc2	vzduch
je	být	k5eAaImIp3nS	být
vysokopecní	vysokopecní	k2eAgInSc4d1	vysokopecní
plyn	plyn	k1gInSc4	plyn
čištěn	čištěn	k2eAgInSc4d1	čištěn
a	a	k8xC	a
zbavován	zbavován	k2eAgInSc4d1	zbavován
prachu	prach	k1gInSc3	prach
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
podíl	podíl	k1gInSc1	podíl
prachu	prach	k1gInSc2	prach
je	být	k5eAaImIp3nS	být
zachycován	zachycovat	k5eAaImNgInS	zachycovat
v	v	k7c6	v
lapači	lapač	k1gInSc6	lapač
prachu	prach	k1gInSc2	prach
(	(	kIx(	(
<g/>
prašníku	prašník	k1gInSc2	prašník
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
cyklonech	cyklon	k1gInPc6	cyklon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
hrubému	hrubý	k2eAgNnSc3d1	hrubé
čištění	čištění	k1gNnSc3	čištění
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
polojemnému	polojemný	k2eAgNnSc3d1	polojemný
čištění	čištění	k1gNnSc3	čištění
obvykle	obvykle	k6eAd1	obvykle
dochází	docházet	k5eAaImIp3nS	docházet
ve	v	k7c6	v
skrubru	skrubr	k1gInSc6	skrubr
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
válcová	válcový	k2eAgFnSc1d1	válcová
nádoba	nádoba	k1gFnSc1	nádoba
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
je	být	k5eAaImIp3nS	být
spodem	spodem	k6eAd1	spodem
přiváděn	přiváděn	k2eAgInSc1d1	přiváděn
plyn	plyn	k1gInSc1	plyn
proti	proti	k7c3	proti
jemně	jemně	k6eAd1	jemně
rozprášené	rozprášený	k2eAgFnSc3d1	rozprášená
vodě	voda	k1gFnSc3	voda
padající	padající	k2eAgFnSc2d1	padající
shora	shora	k6eAd1	shora
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
strhává	strhávat	k5eAaImIp3nS	strhávat
částice	částice	k1gFnSc1	částice
prachu	prach	k1gInSc2	prach
do	do	k7c2	do
spodní	spodní	k2eAgFnSc2d1	spodní
části	část	k1gFnSc2	část
skrubru	skrubr	k1gInSc2	skrubr
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
odvádí	odvádět	k5eAaImIp3nS	odvádět
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
kalu	kal	k1gInSc2	kal
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
následuje	následovat	k5eAaImIp3nS	následovat
jemné	jemný	k2eAgNnSc4d1	jemné
čištění	čištění	k1gNnSc4	čištění
v	v	k7c6	v
pračkách	pračka	k1gFnPc6	pračka
(	(	kIx(	(
<g/>
Venturiho	Venturi	k1gMnSc2	Venturi
pračky	pračka	k1gFnSc2	pračka
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
filtrech	filtr	k1gInPc6	filtr
<g/>
.	.	kIx.	.
</s>
<s>
Předehřátý	předehřátý	k2eAgInSc1d1	předehřátý
vzduch	vzduch	k1gInSc1	vzduch
(	(	kIx(	(
<g/>
700	[number]	k4	700
až	až	k9	až
1	[number]	k4	1
100	[number]	k4	100
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pomocí	pomocí	k7c2	pomocí
mohutných	mohutný	k2eAgNnPc2d1	mohutné
dmychadel	dmychadlo	k1gNnPc2	dmychadlo
dopravován	dopravován	k2eAgInSc1d1	dopravován
vyzděným	vyzděný	k2eAgNnSc7d1	vyzděné
potrubím	potrubí	k1gNnSc7	potrubí
do	do	k7c2	do
výfučen	výfučna	k1gFnPc2	výfučna
vysoké	vysoký	k2eAgFnSc2d1	vysoká
pece	pec	k1gFnSc2	pec
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
dodávky	dodávka	k1gFnSc2	dodávka
potřebného	potřebný	k2eAgInSc2d1	potřebný
kyslíku	kyslík	k1gInSc2	kyslík
pro	pro	k7c4	pro
spalování	spalování	k1gNnSc4	spalování
koksu	koks	k1gInSc2	koks
slouží	sloužit	k5eAaImIp3nS	sloužit
vítr	vítr	k1gInSc1	vítr
ve	v	k7c6	v
vysoké	vysoký	k2eAgFnSc6d1	vysoká
peci	pec	k1gFnSc6	pec
také	také	k9	také
k	k	k7c3	k
udržování	udržování	k1gNnSc3	udržování
konsistence	konsistence	k1gFnSc2	konsistence
vsázky	vsázka	k1gFnSc2	vsázka
(	(	kIx(	(
<g/>
víření	víření	k1gNnSc2	víření
<g/>
)	)	kIx)	)
a	a	k8xC	a
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
vzniku	vznik	k1gInSc3	vznik
spečenin	spečenina	k1gFnPc2	spečenina
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
pece	pec	k1gFnSc2	pec
<g/>
.	.	kIx.	.
</s>
<s>
Vysokopecní	vysokopecní	k2eAgNnPc1d1	vysokopecní
zařízení	zařízení	k1gNnPc1	zařízení
bývají	bývat	k5eAaImIp3nP	bývat
minimálně	minimálně	k6eAd1	minimálně
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
v	v	k7c6	v
nepřetržitém	přetržitý	k2eNgInSc6d1	nepřetržitý
provozu	provoz	k1gInSc6	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uplynutí	uplynutí	k1gNnSc6	uplynutí
této	tento	k3xDgFnSc2	tento
provozní	provozní	k2eAgFnSc2d1	provozní
fáze	fáze	k1gFnSc2	fáze
je	být	k5eAaImIp3nS	být
prováděna	provádět	k5eAaImNgFnS	provádět
generální	generální	k2eAgFnSc1d1	generální
oprava	oprava	k1gFnSc1	oprava
celého	celý	k2eAgNnSc2d1	celé
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vysoké	vysoký	k2eAgFnSc2d1	vysoká
pece	pec	k1gFnSc2	pec
se	se	k3xPyFc4	se
zaváží	zavážit	k5eAaPmIp3nS	zavážit
železná	železný	k2eAgFnSc1d1	železná
ruda	ruda	k1gFnSc1	ruda
<g/>
,	,	kIx,	,
koks	koks	k1gInSc1	koks
a	a	k8xC	a
struskotvorné	struskotvorný	k2eAgFnPc1d1	struskotvorná
přísady	přísada	k1gFnPc1	přísada
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
slouží	sloužit	k5eAaImIp3nS	sloužit
jednak	jednak	k8xC	jednak
k	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
tavicí	tavicí	k2eAgFnSc2d1	tavicí
teploty	teplota	k1gFnSc2	teplota
hlušin	hlušina	k1gFnPc2	hlušina
a	a	k8xC	a
jednak	jednak	k8xC	jednak
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
železa	železo	k1gNnSc2	železo
během	během	k7c2	během
vysokopecního	vysokopecní	k2eAgInSc2d1	vysokopecní
procesu	proces	k1gInSc2	proces
před	před	k7c7	před
zpětnou	zpětný	k2eAgFnSc7d1	zpětná
oxidací	oxidace	k1gFnSc7	oxidace
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
vápenec	vápenec	k1gInSc4	vápenec
<g/>
,	,	kIx,	,
dolomit	dolomit	k1gInSc4	dolomit
a	a	k8xC	a
zřídka	zřídka	k6eAd1	zřídka
také	také	k9	také
křemičitý	křemičitý	k2eAgInSc4d1	křemičitý
štěrk	štěrk	k1gInSc4	štěrk
<g/>
.	.	kIx.	.
</s>
<s>
Použitý	použitý	k2eAgInSc1d1	použitý
typ	typ	k1gInSc1	typ
přísady	přísada	k1gFnSc2	přísada
závisí	záviset	k5eAaImIp3nS	záviset
především	především	k9	především
na	na	k7c6	na
kvalitě	kvalita	k1gFnSc6	kvalita
rudy	ruda	k1gFnSc2	ruda
a	a	k8xC	a
jejím	její	k3xOp3gInSc6	její
indexu	index	k1gInSc6	index
bazicity	bazicita	k1gFnSc2	bazicita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
pece	pec	k1gFnSc2	pec
pod	pod	k7c7	pod
sazebnou	sazebna	k1gFnSc7	sazebna
se	se	k3xPyFc4	se
předehřívá	předehřívat	k5eAaImIp3nS	předehřívat
vsázka	vsázka	k1gFnSc1	vsázka
a	a	k8xC	a
vypařuje	vypařovat	k5eAaImIp3nS	vypařovat
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vlhkost	vlhkost	k1gFnSc1	vlhkost
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
stoupající	stoupající	k2eAgFnSc7d1	stoupající
teplotou	teplota	k1gFnSc7	teplota
v	v	k7c6	v
nižší	nízký	k2eAgFnSc6d2	nižší
části	část	k1gFnSc6	část
šachty	šachta	k1gFnSc2	šachta
pece	pec	k1gFnSc2	pec
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
kontaktu	kontakt	k1gInSc3	kontakt
železné	železný	k2eAgFnSc2d1	železná
rudy	ruda	k1gFnSc2	ruda
s	s	k7c7	s
procházejícími	procházející	k2eAgFnPc7d1	procházející
spalinami	spaliny	k1gFnPc7	spaliny
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
CO	co	k3yRnSc4	co
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
v	v	k7c6	v
teplotním	teplotní	k2eAgNnSc6d1	teplotní
rozmezí	rozmezí	k1gNnSc6	rozmezí
mezi	mezi	k7c7	mezi
400	[number]	k4	400
a	a	k8xC	a
1	[number]	k4	1
000	[number]	k4	000
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
probíhá	probíhat	k5eAaImIp3nS	probíhat
tzv.	tzv.	kA	tzv.
nepřímá	přímý	k2eNgFnSc1d1	nepřímá
redukce	redukce	k1gFnSc1	redukce
oxidů	oxid	k1gInPc2	oxid
železa	železo	k1gNnSc2	železo
obsažených	obsažený	k2eAgInPc2d1	obsažený
v	v	k7c6	v
rudě	ruda	k1gFnSc6	ruda
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
redukce	redukce	k1gFnSc1	redukce
oxidem	oxid	k1gInSc7	oxid
uhelnatým	uhelnatý	k2eAgInSc7d1	uhelnatý
ze	z	k7c2	z
spalin	spaliny	k1gFnPc2	spaliny
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fe	Fe	k?	Fe
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
+	+	kIx~	+
3	[number]	k4	3
CO	co	k8xS	co
→	→	k?	→
3	[number]	k4	3
CO2	CO2	k1gFnPc2	CO2
+	+	kIx~	+
2	[number]	k4	2
Fe	Fe	k1gFnPc2	Fe
V	v	k7c6	v
nižších	nízký	k2eAgNnPc6d2	nižší
pásmech	pásmo	k1gNnPc6	pásmo
pece	pec	k1gFnSc2	pec
dále	daleko	k6eAd2	daleko
stoupá	stoupat	k5eAaImIp3nS	stoupat
teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
zde	zde	k6eAd1	zde
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
mezi	mezi	k7c7	mezi
1	[number]	k4	1
000	[number]	k4	000
a	a	k8xC	a
2	[number]	k4	2
000	[number]	k4	000
°	°	k?	°
<g/>
C	C	kA	C
jak	jak	k8xS	jak
k	k	k7c3	k
přímé	přímý	k2eAgFnSc3d1	přímá
redukci	redukce	k1gFnSc3	redukce
ještě	ještě	k6eAd1	ještě
neredukovaných	redukovaný	k2eNgInPc2d1	neredukovaný
oxidů	oxid	k1gInPc2	oxid
železa	železo	k1gNnSc2	železo
z	z	k7c2	z
již	již	k6eAd1	již
těstovitého	těstovitý	k2eAgInSc2d1	těstovitý
až	až	k8xS	až
tekutého	tekutý	k2eAgInSc2d1	tekutý
kovu	kov	k1gInSc2	kov
uhlíkem	uhlík	k1gInSc7	uhlík
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
k	k	k7c3	k
nauhličování	nauhličování	k1gNnSc3	nauhličování
taveniny	tavenina	k1gFnSc2	tavenina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
surovém	surový	k2eAgNnSc6d1	surové
železe	železo	k1gNnSc6	železo
se	se	k3xPyFc4	se
obsah	obsah	k1gInSc1	obsah
uhlíku	uhlík	k1gInSc2	uhlík
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
3,5	[number]	k4	3,5
až	až	k9	až
4,5	[number]	k4	4,5
%	%	kIx~	%
(	(	kIx(	(
<g/>
sloučeniny	sloučenina	k1gFnPc1	sloučenina
železa	železo	k1gNnSc2	železo
s	s	k7c7	s
uhlíkem	uhlík	k1gInSc7	uhlík
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
podle	podle	k7c2	podle
množství	množství	k1gNnSc2	množství
uhlíku	uhlík	k1gInSc2	uhlík
a	a	k8xC	a
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc2	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
<g/>
:	:	kIx,	:
např.	např.	kA	např.
austenit	austenit	k1gInSc1	austenit
<g/>
,	,	kIx,	,
cementit	cementit	k1gInSc1	cementit
<g/>
,	,	kIx,	,
perlit	perlit	k1gInSc1	perlit
<g/>
,	,	kIx,	,
ledeburit	ledeburit	k1gInSc1	ledeburit
<g/>
,	,	kIx,	,
ferit	ferit	k1gInSc1	ferit
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
diagram	diagram	k1gInSc4	diagram
Fe-C	Fe-C	k1gMnSc2	Fe-C
Přímá	přímý	k2eAgFnSc1d1	přímá
redukce	redukce	k1gFnSc1	redukce
<g/>
:	:	kIx,	:
Fe	Fe	k1gFnSc1	Fe
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
+	+	kIx~	+
4	[number]	k4	4
C	C	kA	C
→	→	k?	→
3	[number]	k4	3
Fe	Fe	k1gFnPc2	Fe
+	+	kIx~	+
4	[number]	k4	4
CO	co	k8xS	co
Fe	Fe	k1gFnSc1	Fe
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
+	+	kIx~	+
3	[number]	k4	3
C	C	kA	C
→	→	k?	→
2	[number]	k4	2
Fe	Fe	k1gFnPc2	Fe
+	+	kIx~	+
3	[number]	k4	3
CO	co	k8xS	co
FeO	FeO	k1gMnSc1	FeO
+	+	kIx~	+
C	C	kA	C
→	→	k?	→
Fe	Fe	k1gMnSc7	Fe
+	+	kIx~	+
CO	co	k8xS	co
Nepřímá	přímý	k2eNgFnSc1d1	nepřímá
redukce	redukce	k1gFnSc1	redukce
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
3	[number]	k4	3
Fe	Fe	k1gFnSc1	Fe
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
+	+	kIx~	+
CO	co	k8xS	co
→	→	k?	→
2	[number]	k4	2
Fe	Fe	k1gFnSc1	Fe
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
+	+	kIx~	+
CO2	CO2	k1gFnSc1	CO2
Fe	Fe	k1gFnSc2	Fe
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
+	+	kIx~	+
CO	co	k8xS	co
→	→	k?	→
3	[number]	k4	3
FeO	FeO	k1gFnSc1	FeO
+	+	kIx~	+
CO2	CO2	k1gFnSc1	CO2
FeO	FeO	k1gFnSc2	FeO
+	+	kIx~	+
CO	co	k8xS	co
→	→	k?	→
Fe	Fe	k1gFnSc1	Fe
+	+	kIx~	+
CO2	CO2	k1gFnSc1	CO2
Roztavené	roztavený	k2eAgNnSc1d1	roztavené
železo	železo	k1gNnSc1	železo
a	a	k8xC	a
struska	struska	k1gFnSc1	struska
se	se	k3xPyFc4	se
shromažďují	shromažďovat	k5eAaImIp3nP	shromažďovat
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
pece	pec	k1gFnSc2	pec
v	v	k7c6	v
nístěji	nístěj	k1gFnSc6	nístěj
<g/>
.	.	kIx.	.
</s>
<s>
Odpichovým	odpichový	k2eAgInSc7d1	odpichový
otvorem	otvor	k1gInSc7	otvor
proudí	proudit	k5eAaImIp3nS	proudit
roztavené	roztavený	k2eAgNnSc1d1	roztavené
železo	železo	k1gNnSc1	železo
do	do	k7c2	do
veronik	veronika	k1gFnPc2	veronika
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
pojízdných	pojízdný	k2eAgInPc2d1	pojízdný
mísičů	mísič	k1gInPc2	mísič
<g/>
=	=	kIx~	=
<g/>
železničních	železniční	k2eAgInPc2d1	železniční
vagónů	vagón	k1gInPc2	vagón
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
odléváno	odléván	k2eAgNnSc1d1	odléváno
do	do	k7c2	do
forem	forma	k1gFnPc2	forma
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
housky	houska	k1gFnPc4	houska
<g/>
.	.	kIx.	.
</s>
<s>
Struska	struska	k1gFnSc1	struska
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
vysoké	vysoký	k2eAgFnSc2d1	vysoká
pece	pec	k1gFnSc2	pec
přepravována	přepravován	k2eAgFnSc1d1	přepravována
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
kolibou	koliba	k1gFnSc7	koliba
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
železničním	železniční	k2eAgInSc7d1	železniční
vagónem	vagón	k1gInSc7	vagón
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
zpracování	zpracování	k1gNnSc3	zpracování
např.	např.	kA	např.
granulování	granulování	k1gNnSc2	granulování
<g/>
.	.	kIx.	.
</s>
<s>
Vysokopecní	vysokopecní	k2eAgInPc1d1	vysokopecní
provozy	provoz	k1gInPc1	provoz
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
součástí	součást	k1gFnSc7	součást
integrovaných	integrovaný	k2eAgInPc2d1	integrovaný
hutních	hutní	k2eAgInPc2d1	hutní
závodů	závod	k1gInPc2	závod
a	a	k8xC	a
tekuté	tekutý	k2eAgNnSc4d1	tekuté
surové	surový	k2eAgNnSc4d1	surové
železo	železo	k1gNnSc4	železo
je	být	k5eAaImIp3nS	být
ihned	ihned	k6eAd1	ihned
přímo	přímo	k6eAd1	přímo
dopravováno	dopravován	k2eAgNnSc1d1	dopravováno
do	do	k7c2	do
oceláren	ocelárna	k1gFnPc2	ocelárna
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
zpracováno	zpracován	k2eAgNnSc1d1	zpracováno
na	na	k7c4	na
ocel	ocel	k1gFnSc4	ocel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
nacházelo	nacházet	k5eAaImAgNnS	nacházet
poměrně	poměrně	k6eAd1	poměrně
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
vysokých	vysoký	k2eAgFnPc2d1	vysoká
pecí	pec	k1gFnPc2	pec
<g/>
,	,	kIx,	,
zprvu	zprvu	k6eAd1	zprvu
dřevouhelných	dřevouhelný	k2eAgMnPc2d1	dřevouhelný
a	a	k8xC	a
zpracovávajících	zpracovávající	k2eAgMnPc2d1	zpracovávající
místní	místní	k2eAgFnSc4d1	místní
rudu	ruda	k1gFnSc4	ruda
<g/>
;	;	kIx,	;
po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
minerálního	minerální	k2eAgNnSc2d1	minerální
uhlí	uhlí	k1gNnSc2	uhlí
jejich	jejich	k3xOp3gFnSc2	jejich
rozměry	rozměra	k1gFnSc2	rozměra
časem	časem	k6eAd1	časem
narůstaly	narůstat	k5eAaImAgInP	narůstat
<g/>
,	,	kIx,	,
přecházely	přecházet	k5eAaImAgInP	přecházet
na	na	k7c4	na
rudu	ruda	k1gFnSc4	ruda
dováženou	dovážený	k2eAgFnSc4d1	dovážená
z	z	k7c2	z
větších	veliký	k2eAgFnPc2d2	veliký
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
se	se	k3xPyFc4	se
snižoval	snižovat	k5eAaImAgInS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
pouze	pouze	k6eAd1	pouze
vysoké	vysoký	k2eAgFnSc2d1	vysoká
pece	pec	k1gFnSc2	pec
v	v	k7c6	v
Třineckých	třinecký	k2eAgFnPc6d1	třinecká
železárnách	železárna	k1gFnPc6	železárna
a	a	k8xC	a
v	v	k7c6	v
ostravské	ostravský	k2eAgFnSc6d1	Ostravská
huti	huť	k1gFnSc6	huť
ArcelorMittal	ArcelorMittal	k1gMnSc1	ArcelorMittal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
byl	být	k5eAaImAgInS	být
zastaven	zastavit	k5eAaPmNgInS	zastavit
provoz	provoz	k1gInSc1	provoz
poslední	poslední	k2eAgFnSc2d1	poslední
vysoké	vysoký	k2eAgFnSc2d1	vysoká
pece	pec	k1gFnSc2	pec
ve	v	k7c6	v
Vítkovických	vítkovický	k2eAgFnPc6d1	Vítkovická
železárnách	železárna	k1gFnPc6	železárna
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgFnPc1d1	zdejší
vysoké	vysoký	k2eAgFnPc1d1	vysoká
pece	pec	k1gFnPc1	pec
však	však	k9	však
nebyly	být	k5eNaImAgFnP	být
zbourány	zbourán	k2eAgFnPc1d1	zbourána
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
bývalou	bývalý	k2eAgFnSc7d1	bývalá
koksovnou	koksovna	k1gFnSc7	koksovna
a	a	k8xC	a
bývalým	bývalý	k2eAgInSc7d1	bývalý
dolem	dol	k1gInSc7	dol
Hlubina	hlubina	k1gFnSc1	hlubina
je	být	k5eAaImIp3nS	být
celý	celý	k2eAgInSc4d1	celý
areál	areál	k1gInSc4	areál
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
historických	historický	k2eAgFnPc2d1	historická
dominant	dominanta	k1gFnPc2	dominanta
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
tento	tento	k3xDgInSc1	tento
areál	areál	k1gInSc1	areál
byl	být	k5eAaImAgInS	být
prohlášen	prohlásit	k5eAaPmNgInS	prohlásit
za	za	k7c4	za
národní	národní	k2eAgFnSc4d1	národní
kulturní	kulturní	k2eAgFnSc4d1	kulturní
památku	památka	k1gFnSc4	památka
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
OTÁHALOVÁ	OTÁHALOVÁ	kA	OTÁHALOVÁ
<g/>
,	,	kIx,	,
Helena	Helena	k1gFnSc1	Helena
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
železných	železný	k2eAgInPc2d1	železný
kovů	kov	k1gInPc2	kov
I	i	k8xC	i
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
2	[number]	k4	2
<g/>
.	.	kIx.	.
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
středních	střední	k1gMnPc2	střední
odborných	odborný	k2eAgNnPc2d1	odborné
učilišť	učiliště	k1gNnPc2	učiliště
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SNTL	SNTL	kA	SNTL
–	–	k?	–
Nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
technické	technický	k2eAgFnSc2d1	technická
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
364	[number]	k4	364
s.	s.	k?	s.
MAKARIUS	MAKARIUS	kA	MAKARIUS
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
surového	surový	k2eAgNnSc2d1	surové
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
I.	I.	kA	I.
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
technické	technický	k2eAgFnSc2d1	technická
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
256	[number]	k4	256
s.	s.	k?	s.
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vysoká	vysoký	k2eAgFnSc1d1	vysoká
pec	pec	k1gFnSc1	pec
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Stahlseite	Stahlseit	k1gInSc5	Stahlseit
–	–	k?	–
vyobrazení	vyobrazení	k1gNnPc4	vyobrazení
a	a	k8xC	a
dobrý	dobrý	k2eAgInSc4d1	dobrý
text	text	k1gInSc4	text
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
oceli	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Stahl	Stahl	k1gFnPc1	Stahl
<g/>
4	[number]	k4	4
<g/>
you	you	k?	you
–	–	k?	–
mnoho	mnoho	k4c4	mnoho
informací	informace	k1gFnPc2	informace
<g />
.	.	kIx.	.
</s>
<s>
o	o	k7c6	o
výrobě	výroba	k1gFnSc6	výroba
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
oceli	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Viktor	Viktor	k1gMnSc1	Viktor
Mácha	Mácha	k1gMnSc1	Mácha
-	-	kIx~	-
fotografická	fotografický	k2eAgFnSc1d1	fotografická
dokumentace	dokumentace	k1gFnSc1	dokumentace
evropského	evropský	k2eAgInSc2d1	evropský
těžkého	těžký	k2eAgInSc2d1	těžký
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
<g/>
/	/	kIx~	/
<g/>
anglicky	anglicky	k6eAd1	anglicky
http://en.wikipedia.org/wiki/Direct_reduced_iron	[url]	k1gInSc1	http://en.wikipedia.org/wiki/Direct_reduced_iron
-	-	kIx~	-
další	další	k2eAgFnSc1d1	další
metoda	metoda	k1gFnSc1	metoda
výroby	výroba	k1gFnSc2	výroba
surového	surový	k2eAgNnSc2d1	surové
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
oceli	ocel	k1gFnSc2	ocel
O	o	k7c6	o
kadovských	kadovský	k2eAgFnPc6d1	kadovský
dřevěnouhelných	dřevěnouhelný	k2eAgFnPc6d1	dřevěnouhelný
vysokých	vysoký	k2eAgFnPc6d1	vysoká
pecích	pec	k1gFnPc6	pec
a	a	k8xC	a
výrobě	výroba	k1gFnSc6	výroba
železa	železo	k1gNnSc2	železo
na	na	k7c6	na
Vysočině	vysočina	k1gFnSc6	vysočina
celkově	celkově	k6eAd1	celkově
–	–	k?	–
článek	článek	k1gInSc1	článek
na	na	k7c6	na
zanikleobce	zanikleobka	k1gFnSc6	zanikleobka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
