<s>
Habsburská	habsburský	k2eAgFnSc1d1
monarchie	monarchie	k1gFnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Habsburgermonarchie	Habsburgermonarchie	k1gFnSc1
<g/>
)	)	kIx)
či	či	k8xC
hovorově	hovorově	k6eAd1
Rakouská	rakouský	k2eAgFnSc1d1
monarchie	monarchie	k1gFnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Österreichische	Österreichische	k1gFnSc1
Monarchie	monarchie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
popř.	popř.	kA
Podunajská	podunajský	k2eAgFnSc1d1
monarchie	monarchie	k1gFnSc1
(	(	kIx(
<g/>
Donaumonarchie	Donaumonarchie	k1gFnSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
zkráceně	zkráceně	k6eAd1
jen	jen	k9
Rakousko	Rakousko	k1gNnSc1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
neoficiální	neoficiální	k2eAgInPc1d1,k2eNgInPc1d1
názvy	název	k1gInPc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
historického	historický	k2eAgNnSc2d1
soustátí	soustátí	k1gNnSc2
<g/>
,	,	kIx,
jemuž	jenž	k3xRgMnSc3
vládla	vládnout	k5eAaImAgFnS
rakouská	rakouský	k2eAgFnSc1d1
větev	větev	k1gFnSc1
habsburské	habsburský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
následnická	následnický	k2eAgFnSc1d1
habsbursko-lotrinská	habsbursko-lotrinský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
v	v	k7c6
letech	let	k1gInPc6
1526	#num#	k4
<g/>
–	–	k?
<g/>
1804	#num#	k4
<g/>
.	.	kIx.
</s>