<s desamb="1">
Kvůli	kvůli	k7c3
obavě	obava	k1gFnSc3
ze	z	k7c2
ztráty	ztráta	k1gFnSc2
císařského	císařský	k2eAgInSc2d1
titulu	titul	k1gInSc2
František	František	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
ustavil	ustavit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1804	[number]	k4
nový	nový	k2eAgInSc1d1
dědičný	dědičný	k2eAgInSc1d1
titul	titul	k1gInSc1
rakouského	rakouský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
se	se	k3xPyFc4
habsburská	habsburský	k2eAgFnSc1d1
monarchie	monarchie	k1gFnSc1
proměnila	proměnit	k5eAaPmAgFnS
na	na	k7c4
Rakouské	rakouský	k2eAgNnSc4d1
císařství	císařství	k1gNnSc4
<g/>
.	.	kIx.
</s>
