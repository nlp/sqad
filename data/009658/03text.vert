<p>
<s>
Chłapowo	Chłapowo	k1gNnSc1	Chłapowo
(	(	kIx(	(
<g/>
kašubsky	kašubsky	k6eAd1	kašubsky
Chłapò	Chłapò	k1gFnSc1	Chłapò
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Chlapau	Chlapaa	k1gFnSc4	Chlapaa
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1942-45	[number]	k4	1942-45
Klappau	Klappaa	k1gFnSc4	Klappaa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
severopolská	severopolský	k2eAgFnSc1d1	severopolský
(	(	kIx(	(
<g/>
kašubská	kašubský	k2eAgFnSc1d1	kašubská
<g/>
)	)	kIx)	)
vesnice	vesnice	k1gFnSc1	vesnice
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Chłapowu	Chłapowus	k1gInSc6	Chłapowus
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1359	[number]	k4	1359
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
obec	obec	k1gFnSc1	obec
nesla	nést	k5eAaImAgFnS	nést
tehdy	tehdy	k6eAd1	tehdy
název	název	k1gInSc4	název
Klappow	Klappow	k1gFnSc2	Klappow
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Chłapowo	Chłapowo	k6eAd1	Chłapowo
se	se	k3xPyFc4	se
ustálil	ustálit	k5eAaPmAgMnS	ustálit
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
roky	rok	k1gInPc7	rok
1877	[number]	k4	1877
až	až	k9	až
1949	[number]	k4	1949
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
Chłapowa	Chłapow	k1gInSc2	Chłapow
objeveno	objevit	k5eAaPmNgNnS	objevit
124	[number]	k4	124
hrobů	hrob	k1gInPc2	hrob
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
železné	železný	k2eAgFnSc2d1	železná
(	(	kIx(	(
<g/>
650	[number]	k4	650
<g/>
-	-	kIx~	-
<g/>
400	[number]	k4	400
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Archeologové	archeolog	k1gMnPc1	archeolog
našli	najít	k5eAaPmAgMnP	najít
dobře	dobře	k6eAd1	dobře
zachovalé	zachovalý	k2eAgInPc4d1	zachovalý
šperky	šperk	k1gInPc4	šperk
<g/>
,	,	kIx,	,
skleněné	skleněný	k2eAgInPc4d1	skleněný
korálky	korálek	k1gInPc4	korálek
a	a	k8xC	a
porcelán	porcelán	k1gInSc4	porcelán
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
archeologický	archeologický	k2eAgInSc1d1	archeologický
výzkum	výzkum	k1gInSc1	výzkum
prováděný	prováděný	k2eAgMnSc1d1	prováděný
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
objevu	objev	k1gInSc3	objev
předmětů	předmět	k1gInPc2	předmět
z	z	k7c2	z
období	období	k1gNnSc2	období
neolitu	neolit	k1gInSc2	neolit
(	(	kIx(	(
<g/>
4200	[number]	k4	4200
<g/>
-	-	kIx~	-
<g/>
1700	[number]	k4	1700
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
a	a	k8xC	a
doby	doba	k1gFnSc2	doba
bronzové	bronzový	k2eAgFnSc2d1	bronzová
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
-	-	kIx~	-
<g/>
1000	[number]	k4	1000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dynamický	dynamický	k2eAgInSc4d1	dynamický
rozvoj	rozvoj	k1gInSc4	rozvoj
Chłapowa	Chłapowa	k1gFnSc1	Chłapowa
začal	začít	k5eAaPmAgInS	začít
na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
tu	tu	k6eAd1	tu
byl	být	k5eAaImAgInS	být
činný	činný	k2eAgInSc1d1	činný
malý	malý	k2eAgInSc1d1	malý
hnědouhelný	hnědouhelný	k2eAgInSc1d1	hnědouhelný
důl	důl	k1gInSc1	důl
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
blízkosti	blízkost	k1gFnSc3	blízkost
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
se	se	k3xPyFc4	se
Chłapowo	Chłapowo	k6eAd1	Chłapowo
v	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc6	období
stalo	stát	k5eAaPmAgNnS	stát
známým	známý	k2eAgNnSc7d1	známé
rekreačním	rekreační	k2eAgNnSc7d1	rekreační
střediskem	středisko	k1gNnSc7	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
konkurence	konkurence	k1gFnSc2	konkurence
ostatních	ostatní	k2eAgNnPc2d1	ostatní
středisek	středisko	k1gNnPc2	středisko
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
(	(	kIx(	(
<g/>
Władysławowo	Władysławowo	k1gNnSc1	Władysławowo
<g/>
,	,	kIx,	,
Jastrzębia	Jastrzębia	k1gFnSc1	Jastrzębia
Góra	Góra	k1gFnSc1	Góra
<g/>
,	,	kIx,	,
Karwia	Karwia	k1gFnSc1	Karwia
<g/>
,	,	kIx,	,
místa	místo	k1gNnPc1	místo
na	na	k7c6	na
Helské	Helský	k2eAgFnSc6d1	Helský
kose	kosa	k1gFnSc6	kosa
<g/>
)	)	kIx)	)
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
svůj	svůj	k3xOyFgInSc4	svůj
význam	význam	k1gInSc4	význam
a	a	k8xC	a
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
si	se	k3xPyFc3	se
charakter	charakter	k1gInSc4	charakter
rybářské	rybářský	k2eAgFnSc2d1	rybářská
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jsou	být	k5eAaImIp3nP	být
ložiska	ložisko	k1gNnPc1	ložisko
soli	sůl	k1gFnSc2	sůl
kamenné	kamenný	k2eAgFnSc2d1	kamenná
a	a	k8xC	a
draselné	draselný	k2eAgFnSc2d1	draselná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Administrativa	administrativa	k1gFnSc1	administrativa
==	==	k?	==
</s>
</p>
<p>
<s>
Administrativně	administrativně	k6eAd1	administrativně
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
část	část	k1gFnSc4	část
města	město	k1gNnSc2	město
Władysławowo	Władysławowo	k6eAd1	Władysławowo
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
náleží	náležet	k5eAaImIp3nP	náležet
k	k	k7c3	k
okresu	okres	k1gInSc3	okres
Powiat	Powiat	k2eAgMnSc1d1	Powiat
Pucki	Puck	k1gMnPc1	Puck
Pomořanského	pomořanský	k2eAgNnSc2d1	Pomořanské
vojvodství	vojvodství	k1gNnSc2	vojvodství
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
tu	tu	k6eAd1	tu
něco	něco	k3yInSc1	něco
přes	přes	k7c4	přes
1000	[number]	k4	1000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Místem	místo	k1gNnSc7	místo
prochází	procházet	k5eAaImIp3nS	procházet
silnice	silnice	k1gFnSc1	silnice
číslo	číslo	k1gNnSc1	číslo
215	[number]	k4	215
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Chłapowo	Chłapowo	k6eAd1	Chłapowo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
Słownik	Słownik	k1gMnSc1	Słownik
geograficzny	geograficzna	k1gFnSc2	geograficzna
Królestwa	Królestwa	k1gMnSc1	Królestwa
Polskiego	Polskiego	k1gMnSc1	Polskiego
i	i	k8xC	i
innych	innych	k1gMnSc1	innych
krajów	krajów	k?	krajów
słowiańskich	słowiańskich	k1gInSc1	słowiańskich
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Chłapowo	Chłapowo	k6eAd1	Chłapowo
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
Stránky	stránka	k1gFnPc1	stránka
o	o	k7c6	o
místě	místo	k1gNnSc6	místo
</s>
</p>
