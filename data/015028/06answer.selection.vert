<s>
Svoboda	Svoboda	k1gMnSc1	Svoboda
a	a	k8xC	a
přímá	přímý	k2eAgFnSc1d1	přímá
demokracie	demokracie	k1gFnSc1	demokracie
–	–	k?	–
Tomio	Tomio	k1gNnSc1	Tomio
Okamura	Okamura	k1gFnSc1	Okamura
(	(	kIx(	(
<g/>
SPD	SPD	kA	SPD
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
SPD	SPD	kA	SPD
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
české	český	k2eAgNnSc1d1	české
pravicově	pravicově	k6eAd1	pravicově
populistické	populistický	k2eAgNnSc1d1	populistické
politické	politický	k2eAgNnSc1d1	politické
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
poslanci	poslanec	k1gMnPc1	poslanec
Tomiem	Tomium	k1gNnSc7	Tomium
Okamurou	Okamura	k1gFnSc7	Okamura
a	a	k8xC	a
Radimem	Radim	k1gMnSc7	Radim
Fialou	Fiala	k1gMnSc7	Fiala
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
odešli	odejít	k5eAaPmAgMnP	odejít
po	po	k7c6	po
roztržce	roztržka	k1gFnSc6	roztržka
v	v	k7c6	v
poslaneckém	poslanecký	k2eAgInSc6d1	poslanecký
klubu	klub	k1gInSc6	klub
z	z	k7c2	z
hnutí	hnutí	k1gNnSc2	hnutí
Úsvit	úsvit	k1gInSc1	úsvit
přímé	přímý	k2eAgFnSc2d1	přímá
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
.	.	kIx.	.
</s>
