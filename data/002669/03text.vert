<s>
Petržalka	Petržalka	k1gFnSc1	Petržalka
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
Pozsonyligetfalu	Pozsonyligetfala	k1gFnSc4	Pozsonyligetfala
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Engerau	Engeraus	k1gInSc3	Engeraus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
Bratislavy	Bratislava	k1gFnSc2	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejlidnatější	lidnatý	k2eAgFnPc4d3	nejlidnatější
části	část	k1gFnPc4	část
slovenské	slovenský	k2eAgFnSc2d1	slovenská
metropole	metropol	k1gFnSc2	metropol
<g/>
;	;	kIx,	;
žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
117000	[number]	k4	117000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
těsně	těsně	k6eAd1	těsně
při	při	k7c6	při
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
Petržalce	Petržalka	k1gFnSc6	Petržalka
pocházejí	pocházet	k5eAaImIp3nP	pocházet
již	již	k6eAd1	již
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1225	[number]	k4	1225
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
už	už	k9	už
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
vesnice	vesnice	k1gFnSc1	vesnice
s	s	k7c7	s
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
pěti	pět	k4xCc2	pět
sty	sto	k4xCgNnPc7	sto
stálými	stálý	k2eAgMnPc7d1	stálý
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ji	on	k3xPp3gFnSc4	on
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
Bratislavou	Bratislava	k1gFnSc7	Bratislava
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
železniční	železniční	k2eAgInSc1d1	železniční
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
dal	dát	k5eAaPmAgMnS	dát
petržalské	petržalský	k2eAgNnSc4d1	petržalský
předmostí	předmostí	k1gNnSc4	předmostí
opevnit	opevnit	k5eAaPmF	opevnit
generál	generál	k1gMnSc1	generál
Josef	Josef	k1gMnSc1	Josef
Šnejdárek	Šnejdárek	k1gMnSc1	Šnejdárek
a	a	k8xC	a
poté	poté	k6eAd1	poté
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
úsek	úsek	k1gInSc1	úsek
zařazen	zařadit	k5eAaPmNgInS	zařadit
do	do	k7c2	do
I.	I.	kA	I.
etapy	etapa	k1gFnSc2	etapa
naléhavého	naléhavý	k2eAgNnSc2d1	naléhavé
opevňování	opevňování	k1gNnSc2	opevňování
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
československého	československý	k2eAgNnSc2d1	Československé
opevnění	opevnění	k1gNnSc2	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zde	zde	k6eAd1	zde
postaveno	postavit	k5eAaPmNgNnS	postavit
25	[number]	k4	25
bunkrů	bunkr	k1gInPc2	bunkr
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1938	[number]	k4	1938
byla	být	k5eAaImAgFnS	být
připojena	připojit	k5eAaPmNgFnS	připojit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
k	k	k7c3	k
nacistickému	nacistický	k2eAgNnSc3d1	nacistické
Německu	Německo	k1gNnSc3	Německo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byla	být	k5eAaImAgNnP	být
obydlena	obydlet	k5eAaPmNgNnP	obydlet
převážně	převážně	k6eAd1	převážně
německy	německy	k6eAd1	německy
hovořícím	hovořící	k2eAgNnSc7d1	hovořící
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
,	,	kIx,	,
po	po	k7c4	po
osvobození	osvobození	k1gNnSc4	osvobození
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
vrácena	vrácen	k2eAgFnSc1d1	vrácena
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
dnes	dnes	k6eAd1	dnes
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vystavělo	vystavět	k5eAaPmAgNnS	vystavět
dnešní	dnešní	k2eAgNnSc4d1	dnešní
panelové	panelový	k2eAgNnSc4d1	panelové
sídliště	sídliště	k1gNnSc4	sídliště
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
malé	malý	k2eAgFnSc2d1	malá
okrajové	okrajový	k2eAgFnSc2d1	okrajová
čtvrti	čtvrt	k1gFnSc2	čtvrt
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
minimální	minimální	k2eAgInSc4d1	minimální
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
sídliště	sídliště	k1gNnSc1	sídliště
bylo	být	k5eAaImAgNnS	být
největší	veliký	k2eAgNnSc1d3	veliký
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Petržalka	Petržalka	k1gFnSc1	Petržalka
je	být	k5eAaImIp3nS	být
pomocí	pomocí	k7c2	pomocí
pěti	pět	k4xCc2	pět
mostů	most	k1gInPc2	most
napojená	napojený	k2eAgFnSc1d1	napojená
na	na	k7c4	na
zbytek	zbytek	k1gInSc4	zbytek
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
také	také	k9	také
železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
Bratislava-Petržalka	Bratislava-Petržalka	k1gFnSc1	Bratislava-Petržalka
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
mostů	most	k1gInPc2	most
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ústí	ústit	k5eAaImIp3nS	ústit
právě	právě	k6eAd1	právě
sem	sem	k6eAd1	sem
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Most	most	k1gInSc1	most
Slovenského	slovenský	k2eAgNnSc2d1	slovenské
národného	národný	k2eAgNnSc2d1	národné
povstania	povstanium	k1gNnSc2	povstanium
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
dominantou	dominanta	k1gFnSc7	dominanta
celého	celý	k2eAgNnSc2d1	celé
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Petržalka	Petržalka	k1gFnSc1	Petržalka
byla	být	k5eAaImAgFnS	být
projektována	projektovat	k5eAaBmNgFnS	projektovat
a	a	k8xC	a
stavěna	stavit	k5eAaImNgFnS	stavit
na	na	k7c4	na
obsluhu	obsluha	k1gFnSc4	obsluha
metrem	metr	k1gInSc7	metr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
výstavby	výstavba	k1gFnSc2	výstavba
metra	metro	k1gNnSc2	metro
byl	být	k5eAaImAgInS	být
projekt	projekt	k1gInSc1	projekt
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Veřejnou	veřejný	k2eAgFnSc4d1	veřejná
dopravu	doprava	k1gFnSc4	doprava
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
čtvrti	čtvrt	k1gFnSc2	čtvrt
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
pouze	pouze	k6eAd1	pouze
autobusy	autobus	k1gInPc4	autobus
<g/>
,	,	kIx,	,
trolejbusové	trolejbusový	k2eAgFnPc4d1	trolejbusová
tratě	trať	k1gFnPc4	trať
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
,	,	kIx,	,
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
trať	trať	k1gFnSc1	trať
začala	začít	k5eAaPmAgFnS	začít
fungovat	fungovat	k5eAaImF	fungovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
autobusové	autobusový	k2eAgInPc1d1	autobusový
spoje	spoj	k1gInPc1	spoj
tak	tak	k9	tak
bývají	bývat	k5eAaImIp3nP	bývat
velmi	velmi	k6eAd1	velmi
vytížené	vytížený	k2eAgInPc1d1	vytížený
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
po	po	k7c6	po
změně	změna	k1gFnSc6	změna
jízdního	jízdní	k2eAgInSc2d1	jízdní
řádu	řád	k1gInSc2	řád
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Petržalka	Petržalka	k1gFnSc1	Petržalka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
