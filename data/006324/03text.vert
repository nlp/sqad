<s>
Futurama	Futurama	k?	Futurama
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
animovaný	animovaný	k2eAgInSc1d1	animovaný
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
Matt	Matt	k2eAgInSc4d1	Matt
Groening	Groening	k1gInSc4	Groening
a	a	k8xC	a
David	David	k1gMnSc1	David
X.	X.	kA	X.
Cohen	Cohen	k1gInSc1	Cohen
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
seriálu	seriál	k1gInSc2	seriál
Simpsonovi	Simpson	k1gMnSc6	Simpson
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
čtyři	čtyři	k4xCgFnPc4	čtyři
produkční	produkční	k2eAgFnPc4d1	produkční
série	série	k1gFnPc4	série
vysílala	vysílat	k5eAaImAgFnS	vysílat
televizní	televizní	k2eAgFnSc1d1	televizní
stanice	stanice	k1gFnSc1	stanice
Fox	fox	k1gInSc1	fox
Network	network	k1gInSc1	network
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1999	[number]	k4	1999
až	až	k9	až
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2006	[number]	k4	2006
televizní	televizní	k2eAgFnSc1d1	televizní
společnost	společnost	k1gFnSc1	společnost
Comedy	Comeda	k1gMnSc2	Comeda
Central	Central	k1gMnSc2	Central
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
objednala	objednat	k5eAaPmAgFnS	objednat
dalších	další	k2eAgFnPc2d1	další
16	[number]	k4	16
epizod	epizoda	k1gFnPc2	epizoda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
začala	začít	k5eAaPmAgFnS	začít
vysílat	vysílat	k5eAaImF	vysílat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
4	[number]	k4	4
níže	nízce	k6eAd2	nízce
uvedené	uvedený	k2eAgInPc1d1	uvedený
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
každý	každý	k3xTgMnSc1	každý
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
4	[number]	k4	4
epizody	epizoda	k1gFnSc2	epizoda
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
společnost	společnost	k1gFnSc1	společnost
Comedy	Comeda	k1gMnSc2	Comeda
Central	Central	k1gMnSc2	Central
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
dohodu	dohoda	k1gFnSc4	dohoda
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
dalších	další	k2eAgInPc2d1	další
26	[number]	k4	26
nových	nový	k2eAgInPc2d1	nový
dílů	díl	k1gInPc2	díl
seriálu	seriál	k1gInSc2	seriál
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
vysílat	vysílat	k5eAaImF	vysílat
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
též	též	k9	též
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sedmá	sedmý	k4xOgFnSc1	sedmý
série	série	k1gFnSc1	série
bude	být	k5eAaImBp3nS	být
sérií	série	k1gFnSc7	série
poslední	poslední	k2eAgFnSc7d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
Závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
epizoda	epizoda	k1gFnSc1	epizoda
Futuramy	Futuram	k1gInPc4	Futuram
byla	být	k5eAaImAgFnS	být
odvysílána	odvysílán	k2eAgFnSc1d1	odvysílána
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
Futurama	Futurama	k?	Futurama
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
Futurama	Futurama	k?	Futurama
<g/>
:	:	kIx,	:
Benderovo	Benderův	k2eAgNnSc4d1	Benderovo
parádní	parádní	k2eAgNnSc4d1	parádní
terno	terno	k1gNnSc4	terno
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2007	[number]	k4	2007
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
film	film	k1gInSc1	film
Futurama	Futurama	k?	Futurama
<g/>
:	:	kIx,	:
Milion	milion	k4xCgInSc4	milion
a	a	k8xC	a
jedno	jeden	k4xCgNnSc4	jeden
chapadlo	chapadlo	k1gNnSc4	chapadlo
byl	být	k5eAaImAgMnS	být
vydán	vydán	k2eAgMnSc1d1	vydán
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
filmy	film	k1gInPc1	film
Futurama	Futurama	k?	Futurama
<g/>
:	:	kIx,	:
Benderova	Benderův	k2eAgFnSc1d1	Benderova
hra	hra	k1gFnSc1	hra
(	(	kIx(	(
<g/>
vydán	vydán	k2eAgInSc1d1	vydán
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
a	a	k8xC	a
Futurama	Futurama	k?	Futurama
<g/>
:	:	kIx,	:
Fialový	fialový	k2eAgMnSc1d1	fialový
trpaslík	trpaslík	k1gMnSc1	trpaslík
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
pravidelně	pravidelně	k6eAd1	pravidelně
následovaly	následovat	k5eAaImAgFnP	následovat
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
stanice	stanice	k1gFnSc2	stanice
Comedy	Comeda	k1gMnSc2	Comeda
Central	Central	k1gMnSc2	Central
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
seriál	seriál	k1gInSc1	seriál
neprodlouží	prodloužit	k5eNaPmIp3nS	prodloužit
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc1d1	poslední
epizoda	epizoda	k1gFnSc1	epizoda
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
odvysílána	odvysílán	k2eAgFnSc1d1	odvysílána
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Novém	Nový	k1gMnSc6	Nový
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
přilehlém	přilehlý	k2eAgInSc6d1	přilehlý
vesmíru	vesmír	k1gInSc6	vesmír
od	od	k7c2	od
roku	rok	k1gInSc2	rok
3000	[number]	k4	3000
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
díle	díl	k1gInSc6	díl
spadne	spadnout	k5eAaPmIp3nS	spadnout
věčný	věčný	k2eAgMnSc1d1	věčný
smolař	smolař	k1gMnSc1	smolař
Philip	Philip	k1gMnSc1	Philip
J.	J.	kA	J.
Fry	Fry	k1gFnSc1	Fry
na	na	k7c4	na
Silvestra	Silvestr	k1gMnSc4	Silvestr
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
do	do	k7c2	do
mrazáku	mrazák	k1gInSc2	mrazák
v	v	k7c6	v
kryogenické	kryogenický	k2eAgFnSc6d1	Kryogenická
laboratoři	laboratoř	k1gFnSc6	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
Rozmražen	rozmražen	k2eAgInSc1d1	rozmražen
je	být	k5eAaImIp3nS	být
až	až	k9	až
po	po	k7c6	po
1000	[number]	k4	1000
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2999	[number]	k4	2999
a	a	k8xC	a
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Novém	Nový	k1gMnSc6	Nový
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
objevenému	objevený	k2eAgInSc3d1	objevený
praprapra	praprapr	k1gInSc2	praprapr
<g/>
...	...	k?	...
<g/>
praprasynovci	praprasynovec	k1gMnSc3	praprasynovec
<g/>
,	,	kIx,	,
profesoru	profesor	k1gMnSc3	profesor
Hubertu	Hubert	k1gMnSc3	Hubert
Farnsworthovi	Farnsworth	k1gMnSc3	Farnsworth
dostane	dostat	k5eAaPmIp3nS	dostat
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
malé	malý	k2eAgFnSc6d1	malá
intergalaktické	intergalaktický	k2eAgFnSc6d1	intergalaktická
doručovatelské	doručovatelský	k2eAgFnSc6d1	doručovatelská
firmě	firma	k1gFnSc6	firma
Planet	planeta	k1gFnPc2	planeta
Express	express	k1gInSc1	express
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
dobrodružstvích	dobrodružství	k1gNnPc6	dobrodružství
Frye	Fry	k1gFnSc2	Fry
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
kolegů	kolega	k1gMnPc2	kolega
při	při	k7c6	při
dodávání	dodávání	k1gNnSc6	dodávání
zásilek	zásilka	k1gFnPc2	zásilka
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
této	tento	k3xDgFnSc2	tento
dopravní	dopravní	k2eAgFnSc2d1	dopravní
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
ve	v	k7c6	v
Futuramě	Futurama	k1gFnSc6	Futurama
<g/>
.	.	kIx.	.
</s>
<s>
Philip	Philip	k1gMnSc1	Philip
J.	J.	kA	J.
Fry	Fry	k1gMnSc1	Fry
–	–	k?	–
smolař	smolař	k1gMnSc1	smolař
Fry	Fry	k1gMnSc1	Fry
je	být	k5eAaImIp3nS	být
ústřední	ústřední	k2eAgFnSc7d1	ústřední
postavou	postava	k1gFnSc7	postava
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
poslíček	poslíček	k1gMnSc1	poslíček
v	v	k7c6	v
pizzerii	pizzerie	k1gFnSc6	pizzerie
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
zmrazen	zmrazit	k5eAaPmNgInS	zmrazit
na	na	k7c4	na
1000	[number]	k4	1000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
31	[number]	k4	31
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ho	on	k3xPp3gInSc4	on
zaměstnal	zaměstnat	k5eAaPmAgMnS	zaměstnat
jeho	jeho	k3xOp3gMnSc1	jeho
jediný	jediný	k2eAgMnSc1d1	jediný
žijící	žijící	k2eAgMnSc1d1	žijící
příbuzný	příbuzný	k1gMnSc1	příbuzný
H.	H.	kA	H.
J.	J.	kA	J.
Farnsworth	Farnswortha	k1gFnPc2	Farnswortha
jako	jako	k8xS	jako
intergalaktického	intergalaktický	k2eAgMnSc2d1	intergalaktický
poslíčka	poslíček	k1gMnSc2	poslíček
ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
Planet	planeta	k1gFnPc2	planeta
Express	express	k1gInSc1	express
<g/>
.	.	kIx.	.
</s>
<s>
Fry	Fry	k?	Fry
se	se	k3xPyFc4	se
neobtěžuje	obtěžovat	k5eNaImIp3nS	obtěžovat
věcmi	věc	k1gFnPc7	věc
jako	jako	k8xS	jako
disciplína	disciplína	k1gFnSc1	disciplína
<g/>
,	,	kIx,	,
pořádek	pořádek	k1gInSc1	pořádek
nebo	nebo	k8xC	nebo
přemýšlení	přemýšlení	k1gNnSc1	přemýšlení
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc7	svůj
lehkomyslností	lehkomyslnost	k1gFnSc7	lehkomyslnost
často	často	k6eAd1	často
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
komplikace	komplikace	k1gFnPc4	komplikace
svému	svůj	k3xOyFgNnSc3	svůj
okolí	okolí	k1gNnSc3	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
ukáže	ukázat	k5eAaPmIp3nS	ukázat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
klíčovou	klíčový	k2eAgFnSc7d1	klíčová
postavou	postava	k1gFnSc7	postava
pro	pro	k7c4	pro
osud	osud	k1gInSc4	osud
celého	celý	k2eAgInSc2d1	celý
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Turanga	Turanga	k1gFnSc1	Turanga
Leela	Leela	k1gFnSc1	Leela
–	–	k?	–
jednooká	jednooký	k2eAgFnSc1d1	jednooká
Leela	Leela	k1gFnSc1	Leela
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgInSc7d1	jediný
členem	člen	k1gInSc7	člen
posádky	posádka	k1gFnSc2	posádka
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
Planet	planeta	k1gFnPc2	planeta
Express	express	k1gInSc4	express
se	s	k7c7	s
smyslem	smysl	k1gInSc7	smysl
pro	pro	k7c4	pro
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
zastává	zastávat	k5eAaImIp3nS	zastávat
funkci	funkce	k1gFnSc4	funkce
kapitána	kapitán	k1gMnSc2	kapitán
<g/>
.	.	kIx.	.
</s>
<s>
Vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
v	v	k7c6	v
sirotčináriu	sirotčinárium	k1gNnSc6	sirotčinárium
<g/>
,	,	kIx,	,
před	před	k7c7	před
kterým	který	k3yQgInSc7	který
ji	on	k3xPp3gFnSc4	on
jako	jako	k9	jako
novorozeně	novorozeně	k6eAd1	novorozeně
našli	najít	k5eAaPmAgMnP	najít
odloženou	odložený	k2eAgFnSc4d1	odložená
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
mimozemšťan	mimozemšťan	k1gMnSc1	mimozemšťan
neznámé	známý	k2eNgFnSc2d1	neznámá
rasy	rasa	k1gFnSc2	rasa
<g/>
,	,	kIx,	,
po	po	k7c6	po
setkání	setkání	k1gNnSc6	setkání
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
rodiči	rodič	k1gMnPc7	rodič
ale	ale	k8xC	ale
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
pouze	pouze	k6eAd1	pouze
<g/>
"	"	kIx"	"
mutant	mutant	k1gMnSc1	mutant
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
ostatní	ostatní	k1gNnSc4	ostatní
vytahuje	vytahovat	k5eAaImIp3nS	vytahovat
z	z	k7c2	z
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
má	mít	k5eAaImIp3nS	mít
talent	talent	k1gInSc1	talent
na	na	k7c4	na
bojová	bojový	k2eAgNnPc4d1	bojové
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
Bender	Bender	k1gMnSc1	Bender
Ohýbač	ohýbač	k1gMnSc1	ohýbač
Rodriguez	Rodriguez	k1gMnSc1	Rodriguez
–	–	k?	–
Bender	Bender	k1gInSc1	Bender
<g/>
,	,	kIx,	,
obhroublý	obhroublý	k2eAgInSc1d1	obhroublý
robot	robot	k1gInSc1	robot
vyrobený	vyrobený	k2eAgInSc1d1	vyrobený
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
2996	[number]	k4	2996
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
a	a	k8xC	a
určený	určený	k2eAgInSc4d1	určený
původně	původně	k6eAd1	původně
k	k	k7c3	k
ohýbání	ohýbání	k1gNnSc3	ohýbání
traverz	traverza	k1gFnPc2	traverza
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
firmy	firma	k1gFnSc2	firma
společně	společně	k6eAd1	společně
s	s	k7c7	s
Fryem	Fry	k1gInSc7	Fry
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
netají	tajit	k5eNaImIp3nP	tajit
svou	svůj	k3xOyFgFnSc7	svůj
záští	zášť	k1gFnSc7	zášť
k	k	k7c3	k
lidem	člověk	k1gMnPc3	člověk
a	a	k8xC	a
přivádí	přivádět	k5eAaImIp3nS	přivádět
své	svůj	k3xOyFgMnPc4	svůj
kolegy	kolega	k1gMnPc4	kolega
do	do	k7c2	do
nesnází	nesnáz	k1gFnPc2	nesnáz
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
mu	on	k3xPp3gInSc3	on
to	ten	k3xDgNnSc1	ten
jakkoli	jakkoli	k6eAd1	jakkoli
vadilo	vadit	k5eAaImAgNnS	vadit
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
kouření	kouření	k1gNnSc2	kouření
<g/>
,	,	kIx,	,
neúspěšných	úspěšný	k2eNgMnPc2d1	neúspěšný
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
vaření	vaření	k1gNnSc4	vaření
a	a	k8xC	a
pití	pití	k1gNnSc4	pití
alkoholu	alkohol	k1gInSc2	alkohol
–	–	k?	–
(	(	kIx(	(
<g/>
který	který	k3yQgMnSc1	který
v	v	k7c6	v
Benderově	Benderův	k2eAgInSc6d1	Benderův
případě	případ	k1gInSc6	případ
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
chemickou	chemický	k2eAgFnSc4d1	chemická
reakci	reakce	k1gFnSc4	reakce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dobíjí	dobíjet	k5eAaImIp3nS	dobíjet
jeho	jeho	k3xOp3gFnPc4	jeho
baterie	baterie	k1gFnPc4	baterie
<g/>
)	)	kIx)	)
–	–	k?	–
což	což	k3yRnSc1	což
aspoň	aspoň	k9	aspoň
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc7	jeho
největší	veliký	k2eAgFnSc7d3	veliký
zálibou	záliba	k1gFnSc7	záliba
kradení	kradení	k1gNnSc2	kradení
při	při	k7c6	při
jakékoliv	jakýkoliv	k3yIgFnSc6	jakýkoliv
příležitosti	příležitost	k1gFnSc6	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Prof.	prof.	kA	prof.
Hubert	Hubert	k1gMnSc1	Hubert
J.	J.	kA	J.
Farnsworth	Farnsworth	k1gMnSc1	Farnsworth
–	–	k?	–
je	být	k5eAaImIp3nS	být
Fryovým	Fryův	k2eAgMnSc7d1	Fryův
velmi	velmi	k6eAd1	velmi
vzdáleným	vzdálený	k2eAgMnSc7d1	vzdálený
prasynovcem	prasynovec	k1gMnSc7	prasynovec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
okolo	okolo	k7c2	okolo
160	[number]	k4	160
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Typický	typický	k2eAgMnSc1d1	typický
"	"	kIx"	"
<g/>
šílený	šílený	k2eAgMnSc1d1	šílený
vědec	vědec	k1gMnSc1	vědec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
vynalézáním	vynalézání	k1gNnSc7	vynalézání
pozoruhodných	pozoruhodný	k2eAgFnPc2d1	pozoruhodná
(	(	kIx(	(
<g/>
častěji	často	k6eAd2	často
než	než	k8xS	než
"	"	kIx"	"
<g/>
občas	občas	k6eAd1	občas
<g/>
"	"	kIx"	"
naprosto	naprosto	k6eAd1	naprosto
nesmyslných	smyslný	k2eNgFnPc2d1	nesmyslná
<g/>
)	)	kIx)	)
věcí	věc	k1gFnPc2	věc
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
je	být	k5eAaImIp3nS	být
šéfem	šéf	k1gMnSc7	šéf
zásilkové	zásilkový	k2eAgFnSc2d1	zásilková
společnosti	společnost	k1gFnSc2	společnost
Planet	planeta	k1gFnPc2	planeta
Express	express	k1gInSc1	express
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
založil	založit	k5eAaPmAgMnS	založit
<g/>
,	,	kIx,	,
aby	aby	k9	aby
jejím	její	k3xOp3gNnSc7	její
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
financoval	financovat	k5eAaBmAgInS	financovat
svou	svůj	k3xOyFgFnSc4	svůj
podivnou	podivný	k2eAgFnSc4d1	podivná
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
John	John	k1gMnSc1	John
Zoidberg	Zoidberg	k1gMnSc1	Zoidberg
–	–	k?	–
Zoidberg	Zoidberg	k1gMnSc1	Zoidberg
<g/>
,	,	kIx,	,
tvor	tvor	k1gMnSc1	tvor
mimozemského	mimozemský	k2eAgInSc2d1	mimozemský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
vzezřením	vzezření	k1gNnSc7	vzezření
podobný	podobný	k2eAgInSc4d1	podobný
humru	humr	k1gMnSc3	humr
(	(	kIx(	(
<g/>
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
rase	rasa	k1gFnSc3	rasa
Decapodianů	Decapodian	k1gInPc2	Decapodian
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zaměstnán	zaměstnat	k5eAaPmNgMnS	zaměstnat
jako	jako	k8xC	jako
firemní	firemní	k2eAgMnSc1d1	firemní
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
jeho	jeho	k3xOp3gFnPc4	jeho
znalosti	znalost	k1gFnPc4	znalost
lidské	lidský	k2eAgFnSc2d1	lidská
anatomie	anatomie	k1gFnSc2	anatomie
jsou	být	k5eAaImIp3nP	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
pochybné	pochybný	k2eAgNnSc1d1	pochybné
<g/>
.	.	kIx.	.
</s>
<s>
Ostatními	ostatní	k2eAgInPc7d1	ostatní
je	on	k3xPp3gMnPc4	on
téměř	téměř	k6eAd1	téměř
neustále	neustále	k6eAd1	neustále
přehlížen	přehlížen	k2eAgInSc1d1	přehlížen
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
utiskován	utiskován	k2eAgMnSc1d1	utiskován
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
si	se	k3xPyFc3	se
pochutnává	pochutnávat	k5eAaImIp3nS	pochutnávat
na	na	k7c4	na
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
nechutných	chutný	k2eNgFnPc6d1	nechutná
věcech	věc	k1gFnPc6	věc
<g/>
,	,	kIx,	,
např.	např.	kA	např.
obsazích	obsah	k1gInPc6	obsah
kontejnerů	kontejner	k1gInPc2	kontejner
<g/>
,	,	kIx,	,
vyhozených	vyhozený	k2eAgInPc6d1	vyhozený
zkažených	zkažený	k2eAgInPc6d1	zkažený
jídlech	jídlo	k1gNnPc6	jídlo
a	a	k8xC	a
nejen	nejen	k6eAd1	nejen
to	ten	k3xDgNnSc4	ten
<g/>
...	...	k?	...
Hermes	Hermes	k1gMnSc1	Hermes
Conrad	Conrada	k1gFnPc2	Conrada
–	–	k?	–
je	být	k5eAaImIp3nS	být
Jamajčan	Jamajčan	k1gMnSc1	Jamajčan
<g/>
,	,	kIx,	,
pracující	pracující	k1gMnSc1	pracující
jako	jako	k8xC	jako
manažer	manažer	k1gMnSc1	manažer
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
Planet	planeta	k1gFnPc2	planeta
Express	express	k1gInSc1	express
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
profesionálním	profesionální	k2eAgMnSc7d1	profesionální
byrokratem	byrokrat	k1gMnSc7	byrokrat
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
vášní	vášeň	k1gFnSc7	vášeň
je	být	k5eAaImIp3nS	být
dodržování	dodržování	k1gNnSc1	dodržování
předpisů	předpis	k1gInPc2	předpis
<g/>
,	,	kIx,	,
byrokratických	byrokratický	k2eAgInPc2d1	byrokratický
postupů	postup	k1gInPc2	postup
<g/>
,	,	kIx,	,
vyplňování	vyplňování	k1gNnSc1	vyplňování
formulářů	formulář	k1gInPc2	formulář
a	a	k8xC	a
jakékoli	jakýkoli	k3yIgNnSc4	jakýkoli
předepsané	předepsaný	k2eAgNnSc4d1	předepsané
papírování	papírování	k1gNnSc4	papírování
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
bývalým	bývalý	k2eAgMnSc7d1	bývalý
vrcholovým	vrcholový	k2eAgMnSc7d1	vrcholový
sportovcem	sportovec	k1gMnSc7	sportovec
v	v	k7c6	v
disciplíně	disciplína	k1gFnSc6	disciplína
podlézání	podlézání	k1gNnSc2	podlézání
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Amy	Amy	k1gMnSc2	Amy
Wongová	Wongový	k2eAgFnSc1d1	Wongová
–	–	k?	–
Amy	Amy	k1gFnSc1	Amy
je	být	k5eAaImIp3nS	být
dcerou	dcera	k1gFnSc7	dcera
enormně	enormně	k6eAd1	enormně
bohatých	bohatý	k2eAgMnPc2d1	bohatý
čínských	čínský	k2eAgMnPc2d1	čínský
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
kterým	který	k3yIgFnPc3	který
patří	patřit	k5eAaImIp3nS	patřit
celá	celý	k2eAgFnSc1d1	celá
západní	západní	k2eAgFnSc1d1	západní
polokoule	polokoule	k1gFnSc1	polokoule
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
stáži	stáž	k1gFnSc6	stáž
<g/>
,	,	kIx,	,
na	na	k7c4	na
universitu	universita	k1gFnSc4	universita
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
díky	díky	k7c3	díky
vlivu	vliv	k1gInSc2	vliv
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
dílů	díl	k1gInPc2	díl
seriálu	seriál	k1gInSc2	seriál
Futurama	Futurama	k?	Futurama
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgInPc2	první
72	[number]	k4	72
dílů	díl	k1gInPc2	díl
seriálu	seriál	k1gInSc2	seriál
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
pro	pro	k7c4	pro
Fox	fox	k1gInSc4	fox
Network	network	k1gInSc2	network
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
produkčních	produkční	k2eAgFnPc6d1	produkční
sériích	série	k1gFnPc6	série
<g/>
.	.	kIx.	.
</s>
<s>
Pátou	pátý	k4xOgFnSc4	pátý
sérii	série	k1gFnSc4	série
tvoří	tvořit	k5eAaImIp3nS	tvořit
16	[number]	k4	16
epizod	epizoda	k1gFnPc2	epizoda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vlastně	vlastně	k9	vlastně
4	[number]	k4	4
filmy	film	k1gInPc4	film
<g/>
:	:	kIx,	:
Futurama	Futurama	k?	Futurama
<g/>
:	:	kIx,	:
Benderovo	Benderův	k2eAgNnSc1d1	Benderovo
parádní	parádní	k2eAgNnSc1d1	parádní
terno	terno	k1gNnSc1	terno
<g/>
,	,	kIx,	,
Futurama	Futurama	k?	Futurama
<g/>
:	:	kIx,	:
Milion	milion	k4xCgInSc4	milion
a	a	k8xC	a
jedno	jeden	k4xCgNnSc4	jeden
chapadlo	chapadlo	k1gNnSc4	chapadlo
<g/>
,	,	kIx,	,
Futurama	Futurama	k?	Futurama
<g/>
:	:	kIx,	:
Benderova	Benderův	k2eAgFnSc1d1	Benderova
hra	hra	k1gFnSc1	hra
a	a	k8xC	a
Futurama	Futurama	k?	Futurama
<g/>
:	:	kIx,	:
Fialový	fialový	k2eAgMnSc1d1	fialový
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
rozdělený	rozdělený	k2eAgMnSc1d1	rozdělený
na	na	k7c4	na
4	[number]	k4	4
epizody	epizoda	k1gFnPc4	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2010	[number]	k4	2010
vysílala	vysílat	k5eAaImAgFnS	vysílat
americká	americký	k2eAgFnSc1d1	americká
televize	televize	k1gFnSc1	televize
Comedy	Comeda	k1gMnSc2	Comeda
Central	Central	k1gMnSc2	Central
jednou	jednou	k6eAd1	jednou
týdně	týdně	k6eAd1	týdně
jeden	jeden	k4xCgInSc4	jeden
díl	díl	k1gInSc4	díl
z	z	k7c2	z
nové	nový	k2eAgFnSc2d1	nová
(	(	kIx(	(
<g/>
šesté	šestý	k4xOgFnSc2	šestý
<g/>
)	)	kIx)	)
série	série	k1gFnSc2	série
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
díly	díl	k1gInPc1	díl
byly	být	k5eAaImAgInP	být
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
označovány	označovat	k5eAaImNgInP	označovat
číslem	číslo	k1gNnSc7	číslo
série	série	k1gFnSc2	série
<g/>
,	,	kIx,	,
pracovním	pracovní	k2eAgInSc7d1	pracovní
kódem	kód	k1gInSc7	kód
ACV	ACV	kA	ACV
přiděleným	přidělený	k2eAgFnPc3d1	přidělená
seriálu	seriál	k1gInSc6	seriál
televizí	televize	k1gFnSc7	televize
a	a	k8xC	a
číslem	číslo	k1gNnSc7	číslo
epizody	epizoda	k1gFnSc2	epizoda
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
1ACV01	[number]	k4	1ACV01
-	-	kIx~	-
první	první	k4xOgFnSc2	první
série	série	k1gFnSc2	série
<g/>
,	,	kIx,	,
kód	kód	k1gInSc4	kód
ACV	ACV	kA	ACV
<g/>
,	,	kIx,	,
epizoda	epizoda	k1gFnSc1	epizoda
jedna	jeden	k4xCgFnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Televize	televize	k1gFnPc4	televize
ale	ale	k8xC	ale
epizody	epizoda	k1gFnPc4	epizoda
při	při	k7c6	při
prvním	první	k4xOgInSc6	první
uvedení	uvedení	k1gNnSc6	uvedení
nevysílala	vysílat	k5eNaImAgFnS	vysílat
v	v	k7c6	v
produkčním	produkční	k2eAgNnSc6d1	produkční
pořadí	pořadí	k1gNnSc6	pořadí
a	a	k8xC	a
běžely	běžet	k5eAaImAgFnP	běžet
celkem	celkem	k6eAd1	celkem
v	v	k7c6	v
pěti	pět	k4xCc6	pět
sériích	série	k1gFnPc6	série
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přehledech	přehled	k1gInPc6	přehled
tohoto	tento	k3xDgNnSc2	tento
řazení	řazení	k1gNnSc2	řazení
bývá	bývat	k5eAaImIp3nS	bývat
používáno	používán	k2eAgNnSc1d1	používáno
označení	označení	k1gNnSc1	označení
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
podobě	podoba	k1gFnSc6	podoba
s	s	k7c7	s
prvními	první	k4xOgNnPc7	první
písmeny	písmeno	k1gNnPc7	písmeno
slov	slovo	k1gNnPc2	slovo
sezóna	sezóna	k1gFnSc1	sezóna
a	a	k8xC	a
epizoda	epizoda	k1gFnSc1	epizoda
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
S01E01	S01E01	k1gFnSc2	S01E01
-	-	kIx~	-
série	série	k1gFnSc2	série
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
epizoda	epizoda	k1gFnSc1	epizoda
jedna	jeden	k4xCgFnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
DVD	DVD	kA	DVD
discích	disk	k1gInPc6	disk
vyšel	vyjít	k5eAaPmAgInS	vyjít
seriál	seriál	k1gInSc1	seriál
v	v	k7c6	v
původním	původní	k2eAgNnSc6d1	původní
produkčním	produkční	k2eAgNnSc6d1	produkční
řazení	řazení	k1gNnSc6	řazení
a	a	k8xC	a
čtyřech	čtyři	k4xCgNnPc6	čtyři
kompletech	komplet	k1gInPc6	komplet
<g/>
.	.	kIx.	.
</s>
<s>
Planet	planeta	k1gFnPc2	planeta
Express	express	k1gInSc4	express
je	být	k5eAaImIp3nS	být
firma	firma	k1gFnSc1	firma
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
doručováním	doručování	k1gNnSc7	doručování
zásilek	zásilka	k1gFnPc2	zásilka
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Osazenstvo	osazenstvo	k1gNnSc1	osazenstvo
majitel	majitel	k1gMnSc1	majitel
<g/>
:	:	kIx,	:
profesor	profesor	k1gMnSc1	profesor
Hubert	Hubert	k1gMnSc1	Hubert
J.	J.	kA	J.
Farnsworth	Farnsworth	k1gMnSc1	Farnsworth
kapitán	kapitán	k1gMnSc1	kapitán
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
<g/>
:	:	kIx,	:
Turanga	Turanga	k1gFnSc1	Turanga
Leela	Leela	k1gMnSc1	Leela
poslíček	poslíček	k1gMnSc1	poslíček
<g/>
:	:	kIx,	:
Philip	Philip	k1gMnSc1	Philip
J.	J.	kA	J.
Fry	Fry	k1gMnSc1	Fry
lodní	lodní	k2eAgMnSc1d1	lodní
kuchař	kuchař	k1gMnSc1	kuchař
a	a	k8xC	a
povaleč	povaleč	k1gMnSc1	povaleč
<g/>
:	:	kIx,	:
Bender	Bender	k1gMnSc1	Bender
Ohýbač	ohýbač	k1gMnSc1	ohýbač
Rodriguez	Rodriguez	k1gMnSc1	Rodriguez
stážistka	stážistka	k1gFnSc1	stážistka
<g/>
:	:	kIx,	:
Amy	Amy	k1gFnSc1	Amy
Wong	Wong	k1gMnSc1	Wong
firemní	firemní	k2eAgMnSc1d1	firemní
lékař	lékař	k1gMnSc1	lékař
<g/>
:	:	kIx,	:
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
John	John	k1gMnSc1	John
Zoidberg	Zoidberg	k1gMnSc1	Zoidberg
manažer	manažer	k1gMnSc1	manažer
<g/>
:	:	kIx,	:
Hermes	Hermes	k1gMnSc1	Hermes
Conrad	Conrada	k1gFnPc2	Conrada
údržbář	údržbář	k1gMnSc1	údržbář
<g/>
:	:	kIx,	:
Scruffy	Scruff	k1gInPc1	Scruff
Technické	technický	k2eAgInPc1d1	technický
zázemí	zázemí	k1gNnSc4	zázemí
Firma	firma	k1gFnSc1	firma
používá	používat	k5eAaImIp3nS	používat
kosmickou	kosmický	k2eAgFnSc4d1	kosmická
loď	loď	k1gFnSc4	loď
Planet	planeta	k1gFnPc2	planeta
Express	express	k1gInSc1	express
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
motory	motor	k1gInPc1	motor
využívají	využívat	k5eAaImIp3nP	využívat
temnou	temný	k2eAgFnSc4d1	temná
hmotu	hmota	k1gFnSc4	hmota
(	(	kIx(	(
<g/>
tu	tu	k6eAd1	tu
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
i	i	k9	i
Leelin	Leelin	k2eAgMnSc1d1	Leelin
Diblík	diblík	k1gMnSc1	diblík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Futuramě	Futurama	k1gFnSc6	Futurama
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
různých	různý	k2eAgFnPc2d1	různá
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
jsou	být	k5eAaImIp3nP	být
pojmenovány	pojmenovat	k5eAaPmNgFnP	pojmenovat
podle	podle	k7c2	podle
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
(	(	kIx(	(
<g/>
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
Čapek	čapka	k1gFnPc2	čapka
9	[number]	k4	9
jsou	být	k5eAaImIp3nP	být
roboti	robot	k1gMnPc1	robot
<g/>
,	,	kIx,	,
na	na	k7c4	na
Sicília	Sicílius	k1gMnSc4	Sicílius
8	[number]	k4	8
Mafiáni	mafián	k1gMnPc5	mafián
<g/>
,	,	kIx,	,
na	na	k7c6	na
Kannibalionu	Kannibalion	k1gInSc6	Kannibalion
Kanibalové	kanibal	k1gMnPc1	kanibal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Eternium	Eternium	k1gNnSc1	Eternium
je	být	k5eAaImIp3nS	být
domovská	domovský	k2eAgFnSc1d1	domovská
planeta	planeta	k1gFnSc1	planeta
Nibbloňanů	Nibbloňan	k1gMnPc2	Nibbloňan
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Nibblonians	Nibblonians	k1gInSc1	Nibblonians
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dlouhožijících	dlouhožijící	k2eAgMnPc2d1	dlouhožijící
tříokých	tříoký	k2eAgMnPc2d1	tříoký
mimozemšťanů	mimozemšťan	k1gMnPc2	mimozemšťan
<g/>
.	.	kIx.	.
</s>
<s>
Eternium	Eternium	k1gNnSc1	Eternium
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
epizodách	epizoda	k1gFnPc6	epizoda
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Day	Day	k1gFnSc2	Day
the	the	k?	the
Earth	Earth	k1gInSc1	Earth
Stood	Stooda	k1gFnPc2	Stooda
Stupid	Stupid	k1gInSc1	Stupid
a	a	k8xC	a
The	The	k1gMnSc1	The
Why	Why	k1gMnSc1	Why
Of	Of	k1gMnSc1	Of
Fry	Fry	k1gMnSc1	Fry
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
popsána	popsat	k5eAaPmNgFnS	popsat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
přesně	přesně	k6eAd1	přesně
uprostřed	uprostřed	k7c2	uprostřed
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
"	"	kIx"	"
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
"	"	kIx"	"
<g/>
nepředstavitelné	představitelný	k2eNgInPc1d1	nepředstavitelný
rozměry	rozměr	k1gInPc1	rozměr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
místo	místo	k1gNnSc1	místo
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
Hall	Hall	k1gInSc1	Hall
of	of	k?	of
Forever	Forever	k1gInSc1	Forever
<g/>
,	,	kIx,	,
umístěna	umístěn	k2eAgFnSc1d1	umístěna
"	"	kIx"	"
<g/>
deset	deset	k4xCc4	deset
mil	míle	k1gFnPc2	míle
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
přesného	přesný	k2eAgInSc2d1	přesný
středu	střed	k1gInSc2	střed
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
planetou	planeta	k1gFnSc7	planeta
je	být	k5eAaImIp3nS	být
Amphibios	Amphibios	k1gInSc4	Amphibios
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
bažinatá	bažinatý	k2eAgFnSc1d1	bažinatá
planeta	planeta	k1gFnSc1	planeta
je	být	k5eAaImIp3nS	být
domov	domov	k1gInSc4	domov
obojživelné	obojživelný	k2eAgFnSc2d1	obojživelná
zelenohlavé	zelenohlavý	k2eAgFnSc2d1	zelenohlavý
mimozemské	mimozemský	k2eAgFnSc2d1	mimozemská
rasy	rasa	k1gFnSc2	rasa
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
má	mít	k5eAaImIp3nS	mít
nejméně	málo	k6eAd3	málo
jeden	jeden	k4xCgInSc4	jeden
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Amphibios	Amphibios	k1gInSc1	Amphibios
9	[number]	k4	9
je	být	k5eAaImIp3nS	být
ukázán	ukázat	k5eAaPmNgInS	ukázat
v	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
Kif	Kif	k1gMnSc1	Kif
Gets	Getsa	k1gFnPc2	Getsa
Knocked	Knocked	k1gMnSc1	Knocked
Up	Up	k1gMnSc1	Up
A	a	k8xC	a
Notch	Notch	k1gMnSc1	Notch
<g/>
,	,	kIx,	,
prvním	první	k4xOgInSc6	první
díle	dílo	k1gNnSc6	dílo
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
epizodě	epizoda	k1gFnSc6	epizoda
<g/>
,	,	kIx,	,
poručík	poručík	k1gMnSc1	poručík
Kif	Kif	k1gMnSc1	Kif
Kroker	Kroker	k1gMnSc1	Kroker
přichází	přicházet	k5eAaImIp3nS	přicházet
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
planetu	planeta	k1gFnSc4	planeta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
porodil	porodit	k5eAaPmAgMnS	porodit
své	svůj	k3xOyFgMnPc4	svůj
potomky	potomek	k1gMnPc4	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Milion	milion	k4xCgInSc4	milion
a	a	k8xC	a
jedno	jeden	k4xCgNnSc4	jeden
chapadlo	chapadlo	k1gNnSc4	chapadlo
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
odehraje	odehrát	k5eAaPmIp3nS	odehrát
svatba	svatba	k1gFnSc1	svatba
Kifa	Kifa	k1gFnSc1	Kifa
a	a	k8xC	a
Amy	Amy	k1gFnPc1	Amy
Wongové	Wongový	k2eAgFnPc1d1	Wongová
<g/>
]]	]]	k?	]]
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
<g/>
,	,	kIx,	,
v	v	k7c6	v
ději	děj	k1gInSc6	děj
významnější	významný	k2eAgFnSc1d2	významnější
planetou	planeta	k1gFnSc7	planeta
je	být	k5eAaImIp3nS	být
Planeta	planeta	k1gFnSc1	planeta
Omicron	Omicron	k1gInSc4	Omicron
Persei	Perse	k1gFnSc2	Perse
VIII	VIII	kA	VIII
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
domovská	domovský	k2eAgFnSc1d1	domovská
planeta	planeta	k1gFnSc1	planeta
Omicronianů	Omicronian	k1gMnPc2	Omicronian
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
vzdálena	vzdálit	k5eAaPmNgFnS	vzdálit
cca	cca	kA	cca
1000	[number]	k4	1000
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
1	[number]	k4	1
měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
podnebí	podnebí	k1gNnSc4	podnebí
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
je	být	k5eAaImIp3nS	být
podobné	podobný	k2eAgNnSc1d1	podobné
jako	jako	k9	jako
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
telenovela	telenovela	k1gFnSc1	telenovela
All	All	k1gFnSc2	All
My	my	k3xPp1nPc1	my
Circuits	Circuits	k1gInSc4	Circuits
(	(	kIx(	(
<g/>
na	na	k7c4	na
Prima	prima	k2eAgInSc4d1	prima
Cool	Cool	k1gInSc4	Cool
přeloženo	přeložit	k5eAaPmNgNnS	přeložit
jako	jako	k9	jako
Velmi	velmi	k6eAd1	velmi
křehké	křehký	k2eAgInPc1d1	křehký
obvody	obvod	k1gInPc1	obvod
(	(	kIx(	(
<g/>
parodie	parodie	k1gFnSc1	parodie
na	na	k7c4	na
Velmi	velmi	k6eAd1	velmi
křehké	křehký	k2eAgInPc4d1	křehký
vztahy	vztah	k1gInPc4	vztah
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
točí	točit	k5eAaImIp3nS	točit
kolem	kolem	k7c2	kolem
robota-podnikatele	robotaodnikatel	k1gMnSc2	robota-podnikatel
Calculona	Calculon	k1gMnSc2	Calculon
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
velkou	velký	k2eAgFnSc4d1	velká
lásku	láska	k1gFnSc4	láska
-	-	kIx~	-
robotku	robotka	k1gFnSc4	robotka
Monique	Moniqu	k1gInSc2	Moniqu
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
o	o	k7c4	o
ni	on	k3xPp3gFnSc4	on
musí	muset	k5eAaImIp3nS	muset
bojovat	bojovat	k5eAaImF	bojovat
a	a	k8xC	a
do	do	k7c2	do
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
plete	plést	k5eAaImIp3nS	plést
spousta	spousta	k6eAd1	spousta
obchodních	obchodní	k2eAgMnPc2d1	obchodní
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
New	New	k1gFnSc6	New
New	New	k1gFnSc1	New
Yorku	York	k1gInSc2	York
velice	velice	k6eAd1	velice
populární	populární	k2eAgFnSc1d1	populární
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnPc4	jeho
pravidelné	pravidelný	k2eAgMnPc4d1	pravidelný
diváky	divák	k1gMnPc4	divák
patří	patřit	k5eAaImIp3nP	patřit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
posádka	posádka	k1gFnSc1	posádka
Planet	planeta	k1gFnPc2	planeta
Expressu	express	k1gInSc2	express
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
několikrát	několikrát	k6eAd1	několikrát
zfilmován	zfilmovat	k5eAaPmNgInS	zfilmovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Simpsonovi	Simpson	k1gMnSc3	Simpson
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
odkazů	odkaz	k1gInPc2	odkaz
na	na	k7c4	na
seriál	seriál	k1gInSc4	seriál
Futurama	Futurama	k?	Futurama
<g/>
.	.	kIx.	.
</s>
<s>
Al	ala	k1gFnPc2	ala
Gore	Gore	k1gFnPc2	Gore
použil	použít	k5eAaPmAgInS	použít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
filmovém	filmový	k2eAgInSc6d1	filmový
dokumentu	dokument	k1gInSc6	dokument
Nepříjemná	příjemný	k2eNgFnSc1d1	nepříjemná
pravda	pravda	k9	pravda
krátký	krátký	k2eAgInSc4d1	krátký
úryvek	úryvek	k1gInSc4	úryvek
z	z	k7c2	z
epizody	epizoda	k1gFnSc2	epizoda
Crimes	Crimes	k1gInSc1	Crimes
of	of	k?	of
the	the	k?	the
Hot	hot	k0	hot
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
seriál	seriál	k1gInSc1	seriál
začal	začít	k5eAaPmAgInS	začít
vysílat	vysílat	k5eAaImF	vysílat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
příznivci	příznivec	k1gMnPc1	příznivec
seriálu	seriál	k1gInSc2	seriál
ale	ale	k8xC	ale
vyrobili	vyrobit	k5eAaPmAgMnP	vyrobit
české	český	k2eAgInPc4d1	český
titulky	titulek	k1gInPc4	titulek
a	a	k8xC	a
díky	díky	k7c3	díky
internetu	internet	k1gInSc3	internet
seriál	seriál	k1gInSc1	seriál
koluje	kolovat	k5eAaImIp3nS	kolovat
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
o	o	k7c6	o
vysílání	vysílání	k1gNnSc6	vysílání
seriálu	seriál	k1gInSc2	seriál
několikrát	několikrát	k6eAd1	několikrát
uvažovala	uvažovat	k5eAaImAgFnS	uvažovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
nepříznivým	příznivý	k2eNgFnPc3d1	nepříznivá
obchodním	obchodní	k2eAgFnPc3d1	obchodní
podmínkám	podmínka	k1gFnPc3	podmínka
nakonec	nakonec	k6eAd1	nakonec
seriál	seriál	k1gInSc4	seriál
nezakoupila	zakoupit	k5eNaPmAgFnS	zakoupit
<g/>
.	.	kIx.	.
</s>
<s>
Zástupkyně	zástupkyně	k1gFnSc1	zástupkyně
TV	TV	kA	TV
PRIMA	prima	k2eAgFnSc1d1	prima
na	na	k7c6	na
Festivalu	festival	k1gInSc6	festival
fantazie	fantazie	k1gFnSc2	fantazie
speciál	speciál	k1gInSc1	speciál
2008	[number]	k4	2008
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Futuramu	Futuram	k1gInSc6	Futuram
zakoupili	zakoupit	k5eAaPmAgMnP	zakoupit
vysílací	vysílací	k2eAgNnPc4d1	vysílací
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
všech	všecek	k3xTgInPc2	všecek
140	[number]	k4	140
dílů	díl	k1gInPc2	díl
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c4	na
stanici	stanice	k1gFnSc4	stanice
Prima	prima	k2eAgFnPc2d1	prima
Cool	Coola	k1gFnPc2	Coola
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2009	[number]	k4	2009
až	až	k9	až
2015	[number]	k4	2015
rozdělený	rozdělený	k2eAgInSc4d1	rozdělený
do	do	k7c2	do
deseti	deset	k4xCc2	deset
televizních	televizní	k2eAgFnPc2d1	televizní
řad	řada	k1gFnPc2	řada
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgFnPc2	první
pět	pět	k4xCc4	pět
řad	řad	k1gInSc4	řad
(	(	kIx(	(
<g/>
odpovídající	odpovídající	k2eAgMnSc1d1	odpovídající
produkčním	produkční	k2eAgFnPc3d1	produkční
sériím	série	k1gFnPc3	série
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
72	[number]	k4	72
dílů	díl	k1gInPc2	díl
<g/>
)	)	kIx)	)
uvedla	uvést	k5eAaPmAgFnS	uvést
televize	televize	k1gFnSc1	televize
najednou	najednou	k6eAd1	najednou
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Šestá	šestý	k4xOgFnSc1	šestý
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byly	být	k5eAaImAgFnP	být
4	[number]	k4	4
filmy	film	k1gInPc1	film
Futuramy	Futuram	k1gInPc4	Futuram
rozdělené	rozdělený	k2eAgInPc4d1	rozdělený
(	(	kIx(	(
<g/>
a	a	k8xC	a
sestříhané	sestříhaný	k2eAgNnSc1d1	sestříhané
<g/>
)	)	kIx)	)
na	na	k7c4	na
16	[number]	k4	16
dílů	díl	k1gInPc2	díl
<g/>
,	,	kIx,	,
dorazila	dorazit	k5eAaPmAgFnS	dorazit
na	na	k7c4	na
české	český	k2eAgFnPc4d1	Česká
obrazovky	obrazovka	k1gFnPc4	obrazovka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Sedmá	sedmý	k4xOgFnSc1	sedmý
řada	řada	k1gFnSc1	řada
(	(	kIx(	(
<g/>
produkční	produkční	k2eAgFnSc1d1	produkční
série	série	k1gFnSc1	série
6	[number]	k4	6
<g/>
A	A	kA	A
<g/>
)	)	kIx)	)
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
osmá	osmý	k4xOgFnSc1	osmý
řada	řada	k1gFnSc1	řada
(	(	kIx(	(
<g/>
produkční	produkční	k2eAgFnSc1d1	produkční
série	série	k1gFnSc1	série
6	[number]	k4	6
<g/>
B	B	kA	B
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
devátá	devátý	k4xOgFnSc1	devátý
řada	řada	k1gFnSc1	řada
(	(	kIx(	(
<g/>
produkční	produkční	k2eAgFnSc1d1	produkční
série	série	k1gFnSc1	série
7	[number]	k4	7
<g/>
A	A	kA	A
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
a	a	k8xC	a
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
desátá	desátý	k4xOgFnSc1	desátý
řada	řada	k1gFnSc1	řada
(	(	kIx(	(
<g/>
produkční	produkční	k2eAgFnSc1d1	produkční
série	série	k1gFnSc1	série
7	[number]	k4	7
<g/>
B	B	kA	B
<g/>
)	)	kIx)	)
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2014	[number]	k4	2014
a	a	k8xC	a
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
po	po	k7c6	po
13	[number]	k4	13
dílech	dílo	k1gNnPc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Pořadí	pořadí	k1gNnSc1	pořadí
dílů	díl	k1gInPc2	díl
bylo	být	k5eAaImAgNnS	být
naprosto	naprosto	k6eAd1	naprosto
shodné	shodný	k2eAgNnSc1d1	shodné
s	s	k7c7	s
americkým	americký	k2eAgNnSc7d1	americké
televizním	televizní	k2eAgNnSc7d1	televizní
uvedením	uvedení	k1gNnSc7	uvedení
<g/>
.	.	kIx.	.
</s>
