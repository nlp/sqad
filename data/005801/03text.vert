<s>
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
FF	ff	kA	ff
MU	MU	kA	MU
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
devíti	devět	k4xCc2	devět
fakult	fakulta	k1gFnPc2	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
též	též	k9	též
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
nejstarších	starý	k2eAgFnPc2d3	nejstarší
fakult	fakulta	k1gFnPc2	fakulta
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
při	při	k7c6	při
založení	založení	k1gNnSc6	založení
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Arna	Arne	k1gMnSc2	Arne
Nováka	Novák	k1gMnSc2	Novák
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
počtem	počet	k1gInSc7	počet
zapsaných	zapsaný	k2eAgMnPc2d1	zapsaný
studentů	student	k1gMnPc2	student
je	být	k5eAaImIp3nS	být
nejen	nejen	k6eAd1	nejen
největší	veliký	k2eAgFnSc1d3	veliký
z	z	k7c2	z
filozofických	filozofický	k2eAgFnPc2d1	filozofická
fakult	fakulta	k1gFnPc2	fakulta
ale	ale	k8xC	ale
rovněž	rovněž	k9	rovněž
největší	veliký	k2eAgFnSc7d3	veliký
fakultou	fakulta	k1gFnSc7	fakulta
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgNnSc1d1	zdejší
studium	studium	k1gNnSc1	studium
je	být	k5eAaImIp3nS	být
členěno	členit	k5eAaImNgNnS	členit
na	na	k7c4	na
bakalářské	bakalářský	k2eAgFnPc4d1	Bakalářská
<g/>
,	,	kIx,	,
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
navazující	navazující	k2eAgInPc4d1	navazující
magisterské	magisterský	k2eAgInPc4d1	magisterský
a	a	k8xC	a
doktorské	doktorský	k2eAgInPc4d1	doktorský
studijní	studijní	k2eAgInPc4d1	studijní
programy	program	k1gInPc4	program
<g/>
,	,	kIx,	,
fakulta	fakulta	k1gFnSc1	fakulta
nabízí	nabízet	k5eAaImIp3nS	nabízet
jednooborová	jednooborový	k2eAgFnSc1d1	jednooborová
i	i	k8xC	i
dvouoborová	dvouoborový	k2eAgNnPc1d1	dvouoborový
studia	studio	k1gNnPc1	studio
<g/>
,	,	kIx,	,
také	také	k9	také
dálkovou	dálkový	k2eAgFnSc4d1	dálková
formu	forma	k1gFnSc4	forma
a	a	k8xC	a
řadu	řada	k1gFnSc4	řada
studií	studie	k1gFnPc2	studie
celoživotního	celoživotní	k2eAgNnSc2d1	celoživotní
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
akademického	akademický	k2eAgInSc2d1	akademický
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
/	/	kIx~	/
<g/>
2016	[number]	k4	2016
zde	zde	k6eAd1	zde
studovalo	studovat	k5eAaImAgNnS	studovat
celkem	celkem	k6eAd1	celkem
8	[number]	k4	8
879	[number]	k4	879
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
děkanem	děkan	k1gMnSc7	děkan
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
profesor	profesor	k1gMnSc1	profesor
pedagogiky	pedagogika	k1gFnSc2	pedagogika
Milan	Milan	k1gMnSc1	Milan
Pol.	Pol.	k1gFnPc2	Pol.
Vznik	vznik	k1gInSc4	vznik
filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
se	s	k7c7	s
založením	založení	k1gNnSc7	založení
celé	celý	k2eAgFnSc2d1	celá
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Československa	Československo	k1gNnSc2	Československo
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
realizaci	realizace	k1gFnSc3	realizace
dlouho	dlouho	k6eAd1	dlouho
trvající	trvající	k2eAgFnSc1d1	trvající
diskuse	diskuse	k1gFnSc1	diskuse
o	o	k7c6	o
druhé	druhý	k4xOgFnSc6	druhý
české	český	k2eAgFnSc6d1	Česká
univerzitě	univerzita	k1gFnSc6	univerzita
a	a	k8xC	a
již	již	k6eAd1	již
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1919	[number]	k4	1919
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
zákon	zákon	k1gInSc1	zákon
o	o	k7c4	o
založení	založení	k1gNnSc4	založení
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
po	po	k7c6	po
prezidentu	prezident	k1gMnSc3	prezident
Tomáši	Tomáš	k1gMnSc3	Tomáš
Garrigue	Garrigue	k1gNnSc2	Garrigue
Masarykovi	Masaryk	k1gMnSc3	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
šestičlenná	šestičlenný	k2eAgFnSc1d1	šestičlenná
komise	komise	k1gFnSc1	komise
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
pracovníků	pracovník	k1gMnPc2	pracovník
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
a	a	k8xC	a
odborníků	odborník	k1gMnPc2	odborník
z	z	k7c2	z
moravských	moravský	k2eAgFnPc2d1	Moravská
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
dostala	dostat	k5eAaPmAgFnS	dostat
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
sestavit	sestavit	k5eAaPmF	sestavit
základ	základ	k1gInSc4	základ
profesorského	profesorský	k2eAgInSc2d1	profesorský
sboru	sbor	k1gInSc2	sbor
nové	nový	k2eAgFnSc2d1	nová
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
byli	být	k5eAaImAgMnP	být
na	na	k7c4	na
tato	tento	k3xDgNnPc4	tento
místa	místo	k1gNnPc4	místo
ustanoveni	ustanoven	k2eAgMnPc1d1	ustanoven
<g/>
:	:	kIx,	:
pro	pro	k7c4	pro
germánskou	germánský	k2eAgFnSc4d1	germánská
jazykovědu	jazykověda	k1gFnSc4	jazykověda
Antonín	Antonín	k1gMnSc1	Antonín
Beer	Beer	k1gMnSc1	Beer
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
německou	německý	k2eAgFnSc4d1	německá
literaturu	literatura	k1gFnSc4	literatura
Jan	Jan	k1gMnSc1	Jan
Krejčí	Krejčí	k1gMnSc1	Krejčí
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
všeobecné	všeobecný	k2eAgFnPc4d1	všeobecná
dějiny	dějiny	k1gFnPc4	dějiny
Julius	Julius	k1gMnSc1	Julius
Glücklich	Glücklich	k1gMnSc1	Glücklich
s	s	k7c7	s
Bohumilem	Bohumil	k1gMnSc7	Bohumil
Navrátilem	Navrátil	k1gMnSc7	Navrátil
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
románskou	románský	k2eAgFnSc4d1	románská
filologii	filologie	k1gFnSc4	filologie
Prokop	Prokop	k1gMnSc1	Prokop
Miroslav	Miroslav	k1gMnSc1	Miroslav
Haškovec	Haškovec	k1gMnSc1	Haškovec
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Titzem	Titz	k1gMnSc7	Titz
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
anglickou	anglický	k2eAgFnSc4d1	anglická
literaturu	literatura	k1gFnSc4	literatura
František	František	k1gMnSc1	František
Chudoba	Chudoba	k1gMnSc1	Chudoba
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
klasickou	klasický	k2eAgFnSc4d1	klasická
filologii	filologie	k1gFnSc4	filologie
František	František	k1gMnSc1	František
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
českou	český	k2eAgFnSc4d1	Česká
literaturu	literatura	k1gFnSc4	literatura
Arne	Arne	k1gMnSc1	Arne
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
českou	český	k2eAgFnSc4d1	Česká
řeč	řeč	k1gFnSc4	řeč
a	a	k8xC	a
literaturu	literatura	k1gFnSc4	literatura
Stanislav	Stanislav	k1gMnSc1	Stanislav
Souček	Souček	k1gMnSc1	Souček
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
československé	československý	k2eAgFnPc4d1	Československá
dějiny	dějiny	k1gFnPc4	dějiny
Rudolf	Rudolf	k1gMnSc1	Rudolf
Urbánek	Urbánek	k1gMnSc1	Urbánek
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
slovanskou	slovanský	k2eAgFnSc4d1	Slovanská
filologii	filologie	k1gFnSc4	filologie
Václav	Václav	k1gMnSc1	Václav
Vondrák	Vondrák	k1gMnSc1	Vondrák
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
pro	pro	k7c4	pro
filosofii	filosofie	k1gFnSc4	filosofie
a	a	k8xC	a
estetiku	estetika	k1gFnSc4	estetika
Otakar	Otakar	k1gMnSc1	Otakar
Zich	Zich	k1gMnSc1	Zich
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
děkanem	děkan	k1gMnSc7	děkan
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
a	a	k8xC	a
na	na	k7c4	na
schůzi	schůze	k1gFnSc4	schůze
profesorského	profesorský	k2eAgInSc2d1	profesorský
sboru	sbor	k1gInSc2	sbor
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1920	[number]	k4	1920
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
Bohumil	Bohumil	k1gMnSc1	Bohumil
Navrátil	Navrátil	k1gMnSc1	Navrátil
<g/>
.	.	kIx.	.
</s>
<s>
Výuku	výuka	k1gFnSc4	výuka
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
zahájit	zahájit	k5eAaPmF	zahájit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
fakulta	fakulta	k1gFnSc1	fakulta
nezískala	získat	k5eNaPmAgFnS	získat
při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
založení	založení	k1gNnSc6	založení
žádnou	žádný	k3yNgFnSc4	žádný
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
budovu	budova	k1gFnSc4	budova
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
po	po	k7c6	po
několika	několik	k4yIc6	několik
neúspěšných	úspěšný	k2eNgInPc6d1	neúspěšný
návrzích	návrh	k1gInPc6	návrh
a	a	k8xC	a
pokusech	pokus	k1gInPc6	pokus
jí	jíst	k5eAaImIp3nS	jíst
byla	být	k5eAaImAgFnS	být
přidělena	přidělen	k2eAgFnSc1d1	přidělena
budova	budova	k1gFnSc1	budova
bývalého	bývalý	k2eAgInSc2d1	bývalý
sirotčince	sirotčinec	k1gInSc2	sirotčinec
na	na	k7c6	na
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
Falkensteinerově	Falkensteinerův	k2eAgFnSc6d1	Falkensteinerův
dnes	dnes	k6eAd1	dnes
Gorkého	Gorkého	k2eAgFnSc6d1	Gorkého
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
používá	používat	k5eAaImIp3nS	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc2	tento
budovy	budova	k1gFnSc2	budova
se	se	k3xPyFc4	se
však	však	k9	však
domáhalo	domáhat	k5eAaImAgNnS	domáhat
také	také	k9	také
ředitelství	ředitelství	k1gNnSc1	ředitelství
státních	státní	k2eAgFnPc2d1	státní
drah	draha	k1gFnPc2	draha
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
po	po	k7c4	po
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
obývaly	obývat	k5eAaImAgInP	obývat
společné	společný	k2eAgInPc1d1	společný
prostory	prostor	k1gInPc1	prostor
obě	dva	k4xCgFnPc1	dva
instituce	instituce	k1gFnPc1	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
problémům	problém	k1gInPc3	problém
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zahájení	zahájení	k1gNnSc3	zahájení
výuky	výuka	k1gFnSc2	výuka
až	až	k9	až
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
si	se	k3xPyFc3	se
fakulta	fakulta	k1gFnSc1	fakulta
musela	muset	k5eAaImAgFnS	muset
propůjčovat	propůjčovat	k5eAaImF	propůjčovat
prostory	prostora	k1gFnPc4	prostora
od	od	k7c2	od
sedmi	sedm	k4xCc2	sedm
dalších	další	k2eAgFnPc2d1	další
institucí	instituce	k1gFnPc2	instituce
a	a	k8xC	a
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
tvoří	tvořit	k5eAaImIp3nS	tvořit
komplex	komplex	k1gInSc1	komplex
budov	budova	k1gFnPc2	budova
nacházejících	nacházející	k2eAgFnPc2d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Arna	Arne	k1gMnSc2	Arne
Nováka	Novák	k1gMnSc2	Novák
1	[number]	k4	1
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
areál	areál	k1gInSc1	areál
tvoří	tvořit	k5eAaImIp3nS	tvořit
budovy	budova	k1gFnPc4	budova
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
a	a	k8xC	a
D	D	kA	D
společně	společně	k6eAd1	společně
s	s	k7c7	s
budovou	budova	k1gFnSc7	budova
ústřední	ústřední	k2eAgFnSc2d1	ústřední
knihovny	knihovna	k1gFnSc2	knihovna
(	(	kIx(	(
<g/>
budova	budova	k1gFnSc1	budova
F	F	kA	F
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
děkanát	děkanát	k1gInSc1	děkanát
fakulty	fakulta	k1gFnSc2	fakulta
a	a	k8xC	a
studijní	studijní	k2eAgNnSc4d1	studijní
oddělení	oddělení	k1gNnSc4	oddělení
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
své	svůj	k3xOyFgFnSc2	svůj
velikosti	velikost	k1gFnSc2	velikost
co	co	k3yQnSc1	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
studentů	student	k1gMnPc2	student
a	a	k8xC	a
množství	množství	k1gNnSc2	množství
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
ústavů	ústav	k1gInPc2	ústav
a	a	k8xC	a
kateder	katedra	k1gFnPc2	katedra
využívá	využívat	k5eAaPmIp3nS	využívat
filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
další	další	k2eAgFnSc2d1	další
budovy	budova	k1gFnSc2	budova
různě	různě	k6eAd1	různě
rozmístěné	rozmístěný	k2eAgFnPc4d1	rozmístěná
v	v	k7c6	v
blízkém	blízký	k2eAgNnSc6d1	blízké
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
A	a	k9	a
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc7d3	nejstarší
budovou	budova	k1gFnSc7	budova
fakultního	fakultní	k2eAgInSc2d1	fakultní
areálu	areál	k1gInSc2	areál
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
letech	let	k1gInPc6	let
1871	[number]	k4	1871
<g/>
–	–	k?	–
<g/>
1872	[number]	k4	1872
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
chlapeckého	chlapecký	k2eAgInSc2d1	chlapecký
sirotčince	sirotčinec	k1gInSc2	sirotčinec
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgFnSc6d1	vzniklá
Masarykově	Masarykův	k2eAgFnSc6d1	Masarykova
univerzitě	univerzita	k1gFnSc6	univerzita
byla	být	k5eAaImAgFnS	být
přidělena	přidělen	k2eAgFnSc1d1	přidělena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
jako	jako	k8xS	jako
sídlo	sídlo	k1gNnSc4	sídlo
pro	pro	k7c4	pro
filozofickou	filozofický	k2eAgFnSc4d1	filozofická
fakultu	fakulta	k1gFnSc4	fakulta
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
fakulta	fakulta	k1gFnSc1	fakulta
musela	muset	k5eAaImAgFnS	muset
zároveň	zároveň	k6eAd1	zároveň
dělit	dělit	k5eAaImF	dělit
s	s	k7c7	s
ředitelstvím	ředitelství	k1gNnSc7	ředitelství
státních	státní	k2eAgFnPc2d1	státní
drah	draha	k1gFnPc2	draha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nutných	nutný	k2eAgFnPc6d1	nutná
stavebních	stavební	k2eAgFnPc6d1	stavební
úpravách	úprava	k1gFnPc6	úprava
zde	zde	k6eAd1	zde
započala	započnout	k5eAaPmAgFnS	započnout
výuka	výuka	k1gFnSc1	výuka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
ústřední	ústřední	k2eAgFnSc2d1	ústřední
knihovny	knihovna	k1gFnSc2	knihovna
je	být	k5eAaImIp3nS	být
nejmladší	mladý	k2eAgFnSc7d3	nejmladší
a	a	k8xC	a
nejnápadnější	nápadní	k2eAgFnSc7d3	nápadní
stavbou	stavba	k1gFnSc7	stavba
umístěnou	umístěný	k2eAgFnSc7d1	umístěná
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Postavena	postaven	k2eAgFnSc1d1	postavena
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
2000	[number]	k4	2000
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
architektů	architekt	k1gMnPc2	architekt
Ladislava	Ladislav	k1gMnSc2	Ladislav
Kuby	Kuba	k1gMnSc2	Kuba
a	a	k8xC	a
Tomáše	Tomáš	k1gMnSc2	Tomáš
Pilaře	Pilař	k1gMnSc2	Pilař
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
knihovny	knihovna	k1gFnSc2	knihovna
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
získala	získat	k5eAaPmAgFnS	získat
zároveň	zároveň	k6eAd1	zároveň
novostavba	novostavba	k1gFnSc1	novostavba
ocenění	ocenění	k1gNnSc2	ocenění
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Obce	obec	k1gFnSc2	obec
architektů	architekt	k1gMnPc2	architekt
<g/>
,	,	kIx,	,
Cenu	cena	k1gFnSc4	cena
unie	unie	k1gFnSc2	unie
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
a	a	k8xC	a
Cenu	cena	k1gFnSc4	cena
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Kapacita	kapacita	k1gFnSc1	kapacita
knihovny	knihovna	k1gFnSc2	knihovna
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
225	[number]	k4	225
000	[number]	k4	000
svazků	svazek	k1gInPc2	svazek
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
výběru	výběr	k1gInSc6	výběr
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
okolo	okolo	k7c2	okolo
135	[number]	k4	135
000	[number]	k4	000
publikací	publikace	k1gFnPc2	publikace
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
pojmou	pojmout	k5eAaPmIp3nP	pojmout
studovny	studovna	k1gFnPc1	studovna
ve	v	k7c6	v
čtyřech	čtyři	k4xCgNnPc6	čtyři
podlažích	podlaží	k1gNnPc6	podlaží
400	[number]	k4	400
čtenářů	čtenář	k1gMnPc2	čtenář
<g/>
.	.	kIx.	.
</s>
<s>
Knihovna	knihovna	k1gFnSc1	knihovna
také	také	k9	také
provozuje	provozovat	k5eAaImIp3nS	provozovat
digitální	digitální	k2eAgFnSc4d1	digitální
knihovnu	knihovna	k1gFnSc4	knihovna
Arna	Arne	k1gMnSc2	Arne
Nováka	Novák	k1gMnSc2	Novák
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
zpřístupňuje	zpřístupňovat	k5eAaImIp3nS	zpřístupňovat
Novákovo	Novákův	k2eAgNnSc1d1	Novákovo
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
dílo	dílo	k1gNnSc1	dílo
vytvořené	vytvořený	k2eAgNnSc1d1	vytvořené
za	za	k7c2	za
jeho	jeho	k3xOp3gMnPc2	jeho
více	hodně	k6eAd2	hodně
než	než	k8xS	než
čtyřicetileté	čtyřicetiletý	k2eAgFnSc2d1	čtyřicetiletá
praxe	praxe	k1gFnSc2	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
přibližně	přibližně	k6eAd1	přibližně
13	[number]	k4	13
000	[number]	k4	000
zdigitalizovaných	zdigitalizovaný	k2eAgFnPc2d1	zdigitalizovaná
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
spojené	spojený	k2eAgFnPc4d1	spojená
dvě	dva	k4xCgFnPc4	dva
budovy	budova	k1gFnPc4	budova
na	na	k7c6	na
adrese	adresa	k1gFnSc6	adresa
Veveří	veveří	k2eAgFnSc6d1	veveří
26	[number]	k4	26
a	a	k8xC	a
28	[number]	k4	28
sousedící	sousedící	k2eAgMnSc1d1	sousedící
s	s	k7c7	s
Moravským	moravský	k2eAgNnSc7d1	Moravské
gymnáziem	gymnázium	k1gNnSc7	gymnázium
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
č.	č.	k?	č.
<g/>
p.	p.	k?	p.
48	[number]	k4	48
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
II	II	kA	II
<g/>
.	.	kIx.	.
střední	střední	k2eAgFnSc1d1	střední
smíšená	smíšený	k2eAgFnSc1d1	smíšená
škola	škola	k1gFnSc1	škola
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
z	z	k7c2	z
Měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
chlapecké	chlapecký	k2eAgFnSc2d1	chlapecká
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
přemístila	přemístit	k5eAaPmAgFnS	přemístit
z	z	k7c2	z
ulice	ulice	k1gFnSc2	ulice
Jánské	Jánská	k1gFnSc2	Jánská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budově	budova	k1gFnSc6	budova
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
knihovny	knihovna	k1gFnSc2	knihovna
semináře	seminář	k1gInSc2	seminář
dějin	dějiny	k1gFnPc2	dějiny
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
Mn	Mn	k1gFnPc2	Mn
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
Moravském	moravský	k2eAgNnSc6d1	Moravské
náměstí	náměstí	k1gNnSc6	náměstí
č.	č.	k?	č.
13	[number]	k4	13
a	a	k8xC	a
sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
katedra	katedra	k1gFnSc1	katedra
UNESCO	UNESCO	kA	UNESCO
pro	pro	k7c4	pro
muzeologii	muzeologie	k1gFnSc4	muzeologie
a	a	k8xC	a
světové	světový	k2eAgNnSc4d1	světové
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
jarního	jarní	k2eAgInSc2d1	jarní
semestru	semestr	k1gInSc2	semestr
2014	[number]	k4	2014
využívá	využívat	k5eAaImIp3nS	využívat
filosofická	filosofický	k2eAgFnSc1d1	filosofická
fakulta	fakulta	k1gFnSc1	fakulta
budovu	budova	k1gFnSc4	budova
Vysokého	vysoký	k2eAgNnSc2d1	vysoké
učení	učení	k1gNnSc2	učení
technického	technický	k2eAgNnSc2d1	technické
mezi	mezi	k7c7	mezi
ulicemi	ulice	k1gFnPc7	ulice
Údolní	údolní	k2eAgFnSc7d1	údolní
<g/>
,	,	kIx,	,
Úvoz	úvoz	k1gInSc1	úvoz
a	a	k8xC	a
Tvrdého	Tvrdého	k2eAgFnSc1d1	Tvrdého
(	(	kIx(	(
<g/>
bývalá	bývalý	k2eAgFnSc1d1	bývalá
strojní	strojní	k2eAgFnSc1d1	strojní
fakulta	fakulta	k1gFnSc1	fakulta
Německé	německý	k2eAgFnSc2d1	německá
techniky	technika	k1gFnSc2	technika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
Ústav	ústav	k1gInSc1	ústav
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
Ústav	ústav	k1gInSc1	ústav
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
knihovnictví	knihovnictví	k1gNnSc2	knihovnictví
<g/>
,	,	kIx,	,
Ústav	ústav	k1gInSc1	ústav
jazykovědy	jazykověda	k1gFnSc2	jazykověda
a	a	k8xC	a
baltistiky	baltistika	k1gFnSc2	baltistika
a	a	k8xC	a
Centrum	centrum	k1gNnSc1	centrum
počítačové	počítačový	k2eAgFnSc2d1	počítačová
lingvistiky	lingvistika	k1gFnSc2	lingvistika
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
samosprávné	samosprávný	k2eAgFnPc4d1	samosprávná
akademické	akademický	k2eAgFnPc4d1	akademická
orgány	orgány	k1gFnPc4	orgány
fakulty	fakulta	k1gFnSc2	fakulta
patří	patřit	k5eAaImIp3nS	patřit
akademický	akademický	k2eAgInSc1d1	akademický
senát	senát	k1gInSc1	senát
<g/>
,	,	kIx,	,
děkan	děkan	k1gMnSc1	děkan
<g/>
,	,	kIx,	,
vědecká	vědecký	k2eAgFnSc1d1	vědecká
rada	rada	k1gFnSc1	rada
a	a	k8xC	a
disciplinární	disciplinární	k2eAgFnSc1d1	disciplinární
komise	komise	k1gFnSc1	komise
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nim	on	k3xPp3gInPc3	on
náleží	náležet	k5eAaImIp3nP	náležet
kolegium	kolegium	k1gNnSc4	kolegium
děkana	děkan	k1gMnSc2	děkan
<g/>
,	,	kIx,	,
komise	komise	k1gFnSc1	komise
děkana	děkan	k1gMnSc2	děkan
a	a	k8xC	a
tajemník	tajemník	k1gMnSc1	tajemník
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
děkanů	děkan	k1gMnPc2	děkan
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Děkan	děkan	k1gMnSc1	děkan
je	být	k5eAaImIp3nS	být
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
představitelem	představitel	k1gMnSc7	představitel
fakulty	fakulta	k1gFnSc2	fakulta
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pověřen	pověřit	k5eAaPmNgMnS	pověřit
jednáním	jednání	k1gNnSc7	jednání
a	a	k8xC	a
rozhodováním	rozhodování	k1gNnSc7	rozhodování
jménem	jméno	k1gNnSc7	jméno
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
záležitostech	záležitost	k1gFnPc6	záležitost
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
fakultnímu	fakultní	k2eAgInSc3d1	fakultní
akademickému	akademický	k2eAgInSc3d1	akademický
senátu	senát	k1gInSc3	senát
a	a	k8xC	a
rektorovi	rektorův	k2eAgMnPc1d1	rektorův
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgNnSc1d1	volební
období	období	k1gNnSc1	období
děkana	děkan	k1gMnSc2	děkan
je	být	k5eAaImIp3nS	být
tříleté	tříletý	k2eAgNnSc1d1	tříleté
a	a	k8xC	a
tatáž	týž	k3xTgFnSc1	týž
osoba	osoba	k1gFnSc1	osoba
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
děkana	děkan	k1gMnSc2	děkan
pouze	pouze	k6eAd1	pouze
dvě	dva	k4xCgFnPc4	dva
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
jdoucí	jdoucí	k2eAgNnSc4d1	jdoucí
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Děkana	děkan	k1gMnSc4	děkan
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
akademického	akademický	k2eAgInSc2d1	akademický
senátu	senát	k1gInSc2	senát
rektor	rektor	k1gMnSc1	rektor
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
může	moct	k5eAaImIp3nS	moct
rovněž	rovněž	k9	rovněž
děkana	děkan	k1gMnSc2	děkan
odvolat	odvolat	k5eAaPmF	odvolat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
akademického	akademický	k2eAgInSc2d1	akademický
senátu	senát	k1gInSc2	senát
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
děkan	děkan	k1gMnSc1	děkan
vážným	vážný	k2eAgInSc7d1	vážný
způsobem	způsob	k1gInSc7	způsob
poškozuje	poškozovat	k5eAaImIp3nS	poškozovat
zájmy	zájem	k1gInPc4	zájem
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
nebo	nebo	k8xC	nebo
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
závažně	závažně	k6eAd1	závažně
neplní	plnit	k5eNaImIp3nS	plnit
své	svůj	k3xOyFgFnPc4	svůj
povinnosti	povinnost	k1gFnPc4	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
odvolání	odvolání	k1gNnSc3	odvolání
děkana	děkan	k1gMnSc2	děkan
nutné	nutný	k2eAgNnSc1d1	nutné
dvoutřetinové	dvoutřetinový	k2eAgNnSc1d1	dvoutřetinové
usnesení	usnesení	k1gNnSc1	usnesení
fakultního	fakultní	k2eAgInSc2d1	fakultní
akademického	akademický	k2eAgInSc2d1	akademický
senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
proděkany	proděkan	k1gMnPc7	proděkan
náleží	náležet	k5eAaImIp3nP	náležet
děkan	děkan	k1gMnSc1	děkan
k	k	k7c3	k
akademickým	akademický	k2eAgMnPc3d1	akademický
funkcionářům	funkcionář	k1gMnPc3	funkcionář
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgMnPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
promoce	promoce	k1gFnSc2	promoce
také	také	k9	také
promotor	promotor	k1gMnSc1	promotor
<g/>
.	.	kIx.	.
</s>
<s>
Děkan	děkan	k1gMnSc1	děkan
má	mít	k5eAaImIp3nS	mít
rovněž	rovněž	k9	rovněž
pravomoc	pravomoc	k1gFnSc4	pravomoc
udělit	udělit	k5eAaPmF	udělit
zlatou	zlatý	k2eAgFnSc4d1	zlatá
<g/>
,	,	kIx,	,
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
nebo	nebo	k8xC	nebo
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
pracovníkům	pracovník	k1gMnPc3	pracovník
a	a	k8xC	a
studentům	student	k1gMnPc3	student
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
zasloužili	zasloužit	k5eAaPmAgMnP	zasloužit
rozvoj	rozvoj	k1gInSc4	rozvoj
fakulty	fakulta	k1gFnSc2	fakulta
nebo	nebo	k8xC	nebo
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
období	období	k1gNnSc4	období
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
funkci	funkce	k1gFnSc4	funkce
děkana	děkan	k1gMnSc4	děkan
profesor	profesor	k1gMnSc1	profesor
pedagogiky	pedagogika	k1gFnSc2	pedagogika
Milan	Milan	k1gMnSc1	Milan
Pol.	Pol.	k1gMnSc1	Pol.
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
programovém	programový	k2eAgNnSc6d1	programové
prohlášení	prohlášení	k1gNnSc6	prohlášení
se	se	k3xPyFc4	se
zavázal	zavázat	k5eAaPmAgMnS	zavázat
posilovat	posilovat	k5eAaImF	posilovat
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
komunikaci	komunikace	k1gFnSc4	komunikace
i	i	k8xC	i
komunikaci	komunikace	k1gFnSc4	komunikace
filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
fakultami	fakulta	k1gFnPc7	fakulta
<g/>
,	,	kIx,	,
úspěšnější	úspěšný	k2eAgNnPc1d2	úspěšnější
dosahování	dosahování	k1gNnPc1	dosahování
na	na	k7c4	na
grantovou	grantový	k2eAgFnSc4d1	Grantová
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
posílení	posílení	k1gNnSc4	posílení
internacionalizace	internacionalizace	k1gFnSc2	internacionalizace
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Proděkani	proděkan	k1gMnPc1	proděkan
jednají	jednat	k5eAaImIp3nP	jednat
jménem	jméno	k1gNnSc7	jméno
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
konkrétních	konkrétní	k2eAgInPc6d1	konkrétní
jim	on	k3xPp3gMnPc3	on
svěřených	svěřený	k2eAgFnPc6d1	svěřená
záležitostech	záležitost	k1gFnPc6	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
jmenování	jmenování	k1gNnSc4	jmenování
a	a	k8xC	a
odvoláváni	odvolávat	k5eAaImNgMnP	odvolávat
děkanem	děkan	k1gMnSc7	děkan
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
po	po	k7c6	po
předchozím	předchozí	k2eAgNnSc6d1	předchozí
vyjádření	vyjádření	k1gNnSc6	vyjádření
akademického	akademický	k2eAgInSc2d1	akademický
senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Akademický	akademický	k2eAgInSc1d1	akademický
senát	senát	k1gInSc1	senát
také	také	k9	také
stanoví	stanovit	k5eAaPmIp3nS	stanovit
počet	počet	k1gInSc1	počet
proděkanů	proděkan	k1gMnPc2	proděkan
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
funkční	funkční	k2eAgFnPc4d1	funkční
povinnosti	povinnost	k1gFnPc4	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
proděkanů	proděkan	k1gMnPc2	proděkan
je	být	k5eAaImIp3nS	být
jmenován	jmenován	k2eAgMnSc1d1	jmenován
jako	jako	k8xC	jako
zástupce	zástupce	k1gMnSc1	zástupce
děkana	děkan	k1gMnSc2	děkan
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
děkanem	děkan	k1gMnSc7	děkan
<g/>
,	,	kIx,	,
tajemníkem	tajemník	k1gMnSc7	tajemník
fakulty	fakulta	k1gFnSc2	fakulta
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
studentem	student	k1gMnSc7	student
delegovaným	delegovaný	k2eAgMnSc7d1	delegovaný
akademickým	akademický	k2eAgInSc7d1	akademický
senátem	senát	k1gInSc7	senát
fakulty	fakulta	k1gFnSc2	fakulta
tvoří	tvořit	k5eAaImIp3nP	tvořit
proděkani	proděkan	k1gMnPc1	proděkan
kolegium	kolegium	k1gNnSc4	kolegium
děkana	děkan	k1gMnSc2	děkan
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
poradní	poradní	k2eAgInSc1d1	poradní
orgán	orgán	k1gInSc1	orgán
děkana	děkan	k1gMnSc2	děkan
<g/>
.	.	kIx.	.
</s>
<s>
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
má	mít	k5eAaImIp3nS	mít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
těchto	tento	k3xDgMnPc2	tento
šest	šest	k4xCc1	šest
proděkanů	proděkan	k1gMnPc2	proděkan
<g/>
:	:	kIx,	:
Petr	Petr	k1gMnSc1	Petr
Kyloušek	Kyloušek	k1gMnSc1	Kyloušek
–	–	k?	–
proděkan	proděkan	k1gMnSc1	proděkan
pro	pro	k7c4	pro
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
doktorské	doktorský	k2eAgNnSc4d1	doktorské
studium	studium	k1gNnSc4	studium
Rostislav	Rostislava	k1gFnPc2	Rostislava
Niederle	Niederl	k1gMnSc2	Niederl
–	–	k?	–
proděkan	proděkan	k1gMnSc1	proděkan
pro	pro	k7c4	pro
bakalářské	bakalářský	k2eAgNnSc4d1	bakalářské
a	a	k8xC	a
magisterské	magisterský	k2eAgNnSc4d1	magisterské
studium	studium	k1gNnSc4	studium
Tomáš	Tomáš	k1gMnSc1	Tomáš
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
–	–	k?	–
proděkan	proděkan	k1gMnSc1	proděkan
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
vztahy	vztah	k1gInPc4	vztah
Lukáš	Lukáš	k1gMnSc1	Lukáš
Fasora	Fasor	k1gMnSc2	Fasor
–	–	k?	–
proděkan	proděkan	k1gMnSc1	proděkan
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
Jana	Jana	k1gFnSc1	Jana
Horáková	Horáková	k1gFnSc1	Horáková
–	–	k?	–
proděkanka	proděkanka	k1gFnSc1	proděkanka
pro	pro	k7c4	pro
ediční	ediční	k2eAgFnSc4d1	ediční
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
veřejností	veřejnost	k1gFnSc7	veřejnost
Petr	Petr	k1gMnSc1	Petr
Škyřík	Škyřík	k1gMnSc1	Škyřík
–	–	k?	–
proděkan	proděkan	k1gMnSc1	proděkan
pro	pro	k7c4	pro
přijímací	přijímací	k2eAgNnSc4d1	přijímací
řízení	řízení	k1gNnSc4	řízení
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
studijních	studijní	k2eAgInPc2d1	studijní
programů	program	k1gInPc2	program
Funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
proděkanů	proděkan	k1gMnPc2	proděkan
začíná	začínat	k5eAaImIp3nS	začínat
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
společně	společně	k6eAd1	společně
s	s	k7c7	s
funkčním	funkční	k2eAgNnSc7d1	funkční
obdobím	období	k1gNnSc7	období
děkana	děkan	k1gMnSc2	děkan
<g/>
.	.	kIx.	.
</s>
<s>
Akademický	akademický	k2eAgInSc1d1	akademický
senát	senát	k1gInSc1	senát
filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
19	[number]	k4	19
členů	člen	k1gInPc2	člen
zvolených	zvolený	k2eAgInPc2d1	zvolený
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
členů	člen	k1gMnPc2	člen
akademické	akademický	k2eAgFnSc2d1	akademická
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
sedm	sedm	k4xCc1	sedm
míst	místo	k1gNnPc2	místo
je	být	k5eAaImIp3nS	být
obsazeno	obsadit	k5eAaPmNgNnS	obsadit
studenty	student	k1gMnPc7	student
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
v	v	k7c6	v
akademickém	akademický	k2eAgInSc6d1	akademický
senátu	senát	k1gInSc6	senát
je	být	k5eAaImIp3nS	být
čestná	čestný	k2eAgFnSc1d1	čestná
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
děkana	děkan	k1gMnSc2	děkan
trvá	trvat	k5eAaImIp3nS	trvat
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
členství	členství	k1gNnSc2	členství
v	v	k7c6	v
akademického	akademický	k2eAgInSc2d1	akademický
senátu	senát	k1gInSc2	senát
jsou	být	k5eAaImIp3nP	být
vyloučeni	vyloučen	k2eAgMnPc1d1	vyloučen
rektor	rektor	k1gMnSc1	rektor
<g/>
,	,	kIx,	,
prorektor	prorektor	k1gMnSc1	prorektor
<g/>
,	,	kIx,	,
děkan	děkan	k1gMnSc1	děkan
a	a	k8xC	a
proděkan	proděkan	k1gMnSc1	proděkan
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
akademického	akademický	k2eAgInSc2d1	akademický
senátu	senát	k1gInSc2	senát
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgInS	volit
senátem	senát	k1gInSc7	senát
z	z	k7c2	z
akademických	akademický	k2eAgMnPc2d1	akademický
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
,	,	kIx,	,
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
jej	on	k3xPp3gMnSc4	on
dva	dva	k4xCgMnPc1	dva
místopředsedové	místopředseda	k1gMnPc1	místopředseda
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
jeden	jeden	k4xCgInSc1	jeden
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgInS	volit
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
akademických	akademický	k2eAgMnPc2d1	akademický
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
pak	pak	k8xC	pak
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Akademický	akademický	k2eAgInSc1d1	akademický
senát	senát	k1gInSc1	senát
fakulty	fakulta	k1gFnSc2	fakulta
volí	volit	k5eAaImIp3nS	volit
a	a	k8xC	a
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
děkana	děkan	k1gMnSc2	děkan
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
akademickým	akademický	k2eAgInSc7d1	akademický
senátem	senát	k1gInSc7	senát
celé	celý	k2eAgFnSc2d1	celá
univerzity	univerzita	k1gFnSc2	univerzita
rovněž	rovněž	k9	rovněž
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
fakultní	fakultní	k2eAgInSc4d1	fakultní
statut	statut	k1gInSc4	statut
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
akademického	akademický	k2eAgInSc2d1	akademický
senátu	senát	k1gInSc2	senát
může	moct	k5eAaImIp3nS	moct
vážných	vážný	k2eAgInPc2d1	vážný
případech	případ	k1gInPc6	případ
svolávat	svolávat	k5eAaImF	svolávat
k	k	k7c3	k
projednání	projednání	k1gNnSc3	projednání
akademickou	akademický	k2eAgFnSc4d1	akademická
obec	obec	k1gFnSc4	obec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
působí	působit	k5eAaImIp3nS	působit
následující	následující	k2eAgFnPc4d1	následující
katedry	katedra	k1gFnPc4	katedra
a	a	k8xC	a
ústavy	ústav	k1gInPc4	ústav
a	a	k8xC	a
jim	on	k3xPp3gMnPc3	on
podřízená	podřízený	k2eAgNnPc1d1	podřízené
výzkumná	výzkumný	k2eAgNnPc1d1	výzkumné
pracoviště	pracoviště	k1gNnPc1	pracoviště
<g/>
:	:	kIx,	:
Katedra	katedra	k1gFnSc1	katedra
anglistiky	anglistika	k1gFnSc2	anglistika
a	a	k8xC	a
amerikanistiky	amerikanistika	k1gFnSc2	amerikanistika
Jazyková	jazykový	k2eAgFnSc1d1	jazyková
škola	škola	k1gFnSc1	škola
při	při	k7c6	při
FF	ff	kA	ff
MU	MU	kA	MU
Centrum	centrum	k1gNnSc4	centrum
asijských	asijský	k2eAgNnPc2d1	asijské
studií	studio	k1gNnPc2	studio
Seminář	seminář	k1gInSc1	seminář
čínských	čínský	k2eAgFnPc2d1	čínská
studií	studie	k1gFnPc2	studie
Seminář	seminář	k1gInSc4	seminář
japonských	japonský	k2eAgFnPc2d1	japonská
studií	studie	k1gFnPc2	studie
Ústav	ústav	k1gInSc1	ústav
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
Kabinet	kabinet	k1gInSc1	kabinet
češtiny	čeština	k1gFnSc2	čeština
pro	pro	k7c4	pro
cizince	cizinec	k1gMnSc4	cizinec
Ústav	ústav	k1gInSc4	ústav
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
knihovnictví	knihovnictví	k1gNnSc4	knihovnictví
Kabinet	kabinet	k1gInSc1	kabinet
informačních	informační	k2eAgNnPc2d1	informační
studií	studio	k1gNnPc2	studio
a	a	k8xC	a
knihovnictví	knihovnictví	k1gNnPc2	knihovnictví
Ústav	ústava	k1gFnPc2	ústava
<g />
.	.	kIx.	.
</s>
<s>
germanistiky	germanistika	k1gFnPc1	germanistika
<g/>
,	,	kIx,	,
nordistiky	nordistika	k1gFnPc1	nordistika
a	a	k8xC	a
nederlandistiky	nederlandistika	k1gFnPc1	nederlandistika
Ústav	ústav	k1gInSc1	ústav
jazykovědy	jazykověda	k1gFnSc2	jazykověda
a	a	k8xC	a
baltistiky	baltistika	k1gFnSc2	baltistika
Ústav	ústava	k1gFnPc2	ústava
klasických	klasický	k2eAgFnPc2d1	klasická
studií	studie	k1gFnPc2	studie
Oddělení	oddělení	k1gNnSc2	oddělení
elektronických	elektronický	k2eAgFnPc2d1	elektronická
databází	databáze	k1gFnPc2	databáze
Ústav	ústava	k1gFnPc2	ústava
románských	románský	k2eAgInPc2d1	románský
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
literatur	literatura	k1gFnPc2	literatura
Ústav	ústava	k1gFnPc2	ústava
slavistiky	slavistika	k1gFnSc2	slavistika
Seminář	seminář	k1gInSc1	seminář
dějin	dějiny	k1gFnPc2	dějiny
umění	umění	k1gNnSc2	umění
Seminář	seminář	k1gInSc1	seminář
estetiky	estetika	k1gFnSc2	estetika
Katedra	katedra	k1gFnSc1	katedra
divadelních	divadelní	k2eAgNnPc2d1	divadelní
studií	studio	k1gNnPc2	studio
Ústav	ústava	k1gFnPc2	ústava
filmu	film	k1gInSc2	film
a	a	k8xC	a
audiovizuální	audiovizuální	k2eAgFnSc2d1	audiovizuální
kultury	kultura	k1gFnSc2	kultura
Ústav	ústava	k1gFnPc2	ústava
hudební	hudební	k2eAgFnSc2d1	hudební
vědy	věda	k1gFnSc2	věda
Akademie	akademie	k1gFnSc2	akademie
staré	starý	k2eAgFnSc2d1	stará
hudby	hudba	k1gFnSc2	hudba
Historický	historický	k2eAgInSc1d1	historický
ústav	ústav	k1gInSc1	ústav
Výzkumné	výzkumný	k2eAgFnSc2d1	výzkumná
středisko	středisko	k1gNnSc4	středisko
pro	pro	k7c4	pro
dějiny	dějiny	k1gFnPc4	dějiny
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g />
.	.	kIx.	.
</s>
<s>
Ústav	ústav	k1gInSc1	ústav
archeologie	archeologie	k1gFnSc2	archeologie
a	a	k8xC	a
muzeologie	muzeologie	k1gFnSc2	muzeologie
Archeologické	archeologický	k2eAgNnSc4d1	Archeologické
pracoviště	pracoviště	k1gNnSc4	pracoviště
Pohansko	Pohansko	k1gNnSc4	Pohansko
Archeologické	archeologický	k2eAgFnSc2d1	archeologická
pracoviště	pracoviště	k1gNnPc1	pracoviště
Těšetice	Těšetika	k1gFnSc6	Těšetika
Středisko	středisko	k1gNnSc4	středisko
pro	pro	k7c4	pro
archeologický	archeologický	k2eAgInSc4d1	archeologický
výzkum	výzkum	k1gInSc4	výzkum
sociálních	sociální	k2eAgFnPc2d1	sociální
struktur	struktura	k1gFnPc2	struktura
pravěku	pravěk	k1gInSc2	pravěk
až	až	k8xS	až
středověku	středověk	k1gInSc2	středověk
Katedra	katedra	k1gFnSc1	katedra
UNESCO	UNESCO	kA	UNESCO
pro	pro	k7c4	pro
muzeologii	muzeologie	k1gFnSc4	muzeologie
a	a	k8xC	a
světové	světový	k2eAgNnSc4d1	světové
dědictví	dědictví	k1gNnSc4	dědictví
Ústav	ústava	k1gFnPc2	ústava
pomocných	pomocný	k2eAgFnPc2d1	pomocná
věd	věda	k1gFnPc2	věda
historických	historický	k2eAgNnPc2d1	historické
a	a	k8xC	a
archivnictví	archivnictví	k1gNnSc4	archivnictví
Ústav	ústav	k1gInSc1	ústav
evropské	evropský	k2eAgFnSc2d1	Evropská
etnologie	etnologie	k1gFnSc2	etnologie
Katedra	katedra	k1gFnSc1	katedra
filozofie	filozofie	k1gFnSc1	filozofie
Ústav	ústav	k1gInSc1	ústav
religionistiky	religionistika	k1gFnSc2	religionistika
Psychologický	psychologický	k2eAgInSc4d1	psychologický
ústav	ústav	k1gInSc4	ústav
Akademické	akademický	k2eAgNnSc1d1	akademické
centrum	centrum	k1gNnSc1	centrum
poradenství	poradenství	k1gNnSc2	poradenství
a	a	k8xC	a
supervize	supervize	k1gFnSc2	supervize
Ústav	ústava	k1gFnPc2	ústava
pedagogických	pedagogický	k2eAgFnPc2d1	pedagogická
věd	věda	k1gFnPc2	věda
Akademické	akademický	k2eAgNnSc1d1	akademické
centrum	centrum	k1gNnSc1	centrum
osobnostního	osobnostní	k2eAgInSc2d1	osobnostní
rozvoje	rozvoj	k1gInSc2	rozvoj
Akademickou	akademický	k2eAgFnSc4d1	akademická
obec	obec	k1gFnSc4	obec
fakulty	fakulta	k1gFnSc2	fakulta
tvoří	tvořit	k5eAaImIp3nP	tvořit
její	její	k3xOp3gMnPc1	její
akademičtí	akademický	k2eAgMnPc1d1	akademický
pracovníci	pracovník	k1gMnPc1	pracovník
a	a	k8xC	a
studenti	student	k1gMnPc1	student
<g/>
.	.	kIx.	.
</s>
<s>
Členy	člen	k1gMnPc7	člen
akademické	akademický	k2eAgFnSc2d1	akademická
obce	obec	k1gFnSc2	obec
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
i	i	k8xC	i
hostující	hostující	k2eAgMnPc1d1	hostující
profesoři	profesor	k1gMnPc1	profesor
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Akademickou	akademický	k2eAgFnSc4d1	akademická
obec	obec	k1gFnSc4	obec
může	moct	k5eAaImIp3nS	moct
svolat	svolat	k5eAaPmF	svolat
k	k	k7c3	k
projednání	projednání	k1gNnSc3	projednání
závažných	závažný	k2eAgFnPc2d1	závažná
záležitostí	záležitost	k1gFnPc2	záležitost
děkan	děkan	k1gMnSc1	děkan
nebo	nebo	k8xC	nebo
předseda	předseda	k1gMnSc1	předseda
akademického	akademický	k2eAgInSc2d1	akademický
senátu	senát	k1gInSc2	senát
filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
akademické	akademický	k2eAgFnSc2d1	akademická
obce	obec	k1gFnSc2	obec
požívají	požívat	k5eAaImIp3nP	požívat
akademických	akademický	k2eAgFnPc2d1	akademická
svobod	svoboda	k1gFnPc2	svoboda
a	a	k8xC	a
práv	právo	k1gNnPc2	právo
podle	podle	k7c2	podle
příslušného	příslušný	k2eAgInSc2d1	příslušný
zákona	zákon	k1gInSc2	zákon
a	a	k8xC	a
statutu	statut	k1gInSc2	statut
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Akademická	akademický	k2eAgFnSc1d1	akademická
obec	obec	k1gFnSc1	obec
fakulty	fakulta	k1gFnSc2	fakulta
je	být	k5eAaImIp3nS	být
subjektem	subjekt	k1gInSc7	subjekt
a	a	k8xC	a
garantem	garant	k1gMnSc7	garant
akademických	akademický	k2eAgNnPc2d1	akademické
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
svobod	svoboda	k1gFnPc2	svoboda
svých	svůj	k3xOyFgInPc2	svůj
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
jsou	být	k5eAaImIp3nP	být
zároveň	zároveň	k6eAd1	zároveň
povinni	povinen	k2eAgMnPc1d1	povinen
tyto	tento	k3xDgFnPc4	tento
svobody	svoboda	k1gFnPc4	svoboda
hájit	hájit	k5eAaImF	hájit
<g/>
.	.	kIx.	.
</s>
<s>
Fakultní	fakultní	k2eAgFnPc1d1	fakultní
insignie	insignie	k1gFnPc1	insignie
tvoří	tvořit	k5eAaImIp3nP	tvořit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
děkanský	děkanský	k2eAgInSc1d1	děkanský
řetěz	řetěz	k1gInSc1	řetěz
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
žezlo	žezlo	k1gNnSc4	žezlo
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
medaile	medaile	k1gFnSc2	medaile
řetězu	řetěz	k1gInSc2	řetěz
byl	být	k5eAaImAgMnS	být
pražský	pražský	k2eAgMnSc1d1	pražský
akademický	akademický	k2eAgMnSc1d1	akademický
sochař	sochař	k1gMnSc1	sochař
Otakar	Otakar	k1gMnSc1	Otakar
Španiel	Španiel	k1gMnSc1	Španiel
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
malířem	malíř	k1gMnSc7	malíř
a	a	k8xC	a
grafikem	grafik	k1gMnSc7	grafik
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Bendou	Benda	k1gMnSc7	Benda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
aversu	avers	k1gInSc6	avers
je	být	k5eAaImIp3nS	být
vyobrazen	vyobrazen	k2eAgMnSc1d1	vyobrazen
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
jako	jako	k8xC	jako
významný	významný	k2eAgMnSc1d1	významný
reprezentant	reprezentant	k1gMnSc1	reprezentant
historické	historický	k2eAgFnSc2d1	historická
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
stěžejních	stěžejní	k2eAgInPc2d1	stěžejní
oborů	obor	k1gInPc2	obor
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
reversu	revers	k1gInSc6	revers
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Palackého	Palackého	k2eAgNnSc4d1	Palackého
heslo	heslo	k1gNnSc4	heslo
"	"	kIx"	"
<g/>
Svoji	svůj	k3xOyFgFnSc4	svůj
k	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
a	a	k8xC	a
vždy	vždy	k6eAd1	vždy
dle	dle	k7c2	dle
pravdy	pravda	k1gFnSc2	pravda
<g/>
"	"	kIx"	"
lemované	lemovaný	k2eAgNnSc1d1	lemované
nahoře	nahoře	k6eAd1	nahoře
a	a	k8xC	a
lipovou	lipový	k2eAgFnSc4d1	Lipová
a	a	k8xC	a
dole	dole	k6eAd1	dole
vavřínovou	vavřínový	k2eAgFnSc7d1	vavřínová
ratolestí	ratolest	k1gFnSc7	ratolest
<g/>
.	.	kIx.	.
</s>
<s>
Fakultní	fakultní	k2eAgNnSc1d1	fakultní
žezlo	žezlo	k1gNnSc1	žezlo
bylo	být	k5eAaImAgNnS	být
zhotoveno	zhotovit	k5eAaPmNgNnS	zhotovit
podle	podle	k7c2	podle
návrhů	návrh	k1gInPc2	návrh
Václava	Václav	k1gMnSc2	Václav
Rady	Rada	k1gMnSc2	Rada
a	a	k8xC	a
Ladislava	Ladislav	k1gMnSc2	Ladislav
Bartoníčka	Bartoníček	k1gMnSc2	Bartoníček
a	a	k8xC	a
zdobí	zdobit	k5eAaImIp3nP	zdobit
jej	on	k3xPp3gMnSc4	on
na	na	k7c6	na
konci	konec	k1gInSc6	konec
ametysty	ametyst	k1gInPc4	ametyst
zdobeného	zdobený	k2eAgInSc2d1	zdobený
dříku	dřík	k1gInSc2	dřík
posazený	posazený	k2eAgInSc4d1	posazený
páskový	páskový	k2eAgInSc4d1	páskový
globus	globus	k1gInSc4	globus
a	a	k8xC	a
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
umístěná	umístěný	k2eAgFnSc1d1	umístěná
sova	sova	k1gFnSc1	sova
sedící	sedící	k2eAgFnSc1d1	sedící
na	na	k7c6	na
otevřené	otevřený	k2eAgFnSc6d1	otevřená
knize	kniha	k1gFnSc6	kniha
jako	jako	k8xC	jako
symbol	symbol	k1gInSc4	symbol
moudrosti	moudrost	k1gFnSc2	moudrost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
fakultním	fakultní	k2eAgFnPc3d1	fakultní
insigniím	insignie	k1gFnPc3	insignie
náleží	náležet	k5eAaImIp3nS	náležet
též	též	k9	též
talár	talár	k1gInSc1	talár
používaný	používaný	k2eAgInSc1d1	používaný
při	při	k7c6	při
akademických	akademický	k2eAgInPc6d1	akademický
obřadech	obřad	k1gInPc6	obřad
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
imatrikulace	imatrikulace	k1gFnPc1	imatrikulace
<g/>
,	,	kIx,	,
promoce	promoce	k1gFnPc1	promoce
a	a	k8xC	a
inaugurace	inaugurace	k1gFnSc1	inaugurace
děkana	děkan	k1gMnSc2	děkan
<g/>
.	.	kIx.	.
</s>
<s>
Taláry	talár	k1gInPc4	talár
smí	smět	k5eAaImIp3nS	smět
užívat	užívat	k5eAaImF	užívat
děkan	děkan	k1gMnSc1	děkan
<g/>
,	,	kIx,	,
proděkani	proděkan	k1gMnPc1	proděkan
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
akademického	akademický	k2eAgInSc2d1	akademický
senátu	senát	k1gInSc2	senát
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
,	,	kIx,	,
členové	člen	k1gMnPc1	člen
vědecké	vědecký	k2eAgFnSc2d1	vědecká
rady	rada	k1gFnSc2	rada
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
,	,	kIx,	,
promotor	promotor	k1gMnSc1	promotor
a	a	k8xC	a
pedel	pedel	k1gMnSc1	pedel
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
promoci	promoce	k1gFnSc6	promoce
též	též	k9	též
doktorandi	doktorand	k1gMnPc1	doktorand
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jiné	jiný	k2eAgFnPc4d1	jiná
osoby	osoba	k1gFnPc4	osoba
pověřené	pověřený	k2eAgNnSc1d1	pověřené
děkanem	děkan	k1gMnSc7	děkan
<g/>
.	.	kIx.	.
</s>
<s>
Logo	logo	k1gNnSc1	logo
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
motiv	motiv	k1gInSc4	motiv
fakultního	fakultní	k2eAgNnSc2d1	fakultní
žezla	žezlo	k1gNnSc2	žezlo
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
jej	on	k3xPp3gMnSc4	on
rovněž	rovněž	k9	rovněž
sova	sova	k1gFnSc1	sova
sedící	sedící	k2eAgFnSc1d1	sedící
na	na	k7c6	na
rozevřené	rozevřený	k2eAgFnSc6d1	rozevřená
knize	kniha	k1gFnSc6	kniha
a	a	k8xC	a
hledící	hledící	k2eAgMnSc1d1	hledící
dopředu	dopředu	k6eAd1	dopředu
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
opise	opis	k1gInSc6	opis
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
latinský	latinský	k2eAgInSc1d1	latinský
název	název	k1gInSc1	název
univerzity	univerzita	k1gFnSc2	univerzita
společně	společně	k6eAd1	společně
s	s	k7c7	s
názvem	název	k1gInSc7	název
filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
(	(	kIx(	(
<g/>
Universitas	Universitas	k1gMnSc1	Universitas
Masarykiana	Masarykian	k1gMnSc2	Masarykian
Brunensis	Brunensis	k1gFnSc2	Brunensis
.	.	kIx.	.
</s>
<s desamb="1">
Facultas	Facultas	k1gMnSc1	Facultas
philosophica	philosophica	k1gMnSc1	philosophica
<g/>
)	)	kIx)	)
</s>
<s>
Barva	barva	k1gFnSc1	barva
fakulty	fakulta	k1gFnSc2	fakulta
je	být	k5eAaImIp3nS	být
modrá	modrý	k2eAgFnSc1d1	modrá
(	(	kIx(	(
<g/>
Pantone	Panton	k1gInSc5	Panton
299	[number]	k4	299
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgMnPc4d1	významný
absolventy	absolvent	k1gMnPc4	absolvent
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
historikové	historik	k1gMnPc1	historik
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Mezník	mezník	k1gInSc1	mezník
<g/>
,	,	kIx,	,
Libor	Libor	k1gMnSc1	Libor
Jan	Jan	k1gMnSc1	Jan
či	či	k8xC	či
Martin	Martin	k1gMnSc1	Martin
Wihoda	Wihoda	k1gMnSc1	Wihoda
<g/>
,	,	kIx,	,
germanista	germanista	k1gMnSc1	germanista
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Masařík	Masařík	k1gMnSc1	Masařík
<g/>
,	,	kIx,	,
filoložka	filoložka	k1gFnSc1	filoložka
Jana	Jana	k1gFnSc1	Jana
Nechutová	Nechutový	k2eAgFnSc1d1	Nechutová
nebo	nebo	k8xC	nebo
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Kateřina	Kateřina	k1gFnSc1	Kateřina
Tučková	Tučková	k1gFnSc1	Tučková
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
absolvovali	absolvovat	k5eAaPmAgMnP	absolvovat
též	též	k9	též
politici	politik	k1gMnPc1	politik
Jiří	Jiří	k1gMnSc1	Jiří
Mihola	Mihola	k1gFnSc1	Mihola
a	a	k8xC	a
emeritní	emeritní	k2eAgMnSc1d1	emeritní
rektor	rektor	k1gMnSc1	rektor
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
ministr	ministr	k1gMnSc1	ministr
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
Petr	Petr	k1gMnSc1	Petr
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
na	na	k7c6	na
Masarykově	Masarykův	k2eAgFnSc6d1	Masarykova
univerzitě	univerzita	k1gFnSc6	univerzita
první	první	k4xOgInSc1	první
ročník	ročník	k1gInSc1	ročník
soutěže	soutěž	k1gFnSc2	soutěž
o	o	k7c4	o
Cenu	cena	k1gFnSc4	cena
rektora	rektor	k1gMnSc2	rektor
pro	pro	k7c4	pro
vynikající	vynikající	k2eAgMnPc4d1	vynikající
pedagogy	pedagog	k1gMnPc4	pedagog
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
filozofickou	filozofický	k2eAgFnSc4d1	filozofická
fakultu	fakulta	k1gFnSc4	fakulta
získala	získat	k5eAaPmAgFnS	získat
po	po	k7c6	po
hlasování	hlasování	k1gNnSc6	hlasování
studentů	student	k1gMnPc2	student
toto	tento	k3xDgNnSc4	tento
ocenění	ocenění	k1gNnPc2	ocenění
docentka	docentka	k1gFnSc1	docentka
Helena	Helena	k1gFnSc1	Helena
Krmíčková	Krmíčková	k1gFnSc1	Krmíčková
z	z	k7c2	z
Ústavu	ústav	k1gInSc2	ústav
pomocných	pomocný	k2eAgFnPc2d1	pomocná
věd	věda	k1gFnPc2	věda
historických	historický	k2eAgNnPc2d1	historické
a	a	k8xC	a
archivnictví	archivnictví	k1gNnSc4	archivnictví
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
působí	působit	k5eAaImIp3nS	působit
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
studentských	studentský	k2eAgMnPc2d1	studentský
spolků	spolek	k1gInPc2	spolek
a	a	k8xC	a
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
organizací	organizace	k1gFnPc2	organizace
s	s	k7c7	s
celouniverzitní	celouniverzitní	k2eAgFnSc7d1	celouniverzitní
působností	působnost	k1gFnSc7	působnost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
studentská	studentský	k2eAgFnSc1d1	studentská
unie	unie	k1gFnSc1	unie
či	či	k8xC	či
studentští	studentský	k2eAgMnPc1d1	studentský
poradci	poradce	k1gMnPc1	poradce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
s	s	k7c7	s
fakultou	fakulta	k1gFnSc7	fakulta
svázána	svázat	k5eAaPmNgFnS	svázat
téměř	téměř	k6eAd1	téměř
dvacítka	dvacítka	k1gFnSc1	dvacítka
uskupení	uskupení	k1gNnSc2	uskupení
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
celé	celý	k2eAgFnSc2d1	celá
fakulty	fakulta	k1gFnSc2	fakulta
nebo	nebo	k8xC	nebo
souvisejí	souviset	k5eAaImIp3nP	souviset
s	s	k7c7	s
činností	činnost	k1gFnSc7	činnost
a	a	k8xC	a
studijní	studijní	k2eAgFnSc7d1	studijní
náplní	náplň	k1gFnSc7	náplň
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
kateder	katedra	k1gFnPc2	katedra
a	a	k8xC	a
ústavů	ústav	k1gInPc2	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
studentská	studentský	k2eAgFnSc1d1	studentská
sekce	sekce	k1gFnSc1	sekce
České	český	k2eAgFnSc2d1	Česká
archivní	archivní	k2eAgFnSc2d1	archivní
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
BSS	BSS	kA	BSS
ČAS	čas	k1gInSc1	čas
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
studentským	studentský	k2eAgInSc7d1	studentský
spolkem	spolek	k1gInSc7	spolek
spadajícím	spadající	k2eAgInSc7d1	spadající
pod	pod	k7c4	pod
Českou	český	k2eAgFnSc4d1	Česká
archivní	archivní	k2eAgFnSc4d1	archivní
společnost	společnost	k1gFnSc4	společnost
–	–	k?	–
profesní	profesní	k2eAgFnSc4d1	profesní
organizaci	organizace	k1gFnSc4	organizace
archivářů	archivář	k1gMnPc2	archivář
<g/>
,	,	kIx,	,
vysokoškolských	vysokoškolský	k2eAgMnPc2d1	vysokoškolský
pedagogů	pedagog	k1gMnPc2	pedagog
a	a	k8xC	a
studentů	student	k1gMnPc2	student
archivnictví	archivnictví	k1gNnSc2	archivnictví
a	a	k8xC	a
pomocných	pomocný	k2eAgFnPc2d1	pomocná
věd	věda	k1gFnPc2	věda
historických	historický	k2eAgFnPc2d1	historická
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
podpora	podpora	k1gFnSc1	podpora
a	a	k8xC	a
propagace	propagace	k1gFnSc1	propagace
těchto	tento	k3xDgInPc2	tento
oborů	obor	k1gInPc2	obor
<g/>
,	,	kIx,	,
aktivizace	aktivizace	k1gFnSc1	aktivizace
studentů	student	k1gMnPc2	student
a	a	k8xC	a
šíření	šíření	k1gNnSc2	šíření
nejnovějších	nový	k2eAgInPc2d3	nejnovější
poznatků	poznatek	k1gInPc2	poznatek
z	z	k7c2	z
oboru	obor	k1gInSc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
Organizuje	organizovat	k5eAaBmIp3nS	organizovat
odborné	odborný	k2eAgFnPc4d1	odborná
exkurze	exkurze	k1gFnPc4	exkurze
<g/>
,	,	kIx,	,
přednášky	přednáška	k1gFnPc4	přednáška
a	a	k8xC	a
konference	konference	k1gFnPc4	konference
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	s	k7c7	s
studenty	student	k1gMnPc7	student
seznamovat	seznamovat	k5eAaImF	seznamovat
s	s	k7c7	s
příbuznými	příbuzný	k2eAgInPc7d1	příbuzný
obory	obor	k1gInPc7	obor
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
archeologie	archeologie	k1gFnSc1	archeologie
<g/>
,	,	kIx,	,
knihovnictví	knihovnictví	k1gNnSc1	knihovnictví
<g/>
,	,	kIx,	,
etnologie	etnologie	k1gFnSc1	etnologie
<g/>
.	.	kIx.	.
apod.	apod.	kA	apod.
Sídlo	sídlo	k1gNnSc4	sídlo
studentské	studentská	k1gFnSc2	studentská
sekce	sekce	k1gFnSc2	sekce
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
Ústavu	ústav	k1gInSc6	ústav
pomocných	pomocný	k2eAgFnPc2d1	pomocná
věd	věda	k1gFnPc2	věda
historických	historický	k2eAgNnPc2d1	historické
a	a	k8xC	a
archivnictví	archivnictví	k1gNnSc4	archivnictví
<g/>
.	.	kIx.	.
</s>
<s>
Spolek	spolek	k1gInSc1	spolek
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
rovněž	rovněž	k9	rovněž
s	s	k7c7	s
Historickým	historický	k2eAgInSc7d1	historický
ústavem	ústav	k1gInSc7	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Poočka	Poočka	k1gFnSc1	Poočka
České	český	k2eAgFnSc2d1	Česká
asociace	asociace	k1gFnSc2	asociace
studentů	student	k1gMnPc2	student
psychologie	psychologie	k1gFnSc1	psychologie
působí	působit	k5eAaImIp3nS	působit
na	na	k7c6	na
Psychologickém	psychologický	k2eAgInSc6d1	psychologický
ústavu	ústav	k1gInSc6	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
její	její	k3xOp3gFnPc4	její
aktivity	aktivita	k1gFnPc4	aktivita
patří	patřit	k5eAaImIp3nS	patřit
podpora	podpora	k1gFnSc1	podpora
mezi	mezi	k7c7	mezi
studenty	student	k1gMnPc7	student
psychologie	psychologie	k1gFnSc2	psychologie
a	a	k8xC	a
podpora	podpora	k1gFnSc1	podpora
studentských	studentský	k2eAgFnPc2d1	studentská
aktivit	aktivita	k1gFnPc2	aktivita
zaměřených	zaměřený	k2eAgFnPc2d1	zaměřená
k	k	k7c3	k
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Spolek	spolek	k1gInSc1	spolek
zároveň	zároveň	k6eAd1	zároveň
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
dalším	další	k2eAgInSc7d1	další
profesními	profesní	k2eAgFnPc7d1	profesní
organizacemi	organizace	k1gFnPc7	organizace
a	a	k8xC	a
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
propagaci	propagace	k1gFnSc4	propagace
psychologie	psychologie	k1gFnSc2	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Asociace	asociace	k1gFnSc1	asociace
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
federace	federace	k1gFnSc2	federace
psychologických	psychologický	k2eAgFnPc2d1	psychologická
studentských	studentský	k2eAgFnPc2d1	studentská
asociací	asociace	k1gFnPc2	asociace
a	a	k8xC	a
České	český	k2eAgFnSc2d1	Česká
rady	rada	k1gFnSc2	rada
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Divadelní	divadelní	k2eAgInSc1d1	divadelní
spolek	spolek	k1gInSc1	spolek
The	The	k1gFnSc2	The
Gypsywood	Gypsywooda	k1gFnPc2	Gypsywooda
Players	Playersa	k1gFnPc2	Playersa
působí	působit	k5eAaImIp3nP	působit
na	na	k7c6	na
filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
Sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
studenty	student	k1gMnPc4	student
anglistiky	anglistika	k1gFnSc2	anglistika
<g/>
,	,	kIx,	,
kterým	který	k3yRgFnPc3	který
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
při	při	k7c6	při
vylepšování	vylepšování	k1gNnSc6	vylepšování
jejich	jejich	k3xOp3gFnPc2	jejich
řečových	řečový	k2eAgFnPc2d1	řečová
schopností	schopnost	k1gFnPc2	schopnost
a	a	k8xC	a
prezentace	prezentace	k1gFnSc2	prezentace
před	před	k7c7	před
publikem	publikum	k1gNnSc7	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
členové	člen	k1gMnPc1	člen
mají	mít	k5eAaImIp3nP	mít
též	též	k9	též
možnost	možnost	k1gFnSc4	možnost
zapojit	zapojit	k5eAaPmF	zapojit
se	se	k3xPyFc4	se
do	do	k7c2	do
širokých	široký	k2eAgFnPc2d1	široká
aktivit	aktivita	k1gFnPc2	aktivita
kolem	kolem	k7c2	kolem
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
vyzkoušet	vyzkoušet	k5eAaPmF	vyzkoušet
si	se	k3xPyFc3	se
všechny	všechen	k3xTgFnPc4	všechen
různé	různý	k2eAgFnPc4d1	různá
pozice	pozice	k1gFnPc4	pozice
<g/>
,	,	kIx,	,
při	při	k7c6	při
organizaci	organizace	k1gFnSc6	organizace
anglickojazyčných	anglickojazyčný	k2eAgNnPc2d1	anglickojazyčné
představení	představení	k1gNnPc2	představení
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
historie	historie	k1gFnSc2	historie
odehrál	odehrát	k5eAaPmAgInS	odehrát
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
představení	představení	k1gNnSc2	představení
a	a	k8xC	a
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
kolik	kolik	k4yRc1	kolik
stovek	stovka	k1gFnPc2	stovka
studentů	student	k1gMnPc2	student
a	a	k8xC	a
vyučujících	vyučující	k2eAgMnPc2d1	vyučující
<g/>
.	.	kIx.	.
</s>
<s>
Divadelní	divadelní	k2eAgInSc1d1	divadelní
spolek	spolek	k1gInSc1	spolek
Teatro	Teatro	k1gNnSc1	Teatro
Bufo	bufa	k1gFnSc5	bufa
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
studenty	student	k1gMnPc7	student
španělštiny	španělština	k1gFnSc2	španělština
a	a	k8xC	a
zájemce	zájemce	k1gMnPc4	zájemce
o	o	k7c4	o
španělský	španělský	k2eAgInSc4d1	španělský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Divadelní	divadelní	k2eAgInSc1d1	divadelní
spolek	spolek	k1gInSc1	spolek
Kočovná	kočovný	k2eAgFnSc1d1	kočovná
filosofická	filosofický	k2eAgFnSc1d1	filosofická
divadelní	divadelní	k2eAgFnSc1d1	divadelní
společnost	společnost	k1gFnSc1	společnost
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
při	při	k7c6	při
Katedře	katedra	k1gFnSc6	katedra
filosofie	filosofie	k1gFnSc2	filosofie
a	a	k8xC	a
vlastními	vlastní	k2eAgFnPc7d1	vlastní
dramatizacemi	dramatizace	k1gFnPc7	dramatizace
popularizuje	popularizovat	k5eAaImIp3nS	popularizovat
klasická	klasický	k2eAgNnPc4d1	klasické
díla	dílo	k1gNnPc4	dílo
a	a	k8xC	a
osobnosti	osobnost	k1gFnPc4	osobnost
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Obměňující	obměňující	k2eAgMnSc1d1	obměňující
se	se	k3xPyFc4	se
soubor	soubor	k1gInSc1	soubor
nacvičí	nacvičit	k5eAaBmIp3nS	nacvičit
(	(	kIx(	(
<g/>
novou	nový	k2eAgFnSc4d1	nová
<g/>
,	,	kIx,	,
či	či	k8xC	či
již	již	k9	již
dříve	dříve	k6eAd2	dříve
uvedenou	uvedený	k2eAgFnSc4d1	uvedená
<g/>
)	)	kIx)	)
hru	hra	k1gFnSc4	hra
každý	každý	k3xTgInSc4	každý
semestr	semestr	k1gInSc4	semestr
<g/>
.	.	kIx.	.
</s>
<s>
Scénáře	scénář	k1gInPc1	scénář
ke	k	k7c3	k
hrám	hra	k1gFnPc3	hra
jsou	být	k5eAaImIp3nP	být
původní	původní	k2eAgFnSc4d1	původní
<g/>
,	,	kIx,	,
od	od	k7c2	od
autorů	autor	k1gMnPc2	autor
spjatých	spjatý	k2eAgMnPc2d1	spjatý
s	s	k7c7	s
Katedrou	katedra	k1gFnSc7	katedra
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Soubor	soubor	k1gInSc1	soubor
mimo	mimo	k7c4	mimo
Filozofickou	filozofický	k2eAgFnSc4d1	filozofická
fakultu	fakulta	k1gFnSc4	fakulta
MU	MU	kA	MU
pravidelně	pravidelně	k6eAd1	pravidelně
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
i	i	k9	i
na	na	k7c6	na
Evropském	evropský	k2eAgInSc6d1	evropský
festivalu	festival	k1gInSc6	festival
filosofie	filosofie	k1gFnSc2	filosofie
ve	v	k7c6	v
Velkém	velký	k2eAgNnSc6d1	velké
Meziříčí	Meziříčí	k1gNnSc6	Meziříčí
a	a	k8xC	a
v	v	k7c6	v
Lanškrouně	Lanškroun	k1gInSc6	Lanškroun
<g/>
.	.	kIx.	.
</s>
