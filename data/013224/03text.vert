<p>
<s>
Fitz	Fitz	k1gInSc1	Fitz
and	and	k?	and
the	the	k?	the
Tantrums	Tantrumsa	k1gFnPc2	Tantrumsa
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Skupinu	skupina	k1gFnSc4	skupina
založil	založit	k5eAaPmAgMnS	založit
zpěvák	zpěvák	k1gMnSc1	zpěvák
Michael	Michael	k1gMnSc1	Michael
Fitzpatrick	Fitzpatrick	k1gMnSc1	Fitzpatrick
a	a	k8xC	a
přidali	přidat	k5eAaPmAgMnP	přidat
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Noelle	Noelle	k1gFnSc2	Noelle
Scaggs	Scaggsa	k1gFnPc2	Scaggsa
a	a	k8xC	a
bubeník	bubeník	k1gMnSc1	bubeník
John	John	k1gMnSc1	John
Wicks	Wicksa	k1gFnPc2	Wicksa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mu	on	k3xPp3gMnSc3	on
doporučil	doporučit	k5eAaPmAgMnS	doporučit
jeho	jeho	k3xOp3gMnSc1	jeho
kamarád	kamarád	k1gMnSc1	kamarád
<g/>
,	,	kIx,	,
saxofonista	saxofonista	k1gMnSc1	saxofonista
James	James	k1gMnSc1	James
King	King	k1gMnSc1	King
<g/>
.	.	kIx.	.
</s>
<s>
Wicks	Wicks	k6eAd1	Wicks
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
přivedl	přivést	k5eAaPmAgMnS	přivést
ještě	ještě	k9	ještě
baskytaristu	baskytarista	k1gMnSc4	baskytarista
Joea	Joeum	k1gNnSc2	Joeum
Karnese	Karnese	k1gFnSc2	Karnese
a	a	k8xC	a
klávesistu	klávesista	k1gMnSc4	klávesista
Jeremyho	Jeremy	k1gMnSc4	Jeremy
Ruzumna	Ruzumn	k1gMnSc4	Ruzumn
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
první	první	k4xOgInPc4	první
EP	EP	kA	EP
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2009	[number]	k4	2009
a	a	k8xC	a
neslo	nést	k5eAaImAgNnS	nést
název	název	k1gInSc4	název
Songs	Songs	k1gInSc4	Songs
for	forum	k1gNnPc2	forum
a	a	k8xC	a
Breakup	Breakup	k1gMnSc1	Breakup
<g/>
,	,	kIx,	,
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
první	první	k4xOgNnSc1	první
řadové	řadový	k2eAgNnSc1d1	řadové
album	album	k1gNnSc1	album
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Pickin	Pickin	k2eAgInSc4d1	Pickin
<g/>
'	'	kIx"	'
Up	Up	k1gFnSc1	Up
the	the	k?	the
Pieces	Pieces	k1gInSc1	Pieces
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2010	[number]	k4	2010
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
další	další	k2eAgFnSc1d1	další
EP	EP	kA	EP
Santa	Santa	k1gFnSc1	Santa
Stole	stol	k1gInSc5	stol
My	my	k3xPp1nPc1	my
Lady	lady	k1gFnSc1	lady
a	a	k8xC	a
druhé	druhý	k4xOgNnSc1	druhý
řadové	řadový	k2eAgNnSc1d1	řadové
album	album	k1gNnSc1	album
More	mor	k1gInSc5	mor
Than	Than	k1gNnSc4	Than
Just	just	k6eAd1	just
a	a	k8xC	a
Dream	Dream	k1gInSc4	Dream
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
Studiová	studiový	k2eAgFnSc1d1	studiová
albaPickin	albaPickin	k1gInSc1	albaPickin
<g/>
'	'	kIx"	'
Up	Up	k1gMnSc1	Up
the	the	k?	the
Pieces	Pieces	k1gMnSc1	Pieces
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
More	mor	k1gInSc5	mor
Than	Than	k1gNnSc4	Than
Just	just	k6eAd1	just
a	a	k8xC	a
Dream	Dream	k1gInSc1	Dream
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fitz	Fitz	k1gInSc1	Fitz
and	and	k?	and
the	the	k?	the
Tantrums	Tantrums	k1gInSc1	Tantrums
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
All	All	k?	All
the	the	k?	the
Feels	Feels	k1gInSc1	Feels
(	(	kIx(	(
<g/>
2019	[number]	k4	2019
<g/>
)	)	kIx)	)
<g/>
EPSongs	EPSongsa	k1gFnPc2	EPSongsa
for	forum	k1gNnPc2	forum
a	a	k8xC	a
Breakup	Breakup	k1gMnSc1	Breakup
<g/>
,	,	kIx,	,
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Santa	Santa	k1gFnSc1	Santa
Stole	stol	k1gInSc6	stol
My	my	k3xPp1nPc1	my
Lady	lady	k1gFnSc1	lady
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
Singly	singl	k1gInPc1	singl
<g/>
"	"	kIx"	"
<g/>
Winds	Winds	k1gInSc1	Winds
of	of	k?	of
Change	change	k1gFnSc1	change
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
L.O.V.	L.O.V.	k1gFnSc6	L.O.V.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Breakin	Breakina	k1gFnPc2	Breakina
<g/>
'	'	kIx"	'
the	the	k?	the
Chains	Chains	k1gInSc1	Chains
of	of	k?	of
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
MoneyGrabber	MoneyGrabber	k1gInSc4	MoneyGrabber
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Don	dona	k1gFnPc2	dona
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Gotta	Gotta	k1gMnSc1	Gotta
Work	Work	k1gMnSc1	Work
It	It	k1gMnSc1	It
Out	Out	k1gMnSc1	Out
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Out	Out	k1gFnPc3	Out
of	of	k?	of
My	my	k3xPp1nPc1	my
League	League	k1gNnPc7	League
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Walker	Walker	k1gMnSc1	Walker
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Fools	Foolsa	k1gFnPc2	Foolsa
Gold	Gold	k1gInSc1	Gold
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
HandClap	HandClap	k1gInSc4	HandClap
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Roll	Roll	k1gMnSc1	Roll
Up	Up	k1gMnSc1	Up
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Fool	Fool	k1gInSc4	Fool
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
123456	[number]	k4	123456
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2019	[number]	k4	2019
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Fitz	Fitza	k1gFnPc2	Fitza
and	and	k?	and
the	the	k?	the
Tantrums	Tantrums	k1gInSc1	Tantrums
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Fitz	Fitz	k1gInSc1	Fitz
and	and	k?	and
the	the	k?	the
Tantrums	Tantrums	k1gInSc4	Tantrums
na	na	k7c4	na
Allmusic	Allmusice	k1gFnPc2	Allmusice
</s>
</p>
