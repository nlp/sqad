<p>
<s>
Litva	Litva	k1gFnSc1	Litva
(	(	kIx(	(
<g/>
litevsky	litevsky	k6eAd1	litevsky
Lietuva	Lietuva	k1gFnSc1	Lietuva
nebo	nebo	k8xC	nebo
Lietuvos	Lietuvos	k1gInSc1	Lietuvos
Respublika	Respublika	k1gFnSc1	Respublika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Litevská	litevský	k2eAgFnSc1d1	Litevská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejjižnější	jižní	k2eAgMnSc1d3	nejjižnější
a	a	k8xC	a
největší	veliký	k2eAgMnSc1d3	veliký
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
pobaltských	pobaltský	k2eAgFnPc2d1	pobaltská
zemí	zem	k1gFnPc2	zem
na	na	k7c6	na
jihovýchodním	jihovýchodní	k2eAgNnSc6d1	jihovýchodní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Lotyšskem	Lotyšsko	k1gNnSc7	Lotyšsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Běloruskem	Bělorusko	k1gNnSc7	Bělorusko
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
a	a	k8xC	a
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
s	s	k7c7	s
ruskou	ruský	k2eAgFnSc7d1	ruská
exklávou	exklávý	k2eAgFnSc7d1	exklávý
Kaliningradskou	kaliningradský	k2eAgFnSc7d1	Kaliningradská
oblastí	oblast	k1gFnSc7	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2012	[number]	k4	2012
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
3,2	[number]	k4	3,2
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
emigrace	emigrace	k1gFnSc2	emigrace
za	za	k7c7	za
prací	práce	k1gFnSc7	práce
do	do	k7c2	do
zemí	zem	k1gFnPc2	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
EU	EU	kA	EU
<g/>
)	)	kIx)	)
však	však	k9	však
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
2017	[number]	k4	2017
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
2,82	[number]	k4	2,82
milionu	milion	k4xCgInSc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Vilnius	Vilnius	k1gInSc1	Vilnius
<g/>
.	.	kIx.	.
</s>
<s>
Litevci	Litevec	k1gMnPc1	Litevec
jsou	být	k5eAaImIp3nP	být
baltským	baltský	k2eAgInSc7d1	baltský
národem	národ	k1gInSc7	národ
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
84	[number]	k4	84
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
litevština	litevština	k1gFnSc1	litevština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Litva	Litva	k1gFnSc1	Litva
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
svazová	svazový	k2eAgFnSc1d1	svazová
republika	republika	k1gFnSc1	republika
obnovila	obnovit	k5eAaPmAgFnS	obnovit
svou	svůj	k3xOyFgFnSc4	svůj
nezávislost	nezávislost	k1gFnSc4	nezávislost
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
rok	rok	k1gInSc4	rok
před	před	k7c7	před
rozpadem	rozpad	k1gInSc7	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
aliance	aliance	k1gFnSc2	aliance
(	(	kIx(	(
<g/>
NATO	NATO	kA	NATO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Schengenského	schengenský	k2eAgInSc2d1	schengenský
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
do	do	k7c2	do
eurozóny	eurozóna	k1gFnSc2	eurozóna
<g/>
,	,	kIx,	,
zákonnou	zákonný	k2eAgFnSc7d1	zákonná
měnou	měna	k1gFnSc7	měna
Litvy	Litva	k1gFnSc2	Litva
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
euro	euro	k1gNnSc1	euro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
Litvy	Litva	k1gFnSc2	Litva
(	(	kIx(	(
<g/>
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
Lituae	Litua	k1gFnSc2	Litua
v	v	k7c6	v
genitivu	genitiv	k1gInSc6	genitiv
<g/>
;	;	kIx,	;
1	[number]	k4	1
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
by	by	kYmCp3nS	by
tedy	tedy	k9	tedy
byl	být	k5eAaImAgInS	být
"	"	kIx"	"
<g/>
Litua	Litua	k1gFnSc1	Litua
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
v	v	k7c6	v
Quedlinburské	Quedlinburský	k2eAgFnSc6d1	Quedlinburský
kronice	kronika	k1gFnSc6	kronika
dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1009	[number]	k4	1009
v	v	k7c6	v
zápise	zápis	k1gInSc6	zápis
o	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
svatého	svatý	k2eAgMnSc2d1	svatý
Bruna	Bruno	k1gMnSc2	Bruno
<g/>
.	.	kIx.	.
</s>
<s>
Bruno	Bruno	k1gMnSc1	Bruno
pokřtil	pokřtít	k5eAaPmAgMnS	pokřtít
Netimera	Netimer	k1gMnSc4	Netimer
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
lokálních	lokální	k2eAgMnPc2d1	lokální
(	(	kIx(	(
<g/>
patrně	patrně	k6eAd1	patrně
jotvingského	jotvingského	k2eAgInPc2d1	jotvingského
<g/>
)	)	kIx)	)
knížat	kníže	k1gNnPc2	kníže
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
blízké	blízký	k2eAgInPc1d1	blízký
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgMnSc7	ten
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Zebeden	Zebeden	k2eAgMnSc1d1	Zebeden
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nechal	nechat	k5eAaPmAgMnS	nechat
Bruna	Bruno	k1gMnSc4	Bruno
i	i	k9	i
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
svitou	svita	k1gFnSc7	svita
stít	stít	k5eAaPmF	stít
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
článek	článek	k1gInSc4	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Litvy	Litva	k1gFnSc2	Litva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
verzí	verze	k1gFnPc2	verze
původu	původ	k1gInSc2	původ
jména	jméno	k1gNnSc2	jméno
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
zemích	zem	k1gFnPc6	zem
převládá	převládat	k5eAaImIp3nS	převládat
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
název	název	k1gInSc4	název
Litva	Litva	k1gFnSc1	Litva
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
litevského	litevský	k2eAgNnSc2d1	litevské
slova	slovo	k1gNnSc2	slovo
lietus	lietus	k1gInSc1	lietus
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
déšť	déšť	k1gInSc1	déšť
<g/>
.	.	kIx.	.
</s>
<s>
Litva	Litva	k1gFnSc1	Litva
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
země	zem	k1gFnSc2	zem
deště	dešť	k1gInSc2	dešť
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
však	však	k9	však
nelze	lze	k6eNd1	lze
ztotožnit	ztotožnit	k5eAaPmF	ztotožnit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prší	pršet	k5eAaImIp3nS	pršet
častěji	často	k6eAd2	často
než	než	k8xS	než
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gNnPc1	jejich
jména	jméno	k1gNnPc1	jméno
nejsou	být	k5eNaImIp3nP	být
odvozena	odvodit	k5eAaPmNgNnP	odvodit
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
déšť	déšť	k1gInSc1	déšť
<g/>
.	.	kIx.	.
</s>
<s>
Sami	sám	k3xTgMnPc1	sám
Litevci	Litevec	k1gMnPc1	Litevec
tuto	tento	k3xDgFnSc4	tento
verzi	verze	k1gFnSc4	verze
berou	brát	k5eAaImIp3nP	brát
pouze	pouze	k6eAd1	pouze
anekdoticky	anekdoticky	k6eAd1	anekdoticky
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
textu	text	k1gInSc6	text
písně	píseň	k1gFnSc2	píseň
úryvek	úryvek	k1gInSc1	úryvek
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
Lietuva	Lietuva	k1gFnSc1	Lietuva
<g/>
,	,	kIx,	,
kur	kur	k1gMnSc1	kur
lietus	lietus	k1gMnSc1	lietus
lyja	lyja	k1gMnSc1	lyja
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
-	-	kIx~	-
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
Litva	Litva	k1gFnSc1	Litva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prší	pršet	k5eAaImIp3nS	pršet
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Litevský	litevský	k2eAgMnSc1d1	litevský
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
Kazimeras	Kazimeras	k1gMnSc1	Kazimeras
Kuzavinis	Kuzavinis	k1gInSc4	Kuzavinis
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jméno	jméno	k1gNnSc1	jméno
země	zem	k1gFnSc2	zem
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
názvu	název	k1gInSc2	název
vodního	vodní	k2eAgInSc2d1	vodní
toku	tok	k1gInSc2	tok
Lietauka	Lietauk	k1gMnSc2	Lietauk
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
název	název	k1gInSc4	název
byl	být	k5eAaImAgMnS	být
slovanismem	slovanismus	k1gInSc7	slovanismus
(	(	kIx(	(
<g/>
správný	správný	k2eAgInSc1d1	správný
název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
Lietava	Lietava	k1gFnSc1	Lietava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
právě	právě	k6eAd1	právě
tato	tento	k3xDgFnSc1	tento
řeka	řeka	k1gFnSc1	řeka
dala	dát	k5eAaPmAgFnS	dát
jméno	jméno	k1gNnSc4	jméno
jednomu	jeden	k4xCgMnSc3	jeden
z	z	k7c2	z
litevských	litevský	k2eAgMnPc2d1	litevský
regionů	region	k1gInPc2	region
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
celé	celý	k2eAgFnSc3d1	celá
Litvě	Litva	k1gFnSc3	Litva
<g/>
.	.	kIx.	.
</s>
<s>
Lietauka	Lietauk	k1gMnSc4	Lietauk
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
pravým	pravý	k2eAgInSc7d1	pravý
přítokem	přítok	k1gInSc7	přítok
řeky	řeka	k1gFnSc2	řeka
Neris	Neris	k1gFnSc2	Neris
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
11	[number]	k4	11
km	km	kA	km
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
vodní	vodní	k2eAgInSc1d1	vodní
tok	tok	k1gInSc1	tok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
30	[number]	k4	30
km	km	kA	km
od	od	k7c2	od
města	město	k1gNnSc2	město
Kernavė	Kernavė	k1gFnSc2	Kernavė
<g/>
,	,	kIx,	,
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
důležitého	důležitý	k2eAgNnSc2d1	důležité
politického	politický	k2eAgNnSc2d1	politické
centra	centrum	k1gNnSc2	centrum
litevského	litevský	k2eAgInSc2d1	litevský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
větší	veliký	k2eAgFnSc6d2	veliký
blízkosti	blízkost	k1gFnSc6	blízkost
nebyly	být	k5eNaImAgFnP	být
nalezeny	nalézt	k5eAaBmNgFnP	nalézt
žádné	žádný	k3yNgFnPc1	žádný
archeologické	archeologický	k2eAgFnPc1d1	archeologická
památky	památka	k1gFnPc1	památka
a	a	k8xC	a
sotva	sotva	k6eAd1	sotva
tam	tam	k6eAd1	tam
co	co	k3yInSc1	co
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
bažinaté	bažinatý	k2eAgNnSc1d1	bažinaté
místo	místo	k1gNnSc1	místo
<g/>
.	.	kIx.	.
</s>
<s>
Říčka	říčka	k1gFnSc1	říčka
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
nevýznamná	významný	k2eNgFnSc1d1	nevýznamná
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
11	[number]	k4	11
km	km	kA	km
<g/>
)	)	kIx)	)
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
dát	dát	k5eAaPmF	dát
jméno	jméno	k1gNnSc4	jméno
celé	celá	k1gFnSc2	celá
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
tuto	tento	k3xDgFnSc4	tento
verzi	verze	k1gFnSc4	verze
lingvisté	lingvista	k1gMnPc1	lingvista
většinou	většina	k1gFnSc7	většina
odmítají	odmítat	k5eAaImIp3nP	odmítat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Simas	Simas	k1gMnSc1	Simas
Karaliū	Karaliū	k1gMnSc1	Karaliū
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
originální	originální	k2eAgFnSc4d1	originální
hypotézu	hypotéza	k1gFnSc4	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
název	název	k1gInSc1	název
Litvy	Litva	k1gFnSc2	Litva
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
německým	německý	k2eAgNnSc7d1	německé
slovem	slovo	k1gNnSc7	slovo
leiten	leiten	k2eAgMnSc1d1	leiten
(	(	kIx(	(
<g/>
vést	vést	k5eAaImF	vést
<g/>
,	,	kIx,	,
velet	velet	k5eAaImF	velet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
slovo	slovo	k1gNnSc1	slovo
je	být	k5eAaImIp3nS	být
pokládáno	pokládat	k5eAaImNgNnS	pokládat
za	za	k7c4	za
odvozeninu	odvozenina	k1gFnSc4	odvozenina
indoevropského	indoevropský	k2eAgInSc2d1	indoevropský
prajazyka	prajazyk	k1gInSc2	prajazyk
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
i	i	k9	i
v	v	k7c6	v
litevštině	litevština	k1gFnSc6	litevština
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
prý	prý	k9	prý
zpočátku	zpočátku	k6eAd1	zpočátku
znamenal	znamenat	k5eAaImAgInS	znamenat
"	"	kIx"	"
<g/>
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
"	"	kIx"	"
a	a	k8xC	a
tuto	tento	k3xDgFnSc4	tento
souvislost	souvislost	k1gFnSc4	souvislost
leiten	leiten	k2eAgMnSc1d1	leiten
-	-	kIx~	-
vojsko	vojsko	k1gNnSc1	vojsko
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
i	i	k9	i
historické	historický	k2eAgInPc4d1	historický
dokumenty	dokument	k1gInPc4	dokument
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Artū	Artū	k?	Artū
Dubonis	Dubonis	k1gFnSc1	Dubonis
na	na	k7c6	na
základě	základ	k1gInSc6	základ
historických	historický	k2eAgInPc2d1	historický
údajů	údaj	k1gInPc2	údaj
uvedl	uvést	k5eAaPmAgMnS	uvést
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
původní	původní	k2eAgFnSc1d1	původní
forma	forma	k1gFnSc1	forma
ethnonyma	ethnonymum	k1gNnSc2	ethnonymum
byla	být	k5eAaImAgFnS	být
leitis	leitis	k1gFnSc1	leitis
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kmen	kmen	k1gInSc1	kmen
slova	slovo	k1gNnSc2	slovo
liet-	liet-	k?	liet-
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
leit-	leit-	k?	leit-
bylo	být	k5eAaImAgNnS	být
lingvistům	lingvista	k1gMnPc3	lingvista
známo	znám	k2eAgNnSc1d1	známo
již	již	k9	již
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
A.	A.	kA	A.
Dubonis	Dubonis	k1gInSc1	Dubonis
dokázal	dokázat	k5eAaPmAgInS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
označením	označení	k1gNnSc7	označení
leitis	leitis	k1gFnSc2	leitis
byli	být	k5eAaImAgMnP	být
Litevci	Litevec	k1gMnPc1	Litevec
nazýváni	nazýván	k2eAgMnPc1d1	nazýván
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
zdrojích	zdroj	k1gInPc6	zdroj
z	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dalo	dát	k5eAaPmAgNnS	dát
název	název	k1gInSc4	název
určité	určitý	k2eAgFnSc6d1	určitá
specifické	specifický	k2eAgFnSc6d1	specifická
vrstvě	vrstva	k1gFnSc6	vrstva
zemanů	zeman	k1gMnPc2	zeman
Velkoknížectví	velkoknížectví	k1gNnPc2	velkoknížectví
litevského	litevský	k2eAgInSc2d1	litevský
<g/>
.	.	kIx.	.
</s>
<s>
Leitis	Leitis	k1gInSc1	Leitis
totiž	totiž	k9	totiž
mělo	mít	k5eAaImAgNnS	mít
zhruba	zhruba	k6eAd1	zhruba
tyto	tento	k3xDgInPc4	tento
významy	význam	k1gInPc4	význam
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Vrstva	vrstva	k1gFnSc1	vrstva
sloužících	sloužící	k2eAgNnPc2d1	sloužící
velkoknížeti	velkokníže	k1gMnSc3	velkokníže
<g/>
:	:	kIx,	:
vlastnili	vlastnit	k5eAaImAgMnP	vlastnit
polnosti	polnost	k1gFnPc4	polnost
ve	v	k7c6	v
vlasti	vlast	k1gFnSc6	vlast
<g/>
,	,	kIx,	,
vydržovali	vydržovat	k5eAaImAgMnP	vydržovat
jeho	jeho	k3xOp3gMnPc1	jeho
koně	kůň	k1gMnPc1	kůň
<g/>
,	,	kIx,	,
platili	platit	k5eAaImAgMnP	platit
daně	daň	k1gFnSc2	daň
přímo	přímo	k6eAd1	přímo
panovníkovi	panovníkův	k2eAgMnPc1d1	panovníkův
<g/>
,	,	kIx,	,
sloužili	sloužit	k5eAaImAgMnP	sloužit
mu	on	k3xPp3gMnSc3	on
za	za	k7c2	za
vojenských	vojenský	k2eAgNnPc2d1	vojenské
tažení	tažení	k1gNnPc2	tažení
<g/>
.	.	kIx.	.
</s>
<s>
Panovníkovi	panovníkův	k2eAgMnPc1d1	panovníkův
vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
také	také	k9	také
vybírali	vybírat	k5eAaImAgMnP	vybírat
mýtné	mýtné	k1gNnSc4	mýtné
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zesílení	zesílení	k1gNnSc6	zesílení
pozic	pozice	k1gFnPc2	pozice
šlechty	šlechta	k1gFnSc2	šlechta
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
pro	pro	k7c4	pro
panovníka	panovník	k1gMnSc4	panovník
již	již	k6eAd1	již
nepotřebnými	potřebný	k2eNgFnPc7d1	nepotřebná
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
zanikli	zaniknout	k5eAaPmAgMnP	zaniknout
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
se	se	k3xPyFc4	se
sami	sám	k3xTgMnPc1	sám
stali	stát	k5eAaPmAgMnP	stát
šlechtici	šlechtic	k1gMnPc1	šlechtic
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Věc	věc	k1gFnSc1	věc
<g/>
,	,	kIx,	,
záležitost	záležitost	k1gFnSc1	záležitost
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
národnosti	národnost	k1gFnSc2	národnost
(	(	kIx(	(
<g/>
Litevců	Litevec	k1gMnPc2	Litevec
<g/>
)	)	kIx)	)
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
takto	takto	k6eAd1	takto
byli	být	k5eAaImAgMnP	být
nazýváni	nazýván	k2eAgMnPc1d1	nazýván
všichni	všechen	k3xTgMnPc1	všechen
Litevci	Litevec	k1gMnPc1	Litevec
kromě	kromě	k7c2	kromě
Žemaitiů	Žemaiti	k1gMnPc2	Žemaiti
(	(	kIx(	(
<g/>
Žmuďanů	Žmuďan	k1gMnPc2	Žmuďan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lotyštině	lotyština	k1gFnSc6	lotyština
se	se	k3xPyFc4	se
termín	termín	k1gInSc1	termín
Leitis	Leitis	k1gFnSc2	Leitis
množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
Leiši	Leiše	k1gFnSc3	Leiše
jako	jako	k8xC	jako
alternativní	alternativní	k2eAgInPc1d1	alternativní
k	k	k7c3	k
novějšímu	nový	k2eAgNnSc3d2	novější
Lietuvietis	Lietuvietis	k1gFnPc3	Lietuvietis
udržel	udržet	k5eAaPmAgInS	udržet
až	až	k9	až
dosud	dosud	k6eAd1	dosud
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
,	,	kIx,	,
stejného	stejný	k2eAgInSc2d1	stejný
původu	původ	k1gInSc2	původ
-	-	kIx~	-
ovšem	ovšem	k9	ovšem
s	s	k7c7	s
jiným	jiný	k2eAgInSc7d1	jiný
směrem	směr	k1gInSc7	směr
variací	variace	k1gFnPc2	variace
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
i	i	k9	i
název	název	k1gInSc1	název
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
(	(	kIx(	(
<g/>
Lettonia	Lettonium	k1gNnSc2	Lettonium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Lidé	člověk	k1gMnPc1	člověk
žili	žít	k5eAaImAgMnP	žít
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
území	území	k1gNnSc6	území
již	již	k6eAd1	již
od	od	k7c2	od
pravěku	pravěk	k1gInSc2	pravěk
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgInPc1d1	historický
záznamy	záznam	k1gInPc1	záznam
svědčí	svědčit	k5eAaImIp3nP	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
obchod	obchod	k1gInSc1	obchod
tu	tu	k6eAd1	tu
vzkvétal	vzkvétat	k5eAaImAgInS	vzkvétat
již	již	k6eAd1	již
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
50	[number]	k4	50
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
odtud	odtud	k6eAd1	odtud
vyvážel	vyvážet	k5eAaImAgInS	vyvážet
jantar	jantar	k1gInSc4	jantar
až	až	k9	až
do	do	k7c2	do
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInPc1	první
litevské	litevský	k2eAgInPc1d1	litevský
státní	státní	k2eAgInPc1d1	státní
útvary	útvar	k1gInPc1	útvar
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
až	až	k9	až
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1240	[number]	k4	1240
se	se	k3xPyFc4	se
spojily	spojit	k5eAaPmAgFnP	spojit
ve	v	k7c6	v
Velkoknížectví	velkoknížectví	k1gNnSc6	velkoknížectví
litevské	litevský	k2eAgNnSc1d1	litevské
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
Litevci	Litevec	k1gMnPc1	Litevec
odrazili	odrazit	k5eAaPmAgMnP	odrazit
útok	útok	k1gInSc4	útok
Řádu	řád	k1gInSc2	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
a	a	k8xC	a
udrželi	udržet	k5eAaPmAgMnP	udržet
si	se	k3xPyFc3	se
svou	svůj	k3xOyFgFnSc4	svůj
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
Litva	Litva	k1gFnSc1	Litva
expandovala	expandovat	k5eAaImAgFnS	expandovat
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
a	a	k8xC	a
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
mongolské	mongolský	k2eAgFnSc2d1	mongolská
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
hordy	horda	k1gFnSc2	horda
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Modrých	modrý	k2eAgFnPc2d1	modrá
vod	voda	k1gFnPc2	voda
roku	rok	k1gInSc2	rok
1362	[number]	k4	1362
se	se	k3xPyFc4	se
Litva	Litva	k1gFnSc1	Litva
zmocnila	zmocnit	k5eAaPmAgFnS	zmocnit
Kyjevského	kyjevský	k2eAgNnSc2d1	Kyjevské
knížectví	knížectví	k1gNnSc2	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1385	[number]	k4	1385
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
Litva	Litva	k1gFnSc1	Litva
do	do	k7c2	do
Krewské	Krewský	k2eAgFnSc2d1	Krewský
unie	unie	k1gFnSc2	unie
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1387	[number]	k4	1387
přijala	přijmout	k5eAaPmAgFnS	přijmout
Litva	Litva	k1gFnSc1	Litva
jako	jako	k8xC	jako
poslední	poslední	k2eAgFnSc1d1	poslední
země	země	k1gFnSc1	země
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Litvy	Litva	k1gFnSc2	Litva
vzešla	vzejít	k5eAaPmAgFnS	vzejít
polská	polský	k2eAgFnSc1d1	polská
panovnická	panovnický	k2eAgFnSc1d1	panovnická
dynastie	dynastie	k1gFnSc1	dynastie
Jagellonců	Jagellonec	k1gMnPc2	Jagellonec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1569	[number]	k4	1569
uzavřelo	uzavřít	k5eAaPmAgNnS	uzavřít
Velkoknížectví	velkoknížectví	k1gNnSc1	velkoknížectví
litevské	litevský	k2eAgFnPc4d1	Litevská
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Královstvím	království	k1gNnSc7	království
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Lublinskou	lublinský	k2eAgFnSc4d1	Lublinská
unii	unie	k1gFnSc4	unie
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
ní	on	k3xPp3gFnSc6	on
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
Litva	Litva	k1gFnSc1	Litva
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
stát	stát	k1gInSc4	stát
dvou	dva	k4xCgInPc2	dva
uznaných	uznaný	k2eAgInPc2d1	uznaný
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Republiku	republika	k1gFnSc4	republika
obou	dva	k4xCgInPc2	dva
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
státě	stát	k1gInSc6	stát
měli	mít	k5eAaImAgMnP	mít
větší	veliký	k2eAgFnSc4d2	veliký
vliv	vliv	k1gInSc1	vliv
Poláci	Polák	k1gMnPc1	Polák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1702	[number]	k4	1702
až	až	k9	až
1706	[number]	k4	1706
byla	být	k5eAaImAgFnS	být
Litva	Litva	k1gFnSc1	Litva
okupována	okupován	k2eAgFnSc1d1	okupována
Švédy	švéda	k1gFnPc1	švéda
<g/>
.	.	kIx.	.
</s>
<s>
Litva	Litva	k1gFnSc1	Litva
byla	být	k5eAaImAgFnS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1795	[number]	k4	1795
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
stát	stát	k1gInSc1	stát
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
třetího	třetí	k4xOgNnSc2	třetí
dělení	dělení	k1gNnSc2	dělení
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
území	území	k1gNnSc2	území
Litvy	Litva	k1gFnSc2	Litva
připadla	připadnout	k5eAaPmAgFnS	připadnout
Ruskému	ruský	k2eAgNnSc3d1	ruské
impériu	impérium	k1gNnSc3	impérium
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnSc4d2	menší
část	část	k1gFnSc4	část
připojilo	připojit	k5eAaPmAgNnS	připojit
Prusko	Prusko	k1gNnSc1	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1812	[number]	k4	1812
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
Litvy	Litva	k1gFnSc2	Litva
nakrátko	nakrátko	k6eAd1	nakrátko
obsazeno	obsadit	k5eAaPmNgNnS	obsadit
francouzskými	francouzský	k2eAgMnPc7d1	francouzský
vojáky	voják	k1gMnPc7	voják
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Napoleona	Napoleon	k1gMnSc2	Napoleon
carem	car	k1gMnSc7	car
Alexandrem	Alexandr	k1gMnSc7	Alexandr
I	i	k8xC	i
nadále	nadále	k6eAd1	nadále
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
Ruskou	ruský	k2eAgFnSc7d1	ruská
provincií	provincie	k1gFnSc7	provincie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
Litvy	Litva	k1gFnSc2	Litva
obsazeno	obsadit	k5eAaPmNgNnS	obsadit
císařským	císařský	k2eAgNnSc7d1	císařské
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
generalita	generalita	k1gFnSc1	generalita
prosazovala	prosazovat	k5eAaImAgFnS	prosazovat
anexi	anexe	k1gFnSc4	anexe
území	území	k1gNnSc2	území
Litvy	Litva	k1gFnSc2	Litva
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1918	[number]	k4	1918
podepsala	podepsat	k5eAaPmAgFnS	podepsat
sovětská	sovětský	k2eAgFnSc1d1	sovětská
delegace	delegace	k1gFnSc1	delegace
mír	mír	k1gInSc1	mír
v	v	k7c6	v
Brestu	Brest	k1gInSc6	Brest
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
se	se	k3xPyFc4	se
vzdala	vzdát	k5eAaPmAgFnS	vzdát
velkého	velký	k2eAgNnSc2d1	velké
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgMnSc2	jenž
patřila	patřit	k5eAaImAgFnS	patřit
i	i	k9	i
Litva	Litva	k1gFnSc1	Litva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
carské	carský	k2eAgFnSc2d1	carská
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
Litva	Litva	k1gFnSc1	Litva
osamostatnila	osamostatnit	k5eAaPmAgFnS	osamostatnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
měla	mít	k5eAaImAgFnS	mít
napjaté	napjatý	k2eAgInPc4d1	napjatý
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
území	území	k1gNnSc4	území
okolí	okolí	k1gNnSc2	okolí
města	město	k1gNnSc2	město
Vilniusu	Vilnius	k1gInSc2	Vilnius
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
Litvou	Litva	k1gFnSc7	Litva
považováno	považován	k2eAgNnSc4d1	považováno
za	za	k7c4	za
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
<g/>
;	;	kIx,	;
Litevci	Litevec	k1gMnPc5	Litevec
zřídili	zřídit	k5eAaPmAgMnP	zřídit
tedy	tedy	k9	tedy
jako	jako	k9	jako
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Kaunas	Kaunasa	k1gFnPc2	Kaunasa
<g/>
.	.	kIx.	.
</s>
<s>
Litva	Litva	k1gFnSc1	Litva
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
anexí	anexe	k1gFnPc2	anexe
přístav	přístav	k1gInSc4	přístav
Klaipė	Klaipė	k1gFnSc4	Klaipė
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
musela	muset	k5eAaImAgFnS	muset
Litva	Litva	k1gFnSc1	Litva
vydat	vydat	k5eAaPmF	vydat
právě	právě	k9	právě
Klaipė	Klaipė	k1gFnSc4	Klaipė
pod	pod	k7c7	pod
nátlakem	nátlak	k1gInSc7	nátlak
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
okupaci	okupace	k1gFnSc6	okupace
východních	východní	k2eAgFnPc2d1	východní
částí	část	k1gFnPc2	část
Polska	Polsko	k1gNnSc2	Polsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
Vilniusu	Vilnius	k1gInSc2	Vilnius
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
předáno	předat	k5eAaPmNgNnS	předat
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
Litvě	Litva	k1gFnSc3	Litva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
byla	být	k5eAaImAgFnS	být
Litva	Litva	k1gFnSc1	Litva
okupována	okupovat	k5eAaBmNgFnS	okupovat
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
okupaci	okupace	k1gFnSc4	okupace
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
dohodl	dohodnout	k5eAaPmAgMnS	dohodnout
sovětský	sovětský	k2eAgMnSc1d1	sovětský
vůdce	vůdce	k1gMnSc1	vůdce
Stalin	Stalin	k1gMnSc1	Stalin
s	s	k7c7	s
Hitlerem	Hitler	k1gMnSc7	Hitler
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1939	[number]	k4	1939
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
paktu	pakt	k1gInSc2	pakt
Ribbentrop-Molotov	Ribbentrop-Molotovo	k1gNnPc2	Ribbentrop-Molotovo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
napadení	napadení	k1gNnSc6	napadení
SSSR	SSSR	kA	SSSR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
Litvy	Litva	k1gFnSc2	Litva
okupováno	okupovat	k5eAaBmNgNnS	okupovat
Němci	Němec	k1gMnPc7	Němec
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1941	[number]	k4	1941
až	až	k9	až
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ponarech	Ponar	k1gInPc6	Ponar
němečtí	německý	k2eAgMnPc1d1	německý
nacisté	nacista	k1gMnPc1	nacista
zavraždili	zavraždit	k5eAaPmAgMnP	zavraždit
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
Poláků	Polák	k1gMnPc2	Polák
a	a	k8xC	a
Rusů	Rus	k1gMnPc2	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
masakrech	masakr	k1gInPc6	masakr
Židů	Žid	k1gMnPc2	Žid
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
podíleli	podílet	k5eAaImAgMnP	podílet
i	i	k9	i
litevští	litevský	k2eAgMnPc1d1	litevský
kolaboranti	kolaborant	k1gMnPc1	kolaborant
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
litevský	litevský	k2eAgInSc4d1	litevský
prapor	prapor	k1gInSc4	prapor
působil	působit	k5eAaImAgMnS	působit
pod	pod	k7c7	pod
německým	německý	k2eAgNnSc7d1	německé
velením	velení	k1gNnSc7	velení
v	v	k7c6	v
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
zavraždění	zavraždění	k1gNnSc6	zavraždění
300	[number]	k4	300
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1944	[number]	k4	1944
Sověti	Sovět	k1gMnPc1	Sovět
společně	společně	k6eAd1	společně
s	s	k7c7	s
Poláky	Polák	k1gMnPc7	Polák
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Vilnius	Vilnius	k1gMnSc1	Vilnius
a	a	k8xC	a
Litva	Litva	k1gFnSc1	Litva
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
stalinský	stalinský	k2eAgInSc1d1	stalinský
režim	režim	k1gInSc1	režim
deportoval	deportovat	k5eAaBmAgInS	deportovat
desítky	desítka	k1gFnPc1	desítka
tisíc	tisíc	k4xCgInSc1	tisíc
Litevců	Litevec	k1gMnPc2	Litevec
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
Litevců	Litevec	k1gMnPc2	Litevec
odešlo	odejít	k5eAaPmAgNnS	odejít
do	do	k7c2	do
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jako	jako	k8xS	jako
lesní	lesní	k2eAgMnPc1d1	lesní
bratři	bratr	k1gMnPc1	bratr
vedli	vést	k5eAaImAgMnP	vést
proti	proti	k7c3	proti
Sovětům	Sovět	k1gMnPc3	Sovět
partyzánskou	partyzánský	k2eAgFnSc4d1	Partyzánská
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
<g/>
Novodobý	novodobý	k2eAgInSc1d1	novodobý
samostatný	samostatný	k2eAgInSc1d1	samostatný
litevský	litevský	k2eAgInSc1d1	litevský
stát	stát	k1gInSc1	stát
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
rada	rada	k1gFnSc1	rada
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Litevské	litevský	k2eAgFnSc2d1	Litevská
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
Zákon	zákon	k1gInSc4	zákon
o	o	k7c4	o
obnovení	obnovení	k1gNnSc4	obnovení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Litevské	litevský	k2eAgFnSc2d1	Litevská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
situace	situace	k1gFnSc1	situace
však	však	k9	však
byla	být	k5eAaImAgFnS	být
svízelná	svízelný	k2eAgFnSc1d1	svízelná
<g/>
;	;	kIx,	;
okamžitě	okamžitě	k6eAd1	okamžitě
vystartovala	vystartovat	k5eAaPmAgFnS	vystartovat
inflace	inflace	k1gFnSc2	inflace
a	a	k8xC	a
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
zaveden	zavést	k5eAaPmNgInS	zavést
přídělový	přídělový	k2eAgInSc1d1	přídělový
systém	systém	k1gInSc1	systém
na	na	k7c4	na
potraviny	potravina	k1gFnPc4	potravina
<g/>
.	.	kIx.	.
</s>
<s>
SSSR	SSSR	kA	SSSR
nezávislost	nezávislost	k1gFnSc1	nezávislost
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
v	v	k7c6	v
ústavě	ústava	k1gFnSc6	ústava
zakotvenou	zakotvený	k2eAgFnSc7d1	zakotvená
<g/>
,	,	kIx,	,
neuznal	uznat	k5eNaPmAgMnS	uznat
a	a	k8xC	a
následně	následně	k6eAd1	následně
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1991	[number]	k4	1991
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
potyčkám	potyčka	k1gFnPc3	potyčka
mezi	mezi	k7c7	mezi
stoupenci	stoupenec	k1gMnPc7	stoupenec
litevské	litevský	k2eAgFnSc2d1	Litevská
samostatnosti	samostatnost	k1gFnSc2	samostatnost
(	(	kIx(	(
<g/>
u	u	k7c2	u
Vilniuské	vilniuský	k2eAgFnSc2d1	Vilniuská
televizní	televizní	k2eAgFnSc2d1	televizní
věže	věž	k1gFnSc2	věž
<g/>
)	)	kIx)	)
a	a	k8xC	a
sovětskými	sovětský	k2eAgMnPc7d1	sovětský
vojáky	voják	k1gMnPc7	voják
rozmístěnými	rozmístěný	k2eAgMnPc7d1	rozmístěný
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zahynulo	zahynout	k5eAaPmAgNnS	zahynout
celkem	celkem	k6eAd1	celkem
14	[number]	k4	14
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
stovky	stovka	k1gFnPc1	stovka
byly	být	k5eAaImAgFnP	být
zraněny	zranit	k5eAaPmNgFnP	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Cestu	cesta	k1gFnSc4	cesta
Litvy	Litva	k1gFnSc2	Litva
k	k	k7c3	k
uznání	uznání	k1gNnSc3	uznání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
urychlila	urychlit	k5eAaPmAgFnS	urychlit
snaha	snaha	k1gFnSc1	snaha
dalších	další	k2eAgFnPc2d1	další
svazových	svazový	k2eAgFnPc2d1	svazová
republik	republika	k1gFnPc2	republika
vystoupit	vystoupit	k5eAaPmF	vystoupit
ze	z	k7c2	z
svazku	svazek	k1gInSc2	svazek
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Litva	Litva	k1gFnSc1	Litva
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
členem	člen	k1gMnSc7	člen
NATO	NATO	kA	NATO
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2004	[number]	k4	2004
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
novějších	nový	k2eAgFnPc2d2	novější
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
opustilo	opustit	k5eAaPmAgNnS	opustit
zemi	zem	k1gFnSc4	zem
370	[number]	k4	370
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poloha	poloha	k1gFnSc1	poloha
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
geografického	geografický	k2eAgInSc2d1	geografický
pohledu	pohled	k1gInSc2	pohled
se	se	k3xPyFc4	se
Litva	Litva	k1gFnSc1	Litva
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
i	i	k8xC	i
když	když	k8xS	když
podle	podle	k7c2	podle
OSN	OSN	kA	OSN
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
do	do	k7c2	do
států	stát	k1gInPc2	stát
severní	severní	k2eAgFnSc2d1	severní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
jihovýchodním	jihovýchodní	k2eAgNnSc6d1	jihovýchodní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nejpravděpodobnější	pravděpodobný	k2eAgFnSc2d3	nejpravděpodobnější
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
metody	metoda	k1gFnSc2	metoda
leží	ležet	k5eAaImIp3nS	ležet
střed	střed	k1gInSc1	střed
Evropy	Evropa	k1gFnSc2	Evropa
26	[number]	k4	26
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Vilniusu	Vilnius	k1gInSc2	Vilnius
u	u	k7c2	u
vesnice	vesnice	k1gFnSc2	vesnice
Purnuškė	Purnuškė	k1gFnSc2	Purnuškė
<g/>
.	.	kIx.	.
</s>
<s>
Stanovili	stanovit	k5eAaPmAgMnP	stanovit
to	ten	k3xDgNnSc1	ten
vědci	vědec	k1gMnPc1	vědec
z	z	k7c2	z
Francouzského	francouzský	k2eAgInSc2d1	francouzský
geografického	geografický	k2eAgInSc2d1	geografický
institutu	institut	k1gInSc2	institut
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
některé	některý	k3yIgInPc4	některý
jiné	jiný	k2eAgInPc4d1	jiný
evropské	evropský	k2eAgInPc4d1	evropský
státy	stát	k1gInPc4	stát
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
střed	střed	k1gInSc1	střed
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
právě	právě	k9	právě
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
území	území	k1gNnSc6	území
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hraničí	hraničit	k5eAaImIp3nS	hraničit
s	s	k7c7	s
Lotyšskem	Lotyšsko	k1gNnSc7	Lotyšsko
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
hranic	hranice	k1gFnPc2	hranice
588	[number]	k4	588
km	km	kA	km
<g/>
)	)	kIx)	)
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
s	s	k7c7	s
Běloruskem	Bělorusko	k1gNnSc7	Bělorusko
(	(	kIx(	(
<g/>
677	[number]	k4	677
km	km	kA	km
<g/>
)	)	kIx)	)
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
<g/>
,	,	kIx,	,
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
(	(	kIx(	(
<g/>
104	[number]	k4	104
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
ruskou	ruský	k2eAgFnSc7d1	ruská
Kaliningradskou	kaliningradský	k2eAgFnSc7d1	Kaliningradská
oblastí	oblast	k1gFnSc7	oblast
(	(	kIx(	(
<g/>
273	[number]	k4	273
km	km	kA	km
<g/>
)	)	kIx)	)
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Litva	Litva	k1gFnSc1	Litva
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
mírném	mírný	k2eAgNnSc6d1	mírné
podnebném	podnebný	k2eAgNnSc6d1	podnebné
pásmu	pásmo	k1gNnSc6	pásmo
na	na	k7c6	na
přechodu	přechod	k1gInSc6	přechod
oceánského	oceánský	k2eAgNnSc2d1	oceánské
a	a	k8xC	a
kontinentálního	kontinentální	k2eAgNnSc2d1	kontinentální
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýchodnější	východní	k2eAgInSc1d3	nejvýchodnější
bod	bod	k1gInSc1	bod
Litvy	Litva	k1gFnSc2	Litva
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
vesnice	vesnice	k1gFnSc2	vesnice
Vosiū	Vosiū	k1gFnSc2	Vosiū
asi	asi	k9	asi
40	[number]	k4	40
km	km	kA	km
od	od	k7c2	od
Ignaliny	Ignalina	k1gFnSc2	Ignalina
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
°	°	k?	°
50	[number]	k4	50
<g/>
'	'	kIx"	'
v.d.	v.d.	k?	v.d.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejzápadnější	západní	k2eAgInSc1d3	nejzápadnější
bod	bod	k1gInSc1	bod
u	u	k7c2	u
města	město	k1gNnSc2	město
Nida	Nid	k1gInSc2	Nid
na	na	k7c6	na
Kuršské	Kuršský	k2eAgFnSc6d1	Kuršská
kose	kosa	k1gFnSc6	kosa
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
°	°	k?	°
57	[number]	k4	57
<g/>
'	'	kIx"	'
v.d.	v.d.	k?	v.d.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejsevernější	severní	k2eAgInSc1d3	nejsevernější
bod	bod	k1gInSc1	bod
u	u	k7c2	u
vesnice	vesnice	k1gFnSc2	vesnice
Aspariškiai	Aspariškia	k1gFnSc2	Aspariškia
(	(	kIx(	(
<g/>
56	[number]	k4	56
<g/>
°	°	k?	°
27	[number]	k4	27
<g/>
'	'	kIx"	'
s.	s.	k?	s.
<g/>
š.	š.	k?	š.
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejjižnější	jižní	k2eAgInSc1d3	nejjižnější
bod	bod	k1gInSc1	bod
u	u	k7c2	u
vesnice	vesnice	k1gFnSc2	vesnice
Musteika	Musteikum	k1gNnSc2	Musteikum
(	(	kIx(	(
<g/>
53	[number]	k4	53
<g/>
°	°	k?	°
54	[number]	k4	54
<g/>
'	'	kIx"	'
s.	s.	k?	s.
<g/>
š.	š.	k?	š.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Střed	střed	k1gInSc1	střed
Litvy	Litva	k1gFnSc2	Litva
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
6,5	[number]	k4	6,5
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
města	město	k1gNnSc2	město
Kė	Kė	k1gFnSc2	Kė
(	(	kIx(	(
<g/>
55	[number]	k4	55
<g/>
°	°	k?	°
20	[number]	k4	20
<g/>
'	'	kIx"	'
s.	s.	k?	s.
<g/>
š.	š.	k?	š.
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
°	°	k?	°
54	[number]	k4	54
<g/>
'	'	kIx"	'
v.d.	v.d.	k?	v.d.
u	u	k7c2	u
vsi	ves	k1gFnSc2	ves
Roščiai	Roščia	k1gFnSc2	Roščia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
středem	střed	k1gInSc7	střed
Litvy	Litva	k1gFnSc2	Litva
a	a	k8xC	a
středem	střed	k1gInSc7	střed
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
850	[number]	k4	850
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
nejzápadnějším	západní	k2eAgInSc7d3	nejzápadnější
a	a	k8xC	a
nejvýchodnějším	východní	k2eAgInSc7d3	nejvýchodnější
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
373	[number]	k4	373
km	km	kA	km
a	a	k8xC	a
mezi	mezi	k7c7	mezi
nejjižnějším	jižní	k2eAgInSc7d3	nejjižnější
a	a	k8xC	a
nejsevernějším	severní	k2eAgInSc7d3	nejsevernější
bodem	bod	k1gInSc7	bod
276	[number]	k4	276
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
rozloha	rozloha	k1gFnSc1	rozloha
Litvy	Litva	k1gFnSc2	Litva
je	být	k5eAaImIp3nS	být
65	[number]	k4	65
200	[number]	k4	200
km2	km2	k4	km2
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
např.	např.	kA	např.
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
Estonsko	Estonsko	k1gNnSc1	Estonsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
např.	např.	kA	např.
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
stejnou	stejný	k2eAgFnSc4d1	stejná
rozlohu	rozloha	k1gFnSc4	rozloha
má	mít	k5eAaImIp3nS	mít
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
<g/>
.	.	kIx.	.
</s>
<s>
Východ	východ	k1gInSc1	východ
slunce	slunce	k1gNnSc2	slunce
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
východě	východ	k1gInSc6	východ
o	o	k7c4	o
23	[number]	k4	23
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
20	[number]	k4	20
sekund	sekunda	k1gFnPc2	sekunda
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Litva	Litva	k1gFnSc1	Litva
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
východoevropském	východoevropský	k2eAgNnSc6d1	východoevropské
časovém	časový	k2eAgNnSc6d1	časové
pásmu	pásmo	k1gNnSc6	pásmo
GMT	GMT	kA	GMT
+2	+2	k4	+2
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
např.	např.	kA	např.
Helsinky	Helsinky	k1gFnPc1	Helsinky
<g/>
,	,	kIx,	,
Riga	Riga	k1gFnSc1	Riga
<g/>
,	,	kIx,	,
Athény	Athéna	k1gFnPc1	Athéna
nebo	nebo	k8xC	nebo
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jako	jako	k8xC	jako
většina	většina	k1gFnSc1	většina
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
zimní	zimní	k2eAgInSc4d1	zimní
a	a	k8xC	a
letní	letní	k2eAgInSc4d1	letní
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
byla	být	k5eAaImAgFnS	být
několikrát	několikrát	k6eAd1	několikrát
na	na	k7c6	na
vládní	vládní	k2eAgFnSc6d1	vládní
úrovni	úroveň	k1gFnSc6	úroveň
zvažována	zvažován	k2eAgFnSc1d1	zvažována
možnost	možnost	k1gFnSc1	možnost
zrušení	zrušení	k1gNnSc2	zrušení
letního	letní	k2eAgInSc2d1	letní
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Povrch	povrch	k1gInSc1	povrch
===	===	k?	===
</s>
</p>
<p>
<s>
Povrch	povrch	k1gInSc1	povrch
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
vesměs	vesměs	k6eAd1	vesměs
rovinatý	rovinatý	k2eAgInSc1d1	rovinatý
a	a	k8xC	a
nížinný	nížinný	k2eAgInSc1d1	nížinný
<g/>
,	,	kIx,	,
průměrná	průměrný	k2eAgFnSc1d1	průměrná
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
činí	činit	k5eAaImIp3nS	činit
99	[number]	k4	99
m.	m.	k?	m.
Východ	východ	k1gInSc1	východ
státu	stát	k1gInSc2	stát
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
Baltských	baltský	k2eAgInPc2d1	baltský
vrchů	vrch	k1gInPc2	vrch
(	(	kIx(	(
<g/>
litevsky	litevsky	k6eAd1	litevsky
Baltijos	Baltijos	k1gMnSc1	Baltijos
aukštumos	aukštumos	k1gMnSc1	aukštumos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
v	v	k7c6	v
pleistocénu	pleistocén	k1gInSc6	pleistocén
jako	jako	k9	jako
ledovcové	ledovcový	k2eAgInPc4d1	ledovcový
nánosy	nános	k1gInPc4	nános
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
i	i	k9	i
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
Žemaitijská	Žemaitijský	k2eAgFnSc1d1	Žemaitijský
vysočina	vysočina	k1gFnSc1	vysočina
(	(	kIx(	(
<g/>
litevsky	litevsky	k6eAd1	litevsky
Žemaičių	Žemaičių	k1gMnSc2	Žemaičių
aukštuma	aukštum	k1gMnSc2	aukštum
<g/>
)	)	kIx)	)
na	na	k7c6	na
západě	západ	k1gInSc6	západ
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Střed	střed	k1gInSc1	střed
země	zem	k1gFnSc2	zem
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
Litevská	litevský	k2eAgFnSc1d1	Litevská
nížina	nížina	k1gFnSc1	nížina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
nadmořskou	nadmořský	k2eAgFnSc4d1	nadmořská
výšku	výška	k1gFnSc4	výška
100	[number]	k4	100
m.	m.	k?	m.
Morénové	morénový	k2eAgInPc1d1	morénový
valy	val	k1gInPc1	val
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
při	při	k7c6	při
ústupu	ústup	k1gInSc6	ústup
ledovce	ledovec	k1gInSc2	ledovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
místem	místo	k1gNnSc7	místo
je	být	k5eAaImIp3nS	být
vrch	vrch	k1gInSc1	vrch
Aukštojas	Aukštojas	k1gInSc1	Aukštojas
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
293,84	[number]	k4	293,84
m	m	kA	m
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Litvy	Litva	k1gFnSc2	Litva
v	v	k7c6	v
pahorkatině	pahorkatina	k1gFnSc6	pahorkatina
Baltských	baltský	k2eAgInPc2d1	baltský
vrchů	vrch	k1gInPc2	vrch
29	[number]	k4	29
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Vilniusu	Vilnius	k1gInSc2	Vilnius
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2005	[number]	k4	2005
se	se	k3xPyFc4	se
za	za	k7c4	za
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
bod	bod	k1gInSc4	bod
Litvy	Litva	k1gFnSc2	Litva
označoval	označovat	k5eAaImAgMnS	označovat
kopec	kopec	k1gInSc4	kopec
Juozapinė	Juozapinė	k1gFnSc2	Juozapinė
<g/>
,	,	kIx,	,
novější	nový	k2eAgNnPc1d2	novější
měření	měření	k1gNnPc1	měření
však	však	k9	však
ukázala	ukázat	k5eAaPmAgNnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
24	[number]	k4	24
cm	cm	kA	cm
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
nedaleký	daleký	k2eNgMnSc1d1	nedaleký
Aukštojas	Aukštojas	k1gMnSc1	Aukštojas
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
podobnou	podobný	k2eAgFnSc4d1	podobná
výšku	výška	k1gFnSc4	výška
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
nedaleký	daleký	k2eNgInSc1d1	nedaleký
kopec	kopec	k1gInSc1	kopec
Kruopynė	Kruopynė	k1gFnSc2	Kruopynė
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měří	měřit	k5eAaImIp3nS	měřit
293,4	[number]	k4	293,4
m.	m.	k?	m.
Litva	Litva	k1gFnSc1	Litva
má	mít	k5eAaImIp3nS	mít
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
pobaltských	pobaltský	k2eAgInPc2d1	pobaltský
států	stát	k1gInPc2	stát
nejníže	nízce	k6eAd3	nízce
položený	položený	k2eAgInSc4d1	položený
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
bod	bod	k1gInSc4	bod
<g/>
:	:	kIx,	:
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
bod	bod	k1gInSc4	bod
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
a	a	k8xC	a
Estonska	Estonsko	k1gNnSc2	Estonsko
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
310	[number]	k4	310
m.	m.	k?	m.
</s>
</p>
<p>
<s>
Kdysi	kdysi	k6eAd1	kdysi
byla	být	k5eAaImAgFnS	být
Litva	Litva	k1gFnSc1	Litva
velmi	velmi	k6eAd1	velmi
zalesněná	zalesněný	k2eAgFnSc1d1	zalesněná
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
lesy	les	k1gInPc1	les
tvoří	tvořit	k5eAaImIp3nP	tvořit
pouze	pouze	k6eAd1	pouze
30	[number]	k4	30
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
rozlohy	rozloha	k1gFnSc2	rozloha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lese	les	k1gInSc6	les
nejčastěji	často	k6eAd3	často
roste	růst	k5eAaImIp3nS	růst
borovice	borovice	k1gFnSc1	borovice
<g/>
,	,	kIx,	,
smrk	smrk	k1gInSc1	smrk
a	a	k8xC	a
bříza	bříza	k1gFnSc1	bříza
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
zřídka	zřídka	k6eAd1	zřídka
i	i	k9	i
jasan	jasan	k1gInSc1	jasan
a	a	k8xC	a
dub	dub	k1gInSc1	dub
<g/>
.	.	kIx.	.
</s>
<s>
Lesy	les	k1gInPc1	les
jsou	být	k5eAaImIp3nP	být
bohaté	bohatý	k2eAgFnPc1d1	bohatá
na	na	k7c4	na
houby	houba	k1gFnPc4	houba
<g/>
,	,	kIx,	,
bobule	bobule	k1gFnPc4	bobule
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Pole	pole	k1gFnPc1	pole
a	a	k8xC	a
louky	louka	k1gFnPc1	louka
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
57	[number]	k4	57
%	%	kIx~	%
<g/>
,	,	kIx,	,
vnitrozemské	vnitrozemský	k2eAgFnSc2d1	vnitrozemská
vody	voda	k1gFnSc2	voda
4	[number]	k4	4
%	%	kIx~	%
<g/>
,	,	kIx,	,
mokřady	mokřad	k1gInPc1	mokřad
a	a	k8xC	a
bažiny	bažina	k1gFnPc1	bažina
3	[number]	k4	3
%	%	kIx~	%
a	a	k8xC	a
6	[number]	k4	6
%	%	kIx~	%
území	území	k1gNnSc4	území
ostatní	ostatní	k2eAgNnSc4d1	ostatní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vodstvo	vodstvo	k1gNnSc1	vodstvo
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
poslední	poslední	k2eAgFnSc6d1	poslední
velké	velký	k2eAgFnSc6d1	velká
době	doba	k1gFnSc6	doba
ledové	ledový	k2eAgFnPc1d1	ledová
se	se	k3xPyFc4	se
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
opět	opět	k6eAd1	opět
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
hustá	hustý	k2eAgFnSc1d1	hustá
říční	říční	k2eAgFnSc1d1	říční
síť	síť	k1gFnSc1	síť
a	a	k8xC	a
množství	množství	k1gNnSc1	množství
drobných	drobný	k2eAgNnPc2d1	drobné
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
bažin	bažina	k1gFnPc2	bažina
<g/>
,	,	kIx,	,
močálů	močál	k1gInPc2	močál
a	a	k8xC	a
rašelinišť	rašeliniště	k1gNnPc2	rašeliniště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Litva	Litva	k1gFnSc1	Litva
má	mít	k5eAaImIp3nS	mít
90	[number]	k4	90
km	km	kA	km
písečného	písečný	k2eAgNnSc2d1	písečné
mořského	mořský	k2eAgNnSc2d1	mořské
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
představuje	představovat	k5eAaImIp3nS	představovat
Kurský	kurský	k2eAgInSc1d1	kurský
záliv	záliv	k1gInSc1	záliv
<g/>
,	,	kIx,	,
oddělený	oddělený	k2eAgMnSc1d1	oddělený
od	od	k7c2	od
moře	moře	k1gNnSc2	moře
úzkým	úzký	k2eAgInSc7d1	úzký
písečným	písečný	k2eAgInSc7d1	písečný
poloostrovem	poloostrov	k1gInSc7	poloostrov
<g/>
,	,	kIx,	,
označovaným	označovaný	k2eAgNnSc7d1	označované
jako	jako	k8xC	jako
Kurská	kurský	k2eAgFnSc1d1	Kurská
kosa	kosa	k1gFnSc1	kosa
<g/>
.	.	kIx.	.
</s>
<s>
Poloostrov	poloostrov	k1gInSc1	poloostrov
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
mezi	mezi	k7c4	mezi
Litvu	Litva	k1gFnSc4	Litva
a	a	k8xC	a
ruskou	ruský	k2eAgFnSc4d1	ruská
Kaliningradskou	kaliningradský	k2eAgFnSc4d1	Kaliningradská
oblast	oblast	k1gFnSc4	oblast
(	(	kIx(	(
<g/>
k	k	k7c3	k
pevnině	pevnina	k1gFnSc3	pevnina
je	být	k5eAaImIp3nS	být
připojen	připojit	k5eAaPmNgInS	připojit
na	na	k7c6	na
ruské	ruský	k2eAgFnSc6d1	ruská
straně	strana	k1gFnSc6	strana
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
zapsán	zapsat	k5eAaPmNgMnS	zapsat
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
světového	světový	k2eAgNnSc2d1	světové
přírodního	přírodní	k2eAgNnSc2d1	přírodní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Kurský	kurský	k2eAgInSc1d1	kurský
záliv	záliv	k1gInSc1	záliv
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
dva	dva	k4xCgInPc4	dva
až	až	k9	až
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
zamrzá	zamrzat	k5eAaImIp3nS	zamrzat
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
teplota	teplota	k1gFnSc1	teplota
vody	voda	k1gFnSc2	voda
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
25	[number]	k4	25
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
litevskou	litevský	k2eAgFnSc7d1	Litevská
řekou	řeka	k1gFnSc7	řeka
je	být	k5eAaImIp3nS	být
Němen	Němen	k1gInSc1	Němen
(	(	kIx(	(
<g/>
litevsky	litevsky	k6eAd1	litevsky
Nemunas	Nemunas	k1gInSc1	Nemunas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
sem	sem	k6eAd1	sem
přitéká	přitékat	k5eAaImIp3nS	přitékat
z	z	k7c2	z
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
a	a	k8xC	a
ústí	ústí	k1gNnSc2	ústí
do	do	k7c2	do
Kurského	kurský	k2eAgInSc2d1	kurský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
116	[number]	k4	116
km	km	kA	km
představuje	představovat	k5eAaImIp3nS	představovat
hranici	hranice	k1gFnSc4	hranice
Litvy	Litva	k1gFnSc2	Litva
s	s	k7c7	s
Ruskou	ruský	k2eAgFnSc7d1	ruská
federací	federace	k1gFnSc7	federace
a	a	k8xC	a
s	s	k7c7	s
Běloruskem	Bělorusko	k1gNnSc7	Bělorusko
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
Němenu	Němen	k1gInSc2	Němen
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
až	až	k9	až
70	[number]	k4	70
%	%	kIx~	%
území	území	k1gNnSc6	území
Litvy	Litva	k1gFnSc2	Litva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
tradici	tradice	k1gFnSc6	tradice
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
otec	otec	k1gMnSc1	otec
litevských	litevský	k2eAgFnPc2d1	Litevská
řek	řeka	k1gFnPc2	řeka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
"	"	kIx"	"
<g/>
matku	matka	k1gFnSc4	matka
litevských	litevský	k2eAgFnPc2d1	Litevská
řek	řeka	k1gFnPc2	řeka
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
pokládá	pokládat	k5eAaImIp3nS	pokládat
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
řeka	řeka	k1gFnSc1	řeka
Litvy	Litva	k1gFnSc2	Litva
-	-	kIx~	-
Neris	Neris	k1gInSc1	Neris
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
přítok	přítok	k1gInSc1	přítok
Němenu	Němen	k1gInSc2	Němen
na	na	k7c6	na
území	území	k1gNnSc6	území
Litvy	Litva	k1gFnSc2	Litva
<g/>
.	.	kIx.	.
</s>
<s>
Neris	Neris	k1gFnSc1	Neris
pramenící	pramenící	k2eAgFnSc1d1	pramenící
v	v	k7c6	v
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
protéká	protékat	k5eAaImIp3nS	protékat
Vilniusem	Vilnius	k1gInSc7	Vilnius
a	a	k8xC	a
vlévá	vlévat	k5eAaImIp3nS	vlévat
se	se	k3xPyFc4	se
do	do	k7c2	do
Němenu	Němen	k1gInSc2	Němen
v	v	k7c6	v
Kaunasu	Kaunas	k1gInSc6	Kaunas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3000	[number]	k4	3000
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
–	–	k?	–
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
jinde	jinde	k6eAd1	jinde
v	v	k7c6	v
Pobaltí	Pobaltí	k1gNnSc6	Pobaltí
–	–	k?	–
ledovcového	ledovcový	k2eAgInSc2d1	ledovcový
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Pozornost	pozornost	k1gFnSc1	pozornost
si	se	k3xPyFc3	se
zaslouží	zasloužit	k5eAaPmIp3nS	zasloužit
zejména	zejména	k9	zejména
jezero	jezero	k1gNnSc1	jezero
Vištytis	Vištytis	k1gFnSc2	Vištytis
u	u	k7c2	u
stejnojmenného	stejnojmenný	k2eAgNnSc2d1	stejnojmenné
městečka	městečko	k1gNnSc2	městečko
<g/>
,	,	kIx,	,
ležícího	ležící	k2eAgInSc2d1	ležící
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
břehu	břeh	k1gInSc6	břeh
(	(	kIx(	(
<g/>
litevsky	litevsky	k6eAd1	litevsky
Vištyčio	Vištyčio	k1gMnSc1	Vištyčio
ežeras	ežeras	k1gMnSc1	ežeras
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
В	В	k?	В
о	о	k?	о
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
velkou	velký	k2eAgFnSc4d1	velká
rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
flory	flora	k1gFnSc2	flora
a	a	k8xC	a
fauny	fauna	k1gFnSc2	fauna
bývá	bývat	k5eAaImIp3nS	bývat
někdy	někdy	k6eAd1	někdy
označováno	označován	k2eAgNnSc4d1	označováno
evropský	evropský	k2eAgInSc4d1	evropský
Bajkal	Bajkal	k1gInSc4	Bajkal
<g/>
.	.	kIx.	.
</s>
<s>
Malebná	malebný	k2eAgFnSc1d1	malebná
zvlněná	zvlněný	k2eAgFnSc1d1	zvlněná
a	a	k8xC	a
zalesněná	zalesněný	k2eAgFnSc1d1	zalesněná
krajina	krajina	k1gFnSc1	krajina
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
za	za	k7c4	za
přírodní	přírodní	k2eAgFnSc4d1	přírodní
rezervaci	rezervace	k1gFnSc4	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
skutečný	skutečný	k2eAgInSc1d1	skutečný
vodní	vodní	k2eAgInSc1d1	vodní
labyrint	labyrint	k1gInSc1	labyrint
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
představuje	představovat	k5eAaImIp3nS	představovat
jezerní	jezerní	k2eAgFnSc1d1	jezerní
oblast	oblast	k1gFnSc1	oblast
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
Ignalina	Ignalin	k2eAgFnSc1d1	Ignalina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
i	i	k9	i
největší	veliký	k2eAgNnSc1d3	veliký
jezero	jezero	k1gNnSc1	jezero
Litvy	Litva	k1gFnSc2	Litva
-	-	kIx~	-
Drū	Drū	k1gFnSc1	Drū
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
dnes	dnes	k6eAd1	dnes
patří	patřit	k5eAaImIp3nS	patřit
Bělorusku	Běloruska	k1gFnSc4	Běloruska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
břehu	břeh	k1gInSc6	břeh
funguje	fungovat	k5eAaImIp3nS	fungovat
od	od	k7c2	od
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
nemá	mít	k5eNaImIp3nS	mít
původně	původně	k6eAd1	původně
plánovanou	plánovaný	k2eAgFnSc4d1	plánovaná
kapacitu	kapacita	k1gFnSc4	kapacita
(	(	kIx(	(
<g/>
po	po	k7c6	po
černobylské	černobylský	k2eAgFnSc6d1	Černobylská
havárii	havárie	k1gFnSc6	havárie
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
zastavili	zastavit	k5eAaPmAgMnP	zastavit
výstavbu	výstavba	k1gFnSc4	výstavba
dalších	další	k2eAgInPc2d1	další
bloků	blok	k1gInPc2	blok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
ochránci	ochránce	k1gMnPc1	ochránce
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
elektrárny	elektrárna	k1gFnSc2	elektrárna
malé	malý	k2eAgFnSc6d1	malá
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
vrací	vracet	k5eAaImIp3nS	vracet
z	z	k7c2	z
chladicí	chladicí	k2eAgFnSc2d1	chladicí
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
jeho	jeho	k3xOp3gNnSc4	jeho
oteplování	oteplování	k1gNnSc4	oteplování
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
negativní	negativní	k2eAgInPc4d1	negativní
důsledky	důsledek	k1gInPc4	důsledek
na	na	k7c4	na
ekosystém	ekosystém	k1gInSc4	ekosystém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Litva	Litva	k1gFnSc1	Litva
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
několik	několik	k4yIc4	několik
malých	malý	k2eAgInPc2d1	malý
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
deltě	delta	k1gFnSc6	delta
Němenu	Němen	k1gInSc2	Němen
–	–	k?	–
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
například	například	k6eAd1	například
Rusnė	Rusnė	k1gMnSc1	Rusnė
sala	sala	k1gMnSc1	sala
<g/>
,	,	kIx,	,
Briedžių	Briedžių	k1gMnSc1	Briedžių
sala	sala	k1gMnSc1	sala
<g/>
,	,	kIx,	,
Kiemo	Kiema	k1gFnSc5	Kiema
sala	sal	k2eAgNnPc1d1	sal
<g/>
,	,	kIx,	,
Vito	vit	k2eAgNnSc1d1	Vito
sala	sala	k6eAd1	sala
<g/>
,	,	kIx,	,
Kubilių	Kubilių	k1gMnSc1	Kubilių
sala	sala	k1gMnSc1	sala
<g/>
,	,	kIx,	,
Trušių	Trušių	k1gMnSc1	Trušių
sala	sala	k1gMnSc1	sala
<g/>
,	,	kIx,	,
Vilkinė	Vilkinė	k1gMnSc1	Vilkinė
sala	sala	k1gMnSc1	sala
<g/>
;	;	kIx,	;
v	v	k7c6	v
deltě	delta	k1gFnSc6	delta
Minije	Minije	k1gFnSc2	Minije
-	-	kIx~	-
například	například	k6eAd1	například
Zingelinė	Zingelinė	k1gMnSc1	Zingelinė
sala	sala	k1gMnSc1	sala
<g/>
;	;	kIx,	;
v	v	k7c6	v
Rusnė	Rusnė	k1gFnSc6	Rusnė
-	-	kIx~	-
Ragininkų	Ragininkų	k1gFnSc1	Ragininkų
sala	sala	k1gMnSc1	sala
<g/>
;	;	kIx,	;
v	v	k7c6	v
Kurském	kurský	k2eAgInSc6d1	kurský
zálivu	záliv	k1gInSc6	záliv
-	-	kIx~	-
Kiaulė	Kiaulė	k1gFnSc1	Kiaulė
nugara	nugara	k1gFnSc1	nugara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podnebí	podnebí	k1gNnSc2	podnebí
===	===	k?	===
</s>
</p>
<p>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
chladnějšího	chladný	k2eAgInSc2d2	chladnější
typu	typ	k1gInSc2	typ
mírného	mírný	k2eAgNnSc2d1	mírné
podnebného	podnebný	k2eAgNnSc2d1	podnebné
pásma	pásmo	k1gNnSc2	pásmo
na	na	k7c6	na
přechodu	přechod	k1gInSc6	přechod
oceánského	oceánský	k2eAgNnSc2d1	oceánské
a	a	k8xC	a
kontinentálního	kontinentální	k2eAgNnSc2d1	kontinentální
podnebí	podnebí	k1gNnSc2	podnebí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
lednové	lednový	k2eAgFnPc1d1	lednová
teploty	teplota	k1gFnPc1	teplota
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
jsou	být	k5eAaImIp3nP	být
okolo	okolo	k7c2	okolo
–	–	k?	–
<g/>
2	[number]	k4	2
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
červencové	červencový	k2eAgNnSc1d1	červencové
okolo	okolo	k7c2	okolo
16	[number]	k4	16
°	°	k?	°
<g/>
C.	C.	kA	C.
Ve	v	k7c6	v
vnitrozemském	vnitrozemský	k2eAgInSc6d1	vnitrozemský
Vilniusu	Vilnius	k1gInSc6	Vilnius
jsou	být	k5eAaImIp3nP	být
větší	veliký	k2eAgInPc1d2	veliký
rozdíly	rozdíl	k1gInPc1	rozdíl
teplot	teplota	k1gFnPc2	teplota
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
okolo	okolo	k7c2	okolo
–	–	k?	–
<g/>
6	[number]	k4	6
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
18	[number]	k4	18
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
teploty	teplota	k1gFnSc2	teplota
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
překračují	překračovat	k5eAaImIp3nP	překračovat
hranici	hranice	k1gFnSc4	hranice
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
i	i	k8xC	i
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
a	a	k8xC	a
noční	noční	k2eAgFnPc4d1	noční
teploty	teplota	k1gFnPc4	teplota
většinou	většinou	k6eAd1	většinou
klesnou	klesnout	k5eAaPmIp3nP	klesnout
pod	pod	k7c7	pod
14	[number]	k4	14
°	°	k?	°
<g/>
C.	C.	kA	C.
Někdy	někdy	k6eAd1	někdy
teploty	teplota	k1gFnPc1	teplota
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
dosáhnou	dosáhnout	k5eAaPmIp3nP	dosáhnout
i	i	k8xC	i
hodnot	hodnota	k1gFnPc2	hodnota
vysoko	vysoko	k6eAd1	vysoko
nad	nad	k7c7	nad
30	[number]	k4	30
°	°	k?	°
<g/>
C.	C.	kA	C.
Zimy	zima	k1gFnPc1	zima
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
mrazivé	mrazivý	k2eAgFnPc1d1	mrazivá
<g/>
.	.	kIx.	.
</s>
<s>
Noční	noční	k2eAgFnSc1d1	noční
teplota	teplota	k1gFnSc1	teplota
ve	v	k7c6	v
Vilniusu	Vilnius	k1gInSc6	Vilnius
pravidelně	pravidelně	k6eAd1	pravidelně
každou	každý	k3xTgFnSc4	každý
zimu	zima	k1gFnSc4	zima
klesne	klesnout	k5eAaPmIp3nS	klesnout
pod	pod	k7c4	pod
-15	-15	k4	-15
°	°	k?	°
<g/>
C.	C.	kA	C.
Extrémy	extrém	k1gInPc1	extrém
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
od	od	k7c2	od
na	na	k7c4	na
-34	-34	k4	-34
°	°	k?	°
<g/>
C	C	kA	C
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
až	až	k9	až
na	na	k7c4	na
-43	-43	k4	-43
°	°	k?	°
<g/>
C	C	kA	C
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
činí	činit	k5eAaImIp3nS	činit
od	od	k7c2	od
600	[number]	k4	600
mm	mm	kA	mm
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Litvy	Litva	k1gFnSc2	Litva
až	až	k9	až
po	po	k7c4	po
900	[number]	k4	900
mm	mm	kA	mm
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Náhlé	náhlý	k2eAgFnPc1d1	náhlá
silné	silný	k2eAgFnPc1d1	silná
bouřky	bouřka	k1gFnPc1	bouřka
a	a	k8xC	a
přeháňky	přeháňka	k1gFnPc1	přeháňka
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
)	)	kIx)	)
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
až	až	k9	až
50	[number]	k4	50
%	%	kIx~	%
všech	všecek	k3xTgFnPc2	všecek
srážek	srážka	k1gFnPc2	srážka
spadne	spadnout	k5eAaPmIp3nS	spadnout
právě	právě	k9	právě
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejméně	málo	k6eAd3	málo
srážek	srážka	k1gFnPc2	srážka
spadne	spadnout	k5eAaPmIp3nS	spadnout
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
sněží	sněžit	k5eAaImIp3nS	sněžit
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
smíšenými	smíšený	k2eAgFnPc7d1	smíšená
srážkami	srážka	k1gFnPc7	srážka
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
v	v	k7c6	v
září	září	k1gNnSc6	září
nebo	nebo	k8xC	nebo
květnu	květen	k1gInSc6	květen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
letech	let	k1gInPc6	let
Litva	Litva	k1gFnSc1	Litva
zažila	zažít	k5eAaPmAgFnS	zažít
kvůli	kvůli	k7c3	kvůli
suchu	sucho	k1gNnSc3	sucho
lesní	lesní	k2eAgInPc1d1	lesní
požáry	požár	k1gInPc1	požár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Absolutně	absolutně	k6eAd1	absolutně
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
-42,9	-42,9	k4	-42,9
°	°	k?	°
<g/>
C	C	kA	C
byla	být	k5eAaImAgFnS	být
naměřena	naměřen	k2eAgFnSc1d1	naměřena
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1956	[number]	k4	1956
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Utena	Uten	k1gInSc2	Uten
<g/>
,	,	kIx,	,
a	a	k8xC	a
absolutně	absolutně	k6eAd1	absolutně
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
teplota	teplota	k1gFnSc1	teplota
37,5	[number]	k4	37,5
°	°	k?	°
<g/>
C	C	kA	C
byla	být	k5eAaImAgFnS	být
naměřena	naměřen	k2eAgFnSc1d1	naměřena
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1994	[number]	k4	1994
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Zarasai	Zarasa	k1gFnSc2	Zarasa
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgFnPc1d3	nejnižší
a	a	k8xC	a
nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
zaznamenané	zaznamenaný	k2eAgFnPc1d1	zaznamenaná
teploty	teplota	k1gFnPc1	teplota
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
měsících	měsíc	k1gInPc6	měsíc
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
Litva	Litva	k1gFnSc1	Litva
je	být	k5eAaImIp3nS	být
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
republikou	republika	k1gFnSc7	republika
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
poloprezidencialismu	poloprezidencialismus	k1gInSc2	poloprezidencialismus
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgMnS	volit
přímou	přímý	k2eAgFnSc7d1	přímá
volbou	volba	k1gFnSc7	volba
<g/>
.	.	kIx.	.
</s>
<s>
Jednokomorový	jednokomorový	k2eAgInSc1d1	jednokomorový
parlament	parlament	k1gInSc1	parlament
(	(	kIx(	(
<g/>
Seimas	Seimas	k1gInSc1	Seimas
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
141	[number]	k4	141
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
na	na	k7c4	na
čtyřleté	čtyřletý	k2eAgNnSc4d1	čtyřleté
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
musí	muset	k5eAaImIp3nS	muset
strana	strana	k1gFnSc1	strana
získat	získat	k5eAaPmF	získat
minimálně	minimálně	k6eAd1	minimálně
5	[number]	k4	5
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
koalice	koalice	k1gFnSc1	koalice
7	[number]	k4	7
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgInS	volit
na	na	k7c4	na
pětileté	pětiletý	k2eAgNnSc4d1	pětileté
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
prezidentka	prezidentka	k1gFnSc1	prezidentka
Dalia	Dalia	k1gFnSc1	Dalia
Grybauskaitė	Grybauskaitė	k1gFnSc1	Grybauskaitė
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
postsovětským	postsovětský	k2eAgMnSc7d1	postsovětský
prezidentem	prezident	k1gMnSc7	prezident
byl	být	k5eAaImAgMnS	být
Algirdas	Algirdas	k1gMnSc1	Algirdas
Brazauskas	Brazauskas	k1gMnSc1	Brazauskas
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
Valdas	Valdas	k1gMnSc1	Valdas
Adamkus	Adamkus	k1gMnSc1	Adamkus
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
a	a	k8xC	a
2004	[number]	k4	2004
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rolandas	Rolandas	k1gMnSc1	Rolandas
Paksas	Paksas	k1gMnSc1	Paksas
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
politické	politický	k2eAgFnSc6d1	politická
aféře	aféra	k1gFnSc6	aféra
odvolán	odvolat	k5eAaPmNgMnS	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
je	být	k5eAaImIp3nS	být
premiérem	premiér	k1gMnSc7	premiér
dřívější	dřívější	k2eAgMnSc1d1	dřívější
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
a	a	k8xC	a
dopravy	doprava	k1gFnSc2	doprava
sociální	sociální	k2eAgMnPc1d1	sociální
demokrat	demokrat	k1gMnSc1	demokrat
Algirdas	Algirdas	k1gMnSc1	Algirdas
Butkevičius	Butkevičius	k1gMnSc1	Butkevičius
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Soudní	soudní	k2eAgInSc1d1	soudní
systém	systém	k1gInSc1	systém
===	===	k?	===
</s>
</p>
<p>
<s>
Litva	Litva	k1gFnSc1	Litva
má	mít	k5eAaImIp3nS	mít
čtyřstupňovou	čtyřstupňový	k2eAgFnSc4d1	čtyřstupňová
soustavu	soustava	k1gFnSc4	soustava
obecných	obecný	k2eAgInPc2d1	obecný
soudů	soud	k1gInPc2	soud
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
soudy	soud	k1gInPc4	soud
okresní	okresní	k2eAgMnSc1d1	okresní
(	(	kIx(	(
<g/>
Apylinkė	Apylinkė	k1gMnSc1	Apylinkė
teismas	teismas	k1gMnSc1	teismas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krajské	krajský	k2eAgFnPc1d1	krajská
(	(	kIx(	(
<g/>
Apygardos	Apygardos	k1gMnSc1	Apygardos
teismas	teismas	k1gMnSc1	teismas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odvolací	odvolací	k2eAgFnSc7d1	odvolací
(	(	kIx(	(
<g/>
Apeliacinis	Apeliacinis	k1gFnSc7	Apeliacinis
teismas	teismasa	k1gFnPc2	teismasa
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
(	(	kIx(	(
<g/>
Lietuvos	Lietuvos	k1gInSc1	Lietuvos
Aukščiausiasis	Aukščiausiasis	k1gFnSc2	Aukščiausiasis
Teismas	Teismasa	k1gFnPc2	Teismasa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
obecných	obecný	k2eAgInPc2d1	obecný
soudů	soud	k1gInPc2	soud
existuje	existovat	k5eAaImIp3nS	existovat
dvoustupňová	dvoustupňový	k2eAgFnSc1d1	dvoustupňová
soustava	soustava	k1gFnSc1	soustava
správních	správní	k2eAgInPc2d1	správní
soudů	soud	k1gInPc2	soud
<g/>
,	,	kIx,	,
sestávající	sestávající	k2eAgMnSc1d1	sestávající
z	z	k7c2	z
krajských	krajský	k2eAgInPc2d1	krajský
správních	správní	k2eAgInPc2d1	správní
soudů	soud	k1gInPc2	soud
(	(	kIx(	(
<g/>
Apygardų	Apygardų	k1gFnSc2	Apygardų
administraciniai	administracinia	k1gFnSc2	administracinia
teismai	teisma	k1gFnSc2	teisma
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
správního	správní	k2eAgInSc2d1	správní
soudu	soud	k1gInSc2	soud
(	(	kIx(	(
<g/>
Vyriausiasis	Vyriausiasis	k1gInSc1	Vyriausiasis
administracinis	administracinis	k1gFnSc2	administracinis
teismas	teismasa	k1gFnPc2	teismasa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
===	===	k?	===
</s>
</p>
<p>
<s>
Litva	Litva	k1gFnSc1	Litva
se	se	k3xPyFc4	se
už	už	k6eAd1	už
od	od	k7c2	od
raného	raný	k2eAgInSc2d1	raný
středověku	středověk	k1gInSc2	středověk
dělila	dělit	k5eAaImAgFnS	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
základní	základní	k2eAgFnPc4d1	základní
historické	historický	k2eAgFnPc4d1	historická
země	zem	k1gFnPc4	zem
<g/>
:	:	kIx,	:
východní	východní	k2eAgNnSc1d1	východní
Aukštaitsko	Aukštaitsko	k1gNnSc1	Aukštaitsko
(	(	kIx(	(
<g/>
Aukštaitija	Aukštaitija	k1gFnSc1	Aukštaitija
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Horní	horní	k2eAgFnSc1d1	horní
země	země	k1gFnSc1	země
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
západní	západní	k2eAgFnSc1d1	západní
Žmuď	Žmuď	k1gFnSc1	Žmuď
<g/>
/	/	kIx~	/
<g/>
Žemaitsko	Žemaitsko	k1gNnSc1	Žemaitsko
(	(	kIx(	(
<g/>
Žemaitija	Žemaitija	k1gFnSc1	Žemaitija
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Dolní	dolní	k2eAgFnSc1d1	dolní
země	země	k1gFnSc1	země
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
s	s	k7c7	s
úzkým	úzký	k2eAgInSc7d1	úzký
přístupem	přístup	k1gInSc7	přístup
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
u	u	k7c2	u
Palangy	Palang	k1gInPc7	Palang
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Litevského	litevský	k2eAgNnSc2d1	litevské
velkoknížectví	velkoknížectví	k1gNnSc2	velkoknížectví
měla	mít	k5eAaImAgFnS	mít
zejména	zejména	k9	zejména
Žmuď	Žmuď	k1gFnSc1	Žmuď
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
správní	správní	k2eAgNnSc4d1	správní
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
sousedila	sousedit	k5eAaImAgFnS	sousedit
Malá	Malá	k1gFnSc1	Malá
Litva	Litva	k1gFnSc1	Litva
(	(	kIx(	(
<g/>
Mažoji	Mažoj	k1gInSc6	Mažoj
Lietuva	Lietuva	k1gFnSc1	Lietuva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
vlastně	vlastně	k9	vlastně
nejsevernějším	severní	k2eAgInSc7d3	nejsevernější
výběžkem	výběžek	k1gInSc7	výběžek
řádového	řádový	k2eAgInSc2d1	řádový
státu	stát	k1gInSc2	stát
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Pruska	Prusko	k1gNnSc2	Prusko
<g/>
;	;	kIx,	;
přes	přes	k7c4	přes
svá	svůj	k3xOyFgNnPc4	svůj
výrazná	výrazný	k2eAgNnPc4d1	výrazné
jazyková	jazykový	k2eAgNnPc4d1	jazykové
a	a	k8xC	a
kulturní	kulturní	k2eAgNnPc4d1	kulturní
specifika	specifikon	k1gNnPc4	specifikon
ale	ale	k8xC	ale
Malá	Malá	k1gFnSc1	Malá
<g/>
/	/	kIx~	/
<g/>
Pruská	pruský	k2eAgFnSc1d1	pruská
Litva	Litva	k1gFnSc1	Litva
nikdy	nikdy	k6eAd1	nikdy
netvořila	tvořit	k5eNaImAgFnS	tvořit
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
správní	správní	k2eAgFnSc4d1	správní
jednotku	jednotka	k1gFnSc4	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
–	–	k?	–
pruh	pruh	k1gInSc1	pruh
území	území	k1gNnSc2	území
mezi	mezi	k7c7	mezi
řekou	řeka	k1gFnSc7	řeka
Němenem	Němen	k1gInSc7	Němen
<g/>
,	,	kIx,	,
Kurským	kurský	k2eAgInSc7d1	kurský
zálivem	záliv	k1gInSc7	záliv
a	a	k8xC	a
hranicí	hranice	k1gFnSc7	hranice
Žmudi	Žmud	k1gMnPc1	Žmud
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
důležitým	důležitý	k2eAgInSc7d1	důležitý
přístavem	přístav	k1gInSc7	přístav
Klajpedou	Klajpedý	k2eAgFnSc4d1	Klajpedý
–	–	k?	–
byla	být	k5eAaImAgFnS	být
po	po	k7c4	po
1	[number]	k4	1
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
válce	válka	k1gFnSc6	válka
anektována	anektovat	k5eAaBmNgFnS	anektovat
Litevskou	litevský	k2eAgFnSc7d1	Litevská
republikou	republika	k1gFnSc7	republika
a	a	k8xC	a
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
rámci	rámec	k1gInSc6	rámec
až	až	k6eAd1	až
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
má	mít	k5eAaImIp3nS	mít
Litva	Litva	k1gFnSc1	Litva
ještě	ještě	k6eAd1	ještě
národopisné	národopisný	k2eAgInPc4d1	národopisný
regiony	region	k1gInPc4	region
<g/>
:	:	kIx,	:
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Dzū	Dzū	k1gFnSc1	Dzū
(	(	kIx(	(
<g/>
či	či	k8xC	či
Dainava	Dainava	k1gFnSc1	Dainava
<g/>
)	)	kIx)	)
s	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Vilniusem	Vilnius	k1gInSc7	Vilnius
a	a	k8xC	a
s	s	k7c7	s
regionální	regionální	k2eAgFnSc7d1	regionální
metropolí	metropol	k1gFnSc7	metropol
Alytusem	Alytus	k1gInSc7	Alytus
<g/>
,	,	kIx,	,
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
<g/>
,	,	kIx,	,
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Kaliningradskou	kaliningradský	k2eAgFnSc7d1	Kaliningradská
enklávou	enkláva	k1gFnSc7	enkláva
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
leží	ležet	k5eAaImIp3nS	ležet
Suvalkija	Suvalkija	k1gFnSc1	Suvalkija
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Súduva	Súduva	k1gFnSc1	Súduva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
situovaná	situovaný	k2eAgFnSc1d1	situovaná
zároveň	zároveň	k6eAd1	zároveň
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Němenu	Němen	k1gInSc2	Němen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ani	ani	k8xC	ani
tradiční	tradiční	k2eAgFnPc4d1	tradiční
hranice	hranice	k1gFnPc4	hranice
mezi	mezi	k7c7	mezi
částí	část	k1gFnSc7	část
Malé	Malé	k2eAgFnSc2d1	Malé
Litvy	Litva	k1gFnSc2	Litva
a	a	k8xC	a
Žmudí	Žmudí	k1gNnSc2	Žmudí
či	či	k8xC	či
mezi	mezi	k7c4	mezi
Žmudí	Žmudí	k1gNnSc4	Žmudí
a	a	k8xC	a
Aukštaitskem	Aukštaitsek	k1gInSc7	Aukštaitsek
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
moderním	moderní	k2eAgNnSc6d1	moderní
správním	správní	k2eAgNnSc6d1	správní
uspořádání	uspořádání	k1gNnSc6	uspořádání
Litvy	Litva	k1gFnSc2	Litva
respektovány	respektovat	k5eAaImNgInP	respektovat
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
méně	málo	k6eAd2	málo
pak	pak	k6eAd1	pak
nezřetelné	zřetelný	k2eNgFnPc4d1	nezřetelná
hranice	hranice	k1gFnPc4	hranice
národopisné	národopisný	k2eAgFnPc4d1	národopisná
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
za	za	k7c2	za
éry	éra	k1gFnSc2	éra
prezidenta	prezident	k1gMnSc2	prezident
Rolanda	Rolando	k1gNnSc2	Rolando
Pakse	Pakse	k1gFnSc2	Pakse
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
s	s	k7c7	s
přesným	přesný	k2eAgNnSc7d1	přesné
vymezováním	vymezování	k1gNnSc7	vymezování
hranic	hranice	k1gFnPc2	hranice
těchto	tento	k3xDgFnPc2	tento
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
regionů	region	k1gInPc2	region
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
myšlenka	myšlenka	k1gFnSc1	myšlenka
na	na	k7c4	na
rozdělení	rozdělení	k1gNnSc4	rozdělení
Litvy	Litva	k1gFnSc2	Litva
na	na	k7c4	na
4	[number]	k4	4
či	či	k8xC	či
5	[number]	k4	5
zemí	zem	k1gFnPc2	zem
místo	místo	k7c2	místo
uměle	uměle	k6eAd1	uměle
narýsovaných	narýsovaný	k2eAgInPc2d1	narýsovaný
krajů	kraj	k1gInPc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tyto	tento	k3xDgFnPc4	tento
budoucí	budoucí	k2eAgFnPc4d1	budoucí
země	zem	k1gFnPc4	zem
(	(	kIx(	(
<g/>
žemė	žemė	k?	žemė
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
dokonce	dokonce	k9	dokonce
navrženy	navržen	k2eAgInPc1d1	navržen
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Žmudi	Žmud	k1gMnPc1	Žmud
a	a	k8xC	a
Malé	Malé	k2eAgFnPc1d1	Malé
Litvy	Litva	k1gFnPc1	Litva
znovuobnoveny	znovuobnoveny	k?	znovuobnoveny
<g/>
)	)	kIx)	)
zemské	zemský	k2eAgInPc1d1	zemský
znaky	znak	k1gInPc1	znak
a	a	k8xC	a
vlajky	vlajka	k1gFnPc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
ale	ale	k9	ale
k	k	k7c3	k
plné	plný	k2eAgFnSc3d1	plná
realizaci	realizace	k1gFnSc3	realizace
záměru	záměr	k1gInSc2	záměr
nedošlo	dojít	k5eNaPmAgNnS	dojít
a	a	k8xC	a
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
tak	tak	k6eAd1	tak
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
dělení	dělení	k1gNnSc1	dělení
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
na	na	k7c4	na
10	[number]	k4	10
krajů	kraj	k1gInPc2	kraj
(	(	kIx(	(
<g/>
apskritis	apskritis	k1gFnSc1	apskritis
<g/>
,	,	kIx,	,
mn	mn	k?	mn
<g/>
.	.	kIx.	.
č.	č.	k?	č.
apskritys	apskritys	k1gInSc1	apskritys
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
děleny	dělen	k2eAgFnPc1d1	dělena
na	na	k7c4	na
celkem	celkem	k6eAd1	celkem
60	[number]	k4	60
okresů	okres	k1gInPc2	okres
<g/>
.	.	kIx.	.
</s>
<s>
Kraje	kraj	k1gInPc1	kraj
jsou	být	k5eAaImIp3nP	být
pojmenovány	pojmenován	k2eAgMnPc4d1	pojmenován
po	po	k7c6	po
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
(	(	kIx(	(
<g/>
uvedeném	uvedený	k2eAgInSc6d1	uvedený
v	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Okresy	okres	k1gInPc1	okres
====	====	k?	====
</s>
</p>
<p>
<s>
Správní	správní	k2eAgNnSc1d1	správní
dělení	dělení	k1gNnSc1	dělení
Litvy	Litva	k1gFnSc2	Litva
<g/>
,	,	kIx,	,
klikací	klikací	k2eAgFnSc1d1	klikací
mapka	mapka	k1gFnSc1	mapka
s	s	k7c7	s
okresy	okres	k1gInPc7	okres
<g/>
,	,	kIx,	,
kterých	který	k3yRgInPc2	který
je	být	k5eAaImIp3nS	být
60	[number]	k4	60
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
Obce	obec	k1gFnPc1	obec
====	====	k?	====
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gFnPc2	on
+	+	kIx~	+
<g/>
-	-	kIx~	-
638	[number]	k4	638
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Litva	Litva	k1gFnSc1	Litva
je	být	k5eAaImIp3nS	být
průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
a	a	k8xC	a
zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
převažuje	převažovat	k5eAaImIp3nS	převažovat
živočišná	živočišný	k2eAgFnSc1d1	živočišná
produkce	produkce	k1gFnSc1	produkce
nad	nad	k7c7	nad
rostlinnou	rostlinný	k2eAgFnSc7d1	rostlinná
<g/>
.	.	kIx.	.
</s>
<s>
Chov	chov	k1gInSc1	chov
prasat	prase	k1gNnPc2	prase
<g/>
,	,	kIx,	,
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
koní	kůň	k1gMnPc2	kůň
a	a	k8xC	a
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
mléka	mléko	k1gNnSc2	mléko
a	a	k8xC	a
rybích	rybí	k2eAgInPc2d1	rybí
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
ječmen	ječmen	k1gInSc1	ječmen
<g/>
,	,	kIx,	,
pšenice	pšenice	k1gFnSc1	pšenice
<g/>
,	,	kIx,	,
žito	žito	k1gNnSc1	žito
<g/>
,	,	kIx,	,
len	len	k1gInSc1	len
<g/>
,	,	kIx,	,
brambory	brambor	k1gInPc1	brambor
<g/>
,	,	kIx,	,
cukrová	cukrový	k2eAgFnSc1d1	cukrová
řepa	řepa	k1gFnSc1	řepa
a	a	k8xC	a
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnPc1d1	hlavní
průmyslová	průmyslový	k2eAgNnPc1d1	průmyslové
odvětví	odvětví	k1gNnPc1	odvětví
zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
strojírenství	strojírenství	k1gNnSc4	strojírenství
<g/>
,	,	kIx,	,
elektrotechnika	elektrotechnika	k1gFnSc1	elektrotechnika
<g/>
,	,	kIx,	,
radioelektronika	radioelektronika	k1gFnSc1	radioelektronika
<g/>
,	,	kIx,	,
papírenský	papírenský	k2eAgInSc4d1	papírenský
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc4d1	chemický
<g/>
,	,	kIx,	,
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
<g/>
,	,	kIx,	,
textilní	textilní	k2eAgInSc4d1	textilní
<g/>
,	,	kIx,	,
dřevozpracující	dřevozpracující	k2eAgInSc4d1	dřevozpracující
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
průmysl	průmysl	k1gInSc4	průmysl
stavebních	stavební	k2eAgFnPc2d1	stavební
hmot	hmota	k1gFnPc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
slabé	slabý	k2eAgFnSc3d1	slabá
surovinové	surovinový	k2eAgFnSc3d1	surovinová
základně	základna	k1gFnSc3	základna
ložiska	ložisko	k1gNnSc2	ložisko
rašeliny	rašelina	k1gFnSc2	rašelina
je	být	k5eAaImIp3nS	být
produkce	produkce	k1gFnSc1	produkce
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
jaderné	jaderný	k2eAgFnSc6d1	jaderná
energetice	energetika	k1gFnSc6	energetika
<g/>
.	.	kIx.	.
</s>
<s>
Průmysl	průmysl	k1gInSc1	průmysl
je	být	k5eAaImIp3nS	být
soustředěn	soustředit	k5eAaPmNgInS	soustředit
zejména	zejména	k9	zejména
do	do	k7c2	do
větších	veliký	k2eAgNnPc2d2	veliký
měst	město	k1gNnPc2	město
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Vilnius	Vilnius	k1gInSc4	Vilnius
<g/>
,	,	kIx,	,
Kaunas	Kaunas	k1gInSc4	Kaunas
<g/>
,	,	kIx,	,
Klaipė	Klaipė	k1gFnSc4	Klaipė
a	a	k8xC	a
Šiauliai	Šiauliae	k1gFnSc4	Šiauliae
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Světoznámá	světoznámý	k2eAgFnSc1d1	světoznámá
je	být	k5eAaImIp3nS	být
produkce	produkce	k1gFnSc1	produkce
jantaru	jantar	k1gInSc2	jantar
<g/>
.	.	kIx.	.
</s>
<s>
Ignalinská	Ignalinský	k2eAgFnSc1d1	Ignalinská
jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
60	[number]	k4	60
km	km	kA	km
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
města	město	k1gNnSc2	město
Utena	Uteno	k1gNnSc2	Uteno
<g/>
,	,	kIx,	,
produkovala	produkovat	k5eAaImAgFnS	produkovat
až	až	k9	až
80	[number]	k4	80
%	%	kIx~	%
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
státu	stát	k1gInSc2	stát
<g/>
;	;	kIx,	;
Litva	Litva	k1gFnSc1	Litva
tím	ten	k3xDgNnSc7	ten
zaujímala	zaujímat	k5eAaImAgFnS	zaujímat
přední	přední	k2eAgNnSc4d1	přední
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
;	;	kIx,	;
od	od	k7c2	od
1.1	[number]	k4	1.1
<g/>
.2010	.2010	k4	.2010
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
přístupovou	přístupový	k2eAgFnSc7d1	přístupová
dohodou	dohoda	k1gFnSc7	dohoda
k	k	k7c3	k
EU	EU	kA	EU
odstavena	odstavit	k5eAaPmNgFnS	odstavit
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
její	její	k3xOp3gNnSc1	její
nahrazení	nahrazení	k1gNnSc1	nahrazení
moderní	moderní	k2eAgFnSc7d1	moderní
elektrárnou	elektrárna	k1gFnSc7	elektrárna
v	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
areálu	areál	k1gInSc6	areál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
dobře	dobře	k6eAd1	dobře
vyvinutou	vyvinutý	k2eAgFnSc4d1	vyvinutá
železniční	železniční	k2eAgFnSc4d1	železniční
i	i	k8xC	i
silniční	silniční	k2eAgFnSc4d1	silniční
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
Dálnice	dálnice	k1gFnSc1	dálnice
A1	A1	k1gFnPc2	A1
spojuje	spojovat	k5eAaImIp3nS	spojovat
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Vilnius	Vilnius	k1gInSc4	Vilnius
přes	přes	k7c4	přes
Kaunas	Kaunas	k1gInSc4	Kaunas
s	s	k7c7	s
přístavem	přístav	k1gInSc7	přístav
Klaipė	Klaipė	k1gMnSc1	Klaipė
<g/>
,	,	kIx,	,
A2	A2	k1gMnSc1	A2
potom	potom	k8xC	potom
Vilnius	Vilnius	k1gMnSc1	Vilnius
a	a	k8xC	a
Panevė	Panevė	k1gMnSc1	Panevė
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
komunikací	komunikace	k1gFnSc7	komunikace
je	být	k5eAaImIp3nS	být
Evropská	evropský	k2eAgFnSc1d1	Evropská
silnice	silnice	k1gFnSc1	silnice
E67	E67	k1gFnSc1	E67
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Helsinek	Helsinky	k1gFnPc2	Helsinky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
železnice	železnice	k1gFnSc1	železnice
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Litvy	Litva	k1gFnSc2	Litva
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
letech	let	k1gInPc6	let
1851	[number]	k4	1851
<g/>
–	–	k?	–
<g/>
1862	[number]	k4	1862
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
1	[number]	k4	1
333	[number]	k4	333
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
trať	trať	k1gFnSc1	trať
mezi	mezi	k7c7	mezi
Petrohradem	Petrohrad	k1gInSc7	Petrohrad
a	a	k8xC	a
Varšavou	Varšava	k1gFnSc7	Varšava
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
vlak	vlak	k1gInSc1	vlak
přijel	přijet	k5eAaPmAgInS	přijet
do	do	k7c2	do
Vilniusu	Vilnius	k1gInSc2	Vilnius
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1860	[number]	k4	1860
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
byla	být	k5eAaImAgFnS	být
trať	trať	k1gFnSc1	trať
mezi	mezi	k7c7	mezi
Vilniusem	Vilnius	k1gInSc7	Vilnius
a	a	k8xC	a
Varšavou	Varšava	k1gFnSc7	Varšava
přebudována	přebudován	k2eAgFnSc1d1	přebudována
na	na	k7c4	na
standardní	standardní	k2eAgInSc4d1	standardní
rozchod	rozchod	k1gInSc4	rozchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
1	[number]	k4	1
749	[number]	k4	749
km	km	kA	km
širokorozchodné	širokorozchodný	k2eAgFnSc2d1	širokorozchodná
železnice	železnice	k1gFnSc2	železnice
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
122	[number]	k4	122
km	km	kA	km
je	být	k5eAaImIp3nS	být
elektrifikováno	elektrifikovat	k5eAaBmNgNnS	elektrifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Normální	normální	k2eAgInSc1d1	normální
rozchod	rozchod	k1gInSc1	rozchod
má	mít	k5eAaImIp3nS	mít
22	[number]	k4	22
km	km	kA	km
trati	trať	k1gFnSc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc7d1	státní
firmou	firma	k1gFnSc7	firma
<g/>
,	,	kIx,	,
provozující	provozující	k2eAgFnSc4d1	provozující
železniční	železniční	k2eAgFnSc4d1	železniční
dopravu	doprava	k1gFnSc4	doprava
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
Lietuvos	Lietuvos	k1gInSc4	Lietuvos
geležinkeliai	geležinkelia	k1gFnSc2	geležinkelia
(	(	kIx(	(
<g/>
Litevské	litevský	k2eAgFnSc2d1	Litevská
železnice	železnice	k1gFnSc2	železnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2024	[number]	k4	2024
je	být	k5eAaImIp3nS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
dokončení	dokončení	k1gNnSc1	dokončení
trati	trať	k1gFnSc2	trať
se	s	k7c7	s
standardním	standardní	k2eAgInSc7d1	standardní
rozchodem	rozchod	k1gInSc7	rozchod
z	z	k7c2	z
Helsinek	Helsinky	k1gFnPc2	Helsinky
do	do	k7c2	do
Varšavy	Varšava	k1gFnSc2	Varšava
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
.	.	kIx.	.
<g/>
Přístav	přístav	k1gInSc1	přístav
Klajpeda	Klajped	k1gMnSc2	Klajped
je	být	k5eAaImIp3nS	být
nejsevernější	severní	k2eAgInSc1d3	nejsevernější
nezamrzající	zamrzající	k2eNgInSc1d1	nezamrzající
přístav	přístav	k1gInSc1	přístav
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
přístavem	přístav	k1gInSc7	přístav
prošlo	projít	k5eAaPmAgNnS	projít
31	[number]	k4	31
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
nákladu	náklad	k1gInSc2	náklad
a	a	k8xC	a
321	[number]	k4	321
000	[number]	k4	000
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
jsou	být	k5eAaImIp3nP	být
čtyři	čtyři	k4xCgNnPc1	čtyři
mezinárodní	mezinárodní	k2eAgNnPc1d1	mezinárodní
letiště	letiště	k1gNnPc1	letiště
(	(	kIx(	(
<g/>
Vilnius	Vilnius	k1gInSc1	Vilnius
<g/>
,	,	kIx,	,
Kaunas	Kaunas	k1gInSc1	Kaunas
<g/>
,	,	kIx,	,
Palanga	Palanga	k1gFnSc1	Palanga
a	a	k8xC	a
Šiauliai	Šiauliai	k1gNnSc1	Šiauliai
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
letiště	letiště	k1gNnSc1	letiště
6	[number]	k4	6
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Vilniusu	Vilnius	k1gInSc2	Vilnius
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
odbavilo	odbavit	k5eAaPmAgNnS	odbavit
2,2	[number]	k4	2,2
milionu	milion	k4xCgInSc2	milion
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Letectví	letectví	k1gNnSc2	letectví
====	====	k?	====
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
letiště	letiště	k1gNnSc1	letiště
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
letiště	letiště	k1gNnSc1	letiště
Vilnius	Vilnius	k1gInSc1	Vilnius
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
přepravilo	přepravit	k5eAaPmAgNnS	přepravit
3,8	[number]	k4	3,8
milionů	milion	k4xCgInPc2	milion
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
leteckou	letecký	k2eAgFnSc7d1	letecká
společností	společnost	k1gFnSc7	společnost
jsou	být	k5eAaImIp3nP	být
dovolenkové	dovolenkový	k2eAgFnPc1d1	dovolenková
Small	Small	k1gInSc4	Small
Planet	planeta	k1gFnPc2	planeta
Airlines	Airlinesa	k1gFnPc2	Airlinesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Pohyb	pohyb	k1gInSc1	pohyb
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
===	===	k?	===
</s>
</p>
<p>
<s>
Litevská	litevský	k2eAgFnSc1d1	Litevská
populace	populace	k1gFnSc1	populace
nebezpečně	bezpečně	k6eNd1	bezpečně
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
to	ten	k3xDgNnSc1	ten
i	i	k9	i
sčítání	sčítání	k1gNnSc1	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
předchozích	předchozí	k2eAgNnPc6d1	předchozí
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
odhadoval	odhadovat	k5eAaImAgInS	odhadovat
na	na	k7c4	na
3	[number]	k4	3
674	[number]	k4	674
800	[number]	k4	800
<g/>
.	.	kIx.	.
</s>
<s>
Litevský	litevský	k2eAgInSc1d1	litevský
statistický	statistický	k2eAgInSc1d1	statistický
úřad	úřad	k1gInSc1	úřad
(	(	kIx(	(
<g/>
Statistikos	Statistikos	k1gMnSc1	Statistikos
Departamentas	Departamentas	k1gMnSc1	Departamentas
<g/>
)	)	kIx)	)
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
čtvrtletí	čtvrtletí	k1gNnSc6	čtvrtletí
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
kolem	kolem	k7c2	kolem
3	[number]	k4	3
384	[number]	k4	384
800	[number]	k4	800
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
odhadován	odhadovat	k5eAaImNgInS	odhadovat
na	na	k7c4	na
3	[number]	k4	3
195	[number]	k4	195
702	[number]	k4	702
(	(	kIx(	(
<g/>
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
rokem	rok	k1gInSc7	rok
1989	[number]	k4	1989
pokles	pokles	k1gInSc1	pokles
o	o	k7c4	o
13	[number]	k4	13
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
pokles	pokles	k1gInSc1	pokles
počtu	počet	k1gInSc2	počet
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
především	především	k9	především
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
výrazného	výrazný	k2eAgNnSc2d1	výrazné
snížení	snížení	k1gNnSc2	snížení
počtu	počet	k1gInSc2	počet
narozených	narozený	k2eAgFnPc2d1	narozená
dětí	dítě	k1gFnPc2	dítě
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
56	[number]	k4	56
868	[number]	k4	868
živě	živě	k6eAd1	živě
narozených	narozený	k2eAgFnPc2d1	narozená
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
39	[number]	k4	39
760	[number]	k4	760
lidí	člověk	k1gMnPc2	člověk
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
bylo	být	k5eAaImAgNnS	být
pouze	pouze	k6eAd1	pouze
31	[number]	k4	31
546	[number]	k4	546
živě	živě	k6eAd1	živě
narozených	narozený	k2eAgFnPc2d1	narozená
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
40	[number]	k4	40
399	[number]	k4	399
lidí	člověk	k1gMnPc2	člověk
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
narodilo	narodit	k5eAaPmAgNnS	narodit
35	[number]	k4	35
626	[number]	k4	626
živě	živě	k6eAd1	živě
narozených	narozený	k2eAgFnPc2d1	narozená
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
42	[number]	k4	42
120	[number]	k4	120
lidí	člověk	k1gMnPc2	člověk
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
-	-	kIx~	-
po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
Litvy	Litva	k1gFnSc2	Litva
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
-	-	kIx~	-
se	se	k3xPyFc4	se
také	také	k9	také
Litva	Litva	k1gFnSc1	Litva
potýká	potýkat	k5eAaImIp3nS	potýkat
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
emigrací	emigrace	k1gFnSc7	emigrace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
přirozený	přirozený	k2eAgInSc1d1	přirozený
přírůstek	přírůstek	k1gInSc1	přírůstek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
-0,284	-0,284	k4	-0,284
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Národnostní	národnostní	k2eAgNnSc4d1	národnostní
složení	složení	k1gNnSc4	složení
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnSc1	obyvatel
3	[number]	k4	3
483	[number]	k4	483
972	[number]	k4	972
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
tvořili	tvořit	k5eAaImAgMnP	tvořit
Litevci	Litevec	k1gMnPc1	Litevec
83,45	[number]	k4	83,45
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
polská	polský	k2eAgFnSc1d1	polská
(	(	kIx(	(
<g/>
6,74	[number]	k4	6,74
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
ruská	ruský	k2eAgFnSc1d1	ruská
(	(	kIx(	(
<g/>
6,31	[number]	k4	6,31
%	%	kIx~	%
<g/>
)	)	kIx)	)
menšina	menšina	k1gFnSc1	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Poláci	Polák	k1gMnPc1	Polák
jsou	být	k5eAaImIp3nP	být
největší	veliký	k2eAgFnSc7d3	veliký
menšinou	menšina	k1gFnSc7	menšina
<g/>
,	,	kIx,	,
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
oblasti	oblast	k1gFnSc6	oblast
Litvy	Litva	k1gFnSc2	Litva
(	(	kIx(	(
<g/>
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Vilniusu	Vilnius	k1gInSc2	Vilnius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rusové	Rus	k1gMnPc1	Rus
jsou	být	k5eAaImIp3nP	být
druhou	druhý	k4xOgFnSc4	druhý
největší	veliký	k2eAgFnSc4d3	veliký
menšinou	menšina	k1gFnSc7	menšina
<g/>
,	,	kIx,	,
žijí	žít	k5eAaImIp3nP	žít
většinou	většinou	k6eAd1	většinou
ve	v	k7c6	v
třech	tři	k4xCgNnPc6	tři
městech	město	k1gNnPc6	město
-	-	kIx~	-
ve	v	k7c6	v
Vilniusu	Vilnius	k1gInSc6	Vilnius
tvoří	tvořit	k5eAaImIp3nS	tvořit
14	[number]	k4	14
%	%	kIx~	%
<g/>
,	,	kIx,	,
v	v	k7c6	v
Klaipė	Klaipė	k1gFnSc6	Klaipė
28	[number]	k4	28
%	%	kIx~	%
a	a	k8xC	a
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Visaginas	Visaginasa	k1gFnPc2	Visaginasa
až	až	k9	až
52	[number]	k4	52
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
už	už	k9	už
méně	málo	k6eAd2	málo
významná	významný	k2eAgFnSc1d1	významná
běloruská	běloruský	k2eAgFnSc1d1	Běloruská
menšina	menšina	k1gFnSc1	menšina
(	(	kIx(	(
<g/>
1,23	[number]	k4	1,23
%	%	kIx~	%
<g/>
)	)	kIx)	)
žije	žít	k5eAaImIp3nS	žít
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
pohraničních	pohraniční	k2eAgFnPc6d1	pohraniční
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Romů	Rom	k1gMnPc2	Rom
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
2571	[number]	k4	2571
(	(	kIx(	(
<g/>
0,09	[number]	k4	0,09
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žijí	žít	k5eAaImIp3nP	žít
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
městech	město	k1gNnPc6	město
Vilnius	Vilnius	k1gInSc1	Vilnius
<g/>
,	,	kIx,	,
Kaunas	Kaunas	k1gInSc1	Kaunas
a	a	k8xC	a
Panevė	Panevė	k1gFnSc1	Panevė
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
menšinami	menšina	k1gFnPc7	menšina
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
(	(	kIx(	(
<g/>
0,65	[number]	k4	0,65
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Židé	Žid	k1gMnPc1	Žid
(	(	kIx(	(
<g/>
0,12	[number]	k4	0,12
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc1	Němec
(	(	kIx(	(
<g/>
0,09	[number]	k4	0,09
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tataři	Tatar	k1gMnPc1	Tatar
(	(	kIx(	(
<g/>
0,09	[number]	k4	0,09
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lotyši	Lotyš	k1gMnPc1	Lotyš
(	(	kIx(	(
<g/>
0,08	[number]	k4	0,08
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Arméni	Armén	k1gMnPc1	Armén
(	(	kIx(	(
<g/>
0,04	[number]	k4	0,04
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
0,18	[number]	k4	0,18
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
jsou	být	k5eAaImIp3nP	být
jiné	jiný	k2eAgInPc1d1	jiný
národnosti	národnost	k1gFnPc4	národnost
a	a	k8xC	a
u	u	k7c2	u
0,9	[number]	k4	0,9
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnSc1	jejich
národnost	národnost	k1gFnSc1	národnost
nedala	dát	k5eNaPmAgFnS	dát
identifikovat	identifikovat	k5eAaBmF	identifikovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Litevština	litevština	k1gFnSc1	litevština
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
lotyštinou	lotyština	k1gFnSc7	lotyština
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
baltských	baltský	k2eAgInPc2d1	baltský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
menšina	menšina	k1gFnSc1	menšina
požaduje	požadovat	k5eAaImIp3nS	požadovat
<g/>
,	,	kIx,	,
aby	aby	k9	aby
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
byla	být	k5eAaImAgFnS	být
i	i	k9	i
ruština	ruština	k1gFnSc1	ruština
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
litevština	litevština	k1gFnSc1	litevština
asi	asi	k9	asi
pro	pro	k7c4	pro
84	[number]	k4	84
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
Litvy	Litva	k1gFnSc2	Litva
mateřským	mateřský	k2eAgInSc7d1	mateřský
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
asi	asi	k9	asi
pro	pro	k7c4	pro
8,2	[number]	k4	8,2
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
ruština	ruština	k1gFnSc1	ruština
a	a	k8xC	a
asi	asi	k9	asi
pro	pro	k7c4	pro
5,8	[number]	k4	5,8
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
polština	polština	k1gFnSc1	polština
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
60	[number]	k4	60
%	%	kIx~	%
umí	umět	k5eAaImIp3nS	umět
plynule	plynule	k6eAd1	plynule
mluvit	mluvit	k5eAaImF	mluvit
rusky	rusky	k6eAd1	rusky
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
anglicky	anglicky	k6eAd1	anglicky
pouze	pouze	k6eAd1	pouze
16	[number]	k4	16
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
Eurobarometr	Eurobarometr	k1gInSc4	Eurobarometr
v	v	k7c6	v
průzkumech	průzkum	k1gInPc6	průzkum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
80	[number]	k4	80
%	%	kIx~	%
Litevců	Litevec	k1gMnPc2	Litevec
umí	umět	k5eAaImIp3nS	umět
mluvit	mluvit	k5eAaImF	mluvit
rusky	rusky	k6eAd1	rusky
a	a	k8xC	a
32	[number]	k4	32
%	%	kIx~	%
mluví	mluvit	k5eAaImIp3nS	mluvit
anglicky	anglicky	k6eAd1	anglicky
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
litevských	litevský	k2eAgFnPc2d1	Litevská
škol	škola	k1gFnPc2	škola
se	se	k3xPyFc4	se
jako	jako	k9	jako
první	první	k4xOgInSc1	první
cizí	cizí	k2eAgInSc1d1	cizí
jazyk	jazyk	k1gInSc1	jazyk
učí	učit	k5eAaImIp3nS	učit
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
studenti	student	k1gMnPc1	student
se	se	k3xPyFc4	se
také	také	k9	také
mohou	moct	k5eAaImIp3nP	moct
učit	učit	k5eAaImF	učit
německy	německy	k6eAd1	německy
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
školách	škola	k1gFnPc6	škola
i	i	k9	i
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
ruskou	ruský	k2eAgFnSc7d1	ruská
menšinou	menšina	k1gFnSc7	menšina
jsou	být	k5eAaImIp3nP	být
částečně	částečně	k6eAd1	částečně
komplikované	komplikovaný	k2eAgInPc1d1	komplikovaný
<g/>
.	.	kIx.	.
</s>
<s>
Menšina	menšina	k1gFnSc1	menšina
požaduje	požadovat	k5eAaImIp3nS	požadovat
texty	text	k1gInPc4	text
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
latince	latinka	k1gFnSc6	latinka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Litevci	Litevec	k1gMnPc1	Litevec
znovu	znovu	k6eAd1	znovu
zavedli	zavést	k5eAaPmAgMnP	zavést
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
cyrilici	cyrilice	k1gFnSc6	cyrilice
<g/>
.	.	kIx.	.
</s>
<s>
Rusové	Rus	k1gMnPc1	Rus
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
od	od	k7c2	od
připojení	připojení	k1gNnSc2	připojení
země	zem	k1gFnSc2	zem
k	k	k7c3	k
carskému	carský	k2eAgNnSc3d1	carské
Rusku	Rusko	k1gNnSc3	Rusko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
vlna	vlna	k1gFnSc1	vlna
osidlování	osidlování	k1gNnSc2	osidlování
začala	začít	k5eAaPmAgFnS	začít
po	po	k7c6	po
sovětské	sovětský	k2eAgFnSc6d1	sovětská
okupaci	okupace	k1gFnSc6	okupace
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Komplikované	komplikovaný	k2eAgInPc1d1	komplikovaný
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
polskou	polský	k2eAgFnSc7d1	polská
menšinou	menšina	k1gFnSc7	menšina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
litevské	litevský	k2eAgInPc1d1	litevský
úřady	úřad	k1gInPc1	úřad
administrativně	administrativně	k6eAd1	administrativně
omezují	omezovat	k5eAaImIp3nP	omezovat
polské	polský	k2eAgNnSc1d1	polské
školství	školství	k1gNnSc1	školství
a	a	k8xC	a
používaní	používaný	k2eAgMnPc1d1	používaný
polštiny	polština	k1gFnSc2	polština
<g/>
.	.	kIx.	.
</s>
<s>
Poláci	Polák	k1gMnPc1	Polák
nemohou	moct	k5eNaImIp3nP	moct
používat	používat	k5eAaImF	používat
svá	svůj	k3xOyFgNnPc4	svůj
polská	polský	k2eAgNnPc4d1	polské
příjmení	příjmení	k1gNnPc4	příjmení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Litvy	Litva	k1gFnSc2	Litva
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
názvy	název	k1gInPc1	název
a	a	k8xC	a
nápisy	nápis	k1gInPc1	nápis
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
litevštině	litevština	k1gFnSc6	litevština
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
opatření	opatření	k1gNnPc1	opatření
jsou	být	k5eAaImIp3nP	být
namířena	namířen	k2eAgNnPc1d1	namířeno
proti	proti	k7c3	proti
všem	všecek	k3xTgFnPc3	všecek
menšinám	menšina	k1gFnPc3	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
restitucemi	restituce	k1gFnPc7	restituce
majetku	majetek	k1gInSc2	majetek
Poláků	polák	k1gInPc2	polák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
polský	polský	k2eAgMnSc1d1	polský
prezident	prezident	k1gMnSc1	prezident
Lech	Lech	k1gMnSc1	Lech
Wałęsa	Wałęs	k1gMnSc4	Wałęs
převzít	převzít	k5eAaPmF	převzít
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
litevské	litevský	k2eAgNnSc4d1	litevské
vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
a	a	k8xC	a
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
je	být	k5eAaImIp3nS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
převzít	převzít	k5eAaPmF	převzít
teprve	teprve	k6eAd1	teprve
poté	poté	k6eAd1	poté
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
zlepší	zlepšit	k5eAaPmIp3nS	zlepšit
postavení	postavení	k1gNnSc1	postavení
polské	polský	k2eAgFnSc2d1	polská
menšiny	menšina	k1gFnSc2	menšina
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
<g/>
.	.	kIx.	.
<g/>
Demografický	demografický	k2eAgInSc1d1	demografický
obraz	obraz	k1gInSc1	obraz
Litvy	Litva	k1gFnSc2	Litva
výrazně	výrazně	k6eAd1	výrazně
poznamenaly	poznamenat	k5eAaPmAgFnP	poznamenat
turbulence	turbulence	k1gFnPc1	turbulence
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
sovětské	sovětský	k2eAgFnSc2d1	sovětská
okupace	okupace	k1gFnSc2	okupace
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1940	[number]	k4	1940
a	a	k8xC	a
1941	[number]	k4	1941
bylo	být	k5eAaImAgNnS	být
asi	asi	k9	asi
5000	[number]	k4	5000
lidí	člověk	k1gMnPc2	člověk
popravených	popravený	k2eAgMnPc2d1	popravený
a	a	k8xC	a
45	[number]	k4	45
000	[number]	k4	000
deportovaných	deportovaný	k2eAgMnPc2d1	deportovaný
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
německé	německý	k2eAgFnSc2d1	německá
okupace	okupace	k1gFnSc2	okupace
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1941	[number]	k4	1941
a	a	k8xC	a
1945	[number]	k4	1945
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
okolo	okolo	k7c2	okolo
370	[number]	k4	370
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
opětovném	opětovný	k2eAgNnSc6d1	opětovné
připojení	připojení	k1gNnSc6	připojení
Litvy	Litva	k1gFnSc2	Litva
k	k	k7c3	k
SSSR	SSSR	kA	SSSR
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
emigrovalo	emigrovat	k5eAaBmAgNnS	emigrovat
asi	asi	k9	asi
80	[number]	k4	80
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
okolo	okolo	k7c2	okolo
205	[number]	k4	205
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
deportovali	deportovat	k5eAaBmAgMnP	deportovat
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Nejkrutější	krutý	k2eAgInSc1d3	nejkrutější
osud	osud	k1gInSc1	osud
postihl	postihnout	k5eAaPmAgInS	postihnout
Židy	Žid	k1gMnPc4	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
se	s	k7c7	s
cca	cca	kA	cca
7,5	[number]	k4	7,5
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
židovské	židovská	k1gFnSc3	židovská
národnosti	národnost	k1gFnSc2	národnost
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
městské	městský	k2eAgNnSc1d1	Městské
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
<g/>
,	,	kIx,	,
řemeslníci	řemeslník	k1gMnPc1	řemeslník
a	a	k8xC	a
obchodníci	obchodník	k1gMnPc1	obchodník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vilniusu	Vilnius	k1gInSc2	Vilnius
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Židé	Žid	k1gMnPc1	Žid
tvořili	tvořit	k5eAaImAgMnP	tvořit
až	až	k6eAd1	až
30	[number]	k4	30
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
někdy	někdy	k6eAd1	někdy
přezdíváno	přezdívat	k5eAaImNgNnS	přezdívat
severní	severní	k2eAgFnSc1d1	severní
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Takřka	takřka	k6eAd1	takřka
všichni	všechen	k3xTgMnPc1	všechen
litevští	litevský	k2eAgMnPc1d1	litevský
Židé	Žid	k1gMnPc1	Žid
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
za	za	k7c2	za
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
během	během	k7c2	během
holokaustu	holokaust	k1gInSc2	holokaust
nebo	nebo	k8xC	nebo
emigrovali	emigrovat	k5eAaBmAgMnP	emigrovat
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
do	do	k7c2	do
USA	USA	kA	USA
a	a	k8xC	a
Izraele	Izrael	k1gInSc2	Izrael
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
dnes	dnes	k6eAd1	dnes
jich	on	k3xPp3gNnPc2	on
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
jen	jen	k9	jen
okolo	okolo	k7c2	okolo
4000	[number]	k4	4000
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
faktorem	faktor	k1gInSc7	faktor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
národnostní	národnostní	k2eAgFnSc4d1	národnostní
strukturu	struktura	k1gFnSc4	struktura
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
po	po	k7c4	po
připojení	připojení	k1gNnSc4	připojení
Litvy	Litva	k1gFnSc2	Litva
k	k	k7c3	k
SSSR	SSSR	kA	SSSR
masivní	masivní	k2eAgNnSc1d1	masivní
přistěhovalectví	přistěhovalectví	k1gNnSc1	přistěhovalectví
Rusů	Rus	k1gMnPc2	Rus
(	(	kIx(	(
<g/>
přesto	přesto	k8xC	přesto
však	však	k9	však
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
v	v	k7c6	v
Lotyšsku	Lotyšsko	k1gNnSc6	Lotyšsko
a	a	k8xC	a
Estonsku	Estonsko	k1gNnSc6	Estonsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
===	===	k?	===
</s>
</p>
<p>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
je	být	k5eAaImIp3nS	být
37,4	[number]	k4	37,4
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
ženy	žena	k1gFnSc2	žena
40,1	[number]	k4	40,1
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
muži	muž	k1gMnPc1	muž
34,8	[number]	k4	34,8
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
3	[number]	k4	3
483	[number]	k4	483
972	[number]	k4	972
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
1	[number]	k4	1
629	[number]	k4	629
148	[number]	k4	148
(	(	kIx(	(
<g/>
46,76	[number]	k4	46,76
%	%	kIx~	%
<g/>
)	)	kIx)	)
byli	být	k5eAaImAgMnP	být
muži	muž	k1gMnPc1	muž
a	a	k8xC	a
1	[number]	k4	1
854	[number]	k4	854
824	[number]	k4	824
(	(	kIx(	(
<g/>
53,24	[number]	k4	53,24
%	%	kIx~	%
<g/>
)	)	kIx)	)
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
očekávaná	očekávaný	k2eAgFnSc1d1	očekávaná
smrt	smrt	k1gFnSc1	smrt
při	při	k7c6	při
narození	narození	k1gNnSc6	narození
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
65	[number]	k4	65
let	léto	k1gNnPc2	léto
a	a	k8xC	a
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
77	[number]	k4	77
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
největší	veliký	k2eAgInSc1d3	veliký
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
pohlavími	pohlaví	k1gNnPc7	pohlaví
a	a	k8xC	a
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
kojenecká	kojenecký	k2eAgFnSc1d1	kojenecká
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
5,9	[number]	k4	5,9
na	na	k7c4	na
1000	[number]	k4	1000
narozených	narozený	k2eAgFnPc2d1	narozená
<g/>
.	.	kIx.	.
</s>
<s>
Životní	životní	k2eAgFnSc1d1	životní
úroveň	úroveň	k1gFnSc1	úroveň
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
se	se	k3xPyFc4	se
za	za	k7c4	za
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
o	o	k7c4	o
0,3	[number]	k4	0,3
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
než	než	k8xS	než
2	[number]	k4	2
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
žije	žít	k5eAaImIp3nS	žít
pod	pod	k7c7	pod
hranicí	hranice	k1gFnSc7	hranice
chudoby	chudoba	k1gFnSc2	chudoba
a	a	k8xC	a
99,6	[number]	k4	99,6
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
nad	nad	k7c4	nad
15	[number]	k4	15
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
gramotných	gramotný	k2eAgFnPc2d1	gramotná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
Litva	Litva	k1gFnSc1	Litva
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
dramatický	dramatický	k2eAgInSc4d1	dramatický
nárůst	nárůst	k1gInSc4	nárůst
sebevražd	sebevražda	k1gFnPc2	sebevražda
<g/>
,	,	kIx,	,
38,6	[number]	k4	38,6
lidí	člověk	k1gMnPc2	člověk
ze	z	k7c2	z
100	[number]	k4	100
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
sebevražd	sebevražda	k1gFnPc2	sebevražda
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
počet	počet	k1gInSc1	počet
vražd	vražda	k1gFnPc2	vražda
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
byla	být	k5eAaImAgFnS	být
Litva	Litva	k1gFnSc1	Litva
christianizována	christianizovat	k5eAaImNgFnS	christianizovat
až	až	k9	až
jako	jako	k9	jako
poslední	poslední	k2eAgFnSc2d1	poslední
–	–	k?	–
roku	rok	k1gInSc2	rok
1386	[number]	k4	1386
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
(	(	kIx(	(
<g/>
79	[number]	k4	79
%	%	kIx~	%
<g/>
)	)	kIx)	)
obyvatel	obyvatel	k1gMnPc2	obyvatel
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
katolické	katolický	k2eAgFnSc3d1	katolická
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
výrazně	výrazně	k6eAd1	výrazně
přispělo	přispět	k5eAaPmAgNnS	přispět
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
padesáti	padesát	k4xCc2	padesát
let	léto	k1gNnPc2	léto
komunistické	komunistický	k2eAgFnSc2d1	komunistická
a	a	k8xC	a
ruské	ruský	k2eAgFnSc2d1	ruská
nadvlády	nadvláda	k1gFnSc2	nadvláda
představovala	představovat	k5eAaImAgFnS	představovat
katolická	katolický	k2eAgFnSc1d1	katolická
víra	víra	k1gFnSc1	víra
významný	významný	k2eAgInSc4d1	významný
sebeidentifikační	sebeidentifikační	k2eAgInSc4d1	sebeidentifikační
prvek	prvek	k1gInSc4	prvek
litevského	litevský	k2eAgInSc2d1	litevský
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Litva	Litva	k1gFnSc1	Litva
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgInSc7d1	jediný
státem	stát	k1gInSc7	stát
na	na	k7c6	na
území	území	k1gNnSc6	území
bývalého	bývalý	k2eAgInSc2d1	bývalý
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
dominantní	dominantní	k2eAgNnSc4d1	dominantní
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
východnímu	východní	k2eAgNnSc3d1	východní
křesťanství	křesťanství	k1gNnSc3	křesťanství
-	-	kIx~	-
pravoslavné	pravoslavný	k2eAgFnSc3d1	pravoslavná
církvi	církev	k1gFnSc3	církev
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
zejména	zejména	k9	zejména
ruské	ruský	k2eAgNnSc1d1	ruské
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
(	(	kIx(	(
<g/>
4,07	[number]	k4	4,07
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protestantismus	protestantismus	k1gInSc1	protestantismus
(	(	kIx(	(
<g/>
1,9	[number]	k4	1,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
počtu	počet	k1gInSc2	počet
věřících	věřící	k1gFnPc2	věřící
jen	jen	k9	jen
okrajový	okrajový	k2eAgInSc4d1	okrajový
význam	význam	k1gInSc4	význam
a	a	k8xC	a
početně	početně	k6eAd1	početně
nevýznamné	významný	k2eNgFnPc1d1	nevýznamná
komunity	komunita	k1gFnPc1	komunita
představují	představovat	k5eAaImIp3nP	představovat
i	i	k9	i
Židé	Žid	k1gMnPc1	Žid
a	a	k8xC	a
muslimové	muslim	k1gMnPc1	muslim
<g/>
.	.	kIx.	.
</s>
<s>
Relativně	relativně	k6eAd1	relativně
malou	malý	k2eAgFnSc4d1	malá
skupinu	skupina	k1gFnSc4	skupina
tvoří	tvořit	k5eAaImIp3nP	tvořit
ateisté	ateista	k1gMnPc1	ateista
(	(	kIx(	(
<g/>
14,86	[number]	k4	14,86
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Města	město	k1gNnSc2	město
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
žilo	žít	k5eAaImAgNnS	žít
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
103	[number]	k4	103
litevských	litevský	k2eAgFnPc2d1	Litevská
městech	město	k1gNnPc6	město
dvě	dva	k4xCgFnPc4	dva
třetiny	třetina	k1gFnPc4	třetina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
(	(	kIx(	(
<g/>
66,7	[number]	k4	66,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Vilniusu	Vilnius	k1gInSc2	Vilnius
jsou	být	k5eAaImIp3nP	být
dalšími	další	k2eAgFnPc7d1	další
významnými	významný	k2eAgFnPc7d1	významná
litevskými	litevský	k2eAgFnPc7d1	Litevská
městy	město	k1gNnPc7	město
přístav	přístav	k1gInSc1	přístav
Klaipė	Klaipė	k1gFnSc3	Klaipė
<g/>
,	,	kIx,	,
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
řek	řeka	k1gFnPc2	řeka
Němen	Němen	k1gInSc4	Němen
a	a	k8xC	a
Neris	Neris	k1gInSc4	Neris
ležící	ležící	k2eAgInSc4d1	ležící
Kaunas	Kaunas	k1gInSc4	Kaunas
a	a	k8xC	a
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
Šiauliai	Šiaulia	k1gFnSc2	Šiaulia
a	a	k8xC	a
Panevė	Panevė	k1gFnSc2	Panevė
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
10	[number]	k4	10
největších	veliký	k2eAgNnPc2d3	veliký
litevských	litevský	k2eAgNnPc2d1	litevské
měst	město	k1gNnPc2	město
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
nejznámějším	známý	k2eAgMnSc7d3	nejznámější
litevským	litevský	k2eAgMnSc7d1	litevský
umělcem	umělec	k1gMnSc7	umělec
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
je	být	k5eAaImIp3nS	být
Mikalojus	Mikalojus	k1gInSc1	Mikalojus
Konstantinas	Konstantinasa	k1gFnPc2	Konstantinasa
Čiurlionis	Čiurlionis	k1gFnSc2	Čiurlionis
<g/>
,	,	kIx,	,
všestranný	všestranný	k2eAgMnSc1d1	všestranný
tvůrce	tvůrce	k1gMnSc1	tvůrce
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
maloval	malovat	k5eAaImAgMnS	malovat
<g/>
,	,	kIx,	,
komponoval	komponovat	k5eAaImAgMnS	komponovat
hudbu	hudba	k1gFnSc4	hudba
i	i	k9	i
psal	psát	k5eAaImAgInS	psát
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgMnSc7d3	nejznámější
litevským	litevský	k2eAgMnSc7d1	litevský
sochařem	sochař	k1gMnSc7	sochař
byl	být	k5eAaImAgMnS	být
kubista	kubista	k1gMnSc1	kubista
Jacques	Jacques	k1gMnSc1	Jacques
Lipchitz	Lipchitz	k1gMnSc1	Lipchitz
<g/>
,	,	kIx,	,
hudebním	hudební	k2eAgMnSc7d1	hudební
skladatelem	skladatel	k1gMnSc7	skladatel
Leopold	Leopolda	k1gFnPc2	Leopolda
Godowsky	Godowska	k1gFnPc4	Godowska
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vilniusu	Vilnius	k1gInSc6	Vilnius
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
i	i	k9	i
skladatel	skladatel	k1gMnSc1	skladatel
César	César	k1gMnSc1	César
Antonovič	Antonovič	k1gMnSc1	Antonovič
Kjui	Kjui	k1gNnPc2	Kjui
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
rolí	role	k1gFnSc7	role
v	v	k7c6	v
Tarkovského	Tarkovský	k2eAgInSc2d1	Tarkovský
snímku	snímek	k1gInSc2	snímek
Solaris	Solaris	k1gFnSc2	Solaris
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
herec	herec	k1gMnSc1	herec
Donatas	Donatas	k1gMnSc1	Donatas
Banionis	Banionis	k1gFnSc1	Banionis
<g/>
.	.	kIx.	.
</s>
<s>
George	Georg	k1gMnSc4	Georg
Maciunas	Maciunas	k1gInSc1	Maciunas
založil	založit	k5eAaPmAgInS	založit
v	v	k7c6	v
USA	USA	kA	USA
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
vlivnou	vlivný	k2eAgFnSc4d1	vlivná
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
komunitu	komunita	k1gFnSc4	komunita
Fluxus	Fluxus	k1gMnSc1	Fluxus
<g/>
.	.	kIx.	.
</s>
<s>
Litevské	litevský	k2eAgNnSc4d1	litevské
občanství	občanství	k1gNnSc4	občanství
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
primabalerína	primabalerína	k1gFnSc1	primabalerína
Maja	Maja	k1gFnSc1	Maja
Plisecká	Plisecká	k1gFnSc1	Plisecká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
autora	autor	k1gMnSc2	autor
první	první	k4xOgFnSc2	první
litevské	litevský	k2eAgFnSc2d1	Litevská
básně	báseň	k1gFnSc2	báseň
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
Kristijonas	Kristijonas	k1gInSc1	Kristijonas
Donelaitis	Donelaitis	k1gFnSc1	Donelaitis
<g/>
.	.	kIx.	.
</s>
<s>
Slova	slovo	k1gNnPc4	slovo
i	i	k8xC	i
hudbu	hudba	k1gFnSc4	hudba
litevské	litevský	k2eAgFnSc2d1	Litevská
národní	národní	k2eAgFnSc2d1	národní
hymny	hymna	k1gFnSc2	hymna
napsal	napsat	k5eAaBmAgMnS	napsat
básník	básník	k1gMnSc1	básník
Vincas	Vincas	k1gMnSc1	Vincas
Kudirka	Kudirka	k1gMnSc1	Kudirka
<g/>
.	.	kIx.	.
</s>
<s>
Výraznou	výrazný	k2eAgFnSc7d1	výrazná
obrozeneckou	obrozenecký	k2eAgFnSc7d1	obrozenecká
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
spisovatel	spisovatel	k1gMnSc1	spisovatel
Simonas	Simonas	k1gMnSc1	Simonas
Daukantas	Daukantas	k1gMnSc1	Daukantas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sovětské	sovětský	k2eAgFnSc6d1	sovětská
éře	éra	k1gFnSc6	éra
litevskou	litevský	k2eAgFnSc4d1	Litevská
poezii	poezie	k1gFnSc4	poezie
reprezentoval	reprezentovat	k5eAaImAgMnS	reprezentovat
například	například	k6eAd1	například
Justinas	Justinas	k1gMnSc1	Justinas
Marcinkevičius	Marcinkevičius	k1gMnSc1	Marcinkevičius
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Jurga	Jurga	k1gFnSc1	Jurga
Ivanauskaitė	Ivanauskaitė	k1gFnSc1	Ivanauskaitė
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
známou	známý	k2eAgFnSc7d1	známá
zpívající	zpívající	k2eAgFnSc7d1	zpívající
básnířkou	básnířka	k1gFnSc7	básnířka
Alina	Alin	k1gInSc2	Alin
Orlova	Orlův	k2eAgInSc2d1	Orlův
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
i	i	k9	i
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
Czesław	Czesław	k1gMnSc1	Czesław
Miłosz	Miłosz	k1gMnSc1	Miłosz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Věda	věda	k1gFnSc1	věda
==	==	k?	==
</s>
</p>
<p>
<s>
Litevský	litevský	k2eAgMnSc1d1	litevský
rodák	rodák	k1gMnSc1	rodák
Aaron	Aaron	k1gMnSc1	Aaron
Klug	Klug	k1gMnSc1	Klug
získal	získat	k5eAaPmAgMnS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vilniusu	Vilnius	k1gInSc6	Vilnius
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
také	také	k9	také
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
Andrew	Andrew	k1gFnSc2	Andrew
Schally	Schalla	k1gFnSc2	Schalla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kaunasu	Kaunas	k1gInSc6	Kaunas
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
proslulý	proslulý	k2eAgMnSc1d1	proslulý
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
geometr	geometr	k1gMnSc1	geometr
Hermann	Hermann	k1gMnSc1	Hermann
Minkowski	Minkowski	k1gNnPc2	Minkowski
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumem	výzkum	k1gInSc7	výzkum
orangutanů	orangutan	k1gMnPc2	orangutan
proslula	proslout	k5eAaPmAgFnS	proslout
etoložka	etoložka	k1gFnSc1	etoložka
Birutė	Birutė	k1gFnSc1	Birutė
Galdikasová	Galdikasová	k1gFnSc1	Galdikasová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Kaunasu	Kaunas	k1gInSc6	Kaunas
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
anarchistická	anarchistický	k2eAgFnSc1d1	anarchistická
politická	politický	k2eAgFnSc1d1	politická
filozofka	filozofka	k1gFnSc1	filozofka
Emma	Emma	k1gFnSc1	Emma
Goldmanová	Goldmanová	k1gFnSc1	Goldmanová
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Vilniusu	Vilnius	k1gInSc6	Vilnius
její	její	k3xOp3gInSc4	její
milenec	milenec	k1gMnSc1	milenec
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
významný	významný	k2eAgMnSc1d1	významný
anarchista	anarchista	k1gMnSc1	anarchista
Alexandr	Alexandr	k1gMnSc1	Alexandr
Berkman	Berkman	k1gMnSc1	Berkman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kaunasu	Kaunas	k1gInSc6	Kaunas
se	se	k3xPyFc4	se
též	též	k9	též
narodil	narodit	k5eAaPmAgMnS	narodit
existencialistický	existencialistický	k2eAgMnSc1d1	existencialistický
filozof	filozof	k1gMnSc1	filozof
Emmanuel	Emmanuel	k1gMnSc1	Emmanuel
Lévinas	Lévinas	k1gMnSc1	Lévinas
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
klíčových	klíčový	k2eAgFnPc2d1	klíčová
postav	postava	k1gFnPc2	postava
sémiotiky	sémiotika	k1gFnSc2	sémiotika
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
Algirdas	Algirdas	k1gMnSc1	Algirdas
Julien	Julina	k1gFnPc2	Julina
Greimas	Greimas	k1gMnSc1	Greimas
<g/>
.	.	kIx.	.
</s>
<s>
Archeoložka	archeoložka	k1gFnSc1	archeoložka
Marija	Marija	k1gFnSc1	Marija
Gimbutasová	Gimbutasová	k1gFnSc1	Gimbutasová
proslula	proslout	k5eAaPmAgFnS	proslout
svými	svůj	k3xOyFgFnPc7	svůj
jungiánsky	jungiánsky	k6eAd1	jungiánsky
inspirovanými	inspirovaný	k2eAgFnPc7d1	inspirovaná
tezemi	teze	k1gFnPc7	teze
o	o	k7c6	o
uctívání	uctívání	k1gNnSc6	uctívání
ženských	ženský	k2eAgNnPc2d1	ženské
božstev	božstvo	k1gNnPc2	božstvo
v	v	k7c6	v
předindoevropských	předindoevropský	k2eAgFnPc6d1	předindoevropský
komunitách	komunita	k1gFnPc6	komunita
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vilniusu	Vilnius	k1gInSc6	Vilnius
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
též	též	k9	též
judaistický	judaistický	k2eAgMnSc1d1	judaistický
učenec	učenec	k1gMnSc1	učenec
Ga	Ga	k1gMnSc1	Ga
<g/>
'	'	kIx"	'
<g/>
on	on	k3xPp3gMnSc1	on
z	z	k7c2	z
Vilna	Viln	k1gInSc2	Viln
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
éře	éra	k1gFnSc6	éra
samostatnosti	samostatnost	k1gFnSc2	samostatnost
Litevci	Litevec	k1gMnPc1	Litevec
získali	získat	k5eAaPmAgMnP	získat
šest	šest	k4xCc1	šest
zlatých	zlatý	k2eAgFnPc2d1	zlatá
olympijských	olympijský	k2eAgFnPc2d1	olympijská
medailí	medaile	k1gFnPc2	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
olympiádách	olympiáda	k1gFnPc6	olympiáda
triumfoval	triumfovat	k5eAaBmAgMnS	triumfovat
diskař	diskař	k1gMnSc1	diskař
Virgilijus	Virgilijus	k1gMnSc1	Virgilijus
Alekna	Alekna	k1gFnSc1	Alekna
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k9	už
ta	ten	k3xDgFnSc1	ten
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc1	první
zlatá	zlatý	k2eAgFnSc1d1	zlatá
olympijská	olympijský	k2eAgFnSc1d1	olympijská
medaile	medaile	k1gFnSc1	medaile
pro	pro	k7c4	pro
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
Litvu	Litva	k1gFnSc4	Litva
byla	být	k5eAaImAgFnS	být
diskařská	diskařský	k2eAgFnSc1d1	Diskařská
a	a	k8xC	a
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
ji	on	k3xPp3gFnSc4	on
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
získal	získat	k5eAaPmAgMnS	získat
Romas	Romas	k1gMnSc1	Romas
Ubartas	Ubartas	k1gMnSc1	Ubartas
<g/>
.	.	kIx.	.
</s>
<s>
Zlato	zlato	k1gNnSc1	zlato
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
100	[number]	k4	100
m	m	kA	m
prsa	prso	k1gNnSc2	prso
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
plavkyně	plavkyně	k1gFnSc1	plavkyně
Rū	Rū	k1gFnSc1	Rū
Meilutytė	Meilutytė	k1gFnSc1	Meilutytė
<g/>
,	,	kIx,	,
získaly	získat	k5eAaPmAgInP	získat
ho	on	k3xPp3gNnSc4	on
i	i	k9	i
dvě	dva	k4xCgFnPc1	dva
další	další	k2eAgFnPc1d1	další
ženy	žena	k1gFnPc1	žena
<g/>
:	:	kIx,	:
pětibojařka	pětibojařka	k1gFnSc1	pětibojařka
Laura	Laura	k1gFnSc1	Laura
Asadauskaitė	Asadauskaitė	k1gFnSc1	Asadauskaitė
a	a	k8xC	a
střelkyně	střelkyně	k1gFnSc1	střelkyně
Daina	Daina	k1gFnSc1	Daina
Gudzinevičiū	Gudzinevičiū	k1gFnSc1	Gudzinevičiū
<g/>
.	.	kIx.	.
</s>
<s>
Litevští	litevský	k2eAgMnPc1d1	litevský
sportovci	sportovec	k1gMnPc1	sportovec
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
prosazovali	prosazovat	k5eAaImAgMnP	prosazovat
již	již	k6eAd1	již
i	i	k8xC	i
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Individuální	individuální	k2eAgFnPc1d1	individuální
zlaté	zlatá	k1gFnPc1	zlatá
si	se	k3xPyFc3	se
z	z	k7c2	z
olympiád	olympiáda	k1gFnPc2	olympiáda
přivezli	přivézt	k5eAaPmAgMnP	přivézt
plavci	plavec	k1gMnPc1	plavec
Robertas	Robertasa	k1gFnPc2	Robertasa
Žulpa	Žulpa	k1gFnSc1	Žulpa
a	a	k8xC	a
Lina	Lina	k1gFnSc1	Lina
Kačiušytė	Kačiušytė	k1gFnSc1	Kačiušytė
<g/>
,	,	kIx,	,
boxer	boxer	k1gMnSc1	boxer
Danas	Danas	k1gMnSc1	Danas
Pozniakas	Pozniakas	k1gMnSc1	Pozniakas
<g/>
,	,	kIx,	,
cyklista	cyklista	k1gMnSc1	cyklista
Gintautas	Gintautas	k1gMnSc1	Gintautas
Umaras	Umaras	k1gMnSc1	Umaras
nebo	nebo	k8xC	nebo
běžkyně	běžkyně	k1gFnSc1	běžkyně
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
Vida	Vida	k?	Vida
Vencienė	Vencienė	k1gFnSc2	Vencienė
<g/>
.	.	kIx.	.
</s>
<s>
Podíleli	podílet	k5eAaImAgMnP	podílet
se	se	k3xPyFc4	se
také	také	k9	také
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
medailí	medaile	k1gFnPc2	medaile
kolektivních	kolektivní	k2eAgFnPc2d1	kolektivní
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
basketbalu	basketbal	k1gInSc6	basketbal
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
k	k	k7c3	k
nejslavnějším	slavný	k2eAgMnPc3d3	nejslavnější
medailistům	medailista	k1gMnPc3	medailista
patří	patřit	k5eAaImIp3nP	patřit
Arvydas	Arvydas	k1gMnSc1	Arvydas
Sabonis	Sabonis	k1gFnSc2	Sabonis
(	(	kIx(	(
<g/>
šestinásobný	šestinásobný	k2eAgMnSc1d1	šestinásobný
vítěz	vítěz	k1gMnSc1	vítěz
ankety	anketa	k1gFnSc2	anketa
o	o	k7c4	o
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
basketbalistu	basketbalista	k1gMnSc4	basketbalista
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Modestas	Modestas	k1gMnSc1	Modestas
Paulauskas	Paulauskas	k1gMnSc1	Paulauskas
<g/>
,	,	kIx,	,
Šarū	Šarū	k1gMnSc1	Šarū
Marčiulionis	Marčiulionis	k1gFnSc2	Marčiulionis
či	či	k8xC	či
Rimas	Rimas	k1gMnSc1	Rimas
Kurtinaitis	Kurtinaitis	k1gFnSc1	Kurtinaitis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
i	i	k9	i
úspěšným	úspěšný	k2eAgMnSc7d1	úspěšný
trenérem	trenér	k1gMnSc7	trenér
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
NBA	NBA	kA	NBA
se	se	k3xPyFc4	se
prosadili	prosadit	k5eAaPmAgMnP	prosadit
i	i	k9	i
Zydrunas	Zydrunas	k1gMnSc1	Zydrunas
Ilgauskas	Ilgauskas	k1gMnSc1	Ilgauskas
či	či	k8xC	či
Jonas	Jonas	k1gMnSc1	Jonas
Valančiū	Valančiū	k1gMnSc1	Valančiū
<g/>
.	.	kIx.	.
</s>
<s>
Basketbal	basketbal	k1gInSc1	basketbal
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
mimořádnou	mimořádný	k2eAgFnSc4d1	mimořádná
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
Litevská	litevský	k2eAgFnSc1d1	Litevská
mužská	mužský	k2eAgFnSc1d1	mužská
basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
reprezentace	reprezentace	k1gFnSc1	reprezentace
získala	získat	k5eAaPmAgFnS	získat
dva	dva	k4xCgInPc4	dva
tituly	titul	k1gInPc4	titul
mistrů	mistr	k1gMnPc2	mistr
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
a	a	k8xC	a
i	i	k9	i
po	po	k7c6	po
obnovení	obnovení	k1gNnSc6	obnovení
samostatnosti	samostatnost	k1gFnSc2	samostatnost
dokázala	dokázat	k5eAaPmAgFnS	dokázat
tento	tento	k3xDgInSc4	tento
úspěch	úspěch	k1gInSc4	úspěch
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
zopakovat	zopakovat	k5eAaPmF	zopakovat
<g/>
.	.	kIx.	.
</s>
<s>
Stejného	stejný	k2eAgInSc2d1	stejný
úspěchu	úspěch	k1gInSc2	úspěch
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
ženy	žena	k1gFnSc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
reprezentace	reprezentace	k1gFnSc1	reprezentace
tolik	tolik	k6eAd1	tolik
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Edgaras	Edgaras	k1gMnSc1	Edgaras
Jankauskas	Jankauskas	k1gMnSc1	Jankauskas
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Ligu	liga	k1gFnSc4	liga
mistrů	mistr	k1gMnPc2	mistr
s	s	k7c7	s
FC	FC	kA	FC
Porto	porto	k1gNnSc1	porto
a	a	k8xC	a
Deividas	Deividas	k1gMnSc1	Deividas
Šemberas	Šemberas	k1gMnSc1	Šemberas
Evropskou	evropský	k2eAgFnSc4d1	Evropská
ligu	liga	k1gFnSc4	liga
s	s	k7c7	s
CSKA	CSKA	kA	CSKA
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Rasa	rasa	k1gFnSc1	rasa
Polikevičiū	Polikevičiū	k1gFnSc1	Polikevičiū
<g/>
,	,	kIx,	,
Edita	Edita	k1gFnSc1	Edita
Pučinskaitė	Pučinskaitė	k1gFnSc1	Pučinskaitė
a	a	k8xC	a
Diana	Diana	k1gFnSc1	Diana
Žiliū	Žiliū	k1gFnPc2	Žiliū
jsou	být	k5eAaImIp3nP	být
mystryněmi	mystryně	k1gFnPc7	mystryně
světa	svět	k1gInSc2	svět
v	v	k7c6	v
cyklistice	cyklistika	k1gFnSc6	cyklistika
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšnou	úspěšný	k2eAgFnSc7d1	úspěšná
šachistkou	šachistka	k1gFnSc7	šachistka
je	být	k5eAaImIp3nS	být
Salomė	Salomė	k1gMnSc1	Salomė
Zaksaitė	Zaksaitė	k1gMnSc1	Zaksaitė
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Litva	Litva	k1gFnSc1	Litva
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BERESNEVIČIŪ	BERESNEVIČIŪ	k?	BERESNEVIČIŪ
<g/>
,	,	kIx,	,
Halina	Halina	k1gFnSc1	Halina
<g/>
.	.	kIx.	.
</s>
<s>
Litva	Litva	k1gFnSc1	Litva
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
300	[number]	k4	300
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠVEC	Švec	k1gMnSc1	Švec
<g/>
,	,	kIx,	,
Luboš	Luboš	k1gMnSc1	Luboš
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Pobaltských	pobaltský	k2eAgFnPc2d1	pobaltská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
154	[number]	k4	154
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Litevská	litevský	k2eAgFnSc1d1	Litevská
armáda	armáda	k1gFnSc1	armáda
</s>
</p>
<p>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Litvy	Litva	k1gFnSc2	Litva
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
měst	město	k1gNnPc2	město
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
</s>
</p>
<p>
<s>
Kraje	kraj	k1gInPc1	kraj
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
řek	řeka	k1gFnPc2	řeka
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
jezer	jezero	k1gNnPc2	jezero
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
</s>
</p>
<p>
<s>
Státní	státní	k2eAgFnSc1d1	státní
hranice	hranice	k1gFnSc1	hranice
Litvy	Litva	k1gFnSc2	Litva
</s>
</p>
<p>
<s>
Národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
</s>
</p>
<p>
<s>
Litevská	litevský	k2eAgFnSc1d1	Litevská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Delta	delta	k1gFnSc1	delta
Němenu	Němen	k1gInSc2	Němen
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Litva	Litva	k1gFnSc1	Litva
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Litva	Litva	k1gFnSc1	Litva
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Průvodce	průvodce	k1gMnSc1	průvodce
Litva	Litva	k1gFnSc1	Litva
ve	v	k7c6	v
Wikicestách	Wikicesta	k1gFnPc6	Wikicesta
</s>
</p>
<p>
<s>
Litva	Litva	k1gFnSc1	Litva
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Litva	Litva	k1gFnSc1	Litva
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Litva	Litva	k1gFnSc1	Litva
ve	v	k7c6	v
Vlastenském	vlastenský	k2eAgInSc6d1	vlastenský
slovníku	slovník	k1gInSc6	slovník
historickém	historický	k2eAgInSc6d1	historický
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
portál	portál	k1gInSc1	portál
litevského	litevský	k2eAgInSc2d1	litevský
internetu	internet	k1gInSc2	internet
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
Litvě	Litva	k1gFnSc6	Litva
na	na	k7c6	na
serveru	server	k1gInSc6	server
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
turistický	turistický	k2eAgInSc1d1	turistický
server	server	k1gInSc1	server
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
Velvyslanectví	velvyslanectví	k1gNnSc2	velvyslanectví
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
ve	v	k7c6	v
Vilniusu	Vilnius	k1gInSc6	Vilnius
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
Velvyslanectví	velvyslanectví	k1gNnSc2	velvyslanectví
Litevské	litevský	k2eAgFnSc2d1	Litevská
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
litevsky	litevsky	k6eAd1	litevsky
a	a	k8xC	a
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Plány	plán	k1gInPc1	plán
na	na	k7c4	na
novou	nový	k2eAgFnSc4d1	nová
litevskou	litevský	k2eAgFnSc4d1	Litevská
jadernou	jaderný	k2eAgFnSc4d1	jaderná
elektrárnu	elektrárna	k1gFnSc4	elektrárna
prozatím	prozatím	k6eAd1	prozatím
narážejí	narážet	k5eAaImIp3nP	narážet
na	na	k7c4	na
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
problémy	problém	k1gInPc4	problém
státu	stát	k1gInSc2	stát
</s>
</p>
<p>
<s>
Lithuania	Lithuanium	k1gNnPc1	Lithuanium
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lithuania	Lithuanium	k1gNnPc1	Lithuanium
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Lithuania	Lithuanium	k1gNnSc2	Lithuanium
Country	country	k2eAgInSc4d1	country
Report	report	k1gInSc4	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
European	European	k1gInSc1	European
and	and	k?	and
Eurasian	Eurasian	k1gInSc1	Eurasian
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Not	k1gInSc2	Not
<g/>
:	:	kIx,	:
Lithuania	Lithuanium	k1gNnSc2	Lithuanium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-03-01	[number]	k4	2011-03-01
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Lithuania	Lithuanium	k1gNnSc2	Lithuanium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
ve	v	k7c6	v
Vilniusu	Vilnius	k1gInSc6	Vilnius
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Litva	Litva	k1gFnSc1	Litva
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011-04-01	[number]	k4	2011-04-01
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BATER	BATER	kA	BATER
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
H	H	kA	H
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Lithuania	Lithuanium	k1gNnPc1	Lithuanium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
