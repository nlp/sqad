<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Těšínského	Těšínského	k2eAgNnSc2d1	Těšínského
knížectví	knížectví	k1gNnSc2	knížectví
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
díky	díky	k7c3	díky
iniciativě	iniciativa	k1gFnSc3	iniciativa
regionálních	regionální	k2eAgMnPc2d1	regionální
nadšenců	nadšenec	k1gMnPc2	nadšenec
historie	historie	k1gFnSc2	historie
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
významným	významný	k2eAgMnSc7d1	významný
polským	polský	k2eAgMnSc7d1	polský
heraldikem	heraldik	k1gMnSc7	heraldik
a	a	k8xC	a
vexilologem	vexilolog	k1gMnSc7	vexilolog
Alfredem	Alfred	k1gMnSc7	Alfred
Znamierowskim	Znamierowskima	k1gFnPc2	Znamierowskima
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
znak	znak	k1gInSc4	znak
a	a	k8xC	a
historické	historický	k2eAgInPc4d1	historický
prapory	prapor	k1gInPc4	prapor
Těšínského	Těšínského	k2eAgNnSc2d1	Těšínského
knížectví	knížectví	k1gNnSc2	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
Novodobá	novodobý	k2eAgFnSc1d1	novodobá
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
úctou	úcta	k1gFnSc7	úcta
k	k	k7c3	k
historii	historie	k1gFnSc3	historie
a	a	k8xC	a
tradici	tradice	k1gFnSc3	tradice
Těšínského	Těšínského	k2eAgNnSc2d1	Těšínského
knížectví	knížectví	k1gNnSc2	knížectví
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
součásti	součást	k1gFnPc1	součást
odlišné	odlišný	k2eAgFnSc2d1	odlišná
identity	identita	k1gFnSc2	identita
obyvatel	obyvatel	k1gMnPc2	obyvatel
Těšínska	Těšínsko	k1gNnSc2	Těšínsko
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
Dolnímu	dolní	k2eAgNnSc3d1	dolní
a	a	k8xC	a
Hornímu	horní	k2eAgNnSc3d1	horní
Slezsku	Slezsko	k1gNnSc3	Slezsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
těšínské	těšínský	k2eAgFnSc6d1	těšínská
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
praporu	prapor	k1gInSc2	prapor
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
vázány	vázat	k5eAaImNgFnP	vázat
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
knížete	kníže	k1gMnSc2	kníže
Přemysla	Přemysl	k1gMnSc2	Přemysl
I.	I.	kA	I.
Nošáka	Nošák	k1gMnSc2	Nošák
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1355	[number]	k4	1355
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
politickém	politický	k2eAgInSc6d1	politický
chodu	chod	k1gInSc6	chod
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
politiků	politik	k1gMnPc2	politik
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
Karla	Karel	k1gMnSc2	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
a	a	k8xC	a
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
prapory	prapor	k1gInPc4	prapor
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
nedochovaly	dochovat	k5eNaPmAgFnP	dochovat
<g/>
.	.	kIx.	.
<g/>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
zachovalý	zachovalý	k2eAgInSc1d1	zachovalý
prapor	prapor	k1gInSc1	prapor
Těšínského	Těšínského	k2eAgNnSc2d1	Těšínského
knížectví	knížectví	k1gNnSc2	knížectví
byl	být	k5eAaImAgInS	být
zhotoven	zhotovit	k5eAaPmNgInS	zhotovit
na	na	k7c4	na
rozkaz	rozkaz	k1gInSc4	rozkaz
těšínského	těšínský	k2eAgMnSc2d1	těšínský
knížete	kníže	k1gMnSc2	kníže
Adama	Adam	k1gMnSc2	Adam
Václava	Václav	k1gMnSc2	Václav
roku	rok	k1gInSc2	rok
1605	[number]	k4	1605
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
potom	potom	k6eAd1	potom
co	co	k3yInSc1	co
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
velitelem	velitel	k1gMnSc7	velitel
slezského	slezský	k2eAgNnSc2d1	Slezské
vojska	vojsko	k1gNnSc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Dochovaný	dochovaný	k2eAgMnSc1d1	dochovaný
ve	v	k7c6	v
sbírkách	sbírka	k1gFnPc6	sbírka
Muzeum	muzeum	k1gNnSc1	muzeum
Śląska	Śląsko	k1gNnSc2	Śląsko
Cieszyńskiego	Cieszyńskiego	k1gNnSc4	Cieszyńskiego
(	(	kIx(	(
<g/>
Muzea	muzeum	k1gNnSc2	muzeum
Těšínského	Těšínského	k2eAgNnSc2d1	Těšínského
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prapor	prapor	k1gInSc1	prapor
má	mít	k5eAaImIp3nS	mít
rozměry	rozměr	k1gInPc4	rozměr
160	[number]	k4	160
<g/>
x	x	k?	x
<g/>
240	[number]	k4	240
cm	cm	kA	cm
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vyhotoven	vyhotovit	k5eAaPmNgInS	vyhotovit
z	z	k7c2	z
modrého	modrý	k2eAgInSc2d1	modrý
saténu	satén	k1gInSc2	satén
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čelní	čelní	k2eAgFnSc6d1	čelní
části	část	k1gFnSc6	část
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zlatá	zlatý	k2eAgFnSc1d1	zlatá
orlice	orlice	k1gFnSc1	orlice
Těšínského	Těšínského	k2eAgNnSc2d1	Těšínského
knížectví	knížectví	k1gNnSc2	knížectví
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
co	co	k9	co
volná	volný	k2eAgFnSc1d1	volná
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
zakončena	zakončit	k5eAaPmNgFnS	zakončit
dvěma	dva	k4xCgInPc7	dva
ostrými	ostrý	k2eAgInPc7d1	ostrý
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Knížecí	knížecí	k2eAgInPc1d1	knížecí
prapory	prapor	k1gInPc1	prapor
se	s	k7c7	s
zlatou	zlatá	k1gFnSc7	zlatá
korunovanou	korunovaný	k2eAgFnSc4d1	korunovaná
orlici	orlice	k1gFnSc4	orlice
v	v	k7c6	v
modrém	modrý	k2eAgNnSc6d1	modré
poli	pole	k1gNnSc6	pole
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgFnP	používat
až	až	k9	až
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
poslední	poslední	k2eAgFnSc2d1	poslední
Piastovny	Piastovna	k1gFnSc2	Piastovna
<g/>
,	,	kIx,	,
Alžběty	Alžběta	k1gFnSc2	Alžběta
Lukrécie	Lukrécie	k1gFnSc2	Lukrécie
Těšínské	Těšínská	k1gFnSc2	Těšínská
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1742	[number]	k4	1742
byla	být	k5eAaImAgFnS	být
oficiální	oficiální	k2eAgFnSc7d1	oficiální
vlajkou	vlajka	k1gFnSc7	vlajka
Těšínska	Těšínsko	k1gNnSc2	Těšínsko
vlajka	vlajka	k1gFnSc1	vlajka
Rakouského	rakouský	k2eAgNnSc2d1	rakouské
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
součásti	součást	k1gFnPc4	součást
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
i	i	k9	i
Těšínsko	Těšínsko	k1gNnSc1	Těšínsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Barvy	barva	k1gFnSc2	barva
vlajky	vlajka	k1gFnSc2	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
Barvy	barva	k1gFnPc1	barva
navazují	navazovat	k5eAaImIp3nP	navazovat
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
dochované	dochovaný	k2eAgInPc4d1	dochovaný
ikonografické	ikonografický	k2eAgInPc4d1	ikonografický
prameny	pramen	k1gInPc4	pramen
rodového	rodový	k2eAgInSc2d1	rodový
erbu	erb	k1gInSc2	erb
těšínských	těšínský	k2eAgNnPc2d1	Těšínské
knížat	kníže	k1gNnPc2	kníže
a	a	k8xC	a
praporu	prapor	k1gInSc2	prapor
Těšínského	Těšínského	k2eAgNnSc2d1	Těšínského
knížectví	knížectví	k1gNnSc2	knížectví
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
znakem	znak	k1gInSc7	znak
těšínských	těšínský	k2eAgMnPc2d1	těšínský
knížat	kníže	k1gMnPc2wR	kníže
byla	být	k5eAaImAgFnS	být
zlatá	zlatý	k2eAgFnSc1d1	zlatá
orlice	orlice	k1gFnSc1	orlice
v	v	k7c6	v
modrém	modrý	k2eAgNnSc6d1	modré
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
otočená	otočený	k2eAgNnPc4d1	otočené
doleva	doleva	k6eAd1	doleva
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
orlice	orlice	k1gFnSc2	orlice
byla	být	k5eAaImAgFnS	být
korunována	korunovat	k5eAaBmNgFnS	korunovat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
korunovaná	korunovaný	k2eAgFnSc1d1	korunovaná
orlice	orlice	k1gFnSc1	orlice
se	se	k3xPyFc4	se
objevovala	objevovat	k5eAaImAgFnS	objevovat
už	už	k6eAd1	už
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
zámecké	zámecký	k2eAgFnSc6d1	zámecká
dlažbě	dlažba	k1gFnSc6	dlažba
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
Přemysla	Přemysl	k1gMnSc2	Přemysl
I	I	kA	I
Nošáka	Nošák	k1gMnSc2	Nošák
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
zahraniční	zahraniční	k2eAgInPc1d1	zahraniční
prameny	pramen	k1gInPc1	pramen
např.	např.	kA	např.
švédský	švédský	k2eAgMnSc1d1	švédský
"	"	kIx"	"
<g/>
Codex	Codex	k1gInSc1	Codex
Bergshammar	Bergshammara	k1gFnPc2	Bergshammara
<g/>
"	"	kIx"	"
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1430	[number]	k4	1430
–	–	k?	–
1436	[number]	k4	1436
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
těšínskou	těšínský	k2eAgFnSc4d1	těšínská
orlici	orlice	k1gFnSc4	orlice
s	s	k7c7	s
červeným	červený	k2eAgInSc7d1	červený
zobákem	zobák	k1gInSc7	zobák
<g/>
,	,	kIx,	,
jazykem	jazyk	k1gInSc7	jazyk
a	a	k8xC	a
drápy	dráp	k1gInPc7	dráp
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgInPc1d2	pozdější
prameny	pramen	k1gInPc1	pramen
jako	jako	k8xC	jako
např.	např.	kA	např.
Slezský	slezský	k2eAgMnSc1d1	slezský
Erbovník	erbovník	k1gMnSc1	erbovník
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1577	[number]	k4	1577
–	–	k?	–
1578	[number]	k4	1578
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
orlici	orlice	k1gFnSc4	orlice
s	s	k7c7	s
pouze	pouze	k6eAd1	pouze
červeným	červený	k2eAgInSc7d1	červený
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
vyobrazení	vyobrazení	k1gNnSc4	vyobrazení
a	a	k8xC	a
barvy	barva	k1gFnPc4	barva
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
také	také	k9	také
Siebmacherův	Siebmacherův	k2eAgMnSc1d1	Siebmacherův
Erbovník	erbovník	k1gMnSc1	erbovník
a	a	k8xC	a
pozdější	pozdní	k2eAgInSc1d2	pozdější
popis	popis	k1gInSc1	popis
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Spenera	Spener	k1gMnSc2	Spener
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
těmto	tento	k3xDgFnPc3	tento
barvám	barva	k1gFnPc3	barva
navazuje	navazovat	k5eAaImIp3nS	navazovat
také	také	k9	také
současná	současný	k2eAgFnSc1d1	současná
vlajka	vlajka	k1gFnSc1	vlajka
Těšínského	Těšínského	k2eAgNnSc2d1	Těšínského
knížectví	knížectví	k1gNnSc2	knížectví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Orlice	Orlice	k1gFnSc2	Orlice
Těšínského	Těšínského	k2eAgNnSc2d1	Těšínského
knížectví	knížectví	k1gNnSc2	knížectví
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Těšínském	těšínské	k1gNnSc6	těšínské
knížectví	knížectví	k1gNnSc2	knížectví
se	se	k3xPyFc4	se
korunovaná	korunovaný	k2eAgFnSc1d1	korunovaná
orlice	orlice	k1gFnSc1	orlice
objevovala	objevovat	k5eAaImAgFnS	objevovat
nepřetržitě	přetržitě	k6eNd1	přetržitě
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
knížecích	knížecí	k2eAgFnPc6d1	knížecí
pečetích	pečeť	k1gFnPc6	pečeť
<g/>
,	,	kIx,	,
mincích	mince	k1gFnPc6	mince
a	a	k8xC	a
praporech	prapor	k1gInPc6	prapor
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
kněžny	kněžna	k1gFnSc2	kněžna
Alžběty	Alžběta	k1gFnSc2	Alžběta
Lukrécie	Lukrécie	k1gFnSc2	Lukrécie
Těšínské	Těšínská	k1gFnSc2	Těšínská
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1653	[number]	k4	1653
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
dobrým	dobrý	k2eAgInPc3d1	dobrý
politickým	politický	k2eAgInPc3d1	politický
vztahům	vztah	k1gInPc3	vztah
se	se	k3xPyFc4	se
dvorem	dvůr	k1gInSc7	dvůr
posledních	poslední	k2eAgInPc2d1	poslední
Jagellonců	Jagellonec	k1gInPc2	Jagellonec
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vzhled	vzhled	k1gInSc4	vzhled
těšínské	těšínský	k2eAgFnSc2d1	těšínská
orlice	orlice	k1gFnSc2	orlice
podobal	podobat	k5eAaImAgInS	podobat
korunní	korunní	k2eAgFnSc4d1	korunní
orlici	orlice	k1gFnSc4	orlice
Polského	polský	k2eAgNnSc2d1	polské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
rozdílem	rozdíl	k1gInSc7	rozdíl
byla	být	k5eAaImAgFnS	být
forma	forma	k1gFnSc1	forma
samotné	samotný	k2eAgFnSc2d1	samotná
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
polská	polský	k2eAgFnSc1d1	polská
orlice	orlice	k1gFnSc1	orlice
měla	mít	k5eAaImAgFnS	mít
renesančně-barokní	renesančněarokní	k2eAgFnSc4d1	renesančně-barokní
uvařenou	uvařený	k2eAgFnSc4d1	uvařená
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
těšínských	těšínský	k2eAgMnPc2d1	těšínský
knížat	kníže	k1gMnPc2wR	kníže
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
do	do	k7c2	do
samého	samý	k3xTgInSc2	samý
konce	konec	k1gInSc2	konec
středověké	středověký	k2eAgNnSc1d1	středověké
<g/>
,	,	kIx,	,
otevřená	otevřený	k2eAgFnSc1d1	otevřená
koruna	koruna	k1gFnSc1	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
heraldice	heraldika	k1gFnSc6	heraldika
korunovaná	korunovaný	k2eAgFnSc1d1	korunovaná
orlice	orlice	k1gFnSc1	orlice
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgFnSc7d1	státní
orlicí	orlice	k1gFnSc7	orlice
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
Orlice	Orlice	k1gFnSc1	Orlice
na	na	k7c6	na
novodobé	novodobý	k2eAgFnSc6d1	novodobá
vlajce	vlajka	k1gFnSc6	vlajka
Těšínského	Těšínského	k2eAgNnSc2d1	Těšínského
knížectví	knížectví	k1gNnSc2	knížectví
je	být	k5eAaImIp3nS	být
vzorována	vzorován	k2eAgFnSc1d1	vzorován
na	na	k7c6	na
verzi	verze	k1gFnSc6	verze
z	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
PANIC	panic	k1gMnSc1	panic
Idzi	Idzi	k1gNnSc2	Idzi
<g/>
,	,	kIx,	,
Poczet	Poczet	k1gMnSc1	Poczet
Piastów	Piastów	k1gMnSc1	Piastów
i	i	k8xC	i
Piastówen	Piastówen	k2eAgMnSc1d1	Piastówen
cieszyńskich	cieszyńskich	k1gMnSc1	cieszyńskich
<g/>
,	,	kIx,	,
Cieszyn	Cieszyn	k1gMnSc1	Cieszyn
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
PANIC	panic	k1gMnSc1	panic
Idzi	Idzi	k1gNnSc2	Idzi
<g/>
,	,	kIx,	,
Śląsk	Śląsk	k1gInSc1	Śląsk
Cieszyński	Cieszyńsk	k1gFnSc2	Cieszyńsk
w	w	k?	w
średniowieczu	średniowiecz	k1gInSc2	średniowiecz
(	(	kIx(	(
<g/>
do	do	k7c2	do
1528	[number]	k4	1528
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Cieszyn	Cieszyn	k1gInSc1	Cieszyn
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
ZNAMIEROWSKI	ZNAMIEROWSKI	kA	ZNAMIEROWSKI
Alfred	Alfred	k1gMnSc1	Alfred
<g/>
,	,	kIx,	,
Pieczęcie	Pieczęcie	k1gFnSc1	Pieczęcie
i	i	k8xC	i
herby	herba	k1gFnSc2	herba
Śląska	Śląsk	k1gInSc2	Śląsk
Cieszyńskiego	Cieszyńskiego	k1gNnSc1	Cieszyńskiego
<g/>
,	,	kIx,	,
Górki	Górki	k1gNnSc1	Górki
Wielkie	Wielkie	k1gFnSc2	Wielkie
-	-	kIx~	-
Cieszyn	Cieszyn	k1gInSc1	Cieszyn
2011	[number]	k4	2011
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
