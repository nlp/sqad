<s>
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
těšínské	těšínský	k2eAgFnSc6d1	těšínská
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
praporu	prapor	k1gInSc2	prapor
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
vázány	vázat	k5eAaImNgFnP	vázat
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
knížete	kníže	k1gMnSc2	kníže
Přemysla	Přemysl	k1gMnSc2	Přemysl
I.	I.	kA	I.
Nošáka	Nošák	k1gMnSc2	Nošák
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1355	[number]	k4	1355
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
politickém	politický	k2eAgInSc6d1	politický
chodu	chod	k1gInSc6	chod
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
politiků	politik	k1gMnPc2	politik
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
Karla	Karel	k1gMnSc2	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
a	a	k8xC	a
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
