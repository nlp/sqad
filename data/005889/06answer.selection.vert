<s>
Červení	červený	k2eAgMnPc1d1	červený
trpaslíci	trpaslík	k1gMnPc1	trpaslík
nikdy	nikdy	k6eAd1	nikdy
nezažehnou	zažehnout	k5eNaPmIp3nP	zažehnout
jadernou	jaderný	k2eAgFnSc4d1	jaderná
fúzi	fúze	k1gFnSc4	fúze
hélia	hélium	k1gNnSc2	hélium
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
nemohou	moct	k5eNaImIp3nP	moct
stát	stát	k5eAaImF	stát
rudými	rudý	k2eAgMnPc7d1	rudý
obry	obr	k1gMnPc7	obr
<g/>
;	;	kIx,	;
zvolna	zvolna	k6eAd1	zvolna
se	se	k3xPyFc4	se
smršťují	smršťovat	k5eAaImIp3nP	smršťovat
a	a	k8xC	a
zahřívají	zahřívat	k5eAaImIp3nP	zahřívat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nespotřebují	spotřebovat	k5eNaPmIp3nP	spotřebovat
všechen	všechen	k3xTgInSc4	všechen
vodík	vodík	k1gInSc4	vodík
<g/>
.	.	kIx.	.
</s>
