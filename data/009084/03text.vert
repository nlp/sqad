<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Halenkovice	Halenkovice	k1gFnPc1	Halenkovice
je	být	k5eAaImIp3nS	být
územní	územní	k2eAgNnSc1d1	územní
společenství	společenství	k1gNnSc1	společenství
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
s	s	k7c7	s
farním	farní	k2eAgInSc7d1	farní
kostelem	kostel	k1gInSc7	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Josefa	Josef	k1gMnSc2	Josef
v	v	k7c6	v
děkanátu	děkanát	k1gInSc6	děkanát
Zlín	Zlín	k1gInSc1	Zlín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
farnosti	farnost	k1gFnSc2	farnost
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1634	[number]	k4	1634
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1784	[number]	k4	1784
byly	být	k5eAaImAgFnP	být
Halenkovice	Halenkovice	k1gFnPc1	Halenkovice
součástí	součást	k1gFnPc2	součást
spytihněvské	spytihněvský	k2eAgFnSc2d1	spytihněvský
farnosti	farnost	k1gFnSc2	farnost
<g/>
.	.	kIx.	.
</s>
<s>
Farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
vysvěcen	vysvětit	k5eAaPmNgInS	vysvětit
byl	být	k5eAaImAgInS	být
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1789	[number]	k4	1789
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Duchovní	duchovní	k2eAgMnPc1d1	duchovní
správci	správce	k1gMnPc1	správce
==	==	k?	==
</s>
</p>
<p>
<s>
Administrátorem	administrátor	k1gMnSc7	administrátor
excurrendo	excurrendo	k6eAd1	excurrendo
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
2008	[number]	k4	2008
R.	R.	kA	R.
D.	D.	kA	D.
Mgr.	Mgr.	kA	Mgr.
Bohumil	Bohumil	k1gMnSc1	Bohumil
Kundl	Kundl	k1gMnSc1	Kundl
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
platností	platnost	k1gFnSc7	platnost
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
2018	[number]	k4	2018
ho	on	k3xPp3gInSc4	on
vystřídal	vystřídat	k5eAaPmAgInS	vystřídat
R.	R.	kA	R.
D.	D.	kA	D.
Mgr.	Mgr.	kA	Mgr.
Jiří	Jiří	k1gMnSc1	Jiří
Zámečník	Zámečník	k1gMnSc1	Zámečník
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
však	však	k9	však
od	od	k7c2	od
podzimu	podzim	k1gInSc2	podzim
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
a	a	k8xC	a
novým	nový	k2eAgMnSc7d1	nový
administrátorem	administrátor	k1gMnSc7	administrátor
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
2018	[number]	k4	2018
ustanoven	ustanovit	k5eAaPmNgMnS	ustanovit
farářem	farář	k1gMnSc7	farář
R.	R.	kA	R.
D.	D.	kA	D.
ThLic	ThLic	k1gMnSc1	ThLic
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Káňa	Káňa	k1gMnSc1	Káňa
<g/>
,	,	kIx,	,
Th	Th	k1gMnSc1	Th
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
,	,	kIx,	,
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
kaplan	kaplan	k1gMnSc1	kaplan
v	v	k7c6	v
Jevíčku	Jevíčko	k1gNnSc6	Jevíčko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bohoslužby	bohoslužba	k1gFnSc2	bohoslužba
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Aktivity	aktivita	k1gFnPc1	aktivita
ve	v	k7c6	v
farnosti	farnost	k1gFnSc6	farnost
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
farnosti	farnost	k1gFnSc6	farnost
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
koná	konat	k5eAaImIp3nS	konat
tříkrálová	tříkrálový	k2eAgFnSc1d1	Tříkrálová
sbírka	sbírka	k1gFnSc1	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
činil	činit	k5eAaImAgInS	činit
její	její	k3xOp3gInSc1	její
výtěžek	výtěžek	k1gInSc1	výtěžek
53	[number]	k4	53
368	[number]	k4	368
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sbírce	sbírka	k1gFnSc6	sbírka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
se	se	k3xPyFc4	se
vybralo	vybrat	k5eAaPmAgNnS	vybrat
59	[number]	k4	59
076	[number]	k4	076
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Halenkovice	Halenkovice	k1gFnPc4	Halenkovice
na	na	k7c6	na
webu	web	k1gInSc6	web
Arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
</s>
</p>
<p>
<s>
Webové	webový	k2eAgFnPc1d1	webová
stránky	stránka	k1gFnPc1	stránka
farnosti	farnost	k1gFnSc2	farnost
</s>
</p>
