<p>
<s>
Hypertext	hypertext	k1gInSc1	hypertext
Markup	Markup	k1gInSc1	Markup
Language	language	k1gFnSc1	language
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
HTML	HTML	kA	HTML
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
název	název	k1gInSc1	název
značkovacího	značkovací	k2eAgInSc2d1	značkovací
jazyka	jazyk	k1gInSc2	jazyk
používaného	používaný	k2eAgInSc2d1	používaný
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
webových	webový	k2eAgFnPc2d1	webová
stránek	stránka	k1gFnPc2	stránka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
propojeny	propojen	k2eAgInPc1d1	propojen
hypertextovými	hypertextový	k2eAgInPc7d1	hypertextový
odkazy	odkaz	k1gInPc7	odkaz
<g/>
.	.	kIx.	.
</s>
<s>
HTML	HTML	kA	HTML
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgMnSc6d1	hlavní
z	z	k7c2	z
jazyků	jazyk	k1gInPc2	jazyk
pro	pro	k7c4	pro
vytváření	vytváření	k1gNnSc4	vytváření
stránek	stránka	k1gFnPc2	stránka
v	v	k7c6	v
systému	systém	k1gInSc6	systém
World	Worlda	k1gFnPc2	Worlda
Wide	Wide	k1gFnPc2	Wide
Web	web	k1gInSc1	web
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
publikaci	publikace	k1gFnSc4	publikace
dokumentů	dokument	k1gInPc2	dokument
na	na	k7c6	na
Internetu	Internet	k1gInSc6	Internet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
aplikací	aplikace	k1gFnSc7	aplikace
dříve	dříve	k6eAd2	dříve
vyvinutého	vyvinutý	k2eAgInSc2d1	vyvinutý
rozsáhlého	rozsáhlý	k2eAgInSc2d1	rozsáhlý
univerzálního	univerzální	k2eAgInSc2d1	univerzální
značkovacího	značkovací	k2eAgInSc2d1	značkovací
jazyka	jazyk	k1gInSc2	jazyk
SGML	SGML	kA	SGML
(	(	kIx(	(
<g/>
Standard	standard	k1gInSc1	standard
Generalized	Generalized	k1gInSc1	Generalized
Markup	Markup	k1gInSc4	Markup
Language	language	k1gFnSc2	language
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
HTML	HTML	kA	HTML
byl	být	k5eAaImAgInS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
vývojem	vývoj	k1gInSc7	vývoj
webových	webový	k2eAgInPc2d1	webový
prohlížečů	prohlížeč	k1gInPc2	prohlížeč
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zpětně	zpětně	k6eAd1	zpětně
ovlivňovaly	ovlivňovat	k5eAaImAgInP	ovlivňovat
definici	definice	k1gFnSc3	definice
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
jazyka	jazyk	k1gInSc2	jazyk
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
Tim	Tim	k?	Tim
Berners-Lee	Berners-Lee	k1gNnSc1	Berners-Lee
a	a	k8xC	a
Robert	Robert	k1gMnSc1	Robert
Cailliau	Cailliaa	k1gFnSc4	Cailliaa
na	na	k7c6	na
propojeném	propojený	k2eAgInSc6d1	propojený
informačním	informační	k2eAgInSc6d1	informační
systému	systém	k1gInSc6	systém
pro	pro	k7c4	pro
CERN	CERN	kA	CERN
<g/>
,	,	kIx,	,
výzkumné	výzkumný	k2eAgNnSc1d1	výzkumné
centrum	centrum	k1gNnSc1	centrum
fyziky	fyzika	k1gFnSc2	fyzika
poblíž	poblíž	k7c2	poblíž
Ženevy	Ženeva	k1gFnSc2	Ženeva
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
dokumentů	dokument	k1gInPc2	dokument
obvykle	obvykle	k6eAd1	obvykle
používaly	používat	k5eAaImAgInP	používat
jazyky	jazyk	k1gInPc1	jazyk
TeX	TeX	k1gMnPc2	TeX
<g/>
,	,	kIx,	,
PostScript	PostScripta	k1gFnPc2	PostScripta
a	a	k8xC	a
také	také	k9	také
SGML	SGML	kA	SGML
<g/>
.	.	kIx.	.
</s>
<s>
Berners-Lee	Berners-Leat	k5eAaPmIp3nS	Berners-Leat
si	se	k3xPyFc3	se
uvědomoval	uvědomovat	k5eAaImAgMnS	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
něco	něco	k3yInSc4	něco
jednoduššího	jednoduchý	k2eAgNnSc2d2	jednodušší
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byl	být	k5eAaImAgInS	být
tedy	tedy	k9	tedy
navržen	navržen	k2eAgInSc1d1	navržen
jazyk	jazyk	k1gInSc1	jazyk
HTML	HTML	kA	HTML
a	a	k8xC	a
protokol	protokol	k1gInSc1	protokol
HTTP	HTTP	kA	HTTP
(	(	kIx(	(
<g/>
Hypertext	hypertext	k1gInSc1	hypertext
Transfer	transfer	k1gInSc1	transfer
Protocol	Protocol	k1gInSc1	Protocol
–	–	k?	–
protokol	protokol	k1gInSc1	protokol
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
hypertextu	hypertext	k1gInSc2	hypertext
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
Tim	Tim	k?	Tim
Berners-Lee	Berners-Lee	k1gFnSc1	Berners-Lee
napsal	napsat	k5eAaBmAgInS	napsat
první	první	k4xOgInSc4	první
webový	webový	k2eAgInSc4d1	webový
prohlížeč	prohlížeč	k1gInSc4	prohlížeč
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nazval	nazvat	k5eAaBmAgInS	nazvat
WorldWideWeb	WorldWideWba	k1gFnPc2	WorldWideWba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
CERN	CERN	kA	CERN
zprovoznil	zprovoznit	k5eAaPmAgMnS	zprovoznit
svůj	svůj	k3xOyFgInSc4	svůj
web	web	k1gInSc4	web
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
organizace	organizace	k1gFnPc1	organizace
NCSA	NCSA	kA	NCSA
(	(	kIx(	(
<g/>
National	National	k1gMnSc2	National
Center	centrum	k1gNnPc2	centrum
for	forum	k1gNnPc2	forum
Supercomputer	Supercomputra	k1gFnPc2	Supercomputra
Applications	Applicationsa	k1gFnPc2	Applicationsa
<g/>
)	)	kIx)	)
vybídla	vybídnout	k5eAaPmAgFnS	vybídnout
Marca	Marca	k1gFnSc1	Marca
Andreessena	Andreessen	k2eAgFnSc1d1	Andreessena
a	a	k8xC	a
Erica	Erica	k1gFnSc1	Erica
Binu	Binus	k1gInSc2	Binus
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
prohlížeče	prohlížeč	k1gInSc2	prohlížeč
Mosaic	Mosaice	k1gFnPc2	Mosaice
<g/>
;	;	kIx,	;
ten	ten	k3xDgInSc1	ten
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
ve	v	k7c6	v
verzích	verze	k1gFnPc6	verze
pro	pro	k7c4	pro
počítače	počítač	k1gInPc1	počítač
IBM	IBM	kA	IBM
PC	PC	kA	PC
a	a	k8xC	a
Macintosh	Macintosh	kA	Macintosh
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
obrovský	obrovský	k2eAgInSc4d1	obrovský
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgInSc1	první
prohlížeč	prohlížeč	k1gInSc1	prohlížeč
s	s	k7c7	s
grafickým	grafický	k2eAgNnSc7d1	grafické
uživatelským	uživatelský	k2eAgNnSc7d1	Uživatelské
rozhraním	rozhraní	k1gNnSc7	rozhraní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následoval	následovat	k5eAaImAgInS	následovat
rychlý	rychlý	k2eAgInSc1d1	rychlý
rozvoj	rozvoj	k1gInSc1	rozvoj
webu	web	k1gInSc2	web
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
pro	pro	k7c4	pro
HTML	HTML	kA	HTML
definovat	definovat	k5eAaBmF	definovat
standardy	standard	k1gInPc4	standard
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Verze	verze	k1gFnSc1	verze
jazyka	jazyk	k1gInSc2	jazyk
===	===	k?	===
</s>
</p>
<p>
<s>
Verze	verze	k1gFnSc1	verze
0.9	[number]	k4	0.9
<g/>
–	–	k?	–
<g/>
1.2	[number]	k4	1.2
</s>
</p>
<p>
<s>
Byly	být	k5eAaImAgInP	být
vytvářeny	vytvářit	k5eAaPmNgInP	vytvářit
postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
letech	let	k1gInPc6	let
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
verzích	verze	k1gFnPc6	verze
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
Tim	Tim	k?	Tim
Berners-Lee	Berners-Lee	k1gNnSc1	Berners-Lee
a	a	k8xC	a
Daniel	Daniel	k1gMnSc1	Daniel
Connolly	Connolla	k1gFnSc2	Connolla
<g/>
.	.	kIx.	.
</s>
<s>
Nepodporují	podporovat	k5eNaImIp3nP	podporovat
grafické	grafický	k2eAgNnSc4d1	grafické
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původní	původní	k2eAgFnSc1d1	původní
specifikace	specifikace	k1gFnSc1	specifikace
(	(	kIx(	(
<g/>
verze	verze	k1gFnSc1	verze
1.1	[number]	k4	1.1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Původní	původní	k2eAgFnSc1d1	původní
specifikace	specifikace	k1gFnSc1	specifikace
(	(	kIx(	(
<g/>
verze	verze	k1gFnSc1	verze
1.2	[number]	k4	1.2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Verze	verze	k1gFnSc1	verze
2.0	[number]	k4	2.0
</s>
</p>
<p>
<s>
Byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1995	[number]	k4	1995
komunitou	komunita	k1gFnSc7	komunita
IETF	IETF	kA	IETF
a	a	k8xC	a
zachycovala	zachycovat	k5eAaImAgFnS	zachycovat
stav	stav	k1gInSc4	stav
jazyka	jazyk	k1gInSc2	jazyk
k	k	k7c3	k
červnu	červen	k1gInSc3	červen
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zcela	zcela	k6eAd1	zcela
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
syntaxi	syntaxe	k1gFnSc4	syntaxe
SGML	SGML	kA	SGML
<g/>
.	.	kIx.	.
</s>
<s>
Přidává	přidávat	k5eAaImIp3nS	přidávat
k	k	k7c3	k
původní	původní	k2eAgFnSc3d1	původní
specifikaci	specifikace	k1gFnSc3	specifikace
interaktivní	interaktivní	k2eAgInPc4d1	interaktivní
formuláře	formulář	k1gInPc4	formulář
a	a	k8xC	a
podporu	podpora	k1gFnSc4	podpora
grafiky	grafika	k1gFnSc2	grafika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původní	původní	k2eAgFnSc1d1	původní
specifikace	specifikace	k1gFnSc1	specifikace
(	(	kIx(	(
<g/>
verze	verze	k1gFnSc1	verze
2.0	[number]	k4	2.0
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Verze	verze	k1gFnSc1	verze
3.2	[number]	k4	3.2
</s>
</p>
<p>
<s>
Byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1997	[number]	k4	1997
komunitou	komunita	k1gFnSc7	komunita
W3C	W3C	k1gFnSc2	W3C
a	a	k8xC	a
zachycovala	zachycovat	k5eAaImAgFnS	zachycovat
stav	stav	k1gInSc4	stav
jazyka	jazyk	k1gInSc2	jazyk
k	k	k7c3	k
počátku	počátek	k1gInSc3	počátek
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
přidává	přidávat	k5eAaImIp3nS	přidávat
k	k	k7c3	k
jazyku	jazyk	k1gInSc3	jazyk
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
,	,	kIx,	,
zarovnávání	zarovnávání	k1gNnSc2	zarovnávání
textu	text	k1gInSc2	text
a	a	k8xC	a
stylové	stylový	k2eAgInPc4d1	stylový
prvky	prvek	k1gInPc4	prvek
pro	pro	k7c4	pro
ovlivňování	ovlivňování	k1gNnSc4	ovlivňování
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
připravovaná	připravovaný	k2eAgFnSc1d1	připravovaná
verze	verze	k1gFnSc1	verze
HTML	HTML	kA	HTML
3.0	[number]	k4	3.0
nebyla	být	k5eNaImAgFnS	být
nikdy	nikdy	k6eAd1	nikdy
přijata	přijmout	k5eAaPmNgFnS	přijmout
jako	jako	k8xC	jako
standard	standard	k1gInSc1	standard
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byla	být	k5eAaImAgFnS	být
příliš	příliš	k6eAd1	příliš
složitá	složitý	k2eAgFnSc1d1	složitá
a	a	k8xC	a
žádná	žádný	k3yNgFnSc1	žádný
firma	firma	k1gFnSc1	firma
nebyla	být	k5eNaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
naprogramovat	naprogramovat	k5eAaPmF	naprogramovat
její	její	k3xOp3gFnSc4	její
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původní	původní	k2eAgFnSc1d1	původní
specifikace	specifikace	k1gFnSc1	specifikace
(	(	kIx(	(
<g/>
verze	verze	k1gFnSc1	verze
3.2	[number]	k4	3.2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Verze	verze	k1gFnSc1	verze
4.0	[number]	k4	4.0
</s>
</p>
<p>
<s>
Byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1997	[number]	k4	1997
komunitou	komunita	k1gFnSc7	komunita
W	W	kA	W
<g/>
3	[number]	k4	3
<g/>
C.	C.	kA	C.
Do	do	k7c2	do
specifikace	specifikace	k1gFnSc2	specifikace
jazyka	jazyk	k1gInSc2	jazyk
přibyly	přibýt	k5eAaPmAgInP	přibýt
nové	nový	k2eAgInPc1d1	nový
prvky	prvek	k1gInPc1	prvek
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
tabulek	tabulka	k1gFnPc2	tabulka
<g/>
,	,	kIx,	,
formulářů	formulář	k1gInPc2	formulář
a	a	k8xC	a
nově	nově	k6eAd1	nově
byly	být	k5eAaImAgInP	být
standardizovány	standardizován	k2eAgInPc1d1	standardizován
rámy	rám	k1gInPc1	rám
(	(	kIx(	(
<g/>
frames	frames	k1gInSc1	frames
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
původního	původní	k2eAgInSc2d1	původní
účelu	účel	k1gInSc2	účel
–	–	k?	–
prvky	prvek	k1gInPc7	prvek
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
vyznačovat	vyznačovat	k5eAaImF	vyznačovat
význam	význam	k1gInSc4	význam
(	(	kIx(	(
<g/>
sémantiku	sémantika	k1gFnSc4	sémantika
<g/>
)	)	kIx)	)
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
dokumentu	dokument	k1gInSc2	dokument
<g/>
,	,	kIx,	,
vzhled	vzhled	k1gInSc1	vzhled
je	být	k5eAaImIp3nS	být
ovlivňován	ovlivňovat	k5eAaImNgInS	ovlivňovat
připojovanými	připojovaný	k2eAgInPc7d1	připojovaný
styly	styl	k1gInPc7	styl
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
prezentační	prezentační	k2eAgInPc1d1	prezentační
prvky	prvek	k1gInPc1	prvek
byly	být	k5eAaImAgInP	být
zavrženy	zavrhnout	k5eAaPmNgInP	zavrhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původní	původní	k2eAgFnSc1d1	původní
specifikace	specifikace	k1gFnSc1	specifikace
(	(	kIx(	(
<g/>
verze	verze	k1gFnSc1	verze
4.0	[number]	k4	4.0
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zrevidovaná	zrevidovaný	k2eAgFnSc1d1	zrevidovaná
specifikace	specifikace	k1gFnSc1	specifikace
</s>
</p>
<p>
<s>
Verze	verze	k1gFnSc1	verze
4.01	[number]	k4	4.01
</s>
</p>
<p>
<s>
Byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1999	[number]	k4	1999
komunitou	komunita	k1gFnSc7	komunita
W	W	kA	W
<g/>
3	[number]	k4	3
<g/>
C.	C.	kA	C.
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
opravuje	opravovat	k5eAaImIp3nS	opravovat
některé	některý	k3yIgFnPc4	některý
chyby	chyba	k1gFnPc4	chyba
verze	verze	k1gFnSc2	verze
předchozí	předchozí	k2eAgFnSc2d1	předchozí
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
původního	původní	k2eAgInSc2d1	původní
předpokladu	předpoklad	k1gInSc2	předpoklad
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
jednat	jednat	k5eAaImF	jednat
o	o	k7c6	o
poslední	poslední	k2eAgFnSc6d1	poslední
verzi	verze	k1gFnSc6	verze
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yRgFnSc6	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
přešlo	přejít	k5eAaPmAgNnS	přejít
na	na	k7c6	na
XHTML	XHTML	kA	XHTML
–	–	k?	–
následníkovi	následník	k1gMnSc6	následník
HTML	HTML	kA	HTML
<g/>
,	,	kIx,	,
využívajícímu	využívající	k2eAgMnSc3d1	využívající
univerzální	univerzální	k2eAgInSc4d1	univerzální
jazyk	jazyk	k1gInSc4	jazyk
XML	XML	kA	XML
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původní	původní	k2eAgFnSc1d1	původní
specifikace	specifikace	k1gFnSc1	specifikace
(	(	kIx(	(
<g/>
verze	verze	k1gFnSc1	verze
4.01	[number]	k4	4.01
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pozastavení	pozastavení	k1gNnSc1	pozastavení
vývoje	vývoj	k1gInSc2	vývoj
</s>
</p>
<p>
<s>
Některým	některý	k3yIgMnPc3	některý
lidem	člověk	k1gMnPc3	člověk
se	se	k3xPyFc4	se
však	však	k9	však
vývoj	vývoj	k1gInSc1	vývoj
okolo	okolo	k7c2	okolo
XHTML	XHTML	kA	XHTML
nezamlouval	zamlouvat	k5eNaImAgMnS	zamlouvat
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
včetně	včetně	k7c2	včetně
některých	některý	k3yIgMnPc2	některý
tvůrců	tvůrce	k1gMnPc2	tvůrce
webových	webový	k2eAgInPc2d1	webový
prohlížečů	prohlížeč	k1gInPc2	prohlížeč
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Mozilla	Mozilla	k1gFnSc1	Mozilla
Foundation	Foundation	k1gInSc1	Foundation
<g/>
,	,	kIx,	,
Opera	opera	k1gFnSc1	opera
Software	software	k1gInSc1	software
či	či	k8xC	či
Apple	Apple	kA	Apple
<g/>
,	,	kIx,	,
založila	založit	k5eAaPmAgFnS	založit
iniciativu	iniciativa	k1gFnSc4	iniciativa
WHATWG	WHATWG	kA	WHATWG
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
připravit	připravit	k5eAaPmF	připravit
specifikace	specifikace	k1gFnPc4	specifikace
pro	pro	k7c4	pro
novou	nový	k2eAgFnSc4d1	nová
verzi	verze	k1gFnSc4	verze
HTML	HTML	kA	HTML
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gNnSc4	on
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
schválit	schválit	k5eAaPmF	schválit
přímo	přímo	k6eAd1	přímo
W	W	kA	W
<g/>
3	[number]	k4	3
<g/>
C.	C.	kA	C.
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2007	[number]	k4	2007
nakonec	nakonec	k6eAd1	nakonec
W3C	W3C	k1gFnSc4	W3C
založilo	založit	k5eAaPmAgNnS	založit
novou	nový	k2eAgFnSc4d1	nová
pracovní	pracovní	k2eAgFnSc4d1	pracovní
skupinu	skupina	k1gFnSc4	skupina
HTML	HTML	kA	HTML
Working	Working	k1gInSc1	Working
Group	Group	k1gInSc1	Group
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gInSc7	její
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
uvolnit	uvolnit	k5eAaPmF	uvolnit
specifikaci	specifikace	k1gFnSc4	specifikace
nové	nový	k2eAgFnSc2d1	nová
verze	verze	k1gFnSc2	verze
HTML	HTML	kA	HTML
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
hlasování	hlasování	k1gNnSc6	hlasování
bylo	být	k5eAaImAgNnS	být
určeno	určit	k5eAaPmNgNnS	určit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nová	nový	k2eAgFnSc1d1	nová
verze	verze	k1gFnSc1	verze
ponese	ponést	k5eAaPmIp3nS	ponést
označení	označení	k1gNnSc4	označení
"	"	kIx"	"
<g/>
HTML	HTML	kA	HTML
<g/>
5	[number]	k4	5
<g/>
"	"	kIx"	"
a	a	k8xC	a
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
specifikacích	specifikace	k1gFnPc6	specifikace
Web	web	k1gInSc4	web
Applications	Applications	k1gInSc4	Applications
1.0	[number]	k4	1.0
a	a	k8xC	a
Web	web	k1gInSc1	web
Forms	Forms	k1gInSc1	Forms
2.0	[number]	k4	2.0
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
WHATWG	WHATWG	kA	WHATWG
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
budou	být	k5eAaImBp3nP	být
dále	daleko	k6eAd2	daleko
upravovány	upravovat	k5eAaImNgFnP	upravovat
<g/>
.	.	kIx.	.
</s>
<s>
Skupinu	skupina	k1gFnSc4	skupina
pro	pro	k7c4	pro
XHTML	XHTML	kA	XHTML
2.0	[number]	k4	2.0
a	a	k8xC	a
XForms	XForms	k1gInSc1	XForms
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
původně	původně	k6eAd1	původně
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
jediný	jediný	k2eAgInSc4d1	jediný
budoucí	budoucí	k2eAgInSc4d1	budoucí
směr	směr	k1gInSc4	směr
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
W3C	W3C	k1gMnSc7	W3C
definitivně	definitivně	k6eAd1	definitivně
rozpustil	rozpustit	k5eAaPmAgMnS	rozpustit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Verze	verze	k1gFnSc1	verze
5.0	[number]	k4	5.0
</s>
</p>
<p>
<s>
Po	po	k7c6	po
patnáctileté	patnáctiletý	k2eAgFnSc6d1	patnáctiletá
odmlce	odmlka	k1gFnSc6	odmlka
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
nová	nový	k2eAgFnSc1d1	nová
verze	verze	k1gFnSc1	verze
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
již	již	k6eAd1	již
ukončuje	ukončovat	k5eAaImIp3nS	ukončovat
závislost	závislost	k1gFnSc4	závislost
HTML	HTML	kA	HTML
na	na	k7c6	na
SGML	SGML	kA	SGML
opravuje	opravovat	k5eAaImIp3nS	opravovat
mnoho	mnoho	k4c1	mnoho
chyb	chyba	k1gFnPc2	chyba
předešlé	předešlý	k2eAgFnSc2d1	předešlá
verze	verze	k1gFnSc2	verze
<g/>
,	,	kIx,	,
vyřazuje	vyřazovat	k5eAaImIp3nS	vyřazovat
mnoho	mnoho	k4c1	mnoho
zastaralých	zastaralý	k2eAgFnPc2d1	zastaralá
a	a	k8xC	a
již	již	k6eAd1	již
nepoužívaných	používaný	k2eNgInPc2d1	nepoužívaný
prvků	prvek	k1gInPc2	prvek
a	a	k8xC	a
přidává	přidávat	k5eAaImIp3nS	přidávat
nové	nový	k2eAgInPc4d1	nový
sémantické	sémantický	k2eAgInPc4d1	sémantický
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
přidává	přidávat	k5eAaImIp3nS	přidávat
podporu	podpora	k1gFnSc4	podpora
mnohých	mnohý	k2eAgFnPc2d1	mnohá
nových	nový	k2eAgFnPc2d1	nová
a	a	k8xC	a
moderních	moderní	k2eAgFnPc2d1	moderní
technologií	technologie	k1gFnPc2	technologie
a	a	k8xC	a
zavádí	zavádět	k5eAaImIp3nS	zavádět
nový	nový	k2eAgInSc4d1	nový
systém	systém	k1gInSc4	systém
vývoje	vývoj	k1gInSc2	vývoj
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
specifikace	specifikace	k1gFnSc1	specifikace
(	(	kIx(	(
<g/>
verze	verze	k1gFnSc1	verze
5.0	[number]	k4	5.0
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Verze	verze	k1gFnSc1	verze
5.1	[number]	k4	5.1
</s>
</p>
<p>
<s>
specifikace	specifikace	k1gFnSc1	specifikace
(	(	kIx(	(
<g/>
verze	verze	k1gFnSc1	verze
5.1	[number]	k4	5.1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Verze	verze	k1gFnSc1	verze
5.2	[number]	k4	5.2
</s>
</p>
<p>
<s>
specifikace	specifikace	k1gFnSc1	specifikace
(	(	kIx(	(
<g/>
verze	verze	k1gFnSc1	verze
5.2	[number]	k4	5.2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Verze	verze	k1gFnSc1	verze
5.3	[number]	k4	5.3
</s>
</p>
<p>
<s>
specifikace	specifikace	k1gFnSc1	specifikace
(	(	kIx(	(
<g/>
verze	verze	k1gFnSc1	verze
5.3	[number]	k4	5.3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
jazyka	jazyk	k1gInSc2	jazyk
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Koncepce	koncepce	k1gFnSc1	koncepce
jazyka	jazyk	k1gInSc2	jazyk
===	===	k?	===
</s>
</p>
<p>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
HTML	HTML	kA	HTML
je	být	k5eAaImIp3nS	být
charakterizován	charakterizovat	k5eAaBmNgInS	charakterizovat
množinou	množina	k1gFnSc7	množina
značek	značka	k1gFnPc2	značka
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
tagů	tag	k1gInPc2	tag
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
vlastností	vlastnost	k1gFnPc2	vlastnost
(	(	kIx(	(
<g/>
atributů	atribut	k1gInPc2	atribut
<g/>
)	)	kIx)	)
definovaných	definovaný	k2eAgInPc2d1	definovaný
pro	pro	k7c4	pro
danou	daný	k2eAgFnSc4d1	daná
verzi	verze	k1gFnSc4	verze
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zápis	zápis	k1gInSc1	zápis
byl	být	k5eAaImAgInS	být
převzat	převzít	k5eAaPmNgInS	převzít
ze	z	k7c2	z
SGML	SGML	kA	SGML
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
upravován	upravovat	k5eAaImNgInS	upravovat
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
aktuálních	aktuální	k2eAgInPc6d1	aktuální
trendech	trend	k1gInPc6	trend
Internetu	Internet	k1gInSc2	Internet
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
značky	značka	k1gFnPc4	značka
se	se	k3xPyFc4	se
uzavírají	uzavírat	k5eAaPmIp3nP	uzavírat
části	část	k1gFnPc1	část
textu	text	k1gInSc2	text
dokumentu	dokument	k1gInSc2	dokument
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
určuje	určovat	k5eAaImIp3nS	určovat
význam	význam	k1gInSc1	význam
(	(	kIx(	(
<g/>
sémantika	sémantika	k1gFnSc1	sémantika
<g/>
)	)	kIx)	)
obsaženého	obsažený	k2eAgInSc2d1	obsažený
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Názvy	název	k1gInPc1	název
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
značek	značka	k1gFnPc2	značka
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
vlastností	vlastnost	k1gFnPc2	vlastnost
se	se	k3xPyFc4	se
uzavírají	uzavírat	k5eAaPmIp3nP	uzavírat
mezi	mezi	k7c4	mezi
úhlové	úhlový	k2eAgFnPc4d1	úhlová
závorky	závorka	k1gFnPc4	závorka
<	<	kIx(	<
a	a	k8xC	a
>	>	kIx)	>
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
dokumentu	dokument	k1gInSc2	dokument
tvořená	tvořený	k2eAgFnSc1d1	tvořená
otevírací	otevírací	k2eAgFnSc7d1	otevírací
značkou	značka	k1gFnSc7	značka
<g/>
,	,	kIx,	,
nějakým	nějaký	k3yIgInSc7	nějaký
obsahem	obsah	k1gInSc7	obsah
a	a	k8xC	a
odpovídající	odpovídající	k2eAgFnSc7d1	odpovídající
ukončovací	ukončovací	k2eAgFnSc7d1	ukončovací
značkou	značka	k1gFnSc7	značka
tvoří	tvořit	k5eAaImIp3nS	tvořit
prvek	prvek	k1gInSc1	prvek
(	(	kIx(	(
<g/>
element	element	k1gInSc1	element
<g/>
)	)	kIx)	)
dokumentu	dokument	k1gInSc2	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
<strong>
je	být	k5eAaImIp3nS	být
otevírací	otevírací	k2eAgFnSc1d1	otevírací
značka	značka	k1gFnSc1	značka
pro	pro	k7c4	pro
zvýraznění	zvýraznění	k1gNnSc4	zvýraznění
textu	text	k1gInSc2	text
a	a	k8xC	a
<strong>
Červená	červenat	k5eAaImIp3nS	červenat
Karkulka	Karkulka	k1gFnSc1	Karkulka
</strong>
je	být	k5eAaImIp3nS	být
prvek	prvek	k1gInSc4	prvek
obsahující	obsahující	k2eAgInSc4d1	obsahující
zvýrazněný	zvýrazněný	k2eAgInSc4d1	zvýrazněný
text	text	k1gInSc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
obsahu	obsah	k1gInSc2	obsah
prvku	prvek	k1gInSc2	prvek
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
další	další	k2eAgInPc4d1	další
vnořené	vnořený	k2eAgInPc4d1	vnořený
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Značky	značka	k1gFnPc1	značka
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
párové	párový	k2eAgMnPc4d1	párový
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
koncová	koncový	k2eAgFnSc1d1	koncová
značka	značka	k1gFnSc1	značka
je	být	k5eAaImIp3nS	být
shodná	shodný	k2eAgFnSc1d1	shodná
se	s	k7c7	s
značkou	značka	k1gFnSc7	značka
počáteční	počáteční	k2eAgFnSc7d1	počáteční
<g/>
,	,	kIx,	,
jen	jen	k9	jen
má	mít	k5eAaImIp3nS	mít
před	před	k7c7	před
názvem	název	k1gInSc7	název
znak	znak	k1gInSc1	znak
lomítko	lomítko	k1gNnSc1	lomítko
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
odstavce	odstavec	k1gInSc2	odstavec
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ukázka	ukázka	k1gFnSc1	ukázka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Text	text	k1gInSc1	text
odstavce	odstavec	k1gInSc2	odstavec
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
značky	značka	k1gFnPc1	značka
jsou	být	k5eAaImIp3nP	být
nepárové	párový	k2eNgInPc1d1	nepárový
–	–	k?	–
nemají	mít	k5eNaImIp3nP	mít
žádný	žádný	k3yNgInSc4	žádný
obsah	obsah	k1gInSc4	obsah
a	a	k8xC	a
nepoužívají	používat	k5eNaImIp3nP	používat
koncovou	koncový	k2eAgFnSc4d1	koncová
značku	značka	k1gFnSc4	značka
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
pro	pro	k7c4	pro
vykreslení	vykreslení	k1gNnSc4	vykreslení
vodorovné	vodorovný	k2eAgFnSc2d1	vodorovná
čáry	čára	k1gFnSc2	čára
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ukázka	ukázka	k1gFnSc1	ukázka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Značky	značka	k1gFnPc1	značka
mohou	moct	k5eAaImIp3nP	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
o	o	k7c6	o
nich	on	k3xPp3gFnPc6	on
uvádějí	uvádět	k5eAaImIp3nP	uvádět
určitou	určitý	k2eAgFnSc4d1	určitá
důležitou	důležitý	k2eAgFnSc4d1	důležitá
informaci	informace	k1gFnSc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
Koncové	koncový	k2eAgFnPc4d1	koncová
značky	značka	k1gFnPc4	značka
již	již	k6eAd1	již
vlastnosti	vlastnost	k1gFnPc4	vlastnost
nemají	mít	k5eNaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
odkaz	odkaz	k1gInSc4	odkaz
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
<a>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
vlastnost	vlastnost	k1gFnSc1	vlastnost
href	href	k1gInSc4	href
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
uživatel	uživatel	k1gMnSc1	uživatel
po	po	k7c6	po
kliknutí	kliknutí	k1gNnSc6	kliknutí
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
dostane	dostat	k5eAaPmIp3nS	dostat
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
příkladu	příklad	k1gInSc6	příklad
na	na	k7c4	na
stránku	stránka	k1gFnSc4	stránka
http://example.com	[url]	k1gInSc1	http://example.com
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
značkám	značka	k1gFnPc3	značka
je	být	k5eAaImIp3nS	být
však	však	k9	však
možno	možno	k6eAd1	možno
přiložit	přiložit	k5eAaPmF	přiložit
i	i	k9	i
více	hodně	k6eAd2	hodně
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
například	například	k6eAd1	například
vytvořit	vytvořit	k5eAaPmF	vytvořit
odkaz	odkaz	k1gInSc4	odkaz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
otevře	otevřít	k5eAaPmIp3nS	otevřít
v	v	k7c6	v
novém	nový	k2eAgNnSc6d1	nové
okně	okno	k1gNnSc6	okno
<g/>
/	/	kIx~	/
<g/>
panelu	panel	k1gInSc2	panel
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
A	a	k9	a
nebo	nebo	k8xC	nebo
odkaz	odkaz	k1gInSc4	odkaz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
otevře	otevřít	k5eAaPmIp3nS	otevřít
v	v	k7c6	v
novém	nový	k2eAgNnSc6d1	nové
okně	okno	k1gNnSc6	okno
<g/>
/	/	kIx~	/
<g/>
panelu	panel	k1gInSc2	panel
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
titulek	titulek	k1gInSc1	titulek
(	(	kIx(	(
<g/>
zobrazí	zobrazit	k5eAaPmIp3nS	zobrazit
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
po	po	k7c6	po
najetí	najetí	k1gNnSc6	najetí
kurzorem	kurzor	k1gInSc7	kurzor
na	na	k7c4	na
daný	daný	k2eAgInSc4d1	daný
prvek	prvek	k1gInSc4	prvek
<g/>
)	)	kIx)	)
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
přiřazenou	přiřazený	k2eAgFnSc4d1	přiřazená
třídu	třída	k1gFnSc4	třída
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
pak	pak	k6eAd1	pak
můžeme	moct	k5eAaImIp1nP	moct
využít	využít	k5eAaPmF	využít
například	například	k6eAd1	například
v	v	k7c6	v
kaskádových	kaskádový	k2eAgInPc6d1	kaskádový
stylech	styl	k1gInPc6	styl
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zadávání	zadávání	k1gNnSc2	zadávání
odkazů	odkaz	k1gInPc2	odkaz
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
zakázané	zakázaný	k2eAgNnSc1d1	zakázané
<g/>
,	,	kIx,	,
nebude	být	k5eNaImBp3nS	být
zde	zde	k6eAd1	zde
možné	možný	k2eAgNnSc1d1	možné
zobrazit	zobrazit	k5eAaPmF	zobrazit
ukázku	ukázka	k1gFnSc4	ukázka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
prohlížeče	prohlížeč	k1gInPc1	prohlížeč
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Opera	opera	k1gFnSc1	opera
nebo	nebo	k8xC	nebo
Mozilla	Mozilla	k1gFnSc1	Mozilla
Firefox	Firefox	k1gInSc1	Firefox
<g/>
)	)	kIx)	)
dokonce	dokonce	k9	dokonce
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
"	"	kIx"	"
<g/>
vytvářet	vytvářet	k5eAaImF	vytvářet
<g/>
"	"	kIx"	"
vlastní	vlastní	k2eAgFnPc4d1	vlastní
značky	značka	k1gFnPc4	značka
a	a	k8xC	a
prvky	prvek	k1gInPc4	prvek
a	a	k8xC	a
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
také	také	k9	také
jejich	jejich	k3xOp3gNnSc4	jejich
stylování	stylování	k1gNnSc4	stylování
pomocí	pomocí	k7c2	pomocí
kaskádových	kaskádový	k2eAgInPc2d1	kaskádový
stylů	styl	k1gInPc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
funkce	funkce	k1gFnSc1	funkce
však	však	k9	však
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
webových	webový	k2eAgInPc2d1	webový
prohlížečů	prohlížeč	k1gInPc2	prohlížeč
není	být	k5eNaImIp3nS	být
podporována	podporovat	k5eAaImNgFnS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
obsah	obsah	k1gInSc1	obsah
neznámých	známý	k2eNgInPc2d1	neznámý
prvků	prvek	k1gInPc2	prvek
v	v	k7c6	v
prohlížečích	prohlížeč	k1gInPc6	prohlížeč
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podpora	podpora	k1gFnSc1	podpora
této	tento	k3xDgFnSc2	tento
funkce	funkce	k1gFnSc2	funkce
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
,	,	kIx,	,
zobrazí	zobrazit	k5eAaPmIp3nS	zobrazit
chybně	chybně	k6eAd1	chybně
a	a	k8xC	a
bez	bez	k7c2	bez
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
stylu	styl	k1gInSc2	styl
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgInPc4d1	vlastní
prvky	prvek	k1gInPc4	prvek
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
nepoužívají	používat	k5eNaImIp3nP	používat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dokument	dokument	k1gInSc1	dokument
může	moct	k5eAaImIp3nS	moct
mimo	mimo	k7c4	mimo
značkování	značkování	k1gNnSc4	značkování
obsahovat	obsahovat	k5eAaImF	obsahovat
další	další	k2eAgInPc4d1	další
prvky	prvek	k1gInPc4	prvek
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
DTD	DTD	kA	DTD
direktivy	direktiva	k1gFnPc1	direktiva
–	–	k?	–
začínají	začínat	k5eAaImIp3nP	začínat
znaky	znak	k1gInPc4	znak
<	<	kIx(	<
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgFnP	určit
pro	pro	k7c4	pro
zpracovatele	zpracovatel	k1gMnPc4	zpracovatel
dokumentu	dokument	k1gInSc2	dokument
(	(	kIx(	(
<g/>
prohlížeč	prohlížeč	k1gMnSc1	prohlížeč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Komentáře	komentář	k1gInPc1	komentář
–	–	k?	–
pomocné	pomocný	k2eAgInPc1d1	pomocný
texty	text	k1gInPc1	text
pro	pro	k7c4	pro
programátora	programátor	k1gMnSc4	programátor
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
součástí	součást	k1gFnSc7	součást
obsahu	obsah	k1gInSc2	obsah
dokumentu	dokument	k1gInSc2	dokument
a	a	k8xC	a
nezobrazují	zobrazovat	k5eNaImIp3nP	zobrazovat
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
prohlížeč	prohlížeč	k1gMnSc1	prohlížeč
je	on	k3xPp3gInPc4	on
ignoruje	ignorovat	k5eAaImIp3nS	ignorovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
komentáře	komentář	k1gInSc2	komentář
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgInS	uvést
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kód	kód	k1gInSc1	kód
skriptovacích	skriptovací	k2eAgInPc2d1	skriptovací
jazyků	jazyk	k1gInPc2	jazyk
</s>
</p>
<p>
<s>
Kaskádové	kaskádový	k2eAgInPc1d1	kaskádový
styly	styl	k1gInPc1	styl
</s>
</p>
<p>
<s>
===	===	k?	===
Struktura	struktura	k1gFnSc1	struktura
dokumentu	dokument	k1gInSc2	dokument
===	===	k?	===
</s>
</p>
<p>
<s>
Dokument	dokument	k1gInSc1	dokument
v	v	k7c6	v
jazyku	jazyk	k1gInSc6	jazyk
HTML	HTML	kA	HTML
má	mít	k5eAaImIp3nS	mít
předepsanou	předepsaný	k2eAgFnSc4d1	předepsaná
strukturu	struktura	k1gFnSc4	struktura
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Deklarace	deklarace	k1gFnSc1	deklarace
typu	typ	k1gInSc2	typ
dokumentu	dokument	k1gInSc2	dokument
–	–	k?	–
značka	značka	k1gFnSc1	značka
<	<	kIx(	<
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
DOCTYPE	DOCTYPE	kA	DOCTYPE
html	html	k1gMnSc1	html
<g/>
>	>	kIx)	>
sděluje	sdělovat	k5eAaImIp3nS	sdělovat
prohlížeči	prohlížeč	k1gInSc3	prohlížeč
<g/>
,	,	kIx,	,
že	že	k8xS	že
otevřel	otevřít	k5eAaPmAgMnS	otevřít
HTML	HTML	kA	HTML
dokument	dokument	k1gInSc1	dokument
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kořenový	kořenový	k2eAgInSc1d1	kořenový
element	element	k1gInSc1	element
–	–	k?	–
prvek	prvek	k1gInSc1	prvek
html	html	k1gInSc1	html
(	(	kIx(	(
<g/>
značky	značka	k1gFnPc1	značka
<html>
a	a	k8xC	a
</html>
)	)	kIx)	)
–	–	k?	–
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
celý	celý	k2eAgInSc4d1	celý
dokument	dokument	k1gInSc4	dokument
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavička	hlavička	k1gFnSc1	hlavička
dokumentu	dokument	k1gInSc2	dokument
–	–	k?	–
prvek	prvek	k1gInSc1	prvek
head	head	k1gInSc1	head
(	(	kIx(	(
<g/>
značky	značka	k1gFnPc1	značka
<head>
a	a	k8xC	a
</head>
)	)	kIx)	)
–	–	k?	–
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
metadata	metadata	k1gFnSc1	metadata
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vztahují	vztahovat	k5eAaImIp3nP	vztahovat
k	k	k7c3	k
celému	celý	k2eAgInSc3d1	celý
dokumentu	dokument	k1gInSc3	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Definuje	definovat	k5eAaBmIp3nS	definovat
např.	např.	kA	např.
kódování	kódování	k1gNnSc1	kódování
<g/>
,	,	kIx,	,
název	název	k1gInSc1	název
dokumentu	dokument	k1gInSc2	dokument
<g/>
,	,	kIx,	,
autora	autor	k1gMnSc2	autor
<g/>
,	,	kIx,	,
popis	popis	k1gInSc4	popis
<g/>
,	,	kIx,	,
klíčová	klíčový	k2eAgNnPc1d1	klíčové
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
titulek	titulek	k1gInSc1	titulek
dokumentu	dokument	k1gInSc2	dokument
nebo	nebo	k8xC	nebo
kaskádové	kaskádový	k2eAgInPc1d1	kaskádový
styly	styl	k1gInPc1	styl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tělo	tělo	k1gNnSc1	tělo
dokumentu	dokument	k1gInSc2	dokument
–	–	k?	–
prvek	prvek	k1gInSc1	prvek
body	bod	k1gInPc1	bod
(	(	kIx(	(
<g/>
značky	značka	k1gFnPc1	značka
<body>
a	a	k8xC	a
</body>
)	)	kIx)	)
–	–	k?	–
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
vlastní	vlastní	k2eAgInSc4d1	vlastní
obsah	obsah	k1gInSc4	obsah
dokumentu	dokument	k1gInSc2	dokument
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Příklad	příklad	k1gInSc1	příklad
zdrojového	zdrojový	k2eAgInSc2d1	zdrojový
kódu	kód	k1gInSc2	kód
===	===	k?	===
</s>
</p>
<p>
<s>
Příklad	příklad	k1gInSc1	příklad
HTML	HTML	kA	HTML
dokumentu	dokument	k1gInSc2	dokument
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
5	[number]	k4	5
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Druhy	druh	k1gInPc4	druh
značek	značka	k1gFnPc2	značka
===	===	k?	===
</s>
</p>
<p>
<s>
Značky	značka	k1gFnPc4	značka
lze	lze	k6eAd1	lze
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
významu	význam	k1gInSc2	význam
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
základní	základní	k2eAgFnPc4d1	základní
skupiny	skupina	k1gFnPc4	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Strukturální	strukturální	k2eAgFnPc1d1	strukturální
značky	značka	k1gFnPc1	značka
</s>
</p>
<p>
<s>
rozvrhují	rozvrhovat	k5eAaImIp3nP	rozvrhovat
strukturu	struktura	k1gFnSc4	struktura
dokumentu	dokument	k1gInSc2	dokument
<g/>
,	,	kIx,	,
příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
třeba	třeba	k6eAd1	třeba
odstavce	odstavec	k1gInSc2	odstavec
(	(	kIx(	(
<g/>
</s>
<p>
<s>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
nadpisy	nadpis	k1gInPc1	nadpis
(	(	kIx(	(
<g/>
<h1>
,	,	kIx,	,
<h2>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dodávají	dodávat	k5eAaImIp3nP	dodávat
dokumentu	dokument	k1gInSc6	dokument
formu	forma	k1gFnSc4	forma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Popisné	popisný	k2eAgFnPc4d1	popisná
(	(	kIx(	(
<g/>
sémantické	sémantický	k2eAgFnPc4d1	sémantická
<g/>
)	)	kIx)	)
značky	značka	k1gFnPc4	značka
</s>
</p>
<p>
<s>
popisují	popisovat	k5eAaImIp3nP	popisovat
povahu	povaha	k1gFnSc4	povaha
obsahu	obsah	k1gInSc2	obsah
prvku	prvek	k1gInSc2	prvek
<g/>
,	,	kIx,	,
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
nadpis	nadpis	k1gInSc1	nadpis
(	(	kIx(	(
<g/>
<title>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
adresa	adresa	k1gFnSc1	adresa
(	(	kIx(	(
<g/>
<address>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
trend	trend	k1gInSc1	trend
je	být	k5eAaImIp3nS	být
orientován	orientovat	k5eAaBmNgInS	orientovat
právě	právě	k9	právě
na	na	k7c4	na
sémantické	sémantický	k2eAgFnPc4d1	sémantická
značky	značka	k1gFnPc4	značka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
usnadňují	usnadňovat	k5eAaImIp3nP	usnadňovat
automatizované	automatizovaný	k2eAgNnSc4d1	automatizované
zpracovávání	zpracovávání	k1gNnSc4	zpracovávání
dokumentů	dokument	k1gInPc2	dokument
a	a	k8xC	a
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
informací	informace	k1gFnPc2	informace
v	v	k7c6	v
záplavě	záplava	k1gFnSc6	záplava
dokumentů	dokument	k1gInPc2	dokument
na	na	k7c6	na
webu	web	k1gInSc6	web
<g/>
.	.	kIx.	.
</s>
<s>
Vyvrcholením	vyvrcholení	k1gNnSc7	vyvrcholení
této	tento	k3xDgFnSc2	tento
snahy	snaha	k1gFnSc2	snaha
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jazyk	jazyk	k1gInSc1	jazyk
XML	XML	kA	XML
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stylistické	stylistický	k2eAgFnPc1d1	stylistická
značky	značka	k1gFnPc1	značka
</s>
</p>
<p>
<s>
určují	určovat	k5eAaImIp3nP	určovat
vzhled	vzhled	k1gInSc4	vzhled
prvku	prvek	k1gInSc2	prvek
při	při	k7c6	při
zobrazení	zobrazení	k1gNnSc6	zobrazení
<g/>
,	,	kIx,	,
typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
značka	značka	k1gFnSc1	značka
pro	pro	k7c4	pro
tučné	tučný	k2eAgNnSc4d1	tučné
písmo	písmo	k1gNnSc4	písmo
(	(	kIx(	(
<g/>
<b>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
značek	značka	k1gFnPc2	značka
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
upouští	upouštět	k5eAaImIp3nS	upouštět
<g/>
,	,	kIx,	,
trendem	trend	k1gInSc7	trend
je	být	k5eAaImIp3nS	být
používání	používání	k1gNnSc1	používání
kaskádových	kaskádový	k2eAgInPc2d1	kaskádový
stylů	styl	k1gInPc2	styl
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vzhled	vzhled	k1gInSc4	vzhled
popisují	popisovat	k5eAaImIp3nP	popisovat
odděleně	odděleně	k6eAd1	odděleně
od	od	k7c2	od
obsahu	obsah	k1gInSc2	obsah
dokumentu	dokument	k1gInSc2	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
důvody	důvod	k1gInPc7	důvod
pro	pro	k7c4	pro
neužívání	neužívání	k1gNnPc4	neužívání
těchto	tento	k3xDgFnPc2	tento
značek	značka	k1gFnPc2	značka
patří	patřit	k5eAaImIp3nS	patřit
především	především	k9	především
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
značky	značka	k1gFnPc1	značka
jsou	být	k5eAaImIp3nP	být
orientovány	orientovat	k5eAaBmNgFnP	orientovat
na	na	k7c4	na
prohlížení	prohlížení	k1gNnSc4	prohlížení
na	na	k7c6	na
obrazovce	obrazovka	k1gFnSc6	obrazovka
počítače	počítač	k1gInSc2	počítač
<g/>
,	,	kIx,	,
příliš	příliš	k6eAd1	příliš
se	se	k3xPyFc4	se
však	však	k9	však
nepočítá	počítat	k5eNaImIp3nS	počítat
s	s	k7c7	s
používáním	používání	k1gNnSc7	používání
dokumentu	dokument	k1gInSc2	dokument
jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
–	–	k?	–
alternativní	alternativní	k2eAgInPc4d1	alternativní
prohlížeče	prohlížeč	k1gInPc4	prohlížeč
pro	pro	k7c4	pro
postižené	postižený	k2eAgMnPc4d1	postižený
(	(	kIx(	(
<g/>
čtečky	čtečka	k1gFnPc1	čtečka
pro	pro	k7c4	pro
slepce	slepec	k1gMnPc4	slepec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
mobilních	mobilní	k2eAgNnPc6d1	mobilní
zařízeních	zařízení	k1gNnPc6	zařízení
apod.	apod.	kA	apod.
Kaskádové	kaskádový	k2eAgInPc1d1	kaskádový
styly	styl	k1gInPc1	styl
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
definovat	definovat	k5eAaBmF	definovat
rozdílná	rozdílný	k2eAgNnPc1d1	rozdílné
zobrazení	zobrazení	k1gNnPc1	zobrazení
pro	pro	k7c4	pro
různá	různý	k2eAgNnPc4d1	různé
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Programy	program	k1gInPc1	program
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
HTML	HTML	kA	HTML
==	==	k?	==
</s>
</p>
<p>
<s>
Programy	program	k1gInPc1	program
pro	pro	k7c4	pro
snadnou	snadný	k2eAgFnSc4d1	snadná
tvorbu	tvorba	k1gFnSc4	tvorba
webových	webový	k2eAgFnPc2d1	webová
stránek	stránka	k1gFnPc2	stránka
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
editory	editor	k1gInPc1	editor
<g/>
.	.	kIx.	.
</s>
<s>
Editorem	editor	k1gInSc7	editor
HTML	HTML	kA	HTML
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
program	program	k1gInSc4	program
pracující	pracující	k1gFnSc2	pracující
s	s	k7c7	s
textem	text	k1gInSc7	text
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
však	však	k9	však
používají	používat	k5eAaImIp3nP	používat
mnohem	mnohem	k6eAd1	mnohem
sofistikovanější	sofistikovaný	k2eAgInPc1d2	sofistikovanější
programy	program	k1gInPc1	program
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Textové	textový	k2eAgInPc1d1	textový
editory	editor	k1gInPc1	editor
===	===	k?	===
</s>
</p>
<p>
<s>
Běžný	běžný	k2eAgInSc1d1	běžný
textový	textový	k2eAgInSc1d1	textový
editor	editor	k1gInSc1	editor
HTML	HTML	kA	HTML
zvládá	zvládat	k5eAaImIp3nS	zvládat
barevnou	barevný	k2eAgFnSc4d1	barevná
syntaxi	syntaxe	k1gFnSc4	syntaxe
(	(	kIx(	(
<g/>
barevně	barevně	k6eAd1	barevně
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
části	část	k1gFnPc4	část
kódu	kód	k1gInSc2	kód
jako	jako	k8xS	jako
například	například	k6eAd1	například
HTML	HTML	kA	HTML
značky	značka	k1gFnSc2	značka
či	či	k8xC	či
vlastnosti	vlastnost	k1gFnPc4	vlastnost
a	a	k8xC	a
prostý	prostý	k2eAgInSc4d1	prostý
text	text	k1gInSc4	text
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dokáže	dokázat	k5eAaPmIp3nS	dokázat
napovídat	napovídat	k5eAaBmF	napovídat
značky	značka	k1gFnPc4	značka
<g/>
,	,	kIx,	,
zná	znát	k5eAaImIp3nS	znát
chytré	chytrý	k2eAgInPc4d1	chytrý
tabulátory	tabulátor	k1gInPc4	tabulátor
nebo	nebo	k8xC	nebo
zvládá	zvládat	k5eAaImIp3nS	zvládat
validovat	validovat	k5eAaPmF	validovat
dokument	dokument	k1gInSc4	dokument
podle	podle	k7c2	podle
předepsané	předepsaný	k2eAgFnSc2d1	předepsaná
specifikace	specifikace	k1gFnSc2	specifikace
<g/>
.	.	kIx.	.
</s>
<s>
Takovými	takový	k3xDgInPc7	takový
editory	editor	k1gInPc7	editor
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Notepad	Notepad	k1gInSc4	Notepad
<g/>
++	++	k?	++
<g/>
,	,	kIx,	,
PSPad	PSPad	k1gInSc1	PSPad
<g/>
,	,	kIx,	,
Atom	atom	k1gInSc1	atom
nebo	nebo	k8xC	nebo
Sublime	Sublim	k1gInSc5	Sublim
Text	text	k1gInSc1	text
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
WYSIWYG	WYSIWYG	kA	WYSIWYG
editory	editor	k1gInPc1	editor
===	===	k?	===
</s>
</p>
<p>
<s>
WYSIWYG	WYSIWYG	kA	WYSIWYG
je	být	k5eAaImIp3nS	být
zkratka	zkratka	k1gFnSc1	zkratka
od	od	k7c2	od
anglického	anglický	k2eAgMnSc2d1	anglický
"	"	kIx"	"
<g/>
What	What	k1gInSc1	What
you	you	k?	you
see	see	k?	see
is	is	k?	is
what	what	k1gInSc1	what
you	you	k?	you
get	get	k?	get
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
Co	co	k3yInSc1	co
vidíš	vidět	k5eAaImIp2nS	vidět
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
dostaneš	dostat	k5eAaPmIp2nS	dostat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Editory	editor	k1gInPc1	editor
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
pracují	pracovat	k5eAaImIp3nP	pracovat
na	na	k7c6	na
opačném	opačný	k2eAgInSc6d1	opačný
principu	princip	k1gInSc6	princip
než	než	k8xS	než
textové	textový	k2eAgInPc1d1	textový
editory	editor	k1gInPc1	editor
–	–	k?	–
ve	v	k7c6	v
WYSIWYG	WYSIWYG	kA	WYSIWYG
editoru	editor	k1gInSc6	editor
pracujete	pracovat	k5eAaImIp2nP	pracovat
přímo	přímo	k6eAd1	přímo
s	s	k7c7	s
již	již	k6eAd1	již
hotovou	hotový	k2eAgFnSc7d1	hotová
stránkou	stránka	k1gFnSc7	stránka
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
neplatí	platit	k5eNaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
uživatel	uživatel	k1gMnSc1	uživatel
tohoto	tento	k3xDgInSc2	tento
editoru	editor	k1gInSc2	editor
musel	muset	k5eAaImAgInS	muset
znát	znát	k5eAaImF	znát
jazyk	jazyk	k1gInSc1	jazyk
HTML	HTML	kA	HTML
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
WYSIWYG	WYSIWYG	kA	WYSIWYG
editoru	editor	k1gInSc6	editor
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
uživatel	uživatel	k1gMnSc1	uživatel
poskládat	poskládat	k5eAaPmF	poskládat
stránku	stránka	k1gFnSc4	stránka
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zlíbí	zlíbit	k5eAaPmIp3nS	zlíbit
<g/>
,	,	kIx,	,
a	a	k8xC	a
program	program	k1gInSc1	program
následně	následně	k6eAd1	následně
vygeneruje	vygenerovat	k5eAaPmIp3nS	vygenerovat
požadovaný	požadovaný	k2eAgInSc4d1	požadovaný
kód	kód	k1gInSc4	kód
HTML	HTML	kA	HTML
<g/>
.	.	kIx.	.
</s>
<s>
Takovými	takový	k3xDgInPc7	takový
editory	editor	k1gInPc7	editor
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Adobe	Adobe	kA	Adobe
Dreamweaver	Dreamweavra	k1gFnPc2	Dreamweavra
nebo	nebo	k8xC	nebo
Microsoft	Microsoft	kA	Microsoft
Expression	Expression	k1gInSc1	Expression
Web	web	k1gInSc1	web
(	(	kIx(	(
<g/>
novější	nový	k2eAgFnSc1d2	novější
verze	verze	k1gFnSc1	verze
Microsoft	Microsoft	kA	Microsoft
FrontPage	FrontPage	k1gFnSc1	FrontPage
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Programy	program	k1gInPc1	program
pro	pro	k7c4	pro
zpracování	zpracování	k1gNnSc4	zpracování
HTML	HTML	kA	HTML
==	==	k?	==
</s>
</p>
<p>
<s>
Programy	program	k1gInPc1	program
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
prezentovat	prezentovat	k5eAaBmF	prezentovat
dokument	dokument	k1gInSc4	dokument
na	na	k7c6	na
zobrazovacím	zobrazovací	k2eAgNnSc6d1	zobrazovací
zařízení	zařízení	k1gNnSc6	zařízení
–	–	k?	–
nejčastěji	často	k6eAd3	často
na	na	k7c6	na
obrazovce	obrazovka	k1gFnSc6	obrazovka
počítače	počítač	k1gInSc2	počítač
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
webové	webový	k2eAgInPc1d1	webový
prohlížeče	prohlížeč	k1gInPc1	prohlížeč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Parsování	Parsování	k1gNnSc1	Parsování
v	v	k7c6	v
prohlížečích	prohlížeč	k1gInPc6	prohlížeč
===	===	k?	===
</s>
</p>
<p>
<s>
Postup	postup	k1gInSc1	postup
zpracování	zpracování	k1gNnSc2	zpracování
HTML	HTML	kA	HTML
kódu	kód	k1gInSc2	kód
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
vykreslení	vykreslení	k1gNnSc4	vykreslení
uživateli	uživatel	k1gMnSc3	uživatel
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
parsování	parsování	k1gNnSc1	parsování
<g/>
.	.	kIx.	.
</s>
<s>
Správný	správný	k2eAgInSc1d1	správný
postup	postup	k1gInSc1	postup
zpracování	zpracování	k1gNnSc2	zpracování
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
webových	webový	k2eAgInPc2d1	webový
prohlížečů	prohlížeč	k1gInPc2	prohlížeč
velice	velice	k6eAd1	velice
důležitý	důležitý	k2eAgInSc1d1	důležitý
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stránka	stránka	k1gFnSc1	stránka
uživateli	uživatel	k1gMnSc3	uživatel
vykreslila	vykreslit	k5eAaPmAgFnS	vykreslit
správně	správně	k6eAd1	správně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dokument	dokument	k1gInSc1	dokument
je	být	k5eAaImIp3nS	být
nejprve	nejprve	k6eAd1	nejprve
prohlížečem	prohlížeč	k1gMnSc7	prohlížeč
načítán	načítán	k2eAgMnSc1d1	načítán
a	a	k8xC	a
rozkládán	rozkládán	k2eAgMnSc1d1	rozkládán
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
syntaktická	syntaktický	k2eAgFnSc1d1	syntaktická
analýza	analýza	k1gFnSc1	analýza
<g/>
)	)	kIx)	)
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
prvky	prvek	k1gInPc4	prvek
podle	podle	k7c2	podle
definic	definice	k1gFnPc2	definice
DTD	DTD	kA	DTD
(	(	kIx(	(
<g/>
Document	Document	k1gMnSc1	Document
Type	typ	k1gInSc5	typ
Definition	Definition	k1gInSc4	Definition
<g/>
)	)	kIx)	)
určujících	určující	k2eAgFnPc2d1	určující
značky	značka	k1gFnPc4	značka
a	a	k8xC	a
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
lze	lze	k6eAd1	lze
používat	používat	k5eAaImF	používat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každému	každý	k3xTgInSc3	každý
prvku	prvek	k1gInSc3	prvek
je	být	k5eAaImIp3nS	být
poté	poté	k6eAd1	poté
přiřazen	přiřazen	k2eAgInSc1d1	přiřazen
styl	styl	k1gInSc1	styl
(	(	kIx(	(
<g/>
způsob	způsob	k1gInSc1	způsob
zobrazení	zobrazení	k1gNnSc2	zobrazení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Styly	styl	k1gInPc1	styl
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
předepsány	předepsat	k5eAaPmNgFnP	předepsat
přímo	přímo	k6eAd1	přímo
programátorem	programátor	k1gInSc7	programátor
ve	v	k7c6	v
zdrojovém	zdrojový	k2eAgInSc6d1	zdrojový
kódu	kód	k1gInSc6	kód
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
stylů	styl	k1gInPc2	styl
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nejsou	být	k5eNaImIp3nP	být
předepsány	předepsán	k2eAgInPc1d1	předepsán
<g/>
,	,	kIx,	,
doplní	doplnit	k5eAaPmIp3nS	doplnit
prohlížeč	prohlížeč	k1gMnSc1	prohlížeč
podle	podle	k7c2	podle
implicitního	implicitní	k2eAgInSc2d1	implicitní
stylu	styl	k1gInSc2	styl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
předepsán	předepsat	k5eAaPmNgInS	předepsat
ve	v	k7c6	v
specifikaci	specifikace	k1gFnSc6	specifikace
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
prohlížeče	prohlížeč	k1gInPc1	prohlížeč
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
uživateli	uživatel	k1gMnPc7	uživatel
implicitní	implicitní	k2eAgFnSc2d1	implicitní
styly	styl	k1gInPc1	styl
definovat	definovat	k5eAaBmF	definovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
dokument	dokument	k1gInSc4	dokument
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
aplikován	aplikován	k2eAgInSc4d1	aplikován
předepsaný	předepsaný	k2eAgInSc4d1	předepsaný
kód	kód	k1gInSc4	kód
skriptovacích	skriptovací	k2eAgInPc2d1	skriptovací
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
volány	volán	k2eAgFnPc4d1	volána
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
provedeny	proveden	k2eAgInPc4d1	proveden
příkazy	příkaz	k1gInPc4	příkaz
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
spuštěno	spuštěn	k2eAgNnSc1d1	spuštěno
sledování	sledování	k1gNnSc1	sledování
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc1	jejich
volání	volání	k1gNnSc1	volání
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
provádění	provádění	k1gNnSc4	provádění
jejich	jejich	k3xOp3gFnSc2	jejich
smyčky	smyčka	k1gFnSc2	smyčka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Takto	takto	k6eAd1	takto
zpracovaný	zpracovaný	k2eAgInSc1d1	zpracovaný
dokument	dokument	k1gInSc1	dokument
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
vykreslen	vykreslit	k5eAaPmNgMnS	vykreslit
uživateli	uživatel	k1gMnPc7	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
prohlížeče	prohlížeč	k1gInPc1	prohlížeč
však	však	k9	však
podporují	podporovat	k5eAaImIp3nP	podporovat
postupné	postupný	k2eAgNnSc4d1	postupné
vykreslování	vykreslování	k1gNnSc4	vykreslování
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
je	být	k5eAaImIp3nS	být
dokument	dokument	k1gInSc1	dokument
postupně	postupně	k6eAd1	postupně
vykreslován	vykreslovat	k5eAaImNgInS	vykreslovat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
parsování	parsování	k1gNnSc2	parsování
<g/>
.	.	kIx.	.
</s>
<s>
Uživateli	uživatel	k1gMnSc3	uživatel
se	se	k3xPyFc4	se
tak	tak	k9	tak
HTML	HTML	kA	HTML
stránka	stránka	k1gFnSc1	stránka
postupně	postupně	k6eAd1	postupně
rýsuje	rýsovat	k5eAaImIp3nS	rýsovat
před	před	k7c7	před
očima	oko	k1gNnPc7	oko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Režimy	režim	k1gInPc1	režim
prohlížečů	prohlížeč	k1gInPc2	prohlížeč
===	===	k?	===
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
prohlížeče	prohlížeč	k1gInPc1	prohlížeč
pracují	pracovat	k5eAaImIp3nP	pracovat
obecně	obecně	k6eAd1	obecně
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
základních	základní	k2eAgInPc6d1	základní
režimech	režim	k1gInPc6	režim
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Standardní	standardní	k2eAgInSc1d1	standardní
režim	režim	k1gInSc1	režim
–	–	k?	–
režim	režim	k1gInSc1	režim
snažící	snažící	k2eAgInSc1d1	snažící
se	se	k3xPyFc4	se
dodržovat	dodržovat	k5eAaImF	dodržovat
definované	definovaný	k2eAgInPc4d1	definovaný
standardy	standard	k1gInPc4	standard
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Quirk	Quirk	k1gInSc1	Quirk
mód	mód	k1gInSc1	mód
–	–	k?	–
režim	režim	k1gInSc4	režim
zaměřený	zaměřený	k2eAgInSc4d1	zaměřený
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
zpětnou	zpětný	k2eAgFnSc4d1	zpětná
kompatibilitu	kompatibilita	k1gFnSc4	kompatibilita
<g/>
,	,	kIx,	,
i	i	k8xC	i
pokud	pokud	k8xS	pokud
takové	takový	k3xDgNnSc1	takový
chování	chování	k1gNnSc1	chování
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
standardy	standard	k1gInPc7	standard
<g/>
.	.	kIx.	.
<g/>
Tyto	tento	k3xDgInPc4	tento
režimy	režim	k1gInPc4	režim
chování	chování	k1gNnSc2	chování
zavedl	zavést	k5eAaPmAgMnS	zavést
Internet	Internet	k1gInSc4	Internet
Explorer	Explorra	k1gFnPc2	Explorra
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
páté	pátý	k4xOgFnSc6	pátý
verzi	verze	k1gFnSc6	verze
právě	právě	k9	právě
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
zpětné	zpětný	k2eAgFnSc2d1	zpětná
kompatibility	kompatibilita	k1gFnSc2	kompatibilita
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
nové	nový	k2eAgFnSc2d1	nová
verze	verze	k1gFnSc2	verze
prohlížeče	prohlížeč	k1gMnSc2	prohlížeč
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
zobrazovaly	zobrazovat	k5eAaImAgFnP	zobrazovat
správně	správně	k6eAd1	správně
stránky	stránka	k1gFnPc4	stránka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
již	již	k6eAd1	již
existují	existovat	k5eAaImIp3nP	existovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
stránky	stránka	k1gFnPc4	stránka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
psány	psát	k5eAaImNgFnP	psát
podle	podle	k7c2	podle
nových	nový	k2eAgInPc2d1	nový
standardů	standard	k1gInPc2	standard
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc4	jaký
režim	režim	k1gInSc4	režim
(	(	kIx(	(
<g/>
a	a	k8xC	a
chování	chování	k1gNnSc1	chování
<g/>
)	)	kIx)	)
prohlížeč	prohlížeč	k1gMnSc1	prohlížeč
zvolí	zvolit	k5eAaPmIp3nS	zvolit
<g/>
,	,	kIx,	,
závisí	záviset	k5eAaImIp3nS	záviset
především	především	k9	především
na	na	k7c6	na
uvedení	uvedení	k1gNnSc6	uvedení
direktivy	direktiva	k1gFnSc2	direktiva
<	<	kIx(	<
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
DOCTYPE	DOCTYPE	kA	DOCTYPE
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
většina	většina	k1gFnSc1	většina
starších	starý	k2eAgFnPc2d2	starší
stránek	stránka	k1gFnPc2	stránka
ji	on	k3xPp3gFnSc4	on
vůbec	vůbec	k9	vůbec
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
na	na	k7c6	na
použitých	použitý	k2eAgInPc6d1	použitý
prvcích	prvek	k1gInPc6	prvek
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
specifikacemi	specifikace	k1gFnPc7	specifikace
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
verzí	verze	k1gFnPc2	verze
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
toto	tento	k3xDgNnSc4	tento
chování	chování	k1gNnSc4	chování
částečně	částečně	k6eAd1	částečně
převzaly	převzít	k5eAaPmAgFnP	převzít
další	další	k2eAgInPc4d1	další
prohlížeče	prohlížeč	k1gInPc4	prohlížeč
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Mozilla	Mozilla	k1gFnSc1	Mozilla
Firefox	Firefox	k1gInSc1	Firefox
nebo	nebo	k8xC	nebo
Opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c4	mezi
režimy	režim	k1gInPc4	režim
nejsou	být	k5eNaImIp3nP	být
natolik	natolik	k6eAd1	natolik
markantní	markantní	k2eAgFnPc1d1	markantní
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
nestandardního	standardní	k2eNgInSc2d1	nestandardní
režimu	režim	k1gInSc2	režim
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
výrobci	výrobce	k1gMnPc1	výrobce
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
prohlížečů	prohlížeč	k1gInPc2	prohlížeč
přizpůsobovali	přizpůsobovat	k5eAaImAgMnP	přizpůsobovat
definici	definice	k1gFnSc4	definice
HTML	HTML	kA	HTML
podle	podle	k7c2	podle
svých	svůj	k3xOyFgFnPc2	svůj
potřeb	potřeba	k1gFnPc2	potřeba
a	a	k8xC	a
prohlížeče	prohlížeč	k1gInSc2	prohlížeč
podporovaly	podporovat	k5eAaImAgInP	podporovat
nestandardní	standardní	k2eNgInPc1d1	nestandardní
prvky	prvek	k1gInPc1	prvek
a	a	k8xC	a
syntaxi	syntax	k1gFnSc6	syntax
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
těchto	tento	k3xDgInPc2	tento
"	"	kIx"	"
<g/>
vylepšení	vylepšení	k1gNnSc3	vylepšení
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
následně	následně	k6eAd1	následně
přejímána	přejímat	k5eAaImNgFnS	přejímat
do	do	k7c2	do
standardů	standard	k1gInPc2	standard
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
však	však	k9	však
byly	být	k5eAaImAgFnP	být
zase	zase	k9	zase
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
verzích	verze	k1gFnPc6	verze
vyřazeny	vyřazen	k2eAgInPc1d1	vyřazen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Budoucnost	budoucnost	k1gFnSc1	budoucnost
HTML	HTML	kA	HTML
==	==	k?	==
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2012	[number]	k4	2012
HTML	HTML	kA	HTML
Working	Working	k1gInSc4	Working
Group	Group	k1gInSc4	Group
uveřejnila	uveřejnit	k5eAaPmAgFnS	uveřejnit
svůj	svůj	k3xOyFgInSc4	svůj
záměr	záměr	k1gInSc4	záměr
o	o	k7c6	o
prodloužení	prodloužení	k1gNnSc6	prodloužení
vývoje	vývoj	k1gInSc2	vývoj
HTML	HTML	kA	HTML
minimálně	minimálně	k6eAd1	minimálně
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Plánu	plán	k1gInSc2	plán
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
si	se	k3xPyFc3	se
také	také	k6eAd1	také
stanovila	stanovit	k5eAaPmAgFnS	stanovit
nové	nový	k2eAgInPc4d1	nový
cíle	cíl	k1gInPc4	cíl
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
cyklu	cyklus	k1gInSc2	cyklus
vydávání	vydávání	k1gNnSc4	vydávání
nových	nový	k2eAgFnPc2d1	nová
verzí	verze	k1gFnPc2	verze
HTML	HTML	kA	HTML
a	a	k8xC	a
milníky	milník	k1gInPc1	milník
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgMnPc2	jenž
chce	chtít	k5eAaImIp3nS	chtít
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Specifikace	specifikace	k1gFnSc1	specifikace
HTML5	HTML5	k1gFnSc1	HTML5
podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
plánu	plán	k1gInSc2	plán
měla	mít	k5eAaImAgFnS	mít
vyjít	vyjít	k5eAaPmF	vyjít
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
čtvrtletí	čtvrtletí	k1gNnSc6	čtvrtletí
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
"	"	kIx"	"
<g/>
Plán	plán	k1gInSc1	plán
2014	[number]	k4	2014
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
specifikace	specifikace	k1gFnSc1	specifikace
HTML	HTML	kA	HTML
5.1	[number]	k4	5.1
by	by	kYmCp3nS	by
pak	pak	k9	pak
měla	mít	k5eAaImAgFnS	mít
vyjít	vyjít	k5eAaPmF	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgInSc2	tento
cyklu	cyklus	k1gInSc2	cyklus
vývoje	vývoj	k1gInSc2	vývoj
HTML	HTML	kA	HTML
Working	Working	k1gInSc1	Working
Group	Group	k1gInSc1	Group
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
stanoví	stanovit	k5eAaPmIp3nS	stanovit
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
prvky	prvek	k1gInPc1	prvek
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgInPc1d1	vhodný
ke	k	k7c3	k
splnění	splnění	k1gNnSc3	splnění
výstupních	výstupní	k2eAgNnPc2d1	výstupní
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
,2014	,2014	k4	,2014
</s>
</p>
<p>
<s>
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
specifikaci	specifikace	k1gFnSc4	specifikace
HTML	HTML	kA	HTML
5.1	[number]	k4	5.1
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
právě	právě	k6eAd1	právě
tyto	tento	k3xDgInPc4	tento
vhodné	vhodný	k2eAgInPc4d1	vhodný
prvky	prvek	k1gInPc4	prvek
a	a	k8xC	a
která	který	k3yIgFnSc1	který
vynechává	vynechávat	k5eAaImIp3nS	vynechávat
zbývající	zbývající	k2eAgInPc4d1	zbývající
nevhodné	vhodný	k2eNgInPc4d1	nevhodný
prvky	prvek	k1gInPc4	prvek
<g/>
,2014	,2014	k4	,2014
</s>
</p>
<p>
<s>
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
návrh	návrh	k1gInSc1	návrh
HTML	HTML	kA	HTML
5.2	[number]	k4	5.2
<g/>
.	.	kIx.	.
</s>
<s>
Nevhodné	vhodný	k2eNgInPc4d1	nevhodný
prvky	prvek	k1gInPc4	prvek
z	z	k7c2	z
HTML	HTML	kA	HTML
5.1	[number]	k4	5.1
a	a	k8xC	a
zamýšlené	zamýšlený	k2eAgInPc1d1	zamýšlený
prvky	prvek	k1gInPc1	prvek
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
návrhu	návrh	k1gInSc6	návrh
začleněny	začleněn	k2eAgInPc1d1	začleněn
<g/>
.2015	.2015	k4	.2015
<g/>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
počáteční	počáteční	k2eAgFnSc1d1	počáteční
fáze	fáze	k1gFnSc1	fáze
návrhu	návrh	k1gInSc2	návrh
HTML	HTML	kA	HTML
5.2	[number]	k4	5.2
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fáze	fáze	k1gFnSc2	fáze
vývoje	vývoj	k1gInSc2	vývoj
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
verzí	verze	k1gFnPc2	verze
HTML	HTML	kA	HTML
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
W3C	W3C	k1gFnSc2	W3C
Process	Process	k1gInSc1	Process
Document	Document	k1gInSc1	Document
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Working	Working	k1gInSc1	Working
Draft	draft	k1gInSc1	draft
(	(	kIx(	(
<g/>
WD	WD	kA	WD
nebo	nebo	k8xC	nebo
FPWD	FPWD	kA	FPWD
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pracovní	pracovní	k2eAgFnSc1d1	pracovní
verze	verze	k1gFnSc1	verze
dokumentu	dokument	k1gInSc2	dokument
publikované	publikovaný	k2eAgFnSc2d1	publikovaná
ke	k	k7c3	k
zrevidování	zrevidování	k1gNnSc3	zrevidování
komunitou	komunita	k1gFnSc7	komunita
a	a	k8xC	a
veřejností	veřejnost	k1gFnSc7	veřejnost
během	během	k7c2	během
vypracovávání	vypracovávání	k1gNnSc2	vypracovávání
specifikace	specifikace	k1gFnSc2	specifikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Last	Last	k2eAgInSc1d1	Last
Call	Call	k1gInSc1	Call
Working	Working	k1gInSc1	Working
Draft	draft	k1gInSc1	draft
(	(	kIx(	(
<g/>
LC	LC	kA	LC
nebo	nebo	k8xC	nebo
LCWD	LCWD	kA	LCWD
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vydáním	vydání	k1gNnSc7	vydání
tohoto	tento	k3xDgInSc2	tento
dokumentu	dokument	k1gInSc2	dokument
pracovní	pracovní	k2eAgFnSc1d1	pracovní
skupina	skupina	k1gFnSc1	skupina
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
považuje	považovat	k5eAaImIp3nS	považovat
specifikaci	specifikace	k1gFnSc4	specifikace
za	za	k7c4	za
kompletní	kompletní	k2eAgFnSc4d1	kompletní
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
chyby	chyba	k1gFnPc1	chyba
za	za	k7c4	za
vyřešené	vyřešený	k2eAgNnSc4d1	vyřešené
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
specifikace	specifikace	k1gFnSc1	specifikace
se	se	k3xPyFc4	se
přesune	přesunout	k5eAaPmIp3nS	přesunout
do	do	k7c2	do
fáze	fáze	k1gFnSc2	fáze
Candidate	Candidat	k1gInSc5	Candidat
Recommendation	Recommendation	k1gInSc4	Recommendation
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
nenaleznou	nalézt	k5eNaBmIp3nP	nalézt
závažné	závažný	k2eAgFnPc1d1	závažná
chyby	chyba	k1gFnPc1	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrazená	vyhrazený	k2eAgFnSc1d1	vyhrazená
doba	doba	k1gFnSc1	doba
platnosti	platnost	k1gFnSc2	platnost
tohoto	tento	k3xDgInSc2	tento
dokumentu	dokument	k1gInSc2	dokument
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgFnSc7d1	poslední
šancí	šance	k1gFnSc7	šance
k	k	k7c3	k
nalezení	nalezení	k1gNnSc3	nalezení
chyb	chyba	k1gFnPc2	chyba
před	před	k7c7	před
přechodem	přechod	k1gInSc7	přechod
k	k	k7c3	k
CR	cr	k0	cr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Candidate	Candidat	k1gMnSc5	Candidat
Recommendation	Recommendation	k1gInSc1	Recommendation
(	(	kIx(	(
<g/>
CR	cr	k0	cr
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dokument	dokument	k1gInSc1	dokument
publikovaný	publikovaný	k2eAgInSc1d1	publikovaný
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
zkušeností	zkušenost	k1gFnPc2	zkušenost
pro	pro	k7c4	pro
implementaci	implementace	k1gFnSc4	implementace
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
již	již	k6eAd1	již
byl	být	k5eAaImAgInS	být
dostatečně	dostatečně	k6eAd1	dostatečně
upraven	upravit	k5eAaPmNgInS	upravit
a	a	k8xC	a
splňuje	splňovat	k5eAaImIp3nS	splňovat
technické	technický	k2eAgInPc4d1	technický
požadavky	požadavek	k1gInPc4	požadavek
pracovní	pracovní	k2eAgFnSc2d1	pracovní
skupiny	skupina	k1gFnSc2	skupina
k	k	k7c3	k
implementaci	implementace	k1gFnSc3	implementace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proposed	Proposed	k1gInSc1	Proposed
Recommendation	Recommendation	k1gInSc1	Recommendation
(	(	kIx(	(
<g/>
PR	pr	k0	pr
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vyspělá	vyspělý	k2eAgFnSc1d1	vyspělá
technická	technický	k2eAgFnSc1d1	technická
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
provedení	provedení	k1gNnSc6	provedení
oprav	oprava	k1gFnPc2	oprava
<g/>
,	,	kIx,	,
komplexního	komplexní	k2eAgNnSc2d1	komplexní
testování	testování	k1gNnSc2	testování
a	a	k8xC	a
schopnosti	schopnost	k1gFnSc2	schopnost
implementace	implementace	k1gFnSc1	implementace
odeslaná	odeslaný	k2eAgFnSc1d1	odeslaná
ke	k	k7c3	k
schválení	schválení	k1gNnSc3	schválení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Recommendation	Recommendation	k1gInSc1	Recommendation
(	(	kIx(	(
<g/>
REC	REC	kA	REC
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Specifikace	specifikace	k1gFnSc1	specifikace
<g/>
,	,	kIx,	,
standard	standard	k1gInSc1	standard
nebo	nebo	k8xC	nebo
soubor	soubor	k1gInSc1	soubor
směrnic	směrnice	k1gFnPc2	směrnice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
schváleny	schválit	k5eAaPmNgFnP	schválit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
doporučeno	doporučit	k5eAaPmNgNnS	doporučit
její	její	k3xOp3gNnSc1	její
úplné	úplný	k2eAgNnSc1d1	úplné
nasazení	nasazení	k1gNnSc1	nasazení
a	a	k8xC	a
používání	používání	k1gNnSc1	používání
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgFnSc1d1	poslední
fáze	fáze	k1gFnSc1	fáze
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
již	již	k6eAd1	již
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
nijak	nijak	k6eAd1	nijak
opravována	opravován	k2eAgFnSc1d1	opravována
nebo	nebo	k8xC	nebo
měněna	měněn	k2eAgFnSc1d1	měněna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Milníky	milník	k1gInPc4	milník
budoucího	budoucí	k2eAgInSc2d1	budoucí
vývoje	vývoj	k1gInSc2	vývoj
HTML	HTML	kA	HTML
===	===	k?	===
</s>
</p>
<p>
<s>
Plán	plán	k1gInSc1	plán
budoucího	budoucí	k2eAgInSc2d1	budoucí
vývoje	vývoj	k1gInSc2	vývoj
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
HTTP	HTTP	kA	HTTP
</s>
</p>
<p>
<s>
XML	XML	kA	XML
</s>
</p>
<p>
<s>
XHTML	XHTML	kA	XHTML
</s>
</p>
<p>
<s>
SGML	SGML	kA	SGML
</s>
</p>
<p>
<s>
Deklarace	deklarace	k1gFnSc1	deklarace
typu	typ	k1gInSc2	typ
dokumentu	dokument	k1gInSc2	dokument
</s>
</p>
<p>
<s>
DTD	DTD	kA	DTD
</s>
</p>
<p>
<s>
DOM	DOM	k?	DOM
</s>
</p>
<p>
<s>
HTML	HTML	kA	HTML
entita	entita	k1gFnSc1	entita
</s>
</p>
<p>
<s>
HTML5	HTML5	k4	HTML5
</s>
</p>
<p>
<s>
W3C	W3C	k4	W3C
</s>
</p>
<p>
<s>
WHATWG	WHATWG	kA	WHATWG
</s>
</p>
<p>
<s>
HTML	HTML	kA	HTML
Working	Working	k1gInSc1	Working
Group	Group	k1gInSc1	Group
</s>
</p>
<p>
<s>
Dynamická	dynamický	k2eAgFnSc1d1	dynamická
webová	webový	k2eAgFnSc1d1	webová
stránka	stránka	k1gFnSc1	stránka
</s>
</p>
<p>
<s>
Mikroformát	Mikroformát	k1gMnSc1	Mikroformát
</s>
</p>
<p>
<s>
CSS	CSS	kA	CSS
</s>
</p>
<p>
<s>
JavaScript	JavaScript	k1gMnSc1	JavaScript
</s>
</p>
<p>
<s>
Webový	webový	k2eAgMnSc1d1	webový
prohlížeč	prohlížeč	k1gMnSc1	prohlížeč
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
HyperText	hypertext	k1gInSc1	hypertext
Markup	Markup	k1gInSc4	Markup
Language	language	k1gFnSc2	language
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
HTML	HTML	kA	HTML
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
HTML	HTML	kA	HTML
ve	v	k7c6	v
Wikiknihách	Wikiknih	k1gInPc6	Wikiknih
</s>
</p>
<p>
<s>
Výukový	výukový	k2eAgInSc1d1	výukový
kurs	kurs	k1gInSc1	kurs
HTML	HTML	kA	HTML
ve	v	k7c6	v
Wikiverzitě	Wikiverzita	k1gFnSc6	Wikiverzita
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
jazyka	jazyk	k1gInSc2	jazyk
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Technická	technický	k2eAgFnSc1d1	technická
specifikace	specifikace	k1gFnSc1	specifikace
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Stránky	stránka	k1gFnPc1	stránka
W3C	W3C	k1gFnSc2	W3C
o	o	k7c6	o
značkovacích	značkovací	k2eAgInPc6d1	značkovací
jazycích	jazyk	k1gInPc6	jazyk
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Jazyk	jazyk	k1gInSc1	jazyk
webov	webov	k1gInSc1	webov
HTML	HTML	kA	HTML
má	mít	k5eAaImIp3nS	mít
novú	novú	k?	novú
verziu	verzium	k1gNnSc6	verzium
<g/>
,	,	kIx,	,
HTML	HTML	kA	HTML
5.1	[number]	k4	5.1
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Wiki	Wik	k1gMnPc1	Wik
o	o	k7c6	o
značkovacích	značkovací	k2eAgInPc6d1	značkovací
jazycích	jazyk	k1gInPc6	jazyk
provozovaná	provozovaný	k2eAgFnSc1d1	provozovaná
WHATWG	WHATWG	kA	WHATWG
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
HTML	HTML	kA	HTML
Living	Living	k1gInSc1	Living
Standard	standard	k1gInSc1	standard
–	–	k?	–
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
a	a	k8xC	a
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
specifikace	specifikace	k1gFnSc1	specifikace
všech	všecek	k3xTgFnPc2	všecek
technologií	technologie	k1gFnPc2	technologie
určených	určený	k2eAgFnPc2d1	určená
pro	pro	k7c4	pro
používání	používání	k1gNnSc4	používání
internetu	internet	k1gInSc2	internet
(	(	kIx(	(
<g/>
HTML	HTML	kA	HTML
<g/>
,	,	kIx,	,
CSS	CSS	kA	CSS
<g/>
,	,	kIx,	,
HTTP	HTTP	kA	HTTP
<g/>
,	,	kIx,	,
MIME	mim	k1gMnSc5	mim
<g/>
,	,	kIx,	,
URL	URL	kA	URL
<g/>
,	,	kIx,	,
DOM	DOM	k?	DOM
<g/>
,	,	kIx,	,
Unicode	Unicod	k1gMnSc5	Unicod
<g/>
,	,	kIx,	,
XML	XML	kA	XML
<g/>
,	,	kIx,	,
SVG	SVG	kA	SVG
<g/>
,	,	kIx,	,
MathML	MathML	k1gMnPc1	MathML
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Plán	plán	k1gInSc1	plán
HTML	HTML	kA	HTML
Working	Working	k1gInSc1	Working
Group	Group	k1gInSc1	Group
–	–	k?	–
týká	týkat	k5eAaImIp3nS	týkat
se	se	k3xPyFc4	se
nového	nový	k2eAgInSc2d1	nový
cyklu	cyklus	k1gInSc2	cyklus
a	a	k8xC	a
periody	perioda	k1gFnSc2	perioda
vydávání	vydávání	k1gNnSc2	vydávání
dalších	další	k2eAgFnPc2d1	další
verzí	verze	k1gFnPc2	verze
HTML	HTML	kA	HTML
</s>
</p>
<p>
<s>
Interval	interval	k1gInSc1	interval
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
české	český	k2eAgFnSc2d1	Česká
stránky	stránka	k1gFnSc2	stránka
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
webdesignu	webdesigno	k1gNnSc3	webdesigno
a	a	k8xC	a
webdevelopmentu	webdevelopment	k1gInSc3	webdevelopment
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
Psát	psát	k5eAaImF	psát
Web	web	k1gInSc4	web
–	–	k?	–
Návod	návod	k1gInSc1	návod
na	na	k7c4	na
tvorbu	tvorba	k1gFnSc4	tvorba
HTML	HTML	kA	HTML
stránek	stránka	k1gFnPc2	stránka
</s>
</p>
<p>
<s>
HTML	HTML	kA	HTML
a	a	k8xC	a
CSS	CSS	kA	CSS
návody	návod	k1gInPc1	návod
</s>
</p>
