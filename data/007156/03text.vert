<s>
Tuky	tuk	k1gInPc1	tuk
jsou	být	k5eAaImIp3nP	být
estery	ester	k1gInPc4	ester
vyšších	vysoký	k2eAgFnPc2d2	vyšší
karboxylových	karboxylový	k2eAgFnPc2d1	karboxylová
(	(	kIx(	(
<g/>
mastných	mastný	k2eAgFnPc2d1	mastná
<g/>
)	)	kIx)	)
kyselin	kyselina	k1gFnPc2	kyselina
s	s	k7c7	s
trojsytným	trojsytný	k2eAgInSc7d1	trojsytný
alkoholem	alkohol	k1gInSc7	alkohol
glycerolem	glycerol	k1gInSc7	glycerol
<g/>
.	.	kIx.	.
</s>
<s>
Řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
lipidy	lipid	k1gInPc4	lipid
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
jsou	být	k5eAaImIp3nP	být
esterifikovány	esterifikovat	k5eAaPmNgFnP	esterifikovat
všechny	všechen	k3xTgFnPc1	všechen
tři	tři	k4xCgFnPc1	tři
hydroxyskupiny	hydroxyskupina	k1gFnPc1	hydroxyskupina
glycerolu	glycerol	k1gInSc2	glycerol
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
triacylglyceroly	triacylglycerol	k1gInPc4	triacylglycerol
<g/>
.	.	kIx.	.
</s>
<s>
Triacylglyceroly	Triacylglycerola	k1gFnPc1	Triacylglycerola
mohou	moct	k5eAaImIp3nP	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
tři	tři	k4xCgInPc4	tři
shodné	shodný	k2eAgInPc4d1	shodný
acylové	acylový	k2eAgInPc4d1	acylový
zbytky	zbytek	k1gInPc4	zbytek
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dvě	dva	k4xCgFnPc1	dva
stejné	stejný	k2eAgFnPc1d1	stejná
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
každá	každý	k3xTgFnSc1	každý
hydroxyskupina	hydroxyskupina	k1gFnSc1	hydroxyskupina
esterifikována	esterifikován	k2eAgFnSc1d1	esterifikován
rozdílnými	rozdílný	k2eAgFnPc7d1	rozdílná
kyselinami	kyselina	k1gFnPc7	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rostoucím	rostoucí	k2eAgInSc7d1	rostoucí
počtem	počet	k1gInSc7	počet
uvažovaných	uvažovaný	k2eAgFnPc2d1	uvažovaná
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
roste	růst	k5eAaImIp3nS	růst
i	i	k9	i
počet	počet	k1gInSc1	počet
možných	možný	k2eAgInPc2d1	možný
triacylglycerolů	triacylglycerol	k1gInPc2	triacylglycerol
<g/>
.	.	kIx.	.
</s>
<s>
Uvažujeme	uvažovat	k5eAaImIp1nP	uvažovat
<g/>
-li	i	k?	-li
50	[number]	k4	50
možných	možný	k2eAgFnPc2d1	možná
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
tuků	tuk	k1gInPc2	tuk
narůstá	narůstat	k5eAaImIp3nS	narůstat
k	k	k7c3	k
několika	několik	k4yIc3	několik
desítkám	desítka	k1gFnPc3	desítka
tisíců	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
přírodní	přírodní	k2eAgInPc1d1	přírodní
tuky	tuk	k1gInPc1	tuk
tvořeny	tvořit	k5eAaImNgInP	tvořit
hlavně	hlavně	k6eAd1	hlavně
třemi	tři	k4xCgFnPc7	tři
mastnými	mastný	k2eAgFnPc7d1	mastná
kyselinami	kyselina	k1gFnPc7	kyselina
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kyselinou	kyselina	k1gFnSc7	kyselina
olejovou	olejový	k2eAgFnSc7d1	olejová
<g/>
,	,	kIx,	,
linolovou	linolový	k2eAgFnSc7d1	linolová
a	a	k8xC	a
palmitovou	palmitový	k2eAgFnSc7d1	palmitová
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
poměrně	poměrně	k6eAd1	poměrně
častými	častý	k2eAgFnPc7d1	častá
kyselinami	kyselina	k1gFnPc7	kyselina
jsou	být	k5eAaImIp3nP	být
kyselina	kyselina	k1gFnSc1	kyselina
stearová	stearový	k2eAgFnSc1d1	stearová
<g/>
,	,	kIx,	,
arachová	arachová	k1gFnSc1	arachová
<g/>
,	,	kIx,	,
lignocerová	lignocerová	k1gFnSc1	lignocerová
<g/>
,	,	kIx,	,
myristová	myristová	k1gFnSc1	myristová
a	a	k8xC	a
různé	různý	k2eAgFnPc1d1	různá
transmastné	transmastný	k2eAgFnPc1d1	transmastný
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Vícenenasycené	Vícenenasycený	k2eAgFnPc1d1	Vícenenasycený
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
PUFA	PUFA	kA	PUFA
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
nejvíce	hodně	k6eAd3	hodně
obsaženy	obsáhnout	k5eAaPmNgInP	obsáhnout
v	v	k7c6	v
tucích	tuk	k1gInPc6	tuk
rostlinného	rostlinný	k2eAgInSc2d1	rostlinný
původu	původ	k1gInSc2	původ
a	a	k8xC	a
v	v	k7c6	v
rybím	rybí	k2eAgInSc6d1	rybí
tuku	tuk	k1gInSc6	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
snižovat	snižovat	k5eAaImF	snižovat
hladinu	hladina	k1gFnSc4	hladina
cholesterolu	cholesterol	k1gInSc2	cholesterol
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
efektivněji	efektivně	k6eAd2	efektivně
než	než	k8xS	než
MUFA	MUFA	kA	MUFA
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
snižovat	snižovat	k5eAaImF	snižovat
riziko	riziko	k1gNnSc1	riziko
vzniku	vznik	k1gInSc2	vznik
krevních	krevní	k2eAgFnPc2d1	krevní
sraženin	sraženina	k1gFnPc2	sraženina
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
významnou	významný	k2eAgFnSc4d1	významná
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
prevenci	prevence	k1gFnSc6	prevence
srdečně-cévních	srdečněévní	k2eAgNnPc2d1	srdečně-cévní
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
vícenenasycených	vícenenasycený	k2eAgFnPc2d1	vícenenasycený
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
patří	patřit	k5eAaImIp3nS	patřit
omega-	omega-	k?	omega-
<g/>
6	[number]	k4	6
a	a	k8xC	a
omega-	omega-	k?	omega-
<g/>
3	[number]	k4	3
nenasycené	nasycený	k2eNgFnPc1d1	nenasycená
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
kyseliny	kyselina	k1gFnSc2	kyselina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
naše	náš	k3xOp1gNnSc4	náš
zdraví	zdraví	k1gNnSc4	zdraví
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgFnPc1d1	důležitá
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
kyselin	kyselina	k1gFnPc2	kyselina
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
esenciálních	esenciální	k2eAgFnPc2d1	esenciální
(	(	kIx(	(
<g/>
nezbytných	zbytný	k2eNgFnPc2d1	zbytný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
takových	takový	k3xDgInPc2	takový
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
si	se	k3xPyFc3	se
náš	náš	k3xOp1gInSc1	náš
organizmus	organizmus	k1gInSc1	organizmus
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
vytvořit	vytvořit	k5eAaPmF	vytvořit
sám	sám	k3xTgMnSc1	sám
a	a	k8xC	a
musíme	muset	k5eAaImIp1nP	muset
je	on	k3xPp3gInPc4	on
proto	proto	k8xC	proto
dodávat	dodávat	k5eAaImF	dodávat
stravou	strava	k1gFnSc7	strava
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
kyselinu	kyselina	k1gFnSc4	kyselina
linolovou	linolový	k2eAgFnSc4d1	linolová
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc4d1	patřící
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
omega-	omega-	k?	omega-
<g/>
6	[number]	k4	6
nenasycených	nasycený	k2eNgFnPc2d1	nenasycená
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
,	,	kIx,	,
a	a	k8xC	a
např.	např.	kA	např.
o	o	k7c4	o
kyselinu	kyselina	k1gFnSc4	kyselina
alfa-linolenovou	alfainolenový	k2eAgFnSc4d1	alfa-linolenová
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc4d1	patřící
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
omega-	omega-	k?	omega-
<g/>
3	[number]	k4	3
nenasycených	nasycený	k2eNgFnPc2d1	nenasycená
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
linolové	linolový	k2eAgFnSc6d1	linolová
mastné	mastný	k2eAgFnSc6d1	mastná
kyselině	kyselina	k1gFnSc6	kyselina
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
řadu	řada	k1gFnSc4	řada
let	let	k1gInSc4	let
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
snižovat	snižovat	k5eAaImF	snižovat
hladinu	hladina	k1gFnSc4	hladina
celkového	celkový	k2eAgInSc2d1	celkový
i	i	k8xC	i
LDL	LDL	kA	LDL
cholesterolu	cholesterol	k1gInSc2	cholesterol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
popředí	popředí	k1gNnSc6	popředí
zájmu	zájem	k1gInSc2	zájem
odborníků	odborník	k1gMnPc2	odborník
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
omega-	omega-	k?	omega-
<g/>
3	[number]	k4	3
nenasycené	nasycený	k2eNgFnPc1d1	nenasycená
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgMnPc2	jenž
byl	být	k5eAaImAgInS	být
prokázán	prokázat	k5eAaPmNgInS	prokázat
příznivý	příznivý	k2eAgInSc1d1	příznivý
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
náš	náš	k3xOp1gInSc4	náš
srdečně-cévní	srdečněévní	k2eAgInSc4d1	srdečně-cévní
systém	systém	k1gInSc4	systém
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
např.	např.	kA	např.
snižovat	snižovat	k5eAaImF	snižovat
hladinu	hladina	k1gFnSc4	hladina
cholesterolu	cholesterol	k1gInSc2	cholesterol
<g/>
.	.	kIx.	.
</s>
<s>
Pozitivně	pozitivně	k6eAd1	pozitivně
působí	působit	k5eAaImIp3nS	působit
také	také	k9	také
mechanizmy	mechanizmus	k1gInPc4	mechanizmus
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nejsou	být	k5eNaImIp3nP	být
ještě	ještě	k6eAd1	ještě
zcela	zcela	k6eAd1	zcela
objasněny	objasněn	k2eAgFnPc1d1	objasněna
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
bychom	by	kYmCp1nP	by
měli	mít	k5eAaImAgMnP	mít
dbát	dbát	k5eAaImF	dbát
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
jich	on	k3xPp3gMnPc2	on
měli	mít	k5eAaImAgMnP	mít
ve	v	k7c6	v
stravě	strava	k1gFnSc6	strava
dostatek	dostatek	k1gInSc4	dostatek
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
omega-	omega-	k?	omega-
<g/>
3	[number]	k4	3
nenasycené	nasycený	k2eNgFnPc1d1	nenasycená
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
najdeme	najít	k5eAaPmIp1nP	najít
zejména	zejména	k9	zejména
v	v	k7c6	v
rybím	rybí	k2eAgInSc6d1	rybí
tuku	tuk	k1gInSc6	tuk
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
jiné	jiný	k2eAgNnSc4d1	jiné
složení	složení	k1gNnSc4	složení
než	než	k8xS	než
ostatní	ostatní	k2eAgInPc4d1	ostatní
živočišné	živočišný	k2eAgInPc4d1	živočišný
tuky	tuk	k1gInPc4	tuk
a	a	k8xC	a
převažují	převažovat	k5eAaImIp3nP	převažovat
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
prospěšné	prospěšný	k2eAgNnSc1d1	prospěšné
nenasycené	nasycený	k2eNgNnSc1d1	nenasycené
mastné	mastné	k1gNnSc1	mastné
kyseliny	kyselina	k1gFnSc2	kyselina
<g/>
,	,	kIx,	,
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
nejdůležitější	důležitý	k2eAgFnPc1d3	nejdůležitější
kyselina	kyselina	k1gFnSc1	kyselina
eikosapentaenová	eikosapentaenová	k1gFnSc1	eikosapentaenová
–	–	k?	–
EPA	EPA	kA	EPA
a	a	k8xC	a
kyselina	kyselina	k1gFnSc1	kyselina
dokosahexaenová	dokosahexaenová	k1gFnSc1	dokosahexaenová
–	–	k?	–
DHA	DHA	kA	DHA
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
rostlinných	rostlinný	k2eAgInPc6d1	rostlinný
olejích	olej	k1gInPc6	olej
<g/>
,	,	kIx,	,
ořechách	ořechách	k?	ořechách
aj.	aj.	kA	aj.
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Transmastné	Transmastný	k2eAgFnSc2d1	Transmastný
kyseliny	kyselina	k1gFnSc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Transizomery	Transizomera	k1gFnPc1	Transizomera
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
transmastné	transmastný	k2eAgFnSc2d1	transmastný
kyseliny	kyselina	k1gFnSc2	kyselina
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
TFA	TFA	kA	TFA
(	(	kIx(	(
<g/>
trans-fatty	transatt	k1gInPc1	trans-fatt
acids	acidsa	k1gFnPc2	acidsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c4	na
zvýšení	zvýšení	k1gNnSc4	zvýšení
hladiny	hladina	k1gFnSc2	hladina
cholesterolu	cholesterol	k1gInSc2	cholesterol
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
negativní	negativní	k2eAgInSc4d1	negativní
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
náš	náš	k3xOp1gInSc4	náš
srdečně-cévní	srdečněévní	k2eAgInSc4d1	srdečně-cévní
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Vznikaly	vznikat	k5eAaImAgFnP	vznikat
při	při	k7c6	při
starších	starý	k2eAgInPc6d2	starší
technologických	technologický	k2eAgInPc6d1	technologický
postupech	postup	k1gInPc6	postup
výroby	výroba	k1gFnSc2	výroba
tuků	tuk	k1gInPc2	tuk
ztužováním	ztužování	k1gNnSc7	ztužování
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
technologie	technologie	k1gFnSc1	technologie
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
tzv.	tzv.	kA	tzv.
tukové	tukový	k2eAgFnSc2d1	tuková
násady	násada	k1gFnSc2	násada
<g/>
.	.	kIx.	.
</s>
<s>
Tuková	tukový	k2eAgFnSc1d1	tuková
násada	násada	k1gFnSc1	násada
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
směs	směs	k1gFnSc1	směs
tuků	tuk	k1gInPc2	tuk
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
byl	být	k5eAaImAgMnS	být
následně	následně	k6eAd1	následně
vyroben	vyroben	k2eAgInSc4d1	vyroben
finální	finální	k2eAgInSc4d1	finální
výrobek	výrobek	k1gInSc4	výrobek
(	(	kIx(	(
<g/>
rostlinný	rostlinný	k2eAgInSc4d1	rostlinný
tuk	tuk	k1gInSc4	tuk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
tvořila	tvořit	k5eAaImAgFnS	tvořit
pouze	pouze	k6eAd1	pouze
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
surovin	surovina	k1gFnPc2	surovina
používaných	používaný	k2eAgFnPc2d1	používaná
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
tuků	tuk	k1gInPc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
moderních	moderní	k2eAgInPc2d1	moderní
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
tuků	tuk	k1gInPc2	tuk
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
běžně	běžně	k6eAd1	běžně
dostupné	dostupný	k2eAgFnPc1d1	dostupná
v	v	k7c6	v
obchodech	obchod	k1gInPc6	obchod
<g/>
,	,	kIx,	,
používán	používat	k5eAaImNgInS	používat
nový	nový	k2eAgInSc1d1	nový
výrobní	výrobní	k2eAgInSc1d1	výrobní
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
interesterifikace	interesterifikace	k1gFnSc1	interesterifikace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vzniku	vznik	k1gInSc3	vznik
transmastných	transmastný	k2eAgFnPc2d1	transmastný
kyselin	kyselina	k1gFnPc2	kyselina
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
zdrojem	zdroj	k1gInSc7	zdroj
těchto	tento	k3xDgFnPc2	tento
špatných	špatný	k2eAgFnPc2d1	špatná
transmastných	transmastný	k2eAgFnPc2d1	transmastný
kyselin	kyselina	k1gFnPc2	kyselina
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
některé	některý	k3yIgInPc4	některý
druhy	druh	k1gInPc4	druh
sladkého	sladký	k2eAgNnSc2d1	sladké
pečiva	pečivo	k1gNnSc2	pečivo
a	a	k8xC	a
zákusků	zákusek	k1gInPc2	zákusek
(	(	kIx(	(
<g/>
kam	kam	k6eAd1	kam
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k9	ještě
stále	stále	k6eAd1	stále
přidávány	přidáván	k2eAgInPc1d1	přidáván
ztužené	ztužený	k2eAgInPc1d1	ztužený
tuky	tuk	k1gInPc1	tuk
vyráběné	vyráběný	k2eAgInPc1d1	vyráběný
pro	pro	k7c4	pro
potravinářské	potravinářský	k2eAgInPc4d1	potravinářský
účely	účel	k1gInPc4	účel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokrmy	pokrm	k1gInPc1	pokrm
rychlého	rychlý	k2eAgNnSc2d1	rychlé
občerstvení	občerstvení	k1gNnSc2	občerstvení
a	a	k8xC	a
živočišné	živočišný	k2eAgInPc4d1	živočišný
tuky	tuk	k1gInPc4	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Transmastné	Transmastný	k2eAgFnPc1d1	Transmastný
kyseliny	kyselina	k1gFnPc1	kyselina
vznikají	vznikat	k5eAaImIp3nP	vznikat
také	také	k9	také
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
–	–	k?	–
např.	např.	kA	např.
v	v	k7c6	v
trávicím	trávicí	k2eAgNnSc6d1	trávicí
ústrojí	ústrojí	k1gNnSc6	ústrojí
přežvýkavců	přežvýkavec	k1gMnPc2	přežvýkavec
–	–	k?	–
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
přirozeně	přirozeně	k6eAd1	přirozeně
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
mléčném	mléčný	k2eAgInSc6d1	mléčný
tuku	tuk	k1gInSc6	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
skupenství	skupenství	k1gNnSc2	skupenství
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
pevné	pevný	k2eAgInPc1d1	pevný
tuky	tuk	k1gInPc1	tuk
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
převažují	převažovat	k5eAaImIp3nP	převažovat
zejména	zejména	k9	zejména
nasycené	nasycený	k2eAgFnPc1d1	nasycená
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
,	,	kIx,	,
a	a	k8xC	a
oleje	olej	k1gInPc1	olej
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
skupenství	skupenství	k1gNnSc1	skupenství
je	být	k5eAaImIp3nS	být
kapalné	kapalný	k2eAgNnSc1d1	kapalné
a	a	k8xC	a
které	který	k3yRgNnSc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
nenasycených	nasycený	k2eNgFnPc2d1	nenasycená
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
základním	základní	k2eAgNnSc7d1	základní
dělením	dělení	k1gNnSc7	dělení
je	být	k5eAaImIp3nS	být
dělení	dělení	k1gNnSc1	dělení
na	na	k7c4	na
tuky	tuk	k1gInPc4	tuk
rostlinné	rostlinný	k2eAgInPc4d1	rostlinný
a	a	k8xC	a
tuky	tuk	k1gInPc1	tuk
živočišné	živočišný	k2eAgInPc1d1	živočišný
<g/>
.	.	kIx.	.
</s>
<s>
Rostlinné	rostlinný	k2eAgInPc1d1	rostlinný
tuky	tuk	k1gInPc1	tuk
jsou	být	k5eAaImIp3nP	být
získávány	získávat	k5eAaImNgInP	získávat
z	z	k7c2	z
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
hromadí	hromadit	k5eAaImIp3nP	hromadit
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
plodech	plod	k1gInPc6	plod
<g/>
,	,	kIx,	,
semenech	semeno	k1gNnPc6	semeno
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
tuky	tuk	k1gInPc4	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
tuky	tuk	k1gInPc4	tuk
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
např.	např.	kA	např.
dužnina	dužnina	k1gFnSc1	dužnina
a	a	k8xC	a
jádra	jádro	k1gNnPc1	jádro
palmy	palma	k1gFnSc2	palma
olejné	olejný	k2eAgFnSc2d1	olejná
<g/>
,	,	kIx,	,
kokos	kokos	k1gInSc1	kokos
<g/>
,	,	kIx,	,
olejniny	olejnina	k1gFnPc1	olejnina
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
řepka	řepka	k1gFnSc1	řepka
<g/>
,	,	kIx,	,
sója	sója	k1gFnSc1	sója
<g/>
,	,	kIx,	,
slunečnice	slunečnice	k1gFnSc1	slunečnice
<g/>
,	,	kIx,	,
sezam	sezam	k1gInSc1	sezam
<g/>
,	,	kIx,	,
podzemnice	podzemnice	k1gFnSc1	podzemnice
<g/>
,	,	kIx,	,
světlice	světlice	k1gFnSc1	světlice
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
Mezi	mezi	k7c4	mezi
potraviny	potravina	k1gFnPc4	potravina
vyrobené	vyrobený	k2eAgFnPc4d1	vyrobená
z	z	k7c2	z
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
tuků	tuk	k1gInPc2	tuk
řadíme	řadit	k5eAaImIp1nP	řadit
zejména	zejména	k9	zejména
<g/>
:	:	kIx,	:
rostlinné	rostlinný	k2eAgInPc1d1	rostlinný
oleje	olej	k1gInPc1	olej
rostlinné	rostlinný	k2eAgInPc1d1	rostlinný
tuky	tuk	k1gInPc1	tuk
(	(	kIx(	(
<g/>
margaríny	margarín	k1gInPc1	margarín
<g/>
)	)	kIx)	)
pokrmové	pokrmový	k2eAgInPc1d1	pokrmový
tuky	tuk	k1gInPc1	tuk
emulgované	emulgovaný	k2eAgInPc1d1	emulgovaný
tuky	tuk	k1gInPc4	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Převážná	převážný	k2eAgFnSc1d1	převážná
většina	většina	k1gFnSc1	většina
tuků	tuk	k1gInPc2	tuk
rostlinného	rostlinný	k2eAgInSc2d1	rostlinný
původu	původ	k1gInSc2	původ
má	mít	k5eAaImIp3nS	mít
ze	z	k7c2	z
zdravotního	zdravotní	k2eAgNnSc2d1	zdravotní
hlediska	hledisko	k1gNnSc2	hledisko
velmi	velmi	k6eAd1	velmi
vhodné	vhodný	k2eAgNnSc4d1	vhodné
složení	složení	k1gNnSc4	složení
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
vícenenasycené	vícenenasycený	k2eAgFnPc1d1	vícenenasycený
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
příznivější	příznivý	k2eAgInSc4d2	příznivější
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
naše	náš	k3xOp1gNnSc4	náš
zdraví	zdraví	k1gNnSc4	zdraví
než	než	k8xS	než
tuky	tuk	k1gInPc4	tuk
živočišné	živočišný	k2eAgInPc4d1	živočišný
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nS	tvořit
například	například	k6eAd1	například
tuk	tuk	k1gInSc1	tuk
kokosový	kokosový	k2eAgInSc1d1	kokosový
a	a	k8xC	a
palmojádrový	palmojádrový	k2eAgInSc1d1	palmojádrový
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
převažují	převažovat	k5eAaImIp3nP	převažovat
nasycené	nasycený	k2eAgFnPc1d1	nasycená
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
nevhodné	vhodný	k2eNgInPc4d1	nevhodný
pro	pro	k7c4	pro
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Kokosový	kokosový	k2eAgInSc4d1	kokosový
a	a	k8xC	a
palmojádrový	palmojádrový	k2eAgInSc4d1	palmojádrový
tuk	tuk	k1gInSc4	tuk
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
celkovou	celkový	k2eAgFnSc4d1	celková
hladinu	hladina	k1gFnSc4	hladina
cholesterolu	cholesterol	k1gInSc2	cholesterol
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
především	především	k9	především
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
hodného	hodný	k2eAgMnSc2d1	hodný
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
HDL	HDL	kA	HDL
<g/>
)	)	kIx)	)
cholesterolu	cholesterol	k1gInSc2	cholesterol
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
existují	existovat	k5eAaImIp3nP	existovat
obavy	obava	k1gFnPc1	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
navzdory	navzdory	k7c3	navzdory
vhodnému	vhodný	k2eAgInSc3d1	vhodný
poměru	poměr	k1gInSc3	poměr
celkového	celkový	k2eAgNnSc2d1	celkové
<g/>
:	:	kIx,	:
<g/>
HDL	HDL	kA	HDL
cholesterolu	cholesterol	k1gInSc2	cholesterol
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
množství	množství	k1gNnSc4	množství
celkového	celkový	k2eAgInSc2d1	celkový
cholesterolu	cholesterol	k1gInSc2	cholesterol
rizikové	rizikový	k2eAgNnSc1d1	rizikové
<g/>
,	,	kIx,	,
kokosový	kokosový	k2eAgInSc1d1	kokosový
a	a	k8xC	a
palmojádrový	palmojádrový	k2eAgInSc1d1	palmojádrový
olej	olej	k1gInSc1	olej
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
zdravější	zdravý	k2eAgInSc4d2	zdravější
tuk	tuk	k1gInSc4	tuk
než	než	k8xS	než
zdroje	zdroj	k1gInPc4	zdroj
trans-mastných	transastný	k2eAgFnPc2d1	trans-mastný
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
živočišné	živočišný	k2eAgInPc4d1	živočišný
tuky	tuk	k1gInPc4	tuk
(	(	kIx(	(
<g/>
máslo	máslo	k1gNnSc1	máslo
<g/>
,	,	kIx,	,
sádlo	sádlo	k1gNnSc1	sádlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
živočišné	živočišný	k2eAgInPc4d1	živočišný
výrobky	výrobek	k1gInPc4	výrobek
<g/>
,	,	kIx,	,
sušenky	sušenka	k1gFnPc4	sušenka
<g/>
,	,	kIx,	,
trvanlivé	trvanlivý	k2eAgNnSc4d1	trvanlivé
a	a	k8xC	a
sladké	sladký	k2eAgNnSc4d1	sladké
pečivo	pečivo	k1gNnSc4	pečivo
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
zmrzlin	zmrzlina	k1gFnPc2	zmrzlina
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
tuky	tuk	k1gInPc1	tuk
přidávají	přidávat	k5eAaImIp3nP	přidávat
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
oleje	olej	k1gInPc1	olej
jsou	být	k5eAaImIp3nP	být
100	[number]	k4	100
<g/>
%	%	kIx~	%
tuky	tuk	k1gInPc1	tuk
<g/>
,	,	kIx,	,
u	u	k7c2	u
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
tuků	tuk	k1gInPc2	tuk
margarínů	margarín	k1gInPc2	margarín
lze	lze	k6eAd1	lze
vybírat	vybírat	k5eAaImF	vybírat
ze	z	k7c2	z
široké	široký	k2eAgFnSc2d1	široká
škály	škála	k1gFnSc2	škála
výrobků	výrobek	k1gInPc2	výrobek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
již	již	k6eAd1	již
od	od	k7c2	od
20	[number]	k4	20
%	%	kIx~	%
tuku	tuk	k1gInSc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Rostlinné	rostlinný	k2eAgInPc1d1	rostlinný
tuky	tuk	k1gInPc1	tuk
se	se	k3xPyFc4	se
také	také	k9	také
někdy	někdy	k6eAd1	někdy
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
margaríny	margarín	k1gInPc1	margarín
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
vlhkosti	vlhkost	k1gFnSc2	vlhkost
a	a	k8xC	a
katalýzou	katalýza	k1gFnSc7	katalýza
přítomnými	přítomný	k2eAgFnPc7d1	přítomná
lipasami	lipasa	k1gFnPc7	lipasa
dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
částečnému	částečný	k2eAgNnSc3d1	částečné
zmýdelňování	zmýdelňování	k1gNnSc3	zmýdelňování
tuků	tuk	k1gInPc2	tuk
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
jejich	jejich	k3xOp3gFnSc1	jejich
kyselost	kyselost	k1gFnSc1	kyselost
<g/>
.	.	kIx.	.
</s>
<s>
Nenasycené	nasycený	k2eNgFnPc1d1	nenasycená
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
,	,	kIx,	,
přítomné	přítomný	k2eAgInPc1d1	přítomný
v	v	k7c6	v
olejích	olej	k1gInPc6	olej
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
podléhají	podléhat	k5eAaImIp3nP	podléhat
oxidaci	oxidace	k1gFnSc4	oxidace
<g/>
.	.	kIx.	.
</s>
<s>
Oxidací	oxidace	k1gFnSc7	oxidace
vícenásobně	vícenásobně	k6eAd1	vícenásobně
nenasycených	nasycený	k2eNgFnPc2d1	nenasycená
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc7	jejich
následnou	následný	k2eAgFnSc7d1	následná
polymerací	polymerace	k1gFnSc7	polymerace
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
tvrdého	tvrdý	k2eAgInSc2d1	tvrdý
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
vysychání	vysychání	k1gNnSc1	vysychání
olejů	olej	k1gInPc2	olej
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
laků	lak	k1gInPc2	lak
a	a	k8xC	a
barviv	barvivo	k1gNnPc2	barvivo
<g/>
.	.	kIx.	.
</s>
<s>
Dvojné	dvojný	k2eAgFnPc1d1	dvojná
vazby	vazba	k1gFnPc1	vazba
nenasycených	nasycený	k2eNgFnPc2d1	nenasycená
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
též	též	k9	též
hydrogenovány	hydrogenovat	k5eAaImNgInP	hydrogenovat
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc2	vznik
polotuhých	polotuhý	k2eAgMnPc2d1	polotuhý
a	a	k8xC	a
tuhých	tuhý	k2eAgInPc2d1	tuhý
tuků	tuk	k1gInPc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
ztužování	ztužování	k1gNnSc1	ztužování
<g/>
.	.	kIx.	.
</s>
<s>
Tuky	tuk	k1gInPc1	tuk
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
původu	původ	k1gInSc2	původ
rostlinného	rostlinný	k2eAgInSc2d1	rostlinný
i	i	k8xC	i
živočišného	živočišný	k2eAgInSc2d1	živočišný
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
esenciální	esenciální	k2eAgFnPc1d1	esenciální
látky	látka	k1gFnPc1	látka
jsou	být	k5eAaImIp3nP	být
přítomny	přítomen	k2eAgInPc1d1	přítomen
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
či	či	k8xC	či
menším	malý	k2eAgNnSc6d2	menší
množství	množství	k1gNnSc6	množství
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
rostlině	rostlina	k1gFnSc6	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
zásobní	zásobní	k2eAgFnPc1d1	zásobní
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
koncentracích	koncentrace	k1gFnPc6	koncentrace
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
určitých	určitý	k2eAgInPc6d1	určitý
rostlinných	rostlinný	k2eAgInPc6d1	rostlinný
orgánech	orgán	k1gInPc6	orgán
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
semenech	semeno	k1gNnPc6	semeno
či	či	k8xC	či
plodech	plod	k1gInPc6	plod
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rostlin	rostlina	k1gFnPc2	rostlina
se	se	k3xPyFc4	se
izolují	izolovat	k5eAaBmIp3nP	izolovat
lisováním	lisování	k1gNnSc7	lisování
za	za	k7c2	za
studena	studeno	k1gNnSc2	studeno
či	či	k8xC	či
za	za	k7c2	za
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
lisováním	lisování	k1gNnSc7	lisování
za	za	k7c2	za
studena	studeno	k1gNnSc2	studeno
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
kvalitnější	kvalitní	k2eAgInSc1d2	kvalitnější
olej	olej	k1gInSc1	olej
<g/>
,	,	kIx,	,
vhodný	vhodný	k2eAgInSc1d1	vhodný
pro	pro	k7c4	pro
terapeutické	terapeutický	k2eAgFnPc4d1	terapeutická
a	a	k8xC	a
potravinářské	potravinářský	k2eAgFnPc4d1	potravinářská
potřeby	potřeba	k1gFnPc4	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Olej	olej	k1gInSc1	olej
<g/>
,	,	kIx,	,
získaný	získaný	k2eAgInSc1d1	získaný
lisováním	lisování	k1gNnPc3	lisování
za	za	k7c2	za
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
pro	pro	k7c4	pro
technické	technický	k2eAgInPc4d1	technický
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Tuky	tuk	k1gInPc1	tuk
lze	lze	k6eAd1	lze
také	také	k9	také
vyextrahovat	vyextrahovat	k5eAaPmF	vyextrahovat
pomocí	pomocí	k7c2	pomocí
organických	organický	k2eAgNnPc2d1	organické
rozpouštědel	rozpouštědlo	k1gNnPc2	rozpouštědlo
s	s	k7c7	s
nízkou	nízký	k2eAgFnSc7d1	nízká
teplotou	teplota	k1gFnSc7	teplota
varu	var	k1gInSc2	var
<g/>
,	,	kIx,	,
např.	např.	kA	např.
trichlorethylen	trichlorethylen	k1gInSc1	trichlorethylen
<g/>
,	,	kIx,	,
benzín	benzín	k1gInSc1	benzín
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tuky	tuk	k1gInPc4	tuk
ve	v	k7c6	v
výživě	výživa	k1gFnSc6	výživa
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidském	lidský	k2eAgNnSc6d1	lidské
těle	tělo	k1gNnSc6	tělo
se	se	k3xPyFc4	se
tuky	tuk	k1gInPc1	tuk
podílí	podílet	k5eAaImIp3nP	podílet
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
mnoha	mnoho	k4c2	mnoho
struktur	struktura	k1gFnPc2	struktura
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hromadí	hromadit	k5eAaImIp3nP	hromadit
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
v	v	k7c6	v
tukové	tukový	k2eAgFnSc6d1	tuková
tkáni	tkáň	k1gFnSc6	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Tukové	tukový	k2eAgFnPc1d1	tuková
buňky	buňka	k1gFnPc1	buňka
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
adipocyty	adipocyt	k1gInPc1	adipocyt
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
lipolýza	lipolýza	k1gFnSc1	lipolýza
<g/>
.	.	kIx.	.
</s>
<s>
Odbourávání	odbourávání	k1gNnSc1	odbourávání
tuků	tuk	k1gInPc2	tuk
v	v	k7c6	v
lidském	lidský	k2eAgNnSc6d1	lidské
těle	tělo	k1gNnSc6	tělo
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
několika	několik	k4yIc6	několik
krocích	krok	k1gInPc6	krok
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
je	být	k5eAaImIp3nS	být
hydrolytické	hydrolytický	k2eAgNnSc1d1	hydrolytické
štěpení	štěpení	k1gNnSc1	štěpení
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
enzymů	enzym	k1gInPc2	enzym
lipáz	lipáza	k1gFnPc2	lipáza
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
molekuly	molekula	k1gFnSc2	molekula
tuku	tuk	k1gInSc2	tuk
vznikají	vznikat	k5eAaImIp3nP	vznikat
tři	tři	k4xCgFnPc1	tři
molekuly	molekula	k1gFnPc1	molekula
mastné	mastný	k2eAgFnSc2d1	mastná
kyseliny	kyselina	k1gFnSc2	kyselina
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
molekula	molekula	k1gFnSc1	molekula
glycerolu	glycerol	k1gInSc2	glycerol
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
z	z	k7c2	z
produktů	produkt	k1gInPc2	produkt
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
odbourává	odbourávat	k5eAaImIp3nS	odbourávat
jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
delších	dlouhý	k2eAgFnPc2d2	delší
a	a	k8xC	a
významnějších	významný	k2eAgFnPc2d2	významnější
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
proces	proces	k1gInSc4	proces
nazývaný	nazývaný	k2eAgInSc4d1	nazývaný
β	β	k?	β
probíhající	probíhající	k2eAgNnSc1d1	probíhající
v	v	k7c6	v
takzvané	takzvaný	k2eAgFnSc6d1	takzvaná
Lynenově	Lynenův	k2eAgFnSc6d1	Lynenův
spirále	spirála	k1gFnSc6	spirála
<g/>
.	.	kIx.	.
</s>
<s>
Vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
sem	sem	k6eAd1	sem
mastná	mastný	k2eAgFnSc1d1	mastná
kyselina	kyselina	k1gFnSc1	kyselina
navázaná	navázaný	k2eAgFnSc1d1	navázaná
na	na	k7c6	na
koenzym-	koenzym-	k?	koenzym-
<g/>
A.	A.	kA	A.
Tuto	tento	k3xDgFnSc4	tento
reakci	reakce	k1gFnSc4	reakce
katalyzuje	katalyzovat	k5eAaPmIp3nS	katalyzovat
enzym	enzym	k1gInSc4	enzym
acyl-CoA-syntenáza	acyl-CoAyntenáza	k1gFnSc1	acyl-CoA-syntenáza
za	za	k7c4	za
účasti	účast	k1gFnPc4	účast
koenzymu-A	koenzymu-A	k?	koenzymu-A
a	a	k8xC	a
adenosintrifosfátu	adenosintrifosfát	k1gInSc2	adenosintrifosfát
(	(	kIx(	(
<g/>
ATP	atp	kA	atp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgNnSc1d1	vlastní
odbourávání	odbourávání	k1gNnSc1	odbourávání
pak	pak	k6eAd1	pak
již	již	k6eAd1	již
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c4	v
matrix	matrix	k1gInSc4	matrix
mitochondrií	mitochondrie	k1gFnPc2	mitochondrie
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
několika	několik	k4yIc2	několik
fázích	fáze	k1gFnPc6	fáze
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
stupeň	stupeň	k1gInSc1	stupeň
odbourávání	odbourávání	k1gNnSc2	odbourávání
–	–	k?	–
dehydrogenace	dehydrogenace	k1gFnSc2	dehydrogenace
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
α	α	k?	α
<g/>
,	,	kIx,	,
<g/>
β	β	k?	β
kyseliny	kyselina	k1gFnSc2	kyselina
vázané	vázaný	k2eAgFnPc1d1	vázaná
na	na	k7c4	na
koenzym	koenzym	k1gInSc4	koenzym
A	a	k9	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
stupeň	stupeň	k1gInSc1	stupeň
odbourávání	odbourávání	k1gNnSc2	odbourávání
–	–	k?	–
adice	adice	k1gFnSc1	adice
H2O	H2O	k1gFnSc2	H2O
na	na	k7c6	na
α	α	k?	α
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
<g/>
β	β	k?	β
kyselinu	kyselina	k1gFnSc4	kyselina
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
β	β	k?	β
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
stupeň	stupeň	k1gInSc1	stupeň
odbourávání	odbourávání	k1gNnSc2	odbourávání
–	–	k?	–
dehydrogenace	dehydrogenace	k1gFnSc1	dehydrogenace
β	β	k?	β
na	na	k7c4	na
β	β	k?	β
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
stupeň	stupeň	k1gInSc1	stupeň
odbourávání	odbourávání	k1gNnSc2	odbourávání
–	–	k?	–
rozpad	rozpad	k1gInSc1	rozpad
labilní	labilní	k2eAgInSc1d1	labilní
β	β	k?	β
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
HSCoA	HSCoA	k1gFnSc2	HSCoA
na	na	k7c6	na
acetyl-CoA	acetyl-CoA	k?	acetyl-CoA
a	a	k8xC	a
mastnou	mastný	k2eAgFnSc4d1	mastná
kyselinu	kyselina	k1gFnSc4	kyselina
vázanou	vázaný	k2eAgFnSc4d1	vázaná
na	na	k7c4	na
koenzym	koenzym	k1gInSc4	koenzym
A	a	k9	a
(	(	kIx(	(
<g/>
o	o	k7c4	o
2	[number]	k4	2
uhlíky	uhlík	k1gInPc1	uhlík
kratší	krátký	k2eAgInPc1d2	kratší
než	než	k8xS	než
původní	původní	k2eAgInSc1d1	původní
<g/>
)	)	kIx)	)
Celý	celý	k2eAgInSc1d1	celý
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
celý	celý	k2eAgInSc1d1	celý
řetězec	řetězec	k1gInSc1	řetězec
původní	původní	k2eAgFnSc2d1	původní
mastné	mastný	k2eAgFnSc2d1	mastná
kyseliny	kyselina	k1gFnSc2	kyselina
zcela	zcela	k6eAd1	zcela
nerozloží	rozložit	k5eNaPmIp3nS	rozložit
na	na	k7c4	na
acetyl-CoA	acetyl-CoA	k?	acetyl-CoA
<g/>
.	.	kIx.	.
</s>
<s>
Tuky	tuk	k1gInPc1	tuk
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
potravinami	potravina	k1gFnPc7	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
farmacii	farmacie	k1gFnSc6	farmacie
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
zejména	zejména	k9	zejména
jako	jako	k9	jako
krycí	krycí	k2eAgInPc4d1	krycí
a	a	k8xC	a
dráždicí	dráždicí	k2eAgInPc4d1	dráždicí
prostředky	prostředek	k1gInPc4	prostředek
pro	pro	k7c4	pro
kůži	kůže	k1gFnSc4	kůže
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
základ	základ	k1gInSc1	základ
mastí	mast	k1gFnPc2	mast
<g/>
,	,	kIx,	,
svým	svůj	k3xOyFgInSc7	svůj
hydrofobním	hydrofobní	k2eAgInSc7d1	hydrofobní
charakterem	charakter	k1gInSc7	charakter
podporují	podporovat	k5eAaImIp3nP	podporovat
vstřebávání	vstřebávání	k1gNnSc4	vstřebávání
některých	některý	k3yIgFnPc2	některý
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
také	také	k9	také
tuky	tuk	k1gInPc1	tuk
samy	sám	k3xTgInPc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
některé	některý	k3yIgFnPc4	některý
příměsi	příměs	k1gFnPc4	příměs
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
steroly	sterol	k1gInPc1	sterol
<g/>
,	,	kIx,	,
lecitiny	lecitin	k1gInPc1	lecitin
<g/>
,	,	kIx,	,
vitamíny	vitamín	k1gInPc1	vitamín
rozpustné	rozpustný	k2eAgInPc1d1	rozpustný
v	v	k7c6	v
tucích	tuk	k1gInPc6	tuk
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Nenasycené	nasycený	k2eNgFnPc1d1	nenasycená
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
kyselina	kyselina	k1gFnSc1	kyselina
linolová	linolový	k2eAgFnSc1d1	linolová
a	a	k8xC	a
linolenová	linolenový	k2eAgFnSc1d1	linolenová
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
samy	sám	k3xTgFnPc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
nepostradatelnou	postradatelný	k2eNgFnSc7d1	nepostradatelná
součástí	součást	k1gFnSc7	součást
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gMnPc2	on
třeba	třeba	k6eAd1	třeba
k	k	k7c3	k
syntéze	syntéza	k1gFnSc3	syntéza
glycerofosfatidů	glycerofosfatid	k1gMnPc2	glycerofosfatid
a	a	k8xC	a
prostaglandinů	prostaglandin	k1gMnPc2	prostaglandin
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
vitamin	vitamin	k1gInSc4	vitamin
F	F	kA	F
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
nepostradatelnost	nepostradatelnost	k1gFnSc1	nepostradatelnost
je	být	k5eAaImIp3nS	být
však	však	k9	však
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
otázkou	otázka	k1gFnSc7	otázka
sporu	spor	k1gInSc2	spor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zaslechnout	zaslechnout	k5eAaPmF	zaslechnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
schopno	schopen	k2eAgNnSc1d1	schopno
si	se	k3xPyFc3	se
poradit	poradit	k5eAaPmF	poradit
i	i	k9	i
bez	bez	k7c2	bez
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Nesmíme	smět	k5eNaImIp1nP	smět
opomenout	opomenout	k5eAaPmF	opomenout
obrovský	obrovský	k2eAgInSc4d1	obrovský
technický	technický	k2eAgInSc4d1	technický
význam	význam	k1gInSc4	význam
tuků	tuk	k1gInPc2	tuk
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
barviv	barvivo	k1gNnPc2	barvivo
<g/>
,	,	kIx,	,
laků	lak	k1gInPc2	lak
a	a	k8xC	a
mýdel	mýdlo	k1gNnPc2	mýdlo
<g/>
.	.	kIx.	.
</s>
