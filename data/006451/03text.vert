<s>
Kyselina	kyselina	k1gFnSc1	kyselina
askorbová	askorbový	k2eAgFnSc1d1	askorbová
<g/>
,	,	kIx,	,
označovaná	označovaný	k2eAgFnSc1d1	označovaná
také	také	k9	také
jako	jako	k8xS	jako
vitamin	vitamin	k1gInSc4	vitamin
C	C	kA	C
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
organická	organický	k2eAgFnSc1d1	organická
sloučenina	sloučenina	k1gFnSc1	sloučenina
s	s	k7c7	s
redukčními	redukční	k2eAgInPc7d1	redukční
účinky	účinek	k1gInPc7	účinek
<g/>
,	,	kIx,	,
v	v	k7c6	v
pevném	pevný	k2eAgInSc6d1	pevný
stavu	stav	k1gInSc6	stav
bílého	bílý	k2eAgInSc2d1	bílý
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
;	;	kIx,	;
znečištěná	znečištěný	k2eAgFnSc1d1	znečištěná
s	s	k7c7	s
možným	možný	k2eAgNnSc7d1	možné
nažloutlým	nažloutlý	k2eAgNnSc7d1	nažloutlé
zabarvením	zabarvení	k1gNnSc7	zabarvení
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
derivát	derivát	k1gInSc1	derivát
glukózy	glukóza	k1gFnSc2	glukóza
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgMnSc2	jenž
řada	řada	k1gFnSc1	řada
zvířat	zvíře	k1gNnPc2	zvíře
dokáže	dokázat	k5eAaPmIp3nS	dokázat
danou	daný	k2eAgFnSc4d1	daná
aktivní	aktivní	k2eAgFnSc4d1	aktivní
látku	látka	k1gFnSc4	látka
vyrábět	vyrábět	k5eAaImF	vyrábět
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
ji	on	k3xPp3gFnSc4	on
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
další	další	k2eAgMnPc1d1	další
primáti	primát	k1gMnPc1	primát
<g/>
,	,	kIx,	,
morče	morče	k1gNnSc1	morče
domácí	domácí	k2eAgInPc1d1	domácí
<g/>
,	,	kIx,	,
ryby	ryba	k1gFnPc1	ryba
z	z	k7c2	z
nadřádu	nadřád	k1gInSc2	nadřád
kostnatí	kostnatět	k5eAaImIp3nS	kostnatět
<g/>
,	,	kIx,	,
netopýři	netopýr	k1gMnPc1	netopýr
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
syntetizovat	syntetizovat	k5eAaImF	syntetizovat
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
absence	absence	k1gFnSc2	absence
L-gulonolaktonoxidázové	Lulonolaktonoxidázový	k2eAgFnSc2d1	L-gulonolaktonoxidázový
aktivity	aktivita	k1gFnSc2	aktivita
a	a	k8xC	a
musí	muset	k5eAaImIp3nP	muset
ji	on	k3xPp3gFnSc4	on
přijímat	přijímat	k5eAaImF	přijímat
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
tak	tak	k9	tak
mezi	mezi	k7c4	mezi
vitamíny	vitamín	k1gInPc4	vitamín
rozpustné	rozpustný	k2eAgInPc4d1	rozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
slabě	slabě	k6eAd1	slabě
kyselý	kyselý	k2eAgInSc4d1	kyselý
roztok	roztok	k1gInSc4	roztok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
organizmu	organizmus	k1gInSc6	organizmus
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
důležitý	důležitý	k2eAgInSc4d1	důležitý
antioxidant	antioxidant	k1gInSc4	antioxidant
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ATC	ATC	kA	ATC
klasifikaci	klasifikace	k1gFnSc6	klasifikace
léčiv	léčivo	k1gNnPc2	léčivo
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
farmakoteraputické	farmakoteraputický	k2eAgFnSc2d1	farmakoteraputický
skupiny	skupina	k1gFnSc2	skupina
Vitaminy	vitamin	k1gInPc4	vitamin
<g/>
,	,	kIx,	,
kód	kód	k1gInSc4	kód
<g/>
:	:	kIx,	:
A	a	k9	a
<g/>
11	[number]	k4	11
<g/>
GA	GA	kA	GA
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Maďarský	maďarský	k2eAgMnSc1d1	maďarský
biochemik	biochemik	k1gMnSc1	biochemik
Albert	Albert	k1gMnSc1	Albert
Szent-Györgyi	Szent-Györgyi	k1gNnPc2	Szent-Györgyi
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
nebo	nebo	k8xC	nebo
lékařství	lékařství	k1gNnSc4	lékařství
"	"	kIx"	"
<g/>
za	za	k7c4	za
objevy	objev	k1gInPc4	objev
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
biologického	biologický	k2eAgInSc2d1	biologický
spalovacího	spalovací	k2eAgInSc2d1	spalovací
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
vitamínu	vitamín	k1gInSc3	vitamín
C	C	kA	C
a	a	k8xC	a
katalýze	katalýza	k1gFnSc6	katalýza
kyseliny	kyselina	k1gFnSc2	kyselina
fumarové	fumarová	k1gFnSc2	fumarová
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Již	již	k6eAd1	již
mezi	mezi	k7c7	mezi
roky	rok	k1gInPc7	rok
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1923	[number]	k4	1923
uvedli	uvést	k5eAaPmAgMnP	uvést
první	první	k4xOgFnPc4	první
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
charakteru	charakter	k1gInSc6	charakter
a	a	k8xC	a
izolaci	izolace	k1gFnSc6	izolace
látky	látka	k1gFnSc2	látka
Zilva	Zilvo	k1gNnSc2	Zilvo
a	a	k8xC	a
Hardena	Hardeno	k1gNnSc2	Hardeno
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
pak	pak	k6eAd1	pak
Szent-Györgyi	Szent-Györgye	k1gFnSc4	Szent-Györgye
izoloval	izolovat	k5eAaBmAgMnS	izolovat
tzv.	tzv.	kA	tzv.
kyselinu	kyselina	k1gFnSc4	kyselina
hexuronovou	hexuronový	k2eAgFnSc4d1	hexuronový
z	z	k7c2	z
nadledvin	nadledvina	k1gFnPc2	nadledvina
a	a	k8xC	a
papriky	paprika	k1gFnSc2	paprika
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
následně	následně	k6eAd1	následně
potvrzena	potvrzen	k2eAgFnSc1d1	potvrzena
strukturní	strukturní	k2eAgFnSc1d1	strukturní
shoda	shoda	k1gFnSc1	shoda
s	s	k7c7	s
"	"	kIx"	"
<g/>
redukčním	redukční	k2eAgInSc7d1	redukční
faktorem	faktor	k1gInSc7	faktor
<g/>
"	"	kIx"	"
Zilvy	Zilva	k1gFnSc2	Zilva
<g/>
.	.	kIx.	.
</s>
<s>
Walter	Walter	k1gMnSc1	Walter
Haworth	Haworth	k1gMnSc1	Haworth
<g/>
,	,	kIx,	,
Edmund	Edmund	k1gMnSc1	Edmund
Hirst	Hirst	k1gMnSc1	Hirst
a	a	k8xC	a
Tadeus	Tadeus	k1gMnSc1	Tadeus
Reichstein	Reichstein	k1gMnSc1	Reichstein
paralelně	paralelně	k6eAd1	paralelně
syntetizovali	syntetizovat	k5eAaImAgMnP	syntetizovat
redukční	redukční	k2eAgInSc4d1	redukční
faktor	faktor	k1gInSc4	faktor
a	a	k8xC	a
potvrdili	potvrdit	k5eAaPmAgMnP	potvrdit
původně	původně	k6eAd1	původně
navržený	navržený	k2eAgInSc4d1	navržený
model	model	k1gInSc4	model
sloučeniny	sloučenina	k1gFnSc2	sloučenina
Karrerem	Karrero	k1gNnSc7	Karrero
<g/>
,	,	kIx,	,
Micheelem	Micheel	k1gInSc7	Micheel
a	a	k8xC	a
Kraftem	Kraft	k1gInSc7	Kraft
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
hexuronovou	hexuronový	k2eAgFnSc4d1	hexuronový
kyselinu	kyselina	k1gFnSc4	kyselina
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
antiskorbutický	antiskorbutický	k2eAgInSc1d1	antiskorbutický
faktor	faktor	k1gInSc1	faktor
<g/>
,	,	kIx,	,
použil	použít	k5eAaPmAgMnS	použít
Szent-Györgyi	Szent-Györgye	k1gFnSc4	Szent-Györgye
nový	nový	k2eAgInSc4d1	nový
název	název	k1gInSc4	název
kyselina	kyselina	k1gFnSc1	kyselina
askorbová	askorbový	k2eAgFnSc1d1	askorbová
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
jeho	jeho	k3xOp3gMnPc2	jeho
léčebných	léčebný	k2eAgMnPc2d1	léčebný
účinků	účinek	k1gInPc2	účinek
skorbutu	skorbut	k1gInSc2	skorbut
<g/>
.	.	kIx.	.
</s>
<s>
Šest	šest	k4xCc1	šest
let	léto	k1gNnPc2	léto
po	po	k7c6	po
objevu	objev	k1gInSc6	objev
vitamínů	vitamín	k1gInPc2	vitamín
biochemikem	biochemik	k1gMnSc7	biochemik
polského	polský	k2eAgInSc2d1	polský
původu	původ	k1gInSc2	původ
Kazimierzem	Kazimierz	k1gMnSc7	Kazimierz
Funkem	funk	k1gInSc7	funk
zavedl	zavést	k5eAaPmAgInS	zavést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
Jack	Jack	k1gMnSc1	Jack
Cecil	Cecil	k1gMnSc1	Cecil
Drummond	Drummond	k1gMnSc1	Drummond
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
C	C	kA	C
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
askorbová	askorbový	k2eAgFnSc1d1	askorbová
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
přítomna	přítomno	k1gNnSc2	přítomno
také	také	k9	také
ve	v	k7c6	v
vázané	vázaný	k2eAgFnSc6d1	vázaná
formě	forma	k1gFnSc6	forma
jako	jako	k9	jako
"	"	kIx"	"
<g/>
askorbigén	askorbigén	k1gInSc1	askorbigén
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
ze	z	k7c2	z
zelí	zelí	k1gNnSc2	zelí
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
získal	získat	k5eAaPmAgMnS	získat
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
přísluší	příslušet	k5eAaImIp3nS	příslušet
daný	daný	k2eAgInSc4d1	daný
termín	termín	k1gInSc4	termín
jen	jen	k9	jen
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
forem	forma	k1gFnPc2	forma
kyseliny	kyselina	k1gFnSc2	kyselina
v	v	k7c6	v
rostlinné	rostlinný	k2eAgFnSc6d1	rostlinná
čeledi	čeleď	k1gFnSc6	čeleď
brukvovité	brukvovitý	k2eAgFnSc6d1	brukvovitá
(	(	kIx(	(
<g/>
Brassicaceae	Brassicaceae	k1gFnSc6	Brassicaceae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Etymologický	etymologický	k2eAgInSc1d1	etymologický
původ	původ	k1gInSc1	původ
má	mít	k5eAaImIp3nS	mít
"	"	kIx"	"
<g/>
kyselina	kyselina	k1gFnSc1	kyselina
askorbová	askorbový	k2eAgFnSc1d1	askorbová
<g/>
"	"	kIx"	"
v	v	k7c6	v
latinských	latinský	k2eAgFnPc6d1	Latinská
<g/>
:	:	kIx,	:
a	a	k8xC	a
(	(	kIx(	(
<g/>
nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
<g/>
)	)	kIx)	)
+	+	kIx~	+
scorbutus	scorbutus	k1gInSc1	scorbutus
(	(	kIx(	(
<g/>
kurděje	kurděje	k1gFnPc1	kurděje
–	–	k?	–
onemocnění	onemocnění	k1gNnSc1	onemocnění
při	při	k7c6	při
deficitu	deficit	k1gInSc6	deficit
vitamínu	vitamín	k1gInSc2	vitamín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
askorbová	askorbový	k2eAgFnSc1d1	askorbová
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
tvorby	tvorba	k1gFnSc2	tvorba
žlučových	žlučový	k2eAgFnPc2d1	žlučová
kyselin	kyselina	k1gFnPc2	kyselina
a	a	k8xC	a
v	v	k7c6	v
metabolismu	metabolismus	k1gInSc6	metabolismus
pojivových	pojivový	k2eAgFnPc2d1	pojivová
tkání	tkáň	k1gFnPc2	tkáň
hydroxyluje	hydroxylovat	k5eAaBmIp3nS	hydroxylovat
aminokyseliny	aminokyselina	k1gFnPc4	aminokyselina
lysin	lysina	k1gFnPc2	lysina
a	a	k8xC	a
prolin	prolina	k1gFnPc2	prolina
v	v	k7c6	v
kolagenu	kolagen	k1gInSc6	kolagen
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
podporuje	podporovat	k5eAaImIp3nS	podporovat
začleňování	začleňování	k1gNnSc1	začleňování
síranů	síran	k1gInPc2	síran
do	do	k7c2	do
mukopolysacharidů	mukopolysacharid	k1gInPc2	mukopolysacharid
<g/>
.	.	kIx.	.
</s>
<s>
Zapojuje	zapojovat	k5eAaImIp3nS	zapojovat
se	se	k3xPyFc4	se
do	do	k7c2	do
syntézy	syntéza	k1gFnSc2	syntéza
karnitinu	karnitin	k1gInSc2	karnitin
<g/>
,	,	kIx,	,
derivátu	derivát	k1gInSc2	derivát
lysinu	lysin	k1gInSc2	lysin
a	a	k8xC	a
methioninu	methionin	k1gInSc2	methionin
<g/>
,	,	kIx,	,
a	a	k8xC	a
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
beta-oxidaci	betaxidace	k1gFnSc4	beta-oxidace
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
aktivitu	aktivita	k1gFnSc4	aktivita
mikrosomálních	mikrosomální	k2eAgInPc2d1	mikrosomální
enzymů	enzym	k1gInPc2	enzym
<g/>
.	.	kIx.	.
</s>
<s>
Askorbáty	Askorbát	k1gInPc1	Askorbát
jsou	být	k5eAaImIp3nP	být
soli	sůl	k1gFnPc4	sůl
odvozené	odvozený	k2eAgFnPc4d1	odvozená
od	od	k7c2	od
relativně	relativně	k6eAd1	relativně
silné	silný	k2eAgFnSc2d1	silná
kyseliny	kyselina	k1gFnSc2	kyselina
askorbové	askorbový	k2eAgFnSc2d1	askorbová
v	v	k7c6	v
redukované	redukovaný	k2eAgFnSc6d1	redukovaná
formě	forma	k1gFnSc6	forma
koenzymu	koenzym	k1gInSc2	koenzym
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
hraje	hrát	k5eAaImIp3nS	hrát
roli	role	k1gFnSc4	role
také	také	k9	také
v	v	k7c6	v
degradaci	degradace	k1gFnSc6	degradace
aminokyseliny	aminokyselina	k1gFnSc2	aminokyselina
tyrosinu	tyrosina	k1gFnSc4	tyrosina
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
metabolismu	metabolismus	k1gInSc2	metabolismus
katecholaminů	katecholamin	k1gInPc2	katecholamin
–	–	k?	–
adrenalinu	adrenalin	k1gInSc2	adrenalin
<g/>
,	,	kIx,	,
noradrenalinu	noradrenalin	k1gInSc2	noradrenalin
a	a	k8xC	a
dopaminu	dopamin	k1gInSc2	dopamin
<g/>
.	.	kIx.	.
</s>
<s>
Snadná	snadný	k2eAgFnSc1d1	snadná
oxidace	oxidace	k1gFnSc1	oxidace
molekuly	molekula	k1gFnSc2	molekula
na	na	k7c4	na
kyselinu	kyselina	k1gFnSc4	kyselina
dehydroaskorbovou	dehydroaskorbový	k2eAgFnSc4d1	dehydroaskorbový
–	–	k?	–
tedy	tedy	k8xC	tedy
oxidovanou	oxidovaný	k2eAgFnSc7d1	oxidovaná
formou	forma	k1gFnSc7	forma
<g/>
,	,	kIx,	,
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
její	její	k3xOp3gNnSc1	její
výrazné	výrazný	k2eAgInPc4d1	výrazný
redukční	redukční	k2eAgInPc4d1	redukční
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
askorbová	askorbový	k2eAgFnSc1d1	askorbová
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
vázat	vázat	k5eAaImF	vázat
reaktivní	reaktivní	k2eAgFnPc4d1	reaktivní
formy	forma	k1gFnPc4	forma
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
snižuje	snižovat	k5eAaImIp3nS	snižovat
rozvoj	rozvoj	k1gInSc1	rozvoj
aterosklérozy	ateroskléroz	k1gInPc7	ateroskléroz
a	a	k8xC	a
zhoršeného	zhoršený	k2eAgNnSc2d1	zhoršené
prokrvení	prokrvení	k1gNnSc2	prokrvení
tkání	tkáň	k1gFnPc2	tkáň
<g/>
,	,	kIx,	,
omezuje	omezovat	k5eAaImIp3nS	omezovat
peroxidaci	peroxidace	k1gFnSc4	peroxidace
lipidů	lipid	k1gInPc2	lipid
a	a	k8xC	a
profylakticky	profylakticky	k6eAd1	profylakticky
působí	působit	k5eAaImIp3nS	působit
před	před	k7c7	před
cigaretovými	cigaretový	k2eAgInPc7d1	cigaretový
oxidanty	oxidant	k1gInPc7	oxidant
<g/>
.	.	kIx.	.
</s>
<s>
Podílí	podílet	k5eAaImIp3nS	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
menší	malý	k2eAgFnPc4d2	menší
přilnavosti	přilnavost	k1gFnPc4	přilnavost
krevních	krevní	k2eAgFnPc2d1	krevní
destiček	destička	k1gFnPc2	destička
a	a	k8xC	a
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
na	na	k7c6	na
endotelu	endotel	k1gInSc6	endotel
stěny	stěna	k1gFnSc2	stěna
cév	céva	k1gFnPc2	céva
<g/>
.	.	kIx.	.
</s>
<s>
Podporuje	podporovat	k5eAaImIp3nS	podporovat
proces	proces	k1gInSc4	proces
hojení	hojení	k1gNnSc2	hojení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
biotransformace	biotransformace	k1gFnSc2	biotransformace
urychluje	urychlovat	k5eAaImIp3nS	urychlovat
detoxikaci	detoxikace	k1gFnSc4	detoxikace
cizorodých	cizorodý	k2eAgFnPc2d1	cizorodá
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
přispívá	přispívat	k5eAaImIp3nS	přispívat
ke	k	k7c3	k
zvyšování	zvyšování	k1gNnSc3	zvyšování
imunity	imunita	k1gFnSc2	imunita
<g/>
.	.	kIx.	.
</s>
<s>
LD50	LD50	k4	LD50
při	při	k7c6	při
perorálním	perorální	k2eAgNnSc6d1	perorální
podání	podání	k1gNnSc6	podání
u	u	k7c2	u
myši	myš	k1gFnSc2	myš
činí	činit	k5eAaImIp3nS	činit
3	[number]	k4	3
367	[number]	k4	367
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Cytotoxiký	Cytotoxiký	k2eAgInSc1d1	Cytotoxiký
účinek	účinek	k1gInSc1	účinek
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
spočívat	spočívat	k5eAaImF	spočívat
v	v	k7c6	v
aktivaci	aktivace	k1gFnSc6	aktivace
DNázy	DNáza	k1gFnSc2	DNáza
a	a	k8xC	a
ničení	ničení	k1gNnSc4	ničení
DNA	dno	k1gNnSc2	dno
rakovinných	rakovinný	k2eAgFnPc2d1	rakovinná
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Znehodnocení	znehodnocení	k1gNnSc1	znehodnocení
kyseliny	kyselina	k1gFnSc2	kyselina
askorbové	askorbový	k2eAgFnSc2d1	askorbová
nastává	nastávat	k5eAaImIp3nS	nastávat
v	v	k7c6	v
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
kovy	kov	k1gInPc7	kov
či	či	k8xC	či
varem	var	k1gInSc7	var
<g/>
.	.	kIx.	.
</s>
<s>
Doporučená	doporučený	k2eAgFnSc1d1	Doporučená
denní	denní	k2eAgFnSc1d1	denní
dávka	dávka	k1gFnSc1	dávka
pro	pro	k7c4	pro
dospělého	dospělý	k2eAgMnSc4d1	dospělý
jedince	jedinec	k1gMnSc4	jedinec
je	být	k5eAaImIp3nS	být
100	[number]	k4	100
mg	mg	kA	mg
<g/>
.	.	kIx.	.
</s>
<s>
Klinické	klinický	k2eAgInPc1d1	klinický
příznaky	příznak	k1gInPc1	příznak
snížené	snížený	k2eAgFnSc2d1	snížená
hladiny	hladina	k1gFnSc2	hladina
v	v	k7c6	v
plazmě	plazma	k1gFnSc6	plazma
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
při	při	k7c6	při
hodnotách	hodnota	k1gFnPc6	hodnota
≤	≤	k?	≤
10	[number]	k4	10
μ	μ	k?	μ
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
<g/>
,	,	kIx,	,
za	za	k7c4	za
indikátor	indikátor	k1gInSc4	indikátor
nízkého	nízký	k2eAgInSc2d1	nízký
přívodu	přívod	k1gInSc2	přívod
vitaminu	vitamin	k1gInSc2	vitamin
C	C	kA	C
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgFnPc1d1	považována
hodnoty	hodnota	k1gFnPc1	hodnota
pod	pod	k7c7	pod
37	[number]	k4	37
μ	μ	k?	μ
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
prevence	prevence	k1gFnSc2	prevence
aterosklerózy	ateroskleróza	k1gFnSc2	ateroskleróza
a	a	k8xC	a
nádorů	nádor	k1gInPc2	nádor
se	se	k3xPyFc4	se
za	za	k7c4	za
žádoucí	žádoucí	k2eAgFnPc4d1	žádoucí
považují	považovat	k5eAaImIp3nP	považovat
hodnoty	hodnota	k1gFnPc1	hodnota
≥	≥	k?	≥
50	[number]	k4	50
μ	μ	k?	μ
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
Projevy	projev	k1gInPc7	projev
nadbytku	nadbytek	k1gInSc2	nadbytek
z	z	k7c2	z
potravy	potrava	k1gFnSc2	potrava
nejsou	být	k5eNaImIp3nP	být
uváděny	uvádět	k5eAaImNgFnP	uvádět
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
%	%	kIx~	%
nevyužitého	využitý	k2eNgInSc2d1	nevyužitý
vitamínu	vitamín	k1gInSc2	vitamín
C	C	kA	C
se	se	k3xPyFc4	se
transformuje	transformovat	k5eAaBmIp3nS	transformovat
na	na	k7c4	na
kyselinu	kyselina	k1gFnSc4	kyselina
šťavelovou	šťavelový	k2eAgFnSc4d1	šťavelová
<g/>
.	.	kIx.	.
</s>
<s>
Riziko	riziko	k1gNnSc1	riziko
tvorby	tvorba	k1gFnSc2	tvorba
ledvinových	ledvinový	k2eAgInPc2d1	ledvinový
kamenů	kámen	k1gInPc2	kámen
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
zdravých	zdravý	k2eAgFnPc2d1	zdravá
osob	osoba	k1gFnPc2	osoba
nízké	nízký	k2eAgFnPc1d1	nízká
<g/>
.	.	kIx.	.
</s>
<s>
Denní	denní	k2eAgInSc1d1	denní
příjem	příjem	k1gInSc1	příjem
by	by	kYmCp3nS	by
neměl	mít	k5eNaImAgInS	mít
překročit	překročit	k5eAaPmF	překročit
1000	[number]	k4	1000
mg	mg	kA	mg
<g/>
.	.	kIx.	.
</s>
<s>
Podávání	podávání	k1gNnSc1	podávání
velmi	velmi	k6eAd1	velmi
vysokých	vysoký	k2eAgFnPc2d1	vysoká
dávek	dávka	k1gFnPc2	dávka
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
5	[number]	k4	5
g	g	kA	g
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
průjem	průjem	k1gInSc1	průjem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Ascorbic	Ascorbic	k1gMnSc1	Ascorbic
acid	acid	k1gMnSc1	acid
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
je	být	k5eAaImIp3nS	být
použit	použit	k2eAgInSc4d1	použit
text	text	k1gInSc4	text
článku	článek	k1gInSc2	článek
Kyselina	kyselina	k1gFnSc1	kyselina
askorbová	askorbový	k2eAgFnSc1d1	askorbová
ve	v	k7c6	v
WikiSkriptech	WikiSkript	k1gInPc6	WikiSkript
českých	český	k2eAgFnPc2d1	Česká
a	a	k8xC	a
slovenských	slovenský	k2eAgFnPc2d1	slovenská
lékařských	lékařský	k2eAgFnPc2d1	lékařská
fakult	fakulta	k1gFnPc2	fakulta
zapojených	zapojený	k2eAgFnPc2d1	zapojená
v	v	k7c6	v
MEFANETu	MEFANETus	k1gInSc6	MEFANETus
<g/>
.	.	kIx.	.
</s>
