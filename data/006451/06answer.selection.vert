<s>
Kyselina	kyselina	k1gFnSc1	kyselina
askorbová	askorbový	k2eAgFnSc1d1	askorbová
<g/>
,	,	kIx,	,
označovaná	označovaný	k2eAgFnSc1d1	označovaná
také	také	k9	také
jako	jako	k8xC	jako
vitamin	vitamin	k1gInSc4	vitamin
C	C	kA	C
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
organická	organický	k2eAgFnSc1d1	organická
sloučenina	sloučenina	k1gFnSc1	sloučenina
s	s	k7c7	s
redukčními	redukční	k2eAgInPc7d1	redukční
účinky	účinek	k1gInPc7	účinek
<g/>
,	,	kIx,	,
v	v	k7c6	v
pevném	pevný	k2eAgInSc6d1	pevný
stavu	stav	k1gInSc6	stav
bílého	bílý	k2eAgInSc2d1	bílý
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
;	;	kIx,	;
znečištěná	znečištěný	k2eAgFnSc1d1	znečištěná
s	s	k7c7	s
možným	možný	k2eAgNnSc7d1	možné
nažloutlým	nažloutlý	k2eAgNnSc7d1	nažloutlé
zabarvením	zabarvení	k1gNnSc7	zabarvení
<g/>
.	.	kIx.	.
</s>
