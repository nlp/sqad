<p>
<s>
Theol	Theol	k1gInSc1	Theol
<g/>
.	.	kIx.	.
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Georg	Georg	k1gMnSc1	Georg
Schönberger	Schönberger	k1gMnSc1	Schönberger
<g/>
,	,	kIx,	,
SJ	SJ	kA	SJ
(	(	kIx(	(
<g/>
1597	[number]	k4	1597
<g/>
,	,	kIx,	,
Innsbruck	Innsbruck	k1gInSc1	Innsbruck
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1645	[number]	k4	1645
<g/>
,	,	kIx,	,
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
jezuitský	jezuitský	k2eAgMnSc1d1	jezuitský
teolog	teolog	k1gMnSc1	teolog
a	a	k8xC	a
představený	představený	k1gMnSc1	představený
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
rektorem	rektor	k1gMnSc7	rektor
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
letech	let	k1gInPc6	let
1640	[number]	k4	1640
–	–	k?	–
1644	[number]	k4	1644
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgNnSc2	svůj
rektorského	rektorský	k2eAgNnSc2d1	rektorské
působení	působení	k1gNnSc2	působení
zažil	zažít	k5eAaPmAgInS	zažít
švédskou	švédský	k2eAgFnSc4d1	švédská
okupaci	okupace	k1gFnSc4	okupace
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
,	,	kIx,	,
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
jezuity	jezuita	k1gMnPc7	jezuita
a	a	k8xC	a
zbytkem	zbytek	k1gInSc7	zbytek
studentů	student	k1gMnPc2	student
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1643	[number]	k4	1643
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Fechtnerová	Fechtnerový	k2eAgFnSc1d1	Fechtnerový
Anna	Anna	k1gFnSc1	Anna
<g/>
,	,	kIx,	,
Rectores	Rectores	k1gInSc1	Rectores
collegiorum	collegiorum	k1gInSc4	collegiorum
Societatis	Societatis	k1gFnSc2	Societatis
Iesu	Iesus	k1gInSc2	Iesus
in	in	k?	in
Bohemia	bohemia	k1gFnSc1	bohemia
<g/>
,	,	kIx,	,
Moravia	Moravia	k1gFnSc1	Moravia
ac	ac	k?	ac
Silesia	Silesia	k1gFnSc1	Silesia
usque	usquat	k5eAaPmIp3nS	usquat
ad	ad	k7c4	ad
annum	annum	k1gInSc4	annum
MDCCLXXIII	MDCCLXXIII	kA	MDCCLXXIII
iacentium	iacentium	k1gNnSc1	iacentium
–	–	k?	–
Rektoři	rektor	k1gMnPc1	rektor
kolejí	kolej	k1gFnPc2	kolej
Tovaryšstva	tovaryšstvo	k1gNnSc2	tovaryšstvo
Ježíšova	Ježíšův	k2eAgFnSc1d1	Ježíšova
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1773	[number]	k4	1773
<g/>
,	,	kIx,	,
I-II	I-II	k1gFnSc1	I-II
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Národní	národní	k2eAgFnSc1d1	národní
knihovna	knihovna	k1gFnSc1	knihovna
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7050-158-8	[number]	k4	80-7050-158-8
-	-	kIx~	-
díl	díl	k1gInSc4	díl
II	II	kA	II
<g/>
,	,	kIx,	,
s.	s.	k?	s.
327	[number]	k4	327
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Fiala	Fiala	k1gMnSc1	Fiala
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
Rektoři	rektor	k1gMnPc1	rektor
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1573	[number]	k4	1573
<g/>
-	-	kIx~	-
<g/>
1860	[number]	k4	1860
<g/>
:	:	kIx,	:
SCHOENBERGER	SCHOENBERGER	kA	SCHOENBERGER
(	(	kIx(	(
<g/>
Schönberger	Schönberger	k1gMnSc1	Schönberger
<g/>
)	)	kIx)	)
Georgius	Georgius	k1gMnSc1	Georgius
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Žurnálu	žurnál	k1gInSc2	žurnál
UP	UP	kA	UP
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
15	[number]	k4	15
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
č.	č.	k?	č.
2	[number]	k4	2
<g/>
,	,	kIx,	,
s.	s.	k?	s.
9	[number]	k4	9
<g/>
,	,	kIx,	,
č.	č.	k?	č.
3	[number]	k4	3
<g/>
,	,	kIx,	,
s.	s.	k?	s.
11	[number]	k4	11
a	a	k8xC	a
č.	č.	k?	č.
4	[number]	k4	4
<g/>
,	,	kIx,	,
s.	s.	k?	s.
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
