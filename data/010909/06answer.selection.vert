<s>
Theol	Theol	k1gInSc1	Theol
<g/>
.	.	kIx.	.
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Georg	Georg	k1gMnSc1	Georg
Schönberger	Schönberger	k1gMnSc1	Schönberger
<g/>
,	,	kIx,	,
SJ	SJ	kA	SJ
(	(	kIx(	(
<g/>
1597	[number]	k4	1597
<g/>
,	,	kIx,	,
Innsbruck	Innsbruck	k1gInSc1	Innsbruck
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1645	[number]	k4	1645
<g/>
,	,	kIx,	,
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
jezuitský	jezuitský	k2eAgMnSc1d1	jezuitský
teolog	teolog	k1gMnSc1	teolog
a	a	k8xC	a
představený	představený	k1gMnSc1	představený
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
rektorem	rektor	k1gMnSc7	rektor
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
letech	let	k1gInPc6	let
1640	[number]	k4	1640
–	–	k?	–
1644	[number]	k4	1644
<g/>
.	.	kIx.	.
</s>
