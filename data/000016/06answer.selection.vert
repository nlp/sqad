<s>
Obec	obec	k1gFnSc1	obec
Dalečín	Dalečína	k1gFnPc2	Dalečína
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zhruba	zhruba	k6eAd1	zhruba
7,5	[number]	k4	7,5
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Bystřice	Bystřice	k1gFnSc2	Bystřice
nad	nad	k7c7	nad
Pernštejnem	Pernštejn	k1gInSc7	Pernštejn
a	a	k8xC	a
13	[number]	k4	13
km	km	kA	km
vsv	vsv	k?	vsv
<g/>
.	.	kIx.	.
od	od	k7c2	od
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Žďár	Žďár	k1gInSc1	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
.	.	kIx.	.
</s>
