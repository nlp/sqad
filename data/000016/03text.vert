<s>
Obec	obec	k1gFnSc1	obec
Dalečín	Dalečína	k1gFnPc2	Dalečína
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zhruba	zhruba	k6eAd1	zhruba
7,5	[number]	k4	7,5
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Bystřice	Bystřice	k1gFnSc2	Bystřice
nad	nad	k7c7	nad
Pernštejnem	Pernštejn	k1gInSc7	Pernštejn
a	a	k8xC	a
13	[number]	k4	13
km	km	kA	km
vsv	vsv	k?	vsv
<g/>
.	.	kIx.	.
od	od	k7c2	od
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Žďár	Žďár	k1gInSc1	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
dni	den	k1gInSc3	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
703	[number]	k4	703
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
osobního	osobní	k2eAgNnSc2d1	osobní
jména	jméno	k1gNnSc2	jméno
Daleca	Dalecum	k1gNnSc2	Dalecum
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
podoba	podoba	k1gFnSc1	podoba
jména	jméno	k1gNnSc2	jméno
Dalebor	Dalebora	k1gFnPc2	Dalebora
či	či	k8xC	či
Dalemil	Dalemil	k1gFnPc2	Dalemil
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
se	se	k3xPyFc4	se
název	název	k1gInSc1	název
obce	obec	k1gFnSc2	obec
objevoval	objevovat	k5eAaImAgInS	objevovat
v	v	k7c6	v
několika	několik	k4yIc6	několik
podobách	podoba	k1gFnPc6	podoba
-	-	kIx~	-
např.	např.	kA	např.
Daleczin	Daleczin	k1gMnSc1	Daleczin
<g/>
,	,	kIx,	,
Daleczyn	Daleczyn	k1gMnSc1	Daleczyn
<g/>
,	,	kIx,	,
Daletčín	Daletčín	k1gMnSc1	Daletčín
<g/>
,	,	kIx,	,
Dalecžin	Dalecžin	k1gMnSc1	Dalecžin
či	či	k8xC	či
Dalletchin	Dalletchin	k1gMnSc1	Dalletchin
<g/>
,	,	kIx,	,
Daletschin	Daletschin	k1gMnSc1	Daletschin
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
zpochybňovaná	zpochybňovaný	k2eAgFnSc1d1	zpochybňovaná
<g/>
,	,	kIx,	,
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Dalečíně	Dalečína	k1gFnSc6	Dalečína
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1086	[number]	k4	1086
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
Opatovického	opatovický	k2eAgInSc2d1	opatovický
kláštera	klášter	k1gInSc2	klášter
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
jako	jako	k9	jako
Daletice	Daletika	k1gFnSc6	Daletika
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
spolehlivá	spolehlivý	k2eAgFnSc1d1	spolehlivá
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1349	[number]	k4	1349
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byl	být	k5eAaImAgInS	být
postoupen	postoupit	k5eAaPmNgInS	postoupit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Písečným	písečný	k2eAgNnSc7d1	písečné
a	a	k8xC	a
Dětochovem	Dětochovo	k1gNnSc7	Dětochovo
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Vítochov	Vítochov	k1gInSc1	Vítochov
<g/>
)	)	kIx)	)
Znatovi	Znatův	k2eAgMnPc1d1	Znatův
z	z	k7c2	z
Tasova	Tasov	k1gInSc2	Tasov
(	(	kIx(	(
<g/>
území	území	k1gNnSc6	území
získal	získat	k5eAaPmAgMnS	získat
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
manželky	manželka	k1gFnSc2	manželka
Kláry	Klára	k1gFnSc2	Klára
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalečín	Dalečín	k1gInSc1	Dalečín
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
tvořil	tvořit	k5eAaImAgMnS	tvořit
středisko	středisko	k1gNnSc4	středisko
samostatného	samostatný	k2eAgNnSc2d1	samostatné
panství	panství	k1gNnSc2	panství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1358	[number]	k4	1358
získal	získat	k5eAaPmAgInS	získat
Jimram	Jimram	k1gInSc1	Jimram
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
události	událost	k1gFnPc1	událost
z	z	k7c2	z
historie	historie	k1gFnSc2	historie
obce	obec	k1gFnSc2	obec
jsou	být	k5eAaImIp3nP	být
nám	my	k3xPp1nPc3	my
známy	znám	k2eAgFnPc1d1	známa
díky	díky	k7c3	díky
záznamům	záznam	k1gInPc3	záznam
v	v	k7c6	v
Moravských	moravský	k2eAgFnPc6d1	Moravská
zemských	zemský	k2eAgFnPc6d1	zemská
deskách	deska	k1gFnPc6	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1588	[number]	k4	1588
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
prodal	prodat	k5eAaPmAgMnS	prodat
obec	obec	k1gFnSc4	obec
Pavlu	Pavel	k1gMnSc3	Pavel
Katharynovi	Katharyn	k1gMnSc3	Katharyn
z	z	k7c2	z
Katharu	Kathar	k1gInSc2	Kathar
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
jej	on	k3xPp3gInSc4	on
získala	získat	k5eAaPmAgFnS	získat
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Krizelda	Krizelda	k1gFnSc1	Krizelda
Čejkovna	Čejkovna	k1gFnSc1	Čejkovna
z	z	k7c2	z
Olbramovic	Olbramovice	k1gFnPc2	Olbramovice
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1603	[number]	k4	1603
jej	on	k3xPp3gInSc4	on
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
okolními	okolní	k2eAgFnPc7d1	okolní
obcemi	obec	k1gFnPc7	obec
Petr	Petr	k1gMnSc1	Petr
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Katharynové	Katharynová	k1gFnSc2	Katharynová
prodali	prodat	k5eAaPmAgMnP	prodat
Vilému	Vilém	k1gMnSc3	Vilém
Dubskému	Dubský	k1gMnSc3	Dubský
z	z	k7c2	z
Třebomyslic	Třebomyslice	k1gFnPc2	Třebomyslice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
majetek	majetek	k1gInSc1	majetek
zkonfiskován	zkonfiskovat	k5eAaPmNgInS	zkonfiskovat
a	a	k8xC	a
novým	nový	k2eAgMnSc7d1	nový
majitelem	majitel	k1gMnSc7	majitel
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Štěpán	Štěpán	k1gMnSc1	Štěpán
Schmidt	Schmidt	k1gMnSc1	Schmidt
z	z	k7c2	z
Freihofenu	Freihofen	k1gInSc2	Freihofen
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1624	[number]	k4	1624
<g/>
-	-	kIx~	-
<g/>
1633	[number]	k4	1633
bylo	být	k5eAaImAgNnS	být
dalečínské	dalečínský	k2eAgNnSc1d1	dalečínský
panství	panství	k1gNnSc1	panství
připojeno	připojen	k2eAgNnSc1d1	připojeno
hraběnkou	hraběnka	k1gFnSc7	hraběnka
Šlikovou	Šliková	k1gFnSc4	Šliková
ze	z	k7c2	z
Salmu	Salm	k1gInSc2	Salm
ke	k	k7c3	k
kunštátskému	kunštátský	k2eAgNnSc3d1	kunštátské
panství	panství	k1gNnSc3	panství
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
patřil	patřit	k5eAaImAgInS	patřit
až	až	k6eAd1	až
do	do	k7c2	do
zrušení	zrušení	k1gNnSc2	zrušení
patrimoniální	patrimoniální	k2eAgFnSc2d1	patrimoniální
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
majitelem	majitel	k1gMnSc7	majitel
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
svobodný	svobodný	k2eAgMnSc1d1	svobodný
pán	pán	k1gMnSc1	pán
Honrichs	Honrichsa	k1gFnPc2	Honrichsa
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
Dalečín	Dalečín	k1gMnSc1	Dalečín
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jakuba	Jakub	k1gMnSc2	Jakub
Většího	veliký	k2eAgMnSc2d2	veliký
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgInSc3	který
vede	vést	k5eAaImIp3nS	vést
po	po	k7c6	po
schodišti	schodiště	k1gNnSc6	schodiště
Křížová	Křížová	k1gFnSc1	Křížová
cesta	cesta	k1gFnSc1	cesta
zámek	zámek	k1gInSc4	zámek
Dalečín	Dalečín	k1gInSc1	Dalečín
zřícenina	zřícenina	k1gFnSc1	zřícenina
hradu	hrad	k1gInSc2	hrad
Dalečín	Dalečín	k1gInSc1	Dalečín
Jaroškova	Jaroškův	k2eAgFnSc1d1	Jaroškův
vila	vila	k1gFnSc1	vila
Dalečín	Dalečína	k1gFnPc2	Dalečína
Hluboké	hluboký	k2eAgNnSc1d1	hluboké
Veselí	veselí	k1gNnSc1	veselí
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
a	a	k8xC	a
na	na	k7c4	na
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
až	až	k9	až
do	do	k7c2	do
jeho	jeho	k3xOp3gInSc2	jeho
zániku	zánik	k1gInSc2	zánik
byl	být	k5eAaImAgInS	být
součástí	součást	k1gFnSc7	součást
Dalečína	Dalečín	k1gInSc2	Dalečín
i	i	k8xC	i
Chudobín	Chudobín	k1gInSc1	Chudobín
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
zaplaven	zaplavit	k5eAaPmNgInS	zaplavit
vírskou	vírský	k2eAgFnSc7d1	Vírská
přehradou	přehrada	k1gFnSc7	přehrada
<g/>
.	.	kIx.	.
</s>
