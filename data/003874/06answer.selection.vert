<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1600	[number]	k4	1600
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Sekigahary	Sekigahara	k1gFnSc2	Sekigahara
šógun	šóguno	k1gNnPc2	šóguno
Tokugawa	Tokugawa	k1gMnSc1	Tokugawa
Iejasu	Iejas	k1gInSc2	Iejas
buď	buď	k8xC	buď
porazil	porazit	k5eAaPmAgMnS	porazit
nebo	nebo	k8xC	nebo
přijal	přijmout	k5eAaPmAgMnS	přijmout
za	za	k7c4	za
spojence	spojenec	k1gMnSc4	spojenec
všechny	všechen	k3xTgMnPc4	všechen
své	svůj	k3xOyFgMnPc4	svůj
nepřátele	nepřítel	k1gMnPc4	nepřítel
a	a	k8xC	a
zformoval	zformovat	k5eAaPmAgMnS	zformovat
šógunát	šógunát	k1gInSc4	šógunát
Tokugawa	Tokugaw	k1gInSc2	Tokugaw
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
rybářské	rybářský	k2eAgFnSc6d1	rybářská
vesničce	vesnička	k1gFnSc6	vesnička
Edo	Eda	k1gMnSc5	Eda
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
přepisováno	přepisovat	k5eAaImNgNnS	přepisovat
též	též	k9	též
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Jeddo	Jeddo	k1gNnSc1	Jeddo
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
Tokio	Tokio	k1gNnSc1	Tokio
(	(	kIx(	(
<g/>
východní	východní	k2eAgNnSc1d1	východní
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
