<p>
<s>
Písně	píseň	k1gFnPc4	píseň
kosmické	kosmický	k2eAgFnPc4d1	kosmická
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
básnické	básnický	k2eAgFnSc2d1	básnická
sbírky	sbírka	k1gFnSc2	sbírka
českého	český	k2eAgMnSc2d1	český
básníka	básník	k1gMnSc2	básník
Jana	Jan	k1gMnSc2	Jan
Nerudy	neruda	k1gFnSc2	neruda
vydané	vydaný	k2eAgFnSc2d1	vydaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnPc4	charakteristikon
sbírky	sbírka	k1gFnSc2	sbírka
==	==	k?	==
</s>
</p>
<p>
<s>
Sbírku	sbírka	k1gFnSc4	sbírka
tvoří	tvořit	k5eAaImIp3nS	tvořit
38	[number]	k4	38
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
je	být	k5eAaImIp3nS	být
opěvováno	opěvován	k2eAgNnSc1d1	opěvováno
lidské	lidský	k2eAgNnSc1d1	lidské
poznání	poznání	k1gNnSc1	poznání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
a	a	k8xC	a
objevuje	objevovat	k5eAaImIp3nS	objevovat
dosud	dosud	k6eAd1	dosud
nepoznané	poznaný	k2eNgFnSc3d1	nepoznaná
skutečnosti	skutečnost	k1gFnSc3	skutečnost
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kontradikci	kontradikce	k1gFnSc6	kontradikce
malého	malý	k2eAgInSc2d1	malý
elementu	element	k1gInSc2	element
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
velkého	velký	k2eAgMnSc2d1	velký
<g/>
,	,	kIx,	,
rozlehlého	rozlehlý	k2eAgInSc2d1	rozlehlý
vesmíru	vesmír	k1gInSc2	vesmír
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
oslavu	oslava	k1gFnSc4	oslava
lidského	lidský	k2eAgMnSc2d1	lidský
ducha	duch	k1gMnSc2	duch
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc2	jeho
činů	čin	k1gInPc2	čin
nového	nový	k2eAgNnSc2d1	nové
objevování	objevování	k1gNnSc2	objevování
<g/>
.	.	kIx.	.
</s>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nelze	lze	k6eNd1	lze
klást	klást	k5eAaImF	klást
racionální	racionální	k2eAgInSc4d1	racionální
a	a	k8xC	a
pozitivisticky	pozitivisticky	k6eAd1	pozitivisticky
zaměřenou	zaměřený	k2eAgFnSc4d1	zaměřená
vědu	věda	k1gFnSc4	věda
do	do	k7c2	do
opozice	opozice	k1gFnSc2	opozice
vůči	vůči	k7c3	vůči
poezii	poezie	k1gFnSc3	poezie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jako	jako	k9	jako
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
staví	stavit	k5eAaBmIp3nS	stavit
na	na	k7c6	na
jejich	jejich	k3xOp3gInPc6	jejich
závěrech	závěr	k1gInPc6	závěr
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
jsou	být	k5eAaImIp3nP	být
oslavována	oslavován	k2eAgNnPc1d1	oslavováno
vesmírná	vesmírný	k2eAgNnPc1d1	vesmírné
tělesa	těleso	k1gNnPc1	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
hlediska	hledisko	k1gNnSc2	hledisko
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
verše	verš	k1gInPc4	verš
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
víceméně	víceméně	k9	víceméně
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
vědeckým	vědecký	k2eAgNnSc7d1	vědecké
poznáním	poznání	k1gNnSc7	poznání
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jak	jak	k6eAd1	jak
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
v	v	k7c6	v
Nerudově	Nerudův	k2eAgFnSc6d1	Nerudova
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
názoru	názor	k1gInSc2	názor
Pavla	Pavel	k1gMnSc2	Pavel
Suchana	Suchan	k1gMnSc2	Suchan
z	z	k7c2	z
Astronomického	astronomický	k2eAgInSc2d1	astronomický
ústavu	ústav	k1gInSc2	ústav
AV	AV	kA	AV
ČR	ČR	kA	ČR
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
Písně	píseň	k1gFnPc1	píseň
kosmické	kosmický	k2eAgFnSc2d1	kosmická
věcné	věcný	k2eAgFnPc4d1	věcná
chyby	chyba	k1gFnPc4	chyba
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
bezchybné	bezchybný	k2eAgFnPc1d1	bezchybná
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
<g/>
Jan	Jan	k1gMnSc1	Jan
Neruda	Neruda	k1gMnSc1	Neruda
měl	mít	k5eAaImAgMnS	mít
dobré	dobrý	k2eAgFnPc4d1	dobrá
znalosti	znalost	k1gFnPc4	znalost
o	o	k7c4	o
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
astronomii	astronomie	k1gFnSc4	astronomie
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
základům	základ	k1gInPc3	základ
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
i	i	k9	i
bratry	bratr	k1gMnPc4	bratr
Fričovy	Fričův	k2eAgMnPc4d1	Fričův
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnPc4d2	pozdější
zakladatele	zakladatel	k1gMnPc4	zakladatel
Hvězdárny	hvězdárna	k1gFnSc2	hvězdárna
v	v	k7c6	v
Ondřejově	Ondřejův	k2eAgNnSc6d1	Ondřejovo
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
pozemku	pozemek	k1gInSc6	pozemek
hvězdárny	hvězdárna	k1gFnSc2	hvězdárna
busta	busta	k1gFnSc1	busta
Josefa	Josef	k1gMnSc2	Josef
Jana	Jan	k1gMnSc2	Jan
Friče	Frič	k1gMnSc2	Frič
s	s	k7c7	s
žábou	žába	k1gFnSc7	žába
a	a	k8xC	a
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
v	v	k7c6	v
pracovně	pracovna	k1gFnSc6	pracovna
lze	lze	k6eAd1	lze
číst	číst	k5eAaImF	číst
nápis	nápis	k1gInSc4	nápis
"	"	kIx"	"
<g/>
Jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
tam	tam	k6eAd1	tam
žáby	žába	k1gFnSc2	žába
taky	taky	k6eAd1	taky
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
z	z	k7c2	z
básně	báseň	k1gFnSc2	báseň
sbírky	sbírka	k1gFnSc2	sbírka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
raketoplánu	raketoplán	k1gInSc2	raketoplán
==	==	k?	==
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
astronautu	astronaut	k1gMnSc3	astronaut
Andrew	Andrew	k1gMnSc3	Andrew
Feustelovi	Feustel	k1gMnSc3	Feustel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
mise	mise	k1gFnSc1	mise
STS-	STS-	k1gFnSc1	STS-
<g/>
125	[number]	k4	125
<g/>
,	,	kIx,	,
raketoplán	raketoplán	k1gInSc1	raketoplán
Atlantis	Atlantis	k1gFnSc1	Atlantis
vynesl	vynést	k5eAaPmAgInS	vynést
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
českou	český	k2eAgFnSc4d1	Česká
vlajku	vlajka	k1gFnSc4	vlajka
a	a	k8xC	a
český	český	k2eAgInSc1d1	český
a	a	k8xC	a
anglický	anglický	k2eAgInSc1d1	anglický
výtisk	výtisk	k1gInSc1	výtisk
Písní	píseň	k1gFnPc2	píseň
kosmických	kosmický	k2eAgFnPc2d1	kosmická
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
vydání	vydání	k1gNnSc1	vydání
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
90	[number]	k4	90
let	léto	k1gNnPc2	léto
staré	starý	k2eAgFnPc1d1	stará
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
astronautovy	astronautův	k2eAgFnSc2d1	Astronautova
manželky	manželka	k1gFnSc2	manželka
je	být	k5eAaImIp3nS	být
Češka	Češka	k1gFnSc1	Češka
pocházející	pocházející	k2eAgFnSc1d1	pocházející
ze	z	k7c2	z
Znojma	Znojmo	k1gNnSc2	Znojmo
<g/>
,	,	kIx,	,
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
pak	pak	k6eAd1	pak
Ind.	Ind.	k1gFnSc4	Ind.
</s>
</p>
<p>
<s>
==	==	k?	==
Ukázka	ukázka	k1gFnSc1	ukázka
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
básní	báseň	k1gFnPc2	báseň
==	==	k?	==
</s>
</p>
<p>
<s>
Letní	letní	k2eAgNnSc1d1	letní
ty	ten	k3xDgFnPc4	ten
noci	noc	k1gFnPc4	noc
zářivá	zářivý	k2eAgFnSc1d1	zářivá
</s>
</p>
<p>
<s>
Když	když	k8xS	když
k	k	k7c3	k
vám	vy	k3xPp2nPc3	vy
vesel	veslo	k1gNnPc2	veslo
hledím	hledět	k5eAaImIp1nS	hledět
<g/>
,	,	kIx,	,
zlatá	zlatý	k2eAgFnSc1d1	zlatá
vy	vy	k3xPp2nPc1	vy
Kuřátka	Kuřátka	k1gNnPc1	Kuřátka
</s>
</p>
<p>
<s>
Ach	ach	k0	ach
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgNnSc1	jaký
to	ten	k3xDgNnSc4	ten
blaho	blaho	k1gNnSc4	blaho
<g/>
:	:	kIx,	:
poležet	poležet	k5eAaPmF	poležet
</s>
</p>
<p>
<s>
Což	což	k3yRnSc4	což
třepotá	třepotat	k5eAaImIp3nS	třepotat
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
tu	tu	k6eAd1	tu
hvězdiček	hvězdička	k1gFnPc2	hvězdička
</s>
</p>
<p>
<s>
Snad	snad	k9	snad
jiní	jiný	k2eAgMnPc1d1	jiný
jinak	jinak	k6eAd1	jinak
uvidí	uvidět	k5eAaPmIp3nP	uvidět
</s>
</p>
<p>
<s>
Věřte	věřit	k5eAaImRp2nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
také	také	k9	také
hvězdičky	hvězdička	k1gFnPc1	hvězdička
</s>
</p>
<p>
<s>
Po	po	k7c6	po
nebi	nebe	k1gNnSc6	nebe
hvězdic	hvězdice	k1gFnPc2	hvězdice
je	být	k5eAaImIp3nS	být
rozseto	rozset	k2eAgNnSc1d1	rozseto
</s>
</p>
<p>
<s>
Poeto	poeta	k1gMnSc5	poeta
Světe	svět	k1gInSc5	svět
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
jsi	být	k5eAaImIp2nS	být
eón	eón	k?	eón
prožil	prožít	k5eAaPmAgMnS	prožít
</s>
</p>
<p>
<s>
Stárnoucí	stárnoucí	k2eAgNnSc1d1	stárnoucí
lidstvo	lidstvo	k1gNnSc1	lidstvo
čte	číst	k5eAaImIp3nS	číst
ve	v	k7c6	v
hvězdách	hvězda	k1gFnPc6	hvězda
</s>
</p>
<p>
<s>
Paprsku	paprsek	k1gInSc3	paprsek
z	z	k7c2	z
Alkyony	Alkyona	k1gFnSc2	Alkyona
mé	můj	k3xOp1gFnSc2	můj
</s>
</p>
<p>
<s>
V	v	k7c6	v
pusté	pustý	k2eAgFnSc6d1	pustá
jsme	být	k5eAaImIp1nP	být
nebeské	nebeský	k2eAgFnSc3d1	nebeská
končině	končina	k1gFnSc3	končina
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
ty	ten	k3xDgFnPc1	ten
vířivé	vířivý	k2eAgFnPc1d1	vířivá
planety	planeta	k1gFnPc1	planeta
</s>
</p>
<p>
<s>
Také	také	k9	také
to	ten	k3xDgNnSc1	ten
Slunce	slunce	k1gNnSc1	slunce
ohnivé	ohnivý	k2eAgNnSc1d1	ohnivé
</s>
</p>
<p>
<s>
Zem	zem	k1gFnSc1	zem
byla	být	k5eAaImAgFnS	být
dítětem	dítě	k1gNnSc7	dítě
<g/>
;	;	kIx,	;
myslela	myslet	k5eAaImAgFnS	myslet
</s>
</p>
<p>
<s>
Měsíček	měsíček	k1gInSc1	měsíček
<g/>
,	,	kIx,	,
pěkný	pěkný	k2eAgMnSc1d1	pěkný
mládenec	mládenec	k1gMnSc1	mládenec
</s>
</p>
<p>
<s>
Báječně	báječně	k6eAd1	báječně
krásný	krásný	k2eAgInSc1d1	krásný
to	ten	k3xDgNnSc4	ten
přec	přec	k9	přec
byl	být	k5eAaImAgInS	být
sen	sen	k1gInSc1	sen
</s>
</p>
<p>
<s>
Měsíček	měsíček	k1gInSc1	měsíček
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
mrtvý	mrtvý	k2eAgMnSc1d1	mrtvý
muž	muž	k1gMnSc1	muž
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
mrtev	mrtev	k2eAgInSc1d1	mrtev
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
něm	on	k3xPp3gMnSc6	on
ve	v	k7c6	v
prodlení	prodlení	k1gNnSc2	prodlení
</s>
</p>
<p>
<s>
Oblaky	oblak	k1gInPc1	oblak
Země	zem	k1gFnSc2	zem
jsou	být	k5eAaImIp3nP	být
synové	syn	k1gMnPc1	syn
tkliví	tklivý	k2eAgMnPc1d1	tklivý
</s>
</p>
<p>
<s>
Čím	co	k3yQnSc7	co
člověk	člověk	k1gMnSc1	člověk
já	já	k3xPp1nSc1	já
ve	v	k7c6	v
světů	svět	k1gInPc2	svět
kruhu	kruh	k1gInSc2	kruh
jsem	být	k5eAaImIp1nS	být
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
lvové	lvový	k2eAgInPc1d1	lvový
bijem	bijem	k?	bijem
o	o	k7c4	o
mříže	mříž	k1gFnPc4	mříž
</s>
</p>
<p>
<s>
Seděly	sedět	k5eAaImAgFnP	sedět
žáby	žába	k1gFnPc1	žába
v	v	k7c6	v
kaluži	kaluž	k1gFnSc6	kaluž
</s>
</p>
<p>
<s>
Že	že	k8xS	že
skály	skála	k1gFnPc1	skála
již	již	k9	již
Země	země	k1gFnSc1	země
plameny	plamen	k1gInPc1	plamen
</s>
</p>
<p>
<s>
Dík	dík	k1gInSc1	dík
budiž	budiž	k9	budiž
vám	vy	k3xPp2nPc3	vy
<g/>
,	,	kIx,	,
zlaté	zlatý	k2eAgFnPc4d1	zlatá
hvězdičky	hvězdička	k1gFnPc4	hvězdička
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Vlast	Vlasta	k1gFnPc2	Vlasta
svou	svůj	k3xOyFgFnSc4	svůj
máš	mít	k5eAaImIp2nS	mít
nade	nad	k7c4	nad
vše	všechen	k3xTgNnSc4	všechen
milovat	milovat	k5eAaImF	milovat
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Vzhůru	vzhůru	k6eAd1	vzhůru
již	již	k9	již
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
národe	národ	k1gInSc5	národ
</s>
</p>
<p>
<s>
Kdo	kdo	k3yRnSc1	kdo
měkkým	měkký	k2eAgMnSc7d1	měkký
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
bídně	bídně	k6eAd1	bídně
mře	mřít	k5eAaImIp3nS	mřít
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
Slunce	slunce	k1gNnSc1	slunce
je	být	k5eAaImIp3nS	být
hvězda	hvězda	k1gFnSc1	hvězda
proměnná	proměnná	k1gFnSc1	proměnná
</s>
</p>
<p>
<s>
Měsíček	měsíček	k1gInSc1	měsíček
mrtvý	mrtvý	k2eAgInSc1d1	mrtvý
–	–	k?	–
budoucnost	budoucnost	k1gFnSc1	budoucnost
</s>
</p>
<p>
<s>
Děj	děj	k1gInSc1	děj
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
krátce	krátce	k6eAd1	krátce
jen	jen	k9	jen
vyprávěn	vyprávěn	k2eAgMnSc1d1	vyprávěn
</s>
</p>
<p>
<s>
Zelená	zelený	k2eAgFnSc1d1	zelená
hvězdo	hvězda	k1gFnSc5	hvězda
v	v	k7c6	v
zenitu	zenit	k1gInSc2	zenit
</s>
</p>
<p>
<s>
Myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
malý	malý	k2eAgInSc1d1	malý
Měsíček	měsíček	k1gInSc1	měsíček
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
vyznám	vyznat	k5eAaBmIp1nS	vyznat
se	se	k3xPyFc4	se
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
hříchů	hřích	k1gInPc2	hřích
svých	svůj	k3xOyFgMnPc2	svůj
</s>
</p>
<p>
<s>
Promluvme	promluvit	k5eAaPmRp1nP	promluvit
sobě	se	k3xPyFc3	se
spolu	spolu	k6eAd1	spolu
</s>
</p>
<p>
<s>
Přijdou	přijít	k5eAaPmIp3nP	přijít
dnové	den	k1gMnPc1	den
<g/>
,	,	kIx,	,
léta	léto	k1gNnPc1	léto
<g/>
,	,	kIx,	,
věky	věk	k1gInPc1	věk
<g/>
,	,	kIx,	,
věků	věk	k1gInPc2	věk
věky	věk	k1gInPc4	věk
</s>
</p>
<p>
<s>
Aj	aj	k0	aj
<g/>
,	,	kIx,	,
tamhle	tamhle	k6eAd1	tamhle
dřímavých	dřímavý	k2eAgFnPc2d1	dřímavý
jiskerek	jiskerka	k1gFnPc2	jiskerka
</s>
</p>
<p>
<s>
Ty	ten	k3xDgInPc1	ten
věčné	věčný	k2eAgInPc1d1	věčný
hlasy	hlas	k1gInPc1	hlas
proroků	prorok	k1gMnPc2	prorok
</s>
</p>
<p>
<s>
Až	až	k9	až
planety	planeta	k1gFnPc1	planeta
sklesnou	sklesnout	k5eAaPmIp3nP	sklesnout
k	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
zpět	zpět	k6eAd1	zpět
</s>
</p>
<p>
<s>
==	==	k?	==
Zhudebněné	zhudebněný	k2eAgNnSc4d1	zhudebněné
dílo	dílo	k1gNnSc4	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Blažek	Blažek	k1gMnSc1	Blažek
napsal	napsat	k5eAaBmAgMnS	napsat
na	na	k7c4	na
básně	báseň	k1gFnPc4	báseň
sbírky	sbírka	k1gFnPc4	sbírka
stejnojmenný	stejnojmenný	k2eAgInSc4d1	stejnojmenný
písňový	písňový	k2eAgInSc4d1	písňový
cyklus	cyklus	k1gInSc4	cyklus
</s>
</p>
<p>
<s>
Radůza	Radůza	k1gFnSc1	Radůza
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
albu	album	k1gNnSc6	album
V	v	k7c6	v
salonu	salon	k1gInSc6	salon
barokních	barokní	k2eAgFnPc2d1	barokní
dam	dáma	k1gFnPc2	dáma
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
zhudebnila	zhudebnit	k5eAaPmAgFnS	zhudebnit
Nerudovu	Nerudův	k2eAgFnSc4d1	Nerudova
Píseň	píseň	k1gFnSc4	píseň
kosmickou	kosmický	k2eAgFnSc4d1	kosmická
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc4	zpěv
XXX	XXX	kA	XXX
<g/>
.	.	kIx.	.
</s>
<s>
Variace	variace	k1gFnPc1	variace
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Hrdobec	Hrdobec	k1gMnSc1	Hrdobec
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Neruda	Neruda	k1gMnSc1	Neruda
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Neruda	Neruda	k1gMnSc1	Neruda
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Knihy	kniha	k1gFnPc1	kniha
básní	básnit	k5eAaImIp3nP	básnit
<g/>
.	.	kIx.	.
</s>
<s>
Orbis	orbis	k1gInSc1	orbis
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NERUDA	Neruda	k1gMnSc1	Neruda
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Písně	píseň	k1gFnPc1	píseň
kosmické	kosmický	k2eAgFnSc2d1	kosmická
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
F.	F.	kA	F.
Topič	topič	k1gMnSc1	topič
<g/>
,	,	kIx,	,
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
57	[number]	k4	57
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Báseň	báseň	k1gFnSc1	báseň
Jak	jak	k8xS	jak
lvové	lvový	k2eAgInPc1d1	lvový
bijem	bijem	k?	bijem
o	o	k7c4	o
mříže	mříž	k1gFnPc4	mříž
zazněla	zaznít	k5eAaPmAgFnS	zaznít
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Marečku	Mareček	k1gMnSc5	Mareček
<g/>
,	,	kIx,	,
podejte	podat	k5eAaPmRp2nP	podat
mi	já	k3xPp1nSc3	já
pero	pero	k1gNnSc4	pero
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
</p>
<p>
<s>
Na	na	k7c4	na
básně	báseň	k1gFnPc4	báseň
Jak	jak	k6eAd1	jak
lvové	lvový	k2eAgInPc1d1	lvový
bijem	bijem	k?	bijem
o	o	k7c4	o
mříže	mříž	k1gFnPc4	mříž
a	a	k8xC	a
Seděly	sedět	k5eAaImAgFnP	sedět
žáby	žába	k1gFnPc1	žába
v	v	k7c6	v
kaluži	kaluž	k1gFnSc6	kaluž
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Jana	Jan	k1gMnSc2	Jan
Rychterové	Rychterová	k1gFnSc2	Rychterová
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
Kosmická	kosmický	k2eAgFnSc1d1	kosmická
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
CD	CD	kA	CD
Zpívací	Zpívací	k2eAgFnSc2d1	Zpívací
písničky	písnička	k1gFnSc2	písnička
<g/>
)	)	kIx)	)
či	či	k8xC	či
Píseň	píseň	k1gFnSc1	píseň
kosmická	kosmický	k2eAgFnSc1d1	kosmická
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
notového	notový	k2eAgInSc2d1	notový
zápisu	zápis	k1gInSc2	zápis
na	na	k7c6	na
internetových	internetový	k2eAgFnPc6d1	internetová
stránkách	stránka	k1gFnPc6	stránka
Jany	Jana	k1gFnSc2	Jana
Rychterové	Rychterová	k1gFnSc2	Rychterová
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Písně	píseň	k1gFnSc2	píseň
kosmické	kosmický	k2eAgFnSc2d1	kosmická
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Písně	píseň	k1gFnPc1	píseň
kosmické	kosmický	k2eAgFnPc1d1	kosmická
v	v	k7c6	v
Digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
</s>
</p>
<p>
<s>
Různá	různý	k2eAgNnPc1d1	různé
vydání	vydání	k1gNnPc1	vydání
Písní	píseň	k1gFnPc2	píseň
kosmických	kosmický	k2eAgFnPc2d1	kosmická
v	v	k7c6	v
digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
Kramerius	Kramerius	k1gMnSc1	Kramerius
NK	NK	kA	NK
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
text	text	k1gInSc1	text
písně	píseň	k1gFnSc2	píseň
Kosmická	kosmický	k2eAgFnSc1d1	kosmická
<g/>
/	/	kIx~	/
<g/>
Píseň	píseň	k1gFnSc1	píseň
kosmická	kosmický	k2eAgFnSc1d1	kosmická
</s>
</p>
