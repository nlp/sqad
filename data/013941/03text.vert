<s>
Kyselina	kyselina	k1gFnSc1
2,5	2,5	k4
<g/>
-dihydroxyskořicová	-dihydroxyskořicová	k1gFnSc1
</s>
<s>
Kyselina	kyselina	k1gFnSc1
2,5	2,5	k4
<g/>
-dihydroxyskořicová	-dihydroxyskořicová	k1gFnSc1
</s>
<s>
Strukturní	strukturní	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Systematický	systematický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Kyselina	kyselina	k1gFnSc1
(	(	kIx(
<g/>
E	E	kA
<g/>
)	)	kIx)
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
(	(	kIx(
<g/>
2,5	2,5	k4
<g/>
-dihydroxyfenyl	-dihydroxyfenyl	k1gInSc1
<g/>
)	)	kIx)
<g/>
prop-	prop-	k?
<g/>
2	#num#	k4
<g/>
-enová	-enovat	k5eAaPmIp3nS,k5eAaImIp3nS
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
Kyselina	kyselina	k1gFnSc1
3	#num#	k4
<g/>
-	-	kIx~
<g/>
(	(	kIx(
<g/>
2,5	2,5	k4
<g/>
-dihydroxyfenyl	-dihydroxyfenyl	k1gInSc1
<g/>
)	)	kIx)
<g/>
akrylová	akrylový	k2eAgFnSc1d1
</s>
<s>
Anglický	anglický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
2,5	2,5	k4
<g/>
-Dihydroxycinnamic	-Dihydroxycinnamic	k1gMnSc1
acid	acid	k1gMnSc1
</s>
<s>
Sumární	sumární	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
C9H8O4	C9H8O4	kA
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
636-01-1	636-01-1	k4
</s>
<s>
PubChem	PubCh	k1gInSc7
</s>
<s>
181581	#num#	k4
</s>
<s>
SMILES	SMILES	kA
</s>
<s>
C	C	kA
<g/>
1	#num#	k4
<g/>
=	=	kIx~
<g/>
CC	CC	kA
<g/>
(	(	kIx(
<g/>
=	=	kIx~
<g/>
C	C	kA
<g/>
(	(	kIx(
<g/>
C	C	kA
<g/>
=	=	kIx~
<g/>
C	C	kA
<g/>
1	#num#	k4
<g/>
O	O	kA
<g/>
)	)	kIx)
<g/>
C	C	kA
<g/>
=	=	kIx~
<g/>
CC	CC	kA
<g/>
(	(	kIx(
<g/>
=	=	kIx~
<g/>
O	O	kA
<g/>
)	)	kIx)
<g/>
O	O	kA
<g/>
)	)	kIx)
<g/>
O	o	k7c6
</s>
<s>
InChI	InChI	k?
</s>
<s>
1	#num#	k4
<g/>
S	s	k7c7
<g/>
/	/	kIx~
<g/>
C	C	kA
<g/>
9	#num#	k4
<g/>
H	H	kA
<g/>
8	#num#	k4
<g/>
O	o	k7c4
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
c	c	k0
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
(	(	kIx(
<g/>
11	#num#	k4
<g/>
)	)	kIx)
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
5	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
(	(	kIx(
<g/>
12	#num#	k4
<g/>
)	)	kIx)
<g/>
13	#num#	k4
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
5,10	5,10	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
H	H	kA
<g/>
,	,	kIx,
<g/>
(	(	kIx(
<g/>
H	H	kA
<g/>
,12	,12	k4
<g/>
,13	,13	k4
<g/>
)	)	kIx)
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Molární	molární	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
180,157	180,157	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kyselina	kyselina	k1gFnSc1
2,5	2,5	k4
<g/>
-dihydroxyskořicová	-dihydroxyskořicová	k1gFnSc1
je	být	k5eAaImIp3nS
aromatická	aromatický	k2eAgFnSc1d1
karboxylová	karboxylový	k2eAgFnSc1d1
kyselina	kyselina	k1gFnSc1
<g/>
,	,	kIx,
jeden	jeden	k4xCgInSc1
z	z	k7c2
izomerů	izomer	k1gInPc2
kyseliny	kyselina	k1gFnSc2
kávové	kávový	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s>
Výroba	výroba	k1gFnSc1
</s>
<s>
Kyselina	kyselina	k1gFnSc1
2,5	2,5	k4
<g/>
-dihydroxyskořicová	-dihydroxyskořicová	k1gFnSc1
se	se	k3xPyFc4
vyrábí	vyrábět	k5eAaImIp3nS
oxidací	oxidace	k1gFnSc7
kyseliny	kyselina	k1gFnSc2
o-kumarové	o-kumarová	k1gFnSc2
peroxodisíranem	peroxodisíran	k1gInSc7
draselným	draselný	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
2,5	2,5	k4
<g/>
-Dihydroxycinnamic	-Dihydroxycinnamic	k1gMnSc1
acid	acid	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
