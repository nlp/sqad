<s>
Markéta	Markéta	k1gFnSc1	Markéta
Irglová	Irglová	k1gFnSc1	Irglová
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1988	[number]	k4	1988
Valašské	valašský	k2eAgNnSc4d1	Valašské
Meziříčí	Meziříčí	k1gNnSc4	Meziříčí
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
skladatelka	skladatelka	k1gFnSc1	skladatelka
<g/>
,	,	kIx,	,
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
a	a	k8xC	a
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
držitelka	držitelka	k1gFnSc1	držitelka
Oscara	Oscar	k1gMnSc2	Oscar
za	za	k7c4	za
originální	originální	k2eAgFnSc4d1	originální
píseň	píseň	k1gFnSc4	píseň
k	k	k7c3	k
filmu	film	k1gInSc3	film
Once	Onc	k1gFnSc2	Onc
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
žila	žít	k5eAaImAgFnS	žít
v	v	k7c6	v
irském	irský	k2eAgInSc6d1	irský
Dublinu	Dublin	k1gInSc6	Dublin
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
žila	žít	k5eAaImAgFnS	žít
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
<g/>
.	.	kIx.	.
</s>
<s>
Markéta	Markéta	k1gFnSc1	Markéta
Irglová	Irglová	k1gFnSc1	Irglová
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
hudbě	hudba	k1gFnSc3	hudba
od	od	k7c2	od
sedmi	sedm	k4xCc2	sedm
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jí	on	k3xPp3gFnSc3	on
rodiče	rodič	k1gMnPc1	rodič
koupili	koupit	k5eAaPmAgMnP	koupit
první	první	k4xOgNnSc4	první
piano	piano	k1gNnSc4	piano
a	a	k8xC	a
přihlásili	přihlásit	k5eAaPmAgMnP	přihlásit
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
hudební	hudební	k2eAgFnSc2d1	hudební
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
devíti	devět	k4xCc6	devět
letech	léto	k1gNnPc6	léto
dostala	dostat	k5eAaPmAgFnS	dostat
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
kytaru	kytara	k1gFnSc4	kytara
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
začala	začít	k5eAaPmAgFnS	začít
hrát	hrát	k5eAaImF	hrát
a	a	k8xC	a
učit	učit	k5eAaImF	učit
se	se	k3xPyFc4	se
písničky	písnička	k1gFnSc2	písnička
poslechem	poslech	k1gInSc7	poslech
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
Valašské	valašský	k2eAgNnSc4d1	Valašské
Meziříčí	Meziříčí	k1gNnSc4	Meziříčí
vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
místní	místní	k2eAgNnSc4d1	místní
gymnázium	gymnázium	k1gNnSc4	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třinácti	třináct	k4xCc3	třináct
se	se	k3xPyFc4	se
seznámila	seznámit	k5eAaPmAgFnS	seznámit
s	s	k7c7	s
Glenem	Glen	k1gMnSc7	Glen
Hansardem	Hansard	k1gMnSc7	Hansard
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgMnSc7	který
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
odjela	odjet	k5eAaPmAgFnS	odjet
koncertovat	koncertovat	k5eAaImF	koncertovat
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
spolu	spolu	k6eAd1	spolu
kamarádili	kamarádit	k5eAaImAgMnP	kamarádit
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
vztah	vztah	k1gInSc1	vztah
přerostl	přerůst	k5eAaPmAgInS	přerůst
v	v	k7c4	v
milostný	milostný	k2eAgInSc4d1	milostný
teprve	teprve	k6eAd1	teprve
až	až	k9	až
po	po	k7c6	po
natočení	natočení	k1gNnSc6	natočení
filmu	film	k1gInSc2	film
Once	Onc	k1gFnSc2	Onc
<g/>
.	.	kIx.	.
</s>
<s>
Hansard	Hansard	k1gInSc1	Hansard
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Irglovou	Irglová	k1gFnSc4	Irglová
miloval	milovat	k5eAaImAgMnS	milovat
sice	sice	k8xC	sice
již	již	k6eAd1	již
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pořád	pořád	k6eAd1	pořád
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
dítě	dítě	k1gNnSc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
po	po	k7c6	po
jejich	jejich	k3xOp3gInSc6	jejich
rozchodu	rozchod	k1gInSc6	rozchod
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2009	[number]	k4	2009
před	před	k7c7	před
novináři	novinář	k1gMnPc7	novinář
připustil	připustit	k5eAaPmAgMnS	připustit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc1	jejich
zamilovaný	zamilovaný	k2eAgInSc1d1	zamilovaný
pár	pár	k1gInSc1	pár
byl	být	k5eAaImAgInS	být
spíše	spíše	k9	spíše
filmovou	filmový	k2eAgFnSc7d1	filmová
fikcí	fikce	k1gFnSc7	fikce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
Irglová	Irglový	k2eAgFnSc1d1	Irglová
a	a	k8xC	a
Hansard	Hansard	k1gInSc4	Hansard
vydali	vydat	k5eAaPmAgMnP	vydat
společné	společný	k2eAgNnSc4d1	společné
album	album	k1gNnSc4	album
jménem	jméno	k1gNnSc7	jméno
The	The	k1gMnSc1	The
Swell	Swell	k1gMnSc1	Swell
Season	Season	k1gMnSc1	Season
v	v	k7c6	v
hudebním	hudební	k2eAgNnSc6d1	hudební
vydavatelství	vydavatelství	k1gNnSc6	vydavatelství
Overcoat	Overcoat	k2eAgInSc4d1	Overcoat
Recordings	Recordings	k1gInSc4	Recordings
<g/>
.	.	kIx.	.
<g/>
Společně	společně	k6eAd1	společně
také	také	k9	také
účinkovali	účinkovat	k5eAaImAgMnP	účinkovat
v	v	k7c6	v
již	již	k6eAd1	již
zmíněném	zmíněný	k2eAgInSc6d1	zmíněný
irském	irský	k2eAgInSc6d1	irský
hudebním	hudební	k2eAgInSc6d1	hudební
filmu	film	k1gInSc6	film
Once	Onc	k1gFnSc2	Onc
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
který	který	k3yQgInSc4	který
také	také	k6eAd1	také
složili	složit	k5eAaPmAgMnP	složit
a	a	k8xC	a
nahráli	nahrát	k5eAaBmAgMnP	nahrát
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
romantickou	romantický	k2eAgFnSc4d1	romantická
píseň	píseň	k1gFnSc4	píseň
Falling	Falling	k1gInSc4	Falling
Slowly	Slowla	k1gFnSc2	Slowla
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
filmu	film	k1gInSc2	film
získali	získat	k5eAaPmAgMnP	získat
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgInS	proslavit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
získal	získat	k5eAaPmAgMnS	získat
cenu	cena	k1gFnSc4	cena
diváků	divák	k1gMnPc2	divák
na	na	k7c6	na
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
Sundance	Sundanec	k1gInSc2	Sundanec
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2008	[number]	k4	2008
získala	získat	k5eAaPmAgFnS	získat
cenu	cena	k1gFnSc4	cena
Akademie	akademie	k1gFnSc2	akademie
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
pro	pro	k7c4	pro
Objev	objev	k1gInSc4	objev
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2011	[number]	k4	2011
se	se	k3xPyFc4	se
Markéta	Markéta	k1gFnSc1	Markéta
Irglová	Irglový	k2eAgFnSc1d1	Irglová
v	v	k7c6	v
USA	USA	kA	USA
vdala	vdát	k5eAaPmAgFnS	vdát
za	za	k7c4	za
amerického	americký	k2eAgMnSc4d1	americký
baskytaristu	baskytarista	k1gMnSc4	baskytarista
Tima	Timus	k1gMnSc4	Timus
Iselera	Iseler	k1gMnSc4	Iseler
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2013	[number]	k4	2013
oznámila	oznámit	k5eAaPmAgFnS	oznámit
Markéta	Markéta	k1gFnSc1	Markéta
Irglová	Irglová	k1gFnSc1	Irglová
rozvod	rozvod	k1gInSc4	rozvod
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
partner	partner	k1gMnSc1	partner
je	být	k5eAaImIp3nS	být
islandský	islandský	k2eAgMnSc1d1	islandský
hudebník	hudebník	k1gMnSc1	hudebník
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
Sturla	Sturla	k1gMnSc1	Sturla
Mio	Mio	k1gMnSc1	Mio
Thorisson	Thorisson	k1gMnSc1	Thorisson
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodila	narodit	k5eAaPmAgFnS	narodit
holčička	holčička	k1gFnSc1	holčička
Árveig	Árveig	k1gMnSc1	Árveig
Sturlasdóttir	Sturlasdóttir	k1gMnSc1	Sturlasdóttir
<g/>
.	.	kIx.	.
</s>
<s>
Syn	syn	k1gMnSc1	syn
Eyvindur	Eyvindur	k1gMnSc1	Eyvindur
Högni	Högň	k1gMnSc3	Högň
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Swell	Swell	k1gMnSc1	Swell
Season	Season	k1gMnSc1	Season
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Once	Onc	k1gInSc2	Onc
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Strict	Strict	k1gMnSc1	Strict
Joy	Joy	k1gMnSc1	Joy
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Anar	Anara	k1gFnPc2	Anara
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Muna	muna	k1gFnSc1	muna
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Markéta	Markéta	k1gFnSc1	Markéta
Irglová	Irglový	k2eAgFnSc1d1	Irglová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Markéta	Markéta	k1gFnSc1	Markéta
Irglová	Irglová	k1gFnSc1	Irglová
ve	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Markéta	Markéta	k1gFnSc1	Markéta
Irglová	Irglová	k1gFnSc1	Irglová
Markéta	Markéta	k1gFnSc1	Markéta
Irglová	Irglová	k1gFnSc1	Irglová
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Swell	Swell	k1gMnSc1	Swell
Season	Season	k1gMnSc1	Season
na	na	k7c4	na
Myspace	Myspace	k1gFnPc4	Myspace
Portrét	portrét	k1gInSc4	portrét
Markéty	Markéta	k1gFnPc1	Markéta
Irglové	Irglová	k1gFnSc2	Irglová
-	-	kIx~	-
Markéta	Markéta	k1gFnSc1	Markéta
Irglová	Irglová	k1gFnSc1	Irglová
<g/>
:	:	kIx,	:
devatenáctka	devatenáctka	k1gFnSc1	devatenáctka
z	z	k7c2	z
Valmezu	Valmez	k1gInSc2	Valmez
nadchla	nadchnout	k5eAaPmAgFnS	nadchnout
Ameriku	Amerika	k1gFnSc4	Amerika
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
původní	původní	k2eAgFnSc1d1	původní
píseň	píseň	k1gFnSc1	píseň
<g/>
,	,	kIx,	,
Oscar	Oscar	k1gInSc1	Oscar
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
YouTube	YouTub	k1gInSc5	YouTub
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
