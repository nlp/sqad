<s>
Tur	tur	k1gMnSc1
<g/>
'	'	kIx"
<g/>
an	an	k?
</s>
<s>
Tur	tur	k1gMnSc1
<g/>
'	'	kIx"
<g/>
an	an	k?
ط	ط	k?
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
32	#num#	k4
<g/>
°	°	k?
<g/>
46	#num#	k4
<g/>
′	′	k?
<g/>
38	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
35	#num#	k4
<g/>
°	°	k?
<g/>
22	#num#	k4
<g/>
′	′	k?
<g/>
14	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
227	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Izrael	Izrael	k1gInSc1
Izrael	Izrael	k1gInSc1
distrikt	distrikt	k1gInSc1
</s>
<s>
Severní	severní	k2eAgFnSc1d1
</s>
<s>
Tur	tur	k1gMnSc1
<g/>
'	'	kIx"
<g/>
an	an	k?
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
12,055	12,055	k4
km²	km²	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
13	#num#	k4
700	#num#	k4
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
1150,2	1150,2	k4
(	(	kIx(
<g/>
r.	r.	kA
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tur	tur	k1gMnSc1
<g/>
'	'	kIx"
<g/>
an	an	k?
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
ת	ת	k?
<g/>
ֻ	ֻ	k?
<g/>
ּ	ּ	k?
<g/>
ר	ר	k?
<g/>
ְ	ְ	k?
<g/>
ע	ע	k?
<g/>
ָ	ָ	k?
<g/>
ן	ן	k?
nebo	nebo	k8xC
ט	ט	k?
<g/>
,	,	kIx,
arabsky	arabsky	k6eAd1
ط	ط	k?
<g/>
,	,	kIx,
v	v	k7c6
oficiálním	oficiální	k2eAgInSc6d1
přepisu	přepis	k1gInSc6
do	do	k7c2
angličtiny	angličtina	k1gFnSc2
Tur	tur	k1gMnSc1
<g/>
'	'	kIx"
<g/>
an	an	k?
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
místní	místní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
(	(	kIx(
<g/>
malé	malý	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
)	)	kIx)
v	v	k7c6
Izraeli	Izrael	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
Severním	severní	k2eAgInSc6d1
distriktu	distrikt	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Leží	ležet	k5eAaImIp3nS
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
227	#num#	k4
metrů	metr	k1gInPc2
v	v	k7c6
Dolní	dolní	k2eAgFnSc6d1
Galileji	Galilea	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
situován	situovat	k5eAaBmNgInS
na	na	k7c4
severní	severní	k2eAgInSc4d1
okraj	okraj	k1gInSc4
údolí	údolí	k1gNnSc2
Bik	bika	k1gFnPc2
<g/>
'	'	kIx"
<g/>
at	at	k?
Tur	tur	k1gMnSc1
<g/>
'	'	kIx"
<g/>
an	an	k?
<g/>
,	,	kIx,
na	na	k7c6
úpatí	úpatí	k1gNnSc6
masivu	masiv	k1gInSc2
hory	hora	k1gFnSc2
Har	Har	k1gMnSc1
Tur	tur	k1gMnSc1
<g/>
'	'	kIx"
<g/>
an	an	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Údolím	údolí	k1gNnSc7
protéká	protékat	k5eAaImIp3nS
vádí	vádí	k1gNnSc4
Nachal	Nachal	k1gMnSc1
Jiftach	Jiftach	k1gMnSc1
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
<g/>
,	,	kIx,
do	do	k7c2
kterého	který	k3yIgMnSc2,k3yRgMnSc2,k3yQgMnSc2
po	po	k7c6
západním	západní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
města	město	k1gNnSc2
přitéká	přitékat	k5eAaImIp3nS
vádí	vádí	k1gNnSc4
Nachal	Nachal	k1gMnSc1
Tur	tur	k1gMnSc1
<g/>
'	'	kIx"
<g/>
an	an	k?
<g/>
.	.	kIx.
</s>
<s>
Město	město	k1gNnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
cca	cca	kA
97	#num#	k4
kilometrů	kilometr	k1gInPc2
severovýchodně	severovýchodně	k6eAd1
od	od	k7c2
centra	centrum	k1gNnSc2
Tel	tel	kA
Avivu	Aviv	k1gInSc2
a	a	k8xC
cca	cca	kA
37	#num#	k4
kilometrů	kilometr	k1gInPc2
východně	východně	k6eAd1
od	od	k7c2
centra	centrum	k1gNnSc2
Haify	Haifa	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
hustě	hustě	k6eAd1
osídleném	osídlený	k2eAgInSc6d1
pásu	pás	k1gInSc6
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
osídlení	osídlení	k1gNnSc1
v	v	k7c6
tomto	tento	k3xDgInSc6
regionu	region	k1gInSc6
je	být	k5eAaImIp3nS
etnicky	etnicky	k6eAd1
smíšené	smíšený	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastní	vlastnit	k5eAaImIp3nS
Tur	tur	k1gMnSc1
<g/>
'	'	kIx"
<g/>
an	an	k?
obývají	obývat	k5eAaImIp3nP
Izraelští	izraelský	k2eAgMnPc1d1
Arabové	Arab	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
okolí	okolí	k1gNnSc6
leží	ležet	k5eAaImIp3nS
mnoho	mnoho	k4c1
dalších	další	k2eAgFnPc2d1
arabských	arabský	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
včetně	včetně	k7c2
velkých	velký	k2eAgNnPc2d1
měst	město	k1gNnPc2
v	v	k7c6
aglomeraci	aglomerace	k1gFnSc6
Nazaretu	Nazaret	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
ale	ale	k8xC
leží	ležet	k5eAaImIp3nP
rozptýlené	rozptýlený	k2eAgFnPc4d1
menší	malý	k2eAgFnPc4d2
židovské	židovský	k2eAgFnPc4d1
vesnice	vesnice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
východní	východní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
pak	pak	k6eAd1
začíná	začínat	k5eAaImIp3nS
oblast	oblast	k1gFnSc4
při	při	k7c6
Galilejském	galilejský	k2eAgNnSc6d1
jezeru	jezero	k1gNnSc6
s	s	k7c7
demografickou	demografický	k2eAgFnSc7d1
převahou	převaha	k1gFnSc7
židů	žid	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
je	být	k5eAaImIp3nS
na	na	k7c4
dopravní	dopravní	k2eAgFnSc4d1
síť	síť	k1gFnSc4
napojeno	napojen	k2eAgNnSc1d1
pomocí	pomocí	k7c2
dálnice	dálnice	k1gFnSc2
číslo	číslo	k1gNnSc1
77	#num#	k4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vede	vést	k5eAaImIp3nS
do	do	k7c2
Tiberiasu	Tiberias	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
Tur	tur	k1gMnSc1
<g/>
'	'	kIx"
<g/>
an	an	k?
podle	podle	k7c2
místní	místní	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
vznikl	vzniknout	k5eAaPmAgInS
cca	cca	kA
před	před	k7c7
200	#num#	k4
lety	léto	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obci	obec	k1gFnSc6
se	se	k3xPyFc4
ale	ale	k9
dochovaly	dochovat	k5eAaPmAgInP
zbytky	zbytek	k1gInPc1
zdí	zeď	k1gFnPc2
a	a	k8xC
nádrží	nádrž	k1gFnPc2
z	z	k7c2
římských	římský	k2eAgFnPc2d1
dob	doba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tur	tur	k1gMnSc1
<g/>
'	'	kIx"
<g/>
an	an	k?
byl	být	k5eAaImAgInS
dobyt	dobýt	k5eAaPmNgInS
izraelskou	izraelský	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
v	v	k7c6
rámci	rámec	k1gInSc6
Operace	operace	k1gFnSc1
Dekel	Dekel	k1gInSc1
během	během	k7c2
války	válka	k1gFnSc2
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
v	v	k7c6
červenci	červenec	k1gInSc6
roku	rok	k1gInSc2
1948	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c4
dobytí	dobytí	k1gNnSc4
nebyla	být	k5eNaImAgFnS
tato	tento	k3xDgFnSc1
arabská	arabský	k2eAgFnSc1d1
vesnice	vesnice	k1gFnSc1
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
mnoha	mnoho	k4c2
jiných	jiné	k1gNnPc2
vysídlena	vysídlen	k2eAgFnSc1d1
a	a	k8xC
udržela	udržet	k5eAaPmAgFnS
si	se	k3xPyFc3
svůj	svůj	k3xOyFgInSc4
arabský	arabský	k2eAgInSc4d1
ráz	ráz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1959	#num#	k4
byl	být	k5eAaImAgMnS
Tur	tur	k1gMnSc1
<g/>
'	'	kIx"
<g/>
an	an	k?
povýšen	povýšit	k5eAaPmNgMnS
na	na	k7c4
místní	místní	k2eAgFnSc4d1
radu	rada	k1gFnSc4
(	(	kIx(
<g/>
malé	malý	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Většina	většina	k1gFnSc1
obyvatel	obyvatel	k1gMnPc2
dojíždí	dojíždět	k5eAaImIp3nS
za	za	k7c7
prací	práce	k1gFnSc7
mimo	mimo	k7c4
obec	obec	k1gFnSc4
<g/>
,	,	kIx,
část	část	k1gFnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
zemědělstvím	zemědělství	k1gNnSc7
<g/>
,	,	kIx,
zejména	zejména	k9
pěstováním	pěstování	k1gNnSc7
oliv	oliva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obci	obec	k1gFnSc6
fungují	fungovat	k5eAaImIp3nP
dva	dva	k4xCgInPc4
křesťanské	křesťanský	k2eAgInPc4d1
kostely	kostel	k1gInPc4
a	a	k8xC
dvě	dva	k4xCgFnPc4
mešity	mešita	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Demografie	demografie	k1gFnSc1
</s>
<s>
Tur	tur	k1gMnSc1
<g/>
'	'	kIx"
<g/>
an	an	k?
je	být	k5eAaImIp3nS
etnicky	etnicky	k6eAd1
zcela	zcela	k6eAd1
arabským	arabský	k2eAgNnSc7d1
městem	město	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
údajů	údaj	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
tvořili	tvořit	k5eAaImAgMnP
arabští	arabský	k2eAgMnPc1d1
muslimové	muslim	k1gMnPc1
87,1	87,1	k4
%	%	kIx~
a	a	k8xC
arabští	arabský	k2eAgMnPc1d1
křesťané	křesťan	k1gMnPc1
12,9	12,9	k4
%	%	kIx~
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Jde	jít	k5eAaImIp3nS
o	o	k7c4
středně	středně	k6eAd1
velké	velký	k2eAgNnSc4d1
sídlo	sídlo	k1gNnSc4
městského	městský	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
prosinci	prosinec	k1gInSc6
2017	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
13	#num#	k4
700	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnSc1
Tur	tur	k1gMnSc1
<g/>
'	'	kIx"
<g/>
an	an	k?
1922	#num#	k4
<g/>
-	-	kIx~
<g/>
2000	#num#	k4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
1922	#num#	k4
</s>
<s>
1931	#num#	k4
</s>
<s>
1945	#num#	k4
</s>
<s>
1948	#num#	k4
</s>
<s>
1949	#num#	k4
</s>
<s>
1955	#num#	k4
</s>
<s>
1961	#num#	k4
</s>
<s>
1972	#num#	k4
</s>
<s>
1980	#num#	k4
</s>
<s>
1983	#num#	k4
</s>
<s>
1990	#num#	k4
</s>
<s>
1993	#num#	k4
</s>
<s>
1994	#num#	k4
</s>
<s>
1995	#num#	k4
</s>
<s>
1996	#num#	k4
</s>
<s>
1997	#num#	k4
</s>
<s>
1998	#num#	k4
</s>
<s>
1999	#num#	k4
</s>
<s>
2000	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
7689611	#num#	k4
3501	#num#	k4
2681	#num#	k4
4441	#num#	k4
9002	#num#	k4
3003	#num#	k4
9005	#num#	k4
4005	#num#	k4
8007	#num#	k4
2008	#num#	k4
0008	#num#	k4
2008	#num#	k4
5008	#num#	k4
7008	#num#	k4
9009	#num#	k4
2009	#num#	k4
5009	#num#	k4
800	#num#	k4
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnSc1
Tur	tur	k1gMnSc1
<g/>
'	'	kIx"
<g/>
an	an	k?
od	od	k7c2
r.	r.	kA
2001	#num#	k4
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
2001	#num#	k4
</s>
<s>
2002	#num#	k4
</s>
<s>
2003	#num#	k4
</s>
<s>
2004	#num#	k4
</s>
<s>
2005	#num#	k4
</s>
<s>
2006	#num#	k4
</s>
<s>
2007	#num#	k4
</s>
<s>
2008	#num#	k4
</s>
<s>
2009	#num#	k4
</s>
<s>
2010	#num#	k4
</s>
<s>
2011	#num#	k4
</s>
<s>
2012	#num#	k4
</s>
<s>
2013	#num#	k4
</s>
<s>
2014	#num#	k4
</s>
<s>
2015	#num#	k4
</s>
<s>
2016	#num#	k4
</s>
<s>
2017	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
10	#num#	k4
00010	#num#	k4
30010	#num#	k4
60010	#num#	k4
90011	#num#	k4
10011	#num#	k4
40011	#num#	k4
60011	#num#	k4
89811	#num#	k4
92412	#num#	k4
10012	#num#	k4
40012	#num#	k4
70012	#num#	k4
80013	#num#	k4
00013	#num#	k4
30013	#num#	k4
50013	#num#	k4
700	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
</s>
<s>
נ	נ	k?
פ	פ	k?
-	-	kIx~
ע	ע	k?
ו	ו	k?
<g/>
.	.	kIx.
<g/>
מ	מ	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izraelský	izraelský	k2eAgInSc1d1
centrální	centrální	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
2	#num#	k4
POPULATION	POPULATION	kA
AND	Anda	k1gFnPc2
DENSITY	DENSITY	kA
PER	pero	k1gNnPc2
SQ	SQ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
KM	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
IN	IN	kA
LOCALITIES	LOCALITIES	kA
NUMBERING	NUMBERING	kA
5,000	5,000	k4
RESIDENTS	RESIDENTS	kA
AND	Anda	k1gFnPc2
MORE	mor	k1gInSc5
ON	on	k3xPp3gMnSc1
31.12	31.12	k4
<g/>
.2017	.2017	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ročenka	ročenka	k1gFnSc1
Centrálního	centrální	k2eAgInSc2d1
statistického	statistický	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
2018	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
,	,	kIx,
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
י	י	k?
2009	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izraelský	izraelský	k2eAgInSc1d1
centrální	centrální	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
</s>
<s>
ת	ת	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
bet-alon	bet-alon	k1gInSc1
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
il	il	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
↑	↑	k?
HERZOG	HERZOG	kA
<g/>
,	,	kIx,
Chaim	Chaim	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arab-Israeli	Arab-Israel	k1gInSc6
Wars	Wars	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Vintage	Vintage	k1gInSc1
Books	Books	k1gInSc1
<g/>
/	/	kIx~
<g/>
Random	Random	k1gInSc1
House	house	k1gNnSc1
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
394	#num#	k4
<g/>
-	-	kIx~
<g/>
71746	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
89	#num#	k4
<g/>
-	-	kIx~
<g/>
91	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Sha	Sha	k1gFnSc1
<g/>
'	'	kIx"
<g/>
ab	ab	k?
(	(	kIx(
<g/>
Israel	Israel	k1gMnSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flags	Flags	k1gInSc1
of	of	k?
the	the	k?
World	World	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Welcome	Welcom	k1gInSc5
To	ten	k3xDgNnSc4
Tur	tur	k1gMnSc1
<g/>
'	'	kIx"
<g/>
an	an	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palestine	Palestin	k1gInSc5
Remembered	Remembered	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Tur	tur	k1gMnSc1
<g/>
'	'	kIx"
<g/>
an	an	k?
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
profil	profil	k1gInSc1
obce	obec	k1gFnSc2
na	na	k7c6
portálu	portál	k1gInSc6
Bet	Bet	k1gMnSc1
Alon	Alon	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Severní	severní	k2eAgInSc1d1
distrikt	distrikt	k1gInSc1
Města	město	k1gNnSc2
</s>
<s>
Afula	Afula	k6eAd1
</s>
<s>
Akko	Akko	k6eAd1
</s>
<s>
Bejt	Bejt	k?
Še	Še	k1gFnSc1
<g/>
'	'	kIx"
<g/>
an	an	k?
</s>
<s>
Jokne	Joknout	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
'	'	kIx"
<g/>
am	am	k?
</s>
<s>
Karmiel	Karmiel	k1gMnSc1
</s>
<s>
Kirjat	Kirjat	k1gInSc1
Šmona	Šmon	k1gInSc2
</s>
<s>
Ma	Ma	k?
<g/>
'	'	kIx"
<g/>
alot-Taršicha	alot-Taršich	k1gMnSc2
</s>
<s>
Migdal	Migdat	k5eAaPmAgMnS,k5eAaImAgMnS
ha-Emek	ha-Emek	k1gMnSc1
</s>
<s>
Naharija	Naharija	k6eAd1
</s>
<s>
Nof	Nof	k?
ha-Galil	ha-Galit	k5eAaPmAgInS,k5eAaImAgInS
</s>
<s>
Nazaret	Nazaret	k1gInSc1
</s>
<s>
Safed	Safed	k1gMnSc1
</s>
<s>
Sachnín	Sachnín	k1gMnSc1
</s>
<s>
Šagor	Šagor	k1gInSc1
(	(	kIx(
<g/>
rozpuštěno	rozpustit	k5eAaPmNgNnS
<g/>
)	)	kIx)
</s>
<s>
Šfar	Šfar	k1gInSc1
<g/>
'	'	kIx"
<g/>
am	am	k?
</s>
<s>
Tamra	Tamra	k6eAd1
</s>
<s>
Tiberias	Tiberias	k1gInSc1
Místní	místní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
</s>
<s>
Abu	Abu	k?
Snan	Snan	k1gInSc1
</s>
<s>
Ajlabún	Ajlabún	k1gMnSc1
</s>
<s>
Arrába	Arrába	k1gFnSc1
</s>
<s>
Basmat	Basmat	k1gInSc1
Tab	tab	kA
<g/>
'	'	kIx"
<g/>
un	un	k?
</s>
<s>
Bejt	Bejt	k?
Džan	Džan	k1gInSc1
</s>
<s>
Bi	Bi	k?
<g/>
'	'	kIx"
<g/>
ina	ina	k?
</s>
<s>
Bir	Bir	k?
al-Maksur	al-Maksur	k1gMnSc1
</s>
<s>
Bu	bu	kA
<g/>
'	'	kIx"
<g/>
ejne-Nudžejdat	ejne-Nudžejdat	k5eAaImF,k5eAaPmF
</s>
<s>
Buk	buk	k1gInSc1
<g/>
'	'	kIx"
<g/>
ata	ata	k?
</s>
<s>
Daburija	Daburija	k6eAd1
</s>
<s>
Dejr	Dejr	k1gMnSc1
al-Asad	al-Asad	k6eAd1
</s>
<s>
Dejr	Dejr	k1gInSc1
Channa	Chann	k1gInSc2
</s>
<s>
Džiš	Džiš	k5eAaPmIp2nS
</s>
<s>
Džudejda-Makr	Džudejda-Makr	k1gMnSc1
</s>
<s>
Džulis	Džulis	k1gFnSc1
</s>
<s>
Ejn	Ejn	k?
Kinije	Kinije	k1gFnSc1
</s>
<s>
Ejn	Ejn	k?
Mahil	Mahil	k1gInSc1
</s>
<s>
Fassuta	Fassut	k2eAgFnSc1d1
</s>
<s>
Ghadžar	Ghadžar	k1gMnSc1
</s>
<s>
Chevel	Chevel	k1gInSc1
Tefen	Tefna	k1gFnPc2
</s>
<s>
Chacor	Chacor	k1gInSc1
ha-Gelilit	ha-Gelilita	k1gFnPc2
</s>
<s>
Churfejš	Churfejš	k5eAaPmIp2nS
</s>
<s>
I	i	k9
<g/>
'	'	kIx"
<g/>
billin	billin	k1gInSc1
</s>
<s>
Iksal	Iksal	k1gMnSc1
</s>
<s>
Ilut	Ilut	k1gMnSc1
</s>
<s>
Jafa	Jaf	k2eAgFnSc1d1
an-Naserija	an-Naserija	k1gFnSc1
</s>
<s>
Januch-Džat	Januch-Džat	k1gInSc1
</s>
<s>
Javne	Javnout	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
</s>
<s>
Jesud	Jesud	k1gMnSc1
ha-Ma	ha-Ma	k1gMnSc1
<g/>
'	'	kIx"
<g/>
ala	ala	k1gFnSc1
</s>
<s>
Jirka	Jirka	k1gFnSc1
</s>
<s>
Ka	Ka	k?
<g/>
'	'	kIx"
<g/>
abija-Tabaš-Chadžadžra	abija-Tabaš-Chadžadžra	k1gMnSc1
</s>
<s>
Kabul	Kabul	k1gInSc1
</s>
<s>
Kacrin	Kacrin	k1gInSc1
</s>
<s>
Kafr	Kafr	k1gMnSc1
Jasif	Jasif	k1gMnSc1
</s>
<s>
Kafr	Kafr	k1gMnSc1
Kama	Kama	k1gMnSc1
</s>
<s>
Kafr	kafr	k1gInSc1
Kanna	Kann	k1gInSc2
</s>
<s>
Kafr	kafr	k1gInSc1
Manda	Manda	k1gFnSc1
</s>
<s>
Kaukab	Kaukab	k1gMnSc1
Abú	abú	k1gMnSc1
al-Hídžá	al-Hídžat	k5eAaPmIp3nS,k5eAaImIp3nS,k5eAaBmIp3nS
</s>
<s>
Kfar	Kfar	k1gMnSc1
Tavor	Tavor	k1gMnSc1
</s>
<s>
Kfar	Kfar	k1gMnSc1
Vradim	Vradim	k1gMnSc1
</s>
<s>
Kisra-Sumej	Kisra-Sumat	k5eAaPmRp2nS
</s>
<s>
Madžd	Madžd	k1gMnSc1
al-Kurúm	al-Kurúm	k1gMnSc1
</s>
<s>
Madždal	Madždat	k5eAaPmAgInS,k5eAaImAgInS
Šams	Šams	k1gInSc1
</s>
<s>
Maghar	Maghar	k1gMnSc1
</s>
<s>
Mas	masa	k1gFnPc2
<g/>
'	'	kIx"
<g/>
ade	ade	k?
</s>
<s>
Mašhad	Mašhad	k6eAd1
</s>
<s>
Mazra	Mazra	k1gFnSc1
<g/>
'	'	kIx"
<g/>
a	a	k8xC
</s>
<s>
Metula	Metula	k1gFnSc1
</s>
<s>
Migdal	Migdat	k5eAaImAgMnS,k5eAaPmAgMnS
</s>
<s>
Mi	já	k3xPp1nSc3
<g/>
'	'	kIx"
<g/>
ilja	ilja	k6eAd1
</s>
<s>
Nahf	Nahf	k1gMnSc1
</s>
<s>
Peki	Peki	k1gNnSc1
<g/>
'	'	kIx"
<g/>
in	in	k?
</s>
<s>
Rama	Rama	k6eAd1
</s>
<s>
Ramat	Ramat	k2eAgInSc1d1
Jišaj	Jišaj	k1gInSc1
</s>
<s>
Rejne	Rejn	k1gInSc5
</s>
<s>
Roš	Roš	k?
Pina	Pina	k1gFnSc1
</s>
<s>
Sadžur	Sadžur	k1gMnSc1
</s>
<s>
Ša	Ša	k?
<g/>
'	'	kIx"
<g/>
ab	ab	k?
</s>
<s>
Šiblí	Šiblý	k2eAgMnPc1d1
–	–	k?
Umm	Umm	k1gMnPc1
al-Ghanam	al-Ghanam	k6eAd1
</s>
<s>
Šlomi	Šlo	k1gFnPc7
</s>
<s>
Tuba-Zangarija	Tuba-Zangarija	k6eAd1
</s>
<s>
Tur	tur	k1gMnSc1
<g/>
'	'	kIx"
<g/>
an	an	k?
</s>
<s>
Zarzir	Zarzir	k1gInSc1
Oblastní	oblastní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
</s>
<s>
al-Batúf	al-Batúf	k1gMnSc1
</s>
<s>
Emek	Emek	k1gInSc1
ha-ma	ha-mum	k1gNnSc2
<g/>
'	'	kIx"
<g/>
ajanot	ajanota	k1gFnPc2
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Bik	bika	k1gFnPc2
<g/>
'	'	kIx"
<g/>
at	at	k?
Bejt	Bejt	k?
Še	Še	k1gFnSc1
<g/>
'	'	kIx"
<g/>
an	an	k?
<g/>
)	)	kIx)
</s>
<s>
Bustán	Bustán	k1gMnSc1
al-mardž	al-mardž	k6eAd1
</s>
<s>
Emek	Emek	k1gInSc1
ha-Jarden	ha-Jardna	k1gFnPc2
</s>
<s>
Gilboa	Gilboa	k6eAd1
</s>
<s>
Golan	Golan	k1gMnSc1
</s>
<s>
Dolní	dolní	k2eAgFnSc1d1
Galilea	Galilea	k1gFnSc1
</s>
<s>
Horní	horní	k2eAgFnSc1d1
Galilea	Galilea	k1gFnSc1
</s>
<s>
Jizre	Jizr	k1gMnSc5
<g/>
'	'	kIx"
<g/>
elské	elská	k1gFnSc3
údolí	údolí	k1gNnSc2
</s>
<s>
Ma	Ma	k?
<g/>
'	'	kIx"
<g/>
ale	ale	k8xC
Josef	Josef	k1gMnSc1
</s>
<s>
Mate	mást	k5eAaImIp3nS
Ašer	Ašer	k1gInSc1
</s>
<s>
Megido	Megida	k1gFnSc5
</s>
<s>
Merom	Merom	k1gInSc1
ha-Galil	ha-Galit	k5eAaImAgInS,k5eAaPmAgInS
</s>
<s>
Mevo	Mevo	k1gMnSc1
<g/>
'	'	kIx"
<g/>
ot	ot	k1gMnSc1
ha-Chermon	ha-Chermon	k1gMnSc1
</s>
<s>
Misgav	Misgav	k1gInSc1
</s>
<s>
zrušené	zrušený	k2eAgFnPc1d1
</s>
<s>
Ga	Ga	k?
<g/>
'	'	kIx"
<g/>
aton	aton	k1gMnSc1
</s>
<s>
Jizre	Jizr	k1gMnSc5
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
</s>
<s>
Kišon	Kišon	k1gMnSc1
</s>
<s>
Merkaz	Merkaz	k1gInSc1
ha-Galil	ha-Galit	k5eAaImAgInS,k5eAaPmAgInS
</s>
<s>
Na	na	k7c4
<g/>
'	'	kIx"
<g/>
aman	aman	k1gNnSc4
</s>
<s>
Nof	Nof	k?
ha-Galil	ha-Galit	k5eAaImAgInS,k5eAaPmAgInS
</s>
<s>
Sulam	Sulam	k1gInSc1
Cur	Cur	k1gMnSc2
Další	další	k2eAgInPc1d1
distrikty	distrikt	k1gInPc7
<g/>
:	:	kIx,
</s>
<s>
Centrální	centrální	k2eAgInSc1d1
distrikt	distrikt	k1gInSc1
</s>
<s>
Haifský	haifský	k2eAgInSc1d1
distrikt	distrikt	k1gInSc1
</s>
<s>
Jeruzalémský	jeruzalémský	k2eAgInSc1d1
distrikt	distrikt	k1gInSc1
</s>
<s>
Jižní	jižní	k2eAgInSc1d1
distrikt	distrikt	k1gInSc1
</s>
<s>
Judea	Judea	k1gFnSc1
a	a	k8xC
Samaří	Samaří	k1gNnSc1
</s>
<s>
Telavivský	telavivský	k2eAgInSc1d1
distrikt	distrikt	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Izrael	Izrael	k1gInSc1
</s>
