<s>
James	James	k1gMnSc1	James
Francis	Francis	k1gFnSc2	Francis
Cameron	Cameron	k1gMnSc1	Cameron
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
Ontario	Ontario	k1gNnSc1	Ontario
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
trojnásobný	trojnásobný	k2eAgMnSc1d1	trojnásobný
držitel	držitel	k1gMnSc1	držitel
Oscara	Oscar	k1gMnSc2	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Režíruje	režírovat	k5eAaImIp3nS	režírovat
většinou	většina	k1gFnSc7	většina
akční	akční	k2eAgFnSc7d1	akční
nebo	nebo	k8xC	nebo
sci-fi	scii	k1gFnSc7	sci-fi
filmy	film	k1gInPc7	film
<g/>
,	,	kIx,	,
tématem	téma	k1gNnSc7	téma
bývají	bývat	k5eAaImIp3nP	bývat
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
člověkem	člověk	k1gMnSc7	člověk
a	a	k8xC	a
technologiemi	technologie	k1gFnPc7	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
režisérem	režisér	k1gMnSc7	režisér
a	a	k8xC	a
tvůrcem	tvůrce	k1gMnSc7	tvůrce
scénáře	scénář	k1gInSc2	scénář
dvou	dva	k4xCgInPc2	dva
nejvýdělečnějších	výdělečný	k2eAgInPc2d3	nejvýdělečnější
filmů	film	k1gInPc2	film
historie	historie	k1gFnSc2	historie
kinematografie	kinematografie	k1gFnSc1	kinematografie
<g/>
,	,	kIx,	,
Avatara	Avatar	k1gMnSc4	Avatar
a	a	k8xC	a
Titanicu	Titanica	k1gMnSc4	Titanica
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
sérii	série	k1gFnSc4	série
filmů	film	k1gInPc2	film
o	o	k7c6	o
Terminátorovi	terminátor	k1gMnSc6	terminátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roli	role	k1gFnSc6	role
šéfproducenta	šéfproducent	k1gMnSc2	šéfproducent
natočil	natočit	k5eAaBmAgInS	natočit
dokument	dokument	k1gInSc4	dokument
Ztracený	ztracený	k2eAgInSc1d1	ztracený
Ježíšův	Ježíšův	k2eAgInSc1d1	Ježíšův
hrob	hrob	k1gInSc1	hrob
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Lost	Lost	k1gMnSc1	Lost
Tomb	Tomb	k1gMnSc1	Tomb
of	of	k?	of
Jesus	Jesus	k1gMnSc1	Jesus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
odvysílal	odvysílat	k5eAaPmAgInS	odvysílat
Discovery	Discover	k1gInPc4	Discover
Channel	Channel	k1gInSc1	Channel
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
Titaniku	Titanic	k1gInSc2	Titanic
se	se	k3xPyFc4	se
od	od	k7c2	od
filmu	film	k1gInSc2	film
na	na	k7c4	na
dvanáct	dvanáct	k4xCc4	dvanáct
let	léto	k1gNnPc2	léto
odmlčel	odmlčet	k5eAaPmAgMnS	odmlčet
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
objevných	objevný	k2eAgFnPc2d1	objevná
podmořských	podmořský	k2eAgFnPc2d1	podmořská
expedicí	expedice	k1gFnPc2	expedice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
na	na	k7c4	na
filmová	filmový	k2eAgNnPc4d1	filmové
plátna	plátno	k1gNnPc4	plátno
opět	opět	k6eAd1	opět
vrací	vracet	k5eAaImIp3nS	vracet
se	s	k7c7	s
sci-fi	scii	k1gNnSc7	sci-fi
snímkem	snímek	k1gInSc7	snímek
Avatar	Avatara	k1gFnPc2	Avatara
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
třetím	třetí	k4xOgMnSc7	třetí
člověkem	člověk	k1gMnSc7	člověk
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
ponořil	ponořit	k5eAaPmAgInS	ponořit
na	na	k7c4	na
dno	dno	k1gNnSc4	dno
Mariánského	mariánský	k2eAgInSc2d1	mariánský
příkopu	příkop	k1gInSc2	příkop
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
ponořil	ponořit	k5eAaPmAgInS	ponořit
do	do	k7c2	do
osmikilometrové	osmikilometrový	k2eAgFnSc2d1	osmikilometrová
hloubky	hloubka	k1gFnSc2	hloubka
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
Papuy-Nové	Papuy-Nové	k2eAgFnSc2d1	Papuy-Nové
Guiney	Guinea	k1gFnSc2	Guinea
a	a	k8xC	a
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
pak	pak	k6eAd1	pak
pokořil	pokořit	k5eAaPmAgMnS	pokořit
hloubku	hloubka	k1gFnSc4	hloubka
10	[number]	k4	10
898	[number]	k4	898
m	m	kA	m
v	v	k7c6	v
Mariánském	mariánský	k2eAgInSc6d1	mariánský
příkopu	příkop	k1gInSc6	příkop
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
případech	případ	k1gInPc6	případ
použil	použít	k5eAaPmAgInS	použít
batyskaf	batyskaf	k1gInSc1	batyskaf
Deepsea	Deepseum	k1gNnSc2	Deepseum
Challenger	Challengra	k1gFnPc2	Challengra
a	a	k8xC	a
během	během	k7c2	během
pobytu	pobyt	k1gInSc2	pobyt
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
pomocí	pomocí	k7c2	pomocí
dvojice	dvojice	k1gFnSc2	dvojice
stereoskopických	stereoskopický	k2eAgFnPc2d1	stereoskopická
kamer	kamera	k1gFnPc2	kamera
na	na	k7c6	na
výložnících	výložník	k1gInPc6	výložník
natáčel	natáčet	k5eAaImAgMnS	natáčet
unikátní	unikátní	k2eAgInPc4d1	unikátní
záběry	záběr	k1gInPc4	záběr
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
další	další	k2eAgInPc4d1	další
dokumentární	dokumentární	k2eAgInPc4d1	dokumentární
i	i	k8xC	i
hrané	hraný	k2eAgInPc4d1	hraný
filmy	film	k1gInPc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
James	James	k1gInSc1	James
Cameron	Cameron	k1gInSc4	Cameron
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
James	James	k1gInSc1	James
Cameron	Cameron	k1gInSc1	Cameron
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
James	James	k1gMnSc1	James
Cameron	Cameron	k1gMnSc1	Cameron
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
James	James	k1gMnSc1	James
Cameron	Cameron	k1gMnSc1	Cameron
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
