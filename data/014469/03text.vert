<s>
Andrea	Andrea	k1gFnSc1
Sacchi	Sacch	k1gFnSc2
</s>
<s>
Andrea	Andrea	k1gFnSc1
Sacchi	Sacchi	k1gNnSc2
Portrét	portrét	k1gInSc1
od	od	k7c2
Carlo	Carlo	k1gNnSc4
Maratta	Maratta	k1gFnSc1
Narození	narození	k1gNnSc2
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1599	#num#	k4
Řím	Řím	k1gInSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1661	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
61	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Řím	Řím	k1gInSc1
Národnost	národnost	k1gFnSc1
</s>
<s>
italská	italský	k2eAgNnPc1d1
Povolání	povolání	k1gNnPc1
</s>
<s>
barokní	barokní	k2eAgMnSc1d1
malíř	malíř	k1gMnSc1
Děti	dítě	k1gFnPc1
</s>
<s>
Giuseppe	Giuseppat	k5eAaPmIp3nS
Sacchi	Sacche	k1gFnSc4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
databázi	databáze	k1gFnSc6
Národní	národní	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Andrea	Andrea	k1gFnSc1
Sacchi	Sacch	k1gFnSc2
(	(	kIx(
<g/>
30	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1599	#num#	k4
<g/>
,	,	kIx,
Řím	Řím	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
21	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1661	#num#	k4
<g/>
,	,	kIx,
tamtéž	tamtéž	k6eAd1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
italský	italský	k2eAgMnSc1d1
barokní	barokní	k2eAgMnSc1d1
malíř	malíř	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
pracoval	pracovat	k5eAaImAgMnS
v	v	k7c6
Římě	Řím	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
umělce	umělec	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
sdíleli	sdílet	k5eAaImAgMnP
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
malířský	malířský	k2eAgInSc4d1
styl	styl	k1gInSc4
<g/>
,	,	kIx,
patřili	patřit	k5eAaImAgMnP
Nicolas	Nicolas	k1gMnSc1
Poussin	Poussin	k1gMnSc1
<g/>
,	,	kIx,
Giovanni	Giovann	k1gMnPc1
Battista	Battista	k1gMnSc1
Passeri	Passer	k1gMnPc1
<g/>
,	,	kIx,
sochaři	sochař	k1gMnPc1
Alessandro	Alessandra	k1gFnSc5
Algardi	Algard	k1gMnPc1
a	a	k8xC
François	François	k1gFnPc1
Duquesnoy	Duquesno	k2eAgFnPc1d1
a	a	k8xC
tehdejší	tehdejší	k2eAgMnSc1d1
životopisec	životopisec	k1gMnSc1
Giovanni	Giovanň	k1gMnSc3
Bellori	Bellor	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sacchiho	Sacchiha	k1gFnSc5
patronem	patron	k1gMnSc7
byl	být	k5eAaImAgMnS
italský	italský	k2eAgMnSc1d1
kardinál	kardinál	k1gMnSc1
Francesco	Francesco	k1gMnSc1
Maria	Maria	k1gFnSc1
del	del	k?
Monte	Mont	k1gInSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Andrea	Andrea	k1gFnSc1
Sacchi	Sacchi	k1gNnSc2
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
v	v	k7c6
Římě	Řím	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc3
otec	otec	k1gMnSc1
Benedetto	Benedetto	k1gNnSc1
<g/>
,	,	kIx,
sám	sám	k3xTgMnSc1
průměrný	průměrný	k2eAgMnSc1d1
malíř	malíř	k1gMnSc1
<g/>
,	,	kIx,
brzy	brzy	k6eAd1
rozeznal	rozeznat	k5eAaPmAgInS
talent	talent	k1gInSc1
svého	svůj	k3xOyFgMnSc4
syna	syn	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
životopisce	životopisec	k1gMnSc2
Giovanni	Giovanň	k1gMnSc5
Pietro	Pietra	k1gMnSc5
Belloriho	Belloriha	k1gMnSc5
(	(	kIx(
<g/>
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
byl	být	k5eAaImAgMnS
také	také	k6eAd1
velkým	velký	k2eAgMnSc7d1
přítelem	přítel	k1gMnSc7
Sacchiho	Sacchi	k1gMnSc2
<g/>
)	)	kIx)
Benedetto	Benedetto	k1gNnSc1
navrhl	navrhnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
jeho	jeho	k3xOp3gMnSc1
syna	syn	k1gMnSc4
učil	učit	k5eAaImAgMnS,k5eAaPmAgMnS
Giuseppe	Giusepp	k1gMnSc5
Cesari	Cesar	k1gMnSc5
<g/>
,	,	kIx,
zvaný	zvaný	k2eAgInSc1d1
také	také	k9
Cavaliere	Cavalier	k1gInSc5
d	d	k?
<g/>
'	'	kIx"
<g/>
Arpino	Arpino	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Toto	tento	k3xDgNnSc1
jsou	být	k5eAaImIp3nP
Belloriho	Bellori	k1gMnSc4
slova	slovo	k1gNnPc1
<g/>
:	:	kIx,
</s>
<s>
...	...	k?
tehdy	tehdy	k6eAd1
Benedetto	Benedett	k2eAgNnSc1d1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
<g/>
,	,	kIx,
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
syn	syn	k1gMnSc1
ho	on	k3xPp3gNnSc4
přes	přes	k7c4
své	svůj	k3xOyFgNnSc4
mládí	mládí	k1gNnSc4
překonal	překonat	k5eAaPmAgMnS
<g/>
,	,	kIx,
a	a	k8xC
netroufal	troufat	k5eNaImAgMnS
si	se	k3xPyFc3
ho	on	k3xPp3gInSc4
dále	daleko	k6eAd2
učit	učit	k5eAaImF
sám	sám	k3xTgMnSc1
<g/>
;	;	kIx,
moudře	moudřit	k5eAaImSgMnS
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgMnS
poskytnout	poskytnout	k5eAaPmF
mu	on	k3xPp3gMnSc3
lepšího	dobrý	k2eAgMnSc2d2
učitele	učitel	k1gMnSc2
a	a	k8xC
doporučil	doporučit	k5eAaPmAgMnS
mu	on	k3xPp3gMnSc3
Giuseppe	Giusepp	k1gInSc5
d	d	k?
<g/>
'	'	kIx"
<g/>
Arpina	Arpina	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
ho	on	k3xPp3gNnSc4
rád	rád	k6eAd1
vzal	vzít	k5eAaPmAgMnS
do	do	k7c2
své	svůj	k3xOyFgFnSc2
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Giuseppe	Giusepp	k1gInSc5
d	d	k?
<g/>
'	'	kIx"
<g/>
Arpino	Arpino	k1gNnSc4
brzy	brzy	k6eAd1
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Sacchi	Sacchi	k1gNnSc3
je	být	k5eAaImIp3nS
pozornější	pozorný	k2eAgMnPc1d2
a	a	k8xC
snaživější	snaživý	k2eAgMnPc1d2
než	než	k8xS
jiní	jiný	k2eAgMnPc1d1
jeho	jeho	k3xOp3gMnPc1
žáci	žák	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sacchi	Sacchi	k6eAd1
později	pozdě	k6eAd2
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
malířské	malířský	k2eAgFnSc2d1
dílny	dílna	k1gFnSc2
Francesca	Francescus	k1gMnSc2
Albaniho	Albani	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinu	většina	k1gFnSc4
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
strávil	strávit	k5eAaPmAgMnS
v	v	k7c6
Římě	Řím	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
také	také	k9
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počátku	počátek	k1gInSc6
jeho	jeho	k3xOp3gFnSc2
umělecké	umělecký	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
mu	on	k3xPp3gMnSc3
hodně	hodně	k6eAd1
pomohl	pomoct	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
patron	patron	k1gMnSc1
kardinál	kardinál	k1gMnSc1
Antonio	Antonio	k1gMnSc1
Barberini	Barberin	k2eAgMnPc1d1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
mu	on	k3xPp3gMnSc3
svěřil	svěřit	k5eAaPmAgMnS
zakázku	zakázka	k1gFnSc4
pro	pro	k7c4
kapucínský	kapucínský	k2eAgInSc4d1
kostel	kostel	k1gInSc4
v	v	k7c6
Římě	Řím	k1gInSc6
a	a	k8xC
zakázku	zakázka	k1gFnSc4
pro	pro	k7c4
Palazzo	Palazza	k1gFnSc5
Barberini	Barberin	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
jeho	jeho	k3xOp3gMnSc4,k3xPp3gMnSc4
současníka	současník	k1gMnSc4
a	a	k8xC
uměleckého	umělecký	k2eAgMnSc4d1
rivala	rival	k1gMnSc4
Pietra	Pietr	k1gMnSc4
da	da	k?
Cortona	Corton	k1gMnSc4
<g/>
,	,	kIx,
Sacchi	Sacche	k1gFnSc4
studoval	studovat	k5eAaImAgMnS
obrazy	obraz	k1gInPc1
Raffaela	Raffael	k1gMnSc2
Santi	Sanť	k1gFnSc2
(	(	kIx(
<g/>
Raffaello	Raffaello	k1gNnSc4
Sanzio	Sanzio	k6eAd1
da	da	k?
Urbino	Urbino	k1gNnSc1
<g/>
,	,	kIx,
1483	#num#	k4
<g/>
–	–	k?
<g/>
1520	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
vliv	vliv	k1gInSc4
tohoto	tento	k3xDgMnSc2
malíře	malíř	k1gMnSc2
je	být	k5eAaImIp3nS
patrný	patrný	k2eAgInSc1d1
v	v	k7c6
řadě	řada	k1gFnSc6
Sacchiho	Sacchi	k1gMnSc4
prací	práce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sacchi	Sacchi	k1gNnPc7
údajně	údajně	k6eAd1
cestoval	cestovat	k5eAaImAgMnS
do	do	k7c2
Benátek	Benátky	k1gFnPc2
a	a	k8xC
Parmy	Parma	k1gFnSc2
a	a	k8xC
studoval	studovat	k5eAaImAgMnS
díla	dílo	k1gNnPc4
Correggiova	Correggiův	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Dvě	dva	k4xCgFnPc1
z	z	k7c2
jeho	jeho	k3xOp3gNnPc2
hlavních	hlavní	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
na	na	k7c6
plátně	plátno	k1gNnSc6
jsou	být	k5eAaImIp3nP
oltářní	oltářní	k2eAgInPc1d1
obrazy	obraz	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
nyní	nyní	k6eAd1
v	v	k7c4
Pinacoteca	Pinacotecus	k1gMnSc4
Vaticana	Vatican	k1gMnSc4
<g/>
,	,	kIx,
malířské	malířský	k2eAgFnSc6d1
galerii	galerie	k1gFnSc6
ve	v	k7c6
Vatikánu	Vatikán	k1gInSc6
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
hlavní	hlavní	k2eAgNnSc1d1
díla	dílo	k1gNnPc1
níže	níže	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Diskuse	diskuse	k1gFnSc1
s	s	k7c7
malířem	malíř	k1gMnSc7
Pietro	Pietro	k1gNnSc4
da	da	k?
Cortona	Cortona	k1gFnSc1
</s>
<s>
Jako	jako	k8xS,k8xC
mladík	mladík	k1gMnSc1
Sacchi	Sacch	k1gFnSc2
spolupracoval	spolupracovat	k5eAaImAgMnS
s	s	k7c7
Pietrem	Pietr	k1gInSc7
da	da	k?
Cortonou	Cortona	k1gFnSc7
ve	v	k7c6
Villa	Vill	k1gMnSc2
Sacchetti	Sacchetť	k1gFnSc6
v	v	k7c6
Castelfusano	Castelfusana	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
řadě	řada	k1gFnSc6
veřejných	veřejný	k2eAgFnPc2d1
debat	debata	k1gFnPc2
v	v	k7c6
římské	římský	k2eAgFnSc6d1
gildě	gilda	k1gFnSc6
Sacchi	Sacch	k1gFnSc2
velmi	velmi	k6eAd1
kritizoval	kritizovat	k5eAaImAgMnS
Cortonovu	Cortonův	k2eAgFnSc4d1
nevázanost	nevázanost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
význam	význam	k1gInSc4
jejich	jejich	k3xOp3gFnSc2
pře	pře	k1gFnSc2
tkví	tkvět	k5eAaImIp3nS
především	především	k9
v	v	k7c6
rozdílných	rozdílný	k2eAgInPc6d1
názorech	názor	k1gInPc6
těchto	tento	k3xDgInPc6
dvou	dva	k4xCgMnPc2
předních	přední	k2eAgMnPc2d1
zastánců	zastánce	k1gMnPc2
hlavních	hlavní	k2eAgInPc2d1
malířských	malířský	k2eAgInPc2d1
stylů	styl	k1gInPc2
<g/>
,	,	kIx,
stylu	styl	k1gInSc2
zvaného	zvaný	k2eAgInSc2d1
klasický	klasický	k2eAgInSc4d1
a	a	k8xC
barokní	barokní	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sacchi	Sacch	k1gInSc6
obhajoval	obhajovat	k5eAaImAgMnS
názor	názor	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
každá	každý	k3xTgFnSc1
postava	postava	k1gFnSc1
v	v	k7c6
kompozici	kompozice	k1gFnSc6
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
přiřazena	přiřazen	k2eAgFnSc1d1
jedinečnému	jedinečný	k2eAgInSc3d1
individuálnímu	individuální	k2eAgInSc3d1
výrazu	výraz	k1gInSc3
<g/>
,	,	kIx,
gestu	gesto	k1gNnSc3
a	a	k8xC
pohybu	pohyb	k1gInSc3
<g/>
,	,	kIx,
měla	mít	k5eAaImAgFnS
by	by	kYmCp3nS
kompozice	kompozice	k1gFnSc1
obsahovat	obsahovat	k5eAaImF
jen	jen	k9
několik	několik	k4yIc4
postav	postava	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
přeplněných	přeplněný	k2eAgFnPc6d1
kompozicích	kompozice	k1gFnPc6
jsou	být	k5eAaImIp3nP
postavy	postava	k1gFnPc1
zbaveny	zbaven	k2eAgFnPc1d1
individuality	individualita	k1gFnPc1
a	a	k8xC
celkové	celkový	k2eAgNnSc1d1
vyznění	vyznění	k1gNnSc1
obrazu	obraz	k1gInSc2
se	se	k3xPyFc4
tím	ten	k3xDgInSc7
ztrácí	ztrácet	k5eAaImIp3nS
<g/>
,	,	kIx,
téma	téma	k1gNnSc1
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
nejasným	jasný	k2eNgMnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgInPc6
ohledech	ohled	k1gInPc6
to	ten	k3xDgNnSc4
byla	být	k5eAaImAgFnS
také	také	k9
reakce	reakce	k1gFnSc1
na	na	k7c4
velkou	velký	k2eAgFnSc4d1
oblíbenost	oblíbenost	k1gFnSc4
davových	davový	k2eAgFnPc2d1
scén	scéna	k1gFnPc2
v	v	k7c6
dílech	dílo	k1gNnPc6
Taddea	Taddeum	k1gNnSc2
Zuccari	Zuccar	k1gFnSc2
v	v	k7c6
minulé	minulý	k2eAgFnSc6d1
generaci	generace	k1gFnSc6
a	a	k8xC
jeho	on	k3xPp3gMnSc2,k3xOp3gMnSc2
současníka	současník	k1gMnSc2
Pietra	Pietrum	k1gNnSc2
da	da	k?
Cortony	Corton	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednoduchost	jednoduchost	k1gFnSc1
a	a	k8xC
jednota	jednota	k1gFnSc1
byly	být	k5eAaImAgFnP
pro	pro	k7c4
Sacchiho	Sacchi	k1gMnSc4
zásadní	zásadní	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cortona	Cortona	k1gFnSc1
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
protinávrhu	protinávrh	k1gInSc6
hájil	hájit	k5eAaImAgMnS
názor	názor	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
velké	velký	k2eAgInPc1d1
obrazy	obraz	k1gInPc1
se	s	k7c7
spoustou	spousta	k1gFnSc7
osobností	osobnost	k1gFnSc7
představují	představovat	k5eAaImIp3nP
epické	epický	k2eAgNnSc4d1
ztvárnění	ztvárnění	k1gNnSc4
námětu	námět	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
schopno	schopen	k2eAgNnSc1d1
rozvinout	rozvinout	k5eAaPmF
několik	několik	k4yIc1
dalších	další	k2eAgNnPc2d1
dílčích	dílčí	k2eAgNnPc2d1
témat	téma	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
pro	pro	k7c4
Sacchiho	Sacchi	k1gMnSc4
obraz	obraz	k1gInSc4
s	s	k7c7
přehnanými	přehnaný	k2eAgInPc7d1
dekorativními	dekorativní	k2eAgInPc7d1
detaily	detail	k1gInPc7
<g/>
,	,	kIx,
včetně	včetně	k7c2
zobrazení	zobrazení	k1gNnSc2
davů	dav	k1gInPc2
<g/>
,	,	kIx,
představoval	představovat	k5eAaImAgInS
spíše	spíše	k9
tapetový	tapetový	k2eAgInSc1d1
papír	papír	k1gInSc1
než	než	k8xS
epický	epický	k2eAgInSc1d1
příběh	příběh	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
zastánce	zastánce	k1gMnSc4
Sacchiho	Sacchi	k1gMnSc4
zásady	zásada	k1gFnSc2
jednoduchosti	jednoduchost	k1gFnSc2
a	a	k8xC
zaměření	zaměření	k1gNnSc2
na	na	k7c4
námět	námět	k1gInSc4
byli	být	k5eAaImAgMnP
jeho	jeho	k3xOp3gMnPc1
přátelé	přítel	k1gMnPc1
<g/>
,	,	kIx,
sochař	sochař	k1gMnSc1
Algardi	Algard	k1gMnPc1
a	a	k8xC
malíř	malíř	k1gMnSc1
Poussin	Poussin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kontroverze	kontroverze	k1gFnSc1
však	však	k9
nebyla	být	k5eNaImAgFnS
tak	tak	k6eAd1
ostrá	ostrý	k2eAgFnSc1d1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
někteří	některý	k3yIgMnPc1
naznačují	naznačovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Sacchi	Sacch	k1gMnPc1
a	a	k8xC
Albani	Alban	k1gMnPc1
sdíleli	sdílet	k5eAaImAgMnP
také	také	k9
nespokojenost	nespokojenost	k1gFnSc4
s	s	k7c7
žánrovou	žánrový	k2eAgFnSc7d1
malbou	malba	k1gFnSc7
<g/>
,	,	kIx,
tedy	tedy	k8xC
se	se	k3xPyFc4
zobrazením	zobrazení	k1gNnSc7
výjevů	výjev	k1gInPc2
z	z	k7c2
života	život	k1gInSc2
prostých	prostý	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
obrazy	obraz	k1gInPc4
s	s	k7c7
předměty	předmět	k1gInPc7
každodenního	každodenní	k2eAgNnSc2d1
užívání	užívání	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc4,k3yQnSc4
byla	být	k5eAaImAgNnP
témata	téma	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yIgNnPc4,k3yRgNnPc4,k3yQgNnPc4
upřednostňovali	upřednostňovat	k5eAaImAgMnP
Bamboccianti	Bambocciant	k1gMnPc1
a	a	k8xC
dokonce	dokonce	k9
i	i	k9
Caravaggisté	Caravaggistý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
jejich	jejich	k3xOp3gInSc2
názoru	názor	k1gInSc2
by	by	kYmCp3nS
se	se	k3xPyFc4
umění	umění	k1gNnSc1
mělo	mít	k5eAaImAgNnS
soustředit	soustředit	k5eAaPmF
na	na	k7c4
ušlechtilá	ušlechtilý	k2eAgNnPc4d1
témata	téma	k1gNnPc4
–	–	k?
náměty	námět	k1gInPc1
biblické	biblický	k2eAgInPc1d1
<g/>
,	,	kIx,
mytologické	mytologický	k2eAgInPc1d1
nebo	nebo	k8xC
témata	téma	k1gNnPc1
z	z	k7c2
klasické	klasický	k2eAgFnSc2d1
dávné	dávný	k2eAgFnSc2d1
historie	historie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Sacchi	Sacch	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
pracoval	pracovat	k5eAaImAgMnS
téměř	téměř	k6eAd1
po	po	k7c4
celý	celý	k2eAgInSc4d1
život	život	k1gInSc4
Římě	Řím	k1gInSc6
<g/>
,	,	kIx,
zanechal	zanechat	k5eAaPmAgInS
několik	několik	k4yIc4
svých	svůj	k3xOyFgInPc2
obrazů	obraz	k1gInPc2
v	v	k7c6
soukromých	soukromý	k2eAgFnPc6d1
galeriích	galerie	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
vzkvétající	vzkvétající	k2eAgFnSc4d1
školu	škola	k1gFnSc4
<g/>
:	:	kIx,
Carlo	Carlo	k1gNnSc4
Maratta	Maratto	k1gNnSc2
byl	být	k5eAaImAgMnS
jeho	jeho	k3xOp3gMnSc1
mladší	mladý	k2eAgMnSc1d2
spolupracovník	spolupracovník	k1gMnSc1
nebo	nebo	k8xC
žák	žák	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sacchho	Sacchha	k1gFnSc5
dílem	dílo	k1gNnSc7
byli	být	k5eAaImAgMnP
ovlivněni	ovlivněn	k2eAgMnPc1d1
takoví	takový	k3xDgMnPc1
umělci	umělec	k1gMnPc1
jako	jako	k9
Francesco	Francesco	k6eAd1
Fiorelli	Fiorelle	k1gFnSc4
<g/>
,	,	kIx,
Luigi	Luige	k1gFnSc4
Garzi	Garze	k1gFnSc4
<g/>
,	,	kIx,
Francesco	Francesco	k6eAd1
Lauri	Lauri	k1gNnSc1
<g/>
,	,	kIx,
Andrea	Andrea	k1gFnSc1
Camassei	Camasse	k1gFnSc2
a	a	k8xC
Giacinto	Giacinto	k1gNnSc4
Gimignani	Gimignaň	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sacchiho	Sacchi	k1gMnSc2
vlastní	vlastní	k2eAgMnSc1d1
nemanželský	manželský	k2eNgMnSc1d1
syn	syn	k1gMnSc1
Giuseppe	Giusepp	k1gMnSc5
Sacchi	Sacch	k1gMnSc5
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
měl	mít	k5eAaImAgInS
před	před	k7c7
sebou	se	k3xPyFc7
nadějnou	nadějný	k2eAgFnSc4d1
budoucnost	budoucnost	k1gFnSc4
<g/>
,	,	kIx,
zemřel	zemřít	k5eAaPmAgMnS
mladý	mladý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Sacchi	Sacchi	k6eAd1
zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
Nettuno	Nettuna	k1gFnSc5
<g/>
,	,	kIx,
v	v	k7c6
římské	římský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
Lazio	Lazio	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1661	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
</s>
<s>
Alegorie	alegorie	k1gFnSc1
božské	božský	k2eAgFnSc2d1
moudrosti	moudrost	k1gFnSc2
</s>
<s>
Alegorie	alegorie	k1gFnSc1
božské	božský	k2eAgFnSc2d1
moudrosti	moudrost	k1gFnSc2
<g/>
,	,	kIx,
detail	detail	k1gInSc4
fresky	freska	k1gFnSc2
v	v	k7c6
Palazzo	Palazza	k1gFnSc5
Barberini	Barberin	k2eAgMnPc1d1
</s>
<s>
Tato	tento	k3xDgFnSc1
Sacciho	Sacci	k1gMnSc4
freska	fresko	k1gNnSc2
v	v	k7c6
Palazzo	Palazza	k1gFnSc5
Barberini	Barberin	k1gMnPc1
v	v	k7c6
Římě	Řím	k1gInSc6
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
jeho	jeho	k3xOp3gNnSc4
mistrovské	mistrovský	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
amerického	americký	k2eAgMnSc2d1
historika	historik	k1gMnSc2
umění	umění	k1gNnSc2
Josepha	Joseph	k1gMnSc2
Connorse	Connorse	k1gFnSc2
je	být	k5eAaImIp3nS
osobní	osobní	k2eAgInSc1d1
znak	znak	k1gInSc1
papeže	papež	k1gMnSc2
Urbana	Urban	k1gMnSc2
VIII	VIII	kA
<g/>
.	.	kIx.
–	–	k?
vycházející	vycházející	k2eAgNnSc4d1
Slunce	slunce	k1gNnSc4
–	–	k?
zobrazen	zobrazit	k5eAaPmNgInS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
návštěvník	návštěvník	k1gMnSc1
paláce	palác	k1gInSc2
viděl	vidět	k5eAaImAgMnS
Slunce	slunce	k1gNnSc4
Božské	božský	k2eAgFnSc2d1
moudrosti	moudrost	k1gFnSc2
a	a	k8xC
na	na	k7c6
trůnu	trůn	k1gInSc6
souhvězdí	souhvězdí	k1gNnSc2
lva	lev	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
pravostranného	pravostranný	k2eAgInSc2d1
pohledu	pohled	k1gInSc2
slunce	slunce	k1gNnSc1
Božské	božský	k2eAgFnSc2d1
moudrosti	moudrost	k1gFnSc2
vypadá	vypadat	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
vznášelo	vznášet	k5eAaImAgNnS
nad	nad	k7c7
kopulí	kopule	k1gFnSc7
kaple	kaple	k1gFnSc2
<g/>
,	,	kIx,
vyzařovalo	vyzařovat	k5eAaImAgNnS
dolů	dolů	k6eAd1
své	svůj	k3xOyFgNnSc4
blahodárné	blahodárný	k2eAgNnSc4d1
světlo	světlo	k1gNnSc4
<g/>
.	.	kIx.
„	„	k?
<g/>
...	...	k?
Scottova	Scottův	k2eAgFnSc1d1
astrologická	astrologický	k2eAgFnSc1d1
interpretace	interpretace	k1gFnSc1
<g/>
...	...	k?
je	být	k5eAaImIp3nS
přesvědčivá	přesvědčivý	k2eAgFnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
také	také	k9
politická	politický	k2eAgFnSc1d1
interpretace	interpretace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
příhodnému	příhodný	k2eAgNnSc3d1
spojení	spojení	k1gNnSc3
hvězd	hvězda	k1gFnPc2
ve	v	k7c6
dvou	dva	k4xCgInPc6
klíčových	klíčový	k2eAgInPc6d1
okamžicích	okamžik	k1gInPc6
narození	narození	k1gNnSc2
a	a	k8xC
volby	volba	k1gFnPc1
papeže	papež	k1gMnSc2
Urbana	Urban	k1gMnSc2
VIII	VIII	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
Barberiniové	Barberinius	k1gMnPc1
zrozeni	zrozen	k2eAgMnPc1d1
a	a	k8xC
předurčeni	předurčen	k2eAgMnPc1d1
vládnout	vládnout	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Campanella	Campanella	k1gMnSc1
by	by	kYmCp3nS
byl	být	k5eAaImAgMnS
mohl	moct	k5eAaImAgInS
papežovi	papež	k1gMnSc3
říct	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
když	když	k8xS
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
<g/>
,	,	kIx,
slunce	slunce	k1gNnSc1
vstoupilo	vstoupit	k5eAaPmAgNnS
do	do	k7c2
Velkého	velký	k2eAgNnSc2d1
konjunkčního	konjunkční	k2eAgNnSc2d1
spojení	spojení	k1gNnSc2
s	s	k7c7
Jupiterem	Jupiter	k1gMnSc7
(	(	kIx(
<g/>
jehož	jehož	k3xOyRp3gFnSc4
orlici	orlice	k1gFnSc4
ukazuje	ukazovat	k5eAaImIp3nS
Sacchi	Sacche	k1gFnSc4
ve	v	k7c6
spojení	spojení	k1gNnSc6
se	s	k7c7
sluncem	slunce	k1gNnSc7
a	a	k8xC
lvem	lev	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Synovec	synovec	k1gMnSc1
papeže	papež	k1gMnSc2
Urbana	Urban	k1gMnSc2
VIII	VIII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taddeo	Taddeo	k6eAd1
Barberini	Barberin	k1gMnPc1
<g/>
,	,	kIx,
patron	patron	k1gInSc1
tohoto	tento	k3xDgNnSc2
křídla	křídlo	k1gNnSc2
paláce	palác	k1gInSc2
<g/>
,	,	kIx,
do	do	k7c2
něhož	jenž	k3xRgMnSc2
rodina	rodina	k1gFnSc1
vkládala	vkládat	k5eAaImAgFnS
naděje	naděje	k1gFnPc4
na	na	k7c4
potomstvo	potomstvo	k1gNnSc4
a	a	k8xC
pokračování	pokračování	k1gNnSc4
rodu	rod	k1gInSc2
<g/>
,	,	kIx,
měl	mít	k5eAaImAgInS
horoskop	horoskop	k1gInSc4
podobný	podobný	k2eAgInSc4d1
se	se	k3xPyFc4
svým	svůj	k3xOyFgMnSc7
strýcem	strýc	k1gMnSc7
a	a	k8xC
náhodou	náhoda	k1gFnSc7
to	ten	k3xDgNnSc1
tak	tak	k6eAd1
bylo	být	k5eAaImAgNnS
i	i	k9
v	v	k7c6
případě	případ	k1gInSc6
dítěte	dítě	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
narodilo	narodit	k5eAaPmAgNnS
během	během	k7c2
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
pobytu	pobyt	k1gInSc2
v	v	k7c6
paláci	palác	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malá	malý	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
přiléhající	přiléhající	k2eAgFnSc1d1
k	k	k7c3
Sacchiho	Sacchiha	k1gFnSc5
fresce	freska	k1gFnSc6
byla	být	k5eAaImAgFnS
navržena	navrhnout	k5eAaPmNgFnS
pro	pro	k7c4
křest	křest	k1gInSc4
dítěte	dítě	k1gNnSc2
a	a	k8xC
její	její	k3xOp3gFnSc2
fresky	freska	k1gFnSc2
nesly	nést	k5eAaImAgInP
všechny	všechen	k3xTgInPc1
obvyklé	obvyklý	k2eAgInPc1d1
talismany	talisman	k1gInPc1
plodnosti	plodnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hvězdy	hvězda	k1gFnPc4
ukazovaly	ukazovat	k5eAaImAgInP
na	na	k7c4
příznivou	příznivý	k2eAgFnSc4d1
budoucnost	budoucnost	k1gFnSc4
rodiny	rodina	k1gFnSc2
zrozené	zrozený	k2eAgFnSc2d1
a	a	k8xC
vyvolené	vyvolená	k1gFnSc2
vládnout	vládnout	k5eAaImF
po	po	k7c4
celé	celý	k2eAgFnPc4d1
další	další	k2eAgFnPc4d1
generace	generace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Vize	vize	k1gFnSc1
sv.	sv.	kA
Romualda	Romualda	k1gFnSc1
</s>
<s>
Vidění	vidění	k1gNnSc1
svatého	svatý	k2eAgMnSc2d1
Romualda	Romuald	k1gMnSc2
<g/>
,	,	kIx,
olej	olej	k1gInSc4
na	na	k7c6
plátně	plátno	k1gNnSc6
<g/>
,	,	kIx,
310	#num#	k4
x	x	k?
175	#num#	k4
cm	cm	kA
</s>
<s>
Tři	tři	k4xCgFnPc1
Marie	Maria	k1gFnPc1
</s>
<s>
Obraz	obraz	k1gInSc1
byl	být	k5eAaImAgInS
dokončen	dokončit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1631	#num#	k4
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
v	v	k7c4
Pinacoteca	Pinacotec	k2eAgMnSc4d1
Vaticana	Vatican	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připomíná	připomínat	k5eAaImIp3nS
epizodu	epizoda	k1gFnSc4
v	v	k7c6
životě	život	k1gInSc6
mladého	mladý	k2eAgMnSc2d1
mnicha	mnich	k1gMnSc2
<g/>
,	,	kIx,
pozdějšího	pozdní	k2eAgMnSc2d2
svatého	svatý	k2eAgMnSc2d1
Romualda	Romuald	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objednavatelem	objednavatel	k1gMnSc7
byli	být	k5eAaImAgMnP
mniši	mnich	k1gMnPc1
a	a	k8xC
jeptišky	jeptiška	k1gFnPc1
z	z	k7c2
Camaldoli	Camaldole	k1gFnSc3
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
Ordo	orda	k1gFnSc5
Camaldulensium	Camaldulensium	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
dvě	dva	k4xCgNnPc1
různá	různý	k2eAgNnPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
příbuzná	příbuzný	k2eAgNnPc4d1
klášterní	klášterní	k2eAgNnPc4d1
společenství	společenství	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
se	se	k3xPyFc4
uchylují	uchylovat	k5eAaImIp3nP
k	k	k7c3
mnišskému	mnišský	k2eAgInSc3d1
životu	život	k1gInSc3
podle	podle	k7c2
svatého	svatý	k2eAgNnSc2d1
Romualda	Romualdo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnSc1
jméno	jméno	k1gNnSc1
je	být	k5eAaImIp3nS
odvozeno	odvodit	k5eAaPmNgNnS
z	z	k7c2
názvu	název	k1gInSc2
Svaté	svatý	k2eAgFnSc2d1
poustevny	poustevna	k1gFnSc2
(	(	kIx(
<g/>
italské	italský	k2eAgNnSc4d1
<g/>
:	:	kIx,
Sacro	Sacro	k1gNnSc4
Eremo	Erema	k1gFnSc5
<g/>
)	)	kIx)
Camaldoli	Camaldole	k1gFnSc6
<g/>
,	,	kIx,
vysoko	vysoko	k6eAd1
v	v	k7c6
horách	hora	k1gFnPc6
centrální	centrální	k2eAgFnSc2d1
Itálie	Itálie	k1gFnSc2
<g/>
,	,	kIx,
poblíž	poblíž	k7c2
města	město	k1gNnSc2
Arezzo	Arezza	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společenství	společenství	k1gNnSc3
Camaldolese	Camaldolese	k1gFnSc2
prý	prý	k9
snilo	snít	k5eAaImAgNnS
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
členové	člen	k1gMnPc1
jeho	jeho	k3xOp3gInSc3
řádu	řád	k1gInSc3
v	v	k7c6
bílém	bílý	k2eAgNnSc6d1
rouchu	roucho	k1gNnSc6
vystoupí	vystoupit	k5eAaPmIp3nS
na	na	k7c4
nebesa	nebesa	k1gNnPc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
je	být	k5eAaImIp3nS
vidět	vidět	k5eAaImF
na	na	k7c6
pozadí	pozadí	k1gNnSc6
obrazu	obraz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
Sacchiho	Sacchi	k1gMnSc4
dílo	dílo	k1gNnSc1
je	být	k5eAaImIp3nS
typické	typický	k2eAgNnSc1d1
zobrazení	zobrazení	k1gNnSc1
klidu	klid	k1gInSc2
a	a	k8xC
vážnosti	vážnost	k1gFnSc2
mnichů	mnich	k1gInPc2
<g/>
,	,	kIx,
diskutujících	diskutující	k2eAgInPc2d1
o	o	k7c6
filozofických	filozofický	k2eAgNnPc6d1
tématech	téma	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Řehoř	Řehoř	k1gMnSc1
a	a	k8xC
zázrak	zázrak	k1gInSc1
s	s	k7c7
korporálem	korporál	k1gInSc7
</s>
<s>
Také	také	k9
známý	známý	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
Zázrak	zázrak	k1gInSc1
sv.	sv.	kA
Řehoře	Řehoř	k1gMnSc2
Velkého	velký	k2eAgMnSc2d1
<g/>
,	,	kIx,
tento	tento	k3xDgInSc1
obraz	obraz	k1gInSc1
byl	být	k5eAaImAgInS
ukončen	ukončit	k5eAaPmNgInS
v	v	k7c6
letech	let	k1gInPc6
1625	#num#	k4
<g/>
–	–	k?
<g/>
57	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
je	být	k5eAaImIp3nS
v	v	k7c4
Pinacoteca	Pinacotec	k2eAgMnSc4d1
Vaticana	Vatican	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Plátno	plátno	k1gNnSc1
zobrazuje	zobrazovat	k5eAaImIp3nS
tuto	tento	k3xDgFnSc4
legenduː	legenduː	k?
císařovna	císařovna	k1gFnSc1
Constantia	Constantia	k1gFnSc1
prosila	prosít	k5eAaPmAgFnS,k5eAaImAgFnS
papeže	papež	k1gMnSc4
Řehoře	Řehoř	k1gMnSc4
I.	I.	kA
(	(	kIx(
<g/>
540	#num#	k4
<g/>
–	–	k?
<g/>
604	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
jí	on	k3xPp3gFnSc3
věnoval	věnovat	k5eAaPmAgInS,k5eAaImAgInS
část	část	k1gFnSc4
ostatků	ostatek	k1gInPc2
z	z	k7c2
těla	tělo	k1gNnSc2
svatých	svatý	k1gMnPc2
Petra	Petr	k1gMnSc2
a	a	k8xC
Pavla	Pavel	k1gMnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
papež	papež	k1gMnSc1
<g/>
,	,	kIx,
nechtěje	chtít	k5eNaImSgMnS
narušit	narušit	k5eAaPmF
svaté	svatý	k2eAgInPc4d1
ostatky	ostatek	k1gInPc4
<g/>
,	,	kIx,
poslal	poslat	k5eAaPmAgMnS
jí	jíst	k5eAaImIp3nS
část	část	k1gFnSc4
plátna	plátno	k1gNnSc2
<g/>
,	,	kIx,
do	do	k7c2
kterého	který	k3yIgInSc2,k3yRgInSc2,k3yQgInSc2
bylo	být	k5eAaImAgNnS
zabaleno	zabalit	k5eAaPmNgNnS
tělo	tělo	k1gNnSc1
sv.	sv.	kA
Jana	Jan	k1gMnSc2
Evangelisty	evangelista	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Constantia	Constantia	k1gFnSc1
odmítla	odmítnout	k5eAaPmAgFnS
tento	tento	k3xDgInSc4
dar	dar	k1gInSc4
od	od	k7c2
papeže	papež	k1gMnSc2
jako	jako	k9
nedostatečný	dostatečný	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
Řehoř	Řehoř	k1gMnSc1
I.	I.	kA
aby	aby	kYmCp3nS
dokázal	dokázat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
moc	moc	k1gFnSc1
ostatků	ostatek	k1gInPc2
dělá	dělat	k5eAaImIp3nS
zázraky	zázrak	k1gInPc4
(	(	kIx(
<g/>
a	a	k8xC
ospravedlňuje	ospravedlňovat	k5eAaImIp3nS
jejich	jejich	k3xOp3gFnSc4
cenu	cena	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
položil	položit	k5eAaPmAgMnS
látku	látka	k1gFnSc4
na	na	k7c4
oltář	oltář	k1gInSc4
a	a	k8xC
po	po	k7c6
modlitbě	modlitba	k1gFnSc6
plátno	plátno	k1gNnSc4
propíchl	propíchnout	k5eAaPmAgInS
nožem	nůž	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
plátna	plátno	k1gNnSc2
tekla	téct	k5eAaImAgNnP
krev	krev	k1gFnSc4
jako	jako	k9
z	z	k7c2
živého	živý	k2eAgNnSc2d1
těla	tělo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1771	#num#	k4
byla	být	k5eAaImAgFnS
vytvořena	vytvořit	k5eAaPmNgFnS
mozaiková	mozaikový	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
tohoto	tento	k3xDgInSc2
obrazu	obraz	k1gInSc2
pro	pro	k7c4
baziliku	bazilika	k1gFnSc4
svatého	svatý	k2eAgMnSc2d1
Petra	Petr	k1gMnSc2
v	v	k7c6
Římě	Řím	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
obraz	obraz	k1gInSc1
odráží	odrážet	k5eAaImIp3nS
postoje	postoj	k1gInPc4
uvedené	uvedený	k2eAgInPc1d1
v	v	k7c6
kánonech	kánon	k1gInPc6
po	po	k7c6
koncilu	koncil	k1gInSc6
v	v	k7c6
Tridentu	Trident	k1gMnSc6
(	(	kIx(
<g/>
1545	#num#	k4
<g/>
–	–	k?
<g/>
1563	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
relikvie	relikvie	k1gFnPc1
hrály	hrát	k5eAaImAgFnP
důležitou	důležitý	k2eAgFnSc4d1
roli	role	k1gFnSc4
v	v	k7c6
zázracích	zázrak	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Papež	Papež	k1gMnSc1
sloužil	sloužit	k5eAaImAgMnS
jako	jako	k9
konečný	konečný	k2eAgMnSc1d1
tlumočník	tlumočník	k1gMnSc1
svatosti	svatost	k1gFnSc2
a	a	k8xC
následně	následně	k6eAd1
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
stalo	stát	k5eAaPmAgNnS
metaforou	metafora	k1gFnSc7
eucharistie	eucharistie	k1gFnSc2
jako	jako	k8xS,k8xC
zázraku	zázrak	k1gInSc2
proměny	proměna	k1gFnSc2
vína	víno	k1gNnSc2
a	a	k8xC
chleba	chléb	k1gInSc2
v	v	k7c4
krev	krev	k1gFnSc4
a	a	k8xC
tělo	tělo	k1gNnSc4
Ježíše	Ježíš	k1gMnSc2
Krista	Kristus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1
práce	práce	k1gFnPc1
</s>
<s>
Další	další	k2eAgFnSc1d1
slavná	slavný	k2eAgFnSc1d1
dílaː	dílaː	k?
</s>
<s>
Smrt	smrt	k1gFnSc1
svaté	svatý	k2eAgFnSc2d1
Anny	Anna	k1gFnSc2
<g/>
,	,	kIx,
San	San	k1gFnPc1
Carlo	Carlo	k1gNnSc1
ai	ai	k?
Catinari	Catinari	k1gNnSc1
<g/>
,	,	kIx,
Řím	Řím	k1gInSc1
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Ondřej	Ondřej	k1gMnSc1
<g/>
,	,	kIx,
Kvirinálský	Kvirinálský	k2eAgInSc1d1
palác	palác	k1gInSc1
<g/>
,	,	kIx,
Řím	Řím	k1gInSc1
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Josef	Josef	k1gMnSc1
<g/>
,	,	kIx,
Caponile	Caponile	k1gMnSc1
Case	Cas	k1gFnSc2
</s>
<s>
Tři	tři	k4xCgMnPc4
Marie	Mario	k1gMnPc4
<g/>
,	,	kIx,
1634	#num#	k4
<g/>
,	,	kIx,
Palazzo	Palazza	k1gFnSc5
Barberini	Barberin	k1gMnPc1
<g/>
,	,	kIx,
Řím	Řím	k1gInSc1
</s>
<s>
Další	další	k1gNnSc1
Sacchiho	Sacchi	k1gMnSc2
oltářní	oltářní	k2eAgInPc1d1
obrazy	obraz	k1gInPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
Perugii	Perugie	k1gFnSc6
<g/>
,	,	kIx,
Folignu	Folign	k1gInSc6
a	a	k8xC
Camerinu	Camerin	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Andrea	Andrea	k1gFnSc1
Sacchi	Sacch	k1gFnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
For	forum	k1gNnPc2
a	a	k8xC
long	longa	k1gFnPc2
time	time	k1gNnSc1
<g/>
,	,	kIx,
Sacchi	Sacchi	k1gNnSc1
scholarship	scholarship	k1gInSc1
believed	believed	k1gMnSc1
the	the	k?
painter	painter	k1gMnSc1
was	was	k?
born	born	k1gMnSc1
in	in	k?
Nettuno	Nettuna	k1gFnSc5
<g/>
,	,	kIx,
but	but	k?
this	this	k1gInSc1
information	information	k1gInSc1
is	is	k?
now	now	k?
regarded	regarded	k1gInSc1
as	as	k9
incorrect	incorrect	k2eAgInSc1d1
following	following	k1gInSc1
many	mana	k1gFnSc2
academic	academic	k1gMnSc1
studies	studies	k1gMnSc1
such	sucho	k1gNnPc2
as	as	k9
those	those	k6eAd1
by	by	kYmCp3nS
Ann	Ann	k1gFnSc1
Sutherland	Sutherland	k1gInSc4
Harris	Harris	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
As	as	k1gNnSc1
far	fara	k1gFnPc2
as	as	k9
Andrea	Andrea	k1gFnSc1
Sacchi	Sacchi	k1gNnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
birth	birth	k1gInSc1
in	in	k?
Rome	Rom	k1gMnSc5
and	and	k?
not	nota	k1gFnPc2
in	in	k?
Nettuno	Nettuna	k1gFnSc5
is	is	k?
concerned	concerned	k1gMnSc1
<g/>
,	,	kIx,
see	see	k?
Ann	Ann	k1gFnSc1
Sutherland	Sutherlando	k1gNnPc2
Harris	Harris	k1gFnSc2
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Andrea	Andrea	k1gFnSc1
Sacchi	Sacch	k1gFnSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
L	L	kA
<g/>
'	'	kIx"
<g/>
idea	idea	k1gFnSc1
del	del	k?
bello	bello	k1gNnSc1
<g/>
:	:	kIx,
viaggio	viaggio	k1gNnSc1
per	pero	k1gNnPc2
Roma	Rom	k1gMnSc2
nel	nel	k?
Seicento	Seicento	k1gNnSc1
con	con	k?
Giovan	Giovan	k1gMnSc1
Pietro	Pietro	k1gNnSc1
Bellori	Bellor	k1gFnPc1
<g/>
,	,	kIx,
exh	exh	k?
<g/>
.	.	kIx.
cat	cat	k?
<g/>
.	.	kIx.
edited	edited	k1gMnSc1
by	by	kYmCp3nS
E.	E.	kA
Borea	Bore	k1gInSc2
and	and	k?
C.	C.	kA
Gasparri	Gasparr	k1gFnSc2
<g/>
,	,	kIx,
Rome	Rom	k1gMnSc5
(	(	kIx(
<g/>
Palazzo	Palazza	k1gFnSc5
delle	dell	k1gInSc6
Esposizioni	Esposizioň	k1gFnSc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Roma	Rom	k1gMnSc4
<g/>
:	:	kIx,
De	De	k?
Luca	Lucus	k1gMnSc2
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
,	,	kIx,
vol	vol	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
II	II	kA
<g/>
,	,	kIx,
2	#num#	k4
vols	volsa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
p.	p.	k?
<g/>
442	#num#	k4
<g/>
-	-	kIx~
<g/>
444	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sacchi	Sacchi	k1gNnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
death	death	k1gInSc1
in	in	k?
Rome	Rom	k1gMnSc5
is	is	k?
proven	proven	k2eAgMnSc1d1
by	by	k9
his	his	k1gNnSc1
will	willa	k1gFnPc2
<g/>
,	,	kIx,
now	now	k?
at	at	k?
the	the	k?
State	status	k1gInSc5
Archives	Archives	k1gMnSc1
in	in	k?
Rome	Rom	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
will	will	k1gMnSc1
has	hasit	k5eAaImRp2nS
been	been	k1gMnSc1
transcribed	transcribed	k1gMnSc1
and	and	k?
may	may	k?
be	be	k?
found	found	k1gMnSc1
in	in	k?
Antonio	Antonio	k1gMnSc1
D	D	kA
<g/>
'	'	kIx"
<g/>
avossa	avossa	k1gFnSc1
<g/>
,	,	kIx,
Andrea	Andrea	k1gFnSc1
Sacchi	Sacch	k1gFnSc2
<g/>
,	,	kIx,
Roma	Rom	k1gMnSc2
<g/>
:	:	kIx,
Kappa	kappa	k1gNnSc1
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CAMIZ	CAMIZ	kA
<g/>
,	,	kIx,
Franca	Franca	k?
Trinchieri	Trinchier	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Music	Music	k1gMnSc1
and	and	k?
Painting	Painting	k1gInSc1
in	in	k?
Cardinal	Cardinal	k1gFnSc2
del	del	k?
Monte	Mont	k1gInSc5
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Household	Householda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Metropolitan	metropolitan	k1gInSc1
Museum	museum	k1gNnSc4
Journal	Journal	k1gMnPc2
<g/>
.	.	kIx.
1991	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
213	#num#	k4
<g/>
–	–	k?
<g/>
226	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.230	10.230	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
1512913	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
See	See	k1gMnSc5
Giovanni	Giovanň	k1gMnSc5
Pietro	Pietra	k1gMnSc5
Bellori	Bellor	k1gMnSc5
<g/>
,	,	kIx,
The	The	k1gMnSc1
Lives	Lives	k1gMnSc1
of	of	k?
the	the	k?
Modern	Modern	k1gInSc1
Painters	Painters	k1gInSc1
<g/>
,	,	kIx,
Sculptors	Sculptors	k1gInSc1
and	and	k?
Architects	Architects	k1gInSc4
<g/>
,	,	kIx,
translated	translated	k1gInSc4
by	by	kYmCp3nS
Alice	Alice	k1gFnSc1
Sedgwick	Sedgwicka	k1gFnPc2
Wohl	Wohl	k1gMnSc1
<g/>
,	,	kIx,
edited	edited	k1gMnSc1
by	by	kYmCp3nS
H.	H.	kA
Wohl	Wohl	k1gInSc4
<g/>
,	,	kIx,
introduction	introduction	k1gInSc4
by	by	kYmCp3nS
T.	T.	kA
Montanari	Montanar	k1gFnPc1
<g/>
,	,	kIx,
New	New	k1gFnPc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
375	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
SUTHERLAND	SUTHERLAND	kA
HARRIS	HARRIS	kA
<g/>
,	,	kIx,
Ann	Ann	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Andrea	Andrea	k1gFnSc1
Sacchi	Sacchi	k1gNnSc2
:	:	kIx,
complete	complést	k5eAaPmIp3nS
edition	edition	k1gInSc4
of	of	k?
the	the	k?
paintings	paintings	k1gInSc1
with	with	k1gMnSc1
a	a	k8xC
critical	criticat	k5eAaPmAgMnS
catalogue	catalogue	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Princeton	Princeton	k1gInSc1
<g/>
:	:	kIx,
Princeton	Princeton	k1gInSc1
Univ	Univa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
RUDOLF	Rudolf	k1gMnSc1
WITTKOWER	WITTKOWER	kA
<g/>
,	,	kIx,
Rudolf	Rudolf	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Art	Art	k1gFnSc1
and	and	k?
Architecture	Architectur	k1gMnSc5
Italy	Ital	k1gMnPc7
<g/>
,	,	kIx,
1600	#num#	k4
<g/>
-	-	kIx~
<g/>
1750	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redakce	redakce	k1gFnSc1
Pelican	Pelican	k1gInSc1
History	Histor	k1gInPc1
of	of	k?
Art	Art	k1gFnPc2
<g/>
;	;	kIx,
1980	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Penguin	Penguin	k2eAgInSc1d1
Books	Books	k1gInSc1
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
261	#num#	k4
<g/>
–	–	k?
<g/>
266	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
MARCHETEAU	MARCHETEAU	kA
DE	DE	k?
QUINÇAY	QUINÇAY	kA
<g/>
,	,	kIx,
Christophe	Christophe	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Didon	Didon	k1gNnSc1
abandonnée	abandonné	k1gInSc2
de	de	k?
Andrea	Andrea	k1gFnSc1
Sacchi	Sacch	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caen	Caen	k1gInSc1
<g/>
:	:	kIx,
Musée	Musée	k1gInSc1
des	des	k1gNnSc1
Beaux-Arts	Beaux-Arts	k1gInSc1
de	de	k?
Caen	Caen	k1gInSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
138657	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118793977	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2118	#num#	k4
6914	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80028449	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500022759	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
5009959	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80028449	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Itálie	Itálie	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Umění	umění	k1gNnSc1
</s>
