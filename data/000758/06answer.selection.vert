<s>
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Jenštejna	Jenštejn	k1gInSc2	Jenštejn
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1347	[number]	k4	1347
<g/>
-	-	kIx~	-
<g/>
1350	[number]	k4	1350
nejpravděpodobněji	pravděpodobně	k6eAd3	pravděpodobně
1350	[number]	k4	1350
-	-	kIx~	-
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1400	[number]	k4	1400
<g/>
,	,	kIx,	,
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
,	,	kIx,	,
duchovní	duchovní	k1gMnSc1	duchovní
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1373	[number]	k4	1373
<g/>
-	-	kIx~	-
<g/>
1384	[number]	k4	1384
kancléřem	kancléř	k1gMnSc7	kancléř
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1379	[number]	k4	1379
<g/>
-	-	kIx~	-
<g/>
1396	[number]	k4	1396
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
třetího	třetí	k4xOgMnSc2	třetí
pražského	pražský	k2eAgMnSc2d1	pražský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
<g/>
.	.	kIx.	.
</s>
