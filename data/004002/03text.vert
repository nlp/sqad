<s>
Karetní	karetní	k2eAgFnSc1d1	karetní
hra	hra	k1gFnSc1	hra
je	být	k5eAaImIp3nS	být
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
s	s	k7c7	s
hracími	hrací	k2eAgFnPc7d1	hrací
kartami	karta	k1gFnPc7	karta
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
je	on	k3xPp3gFnPc4	on
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c6	na
klasické	klasický	k2eAgFnSc6d1	klasická
a	a	k8xC	a
moderní	moderní	k2eAgFnSc6d1	moderní
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
klasické	klasický	k2eAgFnPc4d1	klasická
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgFnPc1d1	považována
hry	hra	k1gFnPc1	hra
hrané	hraný	k2eAgFnPc1d1	hraná
s	s	k7c7	s
tradičními	tradiční	k2eAgInPc7d1	tradiční
karetními	karetní	k2eAgInPc7d1	karetní
listy	list	k1gInPc7	list
(	(	kIx(	(
<g/>
např.	např.	kA	např.
francouzské	francouzský	k2eAgFnPc1d1	francouzská
<g/>
,	,	kIx,	,
známé	známý	k2eAgFnPc1d1	známá
též	též	k9	též
jako	jako	k9	jako
žolíkové	žolíkové	k?	žolíkové
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgInSc1	jeden
stejný	stejný	k2eAgInSc1d1	stejný
balíček	balíček	k1gInSc1	balíček
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
hrát	hrát	k5eAaImF	hrát
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
různých	různý	k2eAgFnPc2d1	různá
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
moderní	moderní	k2eAgFnPc1d1	moderní
karetní	karetní	k2eAgFnPc1d1	karetní
hry	hra	k1gFnPc1	hra
představují	představovat	k5eAaImIp3nP	představovat
jednu	jeden	k4xCgFnSc4	jeden
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
hru	hra	k1gFnSc4	hra
a	a	k8xC	a
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
hrát	hrát	k5eAaImF	hrát
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
pomocí	pomoc	k1gFnSc7	pomoc
hry	hra	k1gFnSc2	hra
jiné	jiný	k2eAgFnSc2d1	jiná
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Bang	Bang	k1gInSc1	Bang
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
karetní	karetní	k2eAgFnSc1d1	karetní
hra	hra	k1gFnSc1	hra
se	se	k3xPyFc4	se
také	také	k9	také
někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
balíček	balíček	k1gInSc1	balíček
hracích	hrací	k2eAgFnPc2d1	hrací
karet	kareta	k1gFnPc2	kareta
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejoblíbenější	oblíbený	k2eAgFnPc4d3	nejoblíbenější
české	český	k2eAgFnPc4d1	Česká
karetní	karetní	k2eAgFnPc4d1	karetní
hry	hra	k1gFnPc4	hra
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Žolíky	žolík	k1gInPc1	žolík
<g/>
,	,	kIx,	,
Červená	Červená	k1gFnSc1	Červená
bere	brát	k5eAaImIp3nS	brát
<g/>
,	,	kIx,	,
Kent	Kent	k1gInSc1	Kent
<g/>
,	,	kIx,	,
Zelená	zelený	k2eAgFnSc1d1	zelená
Louka	louka	k1gFnSc1	louka
<g/>
,	,	kIx,	,
Prší	pršet	k5eAaImIp3nS	pršet
a	a	k8xC	a
Pasiáns	pasiáns	k1gFnSc4	pasiáns
Dopátrat	dopátrat	k5eAaPmF	dopátrat
se	se	k3xPyFc4	se
přesného	přesný	k2eAgInSc2d1	přesný
původu	původ	k1gInSc2	původ
karetních	karetní	k2eAgFnPc2d1	karetní
her	hra	k1gFnPc2	hra
není	být	k5eNaImIp3nS	být
vůbec	vůbec	k9	vůbec
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
,	,	kIx,	,
spíš	spíš	k9	spíš
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
úkol	úkol	k1gInSc1	úkol
nesplnitelný	splnitelný	k2eNgInSc1d1	nesplnitelný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historických	historický	k2eAgInPc6d1	historický
pramenech	pramen	k1gInPc6	pramen
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
sice	sice	k8xC	sice
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ty	ten	k3xDgMnPc4	ten
jsou	být	k5eAaImIp3nP	být
až	až	k9	až
příliš	příliš	k6eAd1	příliš
kusé	kusý	k2eAgFnPc1d1	kusá
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
si	se	k3xPyFc3	se
mohli	moct	k5eAaImAgMnP	moct
udělat	udělat	k5eAaPmF	udělat
jasnou	jasný	k2eAgFnSc4d1	jasná
představu	představa	k1gFnSc4	představa
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
a	a	k8xC	a
pronikání	pronikání	k1gNnSc1	pronikání
karet	kareta	k1gFnPc2	kareta
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historických	historický	k2eAgInPc6d1	historický
pramenech	pramen	k1gInPc6	pramen
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
řada	řada	k1gFnSc1	řada
teorií	teorie	k1gFnPc2	teorie
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
karty	karta	k1gFnPc1	karta
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
a	a	k8xC	a
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
šířily	šířit	k5eAaImAgFnP	šířit
světem	svět	k1gInSc7	svět
<g/>
.	.	kIx.	.
</s>
<s>
Pečlivým	pečlivý	k2eAgNnSc7d1	pečlivé
studiem	studio	k1gNnSc7	studio
a	a	k8xC	a
porovnáváním	porovnávání	k1gNnSc7	porovnávání
dostupných	dostupný	k2eAgFnPc2d1	dostupná
informací	informace	k1gFnPc2	informace
se	se	k3xPyFc4	se
historikům	historik	k1gMnPc3	historik
podařilo	podařit	k5eAaPmAgNnS	podařit
část	část	k1gFnSc4	část
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
vyvrátit	vyvrátit	k5eAaPmF	vyvrátit
a	a	k8xC	a
předložit	předložit	k5eAaPmF	předložit
nejpravděpodobnější	pravděpodobný	k2eAgFnSc4d3	nejpravděpodobnější
teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
karty	karta	k1gFnPc1	karta
podmanily	podmanit	k5eAaPmAgFnP	podmanit
celý	celý	k2eAgInSc4d1	celý
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Karty	karta	k1gFnPc1	karta
jsou	být	k5eAaImIp3nP	být
opředeny	opředen	k2eAgFnPc1d1	opředena
mnoha	mnoho	k4c7	mnoho
legendami	legenda	k1gFnPc7	legenda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
pramenech	pramen	k1gInPc6	pramen
je	být	k5eAaImIp3nS	být
původ	původ	k1gInSc1	původ
prvních	první	k4xOgFnPc2	první
karet	kareta	k1gFnPc2	kareta
připisován	připisovat	k5eAaImNgInS	připisovat
antickému	antický	k2eAgMnSc3d1	antický
filozofovi	filozof	k1gMnSc3	filozof
Chilonovi	Chilon	k1gMnSc3	Chilon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
prý	prý	k9	prý
karetní	karetní	k2eAgFnSc4d1	karetní
hru	hra	k1gFnSc4	hra
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
prostředek	prostředek	k1gInSc4	prostředek
zapomenutí	zapomenutí	k1gNnSc4	zapomenutí
chudiny	chudina	k1gFnSc2	chudina
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
bídu	bída	k1gFnSc4	bída
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
starých	starý	k2eAgFnPc6d1	stará
evropských	evropský	k2eAgFnPc6d1	Evropská
kartách	karta	k1gFnPc6	karta
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
zobrazován	zobrazován	k2eAgMnSc1d1	zobrazován
bůh	bůh	k1gMnSc1	bůh
Merkur	Merkur	k1gMnSc1	Merkur
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
darem	dar	k1gInSc7	dar
lidstvu	lidstvo	k1gNnSc3	lidstvo
prý	prý	k9	prý
karty	karta	k1gFnPc1	karta
jsou	být	k5eAaImIp3nP	být
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
legenda	legenda	k1gFnSc1	legenda
praví	pravit	k5eAaImIp3nS	pravit
<g/>
,	,	kIx,	,
že	že	k8xS	že
karetní	karetní	k2eAgFnSc1d1	karetní
hra	hra	k1gFnSc1	hra
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
chvíle	chvíle	k1gFnSc2	chvíle
při	při	k7c6	při
obléhání	obléhání	k1gNnSc6	obléhání
Tróje	Trója	k1gFnSc2	Trója
<g/>
,	,	kIx,	,
motiv	motiv	k1gInSc4	motiv
krásné	krásný	k2eAgFnSc2d1	krásná
Heleny	Helena	k1gFnSc2	Helena
se	se	k3xPyFc4	se
však	však	k9	však
na	na	k7c6	na
žádném	žádný	k3yNgInSc6	žádný
karetním	karetní	k2eAgInSc6d1	karetní
listu	list	k1gInSc6	list
nedochoval	dochovat	k5eNaPmAgMnS	dochovat
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Není	být	k5eNaImIp3nS	být
bez	bez	k7c2	bez
zajímavosti	zajímavost	k1gFnSc2	zajímavost
<g/>
,	,	kIx,	,
že	že	k8xS	že
podobná	podobný	k2eAgFnSc1d1	podobná
legenda	legenda	k1gFnSc1	legenda
se	se	k3xPyFc4	se
traduje	tradovat	k5eAaImIp3nS	tradovat
i	i	k9	i
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
hry	hra	k1gFnSc2	hra
Dáma	dáma	k1gFnSc1	dáma
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
hry	hra	k1gFnSc2	hra
v	v	k7c4	v
kostky	kostka	k1gFnPc4	kostka
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Většina	většina	k1gFnSc1	většina
teorií	teorie	k1gFnPc2	teorie
umísťuje	umísťovat	k5eAaImIp3nS	umísťovat
původ	původ	k1gInSc4	původ
karet	kareta	k1gFnPc2	kareta
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
názory	názor	k1gInPc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
karty	karta	k1gFnPc1	karta
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
Koreji	Korea	k1gFnSc6	Korea
nebo	nebo	k8xC	nebo
snad	snad	k9	snad
v	v	k7c6	v
Persii	Persie	k1gFnSc6	Persie
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
první	první	k4xOgFnSc1	první
podložená	podložený	k2eAgFnSc1d1	podložená
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c4	o
hraní	hraní	k1gNnSc4	hraní
karet	kareta	k1gFnPc2	kareta
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
praví	pravit	k5eAaBmIp3nS	pravit
kroniky	kronika	k1gFnPc4	kronika
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
Nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
l.	l.	k?	l.
p.	p.	k?	p.
969	[number]	k4	969
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
čínského	čínský	k2eAgInSc2d1	čínský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
)	)	kIx)	)
císař	císař	k1gMnSc1	císař
Mu-tsung	Musung	k1gMnSc1	Mu-tsung
dominové	dominový	k2eAgFnSc2d1	Dominová
karty	karta	k1gFnSc2	karta
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgFnPc7	svůj
ženami	žena	k1gFnPc7	žena
<g/>
.	.	kIx.	.
</s>
<s>
Nejednalo	jednat	k5eNaImAgNnS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
karty	karta	k1gFnPc4	karta
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	on	k3xPp3gMnPc4	on
známe	znát	k5eAaImIp1nP	znát
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nešlo	jít	k5eNaImAgNnS	jít
ani	ani	k8xC	ani
o	o	k7c4	o
domino	domino	k1gNnSc4	domino
<g/>
.	.	kIx.	.
</s>
<s>
Karty	karta	k1gFnPc1	karta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
podobaly	podobat	k5eAaImAgInP	podobat
těm	ten	k3xDgInPc3	ten
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nám	my	k3xPp1nPc3	my
jsou	být	k5eAaImIp3nP	být
dobře	dobře	k6eAd1	dobře
známé	známý	k2eAgFnPc1d1	známá
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
až	až	k9	až
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Písemné	písemný	k2eAgInPc1d1	písemný
prameny	pramen	k1gInPc1	pramen
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1120	[number]	k4	1120
a	a	k8xC	a
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
prý	prý	k9	prý
pro	pro	k7c4	pro
potěchu	potěcha	k1gFnSc4	potěcha
konkubín	konkubína	k1gFnPc2	konkubína
čínského	čínský	k2eAgMnSc2d1	čínský
císaře	císař	k1gMnSc2	císař
Séun-ho	Séun-	k1gMnSc2	Séun-
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
karty	karta	k1gFnPc4	karta
Kwan	Kwana	k1gFnPc2	Kwana
pai	pai	k?	pai
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
tzv.	tzv.	kA	tzv.
peněžních	peněžní	k2eAgFnPc2d1	peněžní
karet	kareta	k1gFnPc2	kareta
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
hrálo	hrát	k5eAaImAgNnS	hrát
se	s	k7c7	s
skutečnými	skutečný	k2eAgInPc7d1	skutečný
penězi	peníze	k1gInPc7	peníze
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dokládá	dokládat	k5eAaImIp3nS	dokládat
i	i	k9	i
čínská	čínský	k2eAgFnSc1d1	čínská
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1678	[number]	k4	1678
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dává	dávat	k5eAaImIp3nS	dávat
rovnítko	rovnítko	k1gNnSc4	rovnítko
mezi	mezi	k7c4	mezi
karty	karta	k1gFnPc4	karta
a	a	k8xC	a
peníze	peníz	k1gInPc4	peníz
Sung	Sunga	k1gFnPc2	Sunga
<g/>
.	.	kIx.	.
</s>
<s>
Teorií	teorie	k1gFnPc2	teorie
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
karty	karta	k1gFnPc1	karta
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
<g/>
,	,	kIx,	,
většinu	většina	k1gFnSc4	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
však	však	k9	však
dostupné	dostupný	k2eAgFnPc1d1	dostupná
informace	informace	k1gFnPc1	informace
vyvracejí	vyvracet	k5eAaImIp3nP	vyvracet
<g/>
.	.	kIx.	.
karty	karta	k1gFnPc1	karta
přinesli	přinést	k5eAaPmAgMnP	přinést
poutníci	poutník	k1gMnPc1	poutník
putující	putující	k2eAgMnPc1d1	putující
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
karty	karta	k1gFnPc1	karta
přinesli	přinést	k5eAaPmAgMnP	přinést
mongolští	mongolský	k2eAgMnPc1d1	mongolský
nájezdníci	nájezdník	k1gMnPc1	nájezdník
karty	karta	k1gFnSc2	karta
přinesli	přinést	k5eAaPmAgMnP	přinést
křižáci	křižák	k1gMnPc1	křižák
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
tažení	tažení	k1gNnPc2	tažení
karty	karta	k1gFnSc2	karta
přinesli	přinést	k5eAaPmAgMnP	přinést
Cikáni	cikán	k1gMnPc1	cikán
karty	karta	k1gFnSc2	karta
se	se	k3xPyFc4	se
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
dostaly	dostat	k5eAaPmAgInP	dostat
působením	působení	k1gNnSc7	působení
arabského	arabský	k2eAgInSc2d1	arabský
vlivu	vliv	k1gInSc2	vliv
O	o	k7c6	o
původu	původ	k1gInSc6	původ
karet	kareta	k1gFnPc2	kareta
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
můžeme	moct	k5eAaImIp1nP	moct
nejvíce	hodně	k6eAd3	hodně
dozvědět	dozvědět	k5eAaPmF	dozvědět
především	především	k9	především
z	z	k7c2	z
jejich	jejich	k3xOp3gInSc2	jejich
názvu	název	k1gInSc2	název
a	a	k8xC	a
podoby	podoba	k1gFnSc2	podoba
nejstarších	starý	k2eAgFnPc2d3	nejstarší
dochovaných	dochovaný	k2eAgFnPc2d1	dochovaná
karet	kareta	k1gFnPc2	kareta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
nesla	nést	k5eAaImAgFnS	nést
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
karetní	karetní	k2eAgFnSc1d1	karetní
hra	hra	k1gFnSc1	hra
pojmenování	pojmenování	k1gNnSc2	pojmenování
naibi	naib	k1gFnSc2	naib
a	a	k8xC	a
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
naypes	naypesa	k1gFnPc2	naypesa
<g/>
.	.	kIx.	.
</s>
<s>
Slova	slovo	k1gNnPc1	slovo
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
arabštiny	arabština	k1gFnSc2	arabština
a	a	k8xC	a
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
.	.	kIx.	.
</s>
<s>
Naibi	Naibi	k6eAd1	Naibi
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
arabského	arabský	k2eAgInSc2d1	arabský
laib	laiba	k1gFnPc2	laiba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
doslovném	doslovný	k2eAgNnSc6d1	doslovné
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
hračka	hračka	k1gFnSc1	hračka
nebo	nebo	k8xC	nebo
hra	hra	k1gFnSc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
naibi	naibi	k6eAd1	naibi
však	však	k9	však
můžeme	moct	k5eAaImIp1nP	moct
hledat	hledat	k5eAaImF	hledat
v	v	k7c6	v
ještě	ještě	k6eAd1	ještě
bližším	blízký	k2eAgNnSc6d2	bližší
arabském	arabský	k2eAgNnSc6d1	arabské
slově	slovo	k1gNnSc6	slovo
nabi	nab	k1gFnSc2	nab
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
prorok	prorok	k1gMnSc1	prorok
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
by	by	kYmCp3nS	by
tedy	tedy	k9	tedy
o	o	k7c4	o
pojem	pojem	k1gInSc4	pojem
věštění	věštění	k1gNnSc2	věštění
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
karet	kareta	k1gFnPc2	kareta
také	také	k9	také
používalo	používat	k5eAaImAgNnS	používat
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
používá	používat	k5eAaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
arabsko-hebrejské	arabskoebrejský	k2eAgFnSc6d1	arabsko-hebrejský
terminologii	terminologie	k1gFnSc6	terminologie
věšteckých	věštecký	k2eAgInPc2d1	věštecký
výrazů	výraz	k1gInPc2	výraz
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
příbuzná	příbuzný	k2eAgNnPc4d1	příbuzné
slova	slovo	k1gNnPc4	slovo
nabi	nab	k1gFnSc2	nab
<g/>
,	,	kIx,	,
naba	naba	k1gMnSc1	naba
<g/>
,	,	kIx,	,
nabas	nabas	k1gMnSc1	nabas
<g/>
,	,	kIx,	,
naibes	naibes	k1gMnSc1	naibes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
arabštině	arabština	k1gFnSc6	arabština
rovněž	rovněž	k9	rovněž
existuje	existovat	k5eAaImIp3nS	existovat
velice	velice	k6eAd1	velice
příbuzný	příbuzný	k2eAgInSc1d1	příbuzný
výraz	výraz	k1gInSc1	výraz
naib	naiba	k1gFnPc2	naiba
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
zástupce	zástupce	k1gMnSc1	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
vysvětlení	vysvětlení	k1gNnSc2	vysvětlení
hledejme	hledat	k5eAaImRp1nP	hledat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vedle	vedle	k7c2	vedle
krále	král	k1gMnSc2	král
v	v	k7c6	v
roli	role	k1gFnSc6	role
svrška	svršek	k1gInSc2	svršek
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
naib	naib	k1gMnSc1	naib
-	-	kIx~	-
zástupce	zástupce	k1gMnSc1	zástupce
a	a	k8xC	a
naib	naib	k1gMnSc1	naib
thani	thaen	k2eAgMnPc1d1	thaen
-	-	kIx~	-
druhý	druhý	k4xOgMnSc1	druhý
zástupce	zástupce	k1gMnSc1	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
velké	velká	k1gFnPc1	velká
byly	být	k5eAaImAgFnP	být
arabské	arabský	k2eAgInPc4d1	arabský
vlivy	vliv	k1gInPc4	vliv
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
evropské	evropský	k2eAgFnSc2d1	Evropská
karetní	karetní	k2eAgFnSc2d1	karetní
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
pojem	pojem	k1gInSc1	pojem
karta	karta	k1gFnSc1	karta
bychom	by	kYmCp1nP	by
mohli	moct	k5eAaImAgMnP	moct
odvodit	odvodit	k5eAaPmF	odvodit
od	od	k7c2	od
termínů	termín	k1gInPc2	termín
ze	z	k7c2	z
středověkých	středověký	k2eAgMnPc2d1	středověký
slovníků	slovník	k1gInPc2	slovník
jako	jako	k8xS	jako
chartae	chartae	k1gFnPc2	chartae
<g/>
,	,	kIx,	,
cartae	cartae	k1gFnPc2	cartae
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
charticelae	charticelae	k6eAd1	charticelae
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
pojmy	pojem	k1gInPc1	pojem
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
používaly	používat	k5eAaImAgInP	používat
jako	jako	k8xC	jako
název	název	k1gInSc1	název
pro	pro	k7c4	pro
psací	psací	k2eAgInSc4d1	psací
papír	papír	k1gInSc4	papír
vyrobený	vyrobený	k2eAgInSc4d1	vyrobený
z	z	k7c2	z
papyru	papyr	k1gInSc2	papyr
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
přeneslo	přenést	k5eAaPmAgNnS	přenést
na	na	k7c4	na
hrací	hrací	k2eAgFnPc4d1	hrací
karty	karta	k1gFnPc4	karta
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
sada	sada	k1gFnSc1	sada
arabských	arabský	k2eAgFnPc2d1	arabská
karet	kareta	k1gFnPc2	kareta
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnPc1	barva
arabských	arabský	k2eAgFnPc2d1	arabská
karet	kareta	k1gFnPc2	kareta
byly	být	k5eAaImAgFnP	být
Darahim	Darahim	k1gInPc3	Darahim
(	(	kIx(	(
<g/>
mince	mince	k1gFnSc2	mince
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tuman	tuman	k1gInSc4	tuman
(	(	kIx(	(
<g/>
poháry	pohár	k1gInPc4	pohár
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Suyuf	Suyuf	k1gMnSc1	Suyuf
(	(	kIx(	(
<g/>
meče	meč	k1gInSc2	meč
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jawkan	Jawkan	k1gInSc1	Jawkan
(	(	kIx(	(
<g/>
hole	hole	k1gFnSc1	hole
na	na	k7c4	na
pólo	pólo	k1gNnSc4	pólo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Porovnáním	porovnání	k1gNnSc7	porovnání
s	s	k7c7	s
barvami	barva	k1gFnPc7	barva
evropských	evropský	k2eAgFnPc2d1	Evropská
karet	kareta	k1gFnPc2	kareta
zjistíme	zjistit	k5eAaPmIp1nP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
velice	velice	k6eAd1	velice
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
barvám	barva	k1gFnPc3	barva
italských	italský	k2eAgFnPc2d1	italská
karet	kareta	k1gFnPc2	kareta
a	a	k8xC	a
jen	jen	k9	jen
o	o	k7c4	o
něco	něco	k3yInSc4	něco
méně	málo	k6eAd2	málo
ke	k	k7c3	k
kartám	karta	k1gFnPc3	karta
španělským	španělský	k2eAgFnPc3d1	španělská
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejstarší	starý	k2eAgFnSc4d3	nejstarší
zmínku	zmínka	k1gFnSc4	zmínka
o	o	k7c6	o
kartách	karta	k1gFnPc6	karta
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
varování	varování	k1gNnSc1	varování
dominikánského	dominikánský	k2eAgMnSc2d1	dominikánský
řeholníka	řeholník	k1gMnSc2	řeholník
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
před	před	k7c7	před
svody	svod	k1gInPc7	svod
hříšné	hříšný	k2eAgFnSc2d1	hříšná
karetní	karetní	k2eAgFnSc2d1	karetní
hry	hra	k1gFnSc2	hra
z	z	k7c2	z
r.	r.	kA	r.
1377	[number]	k4	1377
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
dochovanou	dochovaný	k2eAgFnSc7d1	dochovaná
evropskou	evropský	k2eAgFnSc7d1	Evropská
sadou	sada	k1gFnSc7	sada
-	-	kIx~	-
mimochodem	mimochodem	k9	mimochodem
umělecky	umělecky	k6eAd1	umělecky
velmi	velmi	k6eAd1	velmi
zdařilou	zdařilý	k2eAgFnSc7d1	zdařilá
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
Stuttgartská	stuttgartský	k2eAgFnSc1d1	Stuttgartská
karetní	karetní	k2eAgFnSc1d1	karetní
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
l.	l.	k?	l.
1427-31	[number]	k4	1427-31
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
vášnivého	vášnivý	k2eAgMnSc2d1	vášnivý
lovce	lovec	k1gMnSc2	lovec
<g/>
,	,	kIx,	,
vévody	vévoda	k1gMnSc2	vévoda
württemberského	württemberský	k2eAgMnSc2d1	württemberský
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
masovému	masový	k2eAgNnSc3d1	masové
rozšíření	rozšíření	k1gNnSc3	rozšíření
karetních	karetní	k2eAgFnPc2d1	karetní
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
přispěl	přispět	k5eAaPmAgMnS	přispět
v	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
vynález	vynález	k1gInSc1	vynález
knihtisku	knihtisk	k1gInSc2	knihtisk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
výrazně	výrazně	k6eAd1	výrazně
snížil	snížit	k5eAaPmAgInS	snížit
jejich	jejich	k3xOp3gFnSc4	jejich
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
karty	karta	k1gFnPc1	karta
pravého	pravý	k2eAgInSc2d1	pravý
rozmachu	rozmach	k1gInSc2	rozmach
v	v	k7c6	v
době	doba	k1gFnSc6	doba
po	po	k7c6	po
husitské	husitský	k2eAgFnSc6d1	husitská
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
začínali	začínat	k5eAaImAgMnP	začínat
navracet	navracet	k5eAaImF	navracet
zpět	zpět	k6eAd1	zpět
ke	k	k7c3	k
světským	světský	k2eAgFnPc3d1	světská
zábavám	zábava	k1gFnPc3	zábava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
tak	tak	k9	tak
hazardní	hazardní	k2eAgFnPc1d1	hazardní
hry	hra	k1gFnPc1	hra
jako	jako	k8xC	jako
karty	karta	k1gFnPc1	karta
a	a	k8xC	a
kostky	kostka	k1gFnPc1	kostka
vedle	vedle	k7c2	vedle
složitého	složitý	k2eAgInSc2d1	složitý
šachu	šach	k1gInSc2	šach
rozšířeny	rozšířen	k2eAgInPc1d1	rozšířen
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
vrstvách	vrstva	k1gFnPc6	vrstva
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
-	-	kIx~	-
od	od	k7c2	od
krále	král	k1gMnSc2	král
přes	přes	k7c4	přes
šlechtu	šlechta	k1gFnSc4	šlechta
až	až	k9	až
po	po	k7c4	po
městské	městský	k2eAgFnPc4d1	městská
či	či	k8xC	či
venkovské	venkovský	k2eAgFnPc4d1	venkovská
krčmy	krčma	k1gFnPc4	krčma
<g/>
.	.	kIx.	.
</s>
<s>
Kněží	kněz	k1gMnPc1	kněz
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
kartám	karta	k1gFnPc3	karta
ostře	ostro	k6eAd1	ostro
stavěli	stavět	k5eAaImAgMnP	stavět
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
katoličtí	katolický	k2eAgMnPc1d1	katolický
tak	tak	k9	tak
i	i	k9	i
utrakvističtí	utrakvistický	k2eAgMnPc1d1	utrakvistický
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
hrály	hrát	k5eAaImAgFnP	hrát
zpravidla	zpravidla	k6eAd1	zpravidla
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
flus	flus	k?	flus
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
obdoba	obdoba	k1gFnSc1	obdoba
dnešního	dnešní	k2eAgNnSc2d1	dnešní
oka	oko	k1gNnSc2	oko
či	či	k8xC	či
kvarteto	kvarteto	k1gNnSc1	kvarteto
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnSc2d1	zvaná
strašák	strašák	k1gInSc4	strašák
<g/>
.	.	kIx.	.
</s>
<s>
Přiznat	přiznat	k5eAaPmF	přiznat
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Vyložit	vyložit	k5eAaPmF	vyložit
karty	karta	k1gFnPc4	karta
na	na	k7c4	na
stůl	stůl	k1gInSc4	stůl
<g/>
.	.	kIx.	.
</s>
<s>
Jsi	být	k5eAaImIp2nS	být
zelený	zelený	k2eAgInSc1d1	zelený
jak	jak	k8xC	jak
sedma	sedma	k1gFnSc1	sedma
<g/>
.	.	kIx.	.
</s>
<s>
Mám	mít	k5eAaImIp1nS	mít
eso	eso	k1gNnSc4	eso
v	v	k7c6	v
rukávu	rukáv	k1gInSc6	rukáv
<g/>
.	.	kIx.	.
</s>
<s>
Mám	mít	k5eAaImIp1nS	mít
všechny	všechen	k3xTgInPc4	všechen
trumfy	trumf	k1gInPc4	trumf
v	v	k7c6	v
rukách	ruka	k1gFnPc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Nahlížíš	nahlížet	k5eAaImIp2nS	nahlížet
mi	já	k3xPp1nSc3	já
do	do	k7c2	do
karet	kareta	k1gFnPc2	kareta
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
mi	já	k3xPp1nSc3	já
hraje	hrát	k5eAaImIp3nS	hrát
do	do	k7c2	do
karet	kareta	k1gFnPc2	kareta
<g/>
.	.	kIx.	.
</s>
<s>
HÁJKOVÁ	Hájková	k1gFnSc1	Hájková
<g/>
,	,	kIx,	,
Michaela	Michaela	k1gFnSc1	Michaela
<g/>
.	.	kIx.	.
</s>
<s>
Homo	Homo	k6eAd1	Homo
ludens	ludens	k1gInSc1	ludens
Pragensis	Pragensis	k1gFnSc2	Pragensis
-	-	kIx~	-
Příspěvek	příspěvek	k1gInSc1	příspěvek
k	k	k7c3	k
dějinám	dějiny	k1gFnPc3	dějiny
karetní	karetní	k2eAgFnSc2d1	karetní
hry	hra	k1gFnSc2	hra
v	v	k7c6	v
pozdně	pozdně	k6eAd1	pozdně
středověkých	středověký	k2eAgFnPc6d1	středověká
a	a	k8xC	a
raně	raně	k6eAd1	raně
novověkých	novověký	k2eAgFnPc6d1	novověká
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
IN	IN	kA	IN
<g/>
:	:	kIx,	:
Archeologické	archeologický	k2eAgInPc1d1	archeologický
rozhledy	rozhled	k1gInPc1	rozhled
1997	[number]	k4	1997
/	/	kIx~	/
1	[number]	k4	1
<g/>
,	,	kIx,	,
ročník	ročník	k1gInSc1	ročník
149	[number]	k4	149
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
106	[number]	k4	106
-	-	kIx~	-
123	[number]	k4	123
<g/>
.	.	kIx.	.
</s>
<s>
MACEK	Macek	k1gMnSc1	Macek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Jagellonský	jagellonský	k2eAgInSc1d1	jagellonský
věk	věk	k1gInSc1	věk
českých	český	k2eAgMnPc2d1	český
zemích	zem	k1gFnPc6	zem
1	[number]	k4	1
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
OMASTA	omasta	k1gFnSc1	omasta
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
,	,	kIx,	,
RAVIK	RAVIK	kA	RAVIK
<g/>
,	,	kIx,	,
Slavomír	Slavomír	k1gMnSc1	Slavomír
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
kniha	kniha	k1gFnSc1	kniha
karetních	karetní	k2eAgFnPc2d1	karetní
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
PETRÁŇ	Petráň	k1gMnSc1	Petráň
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
hmotné	hmotný	k2eAgFnSc2d1	hmotná
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
1985	[number]	k4	1985
<g/>
-	-	kIx~	-
<g/>
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
WINTER	Winter	k1gMnSc1	Winter
<g/>
,	,	kIx,	,
Zikmund	Zikmund	k1gMnSc1	Zikmund
<g/>
.	.	kIx.	.
</s>
<s>
Kulturní	kulturní	k2eAgInSc1d1	kulturní
obraz	obraz	k1gInSc1	obraz
českých	český	k2eAgNnPc2d1	české
měst	město	k1gNnPc2	město
-	-	kIx~	-
život	život	k1gInSc1	život
veřejný	veřejný	k2eAgInSc1d1	veřejný
v	v	k7c6	v
XV	XV	kA	XV
<g/>
.	.	kIx.	.
a	a	k8xC	a
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
věku	věk	k1gInSc6	věk
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
1892	[number]	k4	1892
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
karetní	karetní	k2eAgFnSc1d1	karetní
hra	hra	k1gFnSc1	hra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Web	web	k1gInSc1	web
Pravidla	pravidlo	k1gNnSc2	pravidlo
karetních	karetní	k2eAgFnPc2d1	karetní
her	hra	k1gFnPc2	hra
Kniha	kniha	k1gFnSc1	kniha
Pokladnice	pokladnice	k1gFnSc2	pokladnice
her	hra	k1gFnPc2	hra
<g/>
/	/	kIx~	/
<g/>
Klasické	klasický	k2eAgFnSc2d1	klasická
karetní	karetní	k2eAgFnSc2d1	karetní
hry	hra	k1gFnSc2	hra
ve	v	k7c6	v
Wikiknihách	Wikiknih	k1gInPc6	Wikiknih
</s>
