<p>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
Kazimír	Kazimír	k1gMnSc1	Kazimír
Těšínský	Těšínský	k1gMnSc1	Těšínský
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Władysław	Władysław	k1gMnSc2	Władysław
Kazimierzowic	Kazimierzowice	k1gInPc2	Kazimierzowice
cieszyński	cieszyński	k6eAd1	cieszyński
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
narozen	narozen	k2eAgInSc1d1	narozen
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1325	[number]	k4	1325
<g/>
/	/	kIx~	/
<g/>
1331	[number]	k4	1331
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1355	[number]	k4	1355
<g/>
)	)	kIx)	)
–	–	k?	–
kníže	kníže	k1gMnSc1	kníže
těšínský	těšínský	k2eAgMnSc1d1	těšínský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
byl	být	k5eAaImAgMnS	být
nejstarším	starý	k2eAgMnSc7d3	nejstarší
synem	syn	k1gMnSc7	syn
knížete	kníže	k1gMnSc2	kníže
Kazimíra	Kazimír	k1gMnSc2	Kazimír
I.	I.	kA	I.
Těšínského	Těšínského	k2eAgFnSc2d1	Těšínského
a	a	k8xC	a
mazovské	mazovský	k2eAgFnSc2d1	mazovský
kněžny	kněžna	k1gFnSc2	kněžna
Eufemie	eufemie	k1gFnSc2	eufemie
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc1d1	přesné
datum	datum	k1gNnSc1	datum
narození	narození	k1gNnSc2	narození
nebylo	být	k5eNaImAgNnS	být
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
listin	listina	k1gFnPc2	listina
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1325	[number]	k4	1325
a	a	k8xC	a
1331	[number]	k4	1331
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Patřil	patřit	k5eAaImAgMnS	patřit
mezi	mezi	k7c4	mezi
těšínská	těšínský	k2eAgNnPc4d1	Těšínské
knížata	kníže	k1gNnPc4	kníže
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
na	na	k7c4	na
doporučení	doporučení	k1gNnSc4	doporučení
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
spojili	spojit	k5eAaPmAgMnP	spojit
(	(	kIx(	(
<g/>
v	v	k7c6	v
dobrém	dobrý	k2eAgNnSc6d1	dobré
i	i	k8xC	i
zlém	zlý	k2eAgNnSc6d1	zlé
<g/>
)	)	kIx)	)
se	s	k7c7	s
silnějším	silný	k2eAgMnSc7d2	silnější
panovníkem	panovník	k1gMnSc7	panovník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
těšínských	těšínský	k2eAgNnPc2d1	Těšínské
knížat	kníže	k1gNnPc2	kníže
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
i	i	k9	i
u	u	k7c2	u
ostatních	ostatní	k2eAgMnPc2d1	ostatní
slezských	slezský	k2eAgMnPc2d1	slezský
knížat	kníže	k1gMnPc2wR	kníže
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
lenním	lenní	k2eAgInSc7d1	lenní
holdem	hold	k1gInSc7	hold
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
prvopočátku	prvopočátek	k1gInSc6	prvopočátek
s	s	k7c7	s
Přemyslovci	Přemyslovec	k1gMnPc7	Přemyslovec
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
s	s	k7c7	s
Lucemburky	Lucemburk	k1gMnPc7	Lucemburk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
spojenectví	spojenectví	k1gNnSc6	spojenectví
Vladislava	Vladislav	k1gMnSc2	Vladislav
s	s	k7c7	s
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
Karlem	Karel	k1gMnSc7	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
svědčí	svědčit	k5eAaImIp3nS	svědčit
itinerář	itinerář	k1gInSc4	itinerář
knížete	kníže	k1gMnSc2	kníže
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1347	[number]	k4	1347
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
důležitých	důležitý	k2eAgFnPc6d1	důležitá
událostech	událost	k1gFnPc6	událost
<g/>
,	,	kIx,	,
jednáních	jednání	k1gNnPc6	jednání
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgFnP	být
spojeny	spojit	k5eAaPmNgInP	spojit
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1345	[number]	k4	1345
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
dvaceti	dvacet	k4xCc2	dvacet
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
titul	titul	k1gInSc4	titul
dvorní	dvorní	k2eAgMnSc1d1	dvorní
soudce	soudce	k1gMnSc1	soudce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1354	[number]	k4	1354
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
pozval	pozvat	k5eAaPmAgMnS	pozvat
své	svůj	k3xOyFgMnPc4	svůj
vazaly	vazal	k1gMnPc4	vazal
na	na	k7c4	na
výpravu	výprava	k1gFnSc4	výprava
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
si	se	k3xPyFc3	se
jel	jet	k5eAaImAgMnS	jet
pro	pro	k7c4	pro
císařskou	císařský	k2eAgFnSc4d1	císařská
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
Vladislav	Vladislav	k1gMnSc1	Vladislav
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
kruhu	kruh	k1gInSc6	kruh
jeho	jeho	k3xOp3gMnPc2	jeho
nejbližších	blízký	k2eAgMnPc2d3	nejbližší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
listinách	listina	k1gFnPc6	listina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
vydával	vydávat	k5eAaImAgMnS	vydávat
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
často	často	k6eAd1	často
zmiňován	zmiňován	k2eAgMnSc1d1	zmiňován
jako	jako	k8xS	jako
svědek	svědek	k1gMnSc1	svědek
<g/>
.	.	kIx.	.
</s>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
byl	být	k5eAaImAgMnS	být
rovněž	rovněž	k9	rovněž
přítomen	přítomen	k2eAgMnSc1d1	přítomen
při	při	k7c6	při
korunovaci	korunovace	k1gFnSc6	korunovace
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1355	[number]	k4	1355
v	v	k7c6	v
Říme	Řím	k1gInSc5	Řím
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Smrt	smrt	k1gFnSc1	smrt
==	==	k?	==
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
zápis	zápis	k1gInSc1	zápis
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Vladislavovi	Vladislav	k1gMnSc6	Vladislav
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
ze	z	k7c2	z
dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1355	[number]	k4	1355
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
kníže	kníže	k1gMnSc1	kníže
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
místo	místo	k7c2	místo
úmrtí	úmrtí	k1gNnSc2	úmrtí
se	se	k3xPyFc4	se
uvádělo	uvádět	k5eAaImAgNnS	uvádět
město	město	k1gNnSc1	město
Pisa	Pisa	k1gFnSc1	Pisa
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
knížete	kníže	k1gMnSc2	kníže
se	se	k3xPyFc4	se
detailněji	detailně	k6eAd2	detailně
rozepsal	rozepsat	k5eAaPmAgMnS	rozepsat
italský	italský	k2eAgMnSc1d1	italský
kronikář	kronikář	k1gMnSc1	kronikář
Matteo	Matteo	k6eAd1	Matteo
Villani	Villan	k1gMnPc1	Villan
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
byl	být	k5eAaImAgMnS	být
Giovanni	Giovaneň	k1gFnSc3	Giovaneň
Villani	Villaň	k1gFnSc3	Villaň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
v	v	k7c6	v
uváděl	uvádět	k5eAaImAgMnS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kníže	kníže	k1gMnSc1	kníže
odjel	odjet	k5eAaPmAgMnS	odjet
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
jistého	jistý	k2eAgMnSc2d1	jistý
českého	český	k2eAgMnSc2d1	český
velmože	velmož	k1gMnSc2	velmož
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
możnowładca	możnowładcus	k1gMnSc2	możnowładcus
<g/>
)	)	kIx)	)
Čeňka	Čeněk	k1gMnSc2	Čeněk
a	a	k8xC	a
jiných	jiný	k2eAgMnPc2d1	jiný
urozených	urozený	k2eAgMnPc2d1	urozený
pánů	pan	k1gMnPc2	pan
do	do	k7c2	do
Florencie	Florencie	k1gFnSc2	Florencie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
jako	jako	k8xS	jako
vyslance	vyslanec	k1gMnSc2	vyslanec
císaře	císař	k1gMnSc2	císař
přijali	přijmout	k5eAaPmAgMnP	přijmout
se	s	k7c7	s
všemi	všecek	k3xTgFnPc7	všecek
poctami	pocta	k1gFnPc7	pocta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
hostiny	hostina	k1gFnSc2	hostina
měl	mít	k5eAaImAgMnS	mít
kníže	kníže	k1gMnSc1	kníže
sníst	sníst	k5eAaPmF	sníst
úhoře	úhoř	k1gMnSc4	úhoř
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterého	který	k3yQgMnSc2	který
těžce	těžce	k6eAd1	těžce
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Pisy	Pisa	k1gFnSc2	Pisa
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
Vladislava	Vladislav	k1gMnSc2	Vladislav
<g/>
,	,	kIx,	,
králova	králův	k2eAgMnSc4d1	králův
oblíbence	oblíbenec	k1gMnSc4	oblíbenec
<g/>
,	,	kIx,	,
krále	král	k1gMnSc4	král
Karla	Karel	k1gMnSc4	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
velice	velice	k6eAd1	velice
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Spěšně	spěšně	k6eAd1	spěšně
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
Pise	Pisa	k1gFnSc6	Pisa
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
rozhořely	rozhořet	k5eAaPmAgInP	rozhořet
proti	proti	k7c3	proti
císařské	císařský	k2eAgFnSc2d1	císařská
nálady	nálada	k1gFnPc1	nálada
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nechtěl	chtít	k5eNaImAgMnS	chtít
způsobil	způsobit	k5eAaPmAgMnS	způsobit
krveprolití	krveprolití	k1gNnSc4	krveprolití
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
i	i	k9	i
tak	tak	k9	tak
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neměl	mít	k5eNaImAgInS	mít
jinou	jiný	k2eAgFnSc4d1	jiná
možnost	možnost	k1gFnSc4	možnost
než	než	k8xS	než
jet	jet	k5eAaImF	jet
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Vladislav	Vladislav	k1gMnSc1	Vladislav
se	se	k3xPyFc4	se
na	na	k7c6	na
knížecím	knížecí	k2eAgInSc6d1	knížecí
dvoře	dvůr	k1gInSc6	dvůr
v	v	k7c6	v
Těšíne	Těšín	k1gInSc5	Těšín
zdržoval	zdržovat	k5eAaImAgInS	zdržovat
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Nevrátil	vrátit	k5eNaPmAgMnS	vrátit
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
ani	ani	k8xC	ani
posmrtně	posmrtně	k6eAd1	posmrtně
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pochován	pochovat	k5eAaPmNgInS	pochovat
v	v	k7c6	v
Pise	Pisa	k1gFnSc6	Pisa
<g/>
,	,	kIx,	,
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
místních	místní	k2eAgFnPc2d1	místní
katedrál	katedrála	k1gFnPc2	katedrála
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ještě	ještě	k9	ještě
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
vidět	vidět	k5eAaImF	vidět
pěkně	pěkně	k6eAd1	pěkně
vyhotovený	vyhotovený	k2eAgInSc4d1	vyhotovený
náhrobek	náhrobek	k1gInSc4	náhrobek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historické	historický	k2eAgFnPc4d1	historická
pochybnosti	pochybnost	k1gFnPc4	pochybnost
==	==	k?	==
</s>
</p>
<p>
<s>
Historici	historik	k1gMnPc1	historik
se	se	k3xPyFc4	se
přou	přít	k5eAaImIp3nP	přít
o	o	k7c4	o
příčinu	příčina	k1gFnSc4	příčina
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
zda	zda	k8xS	zda
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
jen	jen	k9	jen
nešťastná	šťastný	k2eNgFnSc1d1	nešťastná
náhoda	náhoda	k1gFnSc1	náhoda
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
předem	předem	k6eAd1	předem
připravený	připravený	k2eAgInSc4d1	připravený
čin	čin	k1gInSc4	čin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
zápisů	zápis	k1gInPc2	zápis
kronikáře	kronikář	k1gMnSc4	kronikář
Mattea	Matteus	k1gMnSc4	Matteus
Villaniho	Villani	k1gMnSc4	Villani
jde	jít	k5eAaImIp3nS	jít
odvodit	odvodit	k5eAaPmF	odvodit
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
jasnou	jasný	k2eAgFnSc4d1	jasná
příčinu	příčina	k1gFnSc4	příčina
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
–	–	k?	–
Nejpravděpodobněji	pravděpodobně	k6eAd3	pravděpodobně
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
otravu	otrava	k1gFnSc4	otrava
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
nezapříčinili	zapříčinit	k5eNaPmAgMnP	zapříčinit
hostitelé	hostitel	k1gMnPc1	hostitel
záměrně	záměrně	k6eAd1	záměrně
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
k	k	k7c3	k
záměrné	záměrný	k2eAgFnSc3d1	záměrná
otravě	otrava	k1gFnSc3	otrava
našlo	najít	k5eAaPmAgNnS	najít
několik	několik	k4yIc4	několik
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
smrt	smrt	k1gFnSc1	smrt
knížete	kníže	k1gMnSc2	kníže
urychlila	urychlit	k5eAaPmAgFnS	urychlit
odjezd	odjezd	k1gInSc4	odjezd
neoblíbeného	oblíbený	k2eNgMnSc2d1	neoblíbený
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
každého	každý	k3xTgMnSc2	každý
císaře	císař	k1gMnSc2	císař
<g/>
)	)	kIx)	)
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
pro	pro	k7c4	pro
Karla	Karel	k1gMnSc4	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
varováním	varování	k1gNnSc7	varování
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
jej	on	k3xPp3gMnSc4	on
mohl	moct	k5eAaImAgInS	moct
potkat	potkat	k5eAaPmF	potkat
podobný	podobný	k2eAgInSc1d1	podobný
osud	osud	k1gInSc1	osud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
ještě	ještě	k9	ještě
jedna	jeden	k4xCgFnSc1	jeden
verze	verze	k1gFnSc1	verze
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
zemřít	zemřít	k5eAaPmF	zemřít
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
nepokojů	nepokoj	k1gInPc2	nepokoj
V	v	k7c6	v
Pise	Pisa	k1gFnSc6	Pisa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
zaměřeny	zaměřit	k5eAaPmNgFnP	zaměřit
proti	proti	k7c3	proti
císaři	císař	k1gMnSc3	císař
<g/>
,	,	kIx,	,
když	když	k8xS	když
Karlovi	Karlův	k2eAgMnPc1d1	Karlův
IV	IV	kA	IV
<g/>
.	.	kIx.	.
s	s	k7c7	s
obětování	obětování	k1gNnSc2	obětování
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
zachránil	zachránit	k5eAaPmAgMnS	zachránit
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
však	však	k9	však
pozdější	pozdní	k2eAgFnPc4d2	pozdější
verze	verze	k1gFnPc4	verze
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
neměla	mít	k5eNaImAgFnS	mít
věrohodný	věrohodný	k2eAgInSc4d1	věrohodný
základ	základ	k1gInSc4	základ
ani	ani	k8xC	ani
zdroj	zdroj	k1gInSc4	zdroj
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
císař	císař	k1gMnSc1	císař
by	by	kYmCp3nS	by
nechal	nechat	k5eAaPmAgMnS	nechat
převést	převést	k5eAaPmF	převést
Vladislavovy	Vladislavův	k2eAgInPc4d1	Vladislavův
ostatky	ostatek	k1gInPc4	ostatek
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
ev.	ev.	k?	ev.
Těšína	Těšín	k1gInSc2	Těšín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
smrtí	smrt	k1gFnSc7	smrt
Vladislava	Vladislav	k1gMnSc2	Vladislav
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
1355	[number]	k4	1355
s	s	k7c7	s
objevila	objevit	k5eAaPmAgFnS	objevit
spekulace	spekulace	k1gFnPc1	spekulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemřel	zemřít	k5eAaPmAgMnS	zemřít
později	pozdě	k6eAd2	pozdě
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
až	až	k9	až
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1355	[number]	k4	1355
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
byl	být	k5eAaImAgInS	být
dokument	dokument	k1gInSc1	dokument
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
jistého	jistý	k2eAgNnSc2d1	jisté
těšínského	těšínský	k2eAgNnSc2d1	Těšínské
knížete	kníže	k1gNnSc2wR	kníže
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
dvorního	dvorní	k2eAgMnSc2d1	dvorní
soudce	soudce	k1gMnSc2	soudce
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
však	však	k9	však
o	o	k7c4	o
jeho	jeho	k3xOp3gMnSc4	jeho
mladšího	mladý	k2eAgMnSc4d2	mladší
bratra	bratr	k1gMnSc4	bratr
Přemysla	Přemysl	k1gMnSc2	Přemysl
I.	I.	kA	I.
Nošáka	Nošák	k1gMnSc2	Nošák
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
převzal	převzít	k5eAaPmAgMnS	převzít
jeho	jeho	k3xOp3gInPc4	jeho
tituly	titul	k1gInPc4	titul
a	a	k8xC	a
úřad	úřad	k1gInSc4	úřad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zdroje	zdroj	k1gInPc1	zdroj
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Jasiński	Jasińsk	k1gMnPc1	Jasińsk
K.	K.	kA	K.
<g/>
,	,	kIx,	,
Rodowód	Rodowód	k1gMnSc1	Rodowód
Piastów	Piastów	k1gMnSc1	Piastów
śląskich	śląskich	k1gMnSc1	śląskich
<g/>
,	,	kIx,	,
t.	t.	k?	t.
3	[number]	k4	3
<g/>
,	,	kIx,	,
Wrocław	Wrocław	k1gFnSc1	Wrocław
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rajman	Rajman	k1gMnSc1	Rajman
J.	J.	kA	J.
<g/>
,	,	kIx,	,
Władysław	Władysław	k1gFnSc1	Władysław
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
w	w	k?	w
<g/>
:	:	kIx,	:
<g/>
]	]	kIx)	]
Piastowie	Piastowie	k1gFnSc1	Piastowie
<g/>
.	.	kIx.	.
</s>
<s>
Leksykon	Leksykon	k1gInSc1	Leksykon
biograficzny	biograficzna	k1gFnSc2	biograficzna
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
red	red	k?	red
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
Szczura	Szczura	k1gFnSc1	Szczura
i	i	k8xC	i
K.	K.	kA	K.
Ożoga	Ożoga	k1gFnSc1	Ożoga
<g/>
,	,	kIx,	,
Kraków	Kraków	k1gFnSc1	Kraków
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
s.	s.	k?	s.
787	[number]	k4	787
<g/>
.	.	kIx.	.
<g/>
↑	↑	k?	↑
Władysław	Władysław	k1gMnSc4	Władysław
Kazimierzowic	Kazimierzowic	k1gMnSc1	Kazimierzowic
Cieszyński	Cieszyńsk	k1gFnSc2	Cieszyńsk
<g/>
.	.	kIx.	.
www.zamki.name	www.zamki.nam	k1gInSc5	www.zamki.nam
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2019	[number]	k4	2019
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
↑	↑	k?	↑
Croniche	Croniche	k1gFnSc1	Croniche
storiche	storichat	k5eAaPmIp3nS	storichat
di	di	k?	di
Giovanni	Giovaneň	k1gFnSc3	Giovaneň
<g/>
,	,	kIx,	,
Matteo	Matteo	k6eAd1	Matteo
e	e	k0	e
Filippo	Filippa	k1gFnSc5	Filippa
Villani	Villaň	k1gFnSc6	Villaň
<g/>
,	,	kIx,	,
t.	t.	k?	t.
V	V	kA	V
<g/>
,	,	kIx,	,
s.	s.	k?	s.
417	[number]	k4	417
<g/>
-	-	kIx~	-
<g/>
418	[number]	k4	418
<g/>
,	,	kIx,	,
1848	[number]	k4	1848
<g/>
.	.	kIx.	.
</s>
</p>
