<p>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
Kazimír	Kazimír	k1gMnSc1	Kazimír
Těšínský	Těšínský	k1gMnSc1	Těšínský
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Władysław	Władysław	k1gMnSc2	Władysław
Kazimierzowic	Kazimierzowice	k1gInPc2	Kazimierzowice
cieszyński	cieszyński	k6eAd1	cieszyński
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
narozen	narozen	k2eAgInSc1d1	narozen
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1325	[number]	k4	1325
<g/>
/	/	kIx~	/
<g/>
1331	[number]	k4	1331
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1355	[number]	k4	1355
<g/>
)	)	kIx)	)
–	–	k?	–
kníže	kníže	k1gMnSc1	kníže
těšínský	těšínský	k2eAgMnSc1d1	těšínský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
byl	být	k5eAaImAgMnS	být
nejstarším	starý	k2eAgMnSc7d3	nejstarší
synem	syn	k1gMnSc7	syn
knížete	kníže	k1gMnSc2	kníže
Kazimíra	Kazimír	k1gMnSc2	Kazimír
I.	I.	kA	I.
Těšínského	Těšínského	k2eAgFnSc2d1	Těšínského
a	a	k8xC	a
mazovské	mazovský	k2eAgFnSc2d1	mazovský
kněžny	kněžna	k1gFnSc2	kněžna
Eufemie	eufemie	k1gFnSc2	eufemie
<g/>
.	.	kIx.	.
</s>
