<s>
Endoplazmatické	Endoplazmatický	k2eAgNnSc1d1	Endoplazmatické
retikulum	retikulum	k1gNnSc1	retikulum
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
povrch	povrch	k1gInSc4	povrch
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc1	význam
pro	pro	k7c4	pro
metabolické	metabolický	k2eAgInPc4d1	metabolický
procesy	proces	k1gInPc4	proces
<g/>
.	.	kIx.	.
</s>
