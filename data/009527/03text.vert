<p>
<s>
Makalu	Makala	k1gFnSc4	Makala
(	(	kIx(	(
<g/>
nepálsky	nepálsky	k6eAd1	nepálsky
म	म	k?	म
<g/>
ा	ा	k?	ा
<g/>
ल	ल	k?	ल
<g/>
ु	ु	k?	ु
Makálu	Makál	k1gInSc2	Makál
<g/>
;	;	kIx,	;
čínsky	čínsky	k6eAd1	čínsky
马	马	k?	马
<g/>
;	;	kIx,	;
pinyin	pinyin	k1gMnSc1	pinyin
Mǎ	Mǎ	k1gMnSc1	Mǎ
Shā	Shā	k1gMnSc1	Shā
<g/>
;	;	kIx,	;
význam	význam	k1gInSc1	význam
"	"	kIx"	"
<g/>
Velká	velký	k2eAgFnSc1d1	velká
tma	tma	k1gFnSc1	tma
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pátá	pátý	k4xOgFnSc1	pátý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
22	[number]	k4	22
kilometrů	kilometr	k1gInPc2	kilometr
východně	východně	k6eAd1	východně
od	od	k7c2	od
Mount	Mounta	k1gFnPc2	Mounta
Everestu	Everest	k1gInSc2	Everest
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
Nepálu	Nepál	k1gInSc2	Nepál
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
vrchol	vrchol	k1gInSc1	vrchol
měří	měřit	k5eAaImIp3nS	měřit
8463	[number]	k4	8463
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Makalu	Makala	k1gFnSc4	Makala
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejobtížněji	obtížně	k6eAd3	obtížně
dosažitelných	dosažitelný	k2eAgFnPc2d1	dosažitelná
osmitisícovek	osmitisícovka	k1gFnPc2	osmitisícovka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Hora	hora	k1gFnSc1	hora
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
svými	svůj	k3xOyFgFnPc7	svůj
strmými	strmý	k2eAgFnPc7d1	strmá
stěnami	stěna	k1gFnPc7	stěna
a	a	k8xC	a
ostrými	ostrý	k2eAgInPc7d1	ostrý
hřebeny	hřeben	k1gInPc7	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Finální	finální	k2eAgInSc1d1	finální
výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
vrcholovou	vrcholový	k2eAgFnSc4d1	vrcholová
pyramidu	pyramida	k1gFnSc4	pyramida
v	v	k7c6	v
sobě	se	k3xPyFc3	se
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
technické	technický	k2eAgNnSc4d1	technické
skalní	skalní	k2eAgNnSc4d1	skalní
lezení	lezení	k1gNnSc4	lezení
a	a	k8xC	a
velkou	velký	k2eAgFnSc4d1	velká
výšku	výška	k1gFnSc4	výška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vrcholy	vrchol	k1gInPc1	vrchol
Makalu	Makal	k1gInSc2	Makal
==	==	k?	==
</s>
</p>
<p>
<s>
Makalu	Makal	k1gInSc3	Makal
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
důležité	důležitý	k2eAgInPc4d1	důležitý
vedlejší	vedlejší	k2eAgInPc4d1	vedlejší
vrcholy	vrchol	k1gInPc4	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
je	být	k5eAaImIp3nS	být
Kangčungce	Kangčungce	k1gMnSc4	Kangčungce
(	(	kIx(	(
<g/>
Kangchungtse	Kangchungts	k1gMnSc4	Kangchungts
<g/>
,	,	kIx,	,
Makalu-La	Makalu-Lus	k1gMnSc4	Makalu-Lus
<g/>
,	,	kIx,	,
Makalu	Makala	k1gFnSc4	Makala
II	II	kA	II
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
3	[number]	k4	3
km	km	kA	km
severoseverozápadně	severoseverozápadně	k6eAd1	severoseverozápadně
od	od	k7c2	od
hlavního	hlavní	k2eAgInSc2d1	hlavní
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
významným	významný	k2eAgInSc7d1	významný
vrcholem	vrchol	k1gInSc7	vrchol
masivu	masiv	k1gInSc2	masiv
je	být	k5eAaImIp3nS	být
Čomo	Čomo	k6eAd1	Čomo
Lonzo	Lonza	k1gFnSc5	Lonza
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
Kangčungtse	Kangčungtse	k1gFnSc2	Kangčungtse
oddělen	oddělit	k5eAaPmNgInS	oddělit
úzkým	úzký	k2eAgNnSc7d1	úzké
sedlem	sedlo	k1gNnSc7	sedlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prvovýstup	prvovýstup	k1gInSc4	prvovýstup
==	==	k?	==
</s>
</p>
<p>
<s>
Makalu	Makal	k1gInSc3	Makal
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
zdolána	zdolán	k2eAgFnSc1d1	zdolána
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1955	[number]	k4	1955
Lionelem	Lionel	k1gMnSc7	Lionel
Terrayem	Terray	k1gMnSc7	Terray
a	a	k8xC	a
Jeanem	Jean	k1gMnSc7	Jean
Couzym	Couzym	k1gInSc4	Couzym
<g/>
,	,	kIx,	,
členy	člen	k1gMnPc4	člen
francouzské	francouzský	k2eAgFnSc2d1	francouzská
expedice	expedice	k1gFnSc2	expedice
vedené	vedený	k2eAgFnSc2d1	vedená
Jeanem	Jean	k1gMnSc7	Jean
Franco	Franco	k6eAd1	Franco
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgInSc1d1	francouzský
tým	tým	k1gInSc1	tým
lezl	lézt	k5eAaImAgInS	lézt
severní	severní	k2eAgFnSc7d1	severní
stěnou	stěna	k1gFnSc7	stěna
a	a	k8xC	a
severovýchodním	severovýchodní	k2eAgInSc7d1	severovýchodní
hřebenem	hřeben	k1gInSc7	hřeben
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
sedlo	sedlo	k1gNnSc4	sedlo
mezi	mezi	k7c4	mezi
Makalu	Makala	k1gFnSc4	Makala
a	a	k8xC	a
Kangčungtse	Kangčungtsa	k1gFnSc6	Kangčungtsa
<g/>
,	,	kIx,	,
takzvanou	takzvaný	k2eAgFnSc7d1	takzvaná
normální	normální	k2eAgFnSc7d1	normální
<g/>
,	,	kIx,	,
klasickou	klasický	k2eAgFnSc7d1	klasická
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
výstupů	výstup	k1gInPc2	výstup
==	==	k?	==
</s>
</p>
<p>
<s>
Makalu	Makala	k1gFnSc4	Makala
nebyla	být	k5eNaImAgFnS	být
zkoumána	zkoumán	k2eAgFnSc1d1	zkoumána
ani	ani	k8xC	ani
zlézána	zlézán	k2eAgFnSc1d1	zlézán
až	až	k9	až
do	do	k7c2	do
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nepál	Nepál	k1gInSc1	Nepál
byl	být	k5eAaImAgInS	být
cizincům	cizinec	k1gMnPc3	cizinec
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
a	a	k8xC	a
britské	britský	k2eAgFnSc2d1	britská
expedice	expedice	k1gFnSc2	expedice
postupující	postupující	k2eAgFnSc2d1	postupující
z	z	k7c2	z
Tibetu	Tibet	k1gInSc2	Tibet
směřovaly	směřovat	k5eAaImAgFnP	směřovat
výhradně	výhradně	k6eAd1	výhradně
k	k	k7c3	k
Everestu	Everest	k1gInSc3	Everest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1954	[number]	k4	1954
–	–	k?	–
První	první	k4xOgInPc4	první
pokusy	pokus	k1gInPc4	pokus
o	o	k7c4	o
výstup	výstup	k1gInSc4	výstup
provedli	provést	k5eAaPmAgMnP	provést
současně	současně	k6eAd1	současně
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
Američané	Američan	k1gMnPc1	Američan
jihovýchodním	jihovýchodní	k2eAgInSc7d1	jihovýchodní
hřebenem	hřeben	k1gInSc7	hřeben
(	(	kIx(	(
<g/>
7050	[number]	k4	7050
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Britové	Brit	k1gMnPc1	Brit
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Edmunda	Edmund	k1gMnSc2	Edmund
Hillaryho	Hillary	k1gMnSc2	Hillary
<g/>
)	)	kIx)	)
od	od	k7c2	od
severozápadu	severozápad	k1gInSc2	severozápad
(	(	kIx(	(
<g/>
6500	[number]	k4	6500
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stejnou	stejný	k2eAgFnSc7d1	stejná
trasou	trasa	k1gFnSc7	trasa
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc4	jaký
zkoumali	zkoumat	k5eAaImAgMnP	zkoumat
Britové	Brit	k1gMnPc1	Brit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
o	o	k7c4	o
výstup	výstup	k1gInSc4	výstup
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
.	.	kIx.	.
</s>
<s>
Dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
vrcholů	vrchol	k1gInPc2	vrchol
sousedních	sousední	k2eAgInPc2d1	sousední
Kangčungtse	Kangčungtse	k1gFnSc1	Kangčungtse
a	a	k8xC	a
Čomo	Čomo	k1gNnSc1	Čomo
Lonzo	Lonza	k1gFnSc5	Lonza
<g/>
.1955	.1955	k4	.1955
–	–	k?	–
Francouzi	Francouz	k1gMnPc1	Francouz
se	se	k3xPyFc4	se
vracejí	vracet	k5eAaImIp3nP	vracet
k	k	k7c3	k
hoře	hora	k1gFnSc3	hora
a	a	k8xC	a
dokončují	dokončovat	k5eAaImIp3nP	dokončovat
započatou	započatý	k2eAgFnSc4d1	započatá
trasu	trasa	k1gFnSc4	trasa
přes	přes	k7c4	přes
sedlo	sedlo	k1gNnSc4	sedlo
Makalu	Makal	k1gInSc2	Makal
severní	severní	k2eAgFnSc7d1	severní
stěnou	stěna	k1gFnSc7	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
první	první	k4xOgMnPc1	první
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
vrcholu	vrchol	k1gInSc2	vrchol
hory	hora	k1gFnPc4	hora
Lionel	Lionela	k1gFnPc2	Lionela
Terray	Terraa	k1gFnPc4	Terraa
a	a	k8xC	a
Jean	Jean	k1gMnSc1	Jean
Couzy	Couza	k1gFnSc2	Couza
<g/>
,	,	kIx,	,
po	po	k7c6	po
nich	on	k3xPp3gInPc6	on
postupně	postupně	k6eAd1	postupně
všech	všecek	k3xTgNnPc2	všecek
sedm	sedm	k4xCc4	sedm
ostatních	ostatní	k2eAgMnPc2d1	ostatní
Francouzů	Francouz	k1gMnPc2	Francouz
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
Šerpa	šerpa	k1gFnSc1	šerpa
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
výstup	výstup	k1gInSc1	výstup
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
klasickou	klasický	k2eAgFnSc7d1	klasická
cestou	cesta	k1gFnSc7	cesta
na	na	k7c6	na
Makalu	Makal	k1gInSc6	Makal
<g/>
.1961	.1961	k4	.1961
–	–	k?	–
k	k	k7c3	k
Makalu	Makal	k1gInSc3	Makal
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
Edmund	Edmund	k1gMnSc1	Edmund
Hillary	Hillara	k1gFnSc2	Hillara
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Britů	Brit	k1gMnPc2	Brit
a	a	k8xC	a
Novozélanďanů	Novozélanďan	k1gMnPc2	Novozélanďan
<g/>
.	.	kIx.	.
</s>
<s>
Klasickou	klasický	k2eAgFnSc7d1	klasická
cestou	cesta	k1gFnSc7	cesta
a	a	k8xC	a
bez	bez	k7c2	bez
kyslíkových	kyslíkový	k2eAgInPc2d1	kyslíkový
přístrojů	přístroj	k1gInPc2	přístroj
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
výšky	výška	k1gFnSc2	výška
8400	[number]	k4	8400
m	m	kA	m
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
60	[number]	k4	60
m	m	kA	m
pod	pod	k7c7	pod
vrcholem	vrchol	k1gInSc7	vrchol
<g/>
.1970	.1970	k4	.1970
–	–	k?	–
Jihovýchodní	jihovýchodní	k2eAgInSc4d1	jihovýchodní
hřeben	hřeben	k1gInSc4	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
Makalu	Makala	k1gFnSc4	Makala
cestou	cestou	k7c2	cestou
prvního	první	k4xOgInSc2	první
amerického	americký	k2eAgInSc2d1	americký
pokusu	pokus	k1gInSc2	pokus
podnikají	podnikat	k5eAaImIp3nP	podnikat
Japonci	Japonec	k1gMnPc1	Japonec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
H.	H.	kA	H.
Tanaka	Tanak	k1gMnSc4	Tanak
a	a	k8xC	a
T.	T.	kA	T.
Ozaki	Ozak	k1gFnPc4	Ozak
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
jako	jako	k8xS	jako
první	první	k4xOgMnPc1	první
na	na	k7c4	na
jižní	jižní	k2eAgInSc4d1	jižní
vrchol	vrchol	k1gInSc4	vrchol
(	(	kIx(	(
<g/>
8010	[number]	k4	8010
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.1971	.1971	k4	.1971
–	–	k?	–
Západní	západní	k2eAgInSc4d1	západní
pilíř	pilíř	k1gInSc4	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Francouzi	Francouz	k1gMnPc1	Francouz
otevírají	otevírat	k5eAaImIp3nP	otevírat
trasu	trasa	k1gFnSc4	trasa
impozantním	impozantní	k2eAgInSc7d1	impozantní
pilířem	pilíř	k1gInSc7	pilíř
oddělujícím	oddělující	k2eAgInSc7d1	oddělující
severozápadní	severozápadní	k2eAgInSc1d1	severozápadní
stěnu	stěn	k1gInSc2	stěn
od	od	k7c2	od
jižní	jižní	k2eAgFnSc2d1	jižní
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
překonání	překonání	k1gNnSc6	překonání
obtížných	obtížný	k2eAgFnPc2d1	obtížná
skalnatých	skalnatý	k2eAgFnPc2d1	skalnatá
pasáží	pasáž	k1gFnPc2	pasáž
zlézají	zlézat	k5eAaImIp3nP	zlézat
vrchol	vrchol	k1gInSc4	vrchol
Yannick	Yannick	k1gInSc1	Yannick
Seigneur	Seigneura	k1gFnPc2	Seigneura
a	a	k8xC	a
B.	B.	kA	B.
Mellet	Mellet	k1gInSc1	Mellet
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc4	jejich
výstup	výstup	k1gInSc4	výstup
zopakovali	zopakovat	k5eAaPmAgMnP	zopakovat
Američané	Američan	k1gMnPc1	Američan
bez	bez	k7c2	bez
kyslíkových	kyslíkový	k2eAgInPc2d1	kyslíkový
přístrojů	přístroj	k1gInPc2	přístroj
a	a	k8xC	a
bez	bez	k7c2	bez
podpory	podpora	k1gFnSc2	podpora
Šerpů	Šerp	k1gMnPc2	Šerp
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vrcholu	vrchol	k1gInSc2	vrchol
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
sólo	sólo	k2eAgMnSc1d1	sólo
John	John	k1gMnSc1	John
Roskelley	Roskellea	k1gFnSc2	Roskellea
<g/>
.	.	kIx.	.
</s>
<s>
Potřetí	potřetí	k4xO	potřetí
přelezli	přelézt	k5eAaPmAgMnP	přelézt
západní	západní	k2eAgInSc4d1	západní
pilíř	pilíř	k1gInSc4	pilíř
Španělé	Španěl	k1gMnPc1	Španěl
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
bodu	bod	k1gInSc2	bod
v	v	k7c6	v
8350	[number]	k4	8350
m	m	kA	m
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pilíř	pilíř	k1gInSc1	pilíř
končí	končit	k5eAaImIp3nS	končit
<g/>
,	,	kIx,	,
a	a	k8xC	a
sestoupili	sestoupit	k5eAaPmAgMnP	sestoupit
bez	bez	k7c2	bez
dosažení	dosažení	k1gNnSc2	dosažení
samého	samý	k3xTgInSc2	samý
vrcholu	vrchol	k1gInSc2	vrchol
hory	hora	k1gFnPc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc4d1	další
výstup	výstup	k1gInSc4	výstup
západním	západní	k2eAgInSc7d1	západní
pilířem	pilíř	k1gInSc7	pilíř
realizovali	realizovat	k5eAaBmAgMnP	realizovat
Švýcaři	Švýcar	k1gMnPc1	Švýcar
Erhard	Erharda	k1gFnPc2	Erharda
Loretan	Loretan	k1gInSc1	Loretan
a	a	k8xC	a
Jean	Jean	k1gMnSc1	Jean
Troillet	Troillet	k1gInSc4	Troillet
alpským	alpský	k2eAgInSc7d1	alpský
stylem	styl	k1gInSc7	styl
během	během	k7c2	během
pouhých	pouhý	k2eAgInPc2d1	pouhý
2	[number]	k4	2
dnů	den	k1gInPc2	den
<g/>
.1975	.1975	k4	.1975
–	–	k?	–
Jižní	jižní	k2eAgFnSc1d1	jižní
stěna	stěna	k1gFnSc1	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Jugoslávská	jugoslávský	k2eAgFnSc1d1	jugoslávská
expedice	expedice	k1gFnSc1	expedice
po	po	k7c6	po
předchozím	předchozí	k2eAgInSc6d1	předchozí
pokusu	pokus	k1gInSc6	pokus
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
otevírá	otevírat	k5eAaImIp3nS	otevírat
další	další	k2eAgFnSc4d1	další
obtížnou	obtížný	k2eAgFnSc4d1	obtížná
trasu	trasa	k1gFnSc4	trasa
na	na	k7c4	na
Makalu	Makala	k1gFnSc4	Makala
jižní	jižní	k2eAgFnSc7d1	jižní
stěnou	stěna	k1gFnSc7	stěna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
stěně	stěna	k1gFnSc6	stěna
byla	být	k5eAaImAgFnS	být
předtím	předtím	k6eAd1	předtím
neúspěšná	úspěšný	k2eNgFnSc1d1	neúspěšná
rakouská	rakouský	k2eAgFnSc1d1	rakouská
expedice	expedice	k1gFnSc1	expedice
s	s	k7c7	s
Reinholdem	Reinhold	k1gMnSc7	Reinhold
Messnerem	Messner	k1gMnSc7	Messner
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974.1976	[number]	k4	1974.1976
–	–	k?	–
Jižní	jižní	k2eAgInSc4d1	jižní
pilíř	pilíř	k1gInSc4	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
expedice	expedice	k1gFnSc1	expedice
dokončuje	dokončovat	k5eAaImIp3nS	dokončovat
trasu	trasa	k1gFnSc4	trasa
svého	svůj	k3xOyFgInSc2	svůj
vlastního	vlastní	k2eAgInSc2d1	vlastní
pokusu	pokus	k1gInSc2	pokus
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
vede	vést	k5eAaImIp3nS	vést
jižním	jižní	k2eAgInSc7d1	jižní
pilířem	pilíř	k1gInSc7	pilíř
na	na	k7c4	na
jižní	jižní	k2eAgInSc4d1	jižní
vrchol	vrchol	k1gInSc4	vrchol
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
jihovýchodním	jihovýchodní	k2eAgInSc7d1	jihovýchodní
hřebenem	hřeben	k1gInSc7	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jižní	jižní	k2eAgInSc4d1	jižní
vrchol	vrchol	k1gInSc4	vrchol
(	(	kIx(	(
<g/>
8010	[number]	k4	8010
m	m	kA	m
<g/>
)	)	kIx)	)
vystoupilo	vystoupit	k5eAaPmAgNnS	vystoupit
postupně	postupně	k6eAd1	postupně
11	[number]	k4	11
členů	člen	k1gMnPc2	člen
expedice	expedice	k1gFnSc2	expedice
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Schubert	Schubert	k1gMnSc1	Schubert
a	a	k8xC	a
Milan	Milan	k1gMnSc1	Milan
Kriššák	Kriššák	k1gMnSc1	Kriššák
<g/>
,	,	kIx,	,
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
na	na	k7c4	na
hlavní	hlavní	k2eAgInSc4d1	hlavní
vrchol	vrchol	k1gInSc4	vrchol
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
zlezl	zlézt	k5eAaPmAgMnS	zlézt
Makalu	Makal	k1gInSc2	Makal
i	i	k8xC	i
Jose	Jos	k1gInSc2	Jos
Camprubi	Camprub	k1gFnSc2	Camprub
ze	z	k7c2	z
španělské	španělský	k2eAgFnSc2d1	španělská
expedice	expedice	k1gFnSc2	expedice
postupující	postupující	k2eAgInSc1d1	postupující
jihovýchodním	jihovýchodní	k2eAgInSc7d1	jihovýchodní
hřebenem	hřeben	k1gInSc7	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Schubert	Schubert	k1gMnSc1	Schubert
zemřel	zemřít	k5eAaPmAgMnS	zemřít
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
při	při	k7c6	při
sestupu	sestup	k1gInSc6	sestup
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
jižním	jižní	k2eAgInSc7d1	jižní
pilířem	pilíř	k1gInSc7	pilíř
nebyla	být	k5eNaImAgFnS	být
dosud	dosud	k6eAd1	dosud
zopakována	zopakován	k2eAgFnSc1d1	zopakována
<g/>
.1981	.1981	k4	.1981
–	–	k?	–
Kukuczkova	Kukuczkův	k2eAgFnSc1d1	Kukuczkův
cesta	cesta	k1gFnSc1	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěchu	neúspěch	k1gInSc6	neúspěch
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
stěně	stěna	k1gFnSc6	stěna
podnikl	podniknout	k5eAaPmAgMnS	podniknout
Jerzy	Jerza	k1gFnPc4	Jerza
Kukuczka	Kukuczka	k1gFnSc1	Kukuczka
přímý	přímý	k2eAgInSc1d1	přímý
výstup	výstup	k1gInSc4	výstup
do	do	k7c2	do
sedla	sedlo	k1gNnSc2	sedlo
Makalu	Makal	k1gInSc2	Makal
a	a	k8xC	a
severozápadním	severozápadní	k2eAgInSc7d1	severozápadní
hřebenem	hřeben	k1gInSc7	hřeben
–	–	k?	–
sólo	sólo	k2eAgInSc1d1	sólo
alpským	alpský	k2eAgInSc7d1	alpský
stylem	styl	k1gInSc7	styl
<g/>
.1982	.1982	k4	.1982
–	–	k?	–
Pilíř	pilíř	k1gInSc4	pilíř
západní	západní	k2eAgFnSc2d1	západní
stěny	stěna	k1gFnSc2	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
expedice	expedice	k1gFnSc1	expedice
leze	lézt	k5eAaImIp3nS	lézt
na	na	k7c4	na
Makalu	Makala	k1gFnSc4	Makala
pilířem	pilíř	k1gInSc7	pilíř
v	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
polovině	polovina	k1gFnSc6	polovina
západní	západní	k2eAgFnSc2d1	západní
stěny	stěna	k1gFnSc2	stěna
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
7700	[number]	k4	7700
m.	m.	k?	m.
Andrzej	Andrzej	k1gMnSc1	Andrzej
Czok	Czok	k1gMnSc1	Czok
z	z	k7c2	z
polské	polský	k2eAgFnSc2d1	polská
výpravy	výprava	k1gFnSc2	výprava
později	pozdě	k6eAd2	pozdě
jejich	jejich	k3xOp3gFnSc4	jejich
cestu	cesta	k1gFnSc4	cesta
dokončuje	dokončovat	k5eAaImIp3nS	dokončovat
sólovýstupem	sólovýstup	k1gInSc7	sólovýstup
<g/>
.	.	kIx.	.
</s>
<s>
Závěr	závěr	k1gInSc1	závěr
trasy	trasa	k1gFnSc2	trasa
sleduje	sledovat	k5eAaImIp3nS	sledovat
severozápadní	severozápadní	k2eAgInSc1d1	severozápadní
hřeben	hřeben	k1gInSc1	hřeben
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
–	–	k?	–
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
Makalu	Makal	k1gInSc2	Makal
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
–	–	k?	–
Američanka	Američanka	k1gFnSc1	Američanka
Kitty	Kitta	k1gFnSc2	Kitta
Kalon-Griss	Kalon-Grissa	k1gFnPc2	Kalon-Grissa
se	s	k7c7	s
spolulezcem	spolulezec	k1gMnSc7	spolulezec
Jottem	Jott	k1gMnSc7	Jott
Chatem	Chat	k1gMnSc7	Chat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
americké	americký	k2eAgFnSc2d1	americká
expedice	expedice	k1gFnSc2	expedice
západním	západní	k2eAgInSc7d1	západní
pilířem	pilíř	k1gInSc7	pilíř
<g/>
.1993	.1993	k4	.1993
–	–	k?	–
Beghinova	Beghinův	k2eAgFnSc1d1	Beghinův
cesta	cesta	k1gFnSc1	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Francouz	Francouz	k1gMnSc1	Francouz
Pierre	Pierr	k1gInSc5	Pierr
Beghin	Beghina	k1gFnPc2	Beghina
zlézá	zlézat	k5eAaImIp3nS	zlézat
jižní	jižní	k2eAgFnSc4d1	jižní
stěnu	stěna	k1gFnSc4	stěna
sólo	sólo	k2eAgInSc7d1	sólo
alpským	alpský	k2eAgInSc7d1	alpský
stylem	styl	k1gInSc7	styl
<g/>
.	.	kIx.	.
</s>
<s>
Nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
vpravo	vpravo	k6eAd1	vpravo
od	od	k7c2	od
Jugoslávské	jugoslávský	k2eAgFnSc2d1	jugoslávská
cesty	cesta	k1gFnSc2	cesta
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
napojuje	napojovat	k5eAaImIp3nS	napojovat
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
výstupu	výstup	k1gInSc2	výstup
<g/>
.1995	.1995	k4	.1995
–	–	k?	–
Severovýchodní	severovýchodní	k2eAgInSc4d1	severovýchodní
hřeben	hřeben	k1gInSc4	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1	japonská
expedice	expedice	k1gFnSc1	expedice
otevírá	otevírat	k5eAaImIp3nS	otevírat
jedinou	jediný	k2eAgFnSc4d1	jediná
trasu	trasa	k1gFnSc4	trasa
na	na	k7c4	na
Makalu	Makala	k1gFnSc4	Makala
z	z	k7c2	z
Tibetu	Tibet	k1gInSc2	Tibet
<g/>
.1997	.1997	k4	.1997
–	–	k?	–
Západní	západní	k2eAgFnSc1d1	západní
stěna	stěna	k1gFnSc1	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
7	[number]	k4	7
neúspěšných	úspěšný	k2eNgInPc6d1	neúspěšný
pokusech	pokus	k1gInPc6	pokus
různých	různý	k2eAgFnPc2d1	různá
výprav	výprava	k1gFnPc2	výprava
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
Američané	Američan	k1gMnPc1	Američan
<g/>
,	,	kIx,	,
jaro	jaro	k6eAd1	jaro
1981	[number]	k4	1981
Kurtyka	Kurtyek	k1gInSc2	Kurtyek
<g/>
/	/	kIx~	/
<g/>
McIntyre	McIntyr	k1gMnSc5	McIntyr
<g/>
,	,	kIx,	,
podzim	podzim	k1gInSc4	podzim
1981	[number]	k4	1981
Kukuczka	Kukuczka	k1gFnSc1	Kukuczka
<g/>
/	/	kIx~	/
<g/>
Kurtyka	Kurtyka	k1gFnSc1	Kurtyka
<g/>
/	/	kIx~	/
<g/>
McIntyre	McIntyr	k1gMnSc5	McIntyr
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
Švýcaři	Švýcar	k1gMnPc1	Švýcar
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
Britové	Brit	k1gMnPc1	Brit
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
/	/	kIx~	/
<g/>
Američané	Američan	k1gMnPc1	Američan
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
Rusové	Rusová	k1gFnSc2	Rusová
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nS	dařit
prostoupit	prostoupit	k5eAaPmF	prostoupit
západní	západní	k2eAgFnSc4d1	západní
stěnu	stěna	k1gFnSc4	stěna
Makalu	Makal	k1gInSc2	Makal
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
pravé	pravý	k2eAgFnSc6d1	pravá
části	část	k1gFnSc6	část
ruské	ruský	k2eAgFnSc6d1	ruská
expedici	expedice	k1gFnSc6	expedice
vedené	vedený	k2eAgFnSc2d1	vedená
Sergejem	Sergej	k1gMnSc7	Sergej
Jefimovem	Jefimovo	k1gNnSc7	Jefimovo
a	a	k8xC	a
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Michajlovem	Michajlov	k1gInSc7	Michajlov
<g/>
.	.	kIx.	.
</s>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
družstva	družstvo	k1gNnSc2	družstvo
Salavat	Salavat	k1gFnSc2	Salavat
Chabibullin	Chabibullin	k2eAgMnSc1d1	Chabibullin
zahynul	zahynout	k5eAaPmAgMnS	zahynout
pod	pod	k7c7	pod
vrcholem	vrchol	k1gInSc7	vrchol
<g/>
,	,	kIx,	,
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
hory	hora	k1gFnSc2	hora
však	však	k9	však
postupně	postupně	k6eAd1	postupně
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
Alexej	Alexej	k1gMnSc1	Alexej
Bolotov	Bolotov	k1gInSc4	Bolotov
<g/>
,	,	kIx,	,
Jurij	Jurij	k1gFnSc4	Jurij
Jermaček	Jermačka	k1gFnPc2	Jermačka
<g/>
,	,	kIx,	,
Dmitrij	Dmitrij	k1gFnPc2	Dmitrij
Pavlenko	Pavlenka	k1gFnSc5	Pavlenka
<g/>
,	,	kIx,	,
Igor	Igor	k1gMnSc1	Igor
Bugačevskij	Bugačevskij	k1gMnSc1	Bugačevskij
(	(	kIx(	(
<g/>
zahynul	zahynout	k5eAaPmAgMnS	zahynout
při	při	k7c6	při
sestupu	sestup	k1gInSc6	sestup
<g/>
)	)	kIx)	)
and	and	k?	and
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Žilin	Žilina	k1gFnPc2	Žilina
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
technicky	technicky	k6eAd1	technicky
nejobtížnější	obtížný	k2eAgFnSc4d3	nejobtížnější
cestu	cesta	k1gFnSc4	cesta
na	na	k7c4	na
Makalu	Makala	k1gFnSc4	Makala
<g/>
.	.	kIx.	.
</s>
<s>
Výstup	výstup	k1gInSc1	výstup
byl	být	k5eAaImAgInS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
cenou	cena	k1gFnSc7	cena
Zlatý	zlatý	k2eAgInSc4d1	zlatý
cepín	cepín	k1gInSc4	cepín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
se	se	k3xPyFc4	se
Italovi	Ital	k1gMnSc3	Ital
Simone	Simon	k1gMnSc5	Simon
Morovi	Mora	k1gMnSc6	Mora
a	a	k8xC	a
Kazachstánci	Kazachstánec	k1gMnSc6	Kazachstánec
Děnisi	Děnis	k1gMnSc6	Děnis
Urubkovi	Urubek	k1gMnSc6	Urubek
zdařil	zdařit	k5eAaPmAgMnS	zdařit
první	první	k4xOgInSc4	první
zimní	zimní	k2eAgInSc4d1	zimní
výstup	výstup	k1gInSc4	výstup
na	na	k7c4	na
Makalu	Makala	k1gFnSc4	Makala
klasickou	klasický	k2eAgFnSc7d1	klasická
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Makalu	Makala	k1gFnSc4	Makala
byla	být	k5eAaImAgFnS	být
poslední	poslední	k2eAgFnSc7d1	poslední
nepálskou	nepálský	k2eAgFnSc7d1	nepálská
osmitisícovkou	osmitisícovka	k1gFnSc7	osmitisícovka
bez	bez	k7c2	bez
zimního	zimní	k2eAgInSc2d1	zimní
výstupu	výstup	k1gInSc2	výstup
<g/>
,	,	kIx,	,
při	při	k7c6	při
předchozím	předchozí	k2eAgInSc6d1	předchozí
pokusu	pokus	k1gInSc6	pokus
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
zmizel	zmizet	k5eAaPmAgMnS	zmizet
beze	beze	k7c2	beze
stopy	stopa	k1gFnSc2	stopa
francouzský	francouzský	k2eAgMnSc1d1	francouzský
horolezec	horolezec	k1gMnSc1	horolezec
Jean-Christophe	Jean-Christoph	k1gFnSc2	Jean-Christoph
Lafaille	Lafaille	k1gFnSc2	Lafaille
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Možnosti	možnost	k1gFnPc4	možnost
dalších	další	k2eAgInPc2d1	další
prvovýstupů	prvovýstup	k1gInPc2	prvovýstup
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
nepálské	nepálský	k2eAgFnSc2d1	nepálská
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
nabízejí	nabízet	k5eAaImIp3nP	nabízet
možnosti	možnost	k1gFnPc1	možnost
mimořádně	mimořádně	k6eAd1	mimořádně
obtížných	obtížný	k2eAgInPc2d1	obtížný
výstupů	výstup	k1gInPc2	výstup
středem	středem	k7c2	středem
jižní	jižní	k2eAgFnSc2d1	jižní
stěny	stěna	k1gFnSc2	stěna
do	do	k7c2	do
sedla	sedlo	k1gNnSc2	sedlo
mezi	mezi	k7c7	mezi
jižním	jižní	k2eAgInSc7d1	jižní
a	a	k8xC	a
hlavním	hlavní	k2eAgInSc7d1	hlavní
vrcholem	vrchol	k1gInSc7	vrchol
a	a	k8xC	a
západní	západní	k2eAgFnSc7d1	západní
stěnou	stěna	k1gFnSc7	stěna
středem	střed	k1gInSc7	střed
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc1d1	východní
stěna	stěna	k1gFnSc1	stěna
Makalu	Makal	k1gInSc2	Makal
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
téměř	téměř	k6eAd1	téměř
nelezitelná	lezitelný	k2eNgFnSc1d1	nelezitelná
pro	pro	k7c4	pro
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
lavin	lavina	k1gFnPc2	lavina
<g/>
.	.	kIx.	.
</s>
<s>
Možností	možnost	k1gFnSc7	možnost
z	z	k7c2	z
tibetské	tibetský	k2eAgFnSc2d1	tibetská
strany	strana	k1gFnSc2	strana
je	být	k5eAaImIp3nS	být
výstup	výstup	k1gInSc4	výstup
japonskou	japonský	k2eAgFnSc7d1	japonská
cestou	cesta	k1gFnSc7	cesta
od	od	k7c2	od
severozápadu	severozápad	k1gInSc2	severozápad
na	na	k7c4	na
Čomo	Čomo	k1gNnSc4	Čomo
Lonzo	Lonza	k1gFnSc5	Lonza
s	s	k7c7	s
navázáním	navázání	k1gNnSc7	navázání
do	do	k7c2	do
severní	severní	k2eAgFnSc2d1	severní
stěny	stěna	k1gFnSc2	stěna
nebo	nebo	k8xC	nebo
na	na	k7c4	na
severovýchodní	severovýchodní	k2eAgInSc4d1	severovýchodní
hřeben	hřeben	k1gInSc4	hřeben
Makalu	Makal	k1gInSc2	Makal
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
otevřený	otevřený	k2eAgInSc1d1	otevřený
také	také	k9	také
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
přechod	přechod	k1gInSc4	přechod
Makalu	Makal	k1gInSc2	Makal
od	od	k7c2	od
jihovýchodu	jihovýchod	k1gInSc2	jihovýchod
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výstupy	výstup	k1gInPc4	výstup
českých	český	k2eAgMnPc2d1	český
horolezců	horolezec	k1gMnPc2	horolezec
==	==	k?	==
</s>
</p>
<p>
<s>
1976	[number]	k4	1976
–	–	k?	–
Karel	Karel	k1gMnSc1	Karel
Schubert	Schubert	k1gMnSc1	Schubert
–	–	k?	–
jižním	jižní	k2eAgInSc7d1	jižní
pilířem	pilíř	k1gInSc7	pilíř
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc4	první
Čech	Čechy	k1gFnPc2	Čechy
na	na	k7c4	na
osmitisícovce	osmitisícovec	k1gInPc4	osmitisícovec
(	(	kIx(	(
<g/>
s	s	k7c7	s
M.	M.	kA	M.
Kriššákem	Kriššák	k1gMnSc7	Kriššák
a	a	k8xC	a
J.	J.	kA	J.
Camprubim	Camprubim	k1gInSc1	Camprubim
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zahynul	zahynout	k5eAaPmAgMnS	zahynout
při	při	k7c6	při
sestupu	sestup	k1gInSc6	sestup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
–	–	k?	–
Leopold	Leopold	k1gMnSc1	Leopold
Sulovský	Sulovský	k1gMnSc1	Sulovský
–	–	k?	–
Kukuczkova	Kukuczkův	k2eAgFnSc1d1	Kukuczkův
cesta	cesta	k1gFnSc1	cesta
alpským	alpský	k2eAgNnPc3d1	alpské
stylem	styl	k1gInSc7	styl
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Šimůnek	Šimůnek	k1gMnSc1	Šimůnek
–	–	k?	–
klasickou	klasický	k2eAgFnSc7d1	klasická
cestou	cesta	k1gFnSc7	cesta
bez	bez	k7c2	bez
kyslíku	kyslík	k1gInSc2	kyslík
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
–	–	k?	–
Soňa	Soňa	k1gFnSc1	Soňa
Boštíková	Boštíková	k1gFnSc1	Boštíková
–	–	k?	–
klasickou	klasický	k2eAgFnSc7d1	klasická
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
na	na	k7c6	na
Makalu	Makal	k1gInSc6	Makal
bez	bez	k7c2	bez
kyslíku	kyslík	k1gInSc2	kyslík
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
–	–	k?	–
Radek	Radek	k1gMnSc1	Radek
Jaroš	Jaroš	k1gMnSc1	Jaroš
–	–	k?	–
klasickou	klasický	k2eAgFnSc7d1	klasická
cestou	cesta	k1gFnSc7	cesta
bez	bez	k7c2	bez
kyslíku	kyslík	k1gInSc2	kyslík
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Makalu	Makal	k1gInSc2	Makal
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Makalu	Makala	k1gFnSc4	Makala
na	na	k7c4	na
SummitPost	SummitPost	k1gFnSc4	SummitPost
</s>
</p>
<p>
<s>
Makalu	Makala	k1gFnSc4	Makala
na	na	k7c4	na
Peakware	Peakwar	k1gMnSc5	Peakwar
</s>
</p>
<p>
<s>
Výročí	výročí	k1gNnSc1	výročí
<g/>
:	:	kIx,	:
15	[number]	k4	15
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1955	[number]	k4	1955
prvovýstup	prvovýstup	k1gInSc1	prvovýstup
na	na	k7c4	na
Makalu	Makala	k1gFnSc4	Makala
<g/>
:	:	kIx,	:
http://www.horolezeckaabeceda.cz/	[url]	k?	http://www.horolezeckaabeceda.cz/
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
Zapomenuté	zapomenutý	k2eAgFnPc1d1	zapomenutá
výpravy	výprava	k1gFnPc1	výprava
<g/>
,	,	kIx,	,
Expedice	expedice	k1gFnPc1	expedice
Makalu	Makal	k1gInSc2	Makal
<g/>
:	:	kIx,	:
http://www.ceskatelevize.cz/	[url]	k?	http://www.ceskatelevize.cz/
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
FRANCO	FRANCO	kA	FRANCO
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
<g/>
.	.	kIx.	.
</s>
<s>
Makalu	Makala	k1gFnSc4	Makala
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Jozef	Jozef	k1gMnSc1	Jozef
Brandobur	Brandobur	k1gMnSc1	Brandobur
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Osveta	Osveta	k1gFnSc1	Osveta
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
127	[number]	k4	127
s.	s.	k?	s.
302	[number]	k4	302
13	[number]	k4	13
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WOLF	Wolf	k1gMnSc1	Wolf
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
jménem	jméno	k1gNnSc7	jméno
Červánky	červánek	k1gInPc4	červánek
–	–	k?	–
Příběh	příběh	k1gInSc4	příběh
československé	československý	k2eAgFnSc2d1	Československá
horolezecké	horolezecký	k2eAgFnSc2d1	horolezecká
expedice	expedice	k1gFnSc2	expedice
Himálaj	Himálaj	k1gFnSc1	Himálaj
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
201	[number]	k4	201
<g/>
+	+	kIx~	+
<g/>
64	[number]	k4	64
s.	s.	k?	s.
27	[number]	k4	27
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
48	[number]	k4	48
<g/>
-	-	kIx~	-
<g/>
75	[number]	k4	75
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WOLF	Wolf	k1gMnSc1	Wolf
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
<g/>
.	.	kIx.	.
</s>
<s>
Šivova	Šivův	k2eAgFnSc1d1	Šivova
velká	velký	k2eAgFnSc1d1	velká
noc	noc	k1gFnSc1	noc
–	–	k?	–
Československý	československý	k2eAgInSc4d1	československý
výstup	výstup	k1gInSc4	výstup
na	na	k7c6	na
Makalu	Makal	k1gInSc6	Makal
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
175	[number]	k4	175
<g/>
+	+	kIx~	+
<g/>
32	[number]	k4	32
s.	s.	k?	s.
27	[number]	k4	27
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
26	[number]	k4	26
<g/>
-	-	kIx~	-
<g/>
79	[number]	k4	79
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GÁLFY	GÁLFY	kA	GÁLFY
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
;	;	kIx,	;
KRIŠŠÁK	KRIŠŠÁK	kA	KRIŠŠÁK
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
.	.	kIx.	.
</s>
<s>
Makalu	Makal	k1gMnSc3	Makal
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Šport	Šport	k1gInSc1	Šport
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
204	[number]	k4	204
<g/>
+	+	kIx~	+
<g/>
16	[number]	k4	16
s.	s.	k?	s.
77	[number]	k4	77
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
33	[number]	k4	33
<g/>
-	-	kIx~	-
<g/>
78	[number]	k4	78
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FIALA	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
.	.	kIx.	.
</s>
<s>
Makalu	Makal	k1gInSc2	Makal
1976	[number]	k4	1976
:	:	kIx,	:
Výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
piatu	piata	k1gFnSc4	piata
najvyššiu	najvyššius	k1gMnSc3	najvyššius
horu	hora	k1gFnSc4	hora
sveta	svet	k1gInSc2	svet
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
ČSTK	ČSTK	kA	ČSTK
-	-	kIx~	-
Pressfoto	Pressfota	k1gFnSc5	Pressfota
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
VRANKA	vranka	k1gFnSc1	vranka
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
výzvou	výzva	k1gFnSc7	výzva
velehor	velehora	k1gFnPc2	velehora
<g/>
,	,	kIx,	,
Čeští	český	k2eAgMnPc1d1	český
horolezci	horolezec	k1gMnPc1	horolezec
na	na	k7c6	na
osmitisícovkách	osmitisícovka	k1gFnPc6	osmitisícovka
1969	[number]	k4	1969
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Metafora	metafora	k1gFnSc1	metafora
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
183	[number]	k4	183
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86518	[number]	k4	86518
<g/>
-	-	kIx~	-
<g/>
91	[number]	k4	91
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NOVÁK	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Himálaj	Himálaj	k1gFnSc1	Himálaj
a	a	k8xC	a
Karakorum	Karakorum	k1gNnSc1	Karakorum
-	-	kIx~	-
Československé	československý	k2eAgFnPc1d1	Československá
a	a	k8xC	a
České	český	k2eAgInPc1d1	český
prvovýstupy	prvovýstup	k1gInPc1	prvovýstup
1969	[number]	k4	1969
<g/>
-	-	kIx~	-
<g/>
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
208	[number]	k4	208
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85613	[number]	k4	85613
<g/>
-	-	kIx~	-
<g/>
54	[number]	k4	54
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Himálaj	Himálaj	k1gFnSc1	Himálaj
</s>
</p>
<p>
<s>
Osmitisícovky	Osmitisícovka	k1gFnPc1	Osmitisícovka
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
hor	hora	k1gFnPc2	hora
světa	svět	k1gInSc2	svět
</s>
</p>
