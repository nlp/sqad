<s>
Jaká	jaký	k3yRgFnSc1	jaký
hora	hora	k1gFnSc1	hora
je	být	k5eAaImIp3nS	být
pátá	pátý	k4xOgFnSc1	pátý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
22	[number]	k4	22
kilometrů	kilometr	k1gInPc2	kilometr
východně	východně	k6eAd1	východně
od	od	k7c2	od
Mount	Mounta	k1gFnPc2	Mounta
Everestu	Everest	k1gInSc2	Everest
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
Nepálu	Nepál	k1gInSc2	Nepál
<g/>
?	?	kIx.	?
</s>
