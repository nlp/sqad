<p>
<s>
Makalu	Makala	k1gFnSc4	Makala
(	(	kIx(	(
<g/>
nepálsky	nepálsky	k6eAd1	nepálsky
म	म	k?	म
<g/>
ा	ा	k?	ा
<g/>
ल	ल	k?	ल
<g/>
ु	ु	k?	ु
Makálu	Makál	k1gInSc2	Makál
<g/>
;	;	kIx,	;
čínsky	čínsky	k6eAd1	čínsky
马	马	k?	马
<g/>
;	;	kIx,	;
pinyin	pinyin	k1gMnSc1	pinyin
Mǎ	Mǎ	k1gMnSc1	Mǎ
Shā	Shā	k1gMnSc1	Shā
<g/>
;	;	kIx,	;
význam	význam	k1gInSc1	význam
"	"	kIx"	"
<g/>
Velká	velký	k2eAgFnSc1d1	velká
tma	tma	k1gFnSc1	tma
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pátá	pátý	k4xOgFnSc1	pátý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
22	[number]	k4	22
kilometrů	kilometr	k1gInPc2	kilometr
východně	východně	k6eAd1	východně
od	od	k7c2	od
Mount	Mounta	k1gFnPc2	Mounta
Everestu	Everest	k1gInSc2	Everest
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
Nepálu	Nepál	k1gInSc2	Nepál
<g/>
.	.	kIx.	.
</s>
