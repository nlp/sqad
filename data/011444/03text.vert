<p>
<s>
Pororoca	Pororoca	k6eAd1	Pororoca
je	být	k5eAaImIp3nS	být
místní	místní	k2eAgNnSc4d1	místní
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
přílivovou	přílivový	k2eAgFnSc4d1	přílivová
vlnu	vlna	k1gFnSc4	vlna
(	(	kIx(	(
<g/>
přílivový	přílivový	k2eAgInSc1d1	přílivový
příboj	příboj	k1gInSc1	příboj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
postupuje	postupovat	k5eAaImIp3nS	postupovat
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
Amazonky	Amazonka	k1gFnSc2	Amazonka
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
4-5	[number]	k4	4-5
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
znatelná	znatelný	k2eAgFnSc1d1	znatelná
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
i	i	k9	i
800	[number]	k4	800
km	km	kA	km
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
místního	místní	k2eAgInSc2d1	místní
indiánského	indiánský	k2eAgInSc2d1	indiánský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
přeložit	přeložit	k5eAaPmF	přeložit
jej	on	k3xPp3gInSc4	on
lze	lze	k6eAd1	lze
jako	jako	k9	jako
hřmící	hřmící	k2eAgFnSc2d1	hřmící
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlnu	vlna	k1gFnSc4	vlna
využívají	využívat	k5eAaImIp3nP	využívat
k	k	k7c3	k
zábavě	zábava	k1gFnSc3	zábava
surfaři	surfař	k1gMnPc1	surfař
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
o	o	k7c4	o
poměrně	poměrně	k6eAd1	poměrně
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
množství	množství	k1gNnSc3	množství
větví	větev	k1gFnPc2	větev
a	a	k8xC	a
kmenů	kmen	k1gInPc2	kmen
stromů	strom	k1gInPc2	strom
unášených	unášený	k2eAgMnPc2d1	unášený
proudem	proud	k1gInSc7	proud
<g/>
.	.	kIx.	.
</s>
</p>
