<p>
<s>
Avarua	Avarua	k6eAd1	Avarua
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
svrchovaného	svrchovaný	k2eAgInSc2d1	svrchovaný
státu	stát	k1gInSc2	stát
Cookovy	Cookův	k2eAgInPc1d1	Cookův
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pobřeží	pobřeží	k1gNnSc6	pobřeží
vulkanického	vulkanický	k2eAgInSc2d1	vulkanický
ostrova	ostrov	k1gInSc2	ostrov
Rarotonga	Rarotong	k1gMnSc2	Rarotong
<g/>
,	,	kIx,	,
patřícího	patřící	k2eAgMnSc2d1	patřící
do	do	k7c2	do
jižní	jižní	k2eAgFnSc2d1	jižní
skupiny	skupina	k1gFnSc2	skupina
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
je	být	k5eAaImIp3nS	být
i	i	k9	i
správním	správní	k2eAgNnSc7d1	správní
centrem	centrum	k1gNnSc7	centrum
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
největším	veliký	k2eAgNnSc7d3	veliký
sídlem	sídlo	k1gNnSc7	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
5	[number]	k4	5
445	[number]	k4	445
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
stav	stav	k1gInSc1	stav
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Avaře	Avara	k1gFnSc6	Avara
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přístav	přístav	k1gInSc1	přístav
Avatiu	Avatium	k1gNnSc3	Avatium
a	a	k8xC	a
letiště	letiště	k1gNnSc4	letiště
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysoké	vysoký	k2eAgFnSc3d1	vysoká
frekvenci	frekvence	k1gFnSc3	frekvence
letecké	letecký	k2eAgFnSc2d1	letecká
dopravy	doprava	k1gFnSc2	doprava
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
dopravní	dopravní	k2eAgInSc1d1	dopravní
uzel	uzel	k1gInSc1	uzel
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Cookových	Cookův	k2eAgInPc2d1	Cookův
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zdroje	zdroj	k1gInPc1	zdroj
příjmů	příjem	k1gInPc2	příjem
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc4	obchod
<g/>
,	,	kIx,	,
rybolov	rybolov	k1gInSc4	rybolov
a	a	k8xC	a
zemědělství	zemědělství	k1gNnSc4	zemědělství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Souřadnice	souřadnice	k1gFnPc1	souřadnice
sídla	sídlo	k1gNnSc2	sídlo
jsou	být	k5eAaImIp3nP	být
21	[number]	k4	21
<g/>
°	°	k?	°
<g/>
12	[number]	k4	12
<g/>
'	'	kIx"	'
jižní	jižní	k2eAgFnSc2d1	jižní
šířky	šířka	k1gFnSc2	šířka
<g/>
,	,	kIx,	,
159	[number]	k4	159
<g/>
°	°	k?	°
<g/>
46	[number]	k4	46
<g/>
'	'	kIx"	'
západní	západní	k2eAgFnSc2d1	západní
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Avarua	Avaru	k1gInSc2	Avaru
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Avarua	Avaru	k1gInSc2	Avaru
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
