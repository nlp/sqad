<s>
Babiččino	babiččin	k2eAgNnSc1d1	Babiččino
údolí	údolí	k1gNnSc1	údolí
je	být	k5eAaImIp3nS	být
národní	národní	k2eAgFnSc1d1	národní
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
a	a	k8xC	a
národní	národní	k2eAgFnSc1d1	národní
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Náchod	Náchod	k1gInSc1	Náchod
v	v	k7c6	v
turistickém	turistický	k2eAgInSc6d1	turistický
regionu	region	k1gInSc6	region
Kladské	kladský	k2eAgNnSc4d1	Kladské
pomezí	pomezí	k1gNnSc4	pomezí
v	v	k7c6	v
Královéhradeckém	královéhradecký	k2eAgInSc6d1	královéhradecký
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Chráněné	chráněný	k2eAgNnSc1d1	chráněné
území	území	k1gNnSc1	území
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
regionálního	regionální	k2eAgNnSc2d1	regionální
pracoviště	pracoviště	k1gNnSc2	pracoviště
Východní	východní	k2eAgFnPc1d1	východní
Čechy	Čechy	k1gFnPc1	Čechy
AOPK	AOPK	kA	AOPK
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
údolí	údolí	k1gNnSc2	údolí
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
názvu	název	k1gInSc2	název
díla	dílo	k1gNnSc2	dílo
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcové	k2eAgFnSc1d1	Němcové
Babička	babička	k1gFnSc1	babička
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
děj	děj	k1gInSc4	děj
sem	sem	k6eAd1	sem
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
umístila	umístit	k5eAaPmAgFnS	umístit
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
údolí	údolí	k1gNnSc6	údolí
Úpy	Úpa	k1gFnSc2	Úpa
severně	severně	k6eAd1	severně
od	od	k7c2	od
České	český	k2eAgFnSc2d1	Česká
Skalice	Skalice	k1gFnSc2	Skalice
poblíž	poblíž	k7c2	poblíž
zámku	zámek	k1gInSc2	zámek
v	v	k7c6	v
Ratibořicích	Ratibořice	k1gFnPc6	Ratibořice
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
krajinu	krajina	k1gFnSc4	krajina
Babiččiným	babiččin	k2eAgNnSc7d1	Babiččino
údolím	údolí	k1gNnSc7	údolí
nazval	nazvat	k5eAaBmAgMnS	nazvat
poprvé	poprvé	k6eAd1	poprvé
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
spisovatel	spisovatel	k1gMnSc1	spisovatel
Otakar	Otakar	k1gMnSc1	Otakar
Jedlička	Jedlička	k1gMnSc1	Jedlička
působící	působící	k2eAgMnSc1d1	působící
ve	v	k7c6	v
Smiřicích	Smiřice	k1gFnPc6	Smiřice
jako	jako	k8xS	jako
lékař	lékař	k1gMnSc1	lékař
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Rezervace	rezervace	k1gFnSc1	rezervace
"	"	kIx"	"
<g/>
Babiččino	babiččin	k2eAgNnSc1d1	Babiččino
údolí	údolí	k1gNnSc1	údolí
u	u	k7c2	u
Ratibořic	Ratibořice	k1gFnPc2	Ratibořice
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
9	[number]	k4	9
<g/>
.	.	kIx.	.
6.195	[number]	k4	6.195
<g/>
2	[number]	k4	2
vyhláškou	vyhláška	k1gFnSc7	vyhláška
československého	československý	k2eAgNnSc2d1	Československé
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
krajinný	krajinný	k2eAgInSc1d1	krajinný
celek	celek	k1gInSc1	celek
prohlášen	prohlásit	k5eAaPmNgInS	prohlásit
národní	národní	k2eAgFnSc7d1	národní
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
národní	národní	k2eAgFnSc2d1	národní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc2d1	kulturní
památky	památka	k1gFnSc2	památka
Babiččino	babiččin	k2eAgNnSc1d1	Babiččino
údolí	údolí	k1gNnSc1	údolí
je	být	k5eAaImIp3nS	být
evropsky	evropsky	k6eAd1	evropsky
významná	významný	k2eAgFnSc1d1	významná
lokalita	lokalita	k1gFnSc1	lokalita
Babiččino	babiččin	k2eAgNnSc1d1	Babiččino
údolí	údolí	k1gNnSc1	údolí
-	-	kIx~	-
Rýzmburk	Rýzmburk	k1gInSc1	Rýzmburk
<g/>
,	,	kIx,	,
vyhlášená	vyhlášený	k2eAgFnSc1d1	vyhlášená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Předmětem	předmět	k1gInSc7	předmět
ochrany	ochrana	k1gFnSc2	ochrana
je	být	k5eAaImIp3nS	být
chasmofylitická	chasmofylitický	k2eAgFnSc1d1	chasmofylitický
vegetace	vegetace	k1gFnSc1	vegetace
skalnatých	skalnatý	k2eAgInPc2d1	skalnatý
vápnitých	vápnitý	k2eAgInPc2d1	vápnitý
svahů	svah	k1gInPc2	svah
<g/>
,	,	kIx,	,
smíšené	smíšený	k2eAgInPc1d1	smíšený
porosty	porost	k1gInPc1	porost
lipových	lipový	k2eAgFnPc2d1	Lipová
a	a	k8xC	a
javorových	javorový	k2eAgFnPc2d1	Javorová
dřevin	dřevina	k1gFnPc2	dřevina
na	na	k7c6	na
svazích	svah	k1gInPc6	svah
<g/>
,	,	kIx,	,
v	v	k7c6	v
sutích	suť	k1gFnPc6	suť
a	a	k8xC	a
roklích	rokle	k1gFnPc6	rokle
a	a	k8xC	a
zdejší	zdejší	k2eAgInPc1d1	zdejší
petrifikující	petrifikující	k2eAgInPc1d1	petrifikující
prameny	pramen	k1gInPc1	pramen
s	s	k7c7	s
tvorbou	tvorba	k1gFnSc7	tvorba
pěnovců	pěnovec	k1gInPc2	pěnovec
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
necelé	celý	k2eNgFnSc2d1	necelá
3	[number]	k4	3
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
ratibořického	ratibořický	k2eAgInSc2d1	ratibořický
zámku	zámek	k1gInSc2	zámek
a	a	k8xC	a
cca	cca	kA	cca
400	[number]	k4	400
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
kryté	krytý	k2eAgFnSc2d1	krytá
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
lávky	lávka	k1gFnSc2	lávka
<g/>
,	,	kIx,	,
před	před	k7c7	před
stoupáním	stoupání	k1gNnSc7	stoupání
ke	k	k7c3	k
zřícenině	zřícenina	k1gFnSc3	zřícenina
hradu	hrad	k1gInSc2	hrad
Rýzmburka	Rýzmburka	k1gFnSc1	Rýzmburka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
Úpy	Úpa	k1gFnSc2	Úpa
nachází	nacházet	k5eAaImIp3nS	nacházet
lokalita	lokalita	k1gFnSc1	lokalita
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
prameny	pramen	k1gInPc4	pramen
Haničky	Hanička	k1gFnSc2	Hanička
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
strmou	strmý	k2eAgFnSc4d1	strmá
zvodnělou	zvodnělý	k2eAgFnSc4d1	zvodnělá
stráň	stráň	k1gFnSc4	stráň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
pásu	pás	k1gInSc6	pás
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
asi	asi	k9	asi
150	[number]	k4	150
metrů	metr	k1gInPc2	metr
vyvěrá	vyvěrat	k5eAaImIp3nS	vyvěrat
množství	množství	k1gNnSc1	množství
průsaků	průsak	k1gInPc2	průsak
a	a	k8xC	a
pramínků	pramínek	k1gInPc2	pramínek
<g/>
.	.	kIx.	.
</s>
<s>
Prosakující	prosakující	k2eAgFnSc1d1	prosakující
voda	voda	k1gFnSc1	voda
vyplavuje	vyplavovat	k5eAaImIp3nS	vyplavovat
z	z	k7c2	z
místních	místní	k2eAgFnPc2d1	místní
hornin	hornina	k1gFnPc2	hornina
-	-	kIx~	-
fylitů	fylit	k1gInPc2	fylit
<g/>
,	,	kIx,	,
pískovců	pískovec	k1gInPc2	pískovec
<g/>
,	,	kIx,	,
břidlic	břidlice	k1gFnPc2	břidlice
a	a	k8xC	a
slínovců	slínovec	k1gInPc2	slínovec
-	-	kIx~	-
vápenaté	vápenatý	k2eAgNnSc4d1	vápenaté
pojivo	pojivo	k1gNnSc4	pojivo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zde	zde	k6eAd1	zde
následně	následně	k6eAd1	následně
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
pěnovcový	pěnovcový	k2eAgInSc1d1	pěnovcový
masív	masív	k1gInSc1	masív
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
vrstva	vrstva	k1gFnSc1	vrstva
místy	místy	k6eAd1	místy
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
mocnosti	mocnost	k1gFnSc2	mocnost
až	až	k9	až
několika	několik	k4yIc2	několik
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
místní	místní	k2eAgMnPc1d1	místní
sedláci	sedlák	k1gMnPc1	sedlák
tyto	tento	k3xDgFnPc4	tento
pěnovce	pěnovka	k1gFnSc3	pěnovka
(	(	kIx(	(
<g/>
travertiny	travertin	k1gInPc1	travertin
<g/>
)	)	kIx)	)
těžili	těžit	k5eAaImAgMnP	těžit
a	a	k8xC	a
používali	používat	k5eAaImAgMnP	používat
takto	takto	k6eAd1	takto
získaný	získaný	k2eAgInSc4d1	získaný
materiál	materiál	k1gInSc4	materiál
jako	jako	k8xS	jako
vápnité	vápnitý	k2eAgNnSc4d1	vápnité
hnojivo	hnojivo	k1gNnSc4	hnojivo
na	na	k7c6	na
svých	svůj	k3xOyFgNnPc6	svůj
polích	pole	k1gNnPc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejnavštěvovanějším	navštěvovaný	k2eAgNnPc3d3	nejnavštěvovanější
místům	místo	k1gNnPc3	místo
údolí	údolí	k1gNnSc2	údolí
patří	patřit	k5eAaImIp3nS	patřit
Staré	Staré	k2eAgNnSc1d1	Staré
bělidlo	bělidlo	k1gNnSc1	bělidlo
<g/>
,	,	kIx,	,
domek	domek	k1gInSc1	domek
čp.	čp.	k?	čp.
7	[number]	k4	7
u	u	k7c2	u
Viktorčina	Viktorčin	k2eAgInSc2d1	Viktorčin
splavu	splav	k1gInSc2	splav
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
prožila	prožít	k5eAaPmAgFnS	prožít
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
roku	rok	k1gInSc2	rok
1844	[number]	k4	1844
prázdniny	prázdniny	k1gFnPc1	prázdniny
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
dětmi	dítě	k1gFnPc7	dítě
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
Babička	babička	k1gFnSc1	babička
sem	sem	k6eAd1	sem
přenesla	přenést	k5eAaPmAgFnS	přenést
své	svůj	k3xOyFgFnPc4	svůj
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
prožila	prožít	k5eAaPmAgFnS	prožít
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
babičkou	babička	k1gFnSc7	babička
Marií	Maria	k1gFnSc7	Maria
Magdalénou	Magdaléna	k1gFnSc7	Magdaléna
Novotnou	Novotná	k1gFnSc7	Novotná
na	na	k7c6	na
starém	starý	k2eAgNnSc6d1	staré
panském	panský	k2eAgNnSc6d1	panské
bělidle	bělidlo	k1gNnSc6	bělidlo
pod	pod	k7c7	pod
zámkem	zámek	k1gInSc7	zámek
v	v	k7c6	v
nedalekých	daleký	k2eNgFnPc6d1	nedaleká
Ratibořicích	Ratibořice	k1gFnPc6	Ratibořice
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc4d1	původní
bělidlo	bělidlo	k1gNnSc4	bělidlo
nechala	nechat	k5eAaPmAgFnS	nechat
majitelka	majitelka	k1gFnSc1	majitelka
zámku	zámek	k1gInSc2	zámek
Kateřina	Kateřina	k1gFnSc1	Kateřina
Zaháňská	zaháňský	k2eAgFnSc1d1	Zaháňská
zbořit	zbořit	k5eAaPmF	zbořit
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgInS	být
domek	domek	k1gInSc1	domek
čp.	čp.	k?	čp.
7	[number]	k4	7
vybaven	vybavit	k5eAaPmNgInS	vybavit
dobovým	dobový	k2eAgInSc7d1	dobový
lidovým	lidový	k2eAgInSc7d1	lidový
nábytkem	nábytek	k1gInSc7	nábytek
ze	z	k7c2	z
sbírek	sbírka	k1gFnPc2	sbírka
Muzea	muzeum	k1gNnSc2	muzeum
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
Skalici	Skalice	k1gFnSc6	Skalice
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
a	a	k8xC	a
okolí	okolí	k1gNnSc1	okolí
byly	být	k5eAaImAgFnP	být
později	pozdě	k6eAd2	pozdě
dále	daleko	k6eAd2	daleko
upraveny	upravit	k5eAaPmNgFnP	upravit
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
filmu	film	k1gInSc2	film
Babička	babička	k1gFnSc1	babička
režiséra	režisér	k1gMnSc4	režisér
Antonína	Antonín	k1gMnSc4	Antonín
Moskalyka	Moskalyek	k1gMnSc4	Moskalyek
<g/>
;	;	kIx,	;
jako	jako	k8xC	jako
připomínka	připomínka	k1gFnSc1	připomínka
povídky	povídka	k1gFnSc2	povídka
stojí	stát	k5eAaImIp3nS	stát
asi	asi	k9	asi
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
cesty	cesta	k1gFnSc2	cesta
mezi	mezi	k7c7	mezi
ratibořickým	ratibořický	k2eAgInSc7d1	ratibořický
zámkem	zámek	k1gInSc7	zámek
a	a	k8xC	a
Starým	starý	k2eAgNnSc7d1	staré
bělidlem	bělidlo	k1gNnSc7	bělidlo
také	také	k9	také
sousoší	sousoší	k1gNnSc1	sousoší
babičky	babička	k1gFnSc2	babička
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
a	a	k8xC	a
psy	pes	k1gMnPc7	pes
<g/>
.	.	kIx.	.
</s>
<s>
Viktorčin	Viktorčin	k2eAgInSc1d1	Viktorčin
splav	splav	k1gInSc1	splav
za	za	k7c7	za
Starým	starý	k2eAgNnSc7d1	staré
bělidlem	bělidlo	k1gNnSc7	bělidlo
také	také	k9	také
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	let	k1gInSc1	let
změnil	změnit	k5eAaPmAgInS	změnit
podobu	podoba	k1gFnSc4	podoba
i	i	k8xC	i
polohu	poloha	k1gFnSc4	poloha
<g/>
;	;	kIx,	;
nejprve	nejprve	k6eAd1	nejprve
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
zavodňovacích	zavodňovací	k2eAgFnPc2d1	zavodňovací
soustav	soustava	k1gFnPc2	soustava
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1842	[number]	k4	1842
až	až	k9	až
1848	[number]	k4	1848
a	a	k8xC	a
1874	[number]	k4	1874
až	až	k9	až
1875	[number]	k4	1875
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
regulace	regulace	k1gFnSc2	regulace
toku	tok	k1gInSc2	tok
Úpy	Úpa	k1gFnSc2	Úpa
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
místo	místo	k1gNnSc4	místo
původně	původně	k6eAd1	původně
dřevěného	dřevěný	k2eAgInSc2d1	dřevěný
splavu	splav	k1gInSc2	splav
bylo	být	k5eAaImAgNnS	být
osazeno	osazen	k2eAgNnSc4d1	osazeno
betonové	betonový	k2eAgNnSc4d1	betonové
a	a	k8xC	a
kamenné	kamenný	k2eAgNnSc4d1	kamenné
čelo	čelo	k1gNnSc4	čelo
-	-	kIx~	-
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc3	rok
1970	[number]	k4	1970
při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
filmu	film	k1gInSc2	film
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
obnovení	obnovení	k1gNnSc4	obnovení
staršího	starý	k2eAgInSc2d2	starší
vzhledu	vzhled	k1gInSc2	vzhled
obloženo	obložen	k2eAgNnSc4d1	obloženo
kmeny	kmen	k1gInPc7	kmen
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
splav	splav	k1gInSc1	splav
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
současné	současný	k2eAgFnSc2d1	současná
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
cesty	cesta	k1gFnSc2	cesta
mezi	mezi	k7c7	mezi
babiččiným	babiččin	k2eAgInSc7d1	babiččin
pomníkem	pomník	k1gInSc7	pomník
a	a	k8xC	a
Starým	starý	k2eAgNnSc7d1	staré
bělidlem	bělidlo	k1gNnSc7	bělidlo
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
dubem	dub	k1gInSc7	dub
kamenný	kamenný	k2eAgInSc4d1	kamenný
křížek	křížek	k1gInSc4	křížek
s	s	k7c7	s
letopočtem	letopočet	k1gInSc7	letopočet
1866	[number]	k4	1866
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
nemá	mít	k5eNaImIp3nS	mít
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
s	s	k7c7	s
Viktorkou	Viktorka	k1gFnSc7	Viktorka
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
mylně	mylně	k6eAd1	mylně
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
památku	památka	k1gFnSc4	památka
na	na	k7c4	na
padlého	padlý	k2eAgMnSc4d1	padlý
vojáka	voják	k1gMnSc4	voják
<g/>
.	.	kIx.	.
</s>
<s>
Křížek	křížek	k1gInSc1	křížek
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
uprostřed	uprostřed	k7c2	uprostřed
louky	louka	k1gFnSc2	louka
<g/>
,	,	kIx,	,
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
byl	být	k5eAaImAgMnS	být
přemístěn	přemístit	k5eAaPmNgMnS	přemístit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
jej	on	k3xPp3gInSc4	on
několikrát	několikrát	k6eAd1	několikrát
poškodili	poškodit	k5eAaPmAgMnP	poškodit
zemědělci	zemědělec	k1gMnPc1	zemědělec
při	při	k7c6	při
sekání	sekání	k1gNnSc6	sekání
trávy	tráva	k1gFnSc2	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Babiččin	babiččin	k2eAgInSc4d1	babiččin
pomník	pomník	k1gInSc4	pomník
se	s	k7c7	s
sochami	socha	k1gFnPc7	socha
v	v	k7c6	v
nadživotní	nadživotní	k2eAgFnSc6d1	nadživotní
velikosti	velikost	k1gFnSc6	velikost
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
původně	původně	k6eAd1	původně
umístěn	umístit	k5eAaPmNgMnS	umístit
uprostřed	uprostřed	k7c2	uprostřed
louky	louka	k1gFnSc2	louka
<g/>
.	.	kIx.	.
</s>
<s>
Postavy	postava	k1gFnPc1	postava
měly	mít	k5eAaImAgFnP	mít
budit	budit	k5eAaImF	budit
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
navracejí	navracet	k5eAaBmIp3nP	navracet
ze	z	k7c2	z
zámku	zámek	k1gInSc2	zámek
na	na	k7c4	na
Staré	Staré	k2eAgNnSc4d1	Staré
bělidlo	bělidlo	k1gNnSc4	bělidlo
<g/>
;	;	kIx,	;
pozdvižené	pozdvižený	k2eAgFnSc2d1	pozdvižená
hlavy	hlava	k1gFnSc2	hlava
dětí	dítě	k1gFnPc2	dítě
pak	pak	k6eAd1	pak
měly	mít	k5eAaImAgFnP	mít
jakoby	jakoby	k8xS	jakoby
vyhlížet	vyhlížet	k5eAaImF	vyhlížet
první	první	k4xOgFnPc4	první
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
majitel	majitel	k1gMnSc1	majitel
louky	louka	k1gFnSc2	louka
však	však	k9	však
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
lidé	člověk	k1gMnPc1	člověk
nepošlapali	pošlapat	k5eNaPmAgMnP	pošlapat
trávu	tráva	k1gFnSc4	tráva
na	na	k7c6	na
louce	louka	k1gFnSc6	louka
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
socha	socha	k1gFnSc1	socha
umístěna	umístit	k5eAaPmNgFnS	umístit
hned	hned	k6eAd1	hned
u	u	k7c2	u
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostního	slavnostní	k2eAgNnSc2d1	slavnostní
poklepání	poklepání	k1gNnSc2	poklepání
na	na	k7c4	na
základní	základní	k2eAgInSc4d1	základní
kámen	kámen	k1gInSc4	kámen
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
i	i	k9	i
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zde	zde	k6eAd1	zde
měl	mít	k5eAaImAgInS	mít
proslov	proslov	k1gInSc1	proslov
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
slavnostního	slavnostní	k2eAgNnSc2d1	slavnostní
odhalení	odhalení	k1gNnSc2	odhalení
pomníku	pomník	k1gInSc2	pomník
byl	být	k5eAaImAgMnS	být
natočen	natočen	k2eAgMnSc1d1	natočen
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
němý	němý	k2eAgInSc1d1	němý
<g/>
,	,	kIx,	,
film	film	k1gInSc1	film
Babička	babička	k1gFnSc1	babička
<g/>
.	.	kIx.	.
</s>
