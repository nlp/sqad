<s>
Babiččino	babiččin	k2eAgNnSc1d1	Babiččino
údolí	údolí	k1gNnSc1	údolí
je	být	k5eAaImIp3nS	být
národní	národní	k2eAgFnSc1d1	národní
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
a	a	k8xC	a
národní	národní	k2eAgFnSc1d1	národní
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Náchod	Náchod	k1gInSc1	Náchod
v	v	k7c6	v
turistickém	turistický	k2eAgInSc6d1	turistický
regionu	region	k1gInSc6	region
Kladské	kladský	k2eAgNnSc4d1	Kladské
pomezí	pomezí	k1gNnSc4	pomezí
v	v	k7c6	v
Královéhradeckém	královéhradecký	k2eAgInSc6d1	královéhradecký
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Chráněné	chráněný	k2eAgNnSc1d1	chráněné
území	území	k1gNnSc1	území
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
regionálního	regionální	k2eAgNnSc2d1	regionální
pracoviště	pracoviště	k1gNnSc2	pracoviště
Východní	východní	k2eAgFnPc1d1	východní
Čechy	Čechy	k1gFnPc1	Čechy
AOPK	AOPK	kA	AOPK
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
údolí	údolí	k1gNnSc2	údolí
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
názvu	název	k1gInSc2	název
díla	dílo	k1gNnSc2	dílo
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcové	k2eAgFnSc1d1	Němcové
Babička	babička	k1gFnSc1	babička
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
děj	děj	k1gInSc4	děj
sem	sem	k6eAd1	sem
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
umístila	umístit	k5eAaPmAgFnS	umístit
<g/>
.	.	kIx.	.
</s>

