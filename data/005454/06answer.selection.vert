<s>
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
am	am	k?	am
Main	Main	k1gInSc1	Main
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
Frankobrod	Frankobrod	k1gInSc1	Frankobrod
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
německé	německý	k2eAgFnSc2d1	německá
spolkové	spolkový	k2eAgFnSc2d1	spolková
země	zem	k1gFnSc2	zem
Hesensko	Hesensko	k1gNnSc1	Hesensko
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
pátým	pátý	k4xOgNnSc7	pátý
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
