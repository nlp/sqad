<s>
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
am	am	k?	am
Main	Main	k1gInSc1	Main
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
Frankobrod	Frankobrod	k1gInSc1	Frankobrod
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
německé	německý	k2eAgFnSc2d1	německá
spolkové	spolkový	k2eAgFnSc2d1	spolková
země	zem	k1gFnSc2	zem
Hesensko	Hesensko	k1gNnSc1	Hesensko
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
pátým	pátý	k4xOgNnSc7	pátý
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Mohanu	Mohan	k1gInSc2	Mohan
<g/>
.	.	kIx.	.
</s>
<s>
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
je	být	k5eAaImIp3nS	být
multikulturním	multikulturní	k2eAgNnSc7d1	multikulturní
velkoměstem	velkoměsto	k1gNnSc7	velkoměsto
s	s	k7c7	s
nádechem	nádech	k1gInSc7	nádech
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
charakter	charakter	k1gInSc1	charakter
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
srovnáván	srovnáván	k2eAgMnSc1d1	srovnáván
s	s	k7c7	s
New	New	k1gMnSc7	New
Yorkem	York	k1gInSc7	York
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
Frankfurt	Frankfurt	k1gInSc4	Frankfurt
Němci	Němec	k1gMnPc1	Němec
rádi	rád	k2eAgMnPc1d1	rád
přezdívají	přezdívat	k5eAaImIp3nP	přezdívat
na	na	k7c4	na
"	"	kIx"	"
<g/>
Big	Big	k1gMnSc4	Big
Ebble	Ebbl	k1gMnSc4	Ebbl
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
je	být	k5eAaImIp3nS	být
přezdíván	přezdívat	k5eAaImNgInS	přezdívat
"	"	kIx"	"
<g/>
Big	Big	k1gMnSc1	Big
Apple	Apple	kA	Apple
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
je	být	k5eAaImIp3nS	být
také	také	k9	také
přezdíván	přezdívat	k5eAaImNgInS	přezdívat
"	"	kIx"	"
<g/>
Bankfurt	Bankfurt	k1gInSc1	Bankfurt
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
finančním	finanční	k2eAgNnSc7d1	finanční
srdcem	srdce	k1gNnSc7	srdce
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
centru	centrum	k1gNnSc6	centrum
sídlí	sídlet	k5eAaImIp3nS	sídlet
bankovní	bankovní	k2eAgFnSc1d1	bankovní
instituce	instituce	k1gFnSc1	instituce
světového	světový	k2eAgInSc2d1	světový
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Mohanu	Mohan	k1gInSc2	Mohan
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
označována	označován	k2eAgFnSc1d1	označována
"	"	kIx"	"
<g/>
Mainhattan	Mainhattan	k1gInSc1	Mainhattan
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgNnSc7d1	důležité
veletržním	veletržní	k2eAgNnSc7d1	veletržní
městem	město	k1gNnSc7	město
a	a	k8xC	a
pro	pro	k7c4	pro
Evropu	Evropa	k1gFnSc4	Evropa
významným	významný	k2eAgInSc7d1	významný
dopravním	dopravní	k2eAgInSc7d1	dopravní
a	a	k8xC	a
internetovým	internetový	k2eAgInSc7d1	internetový
uzlem	uzel	k1gInSc7	uzel
<g/>
.	.	kIx.	.
</s>
<s>
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgMnSc1d1	ležící
na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
březích	břeh	k1gInPc6	břeh
Mohanu	Mohan	k1gInSc2	Mohan
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
24	[number]	k4	24
830,6	[number]	k4	830,6
hektarů	hektar	k1gInPc2	hektar
<g/>
,	,	kIx,	,
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
čítá	čítat	k5eAaImIp3nS	čítat
délku	délka	k1gFnSc4	délka
23,3	[number]	k4	23,3
km	km	kA	km
<g/>
,	,	kIx,	,
z	z	k7c2	z
východu	východ	k1gInSc2	východ
na	na	k7c4	na
západ	západ	k1gInSc4	západ
23,4	[number]	k4	23,4
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Obvodová	obvodový	k2eAgFnSc1d1	obvodová
hranice	hranice	k1gFnSc1	hranice
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
113	[number]	k4	113
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
je	být	k5eAaImIp3nS	být
Berger	Berger	k1gMnSc1	Berger
Warte	Wart	k1gInSc5	Wart
(	(	kIx(	(
<g/>
212	[number]	k4	212
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejnižším	nízký	k2eAgInSc6d3	nejnižší
je	být	k5eAaImIp3nS	být
břeh	břeh	k1gInSc1	břeh
Mohanu	Mohan	k1gInSc2	Mohan
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
<g/>
/	/	kIx~	/
<g/>
Sidlingen	Sidlingen	k1gInSc1	Sidlingen
<g/>
/	/	kIx~	/
<g/>
Okriftel	Okriftel	k1gInSc1	Okriftel
(	(	kIx(	(
<g/>
88	[number]	k4	88
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Centrem	centr	k1gInSc7	centr
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
Hauptwache	Hauptwache	k1gNnSc1	Hauptwache
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
dopravním	dopravní	k2eAgInSc7d1	dopravní
uzlem	uzel	k1gInSc7	uzel
a	a	k8xC	a
nákupním	nákupní	k2eAgNnSc7d1	nákupní
centrem	centrum	k1gNnSc7	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
břehu	břeh	k1gInSc2	břeh
řeky	řeka	k1gFnSc2	řeka
Mohan	Mohan	k1gInSc1	Mohan
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
hranic	hranice	k1gFnPc2	hranice
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
je	být	k5eAaImIp3nS	být
16	[number]	k4	16
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
zmíněn	zmínit	k5eAaPmNgInS	zmínit
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
794	[number]	k4	794
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
Karla	Karel	k1gMnSc2	Karel
Velikého	veliký	k2eAgMnSc2d1	veliký
pro	pro	k7c4	pro
Řezenský	řezenský	k2eAgInSc4d1	řezenský
klášter	klášter	k1gInSc4	klášter
Sv.	sv.	kA	sv.
Emmerama	Emmeram	k1gMnSc2	Emmeram
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
listině	listina	k1gFnSc6	listina
bylo	být	k5eAaImAgNnS	být
latinsky	latinsky	k6eAd1	latinsky
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
Actum	Actum	k1gNnSc1	Actum
super	super	k2eAgNnSc1d1	super
fluvium	fluvium	k1gNnSc1	fluvium
Moine	Moin	k1gInSc5	Moin
in	in	k?	in
loco	loca	k1gMnSc5	loca
nuncupante	nuncupant	k1gMnSc5	nuncupant
Franconofurd	Franconofurd	k1gInSc4	Franconofurd
<g/>
"	"	kIx"	"
-	-	kIx~	-
"	"	kIx"	"
<g/>
vystavené	vystavená	k1gFnPc1	vystavená
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Mohan	Mohan	k1gInSc4	Mohan
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgInPc4d1	zvaný
Frankfurt	Frankfurt	k1gInSc4	Frankfurt
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Nepřetržité	přetržitý	k2eNgNnSc1d1	nepřetržité
osídlení	osídlení	k1gNnSc1	osídlení
této	tento	k3xDgFnSc2	tento
pahorkatiny	pahorkatina	k1gFnSc2	pahorkatina
je	být	k5eAaImIp3nS	být
však	však	k9	však
dokázáno	dokázat	k5eAaPmNgNnS	dokázat
od	od	k7c2	od
mladší	mladý	k2eAgFnSc2d2	mladší
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
po	po	k7c6	po
římském	římský	k2eAgInSc6d1	římský
vojenském	vojenský	k2eAgInSc6d1	vojenský
táboře	tábor	k1gInSc6	tábor
v	v	k7c6	v
Merovejovských	Merovejovský	k2eAgInPc6d1	Merovejovský
časech	čas	k1gInPc6	čas
franský	franský	k2eAgInSc1d1	franský
královský	královský	k2eAgInSc1d1	královský
dvůr	dvůr	k1gInSc1	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
843	[number]	k4	843
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
dočasně	dočasně	k6eAd1	dočasně
nejdůležitější	důležitý	k2eAgNnSc4d3	nejdůležitější
královské	královský	k2eAgNnSc4d1	královské
Falcko	Falcko	k1gNnSc4	Falcko
východofranské	východofranský	k2eAgFnSc2d1	Východofranská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
sídlo	sídlo	k1gNnSc4	sídlo
říšského	říšský	k2eAgInSc2d1	říšský
sněmu	sněm	k1gInSc2	sněm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1220	[number]	k4	1220
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
svobodným	svobodný	k2eAgNnSc7d1	svobodné
říšským	říšský	k2eAgNnSc7d1	říšské
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
bula	bula	k1gFnSc1	bula
stanovila	stanovit	k5eAaPmAgFnS	stanovit
Frankfurt	Frankfurt	k1gInSc4	Frankfurt
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1356	[number]	k4	1356
stálým	stálý	k2eAgNnSc7d1	stálé
volebním	volební	k2eAgNnSc7d1	volební
městem	město	k1gNnSc7	město
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1147	[number]	k4	1147
nejvíce	nejvíce	k6eAd1	nejvíce
voleb	volba	k1gFnPc2	volba
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1562	[number]	k4	1562
byly	být	k5eAaImAgInP	být
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
korunováni	korunován	k2eAgMnPc1d1	korunován
i	i	k8xC	i
císaři	císař	k1gMnPc1	císař
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1792	[number]	k4	1792
František	františek	k1gInSc1	františek
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Svaté	svatý	k2eAgFnSc2d1	svatá
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
pod	pod	k7c4	pod
vládu	vláda	k1gFnSc4	vláda
knížecího	knížecí	k2eAgMnSc2d1	knížecí
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
von	von	k1gInSc4	von
Dalbergia	Dalbergium	k1gNnSc2	Dalbergium
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jej	on	k3xPp3gNnSc4	on
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgNnPc7	svůj
knížectvími	knížectví	k1gNnPc7	knížectví
Řeznem	Řezno	k1gNnSc7	Řezno
a	a	k8xC	a
Aschaffenburgu	Aschaffenburg	k1gInSc2	Aschaffenburg
sjednotil	sjednotit	k5eAaPmAgInS	sjednotit
jako	jako	k9	jako
samostatný	samostatný	k2eAgInSc1d1	samostatný
stát	stát	k1gInSc1	stát
do	do	k7c2	do
Rýnského	rýnský	k2eAgInSc2d1	rýnský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1810	[number]	k4	1810
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
Dalbergia	Dalbergius	k1gMnSc2	Dalbergius
Regensburg	Regensburg	k1gMnSc1	Regensburg
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
odškodněn	odškodnit	k5eAaPmNgMnS	odškodnit
územím	území	k1gNnSc7	území
Hanau	Hanaus	k1gInSc2	Hanaus
a	a	k8xC	a
Fulda	Fuldo	k1gNnSc2	Fuldo
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
městem	město	k1gNnSc7	město
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
a	a	k8xC	a
Aschaffenburským	Aschaffenburský	k2eAgNnSc7d1	Aschaffenburský
územím	území	k1gNnSc7	území
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
tento	tento	k3xDgInSc4	tento
a	a	k8xC	a
celek	celek	k1gInSc4	celek
krátce	krátce	k6eAd1	krátce
trvající	trvající	k2eAgMnSc1d1	trvající
(	(	kIx(	(
<g/>
1810	[number]	k4	1810
<g/>
-	-	kIx~	-
<g/>
1813	[number]	k4	1813
<g/>
)	)	kIx)	)
velkovévodství	velkovévodství	k1gNnSc2	velkovévodství
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zhroucením	zhroucení	k1gNnSc7	zhroucení
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
systému	systém	k1gInSc2	systém
se	se	k3xPyFc4	se
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
dostal	dostat	k5eAaPmAgInS	dostat
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1813	[number]	k4	1813
pod	pod	k7c4	pod
dočasnou	dočasný	k2eAgFnSc4d1	dočasná
správu	správa	k1gFnSc4	správa
vítězných	vítězný	k2eAgMnPc2d1	vítězný
spojenců	spojenec	k1gMnPc2	spojenec
pod	pod	k7c7	pod
prefektem	prefekt	k1gMnSc7	prefekt
Carlem	Carl	k1gMnSc7	Carl
von	von	k1gInSc4	von
Günderrodom	Günderrodom	k1gInSc1	Günderrodom
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
kongresu	kongres	k1gInSc6	kongres
plánovalo	plánovat	k5eAaImAgNnS	plánovat
Bavorské	bavorský	k2eAgNnSc1d1	bavorské
království	království	k1gNnSc1	království
anektování	anektování	k1gNnSc2	anektování
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výsledkem	výsledek	k1gInSc7	výsledek
kongresu	kongres	k1gInSc2	kongres
bylo	být	k5eAaImAgNnS	být
obnovení	obnovení	k1gNnSc1	obnovení
statusu	status	k1gInSc2	status
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
jako	jako	k8xS	jako
svobodného	svobodný	k2eAgNnSc2d1	svobodné
města	město	k1gNnSc2	město
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Německého	německý	k2eAgInSc2d1	německý
spolku	spolek	k1gInSc2	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
byl	být	k5eAaImAgInS	být
vedle	vedle	k7c2	vedle
Hamburku	Hamburk	k1gInSc2	Hamburk
<g/>
,	,	kIx,	,
Brém	Brémy	k1gFnPc2	Brémy
a	a	k8xC	a
Lübecku	Lübecka	k1gFnSc4	Lübecka
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgNnPc2	čtyři
svobodných	svobodný	k2eAgNnPc2d1	svobodné
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
si	se	k3xPyFc3	se
udržely	udržet	k5eAaPmAgFnP	udržet
svoji	svůj	k3xOyFgFnSc4	svůj
tradiční	tradiční	k2eAgFnSc4d1	tradiční
městskou	městský	k2eAgFnSc4d1	městská
svobodu	svoboda	k1gFnSc4	svoboda
až	až	k9	až
do	do	k7c2	do
moderních	moderní	k2eAgFnPc2d1	moderní
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
tu	tu	k6eAd1	tu
zřízen	zřídit	k5eAaPmNgInS	zřídit
Spolkový	spolkový	k2eAgInSc1d1	spolkový
sněm	sněm	k1gInSc1	sněm
Německého	německý	k2eAgInSc2d1	německý
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
německých	německý	k2eAgInPc6d1	německý
státech	stát	k1gInPc6	stát
k	k	k7c3	k
revoluci	revoluce	k1gFnSc3	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Svolané	svolaný	k2eAgNnSc1d1	svolané
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
zasedalo	zasedat	k5eAaImAgNnS	zasedat
ve	v	k7c6	v
frankfurtském	frankfurtský	k2eAgInSc6d1	frankfurtský
kostele	kostel	k1gInSc6	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rakousko-pruské	rakouskoruský	k2eAgFnSc6d1	rakousko-pruská
válce	válka	k1gFnSc6	válka
zůstal	zůstat	k5eAaPmAgInS	zůstat
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
věrný	věrný	k2eAgInSc1d1	věrný
Německému	německý	k2eAgInSc3d1	německý
svazu	svaz	k1gInSc3	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Veřejné	veřejný	k2eAgNnSc1d1	veřejné
mínění	mínění	k1gNnSc1	mínění
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
jeho	on	k3xPp3gMnSc2	on
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
také	také	k9	také
z	z	k7c2	z
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
ozývaly	ozývat	k5eAaImAgInP	ozývat
hlasy	hlas	k1gInPc1	hlas
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
z	z	k7c2	z
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
a	a	k8xC	a
zahraničněpolitických	zahraničněpolitický	k2eAgInPc2d1	zahraničněpolitický
důvodů	důvod	k1gInPc2	důvod
hájily	hájit	k5eAaImAgFnP	hájit
dobrovolné	dobrovolný	k2eAgNnSc4d1	dobrovolné
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
Prusku	Prusko	k1gNnSc3	Prusko
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
obsazeno	obsadit	k5eAaPmNgNnS	obsadit
pruskou	pruský	k2eAgFnSc7d1	pruská
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zatíženo	zatížit	k5eAaPmNgNnS	zatížit
velkou	velký	k2eAgFnSc7d1	velká
kontribucí	kontribuce	k1gFnSc7	kontribuce
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
anektováno	anektován	k2eAgNnSc1d1	anektováno
Pruskem	Prusko	k1gNnSc7	Prusko
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
čeho	co	k3yInSc2	co
definitivně	definitivně	k6eAd1	definitivně
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
svůj	svůj	k3xOyFgInSc4	svůj
statut	statut	k1gInSc4	statut
městského	městský	k2eAgInSc2d1	městský
státu	stát	k1gInSc2	stát
<g/>
;	;	kIx,	;
bylo	být	k5eAaImAgNnS	být
přiřazeno	přiřazen	k2eAgNnSc1d1	přiřazeno
k	k	k7c3	k
vládnímu	vládní	k2eAgInSc3d1	vládní
obvodu	obvod	k1gInSc3	obvod
Wiesbaden	Wiesbaden	k1gInSc4	Wiesbaden
provincie	provincie	k1gFnSc2	provincie
Hessen-Nassau	Hessen-Nassaus	k1gInSc2	Hessen-Nassaus
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
symbol	symbol	k1gInSc4	symbol
usmíření	usmíření	k1gNnSc2	usmíření
byla	být	k5eAaImAgFnS	být
frankfurtským	frankfurtský	k2eAgInSc7d1	frankfurtský
mírem	mír	k1gInSc7	mír
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
oficiálně	oficiálně	k6eAd1	oficiálně
ukončena	ukončit	k5eAaPmNgFnS	ukončit
prusko-francouzská	pruskorancouzský	k2eAgFnSc1d1	prusko-francouzská
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
vývoj	vývoj	k1gInSc4	vývoj
města	město	k1gNnSc2	město
na	na	k7c4	na
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
město	město	k1gNnSc4	město
s	s	k7c7	s
rychlým	rychlý	k2eAgInSc7d1	rychlý
růstem	růst	k1gInSc7	růst
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
byla	být	k5eAaImAgFnS	být
anexe	anexe	k1gFnSc1	anexe
výhodná	výhodný	k2eAgFnSc1d1	výhodná
<g/>
.	.	kIx.	.
</s>
<s>
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
přičlenil	přičlenit	k5eAaPmAgInS	přičlenit
v	v	k7c6	v
příštích	příští	k2eAgNnPc6d1	příští
desetiletích	desetiletí	k1gNnPc6	desetiletí
množství	množství	k1gNnSc2	množství
okolních	okolní	k2eAgFnPc2d1	okolní
obcí	obec	k1gFnPc2	obec
a	a	k8xC	a
měst	město	k1gNnPc2	město
a	a	k8xC	a
takto	takto	k6eAd1	takto
zdvojnásobil	zdvojnásobit	k5eAaPmAgInS	zdvojnásobit
svou	svůj	k3xOyFgFnSc4	svůj
plochu	plocha	k1gFnSc4	plocha
oproti	oproti	k7c3	oproti
roku	rok	k1gInSc3	rok
1866	[number]	k4	1866
<g/>
.	.	kIx.	.
</s>
<s>
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
dokonce	dokonce	k9	dokonce
na	na	k7c4	na
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
plochou	plochý	k2eAgFnSc4d1	plochá
nejrozsáhlejším	rozsáhlý	k2eAgNnSc7d3	nejrozsáhlejší
městem	město	k1gNnSc7	město
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dobách	doba	k1gFnPc6	doba
nacionálního	nacionální	k2eAgInSc2d1	nacionální
socialismu	socialismus	k1gInSc2	socialismus
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
deportováno	deportovat	k5eAaBmNgNnS	deportovat
9	[number]	k4	9
000	[number]	k4	000
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následné	následný	k2eAgFnSc6d1	následná
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
zničili	zničit	k5eAaPmAgMnP	zničit
cílené	cílený	k2eAgInPc4d1	cílený
letecké	letecký	k2eAgInPc4d1	letecký
útoky	útok	k1gInPc4	útok
téměř	téměř	k6eAd1	téměř
úplně	úplně	k6eAd1	úplně
Altstadt	Altstadt	k2eAgMnSc1d1	Altstadt
a	a	k8xC	a
Innenstadt	Innenstadt	k2eAgMnSc1d1	Innenstadt
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
téměř	téměř	k6eAd1	téměř
čistě	čistě	k6eAd1	čistě
středověký	středověký	k2eAgInSc4d1	středověký
charakter	charakter	k1gInSc4	charakter
města	město	k1gNnSc2	město
-	-	kIx~	-
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
podobě	podoba	k1gFnSc6	podoba
již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
jedinečný	jedinečný	k2eAgInSc1d1	jedinečný
pro	pro	k7c4	pro
německé	německý	k2eAgNnSc4d1	německé
velkoměsto	velkoměsto	k1gNnSc4	velkoměsto
-	-	kIx~	-
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
moderní	moderní	k2eAgFnSc2d1	moderní
obnovy	obnova	k1gFnSc2	obnova
vytratil	vytratit	k5eAaPmAgMnS	vytratit
<g/>
.	.	kIx.	.
</s>
<s>
Kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
kdysi	kdysi	k6eAd1	kdysi
rozkládala	rozkládat	k5eAaImAgFnS	rozkládat
spleť	spleť	k1gFnSc4	spleť
uliček	ulička	k1gFnPc2	ulička
mezi	mezi	k7c7	mezi
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
těsně	těsně	k6eAd1	těsně
postavených	postavený	k2eAgInPc2d1	postavený
hrázděných	hrázděný	k2eAgInPc2d1	hrázděný
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
velké	velký	k2eAgFnPc1d1	velká
části	část	k1gFnPc1	část
někdejšího	někdejší	k2eAgNnSc2d1	někdejší
historického	historický	k2eAgNnSc2d1	historické
jádra	jádro	k1gNnSc2	jádro
formovány	formovat	k5eAaImNgInP	formovat
betonovými	betonový	k2eAgFnPc7d1	betonová
stavbami	stavba	k1gFnPc7	stavba
z	z	k7c2	z
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
si	se	k3xPyFc3	se
americká	americký	k2eAgFnSc1d1	americká
okupační	okupační	k2eAgFnSc1d1	okupační
armáda	armáda	k1gFnSc1	armáda
zřídila	zřídit	k5eAaPmAgFnS	zřídit
ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlo	sídlo	k1gNnSc1	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
volbě	volba	k1gFnSc6	volba
o	o	k7c4	o
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
velmi	velmi	k6eAd1	velmi
těsně	těsně	k6eAd1	těsně
prohrál	prohrát	k5eAaPmAgMnS	prohrát
proti	proti	k7c3	proti
Adenauerovu	Adenauerův	k2eAgMnSc3d1	Adenauerův
favoritovi	favorit	k1gMnSc3	favorit
Bonnu	Bonn	k1gInSc2	Bonn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
postavená	postavený	k2eAgFnSc1d1	postavená
budova	budova	k1gFnSc1	budova
parlamentu	parlament	k1gInSc2	parlament
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
tak	tak	k6eAd1	tak
dala	dát	k5eAaPmAgFnS	dát
přístřeší	přístřeší	k1gNnSc2	přístřeší
Hesenskému	hesenský	k2eAgInSc3d1	hesenský
rozhlasu	rozhlas	k1gInSc3	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poválečných	poválečný	k2eAgInPc6d1	poválečný
časech	čas	k1gInPc6	čas
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
opět	opět	k6eAd1	opět
hospodářskou	hospodářský	k2eAgFnSc7d1	hospodářská
metropolí	metropol	k1gFnSc7	metropol
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
sídlem	sídlo	k1gNnSc7	sídlo
Evropské	evropský	k2eAgFnSc2d1	Evropská
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
na	na	k7c6	na
červeném	červený	k2eAgNnSc6d1	červené
pozadí	pozadí	k1gNnSc6	pozadí
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
orlici	orlice	k1gFnSc4	orlice
se	s	k7c7	s
zlatou	zlatý	k2eAgFnSc7d1	zlatá
korunou	koruna	k1gFnSc7	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Orlice	Orlice	k1gFnSc1	Orlice
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
někdejší	někdejší	k2eAgInSc4d1	někdejší
status	status	k1gInSc4	status
města	město	k1gNnSc2	město
jako	jako	k9	jako
svobodné	svobodný	k2eAgNnSc4d1	svobodné
říšské	říšský	k2eAgNnSc4d1	říšské
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
znak	znak	k1gInSc1	znak
nahrazen	nahrazen	k2eAgInSc1d1	nahrazen
radikálně	radikálně	k6eAd1	radikálně
zjednodušenou	zjednodušený	k2eAgFnSc7d1	zjednodušená
formou	forma	k1gFnSc7	forma
<g/>
.	.	kIx.	.
</s>
<s>
Primátor	primátor	k1gMnSc1	primátor
Ludwig	Ludwig	k1gMnSc1	Ludwig
Landmann	Landmann	k1gMnSc1	Landmann
chtěl	chtít	k5eAaImAgMnS	chtít
podle	podle	k7c2	podle
duchu	duch	k1gMnSc6	duch
času	čas	k1gInSc2	čas
zaměnit	zaměnit	k5eAaPmF	zaměnit
orlici	orlice	k1gFnSc4	orlice
expresionistickou	expresionistický	k2eAgFnSc7d1	expresionistická
verzí	verze	k1gFnSc7	verze
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
se	se	k3xPyFc4	se
ale	ale	k9	ale
tento	tento	k3xDgInSc1	tento
návrh	návrh	k1gInSc1	návrh
nelíbil	líbit	k5eNaImAgInS	líbit
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
byl	být	k5eAaImAgInS	být
znak	znak	k1gInSc1	znak
vrácen	vrátit	k5eAaPmNgInS	vrátit
zpět	zpět	k6eAd1	zpět
na	na	k7c6	na
původní	původní	k2eAgFnSc6d1	původní
verzi	verze	k1gFnSc6	verze
<g/>
.	.	kIx.	.
</s>
<s>
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
je	být	k5eAaImIp3nS	být
páté	pátý	k4xOgNnSc1	pátý
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
po	po	k7c6	po
Berlínu	Berlín	k1gInSc3	Berlín
(	(	kIx(	(
<g/>
3,5	[number]	k4	3,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hamburku	Hamburk	k1gInSc2	Hamburk
(	(	kIx(	(
<g/>
1,8	[number]	k4	1,8
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mnichově	Mnichov	k1gInSc6	Mnichov
(	(	kIx(	(
<g/>
1,5	[number]	k4	1,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kolíně	Kolín	k1gInSc6	Kolín
(	(	kIx(	(
<g/>
980	[number]	k4	980
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
je	být	k5eAaImIp3nS	být
multikulturním	multikulturní	k2eAgNnSc7d1	multikulturní
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
obyvatelé	obyvatel	k1gMnPc1	obyvatel
ze	z	k7c2	z
180	[number]	k4	180
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
podíl	podíl	k1gInSc1	podíl
cizinců	cizinec	k1gMnPc2	cizinec
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
veškerého	veškerý	k3xTgNnSc2	veškerý
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
je	být	k5eAaImIp3nS	být
také	také	k9	také
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xS	jako
město	město	k1gNnSc1	město
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
podílem	podíl	k1gInSc7	podíl
singles	singlesa	k1gFnPc2	singlesa
-	-	kIx~	-
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
polovina	polovina	k1gFnSc1	polovina
domácností	domácnost	k1gFnPc2	domácnost
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
jednoho	jeden	k4xCgMnSc4	jeden
člena	člen	k1gMnSc4	člen
<g/>
.	.	kIx.	.
</s>
<s>
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
je	být	k5eAaImIp3nS	být
centrem	centrum	k1gNnSc7	centrum
nejatraktivnějšího	atraktivní	k2eAgInSc2d3	nejatraktivnější
a	a	k8xC	a
hospodářsky	hospodářsky	k6eAd1	hospodářsky
nejsilnějšího	silný	k2eAgInSc2d3	nejsilnější
evropského	evropský	k2eAgInSc2d1	evropský
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
poloha	poloha	k1gFnSc1	poloha
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Evropy	Evropa	k1gFnSc2	Evropa
láká	lákat	k5eAaImIp3nS	lákat
především	především	k6eAd1	především
bankovní	bankovní	k2eAgInPc4d1	bankovní
instituty	institut	k1gInPc4	institut
<g/>
.	.	kIx.	.
</s>
<s>
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
je	být	k5eAaImIp3nS	být
nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
evropskou	evropský	k2eAgFnSc7d1	Evropská
bankovní	bankovní	k2eAgFnSc7d1	bankovní
a	a	k8xC	a
burzovní	burzovní	k2eAgFnSc7d1	burzovní
metropolí	metropol	k1gFnSc7	metropol
<g/>
.	.	kIx.	.
</s>
<s>
Banky	banka	k1gFnPc1	banka
stojí	stát	k5eAaImIp3nP	stát
také	také	k9	také
za	za	k7c7	za
zrodem	zrod	k1gInSc7	zrod
nového	nový	k2eAgInSc2d1	nový
symbolu	symbol	k1gInSc2	symbol
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
Skyline	Skylin	k1gInSc5	Skylin
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
nejmodernějšími	moderní	k2eAgInPc7d3	nejmodernější
mrakodrapy	mrakodrap	k1gInPc7	mrakodrap
jako	jako	k8xC	jako
důkaz	důkaz	k1gInSc1	důkaz
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
bohatství	bohatství	k1gNnSc2	bohatství
bank	banka	k1gFnPc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
sídlí	sídlet	k5eAaImIp3nS	sídlet
až	až	k9	až
315	[number]	k4	315
kreditních	kreditní	k2eAgInPc2d1	kreditní
institutů	institut	k1gInPc2	institut
a	a	k8xC	a
175	[number]	k4	175
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
bank	banka	k1gFnPc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc4	sídlo
zde	zde	k6eAd1	zde
našla	najít	k5eAaPmAgFnS	najít
Bundesbank	Bundesbank	k1gInSc4	Bundesbank
(	(	kIx(	(
<g/>
Německá	německý	k2eAgFnSc1d1	německá
národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Deutsche	Deutsche	k1gNnSc1	Deutsche
Bank	banka	k1gFnPc2	banka
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
také	také	k9	také
Evropská	evropský	k2eAgFnSc1d1	Evropská
centrální	centrální	k2eAgFnSc1d1	centrální
banka	banka	k1gFnSc1	banka
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
úřední	úřední	k2eAgFnSc7d1	úřední
budovou	budova	k1gFnSc7	budova
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
mrakodrap	mrakodrap	k1gInSc4	mrakodrap
Komerční	komerční	k2eAgFnSc2d1	komerční
banky	banka	k1gFnSc2	banka
-	-	kIx~	-
Commerzbank	Commerzbank	k1gMnSc1	Commerzbank
(	(	kIx(	(
<g/>
300	[number]	k4	300
m	m	kA	m
včetně	včetně	k7c2	včetně
antény	anténa	k1gFnSc2	anténa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Frankfurtská	frankfurtský	k2eAgFnSc1d1	Frankfurtská
burza	burza	k1gFnSc1	burza
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
a	a	k8xC	a
místními	místní	k2eAgFnPc7d1	místní
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
chrám	chrám	k1gInSc4	chrám
kapitalistů	kapitalista	k1gMnPc2	kapitalista
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
budovou	budova	k1gFnSc7	budova
stojí	stát	k5eAaImIp3nP	stát
bronzové	bronzový	k2eAgFnPc1d1	bronzová
sochy	socha	k1gFnPc1	socha
medvěda	medvěd	k1gMnSc2	medvěd
a	a	k8xC	a
býka	býk	k1gMnSc2	býk
<g/>
.	.	kIx.	.
</s>
<s>
Býk	býk	k1gMnSc1	býk
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
vztyčenou	vztyčený	k2eAgFnSc7d1	vztyčená
hlavou	hlava	k1gFnSc7	hlava
vzhůru	vzhůru	k6eAd1	vzhůru
znakem	znak	k1gInSc7	znak
vzrůstající	vzrůstající	k2eAgFnSc2d1	vzrůstající
ceny	cena	k1gFnSc2	cena
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
medvěd	medvěd	k1gMnSc1	medvěd
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
těžkopádných	těžkopádný	k2eAgMnPc2d1	těžkopádný
<g/>
,	,	kIx,	,
klesajících	klesající	k2eAgMnPc2d1	klesající
kurzů	kurz	k1gInPc2	kurz
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
Frankfurtské	frankfurtský	k2eAgFnSc2d1	Frankfurtská
burzy	burza	k1gFnSc2	burza
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
do	do	k7c2	do
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
první	první	k4xOgInSc1	první
důležitý	důležitý	k2eAgInSc1d1	důležitý
krok	krok	k1gInSc1	krok
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1585	[number]	k4	1585
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nejdůležitější	důležitý	k2eAgMnSc1d3	nejdůležitější
a	a	k8xC	a
největší	veliký	k2eAgMnSc1d3	veliký
velkokupci	velkokupec	k1gMnPc1	velkokupec
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
Burs	bursa	k1gFnPc2	bursa
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
účelem	účel	k1gInSc7	účel
bylo	být	k5eAaImAgNnS	být
stanovit	stanovit	k5eAaPmF	stanovit
a	a	k8xC	a
sjednotit	sjednotit	k5eAaPmF	sjednotit
hodnotu	hodnota	k1gFnSc4	hodnota
mincí	mince	k1gFnPc2	mince
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
regionů	region	k1gInPc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
Frankfurtská	frankfurtský	k2eAgFnSc1d1	Frankfurtská
burza	burza	k1gFnSc1	burza
třetí	třetí	k4xOgFnSc2	třetí
největší	veliký	k2eAgFnSc2d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
patří	patřit	k5eAaImIp3nS	patřit
New	New	k1gFnSc4	New
York	York	k1gInSc1	York
Stock	Stock	k1gMnSc1	Stock
Exchange	Exchange	k1gFnSc1	Exchange
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
obsadil	obsadit	k5eAaPmAgMnS	obsadit
Nasdaq	Nasdaq	k1gFnPc4	Nasdaq
<g/>
)	)	kIx)	)
a	a	k8xC	a
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
se	se	k3xPyFc4	se
na	na	k7c4	na
vícero	vícero	k1gNnSc4	vícero
místech	místo	k1gNnPc6	místo
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
budova	budova	k1gFnSc1	budova
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
určena	určit	k5eAaPmNgFnS	určit
především	především	k9	především
pro	pro	k7c4	pro
televizní	televizní	k2eAgInPc4d1	televizní
přenosy	přenos	k1gInPc4	přenos
a	a	k8xC	a
pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Frankfurtské	frankfurtský	k2eAgNnSc1d1	Frankfurtské
hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
(	(	kIx(	(
<g/>
Main	Main	k1gInSc1	Main
<g/>
)	)	kIx)	)
Hauptbahnhof	Hauptbahnhof	k1gInSc1	Hauptbahnhof
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
a	a	k8xC	a
největších	veliký	k2eAgInPc2d3	veliký
dopravních	dopravní	k2eAgInPc2d1	dopravní
uzlů	uzel	k1gInPc2	uzel
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
nádraží	nádraží	k1gNnSc2	nádraží
je	být	k5eAaImIp3nS	být
architektonickým	architektonický	k2eAgInSc7d1	architektonický
skvostem	skvost	k1gInSc7	skvost
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
prošla	projít	k5eAaPmAgFnS	projít
kompletní	kompletní	k2eAgFnSc7d1	kompletní
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
-	-	kIx~	-
čelo	čelo	k1gNnSc1	čelo
budovy	budova	k1gFnSc2	budova
zdobí	zdobit	k5eAaImIp3nS	zdobit
Atlas	Atlas	k1gInSc1	Atlas
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
projede	projet	k5eAaPmIp3nS	projet
nádražím	nádraží	k1gNnSc7	nádraží
přes	přes	k7c4	přes
360	[number]	k4	360
000	[number]	k4	000
cestujících	cestující	k1gMnPc2	cestující
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
zdrojů	zdroj	k1gInPc2	zdroj
Deutsche	Deutsch	k1gFnSc2	Deutsch
Bahn	Bahna	k1gFnPc2	Bahna
ročně	ročně	k6eAd1	ročně
90	[number]	k4	90
miliónů	milión	k4xCgInPc2	milión
pasažérů	pasažér	k1gMnPc2	pasažér
<g/>
)	)	kIx)	)
a	a	k8xC	a
kolem	kolem	k7c2	kolem
1800	[number]	k4	1800
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Frankfurtské	frankfurtský	k2eAgNnSc1d1	Frankfurtské
nádraží	nádraží	k1gNnSc1	nádraží
je	být	k5eAaImIp3nS	být
také	také	k9	také
označováno	označován	k2eAgNnSc4d1	označováno
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejbezpečnějších	bezpečný	k2eAgInPc2d3	nejbezpečnější
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Přímé	přímý	k2eAgNnSc1d1	přímé
spojení	spojení	k1gNnSc1	spojení
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
s	s	k7c7	s
Českou	český	k2eAgFnSc7d1	Česká
republikou	republika	k1gFnSc7	republika
nabízí	nabízet	k5eAaImIp3nS	nabízet
vlak	vlak	k1gInSc1	vlak
kategorie	kategorie	k1gFnSc2	kategorie
EuroNight	EuroNighta	k1gFnPc2	EuroNighta
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Curych	Curych	k1gInSc1	Curych
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
však	však	k9	však
z	z	k7c2	z
dopravních	dopravní	k2eAgInPc2d1	dopravní
důvodů	důvod	k1gInPc2	důvod
neobsluhuje	obsluhovat	k5eNaImIp3nS	obsluhovat
hlavní	hlavní	k2eAgNnSc4d1	hlavní
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zastavuje	zastavovat	k5eAaImIp3nS	zastavovat
na	na	k7c6	na
zastávce	zastávka	k1gFnSc6	zastávka
Süd	Süd	k1gFnSc1	Süd
vzdálené	vzdálený	k2eAgFnPc1d1	vzdálená
asi	asi	k9	asi
2	[number]	k4	2
km	km	kA	km
<g/>
,	,	kIx,	,
během	během	k7c2	během
dne	den	k1gInSc2	den
je	být	k5eAaImIp3nS	být
nejrychlejší	rychlý	k2eAgNnSc4d3	nejrychlejší
spojení	spojení	k1gNnSc4	spojení
IC	IC	kA	IC
Busem	bus	k1gInSc7	bus
s	s	k7c7	s
přestupem	přestup	k1gInSc7	přestup
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
vlakového	vlakový	k2eAgNnSc2d1	vlakové
nádraží	nádraží	k1gNnSc2	nádraží
je	být	k5eAaImIp3nS	být
autobusové	autobusový	k2eAgNnSc4d1	autobusové
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Autobusové	autobusový	k2eAgFnPc1d1	autobusová
linky	linka	k1gFnPc1	linka
mezi	mezi	k7c7	mezi
Českem	Česko	k1gNnSc7	Česko
a	a	k8xC	a
Německem	Německo	k1gNnSc7	Německo
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
společnosti	společnost	k1gFnSc3	společnost
FlixBus	FlixBus	k1gMnSc1	FlixBus
a	a	k8xC	a
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
mimořádný	mimořádný	k2eAgInSc4d1	mimořádný
význam	význam	k1gInSc4	význam
má	mít	k5eAaImIp3nS	mít
letiště	letiště	k1gNnSc1	letiště
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2010	[number]	k4	2010
je	být	k5eAaImIp3nS	být
letiště	letiště	k1gNnSc2	letiště
na	na	k7c6	na
třetím	třetí	k4xOgNnSc6	třetí
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
devátém	devátý	k4xOgNnSc6	devátý
na	na	k7c6	na
světě	svět	k1gInSc6	svět
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
přepravených	přepravený	k2eAgFnPc2d1	přepravená
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
na	na	k7c6	na
jedenáctém	jedenáctý	k4xOgMnSc6	jedenáctý
na	na	k7c6	na
světě	svět	k1gInSc6	svět
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
vzletů	vzlet	k1gInPc2	vzlet
a	a	k8xC	a
přistání	přistání	k1gNnSc4	přistání
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
v	v	k7c6	v
objemu	objem	k1gInSc6	objem
přepravovaného	přepravovaný	k2eAgInSc2d1	přepravovaný
nákladu	náklad	k1gInSc2	náklad
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
na	na	k7c6	na
sedmém	sedmý	k4xOgInSc6	sedmý
celosvětově	celosvětově	k6eAd1	celosvětově
<g/>
.	.	kIx.	.
</s>
<s>
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
dopravním	dopravní	k2eAgInSc7d1	dopravní
centrem	centr	k1gInSc7	centr
Evropy	Evropa	k1gFnSc2	Evropa
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
spolkem	spolek	k1gInSc7	spolek
RMV	RMV	kA	RMV
Rhein-Main-Verkehrsverbund	Rhein-Main-Verkehrsverbund	k1gInSc1	Rhein-Main-Verkehrsverbund
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Propojuje	propojovat	k5eAaImIp3nS	propojovat
15	[number]	k4	15
krajů	kraj	k1gInPc2	kraj
<g/>
,	,	kIx,	,
4	[number]	k4	4
velkoměsta	velkoměsto	k1gNnSc2	velkoměsto
<g/>
,	,	kIx,	,
spravuje	spravovat	k5eAaImIp3nS	spravovat
oblast	oblast	k1gFnSc4	oblast
velkou	velký	k2eAgFnSc4d1	velká
14	[number]	k4	14
000	[number]	k4	000
km	km	kA	km
<g/>
2	[number]	k4	2
a	a	k8xC	a
čítá	čítat	k5eAaImIp3nS	čítat
přes	přes	k7c4	přes
10	[number]	k4	10
000	[number]	k4	000
zastávek	zastávka	k1gFnPc2	zastávka
<g/>
.	.	kIx.	.
</s>
<s>
RMV	RMV	kA	RMV
přepravuje	přepravovat	k5eAaImIp3nS	přepravovat
ročně	ročně	k6eAd1	ročně
přes	přes	k7c4	přes
600	[number]	k4	600
miliónů	milión	k4xCgInPc2	milión
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
RMV	RMV	kA	RMV
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
dopravu	doprava	k1gFnSc4	doprava
autobusovou	autobusový	k2eAgFnSc4d1	autobusová
(	(	kIx(	(
<g/>
Büsse	Büss	k1gInSc6	Büss
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
metro	metro	k1gNnSc1	metro
(	(	kIx(	(
<g/>
U-Bahn	U-Bahn	k1gInSc1	U-Bahn
a	a	k8xC	a
S-Bahn	S-Bahn	k1gInSc1	S-Bahn
<g/>
)	)	kIx)	)
a	a	k8xC	a
tramvaje	tramvaj	k1gFnPc1	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Frankfurtské	frankfurtský	k2eAgNnSc1d1	Frankfurtské
metro	metro	k1gNnSc1	metro
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
U-Bahn	U-Bahn	k1gNnSc1	U-Bahn
spravuje	spravovat	k5eAaImIp3nS	spravovat
dceřiná	dceřiný	k2eAgFnSc1d1	dceřiná
společnost	společnost	k1gFnSc1	společnost
VGF	VGF	kA	VGF
Verkehrsgesellschaft	Verkehrsgesellschaft	k2eAgInSc1d1	Verkehrsgesellschaft
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
RMV	RMV	kA	RMV
dále	daleko	k6eAd2	daleko
spadá	spadat	k5eAaPmIp3nS	spadat
RE	re	k9	re
(	(	kIx(	(
<g/>
Regionalexpress	Regionalexpress	k1gInSc1	Regionalexpress
<g/>
)	)	kIx)	)
a	a	k8xC	a
RB	RB	kA	RB
(	(	kIx(	(
<g/>
Regionalbahn	Regionalbahn	k1gMnSc1	Regionalbahn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Křižovatkou	křižovatka	k1gFnSc7	křižovatka
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
spoje	spoj	k1gInPc4	spoj
jsou	být	k5eAaImIp3nP	být
zastávky	zastávka	k1gFnPc1	zastávka
Hauptwache	Hauptwache	k1gNnSc2	Hauptwache
a	a	k8xC	a
Konstablerwache	Konstablerwache	k1gNnSc2	Konstablerwache
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
bodem	bod	k1gInSc7	bod
pro	pro	k7c4	pro
automobily	automobil	k1gInPc4	automobil
je	být	k5eAaImIp3nS	být
křižovatka	křižovatka	k1gFnSc1	křižovatka
Frankfurter	Frankfurtra	k1gFnPc2	Frankfurtra
Kreuz	Kreuza	k1gFnPc2	Kreuza
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
propojuje	propojovat	k5eAaImIp3nS	propojovat
dálnice	dálnice	k1gFnSc1	dálnice
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
světových	světový	k2eAgFnPc2d1	světová
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
propojuje	propojovat	k5eAaImIp3nS	propojovat
tak	tak	k6eAd1	tak
spojnice	spojnice	k1gFnSc1	spojnice
od	od	k7c2	od
Hamburku	Hamburk	k1gInSc2	Hamburk
do	do	k7c2	do
Basileje	Basilej	k1gFnSc2	Basilej
či	či	k8xC	či
z	z	k7c2	z
Düsseldorfu	Düsseldorf	k1gInSc2	Düsseldorf
do	do	k7c2	do
Mnichova	Mnichov	k1gInSc2	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Denně	denně	k6eAd1	denně
zde	zde	k6eAd1	zde
projede	projet	k5eAaPmIp3nS	projet
přes	přes	k7c4	přes
300	[number]	k4	300
000	[number]	k4	000
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
je	být	k5eAaImIp3nS	být
také	také	k9	také
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
Amsterodamu	Amsterodam	k1gInSc2	Amsterodam
a	a	k8xC	a
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
internetových	internetový	k2eAgInPc2d1	internetový
uzlů	uzel	k1gInPc2	uzel
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
DE-CIX	DE-CIX	k1gFnSc1	DE-CIX
-	-	kIx~	-
peeringové	peeringový	k2eAgNnSc1d1	peeringové
centrum	centrum	k1gNnSc1	centrum
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
datovým	datový	k2eAgInSc7d1	datový
tokem	tok	k1gInSc7	tok
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
peeringové	peeringový	k2eAgNnSc1d1	peeringové
centrum	centrum	k1gNnSc1	centrum
KleyReX	KleyReX	k1gFnSc2	KleyReX
a	a	k8xC	a
také	také	k9	také
zde	zde	k6eAd1	zde
mají	mít	k5eAaImIp3nP	mít
důležité	důležitý	k2eAgInPc1d1	důležitý
komunikační	komunikační	k2eAgInPc1d1	komunikační
uzly	uzel	k1gInPc1	uzel
všichni	všechen	k3xTgMnPc1	všechen
největší	veliký	k2eAgMnPc1d3	veliký
a	a	k8xC	a
přední	přední	k2eAgMnPc1d1	přední
světoví	světový	k2eAgMnPc1d1	světový
poskytovatelé	poskytovatel	k1gMnPc1	poskytovatel
internetového	internetový	k2eAgNnSc2d1	internetové
připojení	připojení	k1gNnSc2	připojení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
je	být	k5eAaImIp3nS	být
až	až	k9	až
33	[number]	k4	33
divadel	divadlo	k1gNnPc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
budovou	budova	k1gFnSc7	budova
je	být	k5eAaImIp3nS	být
Alte	alt	k1gInSc5	alt
Oper	opera	k1gFnPc2	opera
(	(	kIx(	(
<g/>
stará	starý	k2eAgFnSc1d1	stará
opera	opera	k1gFnSc1	opera
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
opery	opera	k1gFnSc2	opera
byla	být	k5eAaImAgFnS	být
válkou	válka	k1gFnSc7	válka
kompletně	kompletně	k6eAd1	kompletně
zničena	zničit	k5eAaPmNgNnP	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Správa	správa	k1gFnSc1	správa
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
však	však	k9	však
zasadila	zasadit	k5eAaPmAgFnS	zasadit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
znovuvybudování	znovuvybudování	k1gNnSc4	znovuvybudování
<g/>
.	.	kIx.	.
</s>
<s>
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
tak	tak	k9	tak
získal	získat	k5eAaPmAgInS	získat
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznávané	uznávaný	k2eAgNnSc4d1	uznávané
kulturní	kulturní	k2eAgNnSc4d1	kulturní
centrum	centrum	k1gNnSc4	centrum
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
se	se	k3xPyFc4	se
ročně	ročně	k6eAd1	ročně
pořádá	pořádat	k5eAaImIp3nS	pořádat
až	až	k9	až
400	[number]	k4	400
různých	různý	k2eAgNnPc2d1	různé
představení	představení	k1gNnPc2	představení
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
významnými	významný	k2eAgMnPc7d1	významný
centry	centr	k1gMnPc7	centr
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
English	English	k1gInSc1	English
Theater	Theatra	k1gFnPc2	Theatra
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
prezentuje	prezentovat	k5eAaBmIp3nS	prezentovat
divadelní	divadelní	k2eAgInPc4d1	divadelní
kousky	kousek	k1gInPc4	kousek
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
Fritz-Rémond-Theater	Fritz-Rémond-Theater	k1gInSc1	Fritz-Rémond-Theater
(	(	kIx(	(
<g/>
divadlo	divadlo	k1gNnSc1	divadlo
komedie	komedie	k1gFnSc2	komedie
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
Zoologické	zoologický	k2eAgFnSc2d1	zoologická
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Oper	oprat	k5eAaPmRp2nS	oprat
<g/>
/	/	kIx~	/
<g/>
Balett	Balett	k2eAgInSc1d1	Balett
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
či	či	k8xC	či
Tigerpalast	Tigerpalast	k1gInSc1	Tigerpalast
(	(	kIx(	(
<g/>
zážitková	zážitkový	k2eAgFnSc1d1	zážitková
gastronomie	gastronomie	k1gFnSc1	gastronomie
a	a	k8xC	a
muzikály	muzikál	k1gInPc1	muzikál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
nabízí	nabízet	k5eAaImIp3nS	nabízet
kolem	kolem	k7c2	kolem
čtyřiceti	čtyřicet	k4xCc2	čtyřicet
muzeí	muzeum	k1gNnPc2	muzeum
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
sto	sto	k4xCgNnSc1	sto
galerií	galerie	k1gFnPc2	galerie
včetně	včetně	k7c2	včetně
prodejních	prodejní	k2eAgNnPc2d1	prodejní
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
je	on	k3xPp3gMnPc4	on
navštíví	navštívit	k5eAaPmIp3nS	navštívit
přes	přes	k7c4	přes
dva	dva	k4xCgInPc4	dva
milióny	milión	k4xCgInPc4	milión
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Nejhustší	hustý	k2eAgNnPc1d3	nejhustší
zastoupení	zastoupení	k1gNnPc1	zastoupení
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
"	"	kIx"	"
<g/>
Muzejním	muzejní	k2eAgNnSc6d1	muzejní
nábřeží	nábřeží	k1gNnSc6	nábřeží
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Museumsufer	Museumsufer	k1gInSc1	Museumsufer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Mohan	Mohan	k1gInSc4	Mohan
mezi	mezi	k7c7	mezi
Železným	železný	k2eAgInSc7d1	železný
mostem	most	k1gInSc7	most
(	(	kIx(	(
<g/>
Eisernen	Eisernen	k2eAgInSc1d1	Eisernen
Steg	Steg	k1gInSc1	Steg
<g/>
)	)	kIx)	)
a	a	k8xC	a
Mostem	most	k1gInSc7	most
přátelství	přátelství	k1gNnSc2	přátelství
(	(	kIx(	(
<g/>
Friedensbrücke	Friedensbrücke	k1gNnSc2	Friedensbrücke
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
patří	patřit	k5eAaImIp3nS	patřit
Muzeum	muzeum	k1gNnSc1	muzeum
ikon	ikona	k1gFnPc2	ikona
(	(	kIx(	(
<g/>
Ikonenmuseum	Ikonenmuseum	k1gInSc1	Ikonenmuseum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
parku	park	k1gInSc6	park
situované	situovaný	k2eAgNnSc1d1	situované
Uměleckoprůmyslové	uměleckoprůmyslový	k2eAgNnSc1d1	Uměleckoprůmyslové
muzeum	muzeum	k1gNnSc1	muzeum
(	(	kIx(	(
<g/>
Museum	museum	k1gNnSc4	museum
für	für	k?	für
angewandte	angewandte	k5eAaPmIp2nP	angewandte
Kunst	Kunst	k1gFnSc4	Kunst
<g/>
)	)	kIx)	)
s	s	k7c7	s
konstruktivistickou	konstruktivistický	k2eAgFnSc7d1	konstruktivistická
budovou	budova	k1gFnSc7	budova
od	od	k7c2	od
architekta	architekt	k1gMnSc2	architekt
Niedermayera	Niedermayer	k1gMnSc2	Niedermayer
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
a	a	k8xC	a
galerie	galerie	k1gFnSc1	galerie
starého	starý	k2eAgNnSc2d1	staré
umění	umění	k1gNnSc2	umění
s	s	k7c7	s
proslulou	proslulý	k2eAgFnSc7d1	proslulá
sbírkou	sbírka	k1gFnSc7	sbírka
gotické	gotický	k2eAgFnSc2d1	gotická
plastiky	plastika	k1gFnSc2	plastika
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
domě	dům	k1gInSc6	dům
někdejšího	někdejší	k2eAgMnSc2d1	někdejší
severočeského	severočeský	k2eAgMnSc2d1	severočeský
textilního	textilní	k2eAgMnSc2d1	textilní
továrníka	továrník	k1gMnSc2	továrník
Liebiega	Liebieg	k1gMnSc2	Liebieg
(	(	kIx(	(
<g/>
Liebieghaus	Liebieghaus	k1gMnSc1	Liebieghaus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
městu	město	k1gNnSc3	město
odkázal	odkázat	k5eAaPmAgInS	odkázat
svou	svůj	k3xOyFgFnSc4	svůj
soukromou	soukromý	k2eAgFnSc4d1	soukromá
sbírku	sbírka	k1gFnSc4	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
muzeum	muzeum	k1gNnSc1	muzeum
starého	starý	k2eAgNnSc2d1	staré
malířství	malířství	k1gNnSc2	malířství
a	a	k8xC	a
sochařství	sochařství	k1gNnSc2	sochařství
je	být	k5eAaImIp3nS	být
Städel	Städlo	k1gNnPc2	Städlo
(	(	kIx(	(
<g/>
das	das	k?	das
Städel	Städlo	k1gNnPc2	Städlo
<g/>
,	,	kIx,	,
<g/>
)	)	kIx)	)
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
Německé	německý	k2eAgNnSc1d1	německé
filmové	filmový	k2eAgNnSc1d1	filmové
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historickém	historický	k2eAgNnSc6d1	historické
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
rodný	rodný	k2eAgInSc4d1	rodný
dům	dům	k1gInSc4	dům
J.	J.	kA	J.
W.	W.	kA	W.
Goetheho	Goethe	k1gMnSc2	Goethe
<g/>
,	,	kIx,	,
Židovské	židovský	k2eAgNnSc1d1	Židovské
muzeum	muzeum	k1gNnSc1	muzeum
(	(	kIx(	(
<g/>
das	das	k?	das
Jüdische	Jüdische	k1gInSc1	Jüdische
Museum	museum	k1gNnSc1	museum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
karmelitánském	karmelitánský	k2eAgInSc6d1	karmelitánský
klášteře	klášter	k1gInSc6	klášter
sídlí	sídlet	k5eAaImIp3nS	sídlet
Archeologické	archeologický	k2eAgNnSc1d1	Archeologické
muzeum	muzeum	k1gNnSc1	muzeum
(	(	kIx(	(
<g/>
Das	Das	k1gFnSc1	Das
Archäologisches	Archäologisches	k1gInSc1	Archäologisches
Museum	museum	k1gNnSc1	museum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
Muzeum	muzeum	k1gNnSc1	muzeum
architektury	architektura	k1gFnSc2	architektura
(	(	kIx(	(
<g/>
Deutsches	Deutsches	k1gInSc1	Deutsches
Architekturmuseum	Architekturmuseum	k1gInSc1	Architekturmuseum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Přírodovědecké	přírodovědecký	k2eAgNnSc1d1	Přírodovědecké
muzeum	muzeum	k1gNnSc1	muzeum
(	(	kIx(	(
<g/>
das	das	k?	das
Naturmuseum	Naturmuseum	k1gInSc1	Naturmuseum
Senckenberg	Senckenberg	k1gInSc1	Senckenberg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
expozic	expozice	k1gFnPc2	expozice
mají	mít	k5eAaImIp3nP	mít
spojená	spojený	k2eAgNnPc4d1	spojené
Muzea	muzeum	k1gNnPc4	muzeum
techniky	technika	k1gFnSc2	technika
(	(	kIx(	(
<g/>
Technikmuseen	Technikmuseen	k1gInSc1	Technikmuseen
<g/>
)	)	kIx)	)
a	a	k8xC	a
Muzeum	muzeum	k1gNnSc1	muzeum
komunikací	komunikace	k1gFnPc2	komunikace
(	(	kIx(	(
<g/>
das	das	k?	das
Kommunikationsmuseum	Kommunikationsmuseum	k1gInSc1	Kommunikationsmuseum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
výrazným	výrazný	k2eAgFnPc3d1	výrazná
muzejním	muzejní	k2eAgFnPc3d1	muzejní
budovám	budova	k1gFnPc3	budova
patří	patřit	k5eAaImIp3nS	patřit
Muzeum	muzeum	k1gNnSc1	muzeum
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
das	das	k?	das
Museum	museum	k1gNnSc1	museum
für	für	k?	für
moderne	modernout	k5eAaPmIp3nS	modernout
Kunst	Kunst	k1gFnSc1	Kunst
<g/>
)	)	kIx)	)
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1989-1991	[number]	k4	1989-1991
od	od	k7c2	od
vídeňského	vídeňský	k2eAgMnSc2d1	vídeňský
architekta	architekt	k1gMnSc2	architekt
Hanse	Hans	k1gMnSc2	Hans
Holleina	Hollein	k1gMnSc2	Hollein
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
trojbokým	trojboký	k2eAgInSc7d1	trojboký
tvarem	tvar	k1gInSc7	tvar
připomíná	připomínat	k5eAaImIp3nS	připomínat
díl	díl	k1gInSc1	díl
dortu	dort	k1gInSc6	dort
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
stojí	stát	k5eAaImIp3nS	stát
blízko	blízko	k7c2	blízko
frankfurtského	frankfurtský	k2eAgInSc2d1	frankfurtský
dómu	dóm	k1gInSc2	dóm
(	(	kIx(	(
<g/>
Kaiserdom	Kaiserdom	k1gInSc1	Kaiserdom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
vystaven	vystavit	k5eAaPmNgInS	vystavit
Dómský	dómský	k2eAgInSc1d1	dómský
poklad	poklad	k1gInSc1	poklad
(	(	kIx(	(
<g/>
Domschatz	Domschatz	k1gInSc1	Domschatz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nedaleko	daleko	k6eNd1	daleko
odtud	odtud	k6eAd1	odtud
na	na	k7c6	na
základech	základ	k1gInPc6	základ
antické	antický	k2eAgFnSc2d1	antická
římské	římský	k2eAgFnSc2d1	římská
stavby	stavba	k1gFnSc2	stavba
stojí	stát	k5eAaImIp3nS	stát
Historické	historický	k2eAgNnSc1d1	historické
muzeum	muzeum	k1gNnSc1	muzeum
města	město	k1gNnSc2	město
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
(	(	kIx(	(
<g/>
Das	Das	k1gFnSc1	Das
Historisches	Historisches	k1gInSc1	Historisches
Museum	museum	k1gNnSc1	museum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgNnSc1d1	moderní
umění	umění	k1gNnSc1	umění
je	být	k5eAaImIp3nS	být
vystaveno	vystaven	k2eAgNnSc1d1	vystaveno
také	také	k9	také
v	v	k7c6	v
Kunsthalle	Kunsthalla	k1gFnSc6	Kunsthalla
<g/>
.	.	kIx.	.
</s>
<s>
Příměstskou	příměstský	k2eAgFnSc7d1	příměstská
rychlodráhou	rychlodráha	k1gFnSc7	rychlodráha
lze	lze	k6eAd1	lze
za	za	k7c4	za
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
dojet	dojet	k5eAaPmF	dojet
do	do	k7c2	do
Německého	německý	k2eAgNnSc2d1	německé
muzea	muzeum	k1gNnSc2	muzeum
kůže	kůže	k1gFnSc2	kůže
a	a	k8xC	a
obuvi	obuv	k1gFnSc2	obuv
(	(	kIx(	(
<g/>
Das	Das	k1gFnSc1	Das
Deutsches	Deutsches	k1gMnSc1	Deutsches
Ledermuseum	Ledermuseum	k1gInSc1	Ledermuseum
<g/>
)	)	kIx)	)
v	v	k7c6	v
Offenbachu	Offenbach	k1gInSc6	Offenbach
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přes	přes	k7c4	přes
2000	[number]	k4	2000
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
pracují	pracovat	k5eAaImIp3nP	pracovat
v	v	k7c6	v
komunikačním	komunikační	k2eAgInSc6d1	komunikační
průmyslu	průmysl	k1gInSc6	průmysl
-	-	kIx~	-
od	od	k7c2	od
klasických	klasický	k2eAgFnPc2d1	klasická
reklamních	reklamní	k2eAgFnPc2d1	reklamní
agentur	agentura	k1gFnPc2	agentura
po	po	k7c4	po
velké	velký	k2eAgInPc4d1	velký
mediální	mediální	k2eAgInPc4d1	mediální
koncerny	koncern	k1gInPc4	koncern
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
působí	působit	k5eAaImIp3nS	působit
až	až	k9	až
9	[number]	k4	9
denních	denní	k2eAgFnPc2d1	denní
tiskovin	tiskovina	k1gFnPc2	tiskovina
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
center	centrum	k1gNnPc2	centrum
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
novin	novina	k1gFnPc2	novina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
nejznámější	známý	k2eAgInPc4d3	nejznámější
je	být	k5eAaImIp3nS	být
Frankfurter	Frankfurter	k1gInSc4	Frankfurter
Allgemeine	Allgemein	k1gInSc5	Allgemein
Zeitung	Zeitung	k1gMnSc1	Zeitung
a	a	k8xC	a
Frankfurter	Frankfurter	k1gMnSc1	Frankfurter
Rundschau	Rundschaus	k1gInSc2	Rundschaus
<g/>
,	,	kIx,	,
necelých	celý	k2eNgNnPc2d1	necelé
200	[number]	k4	200
vydavatelství	vydavatelství	k1gNnPc2	vydavatelství
a	a	k8xC	a
nakladatelství	nakladatelství	k1gNnPc2	nakladatelství
<g/>
,	,	kIx,	,
cca	cca	kA	cca
80	[number]	k4	80
filmových	filmový	k2eAgFnPc2d1	filmová
produkcí	produkce	k1gFnPc2	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
televizí	televize	k1gFnSc7	televize
je	být	k5eAaImIp3nS	být
Hessische	Hessische	k1gFnSc1	Hessische
Rundfunk	Rundfunk	k1gInSc1	Rundfunk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
programové	programový	k2eAgFnSc6d1	programová
nabídce	nabídka	k1gFnSc6	nabídka
televizních	televizní	k2eAgFnPc2d1	televizní
stanic	stanice	k1gFnPc2	stanice
ARD	ARD	kA	ARD
-	-	kIx~	-
das	das	k?	das
Erste	Erst	k1gMnSc5	Erst
<g/>
,	,	kIx,	,
ARTE	ARTE	kA	ARTE
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
Sat	Sat	k1gFnPc2	Sat
a	a	k8xC	a
Phönix	Phönix	k1gInSc1	Phönix
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc1	sídlo
zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
tisková	tiskový	k2eAgFnSc1d1	tisková
a	a	k8xC	a
informační	informační	k2eAgFnSc1d1	informační
agentura	agentura	k1gFnSc1	agentura
Reuters	Reutersa	k1gFnPc2	Reutersa
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
kolem	kolem	k7c2	kolem
10	[number]	k4	10
programových	programový	k2eAgFnPc2d1	programová
regionálních	regionální	k2eAgFnPc2d1	regionální
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
nese	nést	k5eAaImIp3nS	nést
jméno	jméno	k1gNnSc4	jméno
Johanna	Johann	k1gMnSc2	Johann
Wolfganga	Wolfgang	k1gMnSc2	Wolfgang
von	von	k1gInSc4	von
Goethe	Goethe	k1gNnSc2	Goethe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
16	[number]	k4	16
fakult	fakulta	k1gFnPc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Holý	Holý	k1gMnSc1	Holý
(	(	kIx(	(
<g/>
823	[number]	k4	823
<g/>
–	–	k?	–
<g/>
877	[number]	k4	877
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
západofranský	západofranský	k2eAgMnSc1d1	západofranský
král	král	k1gMnSc1	král
Maria	Mario	k1gMnSc2	Mario
Sibylla	Sibylla	k1gMnSc1	Sibylla
Merianová	Merianová	k1gFnSc1	Merianová
(	(	kIx(	(
<g/>
1647	[number]	k4	1647
<g/>
–	–	k?	–
<g/>
1717	[number]	k4	1717
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malířka	malířka	k1gFnSc1	malířka
a	a	k8xC	a
přírodovědkyně	přírodovědkyně	k1gFnSc1	přírodovědkyně
Mayer	Mayer	k1gMnSc1	Mayer
Amschel	Amschel	k1gMnSc1	Amschel
Rothschild	Rothschild	k1gMnSc1	Rothschild
(	(	kIx(	(
<g/>
1744	[number]	k4	1744
<g/>
–	–	k?	–
<g/>
1812	[number]	k4	1812
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bankéř	bankéř	k1gMnSc1	bankéř
Johann	Johann	k1gMnSc1	Johann
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
von	von	k1gInSc4	von
Goethe	Goeth	k1gFnSc2	Goeth
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1749	[number]	k4	1749
<g/>
–	–	k?	–
<g/>
1832	[number]	k4	1832
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
prozaik	prozaik	k1gMnSc1	prozaik
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
Friedrich	Friedrich	k1gMnSc1	Friedrich
Maximilian	Maximilian	k1gMnSc1	Maximilian
Klinger	Klinger	k1gMnSc1	Klinger
(	(	kIx(	(
<g/>
1752	[number]	k4	1752
<g/>
–	–	k?	–
<g/>
1831	[number]	k4	1831
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
Amschel	Amschel	k1gMnSc1	Amschel
Mayer	Mayer	k1gMnSc1	Mayer
Rothschild	Rothschild	k1gMnSc1	Rothschild
(	(	kIx(	(
<g/>
1773	[number]	k4	1773
<g/>
–	–	k?	–
<g/>
1855	[number]	k4	1855
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bankéř	bankéř	k1gMnSc1	bankéř
Friedrich	Friedrich	k1gMnSc1	Friedrich
Carl	Carl	k1gMnSc1	Carl
von	von	k1gInSc1	von
<g />
.	.	kIx.	.
</s>
<s>
Savigny	Savign	k1gInPc1	Savign
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1779	[number]	k4	1779
<g/>
–	–	k?	–
<g/>
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
Bettina	Bettina	k1gFnSc1	Bettina
von	von	k1gInSc4	von
Arnim	Arnim	k1gInSc1	Arnim
(	(	kIx(	(
<g/>
1785	[number]	k4	1785
<g/>
–	–	k?	–
<g/>
1859	[number]	k4	1859
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Henri	Henr	k1gFnSc2	Henr
Nestlé	Nestlý	k2eAgFnSc2d1	Nestlý
(	(	kIx(	(
<g/>
1814	[number]	k4	1814
<g/>
–	–	k?	–
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
podnikatel	podnikatel	k1gMnSc1	podnikatel
Karl	Karl	k1gMnSc1	Karl
Schwarzschild	Schwarzschild	k1gMnSc1	Schwarzschild
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g />
.	.	kIx.	.
</s>
<s>
<g/>
–	–	k?	–
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
a	a	k8xC	a
fyzik	fyzik	k1gMnSc1	fyzik
Otto	Otto	k1gMnSc1	Otto
Loewi	Loewe	k1gFnSc4	Loewe
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
–	–	k?	–
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
farmakolog	farmakolog	k1gMnSc1	farmakolog
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
medicínu	medicína	k1gFnSc4	medicína
Otto	Otto	k1gMnSc1	Otto
Hahn	Hahn	k1gMnSc1	Hahn
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
–	–	k?	–
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
<g />
.	.	kIx.	.
</s>
<s>
ceny	cena	k1gFnPc1	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
Hans	Hans	k1gMnSc1	Hans
Fischer	Fischer	k1gMnSc1	Fischer
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
Hermann	Hermann	k1gMnSc1	Hermann
Abendroth	Abendroth	k1gMnSc1	Abendroth
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
–	–	k?	–
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dirigent	dirigent	k1gMnSc1	dirigent
Walter	Walter	k1gMnSc1	Walter
Ruttmann	Ruttmann	k1gMnSc1	Ruttmann
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
Ernst	Ernst	k1gMnSc1	Ernst
Udet	Udet	k1gMnSc1	Udet
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stíhací	stíhací	k2eAgMnSc1d1	stíhací
pilot	pilot	k1gMnSc1	pilot
v	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
Willy	Willa	k1gFnSc2	Willa
Messerschmitt	Messerschmitt	k1gInSc1	Messerschmitt
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
–	–	k?	–
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
letecký	letecký	k2eAgMnSc1d1	letecký
konstruktér	konstruktér	k1gMnSc1	konstruktér
a	a	k8xC	a
podnikatel	podnikatel	k1gMnSc1	podnikatel
Erich	Erich	k1gMnSc1	Erich
Fromm	Fromm	k1gMnSc1	Fromm
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
–	–	k?	–
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
psychoanalytik	psychoanalytik	k1gMnSc1	psychoanalytik
<g/>
,	,	kIx,	,
sociolog	sociolog	k1gMnSc1	sociolog
a	a	k8xC	a
filozof	filozof	k1gMnSc1	filozof
Erik	Erik	k1gMnSc1	Erik
Erikson	Erikson	k1gMnSc1	Erikson
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
psycholog	psycholog	k1gMnSc1	psycholog
a	a	k8xC	a
psychoanalytik	psychoanalytik	k1gMnSc1	psychoanalytik
Theodor	Theodora	k1gFnPc2	Theodora
W.	W.	kA	W.
Adorno	Adorno	k1gNnSc4	Adorno
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
sociolog	sociolog	k1gMnSc1	sociolog
a	a	k8xC	a
muzikolog	muzikolog	k1gMnSc1	muzikolog
Ernst	Ernst	k1gMnSc1	Ernst
vom	vom	k?	vom
Rath	Rath	k1gMnSc1	Rath
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g />
.	.	kIx.	.
</s>
<s>
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
diplomat	diplomat	k1gMnSc1	diplomat
Herbert	Herbert	k1gMnSc1	Herbert
Freudenberger	Freudenberger	k1gMnSc1	Freudenberger
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
psychoanalytik	psychoanalytik	k1gMnSc1	psychoanalytik
Anne	Ann	k1gFnSc2	Ann
Franková	Franková	k1gFnSc1	Franková
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
židovská	židovský	k2eAgFnSc1d1	židovská
oběť	oběť	k1gFnSc4	oběť
nacismu	nacismus	k1gInSc2	nacismus
Robert	Robert	k1gMnSc1	Robert
Aumann	Aumann	k1gMnSc1	Aumann
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
1930	[number]	k4	1930
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
izraelský	izraelský	k2eAgMnSc1d1	izraelský
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
ekonomii	ekonomie	k1gFnSc4	ekonomie
Gerd	Gerd	k1gMnSc1	Gerd
Binnig	Binnig	k1gMnSc1	Binnig
(	(	kIx(	(
<g/>
*	*	kIx~	*
20	[number]	k4	20
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
Ulrike	Ulrike	k1gNnSc2	Ulrike
Meyfarthová	Meyfarthová	k1gFnSc1	Meyfarthová
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bývalá	bývalý	k2eAgFnSc1d1	bývalá
výškařka	výškařka	k1gFnSc1	výškařka
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
dvojnásobní	dvojnásobný	k2eAgMnPc1d1	dvojnásobný
olympijská	olympijský	k2eAgFnSc1d1	olympijská
vítězka	vítězka	k1gFnSc1	vítězka
Hans	Hans	k1gMnSc1	Hans
Zimmer	Zimmer	k1gMnSc1	Zimmer
(	(	kIx(	(
<g/>
*	*	kIx~	*
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
filmové	filmový	k2eAgFnSc2d1	filmová
hudby	hudba	k1gFnSc2	hudba
Thomas	Thomas	k1gMnSc1	Thomas
Reiter	Reiter	k1gMnSc1	Reiter
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
astronaut	astronaut	k1gMnSc1	astronaut
Hannes	Hannes	k1gMnSc1	Hannes
Jaenicke	Jaenicke	k1gFnSc1	Jaenicke
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
únor	únor	k1gInSc1	únor
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Martin	Martin	k1gMnSc1	Martin
Lawrence	Lawrence	k1gFnSc1	Lawrence
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
Johannes	Johannes	k1gMnSc1	Johannes
Brandrup	Brandrup	k1gMnSc1	Brandrup
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Tré	tré	k1gNnSc1	tré
Cool	Cool	k1gMnSc1	Cool
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
Alexander	Alexandra	k1gFnPc2	Alexandra
Waske	Waske	k1gFnPc2	Waske
(	(	kIx(	(
<g/>
*	*	kIx~	*
31	[number]	k4	31
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenista	tenista	k1gMnSc1	tenista
Karel	Karel	k1gMnSc1	Karel
Veliký	veliký	k2eAgMnSc1d1	veliký
(	(	kIx(	(
<g/>
748	[number]	k4	748
<g/>
–	–	k?	–
<g/>
814	[number]	k4	814
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
franský	franský	k2eAgMnSc1d1	franský
král	král	k1gMnSc1	král
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
Ludvík	Ludvík	k1gMnSc1	Ludvík
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Němec	Němec	k1gMnSc1	Němec
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
805	[number]	k4	805
–	–	k?	–
876	[number]	k4	876
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bavorský	bavorský	k2eAgMnSc1d1	bavorský
a	a	k8xC	a
východofranský	východofranský	k2eAgMnSc1d1	východofranský
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
835	[number]	k4	835
–	–	k?	–
882	[number]	k4	882
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
východofranský	východofranský	k2eAgMnSc1d1	východofranský
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Němce	Němec	k1gMnSc2	Němec
Giordano	Giordana	k1gFnSc5	Giordana
Bruno	Bruno	k1gMnSc1	Bruno
(	(	kIx(	(
<g/>
1548	[number]	k4	1548
<g/>
–	–	k?	–
<g/>
1600	[number]	k4	1600
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
italský	italský	k2eAgMnSc1d1	italský
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
a	a	k8xC	a
dominikán	dominikán	k1gMnSc1	dominikán
Matthäus	Matthäus	k1gMnSc1	Matthäus
Merian	Merian	k1gMnSc1	Merian
(	(	kIx(	(
<g/>
1593	[number]	k4	1593
<g/>
–	–	k?	–
<g/>
1650	[number]	k4	1650
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
rytec	rytec	k1gMnSc1	rytec
a	a	k8xC	a
nakladatel	nakladatel	k1gMnSc1	nakladatel
Georg	Georg	k1gMnSc1	Georg
Philipp	Philipp	k1gMnSc1	Philipp
Telemann	Telemann	k1gMnSc1	Telemann
(	(	kIx(	(
<g/>
1681	[number]	k4	1681
<g/>
–	–	k?	–
<g/>
1767	[number]	k4	1767
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
skladatel	skladatel	k1gMnSc1	skladatel
Clemens	Clemensa	k1gFnPc2	Clemensa
Brentano	Brentana	k1gFnSc5	Brentana
(	(	kIx(	(
<g/>
1778	[number]	k4	1778
<g/>
–	–	k?	–
<g/>
1842	[number]	k4	1842
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
prozaik	prozaik	k1gMnSc1	prozaik
Arthur	Arthur	k1gMnSc1	Arthur
Schopenhauer	Schopenhauer	k1gMnSc1	Schopenhauer
(	(	kIx(	(
<g/>
1788	[number]	k4	1788
<g/>
–	–	k?	–
<g/>
1860	[number]	k4	1860
<g/>
)	)	kIx)	)
německý	německý	k2eAgInSc1d1	německý
<g />
.	.	kIx.	.
</s>
<s>
filozof	filozof	k1gMnSc1	filozof
Paul	Paul	k1gMnSc1	Paul
Ehrlich	Ehrlich	k1gMnSc1	Ehrlich
(	(	kIx(	(
<g/>
1854	[number]	k4	1854
<g/>
–	–	k?	–
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
sérolog	sérolog	k1gMnSc1	sérolog
a	a	k8xC	a
imunolog	imunolog	k1gMnSc1	imunolog
Engelbert	Engelbert	k1gMnSc1	Engelbert
Humperdinck	Humperdinck	k1gMnSc1	Humperdinck
(	(	kIx(	(
<g/>
1854	[number]	k4	1854
<g/>
–	–	k?	–
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
skladatel	skladatel	k1gMnSc1	skladatel
Max	Max	k1gMnSc1	Max
Beckmann	Beckmann	k1gMnSc1	Beckmann
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
–	–	k?	–
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
a	a	k8xC	a
sochař	sochař	k1gMnSc1	sochař
Max	Max	k1gMnSc1	Max
Horkheimer	Horkheimer	k1gMnSc1	Horkheimer
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
–	–	k?	–
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
filozof	filozof	k1gMnSc1	filozof
a	a	k8xC	a
sociolog	sociolog	k1gMnSc1	sociolog
Paul	Paul	k1gMnSc1	Paul
Hindemith	Hindemith	k1gMnSc1	Hindemith
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
–	–	k?	–
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
violista	violista	k1gMnSc1	violista
<g/>
,	,	kIx,	,
dirigent	dirigent	k1gMnSc1	dirigent
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
Margarete	Margare	k1gNnSc2	Margare
Schütte-Lihotzky	Schütte-Lihotzka	k1gFnSc2	Schütte-Lihotzka
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1897	[number]	k4	1897
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakouská	rakouský	k2eAgFnSc1d1	rakouská
architektka	architektka	k1gFnSc1	architektka
Ludwig	Ludwig	k1gMnSc1	Ludwig
Erhard	Erhard	k1gMnSc1	Erhard
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
–	–	k?	–
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
ekonom	ekonom	k1gMnSc1	ekonom
a	a	k8xC	a
spolkový	spolkový	k2eAgMnSc1d1	spolkový
kancléř	kancléř	k1gMnSc1	kancléř
Hans	Hans	k1gMnSc1	Hans
Bethe	Bethe	k1gFnSc1	Bethe
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
fyzik	fyzik	k1gMnSc1	fyzik
německého	německý	k2eAgInSc2d1	německý
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
<g />
.	.	kIx.	.
</s>
<s>
ceny	cena	k1gFnPc1	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
Oskar	Oskar	k1gMnSc1	Oskar
Schindler	Schindler	k1gMnSc1	Schindler
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
–	–	k?	–
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
,	,	kIx,	,
záchrance	záchranka	k1gFnSc6	záchranka
Židů	Žid	k1gMnPc2	Žid
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
Marcel	Marcel	k1gMnSc1	Marcel
Reich-Ranicki	Reich-Ranick	k1gFnSc2	Reich-Ranick
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
publicista	publicista	k1gMnSc1	publicista
a	a	k8xC	a
literární	literární	k2eAgMnSc1d1	literární
kritik	kritik	k1gMnSc1	kritik
Karl	Karl	k1gMnSc1	Karl
Dedecius	Dedecius	k1gMnSc1	Dedecius
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
polské	polský	k2eAgFnSc2d1	polská
literatury	literatura	k1gFnSc2	literatura
Jürgen	Jürgen	k1gInSc1	Jürgen
Habermas	Habermas	k1gMnSc1	Habermas
(	(	kIx(	(
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
sociolog	sociolog	k1gMnSc1	sociolog
a	a	k8xC	a
filozof	filozof	k1gMnSc1	filozof
Helmut	Helmut	k1gMnSc1	Helmut
Kohl	Kohl	k1gMnSc1	Kohl
(	(	kIx(	(
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
kancléř	kancléř	k1gMnSc1	kancléř
Joschka	Joschka	k1gMnSc1	Joschka
Fischer	Fischer	k1gMnSc1	Fischer
(	(	kIx(	(
<g/>
*	*	kIx~	*
12	[number]	k4	12
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
politik	politik	k1gMnSc1	politik
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
ročně	ročně	k6eAd1	ročně
navštíví	navštívit	k5eAaPmIp3nS	navštívit
přes	přes	k7c4	přes
2,3	[number]	k4	2,3
miliónů	milión	k4xCgInPc2	milión
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
polovinu	polovina	k1gFnSc4	polovina
tvoří	tvořit	k5eAaImIp3nP	tvořit
cizinci	cizinec	k1gMnPc1	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
doba	doba	k1gFnSc1	doba
návštěvy	návštěva	k1gFnSc2	návštěva
je	být	k5eAaImIp3nS	být
1,7	[number]	k4	1,7
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
číslo	číslo	k1gNnSc1	číslo
tvoří	tvořit	k5eAaImIp3nS	tvořit
především	především	k9	především
služební	služební	k2eAgFnSc2d1	služební
cesty	cesta	k1gFnSc2	cesta
obchodníků	obchodník	k1gMnPc2	obchodník
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přes	přes	k7c4	přes
180	[number]	k4	180
hotelů	hotel	k1gInPc2	hotel
a	a	k8xC	a
pensionů	pension	k1gInPc2	pension
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
11	[number]	k4	11
luxusních	luxusní	k2eAgInPc2d1	luxusní
hotelů	hotel	k1gInPc2	hotel
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
First-Class-Hotels	First-Class-Hotels	k1gInSc1	First-Class-Hotels
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
sídlí	sídlet	k5eAaImIp3nS	sídlet
přes	přes	k7c4	přes
80	[number]	k4	80
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
konzulátů	konzulát	k1gInPc2	konzulát
a	a	k8xC	a
úřadů	úřad	k1gInPc2	úřad
ze	z	k7c2	z
60	[number]	k4	60
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgNnPc4d3	veliký
veletržní	veletržní	k2eAgNnPc4d1	veletržní
a	a	k8xC	a
výstavní	výstavní	k2eAgNnPc4d1	výstavní
centra	centrum	k1gNnPc4	centrum
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
co	co	k3yInSc1	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
návštěvníků	návštěvník	k1gMnPc2	návštěvník
a	a	k8xC	a
vystavovatelů	vystavovatel	k1gMnPc2	vystavovatel
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
pořádají	pořádat	k5eAaImIp3nP	pořádat
Frankfurtské	frankfurtský	k2eAgInPc1d1	frankfurtský
veletrhy	veletrh	k1gInPc1	veletrh
přes	přes	k7c4	přes
300	[number]	k4	300
veletrhů	veletrh	k1gInPc2	veletrh
a	a	k8xC	a
výstav	výstava	k1gFnPc2	výstava
doma	doma	k6eAd1	doma
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
v	v	k7c6	v
samotném	samotný	k2eAgInSc6d1	samotný
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
cca	cca	kA	cca
40	[number]	k4	40
výstav	výstava	k1gFnPc2	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
a	a	k8xC	a
nejprestižnějším	prestižní	k2eAgInSc7d3	nejprestižnější
veletrhem	veletrh	k1gInSc7	veletrh
je	být	k5eAaImIp3nS	být
IAA	IAA	kA	IAA
-	-	kIx~	-
Internationale	Internationale	k1gMnSc1	Internationale
Automobilausstellung	Automobilausstellung	k1gMnSc1	Automobilausstellung
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
navštíví	navštívit	k5eAaPmIp3nS	navštívit
přes	přes	k7c4	přes
2	[number]	k4	2
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
,	,	kIx,	,
Buchmesse	Buchmess	k1gMnSc2	Buchmess
knižní	knižní	k2eAgInSc4d1	knižní
veletrh	veletrh	k1gInSc4	veletrh
<g/>
,	,	kIx,	,
Internationale	Internationale	k1gFnSc4	Internationale
Frankfurter	Frankfurter	k1gInSc4	Frankfurter
Messe	Messe	k1gFnSc2	Messe
Ambiente	Ambient	k1gInSc5	Ambient
či	či	k8xC	či
Internationale	Internationale	k1gFnPc6	Internationale
Frankfurter	Frankfurtra	k1gFnPc2	Frankfurtra
Messe	Messe	k1gFnSc1	Messe
Tendence	tendence	k1gFnSc1	tendence
a	a	k8xC	a
hudební	hudební	k2eAgInSc1d1	hudební
veletrh	veletrh	k1gInSc1	veletrh
Pro	pro	k7c4	pro
Light	Light	k2eAgInSc4d1	Light
&	&	k?	&
Sound	Sound	k1gInSc4	Sound
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
kongresovému	kongresový	k2eAgNnSc3d1	Kongresové
centru	centrum	k1gNnSc3	centrum
Congress	Congressa	k1gFnPc2	Congressa
Center	centrum	k1gNnPc2	centrum
Frankfurtského	frankfurtský	k2eAgInSc2d1	frankfurtský
veletrhu	veletrh	k1gInSc2	veletrh
se	se	k3xPyFc4	se
tak	tak	k9	tak
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
stává	stávat	k5eAaImIp3nS	stávat
nejatraktivnějším	atraktivní	k2eAgNnSc7d3	nejatraktivnější
kongresovým	kongresový	k2eAgNnSc7d1	Kongresové
centrem	centrum	k1gNnSc7	centrum
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
a	a	k8xC	a
nejfrekventovanější	frekventovaný	k2eAgFnSc7d3	nejfrekventovanější
ulicí	ulice	k1gFnSc7	ulice
je	být	k5eAaImIp3nS	být
Zeil	Zeil	k1gInSc1	Zeil
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgInSc1d3	nejstarší
Hauptwache	Hauptwache	k1gInSc1	Hauptwache
a	a	k8xC	a
Konstablerwache	Konstablerwache	k1gInSc1	Konstablerwache
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc4	každý
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
a	a	k8xC	a
sobotu	sobota	k1gFnSc4	sobota
konají	konat	k5eAaImIp3nP	konat
místní	místní	k2eAgInPc1d1	místní
zelinářské	zelinářský	k2eAgInPc1d1	zelinářský
trhy	trh	k1gInPc1	trh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
velký	velký	k2eAgInSc1d1	velký
trh	trh	k1gInSc1	trh
zeleniny	zelenina	k1gFnSc2	zelenina
a	a	k8xC	a
sýrů	sýr	k1gInPc2	sýr
na	na	k7c6	na
Schillerstrasse	Schillerstrassa	k1gFnSc6	Schillerstrassa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
na	na	k7c4	na
Mainufer	Mainufer	k1gInSc4	Mainufer
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
od	od	k7c2	od
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
do	do	k7c2	do
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
bleší	bleší	k2eAgInPc1d1	bleší
trhy	trh	k1gInPc1	trh
<g/>
.	.	kIx.	.
</s>
<s>
Směsici	směsice	k1gFnSc4	směsice
malých	malý	k2eAgInPc2d1	malý
specializovaných	specializovaný	k2eAgInPc2d1	specializovaný
obchůdků	obchůdek	k1gInPc2	obchůdek
a	a	k8xC	a
butiků	butik	k1gInPc2	butik
nabízí	nabízet	k5eAaImIp3nS	nabízet
okolní	okolní	k2eAgFnPc4d1	okolní
uličky	ulička	k1gFnPc4	ulička
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
náměstí	náměstí	k1gNnSc2	náměstí
Schweizer	Schweizra	k1gFnPc2	Schweizra
Platz	Platza	k1gFnPc2	Platza
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Sachsenhausen	Sachsenhausna	k1gFnPc2	Sachsenhausna
a	a	k8xC	a
Berger	Berger	k1gMnSc1	Berger
Strasse	Strasse	k1gFnSc2	Strasse
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Bornheim	Bornheima	k1gFnPc2	Bornheima
<g/>
.	.	kIx.	.
</s>
<s>
Nejluxusnější	luxusní	k2eAgFnSc7d3	nejluxusnější
nákupní	nákupní	k2eAgFnSc7d1	nákupní
ulicí	ulice	k1gFnSc7	ulice
je	být	k5eAaImIp3nS	být
Goethestrasse	Goethestrasse	k1gFnSc1	Goethestrasse
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
jsou	být	k5eAaImIp3nP	být
exklusivním	exklusivní	k2eAgInSc7d1	exklusivní
sortimentem	sortiment	k1gInSc7	sortiment
zastoupení	zastoupení	k1gNnSc2	zastoupení
všichni	všechen	k3xTgMnPc1	všechen
nejvýznamnější	významný	k2eAgMnPc1d3	nejvýznamnější
světoví	světový	k2eAgMnPc1d1	světový
designéři	designér	k1gMnPc1	designér
<g/>
.	.	kIx.	.
</s>
<s>
Symbolem	symbol	k1gInSc7	symbol
Hesenska	Hesensko	k1gNnSc2	Hesensko
je	být	k5eAaImIp3nS	být
jablko	jablko	k1gNnSc4	jablko
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgMnPc1d1	místní
pijí	pít	k5eAaImIp3nP	pít
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
jabčák	jabčák	k1gInSc4	jabčák
(	(	kIx(	(
<g/>
nářečím	nářečí	k1gNnSc7	nářečí
zvaný	zvaný	k2eAgInSc1d1	zvaný
Ebblwoi	Ebblwoi	k1gNnSc4	Ebblwoi
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kolem	kolem	k7c2	kolem
5	[number]	k4	5
%	%	kIx~	%
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc1	dítě
pijí	pít	k5eAaImIp3nP	pít
jablečný	jablečný	k2eAgInSc4d1	jablečný
střik	střik	k1gInSc4	střik
Apfelschorle	Apfelschorle	k1gFnSc2	Apfelschorle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
typických	typický	k2eAgFnPc6d1	typická
frankfurtských	frankfurtský	k2eAgFnPc6d1	Frankfurtská
restauracích	restaurace	k1gFnPc6	restaurace
si	se	k3xPyFc3	se
můžete	moct	k5eAaImIp2nP	moct
objednat	objednat	k5eAaPmF	objednat
Frankfurter	Frankfurter	k1gInSc4	Frankfurter
Grüne	Grün	k1gMnSc5	Grün
Sauce	Sauce	k1gMnSc5	Sauce
(	(	kIx(	(
<g/>
brambory	brambora	k1gFnPc1	brambora
s	s	k7c7	s
pikantní	pikantní	k2eAgFnSc7d1	pikantní
omáčkou	omáčka	k1gFnSc7	omáčka
z	z	k7c2	z
drcených	drcený	k2eAgFnPc2d1	drcená
bylinek	bylinka	k1gFnPc2	bylinka
a	a	k8xC	a
s	s	k7c7	s
vařeným	vařený	k2eAgNnSc7d1	vařené
vejcem	vejce	k1gNnSc7	vejce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ochutnat	ochutnat	k5eAaPmF	ochutnat
Handkäs	Handkäs	k1gInSc4	Handkäs
mit	mit	k?	mit
Musik	musika	k1gFnPc2	musika
(	(	kIx(	(
<g/>
tvarůžky	tvarůžek	k1gInPc7	tvarůžek
na	na	k7c6	na
octě	ocet	k1gInSc6	ocet
s	s	k7c7	s
kmínem	kmín	k1gInSc7	kmín
a	a	k8xC	a
chlebem	chléb	k1gInSc7	chléb
<g/>
)	)	kIx)	)
či	či	k8xC	či
známé	známý	k2eAgInPc1d1	známý
frankfurtské	frankfurtský	k2eAgInPc1d1	frankfurtský
klobásky	klobásek	k1gInPc1	klobásek
Frankfurter	Frankfurtra	k1gFnPc2	Frankfurtra
Würstchen	Würstchna	k1gFnPc2	Würstchna
<g/>
.	.	kIx.	.
</s>
<s>
Multikulturní	multikulturní	k2eAgInSc1d1	multikulturní
charakter	charakter	k1gInSc1	charakter
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
nabízí	nabízet	k5eAaImIp3nS	nabízet
bohatou	bohatý	k2eAgFnSc4d1	bohatá
paletu	paleta	k1gFnSc4	paleta
kafeterií	kafeterie	k1gFnPc2	kafeterie
<g/>
,	,	kIx,	,
barů	bar	k1gInPc2	bar
či	či	k8xC	či
restaurací	restaurace	k1gFnPc2	restaurace
s	s	k7c7	s
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
kuchyní	kuchyně	k1gFnSc7	kuchyně
(	(	kIx(	(
<g/>
až	až	k9	až
2.500	[number]	k4	2.500
gastronomických	gastronomický	k2eAgNnPc2d1	gastronomické
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejznámější	známý	k2eAgInPc4d3	nejznámější
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
Café	café	k1gNnSc1	café
Hauptwache	Hauptwache	k1gNnSc2	Hauptwache
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
původních	původní	k2eAgFnPc6d1	původní
prostorách	prostora	k1gFnPc6	prostora
Frankfurtská	frankfurtský	k2eAgFnSc1d1	Frankfurtská
věznice	věznice	k1gFnSc1	věznice
či	či	k8xC	či
pojízdná	pojízdný	k2eAgFnSc1d1	pojízdná
tramvaj	tramvaj	k1gFnSc1	tramvaj
Ebbelwoiexpress	Ebbelwoiexpressa	k1gFnPc2	Ebbelwoiexpressa
<g/>
.	.	kIx.	.
</s>
<s>
Mainfest	Mainfest	k1gFnSc1	Mainfest
–	–	k?	–
Mohanská	mohanský	k2eAgFnSc1d1	Mohanská
slavnost	slavnost	k1gFnSc1	slavnost
je	být	k5eAaImIp3nS	být
každoročně	každoročně	k6eAd1	každoročně
pořádána	pořádat	k5eAaImNgFnS	pořádat
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
oslavu	oslava	k1gFnSc4	oslava
řeky	řeka	k1gFnSc2	řeka
Mohanu	Mohan	k1gInSc2	Mohan
podél	podél	k7c2	podél
jejího	její	k3xOp3gNnSc2	její
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Mainfest	Mainfest	k1gInSc1	Mainfest
nabízí	nabízet	k5eAaImIp3nS	nabízet
kulturní	kulturní	k2eAgInSc1d1	kulturní
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
českým	český	k2eAgFnPc3d1	Česká
poutím	pouť	k1gFnPc3	pouť
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
slavnosti	slavnost	k1gFnPc1	slavnost
začínají	začínat	k5eAaImIp3nP	začínat
pravidelně	pravidelně	k6eAd1	pravidelně
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
ukončeny	ukončen	k2eAgInPc1d1	ukončen
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
velkolepým	velkolepý	k2eAgInSc7d1	velkolepý
ohňostrojem	ohňostroj	k1gInSc7	ohňostroj
<g/>
.	.	kIx.	.
</s>
<s>
Museumsuferfest	Museumsuferfest	k1gFnSc1	Museumsuferfest
–	–	k?	–
Muzejní	muzejní	k2eAgFnSc4d1	muzejní
nábřežní	nábřežní	k2eAgFnSc4d1	nábřežní
slavnost	slavnost	k1gFnSc4	slavnost
pořádá	pořádat	k5eAaImIp3nS	pořádat
úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
multikulturní	multikulturní	k2eAgFnPc4d1	multikulturní
záležitosti	záležitost	k1gFnPc4	záležitost
Amt	Amt	k?	Amt
für	für	k?	für
multikulturelle	multikulturelle	k1gInSc1	multikulturelle
Angelegenheiten	Angelegenheiten	k2eAgInSc1d1	Angelegenheiten
pravidelně	pravidelně	k6eAd1	pravidelně
o	o	k7c6	o
víkendu	víkend	k1gInSc6	víkend
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
srpna	srpen	k1gInSc2	srpen
<g/>
/	/	kIx~	/
<g/>
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
slavnosti	slavnost	k1gFnPc1	slavnost
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
přezdívány	přezdíván	k2eAgFnPc1d1	přezdívána
Frankfurter	Frankfurter	k1gInSc4	Frankfurter
Bühne	Bühn	k1gInSc5	Bühn
(	(	kIx(	(
<g/>
Frankfurtské	frankfurtský	k2eAgNnSc4d1	Frankfurtské
pódium	pódium	k1gNnSc4	pódium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc7	jejich
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
bohatá	bohatý	k2eAgFnSc1d1	bohatá
programová	programový	k2eAgFnSc1d1	programová
prezentace	prezentace	k1gFnSc1	prezentace
umělců	umělec	k1gMnPc2	umělec
80	[number]	k4	80
národů	národ	k1gInPc2	národ
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Mohanu	Mohan	k1gInSc2	Mohan
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
společenská	společenský	k2eAgFnSc1d1	společenská
akce	akce	k1gFnSc1	akce
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
ceněna	cenit	k5eAaImNgFnS	cenit
nejen	nejen	k6eAd1	nejen
laickou	laický	k2eAgFnSc4d1	laická
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
odbornou	odborný	k2eAgFnSc7d1	odborná
veřejností	veřejnost	k1gFnSc7	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnPc2	součást
Museumsuferfest	Museumsuferfest	k1gFnSc1	Museumsuferfest
je	být	k5eAaImIp3nS	být
také	také	k9	také
otevření	otevření	k1gNnSc1	otevření
dveří	dveře	k1gFnPc2	dveře
všech	všecek	k3xTgInPc2	všecek
muzeí	muzeum	k1gNnPc2	muzeum
města	město	k1gNnSc2	město
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvníci	návštěvník	k1gMnPc1	návštěvník
mají	mít	k5eAaImIp3nP	mít
možnost	možnost	k1gFnSc4	možnost
za	za	k7c4	za
minimální	minimální	k2eAgInSc4d1	minimální
poplatek	poplatek	k1gInSc4	poplatek
navštívit	navštívit	k5eAaPmF	navštívit
všechna	všechen	k3xTgNnPc4	všechen
muzea	muzeum	k1gNnPc4	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Nacht	Nacht	k1gMnSc1	Nacht
der	drát	k5eAaImRp2nS	drát
Museen	Museen	k1gInSc1	Museen
–	–	k?	–
Noc	noc	k1gFnSc1	noc
muzeí	muzeum	k1gNnPc2	muzeum
je	být	k5eAaImIp3nS	být
akcí	akce	k1gFnSc7	akce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
uskutečňuje	uskutečňovat	k5eAaImIp3nS	uskutečňovat
pravidelně	pravidelně	k6eAd1	pravidelně
o	o	k7c6	o
letních	letní	k2eAgFnPc6d1	letní
prázdninách	prázdniny	k1gFnPc6	prázdniny
<g/>
.	.	kIx.	.
</s>
<s>
Muzea	muzeum	k1gNnSc2	muzeum
a	a	k8xC	a
galerie	galerie	k1gFnSc2	galerie
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
otevřou	otevřít	k5eAaPmIp3nP	otevřít
své	svůj	k3xOyFgFnPc4	svůj
brány	brána	k1gFnPc4	brána
od	od	k7c2	od
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
do	do	k7c2	do
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Frankfurter	Frankfurter	k1gInSc1	Frankfurter
Weihnachtsmarkt	Weihnachtsmarkt	k1gInSc1	Weihnachtsmarkt
<g/>
,	,	kIx,	,
vánoční	vánoční	k2eAgInPc1d1	vánoční
trhy	trh	k1gInPc1	trh
jsou	být	k5eAaImIp3nP	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřeny	otevřen	k2eAgFnPc1d1	otevřena
měsíc	měsíc	k1gInSc4	měsíc
před	před	k7c7	před
Štědrým	štědrý	k2eAgInSc7d1	štědrý
večerem	večer	k1gInSc7	večer
<g/>
,	,	kIx,	,
původním	původní	k2eAgNnSc7d1	původní
místem	místo	k1gNnSc7	místo
konání	konání	k1gNnSc2	konání
byl	být	k5eAaImAgMnS	být
vždy	vždy	k6eAd1	vždy
Römerberg	Römerberg	k1gMnSc1	Römerberg
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
trhy	trh	k1gInPc1	trh
rozprostírají	rozprostírat	k5eAaImIp3nP	rozprostírat
na	na	k7c4	na
následující	následující	k2eAgNnSc4d1	následující
území	území	k1gNnSc4	území
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
<g/>
:	:	kIx,	:
Paulsplatz	Paulsplatz	k1gMnSc1	Paulsplatz
<g/>
,	,	kIx,	,
Liebfrauenberg	Liebfrauenberg	k1gMnSc1	Liebfrauenberg
<g/>
,	,	kIx,	,
Neue	Neuus	k1gMnSc5	Neuus
Kräme	Kräm	k1gMnSc5	Kräm
<g/>
,	,	kIx,	,
Fahrtor	Fahrtor	k1gInSc1	Fahrtor
<g/>
,	,	kIx,	,
Mainkai	Mainkai	k1gNnSc1	Mainkai
<g/>
,	,	kIx,	,
Zeil	Zeil	k1gInSc1	Zeil
a	a	k8xC	a
Konstablerwache	Konstablerwache	k1gInSc1	Konstablerwache
<g/>
.	.	kIx.	.
</s>
<s>
Dippemess	Dippemess	k6eAd1	Dippemess
se	se	k3xPyFc4	se
pořádá	pořádat	k5eAaImIp3nS	pořádat
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Frühjahr	Frühjahr	k1gInSc1	Frühjahr
Dippemess	Dippemess	k1gInSc1	Dippemess
-	-	kIx~	-
duben	duben	k1gInSc1	duben
<g/>
/	/	kIx~	/
<g/>
květen	květen	k1gInSc1	květen
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Herbst	Herbst	k1gInSc1	Herbst
Dippemess	Dippemess	k1gInSc1	Dippemess
-	-	kIx~	-
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
místem	místo	k1gNnSc7	místo
konání	konání	k1gNnSc2	konání
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
Festplatz	Festplatz	k1gInSc1	Festplatz
am	am	k?	am
Ratsweg	Ratsweg	k1gInSc1	Ratsweg
<g/>
.	.	kIx.	.
</s>
<s>
Sommerfest	Sommerfest	k1gMnSc1	Sommerfest
Opernplatz	Opernplatz	k1gMnSc1	Opernplatz
se	se	k3xPyFc4	se
pořádá	pořádat	k5eAaImIp3nS	pořádat
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
června	červen	k1gInSc2	červen
a	a	k8xC	a
července	červenec	k1gInSc2	červenec
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
před	před	k7c7	před
budovou	budova	k1gFnSc7	budova
Alte	alt	k1gInSc5	alt
Oper	opera	k1gFnPc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
této	tento	k3xDgFnSc2	tento
slavnosti	slavnost	k1gFnSc2	slavnost
je	být	k5eAaImIp3nS	být
zapojení	zapojení	k1gNnSc4	zapojení
dospělých	dospělí	k1gMnPc2	dospělí
k	k	k7c3	k
dětským	dětský	k2eAgFnPc3d1	dětská
hrám	hra	k1gFnPc3	hra
<g/>
.	.	kIx.	.
</s>
<s>
Bundesäppelwoifest	Bundesäppelwoifest	k1gFnSc1	Bundesäppelwoifest
je	být	k5eAaImIp3nS	být
národní	národní	k2eAgFnSc7d1	národní
slavností	slavnost	k1gFnSc7	slavnost
jabčáku	jabčák	k1gInSc2	jabčák
<g/>
.	.	kIx.	.
</s>
<s>
Koná	konat	k5eAaImIp3nS	konat
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
poslední	poslední	k2eAgInSc4d1	poslední
srpnový	srpnový	k2eAgInSc4d1	srpnový
víkend	víkend	k1gInSc4	víkend
v	v	k7c6	v
roce	rok	k1gInSc6	rok
okolí	okolí	k1gNnSc2	okolí
města	město	k1gNnSc2	město
Hanau	Hanaus	k1gInSc2	Hanaus
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
výchozím	výchozí	k2eAgInSc7d1	výchozí
bodem	bod	k1gInSc7	bod
pro	pro	k7c4	pro
tažení	tažení	k1gNnSc4	tažení
slavnosti	slavnost	k1gFnSc2	slavnost
po	po	k7c6	po
hesenských	hesenský	k2eAgFnPc6d1	Hesenská
vinných	vinný	k2eAgFnPc6d1	vinná
ulicích	ulice	k1gFnPc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Výlety	výlet	k1gInPc1	výlet
za	za	k7c7	za
historií	historie	k1gFnSc7	historie
–	–	k?	–
radnice	radnice	k1gFnSc2	radnice
Römer	Römra	k1gFnPc2	Römra
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
jsou	být	k5eAaImIp3nP	být
středem	střed	k1gInSc7	střed
historického	historický	k2eAgNnSc2d1	historické
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
<g/>
.	.	kIx.	.
</s>
<s>
Fasádu	fasáda	k1gFnSc4	fasáda
Frankfurtské	frankfurtský	k2eAgFnSc2d1	Frankfurtská
radnice	radnice	k1gFnSc2	radnice
zdobí	zdobit	k5eAaImIp3nP	zdobit
sochy	socha	k1gFnPc1	socha
císařů	císař	k1gMnPc2	císař
Friedricha	Friedrich	k1gMnSc2	Friedrich
Barbarossy	Barbarossa	k1gMnSc2	Barbarossa
<g/>
,	,	kIx,	,
Ludvíka	Ludvík	k1gMnSc2	Ludvík
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Bavora	Bavor	k1gMnSc2	Bavor
<g/>
,	,	kIx,	,
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
Maximiliána	Maximilián	k1gMnSc4	Maximilián
II	II	kA	II
<g/>
..	..	k?	..
V	v	k7c6	v
budově	budova	k1gFnSc6	budova
radnice	radnice	k1gFnSc2	radnice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Kaisersaalem	Kaisersaal	k1gInSc7	Kaisersaal
–	–	k?	–
císařský	císařský	k2eAgInSc1d1	císařský
sál	sál	k1gInSc1	sál
s	s	k7c7	s
52	[number]	k4	52
portréty	portrét	k1gInPc4	portrét
císařů	císař	k1gMnPc2	císař
životní	životní	k2eAgFnSc2d1	životní
velikosti	velikost	k1gFnSc2	velikost
–	–	k?	–
od	od	k7c2	od
Karla	Karel	k1gMnSc2	Karel
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
Karla	Karel	k1gMnSc4	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
až	až	k9	až
do	do	k7c2	do
Františka	František	k1gMnSc2	František
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
Römerbergu	Römerberg	k1gInSc2	Römerberg
se	se	k3xPyFc4	se
tyčí	tyčit	k5eAaImIp3nS	tyčit
středověká	středověký	k2eAgFnSc1d1	středověká
katedrála	katedrála	k1gFnSc1	katedrála
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgInSc1d1	hlavní
katolický	katolický	k2eAgInSc4d1	katolický
kostel	kostel	k1gInSc4	kostel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
Paulskirche	Paulskirch	k1gFnSc2	Paulskirch
zasedal	zasedat	k5eAaImAgInS	zasedat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1848	[number]	k4	1848
<g/>
/	/	kIx~	/
<g/>
1849	[number]	k4	1849
německý	německý	k2eAgInSc1d1	německý
parlament	parlament	k1gInSc1	parlament
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
a	a	k8xC	a
rodiště	rodiště	k1gNnSc2	rodiště
J.	J.	kA	J.
W.	W.	kA	W.
Goetheho	Goethe	k1gMnSc2	Goethe
(	(	kIx(	(
<g/>
Goethe-Haus	Goethe-Haus	k1gInSc1	Goethe-Haus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kaiserdom	Kaiserdom	k1gInSc1	Kaiserdom
(	(	kIx(	(
<g/>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vedle	vedle	k7c2	vedle
Römerbergu	Römerberg	k1gInSc2	Römerberg
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
perlou	perla	k1gFnSc7	perla
historického	historický	k2eAgNnSc2d1	historické
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
Evropy	Evropa	k1gFnSc2	Evropa
měl	mít	k5eAaImAgMnS	mít
velký	velký	k2eAgInSc4d1	velký
historický	historický	k2eAgInSc4d1	historický
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tento	tento	k3xDgInSc1	tento
kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
místem	místo	k1gNnSc7	místo
volby	volba	k1gFnSc2	volba
německých	německý	k2eAgMnPc2d1	německý
králů	král	k1gMnPc2	král
a	a	k8xC	a
římských	římský	k2eAgMnPc2d1	římský
císařů	císař	k1gMnPc2	císař
Svaté	svatý	k2eAgFnSc2d1	svatá
Říše	říš	k1gFnSc2	říš
Římské	římský	k2eAgFnSc2d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
funkce	funkce	k1gFnSc1	funkce
byla	být	k5eAaImAgFnS	být
kostelu	kostel	k1gInSc3	kostel
připsána	připsán	k2eAgNnPc1d1	připsáno
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
Buly	bula	k1gFnSc2	bula
Karla	Karel	k1gMnSc2	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1356	[number]	k4	1356
<g/>
.	.	kIx.	.
</s>
<s>
Zoo	zoo	k1gFnSc1	zoo
–	–	k?	–
zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
a	a	k8xC	a
nabízí	nabízet	k5eAaImIp3nS	nabízet
svým	svůj	k3xOyFgMnPc3	svůj
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
až	až	k6eAd1	až
5.000	[number]	k4	5.000
zvířat	zvíře	k1gNnPc2	zvíře
600	[number]	k4	600
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Frankfurtské	frankfurtský	k2eAgNnSc1d1	Frankfurtské
ZOO	zoo	k1gNnSc1	zoo
je	být	k5eAaImIp3nS	být
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
svým	svůj	k3xOyFgNnPc3	svůj
architektonickým	architektonický	k2eAgNnPc3d1	architektonické
řešením	řešení	k1gNnPc3	řešení
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
podzemní	podzemní	k2eAgFnSc4d1	podzemní
a	a	k8xC	a
nadzemní	nadzemní	k2eAgFnSc4d1	nadzemní
prezentaci	prezentace	k1gFnSc4	prezentace
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
zvláštností	zvláštnost	k1gFnSc7	zvláštnost
Frankfurtského	frankfurtský	k2eAgNnSc2d1	Frankfurtské
ZOO	zoo	k1gNnSc2	zoo
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc1	možnost
pronájmu	pronájem	k1gInSc2	pronájem
zasedacích	zasedací	k2eAgFnPc2d1	zasedací
místností	místnost	k1gFnPc2	místnost
či	či	k8xC	či
možnosti	možnost	k1gFnPc1	možnost
uspořádání	uspořádání	k1gNnSc2	uspořádání
slavností	slavnost	k1gFnPc2	slavnost
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
vybrané	vybraný	k2eAgFnSc2d1	vybraná
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
areálu	areál	k1gInSc6	areál
Frankfurtského	frankfurtský	k2eAgNnSc2d1	Frankfurtské
ZOO	zoo	k1gNnSc2	zoo
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
komediální	komediální	k2eAgNnSc4d1	komediální
divadlo	divadlo	k1gNnSc4	divadlo
Fritz-Rémond-Theater	Fritz-Rémond-Theatra	k1gFnPc2	Fritz-Rémond-Theatra
<g/>
.	.	kIx.	.
</s>
<s>
Palmengarten	Palmengarten	k2eAgInSc1d1	Palmengarten
(	(	kIx(	(
<g/>
Palmové	palmový	k2eAgNnSc1d1	palmové
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zelenou	zelený	k2eAgFnSc7d1	zelená
oázou	oáza	k1gFnSc7	oáza
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
<g/>
.	.	kIx.	.
</s>
<s>
Palmengarten	Palmengarten	k2eAgInSc1d1	Palmengarten
svým	svůj	k3xOyFgMnPc3	svůj
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
nabízí	nabízet	k5eAaImIp3nS	nabízet
celoročně	celoročně	k6eAd1	celoročně
stálou	stálý	k2eAgFnSc4d1	stálá
exotickou	exotický	k2eAgFnSc4d1	exotická
výstavu	výstava	k1gFnSc4	výstava
až	až	k9	až
3000	[number]	k4	3000
druhů	druh	k1gInPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
obměňována	obměňovat	k5eAaImNgFnS	obměňovat
různými	různý	k2eAgFnPc7d1	různá
společenskými	společenský	k2eAgFnPc7d1	společenská
akcemi	akce	k1gFnPc7	akce
-	-	kIx~	-
výstavy	výstava	k1gFnPc1	výstava
<g/>
,	,	kIx,	,
koncerty	koncert	k1gInPc1	koncert
<g/>
,	,	kIx,	,
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
Main	Main	k1gMnSc1	Main
Tower	Tower	k1gMnSc1	Tower
je	být	k5eAaImIp3nS	být
nejvýše	vysoce	k6eAd3	vysoce
položená	položený	k2eAgFnSc1d1	položená
restaurace	restaurace	k1gFnSc1	restaurace
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
špici	špice	k1gFnSc6	špice
tohoto	tento	k3xDgInSc2	tento
mrakodrapu	mrakodrap	k1gInSc2	mrakodrap
je	být	k5eAaImIp3nS	být
vyhlídková	vyhlídkový	k2eAgFnSc1d1	vyhlídková
terasa	terasa	k1gFnSc1	terasa
<g/>
.	.	kIx.	.
</s>
<s>
Kleinmarkthalle	Kleinmarkthalle	k1gNnSc1	Kleinmarkthalle
(	(	kIx(	(
<g/>
Frankfurtské	frankfurtský	k2eAgNnSc1d1	Frankfurtské
tržiště	tržiště	k1gNnSc1	tržiště
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mezi	mezi	k7c7	mezi
velechrámem	velechrám	k1gInSc7	velechrám
a	a	k8xC	a
historickým	historický	k2eAgInSc7d1	historický
policejným	policejný	k2eAgInSc7d1	policejný
revírem	revír	k1gInSc7	revír
"	"	kIx"	"
<g/>
Konstablerwache	Konstablerwache	k1gInSc1	Konstablerwache
<g/>
"	"	kIx"	"
a	a	k8xC	a
nabízí	nabízet	k5eAaImIp3nS	nabízet
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
gurmánskou	gurmánský	k2eAgFnSc4d1	gurmánská
všehochuť	všehochuť	k1gFnSc4	všehochuť
<g/>
.	.	kIx.	.
</s>
<s>
Goetheturm	Goetheturm	k1gInSc1	Goetheturm
(	(	kIx(	(
<g/>
Goethova	Goethův	k2eAgFnSc1d1	Goethova
věž	věž	k1gFnSc1	věž
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
a	a	k8xC	a
nejstarší	starý	k2eAgFnSc7d3	nejstarší
vyhlídkovou	vyhlídkový	k2eAgFnSc7d1	vyhlídková
věží	věž	k1gFnSc7	věž
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
věž	věž	k1gFnSc4	věž
najdeme	najít	k5eAaPmIp1nP	najít
v	v	k7c6	v
lese	les	k1gInSc6	les
části	část	k1gFnSc2	část
obce	obec	k1gFnSc2	obec
Sachsenhausen	Sachsenhausna	k1gFnPc2	Sachsenhausna
<g/>
.	.	kIx.	.
</s>
<s>
Europaturm	Europaturm	k1gInSc1	Europaturm
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
stavba	stavba	k1gFnSc1	stavba
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Magistrát	magistrát	k1gInSc1	magistrát
města	město	k1gNnSc2	město
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
primátor	primátor	k1gMnSc1	primátor
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnPc1d1	hlavní
členové	člen	k1gMnPc1	člen
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
na	na	k7c4	na
6	[number]	k4	6
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
dobrovolní	dobrovolný	k2eAgMnPc1d1	dobrovolný
členové	člen	k1gMnPc1	člen
na	na	k7c4	na
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
města	město	k1gNnSc2	město
tvoří	tvořit	k5eAaImIp3nS	tvořit
93	[number]	k4	93
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
většina	většina	k1gFnSc1	většina
patří	patřit	k5eAaImIp3nS	patřit
CDU	CDU	kA	CDU
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
část	část	k1gFnSc1	část
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
je	být	k5eAaImIp3nS	být
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
místním	místní	k2eAgFnPc3d1	místní
radou	rada	k1gMnSc7	rada
(	(	kIx(	(
<g/>
16	[number]	k4	16
členů	člen	k1gInPc2	člen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
zastupovat	zastupovat	k5eAaImF	zastupovat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
obce	obec	k1gFnPc4	obec
na	na	k7c6	na
magistrátě	magistrát	k1gInSc6	magistrát
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
členové	člen	k1gMnPc1	člen
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
na	na	k7c4	na
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
magistrátě	magistrát	k1gInSc6	magistrát
působí	působit	k5eAaImIp3nS	působit
komunální	komunální	k2eAgNnSc4d1	komunální
zastoupení	zastoupení	k1gNnSc4	zastoupení
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
KAV	KAV	kA	KAV
(	(	kIx(	(
<g/>
Kommunale	Kommunale	k1gMnSc1	Kommunale
Ausländer-	Ausländer-	k1gMnSc1	Ausländer-
und	und	k?	und
Ausländerinenvertretung	Ausländerinenvertretung	k1gInSc1	Ausländerinenvertretung
<g/>
)	)	kIx)	)
složené	složený	k2eAgInPc1d1	složený
z	z	k7c2	z
37	[number]	k4	37
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
zastupovat	zastupovat	k5eAaImF	zastupovat
zájmy	zájem	k1gInPc4	zájem
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
občanů	občan	k1gMnPc2	občan
města	město	k1gNnSc2	město
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
<g/>
.	.	kIx.	.
</s>
<s>
Honorární	honorární	k2eAgInSc1d1	honorární
konzulát	konzulát	k1gInSc1	konzulát
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Jurgen-Ponto-Platz	Jurgen-Ponto-Platz	k1gInSc1	Jurgen-Ponto-Platz
2	[number]	k4	2
<g/>
,	,	kIx,	,
60329	[number]	k4	60329
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
am	am	k?	am
Main	Main	k1gInSc1	Main
Honorární	honorární	k2eAgInSc1d1	honorární
konzulát	konzulát	k1gInSc4	konzulát
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Honorární	honorární	k2eAgMnSc1d1	honorární
konzul	konzul	k1gMnSc1	konzul
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Robin	Robina	k1gFnPc2	Robina
Leon	Leona	k1gFnPc2	Leona
Fritz	Fritz	k1gInSc1	Fritz
Eschersheimer	Eschersheimer	k1gMnSc1	Eschersheimer
Landstr	Landstr	k1gInSc1	Landstr
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
,	,	kIx,	,
60322	[number]	k4	60322
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
am	am	k?	am
Main	Main	k1gInSc1	Main
Deutsch-Tschechische	Deutsch-Tschechische	k1gInSc1	Deutsch-Tschechische
und	und	k?	und
Deutsch-Slowakische	Deutsch-Slowakische	k1gInSc1	Deutsch-Slowakische
Wirtschaftsvereinigung	Wirtschaftsvereinigung	k1gMnSc1	Wirtschaftsvereinigung
(	(	kIx(	(
<g/>
DTSW	DTSW	kA	DTSW
<g/>
)	)	kIx)	)
e.	e.	k?	e.
V.	V.	kA	V.
Flinschstrasse	Flinschstrasse	k1gFnSc2	Flinschstrasse
55	[number]	k4	55
<g/>
,	,	kIx,	,
60388	[number]	k4	60388
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
am	am	k?	am
Main	Main	k1gInSc4	Main
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
http://altfrankfurt.com/	[url]	k?	http://altfrankfurt.com/
http://www.frankfurt-tourismus.de/	[url]	k?	http://www.frankfurt-tourismus.de/
http://www.meinestadt.de/frankfurt-am-main/home	[url]	k1gInSc5	http://www.meinestadt.de/frankfurt-am-main/home
http://www.journalportal.de/	[url]	k?	http://www.journalportal.de/
http://www.rhein-main.net/sixcms/detail.php?template=v2_rmn_welcome	[url]	k1gInSc5	http://www.rhein-main.net/sixcms/detail.php?template=v2_rmn_welcome
http://www.airportcity-frankfurt.de/cms/default/rubrik/9/9347.htm	[url]	k1gNnSc7	http://www.airportcity-frankfurt.de/cms/default/rubrik/9/9347.htm
http://www.uni-frankfurt.de/	[url]	k?	http://www.uni-frankfurt.de/
</s>
