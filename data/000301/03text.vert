<s>
Karel	Karel	k1gMnSc1	Karel
Havlíček	Havlíček	k1gMnSc1	Havlíček
Borovský	Borovský	k1gMnSc1	Borovský
<g/>
,	,	kIx,	,
pseudonymem	pseudonym	k1gInSc7	pseudonym
Havel	Havla	k1gFnPc2	Havla
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Karel	Karel	k1gMnSc1	Karel
Havlíček	Havlíček	k1gMnSc1	Havlíček
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1821	[number]	k4	1821
Borová	borový	k2eAgFnSc1d1	Borová
u	u	k7c2	u
Přibyslavi	Přibyslav	k1gFnSc2	Přibyslav
–	–	k?	–
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1856	[number]	k4	1856
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
zakladatele	zakladatel	k1gMnPc4	zakladatel
české	český	k2eAgFnSc2d1	Česká
žurnalistiky	žurnalistika	k1gFnSc2	žurnalistika
<g/>
,	,	kIx,	,
satiry	satira	k1gFnSc2	satira
a	a	k8xC	a
literární	literární	k2eAgFnSc2d1	literární
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Literárně	literárně	k6eAd1	literárně
bývá	bývat	k5eAaImIp3nS	bývat
řazen	řadit	k5eAaImNgInS	řadit
do	do	k7c2	do
realismu	realismus	k1gInSc2	realismus
<g/>
,	,	kIx,	,
politicky	politicky	k6eAd1	politicky
pak	pak	k6eAd1	pak
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
2	[number]	k4	2
<g/>
.	.	kIx.	.
generaci	generace	k1gFnSc4	generace
národních	národní	k2eAgInPc2d1	národní
buditelů	buditel	k1gMnPc2	buditel
<g/>
.	.	kIx.	.
</s>
<s>
Přídomek	přídomek	k1gInSc1	přídomek
"	"	kIx"	"
<g/>
Borovský	Borovský	k1gMnSc1	Borovský
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
často	často	k6eAd1	často
podepisoval	podepisovat	k5eAaImAgMnS	podepisovat
své	svůj	k3xOyFgInPc4	svůj
články	článek	k1gInPc4	článek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgMnS	odvodit
od	od	k7c2	od
jeho	on	k3xPp3gNnSc2	on
místa	místo	k1gNnSc2	místo
narození	narození	k1gNnSc2	narození
(	(	kIx(	(
<g/>
Borová	borový	k2eAgFnSc1d1	Borová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kupci	kupec	k1gMnSc3	kupec
Matěji	Matěj	k1gMnSc3	Matěj
Havlíčkovi	Havlíček	k1gMnSc3	Havlíček
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
ženě	žena	k1gFnSc3	žena
Josefíně	Josefína	k1gFnSc3	Josefína
Dvořákové	Dvořáková	k1gFnSc3	Dvořáková
<g/>
,	,	kIx,	,
dceři	dcera	k1gFnSc6	dcera
sládka	sládek	k1gMnSc2	sládek
z	z	k7c2	z
Horní	horní	k2eAgFnSc2d1	horní
Cerekve	Cerekev	k1gFnSc2	Cerekev
<g/>
,	,	kIx,	,
se	s	k7c7	s
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1821	[number]	k4	1821
v	v	k7c6	v
Borové	borový	k2eAgFnSc6d1	Borová
u	u	k7c2	u
Německého	německý	k2eAgInSc2d1	německý
Brodu	Brod	k1gInSc2	Brod
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
žil	žít	k5eAaImAgMnS	žít
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Borová	borový	k2eAgFnSc1d1	Borová
na	na	k7c6	na
Vysočině	vysočina	k1gFnSc6	vysočina
a	a	k8xC	a
v	v	k7c6	v
Německém	německý	k2eAgInSc6d1	německý
Brodě	Brod	k1gInSc6	Brod
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
měl	mít	k5eAaImAgMnS	mít
zde	zde	k6eAd1	zde
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
živnost	živnost	k1gFnSc1	živnost
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
domě	dům	k1gInSc6	dům
muzeum	muzeum	k1gNnSc1	muzeum
se	s	k7c7	s
stálou	stálý	k2eAgFnSc7d1	stálá
expozicí	expozice	k1gFnSc7	expozice
věnovanou	věnovaný	k2eAgFnSc4d1	věnovaná
životu	život	k1gInSc3	život
a	a	k8xC	a
dílu	dílo	k1gNnSc3	dílo
Karla	Karel	k1gMnSc2	Karel
Havlíčka	Havlíček	k1gMnSc2	Havlíček
Borovského	Borovský	k1gMnSc2	Borovský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
devíti	devět	k4xCc6	devět
letech	léto	k1gNnPc6	léto
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
rok	rok	k1gInSc4	rok
do	do	k7c2	do
Jihlavy	Jihlava	k1gFnSc2	Jihlava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
učil	učit	k5eAaImAgMnS	učit
němčině	němčina	k1gFnSc3	němčina
a	a	k8xC	a
také	také	k9	také
zde	zde	k6eAd1	zde
začal	začít	k5eAaPmAgInS	začít
chápat	chápat	k5eAaImF	chápat
národnostní	národnostní	k2eAgInPc4d1	národnostní
rozdíly	rozdíl	k1gInPc4	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1833	[number]	k4	1833
studoval	studovat	k5eAaImAgMnS	studovat
v	v	k7c6	v
Německém	německý	k2eAgInSc6d1	německý
Brodě	Brod	k1gInSc6	Brod
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1838	[number]	k4	1838
gymnázium	gymnázium	k1gNnSc4	gymnázium
dokončil	dokončit	k5eAaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
samého	samý	k3xTgInSc2	samý
roku	rok	k1gInSc2	rok
studoval	studovat	k5eAaImAgMnS	studovat
filosofii	filosofie	k1gFnSc4	filosofie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Havlíček	Havlíček	k1gMnSc1	Havlíček
toužil	toužit	k5eAaImAgMnS	toužit
po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
působit	působit	k5eAaImF	působit
na	na	k7c4	na
výchovu	výchova	k1gFnSc4	výchova
českého	český	k2eAgInSc2d1	český
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
proto	proto	k8xC	proto
do	do	k7c2	do
kněžského	kněžský	k2eAgInSc2d1	kněžský
semináře	seminář	k1gInSc2	seminář
<g/>
.	.	kIx.	.
</s>
<s>
Nelíbily	líbit	k5eNaImAgFnP	líbit
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
však	však	k9	však
poměry	poměr	k1gInPc7	poměr
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zde	zde	k6eAd1	zde
panovaly	panovat	k5eAaImAgFnP	panovat
–	–	k?	–
kněžstvo	kněžstvo	k1gNnSc1	kněžstvo
bylo	být	k5eAaImAgNnS	být
vychováno	vychovat	k5eAaPmNgNnS	vychovat
v	v	k7c6	v
konservatismu	konservatismus	k1gInSc6	konservatismus
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
protinárodním	protinárodní	k2eAgMnSc6d1	protinárodní
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1841	[number]	k4	1841
proto	proto	k8xC	proto
seminář	seminář	k1gInSc1	seminář
opustil	opustit	k5eAaPmAgInS	opustit
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
nekompromisním	kompromisní	k2eNgMnSc7d1	nekompromisní
kritikem	kritik	k1gMnSc7	kritik
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
rovněž	rovněž	k9	rovněž
proti	proti	k7c3	proti
celibátu	celibát	k1gInSc3	celibát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
dle	dle	k7c2	dle
jeho	jeho	k3xOp3gInSc2	jeho
názoru	názor	k1gInSc2	názor
proti	proti	k7c3	proti
přírodě	příroda	k1gFnSc3	příroda
a	a	k8xC	a
lidské	lidský	k2eAgFnSc3d1	lidská
přirozenosti	přirozenost	k1gFnSc3	přirozenost
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
byl	být	k5eAaImAgMnS	být
vlivem	vlivem	k7c2	vlivem
Jána	Ján	k1gMnSc2	Ján
Kollára	Kollár	k1gMnSc2	Kollár
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
přátel	přítel	k1gMnPc2	přítel
zastáncem	zastánce	k1gMnSc7	zastánce
rusofilství	rusofilství	k1gNnSc2	rusofilství
a	a	k8xC	a
všeslovanské	všeslovanský	k2eAgFnSc2d1	všeslovanská
vzájemnosti	vzájemnost	k1gFnSc2	vzájemnost
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
výrazně	výrazně	k6eAd1	výrazně
korigoval	korigovat	k5eAaBmAgMnS	korigovat
po	po	k7c6	po
ročním	roční	k2eAgInSc6d1	roční
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
vychovatel	vychovatel	k1gMnSc1	vychovatel
u	u	k7c2	u
významného	významný	k2eAgMnSc2d1	významný
profesora	profesor	k1gMnSc2	profesor
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1844	[number]	k4	1844
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
s	s	k7c7	s
přesvědčením	přesvědčení	k1gNnSc7	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
slovanská	slovanský	k2eAgFnSc1d1	Slovanská
vzájemnost	vzájemnost	k1gFnSc1	vzájemnost
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
nereálná	reálný	k2eNgFnSc1d1	nereálná
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
vychází	vycházet	k5eAaImIp3nS	vycházet
jeho	jeho	k3xOp3gNnSc1	jeho
první	první	k4xOgNnSc1	první
tištěné	tištěný	k2eAgNnSc1d1	tištěné
dílo	dílo	k1gNnSc1	dílo
v	v	k7c6	v
příloze	příloha	k1gFnSc6	příloha
Pražských	pražský	k2eAgFnPc2d1	Pražská
novin	novina	k1gFnPc2	novina
–	–	k?	–
Obrazy	obraz	k1gInPc1	obraz
z	z	k7c2	z
Rus	Rus	k1gMnSc1	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1846	[number]	k4	1846
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
na	na	k7c4	na
doporučení	doporučení	k1gNnSc4	doporučení
Františka	František	k1gMnSc2	František
Palackého	Palacký	k1gMnSc2	Palacký
redaktorem	redaktor	k1gMnSc7	redaktor
Pražských	pražský	k2eAgFnPc2d1	Pražská
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Psal	psát	k5eAaImAgMnS	psát
také	také	k9	také
do	do	k7c2	do
satirické	satirický	k2eAgFnSc2d1	satirická
přílohy	příloha	k1gFnSc2	příloha
časopisu	časopis	k1gInSc2	časopis
Česká	český	k2eAgFnSc1d1	Česká
včela	včela	k1gFnSc1	včela
s	s	k7c7	s
názvem	název	k1gInSc7	název
Žihadlo	žihadlo	k1gNnSc1	žihadlo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1845	[number]	k4	1845
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
ostrou	ostrý	k2eAgFnSc7d1	ostrá
kritikou	kritika	k1gFnSc7	kritika
nového	nový	k2eAgInSc2d1	nový
Tylova	Tylův	k2eAgInSc2d1	Tylův
románu	román	k1gInSc2	román
Poslední	poslední	k2eAgMnSc1d1	poslední
Čech	Čech	k1gMnSc1	Čech
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
revolučním	revoluční	k2eAgInSc6d1	revoluční
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
Pražských	pražský	k2eAgFnPc2d1	Pražská
novin	novina	k1gFnPc2	novina
<g/>
,	,	kIx,	,
a	a	k8xC	a
za	za	k7c2	za
finanční	finanční	k2eAgFnSc2d1	finanční
pomoci	pomoc	k1gFnSc2	pomoc
šlechtice	šlechtic	k1gMnSc2	šlechtic
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
Deyma	Deym	k1gMnSc2	Deym
založil	založit	k5eAaPmAgMnS	založit
vlastní	vlastní	k2eAgFnSc2d1	vlastní
Národní	národní	k2eAgFnSc2d1	národní
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
velké	velký	k2eAgFnPc4d1	velká
popularity	popularita	k1gFnPc4	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnPc1d1	národní
noviny	novina	k1gFnPc1	novina
byly	být	k5eAaImAgFnP	být
prvním	první	k4xOgInSc7	první
deníkem	deník	k1gInSc7	deník
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
byl	být	k5eAaImAgMnS	být
Havlíček	Havlíček	k1gMnSc1	Havlíček
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
zastánců	zastánce	k1gMnPc2	zastánce
národní	národní	k2eAgFnSc2d1	národní
myšlenky	myšlenka	k1gFnSc2	myšlenka
<g/>
,	,	kIx,	,
formu	forma	k1gFnSc4	forma
revoluce	revoluce	k1gFnSc2	revoluce
spíše	spíše	k9	spíše
odsuzoval	odsuzovat	k5eAaImAgMnS	odsuzovat
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
si	se	k3xPyFc3	se
vědom	vědom	k2eAgMnSc1d1	vědom
slabé	slabý	k2eAgInPc4d1	slabý
síly	síl	k1gInPc4	síl
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
případných	případný	k2eAgInPc2d1	případný
následků	následek	k1gInPc2	následek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mohly	moct	k5eAaImAgFnP	moct
vrátit	vrátit	k5eAaPmF	vrátit
politiku	politika	k1gFnSc4	politika
o	o	k7c4	o
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
tyto	tento	k3xDgFnPc1	tento
obavy	obava	k1gFnPc1	obava
se	se	k3xPyFc4	se
následným	následný	k2eAgInSc7d1	následný
vývojem	vývoj	k1gInSc7	vývoj
potvrdily	potvrdit	k5eAaPmAgFnP	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
organizaci	organizace	k1gFnSc4	organizace
Slovanského	slovanský	k2eAgInSc2d1	slovanský
sjezdu	sjezd	k1gInSc2	sjezd
<g/>
,	,	kIx,	,
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Polsko	Polsko	k1gNnSc4	Polsko
a	a	k8xC	a
Chorvatsko	Chorvatsko	k1gNnSc4	Chorvatsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přesvědčil	přesvědčit	k5eAaPmAgInS	přesvědčit
tamní	tamní	k2eAgMnPc4d1	tamní
spisovatele	spisovatel	k1gMnPc4	spisovatel
k	k	k7c3	k
účasti	účast	k1gFnSc3	účast
na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gInSc7	člen
Národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
po	po	k7c6	po
potlačení	potlačení	k1gNnSc6	potlačení
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
byl	být	k5eAaImAgMnS	být
poprvé	poprvé	k6eAd1	poprvé
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
vězněn	vězněn	k2eAgInSc1d1	vězněn
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
získané	získaný	k2eAgFnSc2d1	získaná
imunity	imunita	k1gFnSc2	imunita
jej	on	k3xPp3gNnSc4	on
vojenští	vojenský	k2eAgMnPc1d1	vojenský
správci	správce	k1gMnPc1	správce
Prahy	Praha	k1gFnSc2	Praha
propustili	propustit	k5eAaPmAgMnP	propustit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgMnS	být
totiž	totiž	k9	totiž
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
zvolen	zvolit	k5eAaPmNgMnS	zvolit
na	na	k7c4	na
rakouský	rakouský	k2eAgInSc4d1	rakouský
ústavodárný	ústavodárný	k2eAgInSc4d1	ústavodárný
Říšský	říšský	k2eAgInSc4d1	říšský
sněm	sněm	k1gInSc4	sněm
<g/>
.	.	kIx.	.
</s>
<s>
Zastupoval	zastupovat	k5eAaImAgInS	zastupovat
volební	volební	k2eAgInSc1d1	volební
obvod	obvod	k1gInSc1	obvod
Humpolec	Humpolec	k1gInSc1	Humpolec
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
jako	jako	k9	jako
redaktor	redaktor	k1gMnSc1	redaktor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
výboru	výbor	k1gInSc6	výbor
byl	být	k5eAaImAgInS	být
poměrně	poměrně	k6eAd1	poměrně
aktivní	aktivní	k2eAgInSc1d1	aktivní
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
na	na	k7c6	na
Říšském	říšský	k2eAgInSc6d1	říšský
sněmu	sněm	k1gInSc6	sněm
však	však	k9	však
působil	působit	k5eAaImAgMnS	působit
více	hodně	k6eAd2	hodně
jako	jako	k9	jako
novinář	novinář	k1gMnSc1	novinář
než	než	k8xS	než
jako	jako	k8xC	jako
poslanec	poslanec	k1gMnSc1	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1848	[number]	k4	1848
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
poslanecký	poslanecký	k2eAgInSc4d1	poslanecký
mandát	mandát	k1gInSc4	mandát
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Havlíček	Havlíček	k1gMnSc1	Havlíček
nadále	nadále	k6eAd1	nadále
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
velmi	velmi	k6eAd1	velmi
ostře	ostro	k6eAd1	ostro
proti	proti	k7c3	proti
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
zavedení	zavedení	k1gNnSc4	zavedení
kaucí	kauce	k1gFnPc2	kauce
na	na	k7c4	na
politická	politický	k2eAgNnPc4d1	politické
periodika	periodikum	k1gNnPc4	periodikum
byl	být	k5eAaImAgInS	být
nucen	nutit	k5eAaImNgMnS	nutit
přestat	přestat	k5eAaPmF	přestat
vydávat	vydávat	k5eAaImF	vydávat
satirickou	satirický	k2eAgFnSc4d1	satirická
přílohu	příloha	k1gFnSc4	příloha
Národních	národní	k2eAgFnPc2d1	národní
novin	novina	k1gFnPc2	novina
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Šotek	šotek	k1gMnSc1	šotek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
výjimečného	výjimečný	k2eAgInSc2d1	výjimečný
stavu	stav	k1gInSc2	stav
nad	nad	k7c7	nad
Prahou	Praha	k1gFnSc7	Praha
a	a	k8xC	a
okolím	okolí	k1gNnSc7	okolí
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tzv.	tzv.	kA	tzv.
májového	májový	k2eAgNnSc2d1	Májové
spiknutí	spiknutí	k1gNnSc2	spiknutí
pražské	pražský	k2eAgNnSc4d1	Pražské
vojenské	vojenský	k2eAgNnSc4d1	vojenské
velitelství	velitelství	k1gNnSc4	velitelství
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1849	[number]	k4	1849
Národní	národní	k2eAgFnSc2d1	národní
noviny	novina	k1gFnSc2	novina
zakázalo	zakázat	k5eAaPmAgNnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
vydávání	vydávání	k1gNnSc1	vydávání
bylo	být	k5eAaImAgNnS	být
povoleno	povolit	k5eAaPmNgNnS	povolit
za	za	k7c4	za
Havlíčkův	Havlíčkův	k2eAgInSc4d1	Havlíčkův
příslib	příslib	k1gInSc4	příslib
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zdrží	zdržet	k5eAaPmIp3nP	zdržet
zásadní	zásadní	k2eAgFnPc1d1	zásadní
kritiky	kritika	k1gFnPc1	kritika
březnové	březnový	k2eAgInPc4d1	březnový
oktrojované	oktrojovaný	k2eAgInPc4d1	oktrojovaný
ústavy	ústav	k1gInPc4	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pokračující	pokračující	k2eAgFnSc4d1	pokračující
kritiku	kritika	k1gFnSc4	kritika
vládní	vládní	k2eAgFnSc2d1	vládní
politiky	politika	k1gFnSc2	politika
byly	být	k5eAaImAgFnP	být
Národní	národní	k2eAgFnPc1d1	národní
noviny	novina	k1gFnPc1	novina
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1850	[number]	k4	1850
pražským	pražský	k2eAgNnSc7d1	Pražské
vojenským	vojenský	k2eAgNnSc7d1	vojenské
velitelstvím	velitelství	k1gNnSc7	velitelství
zakázány	zakázat	k5eAaPmNgFnP	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Havlíček	Havlíček	k1gMnSc1	Havlíček
hledal	hledat	k5eAaImAgMnS	hledat
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
žurnalistické	žurnalistický	k2eAgFnSc6d1	žurnalistická
činnosti	činnost	k1gFnSc6	činnost
mimo	mimo	k7c4	mimo
oblast	oblast	k1gFnSc4	oblast
výjimečného	výjimečný	k2eAgInSc2d1	výjimečný
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Usadil	usadit	k5eAaPmAgInS	usadit
se	se	k3xPyFc4	se
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
založil	založit	k5eAaPmAgInS	založit
časopis	časopis	k1gInSc1	časopis
Slovan	Slovan	k1gInSc4	Slovan
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vycházel	vycházet	k5eAaImAgInS	vycházet
2	[number]	k4	2
<g/>
×	×	k?	×
týdně	týdně	k6eAd1	týdně
<g/>
.	.	kIx.	.
</s>
<s>
Úřední	úřední	k2eAgInPc1d1	úřední
orgány	orgán	k1gInPc1	orgán
se	se	k3xPyFc4	se
však	však	k9	však
snažily	snažit	k5eAaImAgFnP	snažit
jeho	on	k3xPp3gInSc4	on
dosah	dosah	k1gInSc4	dosah
komplikovat	komplikovat	k5eAaBmF	komplikovat
<g/>
,	,	kIx,	,
redaktor	redaktor	k1gMnSc1	redaktor
byl	být	k5eAaImAgMnS	být
sledován	sledován	k2eAgMnSc1d1	sledován
<g/>
,	,	kIx,	,
podrobován	podrobován	k2eAgMnSc1d1	podrobován
domovním	domovní	k2eAgFnPc3d1	domovní
prohlídkám	prohlídka	k1gFnPc3	prohlídka
<g/>
,	,	kIx,	,
několik	několik	k4yIc1	několik
čísel	číslo	k1gNnPc2	číslo
Slovana	Slovan	k1gInSc2	Slovan
bylo	být	k5eAaImAgNnS	být
zkonfiskováno	zkonfiskován	k2eAgNnSc1d1	zkonfiskováno
a	a	k8xC	a
s	s	k7c7	s
Havlíčkem	Havlíček	k1gMnSc7	Havlíček
byly	být	k5eAaImAgInP	být
vedeny	vést	k5eAaImNgInP	vést
soudní	soudní	k2eAgInPc1d1	soudní
procesy	proces	k1gInPc1	proces
pro	pro	k7c4	pro
porušení	porušení	k1gNnSc4	porušení
tiskového	tiskový	k2eAgInSc2d1	tiskový
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
Havlíček	Havlíček	k1gMnSc1	Havlíček
ve	v	k7c6	v
Slovanu	Slovan	k1gInSc6	Slovan
s	s	k7c7	s
osobitým	osobitý	k2eAgInSc7d1	osobitý
humorem	humor	k1gInSc7	humor
komentoval	komentovat	k5eAaBmAgMnS	komentovat
<g/>
.	.	kIx.	.
</s>
<s>
Vládní	vládní	k2eAgFnSc1d1	vládní
strana	strana	k1gFnSc1	strana
se	se	k3xPyFc4	se
jej	on	k3xPp3gInSc4	on
neúspěšně	úspěšně	k6eNd1	úspěšně
snažila	snažit	k5eAaImAgFnS	snažit
přimět	přimět	k5eAaPmF	přimět
k	k	k7c3	k
loajalitě	loajalita	k1gFnSc3	loajalita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
činnost	činnost	k1gFnSc4	činnost
opozičních	opoziční	k2eAgMnPc2d1	opoziční
žurnalistů	žurnalist	k1gMnPc2	žurnalist
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1851	[number]	k4	1851
vydáno	vydat	k5eAaPmNgNnS	vydat
opatření	opatření	k1gNnSc1	opatření
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
základě	základ	k1gInSc6	základ
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
zakázat	zakázat	k5eAaPmF	zakázat
vydávání	vydávání	k1gNnSc4	vydávání
protivládních	protivládní	k2eAgNnPc2d1	protivládní
periodik	periodikum	k1gNnPc2	periodikum
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
obdrží	obdržet	k5eAaPmIp3nP	obdržet
dvě	dva	k4xCgFnPc1	dva
úřední	úřední	k2eAgFnPc1d1	úřední
výstrahy	výstraha	k1gFnPc1	výstraha
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
byly	být	k5eAaImAgFnP	být
Slovanu	Slovan	k1gInSc2	Slovan
tyto	tento	k3xDgFnPc1	tento
výstrahy	výstraha	k1gFnPc1	výstraha
uděleny	udělen	k2eAgFnPc1d1	udělena
<g/>
,	,	kIx,	,
Havlíček	Havlíček	k1gMnSc1	Havlíček
sám	sám	k3xTgMnSc1	sám
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
jeho	jeho	k3xOp3gNnSc4	jeho
vydávání	vydávání	k1gNnSc4	vydávání
zastavil	zastavit	k5eAaPmAgMnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
žurnalistickou	žurnalistický	k2eAgFnSc4d1	žurnalistická
práci	práce	k1gFnSc4	práce
přerušit	přerušit	k5eAaPmF	přerušit
a	a	k8xC	a
věnovat	věnovat	k5eAaPmF	věnovat
se	se	k3xPyFc4	se
rodině	rodina	k1gFnSc3	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Plánoval	plánovat	k5eAaImAgMnS	plánovat
založit	založit	k5eAaPmF	založit
hospodářství	hospodářství	k1gNnSc4	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
radu	rada	k1gFnSc4	rada
Františka	František	k1gMnSc2	František
Palackého	Palacký	k1gMnSc2	Palacký
vydal	vydat	k5eAaPmAgInS	vydat
knižně	knižně	k6eAd1	knižně
soubor	soubor	k1gInSc1	soubor
svých	svůj	k3xOyFgInPc2	svůj
článků	článek	k1gInPc2	článek
Duch	duch	k1gMnSc1	duch
Národních	národní	k2eAgFnPc2d1	národní
novin	novina	k1gFnPc2	novina
a	a	k8xC	a
Kutnohorské	kutnohorský	k2eAgFnSc2d1	Kutnohorská
epištoly	epištola	k1gFnSc2	epištola
<g/>
.	.	kIx.	.
</s>
<s>
Prodej	prodej	k1gInSc1	prodej
Kutnohorských	kutnohorský	k2eAgFnPc2d1	Kutnohorská
epištol	epištola	k1gFnPc2	epištola
byl	být	k5eAaImAgInS	být
policií	policie	k1gFnSc7	policie
zakázán	zakázat	k5eAaPmNgInS	zakázat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
část	část	k1gFnSc4	část
výtisků	výtisk	k1gInPc2	výtisk
distribuovat	distribuovat	k5eAaBmF	distribuovat
před	před	k7c7	před
jejich	jejich	k3xOp3gFnSc7	jejich
konfiskací	konfiskace	k1gFnSc7	konfiskace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1851	[number]	k4	1851
byl	být	k5eAaImAgMnS	být
Havlíček	Havlíček	k1gMnSc1	Havlíček
postaven	postavit	k5eAaPmNgMnS	postavit
před	před	k7c4	před
kutnohorský	kutnohorský	k2eAgInSc4d1	kutnohorský
soud	soud	k1gInSc4	soud
a	a	k8xC	a
obviněn	obviněn	k2eAgMnSc1d1	obviněn
z	z	k7c2	z
několika	několik	k4yIc2	několik
přečinů	přečin	k1gInPc2	přečin
<g/>
.	.	kIx.	.
</s>
<s>
Soud	soud	k1gInSc1	soud
ho	on	k3xPp3gMnSc4	on
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jeho	jeho	k3xOp3gFnSc2	jeho
brilantní	brilantní	k2eAgFnSc2d1	brilantní
obhajoby	obhajoba	k1gFnSc2	obhajoba
a	a	k8xC	a
znalosti	znalost	k1gFnSc2	znalost
zákonů	zákon	k1gInPc2	zákon
osvobodil	osvobodit	k5eAaPmAgInS	osvobodit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
se	se	k3xPyFc4	se
Havlíčkovi	Havlíčkův	k2eAgMnPc1d1	Havlíčkův
vrátili	vrátit	k5eAaPmAgMnP	vrátit
do	do	k7c2	do
domu	dům	k1gInSc2	dům
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Německého	německý	k2eAgInSc2d1	německý
Brodu	Brod	k1gInSc2	Brod
<g/>
.	.	kIx.	.
</s>
<s>
Havlíček	Havlíček	k1gMnSc1	Havlíček
žil	žít	k5eAaImAgMnS	žít
spíše	spíše	k9	spíše
v	v	k7c6	v
ústraní	ústraní	k1gNnSc6	ústraní
a	a	k8xC	a
společensky	společensky	k6eAd1	společensky
se	se	k3xPyFc4	se
neangažoval	angažovat	k5eNaBmAgMnS	angažovat
<g/>
.	.	kIx.	.
</s>
<s>
Těšil	těšit	k5eAaImAgInS	těšit
se	se	k3xPyFc4	se
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
hospodářství	hospodářství	k1gNnSc6	hospodářství
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
nalezl	nalézt	k5eAaBmAgMnS	nalézt
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
dá	dát	k5eAaPmIp3nS	dát
rakouská	rakouský	k2eAgFnSc1d1	rakouská
moc	moc	k1gFnSc1	moc
pokoj	pokoj	k1gInSc4	pokoj
<g/>
,	,	kIx,	,
však	však	k9	však
spíše	spíše	k9	spíše
pochyboval	pochybovat	k5eAaImAgInS	pochybovat
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
prý	prý	k9	prý
dokonce	dokonce	k9	dokonce
před	před	k7c7	před
chystaným	chystaný	k2eAgInSc7d1	chystaný
zákrokem	zákrok	k1gInSc7	zákrok
varován	varovat	k5eAaImNgInS	varovat
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
úřady	úřad	k1gInPc1	úřad
uchýlí	uchýlit	k5eAaPmIp3nP	uchýlit
k	k	k7c3	k
tak	tak	k6eAd1	tak
tvrdému	tvrdý	k2eAgInSc3d1	tvrdý
zásahu	zásah	k1gInSc3	zásah
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nevěřil	věřit	k5eNaImAgMnS	věřit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
prosince	prosinec	k1gInSc2	prosinec
1851	[number]	k4	1851
přinesl	přinést	k5eAaPmAgMnS	přinést
ministr	ministr	k1gMnSc1	ministr
Bach	Bach	k1gMnSc1	Bach
císaři	císař	k1gMnSc3	císař
Franzi	Franze	k1gFnSc6	Franze
Josefovi	Josef	k1gMnSc3	Josef
I.	I.	kA	I.
doporučení	doporučení	k1gNnSc4	doporučení
deportovat	deportovat	k5eAaBmF	deportovat
Havlíčka	Havlíček	k1gMnSc4	Havlíček
do	do	k7c2	do
Salzburgu	Salzburg	k1gInSc2	Salzburg
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
však	však	k9	však
pobyt	pobyt	k1gInSc4	pobyt
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
významném	významný	k2eAgNnSc6d1	významné
městě	město	k1gNnSc6	město
zřejmě	zřejmě	k6eAd1	zřejmě
nepovažoval	považovat	k5eNaImAgInS	považovat
za	za	k7c4	za
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
opatření	opatření	k1gNnSc4	opatření
a	a	k8xC	a
zvolil	zvolit	k5eAaPmAgMnS	zvolit
odlehlý	odlehlý	k2eAgInSc4d1	odlehlý
Brixen	Brixen	k1gInSc4	Brixen
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
jižním	jižní	k2eAgNnSc6d1	jižní
Tyrolsku	Tyrolsko	k1gNnSc6	Tyrolsko
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1851	[number]	k4	1851
kolem	kolem	k7c2	kolem
třetí	třetí	k4xOgFnSc2	třetí
hodiny	hodina	k1gFnSc2	hodina
ranní	ranní	k1gFnSc2	ranní
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
Havlíčkova	Havlíčkův	k2eAgInSc2d1	Havlíčkův
bytu	byt	k1gInSc2	byt
policejní	policejní	k2eAgMnSc1d1	policejní
komisař	komisař	k1gMnSc1	komisař
Franz	Franz	k1gInSc4	Franz
Dedera	Dedero	k1gNnSc2	Dedero
s	s	k7c7	s
dvěma	dva	k4xCgMnPc7	dva
strážníky	strážník	k1gMnPc7	strážník
(	(	kIx(	(
<g/>
akci	akce	k1gFnSc4	akce
dozoroval	dozorovat	k5eAaImAgMnS	dozorovat
i	i	k9	i
brodský	brodský	k2eAgMnSc1d1	brodský
okresní	okresní	k2eAgMnSc1d1	okresní
hejtman	hejtman	k1gMnSc1	hejtman
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Voith	Voith	k1gMnSc1	Voith
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
s	s	k7c7	s
Havlíčkem	Havlíček	k1gMnSc7	Havlíček
znal	znát	k5eAaImAgMnS	znát
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
jej	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gInSc4	on
následoval	následovat	k5eAaImAgInS	následovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
dalších	další	k2eAgInPc2d1	další
tří	tři	k4xCgMnPc2	tři
strážníků	strážník	k1gMnPc2	strážník
byl	být	k5eAaImAgInS	být
spěšným	spěšný	k2eAgInSc7d1	spěšný
dostavníkem	dostavník	k1gInSc7	dostavník
dopraven	dopravna	k1gFnPc2	dopravna
do	do	k7c2	do
tyrolského	tyrolský	k2eAgInSc2d1	tyrolský
Brixenu	Brixen	k1gInSc2	Brixen
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
přes	přes	k7c4	přes
zasněžené	zasněžený	k2eAgFnPc4d1	zasněžená
Alpy	Alpy	k1gFnPc4	Alpy
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
náročná	náročný	k2eAgFnSc1d1	náročná
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
splašili	splašit	k5eAaPmAgMnP	splašit
koně	kůň	k1gMnSc4	kůň
a	a	k8xC	a
Havlíček	Havlíček	k1gMnSc1	Havlíček
byl	být	k5eAaImAgMnS	být
jediný	jediný	k2eAgMnSc1d1	jediný
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
se	se	k3xPyFc4	se
je	on	k3xPp3gFnPc4	on
odvážil	odvážit	k5eAaPmAgMnS	odvážit
zastavit	zastavit	k5eAaPmF	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brixenu	Brixen	k1gInSc6	Brixen
byl	být	k5eAaImAgMnS	být
Havlíček	Havlíček	k1gMnSc1	Havlíček
zpočátku	zpočátku	k6eAd1	zpočátku
ubytován	ubytovat	k5eAaPmNgMnS	ubytovat
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Elephant	Elephanta	k1gFnPc2	Elephanta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
seznámil	seznámit	k5eAaPmAgInS	seznámit
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
vyhnanci	vyhnanec	k1gMnPc7	vyhnanec
–	–	k?	–
s	s	k7c7	s
Johannem	Johann	k1gMnSc7	Johann
Aloisem	Alois	k1gMnSc7	Alois
Schallhammerem	Schallhammer	k1gMnSc7	Schallhammer
<g/>
,	,	kIx,	,
účastníkem	účastník	k1gMnSc7	účastník
revoluce	revoluce	k1gFnSc2	revoluce
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
a	a	k8xC	a
důstojníkem	důstojník	k1gMnSc7	důstojník
Rudolfem	Rudolf	k1gMnSc7	Rudolf
Hebrem	Hebr	k1gMnSc7	Hebr
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
ocitl	ocitnout	k5eAaPmAgMnS	ocitnout
kvůli	kvůli	k7c3	kvůli
dluhům	dluh	k1gInPc3	dluh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1852	[number]	k4	1852
za	za	k7c7	za
Havlíčkem	Havlíček	k1gMnSc7	Havlíček
přijela	přijet	k5eAaPmAgFnS	přijet
jeho	jeho	k3xOp3gMnPc3	jeho
manželka	manželka	k1gFnSc1	manželka
Julie	Julie	k1gFnSc1	Julie
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
Zdeňkou	Zdeňka	k1gFnSc7	Zdeňka
<g/>
.	.	kIx.	.
</s>
<s>
Cestovní	cestovní	k2eAgInPc1d1	cestovní
náklady	náklad	k1gInPc1	náklad
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
150	[number]	k4	150
zlatých	zlatá	k1gFnPc2	zlatá
zaplatilo	zaplatit	k5eAaPmAgNnS	zaplatit
policejní	policejní	k2eAgNnSc1d1	policejní
ředitelství	ředitelství	k1gNnSc1	ředitelství
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnou	hmotný	k2eAgFnSc7d1	hmotná
nouzí	nouze	k1gFnSc7	nouze
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
Havlíček	Havlíček	k1gMnSc1	Havlíček
ani	ani	k8xC	ani
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
netrpěla	trpět	k5eNaImAgFnS	trpět
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
Havlíček	Havlíček	k1gMnSc1	Havlíček
dostával	dostávat	k5eAaImAgMnS	dostávat
od	od	k7c2	od
rakouského	rakouský	k2eAgNnSc2d1	rakouské
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
v	v	k7c6	v
pravidelných	pravidelný	k2eAgFnPc6d1	pravidelná
měsíčních	měsíční	k2eAgFnPc6d1	měsíční
splátkách	splátka	k1gFnPc6	splátka
500	[number]	k4	500
zlatých	zlatý	k1gInPc2	zlatý
ročně	ročně	k6eAd1	ročně
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
představu	představa	k1gFnSc4	představa
vyšší	vysoký	k2eAgMnSc1d2	vyšší
úředník	úředník	k1gMnSc1	úředník
tehdy	tehdy	k6eAd1	tehdy
dostával	dostávat	k5eAaImAgMnS	dostávat
500	[number]	k4	500
–	–	k?	–
700	[number]	k4	700
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
130	[number]	k4	130
a	a	k8xC	a
dělník	dělník	k1gMnSc1	dělník
100	[number]	k4	100
–	–	k?	–
200	[number]	k4	200
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
živobytí	živobytí	k1gNnSc4	živobytí
to	ten	k3xDgNnSc1	ten
však	však	k9	však
evidentně	evidentně	k6eAd1	evidentně
nedostačovalo	dostačovat	k5eNaImAgNnS	dostačovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Havlíček	Havlíček	k1gMnSc1	Havlíček
musel	muset	k5eAaImAgMnS	muset
citelně	citelně	k6eAd1	citelně
sáhnout	sáhnout	k5eAaPmF	sáhnout
do	do	k7c2	do
rodinných	rodinný	k2eAgFnPc2d1	rodinná
úspor	úspora	k1gFnPc2	úspora
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Pobyt	pobyt	k1gInSc1	pobyt
v	v	k7c6	v
horském	horský	k2eAgInSc6d1	horský
Brixenu	Brixen	k1gInSc6	Brixen
zdravotně	zdravotně	k6eAd1	zdravotně
prospíval	prospívat	k5eAaImAgMnS	prospívat
celé	celý	k2eAgFnSc3d1	celá
rodině	rodina	k1gFnSc3	rodina
nakažené	nakažený	k2eAgNnSc4d1	nakažené
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Havlíček	Havlíček	k1gMnSc1	Havlíček
byl	být	k5eAaImAgMnS	být
3,5	[number]	k4	3,5
roku	rok	k1gInSc2	rok
ve	v	k7c6	v
vyhnanství	vyhnanství	k1gNnSc6	vyhnanství
<g/>
,	,	kIx,	,
odtržen	odtrhnout	k5eAaPmNgMnS	odtrhnout
od	od	k7c2	od
vlasti	vlast	k1gFnSc2	vlast
i	i	k8xC	i
od	od	k7c2	od
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středně	středně	k6eAd1	středně
velkém	velký	k2eAgNnSc6d1	velké
městečku	městečko	k1gNnSc6	městečko
byl	být	k5eAaImAgInS	být
nešťastný	šťastný	k2eNgMnSc1d1	nešťastný
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
si	se	k3xPyFc3	se
v	v	k7c6	v
Brixenu	Brixen	k1gInSc6	Brixen
pronajala	pronajmout	k5eAaPmAgFnS	pronajmout
domek	domek	k1gInSc4	domek
se	s	k7c7	s
zahradním	zahradní	k2eAgInSc7d1	zahradní
altánem	altán	k1gInSc7	altán
<g/>
,	,	kIx,	,
jídlo	jídlo	k1gNnSc4	jídlo
jim	on	k3xPp3gFnPc3	on
nosili	nosit	k5eAaImAgMnP	nosit
z	z	k7c2	z
hostince	hostinec	k1gInSc2	hostinec
U	u	k7c2	u
Slona	slon	k1gMnSc2	slon
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
však	však	k9	však
byl	být	k5eAaImAgMnS	být
Havlíček	Havlíček	k1gMnSc1	Havlíček
pod	pod	k7c7	pod
stálým	stálý	k2eAgInSc7d1	stálý
policejním	policejní	k2eAgInSc7d1	policejní
dozorem	dozor	k1gInSc7	dozor
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
sledována	sledovat	k5eAaImNgFnS	sledovat
veškerá	veškerý	k3xTgFnSc1	veškerý
jeho	jeho	k3xOp3gFnSc1	jeho
korespondence	korespondence	k1gFnSc1	korespondence
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
ho	on	k3xPp3gMnSc4	on
také	také	k6eAd1	také
trápilo	trápit	k5eAaImAgNnS	trápit
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
musí	muset	k5eAaImIp3nS	muset
žít	žít	k5eAaImF	žít
mimo	mimo	k7c4	mimo
svůj	svůj	k3xOyFgInSc4	svůj
domov	domov	k1gInSc4	domov
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
omezené	omezený	k2eAgInPc4d1	omezený
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1854	[number]	k4	1854
se	se	k3xPyFc4	se
manželka	manželka	k1gFnSc1	manželka
Julie	Julie	k1gFnSc1	Julie
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
vydaly	vydat	k5eAaPmAgFnP	vydat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Havlíček	Havlíček	k1gMnSc1	Havlíček
tak	tak	k9	tak
doufal	doufat	k5eAaImAgMnS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
spíše	spíše	k9	spíše
za	za	k7c7	za
rodinou	rodina	k1gFnSc7	rodina
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
pustí	pustit	k5eAaPmIp3nS	pustit
<g/>
.	.	kIx.	.
</s>
<s>
Úředníci	úředník	k1gMnPc1	úředník
však	však	k9	však
byli	být	k5eAaImAgMnP	být
neoblomní	oblomný	k2eNgMnPc1d1	neoblomný
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Havlíček	Havlíček	k1gMnSc1	Havlíček
podepsal	podepsat	k5eAaPmAgMnS	podepsat
protokol	protokol	k1gInSc4	protokol
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
zavázal	zavázat	k5eAaPmAgInS	zavázat
nepokračovat	pokračovat	k5eNaImF	pokračovat
ve	v	k7c6	v
veřejných	veřejný	k2eAgFnPc6d1	veřejná
aktivitách	aktivita	k1gFnPc6	aktivita
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
čtyřech	čtyři	k4xCgNnPc6	čtyři
letech	léto	k1gNnPc6	léto
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
propuštěn	propustit	k5eAaPmNgInS	propustit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
přijel	přijet	k5eAaPmAgInS	přijet
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1855	[number]	k4	1855
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cesta	k1gFnSc7	cesta
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
smutnou	smutný	k2eAgFnSc4d1	smutná
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Julie	Julie	k1gFnSc1	Julie
před	před	k7c7	před
měsícem	měsíc	k1gInSc7	měsíc
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1855	[number]	k4	1855
<g/>
,	,	kIx,	,
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
následky	následek	k1gInPc4	následek
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nemoc	nemoc	k1gFnSc1	nemoc
byla	být	k5eAaImAgFnS	být
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
zjištěna	zjistit	k5eAaPmNgFnS	zjistit
i	i	k8xC	i
u	u	k7c2	u
Havlíčka	Havlíček	k1gMnSc2	Havlíček
a	a	k8xC	a
nakažena	nakazit	k5eAaPmNgFnS	nakazit
byla	být	k5eAaImAgFnS	být
i	i	k9	i
jejich	jejich	k3xOp3gFnSc1	jejich
dcera	dcera	k1gFnSc1	dcera
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Havlíček	Havlíček	k1gMnSc1	Havlíček
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
Brixenu	Brixen	k1gInSc2	Brixen
vrátil	vrátit	k5eAaPmAgInS	vrátit
s	s	k7c7	s
téměř	téměř	k6eAd1	téměř
bílými	bílý	k2eAgInPc7d1	bílý
vlasy	vlas	k1gInPc7	vlas
<g/>
,	,	kIx,	,
po	po	k7c6	po
zprávě	zpráva	k1gFnSc6	zpráva
o	o	k7c4	o
úmrtí	úmrtí	k1gNnSc4	úmrtí
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
prý	prý	k9	prý
utekl	utéct	k5eAaPmAgInS	utéct
do	do	k7c2	do
polí	pole	k1gFnPc2	pole
a	a	k8xC	a
křičel	křičet	k5eAaImAgMnS	křičet
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
konec	konec	k1gInSc1	konec
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
se	se	k3xPyFc4	se
Havlíček	Havlíček	k1gMnSc1	Havlíček
ocitl	ocitnout	k5eAaPmAgMnS	ocitnout
bez	bez	k7c2	bez
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
bez	bez	k7c2	bez
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
svých	svůj	k3xOyFgInPc2	svůj
peněz	peníze	k1gInPc2	peníze
totiž	totiž	k9	totiž
půjčil	půjčit	k5eAaPmAgInS	půjčit
švagrovi	švagr	k1gMnSc3	švagr
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
investoval	investovat	k5eAaBmAgMnS	investovat
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
pokrývačské	pokrývačský	k2eAgFnSc2d1	pokrývačská
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
měl	mít	k5eAaImAgMnS	mít
Havlíček	Havlíček	k1gMnSc1	Havlíček
zakázaný	zakázaný	k2eAgInSc4d1	zakázaný
pobyt	pobyt	k1gInSc4	pobyt
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Německém	německý	k2eAgInSc6d1	německý
Brodě	Brod	k1gInSc6	Brod
u	u	k7c2	u
matky	matka	k1gFnSc2	matka
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
nechal	nechat	k5eAaPmAgMnS	nechat
u	u	k7c2	u
své	svůj	k3xOyFgFnSc2	svůj
švagrové	švagrová	k1gFnSc2	švagrová
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
přijet	přijet	k5eAaPmF	přijet
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
až	až	k8xS	až
získá	získat	k5eAaPmIp3nS	získat
nové	nový	k2eAgNnSc4d1	nové
stálé	stálý	k2eAgNnSc4d1	stálé
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Havlíček	Havlíček	k1gMnSc1	Havlíček
měl	mít	k5eAaImAgMnS	mít
úředně	úředně	k6eAd1	úředně
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
opouštět	opouštět	k5eAaImF	opouštět
Německý	německý	k2eAgInSc1d1	německý
Brod	Brod	k1gInSc1	Brod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
každou	každý	k3xTgFnSc4	každý
cestu	cesta	k1gFnSc4	cesta
musel	muset	k5eAaImAgMnS	muset
žádat	žádat	k5eAaImF	žádat
o	o	k7c4	o
povolení	povolení	k1gNnSc4	povolení
<g/>
.	.	kIx.	.
</s>
<s>
Zapovězenou	zapovězený	k2eAgFnSc4d1	zapovězená
měl	mít	k5eAaImAgInS	mít
především	především	k9	především
Prahu	Praha	k1gFnSc4	Praha
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
přestěhovat	přestěhovat	k5eAaPmF	přestěhovat
především	především	k9	především
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
blíže	blízce	k6eAd2	blízce
dceři	dcera	k1gFnSc3	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Pobyt	pobyt	k1gInSc1	pobyt
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
byl	být	k5eAaImAgInS	být
Havlíčkovi	Havlíček	k1gMnSc3	Havlíček
povolen	povolit	k5eAaPmNgInS	povolit
až	až	k9	až
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
byla	být	k5eAaImAgFnS	být
zjištěna	zjistit	k5eAaPmNgFnS	zjistit
jeho	jeho	k3xOp3gFnSc1	jeho
nevyléčitelná	vyléčitelný	k2eNgFnSc1d1	nevyléčitelná
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
si	se	k3xPyFc3	se
začal	začít	k5eAaPmAgInS	začít
stěžovat	stěžovat	k5eAaImF	stěžovat
na	na	k7c4	na
stálou	stálý	k2eAgFnSc4d1	stálá
únavu	únava	k1gFnSc4	únava
a	a	k8xC	a
kašel	kašel	k1gInSc4	kašel
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1856	[number]	k4	1856
dostal	dostat	k5eAaPmAgInS	dostat
chrlení	chrlení	k1gNnSc4	chrlení
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
poté	poté	k6eAd1	poté
doktoři	doktor	k1gMnPc1	doktor
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
pobytu	pobyt	k1gInSc6	pobyt
ve	v	k7c6	v
Šternberku	Šternberk	k1gInSc6	Šternberk
u	u	k7c2	u
Smečna	Smečno	k1gNnSc2	Smečno
na	na	k7c6	na
Kladensku	Kladensko	k1gNnSc6	Kladensko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
léčil	léčit	k5eAaImAgMnS	léčit
u	u	k7c2	u
místních	místní	k2eAgInPc2d1	místní
lázeňských	lázeňský	k2eAgInPc2d1	lázeňský
pramenů	pramen	k1gInPc2	pramen
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1856	[number]	k4	1856
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
horečkách	horečka	k1gFnPc6	horečka
kočárem	kočár	k1gInSc7	kočár
převezen	převézt	k5eAaPmNgMnS	převézt
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cesta	k1gFnSc7	cesta
blouznil	blouznit	k5eAaImAgInS	blouznit
a	a	k8xC	a
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
silou	síla	k1gFnSc7	síla
sebou	se	k3xPyFc7	se
vyčerpávajícím	vyčerpávající	k2eAgInSc7d1	vyčerpávající
způsobem	způsob	k1gInSc7	způsob
zmítal	zmítat	k5eAaImAgMnS	zmítat
<g/>
.	.	kIx.	.
</s>
<s>
Švagr	švagr	k1gMnSc1	švagr
František	František	k1gMnSc1	František
Jaroš	Jaroš	k1gMnSc1	Jaroš
nechal	nechat	k5eAaPmAgMnS	nechat
vykopnout	vykopnout	k5eAaPmF	vykopnout
okno	okno	k1gNnSc4	okno
kočáru	kočár	k1gInSc2	kočár
a	a	k8xC	a
až	až	k9	až
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Havlíček	Havlíček	k1gMnSc1	Havlíček
zklidnil	zklidnit	k5eAaPmAgMnS	zklidnit
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Jarošově	Jarošův	k2eAgInSc6d1	Jarošův
bytě	byt	k1gInSc6	byt
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
nedožitých	dožitý	k2eNgNnPc2d1	nedožité
35	[number]	k4	35
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
posteli	postel	k1gFnSc6	postel
jako	jako	k8xC	jako
o	o	k7c4	o
rok	rok	k1gInSc4	rok
dříve	dříve	k6eAd2	dříve
jeho	jeho	k3xOp3gFnSc1	jeho
milovaná	milovaný	k2eAgFnSc1d1	milovaná
žena	žena	k1gFnSc1	žena
Julie	Julie	k1gFnSc1	Julie
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
prý	prý	k9	prý
v	v	k7c6	v
agónii	agónie	k1gFnSc6	agónie
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
rukou	ruka	k1gFnPc6	ruka
jakoby	jakoby	k8xS	jakoby
horečně	horečně	k6eAd1	horečně
něco	něco	k3yInSc1	něco
psal	psát	k5eAaImAgMnS	psát
a	a	k8xC	a
obracel	obracet	k5eAaImAgMnS	obracet
listy	list	k1gInPc7	list
<g/>
.	.	kIx.	.
</s>
<s>
Pohřeb	pohřeb	k1gInSc4	pohřeb
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
sládek	sládek	k1gMnSc1	sládek
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Fingerhut-Náprstek	Fingerhut-Náprstek	k1gInSc1	Fingerhut-Náprstek
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
Vojty	Vojta	k1gMnSc2	Vojta
Náprstka	Náprstka	k1gFnSc1	Náprstka
a	a	k8xC	a
pořadatelství	pořadatelství	k1gNnSc4	pořadatelství
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
Josef	Josef	k1gMnSc1	Josef
Němec	Němec	k1gMnSc1	Němec
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Josefem	Josef	k1gMnSc7	Josef
Václavem	Václav	k1gMnSc7	Václav
Fričem	Frič	k1gMnSc7	Frič
<g/>
.	.	kIx.	.
</s>
<s>
Pohřbu	pohřeb	k1gInSc2	pohřeb
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
policie	policie	k1gFnSc1	policie
jmenovitě	jmenovitě	k6eAd1	jmenovitě
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
přítomnost	přítomnost	k1gFnSc1	přítomnost
např.	např.	kA	např.
Františka	František	k1gMnSc2	František
Palackého	Palacký	k1gMnSc2	Palacký
<g/>
,	,	kIx,	,
Františka	František	k1gMnSc2	František
Ladislava	Ladislav	k1gMnSc2	Ladislav
Riegra	Riegr	k1gMnSc2	Riegr
<g/>
,	,	kIx,	,
Václava	Václav	k1gMnSc2	Václav
Hanky	Hanka	k1gFnSc2	Hanka
nebo	nebo	k8xC	nebo
Karla	Karel	k1gMnSc2	Karel
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Erbena	Erben	k1gMnSc2	Erben
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Němec	Němec	k1gMnSc1	Němec
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
pohřbu	pohřeb	k1gInSc6	pohřeb
následně	následně	k6eAd1	následně
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
aktivitu	aktivita	k1gFnSc4	aktivita
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
k	k	k7c3	k
osmi	osm	k4xCc2	osm
dnům	den	k1gInPc3	den
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pohřbu	pohřeb	k1gInSc6	pohřeb
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
údajně	údajně	k6eAd1	údajně
položila	položit	k5eAaPmAgFnS	položit
na	na	k7c4	na
Havlíčkovu	Havlíčkův	k2eAgFnSc4d1	Havlíčkova
rakev	rakev	k1gFnSc4	rakev
trnovou	trnový	k2eAgFnSc4d1	Trnová
korunu	koruna	k1gFnSc4	koruna
jako	jako	k8xC	jako
symbol	symbol	k1gInSc4	symbol
mučednictví	mučednictví	k1gNnSc2	mučednictví
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vavřínový	vavřínový	k2eAgInSc4d1	vavřínový
věnec	věnec	k1gInSc4	věnec
dle	dle	k7c2	dle
zjištění	zjištění	k1gNnSc2	zjištění
V.	V.	kA	V.
Macury	Macura	k1gMnSc2	Macura
dal	dát	k5eAaPmAgMnS	dát
do	do	k7c2	do
hrobu	hrob	k1gInSc2	hrob
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Náprstek	náprstek	k1gInSc1	náprstek
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
L.	L.	kA	L.
Turnovský	turnovský	k2eAgMnSc1d1	turnovský
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
pohřbu	pohřeb	k1gInSc6	pohřeb
osobně	osobně	k6eAd1	osobně
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaBmAgMnS	napsat
(	(	kIx(	(
<g/>
str	str	kA	str
<g/>
.	.	kIx.	.
263	[number]	k4	263
<g/>
,	,	kIx,	,
citace	citace	k1gFnSc1	citace
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
O	o	k7c4	o
věnec	věnec	k1gInSc4	věnec
vavřínový	vavřínový	k2eAgInSc4d1	vavřínový
<g/>
,	,	kIx,	,
kterým	který	k3yRgInPc3	který
byla	být	k5eAaImAgFnS	být
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
rakev	rakev	k1gFnSc1	rakev
Havlíčkovu	Havlíčkův	k2eAgFnSc4d1	Havlíčkova
ozdobila	ozdobit	k5eAaPmAgFnS	ozdobit
<g/>
,	,	kIx,	,
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
se	se	k3xPyFc4	se
nejbližší	blízký	k2eAgMnPc1d3	nejbližší
účastníci	účastník	k1gMnPc1	účastník
pohřbu	pohřeb	k1gInSc2	pohřeb
<g/>
,	,	kIx,	,
vzavše	vzít	k5eAaPmDgFnP	vzít
si	se	k3xPyFc3	se
po	po	k7c6	po
lístku	lístek	k1gInSc6	lístek
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Havlíček	Havlíček	k1gMnSc1	Havlíček
<g/>
,	,	kIx,	,
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
rakousko-uherská	rakouskoherský	k2eAgFnSc1d1	rakousko-uherská
moc	moc	k1gFnSc1	moc
obávala	obávat	k5eAaImAgFnS	obávat
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
deportovala	deportovat	k5eAaBmAgFnS	deportovat
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
vlasti	vlast	k1gFnSc2	vlast
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nesporně	sporně	k6eNd1	sporně
obětí	oběť	k1gFnPc2	oběť
svých	svůj	k3xOyFgInPc2	svůj
odvážných	odvážný	k2eAgInPc2d1	odvážný
politických	politický	k2eAgInPc2d1	politický
názorů	názor	k1gInPc2	názor
<g/>
.	.	kIx.	.
</s>
<s>
Izolace	izolace	k1gFnPc1	izolace
od	od	k7c2	od
přátel	přítel	k1gMnPc2	přítel
a	a	k8xC	a
své	svůj	k3xOyFgFnSc2	svůj
vlasti	vlast	k1gFnSc2	vlast
v	v	k7c6	v
Brixenu	Brixen	k1gInSc6	Brixen
jej	on	k3xPp3gMnSc4	on
značně	značně	k6eAd1	značně
psychicky	psychicky	k6eAd1	psychicky
poznamenala	poznamenat	k5eAaPmAgFnS	poznamenat
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
strávenou	strávený	k2eAgFnSc4d1	strávená
v	v	k7c6	v
Brixenu	Brixen	k1gInSc6	Brixen
pobíral	pobírat	k5eAaImAgMnS	pobírat
Havlíček	Havlíček	k1gMnSc1	Havlíček
plat	plat	k1gInSc4	plat
vyššího	vysoký	k2eAgMnSc2d2	vyšší
úřednického	úřednický	k2eAgMnSc2d1	úřednický
zaměstnance	zaměstnanec	k1gMnSc2	zaměstnanec
(	(	kIx(	(
<g/>
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
jej	on	k3xPp3gNnSc4	on
vláda	vláda	k1gFnSc1	vláda
zbavila	zbavit	k5eAaPmAgFnS	zbavit
<g/>
)	)	kIx)	)
a	a	k8xC	a
obýval	obývat	k5eAaImAgMnS	obývat
druhý	druhý	k4xOgInSc4	druhý
nejdražší	drahý	k2eAgInSc4d3	nejdražší
dům	dům	k1gInSc4	dům
v	v	k7c6	v
Brixenu	Brixen	k1gInSc6	Brixen
nebo	nebo	k8xC	nebo
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemoc	nemoc	k1gFnSc1	nemoc
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
nezpůsobila	způsobit	k5eNaPmAgFnS	způsobit
údajná	údajný	k2eAgFnSc1d1	údajná
"	"	kIx"	"
<g/>
izolace	izolace	k1gFnSc1	izolace
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
"	"	kIx"	"
<g/>
jen	jen	k9	jen
<g/>
"	"	kIx"	"
obyčejné	obyčejný	k2eAgNnSc1d1	obyčejné
nachlazení	nachlazení	k1gNnSc1	nachlazení
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mnohdy	mnohdy	k6eAd1	mnohdy
neuvádí	uvádět	k5eNaImIp3nS	uvádět
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
jeho	jeho	k3xOp3gFnSc7	jeho
smrtí	smrt	k1gFnSc7	smrt
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
lidé	člověk	k1gMnPc1	člověk
vyhýbali	vyhýbat	k5eAaImAgMnP	vyhýbat
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
činnost	činnost	k1gFnSc4	činnost
odsuzovali	odsuzovat	k5eAaImAgMnP	odsuzovat
i	i	k9	i
v	v	k7c6	v
tisku	tisk	k1gInSc6	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Paradoxně	paradoxně	k6eAd1	paradoxně
již	již	k6eAd1	již
za	za	k7c4	za
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
národním	národní	k2eAgMnSc7d1	národní
mučedníkem	mučedník	k1gMnSc7	mučedník
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
osoba	osoba	k1gFnSc1	osoba
byla	být	k5eAaImAgFnS	být
uctívána	uctívat	k5eAaImNgFnS	uctívat
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k9	tak
kult	kult	k1gInSc1	kult
<g/>
,	,	kIx,	,
jakého	jaký	k3yQgMnSc4	jaký
jsme	být	k5eAaImIp1nP	být
v	v	k7c6	v
drobných	drobný	k2eAgFnPc6d1	drobná
obměnách	obměna	k1gFnPc6	obměna
byli	být	k5eAaImAgMnP	být
svědky	svědek	k1gMnPc4	svědek
i	i	k8xC	i
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Olšanských	olšanský	k2eAgInPc6d1	olšanský
hřbitovech	hřbitov	k1gInPc6	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
hrob	hrob	k1gInSc4	hrob
umístěn	umístěn	k2eAgInSc4d1	umístěn
QR	QR	kA	QR
kód	kód	k1gInSc1	kód
s	s	k7c7	s
odkazem	odkaz	k1gInSc7	odkaz
na	na	k7c4	na
informace	informace	k1gFnPc4	informace
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
Havlíčkova	Havlíčkův	k2eAgFnSc1d1	Havlíčkova
matka	matka	k1gFnSc1	matka
Josefína	Josefína	k1gFnSc1	Josefína
Havlíčková	Havlíčková	k1gFnSc1	Havlíčková
zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
Německém	německý	k2eAgInSc6d1	německý
Brodě	Brod	k1gInSc6	Brod
v	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1884	[number]	k4	1884
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
93	[number]	k4	93
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Epištoly	epištola	k1gFnPc1	epištola
kutnohorské	kutnohorský	k2eAgFnSc2d1	Kutnohorská
(	(	kIx(	(
<g/>
1850	[number]	k4	1850
<g/>
)	)	kIx)	)
–	–	k?	–
kritika	kritika	k1gFnSc1	kritika
církevní	církevní	k2eAgFnPc1d1	církevní
hierarchie	hierarchie	k1gFnPc1	hierarchie
sloužící	sloužící	k1gFnSc2	sloužící
nastupujícímu	nastupující	k2eAgInSc3d1	nastupující
neoabsolutismu	neoabsolutismus	k1gInSc3	neoabsolutismus
<g/>
.	.	kIx.	.
</s>
<s>
Obrazy	obraz	k1gInPc1	obraz
z	z	k7c2	z
Rus	Rus	k1gMnSc1	Rus
(	(	kIx(	(
<g/>
1843	[number]	k4	1843
<g/>
–	–	k?	–
<g/>
46	[number]	k4	46
<g/>
)	)	kIx)	)
–	–	k?	–
Cestopis	cestopis	k1gInSc1	cestopis
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
též	též	k9	též
první	první	k4xOgFnSc7	první
českou	český	k2eAgFnSc7d1	Česká
objektivní	objektivní	k2eAgFnSc7d1	objektivní
studií	studie	k1gFnSc7	studie
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Havlíček	Havlíček	k1gMnSc1	Havlíček
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
chválí	chválit	k5eAaImIp3nP	chválit
hlavně	hlavně	k9	hlavně
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
náboženství	náboženství	k1gNnSc3	náboženství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
"	"	kIx"	"
<g/>
proti	proti	k7c3	proti
srsti	srst	k1gFnSc3	srst
<g/>
"	"	kIx"	"
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
ruský	ruský	k2eAgInSc4d1	ruský
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
alkoholu	alkohol	k1gInSc3	alkohol
a	a	k8xC	a
také	také	k9	také
kontrast	kontrast	k1gInSc4	kontrast
v	v	k7c6	v
sociální	sociální	k2eAgFnSc6d1	sociální
otázce	otázka	k1gFnSc6	otázka
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
–	–	k?	–
bojaři	bojar	k1gMnPc1	bojar
kontra	kontra	k2eAgFnSc2d1	kontra
obyčejný	obyčejný	k2eAgInSc4d1	obyčejný
lid	lid	k1gInSc4	lid
<g/>
.	.	kIx.	.
</s>
<s>
Tyrolské	tyrolský	k2eAgFnPc1d1	tyrolská
elegie	elegie	k1gFnPc1	elegie
(	(	kIx(	(
<g/>
1852	[number]	k4	1852
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
–	–	k?	–
Název	název	k1gInSc1	název
elegie	elegie	k1gFnSc1	elegie
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
nářek	nářek	k1gInSc1	nářek
<g/>
,	,	kIx,	,
žalozpěv	žalozpěv	k1gInSc1	žalozpěv
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ironickou	ironický	k2eAgFnSc7d1	ironická
nadsázkou	nadsázka	k1gFnSc7	nadsázka
<g/>
;	;	kIx,	;
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
humorným	humorný	k2eAgInSc7d1	humorný
a	a	k8xC	a
satirickým	satirický	k2eAgInSc7d1	satirický
popisem	popis	k1gInSc7	popis
Havlíčkova	Havlíčkův	k2eAgNnSc2d1	Havlíčkovo
zatčení	zatčení	k1gNnSc2	zatčení
a	a	k8xC	a
internace	internace	k1gFnSc2	internace
v	v	k7c6	v
Brixenu	Brixen	k1gInSc6	Brixen
v	v	k7c6	v
Tyrolsku	Tyrolsko	k1gNnSc6	Tyrolsko
<g/>
.	.	kIx.	.
</s>
<s>
Kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
policii	policie	k1gFnSc4	policie
<g/>
,	,	kIx,	,
společenský	společenský	k2eAgInSc4d1	společenský
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
skutečných	skutečný	k2eAgNnPc2d1	skutečné
jmen	jméno	k1gNnPc2	jméno
a	a	k8xC	a
příhod	příhoda	k1gFnPc2	příhoda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
příhoda	příhoda	k1gFnSc1	příhoda
o	o	k7c4	o
zatčení	zatčení	k1gNnSc4	zatčení
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
splašili	splašit	k5eAaPmAgMnP	splašit
koně	kůň	k1gMnPc1	kůň
<g/>
,	,	kIx,	,
a	a	k8xC	a
Havlíček	Havlíček	k1gMnSc1	Havlíček
sám	sám	k3xTgMnSc1	sám
je	být	k5eAaImIp3nS	být
musel	muset	k5eAaImAgMnS	muset
krotit	krotit	k5eAaImF	krotit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
devíti	devět	k4xCc2	devět
zpěvů	zpěv	k1gInPc2	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
satirické	satirický	k2eAgInPc1d1	satirický
<g/>
,	,	kIx,	,
lyrické	lyrický	k2eAgInPc1d1	lyrický
a	a	k8xC	a
ironické	ironický	k2eAgInPc1d1	ironický
prvky	prvek	k1gInPc1	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
psáno	psát	k5eAaImNgNnS	psát
v	v	k7c6	v
pravidelných	pravidelný	k2eAgNnPc6d1	pravidelné
rýmovaných	rýmovaný	k2eAgNnPc6d1	rýmované
čtyřverších	čtyřverší	k1gNnPc6	čtyřverší
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Lávra	Lávra	k1gMnSc1	Lávra
(	(	kIx(	(
<g/>
1854	[number]	k4	1854
<g/>
)	)	kIx)	)
–	–	k?	–
Alegorická	alegorický	k2eAgFnSc1d1	alegorická
satirická	satirický	k2eAgFnSc1d1	satirická
skladba	skladba	k1gFnSc1	skladba
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
námětem	námět	k1gInSc7	námět
je	být	k5eAaImIp3nS	být
stará	starý	k2eAgFnSc1d1	stará
irská	irský	k2eAgFnSc1d1	irská
pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
králi	král	k1gMnSc6	král
s	s	k7c7	s
oslíma	oslí	k2eAgNnPc7d1	oslí
ušima	ucho	k1gNnPc7	ucho
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přizpůsobená	přizpůsobený	k2eAgFnSc1d1	přizpůsobená
českému	český	k2eAgNnSc3d1	české
prostředí	prostředí	k1gNnSc3	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Křest	křest	k1gInSc1	křest
svatého	svatý	k2eAgMnSc2d1	svatý
Vladimíra	Vladimír	k1gMnSc2	Vladimír
(	(	kIx(	(
<g/>
rukopis	rukopis	k1gInSc1	rukopis
<g/>
:	:	kIx,	:
1843	[number]	k4	1843
<g/>
–	–	k?	–
<g/>
1844	[number]	k4	1844
<g/>
,	,	kIx,	,
vydano	vydana	k1gFnSc5	vydana
1877	[number]	k4	1877
<g/>
,	,	kIx,	,
torzo	torzo	k1gNnSc1	torzo
<g/>
)	)	kIx)	)
–	–	k?	–
Příběh	příběh	k1gInSc1	příběh
boha	bůh	k1gMnSc2	bůh
Peruna	Perun	k1gMnSc2	Perun
(	(	kIx(	(
<g/>
slovanský	slovanský	k2eAgMnSc1d1	slovanský
bůh	bůh	k1gMnSc1	bůh
hromu	hrom	k1gInSc2	hrom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Car	car	k1gMnSc1	car
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
svátek	svátek	k1gInSc4	svátek
Perun	perun	k1gInSc1	perun
zahřměl	zahřmět	k5eAaPmAgInS	zahřmět
na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
<g/>
,	,	kIx,	,
Perun	perun	k1gInSc1	perun
to	ten	k3xDgNnSc1	ten
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
splnit	splnit	k5eAaPmF	splnit
<g/>
.	.	kIx.	.
</s>
<s>
Car	car	k1gMnSc1	car
dal	dát	k5eAaPmAgMnS	dát
Peruna	Perun	k1gMnSc4	Perun
chytit	chytit	k5eAaPmF	chytit
a	a	k8xC	a
utopit	utopit	k5eAaPmF	utopit
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
psáno	psát	k5eAaImNgNnS	psát
velmi	velmi	k6eAd1	velmi
čtivým	čtivý	k2eAgInSc7d1	čtivý
<g/>
,	,	kIx,	,
hovorovým	hovorový	k2eAgInSc7d1	hovorový
<g/>
,	,	kIx,	,
místy	místy	k6eAd1	místy
vulgárním	vulgární	k2eAgInSc7d1	vulgární
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Havlíček	Havlíček	k1gMnSc1	Havlíček
zde	zde	k6eAd1	zde
ironizuje	ironizovat	k5eAaImIp3nS	ironizovat
moc	moc	k1gFnSc4	moc
státního	státní	k2eAgInSc2d1	státní
aparátu	aparát	k1gInSc2	aparát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
může	moct	k5eAaImIp3nS	moct
poroučet	poroučet	k5eAaImF	poroučet
i	i	k8xC	i
bohu	bůh	k1gMnSc6	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Epigramy	epigram	k1gInPc1	epigram
(	(	kIx(	(
<g/>
1845	[number]	k4	1845
<g/>
)	)	kIx)	)
–	–	k?	–
krátké	krátký	k2eAgFnPc4d1	krátká
pointované	pointovaný	k2eAgFnPc4d1	pointovaná
veršované	veršovaný	k2eAgFnPc4d1	veršovaná
skladby	skladba	k1gFnPc4	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
kritiku	kritika	k1gFnSc4	kritika
<g/>
,	,	kIx,	,
parodii	parodie	k1gFnSc4	parodie
<g/>
,	,	kIx,	,
či	či	k8xC	či
satiru	satira	k1gFnSc4	satira
<g/>
.	.	kIx.	.
</s>
<s>
Havlíček	Havlíček	k1gMnSc1	Havlíček
je	on	k3xPp3gNnSc4	on
definuje	definovat	k5eAaBmIp3nS	definovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Epigramy	epigram	k1gInPc1	epigram
jsou	být	k5eAaImIp3nP	být
malinké	malinký	k2eAgFnPc1d1	malinká
nádoby	nádoba	k1gFnPc1	nádoba
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yIgFnPc2	který
vztek	vztek	k1gInSc1	vztek
svůj	svůj	k3xOyFgInSc4	svůj
nalévám	nalévat	k5eAaImIp1nS	nalévat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mi	já	k3xPp1nSc3	já
srdce	srdce	k1gNnSc1	srdce
nežral	žrát	k5eNaImAgInS	žrát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
o	o	k7c6	o
kritice	kritika	k1gFnSc6	kritika
–	–	k?	–
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
díle	díl	k1gInSc6	díl
Havlíček	Havlíček	k1gMnSc1	Havlíček
popisuje	popisovat	k5eAaImIp3nS	popisovat
hodnocení	hodnocení	k1gNnSc4	hodnocení
autorů	autor	k1gMnPc2	autor
a	a	k8xC	a
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ohrazoval	ohrazovat	k5eAaImAgMnS	ohrazovat
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
přílišnému	přílišný	k2eAgNnSc3d1	přílišné
vlastenčení	vlastenčení	k1gNnSc3	vlastenčení
bez	bez	k7c2	bez
činu	čin	k1gInSc2	čin
a	a	k8xC	a
"	"	kIx"	"
<g/>
hospodské	hospodský	k2eAgFnSc3d1	hospodská
<g/>
"	"	kIx"	"
kritice	kritika	k1gFnSc3	kritika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
český	český	k2eAgInSc4d1	český
národ	národ	k1gInSc4	národ
typická	typický	k2eAgFnSc1d1	typická
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
že	že	k8xS	že
autorů	autor	k1gMnPc2	autor
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k6eAd1	mnoho
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
nemá	mít	k5eNaImIp3nS	mít
talent	talent	k1gInSc1	talent
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
úspěch	úspěch	k1gInSc1	úspěch
prý	prý	k9	prý
tkvěl	tkvět	k5eAaImAgInS	tkvět
právě	právě	k6eAd1	právě
jen	jen	k9	jen
ve	v	k7c6	v
vlastenectví	vlastenectví	k1gNnSc6	vlastenectví
nikoli	nikoli	k9	nikoli
umělecké	umělecký	k2eAgFnSc6d1	umělecká
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
celé	celý	k2eAgNnSc1d1	celé
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
psáno	psát	k5eAaImNgNnS	psát
humornou	humorný	k2eAgFnSc7d1	humorná
formou	forma	k1gFnSc7	forma
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
dílem	dílo	k1gNnSc7	dílo
položil	položit	k5eAaPmAgInS	položit
základy	základ	k1gInPc4	základ
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgFnSc4d1	budoucí
literární	literární	k2eAgFnSc4d1	literární
kritiku	kritika	k1gFnSc4	kritika
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
literatuře	literatura	k1gFnSc6	literatura
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
–	–	k?	–
knižní	knižní	k2eAgInSc4d1	knižní
soubor	soubor	k1gInSc4	soubor
původně	původně	k6eAd1	původně
časopisecky	časopisecky	k6eAd1	časopisecky
zveřejněných	zveřejněný	k2eAgInPc2d1	zveřejněný
článků	článek	k1gInPc2	článek
Karel	Karel	k1gMnSc1	Karel
Havlíček	Havlíček	k1gMnSc1	Havlíček
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
české	český	k2eAgFnSc2d1	Česká
žurnalistiky	žurnalistika	k1gFnSc2	žurnalistika
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
i	i	k9	i
za	za	k7c2	za
jejího	její	k3xOp3gMnSc2	její
zakladatele	zakladatel	k1gMnSc2	zakladatel
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
bychom	by	kYmCp1nP	by
jej	on	k3xPp3gMnSc4	on
označili	označit	k5eAaPmAgMnP	označit
za	za	k7c4	za
investigativního	investigativní	k2eAgMnSc4d1	investigativní
reportéra	reportér	k1gMnSc4	reportér
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
článcích	článek	k1gInPc6	článek
nejvíce	hodně	k6eAd3	hodně
kritický	kritický	k2eAgMnSc1d1	kritický
k	k	k7c3	k
tehdejší	tehdejší	k2eAgFnSc3d1	tehdejší
absolutistické	absolutistický	k2eAgFnSc3d1	absolutistická
vládě	vláda	k1gFnSc3	vláda
<g/>
,	,	kIx,	,
hledal	hledat	k5eAaImAgMnS	hledat
její	její	k3xOp3gFnPc4	její
slabiny	slabina	k1gFnPc4	slabina
a	a	k8xC	a
ty	ten	k3xDgFnPc4	ten
pak	pak	k6eAd1	pak
zprostředkovával	zprostředkovávat	k5eAaImAgMnS	zprostředkovávat
lidem	lid	k1gInSc7	lid
<g/>
.	.	kIx.	.
</s>
<s>
Havlíček	Havlíček	k1gMnSc1	Havlíček
jako	jako	k8xS	jako
statečný	statečný	k2eAgMnSc1d1	statečný
a	a	k8xC	a
čestný	čestný	k2eAgMnSc1d1	čestný
člověk	člověk	k1gMnSc1	člověk
byl	být	k5eAaImAgMnS	být
několika	několik	k4yIc7	několik
politickými	politický	k2eAgMnPc7d1	politický
režimy	režim	k1gInPc4	režim
nenáviděn	nenáviděn	k2eAgMnSc1d1	nenáviděn
(	(	kIx(	(
<g/>
rakouskouherský	rakouskouherský	k2eAgMnSc1d1	rakouskouherský
císař	císař	k1gMnSc1	císař
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
měl	mít	k5eAaImAgInS	mít
takový	takový	k3xDgInSc4	takový
strach	strach	k1gInSc4	strach
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
nechal	nechat	k5eAaPmAgMnS	nechat
deportovat	deportovat	k5eAaBmF	deportovat
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obyčejnými	obyčejný	k2eAgMnPc7d1	obyčejný
lidmi	člověk	k1gMnPc7	člověk
uznáván	uznáván	k2eAgInSc1d1	uznáván
a	a	k8xC	a
respektován	respektován	k2eAgInSc1d1	respektován
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgInS	působit
v	v	k7c6	v
Pražských	pražský	k2eAgFnPc6d1	Pražská
novinách	novina	k1gFnPc6	novina
a	a	k8xC	a
v	v	k7c6	v
příloze	příloha	k1gFnSc6	příloha
Česká	český	k2eAgFnSc1d1	Česká
včela	včela	k1gFnSc1	včela
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Národních	národní	k2eAgFnPc6d1	národní
novinách	novina	k1gFnPc6	novina
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
sám	sám	k3xTgMnSc1	sám
založil	založit	k5eAaPmAgInS	založit
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
také	také	k9	také
ve	v	k7c6	v
vlastním	vlastní	k2eAgInSc6d1	vlastní
časopise	časopis	k1gInSc6	časopis
Slovan	Slovan	k1gInSc1	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
svým	svůj	k3xOyFgInPc3	svůj
článkům	článek	k1gInPc3	článek
měl	mít	k5eAaImAgMnS	mít
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vždy	vždy	k6eAd1	vždy
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
své	svůj	k3xOyFgInPc4	svůj
články	článek	k1gInPc4	článek
dokázal	dokázat	k5eAaPmAgMnS	dokázat
obhájit	obhájit	k5eAaPmF	obhájit
<g/>
.	.	kIx.	.
</s>
<s>
Pražské	pražský	k2eAgFnSc2d1	Pražská
noviny	novina	k1gFnSc2	novina
–	–	k?	–
V	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
době	doba	k1gFnSc6	doba
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
jediný	jediný	k2eAgInSc1d1	jediný
česky	česky	k6eAd1	česky
psaný	psaný	k2eAgInSc1d1	psaný
list	list	k1gInSc1	list
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
příloze	příloha	k1gFnSc6	příloha
Česká	český	k2eAgFnSc1d1	Česká
včela	včela	k1gFnSc1	včela
vyšel	vyjít	k5eAaPmAgInS	vyjít
i	i	k9	i
článek	článek	k1gInSc1	článek
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
Havlíček	Havlíček	k1gMnSc1	Havlíček
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
Josefa	Josef	k1gMnSc4	Josef
Kajetána	Kajetán	k1gMnSc4	Kajetán
Tyla	Tyl	k1gMnSc4	Tyl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Tylovi	Tyl	k1gMnSc6	Tyl
mu	on	k3xPp3gMnSc3	on
vadilo	vadit	k5eAaImAgNnS	vadit
také	také	k9	také
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlastenectví	vlastenectví	k1gNnSc4	vlastenectví
uznával	uznávat	k5eAaImAgInS	uznávat
jako	jako	k9	jako
prakticky	prakticky	k6eAd1	prakticky
jediné	jediný	k2eAgNnSc1d1	jediné
téma	téma	k1gNnSc1	téma
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
těchto	tento	k3xDgFnPc2	tento
novin	novina	k1gFnPc2	novina
Havlíček	Havlíček	k1gMnSc1	Havlíček
psal	psát	k5eAaImAgMnS	psát
také	také	k6eAd1	také
politické	politický	k2eAgInPc4d1	politický
články	článek	k1gInPc4	článek
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc2d1	národní
noviny	novina	k1gFnSc2	novina
–	–	k?	–
Po	po	k7c6	po
práci	práce	k1gFnSc6	práce
v	v	k7c6	v
Pražských	pražský	k2eAgFnPc6d1	Pražská
novinách	novina	k1gFnPc6	novina
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
stal	stát	k5eAaPmAgInS	stát
provládní	provládní	k2eAgInSc1d1	provládní
list	list	k1gInSc1	list
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Havlíček	Havlíček	k1gMnSc1	Havlíček
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
založit	založit	k5eAaPmF	založit
list	list	k1gInSc4	list
nový	nový	k2eAgInSc4d1	nový
a	a	k8xC	a
rovnou	rovnou	k6eAd1	rovnou
deník	deník	k1gInSc1	deník
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
zde	zde	k6eAd1	zde
i	i	k9	i
satirickou	satirický	k2eAgFnSc4d1	satirická
rubriku	rubrika	k1gFnSc4	rubrika
Šotek	šotek	k1gMnSc1	šotek
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgInS	snažit
se	se	k3xPyFc4	se
noviny	novina	k1gFnSc2	novina
co	co	k3yInSc4	co
nejvíce	hodně	k6eAd3	hodně
politizovat	politizovat	k5eAaImF	politizovat
a	a	k8xC	a
vést	vést	k5eAaImF	vést
čtenáře	čtenář	k1gMnPc4	čtenář
k	k	k7c3	k
národnímu	národní	k2eAgNnSc3d1	národní
uvědomění	uvědomění	k1gNnSc3	uvědomění
a	a	k8xC	a
češtině	čeština	k1gFnSc3	čeština
<g/>
,	,	kIx,	,
vysvětloval	vysvětlovat	k5eAaImAgMnS	vysvětlovat
zde	zde	k6eAd1	zde
například	například	k6eAd1	například
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
politickými	politický	k2eAgInPc7d1	politický
systémy	systém	k1gInPc7	systém
<g/>
,	,	kIx,	,
v	v	k7c6	v
čem	co	k3yRnSc6	co
spočívají	spočívat	k5eAaImIp3nP	spočívat
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
politické	politický	k2eAgFnPc1d1	politická
funkce	funkce	k1gFnPc1	funkce
a	a	k8xC	a
také	také	k9	také
zde	zde	k6eAd1	zde
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
absolutismus	absolutismus	k1gInSc4	absolutismus
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
nakonec	nakonec	k6eAd1	nakonec
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
zákazu	zákaz	k1gInSc3	zákaz
<g/>
.	.	kIx.	.
</s>
<s>
Slovan	Slovan	k1gMnSc1	Slovan
–	–	k?	–
Časopis	časopis	k1gInSc1	časopis
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
snažil	snažit	k5eAaImAgMnS	snažit
o	o	k7c4	o
politickou	politický	k2eAgFnSc4d1	politická
osvětu	osvěta	k1gFnSc4	osvěta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
definitivně	definitivně	k6eAd1	definitivně
nevyplatilo	vyplatit	k5eNaPmAgNnS	vyplatit
–	–	k?	–
byl	být	k5eAaImAgInS	být
policií	policie	k1gFnSc7	policie
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
monarchie	monarchie	k1gFnSc2	monarchie
zajat	zajmout	k5eAaPmNgInS	zajmout
ve	v	k7c6	v
vlastním	vlastní	k2eAgInSc6d1	vlastní
bytě	byt	k1gInSc6	byt
a	a	k8xC	a
deportován	deportovat	k5eAaBmNgMnS	deportovat
do	do	k7c2	do
Brixenu	Brixen	k1gInSc2	Brixen
<g/>
.	.	kIx.	.
</s>
<s>
Duch	duch	k1gMnSc1	duch
Národních	národní	k2eAgFnPc2d1	národní
novin	novina	k1gFnPc2	novina
(	(	kIx(	(
<g/>
1851	[number]	k4	1851
<g/>
)	)	kIx)	)
–	–	k?	–
výbor	výbor	k1gInSc1	výbor
novinových	novinový	k2eAgInPc2d1	novinový
článků	článek	k1gInPc2	článek
z	z	k7c2	z
období	období	k1gNnSc2	období
působení	působení	k1gNnSc2	působení
K.	K.	kA	K.
Havlíčka	Havlíček	k1gMnSc4	Havlíček
Borovského	Borovský	k1gMnSc4	Borovský
v	v	k7c6	v
Národních	národní	k2eAgFnPc6d1	národní
novinách	novina	k1gFnPc6	novina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
ekonomii	ekonomie	k1gFnSc4	ekonomie
zastával	zastávat	k5eAaImAgMnS	zastávat
Karel	Karel	k1gMnSc1	Karel
Havlíček	Havlíček	k1gMnSc1	Havlíček
Borovský	Borovský	k1gMnSc1	Borovský
pozici	pozice	k1gFnSc4	pozice
klasického	klasický	k2eAgInSc2d1	klasický
liberalismu	liberalismus	k1gInSc2	liberalismus
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
svobodný	svobodný	k2eAgInSc4d1	svobodný
obchod	obchod	k1gInSc4	obchod
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
clům	clo	k1gNnPc3	clo
a	a	k8xC	a
odmítal	odmítat	k5eAaImAgMnS	odmítat
papírové	papírový	k2eAgInPc4d1	papírový
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
poukazoval	poukazovat	k5eAaImAgInS	poukazovat
na	na	k7c4	na
hrozbu	hrozba	k1gFnSc4	hrozba
komunismu	komunismus	k1gInSc2	komunismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
Havlíček	Havlíček	k1gMnSc1	Havlíček
navštívil	navštívit	k5eAaPmAgMnS	navštívit
litoměřický	litoměřický	k2eAgInSc4d1	litoměřický
biskupský	biskupský	k2eAgInSc4d1	biskupský
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zhrozil	zhrozit	k5eAaPmAgMnS	zhrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
nijak	nijak	k6eAd1	nijak
označen	označit	k5eAaPmNgInS	označit
hrob	hrob	k1gInSc1	hrob
Karla	Karel	k1gMnSc2	Karel
Hynka	Hynek	k1gMnSc2	Hynek
Máchy	Mácha	k1gMnSc2	Mácha
<g/>
.	.	kIx.	.
</s>
<s>
Zasadil	zasadit	k5eAaPmAgMnS	zasadit
se	se	k3xPyFc4	se
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
náhrobku	náhrobek	k1gInSc2	náhrobek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
Vyšehradě	Vyšehrad	k1gInSc6	Vyšehrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Karel	Karel	k1gMnSc1	Karel
Havlíček	Havlíček	k1gMnSc1	Havlíček
a	a	k8xC	a
zasadil	zasadit	k5eAaPmAgMnS	zasadit
se	se	k3xPyFc4	se
o	o	k7c6	o
přejmenování	přejmenování	k1gNnSc6	přejmenování
pražského	pražský	k2eAgInSc2d1	pražský
Koňského	koňský	k2eAgInSc2d1	koňský
trhu	trh	k1gInSc2	trh
na	na	k7c4	na
Václavské	václavský	k2eAgNnSc4d1	Václavské
náměstí	náměstí	k1gNnSc4	náměstí
a	a	k8xC	a
také	také	k9	také
Dobytčího	dobytčí	k2eAgInSc2d1	dobytčí
trhu	trh	k1gInSc2	trh
na	na	k7c4	na
Karlovo	Karlův	k2eAgNnSc4d1	Karlovo
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Havlíček	Havlíček	k1gMnSc1	Havlíček
se	se	k3xPyFc4	se
neúspěšně	úspěšně	k6eNd1	úspěšně
zasazoval	zasazovat	k5eAaImAgMnS	zasazovat
o	o	k7c6	o
zřízení	zřízení	k1gNnSc6	zřízení
první	první	k4xOgFnSc2	první
české	český	k2eAgFnSc2d1	Česká
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
