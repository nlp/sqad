<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
vlasti	vlast	k1gFnSc3	vlast
nebo	nebo	k8xC	nebo
k	k	k7c3	k
určitému	určitý	k2eAgNnSc3d1	určité
zeměpisnému	zeměpisný	k2eAgNnSc3d1	zeměpisné
<g/>
,	,	kIx,	,
společenskému	společenský	k2eAgNnSc3d1	společenské
<g/>
,	,	kIx,	,
ekonomickému	ekonomický	k2eAgNnSc3d1	ekonomické
<g/>
,	,	kIx,	,
politickému	politický	k2eAgNnSc3d1	politické
a	a	k8xC	a
kulturnímu	kulturní	k2eAgNnSc3d1	kulturní
prostředí	prostředí	k1gNnSc3	prostředí
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
národ	národ	k1gInSc1	národ
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
?	?	kIx.	?
</s>
