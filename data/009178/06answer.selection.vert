<s>
Orff	Orff	k1gMnSc1	Orff
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
do	do	k7c2	do
muzikálního	muzikální	k2eAgNnSc2d1	muzikální
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
a	a	k8xC	a
mládí	mládí	k1gNnSc6	mládí
hrál	hrát	k5eAaImAgInS	hrát
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
varhany	varhany	k1gInPc4	varhany
a	a	k8xC	a
violoncello	violoncello	k1gNnSc4	violoncello
<g/>
,	,	kIx,	,
zajímal	zajímat	k5eAaImAgMnS	zajímat
se	se	k3xPyFc4	se
však	však	k9	však
také	také	k9	také
o	o	k7c4	o
botaniku	botanika	k1gFnSc4	botanika
<g/>
,	,	kIx,	,
loutkové	loutkový	k2eAgNnSc4d1	loutkové
divadlo	divadlo	k1gNnSc4	divadlo
a	a	k8xC	a
činohru	činohra	k1gFnSc4	činohra
<g/>
.	.	kIx.	.
</s>
