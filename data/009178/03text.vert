<p>
<s>
Carl	Carl	k1gMnSc1	Carl
Heinrich	Heinrich	k1gMnSc1	Heinrich
Maria	Mario	k1gMnSc2	Mario
Orff	Orff	k1gMnSc1	Orff
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
Mnichov	Mnichov	k1gInSc1	Mnichov
−	−	k?	−
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
skladatel	skladatel	k1gMnSc1	skladatel
expresivní	expresivní	k2eAgFnSc2d1	expresivní
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
pojmenován	pojmenován	k2eAgMnSc1d1	pojmenován
instrumentář	instrumentář	k1gMnSc1	instrumentář
tzv.	tzv.	kA	tzv.
Orffových	Orffův	k2eAgInPc2d1	Orffův
nástrojů	nástroj	k1gInPc2	nástroj
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
hudební	hudební	k2eAgFnSc2d1	hudební
výchovy	výchova	k1gFnSc2	výchova
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
Orffovým	Orffův	k2eAgInSc7d1	Orffův
dílem	díl	k1gInSc7	díl
je	být	k5eAaImIp3nS	být
Carmina	Carmin	k2eAgMnSc4d1	Carmin
Burana	buran	k1gMnSc4	buran
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
nejznámější	známý	k2eAgFnSc7d3	nejznámější
skladbou	skladba	k1gFnSc7	skladba
tohoto	tento	k3xDgInSc2	tento
názvu	název	k1gInSc2	název
<g/>
;	;	kIx,	;
slávu	sláva	k1gFnSc4	sláva
si	se	k3xPyFc3	se
získala	získat	k5eAaPmAgFnS	získat
zejména	zejména	k9	zejména
první	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
pedagog	pedagog	k1gMnSc1	pedagog
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
spojení	spojení	k1gNnSc4	spojení
hudební	hudební	k2eAgFnSc2d1	hudební
výchovy	výchova	k1gFnSc2	výchova
s	s	k7c7	s
výchovou	výchova	k1gFnSc7	výchova
pohybovou	pohybový	k2eAgFnSc7d1	pohybová
<g/>
,	,	kIx,	,
hudby	hudba	k1gFnSc2	hudba
s	s	k7c7	s
pohybem	pohyb	k1gInSc7	pohyb
a	a	k8xC	a
akcí	akce	k1gFnSc7	akce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Orff	Orff	k1gMnSc1	Orff
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
do	do	k7c2	do
muzikálního	muzikální	k2eAgNnSc2d1	muzikální
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
a	a	k8xC	a
mládí	mládí	k1gNnSc6	mládí
hrál	hrát	k5eAaImAgInS	hrát
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
varhany	varhany	k1gInPc4	varhany
a	a	k8xC	a
violoncello	violoncello	k1gNnSc4	violoncello
<g/>
,	,	kIx,	,
zajímal	zajímat	k5eAaImAgMnS	zajímat
se	se	k3xPyFc4	se
však	však	k9	však
také	také	k9	také
o	o	k7c4	o
botaniku	botanika	k1gFnSc4	botanika
<g/>
,	,	kIx,	,
loutkové	loutkový	k2eAgNnSc4d1	loutkové
divadlo	divadlo	k1gNnSc4	divadlo
a	a	k8xC	a
činohru	činohra	k1gFnSc4	činohra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ředitelem	ředitel	k1gMnSc7	ředitel
prvního	první	k4xOgInSc2	první
vzdělávacího	vzdělávací	k2eAgInSc2d1	vzdělávací
ústavu	ústav	k1gInSc2	ústav
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gFnSc2	jeho
metody	metoda	k1gFnSc2	metoda
spojené	spojený	k2eAgFnSc2d1	spojená
hudební	hudební	k2eAgFnSc2d1	hudební
a	a	k8xC	a
pohybové	pohybový	k2eAgFnSc2d1	pohybová
výchovy	výchova	k1gFnSc2	výchova
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Orff	Orff	k1gInSc1	Orff
Schulwerk	Schulwerk	k1gInSc1	Schulwerk
<g/>
)	)	kIx)	)
–	–	k?	–
Guentherovy	Guentherův	k2eAgFnSc2d1	Guentherův
školy	škola	k1gFnSc2	škola
gymnastiky	gymnastika	k1gFnSc2	gymnastika
<g/>
,	,	kIx,	,
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
tance	tanec	k1gInSc2	tanec
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
období	období	k1gNnSc2	období
nacismu	nacismus	k1gInSc2	nacismus
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
měl	mít	k5eAaImAgInS	mít
konflikty	konflikt	k1gInPc4	konflikt
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
ideologií	ideologie	k1gFnSc7	ideologie
a	a	k8xC	a
prožíval	prožívat	k5eAaImAgMnS	prožívat
těžké	těžký	k2eAgNnSc4d1	těžké
životní	životní	k2eAgNnSc4d1	životní
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
napsal	napsat	k5eAaBmAgInS	napsat
své	svůj	k3xOyFgNnSc4	svůj
nejslavnější	slavný	k2eAgNnSc4d3	nejslavnější
dílo	dílo	k1gNnSc4	dílo
–	–	k?	–
Carmina	Carmin	k2eAgMnSc4d1	Carmin
Burana	buran	k1gMnSc4	buran
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
dvě	dva	k4xCgNnPc4	dva
scénická	scénický	k2eAgNnPc4d1	scénické
ztvárnění	ztvárnění	k1gNnPc4	ztvárnění
pohádek	pohádka	k1gFnPc2	pohádka
bratří	bratr	k1gMnPc2	bratr
Grimmů	Grimm	k1gInPc2	Grimm
–	–	k?	–
Der	drát	k5eAaImRp2nS	drát
Mond	Mond	k1gMnSc1	Mond
a	a	k8xC	a
Die	Die	k1gMnSc1	Die
Kluge	Klug	k1gFnSc2	Klug
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
televizních	televizní	k2eAgInPc2d1	televizní
pořadů	pořad	k1gInPc2	pořad
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
Salzburku	Salzburk	k1gInSc6	Salzburk
centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
pedagogů	pedagog	k1gMnPc2	pedagog
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
učil	učít	k5eAaPmAgMnS	učít
svoji	svůj	k3xOyFgFnSc4	svůj
metodu	metoda	k1gFnSc4	metoda
Orff	Orff	k1gInSc1	Orff
Schulwerk	Schulwerk	k1gInSc4	Schulwerk
(	(	kIx(	(
<g/>
toto	tento	k3xDgNnSc1	tento
centrum	centrum	k1gNnSc1	centrum
působí	působit	k5eAaImIp3nS	působit
dodnes	dodnes	k6eAd1	dodnes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Carl	Carl	k1gMnSc1	Carl
Orff	Orff	k1gMnSc1	Orff
psal	psát	k5eAaImAgMnS	psát
tzv.	tzv.	kA	tzv.
scénické	scénický	k2eAgFnPc4d1	scénická
kompozice	kompozice	k1gFnPc4	kompozice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
lišily	lišit	k5eAaImAgFnP	lišit
od	od	k7c2	od
klasických	klasický	k2eAgFnPc2d1	klasická
hudebních	hudební	k2eAgFnPc2d1	hudební
forem	forma	k1gFnPc2	forma
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
opera	opera	k1gFnSc1	opera
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
kompozice	kompozice	k1gFnPc4	kompozice
nazýval	nazývat	k5eAaImAgMnS	nazývat
hrami	hra	k1gFnPc7	hra
<g/>
,	,	kIx,	,
tragédiemi	tragédie	k1gFnPc7	tragédie
<g/>
,	,	kIx,	,
komediemi	komedie	k1gFnPc7	komedie
či	či	k8xC	či
divadlem	divadlo	k1gNnSc7	divadlo
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dílech	díl	k1gInPc6	díl
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
k	k	k7c3	k
primitivnímu	primitivní	k2eAgNnSc3d1	primitivní
spojení	spojení	k1gNnSc3	spojení
melodie	melodie	k1gFnSc2	melodie
a	a	k8xC	a
rytmu	rytmus	k1gInSc2	rytmus
a	a	k8xC	a
spojuje	spojovat	k5eAaImIp3nS	spojovat
hudbu	hudba	k1gFnSc4	hudba
s	s	k7c7	s
jevištním	jevištní	k2eAgInSc7d1	jevištní
projevem	projev	k1gInSc7	projev
a	a	k8xC	a
jazykem	jazyk	k1gInSc7	jazyk
<g/>
;	;	kIx,	;
hodně	hodně	k6eAd1	hodně
používá	používat	k5eAaImIp3nS	používat
bicí	bicí	k2eAgInPc4d1	bicí
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Svá	svůj	k3xOyFgNnPc4	svůj
tři	tři	k4xCgNnPc4	tři
nejslavnější	slavný	k2eAgNnPc4d3	nejslavnější
díla	dílo	k1gNnPc4	dílo
Carmina	Carmin	k2eAgMnSc4d1	Carmin
Burana	buran	k1gMnSc4	buran
<g/>
,	,	kIx,	,
Catulli	Catulle	k1gFnSc4	Catulle
Carmina	Carmin	k2eAgMnSc2d1	Carmin
a	a	k8xC	a
Trionfo	Trionfo	k6eAd1	Trionfo
di	di	k?	di
Afrodite	Afrodit	k1gInSc5	Afrodit
spojil	spojit	k5eAaPmAgMnS	spojit
do	do	k7c2	do
jediného	jediný	k2eAgInSc2d1	jediný
jevištního	jevištní	k2eAgInSc2d1	jevištní
celku	celek	k1gInSc2	celek
Trionfi	Trionf	k1gFnSc2	Trionf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Carmina	Carmin	k2eAgMnSc4d1	Carmin
Burana	buran	k1gMnSc4	buran
===	===	k?	===
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
skladba	skladba	k1gFnSc1	skladba
Carmina	Carmin	k2eAgMnSc2d1	Carmin
Burana	buran	k1gMnSc2	buran
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
.	.	kIx.	.
</s>
<s>
Okamžitě	okamžitě	k6eAd1	okamžitě
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
obrovského	obrovský	k2eAgInSc2d1	obrovský
úspěchu	úspěch	k1gInSc2	úspěch
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc4d1	světový
věhlas	věhlas	k1gInSc4	věhlas
si	se	k3xPyFc3	se
získala	získat	k5eAaPmAgFnS	získat
až	až	k6eAd1	až
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obsahem	obsah	k1gInSc7	obsah
celé	celý	k2eAgFnSc2d1	celá
skladby	skladba	k1gFnSc2	skladba
jsou	být	k5eAaImIp3nP	být
zhudebněné	zhudebněný	k2eAgInPc1d1	zhudebněný
texty	text	k1gInPc1	text
potulných	potulný	k2eAgMnPc2d1	potulný
středověkých	středověký	k2eAgMnPc2d1	středověký
hudebníků	hudebník	k1gMnPc2	hudebník
a	a	k8xC	a
básníků	básník	k1gMnPc2	básník
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
nalezeny	naleznout	k5eAaPmNgFnP	naleznout
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
Beurum	Beurum	k1gNnSc1	Beurum
(	(	kIx(	(
<g/>
Buranum	Buranum	k?	Buranum
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
pochází	pocházet	k5eAaImIp3nS	pocházet
název	název	k1gInSc1	název
těchto	tento	k3xDgInPc2	tento
textů	text	k1gInPc2	text
–	–	k?	–
Carmina	Carmin	k2eAgMnSc2d1	Carmin
Burana	buran	k1gMnSc2	buran
<g/>
,	,	kIx,	,
i	i	k8xC	i
celé	celý	k2eAgFnPc1d1	celá
skladby	skladba	k1gFnPc1	skladba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInPc1d1	původní
texty	text	k1gInPc1	text
jsou	být	k5eAaImIp3nP	být
psány	psát	k5eAaImNgInP	psát
v	v	k7c6	v
nejrůznějších	různý	k2eAgInPc6d3	nejrůznější
jazycích	jazyk	k1gInPc6	jazyk
–	–	k?	–
staroněmecky	staroněmecky	k6eAd1	staroněmecky
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
<g/>
,	,	kIx,	,
starofrancouzsky	starofrancouzsky	k6eAd1	starofrancouzsky
a	a	k8xC	a
italsky	italsky	k6eAd1	italsky
<g/>
.	.	kIx.	.
</s>
<s>
Carl	Carl	k1gMnSc1	Carl
Orff	Orff	k1gMnSc1	Orff
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
skladbu	skladba	k1gFnSc4	skladba
vybral	vybrat	k5eAaPmAgInS	vybrat
pouze	pouze	k6eAd1	pouze
některé	některý	k3yIgInPc4	některý
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvou	dva	k4xCgNnPc2	dva
set	sto	k4xCgNnPc2	sto
původních	původní	k2eAgMnPc2d1	původní
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Forma	forma	k1gFnSc1	forma
skladby	skladba	k1gFnSc2	skladba
nejvíce	nejvíce	k6eAd1	nejvíce
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
kantátě	kantáta	k1gFnSc3	kantáta
–	–	k?	–
cyklické	cyklický	k2eAgFnSc3d1	cyklická
skladbě	skladba	k1gFnSc3	skladba
pro	pro	k7c4	pro
sólové	sólový	k2eAgInPc4d1	sólový
hlasy	hlas	k1gInPc4	hlas
(	(	kIx(	(
<g/>
soprán	soprán	k1gInSc1	soprán
<g/>
,	,	kIx,	,
tenor	tenor	k1gInSc1	tenor
a	a	k8xC	a
baryton	baryton	k1gInSc1	baryton
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
dětský	dětský	k2eAgInSc1d1	dětský
sbor	sbor	k1gInSc1	sbor
a	a	k8xC	a
orchestr	orchestr	k1gInSc1	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
do	do	k7c2	do
šesti	šest	k4xCc2	šest
tematických	tematický	k2eAgInPc2d1	tematický
celků	celek	k1gInPc2	celek
(	(	kIx(	(
<g/>
písně	píseň	k1gFnPc1	píseň
milostné	milostný	k2eAgFnPc1d1	milostná
<g/>
,	,	kIx,	,
pijácké	pijácký	k2eAgFnPc1d1	pijácká
<g/>
,	,	kIx,	,
o	o	k7c6	o
jaru	jaro	k1gNnSc6	jaro
<g/>
,	,	kIx,	,
štěstěně	štěstěna	k1gFnSc6	štěstěna
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Provedení	provedení	k1gNnSc1	provedení
si	se	k3xPyFc3	se
žádá	žádat	k5eAaImIp3nS	žádat
nadstandardní	nadstandardní	k2eAgNnSc4d1	nadstandardní
obsazení	obsazení	k1gNnSc4	obsazení
v	v	k7c6	v
sekci	sekce	k1gFnSc6	sekce
bicích	bicí	k2eAgInPc2d1	bicí
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většinou	k6eAd1	většinou
bývá	bývat	k5eAaImIp3nS	bývat
prováděna	provádět	k5eAaImNgFnS	provádět
koncertně	koncertně	k6eAd1	koncertně
<g/>
,	,	kIx,	,
známá	známý	k2eAgNnPc1d1	známé
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
i	i	k9	i
provedení	provedení	k1gNnPc4	provedení
scénická	scénický	k2eAgNnPc4d1	scénické
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
s	s	k7c7	s
baletním	baletní	k2eAgInSc7d1	baletní
souborem	soubor	k1gInSc7	soubor
<g/>
,	,	kIx,	,
častěji	často	k6eAd2	často
v	v	k7c6	v
netradičních	tradiční	k2eNgFnPc6d1	netradiční
prostorách	prostora	k1gFnPc6	prostora
s	s	k7c7	s
choreografiemi	choreografie	k1gFnPc7	choreografie
spíše	spíše	k9	spíše
muzikálovými	muzikálový	k2eAgFnPc7d1	muzikálová
(	(	kIx(	(
<g/>
nádvoří	nádvoří	k1gNnSc6	nádvoří
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgInPc1d1	přírodní
amfiteátry	amfiteátr	k1gInPc1	amfiteátr
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hudba	hudba	k1gFnSc1	hudba
Carminy	Carmin	k2eAgMnPc4d1	Carmin
Burany	buran	k1gMnPc4	buran
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
dramatičnost	dramatičnost	k1gFnSc4	dramatičnost
často	často	k6eAd1	často
používána	používán	k2eAgFnSc1d1	používána
jako	jako	k8xC	jako
hudba	hudba	k1gFnSc1	hudba
filmová	filmový	k2eAgFnSc1d1	filmová
–	–	k?	–
příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
film	film	k1gInSc4	film
Excalibur	Excalibura	k1gFnPc2	Excalibura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgMnS	být
kapelníkem	kapelník	k1gMnSc7	kapelník
orchestru	orchestr	k1gInSc2	orchestr
Nationaltheater	Nationaltheater	k1gInSc1	Nationaltheater
Mannheim	Mannheim	k1gInSc1	Mannheim
<g/>
,	,	kIx,	,
intenzivně	intenzivně	k6eAd1	intenzivně
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
studiem	studio	k1gNnSc7	studio
děl	dělo	k1gNnPc2	dělo
starých	starý	k2eAgMnPc2d1	starý
mistrů	mistr	k1gMnPc2	mistr
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1895	[number]	k4	1895
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Můj	můj	k3xOp1gMnSc1	můj
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
důstojník	důstojník	k1gMnSc1	důstojník
tělem	tělo	k1gNnSc7	tělo
i	i	k8xC	i
duší	duše	k1gFnSc7	duše
<g/>
,	,	kIx,	,
moje	můj	k3xOp1gFnSc1	můj
matka	matka	k1gFnSc1	matka
umělecky	umělecky	k6eAd1	umělecky
založená	založený	k2eAgFnSc1d1	založená
<g/>
,	,	kIx,	,
velice	velice	k6eAd1	velice
inteligentní	inteligentní	k2eAgFnSc1d1	inteligentní
a	a	k8xC	a
moudrá	moudrý	k2eAgFnSc1d1	moudrá
žena	žena	k1gFnSc1	žena
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
jej	on	k3xPp3gMnSc4	on
matka	matka	k1gFnSc1	matka
začala	začít	k5eAaPmAgFnS	začít
učit	učit	k5eAaImF	učit
hře	hra	k1gFnSc3	hra
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
se	se	k3xPyFc4	se
nudil	nudit	k5eAaImAgMnS	nudit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
zážitek	zážitek	k1gInSc4	zážitek
z	z	k7c2	z
návštěvy	návštěva	k1gFnSc2	návštěva
loutkového	loutkový	k2eAgNnSc2d1	loutkové
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
však	však	k9	však
i	i	k9	i
na	na	k7c4	na
varhany	varhany	k1gInPc4	varhany
a	a	k8xC	a
violoncello	violoncello	k1gNnSc4	violoncello
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
jako	jako	k9	jako
šestnáctiletý	šestnáctiletý	k2eAgMnSc1d1	šestnáctiletý
složil	složit	k5eAaPmAgInS	složit
na	na	k7c4	na
50	[number]	k4	50
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
skladeb	skladba	k1gFnPc2	skladba
pro	pro	k7c4	pro
varhany	varhany	k1gFnPc4	varhany
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgMnS	být
přijat	přijat	k2eAgMnSc1d1	přijat
na	na	k7c6	na
hudební	hudební	k2eAgFnSc6d1	hudební
akademii	akademie	k1gFnSc6	akademie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
zdála	zdát	k5eAaImAgFnS	zdát
příliš	příliš	k6eAd1	příliš
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
válečném	válečný	k2eAgInSc6d1	válečný
deníku	deník	k1gInSc6	deník
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
si	se	k3xPyFc3	se
výrazně	výrazně	k6eAd1	výrazně
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
"	"	kIx"	"
<g/>
studovat	studovat	k5eAaImF	studovat
staré	starý	k2eAgMnPc4d1	starý
mistry	mistr	k1gMnPc4	mistr
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Období	období	k1gNnSc1	období
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
mělo	mít	k5eAaImAgNnS	mít
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
můj	můj	k3xOp1gInSc4	můj
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
má	mít	k5eAaImIp3nS	mít
učební	učební	k2eAgNnPc4d1	učební
léta	léto	k1gNnPc4	léto
u	u	k7c2	u
starých	starý	k2eAgMnPc2d1	starý
mistrů	mistr	k1gMnPc2	mistr
a	a	k8xC	a
počátek	počátek	k1gInSc4	počátek
práce	práce	k1gFnSc2	práce
na	na	k7c4	na
Schulwerk	Schulwerk	k1gInSc4	Schulwerk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
objevil	objevit	k5eAaPmAgMnS	objevit
Carl	Carl	k1gMnSc1	Carl
Orff	Orff	k1gMnSc1	Orff
středověký	středověký	k2eAgInSc4d1	středověký
rukopis	rukopis	k1gInSc4	rukopis
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
latinských	latinský	k2eAgFnPc2d1	Latinská
a	a	k8xC	a
německých	německý	k2eAgFnPc2d1	německá
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
světských	světský	k2eAgFnPc2d1	světská
a	a	k8xC	a
pijáckých	pijácký	k2eAgFnPc2d1	pijácká
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Štěstěna	Štěstěna	k1gFnSc1	Štěstěna
to	ten	k3xDgNnSc4	ten
se	s	k7c7	s
mnou	já	k3xPp1nSc7	já
myslela	myslet	k5eAaImAgFnS	myslet
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
když	když	k8xS	když
mi	já	k3xPp1nSc3	já
do	do	k7c2	do
ruky	ruka	k1gFnSc2	ruka
přihrála	přihrát	k5eAaPmAgFnS	přihrát
katalog	katalog	k1gInSc4	katalog
antikvariátu	antikvariát	k1gInSc2	antikvariát
ve	v	k7c6	v
Würzburgu	Würzburg	k1gInSc6	Würzburg
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
jsem	být	k5eAaImIp1nS	být
našel	najít	k5eAaPmAgMnS	najít
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
mě	já	k3xPp1nSc4	já
magicky	magicky	k6eAd1	magicky
přitahoval	přitahovat	k5eAaImAgMnS	přitahovat
<g/>
:	:	kIx,	:
Carmina	Carmin	k2eAgMnSc4d1	Carmin
Burana	buran	k1gMnSc4	buran
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Po	po	k7c6	po
prvním	první	k4xOgMnSc6	první
jejich	jejich	k3xOp3gNnSc6	jejich
uvedení	uvedení	k1gNnSc6	uvedení
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1937	[number]	k4	1937
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
napsal	napsat	k5eAaPmAgMnS	napsat
svému	svůj	k3xOyFgMnSc3	svůj
nakladateli	nakladatel	k1gMnSc3	nakladatel
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Všechno	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
jsem	být	k5eAaImIp1nS	být
dosud	dosud	k6eAd1	dosud
napsal	napsat	k5eAaPmAgMnS	napsat
a	a	k8xC	a
Vy	vy	k3xPp2nPc1	vy
také	také	k9	také
bohužel	bohužel	k6eAd1	bohužel
vytiskl	vytisknout	k5eAaPmAgMnS	vytisknout
<g/>
,	,	kIx,	,
můžete	moct	k5eAaImIp2nP	moct
nyní	nyní	k6eAd1	nyní
hodit	hodit	k5eAaImF	hodit
do	do	k7c2	do
stoupy	stoupa	k1gFnSc2	stoupa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Carmina	Carmin	k2eAgMnSc2d1	Carmin
Burana	buran	k1gMnSc2	buran
začíná	začínat	k5eAaImIp3nS	začínat
mé	můj	k3xOp1gNnSc1	můj
souborné	souborný	k2eAgNnSc1d1	souborné
dílo	dílo	k1gNnSc1	dílo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Orff	Orff	k1gMnSc1	Orff
byl	být	k5eAaImAgMnS	být
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
manželkou	manželka	k1gFnSc7	manželka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
Alice	Alice	k1gFnSc1	Alice
Solscherová	Solscherová	k1gFnSc1	Solscherová
<g/>
,	,	kIx,	,
manželství	manželství	k1gNnSc1	manželství
vydrželo	vydržet	k5eAaPmAgNnS	vydržet
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
narodilo	narodit	k5eAaPmAgNnS	narodit
Orffovo	Orffův	k2eAgNnSc1d1	Orffovo
jediné	jediný	k2eAgNnSc1d1	jediné
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Godela	Godela	k1gFnSc1	Godela
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
ženou	žena	k1gFnSc7	žena
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
stala	stát	k5eAaPmAgFnS	stát
Gertrud	Gertrud	k1gInSc4	Gertrud
Willertová	Willertová	k1gFnSc1	Willertová
<g/>
,	,	kIx,	,
k	k	k7c3	k
rozvodu	rozvod	k1gInSc3	rozvod
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
spisovatelkou	spisovatelka	k1gFnSc7	spisovatelka
Luise	Luisa	k1gFnSc3	Luisa
Rinserovou	Rinserová	k1gFnSc4	Rinserová
<g/>
,	,	kIx,	,
manželství	manželství	k1gNnSc1	manželství
bylo	být	k5eAaImAgNnS	být
rozvedeno	rozvést	k5eAaPmNgNnS	rozvést
po	po	k7c6	po
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
manželkou	manželka	k1gFnSc7	manželka
byla	být	k5eAaImAgFnS	být
Liselotte	Liselott	k1gInSc5	Liselott
Schmitzová	Schmitzová	k1gFnSc1	Schmitzová
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Orff	Orff	k1gInSc1	Orff
psal	psát	k5eAaImAgInS	psát
tzv.	tzv.	kA	tzv.
scénické	scénický	k2eAgFnSc2d1	scénická
kompozice	kompozice	k1gFnSc2	kompozice
<g/>
,	,	kIx,	,
podstatně	podstatně	k6eAd1	podstatně
se	se	k3xPyFc4	se
lišící	lišící	k2eAgMnSc1d1	lišící
od	od	k7c2	od
klasických	klasický	k2eAgFnPc2d1	klasická
hudebních	hudební	k2eAgFnPc2d1	hudební
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
kompozice	kompozice	k1gFnPc4	kompozice
nazýval	nazývat	k5eAaImAgMnS	nazývat
hrami	hra	k1gFnPc7	hra
<g/>
,	,	kIx,	,
tragédiemi	tragédie	k1gFnPc7	tragédie
<g/>
,	,	kIx,	,
komediemi	komedie	k1gFnPc7	komedie
či	či	k8xC	či
divadlem	divadlo	k1gNnSc7	divadlo
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dílech	díl	k1gInPc6	díl
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
k	k	k7c3	k
primitivnímu	primitivní	k2eAgNnSc3d1	primitivní
spojení	spojení	k1gNnSc3	spojení
melodie	melodie	k1gFnSc2	melodie
a	a	k8xC	a
rytmu	rytmus	k1gInSc2	rytmus
a	a	k8xC	a
hudbu	hudba	k1gFnSc4	hudba
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
jevištním	jevištní	k2eAgInSc7d1	jevištní
projevem	projev	k1gInSc7	projev
a	a	k8xC	a
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Hojně	hojně	k6eAd1	hojně
používá	používat	k5eAaImIp3nS	používat
bicí	bicí	k2eAgInPc4d1	bicí
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sborová	sborový	k2eAgNnPc1d1	sborové
díla	dílo	k1gNnPc1	dílo
===	===	k?	===
</s>
</p>
<p>
<s>
Kantáty	kantáta	k1gFnPc1	kantáta
a	a	k8xC	a
sborové	sborový	k2eAgFnPc1d1	sborová
věty	věta	k1gFnPc1	věta
na	na	k7c4	na
texty	text	k1gInPc4	text
Brechtovy	Brechtův	k2eAgInPc4d1	Brechtův
<g/>
,	,	kIx,	,
Goethovy	Goethův	k2eAgInPc4d1	Goethův
<g/>
,	,	kIx,	,
Hoelderlinovy	Hoelderlinův	k2eAgInPc4d1	Hoelderlinův
<g/>
,	,	kIx,	,
Klopstockovy	Klopstockův	k2eAgInPc4d1	Klopstockův
<g/>
,	,	kIx,	,
Schillerovy	Schillerův	k2eAgInPc4d1	Schillerův
<g/>
,	,	kIx,	,
Hebbelovy	Hebbelův	k2eAgInPc4d1	Hebbelův
a	a	k8xC	a
Werflovy	Werflův	k2eAgInPc4d1	Werflův
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
z	z	k7c2	z
řeči	řeč	k1gFnSc2	řeč
čerpal	čerpat	k5eAaImAgInS	čerpat
Orff	Orff	k1gInSc4	Orff
důležité	důležitý	k2eAgInPc1d1	důležitý
podněty	podnět	k1gInPc1	podnět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
textů	text	k1gInPc2	text
mnoha	mnoho	k4c2	mnoho
scénických	scénický	k2eAgNnPc2d1	scénické
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
jako	jako	k9	jako
přednášející	přednášející	k1gFnSc1	přednášející
<g/>
,	,	kIx,	,
zpívající	zpívající	k2eAgInSc1d1	zpívající
a	a	k8xC	a
bubnující	bubnující	k2eAgMnSc1d1	bubnující
interpret	interpret	k1gMnSc1	interpret
vyvolával	vyvolávat	k5eAaImAgMnS	vyvolávat
u	u	k7c2	u
diváků	divák	k1gMnPc2	divák
vždy	vždy	k6eAd1	vždy
nadšení	nadšený	k2eAgMnPc1d1	nadšený
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výčtu	výčet	k1gInSc2	výčet
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
sborů	sbor	k1gInPc2	sbor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgNnPc1d1	další
sborová	sborový	k2eAgNnPc1d1	sborové
díla	dílo	k1gNnPc1	dílo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Dithyrambi	Dithyrambi	k6eAd1	Dithyrambi
</s>
</p>
<p>
<s>
Sunt	Sunt	k1gInSc1	Sunt
lacrimae	lacrimaat	k5eAaPmIp3nS	lacrimaat
rerum	rerum	k1gInSc4	rerum
</s>
</p>
<p>
<s>
Kusy	kus	k1gInPc1	kus
pro	pro	k7c4	pro
recitační	recitační	k2eAgInSc4d1	recitační
sbor	sbor	k1gInSc4	sbor
</s>
</p>
<p>
<s>
Kusy	kus	k1gInPc1	kus
pro	pro	k7c4	pro
přednes	přednes	k1gInSc4	přednes
<g/>
,	,	kIx,	,
recitační	recitační	k2eAgInSc4d1	recitační
sbor	sbor	k1gInSc4	sbor
a	a	k8xC	a
bicí	bicí	k2eAgFnSc4d1	bicí
</s>
</p>
<p>
<s>
===	===	k?	===
Pohádková	pohádkový	k2eAgNnPc1d1	pohádkové
díla	dílo	k1gNnPc1	dílo
===	===	k?	===
</s>
</p>
<p>
<s>
Der	drát	k5eAaImRp2nS	drát
Mond	mond	k1gInSc1	mond
(	(	kIx(	(
<g/>
Měsíc	měsíc	k1gInSc1	měsíc
<g/>
,	,	kIx,	,
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
,	,	kIx,	,
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Die	Die	k?	Die
Kluge	Kluge	k1gFnSc1	Kluge
(	(	kIx(	(
<g/>
Chytračka	chytračka	k1gFnSc1	chytračka
<g/>
,	,	kIx,	,
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
,	,	kIx,	,
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
–	–	k?	–
pohádka	pohádka	k1gFnSc1	pohádka
o	o	k7c6	o
králi	král	k1gMnSc6	král
a	a	k8xC	a
chytré	chytrý	k2eAgFnSc3d1	chytrá
ženě	žena	k1gFnSc3	žena
<g/>
.	.	kIx.	.
</s>
<s>
Orff	Orff	k1gMnSc1	Orff
se	se	k3xPyFc4	se
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
na	na	k7c4	na
verzi	verze	k1gFnSc4	verze
bratří	bratr	k1gMnPc2	bratr
Grimmů	Grimm	k1gMnPc2	Grimm
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
se	se	k3xPyFc4	se
ale	ale	k9	ale
inspirovat	inspirovat	k5eAaBmF	inspirovat
i	i	k9	i
jinými	jiný	k2eAgFnPc7d1	jiná
variantami	varianta	k1gFnPc7	varianta
tohoto	tento	k3xDgNnSc2	tento
tématu	téma	k1gNnSc2	téma
<g/>
.	.	kIx.	.
</s>
<s>
Podněty	podnět	k1gInPc1	podnět
pro	pro	k7c4	pro
mluvený	mluvený	k2eAgInSc4d1	mluvený
styl	styl	k1gInSc4	styl
našel	najít	k5eAaPmAgMnS	najít
Orff	Orff	k1gMnSc1	Orff
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
sbírce	sbírka	k1gFnSc6	sbírka
přísloví	přísloví	k1gNnSc2	přísloví
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1846	[number]	k4	1846
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
líbily	líbit	k5eAaImAgFnP	líbit
barvité	barvitý	k2eAgFnPc1d1	barvitá
<g/>
,	,	kIx,	,
drasticky	drasticky	k6eAd1	drasticky
drsné	drsný	k2eAgFnSc2d1	drsná
průpovídky	průpovídka	k1gFnSc2	průpovídka
<g/>
.	.	kIx.	.
</s>
<s>
Chytračka	chytračka	k1gFnSc1	chytračka
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
Carmina	Carmin	k2eAgMnSc4d1	Carmin
Burana	buran	k1gMnSc4	buran
Orffovou	Orffův	k2eAgFnSc7d1	Orffova
nejpopulárnější	populární	k2eAgFnSc7d3	nejpopulárnější
scénickou	scénický	k2eAgFnSc7d1	scénická
skladbou	skladba	k1gFnSc7	skladba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ein	Ein	k?	Ein
Sommernachtstraum	Sommernachtstraum	k1gInSc1	Sommernachtstraum
(	(	kIx(	(
<g/>
Sen	sen	k1gInSc1	sen
letní	letní	k2eAgFnSc2d1	letní
noci	noc	k1gFnSc2	noc
<g/>
,	,	kIx,	,
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Bairisches	Bairisches	k1gMnSc1	Bairisches
Welttheater	Welttheater	k1gMnSc1	Welttheater
===	===	k?	===
</s>
</p>
<p>
<s>
Die	Die	k?	Die
Bernauerin	Bernauerin	k1gInSc1	Bernauerin
(	(	kIx(	(
<g/>
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ludus	Ludus	k1gInSc1	Ludus
de	de	k?	de
Nato	nato	k6eAd1	nato
Infante	infant	k1gMnSc5	infant
Mirificus	Mirificus	k1gInSc1	Mirificus
(	(	kIx(	(
<g/>
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
<g/>
,	,	kIx,	,
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Astutuli	Astutule	k1gFnSc4	Astutule
(	(	kIx(	(
<g/>
Chytráci	chytrák	k1gMnPc1	chytrák
<g/>
,	,	kIx,	,
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
–	–	k?	–
komedie	komedie	k1gFnSc2	komedie
o	o	k7c6	o
obyvatelích	obyvatel	k1gMnPc6	obyvatel
jednoho	jeden	k4xCgNnSc2	jeden
městečka	městečko	k1gNnSc2	městečko
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
jednoho	jeden	k4xCgMnSc4	jeden
šprýmaře	šprýmař	k1gMnSc4	šprýmař
za	za	k7c4	za
blázna	blázen	k1gMnSc4	blázen
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
připraví	připravit	k5eAaPmIp3nS	připravit
o	o	k7c4	o
šaty	šat	k1gInPc4	šat
slibem	slib	k1gInSc7	slib
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
z	z	k7c2	z
kalhotových	kalhotový	k2eAgMnPc2d1	kalhotový
knoflíků	knoflík	k1gMnPc2	knoflík
nadělá	nadělat	k5eAaBmIp3nS	nadělat
zlaťáky	zlaťák	k1gInPc1	zlaťák
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
konečně	konečně	k6eAd1	konečně
v	v	k7c6	v
hlavách	hlava	k1gFnPc6	hlava
rozsvítí	rozsvítit	k5eAaPmIp3nS	rozsvítit
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
sami	sám	k3xTgMnPc1	sám
chytili	chytit	k5eAaPmAgMnP	chytit
za	za	k7c4	za
nos	nos	k1gInSc4	nos
<g/>
,	,	kIx,	,
vynadají	vynadat	k5eAaPmIp3nP	vynadat
publiku	publikum	k1gNnSc3	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgInSc1d1	hudební
moment	moment	k1gInSc1	moment
komedie	komedie	k1gFnSc2	komedie
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
rytmizované	rytmizovaný	k2eAgFnSc6d1	rytmizovaná
řeči	řeč	k1gFnSc6	řeč
–	–	k?	–
deftig	deftig	k1gInSc1	deftig
bairisch	bairisch	k1gInSc1	bairisch
(	(	kIx(	(
<g/>
bavorské	bavorský	k2eAgFnSc6d1	bavorská
němčině	němčina	k1gFnSc6	němčina
<g/>
)	)	kIx)	)
–	–	k?	–
s	s	k7c7	s
vkládanými	vkládaný	k2eAgInPc7d1	vkládaný
latinskými	latinský	k2eAgInPc7d1	latinský
posměšky	posměšek	k1gInPc7	posměšek
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
psáno	psát	k5eAaImNgNnS	psát
pro	pro	k7c4	pro
herce	herec	k1gMnPc4	herec
<g/>
,	,	kIx,	,
doprovázené	doprovázený	k2eAgNnSc1d1	doprovázené
orchestrem	orchestr	k1gInSc7	orchestr
bicích	bicí	k2eAgInPc2d1	bicí
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Comoedia	Comoedium	k1gNnSc2	Comoedium
de	de	k?	de
Christ	Christ	k1gMnSc1	Christ
Resurrectione	Resurrection	k1gInSc5	Resurrection
(	(	kIx(	(
<g/>
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Trionfi	Trionfi	k1gNnPc3	Trionfi
===	===	k?	===
</s>
</p>
<p>
<s>
Carmina	Carmin	k2eAgMnSc4d1	Carmin
Burana	buran	k1gMnSc4	buran
(	(	kIx(	(
<g/>
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
<g/>
,	,	kIx,	,
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Catulli	Catulle	k1gFnSc4	Catulle
Carmina	Carmin	k2eAgMnSc2d1	Carmin
(	(	kIx(	(
<g/>
Lipsko	Lipsko	k1gNnSc1	Lipsko
<g/>
,	,	kIx,	,
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Trionfo	Trionfo	k1gMnSc1	Trionfo
di	di	k?	di
Afrodité	Afroditý	k2eAgNnSc1d1	Afrodité
(	(	kIx(	(
<g/>
Milán	Milán	k1gInSc1	Milán
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
souborně	souborně	k6eAd1	souborně
jako	jako	k9	jako
Trionfi	Trionfe	k1gFnSc4	Trionfe
<g/>
,	,	kIx,	,
Trittico	Trittico	k6eAd1	Trittico
teatrale	teatrale	k6eAd1	teatrale
(	(	kIx(	(
<g/>
Milán	Milán	k1gInSc1	Milán
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Theatrum	Theatrum	k1gNnSc1	Theatrum
Mundi	Mund	k1gMnPc5	Mund
===	===	k?	===
</s>
</p>
<p>
<s>
Antigonae	Antigonae	k1gFnSc1	Antigonae
(	(	kIx(	(
<g/>
Salzburg	Salzburg	k1gInSc1	Salzburg
<g/>
,	,	kIx,	,
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tyran	tyran	k1gMnSc1	tyran
Oidipus	Oidipus	k1gMnSc1	Oidipus
(	(	kIx(	(
<g/>
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Prometheus	Prometheus	k1gMnSc1	Prometheus
(	(	kIx(	(
<g/>
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
De	De	k?	De
Temporum	Temporum	k1gInSc1	Temporum
Fine	Fin	k1gMnSc5	Fin
Comoedia	Comoedium	k1gNnPc1	Comoedium
(	(	kIx(	(
<g/>
Salzburg	Salzburg	k1gInSc1	Salzburg
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
Hudebnost	hudebnost	k1gFnSc1	hudebnost
a	a	k8xC	a
symbolika	symbolika	k1gFnSc1	symbolika
řeckého	řecký	k2eAgInSc2d1	řecký
jazyka	jazyk	k1gInSc2	jazyk
byly	být	k5eAaImAgInP	být
rozhodující	rozhodující	k2eAgInPc1d1	rozhodující
při	při	k7c6	při
hudebně	hudebně	k6eAd1	hudebně
–	–	k?	–
scénickém	scénický	k2eAgNnSc6d1	scénické
ztvárnění	ztvárnění	k1gNnSc6	ztvárnění
Aischylovy	Aischylův	k2eAgFnSc2d1	Aischylova
tragedie	tragedie	k1gFnSc2	tragedie
Prometheus	Prometheus	k1gMnSc1	Prometheus
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Po	po	k7c6	po
Antigoně	Antigon	k1gInSc6	Antigon
a	a	k8xC	a
Oidipovi	Oidipus	k1gMnSc3	Oidipus
jsem	být	k5eAaImIp1nS	být
mohl	moct	k5eAaImAgMnS	moct
pokračovat	pokračovat	k5eAaImF	pokračovat
jen	jen	k9	jen
dílem	dílo	k1gNnSc7	dílo
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Aischylův	Aischylův	k2eAgMnSc1d1	Aischylův
Prometheus	Prometheus	k1gMnSc1	Prometheus
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
jeviště	jeviště	k1gNnSc1	jeviště
tragedie	tragedie	k1gFnSc2	tragedie
stává	stávat	k5eAaImIp3nS	stávat
jevištěm	jeviště	k1gNnSc7	jeviště
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
Prometheovi	Prometheus	k1gMnSc6	Prometheus
viděl	vidět	k5eAaImAgInS	vidět
Orff	Orff	k1gInSc1	Orff
"	"	kIx"	"
<g/>
symbol	symbol	k1gInSc1	symbol
v	v	k7c6	v
mezích	mez	k1gFnPc6	mez
lidské	lidský	k2eAgFnSc2d1	lidská
existence	existence	k1gFnSc2	existence
prokazatelného	prokazatelný	k2eAgInSc2d1	prokazatelný
rozumu	rozum	k1gInSc2	rozum
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Provedení	provedení	k1gNnSc1	provedení
Promethea	Prometheus	k1gMnSc2	Prometheus
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
náročné	náročný	k2eAgNnSc1d1	náročné
<g/>
:	:	kIx,	:
pro	pro	k7c4	pro
scénu	scéna	k1gFnSc4	scéna
proměnami	proměna	k1gFnPc7	proměna
symbolického	symbolický	k2eAgInSc2d1	symbolický
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
pěvce	pěvec	k1gMnPc4	pěvec
starořeckým	starořecký	k2eAgInSc7d1	starořecký
textem	text	k1gInSc7	text
a	a	k8xC	a
pro	pro	k7c4	pro
orchestr	orchestr	k1gInSc4	orchestr
nástrojovým	nástrojový	k2eAgNnSc7d1	nástrojové
vybavením	vybavení	k1gNnSc7	vybavení
(	(	kIx(	(
<g/>
9	[number]	k4	9
kontrabasů	kontrabas	k1gInPc2	kontrabas
<g/>
,	,	kIx,	,
4	[number]	k4	4
klavíry	klavír	k1gInPc1	klavír
<g/>
,	,	kIx,	,
4	[number]	k4	4
harfy	harfa	k1gFnPc1	harfa
<g/>
,	,	kIx,	,
žestě	žestě	k1gInPc1	žestě
<g/>
,	,	kIx,	,
varhany	varhany	k1gFnPc1	varhany
a	a	k8xC	a
45	[number]	k4	45
různých	různý	k2eAgInPc2d1	různý
bicích	bicí	k2eAgInPc2d1	bicí
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Role	role	k1gFnPc1	role
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
od	od	k7c2	od
pěvců	pěvec	k1gMnPc2	pěvec
vysoké	vysoký	k2eAgInPc4d1	vysoký
výkony	výkon	k1gInPc4	výkon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
činnost	činnost	k1gFnSc1	činnost
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
se	se	k3xPyFc4	se
Carl	Carl	k1gMnSc1	Carl
Orff	Orff	k1gMnSc1	Orff
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Gunild	Gunilda	k1gFnPc2	Gunilda
Keetmann	Keetmann	k1gNnSc1	Keetmann
a	a	k8xC	a
Dorothee	Dorothee	k1gInSc1	Dorothee
Günther	Günthra	k1gFnPc2	Günthra
snažil	snažit	k5eAaImAgInS	snažit
o	o	k7c4	o
novou	nový	k2eAgFnSc4d1	nová
koncepci	koncepce	k1gFnSc4	koncepce
hudebního	hudební	k2eAgNnSc2d1	hudební
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Rozvíjel	rozvíjet	k5eAaImAgMnS	rozvíjet
zde	zde	k6eAd1	zde
svou	svůj	k3xOyFgFnSc4	svůj
myšlenku	myšlenka	k1gFnSc4	myšlenka
jednoty	jednota	k1gFnSc2	jednota
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
tanečního	taneční	k2eAgInSc2d1	taneční
projevu	projev	k1gInSc2	projev
a	a	k8xC	a
hledal	hledat	k5eAaImAgInS	hledat
především	především	k9	především
nové	nový	k2eAgInPc4d1	nový
podněty	podnět	k1gInPc4	podnět
pro	pro	k7c4	pro
výrazový	výrazový	k2eAgInSc4d1	výrazový
tanec	tanec	k1gInSc4	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc1	svůj
pedagogické	pedagogický	k2eAgFnPc1d1	pedagogická
ideje	idea	k1gFnPc1	idea
<g/>
,	,	kIx,	,
Schulwerk	Schulwerk	k1gInSc1	Schulwerk
<g/>
,	,	kIx,	,
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
v	v	k7c6	v
pětisvazkové	pětisvazkový	k2eAgFnSc6d1	pětisvazková
sbírce	sbírka	k1gFnSc6	sbírka
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Musik	musika	k1gFnPc2	musika
für	für	k?	für
Kinder	Kinder	k1gMnSc1	Kinder
(	(	kIx(	(
<g/>
Hudba	hudba	k1gFnSc1	hudba
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přeložené	přeložený	k2eAgFnSc2d1	přeložená
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Schulwerk	Schulwerk	k1gInSc1	Schulwerk
je	být	k5eAaImIp3nS	být
výraz	výraz	k1gInSc1	výraz
používaný	používaný	k2eAgInSc1d1	používaný
již	již	k6eAd1	již
Paulem	Paul	k1gMnSc7	Paul
Hindemithem	Hindemith	k1gInSc7	Hindemith
<g/>
.	.	kIx.	.
</s>
<s>
Znamená	znamenat	k5eAaImIp3nS	znamenat
látku	látka	k1gFnSc4	látka
<g/>
,	,	kIx,	,
podnět	podnět	k1gInSc4	podnět
a	a	k8xC	a
příležitost	příležitost	k1gFnSc4	příležitost
k	k	k7c3	k
hraní	hraní	k1gNnSc3	hraní
<g/>
,	,	kIx,	,
rozvoji	rozvoj	k1gInSc3	rozvoj
a	a	k8xC	a
přetváření	přetváření	k1gNnSc4	přetváření
vlastního	vlastní	k2eAgInSc2d1	vlastní
projevu	projev	k1gInSc2	projev
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
řeči	řeč	k1gFnSc3	řeč
a	a	k8xC	a
pohybu	pohyb	k1gInSc3	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Schulwerkem	Schulwerek	k1gInSc7	Schulwerek
přestává	přestávat	k5eAaImIp3nS	přestávat
být	být	k5eAaImF	být
učení	učení	k1gNnSc4	učení
hudbě	hudba	k1gFnSc6	hudba
učením	učení	k1gNnSc7	učení
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
umělecky	umělecky	k6eAd1	umělecky
podnětným	podnětný	k2eAgInSc7d1	podnětný
<g/>
,	,	kIx,	,
živým	živý	k2eAgInSc7d1	živý
a	a	k8xC	a
nápaditým	nápaditý	k2eAgInSc7d1	nápaditý
projevem	projev	k1gInSc7	projev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
hudební	hudební	k2eAgFnSc7d1	hudební
a	a	k8xC	a
taneční	taneční	k2eAgFnSc7d1	taneční
výchovou	výchova	k1gFnSc7	výchova
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zvláště	zvláště	k6eAd1	zvláště
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
podpořen	podpořen	k2eAgInSc4d1	podpořen
rozvoj	rozvoj	k1gInSc4	rozvoj
přirozeného	přirozený	k2eAgInSc2d1	přirozený
talentu	talent	k1gInSc2	talent
<g/>
.	.	kIx.	.
</s>
<s>
Řeč	řeč	k1gFnSc1	řeč
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
tanec	tanec	k1gInSc1	tanec
a	a	k8xC	a
nástrojová	nástrojový	k2eAgFnSc1d1	nástrojová
hra	hra	k1gFnSc1	hra
patří	patřit	k5eAaImIp3nS	patřit
dohromady	dohromady	k6eAd1	dohromady
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
společné	společný	k2eAgFnPc1d1	společná
formy	forma	k1gFnPc1	forma
lidského	lidský	k2eAgInSc2d1	lidský
projevu	projev	k1gInSc2	projev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
stadiu	stadion	k1gNnSc6	stadion
hudebního	hudební	k2eAgNnSc2d1	hudební
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
aktivní	aktivní	k2eAgNnSc1d1	aktivní
používání	používání	k1gNnSc1	používání
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
řeči	řeč	k1gFnSc2	řeč
a	a	k8xC	a
tance	tanec	k1gInSc2	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Vzory	vzor	k1gInPc1	vzor
nejsou	být	k5eNaImIp3nP	být
pouze	pouze	k6eAd1	pouze
napodobovány	napodobován	k2eAgFnPc1d1	napodobována
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
podnětem	podnět	k1gInSc7	podnět
pro	pro	k7c4	pro
variace	variace	k1gFnPc4	variace
<g/>
,	,	kIx,	,
improvizace	improvizace	k1gFnPc4	improvizace
a	a	k8xC	a
vlastní	vlastní	k2eAgFnSc4d1	vlastní
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tzv.	tzv.	kA	tzv.
Orffovy	Orffův	k2eAgInPc4d1	Orffův
nástroje	nástroj	k1gInPc4	nástroj
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
způsobem	způsob	k1gInSc7	způsob
podporují	podporovat	k5eAaImIp3nP	podporovat
cíle	cíl	k1gInPc4	cíl
Musik	musika	k1gFnPc2	musika
für	für	k?	für
Kinder	Kindra	k1gFnPc2	Kindra
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
zvonkohry	zvonkohra	k1gFnPc1	zvonkohra
<g/>
,	,	kIx,	,
xylofony	xylofon	k1gInPc1	xylofon
<g/>
,	,	kIx,	,
metalofony	metalofon	k1gInPc1	metalofon
a	a	k8xC	a
mnohé	mnohý	k2eAgInPc1d1	mnohý
malé	malý	k2eAgInPc1d1	malý
bicí	bicí	k2eAgInPc1d1	bicí
nástroje	nástroj	k1gInPc1	nástroj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
zobcovými	zobcový	k2eAgFnPc7d1	zobcová
flétnami	flétna	k1gFnPc7	flétna
a	a	k8xC	a
smyčcovými	smyčcový	k2eAgInPc7d1	smyčcový
nástroji	nástroj	k1gInPc7	nástroj
doprovázejí	doprovázet	k5eAaImIp3nP	doprovázet
dětský	dětský	k2eAgInSc4d1	dětský
zpěv	zpěv	k1gInSc4	zpěv
a	a	k8xC	a
tanec	tanec	k1gInSc4	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
nástroje	nástroj	k1gInPc1	nástroj
používané	používaný	k2eAgInPc1d1	používaný
již	již	k6eAd1	již
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
moderní	moderní	k2eAgFnPc1d1	moderní
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
staromódní	staromódní	k2eAgInSc1d1	staromódní
<g/>
.	.	kIx.	.
</s>
<s>
Znějí	znět	k5eAaImIp3nP	znět
"	"	kIx"	"
<g/>
jinak	jinak	k6eAd1	jinak
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
cize	cize	k6eAd1	cize
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výraz	výraz	k1gInSc4	výraz
"	"	kIx"	"
<g/>
Orffovy	Orffův	k2eAgInPc4d1	Orffův
nástroje	nástroj	k1gInPc4	nástroj
<g/>
"	"	kIx"	"
nezavedl	zavést	k5eNaPmAgMnS	zavést
Orff	Orff	k1gMnSc1	Orff
sám	sám	k3xTgMnSc1	sám
<g/>
;	;	kIx,	;
s	s	k7c7	s
uvedením	uvedení	k1gNnSc7	uvedení
Musik	musika	k1gFnPc2	musika
für	für	k?	für
Kinder	Kindra	k1gFnPc2	Kindra
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
vyskytlo	vyskytnout	k5eAaPmAgNnS	vyskytnout
toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vedle	vedle	k7c2	vedle
původního	původní	k2eAgNnSc2d1	původní
vydání	vydání	k1gNnSc2	vydání
Musik	musika	k1gFnPc2	musika
für	für	k?	für
Kinder	Kindra	k1gFnPc2	Kindra
existují	existovat	k5eAaImIp3nP	existovat
verze	verze	k1gFnPc4	verze
anglické	anglický	k2eAgFnSc2d1	anglická
<g/>
,	,	kIx,	,
švédské	švédský	k2eAgFnSc2d1	švédská
<g/>
,	,	kIx,	,
holandské	holandský	k2eAgFnSc2d1	holandská
<g/>
,	,	kIx,	,
španělské	španělský	k2eAgFnSc2d1	španělská
<g/>
,	,	kIx,	,
portugalské	portugalský	k2eAgFnSc2d1	portugalská
<g/>
,	,	kIx,	,
japonské	japonský	k2eAgFnSc2d1	japonská
<g/>
,	,	kIx,	,
francouzské	francouzský	k2eAgFnSc2d1	francouzská
<g/>
,	,	kIx,	,
české	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
slovenské	slovenský	k2eAgFnSc2d1	slovenská
<g/>
,	,	kIx,	,
čínské	čínský	k2eAgFnSc2d1	čínská
<g/>
,	,	kIx,	,
korejské	korejský	k2eAgFnSc2d1	Korejská
<g/>
,	,	kIx,	,
italské	italský	k2eAgFnSc2d1	italská
<g/>
,	,	kIx,	,
polské	polský	k2eAgFnSc2d1	polská
a	a	k8xC	a
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
<g/>
.	.	kIx.	.
</s>
<s>
Objevilo	objevit	k5eAaPmAgNnS	objevit
se	se	k3xPyFc4	se
i	i	k9	i
vydání	vydání	k1gNnSc2	vydání
v	v	k7c6	v
Braillově	Braillův	k2eAgNnSc6d1	Braillovo
písmu	písmo	k1gNnSc6	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
dodatky	dodatek	k1gInPc1	dodatek
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
africké	africký	k2eAgFnSc2d1	africká
<g/>
,	,	kIx,	,
brazilské	brazilský	k2eAgFnSc2d1	brazilská
<g/>
,	,	kIx,	,
řecké	řecký	k2eAgFnSc2d1	řecká
a	a	k8xC	a
velšské	velšský	k2eAgFnSc2d1	velšská
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Carl	Carl	k1gMnSc1	Carl
Orff	Orff	k1gMnSc1	Orff
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
náhrobek	náhrobek	k1gInSc1	náhrobek
nese	nést	k5eAaImIp3nS	nést
nápis	nápis	k1gInSc1	nápis
SUMMUS	SUMMUS	kA	SUMMUS
FINIS	finis	k1gInPc1	finis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Orffův	Orffův	k2eAgInSc1d1	Orffův
institut	institut	k1gInSc1	institut
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
Mozarteum	Mozarteum	k1gNnSc1	Mozarteum
v	v	k7c6	v
Salzburku	Salzburk	k1gInSc6	Salzburk
a	a	k8xC	a
četné	četný	k2eAgFnPc1d1	četná
Orffovy	Orffův	k2eAgFnPc1d1	Orffova
společnosti	společnost	k1gFnPc1	společnost
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
pečují	pečovat	k5eAaImIp3nP	pečovat
o	o	k7c6	o
aktualizaci	aktualizace	k1gFnSc6	aktualizace
pedagogických	pedagogický	k2eAgFnPc2d1	pedagogická
myšlenek	myšlenka	k1gFnPc2	myšlenka
Carla	Carl	k1gMnSc2	Carl
Orffa	Orff	k1gMnSc2	Orff
a	a	k8xC	a
Gunild	Gunild	k1gMnSc1	Gunild
Keetmann	Keetmann	k1gMnSc1	Keetmann
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Carmina	Carmin	k2eAgMnSc4d1	Carmin
Burana	buran	k1gMnSc4	buran
</s>
</p>
