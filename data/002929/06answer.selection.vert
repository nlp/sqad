<s>
Finsko	Finsko	k1gNnSc1	Finsko
je	být	k5eAaImIp3nS	být
zemí	zem	k1gFnSc7	zem
tisíců	tisíc	k4xCgInPc2	tisíc
jezer	jezero	k1gNnPc2	jezero
a	a	k8xC	a
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
necelých	celý	k2eNgInPc2d1	necelý
10	[number]	k4	10
%	%	kIx~	%
jeho	jeho	k3xOp3gFnPc1	jeho
plochy	plocha	k1gFnPc1	plocha
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
