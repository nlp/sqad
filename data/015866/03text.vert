<s>
Souhvězdí	souhvězdí	k1gNnSc1
Jižního	jižní	k2eAgInSc2d1
trojúhelníku	trojúhelník	k1gInSc2
</s>
<s>
Souhvězdí	souhvězdí	k1gNnSc1
Jižního	jižní	k2eAgInSc2d1
trojúhelníku	trojúhelník	k1gInSc2
</s>
<s>
Souhvězdí	souhvězdí	k1gNnSc1
Jižního	jižní	k2eAgInSc2d1
trojúhelníku	trojúhelník	k1gInSc2
</s>
<s>
Latinský	latinský	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Triangulum	Triangulum	k1gInSc1
Australe	Austral	k1gMnSc5
</s>
<s>
Latinský	latinský	k2eAgInSc1d1
genitiv	genitiv	k1gInSc1
</s>
<s>
Trianguli	Triangule	k1gFnSc4
Australis	Australis	k1gFnSc2
</s>
<s>
Latinská	latinský	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
</s>
<s>
TrA	tra	k0
</s>
<s>
Rektascenze	rektascenze	k1gFnSc1
</s>
<s>
14	#num#	k4
h	h	k?
55	#num#	k4
m	m	kA
až	až	k9
17	#num#	k4
H	H	kA
10	#num#	k4
m	m	kA
</s>
<s>
Deklinace	deklinace	k1gFnSc1
</s>
<s>
-71	-71	k4
<g/>
°	°	k?
až	až	k9
-61	-61	k4
<g/>
°	°	k?
</s>
<s>
Rozloha	rozloha	k1gFnSc1
</s>
<s>
110	#num#	k4
čtverečných	čtverečný	k2eAgInPc2d1
stupňů	stupeň	k1gInPc2
</s>
<s>
Viditelnost	viditelnost	k1gFnSc1
na	na	k7c6
zeměpisné	zeměpisný	k2eAgFnSc6d1
šířce	šířka	k1gFnSc6
</s>
<s>
+25	+25	k4
<g/>
°	°	k?
až	až	k9
-90	-90	k4
<g/>
°	°	k?
</s>
<s>
Nejlepší	dobrý	k2eAgFnSc1d3
pozorovatelnost	pozorovatelnost	k1gFnSc1
v	v	k7c6
ČR	ČR	kA
</s>
<s>
Nepozorovatelné	pozorovatelný	k2eNgNnSc1d1
</s>
<s>
Počet	počet	k1gInSc1
hvězd	hvězda	k1gFnPc2
jasnějších	jasný	k2eAgFnPc2d2
než	než	k8xS
3	#num#	k4
<g/>
m	m	kA
</s>
<s>
3	#num#	k4
</s>
<s>
Nejjasnější	jasný	k2eAgFnSc1d3
hvězda	hvězda	k1gFnSc1
</s>
<s>
Atria	atrium	k1gNnPc4
(	(	kIx(
<g/>
α	α	k?
TrA	tra	k0
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1,91	1,91	k4
<g/>
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
Sousední	sousední	k2eAgNnPc1d1
souhvězdí	souhvězdí	k1gNnPc1
</s>
<s>
PravítkoOltářKružítkoRajka	PravítkoOltářKružítkoRajka	k1gFnSc1
</s>
<s>
Jižní	jižní	k2eAgInSc1d1
trojúhelník	trojúhelník	k1gInSc1
je	být	k5eAaImIp3nS
souhvězdí	souhvězdí	k1gNnSc4
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
obloze	obloha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
rozlohou	rozloha	k1gFnSc7
110	#num#	k4
čtverečných	čtverečný	k2eAgInPc2d1
stupňů	stupeň	k1gInPc2
je	být	k5eAaImIp3nS
šestým	šestý	k4xOgNnSc7
nejmenším	malý	k2eAgNnSc7d3
souhvězdím	souhvězdí	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Významné	významný	k2eAgFnPc1d1
hvězdy	hvězda	k1gFnPc1
</s>
<s>
Hvězda	Hvězda	k1gMnSc1
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
</s>
<s>
α	α	k?
TrA	tra	k0
</s>
<s>
Atria	atrium	k1gNnPc1
</s>
<s>
1,91	1,91	k4
<g/>
m	m	kA
</s>
<s>
β	β	k?
TrA	tra	k0
</s>
<s>
β	β	k?
TrA	tra	k0
</s>
<s>
2,83	2,83	k4
<g/>
m	m	kA
</s>
<s>
γ	γ	k?
TrA	tra	k0
</s>
<s>
γ	γ	k?
TrA	tra	k0
</s>
<s>
2,87	2,87	k4
<g/>
m	m	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Jižní	jižní	k2eAgInSc4d1
trojúhelník	trojúhelník	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1
heslo	heslo	k1gNnSc4
Jižní	jižní	k2eAgInSc1d1
trojúhelník	trojúhelník	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Hvězdy	hvězda	k1gFnPc1
souhvězdí	souhvězdí	k1gNnSc2
Jižního	jižní	k2eAgInSc2d1
trojúhelníku	trojúhelník	k1gInSc2
</s>
<s>
γ	γ	k?
•	•	k?
ε	ε	k?
•	•	k?
β	β	k?
•	•	k?
κ	κ	k?
•	•	k?
δ	δ	k?
•	•	k?
ι	ι	k?
•	•	k?
ζ	ζ	k?
•	•	k?
θ	θ	k?
•	•	k?
η	η	k?
•	•	k?
α	α	k?
(	(	kIx(
<g/>
Atria	atrium	k1gNnSc2
<g/>
)	)	kIx)
Seznam	seznam	k1gInSc1
hvězd	hvězda	k1gFnPc2
souhvězdí	souhvězdí	k1gNnPc2
Jižního	jižní	k2eAgInSc2d1
trojúhelníku	trojúhelník	k1gInSc2
</s>
<s>
Seznam	seznam	k1gInSc1
88	#num#	k4
souhvězdí	souhvězdí	k1gNnPc2
moderní	moderní	k2eAgFnSc2d1
astronomie	astronomie	k1gFnSc2
</s>
<s>
Andromeda	Andromeda	k1gMnSc1
</s>
<s>
Beran	Beran	k1gMnSc1
(	(	kIx(
<g/>
Aries	Aries	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Blíženci	blíženec	k1gMnPc1
(	(	kIx(
<g/>
Gemini	Gemin	k1gMnPc1
<g/>
)	)	kIx)
</s>
<s>
Býk	býk	k1gMnSc1
(	(	kIx(
<g/>
Taurus	Taurus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Cefeus	Cefeus	k1gMnSc1
(	(	kIx(
<g/>
Cepheus	Cepheus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Chameleon	chameleon	k1gMnSc1
(	(	kIx(
<g/>
Chamaeleon	Chamaeleon	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Dalekohled	dalekohled	k1gInSc1
(	(	kIx(
<g/>
Telescopium	Telescopium	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Delfín	Delfín	k1gMnSc1
(	(	kIx(
<g/>
Delphinus	Delphinus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Drak	drak	k1gMnSc1
(	(	kIx(
<g/>
Draco	Draco	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Eridanus	Eridanus	k1gMnSc1
</s>
<s>
Fénix	fénix	k1gMnSc1
(	(	kIx(
<g/>
Phoneix	Phoneix	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Had	had	k1gMnSc1
(	(	kIx(
<g/>
Serpens	Serpens	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hadonoš	hadonoš	k1gMnSc1
(	(	kIx(
<g/>
Ophiuchus	Ophiuchus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Havran	Havran	k1gMnSc1
(	(	kIx(
<g/>
Corvus	Corvus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Herkules	Herkules	k1gMnSc1
(	(	kIx(
<g/>
Hercules	Hercules	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Hodiny	hodina	k1gFnPc1
(	(	kIx(
<g/>
Horologium	horologium	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Holubice	holubice	k1gFnSc1
(	(	kIx(
<g/>
Columba	Columba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Honicí	honicí	k2eAgMnPc1d1
psi	pes	k1gMnPc1
(	(	kIx(
<g/>
Canes	Canes	k1gMnSc1
Vanatici	Vanatice	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Hydra	hydra	k1gFnSc1
</s>
<s>
Indián	Indián	k1gMnSc1
(	(	kIx(
<g/>
Indus	Indus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Jednorožec	jednorožec	k1gMnSc1
(	(	kIx(
<g/>
Monoceros	Monocerosa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Jeřáb	jeřáb	k1gInSc1
(	(	kIx(
<g/>
Grus	Grus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Ještěrka	ještěrka	k1gFnSc1
(	(	kIx(
<g/>
Lacerta	Lacerta	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
(	(	kIx(
<g/>
Corona	Corona	k1gFnSc1
Australis	Australis	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Jižní	jižní	k2eAgInSc1d1
kříž	kříž	k1gInSc1
(	(	kIx(
<g/>
Crux	Crux	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
ryba	ryba	k1gFnSc1
(	(	kIx(
<g/>
Piscis	Piscis	k1gFnSc1
Austrinus	Austrinus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Jižní	jižní	k2eAgInSc1d1
trojúhelník	trojúhelník	k1gInSc1
(	(	kIx(
<g/>
Triangulum	Triangulum	k1gInSc1
Australis	Australis	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Kasiopeja	Kasiopeja	k1gFnSc1
(	(	kIx(
<g/>
Cassiopeia	Cassiopeia	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Kentaur	kentaur	k1gMnSc1
(	(	kIx(
<g/>
Centaurus	Centaurus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Kompas	kompas	k1gInSc1
(	(	kIx(
<g/>
Pyxis	Pyxis	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Koníček	Koníček	k1gMnSc1
(	(	kIx(
<g/>
Equuleus	Equuleus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Kozoroh	Kozoroh	k1gMnSc1
(	(	kIx(
<g/>
Capricornus	Capricornus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Kružítko	kružítko	k1gNnSc1
(	(	kIx(
<g/>
Circinus	Circinus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Labuť	labuť	k1gFnSc1
(	(	kIx(
<g/>
Cygnus	Cygnus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Lev	Lev	k1gMnSc1
(	(	kIx(
<g/>
Leo	Leo	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Lištička	lištička	k1gFnSc1
(	(	kIx(
<g/>
Vulpecula	Vulpecula	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Lodní	lodní	k2eAgInSc1d1
kýl	kýl	k1gInSc1
(	(	kIx(
<g/>
Carina	Carina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Lodní	lodní	k2eAgFnSc1d1
záď	záď	k1gFnSc1
(	(	kIx(
<g/>
Puppis	Puppis	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Lyra	lyra	k1gFnSc1
</s>
<s>
Létající	létající	k2eAgFnSc1d1
ryba	ryba	k1gFnSc1
(	(	kIx(
<g/>
Volans	Volans	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Malíř	malíř	k1gMnSc1
(	(	kIx(
<g/>
Pictor	Pictor	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Malý	Malý	k1gMnSc1
lev	lev	k1gMnSc1
(	(	kIx(
<g/>
Leo	Leo	k1gMnSc1
Minor	minor	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Malý	Malý	k1gMnSc1
medvěd	medvěd	k1gMnSc1
(	(	kIx(
<g/>
Ursa	Ursa	k1gMnSc1
Minor	minor	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Malý	Malý	k1gMnSc1
pes	pes	k1gMnSc1
(	(	kIx(
<g/>
Canis	Canis	k1gFnSc1
Minor	minor	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Malý	malý	k2eAgMnSc1d1
vodní	vodní	k2eAgMnSc1d1
had	had	k1gMnSc1
(	(	kIx(
<g/>
Hydrus	Hydrus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Mečoun	mečoun	k1gMnSc1
(	(	kIx(
<g/>
Dorado	Dorada	k1gFnSc5
<g/>
)	)	kIx)
</s>
<s>
Mikroskop	mikroskop	k1gInSc1
(	(	kIx(
<g/>
Microscopium	Microscopium	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Moucha	moucha	k1gFnSc1
(	(	kIx(
<g/>
Musca	Musca	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Oktant	oktant	k1gInSc1
(	(	kIx(
<g/>
Octans	Octans	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Oltář	Oltář	k1gInSc1
(	(	kIx(
<g/>
Ara	ara	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Orel	Orel	k1gMnSc1
(	(	kIx(
<g/>
Aquila	Aquila	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Orion	orion	k1gInSc1
</s>
<s>
Panna	Panna	k1gFnSc1
(	(	kIx(
<g/>
Virgo	Virgo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Pastýř	pastýř	k1gMnSc1
(	(	kIx(
<g/>
Bootes	Bootes	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Páv	páv	k1gMnSc1
(	(	kIx(
<g/>
Pavo	Pavo	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Pec	Pec	k1gFnSc1
(	(	kIx(
<g/>
Fornax	Fornax	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Pegas	Pegas	k1gMnSc1
(	(	kIx(
<g/>
Pegasus	Pegasus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Perseus	Perseus	k1gMnSc1
</s>
<s>
Plachty	plachta	k1gFnPc1
(	(	kIx(
<g/>
Vela	vela	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
Crater	Crater	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Pravítko	pravítko	k1gNnSc1
(	(	kIx(
<g/>
Norma	Norma	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Rajka	rajka	k1gFnSc1
(	(	kIx(
<g/>
Apus	Apus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Rak	rak	k1gMnSc1
(	(	kIx(
<g/>
Cancer	Cancer	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Ryby	Ryby	k1gFnPc1
(	(	kIx(
<g/>
Pisces	Pisces	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Rydlo	rydlo	k1gNnSc1
(	(	kIx(
<g/>
Caelum	Caelum	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Rys	rys	k1gInSc1
(	(	kIx(
<g/>
Lynx	Lynx	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Severní	severní	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
(	(	kIx(
<g/>
Corona	Corona	k1gFnSc1
Borealis	Borealis	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Sextant	sextant	k1gInSc1
(	(	kIx(
<g/>
Sextans	Sextans	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Sochař	sochař	k1gMnSc1
(	(	kIx(
<g/>
Sculptor	Sculptor	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Střelec	Střelec	k1gMnSc1
(	(	kIx(
<g/>
Sagittarius	Sagittarius	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Síť	síť	k1gFnSc1
(	(	kIx(
<g/>
Reticulum	Reticulum	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Šíp	Šíp	k1gMnSc1
(	(	kIx(
<g/>
Sagitta	Sagitta	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Štír	štír	k1gMnSc1
(	(	kIx(
<g/>
Scorpius	Scorpius	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Štít	štít	k1gInSc1
(	(	kIx(
<g/>
Scutum	Scutum	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Tabulová	tabulový	k2eAgFnSc1d1
hora	hora	k1gFnSc1
(	(	kIx(
<g/>
Mensa	mensa	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Trojúhelník	trojúhelník	k1gInSc1
(	(	kIx(
<g/>
Triangulum	Triangulum	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Tukan	tukan	k1gMnSc1
(	(	kIx(
<g/>
Tucana	Tucana	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Váhy	Váhy	k1gFnPc1
(	(	kIx(
<g/>
Libra	libra	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Velká	velký	k2eAgFnSc1d1
medvědice	medvědice	k1gFnSc1
(	(	kIx(
<g/>
Ursa	Ursa	k1gMnSc1
Major	major	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Velký	velký	k2eAgMnSc1d1
pes	pes	k1gMnSc1
(	(	kIx(
<g/>
Canis	Canis	k1gFnSc1
Major	major	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Velryba	velryba	k1gFnSc1
(	(	kIx(
<g/>
Cetus	Cetus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Vlasy	vlas	k1gInPc1
Bereniky	Berenik	k1gInPc4
(	(	kIx(
<g/>
Coma	Coma	k1gMnSc1
Berenices	Berenices	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Vlk	Vlk	k1gMnSc1
(	(	kIx(
<g/>
Lupus	lupus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Vodnář	vodnář	k1gMnSc1
(	(	kIx(
<g/>
Aquarius	Aquarius	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Vozka	vozka	k1gMnSc1
(	(	kIx(
<g/>
Auriga	Auriga	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Vývěva	vývěva	k1gFnSc1
(	(	kIx(
<g/>
Antlia	Antlia	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Zajíc	Zajíc	k1gMnSc1
(	(	kIx(
<g/>
Lepus	Lepus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Žirafa	žirafa	k1gFnSc1
(	(	kIx(
<g/>
Camelopardalis	Camelopardalis	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Astronomie	astronomie	k1gFnSc1
</s>
