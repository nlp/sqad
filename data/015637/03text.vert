<s>
Stanislao	Stanislao	k1gMnSc1
Mattei	Mattei	k1gMnSc1
</s>
<s>
Stanislao	Stanislao	k1gMnSc1
Mattei	Matte	k1gFnSc2
Základní	základní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Narození	narození	k1gNnSc2
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1750	#num#	k4
Bologna	Bologna	k1gFnSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1825	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
75	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Bologna	Bologna	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
<g/>
,	,	kIx,
muzikolog	muzikolog	k1gMnSc1
a	a	k8xC
hudební	hudební	k2eAgMnSc1d1
teoretik	teoretik	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Stanislao	Stanislao	k1gNnSc1
Mattei	Matte	k1gFnSc2
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1750	#num#	k4
Bologna	Bologna	k1gFnSc1
–	–	k?
17	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1825	#num#	k4
tamtéž	tamtéž	k6eAd1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
italský	italský	k2eAgMnSc1d1
františkánský	františkánský	k2eAgMnSc1d1
řeholník	řeholník	k1gMnSc1
–	–	k?
minorita	minorita	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
proslavil	proslavit	k5eAaPmAgMnS
jako	jako	k9
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
<g/>
,	,	kIx,
pedagog	pedagog	k1gMnSc1
a	a	k8xC
muzikolog	muzikolog	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	s	k7c7
10	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1750	#num#	k4
v	v	k7c6
Bologni	Bologna	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
tehdy	tehdy	k6eAd1
byla	být	k5eAaImAgFnS
součástí	součást	k1gFnSc7
Papežského	papežský	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
řemeslnické	řemeslnický	k2eAgFnSc6d1
rodině	rodina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
místním	místní	k2eAgInSc6d1
kostele	kostel	k1gInSc6
sv.	sv.	kA
Františka	František	k1gMnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
žákem	žák	k1gMnSc7
slavného	slavný	k2eAgMnSc2d1
hudebníka	hudebník	k1gMnSc2
a	a	k8xC
hudebního	hudební	k2eAgMnSc2d1
teoretika	teoretik	k1gMnSc2
Giovanni	Giovanň	k1gMnPc7
Battisty	Battista	k1gMnSc2
Martiniho	Martini	k1gMnSc2
<g/>
,	,	kIx,
známého	známý	k2eAgMnSc2d1
jako	jako	k8xS,k8xC
Padre	Padr	k1gInSc5
Martini	martini	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vzoru	vzor	k1gInSc6
svého	svůj	k3xOyFgMnSc2
učitele	učitel	k1gMnSc2
vstoupil	vstoupit	k5eAaPmAgMnS
také	také	k6eAd1
do	do	k7c2
Řádu	řád	k1gInSc2
menších	malý	k2eAgMnPc2d2
bratří	bratr	k1gMnPc2
konventuálů	konventuál	k1gMnPc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
minoritů	minorita	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
ukončení	ukončení	k1gNnSc6
noviciátu	noviciát	k1gInSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Martiniho	Martini	k1gMnSc4
asistentem	asistent	k1gMnSc7
a	a	k8xC
druhým	druhý	k4xOgMnSc7
dirigentem	dirigent	k1gMnSc7
proslaveného	proslavený	k2eAgInSc2d1
dívčího	dívčí	k2eAgInSc2d1
kostelního	kostelní	k2eAgInSc2d1
sboru	sbor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
Martiniho	Martini	k1gMnSc2
smrti	smrt	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1784	#num#	k4
převzal	převzít	k5eAaPmAgMnS
řízení	řízení	k1gNnSc3
sboru	sbor	k1gInSc2
a	a	k8xC
v	v	k7c6
této	tento	k3xDgFnSc6
funkci	funkce	k1gFnSc6
pracoval	pracovat	k5eAaImAgMnS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1809	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
krátce	krátce	k6eAd1
působil	působit	k5eAaImAgMnS
v	v	k7c6
Padově	Padova	k1gFnSc6
jako	jako	k9
sbormistr	sbormistr	k1gMnSc1
baziliky	bazilika	k1gFnSc2
sv.	sv.	kA
Antonína	Antonín	k1gMnSc4
z	z	k7c2
Padovy	Padova	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
se	se	k3xPyFc4
však	však	k9
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
Bologni	Bologna	k1gFnSc3
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
hudebním	hudební	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
v	v	k7c6
kostele	kostel	k1gInSc6
sv.	sv.	kA
Petronia	Petronium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1796	#num#	k4
obsadila	obsadit	k5eAaPmAgFnS
Bolognu	Bologna	k1gFnSc4
francouzská	francouzský	k2eAgNnPc4d1
revoluční	revoluční	k2eAgNnPc4d1
vojska	vojsko	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
klášter	klášter	k1gInSc1
uzavřen	uzavřít	k5eAaPmNgInS
a	a	k8xC
řeholníci	řeholník	k1gMnPc1
byli	být	k5eAaImAgMnP
nuceni	nucen	k2eAgMnPc1d1
odejít	odejít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mattei	Mattei	k1gNnPc7
pak	pak	k6eAd1
žil	žít	k5eAaImAgMnS
u	u	k7c2
své	svůj	k3xOyFgFnSc2
matky	matka	k1gFnSc2
a	a	k8xC
živil	živit	k5eAaImAgMnS
se	s	k7c7
soukromým	soukromý	k2eAgNnSc7d1
vyučováním	vyučování	k1gNnSc7
hudby	hudba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1799	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
čestným	čestný	k2eAgMnSc7d1
členem	člen	k1gMnSc7
Filharmonické	filharmonický	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
(	(	kIx(
<g/>
Accademia	Accademia	k1gFnSc1
filarmonica	filarmonica	k6eAd1
bolognese	bolognést	k5eAaPmIp3nS
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
letech	léto	k1gNnPc6
1803	#num#	k4
<g/>
,	,	kIx,
1808	#num#	k4
a	a	k8xC
1818	#num#	k4
byl	být	k5eAaImAgMnS
jejím	její	k3xOp3gMnSc7
presidentem	president	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1808	#num#	k4
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
členem	člen	k1gMnSc7
Italské	italský	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
a	a	k8xC
umění	umění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1804	#num#	k4
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
profesorem	profesor	k1gMnSc7
na	na	k7c4
Liceo	Liceo	k1gNnSc4
Musicale	musical	k1gInSc5
di	di	k?
Bologna	Bologna	k1gFnSc1
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
Conservatorio	Conservatorio	k6eAd1
Giovanni	Giovanen	k2eAgMnPc1d1
Battista	Battista	k1gMnSc1
Martini	Martin	k2eAgMnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
jeho	jeho	k3xOp3gMnPc4
žáky	žák	k1gMnPc4
byli	být	k5eAaImAgMnP
mapř	mapř	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gaetano	Gaetana	k1gFnSc5
Donizetti	Donizetť	k1gFnSc5
<g/>
,	,	kIx,
Jevstigněj	Jevstigněj	k1gMnSc1
Fomin	Fomin	k1gMnSc1
<g/>
,	,	kIx,
Angelo	Angela	k1gFnSc5
Mariani	Mariaň	k1gFnSc5
<g/>
,	,	kIx,
Francesco	Francesco	k1gMnSc1
Morlacchi	Morlacch	k1gFnSc2
<g/>
,	,	kIx,
Gioacchino	Gioacchino	k1gNnSc4
Rossini	Rossin	k2eAgMnPc1d1
či	či	k8xC
Giovanni	Giovann	k1gMnPc1
Tadolini	Tadolin	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
Bologni	Bologna	k1gFnSc6
17	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1825	#num#	k4
ve	v	k7c6
věku	věk	k1gInSc6
75	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pochován	pochován	k2eAgMnSc1d1
byl	být	k5eAaImAgInS
na	na	k7c6
hřbitově	hřbitov	k1gInSc6
Cimitero	Cimitero	k1gNnSc1
monumentale	monumental	k1gMnSc5
della	dello	k1gNnSc2
Certosa	Certosa	k1gFnSc1
di	di	k?
Bologna	Bologna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1926	#num#	k4
byly	být	k5eAaImAgInP
jeho	jeho	k3xOp3gInPc1
ostatky	ostatek	k1gInPc1
přeneseny	přenesen	k2eAgInPc1d1
do	do	k7c2
chrámu	chrám	k1gInSc2
sv.	sv.	kA
Františka	František	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Matteiho	Matteize	k6eAd1
hudební	hudební	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
zahrnuje	zahrnovat	k5eAaImIp3nS
převážně	převážně	k6eAd1
liturgickou	liturgický	k2eAgFnSc4d1
hudbu	hudba	k1gFnSc4
a	a	k8xC
hudbu	hudba	k1gFnSc4
s	s	k7c7
náboženskou	náboženský	k2eAgFnSc7d1
tematikou	tematika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dochovalo	dochovat	k5eAaPmAgNnS
se	se	k3xPyFc4
10	#num#	k4
mší	mše	k1gFnPc2
a	a	k8xC
na	na	k7c4
300	#num#	k4
jednotlivých	jednotlivý	k2eAgFnPc2d1
částí	část	k1gFnPc2
mší	mše	k1gFnPc2
<g/>
,	,	kIx,
motet	moteto	k1gNnPc2
a	a	k8xC
jiných	jiný	k2eAgFnPc2d1
liturgických	liturgický	k2eAgFnPc2d1
kompozic	kompozice	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
responsorií	responsorium	k1gNnPc2
pro	pro	k7c4
Svatý	svatý	k2eAgInSc4d1
týden	týden	k1gInSc4
a	a	k8xC
Magnificat	Magnificat	k1gNnSc4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1792	#num#	k4
bylo	být	k5eAaImAgNnS
v	v	k7c6
Bologni	Bologna	k1gFnSc6
uvedeno	uvést	k5eAaPmNgNnS
jeho	jeho	k3xOp3gNnSc1
oratorium	oratorium	k1gNnSc1
La	la	k1gNnSc2
Passione	Passion	k1gInSc5
di	di	k?
Gesù	Gesù	k1gMnSc5
Cristo	Crista	k1gMnSc5
Signor	signor	k1gMnSc1
Nostro	nostro	k2eAgMnSc1d1
(	(	kIx(
<g/>
Utrpení	utrpení	k1gNnSc1
Ježíše	Ježíš	k1gMnSc2
Krista	Kristus	k1gMnSc2
Pána	pán	k1gMnSc2
našeho	náš	k3xOp1gNnSc2
<g/>
)	)	kIx)
na	na	k7c4
text	text	k1gInSc4
Pietra	Pietr	k1gMnSc2
Metastasia	Metastasius	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přepracovaná	přepracovaný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
pak	pak	k6eAd1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1806	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Komponoval	komponovat	k5eAaImAgMnS
také	také	k9
světskou	světský	k2eAgFnSc4d1
hudbu	hudba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
zkomponoval	zkomponovat	k5eAaPmAgMnS
27	#num#	k4
symfonií	symfonie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pět	pět	k4xCc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
vyšlo	vyjít	k5eAaPmAgNnS
v	v	k7c6
moderním	moderní	k2eAgNnSc6d1
vydání	vydání	k1gNnSc6
v	v	k7c6
nakladatelství	nakladatelství	k1gNnSc6
RM	RM	kA
Longyear	Longyear	k1gMnSc1
<g/>
,	,	kIx,
New	New	k1gMnSc1
York-Londýn	York-Londýn	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
jeho	jeho	k3xOp3gFnPc2
teoretických	teoretický	k2eAgFnPc2d1
prací	práce	k1gFnPc2
je	být	k5eAaImIp3nS
významný	významný	k2eAgInSc4d1
zejména	zejména	k9
spis	spis	k1gInSc4
„	„	k?
<g/>
Pratica	Pratic	k1gInSc2
d	d	k?
<g/>
'	'	kIx"
<g/>
accompagnamento	accompagnamento	k1gNnSc1
sopra	sopra	k6eAd1
bassi	basse	k1gFnSc3
numerati	numerat	k1gMnPc1
<g/>
,	,	kIx,
e	e	k0
contrapunti	contrapunti	k1gNnPc1
a	a	k8xC
più	più	k?
voci	voci	k6eAd1
sulla	sulla	k6eAd1
scala	scát	k5eAaImAgFnS
ascendente	ascendent	k1gMnSc5
e	e	k0
discendente	discendent	k1gMnSc5
maggiore	maggior	k1gInSc5
e	e	k0
minore	minor	k1gMnSc5
<g/>
,	,	kIx,
con	con	k?
diverse	diverse	k1gFnSc1
fughe	fughe	k6eAd1
a	a	k8xC
4	#num#	k4
e	e	k0
a	a	k8xC
8	#num#	k4
<g/>
“	“	k?
z	z	k7c2
roku	rok	k1gInSc2
1788	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Filippo	Filippa	k1gFnSc5
Canuti	Canuť	k1gFnSc5
<g/>
:	:	kIx,
Vita	vit	k2eAgMnSc4d1
di	di	k?
Stanislao	Stanislao	k6eAd1
Mattei	Mattei	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bologna	Bologna	k1gFnSc1
<g/>
,	,	kIx,
1829	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Juste	Juste	k5eAaPmIp2nP
Adrien	Adriena	k1gFnPc2
La-Fage	La-Fage	k1gNnSc2
<g/>
:	:	kIx,
Memoria	Memorium	k1gNnSc2
intorno	intorno	k1gNnSc1
la	la	k0
vita	vit	k2eAgFnSc1d1
e	e	k0
le	le	k?
opere	oprat	k5eAaPmIp3nS
di	di	k?
Sanislao	Sanislaa	k1gMnSc5
Mattei	Matte	k1gMnSc5
<g/>
,	,	kIx,
p.	p.	k?
minorita	minorita	k1gFnSc1
bolognese	bolognese	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bologna	Bologna	k1gFnSc1
<g/>
,	,	kIx,
pei	pei	k?
tipi	tip	k1gFnSc2
di	di	k?
Jacopo	Jacopa	k1gFnSc5
Marsigli	Marsigl	k1gMnPc7
<g/>
,	,	kIx,
1840	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Volně	volně	k6eAd1
přístupné	přístupný	k2eAgFnPc4d1
partitury	partitura	k1gFnPc4
děl	dělo	k1gNnPc2
od	od	k7c2
Stanislaa	Stanisla	k1gInSc2
Matteiho	Mattei	k1gMnSc2
v	v	k7c6
projektu	projekt	k1gInSc6
IMSLP	IMSLP	kA
</s>
<s>
Život	život	k1gInSc1
a	a	k8xC
dílo	dílo	k1gNnSc1
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20000603934	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
134636716	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1475	#num#	k4
6907	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2020063106	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
79200339	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2020063106	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Itálie	Itálie	k1gFnSc1
|	|	kIx~
Hudba	hudba	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
