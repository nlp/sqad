<s>
Mučedník	mučedník	k1gMnSc1	mučedník
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
<g/>
:	:	kIx,	:
μ	μ	k?	μ
<g/>
,	,	kIx,	,
mártys	mártys	k6eAd1	mártys
<g/>
;	;	kIx,	;
doslova	doslova	k6eAd1	doslova
<g/>
:	:	kIx,	:
svědek	svědek	k1gMnSc1	svědek
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
umučená	umučený	k2eAgFnSc1d1	umučená
nebo	nebo	k8xC	nebo
zemřela	zemřít	k5eAaPmAgFnS	zemřít
za	za	k7c7	za
svojí	svojit	k5eAaImIp3nS	svojit
víru	vír	k1gInSc2	vír
nebo	nebo	k8xC	nebo
ideu	idea	k1gFnSc4	idea
<g/>
.	.	kIx.	.
</s>
<s>
Mučedník	mučedník	k1gMnSc1	mučedník
<g/>
,	,	kIx,	,
křesťan	křesťan	k1gMnSc1	křesťan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
víru	víra	k1gFnSc4	víra
položil	položit	k5eAaPmAgMnS	položit
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
křesťanství	křesťanství	k1gNnSc6	křesťanství
prototypem	prototyp	k1gInSc7	prototyp
světce	světec	k1gMnSc2	světec
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
slov	slovo	k1gNnPc2	slovo
Písma	písmo	k1gNnSc2	písmo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nikdo	nikdo	k3yNnSc1	nikdo
nemá	mít	k5eNaImIp3nS	mít
větší	veliký	k2eAgFnSc4d2	veliký
lásku	láska	k1gFnSc4	láska
než	než	k8xS	než
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
položí	položit	k5eAaPmIp3nS	položit
život	život	k1gInSc4	život
za	za	k7c4	za
své	svůj	k3xOyFgMnPc4	svůj
přátele	přítel	k1gMnPc4	přítel
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
15,13	[number]	k4	15,13
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
zpočátku	zpočátku	k6eAd1	zpočátku
také	také	k9	také
jediný	jediný	k2eAgInSc1d1	jediný
typ	typ	k1gInSc1	typ
světců	světec	k1gMnPc2	světec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
uctíváni	uctívat	k5eAaImNgMnP	uctívat
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
začali	začít	k5eAaPmAgMnP	začít
být	být	k5eAaImF	být
uctíváni	uctíván	k2eAgMnPc1d1	uctíván
i	i	k8xC	i
další	další	k2eAgMnPc1d1	další
světci	světec	k1gMnPc1	světec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nezemřeli	zemřít	k5eNaPmAgMnP	zemřít
mučednickou	mučednický	k2eAgFnSc7d1	mučednická
smrtí	smrt	k1gFnSc7	smrt
<g/>
,	,	kIx,	,
především	především	k9	především
mniši	mnich	k1gMnPc1	mnich
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc4	jejichž
život	život	k1gInSc4	život
bylo	být	k5eAaImAgNnS	být
"	"	kIx"	"
<g/>
pomalé	pomalý	k2eAgNnSc4d1	pomalé
utrpení	utrpení	k1gNnSc4	utrpení
a	a	k8xC	a
odříkání	odříkání	k1gNnSc4	odříkání
z	z	k7c2	z
lásky	láska	k1gFnSc2	láska
ke	k	k7c3	k
Kristu	Krista	k1gFnSc4	Krista
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
opět	opět	k6eAd1	opět
jistý	jistý	k2eAgInSc4d1	jistý
druh	druh	k1gInSc4	druh
mučednictví	mučednictví	k1gNnSc2	mučednictví
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dar	dar	k1gInSc1	dar
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
chápán	chápat	k5eAaImNgInS	chápat
jako	jako	k8xS	jako
největší	veliký	k2eAgInSc1d3	veliký
dar	dar	k1gInSc1	dar
a	a	k8xC	a
oběť	oběť	k1gFnSc1	oběť
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mučednictví	mučednictví	k1gNnSc1	mučednictví
pro	pro	k7c4	pro
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
víru	víra	k1gFnSc4	víra
významné	významný	k2eAgFnPc1d1	významná
i	i	k8xC	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vrcholný	vrcholný	k2eAgInSc1d1	vrcholný
akt	akt	k1gInSc1	akt
svědectví	svědectví	k1gNnSc2	svědectví
víře	víra	k1gFnSc6	víra
v	v	k7c4	v
Krista	Kristus	k1gMnSc4	Kristus
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
mučedníka	mučedník	k1gMnSc4	mučedník
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
termín	termín	k1gInSc1	termín
martyr	martyr	k1gMnSc1	martyr
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
<g/>
μ	μ	k?	μ
martys	martys	k1gInSc1	martys
"	"	kIx"	"
<g/>
svědek	svědek	k1gMnSc1	svědek
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mučednictví	mučednictví	k1gNnSc1	mučednictví
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
křesťanství	křesťanství	k1gNnSc6	křesťanství
chápáno	chápat	k5eAaImNgNnS	chápat
v	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
smyslu	smysl	k1gInSc6	smysl
jako	jako	k9	jako
povinnost	povinnost	k1gFnSc4	povinnost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
od	od	k7c2	od
křesťanů	křesťan	k1gMnPc2	křesťan
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
svědectví	svědectví	k1gNnSc3	svědectví
své	svůj	k3xOyFgFnSc2	svůj
víry	víra	k1gFnSc2	víra
obětovali	obětovat	k5eAaBmAgMnP	obětovat
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
<g/>
-li	i	k?	-li
to	ten	k3xDgNnSc4	ten
třeba	třeba	k6eAd1	třeba
<g/>
,	,	kIx,	,
i	i	k9	i
vlastní	vlastní	k2eAgInSc4d1	vlastní
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgMnSc7	první
mučedníkem	mučedník	k1gMnSc7	mučedník
sám	sám	k3xTgMnSc1	sám
Kristus	Kristus	k1gMnSc1	Kristus
(	(	kIx(	(
<g/>
Zj	Zj	k1gFnSc1	Zj
1,5	[number]	k4	1,5
<g/>
)	)	kIx)	)
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
křesťan	křesťan	k1gMnSc1	křesťan
<g/>
,	,	kIx,	,
nakolik	nakolik	k6eAd1	nakolik
vydává	vydávat	k5eAaImIp3nS	vydávat
svědectví	svědectví	k1gNnSc4	svědectví
Kristu	Krista	k1gFnSc4	Krista
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
v	v	k7c6	v
křesťanských	křesťanský	k2eAgFnPc6d1	křesťanská
církvích	církev	k1gFnPc6	církev
tento	tento	k3xDgInSc4	tento
termín	termín	k1gInSc1	termín
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
položil	položit	k5eAaPmAgMnS	položit
život	život	k1gInSc4	život
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
víru	víra	k1gFnSc4	víra
v	v	k7c4	v
Krista	Kristus	k1gMnSc4	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
po	po	k7c6	po
delším	dlouhý	k2eAgNnSc6d2	delší
či	či	k8xC	či
kratším	krátký	k2eAgNnSc6d2	kratší
mučení	mučení	k1gNnSc6	mučení
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
přimět	přimět	k5eAaPmF	přimět
křesťana	křesťan	k1gMnSc4	křesťan
vzdát	vzdát	k5eAaPmF	vzdát
se	se	k3xPyFc4	se
své	svůj	k3xOyFgFnSc2	svůj
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
mučení	mučení	k1gNnSc6	mučení
těchto	tento	k3xDgFnPc2	tento
svatých	svatá	k1gFnPc2	svatá
často	často	k6eAd1	často
vznikaly	vznikat	k5eAaImAgFnP	vznikat
legendy	legenda	k1gFnPc1	legenda
a	a	k8xC	a
zápisy	zápis	k1gInPc1	zápis
ze	z	k7c2	z
soudních	soudní	k2eAgInPc2d1	soudní
šetření	šetření	k1gNnSc2	šetření
proti	proti	k7c3	proti
nim	on	k3xPp3gFnPc3	on
byly	být	k5eAaImAgInP	být
tradovány	tradován	k2eAgInPc1d1	tradován
a	a	k8xC	a
literárně	literárně	k6eAd1	literárně
rozvíjeny	rozvíjet	k5eAaImNgFnP	rozvíjet
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
akt	akt	k1gInSc1	akt
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známá	k1gFnSc1	známá
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
"	"	kIx"	"
<g/>
Akta	akta	k1gNnPc1	akta
sv.	sv.	kA	sv.
Perpetuy	Perpetua	k1gFnSc2	Perpetua
a	a	k8xC	a
Felicity	Felicitas	k1gFnSc2	Felicitas
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
terminologie	terminologie	k1gFnSc1	terminologie
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
mučedníka	mučedník	k1gMnSc2	mučedník
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
vyznavače	vyznavač	k1gMnSc2	vyznavač
(	(	kIx(	(
<g/>
víry	vír	k1gInPc1	vír
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
víru	víra	k1gFnSc4	víra
sice	sice	k8xC	sice
trpěli	trpět	k5eAaImAgMnP	trpět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepoložili	položit	k5eNaPmAgMnP	položit
za	za	k7c4	za
ni	on	k3xPp3gFnSc4	on
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
pronásledování	pronásledování	k1gNnSc2	pronásledování
proti	proti	k7c3	proti
nim	on	k3xPp3gFnPc3	on
nedošlo	dojít	k5eNaPmAgNnS	dojít
až	až	k9	až
tak	tak	k6eAd1	tak
daleko	daleko	k6eAd1	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
titul	titul	k1gInSc1	titul
se	se	k3xPyFc4	se
uděluje	udělovat	k5eAaImIp3nS	udělovat
těm	ten	k3xDgMnPc3	ten
světcům	světec	k1gMnPc3	světec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
prokazatelně	prokazatelně	k6eAd1	prokazatelně
položili	položit	k5eAaPmAgMnP	položit
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
za	za	k7c4	za
víru	víra	k1gFnSc4	víra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
církvi	církev	k1gFnSc6	církev
je	být	k5eAaImIp3nS	být
liturgická	liturgický	k2eAgFnSc1d1	liturgická
barva	barva	k1gFnSc1	barva
náležející	náležející	k2eAgFnSc1d1	náležející
památce	památka	k1gFnSc3	památka
mučedníků	mučedník	k1gMnPc2	mučedník
červená	červenat	k5eAaImIp3nS	červenat
<g/>
,	,	kIx,	,
barva	barva	k1gFnSc1	barva
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
nějaký	nějaký	k3yIgMnSc1	nějaký
svatý	svatý	k1gMnSc1	svatý
více	hodně	k6eAd2	hodně
"	"	kIx"	"
<g/>
titulů	titul	k1gInPc2	titul
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
např.	např.	kA	např.
"	"	kIx"	"
<g/>
panna	panna	k1gFnSc1	panna
a	a	k8xC	a
mučednice	mučednice	k1gFnSc1	mučednice
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
stojí	stát	k5eAaImIp3nS	stát
vždy	vždy	k6eAd1	vždy
aspekt	aspekt	k1gInSc4	aspekt
mučednictví	mučednictví	k1gNnSc2	mučednictví
<g/>
.	.	kIx.	.
</s>
<s>
P.	P.	kA	P.
Kitzler	Kitzler	k1gMnSc1	Kitzler
(	(	kIx(	(
<g/>
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Příběhy	příběh	k1gInPc1	příběh
raně	raně	k6eAd1	raně
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
mučedníků	mučedník	k1gMnPc2	mučedník
<g/>
.	.	kIx.	.
</s>
<s>
Výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
nejstarší	starý	k2eAgFnSc2d3	nejstarší
latinské	latinský	k2eAgFnSc2d1	Latinská
a	a	k8xC	a
řecké	řecký	k2eAgFnSc2d1	řecká
marytrologické	marytrologický	k2eAgFnSc2d1	marytrologický
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
úvodní	úvodní	k2eAgFnSc2d1	úvodní
studie	studie	k1gFnSc2	studie
J.	J.	kA	J.
Šubrt	Šubrt	k1gMnSc1	Šubrt
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
I.	I.	kA	I.
Adámková	Adámková	k1gFnSc1	Adámková
<g/>
,	,	kIx,	,
P.	P.	kA	P.
Dudzik	Dudzik	k1gInSc1	Dudzik
a	a	k8xC	a
P.	P.	kA	P.
Kitzler	Kitzler	k1gInSc1	Kitzler
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
989	[number]	k4	989
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
P.	P.	kA	P.
Kitzler	Kitzler	k1gMnSc1	Kitzler
(	(	kIx(	(
<g/>
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Příběhy	příběh	k1gInPc1	příběh
raně	raně	k6eAd1	raně
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
mučedníků	mučedník	k1gMnPc2	mučedník
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Nauka	nauka	k1gFnSc1	nauka
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	se	k3xPyFc4	se
mučedníky	mučedník	k1gMnPc4	mučedník
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
martyrologie	martyrologie	k1gFnSc1	martyrologie
<g/>
.	.	kIx.	.
</s>
<s>
Výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
latinské	latinský	k2eAgFnSc2d1	Latinská
a	a	k8xC	a
řecké	řecký	k2eAgFnSc2d1	řecká
martyrologické	martyrologický	k2eAgFnSc2d1	martyrologický
literatury	literatura	k1gFnSc2	literatura
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
úvodní	úvodní	k2eAgFnSc2d1	úvodní
studie	studie	k1gFnSc2	studie
J.	J.	kA	J.
Šubrt	Šubrt	k1gMnSc1	Šubrt
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
I.	I.	kA	I.
Adámková	Adámková	k1gFnSc1	Adámková
<g/>
,	,	kIx,	,
P.	P.	kA	P.
Dudzik	Dudzik	k1gInSc1	Dudzik
a	a	k8xC	a
P.	P.	kA	P.
Kitzler	Kitzler	k1gInSc1	Kitzler
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7429	[number]	k4	7429
<g/>
-	-	kIx~	-
<g/>
187	[number]	k4	187
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Mučednický	mučednický	k2eAgInSc4d1	mučednický
komplex	komplex	k1gInSc4	komplex
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mučedník	mučedník	k1gMnSc1	mučedník
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
ELSNER	ELSNER	kA	ELSNER
<g/>
,	,	kIx,	,
J.	J.	kA	J.
T.	T.	kA	T.
/	/	kIx~	/
Martyrologium	martyrologium	k1gNnSc1	martyrologium
Bohemicum	Bohemicum	k1gNnSc1	Bohemicum
oder	odra	k1gFnPc2	odra
die	die	k?	die
böhmischen	böhmischna	k1gFnPc2	böhmischna
Verfolgungsgeschichte	Verfolgungsgeschicht	k1gInSc5	Verfolgungsgeschicht
vom	vom	k?	vom
Jahre	Jahr	k1gInSc5	Jahr
894	[number]	k4	894
bis	bis	k?	bis
1632	[number]	k4	1632
-	-	kIx~	-
mučedníci	mučedník	k1gMnPc1	mučedník
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
dějinách	dějiny	k1gFnPc6	dějiny
-	-	kIx~	-
dostupné	dostupný	k2eAgFnPc4d1	dostupná
v	v	k7c6	v
Digitálnej	Digitálnej	k1gFnSc6	Digitálnej
knižnici	knižnice	k1gFnSc4	knižnice
UKB	UKB	kA	UKB
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
mučedník	mučedník	k1gMnSc1	mučedník
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Téma	téma	k1gNnSc1	téma
Mučedník	mučedník	k1gMnSc1	mučedník
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
