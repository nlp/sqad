<s>
Enfer	Enfer	k1gMnSc1
</s>
<s>
Rytina	rytina	k1gFnSc1
z	z	k7c2
knihy	kniha	k1gFnSc2
Thérè	Thérè	k1gMnSc2
philosophe	philosoph	k1gMnSc2
(	(	kIx(
<g/>
1785	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Enfer	Enfer	k1gInSc1
[	[	kIx(
<g/>
ɑ	ɑ	k?
<g/>
]	]	kIx)
(	(	kIx(
<g/>
česky	česky	k6eAd1
peklo	peklo	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc1
pro	pro	k7c4
dvě	dva	k4xCgNnPc4
zvláštní	zvláštní	k2eAgNnPc4d1
oddělení	oddělení	k1gNnPc4
Francouzské	francouzský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
uchovávají	uchovávat	k5eAaImIp3nP
literaturu	literatura	k1gFnSc4
a	a	k8xC
další	další	k2eAgInPc4d1
dokumenty	dokument	k1gInPc4
s	s	k7c7
erotickým	erotický	k2eAgInSc7d1
a	a	k8xC
pornografickým	pornografický	k2eAgInSc7d1
obsahem	obsah	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
vznikly	vzniknout	k5eAaPmAgFnP
dvě	dva	k4xCgFnPc1
skupiny	skupina	k1gFnPc1
těchto	tento	k3xDgMnPc2
dokumentů	dokument	k1gInPc2
-	-	kIx~
v	v	k7c6
oddělení	oddělení	k1gNnSc6
grafiky	grafika	k1gFnSc2
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
uložené	uložený	k2eAgInPc1d1
v	v	k7c6
původním	původní	k2eAgNnSc6d1
sídle	sídlo	k1gNnSc6
na	na	k7c6
Rue	Rue	k1gFnSc6
de	de	k?
Richelieu	Richelieu	k1gMnSc4
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
oddělení	oddělení	k1gNnSc6
tisků	tisk	k1gInPc2
(	(	kIx(
<g/>
v	v	k7c6
nové	nový	k2eAgFnSc6d1
budově	budova	k1gFnSc6
na	na	k7c6
Rue	Rue	k1gFnSc6
de	de	k?
Tolbiac	Tolbiac	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
svými	svůj	k3xOyFgInPc7
zhruba	zhruba	k6eAd1
2000	#num#	k4
publikacemi	publikace	k1gFnPc7
od	od	k7c2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
do	do	k7c2
současnosti	současnost	k1gFnSc2
patří	patřit	k5eAaImIp3nS
k	k	k7c3
nejvýznamnějším	významný	k2eAgMnPc3d3
neveřejným	veřejný	k2eNgMnPc3d1
knižním	knižní	k2eAgMnPc3d1
fondům	fond	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skupina	skupina	k1gFnSc1
vyčleněných	vyčleněný	k2eAgFnPc2d1
knih	kniha	k1gFnPc2
v	v	k7c6
oddělení	oddělení	k1gNnSc6
tisků	tisk	k1gInPc2
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
součástí	součást	k1gFnSc7
fondu	fond	k1gInSc2
vzácných	vzácný	k2eAgFnPc2d1
knih	kniha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
tvorbě	tvorba	k1gFnSc6
jejího	její	k3xOp3gInSc2
prvního	první	k4xOgInSc2
katalogu	katalog	k1gInSc2
se	se	k3xPyFc4
podílel	podílet	k5eAaImAgMnS
i	i	k9
Guillaume	Guillaum	k1gInSc5
Apollinaire	Apollinair	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Již	již	k9
katalog	katalog	k1gInSc4
z	z	k7c2
konce	konec	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
v	v	k7c6
tehdejší	tehdejší	k2eAgFnSc6d1
královské	královský	k2eAgFnSc6d1
knihovně	knihovna	k1gFnSc6
odděloval	oddělovat	k5eAaImAgMnS
knihy	kniha	k1gFnSc2
„	„	k?
<g/>
dobré	dobrý	k2eAgNnSc1d1
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
závadné	závadný	k2eAgFnSc2d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heterodoxní	heterodoxní	k2eAgFnPc4d1
teologická	teologický	k2eAgNnPc4d1
pojednání	pojednání	k1gNnPc4
nebo	nebo	k8xC
literatura	literatura	k1gFnSc1
a	a	k8xC
romány	román	k1gInPc1
o	o	k7c6
lásce	láska	k1gFnSc6
a	a	k8xC
dobrodružné	dobrodružný	k2eAgInPc1d1
příběhy	příběh	k1gInPc1
byly	být	k5eAaImAgInP
odlišeny	odlišen	k2eAgInPc1d1
zvláštní	zvláštní	k2eAgFnSc7d1
značkou	značka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
texty	text	k1gInPc1
byly	být	k5eAaImAgInP
z	z	k7c2
větší	veliký	k2eAgFnSc2d2
části	část	k1gFnSc2
uloženy	uložen	k2eAgInPc4d1
v	v	k7c6
„	„	k?
<g/>
kabinetu	kabinet	k1gInSc6
<g/>
“	“	k?
(	(	kIx(
<g/>
pravděpodobně	pravděpodobně	k6eAd1
speciální	speciální	k2eAgFnSc6d1
skříni	skříň	k1gFnSc6
<g/>
)	)	kIx)
a	a	k8xC
jejich	jejich	k3xOp3gInSc1
počet	počet	k1gInSc1
až	až	k9
do	do	k7c2
Francouzské	francouzský	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1789	#num#	k4
nepřesáhl	přesáhnout	k5eNaPmAgMnS
padesát	padesát	k4xCc1
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Není	být	k5eNaImIp3nS
přesně	přesně	k6eAd1
známo	znám	k2eAgNnSc1d1
<g/>
,	,	kIx,
za	za	k7c2
jakých	jaký	k3yIgFnPc2,k3yQgFnPc2,k3yRgFnPc2
okolností	okolnost	k1gFnPc2
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
ke	k	k7c3
vzniku	vznik	k1gInSc3
zvláštního	zvláštní	k2eAgNnSc2d1
oddělení	oddělení	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
bylo	být	k5eAaImAgNnS
charakterizováno	charakterizovat	k5eAaBmNgNnS
jako	jako	k9
„	„	k?
<g/>
fort	fort	k?
mauvais	mauvais	k1gInSc1
<g/>
,	,	kIx,
mais	mais	k6eAd1
quelquefois	quelquefois	k1gFnSc1
trè	trè	k1gInSc4
pour	pour	k1gInSc1
les	les	k1gInSc1
bibliophiles	bibliophiles	k1gInSc1
<g/>
,	,	kIx,
et	et	k?
de	de	k?
grand	grand	k1gMnSc1
valeur	valeur	k1gMnSc1
vénal	vénal	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
mimořádně	mimořádně	k6eAd1
trestuhodné	trestuhodný	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
velmi	velmi	k6eAd1
cenné	cenný	k2eAgInPc1d1
pro	pro	k7c4
bibliofily	bibliofil	k1gMnPc4
a	a	k8xC
s	s	k7c7
velkou	velký	k2eAgFnSc7d1
komerční	komerční	k2eAgFnSc7d1
hodnotou	hodnota	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1836	#num#	k4
knihovna	knihovna	k1gFnSc1
založila	založit	k5eAaPmAgFnS
sbírku	sbírka	k1gFnSc4
vzácných	vzácný	k2eAgFnPc2d1
a	a	k8xC
cenných	cenný	k2eAgFnPc2d1
knih	kniha	k1gFnPc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1844	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
jejích	její	k3xOp3gInPc6
inventářích	inventář	k1gInPc6
poprvé	poprvé	k6eAd1
objevil	objevit	k5eAaPmAgInS
termín	termín	k1gInSc1
Enfer	Enfero	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
První	první	k4xOgFnSc1
knižní	knižní	k2eAgFnSc1d1
sbírka	sbírka	k1gFnSc1
s	s	k7c7
tímto	tento	k3xDgNnSc7
označením	označení	k1gNnSc7
se	se	k3xPyFc4
připisuje	připisovat	k5eAaImIp3nS
klášteru	klášter	k1gInSc3
v	v	k7c6
Paříži	Paříž	k1gFnSc6
v	v	k7c6
Rue	Rue	k1gFnSc6
Saint-Honoré	Saint-Honorý	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mniši	mnich	k1gMnPc1
ironicky	ironicky	k6eAd1
označovali	označovat	k5eAaImAgMnP
soubor	soubor	k1gInSc4
shromážděných	shromážděný	k2eAgInPc2d1
protestantských	protestantský	k2eAgInPc2d1
spisů	spis	k1gInPc2
jako	jako	k8xC,k8xS
peklo	peklo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Termín	termín	k1gInSc1
v	v	k7c6
tomto	tento	k3xDgInSc6
významu	význam	k1gInSc6
se	se	k3xPyFc4
všeobecně	všeobecně	k6eAd1
ujal	ujmout	k5eAaPmAgInS
ve	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
dokládá	dokládat	k5eAaImIp3nS
Laroussova	Laroussův	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
Grand	grand	k1gMnSc1
dictionnaire	dictionnair	k1gInSc5
universel	universet	k5eAaImAgInS,k5eAaBmAgInS,k5eAaPmAgInS
du	du	k?
XIXe	XIXe	k1gInSc1
siè	siè	k1gFnSc1
(	(	kIx(
<g/>
Paříž	Paříž	k1gFnSc1
1866	#num#	k4
<g/>
–	–	k?
<g/>
1877	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
jej	on	k3xPp3gMnSc4
definuje	definovat	k5eAaBmIp3nS
jako	jako	k9
„	„	k?
<g/>
endroit	endroit	k5eAaImF,k5eAaPmF,k5eAaBmF
fermé	fermý	k2eAgFnPc4d1
d	d	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
'	'	kIx"
<g/>
une	une	k?
bibliothè	bibliothè	k1gInSc1
<g/>
,	,	kIx,
où	où	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
on	on	k3xPp3gMnSc1
tient	tient	k1gMnSc1
les	les	k1gInSc1
livres	livres	k1gMnSc1
dont	dont	k1gMnSc1
on	on	k3xPp3gMnSc1
pense	pense	k1gFnPc4
que	que	k?
la	la	k1gNnSc1
lecture	lectur	k1gMnSc5
est	est	k?
dangereuse	dangereuse	k1gFnSc2
<g/>
;	;	kIx,
exemple	exemple	k6eAd1
<g/>
:	:	kIx,
l	l	kA
<g/>
'	'	kIx"
<g/>
Enfer	Enfer	k1gMnSc1
de	de	k?
la	la	k1gNnSc4
Bibliothè	Bibliothè	k1gFnSc2
nationale	nationale	k6eAd1
<g/>
“	“	k?
(	(	kIx(
<g/>
uzavřené	uzavřený	k2eAgNnSc1d1
oddělení	oddělení	k1gNnSc1
knihovny	knihovna	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
knihy	kniha	k1gFnPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
četba	četba	k1gFnSc1
je	být	k5eAaImIp3nS
nebezpečná	bezpečný	k2eNgFnSc1d1
<g/>
;	;	kIx,
příklad	příklad	k1gInSc1
<g/>
:	:	kIx,
Enfer	Enfer	k1gInSc1
v	v	k7c6
Národní	národní	k2eAgFnSc6d1
knihovně	knihovna	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Také	také	k9
jiné	jiný	k2eAgFnPc1d1
významné	významný	k2eAgFnPc1d1
knihovny	knihovna	k1gFnPc1
obdobně	obdobně	k6eAd1
vyčlenily	vyčlenit	k5eAaPmAgFnP
erotickou	erotický	k2eAgFnSc4d1
literaturu	literatura	k1gFnSc4
do	do	k7c2
zvláštních	zvláštní	k2eAgInPc2d1
fondů	fond	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
se	se	k3xPyFc4
nazývají	nazývat	k5eAaImIp3nP
např.	např.	kA
Private	Privat	k1gMnSc5
Case	Casus	k1gMnSc5
(	(	kIx(
<g/>
soukromá	soukromý	k2eAgFnSc1d1
záležitost	záležitost	k1gFnSc1
<g/>
)	)	kIx)
v	v	k7c6
Britském	britský	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
<g/>
,	,	kIx,
*****	*****	k?
v	v	k7c6
New	New	k1gFnSc6
York	York	k1gInSc1
Public	publicum	k1gNnPc2
Library	Librara	k1gFnSc2
<g/>
,	,	kIx,
Δ	Δ	k?
(	(	kIx(
<g/>
řecká	řecký	k2eAgFnSc1d1
delta	delta	k1gFnSc1
<g/>
)	)	kIx)
v	v	k7c6
Kongresové	kongresový	k2eAgFnSc6d1
knihovně	knihovna	k1gFnSc6
nebo	nebo	k8xC
Φ	Φ	k?
(	(	kIx(
<g/>
řecké	řecký	k2eAgFnSc2d1
fí	fí	k0
<g/>
)	)	kIx)
v	v	k7c4
Bodleian	Bodleian	k1gInSc4
Library	Librara	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Fond	fond	k1gInSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
katalogizace	katalogizace	k1gFnSc1
</s>
<s>
Po	po	k7c6
založení	založení	k1gNnSc6
oddělení	oddělení	k1gNnSc4
Enfer	Enfra	k1gFnPc2
se	se	k3xPyFc4
započalo	započnout	k5eAaPmAgNnS
s	s	k7c7
podchycením	podchycení	k1gNnSc7
uložených	uložený	k2eAgFnPc2d1
knih	kniha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
očekávání	očekávání	k1gNnSc3
se	se	k3xPyFc4
zde	zde	k6eAd1
nacházelo	nacházet	k5eAaImAgNnS
jen	jen	k9
málo	málo	k4c1
publikací	publikace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1848-1850	1848-1850	k4
byl	být	k5eAaImAgInS
Enfer	Enfer	k1gInSc1
předmětem	předmět	k1gInSc7
veřejné	veřejný	k2eAgFnSc2d1
diskuse	diskuse	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
knihovna	knihovna	k1gFnSc1
obviňována	obviňovat	k5eAaImNgFnS
z	z	k7c2
nedbalé	dbalý	k2eNgFnSc2d1
správy	správa	k1gFnSc2
a	a	k8xC
následné	následný	k2eAgFnSc2d1
ztráty	ztráta	k1gFnSc2
knih	kniha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1886	#num#	k4
vzrostl	vzrůst	k5eAaPmAgInS
počet	počet	k1gInSc1
titulů	titul	k1gInPc2
přes	přes	k7c4
700	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
Enfer	Enfer	k1gInSc1
zahrnoval	zahrnovat	k5eAaImAgInS
již	již	k9
na	na	k7c4
2000	#num#	k4
publikací	publikace	k1gFnPc2
od	od	k7c2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
po	po	k7c4
současnost	současnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgNnSc1
zpracování	zpracování	k1gNnSc1
fondu	fond	k1gInSc2
Enfer	Enfer	k1gInSc1
z	z	k7c2
konce	konec	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byly	být	k5eAaImAgFnP
knihy	kniha	k1gFnPc1
označeny	označen	k2eAgFnPc1d1
značkou	značka	k1gFnSc7
Y	Y	kA
<g/>
2	#num#	k4
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
přepracováno	přepracovat	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1886	#num#	k4
a	a	k8xC
položky	položka	k1gFnPc1
byly	být	k5eAaImAgFnP
publikovány	publikovat	k5eAaBmNgFnP
od	od	k7c2
roku	rok	k1gInSc2
1896	#num#	k4
v	v	k7c6
souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
Národní	národní	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1913	#num#	k4
Apollinaire	Apollinair	k1gInSc5
Guillaume	Guillaum	k1gInSc5
spolu	spolu	k6eAd1
s	s	k7c7
dalšími	další	k2eAgMnPc7d1
vytvořil	vytvořit	k5eAaPmAgMnS
speciální	speciální	k2eAgInSc4d1
katalog	katalog	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
obnášel	obnášet	k5eAaImAgInS
854	#num#	k4
titulů	titul	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kritický	kritický	k2eAgInSc1d1
katalog	katalog	k1gInSc1
Pascala	Pascal	k1gMnSc2
Pia	Pius	k1gMnSc2
v	v	k7c6
prvním	první	k4xOgNnSc6
vydání	vydání	k1gNnSc6
z	z	k7c2
roku	rok	k1gInSc2
1978	#num#	k4
zahrnoval	zahrnovat	k5eAaImAgInS
asi	asi	k9
700	#num#	k4
dalších	další	k2eAgInPc2d1
titulů	titul	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Současnost	současnost	k1gFnSc1
</s>
<s>
Práce	práce	k1gFnPc1
Apollinaira	Apollinair	k1gMnSc2
a	a	k8xC
Pia	Pius	k1gMnSc2
významně	významně	k6eAd1
přispěly	přispět	k5eAaPmAgFnP
ke	k	k7c3
všeobecnému	všeobecný	k2eAgNnSc3d1
uznání	uznání	k1gNnSc3
literatury	literatura	k1gFnSc2
v	v	k7c6
oddělení	oddělení	k1gNnSc6
Enfer	Enfra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
vydalo	vydat	k5eAaPmAgNnS
nakladatelství	nakladatelství	k1gNnSc1
Fayard	Fayard	k1gInSc1
sedmisvazkový	sedmisvazkový	k2eAgInSc4d1
výběr	výběr	k1gInSc4
z	z	k7c2
románů	román	k1gInPc2
oddělení	oddělení	k1gNnSc2
Enfer	Enfer	k1gInSc1
se	s	k7c7
soudobými	soudobý	k2eAgFnPc7d1
ilustracemi	ilustrace	k1gFnPc7
a	a	k8xC
vysvětlujícími	vysvětlující	k2eAgInPc7d1
úvody	úvod	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
vědecké	vědecký	k2eAgFnSc2d1
studie	studie	k1gFnSc2
o	o	k7c4
předrevoluční	předrevoluční	k2eAgFnSc4d1
pornografii	pornografie	k1gFnSc4
nalezly	naleznout	k5eAaPmAgFnP,k5eAaBmAgFnP
své	svůj	k3xOyFgNnSc4
místo	místo	k1gNnSc4
v	v	k7c6
kulturní	kulturní	k2eAgFnSc6d1
historii	historie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1
knižního	knižní	k2eAgInSc2d1
trhu	trh	k1gInSc2
<g/>
,	,	kIx,
měnící	měnící	k2eAgFnSc7d1
se	se	k3xPyFc4
morální	morální	k2eAgFnSc2d1
normy	norma	k1gFnSc2
a	a	k8xC
zrušení	zrušení	k1gNnSc4
cenzury	cenzura	k1gFnPc1
změnily	změnit	k5eAaPmAgFnP
charakter	charakter	k1gInSc4
sbírky	sbírka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
masivní	masivní	k2eAgFnSc7d1
dostupností	dostupnost	k1gFnSc7
erotických	erotický	k2eAgNnPc2d1
a	a	k8xC
pornografických	pornografický	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
ztratil	ztratit	k5eAaPmAgMnS
Enfer	Enfer	k1gInSc4
svůj	svůj	k3xOyFgInSc4
původní	původní	k2eAgFnSc1d1
mysl	mysl	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
nadále	nadále	k6eAd1
shromažďuje	shromažďovat	k5eAaImIp3nS
knihy	kniha	k1gFnPc4
s	s	k7c7
erotickým	erotický	k2eAgNnSc7d1
tématem	téma	k1gNnSc7
<g/>
,	,	kIx,
ovšem	ovšem	k9
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
pouze	pouze	k6eAd1
o	o	k7c4
vzácné	vzácný	k2eAgInPc4d1
tisky	tisk	k1gInPc4
nebo	nebo	k8xC
bibliofie	bibliofie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přístup	přístup	k1gInSc1
ke	k	k7c3
knižnímu	knižní	k2eAgInSc3d1
fondu	fond	k1gInSc3
je	být	k5eAaImIp3nS
i	i	k9
nadále	nadále	k6eAd1
omezený	omezený	k2eAgInSc1d1
<g/>
,	,	kIx,
ne	ne	k9
však	však	k9
přísněji	přísně	k6eAd2
než	než	k8xS
u	u	k7c2
jiných	jiný	k2eAgInPc2d1
vzácných	vzácný	k2eAgInPc2d1
fondů	fond	k1gInPc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
v	v	k7c6
současnosti	současnost	k1gFnSc6
z	z	k7c2
opačného	opačný	k2eAgInSc2d1
důvodu	důvod	k1gInSc2
<g/>
,	,	kIx,
než	než	k8xS
dříve	dříve	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
tehdejším	tehdejší	k2eAgInSc7d1
smyslem	smysl	k1gInSc7
bylo	být	k5eAaImAgNnS
ochránit	ochránit	k5eAaPmF
veřejnost	veřejnost	k1gFnSc4
před	před	k7c7
knihami	kniha	k1gFnPc7
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
jsou	být	k5eAaImIp3nP
knihy	kniha	k1gFnPc1
ochraňovány	ochraňován	k2eAgFnPc1d1
před	před	k7c7
veřejností	veřejnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Enfer	Enfra	k1gFnPc2
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Grand	grand	k1gMnSc1
dictionnaire	dictionnair	k1gInSc5
universel	universlo	k1gNnPc2
du	du	k?
XIXe	XIX	k1gMnSc2
siè	siè	k1gMnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
enfer	enfra	k1gFnPc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Historie	historie	k1gFnSc1
sbírky	sbírka	k1gFnSc2
Enfer	Enfra	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Informace	informace	k1gFnPc1
o	o	k7c6
výstavě	výstava	k1gFnSc6
knih	kniha	k1gFnPc2
ze	z	k7c2
sbírky	sbírka	k1gFnSc2
Enfer	Enfra	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Informační	informační	k2eAgFnSc1d1
věda	věda	k1gFnSc1
a	a	k8xC
knihovnictví	knihovnictví	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4749423-2	4749423-2	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
244305887	#num#	k4
</s>
