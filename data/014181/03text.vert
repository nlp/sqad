<s>
Skalka	skalka	k1gFnSc1
(	(	kIx(
<g/>
podcelek	podcelek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Skalkageomorfologický	Skalkageomorfologický	k2eAgInSc4d1
podcelek	podcelek	k1gInSc4
Okolí	okolí	k1gNnSc2
Svätého	Svätý	k2eAgMnSc2d1
Antona	Anton	k1gMnSc2
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
</s>
<s>
882	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
(	(	kIx(
<g/>
Skalka	Skalka	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Stát	stát	k1gInSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
27	#num#	k4
<g/>
′	′	k?
<g/>
36	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
19	#num#	k4
<g/>
°	°	k?
<g/>
0	#num#	k4
<g/>
′	′	k?
<g/>
22	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Skalka	skalka	k1gFnSc1
je	být	k5eAaImIp3nS
geomorfologický	geomorfologický	k2eAgInSc4d1
podcelek	podcelek	k1gInSc4
Štiavnických	Štiavnický	k2eAgInPc2d1
vrchů	vrch	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgInSc1d3
vrch	vrch	k1gInSc1
podcelku	podcelek	k1gInSc2
je	být	k5eAaImIp3nS
882	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
vysoký	vysoký	k2eAgInSc1d1
se	s	k7c7
stejným	stejný	k2eAgInSc7d1
jmémem	jmém	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Vymezení	vymezení	k1gNnSc1
</s>
<s>
Podcelek	Podcelek	k1gInSc1
zabírá	zabírat	k5eAaImIp3nS
východní	východní	k2eAgFnSc4d1
část	část	k1gFnSc4
Štiavnických	Štiavnický	k2eAgInPc2d1
vrchů	vrch	k1gInPc2
a	a	k8xC
v	v	k7c6
rámci	rámec	k1gInSc6
pohoří	pohoří	k1gNnPc2
sousedí	sousedit	k5eAaImIp3nS
na	na	k7c6
západě	západ	k1gInSc6
s	s	k7c7
Hodrušskou	Hodrušský	k2eAgFnSc7d1
hornatinou	hornatina	k1gFnSc7
a	a	k8xC
Sitnianskou	Sitnianský	k2eAgFnSc7d1
vrchovinou	vrchovina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jižním	jižní	k2eAgInSc7d1
směrem	směr	k1gInSc7
leží	ležet	k5eAaImIp3nS
Krupinská	Krupinský	k2eAgFnSc1d1
planina	planina	k1gFnSc1
s	s	k7c7
podcelky	podcelka	k1gMnPc7
Bzovícka	Bzovícka	k1gFnSc1
pahorkatina	pahorkatina	k1gFnSc1
a	a	k8xC
Závozská	Závozský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
východě	východ	k1gInSc6
je	být	k5eAaImIp3nS
Pliešovská	Pliešovský	k2eAgFnSc1d1
kotlina	kotlina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severu	sever	k1gInSc6
údolí	údolí	k1gNnSc2
Hronu	Hron	k1gInSc2
odděluje	oddělovat	k5eAaImIp3nS
Kremnické	kremnický	k2eAgInPc4d1
vrchy	vrch	k1gInPc4
a	a	k8xC
podcelek	podcelek	k1gInSc4
Turovské	Turovský	k2eAgFnSc2d1
predhorie	predhorie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vybrané	vybraný	k2eAgInPc1d1
vrcholy	vrchol	k1gInPc1
</s>
<s>
Skalka	skalka	k1gFnSc1
(	(	kIx(
<g/>
882	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
–	–	k?
nejvyšší	vysoký	k2eAgInSc4d3
vrch	vrch	k1gInSc4
podcelku	podcelek	k1gInSc2
</s>
<s>
Strela	Strela	k1gFnSc1
(	(	kIx(
<g/>
840	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
</s>
<s>
Tri	Tri	k?
kamene	kámen	k1gInSc2
(	(	kIx(
<g/>
834	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Téměř	téměř	k6eAd1
celé	celý	k2eAgNnSc1d1
území	území	k1gNnSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
CHKO	CHKO	kA
Štiavnické	Štiavnický	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
a	a	k8xC
leží	ležet	k5eAaImIp3nP
tu	tu	k6eAd1
maloplošná	maloplošný	k2eAgNnPc1d1
území	území	k1gNnPc1
<g/>
:	:	kIx,
</s>
<s>
Gajdošovo	Gajdošův	k2eAgNnSc1d1
–	–	k?
přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
</s>
<s>
Holý	holý	k2eAgInSc1d1
vrch	vrch	k1gInSc1
–	–	k?
přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
</s>
<s>
Krupinské	Krupinský	k2eAgInPc1d1
bralce	bralec	k1gInPc1
–	–	k?
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
</s>
<s>
Sixova	Sixův	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
–	–	k?
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Doprava	doprava	k1gFnSc1
</s>
<s>
Západním	západní	k2eAgInSc7d1
okrajem	okraj	k1gInSc7
území	území	k1gNnSc2
vede	vést	k5eAaImIp3nS
cesta	cesta	k1gFnSc1
I	I	kA
<g/>
/	/	kIx~
<g/>
51	#num#	k4
i	i	k8xC
železniční	železniční	k2eAgFnSc1d1
trať	trať	k1gFnSc1
Hronská	hronský	k2eAgFnSc1d1
Dúbrava	Dúbrava	k1gFnSc1
–	–	k?
Banská	banský	k2eAgFnSc1d1
Štiavnica	Štiavnica	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severním	severní	k2eAgInSc7d1
okrajem	okraj	k1gInSc7
vede	vést	k5eAaImIp3nS
údolím	údolí	k1gNnSc7
Hronu	Hron	k1gInSc2
rýchlostní	rýchlostní	k2eAgFnSc2d1
silnice	silnice	k1gFnSc2
R1	R1	k1gFnSc2
(	(	kIx(
<g/>
Nitra	Nitra	k1gFnSc1
–	–	k?
Zvolen	Zvolen	k1gInSc1
<g/>
)	)	kIx)
i	i	k8xC
Železniční	železniční	k2eAgFnSc4d1
trať	trať	k1gFnSc4
Nové	Nové	k2eAgInPc1d1
Zámky	zámek	k1gInPc1
–	–	k?
Zvolen	Zvolen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Skalka	skalka	k1gFnSc1
(	(	kIx(
<g/>
podcelok	podcelok	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
KOČICKÝ	KOČICKÝ	kA
<g/>
,	,	kIx,
Dušan	Dušan	k1gMnSc1
<g/>
;	;	kIx,
IVANIČ	IVANIČ	kA
<g/>
,	,	kIx,
Boris	Boris	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geomorfologické	geomorfologický	k2eAgFnPc4d1
členenie	členenie	k1gFnPc4
Slovenska	Slovensko	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
:	:	kIx,
Štátny	Štátna	k1gMnSc2
geologický	geologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
Dionýza	Dionýz	k1gMnSc2
Štúra	Štúr	k1gMnSc2
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
mapový	mapový	k2eAgInSc4d1
portál	portál	k1gInSc4
HIKING	HIKING	kA
<g/>
.	.	kIx.
<g/>
SK	Sk	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
mapy	mapa	k1gFnPc1
<g/>
.	.	kIx.
<g/>
hiking	hiking	k1gInSc1
<g/>
.	.	kIx.
<g/>
sk	sk	k?
<g/>
,	,	kIx,
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
..	..	k?
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Slovensko	Slovensko	k1gNnSc1
</s>
