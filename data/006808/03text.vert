<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
vymítání	vymítání	k1gNnSc1	vymítání
ďábla	ďábel	k1gMnSc2	ďábel
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc4d1	americký
hororový	hororový	k2eAgInSc4d1	hororový
film	film	k1gInSc4	film
režírován	režírován	k2eAgInSc4d1	režírován
a	a	k8xC	a
sestřihán	sestřihán	k2eAgInSc4d1	sestřihán
Danielem	Daniel	k1gMnSc7	Daniel
Stammem	Stamm	k1gMnSc7	Stamm
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
hrají	hrát	k5eAaImIp3nP	hrát
Patrick	Patrick	k1gMnSc1	Patrick
Fabian	Fabian	k1gMnSc1	Fabian
<g/>
,	,	kIx,	,
Ashley	Ashlea	k1gMnSc2	Ashlea
Bellová	Bellová	k1gFnSc1	Bellová
<g/>
,	,	kIx,	,
Iris	iris	k1gFnSc1	iris
Bahrová	Bahrová	k1gFnSc1	Bahrová
a	a	k8xC	a
Louis	Louis	k1gMnSc1	Louis
Herthum	Herthum	k1gInSc1	Herthum
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
evangelického	evangelický	k2eAgMnSc4d1	evangelický
kněze	kněz	k1gMnSc4	kněz
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
léta	léto	k1gNnPc4	léto
z	z	k7c2	z
kapes	kapsa	k1gFnPc2	kapsa
zoufalých	zoufalý	k2eAgMnPc2d1	zoufalý
věřících	věřící	k1gMnPc2	věřící
tahá	tahat	k5eAaImIp3nS	tahat
peníze	peníz	k1gInPc4	peníz
a	a	k8xC	a
trápí	trápit	k5eAaImIp3nP	trápit
ho	on	k3xPp3gInSc4	on
výčitky	výčitek	k1gInPc4	výčitek
svědomí	svědomí	k1gNnSc2	svědomí
<g/>
.	.	kIx.	.
</s>
<s>
Chce	chtít	k5eAaImIp3nS	chtít
tedy	tedy	k9	tedy
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
štábem	štáb	k1gInSc7	štáb
natočit	natočit	k5eAaBmF	natočit
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
poslední	poslední	k2eAgFnSc6d1	poslední
seanci	seance	k1gFnSc6	seance
dokument	dokument	k1gInSc4	dokument
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
zpovědí	zpověď	k1gFnSc7	zpověď
a	a	k8xC	a
doznáním	doznání	k1gNnSc7	doznání
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příjezdu	příjezd	k1gInSc6	příjezd
na	na	k7c4	na
krví	krev	k1gFnSc7	krev
prosáklou	prosáklý	k2eAgFnSc4d1	prosáklá
rodinnou	rodinný	k2eAgFnSc4d1	rodinná
farmu	farma	k1gFnSc4	farma
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
hned	hned	k6eAd1	hned
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
tady	tady	k6eAd1	tady
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
čeká	čekat	k5eAaImIp3nS	čekat
skutečné	skutečný	k2eAgInPc4d1	skutečný
a	a	k8xC	a
ryzí	ryzí	k2eAgNnSc4d1	ryzí
zlo	zlo	k1gNnSc4	zlo
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
nemohl	moct	k5eNaImAgMnS	moct
nijak	nijak	k6eAd1	nijak
připravit	připravit	k5eAaPmF	připravit
<g/>
.	.	kIx.	.
</s>
<s>
Cesty	cesta	k1gFnPc4	cesta
zpět	zpět	k6eAd1	zpět
už	už	k6eAd1	už
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
tady	tady	k6eAd1	tady
reverend	reverend	k1gMnSc1	reverend
zažije	zažít	k5eAaPmIp3nS	zažít
<g/>
,	,	kIx,	,
do	do	k7c2	do
základů	základ	k1gInPc2	základ
otřese	otřást	k5eAaPmIp3nS	otřást
jeho	jeho	k3xOp3gFnSc7	jeho
vírou	víra	k1gFnSc7	víra
<g/>
.	.	kIx.	.
</s>
<s>
Patrick	Patrick	k1gMnSc1	Patrick
Fabian	Fabian	k1gMnSc1	Fabian
jako	jako	k8xC	jako
Cotton	Cotton	k1gInSc1	Cotton
Marcus	Marcus	k1gInSc1	Marcus
Ashley	Ashlea	k1gMnSc2	Ashlea
Bellová	Bellová	k1gFnSc1	Bellová
jako	jako	k8xS	jako
Nell	Nell	k1gInSc1	Nell
Sweetzerová	Sweetzerový	k2eAgFnSc1d1	Sweetzerový
Iris	iris	k1gFnSc1	iris
Bahrová	Bahrová	k1gFnSc1	Bahrová
jako	jako	k8xC	jako
Iris	iris	k1gInSc1	iris
Reisenová	Reisenová	k1gFnSc1	Reisenová
Louis	Louis	k1gMnSc1	Louis
Herthum	Herthum	k1gInSc1	Herthum
jako	jako	k8xC	jako
Louis	Louis	k1gMnSc1	Louis
Sweetzer	Sweetzer	k1gMnSc1	Sweetzer
Caleb	Calba	k1gFnPc2	Calba
Landry	Landr	k1gMnPc4	Landr
Jones	Jonesa	k1gFnPc2	Jonesa
jako	jako	k8xC	jako
Caleb	Calba	k1gFnPc2	Calba
Sweetzer	Sweetzero	k1gNnPc2	Sweetzero
Tony	Tony	k1gMnSc1	Tony
Bentley	Bentlea	k1gFnSc2	Bentlea
jako	jako	k8xS	jako
pastor	pastor	k1gMnSc1	pastor
Marcus	Marcus	k1gMnSc1	Marcus
Shanna	Shanna	k1gFnSc1	Shanna
Forrestallová	Forrestallová	k1gFnSc1	Forrestallová
jako	jako	k8xS	jako
paní	paní	k1gFnSc1	paní
Marcusová	Marcusový	k2eAgFnSc1d1	Marcusová
Becky	Becka	k1gFnSc2	Becka
Flyová	Flyový	k2eAgFnSc1d1	Flyový
jako	jako	k8xC	jako
Becky	Becka	k1gFnSc2	Becka
Denise	Denisa	k1gFnSc3	Denisa
Lee	Lea	k1gFnSc3	Lea
jako	jako	k8xC	jako
sestra	sestra	k1gFnSc1	sestra
Logan	Logana	k1gFnPc2	Logana
Craig	Craig	k1gMnSc1	Craig
Reid	Reid	k1gMnSc1	Reid
jako	jako	k8xS	jako
Logan	Logan	k1gMnSc1	Logan
Adam	Adam	k1gMnSc1	Adam
Grimes	Grimes	k1gMnSc1	Grimes
jako	jako	k8xC	jako
Daniel	Daniel	k1gMnSc1	Daniel
Moskowitz	Moskowitz	k1gMnSc1	Moskowitz
Jamie	Jamie	k1gFnSc2	Jamie
Alyson	Alyson	k1gMnSc1	Alyson
Caudleová	Caudleová	k1gFnSc1	Caudleová
jako	jako	k8xS	jako
ctitelka	ctitelka	k1gFnSc1	ctitelka
Satana	Satan	k1gMnSc2	Satan
(	(	kIx(	(
<g/>
neuvedena	uveden	k2eNgFnSc1d1	neuvedena
<g/>
)	)	kIx)	)
Allen	allen	k1gInSc1	allen
Boudreaux	Boudreaux	k1gInSc1	Boudreaux
jako	jako	k8xS	jako
ctitel	ctitel	k1gMnSc1	ctitel
Satana	Satan	k1gMnSc2	Satan
(	(	kIx(	(
<g/>
neuveden	uveden	k2eNgInSc1d1	neuveden
<g/>
)	)	kIx)	)
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
promítán	promítat	k5eAaImNgInS	promítat
na	na	k7c6	na
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
South	South	k1gInSc4	South
by	by	kYmCp3nS	by
Southwest	Southwest	k1gMnSc1	Southwest
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2010	[number]	k4	2010
Lionsgate	Lionsgat	k1gInSc5	Lionsgat
odkoupili	odkoupit	k5eAaPmAgMnP	odkoupit
práva	právo	k1gNnPc4	právo
od	od	k7c2	od
US	US	kA	US
Distribution	Distribution	k1gInSc1	Distribution
a	a	k8xC	a
nastavili	nastavit	k5eAaPmAgMnP	nastavit
datum	datum	k1gNnSc4	datum
vydání	vydání	k1gNnSc2	vydání
filmu	film	k1gInSc2	film
na	na	k7c4	na
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
premiéra	premiéra	k1gFnSc1	premiéra
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
na	na	k7c6	na
LA	la	k1gNnSc6	la
Film	film	k1gInSc1	film
Festival	festival	k1gInSc1	festival
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
zde	zde	k6eAd1	zde
uveden	uvést	k5eAaPmNgInS	uvést
Eli	Eli	k1gFnSc3	Eli
Rothovou	Rothová	k1gFnSc7	Rothová
a	a	k8xC	a
Danielem	Daniel	k1gMnSc7	Daniel
Stammem	Stamm	k1gMnSc7	Stamm
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
DVD	DVD	kA	DVD
a	a	k8xC	a
Blu-ray	Bluaa	k1gFnSc2	Blu-raa
byl	být	k5eAaImAgInS	být
film	film	k1gInSc1	film
vydán	vydán	k2eAgInSc1d1	vydán
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
webová	webový	k2eAgFnSc1d1	webová
stránka	stránka	k1gFnSc1	stránka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Poslední	poslední	k2eAgNnSc1d1	poslední
vymítání	vymítání	k1gNnSc1	vymítání
ďábla	ďábel	k1gMnSc2	ďábel
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc6	Databasa
Poslední	poslední	k2eAgNnSc4d1	poslední
vymítání	vymítání	k1gNnSc4	vymítání
ďábla	ďábel	k1gMnSc2	ďábel
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
