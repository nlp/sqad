<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Ománu	Omán	k1gInSc2	Omán
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgInPc4	tři
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pruhy	pruh	k1gInPc4	pruh
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
červený	červený	k2eAgInSc1d1	červený
a	a	k8xC	a
zelený	zelený	k2eAgMnSc1d1	zelený
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
při	při	k7c6	při
žerdi	žerď	k1gFnSc6	žerď
červený	červený	k2eAgInSc1d1	červený
svislý	svislý	k2eAgInSc1d1	svislý
pruh	pruh	k1gInSc1	pruh
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
šířka	šířka	k1gFnSc1	šířka
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
polovině	polovina	k1gFnSc3	polovina
šířky	šířka	k1gFnSc2	šířka
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
rohu	roh	k1gInSc6	roh
je	být	k5eAaImIp3nS	být
umístěný	umístěný	k2eAgInSc1d1	umístěný
ománský	ománský	k2eAgInSc1d1	ománský
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgInSc1d1	pocházející
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
dvě	dva	k4xCgFnPc4	dva
zkřížené	zkřížený	k2eAgFnPc4d1	zkřížená
zahnuté	zahnutý	k2eAgFnPc4d1	zahnutá
šavle	šavle	k1gFnPc4	šavle
v	v	k7c6	v
bohatě	bohatě	k6eAd1	bohatě
zdobených	zdobený	k2eAgFnPc6d1	zdobená
pochvách	pochva	k1gFnPc6	pochva
<g/>
,	,	kIx,	,
dýka	dýka	k1gFnSc1	dýka
(	(	kIx(	(
<g/>
chandžar	chandžar	k1gInSc1	chandžar
<g/>
)	)	kIx)	)
a	a	k8xC	a
závěsník	závěsník	k1gInSc1	závěsník
<g/>
.	.	kIx.	.
<g/>
Převládající	převládající	k2eAgFnSc1d1	převládající
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
připomíná	připomínat	k5eAaImIp3nS	připomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
státu	stát	k1gInSc2	stát
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
islámským	islámský	k2eAgMnPc3d1	islámský
Ibádíjovcům	Ibádíjovec	k1gMnPc3	Ibádíjovec
(	(	kIx(	(
<g/>
starší	starý	k2eAgFnSc1d2	starší
vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
celá	celý	k2eAgFnSc1d1	celá
červená	červená	k1gFnSc1	červená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
také	také	k9	také
barvou	barva	k1gFnSc7	barva
panovníka	panovník	k1gMnSc2	panovník
a	a	k8xC	a
Maskatu	Maskat	k1gInSc2	Maskat
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
je	být	k5eAaImIp3nS	být
barvou	barva	k1gFnSc7	barva
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
Ománu	Omán	k1gInSc2	Omán
a	a	k8xC	a
imáma	imám	k1gMnSc2	imám
(	(	kIx(	(
<g/>
náboženské	náboženský	k2eAgFnSc2d1	náboženská
autority	autorita	k1gFnSc2	autorita
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zelená	zelená	k1gFnSc1	zelená
připomíná	připomínat	k5eAaImIp3nS	připomínat
pohoří	pohoří	k1gNnSc4	pohoří
Džebel	Džebel	k1gMnSc1	Džebel
Achdar	Achdar	k1gMnSc1	Achdar
(	(	kIx(	(
<g/>
Zelené	Zelené	k2eAgFnSc2d1	Zelené
hory	hora	k1gFnSc2	hora
<g/>
)	)	kIx)	)
a	a	k8xC	a
islám	islám	k1gInSc1	islám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
zavedena	zaveden	k2eAgFnSc1d1	zavedena
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
upravená	upravený	k2eAgNnPc1d1	upravené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Vlajka	vlajka	k1gFnSc1	vlajka
Ománu	Omán	k1gInSc2	Omán
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Ománu	Omán	k1gInSc2	Omán
</s>
</p>
<p>
<s>
Ománská	ománský	k2eAgFnSc1d1	ománská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ománská	ománský	k2eAgFnSc1d1	ománská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
