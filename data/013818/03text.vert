<s>
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
Portrét	portrét	k1gInSc4
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1820	#num#	k4
od	od	k7c2
Františka	František	k1gMnSc2
TkadlíkaNarození	TkadlíkaNarození	k1gNnSc2
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1753	#num#	k4
Gyarmat	Gyarma	k1gNnPc2
<g/>
,	,	kIx,
Habsburská	habsburský	k2eAgFnSc1d1
monarchie	monarchie	k1gFnSc2
Habsburská	habsburský	k2eAgFnSc1d1
monarchie	monarchie	k1gFnSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1829	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
75	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Staré	Staré	k2eAgNnSc1d1
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Pobyt	pobyt	k1gInSc1
</s>
<s>
německé	německý	k2eAgFnPc1d1
země	zem	k1gFnPc1
<g/>
,	,	kIx,
Švédsko	Švédsko	k1gNnSc1
<g/>
,	,	kIx,
Dánsko	Dánsko	k1gNnSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Karlo-Ferdinandova	Karlo-Ferdinandův	k2eAgFnSc1d1
Obory	obora	k1gFnSc2
</s>
<s>
filologie	filologie	k1gFnSc1
<g/>
,	,	kIx,
lingvistika	lingvistika	k1gFnSc1
<g/>
,	,	kIx,
slavistika	slavistika	k1gFnSc1
<g/>
,	,	kIx,
bohemistika	bohemistika	k1gFnSc1
<g/>
,	,	kIx,
biblistika	biblistika	k1gFnSc1
<g/>
,	,	kIx,
historiografie	historiografie	k1gFnSc1
Známý	známý	k1gMnSc1
díky	díky	k7c3
</s>
<s>
systematické	systematický	k2eAgNnSc1d1
založení	založení	k1gNnSc1
bohemistiky	bohemistika	k1gFnSc2
a	a	k8xC
slavistiky	slavistika	k1gFnSc2
Významná	významný	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
</s>
<s>
Geschichte	Geschicht	k1gMnSc5
der	drát	k5eAaImRp2nS
böhmischen	böhmischen	k2eAgMnSc1d1
Sprache	Sprache	k1gFnSc7
und	und	k?
Literatur	literatura	k1gFnPc2
<g/>
,	,	kIx,
Ausführliches	Ausführlichesa	k1gFnPc2
Lehrgebäude	Lehrgebäud	k1gInSc5
der	drát	k5eAaImRp2nS
böhmischen	böhmischen	k1gInSc4
Sprache	Sprach	k1gMnSc2
<g/>
,	,	kIx,
Deutsch-böhmisches	Deutsch-böhmisches	k1gInSc1
Wörtebuch	Wörtebuch	k1gInSc1
<g/>
,	,	kIx,
Institutiones	Institutiones	k1gInSc1
linguae	linguae	k6eAd1
slavicae	slavicaat	k5eAaPmIp3nS
dialecti	dialect	k5eAaPmF,k5eAaBmF,k5eAaImF
veteris	veteris	k1gFnSc1
<g/>
,	,	kIx,
Glagolitica	Glagolitica	k1gFnSc1
Podpis	podpis	k1gInSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
(	(	kIx(
<g/>
17	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1753	#num#	k4
Gyarmat	Gyarmat	k1gInSc1
–	–	k?
6	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1829	#num#	k4
Staré	Stará	k1gFnSc6
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
<g/>
,	,	kIx,
filolog	filolog	k1gMnSc1
<g/>
,	,	kIx,
historik	historik	k1gMnSc1
a	a	k8xC
zakladatel	zakladatel	k1gMnSc1
vědecké	vědecký	k2eAgFnSc2d1
bohemistiky	bohemistika	k1gFnSc2
a	a	k8xC
slavistiky	slavistika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svým	svůj	k3xOyFgInSc7
mnohostranným	mnohostranný	k2eAgInSc7d1
dílem	díl	k1gInSc7
předznamenal	předznamenat	k5eAaPmAgInS
i	i	k8xC
podnítil	podnítit	k5eAaPmAgInS
české	český	k2eAgNnSc4d1
národní	národní	k2eAgNnSc4d1
obrození	obrození	k1gNnSc4
<g/>
,	,	kIx,
třebaže	třebaže	k8xS
sám	sám	k3xTgInSc1
k	k	k7c3
romantickým	romantický	k2eAgMnPc3d1
obrozencům	obrozenec	k1gMnPc3
nejen	nejen	k6eAd1
chronologicky	chronologicky	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
ani	ani	k8xC
svým	svůj	k3xOyFgNnSc7
založením	založení	k1gNnSc7
nepatřil	patřit	k5eNaImAgInS
<g/>
.	.	kIx.
</s>
<s>
Byl	být	k5eAaImAgMnS
nejvýznamnějším	významný	k2eAgMnSc7d3
představitelem	představitel	k1gMnSc7
racionalistického	racionalistický	k2eAgNnSc2d1
osvícenství	osvícenství	k1gNnSc2
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
a	a	k8xC
k	k	k7c3
jeho	jeho	k3xOp3gFnPc3
největším	veliký	k2eAgFnPc3d3
zásluhám	zásluha	k1gFnPc3
patří	patřit	k5eAaImIp3nS
zavedení	zavedení	k1gNnSc1
a	a	k8xC
soustavné	soustavný	k2eAgNnSc1d1
uplatňování	uplatňování	k1gNnSc1
kritické	kritický	k2eAgFnSc2d1
metody	metoda	k1gFnSc2
ve	v	k7c6
vědeckém	vědecký	k2eAgNnSc6d1
bádání	bádání	k1gNnSc6
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
dopad	dopad	k1gInSc4
jeho	jeho	k3xOp3gInPc2
výsledků	výsledek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobrovský	Dobrovský	k1gMnSc1
byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
ze	z	k7c2
zakládajících	zakládající	k2eAgMnPc2d1
členů	člen	k1gMnPc2
Královské	královský	k2eAgFnSc2d1
české	český	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
nauk	nauka	k1gFnPc2
a	a	k8xC
společně	společně	k6eAd1
s	s	k7c7
hrabětem	hrabě	k1gMnSc7
Kašparem	Kašpar	k1gMnSc7
Šternberkem	Šternberk	k1gInSc7
rovněž	rovněž	k9
spoluzaložil	spoluzaložit	k5eAaPmAgInS
Vlastenecké	vlastenecký	k2eAgNnSc4d1
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Národní	národní	k2eAgNnSc1d1
<g/>
)	)	kIx)
muzeum	muzeum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
klasických	klasický	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
ovládal	ovládat	k5eAaImAgInS
i	i	k9
hebrejštinu	hebrejština	k1gFnSc4
a	a	k8xC
některé	některý	k3yIgInPc1
další	další	k2eAgInPc1d1
orientální	orientální	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
<g/>
,	,	kIx,
kterým	který	k3yQgMnSc7,k3yRgMnSc7,k3yIgMnSc7
se	se	k3xPyFc4
chtěl	chtít	k5eAaImAgMnS
původně	původně	k6eAd1
odborně	odborně	k6eAd1
věnovat	věnovat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
později	pozdě	k6eAd2
postupně	postupně	k6eAd1
ovládl	ovládnout	k5eAaPmAgInS
i	i	k9
všechny	všechen	k3xTgInPc4
tehdy	tehdy	k6eAd1
známé	známý	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
slovanské	slovanský	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
Husovi	Hus	k1gMnSc6
a	a	k8xC
Komenském	Komenský	k1gMnSc6
je	být	k5eAaImIp3nS
právem	právem	k6eAd1
pokládán	pokládat	k5eAaImNgMnS
za	za	k7c4
třetího	třetí	k4xOgMnSc4
Čecha	Čech	k1gMnSc4
světového	světový	k2eAgInSc2d1
významu	význam	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
T.	T.	kA
G.	G.	kA
Masaryk	Masaryk	k1gMnSc1
jej	on	k3xPp3gMnSc4
označil	označit	k5eAaPmAgMnS
za	za	k7c4
„	„	k?
<g/>
prvního	první	k4xOgMnSc2
světového	světový	k2eAgMnSc2d1
Čecha	Čech	k1gMnSc2
nové	nový	k2eAgFnSc2d1
doby	doba	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dobrovského	Dobrovského	k2eAgMnSc1d1
latinsky	latinsky	k6eAd1
a	a	k8xC
německy	německy	k6eAd1
psané	psaný	k2eAgNnSc1d1
dílo	dílo	k1gNnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
363	#num#	k4
již	již	k9
za	za	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
života	život	k1gInSc2
publikovaných	publikovaný	k2eAgFnPc2d1
studií	studie	k1gFnPc2
<g/>
,	,	kIx,
monografií	monografie	k1gFnPc2
a	a	k8xC
kratších	krátký	k2eAgFnPc2d2
statí	stať	k1gFnPc2
nebo	nebo	k8xC
recenzí	recenze	k1gFnPc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
37	#num#	k4
publikací	publikace	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
kterých	který	k3yRgInPc6,k3yIgInPc6,k3yQgInPc6
se	se	k3xPyFc4
podílel	podílet	k5eAaImAgMnS
jako	jako	k9
redaktor	redaktor	k1gMnSc1
či	či	k8xC
editor	editor	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gFnSc4
rozsáhlou	rozsáhlý	k2eAgFnSc4d1
vědeckou	vědecký	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
lze	lze	k6eAd1
rozdělit	rozdělit	k5eAaPmF
do	do	k7c2
několika	několik	k4yIc2
základních	základní	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
,	,	kIx,
jimiž	jenž	k3xRgFnPc7
jsou	být	k5eAaImIp3nP
historie	historie	k1gFnSc1
<g/>
,	,	kIx,
historiografie	historiografie	k1gFnSc1
<g/>
,	,	kIx,
biblistika	biblistika	k1gFnSc1
<g/>
,	,	kIx,
paleoslavistika	paleoslavistika	k1gFnSc1
<g/>
,	,	kIx,
slovanská	slovanský	k2eAgFnSc1d1
filologie	filologie	k1gFnSc1
a	a	k8xC
bohemistika	bohemistika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
českém	český	k2eAgInSc6d1
kontextu	kontext	k1gInSc6
jsou	být	k5eAaImIp3nP
z	z	k7c2
Dobrovského	Dobrovského	k2eAgInPc2d1
spisů	spis	k1gInPc2
nejzásadnější	zásadní	k2eAgFnPc1d3
<g/>
:	:	kIx,
Geschichte	Geschicht	k1gInSc5
der	drát	k5eAaImRp2nS
böhmischen	böhmischen	k2eAgMnSc1d1
Sprache	Sprache	k1gFnSc7
und	und	k?
Literatur	literatura	k1gFnPc2
(	(	kIx(
<g/>
Dějiny	dějiny	k1gFnPc4
českého	český	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
a	a	k8xC
literatury	literatura	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
především	především	k9
jeho	jeho	k3xOp3gFnSc1
česká	český	k2eAgFnSc1d1
gramatika	gramatika	k1gFnSc1
Ausführliches	Ausführlichesa	k1gFnPc2
Lehrgebäude	Lehrgebäud	k1gInSc5
der	drát	k5eAaImRp2nS
böhmischen	böhmischen	k1gInSc4
Sprache	Sprach	k1gMnSc2
(	(	kIx(
<g/>
Zevrubná	zevrubný	k2eAgFnSc1d1
mluvnice	mluvnice	k1gFnSc1
jazyka	jazyk	k1gInSc2
českého	český	k2eAgInSc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobrovský	Dobrovský	k1gMnSc1
se	se	k3xPyFc4
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
době	doba	k1gFnSc6
pokusil	pokusit	k5eAaPmAgMnS
rekonstruovat	rekonstruovat	k5eAaBmF
tzv.	tzv.	kA
bratrský	bratrský	k2eAgInSc4d1
pravopis	pravopis	k1gInSc4
čili	čili	k8xC
podobu	podoba	k1gFnSc4
češtiny	čeština	k1gFnSc2
v	v	k7c4
období	období	k1gNnSc4
jejího	její	k3xOp3gInSc2
největšího	veliký	k2eAgInSc2d3
rozkvětu	rozkvět	k1gInSc2
v	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
za	za	k7c4
nejklasičtější	klasický	k2eAgInSc4d3
český	český	k2eAgInSc4d1
text	text	k1gInSc4
považoval	považovat	k5eAaImAgInS
kralický	kralický	k2eAgInSc1d1
překlad	překlad	k1gInSc1
Bible	bible	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jazyk	jazyk	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
popsal	popsat	k5eAaPmAgMnS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
gramatice	gramatika	k1gFnSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
základem	základ	k1gInSc7
spisovné	spisovný	k2eAgFnSc2d1
češtiny	čeština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Spisy	spis	k1gInPc1
<g/>
,	,	kIx,
kterými	který	k3yQgInPc7,k3yIgInPc7,k3yRgInPc7
Dobrovský	Dobrovský	k1gMnSc1
vytvořil	vytvořit	k5eAaPmAgMnS
systematické	systematický	k2eAgInPc4d1
základy	základ	k1gInPc4
slavistiky	slavistika	k1gFnSc2
či	či	k8xC
slovanských	slovanský	k2eAgFnPc2d1
studií	studie	k1gFnPc2
se	s	k7c7
všemi	všecek	k3xTgFnPc7
historickými	historický	k2eAgFnPc7d1
<g/>
,	,	kIx,
náboženskými	náboženský	k2eAgInPc7d1
a	a	k8xC
kulturními	kulturní	k2eAgInPc7d1
přesahy	přesah	k1gInPc7
<g/>
,	,	kIx,
měly	mít	k5eAaImAgFnP
zvláště	zvláště	k6eAd1
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
značný	značný	k2eAgInSc4d1
vliv	vliv	k1gInSc4
ve	v	k7c6
všech	všecek	k3xTgFnPc6
slovanských	slovanský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
nejvýznamnějším	významný	k2eAgInSc7d3
dílem	díl	k1gInSc7
v	v	k7c6
tomto	tento	k3xDgInSc6
mezinárodním	mezinárodní	k2eAgInSc6d1
kontextu	kontext	k1gInSc6
je	být	k5eAaImIp3nS
gramatický	gramatický	k2eAgInSc1d1
spis	spis	k1gInSc1
o	o	k7c6
staroslověnštině	staroslověnština	k1gFnSc6
Institutiones	Institutionesa	k1gFnPc2
linguae	linguaat	k5eAaPmIp3nS
slavicae	slavicae	k6eAd1
dialecti	dialect	k5eAaPmF,k5eAaImF,k5eAaBmF
veteris	veteris	k1gFnSc4
(	(	kIx(
<g/>
Základy	základ	k1gInPc1
jazyka	jazyk	k1gInSc2
staroslověnského	staroslověnský	k2eAgInSc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ruském	ruský	k2eAgInSc6d1
prostředí	prostředí	k1gNnSc2
byla	být	k5eAaImAgNnP
Dobrovského	Dobrovského	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
po	po	k7c4
dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
součástí	součást	k1gFnPc2
osnov	osnova	k1gFnPc2
na	na	k7c6
katedrách	katedra	k1gFnPc6
slavistiky	slavistika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Bělorusku	Bělorusko	k1gNnSc6
byla	být	k5eAaImAgFnS
zase	zase	k9
impulzem	impulz	k1gInSc7
pro	pro	k7c4
studium	studium	k1gNnSc4
zdejšího	zdejší	k2eAgInSc2d1
folklóru	folklór	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Inspiraci	inspirace	k1gFnSc6
v	v	k7c6
nich	on	k3xPp3gFnPc6
dále	daleko	k6eAd2
hledalo	hledat	k5eAaImAgNnS
také	také	k9
ukrajinské	ukrajinský	k2eAgNnSc1d1
národní	národní	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
bylo	být	k5eAaImAgNnS
Dobrovského	Dobrovského	k2eAgNnSc1d1
myšlenkami	myšlenka	k1gFnPc7
ovlivněno	ovlivnit	k5eAaPmNgNnS
při	při	k7c6
formování	formování	k1gNnSc6
spisovné	spisovný	k2eAgFnSc2d1
ukrajinštiny	ukrajinština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgFnPc7d1
oblastmi	oblast	k1gFnPc7
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgNnPc6
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
plodné	plodný	k2eAgFnSc3d1
recepci	recepce	k1gFnSc3
jeho	jeho	k3xOp3gInPc2
jazykovědných	jazykovědný	k2eAgInPc2d1
spisů	spis	k1gInPc2
<g/>
,	,	kIx,
potom	potom	k6eAd1
byly	být	k5eAaImAgFnP
např.	např.	kA
Slovinsko	Slovinsko	k1gNnSc4
a	a	k8xC
Lužice	Lužice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Dětství	dětství	k1gNnSc4
a	a	k8xC
mládí	mládí	k1gNnSc4
(	(	kIx(
<g/>
1753	#num#	k4
<g/>
–	–	k?
<g/>
1768	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Horšovský	horšovský	k2eAgInSc1d1
Týn	Týn	k1gInSc1
(	(	kIx(
<g/>
Bischofteinitz	Bischofteinitz	k1gInSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
pohled	pohled	k1gInSc1
na	na	k7c4
jihozápadní	jihozápadní	k2eAgFnSc4d1
část	část	k1gFnSc4
náměstí	náměstí	k1gNnSc2
</s>
<s>
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
17	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1753	#num#	k4
v	v	k7c6
uherském	uherský	k2eAgInSc6d1
Gyarmatu	Gyarmat	k1gInSc6
v	v	k7c6
Novohradské	novohradský	k2eAgFnSc6d1
župě	župa	k1gFnSc6
(	(	kIx(
<g/>
dnešním	dnešní	k2eAgNnSc6d1
Balassagyarmatu	Balassagyarma	k1gNnSc6
<g/>
)	)	kIx)
v	v	k7c6
rodině	rodina	k1gFnSc6
dragounského	dragounský	k2eAgMnSc2d1
desátníka	desátník	k1gMnSc2
Jakuba	Jakub	k1gMnSc2
Doubravského	Doubravský	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jméno	jméno	k1gNnSc1
Dobrovský	Dobrovský	k1gMnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
chybou	chyba	k1gFnSc7
při	při	k7c6
zápisu	zápis	k1gInSc6
do	do	k7c2
církevní	církevní	k2eAgFnSc2d1
matriky	matrika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobrovského	Dobrovský	k1gMnSc2
otec	otec	k1gMnSc1
byl	být	k5eAaImAgMnS
drobný	drobný	k2eAgMnSc1d1
rolník	rolník	k1gMnSc1
z	z	k7c2
jazykově	jazykově	k6eAd1
české	český	k2eAgFnSc2d1
obce	obec	k1gFnSc2
Solnice	solnice	k1gFnSc2
v	v	k7c6
Orlickém	orlický	k2eAgNnSc6d1
podhůří	podhůří	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
unikl	uniknout	k5eAaPmAgMnS
z	z	k7c2
bídy	bída	k1gFnSc2
<g/>
,	,	kIx,
nechal	nechat	k5eAaPmAgMnS
se	se	k3xPyFc4
naverbovat	naverbovat	k5eAaPmF
do	do	k7c2
vojska	vojsko	k1gNnSc2
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
bránilo	bránit	k5eAaImAgNnS
Uhry	Uhry	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
mu	on	k3xPp3gMnSc3
již	již	k9
padesát	padesát	k4xCc4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Dobrovského	Dobrovského	k2eAgFnSc7d1
matkou	matka	k1gFnSc7
<g/>
,	,	kIx,
o	o	k7c4
mnoho	mnoho	k4c4
let	léto	k1gNnPc2
mladší	mladý	k2eAgFnSc2d2
<g/>
,	,	kIx,
negramotnou	gramotný	k2eNgFnSc7d1
Magdalenou	Magdalena	k1gFnSc7
<g/>
,	,	kIx,
rozenou	rozený	k2eAgFnSc7d1
Wannerovou	Wannerová	k1gFnSc7
z	z	k7c2
Čáslavska	Čáslavsko	k1gNnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
byla	být	k5eAaImAgFnS
dcerou	dcera	k1gFnSc7
české	český	k2eAgFnSc2d1
matky	matka	k1gFnSc2
a	a	k8xC
alsaského	alsaský	k2eAgMnSc2d1
otce	otec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doma	doma	k6eAd1
se	se	k3xPyFc4
mluvilo	mluvit	k5eAaImAgNnS
německy	německy	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nedlouho	dlouho	k6eNd1
po	po	k7c6
Josefově	Josefův	k2eAgNnSc6d1
narození	narození	k1gNnSc6
se	se	k3xPyFc4
otec	otec	k1gMnSc1
jako	jako	k8xS,k8xC
vysloužilý	vysloužilý	k2eAgMnSc1d1
voják	voják	k1gMnSc1
usadil	usadit	k5eAaPmAgMnS
s	s	k7c7
rodinou	rodina	k1gFnSc7
v	v	k7c6
Horšovském	horšovský	k2eAgInSc6d1
Týně	Týn	k1gInSc6
(	(	kIx(
<g/>
Bischofteinitz	Bischofteinitz	k1gInSc1
<g/>
)	)	kIx)
s	s	k7c7
převážně	převážně	k6eAd1
německým	německý	k2eAgNnSc7d1
obyvatelstvem	obyvatelstvo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katecheta	katecheta	k1gMnSc1
místní	místní	k2eAgFnSc2d1
školy	škola	k1gFnSc2
doporučil	doporučit	k5eAaPmAgInS
nadaného	nadaný	k2eAgMnSc4d1
hocha	hoch	k1gMnSc4
k	k	k7c3
dalšímu	další	k2eAgNnSc3d1
studiu	studio	k1gNnSc3
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1762	#num#	k4
devítiletý	devítiletý	k2eAgMnSc1d1
Josef	Josef	k1gMnSc1
dostal	dostat	k5eAaPmAgMnS
na	na	k7c4
čtyřleté	čtyřletý	k2eAgNnSc4d1
nižší	nízký	k2eAgNnSc4d2
gymnázium	gymnázium	k1gNnSc4
bosých	bosý	k2eAgMnPc2d1
augustiniánů	augustinián	k1gMnPc2
v	v	k7c6
tehdejším	tehdejší	k2eAgInSc6d1
Německém	německý	k2eAgInSc6d1
Brodě	Brod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
přišel	přijít	k5eAaPmAgInS
poprvé	poprvé	k6eAd1
do	do	k7c2
kontaktu	kontakt	k1gInSc2
s	s	k7c7
českými	český	k2eAgFnPc7d1
dětmi	dítě	k1gFnPc7
a	a	k8xC
naučil	naučit	k5eAaPmAgMnS
se	se	k3xPyFc4
česky	česky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
svých	svůj	k3xOyFgMnPc2
spolužáků	spolužák	k1gMnPc2
však	však	k9
zpočátku	zpočátku	k6eAd1
ledacos	ledacos	k3yInSc4
vytrpěl	vytrpět	k5eAaPmAgInS
<g/>
:	:	kIx,
v	v	k7c6
listu	list	k1gInSc6
Hankovi	Hanek	k1gMnSc3
z	z	k7c2
roku	rok	k1gInSc2
1828	#num#	k4
vzpomínal	vzpomínat	k5eAaImAgInS
na	na	k7c4
posměšnou	posměšný	k2eAgFnSc4d1
říkanku	říkanka	k1gFnSc4
„	„	k?
<g/>
Němec	Němec	k1gMnSc1
brouk	brouk	k1gMnSc1
<g/>
,	,	kIx,
hrnce	hrnec	k1gInSc2
tlouk	tlouk	k1gInSc1
<g/>
,	,	kIx,
pod	pod	k7c4
lavici	lavice	k1gFnSc4
je	být	k5eAaImIp3nS
házel	házet	k5eAaImAgMnS
<g/>
“	“	k?
a	a	k8xC
rovněž	rovněž	k9
na	na	k7c4
krvavé	krvavý	k2eAgFnPc4d1
rány	rána	k1gFnPc4
na	na	k7c6
hlavě	hlava	k1gFnSc6
<g/>
,	,	kIx,
když	když	k8xS
po	po	k7c6
něm	on	k3xPp3gNnSc6
házeli	házet	k5eAaImAgMnP
kameny	kámen	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
té	ten	k3xDgFnSc6
<g/>
,	,	kIx,
co	co	k9
Dobrovského	Dobrovského	k2eAgMnSc1d1
otec	otec	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1764	#num#	k4
zemřel	zemřít	k5eAaPmAgInS
<g/>
,	,	kIx,
provdala	provdat	k5eAaPmAgFnS
se	se	k3xPyFc4
jeho	jeho	k3xOp3gFnSc1
matka	matka	k1gFnSc1
v	v	k7c6
Horšovském	horšovský	k2eAgInSc6d1
Týně	Týn	k1gInSc6
za	za	k7c2
německého	německý	k2eAgMnSc2d1
perníkáře	perníkář	k1gMnSc2
Valentina	Valentin	k1gMnSc2
Steinbacha	Steinbach	k1gMnSc2
<g/>
,	,	kIx,
s	s	k7c7
nímž	jenž	k3xRgNnSc7
pak	pak	k6eAd1
měla	mít	k5eAaImAgFnS
tři	tři	k4xCgFnPc4
dcery	dcera	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
nich	on	k3xPp3gFnPc2
měl	mít	k5eAaImAgMnS
Josef	Josef	k1gMnSc1
ještě	ještě	k9
dva	dva	k4xCgInPc4
vlastní	vlastní	k2eAgInPc4d1
bratry	bratr	k1gMnPc4
(	(	kIx(
<g/>
Matěje	Matěj	k1gMnSc4
a	a	k8xC
Františka	František	k1gMnSc4
<g/>
)	)	kIx)
z	z	k7c2
prvního	první	k4xOgNnSc2
matčina	matčin	k2eAgNnSc2d1
manželství	manželství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
absolvování	absolvování	k1gNnSc6
gramatikálních	gramatikální	k2eAgFnPc2d1
a	a	k8xC
humanitních	humanitní	k2eAgFnPc2d1
tříd	třída	k1gFnPc2
pokračoval	pokračovat	k5eAaImAgInS
od	od	k7c2
roku	rok	k1gInSc2
1766	#num#	k4
na	na	k7c6
jezuitském	jezuitský	k2eAgNnSc6d1
gymnáziu	gymnázium	k1gNnSc6
v	v	k7c6
Klatovech	Klatovy	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
po	po	k7c6
dvou	dva	k4xCgNnPc6
letech	léto	k1gNnPc6
se	s	k7c7
zdarem	zdar	k1gInSc7
ukončil	ukončit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
školním	školní	k2eAgInSc6d1
roce	rok	k1gInSc6
1767	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
byl	být	k5eAaImAgMnS
patnáctiletý	patnáctiletý	k2eAgMnSc1d1
Dobrovský	Dobrovský	k1gMnSc1
přijat	přijmout	k5eAaPmNgMnS
na	na	k7c4
filosofickou	filosofický	k2eAgFnSc4d1
fakultu	fakulta	k1gFnSc4
Univerzity	univerzita	k1gFnSc2
Karlo-Ferdinandovy	Karlo-Ferdinandův	k2eAgFnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pomník	pomník	k1gInSc1
Josepha	Joseph	k1gMnSc2
Steplinga	Stepling	k1gMnSc2
od	od	k7c2
sochaře	sochař	k1gMnSc2
Ignáce	Ignác	k1gMnSc2
Platzera	Platzer	k1gMnSc2
v	v	k7c6
areálu	areál	k1gInSc6
Klementina	Klementinum	k1gNnSc2
z	z	k7c2
roku	rok	k1gInSc2
1780	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Studium	studium	k1gNnSc1
(	(	kIx(
<g/>
1768	#num#	k4
<g/>
–	–	k?
<g/>
1776	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Filosofie	filosofie	k1gFnSc1
na	na	k7c6
Univerzitě	univerzita	k1gFnSc6
Karlo-Ferdinandově	Karlo-Ferdinandův	k2eAgFnSc6d1
(	(	kIx(
<g/>
1768	#num#	k4
<g/>
–	–	k?
<g/>
1771	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
Dobrovského	Dobrovského	k2eAgNnSc2d1
studia	studio	k1gNnSc2
byla	být	k5eAaImAgFnS
filosofická	filosofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Univerzity	univerzita	k1gFnSc2
Karlo-Ferdinandovy	Karlo-Ferdinandův	k2eAgFnPc1d1
ještě	ještě	k6eAd1
do	do	k7c2
značné	značný	k2eAgFnSc2d1
míry	míra	k1gFnSc2
provázána	provázán	k2eAgFnSc1d1
s	s	k7c7
církevními	církevní	k2eAgFnPc7d1
strukturami	struktura	k1gFnPc7
<g/>
:	:	kIx,
většinu	většina	k1gFnSc4
pedagogů	pedagog	k1gMnPc2
tvořili	tvořit	k5eAaImAgMnP
jezuité	jezuita	k1gMnPc1
a	a	k8xC
např.	např.	kA
všechny	všechen	k3xTgFnPc1
čtyři	čtyři	k4xCgFnPc1
fakulty	fakulta	k1gFnPc1
se	se	k3xPyFc4
každoročně	každoročně	k6eAd1
účastnily	účastnit	k5eAaImAgFnP
přísahy	přísaha	k1gFnPc1
<g/>
,	,	kIx,
že	že	k8xS
budou	být	k5eAaImBp3nP
hájit	hájit	k5eAaImF
neposkvrněné	poskvrněný	k2eNgNnSc4d1
početí	početí	k1gNnSc4
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
již	již	k6eAd1
také	také	k9
sem	sem	k6eAd1
začaly	začít	k5eAaPmAgFnP
pozvolna	pozvolna	k6eAd1
pronikat	pronikat	k5eAaImF
nové	nový	k2eAgFnPc4d1
myšlenky	myšlenka	k1gFnPc4
a	a	k8xC
témata	téma	k1gNnPc4
typická	typický	k2eAgNnPc4d1
pro	pro	k7c4
katolické	katolický	k2eAgNnSc4d1
osvícenství	osvícenství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
vlastní	vlastní	k2eAgFnSc2d1
filosofie	filosofie	k1gFnSc2
se	se	k3xPyFc4
na	na	k7c6
fakultě	fakulta	k1gFnSc6
vyučovala	vyučovat	k5eAaImAgFnS
matematika	matematika	k1gFnSc1
<g/>
,	,	kIx,
spekulativní	spekulativní	k2eAgFnSc1d1
i	i	k8xC
experimentální	experimentální	k2eAgFnSc1d1
fyzika	fyzika	k1gFnSc1
<g/>
,	,	kIx,
etika	etika	k1gFnSc1
<g/>
,	,	kIx,
přírodní	přírodní	k2eAgFnPc1d1
vědy	věda	k1gFnPc1
<g/>
,	,	kIx,
montanistika	montanistika	k1gFnSc1
<g/>
,	,	kIx,
kameralistika	kameralistika	k1gFnSc1
<g/>
,	,	kIx,
rétorika	rétorika	k1gFnSc1
<g/>
,	,	kIx,
krásné	krásný	k2eAgFnPc1d1
vědy	věda	k1gFnPc1
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
rovněž	rovněž	k9
orientální	orientální	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
2	#num#	k4
<g/>
]	]	kIx)
Tyto	tento	k3xDgInPc1
předměty	předmět	k1gInPc1
profesoři	profesor	k1gMnPc1
přednášeli	přednášet	k5eAaImAgMnP
latinsky	latinsky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedinou	jediný	k2eAgFnSc7d1
výjimkou	výjimka	k1gFnSc7
byl	být	k5eAaImAgInS
osvícenský	osvícenský	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
z	z	k7c2
Lužice	Lužice	k1gFnSc2
Karl	Karl	k1gMnSc1
Heinrich	Heinrich	k1gMnSc1
Seibt	Seibt	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
již	již	k9
užíval	užívat	k5eAaImAgMnS
německého	německý	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
přednášky	přednáška	k1gFnPc4
o	o	k7c6
etice	etika	k1gFnSc6
<g/>
,	,	kIx,
pedagogice	pedagogika	k1gFnSc6
<g/>
,	,	kIx,
německé	německý	k2eAgFnSc6d1
stylistice	stylistika	k1gFnSc6
<g/>
,	,	kIx,
literatuře	literatura	k1gFnSc6
a	a	k8xC
historii	historie	k1gFnSc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
studenty	student	k1gMnPc4
„	„	k?
<g/>
oslňovaly	oslňovat	k5eAaImAgFnP
svou	svůj	k3xOyFgFnSc7
duchaplností	duchaplnost	k1gFnSc7
a	a	k8xC
vybroušeností	vybroušenost	k1gFnSc7
<g/>
“	“	k?
<g/>
,	,	kIx,
měly	mít	k5eAaImAgInP
na	na	k7c6
mladého	mladý	k2eAgMnSc2d1
Dobrovského	Dobrovského	k2eAgInSc4d1
významný	významný	k2eAgInSc4d1
vliv	vliv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seibtem	Seibto	k1gNnSc7
probudil	probudit	k5eAaPmAgInS
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
kriticismus	kriticismus	k1gInSc4
a	a	k8xC
příznačnou	příznačný	k2eAgFnSc4d1
ironii	ironie	k1gFnSc4
a	a	k8xC
ovlivnil	ovlivnit	k5eAaPmAgInS
jeho	jeho	k3xOp3gInSc1
styl	styl	k1gInSc1
vyjadřování	vyjadřování	k1gNnSc2
v	v	k7c6
německy	německy	k6eAd1
psaných	psaný	k2eAgInPc6d1
textech	text	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
to	ten	k3xDgNnSc4
rovněž	rovněž	k9
Seibt	Seibt	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
ho	on	k3xPp3gNnSc4
později	pozdě	k6eAd2
doporučil	doporučit	k5eAaPmAgMnS
hraběti	hrabě	k1gMnSc3
Nosticovi	Nostic	k1gMnSc3
jako	jako	k8xS,k8xC
vychovatele	vychovatel	k1gMnPc4
jeho	jeho	k3xOp3gMnPc2
synů	syn	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vedle	vedle	k7c2
Seibta	Seibt	k1gInSc2
měl	mít	k5eAaImAgInS
na	na	k7c4
Dobrovského	Dobrovského	k2eAgInSc4d1
důležitý	důležitý	k2eAgInSc4d1
vliv	vliv	k1gInSc4
matematik	matematik	k1gMnSc1
a	a	k8xC
jezuita	jezuita	k1gMnSc1
Joseph	Joseph	k1gMnSc1
Stepling	Stepling	k1gInSc4
<g/>
,	,	kIx,
pro	pro	k7c4
něhož	jenž	k3xRgNnSc4
byla	být	k5eAaImAgFnS
charakteristická	charakteristický	k2eAgFnSc1d1
náboženská	náboženský	k2eAgFnSc1d1
a	a	k8xC
mravní	mravní	k2eAgFnSc1d1
vážnost	vážnost	k1gFnSc1
spojená	spojený	k2eAgFnSc1d1
s	s	k7c7
„	„	k?
<g/>
břitkou	břitký	k2eAgFnSc7d1
racionalitou	racionalita	k1gFnSc7
a	a	k8xC
otevřeností	otevřenost	k1gFnSc7
vůči	vůči	k7c3
novým	nový	k2eAgInPc3d1
vědeckým	vědecký	k2eAgInPc3d1
přístupům	přístup	k1gInPc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Také	také	k9
jemu	on	k3xPp3gMnSc3
Dobrovský	Dobrovský	k1gMnSc1
vděčil	vděčit	k5eAaImAgInS
za	za	k7c4
četné	četný	k2eAgFnPc4d1
přímluvy	přímluva	k1gFnPc4
u	u	k7c2
zámožných	zámožný	k2eAgFnPc2d1
rodin	rodina	k1gFnPc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
si	se	k3xPyFc3
během	během	k7c2
svého	svůj	k3xOyFgNnSc2
studia	studio	k1gNnSc2
musel	muset	k5eAaImAgMnS
přivydělávat	přivydělávat	k5eAaImF
jako	jako	k9
domácí	domácí	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
na	na	k7c6
filosofické	filosofický	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
se	se	k3xPyFc4
pevně	pevně	k6eAd1
ustálily	ustálit	k5eAaPmAgInP
některé	některý	k3yIgInPc1
jeho	jeho	k3xOp3gInPc1
základní	základní	k2eAgInPc1d1
postoje	postoj	k1gInPc1
<g/>
,	,	kIx,
např.	např.	kA
nedůvěra	nedůvěra	k1gFnSc1
ke	k	k7c3
spekulaci	spekulace	k1gFnSc3
a	a	k8xC
metafyzice	metafyzika	k1gFnSc3
<g/>
,	,	kIx,
důraz	důraz	k1gInSc4
na	na	k7c4
ověřitelnost	ověřitelnost	k1gFnSc4
faktů	fakt	k1gInPc2
i	i	k8xC
záliba	záliba	k1gFnSc1
v	v	k7c6
historické	historický	k2eAgFnSc6d1
a	a	k8xC
textové	textový	k2eAgFnSc6d1
kritice	kritika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
filosofů	filosof	k1gMnPc2
jej	on	k3xPp3gMnSc4
zaujal	zaujmout	k5eAaPmAgInS
zvláště	zvláště	k6eAd1
předkritický	předkritický	k2eAgMnSc1d1
Kant	Kant	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1771	#num#	k4
osmnáctiletý	osmnáctiletý	k2eAgMnSc1d1
Dobrovský	Dobrovský	k1gMnSc1
absolvoval	absolvovat	k5eAaPmAgMnS
magisterské	magisterský	k2eAgNnSc4d1
studium	studium	k1gNnSc4
s	s	k7c7
nejlepším	dobrý	k2eAgInSc7d3
prospěchem	prospěch	k1gInSc7
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
ročníku	ročník	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
protože	protože	k8xS
se	se	k3xPyFc4
chtěl	chtít	k5eAaImAgMnS
nadále	nadále	k6eAd1
věnovat	věnovat	k5eAaImF,k5eAaPmF
vědě	věda	k1gFnSc3
<g/>
,	,	kIx,
rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
pro	pro	k7c4
kněžský	kněžský	k2eAgInSc4d1
stav	stav	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
školním	školní	k2eAgInSc6d1
roce	rok	k1gInSc6
1771	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
začal	začít	k5eAaPmAgMnS
studovat	studovat	k5eAaImF
teologii	teologie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
rok	rok	k1gInSc4
na	na	k7c6
teologické	teologický	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
se	se	k3xPyFc4
účastnil	účastnit	k5eAaImAgMnS
přednášek	přednáška	k1gFnPc2
z	z	k7c2
církevních	církevní	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
odpovídalo	odpovídat	k5eAaImAgNnS
tehdejším	tehdejší	k2eAgInPc3d1
tereziánským	tereziánský	k2eAgInPc3d1
předpisům	předpis	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potom	potom	k6eAd1
jej	on	k3xPp3gInSc4
Stepling	Stepling	k1gInSc4
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
další	další	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
doporučil	doporučit	k5eAaPmAgMnS
provinciálovi	provinciál	k1gMnSc3
Tovaryšstva	tovaryšstvo	k1gNnPc1
Ježíšova	Ježíšův	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
přísných	přísný	k2eAgFnPc6d1
zkouškách	zkouška	k1gFnPc6
z	z	k7c2
filosofie	filosofie	k1gFnSc2
se	se	k3xPyFc4
tak	tak	k9
Dobrovský	Dobrovský	k1gMnSc1
navzdory	navzdory	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
mu	on	k3xPp3gMnSc3
již	již	k6eAd1
bylo	být	k5eAaImAgNnS
19	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
ocitl	ocitnout	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1772	#num#	k4
v	v	k7c6
brněnském	brněnský	k2eAgInSc6d1
noviciátu	noviciát	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jezuitský	jezuitský	k2eAgInSc1d1
noviciát	noviciát	k1gInSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
(	(	kIx(
<g/>
1772	#num#	k4
<g/>
–	–	k?
<g/>
1773	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tuhá	tuhý	k2eAgFnSc1d1
disciplína	disciplína	k1gFnSc1
a	a	k8xC
těsné	těsný	k2eAgInPc1d1
myšlenkové	myšlenkový	k2eAgInPc1d1
mantinely	mantinel	k1gInPc1
noviciátu	noviciát	k1gInSc2
se	se	k3xPyFc4
zcela	zcela	k6eAd1
neslučovaly	slučovat	k5eNaImAgFnP
s	s	k7c7
povahou	povaha	k1gFnSc7
kriticky	kriticky	k6eAd1
uvažujícího	uvažující	k2eAgMnSc4d1
mladého	mladý	k2eAgMnSc4d1
muže	muž	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
řádové	řádový	k2eAgFnPc1d1
povinnosti	povinnost	k1gFnPc1
plnil	plnit	k5eAaImAgInS
s	s	k7c7
jistým	jistý	k2eAgInSc7d1
odstupem	odstup	k1gInSc7
a	a	k8xC
nejraději	rád	k6eAd3
se	se	k3xPyFc4
zde	zde	k6eAd1
zabýval	zabývat	k5eAaImAgInS
studiem	studio	k1gNnSc7
a	a	k8xC
četbou	četba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
byla	být	k5eAaImAgFnS
tehdy	tehdy	k6eAd1
péče	péče	k1gFnSc1
o	o	k7c4
novice	novic	k1gMnSc4
svěřena	svěřen	k2eAgMnSc4d1
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
literárně	literárně	k6eAd1
činnému	činný	k2eAgMnSc3d1
Františku	František	k1gMnSc3
Azzonimu	Azzonim	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
měl	mít	k5eAaImAgMnS
jako	jako	k8xC,k8xS
kazatel	kazatel	k1gMnSc1
zkušenosti	zkušenost	k1gFnSc2
z	z	k7c2
pobytu	pobyt	k1gInSc2
v	v	k7c6
jihoamerickém	jihoamerický	k2eAgInSc6d1
Quitu	Quit	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobrovský	Dobrovský	k1gMnSc1
později	pozdě	k6eAd2
zdůvodnil	zdůvodnit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
motiv	motiv	k1gInSc4
pro	pro	k7c4
vstup	vstup	k1gInSc4
do	do	k7c2
řádu	řád	k1gInSc2
v	v	k7c6
listu	list	k1gInSc6
polskému	polský	k2eAgMnSc3d1
filologovi	filolog	k1gMnSc3
Bandtkemu	Bandtkem	k1gMnSc3
takto	takto	k6eAd1
<g/>
:	:	kIx,
„	„	k?
<g/>
Mé	můj	k3xOp1gNnSc1
jezuitské	jezuitský	k2eAgNnSc1d1
smýšlení	smýšlení	k1gNnSc1
budete	být	k5eAaImBp2nP
si	se	k3xPyFc3
moci	moct	k5eAaImF
vysvětlit	vysvětlit	k5eAaPmF
z	z	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
jsem	být	k5eAaImIp1nS
u	u	k7c2
jezuitů	jezuita	k1gMnPc2
v	v	k7c6
Brně	Brno	k1gNnSc6
strávil	strávit	k5eAaPmAgInS
pouze	pouze	k6eAd1
jeden	jeden	k4xCgInSc1
rok	rok	k1gInSc1
(	(	kIx(
<g/>
1773	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
tehdy	tehdy	k6eAd1
jsem	být	k5eAaImIp1nS
měl	mít	k5eAaImAgMnS
chuť	chuť	k1gFnSc4
jet	jet	k5eAaImF
do	do	k7c2
Indie	Indie	k1gFnSc2
jako	jako	k8xC,k8xS
misionář	misionář	k1gMnSc1
a	a	k8xC
jezuitou	jezuita	k1gMnSc7
jsem	být	k5eAaImIp1nS
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
proto	proto	k8xC
<g/>
,	,	kIx,
abych	aby	kYmCp1nS
viděl	vidět	k5eAaImAgInS
nový	nový	k2eAgInSc1d1
svět	svět	k1gInSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1773	#num#	k4
byl	být	k5eAaImAgInS
papežskou	papežský	k2eAgFnSc7d1
bulou	bula	k1gFnSc7
jezuitský	jezuitský	k2eAgInSc1d1
řád	řád	k1gInSc1
zrušen	zrušit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
vzpomínek	vzpomínka	k1gFnPc2
spolunovice	spolunovice	k1gFnSc2
Helferta	Helferta	k1gFnSc1
četl	číst	k5eAaImAgMnS
Dobrovský	Dobrovský	k1gMnSc1
při	při	k7c6
rozloučení	rozloučení	k1gNnSc6
s	s	k7c7
přáteli	přítel	k1gMnPc7
svou	svůj	k3xOyFgFnSc4
latinsky	latinsky	k6eAd1
psanou	psaný	k2eAgFnSc4d1
báseň	báseň	k1gFnSc4
o	o	k7c6
lodi	loď	k1gFnSc6
mladíků	mladík	k1gMnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
z	z	k7c2
přístavu	přístav	k1gInSc2
vypuzena	vypudit	k5eAaPmNgFnS
do	do	k7c2
velkého	velký	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Teologie	teologie	k1gFnSc1
na	na	k7c6
Univerzitě	univerzita	k1gFnSc6
Karlo-Ferdinandově	Karlo-Ferdinandův	k2eAgFnSc6d1
(	(	kIx(
<g/>
1774	#num#	k4
<g/>
–	–	k?
<g/>
1777	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nostický	nostický	k2eAgInSc1d1
palác	palác	k1gInSc1
na	na	k7c6
Maltézském	maltézský	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
na	na	k7c6
Malé	Malé	k2eAgFnSc6d1
Straně	strana	k1gFnSc6
</s>
<s>
Na	na	k7c4
podzim	podzim	k1gInSc4
roku	rok	k1gInSc2
1773	#num#	k4
se	se	k3xPyFc4
Dobrovský	Dobrovský	k1gMnSc1
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
tu	tu	k6eAd1
dokončil	dokončit	k5eAaPmAgMnS
bohoslovecké	bohoslovecký	k2eAgNnSc4d1
studium	studium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
totiž	totiž	k9
zabral	zabrat	k5eAaPmAgMnS
do	do	k7c2
studia	studio	k1gNnSc2
orientálních	orientální	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
,	,	kIx,
nemínil	mínit	k5eNaImAgMnS
se	se	k3xPyFc4
již	již	k6eAd1
kněžského	kněžský	k2eAgInSc2d1
stavu	stav	k1gInSc2
zříci	zříct	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zrušení	zrušení	k1gNnSc1
jezuitského	jezuitský	k2eAgInSc2d1
řádu	řád	k1gInSc2
se	se	k3xPyFc4
citelně	citelně	k6eAd1
dotklo	dotknout	k5eAaPmAgNnS
poměrů	poměr	k1gInPc2
na	na	k7c6
teologické	teologický	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jezuité	jezuita	k1gMnPc1
byli	být	k5eAaImAgMnP
nuceni	nutit	k5eAaImNgMnP
odejít	odejít	k5eAaPmF
a	a	k8xC
na	na	k7c4
jejich	jejich	k3xOp3gNnPc4
místa	místo	k1gNnPc4
nastoupili	nastoupit	k5eAaPmAgMnP
světští	světský	k2eAgMnPc1d1
kněží	kněz	k1gMnPc1
a	a	k8xC
příslušníci	příslušník	k1gMnPc1
jiných	jiný	k2eAgInPc2d1
řádů	řád	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
čela	čelo	k1gNnSc2
fakulty	fakulta	k1gFnSc2
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
osvícensky	osvícensky	k6eAd1
smýšlející	smýšlející	k2eAgMnSc1d1
opat	opat	k1gMnSc1
břevnovsko-broumovský	břevnovsko-broumovský	k2eAgMnSc1d1
Franz	Franz	k1gMnSc1
Stephan	Stephan	k1gMnSc1
Rautenstrauch	Rautenstrauch	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
královně	královna	k1gFnSc3
předložil	předložit	k5eAaPmAgInS
projekt	projekt	k1gInSc1
celkové	celkový	k2eAgFnSc2d1
reformy	reforma	k1gFnSc2
studijního	studijní	k2eAgInSc2d1
plánu	plán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duch	duch	k1gMnSc1
těchto	tento	k3xDgFnPc2
reforem	reforma	k1gFnPc2
se	se	k3xPyFc4
projevil	projevit	k5eAaPmAgInS
zvláště	zvláště	k6eAd1
v	v	k7c6
oblasti	oblast	k1gFnSc6
dogmatiky	dogmatika	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
zproštěna	zprostit	k5eAaPmNgFnS
tradičních	tradiční	k2eAgMnPc2d1
výkladů	výklad	k1gInPc2
sv.	sv.	kA
Augustina	Augustin	k1gMnSc2
<g/>
,	,	kIx,
Tomáše	Tomáš	k1gMnSc2
<g/>
,	,	kIx,
Scota	Scota	k1gFnSc1
apod.	apod.	kA
Spekulativní	spekulativní	k2eAgFnSc1d1
složka	složka	k1gFnSc1
byla	být	k5eAaImAgFnS
potlačena	potlačit	k5eAaPmNgFnS
a	a	k8xC
ke	k	k7c3
slovu	slovo	k1gNnSc3
přišly	přijít	k5eAaPmAgFnP
metody	metoda	k1gFnPc1
zdomácnělé	zdomácnělý	k2eAgFnPc1d1
v	v	k7c6
protestantské	protestantský	k2eAgFnSc6d1
teologii	teologie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
nového	nový	k2eAgInSc2d1
studijního	studijní	k2eAgInSc2d1
programu	program	k1gInSc2
si	se	k3xPyFc3
Dobrovský	Dobrovský	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
reformu	reforma	k1gFnSc4
uvítal	uvítat	k5eAaPmAgMnS
<g/>
,	,	kIx,
zvolil	zvolit	k5eAaPmAgMnS
upravené	upravený	k2eAgNnSc4d1
studium	studium	k1gNnSc4
biblické	biblický	k2eAgFnSc2d1
teologie	teologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
textové	textový	k2eAgFnSc3d1
kritice	kritika	k1gFnSc3
a	a	k8xC
exegezi	exegeze	k1gFnSc3
<g/>
,	,	kIx,
takže	takže	k8xS
mohl	moct	k5eAaImAgMnS
uplatnit	uplatnit	k5eAaPmF
své	svůj	k3xOyFgNnSc4
filologické	filologický	k2eAgNnSc4d1
nadání	nadání	k1gNnSc4
<g/>
,	,	kIx,
vrozený	vrozený	k2eAgInSc4d1
kriticismus	kriticismus	k1gInSc4
i	i	k8xC
zájem	zájem	k1gInSc4
o	o	k7c4
náboženské	náboženský	k2eAgFnPc4d1
otázky	otázka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vliv	vliv	k1gInSc4
profesorů	profesor	k1gMnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
byli	být	k5eAaImAgMnP
hebraisté	hebraista	k1gMnPc1
Leopold	Leopold	k1gMnSc1
Tirsch	Tirsch	k1gMnSc1
a	a	k8xC
Hieronymus	Hieronymus	k1gMnSc1
Frida	Frida	k1gMnSc1
<g/>
,	,	kIx,
nelze	lze	k6eNd1
přeceňovat	přeceňovat	k5eAaImF
<g/>
,	,	kIx,
neboť	neboť	k8xC
pro	pro	k7c4
Dobrovského	Dobrovského	k2eAgNnSc4d1
mělo	mít	k5eAaImAgNnS
v	v	k7c6
této	tento	k3xDgFnSc6
fázi	fáze	k1gFnSc6
větší	veliký	k2eAgInSc4d2
význam	význam	k1gInSc4
samostudium	samostudium	k1gNnSc1
a	a	k8xC
učitelé	učitel	k1gMnPc1
stojící	stojící	k2eAgMnPc1d1
mimo	mimo	k7c4
univerzitu	univerzita	k1gFnSc4
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
především	především	k9
pavlánský	pavlánský	k2eAgMnSc1d1
orientalista	orientalista	k1gMnSc1
a	a	k8xC
později	pozdě	k6eAd2
rovněž	rovněž	k9
slavista	slavista	k1gMnSc1
Václav	Václav	k1gMnSc1
Fortunát	Fortunát	k1gInSc4
Durych	Durycha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
náskoku	náskok	k1gInSc3
protestantské	protestantský	k2eAgFnSc2d1
biblistiky	biblistika	k1gFnSc2
sledoval	sledovat	k5eAaImAgInS
mj.	mj.	kA
také	také	k6eAd1
práci	práce	k1gFnSc4
německého	německý	k2eAgMnSc2d1
teologa	teolog	k1gMnSc2
a	a	k8xC
orientalisty	orientalista	k1gMnSc2
Johanna	Johann	k1gMnSc2
Davida	David	k1gMnSc2
Michaelise	Michaelise	k1gFnSc2
<g/>
,	,	kIx,
za	za	k7c2
jehož	jenž	k3xRgMnSc2,k3xOyRp3gMnSc2
žáka	žák	k1gMnSc2
se	se	k3xPyFc4
považoval	považovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
dalších	další	k2eAgMnPc2d1
pedagogů	pedagog	k1gMnPc2
fakulty	fakulta	k1gFnSc2
v	v	k7c6
něm	on	k3xPp3gInSc6
zanechal	zanechat	k5eAaPmAgInS
dojem	dojem	k1gInSc1
křižovník	křižovník	k1gMnSc1
Franz	Franz	k1gMnSc1
Christian	Christian	k1gMnSc1
Pittroff	Pittroff	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Již	již	k6eAd1
během	během	k7c2
studia	studio	k1gNnSc2
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1775	#num#	k4
přijal	přijmout	k5eAaPmAgMnS
Dobrovský	Dobrovský	k1gMnSc1
nižší	nízký	k2eAgNnSc4d2
svěcení	svěcení	k1gNnSc4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1776	#num#	k4
byl	být	k5eAaImAgMnS
vysvěcen	vysvětit	k5eAaPmNgMnS
na	na	k7c4
subdiakona	subdiakon	k1gMnSc4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
na	na	k7c4
diakona	diakon	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všichni	všechen	k3xTgMnPc1
jáhni	jáhen	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
byli	být	k5eAaImAgMnP
vysvěceni	vysvětit	k5eAaPmNgMnP
spolu	spolu	k6eAd1
s	s	k7c7
ním	on	k3xPp3gNnSc7
<g/>
,	,	kIx,
dosáhli	dosáhnout	k5eAaPmAgMnP
kněžství	kněžství	k1gNnSc2
ještě	ještě	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1777	#num#	k4
<g/>
,	,	kIx,
jen	jen	k9
Dobrovský	Dobrovský	k1gMnSc1
byl	být	k5eAaImAgMnS
vysvěcen	vysvětit	k5eAaPmNgMnS
na	na	k7c4
kněze	kněz	k1gMnPc4
až	až	k9
roku	rok	k1gInSc2
1786	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1777	#num#	k4
úspěšně	úspěšně	k6eAd1
završil	završit	k5eAaPmAgInS
své	svůj	k3xOyFgNnSc4
formální	formální	k2eAgNnSc4d1
teologické	teologický	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
a	a	k8xC
vydal	vydat	k5eAaPmAgMnS
se	s	k7c7
–	–	k?
jako	jako	k8xC,k8xS
pouhý	pouhý	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
–	–	k?
do	do	k7c2
praktického	praktický	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
Seibtova	Seibtův	k2eAgNnSc2d1
a	a	k8xC
Steplingova	Steplingův	k2eAgNnSc2d1
doporučení	doporučení	k1gNnSc2
byl	být	k5eAaImAgInS
přijat	přijmout	k5eAaPmNgInS
jako	jako	k8xC,k8xS
domácí	domácí	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
do	do	k7c2
domu	dům	k1gInSc2
hrabat	hrabě	k1gNnPc2
Nosticů	Nostiec	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Soukromým	soukromý	k2eAgMnSc7d1
učitelem	učitel	k1gMnSc7
v	v	k7c6
domě	dům	k1gInSc6
Nosticů	Nostiec	k1gInPc2
(	(	kIx(
<g/>
1777	#num#	k4
<g/>
–	–	k?
<g/>
1787	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pečeť	pečeť	k1gFnSc1
Královské	královský	k2eAgFnSc2d1
české	český	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
nauk	nauka	k1gFnPc2
s	s	k7c7
nápisem	nápis	k1gInSc7
Societas	Societas	k1gMnSc1
Scientiarum	Scientiarum	k1gNnSc4
Bohemica	Bohemicus	k1gMnSc2
<g/>
,	,	kIx,
Praga	Prag	k1gMnSc2
MDCCLXXXIV	MDCCLXXXIV	kA
</s>
<s>
V	v	k7c6
domě	dům	k1gInSc6
hraběte	hrabě	k1gMnSc2
Františka	František	k1gMnSc2
Antonína	Antonín	k1gMnSc2
Nostice-Rienecka	Nostice-Rienecko	k1gNnSc2
vyučoval	vyučovat	k5eAaImAgMnS
Dobrovský	Dobrovský	k1gMnSc1
jeho	jeho	k3xOp3gFnSc2
čtyři	čtyři	k4xCgMnPc4
syny	syn	k1gMnPc7
matematice	matematika	k1gFnSc3
a	a	k8xC
filosofii	filosofie	k1gFnSc3
<g/>
,	,	kIx,
starší	starý	k2eAgMnSc1d2
ze	z	k7c2
synů	syn	k1gMnPc2
rovněž	rovněž	k9
doprovázel	doprovázet	k5eAaImAgMnS
na	na	k7c4
univerzitu	univerzita	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
byl	být	k5eAaImAgMnS
tak	tak	k6eAd1
přítomen	přítomen	k2eAgMnSc1d1
např.	např.	kA
právnickým	právnický	k2eAgFnPc3d1
přednáškám	přednáška	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prostředí	prostředí	k1gNnSc6
vyšších	vysoký	k2eAgFnPc2d2
vrstev	vrstva	k1gFnPc2
se	se	k3xPyFc4
pohyboval	pohybovat	k5eAaImAgInS
nenuceně	nuceně	k6eNd1
<g/>
,	,	kIx,
nikdy	nikdy	k6eAd1
nezáviděl	závidět	k5eNaImAgMnS
a	a	k8xC
nechoval	chovat	k5eNaImAgMnS
se	se	k3xPyFc4
podlézavě	podlézavě	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
měl	mít	k5eAaImAgInS
vedle	vedle	k7c2
svých	svůj	k3xOyFgFnPc2
povinností	povinnost	k1gFnPc2
dostatek	dostatek	k1gInSc1
času	čas	k1gInSc2
pro	pro	k7c4
vlastní	vlastní	k2eAgNnSc4d1
studium	studium	k1gNnSc4
orientálních	orientální	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
a	a	k8xC
hermeneutiky	hermeneutika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
jeho	jeho	k3xOp3gNnSc4
další	další	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
bylo	být	k5eAaImAgNnS
velmi	velmi	k6eAd1
důležité	důležitý	k2eAgNnSc1d1
setkávání	setkávání	k1gNnSc1
s	s	k7c7
dalšími	další	k2eAgMnPc7d1
učenci	učenec	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
u	u	k7c2
Nosticů	Nostiec	k1gInPc2
scházeli	scházet	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
osvícenského	osvícenský	k2eAgInSc2d1
absolutismu	absolutismus	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byly	být	k5eAaImAgFnP
mnohé	mnohý	k2eAgFnPc1d1
stavovské	stavovský	k2eAgFnPc1d1
kompetence	kompetence	k1gFnPc1
české	český	k2eAgFnSc2d1
šlechty	šlechta	k1gFnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
centralizace	centralizace	k1gFnSc2
státu	stát	k1gInSc2
omezeny	omezen	k2eAgInPc1d1
<g/>
,	,	kIx,
popř.	popř.	kA
rovnou	rovnou	k6eAd1
převedeny	převést	k5eAaPmNgFnP
do	do	k7c2
Vídně	Vídeň	k1gFnSc2
<g/>
,	,	kIx,
totiž	totiž	k9
rostla	růst	k5eAaImAgFnS
v	v	k7c6
těchto	tento	k3xDgInPc6
kruzích	kruh	k1gInPc6
poptávka	poptávka	k1gFnSc1
po	po	k7c6
hlubších	hluboký	k2eAgFnPc6d2
znalostech	znalost	k1gFnPc6
české	český	k2eAgFnSc2d1
historie	historie	k1gFnSc2
a	a	k8xC
bohemikální	bohemikální	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgMnSc7d1
z	z	k7c2
hraběcích	hraběcí	k2eAgMnPc2d1
vychovatelů	vychovatel	k1gMnPc2
byl	být	k5eAaImAgMnS
tehdy	tehdy	k6eAd1
historik	historik	k1gMnSc1
a	a	k8xC
filolog	filolog	k1gMnSc1
František	František	k1gMnSc1
Martin	Martin	k1gMnSc1
Pelcl	Pelcl	k1gMnSc1
<g/>
,	,	kIx,
s	s	k7c7
nímž	jenž	k3xRgMnSc7
Dobrovský	Dobrovský	k1gMnSc1
úzce	úzko	k6eAd1
spolupracoval	spolupracovat	k5eAaImAgMnS
při	při	k7c6
uspořádání	uspořádání	k1gNnSc6
a	a	k8xC
katalogizaci	katalogizace	k1gFnSc6
nostického	nostický	k2eAgInSc2d1
archivu	archiv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
se	se	k3xPyFc4
zprvu	zprvu	k6eAd1
ještě	ještě	k6eAd1
soustředil	soustředit	k5eAaPmAgInS
zejména	zejména	k9
na	na	k7c4
textovou	textový	k2eAgFnSc4d1
kritiku	kritika	k1gFnSc4
Bible	bible	k1gFnSc2
<g/>
,	,	kIx,
získal	získat	k5eAaPmAgMnS
během	během	k7c2
tří	tři	k4xCgNnPc2
let	léto	k1gNnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
pro	pro	k7c4
Pelcla	Pelcl	k1gMnSc4
shromažďoval	shromažďovat	k5eAaImAgMnS
podklady	podklad	k1gInPc4
<g/>
,	,	kIx,
značné	značný	k2eAgNnSc1d1
množství	množství	k1gNnSc1
dat	datum	k1gNnPc2
k	k	k7c3
bibliografii	bibliografie	k1gFnSc3
a	a	k8xC
zevrubnou	zevrubný	k2eAgFnSc4d1
znalost	znalost	k1gFnSc4
starších	starý	k2eAgInPc2d2
bohemikálních	bohemikální	k2eAgInPc2d1
tisků	tisk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
pak	pak	k6eAd1
těžiště	těžiště	k1gNnSc1
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
zájmu	zájem	k1gInSc2
postupně	postupně	k6eAd1
přesouvalo	přesouvat	k5eAaImAgNnS
k	k	k7c3
tématu	téma	k1gNnSc3
české	český	k2eAgFnSc2d1
historie	historie	k1gFnSc2
a	a	k8xC
českého	český	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
patrno	patrn	k2eAgNnSc1d1
již	jenž	k3xRgFnSc4
z	z	k7c2
jeho	jeho	k3xOp3gFnPc2
prvních	první	k4xOgFnPc2
publikací	publikace	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
literární	literární	k2eAgFnSc4d1
půdu	půda	k1gFnSc4
vstoupil	vstoupit	k5eAaPmAgMnS
Dobrovský	Dobrovský	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1777	#num#	k4
anonymním	anonymní	k2eAgInSc7d1
příspěvkem	příspěvek	k1gInSc7
o	o	k7c6
pražských	pražský	k2eAgInPc6d1
hebrejských	hebrejský	k2eAgInPc6d1
rukopisech	rukopis	k1gInPc6
Pragische	Pragisch	k1gFnSc2
Fragmente	fragment	k1gInSc5
Hebräischer	Hebräischra	k1gFnPc2
Handschriften	Handschriftno	k1gNnPc2
(	(	kIx(
<g/>
Pražské	pražský	k2eAgInPc1d1
fragmenty	fragment	k1gInPc1
hebrejských	hebrejský	k2eAgInPc2d1
rukopisů	rukopis	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
mu	on	k3xPp3gMnSc3
v	v	k7c6
Göttingenu	Göttingen	k1gInSc6
v	v	k7c6
časopise	časopis	k1gInSc6
Orientalische	Orientalisch	k1gFnSc2
und	und	k?
exegetische	exegetische	k1gInSc1
Bibliothek	Bibliothek	k6eAd1
otiskl	otisknout	k5eAaPmAgInS
Johann	Johann	k1gInSc1
David	David	k1gMnSc1
Michaelis	Michaelis	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
později	pozdě	k6eAd2
v	v	k7c6
recenzi	recenze	k1gFnSc6
uvedl	uvést	k5eAaPmAgMnS
Dobrovského	Dobrovského	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc1d1
publikační	publikační	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
mladého	mladý	k2eAgMnSc2d1
autora	autor	k1gMnSc2
však	však	k9
nevzbuzovala	vzbuzovat	k5eNaImAgFnS
jen	jen	k9
kladný	kladný	k2eAgInSc4d1
ohlas	ohlas	k1gInSc4
<g/>
,	,	kIx,
protože	protože	k8xS
jeho	jeho	k3xOp3gFnSc1
ironie	ironie	k1gFnSc1
a	a	k8xC
způsob	způsob	k1gInSc1
<g/>
,	,	kIx,
s	s	k7c7
jakým	jaký	k3yIgInSc7,k3yRgInSc7,k3yQgInSc7
vynášel	vynášet	k5eAaImAgMnS
své	svůj	k3xOyFgInPc4
kritické	kritický	k2eAgInPc4d1
soudy	soud	k1gInPc4
<g/>
,	,	kIx,
nezřídka	nezřídka	k6eAd1
vyvolávaly	vyvolávat	k5eAaImAgFnP
podrážděné	podrážděný	k2eAgFnPc1d1
reakce	reakce	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
v	v	k7c6
roce	rok	k1gInSc6
1778	#num#	k4
otiskl	otisknout	k5eAaPmAgInS
a	a	k8xC
okomentoval	okomentovat	k5eAaPmAgInS
latinský	latinský	k2eAgInSc1d1
zlomek	zlomek	k1gInSc1
evangelia	evangelium	k1gNnSc2
Markova	Markův	k2eAgInSc2d1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
(	(	kIx(
<g/>
přestože	přestože	k8xS
byl	být	k5eAaImAgInS
dlouho	dlouho	k6eAd1
považován	považován	k2eAgInSc1d1
za	za	k7c4
evangelistův	evangelistův	k2eAgInSc4d1
autograf	autograf	k1gInSc4
<g/>
)	)	kIx)
datoval	datovat	k5eAaImAgInS
do	do	k7c2
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
vzbudil	vzbudit	k5eAaPmAgInS
nelibost	nelibost	k1gFnSc4
církevní	církevní	k2eAgFnSc2d1
hierarchie	hierarchie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
byl	být	k5eAaImAgMnS
jménem	jméno	k1gNnSc7
arcibiskupa	arcibiskup	k1gMnSc2
předvolán	předvolat	k5eAaPmNgMnS
generálním	generální	k2eAgMnSc7d1
vikářem	vikář	k1gMnSc7
Tvrdým	Tvrdý	k1gMnSc7
<g/>
,	,	kIx,
však	však	k9
uvedl	uvést	k5eAaPmAgMnS
přesvědčivé	přesvědčivý	k2eAgInPc4d1
argumenty	argument	k1gInPc4
pro	pro	k7c4
svá	svůj	k3xOyFgNnPc4
tvrzení	tvrzení	k1gNnPc4
a	a	k8xC
Tvrdý	tvrdý	k2eAgMnSc1d1
se	se	k3xPyFc4
s	s	k7c7
jeho	jeho	k3xOp3gFnSc7
odpovědí	odpověď	k1gFnSc7
plně	plně	k6eAd1
spokojil	spokojit	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Již	již	k9
první	první	k4xOgNnSc1
číslo	číslo	k1gNnSc1
Dobrovského	Dobrovského	k2eAgInSc2d1
časopisu	časopis	k1gInSc2
Böhmische	Böhmisch	k1gFnPc4
Litteratur	Litteratura	k1gFnPc2
auf	auf	k?
das	das	k?
Jahr	Jahr	k1gInSc1
1779	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
vedle	vedle	k7c2
přehledu	přehled	k1gInSc2
literární	literární	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
za	za	k7c4
rok	rok	k1gInSc4
1779	#num#	k4
jako	jako	k8xS,k8xC
odpovědný	odpovědný	k2eAgMnSc1d1
redaktor	redaktor	k1gMnSc1
nevyhýbal	vyhýbat	k5eNaImAgMnS
ani	ani	k8xC
hodnocení	hodnocení	k1gNnSc4
knih	kniha	k1gFnPc2
a	a	k8xC
aktuálních	aktuální	k2eAgFnPc2d1
událostí	událost	k1gFnPc2
<g/>
,	,	kIx,
vyvolalo	vyvolat	k5eAaPmAgNnS
negativní	negativní	k2eAgFnSc4d1
reakci	reakce	k1gFnSc4
konzervativních	konzervativní	k2eAgMnPc2d1
kruhů	kruh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Názory	názor	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
zde	zde	k6eAd1
Dobrovský	Dobrovský	k1gMnSc1
prezentoval	prezentovat	k5eAaBmAgMnS
<g/>
,	,	kIx,
se	se	k3xPyFc4
na	na	k7c6
jedné	jeden	k4xCgFnSc6
straně	strana	k1gFnSc6
vyznačovaly	vyznačovat	k5eAaImAgFnP
kritikou	kritika	k1gFnSc7
zpátečnických	zpátečnický	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
straně	strana	k1gFnSc6
druhé	druhý	k4xOgFnSc6
podporou	podpora	k1gFnSc7
a	a	k8xC
propagací	propagace	k1gFnSc7
osvícenských	osvícenský	k2eAgFnPc2d1
myšlenek	myšlenka	k1gFnPc2
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
zálibou	záliba	k1gFnSc7
v	v	k7c6
přírodě	příroda	k1gFnSc6
a	a	k8xC
přírodovědném	přírodovědný	k2eAgNnSc6d1
bádání	bádání	k1gNnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
zřejmě	zřejmě	k6eAd1
podnícena	podnícen	k2eAgFnSc1d1
styky	styk	k1gInPc4
s	s	k7c7
osvícencem	osvícenec	k1gMnSc7
<g/>
,	,	kIx,
mineralogem	mineralog	k1gMnSc7
<g/>
,	,	kIx,
geologem	geolog	k1gMnSc7
<g/>
,	,	kIx,
montanistou	montanista	k1gMnSc7
a	a	k8xC
svobodným	svobodný	k2eAgMnSc7d1
zednářem	zednář	k1gMnSc7
Ignácem	Ignác	k1gMnSc7
Antonínem	Antonín	k1gMnSc7
Bornem	Born	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
vydáním	vydání	k1gNnSc7
Balbínova	Balbínův	k2eAgNnSc2d1
rukopisného	rukopisný	k2eAgNnSc2d1
díla	dílo	k1gNnSc2
Bohemia	bohemia	k1gFnSc1
docta	docta	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1777	#num#	k4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
se	se	k3xPyFc4
ujal	ujmout	k5eAaPmAgInS
strahovský	strahovský	k2eAgMnSc1d1
knihovník	knihovník	k1gMnSc1
Karel	Karel	k1gMnSc1
Rafael	Rafael	k1gMnSc1
Ungar	Ungar	k1gMnSc1
<g/>
,	,	kIx,
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
Dobrovský	Dobrovský	k1gMnSc1
do	do	k7c2
periodika	periodikum	k1gNnSc2
Prager	Prager	k1gMnSc1
Inteligenzblatt	Inteligenzblatt	k1gMnSc1
kritiku	kritika	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
počátkem	počátkem	k7c2
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
vleklého	vleklý	k2eAgInSc2d1
sporu	spor	k1gInSc2
s	s	k7c7
Ungarem	Ungar	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
<g/>
,	,	kIx,
co	co	k9
Ungarově	Ungarův	k2eAgFnSc6d1
edici	edice	k1gFnSc6
vytýkal	vytýkat	k5eAaImAgMnS
<g/>
,	,	kIx,
následně	následně	k6eAd1
publikoval	publikovat	k5eAaBmAgMnS
v	v	k7c6
dodatcích	dodatek	k1gInPc6
a	a	k8xC
opravách	oprava	k1gFnPc6
pod	pod	k7c7
názvem	název	k1gInSc7
Corrigenda	Corrigenda	k1gFnSc1
in	in	k?
Bohemia	bohemia	k1gFnSc1
docta	docta	k1gFnSc1
Balbini	Balbin	k2eAgMnPc1d1
juxta	juxta	k1gFnSc1
editionem	edition	k1gInSc7
P.	P.	kA
Raphaelis	Raphaelis	k1gInSc1
Ungarn	Ungarn	k1gNnSc1
canon	canon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praemonstr	Praemonstr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
SS	SS	kA
<g/>
.	.	kIx.
theologiae	theologiae	k1gInSc1
doctoris	doctoris	k1gInSc1
(	(	kIx(
<g/>
Opravy	oprava	k1gFnPc1
v	v	k7c6
Balbínových	Balbínův	k2eAgFnPc6d1
Učených	učený	k2eAgFnPc6d1
Čechách	Čechy	k1gFnPc6
vydaných	vydaný	k2eAgInPc2d1
kanovníkem	kanovník	k1gMnSc7
<g/>
,	,	kIx,
totiž	totiž	k9
premonstrátem	premonstrát	k1gMnSc7
a	a	k8xC
doktorem	doktor	k1gMnSc7
teologie	teologie	k1gFnSc2
P.	P.	kA
Rafaelem	Rafael	k1gMnSc7
Ungarem	Ungar	k1gMnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
těchto	tento	k3xDgInPc2
sporů	spor	k1gInPc2
ovšem	ovšem	k9
bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
zabývat	zabývat	k5eAaImF
otázkami	otázka	k1gFnPc7
české	český	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
stále	stále	k6eAd1
intenzivněji	intenzivně	k6eAd2
<g/>
,	,	kIx,
o	o	k7c6
čemž	což	k3yQnSc6,k3yRnSc6
svědčí	svědčit	k5eAaImIp3nS
např.	např.	kA
pojednání	pojednání	k1gNnSc1
o	o	k7c6
původu	původ	k1gInSc6
slova	slovo	k1gNnSc2
„	„	k?
<g/>
Čech	Čechy	k1gFnPc2
<g/>
“	“	k?
Abhandlung	Abhandlung	k1gMnSc1
über	über	k1gMnSc1
den	den	k1gInSc4
Ursprung	Ursprung	k1gInSc1
des	des	k1gNnSc1
Namens	Namens	k1gInSc1
Tschech	Tschech	k1gInSc1
(	(	kIx(
<g/>
Czech	Czech	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Tschechen	Tschechen	k1gInSc1
(	(	kIx(
<g/>
Pojednání	pojednání	k1gNnSc1
o	o	k7c6
původu	původ	k1gInSc6
jména	jméno	k1gNnSc2
Čech	Čechy	k1gFnPc2
<g/>
)	)	kIx)
z	z	k7c2
roku	rok	k1gInSc2
1782	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
roku	rok	k1gInSc2
1782	#num#	k4
se	se	k3xPyFc4
Dobrovský	Dobrovský	k1gMnSc1
zúčastnil	zúčastnit	k5eAaPmAgMnS
honu	hon	k1gInSc2
v	v	k7c6
jindřichovické	jindřichovický	k2eAgFnSc6d1
oboře	obora	k1gFnSc6
u	u	k7c2
dnešního	dnešní	k2eAgInSc2d1
Sokolova	Sokolov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
viděl	vidět	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
hraběnka	hraběnka	k1gFnSc1
Nosticová	Nosticová	k1gFnSc1
míří	mířit	k5eAaImIp3nS
na	na	k7c4
zvěř	zvěř	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
běžela	běžet	k5eAaImAgFnS
k	k	k7c3
místu	místo	k1gNnSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
s	s	k7c7
mladým	mladý	k2eAgMnSc7d1
hrabětem	hrabě	k1gMnSc7
stál	stát	k5eAaImAgMnS
<g/>
,	,	kIx,
strhl	strhnout	k5eAaPmAgMnS
ho	on	k3xPp3gInSc4
okamžitě	okamžitě	k6eAd1
za	za	k7c4
sebe	sebe	k3xPyFc4
a	a	k8xC
zakryl	zakrýt	k5eAaPmAgMnS
jej	on	k3xPp3gMnSc4
svým	svůj	k3xOyFgNnSc7
tělem	tělo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tom	ten	k3xDgNnSc6
již	již	k9
padl	padnout	k5eAaPmAgMnS,k5eAaImAgMnS
výstřel	výstřel	k1gInSc4
a	a	k8xC
kulka	kulka	k1gFnSc1
zasáhla	zasáhnout	k5eAaPmAgFnS
Dobrovského	Dobrovský	k1gMnSc4
do	do	k7c2
kyčle	kyčel	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
několika	několik	k4yIc6
dnech	den	k1gInPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
v	v	k7c6
dosti	dosti	k6eAd1
vážném	vážný	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
postupně	postupně	k6eAd1
zotavil	zotavit	k5eAaPmAgMnS
<g/>
,	,	kIx,
kulku	kulka	k1gFnSc4
však	však	k9
v	v	k7c6
těle	tělo	k1gNnSc6
nosil	nosit	k5eAaImAgInS
po	po	k7c4
zbytek	zbytek	k1gInSc4
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
svému	svůj	k3xOyFgMnSc3
chovanci	chovanec	k1gMnSc3
Bedřichu	Bedřich	k1gMnSc3
Nosticovi	Nostic	k1gMnSc3
zachránil	zachránit	k5eAaPmAgMnS
život	život	k1gInSc4
a	a	k8xC
sám	sám	k3xTgMnSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
smrtelného	smrtelný	k2eAgNnSc2d1
nebezpečí	nebezpečí	k1gNnSc2
<g/>
,	,	kIx,
mu	on	k3xPp3gMnSc3
byli	být	k5eAaImAgMnP
Nosticové	Nostic	k1gMnPc1
zavázáni	zavázat	k5eAaPmNgMnP
velkou	velký	k2eAgFnSc7d1
vděčností	vděčnost	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
mu	on	k3xPp3gMnSc3
potom	potom	k6eAd1
prokazovali	prokazovat	k5eAaImAgMnP
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
propukla	propuknout	k5eAaPmAgFnS
Dobrovského	Dobrovského	k2eAgFnSc1d1
duševní	duševní	k2eAgFnSc1d1
nemoc	nemoc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1784	#num#	k4
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
Praze	Praha	k1gFnSc6
v	v	k7c6
okruhu	okruh	k1gInSc6
Ignáce	Ignác	k1gMnSc2
Antonína	Antonín	k1gMnSc2
Borna	Born	k1gMnSc2
Královská	královský	k2eAgFnSc1d1
česká	český	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
nauk	nauka	k1gFnPc2
(	(	kIx(
<g/>
Königliche	Königliche	k1gInSc1
böhmische	böhmischat	k5eAaPmIp3nS
Gesellschaft	Gesellschaft	k2eAgInSc1d1
der	drát	k5eAaImRp2nS
Wissenschaften	Wissenschaftno	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
po	po	k7c6
více	hodně	k6eAd2
než	než	k8xS
půlstoletí	půlstoletí	k1gNnSc4
jedinou	jediný	k2eAgFnSc7d1
institucí	instituce	k1gFnSc7
svého	svůj	k3xOyFgInSc2
druhu	druh	k1gInSc2
(	(	kIx(
<g/>
tj.	tj.	kA
akademií	akademie	k1gFnPc2
věd	věda	k1gFnPc2
<g/>
)	)	kIx)
v	v	k7c6
rakouské	rakouský	k2eAgFnSc6d1
monarchii	monarchie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
ze	z	k7c2
zakládajících	zakládající	k2eAgMnPc2d1
členů	člen	k1gMnPc2
této	tento	k3xDgFnSc2
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
po	po	k7c4
dlouhá	dlouhý	k2eAgNnPc4d1
léta	léto	k1gNnPc4
významně	významně	k6eAd1
určoval	určovat	k5eAaImAgInS
směr	směr	k1gInSc4
její	její	k3xOp3gFnSc2
činnosti	činnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Exkurz	exkurz	k1gInSc1
<g/>
:	:	kIx,
Dobrovský	Dobrovský	k1gMnSc1
a	a	k8xC
zednářství	zednářství	k1gNnSc1
</s>
<s>
Uroboros	Uroborosa	k1gFnPc2
jako	jako	k8xC,k8xS
symbol	symbol	k1gInSc1
smrtelnosti	smrtelnost	k1gFnSc2
<g/>
;	;	kIx,
rytina	rytina	k1gFnSc1
Crispina	Crispina	k1gFnSc1
van	van	k1gInSc4
Passe	Passe	k1gFnSc2
ve	v	k7c6
sbírce	sbírka	k1gFnSc6
George	Georg	k1gMnSc2
Withera	Wither	k1gMnSc2
A	a	k8xC
Collection	Collection	k1gInSc4
of	of	k?
Emblemes	Emblemes	k1gInSc1
<g/>
,	,	kIx,
Ancient	Ancient	k1gInSc1
and	and	k?
Moderne	Modern	k1gInSc5
(	(	kIx(
<g/>
1635	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
nejstarší	starý	k2eAgFnSc6d3
biografické	biografický	k2eAgFnSc6d1
literatuře	literatura	k1gFnSc6
se	se	k3xPyFc4
považovalo	považovat	k5eAaImAgNnS
za	za	k7c4
jisté	jistý	k2eAgNnSc4d1
<g/>
,	,	kIx,
že	že	k8xS
Dobrovský	Dobrovský	k1gMnSc1
byl	být	k5eAaImAgMnS
v	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
svobodným	svobodný	k2eAgMnSc7d1
zednářem	zednář	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brandl	Brandl	k1gFnSc1
např.	např.	kA
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
náležel	náležet	k5eAaImAgInS
k	k	k7c3
Svatojánské	svatojánský	k2eAgFnSc3d1
zednářské	zednářský	k2eAgFnSc3d1
lóži	lóže	k1gFnSc3
(	(	kIx(
<g/>
Die	Die	k1gFnSc3
sehr	sehra	k1gFnPc2
ehrwürdige	ehrwürdige	k1gNnSc2
S.	S.	kA
<g/>
(	(	kIx(
<g/>
anct	anct	k1gMnSc1
<g/>
)	)	kIx)
J.	J.	kA
<g/>
(	(	kIx(
<g/>
ohanns	ohanns	k1gInSc1
<g/>
)	)	kIx)
Loge	Loge	k1gInSc1
zur	zur	k?
Wahrheit	Wahrheit	k1gInSc1
und	und	k?
Einigkeit	Einigkeit	k1gInSc1
im	im	k?
Orient	Orient	k1gInSc1
zu	zu	k?
Prag	Prag	k1gInSc1
<g/>
)	)	kIx)
založené	založený	k2eAgFnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1780	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
jejímž	jejíž	k3xOyRp3gInSc6
znaku	znak	k1gInSc6
byli	být	k5eAaImAgMnP
dva	dva	k4xCgMnPc4
propletení	propletený	k2eAgMnPc1d1
hadi	had	k1gMnPc1
s	s	k7c7
korunou	koruna	k1gFnSc7
představující	představující	k2eAgInSc1d1
symbol	symbol	k1gInSc1
věčnosti	věčnost	k1gFnSc2
obepínající	obepínající	k2eAgInSc1d1
rovnostranný	rovnostranný	k2eAgInSc1d1
trojúhelník	trojúhelník	k1gInSc1
s	s	k7c7
nápisem	nápis	k1gInSc7
<g/>
:	:	kIx,
„	„	k?
<g/>
Wahrheit	Wahrheit	k1gInSc1
und	und	k?
Eintracht	Eintracht	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
Dobrovského	Dobrovského	k2eAgNnSc4d1
členství	členství	k1gNnSc4
skutečně	skutečně	k6eAd1
existuje	existovat	k5eAaImIp3nS
řada	řada	k1gFnSc1
nepřímých	přímý	k2eNgFnPc2d1
indicií	indicie	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
druh	druh	k1gInSc1
filantropického	filantropický	k2eAgNnSc2d1
náboženství	náboženství	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
jehož	jehož	k3xOyRp3gInSc6
rámci	rámec	k1gInSc6
bylo	být	k5eAaImAgNnS
v	v	k7c6
úzkém	úzký	k2eAgInSc6d1
kruhu	kruh	k1gInSc6
vyvolených	vyvolená	k1gFnPc2
možné	možný	k2eAgInPc1d1
naplňovat	naplňovat	k5eAaImF
osvícenské	osvícenský	k2eAgInPc4d1
ideály	ideál	k1gInPc4
<g/>
,	,	kIx,
mu	on	k3xPp3gMnSc3
jistě	jistě	k9
imponoval	imponovat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
si	se	k3xPyFc3
lze	lze	k6eAd1
představit	představit	k5eAaPmF
<g/>
,	,	kIx,
s	s	k7c7
jakým	jaký	k3yRgNnSc7,k3yIgNnSc7,k3yQgNnSc7
sebezapřením	sebezapření	k1gNnSc7
by	by	kYmCp3nS
dodržoval	dodržovat	k5eAaImAgMnS
zednářskou	zednářský	k2eAgFnSc4d1
zásadu	zásada	k1gFnSc4
podřízení	podřízení	k1gNnSc2
se	se	k3xPyFc4
vyšší	vysoký	k2eAgFnSc3d2
autoritě	autorita	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Určité	určitý	k2eAgInPc1d1
náznaky	náznak	k1gInPc1
lze	lze	k6eAd1
nalézt	nalézt	k5eAaPmF,k5eAaBmF
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
korespondenci	korespondence	k1gFnSc6
<g/>
,	,	kIx,
když	když	k8xS
byl	být	k5eAaImAgMnS
např.	např.	kA
ochoten	ochoten	k2eAgMnSc1d1
přistoupit	přistoupit	k5eAaPmF
na	na	k7c4
„	„	k?
<g/>
tajnou	tajný	k2eAgFnSc4d1
<g/>
“	“	k?
výměnu	výměna	k1gFnSc4
informací	informace	k1gFnPc2
s	s	k7c7
jiným	jiný	k2eAgMnSc7d1
učencem	učenec	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
zednářské	zednářský	k2eAgFnSc3d1
symbolice	symbolika	k1gFnSc3
pak	pak	k6eAd1
inklinoval	inklinovat	k5eAaImAgMnS
zvláště	zvláště	k6eAd1
během	během	k7c2
manických	manický	k2eAgFnPc2d1
epizod	epizoda	k1gFnPc2
své	svůj	k3xOyFgFnSc2
nemoci	nemoc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
Dobrovský	Dobrovský	k1gMnSc1
o	o	k7c6
svém	svůj	k3xOyFgNnSc6
(	(	kIx(
<g/>
ať	ať	k8xS,k8xC
již	již	k9
skutečném	skutečný	k2eAgInSc6d1
či	či	k8xC
domnělém	domnělý	k2eAgMnSc6d1
<g/>
)	)	kIx)
zednářství	zednářství	k1gNnSc6
zachovával	zachovávat	k5eAaImAgMnS
„	„	k?
<g/>
striktní	striktní	k2eAgFnSc3d1
observanci	observance	k1gFnSc3
<g/>
“	“	k?
<g/>
,	,	kIx,
nelze	lze	k6eNd1
vyloučit	vyloučit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gInPc4
kontakty	kontakt	k1gInPc4
se	s	k7c7
zednářskými	zednářský	k2eAgInPc7d1
kruhy	kruh	k1gInPc7
a	a	k8xC
příslušnost	příslušnost	k1gFnSc1
k	k	k7c3
josefínské	josefínský	k2eAgFnSc3d1
církevní	církevní	k2eAgFnSc3d1
elitě	elita	k1gFnSc3
byly	být	k5eAaImAgFnP
po	po	k7c6
roce	rok	k1gInSc6
1790	#num#	k4
důvodem	důvod	k1gInSc7
pro	pro	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
opakovaně	opakovaně	k6eAd1
neuspěl	uspět	k5eNaPmAgMnS
při	při	k7c6
snaze	snaha	k1gFnSc6
získat	získat	k5eAaPmF
místo	místo	k1gNnSc4
ve	v	k7c6
státní	státní	k2eAgFnSc6d1
službě	služba	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kněžské	kněžský	k2eAgNnSc1d1
svěcení	svěcení	k1gNnSc1
(	(	kIx(
<g/>
1786	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bylo	být	k5eAaImAgNnS
již	již	k6eAd1
řečeno	říct	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
všichni	všechen	k3xTgMnPc1
Dobrovského	Dobrovského	k2eAgMnPc1d1
spolužáci	spolužák	k1gMnPc1
dosáhli	dosáhnout	k5eAaPmAgMnP
kněžství	kněžství	k1gNnSc4
ještě	ještě	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1777	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobrovský	Dobrovský	k1gMnSc1
byl	být	k5eAaImAgInS
však	však	k9
vysvěcen	vysvětit	k5eAaPmNgInS
až	až	k9
17	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1786	#num#	k4
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
mimo	mimo	k7c4
svou	svůj	k3xOyFgFnSc4
domovskou	domovský	k2eAgFnSc4d1
diecézi	diecéze	k1gFnSc4
<g/>
,	,	kIx,
z	z	k7c2
níž	jenž	k3xRgFnSc2
byl	být	k5eAaImAgInS
propuštěn	propuštěn	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osvícenské	osvícenský	k2eAgNnSc1d1
ovzduší	ovzduší	k1gNnSc3
<g/>
,	,	kIx,
přednášky	přednáška	k1gFnPc1
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
a	a	k8xC
samostatné	samostatný	k2eAgNnSc1d1
studium	studium	k1gNnSc1
protestantské	protestantský	k2eAgFnSc2d1
biblické	biblický	k2eAgFnSc2d1
hermeneutiky	hermeneutika	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
byly	být	k5eAaImAgFnP
součástí	součást	k1gFnSc7
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
života	život	k1gInSc2
po	po	k7c6
jáhenském	jáhenský	k2eAgNnSc6d1
svěcení	svěcení	k1gNnSc6
<g/>
,	,	kIx,
uvedly	uvést	k5eAaPmAgInP
Dobrovského	Dobrovského	k2eAgInPc1d1
do	do	k7c2
tíživých	tíživý	k2eAgFnPc2d1
pochybností	pochybnost	k1gFnPc2
ve	v	k7c6
vztahu	vztah	k1gInSc6
ke	k	k7c3
kněžství	kněžství	k1gNnSc3
a	a	k8xC
s	s	k7c7
ním	on	k3xPp3gMnSc7
spojené	spojený	k2eAgFnSc6d1
církevní	církevní	k2eAgFnSc6d1
přísaze	přísaha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
patrné	patrný	k2eAgNnSc1d1
např.	např.	kA
z	z	k7c2
dopisu	dopis	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
mu	on	k3xPp3gMnSc3
v	v	k7c6
roce	rok	k1gInSc6
1778	#num#	k4
psal	psát	k5eAaImAgMnS
přítel	přítel	k1gMnSc1
ze	z	k7c2
studií	studio	k1gNnPc2
<g/>
,	,	kIx,
Lužický	lužický	k2eAgMnSc1d1
Srb	Srb	k1gMnSc1
Jakub	Jakub	k1gMnSc1
Žur	Žur	k1gMnSc1
(	(	kIx(
<g/>
Sauer	Sauer	k1gMnSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
„	„	k?
<g/>
V	v	k7c6
souladu	soulad	k1gInSc6
se	s	k7c7
svým	svůj	k3xOyFgNnSc7
smýšlením	smýšlení	k1gNnSc7
nejste	být	k5eNaImIp2nP
nyní	nyní	k6eAd1
schopen	schopen	k2eAgMnSc1d1
složit	složit	k5eAaPmF
vyznání	vyznání	k1gNnSc4
víry	víra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křivě	křivě	k6eAd1
přísahat	přísahat	k5eAaImF
nebudete	být	k5eNaImBp2nP
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
vím	vědět	k5eAaImIp1nS
příliš	příliš	k6eAd1
dobře	dobře	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
nezůstanete	zůstat	k5eNaPmIp2nP
také	také	k9
napořád	napořád	k6eAd1
domácím	domácí	k2eAgMnSc7d1
učitelem	učitel	k1gMnSc7
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
odkladu	odklad	k1gInSc6
svěcení	svěcení	k1gNnSc2
při	při	k7c6
tom	ten	k3xDgNnSc6
nehrály	hrát	k5eNaImAgInP
roli	role	k1gFnSc4
žádné	žádný	k3yNgFnPc4
vnější	vnější	k2eAgFnPc4d1
překážky	překážka	k1gFnPc4
<g/>
,	,	kIx,
sám	sám	k3xTgMnSc1
arcibiskup	arcibiskup	k1gMnSc1
prokazatelně	prokazatelně	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1783	#num#	k4
souhlasil	souhlasit	k5eAaImAgMnS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
Dobrovský	Dobrovský	k1gMnSc1
nechal	nechat	k5eAaPmAgMnS
vysvětit	vysvětit	k5eAaPmF
z	z	k7c2
rukou	ruka	k1gFnPc2
biskupa	biskup	k1gMnSc2
Haye	Hay	k1gMnSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
myšlenkově	myšlenkově	k6eAd1
spřízněného	spřízněný	k2eAgMnSc2d1
osvícence	osvícenec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatím	zatím	k6eAd1
však	však	k9
stále	stále	k6eAd1
sváděl	svádět	k5eAaImAgInS
svůj	svůj	k3xOyFgInSc4
vnitřní	vnitřní	k2eAgInSc4d1
boj	boj	k1gInSc4
a	a	k8xC
vysvěcení	vysvěcení	k1gNnSc4
odmítal	odmítat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1786	#num#	k4
souhlasil	souhlasit	k5eAaImAgInS
a	a	k8xC
kněžského	kněžský	k2eAgNnSc2d1
svěcení	svěcení	k1gNnSc2
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
dostalo	dostat	k5eAaPmAgNnS
z	z	k7c2
rukou	ruka	k1gFnSc7
zmíněného	zmíněný	k2eAgInSc2d1
královéhradeckého	královéhradecký	k2eAgInSc2d1
biskupa	biskup	k1gInSc2
Leopolda	Leopold	k1gMnSc2
Haye	Hay	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
již	již	k6eAd1
bylo	být	k5eAaImAgNnS
jen	jen	k9
otázkou	otázka	k1gFnSc7
času	čas	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Dobrovský	Dobrovský	k1gMnSc1
dostane	dostat	k5eAaPmIp3nS
příležitost	příležitost	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
své	svůj	k3xOyFgFnPc4
mimořádné	mimořádný	k2eAgFnPc4d1
schopnosti	schopnost	k1gFnPc4
prokázal	prokázat	k5eAaPmAgInS
ve	v	k7c6
státních	státní	k2eAgFnPc6d1
službách	služba	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vicerektorem	vicerektor	k1gMnSc7
a	a	k8xC
rektorem	rektor	k1gMnSc7
Generálního	generální	k2eAgInSc2d1
semináře	seminář	k1gInSc2
v	v	k7c6
Hradisku	hradisko	k1gNnSc6
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
(	(	kIx(
<g/>
1787	#num#	k4
<g/>
–	–	k?
<g/>
1790	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bývalý	bývalý	k2eAgInSc1d1
klášter	klášter	k1gInSc1
Hradisko	hradisko	k1gNnSc1
u	u	k7c2
Olomouce	Olomouc	k1gFnSc2
<g/>
,	,	kIx,
tehdejší	tehdejší	k2eAgInSc1d1
generální	generální	k2eAgInSc1d1
seminář	seminář	k1gInSc1
</s>
<s>
S	s	k7c7
doporučením	doporučení	k1gNnSc7
od	od	k7c2
rektora	rektor	k1gMnSc2
Hurdálka	Hurdálek	k1gMnSc2
se	se	k3xPyFc4
Dobrovský	Dobrovský	k1gMnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
výchovná	výchovný	k2eAgFnSc1d1
mise	mise	k1gFnSc1
u	u	k7c2
Nosticů	Nostice	k1gMnPc2
se	se	k3xPyFc4
již	již	k6eAd1
chýlila	chýlit	k5eAaImAgFnS
ke	k	k7c3
konci	konec	k1gInSc3
<g/>
,	,	kIx,
pokusil	pokusit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1786	#num#	k4
uspět	uspět	k5eAaPmF
ve	v	k7c6
výběrovém	výběrový	k2eAgNnSc6d1
řízení	řízení	k1gNnSc6
na	na	k7c4
místo	místo	k1gNnSc4
subrektora	subrektor	k1gMnSc2
v	v	k7c6
pražském	pražský	k2eAgInSc6d1
generálním	generální	k2eAgInSc6d1
semináři	seminář	k1gInSc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byla	být	k5eAaImAgFnS
instituce	instituce	k1gFnSc1
pro	pro	k7c4
výchovu	výchova	k1gFnSc4
a	a	k8xC
vzdělávání	vzdělávání	k1gNnSc4
katolických	katolický	k2eAgMnPc2d1
duchovních	duchovní	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
byl	být	k5eAaImAgInS
však	však	k9
pouze	pouze	k6eAd1
diakonem	diakon	k1gMnSc7
<g/>
,	,	kIx,
takže	takže	k8xS
uspěl	uspět	k5eAaPmAgMnS
jiný	jiný	k2eAgMnSc1d1
kandidát	kandidát	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počátku	počátek	k1gInSc6
roku	rok	k1gInSc2
1787	#num#	k4
ho	on	k3xPp3gNnSc4
informoval	informovat	k5eAaBmAgMnS
dvorní	dvorní	k2eAgMnSc1d1
rada	rada	k1gMnSc1
a	a	k8xC
ředitel	ředitel	k1gMnSc1
teologických	teologický	k2eAgFnPc2d1
studií	studie	k1gFnPc2
Augustin	Augustin	k1gMnSc1
Zippe	Zippe	k1gMnSc1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
uvolnilo	uvolnit	k5eAaPmAgNnS
místo	místo	k1gNnSc1
vicerektora	vicerektor	k1gMnSc2
v	v	k7c6
olomouckém	olomoucký	k2eAgInSc6d1
generálním	generální	k2eAgInSc6d1
semináři	seminář	k1gInSc6
<g/>
,	,	kIx,
na	na	k7c6
které	který	k3yQgFnSc6,k3yRgFnSc6,k3yIgFnSc6
byl	být	k5eAaImAgMnS
Dobrovský	Dobrovský	k1gMnSc1
po	po	k7c6
svém	svůj	k3xOyFgInSc6
vysvěcení	vysvěcení	k1gNnSc6
přijat	přijmout	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpočátku	zpočátku	k6eAd1
nepřestával	přestávat	k5eNaImAgInS
myslet	myslet	k5eAaImF
na	na	k7c4
Prahu	Praha	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
později	pozdě	k6eAd2
si	se	k3xPyFc3
v	v	k7c6
semináři	seminář	k1gInSc6
našel	najít	k5eAaPmAgMnS
nové	nový	k2eAgMnPc4d1
přátele	přítel	k1gMnPc4
a	a	k8xC
až	až	k9
do	do	k7c2
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
zrušení	zrušení	k1gNnSc1
po	po	k7c6
smrti	smrt	k1gFnSc6
Josefa	Josef	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
v	v	k7c6
roce	rok	k1gInSc6
1790	#num#	k4
se	se	k3xPyFc4
zde	zde	k6eAd1
svědomitě	svědomitě	k6eAd1
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
pedagogické	pedagogický	k2eAgFnSc3d1
práci	práce	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svědectví	svědectví	k1gNnPc1
o	o	k7c6
tomto	tento	k3xDgInSc6
období	období	k1gNnSc1
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
života	život	k1gInSc2
lze	lze	k6eAd1
nalézt	nalézt	k5eAaBmF,k5eAaPmF
v	v	k7c6
některých	některý	k3yIgFnPc6
z	z	k7c2
jeho	jeho	k3xOp3gInPc2
spisů	spis	k1gInPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
vznikly	vzniknout	k5eAaPmAgInP
během	během	k7c2
působení	působení	k1gNnSc2
v	v	k7c6
tomto	tento	k3xDgInSc6
semináři	seminář	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejvýznamnější	významný	k2eAgNnSc1d3
Dobrovského	Dobrovského	k2eAgNnSc7d1
dílem	dílo	k1gNnSc7
z	z	k7c2
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
je	být	k5eAaImIp3nS
studie	studie	k1gFnSc1
Geschichte	Geschicht	k1gInSc5
der	drát	k5eAaImRp2nS
böhmischen	böhmischno	k1gNnPc2
Pikarden	Pikardno	k1gNnPc2
und	und	k?
Adamiten	Adamitno	k1gNnPc2
(	(	kIx(
<g/>
Dějiny	dějiny	k1gFnPc1
českých	český	k2eAgMnPc2d1
pikartů	pikart	k1gMnPc2
a	a	k8xC
adamitů	adamita	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
sice	sice	k8xC
Husa	Hus	k1gMnSc2
a	a	k8xC
Jeronýma	Jeroným	k1gMnSc2
nazval	nazvat	k5eAaBmAgMnS,k5eAaPmAgMnS
mučedníky	mučedník	k1gMnPc4
<g/>
,	,	kIx,
rozhodně	rozhodně	k6eAd1
však	však	k9
odsoudil	odsoudit	k5eAaPmAgInS
náboženské	náboženský	k2eAgNnSc4d1
násilí	násilí	k1gNnSc4
všech	všecek	k3xTgFnPc2
zúčastněných	zúčastněný	k2eAgFnPc2d1
stran	strana	k1gFnPc2
a	a	k8xC
zdůraznil	zdůraznit	k5eAaPmAgMnS
nesmyslnost	nesmyslnost	k1gFnSc4
jejich	jejich	k3xOp3gNnSc2
počínání	počínání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zcela	zcela	k6eAd1
jinou	jiný	k2eAgFnSc4d1
povahu	povaha	k1gFnSc4
mají	mít	k5eAaImIp3nP
další	další	k2eAgInPc1d1
texty	text	k1gInPc1
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
řeší	řešit	k5eAaImIp3nP
náboženské	náboženský	k2eAgFnPc4d1
otázky	otázka	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
vznikaly	vznikat	k5eAaImAgFnP
přímo	přímo	k6eAd1
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
Dobrovského	Dobrovského	k2eAgFnSc7d1
úřední	úřední	k2eAgFnSc7d1
funkcí	funkce	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
Vorlesungen	Vorlesungen	k1gInSc1
über	über	k1gInSc1
das	das	k?
Praktischen	Praktischen	k1gInSc1
in	in	k?
der	drát	k5eAaImRp2nS
christlichen	christlichen	k1gInSc4
Religion	religion	k1gInSc1
(	(	kIx(
<g/>
Přednášky	přednáška	k1gFnPc1
o	o	k7c6
praktické	praktický	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
v	v	k7c6
křesťanském	křesťanský	k2eAgNnSc6d1
náboženství	náboženství	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
fragment	fragment	k1gInSc4
proslovu	proslov	k1gInSc2
Über	Über	k1gInSc4
den	den	k1gInSc1
Beruf	Beruf	k1gInSc1
(	(	kIx(
<g/>
O	o	k7c6
povolání	povolání	k1gNnSc6
<g/>
)	)	kIx)
a	a	k8xC
projev	projev	k1gInSc1
známý	známý	k2eAgInSc1d1
v	v	k7c6
českém	český	k2eAgInSc6d1
překladu	překlad	k1gInSc6
jako	jako	k9
Proslov	proslov	k1gInSc4
k	k	k7c3
odcházejícím	odcházející	k2eAgMnPc3d1
seminaristům	seminarista	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
jsou	být	k5eAaImIp3nP
vyjádřeny	vyjádřen	k2eAgInPc1d1
některé	některý	k3yIgInPc1
Dobrovského	Dobrovského	k2eAgInPc1d1
postoje	postoj	k1gInPc1
ke	k	k7c3
křesťanství	křesťanství	k1gNnSc3
<g/>
,	,	kIx,
katolicismu	katolicismus	k1gInSc3
<g/>
,	,	kIx,
kněžství	kněžství	k1gNnSc3
a	a	k8xC
celibátu	celibát	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Text	text	k1gInSc1
přednášek	přednáška	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
v	v	k7c6
českém	český	k2eAgInSc6d1
překladu	překlad	k1gInSc6
<g/>
,	,	kIx,
později	pozdě	k6eAd2
vyšel	vyjít	k5eAaPmAgInS
pod	pod	k7c7
názvem	název	k1gInSc7
Z	z	k7c2
náboženského	náboženský	k2eAgInSc2d1
odkazu	odkaz	k1gInSc2
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
značné	značný	k2eAgFnSc2d1
míry	míra	k1gFnSc2
však	však	k9
parafrázuje	parafrázovat	k5eAaBmIp3nS
Zippeho	Zippeho	k2eAgInPc4d1
spisy	spis	k1gInPc4
na	na	k7c4
stejné	stejný	k2eAgNnSc4d1
téma	téma	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Exkurz	exkurz	k1gInSc1
<g/>
:	:	kIx,
Dobrovský	Dobrovský	k1gMnSc1
a	a	k8xC
josefinismus	josefinismus	k1gInSc1
</s>
<s>
Pamětní	pamětní	k2eAgFnSc1d1
deska	deska	k1gFnSc1
Josefa	Josef	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
v	v	k7c6
Oblastním	oblastní	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
v	v	k7c6
Děčíně	Děčín	k1gInSc6
</s>
<s>
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
byl	být	k5eAaImAgMnS
bezesporu	bezesporu	k9
přesvědčeným	přesvědčený	k2eAgMnSc7d1
zastáncem	zastánce	k1gMnSc7
reforem	reforma	k1gFnPc2
Josefa	Josef	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
neboť	neboť	k8xC
v	v	k7c6
desetiletí	desetiletí	k1gNnSc6
přerodu	přerod	k1gInSc2
sloužil	sloužit	k5eAaImAgMnS
státu	stát	k1gInSc2
<g/>
,	,	kIx,
byť	byť	k8xS
krátce	krátce	k6eAd1
<g/>
,	,	kIx,
na	na	k7c6
tom	ten	k3xDgNnSc6
nejcitlivějším	citlivý	k2eAgNnSc6d3
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
totiž	totiž	k9
při	při	k7c6
výchově	výchova	k1gFnSc6
kněží	kněz	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
za	za	k7c2
života	život	k1gInSc2
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1780	#num#	k4
byl	být	k5eAaImAgInS
sice	sice	k8xC
na	na	k7c6
základě	základ	k1gInSc6
udání	udání	k1gNnSc2
předvolán	předvolat	k5eAaPmNgMnS
ke	k	k7c3
studijní	studijní	k2eAgFnSc3d1
a	a	k8xC
censurní	censurní	k2eAgFnSc3d1
komisi	komise	k1gFnSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
musel	muset	k5eAaImAgMnS
hájit	hájit	k5eAaImF
svůj	svůj	k3xOyFgInSc4
text	text	k1gInSc4
publikovaný	publikovaný	k2eAgInSc4d1
v	v	k7c6
časopise	časopis	k1gInSc6
Böhmische	Böhmisch	k1gFnSc2
Litteratur	Litteratura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
něm	on	k3xPp3gInSc6
obvinil	obvinit	k5eAaPmAgMnS
z	z	k7c2
hlouposti	hloupost	k1gFnSc2
profesora	profesor	k1gMnSc2
Voldřicha	Voldřich	k1gMnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
není	být	k5eNaImIp3nS
třeba	třeba	k6eAd1
se	se	k3xPyFc4
učit	učit	k5eAaImF
orientálním	orientální	k2eAgInPc3d1
jazykům	jazyk	k1gInPc3
<g/>
,	,	kIx,
protože	protože	k8xS
všechno	všechen	k3xTgNnSc1
je	být	k5eAaImIp3nS
už	už	k6eAd1
přeloženo	přeložen	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
aféra	aféra	k1gFnSc1
jej	on	k3xPp3gMnSc4
ale	ale	k9
neodradila	odradit	k5eNaPmAgFnS
od	od	k7c2
další	další	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
a	a	k8xC
snahy	snaha	k1gFnSc2
osobně	osobně	k6eAd1
se	se	k3xPyFc4
podílet	podílet	k5eAaImF
na	na	k7c6
reformním	reformní	k2eAgInSc6d1
kurzu	kurz	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1781	#num#	k4
vydal	vydat	k5eAaPmAgMnS
Dobrovský	Dobrovský	k1gMnSc1
anonymně	anonymně	k6eAd1
brožuru	brožura	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
vyjádřil	vyjádřit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
názor	názor	k1gInSc4
na	na	k7c4
polní	polní	k2eAgNnSc4d1
hospodaření	hospodaření	k1gNnSc4
kněží	kněz	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
zde	zde	k6eAd1
říká	říkat	k5eAaImIp3nS
<g/>
:	:	kIx,
„	„	k?
<g/>
Nechte	nechat	k5eAaPmRp2nP
být	být	k5eAaImF
venkovské	venkovský	k2eAgNnSc4d1
faráře	farář	k1gMnSc2
lidmi	člověk	k1gMnPc7
<g/>
,	,	kIx,
neboť	neboť	k8xC
ve	v	k7c6
smrtelné	smrtelný	k2eAgFnSc6d1
schránce	schránka	k1gFnSc6
na	na	k7c4
stupeň	stupeň	k1gInSc4
serafínů	serafín	k1gMnPc2
nikdy	nikdy	k6eAd1
nevystoupí	vystoupit	k5eNaPmIp3nS
<g/>
…	…	k?
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
době	doba	k1gFnSc6
jmenování	jmenování	k1gNnSc2
vicerektorem	vicerektor	k1gMnSc7
Generálního	generální	k2eAgInSc2d1
semináře	seminář	k1gInSc2
Dobrovského	Dobrovský	k1gMnSc2
jistě	jistě	k6eAd1
muselo	muset	k5eAaImAgNnS
těšit	těšit	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
angažovaný	angažovaný	k2eAgInSc1d1
postoj	postoj	k1gInSc1
ve	v	k7c6
věcech	věc	k1gFnPc6
reformy	reforma	k1gFnSc2
se	se	k3xPyFc4
začíná	začínat	k5eAaImIp3nS
vyplácet	vyplácet	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konečně	konečně	k6eAd1
již	již	k6eAd1
předtím	předtím	k6eAd1
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1786	#num#	k4
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
pěti	pět	k4xCc2
pověřených	pověřený	k2eAgMnPc2d1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
měli	mít	k5eAaImAgMnP
na	na	k7c6
starosti	starost	k1gFnSc6
cenzuru	cenzura	k1gFnSc4
náboženské	náboženský	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
v	v	k7c6
josefínském	josefínský	k2eAgMnSc6d1
duchu	duch	k1gMnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Exkurz	exkurz	k1gInSc1
<g/>
:	:	kIx,
Duševní	duševní	k2eAgFnSc1d1
nemoc	nemoc	k1gFnSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
</s>
<s>
Rajský	rajský	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
v	v	k7c6
areálu	areál	k1gInSc6
Anežského	anežský	k2eAgInSc2d1
kláštera	klášter	k1gInSc2
a	a	k8xC
nemocnice	nemocnice	k1gFnSc2
Milosrdných	milosrdný	k2eAgMnPc2d1
bratří	bratr	k1gMnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
Na	na	k7c6
Františku	František	k1gMnSc6
</s>
<s>
Dobrovského	Dobrovského	k2eAgFnSc1d1
šifra	šifra	k1gFnSc1
z	z	k7c2
dopisu	dopis	k1gInSc2
slovinskému	slovinský	k2eAgMnSc3d1
jazykovědci	jazykovědec	k1gMnSc3
Jerneji	Jernej	k1gMnSc3
Kopitarovi	Kopitar	k1gMnSc3
z	z	k7c2
roku	rok	k1gInSc2
1824	#num#	k4
</s>
<s>
Na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
1787	#num#	k4
si	se	k3xPyFc3
někteří	některý	k3yIgMnPc1
přátelé	přítel	k1gMnPc1
povšimli	povšimnout	k5eAaPmAgMnP
duševních	duševní	k2eAgFnPc2d1
nápadností	nápadnost	k1gFnPc2
tehdy	tehdy	k6eAd1
čtyřiatřicetiletého	čtyřiatřicetiletý	k2eAgMnSc2d1
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
František	František	k1gMnSc1
Martin	Martin	k1gMnSc1
Pelcl	Pelcl	k1gMnSc1
se	se	k3xPyFc4
tehdy	tehdy	k6eAd1
zmínil	zmínit	k5eAaPmAgMnS
o	o	k7c4
jeho	jeho	k3xOp3gFnPc4
„	„	k?
<g/>
podivné	podivný	k2eAgFnPc4d1
zmatenosti	zmatenost	k1gFnPc4
<g/>
“	“	k?
(	(	kIx(
<g/>
„	„	k?
<g/>
ganz	ganz	k1gInSc1
verwirrt	verwirrt	k1gInSc1
im	im	k?
Kopfe	Kopf	k1gInSc5
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1794	#num#	k4
<g/>
,	,	kIx,
tedy	tedy	k9
do	do	k7c2
věku	věk	k1gInSc6
čtyřiceti	čtyřicet	k4xCc2
let	léto	k1gNnPc2
<g/>
,	,	kIx,
ho	on	k3xPp3gNnSc4
však	však	k9
jeho	jeho	k3xOp3gNnSc1
onemocnění	onemocnění	k1gNnSc1
nijak	nijak	k6eAd1
významně	významně	k6eAd1
nepoznamenalo	poznamenat	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potom	potom	k6eAd1
již	již	k6eAd1
byl	být	k5eAaImAgInS
sužován	sužovat	k5eAaImNgInS
pravidelnými	pravidelný	k2eAgFnPc7d1
atakami	ataka	k1gFnPc7
své	svůj	k3xOyFgFnSc2
choroby	choroba	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
sám	sám	k3xTgMnSc1
nazýval	nazývat	k5eAaImAgInS
hypochondrickým	hypochondrický	k2eAgNnSc7d1
zlem	zlo	k1gNnSc7
(	(	kIx(
<g/>
„	„	k?
<g/>
hypochondrisches	hypochondrisches	k1gMnSc1
Übel	Übel	k1gMnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s	s	k7c7
častou	častý	k2eAgFnSc7d1
mentální	mentální	k2eAgFnSc7d1
a	a	k8xC
duševní	duševní	k2eAgFnSc7d1
indispozicí	indispozice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
těchto	tento	k3xDgFnPc6
periodách	perioda	k1gFnPc6
propadal	propadat	k5eAaPmAgMnS,k5eAaImAgMnS
buď	buď	k8xC
těžkým	těžký	k2eAgFnPc3d1
depresím	deprese	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
se	se	k3xPyFc4
pokoušel	pokoušet	k5eAaImAgMnS
léčit	léčit	k5eAaImF
silou	síla	k1gFnSc7
vůle	vůle	k1gFnSc2
a	a	k8xC
fyzickou	fyzický	k2eAgFnSc7d1
prací	práce	k1gFnSc7
<g/>
,	,	kIx,
nebo	nebo	k8xC
manickým	manický	k2eAgInPc3d1
stavům	stav	k1gInPc3
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
v	v	k7c6
několika	několik	k4yIc6
případech	případ	k1gInPc6
probíhaly	probíhat	k5eAaImAgFnP
tak	tak	k6eAd1
bouřlivě	bouřlivě	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
svými	svůj	k3xOyFgFnPc7
nápadnostmi	nápadnost	k1gFnPc7
v	v	k7c6
chování	chování	k1gNnSc6
upoutal	upoutat	k5eAaPmAgInS
pozornost	pozornost	k1gFnSc4
všech	všecek	k3xTgMnPc2
svých	svůj	k3xOyFgMnPc2
bližních	bližní	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
těchto	tento	k3xDgFnPc6
manických	manický	k2eAgFnPc6d1
epizodách	epizoda	k1gFnPc6
se	se	k3xPyFc4
opájel	opájet	k5eAaImAgInS
megalomanskými	megalomanský	k2eAgFnPc7d1
fantaziemi	fantazie	k1gFnPc7
a	a	k8xC
projevoval	projevovat	k5eAaImAgInS
svérázné	svérázný	k2eAgInPc4d1
mystické	mystický	k2eAgInPc4d1
<g/>
,	,	kIx,
prorocky-mesianistické	prorocky-mesianistický	k2eAgInPc4d1
a	a	k8xC
léčitelské	léčitelský	k2eAgInPc4d1
sklony	sklon	k1gInPc4
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
svou	svůj	k3xOyFgFnSc4
obvykle	obvykle	k6eAd1
vlažnou	vlažný	k2eAgFnSc4d1
religiozitu	religiozita	k1gFnSc4
stupňoval	stupňovat	k5eAaImAgInS
k	k	k7c3
vřelé	vřelý	k2eAgFnSc3d1
zbožnosti	zbožnost	k1gFnSc3
s	s	k7c7
vírou	víra	k1gFnSc7
v	v	k7c4
prozřetelnost	prozřetelnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokládají	dokládat	k5eAaImIp3nP
to	ten	k3xDgNnSc1
četné	četný	k2eAgInPc1d1
písemné	písemný	k2eAgInPc1d1
prameny	pramen	k1gInPc1
a	a	k8xC
dopisy	dopis	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
i	i	k9
v	v	k7c6
těchto	tento	k3xDgNnPc6
obdobích	období	k1gNnPc6
Dobrovský	Dobrovský	k1gMnSc1
posílal	posílat	k5eAaImAgMnS
svým	svůj	k3xOyFgMnPc3
přátelům	přítel	k1gMnPc3
z	z	k7c2
vědeckých	vědecký	k2eAgInPc2d1
kruhů	kruh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
těmto	tento	k3xDgInPc3
listům	list	k1gInPc3
připojoval	připojovat	k5eAaImAgMnS
nejrůznější	různý	k2eAgInPc4d3
tajné	tajný	k2eAgInPc4d1
symboly	symbol	k1gInPc4
<g/>
,	,	kIx,
např.	např.	kA
slovinskému	slovinský	k2eAgInSc3d1
jazykovědci	jazykovědec	k1gMnSc6
Jerneji	Jernej	k1gMnSc6
Kopitarovi	Kopitar	k1gMnSc6
poslal	poslat	k5eAaPmAgMnS
v	v	k7c6
dopise	dopis	k1gInSc6
z	z	k7c2
roku	rok	k1gInSc2
1824	#num#	k4
šifru	šifra	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
tři	tři	k4xCgInPc4
„	„	k?
<g/>
V	v	k7c6
<g/>
“	“	k?
znamenají	znamenat	k5eAaImIp3nP
latinské	latinský	k2eAgInPc4d1
„	„	k?
<g/>
via	via	k7c4
<g/>
,	,	kIx,
veritas	veritas	k1gInSc1
<g/>
,	,	kIx,
vita	vit	k2eAgFnSc1d1
<g/>
“	“	k?
<g/>
,	,	kIx,
prostřední	prostřednět	k5eAaImIp3nS
tři	tři	k4xCgNnPc4
písmena	písmeno	k1gNnPc4
znamenají	znamenat	k5eAaImIp3nP
české	český	k2eAgInPc1d1
„	„	k?
<g/>
cesta	cesta	k1gFnSc1
<g/>
,	,	kIx,
pravda	pravda	k1gFnSc1
<g/>
,	,	kIx,
život	život	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
W	W	kA
<g/>
,	,	kIx,
W	W	kA
<g/>
,	,	kIx,
L	L	kA
<g/>
“	“	k?
označuje	označovat	k5eAaImIp3nS
totéž	týž	k3xTgNnSc4
v	v	k7c6
němčině	němčina	k1gFnSc6
<g/>
:	:	kIx,
„	„	k?
<g/>
Weg	Weg	k1gMnSc1
<g/>
,	,	kIx,
Wahrheit	Wahrheit	k1gMnSc1
<g/>
,	,	kIx,
Leben	Leben	k2eAgMnSc1d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
slova	slovo	k1gNnPc4
ze	z	k7c2
šestého	šestý	k4xOgInSc2
verše	verš	k1gInSc2
čtrnácté	čtrnáctý	k4xOgFnSc2
kapitoly	kapitola	k1gFnSc2
Janova	Janův	k2eAgNnSc2d1
evangelia	evangelium	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Dobrovského	Dobrovského	k2eAgFnSc1d1
nápadná	nápadný	k2eAgFnSc1d1
vášeň	vášeň	k1gFnSc1
pro	pro	k7c4
modrou	modrý	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
(	(	kIx(
<g/>
kromě	kromě	k7c2
veškerého	veškerý	k3xTgNnSc2
oblečení	oblečení	k1gNnSc2
měl	mít	k5eAaImAgInS
údajně	údajně	k6eAd1
v	v	k7c6
modré	modrý	k2eAgFnSc6d1
barvě	barva	k1gFnSc6
svázanou	svázaný	k2eAgFnSc4d1
i	i	k8xC
Bibli	bible	k1gFnSc4
<g/>
)	)	kIx)
přispěla	přispět	k5eAaPmAgFnS
k	k	k7c3
jeho	jeho	k3xOp3gFnSc3
pověsti	pověst	k1gFnSc3
geniálního	geniální	k2eAgMnSc2d1
podivína	podivín	k1gMnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vyjadřuje	vyjadřovat	k5eAaImIp3nS
i	i	k9
posmrtná	posmrtný	k2eAgFnSc1d1
přezdívka	přezdívka	k1gFnSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Modrý	modrý	k2eAgMnSc1d1
abbé	abbé	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
der	drát	k5eAaImRp2nS
blaue	blaue	k1gInSc4
Abbé	abbé	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1801	#num#	k4
vystupoval	vystupovat	k5eAaImAgMnS
v	v	k7c6
jedné	jeden	k4xCgFnSc6
z	z	k7c2
atak	ataka	k1gFnPc2
Dobrovský	Dobrovský	k1gMnSc1
natolik	natolik	k6eAd1
„	„	k?
<g/>
antisociálně	antisociálně	k6eAd1
<g/>
“	“	k?
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
zabývala	zabývat	k5eAaImAgFnS
policie	policie	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
byl	být	k5eAaImAgInS
proto	proto	k8xC
na	na	k7c4
podnět	podnět	k1gInSc4
Bedřicha	Bedřich	k1gMnSc2
Nostice	Nostice	k1gFnSc2
umístěn	umístit	k5eAaPmNgMnS
do	do	k7c2
nemocnice	nemocnice	k1gFnSc2
u	u	k7c2
Milosrdných	milosrdný	k2eAgMnPc2d1
bratří	bratr	k1gMnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
o	o	k7c4
něj	on	k3xPp3gMnSc4
pečoval	pečovat	k5eAaImAgMnS
lékař	lékař	k1gMnSc1
Jan	Jan	k1gMnSc1
Theobald	Theobald	k1gMnSc1
Held	Held	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jindy	jindy	k6eAd1
pobýval	pobývat	k5eAaImAgMnS
na	na	k7c6
venkovských	venkovský	k2eAgNnPc6d1
šlechtických	šlechtický	k2eAgNnPc6d1
sídlech	sídlo	k1gNnPc6
Měšice	Měšice	k1gFnSc1
<g/>
,	,	kIx,
Zásmuky	Zásmuky	k1gInPc1
či	či	k8xC
Chudenice	Chudenice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
On	on	k3xPp3gMnSc1
sám	sám	k3xTgInSc1
své	svůj	k3xOyFgInPc4
útrapy	útrap	k1gInPc4
přisuzoval	přisuzovat	k5eAaImAgMnS
onomu	onen	k3xDgInSc3
nešťastnému	šťastný	k2eNgInSc3d1
zástřelu	zástřel	k1gInSc3
z	z	k7c2
lovu	lov	k1gInSc2
v	v	k7c6
jindřichovické	jindřichovický	k2eAgFnSc6d1
oboře	obora	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobrovského	Dobrovského	k2eAgFnSc1d1
duševní	duševní	k2eAgFnSc1d1
nemoc	nemoc	k1gFnSc1
na	na	k7c6
základě	základ	k1gInSc6
doložených	doložený	k2eAgInPc2d1
symptomů	symptom	k1gInPc2
diagnostikoval	diagnostikovat	k5eAaBmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
psychiatr	psychiatr	k1gMnSc1
a	a	k8xC
neurolog	neurolog	k1gMnSc1
Zdeněk	Zdeněk	k1gMnSc1
Mysliveček	Mysliveček	k1gMnSc1
jako	jako	k8xC,k8xS
manickomelancholickou	manickomelancholický	k2eAgFnSc7d1
psychózou	psychóza	k1gFnSc7
v	v	k7c6
Kraepelinově	Kraepelinově	k1gFnSc6
pojetí	pojetí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
novější	nový	k2eAgFnSc2d2
terminologie	terminologie	k1gFnSc2
tedy	tedy	k8xC
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
trpěl	trpět	k5eAaImAgMnS
bipolární	bipolární	k2eAgFnSc7d1
afektivní	afektivní	k2eAgFnSc7d1
poruchou	porucha	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Návrat	návrat	k1gInSc1
k	k	k7c3
Nosticům	Nostic	k1gMnPc3
(	(	kIx(
<g/>
1790	#num#	k4
<g/>
–	–	k?
<g/>
1803	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zástupci	zástupce	k1gMnPc1
Královské	královský	k2eAgFnSc2d1
české	český	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
nauk	nauka	k1gFnPc2
před	před	k7c7
císařem	císař	k1gMnSc7
Leopoldem	Leopold	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
v	v	k7c6
roce	rok	k1gInSc6
1791	#num#	k4
<g/>
;	;	kIx,
čb	čb	k?
reprodukce	reprodukce	k1gFnSc1
nástěnné	nástěnný	k2eAgFnPc1d1
malby	malba	k1gFnPc1
v	v	k7c6
letohrádku	letohrádek	k1gInSc6
královny	královna	k1gFnSc2
Anny	Anna	k1gFnSc2
na	na	k7c6
Pražském	pražský	k2eAgInSc6d1
hradě	hrad	k1gInSc6
z	z	k7c2
cyklu	cyklus	k1gInSc2
podle	podle	k7c2
návrhu	návrh	k1gInSc2
Christiana	Christian	k1gMnSc4
Rubena	ruben	k2eAgMnSc4d1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
v	v	k7c6
letech	let	k1gInPc6
1851	#num#	k4
<g/>
–	–	k?
<g/>
1865	#num#	k4
namalovali	namalovat	k5eAaPmAgMnP
Antonín	Antonín	k1gMnSc1
Lhota	Lhota	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Matyáš	Matyáš	k1gMnSc1
Trenkwald	Trenkwald	k1gMnSc1
<g/>
,	,	kIx,
Emil	Emil	k1gMnSc1
Johann	Johann	k1gMnSc1
Lauffer	Lauffer	k1gMnSc1
a	a	k8xC
Jan	Jan	k1gMnSc1
Till	Till	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
zrušení	zrušení	k1gNnSc6
olomouckého	olomoucký	k2eAgInSc2d1
semináře	seminář	k1gInSc2
obdržel	obdržet	k5eAaPmAgMnS
Dobrovský	Dobrovský	k1gMnSc1
výslužné	výslužná	k1gFnSc2
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
500	#num#	k4
zlatých	zlatý	k2eAgFnPc2d1
a	a	k8xC
tzv.	tzv.	kA
expektační	expektační	k2eAgInSc1d1
list	list	k1gInSc1
<g/>
,	,	kIx,
tj.	tj.	kA
ujištění	ujištění	k1gNnSc1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
hlásit	hlásit	k5eAaImF
o	o	k7c4
takový	takový	k3xDgInSc4
úřad	úřad	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
by	by	kYmCp3nS
odpovídal	odpovídat	k5eAaImAgInS
jeho	jeho	k3xOp3gInPc3
příjmům	příjem	k1gInPc3
v	v	k7c6
semináři	seminář	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
se	se	k3xPyFc4
obrátil	obrátit	k5eAaPmAgMnS
na	na	k7c4
svého	svůj	k3xOyFgMnSc4
žáka	žák	k1gMnSc4
Bedřicha	Bedřich	k1gMnSc4
Nostice	Nostika	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
souhlasil	souhlasit	k5eAaImAgMnS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
učenci	učenec	k1gMnPc7
poskytne	poskytnout	k5eAaPmIp3nS
ubytování	ubytování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
nástupu	nástup	k1gInSc6
císaře	císař	k1gMnSc2
Leopolda	Leopold	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
se	se	k3xPyFc4
politické	politický	k2eAgInPc1d1
poměry	poměr	k1gInPc1
změnily	změnit	k5eAaPmAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Císař	Císař	k1gMnSc1
sice	sice	k8xC
svolal	svolat	k5eAaPmAgInS
český	český	k2eAgInSc1d1
sněm	sněm	k1gInSc1
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
ale	ale	k8xC
odstranil	odstranit	k5eAaPmAgMnS
některé	některý	k3yIgInPc4
dříve	dříve	k6eAd2
povolené	povolený	k2eAgInPc4d1
politické	politický	k2eAgInPc4d1
i	i	k9
náboženské	náboženský	k2eAgFnSc2d1
svobody	svoboda	k1gFnSc2
<g/>
,	,	kIx,
zakázal	zakázat	k5eAaPmAgInS
zednářství	zednářství	k1gNnSc4
a	a	k8xC
zřídil	zřídit	k5eAaPmAgInS
tajnou	tajný	k2eAgFnSc4d1
policii	policie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přes	přes	k7c4
své	svůj	k3xOyFgFnPc4
zdravotní	zdravotní	k2eAgFnPc4d1
obtíže	obtíž	k1gFnPc4
se	se	k3xPyFc4
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
Dobrovský	Dobrovský	k1gMnSc1
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
práci	práce	k1gFnSc4
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
díle	dílo	k1gNnSc6
Geschichte	Geschicht	k1gInSc5
der	drát	k5eAaImRp2nS
Böhmischen	Böhmischen	k2eAgMnSc1d1
Sprache	Sprache	k1gFnSc7
und	und	k?
Litteratur	Litteratura	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
25	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1791	#num#	k4
se	se	k3xPyFc4
za	za	k7c2
účasti	účast	k1gFnSc2
Leopolda	Leopold	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
konalo	konat	k5eAaImAgNnS
zasedání	zasedání	k1gNnSc1
Královské	královský	k2eAgFnSc2d1
české	český	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
nauk	nauka	k1gFnPc2
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
na	na	k7c6
němž	jenž	k3xRgInSc6
Dobrovský	Dobrovský	k1gMnSc1
pronesl	pronést	k5eAaPmAgMnS
důmyslně	důmyslně	k6eAd1
komponovanou	komponovaný	k2eAgFnSc4d1
řeč	řeč	k1gFnSc4
Ueber	Uebra	k1gFnPc2
die	die	k?
Ergebenheit	Ergebenheit	k1gMnSc1
und	und	k?
Anhänglichkeit	Anhänglichkeit	k1gMnSc1
der	drát	k5eAaImRp2nS
Slawischen	Slawischno	k1gNnPc2
Völker	Völkero	k1gNnPc2
an	an	k?
das	das	k?
Erzhaus	Erzhaus	k1gMnSc1
Oestreich	Oestreich	k1gMnSc1
(	(	kIx(
<g/>
O	o	k7c6
stálé	stálý	k2eAgFnSc6d1
věrnosti	věrnost	k1gFnSc6
<g/>
,	,	kIx,
kterouž	kterouž	k?
se	se	k3xPyFc4
národ	národ	k1gInSc1
slovanský	slovanský	k2eAgInSc1d1
domu	dům	k1gInSc2
rakouského	rakouský	k2eAgMnSc2d1
po	po	k7c4
všechen	všechen	k3xTgInSc4
čas	čas	k1gInSc4
přidržel	přidržet	k5eAaPmAgMnS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
znamenala	znamenat	k5eAaImAgFnS
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
i	i	k9
pro	pro	k7c4
Společnost	společnost	k1gFnSc4
významný	významný	k2eAgInSc4d1
zlom	zlom	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Formálně	formálně	k6eAd1
loajální	loajální	k2eAgInSc4d1
projev	projev	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
však	však	k9
obsahuje	obsahovat	k5eAaImIp3nS
také	také	k9
určité	určitý	k2eAgNnSc1d1
poučení	poučení	k1gNnSc1
o	o	k7c4
skutečné	skutečný	k2eAgFnPc4d1
státnické	státnický	k2eAgFnPc4d1
moudrosti	moudrost	k1gFnPc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
v	v	k7c6
českém	český	k2eAgInSc6d1
překladu	překlad	k1gInSc6
Karla	Karel	k1gMnSc2
Ignáce	Ignác	k1gMnSc2
Tháma	Thám	k1gMnSc2
zveřejněn	zveřejnit	k5eAaPmNgInS
v	v	k7c6
Krameriových	Krameriových	k2eAgFnSc6d1
c.	c.	k?
k.	k.	k?
Vlasteneckých	vlastenecký	k2eAgFnPc6d1
novinách	novina	k1gFnPc6
<g/>
,	,	kIx,
a	a	k8xC
oslovil	oslovit	k5eAaPmAgInS
tak	tak	k6eAd1
nejen	nejen	k6eAd1
uzavřený	uzavřený	k2eAgInSc1d1
svět	svět	k1gInSc1
vědeckých	vědecký	k2eAgFnPc2d1
elit	elita	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
širší	široký	k2eAgFnSc2d2
vzdělané	vzdělaný	k2eAgFnSc2d1
vrstvy	vrstva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řeč	řeč	k1gFnSc1
zaujala	zaujmout	k5eAaPmAgFnS
také	také	k9
císaře	císař	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
Společnosti	společnost	k1gFnSc3
nauk	nauka	k1gFnPc2
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
6000	#num#	k4
zlatých	zlatá	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
části	část	k1gFnSc3
této	tento	k3xDgFnSc2
sumy	suma	k1gFnSc2
pak	pak	k6eAd1
mohl	moct	k5eAaImAgMnS
Dobrovský	Dobrovský	k1gMnSc1
uskutečnit	uskutečnit	k5eAaPmF
velkorysou	velkorysý	k2eAgFnSc4d1
studijní	studijní	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
do	do	k7c2
Švédska	Švédsko	k1gNnSc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
ještě	ještě	k6eAd1
zamířil	zamířit	k5eAaPmAgMnS
do	do	k7c2
Ruska	Rusko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Cesta	cesta	k1gFnSc1
do	do	k7c2
německých	německý	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
Švédska	Švédsko	k1gNnSc2
<g/>
,	,	kIx,
Dánska	Dánsko	k1gNnSc2
<g/>
,	,	kIx,
Ruska	Rusko	k1gNnSc2
a	a	k8xC
Polska	Polsko	k1gNnSc2
(	(	kIx(
<g/>
1792	#num#	k4
<g/>
–	–	k?
<g/>
1793	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1792	#num#	k4
se	se	k3xPyFc4
Společnost	společnost	k1gFnSc1
nauk	nauka	k1gFnPc2
rozhodla	rozhodnout	k5eAaPmAgFnS
věnovat	věnovat	k5eAaPmF,k5eAaImF
1000	#num#	k4
zlatých	zlatá	k1gFnPc2
na	na	k7c4
cestu	cesta	k1gFnSc4
Dobrovského	Dobrovský	k1gMnSc2
a	a	k8xC
hraběte	hrabě	k1gMnSc2
Jáchyma	Jáchym	k1gMnSc2
ze	z	k7c2
Šternberka	Šternberk	k1gInSc2
do	do	k7c2
Švédska	Švédsko	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
měli	mít	k5eAaImAgMnP
pátrat	pátrat	k5eAaImF
po	po	k7c6
rukopisech	rukopis	k1gInPc6
a	a	k8xC
knihách	kniha	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
Švédové	Švéd	k1gMnPc1
odvezli	odvézt	k5eAaPmAgMnP
z	z	k7c2
Čech	Čechy	k1gFnPc2
a	a	k8xC
Prahy	Praha	k1gFnSc2
roku	rok	k1gInSc2
1648	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cesta	cesta	k1gFnSc1
za	za	k7c7
ztracenými	ztracený	k2eAgNnPc7d1
bohemiky	bohemikum	k1gNnPc7
vedla	vést	k5eAaImAgFnS
přes	přes	k7c4
Plzeň	Plzeň	k1gFnSc4
a	a	k8xC
Cheb	Cheb	k1gInSc4
do	do	k7c2
německých	německý	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
Dobrovský	Dobrovský	k1gMnSc1
zastavil	zastavit	k5eAaPmAgMnS
v	v	k7c6
Erfurtu	Erfurt	k1gInSc6
<g/>
,	,	kIx,
Gotě	Gotha	k1gFnSc6
a	a	k8xC
Hamburku	Hamburk	k1gInSc6
<g/>
,	,	kIx,
potom	potom	k6eAd1
již	již	k6eAd1
následoval	následovat	k5eAaImAgInS
Stockholm	Stockholm	k1gInSc1
<g/>
,	,	kIx,
Upsala	upsat	k5eAaPmAgFnS
<g/>
,	,	kIx,
Lund	Lund	k1gInSc1
a	a	k8xC
několik	několik	k4yIc1
dalších	další	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
finského	finský	k2eAgInSc2d1
přístavu	přístav	k1gInSc2
Å	Å	k1gFnSc5
pokračovali	pokračovat	k5eAaImAgMnP
do	do	k7c2
Petrohradu	Petrohrad	k1gInSc2
a	a	k8xC
dále	daleko	k6eAd2
do	do	k7c2
Moskvy	Moskva	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
Dobrovský	Dobrovský	k1gMnSc1
hledal	hledat	k5eAaImAgMnS
zejména	zejména	k9
staroslověnské	staroslověnský	k2eAgInPc4d1
literární	literární	k2eAgInPc4d1
prameny	pramen	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
třech	tři	k4xCgInPc6
měsících	měsíc	k1gInPc6
v	v	k7c6
carském	carský	k2eAgNnSc6d1
Rusku	Rusko	k1gNnSc6
odjel	odjet	k5eAaPmAgInS
ještě	ještě	k6eAd1
do	do	k7c2
Varšavy	Varšava	k1gFnSc2
a	a	k8xC
Krakova	Krakov	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vypátral	vypátrat	k5eAaPmAgInS
<g/>
,	,	kIx,
zda	zda	k8xS
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
uloženy	uložit	k5eAaPmNgInP
české	český	k2eAgInPc1d1
či	či	k8xC
staroslověnské	staroslověnský	k2eAgInPc1d1
tisky	tisk	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
návratu	návrat	k1gInSc6
do	do	k7c2
Prahy	Praha	k1gFnSc2
Dobrovský	Dobrovský	k1gMnSc1
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
profesuru	profesura	k1gFnSc4
českého	český	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
mezitím	mezitím	k6eAd1
obsadil	obsadit	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
přítel	přítel	k1gMnSc1
Pelcl	Pelcl	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
nebyl	být	k5eNaImAgInS
kompromitován	kompromitovat	k5eAaBmNgMnS
zednářstvím	zednářství	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Cesta	cesta	k1gFnSc1
do	do	k7c2
Itálie	Itálie	k1gFnSc2
(	(	kIx(
<g/>
1794	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Začátkem	začátkem	k7c2
roku	rok	k1gInSc2
1794	#num#	k4
doprovodil	doprovodit	k5eAaPmAgMnS
Dobrovský	Dobrovský	k1gMnSc1
Bedřicha	Bedřich	k1gMnSc2
Nostice	Nostice	k1gFnSc2
na	na	k7c6
cestě	cesta	k1gFnSc6
do	do	k7c2
Itálie	Itálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zastávce	zastávka	k1gFnSc6
v	v	k7c6
Karlsruhe	Karlsruhe	k1gFnSc6
a	a	k8xC
Kostnici	Kostnice	k1gFnSc6
se	se	k3xPyFc4
vydali	vydat	k5eAaPmAgMnP
přes	přes	k7c4
Tyrolsko	Tyrolsko	k1gNnSc4
do	do	k7c2
Benátek	Benátky	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
navštívili	navštívit	k5eAaPmAgMnP
Bibliotecu	Bibliotec	k2eAgFnSc4d1
Marcianu	Marciana	k1gFnSc4
a	a	k8xC
knihtiskárnu	knihtiskárna	k1gFnSc4
specializovanou	specializovaný	k2eAgFnSc4d1
na	na	k7c4
hlaholici	hlaholice	k1gFnSc4
a	a	k8xC
cyrilici	cyrilice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
pobytu	pobyt	k1gInSc2
v	v	k7c6
Římě	Řím	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
chtěl	chtít	k5eAaImAgMnS
Dobrovský	Dobrovský	k1gMnSc1
studovat	studovat	k5eAaImF
slovanské	slovanský	k2eAgInPc4d1
rukopisy	rukopis	k1gInPc4
<g/>
,	,	kIx,
však	však	k9
nakonec	nakonec	k6eAd1
sešlo	sejít	k5eAaPmAgNnS
<g/>
,	,	kIx,
protože	protože	k8xS
Nosticův	Nosticův	k2eAgMnSc1d1
otec	otec	k1gMnSc1
František	František	k1gMnSc1
Antonín	Antonín	k1gMnSc1
náhle	náhle	k6eAd1
onemocněl	onemocnět	k5eAaPmAgMnS
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
museli	muset	k5eAaImAgMnP
přes	přes	k7c4
Terst	Terst	k1gInSc4
a	a	k8xC
Lublaň	Lublaň	k1gFnSc4
vrátit	vrátit	k5eAaPmF
do	do	k7c2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
Kampě	Kampa	k1gFnSc6
(	(	kIx(
<g/>
1798	#num#	k4
<g/>
–	–	k?
<g/>
1803	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bývalá	bývalý	k2eAgFnSc1d1
koželužna	koželužna	k1gFnSc1
na	na	k7c6
Malé	Malé	k2eAgFnSc6d1
Straně	strana	k1gFnSc6
v	v	k7c6
ulici	ulice	k1gFnSc6
U	u	k7c2
Sovových	Sovových	k2eAgInPc2d1
mlýnů	mlýn	k1gInPc2
501	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
vlastnictví	vlastnictví	k1gNnSc2
rodem	rod	k1gInSc7
Nosticů	Nostice	k1gMnPc2
zde	zde	k6eAd1
žil	žít	k5eAaImAgMnS
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
v	v	k7c6
letech	let	k1gInPc6
1798	#num#	k4
<g/>
–	–	k?
<g/>
1803	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1798	#num#	k4
zakoupil	zakoupit	k5eAaPmAgMnS
Bedřich	Bedřich	k1gMnSc1
Nostic	Nostice	k1gFnPc2
pro	pro	k7c4
Dobrovského	Dobrovského	k2eAgInSc4d1
domek	domek	k1gInSc4
na	na	k7c6
ostrově	ostrov	k1gInSc6
Kampa	Kampa	k1gFnSc1
uprostřed	uprostřed	k7c2
Šenfeldské	Šenfeldský	k2eAgFnSc2d1
zahrady	zahrada	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
mohl	moct	k5eAaImAgInS
v	v	k7c6
klidu	klid	k1gInSc6
věnovat	věnovat	k5eAaImF,k5eAaPmF
své	svůj	k3xOyFgFnSc3
vědecké	vědecký	k2eAgFnSc3d1
práci	práce	k1gFnSc3
a	a	k8xC
ve	v	k7c6
volném	volný	k2eAgInSc6d1
čase	čas	k1gInSc6
pak	pak	k6eAd1
pečovat	pečovat	k5eAaImF
o	o	k7c4
zahradu	zahrada	k1gFnSc4
<g/>
,	,	kIx,
květiny	květina	k1gFnPc4
a	a	k8xC
stromy	strom	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
let	léto	k1gNnPc2
1798	#num#	k4
<g/>
–	–	k?
<g/>
1803	#num#	k4
tady	tady	k6eAd1
vznikly	vzniknout	k5eAaPmAgInP
spisy	spis	k1gInPc4
Die	Die	k1gFnSc2
Bildsamkeit	Bildsamkeita	k1gFnPc2
der	drát	k5eAaImRp2nS
slawischen	slawischen	k1gInSc4
Sprache	Sprach	k1gMnSc2
<g/>
,	,	kIx,
an	an	k?
der	drát	k5eAaImRp2nS
Bildung	Bildung	k1gInSc1
der	drát	k5eAaImRp2nS
Substantive	Substantiv	k1gInSc5
und	und	k?
Adjective	Adjectiv	k1gInSc5
in	in	k?
der	drát	k5eAaImRp2nS
böhmischen	böhmischen	k1gInSc4
Sprache	Sprach	k1gMnSc2
dargestellt	dargestellta	k1gFnPc2
(	(	kIx(
<g/>
Tvárnost	tvárnost	k1gFnSc1
slovanských	slovanský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
představená	představený	k2eAgFnSc1d1
na	na	k7c4
tvoření	tvoření	k1gNnSc4
podstatných	podstatný	k2eAgFnPc2d1
a	a	k8xC
přídavných	přídavný	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
v	v	k7c6
českém	český	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
<g/>
;	;	kIx,
1799	#num#	k4
<g/>
)	)	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
první	první	k4xOgInSc4
díl	díl	k1gInSc4
německo-českého	německo-český	k2eAgInSc2d1
slovníku	slovník	k1gInSc2
Deutsch-böhmisches	Deutsch-böhmisches	k1gMnSc1
Wörterbuch	Wörterbuch	k1gMnSc1
(	(	kIx(
<g/>
Německo-český	německo-český	k2eAgInSc1d1
slovník	slovník	k1gInSc1
<g/>
;	;	kIx,
1802	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
výsledek	výsledek	k1gInSc1
Dobrovského	Dobrovského	k2eAgNnSc2d1
studia	studio	k1gNnSc2
rostlin	rostlina	k1gFnPc2
<g/>
,	,	kIx,
jímž	jenž	k3xRgNnSc7
byl	být	k5eAaImAgInS
spis	spis	k1gInSc4
Entwurf	Entwurf	k1gInSc1
eines	eines	k1gInSc1
Pflanzensystems	Pflanzensystems	k1gInSc1
nach	nach	k1gInSc1
Zahlen	Zahlen	k2eAgInSc1d1
und	und	k?
Verhältnissen	Verhältnissen	k1gInSc1
(	(	kIx(
<g/>
Náčrt	náčrt	k1gInSc1
rostlinného	rostlinný	k2eAgInSc2d1
systému	systém	k1gInSc2
na	na	k7c6
základě	základ	k1gInSc6
čísel	číslo	k1gNnPc2
a	a	k8xC
poměrů	poměr	k1gInPc2
<g/>
;	;	kIx,
1802	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Soukromým	soukromý	k2eAgMnSc7d1
učencem	učenec	k1gMnSc7
(	(	kIx(
<g/>
1803	#num#	k4
<g/>
–	–	k?
<g/>
1829	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zámek	zámek	k1gInSc1
v	v	k7c6
Chudenicích	Chudenice	k1gFnPc6
</s>
<s>
Na	na	k7c4
rady	rada	k1gFnPc4
lékařů	lékař	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
mu	on	k3xPp3gMnSc3
doporučovali	doporučovat	k5eAaImAgMnP
pobývat	pobývat	k5eAaImF
více	hodně	k6eAd2
ve	v	k7c6
společnosti	společnost	k1gFnSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
Dobrovský	Dobrovský	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1803	#num#	k4
odstěhoval	odstěhovat	k5eAaPmAgMnS
ze	z	k7c2
samoty	samota	k1gFnSc2
na	na	k7c6
Kampě	Kampa	k1gFnSc6
a	a	k8xC
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
zimních	zimní	k2eAgInPc2d1
měsíců	měsíc	k1gInPc2
pobýval	pobývat	k5eAaImAgMnS
v	v	k7c6
lázních	lázeň	k1gFnPc6
a	a	k8xC
na	na	k7c6
venkovských	venkovský	k2eAgNnPc6d1
sídlech	sídlo	k1gNnPc6
spřátelené	spřátelený	k2eAgFnSc2d1
šlechty	šlechta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neobyčejně	obyčejně	k6eNd1
si	se	k3xPyFc3
rozuměl	rozumět	k5eAaImAgInS
např.	např.	kA
s	s	k7c7
hrabětem	hrabě	k1gMnSc7
Františkem	František	k1gMnSc7
Josefem	Josef	k1gMnSc7
ze	z	k7c2
Šternberka	Šternberk	k1gInSc2
a	a	k8xC
Manderscheidu	Manderscheid	k1gInSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
ho	on	k3xPp3gMnSc4
seznámil	seznámit	k5eAaPmAgMnS
s	s	k7c7
Goethem	Goeth	k1gInSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
s	s	k7c7
Janem	Jan	k1gMnSc7
Rudolfem	Rudolf	k1gMnSc7
Czerninem	Czernin	k1gInSc7
<g/>
,	,	kIx,
u	u	k7c2
něhož	jenž	k3xRgInSc2
v	v	k7c6
Chudenicích	Chudenice	k1gFnPc6
každý	každý	k3xTgInSc4
rok	rok	k1gInSc4
několik	několik	k4yIc4
týdnů	týden	k1gInPc2
bydlel	bydlet	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1809	#num#	k4
bylo	být	k5eAaImAgNnS
vydáno	vydán	k2eAgNnSc1d1
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
opus	opus	k1gInSc4
magnum	magnum	k1gInSc1
Ausführliches	Ausführliches	k1gInSc1
Lehrgebäude	Lehrgebäud	k1gInSc5
der	drát	k5eAaImRp2nS
Böhmischen	Böhmischen	k1gInSc4
Sprache	Sprach	k1gMnSc2
<g/>
,	,	kIx,
zur	zur	k?
gründlichen	gründlichen	k2eAgInSc1d1
Erlernung	Erlernung	k1gInSc1
derselben	derselben	k2eAgInSc1d1
für	für	k?
Deutsche	Deutsche	k1gInSc1
<g/>
,	,	kIx,
zur	zur	k?
vollkommenern	vollkommenern	k1gInSc1
Kenntniss	Kenntniss	k1gInSc1
für	für	k?
Böhmen	Böhmen	k1gInSc1
(	(	kIx(
<g/>
Zevrubná	zevrubný	k2eAgFnSc1d1
mluvnice	mluvnice	k1gFnSc1
jazyka	jazyk	k1gInSc2
českého	český	k2eAgNnSc2d1
k	k	k7c3
základnímu	základní	k2eAgNnSc3d1
poučení	poučení	k1gNnSc3
pro	pro	k7c4
Němce	Němec	k1gMnPc4
a	a	k8xC
k	k	k7c3
dokonalé	dokonalý	k2eAgFnSc3d1
znalosti	znalost	k1gFnSc3
pro	pro	k7c4
Čechy	Čechy	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
byla	být	k5eAaImAgFnS
česká	český	k2eAgFnSc1d1
gramatika	gramatika	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
níž	jenž	k3xRgFnSc6
pracoval	pracovat	k5eAaImAgMnS
již	již	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1787	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
té	ten	k3xDgFnSc6
se	se	k3xPyFc4
Dobrovský	Dobrovský	k1gMnSc1
zaměřil	zaměřit	k5eAaPmAgMnS
na	na	k7c4
vědecké	vědecký	k2eAgNnSc4d1
zpracování	zpracování	k1gNnSc4
staroslověnštiny	staroslověnština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
dozvěděl	dozvědět	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Josef	Josef	k1gMnSc1
Jungmann	Jungmann	k1gMnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
Jaroslav	Jaroslav	k1gMnSc1
Puchmajer	Puchmajer	k1gMnSc1
a	a	k8xC
Václav	Václav	k1gMnSc1
Hanka	Hanka	k1gFnSc1
pokračují	pokračovat	k5eAaImIp3nP
v	v	k7c6
jeho	jeho	k3xOp3gNnSc6
lexikografickém	lexikografický	k2eAgNnSc6d1
díle	dílo	k1gNnSc6
<g/>
,	,	kIx,
přenechal	přenechat	k5eAaPmAgMnS
jim	on	k3xPp3gMnPc3
část	část	k1gFnSc4
svých	svůj	k3xOyFgInPc2
slovníků	slovník	k1gInPc2
s	s	k7c7
rukopisnými	rukopisný	k2eAgFnPc7d1
poznámkami	poznámka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
podpory	podpora	k1gFnSc2
domácí	domácí	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
však	však	k9
nezapomínal	zapomínat	k5eNaImAgInS
ani	ani	k8xC
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
korespondenci	korespondence	k1gFnSc4
s	s	k7c7
okolním	okolní	k2eAgInSc7d1
světem	svět	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dopisoval	dopisovat	k5eAaImAgMnS
si	se	k3xPyFc3
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
se	s	k7c7
zmíněným	zmíněný	k2eAgInSc7d1
Jernejem	Jernej	k1gInSc7
Kopitarem	Kopitar	k1gMnSc7
<g/>
,	,	kIx,
Karlem	Karel	k1gMnSc7
Gottlobem	Gottlob	k1gMnSc7
Antonem	Anton	k1gMnSc7
a	a	k8xC
Jacobem	Jacob	k1gMnSc7
Grimmem	Grimm	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
podnikl	podniknout	k5eAaPmAgMnS
několik	několik	k4yIc4
kratších	krátký	k2eAgFnPc2d2
cest	cesta	k1gFnPc2
do	do	k7c2
německých	německý	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
a	a	k8xC
do	do	k7c2
Vídně	Vídeň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1818	#num#	k4
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
sporu	spor	k1gInSc2
s	s	k7c7
novou	nova	k1gFnSc7
romanticky	romanticky	k6eAd1
cítící	cítící	k2eAgFnSc7d1
generací	generace	k1gFnSc7
českých	český	k2eAgMnPc2d1
obrozenců	obrozenec	k1gMnPc2
<g/>
,	,	kIx,
když	když	k8xS
vyslovil	vyslovit	k5eAaPmAgMnS
názor	názor	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
tzv.	tzv.	kA
Rukopis	rukopis	k1gInSc1
zelenohorský	zelenohorský	k2eAgMnSc1d1
je	on	k3xPp3gNnSc4
falzum	falzum	k1gNnSc4
<g/>
,	,	kIx,
a	a	k8xC
na	na	k7c4
tuto	tento	k3xDgFnSc4
skutečnost	skutečnost	k1gFnSc4
Jungmanna	Jungmann	k1gMnSc2
několikrát	několikrát	k6eAd1
upozornil	upozornit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
českém	český	k2eAgNnSc6d1
vydání	vydání	k1gNnSc6
rukopisu	rukopis	k1gInSc2
ho	on	k3xPp3gMnSc4
v	v	k7c6
roce	rok	k1gInSc6
1824	#num#	k4
označil	označit	k5eAaPmAgMnS
za	za	k7c4
falzum	falzum	k1gNnSc4
i	i	k9
veřejně	veřejně	k6eAd1
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
začal	začít	k5eAaPmAgMnS
spor	spor	k1gInSc4
o	o	k7c4
rukopisy	rukopis	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
fakt	fakt	k1gInSc4
čeští	český	k2eAgMnPc1d1
vlastenci	vlastenec	k1gMnPc1
nebyli	být	k5eNaImAgMnP
ochotni	ochoten	k2eAgMnPc1d1
akceptovat	akceptovat	k5eAaBmF
a	a	k8xC
označili	označit	k5eAaPmAgMnP
Dobrovského	Dobrovský	k1gMnSc4
za	za	k7c2
„	„	k?
<g/>
slavizujícího	slavizující	k2eAgMnSc4d1
Němce	Němec	k1gMnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
dílo	dílo	k1gNnSc1
pak	pak	k6eAd1
čeští	český	k2eAgMnPc1d1
buditelé	buditel	k1gMnPc1
dlouho	dlouho	k6eAd1
nepovažovali	považovat	k5eNaImAgMnP
za	za	k7c4
hodné	hodný	k2eAgFnPc4d1
pozornosti	pozornost	k1gFnPc4
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
jim	on	k3xPp3gMnPc3
vytvořilo	vytvořit	k5eAaPmAgNnS
teoretické	teoretický	k2eAgInPc4d1
základy	základ	k1gInPc4
pro	pro	k7c4
veškerou	veškerý	k3xTgFnSc4
jejich	jejich	k3xOp3gFnSc1
činnost	činnost	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stáří	stáří	k1gNnSc1
a	a	k8xC
smrt	smrt	k1gFnSc1
(	(	kIx(
<g/>
1829	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dne	den	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1828	#num#	k4
Dobrovský	Dobrovský	k1gMnSc1
přicestoval	přicestovat	k5eAaPmAgMnS
z	z	k7c2
císařské	císařský	k2eAgFnSc2d1
audience	audience	k1gFnSc2
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
ke	k	k7c3
studiu	studio	k1gNnSc3
do	do	k7c2
Brna	Brno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cestou	cesta	k1gFnSc7
se	se	k3xPyFc4
nachladil	nachladit	k5eAaPmAgInS
a	a	k8xC
se	s	k7c7
zápalem	zápal	k1gInSc7
plic	plíce	k1gFnPc2
byl	být	k5eAaImAgInS
hospitalizován	hospitalizovat	k5eAaBmNgInS
v	v	k7c6
nemocnici	nemocnice	k1gFnSc6
Milosrdných	milosrdný	k2eAgMnPc2d1
bratří	bratr	k1gMnPc2
na	na	k7c6
Starém	starý	k2eAgNnSc6d1
Brně	Brno	k1gNnSc6
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
ve	v	k7c6
Štýřicích	Štýřice	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
6	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1829	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohřeb	pohřeb	k1gInSc1
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
8	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1829	#num#	k4
<g/>
,	,	kIx,
pohřební	pohřební	k2eAgInSc4d1
obřad	obřad	k1gInSc4
v	v	k7c6
klášterním	klášterní	k2eAgInSc6d1
kostele	kostel	k1gInSc6
Milosrdných	milosrdný	k2eAgMnPc2d1
bratří	bratr	k1gMnPc2
sv.	sv.	kA
Leopolda	Leopolda	k1gFnSc1
vykonal	vykonat	k5eAaPmAgMnS
opat	opat	k1gMnSc1
augustiniánů	augustinián	k1gMnPc2
Cyril	Cyril	k1gMnSc1
František	František	k1gMnSc1
Napp	Napp	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
patřil	patřit	k5eAaImAgMnS
k	k	k7c3
žákům	žák	k1gMnPc3
Dobrovského	Dobrovského	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatky	ostatek	k1gInPc1
byly	být	k5eAaImAgInP
uloženy	uložit	k5eAaPmNgInP
na	na	k7c4
přilehlý	přilehlý	k2eAgInSc4d1
starobrněnský	starobrněnský	k2eAgInSc4d1
hřbitov	hřbitov	k1gInSc4
u	u	k7c2
zaniklého	zaniklý	k2eAgInSc2d1
kostela	kostel	k1gInSc2
sv.	sv.	kA
Václava	Václav	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1831	#num#	k4
byl	být	k5eAaImAgInS
na	na	k7c6
hrobě	hrob	k1gInSc6
vztyčen	vztyčen	k2eAgInSc1d1
kamenný	kamenný	k2eAgInSc1d1
náhrobek	náhrobek	k1gInSc1
v	v	k7c6
podobě	podoba	k1gFnSc6
kamenné	kamenný	k2eAgFnSc2d1
pyramidy	pyramida	k1gFnSc2
s	s	k7c7
dutinou	dutina	k1gFnSc7
pro	pro	k7c4
litinový	litinový	k2eAgInSc4d1
sarkofág	sarkofág	k1gInSc4
(	(	kIx(
<g/>
kanopu	kanopa	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
navrhl	navrhnout	k5eAaPmAgMnS
architekt	architekt	k1gMnSc1
Leo	Leo	k1gMnSc1
von	von	k1gInSc4
Klenze	Klenze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Financoval	financovat	k5eAaBmAgMnS
jej	on	k3xPp3gNnSc4
a	a	k8xC
litinové	litinový	k2eAgFnPc4d1
části	část	k1gFnPc4
ze	z	k7c2
svých	svůj	k3xOyFgFnPc2
železáren	železárna	k1gFnPc2
v	v	k7c6
Blansku	Blansko	k1gNnSc6
dodal	dodat	k5eAaPmAgMnS
Dobrovského	Dobrovského	k2eAgMnSc1d1
přítel	přítel	k1gMnSc1
<g/>
,	,	kIx,
hrabě	hrabě	k1gMnSc1
Hugo	Hugo	k1gMnSc1
František	František	k1gMnSc1
Salm-Reifferscheidt	Salm-Reifferscheidt	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
zrušení	zrušení	k1gNnSc6
starobrněnského	starobrněnský	k2eAgInSc2d1
hřbitova	hřbitov	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1883	#num#	k4
byly	být	k5eAaImAgInP
ostatky	ostatek	k1gInPc1
exhumovány	exhumován	k2eAgInPc1d1
a	a	k8xC
s	s	k7c7
původním	původní	k2eAgInSc7d1
náhrobkem	náhrobek	k1gInSc7
přeneseny	přenesen	k2eAgFnPc1d1
na	na	k7c4
nově	nově	k6eAd1
zřízený	zřízený	k2eAgInSc4d1
Ústřední	ústřední	k2eAgInSc4d1
hřbitov	hřbitov	k1gInSc4
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
hrob	hrob	k1gInSc1
dosud	dosud	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
ve	v	k7c6
skupině	skupina	k1gFnSc6
32	#num#	k4
s	s	k7c7
číslem	číslo	k1gNnSc7
4	#num#	k4
<g/>
a.	a.	k?
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Povaha	povaha	k1gFnSc1
a	a	k8xC
vzhled	vzhled	k1gInSc1
</s>
<s>
Portrét	portrét	k1gInSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
,	,	kIx,
eglomizovaná	eglomizovaný	k2eAgFnSc1d1
silueta	silueta	k1gFnSc1
na	na	k7c6
skle	sklo	k1gNnSc6
o	o	k7c6
rozměru	rozměr	k1gInSc6
22,2	22,2	k4
x	x	k?
16,7	16,7	k4
cm	cm	kA
<g/>
,	,	kIx,
neznámý	známý	k2eNgMnSc1d1
autor	autor	k1gMnSc1
<g/>
,	,	kIx,
Čechy	Čechy	k1gFnPc1
<g/>
,	,	kIx,
asi	asi	k9
po	po	k7c6
roce	rok	k1gInSc6
1791	#num#	k4
(	(	kIx(
<g/>
Historické	historický	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Ze	z	k7c2
všech	všecek	k3xTgFnPc2
vzpomínek	vzpomínka	k1gFnPc2
současníků	současník	k1gMnPc2
je	být	k5eAaImIp3nS
patrné	patrný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
byl	být	k5eAaImAgMnS
člověk	člověk	k1gMnSc1
s	s	k7c7
nebývalou	nebývalý	k2eAgFnSc7d1,k2eNgFnSc7d1
autoritou	autorita	k1gFnSc7
<g/>
,	,	kIx,
respektovaný	respektovaný	k2eAgInSc4d1
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
vzdělanost	vzdělanost	k1gFnSc4
i	i	k9
v	v	k7c6
aristokratických	aristokratický	k2eAgInPc6d1
kruzích	kruh	k1gInPc6
<g/>
,	,	kIx,
člověk	člověk	k1gMnSc1
pevných	pevný	k2eAgFnPc2d1
morálních	morální	k2eAgFnPc2d1
zásad	zásada	k1gFnPc2
<g/>
,	,	kIx,
kritického	kritický	k2eAgMnSc2d1
ducha	duch	k1gMnSc2
a	a	k8xC
mimořádného	mimořádný	k2eAgNnSc2d1
nadání	nadání	k1gNnSc2
k	k	k7c3
vědecké	vědecký	k2eAgFnSc3d1
práci	práce	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přitom	přitom	k6eAd1
byl	být	k5eAaImAgInS
velmi	velmi	k6eAd1
skromný	skromný	k2eAgMnSc1d1
<g/>
,	,	kIx,
přirozený	přirozený	k2eAgMnSc1d1
<g/>
,	,	kIx,
laskavý	laskavý	k2eAgMnSc1d1
<g/>
,	,	kIx,
společenský	společenský	k2eAgInSc1d1
a	a	k8xC
vtipný	vtipný	k2eAgInSc1d1
<g/>
,	,	kIx,
rád	rád	k6eAd1
býval	bývat	k5eAaImAgInS
veselý	veselý	k2eAgMnSc1d1
a	a	k8xC
rozuměl	rozumět	k5eAaImAgMnS
si	se	k3xPyFc3
s	s	k7c7
dětmi	dítě	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
svobodomyslný	svobodomyslný	k2eAgMnSc1d1
a	a	k8xC
neměl	mít	k5eNaImAgMnS
rád	rád	k6eAd1
společenská	společenský	k2eAgNnPc4d1
klišé	klišé	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
pevné	pevný	k2eAgFnSc3d1
vůli	vůle	k1gFnSc3
se	se	k3xPyFc4
dokázal	dokázat	k5eAaPmAgMnS
potýkat	potýkat	k5eAaImF
i	i	k9
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
duševní	duševní	k2eAgFnSc7d1
nemocí	nemoc	k1gFnSc7
a	a	k8xC
v	v	k7c6
mezidobích	mezidobí	k1gNnPc6
potom	potom	k6eAd1
intenzivně	intenzivně	k6eAd1
pracovat	pracovat	k5eAaImF
a	a	k8xC
plně	plně	k6eAd1
společensky	společensky	k6eAd1
žít	žít	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1813	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
60	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
jej	on	k3xPp3gMnSc4
malíř	malíř	k1gMnSc1
František	František	k1gMnSc1
Horčička	Horčička	k1gMnSc1
popsal	popsat	k5eAaPmAgMnS
takto	takto	k6eAd1
<g/>
:	:	kIx,
„	„	k?
<g/>
Abbé	abbé	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
musel	muset	k5eAaImAgMnS
být	být	k5eAaImF
v	v	k7c6
mladých	mladý	k2eAgNnPc6d1
letech	léto	k1gNnPc6
skutečně	skutečně	k6eAd1
krásný	krásný	k2eAgInSc1d1
a	a	k8xC
statný	statný	k2eAgMnSc1d1
muž	muž	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
postava	postava	k1gFnSc1
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
ještě	ještě	k6eAd1
vysoká	vysoký	k2eAgFnSc1d1
a	a	k8xC
štíhlá	štíhlý	k2eAgFnSc1d1
<g/>
;	;	kIx,
jen	jen	k9
stáří	stáří	k1gNnSc2
a	a	k8xC
nemoci	nemoc	k1gFnSc2
značně	značně	k6eAd1
nachýlily	nachýlit	k5eAaPmAgFnP
jeho	jeho	k3xOp3gFnSc4
hlavu	hlava	k1gFnSc4
<g/>
,	,	kIx,
již	již	k6eAd1
nosil	nosit	k5eAaImAgInS
kdysi	kdysi	k6eAd1
zpříma	zpříma	k6eAd1
a	a	k8xC
mužně	mužně	k6eAd1
<g/>
;	;	kIx,
také	také	k9
jeho	jeho	k3xOp3gInPc1
vlasy	vlas	k1gInPc1
docela	docela	k6eAd1
zbělely	zbělet	k5eAaPmAgInP
a	a	k8xC
splývají	splývat	k5eAaImIp3nP
lehce	lehko	k6eAd1
a	a	k8xC
neuspořádaně	uspořádaně	k6eNd1
ve	v	k7c6
velkých	velký	k2eAgFnPc6d1
stříbřitě	stříbřitě	k6eAd1
bílých	bílý	k2eAgFnPc6d1
kadeřích	kadeř	k1gFnPc6
dolů	dol	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc1
obličej	obličej	k1gInSc4
je	být	k5eAaImIp3nS
oválný	oválný	k2eAgInSc1d1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc3
oko	oko	k1gNnSc1
šedé	šedá	k1gFnSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc1
nos	nos	k1gInSc1
orlí	orlí	k2eAgInSc1d1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
rty	ret	k1gInPc4
jemně	jemně	k6eAd1
vykrojeny	vykrojen	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barva	barva	k1gFnSc1
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
obličeje	obličej	k1gInSc2
byla	být	k5eAaImAgFnS
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jsem	být	k5eAaImIp1nS
ho	on	k3xPp3gMnSc4
důkladněji	důkladně	k6eAd2
poznal	poznat	k5eAaPmAgMnS
<g/>
,	,	kIx,
olovnatě	olovnatě	k6eAd1
šedá	šedat	k5eAaImIp3nS
<g/>
;	;	kIx,
žár	žár	k1gInSc4
v	v	k7c6
jeho	jeho	k3xOp3gNnPc6
očích	oko	k1gNnPc6
jakoby	jakoby	k8xS
téměř	téměř	k6eAd1
odumřel	odumřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
...	...	k?
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gInSc1
hlas	hlas	k1gInSc1
už	už	k6eAd1
občas	občas	k6eAd1
nezní	znět	k5eNaImIp3nS
příjemně	příjemně	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
žalostně	žalostně	k6eAd1
<g/>
,	,	kIx,
nemocně	mocně	k6eNd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
nemluví	mluvit	k5eNaImIp3nS
zcela	zcela	k6eAd1
souvisle	souvisle	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
chvílemi	chvílemi	k6eAd1
skáče	skákat	k5eAaImIp3nS
z	z	k7c2
jedné	jeden	k4xCgFnSc2
věci	věc	k1gFnSc2
na	na	k7c4
druhou	druhý	k4xOgFnSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
František	František	k1gMnSc1
Palacký	Palacký	k1gMnSc1
vzpomínal	vzpomínat	k5eAaImAgMnS
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
po	po	k7c6
Dobrovského	Dobrovského	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
<g/>
,	,	kIx,
že	že	k8xS
<g/>
:	:	kIx,
„	„	k?
<g/>
rád	rád	k6eAd1
měl	mít	k5eAaImAgMnS
zábavy	zábav	k1gInPc4
společenské	společenský	k2eAgInPc4d1
<g/>
,	,	kIx,
duchaplný	duchaplný	k2eAgInSc4d1
vtip	vtip	k1gInSc4
<g/>
,	,	kIx,
žert	žert	k1gInSc4
a	a	k8xC
ironii	ironie	k1gFnSc4
<g/>
;	;	kIx,
byť	byť	k8xS
při	při	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
tom	ten	k3xDgNnSc6
sebe	sebe	k3xPyFc4
peprněji	peprně	k6eAd2
si	se	k3xPyFc3
byl	být	k5eAaImAgMnS
vedl	vést	k5eAaImAgMnS
<g/>
,	,	kIx,
vždy	vždy	k6eAd1
řídil	řídit	k5eAaImAgMnS
se	se	k3xPyFc4
přece	přece	k9
šťastným	šťastný	k2eAgInSc7d1
taktem	takt	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
neurážel	urážet	k5eNaImAgInS
<g/>
.	.	kIx.
<g/>
“	“	k?
V	v	k7c6
Palackého	Palackého	k2eAgFnSc6d1
paměti	paměť	k1gFnSc6
zůstal	zůstat	k5eAaPmAgMnS
Dobrovský	Dobrovský	k1gMnSc1
jako	jako	k8xS,k8xC
„	„	k?
<g/>
krásný	krásný	k2eAgMnSc1d1
muž	muž	k1gMnSc1
<g/>
,	,	kIx,
postavy	postava	k1gFnPc1
vysoké	vysoký	k2eAgFnPc1d1
<g/>
,	,	kIx,
ztepilé	ztepilý	k2eAgFnPc1d1
<g/>
,	,	kIx,
pravidelných	pravidelný	k2eAgInPc2d1
rysů	rys	k1gInPc2
v	v	k7c6
obličeji	obličej	k1gInSc6
<g/>
,	,	kIx,
živého	živý	k2eAgInSc2d1
zraku	zrak	k1gInSc2
a	a	k8xC
výrazné	výrazný	k2eAgFnSc2d1
duchaplné	duchaplný	k2eAgFnSc2d1
tváře	tvář	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tělesné	tělesný	k2eAgInPc1d1
jeho	jeho	k3xOp3gFnSc4
držení	držení	k1gNnSc1
bylo	být	k5eAaImAgNnS
ještě	ještě	k9
ve	v	k7c6
vysokém	vysoký	k2eAgNnSc6d1
stáří	stáří	k1gNnSc6
rovné	rovný	k2eAgFnSc2d1
a	a	k8xC
pevné	pevný	k2eAgFnSc2d1
[	[	kIx(
<g/>
…	…	k?
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
“	“	k?
Ačkoliv	ačkoliv	k8xS
se	se	k3xPyFc4
poměrně	poměrně	k6eAd1
brzy	brzy	k6eAd1
stal	stát	k5eAaPmAgMnS
uznávanou	uznávaný	k2eAgFnSc7d1
vědeckou	vědecký	k2eAgFnSc7d1
celebritou	celebrita	k1gFnSc7
<g/>
,	,	kIx,
většina	většina	k1gFnSc1
jeho	jeho	k3xOp3gInPc2
dochovaných	dochovaný	k2eAgInPc2d1
portrétů	portrét	k1gInPc2
vzniklých	vzniklý	k2eAgInPc2d1
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
mužném	mužný	k2eAgInSc6d1
věku	věk	k1gInSc6
má	mít	k5eAaImIp3nS
charakter	charakter	k1gInSc1
příležitostných	příležitostný	k2eAgFnPc2d1
<g/>
,	,	kIx,
umělecky	umělecky	k6eAd1
nepříliš	příliš	k6eNd1
náročných	náročný	k2eAgNnPc2d1
dílek	dílko	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svých	svůj	k3xOyFgInPc6
67	#num#	k4
letech	léto	k1gNnPc6
se	se	k3xPyFc4
však	však	k9
dočkal	dočkat	k5eAaPmAgMnS
skutečně	skutečně	k6eAd1
zdařilého	zdařilý	k2eAgInSc2d1
a	a	k8xC
důstojného	důstojný	k2eAgInSc2d1
portrétu	portrét	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
jím	on	k3xPp3gNnSc7
proslulá	proslulý	k2eAgFnSc1d1
olejomalba	olejomalba	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
vytvořil	vytvořit	k5eAaPmAgMnS
František	František	k1gMnSc1
Tkadlík	Tkadlík	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1820	#num#	k4
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Portrétista	portrétista	k1gMnSc1
zde	zde	k6eAd1
zachytil	zachytit	k5eAaPmAgMnS
prosvětlenou	prosvětlený	k2eAgFnSc4d1
starcovu	starcův	k2eAgFnSc4d1
tvář	tvář	k1gFnSc4
a	a	k8xC
jasně	jasně	k6eAd1
modrošedé	modrošedý	k2eAgNnSc4d1
oči	oko	k1gNnPc4
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
hledí	hledět	k5eAaImIp3nS
moudrá	moudrý	k2eAgFnSc1d1
skepse	skepse	k1gFnSc1
stáří	stáří	k1gNnSc2
mírněná	mírněný	k2eAgFnSc1d1
náznakem	náznak	k1gInSc7
laskavého	laskavý	k2eAgInSc2d1
úsměvu	úsměv	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Letopis	letopis	k1gInSc4
Vincenciův	Vincenciův	k2eAgInSc4d1
z	z	k7c2
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc4
rukopis	rukopis	k1gInSc4
o	o	k7c6
rozsahu	rozsah	k1gInSc6
30	#num#	k4
listů	list	k1gInPc2
objevil	objevit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1826	#num#	k4
kaplan	kaplan	k1gMnSc1
Josef	Josef	k1gMnSc1
Dietrich	Dietrich	k1gMnSc1
a	a	k8xC
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
jej	on	k3xPp3gMnSc4
Josefu	Josef	k1gMnSc3
Dobrovskému	Dobrovský	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
pak	pak	k6eAd1
rukopis	rukopis	k1gInSc4
roku	rok	k1gInSc2
1828	#num#	k4
daroval	darovat	k5eAaPmAgMnS
Strahovské	strahovský	k2eAgFnSc3d1
knihovně	knihovna	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
tohoto	tento	k3xDgInSc2
roku	rok	k1gInSc2
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
uložen	uložit	k5eAaPmNgMnS
pod	pod	k7c7
signaturou	signatura	k1gFnSc7
DF	DF	kA
III	III	kA
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
a	a	k8xC
historiografie	historiografie	k1gFnSc1
</s>
<s>
„	„	k?
</s>
<s>
Nejbezpečnější	bezpečný	k2eAgInSc1d3
prostředek	prostředek	k1gInSc1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
naše	náš	k3xOp1gFnPc4
dějiny	dějiny	k1gFnPc4
očistit	očistit	k5eAaPmF
od	od	k7c2
pohádek	pohádka	k1gFnPc2
a	a	k8xC
výmyslů	výmysl	k1gInPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nic	nic	k3yNnSc4
nepřijímat	přijímat	k5eNaImF
bez	bez	k7c2
platných	platný	k2eAgNnPc2d1
svědectví	svědectví	k1gNnPc2
a	a	k8xC
vždy	vždy	k6eAd1
jít	jít	k5eAaImF
nazpět	nazpět	k6eAd1
k	k	k7c3
prvnímu	první	k4xOgInSc3
prameni	pramen	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
,	,	kIx,
Kritische	Kritische	k1gFnSc1
Versuche	Versuche	k1gFnSc1
<g/>
,	,	kIx,
die	die	k?
ältere	älter	k1gMnSc5
böhmische	böhmischus	k1gMnSc5
Geschichte	Geschicht	k1gMnSc5
von	von	k1gInSc1
spätern	spätern	k1gNnSc1
Erdichtungen	Erdichtungen	k1gInSc1
zu	zu	k?
reinigen	reinigen	k1gInSc1
<g/>
,	,	kIx,
III	III	kA
<g/>
,	,	kIx,
Prag	Prag	k1gInSc1
1819	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
3	#num#	k4
[	[	kIx(
<g/>
přel	přít	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marie	Marie	k1gFnSc1
Bláhová	Bláhová	k1gFnSc1
<g/>
]	]	kIx)
</s>
<s>
Dobrovského	Dobrovského	k2eAgFnPc1d1
případové	případový	k2eAgFnPc1d1
studie	studie	k1gFnPc1
k	k	k7c3
některým	některý	k3yIgFnPc3
otázkám	otázka	k1gFnPc3
nejstarších	starý	k2eAgFnPc2d3
českých	český	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
jsou	být	k5eAaImIp3nP
obsaženy	obsáhnout	k5eAaPmNgInP
v	v	k7c6
tzv.	tzv.	kA
Kritických	kritický	k2eAgInPc6d1
pokusech	pokus	k1gInPc6
<g/>
,	,	kIx,
resp.	resp.	kA
v	v	k7c6
třídílném	třídílný	k2eAgInSc6d1
spise	spis	k1gInSc6
Kritische	Kritisch	k1gMnSc2
Versuche	Versuch	k1gMnSc2
<g/>
,	,	kIx,
die	die	k?
ältere	älter	k1gMnSc5
böhmische	böhmischus	k1gMnSc5
Geschichte	Geschicht	k1gMnSc5
von	von	k1gInSc1
spätern	spätern	k1gNnSc1
Erdichtungen	Erdichtungen	k1gInSc1
zu	zu	k?
reinigen	reinigen	k1gInSc1
(	(	kIx(
<g/>
Kritické	kritický	k2eAgInPc1d1
pokusy	pokus	k1gInPc1
očistit	očistit	k5eAaPmF
starší	starý	k2eAgFnPc4d2
české	český	k2eAgFnPc4d1
dějiny	dějiny	k1gFnPc4
od	od	k7c2
pozdějších	pozdní	k2eAgFnPc2d2
smyšlenek	smyšlenka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
vyjadřuje	vyjadřovat	k5eAaImIp3nS
k	k	k7c3
otázce	otázka	k1gFnSc3
Bořivojova	Bořivojův	k2eAgInSc2d1
křtu	křest	k1gInSc2
<g/>
,	,	kIx,
k	k	k7c3
legendě	legenda	k1gFnSc3
o	o	k7c6
Ludmile	Ludmila	k1gFnSc6
a	a	k8xC
Drahomíře	Drahomíra	k1gFnSc3
a	a	k8xC
k	k	k7c3
legendě	legenda	k1gFnSc3
o	o	k7c6
svatém	svatý	k1gMnSc6
Václavu	Václav	k1gMnSc6
a	a	k8xC
Boleslavovi	Boleslav	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
cyklu	cyklus	k1gInSc3
Kritických	kritický	k2eAgInPc2d1
pokusů	pokus	k1gInPc2
se	se	k3xPyFc4
dále	daleko	k6eAd2
volně	volně	k6eAd1
řadí	řadit	k5eAaImIp3nS
jeho	jeho	k3xOp3gInSc1
spis	spis	k1gInSc1
o	o	k7c6
slovanských	slovanský	k2eAgMnPc6d1
apoštolech	apoštol	k1gMnPc6
Cyrilovi	Cyril	k1gMnSc3
a	a	k8xC
Metodějovi	Metoděj	k1gMnSc3
Cyrill	Cyrill	k1gMnSc1
und	und	k?
Method	Method	k1gInSc1
der	drát	k5eAaImRp2nS
Slawen	Slawen	k1gInSc4
Apostel	Apostel	k1gInSc4
<g/>
:	:	kIx,
ein	ein	k?
historisch-kritischer	historisch-kritischra	k1gFnPc2
Versuch	Versuch	k1gMnSc1
(	(	kIx(
<g/>
Cyril	Cyril	k1gMnSc1
a	a	k8xC
Metoděj	Metoděj	k1gMnSc1
<g/>
:	:	kIx,
apoštolové	apoštol	k1gMnPc1
Slovanů	Slovan	k1gInPc2
:	:	kIx,
historicko-kritický	historicko-kritický	k2eAgInSc1d1
rozbor	rozbor	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třebaže	třebaže	k8xS
se	se	k3xPyFc4
v	v	k7c6
některých	některý	k3yIgInPc6
svých	svůj	k3xOyFgInPc6
závěrech	závěr	k1gInPc6
jako	jako	k8xS,k8xC
historik	historik	k1gMnSc1
mýlil	mýlit	k5eAaImAgMnS
(	(	kIx(
<g/>
nemohl	moct	k5eNaImAgMnS
např.	např.	kA
zohlednit	zohlednit	k5eAaPmF
některé	některý	k3yIgFnSc3
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
nově	nově	k6eAd1
nalezené	nalezený	k2eAgInPc1d1
prameny	pramen	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
nepopiratelné	popiratelný	k2eNgFnPc4d1
zásluhy	zásluha	k1gFnPc4
na	na	k7c4
prosazení	prosazení	k1gNnSc4
kritické	kritický	k2eAgFnSc2d1
metody	metoda	k1gFnSc2
v	v	k7c6
české	český	k2eAgFnSc6d1
historiografii	historiografie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Pokud	pokud	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
bádání	bádání	k1gNnSc4
v	v	k7c6
pomocných	pomocný	k2eAgFnPc6d1
vědách	věda	k1gFnPc6
historických	historický	k2eAgFnPc6d1
<g/>
,	,	kIx,
lze	lze	k6eAd1
i	i	k9
zde	zde	k6eAd1
konstatovat	konstatovat	k5eAaBmF
<g/>
,	,	kIx,
že	že	k8xS
Dobrovský	Dobrovský	k1gMnSc1
byl	být	k5eAaImAgMnS
k	k	k7c3
němu	on	k3xPp3gMnSc3
vybaven	vybavit	k5eAaPmNgInS
lépe	dobře	k6eAd2
než	než	k8xS
mnohý	mnohý	k2eAgMnSc1d1
z	z	k7c2
jeho	jeho	k3xOp3gMnPc2
současníků	současník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znal	znát	k5eAaImAgMnS
práce	práce	k1gFnPc4
francouzských	francouzský	k2eAgMnPc2d1
i	i	k8xC
německých	německý	k2eAgMnPc2d1
historiků	historik	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
kterých	který	k3yQgInPc2,k3yIgInPc2,k3yRgInPc2
čerpal	čerpat	k5eAaImAgInS
své	svůj	k3xOyFgFnPc4
znalosti	znalost	k1gFnPc4
o	o	k7c6
paleografii	paleografie	k1gFnSc6
a	a	k8xC
diplomatice	diplomatika	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
dalších	další	k2eAgFnPc6d1
příbuzných	příbuzný	k2eAgFnPc6d1
disciplínách	disciplína	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historik	historik	k1gMnSc1
Václav	Václav	k1gMnSc1
Vojtíšek	Vojtíšek	k1gMnSc1
se	se	k3xPyFc4
o	o	k7c6
Dobrovském	Dobrovský	k1gMnSc6
vyjádřil	vyjádřit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
„	„	k?
<g/>
o	o	k7c6
něm	on	k3xPp3gInSc6
možno	možno	k6eAd1
mluvit	mluvit	k5eAaImF
jako	jako	k9
o	o	k7c6
kodikologovi	kodikolog	k1gMnSc6
dávno	dávno	k6eAd1
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
kodikologie	kodikologie	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
<g/>
,	,	kIx,
a	a	k8xC
při	při	k7c6
tom	ten	k3xDgMnSc6
myslil	myslit	k5eAaImAgMnS
na	na	k7c4
úplný	úplný	k2eAgInSc4d1
soupis	soupis	k1gInSc4
českých	český	k2eAgInPc2d1
rukopisů	rukopis	k1gInPc2
<g/>
.	.	kIx.
<g/>
“	“	k?
Jeho	jeho	k3xOp3gNnSc1
soupisy	soupis	k1gInPc1
rukopisů	rukopis	k1gInPc2
jsou	být	k5eAaImIp3nP
pozoruhodné	pozoruhodný	k2eAgInPc1d1
zvláště	zvláště	k9
uváděním	uvádění	k1gNnSc7
vnějších	vnější	k2eAgInPc2d1
znaků	znak	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
stáří	stáří	k1gNnSc1
<g/>
,	,	kIx,
formát	formát	k1gInSc1
<g/>
,	,	kIx,
počet	počet	k1gInSc1
folií	folie	k1gFnPc2
<g/>
,	,	kIx,
výzdoba	výzdoba	k1gFnSc1
a	a	k8xC
vazba	vazba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všímá	všímat	k5eAaImIp3nS
si	se	k3xPyFc3
i	i	k9
písma	písmo	k1gNnPc1
<g/>
,	,	kIx,
vlastnických	vlastnický	k2eAgFnPc2d1
poznámek	poznámka	k1gFnPc2
a	a	k8xC
kolofonů	kolofon	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nesporný	sporný	k2eNgInSc1d1
je	být	k5eAaImIp3nS
také	také	k9
Dobrovského	Dobrovského	k2eAgInSc4d1
přínos	přínos	k1gInSc4
ke	k	k7c3
studiu	studio	k1gNnSc3
bohemik	bohemikum	k1gNnPc2
v	v	k7c6
zahraničních	zahraniční	k2eAgFnPc6d1
knihovnách	knihovna	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
hypotéza	hypotéza	k1gFnSc1
profesora	profesor	k1gMnSc2
Harvardovy	Harvardův	k2eAgFnPc1d1
univerzity	univerzita	k1gFnPc1
Edwarda	Edward	k1gMnSc2
L.	L.	kA
Keenana	Keenan	k1gMnSc2
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
mohl	moct	k5eAaImAgMnS
být	být	k5eAaImF
autorem	autor	k1gMnSc7
eposu	epos	k1gInSc2
Slovo	slovo	k1gNnSc1
o	o	k7c6
pluku	pluk	k1gInSc6
Igorově	Igorův	k2eAgInSc6d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
Jako	jako	k8xS,k8xC
jeden	jeden	k4xCgInSc1
z	z	k7c2
argumentů	argument	k1gInPc2
uvádí	uvádět	k5eAaImIp3nS
slovo	slovo	k1gNnSc4
„	„	k?
<g/>
rána	ráno	k1gNnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
ve	v	k7c6
výrazu	výraz	k1gInSc6
„	„	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
pod	pod	k7c7
ranami	rána	k1gFnPc7
<g/>
“	“	k?
<g/>
)	)	kIx)
použité	použitý	k2eAgFnSc2d1
ve	v	k7c6
významu	význam	k1gInSc6
„	„	k?
<g/>
úder	úder	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
slovanských	slovanský	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
pouze	pouze	k6eAd1
v	v	k7c6
češtině	čeština	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
jiných	jiný	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
má	mít	k5eAaImIp3nS
slovo	slovo	k1gNnSc1
„	„	k?
<g/>
rána	rána	k1gFnSc1
<g/>
“	“	k?
význam	význam	k1gInSc4
„	„	k?
<g/>
zranění	zranění	k1gNnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
Historik	historik	k1gMnSc1
Milan	Milan	k1gMnSc1
Fryščák	Fryščák	k1gMnSc1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
studii	studie	k1gFnSc6
Keenanovy	Keenanův	k2eAgFnSc2d1
teze	teze	k1gFnSc2
zpochybnil	zpochybnit	k5eAaPmAgInS
na	na	k7c6
základě	základ	k1gInSc6
následujících	následující	k2eAgInPc2d1
argumentů	argument	k1gInPc2
<g/>
:	:	kIx,
1	#num#	k4
<g/>
)	)	kIx)
Přestože	přestože	k8xS
Dobrovský	Dobrovský	k1gMnSc1
trpěl	trpět	k5eAaImAgMnS
duševní	duševní	k2eAgFnSc7d1
nemocí	nemoc	k1gFnSc7
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc4
dílo	dílo	k1gNnSc4
se	se	k3xPyFc4
se	s	k7c7
světem	svět	k1gInSc7
nemoci	nemoc	k1gFnSc2
nikde	nikde	k6eAd1
neprolíná	prolínat	k5eNaImIp3nS
<g/>
.	.	kIx.
2	#num#	k4
<g/>
)	)	kIx)
Podle	podle	k7c2
Dobrovského	Dobrovského	k2eAgInSc2d1
postoje	postoj	k1gInSc2
v	v	k7c6
otázce	otázka	k1gFnSc6
rukopisných	rukopisný	k2eAgInPc2d1
padělků	padělek	k1gInPc2
Václava	Václav	k1gMnSc2
Hanky	Hanka	k1gFnSc2
je	být	k5eAaImIp3nS
zřejmé	zřejmý	k2eAgNnSc1d1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
že	že	k8xS
samotná	samotný	k2eAgFnSc1d1
možnost	možnost	k1gFnSc1
takovéhoto	takovýto	k3xDgInSc2
podvodu	podvod	k1gInSc2
by	by	kYmCp3nS
se	se	k3xPyFc4
příčila	příčit	k5eAaImAgFnS
jeho	jeho	k3xOp3gFnSc3
povaze	povaha	k1gFnSc3
<g/>
.	.	kIx.
3	#num#	k4
<g/>
)	)	kIx)
Dobrovský	Dobrovský	k1gMnSc1
nebyl	být	k5eNaImAgMnS
umělcem	umělec	k1gMnSc7
<g/>
,	,	kIx,
o	o	k7c4
jeho	jeho	k3xOp3gFnPc4
hypotetické	hypotetický	k2eAgFnPc4d1
schopnosti	schopnost	k1gFnPc4
a	a	k8xC
motivaci	motivace	k1gFnSc4
imitovat	imitovat	k5eAaBmF
text	text	k1gInSc4
Slova	slovo	k1gNnSc2
přetvářením	přetváření	k1gNnSc7
starších	starý	k2eAgInPc2d2
textů	text	k1gInPc2
lze	lze	k6eAd1
úspěšně	úspěšně	k6eAd1
pochybovat	pochybovat	k5eAaImF
<g/>
.	.	kIx.
4	#num#	k4
<g/>
)	)	kIx)
Dobrovský	Dobrovský	k1gMnSc1
nemohl	moct	k5eNaImAgMnS
mít	mít	k5eAaImF
za	za	k7c2
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
tak	tak	k6eAd1
pokročilé	pokročilý	k2eAgFnPc1d1
znalosti	znalost	k1gFnPc1
církevně-slovanského	církevně-slovanský	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
jeho	jeho	k3xOp3gInPc4
omyly	omyl	k1gInPc4
v	v	k7c6
otázce	otázka	k1gFnSc6
ortografie	ortografie	k1gFnSc2
prokázal	prokázat	k5eAaPmAgMnS
Alexandr	Alexandr	k1gMnSc1
Christoforovič	Christoforovič	k1gMnSc1
Vostokov	Vostokov	k1gInSc1
až	až	k8xS
po	po	k7c6
Dobrovského	Dobrovského	k2eAgInSc6d1
pobytu	pobyt	k1gInSc6
v	v	k7c6
Rusku	Rusko	k1gNnSc6
roku	rok	k1gInSc2
1820	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Paleoslavistika	Paleoslavistika	k1gFnSc1
a	a	k8xC
biblistika	biblistika	k1gFnSc1
</s>
<s>
Bible	bible	k1gFnSc1
olomoucká	olomoucký	k2eAgFnSc1d1
<g/>
,	,	kIx,
I.	I.	kA
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1417	#num#	k4
<g/>
,	,	kIx,
sign	signum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
M	M	kA
III	III	kA
1	#num#	k4
<g/>
/	/	kIx~
<g/>
I	i	k9
<g/>
,	,	kIx,
folio	folio	k1gNnSc1
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
r	r	kA
</s>
<s>
„	„	k?
</s>
<s>
V	v	k7c6
učené	učený	k2eAgFnSc6d1
republice	republika	k1gFnSc6
není	být	k5eNaImIp3nS
žádných	žádný	k3yNgInPc2
Slovanů	Slovan	k1gInPc2
a	a	k8xC
žádných	žádný	k1gMnPc2
Němců	Němec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
,	,	kIx,
[	[	kIx(
<g/>
přel	přít	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Brabec	Brabec	k1gMnSc1
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dobrovský	Dobrovský	k1gMnSc1
se	se	k3xPyFc4
již	již	k6eAd1
od	od	k7c2
vydání	vydání	k1gNnSc2
své	svůj	k3xOyFgFnSc2
první	první	k4xOgFnSc2
práce	práce	k1gFnSc2
o	o	k7c6
staročeské	staročeský	k2eAgFnSc6d1
bibli	bible	k1gFnSc6
Über	Über	k1gMnSc1
das	das	k?
Alter	Alter	k1gMnSc1
der	drát	k5eAaImRp2nS
böhmischen	böhmischen	k1gInSc4
Bibelüberstetzung	Bibelüberstetzung	k1gInSc4
(	(	kIx(
<g/>
O	o	k7c6
stáří	stáří	k1gNnSc6
českého	český	k2eAgInSc2d1
překladu	překlad	k1gInSc2
Bible	bible	k1gFnSc2
<g/>
)	)	kIx)
pokoušel	pokoušet	k5eAaImAgMnS
objasnit	objasnit	k5eAaPmF
vznik	vznik	k1gInSc4
českého	český	k2eAgInSc2d1
překladu	překlad	k1gInSc2
bible	bible	k1gFnSc2
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
poměr	poměr	k1gInSc1
ke	k	k7c3
staršímu	starý	k2eAgInSc3d2
překladu	překlad	k1gInSc3
staroslověnskému	staroslověnský	k2eAgInSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
položil	položit	k5eAaPmAgMnS
základ	základ	k1gInSc4
svého	svůj	k3xOyFgNnSc2
třídění	třídění	k1gNnSc2
českých	český	k2eAgFnPc2d1
rukopisných	rukopisný	k2eAgFnPc2d1
biblí	bible	k1gFnPc2
do	do	k7c2
čtyř	čtyři	k4xCgFnPc2
tzv.	tzv.	kA
biblických	biblický	k2eAgFnPc2d1
„	„	k?
<g/>
recenzí	recenze	k1gFnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základním	základní	k2eAgInSc7d1
předpokladem	předpoklad	k1gInSc7
pro	pro	k7c4
toto	tento	k3xDgNnSc4
zkoumání	zkoumání	k1gNnSc1
bylo	být	k5eAaImAgNnS
pořízení	pořízení	k1gNnSc4
soupisů	soupis	k1gInPc2
všech	všecek	k3xTgInPc2
staročeských	staročeský	k2eAgInPc2d1
rukopisů	rukopis	k1gInPc2
bible	bible	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
oboru	obor	k1gInSc6
tedy	tedy	k9
Dobrovský	Dobrovský	k1gMnSc1
vykonal	vykonat	k5eAaPmAgMnS
základní	základní	k2eAgFnSc4d1
průkopnickou	průkopnický	k2eAgFnSc4d1
práci	práce	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
i	i	k9
když	když	k8xS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
nepodařilo	podařit	k5eNaPmAgNnS
sepsat	sepsat	k5eAaPmF
dějiny	dějiny	k1gFnPc4
české	český	k2eAgFnSc2d1
bible	bible	k1gFnSc2
<g/>
,	,	kIx,
postavil	postavit	k5eAaPmAgMnS
tento	tento	k3xDgInSc4
výzkum	výzkum	k1gInSc4
na	na	k7c6
více	hodně	k6eAd2
než	než	k8xS
solidní	solidní	k2eAgInPc1d1
základy	základ	k1gInPc1
<g/>
,	,	kIx,
na	na	k7c6
nichž	jenž	k3xRgInPc6
mohli	moct	k5eAaImAgMnP
stavět	stavět	k5eAaImF
další	další	k2eAgMnPc1d1
badatelé	badatel	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
Dobrovský	Dobrovský	k1gMnSc1
zdůrazňoval	zdůrazňovat	k5eAaImAgMnS
velký	velký	k2eAgInSc4d1
jazykový	jazykový	k2eAgInSc4d1
<g/>
,	,	kIx,
literárněhistorický	literárněhistorický	k2eAgInSc4d1
a	a	k8xC
kulturní	kulturní	k2eAgInSc4d1
význam	význam	k1gInSc4
staročeské	staročeský	k2eAgFnSc2d1
bible	bible	k1gFnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
mimo	mimo	k7c4
jiné	jiný	k2eAgInPc4d1
poskytuje	poskytovat	k5eAaImIp3nS
bohatý	bohatý	k2eAgInSc1d1
materiál	materiál	k1gInSc1
pro	pro	k7c4
poznání	poznání	k1gNnSc4
slovní	slovní	k2eAgFnSc2d1
zásoby	zásoba	k1gFnSc2
a	a	k8xC
mluvnické	mluvnický	k2eAgFnSc2d1
stavby	stavba	k1gFnSc2
češtiny	čeština	k1gFnSc2
i	i	k8xC
ostatních	ostatní	k2eAgInPc2d1
slovanských	slovanský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Při	při	k7c6
zkoumání	zkoumání	k1gNnSc6
slovanských	slovanský	k2eAgFnPc2d1
biblí	bible	k1gFnPc2
vycházel	vycházet	k5eAaImAgMnS
Dobrovský	Dobrovský	k1gMnSc1
ze	z	k7c2
svých	svůj	k3xOyFgFnPc2
hebraistických	hebraistický	k2eAgFnPc2d1
studií	studie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
profesor	profesor	k1gMnSc1
hebrejštiny	hebrejština	k1gFnSc2
na	na	k7c6
teologické	teologický	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
pražské	pražský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
Hieronymus	Hieronymus	k1gMnSc1
Frida	Frida	k1gMnSc1
opustil	opustit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
úřad	úřad	k1gInSc4
<g/>
,	,	kIx,
měl	mít	k5eAaImAgMnS
Dobrovský	Dobrovský	k1gMnSc1
o	o	k7c4
tuto	tento	k3xDgFnSc4
pozici	pozice	k1gFnSc4
zájem	zájem	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svou	svůj	k3xOyFgFnSc4
kompetenci	kompetence	k1gFnSc4
na	na	k7c4
profesuru	profesura	k1gFnSc4
Starého	Starého	k2eAgInSc2d1
zákona	zákon	k1gInSc2
chtěl	chtít	k5eAaImAgMnS
prokázat	prokázat	k5eAaPmF
prací	práce	k1gFnSc7
o	o	k7c6
hebrejských	hebrejský	k2eAgNnPc6d1
písmenech	písmeno	k1gNnPc6
Iosephi	Iosephi	k1gNnSc2
Dobrowsky	Dobrowska	k1gFnSc2
De	De	k?
antiqvis	antiqvis	k1gInSc1
Hebraeorvm	Hebraeorvm	k1gMnSc1
characteribvs	characteribvsa	k1gFnPc2
dissertatio	dissertatio	k1gMnSc1
<g/>
:	:	kIx,
in	in	k?
qva	qva	k?
speciatim	speciatim	k1gInSc1
Origenis	Origenis	k1gFnSc2
Hieronymiqve	Hieronymiqev	k1gFnSc2
fides	fides	k1gMnSc1
testimonio	testimonio	k1gMnSc1
Iosephi	Iosephe	k1gFnSc4
Flavii	Flavie	k1gFnSc4
defenditvr	defenditvra	k1gFnPc2
(	(	kIx(
<g/>
Disertace	disertace	k1gFnSc1
o	o	k7c6
starých	starý	k2eAgNnPc6d1
hebrejských	hebrejský	k2eAgNnPc6d1
písmenech	písmeno	k1gNnPc6
:	:	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yRgFnSc6,k3yQgFnSc6
je	být	k5eAaImIp3nS
zvláště	zvláště	k6eAd1
hodnověrnost	hodnověrnost	k1gFnSc1
Origena	Origen	k1gMnSc2
a	a	k8xC
Jeronýma	Jeroným	k1gMnSc2
podepřena	podepřít	k5eAaPmNgFnS
Svědectvím	svědectví	k1gNnSc7
Josefa	Josef	k1gMnSc2
Flavia	Flavius	k1gMnSc2
<g/>
;	;	kIx,
1783	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
vyslovil	vyslovit	k5eAaPmAgMnS
s	s	k7c7
poukazem	poukaz	k1gInSc7
na	na	k7c4
Josefa	Josef	k1gMnSc4
Flavia	Flavius	k1gMnSc4
hypotézu	hypotéza	k1gFnSc4
o	o	k7c6
zavedení	zavedení	k1gNnSc6
tzv.	tzv.	kA
aramejského	aramejský	k2eAgNnSc2d1
písma	písmo	k1gNnSc2
Ezdrášem	Ezdráš	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Johann	Johann	k1gInSc1
D.	D.	kA
Michaelis	Michaelis	k1gFnSc2
v	v	k7c6
recenzi	recenze	k1gFnSc6
této	tento	k3xDgFnSc2
práce	práce	k1gFnSc2
ocenil	ocenit	k5eAaPmAgMnS
pečlivé	pečlivý	k2eAgInPc4d1
údaje	údaj	k1gInPc4
<g/>
,	,	kIx,
nesouhlasil	souhlasit	k5eNaImAgMnS
však	však	k9
s	s	k7c7
jejím	její	k3xOp3gInSc7
závěrem	závěr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
totiž	totiž	k9
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
zavedení	zavedení	k1gNnSc1
aramejského	aramejský	k2eAgNnSc2d1
písma	písmo	k1gNnSc2
je	být	k5eAaImIp3nS
dosvědčeno	dosvědčen	k2eAgNnSc1d1
jen	jen	k6eAd1
nespolehlivou	spolehlivý	k2eNgFnSc7d1
ústní	ústní	k2eAgFnSc7d1
tradicí	tradice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozdějším	pozdní	k2eAgInSc7d2
výzkumem	výzkum	k1gInSc7
byl	být	k5eAaImAgInS
však	však	k9
potvrzen	potvrzen	k2eAgInSc1d1
Dobrovského	Dobrovského	k2eAgInSc1d1
názor	názor	k1gInSc1
o	o	k7c4
existenci	existence	k1gFnSc4
dvou	dva	k4xCgInPc2
typů	typ	k1gInPc2
starého	starý	k2eAgNnSc2d1
hebrejského	hebrejský	k2eAgNnSc2d1
písma	písmo	k1gNnSc2
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
tvrzení	tvrzení	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
staré	starý	k2eAgNnSc4d1
hebrejské	hebrejský	k2eAgNnSc4d1
i	i	k8xC
aramejské	aramejský	k2eAgNnSc4d1
písmo	písmo	k1gNnSc4
má	mít	k5eAaImIp3nS
stejné	stejný	k2eAgInPc4d1
kořeny	kořen	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejzávažnější	závažný	k2eAgInPc4d3
Dobrovského	Dobrovského	k2eAgInPc4d1
příspěvky	příspěvek	k1gInPc4
k	k	k7c3
hebraistice	hebraistika	k1gFnSc3
zůstaly	zůstat	k5eAaPmAgInP
neuveřejněny	uveřejněn	k2eNgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
zejména	zejména	k9
jeho	jeho	k3xOp3gInSc1
pokusy	pokus	k1gInPc1
o	o	k7c6
aplikaci	aplikace	k1gFnSc6
matematiky	matematika	k1gFnSc2
na	na	k7c4
studium	studium	k1gNnSc4
souhlásek	souhláska	k1gFnPc2
v	v	k7c6
hebrejských	hebrejský	k2eAgInPc6d1
trojsouhláskových	trojsouhláskový	k2eAgInPc6d1
kořenech	kořen	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc4
znalosti	znalost	k1gFnPc4
a	a	k8xC
zásady	zásada	k1gFnPc4
textové	textový	k2eAgFnPc4d1
kritiky	kritik	k1gMnPc7
hebrejské	hebrejský	k2eAgFnSc2d1
bible	bible	k1gFnSc2
později	pozdě	k6eAd2
použil	použít	k5eAaPmAgMnS
pro	pro	k7c4
studium	studium	k1gNnSc4
starých	starý	k2eAgFnPc2d1
slovanských	slovanský	k2eAgFnPc2d1
biblí	bible	k1gFnPc2
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
v	v	k7c6
samotném	samotný	k2eAgNnSc6d1
třídění	třídění	k1gNnSc6
staroslověnských	staroslověnský	k2eAgFnPc2d1
souhlásek	souhláska	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dobrovský	Dobrovský	k1gMnSc1
a	a	k8xC
(	(	kIx(
<g/>
slovanská	slovanský	k2eAgFnSc1d1
<g/>
)	)	kIx)
jazykověda	jazykověda	k1gFnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
–	–	k?
Geschichte	Geschicht	k1gInSc5
der	drát	k5eAaImRp2nS
Böhmischen	Böhmischen	k1gInSc4
Sprache	Sprach	k1gMnSc2
und	und	k?
ältern	ältern	k1gInSc1
Literatur	literatura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bei	Bei	k1gFnSc1
Gottlieb	Gottliba	k1gFnPc2
Haase	Haase	k1gFnSc1
<g/>
,	,	kIx,
Prag	Prag	k1gInSc1
1818	#num#	k4
(	(	kIx(
<g/>
pdf	pdf	k?
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
osvícenské	osvícenský	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
již	již	k6eAd1
existovaly	existovat	k5eAaImAgFnP
určité	určitý	k2eAgFnPc1d1
domněnky	domněnka	k1gFnPc1
o	o	k7c4
příbuzenství	příbuzenství	k1gNnSc4
latiny	latina	k1gFnSc2
a	a	k8xC
řečtiny	řečtina	k1gFnSc2
se	s	k7c7
sanskrtem	sanskrt	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejstarší	starý	k2eAgFnSc1d3
teorie	teorie	k1gFnSc1
o	o	k7c6
indoevropských	indoevropský	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
formulovali	formulovat	k5eAaImAgMnP
William	William	k1gInSc4
Jones	Jones	k1gMnSc1
a	a	k8xC
Friedrich	Friedrich	k1gMnSc1
Schlegel	Schlegel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgNnSc1
systematické	systematický	k2eAgNnSc1d1
propracování	propracování	k1gNnSc1
těchto	tento	k3xDgFnPc2
tezí	teze	k1gFnPc2
nabízí	nabízet	k5eAaImIp3nS
až	až	k9
dílo	dílo	k1gNnSc1
Conjugationssystem	Conjugationssyst	k1gInSc7
der	drát	k5eAaImRp2nS
Sanskriptsprache	Sanskriptsprache	k1gNnSc4
(	(	kIx(
<g/>
1816	#num#	k4
<g/>
)	)	kIx)
od	od	k7c2
Franze	Franze	k1gFnSc2
Boppa	Bopp	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobrovský	Dobrovský	k1gMnSc1
tyto	tento	k3xDgFnPc4
práce	práce	k1gFnPc4
neznal	znát	k5eNaImAgMnS,k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
před	před	k7c7
Boppem	Bopp	k1gMnSc7
však	však	k8xC
postavil	postavit	k5eAaPmAgMnS
své	svůj	k3xOyFgNnSc4
zkoumání	zkoumání	k1gNnSc4
slovanských	slovanský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
na	na	k7c4
stanovení	stanovení	k1gNnSc4
kořenů	kořen	k1gInPc2
a	a	k8xC
určení	určení	k1gNnSc4
pravidel	pravidlo	k1gNnPc2
tvoření	tvoření	k1gNnSc2
a	a	k8xC
odvozování	odvozování	k1gNnSc4
slov	slovo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svou	svůj	k3xOyFgFnSc4
teorii	teorie	k1gFnSc4
kořenů	kořen	k1gInPc2
spolu	spolu	k6eAd1
s	s	k7c7
úvahami	úvaha	k1gFnPc7
o	o	k7c6
původu	původ	k1gInSc6
slovanského	slovanský	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
poprvé	poprvé	k6eAd1
publikoval	publikovat	k5eAaBmAgMnS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
stati	stať	k1gFnSc6
Ueber	Uebra	k1gFnPc2
den	den	k1gInSc1
Ursprung	Ursprung	k1gMnSc1
und	und	k?
Bildung	Bildung	k1gMnSc1
der	drát	k5eAaImRp2nS
Slawischen	Slawischen	k1gInSc1
<g/>
,	,	kIx,
und	und	k?
insbesondere	insbesondrat	k5eAaPmIp3nS
der	drát	k5eAaImRp2nS
Böhmischen	Böhmischen	k1gInSc4
Sprache	Sprach	k1gMnSc2
(	(	kIx(
<g/>
O	o	k7c6
původu	původ	k1gInSc6
a	a	k8xC
tvoření	tvoření	k1gNnSc6
slovanských	slovanský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
a	a	k8xC
češtiny	čeština	k1gFnSc2
zvláště	zvláště	k6eAd1
<g/>
)	)	kIx)
v	v	k7c6
roce	rok	k1gInSc6
1791	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
své	svůj	k3xOyFgInPc4
názory	názor	k1gInPc4
shrnul	shrnout	k5eAaPmAgMnS
ve	v	k7c6
spisku	spisek	k1gInSc6
Entwurf	Entwurf	k1gMnSc1
zu	zu	k?
einem	einem	k6eAd1
allgemeinen	allgemeinen	k2eAgInSc4d1
Etymologicon	Etymologicon	k1gInSc4
der	drát	k5eAaImRp2nS
slawischen	slawischen	k1gInSc4
Sprachen	Sprachen	k2eAgInSc4d1
(	(	kIx(
<g/>
Náčrt	náčrt	k1gInSc4
k	k	k7c3
obecné	obecný	k2eAgFnSc3d1
etymologii	etymologie	k1gFnSc3
slovanských	slovanský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
;	;	kIx,
1813	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
heslovými	heslový	k2eAgNnPc7d1
slovy	slovo	k1gNnPc7
slovanského	slovanský	k2eAgInSc2d1
etymologického	etymologický	k2eAgInSc2d1
slovníku	slovník	k1gInSc2
by	by	kYmCp3nP
měly	mít	k5eAaImAgFnP
být	být	k5eAaImF
kořeny	kořen	k2eAgFnPc4d1
rozdělené	rozdělená	k1gFnPc4
do	do	k7c2
tří	tři	k4xCgFnPc2
tříd	třída	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
rovněž	rovněž	k9
konstatuje	konstatovat	k5eAaBmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
časování	časování	k1gNnSc1
je	být	k5eAaImIp3nS
jistý	jistý	k2eAgInSc4d1
druh	druh	k1gInSc4
„	„	k?
<g/>
skládání	skládání	k1gNnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
tak	tak	k6eAd1
zde	zde	k6eAd1
vlastně	vlastně	k9
před	před	k7c7
Boppem	Bopp	k1gInSc7
vyslovuje	vyslovovat	k5eAaImIp3nS
„	„	k?
<g/>
aglutinační	aglutinační	k2eAgFnSc1d1
<g/>
“	“	k?
teorii	teorie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
tento	tento	k3xDgInSc4
názor	názor	k1gInSc4
měla	mít	k5eAaImAgFnS
zajisté	zajisté	k9
vliv	vliv	k1gInSc4
i	i	k8xC
Dobrovského	Dobrovského	k2eAgNnPc4d1
hebraistická	hebraistický	k2eAgNnPc4d1
studia	studio	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
korespondence	korespondence	k1gFnSc2
s	s	k7c7
Kopitarem	Kopitar	k1gInSc7
je	být	k5eAaImIp3nS
zřejmé	zřejmý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
zvláště	zvláště	k6eAd1
ke	k	k7c3
konci	konec	k1gInSc3
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
zabýval	zabývat	k5eAaImAgInS
také	také	k9
sanskrtem	sanskrt	k1gInSc7
a	a	k8xC
jeho	jeho	k3xOp3gFnPc2
souvislostí	souvislost	k1gFnPc2
se	s	k7c7
slovanskými	slovanský	k2eAgInPc7d1
jazyky	jazyk	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
dopisech	dopis	k1gInPc6
poprvé	poprvé	k6eAd1
objevuje	objevovat	k5eAaImIp3nS
jméno	jméno	k1gNnSc4
Franze	Franze	k1gFnSc2
Boppa	Boppa	k1gFnSc1
s	s	k7c7
politováním	politování	k1gNnSc7
<g/>
,	,	kIx,
že	že	k8xS
z	z	k7c2
neznalosti	neznalost	k1gFnSc2
jazyka	jazyk	k1gInSc2
nepřihlédl	přihlédnout	k5eNaPmAgInS
i	i	k9
k	k	k7c3
slovanskému	slovanský	k2eAgInSc3d1
materiálu	materiál	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc4
myšlenky	myšlenka	k1gFnPc4
vyslovil	vyslovit	k5eAaPmAgMnS
Dobrovský	Dobrovský	k1gMnSc1
na	na	k7c6
samém	samý	k3xTgInSc6
konci	konec	k1gInSc6
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nS
je	on	k3xPp3gMnPc4
dále	daleko	k6eAd2
rozpracoval	rozpracovat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nelze	lze	k6eNd1
jej	on	k3xPp3gMnSc4
tedy	tedy	k9
pokládat	pokládat	k5eAaImF
za	za	k7c4
jednoho	jeden	k4xCgMnSc4
ze	z	k7c2
zakladatelů	zakladatel	k1gMnPc2
indoevropské	indoevropský	k2eAgFnSc2d1
jazykovědy	jazykověda	k1gFnSc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
to	ten	k3xDgNnSc4
činí	činit	k5eAaImIp3nS
např.	např.	kA
Miloš	Miloš	k1gMnSc1
Weingart	Weingart	k1gInSc1
v	v	k7c6
doslovu	doslov	k1gInSc6
k	k	k7c3
české	český	k2eAgFnSc3d1
edici	edice	k1gFnSc3
Dobrovského	Dobrovského	k2eAgFnSc2d1
církevně-slovanské	církevně-slovanský	k2eAgFnSc2d1
mluvnice	mluvnice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
nejednom	nejeden	k4xCyIgInSc6
ohledu	ohled	k1gInSc6
průkopníkem	průkopník	k1gMnSc7
v	v	k7c6
oblasti	oblast	k1gFnSc6
klasifikace	klasifikace	k1gFnSc2
slovanských	slovanský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založil	založit	k5eAaPmAgMnS
první	první	k4xOgNnSc4
lingvisticky	lingvisticky	k6eAd1
fundovanou	fundovaný	k2eAgFnSc4d1
klasifikaci	klasifikace	k1gFnSc4
těchto	tento	k3xDgInPc2
jazyků	jazyk	k1gInPc2
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
geografické	geografický	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
<g/>
,	,	kIx,
tj.	tj.	kA
na	na	k7c4
dichotomii	dichotomie	k1gFnSc4
mezi	mezi	k7c7
jihovýchodními	jihovýchodní	k2eAgInPc7d1
a	a	k8xC
severozápadními	severozápadní	k2eAgInPc7d1
slovanskými	slovanský	k2eAgInPc7d1
jazyky	jazyk	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gFnSc1
klasifikace	klasifikace	k1gFnSc1
představená	představený	k2eAgFnSc1d1
v	v	k7c6
dílech	díl	k1gInPc6
Lehrgebäude	Lehrgebäud	k1gInSc5
der	drát	k5eAaImRp2nS
Böhmischen	Böhmischen	k1gInSc4
Sprache	Sprach	k1gMnSc2
a	a	k8xC
Institutiones	Institutiones	k1gInSc1
linguae	linguaat	k5eAaPmIp3nS
slavicae	slavicae	k6eAd1
dialecti	dialect	k5eAaBmF,k5eAaImF,k5eAaPmF
veteris	veteris	k1gInSc4
však	však	k9
byla	být	k5eAaImAgFnS
později	pozdě	k6eAd2
kritizována	kritizován	k2eAgFnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
bulharštinu	bulharština	k1gFnSc4
<g/>
,	,	kIx,
ukrajinštinu	ukrajinština	k1gFnSc4
nebo	nebo	k8xC
běloruštinu	běloruština	k1gFnSc4
nepovažoval	považovat	k5eNaImAgMnS
za	za	k7c4
samostatné	samostatný	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
Pokud	pokud	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c6
Dobrovského	Dobrovského	k2eAgNnSc6d1
pojetí	pojetí	k1gNnSc6
tvoření	tvoření	k1gNnSc2
slov	slovo	k1gNnPc2
vyložené	vyložený	k2eAgFnSc2d1
ve	v	k7c6
spisech	spis	k1gInPc6
Die	Die	k1gMnSc1
Bildsamkeit	Bildsamkeit	k1gMnSc1
der	drát	k5eAaImRp2nS
slawischen	slawischen	k1gInSc4
Sprache	Sprach	k1gMnSc2
a	a	k8xC
Die	Die	k1gMnSc2
Bildung	Bildunga	k1gFnPc2
der	drát	k5eAaImRp2nS
Wörter	Wörtrum	k1gNnPc2
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
vykazuje	vykazovat	k5eAaImIp3nS
v	v	k7c6
některých	některý	k3yIgInPc6
ohledech	ohled	k1gInPc6
shody	shoda	k1gFnSc2
s	s	k7c7
dnešním	dnešní	k2eAgNnSc7d1
chápáním	chápání	k1gNnSc7
slovotvorného	slovotvorný	k2eAgInSc2d1
procesu	proces	k1gInSc2
(	(	kIx(
<g/>
např.	např.	kA
pojem	pojem	k1gInSc1
Nebenbegriff	Nebenbegriff	k1gInSc1
<g/>
,	,	kIx,
tj.	tj.	kA
vedlejší	vedlejší	k2eAgInSc1d1
význam	význam	k1gInSc1
slova	slovo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
celku	celek	k1gInSc6
se	se	k3xPyFc4
však	však	k9
od	od	k7c2
současného	současný	k2eAgNnSc2d1
pojetí	pojetí	k1gNnSc2
liší	lišit	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
zjištění	zjištění	k1gNnPc1
Dobrovského	Dobrovského	k2eAgNnPc1d1
platí	platit	k5eAaImIp3nP
stále	stále	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
jiná	jiný	k2eAgFnSc1d1
již	již	k6eAd1
zastarala	zastarat	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
každém	každý	k3xTgInSc6
případě	případ	k1gInSc6
byl	být	k5eAaImAgMnS
Dobrovský	Dobrovský	k1gMnSc1
prvním	první	k4xOgNnSc7
českým	český	k2eAgMnSc7d1
jazykovědcem	jazykovědec	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
si	se	k3xPyFc3
tyto	tento	k3xDgFnPc4
otázky	otázka	k1gFnPc4
kladl	klást	k5eAaImAgMnS
<g/>
,	,	kIx,
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
dobu	doba	k1gFnSc4
rozsáhlé	rozsáhlý	k2eAgInPc4d1
a	a	k8xC
důkladné	důkladný	k2eAgNnSc4d1
zpracování	zpracování	k1gNnSc4
českého	český	k2eAgNnSc2d1
tvoření	tvoření	k1gNnSc2
slov	slovo	k1gNnPc2
zůstává	zůstávat	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc7
trvalou	trvalý	k2eAgFnSc7d1
zásluhou	zásluha	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dobrovský	Dobrovský	k1gMnSc1
a	a	k8xC
čeština	čeština	k1gFnSc1
</s>
<s>
„	„	k?
</s>
<s>
Češi	Čech	k1gMnPc1
opustili	opustit	k5eAaPmAgMnP
svou	svůj	k3xOyFgFnSc4
společnou	společný	k2eAgFnSc4d1
výslovnost	výslovnost	k1gFnSc4
a	a	k8xC
píší	psát	k5eAaImIp3nP
podle	podle	k7c2
moravské	moravský	k2eAgFnSc2d1
a	a	k8xC
slovenské	slovenský	k2eAgFnSc2d1
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
jest	být	k5eAaImIp3nS
starší	starý	k2eAgFnPc4d2
výslovnosti	výslovnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
výslovnost	výslovnost	k1gFnSc1
tak	tak	k9
dlouho	dlouho	k6eAd1
variovala	variovat	k5eAaBmAgFnS
<g/>
,	,	kIx,
až	až	k8xS
se	se	k3xPyFc4
podle	podle	k7c2
českých	český	k2eAgMnPc2d1
bratrů	bratr	k1gMnPc2
konečně	konečně	k6eAd1
ustálil	ustálit	k5eAaPmAgInS
nynější	nynější	k2eAgInSc1d1
pravopis	pravopis	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
…	…	k?
<g/>
]	]	kIx)
bylo	být	k5eAaImAgNnS
by	by	kYmCp3nP
možné	možný	k2eAgNnSc1d1
začít	začít	k5eAaPmF
Hájkem	hájek	k1gInSc7
a	a	k8xC
skončit	skončit	k5eAaPmF
Komenským	Komenská	k1gFnPc3
<g/>
:	:	kIx,
to	ten	k3xDgNnSc1
by	by	kYmCp3nS
bylo	být	k5eAaImAgNnS
období	období	k1gNnSc4
více	hodně	k6eAd2
než	než	k8xS
130	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
Čechy	Čechy	k1gFnPc1
oplývaly	oplývat	k5eAaImAgFnP
bohatou	bohatý	k2eAgFnSc7d1
literaturou	literatura	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
,	,	kIx,
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovského	k2eAgFnSc2d1
korrespondence	korrespondence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
IV	Iva	k1gFnPc2
<g/>
,	,	kIx,
Vzájemné	vzájemný	k2eAgInPc1d1
listy	list	k1gInPc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
a	a	k8xC
Jiřího	Jiří	k1gMnSc2
Ribaye	Ribay	k1gMnSc2
z	z	k7c2
let	léto	k1gNnPc2
1783	#num#	k4
<g/>
-	-	kIx~
<g/>
1810	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1913	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
178	#num#	k4
<g/>
–	–	k?
<g/>
179	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
překl	překl	k1gInSc1
<g/>
.	.	kIx.
<g/>
]	]	kIx)
</s>
<s>
Dobrovského	Dobrovského	k2eAgFnSc1d1
mluvnice	mluvnice	k1gFnSc1
češtiny	čeština	k1gFnSc2
Ausführliches	Ausführlichesa	k1gFnPc2
Lehrgebäude	Lehrgebäud	k1gInSc5
der	drát	k5eAaImRp2nS
böhmischen	böhmischen	k1gInSc4
Sprache	Sprach	k1gMnSc2
dosud	dosud	k6eAd1
nebyla	být	k5eNaImAgFnS
vyčerpávajícím	vyčerpávající	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
analyzována	analyzován	k2eAgNnPc4d1
ve	v	k7c6
všech	všecek	k3xTgFnPc6
rovinách	rovina	k1gFnPc6
<g/>
,	,	kIx,
jimiž	jenž	k3xRgFnPc7
by	by	kYmCp3nS
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
její	její	k3xOp3gNnSc4
zpracování	zpracování	k1gNnSc4
v	v	k7c6
kontextu	kontext	k1gInSc6
předcházejících	předcházející	k2eAgFnPc2d1
<g/>
,	,	kIx,
soudobých	soudobý	k2eAgFnPc2d1
a	a	k8xC
následujících	následující	k2eAgFnPc2d1
mluvnic	mluvnice	k1gFnPc2
a	a	k8xC
rovněž	rovněž	k9
z	z	k7c2
hlediska	hledisko	k1gNnSc2
moderní	moderní	k2eAgFnSc2d1
metodologie	metodologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
pohledu	pohled	k1gInSc2
sociolingvistického	sociolingvistický	k2eAgInSc2d1
lze	lze	k6eAd1
konstatovat	konstatovat	k5eAaBmF
<g/>
,	,	kIx,
že	že	k8xS
plnila	plnit	k5eAaImAgFnS
zvláštní	zvláštní	k2eAgFnSc4d1
úlohu	úloha	k1gFnSc4
v	v	k7c6
jazykovém	jazykový	k2eAgInSc6d1
programu	program	k1gInSc6
českého	český	k2eAgNnSc2d1
národního	národní	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
souvislosti	souvislost	k1gFnSc6
šlo	jít	k5eAaImAgNnS
bezpochyby	bezpochyby	k6eAd1
o	o	k7c4
obranu	obrana	k1gFnSc4
jazyka	jazyk	k1gInSc2
s	s	k7c7
vyzdvihnutím	vyzdvihnutí	k1gNnSc7
jeho	jeho	k3xOp3gFnPc2
estetických	estetický	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
<g/>
,	,	kIx,
slavné	slavný	k2eAgFnSc3d1
minulosti	minulost	k1gFnSc3
a	a	k8xC
výrazové	výrazový	k2eAgFnSc3d1
schopnosti	schopnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Primárním	primární	k2eAgInSc7d1
cílem	cíl	k1gInSc7
Dobrovského	Dobrovský	k1gMnSc2
patrně	patrně	k6eAd1
nebylo	být	k5eNaImAgNnS
hledání	hledání	k1gNnSc1
spisovné	spisovný	k2eAgFnSc2d1
normy	norma	k1gFnSc2
<g/>
,	,	kIx,
Lehrgebäude	Lehrgebäud	k1gInSc5
také	také	k9
postrádá	postrádat	k5eAaImIp3nS
jakýkoliv	jakýkoliv	k3yIgInSc4
aspekt	aspekt	k1gInSc4
komunikace	komunikace	k1gFnSc2
či	či	k8xC
agitace	agitace	k1gFnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
jeho	jeho	k3xOp3gNnSc1
dílo	dílo	k1gNnSc1
nebylo	být	k5eNaImAgNnS
určeno	určit	k5eAaPmNgNnS
širokému	široký	k2eAgNnSc3d1
česky	česky	k6eAd1
hovořícímu	hovořící	k2eAgNnSc3d1
publiku	publikum	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
výklad	výklad	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
výsledkem	výsledek	k1gInSc7
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
zaměření	zaměření	k1gNnSc2
na	na	k7c4
textovou	textový	k2eAgFnSc4d1
kritiku	kritika	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
spíše	spíše	k9
výrazem	výraz	k1gInSc7
učeneckého	učenecký	k2eAgInSc2d1
zájmu	zájem	k1gInSc2
o	o	k7c4
strukturu	struktura	k1gFnSc4
jazyka	jazyk	k1gInSc2
a	a	k8xC
hledání	hledání	k1gNnSc1
jeho	jeho	k3xOp3gFnPc2
hranic	hranice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novější	nový	k2eAgFnSc1d2
čeština	čeština	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
Dobrovskému	Dobrovský	k1gMnSc3
v	v	k7c6
porovnání	porovnání	k1gNnSc6
se	s	k7c7
starší	starý	k2eAgFnSc7d2
řečí	řeč	k1gFnSc7
připadala	připadat	k5eAaPmAgFnS,k5eAaImAgFnS
chudá	chudý	k2eAgFnSc1d1
a	a	k8xC
slohově	slohově	k6eAd1
i	i	k9
výrazově	výrazově	k6eAd1
nesprávná	správný	k2eNgFnSc1d1
<g/>
,	,	kIx,
zde	zde	k6eAd1
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
z	z	k7c2
hlediska	hledisko	k1gNnSc2
dějin	dějiny	k1gFnPc2
či	či	k8xC
dochovaných	dochovaný	k2eAgFnPc2d1
památek	památka	k1gFnPc2
porovnána	porovnán	k2eAgFnSc1d1
s	s	k7c7
popisem	popis	k1gInSc7
jazyka	jazyk	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
považoval	považovat	k5eAaImAgInS
za	za	k7c4
ústrojný	ústrojný	k2eAgInSc4d1
a	a	k8xC
„	„	k?
<g/>
správný	správný	k2eAgInSc1d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
během	během	k7c2
svého	svůj	k3xOyFgNnSc2
působení	působení	k1gNnSc2
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
navštěvoval	navštěvovat	k5eAaImAgInS
„	„	k?
<g/>
učené	učený	k2eAgInPc1d1
sedláky	sedlák	k1gInPc1
<g/>
“	“	k?
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc1
češtinu	čeština	k1gFnSc4
považoval	považovat	k5eAaImAgInS
za	za	k7c7
„	„	k?
<g/>
nezkaženou	zkažený	k2eNgFnSc7d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
otázce	otázka	k1gFnSc6
referenční	referenční	k2eAgFnSc1d1
funkce	funkce	k1gFnSc1
jazyka	jazyk	k1gInSc2
se	se	k3xPyFc4
Dobrovský	Dobrovský	k1gMnSc1
klonil	klonit	k5eAaImAgMnS
k	k	k7c3
názoru	názor	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
úlohu	úloha	k1gFnSc4
přirozeného	přirozený	k2eAgInSc2d1
komunikačního	komunikační	k2eAgInSc2d1
prostředku	prostředek	k1gInSc2
ve	v	k7c6
sféře	sféra	k1gFnSc6
kulturní	kulturní	k2eAgFnSc2d1
komunikace	komunikace	k1gFnSc2
již	již	k6eAd1
plní	plnit	k5eAaImIp3nS
němčina	němčina	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
ještě	ještě	k9
tehdy	tehdy	k6eAd1
nestála	stát	k5eNaImAgFnS
v	v	k7c6
pozici	pozice	k1gFnSc6
„	„	k?
<g/>
nepřítele	nepřítel	k1gMnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
vůči	vůči	k7c3
němuž	jenž	k3xRgNnSc3
by	by	kYmCp3nS
se	se	k3xPyFc4
národní	národní	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
muselo	muset	k5eAaImAgNnS
vymezovat	vymezovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
první	první	k4xOgNnSc4
české	český	k2eAgNnSc4d1
klasické	klasický	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
považoval	považovat	k5eAaImAgInS
kralický	kralický	k2eAgInSc1d1
překlad	překlad	k1gInSc1
bible	bible	k1gFnSc2
<g/>
,	,	kIx,
u	u	k7c2
něhož	jenž	k3xRgNnSc2
si	se	k3xPyFc3
povšiml	povšimnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
čeština	čeština	k1gFnSc1
se	se	k3xPyFc4
obohatila	obohatit	k5eAaPmAgFnS
a	a	k8xC
zjemnila	zjemnit	k5eAaPmAgFnS
<g/>
,	,	kIx,
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
ohebnější	ohebný	k2eAgFnSc1d2
a	a	k8xC
nenucenější	nucený	k2eNgFnSc1d2
<g/>
,	,	kIx,
sloh	sloh	k1gInSc1
plynnější	plynný	k2eAgInSc1d2
<g/>
,	,	kIx,
výraz	výraz	k1gInSc1
výstižnější	výstižný	k2eAgInSc1d2
a	a	k8xC
přiměřenější	přiměřený	k2eAgInSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soudobou	soudobý	k2eAgFnSc4d1
situaci	situace	k1gFnSc4
však	však	k9
hodnotil	hodnotit	k5eAaImAgMnS
jako	jako	k8xC,k8xS
úpadek	úpadek	k1gInSc4
projevující	projevující	k2eAgInSc4d1
se	se	k3xPyFc4
nedostatkem	nedostatek	k1gInSc7
původních	původní	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
svých	svůj	k3xOyFgFnPc6
úvahách	úvaha	k1gFnPc6
o	o	k7c6
gramatice	gramatika	k1gFnSc6
neměl	mít	k5eNaImAgMnS
Dobrovský	Dobrovský	k1gMnSc1
na	na	k7c6
mysli	mysl	k1gFnSc6
úplnou	úplný	k2eAgFnSc4d1
sociální	sociální	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
nového	nový	k2eAgInSc2d1
národa	národ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obrozeneckou	obrozenecký	k2eAgFnSc4d1
intenci	intence	k1gFnSc4
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
dílu	díl	k1gInSc2
přisoudila	přisoudit	k5eAaPmAgFnS
až	až	k9
pozdější	pozdní	k2eAgFnPc4d2
recepce	recepce	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
To	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
Dobrovský	Dobrovský	k1gMnSc1
sáhl	sáhnout	k5eAaPmAgMnS
k	k	k7c3
jazyku	jazyk	k1gInSc3
starší	starý	k2eAgFnSc2d2
normy	norma	k1gFnSc2
<g/>
,	,	kIx,
mělo	mít	k5eAaImAgNnS
své	svůj	k3xOyFgNnSc1
opodstatnění	opodstatnění	k1gNnSc1
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
knížkách	knížka	k1gFnPc6
lidového	lidový	k2eAgNnSc2d1
čtení	čtení	k1gNnSc2
byl	být	k5eAaImAgInS
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
době	doba	k1gFnSc6
tento	tento	k3xDgInSc4
jazyk	jazyk	k1gInSc4
ještě	ještě	k6eAd1
zachován	zachován	k2eAgInSc4d1
a	a	k8xC
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
uměli	umět	k5eAaImAgMnP
a	a	k8xC
chtěli	chtít	k5eAaImAgMnP
číst	číst	k5eAaImF
<g/>
,	,	kIx,
mu	on	k3xPp3gMnSc3
rozuměli	rozumět	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
aplikace	aplikace	k1gFnSc2
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
gramatického	gramatický	k2eAgNnSc2d1
díla	dílo	k1gNnSc2
je	být	k5eAaImIp3nS
potom	potom	k6eAd1
souběžné	souběžný	k2eAgNnSc1d1
používání	používání	k1gNnSc1
spisovného	spisovný	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
a	a	k8xC
obecné	obecný	k2eAgFnSc2d1
češtiny	čeština	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
mělo	mít	k5eAaImAgNnS
pro	pro	k7c4
utváření	utváření	k1gNnSc4
novodobé	novodobý	k2eAgFnSc2d1
spisovné	spisovný	k2eAgFnSc2d1
češtiny	čeština	k1gFnSc2
značný	značný	k2eAgInSc4d1
význam	význam	k1gInSc4
prostředí	prostředí	k1gNnSc2
císařské	císařský	k2eAgFnSc2d1
Vídně	Vídeň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
existoval	existovat	k5eAaImAgInS
úřad	úřad	k1gInSc1
tzv.	tzv.	kA
„	„	k?
<g/>
sprachmeistera	sprachmeister	k1gMnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
böhmischer	böhmischra	k1gFnPc2
Sprachmeister	Sprachmeister	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
byl	být	k5eAaImAgMnS
učitel	učitel	k1gMnSc1
češtiny	čeština	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
tomuto	tento	k3xDgInSc3
jazyku	jazyk	k1gInSc3
učil	učít	k5eAaPmAgMnS,k5eAaImAgMnS
rakouské	rakouský	k2eAgMnPc4d1
arcivévody	arcivévoda	k1gMnPc4
i	i	k8xC
samotného	samotný	k2eAgMnSc4d1
panovníka	panovník	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Dobrovského	Dobrovského	k2eAgFnSc6d1
době	doba	k1gFnSc6
tuto	tento	k3xDgFnSc4
úlohu	úloha	k1gFnSc4
plnili	plnit	k5eAaImAgMnP
jazykovědci	jazykovědec	k1gMnPc1
Jan	Jan	k1gMnSc1
Václav	Václav	k1gMnSc1
Pohl	Pohl	k1gMnSc1
<g/>
,	,	kIx,
Maximilián	Maximilián	k1gMnSc1
Václav	Václav	k1gMnSc1
Šimek	Šimek	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Blažej	Blažej	k1gMnSc1
Athanasius	Athanasius	k1gMnSc1
Spurný	Spurný	k1gMnSc1
a	a	k8xC
Josef	Josef	k1gMnSc1
Valentin	Valentin	k1gMnSc1
Zlobický	Zlobický	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
těmito	tento	k3xDgMnPc7
vídeňskými	vídeňský	k2eAgMnPc7d1
učiteli	učitel	k1gMnPc7
udržoval	udržovat	k5eAaImAgMnS
Dobrovský	Dobrovský	k1gMnSc1
korespondenci	korespondence	k1gFnSc4
a	a	k8xC
vedl	vést	k5eAaImAgMnS
s	s	k7c7
nimi	on	k3xPp3gMnPc7
i	i	k9
četné	četný	k2eAgFnPc1d1
polemiky	polemika	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
vyjasnily	vyjasnit	k5eAaPmAgInP
jeho	jeho	k3xOp3gInPc1
vědecké	vědecký	k2eAgInPc1d1
postoje	postoj	k1gInPc1
k	k	k7c3
řadě	řada	k1gFnSc3
slavistických	slavistický	k2eAgMnPc2d1
<g/>
,	,	kIx,
komparatistických	komparatistický	k2eAgInPc2d1
a	a	k8xC
historických	historický	k2eAgInPc2d1
problémů	problém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
sporu	spor	k1gInSc6
s	s	k7c7
Pohlem	Pohl	k1gMnSc7
se	se	k3xPyFc4
mladý	mladý	k2eAgMnSc1d1
Dobrovský	Dobrovský	k1gMnSc1
vzepřel	vzepřít	k5eAaPmAgMnS
jeho	jeho	k3xOp3gNnSc3
puristickému	puristický	k2eAgNnSc3d1
novotaření	novotaření	k1gNnSc3
ve	v	k7c6
slovotvorbě	slovotvorba	k1gFnSc6
a	a	k8xC
upevnil	upevnit	k5eAaPmAgInS
při	při	k7c6
tom	ten	k3xDgNnSc6
svou	svůj	k3xOyFgFnSc4
argumentaci	argumentace	k1gFnSc4
o	o	k7c6
výhodnosti	výhodnost	k1gFnSc6
kontinuity	kontinuita	k1gFnSc2
se	s	k7c7
starým	starý	k2eAgInSc7d1
bratrským	bratrský	k2eAgInSc7d1
pravopisem	pravopis	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zlobického	Zlobický	k2eAgNnSc2d1
pak	pak	k8xC
kritizoval	kritizovat	k5eAaImAgMnS
za	za	k7c4
jeho	jeho	k3xOp3gFnSc4
germanizující	germanizující	k2eAgFnSc4d1
právnickou	právnický	k2eAgFnSc4d1
češtinu	čeština	k1gFnSc4
v	v	k7c6
překladech	překlad	k1gInPc6
říšských	říšský	k2eAgInPc2d1
zákoníků	zákoník	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
spolupráce	spolupráce	k1gFnSc1
byla	být	k5eAaImAgFnS
oboustranně	oboustranně	k6eAd1
výhodná	výhodný	k2eAgFnSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
vídeňští	vídeňský	k2eAgMnPc1d1
jazykovědci	jazykovědec	k1gMnPc1
mohli	moct	k5eAaImAgMnP
korigovat	korigovat	k5eAaBmF
své	svůj	k3xOyFgInPc4
nedostatky	nedostatek	k1gInPc4
a	a	k8xC
Dobrovský	Dobrovský	k1gMnSc1
byl	být	k5eAaImAgMnS
zase	zase	k9
mírněn	mírněn	k2eAgInSc1d1
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
hyperkriticismu	hyperkriticismus	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkaz	odkaz	k1gInSc1
Dobrovského	Dobrovský	k1gMnSc2
ve	v	k7c6
slovanské	slovanský	k2eAgFnSc6d1
filologii	filologie	k1gFnSc6
</s>
<s>
Busta	busta	k1gFnSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1891	#num#	k4
od	od	k7c2
sochaře	sochař	k1gMnSc2
Tomáše	Tomáš	k1gMnSc2
Seidana	Seidan	k1gMnSc2
</s>
<s>
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
prokazatelně	prokazatelně	k6eAd1
ovlivnil	ovlivnit	k5eAaPmAgMnS
vývoj	vývoj	k1gInSc4
slovanských	slovanský	k2eAgFnPc2d1
studií	studie	k1gFnPc2
v	v	k7c6
Rusku	Rusko	k1gNnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1800	#num#	k4
<g/>
–	–	k?
<g/>
1810	#num#	k4
si	se	k3xPyFc3
dopisoval	dopisovat	k5eAaImAgInS
s	s	k7c7
filologem	filolog	k1gMnSc7
Alexandrem	Alexandr	k1gMnSc7
Christoforovičem	Christoforovič	k1gMnSc7
Vostokovem	Vostokov	k1gInSc7
<g/>
,	,	kIx,
spisovatelem	spisovatel	k1gMnSc7
Alexandrem	Alexandr	k1gMnSc7
Semjonovičem	Semjonovič	k1gMnSc7
Šiškovem	Šiškov	k1gInSc7
a	a	k8xC
historikem	historik	k1gMnSc7
Nikolajem	Nikolaj	k1gMnSc7
Michajlovičem	Michajlovič	k1gMnSc7
Karamzinem	Karamzin	k1gMnSc7
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
za	za	k7c4
člena	člen	k1gMnSc4
Ruské	ruský	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1820	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
rovněž	rovněž	k9
členem	člen	k1gMnSc7
Svobodné	svobodný	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
milovníků	milovník	k1gMnPc2
ruské	ruský	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
Značné	značný	k2eAgFnSc2d1
vážnosti	vážnost	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
Rusku	Rusko	k1gNnSc6
těšilo	těšit	k5eAaImAgNnS
dílo	dílo	k1gNnSc1
Institutiones	Institutiones	k1gInSc1
linguae	linguaat	k5eAaPmIp3nS
slavicae	slavicae	k6eAd1
dialecti	dialect	k5eAaPmF,k5eAaImF,k5eAaBmF
veteris	veteris	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
po	po	k7c6
založení	založení	k1gNnSc6
kateder	katedra	k1gFnPc2
slovanských	slovanský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
na	na	k7c6
ruských	ruský	k2eAgFnPc6d1
univerzitách	univerzita	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1835	#num#	k4
stalo	stát	k5eAaPmAgNnS
součástí	součást	k1gFnPc2
osnov	osnova	k1gFnPc2
zejména	zejména	k9
ve	v	k7c6
smyslu	smysl	k1gInSc6
standardní	standardní	k2eAgFnSc2d1
charakteristiky	charakteristika	k1gFnSc2
slovanských	slovanský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobrovského	Dobrovského	k2eAgFnPc1d1
historické	historický	k2eAgFnPc1d1
práce	práce	k1gFnPc1
ovlivnily	ovlivnit	k5eAaPmAgFnP
ruské	ruský	k2eAgNnSc4d1
podání	podání	k1gNnSc4
cyrilometodějské	cyrilometodějský	k2eAgFnSc2d1
problematiky	problematika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historik	historik	k1gMnSc1
<g/>
,	,	kIx,
slavista	slavista	k1gMnSc1
<g/>
,	,	kIx,
archeolog	archeolog	k1gMnSc1
a	a	k8xC
etnograf	etnograf	k1gMnSc1
Alexandr	Alexandr	k1gMnSc1
Alexandrovič	Alexandrovič	k1gMnSc1
Kotljarevskij	Kotljarevskij	k1gMnSc1
(	(	kIx(
<g/>
1837	#num#	k4
<g/>
–	–	k?
<g/>
1881	#num#	k4
<g/>
)	)	kIx)
o	o	k7c6
Dobrovském	Dobrovský	k1gMnSc6
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
mu	on	k3xPp3gMnSc3
Evropa	Evropa	k1gFnSc1
vděčí	vděčit	k5eAaImIp3nS
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
jí	on	k3xPp3gFnSc3
odkryl	odkrýt	k5eAaPmAgInS
slovanský	slovanský	k2eAgInSc1d1
svět	svět	k1gInSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
]	]	kIx)
Na	na	k7c6
konci	konec	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
v	v	k7c6
Rusku	Rusko	k1gNnSc6
o	o	k7c6
Dobrovském	Dobrovský	k1gMnSc6
publikoval	publikovat	k5eAaBmAgMnS
např.	např.	kA
filolog	filolog	k1gMnSc1
a	a	k8xC
historik	historik	k1gMnSc1
Ivan	Ivan	k1gMnSc1
Alexejevič	Alexejevič	k1gMnSc1
Sněgirev	Sněgirev	k1gFnSc1
(	(	kIx(
<g/>
1843	#num#	k4
<g/>
–	–	k?
<g/>
1893	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgMnS
popsat	popsat	k5eAaPmF
„	„	k?
<g/>
Dobrovského	Dobrovského	k2eAgFnSc4d1
školu	škola	k1gFnSc4
<g/>
“	“	k?
v	v	k7c6
ruské	ruský	k2eAgFnSc6d1
slavistice	slavistika	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přestože	přestože	k8xS
jako	jako	k8xS,k8xC
slavista	slavista	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
neodlišoval	odlišovat	k5eNaImAgMnS
ukrajinský	ukrajinský	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
od	od	k7c2
ruštiny	ruština	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
ani	ani	k8xC
jako	jako	k9
dialekt	dialekt	k1gInSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc4
myšlenky	myšlenka	k1gFnPc1
ovlivnily	ovlivnit	k5eAaPmAgFnP
formování	formování	k1gNnSc4
ukrajinského	ukrajinský	k2eAgInSc2d1
spisovného	spisovný	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
v	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Publikované	publikovaný	k2eAgInPc1d1
spisy	spis	k1gInPc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
otvíraly	otvírat	k5eAaImAgFnP
před	před	k7c4
první	první	k4xOgNnSc4
generací	generace	k1gFnPc2
ukrajinského	ukrajinský	k2eAgNnSc2d1
národního	národní	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
nový	nový	k2eAgInSc4d1
slovanský	slovanský	k2eAgInSc4d1
svět	svět	k1gInSc4
<g/>
,	,	kIx,
seznamovaly	seznamovat	k5eAaImAgInP
ukrajinské	ukrajinský	k2eAgInPc1d1
intelektuály	intelektuál	k1gMnPc7
s	s	k7c7
dějinami	dějiny	k1gFnPc7
<g/>
,	,	kIx,
literaturou	literatura	k1gFnSc7
<g/>
,	,	kIx,
jazykem	jazyk	k1gInSc7
a	a	k8xC
etnografií	etnografie	k1gFnSc7
jednotlivých	jednotlivý	k2eAgInPc2d1
slovanských	slovanský	k2eAgInPc2d1
národů	národ	k1gInPc2
a	a	k8xC
ukazovaly	ukazovat	k5eAaImAgInP
tak	tak	k9
cestu	cesta	k1gFnSc4
k	k	k7c3
podobné	podobný	k2eAgFnSc3d1
práci	práce	k1gFnSc3
v	v	k7c6
rámci	rámec	k1gInSc6
vlastního	vlastní	k2eAgInSc2d1
národa	národ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studie	studie	k1gFnSc1
Dobrovského	Dobrovského	k2eAgInPc1d1
byly	být	k5eAaImAgInP
pro	pro	k7c4
Ukrajince	Ukrajinec	k1gMnPc4
vzorem	vzor	k1gInSc7
a	a	k8xC
podnětem	podnět	k1gInSc7
pro	pro	k7c4
zkoumání	zkoumání	k1gNnSc4
mateřského	mateřský	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
a	a	k8xC
probouzely	probouzet	k5eAaImAgInP
v	v	k7c6
nich	on	k3xPp3gFnPc6
vědomí	vědomí	k1gNnSc1
jazykové	jazykový	k2eAgFnSc2d1
svébytnosti	svébytnost	k1gFnSc2
a	a	k8xC
národní	národní	k2eAgFnSc2d1
hrdosti	hrdost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Recepci	recepce	k1gFnSc4
Dobrovského	Dobrovského	k2eAgNnSc2d1
díla	dílo	k1gNnSc2
ve	v	k7c6
Slovinsku	Slovinsko	k1gNnSc6
zprostředkovával	zprostředkovávat	k5eAaImAgMnS
především	především	k9
jeho	jeho	k3xOp3gMnSc1
přítel	přítel	k1gMnSc1
a	a	k8xC
„	„	k?
<g/>
slovinský	slovinský	k2eAgMnSc1d1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
“	“	k?
Jernej	Jernej	k1gMnSc1
Kopitar	Kopitar	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
ihned	ihned	k6eAd1
po	po	k7c6
vydání	vydání	k1gNnSc6
Institucí	instituce	k1gFnPc2
okomentoval	okomentovat	k5eAaPmAgInS
tento	tento	k3xDgInSc1
spis	spis	k1gInSc1
ve	v	k7c6
dvou	dva	k4xCgInPc6
kratších	krátký	k2eAgInPc6d2
textech	text	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
slovinské	slovinský	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
lze	lze	k6eAd1
vystopovat	vystopovat	k5eAaPmF
recepci	recepce	k1gFnSc4
reflexivní	reflexivní	k2eAgFnSc4d1
(	(	kIx(
<g/>
Kopitar	Kopitar	k1gMnSc1
<g/>
)	)	kIx)
i	i	k9
kompilačně	kompilačně	k6eAd1
reproduktivní	reproduktivní	k2eAgNnSc1d1
(	(	kIx(
<g/>
především	především	k9
Fran	Fran	k1gMnSc1
Serafin	Serafin	k1gMnSc1
Metelko	Metelko	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovinští	slovinský	k2eAgMnPc1d1
učenci	učenec	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
tehdy	tehdy	k6eAd1
procházeli	procházet	k5eAaImAgMnP
zejména	zejména	k9
školou	škola	k1gFnSc7
slovanské	slovanský	k2eAgFnSc2d1
filologie	filologie	k1gFnSc2
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
(	(	kIx(
<g/>
Vatroslav	Vatroslav	k1gMnSc1
Jagić	Jagić	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
se	se	k3xPyFc4
v	v	k7c6
díle	dílo	k1gNnSc6
českého	český	k2eAgMnSc2d1
slavisty	slavista	k1gMnSc2
mohli	moct	k5eAaImAgMnP
poprvé	poprvé	k6eAd1
seznámit	seznámit	k5eAaPmF
s	s	k7c7
nadnářeční	nadnářeční	k2eAgFnSc7d1
podobou	podoba	k1gFnSc7
spisovné	spisovný	k2eAgFnSc2d1
slovanštiny	slovanština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
slovinských	slovinský	k2eAgMnPc2d1
autorů	autor	k1gMnPc2
mluvnic	mluvnice	k1gFnPc2
a	a	k8xC
slovníků	slovník	k1gInPc2
do	do	k7c2
1	#num#	k4
<g/>
.	.	kIx.
poloviny	polovina	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
bylo	být	k5eAaImAgNnS
běžné	běžný	k2eAgNnSc1d1
přejímání	přejímání	k1gNnSc1
jednotlivých	jednotlivý	k2eAgInPc2d1
lexikálních	lexikální	k2eAgInPc2d1
příkladů	příklad	k1gInPc2
z	z	k7c2
Dobrovského	Dobrovského	k2eAgFnPc2d1
prací	práce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Kopitarově	Kopitarův	k2eAgInSc6d1
náhrobku	náhrobek	k1gInSc6
v	v	k7c6
Lublani	Lublaň	k1gFnSc6
je	být	k5eAaImIp3nS
nápis	nápis	k1gInSc1
„	„	k?
<g/>
In	In	k1gFnSc2
slavicis	slavicis	k1gFnSc2
literis	literis	k1gFnSc3
augendis	augendis	k1gFnSc3
magni	mageň	k1gFnSc3
Dobrovii	Dobrovie	k1gFnSc3
ingeniosus	ingeniosus	k1gMnSc1
aemulator	aemulator	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
V	v	k7c6
rozšiřování	rozšiřování	k1gNnSc6
slavistiky	slavistika	k1gFnSc2
velkého	velký	k2eAgMnSc4d1
Dobrovského	Dobrovský	k1gMnSc4
důmyslný	důmyslný	k2eAgMnSc1d1
napodobitel	napodobitel	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dobrovský	Dobrovský	k1gMnSc1
měl	mít	k5eAaImAgMnS
velký	velký	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
lužickosrbské	lužickosrbský	k2eAgNnSc4d1
národní	národní	k2eAgNnSc4d1
obrození	obrození	k1gNnSc4
<g/>
,	,	kIx,
zejména	zejména	k9
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
první	první	k4xOgFnSc6
etapě	etapa	k1gFnSc6
<g/>
,	,	kIx,
neboť	neboť	k8xC
přispěl	přispět	k5eAaPmAgMnS
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
samotných	samotný	k2eAgInPc2d1
základů	základ	k1gInPc2
lužickosrbské	lužickosrbský	k2eAgFnSc2d1
filologie	filologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
patrné	patrný	k2eAgNnSc1d1
z	z	k7c2
gramatických	gramatický	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
Handrije	Handrije	k1gMnSc2
Zejlera	Zejler	k1gMnSc2
a	a	k8xC
Jana	Jan	k1gMnSc2
Arnošta	Arnošt	k1gMnSc2
Smolera	Smoler	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
pro	pro	k7c4
české	český	k2eAgMnPc4d1
slavisty	slavista	k1gMnPc4
byly	být	k5eAaImAgFnP
oba	dva	k4xCgInPc4
lužickosrbské	lužickosrbský	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
důležitým	důležitý	k2eAgInSc7d1
prostředkem	prostředek	k1gInSc7
k	k	k7c3
lepšímu	dobrý	k2eAgNnSc3d2
poznání	poznání	k1gNnSc3
češtiny	čeština	k1gFnSc2
i	i	k9
k	k	k7c3
vlastním	vlastní	k2eAgInPc3d1
filologickým	filologický	k2eAgInPc3d1
výzkumům	výzkum	k1gInPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Park	park	k1gInSc1
Kampa	Kampa	k1gFnSc1
<g/>
,	,	kIx,
pomník	pomník	k1gInSc4
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
</s>
<s>
Pamětní	pamětní	k2eAgFnSc1d1
deska	deska	k1gFnSc1
pobytu	pobyt	k1gInSc2
Dobrovského	Dobrovského	k2eAgInSc1d1
<g/>
,	,	kIx,
lázeňský	lázeňský	k2eAgInSc1d1
dům	dům	k1gInSc1
čp.	čp.	k?
74	#num#	k4
v	v	k7c6
Teplicích	Teplice	k1gFnPc6
</s>
<s>
Pamětní	pamětní	k2eAgFnSc1d1
deska	deska	k1gFnSc1
připomínající	připomínající	k2eAgNnSc4d1
místo	místo	k1gNnSc4
úmrtí	úmrtí	k1gNnSc2
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
</s>
<s>
Dobrovského	Dobrovského	k2eAgFnSc1d1
busta	busta	k1gFnSc1
v	v	k7c6
knihovně	knihovna	k1gFnSc6
kláštera	klášter	k1gInSc2
Hradisko	hradisko	k1gNnSc4
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Výběrová	výběrový	k2eAgFnSc1d1
bibliografie	bibliografie	k1gFnSc1
</s>
<s>
Portrét	portrét	k1gInSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
,	,	kIx,
kresba	kresba	k1gFnSc1
tužkou	tužka	k1gFnSc7
<g/>
,	,	kIx,
Orest	Orest	k1gMnSc1
Adamovič	Adamovič	k1gMnSc1
Kiprenskij	Kiprenskij	k1gMnSc1
<g/>
,	,	kIx,
Mariánské	mariánský	k2eAgFnPc1d1
Lázně	lázeň	k1gFnPc1
4	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1823	#num#	k4
<g/>
,	,	kIx,
Petrohradská	petrohradský	k2eAgFnSc1d1
pobočka	pobočka	k1gFnSc1
Archivu	archiv	k1gInSc2
Ruské	ruský	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Studie	studie	k1gFnPc1
a	a	k8xC
monografie	monografie	k1gFnPc1
</s>
<s>
Pragische	Pragische	k6eAd1
Fragmente	fragment	k1gInSc5
Hebräischer	Hebräischra	k1gFnPc2
Handschriften	Handschriften	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Michaelis	Michaelis	k1gInSc1
<g/>
,	,	kIx,
Johann	Johann	k1gMnSc1
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orientalische	Orientalische	k1gFnSc1
und	und	k?
Exegetische	Exegetisch	k1gFnSc2
Bibliothek	Bibliothky	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teil	Teil	k1gInSc1
12	#num#	k4
<g/>
,	,	kIx,
1777	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
101	#num#	k4
<g/>
–	–	k?
<g/>
111	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Corrigenda	Corrigenda	k1gFnSc1
in	in	k?
Bohemia	bohemia	k1gFnSc1
docta	docta	k1gFnSc1
Balbini	Balbin	k2eAgMnPc1d1
juxta	juxta	k1gFnSc1
editionem	edition	k1gInSc7
P.	P.	kA
Raphaelis	Raphaelis	k1gInSc1
Ungarn	Ungarn	k1gNnSc1
canon	canon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praemonstr	Praemonstr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
SS	SS	kA
<g/>
.	.	kIx.
theologiae	theologiaat	k5eAaPmIp3nS
doctoris	doctoris	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pragae	Pragae	k1gNnSc1
<g/>
:	:	kIx,
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1779	#num#	k4
<g/>
,	,	kIx,
43	#num#	k4
s.	s.	k?
</s>
<s>
Antwort	Antwort	k1gInSc1
auf	auf	k?
die	die	k?
Revision	Revision	k1gInSc1
der	drát	k5eAaImRp2nS
böhmischen	böhmischno	k1gNnPc2
Litteratur	Litteratura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
Zu	Zu	k1gFnSc2
haben	haben	k2eAgMnSc1d1
einzeln	einzeln	k1gMnSc1
<g/>
,	,	kIx,
und	und	k?
mit	mit	k?
der	drát	k5eAaImRp2nS
böhm	böhm	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lit.	Lit.	k1gFnSc1
in	in	k?
der	drát	k5eAaImRp2nS
Mangoldischen	Mangoldischna	k1gFnPc2
Buchhandlung	Buchhandlung	k1gInSc1
<g/>
,	,	kIx,
1780	#num#	k4
<g/>
.	.	kIx.
30	#num#	k4
s.	s.	k?
</s>
<s>
Prüfung	Prüfung	k1gMnSc1
der	drát	k5eAaImRp2nS
Gedanken	Gedankno	k1gNnPc2
über	über	k1gInSc1
die	die	k?
Feldwirtschaften	Feldwirtschaften	k2eAgInSc1d1
der	drát	k5eAaImRp2nS
Landgeistlichen	Landgeistlichen	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
[	[	kIx(
<g/>
s.	s.	k?
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1781	#num#	k4
<g/>
,	,	kIx,
23	#num#	k4
s.	s.	k?
</s>
<s>
Abhandlung	Abhandlung	k1gMnSc1
über	über	k1gMnSc1
den	den	k1gInSc4
Ursprung	Ursprung	k1gInSc1
des	des	k1gNnSc1
Namens	Namens	k1gInSc1
Tschech	Tschech	k1gInSc1
(	(	kIx(
<g/>
Czech	Czech	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Tschechen	Tschechen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
gedruckt	gedruckt	k1gInSc1
bey	bey	k?
Johann	Johann	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
Edlen	Edlen	k2eAgInSc4d1
von	von	k1gInSc4
Schönfeld	Schönfelda	k1gFnPc2
<g/>
,	,	kIx,
1782	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
16	#num#	k4
s.	s.	k?
</s>
<s>
Über	Über	k1gMnSc1
das	das	k?
Alter	Alter	k1gMnSc1
der	drát	k5eAaImRp2nS
böhmischen	böhmischno	k1gNnPc2
Bibelübersetzung	Bibelübersetzunga	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Abhandlungen	Abhandlungen	k1gInSc1
einer	einer	k1gInSc1
Privatgesellschaft	Privatgesellschaft	k1gInSc1
5	#num#	k4
<g/>
,	,	kIx,
1782	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
300	#num#	k4
<g/>
–	–	k?
<g/>
322	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Iosephi	Iosephi	k1gNnSc1
Dobrowsky	Dobrowska	k1gFnSc2
De	De	k?
antiqvis	antiqvis	k1gInSc1
Hebraeorvm	Hebraeorvm	k1gMnSc1
characteribvs	characteribvsa	k1gFnPc2
dissertatio	dissertatio	k1gMnSc1
<g/>
:	:	kIx,
in	in	k?
qva	qva	k?
speciatim	speciatim	k1gInSc1
origenis	origenis	k1gFnSc2
Hieronymiqve	Hieronymiqev	k1gFnSc2
fides	fides	k1gMnSc1
testimonio	testimonio	k1gMnSc1
Iosephi	Iosephe	k1gFnSc4
Flavii	Flavie	k1gFnSc4
defenditvr	defenditvra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pragae	Pragae	k1gInSc1
<g/>
:	:	kIx,
Characteribus	Characteribus	k1gInSc1
Caesareo-Regiae	Caesareo-Regia	k1gFnSc2
Scholae	Schola	k1gFnSc2
Normalis	Normalis	k1gFnSc2
<g/>
,	,	kIx,
1783	#num#	k4
<g/>
.	.	kIx.
51	#num#	k4
<g/>
,	,	kIx,
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
s.	s.	k?
</s>
<s>
Rezension	Rezension	k1gInSc1
und	und	k?
kritische	kritischat	k5eAaPmIp3nS
Anmerkungen	Anmerkungen	k1gInSc4
über	über	k1gInSc1
des	des	k1gNnSc1
Reichstädter	Reichstädter	k1gInSc1
Herrn	Herrn	k1gMnSc1
Dechants	Dechantsa	k1gFnPc2
von	von	k1gInSc1
Schönfeld	Schönfeld	k1gMnSc1
am	am	k?
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
May	May	k1gMnSc1
in	in	k?
der	drát	k5eAaImRp2nS
Skalka	Skalka	k1gMnSc1
gehaltene	gehalten	k1gInSc5
lateinische	lateinische	k1gNnPc3
Lobreie	Lobreie	k1gFnSc2
auf	auf	k?
das	das	k?
Fest	fest	k6eAd1
des	des	k1gNnSc1
heiligen	heiligen	k1gInSc1
Johann	Johann	k1gMnSc1
von	von	k1gInSc1
Nepomuk	Nepomuk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gMnSc1
<g/>
,	,	kIx,
Auf	Auf	k1gMnSc1
Kosten	Kostno	k1gNnPc2
des	des	k1gNnSc7
Verfassers	Verfassersa	k1gFnPc2
<g/>
,	,	kIx,
1783	#num#	k4
<g/>
,	,	kIx,
29	#num#	k4
s.	s.	k?
</s>
<s>
De	De	k?
sacerdotum	sacerdotum	k1gNnSc1
in	in	k?
Bohemia	bohemia	k1gFnSc1
caelibatu	caelibat	k1gInSc2
narratio	narratio	k6eAd1
historica	historica	k6eAd1
<g/>
,	,	kIx,
cui	cui	k?
constitutiones	constitutiones	k1gMnSc1
concilii	concilie	k1gFnSc3
Moguntini	Moguntin	k2eAgMnPc1d1
<g/>
,	,	kIx,
Fritzlariae	Fritzlaria	k1gInSc2
1244	#num#	k4
celebrati	celebrat	k5eAaPmF
<g/>
,	,	kIx,
adnexae	adnexaat	k5eAaPmIp3nS
sunt	sunt	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pragae	Praga	k1gInSc2
1787	#num#	k4
<g/>
,	,	kIx,
40	#num#	k4
<g/>
,	,	kIx,
23	#num#	k4
s.	s.	k?
</s>
<s>
Geschichte	Geschicht	k1gMnSc5
der	drát	k5eAaImRp2nS
böhmischen	böhmischno	k1gNnPc2
Pikarden	Pikardno	k1gNnPc2
und	und	k?
Adamiten	Adamitno	k1gNnPc2
<g/>
,	,	kIx,
In	In	k1gFnSc1
<g/>
:	:	kIx,
Abhandlungen	Abhandlungen	k1gInSc1
der	drát	k5eAaImRp2nS
Böhmischen	Böhmischna	k1gFnPc2
Gesellschaft	Gesellschaft	k1gInSc1
der	drát	k5eAaImRp2nS
Wissenschaften	Wissenschaften	k2eAgInSc4d1
<g/>
:	:	kIx,
auf	auf	k?
das	das	k?
Jahr	Jahr	k1gInSc1
1788	#num#	k4
<g/>
.	.	kIx.
oder	odrat	k5eAaPmRp2nS
Vierter	Vierter	k1gMnSc1
Theil	Theil	k1gMnSc1
<g/>
,	,	kIx,
nebst	nebst	k1gMnSc1
der	drát	k5eAaImRp2nS
Geschichte	Geschicht	k1gInSc5
derselben	derselbit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mit	Mit	k1gMnSc1
Kupfern	Kupfern	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
In	In	k1gFnSc1
der	drát	k5eAaImRp2nS
Waltherischen	Waltherischna	k1gFnPc2
Hofbuchhandlung	Hofbuchhandlung	k1gInSc1
<g/>
,	,	kIx,
1789	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
nečíslované	číslovaný	k2eNgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
300	#num#	k4
<g/>
–	–	k?
<g/>
343	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Ueber	Ueber	k1gMnSc1
die	die	k?
Ergebenheit	Ergebenheit	k1gMnSc1
und	und	k?
Anhänglichkeit	Anhänglichkeit	k1gMnSc1
der	drát	k5eAaImRp2nS
Slawischen	Slawischno	k1gNnPc2
Völker	Völkero	k1gNnPc2
an	an	k?
das	das	k?
Erzhaus	Erzhaus	k1gMnSc1
Oestreich	Oestreich	k1gMnSc1
<g/>
:	:	kIx,
vorgelesen	vorgelesen	k2eAgInSc1d1
den	den	k1gInSc1
25	#num#	k4
Sept	septum	k1gNnPc2
<g/>
.	.	kIx.
1791	#num#	k4
im	im	k?
Saake	Saake	k1gFnPc2
der	drát	k5eAaImRp2nS
k.	k.	k?
Böhm	Böhm	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gesellschaft	Gesellschaft	k1gInSc1
der	drát	k5eAaImRp2nS
Wissenschafte	Wissenschaft	k1gInSc5
in	in	k?
Gegenwart	Gegenwart	k1gInSc4
Sr	Sr	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maj	Maja	k1gFnPc2
<g/>
.	.	kIx.
des	des	k1gNnSc1
Kais	Kaisa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leopold	Leopolda	k1gFnPc2
des	des	k1gNnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
gedruckt	gedruckt	k1gInSc1
bey	bey	k?
Franz	Franz	k1gMnSc1
Gerzabek	Gerzabek	k1gMnSc1
<g/>
,	,	kIx,
1791	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
s.	s.	k?
</s>
<s>
Geschichte	Geschicht	k1gMnSc5
der	drát	k5eAaImRp2nS
Böhmischen	Böhmischen	k2eAgMnSc1d1
Sprache	Sprache	k1gFnSc7
und	und	k?
Litteratur	Litteratura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aus	Aus	k1gFnSc1
dem	dem	k?
ersten	erstno	k1gNnPc2
Bande	band	k1gInSc5
der	drát	k5eAaImRp2nS
neuern	neuern	k1gNnSc4
Abhandlungen	Abhandlungen	k1gInSc1
der	drát	k5eAaImRp2nS
k.	k.	k?
böhm	böhm	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gesellschaft	Gesellschaft	k1gInSc1
der	drát	k5eAaImRp2nS
Wissenschaften	Wissenschaften	k2eAgMnSc1d1
<g/>
;	;	kIx,
jetzt	jetzt	k1gMnSc1
aber	aber	k1gMnSc1
sehr	sehr	k1gMnSc1
und	und	k?
ganz	ganz	k1gMnSc1
umgearbeitet	umgearbeitet	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
bey	bey	k?
Johann	Johann	k1gInSc1
Gottfried	Gottfried	k1gInSc1
Calve	Calev	k1gFnSc2
<g/>
,	,	kIx,
1792	#num#	k4
<g/>
.	.	kIx.
219	#num#	k4
s.	s.	k?
</s>
<s>
Typus	Typus	k?
declinationum	declinationum	k1gNnSc1
linguae	linguae	k1gFnSc1
Bohemicae	Bohemicae	k1gFnSc1
nova	nova	k1gFnSc1
methodo	methoda	k1gFnSc5
dispositarum	dispositarum	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pragae	Praga	k1gInSc2
1793	#num#	k4
<g/>
,	,	kIx,
11	#num#	k4
s.	s.	k?
</s>
<s>
Typus	Typus	k?
declinationum	declinationum	k1gInSc1
ex	ex	k6eAd1
grammatica	grammatic	k2eAgFnSc1d1
Pelcliana	Pelcliana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Grundsätze	Grundsätze	k1gFnSc1
der	drát	k5eAaImRp2nS
böhmischen	böhmischno	k1gNnPc2
Grammatik	Grammatika	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
1795	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
209	#num#	k4
<g/>
–	–	k?
<g/>
246	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Litterarische	Litterarische	k6eAd1
Nachrichten	Nachrichten	k2eAgInSc1d1
von	von	k1gInSc1
einer	einer	k1gMnSc1
auf	auf	k?
Veranlassung	Veranlassung	k1gMnSc1
der	drát	k5eAaImRp2nS
böhm	böhm	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gesellschaft	Gesellschaft	k1gInSc1
der	drát	k5eAaImRp2nS
Wissenschaften	Wissenschaftno	k1gNnPc2
in	in	k?
Jahre	Jahr	k1gInSc5
1792	#num#	k4
unternommenen	unternommenen	k1gInSc4
Reise	Reise	k1gFnSc2
nach	nach	k1gInSc1
Schweden	Schweden	k2eAgInSc1d1
und	und	k?
Rußland	Rußland	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
bei	bei	k?
J.	J.	kA
G.	G.	kA
Calve	Calev	k1gFnSc2
<g/>
,	,	kIx,
1796	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
272	#num#	k4
[	[	kIx(
<g/>
i.	i.	k?
e.	e.	k?
172	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
s.	s.	k?
,	,	kIx,
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
obr	obr	k1gMnSc1
<g/>
.	.	kIx.
příl	příl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Ueber	Ueber	k1gMnSc1
den	den	k1gInSc4
ersten	ersten	k2eAgInSc4d1
Text	text	k1gInSc4
der	drát	k5eAaImRp2nS
Böhmischen	Böhmischen	k1gInSc4
Bibelübersetzung	Bibelübersetzung	k1gInSc1
<g/>
:	:	kIx,
nach	nach	k1gInSc1
den	den	k1gInSc1
ältesten	ältesten	k2eAgInSc1d1
Handschriften	Handschriften	k2eAgInSc1d1
derselben	derselben	k2eAgInSc1d1
<g/>
,	,	kIx,
besonders	besonders	k6eAd1
nach	nach	k1gInSc1
der	drát	k5eAaImRp2nS
Dresdner	Dresdner	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
S.	S.	kA
<g/>
l.	l.	k?
<g/>
:	:	kIx,
s.	s.	k?
<g/>
n.	n.	k?
<g/>
,	,	kIx,
1798	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
.	.	kIx.
27	#num#	k4
s.	s.	k?
</s>
<s>
Drittes	Drittes	k1gMnSc1
<g/>
,	,	kIx,
viertes	viertes	k1gMnSc1
und	und	k?
fünftes	fünftes	k1gMnSc1
Zehend	Zehend	k1gMnSc1
zur	zur	k?
richtigen	richtigen	k1gInSc1
Beurtheilung	Beurtheilunga	k1gFnPc2
des	des	k1gNnSc2
Thamischen	Thamischen	k2eAgInSc4d1
deutsch-böhmischen	deutsch-böhmischen	k2eAgInSc4d1
National-Lexikons	National-Lexikons	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
der	drát	k5eAaImRp2nS
Altstadt	Altstadt	k1gMnSc1
Prag	Prag	k1gMnSc1
in	in	k?
der	drát	k5eAaImRp2nS
Jesuitengasse	Jesuitengasse	k1gFnPc1
Nro	Nro	k1gFnSc7
<g/>
.	.	kIx.
498	#num#	k4
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Herrlische	Herrlische	k1gNnSc1
Buchhandlung	Buchhandlunga	k1gFnPc2
<g/>
,	,	kIx,
1798	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
listy	list	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Dritte	Dritte	k5eAaPmIp2nP
und	und	k?
letzte	letzit	k5eAaPmRp2nP
Antikritik	Antikritik	k1gMnSc1
und	und	k?
Abfertigung	Abfertigung	k1gMnSc1
des	des	k1gNnSc2
Joseph	Joseph	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
mit	mit	k?
allen	allen	k1gInSc1
seinen	seinna	k1gFnPc2
Zehenden	Zehendna	k1gFnPc2
[	[	kIx(
<g/>
et	et	k?
<g/>
]	]	kIx)
<g/>
c.	c.	k?
<g/>
:	:	kIx,
zur	zur	k?
Beurtheilung	Beurtheilung	k1gInSc1
unpartheyischen	unpartheyischen	k2eAgMnSc1d1
Lesern	Lesern	k1gMnSc1
vorgelegt	vorgelegt	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1798	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
s.	s.	k?
</s>
<s>
Neues	Neues	k1gMnSc1
Huelfsmittel	Huelfsmittel	k1gMnSc1
die	die	k?
Russische	Russisch	k1gInPc1
Sprache	Sprache	k1gFnSc1
leichter	leichter	k1gInSc1
zu	zu	k?
verstehen	verstehen	k1gInSc1
<g/>
,	,	kIx,
vorzueglich	vorzueglich	k1gInSc1
fuer	fuera	k1gFnPc2
Boehmen	Boehmen	k1gInSc1
<g/>
,	,	kIx,
zum	zum	k?
Theile	Theila	k1gFnSc3
auch	auch	k1gMnSc1
fuer	fuer	k1gMnSc1
Deutsche	Deutsche	k1gFnSc1
<g/>
:	:	kIx,
Selbst	Selbst	k1gFnSc1
fuer	fuer	k1gMnSc1
Russen	Russen	k1gInSc1
<g/>
,	,	kIx,
die	die	k?
sich	sich	k1gInSc1
in	in	k?
den	den	k1gInSc1
Boehmen	Boehmen	k1gInSc1
verstaendlicher	verstaendlichra	k1gFnPc2
machen	machen	k?
wollen	wollen	k1gInSc1
<g/>
:	:	kIx,
Ein	Ein	k1gMnSc1
zweckmüssiger	zweckmüssiger	k1gMnSc1
Auszug	Auszug	k1gMnSc1
aus	aus	k?
Heyms	Heyms	k1gInSc1
Russischer	Russischra	k1gFnPc2
Sprachlehre	Sprachlehr	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
mit	mit	k?
Schriften	Schriften	k2eAgInSc4d1
der	drát	k5eAaImRp2nS
k.k.	k.k.	k?
Normalschul-Buchdruckerey	Normalschul-Buchdruckere	k1gMnPc7
<g/>
,	,	kIx,
1799	#num#	k4
<g/>
.	.	kIx.
53	#num#	k4
s.	s.	k?
</s>
<s>
Die	Die	k?
Bildsamkeit	Bildsamkeit	k1gInSc1
der	drát	k5eAaImRp2nS
slawischen	slawischen	k1gInSc4
Sprache	Sprach	k1gMnSc2
<g/>
,	,	kIx,
an	an	k?
der	drát	k5eAaImRp2nS
Bildung	Bildung	k1gInSc1
der	drát	k5eAaImRp2nS
Substantive	Substantiv	k1gInSc5
und	und	k?
Adjective	Adjectiv	k1gInSc5
in	in	k?
der	drát	k5eAaImRp2nS
böhmischen	böhmischno	k1gNnPc2
Sprache	Sprache	k1gInSc4
dargestellt	dargestellt	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
In	In	k1gFnSc1
der	drát	k5eAaImRp2nS
Herrlischen	Herrlischna	k1gFnPc2
Buchhandlung	Buchhandlung	k1gInSc1
<g/>
,	,	kIx,
1799	#num#	k4
<g/>
.	.	kIx.
lxviii	lxviie	k1gFnSc3
s.	s.	k?
</s>
<s>
Slovo	slovo	k1gNnSc1
Slauenicum	Slauenicum	k1gInSc1
<g/>
,	,	kIx,
in	in	k?
specie	specie	k1gFnSc1
Czechicum	Czechicum	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praga	Prag	k1gMnSc2
Bohemorum	Bohemorum	k1gInSc4
<g/>
,	,	kIx,
1799	#num#	k4
<g/>
,	,	kIx,
12	#num#	k4
s.	s.	k?
</s>
<s>
Neues	Neues	k1gMnSc1
Hilfsmittel	Hilfsmittel	k1gMnSc1
die	die	k?
russische	russisch	k1gInPc1
Sprache	Sprache	k1gFnSc1
leichter	leichter	k1gInSc1
zu	zu	k?
ferstehen	ferstehen	k1gInSc1
<g/>
,	,	kIx,
vorzűglich	vorzűglich	k1gInSc1
fűr	fűr	k?
Böhmen	Böhmen	k1gInSc1
<g/>
,	,	kIx,
zum	zum	k?
Theile	Theila	k1gFnSc3
auch	auch	k1gMnSc1
fűr	fűr	k?
Deutsche	Deutsch	k1gFnSc2
<g/>
…	…	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Praga	k1gFnPc2
<g/>
,	,	kIx,
1799	#num#	k4
<g/>
,	,	kIx,
53	#num#	k4
s.	s.	k?
</s>
<s>
Ueber	Ueber	k1gInSc1
die	die	k?
ehemaligen	ehemaligen	k1gInSc1
Abbildungen	Abbildungen	k1gInSc1
böhmischer	böhmischra	k1gFnPc2
Regenten	Regentno	k1gNnPc2
<g/>
,	,	kIx,
und	und	k?
ihre	ihre	k1gInSc1
Inschriften	Inschriften	k2eAgInSc1d1
<g/>
,	,	kIx,
in	in	k?
der	drát	k5eAaImRp2nS
prager	prager	k1gInSc4
Königlichen	Königlichen	k2eAgInSc1d1
Burg	Burg	k1gInSc1
<g/>
,	,	kIx,
vor	vor	k1gInSc1
dem	dem	k?
Brande	Brand	k1gInSc5
im	im	k?
Jahre	Jahr	k1gInSc5
1541	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
S.	S.	kA
<g/>
l.	l.	k?
<g/>
:	:	kIx,
s.	s.	k?
<g/>
n.	n.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
--	--	k?
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
.	.	kIx.
8	#num#	k4
s.	s.	k?
</s>
<s>
Entwurf	Entwurf	k1gInSc1
eines	eines	k1gInSc1
Pflanzensystems	Pflanzensystems	k1gInSc1
nach	nach	k1gInSc1
Zahlen	Zahlen	k2eAgInSc1d1
und	und	k?
Verhältnissen	Verhältnissno	k1gNnPc2
<g/>
:	:	kIx,
der	drát	k5eAaImRp2nS
Schlüssel	Schlüssel	k1gInSc1
zur	zur	k?
Vereinigung	Vereinigung	k1gInSc1
der	drát	k5eAaImRp2nS
künstlichen	künstlichen	k1gInSc4
Pflanzensysteme	Pflanzensysteme	k1gMnSc2
mit	mit	k?
der	drát	k5eAaImRp2nS
natürlichen	natürlichna	k1gFnPc2
Methode	Method	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
J.C.	J.C.	k1gFnSc1
Calve	Calev	k1gFnSc2
<g/>
,	,	kIx,
1802	#num#	k4
<g/>
.	.	kIx.
98	#num#	k4
s.	s.	k?
<g/>
,	,	kIx,
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
složený	složený	k2eAgMnSc1d1
l.	l.	k?
obr	obr	k1gMnSc1
<g/>
.	.	kIx.
příl	příl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Deutsch-böhmisches	Deutsch-böhmisches	k1gMnSc1
Wörterbuch	Wörterbuch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Erster	Erstra	k1gFnPc2
Band	banda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
In	In	k1gFnSc1
der	drát	k5eAaImRp2nS
Herrlischen	Herrlischna	k1gFnPc2
Buchhandlung	Buchhandlung	k1gInSc1
<g/>
,	,	kIx,
1802	#num#	k4
<g/>
.	.	kIx.
lxviii	lxviie	k1gFnSc6
<g/>
,	,	kIx,
344	#num#	k4
s.	s.	k?
</s>
<s>
Kritische	Kritischus	k1gMnSc5
Versuche	Versuchus	k1gMnSc5
<g/>
,	,	kIx,
die	die	k?
ältere	älter	k1gMnSc5
böhmische	böhmischus	k1gMnSc5
Geschichte	Geschicht	k1gMnSc5
von	von	k1gInSc1
spätern	spätern	k1gNnSc1
Erdichtungen	Erdichtungen	k1gInSc1
zu	zu	k?
reinigen	reinigen	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Bořiwoy	Bořiwoy	k1gInPc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Taufe	Tauf	k1gInSc5
<g/>
:	:	kIx,
zugleich	zugleich	k1gMnSc1
eine	einus	k1gMnSc5
Probe	Prob	k1gMnSc5
<g/>
,	,	kIx,
wie	wie	k?
man	man	k1gMnSc1
alte	alt	k1gInSc5
Legenden	Legendna	k1gFnPc2
für	für	k?
die	die	k?
Geschichte	Geschicht	k1gInSc5
benutzen	benutzen	k2eAgMnSc1d1
soll	solnout	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
nakladatel	nakladatel	k1gMnSc1
není	být	k5eNaImIp3nS
známý	známý	k2eAgMnSc1d1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1803	#num#	k4
<g/>
.	.	kIx.
111	#num#	k4
s.	s.	k?
</s>
<s>
Kritische	Kritischus	k1gMnSc5
Versuche	Versuchus	k1gMnSc5
<g/>
,	,	kIx,
die	die	k?
ältere	älter	k1gMnSc5
böhmische	böhmischus	k1gMnSc5
Geschichte	Geschicht	k1gMnSc5
von	von	k1gInSc1
spätern	spätern	k1gNnSc1
Erdichtungen	Erdichtungen	k1gInSc1
zu	zu	k?
reinigen	reinigen	k1gInSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
Ludmila	Ludmila	k1gFnSc1
und	und	k?
Drahomir	Drahomir	k1gInSc1
<g/>
:	:	kIx,
forgesetzte	forgesetzit	k5eAaPmRp2nP
Probe	Prob	k1gMnSc5
<g/>
,	,	kIx,
wie	wie	k?
man	man	k1gMnSc1
alte	alt	k1gInSc5
Legenden	Legendna	k1gFnPc2
für	für	k?
die	die	k?
Geschichte	Geschicht	k1gInSc5
benutzen	benutzen	k2eAgMnSc1d1
soll	solnout	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
gedruckt	gedruckt	k1gInSc1
bet	bet	k?
Gottlieb	Gottliba	k1gFnPc2
Haase	Haase	k1gFnSc1
<g/>
,	,	kIx,
1803	#num#	k4
<g/>
.	.	kIx.
87	#num#	k4
s.	s.	k?
</s>
<s>
Glagolitica	Glagolitica	k1gFnSc1
<g/>
:	:	kIx,
ueber	ueber	k1gInSc1
die	die	k?
glagolitische	glagolitisch	k1gInSc2
Literatur	literatura	k1gFnPc2
<g/>
:	:	kIx,
das	das	k?
Alter	Alter	k1gInSc1
der	drát	k5eAaImRp2nS
Bukwitza	Bukwitza	k1gFnSc1
<g/>
:	:	kIx,
ihr	ihr	k?
Muster	Muster	k1gInSc1
<g/>
,	,	kIx,
nach	nach	k1gInSc1
welchem	welch	k1gInSc7
sie	sie	k?
gebildet	gebildet	k1gInSc1
worden	wordna	k1gFnPc2
<g/>
:	:	kIx,
den	den	k1gInSc4
Ursprung	Ursprunga	k1gFnPc2
der	drát	k5eAaImRp2nS
Römisch-Slawischen	Römisch-Slawischen	k1gInSc1
Liturgie	liturgie	k1gFnSc1
<g/>
:	:	kIx,
die	die	k?
Beschaffenheit	Beschaffenheit	k1gInSc1
der	drát	k5eAaImRp2nS
dalmatischen	dalmatischna	k1gFnPc2
Uebersetzung	Uebersetzung	k1gInSc1
<g/>
,	,	kIx,
die	die	k?
man	man	k1gMnSc1
dem	dem	k?
Hieronymus	Hieronymus	k1gMnSc1
zuschrieb	zuschriba	k1gFnPc2
u.	u.	k?
<g/>
s.	s.	k?
<g/>
w.	w.	k?
<g/>
:	:	kIx,
ein	ein	k?
Anhang	Anhang	k1gInSc1
zum	zum	k?
Slavin	Slavina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1807	#num#	k4
<g/>
.	.	kIx.
96	#num#	k4
s.	s.	k?
</s>
<s>
Ausführliches	Ausführliches	k1gMnSc1
Lehrgebäude	Lehrgebäud	k1gInSc5
der	drát	k5eAaImRp2nS
Böhmischen	Böhmischen	k1gInSc4
Sprache	Sprach	k1gMnSc2
<g/>
,	,	kIx,
zur	zur	k?
gründlichen	gründlichen	k2eAgInSc1d1
Erlernung	Erlernung	k1gInSc1
derselben	derselben	k2eAgInSc1d1
für	für	k?
Deutsche	Deutsche	k1gInSc1
<g/>
,	,	kIx,
zur	zur	k?
vollkommenern	vollkommenern	k1gInSc1
Kenntniss	Kenntniss	k1gInSc1
für	für	k?
Böhmen	Böhmen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
bey	bey	k?
Johann	Johann	k1gMnSc1
Herrl	Herrl	k1gMnSc1
<g/>
,	,	kIx,
1809	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
XVIII	XVIII	kA
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
6	#num#	k4
nečíslovaných	číslovaný	k2eNgFnPc2d1
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
399	#num#	k4
s.	s.	k?
</s>
<s>
Altrussische	Altrussischus	k1gMnSc5
Geschichte	Geschicht	k1gMnSc5
nach	nach	k1gInSc1
Nestor	Nestor	k1gMnSc1
<g/>
:	:	kIx,
mit	mit	k?
Rücksicht	Rücksicht	k1gInSc1
aus	aus	k?
Schlözers	Schlözers	k1gInSc1
Russische	Russische	k1gInSc1
Annalen	Annalen	k2eAgInSc1d1
<g/>
,	,	kIx,
die	die	k?
hier	hier	k1gMnSc1
berichtigt	berichtigt	k1gMnSc1
<g/>
,	,	kIx,
ergänzt	ergänzt	k1gMnSc1
und	und	k?
vermehrt	vermehrt	k1gInSc1
werden	werdna	k1gFnPc2
=	=	kIx~
Nestors	Nestorsa	k1gFnPc2
Altrussische	Altrussische	k1gInSc1
Chronik	chronik	k1gMnSc1
von	von	k1gInSc1
der	drát	k5eAaImRp2nS
Sündfluth	Sündfluth	k1gInSc1
bis	bis	k?
zur	zur	k?
Taufe	Tauf	k1gInSc5
Russlands	Russlandsa	k1gFnPc2
unter	unter	k1gMnSc1
Wladimir	Wladimir	k1gMnSc1
(	(	kIx(
<g/>
988	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Berlin	berlina	k1gFnPc2
<g/>
:	:	kIx,
Friedrich	Friedrich	k1gMnSc1
Mauer	Mauer	k1gMnSc1
<g/>
,	,	kIx,
1812	#num#	k4
<g/>
.	.	kIx.
224	#num#	k4
s.	s.	k?
</s>
<s>
Entwurf	Entwurf	k1gInSc1
zu	zu	k?
einem	einem	k6eAd1
allgemeinen	allgemeinen	k2eAgInSc4d1
Etymologikon	Etymologikon	k1gInSc4
der	drát	k5eAaImRp2nS
slawischen	slawischna	k1gFnPc2
Sprachen	Sprachen	k1gInSc1
<g/>
,	,	kIx,
v.	v.	k?
Joseph	Joseph	k1gInSc4
Dobrowsky	Dobrowska	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
bey	bey	k?
Gottlieb	Gottliba	k1gFnPc2
Haase	Haase	k1gFnSc1
<g/>
,	,	kIx,
1813	#num#	k4
<g/>
.	.	kIx.
86	#num#	k4
s.	s.	k?
</s>
<s>
Beyträge	Beyträge	k1gFnSc1
zur	zur	k?
Geschichte	Geschicht	k1gInSc5
des	des	k1gNnSc7
Kelchs	Kelchs	k1gInSc1
in	in	k?
Böhmen	Böhmen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Für	Für	k1gFnSc1
die	die	k?
Abhandlungen	Abhandlungen	k1gInSc1
der	drát	k5eAaImRp2nS
k.	k.	k?
böhmischen	böhmischen	k2eAgInSc4d1
Gesellschaft	Gesellschaft	k1gInSc4
der	drát	k5eAaImRp2nS
Wissenschaften	Wissenschaften	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
gedruckt	gedruckt	k1gInSc1
bei	bei	k?
Gottlieb	Gottliba	k1gFnPc2
Haase	Haase	k1gFnSc1
<g/>
,	,	kIx,
1817	#num#	k4
<g/>
.	.	kIx.
27	#num#	k4
s.	s.	k?
</s>
<s>
Geschichte	Geschicht	k1gMnSc5
der	drát	k5eAaImRp2nS
Böhmischen	Böhmischen	k1gInSc4
Sprache	Sprach	k1gMnSc2
und	und	k?
ältern	ältern	k1gInSc1
Literatur	literatura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ganz	Ganz	k1gInSc1
umgearbeitete	umgearbeiíst	k5eAaPmIp2nP
Ausgabe	Ausgab	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Praga	k1gFnPc2
<g/>
:	:	kIx,
Gottlieb	Gottliba	k1gFnPc2
Haase	Haase	k1gFnSc2
<g/>
,	,	kIx,
1818	#num#	k4
<g/>
.	.	kIx.
408	#num#	k4
s.	s.	k?
</s>
<s>
Lehrgebäude	Lehrgebäude	k6eAd1
der	drát	k5eAaImRp2nS
Böhmischen	Böhmischen	k1gInSc4
Sprache	Sprach	k1gMnSc2
<g/>
:	:	kIx,
zum	zum	k?
Theile	Theila	k1gFnSc3
verkürzt	verkürzt	k1gMnSc1
<g/>
,	,	kIx,
zum	zum	k?
Theile	Theila	k1gFnSc6
umgearbeitet	umgearbeitet	k5eAaPmF,k5eAaImF,k5eAaBmF
und	und	k?
vermehrt	vermehrt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
Bey	Bey	k1gFnSc1
Gottlieb	Gottliba	k1gFnPc2
Haase	Haase	k1gFnSc2
<g/>
,	,	kIx,
1819	#num#	k4
<g/>
.	.	kIx.
xx	xx	k?
<g/>
,	,	kIx,
326	#num#	k4
<g/>
,	,	kIx,
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
s.	s.	k?
</s>
<s>
Deutsch-böhmisches	Deutsch-böhmisches	k1gMnSc1
Wörterbuch	Wörterbuch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Erster	Erster	k1gInSc1
Band	band	k1gInSc4
<g/>
,	,	kIx,
A	a	k8xC
<g/>
–	–	k?
<g/>
K.	K.	kA
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
In	In	k1gFnSc1
der	drát	k5eAaImRp2nS
Herrl	Herrl	k1gInSc1
<g/>
'	'	kIx"
<g/>
schen	schen	k2eAgInSc1d1
Buchhandlung	Buchhandlung	k1gInSc1
<g/>
,	,	kIx,
1821	#num#	k4
<g/>
.	.	kIx.
344	#num#	k4
s.	s.	k?
</s>
<s>
Deutsch-böhmisches	Deutsch-böhmisches	k1gMnSc1
Wörterbuch	Wörterbuch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zweyter	Zweyter	k1gInSc1
Band	band	k1gInSc4
<g/>
,	,	kIx,
L	L	kA
<g/>
–	–	k?
<g/>
Z.	Z.	kA
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
In	In	k1gFnSc1
der	drát	k5eAaImRp2nS
Herrl	Herrl	k1gInSc1
<g/>
'	'	kIx"
<g/>
ischen	ischen	k2eAgInSc1d1
Buchhandlung	Buchhandlung	k1gInSc1
<g/>
,	,	kIx,
1821	#num#	k4
<g/>
.	.	kIx.
482	#num#	k4
s.	s.	k?
</s>
<s>
Ausführliches	Ausführliches	k1gMnSc1
und	und	k?
vollständiges	vollständiges	k1gMnSc1
deutsch-böhmisches	deutsch-böhmisches	k1gMnSc1
synonymisch-phraseologisches	synonymisch-phraseologisches	k1gMnSc1
Wörterbuch	Wörterbuch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Erster	Erster	k1gInSc1
Band	band	k1gInSc4
<g/>
,	,	kIx,
A	a	k8xC
<g/>
–	–	k?
<g/>
K.	K.	kA
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
In	In	k1gFnSc1
der	drát	k5eAaImRp2nS
Cajetan	Cajetan	k1gInSc4
von	von	k1gInSc4
Mayreggschen	Mayreggschna	k1gFnPc2
Buchhandlung	Buchhandlunga	k1gFnPc2
<g/>
,	,	kIx,
1821	#num#	k4
<g/>
.	.	kIx.
344	#num#	k4
s.	s.	k?
</s>
<s>
Ausführliches	Ausführliches	k1gMnSc1
und	und	k?
vollständiges	vollständiges	k1gMnSc1
deutsch-böhmisches	deutsch-böhmisches	k1gMnSc1
synonymisch-phraseologisches	synonymisch-phraseologisches	k1gMnSc1
Wörterbuch	Wörterbuch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zweiter	Zweiter	k1gInSc1
Band	band	k1gInSc4
<g/>
,	,	kIx,
L	L	kA
<g/>
–	–	k?
<g/>
Z.	Z.	kA
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
In	In	k1gFnSc1
der	drát	k5eAaImRp2nS
Cajetan	Cajetan	k1gInSc4
von	von	k1gInSc4
Mayreggschen	Mayreggschna	k1gFnPc2
Buchhandlung	Buchhandlunga	k1gFnPc2
<g/>
,	,	kIx,
1821	#num#	k4
<g/>
.	.	kIx.
482	#num#	k4
s.	s.	k?
</s>
<s>
Josephi	Josephi	k1gNnSc1
Dobrowsky	Dobrowska	k1gFnSc2
Presbyteri	Presbyter	k1gFnSc2
<g/>
,	,	kIx,
AA	AA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
LL	LL	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Et	Et	k1gFnSc1
Philosophiae	Philosophiae	k1gFnSc1
Doctoris	Doctoris	k1gFnSc1
<g/>
,	,	kIx,
Societatis	Societatis	k1gFnSc1
Scientiarum	Scientiarum	k1gInSc1
Bohemicae	Bohemica	k1gInSc2
Atque	Atque	k1gNnSc2
Aliarum	Aliarum	k1gNnSc1
Membri	Membr	k1gFnSc2
Institutiones	Institutionesa	k1gFnPc2
Linguae	Lingua	k1gFnSc2
Slavicae	Slavica	k1gFnSc2
Dialecti	Dialecť	k1gFnSc2
Veteris	Veteris	k1gFnSc2
<g/>
,	,	kIx,
Quae	Quae	k1gFnSc1
Quum	Quum	k1gMnSc1
Apud	Apud	k1gMnSc1
Russos	Russos	k1gMnSc1
<g/>
,	,	kIx,
Serbos	Serbos	k1gMnSc1
Aliosque	Aliosqu	k1gFnSc2
Ritus	ritus	k1gInSc1
Graeci	Graece	k1gFnSc4
<g/>
,	,	kIx,
Tum	Tum	k1gMnSc1
Apud	Apud	k1gMnSc1
Dalmatas	Dalmatas	k1gMnSc1
Glagolitas	Glagolitas	k1gMnSc1
Ritus	ritus	k1gInSc4
Latini	Latin	k2eAgMnPc1d1
Slavos	Slavos	k1gMnSc1
In	In	k1gFnSc2
Libris	Libris	k1gFnSc2
Sacris	Sacris	k1gFnSc2
Obtinet	Obtineta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cum	Cum	k1gFnSc1
Tabulis	Tabulis	k1gFnSc2
Aeri	Aer	k1gFnSc2
Incisis	Incisis	k1gFnSc2
Quatuor	Quatuora	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vindobonae	Vindobonae	k1gInSc1
<g/>
:	:	kIx,
Sumptibus	Sumptibus	k1gInSc1
Et	Et	k1gFnSc2
Typis	Typis	k1gFnSc3
Antonii	Antonie	k1gFnSc3
Schmid	Schmida	k1gFnPc2
<g/>
,	,	kIx,
C.	C.	kA
R.	R.	kA
P.	P.	kA
Typographi	Typographi	k1gNnPc4
<g/>
,	,	kIx,
1822	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
LXVIII	LXVIII	kA
<g/>
,	,	kIx,
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
720	#num#	k4
<g/>
,	,	kIx,
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
s.	s.	k?
<g/>
,	,	kIx,
IV	IV	kA
text	text	k1gInSc1
<g/>
.	.	kIx.
příl	příl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Cyrill	Cyrill	k1gInSc1
und	und	k?
Method	Method	k1gInSc1
der	drát	k5eAaImRp2nS
Slawen	Slawen	k1gInSc4
Apostel	Apostel	k1gInSc4
<g/>
:	:	kIx,
ein	ein	k?
historisch-kritischer	historisch-kritischra	k1gFnPc2
Versuch	Versuch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Praga	k1gFnPc2
<g/>
:	:	kIx,
Gottlieb	Gottliba	k1gFnPc2
Haase	Haase	k1gFnSc2
<g/>
,	,	kIx,
1823	#num#	k4
<g/>
.	.	kIx.
133	#num#	k4
s.	s.	k?
</s>
<s>
Wahl	Wahl	k1gMnSc1
<g/>
,	,	kIx,
Einzug	Einzug	k1gMnSc1
<g/>
,	,	kIx,
und	und	k?
böhmische	böhmische	k1gInSc1
Krönung	Krönung	k1gMnSc1
K.	K.	kA
Ferdinand	Ferdinand	k1gMnSc1
des	des	k1gNnSc7
Ersten	Ersten	k2eAgMnSc1d1
<g/>
:	:	kIx,
Aus	Aus	k1gMnSc1
einer	einer	k1gMnSc1
Handschrift	Handschrift	k1gInSc4
des	des	k1gNnSc2
böhmischen	böhmischna	k1gFnPc2
Museums	Museumsa	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
:	:	kIx,
s.	s.	k?
<g/>
n.	n.	k?
<g/>
,	,	kIx,
1824	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
l.	l.	k?
</s>
<s>
Mährische	Mährischus	k1gMnSc5
Legende	Legend	k1gMnSc5
von	von	k1gInSc1
Cyrill	Cyrill	k1gInSc1
und	und	k?
Method	Method	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Praga	k1gFnPc2
<g/>
:	:	kIx,
Gottlieb	Gottliba	k1gFnPc2
Haase	Haas	k1gMnSc5
Söhne	Söhn	k1gMnSc5
<g/>
,	,	kIx,
1826	#num#	k4
<g/>
.	.	kIx.
124	#num#	k4
s.	s.	k?
</s>
<s>
Dobrowsky	Dobrowska	k1gFnPc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Glagolitica	Glagoliticum	k1gNnPc1
<g/>
:	:	kIx,
ueber	ueber	k1gInSc1
die	die	k?
glagolitische	glagolitisch	k1gInSc2
Literatur	literatura	k1gFnPc2
<g/>
:	:	kIx,
das	das	k?
Alter	Alter	k1gInSc1
der	drát	k5eAaImRp2nS
Bukwitza	Bukwitza	k1gFnSc1
<g/>
:	:	kIx,
ihr	ihr	k?
Muster	Muster	k1gInSc1
<g/>
,	,	kIx,
nach	nach	k1gInSc1
welchem	welch	k1gInSc7
sie	sie	k?
gebildet	gebildet	k1gInSc1
worden	wordna	k1gFnPc2
<g/>
:	:	kIx,
den	den	k1gInSc4
Ursprung	Ursprunga	k1gFnPc2
der	drát	k5eAaImRp2nS
Römisch-Slawischen	Römisch-Slawischen	k1gInSc1
Liturgie	liturgie	k1gFnSc1
<g/>
:	:	kIx,
die	die	k?
Beschaffenheit	Beschaffenheit	k1gInSc1
der	drát	k5eAaImRp2nS
dalmatischen	dalmatischna	k1gFnPc2
Uebersetzung	Uebersetzung	k1gInSc1
<g/>
,	,	kIx,
die	die	k?
man	man	k1gMnSc1
dem	dem	k?
Hieronymus	Hieronymus	k1gMnSc1
zuschrieb	zuschriba	k1gFnPc2
u.	u.	k?
<g/>
s.	s.	k?
<g/>
w.	w.	k?
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
verb.	verb.	k?
und	und	k?
viel	viel	k1gMnSc1
verm	verm	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ausg	Ausg	k1gInSc1
<g/>
.	.	kIx.
von	von	k1gInSc1
Wenceslaw	Wenceslaw	k1gMnSc1
Hanka	Hanka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
Verlag	Verlag	k1gInSc1
der	drát	k5eAaImRp2nS
Cajetan	Cajetan	k1gInSc4
von	von	k1gInSc1
Mayregg	Mayregg	k1gInSc1
<g/>
'	'	kIx"
<g/>
schen	schen	k2eAgInSc1d1
Buchhandlung	Buchhandlung	k1gInSc1
<g/>
,	,	kIx,
1832	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
80	#num#	k4
s.	s.	k?
<g/>
,	,	kIx,
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
l.	l.	k?
obr	obr	k1gMnSc1
<g/>
.	.	kIx.
příl	příl	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
složené	složený	k2eAgFnSc2d1
l.	l.	k?
obr	obr	k1gMnSc1
<g/>
.	.	kIx.
příl	příl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Redakce	redakce	k1gFnSc1
</s>
<s>
Böhmische	Böhmische	k1gFnSc1
Litteratur	Litteratura	k1gFnPc2
auf	auf	k?
das	das	k?
Jahr	Jahr	k1gInSc1
1779	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bandes	Bandesa	k1gFnPc2
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stück	Stücka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
Verlag	Verlag	k1gInSc1
der	drát	k5eAaImRp2nS
Mangoldischen	Mangoldischna	k1gFnPc2
Buchhandlung	Buchhandlung	k1gInSc1
<g/>
,	,	kIx,
1779	#num#	k4
<g/>
.	.	kIx.
82	#num#	k4
s.	s.	k?
</s>
<s>
Böhmische	Böhmische	k1gFnSc1
Litteratur	Litteratura	k1gFnPc2
auf	auf	k?
das	das	k?
Jahr	Jahr	k1gInSc1
1779	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bandes	Bandesa	k1gFnPc2
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stück	Stücka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
Verlag	Verlag	k1gInSc1
der	drát	k5eAaImRp2nS
Mangoldischen	Mangoldischna	k1gFnPc2
Buchhandlung	Buchhandlung	k1gInSc1
<g/>
,	,	kIx,
1779	#num#	k4
<g/>
.	.	kIx.
156	#num#	k4
s.	s.	k?
</s>
<s>
Böhmische	Böhmische	k1gNnSc1
und	und	k?
Mährische	Mährisch	k1gInSc2
Litteratur	Litteratura	k1gFnPc2
auf	auf	k?
das	das	k?
Jahr	Jahr	k1gInSc1
1780	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bandes	Bandesa	k1gFnPc2
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stück	Stücka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
Verlag	Verlag	k1gInSc1
der	drát	k5eAaImRp2nS
Mangoldischen	Mangoldischna	k1gFnPc2
Buchhandlung	Buchhandlung	k1gInSc1
<g/>
,	,	kIx,
1780	#num#	k4
<g/>
.	.	kIx.
63	#num#	k4
s.	s.	k?
</s>
<s>
Böhmische	Böhmische	k1gNnSc1
und	und	k?
Mährische	Mährisch	k1gInSc2
Litteratur	Litteratura	k1gFnPc2
auf	auf	k?
das	das	k?
Jahr	Jahr	k1gInSc1
1780	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bandes	Bandesa	k1gFnPc2
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stück	Stück	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Prag	Prag	k1gInSc1
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
Verlag	Verlag	k1gInSc1
der	drát	k5eAaImRp2nS
Mangoldischen	Mangoldischen	k1gInSc4
Buchhandlung	Buchhandlung	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1780	#num#	k4
<g/>
.	.	kIx.
s.	s.	k?
64	#num#	k4
<g/>
–	–	k?
<g/>
135	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Böhmische	Böhmische	k1gNnSc1
und	und	k?
Mährische	Mährisch	k1gInSc2
Litteratur	Litteratura	k1gFnPc2
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bandes	Bandesa	k1gFnPc2
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stück	Stück	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Prag	Prag	k1gInSc1
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
Verlag	Verlag	k1gInSc1
der	drát	k5eAaImRp2nS
Mangoldischen	Mangoldischen	k1gInSc4
Buchhandlung	Buchhandlung	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
[	[	kIx(
<g/>
1780	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
s.	s.	k?
136	#num#	k4
<g/>
–	–	k?
<g/>
237	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Litterarisches	Litterarisches	k1gInSc1
Magazin	Magazin	k2eAgInSc4d1
von	von	k1gInSc4
Böhmen	Böhmen	k2eAgInSc4d1
und	und	k?
Mähren	Mährna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Erstes	Erstes	k1gMnSc1
Stück	Stück	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
in	in	k?
der	drát	k5eAaImRp2nS
von	von	k1gInSc4
Schönfeldschen	Schönfeldschen	k2eAgMnSc1d1
Handlung	Handlung	k1gMnSc1
<g/>
,	,	kIx,
1786	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
s.	s.	k?
<g/>
,	,	kIx,
s.	s.	k?
3	#num#	k4
<g/>
–	–	k?
<g/>
164	#num#	k4
<g/>
,	,	kIx,
62	#num#	k4
s.	s.	k?
</s>
<s>
Litterarisches	Litterarisches	k1gInSc1
Magazin	Magazin	k2eAgInSc4d1
von	von	k1gInSc4
Böhmen	Böhmen	k2eAgInSc4d1
und	und	k?
Mähren	Mährna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zweytes	Zweytes	k1gMnSc1
Stück	Stück	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
in	in	k?
der	drát	k5eAaImRp2nS
von	von	k1gInSc4
Schönfeldschen	Schönfeldschen	k2eAgMnSc1d1
Handlung	Handlung	k1gMnSc1
<g/>
,	,	kIx,
1786	#num#	k4
<g/>
.	.	kIx.
176	#num#	k4
s.	s.	k?
</s>
<s>
Litterarisches	Litterarisches	k1gInSc1
Magazin	Magazin	k2eAgInSc4d1
von	von	k1gInSc4
Böhmen	Böhmen	k2eAgInSc4d1
und	und	k?
Mähren	Mährna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Drittes	Drittes	k1gMnSc1
Stück	Stück	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
in	in	k?
der	drát	k5eAaImRp2nS
von	von	k1gInSc4
Schönfeldschen	Schönfeldschen	k2eAgMnSc1d1
Handlung	Handlung	k1gMnSc1
<g/>
,	,	kIx,
1787	#num#	k4
<g/>
.	.	kIx.
182	#num#	k4
s.	s.	k?
</s>
<s>
Slawin	Slawin	k1gInSc1
<g/>
:	:	kIx,
Bothschaft	Bothschaft	k1gInSc1
aus	aus	k?
Böhmen	Böhmen	k1gInSc1
an	an	k?
alle	allat	k5eAaPmIp3nS
Slawischen	Slawischen	k2eAgInSc4d1
Völker	Völker	k1gInSc4
<g/>
,	,	kIx,
oder	odrat	k5eAaPmRp2nS
Beiträge	Beiträge	k1gInSc1
zur	zur	k?
Kenntniß	Kenntniß	k1gFnSc2
der	drát	k5eAaImRp2nS
Slawischen	Slawischen	k1gInSc1
Literatur	literatura	k1gFnPc2
nach	nach	k1gInSc4
allen	allen	k1gInSc4
Mundarten	Mundartno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
in	in	k?
der	drát	k5eAaImRp2nS
Herrlschen	Herrlschna	k1gFnPc2
Buchhandlung	Buchhandlung	k1gInSc1
<g/>
,	,	kIx,
1806	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
sv.	sv.	kA
(	(	kIx(
<g/>
479	#num#	k4
s.	s.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Slovanka	Slovanka	k1gFnSc1
<g/>
:	:	kIx,
zur	zur	k?
Kenntniss	Kenntniss	k1gInSc1
der	drát	k5eAaImRp2nS
alten	alten	k1gInSc1
und	und	k?
neuen	uen	k2eNgInSc1d1
slawischen	slawischen	k1gInSc1
Literatur	literatura	k1gFnPc2
<g/>
,	,	kIx,
der	drát	k5eAaImRp2nS
Sprachkunde	Sprachkund	k1gInSc5
nach	nach	k1gInSc4
allen	allen	k1gInSc1
Mundarten	Mundarten	k2eAgInSc1d1
<g/>
,	,	kIx,
der	drát	k5eAaImRp2nS
Geschichte	Geschicht	k1gInSc5
und	und	k?
Alterthümer	Alterthümra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
In	In	k1gFnSc1
der	drát	k5eAaImRp2nS
Herrlschen	Herrlschna	k1gFnPc2
Buchhandlung	Buchhandlung	k1gInSc1
<g/>
,	,	kIx,
1814	#num#	k4
<g/>
–	–	k?
<g/>
1815	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
sv.	sv.	kA
</s>
<s>
Edice	edice	k1gFnSc1
</s>
<s>
Fragmentum	Fragmentum	k1gNnSc1
pragensis	pragensis	k1gFnSc2
evangelii	evangelium	k1gNnPc7
S.	S.	kA
Marci	Marek	k1gMnPc1
vvlgo	vvlgo	k1gMnSc1
avtographi	avtographit	k5eAaImRp2nS,k5eAaPmRp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Edidit	Edidit	k1gFnSc1
<g/>
,	,	kIx,
lectionesque	lectionesque	k1gNnSc1
variantes	variantesa	k1gFnPc2
critice	critice	k1gFnSc2
recensivit	recensivit	k5eAaPmF
Iosephus	Iosephus	k1gInSc4
Dobrowsky	Dobrowska	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pragae	Pragae	k1gFnSc1
<g/>
,	,	kIx,
literis	literis	k1gFnSc1
regiae	regiae	k6eAd1
scholae	scholaat	k5eAaPmIp3nS
normalis	normalis	k1gFnSc1
per	pero	k1gNnPc2
Ioan	Ioano	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adamvm	Adamv	k1gNnSc7
Hagen	Hagna	k1gFnPc2
<g/>
,	,	kIx,
factorem	factor	k1gMnSc7
<g/>
.	.	kIx.
1778	#num#	k4
<g/>
,	,	kIx,
56	#num#	k4
s.	s.	k?
</s>
<s>
De	De	k?
codice	codice	k1gFnSc1
evangeliario	evangeliario	k6eAd1
S.	S.	kA
Marci	Marek	k1gMnPc1
partim	partim	k1gMnSc1
Pragae	Praga	k1gFnPc4
<g/>
,	,	kIx,
partim	partim	k1gInSc4
Venetiis	Venetiis	k1gFnPc2
adservato	adservato	k6eAd1
<g/>
,	,	kIx,
Epistolaris	Epistolaris	k1gInSc4
dissertatio	dissertatio	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pragae	Pragae	k1gFnSc1
<g/>
,	,	kIx,
literis	literis	k1gFnSc1
regiae	regiae	k6eAd1
scholae	scholaat	k5eAaPmIp3nS
normalis	normalis	k1gFnSc1
per	pero	k1gNnPc2
Ioan	Ioano	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adamvm	Adamv	k1gNnSc7
Hagen	Hagna	k1gFnPc2
<g/>
,	,	kIx,
factorem	factor	k1gMnSc7
<g/>
.	.	kIx.
1780	#num#	k4
<g/>
,	,	kIx,
24	#num#	k4
s.	s.	k?
</s>
<s>
Scriptores	Scriptores	k1gInSc1
rerum	rerum	k1gNnSc1
bohemicarum	bohemicarum	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomus	Tomus	k1gInSc1
I	I	kA
<g/>
,	,	kIx,
Cosmae	Cosmae	k1gNnSc7
ecclesiae	ecclesiae	k5eAaImRp2nP
Pragensis	Pragensis	k1gFnSc4
decani	decaň	k1gFnSc3
chronicon	chronicona	k1gFnPc2
Bohemorum	Bohemorum	k1gInSc1
..	..	k?
Tomus	Tomus	k1gInSc1
I	I	kA
<g/>
,	,	kIx,
Cosmae	Cosmae	k1gNnSc7
ecclesiae	ecclesiae	k5eAaImRp2nP
Pragensis	Pragensis	k1gFnSc4
decani	decaň	k1gFnSc3
chronicon	chronicona	k1gFnPc2
Bohemorum	Bohemorum	k1gInSc1
...	...	k?
<g/>
Pragae	Pragae	k1gFnSc1
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
1783	#num#	k4
<g/>
,	,	kIx,
499	#num#	k4
s.	s.	k?
</s>
<s>
Scriptores	Scriptores	k1gInSc1
rerum	rerum	k1gNnSc1
bohemicarum	bohemicarum	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomvs	Tomvs	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Francisci	Francisek	k1gMnPc1
chronicon	chronicona	k1gFnPc2
Pragense	Pragense	k1gFnSc2
<g/>
,	,	kIx,
item	item	k6eAd1
Benessii	Benessie	k1gFnSc4
de	de	k?
Weitmil	Weitmil	k1gMnSc1
chronicon	chronicon	k1gMnSc1
ecclesiae	ecclesiaat	k5eAaPmIp3nS
Pragensis	Pragensis	k1gFnSc4
Tomvs	Tomvs	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Francisci	Francisek	k1gMnPc1
chronicon	chronicona	k1gFnPc2
Pragense	Pragense	k1gFnSc2
<g/>
,	,	kIx,
item	item	k6eAd1
Benessii	Benessie	k1gFnSc4
de	de	k?
Weitmil	Weitmil	k1gMnSc1
chronicon	chronicon	k1gMnSc1
ecclesiae	ecclesiaat	k5eAaPmIp3nS
Pragensis	Pragensis	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pragae	Pragae	k1gNnSc1
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
1784	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
511	#num#	k4
s.	s.	k?
</s>
<s>
Sancti	Sanct	k5eAaImF,k5eAaBmF,k5eAaPmF
Hieronymi	Hierony	k1gFnPc7
presbyteri	presbyter	k1gFnSc2
Epistolae	Epistolae	k1gNnSc2
selectiores	selectiores	k1gMnSc1
de	de	k?
ratione	ration	k1gInSc5
interpretandi	interpretand	k1gMnPc1
scripturam	scripturam	k1gInSc4
<g/>
,	,	kIx,
moribus	moribus	k1gInSc4
<g/>
,	,	kIx,
et	et	k?
disciplina	disciplina	k1gFnSc1
ecclesiastica	ecclesiastica	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pragae	Pragae	k1gNnSc1
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
1785	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Constitutiones	Constitutiones	k1gMnSc1
concilii	concilie	k1gFnSc3
Moguntini	Moguntin	k2eAgMnPc1d1
<g/>
,	,	kIx,
Fritzlariae	Fritzlariae	k1gFnSc1
1244	#num#	k4
<g/>
.	.	kIx.
celebrati	celebrat	k5eAaPmF
<g/>
,	,	kIx,
adnexe	adnex	k1gInSc5
sunt	sunt	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
De	De	k?
sacerdotum	sacerdotum	k1gNnSc4
...	...	k?
coelibatu	coelibat	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nr	Nr	k1gFnSc1
<g/>
.	.	kIx.
125	#num#	k4
<g/>
,	,	kIx,
1787	#num#	k4
<g/>
,	,	kIx,
23	#num#	k4
s.	s.	k?
</s>
<s>
Vita	vit	k2eAgFnSc1d1
Ioannis	Ioannis	k1gFnSc1
de	de	k?
Ienczenstein	Ienczenstein	k1gInSc4
<g/>
,	,	kIx,
archiepiscopi	archiepiscope	k1gFnSc4
Pragensis	Pragensis	k1gFnSc4
tertii	tertie	k1gFnSc4
<g/>
,	,	kIx,
apostolicae	apostolicaat	k5eAaPmIp3nS
sedis	sedis	k1gInSc4
legati	legat	k5eAaBmF,k5eAaImF,k5eAaPmF
secvndi	secvnd	k1gMnPc1
<g/>
,	,	kIx,
postea	postea	k6eAd1
patriarchae	patriarchae	k6eAd1
Alexandrini	Alexandrin	k1gMnPc1
<g/>
,	,	kIx,
olim	olim	k1gMnSc1
episcopi	episcop	k1gFnSc2
Misnensis	Misnensis	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pragae	Pragae	k1gNnSc1
:	:	kIx,
literis	literis	k1gFnSc1
c.	c.	k?
r.	r.	kA
Scholae	Scholae	k1gInSc1
normalis	normalis	k1gFnSc1
1793	#num#	k4
<g/>
,	,	kIx,
60	#num#	k4
s.	s.	k?
</s>
<s>
Einige	Einige	k6eAd1
Varianten	Varianten	k2eAgInSc1d1
aus	aus	k?
slavischen	slavischen	k1gInSc1
Handschriften	Handschriften	k2eAgInSc1d1
des	des	k1gNnSc3
Neuen	Neuen	k2eAgInSc4d1
Testamentes	Testamentes	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
Novvm	Novvm	k1gMnSc1
Testamentvm	Testamentvm	k1gMnSc1
graece	graece	k1gMnSc1
...	...	k?
Griesbach	Griesbach	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nr	Nr	k1gFnSc1
<g/>
.	.	kIx.
148	#num#	k4
<g/>
a.	a.	k?
S.	S.	kA
1	#num#	k4
<g/>
–	–	k?
<g/>
554	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Legenda	legenda	k1gFnSc1
de	de	k?
S.	S.	kA
Ludmila	Ludmila	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Kritische	Kritische	k1gFnSc1
Versuche	Versuche	k1gFnSc1
...	...	k?
I.	I.	kA
Bořiwoy	Bořiwoy	k1gInPc4
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Taufe	Tauf	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
70	#num#	k4
<g/>
–	–	k?
<g/>
105	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Českých	český	k2eAgFnPc2d1
příslowí	příslow	k1gFnPc2
zbírka	zbírk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
nákladem	náklad	k1gInSc7
Jana	Jan	k1gMnSc2
Herrle	Herrl	k1gMnSc2
<g/>
,	,	kIx,
1804	#num#	k4
<g/>
.	.	kIx.
96	#num#	k4
s.	s.	k?
</s>
<s>
Älteste	Ältést	k5eAaPmRp2nP
Legende	Legend	k1gInSc5
vom	vom	k?
heiligen	heiligen	k1gInSc4
Wenzel	Wenzel	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Kritische	Kritische	k1gFnSc1
Versuche	Versuche	k1gFnSc1
...	...	k?
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wenzel	Wenzel	k1gMnSc1
und	und	k?
Boleslaw	Boleslaw	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
53	#num#	k4
<g/>
–	–	k?
<g/>
119	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Legende	Legend	k1gMnSc5
SS	SS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cyrilli	Cyrille	k1gFnSc6
et	et	k?
Methudii	Methudie	k1gFnSc6
Patronum	Patronum	k?
Moraviae	Moraviae	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Mährische	Mährischus	k1gMnSc5
Legende	Legend	k1gMnSc5
....	....	k?
S.	S.	kA
9	#num#	k4
<g/>
–	–	k?
<g/>
51	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Historia	Historium	k1gNnPc1
de	de	k?
expeditione	expedition	k1gInSc5
Friderici	Frideric	k1gMnSc3
Imperatoris	Imperatoris	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pragae	Pragae	k1gNnSc1
:	:	kIx,
Apud	Apud	k1gMnSc1
Cajetanus	Cajetanus	k1gMnSc1
de	de	k?
Mayregg	Mayregg	k1gInSc1
bibliopolam	bibliopolam	k1gInSc1
1827	#num#	k4
<g/>
,	,	kIx,
138	#num#	k4
s.	s.	k?
</s>
<s>
Korespondence	korespondence	k1gFnSc1
</s>
<s>
Briefwechsel	Briefwechsel	k1gInSc1
zwischen	zwischna	k1gFnPc2
Dobrowsky	Dobrowska	k1gFnSc2
und	und	k?
Kopitar	Kopitar	k1gMnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
1808	#num#	k4
<g/>
–	–	k?
<g/>
1828	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Berlin	berlina	k1gFnPc2
<g/>
:	:	kIx,
Weidmann	Weidmann	k1gMnSc1
<g/>
'	'	kIx"
<g/>
sches	sches	k1gMnSc1
Buchhandlung	Buchhandlung	k1gMnSc1
<g/>
,	,	kIx,
1835	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
CVII	CVII	kA
<g/>
,	,	kIx,
751	#num#	k4
s.	s.	k?
<g/>
,	,	kIx,
7	#num#	k4
obr	obr	k1gMnSc1
<g/>
.	.	kIx.
příl	příl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Istočniki	Istočniki	k1gNnSc1
dlja	dlja	k6eAd1
istorii	istorie	k1gFnSc3
slavjanskoj	slavjanskoj	k1gInSc1
filologii	filologie	k1gFnSc3
<g/>
;	;	kIx,
Tom	Tom	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Neue	Neuus	k1gMnSc5
Brife	Brif	k1gMnSc5
von	von	k1gInSc1
Dobrowsky	Dobrowska	k1gFnPc1
<g/>
,	,	kIx,
Kopitar	Kopitar	k1gInSc1
und	und	k?
anderen	anderna	k1gFnPc2
Süd-	Süd-	k1gMnSc1
und	und	k?
Westslaven	Westslaven	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Berlin	berlina	k1gFnPc2
<g/>
:	:	kIx,
Commissionsverlag	Commissionsverlag	k1gInSc1
der	drát	k5eAaImRp2nS
Weidmann	Weidmann	k1gInSc1
<g/>
'	'	kIx"
<g/>
schen	schen	k2eAgInSc1d1
Buchhandlung	Buchhandlung	k1gInSc1
<g/>
,	,	kIx,
1897	#num#	k4
<g/>
.	.	kIx.
cii	cii	k?
<g/>
,	,	kIx,
928	#num#	k4
s.	s.	k?
</s>
<s>
Der	drát	k5eAaImRp2nS
Briefwechsel	Briefwechsel	k1gMnSc1
zwischen	zwischna	k1gFnPc2
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
und	und	k?
Karl	Karl	k1gMnSc1
Gottlob	Gottloba	k1gFnPc2
von	von	k1gInSc4
Anton	Anton	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ed	Ed	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Miloslav	Miloslav	k1gMnSc1
Krbec	krbec	k1gInSc1
<g/>
,	,	kIx,
Věra	Věra	k1gFnSc1
Michálková	Michálková	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Berlin	berlina	k1gFnPc2
<g/>
:	:	kIx,
Akademie	akademie	k1gFnSc1
Verlag	Verlaga	k1gFnPc2
<g/>
,	,	kIx,
1959	#num#	k4
<g/>
.	.	kIx.
77	#num#	k4
s.	s.	k?
<g/>
,	,	kIx,
7	#num#	k4
obr	obr	k1gMnSc1
<g/>
.	.	kIx.
příl	příl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veroeffentlichungen	Veroeffentlichungen	k1gInSc1
des	des	k1gNnSc2
Instituts	Institutsa	k1gFnPc2
für	für	k?
Slawistik	Slawistika	k1gFnPc2
<g/>
;	;	kIx,
Nr	Nr	k1gFnSc1
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
České	český	k2eAgInPc1d1
překlady	překlad	k1gInPc1
</s>
<s>
Studie	studie	k1gFnPc1
a	a	k8xC
monografie	monografie	k1gFnPc1
</s>
<s>
Grammatika	Grammatika	k1gFnSc1
čili	čili	k8xC
Mluwnice	Mluwnice	k1gFnSc1
Českého	český	k2eAgInSc2d1
Gazyka	Gazyek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Djlem	Djlem	k1gInSc4
skrácena	skrácet	k5eAaImNgNnP,k5eAaPmNgNnP
<g/>
,	,	kIx,
djlem	djlem	k6eAd1
rozmnožena	rozmnožen	k2eAgFnSc1d1
od	od	k7c2
Wáclawa	Wáclaw	k1gInSc2
Hanky	Hanka	k1gFnSc2
W	W	kA
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Gost	Gost	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pospjssil	Pospjssil	k1gFnPc2
<g/>
,	,	kIx,
1831	#num#	k4
<g/>
.	.	kIx.
xxvi	xxvi	k1gNnSc1
<g/>
,	,	kIx,
287	#num#	k4
s.	s.	k?
</s>
<s>
Řeč	řeč	k1gFnSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
,	,	kIx,
proslovená	proslovený	k2eAgFnSc1d1
dne	den	k1gInSc2
25	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
1791	#num#	k4
v	v	k7c6
České	český	k2eAgFnSc6d1
učené	učený	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Štenc	Štenc	k1gFnSc1
<g/>
,	,	kIx,
1926	#num#	k4
<g/>
.	.	kIx.
42	#num#	k4
s.	s.	k?
</s>
<s>
Jos	Jos	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobrovského	Dobrovského	k2eAgFnSc1d1
Kritická	kritický	k2eAgFnSc1d1
rozprava	rozprava	k1gFnSc1
o	o	k7c6
legendě	legenda	k1gFnSc6
prokopské	prokopský	k2eAgNnSc1d1
=	=	kIx~
(	(	kIx(
<g/>
Kritische	Kritische	k1gInSc1
Versuche	Versuch	k1gInSc2
die	die	k?
ältere	ältrat	k5eAaPmIp3nS
böhm	böhm	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geschichte	Geschicht	k1gInSc5
von	von	k1gInSc1
späteren	späterna	k1gFnPc2
Erdichtungen	Erdichtungen	k1gInSc4
zu	zu	k?
reinigen	reinigen	k1gInSc1
<g/>
,	,	kIx,
IV	IV	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
:	:	kIx,
ke	k	k7c3
stému	stý	k4xOgNnSc3
výročí	výročí	k1gNnSc3
jeho	jeho	k3xOp3gNnPc2
úmrtí	úmrtí	k1gNnPc2
z	z	k7c2
pozůstalých	pozůstalý	k2eAgInPc2d1
rukopisů	rukopis	k1gInPc2
vydáno	vydat	k5eAaPmNgNnS
péčí	péče	k1gFnSc7
České	český	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
a	a	k8xC
umění	umění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
Nákladem	náklad	k1gInSc7
České	český	k2eAgFnSc2d1
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
a	a	k8xC
umění	umění	k1gNnSc1
<g/>
,	,	kIx,
1929	#num#	k4
<g/>
.	.	kIx.
vii	vii	k?
<g/>
,	,	kIx,
60	#num#	k4
s.	s.	k?
Dobrovský	Dobrovský	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
:	:	kIx,
Kritische	Kritische	k1gFnSc1
Versuche	Versuche	k1gFnSc1
<g/>
,	,	kIx,
die	die	k?
ältere	älter	k1gMnSc5
böhmische	böhmischus	k1gMnSc5
Geschichte	Geschicht	k1gMnSc5
von	von	k1gInSc1
späteren	späterna	k1gFnPc2
Erdichtungen	Erdichtungen	k1gInSc4
zu	zu	k?
reinigen	reinigen	k1gInSc1
<g/>
;	;	kIx,
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
české	český	k2eAgFnSc2d1
řeči	řeč	k1gFnSc2
a	a	k8xC
literatury	literatura	k1gFnSc2
v	v	k7c6
redakcích	redakce	k1gFnPc6
z	z	k7c2
roku	rok	k1gInSc2
1791	#num#	k4
<g/>
,	,	kIx,
1792	#num#	k4
a	a	k8xC
1818	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
Komise	komise	k1gFnSc1
pro	pro	k7c4
vydávání	vydávání	k1gNnSc4
spisů	spis	k1gInPc2
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
při	při	k7c6
Královské	královský	k2eAgFnSc6d1
české	český	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
nauk	nauka	k1gFnPc2
<g/>
,	,	kIx,
1936	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
VIII	VIII	kA
–	–	k?
508	#num#	k4
–	–	k?
[	[	kIx(
<g/>
III	III	kA
<g/>
]	]	kIx)
s.	s.	k?
Dobrovský	Dobrovský	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
:	:	kIx,
Spisy	spis	k1gInPc1
a	a	k8xC
projevy	projev	k1gInPc1
<g/>
;	;	kIx,
Sv.	sv.	kA
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1
Dobrovský	Dobrovský	k1gMnSc1
o	o	k7c6
Nepomucké	Nepomucký	k2eAgFnSc6d1
legendě	legenda	k1gFnSc6
=	=	kIx~
Recension	Recension	k1gInSc1
und	und	k?
kritische	kritischat	k5eAaPmIp3nS
Anmerkungen	Anmerkungen	k1gInSc4
über	über	k1gInSc1
des	des	k1gNnSc1
Reichstädter	Reichstädter	k1gInSc1
Herrn	Herrn	k1gMnSc1
Dechants	Dechantsa	k1gFnPc2
von	von	k1gInSc1
Schönfeld	Schönfeld	k1gMnSc1
am	am	k?
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
May	May	k1gMnSc1
in	in	k?
der	drát	k5eAaImRp2nS
Skalka	Skalka	k1gMnSc1
gehaltene	gehalten	k1gMnSc5
lateinische	lateinischus	k1gMnSc5
Lobrede	Lobred	k1gMnSc5
auf	auf	k?
das	das	k?
Fest	fest	k6eAd1
des	des	k1gNnSc1
heiligen	heiligen	k1gInSc1
Johann	Johann	k1gMnSc1
von	von	k1gInSc1
Nepomuk	Nepomuk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Bartoš	Bartoš	k1gMnSc1
vl	vl	k?
<g/>
.	.	kIx.
n.	n.	k?
<g/>
,	,	kIx,
1940	#num#	k4
<g/>
.	.	kIx.
15	#num#	k4
s.	s.	k?
</s>
<s>
Podrobná	podrobný	k2eAgFnSc1d1
mluvnice	mluvnice	k1gFnSc1
jazyka	jazyk	k1gMnSc2
českého	český	k2eAgMnSc2d1
<g/>
:	:	kIx,
v	v	k7c6
redakcích	redakce	k1gFnPc6
z	z	k7c2
roku	rok	k1gInSc2
1809	#num#	k4
a	a	k8xC
1819	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
sv.	sv.	kA
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
Komise	komise	k1gFnSc1
pro	pro	k7c4
vydávání	vydávání	k1gNnSc4
spisů	spis	k1gInPc2
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
při	při	k7c6
Královské	královský	k2eAgFnSc6d1
české	český	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
nauk	nauka	k1gFnPc2
<g/>
,	,	kIx,
1940	#num#	k4
<g/>
.	.	kIx.
470	#num#	k4
s.	s.	k?
Spisy	spis	k1gInPc1
a	a	k8xC
projevy	projev	k1gInPc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
;	;	kIx,
Sv.	sv.	kA
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Podrobná	podrobný	k2eAgFnSc1d1
mluvnice	mluvnice	k1gFnSc1
jazyka	jazyk	k1gMnSc2
českého	český	k2eAgMnSc2d1
<g/>
:	:	kIx,
v	v	k7c6
redakcích	redakce	k1gFnPc6
z	z	k7c2
roku	rok	k1gInSc2
1809	#num#	k4
a	a	k8xC
1819	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
sv.	sv.	kA
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
Komise	komise	k1gFnSc1
pro	pro	k7c4
vydávání	vydávání	k1gNnSc4
spisů	spis	k1gInPc2
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
při	při	k7c6
Královské	královský	k2eAgFnSc6d1
české	český	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
nauk	nauka	k1gFnPc2
<g/>
,	,	kIx,
1940	#num#	k4
<g/>
.	.	kIx.
471	#num#	k4
<g/>
–	–	k?
<g/>
991	#num#	k4
s.	s.	k?
Spisy	spis	k1gInPc1
a	a	k8xC
projevy	projev	k1gInPc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
;	;	kIx,
Sv.	sv.	kA
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
české	český	k2eAgFnSc2d1
řeči	řeč	k1gFnSc2
a	a	k8xC
literatury	literatura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Benjamin	Benjamin	k1gMnSc1
Jedlička	Jedlička	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydání	vydání	k1gNnSc1
první	první	k4xOgFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
Československý	československý	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
1951	#num#	k4
<g/>
.	.	kIx.
lix	lix	k?
<g/>
,	,	kIx,
187	#num#	k4
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
vi	vi	k?
listů	list	k1gInPc2
obrazových	obrazový	k2eAgFnPc2d1
příloh	příloha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kritická	kritický	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
<g/>
;	;	kIx,
svazek	svazek	k1gInSc1
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Výbor	výbor	k1gInSc1
z	z	k7c2
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
krásné	krásný	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
<g/>
,	,	kIx,
hudby	hudba	k1gFnSc2
a	a	k8xC
umění	umění	k1gNnSc1
<g/>
,	,	kIx,
1953	#num#	k4
<g/>
.	.	kIx.
557	#num#	k4
s.	s.	k?
</s>
<s>
Z	z	k7c2
náboženského	náboženský	k2eAgInSc2d1
odkazu	odkaz	k1gInSc2
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ústřední	ústřední	k2eAgNnSc1d1
církevní	církevní	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
1954	#num#	k4
<g/>
.	.	kIx.
329	#num#	k4
s.	s.	k?
Blahoslav	blahoslavit	k5eAaImRp2nS
<g/>
.	.	kIx.
</s>
<s>
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
:	:	kIx,
autobiografie	autobiografie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Univerzita	univerzita	k1gFnSc1
Palackého	Palackého	k2eAgFnSc1d1
–	–	k?
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
,	,	kIx,
kabinet	kabinet	k1gInSc1
Bedřicha	Bedřich	k1gMnSc2
Václavka	Václavek	k1gMnSc2
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nestr	Nestr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
českých	český	k2eAgMnPc2d1
pikartů	pikart	k1gMnPc2
a	a	k8xC
adamitů	adamita	k1gMnPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
.	.	kIx.
112	#num#	k4
s.	s.	k?
</s>
<s>
Korespondence	korespondence	k1gFnSc1
</s>
<s>
Korrespondence	Korrespondence	k1gFnSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
I	I	kA
<g/>
,	,	kIx,
Vzájemné	vzájemný	k2eAgInPc1d1
dopisy	dopis	k1gInPc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
a	a	k8xC
Fortunáta	Fortunát	k1gMnSc2
Duricha	Durich	k1gMnSc2
z	z	k7c2
let	léto	k1gNnPc2
1778	#num#	k4
<g/>
–	–	k?
<g/>
1800	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
císaře	císař	k1gMnSc2
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
pro	pro	k7c4
vědy	věda	k1gFnPc4
<g/>
,	,	kIx,
slovesnost	slovesnost	k1gFnSc1
a	a	k8xC
umění	umění	k1gNnSc1
<g/>
,	,	kIx,
1895	#num#	k4
<g/>
.	.	kIx.
ix	ix	k?
<g/>
,	,	kIx,
471	#num#	k4
s.	s.	k?
Sbírka	sbírka	k1gFnSc1
pramenův	pramenův	k2eAgInSc4d1
ku	k	k7c3
poznání	poznání	k1gNnSc3
literárního	literární	k2eAgInSc2d1
života	život	k1gInSc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
v	v	k7c6
Slezsku	Slezsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skupina	skupina	k1gFnSc1
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Korrespondence	Korrespondence	k1gFnSc1
a	a	k8xC
cizojazyčné	cizojazyčný	k2eAgInPc1d1
prameny	pramen	k1gInPc1
<g/>
;	;	kIx,
č.	č.	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Korespondence	korespondence	k1gFnSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
II	II	kA
<g/>
,	,	kIx,
Vzájemné	vzájemný	k2eAgInPc1d1
dopisy	dopis	k1gInPc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
a	a	k8xC
Jiřího	Jiří	k1gMnSc2
Samuela	Samuel	k1gMnSc2
Bandtkeho	Bandtke	k1gMnSc2
z	z	k7c2
let	léto	k1gNnPc2
1810	#num#	k4
<g/>
–	–	k?
<g/>
1827	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
Nákladem	náklad	k1gInSc7
České	český	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
císaře	císař	k1gMnSc2
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
pro	pro	k7c4
vědy	věda	k1gFnPc4
<g/>
,	,	kIx,
slovesnost	slovesnost	k1gFnSc1
a	a	k8xC
umění	umění	k1gNnSc1
<g/>
,	,	kIx,
1906	#num#	k4
<g/>
.	.	kIx.
xxxvi	xxxev	k1gFnSc6
<g/>
,	,	kIx,
213	#num#	k4
s.	s.	k?
Sbírka	sbírka	k1gFnSc1
pramenův	pramenův	k2eAgInSc4d1
ku	k	k7c3
poznání	poznání	k1gNnSc3
literárního	literární	k2eAgInSc2d1
života	život	k1gInSc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
v	v	k7c6
Slezsku	Slezsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korrespondence	Korrespondence	k1gFnPc1
a	a	k8xC
cizojazyčné	cizojazyčný	k2eAgInPc1d1
prameny	pramen	k1gInPc1
<g/>
;	;	kIx,
č.	č.	k?
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Korrespondence	Korrespondence	k1gFnSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
III	III	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Vzájemné	vzájemný	k2eAgInPc1d1
dopisy	dopis	k1gInPc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
a	a	k8xC
Josefa	Josef	k1gMnSc2
Valentina	Valentin	k1gMnSc2
Zlobického	Zlobický	k2eAgMnSc2d1
z	z	k7c2
let	léto	k1gNnPc2
1781	#num#	k4
<g/>
–	–	k?
<g/>
1807	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
nákladem	náklad	k1gInSc7
České	český	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
císaře	císař	k1gMnSc2
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
pro	pro	k7c4
vědy	věda	k1gFnPc4
<g/>
,	,	kIx,
slovesnost	slovesnost	k1gFnSc1
a	a	k8xC
umění	umění	k1gNnSc1
<g/>
,	,	kIx,
1908	#num#	k4
<g/>
.	.	kIx.
x	x	k?
<g/>
,	,	kIx,
200	#num#	k4
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sbírka	sbírka	k1gFnSc1
pramenův	pramenův	k2eAgInSc4d1
ku	k	k7c3
poznání	poznání	k1gNnSc3
literárního	literární	k2eAgInSc2d1
života	život	k1gInSc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
v	v	k7c6
Slezsku	Slezsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skupina	skupina	k1gFnSc1
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Korrespondence	Korrespondence	k1gFnSc1
a	a	k8xC
prameny	pramen	k1gInPc1
cizojazyčné	cizojazyčný	k2eAgFnSc2d1
<g/>
;	;	kIx,
číslo	číslo	k1gNnSc1
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Josefa	Josef	k1gMnSc4
Dobrovského	Dobrovského	k2eAgFnSc2d1
korrespondence	korrespondence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
IV	Iva	k1gFnPc2
<g/>
,	,	kIx,
Vzájemné	vzájemný	k2eAgInPc1d1
listy	list	k1gInPc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
a	a	k8xC
Jiřího	Jiří	k1gMnSc2
Ribaye	Ribay	k1gMnSc2
z	z	k7c2
let	léto	k1gNnPc2
1783	#num#	k4
<g/>
–	–	k?
<g/>
1810	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
Nákladem	náklad	k1gInSc7
České	český	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
císaře	císař	k1gMnSc2
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
pro	pro	k7c4
vědy	věda	k1gFnPc4
<g/>
,	,	kIx,
slovesnost	slovesnost	k1gFnSc1
a	a	k8xC
umění	umění	k1gNnSc1
<g/>
,	,	kIx,
1913	#num#	k4
<g/>
.	.	kIx.
xlviii	xlviie	k1gFnSc6
<g/>
,	,	kIx,
343	#num#	k4
s.	s.	k?
Sbírka	sbírka	k1gFnSc1
pramenův	pramenův	k2eAgInSc4d1
ku	k	k7c3
poznání	poznání	k1gNnSc3
literárního	literární	k2eAgInSc2d1
života	život	k1gInSc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
v	v	k7c6
Slezsku	Slezsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skupina	skupina	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Korrespondence	Korrespondence	k1gFnSc1
a	a	k8xC
cizojazyčné	cizojazyčný	k2eAgInPc1d1
prameny	pramen	k1gInPc1
<g/>
;	;	kIx,
č.	č.	k?
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Souborná	souborný	k2eAgNnPc1d1
vydání	vydání	k1gNnPc1
</s>
<s>
Dopisy	dopis	k1gInPc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
s	s	k7c7
Augustinem	Augustin	k1gMnSc7
Helfertem	Helfert	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Královská	královský	k2eAgFnSc1d1
česká	český	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
nauk	nauka	k1gFnPc2
<g/>
,	,	kIx,
1941	#num#	k4
<g/>
.	.	kIx.
178	#num#	k4
s.	s.	k?
Spisy	spis	k1gInPc1
a	a	k8xC
projevy	projev	k1gInPc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
;	;	kIx,
sv.	sv.	kA
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Přednášky	přednáška	k1gFnPc1
o	o	k7c6
praktické	praktický	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
v	v	k7c6
křesťanském	křesťanský	k2eAgNnSc6d1
náboženství	náboženství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
Komise	komise	k1gFnSc1
pro	pro	k7c4
vydávání	vydávání	k1gNnSc4
spisů	spis	k1gInPc2
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
,	,	kIx,
1948	#num#	k4
<g/>
.	.	kIx.
89	#num#	k4
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spisy	spis	k1gInPc1
a	a	k8xC
projevy	projev	k1gInPc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
;	;	kIx,
sv.	sv.	kA
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dopisy	dopis	k1gInPc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
s	s	k7c7
Janem	Jan	k1gMnSc7
Petrem	Petr	k1gMnSc7
Cerronim	Cerronima	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
Komise	komise	k1gFnSc1
pro	pro	k7c4
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spisů	spis	k1gInPc2
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
při	při	k7c6
Král	Král	k1gMnSc1
<g/>
.	.	kIx.
čes.	čes.	k?
společnosti	společnost	k1gFnSc2
nauk	nauka	k1gFnPc2
<g/>
,	,	kIx,
1948	#num#	k4
<g/>
.	.	kIx.
203	#num#	k4
s.	s.	k?
Spisy	spis	k1gInPc1
a	a	k8xC
projevy	projev	k1gInPc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
;	;	kIx,
sv.	sv.	kA
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Cyril	Cyril	k1gMnSc1
a	a	k8xC
Metod	Metod	k1gMnSc1
<g/>
:	:	kIx,
apoštolové	apoštol	k1gMnPc1
slovanští	slovanštit	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
nákladem	náklad	k1gInSc7
Komise	komise	k1gFnSc2
pro	pro	k7c4
vydávání	vydávání	k1gNnSc4
spisů	spis	k1gInPc2
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
při	při	k7c6
Královské	královský	k2eAgFnSc6d1
české	český	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
nauk	nauka	k1gFnPc2
<g/>
,	,	kIx,
1948	#num#	k4
<g/>
.	.	kIx.
223	#num#	k4
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
3	#num#	k4
nečíslované	číslovaný	k2eNgInPc1d1
listy	list	k1gInPc1
obrazových	obrazový	k2eAgFnPc2d1
příloh	příloha	k1gFnPc2
(	(	kIx(
<g/>
převážně	převážně	k6eAd1
složené	složený	k2eAgInPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spisy	spis	k1gInPc1
a	a	k8xC
projevy	projev	k1gInPc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
;	;	kIx,
sv.	sv.	kA
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Rossica	Rossica	k1gFnSc1
:	:	kIx,
srovnání	srovnání	k1gNnSc1
ruské	ruský	k2eAgFnSc2d1
a	a	k8xC
české	český	k2eAgFnSc2d1
řeči	řeč	k1gFnSc2
(	(	kIx(
<g/>
1799	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
pomůcka	pomůcka	k1gFnSc1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
snáze	snadno	k6eAd2
porozuměti	porozumět	k5eAaPmF
ruské	ruský	k2eAgFnSc2d1
řeči	řeč	k1gFnSc2
(	(	kIx(
<g/>
1799	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předmluva	předmluva	k1gFnSc1
k	k	k7c3
Puchmajerově	Puchmajerův	k2eAgFnSc3d1
Mluvnici	mluvnice	k1gFnSc3
ruštiny	ruština	k1gFnSc2
(	(	kIx(
<g/>
1820	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Recense	recense	k1gFnSc1
Slovníku	slovník	k1gInSc2
Ruské	ruský	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
(	(	kIx(
<g/>
1825	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Československá	československý	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
1953	#num#	k4
<g/>
.	.	kIx.
157	#num#	k4
s.	s.	k?
<g/>
,	,	kIx,
8	#num#	k4
obr	obr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spisy	spis	k1gInPc1
a	a	k8xC
projevy	projev	k1gInPc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
;	;	kIx,
Sv.	sv.	kA
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Fragmentum	Fragmentum	k1gNnSc1
Pragense	Pragensa	k1gFnSc3
euangelii	euangelie	k1gFnSc3
S.	S.	kA
Marci	Marek	k1gMnPc1
vulgo	vulgo	k?
autographi	autographi	k1gNnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Československé	československý	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
1953	#num#	k4
<g/>
.	.	kIx.
149	#num#	k4
s.	s.	k?
<g/>
,	,	kIx,
17	#num#	k4
s.	s.	k?
obr	obr	k1gMnSc1
<g/>
.	.	kIx.
příl	příl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spisy	spis	k1gInPc1
a	a	k8xC
projevy	projev	k1gInPc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
;	;	kIx,
sv.	sv.	kA
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c4
zavedení	zavedení	k1gNnSc4
a	a	k8xC
rozšíření	rozšíření	k1gNnSc4
knihtisku	knihtisk	k1gInSc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Československé	československý	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
1954	#num#	k4
<g/>
.	.	kIx.
125	#num#	k4
s.	s.	k?
<g/>
,	,	kIx,
[	[	kIx(
<g/>
xvi	xvi	k?
<g/>
]	]	kIx)
s.	s.	k?
obr	obr	k1gMnSc1
<g/>
.	.	kIx.
příl	příl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spisy	spis	k1gInPc1
a	a	k8xC
projevy	projev	k1gInPc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
;	;	kIx,
sv.	sv.	kA
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Českých	český	k2eAgFnPc2d1
přísloví	přísloví	k1gNnSc2
sbírka	sbírka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydání	vydání	k1gNnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Československé	československý	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
1963	#num#	k4
<g/>
.	.	kIx.
223	#num#	k4
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
viii	viii	k1gNnSc7
stran	strana	k1gFnPc2
obrazových	obrazový	k2eAgFnPc2d1
příloh	příloha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spisy	spis	k1gInPc1
a	a	k8xC
projevy	projev	k1gInPc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
;	;	kIx,
sv.	sv.	kA
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literární	literární	k2eAgNnPc1d1
a	a	k8xC
prozodická	prozodický	k2eAgNnPc1d1
bohemika	bohemikum	k1gNnPc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1974	#num#	k4
<g/>
.	.	kIx.
281	#num#	k4
s.	s.	k?
Spisy	spis	k1gInPc1
a	a	k8xC
projevy	projev	k1gInPc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
;	;	kIx,
sv.	sv.	kA
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Hrob	hrob	k1gInSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
,	,	kIx,
Ústřední	ústřední	k2eAgInSc1d1
hřbitov	hřbitov	k1gInSc1
<g/>
,	,	kIx,
Štýřice	Štýřice	k1gFnSc1
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
</s>
<s>
Vpravo	vpravo	k6eAd1
je	být	k5eAaImIp3nS
hrob	hrob	k1gInSc1
archiváře	archivář	k1gMnSc2
Josefa	Josef	k1gMnSc2
Chytila	Chytil	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Zaniklý	zaniklý	k2eAgInSc4d1
termín	termín	k1gInSc4
krásné	krásný	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
(	(	kIx(
<g/>
schöne	schönout	k5eAaImIp3nS,k5eAaPmIp3nS
Wissenschaften	Wissenschaften	k2eAgMnSc1d1
<g/>
)	)	kIx)
označuje	označovat	k5eAaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
dnes	dnes	k6eAd1
spadá	spadat	k5eAaImIp3nS,k5eAaPmIp3nS
pod	pod	k7c4
tzv.	tzv.	kA
obecnou	obecný	k2eAgFnSc4d1
estetiku	estetika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pojetí	pojetí	k1gNnSc6
Karla	Karel	k1gMnSc2
Heinricha	Heinrich	k1gMnSc2
Seibta	Seibta	k1gMnSc1
tyto	tento	k3xDgFnPc4
vědy	věda	k1gFnPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc7
předmětem	předmět	k1gInSc7
bylo	být	k5eAaImAgNnS
krásno	krásno	k1gNnSc1
a	a	k8xC
dobro	dobro	k1gNnSc1
<g/>
,	,	kIx,
usilovaly	usilovat	k5eAaImAgFnP
o	o	k7c6
předčení	předčení	k1gNnSc6
ostatních	ostatní	k2eAgFnPc2d1
věd	věda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měly	mít	k5eAaImAgInP
se	s	k7c7
stát	stát	k5eAaImF,k5eAaPmF
vrcholnými	vrcholný	k2eAgInPc7d1
obory	obor	k1gInPc7
<g/>
,	,	kIx,
na	na	k7c6
kterých	který	k3yQgInPc6,k3yRgInPc6,k3yIgInPc6
budou	být	k5eAaImBp3nP
všechny	všechen	k3xTgFnPc4
„	„	k?
<g/>
nekrásné	krásný	k2eNgFnPc4d1
<g/>
“	“	k?
vědy	věda	k1gFnSc2
závislé	závislý	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Konkrétně	konkrétně	k6eAd1
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
semitské	semitský	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
měly	mít	k5eAaImAgFnP
nějakou	nějaký	k3yIgFnSc4
souvislost	souvislost	k1gFnSc4
se	s	k7c7
Starým	starý	k2eAgInSc7d1
zákonem	zákon	k1gInSc7
a	a	k8xC
apokryfy	apokryf	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brandl	Brandl	k1gFnSc1
na	na	k7c4
s.	s.	k?
13	#num#	k4
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Dobrovský	Dobrovský	k1gMnSc1
studoval	studovat	k5eAaImAgMnS
hebrejštinu	hebrejština	k1gFnSc4
<g/>
,	,	kIx,
aramejštinu	aramejština	k1gFnSc4
a	a	k8xC
syrštinu	syrština	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Časopis	časopis	k1gInSc1
Orientalische	Orientalisch	k1gFnSc2
und	und	k?
exegetische	exegetischat	k5eAaPmIp3nS
Bibliothek	Bibliothek	k1gInSc1
vydávaný	vydávaný	k2eAgInSc1d1
Johannem	Johann	k1gMnSc7
Davidem	David	k1gMnSc7
Michaelisem	Michaelis	k1gInSc7
(	(	kIx(
<g/>
1717	#num#	k4
<g/>
–	–	k?
<g/>
1791	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
pravidelně	pravidelně	k6eAd1
sledoval	sledovat	k5eAaImAgMnS
<g/>
,	,	kIx,
obsahuje	obsahovat	k5eAaImIp3nS
také	také	k6eAd1
studie	studie	k1gFnPc4
věnované	věnovaný	k2eAgFnSc3d1
arabštině	arabština	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gNnSc1
slavicorum	slavicorum	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
9	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
ČAPEK	Čapek	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hovory	hovor	k1gInPc4
s	s	k7c7
T.G.	T.G.	k1gFnSc7
<g/>
M.	M.	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Fragment	fragment	k1gInSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
306	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
253	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
752	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
80	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	I	kA
<g/>
,	,	kIx,
Bibliographie	Bibliographie	k1gFnSc1
der	drát	k5eAaImRp2nS
Veröffentlichungen	Veröffentlichungen	k1gInSc1
von	von	k1gInSc1
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Miloslav	Miloslava	k1gFnPc2
Krbec	krbec	k1gInSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
Laiske	Laisk	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgNnSc1d1
pedagogické	pedagogický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
1970	#num#	k4
<g/>
.	.	kIx.
258	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Publikace	publikace	k1gFnSc1
filosofické	filosofický	k2eAgFnSc2d1
a	a	k8xC
pedagogické	pedagogický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Univ	Univa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palackého	Palackého	k2eAgMnPc2d1
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Series	Series	k1gInSc1
slavica	slavicum	k1gNnSc2
<g/>
;	;	kIx,
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Л	Л	k?
<g/>
,	,	kIx,
В	В	k?
<g/>
.	.	kIx.
Й	Й	k?
Д	Д	k?
и	и	k?
а	а	k?
и	и	k?
ф	ф	k?
т	т	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
552	#num#	k4
<g/>
–	–	k?
<g/>
559	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
bělorusky	bělorusky	k6eAd1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
BRANDL	BRANDL	kA
<g/>
,	,	kIx,
Vincenc	Vincenc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Jiří	Jiří	k1gMnSc1
Urban	Urban	k1gMnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
upr	upr	k?
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Neklan	Neklan	k1gMnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
254	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
902759	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
9	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
MACHOVEC	Machovec	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
studie	studie	k1gFnSc1
s	s	k7c7
ukázkami	ukázka	k1gFnPc7
z	z	k7c2
díla	dílo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Akropolis	Akropolis	k1gFnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
255	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7304	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
45	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
Kapitola	kapitola	k1gFnSc1
Dobrovského	Dobrovský	k1gMnSc2
život	život	k1gInSc1
a	a	k8xC
dílo	dílo	k1gNnSc1
<g/>
,	,	kIx,
s.	s.	k?
69	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
13	#num#	k4
14	#num#	k4
15	#num#	k4
16	#num#	k4
17	#num#	k4
TRUHLÁŘ	Truhlář	k1gMnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobrovský	Dobrovský	k1gMnSc1
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
naučný	naučný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
J.	J.	kA
Otto	Otto	k1gMnSc1
<g/>
,	,	kIx,
1893	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
729	#num#	k4
<g/>
–	–	k?
<g/>
738.1	738.1	k4
2	#num#	k4
VACEK	Vacek	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duševní	duševní	k2eAgFnSc1d1
nemoc	nemoc	k1gFnSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
a	a	k8xC
slovenská	slovenský	k2eAgFnSc1d1
psychiatrie	psychiatrie	k1gFnSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
104	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
5	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
256	#num#	k4
<g/>
–	–	k?
<g/>
260	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1212	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
383	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Klementinum	Klementinum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
VÍT	Vít	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karl	Karl	k1gMnSc1
Heinrich	Heinrich	k1gMnSc1
Seibt	Seibt	k1gMnSc1
a	a	k8xC
estetika	estetika	k1gFnSc1
napodobování	napodobování	k1gNnSc2
:	:	kIx,
kapitola	kapitola	k1gFnSc1
z	z	k7c2
dějin	dějiny	k1gFnPc2
obecné	obecný	k2eAgFnSc2d1
estetiky	estetika	k1gFnSc2
na	na	k7c6
pražské	pražský	k2eAgFnSc6d1
Karlo-Ferdinandově	Karlo-Ferdinandův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Sborník	sborník	k1gInSc1
prací	práce	k1gFnPc2
filozofické	filozofický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
brněnské	brněnský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
=	=	kIx~
Studia	studio	k1gNnSc2
minora	minora	k1gFnSc1
Facultatis	Facultatis	k1gFnSc1
philosophicae	philosophicae	k1gFnSc1
Universitatis	Universitatis	k1gFnSc1
Brunensis	Brunensis	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
H	H	kA
19	#num#	k4
<g/>
–	–	k?
<g/>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Univerzita	univerzita	k1gFnSc1
J.E.	J.E.	k1gFnSc1
Purkyně	Purkyně	k1gFnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
27	#num#	k4
<g/>
–	–	k?
<g/>
31	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
HLOBIL	HLOBIL	kA
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obhajoby	obhajoba	k1gFnSc2
krásných	krásný	k2eAgFnPc2d1
věd	věda	k1gFnPc2
na	na	k7c6
univerzitách	univerzita	k1gFnPc6
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
Halle	Hallo	k1gNnSc6
a	a	k8xC
Lipsku	Lipsko	k1gNnSc6
(	(	kIx(
<g/>
K.	K.	kA
H.	H.	kA
Seibt	Seibt	k1gMnSc1
<g/>
,	,	kIx,
G.	G.	kA
F.	F.	kA
Meier	Meier	k1gMnSc1
<g/>
,	,	kIx,
Ch	Ch	kA
<g/>
.	.	kIx.
F.	F.	kA
Gellert	Gellert	k1gInSc1
<g/>
,	,	kIx,
J.	J.	kA
Ch	Ch	kA
<g/>
.	.	kIx.
Gottsched	Gottsched	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Estetika	estetika	k1gFnSc1
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
42	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
210	#num#	k4
<g/>
–	–	k?
<g/>
243	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
14	#num#	k4
<g/>
-	-	kIx~
<g/>
1291	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
BRANDL	BRANDL	kA
<g/>
,	,	kIx,
Vincenc	Vincenc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
s.	s.	k?
10	#num#	k4
<g/>
–	–	k?
<g/>
12.1	12.1	k4
2	#num#	k4
3	#num#	k4
TÁBORSKÝ	Táborský	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reformní	reformní	k2eAgMnPc1d1
katolík	katolík	k1gMnSc1
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
37	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
TÁBORSKÝ	Táborský	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reformní	reformní	k2eAgMnPc1d1
katolík	katolík	k1gMnSc1
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
L.	L.	kA
Marek	Marek	k1gMnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
175	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Pontes	Pontes	k1gMnSc1
pragenses	pragenses	k1gMnSc1
<g/>
;	;	kIx,
sv.	sv.	kA
48	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87127	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
37	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Více	hodně	k6eAd2
o	o	k7c6
filosofických	filosofický	k2eAgInPc6d1
a	a	k8xC
nábožensko-etických	nábožensko-etický	k2eAgInPc6d1
názorech	názor	k1gInPc6
Dobrovského	Dobrovský	k1gMnSc2
viz	vidět	k5eAaImRp2nS
zde	zde	k6eAd1
<g/>
:	:	kIx,
DANEŠ	Daneš	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
filozofických	filozofický	k2eAgInPc6d1
a	a	k8xC
nábožensko-etických	nábožensko-etický	k2eAgInPc6d1
postojích	postoj	k1gInPc6
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
GLADKOVA	GLADKOVA	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
;	;	kIx,
SKWARSKA	SKWARSKA	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
32	#num#	k4
<g/>
–	–	k?
<g/>
40	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
NOVÁK	Novák	k1gMnSc1
<g/>
,	,	kIx,
Arne	Arne	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mánes	Mánes	k1gMnSc1
<g/>
,	,	kIx,
1928	#num#	k4
<g/>
.	.	kIx.
85	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Zlatoroh	Zlatoroh	k1gMnSc1
<g/>
;	;	kIx,
sv.	sv.	kA
53	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
8	#num#	k4
<g/>
–	–	k?
<g/>
9	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
TÁBORSKÝ	Táborský	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reformní	reformní	k2eAgMnPc1d1
katolík	katolík	k1gMnSc1
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
41	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BRANDL	BRANDL	kA
<g/>
,	,	kIx,
Vincenc	Vincenc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
s.	s.	k?
12.1	12.1	k4
2	#num#	k4
TÁBORSKÝ	Táborský	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reformní	reformní	k2eAgMnPc1d1
katolík	katolík	k1gMnSc1
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
44	#num#	k4
<g/>
–	–	k?
<g/>
46.1	46.1	k4
2	#num#	k4
3	#num#	k4
BRANDL	BRANDL	kA
<g/>
,	,	kIx,
Vincenc	Vincenc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
s.	s.	k?
12	#num#	k4
<g/>
–	–	k?
<g/>
14	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
TÁBORSKÝ	Táborský	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reformní	reformní	k2eAgMnPc1d1
katolík	katolík	k1gMnSc1
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
42	#num#	k4
<g/>
–	–	k?
<g/>
47	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BRANDL	BRANDL	kA
<g/>
,	,	kIx,
Vincenc	Vincenc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
s.	s.	k?
17	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BRANDL	BRANDL	kA
<g/>
,	,	kIx,
Vincenc	Vincenc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
II	II	kA
<g/>
,	,	kIx,
s.	s.	k?
15	#num#	k4
<g/>
–	–	k?
<g/>
22	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
TÁBORSKÝ	Táborský	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reformní	reformní	k2eAgMnPc1d1
katolík	katolík	k1gMnSc1
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
48	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BRANDL	BRANDL	kA
<g/>
,	,	kIx,
Vincenc	Vincenc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
II-III	II-III	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
15	#num#	k4
<g/>
–	–	k?
<g/>
29	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
TÁBORSKÝ	Táborský	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reformní	reformní	k2eAgMnPc1d1
katolík	katolík	k1gMnSc1
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
49	#num#	k4
<g/>
–	–	k?
<g/>
53	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BRANDL	BRANDL	kA
<g/>
,	,	kIx,
Vincenc	Vincenc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
V	V	kA
<g/>
,	,	kIx,
s.	s.	k?
40	#num#	k4
<g/>
–	–	k?
<g/>
41.1	41.1	k4
2	#num#	k4
POKORNÁ	Pokorná	k1gFnSc1
<g/>
,	,	kIx,
Magdaléna	Magdaléna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
a	a	k8xC
Královská	královský	k2eAgFnSc1d1
česká	český	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
nauk	nauka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
GLADKOVA	GLADKOVA	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
;	;	kIx,
SKWARSKA	SKWARSKA	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
42	#num#	k4
<g/>
–	–	k?
<g/>
52	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BRANDL	BRANDL	kA
<g/>
,	,	kIx,
Vincenc	Vincenc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
:	:	kIx,
Nákladem	náklad	k1gInSc7
Matice	matice	k1gFnSc2
Moravské	moravský	k2eAgInPc1d1
<g/>
,	,	kIx,
1883	#num#	k4
<g/>
.	.	kIx.
s.	s.	k?
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
také	také	k9
z	z	k7c2
<g/>
:	:	kIx,
https://kramerius5.nkp.cz/uuid/uuid:26dcc8c0-9573-11e6-bfc2-001018b5eb5c	https://kramerius5.nkp.cz/uuid/uuid:26dcc8c0-9573-11e6-bfc2-001018b5eb5c	k1gFnSc1
<g/>
↑	↑	k?
ČECHUROVÁ	Čechurová	k1gFnSc1
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čeští	český	k2eAgMnPc1d1
svobodní	svobodný	k2eAgMnPc1d1
zednáři	zednář	k1gMnPc1
ve	v	k7c6
XX	XX	kA
<g/>
.	.	kIx.
století	století	k1gNnPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
527	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
122	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
254	#num#	k4
<g/>
–	–	k?
<g/>
256	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
HLAVAČKA	hlavačka	k1gFnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
doba	doba	k1gFnSc1
aneb	aneb	k?
proměny	proměna	k1gFnSc2
doby	doba	k1gFnSc2
a	a	k8xC
Dobrovského	Dobrovského	k2eAgInPc4d1
limity	limit	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
GLADKOVA	GLADKOVA	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
;	;	kIx,
SKWARSKA	SKWARSKA	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
22	#num#	k4
<g/>
–	–	k?
<g/>
31	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
TÁBORSKÝ	Táborský	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reformní	reformní	k2eAgMnPc1d1
katolík	katolík	k1gMnSc1
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
54	#num#	k4
<g/>
–	–	k?
<g/>
55	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
TÁBORSKÝ	Táborský	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reformní	reformní	k2eAgMnPc1d1
katolík	katolík	k1gMnSc1
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
75	#num#	k4
<g/>
–	–	k?
<g/>
85.1	85.1	k4
2	#num#	k4
TÁBORSKÝ	Táborský	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reformní	reformní	k2eAgMnPc1d1
katolík	katolík	k1gMnSc1
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
83	#num#	k4
<g/>
–	–	k?
<g/>
107	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
DOBROVSKÝ	Dobrovský	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
náboženského	náboženský	k2eAgInSc2d1
odkazu	odkaz	k1gInSc2
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Miloslav	Miloslav	k1gMnSc1
Kaňák	Kaňák	k1gMnSc1
<g/>
;	;	kIx,
překlad	překlad	k1gInSc1
Miloslav	Miloslav	k1gMnSc1
Kaňák	Kaňák	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ústřední	ústřední	k2eAgNnSc1d1
církevní	církevní	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
1954	#num#	k4
<g/>
.	.	kIx.
329	#num#	k4
s.	s.	k?
↑	↑	k?
Prüfung	Prüfung	k1gInSc1
der	drát	k5eAaImRp2nS
Gedanken	Gedankno	k1gNnPc2
über	über	k1gInSc1
die	die	k?
Feldwirthschaften	Feldwirthschaften	k2eAgInSc1d1
der	drát	k5eAaImRp2nS
Landgeistlichen	Landgeistlichen	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
bey	bey	k?
Johann	Johann	k1gMnSc1
Mangoldt	Mangoldt	k1gMnSc1
<g/>
,	,	kIx,
1781	#num#	k4
<g/>
.	.	kIx.
23	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Cit	cit	k1gInSc1
<g/>
.	.	kIx.
překl	překl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josefa	Josef	k1gMnSc2
Táborského	Táborský	k1gMnSc2
<g/>
.	.	kIx.
↑	↑	k?
TÁBORSKÝ	Táborský	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reformní	reformní	k2eAgMnPc1d1
katolík	katolík	k1gMnSc1
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
61	#num#	k4
<g/>
–	–	k?
<g/>
75	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BRANDL	BRANDL	kA
<g/>
,	,	kIx,
Vincenc	Vincenc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
V	V	kA
<g/>
,	,	kIx,
s.	s.	k?
38	#num#	k4
<g/>
–	–	k?
<g/>
44	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Jan	Jan	k1gMnSc1
14	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
MYSLIVEČEK	Mysliveček	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duševní	duševní	k2eAgFnSc1d1
choroba	choroba	k1gFnSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Bratislava	Bratislava	k1gFnSc1
3	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1929	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
825	#num#	k4
<g/>
–	–	k?
<g/>
835	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BRANDL	BRANDL	kA
<g/>
,	,	kIx,
Vincenc	Vincenc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
IX	IX	kA
<g/>
,	,	kIx,
s.	s.	k?
76	#num#	k4
<g/>
–	–	k?
<g/>
95	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BRANDL	BRANDL	kA
<g/>
,	,	kIx,
Vincenc	Vincenc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
VIII	VIII	kA
<g/>
,	,	kIx,
s.	s.	k?
68	#num#	k4
<g/>
–	–	k?
<g/>
75.1	75.1	k4
2	#num#	k4
BRANDL	BRANDL	kA
<g/>
,	,	kIx,
Vincenc	Vincenc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
VIII	VIII	kA
<g/>
,	,	kIx,
s.	s.	k?
68	#num#	k4
<g/>
–	–	k?
<g/>
75.1	75.1	k4
2	#num#	k4
FRYŠČÁK	FRYŠČÁK	kA
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Závěr	závěr	k1gInSc1
Dobrovského	Dobrovského	k2eAgFnSc2d1
studijní	studijní	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
do	do	k7c2
Švédska	Švédsko	k1gNnSc2
a	a	k8xC
Ruska	Rusko	k1gNnSc2
:	:	kIx,
jeho	jeho	k3xOp3gInSc1
pobyt	pobyt	k1gInSc1
v	v	k7c6
Petrohradě	Petrohrad	k1gInSc6
a	a	k8xC
Moskvě	Moskva	k1gFnSc6
(	(	kIx(
<g/>
17	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
1792	#num#	k4
–	–	k?
7	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1793	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
problematika	problematika	k1gFnSc1
autorství	autorství	k1gNnSc2
Slova	slovo	k1gNnSc2
o	o	k7c6
pluku	pluk	k1gInSc6
Igorově	Igorův	k2eAgInSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
GLADKOVA	GLADKOVA	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
;	;	kIx,
SKWARSKA	SKWARSKA	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
215	#num#	k4
<g/>
–	–	k?
<g/>
224	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BRANDL	BRANDL	kA
<g/>
,	,	kIx,
Vincenc	Vincenc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
IX-X	IX-X	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
87	#num#	k4
<g/>
–	–	k?
<g/>
101	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Více	hodně	k6eAd2
o	o	k7c6
Dobrovského	Dobrovského	k2eAgNnSc6d1
setkání	setkání	k1gNnSc6
s	s	k7c7
Goethem	Goeth	k1gInSc7
viz	vidět	k5eAaImRp2nS
zde	zde	k6eAd1
<g/>
:	:	kIx,
ŠVANDRLÍK	ŠVANDRLÍK	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modrý	modrý	k2eAgInSc1d1
abbé	abbé	k1gMnSc1
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
[	[	kIx(
<g/>
online	on-line	k6*tL
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hamelika	Hamelika	k1gFnSc1
:	:	kIx,
historie	historie	k1gFnSc1
Mariánských	mariánský	k2eAgFnPc2d1
Lázní	lázeň	k1gFnPc2
a	a	k8xC
okolí	okolí	k1gNnSc2
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
20181213	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
DOBROVSKÝ	Dobrovský	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Literarischer	Literarischra	k1gFnPc2
Betrug	Betruga	k1gFnPc2
(	(	kIx(
<g/>
Literární	literární	k2eAgInSc1d1
padělek	padělek	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archiv	archiv	k1gInSc1
für	für	k?
Geschichte	Geschicht	k1gMnSc5
<g/>
,	,	kIx,
Statistik	statistika	k1gFnPc2
<g/>
,	,	kIx,
Literatur	literatura	k1gFnPc2
und	und	k?
Kunst	Kunst	k1gMnSc1
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
April	April	k1gInSc1
1824	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
15	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
46	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
260	#num#	k4
<g/>
b.	b.	k?
↑	↑	k?
BRANDL	BRANDL	kA
<g/>
,	,	kIx,
Vincenc	Vincenc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
XI-XIX	XI-XIX	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
105	#num#	k4
<g/>
–	–	k?
<g/>
219	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SEDLÁŘOVÁ	Sedlářová	k1gFnSc1
<g/>
,	,	kIx,
Jitka	Jitka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hugo	Hugo	k1gMnSc1
Franz	Franz	k1gMnSc1
Salm	Salm	k1gMnSc1
:	:	kIx,
průkopník	průkopník	k1gMnSc1
průmyslové	průmyslový	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
:	:	kIx,
železářský	železářský	k2eAgMnSc1d1
magnát	magnát	k1gMnSc1
-	-	kIx~
mecenáš	mecenáš	k1gMnSc1
-	-	kIx~
sběratel	sběratel	k1gMnSc1
-	-	kIx~
lidumil	lidumil	k1gMnSc1
:	:	kIx,
1776	#num#	k4
<g/>
-	-	kIx~
<g/>
1836	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Kroměříži	Kroměříž	k1gFnSc6
<g/>
:	:	kIx,
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
,	,	kIx,
územní	územní	k2eAgFnSc1d1
památková	památkový	k2eAgFnSc1d1
správa	správa	k1gFnSc1
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
162	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87231	#num#	k4
<g/>
-	-	kIx~
<g/>
35	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
120	#num#	k4
<g/>
–	–	k?
<g/>
123	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Hrob	hrob	k1gInSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Portál	portál	k1gInSc1
literárních	literární	k2eAgNnPc2d1
muzeí	muzeum	k1gNnPc2
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2019	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SRŠEŇ	sršeň	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osobnost	osobnost	k1gFnSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
prezentovaná	prezentovaný	k2eAgFnSc1d1
portréty	portrét	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
GLADKOVA	GLADKOVA	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
;	;	kIx,
SKWARSKA	SKWARSKA	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
103	#num#	k4
<g/>
–	–	k?
<g/>
140	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BLÁHOVÁ	Bláhová	k1gFnSc1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
a	a	k8xC
počátky	počátek	k1gInPc1
historické	historický	k2eAgFnSc2d1
kritiky	kritika	k1gFnSc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
GLADKOVA	GLADKOVA	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
;	;	kIx,
SKWARSKA	SKWARSKA	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
143	#num#	k4
<g/>
–	–	k?
<g/>
153	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
PETR	Petr	k1gMnSc1
<g/>
,	,	kIx,
Stanislav	Stanislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
J.	J.	kA
Dobrovský	Dobrovský	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
přínos	přínos	k1gInSc1
k	k	k7c3
výzkumu	výzkum	k1gInSc3
literárních	literární	k2eAgInPc2d1
rukopisů	rukopis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
GLADKOVA	GLADKOVA	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
;	;	kIx,
SKWARSKA	SKWARSKA	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
159	#num#	k4
<g/>
–	–	k?
<g/>
188	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KEENAN	KEENAN	kA
<g/>
,	,	kIx,
Edward	Edward	k1gMnSc1
L.	L.	kA
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
and	and	k?
the	the	k?
origins	origins	k1gInSc1
of	of	k?
the	the	k?
Igor	Igor	k1gMnSc1
<g/>
'	'	kIx"
Tale	Tale	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
:	:	kIx,
Harvard	Harvard	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
©	©	k?
<g/>
2003	#num#	k4
<g/>
.	.	kIx.
xxiii	xxiie	k1gFnSc6
<g/>
,	,	kIx,
541	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Harvard	Harvard	k1gInSc1
series	series	k1gMnSc1
in	in	k?
Ukrainian	Ukrainian	k1gMnSc1
Studies	Studies	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
916458	#num#	k4
<g/>
-	-	kIx~
<g/>
96	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KEENAN	KEENAN	kA
<g/>
,	,	kIx,
Edward	Edward	k1gMnSc1
L.	L.	kA
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
and	and	k?
the	the	k?
С	С	k?
о	о	k?
п	п	k?
И	И	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
GLADKOVA	GLADKOVA	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
;	;	kIx,
SKWARSKA	SKWARSKA	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
207	#num#	k4
<g/>
–	–	k?
<g/>
214	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
BRABEC	Brabec	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masarykovo	Masarykův	k2eAgNnSc4d1
pojetí	pojetí	k1gNnSc4
osobnosti	osobnost	k1gFnSc2
a	a	k8xC
díla	dílo	k1gNnSc2
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
GLADKOVA	GLADKOVA	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
;	;	kIx,
SKWARSKA	SKWARSKA	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
64	#num#	k4
<g/>
–	–	k?
<g/>
72	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
PEČÍRKOVÁ	PEČÍRKOVÁ	kA
<g/>
,	,	kIx,
Jaroslava	Jaroslava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaroslav	Jaroslav	k1gMnSc1
</s>
<s>
Dobrovský	Dobrovský	k1gMnSc1
a	a	k8xC
staročeské	staročeský	k2eAgInPc1d1
biblické	biblický	k2eAgInPc1d1
rukopisy	rukopis	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
GLADKOVA	GLADKOVA	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
;	;	kIx,
SKWARSKA	SKWARSKA	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
281	#num#	k4
<g/>
–	–	k?
<g/>
292	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
PACNEROVÁ	PACNEROVÁ	kA
<g/>
,	,	kIx,
Ludmila	Ludmila	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobrovský	Dobrovský	k1gMnSc1
a	a	k8xC
česká	český	k2eAgFnSc1d1
bible	bible	k1gFnSc1
Hlaholská	hlaholský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
GLADKOVA	GLADKOVA	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
;	;	kIx,
SKWARSKA	SKWARSKA	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
293	#num#	k4
<g/>
–	–	k?
<g/>
297	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SEGERT	SEGERT	kA
<g/>
,	,	kIx,
Stanislav	Stanislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hebraistický	Hebraistický	k2eAgInSc1d1
odkaz	odkaz	k1gInSc1
J.	J.	kA
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
GLADKOVA	GLADKOVA	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
;	;	kIx,
SKWARSKA	SKWARSKA	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
305	#num#	k4
<g/>
–	–	k?
<g/>
308	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SEGERT	SEGERT	kA
<g/>
,	,	kIx,
Stanislav	Stanislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
a	a	k8xC
hebraistika	hebraistika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
Orient	Orient	k1gInSc1
<g/>
.	.	kIx.
1992	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
47	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
65	#num#	k4
<g/>
–	–	k?
<g/>
68	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
29	#num#	k4
<g/>
-	-	kIx~
<g/>
5302	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
HAUPTOVÁ	HAUPTOVÁ	kA
<g/>
,	,	kIx,
Zoe	Zoe	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
a	a	k8xC
počátky	počátek	k1gInPc1
indoevropské	indoevropský	k2eAgFnSc2d1
jazykovědy	jazykověda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
GLADKOVA	GLADKOVA	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
;	;	kIx,
SKWARSKA	SKWARSKA	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
311	#num#	k4
<g/>
–	–	k?
<g/>
318	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MARTI	MARTI	k?
<g/>
,	,	kIx,
Roland	Roland	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Die	Die	k1gFnSc1
„	„	k?
<g/>
Classification	Classification	k1gInSc1
der	drát	k5eAaImRp2nS
slawischen	slawischen	k1gInSc4
Dialecte	Dialecte	k1gMnSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
GLADKOVA	GLADKOVA	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
;	;	kIx,
SKWARSKA	SKWARSKA	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
311	#num#	k4
<g/>
–	–	k?
<g/>
318	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
STEINKE	STEINKE	kA
<g/>
,	,	kIx,
Klaus	Klaus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Der	drát	k5eAaImRp2nS
Stellenwert	Stellenwert	k1gMnSc1
der	drát	k5eAaImRp2nS
südslavischen	südslavischna	k1gFnPc2
Sprachen	Sprachen	k2eAgInSc1d1
in	in	k?
Dobrovskýs	Dobrovskýs	k1gInSc1
Einteilung	Einteilung	k1gInSc1
der	drát	k5eAaImRp2nS
Slavia	Slavia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
GLADKOVA	GLADKOVA	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
;	;	kIx,
SKWARSKA	SKWARSKA	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
338	#num#	k4
<g/>
–	–	k?
<g/>
344	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
HAUSER	HAUSER	kA
<g/>
,	,	kIx,
Přemysl	Přemysl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobrovského	Dobrovského	k2eAgNnSc2d1
pojetí	pojetí	k1gNnSc2
tvoření	tvoření	k1gNnSc2
slov	slovo	k1gNnPc2
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
pojetím	pojetí	k1gNnSc7
dnešním	dnešní	k2eAgNnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
GLADKOVA	GLADKOVA	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
;	;	kIx,
SKWARSKA	SKWARSKA	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
345	#num#	k4
<g/>
–	–	k?
<g/>
353	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GLADKOVA	GLADKOVA	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
jednou	jednou	k6eAd1
ke	k	k7c3
koncepci	koncepce	k1gFnSc3
spisovného	spisovný	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
</s>
<s>
u	u	k7c2
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
GLADKOVA	GLADKOVA	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
;	;	kIx,
SKWARSKA	SKWARSKA	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
373	#num#	k4
<g/>
–	–	k?
<g/>
392	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ULIČNÝ	ULIČNÝ	kA
<g/>
,	,	kIx,
Oldřich	Oldřich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
zavinil	zavinit	k5eAaPmAgMnS
Dobrovský	Dobrovský	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
GLADKOVA	GLADKOVA	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
;	;	kIx,
SKWARSKA	SKWARSKA	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
444	#num#	k4
<g/>
–	–	k?
<g/>
449	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
VINTR	Vintr	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
a	a	k8xC
tehdejší	tehdejší	k2eAgMnPc1d1
vídeňští	vídeňský	k2eAgMnPc1d1
učitelé	učitel	k1gMnPc1
češtiny	čeština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
GLADKOVA	GLADKOVA	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
;	;	kIx,
SKWARSKA	SKWARSKA	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
414	#num#	k4
<g/>
–	–	k?
<g/>
427	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
NEWERKLA	NEWERKLA	kA
<g/>
,	,	kIx,
Stefan	Stefan	k1gMnSc1
M.	M.	kA
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
a	a	k8xC
jazykové	jazykový	k2eAgFnPc1d1
příručky	příručka	k1gFnPc1
výuky	výuka	k1gFnSc2
češtiny	čeština	k1gFnSc2
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
a	a	k8xC
jejím	její	k3xOp3gNnSc6
okolí	okolí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
GLADKOVA	GLADKOVA	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
;	;	kIx,
SKWARSKA	SKWARSKA	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
428	#num#	k4
<g/>
–	–	k?
<g/>
438	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Д	Д	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
713	#num#	k4
<g/>
—	—	k?
<g/>
714	#num#	k4
<g/>
.	.	kIx.
К	К	k?
л	л	k?
э	э	k?
<g/>
,	,	kIx,
Т	Т	k?
2	#num#	k4
<g/>
:	:	kIx,
Г	Г	k?
—	—	k?
З	З	k?
Ш	Ш	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1964	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
20190123	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
713	#num#	k4
<g/>
—	—	k?
<g/>
714	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Л	Л	k?
<g/>
,	,	kIx,
Л	Л	k?
П	П	k?
О	О	k?
в	в	k?
т	т	k?
Й	Й	k?
Д	Д	k?
н	н	k?
р	р	k?
с	с	k?
в	в	k?
Р	Р	k?
в	в	k?
XIX	XIX	kA
в	в	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
GLADKOVA	GLADKOVA	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
;	;	kIx,
SKWARSKA	SKWARSKA	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
459	#num#	k4
<g/>
–	–	k?
<g/>
468	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Г	Г	k?
<g/>
,	,	kIx,
Л	Л	k?
К	К	k?
И	И	k?
Д	Д	k?
в	в	k?
т	т	k?
р	р	k?
у	у	k?
х	х	k?
XIX	XIX	kA
-	-	kIx~
н	н	k?
XX	XX	kA
в	в	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
GLADKOVA	GLADKOVA	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
;	;	kIx,
SKWARSKA	SKWARSKA	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
469	#num#	k4
<g/>
–	–	k?
<g/>
478	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
NAZARENKO	NAZARENKO	kA
<g/>
,	,	kIx,
Lilija	Lilijum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vliv	vliv	k1gInSc1
myšlenek	myšlenka	k1gFnPc2
J.	J.	kA
Dobrovského	Dobrovský	k1gMnSc2
na	na	k7c6
formování	formování	k1gNnSc6
ukrajinského	ukrajinský	k2eAgInSc2d1
spisovného	spisovný	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
XIX	XIX	kA
<g/>
.	.	kIx.
století	století	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
GLADKOVA	GLADKOVA	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
;	;	kIx,
SKWARSKA	SKWARSKA	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
479	#num#	k4
<g/>
–	–	k?
<g/>
488	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
П	П	k?
<g/>
,	,	kIx,
О	О	k?
Л	Л	k?
<g/>
;	;	kIx,
Ч	Ч	k?
<g/>
,	,	kIx,
О	О	k?
P.	P.	kA
Й	Й	k?
Д	Д	k?
и	и	k?
р	р	k?
с	с	k?
ф	ф	k?
в	в	k?
У	У	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
GLADKOVA	GLADKOVA	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
;	;	kIx,
SKWARSKA	SKWARSKA	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
489	#num#	k4
<g/>
–	–	k?
<g/>
496	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
ukrajinsky	ukrajinsky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
VINKLER	Vinkler	k1gMnSc1
<g/>
,	,	kIx,
Jonatan	Jonatan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
recepce	recepce	k1gFnSc1
u	u	k7c2
Slovinců	Slovinec	k1gMnPc2
-	-	kIx~
znaky	znak	k1gInPc1
a	a	k8xC
významy	význam	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
GLADKOVA	GLADKOVA	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
;	;	kIx,
SKWARSKA	SKWARSKA	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
518	#num#	k4
<g/>
–	–	k?
<g/>
527	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
OREL	Orel	k1gMnSc1
<g/>
,	,	kIx,
Irena	Irena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
and	and	k?
His	his	k1gNnSc4
Influence	influence	k1gFnSc2
on	on	k3xPp3gMnSc1
Slovenian	Slovenian	k1gMnSc1
Grammarians	Grammariansa	k1gFnPc2
During	During	k1gInSc4
the	the	k?
First	First	k1gInSc1
Half	halfa	k1gFnPc2
of	of	k?
the	the	k?
19	#num#	k4
<g/>
th	th	k?
Century	Centura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
GLADKOVA	GLADKOVA	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
;	;	kIx,
SKWARSKA	SKWARSKA	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
497	#num#	k4
<g/>
–	–	k?
<g/>
506	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
ŽELE	ŽELE	k?
<g/>
,	,	kIx,
Andreja	Andreja	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Význam	význam	k1gInSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
pro	pro	k7c4
vysvětlení	vysvětlení	k1gNnSc4
některých	některý	k3yIgInPc2
jazykových	jazykový	k2eAgInPc2d1
jevů	jev	k1gInPc2
ve	v	k7c6
slovinštině	slovinština	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
GLADKOVA	GLADKOVA	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
;	;	kIx,
SKWARSKA	SKWARSKA	kA
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
507	#num#	k4
<g/>
–	–	k?
<g/>
517	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ULBRECHTOVÁ-FILIPOVÁ	ULBRECHTOVÁ-FILIPOVÁ	k1gFnSc1
<g/>
,	,	kIx,
Helena	Helena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
und	und	k?
die	die	k?
Lausitz	Lausitz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
540	#num#	k4
<g/>
–	–	k?
<g/>
544	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
SCHUSTER-ŠEWC	SCHUSTER-ŠEWC	k1gMnSc1
<g/>
,	,	kIx,
Heinz	Heinz	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
und	und	k?
die	die	k?
Stellung	Stellung	k1gInSc1
des	des	k1gNnSc1
Sorbischen	Sorbischen	k1gInSc1
im	im	k?
Rahmen	Rahmen	k1gInSc1
der	drát	k5eAaImRp2nS
slawischen	slawischna	k1gFnPc2
Sprachen	Sprachen	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
528	#num#	k4
<g/>
–	–	k?
<g/>
544	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
PÁTA	PÁTA	kA
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
a	a	k8xC
Lužice	Lužice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Česko-lužický	česko-lužický	k2eAgInSc1d1
spolek	spolek	k1gInSc1
Adolf	Adolf	k1gMnSc1
Černý	Černý	k1gMnSc1
<g/>
,	,	kIx,
1929	#num#	k4
<g/>
.	.	kIx.
88	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Česko-lužická	česko-lužický	k2eAgFnSc1d1
knihovnička	knihovnička	k1gFnSc1
<g/>
;	;	kIx,
č.	č.	k?
13	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
RITTER	RITTER	kA
VON	von	k1gInSc1
RITTERSBERG	RITTERSBERG	kA
<g/>
,	,	kIx,
Johann	Johann	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Abbé	abbé	k1gMnSc1
Joseph	Joseph	k1gMnSc1
Dobrowsky	Dobrowska	k1gFnSc2
<g/>
:	:	kIx,
biographische	biographischat	k5eAaPmIp3nS
Skizze	Skizze	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Prag	k1gInSc1
<g/>
:	:	kIx,
C.	C.	kA
W.	W.	kA
Enders	Endersa	k1gFnPc2
<g/>
,	,	kIx,
1829	#num#	k4
<g/>
.	.	kIx.
36	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
PALACKY	PALACKY	kA
<g/>
,	,	kIx,
Franz	Franz	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Joseph	Joseph	k1gInSc1
Dobrowsky	Dobrowska	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Leben	Leben	k1gInSc1
und	und	k?
gelehrtes	gelehrtes	k1gInSc1
Wirken	Wirken	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prag	Praga	k1gFnPc2
<g/>
:	:	kIx,
Gottlieb	Gottliba	k1gFnPc2
Haase	Haas	k1gMnSc5
Söhne	Söhn	k1gMnSc5
<g/>
,	,	kIx,
1833	#num#	k4
<g/>
.	.	kIx.
64	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
BRANDL	BRANDL	kA
<g/>
,	,	kIx,
Vincenc	Vincenc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
:	:	kIx,
Nákladem	náklad	k1gInSc7
Matice	matice	k1gFnSc2
Moravské	moravský	k2eAgInPc1d1
<g/>
,	,	kIx,
1883	#num#	k4
<g/>
.	.	kIx.
296	#num#	k4
<g/>
,	,	kIx,
vi	vi	k?
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
SNEGIREV	SNEGIREV	kA
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
Michajlovič	Michajlovič	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Iosif	Iosif	k1gMnSc1
Dobrovskij	Dobrovskij	k1gMnSc1
<g/>
:	:	kIx,
jego	jego	k1gMnSc1
žizn	žizn	k1gMnSc1
<g/>
'	'	kIx"
<g/>
,	,	kIx,
učeno-literaturnyje	učeno-literaturnýt	k5eAaPmIp3nS
trudy	trud	k1gInPc4
i	i	k9
zaslugi	zaslugi	k6eAd1
dlja	dlj	k2eAgFnSc1d1
slavjanověděnija	slavjanověděnija	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kazan	Kazan	k1gInSc1
<g/>
'	'	kIx"
<g/>
:	:	kIx,
Tipografija	Tipografija	k1gFnSc1
Imperatorskago	Imperatorskago	k6eAd1
Universiteta	Universitet	k2eAgFnSc1d1
<g/>
,	,	kIx,
1884	#num#	k4
<g/>
.	.	kIx.
366	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
TRUHLÁŘ	Truhlář	k1gMnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobrovský	Dobrovský	k1gMnSc1
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
naučný	naučný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
J.	J.	kA
Otto	Otto	k1gMnSc1
<g/>
,	,	kIx,
1893	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
729	#num#	k4
<g/>
–	–	k?
<g/>
738	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KUBKA	KUBKA	kA
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobrovský	Dobrovský	k1gMnSc1
a	a	k8xC
Rusko	Rusko	k1gNnSc1
<g/>
:	:	kIx,
počátky	počátek	k1gInPc1
vztahů	vztah	k1gInPc2
česko-ruských	česko-ruský	k2eAgNnPc2d1
a	a	k8xC
názory	názor	k1gInPc4
Josefa	Josef	k1gMnSc4
Dobrovského	Dobrovský	k1gMnSc2
na	na	k7c4
Rusko	Rusko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Čin	čin	k1gInSc1
<g/>
,	,	kIx,
1926	#num#	k4
<g/>
.	.	kIx.
110	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Knihovna	knihovna	k1gFnSc1
České	český	k2eAgFnSc2d1
mysli	mysl	k1gFnSc2
<g/>
;	;	kIx,
sv.	sv.	kA
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
NOVÁK	Novák	k1gMnSc1
<g/>
,	,	kIx,
Arne	Arne	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mánes	Mánes	k1gMnSc1
<g/>
,	,	kIx,
1928	#num#	k4
<g/>
.	.	kIx.
85	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Zlatoroh	Zlatoroh	k1gMnSc1
<g/>
;	;	kIx,
sv.	sv.	kA
53	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
1753	#num#	k4
<g/>
-	-	kIx~
<g/>
1829	#num#	k4
<g/>
:	:	kIx,
sborník	sborník	k1gInSc1
statí	stať	k1gFnPc2
k	k	k7c3
stému	stý	k4xOgNnSc3
výročí	výročí	k1gNnSc3
smrti	smrt	k1gFnSc2
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Jiří	Jiří	k1gMnSc1
Horák	Horák	k1gMnSc1
<g/>
,	,	kIx,
Matyáš	Matyáš	k1gMnSc1
Murko	Murko	k1gNnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
Weingart	Weingart	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
Výbor	výbor	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
sjezdu	sjezd	k1gInSc2
slovanských	slovanský	k2eAgMnPc2d1
filologů	filolog	k1gMnPc2
<g/>
,	,	kIx,
1929	#num#	k4
<g/>
.	.	kIx.
429	#num#	k4
s.	s.	k?
</s>
<s>
PÁTA	PÁTA	kA
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
a	a	k8xC
Lužice	Lužice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Česko-lužický	česko-lužický	k2eAgInSc1d1
spolek	spolek	k1gInSc1
Adolf	Adolf	k1gMnSc1
Černý	Černý	k1gMnSc1
<g/>
,	,	kIx,
1929	#num#	k4
<g/>
.	.	kIx.
88	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Česko-lužická	česko-lužický	k2eAgFnSc1d1
knihovnička	knihovnička	k1gFnSc1
<g/>
;	;	kIx,
č.	č.	k?
13	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
LUDVÍKOVSKÝ	ludvíkovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobrovského	Dobrovského	k2eAgFnSc1d1
klasická	klasický	k2eAgFnSc1d1
humanita	humanita	k1gFnSc1
<g/>
:	:	kIx,
studie	studie	k1gFnSc1
o	o	k7c6
latinských	latinský	k2eAgInPc6d1
vlivech	vliv	k1gInPc6
na	na	k7c4
počátky	počátek	k1gInPc4
našeho	náš	k3xOp1gNnSc2
obrození	obrození	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Univerzity	univerzita	k1gFnSc2
Komenského	Komenský	k1gMnSc2
<g/>
,	,	kIx,
1933	#num#	k4
<g/>
.	.	kIx.
156	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Spisy	spis	k1gInPc1
filosofické	filosofický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Univerzity	univerzita	k1gFnSc2
Komenského	Komenský	k1gMnSc2
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
<g/>
;	;	kIx,
č.	č.	k?
12	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
VODIČKA	Vodička	k1gMnSc1
<g/>
,	,	kIx,
Felix	Felix	k1gMnSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
české	český	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Literatura	literatura	k1gFnSc1
národního	národní	k2eAgNnSc2d1
obrození	obrození	k1gNnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Československá	československý	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
1960	#num#	k4
<g/>
.	.	kIx.
684	#num#	k4
s.	s.	k?
Kapitola	kapitola	k1gFnSc1
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
99	#num#	k4
<g/>
–	–	k?
<g/>
120	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MACHOVEC	Machovec	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svobodné	svobodný	k2eAgNnSc1d1
slovo	slovo	k1gNnSc1
<g/>
,	,	kIx,
1964	#num#	k4
<g/>
.	.	kIx.
247	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Odkazy	odkaz	k1gInPc1
pokrokových	pokrokový	k2eAgFnPc2d1
osobností	osobnost	k1gFnPc2
naší	náš	k3xOp1gFnSc2
minulosti	minulost	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
PRAŽÁK	Pražák	k1gMnSc1
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
als	als	k?
Hungarist	Hungarist	k1gMnSc1
und	und	k?
Finno-Ugrist	Finno-Ugrist	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Univerzita	univerzita	k1gFnSc1
J.	J.	kA
E.	E.	kA
Purkyně	Purkyně	k1gFnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
1967	#num#	k4
<g/>
.	.	kIx.
188	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Spisy	spis	k1gInPc1
Univerzity	univerzita	k1gFnSc2
J.	J.	kA
E.	E.	kA
Purkyně	Purkyně	k1gFnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
;	;	kIx,
č.	č.	k?
122	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
HEŘMAN	Heřman	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
a	a	k8xC
české	český	k2eAgNnSc4d1
příslovnictví	příslovnictví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ústav	ústav	k1gInSc1
pro	pro	k7c4
čes.	čes.	k?
lit.	lit.	k?
ČSAV	ČSAV	kA
<g/>
,	,	kIx,
1968	#num#	k4
<g/>
.	.	kIx.
156	#num#	k4
s.	s.	k?
</s>
<s>
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	I	kA
<g/>
,	,	kIx,
Bibliographie	Bibliographie	k1gFnSc1
der	drát	k5eAaImRp2nS
Veröffentlichungen	Veröffentlichungen	k1gInSc1
von	von	k1gInSc1
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Miloslav	Miloslava	k1gFnPc2
Krbec	krbec	k1gInSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
Laiske	Laisk	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgNnSc1d1
pedagogické	pedagogický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
1970	#num#	k4
<g/>
.	.	kIx.
258	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Publikace	publikace	k1gFnSc1
filosofické	filosofický	k2eAgFnSc2d1
a	a	k8xC
pedagogické	pedagogický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Univ	Univa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palackého	Palackého	k2eAgMnPc2d1
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Series	Series	k1gInSc1
slavica	slavicum	k1gNnSc2
<g/>
;	;	kIx,
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
SLAVÍK	Slavík	k1gMnSc1
<g/>
,	,	kIx,
Bedřich	Bedřich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
Dobnera	Dobner	k1gMnSc2
k	k	k7c3
Dobrovskému	Dobrovský	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
.	.	kIx.
336	#num#	k4
s.	s.	k?
</s>
<s>
PALACKÝ	Palacký	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
život	život	k1gInSc1
a	a	k8xC
vědecké	vědecký	k2eAgNnSc1d1
působení	působení	k1gNnSc1
<g/>
:	:	kIx,
vylíčené	vylíčený	k2eAgInPc1d1
od	od	k7c2
Františka	František	k1gMnSc2
Palackého	Palacký	k1gMnSc2
<g/>
,	,	kIx,
řádného	řádný	k2eAgMnSc4d1
člena	člen	k1gMnSc4
Královské	královský	k2eAgFnSc2d1
české	český	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
nauk	nauka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Jaromír	Jaromír	k1gMnSc1
Dvořák	Dvořák	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
Šindlerová	Šindlerová	k1gFnSc1
<g/>
;	;	kIx,
překlad	překlad	k1gInSc1
Jaroslava	Jaroslav	k1gMnSc2
Šindlerová	Šindlerová	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Univerzita	univerzita	k1gFnSc1
Palackého	Palacký	k1gMnSc2
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
.	.	kIx.
76	#num#	k4
s.	s.	k?
</s>
<s>
WIRTZ	WIRTZ	kA
<g/>
,	,	kIx,
Markus	Markus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
und	und	k?
die	die	k?
Literatur	literatura	k1gFnPc2
<g/>
:	:	kIx,
frühe	frühat	k5eAaPmIp3nS
bohemistische	bohemistische	k1gFnSc1
Forschung	Forschung	k1gInSc4
zwischen	zwischen	k2eAgInSc1d1
Wissenschaft	Wissenschaft	k1gInSc1
und	und	k?
nationalem	national	k1gInSc7
Auftrag	Auftraga	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dresden	Dresdna	k1gFnPc2
<g/>
:	:	kIx,
Dresden	Dresdna	k1gFnPc2
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
208	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Schriften	Schriften	k2eAgMnSc1d1
zur	zur	k?
Wissenschafts-	Wissenschafts-	k1gMnSc1
und	und	k?
Universitätsgeschichte	Universitätsgeschicht	k1gMnSc5
<g/>
;	;	kIx,
Bd	Bd	k1gMnSc6
<g/>
.	.	kIx.
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
933168	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
KEENAN	KEENAN	kA
<g/>
,	,	kIx,
Edward	Edward	k1gMnSc1
L.	L.	kA
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
and	and	k?
the	the	k?
origins	origins	k1gInSc1
of	of	k?
the	the	k?
Igor	Igor	k1gMnSc1
<g/>
'	'	kIx"
Tale	Tale	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
:	:	kIx,
Harvard	Harvard	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
©	©	k?
<g/>
2003	#num#	k4
<g/>
.	.	kIx.
xxiii	xxiie	k1gFnSc6
<g/>
,	,	kIx,
541	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Harvard	Harvard	k1gInSc1
series	series	k1gMnSc1
in	in	k?
Ukrainian	Ukrainian	k1gMnSc1
Studies	Studies	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
916458	#num#	k4
<g/>
-	-	kIx~
<g/>
96	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
BRANDL	BRANDL	kA
<g/>
,	,	kIx,
Vincenc	Vincenc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc1
Josefa	Josef	k1gMnSc2
Dobrovského	Dobrovský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Jiří	Jiří	k1gMnSc1
Urban	Urban	k1gMnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
upr	upr	k?
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Neklan	Neklan	k1gMnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
254	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
902759	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gInSc1
slavicorum	slavicorum	k1gInSc4
<g/>
:	:	kIx,
příspěvky	příspěvek	k1gInPc4
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
konference	konference	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Vladimír	Vladimír	k1gMnSc1
Vavřínek	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
Gladkova	Gladkův	k2eAgFnSc1d1
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
Skwarska	Skwarska	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
559	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dobrowsky	Dobrowsky	k6eAd1
and	and	k?
the	the	k?
Slavonic	Slavonice	k1gFnPc2
Bible	bible	k1gFnSc1
<g/>
:	:	kIx,
Josef	Josef	k1gMnSc1
Dobrowsky	Dobrowska	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
contribution	contribution	k1gInSc1
to	ten	k3xDgNnSc1
the	the	k?
study	stud	k1gInPc1
of	of	k?
the	the	k?
Slavonic	Slavonice	k1gFnPc2
Bible	bible	k1gFnSc1
together	togethra	k1gFnPc2
with	witha	k1gFnPc2
a	a	k8xC
survey	survea	k1gFnSc2
of	of	k?
precious	precious	k1gMnSc1
scholarship	scholarship	k1gMnSc1
and	and	k?
an	an	k?
appreciation	appreciation	k1gInSc1
of	of	k?
his	his	k1gNnSc1
lasting	lasting	k1gInSc4
influence	influence	k1gFnSc2
on	on	k3xPp3gMnSc1
Slav	slavit	k5eAaImRp2nS
biblical	biblicat	k5eAaPmAgMnS
studies	studies	k1gInSc4
<g/>
:	:	kIx,
a	a	k8xC
companion	companion	k1gInSc1
volume	volum	k1gInSc5
to	ten	k3xDgNnSc1
the	the	k?
proceedings	proceedings	k1gInSc1
of	of	k?
the	the	k?
international	internationat	k5eAaPmAgMnS,k5eAaImAgMnS
conference	conferenec	k1gMnSc2
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
-	-	kIx~
Fundator	Fundator	k1gInSc1
studiorum	studiorum	k1gNnSc1
slavicorum	slavicorum	k1gInSc1
<g/>
,	,	kIx,
Prague	Prague	k1gInSc1
<g/>
,	,	kIx,
10	#num#	k4
<g/>
th-	th-	k?
<g/>
13	#num#	k4
<g/>
th	th	k?
June	jun	k1gMnSc5
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Francis	Francis	k1gFnPc2
J.	J.	kA
Thomson	Thomson	k1gNnSc4
;	;	kIx,
edited	edited	k1gInSc4
by	by	kYmCp3nS
Vladimír	Vladimír	k1gMnSc1
Vavřínek	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
Gladkova	Gladkův	k2eAgFnSc1d1
and	and	k?
Karolína	Karolína	k1gFnSc1
Skwarska	Skwarska	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovanský	slovanský	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
158	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86420	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
MACHOVEC	Machovec	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
studie	studie	k1gFnSc1
s	s	k7c7
ukázkami	ukázka	k1gFnPc7
z	z	k7c2
díla	dílo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Akropolis	Akropolis	k1gFnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
255	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7304	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
45	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
a	a	k8xC
Morava	Morava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Ivo	Ivo	k1gMnSc1
Barteček	Barteček	k1gInSc1
<g/>
,	,	kIx,
Miloslav	Miloslav	k1gMnSc1
Pospíchal	Pospíchal	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Univerzita	univerzita	k1gFnSc1
Palackého	Palacký	k1gMnSc2
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
69	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Verbum	verbum	k1gNnSc1
<g/>
;	;	kIx,
sv.	sv.	kA
17	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
244	#num#	k4
<g/>
-	-	kIx~
<g/>
1371	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
TÁBORSKÝ	Táborský	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reformní	reformní	k2eAgMnPc1d1
katolík	katolík	k1gMnSc1
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
L.	L.	kA
Marek	Marek	k1gMnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
175	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Pontes	Pontes	k1gMnSc1
pragenses	pragenses	k1gMnSc1
<g/>
;	;	kIx,
sv.	sv.	kA
48	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87127	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
SLOVÁČKOVÁ	Slováčková	k1gFnSc1
<g/>
,	,	kIx,
Kateřina	Kateřina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soupis	soupis	k1gInSc1
literatury	literatura	k1gFnSc2
o	o	k7c6
Josefu	Josef	k1gMnSc6
Dobrovském	Dobrovský	k1gMnSc6
(	(	kIx(
<g/>
Dokončení	dokončení	k1gNnPc1
a	a	k8xC
digitální	digitální	k2eAgFnSc1d1
edice	edice	k1gFnSc1
soupisu	soupis	k1gInSc2
Miloslava	Miloslav	k1gMnSc2
Krbce	krbec	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
20190124	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
46	#num#	k4
<g/>
,	,	kIx,
82	#num#	k4
s.	s.	k?
Bakalářská	bakalářský	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
Palackého	Palackého	k2eAgMnPc2d1
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katedra	katedra	k1gFnSc1
bohemistiky	bohemistika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k2eAgFnSc1d1
práce	práce	k1gFnSc1
Jiří	Jiří	k1gMnSc1
Fiala	Fiala	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Česká	český	k2eAgFnSc1d1
slavistika	slavistika	k1gFnSc1
2012-2013	2012-2013	k4
:	:	kIx,
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
a	a	k8xC
problémy	problém	k1gInPc4
současné	současný	k2eAgFnSc2d1
slavistiky	slavistika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Marie	Marie	k1gFnSc1
Sobotková	Sobotková	k1gFnSc1
[	[	kIx(
<g/>
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
262	#num#	k4
s.	s.	k?
Monografické	monografický	k2eAgFnSc2d1
č.	č.	k?
seriálu	seriál	k1gInSc2
<g/>
:	:	kIx,
Slavica	slavicum	k1gNnSc2
litteraria	litterarium	k1gNnSc2
<g/>
,	,	kIx,
ISSN	ISSN	kA
1212-1509	1212-1509	k4
;	;	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
15	#num#	k4
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
č.	č.	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
:	:	kIx,
hungarista	hungarista	k1gMnSc1
a	a	k8xC
ugrofinista	ugrofinista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Richard	Richard	k1gMnSc1
Pražák	Pražák	k1gMnSc1
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
Kovář	Kovář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
.	.	kIx.
122	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
210	#num#	k4
<g/>
-	-	kIx~
<g/>
9266	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Královská	královský	k2eAgFnSc1d1
česká	český	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
nauk	nauka	k1gFnPc2
</s>
<s>
Bohemistika	bohemistika	k1gFnSc1
</s>
<s>
Slavistika	slavistika	k1gFnSc1
</s>
<s>
Dobrovského	Dobrovského	k2eAgFnSc1d1
lípa	lípa	k1gFnSc1
</s>
<s>
Dobrovského	Dobrovského	k2eAgInSc1d1
dub	dub	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Dobrovský	Dobrovský	k1gMnSc1
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
ve	v	k7c6
Slovníku	slovník	k1gInSc6
českých	český	k2eAgMnPc2d1
filosofů	filosof	k1gMnPc2
</s>
<s>
Josef	Josef	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
<g/>
:	:	kIx,
Deutsch-böhmisches	Deutsch-böhmisches	k1gMnSc1
Wörterbuch	Wörterbuch	k1gMnSc1
<g/>
,	,	kIx,
Prag	Prag	k1gMnSc1
1821	#num#	k4
(	(	kIx(
<g/>
elektronická	elektronický	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
slovníku	slovník	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jk	jk	k?
<g/>
0	#num#	k4
<g/>
1022466	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118679961	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0903	#num#	k4
6729	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
50060369	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
56628990	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
50060369	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Jazyk	jazyk	k1gInSc1
|	|	kIx~
Křesťanství	křesťanství	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Literatura	literatura	k1gFnSc1
</s>
