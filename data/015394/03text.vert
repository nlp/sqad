<s>
Star	Star	kA
Trek	Trek	k1gMnSc1
<g/>
:	:	kIx,
Armada	Armada	k1gFnSc1
</s>
<s>
Star	Star	kA
Trek	Trek	k1gMnSc1
<g/>
:	:	kIx,
ArmadaVývojářActivisionVydavatelActivisionPlatformyMicrosoft	ArmadaVývojářActivisionVydavatelActivisionPlatformyMicrosoft	k1gMnSc1
Windowsosobní	Windowsosobní	k2eAgNnSc4d1
počítačDatum	počítačDatum	k1gNnSc4
vydání	vydání	k1gNnSc1
<g/>
29	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2000	#num#	k4
<g/>
Žánrrealtimová	Žánrrealtimový	k2eAgFnSc1d1
strategieHerní	strategieHerní	k2eAgFnSc1d1
módyvideohra	módyvideohra	k1gFnSc1
pro	pro	k7c4
více	hodně	k6eAd2
hráčůvideohra	hráčůvideohra	k1gFnSc1
pro	pro	k7c4
jednoho	jeden	k4xCgMnSc4
hráčeNěkterá	hráčeNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Star	Star	kA
Trek	Trek	k1gMnSc1
<g/>
:	:	kIx,
Armada	Armada	k1gFnSc1
je	být	k5eAaImIp3nS
počítačová	počítačový	k2eAgFnSc1d1
hra	hra	k1gFnSc1
<g/>
,	,	kIx,
real-time	real-timat	k5eAaPmIp3nS
strategie	strategie	k1gFnSc1
od	od	k7c2
společnosti	společnost	k1gFnSc2
Activision	Activision	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
hře	hra	k1gFnSc6
se	se	k3xPyFc4
můžete	moct	k5eAaImIp2nP
postavit	postavit	k5eAaPmF
na	na	k7c4
stranu	strana	k1gFnSc4
Federace	federace	k1gFnSc2
<g/>
,	,	kIx,
Klingonů	Klingon	k1gInPc2
<g/>
,	,	kIx,
Romulanů	Romulan	k1gInPc2
nebo	nebo	k8xC
Borgů	Borg	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
ovšem	ovšem	k9
chcete	chtít	k5eAaImIp2nP
hrát	hrát	k5eAaImF
příběh	příběh	k1gInSc4
<g/>
,	,	kIx,
musíte	muset	k5eAaImIp2nP
je	on	k3xPp3gFnPc4
všechny	všechen	k3xTgFnPc4
vystřídat	vystřídat	k5eAaPmF
(	(	kIx(
<g/>
čtyři	čtyři	k4xCgFnPc1
kampaně	kampaň	k1gFnPc1
v	v	k7c6
uvedeném	uvedený	k2eAgNnSc6d1
pořadí	pořadí	k1gNnSc6
ras	rasa	k1gFnPc2
a	a	k8xC
poté	poté	k6eAd1
následuje	následovat	k5eAaImIp3nS
bonusová	bonusový	k2eAgFnSc1d1
kampaň	kampaň	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Herní	herní	k2eAgInSc1d1
princip	princip	k1gInSc1
</s>
<s>
Ve	v	k7c6
hře	hra	k1gFnSc6
hráč	hráč	k1gMnSc1
staví	stavit	k5eAaImIp3nS,k5eAaBmIp3nS,k5eAaPmIp3nS
výzkumná	výzkumný	k2eAgNnPc4d1
střediska	středisko	k1gNnPc4
<g/>
,	,	kIx,
těžební	těžební	k2eAgFnPc4d1
stanice	stanice	k1gFnPc4
i	i	k8xC
továrny	továrna	k1gFnPc4
na	na	k7c4
vesmírná	vesmírný	k2eAgNnPc4d1
plavidla	plavidlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Suroviny	surovina	k1gFnPc1
<g/>
,	,	kIx,
díky	díky	k7c3
kterým	který	k3yIgFnPc3,k3yRgFnPc3,k3yQgFnPc3
může	moct	k5eAaImIp3nS
stavět	stavět	k5eAaImF
lodě	loď	k1gFnPc4
a	a	k8xC
stanice	stanice	k1gFnPc4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
tři	tři	k4xCgInPc4
–	–	k?
dilithium	dilithium	k1gNnSc4
(	(	kIx(
<g/>
získává	získávat	k5eAaImIp3nS
z	z	k7c2
měsíců	měsíc	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
latinium	latinium	k1gNnSc1
(	(	kIx(
<g/>
z	z	k7c2
mlhovin	mlhovina	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
kov	kov	k1gInSc1
(	(	kIx(
<g/>
těží	těžet	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
planet	planeta	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
osazení	osazení	k1gNnSc3
lodí	loď	k1gFnPc2
a	a	k8xC
vesmírných	vesmírný	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
také	také	k9
dostatek	dostatek	k1gInSc4
personálu	personál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc4
si	se	k3xPyFc3
hráč	hráč	k1gMnSc1
zajistí	zajistit	k5eAaPmIp3nS
kolonizováním	kolonizování	k1gNnSc7
nových	nový	k2eAgFnPc2d1
planet	planeta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Tímto	tento	k3xDgInSc7
principem	princip	k1gInSc7
hra	hra	k1gFnSc1
připomíná	připomínat	k5eAaImIp3nS
ostatní	ostatní	k2eAgFnSc1d1
real-time	real-timat	k5eAaPmIp3nS
strategie	strategie	k1gFnSc1
jako	jako	k8xC,k8xS
třeba	třeba	k6eAd1
StarCraft	StarCraft	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejvýraznější	výrazný	k2eAgFnPc4d3
odlišnosti	odlišnost	k1gFnPc4
patří	patřit	k5eAaImIp3nS
vesmírné	vesmírný	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
(	(	kIx(
<g/>
tedy	tedy	k9
žádná	žádný	k3yNgFnSc1
„	„	k?
<g/>
podlaha	podlaha	k1gFnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
omezený	omezený	k2eAgInSc1d1
pohyb	pohyb	k1gInSc1
v	v	k7c6
třetím	třetí	k4xOgInSc6
rozměru	rozměr	k1gInSc6
(	(	kIx(
<g/>
tedy	tedy	k9
nahoru	nahoru	k6eAd1
a	a	k8xC
dolu	dol	k1gInSc2
<g/>
)	)	kIx)
spolu	spolu	k6eAd1
s	s	k7c7
možností	možnost	k1gFnSc7
velmi	velmi	k6eAd1
volně	volně	k6eAd1
měnit	měnit	k5eAaImF
úhel	úhel	k1gInSc4
kamery	kamera	k1gFnSc2
(	(	kIx(
<g/>
ve	v	k7c6
dvou	dva	k4xCgInPc6
módech	mód	k1gInPc6
<g/>
,	,	kIx,
taktickém	taktický	k2eAgMnSc6d1
a	a	k8xC
filmovém	filmový	k2eAgInSc6d1
<g/>
)	)	kIx)
a	a	k8xC
z	z	k7c2
herních	herní	k2eAgInPc2d1
mechanismů	mechanismus	k1gInPc2
možnost	možnost	k1gFnSc4
zajímání	zajímání	k1gNnSc4
a	a	k8xC
obsazování	obsazování	k1gNnSc4
nepřátelských	přátelský	k2eNgFnPc2d1
lodí	loď	k1gFnPc2
a	a	k8xC
budov	budova	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Součástí	součást	k1gFnSc7
hry	hra	k1gFnSc2
jsou	být	k5eAaImIp3nP
i	i	k9
„	„	k?
<g/>
videosekvence	videosekvence	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
„	„	k?
<g/>
exteriérové	exteriérový	k2eAgFnSc2d1
<g/>
“	“	k?
záběry	záběr	k1gInPc1
jsou	být	k5eAaImIp3nP
ovšem	ovšem	k9
kreslené	kreslený	k2eAgInPc1d1
přímo	přímo	k6eAd1
v	v	k7c6
engine	enginout	k5eAaPmIp3nS
hry	hra	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
po	po	k7c4
dobu	doba	k1gFnSc4
jejich	jejich	k3xOp3gNnSc2
přehrávání	přehrávání	k1gNnSc2
není	být	k5eNaImIp3nS
blokována	blokován	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
vám	vy	k3xPp2nPc3
tedy	tedy	k9
povést	povést	k5eAaPmF
děj	děj	k1gInSc4
videosekvence	videosekvence	k1gFnSc2
narušit	narušit	k5eAaPmF
automatickou	automatický	k2eAgFnSc7d1
reakcí	reakce	k1gFnSc7
dobře	dobře	k6eAd1
umístěné	umístěný	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
případě	případ	k1gInSc6
kampaně	kampaň	k1gFnSc2
za	za	k7c4
Federaci	federace	k1gFnSc4
je	být	k5eAaImIp3nS
hra	hra	k1gFnSc1
doplněna	doplněn	k2eAgFnSc1d1
dabingem	dabing	k1gInSc7
herců	herec	k1gMnPc2
ze	z	k7c2
seriálu	seriál	k1gInSc2
Star	Star	kA
Trek	Trek	k1gMnSc1
<g/>
:	:	kIx,
Nová	nový	k2eAgFnSc1d1
generace	generace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
hru	hra	k1gFnSc4
navazuje	navazovat	k5eAaImIp3nS
druhý	druhý	k4xOgInSc1
díl	díl	k1gInSc1
s	s	k7c7
názvem	název	k1gInSc7
Star	Star	kA
Trek	Trek	k1gInSc1
<g/>
:	:	kIx,
Armada	Armada	k1gFnSc1
II	II	kA
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
přidává	přidávat	k5eAaImIp3nS
mezi	mezi	k7c4
hratelné	hratelný	k2eAgFnPc4d1
rasy	rasa	k1gFnPc4
Cardassiany	Cardassiana	k1gFnSc2
a	a	k8xC
záhadný	záhadný	k2eAgInSc4d1
Druh	druh	k1gInSc4
8472	#num#	k4
<g/>
,	,	kIx,
původem	původ	k1gInSc7
z	z	k7c2
tekutého	tekutý	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
(	(	kIx(
<g/>
ve	v	k7c6
hře	hra	k1gFnSc6
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
také	také	k9
například	například	k6eAd1
Ferengové	Ferengový	k2eAgNnSc1d1
nebo	nebo	k8xC
Jem	Jem	k?
<g/>
'	'	kIx"
<g/>
Hadarové	Hadarová	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
ne	ne	k9
jako	jako	k9
kompletní	kompletní	k2eAgFnSc1d1
hratelná	hratelný	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Star	Star	kA
Trek	Trek	k1gMnSc1
</s>
