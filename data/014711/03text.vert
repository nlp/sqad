<s>
Telefon	telefon	k1gInSc1
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Telefon	telefon	k1gInSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Telefon	telefon	k1gInSc1
Siemens	siemens	k1gInSc1
s	s	k7c7
tlačítkovou	tlačítkový	k2eAgFnSc7d1
volbou	volba	k1gFnSc7
(	(	kIx(
<g/>
po	po	k7c4
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Veřejné	veřejný	k2eAgFnPc1d1
telefonní	telefonní	k2eAgFnPc1d1
budky	budka	k1gFnPc1
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
v	v	k7c6
Campeche	Campeche	k1gFnSc6
(	(	kIx(
<g/>
Mexiko	Mexiko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Telefon	telefon	k1gInSc1
(	(	kIx(
<g/>
řecky	řecky	k6eAd1
<g/>
:	:	kIx,
téle	téle	k6eAd1
=	=	kIx~
vzdálený	vzdálený	k2eAgInSc1d1
a	a	k8xC
fóné	fóný	k2eAgFnPc1d1
=	=	kIx~
hlas	hlas	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
telekomunikační	telekomunikační	k2eAgNnSc4d1
zařízení	zařízení	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
přenáší	přenášet	k5eAaImIp3nS
hovor	hovor	k1gInSc1
(	(	kIx(
<g/>
zvuk	zvuk	k1gInSc1
<g/>
)	)	kIx)
prostřednictvím	prostřednictvím	k7c2
elektrických	elektrický	k2eAgInPc2d1
signálů	signál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokusně	pokusně	k6eAd1
se	se	k3xPyFc4
vyskytly	vyskytnout	k5eAaPmAgInP
i	i	k9
telefony	telefon	k1gInPc1
založené	založený	k2eAgInPc1d1
na	na	k7c6
jiných	jiný	k2eAgInPc6d1
principech	princip	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Klasický	klasický	k2eAgInSc1d1
telefon	telefon	k1gInSc1
s	s	k7c7
pevně	pevně	k6eAd1
instalovaným	instalovaný	k2eAgInSc7d1
telefonním	telefonní	k2eAgInSc7d1
přístrojem	přístroj	k1gInSc7
a	a	k8xC
drátovou	drátový	k2eAgFnSc7d1
linkou	linka	k1gFnSc7
ke	k	k7c3
každému	každý	k3xTgMnSc3
účastníkovi	účastník	k1gMnSc3
se	se	k3xPyFc4
během	běh	k1gInSc7
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
velmi	velmi	k6eAd1
rozšířil	rozšířit	k5eAaPmAgInS
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
bylo	být	k5eAaImAgNnS
po	po	k7c6
světě	svět	k1gInSc6
1,3	1,3	k4
miliardy	miliarda	k4xCgFnSc2
telefonních	telefonní	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
konce	konec	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
jej	on	k3xPp3gMnSc4
postupně	postupně	k6eAd1
vytlačují	vytlačovat	k5eAaImIp3nP
mobilní	mobilní	k2eAgInPc1d1
telefony	telefon	k1gInPc1
s	s	k7c7
bezdrátovým	bezdrátový	k2eAgInSc7d1
přenosem	přenos	k1gInSc7
signálu	signál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
2014	#num#	k4
jich	on	k3xPp3gMnPc2
bylo	být	k5eAaImAgNnS
po	po	k7c6
světě	svět	k1gInSc6
7	#num#	k4
miliard	miliarda	k4xCgFnPc2
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
mezi	mezi	k7c7
nimi	on	k3xPp3gNnPc7
převažují	převažovat	k5eAaImIp3nP
tzv.	tzv.	kA
chytré	chytrý	k2eAgInPc1d1
telefony	telefon	k1gInPc1
(	(	kIx(
<g/>
smartphony	smartphona	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Obvykle	obvykle	k6eAd1
je	být	k5eAaImIp3nS
vynález	vynález	k1gInSc1
telefonu	telefon	k1gInSc2
přisuzován	přisuzován	k2eAgMnSc1d1
vynálezci	vynálezce	k1gMnPc1
jménem	jméno	k1gNnSc7
Alexander	Alexandra	k1gFnPc2
Graham	graham	k1gInSc4
Bell	bell	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc1
první	první	k4xOgInSc1
telefon	telefon	k1gInSc1
byl	být	k5eAaImAgInS
sestrojen	sestrojit	k5eAaPmNgInS
v	v	k7c6
Bostonu	Boston	k1gInSc6
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
v	v	k7c6
roce	rok	k1gInSc6
1876	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
novějších	nový	k2eAgInPc2d2
údajů	údaj	k1gInPc2
vynalezl	vynaleznout	k5eAaPmAgMnS
telefon	telefon	k1gInSc4
italský	italský	k2eAgMnSc1d1
vynálezce	vynálezce	k1gMnSc1
Antonio	Antonio	k1gMnSc1
Meucci	Meucce	k1gFnSc4
(	(	kIx(
<g/>
Antonio	Antonio	k1gMnSc1
Santi	Sanť	k1gFnSc2
Giuseppe	Giusepp	k1gInSc5
Meucci	Meucce	k1gMnPc1
<g/>
)	)	kIx)
už	už	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1849	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
prvenství	prvenství	k1gNnSc1
bylo	být	k5eAaImAgNnS
v	v	k7c6
červnu	červen	k1gInSc6
roku	rok	k1gInSc2
2002	#num#	k4
oficiálně	oficiálně	k6eAd1
potvrzeno	potvrdit	k5eAaPmNgNnS
například	například	k6eAd1
i	i	k9
kongresem	kongres	k1gInSc7
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
(	(	kIx(
<g/>
Rezoluce	rezoluce	k1gFnSc1
269	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
dalších	další	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
vynalezl	vynaleznout	k5eAaPmAgInS
telefon	telefon	k1gInSc1
i	i	k8xC
Johann	Johann	k1gInSc1
Philipp	Philipp	k1gMnSc1
Reis	Reis	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1860	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gInSc1
vynález	vynález	k1gInSc1
pracoval	pracovat	k5eAaImAgInS
na	na	k7c6
principu	princip	k1gInSc6
doteku	dotek	k1gInSc2
velmi	velmi	k6eAd1
jemného	jemný	k2eAgInSc2d1
kontaktu	kontakt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysílač	vysílač	k1gInSc1
(	(	kIx(
<g/>
mikrofon	mikrofon	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
vyroben	vyrobit	k5eAaPmNgInS
z	z	k7c2
pivní	pivní	k2eAgFnSc2d1
bečky	bečka	k1gFnSc2
a	a	k8xC
tvarem	tvar	k1gInSc7
připomínal	připomínat	k5eAaImAgInS
lidské	lidský	k2eAgNnSc4d1
ucho	ucho	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přijímač	přijímač	k1gInSc1
(	(	kIx(
<g/>
reproduktor	reproduktor	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
vyroben	vyrobit	k5eAaPmNgInS
z	z	k7c2
pletací	pletací	k2eAgFnSc2d1
jehlice	jehlice	k1gFnSc2
a	a	k8xC
krabice	krabice	k1gFnSc2
od	od	k7c2
doutníků	doutník	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
telefon	telefon	k1gInSc1
mohl	moct	k5eAaImAgInS
skutečně	skutečně	k6eAd1
přenášet	přenášet	k5eAaImF
lidský	lidský	k2eAgInSc4d1
hlas	hlas	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
velice	velice	k6eAd1
zkresleně	zkresleně	k6eAd1
a	a	k8xC
muselo	muset	k5eAaImAgNnS
se	se	k3xPyFc4
do	do	k7c2
něj	on	k3xPp3gMnSc2
mluvit	mluvit	k5eAaImF
správnou	správný	k2eAgFnSc7d1
hlasitostí	hlasitost	k1gFnSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
kontakt	kontakt	k1gInSc4
pracoval	pracovat	k5eAaImAgMnS
správně	správně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Telefon	telefon	k1gInSc1
lépe	dobře	k6eAd2
než	než	k8xS
hlas	hlas	k1gInSc1
přenášel	přenášet	k5eAaImAgInS
hudbu	hudba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
rozhovor	rozhovor	k1gInSc4
učinil	učinit	k5eAaPmAgInS,k5eAaImAgInS
Reis	Reis	k1gInSc1
z	z	k7c2
fyzikálního	fyzikální	k2eAgInSc2d1
sálu	sál	k1gInSc2
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
vyučoval	vyučovat	k5eAaImAgMnS
fyziku	fyzika	k1gFnSc4
<g/>
,	,	kIx,
do	do	k7c2
blízkého	blízký	k2eAgInSc2d1
bytu	byt	k1gInSc2
svého	svůj	k3xOyFgMnSc2
přítele	přítel	k1gMnSc2
učitele	učitel	k1gMnSc2
zpěvu	zpěv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Údajně	údajně	k6eAd1
měl	mít	k5eAaImAgInS
mít	mít	k5eAaImF
tento	tento	k3xDgInSc1
obsah	obsah	k1gInSc1
<g/>
.	.	kIx.
„	„	k?
<g/>
Koně	kůň	k1gMnPc1
nežerou	žrát	k5eNaImIp3nP
okurkový	okurkový	k2eAgInSc4d1
salát	salát	k1gInSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
zvolal	zvolat	k5eAaPmAgMnS
Reis	Reis	k1gInSc4
<g/>
.	.	kIx.
„	„	k?
<g/>
To	ten	k3xDgNnSc1
vím	vědět	k5eAaImIp1nS
už	už	k6eAd1
dávno	dávno	k6eAd1
<g/>
,	,	kIx,
vy	vy	k3xPp2nPc1
hňupe	hňup	k1gMnSc5
<g/>
“	“	k?
<g/>
,	,	kIx,
odpověděl	odpovědět	k5eAaPmAgMnS
kolega	kolega	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c4
prvenství	prvenství	k1gNnSc4
vynálezu	vynález	k1gInSc2
k	k	k7c3
jednotlivým	jednotlivý	k2eAgFnPc3d1
částem	část	k1gFnPc3
vynálezu	vynález	k1gInSc2
bylo	být	k5eAaImAgNnS
vedeno	vést	k5eAaImNgNnS
mnoho	mnoho	k4c1
soudních	soudní	k2eAgInPc2d1
sporů	spor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláště	zvláště	k6eAd1
společnost	společnost	k1gFnSc4
Bell	bell	k1gInSc1
Telephone	Telephon	k1gInSc5
se	se	k3xPyFc4
snažila	snažit	k5eAaImAgFnS
agresivně	agresivně	k6eAd1
chránit	chránit	k5eAaImF
své	svůj	k3xOyFgInPc4
patenty	patent	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
byly	být	k5eAaImAgInP
ale	ale	k9
spíše	spíše	k9
další	další	k2eAgFnPc4d1
nejasnosti	nejasnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věc	věc	k1gFnSc1
komplikuje	komplikovat	k5eAaBmIp3nS
i	i	k9
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
vynálezci	vynálezce	k1gMnPc1
spíše	spíše	k9
předváděli	předvádět	k5eAaImAgMnP
své	svůj	k3xOyFgInPc4
objevy	objev	k1gInPc4
novinářům	novinář	k1gMnPc3
a	a	k8xC
průmyslníkům	průmyslník	k1gMnPc3
<g/>
,	,	kIx,
místo	místo	k7c2
publikace	publikace	k1gFnSc2
ve	v	k7c6
vědeckých	vědecký	k2eAgInPc6d1
časopisech	časopis	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
poznamenat	poznamenat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
současný	současný	k2eAgInSc1d1
telefon	telefon	k1gInSc1
nemá	mít	k5eNaImIp3nS
jednoho	jeden	k4xCgMnSc4
vynálezce	vynálezce	k1gMnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
výsledkem	výsledek	k1gInSc7
postupného	postupný	k2eAgNnSc2d1
vylepšování	vylepšování	k1gNnSc2
a	a	k8xC
vynálezů	vynález	k1gInPc2
velkého	velký	k2eAgNnSc2d1
množství	množství	k1gNnSc2
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Telefon	telefon	k1gInSc1
na	na	k7c6
jiném	jiný	k2eAgInSc6d1
než	než	k8xS
elektrickém	elektrický	k2eAgInSc6d1
principu	princip	k1gInSc6
</s>
<s>
Pokud	pokud	k8xS
budeme	být	k5eAaImBp1nP
za	za	k7c4
telefon	telefon	k1gInSc4
považovat	považovat	k5eAaImF
každé	každý	k3xTgNnSc4
zařízení	zařízení	k1gNnSc4
<g/>
,	,	kIx,
schopné	schopný	k2eAgFnPc1d1
přenést	přenést	k5eAaPmF
hlas	hlas	k1gInSc4
na	na	k7c4
velkou	velký	k2eAgFnSc4d1
vzdálenost	vzdálenost	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
historie	historie	k1gFnSc1
telefonu	telefon	k1gInSc2
mnohem	mnohem	k6eAd1
starší	starý	k2eAgMnPc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
telefony	telefon	k1gInPc1
byly	být	k5eAaImAgInP
čistě	čistě	k6eAd1
mechanická	mechanický	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s>
Nejobvyklejší	obvyklý	k2eAgInSc1d3
byl	být	k5eAaImAgInS
trubkový	trubkový	k2eAgInSc1d1
telefon	telefon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
známý	známý	k2eAgInSc4d1
popis	popis	k1gInSc4
je	být	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
968	#num#	k4
<g/>
,	,	kIx,
podle	podle	k7c2
něhož	jenž	k3xRgMnSc4
čínský	čínský	k2eAgMnSc1d1
vynálezce	vynálezce	k1gMnSc1
Kung-Foo-Whing	Kung-Foo-Whing	k1gInSc4
využil	využít	k5eAaPmAgMnS
roury	roura	k1gFnSc2
k	k	k7c3
hovoru	hovor	k1gInSc3
na	na	k7c4
dálku	dálka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trubkové	trubkový	k2eAgInPc1d1
telefony	telefon	k1gInPc1
se	se	k3xPyFc4
dožily	dožít	k5eAaPmAgFnP
velkého	velký	k2eAgNnSc2d1
rozšíření	rozšíření	k1gNnSc3
v	v	k7c6
lodní	lodní	k2eAgFnSc6d1
dopravě	doprava	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
umožnily	umožnit	k5eAaPmAgInP
relativně	relativně	k6eAd1
spolehlivé	spolehlivý	k2eAgNnSc1d1
zvukové	zvukový	k2eAgNnSc1d1
spojení	spojení	k1gNnSc1
oddělených	oddělený	k2eAgFnPc2d1
částí	část	k1gFnPc2
lodi	loď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dalším	další	k2eAgMnSc7d1
<g/>
,	,	kIx,
byť	byť	k8xS
spíše	spíše	k9
kratochvilným	kratochvilný	k2eAgInSc7d1
typem	typ	k1gInSc7
telefonu	telefon	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
lankový	lankový	k2eAgInSc4d1
telefon	telefon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
dvěma	dva	k4xCgFnPc7
membránami	membrána	k1gFnPc7
<g/>
,	,	kIx,
spojenými	spojený	k2eAgInPc7d1
napnutým	napnutý	k2eAgInSc7d1
provazem	provaz	k1gInSc7
<g/>
,	,	kIx,
nití	nit	k1gFnSc7
nebo	nebo	k8xC
strunou	struna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chvění	chvění	k1gNnSc4
jedné	jeden	k4xCgFnSc2
membrány	membrána	k1gFnSc2
je	být	k5eAaImIp3nS
strunou	struna	k1gFnSc7
přenášeno	přenášet	k5eAaImNgNnS
na	na	k7c4
druhou	druhý	k4xOgFnSc4
membránu	membrána	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
telefon	telefon	k1gInSc4
si	se	k3xPyFc3
můžete	moct	k5eAaImIp2nP
vyrobit	vyrobit	k5eAaPmF
ze	z	k7c2
dvou	dva	k4xCgInPc2
plastových	plastový	k2eAgInPc2d1
kelímků	kelímek	k1gInPc2
<g/>
,	,	kIx,
když	když	k8xS
jejich	jejich	k3xOp3gNnPc1
dna	dno	k1gNnPc1
propojíte	propojit	k5eAaPmIp2nP
několika	několik	k4yIc7
metry	metr	k1gInPc4
niti	nit	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Elektrické	elektrický	k2eAgInPc1d1
telefony	telefon	k1gInPc1
</s>
<s>
Telefonní	telefonní	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Antonio	Antonio	k1gMnSc1
Meucci	Meucce	k1gMnSc3
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
za	za	k7c4
objevitele	objevitel	k1gMnSc4
telefonu	telefon	k1gInSc2
považován	považován	k2eAgMnSc1d1
Antonio	Antonio	k1gMnSc1
Meucci	Meucce	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
telefon	telefon	k1gInSc1
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
předveden	předvést	k5eAaPmNgInS
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1860	#num#	k4
a	a	k8xC
zpráva	zpráva	k1gFnSc1
o	o	k7c6
něm	on	k3xPp3gInSc6
byla	být	k5eAaImAgFnS
publikována	publikován	k2eAgFnSc1d1
v	v	k7c6
místním	místní	k2eAgInSc6d1
italsky	italsky	k6eAd1
psaném	psaný	k2eAgInSc6d1
tisku	tisk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
principu	princip	k1gInSc6
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
elektromagnetický	elektromagnetický	k2eAgInSc4d1
mikrofon	mikrofon	k1gInSc4
<g/>
/	/	kIx~
<g/>
sluchátko	sluchátko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvuk	zvuk	k1gInSc1
rozkmital	rozkmitat	k5eAaPmAgInS
membránu	membrána	k1gFnSc4
s	s	k7c7
permanentním	permanentní	k2eAgInSc7d1
magnetem	magnet	k1gInSc7
v	v	k7c6
cívce	cívka	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
převedla	převést	k5eAaPmAgFnS
pohyb	pohyb	k1gInSc4
na	na	k7c4
elektrický	elektrický	k2eAgInSc4d1
proud	proud	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
pak	pak	k6eAd1
byl	být	k5eAaImAgInS
přenesen	přenést	k5eAaPmNgInS
dráty	drát	k1gInPc7
do	do	k7c2
stejného	stejný	k2eAgNnSc2d1
zařízení	zařízení	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
jej	on	k3xPp3gMnSc4
přeměnilo	přeměnit	k5eAaPmAgNnS
zpět	zpět	k6eAd1
na	na	k7c4
zvuk	zvuk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výhodou	výhoda	k1gFnSc7
tohoto	tento	k3xDgInSc2
systému	systém	k1gInSc2
byla	být	k5eAaImAgFnS
lepší	dobrý	k2eAgFnSc1d2
kvalita	kvalita	k1gFnSc1
přenášeného	přenášený	k2eAgInSc2d1
zvuku	zvuk	k1gInSc2
<g/>
,	,	kIx,
nevýhodou	nevýhoda	k1gFnSc7
naopak	naopak	k6eAd1
nízká	nízký	k2eAgFnSc1d1
hlasitost	hlasitost	k1gFnSc1
a	a	k8xC
malý	malý	k2eAgInSc1d1
dosah	dosah	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Neznalost	neznalost	k1gFnSc1
angličtiny	angličtina	k1gFnSc2
a	a	k8xC
nedostatek	nedostatek	k1gInSc4
obchodního	obchodní	k2eAgInSc2d1
talentu	talent	k1gInSc2
zapříčinily	zapříčinit	k5eAaPmAgInP
<g/>
,	,	kIx,
že	že	k8xS
Meucci	Meucce	k1gFnSc4
nebyl	být	k5eNaImAgMnS
schopen	schopen	k2eAgInSc4d1
svůj	svůj	k3xOyFgInSc4
vynález	vynález	k1gInSc4
dovést	dovést	k5eAaPmF
do	do	k7c2
komerčně	komerčně	k6eAd1
úspěšné	úspěšný	k2eAgFnSc2d1
podoby	podoba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spor	spor	k1gInSc1
o	o	k7c4
prvenství	prvenství	k1gNnSc4
vynálezu	vynález	k1gInSc2
s	s	k7c7
Bellem	bell	k1gInSc7
byl	být	k5eAaImAgInS
odkládán	odkládat	k5eAaImNgInS
až	až	k9
do	do	k7c2
Meucciho	Meucci	k1gMnSc2
smrti	smrt	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
kauza	kauza	k1gFnSc1
zrušena	zrušit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Meucci	Meucec	k1gInPc7
byl	být	k5eAaImAgInS
uznán	uznat	k5eAaPmNgInS
prvním	první	k4xOgInSc7
vynálezcem	vynálezce	k1gMnSc7
telefonu	telefon	k1gInSc2
kongresem	kongres	k1gInSc7
USA	USA	kA
až	až	k8xS
po	po	k7c6
více	hodně	k6eAd2
než	než	k8xS
sto	sto	k4xCgNnSc4
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
rezolucí	rezoluce	k1gFnSc7
269	#num#	k4
datovanou	datovaný	k2eAgFnSc7d1
11	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největším	veliký	k2eAgInSc7d3
problémem	problém	k1gInSc7
dalšího	další	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
telefonu	telefon	k1gInSc2
byl	být	k5eAaImAgInS
vhodný	vhodný	k2eAgInSc1d1
mikrofon	mikrofon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Charles	Charles	k1gMnSc1
Bourseul	Bourseula	k1gFnPc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1854	#num#	k4
v	v	k7c6
časopise	časopis	k1gInSc6
„	„	k?
<g/>
L	L	kA
<g/>
'	'	kIx"
<g/>
Illustration	Illustration	k1gInSc1
de	de	k?
Paris	Paris	k1gMnSc1
<g/>
“	“	k?
publikoval	publikovat	k5eAaBmAgMnS
francouzský	francouzský	k2eAgMnSc1d1
telegrafista	telegrafista	k1gMnSc1
Charles	Charles	k1gMnSc1
Bourseul	Bourseula	k1gFnPc2
návrh	návrh	k1gInSc1
přístroje	přístroj	k1gInSc2
pro	pro	k7c4
elektrický	elektrický	k2eAgInSc4d1
přenos	přenos	k1gInSc4
zvuku	zvuk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přístroj	přístroj	k1gInSc1
se	se	k3xPyFc4
skládal	skládat	k5eAaImAgInS
z	z	k7c2
pružného	pružný	k2eAgInSc2d1
kotouče	kotouč	k1gInSc2
<g/>
,	,	kIx,
přerušujícího	přerušující	k2eAgInSc2d1
proud	proud	k1gInSc1
a	a	k8xC
druhého	druhý	k4xOgInSc2
podobného	podobný	k2eAgInSc2d1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgMnS
tímto	tento	k3xDgInSc7
proudem	proud	k1gInSc7
rozechvíván	rozechvíván	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc1
pokusy	pokus	k1gInPc1
ale	ale	k9
nevedly	vést	k5eNaImAgInP
k	k	k7c3
praktickému	praktický	k2eAgNnSc3d1
využití	využití	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Johann	Johann	k1gMnSc1
Philipp	Philipp	k1gMnSc1
Reis	Reis	k1gInSc4
</s>
<s>
Podobný	podobný	k2eAgInSc1d1
přístroj	přístroj	k1gInSc1
předváděl	předvádět	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1860	#num#	k4
německý	německý	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
Johann	Johann	k1gMnSc1
Philipp	Philipp	k1gMnSc1
Reis	Reis	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
založen	založit	k5eAaPmNgMnS
na	na	k7c6
přerušovacím	přerušovací	k2eAgInSc6d1
principu	princip	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jehla	jehla	k1gFnSc1
<g/>
,	,	kIx,
připojená	připojený	k2eAgNnPc1d1
k	k	k7c3
pružné	pružný	k2eAgFnSc3d1
membráně	membrána	k1gFnSc3
<g/>
,	,	kIx,
přerušovala	přerušovat	k5eAaImAgFnS
proud	proud	k1gInSc4
podle	podle	k7c2
pohybu	pohyb	k1gInSc2
membrány	membrána	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
přenos	přenos	k1gInSc1
byl	být	k5eAaImAgInS
pouze	pouze	k6eAd1
nedokonalý	dokonalý	k2eNgInSc1d1
dvoustavový	dvoustavový	k2eAgInSc1d1
(	(	kIx(
<g/>
proud	proud	k1gInSc1
teče	teč	k1gFnSc2
<g/>
/	/	kIx~
<g/>
neteče	téct	k5eNaImIp3nS
-	-	kIx~
dnes	dnes	k6eAd1
bychom	by	kYmCp1nP
řekli	říct	k5eAaPmAgMnP
jednobitový	jednobitový	k2eAgInSc4d1
neboli	neboli	k8xC
binární	binární	k2eAgInSc4d1
přenos	přenos	k1gInSc4
<g/>
)	)	kIx)
a	a	k8xC
nikoliv	nikoliv	k9
analogový	analogový	k2eAgMnSc1d1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
u	u	k7c2
pozdějších	pozdní	k2eAgInPc2d2
telefonů	telefon	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
přenášet	přenášet	k5eAaImF
pouze	pouze	k6eAd1
tóny	tón	k1gInPc4
nebo	nebo	k8xC
nezřetelný	zřetelný	k2eNgInSc4d1
šepot	šepot	k1gInSc4
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
složitěji	složitě	k6eAd2
strukturovaný	strukturovaný	k2eAgInSc4d1
zvuk	zvuk	k1gInSc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
lidský	lidský	k2eAgInSc4d1
hlas	hlas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
byl	být	k5eAaImAgInS
tento	tento	k3xDgInSc1
přístroj	přístroj	k1gInSc1
velmi	velmi	k6eAd1
citlivý	citlivý	k2eAgInSc1d1
na	na	k7c4
pečlivé	pečlivý	k2eAgNnSc4d1
nastavení	nastavení	k1gNnSc4
tohoto	tento	k3xDgInSc2
kontaktu	kontakt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Cromwell	Cromwell	k1gInSc1
Varley	Varlea	k1gFnSc2
</s>
<s>
Variaci	variace	k1gFnSc4
na	na	k7c4
Reisovu	Reisův	k2eAgFnSc4d1
práci	práce	k1gFnSc4
si	se	k3xPyFc3
v	v	k7c6
roce	rok	k1gInSc6
1870	#num#	k4
nechal	nechat	k5eAaPmAgMnS
patentovat	patentovat	k5eAaBmF
známý	známý	k2eAgMnSc1d1
anglický	anglický	k2eAgMnSc1d1
elektrotechnik	elektrotechnik	k1gMnSc1
Cromwell	Cromwell	k1gMnSc1
Fleetwood	Fleetwood	k1gInSc4
Varley	Varlea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
ale	ale	k8xC
opět	opět	k6eAd1
jen	jen	k9
o	o	k7c4
přenos	přenos	k1gInSc4
tónů	tón	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Poul	Poul	k1gMnSc1
la	la	k1gNnSc2
Cour	Cour	k?
</s>
<s>
Dánský	dánský	k2eAgMnSc1d1
vynálezce	vynálezce	k1gMnSc1
Poul	Poul	k1gMnSc1
la	la	k1gNnSc2
Cour	Cour	k?
experimentoval	experimentovat	k5eAaImAgInS
s	s	k7c7
audio	audio	k2eAgFnSc7d1
telegrafií	telegrafie	k1gFnSc7
na	na	k7c6
telegrafní	telegrafní	k2eAgFnSc6d1
lince	linka	k1gFnSc6
mezi	mezi	k7c7
Kodaní	Kodaň	k1gFnSc7
a	a	k8xC
Fredericiou	Fredericia	k1gFnSc7
v	v	k7c6
Jutlandu	Jutland	k1gInSc6
kolem	kolem	k7c2
roku	rok	k1gInSc2
1874	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Využíval	využívat	k5eAaImAgMnS,k5eAaPmAgMnS
vibrující	vibrující	k2eAgFnSc4d1
vidlicovou	vidlicový	k2eAgFnSc4d1
ladičku	ladička	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
přerušovala	přerušovat	k5eAaImAgFnS
proud	proud	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
druhém	druhý	k4xOgInSc6
konci	konec	k1gInSc6
linky	linka	k1gFnSc2
elektromagnet	elektromagnet	k1gInSc1
přitahoval	přitahovat	k5eAaImAgInS
rameno	rameno	k1gNnSc4
druhé	druhý	k4xOgFnSc2
stejné	stejný	k2eAgFnSc2d1
ladičky	ladička	k1gFnSc2
a	a	k8xC
ta	ten	k3xDgFnSc1
vydávala	vydávat	k5eAaPmAgFnS,k5eAaImAgFnS
stejný	stejný	k2eAgInSc4d1
tón	tón	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
probíhal	probíhat	k5eAaImAgInS
i	i	k8xC
zápis	zápis	k1gInSc1
tohoto	tento	k3xDgInSc2
tónu	tón	k1gInSc2
na	na	k7c4
papír	papír	k1gInSc4
pomocí	pomocí	k7c2
telegrafního	telegrafní	k2eAgInSc2d1
přístroje	přístroj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
la	la	k1gNnPc4
Cour	Cour	k?
tedy	tedy	k8xC
nepřenášel	přenášet	k5eNaImAgMnS
hlas	hlas	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
tóny	tón	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Elisha	Elisha	k1gFnSc1
Gray	Graa	k1gFnSc2
</s>
<s>
Elisha	Elisha	k1gFnSc1
Gray	Graa	k1gFnSc2
z	z	k7c2
Chicaga	Chicago	k1gNnSc2
vynalezl	vynaleznout	k5eAaPmAgMnS
ve	v	k7c6
stejné	stejný	k2eAgFnSc6d1
době	doba	k1gFnSc6
jako	jako	k8xS,k8xC
la	la	k1gNnSc6
Cour	Cour	k?
podobný	podobný	k2eAgInSc4d1
tónový	tónový	k2eAgInSc4d1
telegraf	telegraf	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jeho	jeho	k3xOp3gInSc6
původním	původní	k2eAgInSc6d1
přístroji	přístroj	k1gInSc6
vibrující	vibrující	k2eAgInSc4d1
ocelový	ocelový	k2eAgInSc4d1
jazýček	jazýček	k1gInSc4
přerušoval	přerušovat	k5eAaImAgInS
proud	proud	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
na	na	k7c6
druhém	druhý	k4xOgInSc6
konci	konec	k1gInSc6
linky	linka	k1gFnPc4
pomocí	pomocí	k7c2
elektromagnetu	elektromagnet	k1gInSc2
rozvibroval	rozvibrovat	k5eAaPmAgInS
stejně	stejně	k6eAd1
naladěný	naladěný	k2eAgInSc1d1
jazýček	jazýček	k1gInSc1
u	u	k7c2
jeho	jeho	k3xOp3gInPc2
pólů	pól	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Grayův	Grayův	k2eAgInSc1d1
harmonický	harmonický	k2eAgInSc1d1
(	(	kIx(
<g/>
tónový	tónový	k2eAgInSc1d1
<g/>
)	)	kIx)
telegraf	telegraf	k1gInSc1
s	s	k7c7
vibrujícími	vibrující	k2eAgInPc7d1
jazýčky	jazýček	k1gInPc7
použila	použít	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
Western	western	k1gInSc4
Union	union	k1gInSc1
Telegraph	Telegrapha	k1gFnPc2
Company	Compana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
jedním	jeden	k4xCgInSc7
drátem	drát	k1gInSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
poslán	poslat	k5eAaPmNgMnS
zároveň	zároveň	k6eAd1
více	hodně	k6eAd2
než	než	k8xS
jeden	jeden	k4xCgInSc1
kmitočet	kmitočet	k1gInSc1
<g/>
,	,	kIx,
harmonický	harmonický	k2eAgInSc1d1
telegraf	telegraf	k1gInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
použit	použít	k5eAaPmNgInS
v	v	k7c6
multiplexním	multiplexní	k2eAgInSc6d1
režimu	režim	k1gInSc6
a	a	k8xC
přenášet	přenášet	k5eAaImF
po	po	k7c6
jednom	jeden	k4xCgInSc6
vedení	vedení	k1gNnSc6
více	hodně	k6eAd2
zpráv	zpráva	k1gFnPc2
naráz	naráz	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Grayův	Grayův	k2eAgInSc1d1
harmonický	harmonický	k2eAgInSc1d1
telegraf	telegraf	k1gInSc1
pokračoval	pokračovat	k5eAaImAgInS
ve	v	k7c6
šlépějích	šlépěj	k1gFnPc6
Reise	Reis	k1gMnSc2
a	a	k8xC
Bourseula	Bourseul	k1gMnSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
v	v	k7c6
přerušování	přerušování	k1gNnSc6
proudu	proud	k1gInSc2
vibrujícím	vibrující	k2eAgInSc7d1
kontaktem	kontakt	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Gray	Gra	k1gMnPc4
zjistil	zjistit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
hlavním	hlavní	k2eAgInSc7d1
nedostatkem	nedostatek	k1gInSc7
je	být	k5eAaImIp3nS
přerušovaný	přerušovaný	k2eAgInSc1d1
-	-	kIx~
dnes	dnes	k6eAd1
bychom	by	kYmCp1nP
řekli	říct	k5eAaPmAgMnP
digitální	digitální	k2eAgInSc4d1
-	-	kIx~
charakter	charakter	k1gInSc1
přenášeného	přenášený	k2eAgInSc2d1
signálu	signál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inspiroval	inspirovat	k5eAaBmAgMnS
se	se	k3xPyFc4
mechanickým	mechanický	k2eAgInSc7d1
lankovým	lankový	k2eAgInSc7d1
telefonem	telefon	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
něm	on	k3xPp3gNnSc6
je	být	k5eAaImIp3nS
pohyb	pohyb	k1gInSc1
membrány	membrána	k1gFnSc2
mikrofonu	mikrofon	k1gInSc2
přenášen	přenášet	k5eAaImNgInS
analogově	analogově	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gray	Graum	k1gNnPc7
sestrojil	sestrojit	k5eAaPmAgMnS
a	a	k8xC
nechal	nechat	k5eAaPmAgMnS
si	se	k3xPyFc3
patentovat	patentovat	k5eAaBmF
kapalinový	kapalinový	k2eAgInSc4d1
mikrofon	mikrofon	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
byla	být	k5eAaImAgFnS
na	na	k7c6
membráně	membrána	k1gFnSc6
mikrofonu	mikrofon	k1gInSc2
jehla	jehla	k1gFnSc1
umístěná	umístěný	k2eAgFnSc1d1
do	do	k7c2
kapalného	kapalný	k2eAgInSc2d1
vodiče	vodič	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
membrána	membrána	k1gFnSc1
vibrovala	vibrovat	k5eAaImAgFnS
<g/>
,	,	kIx,
jehla	jehla	k1gFnSc1
se	se	k3xPyFc4
potápěla	potápět	k5eAaImAgFnS
více	hodně	k6eAd2
nebo	nebo	k8xC
méně	málo	k6eAd2
do	do	k7c2
kapaliny	kapalina	k1gFnSc2
a	a	k8xC
tím	ten	k3xDgNnSc7
regulovala	regulovat	k5eAaImAgFnS
procházející	procházející	k2eAgInSc4d1
proud	proud	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Grayův	Grayův	k2eAgInSc1d1
kapalinový	kapalinový	k2eAgInSc1d1
mikrofon	mikrofon	k1gInSc1
použil	použít	k5eAaPmAgInS
i	i	k9
Bell	bell	k1gInSc1
pro	pro	k7c4
mnoho	mnoho	k4c4
svých	svůj	k3xOyFgFnPc2
raných	raný	k2eAgFnPc2d1
veřejných	veřejný	k2eAgFnPc2d1
produkcí	produkce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapalinový	kapalinový	k2eAgInSc1d1
vysílač	vysílač	k1gInSc1
měl	mít	k5eAaImAgInS
ale	ale	k9
velké	velký	k2eAgInPc4d1
problémy	problém	k1gInPc4
s	s	k7c7
interferencí	interference	k1gFnSc7
vlnek	vlnka	k1gFnPc2
<g/>
,	,	kIx,
vznikajících	vznikající	k2eAgFnPc2d1
na	na	k7c6
hladině	hladina	k1gFnSc6
pohybem	pohyb	k1gInSc7
jehly	jehla	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Uhlíkový	uhlíkový	k2eAgInSc1d1
mikrofon	mikrofon	k1gInSc1
</s>
<s>
Uhlíkový	uhlíkový	k2eAgInSc4d1
mikrofon	mikrofon	k1gInSc4
vynalezl	vynaleznout	k5eAaPmAgMnS
Thomas	Thomas	k1gMnSc1
Alva	Alva	k1gMnSc1
Edison	Edison	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Edison	Edison	k1gMnSc1
objevil	objevit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
uhlíková	uhlíkový	k2eAgNnPc1d1
zrnka	zrnko	k1gNnPc1
<g/>
,	,	kIx,
stlačená	stlačený	k2eAgNnPc1d1
mezi	mezi	k7c4
kovové	kovový	k2eAgFnPc4d1
desky	deska	k1gFnPc4
mají	mít	k5eAaImIp3nP
elektrický	elektrický	k2eAgInSc4d1
odpor	odpor	k1gInSc4
nepřímo	přímo	k6eNd1
úměrný	úměrný	k2eAgInSc4d1
tlaku	tlak	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
na	na	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
desek	deska	k1gFnPc2
působí	působit	k5eAaImIp3nS
zvukové	zvukový	k2eAgNnSc4d1
vlnění	vlnění	k1gNnSc4
<g/>
,	,	kIx,
mění	měnit	k5eAaImIp3nS
se	se	k3xPyFc4
patřičně	patřičně	k6eAd1
i	i	k9
proud	proud	k1gInSc4
<g/>
,	,	kIx,
protékající	protékající	k2eAgInSc4d1
zrnky	zrnko	k1gNnPc7
mezi	mezi	k7c7
deskami	deska	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvalita	kvalita	k1gFnSc1
přeneseného	přenesený	k2eAgInSc2d1
zvuku	zvuk	k1gInSc2
je	být	k5eAaImIp3nS
dostačující	dostačující	k2eAgFnSc1d1
pro	pro	k7c4
hovor	hovor	k1gInSc4
a	a	k8xC
hlavně	hlavně	k9
takový	takový	k3xDgInSc1
mikrofon	mikrofon	k1gInSc1
funguje	fungovat	k5eAaImIp3nS
jako	jako	k9
elektromechanický	elektromechanický	k2eAgInSc1d1
zesilovač	zesilovač	k1gInSc1
(	(	kIx(
<g/>
energie	energie	k1gFnSc1
proudových	proudový	k2eAgFnPc2d1
změn	změna	k1gFnPc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
podstatně	podstatně	k6eAd1
větší	veliký	k2eAgFnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
energie	energie	k1gFnSc1
dopadajících	dopadající	k2eAgFnPc2d1
zvukových	zvukový	k2eAgFnPc2d1
vln	vlna	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objev	objev	k1gInSc1
vedl	vést	k5eAaImAgInS
k	k	k7c3
vývoji	vývoj	k1gInSc3
uhlíkových	uhlíkový	k2eAgInPc2d1
mikrofonů	mikrofon	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
byly	být	k5eAaImAgFnP
základem	základ	k1gInSc7
telefonů	telefon	k1gInPc2
po	po	k7c6
více	hodně	k6eAd2
než	než	k8xS
100	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
se	se	k3xPyFc4
(	(	kIx(
<g/>
byť	byť	k8xS
v	v	k7c6
menší	malý	k2eAgFnSc6d2
míře	míra	k1gFnSc6
<g/>
)	)	kIx)
používají	používat	k5eAaImIp3nP
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uhlíkový	uhlíkový	k2eAgInSc1d1
mikrofon	mikrofon	k1gInSc1
velmi	velmi	k6eAd1
zjednodušil	zjednodušit	k5eAaPmAgInS
konstrukci	konstrukce	k1gFnSc4
telefonního	telefonní	k2eAgInSc2d1
přístroje	přístroj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Telefonní	telefonní	k2eAgInPc1d1
přístroje	přístroj	k1gInPc1
s	s	k7c7
uhlíkovým	uhlíkový	k2eAgInSc7d1
mikrofonem	mikrofon	k1gInSc7
se	se	k3xPyFc4
dále	daleko	k6eAd2
dělí	dělit	k5eAaImIp3nS
podle	podle	k7c2
principu	princip	k1gInSc2
funkce	funkce	k1gFnSc2
a	a	k8xC
zapojení	zapojení	k1gNnSc2
na	na	k7c4
Telefonní	telefonní	k2eAgFnSc4d1
síť	síť	k1gFnSc4
s	s	k7c7
místní	místní	k2eAgFnSc7d1
baterií	baterie	k1gFnSc7
(	(	kIx(
<g/>
MB	MB	kA
<g/>
)	)	kIx)
a	a	k8xC
s	s	k7c7
ústřední	ústřední	k2eAgFnSc7d1
baterií	baterie	k1gFnSc7
(	(	kIx(
<g/>
ÚB	ÚB	kA
<g/>
)	)	kIx)
podle	podle	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
zda	zda	k8xS
je	být	k5eAaImIp3nS
mikrofon	mikrofon	k1gInSc4
napájen	napájen	k2eAgInSc4d1
elektrochemickým	elektrochemický	k2eAgInSc7d1
článkem	článek	k1gInSc7
u	u	k7c2
přístroje	přístroj	k1gInSc2
(	(	kIx(
<g/>
či	či	k8xC
v	v	k7c6
přístroji	přístroj	k1gInSc6
<g/>
)	)	kIx)
nebo	nebo	k8xC
společným	společný	k2eAgInSc7d1
akumulátorem	akumulátor	k1gInSc7
v	v	k7c6
telefonní	telefonní	k2eAgFnSc6d1
ústředně	ústředna	k1gFnSc6
</s>
<s>
Graham	graham	k1gInSc1
Bell	bell	k1gInSc1
</s>
<s>
Jako	jako	k8xS,k8xC
profesor	profesor	k1gMnSc1
vokální	vokální	k2eAgFnSc2d1
fyziologie	fyziologie	k1gFnSc2
na	na	k7c6
Bostonské	bostonský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
se	se	k3xPyFc4
Alexander	Alexandra	k1gFnPc2
Graham	graham	k1gInSc1
Bell	bell	k1gInSc1
zabýval	zabývat	k5eAaImAgInS
školením	školení	k1gNnSc7
učitelů	učitel	k1gMnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
učili	učit	k5eAaImAgMnP,k5eAaPmAgMnP
neslyšící	slyšící	k2eNgMnPc1d1
mluvit	mluvit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Experimentoval	experimentovat	k5eAaImAgInS
s	s	k7c7
fonografem	fonograf	k1gInSc7
Leona	Leo	k1gMnSc2
Scotta	Scott	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
zaznamenával	zaznamenávat	k5eAaImAgMnS
chvění	chvění	k1gNnSc4
vzduchu	vzduch	k1gInSc2
<g/>
,	,	kIx,
způsobené	způsobený	k2eAgFnSc2d1
hovorem	hovor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
přístroj	přístroj	k1gInSc1
obsahoval	obsahovat	k5eAaImAgInS
tenkou	tenký	k2eAgFnSc4d1
membránu	membrána	k1gFnSc4
vibrující	vibrující	k2eAgFnSc4d1
podle	podle	k7c2
hlasu	hlas	k1gInSc2
a	a	k8xC
nesoucí	nesoucí	k2eAgNnSc4d1
rydlo	rydlo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
rylo	rýt	k5eAaImAgNnS
zvlněnou	zvlněný	k2eAgFnSc4d1
linku	linka	k1gFnSc4
na	na	k7c4
desku	deska	k1gFnSc4
sazemi	saze	k1gFnPc7
pokrytého	pokrytý	k2eAgNnSc2d1
skla	sklo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
práce	práce	k1gFnSc1
jej	on	k3xPp3gInSc4
dobře	dobře	k6eAd1
připravila	připravit	k5eAaPmAgFnS
pro	pro	k7c4
zkoumání	zkoumání	k1gNnSc4
elektrického	elektrický	k2eAgInSc2d1
přenosu	přenos	k1gInSc2
zvuku	zvuk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1
výzkum	výzkum	k1gInSc1
začal	začít	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1874	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Použil	použít	k5eAaPmAgInS
tónový	tónový	k2eAgInSc1d1
telegraf	telegraf	k1gInSc1
podobný	podobný	k2eAgInSc1d1
vynálezům	vynález	k1gInPc3
Bourseula	Bourseulum	k1gNnSc2
<g/>
,	,	kIx,
Reise	Reise	k1gFnSc2
a	a	k8xC
Graye	Gray	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
jednom	jeden	k4xCgInSc6
z	z	k7c2
pokusů	pokus	k1gInPc2
zařízení	zařízení	k1gNnSc2
selhalo	selhat	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bell	bell	k1gInSc1
požádal	požádat	k5eAaPmAgInS
asistenta	asistent	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgMnS
na	na	k7c6
druhém	druhý	k4xOgInSc6
konci	konec	k1gInSc6
linky	linka	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
poklepal	poklepat	k5eAaPmAgMnS
na	na	k7c4
kovový	kovový	k2eAgInSc4d1
jazýček	jazýček	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
podle	podle	k7c2
něj	on	k3xPp3gInSc2
uvázl	uváznout	k5eAaPmAgMnS
na	na	k7c6
pólu	pólo	k1gNnSc6
magnetu	magnet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asistent	asistent	k1gMnSc1
Watson	Watson	k1gMnSc1
souhlasil	souhlasit	k5eAaImAgMnS
a	a	k8xC
překvapený	překvapený	k2eAgInSc1d1
Bell	bell	k1gInSc1
zjistil	zjistit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
jazýček	jazýček	k1gInSc1
na	na	k7c6
jeho	jeho	k3xOp3gFnSc6
straně	strana	k1gFnSc6
linky	linka	k1gFnSc2
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
pohybovat	pohybovat	k5eAaImF
a	a	k8xC
vydávat	vydávat	k5eAaPmF,k5eAaImF
stejný	stejný	k2eAgInSc4d1
zvuk	zvuk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
několika	několik	k4yIc2
dalších	další	k2eAgNnPc6d1
pokusech	pokus	k1gInPc6
pochopil	pochopit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
pohyb	pohyb	k1gInSc1
je	být	k5eAaImIp3nS
způsoben	způsobit	k5eAaPmNgInS
proudem	proud	k1gInSc7
<g/>
,	,	kIx,
indukovaným	indukovaný	k2eAgInSc7d1
pouhým	pouhý	k2eAgInSc7d1
pohybem	pohyb	k1gInSc7
vzdáleného	vzdálený	k2eAgInSc2d1
jazýčku	jazýček	k1gInSc2
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
magnetu	magnet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpojil	odpojit	k5eAaPmAgMnS
tedy	tedy	k9
napájení	napájení	k1gNnSc4
a	a	k8xC
začal	začít	k5eAaPmAgMnS
se	se	k3xPyFc4
spoléhat	spoléhat	k5eAaImF
pouze	pouze	k6eAd1
na	na	k7c4
indukční	indukční	k2eAgInPc4d1
proudy	proud	k1gInPc4
vlastních	vlastní	k2eAgInPc2d1
jazýčků	jazýček	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
zvuk	zvuk	k1gInSc1
hovoru	hovor	k1gInSc2
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
přenášet	přenášet	k5eAaImF
na	na	k7c4
značnou	značný	k2eAgFnSc4d1
vzdálenost	vzdálenost	k1gFnSc4
i	i	k9
bez	bez	k7c2
přerušování	přerušování	k1gNnSc2
elektrického	elektrický	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Bell	bell	k1gInSc1
s	s	k7c7
asistentem	asistent	k1gMnSc7
Watsonem	Watson	k1gMnSc7
objevili	objevit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
samotné	samotný	k2eAgInPc1d1
pohyby	pohyb	k1gInPc1
jazýčku	jazýček	k1gInSc2
v	v	k7c6
magnetickém	magnetický	k2eAgNnSc6d1
poli	pole	k1gNnSc6
mohou	moct	k5eAaImIp3nP
přenášet	přenášet	k5eAaImF
zvukovou	zvukový	k2eAgFnSc4d1
modulaci	modulace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bell	bell	k1gInSc1
na	na	k7c6
základě	základ	k1gInSc6
analogie	analogie	k1gFnSc2
s	s	k7c7
fonautografem	fonautograf	k1gInSc7
vymyslel	vymyslet	k5eAaPmAgMnS
přijímač	přijímač	k1gMnSc1
<g/>
,	,	kIx,
sestávající	sestávající	k2eAgMnSc1d1
z	z	k7c2
napnuté	napnutý	k2eAgFnSc2d1
membrány	membrána	k1gFnSc2
nebo	nebo	k8xC
bubínku	bubínek	k1gInSc2
ze	z	k7c2
speciální	speciální	k2eAgFnSc2d1
kůže	kůže	k1gFnSc2
s	s	k7c7
kotvou	kotva	k1gFnSc7
ze	z	k7c2
zmagnetovaného	zmagnetovaný	k2eAgNnSc2d1
železa	železo	k1gNnSc2
<g/>
,	,	kIx,
připevněnou	připevněný	k2eAgFnSc4d1
na	na	k7c4
střed	střed	k1gInSc4
membrány	membrána	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kotva	kotva	k1gFnSc1
mohla	moct	k5eAaImAgFnS
volně	volně	k6eAd1
kmitat	kmitat	k5eAaImF
před	před	k7c7
pólem	pól	k1gInSc7
elektromagnetu	elektromagnet	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnSc1
vinutí	vinutí	k1gNnSc1
bylo	být	k5eAaImAgNnS
zapojeno	zapojit	k5eAaPmNgNnS
do	do	k7c2
linkového	linkový	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
telefonu	telefon	k1gInSc2
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
V	v	k7c6
českém	český	k2eAgInSc6d1
tisku	tisk	k1gInSc6
byla	být	k5eAaImAgFnS
informace	informace	k1gFnSc1
o	o	k7c6
Bellově	Bellův	k2eAgInSc6d1
telefonu	telefon	k1gInSc6
uveřejněna	uveřejnit	k5eAaPmNgFnS
již	již	k6eAd1
v	v	k7c6
prosinci	prosinec	k1gInSc6
1877	#num#	k4
v	v	k7c6
časopisu	časopis	k1gInSc6
Světozor	světozor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zřízení	zřízení	k1gNnSc1
telefonních	telefonní	k2eAgFnPc2d1
linek	linka	k1gFnPc2
muselo	muset	k5eAaImAgNnS
být	být	k5eAaImF
ve	v	k7c6
smyslu	smysl	k1gInSc6
telegrafního	telegrafní	k2eAgInSc2d1
zákona	zákon	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1847	#num#	k4
povoleno	povolit	k5eAaPmNgNnS
státem	stát	k1gInSc7
<g/>
,	,	kIx,
resp.	resp.	kA
ministerstvem	ministerstvo	k1gNnSc7
obchodu	obchod	k1gInSc2
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
telefonní	telefonní	k2eAgFnSc1d1
linka	linka	k1gFnSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
byla	být	k5eAaImAgFnS
uvedena	uvést	k5eAaPmNgFnS
do	do	k7c2
provozu	provoz	k1gInSc2
10	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1881	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
mezi	mezi	k7c7
správou	správa	k1gFnSc7
dolu	dol	k1gInSc2
Georg	Georg	k1gMnSc1
Hartmann	Hartmann	k1gMnSc1
v	v	k7c6
Ledvicích	Ledvice	k1gFnPc6
a	a	k8xC
nádražím	nádraží	k1gNnSc7
v	v	k7c6
Duchcově	Duchcův	k2eAgNnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
telefonní	telefonní	k2eAgFnSc1d1
linka	linka	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
ve	v	k7c6
Vysočanech	Vysočany	k1gInPc6
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
uvedena	uvést	k5eAaPmNgFnS
do	do	k7c2
provozu	provoz	k1gInSc2
koncem	koncem	k7c2
května	květen	k1gInSc2
1881	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pražská	pražský	k2eAgFnSc1d1
telefonní	telefonní	k2eAgFnSc1d1
ústředna	ústředna	k1gFnSc1
pro	pro	k7c4
prvních	první	k4xOgInPc2
11	#num#	k4
účastníků	účastník	k1gMnPc2
zahájila	zahájit	k5eAaPmAgFnS
provoz	provoz	k1gInSc4
11	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1882	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1919	#num#	k4
<g/>
,	,	kIx,
tj.	tj.	kA
<g/>
po	po	k7c6
vzniku	vznik	k1gInSc6
Československa	Československo	k1gNnSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
telefonovat	telefonovat	k5eAaImF
z	z	k7c2
Prahy	Praha	k1gFnSc2
do	do	k7c2
Ostravy	Ostrava	k1gFnSc2
jen	jen	k9
tranzitem	tranzit	k1gInSc7
přes	přes	k7c4
Vídeň	Vídeň	k1gFnSc4
<g/>
;	;	kIx,
telefonní	telefonní	k2eAgNnPc4d1
spojení	spojení	k1gNnPc4
s	s	k7c7
Moravou	Morava	k1gFnSc7
bylo	být	k5eAaImAgNnS
pro	pro	k7c4
soukromou	soukromý	k2eAgFnSc4d1
potřebu	potřeba	k1gFnSc4
povoleno	povolit	k5eAaPmNgNnS
až	až	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1922	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1938	#num#	k4
bylo	být	k5eAaImAgNnS
v	v	k7c6
tehdejším	tehdejší	k2eAgNnSc6d1
Česku	Česko	k1gNnSc6
92	#num#	k4
tisíc	tisíc	k4xCgInPc2
hlavních	hlavní	k2eAgMnPc2d1
a	a	k8xC
18	#num#	k4
tisíc	tisíc	k4xCgInPc2
vedlejších	vedlejší	k2eAgFnPc2d1
telefonních	telefonní	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
62	#num#	k4
tisíc	tisíc	k4xCgInPc2
bylo	být	k5eAaImAgNnS
připojeno	připojit	k5eAaPmNgNnS
na	na	k7c4
automatické	automatický	k2eAgNnSc4d1
<g/>
,	,	kIx,
30	#num#	k4
tisíc	tisíc	k4xCgInPc2
na	na	k7c4
manuální	manuální	k2eAgFnPc4d1
ústředny	ústředna	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
</s>
<s>
Roku	rok	k1gInSc2
1892	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
USA	USA	kA
uvedena	uvést	k5eAaPmNgNnP
do	do	k7c2
provozu	provoz	k1gInSc6
první	první	k4xOgFnSc1
automatická	automatický	k2eAgFnSc1d1
ústředna	ústředna	k1gFnSc1
pro	pro	k7c4
99	#num#	k4
účastníků	účastník	k1gMnPc2
a	a	k8xC
Edison	Edison	k1gInSc1
patentoval	patentovat	k5eAaBmAgInS
uhlíkový	uhlíkový	k2eAgInSc4d1
mikrofon	mikrofon	k1gInSc4
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1913	#num#	k4
si	se	k3xPyFc3
firma	firma	k1gFnSc1
Siemens	siemens	k1gInSc1
patentovala	patentovat	k5eAaBmAgFnS
číselnici	číselnice	k1gFnSc4
pro	pro	k7c4
pulzní	pulzní	k2eAgFnSc4d1
volbu	volba	k1gFnSc4
a	a	k8xC
roku	rok	k1gInSc2
1955	#num#	k4
zavedla	zavést	k5eAaPmAgFnS
firma	firma	k1gFnSc1
Bell	bell	k1gInSc1
Telephone	Telephon	k1gInSc5
Company	Compan	k1gInPc4
tónovou	tónový	k2eAgFnSc4d1
volbu	volba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1978	#num#	k4
zavedla	zavést	k5eAaPmAgFnS
tatáž	týž	k3xTgFnSc1
firma	firma	k1gFnSc1
mobilní	mobilní	k2eAgFnSc1d1
síť	síť	k1gFnSc1
v	v	k7c6
Chicagu	Chicago	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
bylo	být	k5eAaImAgNnS
na	na	k7c6
světě	svět	k1gInSc6
instalováno	instalován	k2eAgNnSc1d1
1,3	1,3	k4
miliardy	miliarda	k4xCgFnSc2
pevných	pevný	k2eAgFnPc2d1
linek	linka	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
vyspělých	vyspělý	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
připadalo	připadat	k5eAaPmAgNnS,k5eAaImAgNnS
asi	asi	k9
50	#num#	k4
linek	linka	k1gFnPc2
na	na	k7c4
100	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
na	na	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
asi	asi	k9
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1983	#num#	k4
jim	on	k3xPp3gMnPc3
však	však	k9
začaly	začít	k5eAaPmAgInP
konkurovat	konkurovat	k5eAaImF
mobilní	mobilní	k2eAgInPc1d1
telefony	telefon	k1gInPc1
s	s	k7c7
bezdrátovým	bezdrátový	k2eAgInSc7d1
přenosem	přenos	k1gInSc7
signálu	signál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
2009	#num#	k4
jich	on	k3xPp3gMnPc2
bylo	být	k5eAaImAgNnS
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
4,6	4,6	k4
miliardy	miliarda	k4xCgFnPc4
<g/>
,	,	kIx,
roku	rok	k1gInSc2
2014	#num#	k4
přes	přes	k7c4
7	#num#	k4
miliard	miliarda	k4xCgFnPc2
mobilních	mobilní	k2eAgInPc2d1
telefonů	telefon	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Švédský	švédský	k2eAgInSc1d1
telefon	telefon	k1gInSc1
pro	pro	k7c4
manuální	manuální	k2eAgFnPc4d1
ústředny	ústředna	k1gFnPc4
(	(	kIx(
<g/>
1896	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Telefon	telefon	k1gInSc1
W28	W28	k1gFnSc2
s	s	k7c7
číselnicí	číselnice	k1gFnSc7
a	a	k8xC
pulzní	pulzní	k2eAgFnSc7d1
volbou	volba	k1gFnSc7
(	(	kIx(
<g/>
Siemens	siemens	k1gInSc1
1925	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1
součásti	součást	k1gFnPc1
telefonu	telefon	k1gInSc2
pro	pro	k7c4
pulzní	pulzní	k2eAgFnSc4d1
volbu	volba	k1gFnSc4
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
1	#num#	k4
<g/>
.	.	kIx.
spojovací	spojovací	k2eAgInSc1d1
kabel	kabel	k1gInSc1
<g/>
;	;	kIx,
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kryt	kryt	k1gInSc1
<g/>
;	;	kIx,
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dno	dno	k1gNnSc1
<g/>
;	;	kIx,
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvonek	zvonek	k1gInSc1
<g/>
;	;	kIx,
5	#num#	k4
<g/>
.	.	kIx.
<g/>
Sluchátko	sluchátko	k1gNnSc4
<g/>
;	;	kIx,
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Číselnice	číselnice	k1gFnSc1
<g/>
;	;	kIx,
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přepínací	přepínací	k2eAgFnSc1d1
deska	deska	k1gFnSc1
</s>
<s>
Místní	místní	k2eAgFnPc4d1
baterie	baterie	k1gFnPc4
<g/>
,	,	kIx,
manuální	manuální	k2eAgFnPc4d1
ústředny	ústředna	k1gFnPc4
</s>
<s>
V	v	k7c6
systému	systém	k1gInSc6
s	s	k7c7
místní	místní	k2eAgFnSc7d1
baterií	baterie	k1gFnSc7
obsahoval	obsahovat	k5eAaImAgInS
telefon	telefon	k1gInSc1
baterii	baterie	k1gFnSc4
<g/>
,	,	kIx,
sluchátko	sluchátko	k1gNnSc4
s	s	k7c7
mikrofonem	mikrofon	k1gInSc7
a	a	k8xC
ruční	ruční	k2eAgInSc1d1
induktor	induktor	k1gInSc1
s	s	k7c7
kličkou	klička	k1gFnSc7
<g/>
,	,	kIx,
kterým	který	k3yQgInSc7,k3yIgInSc7,k3yRgInSc7
se	se	k3xPyFc4
generoval	generovat	k5eAaImAgInS
signál	signál	k1gInSc1
vyzvánění	vyzvánění	k1gNnSc2
u	u	k7c2
druhého	druhý	k4xOgMnSc2
účastníka	účastník	k1gMnSc2
nebo	nebo	k8xC
v	v	k7c6
manuální	manuální	k2eAgFnSc6d1
ústředně	ústředna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žádané	žádané	k1gNnSc1
číslo	číslo	k1gNnSc1
sdělil	sdělit	k5eAaPmAgMnS
volající	volající	k2eAgMnSc1d1
účastník	účastník	k1gMnSc1
operátorce	operátorka	k1gFnSc6
v	v	k7c6
ústředně	ústředna	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
ručně	ručně	k6eAd1
na	na	k7c6
panelu	panel	k1gInSc6
propojila	propojit	k5eAaPmAgFnS
oba	dva	k4xCgMnPc4
účastníky	účastník	k1gMnPc4
kablíkem	kablík	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
systém	systém	k1gInSc1
se	se	k3xPyFc4
ještě	ještě	k6eAd1
dlouho	dlouho	k6eAd1
používal	používat	k5eAaImAgMnS
na	na	k7c6
železnici	železnice	k1gFnSc6
a	a	k8xC
v	v	k7c6
armádách	armáda	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Ústřední	ústřední	k2eAgFnSc1d1
baterie	baterie	k1gFnSc1
<g/>
,	,	kIx,
pulzní	pulzní	k2eAgFnSc1d1
volba	volba	k1gFnSc1
</s>
<s>
Elektromechanický	elektromechanický	k2eAgInSc1d1
telefon	telefon	k1gInSc1
pro	pro	k7c4
pevnou	pevný	k2eAgFnSc4d1
síť	síť	k1gFnSc4
s	s	k7c7
ústředním	ústřední	k2eAgNnSc7d1
napájením	napájení	k1gNnSc7
a	a	k8xC
automatickou	automatický	k2eAgFnSc7d1
pulzní	pulzní	k2eAgFnSc7d1
volbou	volba	k1gFnSc7
se	se	k3xPyFc4
k	k	k7c3
ústředně	ústředna	k1gFnSc3
připojuje	připojovat	k5eAaImIp3nS
dvojicí	dvojice	k1gFnSc7
vodičů	vodič	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnPc4d1
části	část	k1gFnPc4
přístroje	přístroj	k1gInPc1
jsou	být	k5eAaImIp3nP
uhlíkový	uhlíkový	k2eAgInSc4d1
mikrofon	mikrofon	k1gInSc4
a	a	k8xC
elektromagnetické	elektromagnetický	k2eAgNnSc4d1
sluchátko	sluchátko	k1gNnSc4
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
uložené	uložený	k2eAgInPc1d1
ve	v	k7c4
společné	společný	k2eAgFnPc4d1
rukojeti	rukojeť	k1gFnPc4
a	a	k8xC
spojené	spojený	k2eAgNnSc4d1
s	s	k7c7
přístrojem	přístroj	k1gInSc7
vícežilovým	vícežilův	k2eAgInSc7d1
kabelem	kabel	k1gInSc7
(	(	kIx(
<g/>
„	„	k?
<g/>
šňúrou	šňúra	k1gMnSc7
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastní	vlastní	k2eAgInSc1d1
přístroj	přístroj	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
několik	několik	k4yIc4
přepínačů	přepínač	k1gInPc2
<g/>
,	,	kIx,
zvonek	zvonek	k1gInSc4
a	a	k8xC
otočnou	otočný	k2eAgFnSc4d1
číselnici	číselnice	k1gFnSc4
pro	pro	k7c4
automatickou	automatický	k2eAgFnSc4d1
volbu	volba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
sluchátko	sluchátko	k1gNnSc1
zavěšeno	zavěsit	k5eAaPmNgNnS
ve	v	k7c6
vidlici	vidlice	k1gFnSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
do	do	k7c2
vnějšího	vnější	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
zapojen	zapojen	k2eAgInSc4d1
zvonek	zvonek	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
signalizuje	signalizovat	k5eAaImIp3nS
příchozí	příchozí	k1gMnPc4
hovor	hovor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakmile	jakmile	k8xS
se	se	k3xPyFc4
sluchátko	sluchátko	k1gNnSc1
zvedne	zvednout	k5eAaPmIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
do	do	k7c2
obvodu	obvod	k1gInSc2
zapojeno	zapojen	k2eAgNnSc4d1
sluchátko	sluchátko	k1gNnSc4
a	a	k8xC
mikrofon	mikrofon	k1gInSc4
<g/>
,	,	kIx,
takže	takže	k8xS
účastníci	účastník	k1gMnPc1
mohou	moct	k5eAaImIp3nP
hovořit	hovořit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
výchozí	výchozí	k2eAgInSc4d1
hovor	hovor	k1gInSc4
uživatel	uživatel	k1gMnSc1
na	na	k7c6
otočné	otočný	k2eAgFnSc6d1
číselnici	číselnice	k1gFnSc6
postupně	postupně	k6eAd1
volí	volit	k5eAaImIp3nS
žádané	žádaný	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
účastníka	účastník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
zpětném	zpětný	k2eAgInSc6d1
pohybu	pohyb	k1gInSc6
číselnice	číselnice	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
stejnoměrný	stejnoměrný	k2eAgInSc1d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
obvod	obvod	k1gInSc1
přerušuje	přerušovat	k5eAaImIp3nS
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
se	se	k3xPyFc4
přímo	přímo	k6eAd1
ovládá	ovládat	k5eAaImIp3nS
volič	volič	k1gMnSc1
v	v	k7c6
ústředně	ústředna	k1gFnSc6
<g/>
:	:	kIx,
každý	každý	k3xTgInSc4
pulz	pulz	k1gInSc4
posune	posunout	k5eAaPmIp3nS
raménko	raménko	k1gNnSc1
voliče	volič	k1gInSc2
o	o	k7c4
jeden	jeden	k4xCgInSc4
kontakt	kontakt	k1gInSc4
dále	daleko	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
doběhu	doběh	k1gInSc6
číselnice	číselnice	k1gFnSc2
do	do	k7c2
výchozí	výchozí	k2eAgFnSc2d1
polohy	poloha	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
ústředně	ústředna	k1gFnSc6
aktivuje	aktivovat	k5eAaBmIp3nS
další	další	k2eAgMnSc1d1
<g/>
,	,	kIx,
podřazený	podřazený	k2eAgMnSc1d1
volič	volič	k1gMnSc1
pro	pro	k7c4
následující	následující	k2eAgFnSc4d1
číslici	číslice	k1gFnSc4
voleného	volený	k2eAgNnSc2d1
čísla	číslo	k1gNnSc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
vyzváněcí	vyzváněcí	k2eAgInSc4d1
signál	signál	k1gInSc4
k	k	k7c3
volanému	volaný	k2eAgMnSc3d1
účastníkovi	účastník	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakmile	jakmile	k8xS
je	být	k5eAaImIp3nS
spojení	spojení	k1gNnSc1
navázáno	navázat	k5eAaPmNgNnS
<g/>
,	,	kIx,
zapojí	zapojit	k5eAaPmIp3nS
se	se	k3xPyFc4
do	do	k7c2
obvodu	obvod	k1gInSc2
sluchátko	sluchátko	k1gNnSc1
a	a	k8xC
mikrofon	mikrofon	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
během	během	k7c2
volby	volba	k1gFnSc2
blokován	blokován	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zařízení	zařízení	k1gNnSc1
je	být	k5eAaImIp3nS
neobyčejně	obyčejně	k6eNd1
důmyslné	důmyslný	k2eAgNnSc1d1
<g/>
,	,	kIx,
jednoduché	jednoduchý	k2eAgNnSc1d1
a	a	k8xC
spolehlivé	spolehlivý	k2eAgNnSc1d1
a	a	k8xC
nevyužívá	využívat	k5eNaImIp3nS
žádné	žádný	k3yNgInPc4
aktivní	aktivní	k2eAgInPc4d1
elektronické	elektronický	k2eAgInPc4d1
prvky	prvek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Elektronický	elektronický	k2eAgInSc1d1
telefon	telefon	k1gInSc1
<g/>
,	,	kIx,
tónová	tónový	k2eAgFnSc1d1
volba	volba	k1gFnSc1
</s>
<s>
Od	od	k7c2
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
v	v	k7c6
telefonech	telefon	k1gInPc6
začaly	začít	k5eAaPmAgFnP
používat	používat	k5eAaImF
digitální	digitální	k2eAgInPc4d1
elektronické	elektronický	k2eAgInPc4d1
prvky	prvek	k1gInPc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
umožnilo	umožnit	k5eAaPmAgNnS
řadu	řada	k1gFnSc4
dalších	další	k2eAgNnPc2d1
zdokonalení	zdokonalení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pulzní	pulzní	k2eAgFnSc4d1
volbu	volba	k1gFnSc4
mohla	moct	k5eAaImAgFnS
nahradit	nahradit	k5eAaPmF
tónová	tónový	k2eAgFnSc1d1
volba	volba	k1gFnSc1
a	a	k8xC
otočnou	otočný	k2eAgFnSc4d1
číselnici	číselnice	k1gFnSc4
nahradila	nahradit	k5eAaPmAgFnS
tlačítka	tlačítko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volené	volený	k2eAgFnPc4d1
číslice	číslice	k1gFnPc4
se	se	k3xPyFc4
ukládají	ukládat	k5eAaImIp3nP
do	do	k7c2
paměti	paměť	k1gFnSc2
přístroje	přístroj	k1gInSc2
a	a	k8xC
teprve	teprve	k6eAd1
celá	celý	k2eAgFnSc1d1
volba	volba	k1gFnSc1
se	se	k3xPyFc4
najednou	najednou	k6eAd1
odvysílá	odvysílat	k5eAaPmIp3nS
jako	jako	k9
posloupnost	posloupnost	k1gFnSc4
krátkých	krátký	k2eAgInPc2d1
pulzů	pulz	k1gInPc2
o	o	k7c6
různém	různý	k2eAgInSc6d1
kmitočtu	kmitočet	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
jednak	jednak	k8xC
spolehlivější	spolehlivý	k2eAgFnSc1d2
<g/>
,	,	kIx,
jednak	jednak	k8xC
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
zvýšila	zvýšit	k5eAaPmAgFnS
průchodnost	průchodnost	k1gFnSc1
ústředny	ústředna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
drátech	drát	k1gInPc6
se	se	k3xPyFc4
tak	tak	k6eAd1
přenášejí	přenášet	k5eAaImIp3nP
pouze	pouze	k6eAd1
akustické	akustický	k2eAgInPc1d1
signály	signál	k1gInPc1
a	a	k8xC
přístroj	přístroj	k1gInSc1
nemusí	muset	k5eNaImIp3nS
být	být	k5eAaImF
s	s	k7c7
ústřednou	ústředna	k1gFnSc7
galvanicky	galvanicky	k6eAd1
propojen	propojen	k2eAgMnSc1d1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
umožnilo	umožnit	k5eAaPmAgNnS
dálkové	dálkový	k2eAgInPc4d1
přenosy	přenos	k1gInPc4
bezdrátovou	bezdrátový	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlasitost	hlasitost	k1gFnSc1
telefonu	telefon	k1gInSc2
lze	lze	k6eAd1
regulovat	regulovat	k5eAaImF
<g/>
,	,	kIx,
různé	různý	k2eAgInPc1d1
údaje	údaj	k1gInPc1
se	se	k3xPyFc4
zobrazují	zobrazovat	k5eAaImIp3nP
na	na	k7c6
displeji	displej	k1gInSc6
<g/>
,	,	kIx,
často	často	k6eAd1
používaná	používaný	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
si	se	k3xPyFc3
přístroj	přístroj	k1gInSc1
může	moct	k5eAaImIp3nS
pamatovat	pamatovat	k5eAaImF
atd.	atd.	kA
Protože	protože	k8xS
se	se	k3xPyFc4
však	však	k9
přechod	přechod	k1gInSc4
milionů	milion	k4xCgInPc2
stanic	stanice	k1gFnPc2
na	na	k7c4
tónovou	tónový	k2eAgFnSc4d1
volbu	volba	k1gFnSc4
musel	muset	k5eAaImAgMnS
dělat	dělat	k5eAaImF
postupně	postupně	k6eAd1
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
zařízení	zařízení	k1gNnSc1
zkonstruováno	zkonstruovat	k5eAaPmNgNnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
i	i	k9
se	s	k7c7
staršími	starý	k2eAgFnPc7d2
ústřednami	ústředna	k1gFnPc7
kompatibilní	kompatibilní	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkové	celkový	k2eAgInPc1d1
schéma	schéma	k1gNnSc4
telefonního	telefonní	k2eAgInSc2d1
přístroje	přístroj	k1gInSc2
se	se	k3xPyFc4
silně	silně	k6eAd1
zkomplikovalo	zkomplikovat	k5eAaPmAgNnS
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
principu	princip	k1gInSc6
nezměnilo	změnit	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Bezšňůrový	bezšňůrový	k2eAgInSc1d1
telefon	telefon	k1gInSc1
</s>
<s>
Menší	malý	k2eAgNnSc4d2
zdokonalení	zdokonalení	k1gNnSc4
přinesly	přinést	k5eAaPmAgInP
bezšňůrové	bezšňůrový	k2eAgInPc1d1
telefony	telefon	k1gInPc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
přístroj	přístroj	k1gInSc1
není	být	k5eNaImIp3nS
se	s	k7c7
sluchátkem	sluchátko	k1gNnSc7
spojen	spojit	k5eAaPmNgInS
šňůrou	šňůra	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
bezdrátově	bezdrátově	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosah	dosah	k1gInSc1
tohoto	tento	k3xDgNnSc2
spojení	spojení	k1gNnSc2
činí	činit	k5eAaImIp3nS
desítky	desítka	k1gFnPc4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
takže	takže	k8xS
se	s	k7c7
sluchátkem	sluchátko	k1gNnSc7
se	se	k3xPyFc4
lze	lze	k6eAd1
volně	volně	k6eAd1
pohybovat	pohybovat	k5eAaImF
například	například	k6eAd1
po	po	k7c6
bytě	byt	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinak	jinak	k6eAd1
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
funkce	funkce	k1gFnSc1
telefonu	telefon	k1gInSc2
nezměnila	změnit	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Alexander	Alexandra	k1gFnPc2
Graham	Graham	k1gMnSc1
Bell	bell	k1gInSc1
Laboratory	Laborator	k1gMnPc4
Notebook	notebook	k1gInSc1
<g/>
,	,	kIx,
1875-1876	1875-1876	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1875-1876	1875-1876	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
NEMRAVA	Nemrava	k1gMnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc4
století	století	k1gNnPc2
s	s	k7c7
telefonem	telefon	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
PTT	PTT	kA
REVUE	revue	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
1981	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
322	#num#	k4
<g/>
–	–	k?
<g/>
8207	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Ottův	Ottův	k2eAgInSc4d1
slovník	slovník	k1gInSc4
naučný	naučný	k2eAgInSc4d1
nové	nový	k2eAgNnSc4d1
doby	doba	k1gFnPc4
<g/>
,	,	kIx,
sv.	sv.	kA
12	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
1016	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
J.	J.	kA
Boldiš	Boldiš	k1gMnSc1
<g/>
,	,	kIx,
Telefonie	telefonie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nadas	Nadas	k1gInSc1
1981	#num#	k4
</s>
<s>
K.	K.	kA
Mašek	Mašek	k1gMnSc1
<g/>
,	,	kIx,
Telefonie	telefonie	k1gFnSc1
pro	pro	k7c4
střední	střední	k2eAgFnPc4d1
školy	škola	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nadas	Nadas	k1gInSc1
1981	#num#	k4
</s>
<s>
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
naučný	naučný	k2eAgInSc1d1
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
Telefon	telefon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sv.	sv.	kA
25	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
159	#num#	k4
</s>
<s>
Ottův	Ottův	k2eAgInSc4d1
slovník	slovník	k1gInSc4
naučný	naučný	k2eAgInSc4d1
nové	nový	k2eAgNnSc4d1
doby	doba	k1gFnPc4
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
Telefon	telefon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sv.	sv.	kA
12	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
1016	#num#	k4
</s>
<s>
ZÁBĚHLICKÝ	záběhlický	k2eAgMnSc1d1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
pošty	pošta	k1gFnSc2
<g/>
,	,	kIx,
telegrafu	telegraf	k1gInSc2
a	a	k8xC
telefonu	telefon	k1gInSc2
v	v	k7c6
československých	československý	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
(	(	kIx(
<g/>
Od	od	k7c2
nejstarších	starý	k2eAgFnPc2d3
dob	doba	k1gFnPc2
až	až	k9
do	do	k7c2
převratu	převrat	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
vl	vl	k?
<g/>
.	.	kIx.
n.	n.	k?
<g/>
,	,	kIx,
[	[	kIx(
<g/>
1928	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
422	#num#	k4
s.	s.	k?
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Telefonie	telefonie	k1gFnSc1
-	-	kIx~
technologie	technologie	k1gFnSc2
používané	používaný	k2eAgFnSc2d1
pro	pro	k7c4
telefonování	telefonování	k1gNnSc4
</s>
<s>
Telefonní	telefonní	k2eAgFnSc1d1
ústředna	ústředna	k1gFnSc1
-	-	kIx~
zařízení	zařízení	k1gNnPc4
pro	pro	k7c4
spojování	spojování	k1gNnPc4
účastníků	účastník	k1gMnPc2
</s>
<s>
Telefonní	telefonní	k2eAgFnSc1d1
síť	síť	k1gFnSc1
</s>
<s>
Telefonní	telefonní	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
ISDN	ISDN	kA
-	-	kIx~
digitální	digitální	k2eAgFnSc1d1
technika	technika	k1gFnSc1
<g/>
,	,	kIx,
zahrnující	zahrnující	k2eAgFnSc1d1
kromě	kromě	k7c2
telefonie	telefonie	k1gFnSc2
i	i	k8xC
přenos	přenos	k1gInSc1
faxů	fax	k1gInPc2
<g/>
,	,	kIx,
obrazu	obraz	k1gInSc2
<g/>
,	,	kIx,
dat	datum	k1gNnPc2
</s>
<s>
Modem	modem	k1gInSc1
-	-	kIx~
zařízení	zařízení	k1gNnSc1
pro	pro	k7c4
přenos	přenos	k1gInSc4
dat	datum	k1gNnPc2
přes	přes	k7c4
telefonní	telefonní	k2eAgFnSc4d1
linku	linka	k1gFnSc4
</s>
<s>
Mobilní	mobilní	k2eAgInSc4d1
telefon	telefon	k1gInSc4
-	-	kIx~
přenosný	přenosný	k2eAgInSc4d1
telefon	telefon	k1gInSc4
využívající	využívající	k2eAgFnSc1d1
bezdrátového	bezdrátový	k2eAgInSc2d1
přenosu	přenos	k1gInSc2
</s>
<s>
IP	IP	kA
telefon	telefon	k1gInSc1
-	-	kIx~
telefonní	telefonní	k2eAgInSc1d1
přístroj	přístroj	k1gInSc1
pro	pro	k7c4
přímou	přímý	k2eAgFnSc4d1
komunikaci	komunikace	k1gFnSc4
pomocí	pomocí	k7c2
VoIP	VoIP	k1gFnSc2
</s>
<s>
VoIP	VoIP	k?
-	-	kIx~
přenos	přenos	k1gInSc1
hlasu	hlas	k1gInSc2
protokolem	protokol	k1gInSc7
IP	IP	kA
přes	přes	k7c4
datové	datový	k2eAgFnPc4d1
sítě	síť	k1gFnPc4
</s>
<s>
RSU	RSU	kA
-	-	kIx~
vzdálená	vzdálený	k2eAgFnSc1d1
účastnická	účastnický	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
telefon	telefon	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
telefon	telefon	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Virtuální	virtuální	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
telefonů	telefon	k1gInPc2
-	-	kIx~
de	de	k?
</s>
<s>
Základy	základ	k1gInPc1
telekomunukační	telekomunukační	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4016854-2	4016854-2	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
9761	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85133365	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85133365	#num#	k4
</s>
