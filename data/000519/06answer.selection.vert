<s>
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
jsou	být	k5eAaImIp3nP	být
deník	deník	k1gInSc4	deník
založený	založený	k2eAgInSc4d1	založený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
novinářem	novinář	k1gMnSc7	novinář
a	a	k8xC	a
politikem	politik	k1gMnSc7	politik
Adolfem	Adolf	k1gMnSc7	Adolf
Stránským	Stránský	k1gMnSc7	Stránský
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
pravidelně	pravidelně	k6eAd1	pravidelně
publikovala	publikovat	k5eAaBmAgFnS	publikovat
řada	řada	k1gFnSc1	řada
známých	známý	k2eAgMnPc2d1	známý
českých	český	k2eAgMnPc2d1	český
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
.	.	kIx.	.
</s>
