<s>
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
jsou	být	k5eAaImIp3nP	být
deník	deník	k1gInSc4	deník
založený	založený	k2eAgInSc4d1	založený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
novinářem	novinář	k1gMnSc7	novinář
a	a	k8xC	a
politikem	politik	k1gMnSc7	politik
Adolfem	Adolf	k1gMnSc7	Adolf
Stránským	Stránský	k1gMnSc7	Stránský
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
pravidelně	pravidelně	k6eAd1	pravidelně
publikovala	publikovat	k5eAaBmAgFnS	publikovat
řada	řada	k1gFnSc1	řada
známých	známý	k2eAgMnPc2d1	známý
českých	český	k2eAgMnPc2d1	český
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Staly	stát	k5eAaPmAgFnP	stát
se	se	k3xPyFc4	se
nástupcem	nástupce	k1gMnSc7	nástupce
Moravských	moravský	k2eAgInPc2d1	moravský
listů	list	k1gInPc2	list
vycházejících	vycházející	k2eAgInPc2d1	vycházející
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
a	a	k8xC	a
přejmenovaných	přejmenovaný	k2eAgFnPc2d1	přejmenovaná
na	na	k7c4	na
Lidové	lidový	k2eAgFnPc4d1	lidová
noviny	novina	k1gFnPc4	novina
na	na	k7c4	na
popud	popud	k1gInSc4	popud
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
tento	tento	k3xDgInSc1	tento
deník	deník	k1gInSc1	deník
požíval	požívat	k5eAaImAgInS	požívat
vysoké	vysoký	k2eAgFnPc4d1	vysoká
vážnosti	vážnost	k1gFnPc4	vážnost
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Protože	protože	k8xS	protože
vycházel	vycházet	k5eAaImAgMnS	vycházet
i	i	k9	i
za	za	k7c2	za
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
změněno	změnit	k5eAaPmNgNnS	změnit
na	na	k7c4	na
Svobodné	svobodný	k2eAgFnPc4d1	svobodná
noviny	novina	k1gFnPc4	novina
(	(	kIx(	(
<g/>
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
původnímu	původní	k2eAgInSc3d1	původní
názvu	název	k1gInSc3	název
od	od	k7c2	od
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
komunistickém	komunistický	k2eAgInSc6d1	komunistický
převratu	převrat	k1gInSc6	převrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
bylo	být	k5eAaImAgNnS	být
vydávání	vydávání	k1gNnSc1	vydávání
tohoto	tento	k3xDgInSc2	tento
deníku	deník	k1gInSc2	deník
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
zastaveno	zastavit	k5eAaPmNgNnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
bylo	být	k5eAaImAgNnS	být
vydávání	vydávání	k1gNnSc1	vydávání
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
ilegálně	ilegálně	k6eAd1	ilegálně
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
legálně	legálně	k6eAd1	legálně
<g/>
.	.	kIx.	.
</s>
<s>
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
se	se	k3xPyFc4	se
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
věnovaly	věnovat	k5eAaPmAgFnP	věnovat
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politice	politika	k1gFnSc3	politika
a	a	k8xC	a
kultuře	kultura	k1gFnSc3	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgInPc4	první
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
uveřejňovaly	uveřejňovat	k5eAaImAgInP	uveřejňovat
politickou	politický	k2eAgFnSc4d1	politická
karikaturu	karikatura	k1gFnSc4	karikatura
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
okruhu	okruh	k1gInSc2	okruh
jejich	jejich	k3xOp3gMnPc2	jejich
přispěvatelů	přispěvatel	k1gMnPc2	přispěvatel
a	a	k8xC	a
redaktorů	redaktor	k1gMnPc2	redaktor
patřili	patřit	k5eAaImAgMnP	patřit
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
Bass	Bass	k1gMnSc1	Bass
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Poláček	Poláček	k1gMnSc1	Poláček
<g/>
,	,	kIx,	,
Edvard	Edvard	k1gMnSc1	Edvard
Valenta	Valenta	k1gMnSc1	Valenta
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Těsnohlídek	Těsnohlídka	k1gFnPc2	Těsnohlídka
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Mahen	Mahen	k1gInSc1	Mahen
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Drda	Drda	k1gMnSc1	Drda
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Řezáč	Řezáč	k1gMnSc1	Řezáč
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
prezidenti	prezident	k1gMnPc1	prezident
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gNnSc1	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
a	a	k8xC	a
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
tento	tento	k3xDgInSc1	tento
titul	titul	k1gInSc4	titul
obnovila	obnovit	k5eAaPmAgFnS	obnovit
skupina	skupina	k1gFnSc1	skupina
disidentů	disident	k1gMnPc2	disident
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
vytvářet	vytvářet	k5eAaImF	vytvářet
nezávislé	závislý	k2eNgFnPc4d1	nezávislá
noviny	novina	k1gFnPc4	novina
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
počátky	počátek	k1gInPc7	počátek
novodobého	novodobý	k2eAgNnSc2d1	novodobé
vydávání	vydávání	k1gNnSc2	vydávání
těchto	tento	k3xDgFnPc2	tento
novin	novina	k1gFnPc2	novina
jsou	být	k5eAaImIp3nP	být
spojeni	spojen	k2eAgMnPc1d1	spojen
lidé	člověk	k1gMnPc1	člověk
jako	jako	k8xS	jako
Jiří	Jiří	k1gMnSc1	Jiří
Ruml	Ruml	k1gMnSc1	Ruml
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Dienstbier	Dienstbier	k1gMnSc1	Dienstbier
a	a	k8xC	a
Ladislav	Ladislav	k1gMnSc1	Ladislav
Hejdánek	Hejdánek	k1gMnSc1	Hejdánek
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
noviny	novina	k1gFnPc4	novina
oficiálně	oficiálně	k6eAd1	oficiálně
zaregistrovat	zaregistrovat	k5eAaPmF	zaregistrovat
a	a	k8xC	a
vydávat	vydávat	k5eAaImF	vydávat
jako	jako	k8xC	jako
měsíčník	měsíčník	k1gInSc4	měsíčník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1987	[number]	k4	1987
vyšla	vyjít	k5eAaPmAgFnS	vyjít
dvě	dva	k4xCgNnPc4	dva
nultá	nultý	k4xOgNnPc4	nultý
čísla	číslo	k1gNnPc4	číslo
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc4	první
číslo	číslo	k1gNnSc4	číslo
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
noviny	novina	k1gFnPc4	novina
nedařilo	dařit	k5eNaImAgNnS	dařit
až	až	k9	až
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
oficiálně	oficiálně	k6eAd1	oficiálně
zaregistrovat	zaregistrovat	k5eAaPmF	zaregistrovat
<g/>
,	,	kIx,	,
museli	muset	k5eAaImAgMnP	muset
je	on	k3xPp3gNnSc4	on
tvůrci	tvůrce	k1gMnPc1	tvůrce
šířit	šířit	k5eAaImF	šířit
jako	jako	k9	jako
samizdat	samizdat	k1gInSc4	samizdat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1990	[number]	k4	1990
začaly	začít	k5eAaPmAgFnP	začít
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
vycházet	vycházet	k5eAaImF	vycházet
jako	jako	k8xS	jako
deník	deník	k1gInSc1	deník
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgFnP	být
sloučeny	sloučit	k5eAaPmNgInP	sloučit
se	s	k7c7	s
zanikající	zanikající	k2eAgFnSc7d1	zanikající
Lidovou	lidový	k2eAgFnSc7d1	lidová
demokracií	demokracie	k1gFnSc7	demokracie
(	(	kIx(	(
<g/>
deník	deník	k1gInSc1	deník
Československé	československý	k2eAgFnSc2d1	Československá
strany	strana	k1gFnSc2	strana
lidové	lidový	k2eAgFnSc2d1	lidová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
které	který	k3yIgFnSc2	který
převzaly	převzít	k5eAaPmAgFnP	převzít
modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
titulu	titul	k1gInSc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
Deník	deník	k1gInSc1	deník
patřil	patřit	k5eAaImAgInS	patřit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
do	do	k7c2	do
koncernu	koncern	k1gInSc2	koncern
Rheinisch-Bergische	Rheinisch-Bergisch	k1gInSc2	Rheinisch-Bergisch
Druckerei-	Druckerei-	k1gMnPc2	Druckerei-
und	und	k?	und
Verlagsgesellschaft	Verlagsgesellschaftum	k1gNnPc2	Verlagsgesellschaftum
<g/>
,	,	kIx,	,
vydavatele	vydavatel	k1gMnSc4	vydavatel
deníku	deník	k1gInSc2	deník
Rheinische	Rheinische	k1gInSc1	Rheinische
Post	post	k1gInSc1	post
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
rovněž	rovněž	k9	rovněž
vydával	vydávat	k5eAaPmAgInS	vydávat
deník	deník	k1gInSc1	deník
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
DNES	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
koupil	koupit	k5eAaPmAgMnS	koupit
vydavatelství	vydavatelství	k1gNnSc4	vydavatelství
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
jeho	jeho	k3xOp3gFnSc1	jeho
firma	firma	k1gFnSc1	firma
Agrofert	Agrofert	k1gInSc4	Agrofert
<g/>
.	.	kIx.	.
</s>
<s>
Emil	Emil	k1gMnSc1	Emil
Čermák	Čermák	k1gMnSc1	Čermák
(	(	kIx(	(
<g/>
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
1893	[number]	k4	1893
<g/>
-	-	kIx~	-
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
prchl	prchnout	k5eAaPmAgInS	prchnout
před	před	k7c7	před
sňatkem	sňatek	k1gInSc7	sňatek
do	do	k7c2	do
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
dopisovatel	dopisovatel	k1gMnSc1	dopisovatel
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
novin	novina	k1gFnPc2	novina
a	a	k8xC	a
agentur	agentura	k1gFnPc2	agentura
<g/>
.	.	kIx.	.
</s>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Štěchovský	Štěchovský	k2eAgMnSc1d1	Štěchovský
(	(	kIx(	(
<g/>
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
1895	[number]	k4	1895
<g/>
-	-	kIx~	-
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
řídil	řídit	k5eAaImAgInS	řídit
redakci	redakce	k1gFnSc4	redakce
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
téměř	téměř	k6eAd1	téměř
čtvrtstoletí	čtvrtstoletí	k1gNnSc2	čtvrtstoletí
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jeho	jeho	k3xOp3gNnSc2	jeho
působení	působení	k1gNnSc2	působení
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
zakladatel	zakladatel	k1gMnSc1	zakladatel
LN	LN	kA	LN
Adolf	Adolf	k1gMnSc1	Adolf
Stránský	Stránský	k1gMnSc1	Stránský
výhradním	výhradní	k2eAgMnSc7d1	výhradní
majitelem	majitel	k1gMnSc7	majitel
listu	list	k1gInSc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Arnošt	Arnošt	k1gMnSc1	Arnošt
Heinrich	Heinrich	k1gMnSc1	Heinrich
(	(	kIx(	(
<g/>
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
1919	[number]	k4	1919
<g/>
-	-	kIx~	-
<g/>
1927	[number]	k4	1927
a	a	k8xC	a
1929	[number]	k4	1929
<g/>
-	-	kIx~	-
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
poprvé	poprvé	k6eAd1	poprvé
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
do	do	k7c2	do
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1919	[number]	k4	1919
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
třetím	třetí	k4xOgMnSc7	třetí
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
noviny	novina	k1gFnSc2	novina
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
post	post	k1gInSc4	post
šéfredaktora	šéfredaktor	k1gMnSc2	šéfredaktor
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
v	v	k7c6	v
září	září	k1gNnSc6	září
1929	[number]	k4	1929
a	a	k8xC	a
setrval	setrvat	k5eAaPmAgMnS	setrvat
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
až	až	k6eAd1	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Z.	Z.	kA	Z.
Klíma	Klíma	k1gMnSc1	Klíma
(	(	kIx(	(
<g/>
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
1927	[number]	k4	1927
<g/>
-	-	kIx~	-
<g/>
1929	[number]	k4	1929
a	a	k8xC	a
1939	[number]	k4	1939
<g/>
-	-	kIx~	-
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
bývalý	bývalý	k2eAgMnSc1d1	bývalý
stálý	stálý	k2eAgMnSc1d1	stálý
zpravodaj	zpravodaj	k1gMnSc1	zpravodaj
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
dočasně	dočasně	k6eAd1	dočasně
obsadil	obsadit	k5eAaPmAgInS	obsadit
post	post	k1gInSc1	post
šéfredaktora	šéfredaktor	k1gMnSc2	šéfredaktor
po	po	k7c6	po
Heinrichově	Heinrichův	k2eAgInSc6d1	Heinrichův
odchodu	odchod	k1gInSc6	odchod
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
opustil	opustit	k5eAaPmAgMnS	opustit
LN	LN	kA	LN
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
řídil	řídit	k5eAaImAgMnS	řídit
České	český	k2eAgNnSc4d1	české
slovo	slovo	k1gNnSc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
LN	LN	kA	LN
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
redakce	redakce	k1gFnSc2	redakce
jej	on	k3xPp3gMnSc4	on
zatklo	zatknout	k5eAaPmAgNnS	zatknout
gestapo	gestapo	k1gNnSc1	gestapo
a	a	k8xC	a
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1942	[number]	k4	1942
byl	být	k5eAaImAgInS	být
popraven	popravit	k5eAaPmNgInS	popravit
v	v	k7c6	v
Terezíně	Terezín	k1gInSc6	Terezín
<g/>
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
Bass	Bass	k1gMnSc1	Bass
(	(	kIx(	(
<g/>
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
1933	[number]	k4	1933
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
a	a	k8xC	a
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
jmenován	jmenován	k2eAgMnSc1d1	jmenován
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Heinricha	Heinrich	k1gMnSc2	Heinrich
jako	jako	k8xS	jako
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
redaktor	redaktor	k1gMnSc1	redaktor
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
Klapzubovy	Klapzubův	k2eAgFnSc2d1	Klapzubova
jedenáctky	jedenáctka	k1gFnSc2	jedenáctka
se	se	k3xPyFc4	se
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
redakce	redakce	k1gFnSc2	redakce
vrátil	vrátit	k5eAaPmAgMnS	vrátit
po	po	k7c4	po
osvobození	osvobození	k1gNnSc4	osvobození
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1945	[number]	k4	1945
však	však	k9	však
přepustil	přepustit	k5eAaPmAgInS	přepustit
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
Ferdinandu	Ferdinand	k1gMnSc3	Ferdinand
Peroutkovi	Peroutka	k1gMnSc3	Peroutka
<g/>
.	.	kIx.	.
</s>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
Zeman	Zeman	k1gMnSc1	Zeman
(	(	kIx(	(
<g/>
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
1941	[number]	k4	1941
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
dosazen	dosadit	k5eAaPmNgInS	dosadit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
šéfredaktora	šéfredaktor	k1gMnSc2	šéfredaktor
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
souzen	soudit	k5eAaImNgInS	soudit
lidovým	lidový	k2eAgInSc7d1	lidový
soudem	soud	k1gInSc7	soud
pro	pro	k7c4	pro
kolaboraci	kolaborace	k1gFnSc4	kolaborace
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Peroutka	Peroutka	k1gMnSc1	Peroutka
<g/>
,	,	kIx,	,
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
LN	LN	kA	LN
nejprve	nejprve	k6eAd1	nejprve
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
1924	[number]	k4	1924
do	do	k7c2	do
února	únor	k1gInSc2	únor
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
novin	novina	k1gFnPc2	novina
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
a	a	k8xC	a
působil	působit	k5eAaImAgMnS	působit
zde	zde	k6eAd1	zde
až	až	k9	až
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
zatčení	zatčení	k1gNnSc2	zatčení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
Svobodných	svobodný	k2eAgFnPc2d1	svobodná
novin	novina	k1gFnPc2	novina
(	(	kIx(	(
<g/>
poválečný	poválečný	k2eAgInSc1d1	poválečný
název	název	k1gInSc1	název
LN	LN	kA	LN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
řídil	řídit	k5eAaImAgMnS	řídit
rádio	rádio	k1gNnSc4	rádio
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Drda	Drda	k1gMnSc1	Drda
<g/>
,	,	kIx,	,
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
1948	[number]	k4	1948
<g/>
-	-	kIx~	-
<g/>
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc4	tento
komunistický	komunistický	k2eAgMnSc1d1	komunistický
novinář	novinář	k1gMnSc1	novinář
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
LN	LN	kA	LN
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
noviny	novina	k1gFnPc1	novina
rozvázaly	rozvázat	k5eAaPmAgFnP	rozvázat
pracovní	pracovní	k2eAgInSc4d1	pracovní
poměr	poměr	k1gInSc4	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
převratu	převrat	k1gInSc6	převrat
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
a	a	k8xC	a
řídil	řídit	k5eAaImAgMnS	řídit
noviny	novina	k1gFnPc4	novina
až	až	k9	až
do	do	k7c2	do
jejich	jejich	k3xOp3gInSc2	jejich
zániku	zánik	k1gInSc2	zánik
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
A.	A.	kA	A.
J.	J.	kA	J.
Liehm	Liehm	k1gMnSc1	Liehm
<g/>
,	,	kIx,	,
designovaný	designovaný	k2eAgMnSc1d1	designovaný
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
během	během	k7c2	během
Pražského	pražský	k2eAgNnSc2d1	Pražské
jara	jaro	k1gNnSc2	jaro
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
myšlenka	myšlenka	k1gFnSc1	myšlenka
obnovit	obnovit	k5eAaPmF	obnovit
po	po	k7c6	po
16	[number]	k4	16
letech	léto	k1gNnPc6	léto
vydávání	vydávání	k1gNnSc2	vydávání
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Znovuzrození	znovuzrození	k1gNnSc1	znovuzrození
deníku	deník	k1gInSc2	deník
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
designovaným	designovaný	k2eAgMnSc7d1	designovaný
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
A.	A.	kA	A.
J.	J.	kA	J.
Liehm	Liehma	k1gFnPc2	Liehma
<g/>
,	,	kIx,	,
však	však	k9	však
zhatila	zhatit	k5eAaPmAgFnS	zhatit
srpnová	srpnový	k2eAgFnSc1d1	srpnová
invaze	invaze	k1gFnSc1	invaze
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Ruml	Ruml	k1gMnSc1	Ruml
<g/>
,	,	kIx,	,
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
1988	[number]	k4	1988
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
signatářů	signatář	k1gMnPc2	signatář
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
obnově	obnova	k1gFnSc6	obnova
vydávání	vydávání	k1gNnSc2	vydávání
LN	LN	kA	LN
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
samizdatových	samizdatový	k2eAgFnPc2d1	samizdatová
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1989	[number]	k4	1989
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
pobuřování	pobuřování	k1gNnSc2	pobuřování
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
listopadovém	listopadový	k2eAgInSc6d1	listopadový
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
převratu	převrat	k1gInSc2	převrat
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
šéfem	šéf	k1gMnSc7	šéf
obnovených	obnovený	k2eAgFnPc2d1	obnovená
LN	LN	kA	LN
a	a	k8xC	a
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
setrval	setrvat	k5eAaPmAgMnS	setrvat
do	do	k7c2	do
června	červen	k1gInSc2	červen
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
za	za	k7c4	za
Občanské	občanský	k2eAgNnSc4d1	občanské
fórum	fórum	k1gNnSc4	fórum
poslancem	poslanec	k1gMnSc7	poslanec
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
ČSSR	ČSSR	kA	ČSSR
<g/>
.	.	kIx.	.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
signatářů	signatář	k1gMnPc2	signatář
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
vystřídal	vystřídat	k5eAaPmAgInS	vystřídat
Jiřího	Jiří	k1gMnSc4	Jiří
Rumla	Ruml	k1gMnSc4	Ruml
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
odchodu	odchod	k1gInSc6	odchod
do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samizdatových	samizdatový	k2eAgFnPc6d1	samizdatová
LN	LN	kA	LN
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc3	funkce
zástupce	zástupce	k1gMnSc2	zástupce
šéfredaktora	šéfredaktor	k1gMnSc2	šéfredaktor
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
čemuž	což	k3yQnSc3	což
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
až	až	k8xS	až
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
vězněn	věznit	k5eAaImNgMnS	věznit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
LN	LN	kA	LN
jako	jako	k8xC	jako
editor	editor	k1gInSc1	editor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1998	[number]	k4	1998
syndik	syndik	k1gMnSc1	syndik
Syndikátu	syndikát	k1gInSc2	syndikát
novinářů	novinář	k1gMnPc2	novinář
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Veis	Veis	k1gInSc1	Veis
<g/>
,	,	kIx,	,
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
nejdříve	dříve	k6eAd3	dříve
byl	být	k5eAaImAgInS	být
komentátorem	komentátor	k1gMnSc7	komentátor
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
července	červenec	k1gInSc2	červenec
1991	[number]	k4	1991
jejich	jejich	k3xOp3gMnSc7	jejich
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
programový	programový	k2eAgMnSc1d1	programový
ředitel	ředitel	k1gMnSc1	ředitel
Centra	centrum	k1gNnSc2	centrum
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
žurnalistiky	žurnalistika	k1gFnSc2	žurnalistika
a	a	k8xC	a
komentátor	komentátor	k1gMnSc1	komentátor
jiných	jiný	k2eAgFnPc2d1	jiná
novin	novina	k1gFnPc2	novina
a	a	k8xC	a
časopisů	časopis	k1gInPc2	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
poradce	poradce	k1gMnSc4	poradce
Petra	Petr	k1gMnSc4	Petr
Pitharta	Pithart	k1gMnSc4	Pithart
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
místopředsedy	místopředseda	k1gMnPc7	místopředseda
Senátu	senát	k1gInSc2	senát
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Smetánka	smetánka	k1gFnSc1	smetánka
<g/>
,	,	kIx,	,
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
přispíval	přispívat	k5eAaImAgInS	přispívat
již	již	k9	již
do	do	k7c2	do
samizdatových	samizdatový	k2eAgInPc2d1	samizdatový
LN	LN	kA	LN
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
redaktor	redaktor	k1gMnSc1	redaktor
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
rubriky	rubrika	k1gFnSc2	rubrika
<g/>
,	,	kIx,	,
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
1991	[number]	k4	1991
do	do	k7c2	do
května	květen	k1gInSc2	květen
1992	[number]	k4	1992
zástupcem	zástupce	k1gMnSc7	zástupce
šéfredaktora	šéfredaktor	k1gMnSc2	šéfredaktor
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
rok	rok	k1gInSc4	rok
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
LN	LN	kA	LN
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
diplomat	diplomat	k1gMnSc1	diplomat
<g/>
.	.	kIx.	.
</s>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Štětina	štětina	k1gFnSc1	štětina
<g/>
,	,	kIx,	,
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
1993	[number]	k4	1993
<g/>
-	-	kIx~	-
<g/>
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Petrou	Petra	k1gFnSc7	Petra
Procházkovou	Procházková	k1gFnSc7	Procházková
proslul	proslout	k5eAaPmAgInS	proslout
jako	jako	k9	jako
válečný	válečný	k2eAgInSc1d1	válečný
zpravodaj	zpravodaj	k1gInSc1	zpravodaj
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
LN	LN	kA	LN
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1989	[number]	k4	1989
jako	jako	k8xC	jako
zpravodaj	zpravodaj	k1gMnSc1	zpravodaj
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
bývalého	bývalý	k2eAgInSc2d1	bývalý
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
založil	založit	k5eAaPmAgInS	založit
a	a	k8xC	a
vedl	vést	k5eAaImAgInS	vést
Nadaci	nadace	k1gFnSc4	nadace
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
června	červen	k1gInSc2	červen
1993	[number]	k4	1993
byl	být	k5eAaImAgMnS	být
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
senátorem	senátor	k1gMnSc7	senátor
Parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1	Jiří
Kryšpín	Kryšpín	k1gMnSc1	Kryšpín
<g/>
,	,	kIx,	,
pověřen	pověřen	k2eAgMnSc1d1	pověřen
řízením	řízení	k1gNnSc7	řízení
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
až	až	k8xS	až
březnu	březen	k1gInSc6	březen
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
redakci	redakce	k1gFnSc4	redakce
řídil	řídit	k5eAaImAgInS	řídit
jen	jen	k9	jen
první	první	k4xOgInPc4	první
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
soukromé	soukromý	k2eAgFnSc6d1	soukromá
sféře	sféra	k1gFnSc6	sféra
<g/>
.	.	kIx.	.
</s>
<s>
Libor	Libor	k1gMnSc1	Libor
Ševčík	Ševčík	k1gMnSc1	Ševčík
<g/>
,	,	kIx,	,
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
1994	[number]	k4	1994
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
před	před	k7c7	před
listopadem	listopad	k1gInSc7	listopad
1989	[number]	k4	1989
byl	být	k5eAaImAgMnS	být
redaktorem	redaktor	k1gMnSc7	redaktor
Mladé	mladý	k2eAgFnSc2d1	mladá
fronty	fronta	k1gFnSc2	fronta
<g/>
,	,	kIx,	,
po	po	k7c6	po
převratu	převrat	k1gInSc6	převrat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
jejím	její	k3xOp3gMnSc7	její
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
<g/>
.	.	kIx.	.
</s>
<s>
LN	LN	kA	LN
řídil	řídit	k5eAaImAgInS	řídit
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
LN	LN	kA	LN
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
denících	deník	k1gInPc6	deník
Slovo	slovo	k1gNnSc1	slovo
a	a	k8xC	a
Hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
noviny	novina	k1gFnPc1	novina
<g/>
.	.	kIx.	.
</s>
<s>
Jefim	Jefim	k1gMnSc1	Jefim
Fištejn	Fištejn	k1gMnSc1	Fištejn
<g/>
,	,	kIx,	,
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
1996	[number]	k4	1996
<g/>
-	-	kIx~	-
<g/>
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
komentátor	komentátor	k1gMnSc1	komentátor
a	a	k8xC	a
zástupce	zástupce	k1gMnSc1	zástupce
programového	programový	k2eAgMnSc2d1	programový
ředitele	ředitel	k1gMnSc2	ředitel
Rádia	rádio	k1gNnSc2	rádio
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
do	do	k7c2	do
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
jako	jako	k8xC	jako
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
odešel	odejít	k5eAaPmAgMnS	odejít
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Rádia	rádio	k1gNnSc2	rádio
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
publicista	publicista	k1gMnSc1	publicista
<g/>
.	.	kIx.	.
</s>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Šafr	Šafr	k1gMnSc1	Šafr
<g/>
,	,	kIx,	,
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
před	před	k7c7	před
Lidovými	lidový	k2eAgFnPc7d1	lidová
novinami	novina	k1gFnPc7	novina
vedl	vést	k5eAaImAgInS	vést
Český	český	k2eAgInSc1d1	český
deník	deník	k1gInSc1	deník
a	a	k8xC	a
Telegraf	telegraf	k1gInSc1	telegraf
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k9	jako
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
přešel	přejít	k5eAaPmAgMnS	přejít
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
šéfredaktora	šéfredaktor	k1gMnSc2	šéfredaktor
deníku	deník	k1gInSc2	deník
Mladá	mladá	k1gFnSc1	mladá
fronta	fronta	k1gFnSc1	fronta
DNES	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Veselin	Veselin	k2eAgInSc1d1	Veselin
Vačkov	Vačkov	k1gInSc1	Vačkov
<g/>
,	,	kIx,	,
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
2000	[number]	k4	2000
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
s	s	k7c7	s
LN	LN	kA	LN
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
jako	jako	k8xC	jako
zahraniční	zahraniční	k2eAgMnSc1d1	zahraniční
dopisovatel	dopisovatel	k1gMnSc1	dopisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
redakce	redakce	k1gFnSc2	redakce
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
působil	působit	k5eAaImAgInS	působit
jako	jako	k9	jako
editor	editor	k1gInSc1	editor
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgMnSc1d1	vedoucí
redaktor	redaktor	k1gMnSc1	redaktor
magazínu	magazín	k1gInSc2	magazín
a	a	k8xC	a
zástupce	zástupce	k1gMnSc2	zástupce
šéfredaktora	šéfredaktor	k1gMnSc2	šéfredaktor
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
šéfredaktora	šéfredaktor	k1gMnSc2	šéfredaktor
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Dalibor	Dalibor	k1gMnSc1	Dalibor
Balšínek	Balšínek	k1gMnSc1	Balšínek
<g/>
,	,	kIx,	,
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
2009	[number]	k4	2009
<g/>
-	-	kIx~	-
<g/>
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
časopisu	časopis	k1gInSc2	časopis
Týden	týden	k1gInSc1	týden
<g/>
,	,	kIx,	,
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2013	[number]	k4	2013
zakládá	zakládat	k5eAaImIp3nS	zakládat
projekt	projekt	k1gInSc1	projekt
Echo	echo	k1gNnSc1	echo
<g/>
24	[number]	k4	24
s	s	k7c7	s
podtitulem	podtitul	k1gInSc7	podtitul
"	"	kIx"	"
<g/>
deník	deník	k1gInSc1	deník
pro	pro	k7c4	pro
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
byznys	byznys	k1gInSc4	byznys
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
István	István	k2eAgMnSc1d1	István
Léko	Léko	k1gMnSc1	Léko
<g/>
,	,	kIx,	,
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
týdeníku	týdeník	k1gInSc2	týdeník
Euro	euro	k1gNnSc1	euro
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
-	-	kIx~	-
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
<g/>
,	,	kIx,	,
provozovatel	provozovatel	k1gMnSc1	provozovatel
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
a	a	k8xC	a
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
informačního	informační	k2eAgInSc2d1	informační
webu	web	k1gInSc2	web
Česká	český	k2eAgFnSc1d1	Česká
pozice	pozice	k1gFnSc1	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
první	první	k4xOgNnSc1	první
číslo	číslo	k1gNnSc1	číslo
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1893	[number]	k4	1893
<g/>
.	.	kIx.	.
</s>
<s>
Noviny	novina	k1gFnPc1	novina
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
spojením	spojení	k1gNnSc7	spojení
olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
časopisu	časopis	k1gInSc2	časopis
Pozor	pozor	k1gInSc1	pozor
a	a	k8xC	a
brněnských	brněnský	k2eAgInPc2d1	brněnský
Moravských	moravský	k2eAgInPc2d1	moravský
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc4	jejichž
zakladatel	zakladatel	k1gMnSc1	zakladatel
Adolf	Adolf	k1gMnSc1	Adolf
Stránský	Stránský	k1gMnSc1	Stránský
sloučení	sloučení	k1gNnSc4	sloučení
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
redakce	redakce	k1gFnSc1	redakce
LN	LN	kA	LN
s	s	k7c7	s
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
Emilem	Emil	k1gMnSc7	Emil
Čermákem	Čermák	k1gMnSc7	Čermák
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
nákladu	náklad	k1gInSc6	náklad
asi	asi	k9	asi
6000	[number]	k4	6000
výtisků	výtisk	k1gInPc2	výtisk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
se	se	k3xPyFc4	se
Adolf	Adolf	k1gMnSc1	Adolf
Stránský	Stránský	k1gMnSc1	Stránský
stal	stát	k5eAaPmAgMnS	stát
výhradním	výhradní	k2eAgMnSc7d1	výhradní
majitelem	majitel	k1gMnSc7	majitel
deníku	deník	k1gInSc2	deník
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
snížil	snížit	k5eAaPmAgMnS	snížit
náklady	náklad	k1gInPc4	náklad
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
Lidovou	lidový	k2eAgFnSc4d1	lidová
tiskárnu	tiskárna	k1gFnSc4	tiskárna
<g/>
,	,	kIx,	,
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
opatřil	opatřit	k5eAaPmAgInS	opatřit
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
novinám	novina	k1gFnPc3	novina
i	i	k8xC	i
tiskárně	tiskárna	k1gFnSc3	tiskárna
sídlo	sídlo	k1gNnSc1	sídlo
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
domě	dům	k1gInSc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
významné	významný	k2eAgFnSc6d1	významná
modernizaci	modernizace	k1gFnSc6	modernizace
novin	novina	k1gFnPc2	novina
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
Arnošt	Arnošt	k1gMnSc1	Arnošt
Heinrich	Heinrich	k1gMnSc1	Heinrich
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
Stránský	Stránský	k1gMnSc1	Stránský
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
prosadil	prosadit	k5eAaPmAgMnS	prosadit
ranní	ranní	k2eAgNnSc4d1	ranní
vydání	vydání	k1gNnSc4	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
LN	LN	kA	LN
vycházely	vycházet	k5eAaImAgInP	vycházet
až	až	k9	až
odpoledne	odpoledne	k1gNnSc1	odpoledne
a	a	k8xC	a
zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
tak	tak	k6eAd1	tak
nebylo	být	k5eNaImAgNnS	být
aktuální	aktuální	k2eAgNnSc1d1	aktuální
<g/>
.	.	kIx.	.
</s>
<s>
Ranní	ranní	k2eAgNnSc1d1	ranní
vydání	vydání	k1gNnSc1	vydání
mělo	mít	k5eAaImAgNnS	mít
místo	místo	k7c2	místo
úvodníku	úvodník	k1gInSc2	úvodník
fotografii	fotografia	k1gFnSc4	fotografia
<g/>
,	,	kIx,	,
nechyběla	chybět	k5eNaImAgFnS	chybět
"	"	kIx"	"
<g/>
senzace	senzace	k1gFnSc1	senzace
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgFnSc1d1	soudní
síň	síň	k1gFnSc1	síň
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
raníku	raník	k1gInSc2	raník
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
hlavní	hlavní	k2eAgInSc1d1	hlavní
list	list	k1gInSc1	list
<g/>
.	.	kIx.	.
</s>
<s>
Heinrichovy	Heinrichův	k2eAgInPc1d1	Heinrichův
představy	představ	k1gInPc1	představ
upoutaly	upoutat	k5eAaPmAgInP	upoutat
pozornost	pozornost	k1gFnSc4	pozornost
řady	řada	k1gFnSc2	řada
literátů	literát	k1gMnPc2	literát
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
začali	začít	k5eAaPmAgMnP	začít
do	do	k7c2	do
novin	novina	k1gFnPc2	novina
přispívat	přispívat	k5eAaImF	přispívat
<g/>
.	.	kIx.	.
</s>
<s>
Patřili	patřit	k5eAaImAgMnP	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
<g/>
:	:	kIx,	:
Viktor	Viktor	k1gMnSc1	Viktor
Dyk	Dyk	k?	Dyk
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Toman	Toman	k1gMnSc1	Toman
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Sova	Sova	k1gMnSc1	Sova
<g/>
,	,	kIx,	,
S.	S.	kA	S.
K.	K.	kA	K.
Neumann	Neumann	k1gMnSc1	Neumann
<g/>
,	,	kIx,	,
Fráňa	Fráňa	k1gMnSc1	Fráňa
Šrámek	Šrámek	k1gMnSc1	Šrámek
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Mahen	Mahen	k1gInSc1	Mahen
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Gellner	Gellner	k1gMnSc1	Gellner
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Těsnohlídek	Těsnohlídka	k1gFnPc2	Těsnohlídka
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
krokem	krok	k1gInSc7	krok
bylo	být	k5eAaImAgNnS	být
vyslání	vyslání	k1gNnSc1	vyslání
Karla	Karel	k1gMnSc2	Karel
Z.	Z.	kA	Z.
Klímy	Klíma	k1gMnSc2	Klíma
jako	jako	k8xS	jako
parlamentního	parlamentní	k2eAgMnSc4d1	parlamentní
zpravodaje	zpravodaj	k1gMnSc4	zpravodaj
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
<g/>
.	.	kIx.	.
</s>
<s>
Prestiž	prestiž	k1gFnSc1	prestiž
LN	LN	kA	LN
rostla	růst	k5eAaImAgFnS	růst
a	a	k8xC	a
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
i	i	k9	i
Stránského	Stránského	k2eAgInSc4d1	Stránského
politický	politický	k2eAgInSc4d1	politický
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Heinrichem	Heinrich	k1gMnSc7	Heinrich
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
státním	státní	k2eAgInSc6d1	státní
převratu	převrat	k1gInSc6	převrat
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
oba	dva	k4xCgInPc4	dva
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
poslanci	poslanec	k1gMnPc7	poslanec
revolučního	revoluční	k2eAgNnSc2d1	revoluční
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
a	a	k8xC	a
Stránský	Stránský	k1gMnSc1	Stránský
také	také	k6eAd1	také
ministrem	ministr	k1gMnSc7	ministr
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
obdobím	období	k1gNnSc7	období
meziválečného	meziválečný	k2eAgNnSc2d1	meziválečné
Československa	Československo	k1gNnSc2	Československo
je	být	k5eAaImIp3nS	být
spojována	spojovat	k5eAaImNgFnS	spojovat
i	i	k9	i
nejslavnější	slavný	k2eAgFnSc1d3	nejslavnější
éra	éra	k1gFnSc1	éra
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Změnilo	změnit	k5eAaPmAgNnS	změnit
se	se	k3xPyFc4	se
hlavní	hlavní	k2eAgNnSc4d1	hlavní
politické	politický	k2eAgNnSc4d1	politické
centrum	centrum	k1gNnSc4	centrum
<g/>
:	:	kIx,	:
už	už	k6eAd1	už
nikoli	nikoli	k9	nikoli
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
také	také	k6eAd1	také
LN	LN	kA	LN
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
otevřely	otevřít	k5eAaPmAgFnP	otevřít
svou	svůj	k3xOyFgFnSc4	svůj
filiálku	filiálka	k1gFnSc4	filiálka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
následovaly	následovat	k5eAaImAgFnP	následovat
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
,	,	kIx,	,
Liberci	Liberec	k1gInSc6	Liberec
<g/>
,	,	kIx,	,
Znojmě	Znojmo	k1gNnSc6	Znojmo
<g/>
,	,	kIx,	,
Českém	český	k2eAgInSc6d1	český
Těšíně	Těšín	k1gInSc6	Těšín
<g/>
,	,	kIx,	,
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
,	,	kIx,	,
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
Jihlavě	Jihlava	k1gFnSc6	Jihlava
<g/>
,	,	kIx,	,
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
Košicích	Košice	k1gInPc6	Košice
a	a	k8xC	a
Užhorodě	Užhorod	k1gInSc6	Užhorod
<g/>
.	.	kIx.	.
</s>
<s>
LN	LN	kA	LN
také	také	k9	také
investovaly	investovat	k5eAaBmAgInP	investovat
nemalé	malý	k2eNgInPc4d1	nemalý
prostředky	prostředek	k1gInPc4	prostředek
do	do	k7c2	do
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
zpravodajů	zpravodaj	k1gMnPc2	zpravodaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
koupil	koupit	k5eAaPmAgMnS	koupit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Stránský	Stránský	k1gMnSc1	Stránský
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
zakladatele	zakladatel	k1gMnSc2	zakladatel
Adolfa	Adolf	k1gMnSc2	Adolf
Stránského	Stránský	k1gMnSc2	Stránský
<g/>
)	)	kIx)	)
Topičovo	topičův	k2eAgNnSc1d1	Topičovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Národní	národní	k2eAgFnSc6d1	národní
třídě	třída	k1gFnSc6	třída
a	a	k8xC	a
usídlil	usídlit	k5eAaPmAgMnS	usídlit
zde	zde	k6eAd1	zde
svůj	svůj	k3xOyFgInSc4	svůj
tiskový	tiskový	k2eAgInSc4d1	tiskový
podnik	podnik	k1gInSc4	podnik
včetně	včetně	k7c2	včetně
redakce	redakce	k1gFnSc2	redakce
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
adresa	adresa	k1gFnSc1	adresa
byla	být	k5eAaImAgFnS	být
vizitkou	vizitka	k1gFnSc7	vizitka
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
:	:	kIx,	:
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
se	se	k3xPyFc4	se
zvedl	zvednout	k5eAaPmAgMnS	zvednout
náklad	náklad	k1gInSc4	náklad
ve	v	k7c4	v
všední	všední	k2eAgInSc4d1	všední
den	den	k1gInSc4	den
na	na	k7c4	na
44	[number]	k4	44
000	[number]	k4	000
výtisků	výtisk	k1gInPc2	výtisk
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
odpoledník	odpoledník	k1gInSc1	odpoledník
měl	mít	k5eAaImAgInS	mít
ještě	ještě	k9	ještě
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
dvacet	dvacet	k4xCc4	dvacet
tisíc	tisíc	k4xCgInPc2	tisíc
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
novými	nový	k2eAgFnPc7d1	nová
rubrikami	rubrika	k1gFnPc7	rubrika
a	a	k8xC	a
přílohami	příloha	k1gFnPc7	příloha
se	se	k3xPyFc4	se
rozšiřovala	rozšiřovat	k5eAaImAgFnS	rozšiřovat
redakce	redakce	k1gFnSc1	redakce
a	a	k8xC	a
přibývali	přibývat	k5eAaImAgMnP	přibývat
její	její	k3xOp3gMnPc1	její
spolupracovníci	spolupracovník	k1gMnPc1	spolupracovník
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
také	také	k9	také
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
spěl	spět	k5eAaImAgInS	spět
ke	k	k7c3	k
zkratce	zkratka	k1gFnSc3	zkratka
a	a	k8xC	a
jednoduchosti	jednoduchost	k1gFnSc3	jednoduchost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
i	i	k9	i
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
s	s	k7c7	s
obrazem	obraz	k1gInSc7	obraz
<g/>
,	,	kIx,	,
fotografií	fotografia	k1gFnSc7	fotografia
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
kresbou	kresba	k1gFnSc7	kresba
a	a	k8xC	a
karikaturou	karikatura	k1gFnSc7	karikatura
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
kreslíře	kreslíř	k1gMnPc4	kreslíř
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
do	do	k7c2	do
Lidovek	Lidovky	k1gFnPc2	Lidovky
uložili	uložit	k5eAaPmAgMnP	uložit
své	své	k1gNnSc4	své
vidění	vidění	k1gNnSc2	vidění
aktuality	aktualita	k1gFnSc2	aktualita
<g/>
,	,	kIx,	,
patřili	patřit	k5eAaImAgMnP	patřit
Hugo	Hugo	k1gMnSc1	Hugo
Boettinger	Boettinger	k1gMnSc1	Boettinger
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
Milén	Milén	k1gInSc1	Milén
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kratochvíl	Kratochvíl	k1gMnSc1	Kratochvíl
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Lolek	Lolek	k1gMnSc1	Lolek
<g/>
,	,	kIx,	,
Ondřej	Ondřej	k1gMnSc1	Ondřej
Sekora	Sekora	k1gFnSc1	Sekora
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
se	se	k3xPyFc4	se
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Peroutka	Peroutka	k1gMnSc1	Peroutka
stal	stát	k5eAaPmAgMnS	stát
hlavním	hlavní	k2eAgMnSc7d1	hlavní
úvodníkářem	úvodníkář	k1gMnSc7	úvodníkář
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
existence	existence	k1gFnSc2	existence
první	první	k4xOgFnSc2	první
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
sloužily	sloužit	k5eAaImAgFnP	sloužit
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
jako	jako	k8xC	jako
orgán	orgán	k1gInSc1	orgán
Hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
prezidentů	prezident	k1gMnPc2	prezident
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
i	i	k8xC	i
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
pochopitelné	pochopitelný	k2eAgNnSc1d1	pochopitelné
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnSc1	jejich
porážka	porážka	k1gFnSc1	porážka
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
chápána	chápat	k5eAaImNgFnS	chápat
také	také	k9	také
jako	jako	k8xS	jako
neúspěch	neúspěch	k1gInSc4	neúspěch
tohoto	tento	k3xDgInSc2	tento
listu	list	k1gInSc2	list
<g/>
.	.	kIx.	.
</s>
<s>
LN	LN	kA	LN
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
po	po	k7c6	po
Mnichově	Mnichov	k1gInSc6	Mnichov
snažily	snažit	k5eAaImAgFnP	snažit
dodávat	dodávat	k5eAaImF	dodávat
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
odvahy	odvaha	k1gFnSc2	odvaha
čtenářům	čtenář	k1gMnPc3	čtenář
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jako	jako	k8xS	jako
rozhodný	rozhodný	k2eAgMnSc1d1	rozhodný
zastánce	zastánce	k1gMnSc1	zastánce
Benešovy	Benešův	k2eAgFnSc2d1	Benešova
a	a	k8xC	a
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
politiky	politika	k1gFnSc2	politika
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
LN	LN	kA	LN
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
,	,	kIx,	,
na	na	k7c4	na
něhož	jenž	k3xRgMnSc4	jenž
druhorepublikoví	druhorepublikový	k2eAgMnPc1d1	druhorepublikový
politici	politik	k1gMnPc1	politik
a	a	k8xC	a
publicisté	publicista	k1gMnPc1	publicista
ostře	ostro	k6eAd1	ostro
útočili	útočit	k5eAaImAgMnP	útočit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
patrně	patrně	k6eAd1	patrně
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
Čapkově	čapkově	k6eAd1	čapkově
předčasnému	předčasný	k2eAgNnSc3d1	předčasné
úmrtí	úmrtí	k1gNnSc3	úmrtí
o	o	k7c6	o
Vánocích	Vánoce	k1gFnPc6	Vánoce
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
okupaci	okupace	k1gFnSc6	okupace
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1939	[number]	k4	1939
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
podstatně	podstatně	k6eAd1	podstatně
změnila	změnit	k5eAaPmAgFnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tiskových	tiskový	k2eAgInPc2d1	tiskový
podniků	podnik	k1gInPc2	podnik
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Stránského	Stránský	k1gMnSc2	Stránský
byla	být	k5eAaImAgFnS	být
dosazena	dosadit	k5eAaPmNgFnS	dosadit
říšská	říšský	k2eAgFnSc1d1	říšská
správa	správa	k1gFnSc1	správa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
byla	být	k5eAaImAgFnS	být
zavedena	zaveden	k2eAgFnSc1d1	zavedena
nejtužší	tuhý	k2eAgFnSc1d3	nejtužší
cenzura	cenzura	k1gFnSc1	cenzura
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
snaha	snaha	k1gFnSc1	snaha
o	o	k7c6	o
sdělování	sdělování	k1gNnSc6	sdělování
pravdy	pravda	k1gFnSc2	pravda
byla	být	k5eAaImAgFnS	být
krajně	krajně	k6eAd1	krajně
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
<g/>
.	.	kIx.	.
</s>
<s>
Šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
Karel	Karel	k1gMnSc1	Karel
Z.	Z.	kA	Z.
Klíma	Klíma	k1gMnSc1	Klíma
musel	muset	k5eAaImAgMnS	muset
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1941	[number]	k4	1941
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
nacistů	nacista	k1gMnPc2	nacista
odejít	odejít	k5eAaPmF	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Leopold	Leopold	k1gMnSc1	Leopold
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
bez	bez	k7c2	bez
nejmenších	malý	k2eAgFnPc2d3	nejmenší
zábran	zábrana	k1gFnPc2	zábrana
s	s	k7c7	s
Němci	Němec	k1gMnPc7	Němec
kolaboroval	kolaborovat	k5eAaImAgMnS	kolaborovat
<g/>
.	.	kIx.	.
</s>
<s>
Klímu	Klíma	k1gMnSc4	Klíma
počátkem	počátkem	k7c2	počátkem
srpna	srpen	k1gInSc2	srpen
1942	[number]	k4	1942
zatklo	zatknout	k5eAaPmAgNnS	zatknout
gestapo	gestapo	k1gNnSc1	gestapo
a	a	k8xC	a
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1942	[number]	k4	1942
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Terezíně	Terezín	k1gInSc6	Terezín
popraven	popravit	k5eAaPmNgMnS	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Zatčen	zatknout	k5eAaPmNgMnS	zatknout
byl	být	k5eAaImAgMnS	být
i	i	k9	i
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
nějž	jenž	k3xRgMnSc4	jenž
si	se	k3xPyFc3	se
gestapo	gestapo	k1gNnSc1	gestapo
přišlo	přijít	k5eAaPmAgNnS	přijít
v	v	k7c6	v
září	září	k1gNnSc6	září
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
války	válka	k1gFnSc2	válka
během	během	k7c2	během
pochodu	pochod	k1gInSc2	pochod
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1939	[number]	k4	1939
Němci	Němec	k1gMnPc1	Němec
zatkli	zatknout	k5eAaPmAgMnP	zatknout
rovněž	rovněž	k9	rovněž
Ferdinanda	Ferdinand	k1gMnSc4	Ferdinand
Peroutku	Peroutka	k1gMnSc4	Peroutka
<g/>
;	;	kIx,	;
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
vězněn	věznit	k5eAaImNgInS	věznit
v	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
Dachau	Dachaus	k1gInSc2	Dachaus
a	a	k8xC	a
Buchenwald	Buchenwald	k1gInSc4	Buchenwald
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
zatkli	zatknout	k5eAaPmAgMnP	zatknout
i	i	k9	i
Karla	Karel	k1gMnSc4	Karel
Poláčka	Poláček	k1gMnSc4	Poláček
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
deportovali	deportovat	k5eAaBmAgMnP	deportovat
do	do	k7c2	do
Terezína	Terezín	k1gInSc2	Terezín
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
do	do	k7c2	do
Osvětimi	Osvětim	k1gFnSc2	Osvětim
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1944	[number]	k4	1944
zahynul	zahynout	k5eAaPmAgMnS	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
Tvůrci	tvůrce	k1gMnPc1	tvůrce
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
-	-	kIx~	-
jak	jak	k8xS	jak
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
odešli	odejít	k5eAaPmAgMnP	odejít
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zapojili	zapojit	k5eAaPmAgMnP	zapojit
do	do	k7c2	do
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
odboje	odboj	k1gInSc2	odboj
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zůstali	zůstat	k5eAaPmAgMnP	zůstat
v	v	k7c6	v
okupované	okupovaný	k2eAgFnSc6d1	okupovaná
vlasti	vlast	k1gFnSc6	vlast
-	-	kIx~	-
se	se	k3xPyFc4	se
ani	ani	k9	ani
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nemohli	moct	k5eNaImAgMnP	moct
psát	psát	k5eAaImF	psát
svobodně	svobodně	k6eAd1	svobodně
<g/>
,	,	kIx,	,
nezpronevěřili	zpronevěřit	k5eNaPmAgMnP	zpronevěřit
svému	svůj	k3xOyFgNnSc3	svůj
krédu	krédo	k1gNnSc3	krédo
a	a	k8xC	a
přesvědčení	přesvědčení	k1gNnSc3	přesvědčení
a	a	k8xC	a
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
většině	většina	k1gFnSc6	většina
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
sil	síla	k1gFnPc2	síla
bojovali	bojovat	k5eAaImAgMnP	bojovat
za	za	k7c4	za
obnovení	obnovení	k1gNnSc4	obnovení
svobody	svoboda	k1gFnSc2	svoboda
svého	svůj	k3xOyFgInSc2	svůj
národa	národ	k1gInSc2	národ
a	a	k8xC	a
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Porážka	porážka	k1gFnSc1	porážka
Němců	Němec	k1gMnPc2	Němec
se	se	k3xPyFc4	se
odrážela	odrážet	k5eAaImAgFnS	odrážet
i	i	k9	i
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
řídil	řídit	k5eAaImAgMnS	řídit
kolaborant	kolaborant	k1gMnSc1	kolaborant
Leopold	Leopold	k1gMnSc1	Leopold
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
února	únor	k1gInSc2	únor
1945	[number]	k4	1945
vycházely	vycházet	k5eAaImAgFnP	vycházet
noviny	novina	k1gFnPc1	novina
jen	jen	k9	jen
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
listu	list	k1gInSc6	list
a	a	k8xC	a
plné	plný	k2eAgFnSc3d1	plná
zpráv	zpráva	k1gFnPc2	zpráva
o	o	k7c6	o
potížích	potíž	k1gFnPc6	potíž
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yRgInPc2	který
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
nacisté	nacista	k1gMnPc1	nacista
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
vyšly	vyjít	k5eAaPmAgFnP	vyjít
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
naposledy	naposledy	k6eAd1	naposledy
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
už	už	k9	už
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
Československa	Československo	k1gNnSc2	Československo
svobodná	svobodný	k2eAgFnSc1d1	svobodná
a	a	k8xC	a
blížil	blížit	k5eAaImAgInS	blížit
se	se	k3xPyFc4	se
konec	konec	k1gInSc1	konec
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Pražského	pražský	k2eAgNnSc2d1	Pražské
povstání	povstání	k1gNnSc2	povstání
se	se	k3xPyFc4	se
zapojili	zapojit	k5eAaPmAgMnP	zapojit
i	i	k9	i
novináři	novinář	k1gMnPc1	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Vznikaly	vznikat	k5eAaImAgInP	vznikat
revoluční	revoluční	k2eAgInPc1d1	revoluční
výbory	výbor	k1gInPc1	výbor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zajišťovaly	zajišťovat	k5eAaImAgFnP	zajišťovat
výrobu	výroba	k1gFnSc4	výroba
necenzurovaných	cenzurovaný	k2eNgFnPc2d1	necenzurovaná
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
založil	založit	k5eAaPmAgMnS	založit
poslední	poslední	k2eAgMnSc1d1	poslední
žijící	žijící	k2eAgMnSc1d1	žijící
předválečný	předválečný	k2eAgMnSc1d1	předválečný
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
Eduard	Eduard	k1gMnSc1	Eduard
Bass	Bass	k1gMnSc1	Bass
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
kolegy	kolega	k1gMnPc7	kolega
Národní	národní	k2eAgInSc4d1	národní
výbor	výbor	k1gInSc4	výbor
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
se	se	k3xPyFc4	se
pustili	pustit	k5eAaPmAgMnP	pustit
do	do	k7c2	do
přípravy	příprava	k1gFnSc2	příprava
jejich	jejich	k3xOp3gNnSc2	jejich
vydávání	vydávání	k1gNnSc2	vydávání
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nového	nový	k2eAgNnSc2d1	nové
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
vlády	vláda	k1gFnSc2	vláda
ale	ale	k8xC	ale
nesměly	smět	k5eNaImAgFnP	smět
vycházet	vycházet	k5eAaImF	vycházet
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
sloužily	sloužit	k5eAaImAgInP	sloužit
okupantům	okupant	k1gMnPc3	okupant
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
stanovisko	stanovisko	k1gNnSc4	stanovisko
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1945	[number]	k4	1945
také	také	k9	také
prezident	prezident	k1gMnSc1	prezident
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
doplatily	doplatit	k5eAaPmAgInP	doplatit
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
Vycházely	vycházet	k5eAaImAgFnP	vycházet
za	za	k7c4	za
okupace	okupace	k1gFnPc4	okupace
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
byly	být	k5eAaImAgFnP	být
majetkem	majetek	k1gInSc7	majetek
ministra	ministr	k1gMnSc2	ministr
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Stránského	Stránský	k1gMnSc2	Stránský
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
využili	využít	k5eAaPmAgMnP	využít
komunisté	komunista	k1gMnPc1	komunista
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
vydávání	vydávání	k1gNnSc4	vydávání
okamžitě	okamžitě	k6eAd1	okamžitě
zastavili	zastavit	k5eAaPmAgMnP	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Eduardu	Eduard	k1gMnSc3	Eduard
Bassovi	Bass	k1gMnSc3	Bass
se	se	k3xPyFc4	se
však	však	k9	však
podařilo	podařit	k5eAaPmAgNnS	podařit
založit	založit	k5eAaPmF	založit
Sdružení	sdružení	k1gNnSc1	sdružení
kulturních	kulturní	k2eAgFnPc2d1	kulturní
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
vydávání	vydávání	k1gNnSc1	vydávání
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
ujalo	ujmout	k5eAaPmAgNnS	ujmout
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
protože	protože	k8xS	protože
nemohly	moct	k5eNaImAgFnP	moct
vycházet	vycházet	k5eAaImF	vycházet
pod	pod	k7c7	pod
svým	svůj	k3xOyFgInSc7	svůj
názvem	název	k1gInSc7	název
<g/>
,	,	kIx,	,
zrodily	zrodit	k5eAaPmAgFnP	zrodit
se	se	k3xPyFc4	se
Svobodné	svobodný	k2eAgFnPc1d1	svobodná
noviny	novina	k1gFnPc1	novina
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
číslo	číslo	k1gNnSc1	číslo
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
strany	strana	k1gFnPc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
dělalo	dělat	k5eAaImAgNnS	dělat
vedení	vedení	k1gNnSc1	vedení
redakce	redakce	k1gFnSc2	redakce
vše	všechen	k3xTgNnSc4	všechen
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
každému	každý	k3xTgMnSc3	každý
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
Lidové	lidový	k2eAgFnPc4d1	lidová
noviny	novina	k1gFnPc4	novina
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
v	v	k7c6	v
LN	LN	kA	LN
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
z	z	k7c2	z
vedení	vedení	k1gNnSc2	vedení
odešel	odejít	k5eAaPmAgMnS	odejít
Eduard	Eduard	k1gMnSc1	Eduard
Bass	Bass	k1gMnSc1	Bass
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Peroutka	Peroutka	k1gMnSc1	Peroutka
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
svým	svůj	k3xOyFgInPc3	svůj
názorům	názor	k1gInPc3	názor
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
komunisty	komunista	k1gMnPc7	komunista
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
tlak	tlak	k1gInSc1	tlak
postupně	postupně	k6eAd1	postupně
rostl	růst	k5eAaImAgInS	růst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
vydal	vydat	k5eAaPmAgMnS	vydat
svou	svůj	k3xOyFgFnSc4	svůj
polemiku	polemika	k1gFnSc4	polemika
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
vlastně	vlastně	k9	vlastně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
osvobodil	osvobodit	k5eAaPmAgMnS	osvobodit
Prahu	Praha	k1gFnSc4	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejprodávanější	prodávaný	k2eAgInPc4d3	nejprodávanější
tituly	titul	k1gInPc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Peroutka	Peroutka	k1gMnSc1	Peroutka
byl	být	k5eAaImAgMnS	být
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1948	[number]	k4	1948
sesazen	sesazen	k2eAgMnSc1d1	sesazen
a	a	k8xC	a
rudými	rudý	k2eAgMnPc7d1	rudý
revolucionáři	revolucionář	k1gMnPc7	revolucionář
i	i	k8xC	i
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
ze	z	k7c2	z
Syndikátu	syndikát	k1gInSc2	syndikát
českých	český	k2eAgMnPc2d1	český
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Jan	Jan	k1gMnSc1	Jan
Drda	Drda	k1gMnSc1	Drda
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
LN	LN	kA	LN
tím	ten	k3xDgNnSc7	ten
začalo	začít	k5eAaPmAgNnS	začít
období	období	k1gNnSc1	období
nesvobody	nesvoboda	k1gFnSc2	nesvoboda
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
spojeni	spojen	k2eAgMnPc1d1	spojen
s	s	k7c7	s
Peroutkou	peroutka	k1gFnSc7	peroutka
a	a	k8xC	a
neemigrovali	emigrovat	k5eNaBmAgMnP	emigrovat
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
končili	končit	k5eAaImAgMnP	končit
v	v	k7c6	v
komunistických	komunistický	k2eAgInPc6d1	komunistický
lágrech	lágr	k1gInPc6	lágr
<g/>
.	.	kIx.	.
</s>
<s>
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
musely	muset	k5eAaImAgFnP	muset
otiskovat	otiskovat	k5eAaImF	otiskovat
ódy	óda	k1gFnPc1	óda
na	na	k7c4	na
Stalina	Stalin	k1gMnSc4	Stalin
a	a	k8xC	a
Klementa	Klement	k1gMnSc4	Klement
Gottwalda	Gottwald	k1gMnSc4	Gottwald
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Drda	Drda	k1gMnSc1	Drda
navíc	navíc	k6eAd1	navíc
nadělal	nadělat	k5eAaPmAgMnS	nadělat
svým	svůj	k3xOyFgNnPc3	svůj
hospodařením	hospodaření	k1gNnPc3	hospodaření
jen	jen	k9	jen
desetimilionové	desetimilionový	k2eAgInPc1d1	desetimilionový
dluhy	dluh	k1gInPc1	dluh
<g/>
,	,	kIx,	,
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
těm	ten	k3xDgMnPc3	ten
také	také	k9	také
noviny	novina	k1gFnPc1	novina
vyšly	vyjít	k5eAaPmAgFnP	vyjít
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1952	[number]	k4	1952
noviny	novina	k1gFnSc2	novina
naposled	naposled	k6eAd1	naposled
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zasedání	zasedání	k1gNnSc2	zasedání
výboru	výbor	k1gInSc2	výbor
Svazu	svaz	k1gInSc2	svaz
československých	československý	k2eAgMnPc2d1	československý
spisovatelů	spisovatel	k1gMnPc2	spisovatel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
usnesení	usnesení	k1gNnSc1	usnesení
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
budou	být	k5eAaImBp3nP	být
opět	opět	k6eAd1	opět
vydávány	vydáván	k2eAgFnPc1d1	vydávána
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc7	jejich
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
byl	být	k5eAaImAgMnS	být
jmenován	jmenován	k2eAgMnSc1d1	jmenován
Antonín	Antonín	k1gMnSc1	Antonín
J.	J.	kA	J.
Liehm	Liehm	k1gMnSc1	Liehm
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
číslo	číslo	k1gNnSc1	číslo
mělo	mít	k5eAaImAgNnS	mít
vyjít	vyjít	k5eAaPmF	vyjít
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Noviny	novina	k1gFnPc4	novina
podporoval	podporovat	k5eAaImAgMnS	podporovat
z	z	k7c2	z
emigrace	emigrace	k1gFnSc2	emigrace
také	také	k9	také
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Peroutka	Peroutka	k1gMnSc1	Peroutka
<g/>
.	.	kIx.	.
</s>
<s>
Dřív	dříve	k6eAd2	dříve
než	než	k8xS	než
ale	ale	k8xC	ale
začaly	začít	k5eAaPmAgFnP	začít
přípravy	příprava	k1gFnPc1	příprava
<g/>
,	,	kIx,	,
přišel	přijít	k5eAaPmAgMnS	přijít
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc4	srpen
1968	[number]	k4	1968
a	a	k8xC	a
veškeré	veškerý	k3xTgFnPc4	veškerý
naděje	naděje	k1gFnPc4	naděje
na	na	k7c4	na
svobodné	svobodný	k2eAgFnPc4d1	svobodná
noviny	novina	k1gFnPc4	novina
se	se	k3xPyFc4	se
rozplynuly	rozplynout	k5eAaPmAgFnP	rozplynout
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
před	před	k7c7	před
sametovou	sametový	k2eAgFnSc7d1	sametová
revolucí	revoluce	k1gFnSc7	revoluce
usoudili	usoudit	k5eAaPmAgMnP	usoudit
dva	dva	k4xCgInPc4	dva
z	z	k7c2	z
významných	významný	k2eAgMnPc2d1	významný
signatářů	signatář	k1gMnPc2	signatář
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
a	a	k8xC	a
Ladislav	Ladislav	k1gMnSc1	Ladislav
Lis	Lisa	k1gFnPc2	Lisa
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
začít	začít	k5eAaPmF	začít
vydávat	vydávat	k5eAaImF	vydávat
časopis	časopis	k1gInSc4	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
prolomil	prolomit	k5eAaPmAgInS	prolomit
okruh	okruh	k1gInSc4	okruh
chartistů	chartista	k1gMnPc2	chartista
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
mezi	mezi	k7c4	mezi
lid	lid	k1gInSc4	lid
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Ruml	Ruml	k1gMnSc1	Ruml
proto	proto	k8xC	proto
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgFnP	moct
obnovit	obnovit	k5eAaPmF	obnovit
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
pokus	pokus	k1gInSc1	pokus
ze	z	k7c2	z
září	září	k1gNnSc2	září
1987	[number]	k4	1987
dopadl	dopadnout	k5eAaPmAgMnS	dopadnout
katastroficky	katastroficky	k6eAd1	katastroficky
<g/>
,	,	kIx,	,
úpravou	úprava	k1gFnSc7	úprava
i	i	k9	i
po	po	k7c6	po
obsahové	obsahový	k2eAgFnSc6d1	obsahová
stránce	stránka	k1gFnSc6	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc4	druhý
číslo	číslo	k1gNnSc4	číslo
už	už	k6eAd1	už
dopadlo	dopadnout	k5eAaPmAgNnS	dopadnout
lépe	dobře	k6eAd2	dobře
a	a	k8xC	a
další	další	k2eAgNnSc4d1	další
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc4	první
řádné	řádný	k2eAgNnSc4d1	řádné
lednové	lednový	k2eAgNnSc4d1	lednové
číslo	číslo	k1gNnSc4	číslo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
normu	norma	k1gFnSc4	norma
pro	pro	k7c4	pro
ta	ten	k3xDgNnPc4	ten
další	další	k1gNnPc4	další
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
titulní	titulní	k2eAgFnSc6d1	titulní
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
úvodník	úvodník	k1gInSc1	úvodník
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
s	s	k7c7	s
příznačným	příznačný	k2eAgInSc7d1	příznačný
názvem	název	k1gInSc7	název
Noviny	novina	k1gFnSc2	novina
jako	jako	k8xC	jako
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Jiří	Jiří	k1gMnSc1	Jiří
Ruml	Ruml	k1gMnSc1	Ruml
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
redaktor	redaktor	k1gMnSc1	redaktor
působil	působit	k5eAaImAgMnS	působit
Rudolf	Rudolf	k1gMnSc1	Rudolf
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
do	do	k7c2	do
novin	novina	k1gFnPc2	novina
přispívali	přispívat	k5eAaImAgMnP	přispívat
jak	jak	k8xC	jak
chartisté	chartista	k1gMnPc1	chartista
(	(	kIx(	(
<g/>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Dienstbier	Dienstbier	k1gMnSc1	Dienstbier
<g/>
,	,	kIx,	,
Luboš	Luboš	k1gMnSc1	Luboš
Dobrovský	Dobrovský	k1gMnSc1	Dobrovský
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Hejdánek	Hejdánek	k1gMnSc1	Hejdánek
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Urbánek	Urbánek	k1gMnSc1	Urbánek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
šedé	šedý	k2eAgFnSc2d1	šedá
zóny	zóna	k1gFnSc2	zóna
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
většinou	většinou	k6eAd1	většinou
pod	pod	k7c7	pod
pseudonymy	pseudonym	k1gInPc7	pseudonym
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Černý	Černý	k1gMnSc1	Černý
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Lukeš	Lukeš	k1gMnSc1	Lukeš
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Hanzel	Hanzel	k1gMnSc1	Hanzel
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Rejžek	Rejžek	k1gInSc1	Rejžek
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Rychetský	Rychetský	k1gMnSc1	Rychetský
<g/>
,	,	kIx,	,
Rita	Rita	k1gFnSc1	Rita
Klímová	Klímová	k1gFnSc1	Klímová
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Techniku	technika	k1gFnSc4	technika
měli	mít	k5eAaImAgMnP	mít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
Vladimír	Vladimír	k1gMnSc1	Vladimír
Mlynář	Mlynář	k1gMnSc1	Mlynář
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Dobrovský	Dobrovský	k1gMnSc1	Dobrovský
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
samizdatové	samizdatový	k2eAgFnSc2d1	samizdatová
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
opakovaně	opakovaně	k6eAd1	opakovaně
žádaly	žádat	k5eAaImAgInP	žádat
o	o	k7c4	o
oficiální	oficiální	k2eAgFnSc4d1	oficiální
registraci	registrace	k1gFnSc4	registrace
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
až	až	k9	až
do	do	k7c2	do
pádu	pád	k1gInSc2	pád
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
nebyla	být	k5eNaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Ruml	Ruml	k1gMnSc1	Ruml
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
vydání	vydání	k1gNnSc6	vydání
novin	novina	k1gFnPc2	novina
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
však	však	k9	však
nepřerušilo	přerušit	k5eNaPmAgNnS	přerušit
jejich	jejich	k3xOp3gNnSc1	jejich
vydávání	vydávání	k1gNnSc1	vydávání
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
prvních	první	k4xOgNnPc2	první
čísel	číslo	k1gNnPc2	číslo
vzbudily	vzbudit	k5eAaPmAgFnP	vzbudit
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
zájem	zájem	k1gInSc4	zájem
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Zmiňovaly	zmiňovat	k5eAaImAgInP	zmiňovat
se	se	k3xPyFc4	se
o	o	k7c6	o
nich	on	k3xPp3gFnPc6	on
New	New	k1gFnPc6	New
York	York	k1gInSc4	York
Times	Times	k1gMnSc1	Times
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
Le	Le	k1gMnSc1	Le
Monde	Mond	k1gMnSc5	Mond
jim	on	k3xPp3gMnPc3	on
dokonce	dokonce	k9	dokonce
věnoval	věnovat	k5eAaImAgInS	věnovat
celou	celý	k2eAgFnSc4d1	celá
stranu	strana	k1gFnSc4	strana
s	s	k7c7	s
výňatky	výňatek	k1gInPc7	výňatek
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelně	pravidelně	k6eAd1	pravidelně
o	o	k7c6	o
nich	on	k3xPp3gFnPc6	on
referoval	referovat	k5eAaBmAgInS	referovat
Hlas	hlas	k1gInSc1	hlas
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
a	a	k8xC	a
česká	český	k2eAgFnSc1d1	Česká
redakce	redakce	k1gFnSc1	redakce
BBC	BBC	kA	BBC
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
problémem	problém	k1gInSc7	problém
Lidovek	Lidovky	k1gFnPc2	Lidovky
ale	ale	k8xC	ale
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
distribuce	distribuce	k1gFnSc1	distribuce
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
po	po	k7c6	po
uzávěrce	uzávěrka	k1gFnSc6	uzávěrka
čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
mezi	mezi	k7c7	mezi
dvanáctým	dvanáctý	k4xOgInSc7	dvanáctý
a	a	k8xC	a
dvacátým	dvacátý	k4xOgInSc7	dvacátý
dnem	den	k1gInSc7	den
každého	každý	k3xTgInSc2	každý
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
StB	StB	k1gFnSc1	StB
čilou	čilý	k2eAgFnSc4d1	čilá
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
schválení	schválení	k1gNnSc6	schválení
a	a	k8xC	a
zkorigování	zkorigování	k1gNnSc6	zkorigování
rukopisů	rukopis	k1gInPc2	rukopis
vstupovali	vstupovat	k5eAaImAgMnP	vstupovat
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
Vladimír	Vladimír	k1gMnSc1	Vladimír
Mlynář	Mlynář	k1gMnSc1	Mlynář
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Dobrovský	Dobrovský	k1gMnSc1	Dobrovský
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
oni	onen	k3xDgMnPc1	onen
dva	dva	k4xCgMnPc1	dva
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Ruml	Ruml	k1gMnSc1	Ruml
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
číslo	číslo	k1gNnSc1	číslo
"	"	kIx"	"
<g/>
zlomí	zlomit	k5eAaPmIp3nS	zlomit
<g/>
"	"	kIx"	"
a	a	k8xC	a
vytiskne	vytisknout	k5eAaPmIp3nS	vytisknout
<g/>
.	.	kIx.	.
</s>
<s>
Redakce	redakce	k1gFnSc1	redakce
samizdatových	samizdatový	k2eAgInPc2d1	samizdatový
LN	LN	kA	LN
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
tak	tak	k9	tak
350	[number]	k4	350
kusů	kus	k1gInPc2	kus
měsíčně	měsíčně	k6eAd1	měsíčně
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
toto	tento	k3xDgNnSc4	tento
množství	množství	k1gNnSc4	množství
zvýšili	zvýšit	k5eAaPmAgMnP	zvýšit
asi	asi	k9	asi
na	na	k7c4	na
10	[number]	k4	10
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
ale	ale	k8xC	ale
v	v	k7c6	v
září	září	k1gNnSc6	září
1989	[number]	k4	1989
Jiřího	Jiří	k1gMnSc2	Jiří
Rumla	Ruml	k1gMnSc2	Ruml
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jeho	jeho	k3xOp3gMnSc7	jeho
zástupcem	zástupce	k1gMnSc7	zástupce
Rudolfem	Rudolf	k1gMnSc7	Rudolf
Zemanem	Zeman	k1gMnSc7	Zeman
zatkla	zatknout	k5eAaPmAgFnS	zatknout
StB	StB	k1gFnSc4	StB
a	a	k8xC	a
zavřela	zavřít	k5eAaPmAgFnS	zavřít
do	do	k7c2	do
Ruzyňské	ruzyňský	k2eAgFnSc2d1	Ruzyňská
věznice	věznice	k1gFnSc2	věznice
a	a	k8xC	a
zabavila	zabavit	k5eAaPmAgFnS	zabavit
říjnové	říjnový	k2eAgNnSc4d1	říjnové
číslo	číslo	k1gNnSc4	číslo
připravené	připravený	k2eAgFnSc2d1	připravená
k	k	k7c3	k
rozmnožení	rozmnožení	k1gNnSc3	rozmnožení
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
vydání	vydání	k1gNnSc1	vydání
podařilo	podařit	k5eAaPmAgNnS	podařit
Janu	Jan	k1gMnSc3	Jan
Rumlovi	Ruml	k1gMnSc3	Ruml
zrekonstruovat	zrekonstruovat	k5eAaPmF	zrekonstruovat
a	a	k8xC	a
vydat	vydat	k5eAaPmF	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Lidovky	Lidovky	k1gFnPc1	Lidovky
ale	ale	k9	ale
už	už	k6eAd1	už
byly	být	k5eAaImAgFnP	být
zaběhané	zaběhaný	k2eAgFnPc1d1	zaběhaná
a	a	k8xC	a
fungovaly	fungovat	k5eAaImAgFnP	fungovat
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
Vyšlo	vyjít	k5eAaPmAgNnS	vyjít
říjnové	říjnový	k2eAgNnSc4d1	říjnové
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
následovalo	následovat	k5eAaImAgNnS	následovat
listopadové	listopadový	k2eAgNnSc1d1	listopadové
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
dvě	dva	k4xCgNnPc4	dva
zvláštní	zvláštní	k2eAgNnPc4d1	zvláštní
vydání	vydání	k1gNnPc4	vydání
o	o	k7c6	o
polistopadových	polistopadový	k2eAgFnPc6d1	polistopadová
událostech	událost	k1gFnPc6	událost
a	a	k8xC	a
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
vyšly	vyjít	k5eAaPmAgFnP	vyjít
Lidovky	Lidovky	k1gFnPc1	Lidovky
tiskem	tisk	k1gInSc7	tisk
už	už	k9	už
v	v	k7c6	v
půlmilionovém	půlmilionový	k2eAgInSc6d1	půlmilionový
nákladu	náklad	k1gInSc6	náklad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
LN	LN	kA	LN
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
dvakrát	dvakrát	k6eAd1	dvakrát
týdně	týdně	k6eAd1	týdně
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1990	[number]	k4	1990
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
první	první	k4xOgNnSc1	první
deníkové	deníkový	k2eAgNnSc1d1	deníkové
číslo	číslo	k1gNnSc1	číslo
obnovených	obnovený	k2eAgFnPc2d1	obnovená
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
legální	legální	k2eAgNnSc1d1	legální
číslo	číslo	k1gNnSc1	číslo
obnoveného	obnovený	k2eAgInSc2d1	obnovený
deníku	deník	k1gInSc2	deník
Lidové	lidový	k2eAgFnPc4d1	lidová
noviny	novina	k1gFnPc4	novina
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
redakce	redakce	k1gFnSc2	redakce
stál	stát	k5eAaImAgInS	stát
až	až	k6eAd1	až
do	do	k7c2	do
června	červen	k1gInSc2	červen
1990	[number]	k4	1990
Jiří	Jiří	k1gMnSc1	Jiří
Ruml	Ruml	k1gMnSc1	Ruml
<g/>
.	.	kIx.	.
</s>
<s>
LN	LN	kA	LN
získaly	získat	k5eAaPmAgFnP	získat
oficiální	oficiální	k2eAgFnSc4d1	oficiální
registraci	registrace	k1gFnSc4	registrace
a	a	k8xC	a
mohly	moct	k5eAaImAgFnP	moct
se	se	k3xPyFc4	se
prodávat	prodávat	k5eAaImF	prodávat
běžně	běžně	k6eAd1	běžně
na	na	k7c6	na
stáncích	stánek	k1gInPc6	stánek
<g/>
,	,	kIx,	,
řešily	řešit	k5eAaImAgFnP	řešit
ale	ale	k9	ale
problém	problém	k1gInSc1	problém
s	s	k7c7	s
vybavením	vybavení	k1gNnSc7	vybavení
redakce	redakce	k1gFnSc2	redakce
a	a	k8xC	a
distribucí	distribuce	k1gFnSc7	distribuce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
fungujících	fungující	k2eAgInPc2d1	fungující
deníků	deník	k1gInPc2	deník
totiž	totiž	k8xC	totiž
postrádaly	postrádat	k5eAaImAgFnP	postrádat
technické	technický	k2eAgNnSc4d1	technické
zázemí	zázemí	k1gNnSc4	zázemí
a	a	k8xC	a
praxi	praxe	k1gFnSc4	praxe
zkušených	zkušený	k2eAgMnPc2d1	zkušený
deníkářů	deníkář	k1gMnPc2	deníkář
<g/>
.	.	kIx.	.
</s>
<s>
Redaktoři	redaktor	k1gMnPc1	redaktor
neměli	mít	k5eNaImAgMnP	mít
pořádné	pořádný	k2eAgInPc4d1	pořádný
telefony	telefon	k1gInPc4	telefon
<g/>
,	,	kIx,	,
archiv	archiv	k1gInSc4	archiv
ani	ani	k8xC	ani
slušnou	slušný	k2eAgFnSc4d1	slušná
tiskárnu	tiskárna	k1gFnSc4	tiskárna
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
valná	valný	k2eAgFnSc1d1	valná
hromada	hromada	k1gFnSc1	hromada
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
,	,	kIx,	,
o	o	k7c6	o
odprodeji	odprodej	k1gInSc6	odprodej
51	[number]	k4	51
procent	procento	k1gNnPc2	procento
svých	svůj	k3xOyFgFnPc2	svůj
akcií	akcie	k1gFnPc2	akcie
nizozemské	nizozemský	k2eAgFnSc2d1	nizozemská
společnosti	společnost	k1gFnSc2	společnost
Ringier	Ringier	k1gMnSc1	Ringier
Taurus	Taurus	k1gMnSc1	Taurus
<g/>
,	,	kIx,	,
dceřiné	dceřiný	k2eAgFnSc6d1	dceřiná
společnosti	společnost	k1gFnSc6	společnost
švýcarského	švýcarský	k2eAgInSc2d1	švýcarský
vydavatelského	vydavatelský	k2eAgInSc2d1	vydavatelský
koncernu	koncern	k1gInSc2	koncern
Ringier	Ringira	k1gFnPc2	Ringira
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1994	[number]	k4	1994
převzaly	převzít	k5eAaPmAgFnP	převzít
Lidové	lidový	k2eAgFnPc4d1	lidová
noviny	novina	k1gFnPc4	novina
křesťanskodemokratický	křesťanskodemokratický	k2eAgInSc1d1	křesťanskodemokratický
deník	deník	k1gInSc1	deník
Lidová	lidový	k2eAgFnSc1d1	lidová
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
výraznému	výrazný	k2eAgNnSc3d1	výrazné
zvýšení	zvýšení	k1gNnSc3	zvýšení
počtu	počet	k1gInSc2	počet
předplatitelů	předplatitel	k1gMnPc2	předplatitel
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
té	ten	k3xDgFnSc6	ten
příležitosti	příležitost	k1gFnSc6	příležitost
se	se	k3xPyFc4	se
na	na	k7c6	na
titulní	titulní	k2eAgFnSc6d1	titulní
straně	strana	k1gFnSc6	strana
objevila	objevit	k5eAaPmAgFnS	objevit
poprvé	poprvé	k6eAd1	poprvé
modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postupem	k7c2	postupem
doby	doba	k1gFnSc2	doba
změnily	změnit	k5eAaPmAgFnP	změnit
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgInPc1d1	ostatní
deníky	deník	k1gInPc1	deník
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
tvář	tvář	k1gFnSc4	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
příloha	příloha	k1gFnSc1	příloha
LN	LN	kA	LN
začal	začít	k5eAaPmAgInS	začít
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
vycházet	vycházet	k5eAaImF	vycházet
pravidelný	pravidelný	k2eAgInSc4d1	pravidelný
páteční	páteční	k2eAgInSc4d1	páteční
magazín	magazín	k1gInSc4	magazín
<g/>
,	,	kIx,	,
nejdříve	dříve	k6eAd3	dříve
jako	jako	k9	jako
Magazín	magazín	k1gInSc1	magazín
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
jako	jako	k9	jako
Pátek	Pátek	k1gMnSc1	Pátek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1998	[number]	k4	1998
získala	získat	k5eAaPmAgFnS	získat
většinový	většinový	k2eAgInSc4d1	většinový
podíl	podíl	k1gInSc4	podíl
v	v	k7c6	v
Lidových	lidový	k2eAgFnPc6d1	lidová
novinách	novina	k1gFnPc6	novina
společnost	společnost	k1gFnSc1	společnost
Pressinvest	Pressinvest	k1gFnSc1	Pressinvest
a.	a.	k?	a.
s.	s.	k?	s.
(	(	kIx(	(
<g/>
IČ	IČ	kA	IČ
43002331	[number]	k4	43002331
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
německá	německý	k2eAgFnSc1d1	německá
vydavatelská	vydavatelský	k2eAgFnSc1d1	vydavatelská
firma	firma	k1gFnSc1	firma
Rheinisch	Rheinisch	k1gInSc1	Rheinisch
Bergische	Bergische	k1gFnSc1	Bergische
Verlagsgesellschaft	Verlagsgesellschaft	k1gMnSc1	Verlagsgesellschaft
(	(	kIx(	(
<g/>
RBVG	RBVG	kA	RBVG
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vydavatelem	vydavatel	k1gMnSc7	vydavatel
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
(	(	kIx(	(
<g/>
a	a	k8xC	a
také	také	k9	také
deníku	deník	k1gInSc2	deník
MF	MF	kA	MF
Dnes	dnes	k6eAd1	dnes
<g/>
)	)	kIx)	)
společnost	společnost	k1gFnSc1	společnost
MAFRA	MAFRA	kA	MAFRA
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
,	,	kIx,	,
vlastněná	vlastněný	k2eAgFnSc1d1	vlastněná
rovněž	rovněž	k9	rovněž
RBVG	RBVG	kA	RBVG
.	.	kIx.	.
(	(	kIx(	(
<g/>
Společnost	společnost	k1gFnSc1	společnost
Pressinvest	Pressinvest	k1gFnSc1	Pressinvest
a.	a.	k?	a.
s.	s.	k?	s.
byla	být	k5eAaImAgFnS	být
ke	k	k7c3	k
dni	den	k1gInSc3	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
zrušena	zrušit	k5eAaPmNgFnS	zrušit
likvidací	likvidace	k1gFnSc7	likvidace
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
byly	být	k5eAaImAgFnP	být
modernizovány	modernizovat	k5eAaBmNgFnP	modernizovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevily	objevit	k5eAaPmAgFnP	objevit
celé	celá	k1gFnPc1	celá
v	v	k7c6	v
barvě	barva	k1gFnSc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Zakládají	zakládat	k5eAaImIp3nP	zakládat
také	také	k9	také
svou	svůj	k3xOyFgFnSc4	svůj
internetovou	internetový	k2eAgFnSc4d1	internetová
verzi	verze	k1gFnSc4	verze
na	na	k7c6	na
adrese	adresa	k1gFnSc6	adresa
www.lidovky.cz	www.lidovky.cz	k1gInSc4	www.lidovky.cz
a	a	k8xC	a
nasazují	nasazovat	k5eAaImIp3nP	nasazovat
moderní	moderní	k2eAgInSc4d1	moderní
redakční	redakční	k2eAgInSc4d1	redakční
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
online	onlinout	k5eAaPmIp3nS	onlinout
magazín	magazín	k1gInSc4	magazín
Lidovky	Lidovky	k1gFnPc4	Lidovky
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
Křišťálová	křišťálový	k2eAgFnSc1d1	Křišťálová
Lupa	lupa	k1gFnSc1	lupa
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
média	médium	k1gNnSc2	médium
-	-	kIx~	-
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
společnost	společnost	k1gFnSc1	společnost
MAFRA	MAFRA	kA	MAFRA
koupena	koupen	k2eAgFnSc1d1	koupena
od	od	k7c2	od
RBVG	RBVG	kA	RBVG
skupinou	skupina	k1gFnSc7	skupina
Agrofert	Agrofert	k1gMnSc1	Agrofert
Holding	holding	k1gInSc1	holding
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vlastní	vlastní	k2eAgMnSc1d1	vlastní
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
<g/>
;	;	kIx,	;
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
soutěže	soutěž	k1gFnSc2	soutěž
(	(	kIx(	(
<g/>
antimonopolní	antimonopolní	k2eAgInSc1d1	antimonopolní
úřad	úřad	k1gInSc1	úřad
<g/>
)	)	kIx)	)
tuto	tento	k3xDgFnSc4	tento
akvizici	akvizice	k1gFnSc4	akvizice
povolil	povolit	k5eAaPmAgMnS	povolit
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
"	"	kIx"	"
<g/>
rodném	rodný	k2eAgNnSc6d1	rodné
městě	město	k1gNnSc6	město
<g/>
"	"	kIx"	"
Brně	Brno	k1gNnSc6	Brno
sídlila	sídlit	k5eAaImAgFnS	sídlit
redakce	redakce	k1gFnSc1	redakce
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
domě	dům	k1gInSc6	dům
na	na	k7c4	na
Van	van	k1gInSc4	van
der	drát	k5eAaImRp2nS	drát
Strassově	Strassův	k2eAgFnSc6d1	Strassův
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
deníku	deník	k1gInSc2	deník
stal	stát	k5eAaPmAgMnS	stát
na	na	k7c4	na
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
dům	dům	k1gInSc1	dům
číslo	číslo	k1gNnSc1	číslo
popisné	popisný	k2eAgFnPc1d1	popisná
6	[number]	k4	6
na	na	k7c6	na
brněnské	brněnský	k2eAgFnSc6d1	brněnská
Rudolfově	Rudolfův	k2eAgFnSc6d1	Rudolfova
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
České	český	k2eAgFnSc6d1	Česká
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Pražská	pražský	k2eAgFnSc1d1	Pražská
redakce	redakce	k1gFnSc1	redakce
Lidovek	Lidovky	k1gFnPc2	Lidovky
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
a	a	k8xC	a
sídlila	sídlit	k5eAaImAgFnS	sídlit
ve	v	k7c6	v
Štěpánské	štěpánský	k2eAgFnSc6d1	Štěpánská
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
koupil	koupit	k5eAaPmAgMnS	koupit
dědic	dědic	k1gMnSc1	dědic
LN	LN	kA	LN
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Stránský	Stránský	k1gMnSc1	Stránský
Topičovo	topičův	k2eAgNnSc4d1	Topičovo
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
i	i	k9	i
s	s	k7c7	s
budovou	budova	k1gFnSc7	budova
na	na	k7c6	na
Národní	národní	k2eAgFnSc6d1	národní
třídě	třída	k1gFnSc6	třída
číslo	číslo	k1gNnSc1	číslo
9	[number]	k4	9
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
sídlem	sídlo	k1gNnSc7	sídlo
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
adresu	adresa	k1gFnSc4	adresa
se	se	k3xPyFc4	se
přestěhovaly	přestěhovat	k5eAaPmAgFnP	přestěhovat
obnovené	obnovený	k2eAgFnPc1d1	obnovená
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
o	o	k7c6	o
čtyřicet	čtyřicet	k4xCc4	čtyřicet
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
noviny	novina	k1gFnPc1	novina
usadily	usadit	k5eAaPmAgFnP	usadit
v	v	k7c6	v
domě	dům	k1gInSc6	dům
na	na	k7c6	na
rohu	roh	k1gInSc6	roh
Václavského	václavský	k2eAgNnSc2d1	Václavské
náměstí	náměstí	k1gNnSc2	náměstí
a	a	k8xC	a
Opletalovy	Opletalův	k2eAgFnSc2d1	Opletalova
ulice	ulice	k1gFnSc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Stěhování	stěhování	k1gNnSc1	stěhování
na	na	k7c4	na
pražský	pražský	k2eAgInSc4d1	pražský
Žižkov	Žižkov	k1gInSc4	Žižkov
<g/>
,	,	kIx,	,
do	do	k7c2	do
domu	dům	k1gInSc2	dům
na	na	k7c6	na
Žerotínově	Žerotínův	k2eAgFnSc6d1	Žerotínova
ulici	ulice	k1gFnSc6	ulice
čp.	čp.	k?	čp.
38	[number]	k4	38
<g/>
,	,	kIx,	,
přišlo	přijít	k5eAaPmAgNnS	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
sídlily	sídlit	k5eAaImAgFnP	sídlit
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
vlastněné	vlastněný	k2eAgFnPc1d1	vlastněná
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
švýcarskou	švýcarský	k2eAgFnSc7d1	švýcarská
společností	společnost	k1gFnSc7	společnost
Ringier	Ringira	k1gFnPc2	Ringira
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
devětadevadesátém	devětadevadesátý	k4xOgInSc6	devětadevadesátý
pronajímá	pronajímat	k5eAaImIp3nS	pronajímat
vydavatelství	vydavatelství	k1gNnSc3	vydavatelství
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
podporované	podporovaný	k2eAgFnSc2d1	podporovaná
düsseldorfským	düsseldorfský	k2eAgMnSc7d1	düsseldorfský
majoritním	majoritní	k2eAgMnSc7d1	majoritní
akcionářem	akcionář	k1gMnSc7	akcionář
zrekonstruovaný	zrekonstruovaný	k2eAgInSc4d1	zrekonstruovaný
objekt	objekt	k1gInSc4	objekt
v	v	k7c6	v
Pobřežní	pobřežní	k2eAgFnSc6d1	pobřežní
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
Karlíně	Karlín	k1gInSc6	Karlín
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
redakce	redakce	k1gFnSc1	redakce
sídlí	sídlet	k5eAaImIp3nS	sídlet
až	až	k9	až
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Karlín	Karlín	k1gInSc4	Karlín
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
povodňová	povodňový	k2eAgFnSc1d1	povodňová
vlna	vlna	k1gFnSc1	vlna
stoleté	stoletý	k2eAgFnSc2d1	stoletá
záplavy	záplava	k1gFnSc2	záplava
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátkém	krátký	k2eAgInSc6d1	krátký
azylu	azyl	k1gInSc6	azyl
poblíž	poblíž	k7c2	poblíž
tiskárny	tiskárna	k1gFnSc2	tiskárna
v	v	k7c6	v
Malešicích	Malešice	k1gFnPc6	Malešice
se	se	k3xPyFc4	se
redakce	redakce	k1gFnSc1	redakce
LN	LN	kA	LN
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2002	[number]	k4	2002
stěhuje	stěhovat	k5eAaImIp3nS	stěhovat
do	do	k7c2	do
domu	dům	k1gInSc2	dům
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
v	v	k7c6	v
Senovážné	Senovážný	k2eAgFnSc6d1	Senovážná
ulici	ulice	k1gFnSc6	ulice
4	[number]	k4	4
mezi	mezi	k7c7	mezi
Senovážným	Senovážný	k2eAgNnSc7d1	Senovážné
náměstím	náměstí	k1gNnSc7	náměstí
a	a	k8xC	a
Hybernskou	hybernský	k2eAgFnSc7d1	Hybernská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2004	[number]	k4	2004
se	se	k3xPyFc4	se
redakce	redakce	k1gFnSc1	redakce
LN	LN	kA	LN
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
moderní	moderní	k2eAgFnSc2d1	moderní
budovy	budova	k1gFnSc2	budova
Anděl	Anděla	k1gFnPc2	Anděla
Media	medium	k1gNnSc2	medium
Centrum	centrum	k1gNnSc4	centrum
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
Smíchově	Smíchov	k1gInSc6	Smíchov
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Karla	Karel	k1gMnSc2	Karel
Engliše	Engliš	k1gMnSc2	Engliš
<g/>
.	.	kIx.	.
</s>
<s>
Moravská	moravský	k2eAgFnSc1d1	Moravská
redakce	redakce	k1gFnSc1	redakce
sídlila	sídlit	k5eAaImAgFnS	sídlit
po	po	k7c6	po
obnovení	obnovení	k1gNnSc6	obnovení
deníku	deník	k1gInSc2	deník
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
Opletalově	Opletalův	k2eAgFnSc6d1	Opletalova
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
na	na	k7c4	na
Moravské	moravský	k2eAgNnSc4d1	Moravské
náměstí	náměstí	k1gNnSc4	náměstí
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2005	[number]	k4	2005
až	až	k9	až
2014	[number]	k4	2014
sídlila	sídlit	k5eAaImAgFnS	sídlit
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
ulici	ulice	k1gFnSc6	ulice
19	[number]	k4	19
<g/>
,	,	kIx,	,
symbolicky	symbolicky	k6eAd1	symbolicky
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
historického	historický	k2eAgNnSc2d1	historické
působiště	působiště	k1gNnSc2	působiště
listu	list	k1gInSc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc4	provoz
ukončila	ukončit	k5eAaPmAgFnS	ukončit
k	k	k7c3	k
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubnu	duben	k1gInSc3	duben
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
působení	působení	k1gNnSc1	působení
v	v	k7c6	v
LN	LN	kA	LN
po	po	k7c6	po
devíti	devět	k4xCc6	devět
letech	léto	k1gNnPc6	léto
ukončil	ukončit	k5eAaPmAgMnS	ukončit
poslední	poslední	k2eAgMnSc1d1	poslední
brněnský	brněnský	k2eAgMnSc1d1	brněnský
redaktor	redaktor	k1gMnSc1	redaktor
Miloš	Miloš	k1gMnSc1	Miloš
Šenkýř	Šenkýř	k1gMnSc1	Šenkýř
<g/>
.	.	kIx.	.
</s>
<s>
PERNES	PERNES	kA	PERNES
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc1	svět
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
1893-1993	[number]	k4	1893-1993
:	:	kIx,	:
stoletá	stoletý	k2eAgFnSc1d1	stoletá
kapitola	kapitola	k1gFnSc1	kapitola
z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
české	český	k2eAgFnSc2d1	Česká
žurnalistiky	žurnalistika	k1gFnSc2	žurnalistika
<g/>
,	,	kIx,	,
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
143	[number]	k4	143
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
56	[number]	k4	56
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
roku	rok	k1gInSc2	rok
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
-	-	kIx~	-
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
anketa	anketa	k1gFnSc1	anketa
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
Kategorie	kategorie	k1gFnSc2	kategorie
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
v	v	k7c6	v
Digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
-	-	kIx~	-
volně	volně	k6eAd1	volně
přístupné	přístupný	k2eAgInPc1d1	přístupný
od	od	k7c2	od
r.	r.	kA	r.
1893	[number]	k4	1893
do	do	k7c2	do
r.	r.	kA	r.
1945	[number]	k4	1945
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
1948	[number]	k4	1948
<g/>
-	-	kIx~	-
<g/>
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
Přístupné	přístupný	k2eAgNnSc4d1	přístupné
z	z	k7c2	z
budovy	budova	k1gFnSc2	budova
Moravské	moravský	k2eAgFnSc2d1	Moravská
zemské	zemský	k2eAgFnSc2d1	zemská
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Digitalizované	digitalizovaný	k2eAgInPc1d1	digitalizovaný
exempláře	exemplář	k1gInPc1	exemplář
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1892	[number]	k4	1892
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
chráněná	chráněný	k2eAgFnSc1d1	chráněná
autorskými	autorský	k2eAgNnPc7d1	autorské
právy	právo	k1gNnPc7	právo
přístupná	přístupný	k2eAgFnSc1d1	přístupná
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
NK	NK	kA	NK
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
Digitalizované	digitalizovaný	k2eAgInPc1d1	digitalizovaný
exempláře	exemplář	k1gInPc1	exemplář
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1948	[number]	k4	1948
<g/>
-	-	kIx~	-
<g/>
1952	[number]	k4	1952
(	(	kIx(	(
<g/>
přístupné	přístupný	k2eAgNnSc1d1	přístupné
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
NK	NK	kA	NK
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
Lidovky	Lidovky	k1gFnPc1	Lidovky
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
zprávy	zpráva	k1gFnPc1	zpráva
z	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s>
domova	domov	k1gInSc2	domov
i	i	k9	i
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
http://www.lidovky.cz/historie-obrazem.asp	[url]	k1gInSc1	http://www.lidovky.cz/historie-obrazem.asp
-	-	kIx~	-
ukázky	ukázka	k1gFnPc1	ukázka
samizdatových	samizdatový	k2eAgFnPc2d1	samizdatová
vydání	vydání	k1gNnSc6	vydání
Zpráva	zpráva	k1gFnSc1	zpráva
Federálního	federální	k2eAgNnSc2d1	federální
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
pro	pro	k7c4	pro
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
ze	z	k7c2	z
sborníku	sborník	k1gInSc2	sborník
Securitas	Securitas	k1gInSc1	Securitas
Imperii	imperie	k1gFnSc3	imperie
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vydal	vydat	k5eAaPmAgMnS	vydat
ÚDV	ÚDV	kA	ÚDV
Nakladatelství	nakladatelství	k1gNnPc4	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
-	-	kIx~	-
samostatný	samostatný	k2eAgInSc1d1	samostatný
subjekt	subjekt	k1gInSc1	subjekt
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
zaměřené	zaměřený	k2eAgNnSc1d1	zaměřené
zejména	zejména	k9	zejména
na	na	k7c4	na
literaturu	literatura	k1gFnSc4	literatura
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
Vyjádření	vyjádření	k1gNnSc1	vyjádření
redakce	redakce	k1gFnSc2	redakce
elektronické	elektronický	k2eAgFnSc2d1	elektronická
verze	verze	k1gFnSc2	verze
LN	LN	kA	LN
lidovky	lidovky	k1gFnPc1	lidovky
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
rekordního	rekordní	k2eAgInSc2d1	rekordní
milionu	milion	k4xCgInSc2	milion
čtenářů	čtenář	k1gMnPc2	čtenář
serveru	server	k1gInSc2	server
za	za	k7c4	za
měsíc	měsíc	k1gInSc4	měsíc
leden	leden	k1gInSc4	leden
2013	[number]	k4	2013
Aktuální	aktuální	k2eAgFnSc1d1	aktuální
titulní	titulní	k2eAgFnSc1d1	titulní
strana	strana	k1gFnSc1	strana
novin	novina	k1gFnPc2	novina
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
změnily	změnit	k5eAaPmAgFnP	změnit
po	po	k7c6	po
Babišově	Babišův	k2eAgInSc6d1	Babišův
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
Mafry	Mafra	k1gFnSc2	Mafra
-	-	kIx~	-
článek	článek	k1gInSc1	článek
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Přítomnost	přítomnost	k1gFnSc1	přítomnost
</s>
