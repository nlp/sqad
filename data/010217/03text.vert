<p>
<s>
Církevní	církevní	k2eAgFnSc1d1	církevní
archeologie	archeologie	k1gFnSc1	archeologie
(	(	kIx(	(
<g/>
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
archeologie	archeologie	k1gFnSc1	archeologie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
podoborem	podobor	k1gInSc7	podobor
archeologie	archeologie	k1gFnSc2	archeologie
<g/>
.	.	kIx.	.
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	s	k7c7	s
hmotnými	hmotný	k2eAgInPc7d1	hmotný
prameny	pramen	k1gInPc7	pramen
souvisejícími	související	k2eAgInPc7d1	související
s	s	k7c7	s
působením	působení	k1gNnSc7	působení
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
poznání	poznání	k1gNnSc4	poznání
těchto	tento	k3xDgFnPc2	tento
hmotných	hmotný	k2eAgFnPc2d1	hmotná
památek	památka	k1gFnPc2	památka
(	(	kIx(	(
<g/>
kostelů	kostel	k1gInPc2	kostel
<g/>
,	,	kIx,	,
bazilik	bazilika	k1gFnPc2	bazilika
<g/>
,	,	kIx,	,
katedrál	katedrála	k1gFnPc2	katedrála
<g/>
,	,	kIx,	,
kaplí	kaple	k1gFnPc2	kaple
<g/>
,	,	kIx,	,
rotund	rotunda	k1gFnPc2	rotunda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
liturgických	liturgický	k2eAgFnPc2d1	liturgická
nádob	nádoba	k1gFnPc2	nádoba
či	či	k8xC	či
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
textilií	textilie	k1gFnPc2	textilie
užívaných	užívaný	k2eAgFnPc2d1	užívaná
církevními	církevní	k2eAgInPc7d1	církevní
hodnostáři	hodnostář	k1gMnPc1	hodnostář
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poznat	poznat	k5eAaPmF	poznat
středověkou	středověký	k2eAgFnSc4d1	středověká
a	a	k8xC	a
novověkou	novověký	k2eAgFnSc4d1	novověká
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
tuto	tento	k3xDgFnSc4	tento
problematiku	problematika	k1gFnSc4	problematika
chronologicky	chronologicky	k6eAd1	chronologicky
vymezen	vymezit	k5eAaPmNgInS	vymezit
raným	raný	k2eAgInSc7d1	raný
středověkem	středověk	k1gInSc7	středověk
(	(	kIx(	(
<g/>
období	období	k1gNnSc1	období
velkomoravské	velkomoravský	k2eAgNnSc1d1	velkomoravské
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
Východě	východ	k1gInSc6	východ
jsou	být	k5eAaImIp3nP	být
zkoumány	zkoumán	k2eAgFnPc1d1	zkoumána
památky	památka	k1gFnPc1	památka
mnohem	mnohem	k6eAd1	mnohem
starší	starý	k2eAgFnPc1d2	starší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
akademickém	akademický	k2eAgNnSc6d1	akademické
prostředí	prostředí	k1gNnSc6	prostředí
není	být	k5eNaImIp3nS	být
obor	obor	k1gInSc1	obor
institucionálně	institucionálně	k6eAd1	institucionálně
zastoupen	zastoupit	k5eAaPmNgInS	zastoupit
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
sousedním	sousední	k2eAgNnSc6d1	sousední
Německu	Německo	k1gNnSc6	Německo
obor	obor	k1gInSc1	obor
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
archeologie	archeologie	k1gFnSc1	archeologie
existuje	existovat	k5eAaImIp3nS	existovat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
hlavních	hlavní	k2eAgFnPc6d1	hlavní
variantách	varianta	k1gFnPc6	varianta
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
na	na	k7c6	na
teologických	teologický	k2eAgFnPc6d1	teologická
fakultách	fakulta	k1gFnPc6	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
varianta	varianta	k1gFnSc1	varianta
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
sakrálními	sakrální	k2eAgFnPc7d1	sakrální
stavbami	stavba	k1gFnPc7	stavba
a	a	k8xC	a
křesťanským	křesťanský	k2eAgNnSc7d1	křesťanské
uměním	umění	k1gNnSc7	umění
od	od	k7c2	od
samého	samý	k3xTgInSc2	samý
počátku	počátek	k1gInSc2	počátek
křesťanství	křesťanství	k1gNnSc2	křesťanství
(	(	kIx(	(
<g/>
od	od	k7c2	od
pozdní	pozdní	k2eAgFnSc2d1	pozdní
antiky	antika	k1gFnSc2	antika
<g/>
)	)	kIx)	)
do	do	k7c2	do
konce	konec	k1gInSc2	konec
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
až	až	k9	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Humboldtova	Humboldtův	k2eAgFnSc1d1	Humboldtova
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
propojení	propojení	k1gNnSc1	propojení
předmětu	předmět	k1gInSc2	předmět
s	s	k7c7	s
dějinami	dějiny	k1gFnPc7	dějiny
umění	umění	k1gNnSc2	umění
Byzance	Byzanc	k1gFnSc2	Byzanc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
chronologický	chronologický	k2eAgInSc1d1	chronologický
rozsah	rozsah	k1gInSc1	rozsah
předmětu	předmět	k1gInSc2	předmět
zpravidla	zpravidla	k6eAd1	zpravidla
ukončen	ukončit	k5eAaPmNgInS	ukončit
polovinou	polovina	k1gFnSc7	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vyčleněný	vyčleněný	k2eAgInSc1d1	vyčleněný
obor	obor	k1gInSc1	obor
již	již	k6eAd1	již
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
obsahově	obsahově	k6eAd1	obsahově
vymezený	vymezený	k2eAgInSc1d1	vymezený
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
tak	tak	k6eAd1	tak
pouze	pouze	k6eAd1	pouze
navazovat	navazovat	k5eAaImF	navazovat
na	na	k7c4	na
klasickou	klasický	k2eAgFnSc4d1	klasická
archeologii	archeologie	k1gFnSc4	archeologie
<g/>
.	.	kIx.	.
</s>
</p>
