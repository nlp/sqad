<s>
František	František	k1gMnSc1	František
Štěpán	Štěpán	k1gMnSc1	Štěpán
požádal	požádat	k5eAaPmAgMnS	požádat
Marii	Maria	k1gFnSc4	Maria
Terezii	Terezie	k1gFnSc3	Terezie
o	o	k7c4	o
ruku	ruka	k1gFnSc4	ruka
dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1736	[number]	k4	1736
<g/>
,	,	kIx,	,
tentýž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
byli	být	k5eAaImAgMnP	být
ale	ale	k9	ale
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
odloučeni	odloučen	k2eAgMnPc1d1	odloučen
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
zůstala	zůstat	k5eAaPmAgFnS	zůstat
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
František	František	k1gMnSc1	František
Štěpán	Štěpán	k1gMnSc1	Štěpán
se	se	k3xPyFc4	se
přesunul	přesunout	k5eAaPmAgMnS	přesunout
do	do	k7c2	do
Bratislavy	Bratislava	k1gFnSc2	Bratislava
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
to	ten	k3xDgNnSc1	ten
vyžadovala	vyžadovat	k5eAaImAgFnS	vyžadovat
etiketa	etiketa	k1gFnSc1	etiketa
<g/>
.	.	kIx.	.
</s>
