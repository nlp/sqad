<s>
Južno-Sachalinsk	Južno-Sachalinsk	k1gInSc1
</s>
<s>
Južno-Sachalinsk	Južno-Sachalinsk	k1gInSc1
Ю	Ю	k?
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
46	#num#	k4
<g/>
°	°	k?
<g/>
57	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
142	#num#	k4
<g/>
°	°	k?
<g/>
44	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
44	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
m	m	kA
n.	n.	k?
m.	m.	k?
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
UTC	UTC	kA
<g/>
+	+	kIx~
<g/>
11	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Stát	stát	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
Rusko	Rusko	k1gNnSc4
federální	federální	k2eAgInSc1d1
okruh	okruh	k1gInSc1
</s>
<s>
Dálněvýchodní	dálněvýchodní	k2eAgInSc1d1
federální	federální	k2eAgInSc1d1
okruh	okruh	k1gInSc1
oblast	oblast	k1gFnSc1
</s>
<s>
Sachalinská	sachalinský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Sachalinská	sachalinský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
na	na	k7c6
mapě	mapa	k1gFnSc6
Ruska	Rusko	k1gNnSc2
</s>
<s>
Južno-Sachalinsk	Južno-Sachalinsk	k1gInSc1
</s>
<s>
Město	město	k1gNnSc1
na	na	k7c6
mapě	mapa	k1gFnSc6
Sachalinské	sachalinský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
164,7	164,7	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
194	#num#	k4
882	#num#	k4
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
1	#num#	k4
183,5	183,5	k4
obyv	obyva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
1882	#num#	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
yuzhno	yuzhno	k1gNnSc1
<g/>
.	.	kIx.
<g/>
sakh	sakh	k1gInSc1
<g/>
.	.	kIx.
<g/>
ru	ru	k?
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
+	+	kIx~
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
4242	#num#	k4
PSČ	PSČ	kA
</s>
<s>
693000	#num#	k4
<g/>
–	–	k?
<g/>
693023	#num#	k4
Označení	označení	k1gNnSc4
vozidel	vozidlo	k1gNnPc2
</s>
<s>
65	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Južno-Sachalinsk	Južno-Sachalinsk	k1gInSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
Ю	Ю	k?
<g/>
́	́	k?
<g/>
ж	ж	k?
<g/>
́	́	k?
<g/>
н	н	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
do	do	k7c2
roku	rok	k1gInSc2
1945	#num#	k4
Tojohara	Tojohara	k1gFnSc1
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
豊	豊	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
město	město	k1gNnSc1
v	v	k7c6
Rusku	Rusko	k1gNnSc6
<g/>
,	,	kIx,
na	na	k7c6
Dálném	dálný	k2eAgInSc6d1
východě	východ	k1gInSc6
<g/>
,	,	kIx,
na	na	k7c6
ostrově	ostrov	k1gInSc6
Sachalin	Sachalin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
na	na	k7c6
řece	řeka	k1gFnSc6
Susuja	Susuj	k1gInSc2
a	a	k8xC
je	být	k5eAaImIp3nS
správním	správní	k2eAgNnSc7d1
střediskem	středisko	k1gNnSc7
Sachalinské	sachalinský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
přibližně	přibližně	k6eAd1
195	#num#	k4
tisíc	tisíc	k4xCgInPc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Vladimirovka	Vladimirovka	k1gFnSc1
(	(	kIx(
<g/>
80	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Město	město	k1gNnSc4
původně	původně	k6eAd1
založili	založit	k5eAaPmAgMnP
Rusové	Rus	k1gMnPc1
pod	pod	k7c7
jménem	jméno	k1gNnSc7
Vladimirovka	Vladimirovka	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1882	#num#	k4
<g/>
;	;	kIx,
avšak	avšak	k8xC
v	v	k7c6
rusko-japonské	rusko-japonský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
o	o	k7c4
něj	on	k3xPp3gMnSc4
přišli	přijít	k5eAaPmAgMnP
–	–	k?
portsmouthským	portsmouthský	k2eAgInSc7d1
mírem	mír	k1gInSc7
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1905	#num#	k4
jižní	jižní	k2eAgFnSc6d1
části	část	k1gFnSc6
ostrova	ostrov	k1gInSc2
včetně	včetně	k7c2
tohoto	tento	k3xDgNnSc2
města	město	k1gNnSc2
zmocnili	zmocnit	k5eAaPmAgMnP
Japonci	Japonec	k1gMnPc1
a	a	k8xC
přejmenovali	přejmenovat	k5eAaPmAgMnP
jej	on	k3xPp3gMnSc4
na	na	k7c4
Tojohara	Tojohar	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Japonsko	Japonsko	k1gNnSc1
kapitulovalo	kapitulovat	k5eAaBmAgNnS
<g/>
,	,	kIx,
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
zabral	zabrat	k5eAaPmAgInS
celý	celý	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
Sachalin	Sachalin	k1gInSc1
a	a	k8xC
město	město	k1gNnSc1
bylo	být	k5eAaImAgNnS
od	od	k7c2
roku	rok	k1gInSc2
1946	#num#	k4
přejmenováno	přejmenovat	k5eAaPmNgNnS
na	na	k7c4
dnešní	dnešní	k2eAgInSc4d1
název	název	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Dnes	dnes	k6eAd1
se	se	k3xPyFc4
na	na	k7c6
Sachalinu	Sachalin	k1gInSc6
těží	těžet	k5eAaImIp3nS
hlavně	hlavně	k9
zemní	zemní	k2eAgInSc1d1
plyn	plyn	k1gInSc1
<g/>
,	,	kIx,
díky	díky	k7c3
přílivu	příliv	k1gInSc3
investic	investice	k1gFnPc2
od	od	k7c2
společností	společnost	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
např.	např.	kA
ExxonMobil	ExxonMobil	k1gMnSc1
a	a	k8xC
Shell	Shell	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
populace	populace	k1gFnSc1
je	být	k5eAaImIp3nS
ruské	ruský	k2eAgFnPc4d1
národnosti	národnost	k1gFnPc4
<g/>
,	,	kIx,
významnější	významný	k2eAgFnSc4d2
menšinu	menšina	k1gFnSc4
tvoří	tvořit	k5eAaImIp3nP
Korejci	Korejec	k1gMnPc1
(	(	kIx(
<g/>
12	#num#	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Doprava	doprava	k1gFnSc1
</s>
<s>
Město	město	k1gNnSc1
je	být	k5eAaImIp3nS
spojeno	spojit	k5eAaPmNgNnS
mezinárodním	mezinárodní	k2eAgNnSc7d1
letištěm	letiště	k1gNnSc7
Južno-Sachalinsk	Južno-Sachalinsk	k1gInSc1
(	(	kIx(
<g/>
ICAO	ICAO	kA
<g/>
:	:	kIx,
UHSS	UHSS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrovem	ostrov	k1gInSc7
vede	vést	k5eAaImIp3nS
železniční	železniční	k2eAgFnSc1d1
síť	síť	k1gFnSc1
<g/>
,	,	kIx,
Japonci	Japonec	k1gMnPc1
vybudovali	vybudovat	k5eAaPmAgMnP
první	první	k4xOgInSc4
spoj	spoj	k1gInSc4
roku	rok	k1gInSc2
1906	#num#	k4
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
sovětské	sovětský	k2eAgFnSc6d1
éře	éra	k1gFnSc6
byla	být	k5eAaImAgFnS
trať	trať	k1gFnSc1
rozšířená	rozšířený	k2eAgFnSc1d1
i	i	k9
do	do	k7c2
severních	severní	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
ostrova	ostrov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
byla	být	k5eAaImAgFnS
trať	trať	k1gFnSc1
dlouhá	dlouhý	k2eAgFnSc1d1
805	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
budoucna	budoucno	k1gNnSc2
se	se	k3xPyFc4
uvažuje	uvažovat	k5eAaImIp3nS
o	o	k7c4
vybudování	vybudování	k1gNnSc4
podmořského	podmořský	k2eAgInSc2d1
tunelu	tunel	k1gInSc2
k	k	k7c3
ostrovu	ostrov	k1gInSc3
Hokkaidó	Hokkaidó	k1gFnSc2
a	a	k8xC
o	o	k7c6
mostu	most	k1gInSc2
na	na	k7c6
severní	severní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
k	k	k7c3
pevnině	pevnina	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Južno-Sachalinské	Južno-Sachalinský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
</s>
<s>
Katedrála	katedrála	k1gFnSc1
Vzkříšení	vzkříšení	k1gNnSc2
</s>
<s>
Hotel	hotel	k1gInSc1
Megapalac	Megapalac	k1gInSc1
</s>
<s>
Noční	noční	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
město	město	k1gNnSc4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Global	globat	k5eAaImAgInS
Gazetteer	Gazetteer	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falling	Falling	k1gInSc1
Rain	Rain	k1gNnSc1
Genomics	Genomics	k1gInSc1
<g/>
,	,	kIx,
Inc	Inc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ruský	ruský	k2eAgInSc1d1
federální	federální	k2eAgInSc1d1
zákon	zákon	k1gInSc1
107-Ф	107-Ф	k?
Moskva	Moskva	k1gFnSc1
<g/>
:	:	kIx,
П	П	k?
Р	Р	k?
Ф	Ф	k?
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2016-05-04	2016-05-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Russian	Russian	k1gInSc1
Far	fara	k1gFnPc2
East	East	k1gMnSc1
railway	railwaa	k1gFnSc2
project	project	k1gMnSc1
may	may	k?
extend	extend	k1gMnSc1
to	ten	k3xDgNnSc1
Hokkaido	Hokkaida	k1gFnSc5
<g/>
.	.	kIx.
ajw	ajw	k?
<g/>
.	.	kIx.
<g/>
asahi	asah	k1gFnSc2
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Južno-Sachalinsk	Južno-Sachalinsk	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Historie	historie	k1gFnSc1
Južno-Sachalinsku	Južno-Sachalinsko	k1gNnSc3
na	na	k7c6
oficiální	oficiální	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
Sachalinu	Sachalin	k1gInSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Rusko	Rusko	k1gNnSc1
–	–	k?
Р	Р	k?
–	–	k?
(	(	kIx(
<g/>
RUS	Rus	k1gMnSc1
<g/>
)	)	kIx)
Subjekty	subjekt	k1gInPc1
Ruské	ruský	k2eAgFnSc2d1
federace	federace	k1gFnSc2
Republiky	republika	k1gFnSc2
</s>
<s>
Adygejsko	Adygejsko	k1gNnSc1
(	(	kIx(
<g/>
Majkop	Majkop	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Altajsko	Altajsko	k1gNnSc1
(	(	kIx(
<g/>
Gorno-Altajsk	Gorno-Altajsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Baškortostán	Baškortostán	k1gInSc1
(	(	kIx(
<g/>
Ufa	Ufa	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Burjatsko	Burjatsko	k1gNnSc1
(	(	kIx(
<g/>
Ulan-Ude	Ulan-Ud	k1gMnSc5
<g/>
)	)	kIx)
</s>
<s>
Čečensko	Čečensko	k1gNnSc1
(	(	kIx(
<g/>
Grozný	Grozný	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Čuvašsko	Čuvašsko	k1gNnSc1
(	(	kIx(
<g/>
Čeboksary	Čeboksara	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Dagestán	Dagestán	k1gInSc1
(	(	kIx(
<g/>
Machačkala	Machačkala	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Chakasie	Chakasie	k1gFnSc1
(	(	kIx(
<g/>
Abakan	Abakan	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Ingušsko	Ingušsko	k1gNnSc1
(	(	kIx(
<g/>
Magas	Magas	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kabardsko-Balkarsko	Kabardsko-Balkarsko	k1gNnSc1
(	(	kIx(
<g/>
Nalčik	Nalčik	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kalmycko	Kalmycko	k1gNnSc1
(	(	kIx(
<g/>
Elista	Elista	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Karačajevsko-Čerkesko	Karačajevsko-Čerkesko	k1gNnSc1
(	(	kIx(
<g/>
Čerkesk	Čerkesk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Karélie	Karélie	k1gFnSc1
(	(	kIx(
<g/>
Petrozavodsk	Petrozavodsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Komi	Komi	k1gNnSc1
(	(	kIx(
<g/>
Syktyvkar	Syktyvkar	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Krym	Krym	k1gInSc1
(	(	kIx(
<g/>
Simferopol	Simferopol	k1gInSc1
<g/>
)	)	kIx)
<g/>
¹	¹	k?
</s>
<s>
Marijsko	Marijsko	k1gNnSc1
(	(	kIx(
<g/>
Joškar-Ola	Joškar-Ola	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Mordvinsko	Mordvinsko	k1gNnSc1
(	(	kIx(
<g/>
Saransk	Saransk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Sacha	Sacha	k1gFnSc1
(	(	kIx(
<g/>
Jakutsk	Jakutsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Osetie-Alanie	Osetie-Alanie	k1gFnSc1
(	(	kIx(
<g/>
Vladikavkaz	Vladikavkaz	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Tatarstán	Tatarstán	k1gInSc1
(	(	kIx(
<g/>
Kazaň	Kazaň	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Tuva	Tuva	k1gFnSc1
(	(	kIx(
<g/>
Kyzyl	Kyzyl	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Udmurtsko	Udmurtsko	k1gNnSc1
(	(	kIx(
<g/>
Iževsk	Iževsk	k1gInSc1
<g/>
)	)	kIx)
Kraje	kraj	k1gInPc1
</s>
<s>
Altajský	altajský	k2eAgInSc1d1
(	(	kIx(
<g/>
Barnaul	Barnaul	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Chabarovský	chabarovský	k2eAgInSc1d1
(	(	kIx(
<g/>
Chabarovsk	Chabarovsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kamčatský	kamčatský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Petropavlovsk-Kamčatskij	Petropavlovsk-Kamčatskij	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Krasnodarský	krasnodarský	k2eAgInSc1d1
(	(	kIx(
<g/>
Krasnodar	Krasnodar	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Krasnojarský	Krasnojarský	k2eAgInSc1d1
(	(	kIx(
<g/>
Krasnojarsk	Krasnojarsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Permský	permský	k2eAgInSc1d1
(	(	kIx(
<g/>
Perm	perm	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Přímořský	přímořský	k2eAgInSc4d1
(	(	kIx(
<g/>
Vladivostok	Vladivostok	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Stavropolský	stavropolský	k2eAgInSc1d1
(	(	kIx(
<g/>
Stavropol	Stavropol	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Zabajkalský	zabajkalský	k2eAgInSc1d1
(	(	kIx(
<g/>
Čita	čit	k2eAgFnSc1d1
<g/>
)	)	kIx)
Oblasti	oblast	k1gFnPc1
</s>
<s>
Amurská	amurský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Blagověščensk	Blagověščensk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Archangelská	archangelský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Archangelsk	Archangelsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Astrachaňská	astrachaňský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Astrachaň	Astrachaň	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Bělgorodská	Bělgorodský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Bělgorod	Bělgorod	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Brjanská	Brjanský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Brjansk	Brjansk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Čeljabinská	čeljabinský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Čeljabinsk	Čeljabinsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Irkutská	irkutský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Irkutsk	Irkutsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Ivanovská	Ivanovský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Ivanovo	Ivanův	k2eAgNnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Jaroslavská	Jaroslavský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Jaroslavl	Jaroslavl	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Kaliningradská	kaliningradský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Kaliningrad	Kaliningrad	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kalužská	Kalužský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Kaluga	Kaluga	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Kemerovská	Kemerovský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Kemerovo	Kemerův	k2eAgNnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Kirovská	Kirovská	k1gFnSc1
(	(	kIx(
<g/>
Kirov	Kirov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kostromská	Kostromský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Kostroma	Kostroma	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Kurganská	Kurganský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Kurgan	kurgan	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kurská	kurský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Kursk	Kursk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Leningradská	leningradský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Petrohrad	Petrohrad	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Lipecká	Lipecká	k1gFnSc1
(	(	kIx(
<g/>
Lipeck	Lipeck	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Magadanská	Magadanský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Magadan	Magadan	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Moskevská	moskevský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Moskva	Moskva	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Murmanská	murmanský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Murmansk	Murmansk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Nižněnovgorodská	Nižněnovgorodský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
(	(	kIx(
<g/>
Nižnij	Nižnij	k1gFnSc1
Novgorod	Novgorod	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Novgorodská	novgorodský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Veliký	veliký	k2eAgInSc1d1
Novgorod	Novgorod	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Novosibirská	novosibirský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Novosibirsk	Novosibirsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Omská	omský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Omsk	Omsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Orelská	orelský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Orel	Orel	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Orenburská	orenburský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Orenburg	Orenburg	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Penzenská	Penzenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Penza	Penza	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Pskovská	Pskovská	k1gFnSc1
(	(	kIx(
<g/>
Pskov	Pskov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Rjazaňská	rjazaňský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Rjazaň	Rjazaň	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Rostovská	Rostovský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Rostov	Rostov	k1gInSc1
na	na	k7c4
Donu	dona	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Sachalinská	sachalinský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Južno-Sachalinsk	Južno-Sachalinsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Samarská	samarský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Samara	Samara	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Saratovská	Saratovský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Saratov	Saratovo	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Smolenská	Smolenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Smolensk	Smolensk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Sverdlovská	sverdlovský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Jekatěrinburg	Jekatěrinburg	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Tambovská	Tambovská	k1gFnSc1
(	(	kIx(
<g/>
Tambov	Tambov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Tomská	Tomská	k1gFnSc1
(	(	kIx(
<g/>
Tomsk	Tomsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Tverská	Tverský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Tver	Tver	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Tulská	tulský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Tula	Tula	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Ťumeňská	Ťumeňský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Ťumeň	Ťumeň	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Uljanovská	Uljanovský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Uljanovsk	Uljanovsk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Vladimirská	Vladimirský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Vladimir	Vladimir	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Volgogradská	volgogradský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Volgograd	Volgograd	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Vologdská	Vologdský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Vologda	Vologda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Voroněžská	voroněžský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Voroněž	Voroněž	k1gFnSc1
<g/>
)	)	kIx)
Federální	federální	k2eAgNnSc1d1
města	město	k1gNnSc2
</s>
<s>
Moskva	Moskva	k1gFnSc1
</s>
<s>
Petrohrad	Petrohrad	k1gInSc1
</s>
<s>
Sevastopol¹	Sevastopol¹	k?
Autonomní	autonomní	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Židovská	židovská	k1gFnSc1
(	(	kIx(
<g/>
Birobidžan	Birobidžan	k1gInSc1
<g/>
)	)	kIx)
Autonomní	autonomní	k2eAgInPc1d1
okruhy	okruh	k1gInPc1
</s>
<s>
Čukotský	čukotský	k2eAgInSc1d1
(	(	kIx(
<g/>
Anadyr	Anadyr	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Chantymansijský	Chantymansijský	k2eAgInSc1d1
(	(	kIx(
<g/>
Chanty-Mansijsk	Chanty-Mansijsk	k1gInSc1
<g/>
)	)	kIx)
<g/>
²	²	k?
</s>
<s>
Jamalo-něnecký	Jamalo-něnecký	k2eAgInSc1d1
(	(	kIx(
<g/>
Salechard	Salechard	k1gInSc1
<g/>
)	)	kIx)
<g/>
²	²	k?
</s>
<s>
Něnecký	Něnecký	k2eAgInSc1d1
(	(	kIx(
<g/>
Narjan-Mar	Narjan-Mar	k1gInSc1
<g/>
)	)	kIx)
<g/>
³	³	k?
</s>
<s>
¹	¹	k?
Nárokováno	nárokován	k2eAgNnSc1d1
Ukrajinou	Ukrajina	k1gFnSc7
<g/>
,	,	kIx,
mezinárodně	mezinárodně	k6eAd1
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
součást	součást	k1gFnSc4
Ukrajiny	Ukrajina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
²	²	k?
Spravováno	spravovat	k5eAaImNgNnS
orgány	orgán	k1gInPc7
Ťumeňské	Ťumeňský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
³	³	k?
Spravováno	spravovat	k5eAaImNgNnS
orgány	orgán	k1gInPc7
Archangelské	archangelský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Rusko	Rusko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
793275	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4408781-0	4408781-0	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
82081034	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
141916358	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
82081034	#num#	k4
</s>
