<p>
<s>
Přísloví	přísloví	k1gNnSc1	přísloví
je	být	k5eAaImIp3nS	být
ustálený	ustálený	k2eAgInSc1d1	ustálený
stylizovaný	stylizovaný	k2eAgInSc1d1	stylizovaný
výrok	výrok	k1gInSc1	výrok
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
obecně	obecně	k6eAd1	obecně
platné	platný	k2eAgFnPc4d1	platná
zásady	zásada	k1gFnPc4	zásada
nebo	nebo	k8xC	nebo
zkušenosti	zkušenost	k1gFnPc4	zkušenost
mravoučné	mravoučný	k2eAgFnSc2d1	mravoučná
povahy	povaha	k1gFnSc2	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
jsou	být	k5eAaImIp3nP	být
vyjádřeny	vyjádřen	k2eAgInPc1d1	vyjádřen
buď	buď	k8xC	buď
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
obrazně	obrazně	k6eAd1	obrazně
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
snadnější	snadný	k2eAgNnSc4d2	snazší
zapamatování	zapamatování	k1gNnSc4	zapamatování
má	mít	k5eAaImIp3nS	mít
často	často	k6eAd1	často
podobu	podoba	k1gFnSc4	podoba
rýmovaného	rýmovaný	k2eAgNnSc2d1	rýmované
dvojverší	dvojverší	k1gNnSc2	dvojverší
<g/>
.	.	kIx.	.
</s>
<s>
Přísloví	přísloví	k1gNnSc1	přísloví
tvoří	tvořit	k5eAaImIp3nS	tvořit
součást	součást	k1gFnSc4	součást
lidové	lidový	k2eAgFnSc2d1	lidová
slovesnosti	slovesnost	k1gFnSc2	slovesnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přísloví	přísloví	k1gNnPc1	přísloví
jsou	být	k5eAaImIp3nP	být
doložena	doložit	k5eAaPmNgNnP	doložit
už	už	k6eAd1	už
ve	v	k7c6	v
starověkých	starověký	k2eAgFnPc6d1	starověká
literárních	literární	k2eAgFnPc6d1	literární
památkách	památka	k1gFnPc6	památka
(	(	kIx(	(
<g/>
asyrských	asyrský	k2eAgFnPc2d1	Asyrská
<g/>
,	,	kIx,	,
staroindických	staroindický	k2eAgInPc2d1	staroindický
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
a	a	k8xC	a
lze	lze	k6eAd1	lze
to	ten	k3xDgNnSc4	ten
snadno	snadno	k6eAd1	snadno
ověřit	ověřit	k5eAaPmF	ověřit
například	například	k6eAd1	například
v	v	k7c6	v
citátech	citát	k1gInPc6	citát
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
přísloví	přísloví	k1gNnSc4	přísloví
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
jazycích	jazyk	k1gInPc6	jazyk
často	často	k6eAd1	často
úplně	úplně	k6eAd1	úplně
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
dané	daný	k2eAgFnSc2d1	daná
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
jejího	její	k3xOp3gMnSc2	její
sociálního	sociální	k2eAgMnSc2d1	sociální
a	a	k8xC	a
historického	historický	k2eAgInSc2d1	historický
kontextu	kontext	k1gInSc2	kontext
<g/>
,	,	kIx,	,
vystihují	vystihovat	k5eAaImIp3nP	vystihovat
naprosto	naprosto	k6eAd1	naprosto
stejný	stejný	k2eAgInSc4d1	stejný
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Představují	představovat	k5eAaImIp3nP	představovat
tak	tak	k9	tak
pravdu	pravda	k1gFnSc4	pravda
skrytou	skrytý	k2eAgFnSc4d1	skrytá
za	za	k7c7	za
symbolickou	symbolický	k2eAgFnSc7d1	symbolická
formou	forma	k1gFnSc7	forma
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
z	z	k7c2	z
generace	generace	k1gFnSc2	generace
na	na	k7c4	na
generaci	generace	k1gFnSc4	generace
bez	bez	k7c2	bez
pozorovatelné	pozorovatelný	k2eAgFnSc2d1	pozorovatelná
degenerace	degenerace	k1gFnSc2	degenerace
obsahu	obsah	k1gInSc2	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Přísloví	přísloví	k1gNnSc1	přísloví
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
výroků	výrok	k1gInPc2	výrok
nikdy	nikdy	k6eAd1	nikdy
nemají	mít	k5eNaImIp3nP	mít
autora	autor	k1gMnSc2	autor
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
dědictvím	dědictví	k1gNnSc7	dědictví
zdravého	zdravý	k2eAgInSc2d1	zdravý
selského	selský	k2eAgInSc2d1	selský
rozumu	rozum	k1gInSc2	rozum
našich	náš	k3xOp1gInPc2	náš
předků	předek	k1gInPc2	předek
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
tak	tak	k9	tak
součást	součást	k1gFnSc4	součást
národní	národní	k2eAgFnSc2d1	národní
tradice	tradice	k1gFnSc2	tradice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
lidem	člověk	k1gMnPc3	člověk
od	od	k7c2	od
malička	maličko	k1gNnSc2	maličko
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
se	s	k7c7	s
vstřebáním	vstřebání	k1gNnSc7	vstřebání
základních	základní	k2eAgFnPc2d1	základní
morálních	morální	k2eAgFnPc2d1	morální
a	a	k8xC	a
lidských	lidský	k2eAgFnPc2d1	lidská
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Etnograficko-lingvistická	etnografickoingvistický	k2eAgFnSc1d1	etnograficko-lingvistický
věda	věda	k1gFnSc1	věda
zkoumající	zkoumající	k2eAgNnSc1d1	zkoumající
přísloví	přísloví	k1gNnSc1	přísloví
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
paremiologie	paremiologie	k1gFnSc1	paremiologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přísloví	přísloví	k1gNnSc1	přísloví
a	a	k8xC	a
rčení	rčení	k1gNnSc1	rčení
==	==	k?	==
</s>
</p>
<p>
<s>
Rčení	rčení	k1gNnSc1	rčení
je	být	k5eAaImIp3nS	být
útvarem	útvar	k1gInSc7	útvar
podobným	podobný	k2eAgInSc7d1	podobný
přísloví	přísloví	k1gNnSc4	přísloví
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
v	v	k7c6	v
několika	několik	k4yIc6	několik
ohledech	ohled	k1gInPc6	ohled
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Přísloví	přísloví	k1gNnSc1	přísloví
je	být	k5eAaImIp3nS	být
vyjádřením	vyjádření	k1gNnSc7	vyjádření
nějaké	nějaký	k3yIgFnSc2	nějaký
životní	životní	k2eAgFnSc2d1	životní
moudrosti	moudrost	k1gFnSc2	moudrost
<g/>
,	,	kIx,	,
rčení	rčení	k1gNnSc1	rčení
je	být	k5eAaImIp3nS	být
produktem	produkt	k1gInSc7	produkt
lidové	lidový	k2eAgFnSc2d1	lidová
fantazie	fantazie	k1gFnSc2	fantazie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Smyslem	smysl	k1gInSc7	smysl
přísloví	přísloví	k1gNnSc2	přísloví
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
morální	morální	k2eAgNnSc4d1	morální
ponaučení	ponaučení	k1gNnSc4	ponaučení
<g/>
,	,	kIx,	,
výstraha	výstraha	k1gFnSc1	výstraha
či	či	k8xC	či
pokárání	pokárání	k1gNnSc1	pokárání
<g/>
.	.	kIx.	.
</s>
<s>
Smyslem	smysl	k1gInSc7	smysl
rčení	rčení	k1gNnSc2	rčení
je	být	k5eAaImIp3nS	být
pobavení	pobavení	k1gNnSc4	pobavení
<g/>
,	,	kIx,	,
oživení	oživení	k1gNnSc4	oživení
jazykového	jazykový	k2eAgInSc2d1	jazykový
projevu	projev	k1gInSc2	projev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přísloví	přísloví	k1gNnSc1	přísloví
tvoří	tvořit	k5eAaImIp3nS	tvořit
celé	celý	k2eAgFnPc4d1	celá
formalizované	formalizovaný	k2eAgFnPc4d1	formalizovaná
věty	věta	k1gFnPc4	věta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
nelze	lze	k6eNd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
<g/>
,	,	kIx,	,
přeformulovat	přeformulovat	k5eAaPmF	přeformulovat
<g/>
,	,	kIx,	,
skloňovat	skloňovat	k5eAaImF	skloňovat
či	či	k8xC	či
časovat	časovat	k5eAaBmF	časovat
<g/>
.	.	kIx.	.
</s>
<s>
Rčení	rčení	k1gNnPc1	rčení
a	a	k8xC	a
pořekadla	pořekadlo	k1gNnPc1	pořekadlo
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
částmi	část	k1gFnPc7	část
vět	věta	k1gFnPc2	věta
<g/>
,	,	kIx,	,
rčení	rčení	k1gNnPc2	rčení
lze	lze	k6eAd1	lze
skloňovat	skloňovat	k5eAaImF	skloňovat
i	i	k8xC	i
časovat	časovat	k5eAaBmF	časovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
českých	český	k2eAgNnPc2d1	české
přísloví	přísloví	k1gNnPc2	přísloví
==	==	k?	==
</s>
</p>
<p>
<s>
Bez	bez	k7c2	bez
práce	práce	k1gFnSc2	práce
nejsou	být	k5eNaImIp3nP	být
koláče	koláč	k1gInPc1	koláč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kdo	kdo	k3yInSc1	kdo
jinému	jiný	k2eAgMnSc3d1	jiný
jámu	jáma	k1gFnSc4	jáma
kopá	kopat	k5eAaImIp3nS	kopat
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
padá	padat	k5eAaImIp3nS	padat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lepší	dobrý	k2eAgMnSc1d2	lepší
vrabec	vrabec	k1gMnSc1	vrabec
v	v	k7c6	v
hrsti	hrst	k1gFnSc6	hrst
nežli	nežli	k8xS	nežli
holub	holub	k1gMnSc1	holub
na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Komu	kdo	k3yRnSc3	kdo
se	se	k3xPyFc4	se
nelení	lenit	k5eNaImIp3nS	lenit
<g/>
,	,	kIx,	,
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
zelení	zelenit	k5eAaImIp3nS	zelenit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
do	do	k7c2	do
lesa	les	k1gInSc2	les
volá	volat	k5eAaImIp3nS	volat
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
se	se	k3xPyFc4	se
z	z	k7c2	z
lesa	les	k1gInSc2	les
ozývá	ozývat	k5eAaImIp3nS	ozývat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tichá	tichý	k2eAgFnSc1d1	tichá
voda	voda	k1gFnSc1	voda
břehy	břeh	k1gInPc4	břeh
mele	mlít	k5eAaImIp3nS	mlít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kdo	kdo	k3yInSc1	kdo
chce	chtít	k5eAaImIp3nS	chtít
psa	pes	k1gMnSc4	pes
bít	bít	k5eAaImF	bít
<g/>
,	,	kIx,	,
hůl	hůl	k1gFnSc4	hůl
si	se	k3xPyFc3	se
vždy	vždy	k6eAd1	vždy
najde	najít	k5eAaPmIp3nS	najít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tak	tak	k9	tak
dlouho	dlouho	k6eAd1	dlouho
se	se	k3xPyFc4	se
chodí	chodit	k5eAaImIp3nS	chodit
se	s	k7c7	s
džbánem	džbán	k1gInSc7	džbán
pro	pro	k7c4	pro
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
se	se	k3xPyFc4	se
ucho	ucho	k1gNnSc1	ucho
utrhne	utrhnout	k5eAaPmIp3nS	utrhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
dva	dva	k4xCgInPc1	dva
perou	prát	k5eAaImIp3nP	prát
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgMnSc1	třetí
se	se	k3xPyFc4	se
směje	smát	k5eAaImIp3nS	smát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
naučíš	naučit	k5eAaPmIp2nS	naučit
<g/>
,	,	kIx,	,
v	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
jako	jako	k8xS	jako
když	když	k8xS	když
najdeš	najít	k5eAaPmIp2nS	najít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
šeptem	šeptem	k6eAd1	šeptem
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
čertem	čert	k1gMnSc7	čert
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kdo	kdo	k3yInSc1	kdo
lže	lhát	k5eAaImIp3nS	lhát
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
krade	krást	k5eAaImIp3nS	krást
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Špinavé	špinavý	k2eAgNnSc1d1	špinavé
prádlo	prádlo	k1gNnSc1	prádlo
se	se	k3xPyFc4	se
nevynáší	vynášet	k5eNaImIp3nS	vynášet
z	z	k7c2	z
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Rychlé	Rychlé	k2eAgFnPc4d1	Rychlé
nohy	noha	k1gFnPc4	noha
<g/>
,	,	kIx,	,
krátký	krátký	k2eAgInSc4d1	krátký
rozum	rozum	k1gInSc4	rozum
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Komu	kdo	k3yRnSc3	kdo
není	být	k5eNaImIp3nS	být
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
tomu	ten	k3xDgNnSc3	ten
není	být	k5eNaImIp3nS	být
pomoci	pomoct	k5eAaPmF	pomoct
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Kdo	kdo	k3yQnSc1	kdo
uteče	utéct	k5eAaPmIp3nS	utéct
<g/>
,	,	kIx,	,
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Moudřejší	moudrý	k2eAgMnSc1d2	moudřejší
ustoupí	ustoupit	k5eAaPmIp3nS	ustoupit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Koho	kdo	k3yInSc2	kdo
chleba	chléb	k1gInSc2	chléb
jíš	jíst	k5eAaImIp2nS	jíst
<g/>
,	,	kIx,	,
toho	ten	k3xDgMnSc4	ten
píseň	píseň	k1gFnSc1	píseň
zpívej	zpívat	k5eAaImRp2nS	zpívat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Boží	boží	k2eAgInPc1d1	boží
mlýny	mlýn	k1gInPc1	mlýn
melou	mlít	k5eAaImIp3nP	mlít
pomalu	pomalu	k6eAd1	pomalu
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jistě	jistě	k9	jistě
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Práce	práce	k1gFnPc1	práce
kvapná	kvapný	k2eAgFnSc1d1	kvapná
<g/>
,	,	kIx,	,
málo	málo	k6eAd1	málo
platná	platný	k2eAgFnSc1d1	platná
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Bližší	blízký	k2eAgFnPc4d2	bližší
košile	košile	k1gFnPc4	košile
nežli	nežli	k8xS	nežli
kabát	kabát	k1gInSc4	kabát
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Zvyk	zvyk	k1gInSc4	zvyk
je	být	k5eAaImIp3nS	být
železná	železný	k2eAgFnSc1d1	železná
košile	košile	k1gFnSc1	košile
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Pořekadlo	pořekadlo	k1gNnSc1	pořekadlo
</s>
</p>
<p>
<s>
Frazeologizmus	frazeologizmus	k1gInSc1	frazeologizmus
</s>
</p>
<p>
<s>
Idiom	idiom	k1gInSc1	idiom
</s>
</p>
<p>
<s>
Aforismus	aforismus	k1gInSc1	aforismus
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
přísloví	přísloví	k1gNnSc2	přísloví
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Czech	Czecha	k1gFnPc2	Czecha
proverbs	proverbsa	k1gFnPc2	proverbsa
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
přísloví	přísloví	k1gNnSc2	přísloví
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Česká	český	k2eAgFnSc1d1	Česká
přísloví	přísloví	k1gNnSc4	přísloví
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
