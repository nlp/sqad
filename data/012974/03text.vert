<p>
<s>
Světelný	světelný	k2eAgInSc1d1	světelný
meč	meč	k1gInSc1	meč
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
lightsaber	lightsaber	k1gInSc1	lightsaber
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
fiktivní	fiktivní	k2eAgFnSc4d1	fiktivní
zbraň	zbraň	k1gFnSc4	zbraň
z	z	k7c2	z
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
her	hra	k1gFnPc2	hra
a	a	k8xC	a
literárních	literární	k2eAgInPc2d1	literární
příběhů	příběh	k1gInPc2	příběh
vesmíru	vesmír	k1gInSc2	vesmír
Star	Star	kA	Star
Wars	Wars	k1gInSc1	Wars
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
futuristickou	futuristický	k2eAgFnSc4d1	futuristická
verzi	verze	k1gFnSc4	verze
klasického	klasický	k2eAgInSc2d1	klasický
meče	meč	k1gInSc2	meč
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
místo	místo	k1gNnSc4	místo
kovové	kovový	k2eAgFnSc2d1	kovová
čepele	čepel	k1gFnSc2	čepel
proud	proud	k1gInSc1	proud
jasně	jasně	k6eAd1	jasně
zářící	zářící	k2eAgFnSc2d1	zářící
bílé	bílý	k2eAgFnSc2d1	bílá
energie	energie	k1gFnSc2	energie
obklopené	obklopený	k2eAgNnSc1d1	obklopené
barevnou	barevný	k2eAgFnSc7d1	barevná
korónou	koróna	k1gFnSc7	koróna
(	(	kIx(	(
<g/>
vzhledem	vzhled	k1gInSc7	vzhled
připomíná	připomínat	k5eAaImIp3nS	připomínat
barevnou	barevný	k2eAgFnSc4d1	barevná
zářivku	zářivka	k1gFnSc4	zářivka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
meč	meč	k1gInSc1	meč
tvoří	tvořit	k5eAaImIp3nS	tvořit
jen	jen	k9	jen
asi	asi	k9	asi
20	[number]	k4	20
cm	cm	kA	cm
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
kovový	kovový	k2eAgInSc1d1	kovový
jílec	jílec	k1gInSc1	jílec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
zařízení	zařízení	k1gNnSc4	zařízení
emitující	emitující	k2eAgInSc1d1	emitující
energetický	energetický	k2eAgInSc1d1	energetický
paprsek	paprsek	k1gInSc1	paprsek
čepele	čepel	k1gInSc2	čepel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Světelný	světelný	k2eAgInSc1d1	světelný
meč	meč	k1gInSc1	meč
je	být	k5eAaImIp3nS	být
původním	původní	k2eAgInSc7d1	původní
vynálezem	vynález	k1gInSc7	vynález
<g/>
,	,	kIx,	,
zbraní	zbraň	k1gFnSc7	zbraň
a	a	k8xC	a
symbolem	symbol	k1gInSc7	symbol
rytířů	rytíř	k1gMnPc2	rytíř
řádu	řád	k1gInSc2	řád
Jedi	Jede	k1gFnSc4	Jede
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
převzali	převzít	k5eAaPmAgMnP	převzít
ho	on	k3xPp3gInSc4	on
od	od	k7c2	od
nich	on	k3xPp3gInPc2	on
i	i	k9	i
jejich	jejich	k3xOp3gMnPc1	jejich
protivníci	protivník	k1gMnPc1	protivník
<g/>
,	,	kIx,	,
Sithové	Sith	k1gMnPc1	Sith
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
používají	používat	k5eAaImIp3nP	používat
meče	meč	k1gInPc4	meč
s	s	k7c7	s
rudou	rudý	k2eAgFnSc7d1	rudá
korónou	koróna	k1gFnSc7	koróna
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
také	také	k9	také
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
pec	pec	k1gFnSc4	pec
pro	pro	k7c4	pro
vyrábění	vyrábění	k1gNnSc4	vyrábění
syntetických	syntetický	k2eAgInPc2d1	syntetický
krystalů	krystal	k1gInPc2	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
si	se	k3xPyFc3	se
svůj	svůj	k3xOyFgInSc4	svůj
meč	meč	k1gInSc4	meč
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
každý	každý	k3xTgMnSc1	každý
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Barvy	barva	k1gFnSc2	barva
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
trilogii	trilogie	k1gFnSc6	trilogie
existovaly	existovat	k5eAaImAgInP	existovat
světelné	světelný	k2eAgInPc1d1	světelný
meče	meč	k1gInPc1	meč
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
barevných	barevný	k2eAgFnPc6d1	barevná
variantách	varianta	k1gFnPc6	varianta
<g/>
:	:	kIx,	:
červené	červený	k2eAgFnPc1d1	červená
<g/>
,	,	kIx,	,
modré	modrý	k2eAgFnPc1d1	modrá
<g/>
,	,	kIx,	,
fialové	fialový	k2eAgFnPc1d1	fialová
a	a	k8xC	a
zelené	zelený	k2eAgFnPc1d1	zelená
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
v	v	k7c6	v
PC	PC	kA	PC
hře	hra	k1gFnSc6	hra
Jedi	Jedi	k1gNnSc1	Jedi
Knight	Knight	k2eAgMnSc1d1	Knight
<g/>
:	:	kIx,	:
Dark	Dark	k1gMnSc1	Dark
Forces	Forces	k1gMnSc1	Forces
a	a	k8xC	a
pak	pak	k6eAd1	pak
i	i	k9	i
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
filmové	filmový	k2eAgFnSc6d1	filmová
trilogii	trilogie	k1gFnSc6	trilogie
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
hrách	hra	k1gFnPc6	hra
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
barevných	barevný	k2eAgFnPc2d1	barevná
variant	varianta	k1gFnPc2	varianta
(	(	kIx(	(
<g/>
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
,	,	kIx,	,
oranžová	oranžový	k2eAgFnSc1d1	oranžová
<g/>
,	,	kIx,	,
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
zlatá	zlatý	k2eAgFnSc1d1	zlatá
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Barvu	barva	k1gFnSc4	barva
meče	meč	k1gInSc2	meč
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
agamarské	agamarský	k2eAgInPc1d1	agamarský
krystaly	krystal	k1gInPc1	krystal
usazené	usazený	k2eAgInPc1d1	usazený
uvnitř	uvnitř	k7c2	uvnitř
rukojeti	rukojeť	k1gFnSc2	rukojeť
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
usměrňují	usměrňovat	k5eAaImIp3nP	usměrňovat
energii	energie	k1gFnSc4	energie
emitovanou	emitovaný	k2eAgFnSc4d1	emitovaná
uvnitř	uvnitř	k7c2	uvnitř
meče	meč	k1gInSc2	meč
do	do	k7c2	do
čepele	čepel	k1gFnSc2	čepel
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
do	do	k7c2	do
1	[number]	k4	1
m.	m.	k?	m.
Tyto	tento	k3xDgInPc1	tento
krystaly	krystal	k1gInPc1	krystal
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
světech	svět	k1gInPc6	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jde	jít	k5eAaImIp3nS	jít
je	on	k3xPp3gInPc4	on
i	i	k9	i
velmi	velmi	k6eAd1	velmi
složitým	složitý	k2eAgInSc7d1	složitý
způsobem	způsob	k1gInSc7	způsob
vytvářet	vytvářet	k5eAaImF	vytvářet
synteticky	synteticky	k6eAd1	synteticky
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Sithští	Sithský	k2eAgMnPc1d1	Sithský
lordi	lord	k1gMnPc1	lord
si	se	k3xPyFc3	se
své	svůj	k3xOyFgInPc4	svůj
červené	červený	k2eAgInPc4d1	červený
krystaly	krystal	k1gInPc4	krystal
v	v	k7c6	v
sithských	sithský	k2eAgInPc6d1	sithský
mečích	meč	k1gInPc6	meč
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
většinou	většina	k1gFnSc7	většina
uměle	uměle	k6eAd1	uměle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Provedení	provedení	k1gNnSc1	provedení
==	==	k?	==
</s>
</p>
<p>
<s>
Běžnou	běžný	k2eAgFnSc7d1	běžná
verzí	verze	k1gFnSc7	verze
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
obyčejný	obyčejný	k2eAgInSc1d1	obyčejný
<g/>
"	"	kIx"	"
meč	meč	k1gInSc1	meč
s	s	k7c7	s
čepelí	čepel	k1gFnSc7	čepel
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
konci	konec	k1gInSc6	konec
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
historie	historie	k1gFnSc2	historie
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
prvních	první	k4xOgInPc2	první
světelných	světelný	k2eAgInPc2d1	světelný
mečů	meč	k1gInPc2	meč
však	však	k9	však
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
mnoho	mnoho	k4c1	mnoho
různých	různý	k2eAgFnPc2d1	různá
modifikací	modifikace	k1gFnPc2	modifikace
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
vzácně	vzácně	k6eAd1	vzácně
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgFnSc1d1	vyskytující
varianta	varianta	k1gFnSc1	varianta
(	(	kIx(	(
<g/>
používána	používán	k2eAgFnSc1d1	používána
většinou	většina	k1gFnSc7	většina
jen	jen	k8xS	jen
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
ruky	ruka	k1gFnSc2	ruka
<g/>
)	)	kIx)	)
u	u	k7c2	u
které	který	k3yQgFnSc2	který
je	být	k5eAaImIp3nS	být
rukojeť	rukojeť	k1gFnSc1	rukojeť
mírně	mírně	k6eAd1	mírně
zahnutá	zahnutý	k2eAgFnSc1d1	zahnutá
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
pohodlně	pohodlně	k6eAd1	pohodlně
drží	držet	k5eAaImIp3nS	držet
a	a	k8xC	a
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
provádět	provádět	k5eAaImF	provádět
velmi	velmi	k6eAd1	velmi
efektní	efektní	k2eAgInPc4d1	efektní
triky	trik	k1gInPc4	trik
a	a	k8xC	a
výpady	výpad	k1gInPc4	výpad
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
typ	typ	k1gInSc4	typ
meče	meč	k1gInSc2	meč
používal	používat	k5eAaImAgMnS	používat
např.	např.	kA	např.
Darth	Darth	k1gMnSc1	Darth
Tyranus	Tyranus	k1gMnSc1	Tyranus
<g/>
,	,	kIx,	,
alias	alias	k9	alias
Hrabě	Hrabě	k1gMnSc4	Hrabě
Dooku	Dooka	k1gMnSc4	Dooka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gFnSc1	jeho
učednice	učednice	k1gFnSc1	učednice
Asajj	Asajj	k1gMnSc1	Asajj
Ventress	Ventress	k1gInSc1	Ventress
<g/>
.	.	kIx.	.
</s>
<s>
Ventress	Ventress	k6eAd1	Ventress
používala	používat	k5eAaImAgFnS	používat
hned	hned	k6eAd1	hned
dva	dva	k4xCgInPc1	dva
takovéto	takovýto	k3xDgInPc1	takovýto
meče	meč	k1gInPc1	meč
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
si	se	k3xPyFc3	se
je	on	k3xPp3gMnPc4	on
mohla	moct	k5eAaImAgFnS	moct
spojit	spojit	k5eAaPmF	spojit
dohromady	dohromady	k6eAd1	dohromady
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
tak	tak	k6eAd1	tak
poslední	poslední	k2eAgFnSc4d1	poslední
variantu	varianta	k1gFnSc4	varianta
-	-	kIx~	-
oboustranný	oboustranný	k2eAgInSc1d1	oboustranný
meč	meč	k1gInSc1	meč
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
zahnutým	zahnutý	k2eAgFnPc3d1	zahnutá
rukojetím	rukojeť	k1gFnPc3	rukojeť
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
celková	celkový	k2eAgFnSc1d1	celková
spojená	spojený	k2eAgFnSc1d1	spojená
rukojeť	rukojeť	k1gFnSc1	rukojeť
mírně	mírně	k6eAd1	mírně
zakroucená	zakroucený	k2eAgFnSc1d1	zakroucená
<g/>
,	,	kIx,	,
kopírující	kopírující	k2eAgInSc1d1	kopírující
tvar	tvar	k1gInSc1	tvar
~	~	kIx~	~
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
se	se	k3xPyFc4	se
dostáváme	dostávat	k5eAaImIp1nP	dostávat
k	k	k7c3	k
další	další	k2eAgInPc4d1	další
-	-	kIx~	-
poměrně	poměrně	k6eAd1	poměrně
vzácné	vzácný	k2eAgFnSc6d1	vzácná
modifikaci	modifikace	k1gFnSc6	modifikace
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tou	ten	k3xDgFnSc7	ten
je	být	k5eAaImIp3nS	být
oboustranný	oboustranný	k2eAgInSc1d1	oboustranný
světelný	světelný	k2eAgInSc1d1	světelný
meč	meč	k1gInSc1	meč
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
emituje	emitovat	k5eAaBmIp3nS	emitovat
hned	hned	k6eAd1	hned
dvě	dva	k4xCgFnPc1	dva
čepele	čepel	k1gFnPc1	čepel
-	-	kIx~	-
každou	každý	k3xTgFnSc4	každý
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
konci	konec	k1gInSc6	konec
rukojeti	rukojeť	k1gFnSc2	rukojeť
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
modifikací	modifikace	k1gFnSc7	modifikace
přišel	přijít	k5eAaPmAgInS	přijít
mocný	mocný	k2eAgMnSc1d1	mocný
Sithský	Sithský	k1gMnSc1	Sithský
lord	lord	k1gMnSc1	lord
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
Jedi	Jed	k1gFnSc2	Jed
jménem	jméno	k1gNnSc7	jméno
Exar	Exara	k1gFnPc2	Exara
Kun	kuna	k1gFnPc2	kuna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
oboustranný	oboustranný	k2eAgInSc1d1	oboustranný
meč	meč	k1gInSc1	meč
vešel	vejít	k5eAaPmAgInS	vejít
obecně	obecně	k6eAd1	obecně
ve	v	k7c4	v
známost	známost	k1gFnSc4	známost
jako	jako	k8xS	jako
sithská	sithskat	k5eAaPmIp3nS	sithskat
zbraň	zbraň	k1gFnSc4	zbraň
(	(	kIx(	(
<g/>
používal	používat	k5eAaImAgMnS	používat
ho	on	k3xPp3gNnSc4	on
i	i	k9	i
např.	např.	kA	např.
Darth	Darth	k1gInSc1	Darth
Maul	maul	k1gInSc1	maul
ve	v	k7c6	v
Skryté	skrytý	k2eAgFnSc6d1	skrytá
hrozbě	hrozba	k1gFnSc6	hrozba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
používali	používat	k5eAaImAgMnP	používat
ho	on	k3xPp3gNnSc4	on
i	i	k8xC	i
někteří	některý	k3yIgMnPc1	některý
rytíři	rytíř	k1gMnPc1	rytíř
Jedi	Jed	k1gFnSc2	Jed
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
variantou	varianta	k1gFnSc7	varianta
je	být	k5eAaImIp3nS	být
krátký	krátký	k2eAgInSc1d1	krátký
světelný	světelný	k2eAgInSc1d1	světelný
meč	meč	k1gInSc1	meč
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
nejznámějším	známý	k2eAgMnSc7d3	nejznámější
uživatelem	uživatel	k1gMnSc7	uživatel
byl	být	k5eAaImAgMnS	být
Freedon	Freedon	k1gMnSc1	Freedon
Nadd	Nadd	k1gMnSc1	Nadd
<g/>
.	.	kIx.	.
</s>
<s>
Světelný	světelný	k2eAgInSc1d1	světelný
meč	meč	k1gInSc1	meč
může	moct	k5eAaImIp3nS	moct
odrážet	odrážet	k5eAaImF	odrážet
střely	střela	k1gFnPc4	střela
z	z	k7c2	z
blasterů	blaster	k1gInPc2	blaster
a	a	k8xC	a
útoky	útok	k1gInPc4	útok
druhého	druhý	k4xOgInSc2	druhý
světelného	světelný	k2eAgInSc2d1	světelný
meče	meč	k1gInSc2	meč
<g/>
.	.	kIx.	.
</s>
<s>
Čepel	čepel	k1gFnSc1	čepel
projde	projít	k5eAaPmIp3nS	projít
vším	všecek	k3xTgNnSc7	všecek
kromě	kromě	k7c2	kromě
jiné	jiný	k2eAgFnSc2d1	jiná
čepele	čepel	k1gFnSc2	čepel
<g/>
,	,	kIx,	,
střely	střela	k1gFnSc2	střela
z	z	k7c2	z
blasteru	blaster	k1gInSc2	blaster
nebo	nebo	k8xC	nebo
několika	několik	k4yIc2	několik
vzácných	vzácný	k2eAgInPc2d1	vzácný
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
např.	např.	kA	např.
phriku	phrik	k1gInSc2	phrik
<g/>
,	,	kIx,	,
cortosisu	cortosis	k1gInSc2	cortosis
nebo	nebo	k8xC	nebo
mandalorianské	mandalorianský	k2eAgFnSc2d1	mandalorianský
oceli	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
je	být	k5eAaImIp3nS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
brnění	brnění	k1gNnSc1	brnění
nájemného	nájemný	k2eAgMnSc2d1	nájemný
lovce	lovec	k1gMnSc2	lovec
Jango	Jango	k1gMnSc1	Jango
Fetta	Fetta	k1gMnSc1	Fetta
(	(	kIx(	(
později	pozdě	k6eAd2	pozdě
ho	on	k3xPp3gInSc4	on
zdědil	zdědit	k5eAaPmAgMnS	zdědit
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Boba	Bob	k1gMnSc2	Bob
Fett	Fett	k1gMnSc1	Fett
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmé	sedmý	k4xOgFnSc6	sedmý
epizodě	epizoda	k1gFnSc6	epizoda
Star	star	k1gFnSc2	star
Wars	Wars	k1gInSc1	Wars
<g/>
:	:	kIx,	:
Síla	síla	k1gFnSc1	síla
se	se	k3xPyFc4	se
probouzí	probouzet	k5eAaImIp3nS	probouzet
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
s	s	k7c7	s
další	další	k2eAgFnSc7d1	další
verzí	verze	k1gFnSc7	verze
světelného	světelný	k2eAgInSc2d1	světelný
meče	meč	k1gInSc2	meč
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nejvíce	hodně	k6eAd3	hodně
podobá	podobat	k5eAaImIp3nS	podobat
středověkým	středověký	k2eAgInPc3d1	středověký
mečům	meč	k1gInPc3	meč
<g/>
.	.	kIx.	.
</s>
<s>
Meč	meč	k1gInSc1	meč
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
rukojeti	rukojeť	k1gFnSc2	rukojeť
a	a	k8xC	a
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vycházející	vycházející	k2eAgFnSc2d1	vycházející
směrem	směr	k1gInSc7	směr
vzhůru	vzhůru	k6eAd1	vzhůru
čepele	čepel	k1gFnSc2	čepel
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
dvou	dva	k4xCgInPc2	dva
paprsků	paprsek	k1gInPc2	paprsek
namísto	namísto	k7c2	namísto
křížové	křížový	k2eAgFnSc2d1	křížová
záštity	záštita	k1gFnSc2	záštita
<g/>
.	.	kIx.	.
</s>
<s>
Tvarem	tvar	k1gInSc7	tvar
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
nejvíce	nejvíce	k6eAd1	nejvíce
podobá	podobat	k5eAaImIp3nS	podobat
obrácenému	obrácený	k2eAgInSc3d1	obrácený
kříži	kříž	k1gInSc3	kříž
(	(	kIx(	(
<g/>
†	†	k?	†
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
meč	meč	k1gInSc1	meč
si	se	k3xPyFc3	se
sám	sám	k3xTgMnSc1	sám
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
a	a	k8xC	a
používal	používat	k5eAaImAgMnS	používat
vnuk	vnuk	k1gMnSc1	vnuk
Darth	Darth	k1gMnSc1	Darth
Vadera	Vadero	k1gNnSc2	Vadero
Kylo	Kylo	k1gMnSc1	Kylo
Ren	Ren	k1gMnSc1	Ren
<g/>
,	,	kIx,	,
svým	svůj	k3xOyFgNnSc7	svůj
pravým	pravý	k2eAgNnSc7d1	pravé
jménem	jméno	k1gNnSc7	jméno
Ben	Ben	k1gInSc1	Ben
Solo	Solo	k1gMnSc1	Solo
(	(	kIx(	(
<g/>
potomek	potomek	k1gMnSc1	potomek
Hana	Hana	k1gFnSc1	Hana
Sola	Solus	k1gMnSc2	Solus
a	a	k8xC	a
princezny	princezna	k1gFnSc2	princezna
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
generálky	generálka	k1gFnSc2	generálka
Leiy	Leia	k1gFnSc2	Leia
<g/>
)	)	kIx)	)
v	v	k7c6	v
již	již	k6eAd1	již
zmíněné	zmíněný	k2eAgFnSc6d1	zmíněná
sedmé	sedmý	k4xOgFnSc6	sedmý
epizodě	epizoda	k1gFnSc6	epizoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Světelný	světelný	k2eAgInSc4d1	světelný
meč	meč	k1gInSc4	meč
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
