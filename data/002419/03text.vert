<s>
Mrtvola	mrtvola	k1gFnSc1	mrtvola
je	být	k5eAaImIp3nS	být
tělo	tělo	k1gNnSc4	tělo
zemřelého	zemřelý	k2eAgInSc2d1	zemřelý
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Mrtvola	mrtvola	k1gFnSc1	mrtvola
zvířete	zvíře	k1gNnSc2	zvíře
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
zdechlina	zdechlina	k1gFnSc1	zdechlina
nebo	nebo	k8xC	nebo
mršina	mršina	k1gFnSc1	mršina
<g/>
.	.	kIx.	.
</s>
<s>
Odborně	odborně	k6eAd1	odborně
se	se	k3xPyFc4	se
lidská	lidský	k2eAgFnSc1d1	lidská
i	i	k8xC	i
zvířecí	zvířecí	k2eAgFnSc1d1	zvířecí
mrtvola	mrtvola	k1gFnSc1	mrtvola
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
kadáver	kadáver	k1gInSc1	kadáver
(	(	kIx(	(
<g/>
zastarale	zastarale	k6eAd1	zastarale
kadaver	kadaver	k1gMnSc1	kadaver
nebo	nebo	k8xC	nebo
kadávr	kadávr	k?	kadávr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mrtvá	mrtvý	k2eAgNnPc4d1	mrtvé
těla	tělo	k1gNnPc4	tělo
je	být	k5eAaImIp3nS	být
také	také	k9	také
možno	možno	k6eAd1	možno
pitvat	pitvat	k5eAaImF	pitvat
(	(	kIx(	(
<g/>
obdukce	obdukce	k1gFnSc1	obdukce
<g/>
,	,	kIx,	,
sekce	sekce	k1gFnSc1	sekce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Věda	věda	k1gFnSc1	věda
tímto	tento	k3xDgInSc7	tento
se	se	k3xPyFc4	se
zabývající	zabývající	k2eAgMnPc4d1	zabývající
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
patologie	patologie	k1gFnSc2	patologie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
pitvání	pitvání	k1gNnSc1	pitvání
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
zkoumání	zkoumání	k1gNnSc1	zkoumání
chirurgicky	chirurgicky	k6eAd1	chirurgicky
otevřeného	otevřený	k2eAgNnSc2d1	otevřené
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
např.	např.	kA	např.
v	v	k7c6	v
kriminalistice	kriminalistika	k1gFnSc6	kriminalistika
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
pro	pro	k7c4	pro
určování	určování	k1gNnSc4	určování
příčiny	příčina	k1gFnSc2	příčina
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
je	být	k5eAaImIp3nS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
u	u	k7c2	u
nově	nově	k6eAd1	nově
objeveného	objevený	k2eAgInSc2d1	objevený
živočišného	živočišný	k2eAgInSc2d1	živočišný
druhu	druh	k1gInSc2	druh
při	při	k7c6	při
zkoumání	zkoumání	k1gNnSc6	zkoumání
jeho	jeho	k3xOp3gFnSc2	jeho
anatomie	anatomie	k1gFnSc2	anatomie
<g/>
.	.	kIx.	.
</s>
<s>
Pitvu	pitva	k1gFnSc4	pitva
lze	lze	k6eAd1	lze
provádět	provádět	k5eAaImF	provádět
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
prokazatelně	prokazatelně	k6eAd1	prokazatelně
mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
<g/>
.	.	kIx.	.
</s>
<s>
Opačný	opačný	k2eAgInSc1d1	opačný
případ	případ	k1gInSc1	případ
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
vivisekce	vivisekce	k1gFnSc1	vivisekce
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklou	obvyklý	k2eAgFnSc7d1	obvyklá
vlastností	vlastnost	k1gFnSc7	vlastnost
mrtvoly	mrtvola	k1gFnSc2	mrtvola
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
časem	časem	k6eAd1	časem
rozkládat	rozkládat	k5eAaImF	rozkládat
<g/>
.	.	kIx.	.
</s>
<s>
Stupeň	stupeň	k1gInSc1	stupeň
rozkladu	rozklad	k1gInSc2	rozklad
závisí	záviset	k5eAaImIp3nS	záviset
především	především	k9	především
na	na	k7c4	na
stáří	stáří	k1gNnSc4	stáří
mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
mu	on	k3xPp3gNnSc3	on
nelze	lze	k6eNd1	lze
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
jej	on	k3xPp3gNnSc4	on
velmi	velmi	k6eAd1	velmi
podstatně	podstatně	k6eAd1	podstatně
zpomalit	zpomalit	k5eAaPmF	zpomalit
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
konzervací	konzervace	k1gFnPc2	konzervace
v	v	k7c6	v
ledu	led	k1gInSc2	led
či	či	k8xC	či
mumifikací	mumifikace	k1gFnPc2	mumifikace
<g/>
.	.	kIx.	.
</s>
<s>
Rozklad	rozklad	k1gInSc1	rozklad
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
dekompozici	dekompozice	k1gFnSc3	dekompozice
<g/>
,	,	kIx,	,
studují	studovat	k5eAaImIp3nP	studovat
forenzní	forenzní	k2eAgMnPc1d1	forenzní
antropologové	antropolog	k1gMnPc1	antropolog
a	a	k8xC	a
entomologové	entomolog	k1gMnPc1	entomolog
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
na	na	k7c6	na
takzvaných	takzvaný	k2eAgFnPc6d1	takzvaná
farmách	farma	k1gFnPc6	farma
mrtvol	mrtvola	k1gFnPc2	mrtvola
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
hřbitovy	hřbitov	k1gInPc1	hřbitov
udávají	udávat	k5eAaImIp3nP	udávat
tzv.	tzv.	kA	tzv.
tlecí	tlecí	k2eAgFnSc4d1	tlecí
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
po	po	k7c4	po
kterou	který	k3yQgFnSc4	který
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
hrob	hrob	k1gInSc1	hrob
otevřen	otevřít	k5eAaPmNgInS	otevřít
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
dalšího	další	k2eAgInSc2d1	další
pohřbu	pohřeb	k1gInSc2	pohřeb
nebo	nebo	k8xC	nebo
exhumace	exhumace	k1gFnSc2	exhumace
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Zombie	Zombie	k1gFnSc2	Zombie
<g/>
.	.	kIx.	.
</s>
<s>
Skutečná	skutečný	k2eAgFnSc1d1	skutečná
mrtvola	mrtvola	k1gFnSc1	mrtvola
nejeví	jevit	k5eNaImIp3nS	jevit
žádné	žádný	k3yNgFnPc4	žádný
známky	známka	k1gFnPc4	známka
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
nedýchá	dýchat	k5eNaImIp3nS	dýchat
<g/>
,	,	kIx,	,
nepohybuje	pohybovat	k5eNaImIp3nS	pohybovat
se	se	k3xPyFc4	se
a	a	k8xC	a
tak	tak	k6eAd1	tak
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
africké	africký	k2eAgFnSc6d1	africká
mytologii	mytologie	k1gFnSc6	mytologie
se	se	k3xPyFc4	se
však	však	k9	však
prvně	prvně	k?	prvně
objevila	objevit	k5eAaPmAgFnS	objevit
myšlenka	myšlenka	k1gFnSc1	myšlenka
ovládaného	ovládaný	k2eAgNnSc2d1	ovládané
a	a	k8xC	a
bezduchého	bezduchý	k2eAgNnSc2d1	bezduché
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
zombi	zomb	k1gInPc7	zomb
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pojem	pojem	k1gInSc1	pojem
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
přejat	přejmout	k5eAaPmNgInS	přejmout
jako	jako	k9	jako
výraz	výraz	k1gInSc4	výraz
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
oživlou	oživlý	k2eAgFnSc4d1	oživlá
mrtvolu	mrtvola	k1gFnSc4	mrtvola
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
