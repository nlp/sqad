<s>
Staré	Staré	k2eAgFnPc1d1
Ždánice	Ždánice	k1gFnPc1
</s>
<s>
Staré	Staré	k2eAgFnPc1d1
Ždánice	Ždánice	k1gFnPc1
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Václava	Václav	k1gMnSc2
</s>
<s>
znakvlajka	znakvlajka	k1gFnSc1
Lokalita	lokalita	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
obec	obec	k1gFnSc1
LAU	LAU	kA
2	#num#	k4
(	(	kIx(
<g/>
obec	obec	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
CZ0532	CZ0532	k4
575721	#num#	k4
Pověřená	pověřený	k2eAgFnSc1d1
obec	obec	k1gFnSc1
</s>
<s>
Lázně	lázeň	k1gFnPc1
Bohdaneč	Bohdaneč	k1gInSc1
Obec	obec	k1gFnSc1
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
</s>
<s>
Pardubice	Pardubice	k1gInPc1
Okres	okres	k1gInSc1
(	(	kIx(
<g/>
LAU	LAU	kA
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pardubice	Pardubice	k1gInPc1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
532	#num#	k4
<g/>
)	)	kIx)
Kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
NUTS	NUTS	kA
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pardubický	pardubický	k2eAgMnSc1d1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
53	#num#	k4
<g/>
)	)	kIx)
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Čechy	Čechy	k1gFnPc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
7	#num#	k4
<g/>
′	′	k?
<g/>
26	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
43	#num#	k4
<g/>
′	′	k?
<g/>
15	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
671	#num#	k4
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rozloha	rozloha	k1gFnSc1
</s>
<s>
5,73	5,73	k4
km²	km²	k?
Katastrální	katastrální	k2eAgNnSc4d1
území	území	k1gNnSc4
</s>
<s>
Staré	Staré	k2eAgFnPc4d1
Ždánice	Ždánice	k1gFnPc4
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
224	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
PSČ	PSČ	kA
</s>
<s>
533	#num#	k4
44	#num#	k4
Počet	počet	k1gInSc1
částí	část	k1gFnPc2
obce	obec	k1gFnSc2
</s>
<s>
1	#num#	k4
Počet	počet	k1gInSc1
k.	k.	k?
ú.	ú.	k?
</s>
<s>
1	#num#	k4
Počet	počet	k1gInSc1
ZSJ	ZSJ	kA
</s>
<s>
1	#num#	k4
Kontakt	kontakt	k1gInSc1
Adresa	adresa	k1gFnSc1
obecního	obecní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
</s>
<s>
Staré	Staré	k2eAgFnPc1d1
Ždánice	Ždánice	k1gFnPc1
1153344	#num#	k4
Staré	Staré	k2eAgFnPc1d1
Ždánice	Ždánice	k1gFnPc1
obeczdanice@iol.cz	obeczdanice@iol.cz	k1gMnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Martin	Martin	k1gMnSc1
Korunka	korunka	k1gFnSc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
:	:	kIx,
www.starezdanice.cz	www.starezdanice.cz	k1gInSc1
</s>
<s>
Staré	Staré	k2eAgFnPc1d1
Ždánice	Ždánice	k1gFnPc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Kód	kód	k1gInSc4
části	část	k1gFnPc1
obce	obec	k1gFnSc2
</s>
<s>
154784	#num#	k4
Geodata	Geodata	k1gFnSc1
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Obec	obec	k1gFnSc1
Staré	Staré	k2eAgFnPc1d1
Ždánice	Ždánice	k1gFnPc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nP
v	v	k7c6
okrese	okres	k1gInSc6
Pardubice	Pardubice	k1gInPc1
<g/>
,	,	kIx,
kraj	kraj	k1gInSc1
Pardubický	pardubický	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
671	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Ždánice	Ždánice	k1gFnPc1
byly	být	k5eAaImAgFnP
rodným	rodný	k2eAgNnSc7d1
sídlem	sídlo	k1gNnSc7
vladyků	vladyka	k1gMnPc2
ze	z	k7c2
Ždánic	Ždánice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
vladykové	vladyka	k1gMnPc1
zde	zde	k6eAd1
žili	žít	k5eAaImAgMnP
na	na	k7c6
nevelké	velký	k2eNgFnSc6d1
tvrzi	tvrz	k1gFnSc6
<g/>
,	,	kIx,
ze	z	k7c2
které	který	k3yQgFnSc2,k3yRgFnSc2,k3yIgFnSc2
se	se	k3xPyFc4
dochovaly	dochovat	k5eAaPmAgInP
jen	jen	k9
terénní	terénní	k2eAgInPc1d1
náznaky	náznak	k1gInPc1
na	na	k7c6
okraji	okraj	k1gInSc6
vsi	ves	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejstarším	starý	k2eAgMnSc7d3
z	z	k7c2
tohoto	tento	k3xDgInSc2
rodu	rod	k1gInSc2
byl	být	k5eAaImAgMnS
Vaněk	Vaněk	k1gMnSc1
ze	z	k7c2
Ždánic	Ždánice	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
roku	rok	k1gInSc2
1339	#num#	k4
prodal	prodat	k5eAaPmAgMnS
obec	obec	k1gFnSc4
s	s	k7c7
kostelem	kostel	k1gInSc7
Hroznatovi	Hroznatův	k2eAgMnPc1d1
<g/>
,	,	kIx,
opatovi	opat	k1gMnSc3
opatovického	opatovický	k2eAgInSc2d1
kláštera	klášter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ždánický	ždánický	k2eAgInSc1d1
kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Václava	Václav	k1gMnSc2
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1358	#num#	k4
uváděn	uvádět	k5eAaImNgInS
jako	jako	k8xC,k8xS
farní	farní	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klášteru	klášter	k1gInSc3
patřily	patřit	k5eAaImAgFnP
Ždánice	Ždánice	k1gFnPc4
až	až	k9
do	do	k7c2
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
zničení	zničení	k1gNnSc2
Husity	husita	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
husitských	husitský	k2eAgFnPc6d1
válkách	válka	k1gFnPc6
bylo	být	k5eAaImAgNnS
držení	držení	k1gNnSc1
vsi	ves	k1gFnSc2
zapsáno	zapsat	k5eAaPmNgNnS
Diviši	Diviš	k1gMnSc6
Bořkovi	Bořek	k1gMnSc6
z	z	k7c2
Miletínka	Miletínko	k1gNnSc2
<g/>
,	,	kIx,
pánovi	pánův	k2eAgMnPc1d1
na	na	k7c6
Kunětické	kunětický	k2eAgFnSc6d1
hoře	hora	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
byla	být	k5eAaImAgFnS
obec	obec	k1gFnSc1
v	v	k7c6
majetku	majetek	k1gInSc6
různých	různý	k2eAgFnPc2d1
drobných	drobná	k1gFnPc2
šlechticů	šlechtic	k1gMnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
si	se	k3xPyFc3
i	i	k9
často	často	k6eAd1
volili	volit	k5eAaImAgMnP
Ždánice	Ždánice	k1gFnPc4
za	za	k7c4
svůj	svůj	k3xOyFgInSc4
přídomek	přídomek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejvýznamnější	významný	k2eAgNnSc4d3
patří	patřit	k5eAaImIp3nS
královéhradecký	královéhradecký	k2eAgMnSc1d1
měšťan	měšťan	k1gMnSc1
Havel	Havel	k1gMnSc1
ze	z	k7c2
Ždánic	Ždánice	k1gFnPc2
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
ze	z	k7c2
Ždánic	Ždánice	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
podepsal	podepsat	k5eAaPmAgMnS
protest	protest	k1gInSc4
proti	proti	k7c3
upálení	upálení	k1gNnSc3
Jana	Jan	k1gMnSc2
Husa	Hus	k1gMnSc2
roku	rok	k1gInSc2
1415	#num#	k4
a	a	k8xC
Jiří	Jiří	k1gMnSc1
Vániš	Vániš	k1gMnSc1
Ždánický	ždánický	k2eAgMnSc1d1
<g/>
,	,	kIx,
pardubický	pardubický	k2eAgMnSc1d1
hejtman	hejtman	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1540	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začátkem	začátkem	k7c2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
ves	ves	k1gFnSc1
dostala	dostat	k5eAaPmAgFnS
do	do	k7c2
majetku	majetek	k1gInSc2
Viléma	Vilém	k1gMnSc2
z	z	k7c2
Pernštejna	Pernštejn	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
ji	on	k3xPp3gFnSc4
začlenil	začlenit	k5eAaPmAgMnS
do	do	k7c2
svého	svůj	k3xOyFgNnSc2
pardubického	pardubický	k2eAgNnSc2d1
panství	panství	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
čítalo	čítat	k5eAaImAgNnS
šest	šest	k4xCc1
městeček	městečko	k1gNnPc2
a	a	k8xC
na	na	k7c4
sto	sto	k4xCgNnSc4
padesát	padesát	k4xCc4
vesnic	vesnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
Ždánice	Ždánice	k1gFnPc4
a	a	k8xC
celé	celý	k2eAgNnSc4d1
panství	panství	k1gNnSc4
zdědil	zdědit	k5eAaPmAgMnS
Vojtěch	Vojtěch	k1gMnSc1
z	z	k7c2
Pernštejna	Pernštejn	k1gInSc2
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgMnSc1d3
hofmistr	hofmistr	k1gMnSc1
království	království	k1gNnSc2
českého	český	k2eAgNnSc2d1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
panství	panství	k1gNnSc4
ještě	ještě	k9
zvelebil	zvelebit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vybudován	vybudován	k2eAgInSc1d1
byl	být	k5eAaImAgInS
opatovický	opatovický	k2eAgInSc1d1
kanál	kanál	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
Ždánicemi	Ždánice	k1gFnPc7
protéká	protékat	k5eAaImIp3nS
a	a	k8xC
postaven	postavit	k5eAaPmNgInS
ždánický	ždánický	k2eAgInSc1d1
panský	panský	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
se	se	k3xPyFc4
ještě	ještě	k9
v	v	k7c6
držení	držení	k1gNnSc6
panství	panství	k1gNnSc2
vystřídali	vystřídat	k5eAaPmAgMnP
Jan	Jan	k1gMnSc1
a	a	k8xC
posléze	posléze	k6eAd1
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaroslav	Jaroslav	k1gMnSc1
<g/>
,	,	kIx,
poslední	poslední	k2eAgFnSc6d1
z	z	k7c2
rodu	rod	k1gInSc2
Pernštejnů	Pernštejn	k1gInPc2
<g/>
,	,	kIx,
panství	panství	k1gNnSc3
zadlužil	zadlužit	k5eAaPmAgMnS
a	a	k8xC
nakonec	nakonec	k6eAd1
jej	on	k3xPp3gMnSc4
prodal	prodat	k5eAaPmAgMnS
českému	český	k2eAgMnSc3d1
králi	král	k1gMnSc3
Ferdinandovi	Ferdinand	k1gMnSc3
I.	I.	kA
Od	od	k7c2
roku	rok	k1gInSc2
1560	#num#	k4
tak	tak	k9
Ždánice	Ždánice	k1gFnPc1
a	a	k8xC
celé	celý	k2eAgNnSc1d1
pardubické	pardubický	k2eAgNnSc1d1
panství	panství	k1gNnSc1
patří	patřit	k5eAaImIp3nS
české	český	k2eAgFnSc3d1
královské	královský	k2eAgFnSc3d1
komoře	komora	k1gFnSc3
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byla	být	k5eAaImAgFnS
obec	obec	k1gFnSc1
postižena	postižen	k2eAgFnSc1d1
bídou	bída	k1gFnSc7
a	a	k8xC
drancováním	drancování	k1gNnSc7
během	během	k7c2
třicetileté	třicetiletý	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1680	#num#	k4
navštívil	navštívit	k5eAaPmAgMnS
své	svůj	k3xOyFgNnSc4
panství	panství	k1gNnSc4
císař	císař	k1gMnSc1
Leopold	Leopold	k1gMnSc1
I.	I.	kA
a	a	k8xC
v	v	k7c6
Pardubicích	Pardubice	k1gInPc6
podepsal	podepsat	k5eAaPmAgInS
robotní	robotní	k2eAgInSc4d1
patent	patent	k1gInSc4
zmírňující	zmírňující	k2eAgFnSc2d1
robotní	robotní	k2eAgFnSc2d1
povinnosti	povinnost	k1gFnSc2
svých	svůj	k3xOyFgMnPc2
poddaných	poddaný	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byly	být	k5eAaImAgFnP
vlivem	vlivem	k7c2
postupující	postupující	k2eAgFnSc2d1
germanizace	germanizace	k1gFnSc2
zakládány	zakládán	k2eAgFnPc4d1
nové	nový	k2eAgFnPc4d1
kolonizační	kolonizační	k2eAgFnPc4d1
vesnice	vesnice	k1gFnPc4
<g/>
,	,	kIx,
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nich	on	k3xPp3gMnPc2
jsou	být	k5eAaImIp3nP
i	i	k9
Nové	Nové	k2eAgFnPc1d1
Ždánice	Ždánice	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
však	však	k9
ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
spojily	spojit	k5eAaPmAgInP
se	s	k7c7
Starými	starý	k2eAgFnPc7d1
Ždánicemi	Ždánice	k1gFnPc7
v	v	k7c4
jednu	jeden	k4xCgFnSc4
obec	obec	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
také	také	k6eAd1
k	k	k7c3
částečné	částečný	k2eAgFnSc3d1
parcelaci	parcelace	k1gFnSc3
panských	panský	k2eAgInPc2d1
dvorů	dvůr	k1gInPc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1781	#num#	k4
ke	k	k7c3
zrušení	zrušení	k1gNnSc3
roboty	robota	k1gFnSc2
císařem	císař	k1gMnSc7
Josefem	Josef	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ždánice	Ždánice	k1gFnPc1
rovněž	rovněž	k9
fungovaly	fungovat	k5eAaImAgFnP
jako	jako	k9
samostatný	samostatný	k2eAgInSc4d1
panský	panský	k2eAgInSc4d1
dvůr	dvůr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Revolučním	revoluční	k2eAgInSc7d1
rokem	rok	k1gInSc7
1848	#num#	k4
byl	být	k5eAaImAgInS
způsoben	způsobit	k5eAaPmNgInS
hluboký	hluboký	k2eAgInSc1d1
zásah	zásah	k1gInSc1
do	do	k7c2
existence	existence	k1gFnSc2
panství	panství	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
tak	tak	k9
po	po	k7c4
čtvrt	čtvrt	k1gFnSc4
tisíciletí	tisíciletí	k1gNnSc2
v	v	k7c6
majetku	majetek	k1gInSc6
české	český	k2eAgFnSc2d1
komory	komora	k1gFnSc2
přešlo	přejít	k5eAaPmAgNnS
roku	rok	k1gInSc2
1856	#num#	k4
do	do	k7c2
držení	držení	k1gNnSc2
rakouské	rakouský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
banky	banka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1860	#num#	k4
přišel	přijít	k5eAaPmAgMnS
do	do	k7c2
Ždánic	Ždánice	k1gFnPc2
mlynářský	mlynářský	k2eAgInSc4d1
rod	rod	k1gInSc4
Prokešů	Prokeš	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
na	na	k7c6
spáleništi	spáleniště	k1gNnSc6
starého	starý	k2eAgInSc2d1
mlýna	mlýn	k1gInSc2
postavil	postavit	k5eAaPmAgInS
nový	nový	k2eAgInSc1d1
a	a	k8xC
největší	veliký	k2eAgInSc1d3
mlýn	mlýn	k1gInSc1
opatovického	opatovický	k2eAgInSc2d1
kanálu	kanál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obci	obec	k1gFnSc6
si	se	k3xPyFc3
roku	rok	k1gInSc2
1877	#num#	k4
postavili	postavit	k5eAaPmAgMnP
Prokešovi	Prokešův	k2eAgMnPc1d1
reprezentativní	reprezentativní	k2eAgFnSc4d1
vilu	vila	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1863	#num#	k4
koupil	koupit	k5eAaPmAgInS
pardubický	pardubický	k2eAgInSc1d1
velkostatek	velkostatek	k1gInSc1
v	v	k7c6
dražbě	dražba	k1gFnSc6
rakouský	rakouský	k2eAgInSc1d1
úvěrní	úvěrní	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
postupně	postupně	k6eAd1
rozprodal	rozprodat	k5eAaPmAgMnS
několik	několik	k4yIc4
vesnic	vesnice	k1gFnPc2
a	a	k8xC
pozemků	pozemek	k1gInPc2
různým	různý	k2eAgMnPc3d1
zájemcům	zájemce	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbytek	zbytek	k1gInSc1
velkostatku	velkostatek	k1gInSc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
Ždánice	Ždánice	k1gFnPc1
stále	stále	k6eAd1
patřily	patřit	k5eAaImAgFnP
<g/>
,	,	kIx,
koupil	koupit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1881	#num#	k4
vídeňský	vídeňský	k2eAgMnSc1d1
průmyslník	průmyslník	k1gMnSc1
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
baron	baron	k1gMnSc1
Drasche	Drasche	k1gFnSc1
z	z	k7c2
Wartinberga	Wartinberg	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hasičský	hasičský	k2eAgInSc1d1
sbor	sbor	k1gInSc1
byl	být	k5eAaImAgInS
založen	založen	k2eAgInSc1d1
roku	rok	k1gInSc2
1882	#num#	k4
<g/>
,	,	kIx,
Občanská	občanský	k2eAgFnSc1d1
záložna	záložna	k1gFnSc1
roku	rok	k1gInSc2
1893	#num#	k4
a	a	k8xC
poštovní	poštovní	k2eAgInSc4d1
úřad	úřad	k1gInSc4
roku	rok	k1gInSc2
1908	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zrušení	zrušení	k1gNnSc6
monarchie	monarchie	k1gFnSc2
byla	být	k5eAaImAgNnP
při	při	k7c6
pozemkové	pozemkový	k2eAgFnSc6d1
reformě	reforma	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
baronu	baron	k1gMnSc6
Draschemu	Draschem	k1gMnSc6
většina	většina	k1gFnSc1
majetku	majetek	k1gInSc2
zabrána	zabrána	k1gFnSc1
a	a	k8xC
Ždánice	Ždánice	k1gFnPc1
se	se	k3xPyFc4
staly	stát	k5eAaPmAgInP
samostatným	samostatný	k2eAgInSc7d1
statkem	statek	k1gInSc7
a	a	k8xC
pozemky	pozemek	k1gInPc1
připadly	připadnout	k5eAaPmAgInP
obci	obec	k1gFnSc3
a	a	k8xC
rozprodávány	rozprodáván	k2eAgInPc1d1
byly	být	k5eAaImAgFnP
soukromým	soukromý	k2eAgFnPc3d1
osobám	osoba	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
byla	být	k5eAaImAgNnP
zakládána	zakládán	k2eAgNnPc1d1
různá	různý	k2eAgNnPc1d1
družstva	družstvo	k1gNnPc1
<g/>
,	,	kIx,
ve	v	k7c6
Ždánicích	Ždánice	k1gFnPc6
například	například	k6eAd1
lihovarské	lihovarský	k2eAgFnPc1d1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
do	do	k7c2
svého	svůj	k3xOyFgInSc2
majetku	majetek	k1gInSc2
přebírala	přebírat	k5eAaImAgFnS
majetek	majetek	k1gInSc4
bývalého	bývalý	k2eAgInSc2d1
pardubického	pardubický	k2eAgInSc2d1
velkostatku	velkostatek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1
osobností	osobnost	k1gFnSc7
meziválečného	meziválečný	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
ve	v	k7c6
Ždánicích	Ždánice	k1gFnPc6
<g/>
,	,	kIx,
rolník	rolník	k1gMnSc1
a	a	k8xC
tehdejší	tehdejší	k2eAgMnSc1d1
poslanec	poslanec	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
Žilka	Žilka	k1gMnSc1
<g/>
,	,	kIx,
zastřelený	zastřelený	k2eAgInSc1d1
nacisty	nacista	k1gMnPc7
roku	rok	k1gInSc2
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
vlády	vláda	k1gFnSc2
KSČ	KSČ	kA
a	a	k8xC
kolektivizace	kolektivizace	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
znárodňován	znárodňován	k2eAgInSc1d1
majetek	majetek	k1gInSc1
sedlákům	sedlák	k1gMnPc3
a	a	k8xC
statkářům	statkář	k1gMnPc3
<g/>
,	,	kIx,
z	z	k7c2
obce	obec	k1gFnSc2
postupně	postupně	k6eAd1
zmizely	zmizet	k5eAaPmAgInP
statky	statek	k1gInPc1
a	a	k8xC
hospodářská	hospodářský	k2eAgNnPc1d1
stavení	stavení	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k6eAd1
si	se	k3xPyFc3
obec	obec	k1gFnSc1
zachovala	zachovat	k5eAaPmAgFnS
několik	několik	k4yIc4
zemědělských	zemědělský	k2eAgNnPc2d1
stavení	stavení	k1gNnPc2
a	a	k8xC
osobitý	osobitý	k2eAgInSc4d1
rolnický	rolnický	k2eAgInSc4d1
ráz	ráz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
Gotický	gotický	k2eAgInSc1d1
kostel	kostel	k1gInSc1
sv.	sv.	kA
Václava	Václav	k1gMnSc2
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Václava	Václav	k1gMnSc2
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Václava	Václav	k1gMnSc2
byl	být	k5eAaImAgInS
postaven	postaven	k2eAgInSc1d1
roku	rok	k1gInSc2
1339	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stojí	stát	k5eAaImIp3nS
na	na	k7c6
místě	místo	k1gNnSc6
původního	původní	k2eAgInSc2d1
románského	románský	k2eAgInSc2d1
dřevěného	dřevěný	k2eAgInSc2d1
kostelíku	kostelík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
času	čas	k1gInSc2
byl	být	k5eAaImAgInS
několikrát	několikrát	k6eAd1
stavebně	stavebně	k6eAd1
upravován	upravovat	k5eAaImNgInS
a	a	k8xC
opravován	opravovat	k5eAaImNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větší	veliký	k2eAgFnSc1d2
přestavba	přestavba	k1gFnSc1
kostela	kostel	k1gInSc2
proběhla	proběhnout	k5eAaPmAgFnS
v	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
polovině	polovina	k1gFnSc6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
další	další	k2eAgInPc1d1
raně	raně	k6eAd1
barokní	barokní	k2eAgInPc1d1
v	v	k7c4
1	#num#	k4
<g/>
.	.	kIx.
polovině	polovina	k1gFnSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Menší	malý	k2eAgFnPc4d2
opravy	oprava	k1gFnPc4
pak	pak	k6eAd1
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
počátkem	počátkem	k7c2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostel	kostel	k1gInSc1
je	být	k5eAaImIp3nS
dominantní	dominantní	k2eAgFnSc1d1
svou	svůj	k3xOyFgFnSc7
hranolovou	hranolový	k2eAgFnSc7d1
věží	věž	k1gFnSc7
zakončenou	zakončený	k2eAgFnSc7d1
cimbuřím	cimbuří	k1gNnSc7
a	a	k8xC
kamenným	kamenný	k2eAgInSc7d1
jehlanem	jehlan	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Ždánický	ždánický	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
</s>
<s>
Ždánický	ždánický	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
byl	být	k5eAaImAgInS
zbudován	zbudovat	k5eAaPmNgInS
hned	hned	k6eAd1
po	po	k7c6
výstavbě	výstavba	k1gFnSc6
opatovického	opatovický	k2eAgInSc2d1
kanálu	kanál	k1gInSc2
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
roku	rok	k1gInSc2
1515	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
jako	jako	k9
mlýn	mlýn	k1gInSc1
panský	panský	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mlýn	mlýn	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1548	#num#	k4
převzal	převzít	k5eAaPmAgMnS
do	do	k7c2
dědičného	dědičný	k2eAgInSc2d1
nájmu	nájem	k1gInSc2
první	první	k4xOgMnSc1
známý	známý	k2eAgMnSc1d1
mlynář	mlynář	k1gMnSc1
Matouš	Matouš	k1gMnSc1
Střebský	Střebský	k2eAgMnSc1d1
a	a	k8xC
po	po	k7c6
něm	on	k3xPp3gInSc6
jeho	jeho	k3xOp3gInSc6
další	další	k2eAgMnPc1d1
příbuzní	příbuzný	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1560	#num#	k4
byl	být	k5eAaImAgInS
mlýn	mlýn	k1gInSc1
prodán	prodat	k5eAaPmNgInS
vdovou	vdova	k1gFnSc7
Martou	Marta	k1gFnSc7
svému	svůj	k3xOyFgMnSc3
zeťovi	zeť	k1gMnSc3
Jakubovi	Jakub	k1gMnSc3
<g/>
,	,	kIx,
po	po	k7c6
něm	on	k3xPp3gMnSc6
jej	on	k3xPp3gMnSc4
v	v	k7c6
roce	rok	k1gInSc6
1607	#num#	k4
zdědil	zdědit	k5eAaPmAgMnS
syn	syn	k1gMnSc1
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1638	#num#	k4
patřil	patřit	k5eAaImAgMnS
mlýn	mlýn	k1gInSc4
mlynářce	mlynářka	k1gFnSc3
Anně	Anna	k1gFnSc3
Vilemovské	Vilemovská	k1gFnSc3
<g/>
,	,	kIx,
ke	k	k7c3
které	který	k3yQgFnSc3,k3yRgFnSc3,k3yIgFnSc3
se	se	k3xPyFc4
přiženil	přiženit	k5eAaPmAgMnS
Adam	Adam	k1gMnSc1
Kohout	Kohout	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
zbořen	zbořit	k5eAaPmNgMnS
Švédy	Švéd	k1gMnPc7
během	během	k7c2
třicetileté	třicetiletý	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1656	#num#	k4
mlýn	mlýn	k1gInSc1
vyhořel	vyhořet	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vypálený	vypálený	k2eAgInSc1d1
jej	on	k3xPp3gMnSc4
koupil	koupit	k5eAaPmAgMnS
majitel	majitel	k1gMnSc1
pardubického	pardubický	k2eAgNnSc2d1
panství	panství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
je	být	k5eAaImIp3nS
až	až	k9
z	z	k7c2
roku	rok	k1gInSc2
1733	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jej	on	k3xPp3gMnSc4
koupil	koupit	k5eAaPmAgMnS
Václav	Václav	k1gMnSc1
Chudoba	Chudoba	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
pochází	pocházet	k5eAaImIp3nS
i	i	k9
socha	socha	k1gFnSc1
sv.	sv.	kA
Nepomuka	Nepomuk	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
zbudována	zbudovat	k5eAaPmNgFnS
na	na	k7c4
náklady	náklad	k1gInPc4
mlynáře	mlynář	k1gMnSc2
Chudoby	Chudoba	k1gMnSc2
a	a	k8xC
stojí	stát	k5eAaImIp3nS
naproti	naproti	k7c3
bývalému	bývalý	k2eAgInSc3d1
mlýnu	mlýn	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1763	#num#	k4
koupil	koupit	k5eAaPmAgInS
mlýn	mlýn	k1gInSc1
v	v	k7c6
dražbě	dražba	k1gFnSc6
Tomáš	Tomáš	k1gMnSc1
Roudný	Roudný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
jej	on	k3xPp3gMnSc4
koupil	koupit	k5eAaPmAgMnS
Josef	Josef	k1gMnSc1
Roudný	Roudný	k2eAgMnSc1d1
za	za	k7c4
6000	#num#	k4
zlatých	zlatá	k1gFnPc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1796	#num#	k4
Josef	Josef	k1gMnSc1
Dostál	Dostál	k1gMnSc1
za	za	k7c4
18000	#num#	k4
zlatých	zlatá	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
jej	on	k3xPp3gMnSc4
v	v	k7c6
roce	rok	k1gInSc6
1802	#num#	k4
prodal	prodat	k5eAaPmAgInS
svému	svůj	k3xOyFgMnSc3
synovi	syn	k1gMnSc3
Františkovi	František	k1gMnSc3
Dostálovi	Dostál	k1gMnSc3
za	za	k7c2
22	#num#	k4
800	#num#	k4
zlatých	zlatá	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
jej	on	k3xPp3gInSc2
roku	rok	k1gInSc2
1810	#num#	k4
prodal	prodat	k5eAaPmAgMnS
Janu	Jana	k1gFnSc4
a	a	k8xC
Kateřině	Kateřina	k1gFnSc6
Morávkovým	Morávkův	k2eAgNnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mlýn	mlýn	k1gInSc1
opět	opět	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1860	#num#	k4
do	do	k7c2
základů	základ	k1gInPc2
vyhořel	vyhořet	k5eAaPmAgInS
a	a	k8xC
spáleniště	spáleniště	k1gNnSc4
koupil	koupit	k5eAaPmAgMnS
mlynář	mlynář	k1gMnSc1
Václav	Václav	k1gMnSc1
Prokeš	Prokeš	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
vybudoval	vybudovat	k5eAaPmAgMnS
mlýn	mlýn	k1gInSc4
nový	nový	k2eAgInSc4d1
<g/>
,	,	kIx,
již	již	k6eAd1
cihlový	cihlový	k2eAgInSc4d1
s	s	k7c7
pěti	pět	k4xCc7
koly	kolo	k1gNnPc7
a	a	k8xC
obytnými	obytný	k2eAgFnPc7d1
budovami	budova	k1gFnPc7
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
největší	veliký	k2eAgInSc1d3
mlýn	mlýn	k1gInSc1
na	na	k7c6
opatovickém	opatovický	k2eAgInSc6d1
kanálu	kanál	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mlýn	mlýn	k1gInSc1
pak	pak	k6eAd1
provozoval	provozovat	k5eAaImAgInS
jeho	jeho	k3xOp3gInSc1
rod	rod	k1gInSc1
po	po	k7c4
několik	několik	k4yIc4
generací	generace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1890	#num#	k4
přešel	přejít	k5eAaPmAgInS
mlýn	mlýn	k1gInSc1
na	na	k7c4
jeho	jeho	k3xOp3gMnSc4,k3xPp3gMnSc4
syna	syn	k1gMnSc4
Josefa	Josef	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1912	#num#	k4
mlýn	mlýn	k1gInSc1
opět	opět	k6eAd1
vyhořel	vyhořet	k5eAaPmAgInS
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
proto	proto	k8xC
přestavěn	přestavěn	k2eAgInSc1d1
a	a	k8xC
zmodernizován	zmodernizován	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1923	#num#	k4
vyhořely	vyhořet	k5eAaPmAgFnP
provozní	provozní	k2eAgFnPc1d1
budovy	budova	k1gFnPc1
mlýna	mlýn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1935	#num#	k4
jej	on	k3xPp3gInSc4
převzal	převzít	k5eAaPmAgMnS
vnuk	vnuk	k1gMnSc1
Jaroslav	Jaroslav	k1gMnSc1
Prokeš	Prokeš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
sloužil	sloužit	k5eAaImAgInS
mlýn	mlýn	k1gInSc4
širokému	široký	k2eAgNnSc3d1
okolí	okolí	k1gNnSc3
a	a	k8xC
k	k	k7c3
obživě	obživa	k1gFnSc3
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1954	#num#	k4
byl	být	k5eAaImAgInS
mlýn	mlýn	k1gInSc1
zrušen	zrušit	k5eAaPmNgInS
a	a	k8xC
zabaven	zabavit	k5eAaPmNgInS
státními	státní	k2eAgInPc7d1
statky	statek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rodina	rodina	k1gFnSc1
Prokešových	Prokešových	k2eAgMnPc2d1
byla	být	k5eAaImAgFnS
vyhnána	vyhnat	k5eAaPmNgFnS
během	během	k7c2
jednoho	jeden	k4xCgInSc2
dne	den	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1957	#num#	k4
objekty	objekt	k1gInPc1
mlýna	mlýn	k1gInSc2
a	a	k8xC
pozemky	pozemka	k1gFnSc2
převzalo	převzít	k5eAaPmAgNnS
místní	místní	k2eAgFnSc4d1
JZD	JZD	kA
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
opustilo	opustit	k5eAaPmAgNnS
<g/>
,	,	kIx,
a	a	k8xC
mlýn	mlýn	k1gInSc1
už	už	k6eAd1
jen	jen	k9
chátral	chátrat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1979	#num#	k4
byl	být	k5eAaImAgInS
zcela	zcela	k6eAd1
zdevastovaný	zdevastovaný	k2eAgInSc4d1
mlýn	mlýn	k1gInSc4
rozbořen	rozbořen	k2eAgInSc4d1
buldozery	buldozer	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plocha	plocha	k1gFnSc1
po	po	k7c6
mlýnu	mlýn	k1gInSc3
byla	být	k5eAaImAgFnS
srovnána	srovnat	k5eAaPmNgFnS
se	s	k7c7
zemí	zem	k1gFnSc7
<g/>
,	,	kIx,
vybetonována	vybetonován	k2eAgFnSc1d1
a	a	k8xC
vybudována	vybudován	k2eAgFnSc1d1
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
skládka	skládka	k1gFnSc1
řepy	řepa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zámeček	zámeček	k1gInSc1
</s>
<s>
Zámeček	zámeček	k1gInSc1
ve	v	k7c6
Starých	Starých	k2eAgFnPc6d1
Ždánicích	Ždánice	k1gFnPc6
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
mlynářem	mlynář	k1gMnSc7
Václavem	Václav	k1gMnSc7
Prokešem	Prokeš	k1gMnSc7
roku	rok	k1gInSc2
1877	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
do	do	k7c2
Ždánic	Ždánice	k1gFnPc2
přišel	přijít	k5eAaPmAgInS
rod	rod	k1gInSc1
Prokešů	Prokeš	k1gMnPc2
a	a	k8xC
na	na	k7c6
spáleništi	spáleniště	k1gNnSc6
starého	starý	k2eAgInSc2d1
mlýna	mlýn	k1gInSc2
postavil	postavit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1860	#num#	k4
mlýn	mlýn	k1gInSc1
nový	nový	k2eAgInSc1d1
<g/>
,	,	kIx,
již	již	k6eAd1
zděný	zděný	k2eAgMnSc1d1
a	a	k8xC
kamenný	kamenný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
nejmovitější	movitý	k2eAgFnSc1d3
rodina	rodina	k1gFnSc1
obce	obec	k1gFnSc2
si	se	k3xPyFc3
nedlouho	dlouho	k6eNd1
po	po	k7c6
stavbě	stavba	k1gFnSc6
mlýna	mlýn	k1gInSc2
nechala	nechat	k5eAaPmAgFnS
postavit	postavit	k5eAaPmF
honosnou	honosný	k2eAgFnSc4d1
vilu	vila	k1gFnSc4
<g/>
,	,	kIx,
tehdy	tehdy	k6eAd1
na	na	k7c6
okraji	okraj	k1gInSc6
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
rozcestí	rozcestí	k1gNnSc6
u	u	k7c2
křížku	křížek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budova	budova	k1gFnSc1
je	být	k5eAaImIp3nS
postavena	postaven	k2eAgFnSc1d1
v	v	k7c6
klasicistním	klasicistní	k2eAgInSc6d1
stylu	styl	k1gInSc6
s	s	k7c7
dekorativní	dekorativní	k2eAgFnSc7d1
čelní	čelní	k2eAgFnSc7d1
fasádou	fasáda	k1gFnSc7
a	a	k8xC
rizalitem	rizalit	k1gInSc7
se	s	k7c7
štítem	štít	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
zámeček	zámeček	k1gInSc4
dědili	dědit	k5eAaImAgMnP
ždáničtí	ždánický	k2eAgMnPc1d1
mlynáři	mlynář	k1gMnPc1
po	po	k7c4
tři	tři	k4xCgFnPc4
generace	generace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
konci	konec	k1gInSc3
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
nechal	nechat	k5eAaPmAgMnS
syn	syn	k1gMnSc1
Josef	Josef	k1gMnSc1
Prokeš	Prokeš	k1gMnSc1
vystavět	vystavět	k5eAaPmF
přízemní	přízemní	k2eAgFnSc4d1
přístavbu	přístavba	k1gFnSc4
z	z	k7c2
pravé	pravý	k2eAgFnSc2d1
strany	strana	k1gFnSc2
domu	dům	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1929	#num#	k4
byla	být	k5eAaImAgFnS
vnukem	vnuk	k1gMnSc7
Jaroslavem	Jaroslav	k1gMnSc7
Prokešem	Prokeš	k1gMnSc7
provedena	proveden	k2eAgFnSc1d1
nástavba	nástavba	k1gFnSc1
jednoho	jeden	k4xCgInSc2
podlaží	podlaží	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc4
domu	dům	k1gInSc2
sloužila	sloužit	k5eAaImAgFnS
téměř	téměř	k6eAd1
sto	sto	k4xCgNnSc4
let	léto	k1gNnPc2
jako	jako	k9
pekárna	pekárna	k1gFnSc1
a	a	k8xC
obchod	obchod	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
byla	být	k5eAaImAgFnS
tato	tento	k3xDgFnSc1
přístavba	přístavba	k1gFnSc1
zbourána	zbourat	k5eAaPmNgFnS
a	a	k8xC
odstraněn	odstraněn	k2eAgInSc4d1
střešní	střešní	k2eAgInSc4d1
štít	štít	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pošta	pošta	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
ve	v	k7c6
Ždánicích	Ždánice	k1gFnPc6
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1908	#num#	k4
<g/>
,	,	kIx,
provozovala	provozovat	k5eAaImAgFnS
několik	několik	k4yIc4
prvních	první	k4xOgNnPc2
let	léto	k1gNnPc2
svůj	svůj	k3xOyFgInSc4
úřad	úřad	k1gInSc4
v	v	k7c6
pronajatém	pronajatý	k2eAgNnSc6d1
přízemí	přízemí	k1gNnSc6
domu	dům	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
prodeji	prodej	k1gInSc6
zámečku	zámeček	k1gInSc2
<g/>
,	,	kIx,
poslední	poslední	k2eAgFnSc7d1
členkou	členka	k1gFnSc7
rodu	rod	k1gInSc2
Prokešů	Prokeš	k1gMnPc2
roku	rok	k1gInSc2
1991	#num#	k4
<g/>
,	,	kIx,
budova	budova	k1gFnSc1
jen	jen	k9
chátrala	chátrat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
prochází	procházet	k5eAaImIp3nS
renovací	renovace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
Most	most	k1gInSc1
přes	přes	k7c4
Opatovický	opatovický	k2eAgInSc4d1
kanál	kanál	k1gInSc4
</s>
<s>
Barokní	barokní	k2eAgFnSc1d1
socha	socha	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1733	#num#	k4
</s>
<s>
Socha	socha	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Marka	Marek	k1gMnSc2
na	na	k7c6
velkém	velký	k2eAgInSc6d1
neogotickém	ogotický	k2eNgInSc6d1
podstavci	podstavec	k1gInSc6
</s>
<s>
Litinový	litinový	k2eAgInSc1d1
kříž	kříž	k1gInSc1
na	na	k7c6
neogotickém	ogotický	k2eNgInSc6d1
pískovcovém	pískovcový	k2eAgInSc6d1
podstavci	podstavec	k1gInSc6
z	z	k7c2
roku	rok	k1gInSc2
1884	#num#	k4
(	(	kIx(
<g/>
u	u	k7c2
zámečku	zámeček	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Litinový	litinový	k2eAgInSc1d1
kříž	kříž	k1gInSc1
na	na	k7c6
neogotickém	ogotický	k2eNgInSc6d1
pískovcovém	pískovcový	k2eAgInSc6d1
podstavci	podstavec	k1gInSc6
(	(	kIx(
<g/>
u	u	k7c2
hostince	hostinec	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Železná	železný	k2eAgFnSc1d1
šestimetrová	šestimetrový	k2eAgFnSc1d1
zvonička	zvonička	k1gFnSc1
(	(	kIx(
<g/>
u	u	k7c2
hostince	hostinec	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Kubistický	kubistický	k2eAgInSc1d1
památník	památník	k1gInSc1
obětem	oběť	k1gFnPc3
I.	I.	kA
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1921	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
parku	park	k1gInSc6
<g/>
)	)	kIx)
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Staré	Staré	k2eAgFnPc1d1
Ždánice	Ždánice	k1gFnPc1
</s>
<s>
obecní	obecní	k2eAgInSc1d1
hostinec	hostinec	k1gInSc1
ve	v	k7c6
Starých	Starých	k2eAgFnPc6d1
Ždánicích	Ždánice	k1gFnPc6
</s>
<s>
Návesní	návesní	k2eAgInSc4d1
rybníček	rybníček	k1gInSc4
ve	v	k7c6
Starých	Starých	k2eAgFnPc6d1
Ždánicích	Ždánice	k1gFnPc6
</s>
<s>
Zámeček	zámeček	k1gInSc1
ve	v	k7c6
Starých	Starých	k2eAgFnPc6d1
Ždánicích	Ždánice	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
:	:	kIx,
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
obcích	obec	k1gFnPc6
-	-	kIx~
k	k	k7c3
1.1	1.1	k4
<g/>
.2020	.2020	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Staré	Staré	k2eAgFnPc1d1
Ždánice	Ždánice	k1gFnPc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
Staré	Staré	k2eAgFnPc1d1
Ždánice	Ždánice	k1gFnPc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Ždánice	Ždánice	k1gFnPc1
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Staré	Staré	k2eAgFnPc1d1
Ždánice	Ždánice	k1gFnPc1
v	v	k7c6
Registru	registrum	k1gNnSc6
územní	územní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
<g/>
,	,	kIx,
adres	adresa	k1gFnPc2
a	a	k8xC
nemovitostí	nemovitost	k1gFnPc2
(	(	kIx(
<g/>
RÚIAN	RÚIAN	kA
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Města	město	k1gNnSc2
<g/>
,	,	kIx,
městyse	městys	k1gInSc2
a	a	k8xC
obce	obec	k1gFnSc2
okresu	okres	k1gInSc2
Pardubice	Pardubice	k1gInPc1
</s>
<s>
Barchov	Barchov	k1gInSc1
•	•	k?
Bezděkov	Bezděkov	k1gInSc1
•	•	k?
Borek	borek	k1gInSc1
•	•	k?
Brloh	brloh	k1gInSc4
•	•	k?
Břehy	břeh	k1gInPc4
•	•	k?
Bukovina	Bukovina	k1gFnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
•	•	k?
Bukovina	Bukovina	k1gFnSc1
u	u	k7c2
Přelouče	Přelouč	k1gFnSc2
•	•	k?
Bukovka	bukovka	k1gFnSc1
•	•	k?
Býšť	Býšť	k1gFnSc1
•	•	k?
Časy	čas	k1gInPc4
•	•	k?
Čeperka	Čeperka	k1gFnSc1
•	•	k?
Čepí	čepit	k5eAaImIp3nS
•	•	k?
Černá	Černá	k1gFnSc1
u	u	k7c2
Bohdanče	Bohdanec	k1gMnSc5
•	•	k?
Dašice	Dašice	k1gFnSc1
•	•	k?
Dolany	Dolany	k1gInPc1
•	•	k?
Dolní	dolní	k2eAgFnSc1d1
Roveň	roveň	k1gFnSc1
•	•	k?
Dolní	dolní	k2eAgFnSc1d1
Ředice	Ředice	k1gFnSc1
•	•	k?
Dříteč	Dříteč	k1gInSc1
•	•	k?
Dubany	Dubana	k1gFnSc2
•	•	k?
Hlavečník	Hlavečník	k1gMnSc1
•	•	k?
Holice	holice	k1gFnSc2
•	•	k?
Holotín	Holotín	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Horní	horní	k2eAgFnSc1d1
Jelení	jelení	k2eAgFnSc1d1
•	•	k?
Horní	horní	k2eAgFnSc1d1
Ředice	Ředice	k1gFnSc1
•	•	k?
Hrobice	Hrobice	k1gFnSc2
•	•	k?
Choltice	Choltice	k1gFnSc2
•	•	k?
Choteč	Choteč	k1gMnSc1
•	•	k?
Chrtníky	Chrtník	k1gInPc1
•	•	k?
Chvaletice	Chvaletice	k1gFnPc4
•	•	k?
Chvojenec	Chvojenec	k1gMnSc1
•	•	k?
Chýšť	Chýšť	k1gMnSc1
•	•	k?
Jankovice	Jankovice	k1gFnSc2
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
•	•	k?
Jedousov	Jedousov	k1gInSc1
•	•	k?
Jeníkovice	Jeníkovice	k1gFnSc2
•	•	k?
Jezbořice	Jezbořice	k1gFnSc2
•	•	k?
Kasalice	Kasalice	k1gFnSc1
•	•	k?
Kladruby	Kladruby	k1gInPc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
•	•	k?
Kojice	Kojice	k1gFnSc2
•	•	k?
Kostěnice	Kostěnice	k1gFnSc2
•	•	k?
Křičeň	Křičeň	k1gFnSc1
•	•	k?
Kunětice	Kunětice	k1gFnSc2
•	•	k?
Labské	labský	k2eAgFnSc2d1
Chrčice	Chrčice	k1gFnSc2
•	•	k?
Lány	lán	k1gInPc4
u	u	k7c2
<g />
.	.	kIx.
</s>
<s hack="1">
Dašic	Dašic	k1gMnSc1
•	•	k?
Lázně	lázeň	k1gFnSc2
Bohdaneč	Bohdaneč	k1gMnSc1
•	•	k?
Libišany	Libišana	k1gFnSc2
•	•	k?
Lipoltice	Lipoltice	k1gFnSc2
•	•	k?
Litošice	Litošice	k1gFnSc2
•	•	k?
Malé	Malé	k2eAgInPc1d1
Výkleky	Výklek	k1gInPc1
•	•	k?
Mikulovice	Mikulovice	k1gFnSc2
•	•	k?
Mokošín	Mokošín	k1gMnSc1
•	•	k?
Morašice	Morašice	k1gFnSc2
•	•	k?
Moravany	Moravan	k1gMnPc4
•	•	k?
Němčice	Němčice	k1gFnSc1
•	•	k?
Neratov	Neratov	k1gInSc1
•	•	k?
Opatovice	Opatovice	k1gFnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
•	•	k?
Ostřešany	Ostřešana	k1gFnSc2
•	•	k?
Ostřetín	Ostřetín	k1gMnSc1
•	•	k?
Pardubice	Pardubice	k1gInPc1
•	•	k?
Plch	Plch	k1gMnSc1
•	•	k?
Poběžovice	Poběžovice	k1gFnPc4
u	u	k7c2
Holic	Holice	k1gFnPc2
•	•	k?
Poběžovice	Poběžovice	k1gFnPc4
u	u	k7c2
Přelouče	Přelouč	k1gFnSc2
•	•	k?
Podůlšany	Podůlšana	k1gFnSc2
•	•	k?
Pravy	Pravy	k?
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Přelouč	Přelouč	k1gFnSc1
•	•	k?
Přelovice	Přelovice	k1gFnSc1
•	•	k?
Přepychy	přepych	k1gInPc1
•	•	k?
Ráby	Ráb	k1gInPc1
•	•	k?
Rohovládova	Rohovládův	k2eAgMnSc2d1
Bělá	bělat	k5eAaImIp3nS
•	•	k?
Rohoznice	Rohoznice	k1gFnSc1
•	•	k?
Rokytno	Rokytno	k6eAd1
•	•	k?
Rybitví	Rybitví	k1gNnSc2
•	•	k?
Řečany	Řečan	k1gInPc7
nad	nad	k7c7
Labem	Labe	k1gNnSc7
•	•	k?
Selmice	Selmika	k1gFnSc3
•	•	k?
Semín	Semín	k1gInSc1
•	•	k?
Sezemice	Sezemika	k1gFnSc6
•	•	k?
Slepotice	Slepotice	k1gFnSc2
•	•	k?
Sopřeč	Sopřeč	k1gMnSc1
•	•	k?
Sovolusky	Sovoluska	k1gFnSc2
•	•	k?
Spojil	spojit	k5eAaPmAgMnS
•	•	k?
Srch	Srch	k1gInSc4
•	•	k?
Srnojedy	Srnojeda	k1gMnSc2
•	•	k?
Staré	Stará	k1gFnSc2
Hradiště	Hradiště	k1gNnSc2
•	•	k?
Staré	Staré	k2eAgMnPc7d1
Jesenčany	Jesenčan	k1gMnPc7
•	•	k?
Staré	Staré	k2eAgFnPc1d1
Ždánice	Ždánice	k1gFnPc1
•	•	k?
Starý	starý	k2eAgInSc4d1
Mateřov	Mateřov	k1gInSc4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Stéblová	stéblový	k2eAgFnSc1d1
•	•	k?
Stojice	Stojice	k1gFnSc1
•	•	k?
Strašov	Strašov	k1gInSc1
•	•	k?
Svinčany	Svinčan	k1gMnPc4
•	•	k?
Svojšice	Svojšice	k1gFnSc1
•	•	k?
Tetov	Tetov	k1gInSc1
•	•	k?
Trnávka	Trnávka	k1gFnSc1
•	•	k?
Trusnov	Trusnov	k1gInSc1
•	•	k?
Třebosice	Třebosice	k1gFnSc2
•	•	k?
Turkovice	Turkovice	k1gFnSc2
•	•	k?
Uhersko	Uhersko	k1gNnSc1
•	•	k?
Úhřetická	Úhřetický	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
•	•	k?
Újezd	Újezd	k1gInSc1
u	u	k7c2
Přelouče	Přelouč	k1gFnSc2
•	•	k?
Újezd	Újezd	k1gInSc1
u	u	k7c2
Sezemic	Sezemice	k1gInPc2
•	•	k?
Urbanice	Urbanice	k1gFnSc1
•	•	k?
Valy	Vala	k1gMnSc2
•	•	k?
Vápno	vápno	k1gNnSc1
•	•	k?
Veliny	Velina	k1gMnSc2
•	•	k?
Veselí	veselit	k5eAaImIp3nS
•	•	k?
Vlčí	vlčí	k2eAgFnSc1d1
Habřina	habřina	k1gFnSc1
•	•	k?
Voleč	Voleč	k1gMnSc1
•	•	k?
Vysoké	vysoký	k2eAgNnSc1d1
Chvojno	Chvojno	k1gNnSc1
•	•	k?
Vyšehněvice	Vyšehněvice	k1gFnSc2
•	•	k?
Zdechovice	Zdechovice	k1gFnSc2
•	•	k?
Žáravice	Žáravice	k1gFnSc2
•	•	k?
Živanice	Živanice	k1gFnSc1
legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
město	město	k1gNnSc1
<g/>
,	,	kIx,
městys	městys	k1gInSc1
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
</s>
