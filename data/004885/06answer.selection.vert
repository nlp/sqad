<s>
První	první	k4xOgNnSc1	první
elektrické	elektrický	k2eAgNnSc1d1	elektrické
křeslo	křeslo	k1gNnSc1	křeslo
bylo	být	k5eAaImAgNnS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
popravený	popravený	k2eAgInSc1d1	popravený
byl	být	k5eAaImAgInS	být
William	William	k1gInSc1	William
Kemmler	Kemmler	k1gInSc1	Kemmler
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
<g/>
.	.	kIx.	.
</s>
