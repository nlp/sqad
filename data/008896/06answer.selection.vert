<s>
Státní	státní	k2eAgInSc1d1	státní
ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
léčiv	léčivo	k1gNnPc2	léčivo
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
také	také	k9	také
SÚKL	SÚKL	kA	SÚKL
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
správní	správní	k2eAgInSc1d1	správní
úřad	úřad	k1gInSc1	úřad
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
podřízen	podřídit	k5eAaPmNgMnS	podřídit
Ministerstvu	ministerstvo	k1gNnSc3	ministerstvo
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
a	a	k8xC	a
organizační	organizační	k2eAgFnSc1d1	organizační
složka	složka	k1gFnSc1	složka
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
