<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
léčiv	léčivo	k1gNnPc2	léčivo
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
také	také	k9	také
SÚKL	SÚKL	kA	SÚKL
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
správní	správní	k2eAgInSc1d1	správní
úřad	úřad	k1gInSc1	úřad
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
podřízen	podřídit	k5eAaPmNgMnS	podřídit
Ministerstvu	ministerstvo	k1gNnSc3	ministerstvo
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
a	a	k8xC	a
organizační	organizační	k2eAgFnSc1d1	organizační
složka	složka	k1gFnSc1	složka
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
ústavu	ústav	k1gInSc2	ústav
je	být	k5eAaImIp3nS	být
dohlížet	dohlížet	k5eAaImF	dohlížet
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
používaly	používat	k5eAaImAgInP	používat
pouze	pouze	k6eAd1	pouze
jakostní	jakostní	k2eAgInPc1d1	jakostní
<g/>
,	,	kIx,	,
bezpečné	bezpečný	k2eAgInPc1d1	bezpečný
a	a	k8xC	a
účinné	účinný	k2eAgInPc1d1	účinný
léky	lék	k1gInPc1	lék
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
funkční	funkční	k2eAgFnPc1d1	funkční
a	a	k8xC	a
bezpečné	bezpečný	k2eAgFnPc1d1	bezpečná
zdravotnické	zdravotnický	k2eAgFnPc1d1	zdravotnická
pomůcky	pomůcka	k1gFnPc1	pomůcka
respektive	respektive	k9	respektive
prostředky	prostředek	k1gInPc1	prostředek
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
techniky	technika	k1gFnSc2	technika
(	(	kIx(	(
<g/>
PZT	PZT	kA	PZT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
činnost	činnost	k1gFnSc1	činnost
je	být	k5eAaImIp3nS	být
upravena	upravit	k5eAaPmNgFnS	upravit
českou	český	k2eAgFnSc7d1	Česká
legislativou	legislativa	k1gFnSc7	legislativa
i	i	k8xC	i
mezinárodními	mezinárodní	k2eAgFnPc7d1	mezinárodní
dohodami	dohoda	k1gFnPc7	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Předchůdcem	předchůdce	k1gMnSc7	předchůdce
dnešního	dnešní	k2eAgInSc2d1	dnešní
Státního	státní	k2eAgInSc2d1	státní
ústavu	ústav	k1gInSc2	ústav
byl	být	k5eAaImAgInS	být
někdejší	někdejší	k2eAgInSc1d1	někdejší
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
zkoumání	zkoumání	k1gNnSc4	zkoumání
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
již	již	k6eAd1	již
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
jako	jako	k8xS	jako
součást	součást	k1gFnSc4	součást
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgInSc1d1	dnešní
ústav	ústav	k1gInSc1	ústav
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
osamostatnil	osamostatnit	k5eAaPmAgInS	osamostatnit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obecně	obecně	k6eAd1	obecně
o	o	k7c6	o
SÚKL	SÚKL	kA	SÚKL
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poslání	poslání	k1gNnPc1	poslání
SÚKL	SÚKL	kA	SÚKL
===	===	k?	===
</s>
</p>
<p>
<s>
Posláním	poslání	k1gNnSc7	poslání
Státního	státní	k2eAgInSc2d1	státní
ústavu	ústav	k1gInSc2	ústav
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
léčiv	léčivo	k1gNnPc2	léčivo
(	(	kIx(	(
<g/>
SÚKL	SÚKL	kA	SÚKL
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
ochrany	ochrana	k1gFnSc2	ochrana
zdraví	zdraví	k1gNnSc2	zdraví
občanů	občan	k1gMnPc2	občan
zajistit	zajistit	k5eAaPmF	zajistit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
a	a	k8xC	a
při	při	k7c6	při
klinickém	klinický	k2eAgNnSc6d1	klinické
hodnocení	hodnocení	k1gNnSc6	hodnocení
používala	používat	k5eAaImAgFnS	používat
pouze	pouze	k6eAd1	pouze
farmaceuticky	farmaceuticky	k6eAd1	farmaceuticky
jakostní	jakostní	k2eAgNnPc1d1	jakostní
<g/>
,	,	kIx,	,
účinná	účinný	k2eAgNnPc1d1	účinné
a	a	k8xC	a
bezpečná	bezpečný	k2eAgNnPc1d1	bezpečné
léčiva	léčivo	k1gNnPc1	léčivo
<g/>
,	,	kIx,	,
jakostní	jakostní	k2eAgFnPc1d1	jakostní
a	a	k8xC	a
bezpečné	bezpečný	k2eAgFnPc1d1	bezpečná
suroviny	surovina	k1gFnPc1	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
a	a	k8xC	a
přípravu	příprava	k1gFnSc4	příprava
léčiv	léčivo	k1gNnPc2	léčivo
a	a	k8xC	a
bezpečné	bezpečný	k2eAgInPc1d1	bezpečný
a	a	k8xC	a
funkční	funkční	k2eAgInPc1d1	funkční
zdravotnické	zdravotnický	k2eAgInPc1d1	zdravotnický
prostředky	prostředek	k1gInPc1	prostředek
s	s	k7c7	s
informacemi	informace	k1gFnPc7	informace
popisujícími	popisující	k2eAgFnPc7d1	popisující
jejich	jejich	k3xOp3gFnPc4	jejich
objektivně	objektivně	k6eAd1	objektivně
zjištěné	zjištěný	k2eAgFnPc4d1	zjištěná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
a	a	k8xC	a
aby	aby	kYmCp3nP	aby
údaje	údaj	k1gInPc1	údaj
z	z	k7c2	z
výzkumu	výzkum	k1gInSc2	výzkum
léčiv	léčivo	k1gNnPc2	léčivo
a	a	k8xC	a
zdravotnických	zdravotnický	k2eAgInPc2d1	zdravotnický
prostředků	prostředek	k1gInPc2	prostředek
byly	být	k5eAaImAgFnP	být
věrohodné	věrohodný	k2eAgFnPc1d1	věrohodná
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
získávány	získávat	k5eAaImNgInP	získávat
eticky	eticky	k6eAd1	eticky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Činnost	činnost	k1gFnSc1	činnost
SÚKL	SÚKL	kA	SÚKL
===	===	k?	===
</s>
</p>
<p>
<s>
SÚKL	SÚKL	kA	SÚKL
je	být	k5eAaImIp3nS	být
správním	správní	k2eAgInSc7d1	správní
úřadem	úřad	k1gInSc7	úřad
s	s	k7c7	s
celostátní	celostátní	k2eAgFnSc7d1	celostátní
působností	působnost	k1gFnSc7	působnost
podřízeným	podřízený	k2eAgMnPc3d1	podřízený
Ministerstvu	ministerstvo	k1gNnSc3	ministerstvo
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
a	a	k8xC	a
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
ministr	ministr	k1gMnSc1	ministr
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
zákon	zákon	k1gInSc1	zákon
nestanoví	stanovit	k5eNaPmIp3nS	stanovit
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Činnost	činnost	k1gFnSc1	činnost
SÚKL	SÚKL	kA	SÚKL
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
léčiv	léčivo	k1gNnPc2	léčivo
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
primárně	primárně	k6eAd1	primárně
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
378	[number]	k4	378
<g/>
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
léčivech	léčivo	k1gNnPc6	léčivo
a	a	k8xC	a
o	o	k7c6	o
změnách	změna	k1gFnPc6	změna
některých	některý	k3yIgMnPc2	některý
souvisejících	související	k2eAgMnPc2d1	související
zákonů	zákon	k1gInPc2	zákon
(	(	kIx(	(
<g/>
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
léčivech	léčivo	k1gNnPc6	léčivo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
167	[number]	k4	167
<g/>
/	/	kIx~	/
<g/>
1998	[number]	k4	1998
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
návykových	návykový	k2eAgFnPc6d1	návyková
látkách	látka	k1gFnPc6	látka
a	a	k8xC	a
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
některých	některý	k3yIgInPc2	některý
dalších	další	k2eAgInPc2d1	další
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
160	[number]	k4	160
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c4	o
zdravotní	zdravotní	k2eAgFnSc4d1	zdravotní
péči	péče	k1gFnSc4	péče
v	v	k7c6	v
nestátních	státní	k2eNgNnPc6d1	nestátní
zdravotnických	zdravotnický	k2eAgNnPc6d1	zdravotnické
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
,	,	kIx,	,
a	a	k8xC	a
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
40	[number]	k4	40
<g/>
/	/	kIx~	/
<g/>
1995	[number]	k4	1995
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c4	o
regulaci	regulace	k1gFnSc4	regulace
reklamy	reklama	k1gFnSc2	reklama
a	a	k8xC	a
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
a	a	k8xC	a
doplnění	doplnění	k1gNnSc6	doplnění
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
468	[number]	k4	468
<g/>
/	/	kIx~	/
<g/>
1991	[number]	k4	1991
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c4	o
provozování	provozování	k1gNnSc4	provozování
rozhlasového	rozhlasový	k2eAgNnSc2d1	rozhlasové
a	a	k8xC	a
televizního	televizní	k2eAgNnSc2d1	televizní
vysílání	vysílání	k1gNnSc2	vysílání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Činnost	činnost	k1gFnSc1	činnost
SÚKL	SÚKL	kA	SÚKL
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
cen	cena	k1gFnPc2	cena
a	a	k8xC	a
úhrad	úhrada	k1gFnPc2	úhrada
léčiv	léčivo	k1gNnPc2	léčivo
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
48	[number]	k4	48
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
veřejném	veřejný	k2eAgNnSc6d1	veřejné
zdravotním	zdravotní	k2eAgNnSc6d1	zdravotní
pojištění	pojištění	k1gNnSc6	pojištění
a	a	k8xC	a
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
a	a	k8xC	a
doplnění	doplnění	k1gNnSc4	doplnění
některých	některý	k3yIgInPc2	některý
souvisejících	související	k2eAgInPc2d1	související
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
265	[number]	k4	265
<g/>
/	/	kIx~	/
<g/>
1991	[number]	k4	1991
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
působnosti	působnost	k1gFnSc6	působnost
orgánů	orgán	k1gInPc2	orgán
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
a	a	k8xC	a
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
526	[number]	k4	526
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
cenách	cena	k1gFnPc6	cena
<g/>
.	.	kIx.	.
</s>
<s>
Činnost	činnost	k1gFnSc1	činnost
SÚKL	SÚKL	kA	SÚKL
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
lidských	lidský	k2eAgFnPc2d1	lidská
tkání	tkáň	k1gFnPc2	tkáň
a	a	k8xC	a
buněk	buňka	k1gFnPc2	buňka
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
296	[number]	k4	296
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c4	o
zajištění	zajištění	k1gNnSc4	zajištění
jakosti	jakost	k1gFnSc2	jakost
a	a	k8xC	a
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
lidských	lidský	k2eAgFnPc2d1	lidská
tkání	tkáň	k1gFnPc2	tkáň
a	a	k8xC	a
buněk	buňka	k1gFnPc2	buňka
určených	určený	k2eAgInPc2d1	určený
k	k	k7c3	k
použití	použití	k1gNnSc3	použití
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
souvisejících	související	k2eAgInPc2d1	související
zákonů	zákon	k1gInPc2	zákon
(	(	kIx(	(
<g/>
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
lidských	lidský	k2eAgFnPc6d1	lidská
tkáních	tkáň	k1gFnPc6	tkáň
a	a	k8xC	a
buňkách	buňka	k1gFnPc6	buňka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Činnost	činnost	k1gFnSc1	činnost
SÚKL	SÚKL	kA	SÚKL
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zdravotnických	zdravotnický	k2eAgInPc2d1	zdravotnický
prostředků	prostředek	k1gInPc2	prostředek
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
123	[number]	k4	123
<g/>
/	/	kIx~	/
<g/>
200	[number]	k4	200
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
zdravotnických	zdravotnický	k2eAgInPc6d1	zdravotnický
prostředcích	prostředek	k1gInPc6	prostředek
a	a	k8xC	a
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
některých	některý	k3yIgInPc2	některý
souvisejících	související	k2eAgInPc2d1	související
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Veřejnost	veřejnost	k1gFnSc1	veřejnost
často	často	k6eAd1	často
mylně	mylně	k6eAd1	mylně
přiřazuje	přiřazovat	k5eAaImIp3nS	přiřazovat
k	k	k7c3	k
činnostem	činnost	k1gFnPc3	činnost
SÚKL	SÚKL	kA	SÚKL
i	i	k9	i
schvalování	schvalování	k1gNnSc2	schvalování
a	a	k8xC	a
dozor	dozor	k1gInSc1	dozor
nad	nad	k7c7	nad
doplňky	doplněk	k1gInPc7	doplněk
stravy	strava	k1gFnSc2	strava
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
SÚKL	SÚKL	kA	SÚKL
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
svých	svůj	k3xOyFgFnPc2	svůj
pravomocí	pravomoc	k1gFnPc2	pravomoc
stará	starat	k5eAaImIp3nS	starat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
léčivé	léčivý	k2eAgInPc4d1	léčivý
přípravky	přípravek	k1gInPc4	přípravek
<g/>
.	.	kIx.	.
</s>
<s>
Doplňky	doplněk	k1gInPc1	doplněk
stravy	strava	k1gFnSc2	strava
spadají	spadat	k5eAaPmIp3nP	spadat
do	do	k7c2	do
kompetencí	kompetence	k1gFnPc2	kompetence
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
Státního	státní	k2eAgInSc2d1	státní
zdravotního	zdravotní	k2eAgInSc2d1	zdravotní
ústavu	ústav	k1gInSc2	ústav
a	a	k8xC	a
Státní	státní	k2eAgFnSc2d1	státní
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
a	a	k8xC	a
potravinářské	potravinářský	k2eAgFnSc2d1	potravinářská
inspekce	inspekce	k1gFnSc2	inspekce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kompetence	kompetence	k1gFnSc2	kompetence
SÚKL	SÚKL	kA	SÚKL
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
kompetencí	kompetence	k1gFnPc2	kompetence
SÚKL	SÚKL	kA	SÚKL
spadají	spadat	k5eAaPmIp3nP	spadat
následující	následující	k2eAgFnPc4d1	následující
oblasti	oblast	k1gFnPc4	oblast
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ceny	cena	k1gFnPc1	cena
a	a	k8xC	a
úhrady	úhrada	k1gFnSc2	úhrada
–	–	k?	–
SÚKL	SÚKL	kA	SÚKL
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c6	o
maximálních	maximální	k2eAgFnPc6d1	maximální
cenách	cena	k1gFnPc6	cena
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
a	a	k8xC	a
o	o	k7c6	o
výši	výše	k1gFnSc6	výše
a	a	k8xC	a
podmínkách	podmínka	k1gFnPc6	podmínka
jejich	jejich	k3xOp3gFnPc2	jejich
úhrad	úhrada	k1gFnPc2	úhrada
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
stanovování	stanovování	k1gNnSc2	stanovování
maximálních	maximální	k2eAgFnPc2d1	maximální
cen	cena	k1gFnPc2	cena
a	a	k8xC	a
výše	výše	k1gFnSc2	výše
a	a	k8xC	a
podmínek	podmínka	k1gFnPc2	podmínka
úhrady	úhrada	k1gFnSc2	úhrada
je	být	k5eAaImIp3nS	být
individuální	individuální	k2eAgMnSc1d1	individuální
a	a	k8xC	a
přezkoumatelný	přezkoumatelný	k2eAgMnSc1d1	přezkoumatelný
<g/>
,	,	kIx,	,
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
režimu	režim	k1gInSc6	režim
správního	správní	k2eAgNnSc2d1	správní
řízení	řízení	k1gNnSc2	řízení
s	s	k7c7	s
pevně	pevně	k6eAd1	pevně
stanovenými	stanovený	k2eAgFnPc7d1	stanovená
lhůtami	lhůta	k1gFnPc7	lhůta
a	a	k8xC	a
plně	plně	k6eAd1	plně
respektuje	respektovat	k5eAaImIp3nS	respektovat
Evropskou	evropský	k2eAgFnSc4d1	Evropská
transparenční	transparenční	k2eAgFnSc4d1	transparenční
směrnici	směrnice	k1gFnSc4	směrnice
<g/>
.	.	kIx.	.
</s>
<s>
Žádosti	žádost	k1gFnPc1	žádost
a	a	k8xC	a
podněty	podnět	k1gInPc1	podnět
jsou	být	k5eAaImIp3nP	být
hodnoceny	hodnotit	k5eAaImNgInP	hodnotit
především	především	k6eAd1	především
na	na	k7c6	na
základě	základ	k1gInSc6	základ
posouzení	posouzení	k1gNnSc2	posouzení
účinnosti	účinnost	k1gFnSc2	účinnost
<g/>
,	,	kIx,	,
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
a	a	k8xC	a
nákladové	nákladový	k2eAgFnSc2d1	nákladová
efektivity	efektivita	k1gFnSc2	efektivita
<g/>
.	.	kIx.	.
</s>
<s>
Účastníky	účastník	k1gMnPc7	účastník
správního	správní	k2eAgNnSc2d1	správní
řízení	řízení	k1gNnSc2	řízení
jsou	být	k5eAaImIp3nP	být
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
pojišťovny	pojišťovna	k1gFnSc2	pojišťovna
a	a	k8xC	a
držitelé	držitel	k1gMnPc1	držitel
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
o	o	k7c4	o
registraci	registrace	k1gFnSc4	registrace
<g/>
.	.	kIx.	.
</s>
<s>
Podněty	podnět	k1gInPc1	podnět
mohou	moct	k5eAaImIp3nP	moct
podávat	podávat	k5eAaImF	podávat
i	i	k9	i
pacientské	pacientský	k2eAgFnPc4d1	pacientská
organizace	organizace	k1gFnPc4	organizace
či	či	k8xC	či
odborné	odborný	k2eAgFnPc4d1	odborná
společnosti	společnost	k1gFnPc4	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hraniční	hraniční	k2eAgFnPc1d1	hraniční
přípravky	přípravka	k1gFnPc1	přípravka
–	–	k?	–
Vydávání	vydávání	k1gNnSc1	vydávání
stanovisek	stanovisko	k1gNnPc2	stanovisko
<g/>
/	/	kIx~	/
<g/>
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
v	v	k7c6	v
případech	případ	k1gInPc6	případ
pochybností	pochybnost	k1gFnPc2	pochybnost
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
léčivý	léčivý	k2eAgInSc4d1	léčivý
přípravek	přípravek	k1gInSc4	přípravek
podléhající	podléhající	k2eAgFnSc4d1	podléhající
registraci	registrace	k1gFnSc4	registrace
nebo	nebo	k8xC	nebo
o	o	k7c4	o
léčivou	léčivý	k2eAgFnSc4d1	léčivá
látku	látka	k1gFnSc4	látka
nebo	nebo	k8xC	nebo
o	o	k7c4	o
jiný	jiný	k2eAgInSc4d1	jiný
výrobek	výrobek	k1gInSc4	výrobek
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
homeopatický	homeopatický	k2eAgInSc4d1	homeopatický
přípravek	přípravek	k1gInSc4	přípravek
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
nebo	nebo	k8xC	nebo
z	z	k7c2	z
vlastního	vlastní	k2eAgInSc2d1	vlastní
podnětu	podnět	k1gInSc2	podnět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Registrace	registrace	k1gFnSc1	registrace
léčiv	léčivo	k1gNnPc2	léčivo
–	–	k?	–
Každý	každý	k3xTgMnSc1	každý
hromadně	hromadně	k6eAd1	hromadně
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
léčivý	léčivý	k2eAgInSc1d1	léčivý
přípravek	přípravek	k1gInSc1	přípravek
podléhá	podléhat	k5eAaImIp3nS	podléhat
před	před	k7c7	před
uvedením	uvedení	k1gNnSc7	uvedení
na	na	k7c4	na
trh	trh	k1gInSc4	trh
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
registraci	registrace	k1gFnSc4	registrace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
registračního	registrační	k2eAgInSc2d1	registrační
procesu	proces	k1gInSc2	proces
se	se	k3xPyFc4	se
posuzuje	posuzovat	k5eAaImIp3nS	posuzovat
dokumentace	dokumentace	k1gFnSc1	dokumentace
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgInPc4	který
budoucí	budoucí	k2eAgMnSc1d1	budoucí
držitel	držitel	k1gMnSc1	držitel
registračního	registrační	k2eAgNnSc2d1	registrační
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
prokazuje	prokazovat	k5eAaImIp3nS	prokazovat
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
účinnost	účinnost	k1gFnSc4	účinnost
a	a	k8xC	a
kvalitu	kvalita	k1gFnSc4	kvalita
přípravku	přípravek	k1gInSc2	přípravek
<g/>
.	.	kIx.	.
</s>
<s>
Posuzují	posuzovat	k5eAaImIp3nP	posuzovat
se	se	k3xPyFc4	se
také	také	k9	také
indikace	indikace	k1gFnSc1	indikace
<g/>
,	,	kIx,	,
kontraindikace	kontraindikace	k1gFnSc1	kontraindikace
<g/>
,	,	kIx,	,
dávkování	dávkování	k1gNnSc1	dávkování
přípravku	přípravek	k1gInSc2	přípravek
<g/>
,	,	kIx,	,
klasifikace	klasifikace	k1gFnSc1	klasifikace
pro	pro	k7c4	pro
výdej	výdej	k1gInSc4	výdej
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
příbalová	příbalový	k2eAgFnSc1d1	příbalová
informace	informace	k1gFnSc1	informace
<g/>
]	]	kIx)	]
pro	pro	k7c4	pro
pacienta	pacient	k1gMnSc4	pacient
a	a	k8xC	a
návrh	návrh	k1gInSc4	návrh
textů	text	k1gInPc2	text
na	na	k7c4	na
obal	obal	k1gInSc4	obal
léčivého	léčivý	k2eAgInSc2d1	léčivý
přípravku	přípravek	k1gInSc2	přípravek
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
o	o	k7c4	o
registraci	registrace	k1gFnSc4	registrace
je	být	k5eAaImIp3nS	být
i	i	k9	i
souhrn	souhrn	k1gInSc1	souhrn
údajů	údaj	k1gInPc2	údaj
o	o	k7c4	o
přípravku	přípravka	k1gFnSc4	přípravka
(	(	kIx(	(
<g/>
SPC	SPC	kA	SPC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
lékařům	lékař	k1gMnPc3	lékař
a	a	k8xC	a
zdravotnickým	zdravotnický	k2eAgMnPc3d1	zdravotnický
odborníkům	odborník	k1gMnPc3	odborník
jako	jako	k8xC	jako
klíčový	klíčový	k2eAgInSc1d1	klíčový
zdroj	zdroj	k1gInSc1	zdroj
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
léčivém	léčivý	k2eAgInSc6d1	léčivý
přípravku	přípravek	k1gInSc6	přípravek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klinické	klinický	k2eAgNnSc1d1	klinické
hodnocení	hodnocení	k1gNnSc1	hodnocení
léčiv	léčivo	k1gNnPc2	léčivo
–	–	k?	–
Posuzování	posuzování	k1gNnSc2	posuzování
žádostí	žádost	k1gFnPc2	žádost
o	o	k7c4	o
povolení	povolení	k1gNnSc4	povolení
<g/>
/	/	kIx~	/
<g/>
ohlášení	ohlášení	k1gNnSc1	ohlášení
klinického	klinický	k2eAgNnSc2d1	klinické
hodnocení	hodnocení	k1gNnSc2	hodnocení
<g/>
,	,	kIx,	,
dohled	dohled	k1gInSc1	dohled
nad	nad	k7c7	nad
průběhem	průběh	k1gInSc7	průběh
klinických	klinický	k2eAgNnPc2d1	klinické
hodnocení	hodnocení	k1gNnPc2	hodnocení
<g/>
,	,	kIx,	,
vydávání	vydávání	k1gNnSc1	vydávání
stanovisek	stanovisko	k1gNnPc2	stanovisko
pro	pro	k7c4	pro
posouzení	posouzení	k1gNnSc4	posouzení
projektů	projekt	k1gInPc2	projekt
<g/>
,	,	kIx,	,
nejedná	jednat	k5eNaImIp3nS	jednat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
o	o	k7c4	o
klinické	klinický	k2eAgNnSc4d1	klinické
hodnocení	hodnocení	k1gNnSc4	hodnocení
regulované	regulovaný	k2eAgFnSc2d1	regulovaná
SÚKL	SÚKL	kA	SÚKL
a	a	k8xC	a
evidence	evidence	k1gFnSc1	evidence
použití	použití	k1gNnSc2	použití
neregistrovaných	registrovaný	k2eNgInPc2d1	neregistrovaný
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Specifické	specifický	k2eAgInPc1d1	specifický
léčebné	léčebný	k2eAgInPc1d1	léčebný
programy	program	k1gInPc1	program
–	–	k?	–
Vydávání	vydávání	k1gNnSc1	vydávání
stanovisek	stanovisko	k1gNnPc2	stanovisko
k	k	k7c3	k
žádostem	žádost	k1gFnPc3	žádost
o	o	k7c4	o
specifické	specifický	k2eAgInPc4d1	specifický
léčebné	léčebný	k2eAgInPc4d1	léčebný
programy	program	k1gInPc4	program
(	(	kIx(	(
<g/>
SLP	SLP	kA	SLP
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výdeje	výdej	k1gInPc1	výdej
<g/>
,	,	kIx,	,
prodeje	prodej	k1gInSc2	prodej
a	a	k8xC	a
přípravy	příprava	k1gFnSc2	příprava
léčiv	léčivo	k1gNnPc2	léčivo
–	–	k?	–
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
spadá	spadat	k5eAaPmIp3nS	spadat
výdej	výdej	k1gInSc4	výdej
osvědčení	osvědčení	k1gNnSc4	osvědčení
o	o	k7c6	o
technickém	technický	k2eAgNnSc6d1	technické
a	a	k8xC	a
věcném	věcný	k2eAgNnSc6d1	věcné
vybavení	vybavení	k1gNnSc6	vybavení
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
kontrola	kontrola	k1gFnSc1	kontrola
dodržování	dodržování	k1gNnSc2	dodržování
legislativních	legislativní	k2eAgInPc2d1	legislativní
požadavků	požadavek	k1gInPc2	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Kontrolovanými	kontrolovaný	k2eAgInPc7d1	kontrolovaný
subjekty	subjekt	k1gInPc7	subjekt
jsou	být	k5eAaImIp3nP	být
lékárny	lékárna	k1gFnSc2	lékárna
<g/>
,	,	kIx,	,
prodejci	prodejce	k1gMnPc1	prodejce
vyhrazených	vyhrazený	k2eAgNnPc2d1	vyhrazené
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
,	,	kIx,	,
specializovaná	specializovaný	k2eAgNnPc1d1	specializované
pracoviště	pracoviště	k1gNnPc1	pracoviště
zdravotnických	zdravotnický	k2eAgFnPc2d1	zdravotnická
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Kontrola	kontrola	k1gFnSc1	kontrola
zacházení	zacházení	k1gNnSc2	zacházení
s	s	k7c7	s
léčivými	léčivý	k2eAgInPc7d1	léčivý
přípravky	přípravek	k1gInPc7	přípravek
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
i	i	k9	i
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
zdravotnických	zdravotnický	k2eAgNnPc6d1	zdravotnické
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Kontrolu	kontrola	k1gFnSc4	kontrola
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
podle	podle	k7c2	podle
příslušnosti	příslušnost	k1gFnSc2	příslušnost
regionální	regionální	k2eAgNnSc4d1	regionální
pracoviště	pracoviště	k1gNnSc4	pracoviště
SÚKL	SÚKL	kA	SÚKL
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Farmakovigilance	Farmakovigilance	k1gFnSc1	Farmakovigilance
–	–	k?	–
Dozor	dozor	k1gInSc4	dozor
nad	nad	k7c7	nad
léčivými	léčivý	k2eAgInPc7d1	léčivý
přípravky	přípravek	k1gInPc7	přípravek
po	po	k7c6	po
jejich	jejich	k3xOp3gFnSc6	jejich
registraci	registrace	k1gFnSc6	registrace
směřující	směřující	k2eAgFnSc6d1	směřující
k	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
maximální	maximální	k2eAgFnSc2d1	maximální
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
a	a	k8xC	a
co	co	k9	co
nejvýhodnějšího	výhodný	k2eAgInSc2d3	nejvýhodnější
poměru	poměr	k1gInSc2	poměr
prospěšnosti	prospěšnost	k1gFnSc2	prospěšnost
léčivého	léčivý	k2eAgInSc2d1	léčivý
přípravku	přípravek	k1gInSc2	přípravek
k	k	k7c3	k
jeho	jeho	k3xOp3gNnPc3	jeho
rizikům	riziko	k1gNnPc3	riziko
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
této	tento	k3xDgFnSc2	tento
činnosti	činnost	k1gFnSc2	činnost
je	být	k5eAaImIp3nS	být
detekce	detekce	k1gFnSc1	detekce
<g/>
,	,	kIx,	,
hodnocení	hodnocení	k1gNnSc1	hodnocení
<g/>
,	,	kIx,	,
pochopení	pochopení	k1gNnSc1	pochopení
a	a	k8xC	a
prevence	prevence	k1gFnSc1	prevence
nežádoucích	žádoucí	k2eNgInPc2d1	nežádoucí
účinků	účinek	k1gInPc2	účinek
léků	lék	k1gInPc2	lék
nebo	nebo	k8xC	nebo
problémů	problém	k1gInPc2	problém
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
špatné	špatný	k2eAgNnSc1d1	špatné
užívání	užívání	k1gNnSc1	užívání
nebo	nebo	k8xC	nebo
zneužívání	zneužívání	k1gNnSc1	zneužívání
léků	lék	k1gInPc2	lék
<g/>
,	,	kIx,	,
lékové	lékový	k2eAgFnPc4d1	léková
interakce	interakce	k1gFnPc4	interakce
<g/>
,	,	kIx,	,
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
plod	plod	k1gInSc4	plod
<g/>
,	,	kIx,	,
na	na	k7c4	na
kojené	kojený	k2eAgFnPc4d1	kojená
děti	dítě	k1gFnPc4	dítě
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Distribuce	distribuce	k1gFnSc1	distribuce
léčiv	léčivo	k1gNnPc2	léčivo
–	–	k?	–
Kontrola	kontrola	k1gFnSc1	kontrola
dodržování	dodržování	k1gNnSc2	dodržování
legislativních	legislativní	k2eAgInPc2d1	legislativní
požadavků	požadavek	k1gInPc2	požadavek
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
distribuce	distribuce	k1gFnSc2	distribuce
léčiv	léčivo	k1gNnPc2	léčivo
se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c4	na
zásady	zásada	k1gFnPc4	zásada
správné	správný	k2eAgFnSc2d1	správná
distribuční	distribuční	k2eAgFnSc2d1	distribuční
praxe	praxe	k1gFnSc2	praxe
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
výdej	výdej	k1gInSc1	výdej
povolení	povolení	k1gNnSc2	povolení
k	k	k7c3	k
distribuční	distribuční	k2eAgFnSc3d1	distribuční
činnosti	činnost	k1gFnSc3	činnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dozor	dozor	k1gInSc4	dozor
nad	nad	k7c7	nad
výrobou	výroba	k1gFnSc7	výroba
léčiv	léčivo	k1gNnPc2	léčivo
–	–	k?	–
Dozorové	dozorový	k2eAgFnSc2d1	dozorová
aktivity	aktivita	k1gFnSc2	aktivita
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
výroby	výroba	k1gFnSc2	výroba
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
,	,	kIx,	,
správné	správný	k2eAgFnSc3d1	správná
klinické	klinický	k2eAgFnPc4d1	klinická
a	a	k8xC	a
laboratorní	laboratorní	k2eAgFnPc4d1	laboratorní
praxe	praxe	k1gFnSc2	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
agendu	agenda	k1gFnSc4	agenda
řešení	řešení	k1gNnSc2	řešení
závad	závada	k1gFnPc2	závada
v	v	k7c6	v
jakosti	jakost	k1gFnSc6	jakost
léčiv	léčivo	k1gNnPc2	léčivo
a	a	k8xC	a
pomocných	pomocný	k2eAgFnPc2d1	pomocná
látek	látka	k1gFnPc2	látka
dostupných	dostupný	k2eAgFnPc2d1	dostupná
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
vydávání	vydávání	k1gNnSc1	vydávání
závazných	závazný	k2eAgNnPc2d1	závazné
stanovisek	stanovisko	k1gNnPc2	stanovisko
k	k	k7c3	k
dovozu	dovoz	k1gInSc3	dovoz
a	a	k8xC	a
vývozu	vývoz	k1gInSc3	vývoz
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
celními	celní	k2eAgInPc7d1	celní
orgány	orgán	k1gInPc7	orgán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lidské	lidský	k2eAgFnSc2d1	lidská
tkáně	tkáň	k1gFnSc2	tkáň
a	a	k8xC	a
buňky	buňka	k1gFnSc2	buňka
–	–	k?	–
Dozor	dozor	k1gInSc1	dozor
nad	nad	k7c7	nad
darováním	darování	k1gNnSc7	darování
<g/>
,	,	kIx,	,
opatřováním	opatřování	k1gNnSc7	opatřování
<g/>
,	,	kIx,	,
vyšetřováním	vyšetřování	k1gNnSc7	vyšetřování
<g/>
,	,	kIx,	,
zpracováním	zpracování	k1gNnSc7	zpracování
<g/>
,	,	kIx,	,
skladováním	skladování	k1gNnSc7	skladování
a	a	k8xC	a
distribucí	distribuce	k1gFnSc7	distribuce
lidských	lidský	k2eAgFnPc2d1	lidská
tkání	tkáň	k1gFnPc2	tkáň
a	a	k8xC	a
buněk	buňka	k1gFnPc2	buňka
směřující	směřující	k2eAgInSc4d1	směřující
k	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
jejich	jejich	k3xOp3gFnSc2	jejich
jakosti	jakost	k1gFnSc2	jakost
a	a	k8xC	a
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
této	tento	k3xDgFnSc2	tento
činnosti	činnost	k1gFnSc2	činnost
je	být	k5eAaImIp3nS	být
vydávání	vydávání	k1gNnSc1	vydávání
povolení	povolení	k1gNnPc2	povolení
k	k	k7c3	k
činnosti	činnost	k1gFnSc3	činnost
tkáňového	tkáňový	k2eAgNnSc2d1	tkáňové
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
,	,	kIx,	,
odběrového	odběrový	k2eAgNnSc2d1	odběrové
zařízení	zařízení	k1gNnSc2	zařízení
nebo	nebo	k8xC	nebo
diagnostické	diagnostický	k2eAgFnSc2d1	diagnostická
laboratoře	laboratoř	k1gFnSc2	laboratoř
<g/>
,	,	kIx,	,
provádění	provádění	k1gNnSc2	provádění
kontrol	kontrola	k1gFnPc2	kontrola
<g/>
,	,	kIx,	,
sledování	sledování	k1gNnSc4	sledování
závažných	závažný	k2eAgFnPc2d1	závažná
nežádoucích	žádoucí	k2eNgFnPc2d1	nežádoucí
událostí	událost	k1gFnPc2	událost
a	a	k8xC	a
reakcí	reakce	k1gFnPc2	reakce
nebo	nebo	k8xC	nebo
podezření	podezření	k1gNnSc4	podezření
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
případech	případ	k1gInPc6	případ
pochybností	pochybnost	k1gFnPc2	pochybnost
rozhodování	rozhodování	k1gNnSc2	rozhodování
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
tkáně	tkáň	k1gFnPc4	tkáň
a	a	k8xC	a
buňky	buňka	k1gFnPc4	buňka
podléhající	podléhající	k2eAgFnSc4d1	podléhající
regulaci	regulace	k1gFnSc4	regulace
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
296	[number]	k4	296
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c4	o
zajištění	zajištění	k1gNnSc4	zajištění
jakosti	jakost	k1gFnSc2	jakost
a	a	k8xC	a
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
lidských	lidský	k2eAgFnPc2d1	lidská
tkání	tkáň	k1gFnPc2	tkáň
a	a	k8xC	a
buněk	buňka	k1gFnPc2	buňka
určených	určený	k2eAgInPc2d1	určený
k	k	k7c3	k
použití	použití	k1gNnSc3	použití
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
některých	některý	k3yIgInPc2	některý
souvisejících	související	k2eAgInPc2d1	související
zákonů	zákon	k1gInPc2	zákon
(	(	kIx(	(
<g/>
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
lidských	lidský	k2eAgFnPc6d1	lidská
tkáních	tkáň	k1gFnPc6	tkáň
a	a	k8xC	a
buňkách	buňka	k1gFnPc6	buňka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prosazování	prosazování	k1gNnSc1	prosazování
práva	právo	k1gNnSc2	právo
(	(	kIx(	(
<g/>
enforcement	enforcement	k1gInSc1	enforcement
<g/>
)	)	kIx)	)
–	–	k?	–
Vyhledávání	vyhledávání	k1gNnSc1	vyhledávání
a	a	k8xC	a
postihování	postihování	k1gNnSc1	postihování
protiprávního	protiprávní	k2eAgNnSc2d1	protiprávní
jednání	jednání	k1gNnSc2	jednání
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
prosazování	prosazování	k1gNnSc1	prosazování
práva	právo	k1gNnSc2	právo
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
zjištěn	zjistit	k5eAaPmNgInS	zjistit
nelegální	legální	k2eNgInSc1d1	nelegální
stav	stav	k1gInSc1	stav
<g/>
.	.	kIx.	.
</s>
<s>
Neoprávněné	oprávněný	k2eNgNnSc1d1	neoprávněné
zacházení	zacházení	k1gNnSc1	zacházení
s	s	k7c7	s
léčivy	léčivo	k1gNnPc7	léčivo
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
výrobu	výroba	k1gFnSc4	výroba
<g/>
,	,	kIx,	,
dovoz	dovoz	k1gInSc4	dovoz
<g/>
,	,	kIx,	,
distribuci	distribuce	k1gFnSc4	distribuce
<g/>
,	,	kIx,	,
výdej	výdej	k1gInSc4	výdej
nebo	nebo	k8xC	nebo
prodej	prodej	k1gInSc4	prodej
<g/>
,	,	kIx,	,
přípravu	příprava	k1gFnSc4	příprava
a	a	k8xC	a
souběžný	souběžný	k2eAgInSc4d1	souběžný
dovoz	dovoz	k1gInSc4	dovoz
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
bez	bez	k7c2	bez
platného	platný	k2eAgNnSc2d1	platné
povolení	povolení	k1gNnSc2	povolení
či	či	k8xC	či
oprávnění	oprávnění	k1gNnSc2	oprávnění
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
provádění	provádění	k1gNnSc2	provádění
klinického	klinický	k2eAgNnSc2d1	klinické
hodnocení	hodnocení	k1gNnSc2	hodnocení
a	a	k8xC	a
provádění	provádění	k1gNnSc2	provádění
činnosti	činnost	k1gFnSc2	činnost
kontrolní	kontrolní	k2eAgFnSc2d1	kontrolní
laboratoře	laboratoř	k1gFnSc2	laboratoř
bez	bez	k7c2	bez
příslušných	příslušný	k2eAgNnPc2d1	příslušné
povolení	povolení	k1gNnPc2	povolení
či	či	k8xC	či
ohlášení	ohlášení	k1gNnSc2	ohlášení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
prosazování	prosazování	k1gNnSc2	prosazování
práva	právo	k1gNnSc2	právo
Ústav	ústava	k1gFnPc2	ústava
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
institucemi	instituce	k1gFnPc7	instituce
v	v	k7c6	v
ČR	ČR	kA	ČR
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
Policie	policie	k1gFnSc1	policie
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
Celní	celní	k2eAgFnSc1d1	celní
správa	správa	k1gFnSc1	správa
<g/>
,	,	kIx,	,
SZPI	SZPI	kA	SZPI
<g/>
,	,	kIx,	,
kontrolní	kontrolní	k2eAgInPc1d1	kontrolní
úřady	úřad	k1gInPc1	úřad
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
EU	EU	kA	EU
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dozor	dozor	k1gInSc4	dozor
nad	nad	k7c7	nad
reklamou	reklama	k1gFnSc7	reklama
–	–	k?	–
Výkon	výkon	k1gInSc4	výkon
dozoru	dozor	k1gInSc2	dozor
nad	nad	k7c7	nad
dodržováním	dodržování	k1gNnSc7	dodržování
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
40	[number]	k4	40
<g/>
/	/	kIx~	/
<g/>
1995	[number]	k4	1995
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c4	o
regulaci	regulace	k1gFnSc4	regulace
reklamy	reklama	k1gFnSc2	reklama
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
reklamy	reklama	k1gFnSc2	reklama
na	na	k7c4	na
humánní	humánní	k2eAgInPc4d1	humánní
léčivé	léčivý	k2eAgInPc4d1	léčivý
přípravky	přípravek	k1gInPc4	přípravek
a	a	k8xC	a
sponzorování	sponzorování	k1gNnSc4	sponzorování
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
rozhlasového	rozhlasový	k2eAgNnSc2d1	rozhlasové
a	a	k8xC	a
televizního	televizní	k2eAgNnSc2d1	televizní
vysílání	vysílání	k1gNnSc2	vysílání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šetření	šetření	k1gNnSc1	šetření
podnětů	podnět	k1gInPc2	podnět
na	na	k7c4	na
závadnou	závadný	k2eAgFnSc4d1	závadná
reklamu	reklama	k1gFnSc4	reklama
na	na	k7c4	na
léčivé	léčivý	k2eAgInPc4d1	léčivý
přípravky	přípravek	k1gInPc4	přípravek
<g/>
,	,	kIx,	,
vydávání	vydávání	k1gNnSc1	vydávání
odborných	odborný	k2eAgNnPc2d1	odborné
stanovisek	stanovisko	k1gNnPc2	stanovisko
k	k	k7c3	k
reklamním	reklamní	k2eAgInPc3d1	reklamní
materiálům	materiál	k1gInPc3	materiál
a	a	k8xC	a
k	k	k7c3	k
problematice	problematika	k1gFnSc3	problematika
regulace	regulace	k1gFnSc2	regulace
reklamy	reklama	k1gFnSc2	reklama
<g/>
.	.	kIx.	.
</s>
<s>
Zákonem	zákon	k1gInSc7	zákon
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nP	muset
řídit	řídit	k5eAaImF	řídit
zadavatelé	zadavatel	k1gMnPc1	zadavatel
<g/>
,	,	kIx,	,
zpracovatelé	zpracovatel	k1gMnPc1	zpracovatel
<g/>
,	,	kIx,	,
šiřitelé	šiřitel	k1gMnPc1	šiřitel
reklamy	reklama	k1gFnSc2	reklama
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zdravotničtí	zdravotnický	k2eAgMnPc1d1	zdravotnický
odborníci	odborník	k1gMnPc1	odborník
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
např.	např.	kA	např.
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zúčastňují	zúčastňovat	k5eAaImIp3nP	zúčastňovat
sponzorovaných	sponzorovaný	k2eAgFnPc2d1	sponzorovaná
nebo	nebo	k8xC	nebo
reklamních	reklamní	k2eAgFnPc2d1	reklamní
akcí	akce	k1gFnPc2	akce
(	(	kIx(	(
<g/>
např.	např.	kA	např.
setkání	setkání	k1gNnSc1	setkání
odborníků	odborník	k1gMnPc2	odborník
<g/>
,	,	kIx,	,
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
konferencí	konference	k1gFnPc2	konference
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
využívají	využívat	k5eAaImIp3nP	využívat
reklamních	reklamní	k2eAgInPc2d1	reklamní
vzorků	vzorek	k1gInPc2	vzorek
a	a	k8xC	a
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
pacientům	pacient	k1gMnPc3	pacient
reklamní	reklamní	k2eAgInPc4d1	reklamní
materiály	materiál	k1gInPc7	materiál
o	o	k7c6	o
léčivých	léčivý	k2eAgInPc6d1	léčivý
přípravcích	přípravek	k1gInPc6	přípravek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Laboratorní	laboratorní	k2eAgFnPc1d1	laboratorní
činnosti	činnost	k1gFnPc1	činnost
a	a	k8xC	a
lékopisy	lékopis	k1gInPc1	lékopis
–	–	k?	–
Rozbory	rozbor	k1gInPc1	rozbor
léčiv	léčivo	k1gNnPc2	léčivo
požadované	požadovaný	k2eAgInPc4d1	požadovaný
zákonem	zákon	k1gInSc7	zákon
(	(	kIx(	(
<g/>
např.	např.	kA	např.
z	z	k7c2	z
namátkových	namátkový	k2eAgFnPc2d1	namátková
kontrol	kontrola	k1gFnPc2	kontrola
léčiv	léčivo	k1gNnPc2	léčivo
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
,	,	kIx,	,
propouštění	propouštění	k1gNnSc1	propouštění
šarží	šarže	k1gFnPc2	šarže
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
vyžádání	vyžádání	k1gNnPc2	vyžádání
jinými	jiný	k2eAgInPc7d1	jiný
útvary	útvar	k1gInPc7	útvar
ústavu	ústav	k1gInSc2	ústav
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
orgány	orgán	k1gInPc1	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
a	a	k8xC	a
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Laboratoře	laboratoř	k1gFnPc1	laboratoř
jsou	být	k5eAaImIp3nP	být
začleněny	začlenit	k5eAaPmNgFnP	začlenit
do	do	k7c2	do
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
sítě	síť	k1gFnSc2	síť
General	General	k1gFnSc2	General
Network	network	k1gInSc1	network
of	of	k?	of
Official	Official	k1gInSc1	Official
Medicines	Medicines	k1gMnSc1	Medicines
Control	Control	k1gInSc1	Control
Laboratoriem	laboratorium	k1gNnSc7	laboratorium
<g/>
.	.	kIx.	.
</s>
<s>
Lékopisná	lékopisný	k2eAgFnSc1d1	lékopisná
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c4	na
vydávání	vydávání	k1gNnSc4	vydávání
Českého	český	k2eAgInSc2d1	český
lékopisu	lékopis	k1gInSc2	lékopis
a	a	k8xC	a
přípravě	příprava	k1gFnSc3	příprava
Evropského	evropský	k2eAgInSc2d1	evropský
lékopisu	lékopis	k1gInSc2	lékopis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zdravotnické	zdravotnický	k2eAgInPc1d1	zdravotnický
prostředky	prostředek	k1gInPc1	prostředek
–	–	k?	–
Činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vyplývají	vyplývat	k5eAaImIp3nP	vyplývat
z	z	k7c2	z
legislativy	legislativa	k1gFnSc2	legislativa
vztahující	vztahující	k2eAgFnSc1d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
bezpečnosti	bezpečnost	k1gFnSc3	bezpečnost
zdravotnických	zdravotnický	k2eAgInPc2d1	zdravotnický
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
123	[number]	k4	123
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
zdravotnických	zdravotnický	k2eAgInPc6d1	zdravotnický
prostředcích	prostředek	k1gInPc6	prostředek
<g/>
,	,	kIx,	,
provádí	provádět	k5eAaImIp3nS	provádět
šetření	šetření	k1gNnSc1	šetření
nežádoucích	žádoucí	k2eNgFnPc2d1	nežádoucí
příhod	příhoda	k1gFnPc2	příhoda
zdravotnických	zdravotnický	k2eAgInPc2d1	zdravotnický
prostředků	prostředek	k1gInPc2	prostředek
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
vyhodnocování	vyhodnocování	k1gNnSc4	vyhodnocování
<g/>
,	,	kIx,	,
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
provádění	provádění	k1gNnSc4	provádění
klinického	klinický	k2eAgNnSc2d1	klinické
hodnocení	hodnocení	k1gNnSc2	hodnocení
nebo	nebo	k8xC	nebo
klinických	klinický	k2eAgFnPc2d1	klinická
zkoušek	zkouška	k1gFnPc2	zkouška
zdravotnických	zdravotnický	k2eAgInPc2d1	zdravotnický
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
zdravotnické	zdravotnický	k2eAgInPc4d1	zdravotnický
prostředky	prostředek	k1gInPc4	prostředek
u	u	k7c2	u
poskytovatelů	poskytovatel	k1gMnPc2	poskytovatel
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
péče	péče	k1gFnSc2	péče
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgFnPc6	který
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
zejména	zejména	k9	zejména
na	na	k7c4	na
vedení	vedení	k1gNnSc4	vedení
a	a	k8xC	a
uchovávaní	uchovávaný	k2eAgMnPc1d1	uchovávaný
evidence	evidence	k1gFnSc2	evidence
a	a	k8xC	a
dokumentace	dokumentace	k1gFnSc2	dokumentace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
na	na	k7c4	na
záznamy	záznam	k1gInPc4	záznam
o	o	k7c4	o
provádění	provádění	k1gNnSc4	provádění
údržeb	údržba	k1gFnPc2	údržba
a	a	k8xC	a
oprav	oprava	k1gFnPc2	oprava
<g/>
,	,	kIx,	,
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
instruktážích	instruktáž	k1gFnPc6	instruktáž
<g/>
,	,	kIx,	,
periodických	periodický	k2eAgFnPc6d1	periodická
kontrolách	kontrola	k1gFnPc6	kontrola
a	a	k8xC	a
nežádoucích	žádoucí	k2eNgFnPc6d1	nežádoucí
příhodách	příhoda	k1gFnPc6	příhoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Publikační	publikační	k2eAgFnSc1d1	publikační
činnost	činnost	k1gFnSc1	činnost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Věstník	věstník	k1gInSc1	věstník
SÚKL	SÚKL	kA	SÚKL
vychází	vycházet	k5eAaImIp3nS	vycházet
pravidelně	pravidelně	k6eAd1	pravidelně
1	[number]	k4	1
<g/>
×	×	k?	×
měsíčně	měsíčně	k6eAd1	měsíčně
a	a	k8xC	a
informuje	informovat	k5eAaBmIp3nS	informovat
o	o	k7c6	o
aktivitách	aktivita	k1gFnPc6	aktivita
SÚKL	SÚKL	kA	SÚKL
a	a	k8xC	a
vývoji	vývoj	k1gInSc6	vývoj
regulačních	regulační	k2eAgNnPc2d1	regulační
opatření	opatření	k1gNnPc2	opatření
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
léčiv	léčivo	k1gNnPc2	léčivo
a	a	k8xC	a
zdravotnických	zdravotnický	k2eAgInPc2d1	zdravotnický
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Výroční	výroční	k2eAgFnSc1d1	výroční
zpráva	zpráva	k1gFnSc1	zpráva
vychází	vycházet	k5eAaImIp3nS	vycházet
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
souhrn	souhrn	k1gInSc4	souhrn
všech	všecek	k3xTgFnPc2	všecek
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
činnosti	činnost	k1gFnSc6	činnost
SÚKL	SÚKL	kA	SÚKL
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Bulletin	bulletin	k1gInSc1	bulletin
Farmakoterapeutické	Farmakoterapeutický	k2eAgFnSc2d1	Farmakoterapeutická
informace	informace	k1gFnSc2	informace
vychází	vycházet	k5eAaImIp3nS	vycházet
měsíčně	měsíčně	k6eAd1	měsíčně
a	a	k8xC	a
jako	jako	k9	jako
nezávislý	závislý	k2eNgInSc1d1	nezávislý
lékový	lékový	k2eAgInSc1d1	lékový
bulletin	bulletin	k1gInSc1	bulletin
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
prestižní	prestižní	k2eAgFnSc2d1	prestižní
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
společnosti	společnost	k1gFnSc2	společnost
lékových	lékový	k2eAgInPc2d1	lékový
bulletinů	bulletin	k1gInPc2	bulletin
(	(	kIx(	(
<g/>
ISDB	ISDB	kA	ISDB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Informační	informační	k2eAgInSc1d1	informační
zpravodaj	zpravodaj	k1gInSc1	zpravodaj
–	–	k?	–
Nežádoucí	žádoucí	k2eNgInPc4d1	nežádoucí
účinky	účinek	k1gInPc4	účinek
léčiv	léčivo	k1gNnPc2	léčivo
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
předávání	předávání	k1gNnSc3	předávání
důležitých	důležitý	k2eAgFnPc2d1	důležitá
informací	informace	k1gFnPc2	informace
z	z	k7c2	z
farmakovigilančního	farmakovigilanční	k2eAgInSc2d1	farmakovigilanční
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
nežádoucích	žádoucí	k2eNgInPc6d1	nežádoucí
účincích	účinek	k1gInPc6	účinek
léčiv	léčivo	k1gNnPc2	léčivo
po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
uvedení	uvedení	k1gNnSc6	uvedení
do	do	k7c2	do
klinické	klinický	k2eAgFnSc2d1	klinická
praxe	praxe	k1gFnSc2	praxe
a	a	k8xC	a
také	také	k9	také
plní	plnit	k5eAaImIp3nP	plnit
funkci	funkce	k1gFnSc4	funkce
nástroje	nástroj	k1gInSc2	nástroj
zpětné	zpětný	k2eAgFnSc2d1	zpětná
vazby	vazba	k1gFnSc2	vazba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konzultační	konzultační	k2eAgFnSc1d1	konzultační
činnost	činnost	k1gFnSc1	činnost
–	–	k?	–
Státní	státní	k2eAgInSc1d1	státní
ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
léčiv	léčivo	k1gNnPc2	léčivo
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
své	svůj	k3xOyFgFnSc2	svůj
činnosti	činnost	k1gFnSc2	činnost
konzultace	konzultace	k1gFnPc4	konzultace
vztahující	vztahující	k2eAgFnPc4d1	vztahující
se	se	k3xPyFc4	se
ke	k	k7c3	k
všem	všecek	k3xTgFnPc3	všecek
oblastem	oblast	k1gFnPc3	oblast
jeho	jeho	k3xOp3gNnPc2	jeho
působení	působení	k1gNnSc2	působení
<g/>
.	.	kIx.	.
</s>
<s>
Konzultace	konzultace	k1gFnPc1	konzultace
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
odborné	odborný	k2eAgFnPc1d1	odborná
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
týkat	týkat	k5eAaImF	týkat
formálních	formální	k2eAgFnPc2d1	formální
a	a	k8xC	a
procedurálních	procedurální	k2eAgFnPc2d1	procedurální
stránek	stránka	k1gFnPc2	stránka
činností	činnost	k1gFnPc2	činnost
zajišťovaných	zajišťovaný	k2eAgInPc2d1	zajišťovaný
SÚKL	SÚKL	kA	SÚKL
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Spolupráce	spolupráce	k1gFnSc1	spolupráce
SÚKL	SÚKL	kA	SÚKL
===	===	k?	===
</s>
</p>
<p>
<s>
Spolupráce	spolupráce	k1gFnSc1	spolupráce
SÚKL	SÚKL	kA	SÚKL
s	s	k7c7	s
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
státními	státní	k2eAgFnPc7d1	státní
i	i	k8xC	i
mezinárodními	mezinárodní	k2eAgFnPc7d1	mezinárodní
organizacemi	organizace	k1gFnPc7	organizace
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
úkolů	úkol	k1gInPc2	úkol
vedle	vedle	k7c2	vedle
základní	základní	k2eAgFnSc2d1	základní
činnosti	činnost	k1gFnSc2	činnost
a	a	k8xC	a
poslání	poslání	k1gNnSc2	poslání
SÚKL	SÚKL	kA	SÚKL
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
institucemi	instituce	k1gFnPc7	instituce
EU	EU	kA	EU
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
zahraničními	zahraniční	k2eAgMnPc7d1	zahraniční
partnery	partner	k1gMnPc7	partner
====	====	k?	====
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
aktivity	aktivita	k1gFnSc2	aktivita
SÚKL	SÚKL	kA	SÚKL
jsou	být	k5eAaImIp3nP	být
především	především	k6eAd1	především
soustředěny	soustředit	k5eAaPmNgFnP	soustředit
na	na	k7c6	na
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
institucemi	instituce	k1gFnPc7	instituce
EU	EU	kA	EU
a	a	k8xC	a
členskými	členský	k2eAgInPc7d1	členský
státy	stát	k1gInPc7	stát
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
SÚKL	SÚKL	kA	SÚKL
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
zúčastňuje	zúčastňovat	k5eAaImIp3nS	zúčastňovat
práce	práce	k1gFnPc4	práce
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
evropských	evropský	k2eAgFnPc2d1	Evropská
nebo	nebo	k8xC	nebo
mezinárodních	mezinárodní	k2eAgInPc6d1	mezinárodní
výborech	výbor	k1gInPc6	výbor
nebo	nebo	k8xC	nebo
pracovních	pracovní	k2eAgFnPc6d1	pracovní
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Síť	síť	k1gFnSc1	síť
ředitelů	ředitel	k1gMnPc2	ředitel
lékových	lékový	k2eAgFnPc2d1	léková
agentur	agentura	k1gFnPc2	agentura
(	(	kIx(	(
<g/>
HMA	HMA	kA	HMA
<g/>
)	)	kIx)	)
–	–	k?	–
SÚKL	SÚKL	kA	SÚKL
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
a	a	k8xC	a
koordinuje	koordinovat	k5eAaBmIp3nS	koordinovat
své	svůj	k3xOyFgFnPc4	svůj
odborné	odborný	k2eAgFnPc4d1	odborná
činnosti	činnost	k1gFnPc4	činnost
se	s	k7c7	s
zástupci	zástupce	k1gMnPc7	zástupce
jiných	jiný	k2eAgFnPc2d1	jiná
lékových	lékový	k2eAgFnPc2d1	léková
agentur	agentura	k1gFnPc2	agentura
EU	EU	kA	EU
a	a	k8xC	a
EHP	EHP	kA	EHP
nejenom	nejenom	k6eAd1	nejenom
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
ředitelů	ředitel	k1gMnPc2	ředitel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
mnoha	mnoho	k4c2	mnoho
pracovních	pracovní	k2eAgFnPc2d1	pracovní
skupin	skupina	k1gFnPc2	skupina
založených	založený	k2eAgFnPc2d1	založená
řediteli	ředitel	k1gMnPc7	ředitel
pro	pro	k7c4	pro
aktuální	aktuální	k2eAgNnPc4d1	aktuální
témata	téma	k1gNnPc4	téma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Síť	síť	k1gFnSc1	síť
regulačních	regulační	k2eAgFnPc2d1	regulační
autorit	autorita	k1gFnPc2	autorita
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
cen	cena	k1gFnPc2	cena
a	a	k8xC	a
úhrad	úhrada	k1gFnPc2	úhrada
léčiv	léčivo	k1gNnPc2	léčivo
(	(	kIx(	(
<g/>
NMCAPR	NMCAPR	kA	NMCAPR
<g/>
)	)	kIx)	)
–	–	k?	–
Spolupráce	spolupráce	k1gFnSc1	spolupráce
zástupců	zástupce	k1gMnPc2	zástupce
kompetentních	kompetentní	k2eAgFnPc2d1	kompetentní
autorit	autorita	k1gFnPc2	autorita
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
cen	cena	k1gFnPc2	cena
a	a	k8xC	a
úhrad	úhrada	k1gFnPc2	úhrada
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
síť	síť	k1gFnSc1	síť
spolupráce	spolupráce	k1gFnSc1	spolupráce
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
mítinku	mítink	k1gInSc6	mítink
ve	v	k7c6	v
slovinském	slovinský	k2eAgNnSc6d1	slovinské
Brdu	brdo	k1gNnSc6	brdo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
v	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
činnost	činnost	k1gFnSc4	činnost
Farmaceutického	farmaceutický	k2eAgNnSc2d1	farmaceutické
fóra	fórum	k1gNnSc2	fórum
<g/>
.	.	kIx.	.
</s>
<s>
Setkání	setkání	k1gNnSc1	setkání
pořádá	pořádat	k5eAaImIp3nS	pořádat
stát	stát	k5eAaImF	stát
předsedající	předsedající	k1gMnSc1	předsedající
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Přípravu	příprava	k1gFnSc4	příprava
jednání	jednání	k1gNnSc2	jednání
a	a	k8xC	a
sledování	sledování	k1gNnSc2	sledování
aktuálního	aktuální	k2eAgNnSc2d1	aktuální
dění	dění	k1gNnSc2	dění
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
starosti	starost	k1gFnPc4	starost
organizační	organizační	k2eAgFnSc2d1	organizační
komise	komise	k1gFnSc2	komise
(	(	kIx(	(
<g/>
OC	OC	kA	OC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
zástupců	zástupce	k1gMnPc2	zástupce
4	[number]	k4	4
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
stát	stát	k1gInSc1	stát
s	s	k7c7	s
minulým	minulý	k2eAgNnSc7d1	Minulé
předsednictvím	předsednictví	k1gNnSc7	předsednictví
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
předsedající	předsedající	k2eAgFnSc1d1	předsedající
<g/>
,	,	kIx,	,
a	a	k8xC	a
2	[number]	k4	2
státy	stát	k1gInPc4	stát
s	s	k7c7	s
následujícím	následující	k2eAgNnSc7d1	následující
předsednictvím	předsednictví	k1gNnSc7	předsednictví
<g/>
)	)	kIx)	)
a	a	k8xC	a
2	[number]	k4	2
zástupců	zástupce	k1gMnPc2	zástupce
komise	komise	k1gFnSc2	komise
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
práci	práce	k1gFnSc4	práce
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
starosti	starost	k1gFnPc4	starost
předsedající	předsedající	k2eAgInSc1d1	předsedající
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
léková	lékový	k2eAgFnSc1d1	léková
agentura	agentura	k1gFnSc1	agentura
(	(	kIx(	(
<g/>
EMA	Ema	k1gFnSc1	Ema
<g/>
)	)	kIx)	)
–	–	k?	–
Odborní	odbornět	k5eAaImIp3nP	odbornět
pracovníci	pracovník	k1gMnPc1	pracovník
SÚKL	SÚKL	kA	SÚKL
jsou	být	k5eAaImIp3nP	být
aktivně	aktivně	k6eAd1	aktivně
zapojeni	zapojit	k5eAaPmNgMnP	zapojit
zejména	zejména	k9	zejména
v	v	k7c4	v
práci	práce	k1gFnSc4	práce
vědeckého	vědecký	k2eAgInSc2d1	vědecký
Výboru	výbor	k1gInSc2	výbor
EMA	Ema	k1gMnSc1	Ema
pro	pro	k7c4	pro
humánní	humánní	k2eAgInPc4d1	humánní
léčivé	léčivý	k2eAgInPc4d1	léčivý
přípravky	přípravek	k1gInPc4	přípravek
(	(	kIx(	(
<g/>
CHMP	CHMP	kA	CHMP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
léčivé	léčivý	k2eAgInPc4d1	léčivý
přípravky	přípravek	k1gInPc4	přípravek
pro	pro	k7c4	pro
vzácná	vzácný	k2eAgNnPc4d1	vzácné
onemocnění	onemocnění	k1gNnPc4	onemocnění
(	(	kIx(	(
<g/>
COMP	COMP	kA	COMP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Koordinační	koordinační	k2eAgFnSc2d1	koordinační
skupiny	skupina	k1gFnSc2	skupina
pro	pro	k7c4	pro
postupy	postup	k1gInPc4	postup
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
uznávání	uznávání	k1gNnSc2	uznávání
a	a	k8xC	a
decentralizované	decentralizovaný	k2eAgInPc1d1	decentralizovaný
postupy	postup	k1gInPc1	postup
–	–	k?	–
humánní	humánní	k2eAgInPc4d1	humánní
léčivé	léčivý	k2eAgInPc4d1	léčivý
přípravky	přípravek	k1gInPc4	přípravek
(	(	kIx(	(
<g/>
CMD-h	CMD	k1gInSc4	CMD-h
<g/>
)	)	kIx)	)
a	a	k8xC	a
Výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
rostlinné	rostlinný	k2eAgInPc4d1	rostlinný
léčivé	léčivý	k2eAgInPc4d1	léčivý
přípravky	přípravek	k1gInPc4	přípravek
(	(	kIx(	(
<g/>
HMPC	HMPC	kA	HMPC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
(	(	kIx(	(
<g/>
EK	EK	kA	EK
<g/>
)	)	kIx)	)
–	–	k?	–
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
SÚKL	SÚKL	kA	SÚKL
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
ve	v	k7c6	v
výborech	výbor	k1gInPc6	výbor
<g/>
,	,	kIx,	,
Pharmaceutical	Pharmaceutical	k1gFnSc1	Pharmaceutical
Committee	Committee	k1gFnSc1	Committee
a	a	k8xC	a
Standing	Standing	k1gInSc1	Standing
Committee	Committe	k1gFnSc2	Committe
a	a	k8xC	a
také	také	k9	také
v	v	k7c4	v
Transparency	Transparenc	k2eAgFnPc4d1	Transparenc
Committee	Committe	k1gFnPc4	Committe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rada	rada	k1gFnSc1	rada
EU	EU	kA	EU
–	–	k?	–
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
SÚKL	SÚKL	kA	SÚKL
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
své	svůj	k3xOyFgFnSc2	svůj
gesce	gesce	k1gFnSc2	gesce
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
aktuálních	aktuální	k2eAgInPc2d1	aktuální
dokumentů	dokument	k1gInPc2	dokument
na	na	k7c6	na
jednání	jednání	k1gNnSc6	jednání
Pracovní	pracovní	k2eAgFnSc2d1	pracovní
skupiny	skupina	k1gFnSc2	skupina
Rady	rada	k1gFnSc2	rada
EU	EU	kA	EU
pro	pro	k7c4	pro
léčiva	léčivo	k1gNnPc4	léčivo
a	a	k8xC	a
zdravotnické	zdravotnický	k2eAgInPc4d1	zdravotnický
prostředky	prostředek	k1gInPc4	prostředek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rada	rada	k1gFnSc1	rada
Evropy	Evropa	k1gFnSc2	Evropa
–	–	k?	–
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
prohlubuje	prohlubovat	k5eAaImIp3nS	prohlubovat
spolupráce	spolupráce	k1gFnSc1	spolupráce
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
padělků	padělek	k1gInPc2	padělek
<g/>
,	,	kIx,	,
internetového	internetový	k2eAgInSc2d1	internetový
prodeje	prodej	k1gInSc2	prodej
léčiv	léčivo	k1gNnPc2	léčivo
a	a	k8xC	a
nelegálního	legální	k2eNgNnSc2d1	nelegální
zacházení	zacházení	k1gNnSc2	zacházení
s	s	k7c7	s
léčivými	léčivý	k2eAgInPc7d1	léčivý
přípravky	přípravek	k1gInPc7	přípravek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Evropské	evropský	k2eAgNnSc1d1	Evropské
ústředí	ústředí	k1gNnSc1	ústředí
pro	pro	k7c4	pro
kvalitu	kvalita	k1gFnSc4	kvalita
léčiv	léčivo	k1gNnPc2	léčivo
a	a	k8xC	a
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
péče	péče	k1gFnSc2	péče
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
EDQM	EDQM	kA	EDQM
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
spolupráci	spolupráce	k1gFnSc4	spolupráce
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
(	(	kIx(	(
<g/>
OECD	OECD	kA	OECD
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Světová	světový	k2eAgFnSc1d1	světová
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
organizace	organizace	k1gFnSc1	organizace
(	(	kIx(	(
<g/>
WHO	WHO	kA	WHO
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Medicine	Medicinout	k5eAaPmIp3nS	Medicinout
Evaluation	Evaluation	k1gInSc1	Evaluation
Committee	Committe	k1gFnSc2	Committe
(	(	kIx(	(
<g/>
MEDEV	MEDEV	kA	MEDEV
<g/>
)	)	kIx)	)
–	–	k?	–
SÚKL	SÚKL	kA	SÚKL
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
členem	člen	k1gInSc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
komisi	komise	k1gFnSc4	komise
založenou	založený	k2eAgFnSc4d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
členy	člen	k1gMnPc7	člen
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
evropské	evropský	k2eAgFnPc4d1	Evropská
instituce	instituce	k1gFnPc4	instituce
rozhodující	rozhodující	k2eAgFnPc4d1	rozhodující
o	o	k7c6	o
cenách	cena	k1gFnPc6	cena
a	a	k8xC	a
úhradách	úhrada	k1gFnPc6	úhrada
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
<g/>
,	,	kIx,	,
plátci	plátce	k1gMnPc1	plátce
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
péče	péče	k1gFnSc2	péče
a	a	k8xC	a
akademické	akademický	k2eAgFnSc2d1	akademická
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
komise	komise	k1gFnSc1	komise
je	být	k5eAaImIp3nS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
výměnu	výměna	k1gFnSc4	výměna
informací	informace	k1gFnPc2	informace
v	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
hodnocení	hodnocení	k1gNnSc2	hodnocení
klinické	klinický	k2eAgFnSc2d1	klinická
a	a	k8xC	a
relativní	relativní	k2eAgFnSc2d1	relativní
efektivity	efektivita	k1gFnSc2	efektivita
nových	nový	k2eAgNnPc2d1	nové
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
,	,	kIx,	,
farmakoekonomiky	farmakoekonomika	k1gFnSc2	farmakoekonomika
a	a	k8xC	a
nákladů	náklad	k1gInPc2	náklad
na	na	k7c4	na
zdravotní	zdravotní	k2eAgFnSc4d1	zdravotní
péči	péče	k1gFnSc4	péče
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Health	Health	k1gMnSc1	Health
Technology	technolog	k1gMnPc4	technolog
Assessment	Assessment	k1gInSc4	Assessment
international	internationat	k5eAaImAgInS	internationat
(	(	kIx(	(
<g/>
HTAi	HTAe	k1gFnSc4	HTAe
<g/>
)	)	kIx)	)
<g/>
SÚKL	SÚKL	kA	SÚKL
se	se	k3xPyFc4	se
do	do	k7c2	do
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
diskusí	diskuse	k1gFnPc2	diskuse
zapojuje	zapojovat	k5eAaImIp3nS	zapojovat
i	i	k9	i
jinou	jiný	k2eAgFnSc7d1	jiná
formou	forma	k1gFnSc7	forma
než	než	k8xS	než
členstvím	členství	k1gNnSc7	členství
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
pracovních	pracovní	k2eAgFnPc6d1	pracovní
skupinách	skupina	k1gFnPc6	skupina
a	a	k8xC	a
aktivní	aktivní	k2eAgFnSc7d1	aktivní
účastí	účast	k1gFnSc7	účast
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejčastější	častý	k2eAgFnPc4d3	nejčastější
formy	forma	k1gFnPc4	forma
patří	patřit	k5eAaImIp3nS	patřit
přispívání	přispívání	k1gNnPc4	přispívání
do	do	k7c2	do
veřejných	veřejný	k2eAgFnPc2d1	veřejná
konzultací	konzultace	k1gFnPc2	konzultace
<g/>
,	,	kIx,	,
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
rešerší	rešerše	k1gFnPc2	rešerše
či	či	k8xC	či
podílení	podílení	k1gNnSc2	podílení
se	se	k3xPyFc4	se
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
iniciativách	iniciativa	k1gFnPc6	iniciativa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Spolupráce	spolupráce	k1gFnSc1	spolupráce
se	s	k7c7	s
státními	státní	k2eAgFnPc7d1	státní
institucemi	instituce	k1gFnPc7	instituce
v	v	k7c6	v
ČR	ČR	kA	ČR
====	====	k?	====
</s>
</p>
<p>
<s>
SÚKL	SÚKL	kA	SÚKL
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
rozbory	rozbor	k1gInPc4	rozbor
<g/>
,	,	kIx,	,
stanoviska	stanovisko	k1gNnPc4	stanovisko
<g/>
,	,	kIx,	,
hlášení	hlášení	k1gNnPc4	hlášení
a	a	k8xC	a
připomínky	připomínka	k1gFnPc4	připomínka
k	k	k7c3	k
různým	různý	k2eAgInPc3d1	různý
materiálům	materiál	k1gInPc3	materiál
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
–	–	k?	–
Intenzivní	intenzivní	k2eAgFnSc2d1	intenzivní
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
probíhá	probíhat	k5eAaImIp3nS	probíhat
zejména	zejména	k9	zejména
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
a	a	k8xC	a
následném	následný	k2eAgInSc6d1	následný
legislativním	legislativní	k2eAgInSc6d1	legislativní
procesu	proces	k1gInSc6	proces
schvalovaní	schvalovaný	k2eAgMnPc1d1	schvalovaný
nových	nový	k2eAgInPc2d1	nový
právních	právní	k2eAgInPc2d1	právní
předpisů	předpis	k1gInPc2	předpis
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
činnosti	činnost	k1gFnPc1	činnost
SÚKL	SÚKL	kA	SÚKL
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
financí	finance	k1gFnPc2	finance
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
–	–	k?	–
Spolupráce	spolupráce	k1gFnSc1	spolupráce
v	v	k7c6	v
agendách	agenda	k1gFnPc6	agenda
celní	celní	k2eAgFnSc2d1	celní
správy	správa	k1gFnSc2	správa
(	(	kIx(	(
<g/>
prevence	prevence	k1gFnSc1	prevence
padělání	padělání	k1gNnSc2	padělání
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
)	)	kIx)	)
a	a	k8xC	a
finanční	finanční	k2eAgFnSc2d1	finanční
<g/>
/	/	kIx~	/
<g/>
daňové	daňový	k2eAgFnSc2d1	daňová
správy	správa	k1gFnSc2	správa
(	(	kIx(	(
<g/>
prevence	prevence	k1gFnSc1	prevence
daňových	daňový	k2eAgInPc2d1	daňový
deliktů	delikt	k1gInPc2	delikt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
–	–	k?	–
Na	na	k7c4	na
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
je	být	k5eAaImIp3nS	být
SÚKL	SÚKL	kA	SÚKL
napojen	napojit	k5eAaPmNgInS	napojit
v	v	k7c6	v
agendě	agenda	k1gFnSc6	agenda
geneticky	geneticky	k6eAd1	geneticky
modifikovaných	modifikovaný	k2eAgInPc2d1	modifikovaný
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
obalů	obal	k1gInPc2	obal
léčiv	léčivo	k1gNnPc2	léčivo
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
inspekčních	inspekční	k2eAgFnPc2d1	inspekční
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
–	–	k?	–
S	s	k7c7	s
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
konzultuje	konzultovat	k5eAaImIp3nS	konzultovat
regulační	regulační	k2eAgFnPc4d1	regulační
otázky	otázka	k1gFnPc4	otázka
s	s	k7c7	s
dopadem	dopad	k1gInSc7	dopad
na	na	k7c4	na
výrobce	výrobce	k1gMnSc4	výrobce
léčiv	léčivo	k1gNnPc2	léčivo
a	a	k8xC	a
reklamu	reklama	k1gFnSc4	reklama
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
–	–	k?	–
S	s	k7c7	s
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
a	a	k8xC	a
Stálým	stálý	k2eAgNnSc7d1	stálé
zastoupením	zastoupení	k1gNnSc7	zastoupení
ČR	ČR	kA	ČR
při	při	k7c6	při
EU	EU	kA	EU
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
probíhají	probíhat	k5eAaImIp3nP	probíhat
jednání	jednání	k1gNnSc4	jednání
zejména	zejména	k9	zejména
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
projednávaným	projednávaný	k2eAgFnPc3d1	projednávaná
změnám	změna	k1gFnPc3	změna
farmaceutické	farmaceutický	k2eAgFnSc2d1	farmaceutická
legislativy	legislativa	k1gFnSc2	legislativa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
tělovýchovy	tělovýchova	k1gFnSc2	tělovýchova
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
pro	pro	k7c4	pro
další	další	k2eAgInPc4d1	další
ústřední	ústřední	k2eAgInPc4d1	ústřední
správní	správní	k2eAgInPc4d1	správní
orgány	orgán	k1gInPc4	orgán
<g/>
.	.	kIx.	.
<g/>
Zásadní	zásadní	k2eAgInSc4d1	zásadní
význam	význam	k1gInSc4	význam
je	být	k5eAaImIp3nS	být
přikládán	přikládat	k5eAaImNgInS	přikládat
i	i	k9	i
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
úřadem	úřad	k1gInSc7	úřad
Zmocněnce	zmocněnec	k1gMnSc2	zmocněnec
pro	pro	k7c4	pro
zastupování	zastupování	k1gNnSc4	zastupování
státu	stát	k1gInSc2	stát
před	před	k7c7	před
Soudním	soudní	k2eAgInSc7d1	soudní
dvorem	dvůr	k1gInSc7	dvůr
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
ESD	ESD	kA	ESD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Organizační	organizační	k2eAgFnSc1d1	organizační
struktura	struktura	k1gFnSc1	struktura
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
areálu	areál	k1gInSc6	areál
Státního	státní	k2eAgInSc2d1	státní
zdravotního	zdravotní	k2eAgInSc2d1	zdravotní
ústavu	ústav	k1gInSc2	ústav
se	se	k3xPyFc4	se
na	na	k7c6	na
adrese	adresa	k1gFnSc6	adresa
Šrobárova	Šrobárův	k2eAgFnSc1d1	Šrobárova
48	[number]	k4	48
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
10	[number]	k4	10
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
centrální	centrální	k2eAgNnSc4d1	centrální
pracoviště	pracoviště	k1gNnSc4	pracoviště
SÚKL	SÚKL	kA	SÚKL
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Regionální	regionální	k2eAgNnSc1d1	regionální
pracoviště	pracoviště	k1gNnSc1	pracoviště
SÚKL	SÚKL	kA	SÚKL
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
kromě	kromě	k7c2	kromě
Prahy	Praha	k1gFnSc2	Praha
také	také	k9	také
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
,	,	kIx,	,
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
Olomouci	Olomouc	k1gFnSc6	Olomouc
a	a	k8xC	a
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Financování	financování	k1gNnSc1	financování
===	===	k?	===
</s>
</p>
<p>
<s>
Činnost	činnost	k1gFnSc1	činnost
SÚKL	SÚKL	kA	SÚKL
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
financován	financovat	k5eAaBmNgInS	financovat
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
vlastní	vlastní	k2eAgFnSc2d1	vlastní
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
náhrad	náhrada	k1gFnPc2	náhrada
výdajů	výdaj	k1gInPc2	výdaj
za	za	k7c2	za
odborné	odborný	k2eAgFnSc2d1	odborná
činnosti	činnost	k1gFnSc2	činnost
SÚKL	SÚKL	kA	SÚKL
získává	získávat	k5eAaImIp3nS	získávat
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
378	[number]	k4	378
<g/>
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
o	o	k7c6	o
léčivech	léčivo	k1gNnPc6	léčivo
<g/>
,	,	kIx,	,
prováděl	provádět	k5eAaImAgMnS	provádět
SÚKL	SÚKL	kA	SÚKL
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
výrobců	výrobce	k1gMnPc2	výrobce
<g/>
,	,	kIx,	,
distributorů	distributor	k1gMnPc2	distributor
<g/>
,	,	kIx,	,
prodejců	prodejce	k1gMnPc2	prodejce
a	a	k8xC	a
jiných	jiný	k2eAgMnPc2d1	jiný
právnických	právnický	k2eAgMnPc2d1	právnický
i	i	k8xC	i
fyzických	fyzický	k2eAgMnPc2d1	fyzický
subjektů	subjekt	k1gInPc2	subjekt
<g/>
.	.	kIx.	.
</s>
<s>
Příjmy	příjem	k1gInPc1	příjem
za	za	k7c4	za
provedené	provedený	k2eAgInPc4d1	provedený
odborné	odborný	k2eAgInPc4d1	odborný
úkony	úkon	k1gInPc4	úkon
SÚKL	SÚKL	kA	SÚKL
postupně	postupně	k6eAd1	postupně
využívá	využívat	k5eAaPmIp3nS	využívat
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
218	[number]	k4	218
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
rozpočtových	rozpočtový	k2eAgNnPc6d1	rozpočtové
pravidlech	pravidlo	k1gNnPc6	pravidlo
<g/>
,	,	kIx,	,
na	na	k7c6	na
financování	financování	k1gNnSc6	financování
výdajů	výdaj	k1gInPc2	výdaj
<g/>
,	,	kIx,	,
nezajištěných	zajištěný	k2eNgFnPc2d1	nezajištěná
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
přídělu	příděl	k1gInSc2	příděl
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
ze	z	k7c2	z
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
financování	financování	k1gNnSc4	financování
mzdových	mzdový	k2eAgInPc2d1	mzdový
<g/>
,	,	kIx,	,
provozních	provozní	k2eAgInPc2d1	provozní
a	a	k8xC	a
investičních	investiční	k2eAgFnPc2d1	investiční
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svojí	svojit	k5eAaImIp3nS	svojit
činností	činnost	k1gFnPc2	činnost
SÚKL	SÚKL	kA	SÚKL
dále	daleko	k6eAd2	daleko
získává	získávat	k5eAaImIp3nS	získávat
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
následně	následně	k6eAd1	následně
odvádí	odvádět	k5eAaImIp3nP	odvádět
do	do	k7c2	do
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
vybíráním	vybírání	k1gNnSc7	vybírání
správních	správní	k2eAgInPc2d1	správní
poplatků	poplatek	k1gInPc2	poplatek
za	za	k7c2	za
podávané	podávaný	k2eAgFnSc2d1	podávaná
žádosti	žádost	k1gFnSc2	žádost
<g/>
,	,	kIx,	,
ostatními	ostatní	k2eAgInPc7d1	ostatní
rozpočtovými	rozpočtový	k2eAgInPc7d1	rozpočtový
příjmy	příjem	k1gInPc7	příjem
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
úroky	úrok	k1gInPc1	úrok
z	z	k7c2	z
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
na	na	k7c6	na
vkladových	vkladový	k2eAgInPc6d1	vkladový
účtech	účet	k1gInPc6	účet
u	u	k7c2	u
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
<g/>
,	,	kIx,	,
příjmy	příjem	k1gInPc4	příjem
za	za	k7c4	za
poskytování	poskytování	k1gNnSc4	poskytování
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
pronájem	pronájem	k1gInSc1	pronájem
aj.	aj.	kA	aj.
V	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
pokuty	pokuta	k1gFnSc2	pokuta
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
SÚKL	SÚKL	kA	SÚKL
ukládá	ukládat	k5eAaImIp3nS	ukládat
za	za	k7c4	za
porušení	porušení	k1gNnSc4	porušení
některých	některý	k3yIgFnPc2	některý
zákonných	zákonný	k2eAgFnPc2d1	zákonná
povinností	povinnost	k1gFnPc2	povinnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Politika	politika	k1gFnSc1	politika
SÚKL	SÚKL	kA	SÚKL
===	===	k?	===
</s>
</p>
<p>
<s>
SÚKL	SÚKL	kA	SÚKL
má	mít	k5eAaImIp3nS	mít
stanovené	stanovený	k2eAgInPc4d1	stanovený
následující	následující	k2eAgInPc4d1	následující
strategické	strategický	k2eAgInPc4d1	strategický
cíle	cíl	k1gInPc4	cíl
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Služby	služba	k1gFnPc1	služba
a	a	k8xC	a
činnosti	činnost	k1gFnPc1	činnost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
humánních	humánní	k2eAgNnPc2d1	humánní
léčiv	léčivo	k1gNnPc2	léčivo
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
v	v	k7c6	v
reálně	reálně	k6eAd1	reálně
nejkratších	krátký	k2eAgInPc6d3	nejkratší
časových	časový	k2eAgInPc6d1	časový
termínech	termín	k1gInPc6	termín
a	a	k8xC	a
bez	bez	k7c2	bez
vytváření	vytváření	k1gNnSc2	vytváření
překážek	překážka	k1gFnPc2	překážka
pro	pro	k7c4	pro
používání	používání	k1gNnSc4	používání
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proaktivně	Proaktivně	k6eAd1	Proaktivně
harmonizuje	harmonizovat	k5eAaBmIp3nS	harmonizovat
nesoulad	nesoulad	k1gInSc1	nesoulad
mezi	mezi	k7c7	mezi
rozdílným	rozdílný	k2eAgNnSc7d1	rozdílné
stanovením	stanovení	k1gNnSc7	stanovení
regulací	regulace	k1gFnPc2	regulace
u	u	k7c2	u
stejných	stejný	k2eAgInPc2d1	stejný
nebo	nebo	k8xC	nebo
zaměnitelných	zaměnitelný	k2eAgInPc2d1	zaměnitelný
předmětů	předmět	k1gInPc2	předmět
regulačního	regulační	k2eAgInSc2d1	regulační
zájmu	zájem	k1gInSc2	zájem
<g/>
,	,	kIx,	,
ať	ať	k9	ať
již	již	k9	již
byly	být	k5eAaImAgFnP	být
stanoveny	stanovit	k5eAaPmNgFnP	stanovit
výhradně	výhradně	k6eAd1	výhradně
ústavem	ústav	k1gInSc7	ústav
nebo	nebo	k8xC	nebo
jinými	jiný	k2eAgInPc7d1	jiný
regulátory	regulátor	k1gInPc7	regulátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
aktivitou	aktivita	k1gFnSc7	aktivita
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dohledu	dohled	k1gInSc2	dohled
nad	nad	k7c7	nad
reklamou	reklama	k1gFnSc7	reklama
a	a	k8xC	a
nelegálním	legální	k2eNgNnSc7d1	nelegální
zacházením	zacházení	k1gNnSc7	zacházení
s	s	k7c7	s
léčivy	léčivo	k1gNnPc7	léčivo
<g/>
,	,	kIx,	,
a	a	k8xC	a
obdobně	obdobně	k6eAd1	obdobně
zvýšeným	zvýšený	k2eAgInSc7d1	zvýšený
zájmem	zájem	k1gInSc7	zájem
o	o	k7c4	o
kvalitu	kvalita	k1gFnSc4	kvalita
a	a	k8xC	a
použitelnost	použitelnost	k1gFnSc4	použitelnost
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
registrovaných	registrovaný	k2eAgInPc6d1	registrovaný
léčivých	léčivý	k2eAgInPc6d1	léčivý
přípravcích	přípravek	k1gInPc6	přípravek
pro	pro	k7c4	pro
pacienty	pacient	k1gMnPc4	pacient
<g/>
,	,	kIx,	,
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
emancipaci	emancipace	k1gFnSc3	emancipace
uživatele	uživatel	k1gMnSc2	uživatel
léčiv	léčivo	k1gNnPc2	léčivo
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
vlastní	vlastní	k2eAgNnSc4d1	vlastní
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
rozhodování	rozhodování	k1gNnSc4	rozhodování
o	o	k7c4	o
užití	užití	k1gNnSc4	užití
léčiv	léčivo	k1gNnPc2	léčivo
na	na	k7c6	na
základě	základ	k1gInSc6	základ
informovanosti	informovanost	k1gFnSc2	informovanost
jak	jak	k8xC	jak
o	o	k7c6	o
účincích	účinek	k1gInPc6	účinek
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
o	o	k7c6	o
rizicích	rizice	k1gFnPc6	rizice
léčby	léčba	k1gFnSc2	léčba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
správu	správa	k1gFnSc4	správa
datového	datový	k2eAgNnSc2d1	datové
úložiště	úložiště	k1gNnSc2	úložiště
pro	pro	k7c4	pro
elektronickou	elektronický	k2eAgFnSc4d1	elektronická
preskripci	preskripce	k1gFnSc4	preskripce
<g/>
,	,	kIx,	,
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
podpory	podpora	k1gFnSc2	podpora
farmakovigilanční	farmakovigilanční	k2eAgFnSc2d1	farmakovigilanční
aktivity	aktivita	k1gFnSc2	aktivita
a	a	k8xC	a
možnosti	možnost	k1gFnSc2	možnost
intervence	intervence	k1gFnSc2	intervence
ústavu	ústav	k1gInSc2	ústav
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
ochrany	ochrana	k1gFnSc2	ochrana
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
a	a	k8xC	a
veřejnosti	veřejnost	k1gFnSc2	veřejnost
před	před	k7c7	před
riziky	riziko	k1gNnPc7	riziko
farmakoterapie	farmakoterapie	k1gFnSc2	farmakoterapie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
efektivnost	efektivnost	k1gFnSc4	efektivnost
systému	systém	k1gInSc2	systém
regulace	regulace	k1gFnSc2	regulace
pravidelným	pravidelný	k2eAgNnSc7d1	pravidelné
hodnocením	hodnocení	k1gNnSc7	hodnocení
ukazatelů	ukazatel	k1gMnPc2	ukazatel
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
aktivit	aktivita	k1gFnPc2	aktivita
útvarů	útvar	k1gInPc2	útvar
SÚKL	SÚKL	kA	SÚKL
a	a	k8xC	a
hodnocení	hodnocení	k1gNnSc4	hodnocení
spokojenosti	spokojenost	k1gFnSc2	spokojenost
jeho	jeho	k3xOp3gMnPc2	jeho
zákazníků	zákazník	k1gMnPc2	zákazník
a	a	k8xC	a
partnerů	partner	k1gMnPc2	partner
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
služby	služba	k1gFnPc4	služba
a	a	k8xC	a
činnosti	činnost	k1gFnPc4	činnost
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
zásadami	zásada	k1gFnPc7	zásada
systému	systém	k1gInSc2	systém
řízení	řízení	k1gNnSc2	řízení
jakosti	jakost	k1gFnSc2	jakost
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
požadavky	požadavek	k1gInPc7	požadavek
normy	norma	k1gFnSc2	norma
ČSN	ČSN	kA	ČSN
EN	EN	kA	EN
ISO	ISO	kA	ISO
9001	[number]	k4	9001
<g/>
:	:	kIx,	:
<g/>
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kontrolních	kontrolní	k2eAgFnPc2d1	kontrolní
laboratoří	laboratoř	k1gFnPc2	laboratoř
podle	podle	k7c2	podle
normy	norma	k1gFnSc2	norma
ČSN	ČSN	kA	ČSN
EN	EN	kA	EN
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
17025	[number]	k4	17025
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
již	již	k6eAd1	již
zavedený	zavedený	k2eAgInSc1d1	zavedený
systém	systém	k1gInSc1	systém
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
informací	informace	k1gFnPc2	informace
podle	podle	k7c2	podle
normy	norma	k1gFnSc2	norma
ČSN	ČSN	kA	ČSN
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
27001	[number]	k4	27001
<g/>
:	:	kIx,	:
<g/>
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
informační	informační	k2eAgFnSc4d1	informační
podporu	podpora	k1gFnSc4	podpora
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
a	a	k8xC	a
veřejnosti	veřejnost	k1gFnSc2	veřejnost
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
odstraňování	odstraňování	k1gNnSc1	odstraňování
neznalosti	neznalost	k1gFnSc2	neznalost
o	o	k7c6	o
lékové	lékový	k2eAgFnSc6d1	léková
politice	politika	k1gFnSc6	politika
a	a	k8xC	a
reálném	reálný	k2eAgInSc6d1	reálný
stavu	stav	k1gInSc6	stav
zacházení	zacházení	k1gNnSc1	zacházení
s	s	k7c7	s
léčivy	léčivo	k1gNnPc7	léčivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proaktivně	Proaktivně	k6eAd1	Proaktivně
prezentuje	prezentovat	k5eAaBmIp3nS	prezentovat
dosažené	dosažený	k2eAgInPc4d1	dosažený
cíle	cíl	k1gInPc4	cíl
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
pozitivní	pozitivní	k2eAgNnSc4d1	pozitivní
vnímání	vnímání	k1gNnSc4	vnímání
SÚKL	SÚKL	kA	SÚKL
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
klíčových	klíčový	k2eAgMnPc2d1	klíčový
účastníků	účastník	k1gMnPc2	účastník
regulace	regulace	k1gFnSc2	regulace
a	a	k8xC	a
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
uznání	uznání	k1gNnSc4	uznání
činnosti	činnost	k1gFnSc2	činnost
SÚKL	SÚKL	kA	SÚKL
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
spolupráce	spolupráce	k1gFnSc2	spolupráce
lékových	lékový	k2eAgFnPc2d1	léková
agentur	agentura	k1gFnPc2	agentura
v	v	k7c6	v
EU	EU	kA	EU
<g/>
,	,	kIx,	,
se	s	k7c7	s
strukturami	struktura	k1gFnPc7	struktura
EU	EU	kA	EU
spojenými	spojený	k2eAgInPc7d1	spojený
s	s	k7c7	s
předsednictvím	předsednictví	k1gNnSc7	předsednictví
ČR	ČR	kA	ČR
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
je	být	k5eAaImIp3nS	být
podporována	podporován	k2eAgFnSc1d1	podporována
aktivní	aktivní	k2eAgFnSc1d1	aktivní
účast	účast	k1gFnSc1	účast
zástupců	zástupce	k1gMnPc2	zástupce
SÚKL	SÚKL	kA	SÚKL
při	při	k7c6	při
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
spolupráci	spolupráce	k1gFnSc6	spolupráce
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Evropského	evropský	k2eAgNnSc2d1	Evropské
společenství	společenství	k1gNnSc2	společenství
i	i	k9	i
v	v	k7c6	v
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
kvalifikaci	kvalifikace	k1gFnSc3	kvalifikace
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
pro	pro	k7c4	pro
zajišťování	zajišťování	k1gNnSc4	zajišťování
expertních	expertní	k2eAgFnPc2d1	expertní
činností	činnost	k1gFnPc2	činnost
<g/>
,	,	kIx,	,
podporuje	podporovat	k5eAaImIp3nS	podporovat
prohlubování	prohlubování	k1gNnSc1	prohlubování
odborné	odborný	k2eAgFnSc2d1	odborná
způsobilosti	způsobilost	k1gFnSc2	způsobilost
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
a	a	k8xC	a
zabezpečování	zabezpečování	k1gNnSc4	zabezpečování
jejich	jejich	k3xOp3gInSc2	jejich
trvalého	trvalý	k2eAgInSc2d1	trvalý
odborného	odborný	k2eAgInSc2d1	odborný
růstu	růst	k1gInSc2	růst
ověřováním	ověřování	k1gNnSc7	ověřování
jejich	jejich	k3xOp3gFnPc2	jejich
potřeb	potřeba	k1gFnPc2	potřeba
a	a	k8xC	a
realizací	realizace	k1gFnPc2	realizace
plánů	plán	k1gInPc2	plán
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Naplňuje	naplňovat	k5eAaImIp3nS	naplňovat
politiku	politika	k1gFnSc4	politika
používáním	používání	k1gNnSc7	používání
vhodných	vhodný	k2eAgInPc2d1	vhodný
motivačních	motivační	k2eAgInPc2d1	motivační
nástrojů	nástroj	k1gInPc2	nástroj
pro	pro	k7c4	pro
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
základním	základní	k2eAgInSc7d1	základní
zdrojem	zdroj	k1gInSc7	zdroj
pro	pro	k7c4	pro
plnění	plnění	k1gNnSc4	plnění
úkolů	úkol	k1gInPc2	úkol
ústavu	ústav	k1gInSc2	ústav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Historie	historie	k1gFnSc1	historie
SÚKL	SÚKL	kA	SÚKL
===	===	k?	===
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
Státního	státní	k2eAgInSc2d1	státní
ústavu	ústav	k1gInSc2	ústav
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
léčiv	léčivo	k1gNnPc2	léčivo
se	se	k3xPyFc4	se
pojí	pojíst	k5eAaPmIp3nS	pojíst
se	s	k7c7	s
začátky	začátek	k1gInPc7	začátek
formování	formování	k1gNnSc2	formování
Československa	Československo	k1gNnSc2	Československo
jako	jako	k8xS	jako
samostatného	samostatný	k2eAgInSc2d1	samostatný
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Bezprostředním	bezprostřední	k2eAgMnSc7d1	bezprostřední
předchůdcem	předchůdce	k1gMnSc7	předchůdce
Státního	státní	k2eAgInSc2d1	státní
ústavu	ústav	k1gInSc2	ústav
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
léčiv	léčivo	k1gNnPc2	léčivo
byl	být	k5eAaImAgMnS	být
Ústav	ústav	k1gInSc4	ústav
pro	pro	k7c4	pro
zkoumání	zkoumání	k1gNnSc4	zkoumání
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
ustaven	ustavit	k5eAaPmNgInS	ustavit
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Laboratoře	laboratoř	k1gFnPc1	laboratoř
tohoto	tento	k3xDgInSc2	tento
ústavu	ústav	k1gInSc2	ústav
byly	být	k5eAaImAgFnP	být
rozmístěny	rozmístěn	k2eAgFnPc1d1	rozmístěna
na	na	k7c6	na
několika	několik	k4yIc6	několik
pracovištích	pracoviště	k1gNnPc6	pracoviště
University	universita	k1gFnSc2	universita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
byly	být	k5eAaImAgFnP	být
laboratoře	laboratoř	k1gFnPc1	laboratoř
ústavu	ústav	k1gInSc2	ústav
soustředěny	soustředit	k5eAaPmNgFnP	soustředit
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
Státního	státní	k2eAgInSc2d1	státní
zdravotního	zdravotní	k2eAgInSc2d1	zdravotní
ústavu	ústav	k1gInSc2	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
v	v	k7c6	v
budovách	budova	k1gFnPc6	budova
24	[number]	k4	24
a	a	k8xC	a
30	[number]	k4	30
<g/>
,	,	kIx,	,
umístěn	umístěn	k2eAgInSc1d1	umístěn
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyhláškou	vyhláška	k1gFnSc7	vyhláška
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
z	z	k7c2	z
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1952	[number]	k4	1952
byl	být	k5eAaImAgInS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
Státní	státní	k2eAgInSc1d1	státní
ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
léčiv	léčivo	k1gNnPc2	léčivo
(	(	kIx(	(
<g/>
SÚKL	SÚKL	kA	SÚKL
<g/>
)	)	kIx)	)
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
tak	tak	k6eAd1	tak
zcela	zcela	k6eAd1	zcela
vyčleněn	vyčlenit	k5eAaPmNgMnS	vyčlenit
z	z	k7c2	z
organizační	organizační	k2eAgFnSc2d1	organizační
struktury	struktura	k1gFnSc2	struktura
Státního	státní	k2eAgInSc2d1	státní
zdravotního	zdravotní	k2eAgInSc2d1	zdravotní
ústavu	ústav	k1gInSc2	ústav
(	(	kIx(	(
<g/>
SZÚ	SZÚ	kA	SZÚ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
byla	být	k5eAaImAgFnS	být
činnost	činnost	k1gFnSc1	činnost
ústavu	ústav	k1gInSc2	ústav
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
i	i	k8xC	i
o	o	k7c4	o
kontrolu	kontrola	k1gFnSc4	kontrola
"	"	kIx"	"
<g/>
zdravotních	zdravotní	k2eAgFnPc2d1	zdravotní
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
léčebných	léčebný	k2eAgFnPc2d1	léčebná
a	a	k8xC	a
diagnostických	diagnostický	k2eAgFnPc2d1	diagnostická
pomůcek	pomůcka	k1gFnPc2	pomůcka
a	a	k8xC	a
desinfekčních	desinfekční	k2eAgInPc2d1	desinfekční
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgInPc2d1	jiný
prostředků	prostředek	k1gInPc2	prostředek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
byla	být	k5eAaImAgFnS	být
všechna	všechen	k3xTgNnPc4	všechen
pracoviště	pracoviště	k1gNnPc4	pracoviště
ústavu	ústav	k1gInSc2	ústav
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
budově	budova	k1gFnSc6	budova
(	(	kIx(	(
<g/>
č.	č.	k?	č.
24	[number]	k4	24
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
byly	být	k5eAaImAgInP	být
k	k	k7c3	k
ústavu	ústav	k1gInSc3	ústav
přičleněny	přičleněn	k2eAgInPc1d1	přičleněn
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
laboratoře	laboratoř	k1gFnSc2	laboratoř
kontroly	kontrola	k1gFnSc2	kontrola
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zaměřené	zaměřený	k2eAgFnSc2d1	zaměřená
na	na	k7c4	na
kontrolu	kontrola	k1gFnSc4	kontrola
provozu	provoz	k1gInSc2	provoz
lékáren	lékárna	k1gFnPc2	lékárna
<g/>
,	,	kIx,	,
kontrolu	kontrola	k1gFnSc4	kontrola
zacházení	zacházení	k1gNnSc2	zacházení
s	s	k7c7	s
léčivými	léčivý	k2eAgInPc7d1	léčivý
přípravky	přípravek	k1gInPc7	přípravek
dodávanými	dodávaný	k2eAgInPc7d1	dodávaný
do	do	k7c2	do
zdravotnických	zdravotnický	k2eAgNnPc2d1	zdravotnické
zařízení	zařízení	k1gNnPc2	zařízení
a	a	k8xC	a
kontrolu	kontrola	k1gFnSc4	kontrola
kvality	kvalita	k1gFnSc2	kvalita
surovin	surovina	k1gFnPc2	surovina
dodávaných	dodávaný	k2eAgInPc2d1	dodávaný
do	do	k7c2	do
lékáren	lékárna	k1gFnPc2	lékárna
distributory	distributor	k1gMnPc4	distributor
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
laboratoře	laboratoř	k1gFnPc1	laboratoř
byly	být	k5eAaImAgFnP	být
dříve	dříve	k6eAd2	dříve
součástí	součást	k1gFnSc7	součást
Lékárenské	lékárenský	k2eAgFnSc2d1	lékárenská
služby	služba	k1gFnSc2	služba
a	a	k8xC	a
působily	působit	k5eAaImAgFnP	působit
regionálně	regionálně	k6eAd1	regionálně
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tehdejších	tehdejší	k2eAgInPc2d1	tehdejší
Krajských	krajský	k2eAgInPc2d1	krajský
ústavů	ústav	k1gInPc2	ústav
národního	národní	k2eAgNnSc2d1	národní
zdraví	zdraví	k1gNnSc2	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
ústav	ústav	k1gInSc1	ústav
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
pracovištěm	pracoviště	k1gNnSc7	pracoviště
posuzujícím	posuzující	k2eAgNnSc7d1	posuzující
přístrojovou	přístrojový	k2eAgFnSc4d1	přístrojová
zdravotnickou	zdravotnický	k2eAgFnSc4d1	zdravotnická
techniku	technika	k1gFnSc4	technika
používanou	používaný	k2eAgFnSc4d1	používaná
ve	v	k7c6	v
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
převzal	převzít	k5eAaPmAgMnS	převzít
i	i	k9	i
agendu	agenda	k1gFnSc4	agenda
schvalování	schvalování	k1gNnSc3	schvalování
zdravotnických	zdravotnický	k2eAgInPc2d1	zdravotnický
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
došlo	dojít	k5eAaPmAgNnS	dojít
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
ministra	ministr	k1gMnSc2	ministr
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
této	tento	k3xDgFnSc2	tento
agendy	agenda	k1gFnSc2	agenda
na	na	k7c4	na
veškerou	veškerý	k3xTgFnSc4	veškerý
schvalovací	schvalovací	k2eAgFnSc1d1	schvalovací
činnost	činnost	k1gFnSc1	činnost
vztahující	vztahující	k2eAgFnSc1d1	vztahující
se	se	k3xPyFc4	se
ke	k	k7c3	k
všem	všecek	k3xTgMnPc3	všecek
typům	typ	k1gInPc3	typ
zdravotnických	zdravotnický	k2eAgInPc2d1	zdravotnický
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Rozsah	rozsah	k1gInSc1	rozsah
činností	činnost	k1gFnPc2	činnost
ústavu	ústav	k1gInSc2	ústav
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
léčiv	léčivo	k1gNnPc2	léčivo
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
upřesněn	upřesnit	k5eAaPmNgInS	upřesnit
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
79	[number]	k4	79
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
léčivech	léčivo	k1gNnPc6	léčivo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
úpravám	úprava	k1gFnPc3	úprava
docházelo	docházet	k5eAaImAgNnS	docházet
průběžnou	průběžný	k2eAgFnSc7d1	průběžná
novelizací	novelizace	k1gFnSc7	novelizace
tohoto	tento	k3xDgInSc2	tento
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zdravotnických	zdravotnický	k2eAgInPc2d1	zdravotnický
prostředků	prostředek	k1gInPc2	prostředek
byl	být	k5eAaImAgInS	být
rozsah	rozsah	k1gInSc1	rozsah
činností	činnost	k1gFnPc2	činnost
ústavu	ústav	k1gInSc2	ústav
upraven	upravit	k5eAaPmNgInS	upravit
zejména	zejména	k9	zejména
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
123	[number]	k4	123
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
zdravotnických	zdravotnický	k2eAgInPc6d1	zdravotnický
prostředcích	prostředek	k1gInPc6	prostředek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
činnost	činnost	k1gFnSc1	činnost
SÚKL	SÚKL	kA	SÚKL
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
soustředil	soustředit	k5eAaPmAgMnS	soustředit
na	na	k7c4	na
zajištění	zajištění	k1gNnSc4	zajištění
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
schvalování	schvalování	k1gNnSc2	schvalování
zdravotnických	zdravotnický	k2eAgInPc2d1	zdravotnický
prostředků	prostředek	k1gInPc2	prostředek
byl	být	k5eAaImAgInS	být
převeden	převést	k5eAaPmNgInS	převést
na	na	k7c4	na
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
138	[number]	k4	138
<g/>
/	/	kIx~	/
<g/>
2002	[number]	k4	2002
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
novelizoval	novelizovat	k5eAaBmAgMnS	novelizovat
zákon	zákon	k1gInSc4	zákon
o	o	k7c4	o
regulaci	regulace	k1gFnSc4	regulace
reklamy	reklama	k1gFnSc2	reklama
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2002	[number]	k4	2002
stal	stát	k5eAaPmAgInS	stát
SÚKL	SÚKL	kA	SÚKL
dozorovým	dozorový	k2eAgInSc7d1	dozorový
orgánem	orgán	k1gInSc7	orgán
pro	pro	k7c4	pro
dohled	dohled	k1gInSc4	dohled
nad	nad	k7c7	nad
reklamou	reklama	k1gFnSc7	reklama
na	na	k7c4	na
humánní	humánní	k2eAgInPc4d1	humánní
léčivé	léčivý	k2eAgInPc4d1	léčivý
přípravky	přípravek	k1gInPc4	přípravek
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
SÚKL	SÚKL	kA	SÚKL
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
48	[number]	k4	48
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
veřejném	veřejný	k2eAgNnSc6d1	veřejné
zdravotním	zdravotní	k2eAgNnSc6d1	zdravotní
pojištění	pojištění	k1gNnSc6	pojištění
<g/>
,	,	kIx,	,
pověřen	pověřen	k2eAgMnSc1d1	pověřen
stanovováním	stanovování	k1gNnSc7	stanovování
maximálních	maximální	k2eAgFnPc2d1	maximální
cen	cena	k1gFnPc2	cena
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
a	a	k8xC	a
stanovováním	stanovování	k1gNnSc7	stanovování
výše	výše	k1gFnSc2	výše
a	a	k8xC	a
podmínek	podmínka	k1gFnPc2	podmínka
úhrady	úhrada	k1gFnSc2	úhrada
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
stejného	stejný	k2eAgNnSc2d1	stejné
data	datum	k1gNnSc2	datum
SÚKL	SÚKL	kA	SÚKL
provádí	provádět	k5eAaImIp3nS	provádět
cenovou	cenový	k2eAgFnSc4d1	cenová
kontrolu	kontrola	k1gFnSc4	kontrola
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
<g/>
,	,	kIx,	,
potravin	potravina	k1gFnPc2	potravina
pro	pro	k7c4	pro
zvláštní	zvláštní	k2eAgInPc4d1	zvláštní
lékařské	lékařský	k2eAgInPc4d1	lékařský
účely	účel	k1gInPc4	účel
a	a	k8xC	a
zdravotnických	zdravotnický	k2eAgInPc2d1	zdravotnický
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1	rozšíření
činností	činnost	k1gFnPc2	činnost
SÚKL	SÚKL	kA	SÚKL
přinesl	přinést	k5eAaPmAgInS	přinést
i	i	k9	i
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
296	[number]	k4	296
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
lidských	lidský	k2eAgFnPc6d1	lidská
tkáních	tkáň	k1gFnPc6	tkáň
a	a	k8xC	a
buňkách	buňka	k1gFnPc6	buňka
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
postavil	postavit	k5eAaPmAgInS	postavit
SÚKL	SÚKL	kA	SÚKL
do	do	k7c2	do
pozice	pozice	k1gFnSc2	pozice
instituce	instituce	k1gFnSc2	instituce
zajišťující	zajišťující	k2eAgInSc4d1	zajišťující
dohled	dohled	k1gInSc4	dohled
nad	nad	k7c7	nad
jakostí	jakost	k1gFnSc7	jakost
a	a	k8xC	a
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
lidských	lidský	k2eAgFnPc2d1	lidská
tkání	tkáň	k1gFnPc2	tkáň
a	a	k8xC	a
buněk	buňka	k1gFnPc2	buňka
určených	určený	k2eAgInPc2d1	určený
k	k	k7c3	k
použití	použití	k1gNnSc3	použití
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgFnSc4d1	poslední
změnu	změna	k1gFnSc4	změna
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
činnosti	činnost	k1gFnSc2	činnost
přinesla	přinést	k5eAaPmAgFnS	přinést
novela	novela	k1gFnSc1	novela
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
167	[number]	k4	167
<g/>
/	/	kIx~	/
<g/>
1998	[number]	k4	1998
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
návykových	návykový	k2eAgFnPc6d1	návyková
látkách	látka	k1gFnPc6	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
dozorové	dozorový	k2eAgFnPc4d1	dozorová
činnosti	činnost	k1gFnPc4	činnost
i	i	k9	i
na	na	k7c4	na
oblast	oblast	k1gFnSc4	oblast
omamných	omamný	k2eAgFnPc2d1	omamná
a	a	k8xC	a
psychotropních	psychotropní	k2eAgFnPc2d1	psychotropní
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ředitelé	ředitel	k1gMnPc1	ředitel
SÚKL	SÚKL	kA	SÚKL
===	===	k?	===
</s>
</p>
<p>
<s>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
–	–	k?	–
prof.	prof.	kA	prof.
PhDr.	PhDr.	kA	PhDr.
PhMr	PhMr	k1gMnSc1	PhMr
<g/>
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
Skarnitzl	Skarnitzl	k1gMnSc1	Skarnitzl
</s>
</p>
<p>
<s>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
–	–	k?	–
Ing.	ing.	kA	ing.
Jan	Jan	k1gMnSc1	Jan
Buriánek	Buriánek	k1gMnSc1	Buriánek
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
</s>
</p>
<p>
<s>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
–	–	k?	–
doc.	doc.	kA	doc.
MUDr.	MUDr.	kA	MUDr.
Jiří	Jiří	k1gMnSc1	Jiří
Elis	Elis	k1gFnSc2	Elis
<g/>
,	,	kIx,	,
DrSc	DrSc	kA	DrSc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
na	na	k7c4	na
přechodné	přechodný	k2eAgNnSc4d1	přechodné
období	období	k1gNnSc4	období
od	od	k7c2	od
31	[number]	k4	31
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
do	do	k7c2	do
25	[number]	k4	25
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1991	[number]	k4	1991
byl	být	k5eAaImAgInS	být
vedením	vedení	k1gNnSc7	vedení
pověřen	pověřit	k5eAaPmNgMnS	pověřit
RNDr.	RNDr.	kA	RNDr.
Jan	Jan	k1gMnSc1	Jan
Mikeska	Mikeska	k1gFnSc1	Mikeska
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
</s>
</p>
<p>
<s>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
–	–	k?	–
doc.	doc.	kA	doc.
RNDr.	RNDr.	kA	RNDr.
Jiří	Jiří	k1gMnSc1	Jiří
Portych	Portych	k1gMnSc1	Portych
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
</s>
</p>
<p>
<s>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
–	–	k?	–
RNDr.	RNDr.	kA	RNDr.
Jan	Jan	k1gMnSc1	Jan
Mikeska	Mikeska	k1gFnSc1	Mikeska
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
</s>
</p>
<p>
<s>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
–	–	k?	–
MUDr.	MUDr.	kA	MUDr.
Milan	Milan	k1gMnSc1	Milan
Šmíd	Šmíd	k1gMnSc1	Šmíd
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
</s>
</p>
<p>
<s>
na	na	k7c4	na
přechodné	přechodný	k2eAgNnSc4d1	přechodné
období	období	k1gNnSc4	období
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
7	[number]	k4	7
2006	[number]	k4	2006
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
vedením	vedení	k1gNnSc7	vedení
pověřen	pověřit	k5eAaPmNgMnS	pověřit
MUDr.	MUDr.	kA	MUDr.
Michal	Michal	k1gMnSc1	Michal
Sojka	Sojka	k1gMnSc1	Sojka
</s>
</p>
<p>
<s>
na	na	k7c4	na
přechodné	přechodný	k2eAgNnSc4d1	přechodné
období	období	k1gNnSc4	období
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
vedením	vedení	k1gNnSc7	vedení
pověřena	pověřit	k5eAaPmNgFnS	pověřit
RNDr.	RNDr.	kA	RNDr.
Jitka	Jitka	k1gFnSc1	Jitka
Šabartová	Šabartová	k1gFnSc1	Šabartová
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
</s>
</p>
<p>
<s>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
–	–	k?	–
PharmDr.	PharmDr.	k1gMnSc1	PharmDr.
Martin	Martin	k1gMnSc1	Martin
Beneš	Beneš	k1gMnSc1	Beneš
</s>
</p>
<p>
<s>
od	od	k7c2	od
2.5	[number]	k4	2.5
<g/>
.2012	.2012	k4	.2012
–	–	k?	–
MUDr.	MUDr.	kA	MUDr.
Pavel	Pavel	k1gMnSc1	Pavel
Březovský	Březovský	k1gMnSc1	Březovský
<g/>
,	,	kIx,	,
MBA	MBA	kA	MBA
</s>
</p>
<p>
<s>
od	od	k7c2	od
10.2	[number]	k4	10.2
<g/>
.2014	.2014	k4	.2014
–	–	k?	–
PharmDr.	PharmDr.	k1gMnSc4	PharmDr.
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Blahuta	Blahut	k1gMnSc4	Blahut
</s>
</p>
<p>
<s>
od	od	k7c2	od
1.3	[number]	k4	1.3
<g/>
.2018	.2018	k4	.2018
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
–	–	k?	–
Mgr.	Mgr.	kA	Mgr.
Irena	Irena	k1gFnSc1	Irena
Storová	Storová	k1gFnSc1	Storová
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2018	[number]	k4	2018
pověřená	pověřený	k2eAgFnSc1d1	pověřená
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2018	[number]	k4	2018
jmenovaná	jmenovaná	k1gFnSc1	jmenovaná
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Léčiva	léčivo	k1gNnPc1	léčivo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Životní	životní	k2eAgInSc1d1	životní
cyklus	cyklus	k1gInSc1	cyklus
léčiva	léčivo	k1gNnSc2	léčivo
===	===	k?	===
</s>
</p>
<p>
<s>
Uvedení	uvedení	k1gNnSc4	uvedení
léčivého	léčivý	k2eAgInSc2d1	léčivý
přípravku	přípravek	k1gInSc2	přípravek
na	na	k7c4	na
trh	trh	k1gInSc4	trh
předchází	předcházet	k5eAaImIp3nS	předcházet
dlouholetý	dlouholetý	k2eAgInSc4d1	dlouholetý
vývoj	vývoj	k1gInSc4	vývoj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
i	i	k9	i
značně	značně	k6eAd1	značně
finančně	finančně	k6eAd1	finančně
náročný	náročný	k2eAgInSc1d1	náročný
<g/>
.	.	kIx.	.
</s>
<s>
Léčivo	léčivo	k1gNnSc1	léčivo
musí	muset	k5eAaImIp3nS	muset
projít	projít	k5eAaPmF	projít
několika	několik	k4yIc2	několik
fázemi	fáze	k1gFnPc7	fáze
klinického	klinický	k2eAgNnSc2d1	klinické
hodnocení	hodnocení	k1gNnSc2	hodnocení
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
stanovení	stanovení	k1gNnSc1	stanovení
nejvhodnějšího	vhodný	k2eAgNnSc2d3	nejvhodnější
dávkování	dávkování	k1gNnSc2	dávkování
pro	pro	k7c4	pro
pacienta	pacient	k1gMnSc4	pacient
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
přípravek	přípravek	k1gInSc1	přípravek
účinný	účinný	k2eAgInSc1d1	účinný
s	s	k7c7	s
minimálním	minimální	k2eAgNnSc7d1	minimální
rizikem	riziko	k1gNnSc7	riziko
nežádoucích	žádoucí	k2eNgInPc2d1	nežádoucí
účinků	účinek	k1gInPc2	účinek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc7d1	další
etapou	etapa	k1gFnSc7	etapa
je	být	k5eAaImIp3nS	být
registrace	registrace	k1gFnSc1	registrace
SÚKL	SÚKL	kA	SÚKL
nebo	nebo	k8xC	nebo
evropské	evropský	k2eAgFnSc2d1	Evropská
lékové	lékový	k2eAgFnSc2d1	léková
agentury	agentura	k1gFnSc2	agentura
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
posuzovány	posuzován	k2eAgInPc1d1	posuzován
předložené	předložený	k2eAgInPc1d1	předložený
výsledky	výsledek	k1gInPc1	výsledek
klinického	klinický	k2eAgNnSc2d1	klinické
hodnocení	hodnocení	k1gNnSc2	hodnocení
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
požadavky	požadavek	k1gInPc4	požadavek
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
registraci	registrace	k1gFnSc6	registrace
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
léčivý	léčivý	k2eAgInSc1d1	léčivý
přípravek	přípravek	k1gInSc1	přípravek
uveden	uvést	k5eAaPmNgInS	uvést
na	na	k7c4	na
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zajímavost	zajímavost	k1gFnSc4	zajímavost
z	z	k7c2	z
10	[number]	k4	10
až	až	k9	až
20	[number]	k4	20
tisíc	tisíc	k4xCgInPc2	tisíc
nově	nově	k6eAd1	nově
vyvinutých	vyvinutý	k2eAgFnPc2d1	vyvinutá
látek	látka	k1gFnPc2	látka
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
5	[number]	k4	5
až	až	k9	až
10	[number]	k4	10
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
fáze	fáze	k1gFnSc2	fáze
klinických	klinický	k2eAgNnPc2d1	klinické
hodnocení	hodnocení	k1gNnPc2	hodnocení
na	na	k7c6	na
člověku	člověk	k1gMnSc6	člověk
a	a	k8xC	a
jen	jen	k6eAd1	jen
jedna	jeden	k4xCgFnSc1	jeden
léčivá	léčivý	k2eAgFnSc1d1	léčivá
látka	látka	k1gFnSc1	látka
je	být	k5eAaImIp3nS	být
následně	následně	k6eAd1	následně
registrována	registrován	k2eAgFnSc1d1	registrována
jako	jako	k8xC	jako
léčivo	léčivo	k1gNnSc1	léčivo
vhodné	vhodný	k2eAgNnSc1d1	vhodné
pro	pro	k7c4	pro
pacienty	pacient	k1gMnPc4	pacient
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
chvíli	chvíle	k1gFnSc6	chvíle
začíná	začínat	k5eAaImIp3nS	začínat
jeho	jeho	k3xOp3gNnSc1	jeho
hodnocení	hodnocení	k1gNnSc1	hodnocení
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Sleduje	sledovat	k5eAaImIp3nS	sledovat
se	se	k3xPyFc4	se
výskyt	výskyt	k1gInSc1	výskyt
nežádoucích	žádoucí	k2eNgInPc2d1	nežádoucí
účinků	účinek	k1gInPc2	účinek
<g/>
,	,	kIx,	,
účinky	účinek	k1gInPc1	účinek
přípravku	přípravek	k1gInSc2	přípravek
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
interakce	interakce	k1gFnSc1	interakce
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
přípravky	přípravek	k1gInPc7	přípravek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
výskytu	výskyt	k1gInSc2	výskyt
neočekávaných	očekávaný	k2eNgInPc2d1	neočekávaný
nežádoucích	žádoucí	k2eNgInPc2d1	nežádoucí
účinků	účinek	k1gInPc2	účinek
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
informace	informace	k1gFnSc1	informace
zpracována	zpracovat	k5eAaPmNgFnS	zpracovat
a	a	k8xC	a
uvedena	uvést	k5eAaPmNgFnS	uvést
v	v	k7c6	v
souhrnné	souhrnný	k2eAgFnSc6d1	souhrnná
informaci	informace	k1gFnSc6	informace
o	o	k7c6	o
přípravku	přípravek	k1gInSc6	přípravek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
závažnější	závažný	k2eAgInPc4d2	závažnější
nežádoucí	žádoucí	k2eNgInPc4d1	nežádoucí
účinky	účinek	k1gInPc4	účinek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
léčivých	léčivý	k2eAgFnPc2d1	léčivá
přípravek	přípravka	k1gFnPc2	přípravka
úplně	úplně	k6eAd1	úplně
stažen	stáhnout	k5eAaPmNgInS	stáhnout
z	z	k7c2	z
oběhu	oběh	k1gInSc2	oběh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Klinické	klinický	k2eAgNnSc1d1	klinické
hodnocení	hodnocení	k1gNnSc1	hodnocení
léčiv	léčivo	k1gNnPc2	léčivo
===	===	k?	===
</s>
</p>
<p>
<s>
Klinický	klinický	k2eAgInSc4d1	klinický
vývoj	vývoj	k1gInSc4	vývoj
zcela	zcela	k6eAd1	zcela
nového	nový	k2eAgInSc2d1	nový
léku	lék	k1gInSc2	lék
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
několika	několik	k4yIc6	několik
fázích	fáze	k1gFnPc6	fáze
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
získat	získat	k5eAaPmF	získat
pro	pro	k7c4	pro
navržený	navržený	k2eAgInSc4d1	navržený
účel	účel	k1gInSc4	účel
použití	použití	k1gNnSc2	použití
léku	lék	k1gInSc2	lék
dostatek	dostatek	k1gInSc1	dostatek
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
vyhodnotit	vyhodnotit	k5eAaPmF	vyhodnotit
poměr	poměr	k1gInSc4	poměr
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gInSc7	jeho
prospěchem	prospěch	k1gInSc7	prospěch
a	a	k8xC	a
riziky	riziko	k1gNnPc7	riziko
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
I.	I.	kA	I.
fáze	fáze	k1gFnSc1	fáze
představuje	představovat	k5eAaImIp3nS	představovat
studie	studie	k1gFnPc4	studie
<g/>
/	/	kIx~	/
<g/>
klinická	klinický	k2eAgNnPc4d1	klinické
hodnocení	hodnocení	k1gNnPc4	hodnocení
nejčastěji	často	k6eAd3	často
prováděná	prováděný	k2eAgNnPc1d1	prováděné
na	na	k7c6	na
zdravých	zdravý	k2eAgMnPc6d1	zdravý
dobrovolnících	dobrovolník	k1gMnPc6	dobrovolník
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
na	na	k7c6	na
zdravých	zdravý	k2eAgMnPc6d1	zdravý
dobrovolnících	dobrovolník	k1gMnPc6	dobrovolník
se	se	k3xPyFc4	se
neprovádí	provádět	k5eNaImIp3nS	provádět
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
podání	podání	k1gNnSc1	podání
látky	látka	k1gFnSc2	látka
zdravému	zdravý	k1gMnSc3	zdravý
člověku	člověk	k1gMnSc3	člověk
vysoce	vysoce	k6eAd1	vysoce
nevhodné	vhodný	k2eNgFnPc1d1	nevhodná
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
cytostatik	cytostatikum	k1gNnPc2	cytostatikum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
I.	I.	kA	I.
fáze	fáze	k1gFnSc1	fáze
se	se	k3xPyFc4	se
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
nová	nový	k2eAgFnSc1d1	nová
látka	látka	k1gFnSc1	látka
lidským	lidský	k2eAgInSc7d1	lidský
organismem	organismus	k1gInSc7	organismus
tolerována	tolerován	k2eAgFnSc1d1	tolerována
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jaký	jaký	k3yRgInSc4	jaký
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gInSc1	její
osud	osud	k1gInSc1	osud
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
zjistit	zjistit	k5eAaPmF	zjistit
vhodné	vhodný	k2eAgNnSc4d1	vhodné
dávkování	dávkování	k1gNnSc4	dávkování
s	s	k7c7	s
minimálními	minimální	k2eAgInPc7d1	minimální
nežádoucími	žádoucí	k2eNgInPc7d1	nežádoucí
účinky	účinek	k1gInPc7	účinek
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
se	se	k3xPyFc4	se
podáváním	podávání	k1gNnSc7	podávání
nízkých	nízký	k2eAgFnPc2d1	nízká
dávek	dávka	k1gFnPc2	dávka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
této	tento	k3xDgFnSc2	tento
fáze	fáze	k1gFnSc2	fáze
tedy	tedy	k9	tedy
není	být	k5eNaImIp3nS	být
hodnocena	hodnotit	k5eAaImNgFnS	hodnotit
účinnost	účinnost	k1gFnSc1	účinnost
léčivého	léčivý	k2eAgInSc2d1	léčivý
přípravku	přípravek	k1gInSc2	přípravek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
II	II	kA	II
<g/>
.	.	kIx.	.
fázi	fáze	k1gFnSc6	fáze
se	se	k3xPyFc4	se
látka	látka	k1gFnSc1	látka
poprvé	poprvé	k6eAd1	poprvé
podává	podávat	k5eAaImIp3nS	podávat
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
indikaci	indikace	k1gFnSc6	indikace
malému	malý	k2eAgInSc3d1	malý
počtu	počet	k1gInSc3	počet
vybraných	vybraný	k2eAgMnPc2d1	vybraný
nemocných	nemocný	k1gMnPc2	nemocný
(	(	kIx(	(
<g/>
desítky	desítka	k1gFnPc1	desítka
až	až	k8xS	až
stovky	stovka	k1gFnPc1	stovka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ověřují	ověřovat	k5eAaImIp3nP	ověřovat
se	se	k3xPyFc4	se
léčebné	léčebný	k2eAgInPc1d1	léčebný
účinky	účinek	k1gInPc1	účinek
na	na	k7c4	na
lidský	lidský	k2eAgInSc4d1	lidský
organismus	organismus	k1gInSc4	organismus
<g/>
,	,	kIx,	,
hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
vhodná	vhodný	k2eAgFnSc1d1	vhodná
dávka	dávka	k1gFnSc1	dávka
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
shromažďují	shromažďovat	k5eAaImIp3nP	shromažďovat
i	i	k9	i
další	další	k2eAgInPc1d1	další
údaje	údaj	k1gInPc1	údaj
<g/>
,	,	kIx,	,
např.	např.	kA	např.
o	o	k7c4	o
chování	chování	k1gNnSc4	chování
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
a	a	k8xC	a
snášenlivost	snášenlivost	k1gFnSc1	snášenlivost
<g/>
.	.	kIx.	.
</s>
<s>
Potvrdí	potvrdit	k5eAaPmIp3nP	potvrdit
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
dobrá	dobrý	k2eAgFnSc1d1	dobrá
účinnost	účinnost	k1gFnSc1	účinnost
a	a	k8xC	a
přijatelně	přijatelně	k6eAd1	přijatelně
nízký	nízký	k2eAgInSc1d1	nízký
výskyt	výskyt	k1gInSc1	výskyt
nežádoucích	žádoucí	k2eNgInPc2d1	nežádoucí
účinků	účinek	k1gInPc2	účinek
<g/>
,	,	kIx,	,
přechází	přecházet	k5eAaImIp3nS	přecházet
se	se	k3xPyFc4	se
do	do	k7c2	do
III	III	kA	III
<g/>
.	.	kIx.	.
fáze	fáze	k1gFnSc1	fáze
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
již	již	k9	již
studie	studie	k1gFnPc1	studie
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
souborem	soubor	k1gInSc7	soubor
testovaných	testovaný	k2eAgFnPc2d1	testovaná
osob	osoba	k1gFnPc2	osoba
(	(	kIx(	(
<g/>
stovky	stovka	k1gFnPc1	stovka
až	až	k9	až
tisíce	tisíc	k4xCgInPc1	tisíc
pacientů	pacient	k1gMnPc2	pacient
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
velkém	velký	k2eAgInSc6d1	velký
počtu	počet	k1gInSc6	počet
pacientů	pacient	k1gMnPc2	pacient
prokázat	prokázat	k5eAaPmF	prokázat
účinnost	účinnost	k1gFnSc4	účinnost
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
upřesní	upřesnit	k5eAaPmIp3nS	upřesnit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
nový	nový	k2eAgInSc1d1	nový
lék	lék	k1gInSc1	lék
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
zvoleného	zvolený	k2eAgNnSc2d1	zvolené
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
,	,	kIx,	,
určené	určený	k2eAgFnPc1d1	určená
skupiny	skupina	k1gFnPc1	skupina
pacientů	pacient	k1gMnPc2	pacient
a	a	k8xC	a
při	při	k7c6	při
zvoleném	zvolený	k2eAgInSc6d1	zvolený
způsobu	způsob	k1gInSc6	způsob
podávání	podávání	k1gNnSc1	podávání
účinný	účinný	k2eAgMnSc1d1	účinný
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnPc1	studie
zároveň	zároveň	k6eAd1	zároveň
poskytnou	poskytnout	k5eAaPmIp3nP	poskytnout
další	další	k2eAgFnPc4d1	další
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
bezpečnosti	bezpečnost	k1gFnSc6	bezpečnost
hodnoceného	hodnocený	k2eAgInSc2d1	hodnocený
přípravku	přípravek	k1gInSc2	přípravek
<g/>
.	.	kIx.	.
</s>
<s>
Projde	projít	k5eAaPmIp3nS	projít
<g/>
-li	i	k?	-li
nový	nový	k2eAgInSc4d1	nový
lék	lék	k1gInSc4	lék
úspěšně	úspěšně	k6eAd1	úspěšně
všemi	všecek	k3xTgFnPc7	všecek
fázemi	fáze	k1gFnPc7	fáze
klinického	klinický	k2eAgNnSc2d1	klinické
hodnocení	hodnocení	k1gNnSc2	hodnocení
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
všechny	všechen	k3xTgInPc4	všechen
výsledky	výsledek	k1gInPc4	výsledek
testování	testování	k1gNnPc2	testování
předložit	předložit	k5eAaPmF	předložit
k	k	k7c3	k
registraci	registrace	k1gFnSc3	registrace
léku	lék	k1gInSc2	lék
státní	státní	k2eAgFnSc7d1	státní
autoritou	autorita	k1gFnSc7	autorita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zaregistrování	zaregistrování	k1gNnSc6	zaregistrování
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
používat	používat	k5eAaImF	používat
přípravek	přípravek	k1gInSc4	přípravek
při	při	k7c6	při
poskytování	poskytování	k1gNnSc6	poskytování
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
péče	péče	k1gFnSc2	péče
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Uvedením	uvedení	k1gNnSc7	uvedení
do	do	k7c2	do
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
péče	péče	k1gFnSc2	péče
však	však	k9	však
sledování	sledování	k1gNnSc4	sledování
nového	nový	k2eAgNnSc2d1	nové
léčiva	léčivo	k1gNnSc2	léčivo
nekončí	končit	k5eNaImIp3nP	končit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
IV	IV	kA	IV
<g/>
.	.	kIx.	.
fázi	fáze	k1gFnSc3	fáze
se	se	k3xPyFc4	se
shromažďují	shromažďovat	k5eAaImIp3nP	shromažďovat
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
výskytu	výskyt	k1gInSc6	výskyt
nežádoucích	žádoucí	k2eNgInPc2d1	nežádoucí
účinků	účinek	k1gInPc2	účinek
<g/>
,	,	kIx,	,
o	o	k7c6	o
účincích	účinek	k1gInPc6	účinek
při	při	k7c6	při
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
podávání	podávání	k1gNnSc6	podávání
<g/>
,	,	kIx,	,
nové	nový	k2eAgFnPc4d1	nová
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
možných	možný	k2eAgFnPc6d1	možná
interakcích	interakce	k1gFnPc6	interakce
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
léky	lék	k1gInPc7	lék
<g/>
,	,	kIx,	,
o	o	k7c6	o
podávání	podávání	k1gNnSc6	podávání
speciálním	speciální	k2eAgNnSc6d1	speciální
skupinám	skupina	k1gFnPc3	skupina
osob	osoba	k1gFnPc2	osoba
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
staří	starý	k2eAgMnPc1d1	starý
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
gravidní	gravidní	k2eAgFnPc1d1	gravidní
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
dialyzovaní	dialyzovaný	k2eAgMnPc1d1	dialyzovaný
<g />
.	.	kIx.	.
</s>
<s>
pacienti	pacient	k1gMnPc1	pacient
apod.	apod.	kA	apod.
V	v	k7c6	v
dlouhodobých	dlouhodobý	k2eAgFnPc6d1	dlouhodobá
studiích	studie	k1gFnPc6	studie
se	se	k3xPyFc4	se
např.	např.	kA	např.
sleduje	sledovat	k5eAaImIp3nS	sledovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
léčivo	léčivo	k1gNnSc1	léčivo
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
mortalitu	mortalita	k1gFnSc4	mortalita
pacientů	pacient	k1gMnPc2	pacient
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
zda	zda	k8xS	zda
jeho	jeho	k3xOp3gNnPc2	jeho
podávání	podávání	k1gNnPc2	podávání
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
jejich	jejich	k3xOp3gInSc4	jejich
život	život	k1gInSc4	život
a	a	k8xC	a
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
jeho	jeho	k3xOp3gFnSc4	jeho
kvalitu	kvalita	k1gFnSc4	kvalita
(	(	kIx(	(
<g/>
či	či	k8xC	či
naopak	naopak	k6eAd1	naopak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Přehled	přehled	k1gInSc4	přehled
schválených	schválený	k2eAgNnPc2d1	schválené
klinických	klinický	k2eAgNnPc2d1	klinické
hodnocení	hodnocení	k1gNnPc2	hodnocení
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
na	na	k7c6	na
internetových	internetový	k2eAgFnPc6d1	internetová
stránkách	stránka	k1gFnPc6	stránka
SÚKL	SÚKL	kA	SÚKL
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Registrace	registrace	k1gFnSc1	registrace
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
===	===	k?	===
</s>
</p>
<p>
<s>
Léčivé	léčivý	k2eAgFnPc1d1	léčivá
přípravky	přípravka	k1gFnPc1	přípravka
musí	muset	k5eAaImIp3nP	muset
před	před	k7c7	před
uvedením	uvedení	k1gNnSc7	uvedení
na	na	k7c4	na
náš	náš	k3xOp1gInSc4	náš
trh	trh	k1gInSc4	trh
projít	projít	k5eAaPmF	projít
schvalovacím	schvalovací	k2eAgNnSc7d1	schvalovací
řízením	řízení	k1gNnSc7	řízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nazýváme	nazývat	k5eAaImIp1nP	nazývat
registrace	registrace	k1gFnPc1	registrace
léčivého	léčivý	k2eAgInSc2d1	léčivý
přípravku	přípravek	k1gInSc2	přípravek
(	(	kIx(	(
<g/>
udělení	udělení	k1gNnSc1	udělení
povolení	povolení	k1gNnPc2	povolení
k	k	k7c3	k
uvedení	uvedení	k1gNnSc3	uvedení
na	na	k7c4	na
trh	trh	k1gInSc4	trh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
postup	postup	k1gInSc4	postup
je	být	k5eAaImIp3nS	být
minimalizace	minimalizace	k1gFnSc1	minimalizace
předvídatelných	předvídatelný	k2eAgNnPc2d1	předvídatelné
rizik	riziko	k1gNnPc2	riziko
spojených	spojený	k2eAgInPc2d1	spojený
s	s	k7c7	s
uvedením	uvedení	k1gNnSc7	uvedení
léčivého	léčivý	k2eAgInSc2d1	léčivý
přípravku	přípravek	k1gInSc2	přípravek
na	na	k7c4	na
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInSc1d1	státní
ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
léčiv	léčivo	k1gNnPc2	léčivo
(	(	kIx(	(
<g/>
SÚKL	SÚKL	kA	SÚKL
<g/>
)	)	kIx)	)
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
Evropská	evropský	k2eAgFnSc1d1	Evropská
léková	lékový	k2eAgFnSc1d1	léková
agentura	agentura	k1gFnSc1	agentura
(	(	kIx(	(
<g/>
EMA	Ema	k1gFnSc1	Ema
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
EU	EU	kA	EU
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
procesu	proces	k1gInSc6	proces
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
doložení	doložení	k1gNnSc1	doložení
účinnosti	účinnost	k1gFnSc2	účinnost
<g/>
,	,	kIx,	,
jakosti	jakost	k1gFnSc2	jakost
a	a	k8xC	a
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
léčivého	léčivý	k2eAgInSc2d1	léčivý
přípravku	přípravek	k1gInSc2	přípravek
<g/>
.	.	kIx.	.
</s>
<s>
Postup	postup	k1gInSc1	postup
je	být	k5eAaImIp3nS	být
založený	založený	k2eAgInSc1d1	založený
na	na	k7c4	na
hodnocení	hodnocení	k1gNnSc4	hodnocení
souladu	soulad	k1gInSc2	soulad
předložené	předložený	k2eAgFnSc2d1	předložená
dokumentace	dokumentace	k1gFnSc2	dokumentace
s	s	k7c7	s
požadavky	požadavek	k1gInPc7	požadavek
právních	právní	k2eAgInPc2d1	právní
předpisů	předpis	k1gInPc2	předpis
a	a	k8xC	a
nejaktuálnějších	aktuální	k2eAgInPc2d3	nejaktuálnější
vědeckých	vědecký	k2eAgInPc2d1	vědecký
poznatků	poznatek	k1gInPc2	poznatek
<g/>
.	.	kIx.	.
</s>
<s>
Výstupem	výstup	k1gInSc7	výstup
je	být	k5eAaImIp3nS	být
hodnotící	hodnotící	k2eAgFnSc1d1	hodnotící
zpráva	zpráva	k1gFnSc1	zpráva
a	a	k8xC	a
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
o	o	k7c6	o
registraci	registrace	k1gFnSc6	registrace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Typy	typ	k1gInPc1	typ
registračních	registrační	k2eAgFnPc2d1	registrační
procedur	procedura	k1gFnPc2	procedura
v	v	k7c6	v
ČR	ČR	kA	ČR
====	====	k?	====
</s>
</p>
<p>
<s>
Národní	národní	k2eAgFnSc1d1	národní
–	–	k?	–
registrace	registrace	k1gFnSc1	registrace
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jenom	jenom	k8xS	jenom
státě	stát	k1gInSc6	stát
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
léčivý	léčivý	k2eAgInSc4d1	léčivý
přípravek	přípravek	k1gInSc4	přípravek
není	být	k5eNaImIp3nS	být
registrován	registrovat	k5eAaBmNgMnS	registrovat
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
zemi	zem	k1gFnSc6	zem
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zakázána	zakázat	k5eAaPmNgFnS	zakázat
pro	pro	k7c4	pro
přípravky	přípravek	k1gInPc4	přípravek
s	s	k7c7	s
povinnou	povinný	k2eAgFnSc7d1	povinná
centralizovanou	centralizovaný	k2eAgFnSc7d1	centralizovaná
registrací	registrace	k1gFnSc7	registrace
a	a	k8xC	a
pro	pro	k7c4	pro
přípravky	přípravek	k1gInPc4	přípravek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
už	už	k6eAd1	už
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
EU	EU	kA	EU
registrovány	registrovat	k5eAaBmNgInP	registrovat
nebo	nebo	k8xC	nebo
u	u	k7c2	u
kterých	který	k3yQgFnPc2	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
registrace	registrace	k1gFnSc1	registrace
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
členském	členský	k2eAgInSc6d1	členský
státě	stát	k1gInSc6	stát
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
procedury	procedura	k1gFnSc2	procedura
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
méně	málo	k6eAd2	málo
používán	používat	k5eAaImNgInS	používat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
uznávání	uznávání	k1gNnSc2	uznávání
–	–	k?	–
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
uznání	uznání	k1gNnSc4	uznání
již	již	k6eAd1	již
existující	existující	k2eAgFnSc2d1	existující
registrace	registrace	k1gFnSc2	registrace
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
členském	členský	k2eAgInSc6d1	členský
státě	stát	k1gInSc6	stát
ostatními	ostatní	k2eAgInPc7d1	ostatní
státy	stát	k1gInPc7	stát
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
je	být	k5eAaImIp3nS	být
přípravek	přípravek	k1gInSc1	přípravek
zaregistrován	zaregistrován	k2eAgInSc1d1	zaregistrován
"	"	kIx"	"
<g/>
národně	národně	k6eAd1	národně
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
referenčním	referenční	k2eAgMnPc3d1	referenční
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgInPc1d1	ostatní
státy	stát	k1gInPc1	stát
Společenství	společenství	k1gNnSc2	společenství
vybrané	vybraná	k1gFnSc2	vybraná
žadatelem	žadatel	k1gMnSc7	žadatel
jsou	být	k5eAaImIp3nP	být
členské	členský	k2eAgInPc1d1	členský
(	(	kIx(	(
<g/>
CMS	CMS	kA	CMS
<g/>
,	,	kIx,	,
concerned	concerned	k1gMnSc1	concerned
member	member	k1gMnSc1	member
state	status	k1gInSc5	status
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Referenční	referenční	k2eAgInSc1d1	referenční
stát	stát	k1gInSc1	stát
vypracuje	vypracovat	k5eAaPmIp3nS	vypracovat
hodnotící	hodnotící	k2eAgFnSc4d1	hodnotící
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgInPc1d1	ostatní
státy	stát	k1gInPc1	stát
ji	on	k3xPp3gFnSc4	on
během	během	k7c2	během
registrační	registrační	k2eAgFnSc2d1	registrační
procedury	procedura	k1gFnSc2	procedura
posoudí	posoudit	k5eAaPmIp3nP	posoudit
a	a	k8xC	a
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
o	o	k7c6	o
kladném	kladný	k2eAgNnSc6d1	kladné
nebo	nebo	k8xC	nebo
záporném	záporný	k2eAgNnSc6d1	záporné
stanovisku	stanovisko	k1gNnSc6	stanovisko
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Decentralizovaná	decentralizovaný	k2eAgFnSc1d1	decentralizovaná
–	–	k?	–
registrace	registrace	k1gFnSc1	registrace
přípravku	přípravek	k1gInSc2	přípravek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ještě	ještě	k6eAd1	ještě
není	být	k5eNaImIp3nS	být
registrován	registrován	k2eAgInSc1d1	registrován
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
členském	členský	k2eAgInSc6d1	členský
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
států	stát	k1gInPc2	stát
je	být	k5eAaImIp3nS	být
žadatelem	žadatel	k1gMnSc7	žadatel
zvolen	zvolen	k2eAgMnSc1d1	zvolen
jako	jako	k8xS	jako
referenční	referenční	k2eAgMnSc1d1	referenční
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgInPc1d1	ostatní
vybrané	vybraný	k2eAgInPc1d1	vybraný
státy	stát	k1gInPc1	stát
Společenství	společenství	k1gNnSc2	společenství
jsou	být	k5eAaImIp3nP	být
členské	členský	k2eAgInPc1d1	členský
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
zainteresované	zainteresovaný	k2eAgInPc1d1	zainteresovaný
státy	stát	k1gInPc1	stát
během	během	k7c2	během
registrační	registrační	k2eAgFnSc2d1	registrační
procedury	procedura	k1gFnSc2	procedura
žádost	žádost	k1gFnSc1	žádost
posoudí	posoudit	k5eAaPmIp3nS	posoudit
a	a	k8xC	a
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
o	o	k7c6	o
kladném	kladný	k2eAgNnSc6d1	kladné
nebo	nebo	k8xC	nebo
záporném	záporný	k2eAgNnSc6d1	záporné
stanovisku	stanovisko	k1gNnSc6	stanovisko
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Centralizovaná	centralizovaný	k2eAgFnSc1d1	centralizovaná
–	–	k?	–
hodnocení	hodnocení	k1gNnSc6	hodnocení
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
Evropská	evropský	k2eAgFnSc1d1	Evropská
léková	lékový	k2eAgFnSc1d1	léková
agentura	agentura	k1gFnSc1	agentura
(	(	kIx(	(
<g/>
EMA	Ema	k1gFnSc1	Ema
<g/>
)	)	kIx)	)
a	a	k8xC	a
registraci	registrace	k1gFnSc4	registrace
uděluje	udělovat	k5eAaImIp3nS	udělovat
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
(	(	kIx(	(
<g/>
EK	EK	kA	EK
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
registrace	registrace	k1gFnSc1	registrace
platí	platit	k5eAaImIp3nS	platit
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
členských	členský	k2eAgInPc6d1	členský
státech	stát	k1gInPc6	stát
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
procedura	procedura	k1gFnSc1	procedura
je	být	k5eAaImIp3nS	být
povinná	povinný	k2eAgFnSc1d1	povinná
pro	pro	k7c4	pro
biotechnologicky	biotechnologicky	k6eAd1	biotechnologicky
připravené	připravený	k2eAgInPc4d1	připravený
přípravky	přípravek	k1gInPc4	přípravek
<g/>
,	,	kIx,	,
nové	nový	k2eAgFnPc4d1	nová
léčivé	léčivý	k2eAgFnPc4d1	léčivá
látky	látka	k1gFnPc4	látka
pro	pro	k7c4	pro
indikace	indikace	k1gFnPc4	indikace
AIDS	aids	k1gInSc4	aids
<g/>
,	,	kIx,	,
onkologická	onkologický	k2eAgNnPc1d1	onkologické
onemocnění	onemocnění	k1gNnPc1	onemocnění
<g/>
,	,	kIx,	,
neurodegenerativní	urodegenerativní	k2eNgNnPc1d1	neurodegenerativní
onemocnění	onemocnění	k1gNnPc1	onemocnění
<g/>
,	,	kIx,	,
diabetes	diabetes	k1gInSc1	diabetes
a	a	k8xC	a
pro	pro	k7c4	pro
přípravky	přípravek	k1gInPc4	přípravek
určené	určený	k2eAgInPc4d1	určený
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
velmi	velmi	k6eAd1	velmi
vzácných	vzácný	k2eAgNnPc2d1	vzácné
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
pouze	pouze	k6eAd1	pouze
omezeného	omezený	k2eAgInSc2d1	omezený
počtu	počet	k1gInSc2	počet
pacientů	pacient	k1gMnPc2	pacient
<g/>
.	.	kIx.	.
<g/>
Registrační	registrační	k2eAgInPc1d1	registrační
postupy	postup	k1gInPc1	postup
v	v	k7c6	v
ČR	ČR	kA	ČR
probíhají	probíhat	k5eAaImIp3nP	probíhat
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
legislativou	legislativa	k1gFnSc7	legislativa
v	v	k7c6	v
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Registrační	registrační	k2eAgInPc1d1	registrační
postupy	postup	k1gInPc1	postup
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
hlavně	hlavně	k9	hlavně
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
registraci	registrace	k1gFnSc4	registrace
přípravku	přípravek	k1gInSc2	přípravek
"	"	kIx"	"
<g/>
originálního	originální	k2eAgInSc2d1	originální
–	–	k?	–
referenčního	referenční	k2eAgInSc2d1	referenční
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
generika	generika	k1gFnSc1	generika
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
levnější	levný	k2eAgFnSc1d2	levnější
alternativa	alternativa	k1gFnSc1	alternativa
originálních	originální	k2eAgInPc2d1	originální
léků	lék	k1gInPc2	lék
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
registrací	registrace	k1gFnSc7	registrace
originálního	originální	k2eAgInSc2d1	originální
přípravku	přípravek	k1gInSc2	přípravek
probíhá	probíhat	k5eAaImIp3nS	probíhat
několikaletý	několikaletý	k2eAgInSc4d1	několikaletý
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc4	jejich
testování	testování	k1gNnPc4	testování
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
klinického	klinický	k2eAgNnSc2d1	klinické
hodnocení	hodnocení	k1gNnSc2	hodnocení
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
generických	generický	k2eAgInPc2d1	generický
přípravků	přípravek	k1gInPc2	přípravek
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
provedeny	provést	k5eAaPmNgInP	provést
všechny	všechen	k3xTgInPc4	všechen
fáze	fáze	k1gFnPc1	fáze
klinických	klinický	k2eAgFnPc2d1	klinická
studií	studie	k1gFnPc2	studie
(	(	kIx(	(
<g/>
od	od	k7c2	od
fáze	fáze	k1gFnSc2	fáze
I.	I.	kA	I.
až	až	k9	až
po	po	k7c6	po
fázi	fáze	k1gFnSc6	fáze
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postačí	postačit	k5eAaPmIp3nS	postačit
provést	provést	k5eAaPmF	provést
pouze	pouze	k6eAd1	pouze
bioekvivalenční	bioekvivalenční	k2eAgFnSc4d1	bioekvivalenční
studii	studie	k1gFnSc4	studie
(	(	kIx(	(
<g/>
provádějí	provádět	k5eAaImIp3nP	provádět
se	se	k3xPyFc4	se
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
prokázat	prokázat	k5eAaPmF	prokázat
porovnáním	porovnání	k1gNnSc7	porovnání
farmakokinetických	farmakokinetický	k2eAgFnPc2d1	farmakokinetická
vlastností	vlastnost	k1gFnPc2	vlastnost
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
jejich	jejich	k3xOp3gFnSc4	jejich
terapeutickou	terapeutický	k2eAgFnSc4d1	terapeutická
srovnatelnost	srovnatelnost	k1gFnSc4	srovnatelnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
registraci	registrace	k1gFnSc4	registrace
tradičních	tradiční	k2eAgInPc2d1	tradiční
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
přípravků	přípravek	k1gInPc2	přípravek
(	(	kIx(	(
<g/>
přípravek	přípravek	k1gInSc1	přípravek
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
minimálně	minimálně	k6eAd1	minimálně
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
15	[number]	k4	15
let	léto	k1gNnPc2	léto
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
EU	EU	kA	EU
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k6eAd1	také
homeopatik	homeopatikum	k1gNnPc2	homeopatikum
je	být	k5eAaImIp3nS	být
používána	používán	k2eAgFnSc1d1	používána
zjednodušená	zjednodušený	k2eAgFnSc1d1	zjednodušená
registrační	registrační	k2eAgFnSc1d1	registrační
procedura	procedura	k1gFnSc1	procedura
(	(	kIx(	(
<g/>
u	u	k7c2	u
homeopatik	homeopatikum	k1gNnPc2	homeopatikum
nejsou	být	k5eNaImIp3nP	být
např.	např.	kA	např.
vyžadovány	vyžadován	k2eAgInPc4d1	vyžadován
důkazy	důkaz	k1gInPc4	důkaz
účinnosti	účinnost	k1gFnSc2	účinnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
registračního	registrační	k2eAgNnSc2d1	registrační
řízení	řízení	k1gNnSc2	řízení
SÚKL	SÚKL	kA	SÚKL
rovněž	rovněž	k9	rovněž
posuzuje	posuzovat	k5eAaImIp3nS	posuzovat
způsob	způsob	k1gInSc1	způsob
výdeje	výdej	k1gInSc2	výdej
léčivého	léčivý	k2eAgInSc2d1	léčivý
přípravku	přípravek	k1gInSc2	přípravek
<g/>
.	.	kIx.	.
</s>
<s>
Kritéria	kritérion	k1gNnPc1	kritérion
braná	braný	k2eAgNnPc1d1	brané
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
při	při	k7c6	při
posuzování	posuzování	k1gNnSc6	posuzování
způsobu	způsob	k1gInSc2	způsob
výdeje	výdej	k1gInSc2	výdej
vycházejí	vycházet	k5eAaImIp3nP	vycházet
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
léčivech	léčivo	k1gNnPc6	léčivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
poregistračních	poregistrační	k2eAgFnPc2d1	poregistrační
aktivit	aktivita	k1gFnPc2	aktivita
SÚKL	SÚKL	kA	SÚKL
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
registraci	registrace	k1gFnSc6	registrace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
upřesnění	upřesnění	k1gNnSc4	upřesnění
způsobu	způsob	k1gInSc2	způsob
uchovávání	uchovávání	k1gNnSc4	uchovávání
<g/>
,	,	kIx,	,
doplnění	doplnění	k1gNnSc4	doplnění
indikace	indikace	k1gFnSc2	indikace
<g/>
,	,	kIx,	,
změnu	změna	k1gFnSc4	změna
držitele	držitel	k1gMnSc2	držitel
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
<g/>
,	,	kIx,	,
změnu	změna	k1gFnSc4	změna
výrobce	výrobce	k1gMnSc2	výrobce
<g/>
,	,	kIx,	,
změnu	změna	k1gFnSc4	změna
vnějšího	vnější	k2eAgInSc2d1	vnější
obalu	obal	k1gInSc2	obal
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prodloužení	prodloužení	k1gNnSc4	prodloužení
registrace	registrace	k1gFnSc2	registrace
<g/>
,	,	kIx,	,
převody	převod	k1gInPc4	převod
registrací	registrace	k1gFnPc2	registrace
<g/>
,	,	kIx,	,
ukončení	ukončení	k1gNnSc4	ukončení
registrace	registrace	k1gFnSc2	registrace
<g/>
,	,	kIx,	,
roční	roční	k2eAgNnSc1d1	roční
přehodnocování	přehodnocování	k1gNnSc1	přehodnocování
<g/>
,	,	kIx,	,
arbitráže	arbitráž	k1gFnPc1	arbitráž
<g/>
.	.	kIx.	.
</s>
<s>
Registrační	registrační	k2eAgInPc1d1	registrační
a	a	k8xC	a
poregistrační	poregistrační	k2eAgInPc1d1	poregistrační
procesy	proces	k1gInPc1	proces
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
a	a	k8xC	a
zdokonalují	zdokonalovat	k5eAaImIp3nP	zdokonalovat
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
vědecko-technický	vědeckoechnický	k2eAgInSc4d1	vědecko-technický
pokrok	pokrok	k1gInSc4	pokrok
a	a	k8xC	a
zajistit	zajistit	k5eAaPmF	zajistit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
léčiva	léčivo	k1gNnSc2	léčivo
legálně	legálně	k6eAd1	legálně
dostupná	dostupný	k2eAgFnSc1d1	dostupná
v	v	k7c6	v
ČR	ČR	kA	ČR
měla	mít	k5eAaImAgFnS	mít
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
úroveň	úroveň	k1gFnSc4	úroveň
jakosti	jakost	k1gFnSc2	jakost
<g/>
,	,	kIx,	,
účinnosti	účinnost	k1gFnSc2	účinnost
a	a	k8xC	a
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Databáze	databáze	k1gFnSc1	databáze
registrovaných	registrovaný	k2eAgInPc2d1	registrovaný
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
na	na	k7c6	na
internetových	internetový	k2eAgFnPc6d1	internetová
stránkách	stránka	k1gFnPc6	stránka
SÚKL	SÚKL	kA	SÚKL
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozdělení	rozdělení	k1gNnSc1	rozdělení
léků	lék	k1gInPc2	lék
podle	podle	k7c2	podle
výdeje	výdej	k1gInSc2	výdej
===	===	k?	===
</s>
</p>
<p>
<s>
Léčivé	léčivý	k2eAgFnPc1d1	léčivá
přípravky	přípravka	k1gFnPc1	přípravka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vázány	vázat	k5eAaImNgFnP	vázat
na	na	k7c4	na
lékařský	lékařský	k2eAgInSc4d1	lékařský
předpis	předpis	k1gInSc4	předpis
–	–	k?	–
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
spadají	spadat	k5eAaPmIp3nP	spadat
přípravky	přípravek	k1gInPc1	přípravek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
představovat	představovat	k5eAaImF	představovat
neúměrné	úměrný	k2eNgNnSc4d1	neúměrné
riziko	riziko	k1gNnSc4	riziko
poškození	poškození	k1gNnSc2	poškození
zdraví	zdraví	k1gNnSc2	zdraví
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
použity	použít	k5eAaPmNgInP	použít
bez	bez	k7c2	bez
dohledu	dohled	k1gInSc2	dohled
lékaře	lékař	k1gMnSc2	lékař
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
používány	používat	k5eAaImNgInP	používat
nesprávně	správně	k6eNd1	správně
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
parenterálně	parenterálně	k6eAd1	parenterálně
(	(	kIx(	(
<g/>
injekčně	injekčně	k6eAd1	injekčně
<g/>
,	,	kIx,	,
infuzně	infuzně	k6eAd1	infuzně
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výdej	výdej	k1gInSc1	výdej
těchto	tento	k3xDgInPc2	tento
přípravků	přípravek	k1gInPc2	přípravek
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
lékárně	lékárna	k1gFnSc6	lékárna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Léčivé	léčivý	k2eAgFnPc1d1	léčivá
přípravky	přípravka	k1gFnPc1	přípravka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
vázány	vázat	k5eAaImNgFnP	vázat
na	na	k7c4	na
recept	recept	k1gInSc4	recept
–	–	k?	–
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
spadají	spadat	k5eAaPmIp3nP	spadat
přípravky	přípravek	k1gInPc1	přípravek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
nízkou	nízký	k2eAgFnSc4d1	nízká
toxicitu	toxicita	k1gFnSc4	toxicita
<g/>
,	,	kIx,	,
nízké	nízký	k2eAgNnSc4d1	nízké
riziko	riziko	k1gNnSc4	riziko
závažných	závažný	k2eAgInPc2d1	závažný
nežádoucích	žádoucí	k2eNgInPc2d1	nežádoucí
účinků	účinek	k1gInPc2	účinek
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
používány	používat	k5eAaImNgInP	používat
u	u	k7c2	u
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
si	se	k3xPyFc3	se
dokáže	dokázat	k5eAaPmIp3nS	dokázat
pacient	pacient	k1gMnSc1	pacient
určit	určit	k5eAaPmF	určit
sám	sám	k3xTgMnSc1	sám
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
samodiagnóza	samodiagnóza	k1gFnSc1	samodiagnóza
<g/>
)	)	kIx)	)
a	a	k8xC	a
riziko	riziko	k1gNnSc4	riziko
ohrožení	ohrožení	k1gNnSc2	ohrožení
zdraví	zdraví	k1gNnSc2	zdraví
při	při	k7c6	při
nesprávném	správný	k2eNgNnSc6d1	nesprávné
užívání	užívání	k1gNnSc6	užívání
přípravku	přípravek	k1gInSc2	přípravek
je	být	k5eAaImIp3nS	být
nízké	nízký	k2eAgNnSc1d1	nízké
<g/>
.	.	kIx.	.
</s>
<s>
Užívání	užívání	k1gNnSc1	užívání
těchto	tento	k3xDgInPc2	tento
přípravků	přípravek	k1gInPc2	přípravek
by	by	kYmCp3nS	by
nemělo	mít	k5eNaImAgNnS	mít
zakrýt	zakrýt	k5eAaPmF	zakrýt
příznaky	příznak	k1gInPc4	příznak
závažnějšího	závažný	k2eAgNnSc2d2	závažnější
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
pacient	pacient	k1gMnSc1	pacient
nerozpozná	rozpoznat	k5eNaPmIp3nS	rozpoznat
a	a	k8xC	a
nevyhledá	vyhledat	k5eNaPmIp3nS	vyhledat
tak	tak	k9	tak
včas	včas	k6eAd1	včas
lékaře	lékař	k1gMnSc4	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Výdej	výdej	k1gInSc1	výdej
těchto	tento	k3xDgInPc2	tento
přípravků	přípravek	k1gInPc2	přípravek
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
v	v	k7c6	v
lékárnách	lékárna	k1gFnPc6	lékárna
nebo	nebo	k8xC	nebo
zásilkovým	zásilkový	k2eAgInSc7d1	zásilkový
prodejem	prodej	k1gInSc7	prodej
přes	přes	k7c4	přes
internet	internet	k1gInSc4	internet
provozovaný	provozovaný	k2eAgInSc4d1	provozovaný
lékárnami	lékárna	k1gFnPc7	lékárna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Léčivé	léčivý	k2eAgFnPc1d1	léčivá
přípravky	přípravka	k1gFnPc1	přípravka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
vázány	vázat	k5eAaImNgFnP	vázat
na	na	k7c4	na
recept	recept	k1gInSc4	recept
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInSc1	jejich
výdej	výdej	k1gInSc1	výdej
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
omezením	omezení	k1gNnSc7	omezení
–	–	k?	–
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
spadají	spadat	k5eAaPmIp3nP	spadat
přípravky	přípravek	k1gInPc1	přípravek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nesplňují	splňovat	k5eNaImIp3nP	splňovat
kritéria	kritérion	k1gNnPc1	kritérion
pro	pro	k7c4	pro
výdej	výdej	k1gInSc4	výdej
na	na	k7c4	na
předpis	předpis	k1gInSc4	předpis
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
přímo	přímo	k6eAd1	přímo
nebo	nebo	k8xC	nebo
nepřímo	přímo	k6eNd1	přímo
představovat	představovat	k5eAaImF	představovat
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
pro	pro	k7c4	pro
zdraví	zdraví	k1gNnSc4	zdraví
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
a	a	k8xC	a
v	v	k7c6	v
širokém	široký	k2eAgInSc6d1	široký
rozsahu	rozsah	k1gInSc6	rozsah
používány	používat	k5eAaImNgFnP	používat
nesprávně	správně	k6eNd1	správně
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gNnSc4	jejich
správné	správný	k2eAgNnSc4d1	správné
použití	použití	k1gNnSc4	použití
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
nezbytně	nezbytně	k6eAd1	nezbytně
nutnou	nutný	k2eAgFnSc4d1	nutná
odbornou	odborný	k2eAgFnSc4d1	odborná
poradu	porada	k1gFnSc4	porada
s	s	k7c7	s
farmaceutem	farmaceut	k1gMnSc7	farmaceut
<g/>
.	.	kIx.	.
</s>
<s>
Výdej	výdej	k1gInSc1	výdej
těchto	tento	k3xDgInPc2	tento
přípravků	přípravek	k1gInPc2	přípravek
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
lékárnách	lékárna	k1gFnPc6	lékárna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyhrazená	vyhrazený	k2eAgNnPc4d1	vyhrazené
léčiva	léčivo	k1gNnPc4	léčivo
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
humánní	humánní	k2eAgInPc4d1	humánní
léčivé	léčivý	k2eAgInPc4d1	léčivý
přípravky	přípravek	k1gInPc4	přípravek
určené	určený	k2eAgInPc4d1	určený
pro	pro	k7c4	pro
jednotlivého	jednotlivý	k2eAgMnSc4d1	jednotlivý
pacienta	pacient	k1gMnSc4	pacient
s	s	k7c7	s
předpokladem	předpoklad	k1gInSc7	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
pokrytí	pokrytí	k1gNnSc3	pokrytí
individuální	individuální	k2eAgFnSc2d1	individuální
potřeby	potřeba	k1gFnSc2	potřeba
pacienta	pacient	k1gMnSc2	pacient
nebo	nebo	k8xC	nebo
jemu	on	k3xPp3gMnSc3	on
blízkých	blízký	k2eAgMnPc2d1	blízký
osob	osoba	k1gFnPc2	osoba
bez	bez	k7c2	bez
odborné	odborný	k2eAgFnSc2d1	odborná
konzultace	konzultace	k1gFnSc2	konzultace
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zvláště	zvláště	k6eAd1	zvláště
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
potřebná	potřebný	k2eAgFnSc1d1	potřebná
akutní	akutní	k2eAgFnSc1d1	akutní
dostupnost	dostupnost	k1gFnSc1	dostupnost
přípravku	přípravek	k1gInSc2	přípravek
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
i	i	k9	i
velikost	velikost	k1gFnSc1	velikost
balení	balení	k1gNnSc2	balení
takového	takový	k3xDgInSc2	takový
přípravku	přípravek	k1gInSc2	přípravek
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Prodej	prodej	k1gInSc1	prodej
těchto	tento	k3xDgInPc2	tento
přípravků	přípravek	k1gInPc2	přípravek
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
i	i	k9	i
mimo	mimo	k7c4	mimo
lékárny	lékárna	k1gFnPc4	lékárna
–	–	k?	–
u	u	k7c2	u
prodejců	prodejce	k1gMnPc2	prodejce
vyhrazených	vyhrazený	k2eAgNnPc2d1	vyhrazené
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Homeopatika	homeopatikum	k1gNnPc4	homeopatikum
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
léčivé	léčivý	k2eAgInPc1d1	léčivý
přípravky	přípravek	k1gInPc1	přípravek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgNnSc4d1	malé
množství	množství	k1gNnSc4	množství
působících	působící	k2eAgFnPc2d1	působící
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
dávkách	dávka	k1gFnPc6	dávka
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
efekt	efekt	k1gInSc1	efekt
podobný	podobný	k2eAgInSc1d1	podobný
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc4	jaký
má	mít	k5eAaImIp3nS	mít
léčená	léčený	k2eAgFnSc1d1	léčená
choroba	choroba	k1gFnSc1	choroba
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
spustí	spustit	k5eAaPmIp3nS	spustit
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
imunitní	imunitní	k2eAgFnSc4d1	imunitní
odezvu	odezva	k1gFnSc4	odezva
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Výdej	výdej	k1gInSc1	výdej
těchto	tento	k3xDgInPc2	tento
přípravků	přípravek	k1gInPc2	přípravek
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
v	v	k7c6	v
lékárnách	lékárna	k1gFnPc6	lékárna
nebo	nebo	k8xC	nebo
zásilkovým	zásilkový	k2eAgInSc7d1	zásilkový
prodejem	prodej	k1gInSc7	prodej
přes	přes	k7c4	přes
internet	internet	k1gInSc4	internet
provozovaný	provozovaný	k2eAgInSc1d1	provozovaný
lékárnami	lékárna	k1gFnPc7	lékárna
v	v	k7c6	v
případě	případ	k1gInSc6	případ
volně	volně	k6eAd1	volně
prodejných	prodejný	k2eAgNnPc2d1	prodejné
homeopatik	homeopatikum	k1gNnPc2	homeopatikum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Neintervenční	intervenční	k2eNgFnPc1d1	neintervenční
poregistrační	poregistrační	k2eAgFnPc1d1	poregistrační
studie	studie	k1gFnPc1	studie
===	===	k?	===
</s>
</p>
<p>
<s>
Držitel	držitel	k1gMnSc1	držitel
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
o	o	k7c4	o
registraci	registrace	k1gFnSc4	registrace
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
léčivech	léčivo	k1gNnPc6	léčivo
povinnost	povinnost	k1gFnSc1	povinnost
hlásit	hlásit	k5eAaImF	hlásit
veškeré	veškerý	k3xTgFnPc4	veškerý
prováděné	prováděný	k2eAgFnPc4d1	prováděná
poregistrační	poregistrační	k2eAgFnPc4d1	poregistrační
studie	studie	k1gFnPc4	studie
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
neintervenční	intervenční	k2eNgFnSc1d1	neintervenční
poregistrační	poregistrační	k2eAgFnSc1d1	poregistrační
studie	studie	k1gFnSc1	studie
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
platností	platnost	k1gFnSc7	platnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Neintervenční	intervenční	k2eNgFnSc1d1	neintervenční
studie	studie	k1gFnSc1	studie
je	být	k5eAaImIp3nS	být
hodnocení	hodnocení	k1gNnSc4	hodnocení
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
léčivé	léčivý	k2eAgFnSc2d1	léčivá
přípravky	přípravka	k1gFnSc2	přípravka
běžným	běžný	k2eAgInSc7d1	běžný
způsobem	způsob	k1gInSc7	způsob
a	a	k8xC	a
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
podmínkami	podmínka	k1gFnPc7	podmínka
jejich	jejich	k3xOp3gFnSc2	jejich
registrace	registrace	k1gFnSc2	registrace
<g/>
.	.	kIx.	.
</s>
<s>
Neintervenční	intervenční	k2eNgFnSc1d1	neintervenční
poregistrační	poregistrační	k2eAgFnSc1d1	poregistrační
studie	studie	k1gFnSc1	studie
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
je	být	k5eAaImIp3nS	být
farmakoepidemiologická	farmakoepidemiologický	k2eAgFnSc1d1	farmakoepidemiologický
studie	studie	k1gFnSc1	studie
prováděná	prováděný	k2eAgFnSc1d1	prováděná
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
o	o	k7c4	o
registraci	registrace	k1gFnSc4	registrace
a	a	k8xC	a
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
identifikace	identifikace	k1gFnSc2	identifikace
nebo	nebo	k8xC	nebo
kvantifikace	kvantifikace	k1gFnSc2	kvantifikace
bezpečnostního	bezpečnostní	k2eAgNnSc2d1	bezpečnostní
rizika	riziko	k1gNnSc2	riziko
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
registrovanému	registrovaný	k2eAgInSc3d1	registrovaný
léčivému	léčivý	k2eAgInSc3d1	léčivý
přípravku	přípravek	k1gInSc3	přípravek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
je	být	k5eAaImIp3nS	být
povinností	povinnost	k1gFnSc7	povinnost
držitele	držitel	k1gMnSc2	držitel
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
o	o	k7c4	o
registraci	registrace	k1gFnSc4	registrace
informovat	informovat	k5eAaBmF	informovat
SÚKL	SÚKL	kA	SÚKL
o	o	k7c6	o
záměru	záměr	k1gInSc6	záměr
provést	provést	k5eAaPmF	provést
neintervenční	intervenční	k2eNgFnSc4d1	neintervenční
poregistrační	poregistrační	k2eAgFnSc4d1	poregistrační
studii	studie	k1gFnSc4	studie
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
poskytne	poskytnout	k5eAaPmIp3nS	poskytnout
následující	následující	k2eAgInPc4d1	následující
údaje	údaj	k1gInPc4	údaj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
jméno	jméno	k1gNnSc1	jméno
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
jména	jméno	k1gNnSc2	jméno
<g/>
,	,	kIx,	,
příjmení	příjmení	k1gNnSc2	příjmení
a	a	k8xC	a
místo	místo	k1gNnSc4	místo
podnikání	podnikání	k1gNnSc2	podnikání
držitele	držitel	k1gMnSc2	držitel
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
o	o	k7c4	o
registraci	registrace	k1gFnSc4	registrace
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
<g/>
-li	i	k?	-li
o	o	k7c4	o
fyzickou	fyzický	k2eAgFnSc4d1	fyzická
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
obchodní	obchodní	k2eAgFnSc4d1	obchodní
firmu	firma	k1gFnSc4	firma
a	a	k8xC	a
sídlo	sídlo	k1gNnSc4	sídlo
držitele	držitel	k1gMnSc2	držitel
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
o	o	k7c4	o
registraci	registrace	k1gFnSc4	registrace
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
<g/>
-li	i	k?	-li
o	o	k7c4	o
právnickou	právnický	k2eAgFnSc4d1	právnická
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
identifikaci	identifikace	k1gFnSc3	identifikace
přípravku	přípravek	k1gInSc2	přípravek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
předmětem	předmět	k1gInSc7	předmět
studie	studie	k1gFnSc2	studie
<g/>
,	,	kIx,	,
kódem	kód	k1gInSc7	kód
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
přidělený	přidělený	k2eAgInSc1d1	přidělený
přípravku	přípravek	k1gInSc3	přípravek
Ústavem	ústav	k1gInSc7	ústav
</s>
</p>
<p>
<s>
název	název	k1gInSc1	název
studie	studie	k1gFnSc2	studie
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
identifikační	identifikační	k2eAgNnSc1d1	identifikační
číslo	číslo	k1gNnSc1	číslo
studie	studie	k1gFnSc2	studie
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterým	který	k3yIgInSc7	který
jsou	být	k5eAaImIp3nP	být
vedeny	vést	k5eAaImNgInP	vést
dokumenty	dokument	k1gInPc1	dokument
držitele	držitel	k1gMnSc4	držitel
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
o	o	k7c6	o
registraci	registrace	k1gFnSc6	registrace
vztahující	vztahující	k2eAgFnSc1d1	vztahující
se	se	k3xPyFc4	se
ke	k	k7c3	k
studii	studie	k1gFnSc3	studie
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
účel	účel	k1gInSc4	účel
<g/>
,	,	kIx,	,
uspořádání	uspořádání	k1gNnSc4	uspořádání
<g/>
,	,	kIx,	,
zaslepení	zaslepení	k1gNnSc4	zaslepení
<g/>
,	,	kIx,	,
rozsah	rozsah	k1gInSc4	rozsah
a	a	k8xC	a
cílové	cílový	k2eAgInPc4d1	cílový
parametry	parametr	k1gInPc4	parametr
studie	studie	k1gFnSc2	studie
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
datum	datum	k1gNnSc1	datum
zahájení	zahájení	k1gNnSc2	zahájení
studie	studie	k1gFnSc2	studie
<g/>
,	,	kIx,	,
předpokládané	předpokládaný	k2eAgNnSc1d1	předpokládané
datum	datum	k1gNnSc1	datum
ukončení	ukončení	k1gNnSc2	ukončení
sběru	sběr	k1gInSc2	sběr
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
dokončení	dokončení	k1gNnSc1	dokončení
analýz	analýza	k1gFnPc2	analýza
a	a	k8xC	a
předání	předání	k1gNnSc2	předání
závěrečné	závěrečný	k2eAgFnSc2d1	závěrečná
zprávy	zpráva	k1gFnSc2	zpráva
<g/>
.	.	kIx.	.
<g/>
Dále	daleko	k6eAd2	daleko
musí	muset	k5eAaImIp3nS	muset
nahlásit	nahlásit	k5eAaPmF	nahlásit
ukončení	ukončení	k1gNnSc4	ukončení
studie	studie	k1gFnSc2	studie
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kterého	který	k3yIgInSc2	který
SÚKL	SÚKL	kA	SÚKL
informuje	informovat	k5eAaBmIp3nS	informovat
o	o	k7c6	o
místu	místo	k1gNnSc3	místo
konání	konání	k1gNnSc2	konání
studie	studie	k1gFnSc2	studie
(	(	kIx(	(
<g/>
jména	jméno	k1gNnSc2	jméno
lékařů	lékař	k1gMnPc2	lékař
zapojených	zapojený	k2eAgMnPc2d1	zapojený
do	do	k7c2	do
studie	studie	k1gFnSc2	studie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c6	o
nákladech	náklad	k1gInPc6	náklad
zkoušejícího	zkoušející	k2eAgMnSc2d1	zkoušející
(	(	kIx(	(
<g/>
lékaře	lékař	k1gMnSc2	lékař
<g/>
)	)	kIx)	)
na	na	k7c6	na
provedení	provedení	k1gNnSc6	provedení
studie	studie	k1gFnSc2	studie
a	a	k8xC	a
předává	předávat	k5eAaImIp3nS	předávat
datum	datum	k1gNnSc1	datum
ukončení	ukončení	k1gNnSc2	ukončení
studie	studie	k1gFnSc2	studie
a	a	k8xC	a
závěrečnou	závěrečný	k2eAgFnSc4d1	závěrečná
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SÚKL	SÚKL	kA	SÚKL
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
pravidelného	pravidelný	k2eAgNnSc2d1	pravidelné
monitorování	monitorování	k1gNnSc2	monitorování
neintervenční	intervenční	k2eNgFnSc2d1	neintervenční
poregistračních	poregistrační	k2eAgNnPc2d1	poregistrační
studií	studio	k1gNnPc2	studio
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
studie	studie	k1gFnSc1	studie
nesloužila	sloužit	k5eNaImAgFnS	sloužit
jako	jako	k9	jako
krytí	krytí	k1gNnSc4	krytí
pro	pro	k7c4	pro
reklamu	reklama	k1gFnSc4	reklama
<g/>
.	.	kIx.	.
</s>
<s>
Prověřuje	prověřovat	k5eAaImIp3nS	prověřovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
nebyl	být	k5eNaImAgInS	být
porušen	porušen	k2eAgInSc1d1	porušen
zákaz	zákaz	k1gInSc1	zákaz
zakotvený	zakotvený	k2eAgInSc1d1	zakotvený
v	v	k7c6	v
zákoně	zákon	k1gInSc6	zákon
o	o	k7c4	o
regulaci	regulace	k1gFnSc4	regulace
reklamy	reklama	k1gFnSc2	reklama
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
držiteli	držitel	k1gMnSc3	držitel
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
o	o	k7c4	o
registraci	registrace	k1gFnSc4	registrace
<g/>
,	,	kIx,	,
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
reklamou	reklama	k1gFnSc7	reklama
na	na	k7c4	na
humánní	humánní	k2eAgInPc4d1	humánní
léčivé	léčivý	k2eAgInPc4d1	léčivý
přípravky	přípravek	k1gInPc4	přípravek
zaměřenou	zaměřený	k2eAgFnSc7d1	zaměřená
na	na	k7c4	na
odborníky	odborník	k1gMnPc4	odborník
<g/>
,	,	kIx,	,
nabízet	nabízet	k5eAaImF	nabízet
<g/>
,	,	kIx,	,
slibovat	slibovat	k5eAaImF	slibovat
nebo	nebo	k8xC	nebo
poskytovat	poskytovat	k5eAaImF	poskytovat
odborníkům	odborník	k1gMnPc3	odborník
(	(	kIx(	(
<g/>
lékařům	lékař	k1gMnPc3	lékař
<g/>
,	,	kIx,	,
farmaceutům	farmaceut	k1gMnPc3	farmaceut
a	a	k8xC	a
jiným	jiný	k2eAgMnPc3d1	jiný
zdravotnickým	zdravotnický	k2eAgMnPc3d1	zdravotnický
pracovníkům	pracovník	k1gMnPc3	pracovník
<g/>
)	)	kIx)	)
dary	dar	k1gInPc4	dar
nebo	nebo	k8xC	nebo
jiný	jiný	k2eAgInSc4d1	jiný
prospěch	prospěch	k1gInSc4	prospěch
<g/>
,	,	kIx,	,
ledaže	ledaže	k8xS	ledaže
jsou	být	k5eAaImIp3nP	být
nepatrné	nepatrný	k2eAgFnPc1d1	nepatrná
hodnoty	hodnota	k1gFnPc1	hodnota
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
jimi	on	k3xPp3gMnPc7	on
vykonávané	vykonávaný	k2eAgFnSc3d1	vykonávaná
odborné	odborný	k2eAgFnSc3d1	odborná
činnosti	činnost	k1gFnSc3	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Přehled	přehled	k1gInSc1	přehled
všech	všecek	k3xTgFnPc2	všecek
nahlášených	nahlášený	k2eAgFnPc2d1	nahlášená
studií	studie	k1gFnPc2	studie
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
na	na	k7c6	na
internetových	internetový	k2eAgFnPc6d1	internetová
stránkách	stránka	k1gFnPc6	stránka
SÚKL	SÚKL	kA	SÚKL
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dostupnost	dostupnost	k1gFnSc4	dostupnost
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
lékárnách	lékárna	k1gFnPc6	lékárna
a	a	k8xC	a
ve	v	k7c6	v
zdravotnických	zdravotnický	k2eAgNnPc6d1	zdravotnické
zařízeních	zařízení	k1gNnPc6	zařízení
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
setkáváme	setkávat	k5eAaImIp1nP	setkávat
s	s	k7c7	s
registrovanými	registrovaný	k2eAgInPc7d1	registrovaný
léčivými	léčivý	k2eAgInPc7d1	léčivý
přípravky	přípravek	k1gInPc7	přípravek
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
s	s	k7c7	s
přípravky	přípravek	k1gInPc7	přípravek
<g/>
,	,	kIx,	,
o	o	k7c6	o
jejichž	jejichž	k3xOyRp3gFnSc6	jejichž
registraci	registrace	k1gFnSc6	registrace
vydá	vydat	k5eAaPmIp3nS	vydat
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
SÚKL	SÚKL	kA	SÚKL
nebo	nebo	k8xC	nebo
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
s	s	k7c7	s
registračním	registrační	k2eAgInSc7d1	registrační
procesem	proces	k1gInSc7	proces
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
finanční	finanční	k2eAgInPc1d1	finanční
náklady	náklad	k1gInPc1	náklad
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
i	i	k9	i
léčivý	léčivý	k2eAgInSc1d1	léčivý
přípravek	přípravek	k1gInSc1	přípravek
je	být	k5eAaImIp3nS	být
druhem	druh	k1gInSc7	druh
zboží	zboží	k1gNnSc2	zboží
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
přinášet	přinášet	k5eAaImF	přinášet
zisk	zisk	k1gInSc4	zisk
<g/>
,	,	kIx,	,
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
farmaceutická	farmaceutický	k2eAgFnSc1d1	farmaceutická
firma	firma	k1gFnSc1	firma
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
pro	pro	k7c4	pro
registraci	registrace	k1gFnSc4	registrace
svého	svůj	k3xOyFgInSc2	svůj
přípravku	přípravek	k1gInSc2	přípravek
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
léčby	léčba	k1gFnSc2	léčba
může	moct	k5eAaImIp3nS	moct
lékař	lékař	k1gMnSc1	lékař
použít	použít	k5eAaPmF	použít
i	i	k9	i
neregistrované	registrovaný	k2eNgInPc4d1	neregistrovaný
léčivé	léčivý	k2eAgInPc4d1	léčivý
přípravky	přípravek	k1gInPc4	přípravek
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
je	být	k5eAaImIp3nS	být
však	však	k9	však
dáno	dán	k2eAgNnSc4d1	dáno
zákonem	zákon	k1gInSc7	zákon
a	a	k8xC	a
pacient	pacient	k1gMnSc1	pacient
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
vždy	vždy	k6eAd1	vždy
informován	informovat	k5eAaBmNgMnS	informovat
lékařem	lékař	k1gMnSc7	lékař
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Léčiva	léčivo	k1gNnPc4	léčivo
pro	pro	k7c4	pro
vzácná	vzácný	k2eAgNnPc4d1	vzácné
onemocnění	onemocnění	k1gNnPc4	onemocnění
–	–	k?	–
"	"	kIx"	"
<g/>
orphans	orphans	k1gInSc1	orphans
<g/>
"	"	kIx"	"
===	===	k?	===
</s>
</p>
<p>
<s>
Označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
vzácná	vzácný	k2eAgNnPc1d1	vzácné
onemocnění	onemocnění	k1gNnPc1	onemocnění
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
rare	rarat	k5eAaPmIp3nS	rarat
diseases	diseases	k1gInSc4	diseases
–	–	k?	–
RD	RD	kA	RD
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
na	na	k7c4	na
skupinu	skupina	k1gFnSc4	skupina
závažných	závažný	k2eAgInPc2d1	závažný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přitom	přitom	k6eAd1	přitom
různorodých	různorodý	k2eAgNnPc2d1	různorodé
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
společným	společný	k2eAgInSc7d1	společný
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
nízký	nízký	k2eAgInSc1d1	nízký
výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
(	(	kIx(	(
<g/>
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
vzácných	vzácný	k2eAgFnPc2d1	vzácná
diagnóz	diagnóza	k1gFnPc2	diagnóza
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
nemocní	mocnit	k5eNaImIp3nS	mocnit
na	na	k7c4	na
10	[number]	k4	10
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
léčivý	léčivý	k2eAgInSc4d1	léčivý
přípravek	přípravek	k1gInSc4	přípravek
není	být	k5eNaImIp3nS	být
registrován	registrovat	k5eAaBmNgMnS	registrovat
v	v	k7c6	v
žádné	žádný	k3yNgFnSc6	žádný
členské	členský	k2eAgFnSc6d1	členská
zemi	zem	k1gFnSc6	zem
EU	EU	kA	EU
<g/>
,	,	kIx,	,
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
léčivý	léčivý	k2eAgInSc4d1	léčivý
přípravek	přípravek	k1gInSc4	přípravek
určený	určený	k2eAgInSc4d1	určený
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
vzácných	vzácný	k2eAgNnPc2d1	vzácné
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
usnadněny	usnadněn	k2eAgInPc1d1	usnadněn
postupy	postup	k1gInPc1	postup
jeho	jeho	k3xOp3gFnSc2	jeho
registrace	registrace	k1gFnSc2	registrace
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
Unii	unie	k1gFnSc4	unie
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
i	i	k9	i
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Administrativní	administrativní	k2eAgInSc1d1	administrativní
proces	proces	k1gInSc1	proces
přiznání	přiznání	k1gNnSc2	přiznání
statutu	statut	k1gInSc2	statut
léčivého	léčivý	k2eAgInSc2d1	léčivý
přípravku	přípravek	k1gInSc2	přípravek
pro	pro	k7c4	pro
vzácná	vzácný	k2eAgNnPc4d1	vzácné
onemocnění	onemocnění	k1gNnPc4	onemocnění
(	(	kIx(	(
<g/>
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
termínu	termín	k1gInSc2	termín
orphan	orphana	k1gFnPc2	orphana
drugs	drugs	k6eAd1	drugs
nebo	nebo	k8xC	nebo
orphan	orphan	k1gMnSc1	orphan
medicines	medicines	k1gMnSc1	medicines
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
proces	proces	k1gInSc1	proces
designace	designace	k1gFnSc2	designace
orphan	orphana	k1gFnPc2	orphana
(	(	kIx(	(
<g/>
designation	designation	k1gInSc1	designation
process	processa	k1gFnPc2	processa
for	forum	k1gNnPc2	forum
orphan	orphan	k1gMnSc1	orphan
medicines	medicines	k1gMnSc1	medicines
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
statutu	statut	k1gInSc2	statut
vyplývají	vyplývat	k5eAaImIp3nP	vyplývat
pro	pro	k7c4	pro
výrobce	výrobce	k1gMnPc4	výrobce
určité	určitý	k2eAgFnSc2d1	určitá
výhody	výhoda	k1gFnSc2	výhoda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
kompenzují	kompenzovat	k5eAaBmIp3nP	kompenzovat
malý	malý	k2eAgInSc4d1	malý
počet	počet	k1gInSc4	počet
pacientů	pacient	k1gMnPc2	pacient
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgInPc4	který
je	být	k5eAaImIp3nS	být
lék	lék	k1gInSc1	lék
určen	určen	k2eAgInSc1d1	určen
(	(	kIx(	(
<g/>
poradenství	poradenství	k1gNnSc1	poradenství
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
žádosti	žádost	k1gFnSc2	žádost
<g/>
,	,	kIx,	,
delší	dlouhý	k2eAgFnSc1d2	delší
exkluzivita	exkluzivita	k1gFnSc1	exkluzivita
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
,	,	kIx,	,
nižší	nízký	k2eAgInPc4d2	nižší
poplatky	poplatek	k1gInPc4	poplatek
za	za	k7c4	za
registraci	registrace	k1gFnSc4	registrace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proces	proces	k1gInSc1	proces
designace	designace	k1gFnSc2	designace
orphan	orphana	k1gFnPc2	orphana
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
lékové	lékový	k2eAgFnSc6d1	léková
agentuře	agentura	k1gFnSc6	agentura
Komise	komise	k1gFnSc2	komise
pro	pro	k7c4	pro
léčivé	léčivý	k2eAgInPc4d1	léčivý
přípravky	přípravek	k1gInPc4	přípravek
pro	pro	k7c4	pro
vzácné	vzácný	k2eAgNnSc4d1	vzácné
použití	použití	k1gNnSc4	použití
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
formuluje	formulovat	k5eAaImIp3nS	formulovat
k	k	k7c3	k
předložené	předložený	k2eAgFnSc3d1	předložená
žádosti	žádost	k1gFnSc3	žádost
své	svůj	k3xOyFgNnSc4	svůj
stanovisko	stanovisko	k1gNnSc4	stanovisko
<g/>
.	.	kIx.	.
</s>
<s>
Konečné	Konečné	k2eAgNnSc1d1	Konečné
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
vydává	vydávat	k5eAaPmIp3nS	vydávat
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Evropskou	evropský	k2eAgFnSc7d1	Evropská
lékovou	lékový	k2eAgFnSc7d1	léková
agenturou	agentura	k1gFnSc7	agentura
(	(	kIx(	(
<g/>
EMA	Ema	k1gFnSc1	Ema
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ceny	cena	k1gFnPc1	cena
a	a	k8xC	a
platby	platba	k1gFnPc1	platba
za	za	k7c4	za
léčiva	léčivo	k1gNnPc4	léčivo
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Regulace	regulace	k1gFnSc1	regulace
cen	cena	k1gFnPc2	cena
léčiv	léčivo	k1gNnPc2	léčivo
v	v	k7c6	v
ČR	ČR	kA	ČR
====	====	k?	====
</s>
</p>
<p>
<s>
Léčivé	léčivý	k2eAgInPc1d1	léčivý
přípravky	přípravek	k1gInPc1	přípravek
hrazené	hrazený	k2eAgInPc1d1	hrazený
ze	z	k7c2	z
zdravotního	zdravotní	k2eAgNnSc2d1	zdravotní
pojištění	pojištění	k1gNnSc2	pojištění
jsou	být	k5eAaImIp3nP	být
regulovány	regulovat	k5eAaImNgFnP	regulovat
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ceny	cena	k1gFnPc1	cena
výrobce	výrobce	k1gMnSc1	výrobce
–	–	k?	–
Je	být	k5eAaImIp3nS	být
regulována	regulován	k2eAgFnSc1d1	regulována
maximální	maximální	k2eAgFnSc1d1	maximální
cena	cena	k1gFnSc1	cena
výrobce	výrobce	k1gMnSc2	výrobce
(	(	kIx(	(
<g/>
výše	vysoce	k6eAd2	vysoce
maximální	maximální	k2eAgFnPc1d1	maximální
ceny	cena	k1gFnPc1	cena
stanovená	stanovený	k2eAgNnPc4d1	stanovené
SÚKL	SÚKL	kA	SÚKL
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
cenu	cena	k1gFnSc4	cena
nesmí	smět	k5eNaImIp3nS	smět
výrobce	výrobce	k1gMnSc1	výrobce
nebo	nebo	k8xC	nebo
dovozce	dovozce	k1gMnSc1	dovozce
překročit	překročit	k5eAaPmF	překročit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
léky	lék	k1gInPc1	lék
podléhají	podléhat	k5eAaImIp3nP	podléhat
ohlašovací	ohlašovací	k2eAgFnSc2d1	ohlašovací
povinnosti	povinnost	k1gFnSc2	povinnost
tzv.	tzv.	kA	tzv.
cen	cena	k1gFnPc2	cena
původce	původce	k1gMnSc1	původce
(	(	kIx(	(
<g/>
před	před	k7c7	před
uvedením	uvedení	k1gNnSc7	uvedení
na	na	k7c4	na
trh	trh	k1gInSc4	trh
držitel	držitel	k1gMnSc1	držitel
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
o	o	k7c4	o
registraci	registrace	k1gFnSc4	registrace
nahlásí	nahlásit	k5eAaPmIp3nS	nahlásit
SÚKL	SÚKL	kA	SÚKL
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
poté	poté	k6eAd1	poté
nesmí	smět	k5eNaImIp3nS	smět
překročit	překročit	k5eAaPmF	překročit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
výše	výše	k1gFnSc1	výše
uvedené	uvedený	k2eAgFnSc2d1	uvedená
ceny	cena	k1gFnSc2	cena
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
překročit	překročit	k5eAaPmF	překročit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
možné	možný	k2eAgNnSc4d1	možné
léčivý	léčivý	k2eAgInSc4d1	léčivý
přípravek	přípravek	k1gInSc4	přípravek
obchodovat	obchodovat	k5eAaImF	obchodovat
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
nižší	nízký	k2eAgFnSc4d2	nižší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
výkonů	výkon	k1gInPc2	výkon
velkoobchodu	velkoobchod	k1gInSc2	velkoobchod
<g/>
/	/	kIx~	/
<g/>
maloobchodu	maloobchod	k1gInSc2	maloobchod
(	(	kIx(	(
<g/>
distributor	distributor	k1gMnSc1	distributor
<g/>
/	/	kIx~	/
<g/>
lékárna	lékárna	k1gFnSc1	lékárna
<g/>
)	)	kIx)	)
–	–	k?	–
Je	být	k5eAaImIp3nS	být
regulována	regulován	k2eAgFnSc1d1	regulována
obchodní	obchodní	k2eAgFnSc1d1	obchodní
přirážka	přirážka	k1gFnSc1	přirážka
(	(	kIx(	(
<g/>
distributor	distributor	k1gMnSc1	distributor
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
lékárnou	lékárna	k1gFnSc7	lékárna
mohou	moct	k5eAaImIp3nP	moct
uplatnit	uplatnit	k5eAaPmF	uplatnit
obchodní	obchodní	k2eAgFnSc4d1	obchodní
přirážku	přirážka	k1gFnSc4	přirážka
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
stanovené	stanovený	k2eAgFnSc6d1	stanovená
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Léčivé	léčivý	k2eAgInPc4d1	léčivý
přípravky	přípravek	k1gInPc4	přípravek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nejsou	být	k5eNaImIp3nP	být
hrazeny	hradit	k5eAaImNgInP	hradit
ze	z	k7c2	z
zdravotního	zdravotní	k2eAgNnSc2d1	zdravotní
pojištění	pojištění	k1gNnSc2	pojištění
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
regulovánu	regulován	k2eAgFnSc4d1	regulována
cenu	cena	k1gFnSc4	cena
ani	ani	k8xC	ani
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
výrobce	výrobce	k1gMnSc1	výrobce
(	(	kIx(	(
<g/>
cena	cena	k1gFnSc1	cena
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yRgFnSc4	který
je	být	k5eAaImIp3nS	být
přípravek	přípravek	k1gInSc1	přípravek
uváděn	uvádět	k5eAaImNgInS	uvádět
na	na	k7c4	na
trh	trh	k1gInSc4	trh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
obchodní	obchodní	k2eAgFnSc2d1	obchodní
přirážky	přirážka	k1gFnSc2	přirážka
za	za	k7c4	za
výkony	výkon	k1gInPc4	výkon
velkoobchodu	velkoobchod	k1gInSc2	velkoobchod
<g/>
/	/	kIx~	/
<g/>
maloobchodu	maloobchod	k1gInSc2	maloobchod
(	(	kIx(	(
<g/>
distributor	distributor	k1gMnSc1	distributor
<g/>
/	/	kIx~	/
<g/>
lékárna	lékárna	k1gFnSc1	lékárna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
volně	volně	k6eAd1	volně
prodejných	prodejný	k2eAgNnPc2d1	prodejné
léčiv	léčivo	k1gNnPc2	léčivo
není	být	k5eNaImIp3nS	být
hrazena	hradit	k5eAaImNgFnS	hradit
ze	z	k7c2	z
zdravotního	zdravotní	k2eAgNnSc2d1	zdravotní
pojištění	pojištění	k1gNnSc2	pojištění
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
nepodléhají	podléhat	k5eNaImIp3nP	podléhat
výše	vysoce	k6eAd2	vysoce
popsané	popsaný	k2eAgFnSc3d1	popsaná
regulaci	regulace	k1gFnSc3	regulace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Stanovení	stanovení	k1gNnSc1	stanovení
ceny	cena	k1gFnSc2	cena
====	====	k?	====
</s>
</p>
<p>
<s>
Maximální	maximální	k2eAgFnSc4d1	maximální
cenu	cena	k1gFnSc4	cena
výrobce	výrobce	k1gMnSc2	výrobce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
výpočet	výpočet	k1gInSc4	výpočet
prodejní	prodejní	k2eAgFnSc2d1	prodejní
ceny	cena	k1gFnSc2	cena
<g/>
,	,	kIx,	,
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2008	[number]	k4	2008
SÚKL	SÚKL	kA	SÚKL
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
pro	pro	k7c4	pro
stanovení	stanovení	k1gNnSc4	stanovení
maximální	maximální	k2eAgFnSc2d1	maximální
ceny	cena	k1gFnSc2	cena
výrobce	výrobce	k1gMnSc2	výrobce
jsou	být	k5eAaImIp3nP	být
jasně	jasně	k6eAd1	jasně
vymezena	vymezit	k5eAaPmNgFnS	vymezit
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
cena	cena	k1gFnSc1	cena
výrobce	výrobce	k1gMnSc2	výrobce
se	se	k3xPyFc4	se
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
•	•	k?	•
průměru	průměr	k1gInSc2	průměr
výrobních	výrobní	k2eAgFnPc2d1	výrobní
cen	cena	k1gFnPc2	cena
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
referenčního	referenční	k2eAgInSc2d1	referenční
koše	koš	k1gInSc2	koš
(	(	kIx(	(
<g/>
8	[number]	k4	8
zemí	zem	k1gFnPc2	zem
EU	EU	kA	EU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
•	•	k?	•
průměru	průměr	k1gInSc2	průměr
tří	tři	k4xCgFnPc2	tři
nejnižších	nízký	k2eAgFnPc2d3	nejnižší
výrobních	výrobní	k2eAgFnPc2d1	výrobní
cen	cena	k1gFnPc2	cena
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
EU	EU	kA	EU
nebo	nebo	k8xC	nebo
</s>
</p>
<p>
<s>
•	•	k?	•
výrobní	výrobní	k2eAgFnSc2d1	výrobní
ceny	cena	k1gFnSc2	cena
nejbližšího	blízký	k2eAgMnSc2d3	nejbližší
terapeuticky	terapeuticky	k6eAd1	terapeuticky
porovnatelného	porovnatelný	k2eAgInSc2d1	porovnatelný
přípravku	přípravek	k1gInSc2	přípravek
v	v	k7c6	v
ČR	ČR	kA	ČR
nebo	nebo	k8xC	nebo
v	v	k7c6	v
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úhradu	úhrada	k1gFnSc4	úhrada
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
část	část	k1gFnSc1	část
ceny	cena	k1gFnSc2	cena
pro	pro	k7c4	pro
konečného	konečný	k2eAgMnSc4d1	konečný
spotřebitele	spotřebitel	k1gMnSc4	spotřebitel
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zaplatí	zaplatit	k5eAaPmIp3nS	zaplatit
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
pojišťovna	pojišťovna	k1gFnSc1	pojišťovna
<g/>
,	,	kIx,	,
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2008	[number]	k4	2008
SUKL	SUKL	kA	SUKL
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
stanovení	stanovení	k1gNnSc2	stanovení
úhrady	úhrada	k1gFnSc2	úhrada
léku	lék	k1gInSc2	lék
je	být	k5eAaImIp3nS	být
popsán	popsat	k5eAaPmNgInS	popsat
v	v	k7c6	v
zákoně	zákon	k1gInSc6	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Stanovení	stanovení	k1gNnSc1	stanovení
úhrady	úhrada	k1gFnSc2	úhrada
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
správním	správní	k2eAgNnSc6d1	správní
řízení	řízení	k1gNnSc6	řízení
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yIgInSc2	který
může	moct	k5eAaImIp3nS	moct
vstupovat	vstupovat	k5eAaImF	vstupovat
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
pojišťovna	pojišťovna	k1gFnSc1	pojišťovna
a	a	k8xC	a
výrobce	výrobce	k1gMnSc1	výrobce
léku	lék	k1gInSc2	lék
<g/>
.	.	kIx.	.
</s>
<s>
Metodiky	metodika	k1gFnPc1	metodika
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
kterých	který	k3yIgMnPc2	který
jsou	být	k5eAaImIp3nP	být
vypočítávány	vypočítáván	k2eAgFnPc1d1	vypočítávána
maximální	maximální	k2eAgFnPc1d1	maximální
ceny	cena	k1gFnPc1	cena
výrobce	výrobce	k1gMnSc2	výrobce
a	a	k8xC	a
úhrady	úhrada	k1gFnSc2	úhrada
zdravotní	zdravotní	k2eAgFnSc7d1	zdravotní
pojišťovnou	pojišťovna	k1gFnSc7	pojišťovna
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
na	na	k7c6	na
internetových	internetový	k2eAgFnPc6d1	internetová
stránkách	stránka	k1gFnPc6	stránka
SÚKL	SÚKL	kA	SÚKL
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Rozdíl	rozdíl	k1gInSc1	rozdíl
v	v	k7c6	v
ceně	cena	k1gFnSc6	cena
mezi	mezi	k7c7	mezi
originálním	originální	k2eAgInSc7d1	originální
lékem	lék	k1gInSc7	lék
a	a	k8xC	a
generikem	generik	k1gMnSc7	generik
====	====	k?	====
</s>
</p>
<p>
<s>
Originální	originální	k2eAgFnPc1d1	originální
léčivé	léčivý	k2eAgFnPc1d1	léčivá
přípravky	přípravka	k1gFnPc1	přípravka
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
vývoj	vývoj	k1gInSc1	vývoj
trvá	trvat	k5eAaImIp3nS	trvat
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
pro	pro	k7c4	pro
registraci	registrace	k1gFnSc4	registrace
(	(	kIx(	(
<g/>
schválení	schválení	k1gNnSc4	schválení
<g/>
)	)	kIx)	)
doložit	doložit	k5eAaPmF	doložit
účinnost	účinnost	k1gFnSc4	účinnost
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
léčiva	léčivo	k1gNnSc2	léčivo
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
pouze	pouze	k6eAd1	pouze
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
a	a	k8xC	a
finančně	finančně	k6eAd1	finančně
náročnou	náročný	k2eAgFnSc7d1	náročná
klinickou	klinický	k2eAgFnSc7d1	klinická
studií	studie	k1gFnSc7	studie
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
registrace	registrace	k1gFnSc2	registrace
generik	generika	k1gFnPc2	generika
je	být	k5eAaImIp3nS	být
předložena	předložit	k5eAaPmNgFnS	předložit
"	"	kIx"	"
<g/>
pouze	pouze	k6eAd1	pouze
<g/>
"	"	kIx"	"
žádost	žádost	k1gFnSc1	žádost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
klinickou	klinický	k2eAgFnSc4d1	klinická
studii	studie	k1gFnSc4	studie
provedenou	provedený	k2eAgFnSc4d1	provedená
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
registrace	registrace	k1gFnSc2	registrace
nějakého	nějaký	k3yIgNnSc2	nějaký
originálního	originální	k2eAgNnSc2d1	originální
léčiva	léčivo	k1gNnSc2	léčivo
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
předložena	předložit	k5eAaPmNgFnS	předložit
většinou	většina	k1gFnSc7	většina
jen	jen	k9	jen
studie	studie	k1gFnSc1	studie
bioekvivalence	bioekvivalence	k1gFnSc1	bioekvivalence
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
popisu	popis	k1gInSc2	popis
je	být	k5eAaImIp3nS	být
jednoznačné	jednoznačný	k2eAgNnSc1d1	jednoznačné
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
registraci	registrace	k1gFnSc4	registrace
originálu	originál	k1gInSc2	originál
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nutné	nutný	k2eAgNnSc1d1	nutné
vynaložit	vynaložit	k5eAaPmF	vynaložit
větší	veliký	k2eAgInPc4d2	veliký
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
však	však	k9	však
poté	poté	k6eAd1	poté
odrazí	odrazit	k5eAaPmIp3nS	odrazit
i	i	k9	i
v	v	k7c6	v
prodejní	prodejní	k2eAgFnSc6d1	prodejní
ceně	cena	k1gFnSc6	cena
léčiva	léčivo	k1gNnSc2	léčivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Léky	lék	k1gInPc4	lék
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
===	===	k?	===
</s>
</p>
<p>
<s>
Používání	používání	k1gNnSc1	používání
každého	každý	k3xTgInSc2	každý
léku	lék	k1gInSc2	lék
s	s	k7c7	s
sebou	se	k3xPyFc7	se
nese	nést	k5eAaImIp3nS	nést
možná	možný	k2eAgNnPc4d1	možné
rizika	riziko	k1gNnPc4	riziko
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
schvalování	schvalování	k1gNnSc6	schvalování
léku	lék	k1gInSc2	lék
se	se	k3xPyFc4	se
posuzuje	posuzovat	k5eAaImIp3nS	posuzovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
přínos	přínos	k1gInSc1	přínos
z	z	k7c2	z
léčby	léčba	k1gFnSc2	léčba
převažuje	převažovat	k5eAaImIp3nS	převažovat
nad	nad	k7c7	nad
případnými	případný	k2eAgNnPc7d1	případné
riziky	riziko	k1gNnPc7	riziko
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
rizikům	riziko	k1gNnPc3	riziko
se	se	k3xPyFc4	se
předchází	předcházet	k5eAaImIp3nS	předcházet
registračním	registrační	k2eAgInSc7d1	registrační
procesem	proces	k1gInSc7	proces
a	a	k8xC	a
kontrolou	kontrola	k1gFnSc7	kontrola
kvality	kvalita	k1gFnSc2	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
negativního	negativní	k2eAgInSc2d1	negativní
vlivu	vliv	k1gInSc2	vliv
léčiv	léčivo	k1gNnPc2	léčivo
jsou	být	k5eAaImIp3nP	být
chyby	chyba	k1gFnPc1	chyba
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
používání	používání	k1gNnSc6	používání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
výskytu	výskyt	k1gInSc2	výskyt
závažných	závažný	k2eAgInPc2d1	závažný
nebo	nebo	k8xC	nebo
neočekávaných	očekávaný	k2eNgInPc2d1	neočekávaný
nežádoucích	žádoucí	k2eNgInPc2d1	nežádoucí
účinků	účinek	k1gInPc2	účinek
musí	muset	k5eAaImIp3nS	muset
lékař	lékař	k1gMnSc1	lékař
nebo	nebo	k8xC	nebo
jiný	jiný	k2eAgMnSc1d1	jiný
zdravotnický	zdravotnický	k2eAgMnSc1d1	zdravotnický
pracovník	pracovník	k1gMnSc1	pracovník
hlásit	hlásit	k5eAaImF	hlásit
tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
do	do	k7c2	do
15	[number]	k4	15
dnů	den	k1gInPc2	den
na	na	k7c6	na
SÚKL	SÚKL	kA	SÚKL
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
všechny	všechen	k3xTgFnPc1	všechen
farmaceutické	farmaceutický	k2eAgFnPc1d1	farmaceutická
společnosti	společnost	k1gFnPc1	společnost
musí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
svůj	svůj	k3xOyFgInSc4	svůj
systém	systém	k1gInSc4	systém
sledování	sledování	k1gNnSc4	sledování
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
léků	lék	k1gInPc2	lék
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
přípravek	přípravek	k1gInSc4	přípravek
je	být	k5eAaImIp3nS	být
minimálně	minimálně	k6eAd1	minimálně
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
5	[number]	k4	5
let	léto	k1gNnPc2	léto
držitelem	držitel	k1gMnSc7	držitel
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
o	o	k7c4	o
registraci	registrace	k1gFnSc4	registrace
vypracován	vypracován	k2eAgInSc4d1	vypracován
a	a	k8xC	a
předložen	předložen	k2eAgInSc4d1	předložen
SÚKL	SÚKL	kA	SÚKL
podrobný	podrobný	k2eAgInSc4d1	podrobný
rozbor	rozbor	k1gInSc4	rozbor
jeho	jeho	k3xOp3gFnSc2	jeho
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
Každý	každý	k3xTgInSc1	každý
léčivý	léčivý	k2eAgInSc1d1	léčivý
přípravek	přípravek	k1gInSc1	přípravek
je	být	k5eAaImIp3nS	být
provázen	provázet	k5eAaImNgInS	provázet
schválenými	schválený	k2eAgInPc7d1	schválený
podrobnými	podrobný	k2eAgInPc7d1	podrobný
texty	text	k1gInPc7	text
(	(	kIx(	(
<g/>
Příbalový	Příbalový	k2eAgInSc1d1	Příbalový
leták	leták	k1gInSc1	leták
a	a	k8xC	a
Souhrn	souhrn	k1gInSc1	souhrn
údajů	údaj	k1gInPc2	údaj
o	o	k7c6	o
přípravku	přípravek	k1gInSc6	přípravek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
důležité	důležitý	k2eAgFnPc1d1	důležitá
informace	informace	k1gFnPc1	informace
pro	pro	k7c4	pro
správné	správný	k2eAgNnSc4d1	správné
a	a	k8xC	a
bezpečné	bezpečný	k2eAgNnSc4d1	bezpečné
používání	používání	k1gNnSc4	používání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Farmakovigilance	Farmakovigilanka	k1gFnSc6	Farmakovigilanka
====	====	k?	====
</s>
</p>
<p>
<s>
Farmakovigilance	Farmakovigilance	k1gFnSc1	Farmakovigilance
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
léková	lékový	k2eAgFnSc1d1	léková
bdělost	bdělost	k1gFnSc1	bdělost
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc4	systém
pro	pro	k7c4	pro
sledování	sledování	k1gNnSc4	sledování
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
při	při	k7c6	při
běžném	běžný	k2eAgNnSc6d1	běžné
používání	používání	k1gNnSc6	používání
(	(	kIx(	(
<g/>
v	v	k7c6	v
klinické	klinický	k2eAgFnSc6d1	klinická
praxi	praxe	k1gFnSc6	praxe
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
systém	systém	k1gInSc1	systém
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
na	na	k7c4	na
sběr	sběr	k1gInSc4	sběr
a	a	k8xC	a
vyhodnocování	vyhodnocování	k1gNnSc4	vyhodnocování
relevantních	relevantní	k2eAgFnPc2d1	relevantní
informací	informace	k1gFnPc2	informace
a	a	k8xC	a
prosazení	prosazení	k1gNnSc4	prosazení
přiměřených	přiměřený	k2eAgInPc2d1	přiměřený
zásahů	zásah	k1gInPc2	zásah
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
sběru	sběr	k1gInSc2	sběr
těchto	tento	k3xDgNnPc2	tento
dat	datum	k1gNnPc2	datum
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
hlášených	hlášený	k2eAgInPc2d1	hlášený
nežádoucích	žádoucí	k2eNgInPc2d1	nežádoucí
účinků	účinek	k1gInPc2	účinek
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
či	či	k8xC	či
rozšíření	rozšíření	k1gNnSc3	rozšíření
indikace	indikace	k1gFnSc2	indikace
či	či	k8xC	či
kontraindikace	kontraindikace	k1gFnSc2	kontraindikace
léčiva	léčivo	k1gNnSc2	léčivo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejzávažnějších	závažný	k2eAgInPc6d3	nejzávažnější
případech	případ	k1gInPc6	případ
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
léčivo	léčivo	k1gNnSc1	léčivo
úplně	úplně	k6eAd1	úplně
staženo	stáhnout	k5eAaPmNgNnS	stáhnout
z	z	k7c2	z
trhu	trh	k1gInSc2	trh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zákonná	zákonný	k2eAgFnSc1d1	zákonná
povinnost	povinnost	k1gFnSc1	povinnost
hlásit	hlásit	k5eAaImF	hlásit
nežádoucí	žádoucí	k2eNgInPc4d1	nežádoucí
účinky	účinek	k1gInPc4	účinek
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
na	na	k7c4	na
všechna	všechen	k3xTgNnPc4	všechen
podezření	podezření	k1gNnSc4	podezření
ze	z	k7c2	z
závažného	závažný	k2eAgInSc2d1	závažný
či	či	k8xC	či
neočekávaného	očekávaný	k2eNgInSc2d1	neočekávaný
nežádoucího	žádoucí	k2eNgInSc2d1	nežádoucí
účinku	účinek	k1gInSc2	účinek
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnPc1d1	jiná
skutečnosti	skutečnost	k1gFnPc1	skutečnost
závažné	závažný	k2eAgFnPc1d1	závažná
pro	pro	k7c4	pro
zdraví	zdraví	k1gNnSc4	zdraví
léčených	léčený	k2eAgFnPc2d1	léčená
osob	osoba	k1gFnPc2	osoba
související	související	k2eAgFnSc1d1	související
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
léčivého	léčivý	k2eAgInSc2d1	léčivý
přípravku	přípravek	k1gInSc2	přípravek
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
povinnost	povinnost	k1gFnSc4	povinnost
mají	mít	k5eAaImIp3nP	mít
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
lékaři	lékař	k1gMnSc3	lékař
<g/>
,	,	kIx,	,
lékárníci	lékárník	k1gMnPc1	lékárník
<g/>
,	,	kIx,	,
držitelé	držitel	k1gMnPc1	držitel
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
o	o	k7c4	o
registraci	registrace	k1gFnSc4	registrace
a	a	k8xC	a
provozovatelé	provozovatel	k1gMnPc1	provozovatel
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
byl	být	k5eAaImAgMnS	být
pacient	pacient	k1gMnSc1	pacient
léčen	léčen	k2eAgMnSc1d1	léčen
více	hodně	k6eAd2	hodně
léčivými	léčivý	k2eAgInPc7d1	léčivý
přípravky	přípravek	k1gInPc7	přípravek
najednou	najednou	k6eAd1	najednou
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
podezřelé	podezřelý	k2eAgInPc1d1	podezřelý
všechny	všechen	k3xTgInPc4	všechen
podávané	podávaný	k2eAgInPc4d1	podávaný
přípravky	přípravek	k1gInPc4	přípravek
stejným	stejný	k2eAgInSc7d1	stejný
dílem	díl	k1gInSc7	díl
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
existovat	existovat	k5eAaImF	existovat
důvody	důvod	k1gInPc1	důvod
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
lékař	lékař	k1gMnSc1	lékař
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kauzalita	kauzalita	k1gFnSc1	kauzalita
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
k	k	k7c3	k
určitému	určitý	k2eAgInSc3d1	určitý
přípravku	přípravek	k1gInSc3	přípravek
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
hlášení	hlášení	k1gNnSc6	hlášení
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
zdůraznit	zdůraznit	k5eAaPmF	zdůraznit
<g/>
.	.	kIx.	.
</s>
<s>
Nežádoucí	žádoucí	k2eNgInSc4d1	nežádoucí
účinek	účinek	k1gInSc4	účinek
lze	lze	k6eAd1	lze
SÚKL	SÚKL	kA	SÚKL
nahlásit	nahlásit	k5eAaPmF	nahlásit
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
formuláře	formulář	k1gInSc2	formulář
hlášení	hlášení	k1gNnSc2	hlášení
podezření	podezření	k1gNnSc2	podezření
na	na	k7c4	na
nežádoucí	žádoucí	k2eNgInSc4d1	nežádoucí
účinek	účinek	k1gInSc4	účinek
léčiva	léčivo	k1gNnSc2	léčivo
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
SÚKL	SÚKL	kA	SÚKL
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
buď	buď	k8xC	buď
v	v	k7c6	v
elektronické	elektronický	k2eAgFnSc6d1	elektronická
nebo	nebo	k8xC	nebo
v	v	k7c6	v
tištěné	tištěný	k2eAgFnSc6d1	tištěná
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
není	být	k5eNaImIp3nS	být
hlášení	hlášení	k1gNnSc1	hlášení
nežádoucích	žádoucí	k2eNgInPc2d1	nežádoucí
účinků	účinek	k1gInPc2	účinek
honorováno	honorován	k2eAgNnSc1d1	honorováno
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
však	však	k9	však
o	o	k7c4	o
profesionální	profesionální	k2eAgFnSc4d1	profesionální
povinnost	povinnost	k1gFnSc4	povinnost
lékaře	lékař	k1gMnSc2	lékař
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
podepřena	podepřít	k5eAaPmNgFnS	podepřít
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
za	za	k7c4	za
nesplnění	nesplnění	k1gNnSc4	nesplnění
zákonné	zákonný	k2eAgFnSc2d1	zákonná
povinnosti	povinnost	k1gFnSc2	povinnost
hlášení	hlášení	k1gNnSc2	hlášení
závažného	závažný	k2eAgInSc2d1	závažný
či	či	k8xC	či
neočekávaného	očekávaný	k2eNgInSc2d1	neočekávaný
nežádoucího	žádoucí	k2eNgInSc2d1	nežádoucí
účinku	účinek	k1gInSc2	účinek
nestanoví	stanovit	k5eNaPmIp3nS	stanovit
sankce	sankce	k1gFnPc4	sankce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Co	co	k3yQnSc4	co
nejsou	být	k5eNaImIp3nP	být
léčiva	léčivo	k1gNnPc4	léčivo
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Neléčiva	Neléčivo	k1gNnSc2	Neléčivo
====	====	k?	====
</s>
</p>
<p>
<s>
Výrobky	výrobek	k1gInPc1	výrobek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nemají	mít	k5eNaImIp3nP	mít
doklad	doklad	k1gInSc4	doklad
o	o	k7c4	o
účinnosti	účinnost	k1gFnPc4	účinnost
<g/>
,	,	kIx,	,
nepodléhají	podléhat	k5eNaImIp3nP	podléhat
registračnímu	registrační	k2eAgInSc3d1	registrační
řízení	řízení	k1gNnSc4	řízení
SÚKL	SÚKL	kA	SÚKL
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Doplňky	doplněk	k1gInPc1	doplněk
stravy	strava	k1gFnSc2	strava
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
potraviny	potravina	k1gFnPc4	potravina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
potravin	potravina	k1gFnPc2	potravina
pro	pro	k7c4	pro
běžnou	běžný	k2eAgFnSc4d1	běžná
spotřebu	spotřeba	k1gFnSc4	spotřeba
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
vitaminů	vitamin	k1gInPc2	vitamin
<g/>
,	,	kIx,	,
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgFnPc2d1	jiná
látek	látka	k1gFnPc2	látka
s	s	k7c7	s
nutričním	nutriční	k2eAgInSc7d1	nutriční
nebo	nebo	k8xC	nebo
fyziologickým	fyziologický	k2eAgInSc7d1	fyziologický
účinkem	účinek	k1gInSc7	účinek
<g/>
,	,	kIx,	,
a	a	k8xC	a
které	který	k3yIgNnSc1	který
byly	být	k5eAaImAgFnP	být
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
doplnění	doplnění	k1gNnSc2	doplnění
běžné	běžný	k2eAgFnSc2d1	běžná
stravy	strava	k1gFnSc2	strava
spotřebitele	spotřebitel	k1gMnPc4	spotřebitel
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
příznivě	příznivě	k6eAd1	příznivě
ovlivňující	ovlivňující	k2eAgInSc1d1	ovlivňující
jeho	jeho	k3xOp3gInSc1	jeho
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
<g/>
.	.	kIx.	.
</s>
<s>
Účinky	účinek	k1gInPc1	účinek
deklarované	deklarovaný	k2eAgInPc1d1	deklarovaný
výrobcem	výrobce	k1gMnSc7	výrobce
nejsou	být	k5eNaImIp3nP	být
ověřovány	ověřován	k2eAgFnPc1d1	ověřována
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
u	u	k7c2	u
doplňků	doplněk	k1gInPc2	doplněk
stravy	strava	k1gFnSc2	strava
není	být	k5eNaImIp3nS	být
posuzována	posuzovat	k5eAaImNgFnS	posuzovat
jejich	jejich	k3xOp3gFnSc1	jejich
účinnost	účinnost	k1gFnSc1	účinnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
tedy	tedy	k8xC	tedy
určeny	určit	k5eAaPmNgFnP	určit
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
či	či	k8xC	či
prevenci	prevence	k1gFnSc3	prevence
(	(	kIx(	(
<g/>
předcházení	předcházení	k1gNnSc1	předcházení
<g/>
)	)	kIx)	)
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Regulace	regulace	k1gFnSc1	regulace
doplňků	doplněk	k1gInPc2	doplněk
stravy	strava	k1gFnSc2	strava
spadá	spadat	k5eAaImIp3nS	spadat
do	do	k7c2	do
kompetence	kompetence	k1gFnSc2	kompetence
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Potraviny	potravina	k1gFnPc1	potravina
pro	pro	k7c4	pro
zvláštní	zvláštní	k2eAgInPc4d1	zvláštní
lékařské	lékařský	k2eAgInPc4d1	lékařský
účely	účel	k1gInPc4	účel
(	(	kIx(	(
<g/>
léčebné	léčebné	k1gNnSc1	léčebné
výživy	výživa	k1gFnSc2	výživa
<g/>
,	,	kIx,	,
výživy	výživa	k1gFnSc2	výživa
sondou	sonda	k1gFnSc7	sonda
<g/>
)	)	kIx)	)
–	–	k?	–
regulace	regulace	k1gFnSc2	regulace
potravin	potravina	k1gFnPc2	potravina
určených	určený	k2eAgInPc2d1	určený
pro	pro	k7c4	pro
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
výživu	výživa	k1gFnSc4	výživa
spadá	spadat	k5eAaImIp3nS	spadat
do	do	k7c2	do
kompetence	kompetence	k1gFnSc2	kompetence
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
ČR	ČR	kA	ČR
a	a	k8xC	a
Státní	státní	k2eAgFnSc2d1	státní
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
a	a	k8xC	a
potravinářské	potravinářský	k2eAgFnSc2d1	potravinářská
inspekce	inspekce	k1gFnSc2	inspekce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kosmetika	kosmetika	k1gFnSc1	kosmetika
–	–	k?	–
výrobce	výrobce	k1gMnSc1	výrobce
<g/>
,	,	kIx,	,
dovozce	dovozce	k1gMnSc1	dovozce
nebo	nebo	k8xC	nebo
distributor	distributor	k1gMnSc1	distributor
je	být	k5eAaImIp3nS	být
povinen	povinen	k2eAgMnSc1d1	povinen
zajistit	zajistit	k5eAaPmF	zajistit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jím	on	k3xPp3gNnSc7	on
vyráběné	vyráběný	k2eAgInPc1d1	vyráběný
nebo	nebo	k8xC	nebo
dovážené	dovážený	k2eAgInPc4d1	dovážený
kosmetické	kosmetický	k2eAgInPc4d1	kosmetický
prostředky	prostředek	k1gInPc4	prostředek
byly	být	k5eAaImAgFnP	být
při	při	k7c6	při
uvedení	uvedení	k1gNnSc6	uvedení
na	na	k7c4	na
trh	trh	k1gInSc4	trh
bezpečné	bezpečný	k2eAgFnPc1d1	bezpečná
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
aby	aby	kYmCp3nP	aby
nezpůsobily	způsobit	k5eNaPmAgInP	způsobit
poškození	poškození	k1gNnSc4	poškození
zdraví	zdraví	k1gNnSc2	zdraví
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
vyhovovaly	vyhovovat	k5eAaImAgInP	vyhovovat
hygienickým	hygienický	k2eAgInPc3d1	hygienický
požadavkům	požadavek	k1gInPc3	požadavek
stanoveným	stanovený	k2eAgInPc3d1	stanovený
vyhláškou	vyhláška	k1gFnSc7	vyhláška
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
uvedením	uvedení	k1gNnSc7	uvedení
kosmetického	kosmetický	k2eAgInSc2d1	kosmetický
prostředku	prostředek	k1gInSc2	prostředek
na	na	k7c4	na
trh	trh	k1gInSc4	trh
je	být	k5eAaImIp3nS	být
výrobce	výrobce	k1gMnSc1	výrobce
nebo	nebo	k8xC	nebo
dovozce	dovozce	k1gMnSc1	dovozce
povinen	povinen	k2eAgMnSc1d1	povinen
zajistit	zajistit	k5eAaPmF	zajistit
hodnocení	hodnocení	k1gNnSc2	hodnocení
jeho	jeho	k3xOp3gFnSc2	jeho
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
pro	pro	k7c4	pro
zdraví	zdraví	k1gNnSc4	zdraví
fyzických	fyzický	k2eAgFnPc2d1	fyzická
osob	osoba	k1gFnPc2	osoba
oprávněnou	oprávněný	k2eAgFnSc7d1	oprávněná
(	(	kIx(	(
<g/>
kvalifikovanou	kvalifikovaný	k2eAgFnSc7d1	kvalifikovaná
<g/>
)	)	kIx)	)
osobou	osoba	k1gFnSc7	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
uvedením	uvedení	k1gNnSc7	uvedení
kosmetického	kosmetický	k2eAgInSc2d1	kosmetický
prostředku	prostředek	k1gInSc2	prostředek
na	na	k7c4	na
trh	trh	k1gInSc4	trh
je	být	k5eAaImIp3nS	být
výrobce	výrobce	k1gMnSc1	výrobce
nebo	nebo	k8xC	nebo
dovozce	dovozce	k1gMnSc1	dovozce
povinen	povinen	k2eAgMnSc1d1	povinen
provést	provést	k5eAaPmF	provést
tzv.	tzv.	kA	tzv.
notifikaci	notifikace	k1gFnSc4	notifikace
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
oznámit	oznámit	k5eAaPmF	oznámit
příslušnému	příslušný	k2eAgMnSc3d1	příslušný
orgánu	orgán	k1gMnSc3	orgán
ochrany	ochrana	k1gFnSc2	ochrana
veřejného	veřejný	k2eAgNnSc2d1	veřejné
zdraví	zdraví	k1gNnSc2	zdraví
(	(	kIx(	(
<g/>
krajské	krajský	k2eAgFnSc6d1	krajská
hygienické	hygienický	k2eAgFnSc6d1	hygienická
stanici	stanice	k1gFnSc6	stanice
<g/>
)	)	kIx)	)
mj.	mj.	kA	mj.
datum	datum	k1gNnSc1	datum
zahájení	zahájení	k1gNnSc1	zahájení
výroby	výroba	k1gFnSc2	výroba
nebo	nebo	k8xC	nebo
dovozu	dovoz	k1gInSc2	dovoz
včetně	včetně	k7c2	včetně
seznamu	seznam	k1gInSc2	seznam
kosmetických	kosmetický	k2eAgInPc2d1	kosmetický
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
hodlá	hodlat	k5eAaImIp3nS	hodlat
vyrábět	vyrábět	k5eAaImF	vyrábět
nebo	nebo	k8xC	nebo
dovážet	dovážet	k5eAaImF	dovážet
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
uchovávána	uchováván	k2eAgFnSc1d1	uchovávána
povinná	povinný	k2eAgFnSc1d1	povinná
dokumentace	dokumentace	k1gFnSc1	dokumentace
k	k	k7c3	k
výrobku	výrobek	k1gInSc3	výrobek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Informace	informace	k1gFnPc1	informace
a	a	k8xC	a
propagace	propagace	k1gFnSc1	propagace
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
léčivých	léčivý	k2eAgInPc6d1	léčivý
přípravcích	přípravek	k1gInPc6	přípravek
====	====	k?	====
</s>
</p>
<p>
<s>
Na	na	k7c6	na
internetových	internetový	k2eAgFnPc6d1	internetová
stránkách	stránka	k1gFnPc6	stránka
SÚKL	SÚKL	kA	SÚKL
je	být	k5eAaImIp3nS	být
volně	volně	k6eAd1	volně
přístupná	přístupný	k2eAgFnSc1d1	přístupná
databáze	databáze	k1gFnSc1	databáze
registrovaných	registrovaný	k2eAgInPc2d1	registrovaný
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
všechny	všechen	k3xTgInPc4	všechen
léky	lék	k1gInPc4	lék
schválené	schválený	k2eAgInPc4d1	schválený
pro	pro	k7c4	pro
prodej	prodej	k1gInSc4	prodej
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
databáze	databáze	k1gFnSc2	databáze
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
léčiv	léčivo	k1gNnPc2	léčivo
je	být	k5eAaImIp3nS	být
Příbalový	Příbalový	k2eAgInSc1d1	Příbalový
informační	informační	k2eAgInSc1d1	informační
leták	leták	k1gInSc1	leták
<g/>
,	,	kIx,	,
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
informace	informace	k1gFnSc1	informace
o	o	k7c4	o
přípravku	přípravka	k1gFnSc4	přípravka
<g/>
,	,	kIx,	,
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
účinné	účinný	k2eAgFnSc6d1	účinná
látce	látka	k1gFnSc6	látka
a	a	k8xC	a
držiteli	držitel	k1gMnSc3	držitel
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
o	o	k7c4	o
registraci	registrace	k1gFnSc4	registrace
<g/>
,	,	kIx,	,
přehled	přehled	k1gInSc4	přehled
s	s	k7c7	s
maximální	maximální	k2eAgFnSc7d1	maximální
cenou	cena	k1gFnSc7	cena
výrobce	výrobce	k1gMnSc1	výrobce
<g/>
/	/	kIx~	/
<g/>
cenou	cena	k1gFnSc7	cena
původce	původce	k1gMnSc2	původce
<g/>
,	,	kIx,	,
úhradou	úhrada	k1gFnSc7	úhrada
ze	z	k7c2	z
zdravotního	zdravotní	k2eAgNnSc2d1	zdravotní
pojištění	pojištění	k1gNnSc2	pojištění
a	a	k8xC	a
také	také	k9	také
orientační	orientační	k2eAgFnSc7d1	orientační
prodejní	prodejní	k2eAgFnSc7d1	prodejní
cenou	cena	k1gFnSc7	cena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Příbalová	Příbalový	k2eAgFnSc1d1	Příbalová
informace	informace	k1gFnPc1	informace
(	(	kIx(	(
<g/>
PIL	pít	k5eAaImAgInS	pít
<g/>
)	)	kIx)	)
=====	=====	k?	=====
</s>
</p>
<p>
<s>
Základními	základní	k2eAgInPc7d1	základní
požadavky	požadavek	k1gInPc7	požadavek
na	na	k7c4	na
příbalový	příbalový	k2eAgInSc4d1	příbalový
leták	leták	k1gInSc4	leták
je	být	k5eAaImIp3nS	být
čitelnost	čitelnost	k1gFnSc4	čitelnost
a	a	k8xC	a
srozumitelnost	srozumitelnost	k1gFnSc4	srozumitelnost
pro	pro	k7c4	pro
cílovou	cílový	k2eAgFnSc4d1	cílová
skupinu	skupina	k1gFnSc4	skupina
pacientů	pacient	k1gMnPc2	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
doporučeno	doporučen	k2eAgNnSc1d1	doporučeno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
příbalová	příbalový	k2eAgFnSc1d1	příbalová
informace	informace	k1gFnSc1	informace
byla	být	k5eAaImAgFnS	být
členěna	členit	k5eAaImNgFnS	členit
do	do	k7c2	do
kapitol	kapitola	k1gFnPc2	kapitola
<g/>
,	,	kIx,	,
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
klade	klást	k5eAaImIp3nS	klást
důraz	důraz	k1gInSc1	důraz
i	i	k9	i
na	na	k7c4	na
grafickou	grafický	k2eAgFnSc4d1	grafická
prezentaci	prezentace	k1gFnSc4	prezentace
celého	celý	k2eAgInSc2d1	celý
letáku	leták	k1gInSc2	leták
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
např.	např.	kA	např.
nevhodného	vhodný	k2eNgNnSc2d1	nevhodné
písma	písmo	k1gNnSc2	písmo
(	(	kIx(	(
<g/>
velikosti	velikost	k1gFnSc2	velikost
<g/>
)	)	kIx)	)
či	či	k8xC	či
špatné	špatný	k2eAgNnSc1d1	špatné
grafické	grafický	k2eAgNnSc1d1	grafické
uspořádání	uspořádání	k1gNnSc1	uspořádání
může	moct	k5eAaImIp3nS	moct
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
čitelnost	čitelnost	k1gFnSc4	čitelnost
letáku	leták	k1gInSc2	leták
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Propagace	propagace	k1gFnSc1	propagace
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
====	====	k?	====
</s>
</p>
<p>
<s>
Reklama	reklama	k1gFnSc1	reklama
na	na	k7c4	na
širokou	široký	k2eAgFnSc4d1	široká
veřejnost	veřejnost	k1gFnSc4	veřejnost
je	být	k5eAaImIp3nS	být
povolena	povolit	k5eAaPmNgFnS	povolit
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
z	z	k7c2	z
kategorie	kategorie	k1gFnSc2	kategorie
bez	bez	k7c2	bez
lékařského	lékařský	k2eAgInSc2d1	lékařský
předpisu	předpis	k1gInSc2	předpis
(	(	kIx(	(
<g/>
volně	volně	k6eAd1	volně
prodejné	prodejný	k2eAgFnPc1d1	prodejná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Reklama	reklama	k1gFnSc1	reklama
musí	muset	k5eAaImIp3nS	muset
splňovat	splňovat	k5eAaImF	splňovat
všechny	všechen	k3xTgInPc1	všechen
požadavky	požadavek	k1gInPc1	požadavek
stanovené	stanovený	k2eAgInPc1d1	stanovený
zákonem	zákon	k1gInSc7	zákon
o	o	k7c6	o
regulaci	regulace	k1gFnSc6	regulace
reklamy	reklama	k1gFnSc2	reklama
a	a	k8xC	a
zejména	zejména	k9	zejména
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
vyvarovat	vyvarovat	k5eAaPmF	vyvarovat
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
naplňovala	naplňovat	k5eAaImAgFnS	naplňovat
podstatu	podstata	k1gFnSc4	podstata
nekalé	kalý	k2eNgFnSc2d1	nekalá
obchodní	obchodní	k2eAgFnSc2d1	obchodní
praktiky	praktika	k1gFnSc2	praktika
nebo	nebo	k8xC	nebo
reklamy	reklama	k1gFnSc2	reklama
skryté	skrytý	k2eAgInPc1d1	skrytý
či	či	k8xC	či
byla	být	k5eAaImAgFnS	být
reklamou	reklama	k1gFnSc7	reklama
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
dobrými	dobrý	k2eAgInPc7d1	dobrý
mravy	mrav	k1gInPc7	mrav
atd.	atd.	kA	atd.
Jak	jak	k8xC	jak
reklama	reklama	k1gFnSc1	reklama
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
nekalou	kalý	k2eNgFnSc7d1	nekalá
obchodní	obchodní	k2eAgFnSc7d1	obchodní
praktikou	praktika	k1gFnSc7	praktika
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
skrytá	skrytý	k2eAgFnSc1d1	skrytá
reklama	reklama	k1gFnSc1	reklama
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
způsobilé	způsobilý	k2eAgInPc4d1	způsobilý
vážně	vážně	k6eAd1	vážně
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
vnímání	vnímání	k1gNnSc4	vnímání
poskytované	poskytovaný	k2eAgFnSc2d1	poskytovaná
informace	informace	k1gFnSc2	informace
běžným	běžný	k2eAgMnSc7d1	běžný
spotřebitelem	spotřebitel	k1gMnSc7	spotřebitel
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
případě	případ	k1gInSc6	případ
reklamy	reklama	k1gFnSc2	reklama
na	na	k7c4	na
léčivé	léčivý	k2eAgInPc4d1	léčivý
přípravky	přípravek	k1gInPc4	přípravek
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
nesprávné	správný	k2eNgNnSc4d1	nesprávné
pochopení	pochopení	k1gNnSc4	pochopení
informace	informace	k1gFnSc2	informace
pacientem	pacient	k1gMnSc7	pacient
–	–	k?	–
spotřebitelem	spotřebitel	k1gMnSc7	spotřebitel
<g/>
,	,	kIx,	,
zásadní	zásadní	k2eAgInSc4d1	zásadní
negativní	negativní	k2eAgInSc4d1	negativní
dopad	dopad	k1gInSc4	dopad
na	na	k7c6	na
používání	používání	k1gNnSc6	používání
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
a	a	k8xC	a
zdraví	zdraví	k1gNnSc4	zdraví
pacientů	pacient	k1gMnPc2	pacient
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dozorem	dozor	k1gInSc7	dozor
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
reklamy	reklama	k1gFnSc2	reklama
na	na	k7c4	na
léčivé	léčivý	k2eAgInPc4d1	léčivý
přípravky	přípravek	k1gInPc4	přípravek
jsou	být	k5eAaImIp3nP	být
pověřeni	pověřen	k2eAgMnPc1d1	pověřen
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Rada	rada	k1gFnSc1	rada
pro	pro	k7c4	pro
rozhlasové	rozhlasový	k2eAgNnSc4d1	rozhlasové
a	a	k8xC	a
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
pro	pro	k7c4	pro
reklamu	reklama	k1gFnSc4	reklama
šířenou	šířený	k2eAgFnSc4d1	šířená
v	v	k7c6	v
rozhlasovém	rozhlasový	k2eAgInSc6d1	rozhlasový
a	a	k8xC	a
televizním	televizní	k2eAgNnSc6d1	televizní
vysílání	vysílání	k1gNnSc6	vysílání
a	a	k8xC	a
pro	pro	k7c4	pro
sponzorování	sponzorování	k1gNnSc4	sponzorování
v	v	k7c6	v
rozhlasovém	rozhlasový	k2eAgInSc6d1	rozhlasový
a	a	k8xC	a
televizním	televizní	k2eAgNnSc6d1	televizní
vysílání	vysílání	k1gNnSc6	vysílání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
léčiv	léčivo	k1gNnPc2	léčivo
pro	pro	k7c4	pro
reklamu	reklama	k1gFnSc4	reklama
na	na	k7c4	na
humánní	humánní	k2eAgInPc4d1	humánní
léčivé	léčivý	k2eAgInPc4d1	léčivý
přípravky	přípravek	k1gInPc4	přípravek
a	a	k8xC	a
sponzorování	sponzorování	k1gNnSc4	sponzorování
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
reklamy	reklama	k1gFnSc2	reklama
šířené	šířený	k2eAgFnSc2d1	šířená
v	v	k7c6	v
rozhlasovém	rozhlasový	k2eAgInSc6d1	rozhlasový
a	a	k8xC	a
televizním	televizní	k2eAgNnSc6d1	televizní
vysílání	vysílání	k1gNnSc6	vysílání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
státní	státní	k2eAgFnSc4d1	státní
kontrolu	kontrola	k1gFnSc4	kontrola
veterinárních	veterinární	k2eAgMnPc2d1	veterinární
biopreparátů	biopreparát	k1gInPc2	biopreparát
a	a	k8xC	a
léčiv	léčivo	k1gNnPc2	léčivo
pro	pro	k7c4	pro
reklamu	reklama	k1gFnSc4	reklama
na	na	k7c4	na
veterinární	veterinární	k2eAgInPc4d1	veterinární
léčivé	léčivý	k2eAgInPc4d1	léčivý
přípravky	přípravek	k1gInPc4	přípravek
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
reklamy	reklama	k1gFnSc2	reklama
šířené	šířený	k2eAgFnSc2d1	šířená
v	v	k7c6	v
rozhlasovém	rozhlasový	k2eAgInSc6d1	rozhlasový
a	a	k8xC	a
televizním	televizní	k2eAgNnSc6d1	televizní
vysílání	vysílání	k1gNnSc6	vysílání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krajské	krajský	k2eAgInPc1d1	krajský
živnostenské	živnostenský	k2eAgInPc1d1	živnostenský
úřady	úřad	k1gInPc1	úřad
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
má	mít	k5eAaImIp3nS	mít
sídlo	sídlo	k1gNnSc1	sídlo
předpokládaný	předpokládaný	k2eAgMnSc1d1	předpokládaný
zadavatel	zadavatel	k1gMnSc1	zadavatel
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
reklamě	reklama	k1gFnSc6	reklama
podepsán	podepsat	k5eAaPmNgInS	podepsat
–	–	k?	–
firma	firma	k1gFnSc1	firma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpracovatel	zpracovatel	k1gMnSc1	zpracovatel
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
reklamní	reklamní	k2eAgFnSc1d1	reklamní
agentura	agentura	k1gFnSc1	agentura
<g/>
)	)	kIx)	)
či	či	k8xC	či
šiřitel	šiřitel	k1gMnSc1	šiřitel
reklamy	reklama	k1gFnSc2	reklama
(	(	kIx(	(
<g/>
např.	např.	kA	např.
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
časopis	časopis	k1gInSc1	časopis
<g/>
,	,	kIx,	,
pošta	pošta	k1gFnSc1	pošta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
případech	případ	k1gInPc6	případ
reklamy	reklama	k1gFnSc2	reklama
na	na	k7c4	na
potraviny	potravina	k1gFnPc4	potravina
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
reklamy	reklama	k1gFnSc2	reklama
šířené	šířený	k2eAgFnSc2d1	šířená
v	v	k7c6	v
rozhlasovém	rozhlasový	k2eAgInSc6d1	rozhlasový
a	a	k8xC	a
televizním	televizní	k2eAgNnSc6d1	televizní
vysílání	vysílání	k1gNnSc6	vysílání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Padělky	padělek	k1gInPc1	padělek
a	a	k8xC	a
nelegální	legální	k2eNgInPc1d1	nelegální
přípravky	přípravek	k1gInPc1	přípravek
===	===	k?	===
</s>
</p>
<p>
<s>
Nelegálním	legální	k2eNgInSc7d1	nelegální
přípravkem	přípravek	k1gInSc7	přípravek
se	se	k3xPyFc4	se
rozumí	rozumět	k5eAaImIp3nS	rozumět
výrobek	výrobek	k1gInSc1	výrobek
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
identita	identita	k1gFnSc1	identita
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
původ	původ	k1gInSc4	původ
nejsou	být	k5eNaImIp3nP	být
úmyslně	úmyslně	k6eAd1	úmyslně
a	a	k8xC	a
podvodně	podvodně	k6eAd1	podvodně
špatně	špatně	k6eAd1	špatně
označeny	označit	k5eAaPmNgInP	označit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
výrobek	výrobek	k1gInSc4	výrobek
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
vlastním	vlastní	k2eAgInSc7d1	vlastní
názvem	název	k1gInSc7	název
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jím	on	k3xPp3gInSc7	on
např.	např.	kA	např.
výrobek	výrobek	k1gInSc1	výrobek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
registrován	registrován	k2eAgInSc1d1	registrován
jako	jako	k8xC	jako
léčivý	léčivý	k2eAgInSc1d1	léčivý
přípravek	přípravek	k1gInSc1	přípravek
mimo	mimo	k7c4	mimo
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
výrobek	výrobek	k1gInSc1	výrobek
tvářící	tvářící	k2eAgInSc1d1	tvářící
se	se	k3xPyFc4	se
jako	jako	k9	jako
jiný	jiný	k2eAgInSc1d1	jiný
produkt	produkt	k1gInSc1	produkt
než	než	k8xS	než
léčivý	léčivý	k2eAgInSc1d1	léčivý
přípravek	přípravek	k1gInSc1	přípravek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
doplněk	doplněk	k1gInSc4	doplněk
stravy	strava	k1gFnSc2	strava
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
výrobek	výrobek	k1gInSc1	výrobek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nemá	mít	k5eNaImIp3nS	mít
jasně	jasně	k6eAd1	jasně
definován	definovat	k5eAaBmNgInS	definovat
svůj	svůj	k3xOyFgInSc4	svůj
statut	statut	k1gInSc4	statut
<g/>
.	.	kIx.	.
</s>
<s>
Nelegální	legální	k2eNgInSc1d1	nelegální
přípravek	přípravek	k1gInSc1	přípravek
splňuje	splňovat	k5eAaImIp3nS	splňovat
definici	definice	k1gFnSc4	definice
léčivého	léčivý	k2eAgInSc2d1	léčivý
přípravku	přípravek	k1gInSc2	přípravek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
obsah	obsah	k1gInSc4	obsah
účinné	účinný	k2eAgFnSc2d1	účinná
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
deklaraci	deklarace	k1gFnSc3	deklarace
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Padělkem	padělek	k1gInSc7	padělek
léčivého	léčivý	k2eAgInSc2d1	léčivý
přípravku	přípravek	k1gInSc2	přípravek
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
definice	definice	k1gFnSc2	definice
WHO	WHO	kA	WHO
rozumí	rozumět	k5eAaImIp3nS	rozumět
takový	takový	k3xDgInSc4	takový
přípravek	přípravek	k1gInSc4	přípravek
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
identita	identita	k1gFnSc1	identita
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
původ	původ	k1gInSc4	původ
jsou	být	k5eAaImIp3nP	být
úmyslně	úmyslně	k6eAd1	úmyslně
a	a	k8xC	a
podvodně	podvodně	k6eAd1	podvodně
nesprávně	správně	k6eNd1	správně
označeny	označit	k5eAaPmNgInP	označit
<g/>
.	.	kIx.	.
</s>
<s>
Předmětem	předmět	k1gInSc7	předmět
padělání	padělání	k1gNnSc2	padělání
jsou	být	k5eAaImIp3nP	být
originální	originální	k2eAgInPc1d1	originální
i	i	k9	i
generická	generický	k2eAgNnPc4d1	generické
léčiva	léčivo	k1gNnPc4	léčivo
<g/>
.	.	kIx.	.
</s>
<s>
Padělky	padělek	k1gInPc1	padělek
mohou	moct	k5eAaImIp3nP	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
správné	správný	k2eAgFnPc1d1	správná
nebo	nebo	k8xC	nebo
nesprávné	správný	k2eNgFnPc1d1	nesprávná
obsahové	obsahový	k2eAgFnPc1d1	obsahová
látky	látka	k1gFnPc1	látka
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
účinné	účinný	k2eAgFnSc2d1	účinná
látky	látka	k1gFnSc2	látka
nebo	nebo	k8xC	nebo
bez	bez	k7c2	bez
ní	on	k3xPp3gFnSc2	on
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jsou	být	k5eAaImIp3nP	být
zabaleny	zabalit	k5eAaPmNgFnP	zabalit
do	do	k7c2	do
falešného	falešný	k2eAgInSc2d1	falešný
obalu	obal	k1gInSc2	obal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
padělky	padělek	k1gInPc7	padělek
a	a	k8xC	a
nelegálními	legální	k2eNgInPc7d1	nelegální
přípravky	přípravek	k1gInPc7	přípravek
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
na	na	k7c6	na
černém	černý	k2eAgInSc6d1	černý
trhu	trh	k1gInSc6	trh
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
protiprávní	protiprávní	k2eAgMnSc1d1	protiprávní
a	a	k8xC	a
pro	pro	k7c4	pro
zákazníka	zákazník	k1gMnSc4	zákazník
vždy	vždy	k6eAd1	vždy
rizikové	rizikový	k2eAgInPc1d1	rizikový
<g/>
.	.	kIx.	.
</s>
<s>
Nelegálními	legální	k2eNgNnPc7d1	nelegální
místy	místo	k1gNnPc7	místo
prodejů	prodej	k1gInPc2	prodej
léků	lék	k1gInPc2	lék
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
tržiště	tržiště	k1gNnSc2	tržiště
<g/>
,	,	kIx,	,
fitcentra	fitcentrum	k1gNnSc2	fitcentrum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
nejoblíbenější	oblíbený	k2eAgFnSc2d3	nejoblíbenější
internetové	internetový	k2eAgFnSc2d1	internetová
stránky	stránka	k1gFnSc2	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
nezákonným	zákonný	k2eNgInSc7d1	nezákonný
způsobem	způsob	k1gInSc7	způsob
jsou	být	k5eAaImIp3nP	být
nabízeny	nabízet	k5eAaImNgInP	nabízet
nejčastěji	často	k6eAd3	často
léky	lék	k1gInPc1	lék
na	na	k7c4	na
redukci	redukce	k1gFnSc4	redukce
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
léčiva	léčivo	k1gNnSc2	léčivo
na	na	k7c4	na
erektilní	erektilní	k2eAgFnSc4d1	erektilní
dysfunkci	dysfunkce	k1gFnSc4	dysfunkce
nebo	nebo	k8xC	nebo
přípravky	přípravek	k1gInPc4	přípravek
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
body	bod	k1gInPc1	bod
building	building	k1gInSc1	building
–	–	k?	–
anabolické	anabolický	k2eAgInPc1d1	anabolický
steroidy	steroid	k1gInPc1	steroid
<g/>
.	.	kIx.	.
</s>
<s>
Internet	Internet	k1gInSc1	Internet
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
snazší	snadný	k2eAgFnSc3d2	snazší
dostupnosti	dostupnost	k1gFnSc3	dostupnost
a	a	k8xC	a
díky	díky	k7c3	díky
možné	možný	k2eAgFnSc3d1	možná
anonymitě	anonymita	k1gFnSc3	anonymita
stává	stávat	k5eAaImIp3nS	stávat
nejčastějším	častý	k2eAgInSc7d3	nejčastější
zdrojem	zdroj	k1gInSc7	zdroj
nelegálních	legální	k2eNgFnPc2d1	nelegální
nabídek	nabídka	k1gFnPc2	nabídka
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
<g/>
.	.	kIx.	.
</s>
<s>
Prodávající	prodávající	k2eAgMnSc1d1	prodávající
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
schovává	schovávat	k5eAaImIp3nS	schovávat
za	za	k7c4	za
různé	různý	k2eAgInPc4d1	různý
inzeráty	inzerát	k1gInPc4	inzerát
či	či	k8xC	či
nabídky	nabídka	k1gFnPc4	nabídka
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
chatu	chata	k1gFnSc4	chata
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nákupu	nákup	k1gInSc6	nákup
léků	lék	k1gInPc2	lék
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
důležité	důležitý	k2eAgNnSc1d1	důležité
si	se	k3xPyFc3	se
uvědomit	uvědomit	k5eAaPmF	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
lze	lze	k6eAd1	lze
nabízet	nabízet	k5eAaImF	nabízet
pouze	pouze	k6eAd1	pouze
registrované	registrovaný	k2eAgInPc4d1	registrovaný
léčivé	léčivý	k2eAgInPc4d1	léčivý
přípravky	přípravek	k1gInPc4	přípravek
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
vázány	vázat	k5eAaImNgFnP	vázat
na	na	k7c4	na
lékařský	lékařský	k2eAgInSc4d1	lékařský
předpis	předpis	k1gInSc4	předpis
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
nabízejícím	nabízející	k2eAgInSc7d1	nabízející
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pouze	pouze	k6eAd1	pouze
schválená	schválený	k2eAgFnSc1d1	schválená
"	"	kIx"	"
<g/>
kamenná	kamenný	k2eAgFnSc1d1	kamenná
<g/>
"	"	kIx"	"
lékárna	lékárna	k1gFnSc1	lékárna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
internetových	internetový	k2eAgFnPc6d1	internetová
stránkách	stránka	k1gFnPc6	stránka
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
jednoznačně	jednoznačně	k6eAd1	jednoznačně
uveden	uveden	k2eAgMnSc1d1	uveden
provozovatel	provozovatel	k1gMnSc1	provozovatel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
informuje	informovat	k5eAaBmIp3nS	informovat
o	o	k7c6	o
cenách	cena	k1gFnPc6	cena
<g/>
,	,	kIx,	,
způsobu	způsob	k1gInSc6	způsob
objednávání	objednávání	k1gNnSc2	objednávání
<g/>
,	,	kIx,	,
možnosti	možnost	k1gFnSc2	možnost
reklamace	reklamace	k1gFnSc2	reklamace
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
kontakt	kontakt	k1gInSc4	kontakt
na	na	k7c4	na
informační	informační	k2eAgFnSc4d1	informační
službu	služba	k1gFnSc4	služba
poskytovanou	poskytovaný	k2eAgFnSc7d1	poskytovaná
farmaceutem	farmaceut	k1gMnSc7	farmaceut
nebo	nebo	k8xC	nebo
farmaceutickým	farmaceutický	k2eAgMnSc7d1	farmaceutický
asistentem	asistent	k1gMnSc7	asistent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
====	====	k?	====
</s>
</p>
<p>
<s>
U	u	k7c2	u
nelegálních	legální	k2eNgInPc2d1	nelegální
přípravků	přípravek	k1gInPc2	přípravek
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zaručit	zaručit	k5eAaPmF	zaručit
účinnost	účinnost	k1gFnSc4	účinnost
<g/>
,	,	kIx,	,
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
a	a	k8xC	a
kvalitu	kvalita	k1gFnSc4	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
a	a	k8xC	a
kdo	kdo	k3yRnSc1	kdo
je	být	k5eAaImIp3nS	být
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
byly	být	k5eAaImAgInP	být
přepravovány	přepravován	k2eAgInPc1d1	přepravován
a	a	k8xC	a
uchovávány	uchováván	k2eAgInPc1d1	uchováván
či	či	k8xC	či
jaké	jaký	k3yRgNnSc4	jaký
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnSc7	jejich
složení	složení	k1gNnSc4	složení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
kdy	kdy	k6eAd1	kdy
takový	takový	k3xDgInSc4	takový
lék	lék	k1gInSc4	lék
užijete	užít	k5eAaPmIp2nP	užít
<g/>
,	,	kIx,	,
musíte	muset	k5eAaImIp2nP	muset
vždy	vždy	k6eAd1	vždy
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
můžete	moct	k5eAaImIp2nP	moct
mít	mít	k5eAaImF	mít
žaludeční	žaludeční	k2eAgInPc4d1	žaludeční
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
můžete	moct	k5eAaImIp2nP	moct
skončit	skončit	k5eAaPmF	skončit
na	na	k7c6	na
jednotce	jednotka	k1gFnSc6	jednotka
intenzivní	intenzivní	k2eAgFnSc2d1	intenzivní
péče	péče	k1gFnSc2	péče
<g/>
,	,	kIx,	,
např.	např.	kA	např.
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
selhání	selhání	k1gNnSc4	selhání
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
.	.	kIx.	.
<g/>
Takové	takový	k3xDgNnSc1	takový
riziko	riziko	k1gNnSc1	riziko
podstupuje	podstupovat	k5eAaImIp3nS	podstupovat
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
nakupuje	nakupovat	k5eAaBmIp3nS	nakupovat
léky	lék	k1gInPc4	lék
pomocí	pomocí	k7c2	pomocí
nelegálních	legální	k2eNgFnPc2d1	nelegální
nabídek	nabídka	k1gFnPc2	nabídka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ať	ať	k9	ať
již	již	k9	již
kvůli	kvůli	k7c3	kvůli
zdánlivé	zdánlivý	k2eAgFnSc3d1	zdánlivá
finanční	finanční	k2eAgFnSc3d1	finanční
výhodnosti	výhodnost	k1gFnSc3	výhodnost
nebo	nebo	k8xC	nebo
kvůli	kvůli	k7c3	kvůli
strachu	strach	k1gInSc3	strach
jít	jít	k5eAaImF	jít
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
problémem	problém	k1gInSc7	problém
k	k	k7c3	k
lékaři	lékař	k1gMnSc3	lékař
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Hlášení	hlášení	k1gNnSc3	hlášení
podezření	podezření	k1gNnSc3	podezření
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
podezření	podezření	k1gNnSc2	podezření
na	na	k7c4	na
výskyt	výskyt	k1gInSc4	výskyt
podělku	podělek	k1gInSc2	podělek
nebo	nebo	k8xC	nebo
nelegálního	legální	k2eNgInSc2d1	nelegální
přípravku	přípravek	k1gInSc2	přípravek
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vyplnit	vyplnit	k5eAaPmF	vyplnit
formulář	formulář	k1gInSc4	formulář
SÚKL	SÚKL	kA	SÚKL
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Český	český	k2eAgInSc1d1	český
lékopis	lékopis	k1gInSc1	lékopis
===	===	k?	===
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
lékopis	lékopis	k1gInSc1	lékopis
(	(	kIx(	(
<g/>
zkr.	zkr.	kA	zkr.
ČL	čl	kA	čl
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
Pharmacopoea	Pharmacopoea	k1gMnSc1	Pharmacopoea
Bohemica	Bohemica	k1gMnSc1	Bohemica
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgNnSc4d1	základní
farmaceutické	farmaceutický	k2eAgNnSc4d1	farmaceutické
dílo	dílo	k1gNnSc4	dílo
normativního	normativní	k2eAgInSc2d1	normativní
charakteru	charakter	k1gInSc2	charakter
závazné	závazný	k2eAgInPc1d1	závazný
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
,	,	kIx,	,
účinnosti	účinnost	k1gFnSc2	účinnost
a	a	k8xC	a
kvality	kvalita	k1gFnSc2	kvalita
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
lékopis	lékopis	k1gInSc1	lékopis
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
postupy	postup	k1gInPc4	postup
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
léčivých	léčivý	k2eAgFnPc2d1	léčivá
a	a	k8xC	a
pomocných	pomocný	k2eAgFnPc2d1	pomocná
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
a	a	k8xC	a
přípravu	příprava	k1gFnSc4	příprava
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
a	a	k8xC	a
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
zkoušení	zkoušení	k1gNnSc4	zkoušení
a	a	k8xC	a
skladování	skladování	k1gNnSc4	skladování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
připravován	připravovat	k5eAaImNgInS	připravovat
Lékopisnou	lékopisný	k2eAgFnSc7d1	lékopisná
komisí	komise	k1gFnSc7	komise
a	a	k8xC	a
vydáván	vydávat	k5eAaImNgInS	vydávat
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
Český	český	k2eAgInSc1d1	český
lékopis	lékopis	k1gInSc1	lékopis
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
především	především	k9	především
články	článek	k1gInPc1	článek
převzaté	převzatý	k2eAgInPc1d1	převzatý
z	z	k7c2	z
Evropského	evropský	k2eAgInSc2d1	evropský
lékopisu	lékopis	k1gInSc2	lékopis
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
tzv.	tzv.	kA	tzv.
národní	národní	k2eAgInPc4d1	národní
články	článek	k1gInPc4	článek
<g/>
,	,	kIx,	,
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
specifických	specifický	k2eAgFnPc2d1	specifická
českých	český	k2eAgFnPc2d1	Česká
záležitostí	záležitost	k1gFnPc2	záležitost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Publikační	publikační	k2eAgFnSc4d1	publikační
činnost	činnost	k1gFnSc4	činnost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Věstník	věstník	k1gInSc4	věstník
SÚKL	SÚKL	kA	SÚKL
===	===	k?	===
</s>
</p>
<p>
<s>
Věstník	věstník	k1gInSc1	věstník
SÚKL	SÚKL	kA	SÚKL
je	být	k5eAaImIp3nS	být
vydáván	vydávat	k5eAaImNgInS	vydávat
Státním	státní	k2eAgInSc7d1	státní
ústavem	ústav	k1gInSc7	ústav
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
léčiv	léčivo	k1gNnPc2	léčivo
měsíčně	měsíčně	k6eAd1	měsíčně
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
odborné	odborný	k2eAgFnPc4d1	odborná
veřejnosti	veřejnost	k1gFnPc4	veřejnost
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
léčivech	léčivo	k1gNnPc6	léčivo
a	a	k8xC	a
zdravotnických	zdravotnický	k2eAgInPc6d1	zdravotnický
prostředcích	prostředek	k1gInPc6	prostředek
a	a	k8xC	a
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
378	[number]	k4	378
<g/>
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
léčivech	léčivo	k1gNnPc6	léčivo
<g/>
,	,	kIx,	,
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
ve	v	k7c6	v
Věstníku	věstník	k1gInSc6	věstník
SÚKL	SÚKL	kA	SÚKL
zveřejnění	zveřejnění	k1gNnSc2	zveřejnění
vydaných	vydaný	k2eAgNnPc2d1	vydané
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
o	o	k7c6	o
registraci	registrace	k1gFnSc6	registrace
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pravidelně	pravidelně	k6eAd1	pravidelně
jsou	být	k5eAaImIp3nP	být
publikována	publikován	k2eAgNnPc1d1	Publikováno
opatření	opatření	k1gNnPc1	opatření
při	při	k7c6	při
závadách	závada	k1gFnPc6	závada
kvality	kvalita	k1gFnSc2	kvalita
a	a	k8xC	a
nežádoucích	žádoucí	k2eNgInPc6d1	nežádoucí
účincích	účinek	k1gInPc6	účinek
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
,	,	kIx,	,
opatření	opatření	k1gNnSc1	opatření
při	při	k7c6	při
nežádoucích	žádoucí	k2eNgFnPc6d1	nežádoucí
příhodách	příhoda	k1gFnPc6	příhoda
zdravotnických	zdravotnický	k2eAgInPc2d1	zdravotnický
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
publikovaných	publikovaný	k2eAgFnPc6d1	publikovaná
technických	technický	k2eAgFnPc6d1	technická
normách	norma	k1gFnPc6	norma
zaměřených	zaměřený	k2eAgFnPc2d1	zaměřená
na	na	k7c4	na
zdravotnické	zdravotnický	k2eAgInPc4d1	zdravotnický
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
dokumentech	dokument	k1gInPc6	dokument
vydaných	vydaný	k2eAgInPc2d1	vydaný
Evropskou	evropský	k2eAgFnSc7d1	Evropská
lékovou	lékový	k2eAgFnSc7d1	léková
agenturou	agentura	k1gFnSc7	agentura
<g/>
,	,	kIx,	,
číselné	číselný	k2eAgInPc4d1	číselný
údaje	údaj	k1gInPc4	údaj
o	o	k7c6	o
stavu	stav	k1gInSc6	stav
žádostí	žádost	k1gFnPc2	žádost
v	v	k7c6	v
SÚKL	SÚKL	kA	SÚKL
<g/>
,	,	kIx,	,
měsíční	měsíční	k2eAgInSc1d1	měsíční
<g />
.	.	kIx.	.
</s>
<s>
přehled	přehled	k1gInSc1	přehled
nově	nově	k6eAd1	nově
schválených	schválený	k2eAgMnPc2d1	schválený
výrobců	výrobce	k1gMnPc2	výrobce
a	a	k8xC	a
distributorů	distributor	k1gMnPc2	distributor
léčiv	léčivo	k1gNnPc2	léčivo
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
čtvrtletní	čtvrtletní	k2eAgInSc4d1	čtvrtletní
přehled	přehled	k1gInSc4	přehled
nově	nově	k6eAd1	nově
schválených	schválený	k2eAgFnPc2d1	schválená
lékáren	lékárna	k1gFnPc2	lékárna
<g/>
,	,	kIx,	,
seznamy	seznam	k1gInPc1	seznam
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
skončí	skončit	k5eAaPmIp3nS	skončit
do	do	k7c2	do
3	[number]	k4	3
měsíců	měsíc	k1gInPc2	měsíc
platnost	platnost	k1gFnSc4	platnost
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
o	o	k7c4	o
registraci	registrace	k1gFnSc4	registrace
a	a	k8xC	a
seznamy	seznam	k1gInPc4	seznam
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
měsíci	měsíc	k1gInSc6	měsíc
skončila	skončit	k5eAaPmAgFnS	skončit
platnost	platnost	k1gFnSc4	platnost
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
o	o	k7c4	o
registraci	registrace	k1gFnSc4	registrace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
citovaným	citovaný	k2eAgInSc7d1	citovaný
zákonem	zákon	k1gInSc7	zákon
jsou	být	k5eAaImIp3nP	být
měsíčně	měsíčně	k6eAd1	měsíčně
publikovány	publikován	k2eAgInPc1d1	publikován
přehledy	přehled	k1gInPc1	přehled
nově	nově	k6eAd1	nově
registrovaných	registrovaný	k2eAgInPc2d1	registrovaný
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
a	a	k8xC	a
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
registracích	registrace	k1gFnPc6	registrace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Věstníku	věstník	k1gInSc6	věstník
SÚKL	SÚKL	kA	SÚKL
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
publikovány	publikovat	k5eAaBmNgInP	publikovat
všechny	všechen	k3xTgInPc1	všechen
nové	nový	k2eAgInPc1d1	nový
pokyny	pokyn	k1gInPc1	pokyn
SÚKL	SÚKL	kA	SÚKL
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
aktuální	aktuální	k2eAgInSc1d1	aktuální
seznam	seznam	k1gInSc1	seznam
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
obměně	obměna	k1gFnSc3	obměna
<g/>
,	,	kIx,	,
čtvrtletně	čtvrtletně	k6eAd1	čtvrtletně
je	být	k5eAaImIp3nS	být
zveřejňován	zveřejňován	k2eAgInSc1d1	zveřejňován
článek	článek	k1gInSc1	článek
o	o	k7c6	o
"	"	kIx"	"
<g/>
spotřebě	spotřeba	k1gFnSc6	spotřeba
<g/>
"	"	kIx"	"
léčiv	léčivo	k1gNnPc2	léčivo
po	po	k7c6	po
zpracování	zpracování	k1gNnSc6	zpracování
příslušného	příslušný	k2eAgNnSc2d1	příslušné
čtvrtletí	čtvrtletí	k1gNnSc2	čtvrtletí
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
jsou	být	k5eAaImIp3nP	být
publikovány	publikován	k2eAgFnPc4d1	publikována
různé	různý	k2eAgFnPc4d1	různá
informace	informace	k1gFnPc4	informace
SÚKL	SÚKL	kA	SÚKL
určené	určený	k2eAgFnPc4d1	určená
regulovaným	regulovaný	k2eAgMnPc3d1	regulovaný
subjektům	subjekt	k1gInPc3	subjekt
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
je	být	k5eAaImIp3nS	být
zavedena	zaveden	k2eAgFnSc1d1	zavedena
farmakovigilanční	farmakovigilanční	k2eAgFnSc1d1	farmakovigilanční
rubrika	rubrika	k1gFnSc1	rubrika
Černý	Černý	k1gMnSc1	Černý
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Redakční	redakční	k2eAgFnSc4d1	redakční
radu	rada	k1gFnSc4	rada
Věstníku	věstník	k1gInSc2	věstník
SÚKL	SÚKL	kA	SÚKL
tvoří	tvořit	k5eAaImIp3nS	tvořit
ředitel	ředitel	k1gMnSc1	ředitel
ústavu	ústav	k1gInSc2	ústav
a	a	k8xC	a
vedoucí	vedoucí	k2eAgMnPc1d1	vedoucí
pracovníci	pracovník	k1gMnPc1	pracovník
útvarů	útvar	k1gInPc2	útvar
přímo	přímo	k6eAd1	přímo
řízených	řízený	k2eAgInPc2d1	řízený
ředitelem	ředitel	k1gMnSc7	ředitel
ústavu	ústav	k1gInSc2	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
pracovníci	pracovník	k1gMnPc1	pracovník
jsou	být	k5eAaImIp3nP	být
odpovědni	odpověden	k2eAgMnPc1d1	odpověden
za	za	k7c4	za
pravidelné	pravidelný	k2eAgFnPc4d1	pravidelná
rubriky	rubrika	k1gFnPc4	rubrika
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
náplní	náplň	k1gFnSc7	náplň
a	a	k8xC	a
za	za	k7c4	za
příspěvky	příspěvek	k1gInPc4	příspěvek
nepravidelné	pravidelný	k2eNgNnSc1d1	nepravidelné
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
publikování	publikování	k1gNnSc1	publikování
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
redakční	redakční	k2eAgFnSc1d1	redakční
rada	rada	k1gFnSc1	rada
na	na	k7c6	na
pravidelné	pravidelný	k2eAgFnSc6d1	pravidelná
poradě	porada	k1gFnSc6	porada
výkonného	výkonný	k2eAgInSc2d1	výkonný
managementu	management	k1gInSc2	management
SÚKL	SÚKL	kA	SÚKL
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInSc1d1	státní
ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
léčiv	léčivo	k1gNnPc2	léčivo
vydává	vydávat	k5eAaPmIp3nS	vydávat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
Věstník	věstník	k1gInSc1	věstník
SÚKL	SÚKL	kA	SÚKL
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
elektronické	elektronický	k2eAgFnSc6d1	elektronická
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgFnPc4d1	další
publikace	publikace	k1gFnPc4	publikace
===	===	k?	===
</s>
</p>
<p>
<s>
Bulletin	bulletin	k1gInSc1	bulletin
Farmakoterapeutické	Farmakoterapeutický	k2eAgFnSc2d1	Farmakoterapeutická
informace	informace	k1gFnSc2	informace
vychází	vycházet	k5eAaImIp3nS	vycházet
měsíčně	měsíčně	k6eAd1	měsíčně
a	a	k8xC	a
jako	jako	k9	jako
nezávislý	závislý	k2eNgInSc1d1	nezávislý
lékový	lékový	k2eAgInSc1d1	lékový
bulletin	bulletin	k1gInSc1	bulletin
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
společnosti	společnost	k1gFnSc2	společnost
lékových	lékový	k2eAgInPc2d1	lékový
bulletinů	bulletin	k1gInPc2	bulletin
(	(	kIx(	(
<g/>
ISDB	ISDB	kA	ISDB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Farmakoterapeutické	Farmakoterapeutický	k2eAgFnPc1d1	Farmakoterapeutická
informace	informace	k1gFnPc1	informace
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgFnP	určit
především	především	k9	především
lékařům	lékař	k1gMnPc3	lékař
a	a	k8xC	a
lékárníkům	lékárník	k1gMnPc3	lékárník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výroční	výroční	k2eAgFnSc1d1	výroční
zpráva	zpráva	k1gFnSc1	zpráva
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
souhrn	souhrn	k1gInSc4	souhrn
všech	všecek	k3xTgFnPc2	všecek
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
činnosti	činnost	k1gFnSc6	činnost
SÚKL	SÚKL	kA	SÚKL
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Informační	informační	k2eAgInSc1d1	informační
zpravodaj	zpravodaj	k1gInSc1	zpravodaj
Nežádoucí	žádoucí	k2eNgInPc1d1	nežádoucí
účinky	účinek	k1gInPc1	účinek
léčiv	léčivo	k1gNnPc2	léčivo
má	mít	k5eAaImIp3nS	mít
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
předávání	předávání	k1gNnSc3	předávání
důležitých	důležitý	k2eAgFnPc2d1	důležitá
informací	informace	k1gFnPc2	informace
z	z	k7c2	z
farmakovigilančního	farmakovigilanční	k2eAgInSc2d1	farmakovigilanční
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
nežádoucích	žádoucí	k2eNgInPc6d1	nežádoucí
účincích	účinek	k1gInPc6	účinek
léčiv	léčivo	k1gNnPc2	léčivo
po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
uvedení	uvedení	k1gNnSc6	uvedení
do	do	k7c2	do
klinické	klinický	k2eAgFnSc2d1	klinická
praxe	praxe	k1gFnSc2	praxe
a	a	k8xC	a
také	také	k9	také
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
plnit	plnit	k5eAaImF	plnit
funkci	funkce	k1gFnSc4	funkce
nástroje	nástroj	k1gInSc2	nástroj
zpětné	zpětný	k2eAgFnSc2d1	zpětná
vazby	vazba	k1gFnSc2	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
rozvoji	rozvoj	k1gInSc3	rozvoj
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
spolupráce	spolupráce	k1gFnSc2	spolupráce
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
soustavného	soustavný	k2eAgNnSc2d1	soustavné
hodnocení	hodnocení	k1gNnSc2	hodnocení
poměru	poměr	k1gInSc2	poměr
přínosů	přínos	k1gInPc2	přínos
a	a	k8xC	a
rizik	riziko	k1gNnPc2	riziko
léčiv	léčivo	k1gNnPc2	léčivo
se	se	k3xPyFc4	se
budete	být	k5eAaImBp2nP	být
moci	moct	k5eAaImF	moct
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
zpravodaje	zpravodaj	k1gInSc2	zpravodaj
setkávat	setkávat	k5eAaImF	setkávat
s	s	k7c7	s
novinkami	novinka	k1gFnPc7	novinka
nejen	nejen	k6eAd1	nejen
z	z	k7c2	z
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Služby	služba	k1gFnPc1	služba
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Odborná	odborný	k2eAgFnSc1d1	odborná
veřejnost	veřejnost	k1gFnSc1	veřejnost
===	===	k?	===
</s>
</p>
<p>
<s>
Spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
odbornou	odborný	k2eAgFnSc7d1	odborná
lékařskou	lékařský	k2eAgFnSc7d1	lékařská
veřejností	veřejnost	k1gFnSc7	veřejnost
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
priorit	priorita	k1gFnPc2	priorita
SÚKL	SÚKL	kA	SÚKL
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
internetových	internetový	k2eAgFnPc6d1	internetová
stránkách	stránka	k1gFnPc6	stránka
najdou	najít	k5eAaPmIp3nP	najít
např.	např.	kA	např.
lékaři	lékař	k1gMnPc7	lékař
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgMnPc4d1	jiný
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
důležité	důležitý	k2eAgFnPc1d1	důležitá
informace	informace	k1gFnPc1	informace
a	a	k8xC	a
upozornění	upozornění	k1gNnSc1	upozornění
SÚKL	SÚKL	kA	SÚKL
(	(	kIx(	(
<g/>
např.	např.	kA	např.
závady	závada	k1gFnSc2	závada
v	v	k7c6	v
jakosti	jakost	k1gFnSc6	jakost
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
,	,	kIx,	,
nežádoucí	žádoucí	k2eNgInPc4d1	nežádoucí
účinky	účinek	k1gInPc4	účinek
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
,	,	kIx,	,
nové	nový	k2eAgFnPc4d1	nová
informace	informace	k1gFnPc4	informace
k	k	k7c3	k
bezpečnosti	bezpečnost	k1gFnSc3	bezpečnost
léčiv	léčivo	k1gNnPc2	léčivo
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
databáze	databáze	k1gFnSc1	databáze
registrovaných	registrovaný	k2eAgNnPc2d1	registrované
léčiv	léčivo	k1gNnPc2	léčivo
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
souhrnu	souhrn	k1gInSc2	souhrn
informací	informace	k1gFnPc2	informace
o	o	k7c4	o
přípravku	přípravka	k1gFnSc4	přípravka
<g/>
,	,	kIx,	,
příbalového	příbalový	k2eAgInSc2d1	příbalový
informačního	informační	k2eAgInSc2d1	informační
letáku	leták	k1gInSc2	leták
a	a	k8xC	a
cenových	cenový	k2eAgFnPc2d1	cenová
a	a	k8xC	a
úhradových	úhradový	k2eAgInPc6d1	úhradový
údajích	údaj	k1gInPc6	údaj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
přehledy	přehled	k1gInPc1	přehled
schválených	schválený	k2eAgFnPc2d1	schválená
lékáren	lékárna	k1gFnPc2	lékárna
<g/>
,	,	kIx,	,
distributorů	distributor	k1gMnPc2	distributor
<g/>
,	,	kIx,	,
výrobců	výrobce	k1gMnPc2	výrobce
a	a	k8xC	a
kontrolních	kontrolní	k2eAgFnPc2d1	kontrolní
laboratoří	laboratoř	k1gFnPc2	laboratoř
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
přehledy	přehled	k1gInPc1	přehled
zařízení	zařízení	k1gNnSc2	zařízení
transfuzní	transfuzní	k2eAgFnSc2d1	transfuzní
služby	služba	k1gFnSc2	služba
a	a	k8xC	a
krevních	krevní	k2eAgFnPc2d1	krevní
bank	banka	k1gFnPc2	banka
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
spotřeby	spotřeba	k1gFnPc4	spotřeba
a	a	k8xC	a
jiná	jiný	k2eAgNnPc4d1	jiné
hodnocení	hodnocení	k1gNnPc4	hodnocení
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
formuláře	formulář	k1gInPc1	formulář
pro	pro	k7c4	pro
hlášení	hlášení	k1gNnSc4	hlášení
pro	pro	k7c4	pro
SÚKL	SÚKL	kA	SÚKL
(	(	kIx(	(
<g/>
např.	např.	kA	např.
hlášení	hlášení	k1gNnSc4	hlášení
podezření	podezření	k1gNnSc2	podezření
na	na	k7c4	na
nežádoucí	žádoucí	k2eNgInSc4d1	nežádoucí
účinek	účinek	k1gInSc4	účinek
léku	lék	k1gInSc2	lék
<g/>
,	,	kIx,	,
hlášení	hlášení	k1gNnSc2	hlášení
použití	použití	k1gNnSc2	použití
neregistrovaného	registrovaný	k2eNgInSc2d1	neregistrovaný
léčivého	léčivý	k2eAgInSc2d1	léčivý
přípravku	přípravek	k1gInSc2	přípravek
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
formulář	formulář	k1gInSc1	formulář
pro	pro	k7c4	pro
dotazy	dotaz	k1gInPc4	dotaz
a	a	k8xC	a
</s>
</p>
<p>
<s>
kontaktní	kontaktní	k2eAgFnSc1d1	kontaktní
informace	informace	k1gFnSc1	informace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Široká	široký	k2eAgFnSc1d1	široká
veřejnost	veřejnost	k1gFnSc1	veřejnost
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Portál	portál	k1gInSc4	portál
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
====	====	k?	====
</s>
</p>
<p>
<s>
Informační	informační	k2eAgInSc1d1	informační
portál	portál	k1gInSc1	portál
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc1	veřejnost
pacienty	pacient	k1gMnPc4	pacient
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
s	s	k7c7	s
nově	nově	k6eAd1	nově
uváděnými	uváděný	k2eAgInPc7d1	uváděný
léky	lék	k1gInPc7	lék
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trh	trh	k1gInSc4	trh
<g/>
,	,	kIx,	,
s	s	k7c7	s
pravidly	pravidlo	k1gNnPc7	pravidlo
výdeje	výdej	k1gInSc2	výdej
nových	nový	k2eAgNnPc2d1	nové
i	i	k8xC	i
již	již	k6eAd1	již
zavedených	zavedený	k2eAgInPc2d1	zavedený
léků	lék	k1gInPc2	lék
a	a	k8xC	a
s	s	k7c7	s
jejich	jejich	k3xOp3gNnSc7	jejich
působením	působení	k1gNnSc7	působení
na	na	k7c4	na
lidský	lidský	k2eAgInSc4d1	lidský
organismus	organismus	k1gInSc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
SÚKL	SÚKL	kA	SÚKL
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
informačním	informační	k2eAgInSc7d1	informační
portálem	portál	k1gInSc7	portál
snaží	snažit	k5eAaImIp3nS	snažit
srozumitelnou	srozumitelný	k2eAgFnSc7d1	srozumitelná
formou	forma	k1gFnSc7	forma
přiblížit	přiblížit	k5eAaPmF	přiblížit
široké	široký	k2eAgFnSc2d1	široká
veřejnosti	veřejnost	k1gFnSc2	veřejnost
problematiku	problematika	k1gFnSc4	problematika
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
<g/>
.	.	kIx.	.
</s>
<s>
Informační	informační	k2eAgInSc1d1	informační
portál	portál	k1gInSc1	portál
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
hlavních	hlavní	k2eAgInPc2d1	hlavní
modulů	modul	k1gInPc2	modul
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgNnPc6	který
pacient	pacient	k1gMnSc1	pacient
může	moct	k5eAaImIp3nS	moct
najít	najít	k5eAaPmF	najít
odpovědi	odpověď	k1gFnPc4	odpověď
na	na	k7c4	na
často	často	k6eAd1	často
kladené	kladený	k2eAgFnPc4d1	kladená
otázky	otázka	k1gFnPc4	otázka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Léky	lék	k1gInPc1	lék
</s>
</p>
<p>
<s>
Klinické	klinický	k2eAgFnPc1d1	klinická
studie	studie	k1gFnPc1	studie
</s>
</p>
<p>
<s>
Lékárny	lékárna	k1gFnPc1	lékárna
</s>
</p>
<p>
<s>
Zdravotnické	zdravotnický	k2eAgInPc1d1	zdravotnický
potřebya	potřebya	k6eAd1	potřebya
čtyř	čtyři	k4xCgInPc2	čtyři
vedlejších	vedlejší	k2eAgInPc2d1	vedlejší
modulů	modul	k1gInPc2	modul
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Zdraví	zdraví	k1gNnSc1	zdraví
<g/>
,	,	kIx,	,
nemoc	nemoc	k1gFnSc1	nemoc
a	a	k8xC	a
léky	lék	k1gInPc1	lék
</s>
</p>
<p>
<s>
SÚKL	SÚKL	kA	SÚKL
vás	vy	k3xPp2nPc4	vy
chrání	chránit	k5eAaImIp3nS	chránit
</s>
</p>
<p>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
SÚKL	SÚKL	kA	SÚKL
</s>
</p>
<p>
<s>
SÚKL	SÚKL	kA	SÚKL
a	a	k8xC	a
médiaJe	médiaJat	k5eAaPmIp3nS	médiaJat
možno	možno	k6eAd1	možno
zde	zde	k6eAd1	zde
nalézt	nalézt	k5eAaPmF	nalézt
úplný	úplný	k2eAgInSc1d1	úplný
přehled	přehled	k1gInSc1	přehled
všech	všecek	k3xTgInPc2	všecek
registrovaných	registrovaný	k2eAgInPc2d1	registrovaný
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
<g/>
,	,	kIx,	,
schválených	schválený	k2eAgFnPc2d1	schválená
klinických	klinický	k2eAgFnPc2d1	klinická
studií	studie	k1gFnPc2	studie
a	a	k8xC	a
přehled	přehled	k1gInSc4	přehled
lékáren	lékárna	k1gFnPc2	lékárna
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
je	být	k5eAaImIp3nS	být
připravena	připravit	k5eAaPmNgFnS	připravit
i	i	k9	i
databáze	databáze	k1gFnSc1	databáze
volně	volně	k6eAd1	volně
prodejných	prodejný	k2eAgNnPc2d1	prodejné
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
snazší	snadný	k2eAgFnSc4d2	snazší
orientaci	orientace	k1gFnSc4	orientace
je	být	k5eAaImIp3nS	být
připraven	připravit	k5eAaPmNgInS	připravit
i	i	k9	i
slovníček	slovníček	k1gInSc1	slovníček
a	a	k8xC	a
soubor	soubor	k1gInSc1	soubor
nejčastějších	častý	k2eAgFnPc2d3	nejčastější
otázek	otázka	k1gFnPc2	otázka
a	a	k8xC	a
odpovědí	odpověď	k1gFnPc2	odpověď
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
portál	portál	k1gInSc1	portál
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
využíván	využívat	k5eAaPmNgInS	využívat
také	také	k9	také
pro	pro	k7c4	pro
informování	informování	k1gNnSc4	informování
veřejnosti	veřejnost	k1gFnSc2	veřejnost
například	například	k6eAd1	například
o	o	k7c6	o
přístupu	přístup	k1gInSc6	přístup
občanů	občan	k1gMnPc2	občan
k	k	k7c3	k
osobním	osobní	k2eAgInPc3d1	osobní
lékovým	lékový	k2eAgInPc3d1	lékový
záznamům	záznam	k1gInPc3	záznam
v	v	k7c6	v
centrálním	centrální	k2eAgNnSc6d1	centrální
úložišti	úložiště	k1gNnSc6	úložiště
elektronických	elektronický	k2eAgInPc2d1	elektronický
receptů	recept	k1gInPc2	recept
nebo	nebo	k8xC	nebo
léčivých	léčivý	k2eAgInPc6d1	léčivý
přípravcích	přípravek	k1gInPc6	přípravek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
budou	být	k5eAaImBp3nP	být
nově	nově	k6eAd1	nově
dostupné	dostupný	k2eAgFnPc1d1	dostupná
bez	bez	k7c2	bez
lékařského	lékařský	k2eAgInSc2d1	lékařský
předpisu	předpis	k1gInSc2	předpis
s	s	k7c7	s
omezením	omezení	k1gNnSc7	omezení
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
novinky	novinka	k1gFnPc1	novinka
jsou	být	k5eAaImIp3nP	být
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
informační	informační	k2eAgInSc1d1	informační
portál	portál	k1gInSc1	portál
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
zaměřeny	zaměřit	k5eAaPmNgInP	zaměřit
na	na	k7c4	na
emancipaci	emancipace	k1gFnSc4	emancipace
pacientů	pacient	k1gMnPc2	pacient
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc3	jejich
lepší	dobrý	k2eAgFnSc3d2	lepší
orientaci	orientace	k1gFnSc3	orientace
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
.	.	kIx.	.
</s>
<s>
Informační	informační	k2eAgInSc1d1	informační
portál	portál	k1gInSc1	portál
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
neustále	neustále	k6eAd1	neustále
aktualizován	aktualizován	k2eAgInSc1d1	aktualizován
a	a	k8xC	a
rozšiřován	rozšiřován	k2eAgInSc1d1	rozšiřován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Databáze	databáze	k1gFnSc1	databáze
všech	všecek	k3xTgInPc2	všecek
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
====	====	k?	====
</s>
</p>
<p>
<s>
Databáze	databáze	k1gFnSc1	databáze
registrovaných	registrovaný	k2eAgInPc2d1	registrovaný
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
všechna	všechen	k3xTgNnPc4	všechen
léčiva	léčivo	k1gNnPc4	léčivo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
registrována	registrován	k2eAgNnPc1d1	registrováno
k	k	k7c3	k
užití	užití	k1gNnSc3	užití
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
pro	pro	k7c4	pro
zvláštní	zvláštní	k2eAgInPc4d1	zvláštní
lékařské	lékařský	k2eAgInPc4d1	lékařský
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
tak	tak	k9	tak
A	a	k9	a
volně	volně	k6eAd1	volně
prodejné	prodejný	k2eAgInPc1d1	prodejný
léčivé	léčivý	k2eAgInPc1d1	léčivý
přípravky	přípravek	k1gInPc1	přípravek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Databáze	databáze	k1gFnPc1	databáze
všech	všecek	k3xTgFnPc2	všecek
lékáren	lékárna	k1gFnPc2	lékárna
====	====	k?	====
</s>
</p>
<p>
<s>
Databáze	databáze	k1gFnPc1	databáze
všech	všecek	k3xTgFnPc2	všecek
lékáren	lékárna	k1gFnPc2	lékárna
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
600	[number]	k4	600
lékárnách	lékárna	k1gFnPc6	lékárna
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
provoz	provoz	k1gInSc1	provoz
schválil	schválit	k5eAaPmAgInS	schválit
SÚKL	SÚKL	kA	SÚKL
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Databáze	databáze	k1gFnSc2	databáze
schválených	schválený	k2eAgNnPc2d1	schválené
klinických	klinický	k2eAgNnPc2d1	klinické
hodnocení	hodnocení	k1gNnPc2	hodnocení
====	====	k?	====
</s>
</p>
<p>
<s>
SÚKL	SÚKL	kA	SÚKL
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
zpřístupňuje	zpřístupňovat	k5eAaImIp3nS	zpřístupňovat
databázi	databáze	k1gFnSc3	databáze
klinických	klinický	k2eAgNnPc2d1	klinické
studií	studio	k1gNnPc2	studio
povolených	povolený	k2eAgNnPc2d1	povolené
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Klinické	klinický	k2eAgFnPc1d1	klinická
studie	studie	k1gFnPc1	studie
(	(	kIx(	(
<g/>
klinická	klinický	k2eAgNnPc1d1	klinické
hodnocení	hodnocení	k1gNnPc1	hodnocení
<g/>
)	)	kIx)	)
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
prokázání	prokázání	k1gNnSc3	prokázání
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
,	,	kIx,	,
účinnosti	účinnost	k1gFnSc2	účinnost
nebo	nebo	k8xC	nebo
jakosti	jakost	k1gFnSc2	jakost
nových	nový	k2eAgInPc2d1	nový
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Očkovací	očkovací	k2eAgInSc4d1	očkovací
kalendář	kalendář	k1gInSc4	kalendář
====	====	k?	====
</s>
</p>
<p>
<s>
Interaktivní	interaktivní	k2eAgInSc1d1	interaktivní
Očkovací	očkovací	k2eAgInSc1d1	očkovací
kalendář	kalendář	k1gInSc1	kalendář
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
informace	informace	k1gFnPc4	informace
o	o	k7c4	o
očkování	očkování	k1gNnSc4	očkování
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
sestaven	sestavit	k5eAaPmNgInS	sestavit
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
úřadem	úřad	k1gInSc7	úřad
hlavního	hlavní	k2eAgMnSc2d1	hlavní
hygienika	hygienik	k1gMnSc2	hygienik
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
zjistit	zjistit	k5eAaPmF	zjistit
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
očkování	očkování	k1gNnSc6	očkování
a	a	k8xC	a
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
vakcínách	vakcína	k1gFnPc6	vakcína
<g/>
,	,	kIx,	,
o	o	k7c6	o
harmonogramu	harmonogram	k1gInSc6	harmonogram
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
povinných	povinný	k2eAgNnPc2d1	povinné
i	i	k8xC	i
dobrovolných	dobrovolný	k2eAgNnPc2d1	dobrovolné
očkování	očkování	k1gNnPc2	očkování
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
lepší	dobrý	k2eAgFnSc4d2	lepší
orientaci	orientace	k1gFnSc4	orientace
jsou	být	k5eAaImIp3nP	být
povinná	povinný	k2eAgNnPc1d1	povinné
očkování	očkování	k1gNnPc1	očkování
znázorněna	znázornit	k5eAaPmNgFnS	znázornit
červeným	červený	k2eAgNnSc7d1	červené
písmem	písmo	k1gNnSc7	písmo
<g/>
,	,	kIx,	,
nepovinná	povinný	k2eNgFnSc1d1	nepovinná
pak	pak	k6eAd1	pak
modrým	modrý	k2eAgInSc7d1	modrý
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
kalendář	kalendář	k1gInSc1	kalendář
přehledně	přehledně	k6eAd1	přehledně
strukturován	strukturovat	k5eAaImNgInS	strukturovat
podle	podle	k7c2	podle
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
onemocnění	onemocnění	k1gNnSc2	onemocnění
či	či	k8xC	či
očkovací	očkovací	k2eAgFnSc2d1	očkovací
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
každého	každý	k3xTgNnSc2	každý
očkování	očkování	k1gNnSc2	očkování
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
vakcíny	vakcína	k1gFnPc1	vakcína
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
schváleny	schválen	k2eAgFnPc1d1	schválena
a	a	k8xC	a
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
aktuálně	aktuálně	k6eAd1	aktuálně
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trhu	trh	k1gInSc6	trh
dostupné	dostupný	k2eAgNnSc1d1	dostupné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
registrovaných	registrovaný	k2eAgFnPc2d1	registrovaná
vakcín	vakcína	k1gFnPc2	vakcína
lze	lze	k6eAd1	lze
zjistit	zjistit	k5eAaPmF	zjistit
detailní	detailní	k2eAgFnPc4d1	detailní
informace	informace	k1gFnPc4	informace
z	z	k7c2	z
elektronického	elektronický	k2eAgInSc2d1	elektronický
příbalového	příbalový	k2eAgInSc2d1	příbalový
letáku	leták	k1gInSc2	leták
nebo	nebo	k8xC	nebo
souhrnu	souhrn	k1gInSc6	souhrn
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
aplikovaném	aplikovaný	k2eAgInSc6d1	aplikovaný
přípravku	přípravek	k1gInSc6	přípravek
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
kalendáře	kalendář	k1gInSc2	kalendář
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
na	na	k7c6	na
informačním	informační	k2eAgInSc6d1	informační
portálu	portál	k1gInSc6	portál
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
prostudovat	prostudovat	k5eAaPmF	prostudovat
rady	rada	k1gFnPc4	rada
určené	určený	k2eAgFnPc4d1	určená
speciálně	speciálně	k6eAd1	speciálně
rodičům	rodič	k1gMnPc3	rodič
očkovaných	očkovaný	k2eAgFnPc2d1	očkovaná
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Kalendář	kalendář	k1gInSc1	kalendář
cestovatele	cestovatel	k1gMnSc2	cestovatel
====	====	k?	====
</s>
</p>
<p>
<s>
Kalendář	kalendář	k1gInSc1	kalendář
cestovatele	cestovatel	k1gMnSc2	cestovatel
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nejen	nejen	k6eAd1	nejen
informace	informace	k1gFnSc1	informace
k	k	k7c3	k
očkováním	očkování	k1gNnPc3	očkování
doporučeným	doporučený	k2eAgInSc7d1	doporučený
Světovou	světový	k2eAgFnSc7d1	světová
zdravotnickou	zdravotnický	k2eAgFnSc7d1	zdravotnická
organizací	organizace	k1gFnSc7	organizace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
schválených	schválený	k2eAgFnPc6d1	schválená
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
aktuálně	aktuálně	k6eAd1	aktuálně
obchodovaných	obchodovaný	k2eAgFnPc6d1	obchodovaná
vakcínách	vakcína	k1gFnPc6	vakcína
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
kalendáře	kalendář	k1gInSc2	kalendář
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
také	také	k9	také
dostat	dostat	k5eAaPmF	dostat
na	na	k7c4	na
praktické	praktický	k2eAgFnPc4d1	praktická
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
cizí	cizí	k2eAgFnSc6d1	cizí
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
publikované	publikovaný	k2eAgNnSc4d1	publikované
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Řízené	řízený	k2eAgNnSc4d1	řízené
dotazování	dotazování	k1gNnSc4	dotazování
a	a	k8xC	a
dotazování	dotazování	k1gNnSc4	dotazování
odborníků	odborník	k1gMnPc2	odborník
====	====	k?	====
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
nelze	lze	k6eNd1	lze
najít	najít	k5eAaPmF	najít
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
informaci	informace	k1gFnSc4	informace
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
"	"	kIx"	"
<g/>
řízené	řízený	k2eAgNnSc1d1	řízené
dotazování	dotazování	k1gNnSc1	dotazování
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dotaz	dotaz	k1gInSc1	dotaz
bude	být	k5eAaImBp3nS	být
odeslán	odeslat	k5eAaPmNgInS	odeslat
do	do	k7c2	do
informačního	informační	k2eAgNnSc2d1	informační
oddělení	oddělení	k1gNnSc2	oddělení
Státního	státní	k2eAgInSc2d1	státní
ústavu	ústav	k1gInSc2	ústav
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
spuštěna	spustit	k5eAaPmNgFnS	spustit
další	další	k2eAgFnSc1d1	další
služba	služba	k1gFnSc1	služba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
portálu	portál	k1gInSc2	portál
umožní	umožnit	k5eAaPmIp3nS	umožnit
ptát	ptát	k5eAaImF	ptát
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
vybraných	vybraný	k2eAgMnPc2d1	vybraný
odborníků	odborník	k1gMnPc2	odborník
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
lékařství	lékařství	k1gNnSc2	lékařství
a	a	k8xC	a
farmacie	farmacie	k1gFnSc2	farmacie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
služba	služba	k1gFnSc1	služba
informačního	informační	k2eAgInSc2d1	informační
portálu	portál	k1gInSc2	portál
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
nicméně	nicméně	k8xC	nicméně
nenahrazuje	nahrazovat	k5eNaImIp3nS	nahrazovat
osobní	osobní	k2eAgFnSc4d1	osobní
konzultaci	konzultace	k1gFnSc4	konzultace
s	s	k7c7	s
lékařem	lékař	k1gMnSc7	lékař
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Portál	portál	k1gInSc1	portál
Nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
léky	lék	k1gInPc1	lék
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
spuštěna	spustit	k5eAaPmNgFnS	spustit
velká	velký	k2eAgFnSc1d1	velká
informační	informační	k2eAgFnSc1d1	informační
kampaň	kampaň	k1gFnSc1	kampaň
"	"	kIx"	"
<g/>
Nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
léky	lék	k1gInPc1	lék
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
proti	proti	k7c3	proti
padělkům	padělek	k1gInPc3	padělek
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
,	,	kIx,	,
nelegálním	legální	k2eNgInPc3d1	nelegální
přípravkům	přípravek	k1gInPc3	přípravek
a	a	k8xC	a
nebezpečným	bezpečný	k2eNgInPc3d1	nebezpečný
lékům	lék	k1gInPc3	lék
obecně	obecně	k6eAd1	obecně
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
upozornit	upozornit	k5eAaPmF	upozornit
širokou	široký	k2eAgFnSc4d1	široká
veřejnost	veřejnost	k1gFnSc4	veřejnost
na	na	k7c4	na
možná	možný	k2eAgNnPc4d1	možné
rizika	riziko	k1gNnPc4	riziko
nákupu	nákup	k1gInSc2	nákup
léků	lék	k1gInPc2	lék
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kampaně	kampaň	k1gFnSc2	kampaň
byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
spuštěn	spustit	k5eAaPmNgInS	spustit
portál	portál	k1gInSc1	portál
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
obsahující	obsahující	k2eAgFnSc2d1	obsahující
informace	informace	k1gFnSc2	informace
k	k	k7c3	k
problematice	problematika	k1gFnSc3	problematika
<g/>
:	:	kIx,	:
manuál	manuál	k1gInSc1	manuál
správného	správný	k2eAgNnSc2d1	správné
nakupování	nakupování	k1gNnSc2	nakupování
léků	lék	k1gInPc2	lék
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
,	,	kIx,	,
seznam	seznam	k1gInSc1	seznam
certifikovaných	certifikovaný	k2eAgFnPc2d1	certifikovaná
lékáren	lékárna	k1gFnPc2	lékárna
s	s	k7c7	s
povoleným	povolený	k2eAgInSc7d1	povolený
online	onlinout	k5eAaPmIp3nS	onlinout
prodejem	prodej	k1gInSc7	prodej
léků	lék	k1gInPc2	lék
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInPc1d1	poslední
zadržené	zadržený	k2eAgInPc1d1	zadržený
padělky	padělek	k1gInPc1	padělek
léčiv	léčivo	k1gNnPc2	léčivo
či	či	k8xC	či
nelegální	legální	k2eNgInPc4d1	nelegální
přípravky	přípravek	k1gInPc4	přípravek
<g/>
,	,	kIx,	,
vyřešené	vyřešený	k2eAgInPc4d1	vyřešený
případy	případ	k1gInPc4	případ
<g/>
,	,	kIx,	,
ukázky	ukázka	k1gFnPc4	ukázka
kampaní	kampaň	k1gFnPc2	kampaň
jiných	jiný	k2eAgInPc2d1	jiný
evropských	evropský	k2eAgInPc2d1	evropský
států	stát	k1gInPc2	stát
nebo	nebo	k8xC	nebo
video	video	k1gNnSc4	video
razie	razie	k1gFnSc2	razie
do	do	k7c2	do
nelegální	legální	k2eNgFnSc2d1	nelegální
výrobny	výrobna	k1gFnSc2	výrobna
léků	lék	k1gInPc2	lék
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
SÚKL	SÚKL	kA	SÚKL
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kampaně	kampaň	k1gFnSc2	kampaň
připravil	připravit	k5eAaPmAgInS	připravit
pro	pro	k7c4	pro
certifikované	certifikovaný	k2eAgFnPc4d1	certifikovaná
lékárny	lékárna	k1gFnPc4	lékárna
s	s	k7c7	s
povoleným	povolený	k2eAgInSc7d1	povolený
internetovým	internetový	k2eAgInSc7d1	internetový
prodejem	prodej	k1gInSc7	prodej
léků	lék	k1gInPc2	lék
možnost	možnost	k1gFnSc4	možnost
sdílet	sdílet	k5eAaImF	sdílet
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
internetových	internetový	k2eAgFnPc6d1	internetová
stránkách	stránka	k1gFnPc6	stránka
banner	banner	k1gInSc4	banner
SÚKL	SÚKL	kA	SÚKL
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
k	k	k7c3	k
ověření	ověření	k1gNnSc3	ověření
lékárny	lékárna	k1gFnSc2	lékárna
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
schválených	schválený	k2eAgFnPc2d1	schválená
lékáren	lékárna	k1gFnPc2	lékárna
zveřejněné	zveřejněný	k2eAgFnPc4d1	zveřejněná
na	na	k7c6	na
webu	web	k1gInSc6	web
SÚKL	SÚKL	kA	SÚKL
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Centrální	centrální	k2eAgNnSc1d1	centrální
úložiště	úložiště	k1gNnSc1	úložiště
receptů	recept	k1gInPc2	recept
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2009	[number]	k4	2009
spravuje	spravovat	k5eAaImIp3nS	spravovat
SÚKL	SÚKL	kA	SÚKL
tzv.	tzv.	kA	tzv.
centrální	centrální	k2eAgNnSc1d1	centrální
úložiště	úložiště	k1gNnSc1	úložiště
lékařských	lékařský	k2eAgInPc2d1	lékařský
předpisů	předpis	k1gInPc2	předpis
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yIgMnSc2	který
mají	mít	k5eAaImIp3nP	mít
lékárny	lékárna	k1gFnPc1	lékárna
ukládat	ukládat	k5eAaImF	ukládat
vybraná	vybraný	k2eAgNnPc1d1	vybrané
data	datum	k1gNnPc1	datum
o	o	k7c6	o
pacientech	pacient	k1gMnPc6	pacient
–	–	k?	–
kdo	kdo	k3yQnSc1	kdo
lék	lék	k1gInSc4	lék
předepsal	předepsat	k5eAaPmAgMnS	předepsat
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
jej	on	k3xPp3gMnSc4	on
vydal	vydat	k5eAaPmAgMnS	vydat
a	a	k8xC	a
kdo	kdo	k3yQnSc1	kdo
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Ředitel	ředitel	k1gMnSc1	ředitel
SÚKL	SÚKL	kA	SÚKL
sice	sice	k8xC	sice
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
veškerá	veškerý	k3xTgNnPc1	veškerý
data	datum	k1gNnPc1	datum
o	o	k7c6	o
pacientech	pacient	k1gMnPc6	pacient
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
anonymizované	anonymizovaný	k2eAgFnSc6d1	anonymizovaná
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
projekt	projekt	k1gInSc1	projekt
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
Česká	český	k2eAgFnSc1d1	Česká
lékárnická	lékárnický	k2eAgFnSc1d1	lékárnická
komora	komora	k1gFnSc1	komora
<g/>
.	.	kIx.	.
</s>
<s>
Podala	podat	k5eAaPmAgFnS	podat
podnět	podnět	k1gInSc4	podnět
k	k	k7c3	k
prošetření	prošetření	k1gNnSc3	prošetření
k	k	k7c3	k
Úřadu	úřad	k1gInSc3	úřad
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
osobních	osobní	k2eAgInPc2d1	osobní
údajů	údaj	k1gInPc2	údaj
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zákon	zákon	k1gInSc1	zákon
prý	prý	k9	prý
takový	takový	k3xDgInSc4	takový
sběr	sběr	k1gInSc4	sběr
dat	datum	k1gNnPc2	datum
neumožňuje	umožňovat	k5eNaImIp3nS	umožňovat
<g/>
.	.	kIx.	.
</s>
<s>
Lékárnická	lékárnický	k2eAgFnSc1d1	lékárnická
komora	komora	k1gFnSc1	komora
proto	proto	k8xC	proto
také	také	k6eAd1	také
vyzvala	vyzvat	k5eAaPmAgFnS	vyzvat
své	svůj	k3xOyFgMnPc4	svůj
členy	člen	k1gMnPc4	člen
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
k	k	k7c3	k
systému	systém	k1gInSc3	systém
nepřipojovali	připojovat	k5eNaImAgMnP	připojovat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
zacházení	zacházení	k1gNnSc2	zacházení
s	s	k7c7	s
citlivými	citlivý	k2eAgNnPc7d1	citlivé
daty	datum	k1gNnPc7	datum
pacientů	pacient	k1gMnPc2	pacient
nevyřeší	vyřešit	k5eNaPmIp3nS	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
srpna	srpen	k1gInSc2	srpen
2009	[number]	k4	2009
označil	označit	k5eAaPmAgInS	označit
sběr	sběr	k1gInSc1	sběr
dat	datum	k1gNnPc2	datum
o	o	k7c6	o
zdraví	zdraví	k1gNnSc6	zdraví
pacientů	pacient	k1gMnPc2	pacient
za	za	k7c4	za
nezákonný	zákonný	k2eNgInSc4d1	nezákonný
Úřad	úřad	k1gInSc4	úřad
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
osobních	osobní	k2eAgInPc2d1	osobní
údajů	údaj	k1gInPc2	údaj
<g/>
.	.	kIx.	.
</s>
<s>
Uvedené	uvedený	k2eAgNnSc1d1	uvedené
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
Úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
osobních	osobní	k2eAgInPc2d1	osobní
údajů	údaj	k1gInPc2	údaj
dosud	dosud	k6eAd1	dosud
není	být	k5eNaImIp3nS	být
pravomocně	pravomocně	k6eAd1	pravomocně
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jej	on	k3xPp3gNnSc4	on
SÚKL	SÚKL	kA	SÚKL
napadl	napadnout	k5eAaPmAgInS	napadnout
rozkladem	rozklad	k1gInSc7	rozklad
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yQgMnSc6	který
dosud	dosud	k6eAd1	dosud
nebylo	být	k5eNaImAgNnS	být
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Úřadu	úřad	k1gInSc2	úřad
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Státní	státní	k2eAgInSc4d1	státní
ústav	ústav	k1gInSc4	ústav
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
léčiv	léčivo	k1gNnPc2	léčivo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
Státního	státní	k2eAgInSc2d1	státní
ústavu	ústav	k1gInSc2	ústav
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
léčiv	léčivo	k1gNnPc2	léčivo
</s>
</p>
<p>
<s>
Pacientský	pacientský	k2eAgInSc1d1	pacientský
portál	portál	k1gInSc1	portál
</s>
</p>
<p>
<s>
Informační	informační	k2eAgFnSc1d1	informační
kampaň	kampaň	k1gFnSc1	kampaň
"	"	kIx"	"
<g/>
Nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
léky	lék	k1gInPc1	lék
<g/>
"	"	kIx"	"
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
proti	proti	k7c3	proti
padělkům	padělek	k1gInPc3	padělek
a	a	k8xC	a
nelegálním	legální	k2eNgInPc3d1	nelegální
léčivým	léčivý	k2eAgInPc3d1	léčivý
přípravkům	přípravek	k1gInPc3	přípravek
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Státní	státní	k2eAgFnSc1d1	státní
agentura	agentura	k1gFnSc1	agentura
pro	pro	k7c4	pro
konopí	konopí	k1gNnSc4	konopí
pro	pro	k7c4	pro
léčebné	léčebný	k2eAgNnSc4d1	léčebné
použití	použití	k1gNnSc4	použití
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
státní	státní	k2eAgFnSc4d1	státní
kontrolu	kontrola	k1gFnSc4	kontrola
veterinárních	veterinární	k2eAgMnPc2d1	veterinární
biopreparátů	biopreparát	k1gInPc2	biopreparát
a	a	k8xC	a
léčiv	léčivo	k1gNnPc2	léčivo
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
zdravotnických	zdravotnický	k2eAgFnPc2d1	zdravotnická
informací	informace	k1gFnPc2	informace
a	a	k8xC	a
statistiky	statistika	k1gFnSc2	statistika
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
ústav	ústav	k1gInSc1	ústav
</s>
</p>
