<s>
Altair	Altair	k1gInSc1	Altair
8800	[number]	k4	8800
byl	být	k5eAaImAgInS	být
8	[number]	k4	8
<g/>
bitový	bitový	k2eAgInSc1d1	bitový
mikropočítač	mikropočítač	k1gInSc1	mikropočítač
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
procesoru	procesor	k1gInSc6	procesor
Intel	Intel	kA	Intel
8080	[number]	k4	8080
a	a	k8xC	a
vyráběný	vyráběný	k2eAgInSc4d1	vyráběný
společností	společnost	k1gFnSc7	společnost
MITS	MITS	kA	MITS
od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
ve	v	k7c6	v
stavebnicové	stavebnicový	k2eAgFnSc6d1	stavebnicová
verzi	verze	k1gFnSc6	verze
<g/>
.	.	kIx.	.
</s>
<s>
Sběrnice	sběrnice	k1gFnSc1	sběrnice
Altairu	Altair	k1gInSc2	Altair
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
de	de	k?	de
facto	fact	k2eAgNnSc4d1	facto
standardem	standard	k1gInSc7	standard
jako	jako	k8xS	jako
sběrnice	sběrnice	k1gFnSc1	sběrnice
S-	S-	k1gFnSc2	S-
<g/>
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Počítač	počítač	k1gInSc1	počítač
používal	používat	k5eAaImAgInS	používat
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
Altair	Altaira	k1gFnPc2	Altaira
BASIC	Basic	kA	Basic
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pro	pro	k7c4	pro
MITS	MITS	kA	MITS
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Microsoft	Microsoft	kA	Microsoft
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
nízké	nízký	k2eAgFnSc3d1	nízká
ceně	cena	k1gFnSc3	cena
(	(	kIx(	(
<g/>
400	[number]	k4	400
<g/>
$	$	kIx~	$
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
<g/>
.	.	kIx.	.
</s>
<s>
Koncepce	koncepce	k1gFnSc1	koncepce
počítače	počítač	k1gInSc2	počítač
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
modulární	modulární	k2eAgFnSc1d1	modulární
<g/>
,	,	kIx,	,
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
se	se	k3xPyFc4	se
veškeré	veškerý	k3xTgFnPc1	veškerý
komponenty	komponenta	k1gFnPc1	komponenta
připojovaly	připojovat	k5eAaImAgFnP	připojovat
přes	přes	k7c4	přes
sloty	slot	k1gInPc4	slot
na	na	k7c4	na
základní	základní	k2eAgFnSc4d1	základní
desku	deska	k1gFnSc4	deska
pomocí	pomocí	k7c2	pomocí
sběrnice	sběrnice	k1gFnSc2	sběrnice
S-	S-	k1gFnSc2	S-
<g/>
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Procesor	procesor	k1gInSc1	procesor
Intel	Intel	kA	Intel
8080	[number]	k4	8080
byl	být	k5eAaImAgMnS	být
taktovaný	taktovaný	k2eAgMnSc1d1	taktovaný
na	na	k7c4	na
2	[number]	k4	2
MHz	Mhz	kA	Mhz
<g/>
,	,	kIx,	,
paměť	paměť	k1gFnSc1	paměť
měla	mít	k5eAaImAgFnS	mít
velikost	velikost	k1gFnSc4	velikost
256	[number]	k4	256
bytů	byt	k1gInPc2	byt
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
pro	pro	k7c4	pro
provoz	provoz	k1gInSc4	provoz
BASICu	BASICus	k1gInSc2	BASICus
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
paměť	paměť	k1gFnSc4	paměť
rozšířit	rozšířit	k5eAaPmF	rozšířit
přídavnou	přídavný	k2eAgFnSc7d1	přídavná
kartou	karta	k1gFnSc7	karta
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnSc2	datum
se	se	k3xPyFc4	se
ukládala	ukládat	k5eAaImAgFnS	ukládat
na	na	k7c4	na
magnetickou	magnetický	k2eAgFnSc4d1	magnetická
pásku	páska	k1gFnSc4	páska
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
na	na	k7c4	na
diskety	disketa	k1gFnPc4	disketa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
ALTAIR	ALTAIR	kA	ALTAIR
8800	[number]	k4	8800
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Altair	Altair	k1gInSc1	Altair
8800	[number]	k4	8800
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Historie	historie	k1gFnSc2	historie
ALTAIRu	ALTAIRus	k1gInSc2	ALTAIRus
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
