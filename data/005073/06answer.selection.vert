<s>
Altair	Altair	k1gInSc1	Altair
8800	[number]	k4	8800
byl	být	k5eAaImAgInS	být
8	[number]	k4	8
<g/>
bitový	bitový	k2eAgInSc1d1	bitový
mikropočítač	mikropočítač	k1gInSc1	mikropočítač
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
procesoru	procesor	k1gInSc6	procesor
Intel	Intel	kA	Intel
8080	[number]	k4	8080
a	a	k8xC	a
vyráběný	vyráběný	k2eAgInSc4d1	vyráběný
společností	společnost	k1gFnSc7	společnost
MITS	MITS	kA	MITS
od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
ve	v	k7c6	v
stavebnicové	stavebnicový	k2eAgFnSc6d1	stavebnicová
verzi	verze	k1gFnSc6	verze
<g/>
.	.	kIx.	.
</s>
