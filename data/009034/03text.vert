<p>
<s>
Velikonoce	Velikonoce	k1gFnPc1	Velikonoce
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
pascha	pascha	k1gFnSc1	pascha
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
π	π	k?	π
–	–	k?	–
pascha	pascha	k1gFnSc1	pascha
<g/>
,	,	kIx,	,
hebrejsky	hebrejsky	k6eAd1	hebrejsky
פ	פ	k?	פ
<g/>
ֶ	ֶ	k?	ֶ
<g/>
ּ	ּ	k?	ּ
<g/>
ס	ס	k?	ס
<g/>
ַ	ַ	k?	ַ
<g/>
ח	ח	k?	ח
pesach	pesach	k1gInSc1	pesach
–	–	k?	–
přechod	přechod	k1gInSc1	přechod
<g/>
,	,	kIx,	,
přejití	přejití	k1gNnPc1	přejití
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
křesťanským	křesťanský	k2eAgInSc7d1	křesťanský
svátkem	svátek	k1gInSc7	svátek
<g/>
,	,	kIx,	,
oslavou	oslava	k1gFnSc7	oslava
zmrtvýchvstání	zmrtvýchvstání	k1gNnPc2	zmrtvýchvstání
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
jsou	být	k5eAaImIp3nP	být
Velikonoce	Velikonoce	k1gFnPc1	Velikonoce
obdobím	období	k1gNnSc7	období
lidových	lidový	k2eAgFnPc2d1	lidová
tradic	tradice	k1gFnPc2	tradice
spojených	spojený	k2eAgFnPc2d1	spojená
s	s	k7c7	s
vítáním	vítání	k1gNnSc7	vítání
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
s	s	k7c7	s
náboženským	náboženský	k2eAgInSc7d1	náboženský
svátkem	svátek	k1gInSc7	svátek
souvisí	souviset	k5eAaImIp3nS	souviset
jen	jen	k9	jen
volně	volně	k6eAd1	volně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
užším	úzký	k2eAgNnSc6d2	užší
náboženském	náboženský	k2eAgNnSc6d1	náboženské
pojetí	pojetí	k1gNnSc6	pojetí
se	se	k3xPyFc4	se
Velikonocemi	Velikonoce	k1gFnPc7	Velikonoce
míní	mínit	k5eAaImIp3nP	mínit
pouze	pouze	k6eAd1	pouze
slavnost	slavnost	k1gFnSc4	slavnost
Zmrtvýchvstání	zmrtvýchvstání	k1gNnSc2	zmrtvýchvstání
Páně	páně	k2eAgNnSc2d1	páně
neboli	neboli	k8xC	neboli
Vzkříšení	vzkříšení	k1gNnSc2	vzkříšení
Krista	Kristus	k1gMnSc2	Kristus
(	(	kIx(	(
<g/>
Boží	boží	k2eAgInSc1d1	boží
hod	hod	k1gInSc1	hod
velikonoční	velikonoční	k2eAgInSc1d1	velikonoční
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yQgInSc3	který
mělo	mít	k5eAaImAgNnS	mít
dojít	dojít	k5eAaPmF	dojít
třetího	třetí	k4xOgInSc2	třetí
dne	den	k1gInSc2	den
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
ukřižování	ukřižování	k1gNnSc6	ukřižování
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
vigilie	vigilie	k1gFnSc1	vigilie
na	na	k7c4	na
Bílou	bílý	k2eAgFnSc4d1	bílá
sobotu	sobota	k1gFnSc4	sobota
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
velká	velký	k2eAgFnSc1d1	velká
noc	noc	k1gFnSc1	noc
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
širším	široký	k2eAgNnSc6d2	širší
pojetí	pojetí	k1gNnSc6	pojetí
se	se	k3xPyFc4	se
jimi	on	k3xPp3gFnPc7	on
myslí	myslet	k5eAaImIp3nS	myslet
Velikonoční	velikonoční	k2eAgInSc1d1	velikonoční
triduum	triduum	k1gInSc1	triduum
(	(	kIx(	(
<g/>
přičemž	přičemž	k6eAd1	přičemž
období	období	k1gNnSc1	období
od	od	k7c2	od
Zeleného	Zeleného	k2eAgInSc2d1	Zeleného
čtvrtka	čtvrtek	k1gInSc2	čtvrtek
až	až	k9	až
do	do	k7c2	do
sobotní	sobotní	k2eAgFnSc2d1	sobotní
vigilie	vigilie	k1gFnSc2	vigilie
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
součástí	součást	k1gFnSc7	součást
postní	postní	k2eAgFnSc2d1	postní
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ne	ne	k9	ne
doby	doba	k1gFnSc2	doba
velikonoční	velikonoční	k2eAgFnSc2d1	velikonoční
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
pojetí	pojetí	k1gNnSc1	pojetí
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
terminologicky	terminologicky	k6eAd1	terminologicky
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
správné	správný	k2eAgNnSc1d1	správné
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
nejširším	široký	k2eAgInSc6d3	nejširší
smyslu	smysl	k1gInSc6	smysl
pak	pak	k6eAd1	pak
celá	celý	k2eAgFnSc1d1	celá
doba	doba	k1gFnSc1	doba
velikonoční	velikonoční	k2eAgFnSc1d1	velikonoční
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
padesátidenní	padesátidenní	k2eAgNnSc1d1	padesátidenní
období	období	k1gNnSc1	období
od	od	k7c2	od
neděle	neděle	k1gFnSc2	neděle
Zmrtvýchvstání	zmrtvýchvstání	k1gNnSc2	zmrtvýchvstání
do	do	k7c2	do
letnic	letnice	k1gFnPc2	letnice
<g/>
.	.	kIx.	.
</s>
<s>
Kristovo	Kristův	k2eAgNnSc1d1	Kristovo
ukřižování	ukřižování	k1gNnSc1	ukřižování
se	se	k3xPyFc4	se
událo	udát	k5eAaPmAgNnS	udát
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
30	[number]	k4	30
či	či	k8xC	či
33	[number]	k4	33
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
významného	významný	k2eAgInSc2d1	významný
židovského	židovský	k2eAgInSc2d1	židovský
svátku	svátek	k1gInSc2	svátek
pesach	pesach	k1gInSc1	pesach
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
památkou	památka	k1gFnSc7	památka
vysvobození	vysvobození	k1gNnSc2	vysvobození
Izraelitů	izraelita	k1gMnPc2	izraelita
Mojžíšem	Mojžíš	k1gMnSc7	Mojžíš
z	z	k7c2	z
egyptského	egyptský	k2eAgNnSc2d1	egyptské
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
jako	jako	k9	jako
Letnice	letnice	k1gFnPc1	letnice
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
původně	původně	k6eAd1	původně
(	(	kIx(	(
<g/>
i	i	k9	i
podle	podle	k7c2	podle
latinského	latinský	k2eAgInSc2d1	latinský
názvu	název	k1gInSc2	název
<g/>
)	)	kIx)	)
svátkem	svátek	k1gInSc7	svátek
židovským	židovská	k1gFnPc3	židovská
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
325	[number]	k4	325
se	se	k3xPyFc4	se
slavily	slavit	k5eAaImAgInP	slavit
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
jako	jako	k8xS	jako
svátek	svátek	k1gInSc4	svátek
židovský	židovský	k2eAgInSc4d1	židovský
<g/>
.	.	kIx.	.
<g/>
Velikonoce	Velikonoce	k1gFnPc1	Velikonoce
jsou	být	k5eAaImIp3nP	být
pohyblivý	pohyblivý	k2eAgInSc4d1	pohyblivý
svátek	svátek	k1gInSc4	svátek
<g/>
,	,	kIx,	,
datum	datum	k1gNnSc1	datum
se	se	k3xPyFc4	se
rok	rok	k1gInSc4	rok
od	od	k7c2	od
roku	rok	k1gInSc2	rok
mění	měnit	k5eAaImIp3nS	měnit
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
Výpočet	výpočet	k1gInSc1	výpočet
data	datum	k1gNnSc2	datum
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
křesťanské	křesťanský	k2eAgFnSc6d1	křesťanská
tradici	tradice	k1gFnSc6	tradice
neděle	neděle	k1gFnSc2	neděle
Zmrtvýchvstání	zmrtvýchvstání	k1gNnSc2	zmrtvýchvstání
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
první	první	k4xOgFnSc4	první
neděli	neděle	k1gFnSc4	neděle
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
jarním	jarní	k2eAgInSc6d1	jarní
úplňku	úplněk	k1gInSc6	úplněk
po	po	k7c6	po
rovnodennosti	rovnodennost	k1gFnSc6	rovnodennost
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
na	na	k7c4	na
měsíc	měsíc	k1gInSc4	měsíc
březen	březen	k1gInSc1	březen
či	či	k8xC	či
duben	duben	k1gInSc1	duben
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
Slovanů	Slovan	k1gMnPc2	Slovan
a	a	k8xC	a
Germánů	Germán	k1gMnPc2	Germán
splynuly	splynout	k5eAaPmAgFnP	splynout
lidové	lidový	k2eAgFnPc1d1	lidová
oslavy	oslava	k1gFnPc1	oslava
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
s	s	k7c7	s
pohanskými	pohanský	k2eAgFnPc7d1	pohanská
slavnostmi	slavnost	k1gFnPc7	slavnost
jara	jaro	k1gNnSc2	jaro
(	(	kIx(	(
<g/>
pohanský	pohanský	k2eAgInSc1d1	pohanský
název	název	k1gInSc1	název
Easter	Easter	k1gInSc1	Easter
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
oslavovaly	oslavovat	k5eAaImAgFnP	oslavovat
procitnutí	procitnutí	k1gNnSc4	procitnutí
přírody	příroda	k1gFnSc2	příroda
ze	z	k7c2	z
zimního	zimní	k2eAgInSc2d1	zimní
spánku	spánek	k1gInSc2	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
do	do	k7c2	do
lidových	lidový	k2eAgFnPc2d1	lidová
oslav	oslava	k1gFnPc2	oslava
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
přešly	přejít	k5eAaPmAgInP	přejít
v	v	k7c6	v
germánském	germánský	k2eAgInSc6d1	germánský
a	a	k8xC	a
slovanském	slovanský	k2eAgInSc6d1	slovanský
prostoru	prostor	k1gInSc6	prostor
mnohé	mnohý	k2eAgFnPc1d1	mnohá
původem	původ	k1gInSc7	původ
pohanské	pohanský	k2eAgInPc4d1	pohanský
zvyky	zvyk	k1gInPc4	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
lze	lze	k6eAd1	lze
symboly	symbol	k1gInPc4	symbol
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
vystopovat	vystopovat	k5eAaPmF	vystopovat
jako	jako	k9	jako
univerzální	univerzální	k2eAgInPc4d1	univerzální
symboly	symbol	k1gInPc4	symbol
jara	jaro	k1gNnSc2	jaro
a	a	k8xC	a
plodnosti	plodnost	k1gFnSc2	plodnost
například	například	k6eAd1	například
až	až	k9	až
do	do	k7c2	do
starověkého	starověký	k2eAgInSc2d1	starověký
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zelený	zelený	k2eAgInSc1d1	zelený
Chonsu	Chons	k1gInSc3	Chons
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
beraního	beraní	k2eAgInSc2d1	beraní
Amona	Amon	k1gInSc2	Amon
<g/>
,	,	kIx,	,
stvořitelsky	stvořitelsky	k6eAd1	stvořitelsky
oplodní	oplodnit	k5eAaPmIp3nS	oplodnit
Kosmické	kosmický	k2eAgNnSc1d1	kosmické
vejce	vejce	k1gNnSc1	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Lidové	lidový	k2eAgFnPc4d1	lidová
zvyklosti	zvyklost	k1gFnPc4	zvyklost
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
Velikonocemi	Velikonoce	k1gFnPc7	Velikonoce
se	se	k3xPyFc4	se
pochopitelně	pochopitelně	k6eAd1	pochopitelně
místně	místně	k6eAd1	místně
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
časové	časový	k2eAgFnSc3d1	časová
blízkosti	blízkost	k1gFnSc3	blízkost
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
a	a	k8xC	a
jarní	jarní	k2eAgFnPc1d1	jarní
rovnodennosti	rovnodennost	k1gFnPc1	rovnodennost
mají	mít	k5eAaImIp3nP	mít
tyto	tento	k3xDgFnPc1	tento
tradice	tradice	k1gFnPc1	tradice
původ	původ	k1gInSc4	původ
v	v	k7c6	v
pohanských	pohanský	k2eAgFnPc6d1	pohanská
oslavách	oslava	k1gFnPc6	oslava
příchodu	příchod	k1gInSc2	příchod
jara	jaro	k1gNnSc2	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
etnologie	etnologie	k1gFnSc2	etnologie
tedy	tedy	k9	tedy
navazují	navazovat	k5eAaImIp3nP	navazovat
na	na	k7c4	na
tradici	tradice	k1gFnSc4	tradice
původně	původně	k6eAd1	původně
pohanskou	pohanský	k2eAgFnSc4d1	pohanská
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
např.	např.	kA	např.
Beltain	Beltain	k1gInSc1	Beltain
<g/>
,	,	kIx,	,
Morana	Morana	k1gFnSc1	Morana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Označení	označení	k1gNnSc1	označení
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
jako	jako	k8xC	jako
velké	velký	k2eAgFnSc2d1	velká
noci	noc	k1gFnSc2	noc
nebo	nebo	k8xC	nebo
velkého	velký	k2eAgInSc2d1	velký
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
kalk	kalk	k1gInSc1	kalk
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
μ	μ	k?	μ
ἡ	ἡ	k?	ἡ
(	(	kIx(	(
<g/>
megalē	megalē	k?	megalē
hē	hē	k?	hē
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
dalších	další	k2eAgInPc2d1	další
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
velký	velký	k2eAgInSc1d1	velký
<g/>
"	"	kIx"	"
odráží	odrážet	k5eAaImIp3nS	odrážet
význam	význam	k1gInSc4	význam
svátku	svátek	k1gInSc2	svátek
v	v	k7c4	v
křesťanství	křesťanství	k1gNnSc4	křesťanství
a	a	k8xC	a
slovo	slovo	k1gNnSc4	slovo
"	"	kIx"	"
<g/>
noc	noc	k1gFnSc4	noc
<g/>
"	"	kIx"	"
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
velikonoční	velikonoční	k2eAgFnPc4d1	velikonoční
vigilie	vigilie	k1gFnPc4	vigilie
<g/>
,	,	kIx,	,
noční	noční	k2eAgFnSc4d1	noční
liturgii	liturgie	k1gFnSc4	liturgie
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
začíná	začínat	k5eAaImIp3nS	začínat
oslava	oslava	k1gFnSc1	oslava
nedělního	nedělní	k2eAgNnSc2d1	nedělní
Zmrtvýchvstání	zmrtvýchvstání	k1gNnSc2	zmrtvýchvstání
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
dříve	dříve	k6eAd2	dříve
začínal	začínat	k5eAaImAgInS	začínat
západem	západ	k1gInSc7	západ
slunce	slunce	k1gNnSc2	slunce
předchozího	předchozí	k2eAgInSc2d1	předchozí
dne	den	k1gInSc2	den
a	a	k8xC	a
proto	proto	k8xC	proto
i	i	k9	i
oslava	oslava	k1gFnSc1	oslava
svátku	svátek	k1gInSc2	svátek
začínala	začínat	k5eAaImAgFnS	začínat
již	již	k9	již
předchozího	předchozí	k2eAgInSc2d1	předchozí
večera	večer	k1gInSc2	večer
<g/>
;	;	kIx,	;
podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
například	například	k6eAd1	například
Boží	boží	k2eAgInSc1d1	boží
hod	hod	k1gInSc1	hod
vánoční	vánoční	k2eAgInSc1d1	vánoční
začíná	začínat	k5eAaImIp3nS	začínat
slavit	slavit	k5eAaImF	slavit
na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
večer	večer	k1gInSc4	večer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
používá	používat	k5eAaImIp3nS	používat
latinské	latinský	k2eAgNnSc1d1	latinské
Pascha	Pasch	k1gMnSc4	Pasch
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
přes	přes	k7c4	přes
řečtinu	řečtina	k1gFnSc4	řečtina
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
hebrejského	hebrejský	k2eAgMnSc2d1	hebrejský
pesach	pesach	k1gInSc4	pesach
<g/>
,	,	kIx,	,
názvu	název	k1gInSc2	název
židovského	židovský	k2eAgInSc2d1	židovský
svátku	svátek	k1gInSc2	svátek
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yRgInSc4	který
Velikonoce	Velikonoce	k1gFnPc4	Velikonoce
navazují	navazovat	k5eAaImIp3nP	navazovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
slova	slovo	k1gNnSc2	slovo
je	být	k5eAaImIp3nS	být
také	také	k9	také
odvozen	odvozen	k2eAgInSc1d1	odvozen
název	název	k1gInSc1	název
velikonoční	velikonoční	k2eAgFnSc2d1	velikonoční
svíce	svíce	k1gFnSc2	svíce
paškál	paškál	k1gInSc1	paškál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Datum	datum	k1gNnSc1	datum
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
==	==	k?	==
</s>
</p>
<p>
<s>
Datum	datum	k1gNnSc1	datum
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
od	od	k7c2	od
židovského	židovský	k2eAgInSc2d1	židovský
svátku	svátek	k1gInSc2	svátek
pesach	pesach	k1gInSc1	pesach
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
slavil	slavit	k5eAaImAgInS	slavit
dle	dle	k7c2	dle
lunisolárního	lunisolární	k2eAgInSc2d1	lunisolární
židovského	židovský	k2eAgInSc2d1	židovský
kalendáře	kalendář	k1gInSc2	kalendář
14	[number]	k4	14
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
měsíce	měsíc	k1gInSc2	měsíc
nisan	nisan	k1gInSc4	nisan
<g/>
,	,	kIx,	,
v	v	k7c6	v
den	den	k1gInSc1	den
prvního	první	k4xOgInSc2	první
jarního	jarní	k2eAgInSc2d1	jarní
úplňku	úplněk	k1gInSc2	úplněk
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
velikonoční	velikonoční	k2eAgFnSc7d1	velikonoční
událostí	událost	k1gFnSc7	událost
je	být	k5eAaImIp3nS	být
Ježíšovo	Ježíšův	k2eAgNnSc4d1	Ježíšovo
zmrtvýchvstání	zmrtvýchvstání	k1gNnSc4	zmrtvýchvstání
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgInSc3	který
mělo	mít	k5eAaImAgNnS	mít
dojít	dojít	k5eAaPmF	dojít
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
datum	datum	k1gNnSc1	datum
velikonoc	velikonoce	k1gFnPc2	velikonoce
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
výpočtu	výpočet	k1gInSc2	výpočet
první	první	k4xOgFnSc2	první
neděle	neděle	k1gFnSc2	neděle
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
jarním	jarní	k2eAgInSc6d1	jarní
úplňku	úplněk	k1gInSc6	úplněk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Datum	datum	k1gNnSc1	datum
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
církvi	církev	k1gFnSc6	církev
===	===	k?	===
</s>
</p>
<p>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
data	datum	k1gNnSc2	datum
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
stanovil	stanovit	k5eAaPmAgMnS	stanovit
roku	rok	k1gInSc2	rok
325	[number]	k4	325
První	první	k4xOgInSc4	první
nikajský	nikajský	k2eAgInSc4d1	nikajský
koncil	koncil	k1gInSc4	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nich	on	k3xPp3gFnPc2	on
připadají	připadat	k5eAaImIp3nP	připadat
velikonoční	velikonoční	k2eAgInPc4d1	velikonoční
svátky	svátek	k1gInPc4	svátek
na	na	k7c4	na
neděli	neděle	k1gFnSc4	neděle
následující	následující	k2eAgFnSc1d1	následující
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
jarním	jarní	k2eAgInSc6d1	jarní
úplňku	úplněk	k1gInSc6	úplněk
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
první	první	k4xOgInSc1	první
jarní	jarní	k2eAgInSc1d1	jarní
úplněk	úplněk	k1gInSc1	úplněk
připadne	připadnout	k5eAaPmIp3nS	připadnout
na	na	k7c4	na
neděli	neděle	k1gFnSc4	neděle
<g/>
,	,	kIx,	,
slaví	slavit	k5eAaImIp3nP	slavit
se	se	k3xPyFc4	se
Velikonoce	Velikonoce	k1gFnPc1	Velikonoce
až	až	k9	až
další	další	k2eAgFnSc4d1	další
neděli	neděle	k1gFnSc4	neděle
<g/>
.	.	kIx.	.
</s>
<s>
Pondělí	pondělí	k1gNnSc1	pondělí
velikonoční	velikonoční	k2eAgFnSc1d1	velikonoční
podle	podle	k7c2	podle
těchto	tento	k3xDgNnPc2	tento
pravidel	pravidlo	k1gNnPc2	pravidlo
může	moct	k5eAaImIp3nS	moct
připadnout	připadnout	k5eAaPmF	připadnout
na	na	k7c4	na
den	den	k1gInSc4	den
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
do	do	k7c2	do
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
přibližně	přibližně	k6eAd1	přibližně
znamení	znamení	k1gNnSc1	znamení
Berana	Beran	k1gMnSc2	Beran
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
zvěrokruhu	zvěrokruh	k1gInSc6	zvěrokruh
již	již	k6eAd1	již
od	od	k7c2	od
Hipparcha	Hipparch	k1gMnSc2	Hipparch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Křesťanský	křesťanský	k2eAgInSc1d1	křesťanský
svátek	svátek	k1gInSc1	svátek
==	==	k?	==
</s>
</p>
<p>
<s>
Nejstarším	starý	k2eAgNnSc7d3	nejstarší
svědectvím	svědectví	k1gNnSc7	svědectví
o	o	k7c6	o
slavení	slavení	k1gNnSc6	slavení
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
ovšem	ovšem	k9	ovšem
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
pascha	pascha	k1gFnSc1	pascha
<g/>
,	,	kIx,	,
v	v	k7c6	v
originále	originál	k1gInSc6	originál
π	π	k?	π
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zmínka	zmínka	k1gFnSc1	zmínka
v	v	k7c6	v
listech	list	k1gInPc6	list
apoštola	apoštol	k1gMnSc2	apoštol
Pavla	Pavel	k1gMnSc2	Pavel
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
50	[number]	k4	50
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejstaršími	starý	k2eAgInPc7d3	nejstarší
mimobiblickými	mimobiblický	k2eAgInPc7d1	mimobiblický
doklady	doklad	k1gInPc7	doklad
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
spory	spora	k1gFnPc1	spora
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
o	o	k7c4	o
datum	datum	k1gNnSc4	datum
slavení	slavení	k1gNnSc2	slavení
"	"	kIx"	"
<g/>
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
<g/>
"	"	kIx"	"
a	a	k8xC	a
spis	spis	k1gInSc1	spis
Peri	peri	k1gFnSc2	peri
Pascha	Pasch	k1gMnSc2	Pasch
Melitona	Meliton	k1gMnSc2	Meliton
ze	z	k7c2	z
Sard	Sardy	k1gFnPc2	Sardy
z	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Slavení	slavení	k1gNnSc1	slavení
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
objevilo	objevit	k5eAaPmAgNnS	objevit
velmi	velmi	k6eAd1	velmi
brzy	brzy	k6eAd1	brzy
a	a	k8xC	a
již	již	k6eAd1	již
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
je	být	k5eAaImIp3nS	být
významově	významově	k6eAd1	významově
provázáno	provázat	k5eAaPmNgNnS	provázat
s	s	k7c7	s
židovskou	židovský	k2eAgFnSc7d1	židovská
oslavou	oslava	k1gFnSc7	oslava
Pesachu	pesach	k1gInSc2	pesach
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
prvky	prvek	k1gInPc4	prvek
dodnes	dodnes	k6eAd1	dodnes
v	v	k7c6	v
sobě	se	k3xPyFc3	se
nese	nést	k5eAaImIp3nS	nést
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ježíšovo	Ježíšův	k2eAgNnSc1d1	Ježíšovo
projití	projití	k1gNnSc1	projití
smrtí	smrt	k1gFnPc2	smrt
a	a	k8xC	a
vzkříšení	vzkříšený	k2eAgMnPc1d1	vzkříšený
křesťané	křesťan	k1gMnPc1	křesťan
chápou	chápat	k5eAaImIp3nP	chápat
jako	jako	k9	jako
naplnění	naplnění	k1gNnSc4	naplnění
starozákonního	starozákonní	k2eAgInSc2d1	starozákonní
obrazu	obraz	k1gInSc2	obraz
přejití	přejití	k1gNnPc2	přejití
Izraelitů	izraelita	k1gMnPc2	izraelita
Rudým	rudý	k2eAgNnSc7d1	Rudé
mořem	moře	k1gNnSc7	moře
při	při	k7c6	při
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Oslava	oslava	k1gFnSc1	oslava
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
tradičně	tradičně	k6eAd1	tradičně
trvá	trvat	k5eAaImIp3nS	trvat
celých	celý	k2eAgInPc2d1	celý
padesát	padesát	k4xCc4	padesát
dní	den	k1gInPc2	den
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
velikonoční	velikonoční	k2eAgFnSc1d1	velikonoční
doba	doba	k1gFnSc1	doba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vrcholí	vrcholit	k5eAaImIp3nP	vrcholit
svátkem	svátek	k1gInSc7	svátek
Seslání	seslání	k1gNnSc2	seslání
Ducha	duch	k1gMnSc2	duch
svatého	svatý	k1gMnSc2	svatý
–	–	k?	–
letnicemi	letnice	k1gFnPc7	letnice
<g/>
.	.	kIx.	.
</s>
<s>
Velikonoce	Velikonoce	k1gFnPc1	Velikonoce
podle	podle	k7c2	podle
katolické	katolický	k2eAgFnSc2d1	katolická
tradice	tradice	k1gFnSc2	tradice
začínají	začínat	k5eAaImIp3nP	začínat
vigilií	vigilie	k1gFnSc7	vigilie
neděle	neděle	k1gFnSc2	neděle
Vzkříšení	vzkříšení	k1gNnSc2	vzkříšení
(	(	kIx(	(
<g/>
která	který	k3yRgFnSc1	který
završuje	završovat	k5eAaImIp3nS	završovat
velikonoční	velikonoční	k2eAgInSc4d1	velikonoční
triduum	triduum	k1gInSc4	triduum
Zeleného	Zeleného	k2eAgInSc2d1	Zeleného
čtvrtku	čtvrtek	k1gInSc2	čtvrtek
<g/>
,	,	kIx,	,
Velkého	velký	k2eAgInSc2d1	velký
pátku	pátek	k1gInSc2	pátek
a	a	k8xC	a
Bílé	bílý	k2eAgFnSc2d1	bílá
soboty	sobota	k1gFnSc2	sobota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
týden	týden	k1gInSc1	týden
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Velikonoční	velikonoční	k2eAgInSc1d1	velikonoční
oktáv	oktáv	k1gInSc1	oktáv
<g/>
.	.	kIx.	.
40	[number]	k4	40
<g/>
.	.	kIx.	.
den	den	k1gInSc1	den
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
je	být	k5eAaImIp3nS	být
slavnost	slavnost	k1gFnSc1	slavnost
Nanebevstoupení	nanebevstoupení	k1gNnSc2	nanebevstoupení
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
připomíná	připomínat	k5eAaImIp3nS	připomínat
Ježíšův	Ježíšův	k2eAgInSc4d1	Ježíšův
výstup	výstup	k1gInSc4	výstup
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
do	do	k7c2	do
nebe	nebe	k1gNnSc2	nebe
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
oslavení	oslavení	k1gNnSc4	oslavení
u	u	k7c2	u
Otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Velikonoční	velikonoční	k2eAgInPc4d1	velikonoční
symboly	symbol	k1gInPc4	symbol
==	==	k?	==
</s>
</p>
<p>
<s>
Beránek	Beránek	k1gMnSc1	Beránek
představuje	představovat	k5eAaImIp3nS	představovat
v	v	k7c6	v
židovské	židovský	k2eAgFnSc6d1	židovská
tradici	tradice	k1gFnSc6	tradice
Izrael	Izrael	k1gInSc4	Izrael
jako	jako	k8xC	jako
Boží	boží	k2eAgNnSc4d1	boží
stádo	stádo	k1gNnSc4	stádo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
vede	vést	k5eAaImIp3nS	vést
Hospodin	Hospodin	k1gMnSc1	Hospodin
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
Židé	Žid	k1gMnPc1	Žid
při	při	k7c6	při
pesachovovém	pesachovový	k2eAgInSc6d1	pesachovový
svátku	svátek	k1gInSc6	svátek
pojídají	pojídat	k5eAaImIp3nP	pojídat
pesachového	pesachový	k2eAgMnSc4d1	pesachový
beránka	beránek	k1gMnSc4	beránek
jako	jako	k8xC	jako
připomínku	připomínka	k1gFnSc4	připomínka
svého	svůj	k3xOyFgNnSc2	svůj
vysvobození	vysvobození	k1gNnSc2	vysvobození
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
křesťanství	křesťanství	k1gNnSc6	křesťanství
je	být	k5eAaImIp3nS	být
beránek	beránek	k1gInSc1	beránek
Boží	božit	k5eAaImIp3nS	božit
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
symbolů	symbol	k1gInPc2	symbol
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
obrazně	obrazně	k6eAd1	obrazně
podle	podle	k7c2	podle
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
víry	víra	k1gFnSc2	víra
on	on	k3xPp3gMnSc1	on
je	být	k5eAaImIp3nS	být
beránek	beránek	k1gMnSc1	beránek
<g/>
,	,	kIx,	,
obětovaný	obětovaný	k2eAgMnSc1d1	obětovaný
za	za	k7c4	za
spásu	spása	k1gFnSc4	spása
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kříž	Kříž	k1gMnSc1	Kříž
je	být	k5eAaImIp3nS	být
nejdůležitějším	důležitý	k2eAgMnSc7d3	nejdůležitější
z	z	k7c2	z
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
symbolů	symbol	k1gInPc2	symbol
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Kristus	Kristus	k1gMnSc1	Kristus
byl	být	k5eAaImAgMnS	být
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
k	k	k7c3	k
smrti	smrt	k1gFnSc2	smrt
ukřižováním	ukřižování	k1gNnSc7	ukřižování
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
trest	trest	k1gInSc4	trest
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
trestům	trest	k1gInPc3	trest
nejvíce	nejvíce	k6eAd1	nejvíce
krutým	krutý	k2eAgMnPc3d1	krutý
a	a	k8xC	a
ponižujícím	ponižující	k2eAgMnSc7d1	ponižující
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
symbol	symbol	k1gInSc1	symbol
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
mnohem	mnohem	k6eAd1	mnohem
starší	starý	k2eAgMnSc1d2	starší
<g/>
.	.	kIx.	.
</s>
<s>
Ukřižování	ukřižování	k1gNnSc1	ukřižování
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
zimnímu	zimní	k2eAgInSc3d1	zimní
slunovratu	slunovrat	k1gInSc3	slunovrat
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Slunce	slunce	k1gNnSc1	slunce
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
Jižního	jižní	k2eAgInSc2d1	jižní
kříže	kříž	k1gInSc2	kříž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bohoslužba	bohoslužba	k1gFnSc1	bohoslužba
velikonoční	velikonoční	k2eAgFnSc2d1	velikonoční
vigilie	vigilie	k1gFnSc2	vigilie
začíná	začínat	k5eAaImIp3nS	začínat
zapálením	zapálení	k1gNnSc7	zapálení
velikonočního	velikonoční	k2eAgInSc2d1	velikonoční
ohně	oheň	k1gInSc2	oheň
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
vítězství	vítězství	k1gNnSc4	vítězství
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
nad	nad	k7c7	nad
temnotou	temnota	k1gFnSc7	temnota
a	a	k8xC	a
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
ohně	oheň	k1gInSc2	oheň
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
zapaluje	zapalovat	k5eAaImIp3nS	zapalovat
velikonoční	velikonoční	k2eAgFnSc1d1	velikonoční
svíce	svíce	k1gFnSc1	svíce
(	(	kIx(	(
<g/>
paškál	paškál	k1gInSc1	paškál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
kulturách	kultura	k1gFnPc6	kultura
chápána	chápat	k5eAaImNgFnS	chápat
jako	jako	k8xC	jako
znamení	znamení	k1gNnSc1	znamení
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
zapálená	zapálený	k2eAgFnSc1d1	zapálená
svíce	svíce	k1gFnSc1	svíce
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
velikonoční	velikonoční	k2eAgFnSc2d1	velikonoční
bohoslužby	bohoslužba	k1gFnSc2	bohoslužba
noří	nořit	k5eAaImIp3nS	nořit
do	do	k7c2	do
křestní	křestní	k2eAgFnSc2d1	křestní
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ozdobena	ozdobit	k5eAaPmNgFnS	ozdobit
znamením	znamení	k1gNnSc7	znamení
kříže	kříž	k1gInSc2	kříž
a	a	k8xC	a
symboly	symbol	k1gInPc4	symbol
Α	Α	k?	Α
a	a	k8xC	a
Ω	Ω	k?	Ω
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
začátku	začátek	k1gInSc2	začátek
a	a	k8xC	a
konce	konec	k1gInSc2	konec
věků	věk	k1gInPc2	věk
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgNnPc7	jenž
je	být	k5eAaImIp3nS	být
Kristus	Kristus	k1gMnSc1	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
svíce	svíce	k1gFnSc1	svíce
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
zapaluje	zapalovat	k5eAaImIp3nS	zapalovat
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
velikonoční	velikonoční	k2eAgFnSc4d1	velikonoční
dobu	doba	k1gFnSc4	doba
až	až	k9	až
do	do	k7c2	do
letnic	letnice	k1gFnPc2	letnice
a	a	k8xC	a
při	při	k7c6	při
každém	každý	k3xTgInSc6	každý
křtu	křest	k1gInSc6	křest
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
naznačilo	naznačit	k5eAaPmAgNnS	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
křest	křest	k1gInSc1	křest
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
Velikonocům	Velikonoce	k1gFnPc3	Velikonoce
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
svíce	svíce	k1gFnSc1	svíce
se	se	k3xPyFc4	se
též	též	k9	též
rozžíhá	rozžíhat	k5eAaImIp3nS	rozžíhat
při	při	k7c6	při
křesťanském	křesťanský	k2eAgInSc6d1	křesťanský
pohřbu	pohřeb	k1gInSc6	pohřeb
na	na	k7c6	na
znamení	znamení	k1gNnSc6	znamení
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemřelý	zemřelý	k1gMnSc1	zemřelý
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Kristus	Kristus	k1gMnSc1	Kristus
prošel	projít	k5eAaPmAgMnS	projít
branou	brána	k1gFnSc7	brána
smrti	smrt	k1gFnSc2	smrt
<g/>
;	;	kIx,	;
a	a	k8xC	a
církev	církev	k1gFnSc1	církev
se	se	k3xPyFc4	se
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
modlí	modlit	k5eAaImIp3nS	modlit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vstal	vstát	k5eAaPmAgMnS	vstát
k	k	k7c3	k
novému	nový	k2eAgInSc3d1	nový
životu	život	k1gInSc3	život
s	s	k7c7	s
Bohem	bůh	k1gMnSc7	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Pálení	pálení	k1gNnSc1	pálení
čarodějnic	čarodějnice	k1gFnPc2	čarodějnice
či	či	k8xC	či
Beltain	Beltaina	k1gFnPc2	Beltaina
je	být	k5eAaImIp3nS	být
pohanskou	pohanský	k2eAgFnSc7d1	pohanská
obdobou	obdoba	k1gFnSc7	obdoba
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
På	På	k1gMnSc2	På
<g/>
)	)	kIx)	)
a	a	k8xC	a
například	například	k6eAd1	například
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
se	se	k3xPyFc4	se
koledníci	koledník	k1gMnPc1	koledník
převlékají	převlékat	k5eAaImIp3nP	převlékat
za	za	k7c4	za
čarodějnice	čarodějnice	k1gFnPc4	čarodějnice
<g/>
.	.	kIx.	.
</s>
<s>
Oheň	oheň	k1gInSc1	oheň
zde	zde	k6eAd1	zde
symbolicky	symbolicky	k6eAd1	symbolicky
očišťuje	očišťovat	k5eAaImIp3nS	očišťovat
od	od	k7c2	od
zlého	zlé	k1gNnSc2	zlé
ze	z	k7c2	z
zimního	zimní	k2eAgNnSc2d1	zimní
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgMnSc7d1	další
z	z	k7c2	z
velikonočních	velikonoční	k2eAgMnPc2d1	velikonoční
symbolů	symbol	k1gInPc2	symbol
je	být	k5eAaImIp3nS	být
vajíčko	vajíčko	k1gNnSc1	vajíčko
<g/>
,	,	kIx,	,
symbol	symbol	k1gInSc1	symbol
nového	nový	k2eAgInSc2d1	nový
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
samo	sám	k3xTgNnSc1	sám
zárodek	zárodek	k1gInSc4	zárodek
života	život	k1gInSc2	život
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
kulturách	kultura	k1gFnPc6	kultura
je	být	k5eAaImIp3nS	být
vejce	vejce	k1gNnSc1	vejce
symbolem	symbol	k1gInSc7	symbol
plodnosti	plodnost	k1gFnSc2	plodnost
<g/>
,	,	kIx,	,
života	život	k1gInSc2	život
a	a	k8xC	a
vzkříšení	vzkříšení	k1gNnSc2	vzkříšení
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Egyptě	Egypt	k1gInSc6	Egypt
či	či	k8xC	či
Persii	Persie	k1gFnSc6	Persie
se	se	k3xPyFc4	se
na	na	k7c4	na
svátky	svátek	k1gInPc4	svátek
jara	jaro	k1gNnSc2	jaro
barvila	barvit	k5eAaImAgNnP	barvit
červeně	červeně	k6eAd1	červeně
vajíčka	vajíčko	k1gNnPc1	vajíčko
(	(	kIx(	(
<g/>
červená	červená	k1gFnSc1	červená
jako	jako	k8xS	jako
symbol	symbol	k1gInSc1	symbol
dělohy	děloha	k1gFnSc2	děloha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdobení	zdobení	k1gNnSc1	zdobení
skořápek	skořápka	k1gFnPc2	skořápka
vajec	vejce	k1gNnPc2	vejce
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
sahat	sahat	k5eAaImF	sahat
až	až	k9	až
do	do	k7c2	do
pravěku	pravěk	k1gInSc2	pravěk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislostí	souvislost	k1gFnPc2	souvislost
s	s	k7c7	s
lidovou	lidový	k2eAgFnSc7d1	lidová
tradicí	tradice	k1gFnSc7	tradice
se	se	k3xPyFc4	se
zvyk	zvyk	k1gInSc1	zvyk
tato	tento	k3xDgNnPc4	tento
vejce	vejce	k1gNnPc4	vejce
malovat	malovat	k5eAaImF	malovat
udržuje	udržovat	k5eAaImIp3nS	udržovat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
(	(	kIx(	(
<g/>
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
kraslice	kraslice	k1gFnSc1	kraslice
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
červené	červený	k2eAgFnSc2d1	červená
barvy	barva	k1gFnSc2	barva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
pojídání	pojídání	k1gNnSc2	pojídání
vajec	vejce	k1gNnPc2	vejce
o	o	k7c6	o
Velikonocích	Velikonoce	k1gFnPc6	Velikonoce
byla	být	k5eAaImAgFnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k9	i
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
vejce	vejce	k1gNnSc4	vejce
se	se	k3xPyFc4	se
nesměla	smět	k5eNaImAgFnS	smět
jíst	jíst	k5eAaImF	jíst
v	v	k7c6	v
postní	postní	k2eAgFnSc6d1	postní
době	doba	k1gFnSc6	doba
jakožto	jakožto	k8xS	jakožto
lacticinie	lacticinie	k1gFnSc2	lacticinie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
křesťanství	křesťanství	k1gNnSc6	křesťanství
se	se	k3xPyFc4	se
vejce	vejce	k1gNnSc1	vejce
vykládá	vykládat	k5eAaImIp3nS	vykládat
jako	jako	k9	jako
symbol	symbol	k1gInSc1	symbol
zavřeného	zavřený	k2eAgInSc2d1	zavřený
hrobu	hrob	k1gInSc2	hrob
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
vstal	vstát	k5eAaPmAgMnS	vstát
Kristus	Kristus	k1gMnSc1	Kristus
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
symbol	symbol	k1gInSc1	symbol
nesmrtelnosti	nesmrtelnost	k1gFnSc2	nesmrtelnost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
křesťanství	křesťanství	k1gNnSc6	křesťanství
se	se	k3xPyFc4	se
červené	červené	k1gNnSc1	červené
vykládá	vykládat	k5eAaImIp3nS	vykládat
jako	jako	k9	jako
krev	krev	k1gFnSc1	krev
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
<g/>
Kočičky	kočička	k1gFnPc1	kočička
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
palmové	palmový	k2eAgFnPc1d1	Palmová
ratolesti	ratolest	k1gFnPc1	ratolest
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
vítali	vítat	k5eAaImAgMnP	vítat
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
přicházejícího	přicházející	k2eAgMnSc2d1	přicházející
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Tradičním	tradiční	k2eAgInSc7d1	tradiční
křesťanským	křesťanský	k2eAgInSc7d1	křesťanský
zvykem	zvyk	k1gInSc7	zvyk
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnSc2	jejich
svěcení	svěcení	k1gNnSc2	svěcení
na	na	k7c4	na
Květnou	květný	k2eAgFnSc4d1	Květná
neděli	neděle	k1gFnSc4	neděle
a	a	k8xC	a
používání	používání	k1gNnSc4	používání
popela	popel	k1gInSc2	popel
z	z	k7c2	z
jejich	jejich	k3xOp3gNnSc2	jejich
spálení	spálení	k1gNnSc1	spálení
o	o	k7c6	o
Popeleční	popeleční	k2eAgFnSc6d1	popeleční
středě	středa	k1gFnSc6	středa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
mnoho	mnoho	k4c1	mnoho
nenáboženských	náboženský	k2eNgFnPc2d1	nenáboženská
tradic	tradice	k1gFnPc2	tradice
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
kořeny	kořen	k1gInPc4	kořen
v	v	k7c6	v
křesťanské	křesťanský	k2eAgFnSc6d1	křesťanská
symbolice	symbolika	k1gFnSc6	symbolika
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc4	některý
velikonoční	velikonoční	k2eAgInPc4d1	velikonoční
symboly	symbol	k1gInPc4	symbol
můžeme	moct	k5eAaImIp1nP	moct
vystopovat	vystopovat	k5eAaPmF	vystopovat
až	až	k9	až
z	z	k7c2	z
předkřesťanské	předkřesťanský	k2eAgFnSc2d1	předkřesťanská
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
zajíček	zajíček	k1gMnSc1	zajíček
má	mít	k5eAaImIp3nS	mít
zřejmě	zřejmě	k6eAd1	zřejmě
původ	původ	k1gInSc4	původ
v	v	k7c6	v
pohanských	pohanský	k2eAgInPc6d1	pohanský
rituálech	rituál	k1gInPc6	rituál
oslavující	oslavující	k2eAgInSc4d1	oslavující
příchod	příchod	k1gInSc4	příchod
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
např.	např.	kA	např.
v	v	k7c6	v
byzantské	byzantský	k2eAgFnSc6d1	byzantská
ikonografii	ikonografie	k1gFnSc6	ikonografie
představoval	představovat	k5eAaImAgMnS	představovat
zajíc	zajíc	k1gMnSc1	zajíc
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Symbolika	symbolika	k1gFnSc1	symbolika
zajíce	zajíc	k1gMnPc4	zajíc
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
tradice	tradice	k1gFnSc2	tradice
oslav	oslava	k1gFnPc2	oslava
svátku	svátek	k1gInSc2	svátek
pohanské	pohanský	k2eAgFnSc2d1	pohanská
bohyně	bohyně	k1gFnSc2	bohyně
plodnosti	plodnost	k1gFnSc2	plodnost
Eostre	Eostr	k1gMnSc5	Eostr
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jejího	její	k3xOp3gNnSc2	její
jména	jméno	k1gNnSc2	jméno
je	být	k5eAaImIp3nS	být
odvozeno	odvozen	k2eAgNnSc1d1	odvozeno
slovo	slovo	k1gNnSc1	slovo
Easter	Easter	k1gInSc1	Easter
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgInSc1d1	anglický
název	název	k1gInSc1	název
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
bohyně	bohyně	k1gFnSc2	bohyně
Eostre	Eostr	k1gInSc5	Eostr
proměnila	proměnit	k5eAaPmAgFnS	proměnit
ptáčka	ptáček	k1gMnSc4	ptáček
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
umrzl	umrznout	k5eAaPmAgMnS	umrznout
ve	v	k7c6	v
vánici	vánice	k1gFnSc6	vánice
<g/>
,	,	kIx,	,
v	v	k7c4	v
zajíce	zajíc	k1gMnPc4	zajíc
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
pak	pak	k6eAd1	pak
z	z	k7c2	z
vděčnosti	vděčnost	k1gFnSc2	vděčnost
každé	každý	k3xTgNnSc4	každý
jaro	jaro	k1gNnSc4	jaro
kladl	klást	k5eAaImAgInS	klást
vejce	vejce	k1gNnSc4	vejce
jako	jako	k8xS	jako
pták	pták	k1gMnSc1	pták
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Křen	křen	k1gInSc1	křen
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
hřebíky	hřebík	k1gInPc4	hřebík
či	či	k8xC	či
hořkost	hořkost	k1gFnSc4	hořkost
utrpení	utrpení	k1gNnSc2	utrpení
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Zvyk	zvyk	k1gInSc1	zvyk
pojídat	pojídat	k5eAaImF	pojídat
křen	křen	k1gInSc4	křen
o	o	k7c6	o
Velikonocích	Velikonoce	k1gFnPc6	Velikonoce
se	se	k3xPyFc4	se
udržuje	udržovat	k5eAaImIp3nS	udržovat
především	především	k9	především
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
<g/>
.	.	kIx.	.
</s>
<s>
Odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
židovské	židovský	k2eAgFnSc3d1	židovská
tradici	tradice	k1gFnSc3	tradice
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
o	o	k7c6	o
pesachu	pesach	k1gInSc6	pesach
se	se	k3xPyFc4	se
pojídá	pojídat	k5eAaImIp3nS	pojídat
jako	jako	k9	jako
hořká	hořký	k2eAgFnSc1d1	hořká
bylina	bylina	k1gFnSc1	bylina
(	(	kIx(	(
<g/>
maror	maror	k1gMnSc1	maror
symbolizující	symbolizující	k2eAgNnSc4d1	symbolizující
utrpení	utrpení	k1gNnSc4	utrpení
při	při	k7c6	při
Exodu	Exodus	k1gInSc6	Exodus
<g/>
)	)	kIx)	)
při	při	k7c6	při
Seder	sedrat	k5eAaPmRp2nS	sedrat
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
také	také	k9	také
pojídá	pojídat	k5eAaImIp3nS	pojídat
kost	kost	k1gFnSc4	kost
z	z	k7c2	z
beránka	beránek	k1gMnSc2	beránek
či	či	k8xC	či
bejca	bejcus	k1gMnSc2	bejcus
(	(	kIx(	(
<g/>
vejce	vejce	k1gNnSc1	vejce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mléko	mléko	k1gNnSc1	mléko
s	s	k7c7	s
medem	med	k1gInSc7	med
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
dvojjedinost	dvojjedinost	k1gFnSc1	dvojjedinost
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
tzv.	tzv.	kA	tzv.
jidáše	jidáš	k1gMnSc2	jidáš
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
provaz	provaz	k1gInSc4	provaz
Jidáše	jidáš	k1gInSc2	jidáš
Iškariotského	iškariotský	k2eAgInSc2d1	iškariotský
<g/>
.	.	kIx.	.
</s>
<s>
Mléko	mléko	k1gNnSc1	mléko
a	a	k8xC	a
strdí	strdí	k1gNnSc1	strdí
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
zaslíbenou	zaslíbený	k2eAgFnSc4d1	zaslíbená
zem	zem	k1gFnSc4	zem
z	z	k7c2	z
Exodu	Exodus	k1gInSc2	Exodus
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
přejato	přejat	k2eAgNnSc1d1	přejato
i	i	k9	i
do	do	k7c2	do
legendy	legenda	k1gFnSc2	legenda
o	o	k7c6	o
Praotci	praotec	k1gMnSc6	praotec
Čechovi	Čech	k1gMnSc6	Čech
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
beránčím	beránčí	k2eAgInSc7d1	beránčí
Amonem	Amon	k1gInSc7	Amon
byl	být	k5eAaImAgInS	být
ztotožňován	ztotožňovat	k5eAaImNgInS	ztotožňovat
i	i	k9	i
Zeus	Zeus	k1gInSc1	Zeus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
jako	jako	k8xC	jako
mladý	mladý	k2eAgMnSc1d1	mladý
krmen	krmit	k5eAaImNgMnS	krmit
mlékem	mléko	k1gNnSc7	mléko
a	a	k8xC	a
medem	med	k1gInSc7	med
<g/>
.	.	kIx.	.
</s>
<s>
Dvojjediný	dvojjediný	k2eAgInSc1d1	dvojjediný
Amon-Re	Amon-Re	k1gInSc1	Amon-Re
má	mít	k5eAaImIp3nS	mít
slzy	slza	k1gFnPc4	slza
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgInPc2	který
se	se	k3xPyFc4	se
zrodili	zrodit	k5eAaPmAgMnP	zrodit
lidé	člověk	k1gMnPc1	člověk
a	a	k8xC	a
které	který	k3yQgFnPc1	který
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
med	med	k1gInSc4	med
nebo	nebo	k8xC	nebo
včely	včela	k1gFnPc4	včela
<g/>
.	.	kIx.	.
</s>
<s>
Reova	Reova	k6eAd1	Reova
manželka	manželka	k1gFnSc1	manželka
Hathor	Hathora	k1gFnPc2	Hathora
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
Mut	Mut	k?	Mut
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kraví	kraví	k2eAgFnSc7d1	kraví
bohyní	bohyně	k1gFnSc7	bohyně
mléka	mléko	k1gNnSc2	mléko
(	(	kIx(	(
<g/>
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
Mléčnou	mléčný	k2eAgFnSc4d1	mléčná
dráhu	dráha	k1gFnSc4	dráha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Velikonoční	velikonoční	k2eAgFnPc4d1	velikonoční
tradice	tradice	k1gFnPc4	tradice
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Nenáboženské	náboženský	k2eNgFnPc1d1	nenáboženská
velikonoční	velikonoční	k2eAgFnPc1d1	velikonoční
tradice	tradice	k1gFnPc1	tradice
===	===	k?	===
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
svátků	svátek	k1gInPc2	svátek
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
starší	starý	k2eAgInPc1d2	starší
než	než	k8xS	než
Velikonoce	Velikonoce	k1gFnPc4	Velikonoce
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
časem	čas	k1gInSc7	čas
oslav	oslava	k1gFnPc2	oslava
a	a	k8xC	a
veselí	veselí	k1gNnSc2	veselí
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
komerčně	komerčně	k6eAd1	komerčně
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
na	na	k7c4	na
ně	on	k3xPp3gFnPc4	on
váže	vázat	k5eAaImIp3nS	vázat
mnoho	mnoho	k4c1	mnoho
zvyků	zvyk	k1gInPc2	zvyk
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejichž	jejichž	k3xOyRp3gNnSc3	jejichž
uskutečnění	uskutečnění	k1gNnSc3	uskutečnění
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
vynaložit	vynaložit	k5eAaPmF	vynaložit
nějaké	nějaký	k3yIgNnSc4	nějaký
úsilí	úsilí	k1gNnSc4	úsilí
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
tak	tak	k6eAd1	tak
zajít	zajít	k5eAaPmF	zajít
na	na	k7c4	na
nákup	nákup	k1gInSc4	nákup
<g/>
.	.	kIx.	.
</s>
<s>
Prodávají	prodávat	k5eAaImIp3nP	prodávat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
velikonoční	velikonoční	k2eAgInPc1d1	velikonoční
pohledy	pohled	k1gInPc1	pohled
<g/>
,	,	kIx,	,
ozdoby	ozdoba	k1gFnPc1	ozdoba
nebo	nebo	k8xC	nebo
cukroví	cukroví	k1gNnSc1	cukroví
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
velikonočních	velikonoční	k2eAgNnPc2d1	velikonoční
vajíček	vajíčko	k1gNnPc2	vajíčko
<g/>
,	,	kIx,	,
beránků	beránek	k1gMnPc2	beránek
nebo	nebo	k8xC	nebo
zajíčků	zajíček	k1gMnPc2	zajíček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řehtání	řehtání	k1gNnSc1	řehtání
-	-	kIx~	-
od	od	k7c2	od
čtvrtka	čtvrtek	k1gInSc2	čtvrtek
do	do	k7c2	do
soboty	sobota	k1gFnSc2	sobota
chodili	chodit	k5eAaImAgMnP	chodit
po	po	k7c6	po
vsi	ves	k1gFnSc6	ves
chlapci	chlapec	k1gMnPc1	chlapec
školou	škola	k1gFnSc7	škola
povinní	povinný	k2eAgMnPc1d1	povinný
a	a	k8xC	a
nahrazovali	nahrazovat	k5eAaImAgMnP	nahrazovat
zvony	zvon	k1gInPc4	zvon
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
odletěly	odletět	k5eAaPmAgInP	odletět
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
zelený	zelený	k2eAgInSc4d1	zelený
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
se	se	k3xPyFc4	se
šlo	jít	k5eAaImAgNnS	jít
po	po	k7c6	po
škole	škola	k1gFnSc6	škola
a	a	k8xC	a
navečer	navečer	k6eAd1	navečer
<g/>
,	,	kIx,	,
na	na	k7c4	na
velký	velký	k2eAgInSc4d1	velký
pátek	pátek	k1gInSc4	pátek
ráno	ráno	k6eAd1	ráno
před	před	k7c7	před
školou	škola	k1gFnSc7	škola
<g/>
,	,	kIx,	,
po	po	k7c6	po
škole	škola	k1gFnSc6	škola
a	a	k8xC	a
na	na	k7c6	na
večer	večer	k6eAd1	večer
a	a	k8xC	a
naposledy	naposledy	k6eAd1	naposledy
na	na	k7c4	na
bílou	bílý	k2eAgFnSc4d1	bílá
sobotu	sobota	k1gFnSc4	sobota
ráno	ráno	k6eAd1	ráno
<g/>
.	.	kIx.	.
</s>
<s>
Chodili	chodit	k5eAaImAgMnP	chodit
tak	tak	k6eAd1	tak
aby	aby	kYmCp3nP	aby
obešli	obejít	k5eAaPmAgMnP	obejít
celou	celý	k2eAgFnSc4d1	celá
vesnici	vesnice	k1gFnSc4	vesnice
a	a	k8xC	a
u	u	k7c2	u
každého	každý	k3xTgInSc2	každý
křížku	křížek	k1gInSc2	křížek
se	se	k3xPyFc4	se
modlili	modlit	k5eAaImAgMnP	modlit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
v	v	k7c4	v
podvečer	podvečer	k1gInSc4	podvečer
obešli	obejít	k5eAaPmAgMnP	obejít
ves	ves	k1gFnSc4	ves
dům	dům	k1gInSc1	dům
od	od	k7c2	od
domu	dům	k1gInSc2	dům
kde	kde	k6eAd1	kde
vykoledovali	vykoledovat	k5eAaPmAgMnP	vykoledovat
nějaké	nějaký	k3yIgNnSc4	nějaký
vajíčko	vajíčko	k1gNnSc4	vajíčko
<g/>
,	,	kIx,	,
sladkost	sladkost	k1gFnSc4	sladkost
-	-	kIx~	-
sušené	sušený	k2eAgFnPc4d1	sušená
švestky	švestka	k1gFnPc4	švestka
<g/>
,	,	kIx,	,
křížaly	křížala	k1gFnPc4	křížala
či	či	k8xC	či
drobné	drobný	k2eAgFnPc4d1	drobná
mince	mince	k1gFnPc4	mince
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
z	z	k7c2	z
chlapců	chlapec	k1gMnPc2	chlapec
se	se	k3xPyFc4	se
nazýval	nazývat	k5eAaImAgMnS	nazývat
kaprál	kaprál	k1gMnSc1	kaprál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
měli	mít	k5eAaImAgMnP	mít
slaměného	slaměný	k2eAgMnSc4d1	slaměný
Jidáše	Jidáš	k1gMnSc4	Jidáš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
prastarou	prastarý	k2eAgFnSc7d1	prastará
tradicí	tradice	k1gFnSc7	tradice
hodování	hodování	k1gNnSc2	hodování
a	a	k8xC	a
pomlázka	pomlázka	k1gFnSc1	pomlázka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Velikonoční	velikonoční	k2eAgNnSc4d1	velikonoční
pondělí	pondělí	k1gNnSc4	pondělí
ráno	ráno	k6eAd1	ráno
muži	muž	k1gMnPc1	muž
a	a	k8xC	a
chlapci	chlapec	k1gMnPc1	chlapec
chodí	chodit	k5eAaImIp3nP	chodit
po	po	k7c6	po
domácnostech	domácnost	k1gFnPc6	domácnost
svých	svůj	k3xOyFgMnPc2	svůj
známých	známý	k1gMnPc2	známý
a	a	k8xC	a
šlehají	šlehat	k5eAaImIp3nP	šlehat
ženy	žena	k1gFnPc1	žena
a	a	k8xC	a
dívky	dívka	k1gFnPc1	dívka
ručně	ručně	k6eAd1	ručně
vyrobenou	vyrobený	k2eAgFnSc7d1	vyrobená
pomlázkou	pomlázka	k1gFnSc7	pomlázka
z	z	k7c2	z
vrbového	vrbový	k2eAgNnSc2d1	vrbové
proutí	proutí	k1gNnSc2	proutí
<g/>
.	.	kIx.	.
</s>
<s>
Pomlázka	pomlázka	k1gFnSc1	pomlázka
je	být	k5eAaImIp3nS	být
spletena	spleten	k2eAgFnSc1d1	spletena
až	až	k9	až
z	z	k7c2	z
dvaceti	dvacet	k4xCc2	dvacet
čtyř	čtyři	k4xCgInPc2	čtyři
proutků	proutek	k1gInPc2	proutek
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
od	od	k7c2	od
půl	půl	k1xP	půl
do	do	k7c2	do
dvou	dva	k4xCgMnPc2	dva
metrů	metr	k1gInPc2	metr
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
ozdobená	ozdobený	k2eAgFnSc1d1	ozdobená
pletenou	pletený	k2eAgFnSc7d1	pletená
rukojetí	rukojeť	k1gFnSc7	rukojeť
a	a	k8xC	a
barevnými	barevný	k2eAgFnPc7d1	barevná
stužkami	stužka	k1gFnPc7	stužka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
muži	muž	k1gMnPc1	muž
při	při	k7c6	při
hodování	hodování	k1gNnSc6	hodování
pronášejí	pronášet	k5eAaImIp3nP	pronášet
koledy	koleda	k1gFnPc4	koleda
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
velikonoční	velikonoční	k2eAgFnSc7d1	velikonoční
koledou	koleda	k1gFnSc7	koleda
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
krátká	krátký	k2eAgFnSc1d1	krátká
říkanka	říkanka	k1gFnSc1	říkanka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Hody	hod	k1gInPc4	hod
<g/>
,	,	kIx,	,
hody	hod	k1gInPc4	hod
doprovody	doprovod	k1gInPc4	doprovod
<g/>
,	,	kIx,	,
dejte	dát	k5eAaPmRp2nP	dát
vejce	vejce	k1gNnSc4	vejce
malovaný	malovaný	k2eAgInSc1d1	malovaný
<g/>
,	,	kIx,	,
nedáte	dát	k5eNaPmIp2nP	dát
<g/>
-li	i	k?	-li
malovaný	malovaný	k2eAgInSc1d1	malovaný
<g/>
,	,	kIx,	,
dejte	dát	k5eAaPmRp2nP	dát
aspoň	aspoň	k9	aspoň
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
slepička	slepička	k1gFnSc1	slepička
vám	vy	k3xPp2nPc3	vy
snese	snést	k5eAaPmIp3nS	snést
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Jestli	jestli	k8xS	jestli
dojde	dojít	k5eAaPmIp3nS	dojít
dříve	dříve	k6eAd2	dříve
na	na	k7c4	na
pomlázku	pomlázka	k1gFnSc4	pomlázka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
koledy	koleda	k1gFnPc4	koleda
<g/>
,	,	kIx,	,
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c4	na
situaci	situace	k1gFnSc4	situace
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyšlehání	vyšlehání	k1gNnSc1	vyšlehání
bolestivé	bolestivý	k2eAgNnSc1d1	bolestivé
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
cílem	cíl	k1gInSc7	cíl
způsobovat	způsobovat	k5eAaImF	způsobovat
příkoří	příkoří	k1gNnSc4	příkoří
<g/>
.	.	kIx.	.
</s>
<s>
Spíše	spíše	k9	spíše
je	být	k5eAaImIp3nS	být
pomlázka	pomlázka	k1gFnSc1	pomlázka
symbolem	symbol	k1gInSc7	symbol
zájmu	zájem	k1gInSc2	zájem
mužů	muž	k1gMnPc2	muž
o	o	k7c4	o
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Nenavštívené	navštívený	k2eNgFnPc1d1	nenavštívená
dívky	dívka	k1gFnPc1	dívka
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
dokonce	dokonce	k9	dokonce
cítit	cítit	k5eAaImF	cítit
uražené	uražený	k2eAgNnSc4d1	uražené
<g/>
.	.	kIx.	.
</s>
<s>
Vyšupaná	vyšupaný	k2eAgFnSc1d1	vyšupaná
žena	žena	k1gFnSc1	žena
dává	dávat	k5eAaImIp3nS	dávat
muži	muž	k1gMnSc3	muž
barevné	barevný	k2eAgNnSc1d1	barevné
vajíčko	vajíčko	k1gNnSc1	vajíčko
jako	jako	k8xC	jako
symbol	symbol	k1gInSc1	symbol
jejích	její	k3xOp3gInPc2	její
díků	dík	k1gInPc2	dík
a	a	k8xC	a
prominutí	prominutí	k1gNnSc4	prominutí
<g/>
.	.	kIx.	.
</s>
<s>
Pověst	pověst	k1gFnSc1	pověst
praví	pravit	k5eAaBmIp3nS	pravit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dívky	dívka	k1gFnPc1	dívka
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
na	na	k7c4	na
Velikonoce	Velikonoce	k1gFnPc4	Velikonoce
vyšlehány	vyšlehán	k2eAgFnPc4d1	vyšlehán
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
zdravé	zdravá	k1gFnSc2	zdravá
a	a	k8xC	a
uchovaly	uchovat	k5eAaPmAgFnP	uchovat
si	se	k3xPyFc3	se
plodnost	plodnost	k1gFnSc4	plodnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
ženy	žena	k1gFnSc2	žena
mohou	moct	k5eAaImIp3nP	moct
pomlázku	pomlázka	k1gFnSc4	pomlázka
oplatit	oplatit	k5eAaPmF	oplatit
odpoledne	odpoledne	k6eAd1	odpoledne
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vylívají	vylívat	k5eAaImIp3nP	vylívat
na	na	k7c4	na
muže	muž	k1gMnSc4	muž
a	a	k8xC	a
chlapce	chlapec	k1gMnSc4	chlapec
kbelíky	kbelík	k1gInPc1	kbelík
studené	studený	k2eAgFnSc2d1	studená
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Zvyk	zvyk	k1gInSc1	zvyk
se	se	k3xPyFc4	se
napříč	napříč	k7c7	napříč
českými	český	k2eAgFnPc7d1	Česká
zeměmi	zem	k1gFnPc7	zem
mírně	mírně	k6eAd1	mírně
mění	měnit	k5eAaImIp3nP	měnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiný	jiný	k2eAgInSc1d1	jiný
výklad	výklad	k1gInSc1	výklad
pomlázky	pomlázka	k1gFnSc2	pomlázka
–	–	k?	–
odvozeno	odvozen	k2eAgNnSc4d1	odvozeno
od	od	k7c2	od
pomlazení	pomlazení	k1gNnSc2	pomlazení
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
omlazení	omlazení	k1gNnSc2	omlazení
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
muži	muž	k1gMnPc1	muž
používají	používat	k5eAaImIp3nP	používat
mladé	mladý	k2eAgNnSc4d1	mladé
proutí	proutí	k1gNnSc4	proutí
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
podílem	podíl	k1gInSc7	podíl
"	"	kIx"	"
<g/>
životní	životní	k2eAgFnPc4d1	životní
síly	síla	k1gFnPc4	síla
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
jakoby	jakoby	k8xS	jakoby
předávají	předávat	k5eAaImIp3nP	předávat
vyšlehané	vyšlehaný	k2eAgFnSc3d1	vyšlehaná
osobě	osoba	k1gFnSc3	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
téhož	týž	k3xTgInSc2	týž
důvodu	důvod	k1gInSc2	důvod
ženy	žena	k1gFnPc1	žena
dávají	dávat	k5eAaImIp3nP	dávat
jako	jako	k9	jako
odměnu	odměna	k1gFnSc4	odměna
za	za	k7c4	za
omlazení	omlazení	k1gNnSc4	omlazení
vajíčko	vajíčko	k1gNnSc1	vajíčko
<g/>
,	,	kIx,	,
prastarý	prastarý	k2eAgInSc1d1	prastarý
symbol	symbol	k1gInSc1	symbol
nového	nový	k2eAgInSc2d1	nový
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
nejsou	být	k5eNaImIp3nP	být
tradicí	tradice	k1gFnSc7	tradice
hody	hod	k1gInPc1	hod
ale	ale	k8xC	ale
koleda	koleda	k1gFnSc1	koleda
(	(	kIx(	(
<g/>
hody	hod	k1gInPc1	hod
jsou	být	k5eAaImIp3nP	být
spíše	spíše	k9	spíše
pomístní	pomístní	k2eAgInSc4d1	pomístní
název	název	k1gInSc4	název
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
koleda	koleda	k1gFnSc1	koleda
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
vícekrát	vícekrát	k6eAd1	vícekrát
<g/>
,	,	kIx,	,
ne	ne	k9	ne
jen	jen	k9	jen
v	v	k7c6	v
období	období	k1gNnSc6	období
velikonoc	velikonoce	k1gFnPc2	velikonoce
a	a	k8xC	a
jejím	její	k3xOp3gInSc7	její
původním	původní	k2eAgInSc7d1	původní
smyslem	smysl	k1gInSc7	smysl
byla	být	k5eAaImAgFnS	být
ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c7	před
špatnými	špatný	k2eAgInPc7d1	špatný
vlivy	vliv	k1gInPc7	vliv
a	a	k8xC	a
posílení	posílení	k1gNnSc1	posílení
těch	ten	k3xDgFnPc2	ten
dobrých	dobrá	k1gFnPc2	dobrá
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
toto	tento	k3xDgNnSc4	tento
byli	být	k5eAaImAgMnP	být
koledníci	koledník	k1gMnPc1	koledník
odměňováni	odměňovat	k5eAaImNgMnP	odměňovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
původní	původní	k2eAgInSc1d1	původní
smysl	smysl	k1gInSc1	smysl
vytrácel	vytrácet	k5eAaImAgInS	vytrácet
a	a	k8xC	a
vlastně	vlastně	k9	vlastně
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
"	"	kIx"	"
<g/>
lepší	dobrý	k2eAgFnSc7d2	lepší
<g/>
"	"	kIx"	"
formou	forma	k1gFnSc7	forma
žebroty	žebrota	k1gFnSc2	žebrota
chudší	chudý	k2eAgFnSc2d2	chudší
části	část	k1gFnSc2	část
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženské	náboženský	k2eAgFnPc1d1	náboženská
velikonoční	velikonoční	k2eAgFnPc1d1	velikonoční
tradice	tradice	k1gFnPc1	tradice
===	===	k?	===
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
velmi	velmi	k6eAd1	velmi
podstatné	podstatný	k2eAgFnSc2d1	podstatná
návštěvy	návštěva	k1gFnSc2	návštěva
bohoslužeb	bohoslužba	k1gFnPc2	bohoslužba
během	během	k7c2	během
velikonočního	velikonoční	k2eAgInSc2d1	velikonoční
tridua	tridu	k1gInSc2	tridu
můžeme	moct	k5eAaImIp1nP	moct
mezi	mezi	k7c4	mezi
náboženské	náboženský	k2eAgFnPc4d1	náboženská
tradice	tradice	k1gFnPc4	tradice
zařadit	zařadit	k5eAaPmF	zařadit
velikonoční	velikonoční	k2eAgInSc1d1	velikonoční
pozdrav	pozdrav	k1gInSc1	pozdrav
<g/>
,	,	kIx,	,
zvyk	zvyk	k1gInSc1	zvyk
pocházející	pocházející	k2eAgInSc1d1	pocházející
především	především	k9	především
z	z	k7c2	z
východních	východní	k2eAgFnPc2d1	východní
církví	církev	k1gFnPc2	církev
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
i	i	k9	i
mezi	mezi	k7c4	mezi
římské	římský	k2eAgMnPc4d1	římský
katolíky	katolík	k1gMnPc4	katolík
a	a	k8xC	a
protestanty	protestant	k1gMnPc4	protestant
<g/>
.	.	kIx.	.
</s>
<s>
Zvyk	zvyk	k1gInSc1	zvyk
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
křesťané	křesťan	k1gMnPc1	křesťan
o	o	k7c6	o
slavnosti	slavnost	k1gFnSc6	slavnost
Zmrtvýchvstání	zmrtvýchvstání	k1gNnSc2	zmrtvýchvstání
Páně	páně	k2eAgNnSc2d1	páně
zdraví	zdraví	k1gNnSc2	zdraví
místo	místo	k7c2	místo
běžného	běžný	k2eAgInSc2d1	běžný
pozdravu	pozdrav	k1gInSc2	pozdrav
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kristus	Kristus	k1gMnSc1	Kristus
vstal	vstát	k5eAaPmAgMnS	vstát
z	z	k7c2	z
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Odpovědí	odpověď	k1gFnSc7	odpověď
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Opravdu	opravdu	k6eAd1	opravdu
vstal	vstát	k5eAaPmAgMnS	vstát
z	z	k7c2	z
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Velikonoční	velikonoční	k2eAgFnPc4d1	velikonoční
zvyklosti	zvyklost	k1gFnPc4	zvyklost
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
jsou	být	k5eAaImIp3nP	být
velikonoční	velikonoční	k2eAgInPc4d1	velikonoční
svátky	svátek	k1gInPc4	svátek
náboženským	náboženský	k2eAgInSc7d1	náboženský
svátkem	svátek	k1gInSc7	svátek
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mnoho	mnoho	k4c1	mnoho
amerických	americký	k2eAgFnPc2d1	americká
rodin	rodina	k1gFnPc2	rodina
mimo	mimo	k7c4	mimo
návštěvy	návštěva	k1gFnPc4	návštěva
kostela	kostel	k1gInSc2	kostel
se	se	k3xPyFc4	se
schází	scházet	k5eAaImIp3nS	scházet
kolem	kolem	k7c2	kolem
zdobení	zdobení	k1gNnSc2	zdobení
velikonočních	velikonoční	k2eAgNnPc2d1	velikonoční
vajíček	vajíčko	k1gNnPc2	vajíčko
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
večer	večer	k6eAd1	večer
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc6	jejich
"	"	kIx"	"
<g/>
lovu	lov	k1gInSc6	lov
<g/>
"	"	kIx"	"
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
ráno	ráno	k6eAd1	ráno
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dětských	dětský	k2eAgFnPc2d1	dětská
pohádek	pohádka	k1gFnPc2	pohádka
byla	být	k5eAaImAgNnP	být
vajíčka	vajíčko	k1gNnPc1	vajíčko
během	během	k7c2	během
noci	noc	k1gFnSc2	noc
přinesena	přinesen	k2eAgFnSc1d1	přinesena
Velikonočním	velikonoční	k2eAgMnSc7d1	velikonoční
zajíčkem	zajíček	k1gMnSc7	zajíček
a	a	k8xC	a
poschovávaná	poschovávaný	k2eAgFnSc1d1	poschovávaná
po	po	k7c6	po
domě	dům	k1gInSc6	dům
a	a	k8xC	a
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
počkala	počkat	k5eAaPmAgNnP	počkat
na	na	k7c4	na
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
se	se	k3xPyFc4	se
probudí	probudit	k5eAaPmIp3nS	probudit
<g/>
.	.	kIx.	.
</s>
<s>
Důvod	důvod	k1gInSc1	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
měl	mít	k5eAaImAgMnS	mít
zajíček	zajíček	k1gMnSc1	zajíček
dělat	dělat	k5eAaImF	dělat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
tradičně	tradičně	k6eAd1	tradičně
ženy	žena	k1gFnPc1	žena
přivazují	přivazovat	k5eAaImIp3nP	přivazovat
muže	muž	k1gMnPc4	muž
k	k	k7c3	k
židlím	židle	k1gFnPc3	židle
a	a	k8xC	a
za	za	k7c4	za
propuštění	propuštění	k1gNnSc4	propuštění
požadují	požadovat	k5eAaImIp3nP	požadovat
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
lyžování	lyžování	k1gNnSc2	lyžování
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
a	a	k8xC	a
malování	malování	k1gNnSc4	malování
vajíček	vajíčko	k1gNnPc2	vajíčko
<g/>
,	,	kIx,	,
tradicí	tradice	k1gFnSc7	tradice
řešení	řešení	k1gNnSc2	řešení
vražd	vražda	k1gFnPc2	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
velké	velký	k2eAgFnPc1d1	velká
televizní	televizní	k2eAgFnPc1d1	televizní
stanice	stanice	k1gFnPc1	stanice
vysílají	vysílat	k5eAaImIp3nP	vysílat
kriminální	kriminální	k2eAgInPc1d1	kriminální
a	a	k8xC	a
detektivní	detektivní	k2eAgInPc1d1	detektivní
příběhy	příběh	k1gInPc1	příběh
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Hercule	Hercule	k1gFnSc1	Hercule
Poirot	Poirota	k1gFnPc2	Poirota
nebo	nebo	k8xC	nebo
další	další	k2eAgInPc4d1	další
příběhy	příběh	k1gInPc4	příběh
od	od	k7c2	od
Agathy	Agatha	k1gFnSc2	Agatha
Christie	Christie	k1gFnSc2	Christie
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
noviny	novina	k1gFnPc1	novina
otiskují	otiskovat	k5eAaImIp3nP	otiskovat
články	článek	k1gInPc4	článek
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgMnPc2	který
mohou	moct	k5eAaImIp3nP	moct
čtenáři	čtenář	k1gMnPc1	čtenář
zkusit	zkusit	k5eAaPmF	zkusit
odvodit	odvodit	k5eAaPmF	odvodit
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
pachatelem	pachatel	k1gMnSc7	pachatel
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
také	také	k9	také
vychází	vycházet	k5eAaImIp3nS	vycházet
mnoho	mnoho	k4c4	mnoho
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
krabice	krabice	k1gFnPc4	krabice
od	od	k7c2	od
mléka	mléko	k1gNnSc2	mléko
bývají	bývat	k5eAaImIp3nP	bývat
potištěny	potištěn	k2eAgInPc1d1	potištěn
příběhy	příběh	k1gInPc1	příběh
s	s	k7c7	s
vraždami	vražda	k1gFnPc7	vražda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ukrajinský	ukrajinský	k2eAgInSc1d1	ukrajinský
velikonoční	velikonoční	k2eAgInSc1d1	velikonoční
chléb	chléb	k1gInSc1	chléb
pascha	pasch	k1gMnSc2	pasch
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
pečen	péct	k5eAaImNgInS	péct
v	v	k7c6	v
několika	několik	k4yIc6	několik
různých	různý	k2eAgFnPc6d1	různá
velikostech	velikost	k1gFnPc6	velikost
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
člena	člen	k1gMnSc4	člen
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
velký	velký	k2eAgMnSc1d1	velký
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Pascha	Pascha	k1gFnSc1	Pascha
je	být	k5eAaImIp3nS	být
zdobená	zdobený	k2eAgFnSc1d1	zdobená
různými	různý	k2eAgInPc7d1	různý
ornamenty	ornament	k1gInPc7	ornament
–	–	k?	–
osmiramennými	osmiramenný	k2eAgInPc7d1	osmiramenný
kříži	kříž	k1gInPc7	kříž
<g/>
,	,	kIx,	,
věnci	věnec	k1gInPc7	věnec
<g/>
,	,	kIx,	,
květinami	květina	k1gFnPc7	květina
a	a	k8xC	a
ptáčky	ptáček	k1gInPc7	ptáček
z	z	k7c2	z
téhož	týž	k3xTgNnSc2	týž
těsta	těsto	k1gNnSc2	těsto
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tímto	tento	k3xDgNnSc7	tento
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
ruské	ruský	k2eAgFnSc2d1	ruská
kuliči	kulič	k1gMnPc1	kulič
–	–	k?	–
neozdobeného	ozdobený	k2eNgInSc2d1	neozdobený
chleba	chléb	k1gInSc2	chléb
politého	politý	k2eAgInSc2d1	politý
sněhem	sníh	k1gInSc7	sníh
z	z	k7c2	z
bílků	bílek	k1gInPc2	bílek
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pečení	pečení	k1gNnSc4	pečení
paschy	pascha	k1gFnSc2	pascha
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
speciálně	speciálně	k6eAd1	speciálně
vytvořené	vytvořený	k2eAgFnPc1d1	vytvořená
keramické	keramický	k2eAgFnPc1d1	keramická
formy	forma	k1gFnPc1	forma
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
domácnostech	domácnost	k1gFnPc6	domácnost
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
i	i	k9	i
různě	různě	k6eAd1	různě
široké	široký	k2eAgInPc4d1	široký
hrnce	hrnec	k1gInPc4	hrnec
<g/>
.	.	kIx.	.
</s>
<s>
Paschu	Pascha	k1gFnSc4	Pascha
pečeme	péct	k5eAaImIp1nP	péct
v	v	k7c6	v
pecích	pec	k1gFnPc6	pec
na	na	k7c4	na
Velký	velký	k2eAgInSc4d1	velký
pátek	pátek	k1gInSc4	pátek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
GRYGAR	Grygar	k1gMnSc1	Grygar
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Věda	věda	k1gFnSc1	věda
a	a	k8xC	a
víra	víra	k1gFnSc1	víra
<g/>
.	.	kIx.	.
</s>
<s>
Kostelní	kostelní	k2eAgNnSc1d1	kostelní
Vydří	vydří	k2eAgNnSc1d1	vydří
<g/>
:	:	kIx,	:
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
128	[number]	k4	128
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7192	[number]	k4	7192
<g/>
-	-	kIx~	-
<g/>
535	[number]	k4	535
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KROLL	KROLL	kA	KROLL
<g/>
,	,	kIx,	,
G.	G.	kA	G.
Po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
Ježíšových	Ježíšův	k2eAgMnPc2d1	Ježíšův
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Kostelní	kostelní	k2eAgNnSc1d1	kostelní
Vydří	vydří	k2eAgNnSc1d1	vydří
<g/>
:	:	kIx,	:
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
486	[number]	k4	486
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7192	[number]	k4	7192
<g/>
-	-	kIx~	-
<g/>
711	[number]	k4	711
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
<g/>
́	́	k?	́
<g/>
CONNELL	CONNELL	kA	CONNELL
<g/>
,	,	kIx,	,
M	M	kA	M
<g/>
;	;	kIx,	;
AIFREY	AIFREY	kA	AIFREY
<g/>
,	,	kIx,	,
R.	R.	kA	R.
Znaky	znak	k1gInPc4	znak
a	a	k8xC	a
symboly	symbol	k1gInPc4	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Reader	Reader	k1gInSc1	Reader
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Digest	Digest	k1gInSc1	Digest
Výběr	výběr	k1gInSc1	výběr
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
256	[number]	k4	256
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86880	[number]	k4	86880
<g/>
-	-	kIx~	-
<g/>
96	[number]	k4	96
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZEMAN	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Hody	hod	k1gInPc1	hod
hody	hod	k1gInPc4	hod
doprovody	doprovod	k1gInPc1	doprovod
<g/>
,	,	kIx,	,
aneb	aneb	k?	aneb
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
slavily	slavit	k5eAaImAgFnP	slavit
Velikonoce	Velikonoce	k1gFnPc1	Velikonoce
ve	v	k7c6	v
Staré	Staré	k2eAgFnSc6d1	Staré
Praze	Praha	k1gFnSc6	Praha
Třebíč	Třebíč	k1gFnSc1	Třebíč
Akcent	akcent	k1gInSc4	akcent
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
260	[number]	k4	260
s.	s.	k?	s.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Výpočet	výpočet	k1gInSc1	výpočet
data	datum	k1gNnSc2	datum
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
</s>
</p>
<p>
<s>
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
</s>
</p>
<p>
<s>
Vzkříšení	vzkříšení	k1gNnSc1	vzkříšení
</s>
</p>
<p>
<s>
Kříž	Kříž	k1gMnSc1	Kříž
</s>
</p>
<p>
<s>
Pesach	pesach	k1gInSc1	pesach
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Velikonoce	Velikonoce	k1gFnPc1	Velikonoce
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Velikonoce	Velikonoce	k1gFnPc1	Velikonoce
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Velikonoce	Velikonoce	k1gFnPc1	Velikonoce
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Velikonoce	Velikonoce	k1gFnPc1	Velikonoce
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
Velikonoce	Velikonoce	k1gFnPc4	Velikonoce
ve	v	k7c6	v
Wikiknihách	Wikiknih	k1gInPc6	Wikiknih
</s>
</p>
<p>
<s>
Téma	téma	k1gFnSc1	téma
Velikonoční	velikonoční	k2eAgFnSc1d1	velikonoční
koleda	koleda	k1gFnSc1	koleda
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nP	slavit
Velikonoce	Velikonoce	k1gFnPc1	Velikonoce
</s>
</p>
<p>
<s>
Průvodce	průvodce	k1gMnSc1	průvodce
Velikonocemi	Velikonoce	k1gFnPc7	Velikonoce
</s>
</p>
<p>
<s>
Víra	víra	k1gFnSc1	víra
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Pastorace	pastorace	k1gFnSc1	pastorace
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Křesťanské	křesťanský	k2eAgNnSc1d1	křesťanské
slavení	slavení	k1gNnSc1	slavení
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
v	v	k7c6	v
prvních	první	k4xOgNnPc6	první
staletích	staletí	k1gNnPc6	staletí
(	(	kIx(	(
<g/>
Apologia	Apologia	k1gFnSc1	Apologia
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Velikonoce	Velikonoce	k1gFnPc1	Velikonoce
a	a	k8xC	a
staří	starý	k2eAgMnPc1d1	starý
Slované	Slovan	k1gMnPc1	Slovan
</s>
</p>
