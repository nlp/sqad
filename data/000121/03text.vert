<s>
Balon	balon	k1gInSc1	balon
(	(	kIx(	(
<g/>
správně	správně	k6eAd1	správně
také	také	k9	také
balón	balón	k1gInSc1	balón
<g/>
,	,	kIx,	,
odborně	odborně	k6eAd1	odborně
aerostat	aerostat	k1gInSc1	aerostat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnSc1	zařízení
lehčí	lehčit	k5eAaImIp3nS	lehčit
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
principu	princip	k1gInSc6	princip
Archimédova	Archimédův	k2eAgInSc2d1	Archimédův
zákona	zákon	k1gInSc2	zákon
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
vlastního	vlastní	k2eAgInSc2d1	vlastní
balónu	balón	k1gInSc2	balón
a	a	k8xC	a
koše	koš	k1gInSc2	koš
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějším	častý	k2eAgInSc7d3	nejčastější
typem	typ	k1gInSc7	typ
balónu	balón	k1gInSc2	balón
používaným	používaný	k2eAgInSc7d1	používaný
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
horkovzdušný	horkovzdušný	k2eAgInSc1d1	horkovzdušný
balon	balon	k1gInSc1	balon
<g/>
,	,	kIx,	,
plněný	plněný	k2eAgInSc1d1	plněný
zahřátým	zahřátý	k2eAgInSc7d1	zahřátý
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
,	,	kIx,	,
u	u	k7c2	u
řiditelných	řiditelný	k2eAgInPc2d1	řiditelný
balónů	balón	k1gInPc2	balón
pak	pak	k6eAd1	pak
vzducholodě	vzducholoď	k1gFnPc1	vzducholoď
plněné	plněný	k2eAgFnPc1d1	plněná
netečným	netečný	k2eAgInSc7d1	netečný
plynem	plyn	k1gInSc7	plyn
heliem	helium	k1gNnSc7	helium
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
balonového	balonový	k2eAgNnSc2d1	balonové
létání	létání	k1gNnSc2	létání
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
4	[number]	k4	4
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
1783	[number]	k4	1783
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vznesl	vznést	k5eAaPmAgInS	vznést
horkovzdušný	horkovzdušný	k2eAgInSc1d1	horkovzdušný
balon	balon	k1gInSc1	balon
bratrů	bratr	k1gMnPc2	bratr
Montgolfiérů	Montgolfiér	k1gMnPc2	Montgolfiér
před	před	k7c7	před
zraky	zrak	k1gInPc7	zrak
dvora	dvůr	k1gInSc2	dvůr
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgFnPc7	první
pasažéry	pasažér	k1gMnPc7	pasažér
byly	být	k5eAaImAgFnP	být
ovce	ovce	k1gFnSc1	ovce
<g/>
,	,	kIx,	,
kachna	kachna	k1gFnSc1	kachna
a	a	k8xC	a
kohout	kohout	k1gMnSc1	kohout
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
tentýž	týž	k3xTgInSc4	týž
rok	rok	k1gInSc4	rok
se	se	k3xPyFc4	se
vznesl	vznést	k5eAaPmAgMnS	vznést
i	i	k9	i
první	první	k4xOgMnSc1	první
plynem	plyn	k1gInSc7	plyn
plněný	plněný	k2eAgInSc1d1	plněný
balon	balon	k1gInSc1	balon
profesora	profesor	k1gMnSc2	profesor
Charlese	Charles	k1gMnSc2	Charles
<g/>
.	.	kIx.	.
</s>
<s>
Plynové	plynový	k2eAgInPc1d1	plynový
balony	balon	k1gInPc1	balon
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgInP	být
využívány	využívat	k5eAaPmNgInP	využívat
v	v	k7c6	v
civilní	civilní	k2eAgFnSc6d1	civilní
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
vojenské	vojenský	k2eAgFnSc6d1	vojenská
sféře	sféra	k1gFnSc6	sféra
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
plněny	plnit	k5eAaImNgInP	plnit
vodíkem	vodík	k1gInSc7	vodík
nebo	nebo	k8xC	nebo
svítiplynem	svítiplyn	k1gInSc7	svítiplyn
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
bezpečným	bezpečný	k2eAgNnSc7d1	bezpečné
heliem	helium	k1gNnSc7	helium
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
i	i	k8xC	i
první	první	k4xOgInPc1	první
použitelné	použitelný	k2eAgInPc1d1	použitelný
řiditelné	řiditelný	k2eAgInPc1d1	řiditelný
balony	balon	k1gInPc1	balon
-	-	kIx~	-
vzducholodě	vzducholoď	k1gFnPc1	vzducholoď
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
vojenské	vojenský	k2eAgNnSc4d1	vojenské
využití	využití	k1gNnSc4	využití
balonu	balon	k1gInSc2	balon
podal	podat	k5eAaPmAgMnS	podat
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1783	[number]	k4	1783
Francouz	Francouz	k1gMnSc1	Francouz
Giroud	Giroud	k1gMnSc1	Giroud
de	de	k?	de
Villette	Villett	k1gInSc5	Villett
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
letu	let	k1gInSc6	let
montgolfiérou	montgolfiéra	k1gFnSc7	montgolfiéra
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
praktické	praktický	k2eAgNnSc1d1	praktické
použití	použití	k1gNnSc1	použití
je	být	k5eAaImIp3nS	být
připisováno	připisovat	k5eAaImNgNnS	připisovat
jinému	jiný	k2eAgMnSc3d1	jiný
Francouzi	Francouz	k1gMnSc3	Francouz
<g/>
.	.	kIx.	.
</s>
<s>
Guiton	Guiton	k1gInSc1	Guiton
de	de	k?	de
Morveau	Morveaus	k1gInSc2	Morveaus
<g/>
,	,	kIx,	,
poradce	poradce	k1gMnSc1	poradce
Výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
státní	státní	k2eAgNnSc4d1	státní
blaho	blaho	k1gNnSc4	blaho
francouzské	francouzský	k2eAgFnSc2d1	francouzská
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
balonem	balon	k1gInSc7	balon
přepravit	přepravit	k5eAaPmF	přepravit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1793	[number]	k4	1793
depeše	depeše	k1gFnSc1	depeše
z	z	k7c2	z
obléhaného	obléhaný	k2eAgNnSc2d1	obléhané
města	město	k1gNnSc2	město
Condré	Condrý	k2eAgNnSc1d1	Condrý
<g/>
.	.	kIx.	.
</s>
<s>
Netěsný	těsný	k2eNgInSc1d1	netěsný
balon	balon	k1gInSc1	balon
však	však	k9	však
spadl	spadnout	k5eAaPmAgInS	spadnout
do	do	k7c2	do
ležení	ležení	k1gNnSc2	ležení
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
však	však	k9	však
přesto	přesto	k6eAd1	přesto
získal	získat	k5eAaPmAgMnS	získat
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
záměry	záměr	k1gInPc4	záměr
a	a	k8xC	a
už	už	k6eAd1	už
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
balonem	balon	k1gInSc7	balon
Entreprenant	Entreprenant	k1gInSc1	Entreprenant
(	(	kIx(	(
<g/>
podnikavý	podnikavý	k2eAgInSc1d1	podnikavý
<g/>
)	)	kIx)	)
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
u	u	k7c2	u
Maubeuge	Maubeug	k1gFnSc2	Maubeug
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
ale	ale	k9	ale
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
chemika	chemik	k1gMnSc4	chemik
A.	A.	kA	A.
Lavoisiera	Lavoisier	k1gMnSc4	Lavoisier
a	a	k8xC	a
fyzika	fyzik	k1gMnSc4	fyzik
Coutelle	Coutell	k1gMnSc4	Coutell
objevit	objevit	k5eAaPmF	objevit
a	a	k8xC	a
zvládnout	zvládnout	k5eAaPmF	zvládnout
nový	nový	k2eAgInSc4d1	nový
způsob	způsob	k1gInSc4	způsob
výroby	výroba	k1gFnSc2	výroba
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pruského	pruský	k2eAgNnSc2d1	pruské
obléhání	obléhání	k1gNnSc2	obléhání
Paříže	Paříž	k1gFnSc2	Paříž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
byly	být	k5eAaImAgInP	být
balony	balon	k1gInPc4	balon
jediným	jediný	k2eAgInSc7d1	jediný
prostředkem	prostředek	k1gInSc7	prostředek
pro	pro	k7c4	pro
spojení	spojení	k1gNnSc4	spojení
obránců	obránce	k1gMnPc2	obránce
města	město	k1gNnSc2	město
a	a	k8xC	a
zbytku	zbytek	k1gInSc2	zbytek
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
obléhání	obléhání	k1gNnSc2	obléhání
odstartovalo	odstartovat	k5eAaPmAgNnS	odstartovat
66	[number]	k4	66
balonů	balon	k1gInPc2	balon
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
většina	většina	k1gFnSc1	většina
dorazila	dorazit	k5eAaPmAgFnS	dorazit
s	s	k7c7	s
poštou	pošta	k1gFnSc7	pošta
a	a	k8xC	a
pasažéry	pasažér	k1gMnPc7	pasažér
mimo	mimo	k7c4	mimo
pruské	pruský	k2eAgNnSc4d1	pruské
obklíčení	obklíčení	k1gNnSc4	obklíčení
<g/>
.	.	kIx.	.
</s>
<s>
Upoutané	upoutaný	k2eAgInPc1d1	upoutaný
balony	balon	k1gInPc1	balon
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
devatenáctém	devatenáctý	k4xOgInSc6	devatenáctý
a	a	k8xC	a
první	první	k4xOgFnSc3	první
polovině	polovina	k1gFnSc3	polovina
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
využívány	využíván	k2eAgFnPc1d1	využívána
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
jako	jako	k9	jako
pozorovací	pozorovací	k2eAgInSc4d1	pozorovací
prostředek	prostředek	k1gInSc4	prostředek
-	-	kIx~	-
především	především	k9	především
pro	pro	k7c4	pro
řízení	řízení	k1gNnSc4	řízení
dělostřelecké	dělostřelecký	k2eAgFnSc2d1	dělostřelecká
palby	palba	k1gFnSc2	palba
<g/>
,	,	kIx,	,
od	od	k7c2	od
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
jako	jako	k8xC	jako
protiletadlová	protiletadlový	k2eAgFnSc1d1	protiletadlová
baráž	baráž	k1gFnSc1	baráž
<g/>
,	,	kIx,	,
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
pak	pak	k6eAd1	pak
jako	jako	k8xS	jako
protiponorková	protiponorkový	k2eAgFnSc1d1	protiponorková
stráž	stráž	k1gFnSc1	stráž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1901	[number]	k4	1901
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
němečtí	německý	k2eAgMnPc1d1	německý
vzduchoplavci	vzduchoplavec	k1gMnPc1	vzduchoplavec
Berson	Berson	k1gMnSc1	Berson
a	a	k8xC	a
Suerig	Suerig	k1gMnSc1	Suerig
výšky	výška	k1gFnSc2	výška
10	[number]	k4	10
800	[number]	k4	800
m	m	kA	m
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
rekordem	rekord	k1gInSc7	rekord
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
výškové	výškový	k2eAgInPc4d1	výškový
rekordy	rekord	k1gInPc4	rekord
několikrát	několikrát	k6eAd1	několikrát
překonány	překonán	k2eAgInPc4d1	překonán
různými	různý	k2eAgInPc7d1	různý
týmy	tým	k1gInPc7	tým
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
překonal	překonat	k5eAaPmAgMnS	překonat
výškový	výškový	k2eAgInSc4d1	výškový
rekord	rekord	k1gInSc4	rekord
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Švýcar	Švýcar	k1gMnSc1	Švýcar
Auguste	August	k1gMnSc5	August
Piccard	Piccarda	k1gFnPc2	Piccarda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
speciálně	speciálně	k6eAd1	speciálně
vyrobeném	vyrobený	k2eAgInSc6d1	vyrobený
balónu	balón	k1gInSc6	balón
o	o	k7c6	o
objemu	objem	k1gInSc6	objem
14	[number]	k4	14
130	[number]	k4	130
m3	m3	k4	m3
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1931	[number]	k4	1931
výšky	výška	k1gFnSc2	výška
15	[number]	k4	15
780	[number]	k4	780
m.	m.	k?	m.
Závody	závod	k1gInPc1	závod
o	o	k7c4	o
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
dosaženou	dosažený	k2eAgFnSc4d1	dosažená
výšku	výška	k1gFnSc4	výška
však	však	k9	však
také	také	k6eAd1	také
měly	mít	k5eAaImAgInP	mít
své	svůj	k3xOyFgFnPc4	svůj
oběti	oběť	k1gFnPc4	oběť
-	-	kIx~	-
například	například	k6eAd1	například
posádku	posádka	k1gFnSc4	posádka
sovětského	sovětský	k2eAgInSc2d1	sovětský
stratostatu	stratostat	k1gInSc2	stratostat
Osoaviachim-	Osoaviachim-	k1gFnSc2	Osoaviachim-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgInSc4d1	poslední
výškový	výškový	k2eAgInSc4d1	výškový
rekord	rekord	k1gInSc4	rekord
byl	být	k5eAaImAgInS	být
dosažen	dosažen	k2eAgInSc1d1	dosažen
dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
<g/>
/	/	kIx~	/
<g/>
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Felix	Felix	k1gMnSc1	Felix
Baumgartner	Baumgartner	k1gMnSc1	Baumgartner
vystoupal	vystoupat	k5eAaPmAgMnS	vystoupat
ve	v	k7c6	v
speciálním	speciální	k2eAgInSc6d1	speciální
heliovém	heliový	k2eAgInSc6d1	heliový
balónu	balón	k1gInSc6	balón
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
39	[number]	k4	39
0	[number]	k4	0
<g/>
44	[number]	k4	44
<g/>
m	m	kA	m
(	(	kIx(	(
<g/>
128	[number]	k4	128
097	[number]	k4	097
stop	stopa	k1gFnPc2	stopa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
seskočil	seskočit	k5eAaPmAgMnS	seskočit
s	s	k7c7	s
padákem	padák	k1gInSc7	padák
a	a	k8xC	a
pokořil	pokořit	k5eAaPmAgMnS	pokořit
tím	ten	k3xDgMnSc7	ten
další	další	k2eAgInPc4d1	další
3	[number]	k4	3
světové	světový	k2eAgInPc4d1	světový
rekordy	rekord	k1gInPc4	rekord
-	-	kIx~	-
seskok	seskok	k1gInSc4	seskok
z	z	k7c2	z
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
výšky	výška	k1gFnSc2	výška
(	(	kIx(	(
<g/>
39	[number]	k4	39
<g/>
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
pádová	pádový	k2eAgFnSc1d1	pádová
rychlost	rychlost	k1gFnSc1	rychlost
(	(	kIx(	(
<g/>
1342	[number]	k4	1342
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
volným	volný	k2eAgInSc7d1	volný
pádem	pád	k1gInSc7	pád
překonal	překonat	k5eAaPmAgInS	překonat
rychlost	rychlost	k1gFnSc4	rychlost
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Balón	balón	k1gInSc1	balón
s	s	k7c7	s
gondolou	gondola	k1gFnSc7	gondola
následně	následně	k6eAd1	následně
přistál	přistát	k5eAaPmAgInS	přistát
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
padáku	padák	k1gInSc6	padák
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
rekuperován	rekuperovat	k5eAaBmNgInS	rekuperovat
<g/>
.	.	kIx.	.
</s>
<s>
Renesanci	renesance	k1gFnSc4	renesance
horkovzdušných	horkovzdušný	k2eAgInPc2d1	horkovzdušný
balonů	balon	k1gInPc2	balon
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
umožnil	umožnit	k5eAaPmAgInS	umožnit
dostupný	dostupný	k2eAgInSc1d1	dostupný
stlačený	stlačený	k2eAgInSc1d1	stlačený
propan-butan	propanutan	k1gInSc1	propan-butan
jako	jako	k8xC	jako
zdroj	zdroj	k1gInSc1	zdroj
tepla	teplo	k1gNnSc2	teplo
pro	pro	k7c4	pro
ohřev	ohřev	k1gInSc4	ohřev
vzduchu	vzduch	k1gInSc2	vzduch
i	i	k9	i
dostatečně	dostatečně	k6eAd1	dostatečně
lehká	lehký	k2eAgFnSc1d1	lehká
a	a	k8xC	a
pevná	pevný	k2eAgFnSc1d1	pevná
nylonová	nylonový	k2eAgFnSc1d1	nylonová
tkanina	tkanina	k1gFnSc1	tkanina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
obalu	obal	k1gInSc2	obal
balónu	balón	k1gInSc2	balón
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
byl	být	k5eAaImAgInS	být
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
rychlý	rychlý	k2eAgInSc1d1	rychlý
a	a	k8xC	a
mobilní	mobilní	k2eAgInSc1d1	mobilní
pozorovací	pozorovací	k2eAgInSc1d1	pozorovací
prostředek	prostředek	k1gInSc1	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
byl	být	k5eAaImAgInS	být
projekt	projekt	k1gInSc1	projekt
uvolněn	uvolněn	k2eAgInSc1d1	uvolněn
k	k	k7c3	k
civilnímu	civilní	k2eAgNnSc3d1	civilní
využití	využití	k1gNnSc3	využití
a	a	k8xC	a
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
koníčkem	koníček	k1gMnSc7	koníček
mnoha	mnoho	k4c2	mnoho
nadšenců	nadšenec	k1gMnPc2	nadšenec
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
český	český	k2eAgInSc1d1	český
horkovzdušný	horkovzdušný	k2eAgInSc1d1	horkovzdušný
balón	balón	k1gInSc1	balón
byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
brněnským	brněnský	k2eAgInSc7d1	brněnský
Aviatik	aviatika	k1gFnPc2	aviatika
Klubem	klub	k1gInSc7	klub
a	a	k8xC	a
veřejnosti	veřejnost	k1gFnSc3	veřejnost
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Balóny	balón	k1gInPc1	balón
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
i	i	k9	i
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
například	například	k6eAd1	například
v	v	k7c6	v
románech	román	k1gInPc6	román
Julese	Julese	k1gFnSc2	Julese
Vernea	Verne	k1gInSc2	Verne
(	(	kIx(	(
<g/>
Pět	pět	k4xCc1	pět
neděl	neděle	k1gFnPc2	neděle
v	v	k7c6	v
balóně	balón	k1gInSc6	balón
<g/>
,	,	kIx,	,
Tajuplný	tajuplný	k2eAgInSc1d1	tajuplný
ostrov	ostrov	k1gInSc1	ostrov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pohár	pohár	k1gInSc1	pohár
Gordona	Gordon	k1gMnSc2	Gordon
Bennetta	Bennett	k1gMnSc2	Bennett
Balónová	balónový	k2eAgFnSc1d1	balónová
fotografie	fotografie	k1gFnSc1	fotografie
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
balon	balon	k1gInSc4	balon
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Galerie	galerie	k1gFnSc2	galerie
balon	balon	k1gInSc4	balon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
balón	balón	k1gInSc1	balón
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Zpráva	zpráva	k1gFnSc1	zpráva
Francie	Francie	k1gFnSc1	Francie
<g/>
:	:	kIx,	:
225	[number]	k4	225
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
prvního	první	k4xOgMnSc2	první
letu	let	k1gInSc2	let
balónem	balón	k1gInSc7	balón
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
Historie	historie	k1gFnSc2	historie
balónového	balónový	k2eAgNnSc2d1	balónové
létání	létání	k1gNnSc2	létání
Balónové	balónový	k2eAgNnSc4d1	balónové
létání	létání	k1gNnSc4	létání
Let	léto	k1gNnPc2	léto
balónem	balón	k1gInSc7	balón
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
</s>
