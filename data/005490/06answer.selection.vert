<s>
Hektar	hektar	k1gInSc1	hektar
je	být	k5eAaImIp3nS	být
plocha	plocha	k1gFnSc1	plocha
čtverce	čtverec	k1gInSc2	čtverec
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
100	[number]	k4	100
<g/>
×	×	k?	×
<g/>
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
1	[number]	k4	1
km2	km2	k4	km2
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
100	[number]	k4	100
hektarům	hektar	k1gInPc3	hektar
<g/>
.	.	kIx.	.
</s>
