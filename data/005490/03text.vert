<s>
Rozloha	rozloha	k1gFnSc1	rozloha
neboli	neboli	k8xC	neboli
plocha	plocha	k1gFnSc1	plocha
je	být	k5eAaImIp3nS	být
údaj	údaj	k1gInSc4	údaj
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
území	území	k1gNnPc2	území
(	(	kIx(	(
<g/>
např.	např.	kA	např.
státu	stát	k1gInSc2	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Určuje	určovat	k5eAaImIp3nS	určovat
se	se	k3xPyFc4	se
v	v	k7c6	v
plošných	plošný	k2eAgFnPc6d1	plošná
mírách	míra	k1gFnPc6	míra
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
km2	km2	k4	km2
(	(	kIx(	(
<g/>
kilometr	kilometr	k1gInSc4	kilometr
čtvereční	čtvereční	k2eAgInSc4d1	čtvereční
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
ha	ha	kA	ha
(	(	kIx(	(
<g/>
hektar	hektar	k1gInSc4	hektar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hektar	hektar	k1gInSc1	hektar
je	být	k5eAaImIp3nS	být
plocha	plocha	k1gFnSc1	plocha
čtverce	čtverec	k1gInSc2	čtverec
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
100	[number]	k4	100
<g/>
×	×	k?	×
<g/>
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
1	[number]	k4	1
km2	km2	k4	km2
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
100	[number]	k4	100
hektarům	hektar	k1gInPc3	hektar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hektarech	hektar	k1gInPc6	hektar
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
rozloha	rozloha	k1gFnSc1	rozloha
některých	některý	k3yIgNnPc2	některý
menších	malý	k2eAgNnPc2d2	menší
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
např.	např.	kA	např.
přírodních	přírodní	k2eAgFnPc2d1	přírodní
rezervací	rezervace	k1gFnPc2	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
země	zem	k1gFnPc1	zem
používají	používat	k5eAaImIp3nP	používat
vlastní	vlastní	k2eAgFnPc1d1	vlastní
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
vybočující	vybočující	k2eAgFnPc1d1	vybočující
z	z	k7c2	z
metrického	metrický	k2eAgInSc2d1	metrický
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
se	se	k3xPyFc4	se
rozloha	rozloha	k1gFnSc1	rozloha
udává	udávat	k5eAaImIp3nS	udávat
ve	v	k7c6	v
čtverečních	čtvereční	k2eAgFnPc6d1	čtvereční
mílích	míle	k1gFnPc6	míle
(	(	kIx(	(
<g/>
1	[number]	k4	1
mi2	mi2	k4	mi2
=	=	kIx~	=
2,589	[number]	k4	2,589
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
496	[number]	k4	496
km2	km2	k4	km2
neboli	neboli	k8xC	neboli
49	[number]	k4	49
600	[number]	k4	600
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Okres	okres	k1gInSc1	okres
Mělník	Mělník	k1gInSc1	Mělník
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
712,4	[number]	k4	712,4
km2	km2	k4	km2
Česko	Česko	k1gNnSc1	Česko
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
78	[number]	k4	78
867	[number]	k4	867
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Bývalé	bývalý	k2eAgNnSc1d1	bývalé
Československo	Československo	k1gNnSc1	Československo
mělo	mít	k5eAaImAgNnS	mít
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
rozlohu	rozloh	k1gInSc2	rozloh
127	[number]	k4	127
876	[number]	k4	876
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
má	mít	k5eAaImIp3nS	mít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
rozlohu	rozloh	k1gInSc2	rozloh
4	[number]	k4	4
381	[number]	k4	381
376	[number]	k4	376
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Evropa	Evropa	k1gFnSc1	Evropa
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
10	[number]	k4	10
058	[number]	k4	058
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
stát	stát	k1gInSc1	stát
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
17	[number]	k4	17
128	[number]	k4	128
426	[number]	k4	426
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gFnSc1	náš
planeta	planeta	k1gFnSc1	planeta
Země	zem	k1gFnSc2	zem
má	mít	k5eAaImIp3nS	mít
povrch	povrch	k1gInSc4	povrch
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
510	[number]	k4	510
065	[number]	k4	065
284	[number]	k4	284
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
Seznam	seznam	k1gInSc1	seznam
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
podle	podle	k7c2	podle
rozlohy	rozloha	k1gFnSc2	rozloha
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
rozloha	rozloha	k1gFnSc1	rozloha
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
