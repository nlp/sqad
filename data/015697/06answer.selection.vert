<s desamb="1">
Tříletá	tříletý	k2eAgFnSc1d1
krvavá	krvavý	k2eAgFnSc1d1
britsko-búrská	britsko-búrský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
zpočátku	zpočátku	k6eAd1
Búrové	Búr	k1gMnPc1
vítězili	vítězit	k5eAaImAgMnP
<g/>
,	,	kIx,
skončila	skončit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1902	#num#	k4
anexí	anexe	k1gFnPc2
búrských	búrský	k2eAgFnPc2d1
republik	republika	k1gFnPc2
a	a	k8xC
rozšířením	rozšíření	k1gNnSc7
britského	britský	k2eAgNnSc2d1
koloniálního	koloniální	k2eAgNnSc2d1
panství	panství	k1gNnSc2
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
.	.	kIx.
</s>