<s>
Druhá	druhý	k4xOgFnSc1
búrská	búrský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
probíhala	probíhat	k5eAaImAgFnS
mezi	mezi	k7c7
anglickými	anglický	k2eAgMnPc7d1
a	a	k8xC
búrskými	búrský	k2eAgMnPc7d1
kolonizátory	kolonizátor	k1gMnPc7
Jižní	jižní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
(	(	kIx(
<g/>
tj.	tj.	kA
potomky	potomek	k1gMnPc7
nizozemských	nizozemský	k2eAgMnPc2d1
osadníků	osadník	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
v	v	k7c6
letech	let	k1gInPc6
1899	#num#	k4
–	–	k?
1902	#num#	k4
<g/>
.	.	kIx.
</s>