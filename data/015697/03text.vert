<s>
Druhá	druhý	k4xOgFnSc1
búrská	búrský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
búrská	búrský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Búrské	búrský	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
Búrská	búrský	k2eAgNnPc1d1
komanda	komando	k1gNnPc1
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1899	#num#	k4
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1902	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
<g/>
,	,	kIx,
Svazijsko	Svazijsko	k1gNnSc1
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Britské	britský	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Oranžský	oranžský	k2eAgInSc1d1
svobodný	svobodný	k2eAgInSc1d1
stát	stát	k1gInSc1
Transvaal	Transvaal	k1gInSc1
</s>
<s>
Britské	britský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
</s>
<s>
Kapská	kapský	k2eAgFnSc1d1
kolonie	kolonie	k1gFnSc1
</s>
<s>
Kolonie	kolonie	k1gFnSc1
Natal	Natal	k1gInSc1
</s>
<s>
Rhodesie	Rhodesie	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Britská	britský	k2eAgFnSc1d1
Indie	Indie	k1gFnSc1
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Paul	Paul	k1gMnSc1
KrugerLouis	KrugerLouis	k1gFnSc2
BothaKoos	BothaKoos	k1gMnSc1
de	de	k?
la	la	k1gNnPc2
ReyMartinus	ReyMartinus	k1gMnSc1
Theunis	Theunis	k1gFnSc2
SteynChristiaan	SteynChristiaan	k1gMnSc1
de	de	k?
WetPiet	WetPiet	k1gMnSc1
CronjeFrederick	CronjeFrederick	k1gMnSc1
Joubert	Joubert	k1gMnSc1
DuquesneJan	DuquesneJan	k1gMnSc1
Christian	Christian	k1gMnSc1
Smuts	Smuts	k1gInSc4
</s>
<s>
Alfred	Alfred	k1gMnSc1
MilnerRedvers	MilnerRedversa	k1gFnPc2
BullerHoratio	BullerHoratio	k1gMnSc1
KitchenerFrederick	KitchenerFrederick	k1gMnSc1
RobertsRobert	RobertsRobert	k1gMnSc1
Baden-Powell	Baden-Powell	k1gMnSc1
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
9093	#num#	k4
mrtvých	mrtvý	k2eAgMnPc2d1
</s>
<s>
7884	#num#	k4
padlých	padlý	k2eAgMnPc2d1
v	v	k7c6
boji	boj	k1gInSc6
nebo	nebo	k8xC
následkem	následkem	k7c2
zranění	zranění	k1gNnSc2
<g/>
;	;	kIx,
13	#num#	k4
250	#num#	k4
mrtvých	mrtvý	k1gMnPc2
z	z	k7c2
jiných	jiný	k2eAgFnPc2d1
příčin	příčina	k1gFnPc2
<g/>
;	;	kIx,
934	#num#	k4
nezvěstných	zvěstný	k2eNgMnPc2d1
<g/>
;	;	kIx,
22	#num#	k4
828	#num#	k4
raněných	raněný	k1gMnPc2
</s>
<s>
Druhá	druhý	k4xOgFnSc1
búrská	búrský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
probíhala	probíhat	k5eAaImAgFnS
mezi	mezi	k7c7
anglickými	anglický	k2eAgMnPc7d1
a	a	k8xC
búrskými	búrský	k2eAgMnPc7d1
kolonizátory	kolonizátor	k1gMnPc7
Jižní	jižní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
(	(	kIx(
<g/>
tj.	tj.	kA
potomky	potomek	k1gMnPc7
nizozemských	nizozemský	k2eAgMnPc2d1
osadníků	osadník	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
v	v	k7c6
letech	let	k1gInPc6
1899	#num#	k4
–	–	k?
1902	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
</s>
<s>
Válka	válka	k1gFnSc1
byla	být	k5eAaImAgFnS
vedena	vést	k5eAaImNgFnS
nesmírně	smírně	k6eNd1
krutými	krutý	k2eAgInPc7d1
prostředky	prostředek	k1gInPc7
<g/>
,	,	kIx,
poprvé	poprvé	k6eAd1
se	se	k3xPyFc4
vyskytly	vyskytnout	k5eAaPmAgInP
koncentrační	koncentrační	k2eAgInPc1d1
tábory	tábor	k1gInPc1
moderního	moderní	k2eAgInSc2d1
typu	typ	k1gInSc2
pro	pro	k7c4
rodiny	rodina	k1gFnSc2
bojujícího	bojující	k2eAgMnSc4d1
nepřítele	nepřítel	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajatí	zajatý	k2eAgMnPc1d1
Búrové	Búr	k1gMnPc1
byli	být	k5eAaImAgMnP
deportováni	deportovat	k5eAaBmNgMnP
například	například	k6eAd1
na	na	k7c4
Ceylon	Ceylon	k1gInSc4
a	a	k8xC
ostrov	ostrov	k1gInSc4
Svaté	svatý	k2eAgFnSc2d1
Heleny	Helena	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Vězni	vězeň	k1gMnPc1
umírali	umírat	k5eAaImAgMnP
hlavně	hlavně	k9
na	na	k7c4
infekční	infekční	k2eAgFnPc4d1
nemoci	nemoc	k1gFnPc4
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
celkově	celkově	k6eAd1
zemřelo	zemřít	k5eAaPmAgNnS
asi	asi	k9
30	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíce	nejvíce	k6eAd1,k6eAd3
však	však	k9
utrpělo	utrpět	k5eAaPmAgNnS
černošské	černošský	k2eAgNnSc1d1
obyvatelstvo	obyvatelstvo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
mělo	mít	k5eAaImAgNnS
největší	veliký	k2eAgFnPc4d3
ztráty	ztráta	k1gFnPc4
a	a	k8xC
bylo	být	k5eAaImAgNnS
násilně	násilně	k6eAd1
přesídlováno	přesídlován	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
k	k	k7c3
válce	válka	k1gFnSc3
byla	být	k5eAaImAgFnS
touha	touha	k1gFnSc1
po	po	k7c6
kontrole	kontrola	k1gFnSc6
území	území	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nacházela	nacházet	k5eAaImAgNnP
bohatá	bohatý	k2eAgNnPc1d1
naleziště	naleziště	k1gNnPc1
surovin	surovina	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
zlata	zlato	k1gNnSc2
(	(	kIx(
<g/>
to	ten	k3xDgNnSc1
se	se	k3xPyFc4
Búrové	Búr	k1gMnPc1
pokoušeli	pokoušet	k5eAaImAgMnP
odvézt	odvézt	k5eAaPmF
do	do	k7c2
Mosambiku	Mosambik	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
touha	touha	k1gFnSc1
po	po	k7c6
rozšíření	rozšíření	k1gNnSc6
pastvin	pastvina	k1gFnPc2
pro	pro	k7c4
chov	chov	k1gInSc4
dobytka	dobytek	k1gInSc2
a	a	k8xC
v	v	k7c6
neposlední	poslední	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
také	také	k9
snaha	snaha	k1gFnSc1
podrobit	podrobit	k5eAaPmF
odbojné	odbojný	k2eAgInPc4d1
státy	stát	k1gInPc4
(	(	kIx(
<g/>
Transvaal	Transvaal	k1gMnSc1
<g/>
,	,	kIx,
Orange	Orange	k1gFnSc1
<g/>
)	)	kIx)
anglické	anglický	k2eAgFnSc3d1
koruně	koruna	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válku	válek	k1gInSc2
také	také	k9
díky	díky	k7c3
nesrovnatelně	srovnatelně	k6eNd1
technicky	technicky	k6eAd1
lépe	dobře	k6eAd2
vybavené	vybavený	k2eAgFnSc6d1
armádě	armáda	k1gFnSc6
vyhráli	vyhrát	k5eAaPmAgMnP
Angličané	Angličan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInSc4d3
zájem	zájem	k1gInSc4
projevila	projevit	k5eAaPmAgFnS
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
o	o	k7c4
zlatonosná	zlatonosný	k2eAgNnPc4d1
a	a	k8xC
diamantová	diamantový	k2eAgNnPc4d1
ložiska	ložisko	k1gNnPc4
nedaleko	nedaleko	k7c2
Johannesburgu	Johannesburg	k1gInSc2
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
kraje	kraj	k1gInPc4
ale	ale	k8xC
již	již	k6eAd1
kromě	kromě	k7c2
domorodců	domorodec	k1gMnPc2
obývali	obývat	k5eAaImAgMnP
nizozemští	nizozemský	k2eAgMnPc1d1
osadníci	osadník	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
zde	zde	k6eAd1
zřídili	zřídit	k5eAaPmAgMnP
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spory	spor	k1gInPc1
nových	nový	k2eAgMnPc2d1
anglických	anglický	k2eAgMnPc2d1
přistěhovalců	přistěhovalec	k1gMnPc2
s	s	k7c7
nizozemskými	nizozemský	k2eAgMnPc7d1
usedlíky	usedlík	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
nazývali	nazývat	k5eAaImAgMnP
Búrové	Búr	k1gMnPc1
<g/>
,	,	kIx,
daly	dát	k5eAaPmAgFnP
Velké	velký	k2eAgFnPc1d1
Británii	Británie	k1gFnSc3
záminku	záminka	k1gFnSc4
k	k	k7c3
ozbrojenému	ozbrojený	k2eAgInSc3d1
zásahu	zásah	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tříletá	tříletý	k2eAgFnSc1d1
krvavá	krvavý	k2eAgFnSc1d1
britsko-búrská	britsko-búrský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
zpočátku	zpočátku	k6eAd1
Búrové	Búr	k1gMnPc1
vítězili	vítězit	k5eAaImAgMnP
<g/>
,	,	kIx,
skončila	skončit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1902	#num#	k4
anexí	anexe	k1gFnPc2
búrských	búrský	k2eAgFnPc2d1
republik	republika	k1gFnPc2
a	a	k8xC
rozšířením	rozšíření	k1gNnSc7
britského	britský	k2eAgNnSc2d1
koloniálního	koloniální	k2eAgNnSc2d1
panství	panství	k1gNnSc2
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Ve	v	k7c6
válce	válka	k1gFnSc6
sloužily	sloužit	k5eAaImAgFnP
i	i	k8xC
dvě	dva	k4xCgFnPc4
rhodéské	rhodéský	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
<g/>
,	,	kIx,
Rhodéský	Rhodéský	k2eAgInSc4d1
regiment	regiment	k1gInSc4
a	a	k8xC
Dobrovolníci	dobrovolník	k1gMnPc1
z	z	k7c2
Jižní	jižní	k2eAgFnSc2d1
Rhodésie	Rhodésie	k1gFnSc2
<g/>
,	,	kIx,
dohromadfy	dohromadf	k1gInPc4
asi	asi	k9
1000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viz	vidět	k5eAaImRp2nS
</s>
<s>
KEPPEL-JONES	KEPPEL-JONES	k?
<g/>
,	,	kIx,
Arthur	Arthur	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rhodes	Rhodes	k1gMnSc1
and	and	k?
Rhodesia	Rhodesia	k1gFnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc5
White	Whit	k1gMnSc5
Conquest	Conquest	k1gFnSc1
of	of	k?
Zimbabwe	Zimbabwe	k1gNnSc1
<g/>
,	,	kIx,
1884	#num#	k4
<g/>
–	–	k?
<g/>
1902	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Montreal	Montreal	k1gInSc1
<g/>
,	,	kIx,
Quebec	Quebec	k1gInSc1
and	and	k?
Kingston	Kingston	k1gInSc1
<g/>
,	,	kIx,
Ontario	Ontario	k1gNnSc1
<g/>
:	:	kIx,
McGill-Queen	McGill-Queen	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
University	universita	k1gFnPc1
Press	Press	k1gInSc1
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780773505346	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
590	#num#	k4
<g/>
–	–	k?
<g/>
599	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Oslava	oslava	k1gFnSc1
Krügerových	Krügerův	k2eAgFnPc2d1
narozenin	narozeniny	k1gFnPc2
zajatými	zajatý	k2eAgNnPc7d1
Boery	Boero	k1gNnPc7
<g/>
..	..	k?
Illustrovaný	Illustrovaný	k2eAgInSc4d1
svět	svět	k1gInSc4
<g/>
.	.	kIx.
1901	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	I	kA
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
21	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
642	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
NERAD	nerad	k2eAgMnSc1d1
<g/>
,	,	kIx,
Filip	Filip	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Búrská	búrský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Aleš	Aleš	k1gMnSc1
Skřivan	Skřivan	k1gMnSc1
ml.	ml.	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
232	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86493	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
SKŘIVAN	Skřivan	k1gMnSc1
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Die	Die	k1gFnSc1
Welt	Welta	k1gFnPc2
vor	vor	k1gInSc4
dem	dem	k?
Ersten	Ersten	k2eAgMnSc1d1
Weltkrieg	Weltkrieg	k1gMnSc1
<g/>
:	:	kIx,
Die	Die	k1gMnSc1
Entwicklung	Entwicklung	k1gMnSc1
im	im	k?
Süden	Süden	k2eAgMnSc1d1
Afrikas	Afrikas	k1gMnSc1
und	und	k?
der	drát	k5eAaImRp2nS
Burenkrieg	Burenkrieg	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prague	Prague	k1gNnSc1
Papers	Papersa	k1gFnPc2
on	on	k3xPp3gMnSc1
the	the	k?
History	Histor	k1gInPc4
of	of	k?
International	International	k1gFnSc2
Relations	Relationsa	k1gFnPc2
<g/>
.	.	kIx.
2001	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
5	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
85	#num#	k4
<g/>
-	-	kIx~
<g/>
100	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
PDF	PDF	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7308	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
16	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
druhá	druhý	k4xOgFnSc1
búrská	búrský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
druhá	druhý	k4xOgFnSc1
búrská	búrský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
118465	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4147009-6	4147009-6	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85125514	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85125514	#num#	k4
</s>
