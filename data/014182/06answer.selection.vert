<s desamb="1">
Při	při	k7c6
zkoumání	zkoumání	k1gNnSc6
hospodářského	hospodářský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
rozlišovat	rozlišovat	k5eAaImF
mezi	mezi	k7c7
cyklickými	cyklický	k2eAgInPc7d1
jevy	jev	k1gInPc7
a	a	k8xC
běžnými	běžný	k2eAgFnPc7d1
fluktuacemi	fluktuace	k1gFnPc7
ekonomiky	ekonomika	k1gFnSc2
<g/>
.	.	kIx.
</s>