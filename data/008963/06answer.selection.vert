<s>
Pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
jen	jen	k9	jen
několik	několik	k4yIc1	několik
měst	město	k1gNnPc2	město
na	na	k7c6	na
západě	západ	k1gInSc6	západ
země	zem	k1gFnSc2	zem
a	a	k8xC	a
především	především	k6eAd1	především
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Tripolis	Tripolis	k1gInSc1	Tripolis
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yRgMnPc4	který
média	médium	k1gNnPc1	médium
předpokládaly	předpokládat	k5eAaImAgFnP	předpokládat
nejtěžší	těžký	k2eAgInPc4d3	nejtěžší
boje	boj	k1gInPc4	boj
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Kaddáfí	Kaddáfí	k1gMnSc1	Kaddáfí
se	se	k3xPyFc4	se
nemínil	mínit	k5eNaImAgMnS	mínit
vzdát	vzdát	k5eAaPmF	vzdát
a	a	k8xC	a
opřít	opřít	k5eAaPmF	opřít
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
minimálně	minimálně	k6eAd1	minimálně
o	o	k7c4	o
svou	svůj	k3xOyFgFnSc4	svůj
elitní	elitní	k2eAgFnSc4d1	elitní
prezidentskou	prezidentský	k2eAgFnSc4d1	prezidentská
gardu	garda	k1gFnSc4	garda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
tvořilo	tvořit	k5eAaImAgNnS	tvořit
3	[number]	k4	3
000	[number]	k4	000
dobře	dobře	k6eAd1	dobře
vyzbrojených	vyzbrojený	k2eAgMnPc2d1	vyzbrojený
a	a	k8xC	a
vycvičených	vycvičený	k2eAgMnPc2d1	vycvičený
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
