<p>
<s>
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
začala	začít	k5eAaPmAgFnS	začít
demonstracemi	demonstrace	k1gFnPc7	demonstrace
a	a	k8xC	a
protesty	protest	k1gInPc7	protest
v	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
protesty	protest	k1gInPc4	protest
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
arabských	arabský	k2eAgFnPc6d1	arabská
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Libyjci	Libyjec	k1gMnPc1	Libyjec
protestovali	protestovat	k5eAaBmAgMnP	protestovat
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2011	[number]	k4	2011
hlavně	hlavně	k6eAd1	hlavně
proti	proti	k7c3	proti
vůdci	vůdce	k1gMnSc3	vůdce
Muammaru	Muammar	k1gMnSc3	Muammar
Kaddáfímu	Kaddáfí	k1gMnSc3	Kaddáfí
<g/>
.	.	kIx.	.
</s>
<s>
Režim	režim	k1gInSc1	režim
reagoval	reagovat	k5eAaBmAgInS	reagovat
střílením	střílení	k1gNnSc7	střílení
do	do	k7c2	do
demonstrantů	demonstrant	k1gMnPc2	demonstrant
a	a	k8xC	a
nasazením	nasazení	k1gNnSc7	nasazení
armády	armáda	k1gFnSc2	armáda
včetně	včetně	k7c2	včetně
letadel	letadlo	k1gNnPc2	letadlo
k	k	k7c3	k
potlačení	potlačení	k1gNnSc3	potlačení
protestů	protest	k1gInPc2	protest
<g/>
.	.	kIx.	.
</s>
<s>
Počty	počet	k1gInPc1	počet
mrtvých	mrtvý	k1gMnPc2	mrtvý
se	se	k3xPyFc4	se
různí	různit	k5eAaImIp3nS	různit
<g/>
,	,	kIx,	,
samotná	samotný	k2eAgFnSc1d1	samotná
Libye	Libye	k1gFnSc1	Libye
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
přiznávala	přiznávat	k5eAaImAgFnS	přiznávat
300	[number]	k4	300
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
odhadovala	odhadovat	k5eAaImAgFnS	odhadovat
až	až	k6eAd1	až
tisíc	tisíc	k4xCgInSc1	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Demonstrovalo	demonstrovat	k5eAaBmAgNnS	demonstrovat
se	se	k3xPyFc4	se
především	především	k9	především
v	v	k7c6	v
největších	veliký	k2eAgNnPc6d3	veliký
městech	město	k1gNnPc6	město
(	(	kIx(	(
<g/>
Tripolis	Tripolis	k1gInSc1	Tripolis
<g/>
,	,	kIx,	,
Benghází	Bengháze	k1gFnPc2	Bengháze
<g/>
,	,	kIx,	,
Al	ala	k1gFnPc2	ala
Bayda	Bayda	k1gFnSc1	Bayda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
<s>
Demonstrující	demonstrující	k2eAgMnPc1d1	demonstrující
podpalovali	podpalovat	k5eAaImAgMnP	podpalovat
policejní	policejní	k2eAgFnPc4d1	policejní
stanice	stanice	k1gFnPc4	stanice
<g/>
,	,	kIx,	,
dostaly	dostat	k5eAaPmAgInP	dostat
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
i	i	k8xC	i
vojenské	vojenský	k2eAgFnPc1d1	vojenská
zbraně	zbraň	k1gFnPc1	zbraň
a	a	k8xC	a
situace	situace	k1gFnSc1	situace
dospěla	dochvít	k5eAaPmAgFnS	dochvít
až	až	k9	až
k	k	k7c3	k
otevřenému	otevřený	k2eAgNnSc3d1	otevřené
povstání	povstání	k1gNnSc3	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
odpůrců	odpůrce	k1gMnPc2	odpůrce
režimu	režim	k1gInSc2	režim
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
přidaly	přidat	k5eAaPmAgFnP	přidat
i	i	k8xC	i
části	část	k1gFnPc1	část
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Režim	režim	k1gInSc1	režim
pak	pak	k6eAd1	pak
postupně	postupně	k6eAd1	postupně
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
celý	celý	k2eAgInSc4d1	celý
východ	východ	k1gInSc4	východ
Libye	Libye	k1gFnSc2	Libye
<g/>
,	,	kIx,	,
libyjsko-egyptskou	libyjskogyptský	k2eAgFnSc4d1	libyjsko-egyptský
hranici	hranice	k1gFnSc4	hranice
začali	začít	k5eAaPmAgMnP	začít
hlídat	hlídat	k5eAaImF	hlídat
vzbouřenci	vzbouřenec	k1gMnPc1	vzbouřenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
se	se	k3xPyFc4	se
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
plukovník	plukovník	k1gMnSc1	plukovník
Kaddáfí	Kaddáfí	k1gMnSc1	Kaddáfí
zemi	zem	k1gFnSc4	zem
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
však	však	k9	však
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
vůdcem	vůdce	k1gMnSc7	vůdce
revoluce	revoluce	k1gFnPc1	revoluce
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vede	vést	k5eAaImIp3nS	vést
již	již	k6eAd1	již
déle	dlouho	k6eAd2	dlouho
jak	jak	k8xC	jak
40	[number]	k4	40
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zůstane	zůstat	k5eAaPmIp3nS	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
pohrozil	pohrozit	k5eAaPmAgMnS	pohrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
začnou	začít	k5eAaPmIp3nP	začít
jatka	jatka	k1gFnSc1	jatka
<g/>
.	.	kIx.	.
</s>
<s>
Vyzval	vyzvat	k5eAaPmAgInS	vyzvat
také	také	k9	také
své	svůj	k3xOyFgMnPc4	svůj
příznivce	příznivec	k1gMnPc4	příznivec
k	k	k7c3	k
veřejné	veřejný	k2eAgFnSc3d1	veřejná
podpoře	podpora	k1gFnSc3	podpora
<g/>
,	,	kIx,	,
den	den	k1gInSc4	den
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
však	však	k9	však
sešlo	sejít	k5eAaPmAgNnS	sejít
jen	jen	k9	jen
asi	asi	k9	asi
150	[number]	k4	150
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
západním	západní	k2eAgNnSc6d1	západní
médiím	médium	k1gNnPc3	médium
rozhovor	rozhovor	k1gInSc4	rozhovor
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
rezignaci	rezignace	k1gFnSc3	rezignace
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nezastává	zastávat	k5eNaImIp3nS	zastávat
žádnou	žádný	k3yNgFnSc4	žádný
oficiální	oficiální	k2eAgFnSc4d1	oficiální
funkci	funkce	k1gFnSc4	funkce
a	a	k8xC	a
postěžoval	postěžovat	k5eAaPmAgMnS	postěžovat
si	se	k3xPyFc3	se
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
západní	západní	k2eAgFnPc1d1	západní
země	zem	k1gFnPc1	zem
opustily	opustit	k5eAaPmAgFnP	opustit
a	a	k8xC	a
nerozumí	rozumět	k5eNaImIp3nS	rozumět
politickému	politický	k2eAgInSc3d1	politický
systému	systém	k1gInSc3	systém
Libye	Libye	k1gFnSc2	Libye
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
všichni	všechen	k3xTgMnPc1	všechen
jeho	jeho	k3xOp3gMnPc1	jeho
lidé	člověk	k1gMnPc1	člověk
jej	on	k3xPp3gNnSc4	on
"	"	kIx"	"
<g/>
milují	milovat	k5eAaImIp3nP	milovat
<g/>
"	"	kIx"	"
a	a	k8xC	a
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
by	by	kYmCp3nP	by
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gInSc4	on
ochránili	ochránit	k5eAaPmAgMnP	ochránit
<g/>
.	.	kIx.	.
<g/>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
režim	režim	k1gInSc1	režim
ztratil	ztratit	k5eAaPmAgInS	ztratit
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
východem	východ	k1gInSc7	východ
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mj.	mj.	kA	mj.
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
libyjské	libyjský	k2eAgNnSc4d1	Libyjské
město	město	k1gNnSc4	město
Benghází	Bengháze	k1gFnPc2	Bengháze
<g/>
,	,	kIx,	,
začali	začít	k5eAaPmAgMnP	začít
tamní	tamní	k2eAgMnPc1d1	tamní
lidé	člověk	k1gMnPc1	člověk
oslavovat	oslavovat	k5eAaImF	oslavovat
a	a	k8xC	a
odevzdávat	odevzdávat	k5eAaImF	odevzdávat
ukořistěné	ukořistěný	k2eAgFnPc4d1	ukořistěná
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
oslavách	oslava	k1gFnPc6	oslava
začali	začít	k5eAaPmAgMnP	začít
používat	používat	k5eAaImF	používat
červeno-zeleno-černé	červenoeleno-černý	k2eAgFnPc4d1	červeno-zeleno-černý
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
,	,	kIx,	,
připomínku	připomínka	k1gFnSc4	připomínka
období	období	k1gNnSc2	období
před	před	k7c7	před
Kaddáfím	Kaddáfí	k1gNnSc7	Kaddáfí
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
jen	jen	k9	jen
několik	několik	k4yIc1	několik
měst	město	k1gNnPc2	město
na	na	k7c6	na
západě	západ	k1gInSc6	západ
země	zem	k1gFnSc2	zem
a	a	k8xC	a
především	především	k6eAd1	především
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Tripolis	Tripolis	k1gInSc1	Tripolis
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yIgMnPc4	který
média	médium	k1gNnPc1	médium
předpokládaly	předpokládat	k5eAaImAgFnP	předpokládat
nejtěžší	těžký	k2eAgInPc4d3	nejtěžší
boje	boj	k1gInPc4	boj
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Kaddáfí	Kaddáfí	k1gMnSc1	Kaddáfí
se	se	k3xPyFc4	se
nemínil	mínit	k5eNaImAgMnS	mínit
vzdát	vzdát	k5eAaPmF	vzdát
a	a	k8xC	a
opřít	opřít	k5eAaPmF	opřít
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
minimálně	minimálně	k6eAd1	minimálně
o	o	k7c4	o
svou	svůj	k3xOyFgFnSc4	svůj
elitní	elitní	k2eAgFnSc4d1	elitní
prezidentskou	prezidentský	k2eAgFnSc4d1	prezidentská
gardu	garda	k1gFnSc4	garda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
tvořilo	tvořit	k5eAaImAgNnS	tvořit
3	[number]	k4	3
000	[number]	k4	000
dobře	dobře	k6eAd1	dobře
vyzbrojených	vyzbrojený	k2eAgMnPc2d1	vyzbrojený
a	a	k8xC	a
vycvičených	vycvičený	k2eAgMnPc2d1	vycvičený
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zpráv	zpráva	k1gFnPc2	zpráva
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
tanky	tank	k1gInPc1	tank
a	a	k8xC	a
žoldáci	žoldák	k1gMnPc1	žoldák
z	z	k7c2	z
cizích	cizí	k2eAgFnPc2d1	cizí
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
Kaddáfí	Kaddáfí	k1gFnPc4	Kaddáfí
najal	najmout	k5eAaPmAgMnS	najmout
a	a	k8xC	a
kteří	který	k3yQgMnPc1	který
stříleli	střílet	k5eAaImAgMnP	střílet
i	i	k9	i
po	po	k7c6	po
těch	ten	k3xDgMnPc6	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
pomoci	pomoct	k5eAaPmF	pomoct
zraněným	zraněný	k2eAgMnSc7d1	zraněný
<g/>
.	.	kIx.	.
<g/>
Jako	jako	k8xS	jako
vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
složka	složka	k1gFnSc1	složka
povstání	povstání	k1gNnSc2	povstání
se	se	k3xPyFc4	se
zformovala	zformovat	k5eAaPmAgFnS	zformovat
Dočasná	dočasný	k2eAgFnSc1d1	dočasná
národní	národní	k2eAgFnSc1d1	národní
přechodná	přechodný	k2eAgFnSc1d1	přechodná
rada	rada	k1gFnSc1	rada
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
<g/>
:	:	kIx,	:
ا	ا	k?	ا
ا	ا	k?	ا
al-Jumhū	al-Jumhū	k?	al-Jumhū
al-Lī	al-Lī	k?	al-Lī
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xS	jako
Národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rada	rada	k1gFnSc1	rada
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
svoje	svůj	k3xOyFgNnPc4	svůj
ustavující	ustavující	k2eAgNnPc4d1	ustavující
setkání	setkání	k1gNnPc4	setkání
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
v	v	k7c6	v
Benghází	Bengháze	k1gFnPc2	Bengháze
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
také	také	k9	také
označila	označit	k5eAaPmAgFnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
jediného	jediný	k2eAgMnSc4d1	jediný
představitele	představitel	k1gMnSc4	představitel
celé	celý	k2eAgFnSc2d1	celá
Libye	Libye	k1gFnSc2	Libye
<g/>
"	"	kIx"	"
a	a	k8xC	a
zemi	zem	k1gFnSc4	zem
začala	začít	k5eAaPmAgFnS	začít
označovat	označovat	k5eAaImF	označovat
pojmenováním	pojmenování	k1gNnSc7	pojmenování
Libyjská	libyjský	k2eAgFnSc1d1	Libyjská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
Radu	Rada	k1gMnSc4	Rada
oficiálně	oficiálně	k6eAd1	oficiálně
uznala	uznat	k5eAaPmAgFnS	uznat
jako	jako	k9	jako
představitele	představitel	k1gMnSc4	představitel
libyjského	libyjský	k2eAgInSc2d1	libyjský
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Francie	Francie	k1gFnSc1	Francie
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
jako	jako	k8xS	jako
první	první	k4xOgInSc1	první
uznal	uznat	k5eAaPmAgInS	uznat
rebely	rebel	k1gMnPc4	rebel
ustavenou	ustavený	k2eAgFnSc4d1	ustavená
vládu	vláda	k1gFnSc4	vláda
Katar	katar	k1gMnSc1	katar
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
také	také	k9	také
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
z	z	k7c2	z
arabských	arabský	k2eAgFnPc2d1	arabská
zemí	zem	k1gFnPc2	zem
vojensky	vojensky	k6eAd1	vojensky
podpořil	podpořit	k5eAaPmAgMnS	podpořit
vytvoření	vytvoření	k1gNnSc4	vytvoření
bezletové	bezletový	k2eAgFnSc2d1	bezletová
zóny	zóna	k1gFnSc2	zóna
nad	nad	k7c7	nad
Libyí	Libye	k1gFnSc7	Libye
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reakce	reakce	k1gFnPc1	reakce
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
==	==	k?	==
</s>
</p>
<p>
<s>
Ibrahim	Ibrahim	k6eAd1	Ibrahim
Dabáší	Dabáší	k2eAgFnSc1d1	Dabáší
<g/>
,	,	kIx,	,
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
Libye	Libye	k1gFnSc2	Libye
při	při	k7c6	při
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
televizi	televize	k1gFnSc4	televize
Al-Džazíra	Al-Džazír	k1gMnSc2	Al-Džazír
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
genocidu	genocida	k1gFnSc4	genocida
<g/>
"	"	kIx"	"
a	a	k8xC	a
že	že	k8xS	že
"	"	kIx"	"
<g/>
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
konec	konec	k1gInSc1	konec
režimu	režim	k1gInSc2	režim
Muammara	Muammar	k1gMnSc2	Muammar
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vlivný	vlivný	k2eAgInSc1d1	vlivný
egyptský	egyptský	k2eAgInSc1d1	egyptský
duchovní	duchovní	k2eAgInSc1d1	duchovní
Jusúf	Jusúf	k1gInSc1	Jusúf
al-Karadáví	al-Karadáví	k1gNnSc2	al-Karadáví
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
Muammara	Muammar	k1gMnSc4	Muammar
Kaddáfího	Kaddáfí	k1gMnSc4	Kaddáfí
fatvu	fatva	k1gFnSc4	fatva
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
fyzické	fyzický	k2eAgFnSc3d1	fyzická
likvidaci	likvidace	k1gFnSc3	likvidace
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
povstalců	povstalec	k1gMnPc2	povstalec
se	se	k3xPyFc4	se
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
povstání	povstání	k1gNnSc2	povstání
přidala	přidat	k5eAaPmAgFnS	přidat
řada	řada	k1gFnSc1	řada
členů	člen	k1gMnPc2	člen
diplomatické	diplomatický	k2eAgFnSc2d1	diplomatická
reprezentace	reprezentace	k1gFnSc2	reprezentace
Libye	Libye	k1gFnSc2	Libye
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Libyjské	libyjský	k2eAgFnSc2d1	Libyjská
mise	mise	k1gFnSc2	mise
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Tripolisem	Tripolis	k1gInSc7	Tripolis
rovněž	rovněž	k9	rovněž
přerušila	přerušit	k5eAaPmAgFnS	přerušit
velvyslanectví	velvyslanectví	k1gNnSc4	velvyslanectví
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc4	Austrálie
<g/>
,	,	kIx,	,
Indii	Indie	k1gFnSc4	Indie
<g/>
,	,	kIx,	,
Malajsii	Malajsie	k1gFnSc4	Malajsie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
velvyslanectvím	velvyslanectví	k1gNnSc7	velvyslanectví
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
připojili	připojit	k5eAaPmAgMnP	připojit
vyslanci	vyslanec	k1gMnPc1	vyslanec
k	k	k7c3	k
Lize	liga	k1gFnSc3	liga
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
zemí	zem	k1gFnPc2	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
násilí	násilí	k1gNnSc2	násilí
odsoudila	odsoudit	k5eAaPmAgFnS	odsoudit
a	a	k8xC	a
volala	volat	k5eAaImAgFnS	volat
po	po	k7c6	po
dialogu	dialog	k1gInSc6	dialog
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
deníku	deník	k1gInSc2	deník
EUobserver	EUobservra	k1gFnPc2	EUobservra
jsou	být	k5eAaImIp3nP	být
výjimkou	výjimka	k1gFnSc7	výjimka
pouze	pouze	k6eAd1	pouze
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
Česko	Česko	k1gNnSc1	Česko
<g/>
:	:	kIx,	:
článek	článek	k1gInSc1	článek
cituje	citovat	k5eAaBmIp3nS	citovat
Silvia	Silvia	k1gFnSc1	Silvia
Berlusconiho	Berlusconi	k1gMnSc2	Berlusconi
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
novináři	novinář	k1gMnPc7	novinář
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
nechce	chtít	k5eNaImIp3nS	chtít
nikoho	nikdo	k3yNnSc4	nikdo
rušit	rušit	k5eAaImF	rušit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Karel	Karel	k1gMnSc1	Karel
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
podle	podle	k7c2	podle
článku	článek	k1gInSc2	článek
mluvil	mluvit	k5eAaImAgMnS	mluvit
o	o	k7c4	o
možnosti	možnost	k1gFnPc4	možnost
pádu	pád	k1gInSc2	pád
Kaddáfího	Kaddáfí	k2eAgInSc2d1	Kaddáfí
jako	jako	k8xS	jako
o	o	k7c6	o
katastrofě	katastrofa	k1gFnSc6	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
Ministr	ministr	k1gMnSc1	ministr
však	však	k9	však
později	pozdě	k6eAd2	pozdě
tato	tento	k3xDgNnPc4	tento
slova	slovo	k1gNnPc4	slovo
popřel	popřít	k5eAaPmAgMnS	popřít
a	a	k8xC	a
označil	označit	k5eAaPmAgMnS	označit
citaci	citace	k1gFnSc4	citace
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
blbost	blbost	k1gFnSc1	blbost
<g/>
"	"	kIx"	"
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
slovy	slovo	k1gNnPc7	slovo
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
EU	EU	kA	EU
neměla	mít	k5eNaImAgFnS	mít
do	do	k7c2	do
situace	situace	k1gFnSc2	situace
příliš	příliš	k6eAd1	příliš
vměšovat	vměšovat	k5eAaImF	vměšovat
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
země	zem	k1gFnPc1	zem
EU	EU	kA	EU
začaly	začít	k5eAaPmAgFnP	začít
z	z	k7c2	z
Libye	Libye	k1gFnSc2	Libye
evakuovat	evakuovat	k5eAaBmF	evakuovat
své	svůj	k3xOyFgMnPc4	svůj
občany	občan	k1gMnPc4	občan
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
nakonec	nakonec	k6eAd1	nakonec
vyzvala	vyzvat	k5eAaPmAgFnS	vyzvat
všechny	všechen	k3xTgMnPc4	všechen
své	svůj	k3xOyFgMnPc4	svůj
občany	občan	k1gMnPc4	občan
k	k	k7c3	k
opuštění	opuštění	k1gNnSc3	opuštění
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
své	svůj	k3xOyFgNnSc4	svůj
velvyslanectví	velvyslanectví	k1gNnSc4	velvyslanectví
a	a	k8xC	a
evakuovala	evakuovat	k5eAaBmAgFnS	evakuovat
jeho	jeho	k3xOp3gMnPc4	jeho
pracovníky	pracovník	k1gMnPc4	pracovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
jednala	jednat	k5eAaImAgFnS	jednat
o	o	k7c6	o
sankcích	sankce	k1gFnPc6	sankce
proti	proti	k7c3	proti
Libyi	Libye	k1gFnSc3	Libye
a	a	k8xC	a
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
o	o	k7c4	o
zablokování	zablokování	k1gNnSc4	zablokování
Kaddáfího	Kaddáfí	k2eAgInSc2d1	Kaddáfí
účtů	účet	k1gInPc2	účet
<g/>
,	,	kIx,	,
o	o	k7c6	o
úplném	úplný	k2eAgNnSc6d1	úplné
zbrojním	zbrojní	k2eAgNnSc6d1	zbrojní
embargu	embargo	k1gNnSc6	embargo
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
trestní	trestní	k2eAgInSc1d1	trestní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Haagu	Haag	k1gInSc6	Haag
bude	být	k5eAaImBp3nS	být
vyšetřovat	vyšetřovat	k5eAaImF	vyšetřovat
Kaddáfího	Kaddáfí	k1gMnSc4	Kaddáfí
masakry	masakr	k1gInPc1	masakr
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
již	již	k9	již
také	také	k9	také
uvalily	uvalit	k5eAaPmAgFnP	uvalit
ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
sankce	sankce	k1gFnPc1	sankce
a	a	k8xC	a
nevyloučily	vyloučit	k5eNaPmAgFnP	vyloučit
ani	ani	k9	ani
použití	použití	k1gNnSc2	použití
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgMnSc3	ten
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
zatím	zatím	k6eAd1	zatím
o	o	k7c6	o
sankcích	sankce	k1gFnPc6	sankce
jedná	jednat	k5eAaImIp3nS	jednat
a	a	k8xC	a
vojensky	vojensky	k6eAd1	vojensky
zakročit	zakročit	k5eAaPmF	zakročit
neplánuje	plánovat	k5eNaImIp3nS	plánovat
<g/>
.	.	kIx.	.
</s>
<s>
Ruský	ruský	k2eAgMnSc1d1	ruský
prezident	prezident	k1gMnSc1	prezident
Medvěděv	Medvěděv	k1gMnSc1	Medvěděv
pak	pak	k6eAd1	pak
označil	označit	k5eAaPmAgMnS	označit
použití	použití	k1gNnSc4	použití
vojenské	vojenský	k2eAgFnSc2d1	vojenská
síly	síla	k1gFnSc2	síla
proti	proti	k7c3	proti
vlastnímu	vlastní	k2eAgInSc3d1	vlastní
národu	národ	k1gInSc3	národ
za	za	k7c4	za
nepřijatelné	přijatelný	k2eNgFnPc4d1	nepřijatelná
a	a	k8xC	a
označil	označit	k5eAaPmAgMnS	označit
Kaddáfího	Kaddáfí	k1gMnSc4	Kaddáfí
za	za	k7c4	za
politickou	politický	k2eAgFnSc4d1	politická
mrtvolu	mrtvola	k1gFnSc4	mrtvola
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
musí	muset	k5eAaImIp3nS	muset
odejít	odejít	k5eAaPmF	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
by	by	kYmCp3nS	by
však	však	k9	však
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
vojenský	vojenský	k2eAgInSc4d1	vojenský
zásah	zásah	k1gInSc4	zásah
zemí	zem	k1gFnPc2	zem
NATO	nato	k6eAd1	nato
považovalo	považovat	k5eAaImAgNnS	považovat
za	za	k7c4	za
porušení	porušení	k1gNnSc4	porušení
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgInSc1d1	další
vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
Radou	Rada	k1gMnSc7	Rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
poměrem	poměr	k1gInSc7	poměr
hlasů	hlas	k1gInPc2	hlas
10	[number]	k4	10
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
(	(	kIx(	(
<g/>
při	při	k7c6	při
absenci	absence	k1gFnSc6	absence
pěti	pět	k4xCc2	pět
států	stát	k1gInPc2	stát
–	–	k?	–
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc2	Indie
–	–	k?	–
a	a	k8xC	a
dvou	dva	k4xCgMnPc6	dva
ze	z	k7c2	z
stálých	stálý	k2eAgMnPc2d1	stálý
členů	člen	k1gMnPc2	člen
RB	RB	kA	RB
<g/>
,	,	kIx,	,
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
Číny	Čína	k1gFnSc2	Čína
<g/>
)	)	kIx)	)
schválena	schválen	k2eAgFnSc1d1	schválena
rezoluce	rezoluce	k1gFnSc1	rezoluce
RB	RB	kA	RB
OSN	OSN	kA	OSN
č.	č.	k?	č.
1973	[number]	k4	1973
<g />
.	.	kIx.	.
</s>
<s>
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
zakazující	zakazující	k2eAgMnSc1d1	zakazující
všechny	všechen	k3xTgInPc4	všechen
lety	let	k1gInPc4	let
ve	v	k7c6	v
vzdušném	vzdušný	k2eAgInSc6d1	vzdušný
prostoru	prostor	k1gInSc6	prostor
Libye	Libye	k1gFnSc2	Libye
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
humanitárních	humanitární	k2eAgInPc2d1	humanitární
<g/>
,	,	kIx,	,
a	a	k8xC	a
povolující	povolující	k2eAgFnSc1d1	povolující
členským	členský	k2eAgInPc3d1	členský
státům	stát	k1gInPc3	stát
použití	použití	k1gNnSc2	použití
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
okupace	okupace	k1gFnPc4	okupace
<g/>
,	,	kIx,	,
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
životů	život	k1gInPc2	život
civilního	civilní	k2eAgNnSc2d1	civilní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
a	a	k8xC	a
obydlených	obydlený	k2eAgNnPc2d1	obydlené
území	území	k1gNnPc2	území
Libyjské	libyjský	k2eAgFnSc2d1	Libyjská
arabské	arabský	k2eAgFnSc2d1	arabská
džamáhíríje	džamáhíríj	k1gFnSc2	džamáhíríj
před	před	k7c7	před
je	být	k5eAaImIp3nS	být
ohrožujícími	ohrožující	k2eAgInPc7d1	ohrožující
útoky	útok	k1gInPc7	útok
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
porovnání	porovnání	k1gNnSc6	porovnání
průběhu	průběh	k1gInSc2	průběh
aliančního	alianční	k2eAgNnSc2d1	alianční
vojenského	vojenský	k2eAgNnSc2d1	vojenské
angažmá	angažmá	k1gNnSc2	angažmá
s	s	k7c7	s
mandátem	mandát	k1gInSc7	mandát
RB	RB	kA	RB
OSN	OSN	kA	OSN
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
NATO	NATO	kA	NATO
nemělo	mít	k5eNaImAgNnS	mít
mandát	mandát	k1gInSc4	mandát
/	/	kIx~	/
<g/>
Aliance	aliance	k1gFnSc1	aliance
není	být	k5eNaImIp3nS	být
regionální	regionální	k2eAgFnSc7d1	regionální
organizací	organizace	k1gFnSc7	organizace
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
Charty	charta	k1gFnSc2	charta
OSN	OSN	kA	OSN
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
NATO	nato	k6eAd1	nato
svojí	svůj	k3xOyFgFnSc7	svůj
politikou	politika	k1gFnSc7	politika
sledovalo	sledovat	k5eAaImAgNnS	sledovat
změnu	změna	k1gFnSc4	změna
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
rezoluce	rezoluce	k1gFnSc1	rezoluce
RB	RB	kA	RB
nedávají	dávat	k5eNaImIp3nP	dávat
žádné	žádný	k3yNgNnSc4	žádný
zmocnění	zmocnění	k1gNnSc4	zmocnění
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
porušení	porušení	k1gNnSc3	porušení
rezolucí	rezoluce	k1gFnPc2	rezoluce
RB	RB	kA	RB
OSN	OSN	kA	OSN
dodávkami	dodávka	k1gFnPc7	dodávka
zbraní	zbraň	k1gFnSc7	zbraň
a	a	k8xC	a
poskytnutím	poskytnutí	k1gNnSc7	poskytnutí
vojenského	vojenský	k2eAgInSc2d1	vojenský
výcviku	výcvik	k1gInSc2	výcvik
povstalcům	povstalec	k1gMnPc3	povstalec
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
nebyl	být	k5eNaImAgInS	být
dodržen	dodržen	k2eAgInSc1d1	dodržen
princip	princip	k1gInSc1	princip
nestrannosti	nestrannost	k1gFnSc2	nestrannost
při	při	k7c6	při
vynucování	vynucování	k1gNnSc6	vynucování
přijaté	přijatý	k2eAgFnSc2d1	přijatá
rezoluce	rezoluce	k1gFnSc2	rezoluce
RB	RB	kA	RB
<g/>
.	.	kIx.	.
</s>
<s>
Ochrana	ochrana	k1gFnSc1	ochrana
civilistů	civilista	k1gMnPc2	civilista
byla	být	k5eAaImAgFnS	být
zajišťována	zajišťovat	k5eAaImNgFnS	zajišťovat
systematickým	systematický	k2eAgNnSc7d1	systematické
ničením	ničení	k1gNnSc7	ničení
ozbrojených	ozbrojený	k2eAgInPc2d1	ozbrojený
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznávané	uznávaný	k2eAgFnSc2d1	uznávaná
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Případ	případ	k1gInSc1	případ
Libye	Libye	k1gFnSc2	Libye
je	být	k5eAaImIp3nS	být
posuzován	posuzovat	k5eAaImNgInS	posuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
nelegální	legální	k2eNgFnSc4d1	nelegální
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
intervenci	intervence	k1gFnSc4	intervence
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
výrazné	výrazný	k2eAgNnSc4d1	výrazné
překročení	překročení	k1gNnSc4	překročení
mandátu	mandát	k1gInSc2	mandát
RB	RB	kA	RB
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Aliančnímu	alianční	k2eAgInSc3d1	alianční
zásahu	zásah	k1gInSc3	zásah
dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
předcházely	předcházet	k5eAaImAgInP	předcházet
zásahy	zásah	k1gInPc1	zásah
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
zahájila	zahájit	k5eAaPmAgFnS	zahájit
letecké	letecký	k2eAgInPc4d1	letecký
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
vládní	vládní	k2eAgFnPc4d1	vládní
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
v	v	k7c6	v
součinnosti	součinnost	k1gFnSc6	součinnost
se	s	k7c7	s
spojenci	spojenec	k1gMnPc7	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
součinnost	součinnost	k1gFnSc1	součinnost
měla	mít	k5eAaImAgFnS	mít
podobu	podoba	k1gFnSc4	podoba
neformálního	formální	k2eNgInSc2d1	neformální
rozhovoru	rozhovor	k1gInSc2	rozhovor
francouzského	francouzský	k2eAgMnSc4d1	francouzský
prezidenta	prezident	k1gMnSc4	prezident
Nicolase	Nicolasa	k1gFnSc3	Nicolasa
Sarkozyho	Sarkozy	k1gMnSc4	Sarkozy
s	s	k7c7	s
americkou	americký	k2eAgFnSc7d1	americká
ministryní	ministryně	k1gFnSc7	ministryně
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
Hillary	Hillara	k1gFnSc2	Hillara
Clintonovou	Clintonová	k1gFnSc7	Clintonová
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
zahájila	zahájit	k5eAaPmAgFnS	zahájit
letecké	letecký	k2eAgInPc4d1	letecký
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
vládní	vládní	k2eAgFnPc4d1	vládní
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
prý	prý	k9	prý
v	v	k7c6	v
koordinaci	koordinace	k1gFnSc6	koordinace
s	s	k7c7	s
britským	britský	k2eAgMnSc7d1	britský
premiérem	premiér	k1gMnSc7	premiér
Davidem	David	k1gMnSc7	David
Cameronem	Cameron	k1gMnSc7	Cameron
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yRgNnSc1	který
jim	on	k3xPp3gMnPc3	on
bylo	být	k5eAaImAgNnS	být
dáno	dát	k5eAaPmNgNnS	dát
na	na	k7c4	na
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
,	,	kIx,	,
že	že	k8xS	že
francouzská	francouzský	k2eAgFnSc1d1	francouzská
vojenská	vojenský	k2eAgFnSc1d1	vojenská
akce	akce	k1gFnSc1	akce
již	již	k6eAd1	již
začala	začít	k5eAaPmAgFnS	začít
<g/>
.	.	kIx.	.
</s>
<s>
Okolnosti	okolnost	k1gFnPc1	okolnost
nasvědčují	nasvědčovat	k5eAaImIp3nP	nasvědčovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Francie	Francie	k1gFnSc2	Francie
své	svůj	k3xOyFgMnPc4	svůj
spojence	spojenec	k1gMnPc4	spojenec
překvapila	překvapit	k5eAaPmAgFnS	překvapit
a	a	k8xC	a
postavila	postavit	k5eAaPmAgFnS	postavit
je	on	k3xPp3gMnPc4	on
před	před	k7c4	před
hotovou	hotový	k2eAgFnSc4d1	hotová
věc	věc	k1gFnSc4	věc
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
být	být	k5eAaImF	být
vůdčí	vůdčí	k2eAgFnSc7d1	vůdčí
zemí	zem	k1gFnSc7	zem
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
událostech	událost	k1gFnPc6	událost
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
si	se	k3xPyFc3	se
svoji	svůj	k3xOyFgFnSc4	svůj
politiku	politika	k1gFnSc4	politika
Francie	Francie	k1gFnSc2	Francie
nechala	nechat	k5eAaPmAgNnP	nechat
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
posvětit	posvětit	k5eAaPmF	posvětit
lídry	lídr	k1gMnPc4	lídr
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
postoj	postoj	k1gInSc1	postoj
ke	k	k7c3	k
krizi	krize	k1gFnSc3	krize
se	se	k3xPyFc4	se
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
pozvolna	pozvolna	k6eAd1	pozvolna
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
Barack	Barack	k1gMnSc1	Barack
Obama	Obama	k?	Obama
už	už	k6eAd1	už
od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
zásadu	zásada	k1gFnSc4	zásada
<g/>
,	,	kIx,	,
že	že	k8xS	že
Muammar	Muammar	k1gInSc1	Muammar
Kaddáffí	Kaddáff	k1gFnPc2	Kaddáff
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
vzdát	vzdát	k5eAaPmF	vzdát
moci	moct	k5eAaImF	moct
a	a	k8xC	a
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
uvalily	uvalit	k5eAaPmAgFnP	uvalit
na	na	k7c6	na
Libyi	Libye	k1gFnSc6	Libye
sankce	sankce	k1gFnPc4	sankce
a	a	k8xC	a
pracovaly	pracovat	k5eAaImAgFnP	pracovat
na	na	k7c4	na
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
koalici	koalice	k1gFnSc4	koalice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
krizi	krize	k1gFnSc3	krize
řešila	řešit	k5eAaImAgFnS	řešit
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
NATO	nato	k6eAd1	nato
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Aliance	aliance	k1gFnSc1	aliance
jako	jako	k8xC	jako
organizace	organizace	k1gFnSc1	organizace
zúčastní	zúčastnit	k5eAaPmIp3nS	zúčastnit
vojenské	vojenský	k2eAgFnSc2d1	vojenská
intervence	intervence	k1gFnSc2	intervence
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počáteční	počáteční	k2eAgFnSc6d1	počáteční
fázi	fáze	k1gFnSc6	fáze
se	se	k3xPyFc4	se
Aliance	aliance	k1gFnSc1	aliance
soustředila	soustředit	k5eAaPmAgFnS	soustředit
na	na	k7c6	na
evakuaci	evakuace	k1gFnSc6	evakuace
cizích	cizí	k2eAgMnPc2d1	cizí
státních	státní	k2eAgMnPc2d1	státní
příslušníků	příslušník	k1gMnPc2	příslušník
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
a	a	k8xC	a
poskytnutí	poskytnutí	k1gNnSc2	poskytnutí
humanitární	humanitární	k2eAgFnSc2d1	humanitární
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
února	únor	k1gInSc2	únor
a	a	k8xC	a
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
síly	síla	k1gFnPc1	síla
věrné	věrný	k2eAgFnPc1d1	věrná
Kaddáffímu	Kaddáffí	k1gMnSc3	Kaddáffí
začaly	začít	k5eAaPmAgInP	začít
mít	mít	k5eAaImF	mít
nad	nad	k7c7	nad
povstáním	povstání	k1gNnSc7	povstání
převahu	převaha	k1gFnSc4	převaha
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
politika	politika	k1gFnSc1	politika
NATO	nato	k6eAd1	nato
změnila	změnit	k5eAaPmAgFnS	změnit
a	a	k8xC	a
Aliance	aliance	k1gFnSc1	aliance
začala	začít	k5eAaPmAgFnS	začít
zdůrazňovat	zdůrazňovat	k5eAaImF	zdůrazňovat
potřebu	potřeba	k1gFnSc4	potřeba
vojenského	vojenský	k2eAgInSc2d1	vojenský
zásahu	zásah	k1gInSc2	zásah
V	v	k7c6	v
odpoledních	odpolední	k2eAgFnPc6d1	odpolední
hodinách	hodina	k1gFnPc6	hodina
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
Muammar	Muammar	k1gInSc1	Muammar
Kaddáfí	Kaddáfí	k2eAgInSc1d1	Kaddáfí
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
rezoluce	rezoluce	k1gFnSc2	rezoluce
okamžité	okamžitý	k2eAgNnSc4d1	okamžité
příměří	příměří	k1gNnSc4	příměří
a	a	k8xC	a
zastavení	zastavení	k1gNnSc4	zastavení
vojenských	vojenský	k2eAgFnPc2d1	vojenská
operací	operace	k1gFnPc2	operace
na	na	k7c6	na
území	území	k1gNnSc6	území
Libye	Libye	k1gFnSc2	Libye
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
toto	tento	k3xDgNnSc4	tento
prohlášení	prohlášení	k1gNnSc4	prohlášení
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
dělostřelecké	dělostřelecký	k2eAgInPc1d1	dělostřelecký
a	a	k8xC	a
letecké	letecký	k2eAgInPc4d1	letecký
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
Benghází	Bengháze	k1gFnSc7	Bengháze
<g/>
,	,	kIx,	,
a	a	k8xC	a
svědci	svědek	k1gMnPc1	svědek
hlásili	hlásit	k5eAaImAgMnP	hlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
pozorován	pozorovat	k5eAaImNgInS	pozorovat
vstup	vstup	k1gInSc4	vstup
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
pozemních	pozemní	k2eAgFnPc2d1	pozemní
jednotek	jednotka	k1gFnPc2	jednotka
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
vláda	vláda	k1gFnSc1	vláda
popřela	popřít	k5eAaPmAgFnS	popřít
pokračování	pokračování	k1gNnSc4	pokračování
bojových	bojový	k2eAgFnPc2d1	bojová
akcí	akce	k1gFnPc2	akce
a	a	k8xC	a
útoky	útok	k1gInPc4	útok
připsala	připsat	k5eAaPmAgFnS	připsat
povstalcům	povstalec	k1gMnPc3	povstalec
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
vyprovokovat	vyprovokovat	k5eAaPmF	vyprovokovat
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
intervenci	intervence	k1gFnSc4	intervence
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
-	-	kIx~	-
Nálety	nálet	k1gInPc4	nálet
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
koalice	koalice	k1gFnSc2	koalice
donutily	donutit	k5eAaPmAgFnP	donutit
vojska	vojsko	k1gNnPc4	vojsko
Muammara	Muammar	k1gMnSc2	Muammar
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
se	se	k3xPyFc4	se
od	od	k7c2	od
Bengází	Bengáze	k1gFnPc2	Bengáze
stáhnout	stáhnout	k5eAaPmF	stáhnout
k	k	k7c3	k
městu	město	k1gNnSc3	město
Adždabíja	Adždabíjum	k1gNnSc2	Adždabíjum
<g/>
,	,	kIx,	,
vzdáleného	vzdálený	k2eAgNnSc2d1	vzdálené
deset	deset	k4xCc4	deset
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
Bengází	Bengáze	k1gFnPc2	Bengáze
<g/>
.30	.30	k4	.30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
-	-	kIx~	-
Ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Músá	Músé	k1gNnPc4	Músé
Kúsa	Kúsa	k1gMnSc1	Kúsa
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
z	z	k7c2	z
Libye	Libye	k1gFnSc2	Libye
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Mluvčí	mluvčí	k1gMnSc1	mluvčí
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
vlády	vláda	k1gFnSc2	vláda
jeho	jeho	k3xOp3gFnSc4	jeho
dezerci	dezerce	k1gFnSc4	dezerce
zprvu	zprvu	k6eAd1	zprvu
popíral	popírat	k5eAaImAgMnS	popírat
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ministr	ministr	k1gMnSc1	ministr
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
tiskové	tiskový	k2eAgFnSc2d1	tisková
konference	konference	k1gFnSc2	konference
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
uvedl	uvést	k5eAaPmAgMnS	uvést
jako	jako	k8xC	jako
důvod	důvod	k1gInSc4	důvod
svého	svůj	k3xOyFgInSc2	svůj
odchodu	odchod	k1gInSc2	odchod
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
varoval	varovat	k5eAaImAgMnS	varovat
před	před	k7c7	před
následky	následek	k1gInPc7	následek
dlouhodobé	dlouhodobý	k2eAgFnSc2d1	dlouhodobá
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
a	a	k8xC	a
volal	volat	k5eAaImAgMnS	volat
po	po	k7c6	po
dohodě	dohoda	k1gFnSc6	dohoda
zúčastněných	zúčastněný	k2eAgFnPc2d1	zúčastněná
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
<g/>
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
-	-	kIx~	-
Vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
Ruský	ruský	k2eAgMnSc1d1	ruský
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Sergej	Sergej	k1gMnSc1	Sergej
Lavrov	Lavrov	k1gInSc4	Lavrov
obavy	obava	k1gFnSc2	obava
z	z	k7c2	z
účasti	účast	k1gFnSc2	účast
islamistů	islamista	k1gMnPc2	islamista
na	na	k7c6	na
povstání	povstání	k1gNnSc6	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
vrchního	vrchní	k2eAgMnSc2d1	vrchní
velitele	velitel	k1gMnSc2	velitel
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
sil	síla	k1gFnPc2	síla
NATO	NATO	kA	NATO
Jamese	Jamese	k1gFnSc2	Jamese
Stavridise	Stavridise	k1gFnSc2	Stavridise
tajné	tajný	k2eAgFnSc2d1	tajná
služby	služba	k1gFnSc2	služba
"	"	kIx"	"
<g/>
zaregistrovaly	zaregistrovat	k5eAaPmAgFnP	zaregistrovat
stopy	stopa	k1gFnPc4	stopa
Al-Káidy	Al-Káida	k1gFnPc4	Al-Káida
a	a	k8xC	a
Hizbaláhu	Hizbaláha	k1gFnSc4	Hizbaláha
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Velvyslankyně	velvyslankyně	k1gFnSc1	velvyslankyně
USA	USA	kA	USA
při	při	k7c6	při
OSN	OSN	kA	OSN
Susan	Susana	k1gFnPc2	Susana
Rice	Rice	k1gFnSc1	Rice
možnost	možnost	k1gFnSc1	možnost
infiltrace	infiltrace	k1gFnSc1	infiltrace
extrémistů	extrémista	k1gMnPc2	extrémista
nevyloučila	vyloučit	k5eNaPmAgFnS	vyloučit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
dle	dle	k7c2	dle
jejího	její	k3xOp3gNnSc2	její
tvrzení	tvrzení	k1gNnSc2	tvrzení
neexistují	existovat	k5eNaImIp3nP	existovat
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
Stavridisovo	Stavridisův	k2eAgNnSc4d1	Stavridisův
tvrzení	tvrzení	k1gNnSc4	tvrzení
podložit	podložit	k5eAaPmF	podložit
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
-	-	kIx~	-
Skupina	skupina	k1gFnSc1	skupina
států	stát	k1gInPc2	stát
G	G	kA	G
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
shodla	shodnout	k5eAaPmAgFnS	shodnout
na	na	k7c6	na
nutnosti	nutnost	k1gFnSc6	nutnost
odchodu	odchod	k1gInSc2	odchod
Muammara	Muammara	k1gFnSc1	Muammara
<g />
.	.	kIx.	.
</s>
<s>
Kaddáfího	Kaddáfí	k1gMnSc4	Kaddáfí
z	z	k7c2	z
čela	čelo	k1gNnSc2	čelo
Libye	Libye	k1gFnSc2	Libye
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Ruská	ruský	k2eAgFnSc1d1	ruská
federace	federace	k1gFnSc1	federace
má	mít	k5eAaImIp3nS	mít
zastat	zastat	k5eAaPmF	zastat
roli	role	k1gFnSc4	role
prostředníka	prostředník	k1gMnSc2	prostředník
ve	v	k7c4	v
vyjednání	vyjednání	k1gNnSc4	vyjednání
jeho	jeho	k3xOp3gInSc2	jeho
odchodu	odchod	k1gInSc2	odchod
<g/>
.16	.16	k4	.16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
-	-	kIx~	-
Ruský	ruský	k2eAgMnSc1d1	ruský
vyjednavač	vyjednavač	k1gMnSc1	vyjednavač
Michail	Michail	k1gMnSc1	Michail
Margelov	Margelov	k1gInSc4	Margelov
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
dojednat	dojednat	k5eAaPmF	dojednat
odstoupení	odstoupení	k1gNnSc4	odstoupení
Muammara	Muammar	k1gMnSc2	Muammar
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
z	z	k7c2	z
čela	čelo	k1gNnSc2	čelo
Velké	velký	k2eAgFnSc2d1	velká
libyjské	libyjský	k2eAgFnSc2d1	Libyjská
arabské	arabský	k2eAgFnSc2d1	arabská
lidové	lidový	k2eAgFnSc2d1	lidová
socialistické	socialistický	k2eAgFnSc2d1	socialistická
džamáhiríje	džamáhiríj	k1gFnSc2	džamáhiríj
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
Kaddáfí	Kaddáfí	k1gMnSc1	Kaddáfí
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.30	.30	k4	.30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
-	-	kIx~	-
Pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
dezerce	dezerce	k1gFnSc1	dezerce
vojáků	voják	k1gMnPc2	voják
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
vládních	vládní	k2eAgNnPc2d1	vládní
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
generálů	generál	k1gMnPc2	generál
<g/>
,	,	kIx,	,
dva	dva	k4xCgMnPc1	dva
plukovníci	plukovník	k1gMnPc1	plukovník
a	a	k8xC	a
major	major	k1gMnSc1	major
opustili	opustit	k5eAaPmAgMnP	opustit
zemi	zem	k1gFnSc3	zem
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgNnPc7d1	další
120	[number]	k4	120
vojáky	voják	k1gMnPc7	voják
<g/>
.	.	kIx.	.
</s>
<s>
Uprchlí	uprchlý	k2eAgMnPc1d1	uprchlý
vysocí	vysoký	k2eAgMnPc1d1	vysoký
důstojníci	důstojník	k1gMnPc1	důstojník
následně	následně	k6eAd1	následně
během	během	k7c2	během
tiskové	tiskový	k2eAgFnSc2d1	tisková
konference	konference	k1gFnSc2	konference
odsoudili	odsoudit	k5eAaPmAgMnP	odsoudit
zločiny	zločin	k1gInPc4	zločin
režimu	režim	k1gInSc2	režim
<g/>
.28	.28	k4	.28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
-	-	kIx~	-
Byl	být	k5eAaImAgInS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
vrchní	vrchní	k2eAgMnSc1d1	vrchní
vojenský	vojenský	k2eAgMnSc1d1	vojenský
velitel	velitel	k1gMnSc1	velitel
povstalců	povstalec	k1gMnPc2	povstalec
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ministr	ministr	k1gMnSc1	ministr
vnitra	vnitro	k1gNnSc2	vnitro
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Muammara	Muammar	k1gMnSc2	Muammar
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
<g/>
,	,	kIx,	,
Abdal	Abdal	k1gInSc4	Abdal
Fatah	Fataha	k1gFnPc2	Fataha
Júnis	Júnis	k1gFnPc2	Júnis
<g/>
.	.	kIx.	.
<g/>
.14	.14	k4	.14
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
-	-	kIx~	-
Povstalci	povstalec	k1gMnPc1	povstalec
postoupili	postoupit	k5eAaPmAgMnP	postoupit
po	po	k7c6	po
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
bojích	boj	k1gInPc6	boj
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
hor	hora	k1gFnPc2	hora
Nafusa	Nafus	k1gMnSc2	Nafus
do	do	k7c2	do
města	město	k1gNnSc2	město
Zavíja	Zavíjum	k1gNnSc2	Zavíjum
<g/>
,	,	kIx,	,
vzdáleného	vzdálený	k2eAgNnSc2d1	vzdálené
50	[number]	k4	50
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Tripolis	Tripolis	k1gInSc1	Tripolis
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
tak	tak	k6eAd1	tak
přerušili	přerušit	k5eAaPmAgMnP	přerušit
zásobovací	zásobovací	k2eAgFnSc4d1	zásobovací
trasu	trasa	k1gFnSc4	trasa
mezi	mezi	k7c7	mezi
Tripolisem	Tripolis	k1gInSc7	Tripolis
a	a	k8xC	a
Tuniskou	tuniský	k2eAgFnSc7d1	Tuniská
hranicí	hranice	k1gFnSc7	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Mluvčí	mluvčí	k1gMnSc1	mluvčí
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
vlády	vláda	k1gFnSc2	vláda
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
malý	malý	k2eAgInSc4d1	malý
útok	útok	k1gInSc4	útok
jednotky	jednotka	k1gFnSc2	jednotka
o	o	k7c4	o
50	[number]	k4	50
mužích	muž	k1gMnPc6	muž
a	a	k8xC	a
že	že	k8xS	že
loajalistické	loajalistický	k2eAgFnPc1d1	loajalistický
jednotky	jednotka	k1gFnPc1	jednotka
se	se	k3xPyFc4	se
s	s	k7c7	s
problémem	problém	k1gInSc7	problém
dokáží	dokázat	k5eAaPmIp3nP	dokázat
vypořádat	vypořádat	k5eAaPmF	vypořádat
<g/>
.19	.19	k4	.19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
-	-	kIx~	-
Z	z	k7c2	z
Tripolisu	Tripolis	k1gInSc2	Tripolis
uprchl	uprchnout	k5eAaPmAgInS	uprchnout
Abdas	Abdas	k1gInSc1	Abdas
Salám	salám	k1gInSc1	salám
Džallúd	Džallúda	k1gFnPc2	Džallúda
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
na	na	k7c6	na
převratu	převrat	k1gInSc6	převrat
vedeném	vedený	k2eAgInSc6d1	vedený
Muammarem	Muammar	k1gInSc7	Muammar
Kaddáfím	Kaddáfí	k1gMnSc7	Kaddáfí
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
premiér	premiér	k1gMnSc1	premiér
a	a	k8xC	a
několikanásobný	několikanásobný	k2eAgMnSc1d1	několikanásobný
ministr	ministr	k1gMnSc1	ministr
<g/>
.	.	kIx.	.
</s>
<s>
Přešel	přejít	k5eAaPmAgMnS	přejít
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
povstání	povstání	k1gNnSc2	povstání
společně	společně	k6eAd1	společně
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
rodinou	rodina	k1gFnSc7	rodina
<g/>
.20	.20	k4	.20
<g/>
.	.	kIx.	.
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
Za	za	k7c2	za
pokračujících	pokračující	k2eAgInPc2d1	pokračující
bojů	boj	k1gInPc2	boj
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Zavíji	Zavíje	k1gFnSc4	Zavíje
<g/>
,	,	kIx,	,
Zlitanu	Zlitan	k1gInSc2	Zlitan
a	a	k8xC	a
Brigy	briga	k1gFnSc2	briga
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
na	na	k7c4	na
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
prvního	první	k4xOgNnSc2	první
k	k	k7c3	k
otevřeným	otevřený	k2eAgInPc3d1	otevřený
střetům	střet	k1gInPc3	střet
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
Tripolisu	Tripolis	k1gInSc2	Tripolis
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
se	se	k3xPyFc4	se
rozléhala	rozléhat	k5eAaImAgFnS	rozléhat
střelba	střelba	k1gFnSc1	střelba
a	a	k8xC	a
výbuchy	výbuch	k1gInPc1	výbuch
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
mezi	mezi	k7c7	mezi
guerillou	guerilla	k1gFnSc7	guerilla
a	a	k8xC	a
loajalisty	loajalista	k1gMnPc7	loajalista
trvaly	trvat	k5eAaImAgFnP	trvat
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
za	za	k7c2	za
zvuků	zvuk	k1gInPc2	zvuk
střelby	střelba	k1gFnSc2	střelba
vládní	vládní	k2eAgMnSc1d1	vládní
mluvčí	mluvčí	k1gMnSc1	mluvčí
Músa	Músa	k1gMnSc1	Músa
Ibrahím	Ibrahí	k1gMnPc3	Ibrahí
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vládní	vládní	k2eAgFnSc1d1	vládní
strana	strana	k1gFnSc1	strana
s	s	k7c7	s
povstalci	povstalec	k1gMnPc7	povstalec
"	"	kIx"	"
<g/>
vypořádala	vypořádat	k5eAaPmAgFnS	vypořádat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Za	za	k7c2	za
rozbřesku	rozbřesk	k1gInSc2	rozbřesk
však	však	k9	však
boje	boj	k1gInPc1	boj
opět	opět	k6eAd1	opět
propukly	propuknout	k5eAaPmAgInP	propuknout
<g/>
,	,	kIx,	,
probíhaly	probíhat	k5eAaImAgInP	probíhat
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
povstalci	povstalec	k1gMnPc1	povstalec
začali	začít	k5eAaPmAgMnP	začít
postupovat	postupovat	k5eAaImF	postupovat
ze	z	k7c2	z
směru	směr	k1gInSc2	směr
od	od	k7c2	od
Zavíje	Zavíj	k1gInSc2	Zavíj
a	a	k8xC	a
Zlitanu	Zlitan	k1gInSc2	Zlitan
k	k	k7c3	k
Tripolisu	Tripolis	k1gInSc2	Tripolis
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
podpořili	podpořit	k5eAaPmAgMnP	podpořit
povstání	povstání	k1gNnSc4	povstání
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Benghází	Bengháze	k1gFnPc2	Bengháze
pak	pak	k6eAd1	pak
tyto	tento	k3xDgFnPc1	tento
zprávy	zpráva	k1gFnPc1	zpráva
vyústily	vyústit	k5eAaPmAgFnP	vyústit
v	v	k7c4	v
oslavy	oslava	k1gFnPc4	oslava
<g/>
.	.	kIx.	.
<g/>
Mluvčí	mluvčí	k1gMnSc1	mluvčí
NATO	NATO	kA	NATO
k	k	k7c3	k
situaci	situace	k1gFnSc3	situace
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
konfliktu	konflikt	k1gInSc6	konflikt
již	již	k6eAd1	již
neexistují	existovat	k5eNaImIp3nP	existovat
tradiční	tradiční	k2eAgFnPc4d1	tradiční
frontové	frontový	k2eAgFnPc4d1	frontová
linie	linie	k1gFnPc4	linie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
dříve	dříve	k6eAd2	dříve
přítomny	přítomen	k2eAgInPc1d1	přítomen
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nepřehledná	přehledný	k2eNgFnSc1d1	nepřehledná
situace	situace	k1gFnSc1	situace
stěžuje	stěžovat	k5eAaImIp3nS	stěžovat
identifikaci	identifikace	k1gFnSc4	identifikace
cílů	cíl	k1gInPc2	cíl
<g/>
.23	.23	k4	.23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
-	-	kIx~	-
Povstalci	povstalec	k1gMnPc1	povstalec
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Kaddáfího	Kaddáfí	k1gMnSc4	Kaddáfí
komplex	komplex	k1gInSc1	komplex
Báb	Báb	k1gMnPc3	Báb
al-Azízíju	al-Azízíju	k5eAaPmIp1nS	al-Azízíju
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
nalézt	nalézt	k5eAaBmF	nalézt
samotného	samotný	k2eAgMnSc4d1	samotný
Kaddáfího	Kaddáfí	k1gMnSc4	Kaddáfí
<g/>
.24	.24	k4	.24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
-	-	kIx~	-
Povstalci	povstalec	k1gMnPc1	povstalec
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Libye	Libye	k1gFnSc2	Libye
Tripolis	Tripolis	k1gInSc4	Tripolis
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
Tripolisem	Tripolis	k1gInSc7	Tripolis
dobyli	dobýt	k5eAaPmAgMnP	dobýt
strategickou	strategický	k2eAgFnSc4d1	strategická
oblast	oblast	k1gFnSc4	oblast
Brigy	briga	k1gFnSc2	briga
a	a	k8xC	a
přístavu	přístav	k1gInSc2	přístav
Ras	ras	k1gMnSc1	ras
Lanúf	Lanúf	k1gMnSc1	Lanúf
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
důležité	důležitý	k2eAgFnPc4d1	důležitá
ropné	ropný	k2eAgFnPc4d1	ropná
rafinérie	rafinérie	k1gFnPc4	rafinérie
<g/>
.	.	kIx.	.
</s>
<s>
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
pozornost	pozornost	k1gFnSc1	pozornost
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
obrátila	obrátit	k5eAaPmAgFnS	obrátit
ke	k	k7c3	k
zbývajícím	zbývající	k2eAgFnPc3d1	zbývající
oblastem	oblast	k1gFnPc3	oblast
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
kaddáfího	kaddáfí	k1gMnSc2	kaddáfí
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
městům	město	k1gNnPc3	město
Syrta	Syrt	k1gMnSc2	Syrt
a	a	k8xC	a
Baní	baně	k1gFnSc7	baně
Walíd	Walída	k1gFnPc2	Walída
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgNnPc6	dva
městech	město	k1gNnPc6	město
se	se	k3xPyFc4	se
povstalci	povstalec	k1gMnPc1	povstalec
snažili	snažit	k5eAaImAgMnP	snažit
vyjednat	vyjednat	k5eAaPmF	vyjednat
kapitulaci	kapitulace	k1gFnSc4	kapitulace
obránců	obránce	k1gMnPc2	obránce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
oznámili	oznámit	k5eAaPmAgMnP	oznámit
že	že	k8xS	že
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
dohody	dohoda	k1gFnPc4	dohoda
<g/>
.9	.9	k4	.9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
-	-	kIx~	-
Po	po	k7c6	po
neúspěšných	úspěšný	k2eNgNnPc6d1	neúspěšné
vyjednáváních	vyjednávání	k1gNnPc6	vyjednávání
s	s	k7c7	s
obránci	obránce	k1gMnPc7	obránce
města	město	k1gNnSc2	město
Bani	baně	k1gFnSc4	baně
Walid	Walida	k1gFnPc2	Walida
se	se	k3xPyFc4	se
opětovně	opětovně	k6eAd1	opětovně
rozběhly	rozběhnout	k5eAaPmAgInP	rozběhnout
boje	boj	k1gInPc1	boj
<g/>
.	.	kIx.	.
</s>
<s>
Povstalci	povstalec	k1gMnPc1	povstalec
postoupili	postoupit	k5eAaPmAgMnP	postoupit
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
dvou	dva	k4xCgInPc2	dva
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.23	.23	k4	.23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
-	-	kIx~	-
Povstalci	povstalec	k1gMnPc1	povstalec
dobyli	dobýt	k5eAaPmAgMnP	dobýt
město	město	k1gNnSc4	město
Sabhá	Sabhý	k2eAgFnSc1d1	Sabhá
<g/>
.14	.14	k4	.14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
-	-	kIx~	-
v	v	k7c6	v
Tripolisu	Tripolis	k1gInSc6	Tripolis
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
přestřelka	přestřelka	k1gFnSc1	přestřelka
mezi	mezi	k7c7	mezi
jednotkami	jednotka	k1gFnPc7	jednotka
Přechodné	přechodný	k2eAgFnSc2d1	přechodná
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
asi	asi	k9	asi
50	[number]	k4	50
ozbrojenými	ozbrojený	k2eAgFnPc7d1	ozbrojená
stoupenci	stoupenec	k1gMnPc1	stoupenec
svrženého	svržený	k2eAgMnSc2d1	svržený
vůdce	vůdce	k1gMnSc2	vůdce
Muammara	Muammar	k1gMnSc2	Muammar
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Kaddáfí	Kaddáfí	k1gMnSc1	Kaddáfí
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
rebely	rebel	k1gMnPc7	rebel
<g/>
.23	.23	k4	.23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
-	-	kIx~	-
Dočasná	dočasný	k2eAgFnSc1d1	dočasná
národní	národní	k2eAgFnSc1d1	národní
přechodná	přechodný	k2eAgFnSc1d1	přechodná
rada	rada	k1gFnSc1	rada
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
osvobození	osvobození	k1gNnSc4	osvobození
od	od	k7c2	od
Kaddáfího	Kaddáfí	k2eAgInSc2d1	Kaddáfí
režimu	režim	k1gInSc2	režim
</s>
</p>
<p>
<s>
==	==	k?	==
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
intervence	intervence	k1gFnSc1	intervence
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyhlášené	vyhlášený	k2eAgNnSc1d1	vyhlášené
příměří	příměří	k1gNnSc1	příměří
nebylo	být	k5eNaImAgNnS	být
dodržováno	dodržovat	k5eAaImNgNnS	dodržovat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
zahájena	zahájen	k2eAgFnSc1d1	zahájena
intervence	intervence	k1gFnSc1	intervence
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
zejména	zejména	k9	zejména
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
britských	britský	k2eAgInPc2d1	britský
a	a	k8xC	a
francouzských	francouzský	k2eAgFnPc2d1	francouzská
vzdušných	vzdušný	k2eAgFnPc2d1	vzdušná
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
deklarovaným	deklarovaný	k2eAgInSc7d1	deklarovaný
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
vytvoření	vytvoření	k1gNnSc1	vytvoření
bezletové	bezletový	k2eAgFnSc2d1	bezletová
zóny	zóna	k1gFnSc2	zóna
<g/>
,	,	kIx,	,
zastavení	zastavení	k1gNnSc4	zastavení
postupu	postup	k1gInSc2	postup
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
pozemních	pozemní	k2eAgFnPc2d1	pozemní
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
ukončení	ukončení	k1gNnSc1	ukončení
bojů	boj	k1gInPc2	boj
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
ochrana	ochrana	k1gFnSc1	ochrana
civilního	civilní	k2eAgNnSc2d1	civilní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
<g/>
Ačkoliv	ačkoliv	k8xS	ačkoliv
rezoluce	rezoluce	k1gFnSc2	rezoluce
RB	RB	kA	RB
OSN	OSN	kA	OSN
č.	č.	k?	č.
1973	[number]	k4	1973
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
hovoří	hovořit	k5eAaImIp3nS	hovořit
pouze	pouze	k6eAd1	pouze
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
civilního	civilní	k2eAgNnSc2d1	civilní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
vyjádření	vyjádření	k1gNnSc2	vyjádření
i	i	k8xC	i
kroky	krok	k1gInPc1	krok
koalice	koalice	k1gFnSc2	koalice
států	stát	k1gInPc2	stát
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
angažovaly	angažovat	k5eAaBmAgFnP	angažovat
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
<g/>
,	,	kIx,	,
fakticky	fakticky	k6eAd1	fakticky
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
povstalců	povstalec	k1gMnPc2	povstalec
proti	proti	k7c3	proti
vládě	vláda	k1gFnSc3	vláda
Muammara	Muammar	k1gMnSc2	Muammar
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
podpora	podpora	k1gFnSc1	podpora
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
zdůvodňována	zdůvodňován	k2eAgFnSc1d1	zdůvodňována
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
měl	mít	k5eAaImAgMnS	mít
dle	dle	k7c2	dle
některých	některý	k3yIgFnPc2	některý
předních	přední	k2eAgFnPc2d1	přední
osobností	osobnost	k1gFnPc2	osobnost
koalice	koalice	k1gFnSc1	koalice
režim	režim	k1gInSc1	režim
představovat	představovat	k5eAaImF	představovat
vůči	vůči	k7c3	vůči
civilistům	civilista	k1gMnPc3	civilista
<g/>
..	..	k?	..
Kritici	kritik	k1gMnPc1	kritik
pak	pak	k6eAd1	pak
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tímto	tento	k3xDgInSc7	tento
přístupem	přístup	k1gInSc7	přístup
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
koalice	koalice	k1gFnSc1	koalice
přesáhla	přesáhnout	k5eAaPmAgFnS	přesáhnout
daný	daný	k2eAgInSc4d1	daný
právní	právní	k2eAgInSc4d1	právní
mandát	mandát	k1gInSc4	mandát
<g/>
.1	.1	k4	.1
<g/>
.7	.7	k4	.7
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
otevřeně	otevřeně	k6eAd1	otevřeně
přiznala	přiznat	k5eAaPmAgFnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyzbrojila	vyzbrojit	k5eAaPmAgFnS	vyzbrojit
povstalce	povstalec	k1gMnPc4	povstalec
zbraněmi	zbraň	k1gFnPc7	zbraň
shazovanými	shazovaný	k2eAgFnPc7d1	shazovaná
z	z	k7c2	z
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
krok	krok	k1gInSc1	krok
byl	být	k5eAaImAgInS	být
kritizován	kritizovat	k5eAaImNgInS	kritizovat
některými	některý	k3yIgInPc7	některý
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
Čínou	Čína	k1gFnSc7	Čína
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
porušování	porušování	k1gNnSc1	porušování
embarga	embargo	k1gNnSc2	embargo
OSN	OSN	kA	OSN
na	na	k7c4	na
dovoz	dovoz	k1gInSc4	dovoz
zbraní	zbraň	k1gFnPc2	zbraň
do	do	k7c2	do
Libye	Libye	k1gFnSc2	Libye
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
na	na	k7c4	na
kritiku	kritika	k1gFnSc4	kritika
odpověděla	odpovědět	k5eAaPmAgFnS	odpovědět
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
dodávky	dodávka	k1gFnPc1	dodávka
byly	být	k5eAaImAgFnP	být
nutné	nutný	k2eAgFnPc1d1	nutná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
měly	mít	k5eAaImAgFnP	mít
sloužit	sloužit	k5eAaImF	sloužit
civilnímu	civilní	k2eAgNnSc3d1	civilní
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
k	k	k7c3	k
sebeobraně	sebeobrana	k1gFnSc6	sebeobrana
<g/>
.26	.26	k4	.26
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Katar	katar	k1gMnSc1	katar
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
armáda	armáda	k1gFnSc1	armáda
během	během	k7c2	během
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
bojovala	bojovat	k5eAaImAgFnS	bojovat
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
rebelů	rebel	k1gMnPc2	rebel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
Mali	Mali	k1gNnSc6	Mali
==	==	k?	==
</s>
</p>
<p>
<s>
Občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
účastnili	účastnit	k5eAaImAgMnP	účastnit
i	i	k9	i
Tuaregové	Tuareg	k1gMnPc1	Tuareg
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
militarizaci	militarizace	k1gFnSc3	militarizace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
založili	založit	k5eAaPmAgMnP	založit
hnutí	hnutí	k1gNnSc4	hnutí
MNLA	MNLA	kA	MNLA
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
bojovat	bojovat	k5eAaImF	bojovat
za	za	k7c4	za
emancipaci	emancipace	k1gFnSc4	emancipace
Tuarégů	Tuarég	k1gMnPc2	Tuarég
na	na	k7c6	na
území	území	k1gNnSc6	území
Mali	Mali	k1gNnSc2	Mali
<g/>
.	.	kIx.	.
</s>
<s>
Složitá	složitý	k2eAgFnSc1d1	složitá
vnitropolitická	vnitropolitický	k2eAgFnSc1d1	vnitropolitická
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
částech	část	k1gFnPc6	část
Mali	Mali	k1gNnSc2	Mali
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zaneprázdnila	zaneprázdnit	k5eAaPmAgFnS	zaneprázdnit
malijskou	malijský	k2eAgFnSc4d1	malijská
národní	národní	k2eAgFnSc4d1	národní
armádu	armáda	k1gFnSc4	armáda
a	a	k8xC	a
přísun	přísun	k1gInSc4	přísun
množství	množství	k1gNnSc2	množství
zbraní	zbraň	k1gFnPc2	zbraň
z	z	k7c2	z
libyjské	libyjský	k2eAgFnSc2d1	Libyjská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
triumfální	triumfální	k2eAgInSc4d1	triumfální
úspěch	úspěch	k1gInSc4	úspěch
MNLA	MNLA	kA	MNLA
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2012	[number]	k4	2012
jednostranně	jednostranně	k6eAd1	jednostranně
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
nezávislý	závislý	k2eNgInSc4d1	nezávislý
stát	stát	k1gInSc4	stát
Azavad	Azavad	k1gInSc1	Azavad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2012	[number]	k4	2012
ale	ale	k8xC	ale
byli	být	k5eAaImAgMnP	být
Tuaregové	Tuareg	k1gMnPc1	Tuareg
vytlačeni	vytlačit	k5eAaPmNgMnP	vytlačit
radikálními	radikální	k2eAgMnPc7d1	radikální
islamisty	islamista	k1gMnPc7	islamista
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
podnítilo	podnítit	k5eAaPmAgNnS	podnítit
další	další	k2eAgFnSc4d1	další
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
intervenci	intervence	k1gFnSc4	intervence
Západu	západ	k1gInSc2	západ
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
vedenou	vedený	k2eAgFnSc7d1	vedená
Francií	Francie	k1gFnSc7	Francie
(	(	kIx(	(
<g/>
Operace	operace	k1gFnSc1	operace
Serval	serval	k1gMnSc1	serval
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Dočasná	dočasný	k2eAgFnSc1d1	dočasná
národní	národní	k2eAgFnSc1d1	národní
přechodná	přechodný	k2eAgFnSc1d1	přechodná
rada	rada	k1gFnSc1	rada
</s>
</p>
