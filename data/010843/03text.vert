<p>
<s>
Victor	Victor	k1gMnSc1	Victor
Mugubi	Mugubi	k1gNnSc2	Mugubi
Wanyama	Wanyamum	k1gNnSc2	Wanyamum
(	(	kIx(	(
<g/>
*	*	kIx~	*
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
Nairobi	Nairobi	k1gNnSc2	Nairobi
<g/>
,	,	kIx,	,
Keňa	Keňa	k1gFnSc1	Keňa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
keňský	keňský	k2eAgMnSc1d1	keňský
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
záložník	záložník	k1gMnSc1	záložník
a	a	k8xC	a
reprezentant	reprezentant	k1gMnSc1	reprezentant
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
klubu	klub	k1gInSc6	klub
Tottenham	Tottenham	k1gInSc1	Tottenham
Hotspur	Hotspur	k1gMnSc1	Hotspur
FC	FC	kA	FC
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
defenzivního	defenzivní	k2eAgMnSc2d1	defenzivní
záložníka	záložník	k1gMnSc2	záložník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
prvním	první	k4xOgMnSc7	první
keňským	keňský	k2eAgMnSc7d1	keňský
fotbalistou	fotbalista	k1gMnSc7	fotbalista
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
gól	gól	k1gInSc4	gól
v	v	k7c6	v
Lize	liga	k1gFnSc6	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
(	(	kIx(	(
<g/>
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
proti	proti	k7c3	proti
FC	FC	kA	FC
Barcelona	Barcelona	k1gFnSc1	Barcelona
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
přispěl	přispět	k5eAaPmAgMnS	přispět
gólem	gól	k1gInSc7	gól
k	k	k7c3	k
výhře	výhra	k1gFnSc3	výhra
Celtiku	Celtika	k1gFnSc4	Celtika
Glasgow	Glasgow	k1gInSc1	Glasgow
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
sportovně	sportovně	k6eAd1	sportovně
založené	založený	k2eAgFnSc2d1	založená
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
otec	otec	k1gMnSc1	otec
Noah	Noah	k1gMnSc1	Noah
byl	být	k5eAaImAgMnS	být
keňským	keňský	k2eAgMnSc7d1	keňský
fotbalovým	fotbalový	k2eAgMnSc7d1	fotbalový
reprezentantem	reprezentant	k1gMnSc7	reprezentant
<g/>
,	,	kIx,	,
bratři	bratr	k1gMnPc1	bratr
McDonald	McDonald	k1gMnSc1	McDonald
Mariga	Mariga	k1gFnSc1	Mariga
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
a	a	k8xC	a
Sylvester	Sylvester	k1gMnSc1	Sylvester
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
fotbalisté	fotbalista	k1gMnPc1	fotbalista
a	a	k8xC	a
sestra	sestra	k1gFnSc1	sestra
Mercy	Merca	k1gFnSc2	Merca
hrála	hrát	k5eAaImAgFnS	hrát
basketbal	basketbal	k1gInSc4	basketbal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klubová	klubový	k2eAgFnSc1d1	klubová
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
JMJ	JMJ	kA	JMJ
Youth	Youth	k1gMnSc1	Youth
Academy	Academa	k1gFnSc2	Academa
(	(	kIx(	(
<g/>
mládež	mládež	k1gFnSc1	mládež
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nairobi	Nairobi	k1gNnSc1	Nairobi
City	City	k1gFnSc2	City
Stars	Starsa	k1gFnPc2	Starsa
</s>
</p>
<p>
<s>
AFC	AFC	kA	AFC
Leopards	Leopards	k1gInSc1	Leopards
</s>
</p>
<p>
<s>
Helsingborgs	Helsingborgs	k1gInSc1	Helsingborgs
IF	IF	kA	IF
(	(	kIx(	(
<g/>
mládež	mládež	k1gFnSc1	mládež
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
Beerschot	Beerschot	k1gInSc1	Beerschot
AC	AC	kA	AC
2008	[number]	k4	2008
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
</s>
</p>
<p>
<s>
Celtic	Celtice	k1gFnPc2	Celtice
FC	FC	kA	FC
2011	[number]	k4	2011
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
</s>
</p>
<p>
<s>
Southampton	Southampton	k1gInSc1	Southampton
FC	FC	kA	FC
2013	[number]	k4	2013
<g/>
–	–	k?	–
<g/>
2016	[number]	k4	2016
</s>
</p>
<p>
<s>
Tottenham	Tottenham	k6eAd1	Tottenham
Hotspur	Hotspur	k1gMnSc1	Hotspur
FC	FC	kA	FC
2016	[number]	k4	2016
<g/>
–	–	k?	–
</s>
</p>
<p>
<s>
==	==	k?	==
Reprezentační	reprezentační	k2eAgFnSc1d1	reprezentační
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
A-mužstvu	Aužstvo	k1gNnSc6	A-mužstvo
Keni	Keňa	k1gFnSc2	Keňa
debutoval	debutovat	k5eAaBmAgInS	debutovat
27	[number]	k4	27
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
15	[number]	k4	15
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
přátelském	přátelský	k2eAgNnSc6d1	přátelské
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
Nigérii	Nigérie	k1gFnSc3	Nigérie
(	(	kIx(	(
<g/>
porážka	porážka	k1gFnSc1	porážka
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Victor	Victor	k1gMnSc1	Victor
Wanyama	Wanyama	k1gNnSc4	Wanyama
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
hráče	hráč	k1gMnSc2	hráč
na	na	k7c4	na
Transfermarkt	Transfermarkt	k1gInSc4	Transfermarkt
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
hráče	hráč	k1gMnSc2	hráč
na	na	k7c6	na
National	National	k1gFnSc6	National
Football	Footballa	k1gFnPc2	Footballa
Teams	Teamsa	k1gFnPc2	Teamsa
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
