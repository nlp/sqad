<s>
Charles	Charles	k1gMnSc1	Charles
Robert	Robert	k1gMnSc1	Robert
Darwin	Darwin	k1gMnSc1	Darwin
<g/>
,	,	kIx,	,
FRS	FRS	kA	FRS
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1809	[number]	k4	1809
Shrewsbury	Shrewsbura	k1gFnSc2	Shrewsbura
-	-	kIx~	-
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1882	[number]	k4	1882
Downe	Down	k1gInSc5	Down
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
britský	britský	k2eAgMnSc1d1	britský
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
evoluční	evoluční	k2eAgFnSc2d1	evoluční
biologie	biologie	k1gFnSc2	biologie
<g/>
.	.	kIx.	.
</s>
<s>
Evoluční	evoluční	k2eAgFnSc4d1	evoluční
teorii	teorie	k1gFnSc4	teorie
opíral	opírat	k5eAaImAgMnS	opírat
o	o	k7c4	o
přírodní	přírodní	k2eAgInSc4d1	přírodní
výběr	výběr	k1gInSc4	výběr
a	a	k8xC	a
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
výběr	výběr	k1gInSc4	výběr
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Pohlavní	pohlavní	k2eAgInSc1d1	pohlavní
výběr	výběr	k1gInSc1	výběr
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
převaze	převaha	k1gFnSc6	převaha
určitých	určitý	k2eAgMnPc2d1	určitý
jedinců	jedinec	k1gMnPc2	jedinec
nad	nad	k7c7	nad
druhými	druhý	k4xOgFnPc7	druhý
stejného	stejný	k2eAgNnSc2d1	stejné
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
propagace	propagace	k1gFnSc1	propagace
druhů	druh	k1gInPc2	druh
<g/>
;	;	kIx,	;
zatímco	zatímco	k8xS	zatímco
přírodní	přírodní	k2eAgInSc1d1	přírodní
výběr	výběr	k1gInSc1	výběr
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c4	na
vítězství	vítězství	k1gNnSc4	vítězství
obou	dva	k4xCgNnPc2	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
<g/>
,	,	kIx,	,
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
-	-	kIx~	-
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
<g/>
,	,	kIx,	,
1871	[number]	k4	1871
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
lékaře	lékař	k1gMnPc4	lékař
a	a	k8xC	a
vnukem	vnuk	k1gMnSc7	vnuk
botanika	botanik	k1gMnSc2	botanik
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
teologii	teologie	k1gFnSc4	teologie
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
Cambridge	Cambridge	k1gFnSc2	Cambridge
<g/>
,	,	kIx,	,
studia	studio	k1gNnSc2	studio
ukončil	ukončit	k5eAaPmAgInS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1831	[number]	k4	1831
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
studiem	studio	k1gNnSc7	studio
geologických	geologický	k2eAgFnPc2d1	geologická
formací	formace	k1gFnPc2	formace
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
Walesu	Wales	k1gInSc2	Wales
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1831	[number]	k4	1831
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
1742	[number]	k4	1742
dnů	den	k1gInPc2	den
trvající	trvající	k2eAgFnSc4d1	trvající
výzkumnou	výzkumný	k2eAgFnSc4d1	výzkumná
cestu	cesta	k1gFnSc4	cesta
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
HMS	HMS	kA	HMS
Beagle	Beagle	k1gInSc1	Beagle
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1831	[number]	k4	1831
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1836	[number]	k4	1836
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
plavby	plavba	k1gFnSc2	plavba
Darwin	Darwin	k1gMnSc1	Darwin
shromáždil	shromáždit	k5eAaPmAgMnS	shromáždit
cenný	cenný	k2eAgInSc4d1	cenný
přírodovědecký	přírodovědecký	k2eAgInSc4d1	přírodovědecký
materiál	materiál	k1gInSc4	materiál
a	a	k8xC	a
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
svou	svůj	k3xOyFgFnSc4	svůj
základní	základní	k2eAgFnSc4d1	základní
koncepci	koncepce	k1gFnSc4	koncepce
přirozeného	přirozený	k2eAgInSc2d1	přirozený
vzniku	vznik	k1gInSc2	vznik
a	a	k8xC	a
vývoje	vývoj	k1gInSc2	vývoj
druhů	druh	k1gInPc2	druh
evolucí	evoluce	k1gFnPc2	evoluce
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
hlavním	hlavní	k2eAgMnSc7d1	hlavní
hybatelem	hybatel	k1gMnSc7	hybatel
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
dle	dle	k7c2	dle
jeho	jeho	k3xOp3gInSc2	jeho
názoru	názor	k1gInSc2	názor
přírodní	přírodní	k2eAgInSc4d1	přírodní
výběr	výběr	k1gInSc4	výběr
<g/>
.	.	kIx.	.
</s>
<s>
Nejzásadnější	zásadní	k2eAgInSc1d3	nejzásadnější
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
pětitýdenní	pětitýdenní	k2eAgInSc4d1	pětitýdenní
pobyt	pobyt	k1gInSc4	pobyt
na	na	k7c6	na
Galapágách	Galapágy	k1gFnPc6	Galapágy
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
autorství	autorství	k1gNnSc6	autorství
teorie	teorie	k1gFnSc2	teorie
evoluce	evoluce	k1gFnSc2	evoluce
se	se	k3xPyFc4	se
Darwin	Darwin	k1gMnSc1	Darwin
dělí	dělit	k5eAaImIp3nS	dělit
s	s	k7c7	s
Alfredem	Alfred	k1gMnSc7	Alfred
R.	R.	kA	R.
Wallacem	Wallace	k1gNnSc7	Wallace
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c4	v
Mount	Mount	k1gInSc4	Mount
House	house	k1gNnSc4	house
<g/>
,	,	kIx,	,
ve	v	k7c4	v
Shrewsbury	Shrewsbura	k1gFnPc4	Shrewsbura
<g/>
,	,	kIx,	,
hrabství	hrabství	k1gNnSc4	hrabství
Shropshire	Shropshir	k1gMnSc5	Shropshir
<g/>
,	,	kIx,	,
v	v	k7c4	v
Anglii	Anglie	k1gFnSc4	Anglie
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1809	[number]	k4	1809
jako	jako	k8xC	jako
pátý	pátý	k4xOgMnSc1	pátý
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
dětí	dítě	k1gFnPc2	dítě
bohatého	bohatý	k2eAgMnSc2d1	bohatý
lékaře	lékař	k1gMnSc2	lékař
Roberta	Robert	k1gMnSc2	Robert
Darwina	Darwin	k1gMnSc2	Darwin
a	a	k8xC	a
Susannahy	Susannaha	k1gFnSc2	Susannaha
Darwin	Darwin	k1gMnSc1	Darwin
(	(	kIx(	(
<g/>
rozené	rozený	k2eAgNnSc1d1	rozené
Wedgwoodové	Wedgwoodový	k2eAgNnSc1d1	Wedgwoodový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vnukem	vnuk	k1gMnSc7	vnuk
hrnčíře	hrnčíř	k1gMnSc2	hrnčíř
a	a	k8xC	a
podnikatele	podnikatel	k1gMnSc2	podnikatel
Josiaha	Josiah	k1gMnSc2	Josiah
Wedgwooda	Wedgwood	k1gMnSc2	Wedgwood
(	(	kIx(	(
<g/>
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
<g/>
)	)	kIx)	)
a	a	k8xC	a
Erasma	Erasm	k1gMnSc4	Erasm
Darwina	Darwin	k1gMnSc4	Darwin
<g/>
,	,	kIx,	,
lékaře	lékař	k1gMnSc4	lékař
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc4	vynálezce
<g/>
,	,	kIx,	,
básníka	básník	k1gMnSc4	básník
a	a	k8xC	a
vědce	vědec	k1gMnSc4	vědec
<g/>
,	,	kIx,	,
liberálního	liberální	k2eAgMnSc4d1	liberální
whigovce	whigovec	k1gMnSc4	whigovec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
formuloval	formulovat	k5eAaImAgInS	formulovat
evoluční	evoluční	k2eAgFnPc4d1	evoluční
myšlenky	myšlenka	k1gFnPc4	myšlenka
již	již	k6eAd1	již
na	na	k7c6	na
konci	konec	k1gInSc6	konec
osmnáctého	osmnáctý	k4xOgNnSc2	osmnáctý
století	století	k1gNnSc2	století
v	v	k7c6	v
lékařsko-zoologickém	lékařskooologický	k2eAgNnSc6d1	lékařsko-zoologický
pojednání	pojednání	k1gNnSc6	pojednání
Zoonomie	Zoonomie	k1gFnSc2	Zoonomie
<g/>
.	.	kIx.	.
</s>
<s>
Erasmus	Erasmus	k1gMnSc1	Erasmus
Darwin	Darwin	k1gMnSc1	Darwin
dokonce	dokonce	k9	dokonce
přidal	přidat	k5eAaPmAgMnS	přidat
do	do	k7c2	do
rodinného	rodinný	k2eAgInSc2d1	rodinný
erbu	erb	k1gInSc2	erb
3	[number]	k4	3
lastury	lastura	k1gFnSc2	lastura
hřebenatky	hřebenatka	k1gFnSc2	hřebenatka
a	a	k8xC	a
latinsky	latinsky	k6eAd1	latinsky
psaný	psaný	k2eAgInSc1d1	psaný
text	text	k1gInSc1	text
"	"	kIx"	"
<g/>
E	E	kA	E
Conhis	Conhis	k1gInSc1	Conhis
Omnia	omnium	k1gNnSc2	omnium
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
Vše	všechen	k3xTgNnSc1	všechen
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
lastur	lastura	k1gFnPc2	lastura
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
převzal	převzít	k5eAaPmAgInS	převzít
i	i	k8xC	i
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Robert	Robert	k1gMnSc1	Robert
Darwin	Darwin	k1gMnSc1	Darwin
a	a	k8xC	a
přidal	přidat	k5eAaPmAgMnS	přidat
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
ex	ex	k6eAd1	ex
libris	libris	k1gFnSc2	libris
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
dědečkové	dědeček	k1gMnPc1	dědeček
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
prominentní	prominentní	k2eAgFnSc2d1	prominentní
anglické	anglický	k2eAgFnSc2d1	anglická
rodiny	rodina	k1gFnSc2	rodina
Darwinů	Darwin	k1gMnPc2	Darwin
<g/>
-	-	kIx~	-
<g/>
Wedgwoodů	Wedgwood	k1gMnPc2	Wedgwood
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
podporovala	podporovat	k5eAaImAgFnS	podporovat
unitářskou	unitářský	k2eAgFnSc4d1	unitářská
církev	církev	k1gFnSc4	církev
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
pouhých	pouhý	k2eAgNnPc2d1	pouhé
osm	osm	k4xCc1	osm
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
"	"	kIx"	"
<g/>
internátní	internátní	k2eAgFnSc4d1	internátní
<g/>
"	"	kIx"	"
školu	škola	k1gFnSc4	škola
v	v	k7c4	v
Shrewsbury	Shrewsbur	k1gMnPc4	Shrewsbur
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1825	[number]	k4	1825
<g/>
,	,	kIx,	,
po	po	k7c6	po
létě	léto	k1gNnSc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
otci	otec	k1gMnSc3	otec
s	s	k7c7	s
péčí	péče	k1gFnSc7	péče
o	o	k7c4	o
chudé	chudý	k2eAgMnPc4d1	chudý
pacienty	pacient	k1gMnPc4	pacient
ze	z	k7c2	z
Shropshire	Shropshir	k1gInSc5	Shropshir
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
Darwin	Darwin	k1gMnSc1	Darwin
studovat	studovat	k5eAaImF	studovat
medicínu	medicína	k1gFnSc4	medicína
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Edinburghu	Edinburgh	k1gInSc6	Edinburgh
<g/>
.	.	kIx.	.
</s>
<s>
Odpor	odpor	k1gInSc1	odpor
k	k	k7c3	k
brutalitě	brutalita	k1gFnSc3	brutalita
operací	operace	k1gFnPc2	operace
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
zanedbávání	zanedbávání	k1gNnSc3	zanedbávání
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
osvobozeného	osvobozený	k2eAgMnSc2d1	osvobozený
černošského	černošský	k2eAgMnSc2d1	černošský
otroka	otrok	k1gMnSc2	otrok
Johna	John	k1gMnSc2	John
Edmonstona	Edmonston	k1gMnSc2	Edmonston
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
vyprávěl	vyprávět	k5eAaImAgMnS	vyprávět
vzrušující	vzrušující	k2eAgFnPc4d1	vzrušující
historky	historka	k1gFnPc4	historka
o	o	k7c6	o
jihoamerickém	jihoamerický	k2eAgInSc6d1	jihoamerický
pralese	prales	k1gInSc6	prales
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
vycpávat	vycpávat	k5eAaImF	vycpávat
ptáky	pták	k1gMnPc4	pták
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
roce	rok	k1gInSc6	rok
studií	studie	k1gFnPc2	studie
se	se	k3xPyFc4	se
Darwin	Darwin	k1gMnSc1	Darwin
aktivně	aktivně	k6eAd1	aktivně
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
činnosti	činnost	k1gFnSc2	činnost
studentských	studentský	k2eAgFnPc2d1	studentská
přírodovědeckých	přírodovědecký	k2eAgFnPc2d1	Přírodovědecká
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Plinian	Plinian	k1gInSc1	Plinian
Society	societa	k1gFnSc2	societa
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
zapáleným	zapálený	k2eAgMnSc7d1	zapálený
žákem	žák	k1gMnSc7	žák
Roberta	Robert	k1gMnSc2	Robert
Edmunda	Edmund	k1gMnSc2	Edmund
Granta	Grant	k1gMnSc2	Grant
<g/>
,	,	kIx,	,
propagátora	propagátor	k1gMnSc2	propagátor
vývojových	vývojový	k2eAgFnPc2d1	vývojová
teorií	teorie	k1gFnPc2	teorie
Jeana	Jean	k1gMnSc2	Jean
Baptiste	Baptist	k1gMnSc5	Baptist
Lamarcka	Lamarcko	k1gNnSc2	Lamarcko
a	a	k8xC	a
Charlesova	Charlesův	k2eAgMnSc2d1	Charlesův
dědečka	dědeček	k1gMnSc2	dědeček
Erasma	Erasm	k1gMnSc2	Erasm
o	o	k7c4	o
evoluci	evoluce	k1gFnSc4	evoluce
pomocí	pomocí	k7c2	pomocí
získaných	získaný	k2eAgFnPc2d1	získaná
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Darwin	Darwin	k1gMnSc1	Darwin
navázal	navázat	k5eAaPmAgMnS	navázat
také	také	k9	také
na	na	k7c4	na
francouzského	francouzský	k2eAgMnSc4d1	francouzský
vědce	vědec	k1gMnSc4	vědec
Étienna	Étienn	k1gMnSc4	Étienn
Geoffroye	Geoffroy	k1gMnSc4	Geoffroy
Saint-Hilaira	Saint-Hilair	k1gMnSc4	Saint-Hilair
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
jednotě	jednota	k1gFnSc6	jednota
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
i	i	k9	i
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Saint-Hilaira	Saint-Hilaira	k1gMnSc1	Saint-Hilaira
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
prvního	první	k4xOgMnSc4	první
z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
předjímali	předjímat	k5eAaImAgMnP	předjímat
jeho	jeho	k3xOp3gFnSc4	jeho
evoluční	evoluční	k2eAgFnSc4d1	evoluční
teorii	teorie	k1gFnSc4	teorie
.	.	kIx.	.
</s>
<s>
Darwin	Darwin	k1gMnSc1	Darwin
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
Grantova	Grantův	k2eAgInSc2d1	Grantův
výzkumu	výzkum	k1gInSc2	výzkum
životního	životní	k2eAgInSc2d1	životní
cyklu	cyklus	k1gInSc2	cyklus
mořských	mořský	k2eAgNnPc2d1	mořské
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
polypů	polyp	k1gInPc2	polyp
<g/>
)	)	kIx)	)
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
Firth	Firtha	k1gFnPc2	Firtha
of	of	k?	of
Forth	Forth	k1gInSc1	Forth
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
prokázat	prokázat	k5eAaPmF	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lamarckův	Lamarckův	k2eAgInSc1d1	Lamarckův
transmutacionismus	transmutacionismus	k1gInSc1	transmutacionismus
je	být	k5eAaImIp3nS	být
správný	správný	k2eAgInSc1d1	správný
a	a	k8xC	a
přechod	přechod	k1gInSc1	přechod
mezi	mezi	k7c7	mezi
rostlinnou	rostlinný	k2eAgFnSc7d1	rostlinná
a	a	k8xC	a
živočišnou	živočišný	k2eAgFnSc7d1	živočišná
říší	říš	k1gFnSc7	říš
je	být	k5eAaImIp3nS	být
neostrý	ostrý	k2eNgInSc1d1	neostrý
a	a	k8xC	a
postupný	postupný	k2eAgInSc1d1	postupný
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc4d1	stejný
postup	postup	k1gInSc4	postup
pak	pak	k6eAd1	pak
Darwin	Darwin	k1gMnSc1	Darwin
využil	využít	k5eAaPmAgMnS	využít
i	i	k9	i
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dalších	další	k2eAgInPc6d1	další
výzkumech	výzkum	k1gInPc6	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Našli	najít	k5eAaPmAgMnP	najít
důkazy	důkaz	k1gInPc4	důkaz
podporující	podporující	k2eAgFnSc4d1	podporující
existenci	existence	k1gFnSc4	existence
homologie	homologie	k1gFnSc2	homologie
-	-	kIx~	-
teorie	teorie	k1gFnSc1	teorie
o	o	k7c4	o
podobnosti	podobnost	k1gFnPc4	podobnost
orgánů	orgán	k1gInPc2	orgán
sloužících	sloužící	k2eAgInPc2d1	sloužící
stejnému	stejný	k2eAgInSc3d1	stejný
účelu	účel	k1gInSc3	účel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyskytujících	vyskytující	k2eAgInPc2d1	vyskytující
se	se	k3xPyFc4	se
u	u	k7c2	u
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1827	[number]	k4	1827
předložil	předložit	k5eAaPmAgMnS	předložit
Darwin	Darwin	k1gMnSc1	Darwin
Plinian	Plinian	k1gMnSc1	Plinian
Society	societa	k1gFnSc2	societa
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
objev	objev	k1gInSc4	objev
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
černé	černý	k2eAgInPc1d1	černý
útvary	útvar	k1gInPc1	útvar
často	často	k6eAd1	často
nalezené	nalezený	k2eAgFnPc4d1	nalezená
v	v	k7c6	v
lasturách	lastura	k1gFnPc6	lastura
ústřic	ústřice	k1gFnPc2	ústřice
jsou	být	k5eAaImIp3nP	být
vajíčky	vajíčko	k1gNnPc7	vajíčko
pijavice	pijavice	k1gFnSc2	pijavice
Pontobdella	Pontobdell	k1gMnSc4	Pontobdell
muricata	muricat	k1gMnSc4	muricat
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
také	také	k9	také
přírodopisného	přírodopisný	k2eAgInSc2d1	přírodopisný
kurzu	kurz	k1gInSc2	kurz
Roberta	Robert	k1gMnSc2	Robert
Jamesona	Jameson	k1gMnSc2	Jameson
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
si	se	k3xPyFc3	se
osvojil	osvojit	k5eAaPmAgMnS	osvojit
vědomosti	vědomost	k1gFnPc4	vědomost
z	z	k7c2	z
geologie	geologie	k1gFnSc2	geologie
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
také	také	k9	také
s	s	k7c7	s
prací	práce	k1gFnSc7	práce
na	na	k7c6	na
rozsáhlých	rozsáhlý	k2eAgFnPc6d1	rozsáhlá
sbírkách	sbírka	k1gFnPc6	sbírka
Muzea	muzeum	k1gNnSc2	muzeum
Edinburské	Edinburský	k2eAgFnSc2d1	Edinburská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
klasifikovat	klasifikovat	k5eAaImF	klasifikovat
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Darwinův	Darwinův	k2eAgMnSc1d1	Darwinův
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
nešťastný	šťastný	k2eNgMnSc1d1	nešťastný
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mladší	mladý	k2eAgMnSc1d2	mladší
syn	syn	k1gMnSc1	syn
nemá	mít	k5eNaImIp3nS	mít
zájem	zájem	k1gInSc4	zájem
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
lékařem	lékař	k1gMnSc7	lékař
<g/>
,	,	kIx,	,
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1827	[number]	k4	1827
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
k	k	k7c3	k
bakalářskému	bakalářský	k2eAgNnSc3d1	bakalářské
studiu	studio	k1gNnSc3	studio
na	na	k7c4	na
Christ	Christ	k1gInSc4	Christ
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
College	College	k1gNnSc7	College
Univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c4	v
Cambridge	Cambridge	k1gFnPc4	Cambridge
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
duchovním	duchovní	k1gMnSc7	duchovní
<g/>
.	.	kIx.	.
</s>
<s>
Charlesovu	Charlesův	k2eAgMnSc3d1	Charlesův
otci	otec	k1gMnSc3	otec
to	ten	k3xDgNnSc1	ten
připadalo	připadat	k5eAaImAgNnS	připadat
jako	jako	k9	jako
nejrozumnější	rozumný	k2eAgNnSc1d3	nejrozumnější
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
anglikánští	anglikánský	k2eAgMnPc1d1	anglikánský
duchovní	duchovní	k1gMnPc1	duchovní
byli	být	k5eAaImAgMnP	být
zajištěni	zajistit	k5eAaPmNgMnP	zajistit
dostatečným	dostatečný	k2eAgInSc7d1	dostatečný
finančním	finanční	k2eAgInSc7d1	finanční
příjmem	příjem	k1gInSc7	příjem
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
většina	většina	k1gFnSc1	většina
přírodovědců	přírodovědec	k1gMnPc2	přírodovědec
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
byli	být	k5eAaImAgMnP	být
duchovní	duchovní	k2eAgMnPc1d1	duchovní
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
svých	svůj	k3xOyFgFnPc2	svůj
povinností	povinnost	k1gFnPc2	povinnost
"	"	kIx"	"
<g/>
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
zázraky	zázrak	k1gInPc4	zázrak
božího	boží	k2eAgNnSc2d1	boží
stvoření	stvoření	k1gNnSc2	stvoření
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Cambridge	Cambridge	k1gFnPc4	Cambridge
však	však	k9	však
Darwin	Darwin	k1gMnSc1	Darwin
před	před	k7c7	před
studiem	studio	k1gNnSc7	studio
preferoval	preferovat	k5eAaImAgInS	preferovat
střelbu	střelba	k1gFnSc4	střelba
a	a	k8xC	a
jízdu	jízda	k1gFnSc4	jízda
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
bratrancem	bratranec	k1gMnSc7	bratranec
Williamem	William	k1gInSc7	William
Darwinem	Darwin	k1gMnSc7	Darwin
Foxem	fox	k1gInSc7	fox
také	také	k9	také
zcela	zcela	k6eAd1	zcela
propadli	propadnout	k5eAaPmAgMnP	propadnout
vášni	vášeň	k1gFnSc3	vášeň
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnSc4d1	populární
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
bylo	být	k5eAaImAgNnS	být
sbírání	sbírání	k1gNnSc1	sbírání
brouků	brouk	k1gMnPc2	brouk
<g/>
.	.	kIx.	.
</s>
<s>
Fox	fox	k1gInSc1	fox
Darwina	Darwin	k1gMnSc4	Darwin
představil	představit	k5eAaPmAgInS	představit
reverendu	reverend	k1gMnSc3	reverend
Johnu	John	k1gMnSc3	John
Stevens	Stevens	k1gInSc1	Stevens
Henslowovi	Henslowa	k1gMnSc3	Henslowa
<g/>
,	,	kIx,	,
profesoru	profesor	k1gMnSc3	profesor
botaniky	botanika	k1gFnSc2	botanika
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
mu	on	k3xPp3gMnSc3	on
mohl	moct	k5eAaImAgMnS	moct
pomoci	pomoct	k5eAaPmF	pomoct
se	s	k7c7	s
sběrem	sběr	k1gInSc7	sběr
<g/>
.	.	kIx.	.
</s>
<s>
Darwin	Darwin	k1gMnSc1	Darwin
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
do	do	k7c2	do
Henslowova	Henslowův	k2eAgInSc2d1	Henslowův
přírodovědeckého	přírodovědecký	k2eAgInSc2d1	přírodovědecký
kurzu	kurz	k1gInSc2	kurz
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc7	jeho
oblíbeným	oblíbený	k2eAgMnSc7d1	oblíbený
žákem	žák	k1gMnSc7	žák
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
kráčí	kráčet	k5eAaImIp3nS	kráčet
s	s	k7c7	s
Henslowem	Henslowum	k1gNnSc7	Henslowum
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
blížícím	blížící	k2eAgMnSc7d1	blížící
se	se	k3xPyFc4	se
obdobím	období	k1gNnSc7	období
zkoušek	zkouška	k1gFnPc2	zkouška
se	se	k3xPyFc4	se
Darwin	Darwin	k1gMnSc1	Darwin
více	hodně	k6eAd2	hodně
soustředil	soustředit	k5eAaPmAgMnS	soustředit
na	na	k7c4	na
studium	studium	k1gNnSc4	studium
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
byl	být	k5eAaImAgInS	být
privátně	privátně	k6eAd1	privátně
doučován	doučován	k2eAgInSc1d1	doučován
Henslowem	Henslowem	k1gInSc1	Henslowem
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
předměty	předmět	k1gInPc1	předmět
byly	být	k5eAaImAgFnP	být
matematika	matematika	k1gFnSc1	matematika
a	a	k8xC	a
teologie	teologie	k1gFnSc1	teologie
<g/>
.	.	kIx.	.
</s>
<s>
Darwin	Darwin	k1gMnSc1	Darwin
byl	být	k5eAaImAgMnS	být
zvláště	zvláště	k6eAd1	zvláště
nadšen	nadchnout	k5eAaPmNgMnS	nadchnout
spisy	spis	k1gInPc7	spis
Williama	William	k1gMnSc2	William
Paleyho	Paley	k1gMnSc2	Paley
(	(	kIx(	(
<g/>
1743	[number]	k4	1743
<g/>
-	-	kIx~	-
<g/>
1805	[number]	k4	1805
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
argumentu	argument	k1gInSc2	argument
Božího	boží	k2eAgInSc2d1	boží
záměru	záměr	k1gInSc2	záměr
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
závěrečných	závěrečný	k2eAgFnPc6d1	závěrečná
zkouškách	zkouška	k1gFnPc6	zkouška
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1831	[number]	k4	1831
si	se	k3xPyFc3	se
v	v	k7c6	v
teologii	teologie	k1gFnSc6	teologie
vedl	vést	k5eAaImAgInS	vést
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
a	a	k8xC	a
uspěl	uspět	k5eAaPmAgMnS	uspět
i	i	k8xC	i
znalostmi	znalost	k1gFnPc7	znalost
z	z	k7c2	z
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
desátém	desátý	k4xOgNnSc6	desátý
místě	místo	k1gNnSc6	místo
ze	z	k7c2	z
178	[number]	k4	178
absolventů	absolvent	k1gMnPc2	absolvent
<g/>
.	.	kIx.	.
</s>
<s>
Darwin	Darwin	k1gMnSc1	Darwin
musel	muset	k5eAaImAgMnS	muset
v	v	k7c4	v
Cambridge	Cambridge	k1gFnPc4	Cambridge
zůstat	zůstat	k5eAaPmF	zůstat
do	do	k7c2	do
června	červen	k1gInSc2	červen
1831	[number]	k4	1831
<g/>
.	.	kIx.	.
</s>
<s>
Inspirován	inspirovat	k5eAaBmNgInS	inspirovat
dílem	dílo	k1gNnSc7	dílo
Alexandra	Alexandr	k1gMnSc2	Alexandr
von	von	k1gInSc4	von
Humboldta	Humboldto	k1gNnSc2	Humboldto
Personal	Personal	k1gFnSc2	Personal
Narrative	Narrativ	k1gInSc5	Narrativ
<g/>
,	,	kIx,	,
plánoval	plánovat	k5eAaImAgMnS	plánovat
navštívit	navštívit	k5eAaPmF	navštívit
s	s	k7c7	s
některými	některý	k3yIgMnPc7	některý
spolužáky	spolužák	k1gMnPc7	spolužák
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
školy	škola	k1gFnSc2	škola
ostrovy	ostrov	k1gInPc1	ostrov
Madeiry	Madeira	k1gFnSc2	Madeira
a	a	k8xC	a
studovat	studovat	k5eAaImF	studovat
tam	tam	k6eAd1	tam
přírodopis	přírodopis	k1gInSc4	přírodopis
v	v	k7c6	v
tropech	trop	k1gInPc6	trop
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
být	být	k5eAaImF	být
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
dobře	dobře	k6eAd1	dobře
připraven	připravit	k5eAaPmNgMnS	připravit
<g/>
,	,	kIx,	,
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
do	do	k7c2	do
kurzu	kurz	k1gInSc2	kurz
geologie	geologie	k1gFnSc2	geologie
reverenda	reverend	k1gMnSc2	reverend
Adama	Adam	k1gMnSc2	Adam
Sedgwicka	Sedgwicka	k1gFnSc1	Sedgwicka
<g/>
,	,	kIx,	,
silného	silný	k2eAgMnSc4d1	silný
zastánce	zastánce	k1gMnSc4	zastánce
Božího	boží	k2eAgInSc2d1	boží
záměru	záměr	k1gInSc2	záměr
<g/>
,	,	kIx,	,
kterému	který	k3yIgInSc3	který
následně	následně	k6eAd1	následně
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
ve	v	k7c6	v
Walesu	Wales	k1gInSc6	Wales
mapovat	mapovat	k5eAaImF	mapovat
geologické	geologický	k2eAgFnPc1d1	geologická
vrstvy	vrstva	k1gFnPc1	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
ho	on	k3xPp3gNnSc4	on
zastihla	zastihnout	k5eAaPmAgFnS	zastihnout
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
společník	společník	k1gMnSc1	společník
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgNnSc7	který
cestu	cesta	k1gFnSc4	cesta
na	na	k7c4	na
ostrovy	ostrov	k1gInPc4	ostrov
plánoval	plánovat	k5eAaImAgMnS	plánovat
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gInPc1	jeho
plány	plán	k1gInPc1	plán
navštívit	navštívit	k5eAaPmF	navštívit
Madeiru	Madeira	k1gFnSc4	Madeira
zmařeny	zmařen	k2eAgFnPc4d1	zmařena
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
domů	dům	k1gInPc2	dům
však	však	k9	však
obdržel	obdržet	k5eAaPmAgMnS	obdržet
další	další	k2eAgInSc4d1	další
dopis	dopis	k1gInSc4	dopis
<g/>
.	.	kIx.	.
</s>
<s>
Henslow	Henslow	k?	Henslow
doporučil	doporučit	k5eAaPmAgInS	doporučit
Darwina	Darwin	k1gMnSc4	Darwin
na	na	k7c4	na
neplacenou	placený	k2eNgFnSc4d1	neplacená
pozici	pozice	k1gFnSc4	pozice
pánského	pánský	k2eAgMnSc2d1	pánský
společníka	společník	k1gMnSc2	společník
pro	pro	k7c4	pro
R.	R.	kA	R.
FitzRoye	FitzRoy	k1gMnSc2	FitzRoy
<g/>
,	,	kIx,	,
kapitána	kapitán	k1gMnSc2	kapitán
lodi	loď	k1gFnSc2	loď
Beagle	Beagle	k1gFnSc1	Beagle
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
chystala	chystat	k5eAaImAgFnS	chystat
na	na	k7c4	na
dvouletou	dvouletý	k2eAgFnSc4d1	dvouletá
expedici	expedice	k1gFnSc4	expedice
mající	mající	k2eAgFnSc4d1	mající
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
zmapování	zmapování	k1gNnSc4	zmapování
pobřeží	pobřeží	k1gNnSc2	pobřeží
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
Darwina	Darwin	k1gMnSc4	Darwin
vzácná	vzácný	k2eAgFnSc1d1	vzácná
příležitost	příležitost	k1gFnSc1	příležitost
jak	jak	k6eAd1	jak
obohatit	obohatit	k5eAaPmF	obohatit
a	a	k8xC	a
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
své	svůj	k3xOyFgFnPc4	svůj
přírodovědné	přírodovědný	k2eAgFnPc4d1	přírodovědná
vědomosti	vědomost	k1gFnPc4	vědomost
a	a	k8xC	a
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
nejprve	nejprve	k6eAd1	nejprve
proti	proti	k7c3	proti
plavbě	plavba	k1gFnSc3	plavba
<g/>
,	,	kIx,	,
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ztráta	ztráta	k1gFnSc1	ztráta
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Josiah	Josiah	k1gInSc1	Josiah
Wedgwood	Wedgwooda	k1gFnPc2	Wedgwooda
II	II	kA	II
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
Emmy	Emma	k1gFnSc2	Emma
Wedgwoodové	Wedgwoodový	k2eAgFnSc2d1	Wedgwoodový
<g/>
,	,	kIx,	,
ho	on	k3xPp3gMnSc4	on
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
se	s	k7c7	s
synovou	synův	k2eAgFnSc7d1	synova
účastí	účast	k1gFnSc7	účast
nakonec	nakonec	k6eAd1	nakonec
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Plavba	plavba	k1gFnSc1	plavba
trvala	trvat	k5eAaImAgFnS	trvat
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
a	a	k8xC	a
přinesla	přinést	k5eAaPmAgFnS	přinést
dramatické	dramatický	k2eAgFnPc4d1	dramatická
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
odvětvích	odvětví	k1gNnPc6	odvětví
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Darwin	Darwin	k1gMnSc1	Darwin
strávil	strávit	k5eAaPmAgMnS	strávit
dvě	dva	k4xCgFnPc4	dva
třetiny	třetina	k1gFnPc4	třetina
času	čas	k1gInSc2	čas
zkoumáním	zkoumání	k1gNnSc7	zkoumání
pevniny	pevnina	k1gFnSc2	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
geologické	geologický	k2eAgInPc4d1	geologický
jevy	jev	k1gInPc4	jev
<g/>
,	,	kIx,	,
fosílie	fosílie	k1gFnPc4	fosílie
a	a	k8xC	a
živé	živý	k2eAgInPc4d1	živý
organismy	organismus	k1gInPc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Setkal	setkat	k5eAaPmAgInS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
širokým	široký	k2eAgNnSc7d1	široké
spektrem	spektrum	k1gNnSc7	spektrum
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
domorodců	domorodec	k1gMnPc2	domorodec
<g/>
,	,	kIx,	,
tak	tak	k9	tak
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
<g/>
.	.	kIx.	.
</s>
<s>
Metodicky	metodicky	k6eAd1	metodicky
nasbíral	nasbírat	k5eAaPmAgMnS	nasbírat
ohromné	ohromný	k2eAgNnSc4d1	ohromné
množství	množství	k1gNnSc4	množství
vzorků	vzorek	k1gInPc2	vzorek
<g/>
,	,	kIx,	,
mnohé	mnohý	k2eAgFnPc4d1	mnohá
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
vědě	věda	k1gFnSc3	věda
neznámé	známý	k2eNgFnSc3d1	neznámá
<g/>
.	.	kIx.	.
</s>
<s>
Zajistil	zajistit	k5eAaPmAgInS	zajistit
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
pověst	pověst	k1gFnSc4	pověst
uznávaného	uznávaný	k2eAgMnSc4d1	uznávaný
přírodovědce	přírodovědec	k1gMnSc4	přírodovědec
<g/>
.	.	kIx.	.
</s>
<s>
Pořízené	pořízený	k2eAgFnPc1d1	pořízená
detailní	detailní	k2eAgFnPc1d1	detailní
poznámky	poznámka	k1gFnPc1	poznámka
poukazovaly	poukazovat	k5eAaImAgFnP	poukazovat
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
dar	dar	k1gInSc4	dar
teoretika	teoretik	k1gMnSc2	teoretik
<g/>
,	,	kIx,	,
tvořily	tvořit	k5eAaImAgInP	tvořit
základy	základ	k1gInPc4	základ
jeho	jeho	k3xOp3gFnSc2	jeho
pozdější	pozdní	k2eAgFnSc2d2	pozdější
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
umožnily	umožnit	k5eAaPmAgInP	umožnit
sociální	sociální	k2eAgInPc4d1	sociální
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc4d1	politický
a	a	k8xC	a
antropologický	antropologický	k2eAgInSc4d1	antropologický
náhled	náhled	k1gInSc4	náhled
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
navštívil	navštívit	k5eAaPmAgMnS	navštívit
<g/>
.	.	kIx.	.
</s>
<s>
Beagle	Beagle	k6eAd1	Beagle
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
učinila	učinit	k5eAaImAgFnS	učinit
zastávku	zastávka	k1gFnSc4	zastávka
v	v	k7c4	v
Tierra	Tierr	k1gMnSc4	Tierr
del	del	k?	del
Fuego	Fuego	k1gMnSc1	Fuego
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
navrátili	navrátit	k5eAaPmAgMnP	navrátit
tři	tři	k4xCgMnPc4	tři
domorodce	domorodec	k1gMnPc4	domorodec
z	z	k7c2	z
prvního	první	k4xOgInSc2	první
průzkumu	průzkum	k1gInSc2	průzkum
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
1826	[number]	k4	1826
<g/>
-	-	kIx~	-
<g/>
1830	[number]	k4	1830
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
se	se	k3xPyFc4	se
Darwin	Darwin	k1gMnSc1	Darwin
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
místními	místní	k2eAgMnPc7d1	místní
domorodci	domorodec	k1gMnPc7	domorodec
<g/>
,	,	kIx,	,
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
nejhorší	zlý	k2eAgFnPc4d3	nejhorší
a	a	k8xC	a
nejbídnější	bídný	k2eAgFnPc4d3	nejbídnější
bytosti	bytost	k1gFnPc4	bytost
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgFnPc4	jaký
kdy	kdy	k6eAd1	kdy
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
takové	takový	k3xDgMnPc4	takový
lidi	člověk	k1gMnPc4	člověk
lze	lze	k6eAd1	lze
jen	jen	k9	jen
stěží	stěží	k6eAd1	stěží
uvěřit	uvěřit	k5eAaPmF	uvěřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
tvory	tvor	k1gMnPc4	tvor
Božími	boží	k2eAgFnPc7d1	boží
a	a	k8xC	a
obyvateli	obyvatel	k1gMnPc7	obyvatel
téhož	týž	k3xTgInSc2	týž
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Usoudil	usoudit	k5eAaPmAgMnS	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
nejzazší	zadní	k2eAgFnSc6d3	nejzazší
části	část	k1gFnSc6	část
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
člověk	člověk	k1gMnSc1	člověk
existuje	existovat	k5eAaImIp3nS	existovat
na	na	k7c6	na
nižším	nízký	k2eAgInSc6d2	nižší
stupni	stupeň	k1gInSc6	stupeň
dokonalosti	dokonalost	k1gFnSc2	dokonalost
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
kterékoliv	kterýkoliv	k3yIgFnSc6	kterýkoliv
jiné	jiný	k2eAgFnSc6d1	jiná
části	část	k1gFnSc6	část
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Toto	tento	k3xDgNnSc1	tento
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
Darwinovo	Darwinův	k2eAgNnSc4d1	Darwinovo
uvažování	uvažování	k1gNnSc4	uvažování
o	o	k7c6	o
původu	původ	k1gInSc6	původ
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
plavby	plavba	k1gFnSc2	plavba
Darwin	Darwin	k1gMnSc1	Darwin
trpěl	trpět	k5eAaImAgMnS	trpět
mořskou	mořský	k2eAgFnSc7d1	mořská
nemocí	nemoc	k1gFnSc7	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1833	[number]	k4	1833
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
horečku	horečka	k1gFnSc4	horečka
a	a	k8xC	a
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1834	[number]	k4	1834
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
během	během	k7c2	během
cesty	cesta	k1gFnSc2	cesta
z	z	k7c2	z
And	Anda	k1gFnPc2	Anda
do	do	k7c2	do
Valparaisa	Valparais	k1gMnSc2	Valparais
a	a	k8xC	a
strávil	strávit	k5eAaPmAgMnS	strávit
měsíc	měsíc	k1gInSc4	měsíc
upoután	upoután	k2eAgInSc4d1	upoután
na	na	k7c4	na
lůžko	lůžko	k1gNnSc4	lůžko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1837	[number]	k4	1837
Darwin	Darwin	k1gMnSc1	Darwin
opakovaně	opakovaně	k6eAd1	opakovaně
trpěl	trpět	k5eAaImAgMnS	trpět
bolestmi	bolest	k1gFnPc7	bolest
žaludku	žaludek	k1gInSc2	žaludek
<g/>
,	,	kIx,	,
zvracením	zvracení	k1gNnSc7	zvracení
<g/>
,	,	kIx,	,
třesem	třes	k1gInSc7	třes
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
zdravotními	zdravotní	k2eAgInPc7d1	zdravotní
problémy	problém	k1gInPc7	problém
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
příznaky	příznak	k1gInPc1	příznak
ho	on	k3xPp3gMnSc4	on
zvláště	zvláště	k6eAd1	zvláště
sužovaly	sužovat	k5eAaImAgFnP	sužovat
ve	v	k7c6	v
stresových	stresový	k2eAgFnPc6d1	stresová
situacích	situace	k1gFnPc6	situace
<g/>
,	,	kIx,	,
jakými	jaký	k3yQgFnPc7	jaký
byla	být	k5eAaImAgNnP	být
důležitá	důležitý	k2eAgNnPc1d1	důležité
jednání	jednání	k1gNnPc1	jednání
či	či	k8xC	či
diskuze	diskuze	k1gFnPc1	diskuze
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
teorii	teorie	k1gFnSc4	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Příčina	příčina	k1gFnSc1	příčina
Darwinovy	Darwinův	k2eAgFnSc2d1	Darwinova
nemoci	nemoc	k1gFnSc2	nemoc
nebyla	být	k5eNaImAgNnP	být
během	během	k7c2	během
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
známa	známo	k1gNnSc2	známo
a	a	k8xC	a
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
léčení	léčení	k1gNnSc4	léčení
měly	mít	k5eAaImAgFnP	mít
malý	malý	k2eAgInSc4d1	malý
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Nedávné	dávný	k2eNgInPc1d1	nedávný
výzkumy	výzkum	k1gInPc1	výzkum
spekulují	spekulovat	k5eAaImIp3nP	spekulovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
u	u	k7c2	u
něho	on	k3xPp3gNnSc2	on
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
Chagasova	Chagasův	k2eAgFnSc1d1	Chagasova
nemoc	nemoc	k1gFnSc1	nemoc
po	po	k7c6	po
bodnutí	bodnutí	k1gNnSc1	bodnutí
hmyzem	hmyz	k1gInSc7	hmyz
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
možné	možný	k2eAgFnPc1d1	možná
příčiny	příčina	k1gFnPc1	příčina
mohou	moct	k5eAaImIp3nP	moct
souviset	souviset	k5eAaImF	souviset
se	s	k7c7	s
psychosomatickými	psychosomatický	k2eAgInPc7d1	psychosomatický
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
s	s	k7c7	s
Méniè	Méniè	k1gFnSc7	Méniè
chorobou	choroba	k1gFnSc7	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
se	s	k7c7	s
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1836	[number]	k4	1836
loď	loď	k1gFnSc1	loď
Beagle	Beagle	k1gFnSc1	Beagle
vrátila	vrátit	k5eAaPmAgFnS	vrátit
po	po	k7c6	po
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
Darwin	Darwin	k1gMnSc1	Darwin
především	především	k9	především
dík	dík	k7c3	dík
úsilí	úsilí	k1gNnSc3	úsilí
Johna	John	k1gMnSc2	John
Henslowa	Henslowus	k1gMnSc2	Henslowus
a	a	k8xC	a
Adama	Adam	k1gMnSc2	Adam
Sedgwicka	Sedgwicko	k1gNnSc2	Sedgwicko
uznávanou	uznávaný	k2eAgFnSc7d1	uznávaná
osobností	osobnost	k1gFnSc7	osobnost
ve	v	k7c6	v
vědeckých	vědecký	k2eAgInPc6d1	vědecký
kruzích	kruh	k1gInPc6	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
Darwinův	Darwinův	k2eAgMnSc1d1	Darwinův
otec	otec	k1gMnSc1	otec
získal	získat	k5eAaPmAgMnS	získat
vhodnými	vhodný	k2eAgFnPc7d1	vhodná
investicemi	investice	k1gFnPc7	investice
dostatek	dostatek	k1gInSc1	dostatek
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
syn	syn	k1gMnSc1	syn
mohl	moct	k5eAaImAgMnS	moct
stát	stát	k5eAaImF	stát
finančně	finančně	k6eAd1	finančně
nezávislým	závislý	k2eNgMnSc7d1	nezávislý
vědcem	vědec	k1gMnSc7	vědec
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
Cambridge	Cambridge	k1gFnSc1	Cambridge
Darwin	Darwin	k1gMnSc1	Darwin
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
Henslowa	Henslowus	k1gMnSc4	Henslowus
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
botanických	botanický	k2eAgInPc6d1	botanický
popisech	popis	k1gInPc6	popis
nových	nový	k2eAgFnPc2d1	nová
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
cest	cesta	k1gFnPc2	cesta
přivezl	přivézt	k5eAaPmAgInS	přivézt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
získat	získat	k5eAaPmF	získat
ty	ten	k3xDgMnPc4	ten
nejlepší	dobrý	k2eAgMnPc4d3	nejlepší
přírodovědce	přírodovědec	k1gMnPc4	přírodovědec
pro	pro	k7c4	pro
katalogizaci	katalogizace	k1gFnSc4	katalogizace
svých	svůj	k3xOyFgMnPc2	svůj
další	další	k1gNnPc1	další
sbírek	sbírka	k1gFnPc2	sbírka
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
včasnou	včasný	k2eAgFnSc4d1	včasná
publikaci	publikace	k1gFnSc4	publikace
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Darwin	Darwin	k1gMnSc1	Darwin
také	také	k9	také
různé	různý	k2eAgFnPc4d1	různá
instituce	instituce	k1gFnPc4	instituce
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1836	[number]	k4	1836
se	se	k3xPyFc4	se
s	s	k7c7	s
Darwinem	Darwin	k1gMnSc7	Darwin
setkal	setkat	k5eAaPmAgMnS	setkat
horlivý	horlivý	k2eAgMnSc1d1	horlivý
Charles	Charles	k1gMnSc1	Charles
Lyell	Lyell	k1gMnSc1	Lyell
a	a	k8xC	a
představil	představit	k5eAaPmAgMnS	představit
ho	on	k3xPp3gMnSc4	on
nadějnému	nadějný	k2eAgMnSc3d1	nadějný
anatomu	anatom	k1gMnSc3	anatom
Richardu	Richard	k1gMnSc3	Richard
Owenovi	Owen	k1gMnSc3	Owen
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
práci	práce	k1gFnSc6	práce
na	na	k7c6	na
Darwinově	Darwinův	k2eAgFnSc6d1	Darwinova
sbírce	sbírka	k1gFnSc6	sbírka
fosilních	fosilní	k2eAgFnPc2d1	fosilní
kostí	kost	k1gFnPc2	kost
v	v	k7c4	v
Royal	Royal	k1gInSc4	Royal
College	Colleg	k1gFnSc2	Colleg
of	of	k?	of
Surgeons	Surgeons	k1gInSc1	Surgeons
Owen	Owen	k1gMnSc1	Owen
odhalil	odhalit	k5eAaPmAgMnS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnohé	mnohý	k2eAgFnPc1d1	mnohá
pocházely	pocházet	k5eAaImAgFnP	pocházet
z	z	k7c2	z
vyhynulých	vyhynulý	k2eAgMnPc2d1	vyhynulý
tvorů	tvor	k1gMnPc2	tvor
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
zjištěním	zjištění	k1gNnSc7	zjištění
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
Darwinova	Darwinův	k2eAgFnSc1d1	Darwinova
reputace	reputace	k1gFnSc1	reputace
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
nadšené	nadšený	k2eAgFnSc2d1	nadšená
Lyellovy	Lyellův	k2eAgFnSc2d1	Lyellův
podpory	podpora	k1gFnSc2	podpora
předložil	předložit	k5eAaPmAgMnS	předložit
Darwin	Darwin	k1gMnSc1	Darwin
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1837	[number]	k4	1837
Londýnské	londýnský	k2eAgFnSc2d1	londýnská
geologické	geologický	k2eAgFnSc2d1	geologická
společnosti	společnost	k1gFnSc2	společnost
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jihoamerická	jihoamerický	k2eAgFnSc1d1	jihoamerická
pevnina	pevnina	k1gFnSc1	pevnina
pomalu	pomalu	k6eAd1	pomalu
zvedá	zvedat	k5eAaImIp3nS	zvedat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
Darwin	Darwin	k1gMnSc1	Darwin
také	také	k9	také
představil	představit	k5eAaPmAgMnS	představit
své	svůj	k3xOyFgInPc4	svůj
vzorky	vzorek	k1gInPc4	vzorek
savců	savec	k1gMnPc2	savec
a	a	k8xC	a
ptáků	pták	k1gMnPc2	pták
Londýnské	londýnský	k2eAgFnSc2d1	londýnská
zoologické	zoologický	k2eAgFnSc2d1	zoologická
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
biologická	biologický	k2eAgFnSc1d1	biologická
i	i	k8xC	i
geologická	geologický	k2eAgNnPc1d1	geologické
pozorování	pozorování	k1gNnPc1	pozorování
ho	on	k3xPp3gMnSc4	on
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
úvahám	úvaha	k1gFnPc3	úvaha
o	o	k7c6	o
možných	možný	k2eAgFnPc6d1	možná
přeměnách	přeměna	k1gFnPc6	přeměna
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1838	[number]	k4	1838
<g/>
,	,	kIx,	,
po	po	k7c6	po
přečtení	přečtení	k1gNnSc6	přečtení
eseje	esej	k1gFnSc2	esej
Thomase	Thomas	k1gMnSc2	Thomas
Malthuse	Malthuse	k1gFnSc2	Malthuse
O	o	k7c6	o
principu	princip	k1gInSc6	princip
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
ho	on	k3xPp3gInSc4	on
dovedla	dovést	k5eAaPmAgFnS	dovést
až	až	k6eAd1	až
k	k	k7c3	k
představě	představa	k1gFnSc3	představa
teorie	teorie	k1gFnSc2	teorie
přírodního	přírodní	k2eAgInSc2d1	přírodní
výběru	výběr	k1gInSc2	výběr
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
si	se	k3xPyFc3	se
byl	být	k5eAaImAgMnS	být
plně	plně	k6eAd1	plně
vědom	vědom	k2eAgMnSc1d1	vědom
pravděpodobné	pravděpodobný	k2eAgFnPc1d1	pravděpodobná
reakce	reakce	k1gFnSc2	reakce
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
objev	objev	k1gInSc4	objev
<g/>
,	,	kIx,	,
svěřil	svěřit	k5eAaPmAgInS	svěřit
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
svojí	svůj	k3xOyFgFnSc7	svůj
teorií	teorie	k1gFnSc7	teorie
jen	jen	k9	jen
nejbližším	blízký	k2eAgMnPc3d3	nejbližší
přátelům	přítel	k1gMnPc3	přítel
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
být	být	k5eAaImF	být
připraven	připravit	k5eAaPmNgMnS	připravit
čelit	čelit	k5eAaImF	čelit
očekávaným	očekávaný	k2eAgFnPc3d1	očekávaná
námitkám	námitka	k1gFnPc3	námitka
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1839	[number]	k4	1839
byl	být	k5eAaImAgMnS	být
Darwin	Darwin	k1gMnSc1	Darwin
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
členem	člen	k1gMnSc7	člen
Královské	královský	k2eAgFnSc2d1	královská
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
Royal	Royal	k1gInSc1	Royal
Society	societa	k1gFnSc2	societa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1858	[number]	k4	1858
objevily	objevit	k5eAaPmAgFnP	objevit
informace	informace	k1gFnPc1	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
Alfred	Alfred	k1gMnSc1	Alfred
Russel	Russel	k1gMnSc1	Russel
Wallace	Wallace	k1gFnSc2	Wallace
dospěl	dochvít	k5eAaPmAgMnS	dochvít
k	k	k7c3	k
podobné	podobný	k2eAgFnSc3d1	podobná
teorii	teorie	k1gFnSc3	teorie
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
Darwin	Darwin	k1gMnSc1	Darwin
urychlit	urychlit	k5eAaPmF	urychlit
publikování	publikování	k1gNnSc2	publikování
své	svůj	k3xOyFgFnSc2	svůj
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kniha	kniha	k1gFnSc1	kniha
On	on	k3xPp3gInSc1	on
the	the	k?	the
Origin	Origin	k1gInSc1	Origin
of	of	k?	of
Species	species	k1gFnSc4	species
by	by	k9	by
Means	Means	k1gInSc4	Means
of	of	k?	of
Natural	Natural	k?	Natural
Selection	Selection	k1gInSc1	Selection
<g/>
,	,	kIx,	,
or	or	k?	or
the	the	k?	the
Preservation	Preservation	k1gInSc1	Preservation
of	of	k?	of
Favoured	Favoured	k1gInSc1	Favoured
Races	Races	k1gInSc1	Races
in	in	k?	in
the	the	k?	the
Struggle	Struggle	k1gNnSc4	Struggle
for	forum	k1gNnPc2	forum
Life	Lif	k1gFnSc2	Lif
(	(	kIx(	(
<g/>
O	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
druhů	druh	k1gInPc2	druh
přírodním	přírodní	k2eAgInSc7d1	přírodní
výběrem	výběr	k1gInSc7	výběr
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
uchováním	uchování	k1gNnSc7	uchování
prospěšných	prospěšný	k2eAgNnPc2d1	prospěšné
plemen	plemeno	k1gNnPc2	plemeno
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
život	život	k1gInSc4	život
-	-	kIx~	-
většinou	většinou	k6eAd1	většinou
zkracovaná	zkracovaný	k2eAgFnSc1d1	zkracovaná
na	na	k7c4	na
<g/>
:	:	kIx,	:
O	o	k7c6	o
původu	původ	k1gInSc6	původ
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydaná	vydaný	k2eAgFnSc1d1	vydaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
<g/>
,	,	kIx,	,
představila	představit	k5eAaPmAgFnS	představit
vývoj	vývoj	k1gInSc4	vývoj
organismů	organismus	k1gInPc2	organismus
od	od	k7c2	od
společného	společný	k2eAgMnSc2d1	společný
předka	předek	k1gMnSc2	předek
a	a	k8xC	a
prezentovala	prezentovat	k5eAaBmAgFnS	prezentovat
ji	on	k3xPp3gFnSc4	on
jako	jako	k9	jako
komplexní	komplexní	k2eAgFnSc4d1	komplexní
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
teorii	teorie	k1gFnSc4	teorie
vývoje	vývoj	k1gInSc2	vývoj
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Darwin	Darwin	k1gMnSc1	Darwin
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
a	a	k8xC	a
napsal	napsat	k5eAaBmAgMnS	napsat
sérii	série	k1gFnSc4	série
knih	kniha	k1gFnPc2	kniha
o	o	k7c6	o
rostlinách	rostlina	k1gFnPc6	rostlina
a	a	k8xC	a
živočiších	živočich	k1gMnPc6	živočich
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
,	,	kIx,	,
k	k	k7c3	k
významnějším	významný	k2eAgFnPc3d2	významnější
patřily	patřit	k5eAaImAgFnP	patřit
The	The	k1gFnPc1	The
Descent	Descent	k1gInSc1	Descent
of	of	k?	of
Man	mana	k1gFnPc2	mana
<g/>
,	,	kIx,	,
and	and	k?	and
Selection	Selection	k1gInSc1	Selection
in	in	k?	in
Relation	Relation	k1gInSc1	Relation
to	ten	k3xDgNnSc1	ten
Sex	sex	k1gInSc4	sex
<g/>
,	,	kIx,	,
a	a	k8xC	a
The	The	k1gFnPc1	The
Expression	Expression	k1gInSc1	Expression
of	of	k?	of
the	the	k?	the
Emotions	Emotionsa	k1gFnPc2	Emotionsa
in	in	k?	in
Man	Man	k1gMnSc1	Man
and	and	k?	and
Animals	Animals	k1gInSc1	Animals
(	(	kIx(	(
<g/>
Výraz	výraz	k1gInSc1	výraz
emocí	emoce	k1gFnPc2	emoce
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
zvířat	zvíře	k1gNnPc2	zvíře
-	-	kIx~	-
česky	česky	k6eAd1	česky
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
opakovaným	opakovaný	k2eAgInPc3d1	opakovaný
zápasům	zápas	k1gInPc3	zápas
s	s	k7c7	s
nemocí	nemoc	k1gFnSc7	nemoc
během	během	k7c2	během
posledních	poslední	k2eAgNnPc2d1	poslední
dvaceti	dvacet	k4xCc2	dvacet
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
Darwin	Darwin	k1gMnSc1	Darwin
nutil	nutit	k5eAaImAgMnS	nutit
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
experimenty	experiment	k1gInPc1	experiment
<g/>
,	,	kIx,	,
výzkum	výzkum	k1gInSc1	výzkum
a	a	k8xC	a
psaní	psaní	k1gNnSc4	psaní
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Publikoval	publikovat	k5eAaBmAgMnS	publikovat
úryvky	úryvek	k1gInPc1	úryvek
své	svůj	k3xOyFgFnSc2	svůj
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kontroverznější	kontroverzní	k2eAgInPc1d2	kontroverznější
aspekty	aspekt	k1gInPc1	aspekt
jeho	jeho	k3xOp3gFnSc2	jeho
"	"	kIx"	"
<g/>
velké	velký	k2eAgFnSc2d1	velká
knihy	kniha	k1gFnSc2	kniha
<g/>
"	"	kIx"	"
-	-	kIx~	-
lidský	lidský	k2eAgInSc4d1	lidský
vzestup	vzestup	k1gInSc4	vzestup
od	od	k7c2	od
dřívějších	dřívější	k2eAgNnPc2d1	dřívější
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
mechanismus	mechanismus	k1gInSc1	mechanismus
pohlavního	pohlavní	k2eAgInSc2d1	pohlavní
výběru	výběr	k1gInSc2	výběr
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
doporučující	doporučující	k2eAgFnPc1d1	doporučující
možné	možný	k2eAgFnPc1d1	možná
příčiny	příčina	k1gFnPc1	příčina
podtrhující	podtrhující	k2eAgFnPc1d1	podtrhující
vývoj	vývoj	k1gInSc4	vývoj
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
lidských	lidský	k2eAgFnPc2d1	lidská
duševních	duševní	k2eAgFnPc2d1	duševní
způsobilostí	způsobilost	k1gFnPc2	způsobilost
-	-	kIx~	-
byly	být	k5eAaImAgFnP	být
ještě	ještě	k9	ještě
neúplné	úplný	k2eNgFnPc1d1	neúplná
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
současní	současný	k2eAgMnPc1d1	současný
Darwinovi	Darwinův	k2eAgMnPc1d1	Darwinův
kritici	kritik	k1gMnPc1	kritik
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
kreacionistů	kreacionista	k1gMnPc2	kreacionista
<g/>
)	)	kIx)	)
určují	určovat	k5eAaImIp3nP	určovat
období	období	k1gNnSc4	období
definování	definování	k1gNnPc2	definování
teorie	teorie	k1gFnSc2	teorie
přírodního	přírodní	k2eAgInSc2d1	přírodní
výběru	výběr	k1gInSc2	výběr
do	do	k7c2	do
období	období	k1gNnSc2	období
před	před	k7c7	před
plavbou	plavba	k1gFnSc7	plavba
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
Beagle	Beagle	k1gFnSc2	Beagle
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Darwin	Darwin	k1gMnSc1	Darwin
v	v	k7c6	v
době	doba	k1gFnSc6	doba
svých	svůj	k3xOyFgFnPc2	svůj
studií	studie	k1gFnPc2	studie
prostudoval	prostudovat	k5eAaPmAgMnS	prostudovat
Zoonomii	Zoonomie	k1gFnSc4	Zoonomie
svého	svůj	k3xOyFgMnSc2	svůj
dědečka	dědeček	k1gMnSc2	dědeček
Erasma	Erasm	k1gMnSc2	Erasm
Darwina	Darwin	k1gMnSc2	Darwin
<g/>
,	,	kIx,	,
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
studoval	studovat	k5eAaImAgMnS	studovat
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
radikálního	radikální	k2eAgMnSc2d1	radikální
evolucionisty	evolucionista	k1gMnSc2	evolucionista
Roberta	Robert	k1gMnSc2	Robert
Granta	Grant	k1gMnSc2	Grant
<g/>
,	,	kIx,	,
znal	znát	k5eAaImAgInS	znát
Lamarckovu	Lamarckův	k2eAgFnSc4d1	Lamarckova
argumentaci	argumentace	k1gFnSc4	argumentace
ohledně	ohledně	k7c2	ohledně
vývoje	vývoj	k1gInSc2	vývoj
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
sdílel	sdílet	k5eAaImAgMnS	sdílet
společné	společný	k2eAgInPc4d1	společný
názory	názor	k1gInPc4	názor
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
otcem	otec	k1gMnSc7	otec
i	i	k8xC	i
bratrem	bratr	k1gMnSc7	bratr
<g/>
,	,	kIx,	,
oběma	dva	k4xCgMnPc7	dva
význačnými	význačný	k2eAgMnPc7d1	význačný
whigovci	whigovec	k1gMnPc7	whigovec
<g/>
.	.	kIx.	.
</s>
<s>
Benjamin	Benjamin	k1gMnSc1	Benjamin
Wiker	Wiker	k1gMnSc1	Wiker
tak	tak	k6eAd1	tak
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
teorie	teorie	k1gFnSc1	teorie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ještě	ještě	k6eAd1	ještě
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
začal	začít	k5eAaPmAgMnS	začít
Darwin	Darwin	k1gMnSc1	Darwin
sbírat	sbírat	k5eAaImF	sbírat
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
podpořily	podpořit	k5eAaPmAgInP	podpořit
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgFnSc7d1	standardní
interpretací	interpretace	k1gFnSc7	interpretace
dobových	dobový	k2eAgInPc2d1	dobový
dokumentů	dokument	k1gInPc2	dokument
je	být	k5eAaImIp3nS	být
však	však	k9	však
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
formulování	formulování	k1gNnSc1	formulování
teorie	teorie	k1gFnSc2	teorie
přirozeného	přirozený	k2eAgInSc2d1	přirozený
výběru	výběr	k1gInSc2	výběr
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
období	období	k1gNnSc2	období
let	let	k1gInSc4	let
1842	[number]	k4	1842
(	(	kIx(	(
<g/>
první	první	k4xOgInPc4	první
črty	črt	k1gInPc1	črt
<g/>
,	,	kIx,	,
35	[number]	k4	35
str	str	kA	str
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1844	[number]	k4	1844
(	(	kIx(	(
<g/>
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
koncept	koncept	k1gInSc1	koncept
<g/>
,	,	kIx,	,
230	[number]	k4	230
str	str	kA	str
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
následně	následně	k6eAd1	následně
1856	[number]	k4	1856
<g/>
-	-	kIx~	-
<g/>
1859	[number]	k4	1859
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Darwin	Darwin	k1gMnSc1	Darwin
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
roce	rok	k1gInSc6	rok
urychleně	urychleně	k6eAd1	urychleně
dopisoval	dopisovat	k5eAaImAgMnS	dopisovat
zkrácenou	zkrácený	k2eAgFnSc4d1	zkrácená
verzi	verze	k1gFnSc4	verze
kvůli	kvůli	k7c3	kvůli
A.	A.	kA	A.
R.	R.	kA	R.
Wallacemu	Wallacem	k1gInSc6	Wallacem
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenky	myšlenka	k1gFnPc1	myšlenka
a	a	k8xC	a
poznatky	poznatek	k1gInPc1	poznatek
Charlese	Charles	k1gMnSc2	Charles
Darwina	Darwin	k1gMnSc2	Darwin
byly	být	k5eAaImAgFnP	být
rozvíjeny	rozvíjen	k2eAgFnPc1d1	rozvíjena
i	i	k9	i
mimo	mimo	k7c4	mimo
přírodní	přírodní	k2eAgFnSc4d1	přírodní
vědu	věda	k1gFnSc4	věda
ve	v	k7c6	v
společenském	společenský	k2eAgInSc6d1	společenský
kontextu	kontext	k1gInSc6	kontext
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
sociálními	sociální	k2eAgMnPc7d1	sociální
darwinisty	darwinista	k1gMnPc7	darwinista
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
úsilí	úsilí	k1gNnSc1	úsilí
nakonec	nakonec	k6eAd1	nakonec
vedlo	vést	k5eAaImAgNnS	vést
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
až	až	k9	až
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
eugeniky	eugenika	k1gFnSc2	eugenika
a	a	k8xC	a
rasových	rasový	k2eAgFnPc2d1	rasová
teorií	teorie	k1gFnPc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Darwin	Darwin	k1gMnSc1	Darwin
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Downe	Down	k1gMnSc5	Down
<g/>
,	,	kIx,	,
v	v	k7c4	v
Kentu	Kenta	k1gFnSc4	Kenta
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1882	[number]	k4	1882
<g/>
.	.	kIx.	.
</s>
<s>
Myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
pohřben	pohřbít	k5eAaPmNgInS	pohřbít
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
St.	st.	kA	st.
Mary	Mary	k1gFnSc4	Mary
v	v	k7c6	v
Downe	Down	k1gMnSc5	Down
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
Darwinových	Darwinových	k2eAgMnPc2d1	Darwinových
kolegů	kolega	k1gMnPc2	kolega
a	a	k8xC	a
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
jeho	jeho	k3xOp3gFnSc2	jeho
významnosti	významnost	k1gFnSc2	významnost
zařídil	zařídit	k5eAaPmAgMnS	zařídit
William	William	k1gInSc4	William
Spottiswoode	Spottiswood	k1gInSc5	Spottiswood
-	-	kIx~	-
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
prezident	prezident	k1gMnSc1	prezident
Královské	královský	k2eAgFnSc2d1	královská
společnosti	společnost	k1gFnSc2	společnost
pro	pro	k7c4	pro
Darwina	Darwin	k1gMnSc4	Darwin
státní	státní	k2eAgInSc1d1	státní
pohřeb	pohřeb	k1gInSc1	pohřeb
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
jsou	být	k5eAaImIp3nP	být
uloženy	uložen	k2eAgInPc1d1	uložen
ve	v	k7c6	v
Westminsterském	Westminsterský	k2eAgNnSc6d1	Westminsterské
opatství	opatství	k1gNnSc6	opatství
<g/>
,	,	kIx,	,
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
hrobů	hrob	k1gInPc2	hrob
Williama	William	k1gMnSc2	William
Herschela	Herschel	k1gMnSc2	Herschel
a	a	k8xC	a
Isaaca	Isaacus	k1gMnSc2	Isaacus
Newtona	Newton	k1gMnSc2	Newton
<g/>
.	.	kIx.	.
</s>
<s>
Evoluční	evoluční	k2eAgFnSc4d1	evoluční
teorii	teorie	k1gFnSc4	teorie
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
široce	široko	k6eAd1	široko
známým	známý	k2eAgInSc7d1	známý
dílem	díl	k1gInSc7	díl
Charlese	Charles	k1gMnSc2	Charles
Darwina	Darwin	k1gMnSc2	Darwin
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
geniální	geniální	k2eAgMnSc1d1	geniální
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
však	však	k9	však
významně	významně	k6eAd1	významně
zasáhl	zasáhnout	k5eAaPmAgMnS	zasáhnout
do	do	k7c2	do
více	hodně	k6eAd2	hodně
oborů	obor	k1gInPc2	obor
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Atol	atol	k1gInSc1	atol
<g/>
.	.	kIx.	.
</s>
<s>
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
Darwin	Darwin	k1gMnSc1	Darwin
již	již	k6eAd1	již
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
plavbě	plavba	k1gFnSc6	plavba
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
Beagle	Beagle	k1gFnSc2	Beagle
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
správnou	správný	k2eAgFnSc4d1	správná
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
všeobecně	všeobecně	k6eAd1	všeobecně
uznávanou	uznávaný	k2eAgFnSc4d1	uznávaná
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
rozvíjenou	rozvíjený	k2eAgFnSc4d1	rozvíjená
teorii	teorie	k1gFnSc4	teorie
vzniku	vznik	k1gInSc2	vznik
a	a	k8xC	a
vývoje	vývoj	k1gInSc2	vývoj
korálových	korálový	k2eAgInPc2d1	korálový
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
známé	známý	k2eAgNnSc1d1	známé
též	též	k9	též
jako	jako	k8xS	jako
teorie	teorie	k1gFnSc1	teorie
poklesu	pokles	k1gInSc2	pokles
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
napřed	napřed	k6eAd1	napřed
okolo	okolo	k7c2	okolo
centrálního	centrální	k2eAgInSc2d1	centrální
sopečného	sopečný	k2eAgInSc2d1	sopečný
ostrova	ostrov	k1gInSc2	ostrov
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
lemový	lemový	k2eAgInSc1d1	lemový
korálový	korálový	k2eAgInSc1d1	korálový
útes	útes	k1gInSc1	útes
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
následném	následný	k2eAgInSc6d1	následný
poklesu	pokles	k1gInSc6	pokles
centrálního	centrální	k2eAgInSc2d1	centrální
ostrova	ostrov	k1gInSc2	ostrov
a	a	k8xC	a
současném	současný	k2eAgInSc6d1	současný
růstu	růst	k1gInSc6	růst
korálů	korál	k1gMnPc2	korál
se	se	k3xPyFc4	se
okolo	okolo	k7c2	okolo
ostrova	ostrov	k1gInSc2	ostrov
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
pásmo	pásmo	k1gNnSc1	pásmo
bariérových	bariérový	k2eAgInPc2d1	bariérový
útesů	útes	k1gInPc2	útes
a	a	k8xC	a
po	po	k7c6	po
potopení	potopení	k1gNnSc6	potopení
ostrova	ostrov	k1gInSc2	ostrov
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
centrální	centrální	k2eAgFnSc1d1	centrální
laguna	laguna	k1gFnSc1	laguna
a	a	k8xC	a
atol	atol	k1gInSc1	atol
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Původní	původní	k2eAgNnPc4d1	původní
díla	dílo	k1gNnPc4	dílo
<g/>
:	:	kIx,	:
1842	[number]	k4	1842
<g/>
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Svijonožci	svijonožec	k1gMnSc3	svijonožec
<g/>
.	.	kIx.	.
</s>
<s>
Klasické	klasický	k2eAgNnSc1d1	klasické
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
popisné	popisný	k2eAgNnSc1d1	popisné
a	a	k8xC	a
systematické	systematický	k2eAgNnSc1d1	systematické
dílo	dílo	k1gNnSc1	dílo
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
prací	práce	k1gFnSc7	práce
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
doufal	doufat	k5eAaImAgMnS	doufat
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
Darwin	Darwin	k1gMnSc1	Darwin
získat	získat	k5eAaPmF	získat
všeobecné	všeobecný	k2eAgNnSc4d1	všeobecné
uznání	uznání	k1gNnSc4	uznání
a	a	k8xC	a
vážnost	vážnost	k1gFnSc4	vážnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgFnP	být
oporou	opora	k1gFnSc7	opora
při	při	k7c6	při
publikování	publikování	k1gNnSc6	publikování
evoluční	evoluční	k2eAgFnSc2d1	evoluční
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Původní	původní	k2eAgNnPc4d1	původní
díla	dílo	k1gNnPc4	dílo
<g/>
:	:	kIx,	:
1851	[number]	k4	1851
<g/>
,	,	kIx,	,
1854	[number]	k4	1854
<g/>
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Evoluce	evoluce	k1gFnSc2	evoluce
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnější	slavný	k2eAgNnSc4d3	nejslavnější
a	a	k8xC	a
nejzávažnější	závažný	k2eAgNnSc4d3	nejzávažnější
dílo	dílo	k1gNnSc4	dílo
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
Darwina	Darwin	k1gMnSc2	Darwin
<g/>
.	.	kIx.	.
</s>
<s>
Otázkou	otázka	k1gFnSc7	otázka
vzniku	vznik	k1gInSc2	vznik
a	a	k8xC	a
vývoje	vývoj	k1gInSc2	vývoj
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
zabývalo	zabývat	k5eAaImAgNnS	zabývat
více	hodně	k6eAd2	hodně
učenců	učenec	k1gMnPc2	učenec
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Darwinova	Darwinův	k2eAgMnSc2d1	Darwinův
dědečka	dědeček	k1gMnSc2	dědeček
Erasma	Erasm	k1gMnSc2	Erasm
Darwina	Darwin	k1gMnSc2	Darwin
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
tak	tak	k6eAd1	tak
nebyl	být	k5eNaImAgMnS	být
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
přišel	přijít	k5eAaPmAgMnS	přijít
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
proměny	proměna	k1gFnSc2	proměna
a	a	k8xC	a
vývoje	vývoj	k1gInSc2	vývoj
druhů	druh	k1gInPc2	druh
neboli	neboli	k8xC	neboli
evoluce	evoluce	k1gFnSc2	evoluce
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
však	však	k9	však
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
objevil	objevit	k5eAaPmAgMnS	objevit
a	a	k8xC	a
správně	správně	k6eAd1	správně
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
na	na	k7c6	na
jakém	jaký	k3yIgInSc6	jaký
principu	princip	k1gInSc6	princip
evoluce	evoluce	k1gFnSc2	evoluce
probíhá	probíhat	k5eAaImIp3nS	probíhat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Darwinově	Darwinův	k2eAgFnSc6d1	Darwinova
době	doba	k1gFnSc6	doba
nebyly	být	k5eNaImAgInP	být
známy	znám	k2eAgInPc1d1	znám
geny	gen	k1gInPc1	gen
a	a	k8xC	a
DNA	dna	k1gFnSc1	dna
<g/>
,	,	kIx,	,
Darwin	Darwin	k1gMnSc1	Darwin
proto	proto	k8xC	proto
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
základní	základní	k2eAgFnSc4d1	základní
jednotku	jednotka	k1gFnSc4	jednotka
přírodního	přírodní	k2eAgInSc2d1	přírodní
výběru	výběr	k1gInSc2	výběr
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
přinášelo	přinášet	k5eAaImAgNnS	přinášet
některé	některý	k3yIgInPc4	některý
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Upřesnění	upřesnění	k1gNnSc1	upřesnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
základní	základní	k2eAgFnSc7d1	základní
jednotkou	jednotka	k1gFnSc7	jednotka
přírodního	přírodní	k2eAgInSc2d1	přírodní
výběru	výběr	k1gInSc2	výběr
je	být	k5eAaImIp3nS	být
gen	gen	k1gInSc1	gen
čekalo	čekat	k5eAaImAgNnS	čekat
až	až	k9	až
na	na	k7c4	na
Darwinovy	Darwinův	k2eAgMnPc4d1	Darwinův
následovníky	následovník	k1gMnPc4	následovník
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Původní	původní	k2eAgNnPc4d1	původní
díla	dílo	k1gNnPc4	dílo
<g/>
:	:	kIx,	:
1858	[number]	k4	1858
<g/>
,	,	kIx,	,
1859	[number]	k4	1859
<g/>
,	,	kIx,	,
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Masožravé	masožravý	k2eAgFnSc2d1	masožravá
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
masožravých	masožravý	k2eAgFnPc2d1	masožravá
rostlin	rostlina	k1gFnPc2	rostlina
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgMnSc1d1	populární
<g/>
,	,	kIx,	,
věnovali	věnovat	k5eAaImAgMnP	věnovat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
Darwinovi	Darwinův	k2eAgMnPc1d1	Darwinův
předchůdci	předchůdce	k1gMnPc1	předchůdce
i	i	k8xC	i
současníci	současník	k1gMnPc1	současník
<g/>
.	.	kIx.	.
</s>
<s>
Darwin	Darwin	k1gMnSc1	Darwin
se	se	k3xPyFc4	se
pěstování	pěstování	k1gNnSc3	pěstování
a	a	k8xC	a
studiu	studio	k1gNnSc3	studio
těchto	tento	k3xDgFnPc2	tento
rostlin	rostlina	k1gFnPc2	rostlina
začal	začít	k5eAaPmAgInS	začít
věnovat	věnovat	k5eAaImF	věnovat
někdy	někdy	k6eAd1	někdy
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
patnácti	patnáct	k4xCc6	patnáct
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1875	[number]	k4	1875
vydal	vydat	k5eAaPmAgMnS	vydat
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
první	první	k4xOgFnSc4	první
ucelenou	ucelený	k2eAgFnSc4d1	ucelená
monografii	monografie	k1gFnSc4	monografie
o	o	k7c6	o
masožravých	masožravý	k2eAgFnPc6d1	masožravá
rostlinách	rostlina	k1gFnPc6	rostlina
Insectivorous	Insectivorous	k1gMnSc1	Insectivorous
Plants	Plants	k1gInSc1	Plants
<g/>
.	.	kIx.	.
</s>
<s>
Publikace	publikace	k1gFnSc1	publikace
byla	být	k5eAaImAgFnS	být
přeložena	přeložit	k5eAaPmNgFnS	přeložit
do	do	k7c2	do
francouzštiny	francouzština	k1gFnSc2	francouzština
a	a	k8xC	a
němčiny	němčina	k1gFnSc2	němčina
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
systematického	systematický	k2eAgInSc2d1	systematický
vědeckého	vědecký	k2eAgInSc2d1	vědecký
výzkumu	výzkum	k1gInSc2	výzkum
masožravých	masožravý	k2eAgFnPc2d1	masožravá
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
spolek	spolek	k1gInSc1	spolek
pěstitelů	pěstitel	k1gMnPc2	pěstitel
masožravých	masožravý	k2eAgFnPc2d1	masožravá
rostlin	rostlina	k1gFnPc2	rostlina
se	se	k3xPyFc4	se
na	na	k7c4	na
Darwinovu	Darwinův	k2eAgFnSc4d1	Darwinova
počest	počest	k1gFnSc4	počest
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
Darwiniana	Darwinian	k1gMnSc4	Darwinian
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Původní	původní	k2eAgNnPc4d1	původní
díla	dílo	k1gNnPc4	dílo
<g/>
:	:	kIx,	:
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Žížaly	žížala	k1gFnSc2	žížala
<g/>
.	.	kIx.	.
</s>
<s>
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
Darwin	Darwin	k1gMnSc1	Darwin
se	se	k3xPyFc4	se
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
věnoval	věnovat	k5eAaImAgMnS	věnovat
studiu	studio	k1gNnSc3	studio
činnosti	činnost	k1gFnSc2	činnost
žížal	žížala	k1gFnPc2	žížala
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
humusu	humus	k1gInSc2	humus
jílovitého	jílovitý	k2eAgInSc2d1	jílovitý
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
pokusný	pokusný	k2eAgInSc1d1	pokusný
pozemek	pozemek	k1gInSc1	pozemek
ponechal	ponechat	k5eAaPmAgInS	ponechat
po	po	k7c4	po
30	[number]	k4	30
sezon	sezona	k1gFnPc2	sezona
(	(	kIx(	(
<g/>
1842	[number]	k4	1842
<g/>
-	-	kIx~	-
<g/>
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
vlivu	vliv	k1gInSc2	vliv
působení	působení	k1gNnSc2	působení
žížal	žížala	k1gFnPc2	žížala
bez	bez	k7c2	bez
zásahu	zásah	k1gInSc2	zásah
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
ústřižků	ústřižek	k1gInPc2	ústřižek
papíru	papír	k1gInSc2	papír
sledoval	sledovat	k5eAaImAgMnS	sledovat
zatahování	zatahování	k1gNnSc4	zatahování
listů	list	k1gInPc2	list
žížalami	žížala	k1gFnPc7	žížala
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
kvantifikovat	kvantifikovat	k5eAaBmF	kvantifikovat
vliv	vliv	k1gInSc4	vliv
žížal	žížala	k1gFnPc2	žížala
na	na	k7c4	na
půdotvorný	půdotvorný	k2eAgInSc4d1	půdotvorný
proces	proces	k1gInSc4	proces
<g/>
.	.	kIx.	.
</s>
<s>
Sbíral	sbírat	k5eAaImAgInS	sbírat
a	a	k8xC	a
vážil	vážit	k5eAaImAgInS	vážit
exkrementy	exkrement	k1gInPc4	exkrement
žížal	žížala	k1gFnPc2	žížala
na	na	k7c6	na
dané	daný	k2eAgFnSc6d1	daná
ploše	plocha	k1gFnSc6	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
zjištění	zjištění	k1gNnSc1	zjištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
žížaly	žížala	k1gFnPc1	žížala
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
významně	významně	k6eAd1	významně
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
humusu	humus	k1gInSc2	humus
v	v	k7c6	v
jílovitých	jílovitý	k2eAgFnPc6d1	jílovitá
půdách	půda	k1gFnPc6	půda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
až	až	k9	až
2,5	[number]	k4	2,5
kg	kg	kA	kg
humusu	humus	k1gInSc2	humus
na	na	k7c6	na
m2	m2	k4	m2
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
vrstvu	vrstva	k1gFnSc4	vrstva
3	[number]	k4	3
mm	mm	kA	mm
silnou	silný	k2eAgFnSc7d1	silná
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Původní	původní	k2eAgNnPc4d1	původní
díla	dílo	k1gNnPc4	dílo
<g/>
:	:	kIx,	:
1881	[number]	k4	1881
<g/>
)	)	kIx)	)
Během	během	k7c2	během
plavby	plavba	k1gFnSc2	plavba
na	na	k7c6	na
Beagle	Beagla	k1gFnSc6	Beagla
zastával	zastávat	k5eAaImAgMnS	zastávat
Darwin	Darwin	k1gMnSc1	Darwin
poněkud	poněkud	k6eAd1	poněkud
ortodoxní	ortodoxní	k2eAgMnPc4d1	ortodoxní
názory	názor	k1gInPc4	názor
a	a	k8xC	a
citoval	citovat	k5eAaBmAgMnS	citovat
bibli	bible	k1gFnSc4	bible
jako	jako	k8xC	jako
vzor	vzor	k1gInSc4	vzor
morálky	morálka	k1gFnSc2	morálka
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
však	však	k9	však
považoval	považovat	k5eAaImAgMnS	považovat
poselství	poselství	k1gNnSc4	poselství
Starého	Starého	k2eAgInSc2d1	Starého
zákona	zákon	k1gInSc2	zákon
za	za	k7c4	za
falešné	falešný	k2eAgInPc4d1	falešný
a	a	k8xC	a
nevěrohodné	věrohodný	k2eNgInPc4d1	nevěrohodný
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
plavby	plavba	k1gFnSc2	plavba
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
svou	svůj	k3xOyFgFnSc4	svůj
teorii	teorie	k1gFnSc4	teorie
přirozeného	přirozený	k2eAgInSc2d1	přirozený
výběru	výběr	k1gInSc2	výběr
<g/>
.	.	kIx.	.
</s>
<s>
Věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
přírodovědci	přírodovědec	k1gMnPc1	přírodovědec
mezi	mezi	k7c7	mezi
duchovními	duchovní	k2eAgInPc7d1	duchovní
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
jeho	jeho	k3xOp3gMnPc1	jeho
přátelé	přítel	k1gMnPc1	přítel
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
přesvědčeni	přesvědčit	k5eAaPmNgMnP	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
bestiální	bestiální	k2eAgNnSc1d1	bestiální
kacířství	kacířství	k1gNnSc1	kacířství
<g/>
,	,	kIx,	,
podkopávající	podkopávající	k2eAgNnPc4d1	podkopávající
zázračná	zázračný	k2eAgNnPc4d1	zázračné
ospravedlnění	ospravedlnění	k1gNnPc4	ospravedlnění
pro	pro	k7c4	pro
společnost	společnost	k1gFnSc4	společnost
a	a	k8xC	a
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
takové	takový	k3xDgFnPc1	takový
revoluční	revoluční	k2eAgFnPc1d1	revoluční
myšlenky	myšlenka	k1gFnPc1	myšlenka
jsou	být	k5eAaImIp3nP	být
zvláště	zvláště	k6eAd1	zvláště
nevítané	vítaný	k2eNgFnPc1d1	nevítaná
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pozice	pozice	k1gFnSc1	pozice
církve	církev	k1gFnSc2	církev
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
útokem	útok	k1gInSc7	útok
radikálních	radikální	k2eAgMnPc2d1	radikální
Nesouhlasících	souhlasící	k2eNgMnPc2d1	nesouhlasící
a	a	k8xC	a
ateistů	ateista	k1gMnPc2	ateista
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
tajně	tajně	k6eAd1	tajně
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
teorii	teorie	k1gFnSc6	teorie
přirozeného	přirozený	k2eAgInSc2d1	přirozený
výběru	výběr	k1gInSc2	výběr
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
psal	psát	k5eAaImAgInS	psát
o	o	k7c6	o
náboženství	náboženství	k1gNnSc6	náboženství
jako	jako	k8xS	jako
o	o	k7c6	o
domorodé	domorodý	k2eAgFnSc6d1	domorodá
strategii	strategie	k1gFnSc6	strategie
přežití	přežití	k1gNnSc2	přežití
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bůh	bůh	k1gMnSc1	bůh
je	být	k5eAaImIp3nS	být
ten	ten	k3xDgMnSc1	ten
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
zákonodárce	zákonodárce	k1gMnSc1	zákonodárce
<g/>
.	.	kIx.	.
</s>
<s>
Darwinova	Darwinův	k2eAgFnSc1d1	Darwinova
víra	víra	k1gFnSc1	víra
v	v	k7c6	v
křesťanství	křesťanství	k1gNnSc6	křesťanství
postupně	postupně	k6eAd1	postupně
časem	časem	k6eAd1	časem
slábla	slábnout	k5eAaImAgFnS	slábnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
se	s	k7c7	s
smrtí	smrt	k1gFnSc7	smrt
dcery	dcera	k1gFnSc2	dcera
Annie	Annie	k1gFnSc2	Annie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1851	[number]	k4	1851
nakonec	nakonec	k9	nakonec
vyhasla	vyhasnout	k5eAaPmAgFnS	vyhasnout
zcela	zcela	k6eAd1	zcela
<g/>
.	.	kIx.	.
</s>
<s>
Sice	sice	k8xC	sice
nadále	nadále	k6eAd1	nadále
podporoval	podporovat	k5eAaImAgInS	podporovat
místní	místní	k2eAgInSc1d1	místní
kostel	kostel	k1gInSc1	kostel
a	a	k8xC	a
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
s	s	k7c7	s
farní	farní	k2eAgFnSc7d1	farní
prací	práce	k1gFnSc7	práce
<g/>
,	,	kIx,	,
o	o	k7c6	o
nedělích	neděle	k1gFnPc6	neděle
dával	dávat	k5eAaImAgMnS	dávat
přednost	přednost	k1gFnSc4	přednost
procházkám	procházka	k1gFnPc3	procházka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
tázán	tázat	k5eAaImNgMnS	tázat
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
víře	víra	k1gFnSc3	víra
<g/>
,	,	kIx,	,
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgMnS	být
ateista	ateista	k1gMnSc1	ateista
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
popření	popření	k1gNnSc2	popření
existence	existence	k1gFnSc2	existence
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
říci	říct	k5eAaPmF	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Agnostický	agnostický	k2eAgMnSc1d1	agnostický
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
správnější	správní	k2eAgInSc1d2	správnější
popis	popis	k1gInSc1	popis
mého	můj	k3xOp1gInSc2	můj
duševního	duševní	k2eAgInSc2d1	duševní
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
životopisu	životopis	k1gInSc6	životopis
popřel	popřít	k5eAaPmAgMnS	popřít
falešné	falešný	k2eAgInPc4d1	falešný
příběhy	příběh	k1gInPc4	příběh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
kolovaly	kolovat	k5eAaImAgInP	kolovat
o	o	k7c6	o
jeho	jeho	k3xOp3gMnSc6	jeho
dědečkovi	dědeček	k1gMnSc6	dědeček
Erasmovi	Erasm	k1gMnSc6	Erasm
Darwinovi	Darwin	k1gMnSc6	Darwin
a	a	k8xC	a
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
se	se	k3xPyFc4	se
tvrdilo	tvrdit	k5eAaImAgNnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Erasmus	Erasmus	k1gMnSc1	Erasmus
na	na	k7c6	na
smrtelné	smrtelný	k2eAgFnSc6d1	smrtelná
posteli	postel	k1gFnSc6	postel
volal	volat	k5eAaImAgMnS	volat
pro	pro	k7c4	pro
Ježíše	Ježíš	k1gMnSc4	Ježíš
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
své	svůj	k3xOyFgNnSc4	svůj
psaní	psaní	k1gNnSc4	psaní
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
Takový	takový	k3xDgInSc4	takový
byl	být	k5eAaImAgInS	být
stav	stav	k1gInSc1	stav
křesťanství	křesťanství	k1gNnSc2	křesťanství
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
zemi	zem	k1gFnSc6	zem
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1802	[number]	k4	1802
<g/>
)	)	kIx)	)
...	...	k?	...
Můžeme	moct	k5eAaImIp1nP	moct
přinejmenším	přinejmenším	k6eAd1	přinejmenším
doufat	doufat	k5eAaImF	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nic	nic	k3yNnSc1	nic
takového	takový	k3xDgNnSc2	takový
dnes	dnes	k6eAd1	dnes
nezvítězí	zvítězit	k5eNaPmIp3nS	zvítězit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Navzdory	navzdory	k7c3	navzdory
tomuto	tento	k3xDgNnSc3	tento
očekávání	očekávání	k1gNnSc3	očekávání
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
i	i	k9	i
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Charlese	Charles	k1gMnSc2	Charles
Darwina	Darwin	k1gMnSc2	Darwin
kolovaly	kolovat	k5eAaImAgFnP	kolovat
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgInPc1d1	podobný
příběhy	příběh	k1gInPc1	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
příběhy	příběh	k1gInPc1	příběh
byly	být	k5eAaImAgInP	být
natolik	natolik	k6eAd1	natolik
silně	silně	k6eAd1	silně
propagovány	propagován	k2eAgFnPc1d1	propagována
některými	některý	k3yIgFnPc7	některý
křesťanskými	křesťanský	k2eAgFnPc7d1	křesťanská
skupinami	skupina	k1gFnPc7	skupina
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stávaly	stávat	k5eAaImAgFnP	stávat
legendami	legenda	k1gFnPc7	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
tvrzení	tvrzení	k1gNnSc3	tvrzení
byla	být	k5eAaImAgFnS	být
vyvrácena	vyvrátit	k5eAaPmNgFnS	vyvrátit
Darwinovými	Darwinův	k2eAgFnPc7d1	Darwinova
dětmi	dítě	k1gFnPc7	dítě
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
též	též	k9	též
dějepisci	dějepisec	k1gMnSc3	dějepisec
označena	označen	k2eAgFnSc1d1	označena
za	za	k7c4	za
falešná	falešný	k2eAgNnPc4d1	falešné
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
názory	názor	k1gInPc1	názor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
zastával	zastávat	k5eAaImAgMnS	zastávat
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
daly	dát	k5eAaPmAgFnP	dát
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
rasistické	rasistický	k2eAgNnSc4d1	rasistické
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
práci	práce	k1gFnSc6	práce
Původ	původ	k1gInSc4	původ
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Descent	Descent	k1gMnSc1	Descent
of	of	k?	of
Man	Man	k1gMnSc1	Man
<g/>
)	)	kIx)	)
teoretizuje	teoretizovat	k5eAaImIp3nS	teoretizovat
Darwin	Darwin	k1gMnSc1	Darwin
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
úspěch	úspěch	k1gInSc1	úspěch
v	v	k7c6	v
konkurenčním	konkurenční	k2eAgInSc6d1	konkurenční
boji	boj	k1gInSc6	boj
o	o	k7c4	o
přežití	přežití	k1gNnSc4	přežití
zvýhodnil	zvýhodnit	k5eAaPmAgMnS	zvýhodnit
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
rasy	rasa	k1gFnPc4	rasa
podle	podle	k7c2	podle
"	"	kIx"	"
<g/>
stupně	stupeň	k1gInSc2	stupeň
jejich	jejich	k3xOp3gFnSc2	jejich
civilizovanosti	civilizovanost	k1gFnSc2	civilizovanost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jasně	jasně	k6eAd1	jasně
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
primitivní	primitivní	k2eAgInPc4d1	primitivní
národy	národ	k1gInPc4	národ
světa	svět	k1gInSc2	svět
by	by	kYmCp3nS	by
nakonec	nakonec	k6eAd1	nakonec
mělo	mít	k5eAaImAgNnS	mít
čekat	čekat	k5eAaImF	čekat
vyhynutí	vyhynutí	k1gNnSc1	vyhynutí
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
Evoluční	evoluční	k2eAgFnSc1d1	evoluční
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
vyhlášená	vyhlášený	k2eAgFnSc1d1	vyhlášená
Darwinem	Darwin	k1gMnSc7	Darwin
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vrcholícího	vrcholící	k2eAgInSc2d1	vrcholící
britského	britský	k2eAgInSc2d1	britský
kolonialismu	kolonialismus	k1gInSc2	kolonialismus
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Evropany	Evropan	k1gMnPc4	Evropan
zneužívána	zneužíván	k2eAgFnSc1d1	zneužívána
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
jednodušeji	jednoduše	k6eAd2	jednoduše
dařilo	dařit	k5eAaImAgNnS	dařit
odlidšťovat	odlidšťovat	k5eAaImF	odlidšťovat
jiné	jiný	k2eAgFnPc4d1	jiná
rasy	rasa	k1gFnPc4	rasa
a	a	k8xC	a
obhájit	obhájit	k5eAaPmF	obhájit
tak	tak	k9	tak
hierarchii	hierarchie	k1gFnSc4	hierarchie
pokroku	pokrok	k1gInSc2	pokrok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
"	"	kIx"	"
<g/>
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
"	"	kIx"	"
forma	forma	k1gFnSc1	forma
má	mít	k5eAaImIp3nS	mít
přirozené	přirozený	k2eAgNnSc4d1	přirozené
právo	právo	k1gNnSc4	právo
ovládat	ovládat	k5eAaImF	ovládat
formu	forma	k1gFnSc4	forma
"	"	kIx"	"
<g/>
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
"	"	kIx"	"
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začali	začít	k5eAaPmAgMnP	začít
někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
chápat	chápat	k5eAaImF	chápat
pojem	pojem	k1gInSc4	pojem
darwinismus	darwinismus	k1gInSc4	darwinismus
nejen	nejen	k6eAd1	nejen
jako	jako	k8xS	jako
empirickou	empirický	k2eAgFnSc4d1	empirická
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
jako	jako	k9	jako
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
rasové	rasový	k2eAgNnSc1d1	rasové
zdokonalování	zdokonalování	k1gNnSc1	zdokonalování
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
boje	boj	k1gInSc2	boj
o	o	k7c4	o
přežití	přežití	k1gNnSc4	přežití
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rasismus	rasismus	k1gInSc1	rasismus
dovedl	dovést	k5eAaPmAgInS	dovést
do	do	k7c2	do
extrému	extrém	k1gInSc2	extrém
nadšený	nadšený	k2eAgMnSc1d1	nadšený
obdivovatel	obdivovatel	k1gMnSc1	obdivovatel
Charlese	Charles	k1gMnSc2	Charles
Darwina	Darwin	k1gMnSc2	Darwin
Ernst	Ernst	k1gMnSc1	Ernst
Haeckel	Haeckel	k1gMnSc1	Haeckel
<g/>
.	.	kIx.	.
</s>
<s>
Ernst	Ernst	k1gMnSc1	Ernst
Haeckel	Haeckel	k1gMnSc1	Haeckel
se	se	k3xPyFc4	se
dál	daleko	k6eAd2	daleko
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
Darwinovo	Darwinův	k2eAgNnSc4d1	Darwinovo
dílo	dílo	k1gNnSc4	dílo
a	a	k8xC	a
také	také	k9	také
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
přišel	přijít	k5eAaPmAgInS	přijít
s	s	k7c7	s
onou	onen	k3xDgFnSc7	onen
značně	značně	k6eAd1	značně
nepřesnou	přesný	k2eNgFnSc7d1	nepřesná
<g/>
,	,	kIx,	,
zjednodušující	zjednodušující	k2eAgFnSc7d1	zjednodušující
a	a	k8xC	a
zavádějící	zavádějící	k2eAgFnSc7d1	zavádějící
formulací	formulace	k1gFnSc7	formulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
člověk	člověk	k1gMnSc1	člověk
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
opice	opice	k1gFnSc2	opice
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zastánce	zastánce	k1gMnSc4	zastánce
eugeniky	eugenika	k1gFnSc2	eugenika
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
filozof	filozof	k1gMnSc1	filozof
Ernst	Ernst	k1gMnSc1	Ernst
Haeckel	Haeckel	k1gInSc4	Haeckel
uznávanou	uznávaný	k2eAgFnSc7d1	uznávaná
osobností	osobnost	k1gFnSc7	osobnost
a	a	k8xC	a
eugenika	eugenika	k1gFnSc1	eugenika
zase	zase	k9	zase
získala	získat	k5eAaPmAgFnS	získat
značnou	značný	k2eAgFnSc4d1	značná
popularitu	popularita	k1gFnSc4	popularita
především	především	k9	především
v	v	k7c6	v
nacistickém	nacistický	k2eAgNnSc6d1	nacistické
Německu	Německo	k1gNnSc6	Německo
(	(	kIx(	(
ale	ale	k8xC	ale
samozřejmě	samozřejmě	k6eAd1	samozřejmě
nejen	nejen	k6eAd1	nejen
tam	tam	k6eAd1	tam
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
eugenika	eugenika	k1gFnSc1	eugenika
byla	být	k5eAaImAgFnS	být
běžně	běžně	k6eAd1	běžně
známa	znám	k2eAgFnSc1d1	známa
i	i	k8xC	i
mezi	mezi	k7c7	mezi
řadovými	řadový	k2eAgMnPc7d1	řadový
Němci	Němec	k1gMnPc7	Němec
<g/>
,	,	kIx,	,
svědčí	svědčit	k5eAaImIp3nP	svědčit
i	i	k9	i
následující	následující	k2eAgFnPc1d1	následující
citace	citace	k1gFnPc1	citace
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgMnPc1d1	pocházející
z	z	k7c2	z
publikace	publikace	k1gFnSc2	publikace
vydané	vydaný	k2eAgFnSc2d1	vydaná
v	v	k7c6	v
nacistickém	nacistický	k2eAgNnSc6d1	nacistické
Německu	Německo	k1gNnSc6	Německo
jednou	jednou	k6eAd1	jednou
společností	společnost	k1gFnPc2	společnost
chovatelů	chovatel	k1gMnPc2	chovatel
králíků	králík	k1gMnPc2	králík
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
a	a	k8xC	a
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
:	:	kIx,	:
"	"	kIx"	"
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
co	co	k9	co
se	se	k3xPyFc4	se
národní	národní	k2eAgMnPc1d1	národní
socialisté	socialist	k1gMnPc1	socialist
chopili	chopit	k5eAaPmAgMnP	chopit
moci	moct	k5eAaImF	moct
<g/>
,	,	kIx,	,
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
křížení	křížení	k1gNnSc1	křížení
králíků	králík	k1gMnPc2	králík
neslýchaného	slýchaný	k2eNgInSc2d1	neslýchaný
pokroku	pokrok	k1gInSc2	pokrok
<g/>
...	...	k?	...
<g/>
a	a	k8xC	a
získalo	získat	k5eAaPmAgNnS	získat
nový	nový	k2eAgInSc4d1	nový
cíl	cíl	k1gInSc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
jím	on	k3xPp3gNnSc7	on
zabývají	zabývat	k5eAaImIp3nP	zabývat
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
náhle	náhle	k6eAd1	náhle
naplněni	naplnit	k5eAaPmNgMnP	naplnit
novými	nový	k2eAgInPc7d1	nový
organizačními	organizační	k2eAgInPc7d1	organizační
a	a	k8xC	a
eugenickými	eugenický	k2eAgInPc7d1	eugenický
impulsy	impuls	k1gInPc7	impuls
<g/>
"	"	kIx"	"
.	.	kIx.	.
</s>
<s>
Nacisté	nacista	k1gMnPc1	nacista
záměrně	záměrně	k6eAd1	záměrně
pokřivili	pokřivit	k5eAaPmAgMnP	pokřivit
darwinovskou	darwinovský	k2eAgFnSc4d1	darwinovská
biologii	biologie	k1gFnSc4	biologie
a	a	k8xC	a
snažili	snažit	k5eAaImAgMnP	snažit
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
zneužít	zneužít	k5eAaPmF	zneužít
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
rasistické	rasistický	k2eAgFnPc4d1	rasistická
teorie	teorie	k1gFnPc4	teorie
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
nevědecké	vědecký	k2eNgNnSc1d1	nevědecké
a	a	k8xC	a
metafyzické	metafyzický	k2eAgNnSc1d1	metafyzické
překroucení	překroucení	k1gNnSc1	překroucení
Evoluční	evoluční	k2eAgFnSc2d1	evoluční
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
označované	označovaný	k2eAgFnSc2d1	označovaná
jako	jako	k8xC	jako
etnicko-nacionální	etnickoacionální	k2eAgFnSc2d1	etnicko-nacionální
ideologie	ideologie	k1gFnSc2	ideologie
<g/>
,	,	kIx,	,
použili	použít	k5eAaPmAgMnP	použít
nacisté	nacista	k1gMnPc1	nacista
k	k	k7c3	k
oslavě	oslava	k1gFnSc3	oslava
boje	boj	k1gInSc2	boj
o	o	k7c4	o
přežití	přežití	k1gNnSc4	přežití
<g/>
,	,	kIx,	,
konkurenčního	konkurenční	k2eAgInSc2d1	konkurenční
boje	boj	k1gInSc2	boj
mezi	mezi	k7c7	mezi
národy	národ	k1gInPc7	národ
<g/>
,	,	kIx,	,
pohrdání	pohrdání	k1gNnSc6	pohrdání
slabými	slabý	k2eAgFnPc7d1	slabá
a	a	k8xC	a
nadřazenosti	nadřazenost	k1gFnSc3	nadřazenost
jedné	jeden	k4xCgFnSc2	jeden
lidské	lidský	k2eAgFnSc2d1	lidská
rasy	rasa	k1gFnSc2	rasa
nad	nad	k7c7	nad
druhou	druhý	k4xOgFnSc7	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Nacisté	nacista	k1gMnPc1	nacista
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
jednoznačně	jednoznačně	k6eAd1	jednoznačně
nepřijali	přijmout	k5eNaPmAgMnP	přijmout
tu	ten	k3xDgFnSc4	ten
část	část	k1gFnSc4	část
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vztahují	vztahovat	k5eAaImIp3nP	vztahovat
k	k	k7c3	k
původu	původ	k1gInSc3	původ
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc2	jeho
vyvinutí	vyvinutí	k1gNnPc2	vyvinutí
se	se	k3xPyFc4	se
z	z	k7c2	z
nižších	nízký	k2eAgFnPc2d2	nižší
živočišných	živočišný	k2eAgFnPc2d1	živočišná
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nacistickou	nacistický	k2eAgFnSc4d1	nacistická
ideologii	ideologie	k1gFnSc4	ideologie
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
ovlivněnou	ovlivněný	k2eAgFnSc4d1	ovlivněná
mysticismem	mysticismus	k1gInSc7	mysticismus
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Darwinova	Darwinův	k2eAgFnSc1d1	Darwinova
vědecká	vědecký	k2eAgFnSc1d1	vědecká
teorie	teorie	k1gFnSc1	teorie
přitažlivá	přitažlivý	k2eAgFnSc1d1	přitažlivá
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
co	co	k3yQnSc1	co
do	do	k7c2	do
struktury	struktura	k1gFnSc2	struktura
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
něčemu	něco	k3yInSc3	něco
zcela	zcela	k6eAd1	zcela
nevědeckému	vědecký	k2eNgMnSc3d1	nevědecký
a	a	k8xC	a
primitivnímu	primitivní	k2eAgMnSc3d1	primitivní
-	-	kIx~	-
totiž	totiž	k8xC	totiž
totemismu	totemismus	k1gInSc2	totemismus
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
je	být	k5eAaImIp3nS	být
původ	původ	k1gInSc1	původ
každého	každý	k3xTgInSc2	každý
kmene	kmen	k1gInSc2	kmen
odvozován	odvozovat	k5eAaImNgInS	odvozovat
od	od	k7c2	od
posvátného	posvátný	k2eAgNnSc2d1	posvátné
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Němce	Němec	k1gMnSc4	Němec
však	však	k9	však
byla	být	k5eAaImAgFnS	být
nepřijatelná	přijatelný	k2eNgFnSc1d1	nepřijatelná
představa	představa	k1gFnSc1	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
tímto	tento	k3xDgNnSc7	tento
totemickým	totemický	k2eAgNnSc7d1	totemické
zvířetem	zvíře	k1gNnSc7	zvíře
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgNnP	mít
být	být	k5eAaImF	být
právě	právě	k9	právě
opice	opice	k1gFnSc1	opice
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
to	ten	k3xDgNnSc1	ten
naznačoval	naznačovat	k5eAaImAgMnS	naznačovat
darwinista	darwinista	k1gMnSc1	darwinista
Ernst	Ernst	k1gMnSc1	Ernst
Haeckel	Haeckel	k1gMnSc1	Haeckel
<g/>
.	.	kIx.	.
</s>
<s>
Nacisté	nacista	k1gMnPc1	nacista
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
přikláněli	přiklánět	k5eAaImAgMnP	přiklánět
k	k	k7c3	k
nevědeckému	vědecký	k2eNgNnSc3d1	nevědecké
vysvětlení	vysvětlení	k1gNnSc3	vysvětlení
<g/>
,	,	kIx,	,
známému	známý	k2eAgNnSc3d1	známé
jako	jako	k8xC	jako
polygeneze	polygeneze	k1gFnSc1	polygeneze
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
východiskem	východisko	k1gNnSc7	východisko
byla	být	k5eAaImAgFnS	být
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
různé	různý	k2eAgFnPc1d1	různá
lidské	lidský	k2eAgFnPc1d1	lidská
rasy	rasa	k1gFnPc1	rasa
byly	být	k5eAaImAgFnP	být
stvořeny	stvořit	k5eAaPmNgFnP	stvořit
odděleně	odděleně	k6eAd1	odděleně
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
pak	pak	k6eAd1	pak
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
třeba	třeba	k6eAd1	třeba
zrovna	zrovna	k6eAd1	zrovna
Germáni	Germán	k1gMnPc1	Germán
by	by	kYmCp3nS	by
mohli	moct	k5eAaImAgMnP	moct
být	být	k5eAaImF	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
evoluce	evoluce	k1gFnSc2	evoluce
potomky	potomek	k1gMnPc4	potomek
opic	opice	k1gFnPc2	opice
-	-	kIx~	-
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
projevu	projev	k1gInSc6	projev
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Odmyslete	odmyslet	k5eAaPmRp2nP	odmyslet
si	se	k3xPyFc3	se
nordické	nordický	k2eAgFnSc2d1	nordická
Němce	Němka	k1gFnSc3	Němka
a	a	k8xC	a
nezbyde	zbýt	k5eNaPmIp3nS	zbýt
vám	vy	k3xPp2nPc3	vy
nic	nic	k3yNnSc1	nic
<g/>
,	,	kIx,	,
než	než	k8xS	než
jen	jen	k9	jen
tanec	tanec	k1gInSc1	tanec
opic	opice	k1gFnPc2	opice
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
výrokem	výrok	k1gInSc7	výrok
navázal	navázat	k5eAaPmAgMnS	navázat
na	na	k7c4	na
antisemitské	antisemitský	k2eAgInPc4d1	antisemitský
názory	názor	k1gInPc4	názor
<g/>
,	,	kIx,	,
předcházející	předcházející	k2eAgFnSc4d1	předcházející
nacismu	nacismus	k1gInSc2	nacismus
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
např.	např.	kA	např.
Richard	Richard	k1gMnSc1	Richard
Wagner	Wagner	k1gMnSc1	Wagner
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
nižší	nízký	k2eAgFnPc1d2	nižší
<g/>
"	"	kIx"	"
rasy	rasa	k1gFnPc1	rasa
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
opic	opice	k1gFnPc2	opice
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Árijci	Árijec	k1gMnPc1	Árijec
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
od	od	k7c2	od
bohů	bůh	k1gMnPc2	bůh
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1839	[number]	k4	1839
se	se	k3xPyFc4	se
Darwin	Darwin	k1gMnSc1	Darwin
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
sestřenicí	sestřenice	k1gFnSc7	sestřenice
Emmou	Emma	k1gFnSc7	Emma
Wedgwoodovou	Wedgwoodový	k2eAgFnSc4d1	Wedgwoodový
v	v	k7c4	v
Maer	Maer	k1gInSc4	Maer
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
anglikánský	anglikánský	k2eAgInSc1d1	anglikánský
obřad	obřad	k1gInSc1	obřad
byl	být	k5eAaImAgInS	být
uspořádán	uspořádat	k5eAaPmNgInS	uspořádat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyhovoval	vyhovovat	k5eAaImAgMnS	vyhovovat
i	i	k9	i
unitářům	unitář	k1gMnPc3	unitář
<g/>
.	.	kIx.	.
</s>
<s>
Novomanželé	novomanžel	k1gMnPc1	novomanžel
nejprve	nejprve	k6eAd1	nejprve
bydleli	bydlet	k5eAaImAgMnP	bydlet
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Gower	Gowra	k1gFnPc2	Gowra
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1842	[number]	k4	1842
se	se	k3xPyFc4	se
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
do	do	k7c2	do
Down	Downa	k1gFnPc2	Downa
House	house	k1gNnSc4	house
v	v	k7c6	v
Downe	Down	k1gInSc5	Down
(	(	kIx(	(
<g/>
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
od	od	k7c2	od
Orpingtonu	Orpington	k1gInSc2	Orpington
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
otevřený	otevřený	k2eAgInSc1d1	otevřený
pro	pro	k7c4	pro
návštěvy	návštěva	k1gFnPc4	návštěva
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Darwinovi	Darwin	k1gMnSc3	Darwin
měli	mít	k5eAaImAgMnP	mít
deset	deset	k4xCc4	deset
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
tři	tři	k4xCgFnPc1	tři
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
zemřely	zemřít	k5eAaPmAgFnP	zemřít
velmi	velmi	k6eAd1	velmi
brzy	brzy	k6eAd1	brzy
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
jejich	jejich	k3xOp3gFnPc2	jejich
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
vnoučat	vnouče	k1gNnPc2	vnouče
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
také	také	k9	také
proslavilo	proslavit	k5eAaPmAgNnS	proslavit
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k1gInSc1	William
Erasmus	Erasmus	k1gInSc1	Erasmus
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1839	[number]	k4	1839
<g/>
-	-	kIx~	-
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
Anna	Anna	k1gFnSc1	Anna
Elisabeth	Elisabeth	k1gMnSc1	Elisabeth
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1841	[number]	k4	1841
-	-	kIx~	-
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1851	[number]	k4	1851
<g/>
)	)	kIx)	)
Marie	Marie	k1gFnSc1	Marie
Eleonora	Eleonora	k1gFnSc1	Eleonora
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1842	[number]	k4	1842
-	-	kIx~	-
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1842	[number]	k4	1842
<g/>
)	)	kIx)	)
Henrietta	Henriett	k1gInSc2	Henriett
<g />
.	.	kIx.	.
</s>
<s>
Emma	Emma	k1gFnSc1	Emma
"	"	kIx"	"
<g/>
Etty	Etty	k1gInPc1	Etty
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1843	[number]	k4	1843
<g/>
-	-	kIx~	-
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
George	Georg	k1gInSc2	Georg
Howard	Howard	k1gInSc1	Howard
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1845	[number]	k4	1845
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
Elisabeth	Elisabeth	k1gInSc1	Elisabeth
"	"	kIx"	"
<g/>
Bessy	Bessa	k1gFnPc1	Bessa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1847	[number]	k4	1847
<g/>
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
Francis	Francis	k1gFnSc2	Francis
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1848	[number]	k4	1848
-	-	kIx~	-
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
Leonard	Leonard	k1gMnSc1	Leonard
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1850	[number]	k4	1850
-	-	kIx~	-
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
Horace	Horace	k1gFnSc2	Horace
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1851	[number]	k4	1851
-	-	kIx~	-
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g />
.	.	kIx.	.
</s>
<s>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
Charles	Charles	k1gMnSc1	Charles
Waring	Waring	k1gInSc1	Waring
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1856	[number]	k4	1856
-	-	kIx~	-
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1858	[number]	k4	1858
<g/>
)	)	kIx)	)
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
jejich	jejich	k3xOp3gFnPc2	jejich
dětí	dítě	k1gFnPc2	dítě
trpěly	trpět	k5eAaImAgFnP	trpět
nemocemi	nemoc	k1gFnPc7	nemoc
nebo	nebo	k8xC	nebo
slabostmi	slabost	k1gFnPc7	slabost
a	a	k8xC	a
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
se	se	k3xPyFc4	se
obával	obávat	k5eAaImAgMnS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
blízkým	blízký	k2eAgInSc7d1	blízký
příbuzenským	příbuzenský	k2eAgInSc7d1	příbuzenský
vztahem	vztah	k1gInSc7	vztah
jeho	jeho	k3xOp3gInSc2	jeho
rodokmenu	rodokmen	k1gInSc2	rodokmen
a	a	k8xC	a
rodokmenu	rodokmen	k1gInSc2	rodokmen
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
spisech	spis	k1gInPc6	spis
v	v	k7c6	v
pojednáních	pojednání	k1gNnPc6	pojednání
o	o	k7c6	o
škodlivých	škodlivý	k2eAgInPc6d1	škodlivý
následcích	následek	k1gInPc6	následek
příbuzenského	příbuzenský	k2eAgNnSc2d1	příbuzenské
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
a	a	k8xC	a
výhodách	výhoda	k1gFnPc6	výhoda
křížení	křížení	k1gNnSc2	křížení
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgInSc1d1	vlastní
životopis	životopis	k1gInSc1	životopis
<g/>
,	,	kIx,	,
1950	[number]	k4	1950
(	(	kIx(	(
<g/>
Autobiography	Autobiographa	k1gFnSc2	Autobiographa
of	of	k?	of
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
<g/>
,	,	kIx,	,
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
O	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
druhů	druh	k1gInPc2	druh
přírodním	přírodní	k2eAgInSc7d1	přírodní
výběrem	výběr	k1gInSc7	výběr
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
(	(	kIx(	(
<g/>
On	on	k3xPp3gMnSc1	on
the	the	k?	the
Origin	Origin	k2eAgInSc4d1	Origin
of	of	k?	of
Species	species	k1gFnSc4	species
by	by	k9	by
Means	Means	k1gInSc4	Means
of	of	k?	of
Natural	Natural	k?	Natural
Selection	Selection	k1gInSc1	Selection
<g/>
,	,	kIx,	,
or	or	k?	or
the	the	k?	the
Preservation	Preservation	k1gInSc1	Preservation
of	of	k?	of
Favoured	Favoured	k1gInSc1	Favoured
Races	Races	k1gInSc1	Races
in	in	k?	in
the	the	k?	the
Struggle	Struggle	k1gFnSc1	Struggle
for	forum	k1gNnPc2	forum
Life	Life	k1gNnPc2	Life
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
1859	[number]	k4	1859
<g/>
)	)	kIx)	)
Cesta	cesta	k1gFnSc1	cesta
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
(	(	kIx(	(
<g/>
Journal	Journal	k1gFnSc1	Journal
and	and	k?	and
Remarks	Remarksa	k1gFnPc2	Remarksa
nebo	nebo	k8xC	nebo
The	The	k1gFnPc2	The
Voyage	Voyag	k1gFnSc2	Voyag
of	of	k?	of
the	the	k?	the
Beagle	Beagle	k1gInSc1	Beagle
<g/>
,	,	kIx,	,
1839	[number]	k4	1839
<g/>
)	)	kIx)	)
Cesta	cesta	k1gFnSc1	cesta
přírodozpytcova	přírodozpytcův	k2eAgFnSc1d1	přírodozpytcův
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
(	(	kIx(	(
<g/>
Journal	Journal	k1gFnSc1	Journal
and	and	k?	and
Remarks	Remarksa	k1gFnPc2	Remarksa
nebo	nebo	k8xC	nebo
The	The	k1gFnPc2	The
Voyage	Voyag	k1gFnSc2	Voyag
of	of	k?	of
the	the	k?	the
Beagle	Beagle	k1gInSc1	Beagle
<g/>
,	,	kIx,	,
1839	[number]	k4	1839
<g/>
)	)	kIx)	)
Výraz	výraz	k1gInSc1	výraz
<g />
.	.	kIx.	.
</s>
<s>
emocí	emoce	k1gFnSc7	emoce
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
u	u	k7c2	u
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Expression	Expression	k1gInSc1	Expression
of	of	k?	of
Emotions	Emotions	k1gInSc1	Emotions
in	in	k?	in
Man	mana	k1gFnPc2	mana
and	and	k?	and
Animals	Animalsa	k1gFnPc2	Animalsa
<g/>
,	,	kIx,	,
1872	[number]	k4	1872
<g/>
)	)	kIx)	)
O	o	k7c6	o
původu	původ	k1gInSc6	původ
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Descent	Descent	k1gMnSc1	Descent
of	of	k?	of
Man	Man	k1gMnSc1	Man
and	and	k?	and
Selection	Selection	k1gInSc1	Selection
in	in	k?	in
Relation	Relation	k1gInSc1	Relation
to	ten	k3xDgNnSc1	ten
Sex	sex	k1gInSc1	sex
<g/>
,	,	kIx,	,
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
O	o	k7c6	o
pohlavním	pohlavní	k2eAgInSc6d1	pohlavní
výběru	výběr	k1gInSc6	výběr
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
O	o	k7c6	o
původu	původ	k1gInSc6	původ
člověka	člověk	k1gMnSc2	člověk
<g/>
)	)	kIx)	)
1836	[number]	k4	1836
<g/>
:	:	kIx,	:
A	a	k8xC	a
LETTER	LETTER	kA	LETTER
<g/>
,	,	kIx,	,
Containing	Containing	k1gInSc4	Containing
Remarks	Remarksa	k1gFnPc2	Remarksa
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Moral	Moral	k1gMnSc1	Moral
State	status	k1gInSc5	status
of	of	k?	of
TAHITI	Tahiti	k1gNnPc2	Tahiti
<g/>
,	,	kIx,	,
NEW	NEW	kA	NEW
ZEALAND	ZEALAND	kA	ZEALAND
<g/>
,	,	kIx,	,
&	&	k?	&
<g/>
c.	c.	k?	c.
-	-	kIx~	-
BY	by	k9	by
CAPT	CAPT	kA	CAPT
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
FITZROY	FITZROY	kA	FITZROY
AND	Anda	k1gFnPc2	Anda
C.	C.	kA	C.
DARWIN	Darwin	k1gMnSc1	Darwin
<g/>
,	,	kIx,	,
ESQ	ESQ	kA	ESQ
<g/>
.	.	kIx.	.
</s>
<s>
OF	OF	kA	OF
H.	H.	kA	H.
<g/>
M.	M.	kA	M.
<g/>
S.	S.	kA	S.
'	'	kIx"	'
<g/>
Beagle	Beagl	k1gMnSc2	Beagl
<g/>
.	.	kIx.	.
<g/>
'	'	kIx"	'
Zoology	zoolog	k1gMnPc4	zoolog
of	of	k?	of
the	the	k?	the
Voyage	Voyag	k1gFnSc2	Voyag
of	of	k?	of
H.	H.	kA	H.
<g/>
M.	M.	kA	M.
<g/>
S.	S.	kA	S.
Beagle	Beagl	k1gMnSc2	Beagl
<g/>
:	:	kIx,	:
publikováno	publikovat	k5eAaBmNgNnS	publikovat
mezi	mezi	k7c7	mezi
1839	[number]	k4	1839
a	a	k8xC	a
1843	[number]	k4	1843
v	v	k7c6	v
pěti	pět	k4xCc2	pět
částech	část	k1gFnPc6	část
různými	různý	k2eAgNnPc7d1	různé
spisovateli	spisovatel	k1gMnSc6	spisovatel
<g/>
,	,	kIx,	,
editováno	editován	k2eAgNnSc1d1	Editováno
a	a	k8xC	a
dohlíženo	dohlížen	k2eAgNnSc1d1	dohlíženo
Charlesem	Charles	k1gMnSc7	Charles
Darwinem	Darwin	k1gMnSc7	Darwin
1840	[number]	k4	1840
<g/>
:	:	kIx,	:
Part	part	k1gInSc1	part
I.	I.	kA	I.
Fossil	Fossil	k1gMnSc2	Fossil
Mammalia	Mammalius	k1gMnSc2	Mammalius
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
Richard	Richard	k1gMnSc1	Richard
Owen	Owen	k1gMnSc1	Owen
(	(	kIx(	(
<g/>
Darwin	Darwin	k1gMnSc1	Darwin
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
introduction	introduction	k1gInSc1	introduction
<g/>
)	)	kIx)	)
1839	[number]	k4	1839
<g/>
:	:	kIx,	:
Part	part	k1gInSc1	part
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Mammalia	Mammalia	k1gFnSc1	Mammalia
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
George	George	k1gFnSc1	George
R.	R.	kA	R.
Waterhouse	Waterhouse	k1gFnSc1	Waterhouse
(	(	kIx(	(
<g/>
Darwin	Darwin	k1gMnSc1	Darwin
on	on	k3xPp3gMnSc1	on
habits	habits	k6eAd1	habits
and	and	k?	and
ranges	ranges	k1gInSc1	ranges
<g/>
)	)	kIx)	)
1839	[number]	k4	1839
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Voyage	Voyag	k1gFnSc2	Voyag
of	of	k?	of
the	the	k?	the
Beagle	Beagle	k1gInSc1	Beagle
(	(	kIx(	(
<g/>
Journal	Journal	k1gFnSc1	Journal
and	and	k?	and
Remarks	Remarks	k1gInSc1	Remarks
<g/>
)	)	kIx)	)
1842	[number]	k4	1842
<g/>
:	:	kIx,	:
The	The	k1gMnSc5	The
Structure	Structur	k1gMnSc5	Structur
and	and	k?	and
Distribution	Distribution	k1gInSc1	Distribution
of	of	k?	of
Coral	Coral	k1gInSc1	Coral
Reefs	Reefs	k1gInSc1	Reefs
1844	[number]	k4	1844
<g/>
:	:	kIx,	:
Geological	Geological	k1gFnSc1	Geological
Observations	Observationsa	k1gFnPc2	Observationsa
of	of	k?	of
Volcanic	Volcanice	k1gFnPc2	Volcanice
Islands	Islandsa	k1gFnPc2	Islandsa
1846	[number]	k4	1846
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Geological	Geological	k1gFnPc1	Geological
Observations	Observations	k1gInSc4	Observations
on	on	k3xPp3gMnSc1	on
South	South	k1gMnSc1	South
America	Americ	k1gInSc2	Americ
1849	[number]	k4	1849
<g/>
:	:	kIx,	:
Geology	geolog	k1gMnPc7	geolog
from	from	k6eAd1	from
A	a	k9	a
Manual	Manual	k1gMnSc1	Manual
of	of	k?	of
scientific	scientific	k1gMnSc1	scientific
enquiry	enquira	k1gMnSc2	enquira
<g/>
;	;	kIx,	;
prepared	prepared	k1gMnSc1	prepared
for	forum	k1gNnPc2	forum
the	the	k?	the
use	usus	k1gInSc5	usus
of	of	k?	of
Her	hra	k1gFnPc2	hra
Majesty	Majest	k1gInPc4	Majest
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Navy	Navy	k?	Navy
<g/>
:	:	kIx,	:
and	and	k?	and
adapted	adapted	k1gMnSc1	adapted
for	forum	k1gNnPc2	forum
travellers	travellers	k6eAd1	travellers
in	in	k?	in
general	generat	k5eAaImAgInS	generat
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
F.	F.	kA	F.
<g/>
W.	W.	kA	W.
Herschel	Herschel	k1gMnSc1	Herschel
ed	ed	k?	ed
<g/>
.	.	kIx.	.
1851	[number]	k4	1851
<g/>
:	:	kIx,	:
A	a	k8xC	a
Monograph	Monograph	k1gInSc1	Monograph
of	of	k?	of
the	the	k?	the
Sub-class	Sublass	k1gInSc1	Sub-class
Cirripedia	Cirripedium	k1gNnSc2	Cirripedium
<g/>
,	,	kIx,	,
with	with	k1gMnSc1	with
Figures	Figures	k1gMnSc1	Figures
of	of	k?	of
all	all	k?	all
the	the	k?	the
Species	species	k1gFnSc2	species
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Lepadidae	Lepadidae	k1gFnSc1	Lepadidae
<g/>
;	;	kIx,	;
or	or	k?	or
<g/>
,	,	kIx,	,
Pedunculated	Pedunculated	k1gMnSc1	Pedunculated
Cirripedes	Cirripedes	k1gMnSc1	Cirripedes
<g/>
.	.	kIx.	.
1851	[number]	k4	1851
<g/>
:	:	kIx,	:
A	a	k8xC	a
Monograph	Monograph	k1gMnSc1	Monograph
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Fossil	Fossil	k1gMnSc2	Fossil
Lepadidae	Lepadida	k1gMnSc2	Lepadida
<g/>
;	;	kIx,	;
or	or	k?	or
<g/>
,	,	kIx,	,
Pedunculated	Pedunculated	k1gMnSc1	Pedunculated
Cirripedes	Cirripedes	k1gMnSc1	Cirripedes
of	of	k?	of
Great	Great	k1gInSc1	Great
Britain	Britain	k1gInSc1	Britain
1854	[number]	k4	1854
<g/>
:	:	kIx,	:
A	a	k8xC	a
Monograph	Monograph	k1gInSc1	Monograph
of	of	k?	of
the	the	k?	the
Sub-class	Sublass	k1gInSc1	Sub-class
Cirripedia	Cirripedium	k1gNnSc2	Cirripedium
<g/>
,	,	kIx,	,
with	with	k1gMnSc1	with
Figures	Figures	k1gMnSc1	Figures
of	of	k?	of
all	all	k?	all
the	the	k?	the
Species	species	k1gFnSc2	species
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Balanidae	Balanidae	k1gFnSc1	Balanidae
(	(	kIx(	(
<g/>
or	or	k?	or
Sessile	Sessila	k1gFnSc3	Sessila
Cirripedes	Cirripedes	k1gMnSc1	Cirripedes
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
the	the	k?	the
Verrucidae	Verrucidae	k1gInSc1	Verrucidae
<g/>
,	,	kIx,	,
etc	etc	k?	etc
<g/>
.	.	kIx.	.
1854	[number]	k4	1854
<g/>
:	:	kIx,	:
A	a	k8xC	a
Monograph	Monograph	k1gMnSc1	Monograph
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Fossil	Fossil	k1gMnSc2	Fossil
Balanidæ	Balanidæ	k1gMnSc2	Balanidæ
and	and	k?	and
Verrucidæ	Verrucidæ	k1gMnSc2	Verrucidæ
of	of	k?	of
Great	Great	k2eAgInSc4d1	Great
Britain	Britain	k1gInSc4	Britain
1858	[number]	k4	1858
<g/>
:	:	kIx,	:
On	on	k3xPp3gMnSc1	on
the	the	k?	the
Perpetuation	Perpetuation	k1gInSc1	Perpetuation
of	of	k?	of
Varieties	Varieties	k1gInSc1	Varieties
and	and	k?	and
Species	species	k1gFnSc2	species
by	by	kYmCp3nP	by
Natural	Natural	k?	Natural
Means	Means	k1gInSc1	Means
of	of	k?	of
Selection	Selection	k1gInSc1	Selection
1859	[number]	k4	1859
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gInSc1	on
the	the	k?	the
Origin	Origin	k1gInSc1	Origin
of	of	k?	of
Species	species	k1gFnSc4	species
by	by	k9	by
Means	Means	k1gInSc4	Means
of	of	k?	of
Natural	Natural	k?	Natural
Selection	Selection	k1gInSc1	Selection
<g/>
,	,	kIx,	,
or	or	k?	or
the	the	k?	the
Preservation	Preservation	k1gInSc1	Preservation
of	of	k?	of
Favoured	Favoured	k1gInSc1	Favoured
Races	Races	k1gInSc1	Races
in	in	k?	in
the	the	k?	the
Struggle	Struggle	k1gNnPc2	Struggle
for	forum	k1gNnPc2	forum
Life	Life	k1gFnSc1	Life
1862	[number]	k4	1862
<g/>
:	:	kIx,	:
On	on	k3xPp3gMnSc1	on
the	the	k?	the
various	various	k1gInSc1	various
contrivances	contrivancesa	k1gFnPc2	contrivancesa
by	by	kYmCp3nS	by
which	which	k1gMnSc1	which
British	British	k1gMnSc1	British
and	and	k?	and
foreign	foreign	k1gMnSc1	foreign
orchids	orchids	k6eAd1	orchids
are	ar	k1gInSc5	ar
fertilised	fertilised	k1gMnSc1	fertilised
by	by	kYmCp3nP	by
insects	insects	k6eAd1	insects
1868	[number]	k4	1868
<g/>
:	:	kIx,	:
Variation	Variation	k1gInSc1	Variation
of	of	k?	of
Plants	Plants	k1gInSc1	Plants
and	and	k?	and
Animals	Animals	k1gInSc1	Animals
Under	Under	k1gMnSc1	Under
Domestication	Domestication	k1gInSc1	Domestication
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
PDF	PDF	kA	PDF
format	format	k1gInSc1	format
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
(	(	kIx(	(
<g/>
Proměnlivost	proměnlivost	k1gFnSc1	proměnlivost
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
živočichů	živočich	k1gMnPc2	živočich
vlivem	vlivem	k7c2	vlivem
domestikace	domestikace	k1gFnSc2	domestikace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
1871	[number]	k4	1871
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Descent	Descent	k1gMnSc1	Descent
of	of	k?	of
Man	Man	k1gMnSc1	Man
and	and	k?	and
Selection	Selection	k1gInSc1	Selection
in	in	k?	in
Relation	Relation	k1gInSc1	Relation
to	ten	k3xDgNnSc1	ten
Sex	sex	k1gInSc1	sex
1872	[number]	k4	1872
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Expression	Expression	k1gInSc1	Expression
of	of	k?	of
Emotions	Emotions	k1gInSc1	Emotions
in	in	k?	in
Man	Man	k1gMnSc1	Man
<g />
.	.	kIx.	.
</s>
<s>
and	and	k?	and
Animals	Animals	k1gInSc1	Animals
1875	[number]	k4	1875
<g/>
:	:	kIx,	:
Movement	Movement	k1gInSc1	Movement
and	and	k?	and
Habits	Habits	k1gInSc1	Habits
of	of	k?	of
Climbing	Climbing	k1gInSc1	Climbing
Plants	Plants	k1gInSc1	Plants
1875	[number]	k4	1875
<g/>
:	:	kIx,	:
Insectivorous	Insectivorous	k1gInSc1	Insectivorous
Plants	Plants	k1gInSc1	Plants
1876	[number]	k4	1876
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Effects	Effects	k1gInSc1	Effects
of	of	k?	of
Cross	Cross	k1gInSc1	Cross
and	and	k?	and
Self-Fertilisation	Self-Fertilisation	k1gInSc1	Self-Fertilisation
in	in	k?	in
the	the	k?	the
Vegetable	Vegetable	k1gFnSc1	Vegetable
Kingdom	Kingdom	k1gInSc1	Kingdom
1877	[number]	k4	1877
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Different	Different	k1gMnSc1	Different
Forms	Forms	k1gInSc1	Forms
of	of	k?	of
Flowers	Flowers	k1gInSc1	Flowers
on	on	k3xPp3gInSc1	on
Plants	Plants	k1gInSc1	Plants
of	of	k?	of
the	the	k?	the
Same	Sam	k1gMnSc5	Sam
Species	species	k1gFnSc7	species
1879	[number]	k4	1879
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Preface	preface	k1gFnSc1	preface
and	and	k?	and
<g />
.	.	kIx.	.
</s>
<s>
'	'	kIx"	'
<g/>
a	a	k8xC	a
preliminary	preliminar	k1gInPc1	preliminar
notice	notice	k?	notice
<g/>
'	'	kIx"	'
<g/>
"	"	kIx"	"
in	in	k?	in
Ernst	Ernst	k1gMnSc1	Ernst
Krause	Kraus	k1gMnSc2	Kraus
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Erasmus	Erasmus	k1gMnSc1	Erasmus
Darwin	Darwin	k1gMnSc1	Darwin
1880	[number]	k4	1880
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Power	Power	k1gMnSc1	Power
of	of	k?	of
Movement	Movement	k1gMnSc1	Movement
in	in	k?	in
Plants	Plants	k1gInSc1	Plants
1881	[number]	k4	1881
<g/>
:	:	kIx,	:
Formation	Formation	k1gInSc1	Formation
of	of	k?	of
vegetable	vegetable	k6eAd1	vegetable
Mould	Mould	k1gMnSc1	Mould
Through	Through	k1gMnSc1	Through
the	the	k?	the
Action	Action	k1gInSc1	Action
of	of	k?	of
Worms	Worms	k1gInSc1	Worms
1887	[number]	k4	1887
<g/>
:	:	kIx,	:
Autobiography	Autobiographa	k1gFnSc2	Autobiographa
of	of	k?	of
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
Correspondence	Correspondence	k1gFnSc2	Correspondence
of	of	k?	of
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
1887	[number]	k4	1887
<g/>
:	:	kIx,	:
Life	Lif	k1gInSc2	Lif
and	and	k?	and
Letters	Lettersa	k1gFnPc2	Lettersa
of	of	k?	of
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Francis	Francis	k1gFnSc3	Francis
Darwin	Darwin	k1gMnSc1	Darwin
Volume	volum	k1gInSc5	volum
I	i	k9	i
,	,	kIx,	,
Volume	volum	k1gInSc5	volum
II	II	kA	II
1903	[number]	k4	1903
<g/>
:	:	kIx,	:
More	mor	k1gInSc5	mor
Letters	Lettersa	k1gFnPc2	Lettersa
of	of	k?	of
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Francis	Francis	k1gFnSc3	Francis
Darwin	Darwin	k1gMnSc1	Darwin
and	and	k?	and
A.C.	A.C.	k1gMnSc1	A.C.
Seward	Seward	k1gMnSc1	Seward
Volume	volum	k1gInSc5	volum
I	i	k9	i
<g/>
,	,	kIx,	,
Volume	volum	k1gInSc5	volum
II	II	kA	II
</s>
