<s>
Manětín	Manětín	k1gInSc1	Manětín
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Manětínského	manětínský	k2eAgInSc2d1	manětínský
potoka	potok	k1gInSc2	potok
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
a	a	k8xC	a
svahu	svah	k1gInSc6	svah
Chlumské	chlumský	k2eAgFnSc2d1	Chlumská
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
vzdálený	vzdálený	k2eAgMnSc1d1	vzdálený
29	[number]	k4	29
km	km	kA	km
ssz	ssz	k?	ssz
<g/>
.	.	kIx.	.
od	od	k7c2	od
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
.	.	kIx.	.
</s>
