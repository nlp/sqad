<s>
The	The	k?	The
Beatles	Beatles	k1gFnSc1	Beatles
byla	být	k5eAaImAgFnS	být
anglická	anglický	k2eAgFnSc1d1	anglická
rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
z	z	k7c2	z
Liverpoolu	Liverpool	k1gInSc2	Liverpool
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
členy	člen	k1gMnPc7	člen
byli	být	k5eAaImAgMnP	být
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
doprovodná	doprovodný	k2eAgFnSc1d1	doprovodná
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
McCartney	McCartnea	k1gFnSc2	McCartnea
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Harrison	Harrison	k1gMnSc1	Harrison
(	(	kIx(	(
<g/>
sólová	sólový	k2eAgFnSc1d1	sólová
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ringo	Ringo	k1gMnSc1	Ringo
Starr	Starr	k1gMnSc1	Starr
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
,	,	kIx,	,
příležitostně	příležitostně	k6eAd1	příležitostně
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
komerčně	komerčně	k6eAd1	komerčně
nejúspěšnější	úspěšný	k2eAgMnPc4d3	nejúspěšnější
a	a	k8xC	a
kritiky	kritik	k1gMnPc4	kritik
nejuznávanější	uznávaný	k2eAgFnSc2d3	nejuznávanější
kapely	kapela	k1gFnSc2	kapela
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
Beatles	Beatles	k1gFnPc2	Beatles
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
hraná	hraný	k2eAgFnSc1d1	hraná
a	a	k8xC	a
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
renomovaný	renomovaný	k2eAgInSc1d1	renomovaný
hudební	hudební	k2eAgInSc1d1	hudební
časopis	časopis	k1gInSc1	časopis
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
sestavil	sestavit	k5eAaPmAgInS	sestavit
žebříček	žebříček	k1gInSc4	žebříček
nejlepších	dobrý	k2eAgNnPc2d3	nejlepší
alb	album	k1gNnPc2	album
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
Beatles	Beatles	k1gFnPc1	Beatles
se	se	k3xPyFc4	se
do	do	k7c2	do
první	první	k4xOgFnSc2	první
desítky	desítka	k1gFnSc2	desítka
dostali	dostat	k5eAaPmAgMnP	dostat
celkem	celkem	k6eAd1	celkem
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
1	[number]	k4	1
<g/>
.	.	kIx.	.
místa	místo	k1gNnPc4	místo
za	za	k7c4	za
Sgt	Sgt	k1gFnSc4	Sgt
<g/>
.	.	kIx.	.
</s>
<s>
Pepper	Pepper	k1gInSc1	Pepper
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Lonely	Lonel	k1gMnPc7	Lonel
Hearts	Hearts	k1gInSc4	Hearts
Club	club	k1gInSc1	club
Band	banda	k1gFnPc2	banda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Asociace	asociace	k1gFnSc1	asociace
RIAA	RIAA	kA	RIAA
na	na	k7c6	na
základě	základ	k1gInSc6	základ
prodeje	prodej	k1gInSc2	prodej
singlů	singl	k1gInPc2	singl
a	a	k8xC	a
alb	album	k1gNnPc2	album
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
označila	označit	k5eAaPmAgFnS	označit
Beatles	Beatles	k1gFnSc4	Beatles
za	za	k7c4	za
nejvíce	nejvíce	k6eAd1	nejvíce
prodávanou	prodávaný	k2eAgFnSc4d1	prodávaná
kapelu	kapela	k1gFnSc4	kapela
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
a	a	k8xC	a
nejprodávanější	prodávaný	k2eAgNnSc4d3	nejprodávanější
hudební	hudební	k2eAgNnSc4d1	hudební
uskupení	uskupení	k1gNnSc4	uskupení
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
vydali	vydat	k5eAaPmAgMnP	vydat
Beatles	Beatles	k1gFnSc2	Beatles
více	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
různých	různý	k2eAgInPc2d1	různý
singlů	singl	k1gInPc2	singl
<g/>
,	,	kIx,	,
alb	album	k1gNnPc2	album
a	a	k8xC	a
desek	deska	k1gFnPc2	deska
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vyšplhaly	vyšplhat	k5eAaPmAgInP	vyšplhat
na	na	k7c4	na
první	první	k4xOgNnPc4	první
místa	místo	k1gNnPc4	místo
hitparád	hitparáda	k1gFnPc2	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
komerční	komerční	k2eAgInSc1d1	komerční
úspěch	úspěch	k1gInSc1	úspěch
se	se	k3xPyFc4	se
opakoval	opakovat	k5eAaImAgInS	opakovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
nahrávací	nahrávací	k2eAgFnSc1d1	nahrávací
společnost	společnost	k1gFnSc1	společnost
EMI	EMI	kA	EMI
odhadla	odhadnout	k5eAaPmAgFnS	odhadnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
prodali	prodat	k5eAaPmAgMnP	prodat
přes	přes	k7c4	přes
miliardu	miliarda	k4xCgFnSc4	miliarda
desek	deska	k1gFnPc2	deska
a	a	k8xC	a
kazet	kazeta	k1gFnPc2	kazeta
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
umístil	umístit	k5eAaPmAgInS	umístit
časopis	časopis	k1gInSc1	časopis
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
Beatles	Beatles	k1gFnPc2	Beatles
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
100	[number]	k4	100
největších	veliký	k2eAgMnPc2d3	veliký
umělců	umělec	k1gMnPc2	umělec
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
časopisu	časopis	k1gInSc2	časopis
pomohla	pomoct	k5eAaPmAgFnS	pomoct
jejich	jejich	k3xOp3gFnSc4	jejich
novátorská	novátorský	k2eAgFnSc1d1	novátorská
hudba	hudba	k1gFnSc1	hudba
a	a	k8xC	a
kulturní	kulturní	k2eAgInSc1d1	kulturní
vliv	vliv	k1gInSc1	vliv
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
60	[number]	k4	60
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
populární	populární	k2eAgFnSc4d1	populární
kulturu	kultura	k1gFnSc4	kultura
je	být	k5eAaImIp3nS	být
patrný	patrný	k2eAgInSc1d1	patrný
ještě	ještě	k9	ještě
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Beatles	beatles	k1gMnPc1	beatles
stáli	stát	k5eAaImAgMnP	stát
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
invaze	invaze	k1gFnSc2	invaze
britské	britský	k2eAgFnSc2d1	britská
hudby	hudba	k1gFnSc2	hudba
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
měl	mít	k5eAaImAgInS	mít
jejich	jejich	k3xOp3gFnSc7	jejich
počáteční	počáteční	k2eAgInSc1d1	počáteční
hudební	hudební	k2eAgInSc1d1	hudební
styl	styl	k1gInSc1	styl
kořeny	kořen	k1gInPc1	kořen
v	v	k7c4	v
rock	rock	k1gInSc4	rock
and	and	k?	and
rollu	roll	k1gInSc2	roll
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
v	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
verzi	verze	k1gFnSc6	verze
skifflu	skiffl	k1gInSc2	skiffl
<g/>
,	,	kIx,	,
vyzkoušela	vyzkoušet	k5eAaPmAgFnS	vyzkoušet
kapela	kapela	k1gFnSc1	kapela
mnoho	mnoho	k6eAd1	mnoho
různých	různý	k2eAgInPc2d1	různý
žánrů	žánr	k1gInPc2	žánr
od	od	k7c2	od
Tin	Tina	k1gFnPc2	Tina
Pan	Pan	k1gMnSc1	Pan
Alley	Allea	k1gFnSc2	Allea
až	až	k9	až
po	po	k7c4	po
psychedelický	psychedelický	k2eAgInSc4d1	psychedelický
rock	rock	k1gInSc4	rock
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgNnSc7	svůj
oblečením	oblečení	k1gNnSc7	oblečení
<g/>
,	,	kIx,	,
styly	styl	k1gInPc1	styl
a	a	k8xC	a
výroky	výrok	k1gInPc1	výrok
Beatles	beatles	k1gMnPc2	beatles
určovali	určovat	k5eAaImAgMnP	určovat
trendy	trend	k1gInPc4	trend
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jejich	jejich	k3xOp3gInSc1	jejich
vzrůstající	vzrůstající	k2eAgInSc1d1	vzrůstající
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
společenské	společenský	k2eAgFnPc4d1	společenská
otázky	otázka	k1gFnPc4	otázka
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
společenské	společenský	k2eAgInPc4d1	společenský
a	a	k8xC	a
kulturní	kulturní	k2eAgInPc4d1	kulturní
převraty	převrat	k1gInPc4	převrat
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
The	The	k1gFnSc1	The
Quarrymen	Quarrymen	k1gInSc1	Quarrymen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
založil	založit	k5eAaPmAgMnS	založit
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
docházky	docházka	k1gFnSc2	docházka
do	do	k7c2	do
gymnázia	gymnázium	k1gNnSc2	gymnázium
Quarry	Quarra	k1gFnSc2	Quarra
Bank	banka	k1gFnPc2	banka
v	v	k7c6	v
Liverpoolu	Liverpool	k1gInSc6	Liverpool
skifflovou	skifflový	k2eAgFnSc4d1	skifflový
skupinu	skupina	k1gFnSc4	skupina
The	The	k1gMnPc2	The
Blackjacks	Blackjacks	k1gInSc4	Blackjacks
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
po	po	k7c6	po
týdnu	týden	k1gInSc6	týden
ji	on	k3xPp3gFnSc4	on
přejmenoval	přejmenovat	k5eAaPmAgMnS	přejmenovat
na	na	k7c6	na
The	The	k1gFnSc6	The
Quarrymen	Quarrymen	k1gInSc1	Quarrymen
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
se	se	k3xPyFc4	se
po	po	k7c6	po
vystoupení	vystoupení	k1gNnSc6	vystoupení
The	The	k1gFnSc2	The
Quarrymen	Quarrymen	k1gInSc4	Quarrymen
na	na	k7c4	na
wooltonské	wooltonský	k2eAgFnPc4d1	wooltonský
zahradní	zahradní	k2eAgFnPc4d1	zahradní
slavnosti	slavnost	k1gFnPc4	slavnost
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
Sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
přijde	přijít	k5eAaPmIp3nS	přijít
do	do	k7c2	do
šatny	šatna	k1gFnSc2	šatna
na	na	k7c4	na
skupinu	skupina	k1gFnSc4	skupina
podívat	podívat	k5eAaImF	podívat
Paul	Paul	k1gMnSc1	Paul
McCartney	McCartnea	k1gFnSc2	McCartnea
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
kapele	kapela	k1gFnSc3	kapela
sebejistě	sebejistě	k6eAd1	sebejistě
předvede	předvést	k5eAaPmIp3nS	předvést
několik	několik	k4yIc4	několik
rock	rock	k1gInSc4	rock
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
rollových	rollův	k2eAgFnPc2d1	rollův
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
mu	on	k3xPp3gMnSc3	on
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
týdnech	týden	k1gInPc6	týden
vzkáže	vzkázat	k5eAaPmIp3nS	vzkázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
bere	brát	k5eAaImIp3nS	brát
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Musel	muset	k5eAaImAgMnS	muset
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
:	:	kIx,	:
buď	buď	k8xC	buď
posílím	posílit	k5eAaPmIp1nS	posílit
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přijmu	přijmout	k5eAaPmIp1nS	přijmout
Paula	Paula	k1gFnSc1	Paula
a	a	k8xC	a
posílím	posílit	k5eAaPmIp1nS	posílit
skupinu	skupina	k1gFnSc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
posílit	posílit	k5eAaPmF	posílit
skupinu	skupina	k1gFnSc4	skupina
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1958	[number]	k4	1958
pozvala	pozvat	k5eAaPmAgFnS	pozvat
kapela	kapela	k1gFnSc1	kapela
(	(	kIx(	(
<g/>
která	který	k3yIgFnSc1	který
tehdy	tehdy	k6eAd1	tehdy
hrála	hrát	k5eAaImAgFnS	hrát
pod	pod	k7c7	pod
různými	různý	k2eAgNnPc7d1	různé
jmény	jméno	k1gNnPc7	jméno
<g/>
)	)	kIx)	)
na	na	k7c4	na
McCartneyho	McCartney	k1gMnSc4	McCartney
naléhání	naléhání	k1gNnSc2	naléhání
čtrnáctiletého	čtrnáctiletý	k2eAgMnSc4d1	čtrnáctiletý
kytaristu	kytarista	k1gMnSc4	kytarista
George	Georg	k1gMnSc4	Georg
Harrisona	Harrison	k1gMnSc4	Harrison
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
podíval	podívat	k5eAaImAgMnS	podívat
ve	v	k7c4	v
Wilson	Wilson	k1gNnSc4	Wilson
Hall	Halla	k1gFnPc2	Halla
v	v	k7c6	v
liverpoolském	liverpoolský	k2eAgInSc6d1	liverpoolský
Garstonu	Garston	k1gInSc6	Garston
<g/>
.	.	kIx.	.
</s>
<s>
McCartney	McCartney	k1gInPc1	McCartney
se	se	k3xPyFc4	se
s	s	k7c7	s
Harrisonem	Harrison	k1gMnSc7	Harrison
seznámil	seznámit	k5eAaPmAgMnS	seznámit
ve	v	k7c6	v
školním	školní	k2eAgInSc6d1	školní
autobuse	autobus	k1gInSc6	autobus
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Liverpoolského	liverpoolský	k2eAgInSc2d1	liverpoolský
Institutu	institut	k1gInSc2	institut
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
oba	dva	k4xCgMnPc1	dva
navštěvovali	navštěvovat	k5eAaImAgMnP	navštěvovat
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Lennon	Lennon	k1gMnSc1	Lennon
nechtěl	chtít	k5eNaImAgMnS	chtít
o	o	k7c4	o
přijetí	přijetí	k1gNnSc4	přijetí
Harrisona	Harrison	k1gMnSc2	Harrison
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
i	i	k9	i
přes	přes	k7c4	přes
jeho	jeho	k3xOp3gFnPc4	jeho
instrumentální	instrumentální	k2eAgFnPc4d1	instrumentální
kvality	kvalita	k1gFnPc4	kvalita
původně	původně	k6eAd1	původně
ani	ani	k8xC	ani
slyšet	slyšet	k5eAaImF	slyšet
(	(	kIx(	(
<g/>
důvodem	důvod	k1gInSc7	důvod
byl	být	k5eAaImAgInS	být
velký	velký	k2eAgInSc1d1	velký
věkový	věkový	k2eAgInSc1d1	věkový
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
-	-	kIx~	-
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Harrison	Harrison	k1gMnSc1	Harrison
se	se	k3xPyFc4	se
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
března	březen	k1gInSc2	březen
1958	[number]	k4	1958
sám	sám	k3xTgMnSc1	sám
"	"	kIx"	"
<g/>
infiltroval	infiltrovat	k5eAaBmAgMnS	infiltrovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
měnilo	měnit	k5eAaImAgNnS	měnit
obsazení	obsazení	k1gNnSc1	obsazení
skupiny	skupina	k1gFnSc2	skupina
-	-	kIx~	-
většinou	většinou	k6eAd1	většinou
kvůli	kvůli	k7c3	kvůli
McCartneyovým	McCartneyův	k2eAgInPc3d1	McCartneyův
vysokým	vysoký	k2eAgInPc3d1	vysoký
požadavkům	požadavek	k1gInPc3	požadavek
na	na	k7c4	na
instrumentální	instrumentální	k2eAgFnSc4d1	instrumentální
zdatnost	zdatnost	k1gFnSc4	zdatnost
hráčů	hráč	k1gMnPc2	hráč
(	(	kIx(	(
<g/>
vedoucí	vedoucí	k1gMnSc1	vedoucí
Lennon	Lennon	k1gMnSc1	Lennon
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
nechal	nechat	k5eAaPmAgMnS	nechat
jeho	jeho	k3xOp3gNnPc4	jeho
logickým	logický	k2eAgNnSc7d1	logické
argumenty	argument	k1gInPc7	argument
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1960	[number]	k4	1960
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
Lennonův	Lennonův	k2eAgMnSc1d1	Lennonův
přítel	přítel	k1gMnSc1	přítel
z	z	k7c2	z
umělecké	umělecký	k2eAgFnSc2d1	umělecká
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
Stuart	Stuarta	k1gFnPc2	Stuarta
Sutcliffe	Sutcliff	k1gInSc5	Sutcliff
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
Lennon	Lennon	k1gMnSc1	Lennon
s	s	k7c7	s
McCartneym	McCartneym	k1gInSc4	McCartneym
přinutili	přinutit	k5eAaPmAgMnP	přinutit
koupit	koupit	k5eAaPmF	koupit
baskytaru	baskytara	k1gFnSc4	baskytara
a	a	k8xC	a
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
to	ten	k3xDgNnSc1	ten
neuměl	umět	k5eNaImAgMnS	umět
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gMnSc1	Lennon
s	s	k7c7	s
McCartneym	McCartneym	k1gInSc1	McCartneym
hráli	hrát	k5eAaImAgMnP	hrát
oba	dva	k4xCgInPc4	dva
dva	dva	k4xCgInPc4	dva
na	na	k7c4	na
doprovodné	doprovodný	k2eAgFnPc4d1	doprovodná
kytary	kytara	k1gFnPc4	kytara
<g/>
,	,	kIx,	,
Harrison	Harrison	k1gMnSc1	Harrison
na	na	k7c4	na
sólovou	sólový	k2eAgFnSc4d1	sólová
a	a	k8xC	a
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
se	se	k3xPyFc4	se
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
bubeníků	bubeník	k1gMnPc2	bubeník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
The	The	k1gMnPc1	The
Quarrymen	Quarrymen	k1gInSc4	Quarrymen
vystřídali	vystřídat	k5eAaPmAgMnP	vystřídat
řadu	řada	k1gFnSc4	řada
jmen	jméno	k1gNnPc2	jméno
-	-	kIx~	-
"	"	kIx"	"
<g/>
Johnny	Johnna	k1gFnPc1	Johnna
and	and	k?	and
the	the	k?	the
Moondogs	Moondogs	k1gInSc1	Moondogs
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Long	Long	k1gMnSc1	Long
John	John	k1gMnSc1	John
and	and	k?	and
the	the	k?	the
Beatles	Beatles	k1gFnPc2	Beatles
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Silver	Silver	k1gMnSc1	Silver
Beetles	Beetles	k1gMnSc1	Beetles
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
pocházející	pocházející	k2eAgFnPc1d1	pocházející
z	z	k7c2	z
návrhu	návrh	k1gInSc2	návrh
Larryho	Larry	k1gMnSc2	Larry
Parnese	Parnese	k1gFnSc2	Parnese
na	na	k7c4	na
"	"	kIx"	"
<g/>
Long	Long	k1gMnSc1	Long
John	John	k1gMnSc1	John
and	and	k?	and
the	the	k?	the
Silver	Silver	k1gMnSc1	Silver
Beetles	Beetles	k1gMnSc1	Beetles
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
-	-	kIx~	-
před	před	k7c7	před
ustálením	ustálení	k1gNnSc7	ustálení
se	se	k3xPyFc4	se
na	na	k7c6	na
The	The	k1gFnSc6	The
Beatles	Beatles	k1gFnPc2	Beatles
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c4	mnoho
teorií	teorie	k1gFnPc2	teorie
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
názvu	název	k1gInSc3	název
kapely	kapela	k1gFnSc2	kapela
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc2	jeho
nezvyklého	zvyklý	k2eNgInSc2d1	nezvyklý
pravopisu	pravopis	k1gInSc2	pravopis
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
připisováno	připisovat	k5eAaImNgNnS	připisovat
Lennonovi	Lennon	k1gMnSc6	Lennon
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
kombinací	kombinace	k1gFnSc7	kombinace
slova	slovo	k1gNnSc2	slovo
beetles	beetles	k1gInSc4	beetles
(	(	kIx(	(
<g/>
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
brouci	brouk	k1gMnPc1	brouk
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
odkaz	odkaz	k1gInSc1	odkaz
na	na	k7c4	na
kapelu	kapela	k1gFnSc4	kapela
Buddy	Budda	k1gMnSc2	Budda
Hollyho	Holly	k1gMnSc2	Holly
The	The	k1gFnSc1	The
Crickets	Crickets	k1gInSc1	Crickets
<g/>
)	)	kIx)	)
a	a	k8xC	a
slova	slovo	k1gNnSc2	slovo
beat	beat	k1gInSc1	beat
<g/>
.	.	kIx.	.
</s>
<s>
Cynthia	Cynthia	k1gFnSc1	Cynthia
Lennon	Lennona	k1gFnPc2	Lennona
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lennon	Lennon	k1gMnSc1	Lennon
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
název	název	k1gInSc4	název
Beatles	Beatles	k1gFnSc2	Beatles
"	"	kIx"	"
<g/>
během	během	k7c2	během
brainstormingového	brainstormingový	k2eAgNnSc2d1	brainstormingový
sezení	sezení	k1gNnSc2	sezení
u	u	k7c2	u
pivem	pivo	k1gNnSc7	pivo
nasáklého	nasáklý	k2eAgInSc2d1	nasáklý
stolu	stol	k1gInSc2	stol
v	v	k7c6	v
baru	bar	k1gInSc6	bar
Renshaw	Renshaw	k1gMnSc1	Renshaw
Hall	Hall	k1gMnSc1	Hall
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Lennon	Lennon	k1gMnSc1	Lennon
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
známý	známý	k1gMnSc1	známý
svým	svůj	k3xOyFgNnSc7	svůj
vyprávěním	vyprávění	k1gNnSc7	vyprávění
různých	různý	k2eAgFnPc2d1	různá
verzí	verze	k1gFnPc2	verze
příběhů	příběh	k1gInPc2	příběh
zavtipkoval	zavtipkovat	k5eAaPmAgMnS	zavtipkovat
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Mersey	Mersea	k1gFnSc2	Mersea
Beat	beat	k1gInSc1	beat
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
to	ten	k3xDgNnSc1	ten
přišlo	přijít	k5eAaPmAgNnS	přijít
ve	v	k7c6	v
vidině	vidina	k1gFnSc6	vidina
-	-	kIx~	-
objevil	objevit	k5eAaPmAgMnS	objevit
se	se	k3xPyFc4	se
muž	muž	k1gMnSc1	muž
na	na	k7c6	na
ohnivém	ohnivý	k2eAgInSc6d1	ohnivý
koláči	koláč	k1gInSc6	koláč
a	a	k8xC	a
řekl	říct	k5eAaPmAgMnS	říct
jim	on	k3xPp3gMnPc3	on
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
dne	den	k1gInSc2	den
dále	daleko	k6eAd2	daleko
budete	být	k5eAaImBp2nP	být
Beatles	Beatles	k1gFnPc1	Beatles
s	s	k7c7	s
A.	A.	kA	A.
<g/>
"	"	kIx"	"
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
převzal	převzít	k5eAaPmAgInS	převzít
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
pravopis	pravopis	k1gInSc4	pravopis
jména	jméno	k1gNnSc2	jméno
Paul	Paula	k1gFnPc2	Paula
McCartney	McCartnea	k1gFnSc2	McCartnea
<g/>
,	,	kIx,	,
když	když	k8xS	když
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
John	John	k1gMnSc1	John
měl	mít	k5eAaImAgMnS	mít
nápad	nápad	k1gInSc4	nápad
<g/>
,	,	kIx,	,
že	že	k8xS	že
bychom	by	kYmCp1nP	by
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
jmenovat	jmenovat	k5eAaImF	jmenovat
Beetles	Beetles	k1gInSc4	Beetles
a	a	k8xC	a
já	já	k3xPp1nSc1	já
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
"	"	kIx"	"
<g/>
a	a	k8xC	a
co	co	k9	co
třeba	třeba	k6eAd1	třeba
Beatles	Beatles	k1gFnPc2	Beatles
<g/>
;	;	kIx,	;
jako	jako	k8xC	jako
tlukot	tlukot	k1gInSc4	tlukot
bubnu	buben	k1gInSc2	buben
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Je	být	k5eAaImIp3nS	být
zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
dějiny	dějiny	k1gFnPc1	dějiny
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1960	[number]	k4	1960
Beatles	beatles	k1gMnPc1	beatles
objížděli	objíždět	k5eAaImAgMnP	objíždět
severovýchodní	severovýchodní	k2eAgNnSc4d1	severovýchodní
Skotsko	Skotsko	k1gNnSc4	Skotsko
jako	jako	k8xS	jako
doprovodná	doprovodný	k2eAgFnSc1d1	doprovodná
kapela	kapela	k1gFnSc1	kapela
zpěváka	zpěvák	k1gMnSc2	zpěvák
Johnnyho	Johnny	k1gMnSc2	Johnny
Gentlea	Gentleus	k1gMnSc2	Gentleus
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Gentlem	Gentl	k1gInSc7	Gentl
se	se	k3xPyFc4	se
setkali	setkat	k5eAaPmAgMnP	setkat
hodinu	hodina	k1gFnSc4	hodina
před	před	k7c7	před
jejich	jejich	k3xOp3gNnSc7	jejich
prvním	první	k4xOgNnSc7	první
vystoupením	vystoupení	k1gNnSc7	vystoupení
a	a	k8xC	a
McCartney	McCartneum	k1gNnPc7	McCartneum
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
jako	jako	k8xC	jako
na	na	k7c4	na
velkou	velký	k2eAgFnSc4d1	velká
zkušenost	zkušenost	k1gFnSc4	zkušenost
pro	pro	k7c4	pro
kapelu	kapela	k1gFnSc4	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
bez	bez	k7c2	bez
bubeníka	bubeník	k1gMnSc2	bubeník
si	se	k3xPyFc3	se
na	na	k7c6	na
turné	turné	k1gNnSc6	turné
zajistila	zajistit	k5eAaPmAgFnS	zajistit
služby	služba	k1gFnPc4	služba
Tommyho	Tommy	k1gMnSc2	Tommy
Moora	Moor	k1gMnSc2	Moor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
mnohem	mnohem	k6eAd1	mnohem
starší	starý	k2eAgMnSc1d2	starší
než	než	k8xS	než
ostatní	ostatní	k2eAgMnSc1d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
turné	turné	k1gNnSc6	turné
se	se	k3xPyFc4	se
Moore	Moor	k1gInSc5	Moor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
cítil	cítit	k5eAaImAgMnS	cítit
velký	velký	k2eAgInSc4d1	velký
věkový	věkový	k2eAgInSc4d1	věkový
rozdíl	rozdíl	k1gInSc4	rozdíl
<g/>
,	,	kIx,	,
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
továrny	továrna	k1gFnSc2	továrna
na	na	k7c4	na
lahve	lahev	k1gFnPc4	lahev
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k8xS	jako
řidič	řidič	k1gMnSc1	řidič
vysokozdvižného	vysokozdvižný	k2eAgInSc2d1	vysokozdvižný
vozíku	vozík	k1gInSc2	vozík
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
bubeníkem	bubeník	k1gMnSc7	bubeník
byl	být	k5eAaImAgMnS	být
Norman	Norman	k1gMnSc1	Norman
Chapman	Chapman	k1gMnSc1	Chapman
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c4	za
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
byl	být	k5eAaImAgInS	být
povolán	povolat	k5eAaPmNgInS	povolat
do	do	k7c2	do
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
odjezd	odjezd	k1gInSc1	odjezd
způsobil	způsobit	k5eAaPmAgInS	způsobit
závažný	závažný	k2eAgInSc1d1	závažný
problém	problém	k1gInSc1	problém
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
Allan	Allan	k1gMnSc1	Allan
Williams	Williams	k1gInSc1	Williams
<g/>
,	,	kIx,	,
vlastník	vlastník	k1gMnSc1	vlastník
liverpoolského	liverpoolský	k2eAgInSc2d1	liverpoolský
klubu	klub	k1gInSc2	klub
Jacaranda	Jacarando	k1gNnSc2	Jacarando
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nově	nově	k6eAd1	nově
vznikající	vznikající	k2eAgFnSc2d1	vznikající
domácí	domácí	k2eAgFnSc2d1	domácí
skupiny	skupina	k1gFnSc2	skupina
podporoval	podporovat	k5eAaImAgInS	podporovat
a	a	k8xC	a
fungoval	fungovat	k5eAaImAgInS	fungovat
jako	jako	k9	jako
jejich	jejich	k3xOp3gInSc1	jejich
neoficiální	neoficiální	k2eAgInSc1d1	neoficiální
manažer	manažer	k1gInSc1	manažer
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
domluvil	domluvit	k5eAaPmAgMnS	domluvit
hraní	hraní	k1gNnSc4	hraní
v	v	k7c6	v
klubech	klub	k1gInPc6	klub
na	na	k7c4	na
Reeperbahn	Reeperbahn	k1gInSc4	Reeperbahn
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Hamburku	Hamburk	k1gInSc6	Hamburk
<g/>
.	.	kIx.	.
</s>
<s>
Hamburk	Hamburk	k1gInSc1	Hamburk
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1960	[number]	k4	1960
pozval	pozvat	k5eAaPmAgInS	pozvat
McCartney	McCartnea	k1gFnSc2	McCartnea
Petea	Peteus	k1gMnSc2	Peteus
Besta	Best	k1gMnSc2	Best
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
stálým	stálý	k2eAgMnSc7d1	stálý
bubeníkem	bubeník	k1gMnSc7	bubeník
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Viděl	vidět	k5eAaImAgMnS	vidět
Besta	Besta	k1gMnSc1	Besta
hrát	hrát	k5eAaImF	hrát
s	s	k7c7	s
The	The	k1gMnPc7	The
Blackjacks	Blackjacks	k1gInSc4	Blackjacks
v	v	k7c4	v
Casbah	Casbah	k1gInSc4	Casbah
Clubu	club	k1gInSc2	club
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
Petova	Petův	k2eAgFnSc1d1	Petova
matka	matka	k1gFnSc1	matka
Mona	Mona	k1gFnSc1	Mona
Bestová	Bestová	k1gFnSc1	Bestová
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
sklepní	sklepní	k2eAgInSc1d1	sklepní
klub	klub	k1gInSc1	klub
v	v	k7c6	v
liverpoolském	liverpoolský	k2eAgInSc6d1	liverpoolský
West	West	k1gInSc1	West
Derby	derby	k1gNnSc7	derby
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Beatles	beatles	k1gMnPc1	beatles
hrávali	hrávat	k5eAaImAgMnP	hrávat
a	a	k8xC	a
často	často	k6eAd1	často
ho	on	k3xPp3gMnSc4	on
navštěvovali	navštěvovat	k5eAaImAgMnP	navštěvovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dokumentárním	dokumentární	k2eAgInSc6d1	dokumentární
snímku	snímek	k1gInSc6	snímek
The	The	k1gFnSc1	The
Compleat	Compleat	k1gInSc1	Compleat
Beatles	beatles	k1gMnSc1	beatles
řekl	říct	k5eAaPmAgMnS	říct
Williams	Williams	k1gInSc4	Williams
<g/>
,	,	kIx,	,
že	že	k8xS	že
Best	Best	k1gMnSc1	Best
"	"	kIx"	"
<g/>
nehrál	hrát	k5eNaImAgMnS	hrát
příliš	příliš	k6eAd1	příliš
obratně	obratně	k6eAd1	obratně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obstojně	obstojně	k6eAd1	obstojně
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Beatles	beatles	k1gMnPc1	beatles
začali	začít	k5eAaPmAgMnP	začít
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
hamburských	hamburský	k2eAgInPc6d1	hamburský
barech	bar	k1gInPc6	bar
Indra	Indra	k1gMnSc1	Indra
a	a	k8xC	a
Kaiserkeller	Kaiserkeller	k1gMnSc1	Kaiserkeller
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
provozovatelem	provozovatel	k1gMnSc7	provozovatel
byl	být	k5eAaImAgMnS	být
Bruno	Bruno	k1gMnSc1	Bruno
Koschmider	Koschmider	k1gMnSc1	Koschmider
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
po	po	k7c6	po
Beatles	Beatles	k1gFnSc6	Beatles
vyžadoval	vyžadovat	k5eAaImAgMnS	vyžadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
hráli	hrát	k5eAaImAgMnP	hrát
šest	šest	k4xCc4	šest
až	až	k9	až
sedm	sedm	k4xCc4	sedm
hodin	hodina	k1gFnPc2	hodina
za	za	k7c4	za
večer	večer	k1gInSc4	večer
<g/>
,	,	kIx,	,
sedm	sedm	k4xCc4	sedm
dní	den	k1gInPc2	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
.	.	kIx.	.
</s>
<s>
Beatles	beatles	k1gMnPc1	beatles
si	se	k3xPyFc3	se
získali	získat	k5eAaPmAgMnP	získat
menší	malý	k2eAgInSc4d2	menší
okruh	okruh	k1gInSc4	okruh
příznivců	příznivec	k1gMnPc2	příznivec
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgInPc7	jenž
byli	být	k5eAaImAgMnP	být
i	i	k8xC	i
fotografka	fotografka	k1gFnSc1	fotografka
Astrid	Astrida	k1gFnPc2	Astrida
Kirchherrová	Kirchherrová	k1gFnSc1	Kirchherrová
a	a	k8xC	a
výtvarník	výtvarník	k1gMnSc1	výtvarník
Klaus	Klaus	k1gMnSc1	Klaus
Voormann	Voormann	k1gMnSc1	Voormann
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
Beatles	beatles	k1gMnSc1	beatles
přetáhl	přetáhnout	k5eAaPmAgMnS	přetáhnout
Koschmiderův	Koschmiderův	k2eAgMnSc1d1	Koschmiderův
konkurent	konkurent	k1gMnSc1	konkurent
Peter	Peter	k1gMnSc1	Peter
Eckhorn	Eckhorn	k1gMnSc1	Eckhorn
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
klubu	klub	k1gInSc2	klub
Top	topit	k5eAaImRp2nS	topit
Ten	ten	k3xDgInSc4	ten
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Harrison	Harrison	k1gMnSc1	Harrison
vyhoštěn	vyhostit	k5eAaPmNgMnS	vyhostit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
přišlo	přijít	k5eAaPmAgNnS	přijít
udání	udání	k1gNnSc1	udání
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
plnoletý	plnoletý	k2eAgMnSc1d1	plnoletý
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
byli	být	k5eAaImAgMnP	být
McCartney	McCartnea	k1gFnSc2	McCartnea
a	a	k8xC	a
Best	Besta	k1gFnPc2	Besta
zatčeni	zatknout	k5eAaPmNgMnP	zatknout
<g/>
,	,	kIx,	,
obviněni	obvinit	k5eAaPmNgMnP	obvinit
za	za	k7c4	za
žhářství	žhářství	k1gNnSc4	žhářství
a	a	k8xC	a
vyhoštěni	vyhostit	k5eAaPmNgMnP	vyhostit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
při	při	k7c6	při
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
pokoje	pokoj	k1gInSc2	pokoj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
během	během	k7c2	během
angažmá	angažmá	k1gNnSc2	angažmá
u	u	k7c2	u
Koschmidera	Koschmidero	k1gNnSc2	Koschmidero
bydleli	bydlet	k5eAaImAgMnP	bydlet
<g/>
,	,	kIx,	,
zapálili	zapálit	k5eAaPmAgMnP	zapálit
na	na	k7c6	na
zdi	zeď	k1gFnSc6	zeď
kondom	kondom	k1gInSc4	kondom
a	a	k8xC	a
způsobili	způsobit	k5eAaPmAgMnP	způsobit
malý	malý	k2eAgInSc4d1	malý
požár	požár	k1gInSc4	požár
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gMnSc1	Lennon
sám	sám	k3xTgMnSc1	sám
následoval	následovat	k5eAaImAgMnS	následovat
ostatní	ostatní	k2eAgMnSc1d1	ostatní
v	v	k7c6	v
půlce	půlka	k1gFnSc6	půlka
prosince	prosinec	k1gInSc2	prosinec
<g/>
,	,	kIx,	,
Sutcliffe	Sutcliff	k1gInSc5	Sutcliff
zůstal	zůstat	k5eAaPmAgInS	zůstat
v	v	k7c6	v
Hamburku	Hamburk	k1gInSc6	Hamburk
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zasnoubí	zasnoubit	k5eAaPmIp3nS	zasnoubit
s	s	k7c7	s
Kirchherrovou	Kirchherrová	k1gFnSc7	Kirchherrová
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
spolu	spolu	k6eAd1	spolu
hráli	hrát	k5eAaImAgMnP	hrát
Beatles	Beatles	k1gFnSc4	Beatles
poprvé	poprvé	k6eAd1	poprvé
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1960	[number]	k4	1960
v	v	k7c6	v
Casbah	Casbaha	k1gFnPc2	Casbaha
Clubu	club	k1gInSc2	club
<g/>
.	.	kIx.	.
</s>
<s>
Baskytarista	baskytarista	k1gMnSc1	baskytarista
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Hamburku	Hamburk	k1gInSc6	Hamburk
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
musel	muset	k5eAaImAgMnS	muset
basu	basa	k1gFnSc4	basa
převzít	převzít	k5eAaPmF	převzít
Paul	Paul	k1gMnSc1	Paul
McCartney	McCartnea	k1gFnPc4	McCartnea
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
zprvu	zprvu	k6eAd1	zprvu
neměl	mít	k5eNaImAgMnS	mít
vůbec	vůbec	k9	vůbec
žádnou	žádný	k3yNgFnSc4	žádný
radost	radost	k1gFnSc4	radost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1961	[number]	k4	1961
se	se	k3xPyFc4	se
Beatles	beatles	k1gMnPc1	beatles
vrátili	vrátit	k5eAaPmAgMnP	vrátit
do	do	k7c2	do
Hamburku	Hamburk	k1gInSc2	Hamburk
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
s	s	k7c7	s
řádně	řádně	k6eAd1	řádně
zajištěnými	zajištěný	k2eAgFnPc7d1	zajištěná
pracovními	pracovní	k2eAgNnPc7d1	pracovní
povoleními	povolení	k1gNnPc7	povolení
<g/>
,	,	kIx,	,
o	o	k7c4	o
něž	jenž	k3xRgMnPc4	jenž
se	se	k3xPyFc4	se
postarala	postarat	k5eAaPmAgFnS	postarat
Bestova	Bestův	k2eAgFnSc1d1	Bestova
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
moment	moment	k1gInSc4	moment
převzala	převzít	k5eAaPmAgFnS	převzít
ochranné	ochranný	k2eAgFnPc4d1	ochranná
otěže	otěž	k1gFnPc4	otěž
nad	nad	k7c7	nad
skupinou	skupina	k1gFnSc7	skupina
a	a	k8xC	a
vytěsnila	vytěsnit	k5eAaPmAgFnS	vytěsnit
tak	tak	k9	tak
Williamse	Williamse	k1gFnSc1	Williamse
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
hraní	hraní	k1gNnSc2	hraní
v	v	k7c6	v
hamburském	hamburský	k2eAgInSc6d1	hamburský
Top	topit	k5eAaImRp2nS	topit
Ten	ten	k3xDgInSc1	ten
Club	club	k1gInSc4	club
si	se	k3xPyFc3	se
Beatles	beatles	k1gMnSc1	beatles
vybral	vybrat	k5eAaPmAgMnS	vybrat
zpěvák	zpěvák	k1gMnSc1	zpěvák
Tony	Tony	k1gMnSc1	Tony
Sheridan	Sheridan	k1gMnSc1	Sheridan
jako	jako	k9	jako
svou	svůj	k3xOyFgFnSc4	svůj
doprovodnou	doprovodný	k2eAgFnSc4d1	doprovodná
kapelu	kapela	k1gFnSc4	kapela
pro	pro	k7c4	pro
sérii	série	k1gFnSc4	série
nahrávek	nahrávka	k1gFnPc2	nahrávka
pro	pro	k7c4	pro
německou	německý	k2eAgFnSc4d1	německá
společnost	společnost	k1gFnSc4	společnost
Polydor	Polydora	k1gFnPc2	Polydora
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
produkoval	produkovat	k5eAaImAgMnS	produkovat
známý	známý	k2eAgMnSc1d1	známý
kapelník	kapelník	k1gMnSc1	kapelník
Bert	Berta	k1gFnPc2	Berta
Kaempfert	Kaempfert	k1gMnSc1	Kaempfert
<g/>
.	.	kIx.	.
</s>
<s>
Kaempfert	Kaempfert	k1gMnSc1	Kaempfert
podepsal	podepsat	k5eAaPmAgMnS	podepsat
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
při	při	k7c6	při
prvním	první	k4xOgNnSc6	první
setkání	setkání	k1gNnSc6	setkání
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1961	[number]	k4	1961
jejich	jejich	k3xOp3gFnSc4	jejich
vlastní	vlastní	k2eAgFnSc4d1	vlastní
smlouvu	smlouva	k1gFnSc4	smlouva
pro	pro	k7c4	pro
Polydor	Polydor	k1gInSc4	Polydor
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
vydal	vydat	k5eAaPmAgMnS	vydat
Polydor	Polydor	k1gMnSc1	Polydor
nahrávku	nahrávka	k1gFnSc4	nahrávka
písně	píseň	k1gFnSc2	píseň
My	my	k3xPp1nPc1	my
Bonnie	Bonnie	k1gFnSc1	Bonnie
(	(	kIx(	(
<g/>
Mein	Mein	k1gInSc1	Mein
Hertz	hertz	k1gInSc1	hertz
ist	ist	k?	ist
bei	bei	k?	bei
dir	dir	k?	dir
nur	nur	k?	nur
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
německých	německý	k2eAgInPc6d1	německý
žebříčcích	žebříček	k1gInPc6	žebříček
objevila	objevit	k5eAaPmAgFnS	objevit
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Tony	Tony	k1gMnSc1	Tony
Sheridan	Sheridan	k1gMnSc1	Sheridan
and	and	k?	and
the	the	k?	the
Beat	beat	k1gInSc1	beat
Brothers	Brothers	k1gInSc1	Brothers
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
byl	být	k5eAaImAgInS	být
obecný	obecný	k2eAgInSc1d1	obecný
název	název	k1gInSc1	název
pro	pro	k7c4	pro
kohokoliv	kdokoliv	k3yInSc4	kdokoliv
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
Sheridanově	Sheridanův	k2eAgFnSc6d1	Sheridanova
doprovodné	doprovodný	k2eAgFnSc6d1	doprovodná
kapele	kapela	k1gFnSc6	kapela
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
I	i	k9	i
pozdější	pozdní	k2eAgFnSc2d2	pozdější
hamburské	hamburský	k2eAgFnSc2d1	hamburská
nahrávky	nahrávka	k1gFnSc2	nahrávka
Tonyho	Tony	k1gMnSc2	Tony
Sheridana	Sheridan	k1gMnSc2	Sheridan
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
hudebníky	hudebník	k1gMnPc7	hudebník
jsou	být	k5eAaImIp3nP	být
souhrnně	souhrnně	k6eAd1	souhrnně
připsány	připsat	k5eAaPmNgInP	připsat
skupině	skupina	k1gFnSc6	skupina
The	The	k1gFnSc1	The
Beat	beat	k1gInSc4	beat
Brothers	Brothers	k1gInSc1	Brothers
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
omylem	omylem	k6eAd1	omylem
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
nahrávky	nahrávka	k1gFnPc4	nahrávka
Beatles	Beatles	k1gFnPc2	Beatles
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
K	k	k7c3	k
pověsti	pověst	k1gFnSc3	pověst
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
nahrávka	nahrávka	k1gFnSc1	nahrávka
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
setkání	setkání	k1gNnSc3	setkání
kapely	kapela	k1gFnSc2	kapela
s	s	k7c7	s
jejich	jejich	k3xOp3gMnSc7	jejich
budoucím	budoucí	k2eAgMnSc7d1	budoucí
manažerem	manažer	k1gMnSc7	manažer
Brianem	Brian	k1gMnSc7	Brian
Epsteinem	Epstein	k1gMnSc7	Epstein
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byli	být	k5eAaImAgMnP	být
díky	díky	k7c3	díky
ní	on	k3xPp3gFnSc3	on
také	také	k6eAd1	také
poprvé	poprvé	k6eAd1	poprvé
zmíněni	zmíněn	k2eAgMnPc1d1	zmíněn
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
tisku	tisk	k1gInSc6	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Epstein	Epstein	k1gMnSc1	Epstein
vzal	vzít	k5eAaPmAgMnS	vzít
skupinu	skupina	k1gFnSc4	skupina
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
rukou	ruka	k1gFnPc2	ruka
a	a	k8xC	a
přestože	přestože	k8xS	přestože
neměl	mít	k5eNaImAgMnS	mít
žádné	žádný	k3yNgFnPc4	žádný
manažerské	manažerský	k2eAgFnPc4d1	manažerská
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
,	,	kIx,	,
se	s	k7c7	s
zápalem	zápal	k1gInSc7	zápal
sobě	se	k3xPyFc3	se
vlastním	vlastnit	k5eAaImIp1nS	vlastnit
započal	započnout	k5eAaPmAgInS	započnout
s	s	k7c7	s
neúnavnou	únavný	k2eNgFnSc7d1	neúnavná
péčí	péče	k1gFnSc7	péče
o	o	k7c6	o
Beatles	Beatles	k1gFnSc6	Beatles
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
propagaci	propagace	k1gFnSc4	propagace
<g/>
,	,	kIx,	,
počínaje	počínaje	k7c7	počínaje
jevištní	jevištní	k2eAgFnSc7d1	jevištní
prezentací	prezentace	k1gFnSc7	prezentace
a	a	k8xC	a
konče	konče	k7c7	konče
obcházením	obcházení	k1gNnSc7	obcházení
vydavatelských	vydavatelský	k2eAgFnPc2d1	vydavatelská
společností	společnost	k1gFnPc2	společnost
s	s	k7c7	s
nahrávkami	nahrávka	k1gFnPc7	nahrávka
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
získat	získat	k5eAaPmF	získat
nahrávací	nahrávací	k2eAgFnSc4d1	nahrávací
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1962	[number]	k4	1962
Beatles	beatles	k1gMnPc1	beatles
jeli	jet	k5eAaImAgMnP	jet
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
na	na	k7c4	na
přehrávky	přehrávka	k1gFnPc4	přehrávka
pro	pro	k7c4	pro
firmu	firma	k1gFnSc4	firma
Decca	Deccum	k1gNnSc2	Deccum
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
.	.	kIx.	.
</s>
<s>
Decca	Decca	k1gFnSc1	Decca
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
mezi	mezi	k7c7	mezi
Beatles	Beatles	k1gFnSc7	Beatles
a	a	k8xC	a
skupinou	skupina	k1gFnSc7	skupina
The	The	k1gFnPc2	The
Tremeloes	Tremeloesa	k1gFnPc2	Tremeloesa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dělala	dělat	k5eAaImAgFnS	dělat
konkurz	konkurz	k1gInSc4	konkurz
týž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
jako	jako	k8xC	jako
Beatles	Beatles	k1gFnSc4	Beatles
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
upřednostnila	upřednostnit	k5eAaPmAgFnS	upřednostnit
Tremeloes	Tremeloes	k1gInSc4	Tremeloes
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
kvůli	kvůli	k7c3	kvůli
dostupnosti	dostupnost	k1gFnSc3	dostupnost
(	(	kIx(	(
<g/>
Beatles	Beatles	k1gFnPc1	Beatles
byli	být	k5eAaImAgMnP	být
přespolní	přespolní	k2eAgInSc4d1	přespolní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1962	[number]	k4	1962
Beatles	Beatles	k1gFnPc2	Beatles
opět	opět	k6eAd1	opět
odjeli	odjet	k5eAaPmAgMnP	odjet
do	do	k7c2	do
Hamburku	Hamburk	k1gInSc2	Hamburk
a	a	k8xC	a
tentokrát	tentokrát	k6eAd1	tentokrát
měli	mít	k5eAaImAgMnP	mít
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
nově	nově	k6eAd1	nově
otevřeném	otevřený	k2eAgNnSc6d1	otevřené
Star	Star	kA	Star
Clubu	club	k1gInSc6	club
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
před	před	k7c7	před
jejich	jejich	k3xOp3gInSc7	jejich
příjezdem	příjezd	k1gInSc7	příjezd
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Hamburku	Hamburk	k1gInSc6	Hamburk
na	na	k7c4	na
krvácení	krvácení	k1gNnSc4	krvácení
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
Stuart	Stuarta	k1gFnPc2	Stuarta
Sutcliffe	Sutcliff	k1gInSc5	Sutcliff
<g/>
.	.	kIx.	.
</s>
<s>
Epsteinovi	Epstein	k1gMnSc3	Epstein
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
podařilo	podařit	k5eAaPmAgNnS	podařit
uspět	uspět	k5eAaPmF	uspět
u	u	k7c2	u
firmy	firma	k1gFnSc2	firma
Parlophone	Parlophon	k1gInSc5	Parlophon
(	(	kIx(	(
<g/>
odnož	odnož	k1gFnSc4	odnož
koncernu	koncern	k1gInSc2	koncern
EMI	EMI	kA	EMI
<g/>
)	)	kIx)	)
a	a	k8xC	a
producenta	producent	k1gMnSc2	producent
George	Georg	k1gMnSc2	Georg
Martina	Martin	k1gMnSc2	Martin
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
Beatles	beatles	k1gMnSc1	beatles
pozval	pozvat	k5eAaPmAgMnS	pozvat
ke	k	k7c3	k
zkoušce	zkouška	k1gFnSc3	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Zkouška	zkouška	k1gFnSc1	zkouška
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
rozchodu	rozchod	k1gInSc3	rozchod
s	s	k7c7	s
Petem	Pet	k1gMnSc7	Pet
Bestem	Best	k1gMnSc7	Best
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
George	Georg	k1gMnSc2	Georg
Martin	Martin	k1gInSc1	Martin
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
nebyl	být	k5eNaImAgInS	být
spokojen	spokojen	k2eAgMnSc1d1	spokojen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
post	post	k1gInSc4	post
bubeníka	bubeník	k1gMnSc2	bubeník
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
liverpoolských	liverpoolský	k2eAgMnPc2d1	liverpoolský
adeptů	adept	k1gMnPc2	adept
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
-	-	kIx~	-
včetně	včetně	k7c2	včetně
šestnáctiletého	šestnáctiletý	k2eAgInSc2d1	šestnáctiletý
Lewise	Lewise	k1gFnPc4	Lewise
Collinse	Collins	k1gMnSc2	Collins
<g/>
,	,	kIx,	,
budoucího	budoucí	k2eAgMnSc4d1	budoucí
Bodieho	Bodie	k1gMnSc4	Bodie
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
Profesionálové	profesionál	k1gMnPc1	profesionál
<g/>
)	)	kIx)	)
přišel	přijít	k5eAaPmAgInS	přijít
nakonec	nakonec	k6eAd1	nakonec
na	na	k7c6	na
konci	konec	k1gInSc6	konec
srpna	srpen	k1gInSc2	srpen
1962	[number]	k4	1962
Ringo	Ringo	k1gMnSc1	Ringo
Starr	Starr	k1gMnSc1	Starr
(	(	kIx(	(
<g/>
narozený	narozený	k2eAgMnSc1d1	narozený
jako	jako	k8xC	jako
Richard	Richard	k1gMnSc1	Richard
Starkey	Starkea	k1gFnSc2	Starkea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgMnSc1d3	nejstarší
hráč	hráč	k1gMnSc1	hráč
se	s	k7c7	s
zkušeností	zkušenost	k1gFnSc7	zkušenost
z	z	k7c2	z
několika	několik	k4yIc2	několik
dalších	další	k2eAgFnPc2d1	další
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1962	[number]	k4	1962
vydala	vydat	k5eAaPmAgFnS	vydat
skupina	skupina	k1gFnSc1	skupina
u	u	k7c2	u
společnosti	společnost	k1gFnSc2	společnost
EMI	EMI	kA	EMI
první	první	k4xOgInSc4	první
singl	singl	k1gInSc4	singl
s	s	k7c7	s
názvem	název	k1gInSc7	název
Love	lov	k1gInSc5	lov
Me	Me	k1gFnPc6	Me
Do	do	k7c2	do
(	(	kIx(	(
<g/>
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
píseň	píseň	k1gFnSc4	píseň
P.	P.	kA	P.
<g/>
S.	S.	kA	S.
I	i	k9	i
Love	lov	k1gInSc5	lov
You	You	k1gMnSc5	You
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
na	na	k7c4	na
17	[number]	k4	17
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
nejprodávanějších	prodávaný	k2eAgFnPc2d3	nejprodávanější
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
singl	singl	k1gInSc1	singl
Please	Pleasa	k1gFnSc6	Pleasa
Please	Pleasa	k1gFnSc3	Pleasa
Me	Me	k1gFnSc3	Me
/	/	kIx~	/
From	From	k1gInSc1	From
Me	Me	k1gFnPc2	Me
To	to	k9	to
You	You	k1gMnPc2	You
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
už	už	k6eAd1	už
velkým	velký	k2eAgInSc7d1	velký
hitem	hit	k1gInSc7	hit
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
až	až	k9	až
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
celonárodního	celonárodní	k2eAgInSc2d1	celonárodní
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
rovněž	rovněž	k6eAd1	rovněž
Please	Pleasa	k1gFnSc6	Pleasa
Please	Pleasa	k1gFnSc6	Pleasa
Me	Me	k1gFnSc1	Me
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
znamenalo	znamenat	k5eAaImAgNnS	znamenat
začátek	začátek	k1gInSc4	začátek
beatlemánie	beatlemánie	k1gFnSc2	beatlemánie
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Beatles	beatles	k1gMnPc1	beatles
také	také	k9	také
hráli	hrát	k5eAaImAgMnP	hrát
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c4	v
Cavern	Cavern	k1gInSc4	Cavern
Clubu	club	k1gInSc2	club
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
koncertovat	koncertovat	k5eAaImF	koncertovat
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
i	i	k9	i
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
1963	[number]	k4	1963
a	a	k8xC	a
1964	[number]	k4	1964
odehráli	odehrát	k5eAaPmAgMnP	odehrát
přes	přes	k7c4	přes
200	[number]	k4	200
velkých	velký	k2eAgInPc2d1	velký
koncertů	koncert	k1gInPc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
následovalo	následovat	k5eAaImAgNnS	následovat
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
turné	turné	k1gNnSc1	turné
po	po	k7c6	po
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
vystoupení	vystoupení	k1gNnSc1	vystoupení
v	v	k7c6	v
The	The	k1gFnSc6	The
Ed	Ed	k1gMnSc1	Ed
Sullivan	Sullivan	k1gMnSc1	Sullivan
Show	show	k1gFnSc2	show
(	(	kIx(	(
<g/>
Show	show	k1gFnSc2	show
Eda	Eda	k1gMnSc1	Eda
Sullivana	Sullivan	k1gMnSc2	Sullivan
<g/>
)	)	kIx)	)
a	a	k8xC	a
natočení	natočení	k1gNnSc2	natočení
prvního	první	k4xOgMnSc2	první
filmu	film	k1gInSc2	film
<g/>
:	:	kIx,	:
A	a	k9	a
Hard	Hard	k1gMnSc1	Hard
Day	Day	k1gMnSc1	Day
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Night	Nighta	k1gFnPc2	Nighta
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Perný	perný	k2eAgInSc4d1	perný
den	den	k1gInSc4	den
<g/>
)	)	kIx)	)
i	i	k9	i
stejnojmenné	stejnojmenný	k2eAgNnSc4d1	stejnojmenné
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
úspěchu	úspěch	k1gInSc3	úspěch
Beatles	Beatles	k1gFnSc2	Beatles
se	se	k3xPyFc4	se
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
žebříčku	žebříček	k1gInSc6	žebříček
hitparád	hitparáda	k1gFnPc2	hitparáda
začaly	začít	k5eAaPmAgFnP	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
i	i	k9	i
další	další	k2eAgFnPc1d1	další
britské	britský	k2eAgFnPc1d1	britská
rockové	rockový	k2eAgFnPc1d1	rocková
skupiny	skupina	k1gFnPc1	skupina
jako	jako	k8xS	jako
např.	např.	kA	např.
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stones	k1gInSc1	Stones
<g/>
,	,	kIx,	,
Gerry	Gerr	k1gInPc1	Gerr
&	&	k?	&
The	The	k1gFnSc2	The
Pacemakers	Pacemakersa	k1gFnPc2	Pacemakersa
nebo	nebo	k8xC	nebo
Dave	Dav	k1gInSc5	Dav
Clark	Clark	k1gInSc1	Clark
Five	Fiv	k1gInPc1	Fiv
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1965	[number]	k4	1965
byli	být	k5eAaImAgMnP	být
Beatles	beatles	k1gMnPc1	beatles
vyznamenáni	vyznamenat	k5eAaPmNgMnP	vyznamenat
Řádem	řád	k1gInSc7	řád
britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
a	a	k8xC	a
natočili	natočit	k5eAaBmAgMnP	natočit
další	další	k2eAgInSc4d1	další
film	film	k1gInSc4	film
Help	help	k1gInSc1	help
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Pomoc	pomoc	k1gFnSc1	pomoc
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vystoupení	vystoupení	k1gNnSc1	vystoupení
na	na	k7c4	na
Shea	She	k2eAgNnPc4d1	She
Stadium	stadium	k1gNnSc4	stadium
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
při	při	k7c6	při
druhém	druhý	k4xOgNnSc6	druhý
americkém	americký	k2eAgNnSc6d1	americké
turné	turné	k1gNnSc6	turné
<g/>
,	,	kIx,	,
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
rekordní	rekordní	k2eAgInSc4d1	rekordní
počet	počet	k1gInSc4	počet
55	[number]	k4	55
tisíc	tisíc	k4xCgInPc2	tisíc
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Hudebně	hudebně	k6eAd1	hudebně
se	se	k3xPyFc4	se
Beatles	beatles	k1gMnPc1	beatles
začali	začít	k5eAaPmAgMnP	začít
odklánět	odklánět	k5eAaImF	odklánět
od	od	k7c2	od
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
textů	text	k1gInPc2	text
o	o	k7c6	o
lásce	láska	k1gFnSc6	láska
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
experimentovat	experimentovat	k5eAaImF	experimentovat
se	s	k7c7	s
zvuky	zvuk	k1gInPc7	zvuk
různých	různý	k2eAgInPc2d1	různý
nástrojů	nástroj	k1gInPc2	nástroj
(	(	kIx(	(
<g/>
např.	např.	kA	např.
indický	indický	k2eAgInSc1d1	indický
strunný	strunný	k2eAgInSc1d1	strunný
nástroj	nástroj	k1gInSc1	nástroj
sitár	sitár	k1gInSc1	sitár
ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
Norwegian	Norwegian	k1gMnSc1	Norwegian
Wood	Wood	k1gMnSc1	Wood
<g/>
)	)	kIx)	)
i	i	k9	i
hraním	hranit	k5eAaImIp1nS	hranit
se	s	k7c7	s
smyčcovým	smyčcový	k2eAgInSc7d1	smyčcový
kvartetem	kvartet	k1gInSc7	kvartet
(	(	kIx(	(
<g/>
píseň	píseň	k1gFnSc1	píseň
Yesterday	Yesterdaa	k1gFnSc2	Yesterdaa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
s	s	k7c7	s
celým	celý	k2eAgInSc7d1	celý
orchestrem	orchestr	k1gInSc7	orchestr
(	(	kIx(	(
<g/>
A	a	k9	a
Day	Day	k1gFnSc1	Day
In	In	k1gFnSc1	In
The	The	k1gFnSc1	The
Life	Life	k1gFnSc1	Life
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
Beatles	Beatles	k1gFnPc2	Beatles
a	a	k8xC	a
už	už	k6eAd1	už
zavedených	zavedený	k2eAgFnPc2d1	zavedená
skupin	skupina	k1gFnPc2	skupina
jako	jako	k8xC	jako
The	The	k1gFnPc2	The
Beach	Beacha	k1gFnPc2	Beacha
Boys	boy	k1gMnPc2	boy
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
prosazovaly	prosazovat	k5eAaImAgFnP	prosazovat
i	i	k9	i
skupiny	skupina	k1gFnPc1	skupina
vycházející	vycházející	k2eAgFnPc1d1	vycházející
jak	jak	k8xS	jak
z	z	k7c2	z
hudebního	hudební	k2eAgInSc2d1	hudební
stylu	styl	k1gInSc2	styl
Beatles	Beatles	k1gFnPc2	Beatles
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
absorbující	absorbující	k2eAgInPc4d1	absorbující
jiné	jiný	k2eAgInPc4d1	jiný
vlivy	vliv	k1gInPc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
prosadily	prosadit	k5eAaPmAgFnP	prosadit
skupiny	skupina	k1gFnPc1	skupina
jako	jako	k8xC	jako
např.	např.	kA	např.
The	The	k1gFnSc1	The
Byrds	Byrds	k1gInSc1	Byrds
<g/>
,	,	kIx,	,
Buffalo	Buffalo	k1gMnSc1	Buffalo
Springfield	Springfield	k1gMnSc1	Springfield
<g/>
,	,	kIx,	,
Mamas	Mamas	k1gMnSc1	Mamas
&	&	k?	&
Papas	Papas	k1gMnSc1	Papas
<g/>
,	,	kIx,	,
Quicksilver	Quicksilver	k1gMnSc1	Quicksilver
Messenger	Messenger	k1gMnSc1	Messenger
Service	Service	k1gFnSc1	Service
<g/>
,	,	kIx,	,
Jefferson	Jefferson	k1gMnSc1	Jefferson
Airplane	Airplan	k1gMnSc5	Airplan
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
se	se	k3xPyFc4	se
na	na	k7c6	na
žebříčcích	žebříček	k1gInPc6	žebříček
umísťovali	umísťovat	k5eAaImAgMnP	umísťovat
vedle	vedle	k7c2	vedle
Beatles	Beatles	k1gFnSc2	Beatles
také	také	k9	také
"	"	kIx"	"
<g/>
syrovější	syrový	k2eAgInSc4d2	syrovější
<g/>
"	"	kIx"	"
Rolling	Rolling	k1gInSc4	Rolling
Stones	Stonesa	k1gFnPc2	Stonesa
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
bluesovější	bluesový	k2eAgFnSc4d2	bluesovější
<g/>
"	"	kIx"	"
The	The	k1gFnSc4	The
Yardbirds	Yardbirdsa	k1gFnPc2	Yardbirdsa
(	(	kIx(	(
<g/>
či	či	k8xC	či
jejich	jejich	k3xOp3gMnPc1	jejich
následovníci	následovník	k1gMnPc1	následovník
Cream	Cream	k1gInSc4	Cream
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc1	Hendrix
atd.	atd.	kA	atd.
Beatles	Beatles	k1gFnPc1	Beatles
mají	mít	k5eAaImIp3nP	mít
na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
kapely	kapela	k1gFnPc4	kapela
obrovský	obrovský	k2eAgInSc1d1	obrovský
vliv	vliv	k1gInSc1	vliv
svými	svůj	k3xOyFgInPc7	svůj
novátorskými	novátorský	k2eAgInPc7d1	novátorský
přístupy	přístup	k1gInPc7	přístup
k	k	k7c3	k
technice	technika	k1gFnSc3	technika
nahrávání	nahrávání	k1gNnSc2	nahrávání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
nechávají	nechávat	k5eAaImIp3nP	nechávat
sami	sám	k3xTgMnPc1	sám
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
<g/>
.	.	kIx.	.
</s>
<s>
Dokladem	doklad	k1gInSc7	doklad
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
nahrávání	nahrávání	k1gNnSc4	nahrávání
první	první	k4xOgFnSc2	první
desky	deska	k1gFnSc2	deska
Pink	pink	k2eAgInSc4d1	pink
Floyd	Floyd	k1gInSc4	Floyd
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Piper	Piper	k1gMnSc1	Piper
at	at	k?	at
the	the	k?	the
Gates	Gates	k1gMnSc1	Gates
of	of	k?	of
Down	Down	k1gMnSc1	Down
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
Beatles	Beatles	k1gFnSc2	Beatles
trávili	trávit	k5eAaImAgMnP	trávit
mnoho	mnoho	k6eAd1	mnoho
času	čas	k1gInSc2	čas
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
"	"	kIx"	"
<g/>
Sgt	Sgt	k1gFnPc6	Sgt
<g/>
.	.	kIx.	.
</s>
<s>
Pepper	Pepper	k1gInSc1	Pepper
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Lonely	Lonel	k1gMnPc7	Lonel
Hearts	Hearts	k1gInSc4	Hearts
Club	club	k1gInSc1	club
Band	banda	k1gFnPc2	banda
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
kapely	kapela	k1gFnPc1	kapela
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
navštěvovaly	navštěvovat	k5eAaImAgFnP	navštěvovat
a	a	k8xC	a
jistá	jistý	k2eAgFnSc1d1	jistá
podoba	podoba	k1gFnSc1	podoba
postupů	postup	k1gInPc2	postup
při	při	k7c6	při
větších	veliký	k2eAgFnPc6d2	veliký
plochách	plocha	k1gFnPc6	plocha
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
deskách	deska	k1gFnPc6	deska
není	být	k5eNaImIp3nS	být
náhodná	náhodný	k2eAgFnSc1d1	náhodná
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1966	[number]	k4	1966
pronesl	pronést	k5eAaPmAgMnS	pronést
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
při	při	k7c6	při
jednom	jeden	k4xCgInSc6	jeden
interview	interview	k1gNnSc6	interview
poznámku	poznámka	k1gFnSc4	poznámka
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Beatles	Beatles	k1gFnPc1	Beatles
jsou	být	k5eAaImIp3nP	být
populárnější	populární	k2eAgMnSc1d2	populárnější
než	než	k8xS	než
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
<g/>
,	,	kIx,	,
rozpoutá	rozpoutat	k5eAaPmIp3nS	rozpoutat
se	se	k3xPyFc4	se
vlna	vlna	k1gFnSc1	vlna
odporu	odpor	k1gInSc2	odpor
a	a	k8xC	a
rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
stanice	stanice	k1gFnSc2	stanice
v	v	k7c6	v
USA	USA	kA	USA
bojkotují	bojkotovat	k5eAaImIp3nP	bojkotovat
vysílání	vysílání	k1gNnSc4	vysílání
hudby	hudba	k1gFnSc2	hudba
Beatles	Beatles	k1gFnPc2	Beatles
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
Beatles	Beatles	k1gFnSc2	Beatles
přestali	přestat	k5eAaPmAgMnP	přestat
koncertovat	koncertovat	k5eAaImF	koncertovat
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1966	[number]	k4	1966
měli	mít	k5eAaImAgMnP	mít
poslední	poslední	k2eAgNnPc4d1	poslední
vystoupení	vystoupení	k1gNnPc4	vystoupení
ve	v	k7c4	v
Wembley	Wembley	k1gInPc4	Wembley
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
měli	mít	k5eAaImAgMnP	mít
poslední	poslední	k2eAgInSc4d1	poslední
oficiální	oficiální	k2eAgInSc4d1	oficiální
koncert	koncert	k1gInSc4	koncert
v	v	k7c4	v
Candlestick	Candlestick	k1gInSc4	Candlestick
Park	park	k1gInSc4	park
v	v	k7c6	v
San	San	k1gFnSc6	San
Francisku	Francisek	k1gInSc2	Francisek
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
dle	dle	k7c2	dle
kritiků	kritik	k1gMnPc2	kritik
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgNnPc2d3	nejlepší
alb	album	k1gNnPc2	album
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
Sgt	Sgt	k1gFnSc2	Sgt
<g/>
.	.	kIx.	.
</s>
<s>
Pepper	Pepper	k1gInSc1	Pepper
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Lonely	Lonel	k1gMnPc7	Lonel
Hearts	Hearts	k1gInSc4	Hearts
Club	club	k1gInSc4	club
Band	banda	k1gFnPc2	banda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
poznamenáno	poznamenat	k5eAaPmNgNnS	poznamenat
hudebními	hudební	k2eAgInPc7d1	hudební
studiovými	studiový	k2eAgInPc7d1	studiový
experimenty	experiment	k1gInPc7	experiment
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1967	[number]	k4	1967
zemřel	zemřít	k5eAaPmAgMnS	zemřít
manažer	manažer	k1gMnSc1	manažer
Brian	Brian	k1gMnSc1	Brian
Epstein	Epstein	k1gMnSc1	Epstein
po	po	k7c6	po
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
náhodném	náhodný	k2eAgNnSc6d1	náhodné
předávkování	předávkování	k1gNnSc6	předávkování
barbituráty	barbiturát	k1gInPc1	barbiturát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Epsteinově	Epsteinův	k2eAgFnSc6d1	Epsteinova
smrti	smrt	k1gFnSc6	smrt
Beatles	Beatles	k1gFnSc2	Beatles
natočili	natočit	k5eAaBmAgMnP	natočit
další	další	k2eAgInSc4d1	další
film	film	k1gInSc4	film
Magical	Magical	k1gFnSc2	Magical
Mystery	Myster	k1gInPc1	Myster
Tour	Toura	k1gFnPc2	Toura
a	a	k8xC	a
vydali	vydat	k5eAaPmAgMnP	vydat
i	i	k9	i
stejnojmenné	stejnojmenný	k2eAgNnSc4d1	stejnojmenné
album	album	k1gNnSc4	album
poznamenané	poznamenaný	k2eAgInPc4d1	poznamenaný
halucinogenními	halucinogenní	k2eAgInPc7d1	halucinogenní
prožitky	prožitek	k1gInPc7	prožitek
z	z	k7c2	z
užívání	užívání	k1gNnSc2	užívání
drog	droga	k1gFnPc2	droga
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
první	první	k4xOgInPc4	první
vážné	vážný	k2eAgInPc4d1	vážný
rozmíšky	rozmíšek	k1gInPc4	rozmíšek
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgMnPc7d1	jednotlivý
členy	člen	k1gMnPc7	člen
skupina	skupina	k1gFnSc1	skupina
natočila	natočit	k5eAaBmAgFnS	natočit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
pozoruhodné	pozoruhodný	k2eAgFnPc1d1	pozoruhodná
The	The	k1gFnPc1	The
Beatles	Beatles	k1gFnPc2	Beatles
(	(	kIx(	(
<g/>
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
také	také	k9	také
The	The	k1gFnSc7	The
White	Whit	k1gInSc5	Whit
Album	album	k1gNnSc4	album
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gInSc2	jeho
bílého	bílý	k2eAgInSc2d1	bílý
přebalu	přebal	k1gInSc2	přebal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ringo	Ringo	k1gMnSc1	Ringo
Starr	Starr	k1gMnSc1	Starr
během	během	k7c2	během
nahrávání	nahrávání	k1gNnSc2	nahrávání
alba	album	k1gNnSc2	album
krátkodobě	krátkodobě	k6eAd1	krátkodobě
opustil	opustit	k5eAaPmAgMnS	opustit
skupinu	skupina	k1gFnSc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
založili	založit	k5eAaPmAgMnP	založit
Beatles	beatles	k1gMnPc1	beatles
firmu	firma	k1gFnSc4	firma
Apple	Apple	kA	Apple
Corps	corps	k1gInSc4	corps
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
jablko	jablko	k1gNnSc1	jablko
<g/>
)	)	kIx)	)
-	-	kIx~	-
vlastní	vlastní	k2eAgFnSc4d1	vlastní
gramofonovou	gramofonový	k2eAgFnSc4d1	gramofonová
značku	značka	k1gFnSc4	značka
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc4	obchod
<g/>
,	,	kIx,	,
filmovou	filmový	k2eAgFnSc4d1	filmová
společnost	společnost	k1gFnSc4	společnost
atd.	atd.	kA	atd.
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
další	další	k2eAgInSc4d1	další
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
kreslený	kreslený	k2eAgInSc1d1	kreslený
<g/>
,	,	kIx,	,
Yellow	Yellow	k1gMnSc1	Yellow
Submarine	Submarin	k1gInSc5	Submarin
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
ponorka	ponorka	k1gFnSc1	ponorka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1969	[number]	k4	1969
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
skupinu	skupina	k1gFnSc4	skupina
kritický	kritický	k2eAgInSc4d1	kritický
a	a	k8xC	a
definitivně	definitivně	k6eAd1	definitivně
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
rozpadu	rozpad	k1gInSc3	rozpad
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
začal	začít	k5eAaPmAgMnS	začít
na	na	k7c4	na
nahrávání	nahrávání	k1gNnSc4	nahrávání
vodit	vodit	k5eAaImF	vodit
kontroverzní	kontroverzní	k2eAgFnSc4d1	kontroverzní
japonskou	japonský	k2eAgFnSc4d1	japonská
umělkyni	umělkyně	k1gFnSc4	umělkyně
a	a	k8xC	a
svoji	svůj	k3xOyFgFnSc4	svůj
druhou	druhý	k4xOgFnSc4	druhý
ženu	žena	k1gFnSc4	žena
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc4	onen
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
čemuž	což	k3yRnSc3	což
protestoval	protestovat	k5eAaBmAgMnS	protestovat
hlavně	hlavně	k9	hlavně
Paul	Paul	k1gMnSc1	Paul
McCartney	McCartnea	k1gFnSc2	McCartnea
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
natočil	natočit	k5eAaBmAgMnS	natočit
album	album	k1gNnSc4	album
Unfinished	Unfinished	k1gMnSc1	Unfinished
Music	Music	k1gMnSc1	Music
No	no	k9	no
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
:	:	kIx,	:
Two	Two	k1gFnSc1	Two
Virgins	Virginsa	k1gFnPc2	Virginsa
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
zabývat	zabývat	k5eAaImF	zabývat
vlastními	vlastní	k2eAgInPc7d1	vlastní
"	"	kIx"	"
<g/>
nebeatlovskými	beatlovský	k2eNgInPc7d1	beatlovský
<g/>
"	"	kIx"	"
hudebními	hudební	k2eAgInPc7d1	hudební
projekty	projekt	k1gInPc7	projekt
(	(	kIx(	(
<g/>
založil	založit	k5eAaPmAgInS	založit
skupinu	skupina	k1gFnSc4	skupina
Plastic	Plastice	k1gFnPc2	Plastice
Ono	onen	k3xDgNnSc1	onen
Band	banda	k1gFnPc2	banda
<g/>
)	)	kIx)	)
a	a	k8xC	a
protesty	protest	k1gInPc1	protest
proti	proti	k7c3	proti
válce	válka	k1gFnSc3	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Beatles	Beatles	k1gFnPc1	Beatles
jako	jako	k8xS	jako
skupina	skupina	k1gFnSc1	skupina
spolu	spolu	k6eAd1	spolu
naposledy	naposledy	k6eAd1	naposledy
veřejně	veřejně	k6eAd1	veřejně
koncertovali	koncertovat	k5eAaImAgMnP	koncertovat
na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
svého	svůj	k3xOyFgNnSc2	svůj
nahrávacího	nahrávací	k2eAgNnSc2d1	nahrávací
studia	studio	k1gNnSc2	studio
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
i	i	k9	i
poslední	poslední	k2eAgInSc1d1	poslední
film	film	k1gInSc1	film
a	a	k8xC	a
LP	LP	kA	LP
Let	let	k1gInSc1	let
It	It	k1gMnSc2	It
Be	Be	k1gMnSc2	Be
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jejího	její	k3xOp3gNnSc2	její
natáčení	natáčení	k1gNnSc2	natáčení
po	po	k7c6	po
hádkách	hádka	k1gFnPc6	hádka
s	s	k7c7	s
McCartneym	McCartneym	k1gInSc1	McCartneym
i	i	k9	i
Lennonem	Lennon	k1gInSc7	Lennon
dočasně	dočasně	k6eAd1	dočasně
skupinu	skupina	k1gFnSc4	skupina
opustil	opustit	k5eAaPmAgMnS	opustit
George	George	k1gFnSc4	George
Harrison	Harrison	k1gMnSc1	Harrison
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1969	[number]	k4	1969
skupina	skupina	k1gFnSc1	skupina
natočila	natočit	k5eAaBmAgFnS	natočit
své	svůj	k3xOyFgNnSc4	svůj
poslední	poslední	k2eAgNnSc4d1	poslední
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
Abbey	Abbea	k1gFnSc2	Abbea
Road	Roada	k1gFnPc2	Roada
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
nevydané	vydaný	k2eNgInPc4d1	nevydaný
snímky	snímek	k1gInPc4	snímek
Let	léto	k1gNnPc2	léto
It	It	k1gFnSc1	It
Be	Be	k1gFnSc1	Be
byly	být	k5eAaImAgFnP	být
předány	předat	k5eAaPmNgFnP	předat
hudebnímu	hudební	k2eAgMnSc3d1	hudební
producentovi	producent	k1gMnSc3	producent
Philu	Phil	k1gMnSc3	Phil
Spectorovi	Spector	k1gMnSc3	Spector
k	k	k7c3	k
finálnímu	finální	k2eAgNnSc3d1	finální
zpracování	zpracování	k1gNnSc3	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
věnoval	věnovat	k5eAaPmAgMnS	věnovat
pouze	pouze	k6eAd1	pouze
Plastic	Plastice	k1gFnPc2	Plastice
Ono	onen	k3xDgNnSc1	onen
Band	band	k1gInSc4	band
a	a	k8xC	a
svým	svůj	k3xOyFgFnPc3	svůj
mírovým	mírový	k2eAgFnPc3d1	mírová
aktivitám	aktivita	k1gFnPc3	aktivita
a	a	k8xC	a
o	o	k7c4	o
další	další	k2eAgInPc4d1	další
hudební	hudební	k2eAgInPc4d1	hudební
výboje	výboj	k1gInPc4	výboj
Beatles	Beatles	k1gFnSc2	Beatles
již	již	k9	již
ztratil	ztratit	k5eAaPmAgInS	ztratit
zájem	zájem	k1gInSc1	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
poslechu	poslech	k1gInSc6	poslech
zpracovaných	zpracovaný	k2eAgFnPc2d1	zpracovaná
skladeb	skladba	k1gFnPc2	skladba
alba	alba	k1gFnSc1	alba
Let	let	k1gInSc1	let
It	It	k1gMnSc1	It
Be	Be	k1gFnPc2	Be
požádal	požádat	k5eAaPmAgMnS	požádat
Paul	Paul	k1gMnSc1	Paul
McCartney	McCartnea	k1gFnSc2	McCartnea
o	o	k7c4	o
odstranění	odstranění	k1gNnSc4	odstranění
dle	dle	k7c2	dle
jeho	jeho	k3xOp3gInSc2	jeho
názoru	názor	k1gInSc2	názor
nemístných	místný	k2eNgFnPc2d1	nemístná
úprav	úprava	k1gFnPc2	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
vyhověno	vyhověn	k2eAgNnSc1d1	vyhověno
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1970	[number]	k4	1970
byl	být	k5eAaImAgMnS	být
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
oficiálně	oficiálně	k6eAd1	oficiálně
odešel	odejít	k5eAaPmAgMnS	odejít
a	a	k8xC	a
veřejnost	veřejnost	k1gFnSc1	veřejnost
se	se	k3xPyFc4	se
tak	tak	k9	tak
poprvé	poprvé	k6eAd1	poprvé
dozvěděla	dozvědět	k5eAaPmAgFnS	dozvědět
o	o	k7c6	o
definitivním	definitivní	k2eAgInSc6d1	definitivní
rozchodu	rozchod	k1gInSc6	rozchod
Beatles	Beatles	k1gFnPc2	Beatles
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgNnSc6	jenž
se	se	k3xPyFc4	se
již	již	k6eAd1	již
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
šuškalo	šuškat	k5eAaImAgNnS	šuškat
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
byl	být	k5eAaImAgMnS	být
obviňován	obviňovat	k5eAaImNgMnS	obviňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
opustil	opustit	k5eAaPmAgMnS	opustit
Beatles	Beatles	k1gFnSc4	Beatles
(	(	kIx(	(
<g/>
jakkoli	jakkoli	k6eAd1	jakkoli
právě	právě	k9	právě
jemu	on	k3xPp3gNnSc3	on
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c6	o
udržení	udržení	k1gNnSc6	udržení
skupiny	skupina	k1gFnSc2	skupina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yRnSc4	což
reagoval	reagovat	k5eAaBmAgMnS	reagovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Beatles	beatles	k1gMnPc1	beatles
opustili	opustit	k5eAaPmAgMnP	opustit
sami	sám	k3xTgMnPc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
<g/>
Paul	Paul	k1gMnSc1	Paul
McCartney	McCartnea	k1gFnSc2	McCartnea
podal	podat	k5eAaPmAgMnS	podat
k	k	k7c3	k
Nejvyššímu	vysoký	k2eAgInSc3d3	Nejvyšší
soudu	soud	k1gInSc3	soud
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1970	[number]	k4	1970
žádost	žádost	k1gFnSc1	žádost
o	o	k7c4	o
oficiální	oficiální	k2eAgNnSc4d1	oficiální
ukončení	ukončení	k1gNnSc4	ukončení
partnerství	partnerství	k1gNnSc2	partnerství
Beatles	Beatles	k1gFnPc2	Beatles
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
sólovou	sólový	k2eAgFnSc4d1	sólová
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
objevovaly	objevovat	k5eAaImAgFnP	objevovat
spekulace	spekulace	k1gFnPc1	spekulace
o	o	k7c6	o
chystaném	chystaný	k2eAgNnSc6d1	chystané
znovuspojení	znovuspojení	k1gNnSc6	znovuspojení
Beatles	Beatles	k1gFnPc2	Beatles
<g/>
.	.	kIx.	.
</s>
<s>
Bývalí	bývalý	k2eAgMnPc1d1	bývalý
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
tyto	tento	k3xDgFnPc4	tento
teorie	teorie	k1gFnPc4	teorie
buď	buď	k8xC	buď
odmítali	odmítat	k5eAaImAgMnP	odmítat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ignorovali	ignorovat	k5eAaImAgMnP	ignorovat
<g/>
.	.	kIx.	.
</s>
<s>
Tečku	tečka	k1gFnSc4	tečka
za	za	k7c7	za
těmito	tento	k3xDgInPc7	tento
dohady	dohad	k1gInPc7	dohad
ukončila	ukončit	k5eAaPmAgFnS	ukončit
násilná	násilný	k2eAgFnSc1d1	násilná
smrt	smrt	k1gFnSc1	smrt
Johna	John	k1gMnSc2	John
Lennona	Lennon	k1gMnSc2	Lennon
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
před	před	k7c7	před
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
nájemního	nájemní	k2eAgInSc2d1	nájemní
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žil	žít	k5eAaImAgInS	žít
s	s	k7c7	s
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
a	a	k8xC	a
druhým	druhý	k4xOgMnSc7	druhý
synem	syn	k1gMnSc7	syn
Seanem	Sean	k1gInSc7	Sean
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
předala	předat	k5eAaPmAgFnS	předat
Yoko	Yoko	k1gNnSc4	Yoko
Ono	onen	k3xDgNnSc1	onen
McCartneymu	McCartneym	k1gInSc2	McCartneym
demosnímky	demosnímek	k1gInPc7	demosnímek
Lennonových	Lennonův	k2eAgFnPc2d1	Lennonova
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yQgFnPc3	který
zbylí	zbylý	k2eAgMnPc1d1	zbylý
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
nahráli	nahrát	k5eAaPmAgMnP	nahrát
doprovod	doprovod	k1gInSc4	doprovod
a	a	k8xC	a
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
tak	tak	k6eAd1	tak
poslední	poslední	k2eAgFnPc1d1	poslední
písně	píseň	k1gFnPc1	píseň
Real	Real	k1gInSc1	Real
Love	lov	k1gInSc5	lov
a	a	k8xC	a
Free	Free	k1gNnPc1	Free
as	as	k1gNnSc1	as
a	a	k8xC	a
Bird	Bird	k1gInSc4	Bird
vydané	vydaný	k2eAgMnPc4d1	vydaný
na	na	k7c6	na
albech	album	k1gNnPc6	album
Anthology	Antholog	k1gMnPc4	Antholog
1	[number]	k4	1
(	(	kIx(	(
<g/>
Apple	Apple	kA	Apple
Records	Records	k1gInSc1	Records
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Anthology	Antholog	k1gMnPc4	Antholog
2	[number]	k4	2
(	(	kIx(	(
<g/>
Apple	Apple	kA	Apple
Records	Records	k1gInSc1	Records
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Anthology	Antholog	k1gMnPc4	Antholog
3	[number]	k4	3
(	(	kIx(	(
<g/>
Apple	Apple	kA	Apple
Records	Records	k1gInSc1	Records
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
svoji	svůj	k3xOyFgFnSc4	svůj
hvězdu	hvězda	k1gFnSc4	hvězda
na	na	k7c6	na
Hollywoodském	hollywoodský	k2eAgInSc6d1	hollywoodský
chodníku	chodník	k1gInSc6	chodník
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Neustálé	neustálý	k2eAgInPc1d1	neustálý
nároky	nárok	k1gInPc1	nárok
členů	člen	k1gMnPc2	člen
Beatles	Beatles	k1gFnPc2	Beatles
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
nového	nový	k2eAgInSc2d1	nový
zvuku	zvuk	k1gInSc2	zvuk
kapely	kapela	k1gFnSc2	kapela
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
nové	nový	k2eAgFnSc6d1	nová
desce	deska	k1gFnSc6	deska
<g/>
,	,	kIx,	,
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
aranžérskými	aranžérský	k2eAgFnPc7d1	aranžérská
schopnostmi	schopnost	k1gFnPc7	schopnost
George	Georg	k1gMnSc2	Georg
Martina	Martin	k1gMnSc2	Martin
a	a	k8xC	a
odbornými	odborný	k2eAgFnPc7d1	odborná
znalostmi	znalost	k1gFnPc7	znalost
techniků	technik	k1gMnPc2	technik
EMI	EMI	kA	EMI
jako	jako	k9	jako
byli	být	k5eAaImAgMnP	být
Norman	Norman	k1gMnSc1	Norman
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
Ken	Ken	k1gMnSc1	Ken
Townshed	Townshed	k1gMnSc1	Townshed
a	a	k8xC	a
Geoff	Geoff	k1gMnSc1	Geoff
Emerick	Emerick	k1gMnSc1	Emerick
<g/>
,	,	kIx,	,
hrály	hrát	k5eAaImAgInP	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
novátorském	novátorský	k2eAgInSc6d1	novátorský
zvuku	zvuk	k1gInSc6	zvuk
alb	alba	k1gFnPc2	alba
Rubber	Rubbra	k1gFnPc2	Rubbra
Soul	Soul	k1gInSc1	Soul
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Revolver	revolver	k1gInSc1	revolver
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sgt	Sgt	k1gFnSc1	Sgt
<g/>
.	.	kIx.	.
</s>
<s>
Pepper	Pepper	k1gInSc1	Pepper
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Lonely	Lonel	k1gMnPc7	Lonel
Hearts	Hearts	k1gInSc4	Hearts
Club	club	k1gInSc1	club
Band	banda	k1gFnPc2	banda
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Beatles	beatles	k1gMnPc1	beatles
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
čerpání	čerpání	k1gNnSc6	čerpání
vlivů	vliv	k1gInPc2	vliv
dlouho	dlouho	k6eAd1	dlouho
po	po	k7c6	po
jejich	jejich	k3xOp3gInSc6	jejich
úvodním	úvodní	k2eAgInSc6d1	úvodní
úspěchu	úspěch	k1gInSc6	úspěch
<g/>
,	,	kIx,	,
a	a	k8xC	a
často	často	k6eAd1	často
nalézali	nalézat	k5eAaImAgMnP	nalézat
nové	nový	k2eAgFnPc4d1	nová
cesty	cesta	k1gFnPc4	cesta
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
a	a	k8xC	a
textech	text	k1gInPc6	text
díky	díky	k7c3	díky
poslechu	poslech	k1gInSc3	poslech
svých	svůj	k3xOyFgMnPc2	svůj
současníků	současník	k1gMnPc2	současník
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tyto	tento	k3xDgInPc4	tento
vlivy	vliv	k1gInPc4	vliv
patřil	patřit	k5eAaImAgMnS	patřit
Bob	Bob	k1gMnSc1	Bob
Dylan	Dylan	k1gMnSc1	Dylan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
takové	takový	k3xDgFnPc4	takový
písně	píseň	k1gFnPc4	píseň
jako	jako	k8xC	jako
např.	např.	kA	např.
You	You	k1gFnSc1	You
<g/>
'	'	kIx"	'
<g/>
ve	v	k7c6	v
Got	Got	k1gFnSc6	Got
to	ten	k3xDgNnSc1	ten
Hide	Hide	k1gNnSc1	Hide
Your	Youra	k1gFnPc2	Youra
Love	lov	k1gInSc5	lov
Away	Awaa	k1gFnSc2	Awaa
a	a	k8xC	a
Norwegian	Norwegian	k1gMnSc1	Norwegian
Wood	Wood	k1gMnSc1	Wood
(	(	kIx(	(
<g/>
This	This	k1gInSc1	This
Bird	Birda	k1gFnPc2	Birda
Has	hasit	k5eAaImRp2nS	hasit
Flown	Flown	k1gInSc1	Flown
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgMnPc4d1	další
současníky	současník	k1gMnPc4	současník
můžeme	moct	k5eAaImIp1nP	moct
zařadit	zařadit	k5eAaPmF	zařadit
The	The	k1gFnSc4	The
Byrds	Byrdsa	k1gFnPc2	Byrdsa
a	a	k8xC	a
The	The	k1gFnPc2	The
Beach	Beacha	k1gFnPc2	Beacha
Boys	boy	k1gMnPc2	boy
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
album	album	k1gNnSc1	album
Pet	Pet	k1gFnSc2	Pet
Sounds	Soundsa	k1gFnPc2	Soundsa
bylo	být	k5eAaImAgNnS	být
McCartneyho	McCartney	k1gMnSc2	McCartney
oblíbené	oblíbený	k2eAgNnSc4d1	oblíbené
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
triky	trik	k1gInPc7	trik
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
jako	jako	k8xC	jako
byly	být	k5eAaImAgInP	být
zvukové	zvukový	k2eAgInPc1d1	zvukový
efekty	efekt	k1gInPc1	efekt
<g/>
,	,	kIx,	,
netradiční	tradiční	k2eNgInPc1d1	netradiční
umístění	umístění	k1gNnSc4	umístění
mikrofonu	mikrofon	k1gInSc2	mikrofon
<g/>
,	,	kIx,	,
nahrávací	nahrávací	k2eAgFnPc1d1	nahrávací
smyčky	smyčka	k1gFnPc1	smyčka
<g/>
,	,	kIx,	,
dvoustopé	dvoustopý	k2eAgFnPc1d1	dvoustopá
a	a	k8xC	a
různě	různě	k6eAd1	různě
rychlé	rychlý	k2eAgNnSc4d1	rychlé
nahrávání	nahrávání	k1gNnSc4	nahrávání
<g/>
,	,	kIx,	,
začali	začít	k5eAaPmAgMnP	začít
Beatles	beatles	k1gMnPc1	beatles
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
své	svůj	k3xOyFgFnPc4	svůj
nahrávky	nahrávka	k1gFnPc4	nahrávka
o	o	k7c4	o
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
pro	pro	k7c4	pro
rockovou	rockový	k2eAgFnSc4d1	rocková
hudbu	hudba	k1gFnSc4	hudba
neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
<g/>
.	.	kIx.	.
</s>
<s>
Patřily	patřit	k5eAaImAgInP	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gFnPc4	on
smyčcové	smyčcový	k2eAgFnPc4d1	smyčcová
a	a	k8xC	a
dechové	dechový	k2eAgFnPc4d1	dechová
orchestry	orchestra	k1gFnPc4	orchestra
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
indické	indický	k2eAgInPc4d1	indický
nástroje	nástroj	k1gInPc4	nástroj
jako	jako	k8xS	jako
např.	např.	kA	např.
sitár	sitár	k1gInSc1	sitár
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
Norwegian	Norwegian	k1gMnSc1	Norwegian
Wood	Wood	k1gMnSc1	Wood
(	(	kIx(	(
<g/>
This	This	k1gInSc1	This
Bird	Birda	k1gFnPc2	Birda
Has	hasit	k5eAaImRp2nS	hasit
Flown	Flown	k1gNnSc4	Flown
<g/>
)	)	kIx)	)
a	a	k8xC	a
swarmandel	swarmandlo	k1gNnPc2	swarmandlo
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
Strawberry	Strawberra	k1gFnSc2	Strawberra
Fields	Fieldsa	k1gFnPc2	Fieldsa
Forever	Forever	k1gMnSc1	Forever
<g/>
.	.	kIx.	.
</s>
<s>
Využívali	využívat	k5eAaPmAgMnP	využívat
také	také	k9	také
elektronické	elektronický	k2eAgInPc4d1	elektronický
nástroje	nástroj	k1gInPc4	nástroj
jako	jako	k8xS	jako
byl	být	k5eAaImAgMnS	být
např.	např.	kA	např.
mellotron	mellotron	k1gInSc1	mellotron
s	s	k7c7	s
jehož	jehož	k3xOyRp3gFnSc7	jehož
pomocí	pomoc	k1gFnSc7	pomoc
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
McCartney	McCartne	k1gMnPc4	McCartne
flétny	flétna	k1gFnSc2	flétna
v	v	k7c6	v
úvodu	úvod	k1gInSc6	úvod
písně	píseň	k1gFnPc4	píseň
Strawberry	Strawberra	k1gFnSc2	Strawberra
Fields	Fieldsa	k1gFnPc2	Fieldsa
Forever	Forever	k1gInSc1	Forever
<g/>
,	,	kIx,	,
a	a	k8xC	a
Ondioline	Ondiolin	k1gInSc5	Ondiolin
<g/>
,	,	kIx,	,
elektronické	elektronický	k2eAgFnSc2d1	elektronická
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
produkovaly	produkovat	k5eAaImAgFnP	produkovat
neobvyklý	obvyklý	k2eNgInSc4d1	neobvyklý
zvuk	zvuk	k1gInSc4	zvuk
podobný	podobný	k2eAgInSc4d1	podobný
hoboji	hoboj	k1gInPc7	hoboj
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
Baby	baba	k1gFnSc2	baba
<g/>
,	,	kIx,	,
You	You	k1gFnSc2	You
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
a	a	k8xC	a
Rich	Rich	k1gMnSc1	Rich
Man	Man	k1gMnSc1	Man
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
článek	článek	k1gInSc1	článek
<g/>
:	:	kIx,	:
Diskografie	diskografie	k1gFnSc1	diskografie
The	The	k1gFnSc2	The
Beatles	Beatles	k1gFnSc2	Beatles
Please	Pleasa	k1gFnSc3	Pleasa
Please	Pleasa	k1gFnSc3	Pleasa
Me	Me	k1gFnSc3	Me
(	(	kIx(	(
<g/>
Parlophone	Parlophon	k1gMnSc5	Parlophon
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
With	With	k1gMnSc1	With
The	The	k1gMnSc1	The
Beatles	beatles	k1gMnSc1	beatles
(	(	kIx(	(
<g/>
Parlophone	Parlophon	k1gMnSc5	Parlophon
<g/>
,	,	kIx,	,
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
A	a	k8xC	a
Hard	Hard	k1gMnSc1	Hard
Day	Day	k1gMnSc1	Day
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Night	Nighta	k1gFnPc2	Nighta
(	(	kIx(	(
<g/>
Parlophone	Parlophon	k1gMnSc5	Parlophon
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
Beatles	Beatles	k1gFnPc2	Beatles
for	forum	k1gNnPc2	forum
Sale	Sale	k1gInSc1	Sale
(	(	kIx(	(
<g/>
Parlophone	Parlophon	k1gMnSc5	Parlophon
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
Help	help	k1gInSc1	help
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Parlophone	Parlophon	k1gMnSc5	Parlophon
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
Rubber	Rubber	k1gInSc1	Rubber
Soul	Soul	k1gInSc1	Soul
(	(	kIx(	(
<g/>
Parlophone	Parlophon	k1gMnSc5	Parlophon
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
Revolver	revolver	k1gInSc4	revolver
(	(	kIx(	(
<g/>
Parlophone	Parlophon	k1gMnSc5	Parlophon
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
Sgt	Sgt	k1gFnSc2	Sgt
<g/>
.	.	kIx.	.
</s>
<s>
Pepper	Pepper	k1gInSc1	Pepper
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Lonely	Lonel	k1gMnPc7	Lonel
Hearts	Hearts	k1gInSc1	Hearts
Club	club	k1gInSc1	club
Band	band	k1gInSc4	band
(	(	kIx(	(
<g/>
Parlophone	Parlophon	k1gMnSc5	Parlophon
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
Magical	Magical	k1gFnSc2	Magical
Mystery	Myster	k1gInPc1	Myster
Tour	Toura	k1gFnPc2	Toura
(	(	kIx(	(
<g/>
Parlophone	Parlophon	k1gMnSc5	Parlophon
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Nejprve	nejprve	k6eAd1	nejprve
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
UK	UK	kA	UK
jako	jako	k8xS	jako
Double	double	k2eAgFnSc7d1	double
EP	EP	kA	EP
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
až	až	k9	až
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1976	[number]	k4	1976
poprvé	poprvé	k6eAd1	poprvé
jako	jako	k8xS	jako
LP	LP	kA	LP
<g/>
!	!	kIx.	!
</s>
<s>
The	The	k?	The
Beatles	Beatles	k1gFnSc4	Beatles
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
White	Whit	k1gMnSc5	Whit
Album	album	k1gNnSc1	album
<g/>
;	;	kIx,	;
Apple	Apple	kA	Apple
/	/	kIx~	/
Parlophone	Parlophon	k1gMnSc5	Parlophon
<g/>
,	,	kIx,	,
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Yellow	Yellow	k1gFnSc1	Yellow
Submarine	Submarin	k1gInSc5	Submarin
(	(	kIx(	(
<g/>
Apple	Apple	kA	Apple
/	/	kIx~	/
Parlophone	Parlophon	k1gMnSc5	Parlophon
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Abbey	Abbea	k1gFnSc2	Abbea
Road	Road	k1gMnSc1	Road
(	(	kIx(	(
<g/>
Apple	Apple	kA	Apple
/	/	kIx~	/
Parlophone	Parlophon	k1gMnSc5	Parlophon
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g />
.	.	kIx.	.
</s>
<s>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Let	let	k1gInSc1	let
It	It	k1gMnSc2	It
Be	Be	k1gMnSc2	Be
(	(	kIx(	(
<g/>
Apple	Apple	kA	Apple
/	/	kIx~	/
Parlophone	Parlophon	k1gMnSc5	Parlophon
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2003	[number]	k4	2003
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
odlišném	odlišný	k2eAgInSc6d1	odlišný
mixu	mix	k1gInSc6	mix
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Let	léto	k1gNnPc2	léto
It	It	k1gMnSc1	It
Be	Be	k1gMnSc1	Be
<g/>
...	...	k?	...
Naked	Naked	k1gInSc1	Naked
A	a	k8xC	a
Collection	Collection	k1gInSc1	Collection
Of	Of	k1gFnSc2	Of
Beatles	Beatles	k1gFnPc2	Beatles
Oldies	Oldies	k1gMnSc1	Oldies
But	But	k1gMnSc1	But
Goldies	Goldies	k1gMnSc1	Goldies
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Parlophone	Parlophon	k1gMnSc5	Parlophon
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
Hey	Hey	k1gFnSc1	Hey
Jude	Jude	k1gFnSc1	Jude
(	(	kIx(	(
<g/>
Apple	Apple	kA	Apple
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
únor	únor	k1gInSc1	únor
1970	[number]	k4	1970
<g/>
;	;	kIx,	;
původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Beatles	beatles	k1gMnSc1	beatles
Again	Again	k1gMnSc1	Again
<g/>
)	)	kIx)	)
1962	[number]	k4	1962
-	-	kIx~	-
1966	[number]	k4	1966
(	(	kIx(	(
<g/>
Apple	Apple	kA	Apple
<g/>
,	,	kIx,	,
Red	Red	k1gMnPc1	Red
Album	album	k1gNnSc1	album
<g/>
;	;	kIx,	;
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g />
.	.	kIx.	.
</s>
<s>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
1967	[number]	k4	1967
-	-	kIx~	-
1970	[number]	k4	1970
(	(	kIx(	(
<g/>
Apple	Apple	kA	Apple
<g/>
,	,	kIx,	,
Blue	Blue	k1gNnSc1	Blue
Album	album	k1gNnSc1	album
<g/>
;	;	kIx,	;
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Rock	rock	k1gInSc1	rock
'	'	kIx"	'
<g/>
N	N	kA	N
<g/>
'	'	kIx"	'
Roll	Roll	k1gMnSc1	Roll
Music	Music	k1gMnSc1	Music
(	(	kIx(	(
<g/>
Parlophone	Parlophon	k1gMnSc5	Parlophon
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
The	The	k1gFnSc6	The
Beatles	Beatles	k1gFnPc2	Beatles
At	At	k1gFnSc2	At
The	The	k1gMnPc2	The
Hollywood	Hollywood	k1gInSc1	Hollywood
Bowl	Bowla	k1gFnPc2	Bowla
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Parlophone	Parlophon	k1gMnSc5	Parlophon
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
Love	lov	k1gInSc5	lov
Songs	Songs	k1gInSc4	Songs
(	(	kIx(	(
<g/>
Parlophone	Parlophon	k1gMnSc5	Parlophon
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
The	The	k1gFnSc6	The
Beatles	Beatles	k1gFnPc2	Beatles
Collection	Collection	k1gInSc1	Collection
(	(	kIx(	(
<g/>
Box	box	k1gInSc1	box
set	set	k1gInSc1	set
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Rarities	Raritiesa	k1gFnPc2	Raritiesa
<g/>
;	;	kIx,	;
EMI	EMI	kA	EMI
/	/	kIx~	/
Parlophone	Parlophon	k1gMnSc5	Parlophon
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1978	[number]	k4	1978
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
The	The	k1gFnSc1	The
Beatles	Beatles	k1gFnPc2	Beatles
<g/>
'	'	kIx"	'
Ballads	Ballads	k1gInSc4	Ballads
(	(	kIx(	(
<g/>
Parlophone	Parlophon	k1gMnSc5	Parlophon
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
20	[number]	k4	20
Greatest	Greatest	k1gInSc1	Greatest
Hits	Hits	k1gInSc4	Hits
(	(	kIx(	(
<g/>
Parlophone	Parlophon	k1gMnSc5	Parlophon
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
Reel	Reel	k1gMnSc1	Reel
Music	Music	k1gMnSc1	Music
(	(	kIx(	(
<g/>
Parlophone	Parlophon	k1gMnSc5	Parlophon
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
Past	past	k1gFnSc1	past
Masters	Mastersa	k1gFnPc2	Mastersa
Volume	volum	k1gInSc5	volum
<g />
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
(	(	kIx(	(
<g/>
Parlophone	Parlophon	k1gMnSc5	Parlophon
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Past	past	k1gFnSc1	past
Masters	Mastersa	k1gFnPc2	Mastersa
Volume	volum	k1gInSc5	volum
2	[number]	k4	2
(	(	kIx(	(
<g/>
Parlophone	Parlophon	k1gMnSc5	Parlophon
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Live	Liv	k1gInSc2	Liv
At	At	k1gMnSc1	At
The	The	k1gMnSc1	The
BBC	BBC	kA	BBC
(	(	kIx(	(
<g/>
Apple	Apple	kA	Apple
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Anthology	Antholog	k1gMnPc4	Antholog
1	[number]	k4	1
(	(	kIx(	(
<g/>
Apple	Apple	kA	Apple
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Anthology	Antholog	k1gMnPc4	Antholog
2	[number]	k4	2
(	(	kIx(	(
<g/>
Apple	Apple	kA	Apple
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Anthology	Antholog	k1gMnPc4	Antholog
3	[number]	k4	3
(	(	kIx(	(
<g/>
Apple	Apple	kA	Apple
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Yellow	Yellow	k1gFnSc1	Yellow
Submarine	Submarin	k1gInSc5	Submarin
Songtrack	Songtrack	k1gMnSc1	Songtrack
(	(	kIx(	(
<g/>
Apple	Apple	kA	Apple
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1999	[number]	k4	1999
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
1	[number]	k4	1
(	(	kIx(	(
<g/>
Apple	Apple	kA	Apple
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
Apple	Apple	kA	Apple
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Beatles	Beatles	k1gFnPc2	Beatles
Stereo	stereo	k6eAd1	stereo
Box	box	k1gInSc1	box
Set	set	k1gInSc1	set
-	-	kIx~	-
remasterováno	remasterován	k2eAgNnSc1d1	remasterován
(	(	kIx(	(
<g/>
EMI	EMI	kA	EMI
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
Tomorrow	Tomorrow	k1gFnSc1	Tomorrow
Never	Never	k1gMnSc1	Never
Knows	Knows	k1gInSc1	Knows
<g />
.	.	kIx.	.
</s>
<s>
24	[number]	k4	24
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
(	(	kIx(	(
<g/>
Apple	Apple	kA	Apple
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
;	;	kIx,	;
pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
digitální	digitální	k2eAgInSc4d1	digitální
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Perný	perný	k2eAgInSc1d1	perný
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
černobílý	černobílý	k2eAgInSc1d1	černobílý
celovečerní	celovečerní	k2eAgInSc4d1	celovečerní
hraný	hraný	k2eAgInSc4d1	hraný
film	film	k1gInSc4	film
Scénář	scénář	k1gInSc1	scénář
<g/>
:	:	kIx,	:
Alun	Alun	k1gInSc1	Alun
Owen	Owen	k1gInSc1	Owen
Kamera	kamera	k1gFnSc1	kamera
<g/>
:	:	kIx,	:
Gilbert	Gilbert	k1gMnSc1	Gilbert
Taylor	Taylor	k1gMnSc1	Taylor
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Richard	Richard	k1gMnSc1	Richard
Lester	Lester	k1gMnSc1	Lester
Dále	daleko	k6eAd2	daleko
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
John	John	k1gMnSc1	John
Junkin	Junkin	k1gMnSc1	Junkin
<g/>
,	,	kIx,	,
Norman	Norman	k1gMnSc1	Norman
Rossington	Rossington	k1gInSc1	Rossington
<g/>
,	,	kIx,	,
Wilfred	Wilfred	k1gMnSc1	Wilfred
Brambell	Brambell	k1gMnSc1	Brambell
<g/>
,	,	kIx,	,
Victor	Victor	k1gMnSc1	Victor
Spinetti	Spinetť	k1gFnSc2	Spinetť
Help	help	k1gInSc1	help
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
barevný	barevný	k2eAgInSc1d1	barevný
celovečerní	celovečerní	k2eAgInSc4d1	celovečerní
hraný	hraný	k2eAgInSc4d1	hraný
film	film	k1gInSc4	film
Scénář	scénář	k1gInSc1	scénář
<g/>
:	:	kIx,	:
Marc	Marc	k1gFnSc1	Marc
Behm	Behm	k1gInSc1	Behm
Kamera	kamera	k1gFnSc1	kamera
<g/>
:	:	kIx,	:
Gilbert	Gilbert	k1gMnSc1	Gilbert
Taylor	Taylor	k1gMnSc1	Taylor
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Richard	Richard	k1gMnSc1	Richard
Lester	Lester	k1gMnSc1	Lester
Dále	daleko	k6eAd2	daleko
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Eleanor	Eleanor	k1gMnSc1	Eleanor
Bron	Bron	k1gMnSc1	Bron
<g/>
,	,	kIx,	,
Leo	Leo	k1gMnSc1	Leo
McKern	McKern	k1gMnSc1	McKern
<g/>
,	,	kIx,	,
Roy	Roy	k1gMnSc1	Roy
Kinnear	Kinnear	k1gMnSc1	Kinnear
<g/>
,	,	kIx,	,
Victor	Victor	k1gMnSc1	Victor
Spinetti	Spinetť	k1gFnSc2	Spinetť
Yellow	Yellow	k1gMnSc1	Yellow
Submarine	Submarin	k1gInSc5	Submarin
<g/>
,	,	kIx,	,
barevný	barevný	k2eAgInSc1d1	barevný
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
Scénář	scénář	k1gInSc1	scénář
<g/>
:	:	kIx,	:
Lee	Lea	k1gFnSc3	Lea
<g />
.	.	kIx.	.
</s>
<s>
Minoff	Minoff	k1gInSc1	Minoff
<g/>
,	,	kIx,	,
Al	ala	k1gFnPc2	ala
Brodax	Brodax	k1gInSc1	Brodax
Výtvarník	výtvarník	k1gMnSc1	výtvarník
<g/>
:	:	kIx,	:
Heinz	Heinz	k1gMnSc1	Heinz
Edelmann	Edelmann	k1gMnSc1	Edelmann
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
George	George	k1gFnSc1	George
Dunning	Dunning	k1gInSc4	Dunning
Magical	Magical	k1gFnSc2	Magical
Mystery	Myster	k1gInPc1	Myster
Tour	Toura	k1gFnPc2	Toura
<g/>
,	,	kIx,	,
barevný	barevný	k2eAgInSc1d1	barevný
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
Námět	námět	k1gInSc1	námět
<g/>
,	,	kIx,	,
scénář	scénář	k1gInSc1	scénář
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Beatles	Beatles	k1gFnSc2	Beatles
Let	léto	k1gNnPc2	léto
It	It	k1gMnSc1	It
Be	Be	k1gMnSc1	Be
<g/>
,	,	kIx,	,
barevný	barevný	k2eAgInSc1d1	barevný
hraný	hraný	k2eAgInSc1d1	hraný
dokumentární	dokumentární	k2eAgInSc1d1	dokumentární
film	film	k1gInSc1	film
Kamera	kamera	k1gFnSc1	kamera
<g/>
:	:	kIx,	:
Tony	Tony	k1gFnSc1	Tony
Richards	Richards	k1gInSc1	Richards
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Michael	Michael	k1gMnSc1	Michael
Lindsay-Hogg	Lindsay-Hogg	k1gMnSc1	Lindsay-Hogg
Beatles	beatles	k1gMnSc1	beatles
Cartoon	Cartoon	k1gMnSc1	Cartoon
<g/>
,	,	kIx,	,
barevný	barevný	k2eAgInSc1d1	barevný
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
Matzner	Matzner	k1gMnSc1	Matzner
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
:	:	kIx,	:
Beatles	beatles	k1gMnSc1	beatles
<g/>
,	,	kIx,	,
výpověď	výpověď	k1gFnSc1	výpověď
o	o	k7c6	o
jedné	jeden	k4xCgFnSc6	jeden
generaci	generace	k1gFnSc6	generace
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
Fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
OCLC	OCLC	kA	OCLC
85618421	[number]	k4	85618421
<g/>
,	,	kIx,	,
OCLC	OCLC	kA	OCLC
39407088	[number]	k4	39407088
<g/>
;	;	kIx,	;
2	[number]	k4	2
<g/>
.	.	kIx.	.
dopl	dopla	k1gFnPc2	dopla
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-204-0581-X	[number]	k4	80-204-0581-X
Černá	černat	k5eAaImIp3nS	černat
Miroslava	Miroslava	k1gFnSc1	Miroslava
<g/>
,	,	kIx,	,
Černý	Černý	k1gMnSc1	Černý
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
:	:	kIx,	:
Poplach	poplach	k1gInSc1	poplach
kolem	kolem	k7c2	kolem
Beatles	Beatles	k1gFnSc2	Beatles
<g/>
.	.	kIx.	.
</s>
<s>
PANTON	PANTON	kA	PANTON
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
OCLC	OCLC	kA	OCLC
42161054	[number]	k4	42161054
Clifford	Cliffordo	k1gNnPc2	Cliffordo
Mike	Mike	k1gFnPc2	Mike
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Album	album	k1gNnSc1	album
Rocku	rock	k1gInSc2	rock
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladá	k1gFnSc2	mladá
letá	letý	k2eAgFnSc1d1	letá
(	(	kIx(	(
<g/>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-06-00394-7	[number]	k4	80-06-00394-7
Černý	Černý	k1gMnSc1	Černý
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
:	:	kIx,	:
Hvězdy	hvězda	k1gFnPc1	hvězda
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
hitparád	hitparáda	k1gFnPc2	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
PANTON	PANTON	kA	PANTON
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
OCLC	OCLC	kA	OCLC
24413638	[number]	k4	24413638
Bairdová	Bairdová	k1gFnSc1	Bairdová
Julie	Julie	k1gFnSc1	Julie
<g/>
,	,	kIx,	,
Giuliano	Giuliana	k1gFnSc5	Giuliana
Geoffrey	Geoffrea	k1gMnSc2	Geoffrea
<g/>
:	:	kIx,	:
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
-	-	kIx~	-
můj	můj	k3xOp1gMnSc1	můj
bratr	bratr	k1gMnSc1	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
filmový	filmový	k2eAgInSc1d1	filmový
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7004-072-6	[number]	k4	80-7004-072-6
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
Paul	Paul	k1gMnSc1	Paul
McCartney	McCartnea	k1gMnSc2	McCartnea
George	Georg	k1gMnSc2	Georg
Harrison	Harrison	k1gMnSc1	Harrison
Ringo	Ringo	k1gMnSc1	Ringo
Starr	Starr	k1gMnSc1	Starr
Brian	Brian	k1gMnSc1	Brian
Epstein	Epstein	k1gMnSc1	Epstein
George	George	k1gFnPc2	George
Martin	Martin	k1gMnSc1	Martin
Pete	Pet	k1gFnSc2	Pet
Best	Best	k1gMnSc1	Best
Stuart	Stuarta	k1gFnPc2	Stuarta
Sutcliffe	Sutcliff	k1gInSc5	Sutcliff
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
The	The	k1gFnSc2	The
Beatles	Beatles	k1gFnSc2	Beatles
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gFnSc1	téma
The	The	k1gFnSc2	The
Beatles	Beatles	k1gFnPc2	Beatles
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
The	The	k1gFnSc1	The
Beatles	Beatles	k1gFnPc2	Beatles
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
<g/>
,	,	kIx,	,
beatles	beatles	k1gMnSc1	beatles
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
Brouci	brouk	k1gMnPc1	brouk
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
Beatles-komplet	Beatlesomplet	k1gInSc1	Beatles-komplet
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Revoluce	revoluce	k1gFnSc1	revoluce
jménem	jméno	k1gNnSc7	jméno
Beatles	Beatles	k1gFnSc2	Beatles
-	-	kIx~	-
video	video	k1gNnSc1	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Historický	historický	k2eAgInSc1d1	historický
magazín	magazín	k1gInSc1	magazín
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Audio	audio	k2eAgMnSc1d1	audio
a	a	k8xC	a
video	video	k1gNnSc1	video
s	s	k7c7	s
The	The	k1gFnSc7	The
Beatles	Beatles	k1gFnPc2	Beatles
pro	pro	k7c4	pro
volnému	volný	k2eAgInSc3d1	volný
použití	použití	k1gNnSc3	použití
pod	pod	k7c7	pod
licencí	licence	k1gFnSc7	licence
Creative	Creativ	k1gInSc5	Creativ
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
