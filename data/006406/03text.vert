<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Grof	Grof	k1gMnSc1	Grof
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1931	[number]	k4	1931
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
psychiatr	psychiatr	k1gMnSc1	psychiatr
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
transpersonální	transpersonální	k2eAgFnSc2d1	transpersonální
psychologie	psychologie	k1gFnSc2	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
teoretického	teoretický	k2eAgInSc2d1	teoretický
nástinu	nástin	k1gInSc2	nástin
tohoto	tento	k3xDgNnSc2	tento
hnutí	hnutí	k1gNnSc2	hnutí
zkoumal	zkoumat	k5eAaImAgInS	zkoumat
také	také	k9	také
využití	využití	k1gNnSc4	využití
změněných	změněný	k2eAgInPc2d1	změněný
stavů	stav	k1gInPc2	stav
vědomí	vědomí	k1gNnSc2	vědomí
při	při	k7c6	při
léčení	léčení	k1gNnSc6	léčení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
objevitelem	objevitel	k1gMnSc7	objevitel
holotropního	holotropní	k2eAgNnSc2d1	holotropní
dýchání	dýchání	k1gNnSc2	dýchání
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaPmAgInS	napsat
několik	několik	k4yIc4	několik
knih	kniha	k1gFnPc2	kniha
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
psychospirituální	psychospirituální	k2eAgFnSc2d1	psychospirituální
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
mu	on	k3xPp3gMnSc3	on
přinesla	přinést	k5eAaPmAgFnS	přinést
několik	několik	k4yIc4	několik
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
,	,	kIx,	,
např.	např.	kA	např.
cenu	cena	k1gFnSc4	cena
VIZE	vize	k1gFnSc1	vize
97	[number]	k4	97
manželů	manžel	k1gMnPc2	manžel
Dagmar	Dagmar	k1gFnSc2	Dagmar
a	a	k8xC	a
Václava	Václav	k1gMnSc2	Václav
Havlových	Havlových	k2eAgMnSc2d1	Havlových
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
ho	on	k3xPp3gNnSc4	on
jeho	jeho	k3xOp3gMnPc1	jeho
kritikové	kritik	k1gMnPc1	kritik
obviňují	obviňovat	k5eAaImIp3nP	obviňovat
z	z	k7c2	z
podporování	podporování	k1gNnSc2	podporování
nevědecké	vědecký	k2eNgFnSc2d1	nevědecká
psychologie	psychologie	k1gFnSc2	psychologie
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nositelem	nositel	k1gMnSc7	nositel
dvou	dva	k4xCgInPc2	dva
Bludných	bludný	k2eAgInPc2d1	bludný
balvanů	balvan	k1gInPc2	balvan
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
Třebové	Třebová	k1gFnSc2	Třebová
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
katolické	katolický	k2eAgFnSc2d1	katolická
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
rodina	rodina	k1gFnSc1	rodina
jeho	on	k3xPp3gMnSc2	on
otce	otec	k1gMnSc2	otec
byla	být	k5eAaImAgFnS	být
bez	bez	k7c2	bez
náboženského	náboženský	k2eAgNnSc2d1	náboženské
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
malého	malý	k2eAgMnSc2d1	malý
Stanislava	Stanislav	k1gMnSc2	Stanislav
i	i	k8xC	i
jeho	jeho	k3xOp3gMnSc2	jeho
bratra	bratr	k1gMnSc2	bratr
budou	být	k5eAaImBp3nP	být
vést	vést	k5eAaImF	vést
k	k	k7c3	k
náboženství	náboženství	k1gNnSc3	náboženství
a	a	k8xC	a
nechají	nechat	k5eAaPmIp3nP	nechat
je	on	k3xPp3gFnPc4	on
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
se	se	k3xPyFc4	se
v	v	k7c6	v
pozdějším	pozdní	k2eAgInSc6d2	pozdější
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
našli	najít	k5eAaPmAgMnP	najít
svoji	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
cestu	cesta	k1gFnSc4	cesta
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
40	[number]	k4	40
<g/>
.	.	kIx.	.
a	a	k8xC	a
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
stát	stát	k5eAaImF	stát
psychoanalytikem	psychoanalytik	k1gMnSc7	psychoanalytik
–	–	k?	–
uchvátil	uchvátit	k5eAaPmAgMnS	uchvátit
ho	on	k3xPp3gInSc4	on
Freudův	Freudův	k2eAgInSc4d1	Freudův
Úvod	úvod	k1gInSc4	úvod
do	do	k7c2	do
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgInS	pracovat
ve	v	k7c6	v
Výzkumném	výzkumný	k2eAgInSc6d1	výzkumný
ústavu	ústav	k1gInSc6	ústav
psychiatrickém	psychiatrický	k2eAgInSc6d1	psychiatrický
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Bohnicích	Bohnice	k1gInPc6	Bohnice
pod	pod	k7c7	pod
doc.	doc.	kA	doc.
Roubíčkem	Roubíček	k1gMnSc7	Roubíček
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
výzkumu	výzkum	k1gInSc2	výzkum
zásilku	zásilka	k1gFnSc4	zásilka
LSD	LSD	kA	LSD
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
následně	následně	k6eAd1	následně
Grof	Grof	k1gMnSc1	Grof
vyzkoušel	vyzkoušet	k5eAaPmAgMnS	vyzkoušet
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Grof	Grof	k1gMnSc1	Grof
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
desítek	desítka	k1gFnPc2	desítka
lékařů	lékař	k1gMnPc2	lékař
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
s	s	k7c7	s
diethylamidem	diethylamid	k1gInSc7	diethylamid
kyseliny	kyselina	k1gFnSc2	kyselina
lysergové	lysergový	k2eAgFnSc2d1	lysergová
v	v	k7c6	v
socialistickém	socialistický	k2eAgNnSc6d1	socialistické
Československu	Československo	k1gNnSc6	Československo
legálně	legálně	k6eAd1	legálně
experimentovali	experimentovat	k5eAaImAgMnP	experimentovat
<g/>
.	.	kIx.	.
</s>
<s>
Pocity	pocit	k1gInPc1	pocit
a	a	k8xC	a
stavy	stav	k1gInPc1	stav
vědomí	vědomí	k1gNnSc2	vědomí
spjaté	spjatý	k2eAgNnSc1d1	spjaté
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
této	tento	k3xDgFnSc2	tento
drogy	droga	k1gFnSc2	droga
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
udělaly	udělat	k5eAaPmAgInP	udělat
zásadní	zásadní	k2eAgInSc4d1	zásadní
dojem	dojem	k1gInSc4	dojem
<g/>
.	.	kIx.	.
–	–	k?	–
Stanislav	Stanislav	k1gMnSc1	Stanislav
Grof	Grof	k1gMnSc1	Grof
Grof	Grof	k1gMnSc1	Grof
poté	poté	k6eAd1	poté
začal	začít	k5eAaPmAgMnS	začít
studovat	studovat	k5eAaImF	studovat
použití	použití	k1gNnSc4	použití
psychotropních	psychotropní	k2eAgFnPc2d1	psychotropní
látek	látka	k1gFnPc2	látka
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
kulturách	kultura	k1gFnPc6	kultura
a	a	k8xC	a
náboženstvích	náboženství	k1gNnPc6	náboženství
včetně	včetně	k7c2	včetně
mysticismu	mysticismus	k1gInSc2	mysticismus
a	a	k8xC	a
různých	různý	k2eAgFnPc2d1	různá
koncepcí	koncepce	k1gFnPc2	koncepce
spirituality	spiritualita	k1gFnSc2	spiritualita
jako	jako	k8xC	jako
protipól	protipól	k1gInSc1	protipól
ke	k	k7c3	k
koncepci	koncepce	k1gFnSc3	koncepce
západní	západní	k2eAgFnSc2d1	západní
medicíny	medicína	k1gFnSc2	medicína
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnPc2	jeho
slov	slovo	k1gNnPc2	slovo
tyto	tento	k3xDgInPc4	tento
zážitky	zážitek	k1gInPc4	zážitek
snaží	snažit	k5eAaImIp3nP	snažit
patologizovat	patologizovat	k5eAaImF	patologizovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
dostal	dostat	k5eAaPmAgMnS	dostat
stipendium	stipendium	k1gNnSc4	stipendium
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
–	–	k?	–
v	v	k7c4	v
Maryland	Maryland	k1gInSc4	Maryland
Psychiatric	Psychiatric	k1gMnSc1	Psychiatric
Research	Research	k1gMnSc1	Research
Centre	centr	k1gInSc5	centr
v	v	k7c4	v
Baltimore	Baltimore	k1gInSc4	Baltimore
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
několika	několik	k4yIc2	několik
výzkumných	výzkumný	k2eAgInPc2d1	výzkumný
projektů	projekt	k1gInPc2	projekt
(	(	kIx(	(
<g/>
např.	např.	kA	např.
se	s	k7c7	s
skupinami	skupina	k1gFnPc7	skupina
chronických	chronický	k2eAgMnPc2d1	chronický
alkoholiků	alkoholik	k1gMnPc2	alkoholik
<g/>
,	,	kIx,	,
vězňů-toxikomanů	vězňůoxikoman	k1gMnPc2	vězňů-toxikoman
<g/>
,	,	kIx,	,
pacientů	pacient	k1gMnPc2	pacient
umírajících	umírající	k1gFnPc2	umírající
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
srpnu	srpen	k1gInSc6	srpen
'	'	kIx"	'
<g/>
68	[number]	k4	68
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
v	v	k7c6	v
USA	USA	kA	USA
zůstat	zůstat	k5eAaPmF	zůstat
<g/>
,	,	kIx,	,
v	v	k7c6	v
marylandském	marylandský	k2eAgInSc6d1	marylandský
výzkumném	výzkumný	k2eAgInSc6d1	výzkumný
ústavu	ústav	k1gInSc6	ústav
povýšil	povýšit	k5eAaPmAgMnS	povýšit
na	na	k7c4	na
vedoucího	vedoucí	k1gMnSc4	vedoucí
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
dostal	dostat	k5eAaPmAgMnS	dostat
nabídku	nabídka	k1gFnSc4	nabídka
na	na	k7c4	na
napsání	napsání	k1gNnSc4	napsání
knihy	kniha	k1gFnSc2	kniha
o	o	k7c6	o
psychedelických	psychedelický	k2eAgFnPc6d1	psychedelická
látkách	látka	k1gFnPc6	látka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Saline	Salin	k1gInSc5	Salin
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
poznal	poznat	k5eAaPmAgMnS	poznat
svoji	svůj	k3xOyFgFnSc4	svůj
budoucí	budoucí	k2eAgFnSc4d1	budoucí
manželku	manželka	k1gFnSc4	manželka
Christinu	Christin	k2eAgFnSc4d1	Christina
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
spolu	spolu	k6eAd1	spolu
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
holotropní	holotropní	k2eAgNnSc4d1	holotropní
dýchání	dýchání	k1gNnSc4	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
začal	začít	k5eAaPmAgInS	začít
věnovat	věnovat	k5eAaPmF	věnovat
psaní	psaní	k1gNnSc4	psaní
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc1	jeho
knihy	kniha	k1gFnPc1	kniha
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
medicíny	medicína	k1gFnSc2	medicína
<g/>
,	,	kIx,	,
psychologie	psychologie	k1gFnSc2	psychologie
<g/>
,	,	kIx,	,
mystiky	mystika	k1gFnSc2	mystika
<g/>
,	,	kIx,	,
kulturologie	kulturologie	k1gFnSc1	kulturologie
a	a	k8xC	a
filosofie	filosofie	k1gFnSc1	filosofie
byly	být	k5eAaImAgFnP	být
přeloženy	přeložit	k5eAaPmNgFnP	přeložit
do	do	k7c2	do
několika	několik	k4yIc2	několik
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
vztahu	vztah	k1gInSc6	vztah
vědomí	vědomí	k1gNnSc2	vědomí
a	a	k8xC	a
hmoty	hmota	k1gFnSc2	hmota
vyjadřoval	vyjadřovat	k5eAaImAgInS	vyjadřovat
Grof	Grof	k1gInSc1	Grof
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
vědomí	vědomí	k1gNnSc1	vědomí
není	být	k5eNaImIp3nS	být
průvodním	průvodní	k2eAgInSc7d1	průvodní
jevem	jev	k1gInSc7	jev
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prvotní	prvotní	k2eAgFnSc7d1	prvotní
vlastností	vlastnost	k1gFnSc7	vlastnost
bytí	bytí	k1gNnSc2	bytí
<g/>
.	.	kIx.	.
–	–	k?	–
GROF	GROF	kA	GROF
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
Psychologie	psychologie	k1gFnSc1	psychologie
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Perla	perla	k1gFnSc1	perla
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
s.	s.	k?	s.
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
důkaz	důkaz	k1gInSc1	důkaz
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
označil	označit	k5eAaPmAgMnS	označit
některé	některý	k3yIgFnPc4	některý
informace	informace	k1gFnPc4	informace
získané	získaný	k2eAgFnPc4d1	získaná
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
holotropních	holotropní	k2eAgInPc2d1	holotropní
stavů	stav	k1gInPc2	stav
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
.	.	kIx.	.
–	–	k?	–
GROF	GROF	kA	GROF
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
Psychologie	psychologie	k1gFnSc1	psychologie
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Perla	perla	k1gFnSc1	perla
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
s.	s.	k?	s.
268	[number]	k4	268
<g/>
–	–	k?	–
<g/>
269	[number]	k4	269
<g/>
.	.	kIx.	.
</s>
<s>
Zdůrazňoval	zdůrazňovat	k5eAaImAgMnS	zdůrazňovat
ty	ten	k3xDgInPc4	ten
případy	případ	k1gInPc4	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
dle	dle	k7c2	dle
jeho	jeho	k3xOp3gNnPc2	jeho
tvrzení	tvrzení	k1gNnPc2	tvrzení
podařilo	podařit	k5eAaPmAgNnS	podařit
pozorovaným	pozorovaný	k2eAgMnPc3d1	pozorovaný
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
uvedeným	uvedený	k2eAgFnPc3d1	uvedená
do	do	k7c2	do
změněného	změněný	k2eAgInSc2d1	změněný
stavu	stav	k1gInSc2	stav
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
,	,	kIx,	,
zjistit	zjistit	k5eAaPmF	zjistit
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
těchto	tento	k3xDgInPc2	tento
stavů	stav	k1gInPc2	stav
informace	informace	k1gFnSc2	informace
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
dle	dle	k7c2	dle
Grofa	Grof	k1gMnSc2	Grof
nemohli	moct	k5eNaImAgMnP	moct
tito	tento	k3xDgMnPc1	tento
lidé	člověk	k1gMnPc1	člověk
získat	získat	k5eAaPmF	získat
jinou	jiný	k2eAgFnSc7d1	jiná
cestou	cesta	k1gFnSc7	cesta
a	a	k8xC	a
které	který	k3yQgNnSc1	který
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
zpětně	zpětně	k6eAd1	zpětně
možné	možný	k2eAgNnSc1d1	možné
objektivně	objektivně	k6eAd1	objektivně
ověřit	ověřit	k5eAaPmF	ověřit
<g/>
.	.	kIx.	.
–	–	k?	–
GROF	GROF	kA	GROF
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
Psychologie	psychologie	k1gFnSc1	psychologie
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Perla	perla	k1gFnSc1	perla
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
s.	s.	k?	s.
233	[number]	k4	233
<g/>
–	–	k?	–
<g/>
234	[number]	k4	234
<g/>
.	.	kIx.	.
</s>
<s>
Grof	Grof	k1gInSc1	Grof
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
knihách	kniha	k1gFnPc6	kniha
uvádí	uvádět	k5eAaImIp3nS	uvádět
i	i	k9	i
některé	některý	k3yIgInPc4	některý
konkrétní	konkrétní	k2eAgInPc4d1	konkrétní
příklady	příklad	k1gInPc4	příklad
<g/>
,	,	kIx,	,
takto	takto	k6eAd1	takto
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gInSc2	jeho
názoru	názor	k1gInSc2	názor
objektivně	objektivně	k6eAd1	objektivně
ověřených	ověřený	k2eAgFnPc2d1	ověřená
informací	informace	k1gFnPc2	informace
získaných	získaný	k2eAgFnPc2d1	získaná
cestou	cestou	k7c2	cestou
změněných	změněný	k2eAgInPc2d1	změněný
stavů	stav	k1gInPc2	stav
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
.	.	kIx.	.
–	–	k?	–
GROF	GROF	kA	GROF
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
Psychologie	psychologie	k1gFnSc1	psychologie
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Perla	perla	k1gFnSc1	perla
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
s.	s.	k?	s.
239	[number]	k4	239
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
informací	informace	k1gFnPc2	informace
získaných	získaný	k2eAgFnPc2d1	získaná
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
změněných	změněný	k2eAgInPc2d1	změněný
stavů	stav	k1gInPc2	stav
vědomí	vědomí	k1gNnSc2	vědomí
Grof	Grof	k1gMnSc1	Grof
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
rozšíření	rozšíření	k1gNnSc4	rozšíření
mapy	mapa	k1gFnSc2	mapa
lidské	lidský	k2eAgFnSc2d1	lidská
psýché	psýché	k1gFnSc2	psýché
o	o	k7c4	o
oblast	oblast	k1gFnSc4	oblast
perinatální	perinatální	k2eAgFnSc4d1	perinatální
a	a	k8xC	a
transpersonální	transpersonální	k2eAgFnSc4d1	transpersonální
<g/>
.	.	kIx.	.
–	–	k?	–
GROF	GROF	kA	GROF
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
Psychologie	psychologie	k1gFnSc1	psychologie
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Perla	perla	k1gFnSc1	perla
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
s.	s.	k?	s.
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
GROF	GROF	kA	GROF
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
;	;	kIx,	;
GROF	GROF	kA	GROF
<g/>
,	,	kIx,	,
Christina	Christina	k1gFnSc1	Christina
<g/>
.	.	kIx.	.
</s>
<s>
Holotropní	holotropní	k2eAgNnSc1d1	holotropní
dýchání	dýchání	k1gNnSc1	dýchání
:	:	kIx,	:
nová	nový	k2eAgFnSc1d1	nová
cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
sebeobjevování	sebeobjevování	k1gNnSc3	sebeobjevování
a	a	k8xC	a
léčení	léčení	k1gNnSc3	léčení
<g/>
.	.	kIx.	.
</s>
<s>
Opava	Opava	k1gFnSc1	Opava
<g/>
:	:	kIx,	:
Holos	Holos	k1gMnSc1	Holos
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
254	[number]	k4	254
<g/>
-	-	kIx~	-
<g/>
9215	[number]	k4	9215
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
GROF	GROF	kA	GROF
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgNnSc1d1	lidské
vědomí	vědomí	k1gNnSc1	vědomí
a	a	k8xC	a
tajemství	tajemství	k1gNnSc1	tajemství
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
257	[number]	k4	257
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
177	[number]	k4	177
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
GROF	GROF	kA	GROF
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
perspektivy	perspektiva	k1gFnPc1	perspektiva
v	v	k7c6	v
psychiatrii	psychiatrie	k1gFnSc6	psychiatrie
a	a	k8xC	a
psychologii	psychologie	k1gFnSc6	psychologie
:	:	kIx,	:
pozorování	pozorování	k1gNnSc1	pozorování
z	z	k7c2	z
moderního	moderní	k2eAgInSc2d1	moderní
výzkumu	výzkum	k1gInSc2	výzkum
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Moraviapress	Moraviapress	k1gInSc1	Moraviapress
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86181	[number]	k4	86181
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
GROF	GROF	kA	GROF
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
Psychologie	psychologie	k1gFnSc1	psychologie
budoucnosti	budoucnost	k1gFnSc2	budoucnost
:	:	kIx,	:
poznatky	poznatek	k1gInPc1	poznatek
a	a	k8xC	a
poučení	poučení	k1gNnSc1	poučení
z	z	k7c2	z
moderního	moderní	k2eAgInSc2d1	moderní
výzkumu	výzkum	k1gInSc2	výzkum
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Perla	perla	k1gFnSc1	perla
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902156	[number]	k4	902156
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
937	[number]	k4	937
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
v	v	k7c6	v
Argu	Argos	k1gInSc6	Argos
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
257	[number]	k4	257
<g/>
-	-	kIx~	-
<g/>
1310	[number]	k4	1310
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
GROF	GROF	kA	GROF
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
;	;	kIx,	;
BENNETT	BENNETT	kA	BENNETT
<g/>
,	,	kIx,	,
Hal	hala	k1gFnPc2	hala
Zina	Zina	k1gFnSc1	Zina
<g/>
.	.	kIx.	.
</s>
<s>
Holotropní	holotropní	k2eAgNnSc1d1	holotropní
vnímání	vnímání	k1gNnSc1	vnímání
:	:	kIx,	:
tři	tři	k4xCgFnPc1	tři
úrovně	úroveň	k1gFnPc1	úroveň
lidského	lidský	k2eAgNnSc2d1	lidské
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
,	,	kIx,	,
formující	formující	k2eAgMnSc1d1	formující
naše	náš	k3xOp1gInPc4	náš
životy	život	k1gInPc4	život
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Perla	perla	k1gFnSc1	perla
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902156	[number]	k4	902156
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
GROF	GROF	kA	GROF
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
;	;	kIx,	;
GROF	GROF	kA	GROF
<g/>
,	,	kIx,	,
Christina	Christina	k1gFnSc1	Christina
(	(	kIx(	(
<g/>
sest	sest	k1gInSc1	sest
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krize	krize	k1gFnSc1	krize
duchovního	duchovní	k2eAgInSc2d1	duchovní
vývoje	vývoj	k1gInSc2	vývoj
:	:	kIx,	:
když	když	k8xS	když
se	se	k3xPyFc4	se
osobní	osobní	k2eAgFnSc1d1	osobní
transformace	transformace	k1gFnSc1	transformace
promění	proměnit	k5eAaPmIp3nS	proměnit
v	v	k7c6	v
krizi	krize	k1gFnSc6	krize
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Chvojkovo	Chvojkův	k2eAgNnSc4d1	Chvojkovo
nakl	naknout	k5eAaPmAgMnS	naknout
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86183	[number]	k4	86183
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Opava	Opava	k1gFnSc1	Opava
<g/>
:	:	kIx,	:
Holos	Holos	k1gMnSc1	Holos
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
905024	[number]	k4	905024
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
GROF	GROF	kA	GROF
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
;	;	kIx,	;
GROF	GROF	kA	GROF
<g/>
,	,	kIx,	,
Christina	Christina	k1gFnSc1	Christina
<g/>
.	.	kIx.	.
</s>
<s>
Nesnadné	snadný	k2eNgNnSc1d1	nesnadné
hledání	hledání	k1gNnSc1	hledání
vlastního	vlastní	k2eAgInSc2d1	vlastní
já	já	k3xPp1nSc1	já
:	:	kIx,	:
růst	růst	k5eAaImF	růst
osobnosti	osobnost	k1gFnPc4	osobnost
pomocí	pomocí	k7c2	pomocí
transformační	transformační	k2eAgFnSc2d1	transformační
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Chvojkovo	Chvojkův	k2eAgNnSc4d1	Chvojkovo
nakl	naknout	k5eAaPmAgMnS	naknout
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86183	[number]	k4	86183
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
GROF	GROF	kA	GROF
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
Kosmická	kosmický	k2eAgFnSc1d1	kosmická
hra	hra	k1gFnSc1	hra
:	:	kIx,	:
zkoumání	zkoumání	k1gNnSc1	zkoumání
hranic	hranice	k1gFnPc2	hranice
lidského	lidský	k2eAgNnSc2d1	lidské
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Perla	perla	k1gFnSc1	perla
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902156	[number]	k4	902156
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Práh	práh	k1gInSc1	práh
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7252	[number]	k4	7252
<g/>
-	-	kIx~	-
<g/>
418	[number]	k4	418
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
GROF	GROF	kA	GROF
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
mozku	mozek	k1gInSc2	mozek
:	:	kIx,	:
narození	narození	k1gNnSc1	narození
<g/>
,	,	kIx,	,
smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
transcendence	transcendence	k1gFnSc1	transcendence
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Gemma	Gemma	k1gFnSc1	Gemma
89	[number]	k4	89
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
[	[	kIx(	[
<g/>
dotisk	dotisk	k1gInSc1	dotisk
1993	[number]	k4	1993
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-85	[number]	k4	80-85
206	[number]	k4	206
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Perla	perla	k1gFnSc1	perla
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902156	[number]	k4	902156
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
GROF	GROF	kA	GROF
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
Dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
sebeobjevování	sebeobjevování	k1gNnSc2	sebeobjevování
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Gemma	Gemma	k1gFnSc1	Gemma
89	[number]	k4	89
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
[	[	kIx(	[
<g/>
dotisk	dotisk	k1gInSc1	dotisk
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85206	[number]	k4	85206
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
dopl	dopl	k1gMnSc1	dopl
<g/>
.	.	kIx.	.
a	a	k8xC	a
aktualiz	aktualiz	k1gInSc1	aktualiz
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Perla	perla	k1gFnSc1	perla
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902156	[number]	k4	902156
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
GROF	GROF	kA	GROF
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
;	;	kIx,	;
BENNETT	BENNETT	kA	BENNETT
<g/>
,	,	kIx,	,
Hal	hala	k1gFnPc2	hala
Zina	Zina	k1gFnSc1	Zina
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Holotropic	Holotropic	k1gMnSc1	Holotropic
Mind	Mind	k1gMnSc1	Mind
:	:	kIx,	:
the	the	k?	the
three	three	k1gInSc1	three
levels	levels	k1gInSc1	levels
of	of	k?	of
human	human	k1gInSc1	human
consciousness	consciousness	k1gInSc1	consciousness
and	and	k?	and
how	how	k?	how
they	thea	k1gMnSc2	thea
shape	shapat	k5eAaPmIp3nS	shapat
our	our	k?	our
lives	lives	k1gInSc1	lives
<g/>
.	.	kIx.	.
</s>
<s>
San	San	k?	San
Francisco	Francisco	k1gMnSc1	Francisco
<g/>
:	:	kIx,	:
Harper	Harper	k1gMnSc1	Harper
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
62503677	[number]	k4	62503677
<g/>
.	.	kIx.	.
</s>
<s>
GROF	GROF	kA	GROF
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
LSD	LSD	kA	LSD
Psychotherapy	Psychotherap	k1gInPc1	Psychotherap
<g/>
.	.	kIx.	.
</s>
<s>
Pomona	Pomona	k1gFnSc1	Pomona
(	(	kIx(	(
<g/>
California	Californium	k1gNnPc4	Californium
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
897930088	[number]	k4	897930088
<g/>
.	.	kIx.	.
</s>
<s>
LÁSZLÓ	LÁSZLÓ	kA	LÁSZLÓ
<g/>
,	,	kIx,	,
Ervin	Ervin	k1gMnSc1	Ervin
<g/>
;	;	kIx,	;
GROF	GROF	kA	GROF
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
;	;	kIx,	;
RUSSELL	RUSSELL	kA	RUSSELL
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
<g/>
.	.	kIx.	.
</s>
<s>
Revoluce	revoluce	k1gFnSc1	revoluce
vědomí	vědomí	k1gNnSc2	vědomí
:	:	kIx,	:
transatlantický	transatlantický	k2eAgInSc1d1	transatlantický
dialog	dialog	k1gInSc1	dialog
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Carpe	Carp	k1gInSc5	Carp
Momentum	Momentum	k1gNnSc1	Momentum
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
905334	[number]	k4	905334
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2007	[number]	k4	2007
obdržel	obdržet	k5eAaPmAgMnS	obdržet
za	za	k7c4	za
rozvinutí	rozvinutí	k1gNnSc4	rozvinutí
metody	metoda	k1gFnSc2	metoda
holotropního	holotropní	k2eAgNnSc2d1	holotropní
dýchání	dýchání	k1gNnSc2	dýchání
Cenu	cena	k1gFnSc4	cena
VIZE	vize	k1gFnSc2	vize
97	[number]	k4	97
manželů	manžel	k1gMnPc2	manžel
Dagmar	Dagmar	k1gFnSc2	Dagmar
a	a	k8xC	a
Václava	Václav	k1gMnSc2	Václav
Havlových	Havlových	k2eAgMnSc2d1	Havlových
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
mu	on	k3xPp3gNnSc3	on
Český	český	k2eAgInSc4d1	český
klub	klub	k1gInSc4	klub
skeptiků	skeptik	k1gMnPc2	skeptik
Sisyfos	Sisyfos	k1gMnSc1	Sisyfos
udělil	udělit	k5eAaPmAgMnS	udělit
bronzový	bronzový	k2eAgInSc4d1	bronzový
Bludný	bludný	k2eAgInSc4d1	bludný
balvan	balvan	k1gInSc4	balvan
za	za	k7c4	za
"	"	kIx"	"
<g/>
přetavení	přetavení	k1gNnSc4	přetavení
šamanských	šamanský	k2eAgFnPc2d1	šamanská
praktik	praktika	k1gFnPc2	praktika
hyperventilace	hyperventilace	k1gFnSc2	hyperventilace
do	do	k7c2	do
tak	tak	k6eAd1	tak
zvaného	zvaný	k2eAgNnSc2d1	zvané
holotropního	holotropní	k2eAgNnSc2d1	holotropní
dýchání	dýchání	k1gNnSc2	dýchání
<g/>
,	,	kIx,	,
skrze	skrze	k?	skrze
něž	jenž	k3xRgInPc4	jenž
lze	lze	k6eAd1	lze
způsobit	způsobit	k5eAaPmF	způsobit
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
chemickou	chemický	k2eAgFnSc4d1	chemická
bouři	bouře	k1gFnSc4	bouře
a	a	k8xC	a
navodit	navodit	k5eAaBmF	navodit
tak	tak	k6eAd1	tak
přiotrávenému	přiotrávený	k2eAgInSc3d1	přiotrávený
mimořádné	mimořádný	k2eAgInPc4d1	mimořádný
pocity	pocit	k1gInPc4	pocit
a	a	k8xC	a
zážitky	zážitek	k1gInPc4	zážitek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
udělen	udělen	k2eAgInSc4d1	udělen
diamantový	diamantový	k2eAgInSc4d1	diamantový
bludný	bludný	k2eAgInSc4d1	bludný
balvan	balvan	k1gInSc4	balvan
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Navozování	navozování	k1gNnSc6	navozování
a	a	k8xC	a
zkoumání	zkoumání	k1gNnSc6	zkoumání
stavů	stav	k1gInPc2	stav
holotropního	holotropní	k2eAgNnSc2d1	holotropní
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
láme	lámat	k5eAaImIp3nS	lámat
pouta	pouto	k1gNnPc4	pouto
časoprostoru	časoprostor	k1gInSc2	časoprostor
a	a	k8xC	a
dokáže	dokázat	k5eAaPmIp3nS	dokázat
tudíž	tudíž	k8xC	tudíž
kdykoli	kdykoli	k6eAd1	kdykoli
a	a	k8xC	a
kamkoli	kamkoli	k6eAd1	kamkoli
vlézt	vlézt	k5eAaPmF	vlézt
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Stanislav	Stanislav	k1gMnSc1	Stanislav
Grof	Grof	k1gMnSc1	Grof
se	se	k3xPyFc4	se
tak	tak	k9	tak
stal	stát	k5eAaPmAgMnS	stát
jedinou	jediný	k2eAgFnSc7d1	jediná
osobou	osoba	k1gFnSc7	osoba
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
bludný	bludný	k2eAgInSc4d1	bludný
balvan	balvan	k1gInSc4	balvan
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jedenkrát	jedenkrát	k6eAd1	jedenkrát
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byl	být	k5eAaImAgMnS	být
Grof	Grof	k1gMnSc1	Grof
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
reálné	reálný	k2eAgInPc4d1	reálný
jevy	jev	k1gInPc4	jev
považuje	považovat	k5eAaImIp3nS	považovat
telepatii	telepatie	k1gFnSc4	telepatie
<g/>
,	,	kIx,	,
zázračné	zázračný	k2eAgNnSc4d1	zázračné
extrakční	extrakční	k2eAgFnPc4d1	extrakční
operace	operace	k1gFnPc4	operace
filipínských	filipínský	k2eAgMnPc2d1	filipínský
léčitelů	léčitel	k1gMnPc2	léčitel
či	či	k8xC	či
ohýbání	ohýbání	k1gNnSc3	ohýbání
lžic	lžíce	k1gFnPc2	lžíce
mentální	mentální	k2eAgFnSc7d1	mentální
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
