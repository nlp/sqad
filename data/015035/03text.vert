<s>
Rezoluce	rezoluce	k1gFnSc1
Rady	rada	k1gFnSc2
bezpečnosti	bezpečnost	k1gFnSc2
OSN	OSN	kA
č.	č.	k?
746	#num#	k4
</s>
<s>
Rezoluce	rezoluce	k1gFnSc1
Rady	rada	k1gFnSc2
bezpečnosti	bezpečnost	k1gFnSc2
OSN	OSN	kA
č.	č.	k?
746	#num#	k4
</s>
<s>
Datum	datum	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1992	#num#	k4
</s>
<s>
Zasedání	zasedání	k1gNnSc1
č.	č.	k?
<g/>
:	:	kIx,
</s>
<s>
3060	#num#	k4
</s>
<s>
Kód	kód	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
S	s	k7c7
<g/>
/	/	kIx~
<g/>
RES	RES	kA
<g/>
/	/	kIx~
<g/>
746	#num#	k4
(	(	kIx(
<g/>
dokument	dokument	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hlasy	hlas	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
pro	pro	k7c4
<g/>
:	:	kIx,
15	#num#	k4
zdrželo	zdržet	k5eAaPmAgNnS
se	s	k7c7
<g/>
:	:	kIx,
0	#num#	k4
proti	proti	k7c3
<g/>
:	:	kIx,
0	#num#	k4
</s>
<s>
Předmět	předmět	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
situace	situace	k1gFnSc1
v	v	k7c6
Somálsku	Somálsko	k1gNnSc6
</s>
<s>
Výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
schváleno	schválit	k5eAaPmNgNnS
</s>
<s>
Složení	složení	k1gNnSc1
Rady	rada	k1gFnSc2
bezpečnosti	bezpečnost	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
stálí	stálý	k2eAgMnPc1d1
členové	člen	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
CHN	CHN	kA
FRA	FRA	kA
RUS	Rus	k1gMnSc1
GBR	GBR	kA
USA	USA	kA
</s>
<s>
nestálí	stálý	k2eNgMnPc1d1
členové	člen	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
AUT	aut	k1gInSc1
BEL	bel	k1gInSc1
CPV	CPV	kA
ECU	ECU	kA
HUN	Hun	k1gMnSc1
</s>
<s>
IND	Ind	k1gMnSc1
JPN	JPN	kA
MAR	MAR	kA
VEN	ven	k6eAd1
ZWE	ZWE	kA
</s>
<s>
Somálské	somálský	k2eAgFnPc4d1
děti	dítě	k1gFnPc4
čekající	čekající	k2eAgFnPc4d1
na	na	k7c4
pomoc	pomoc	k1gFnSc4
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rezoluce	rezoluce	k1gFnSc1
Rady	rada	k1gFnSc2
bezpečnosti	bezpečnost	k1gFnSc2
OSN	OSN	kA
č.	č.	k?
746	#num#	k4
(	(	kIx(
<g/>
S	s	k7c7
<g/>
/	/	kIx~
<g/>
RES	RES	kA
<g/>
/	/	kIx~
<g/>
746	#num#	k4
<g/>
)	)	kIx)
přijatá	přijatý	k2eAgFnSc1d1
jednomyslně	jednomyslně	k6eAd1
17	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1992	#num#	k4
<g/>
,	,	kIx,
potvrdila	potvrdit	k5eAaPmAgFnS
platnost	platnost	k1gFnSc4
dříve	dříve	k6eAd2
schválené	schválený	k2eAgFnSc2d1
rezoluce	rezoluce	k1gFnSc2
č.	č.	k?
733	#num#	k4
a	a	k8xC
přivítala	přivítat	k5eAaPmAgFnS
dohodu	dohoda	k1gFnSc4
o	o	k7c4
zastavení	zastavení	k1gNnSc4
palby	palba	k1gFnSc2
v	v	k7c6
Mogadišu	Mogadišo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rada	rada	k1gFnSc1
vzala	vzít	k5eAaPmAgFnS
na	na	k7c6
vědomí	vědomí	k1gNnSc6
zprávu	zpráva	k1gFnSc4
generálního	generální	k2eAgMnSc2d1
tajemníka	tajemník	k1gMnSc2
OSN	OSN	kA
Butrus	Butrus	k1gInSc1
Butrus-Ghálího	Butrus-Ghálí	k2eAgInSc2d1
a	a	k8xC
vyzvala	vyzvat	k5eAaPmAgFnS
k	k	k7c3
pokračování	pokračování	k1gNnSc3
dodávek	dodávka	k1gFnPc2
humanitární	humanitární	k2eAgFnSc2d1
pomoci	pomoc	k1gFnSc2
do	do	k7c2
Somálska	Somálsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podpořila	podpořit	k5eAaPmAgFnS
také	také	k9
návrh	návrh	k1gInSc4
generálního	generální	k2eAgMnSc2d1
tajemníka	tajemník	k1gMnSc2
na	na	k7c6
vyslání	vyslání	k1gNnSc6
týmu	tým	k1gInSc2
technické	technický	k2eAgFnSc2d1
podpory	podpora	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rada	rada	k1gFnSc1
vyzvala	vyzvat	k5eAaPmAgFnS
všechny	všechen	k3xTgFnPc4
strany	strana	k1gFnPc4
somálského	somálský	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
k	k	k7c3
dodržování	dodržování	k1gNnSc3
příměří	příměří	k1gNnSc2
z	z	k7c2
3	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1992	#num#	k4
a	a	k8xC
k	k	k7c3
jejich	jejich	k3xOp3gFnSc3
další	další	k2eAgFnSc3d1
spolupráci	spolupráce	k1gFnSc3
s	s	k7c7
generálním	generální	k2eAgMnSc7d1
tajemníkem	tajemník	k1gMnSc7
a	a	k8xC
s	s	k7c7
pracovníky	pracovník	k1gMnPc7
OSN	OSN	kA
a	a	k8xC
dalších	další	k2eAgFnPc2d1
mezinárodních	mezinárodní	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
na	na	k7c6
dopravě	doprava	k1gFnSc6
humanitárních	humanitární	k2eAgFnPc2d1
dodávek	dodávka	k1gFnPc2
k	k	k7c3
somálskému	somálský	k2eAgNnSc3d1
obyvatelstvu	obyvatelstvo	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rada	rada	k1gFnSc1
také	také	k9
podpořila	podpořit	k5eAaPmAgFnS
vyslání	vyslání	k1gNnSc4
týmu	tým	k1gInSc2
technické	technický	k2eAgFnSc2d1
podpory	podpora	k1gFnSc2
(	(	kIx(
<g/>
UNOSOM	UNOSOM	kA
I	I	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
by	by	kYmCp3nS
na	na	k7c6
místě	místo	k1gNnSc6
zajistil	zajistit	k5eAaPmAgMnS
logistickou	logistický	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
pro	pro	k7c4
rozvoz	rozvoz	k1gInSc4
pomoci	pomoc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyzvala	vyzvat	k5eAaPmAgFnS
také	také	k9
všechny	všechen	k3xTgFnPc4
strany	strana	k1gFnPc4
somálského	somálský	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
ke	k	k7c3
garanci	garance	k1gFnSc3
bezpečnosti	bezpečnost	k1gFnSc2
pro	pro	k7c4
členy	člen	k1gMnPc4
týmu	tým	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Rada	rada	k1gFnSc1
také	také	k9
podpořila	podpořit	k5eAaPmAgFnS
další	další	k2eAgFnSc4d1
spolupráci	spolupráce	k1gFnSc4
mezi	mezi	k7c7
Organizací	organizace	k1gFnSc7
africké	africký	k2eAgFnSc2d1
jednoty	jednota	k1gFnSc2
<g/>
,	,	kIx,
Arabskou	arabský	k2eAgFnSc7d1
ligou	liga	k1gFnSc7
a	a	k8xC
generálním	generální	k2eAgMnSc7d1
tajemníkem	tajemník	k1gMnSc7
OSN	OSN	kA
v	v	k7c6
naději	naděje	k1gFnSc6
na	na	k7c4
svolání	svolání	k1gNnSc4
celonárodní	celonárodní	k2eAgFnSc2d1
konference	konference	k1gFnSc2
o	o	k7c6
usmíření	usmíření	k1gNnSc6
v	v	k7c6
Somálsku	Somálsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
v	v	k7c6
Somálsku	Somálsko	k1gNnSc6
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
United	United	k1gMnSc1
Nations	Nationsa	k1gFnPc2
Security	Securita	k1gFnSc2
Council	Council	k1gInSc1
Resolution	Resolution	k1gInSc1
746	#num#	k4
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
United	United	k1gInSc1
Nations	Nations	k1gInSc1
Operations	Operations	k1gInSc1
in	in	k?
Somalia	Somalia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gInSc1
Nations	Nations	k1gInSc4
<g/>
.	.	kIx.
</s>
