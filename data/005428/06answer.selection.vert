<s>
Měrná	měrný	k2eAgFnSc1d1	měrná
tepelná	tepelný	k2eAgFnSc1d1	tepelná
kapacita	kapacita	k1gFnSc1	kapacita
(	(	kIx(	(
<g/>
specifické	specifický	k2eAgNnSc1d1	specifické
teplo	teplo	k1gNnSc1	teplo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
vody	voda	k1gFnSc2	voda
zhruba	zhruba	k6eAd1	zhruba
tři	tři	k4xCgNnPc1	tři
až	až	k8xS	až
desetkrát	desetkrát	k6eAd1	desetkrát
(	(	kIx(	(
<g/>
desetkrát	desetkrát	k6eAd1	desetkrát
u	u	k7c2	u
železa	železo	k1gNnSc2	železo
<g/>
)	)	kIx)	)
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
ostatních	ostatní	k2eAgFnPc2d1	ostatní
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
horniny	hornina	k1gFnPc1	hornina
<g/>
,	,	kIx,	,
železo	železo	k1gNnSc1	železo
<g/>
,	,	kIx,	,
hliník	hliník	k1gInSc1	hliník
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
</s>
