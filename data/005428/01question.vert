<s>
Kolikrát	kolikrát	k6eAd1	kolikrát
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
měrná	měrný	k2eAgFnSc1d1	měrná
tepelná	tepelný	k2eAgFnSc1d1	tepelná
kapacita	kapacita	k1gFnSc1	kapacita
u	u	k7c2	u
vody	voda	k1gFnSc2	voda
než	než	k8xS	než
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
ostatních	ostatní	k2eAgFnPc2d1	ostatní
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
horniny	hornina	k1gFnPc1	hornina
<g/>
,	,	kIx,	,
železo	železo	k1gNnSc1	železo
<g/>
,	,	kIx,	,
hliník	hliník	k1gInSc1	hliník
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
?	?	kIx.	?
</s>
