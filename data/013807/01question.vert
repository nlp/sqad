<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
stroj	stroj	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
k	k	k7c3
vytváření	vytváření	k1gNnSc3
tlaku	tlak	k1gInSc2
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
nestlačitelnosti	nestlačitelnost	k1gFnPc4
kapaliny	kapalina	k1gFnSc2
<g/>
?	?	kIx.
</s>