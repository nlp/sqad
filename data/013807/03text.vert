<s>
Hydraulický	hydraulický	k2eAgInSc1d1
lis	lis	k1gInSc1
</s>
<s>
Hydraulický	hydraulický	k2eAgInSc1d1
lis	lis	k1gInSc1
je	být	k5eAaImIp3nS
stroj	stroj	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
k	k	k7c3
vytváření	vytváření	k1gNnSc3
tlaku	tlak	k1gInSc2
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
vlastností	vlastnost	k1gFnSc7
kapaliny	kapalina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Princip	princip	k1gInSc1
</s>
<s>
Princip	princip	k1gInSc1
hydraulického	hydraulický	k2eAgInSc2d1
lisu	lis	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
dvou	dva	k4xCgFnPc6
válcových	válcový	k2eAgFnPc6d1
nádobách	nádoba	k1gFnPc6
je	být	k5eAaImIp3nS
uzavřena	uzavřen	k2eAgFnSc1d1
kapalina	kapalina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Písty	píst	k1gInPc7
jsou	být	k5eAaImIp3nP
pohyblivé	pohyblivý	k2eAgInPc1d1
a	a	k8xC
mají	mít	k5eAaImIp3nP
plochy	plocha	k1gFnPc1
o	o	k7c6
obsahu	obsah	k1gInSc6
</s>
<s>
S	s	k7c7
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
S_	S_	k1gMnSc6
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
a	a	k8xC
</s>
<s>
S	s	k7c7
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
S_	S_	k1gMnSc6
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
Na	na	k7c4
píst	píst	k1gInSc4
o	o	k7c6
ploše	plocha	k1gFnSc6
</s>
<s>
S	s	k7c7
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
S_	S_	k1gMnSc6
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
působíme	působit	k5eAaImIp1nP
silou	síla	k1gFnSc7
</s>
<s>
F	F	kA
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
F_	F_	k1gMnSc6
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
kolmá	kolmý	k2eAgFnSc1d1
k	k	k7c3
pístu	píst	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
síla	síla	k1gFnSc1
vyvolá	vyvolat	k5eAaPmIp3nS
v	v	k7c6
kapalině	kapalina	k1gFnSc6
tlak	tlak	k1gInSc1
</s>
<s>
p	p	k?
</s>
<s>
=	=	kIx~
</s>
<s>
F	F	kA
</s>
<s>
1	#num#	k4
</s>
<s>
S	s	k7c7
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
p	p	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
F_	F_	k1gFnSc1
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
S_	S_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}}}	}}}}	k?
</s>
<s>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
podle	podle	k7c2
Pascalova	Pascalův	k2eAgInSc2d1
zákona	zákon	k1gInSc2
ve	v	k7c6
všech	všecek	k3xTgNnPc6
místech	místo	k1gNnPc6
kapaliny	kapalina	k1gFnSc2
stejný	stejný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
píst	píst	k1gInSc4
s	s	k7c7
obsahem	obsah	k1gInSc7
</s>
<s>
S	s	k7c7
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
S_	S_	k1gMnSc6
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
tak	tak	k6eAd1
působí	působit	k5eAaImIp3nS
tlaková	tlakový	k2eAgFnSc1d1
síla	síla	k1gFnSc1
</s>
<s>
F	F	kA
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
p	p	k?
</s>
<s>
S	s	k7c7
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
F	F	kA
</s>
<s>
1	#num#	k4
</s>
<s>
S	s	k7c7
</s>
<s>
2	#num#	k4
</s>
<s>
S	s	k7c7
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
F_	F_	k1gMnSc6
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
pS_	pS_	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
F_	F_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
S_	S_	k1gFnSc1
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
S_	S_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}}}	}}}}	k?
</s>
<s>
Síly	síla	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
na	na	k7c4
písty	píst	k1gInPc4
působí	působit	k5eAaImIp3nP
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
poměru	poměr	k1gInSc6
jako	jako	k9
obsahy	obsah	k1gInPc1
průřezů	průřez	k1gInPc2
obou	dva	k4xCgInPc2
pístů	píst	k1gInPc2
<g/>
,	,	kIx,
tedy	tedy	k9
</s>
<s>
F	F	kA
</s>
<s>
2	#num#	k4
</s>
<s>
F	F	kA
</s>
<s>
1	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
S	s	k7c7
</s>
<s>
2	#num#	k4
</s>
<s>
S	s	k7c7
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
F_	F_	k1gFnSc1
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
F_	F_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}}	}}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
S_	S_	k1gFnSc1
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
S_	S_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}}}	}}}}	k?
</s>
<s>
</s>
<s>
Použití	použití	k1gNnSc1
</s>
<s>
Hydraulické	hydraulický	k2eAgInPc1d1
lisy	lis	k1gInPc1
patří	patřit	k5eAaImIp3nP
mezi	mezi	k7c7
tzv.	tzv.	kA
silové	silový	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Síla	síla	k1gFnSc1
je	být	k5eAaImIp3nS
vyvozena	vyvodit	k5eAaBmNgFnS,k5eAaPmNgFnS
pomocí	pomocí	k7c2
tlakové	tlakový	k2eAgFnSc2d1
kapaliny	kapalina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc7
výhodou	výhoda	k1gFnSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
mohou	moct	k5eAaImIp3nP
maximální	maximální	k2eAgFnPc1d1
tvářecí	tvářecí	k2eAgFnPc1d1
síly	síla	k1gFnPc1
odebrat	odebrat	k5eAaPmF
v	v	k7c6
kterémkoli	kterýkoli	k3yIgNnSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Díky	díky	k7c3
pomalejším	pomalý	k2eAgFnPc3d2
rychlostem	rychlost	k1gFnPc3
pohybu	pohyb	k1gInSc2
beranu	beran	k1gInSc2
nejsou	být	k5eNaImIp3nP
vhodné	vhodný	k2eAgInPc1d1
pro	pro	k7c4
kování	kování	k1gNnSc4
vysokých	vysoký	k2eAgInPc2d1
výkovků	výkovek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
nacházejí	nacházet	k5eAaImIp3nP
čím	čí	k3xOyRgNnSc7,k3xOyQgNnSc7
dál	daleko	k6eAd2
větší	veliký	k2eAgNnSc4d2
uplatnění	uplatnění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dají	dát	k5eAaPmIp3nP
se	se	k3xPyFc4
snadno	snadno	k6eAd1
automatizovat	automatizovat	k5eAaBmF
a	a	k8xC
ovládat	ovládat	k5eAaImF
na	na	k7c4
větší	veliký	k2eAgFnPc4d2
vzdálenosti	vzdálenost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používají	používat	k5eAaImIp3nP
se	se	k3xPyFc4
na	na	k7c4
různé	různý	k2eAgFnPc4d1
tvářecí	tvářecí	k2eAgFnPc4d1
operace	operace	k1gFnPc4
(	(	kIx(
ražení	ražení	k1gNnSc1
<g/>
,	,	kIx,
kování	kování	k1gNnSc1
<g/>
,	,	kIx,
tažení	tažení	k1gNnSc1
<g/>
,	,	kIx,
<g/>
...	...	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Lis	lis	k1gInSc1
</s>
<s>
Tlaková	tlakový	k2eAgFnSc1d1
síla	síla	k1gFnSc1
</s>
<s>
Tlak	tlak	k1gInSc1
</s>
<s>
Spojené	spojený	k2eAgFnPc1d1
nádoby	nádoba	k1gFnPc1
</s>
<s>
Hydraulické	hydraulický	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
</s>
<s>
Hydraulický	hydraulický	k2eAgInSc1d1
lis	lis	k1gInSc1
na	na	k7c4
svislé	svislý	k2eAgInPc4d1
čepy	čep	k1gInPc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fyzika	fyzika	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
PSH	PSH	kA
<g/>
:	:	kIx,
5279	#num#	k4
</s>
