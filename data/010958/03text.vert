<p>
<s>
Tanzanie	Tanzanie	k1gFnSc1	Tanzanie
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
střední	střední	k2eAgFnSc2d1	střední
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
sloučením	sloučení	k1gNnSc7	sloučení
dvou	dva	k4xCgInPc2	dva
původně	původně	k6eAd1	původně
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
Tanganiky	Tanganika	k1gFnSc2	Tanganika
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
u	u	k7c2	u
stejnojmenného	stejnojmenný	k2eAgNnSc2d1	stejnojmenné
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
a	a	k8xC	a
Zanzibaru	Zanzibar	k1gInSc2	Zanzibar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
sousedí	sousedit	k5eAaImIp3nS	sousedit
Keňa	Keňa	k1gFnSc1	Keňa
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Uganda	Uganda	k1gFnSc1	Uganda
<g/>
,	,	kIx,	,
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
Rwanda	Rwanda	k1gFnSc1	Rwanda
a	a	k8xC	a
Burundi	Burundi	k1gNnSc1	Burundi
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
Kongo	Kongo	k1gNnSc1	Kongo
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
Zambie	Zambie	k1gFnSc2	Zambie
a	a	k8xC	a
Malawi	Malawi	k1gNnSc2	Malawi
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Mosambik	Mosambik	k1gInSc1	Mosambik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1885	[number]	k4	1885
<g/>
–	–	k?	–
<g/>
1919	[number]	k4	1919
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
německou	německý	k2eAgFnSc7d1	německá
kolonií	kolonie	k1gFnSc7	kolonie
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Německá	německý	k2eAgFnSc1d1	německá
východní	východní	k2eAgFnSc1d1	východní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Dřívější	dřívější	k2eAgInSc1d1	dřívější
oficiální	oficiální	k2eAgInSc1d1	oficiální
anglický	anglický	k2eAgInSc1d1	anglický
název	název	k1gInSc1	název
země	zem	k1gFnSc2	zem
byl	být	k5eAaImAgInS	být
United	United	k1gInSc1	United
Republic	Republice	k1gFnPc2	Republice
of	of	k?	of
Tanganyika	Tanganyik	k1gMnSc2	Tanganyik
and	and	k?	and
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
změnil	změnit	k5eAaPmAgInS	změnit
na	na	k7c4	na
United	United	k1gInSc4	United
Republic	Republice	k1gFnPc2	Republice
of	of	k?	of
Tanzania	Tanzanium	k1gNnSc2	Tanzanium
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Tanzanie	Tanzanie	k1gFnSc2	Tanzanie
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
názvů	název	k1gInPc2	název
obou	dva	k4xCgInPc2	dva
zakladatelských	zakladatelský	k2eAgInPc2d1	zakladatelský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
sloučily	sloučit	k5eAaPmAgInP	sloučit
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
po	po	k7c6	po
získání	získání	k1gNnSc6	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
na	na	k7c6	na
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Originální	originální	k2eAgInSc1d1	originální
název	název	k1gInSc1	název
ve	v	k7c6	v
svahilštině	svahilština	k1gFnSc6	svahilština
je	být	k5eAaImIp3nS	být
Jamhuri	Jamhuri	k1gNnSc1	Jamhuri
ya	ya	k?	ya
Muungano	Muungana	k1gFnSc5	Muungana
wa	wa	k?	wa
Tanzania	Tanzanium	k1gNnSc2	Tanzanium
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
Tanzanská	tanzanský	k2eAgFnSc1d1	tanzanská
sjednocená	sjednocený	k2eAgFnSc1d1	sjednocená
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Tanzanie	Tanzanie	k1gFnSc1	Tanzanie
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejdéle	dlouho	k6eAd3	dlouho
osídleným	osídlený	k2eAgFnPc3d1	osídlená
zemím	zem	k1gFnPc3	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
proslulé	proslulý	k2eAgFnPc1d1	proslulá
paleoantropologické	paleoantropologický	k2eAgFnPc1d1	paleoantropologický
a	a	k8xC	a
archeologické	archeologický	k2eAgFnPc1d1	archeologická
lokality	lokalita	k1gFnPc1	lokalita
<g/>
,	,	kIx,	,
vydávající	vydávající	k2eAgInPc1d1	vydávající
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
prvních	první	k4xOgMnPc2	první
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
Homo	Homo	k1gNnSc1	Homo
habilis	habilis	k1gFnSc2	habilis
<g/>
)	)	kIx)	)
i	i	k8xC	i
jejich	jejich	k3xOp3gMnPc2	jejich
předchůdců	předchůdce	k1gMnPc2	předchůdce
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Australopithecus	Australopithecus	k1gInSc1	Australopithecus
(	(	kIx(	(
<g/>
A.	A.	kA	A.
afarensis	afarensis	k1gInSc1	afarensis
<g/>
,	,	kIx,	,
A.	A.	kA	A.
aeethiopicus	aeethiopicus	k1gInSc1	aeethiopicus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Laetoli	Laetole	k1gFnSc6	Laetole
byly	být	k5eAaImAgInP	být
navíc	navíc	k6eAd1	navíc
zachyceny	zachycen	k2eAgInPc1d1	zachycen
otisky	otisk	k1gInPc1	otisk
stop	stop	k2eAgInPc2d1	stop
dvojnohých	dvojnohý	k2eAgInPc2d1	dvojnohý
homininů	hominin	k1gInPc2	hominin
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
nedaleká	daleký	k2eNgFnSc1d1	nedaleká
Olduvajská	Olduvajský	k2eAgFnSc1d1	Olduvajská
rokle	rokle	k1gFnSc1	rokle
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
nálezy	nález	k1gInPc1	nález
nejstarších	starý	k2eAgInPc2d3	nejstarší
kamenných	kamenný	k2eAgInPc2d1	kamenný
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
vnitrozemí	vnitrozemí	k1gNnSc4	vnitrozemí
Tanzanie	Tanzanie	k1gFnSc1	Tanzanie
osídleno	osídlen	k2eAgNnSc4d1	osídleno
kmeny	kmen	k1gInPc7	kmen
Sanů	San	k1gInPc2	San
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
postupně	postupně	k6eAd1	postupně
začaly	začít	k5eAaPmAgInP	začít
vytlačovat	vytlačovat	k5eAaImF	vytlačovat
bantuské	bantuský	k2eAgInPc1d1	bantuský
kmeny	kmen	k1gInPc1	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnPc4	pobřeží
a	a	k8xC	a
okolní	okolní	k2eAgInPc4d1	okolní
ostrovy	ostrov	k1gInPc4	ostrov
obsazovali	obsazovat	k5eAaImAgMnP	obsazovat
od	od	k7c2	od
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
arabští	arabský	k2eAgMnPc1d1	arabský
a	a	k8xC	a
perští	perský	k2eAgMnPc1d1	perský
obchodníci	obchodník	k1gMnPc1	obchodník
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
pobřežní	pobřežní	k2eAgFnSc6d1	pobřežní
oblasti	oblast	k1gFnSc6	oblast
začala	začít	k5eAaPmAgFnS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
islámská	islámský	k2eAgFnSc1d1	islámská
a	a	k8xC	a
arabsky	arabsky	k6eAd1	arabsky
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
kultura	kultura	k1gFnSc1	kultura
Svahilců	Svahilec	k1gMnPc2	Svahilec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1503	[number]	k4	1503
byl	být	k5eAaImAgInS	být
osídlen	osídlen	k2eAgInSc1d1	osídlen
ostrov	ostrov	k1gInSc1	ostrov
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
Portugalci	Portugalec	k1gMnPc1	Portugalec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1698	[number]	k4	1698
byl	být	k5eAaImAgInS	být
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
dobyt	dobýt	k5eAaPmNgInS	dobýt
maskatským	maskatský	k2eAgMnSc7d1	maskatský
sultánem	sultán	k1gMnSc7	sultán
z	z	k7c2	z
Ománu	Omán	k1gInSc2	Omán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
pobřeží	pobřeží	k1gNnSc2	pobřeží
ovládal	ovládat	k5eAaImAgMnS	ovládat
zanzibarský	zanzibarský	k2eAgMnSc1d1	zanzibarský
sultán	sultán	k1gMnSc1	sultán
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
stal	stát	k5eAaPmAgInS	stát
centrem	centr	k1gInSc7	centr
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
pro	pro	k7c4	pro
celé	celý	k2eAgNnSc4d1	celé
východoafrické	východoafrický	k2eAgNnSc4d1	Východoafrické
pobřeží	pobřeží	k1gNnSc4	pobřeží
a	a	k8xC	a
přes	přes	k7c4	přes
ostrov	ostrov	k1gInSc4	ostrov
prošlo	projít	k5eAaPmAgNnS	projít
ročně	ročně	k6eAd1	ročně
až	až	k9	až
50	[number]	k4	50
000	[number]	k4	000
otroků	otrok	k1gMnPc2	otrok
<g/>
.	.	kIx.	.
<g/>
Britové	Brit	k1gMnPc1	Brit
se	se	k3xPyFc4	se
se	s	k7c7	s
zanzibarským	zanzibarský	k2eAgMnSc7d1	zanzibarský
sultánem	sultán	k1gMnSc7	sultán
snažili	snažit	k5eAaImAgMnP	snažit
uzavírat	uzavírat	k5eAaImF	uzavírat
dohody	dohoda	k1gFnPc4	dohoda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
o	o	k7c4	o
oblast	oblast	k1gFnSc4	oblast
projevili	projevit	k5eAaPmAgMnP	projevit
zájem	zájem	k1gInSc1	zájem
Němci	Němec	k1gMnPc1	Němec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
se	se	k3xPyFc4	se
pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
oblast	oblast	k1gFnSc1	oblast
stala	stát	k5eAaPmAgFnS	stát
protektorátem	protektorát	k1gInSc7	protektorát
německé	německý	k2eAgFnSc2d1	německá
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Německá	německý	k2eAgFnSc1d1	německá
východní	východní	k2eAgFnSc1d1	východní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
získali	získat	k5eAaPmAgMnP	získat
Zanzibar	Zanzibar	k1gInSc4	Zanzibar
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
jiné	jiný	k2eAgNnSc4d1	jiné
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
došlo	dojít	k5eAaPmAgNnS	dojít
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
k	k	k7c3	k
odlivu	odliv	k1gInSc3	odliv
surovin	surovina	k1gFnPc2	surovina
včetně	včetně	k7c2	včetně
lidských	lidský	k2eAgInPc2d1	lidský
zdrojů	zdroj	k1gInPc2	zdroj
do	do	k7c2	do
válčící	válčící	k2eAgFnSc2d1	válčící
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
připadlo	připadnout	k5eAaPmAgNnS	připadnout
území	území	k1gNnSc1	území
Německé	německý	k2eAgFnSc2d1	německá
východní	východní	k2eAgFnSc2d1	východní
Afriky	Afrika	k1gFnSc2	Afrika
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Tanganika	Tanganika	k1gFnSc1	Tanganika
Velké	velká	k1gFnSc3	velká
Británii	Británie	k1gFnSc4	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
náleželo	náležet	k5eAaImAgNnS	náležet
území	území	k1gNnSc1	území
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
správu	správa	k1gFnSc4	správa
vykonávala	vykonávat	k5eAaImAgFnS	vykonávat
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Tanganický	tanganický	k2eAgInSc1d1	tanganický
africký	africký	k2eAgInSc1d1	africký
národní	národní	k2eAgInSc1d1	národní
svaz	svaz	k1gInSc1	svaz
(	(	kIx(	(
<g/>
TANU	tanout	k5eAaImIp1nS	tanout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1961	[number]	k4	1961
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
nezávislost	nezávislost	k1gFnSc1	nezávislost
Tanganiky	Tanganika	k1gFnSc2	Tanganika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
Tanganiky	Tanganika	k1gFnSc2	Tanganika
a	a	k8xC	a
Zanzibarské	zanzibarský	k2eAgFnSc2d1	zanzibarská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Tanzanie	Tanzanie	k1gFnSc1	Tanzanie
<g/>
.	.	kIx.	.
</s>
<s>
Zanzibarský	zanzibarský	k2eAgMnSc1d1	zanzibarský
sultán	sultán	k1gMnSc1	sultán
byl	být	k5eAaImAgMnS	být
svržen	svrhnout	k5eAaPmNgMnS	svrhnout
a	a	k8xC	a
při	při	k7c6	při
revoluci	revoluce	k1gFnSc6	revoluce
byly	být	k5eAaImAgInP	být
povražděny	povražděn	k2eAgInPc1d1	povražděn
tisíce	tisíc	k4xCgInPc1	tisíc
Indů	Ind	k1gMnPc2	Ind
a	a	k8xC	a
Arabů	Arab	k1gMnPc2	Arab
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
půd	půda	k1gFnPc2	půda
převládají	převládat	k5eAaImIp3nP	převládat
červenohnědé	červenohnědý	k2eAgFnPc1d1	červenohnědá
půdy	půda	k1gFnPc1	půda
savan	savana	k1gFnPc2	savana
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
ovlivňováno	ovlivňován	k2eAgNnSc4d1	ovlivňováno
monzuny	monzun	k1gInPc1	monzun
–	–	k?	–
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
podnebí	podnebí	k1gNnSc1	podnebí
severozápadní	severozápadní	k2eAgNnSc1d1	severozápadní
monzun	monzun	k1gInSc4	monzun
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
jihovýchodní	jihovýchodní	k2eAgInSc1d1	jihovýchodní
pasát	pasát	k1gInSc1	pasát
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
velikosti	velikost	k1gFnSc3	velikost
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
rozlišit	rozlišit	k5eAaPmF	rozlišit
čtyři	čtyři	k4xCgInPc4	čtyři
typy	typ	k1gInPc4	typ
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
období	období	k1gNnSc1	období
dešťů	dešť	k1gInPc2	dešť
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
měsíce	měsíc	k1gInPc4	měsíc
březen	březno	k1gNnPc2	březno
<g/>
,	,	kIx,	,
duben	duben	k1gInSc4	duben
a	a	k8xC	a
květen	květen	k1gInSc4	květen
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
drží	držet	k5eAaImIp3nS	držet
nad	nad	k7c7	nad
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
kromě	kromě	k7c2	kromě
vyšších	vysoký	k2eAgFnPc2d2	vyšší
nadmořských	nadmořský	k2eAgFnPc2d1	nadmořská
výšek	výška	k1gFnPc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
během	během	k7c2	během
celého	celý	k2eAgInSc2d1	celý
roku	rok	k1gInSc2	rok
příliš	příliš	k6eAd1	příliš
nemění	měnit	k5eNaImIp3nS	měnit
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
kolem	kolem	k7c2	kolem
30	[number]	k4	30
stupňů	stupeň	k1gInPc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
.	.	kIx.	.
</s>
<s>
Nejteplejšími	teplý	k2eAgInPc7d3	nejteplejší
měsíci	měsíc	k1gInPc7	měsíc
jsou	být	k5eAaImIp3nP	být
listopad	listopad	k1gInSc4	listopad
až	až	k9	až
únor	únor	k1gInSc4	únor
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
března	březen	k1gInSc2	březen
do	do	k7c2	do
května	květen	k1gInSc2	květen
a	a	k8xC	a
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
období	období	k1gNnSc2	období
dešťů	dešť	k1gInPc2	dešť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Říční	říční	k2eAgFnSc1d1	říční
síť	síť	k1gFnSc1	síť
není	být	k5eNaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
vyvinutá	vyvinutý	k2eAgFnSc1d1	vyvinutá
a	a	k8xC	a
převládají	převládat	k5eAaImIp3nP	převládat
krátké	krátký	k2eAgFnPc1d1	krátká
toky	toka	k1gFnPc1	toka
s	s	k7c7	s
peřejemi	peřej	k1gFnPc7	peřej
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
řekou	řeka	k1gFnSc7	řeka
je	být	k5eAaImIp3nS	být
Rovuma	Rovuma	k1gFnSc1	Rovuma
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
1100	[number]	k4	1100
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
významné	významný	k2eAgFnPc1d1	významná
řeky	řeka	k1gFnPc1	řeka
jsou	být	k5eAaImIp3nP	být
Rufiji	Rufiji	k1gFnSc4	Rufiji
(	(	kIx(	(
<g/>
cca	cca	kA	cca
100	[number]	k4	100
km	km	kA	km
splavný	splavný	k2eAgInSc4d1	splavný
úsek	úsek	k1gInSc4	úsek
<g/>
)	)	kIx)	)
a	a	k8xC	a
Great	Great	k2eAgInSc1d1	Great
ruah	ruah	k1gInSc1	ruah
<g/>
.	.	kIx.	.
</s>
<s>
Zemí	zem	k1gFnSc7	zem
protéká	protékat	k5eAaImIp3nS	protékat
přítok	přítok	k1gInSc1	přítok
Nilu	Nil	k1gInSc2	Nil
Kagera	Kagero	k1gNnSc2	Kagero
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgNnSc7d3	veliký
jezerem	jezero	k1gNnSc7	jezero
je	být	k5eAaImIp3nS	být
Viktoriino	Viktoriin	k2eAgNnSc1d1	Viktoriino
jezero	jezero	k1gNnSc1	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
i	i	k9	i
několik	několik	k4yIc4	několik
dalších	další	k2eAgNnPc2d1	další
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Tanganika	Tanganika	k1gFnSc1	Tanganika
<g/>
,	,	kIx,	,
Malawi	Malawi	k1gNnSc1	Malawi
a	a	k8xC	a
Rukwa	Rukwa	k1gFnSc1	Rukwa
<g/>
.	.	kIx.	.
</s>
<s>
Plošně	plošně	k6eAd1	plošně
největší	veliký	k2eAgFnSc7d3	veliký
rostlinnou	rostlinný	k2eAgFnSc7d1	rostlinná
formací	formace	k1gFnSc7	formace
jsou	být	k5eAaImIp3nP	být
travnaté	travnatý	k2eAgFnPc1d1	travnatá
savany	savana	k1gFnPc1	savana
a	a	k8xC	a
savanové	savanový	k2eAgInPc1d1	savanový
lesy	les	k1gInPc1	les
(	(	kIx(	(
<g/>
Miombo	Miomba	k1gFnSc5	Miomba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ekologicky	ekologicky	k6eAd1	ekologicky
významné	významný	k2eAgInPc1d1	významný
jsou	být	k5eAaImIp3nP	být
mangrovové	mangrovový	k2eAgInPc1d1	mangrovový
lesy	les	k1gInPc1	les
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
a	a	k8xC	a
afro-alpská	afrolpský	k2eAgFnSc1d1	afro-alpský
vegetace	vegetace	k1gFnSc1	vegetace
nejvyšších	vysoký	k2eAgNnPc2d3	nejvyšší
sopečných	sopečný	k2eAgNnPc2d1	sopečné
pohoří	pohoří	k1gNnPc2	pohoří
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
a	a	k8xC	a
rezervace	rezervace	k1gFnPc1	rezervace
jako	jako	k8xS	jako
Serengeti	Serenget	k1gMnPc1	Serenget
<g/>
,	,	kIx,	,
Kilimandžáro	Kilimandžáro	k1gNnSc1	Kilimandžáro
<g/>
,	,	kIx,	,
Ngorongoro	Ngorongora	k1gFnSc5	Ngorongora
<g/>
,	,	kIx,	,
Selous	Selous	k1gMnSc1	Selous
<g/>
,	,	kIx,	,
Arusha	Arusha	k1gMnSc1	Arusha
</s>
</p>
<p>
<s>
===	===	k?	===
Města	město	k1gNnSc2	město
===	===	k?	===
</s>
</p>
<p>
<s>
Dar	dar	k1gInSc1	dar
es	es	k1gNnSc1	es
Salaam	Salaam	k1gInSc1	Salaam
–	–	k?	–
2	[number]	k4	2
500	[number]	k4	500
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
Mwanza	Mwanza	k1gFnSc1	Mwanza
–	–	k?	–
223	[number]	k4	223
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
Dodoma	Dodoma	k1gFnSc1	Dodoma
–	–	k?	–
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
–	–	k?	–
204	[number]	k4	204
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
Tanga	tango	k1gNnSc2	tango
–	–	k?	–
184	[number]	k4	184
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
–	–	k?	–
158	[number]	k4	158
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
===	===	k?	===
Jezera	jezero	k1gNnSc2	jezero
===	===	k?	===
</s>
</p>
<p>
<s>
Tanganika	Tanganika	k1gFnSc1	Tanganika
</s>
</p>
<p>
<s>
Malawi	Malawi	k1gNnSc1	Malawi
</s>
</p>
<p>
<s>
Viktoriino	Viktoriin	k2eAgNnSc1d1	Viktoriino
jezero	jezero	k1gNnSc1	jezero
</s>
</p>
<p>
<s>
Eyasi	Eyase	k1gFnSc4	Eyase
</s>
</p>
<p>
<s>
Manyara	Manyara	k1gFnSc1	Manyara
–	–	k?	–
jezero	jezero	k1gNnSc1	jezero
proslulé	proslulý	k2eAgFnPc1d1	proslulá
populacemi	populace	k1gFnPc7	populace
plameňáků	plameňák	k1gMnPc2	plameňák
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
</s>
</p>
<p>
<s>
Natron	natron	k1gInSc1	natron
–	–	k?	–
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
se	se	k3xPyFc4	se
zvedá	zvedat	k5eAaImIp3nS	zvedat
činný	činný	k2eAgInSc1d1	činný
vulkán	vulkán	k1gInSc1	vulkán
Ol	Ola	k1gFnPc2	Ola
Doinyo	Doinyo	k1gMnSc1	Doinyo
Lengai	Lenga	k1gFnSc2	Lenga
(	(	kIx(	(
<g/>
z	z	k7c2	z
masajského	masajský	k2eAgInSc2d1	masajský
překladu	překlad	k1gInSc2	překlad
"	"	kIx"	"
<g/>
hora	hora	k1gFnSc1	hora
bohů	bůh	k1gMnPc2	bůh
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
příčina	příčina	k1gFnSc1	příčina
extrémně	extrémně	k6eAd1	extrémně
zásadité	zásaditý	k2eAgFnSc2d1	zásaditá
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
jezeře	jezero	k1gNnSc6	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Narůžovělá	narůžovělý	k2eAgFnSc1d1	narůžovělá
krusta	krusta	k1gFnSc1	krusta
z	z	k7c2	z
uhličitanu	uhličitan	k1gInSc2	uhličitan
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
,	,	kIx,	,
Vzkvétající	vzkvétající	k2eAgFnSc2d1	vzkvétající
populace	populace	k1gFnSc2	populace
plameňáků	plameňák	k1gMnPc2	plameňák
malých	malý	k2eAgMnPc2d1	malý
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
mikroskopickými	mikroskopický	k2eAgFnPc7d1	mikroskopická
řasami	řasa	k1gFnPc7	řasa
a	a	k8xC	a
pro	pro	k7c4	pro
něž	jenž	k3xRgMnPc4	jenž
je	být	k5eAaImIp3nS	být
jezero	jezero	k1gNnSc1	jezero
nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
místem	místo	k1gNnSc7	místo
pro	pro	k7c4	pro
páření	páření	k1gNnSc4	páření
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ostrovy	ostrov	k1gInPc1	ostrov
===	===	k?	===
</s>
</p>
<p>
<s>
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
</s>
</p>
<p>
<s>
Pemba	Pemba	k1gFnSc1	Pemba
</s>
</p>
<p>
<s>
Mafia	Mafia	k1gFnSc1	Mafia
</s>
</p>
<p>
<s>
===	===	k?	===
UNESCO	Unesco	k1gNnSc1	Unesco
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
světové	světový	k2eAgNnSc4d1	světové
dědictví	dědictví	k1gNnSc4	dědictví
UNESCO	UNESCO	kA	UNESCO
patří	patřit	k5eAaImIp3nS	patřit
chráněné	chráněný	k2eAgNnSc4d1	chráněné
území	území	k1gNnSc4	území
Ngorongoro	Ngorongora	k1gFnSc5	Ngorongora
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	daleko	k6eNd1	daleko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
rokle	rokle	k1gFnSc1	rokle
Olduvai	Olduva	k1gFnSc2	Olduva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
nalezeny	nalézt	k5eAaBmNgInP	nalézt
kosterní	kosterní	k2eAgInPc1d1	kosterní
zbytky	zbytek	k1gInPc1	zbytek
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgMnPc2d3	nejstarší
předchůdců	předchůdce	k1gMnPc2	předchůdce
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
hektarů	hektar	k1gInPc2	hektar
savany	savana	k1gFnSc2	savana
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
Serengeti	Serenget	k1gMnPc1	Serenget
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
přírodní	přírodní	k2eAgNnSc4d1	přírodní
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pozorovat	pozorovat	k5eAaImF	pozorovat
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
migraci	migrace	k1gFnSc4	migrace
velkých	velký	k2eAgNnPc2d1	velké
stád	stádo	k1gNnPc2	stádo
zeber	zebra	k1gFnPc2	zebra
<g/>
,	,	kIx,	,
pakoňů	pakůň	k1gMnPc2	pakůň
a	a	k8xC	a
antilop	antilopa	k1gFnPc2	antilopa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rezervaci	rezervace	k1gFnSc6	rezervace
Selous	Selous	k1gInSc1	Selous
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
zapsána	zapsat	k5eAaPmNgFnS	zapsat
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
UNESCO	Unesco	k1gNnSc1	Unesco
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
řada	řada	k1gFnSc1	řada
ohrožených	ohrožený	k2eAgNnPc2d1	ohrožené
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Kilimandžáro	Kilimandžáro	k1gNnSc1	Kilimandžáro
byl	být	k5eAaImAgInS	být
zapsán	zapsán	k2eAgInSc1d1	zapsán
do	do	k7c2	do
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Afriky	Afrika	k1gFnSc2	Afrika
Kilimandžáro	Kilimandžáro	k1gNnSc4	Kilimandžáro
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
5	[number]	k4	5
895	[number]	k4	895
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
je	být	k5eAaImIp3nS	být
Tanzanie	Tanzanie	k1gFnSc1	Tanzanie
republikou	republika	k1gFnSc7	republika
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
a	a	k8xC	a
současně	současně	k6eAd1	současně
předsedou	předseda	k1gMnSc7	předseda
je	být	k5eAaImIp3nS	být
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgMnPc4	dva
náměstky	náměstek	k1gMnPc4	náměstek
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
předsedou	předseda	k1gMnSc7	předseda
tamní	tamní	k2eAgFnSc2d1	tamní
autonomní	autonomní	k2eAgFnSc2d1	autonomní
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
Tanganika	Tanganika	k1gFnSc1	Tanganika
(	(	kIx(	(
<g/>
pevninskou	pevninský	k2eAgFnSc4d1	pevninská
část	část	k1gFnSc4	část
Tanzanie	Tanzanie	k1gFnSc2	Tanzanie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Národním	národní	k2eAgNnSc7d1	národní
shromážděním	shromáždění	k1gNnSc7	shromáždění
mají	mít	k5eAaImIp3nP	mít
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
je	být	k5eAaImIp3nS	být
jednokomorové	jednokomorový	k2eAgNnSc1d1	jednokomorové
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgMnS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
stranou	strana	k1gFnSc7	strana
Africký	africký	k2eAgInSc1d1	africký
národní	národní	k2eAgInSc1d1	národní
svaz	svaz	k1gInSc1	svaz
Tanganika	Tanganika	k1gFnSc1	Tanganika
(	(	kIx(	(
<g/>
Tanum	Tanum	k1gInSc1	Tanum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
===	===	k?	===
</s>
</p>
<p>
<s>
Tanzanie	Tanzanie	k1gFnSc1	Tanzanie
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
třiceti	třicet	k4xCc2	třicet
jedna	jeden	k4xCgFnSc1	jeden
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
26	[number]	k4	26
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
a	a	k8xC	a
5	[number]	k4	5
na	na	k7c6	na
Zanzibaru	Zanzibar	k1gInSc6	Zanzibar
a	a	k8xC	a
na	na	k7c4	na
99	[number]	k4	99
okresů	okres	k1gInPc2	okres
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgNnPc4	dva
hlavní	hlavní	k2eAgNnPc4d1	hlavní
města	město	k1gNnPc4	město
<g/>
:	:	kIx,	:
Dar	dar	k1gInSc1	dar
es	es	k1gNnSc1	es
Salaam	Salaam	k1gInSc1	Salaam
je	být	k5eAaImIp3nS	být
současné	současný	k2eAgNnSc4d1	současné
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
přesouvá	přesouvat	k5eAaImIp3nS	přesouvat
do	do	k7c2	do
města	město	k1gNnSc2	město
Dodoma	Dodomum	k1gNnSc2	Dodomum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Tanzanie	Tanzanie	k1gFnSc1	Tanzanie
je	být	k5eAaImIp3nS	být
chudá	chudý	k2eAgFnSc1d1	chudá
africká	africký	k2eAgFnSc1d1	africká
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
průměrné	průměrný	k2eAgFnPc4d1	průměrná
HDP	HDP	kA	HDP
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
dosahovalo	dosahovat	k5eAaImAgNnS	dosahovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
jen	jen	k9	jen
170	[number]	k4	170
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
85	[number]	k4	85
%	%	kIx~	%
exportu	export	k1gInSc2	export
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
tvořilo	tvořit	k5eAaImAgNnS	tvořit
24,5	[number]	k4	24,5
procent	procento	k1gNnPc2	procento
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
a	a	k8xC	a
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
polovinu	polovina	k1gFnSc4	polovina
ekonomicky	ekonomicky	k6eAd1	ekonomicky
aktivního	aktivní	k2eAgNnSc2d1	aktivní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Topografie	topografie	k1gFnPc1	topografie
a	a	k8xC	a
klimatické	klimatický	k2eAgFnPc1d1	klimatická
podmínky	podmínka	k1gFnPc1	podmínka
jej	on	k3xPp3gInSc2	on
však	však	k9	však
značně	značně	k6eAd1	značně
limitují	limitovat	k5eAaBmIp3nP	limitovat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pouze	pouze	k6eAd1	pouze
4	[number]	k4	4
%	%	kIx~	%
území	území	k1gNnPc1	území
jsou	být	k5eAaImIp3nP	být
kultivována	kultivován	k2eAgNnPc1d1	kultivováno
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
významným	významný	k2eAgInSc7d1	významný
zdrojem	zdroj	k1gInSc7	zdroj
příjmů	příjem	k1gInPc2	příjem
je	být	k5eAaImIp3nS	být
turismus	turismus	k1gInSc1	turismus
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
reformy	reforma	k1gFnPc1	reforma
veřejného	veřejný	k2eAgInSc2d1	veřejný
sektoru	sektor	k1gInSc2	sektor
a	a	k8xC	a
bankovnictví	bankovnictví	k1gNnSc2	bankovnictví
posilují	posilovat	k5eAaImIp3nP	posilovat
soukromý	soukromý	k2eAgInSc4d1	soukromý
sektor	sektor	k1gInSc4	sektor
<g/>
,	,	kIx,	,
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
pokrok	pokrok	k1gInSc4	pokrok
však	však	k9	však
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
brzdí	brzdit	k5eAaImIp3nS	brzdit
i	i	k9	i
vysoká	vysoký	k2eAgFnSc1d1	vysoká
míra	míra	k1gFnSc1	míra
korupce	korupce	k1gFnSc2	korupce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyváží	vyvážet	k5eAaImIp3nS	vyvážet
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
bavlna	bavlna	k1gFnSc1	bavlna
<g/>
,	,	kIx,	,
káva	káva	k1gFnSc1	káva
a	a	k8xC	a
ořechy	ořech	k1gInPc1	ořech
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
a	a	k8xC	a
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
panuje	panovat	k5eAaImIp3nS	panovat
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
ohromná	ohromný	k2eAgFnSc1d1	ohromná
bída	bída	k1gFnSc1	bída
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městech	město	k1gNnPc6	město
je	být	k5eAaImIp3nS	být
situace	situace	k1gFnSc1	situace
trochu	trochu	k6eAd1	trochu
stabilnější	stabilní	k2eAgFnSc1d2	stabilnější
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
politická	politický	k2eAgFnSc1d1	politická
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
zemi	zem	k1gFnSc4	zem
neohrožuje	ohrožovat	k5eNaImIp3nS	ohrožovat
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
její	její	k3xOp3gMnPc4	její
sousedy	soused	k1gMnPc4	soused
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Turismus	turismus	k1gInSc4	turismus
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
hospodářství	hospodářství	k1gNnSc4	hospodářství
Tanzanie	Tanzanie	k1gFnSc2	Tanzanie
začíná	začínat	k5eAaImIp3nS	začínat
být	být	k5eAaImF	být
velkým	velký	k2eAgInSc7d1	velký
přínosem	přínos	k1gInSc7	přínos
odvětví	odvětví	k1gNnSc2	odvětví
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
roste	růst	k5eAaImIp3nS	růst
z	z	k7c2	z
roku	rok	k1gInSc2	rok
na	na	k7c4	na
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
láká	lákat	k5eAaImIp3nS	lákat
turisty	turist	k1gMnPc4	turist
především	především	k9	především
na	na	k7c4	na
safari	safari	k1gNnSc4	safari
v	v	k7c6	v
národních	národní	k2eAgInPc6d1	národní
parcích	park	k1gInPc6	park
a	a	k8xC	a
rezervacích	rezervace	k1gFnPc6	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Serengeti	Serenget	k1gMnPc1	Serenget
a	a	k8xC	a
Ngorongoro	Ngorongora	k1gFnSc5	Ngorongora
jsou	být	k5eAaImIp3nP	být
světoznámé	světoznámý	k2eAgFnPc1d1	světoznámá
<g/>
.	.	kIx.	.
</s>
<s>
Velkým	velký	k2eAgNnSc7d1	velké
lákadlem	lákadlo	k1gNnSc7	lákadlo
je	být	k5eAaImIp3nS	být
také	také	k9	také
výstup	výstup	k1gInSc4	výstup
na	na	k7c4	na
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
horu	hora	k1gFnSc4	hora
Afriky	Afrika	k1gFnSc2	Afrika
–	–	k?	–
Kilimandžáro	Kilimandžáro	k1gNnSc1	Kilimandžáro
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
historických	historický	k2eAgFnPc2d1	historická
památek	památka	k1gFnPc2	památka
je	být	k5eAaImIp3nS	být
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
Stone	ston	k1gInSc5	ston
Town	Towno	k1gNnPc2	Towno
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
zcela	zcela	k6eAd1	zcela
ignorována	ignorován	k2eAgFnSc1d1	ignorována
je	být	k5eAaImIp3nS	být
Kilwa	Kilwa	k1gFnSc1	Kilwa
(	(	kIx(	(
<g/>
chrání	chránit	k5eAaImIp3nS	chránit
ji	on	k3xPp3gFnSc4	on
také	také	k9	také
UNESCO	UNESCO	kA	UNESCO
<g/>
)	)	kIx)	)
–	–	k?	–
historické	historický	k2eAgNnSc4d1	historické
centrum	centrum	k1gNnSc4	centrum
starobylé	starobylý	k2eAgFnSc2d1	starobylá
říše	říš	k1gFnSc2	říš
Kilwa	Kilw	k1gInSc2	Kilw
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rozmezí	rozmezí	k1gNnSc3	rozmezí
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Tanzanii	Tanzanie	k1gFnSc6	Tanzanie
silné	silný	k2eAgNnSc1d1	silné
střídání	střídání	k1gNnSc1	střídání
teplého	teplý	k2eAgNnSc2d1	teplé
a	a	k8xC	a
chladnějšího	chladný	k2eAgNnSc2d2	chladnější
počasí	počasí	k1gNnSc2	počasí
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Indického	indický	k2eAgInSc2d1	indický
oceánu	oceán	k1gInSc2	oceán
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
krásné	krásný	k2eAgFnPc4d1	krásná
pláže	pláž	k1gFnPc4	pláž
<g/>
.	.	kIx.	.
</s>
<s>
Nudismus	nudismus	k1gInSc1	nudismus
a	a	k8xC	a
opalování	opalování	k1gNnSc1	opalování
"	"	kIx"	"
<g/>
nahoře	nahoře	k6eAd1	nahoře
bez	bez	k1gInSc4	bez
<g/>
"	"	kIx"	"
není	být	k5eNaImIp3nS	být
povoleno	povolit	k5eAaPmNgNnS	povolit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
převládá	převládat	k5eAaImIp3nS	převládat
islám	islám	k1gInSc1	islám
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nošení	nošení	k1gNnSc4	nošení
vhodného	vhodný	k2eAgNnSc2d1	vhodné
oblečení	oblečení	k1gNnSc2	oblečení
nutností	nutnost	k1gFnPc2	nutnost
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
přechovávání	přechovávání	k1gNnSc4	přechovávání
drog	droga	k1gFnPc2	droga
jsou	být	k5eAaImIp3nP	být
přísné	přísný	k2eAgInPc1d1	přísný
tresty	trest	k1gInPc1	trest
<g/>
.	.	kIx.	.
</s>
<s>
Homosexualita	homosexualita	k1gFnSc1	homosexualita
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
projevy	projev	k1gInPc1	projev
nejsou	být	k5eNaImIp3nP	být
povoleny	povolen	k2eAgInPc1d1	povolen
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
za	za	k7c4	za
ni	on	k3xPp3gFnSc4	on
velké	velký	k2eAgInPc1d1	velký
tresty	trest	k1gInPc7	trest
(	(	kIx(	(
<g/>
až	až	k9	až
25	[number]	k4	25
let	léto	k1gNnPc2	léto
vězení	vězení	k1gNnSc2	vězení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nP	členit
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jsou	být	k5eAaImIp3nP	být
nejpočetnější	početní	k2eAgMnPc1d3	nejpočetnější
Svahilci	Svahilec	k1gMnPc1	Svahilec
<g/>
,	,	kIx,	,
Sukumové	Sukum	k1gMnPc1	Sukum
a	a	k8xC	a
Ňamweziové	Ňamweziový	k2eAgInPc1d1	Ňamweziový
hovořící	hovořící	k2eAgInPc1d1	hovořící
bantuskými	bantuský	k2eAgInPc7d1	bantuský
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
afrických	africký	k2eAgFnPc2d1	africká
zemí	zem	k1gFnPc2	zem
žije	žít	k5eAaImIp3nS	žít
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
.	.	kIx.	.
</s>
<s>
Gramotných	gramotný	k2eAgInPc2d1	gramotný
je	být	k5eAaImIp3nS	být
67	[number]	k4	67
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Tanzanci	Tanzanec	k1gMnPc1	Tanzanec
vyznávají	vyznávat	k5eAaImIp3nP	vyznávat
křesťanství	křesťanství	k1gNnSc4	křesťanství
(	(	kIx(	(
<g/>
Tanganika	Tanganika	k1gFnSc1	Tanganika
<g/>
)	)	kIx)	)
a	a	k8xC	a
islám	islám	k1gInSc1	islám
(	(	kIx(	(
<g/>
na	na	k7c6	na
Zanzibaru	Zanzibar	k1gInSc6	Zanzibar
až	až	k9	až
97	[number]	k4	97
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úředními	úřední	k2eAgMnPc7d1	úřední
jazyky	jazyk	k1gMnPc7	jazyk
jsou	být	k5eAaImIp3nP	být
angličtina	angličtina	k1gFnSc1	angličtina
a	a	k8xC	a
svahilština	svahilština	k1gFnSc1	svahilština
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
žijí	žít	k5eAaImIp3nP	žít
nilotští	nilotský	k2eAgMnPc1d1	nilotský
pastevci	pastevec	k1gMnPc1	pastevec
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
Masajů	Masaj	k1gMnPc2	Masaj
a	a	k8xC	a
lovci	lovec	k1gMnPc1	lovec
a	a	k8xC	a
sběrači	sběrač	k1gMnPc1	sběrač
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
Hadzů	Hadz	k1gInPc2	Hadz
<g/>
.	.	kIx.	.
</s>
<s>
Indové	Ind	k1gMnPc1	Ind
a	a	k8xC	a
Arabové	Arab	k1gMnPc1	Arab
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Ománu	Omán	k1gInSc2	Omán
žijí	žít	k5eAaImIp3nP	žít
především	především	k9	především
na	na	k7c6	na
Zanzibaru	Zanzibar	k1gInSc6	Zanzibar
a	a	k8xC	a
ve	v	k7c6	v
městech	město	k1gNnPc6	město
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zanzibaru	Zanzibar	k1gInSc6	Zanzibar
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
také	také	k9	také
zpěvák	zpěvák	k1gMnSc1	zpěvák
Freddie	Freddie	k1gFnSc2	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
jehož	jehož	k3xOyRp3gFnSc1	jehož
rodina	rodina	k1gFnSc1	rodina
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
Pársů	Párs	k1gMnPc2	Párs
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
68	[number]	k4	68
let	léto	k1gNnPc2	léto
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
<g/>
,	,	kIx,	,
70	[number]	k4	70
let	léto	k1gNnPc2	léto
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Tanzanie	Tanzanie	k1gFnSc2	Tanzanie
obyvatelé	obyvatel	k1gMnPc1	obyvatel
vyznávají	vyznávat	k5eAaImIp3nP	vyznávat
islám	islám	k1gInSc4	islám
již	již	k6eAd1	již
od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Muslimky	muslimka	k1gFnPc1	muslimka
nedbají	dbát	k5eNaImIp3nP	dbát
na	na	k7c4	na
zahalení	zahalení	k1gNnSc4	zahalení
<g/>
,	,	kIx,	,
nosí	nosit	k5eAaImIp3nS	nosit
jenom	jenom	k9	jenom
šátek	šátek	k1gInSc1	šátek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nechává	nechávat	k5eAaImIp3nS	nechávat
zcela	zcela	k6eAd1	zcela
odkrytý	odkrytý	k2eAgInSc4d1	odkrytý
obličej	obličej	k1gInSc4	obličej
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
nosí	nosit	k5eAaImIp3nP	nosit
typické	typický	k2eAgInPc4d1	typický
muslimské	muslimský	k2eAgInPc4d1	muslimský
hábity	hábit	k1gInPc4	hábit
nebo	nebo	k8xC	nebo
normální	normální	k2eAgNnSc4d1	normální
oblečení	oblečení	k1gNnSc4	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
jedna	jeden	k4xCgFnSc1	jeden
třetina	třetina	k1gFnSc1	třetina
obyvatel	obyvatel	k1gMnPc2	obyvatel
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
jsou	být	k5eAaImIp3nP	být
hluboce	hluboko	k6eAd1	hluboko
věřící	věřící	k2eAgMnPc1d1	věřící
a	a	k8xC	a
pravidelně	pravidelně	k6eAd1	pravidelně
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
nedělní	nedělní	k2eAgFnSc2d1	nedělní
mše	mše	k1gFnSc2	mše
<g/>
.	.	kIx.	.
</s>
<s>
Muslimové	muslim	k1gMnPc1	muslim
a	a	k8xC	a
křesťané	křesťan	k1gMnPc1	křesťan
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
respektují	respektovat	k5eAaImIp3nP	respektovat
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
povoluje	povolovat	k5eAaImIp3nS	povolovat
i	i	k9	i
smíšená	smíšený	k2eAgNnPc4d1	smíšené
manželství	manželství	k1gNnPc4	manželství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Populárním	populární	k2eAgInSc7d1	populární
hudebním	hudební	k2eAgInSc7d1	hudební
žánrem	žánr	k1gInSc7	žánr
v	v	k7c6	v
Tanzanii	Tanzanie	k1gFnSc6	Tanzanie
je	být	k5eAaImIp3nS	být
taarab	taarab	k1gInSc4	taarab
<g/>
,	,	kIx,	,
k	k	k7c3	k
jeho	jeho	k3xOp3gMnPc3	jeho
nejznámějším	známý	k2eAgMnPc3d3	nejznámější
představitelům	představitel	k1gMnPc3	představitel
patří	patřit	k5eAaImIp3nP	patřit
Bi	Bi	k1gMnSc1	Bi
Kidude	Kidud	k1gInSc5	Kidud
a	a	k8xC	a
Siti	Siti	k1gNnPc1	Siti
binti	binti	k1gNnSc2	binti
Saad	Saada	k1gFnPc2	Saada
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Tanzanie	Tanzanie	k1gFnSc2	Tanzanie
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
i	i	k9	i
slavný	slavný	k2eAgMnSc1d1	slavný
zpěvák	zpěvák	k1gMnSc1	zpěvák
skupiny	skupina	k1gFnSc2	skupina
Queen	Queen	k1gInSc1	Queen
Freddie	Freddie	k1gFnSc2	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
<g/>
.	.	kIx.	.
</s>
<s>
Herečka	herečka	k1gFnSc1	herečka
Rachel	Rachela	k1gFnPc2	Rachela
Luttrellová	Luttrellová	k1gFnSc1	Luttrellová
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
rolí	role	k1gFnSc7	role
Teyly	Teyla	k1gMnSc2	Teyla
Emmaganové	Emmaganová	k1gFnSc2	Emmaganová
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
televizním	televizní	k2eAgInSc6d1	televizní
seriálu	seriál	k1gInSc6	seriál
Stargate	Stargat	k1gInSc5	Stargat
Atlantis	Atlantis	k1gFnSc1	Atlantis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
se	se	k3xPyFc4	se
Tanzanie	Tanzanie	k1gFnSc1	Tanzanie
účastní	účastnit	k5eAaImIp3nS	účastnit
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
získala	získat	k5eAaPmAgFnS	získat
dvě	dva	k4xCgFnPc4	dva
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
medaile	medaile	k1gFnPc4	medaile
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc4	dva
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
v	v	k7c6	v
bězích	běh	k1gInPc6	běh
na	na	k7c4	na
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
tratě	trať	k1gFnPc4	trať
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
3000	[number]	k4	3000
metrů	metr	k1gInPc2	metr
s	s	k7c7	s
překážkami	překážka	k1gFnPc7	překážka
skončil	skončit	k5eAaPmAgInS	skončit
druhý	druhý	k4xOgInSc1	druhý
Filbert	Filbert	k1gInSc1	Filbert
Bayi	Bay	k1gFnSc2	Bay
<g/>
,	,	kIx,	,
na	na	k7c6	na
pěti	pět	k4xCc6	pět
kilometrech	kilometr	k1gInPc6	kilometr
pak	pak	k6eAd1	pak
Suleiman	Suleiman	k1gMnSc1	Suleiman
Nyambui	Nyambu	k1gFnSc2	Nyambu
<g/>
.	.	kIx.	.
</s>
<s>
Christopher	Christophra	k1gFnPc2	Christophra
Isengwe	Isengw	k1gInSc2	Isengw
získal	získat	k5eAaPmAgInS	získat
stříbro	stříbro	k1gNnSc4	stříbro
v	v	k7c6	v
maratonském	maratonský	k2eAgInSc6d1	maratonský
běhu	běh	k1gInSc6	běh
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Hasheem	Hasheum	k1gNnSc7	Hasheum
Thabeet	Thabeet	k1gMnSc1	Thabeet
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
tanzanským	tanzanský	k2eAgMnSc7d1	tanzanský
basketbalistou	basketbalista	k1gMnSc7	basketbalista
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
NBA	NBA	kA	NBA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1909	[number]	k4	1909
<g/>
–	–	k?	–
<g/>
1913	[number]	k4	1913
probíhaly	probíhat	k5eAaImAgFnP	probíhat
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Tanzanie	Tanzanie	k1gFnSc2	Tanzanie
velké	velký	k2eAgFnSc2d1	velká
vykopávky	vykopávka	k1gFnSc2	vykopávka
pod	pod	k7c7	pod
německým	německý	k2eAgNnSc7d1	německé
vedením	vedení	k1gNnSc7	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Slavná	slavný	k2eAgFnSc1d1	slavná
lokalita	lokalita	k1gFnSc1	lokalita
Tendaguru	Tendagur	k1gInSc2	Tendagur
dala	dát	k5eAaPmAgFnS	dát
světu	svět	k1gInSc3	svět
například	například	k6eAd1	například
kostru	kostra	k1gFnSc4	kostra
obřího	obří	k2eAgMnSc4d1	obří
dinosaura	dinosaurus	k1gMnSc4	dinosaurus
rodu	rod	k1gInSc2	rod
Giraffatitan	Giraffatitan	k1gInSc1	Giraffatitan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
archeologickém	archeologický	k2eAgNnSc6d1	Archeologické
nalezišti	naleziště	k1gNnSc6	naleziště
Ngorongoro	Ngorongora	k1gFnSc5	Ngorongora
byly	být	k5eAaImAgInP	být
zase	zase	k9	zase
nalezeny	nalezen	k2eAgInPc1d1	nalezen
nejstarší	starý	k2eAgInPc1d3	nejstarší
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
předchůdce	předchůdce	k1gMnSc2	předchůdce
dnešního	dnešní	k2eAgMnSc2d1	dnešní
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
homo	homo	k1gNnSc1	homo
habilis	habilis	k1gFnPc2	habilis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
lokalita	lokalita	k1gFnSc1	lokalita
zapsána	zapsat	k5eAaPmNgFnS	zapsat
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
se	se	k3xPyFc4	se
stejné	stejný	k2eAgFnSc2d1	stejná
pocty	pocta	k1gFnSc2	pocta
dočkaly	dočkat	k5eAaPmAgInP	dočkat
dva	dva	k4xCgInPc1	dva
historické	historický	k2eAgInPc1d1	historický
přístavy	přístav	k1gInPc1	přístav
<g/>
,	,	kIx,	,
Kilwa	Kilwum	k1gNnPc1	Kilwum
Kisiwani	Kisiwan	k1gMnPc1	Kisiwan
a	a	k8xC	a
Songo	Songo	k1gMnSc1	Songo
Mnara	Mnara	k1gMnSc1	Mnara
<g/>
.	.	kIx.	.
</s>
<s>
Obchodníci	obchodník	k1gMnPc1	obchodník
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
přístavů	přístav	k1gInPc2	přístav
<g/>
,	,	kIx,	,
ležících	ležící	k2eAgInPc2d1	ležící
na	na	k7c6	na
ostrůvcích	ostrůvek	k1gInPc6	ostrůvek
kousek	kousek	k6eAd1	kousek
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
obchodovali	obchodovat	k5eAaImAgMnP	obchodovat
mezi	mezi	k7c7	mezi
13	[number]	k4	13
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
stoletím	století	k1gNnSc7	století
se	se	k3xPyFc4	se
zlatem	zlato	k1gNnSc7	zlato
<g/>
,	,	kIx,	,
stříbrem	stříbro	k1gNnSc7	stříbro
<g/>
,	,	kIx,	,
perlami	perla	k1gFnPc7	perla
<g/>
,	,	kIx,	,
parfémy	parfém	k1gInPc7	parfém
<g/>
,	,	kIx,	,
keramikou	keramika	k1gFnSc7	keramika
a	a	k8xC	a
čínským	čínský	k2eAgInSc7d1	čínský
porcelánem	porcelán	k1gInSc7	porcelán
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
zboží	zboží	k1gNnSc2	zboží
dopravovaná	dopravovaný	k2eAgFnSc1d1	dopravovaná
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
přes	přes	k7c4	přes
Indický	indický	k2eAgInSc4d1	indický
oceán	oceán	k1gInSc4	oceán
těmito	tento	k3xDgFnPc7	tento
dvěma	dva	k4xCgFnPc7	dva
základnami	základna	k1gFnPc7	základna
prošla	projít	k5eAaPmAgFnS	projít
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
připsán	připsán	k2eAgInSc4d1	připsán
z	z	k7c2	z
podobných	podobný	k2eAgInPc2d1	podobný
důvodů	důvod	k1gInPc2	důvod
i	i	k8xC	i
přístav	přístav	k1gInSc1	přístav
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	s	k7c7	s
světovým	světový	k2eAgNnSc7d1	světové
dědictvím	dědictví	k1gNnSc7	dědictví
staly	stát	k5eAaPmAgFnP	stát
i	i	k9	i
skalní	skalní	k2eAgFnPc1d1	skalní
malby	malba	k1gFnPc1	malba
v	v	k7c4	v
Kondoa	Kondoum	k1gNnPc4	Kondoum
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
východních	východní	k2eAgInPc6d1	východní
svazích	svah	k1gInPc6	svah
Masajského	Masajský	k2eAgInSc2d1	Masajský
valu	val	k1gInSc2	val
<g/>
,	,	kIx,	,
v	v	k7c6	v
přírodní	přírodní	k2eAgFnSc6d1	přírodní
skalních	skalní	k2eAgInPc6d1	skalní
úkrytech	úkryt	k1gInPc6	úkryt
<g/>
.	.	kIx.	.
</s>
<s>
Malby	malba	k1gFnPc1	malba
tam	tam	k6eAd1	tam
lidé	člověk	k1gMnPc1	člověk
malují	malovat	k5eAaImIp3nP	malovat
asi	asi	k9	asi
2000	[number]	k4	2000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Skalních	skalní	k2eAgInPc2d1	skalní
úkrytů	úkryt	k1gInPc2	úkryt
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
150	[number]	k4	150
<g/>
,	,	kIx,	,
na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
ploše	plocha	k1gFnSc6	plocha
2	[number]	k4	2
400	[number]	k4	400
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
měst	město	k1gNnPc2	město
v	v	k7c6	v
Tanzanii	Tanzanie	k1gFnSc6	Tanzanie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tanzanie	Tanzanie	k1gFnSc2	Tanzanie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Tanzanie	Tanzanie	k1gFnSc2	Tanzanie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Tanzanie	Tanzanie	k1gFnSc2	Tanzanie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Parlament	parlament	k1gInSc1	parlament
Tanzanie	Tanzanie	k1gFnSc2	Tanzanie
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
</s>
</p>
<p>
<s>
mapy	mapa	k1gFnPc1	mapa
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c4	o
Tanzanii	Tanzanie	k1gFnSc4	Tanzanie
a	a	k8xC	a
tipy	tip	k1gInPc4	tip
na	na	k7c4	na
výlet	výlet	k1gInSc4	výlet
–	–	k?	–
česky	česky	k6eAd1	česky
</s>
</p>
<p>
<s>
Tanzania	Tanzanium	k1gNnPc1	Tanzanium
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tanzania	Tanzanium	k1gNnPc1	Tanzanium
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Tanzania	Tanzanium	k1gNnSc2	Tanzanium
Country	country	k2eAgInSc4d1	country
Report	report	k1gInSc4	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
African	African	k1gInSc1	African
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Not	k1gInSc2	Not
<g/>
:	:	kIx,	:
Tanzania	Tanzanium	k1gNnSc2	Tanzanium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-04-11	[number]	k4	2011-04-11
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Tanzania	Tanzanium	k1gNnSc2	Tanzanium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Nairobi	Nairobi	k1gNnSc6	Nairobi
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Tanzanie	Tanzanie	k1gFnSc1	Tanzanie
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2009-07-08	[number]	k4	2009-07-08
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BRYCESON	BRYCESON	kA	BRYCESON
<g/>
,	,	kIx,	,
Deborah	Deborah	k1gInSc4	Deborah
Fahy	Faha	k1gFnSc2	Faha
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Tanzania	Tanzanium	k1gNnPc1	Tanzanium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
