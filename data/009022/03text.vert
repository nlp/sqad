<p>
<s>
Lékařství	lékařství	k1gNnSc1	lékařství
nebo	nebo	k8xC	nebo
medicína	medicína	k1gFnSc1	medicína
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
ars	ars	k?	ars
medicina	medicin	k2eAgMnSc2d1	medicin
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc4	umění
léčit	léčit	k5eAaImF	léčit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
aplikovaná	aplikovaný	k2eAgFnSc1d1	aplikovaná
biologická	biologický	k2eAgFnSc1d1	biologická
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
zdraví	zdraví	k1gNnSc6	zdraví
<g/>
,	,	kIx,	,
stavech	stav	k1gInPc6	stav
a	a	k8xC	a
chorobných	chorobný	k2eAgInPc6d1	chorobný
procesech	proces	k1gInPc6	proces
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
o	o	k7c6	o
způsobech	způsob	k1gInPc6	způsob
léčení	léčení	k1gNnSc2	léčení
a	a	k8xC	a
předcházení	předcházení	k1gNnSc2	předcházení
nemocem	nemoc	k1gFnPc3	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
tohoto	tento	k3xDgInSc2	tento
vědního	vědní	k2eAgInSc2d1	vědní
oboru	obor	k1gInSc2	obor
je	být	k5eAaImIp3nS	být
chránit	chránit	k5eAaImF	chránit
a	a	k8xC	a
zlepšovat	zlepšovat	k5eAaImF	zlepšovat
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
opírá	opírat	k5eAaImIp3nS	opírat
o	o	k7c4	o
poznatky	poznatek	k1gInPc4	poznatek
biologie	biologie	k1gFnSc2	biologie
<g/>
,	,	kIx,	,
chemie	chemie	k1gFnSc2	chemie
<g/>
,	,	kIx,	,
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
také	také	k9	také
informatika	informatika	k1gFnSc1	informatika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
tradice	tradice	k1gFnSc2	tradice
současného	současný	k2eAgNnSc2d1	současné
lékařství	lékařství	k1gNnSc2	lékařství
byl	být	k5eAaImAgMnS	být
řecký	řecký	k2eAgMnSc1d1	řecký
lékař	lékař	k1gMnSc1	lékař
Hippokratés	Hippokratésa	k1gFnPc2	Hippokratésa
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
<s>
Lékařství	lékařství	k1gNnSc1	lékařství
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
teoretických	teoretický	k2eAgFnPc2d1	teoretická
a	a	k8xC	a
praktických	praktický	k2eAgFnPc2d1	praktická
disciplín	disciplína	k1gFnPc2	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
chirurgie	chirurgie	k1gFnSc1	chirurgie
<g/>
,	,	kIx,	,
zubní	zubní	k2eAgNnSc1d1	zubní
lékařství	lékařství	k1gNnSc1	lékařství
<g/>
,	,	kIx,	,
patologie	patologie	k1gFnSc1	patologie
<g/>
,	,	kIx,	,
imunologie	imunologie	k1gFnSc1	imunologie
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
==	==	k?	==
Východiska	východisko	k1gNnSc2	východisko
medicíny	medicína	k1gFnSc2	medicína
==	==	k?	==
</s>
</p>
<p>
<s>
Standardně	standardně	k6eAd1	standardně
pod	pod	k7c4	pod
pojem	pojem	k1gInSc4	pojem
medicína	medicína	k1gFnSc1	medicína
spadají	spadat	k5eAaImIp3nP	spadat
takové	takový	k3xDgInPc4	takový
obory	obor	k1gInPc4	obor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
využívají	využívat	k5eAaPmIp3nP	využívat
postupy	postup	k1gInPc1	postup
a	a	k8xC	a
metody	metoda	k1gFnPc1	metoda
vyučované	vyučovaný	k2eAgFnPc1d1	vyučovaná
na	na	k7c6	na
lékařských	lékařský	k2eAgFnPc6d1	lékařská
fakultách	fakulta	k1gFnPc6	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
lze	lze	k6eAd1	lze
prohlásit	prohlásit	k5eAaPmF	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
medicína	medicína	k1gFnSc1	medicína
je	být	k5eAaImIp3nS	být
aplikovaný	aplikovaný	k2eAgInSc4d1	aplikovaný
biologický	biologický	k2eAgInSc4d1	biologický
obor	obor	k1gInSc4	obor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vhodným	vhodný	k2eAgInSc7d1	vhodný
způsobem	způsob	k1gInSc7	způsob
přihlíží	přihlížet	k5eAaImIp3nS	přihlížet
i	i	k9	i
poznatkům	poznatek	k1gInPc3	poznatek
dalších	další	k2eAgFnPc2d1	další
disciplín	disciplína	k1gFnPc2	disciplína
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
disciplín	disciplína	k1gFnPc2	disciplína
z	z	k7c2	z
okruhu	okruh	k1gInSc2	okruh
chemie	chemie	k1gFnSc2	chemie
<g/>
,	,	kIx,	,
psychologie	psychologie	k1gFnSc2	psychologie
<g/>
,	,	kIx,	,
etiky	etika	k1gFnSc2	etika
a	a	k8xC	a
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
medicíny	medicína	k1gFnSc2	medicína
je	být	k5eAaImIp3nS	být
i	i	k9	i
vědecká	vědecký	k2eAgFnSc1d1	vědecká
metoda	metoda	k1gFnSc1	metoda
založená	založený	k2eAgFnSc1d1	založená
především	především	k9	především
na	na	k7c6	na
empirii	empirie	k1gFnSc6	empirie
a	a	k8xC	a
následném	následný	k2eAgNnSc6d1	následné
zobecňování	zobecňování	k1gNnSc6	zobecňování
těchto	tento	k3xDgInPc2	tento
poznatků	poznatek	k1gInPc2	poznatek
pomocí	pomocí	k7c2	pomocí
neúplné	úplný	k2eNgFnSc2d1	neúplná
indukce	indukce	k1gFnSc2	indukce
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yQnSc3	což
je	být	k5eAaImIp3nS	být
široce	široko	k6eAd1	široko
využívána	využíván	k2eAgFnSc1d1	využívána
statistika	statistika	k1gFnSc1	statistika
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
koncept	koncept	k1gInSc1	koncept
medicíny	medicína	k1gFnSc2	medicína
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Evidence	evidence	k1gFnSc1	evidence
Based	Based	k1gInSc1	Based
Medicine	Medicin	k1gInSc5	Medicin
(	(	kIx(	(
<g/>
EBM	EBM	kA	EBM
<g/>
,	,	kIx,	,
medicína	medicína	k1gFnSc1	medicína
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
důkazech	důkaz	k1gInPc6	důkaz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
EBM	EBM	kA	EBM
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
mladý	mladý	k2eAgInSc1d1	mladý
koncept	koncept	k1gInSc1	koncept
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
několik	několik	k4yIc1	několik
aspektů	aspekt	k1gInPc2	aspekt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
non-EBM	non-EBM	k?	non-EBM
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
oblibu	obliba	k1gFnSc4	obliba
některých	některý	k3yIgInPc2	některý
okrajových	okrajový	k2eAgInPc2d1	okrajový
terapeutických	terapeutický	k2eAgInPc2d1	terapeutický
postupů	postup	k1gInPc2	postup
(	(	kIx(	(
<g/>
např.	např.	kA	např.
podávání	podávání	k1gNnSc1	podávání
pentoxifylinu	pentoxifylin	k1gInSc2	pentoxifylin
<g/>
,	,	kIx,	,
systémová	systémový	k2eAgFnSc1d1	systémová
enzymoterapie	enzymoterapie	k1gFnSc1	enzymoterapie
nebo	nebo	k8xC	nebo
terapie	terapie	k1gFnSc1	terapie
soft-lasery	softasera	k1gFnSc2	soft-lasera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgInPc2	který
se	se	k3xPyFc4	se
očekává	očekávat	k5eAaImIp3nS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
přesunou	přesunout	k5eAaPmIp3nP	přesunout
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
alternativní	alternativní	k2eAgFnSc2d1	alternativní
medicíny	medicína	k1gFnSc2	medicína
s	s	k7c7	s
přirozeným	přirozený	k2eAgInSc7d1	přirozený
úbytkem	úbytek	k1gInSc7	úbytek
lékařů	lékař	k1gMnPc2	lékař
naučených	naučený	k2eAgInPc2d1	naučený
tyto	tento	k3xDgInPc4	tento
postupy	postup	k1gInPc4	postup
volit	volit	k5eAaImF	volit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Paradigma	paradigma	k1gNnSc1	paradigma
medicíny	medicína	k1gFnSc2	medicína
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
zdůraznění	zdůraznění	k1gNnSc4	zdůraznění
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
když	když	k8xS	když
má	mít	k5eAaImIp3nS	mít
medicína	medicína	k1gFnSc1	medicína
biologické	biologický	k2eAgFnPc4d1	biologická
kořeny	kořen	k2eAgFnPc4d1	Kořena
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
bio-psycho-socio-spirituálním	biosychoociopirituální	k2eAgNnSc6d1	bio-psycho-socio-spirituální
paradigmatu	paradigma	k1gNnSc6	paradigma
medicíny	medicína	k1gFnSc2	medicína
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
podstatou	podstata	k1gFnSc7	podstata
je	být	k5eAaImIp3nS	být
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
pacienta	pacient	k1gMnSc4	pacient
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
chorobu	choroba	k1gFnSc4	choroba
jako	jako	k8xS	jako
na	na	k7c4	na
výslednici	výslednice	k1gFnSc4	výslednice
působení	působení	k1gNnSc3	působení
biologických	biologický	k2eAgInPc2d1	biologický
<g/>
,	,	kIx,	,
psychologických	psychologický	k2eAgInPc2d1	psychologický
a	a	k8xC	a
sociálních	sociální	k2eAgInPc2d1	sociální
faktorů	faktor	k1gInPc2	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
paradigma	paradigma	k1gNnSc1	paradigma
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
rozvíjeno	rozvíjet	k5eAaImNgNnS	rozvíjet
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
explicitně	explicitně	k6eAd1	explicitně
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
faktory	faktor	k1gInPc1	faktor
ekologické	ekologický	k2eAgInPc1d1	ekologický
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
lze	lze	k6eAd1	lze
pokládat	pokládat	k5eAaImF	pokládat
za	za	k7c4	za
biologické	biologický	k2eAgInPc4d1	biologický
faktory	faktor	k1gInPc4	faktor
<g/>
,	,	kIx,	,
a	a	k8xC	a
faktory	faktor	k1gInPc1	faktor
spirituální	spirituální	k2eAgInPc1d1	spirituální
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
lze	lze	k6eAd1	lze
ovšem	ovšem	k9	ovšem
jen	jen	k9	jen
obtížně	obtížně	k6eAd1	obtížně
odlišit	odlišit	k5eAaPmF	odlišit
od	od	k7c2	od
faktorů	faktor	k1gInPc2	faktor
psychologických	psychologický	k2eAgInPc2d1	psychologický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zdůraznit	zdůraznit	k5eAaPmF	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
takové	takový	k3xDgNnSc1	takový
komplexní	komplexní	k2eAgNnSc1d1	komplexní
paradigma	paradigma	k1gNnSc1	paradigma
představuje	představovat	k5eAaImIp3nS	představovat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
řady	řada	k1gFnSc2	řada
onemocnění	onemocnění	k1gNnSc2	onemocnění
pouze	pouze	k6eAd1	pouze
faktor	faktor	k1gInSc1	faktor
komplikující	komplikující	k2eAgFnSc4d1	komplikující
diagnostiku	diagnostika	k1gFnSc4	diagnostika
a	a	k8xC	a
terapii	terapie	k1gFnSc4	terapie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
lékař	lékař	k1gMnSc1	lékař
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
pouze	pouze	k6eAd1	pouze
omezený	omezený	k2eAgInSc4d1	omezený
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
situace	situace	k1gFnSc1	situace
nastává	nastávat	k5eAaImIp3nS	nastávat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
choroby	choroba	k1gFnPc4	choroba
vzniklé	vzniklý	k2eAgFnPc4d1	vzniklá
bez	bez	k7c2	bez
jedné	jeden	k4xCgFnSc2	jeden
zjevné	zjevný	k2eAgFnSc2d1	zjevná
příčiny	příčina	k1gFnSc2	příčina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
sumace	sumace	k1gFnSc2	sumace
a	a	k8xC	a
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
působení	působení	k1gNnSc2	působení
řady	řada	k1gFnSc2	řada
malých	malý	k2eAgInPc2d1	malý
inzultů	inzult	k1gInPc2	inzult
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
může	moct	k5eAaImIp3nS	moct
psychika	psychika	k1gFnSc1	psychika
jedince	jedinko	k6eAd1	jedinko
i	i	k9	i
jeho	jeho	k3xOp3gNnSc4	jeho
sociální	sociální	k2eAgNnSc4d1	sociální
prostředí	prostředí	k1gNnSc4	prostředí
sehrát	sehrát	k5eAaPmF	sehrát
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
v	v	k7c6	v
tíži	tíž	k1gFnSc6	tíž
postižení	postižení	k1gNnSc6	postižení
<g/>
,	,	kIx,	,
klinickém	klinický	k2eAgInSc6d1	klinický
obrazu	obraz	k1gInSc6	obraz
i	i	k8xC	i
prognóze	prognóza	k1gFnSc6	prognóza
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
výrazně	výrazně	k6eAd1	výrazně
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
i	i	k9	i
rozhodování	rozhodování	k1gNnSc4	rozhodování
o	o	k7c6	o
vhodnosti	vhodnost	k1gFnSc6	vhodnost
terapeutických	terapeutický	k2eAgInPc2d1	terapeutický
postupů	postup	k1gInPc2	postup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
pacientovi	pacient	k1gMnSc3	pacient
===	===	k?	===
</s>
</p>
<p>
<s>
Původní	původní	k2eAgInSc1d1	původní
přístup	přístup	k1gInSc1	přístup
lékaře	lékař	k1gMnSc2	lékař
k	k	k7c3	k
pacientovi	pacient	k1gMnSc3	pacient
je	být	k5eAaImIp3nS	být
paternalistický	paternalistický	k2eAgMnSc1d1	paternalistický
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
lékař	lékař	k1gMnSc1	lékař
pacientovi	pacient	k1gMnSc3	pacient
předepisuje	předepisovat	k5eAaImIp3nS	předepisovat
režim	režim	k1gInSc4	režim
a	a	k8xC	a
jen	jen	k9	jen
málo	málo	k6eAd1	málo
jej	on	k3xPp3gNnSc4	on
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
probírá	probírat	k5eAaImIp3nS	probírat
<g/>
.	.	kIx.	.
</s>
<s>
Logickým	logický	k2eAgNnSc7d1	logické
opodstatněním	opodstatnění	k1gNnSc7	opodstatnění
tohoto	tento	k3xDgInSc2	tento
přístupu	přístup	k1gInSc2	přístup
je	být	k5eAaImIp3nS	být
jednak	jednak	k8xC	jednak
jeho	jeho	k3xOp3gFnSc1	jeho
ekonomičnost	ekonomičnost	k1gFnSc1	ekonomičnost
(	(	kIx(	(
<g/>
lékař	lékař	k1gMnSc1	lékař
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
pacienta	pacient	k1gMnSc4	pacient
méně	málo	k6eAd2	málo
času	čas	k1gInSc2	čas
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednak	jednak	k8xC	jednak
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
lékař	lékař	k1gMnSc1	lékař
má	mít	k5eAaImIp3nS	mít
alespoň	alespoň	k9	alespoň
teoreticky	teoreticky	k6eAd1	teoreticky
větší	veliký	k2eAgFnPc4d2	veliký
znalosti	znalost	k1gFnPc4	znalost
o	o	k7c6	o
chorobě	choroba	k1gFnSc6	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
paternalistického	paternalistický	k2eAgInSc2d1	paternalistický
přístupu	přístup	k1gInSc2	přístup
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pacient	pacient	k1gMnSc1	pacient
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
pasivním	pasivní	k2eAgInSc7d1	pasivní
v	v	k7c6	v
přístupu	přístup	k1gInSc6	přístup
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
chorobě	choroba	k1gFnSc3	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Extrémem	extrém	k1gInSc7	extrém
paternalistického	paternalistický	k2eAgInSc2d1	paternalistický
přístupu	přístup	k1gInSc2	přístup
je	být	k5eAaImIp3nS	být
tajení	tajení	k1gNnSc4	tajení
diagnózy	diagnóza	k1gFnSc2	diagnóza
s	s	k7c7	s
infaustní	infaustní	k2eAgFnSc7d1	infaustní
prognózou	prognóza	k1gFnSc7	prognóza
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
začíná	začínat	k5eAaImIp3nS	začínat
převládat	převládat	k5eAaImF	převládat
přístup	přístup	k1gInSc4	přístup
partnerský	partnerský	k2eAgInSc4d1	partnerský
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
přístup	přístup	k1gInSc1	přístup
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
lékař	lékař	k1gMnSc1	lékař
spíše	spíše	k9	spíše
průvodcem	průvodce	k1gMnSc7	průvodce
a	a	k8xC	a
pacientovi	pacient	k1gMnSc3	pacient
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
jeho	jeho	k3xOp3gInPc4	jeho
problémy	problém	k1gInPc4	problém
a	a	k8xC	a
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
další	další	k2eAgInSc4d1	další
postup	postup	k1gInSc4	postup
<g/>
.	.	kIx.	.
</s>
<s>
Nespornou	sporný	k2eNgFnSc7d1	nesporná
výhodou	výhoda	k1gFnSc7	výhoda
takového	takový	k3xDgInSc2	takový
postupu	postup	k1gInSc2	postup
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pacient	pacient	k1gMnSc1	pacient
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
vyšší	vysoký	k2eAgFnSc1d2	vyšší
potřeba	potřeba	k1gFnSc1	potřeba
času	čas	k1gInSc2	čas
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
pacienta	pacient	k1gMnSc4	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgInPc1	některý
typy	typ	k1gInPc1	typ
osobností	osobnost	k1gFnPc2	osobnost
preferují	preferovat	k5eAaImIp3nP	preferovat
paternalistický	paternalistický	k2eAgInSc4d1	paternalistický
přístup	přístup	k1gInSc4	přístup
<g/>
.	.	kIx.	.
</s>
<s>
Extrémem	extrém	k1gInSc7	extrém
partnerského	partnerský	k2eAgInSc2d1	partnerský
přístupu	přístup	k1gInSc2	přístup
je	být	k5eAaImIp3nS	být
popírání	popírání	k1gNnSc4	popírání
práva	právo	k1gNnSc2	právo
neznat	neznat	k5eAaImF	neznat
nepříznivou	příznivý	k2eNgFnSc4d1	nepříznivá
diagnózu	diagnóza	k1gFnSc4	diagnóza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obory	obor	k1gInPc1	obor
medicíny	medicína	k1gFnSc2	medicína
===	===	k?	===
</s>
</p>
<p>
<s>
Medicína	medicína	k1gFnSc1	medicína
jako	jako	k8xC	jako
celek	celek	k1gInSc1	celek
představuje	představovat	k5eAaImIp3nS	představovat
natolik	natolik	k6eAd1	natolik
složitou	složitý	k2eAgFnSc4d1	složitá
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
specializace	specializace	k1gFnSc1	specializace
<g/>
.	.	kIx.	.
</s>
<s>
Nechybí	chybět	k5eNaImIp3nS	chybět
však	však	k9	však
ani	ani	k8xC	ani
integrující	integrující	k2eAgFnSc1d1	integrující
složka	složka	k1gFnSc1	složka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
všeobecný	všeobecný	k2eAgMnSc1d1	všeobecný
praktický	praktický	k2eAgMnSc1d1	praktický
lékař	lékař	k1gMnSc1	lékař
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
obvoďák	obvoďák	k1gInSc1	obvoďák
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
ev.	ev.	k?	ev.
rodinný	rodinný	k2eAgMnSc1d1	rodinný
lékař	lékař	k1gMnSc1	lékař
ev.	ev.	k?	ev.
praktický	praktický	k2eAgMnSc1d1	praktický
lékař	lékař	k1gMnSc1	lékař
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
dorost	dorost	k1gInSc4	dorost
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
obory	obor	k1gInPc1	obor
medicíny	medicína	k1gFnSc2	medicína
dělí	dělit	k5eAaImIp3nP	dělit
obory	obor	k1gInPc1	obor
teoretické	teoretický	k2eAgInPc1d1	teoretický
<g/>
,	,	kIx,	,
paraklinické	paraklinický	k2eAgInPc1d1	paraklinický
a	a	k8xC	a
klinické	klinický	k2eAgInPc1d1	klinický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Teoretické	teoretický	k2eAgInPc1d1	teoretický
obory	obor	k1gInPc1	obor
====	====	k?	====
</s>
</p>
<p>
<s>
Teoretické	teoretický	k2eAgInPc1d1	teoretický
obory	obor	k1gInPc1	obor
představují	představovat	k5eAaImIp3nP	představovat
základní	základní	k2eAgInPc1d1	základní
obory	obor	k1gInPc1	obor
medicínského	medicínský	k2eAgNnSc2d1	medicínské
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Student	student	k1gMnSc1	student
medicíny	medicína	k1gFnSc2	medicína
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
seznámí	seznámit	k5eAaPmIp3nS	seznámit
obvykle	obvykle	k6eAd1	obvykle
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
s	s	k7c7	s
klinickými	klinický	k2eAgInPc7d1	klinický
obory	obor	k1gInPc7	obor
<g/>
,	,	kIx,	,
představují	představovat	k5eAaImIp3nP	představovat
biologický	biologický	k2eAgInSc4d1	biologický
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
vědecký	vědecký	k2eAgInSc4d1	vědecký
základ	základ	k1gInSc4	základ
medicíny	medicína	k1gFnSc2	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
sem	sem	k6eAd1	sem
zejména	zejména	k9	zejména
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
morfologické	morfologický	k2eAgInPc1d1	morfologický
obory	obor	k1gInPc1	obor
–	–	k?	–
anatomie	anatomie	k1gFnSc2	anatomie
<g/>
,	,	kIx,	,
histologie	histologie	k1gFnSc2	histologie
<g/>
,	,	kIx,	,
embryologie	embryologie	k1gFnSc2	embryologie
</s>
</p>
<p>
<s>
obory	obor	k1gInPc1	obor
přírodovědného	přírodovědný	k2eAgInSc2d1	přírodovědný
základu	základ	k1gInSc2	základ
–	–	k?	–
lékařská	lékařský	k2eAgFnSc1d1	lékařská
biofyzika	biofyzika	k1gFnSc1	biofyzika
<g/>
,	,	kIx,	,
biochemie	biochemie	k1gFnSc1	biochemie
<g/>
,	,	kIx,	,
biologie	biologie	k1gFnSc1	biologie
<g/>
,	,	kIx,	,
genetika	genetika	k1gFnSc1	genetika
<g/>
,	,	kIx,	,
mikrobiologie	mikrobiologie	k1gFnSc1	mikrobiologie
<g/>
,	,	kIx,	,
fyziologie	fyziologie	k1gFnSc1	fyziologie
<g/>
,	,	kIx,	,
patologická	patologický	k2eAgFnSc1d1	patologická
fyziologie	fyziologie	k1gFnSc1	fyziologie
<g/>
,	,	kIx,	,
imunologie	imunologie	k1gFnSc1	imunologie
<g/>
,	,	kIx,	,
neurovědy	neurověda	k1gFnPc1	neurověda
</s>
</p>
<p>
<s>
obory	obor	k1gInPc1	obor
přesahující	přesahující	k2eAgFnSc4d1	přesahující
medicínu	medicína	k1gFnSc4	medicína
–	–	k?	–
lékařská	lékařský	k2eAgFnSc1d1	lékařská
etika	etika	k1gFnSc1	etika
<g/>
,	,	kIx,	,
filozofie	filozofie	k1gFnSc1	filozofie
medicíny	medicína	k1gFnSc2	medicína
<g/>
,	,	kIx,	,
lékařská	lékařský	k2eAgFnSc1d1	lékařská
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
,	,	kIx,	,
biostatistika	biostatistika	k1gFnSc1	biostatistika
<g/>
,	,	kIx,	,
lékařská	lékařský	k2eAgFnSc1d1	lékařská
informatika	informatika	k1gFnSc1	informatika
<g/>
,	,	kIx,	,
demografie	demografie	k1gFnSc1	demografie
<g/>
,	,	kIx,	,
medicínská	medicínský	k2eAgFnSc1d1	medicínská
geografie	geografie	k1gFnSc1	geografie
</s>
</p>
<p>
<s>
====	====	k?	====
Paraklinické	Paraklinický	k2eAgInPc1d1	Paraklinický
obory	obor	k1gInPc1	obor
====	====	k?	====
</s>
</p>
<p>
<s>
Paraklinické	Paraklinický	k2eAgInPc1d1	Paraklinický
obory	obor	k1gInPc1	obor
jsou	být	k5eAaImIp3nP	být
takové	takový	k3xDgInPc1	takový
obory	obor	k1gInPc1	obor
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
lékaři	lékař	k1gMnPc1	lékař
obvykle	obvykle	k6eAd1	obvykle
nepřichází	přicházet	k5eNaImIp3nP	přicházet
do	do	k7c2	do
styku	styk	k1gInSc2	styk
s	s	k7c7	s
pacienty	pacient	k1gMnPc7	pacient
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
některé	některý	k3yIgNnSc4	některý
nebo	nebo	k8xC	nebo
i	i	k8xC	i
všechny	všechen	k3xTgFnPc4	všechen
činnosti	činnost	k1gFnPc4	činnost
paraklinických	paraklinický	k2eAgInPc2d1	paraklinický
oborů	obor	k1gInPc2	obor
delegovány	delegovat	k5eAaBmNgFnP	delegovat
na	na	k7c4	na
pracovníky	pracovník	k1gMnPc4	pracovník
bez	bez	k7c2	bez
lékařského	lékařský	k2eAgNnSc2d1	lékařské
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
o	o	k7c4	o
obory	obor	k1gInPc4	obor
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
diagnostické	diagnostický	k2eAgInPc1d1	diagnostický
–	–	k?	–
klinická	klinický	k2eAgFnSc1d1	klinická
biochemie	biochemie	k1gFnSc1	biochemie
<g/>
,	,	kIx,	,
klinická	klinický	k2eAgFnSc1d1	klinická
mikrobiologie	mikrobiologie	k1gFnSc1	mikrobiologie
<g/>
,	,	kIx,	,
radiodiagnostika	radiodiagnostika	k1gFnSc1	radiodiagnostika
<g/>
,	,	kIx,	,
histopatologie	histopatologie	k1gFnSc1	histopatologie
<g/>
,	,	kIx,	,
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
preventivní	preventivní	k2eAgFnSc1d1	preventivní
–	–	k?	–
hygiena	hygiena	k1gFnSc1	hygiena
<g/>
,	,	kIx,	,
epidemiologie	epidemiologie	k1gFnSc1	epidemiologie
</s>
</p>
<p>
<s>
poradní	poradní	k2eAgFnSc1d1	poradní
–	–	k?	–
klinická	klinický	k2eAgFnSc1d1	klinická
farmakologie	farmakologie	k1gFnSc1	farmakologie
</s>
</p>
<p>
<s>
====	====	k?	====
Klinické	klinický	k2eAgInPc1d1	klinický
obory	obor	k1gInPc1	obor
====	====	k?	====
</s>
</p>
<p>
<s>
Klinické	klinický	k2eAgInPc1d1	klinický
obory	obor	k1gInPc1	obor
bývá	bývat	k5eAaImIp3nS	bývat
zvykem	zvyk	k1gInSc7	zvyk
dělit	dělit	k5eAaImF	dělit
na	na	k7c4	na
chirurgické	chirurgický	k2eAgFnPc4d1	chirurgická
a	a	k8xC	a
nechirurgické	chirurgický	k2eNgFnPc4d1	nechirurgická
<g/>
.	.	kIx.	.
</s>
<s>
Chirurgické	chirurgický	k2eAgInPc1d1	chirurgický
obory	obor	k1gInPc1	obor
jsou	být	k5eAaImIp3nP	být
takové	takový	k3xDgInPc1	takový
obory	obor	k1gInPc1	obor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
tradičním	tradiční	k2eAgInSc7d1	tradiční
léčebným	léčebný	k2eAgInSc7d1	léčebný
postupem	postup	k1gInSc7	postup
manuální	manuální	k2eAgInSc4d1	manuální
zákrok	zákrok	k1gInSc4	zákrok
(	(	kIx(	(
<g/>
chirurugie	chirurugie	k1gFnSc2	chirurugie
<g/>
,	,	kIx,	,
ortopedie	ortopedie	k1gFnSc2	ortopedie
<g/>
,	,	kIx,	,
gynekologie	gynekologie	k1gFnSc2	gynekologie
a	a	k8xC	a
porodnictví	porodnictví	k1gNnSc2	porodnictví
<g/>
,	,	kIx,	,
urologie	urologie	k1gFnSc2	urologie
a	a	k8xC	a
ORL	ORL	kA	ORL
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nechirurgické	chirurgický	k2eNgInPc1d1	nechirurgický
obory	obor	k1gInPc1	obor
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
nesprávně	správně	k6eNd1	správně
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k8xC	jako
obory	obor	k1gInPc1	obor
interní	interní	k2eAgInPc1d1	interní
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc4	jejich
léčebné	léčebný	k2eAgInPc4d1	léčebný
postupy	postup	k1gInPc4	postup
obvykle	obvykle	k6eAd1	obvykle
spočívají	spočívat	k5eAaImIp3nP	spočívat
v	v	k7c6	v
nekrvavém	krvavý	k2eNgNnSc6d1	nekrvavé
ovlivnění	ovlivnění	k1gNnSc6	ovlivnění
funkcí	funkce	k1gFnPc2	funkce
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dělení	dělení	k1gNnSc1	dělení
však	však	k9	však
zrácí	zrácet	k5eAaImIp3nS	zrácet
na	na	k7c6	na
významu	význam	k1gInSc6	význam
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
např.	např.	kA	např.
endoskopické	endoskopický	k2eAgInPc4d1	endoskopický
výkony	výkon	k1gInPc4	výkon
provádí	provádět	k5eAaImIp3nS	provádět
i	i	k9	i
řada	řada	k1gFnSc1	řada
interních	interní	k2eAgMnPc2d1	interní
oborů	obor	k1gInPc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
Poněkud	poněkud	k6eAd1	poněkud
výjimečné	výjimečný	k2eAgNnSc4d1	výjimečné
postavení	postavení	k1gNnSc4	postavení
má	mít	k5eAaImIp3nS	mít
psychiatrie	psychiatrie	k1gFnSc1	psychiatrie
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
objektem	objekt	k1gInSc7	objekt
jejich	jejich	k3xOp3gInSc2	jejich
zájmu	zájem	k1gInSc2	zájem
jsou	být	k5eAaImIp3nP	být
poruchy	porucha	k1gFnPc1	porucha
psychiky	psychika	k1gFnSc2	psychika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Alternativní	alternativní	k2eAgFnSc1d1	alternativní
medicína	medicína	k1gFnSc1	medicína
==	==	k?	==
</s>
</p>
<p>
<s>
Alternativní	alternativní	k2eAgFnSc1d1	alternativní
medicína	medicína	k1gFnSc1	medicína
není	být	k5eNaImIp3nS	být
alternativou	alternativa	k1gFnSc7	alternativa
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
jiné	jiný	k2eAgFnSc2d1	jiná
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
alternativní	alternativní	k2eAgFnSc1d1	alternativní
medicína	medicína	k1gFnSc1	medicína
je	být	k5eAaImIp3nS	být
alternativní	alternativní	k2eAgFnSc1d1	alternativní
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
jiných	jiný	k2eAgNnPc2d1	jiné
filozofických	filozofický	k2eAgNnPc2d1	filozofické
východisek	východisko	k1gNnPc2	východisko
<g/>
.	.	kIx.	.
</s>
<s>
Alternativní	alternativní	k2eAgFnSc1d1	alternativní
medicína	medicína	k1gFnSc1	medicína
není	být	k5eNaImIp3nS	být
jednotný	jednotný	k2eAgInSc4d1	jednotný
proud	proud	k1gInSc4	proud
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
někdy	někdy	k6eAd1	někdy
provázaných	provázaný	k2eAgFnPc2d1	provázaná
a	a	k8xC	a
jindy	jindy	k6eAd1	jindy
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
navzájem	navzájem	k6eAd1	navzájem
potírajících	potírající	k2eAgInPc2d1	potírající
proudů	proud	k1gInPc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Společným	společný	k2eAgInSc7d1	společný
jmenovatelem	jmenovatel	k1gInSc7	jmenovatel
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
opozice	opozice	k1gFnSc1	opozice
vůči	vůči	k7c3	vůči
medicíně	medicína	k1gFnSc3	medicína
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
klasická	klasický	k2eAgFnSc1d1	klasická
<g/>
,	,	kIx,	,
akademická	akademický	k2eAgFnSc1d1	akademická
<g/>
,	,	kIx,	,
školní	školní	k2eAgFnSc1d1	školní
nebo	nebo	k8xC	nebo
např.	např.	kA	např.
alopatická	alopatický	k2eAgNnPc1d1	alopatické
<g/>
.	.	kIx.	.
</s>
<s>
Pragmatický	pragmatický	k2eAgInSc1d1	pragmatický
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
alternativní	alternativní	k2eAgFnSc4d1	alternativní
medicínu	medicína	k1gFnSc4	medicína
definuje	definovat	k5eAaBmIp3nS	definovat
alternativní	alternativní	k2eAgFnSc4d1	alternativní
medicínu	medicína	k1gFnSc4	medicína
jako	jako	k8xC	jako
takové	takový	k3xDgInPc1	takový
proudy	proud	k1gInPc1	proud
v	v	k7c6	v
diagnostice	diagnostika	k1gFnSc6	diagnostika
<g/>
,	,	kIx,	,
prevenci	prevence	k1gFnSc6	prevence
a	a	k8xC	a
léčení	léčení	k1gNnSc6	léčení
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
používají	používat	k5eAaImIp3nP	používat
překonané	překonaný	k2eAgFnPc1d1	překonaná
<g/>
,	,	kIx,	,
nedostatečně	dostatečně	k6eNd1	dostatečně
efektivní	efektivní	k2eAgFnSc3d1	efektivní
<g/>
,	,	kIx,	,
neprokázané	prokázaný	k2eNgFnSc3d1	neprokázaná
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
neprokazatelné	prokazatelný	k2eNgFnPc4d1	neprokazatelná
metody	metoda	k1gFnPc4	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zařazení	zařazení	k1gNnSc4	zařazení
není	být	k5eNaImIp3nS	být
podstatné	podstatný	k2eAgNnSc1d1	podstatné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
má	mít	k5eAaImIp3nS	mít
příslušná	příslušný	k2eAgFnSc1d1	příslušná
metoda	metoda	k1gFnSc1	metoda
racionální	racionální	k2eAgFnSc1d1	racionální
základ	základ	k1gInSc1	základ
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
výsledky	výsledek	k1gInPc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
zastánců	zastánce	k1gMnPc2	zastánce
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
proudů	proud	k1gInPc2	proud
je	být	k5eAaImIp3nS	být
i	i	k9	i
toto	tento	k3xDgNnSc1	tento
problematické	problematický	k2eAgNnSc1d1	problematické
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
možnost	možnost	k1gFnSc1	možnost
testování	testování	k1gNnSc2	testování
východisek	východisko	k1gNnPc2	východisko
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
i	i	k9	i
výsledků	výsledek	k1gInPc2	výsledek
aktivně	aktivně	k6eAd1	aktivně
popírají	popírat	k5eAaImIp3nP	popírat
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
pokládají	pokládat	k5eAaImIp3nP	pokládat
vlastní	vlastní	k2eAgNnSc4d1	vlastní
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
za	za	k7c4	za
dostatečný	dostatečný	k2eAgInSc4d1	dostatečný
důkaz	důkaz	k1gInSc4	důkaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
výše	výše	k1gFnSc2	výše
uvedeného	uvedený	k2eAgNnSc2d1	uvedené
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
alternativní	alternativní	k2eAgFnSc1d1	alternativní
medicína	medicína	k1gFnSc1	medicína
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
oborů	obor	k1gInPc2	obor
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
medicíny	medicína	k1gFnSc2	medicína
a	a	k8xC	a
alternativní	alternativní	k2eAgFnSc2d1	alternativní
medicíny	medicína	k1gFnSc2	medicína
(	(	kIx(	(
<g/>
např.	např.	kA	např.
fytoterapie	fytoterapie	k1gFnSc2	fytoterapie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiné	jiná	k1gFnPc1	jiná
nemají	mít	k5eNaImIp3nP	mít
s	s	k7c7	s
medicínou	medicína	k1gFnSc7	medicína
jako	jako	k8xC	jako
aplikovaným	aplikovaný	k2eAgInSc7d1	aplikovaný
přírodovědným	přírodovědný	k2eAgInSc7d1	přírodovědný
oborem	obor	k1gInSc7	obor
společného	společný	k2eAgNnSc2d1	společné
vůbec	vůbec	k9	vůbec
nic	nic	k3yNnSc1	nic
(	(	kIx(	(
<g/>
např.	např.	kA	např.
homeopatie	homeopatie	k1gFnSc2	homeopatie
nebo	nebo	k8xC	nebo
reiki	reik	k1gFnSc2	reik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
alternativní	alternativní	k2eAgFnSc2d1	alternativní
medicíny	medicína	k1gFnSc2	medicína
mohou	moct	k5eAaImIp3nP	moct
patřit	patřit	k5eAaImF	patřit
některé	některý	k3yIgFnPc4	některý
tradiční	tradiční	k2eAgFnPc4d1	tradiční
metody	metoda	k1gFnPc4	metoda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
akupunktura	akupunktura	k1gFnSc1	akupunktura
nebo	nebo	k8xC	nebo
ájurvéda	ájurvéda	k1gFnSc1	ájurvéda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
metody	metoda	k1gFnSc2	metoda
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
jako	jako	k8xC	jako
slepé	slepý	k2eAgFnSc2d1	slepá
větve	větev	k1gFnSc2	větev
během	během	k7c2	během
vývoje	vývoj	k1gInSc2	vývoj
medicíny	medicína	k1gFnSc2	medicína
jako	jako	k8xC	jako
oboru	obor	k1gInSc2	obor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
homeopatie	homeopatie	k1gFnSc2	homeopatie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
obory	obor	k1gInPc1	obor
relativně	relativně	k6eAd1	relativně
nové	nový	k2eAgInPc1d1	nový
a	a	k8xC	a
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
jako	jako	k8xS	jako
produkt	produkt	k1gInSc1	produkt
nových	nový	k2eAgInPc2d1	nový
metafyzických	metafyzický	k2eAgInPc2d1	metafyzický
systémů	systém	k1gInPc2	systém
(	(	kIx(	(
<g/>
např.	např.	kA	např.
reiki	reiki	k1gNnSc1	reiki
nebo	nebo	k8xC	nebo
antroposofická	antroposofický	k2eAgFnSc1d1	antroposofická
medicína	medicína	k1gFnSc1	medicína
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
nápodoba	nápodoba	k1gFnSc1	nápodoba
technického	technický	k2eAgInSc2d1	technický
pokroku	pokrok	k1gInSc2	pokrok
(	(	kIx(	(
<g/>
např.	např.	kA	např.
elektroakupunktura	elektroakupunktura	k1gFnSc1	elektroakupunktura
dle	dle	k7c2	dle
Volla	Vollo	k1gNnSc2	Vollo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
vysoké	vysoký	k2eAgFnSc3d1	vysoká
popularitě	popularita	k1gFnSc3	popularita
některých	některý	k3yIgInPc2	některý
oborů	obor	k1gInPc2	obor
alternativní	alternativní	k2eAgFnSc2d1	alternativní
medicíny	medicína	k1gFnSc2	medicína
pronikají	pronikat	k5eAaImIp3nP	pronikat
některé	některý	k3yIgInPc1	některý
proudy	proud	k1gInPc1	proud
alternativní	alternativní	k2eAgFnSc2d1	alternativní
medicíny	medicína	k1gFnSc2	medicína
do	do	k7c2	do
výuky	výuka	k1gFnSc2	výuka
na	na	k7c6	na
lékařských	lékařský	k2eAgFnPc6d1	lékařská
fakultách	fakulta	k1gFnPc6	fakulta
ať	ať	k8xC	ať
již	již	k6eAd1	již
jako	jako	k9	jako
dílčí	dílčí	k2eAgInPc1d1	dílčí
kurzy	kurz	k1gInPc1	kurz
(	(	kIx(	(
<g/>
kurzy	kurz	k1gInPc1	kurz
akupunktury	akupunktura	k1gFnSc2	akupunktura
nebo	nebo	k8xC	nebo
homeopatie	homeopatie	k1gFnSc2	homeopatie
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
jako	jako	k9	jako
samostatné	samostatný	k2eAgNnSc4d1	samostatné
studium	studium	k1gNnSc4	studium
(	(	kIx(	(
<g/>
např.	např.	kA	např.
studium	studium	k1gNnSc4	studium
osteopatie	osteopatie	k1gFnSc2	osteopatie
v	v	k7c6	v
USA	USA	kA	USA
nebo	nebo	k8xC	nebo
studium	studium	k1gNnSc4	studium
tradiční	tradiční	k2eAgFnSc2d1	tradiční
čínské	čínský	k2eAgFnSc2d1	čínská
medicíny	medicína	k1gFnSc2	medicína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lékařské	lékařský	k2eAgInPc1d1	lékařský
směry	směr	k1gInPc1	směr
v	v	k7c6	v
USA	USA	kA	USA
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
allopatie	allopatie	k1gFnSc1	allopatie
</s>
</p>
<p>
<s>
eklektická	eklektický	k2eAgFnSc1d1	eklektická
medicína	medicína	k1gFnSc1	medicína
</s>
</p>
<p>
<s>
homeopatie	homeopatie	k1gFnSc1	homeopatie
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Atestace	atestace	k1gFnSc1	atestace
lékaře	lékař	k1gMnSc2	lékař
v	v	k7c6	v
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Lege	Lege	k1gFnSc1	Lege
artis	artis	k1gFnSc2	artis
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
lékařů	lékař	k1gMnPc2	lékař
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
českých	český	k2eAgMnPc2d1	český
lékařů	lékař	k1gMnPc2	lékař
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
lékařských	lékařský	k2eAgFnPc2d1	lékařská
odborností	odbornost	k1gFnPc2	odbornost
</s>
</p>
<p>
<s>
Zdraví	zdraví	k1gNnSc1	zdraví
</s>
</p>
<p>
<s>
Národní	národní	k2eAgFnSc1d1	národní
lékařská	lékařský	k2eAgFnSc1d1	lékařská
knihovna	knihovna	k1gFnSc1	knihovna
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
lékařství	lékařství	k1gNnSc2	lékařství
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Lékařství	lékařství	k1gNnSc2	lékařství
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
lékařství	lékařství	k1gNnSc2	lékařství
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Databáze	databáze	k1gFnPc1	databáze
lékařských	lékařský	k2eAgInPc2d1	lékařský
a	a	k8xC	a
biologických	biologický	k2eAgInPc2d1	biologický
časopiseckých	časopisecký	k2eAgInPc2d1	časopisecký
článku	článek	k1gInSc6	článek
PubMed	PubMed	k1gInSc4	PubMed
</s>
</p>
<p>
<s>
Databáze	databáze	k1gFnPc1	databáze
pro	pro	k7c4	pro
snadné	snadný	k2eAgNnSc4d1	snadné
vyhledání	vyhledání	k1gNnSc4	vyhledání
lékaře	lékař	k1gMnSc2	lékař
podle	podle	k7c2	podle
lokality	lokalita	k1gFnSc2	lokalita
a	a	k8xC	a
specializace	specializace	k1gFnSc2	specializace
</s>
</p>
<p>
<s>
Institut	institut	k1gInSc1	institut
postgraduálního	postgraduální	k2eAgNnSc2d1	postgraduální
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
pracovníků	pracovník	k1gMnPc2	pracovník
ve	v	k7c6	v
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
–	–	k?	–
mj.	mj.	kA	mj.
i	i	k8xC	i
seznam	seznam	k1gInSc4	seznam
oborů	obor	k1gInPc2	obor
v	v	k7c6	v
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Odborné	odborný	k2eAgFnPc1d1	odborná
lékařské	lékařský	k2eAgFnPc1d1	lékařská
společnosti	společnost	k1gFnPc1	společnost
</s>
</p>
