<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
tradice	tradice	k1gFnSc2	tradice
současného	současný	k2eAgNnSc2d1	současné
lékařství	lékařství	k1gNnSc2	lékařství
byl	být	k5eAaImAgMnS	být
řecký	řecký	k2eAgMnSc1d1	řecký
lékař	lékař	k1gMnSc1	lékař
Hippokratés	Hippokratésa	k1gFnPc2	Hippokratésa
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
