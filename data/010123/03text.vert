<p>
<s>
Bioderma	Bioderma	k1gFnSc1	Bioderma
Laboratories	Laboratoriesa	k1gFnPc2	Laboratoriesa
je	být	k5eAaImIp3nS	být
soukromá	soukromý	k2eAgFnSc1d1	soukromá
francouzská	francouzský	k2eAgFnSc1d1	francouzská
farmaceutická	farmaceutický	k2eAgFnSc1d1	farmaceutická
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
léčbu	léčba	k1gFnSc4	léčba
dermatologických	dermatologický	k2eAgInPc2d1	dermatologický
problémů	problém	k1gInPc2	problém
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
na	na	k7c4	na
pediatrii	pediatrie	k1gFnSc4	pediatrie
<g/>
,	,	kIx,	,
regeneraci	regenerace	k1gFnSc4	regenerace
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
vlasy	vlas	k1gInPc4	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
v	v	k7c4	v
Aix-en-Provence	Aixn-Provence	k1gFnPc4	Aix-en-Provence
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
společnost	společnost	k1gFnSc1	společnost
otevřela	otevřít	k5eAaPmAgFnS	otevřít
v	v	k7c6	v
Lyonu	Lyon	k1gInSc6	Lyon
první	první	k4xOgFnSc4	první
specializovanou	specializovaný	k2eAgFnSc4d1	specializovaná
biometrologickou	biometrologický	k2eAgFnSc4d1	biometrologický
laboratoř	laboratoř	k1gFnSc4	laboratoř
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
a	a	k8xC	a
vývoj	vývoj	k1gInSc4	vývoj
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
pleť	pleť	k1gFnSc4	pleť
<g/>
.	.	kIx.	.
</s>
<s>
Primárním	primární	k2eAgInSc7d1	primární
obchodním	obchodní	k2eAgInSc7d1	obchodní
sektorem	sektor	k1gInSc7	sektor
je	být	k5eAaImIp3nS	být
dermatologie	dermatologie	k1gFnSc1	dermatologie
s	s	k7c7	s
produkty	produkt	k1gInPc7	produkt
jako	jako	k9	jako
ABCDerm	ABCDerm	k1gInSc1	ABCDerm
<g/>
,	,	kIx,	,
Atoderm	Atoderm	k1gInSc1	Atoderm
<g/>
,	,	kIx,	,
Cicabio	Cicabio	k1gNnSc1	Cicabio
<g/>
,	,	kIx,	,
Créaline	Créalin	k1gInSc5	Créalin
<g/>
,	,	kIx,	,
Hydrabio	Hydrabia	k1gMnSc5	Hydrabia
<g/>
,	,	kIx,	,
Matriciane	Matrician	k1gMnSc5	Matrician
<g/>
,	,	kIx,	,
Matricium	Matricium	k1gNnSc1	Matricium
<g/>
,	,	kIx,	,
Sébium	Sébium	k1gNnSc1	Sébium
<g/>
,	,	kIx,	,
White	Whit	k1gInSc5	Whit
Objective	Objectiv	k1gInSc5	Objectiv
<g/>
,	,	kIx,	,
Secure	Secur	k1gMnSc5	Secur
(	(	kIx(	(
<g/>
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
pleť	pleť	k1gFnSc4	pleť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nodé	Nodé	k1gNnSc1	Nodé
(	(	kIx(	(
<g/>
péče	péče	k1gFnSc1	péče
o	o	k7c4	o
vlasy	vlas	k1gInPc4	vlas
<g/>
)	)	kIx)	)
a	a	k8xC	a
Photoderm	Photoderm	k1gInSc4	Photoderm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Firma	firma	k1gFnSc1	firma
Bioderma	Bioderma	k1gFnSc1	Bioderma
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Jean-Noël	Jean-Noëla	k1gFnPc2	Jean-Noëla
Thorelem	Thorel	k1gInSc7	Thorel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
byla	být	k5eAaImAgFnS	být
typická	typický	k2eAgFnSc1d1	typická
inovace	inovace	k1gFnSc1	inovace
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgInSc4d1	vlastní
výzkum	výzkum	k1gInSc4	výzkum
a	a	k8xC	a
nový	nový	k2eAgInSc4d1	nový
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
dermatologii	dermatologie	k1gFnSc3	dermatologie
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
inovačních	inovační	k2eAgInPc2d1	inovační
produktů	produkt	k1gInPc2	produkt
byl	být	k5eAaImAgInS	být
šampon	šampon	k1gInSc1	šampon
Nodé	Nodá	k1gFnSc2	Nodá
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přistupoval	přistupovat	k5eAaImAgMnS	přistupovat
k	k	k7c3	k
mytí	mytí	k1gNnSc3	mytí
vlasů	vlas	k1gInPc2	vlas
šetrněji	šetrně	k6eAd2	šetrně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
přišla	přijít	k5eAaPmAgFnS	přijít
společnost	společnost	k1gFnSc1	společnost
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
řadou	řada	k1gFnSc7	řada
produktů	produkt	k1gInPc2	produkt
spojenou	spojený	k2eAgFnSc7d1	spojená
s	s	k7c7	s
lékaři	lékař	k1gMnPc7	lékař
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
začal	začít	k5eAaPmAgInS	začít
laboratorní	laboratorní	k2eAgInSc1d1	laboratorní
vývoj	vývoj	k1gInSc1	vývoj
dalších	další	k2eAgInPc2d1	další
dermatologických	dermatologický	k2eAgInPc2d1	dermatologický
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
první	první	k4xOgFnSc1	první
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
laboratoř	laboratoř	k1gFnSc1	laboratoř
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
začíná	začínat	k5eAaImIp3nS	začínat
Bioderma	Bioderma	k1gFnSc1	Bioderma
brát	brát	k5eAaImF	brát
větší	veliký	k2eAgInSc4d2	veliký
ohled	ohled	k1gInSc4	ohled
na	na	k7c4	na
různorodé	různorodý	k2eAgFnPc4d1	různorodá
potřeby	potřeba	k1gFnPc4	potřeba
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
oblastech	oblast	k1gFnPc6	oblast
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
začíná	začínat	k5eAaImIp3nS	začínat
Bioderma	Bioderma	k1gFnSc1	Bioderma
provádět	provádět	k5eAaImF	provádět
sociální	sociální	k2eAgFnPc4d1	sociální
mise	mise	k1gFnPc4	mise
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
plánovala	plánovat	k5eAaImAgFnS	plánovat
Nadace	nadace	k1gFnSc1	nadace
Bioderma	Bioderma	k1gFnSc1	Bioderma
pomáhát	pomáhát	k5eAaPmF	pomáhát
v	v	k7c6	v
rozvoji	rozvoj	k1gInSc6	rozvoj
dermatologie	dermatologie	k1gFnSc2	dermatologie
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
firma	firma	k1gFnSc1	firma
vykázala	vykázat	k5eAaPmAgFnS	vykázat
obrat	obrat	k1gInSc4	obrat
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
261,5	[number]	k4	261,5
milionů	milion	k4xCgInPc2	milion
eur	euro	k1gNnPc2	euro
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
druhé	druhý	k4xOgFnSc2	druhý
dekády	dekáda	k1gFnSc2	dekáda
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
její	její	k3xOp3gInPc4	její
výrobky	výrobek	k1gInPc4	výrobek
možné	možný	k2eAgNnSc1d1	možné
koupit	koupit	k5eAaPmF	koupit
v	v	k7c6	v
90	[number]	k4	90
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
s	s	k7c7	s
většinou	většina	k1gFnSc7	většina
poboček	pobočka	k1gFnPc2	pobočka
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
kontinentu	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
české	český	k2eAgFnSc2d1	Česká
pobočky	pobočka	k1gFnSc2	pobočka
</s>
</p>
