<s>
Index	index	k1gInSc1
lidského	lidský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
nebo	nebo	k8xC
jeho	jeho	k3xOp3gFnSc1
část	část	k1gFnSc1
potřebuje	potřebovat	k5eAaImIp3nS
přiměřeně	přiměřeně	k6eAd1
doplnit	doplnit	k5eAaPmF
wikiodkazy	wikiodkaz	k1gInPc4
na	na	k7c4
ostatní	ostatní	k2eAgInPc4d1
články	článek	k1gInPc4
<g/>
.	.	kIx.
<g/>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
států	stát	k1gInPc2
světa	svět	k1gInSc2
podle	podle	k7c2
indexu	index	k1gInSc2
lidského	lidský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
World	World	k1gMnSc1
map	mapa	k1gFnPc2
representing	representing	k1gInSc4
the	the	k?
inequality-adjusted	inequality-adjusted	k1gInSc1
Human	Human	k1gInSc1
Development	Development	k1gInSc1
Index	index	k1gInSc1
categories	categories	k1gInSc1
(	(	kIx(
<g/>
based	based	k1gMnSc1
on	on	k3xPp3gMnSc1
2018	#num#	k4
data	datum	k1gNnPc4
<g/>
,	,	kIx,
published	published	k1gInSc1
in	in	k?
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
0.800	0.800	k4
<g/>
–	–	k?
<g/>
1.000	1.000	k4
(	(	kIx(
<g/>
very	vera	k1gFnSc2
high	high	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
0.700	0.700	k4
<g/>
–	–	k?
<g/>
0.799	0.799	k4
(	(	kIx(
<g/>
high	high	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
0.550	0.550	k4
<g/>
–	–	k?
<g/>
0.699	0.699	k4
(	(	kIx(
<g/>
medium	medium	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
0.350	0.350	k4
<g/>
–	–	k?
<g/>
0.549	0.549	k4
(	(	kIx(
<g/>
low	low	k?
<g/>
)	)	kIx)
</s>
<s>
Data	datum	k1gNnPc1
unavailable	unavailable	k6eAd1
</s>
<s>
Mapa	mapa	k1gFnSc1
světa	svět	k1gInSc2
s	s	k7c7
rozdělením	rozdělení	k1gNnSc7
HDI	HDI	kA
v	v	k7c6
členských	členský	k2eAgInPc6d1
státech	stát	k1gInPc6
OSN	OSN	kA
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
0,900	0,900	k4
a	a	k8xC
více	hodně	k6eAd2
</s>
<s>
0,850	0,850	k4
<g/>
–	–	k?
<g/>
0,899	0,899	k4
</s>
<s>
0,800	0,800	k4
<g/>
–	–	k?
<g/>
0,849	0,849	k4
</s>
<s>
0,750	0,750	k4
<g/>
–	–	k?
<g/>
0,799	0,799	k4
</s>
<s>
0,700	0,700	k4
<g/>
–	–	k?
<g/>
0,749	0,749	k4
</s>
<s>
0,650	0,650	k4
<g/>
–	–	k?
<g/>
0,699	0,699	k4
</s>
<s>
0,600	0,600	k4
<g/>
–	–	k?
<g/>
0,649	0,649	k4
</s>
<s>
0,550	0,550	k4
<g/>
–	–	k?
<g/>
0,599	0,599	k4
</s>
<s>
0,500	0,500	k4
<g/>
–	–	k?
<g/>
0,549	0,549	k4
</s>
<s>
0,450	0,450	k4
<g/>
–	–	k?
<g/>
0,499	0,499	k4
</s>
<s>
0,400	0,400	k4
<g/>
–	–	k?
<g/>
0,449	0,449	k4
</s>
<s>
0,350	0,350	k4
<g/>
–	–	k?
<g/>
0,399	0,399	k4
</s>
<s>
0,300	0,300	k4
<g/>
–	–	k?
<g/>
0,349	0,349	k4
</s>
<s>
méně	málo	k6eAd2
než	než	k8xS
0.300	0.300	k4
</s>
<s>
Data	datum	k1gNnPc1
nejsou	být	k5eNaImIp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
</s>
<s>
Mapa	mapa	k1gFnSc1
světa	svět	k1gInSc2
s	s	k7c7
rozdělením	rozdělení	k1gNnSc7
HDI	HDI	kA
v	v	k7c6
členských	členský	k2eAgInPc6d1
státech	stát	k1gInPc6
OSN	OSN	kA
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
0,900	0,900	k4
a	a	k8xC
více	hodně	k6eAd2
</s>
<s>
0,850	0,850	k4
<g/>
–	–	k?
<g/>
0,899	0,899	k4
</s>
<s>
0,800	0,800	k4
<g/>
–	–	k?
<g/>
0,849	0,849	k4
</s>
<s>
0,750	0,750	k4
<g/>
–	–	k?
<g/>
0,799	0,799	k4
</s>
<s>
0,700	0,700	k4
<g/>
–	–	k?
<g/>
0,749	0,749	k4
</s>
<s>
0,650	0,650	k4
<g/>
–	–	k?
<g/>
0,699	0,699	k4
</s>
<s>
0,600	0,600	k4
<g/>
–	–	k?
<g/>
0,649	0,649	k4
</s>
<s>
0,550	0,550	k4
<g/>
–	–	k?
<g/>
0,599	0,599	k4
</s>
<s>
0,500	0,500	k4
<g/>
–	–	k?
<g/>
0,549	0,549	k4
</s>
<s>
0,450	0,450	k4
<g/>
–	–	k?
<g/>
0,499	0,499	k4
</s>
<s>
0,400	0,400	k4
<g/>
–	–	k?
<g/>
0,449	0,449	k4
</s>
<s>
0,350	0,350	k4
<g/>
–	–	k?
<g/>
0,399	0,399	k4
</s>
<s>
0,300	0,300	k4
<g/>
–	–	k?
<g/>
0,349	0,349	k4
</s>
<s>
méně	málo	k6eAd2
než	než	k8xS
0,300	0,300	k4
</s>
<s>
Data	datum	k1gNnPc1
nejsou	být	k5eNaImIp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
</s>
<s>
Mapa	mapa	k1gFnSc1
světa	svět	k1gInSc2
s	s	k7c7
rozdělením	rozdělení	k1gNnSc7
HDI	HDI	kA
v	v	k7c6
členských	členský	k2eAgInPc6d1
státech	stát	k1gInPc6
OSN	OSN	kA
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
0,950	0,950	k4
a	a	k8xC
více	hodně	k6eAd2
</s>
<s>
0,900	0,900	k4
<g/>
-	-	kIx~
<g/>
0,949	0,949	k4
</s>
<s>
0,850	0,850	k4
<g/>
-	-	kIx~
<g/>
0,899	0,899	k4
</s>
<s>
0,800	0,800	k4
<g/>
-	-	kIx~
<g/>
0,849	0,849	k4
</s>
<s>
0,750	0,750	k4
<g/>
-	-	kIx~
<g/>
0,799	0,799	k4
</s>
<s>
0,700	0,700	k4
<g/>
-	-	kIx~
<g/>
0,749	0,749	k4
</s>
<s>
0,650	0,650	k4
<g/>
-	-	kIx~
<g/>
0,699	0,699	k4
</s>
<s>
0,600	0,600	k4
<g/>
-	-	kIx~
<g/>
0,649	0,649	k4
</s>
<s>
0,550	0,550	k4
<g/>
-	-	kIx~
<g/>
0,599	0,599	k4
</s>
<s>
0,500	0,500	k4
<g/>
-	-	kIx~
<g/>
0,549	0,549	k4
</s>
<s>
0,450	0,450	k4
<g/>
-	-	kIx~
<g/>
0,499	0,499	k4
</s>
<s>
0,400	0,400	k4
<g/>
-	-	kIx~
<g/>
0,449	0,449	k4
</s>
<s>
0,350	0,350	k4
<g/>
-	-	kIx~
<g/>
0,399	0,399	k4
</s>
<s>
0,300	0,300	k4
<g/>
-	-	kIx~
<g/>
0,349	0,349	k4
</s>
<s>
méně	málo	k6eAd2
než	než	k8xS
0,300	0,300	k4
</s>
<s>
data	datum	k1gNnPc1
nejsou	být	k5eNaImIp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
</s>
<s>
Vývoj	vývoj	k1gInSc1
HDI	HDI	kA
v	v	k7c6
letech	léto	k1gNnPc6
1975	#num#	k4
až	až	k9
2004	#num#	k4
</s>
<s>
OECD	OECD	kA
</s>
<s>
Střední	střední	k2eAgFnSc1d1
a	a	k8xC
východní	východní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
a	a	k8xC
SNS	SNS	kA
</s>
<s>
Latinská	latinský	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
a	a	k8xC
Karibik	Karibik	k1gInSc1
</s>
<s>
Východní	východní	k2eAgFnSc1d1
Asie	Asie	k1gFnSc1
</s>
<s>
Arabský	arabský	k2eAgInSc1d1
svět	svět	k1gInSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Asie	Asie	k1gFnSc1
</s>
<s>
Subsaharská	subsaharský	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
</s>
<s>
Index	index	k1gInSc1
lidského	lidský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Human	Human	k1gMnSc1
development	development	k1gMnSc1
index	index	k1gInSc4
<g/>
,	,	kIx,
HDI	HDI	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
prostředek	prostředek	k1gInSc4
pro	pro	k7c4
srovnání	srovnání	k1gNnSc4
klíčových	klíčový	k2eAgInPc2d1
rozměrů	rozměr	k1gInPc2
lidského	lidský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
<g/>
,	,	kIx,
mezi	mezi	k7c4
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
patří	patřit	k5eAaImIp3nS
<g/>
:	:	kIx,
dlouhý	dlouhý	k2eAgInSc1d1
a	a	k8xC
zdravý	zdravý	k2eAgInSc1d1
život	život	k1gInSc1
<g/>
,	,	kIx,
přístup	přístup	k1gInSc1
ke	k	k7c3
vzdělání	vzdělání	k1gNnSc3
<g/>
,	,	kIx,
životní	životní	k2eAgInSc1d1
standard	standard	k1gInSc1
a	a	k8xC
celková	celkový	k2eAgFnSc1d1
vyspělost	vyspělost	k1gFnSc1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
o	o	k7c4
ukazatel	ukazatel	k1gInSc4
životní	životní	k2eAgFnSc2d1
úrovně	úroveň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
HDI	HDI	kA
je	být	k5eAaImIp3nS
geometrický	geometrický	k2eAgInSc1d1
průměr	průměr	k1gInSc1
z	z	k7c2
indexů	index	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
vyjadřují	vyjadřovat	k5eAaImIp3nP
každý	každý	k3xTgInSc4
z	z	k7c2
těchto	tento	k3xDgInPc2
tří	tři	k4xCgInPc2
rozměrů	rozměr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Index	index	k1gInSc1
udává	udávat	k5eAaImIp3nS
hodnotu	hodnota	k1gFnSc4
mezi	mezi	k7c7
0	#num#	k4
a	a	k8xC
1	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
základě	základ	k1gInSc6
které	který	k3yRgFnSc3,k3yQgFnSc3,k3yIgFnSc3
se	se	k3xPyFc4
státy	stát	k1gInPc7
zařadí	zařadit	k5eAaPmIp3nS
do	do	k7c2
jednoho	jeden	k4xCgInSc2
ze	z	k7c2
čtyř	čtyři	k4xCgInPc2
stupňů	stupeň	k1gInPc2
rozvoje	rozvoj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Index	index	k1gInSc1
byl	být	k5eAaImAgInS
vytvořen	vytvořit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
pákistánským	pákistánský	k2eAgMnSc7d1
ekonomem	ekonom	k1gMnSc7
Mahbubem	Mahbub	k1gMnSc7
ul	ul	kA
Haqem	Haq	k1gMnSc7
a	a	k8xC
indickým	indický	k2eAgMnSc7d1
ekonomem	ekonom	k1gMnSc7
Amartyem	Amarty	k1gMnSc7
Senem	seno	k1gNnSc7
a	a	k8xC
používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
každoroční	každoroční	k2eAgFnSc6d1
zprávě	zpráva	k1gFnSc6
OSN	OSN	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vznik	vznik	k1gInSc1
a	a	k8xC
význam	význam	k1gInSc1
indexu	index	k1gInSc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
první	první	k4xOgFnSc6
zprávě	zpráva	k1gFnSc6
o	o	k7c6
lidském	lidský	k2eAgInSc6d1
rozvoji	rozvoj	k1gInSc6
programu	program	k1gInSc2
OSN	OSN	kA
pro	pro	k7c4
rozvoj	rozvoj	k1gInSc4
(	(	kIx(
<g/>
UNDP	UNDP	kA
<g/>
)	)	kIx)
objevil	objevit	k5eAaPmAgMnS
ukazatel	ukazatel	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
vypovídat	vypovídat	k5eAaImF,k5eAaPmF
o	o	k7c6
mezistátních	mezistátní	k2eAgInPc6d1
rozdílech	rozdíl	k1gInPc6
v	v	k7c6
kvalitě	kvalita	k1gFnSc6
života	život	k1gInSc2
lépe	dobře	k6eAd2
než	než	k8xS
ukazatele	ukazatel	k1gInPc1
ekonomické	ekonomický	k2eAgFnSc2d1
povahy	povaha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInSc4d3
podíl	podíl	k1gInSc4
na	na	k7c6
vzniku	vznik	k1gInSc6
úvodní	úvodní	k2eAgFnSc2d1
zprávy	zpráva	k1gFnSc2
o	o	k7c6
lidském	lidský	k2eAgInSc6d1
rozvoji	rozvoj	k1gInSc6
měl	mít	k5eAaImAgMnS
pákistánský	pákistánský	k2eAgMnSc1d1
ekonom	ekonom	k1gMnSc1
Mahbub	Mahbub	k1gMnSc1
ul	ul	kA
Haq	Haq	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
vývoji	vývoj	k1gInSc6
spolupracoval	spolupracovat	k5eAaImAgInS
s	s	k7c7
dalšími	další	k2eAgMnPc7d1
světovými	světový	k2eAgMnPc7d1
ekonomy	ekonom	k1gMnPc7
včetně	včetně	k7c2
Paula	Paul	k1gMnSc2
Streetena	Streeten	k2eAgFnSc1d1
<g/>
,	,	kIx,
Francese	Francese	k1gFnSc1
Stewarta	Stewart	k1gMnSc2
<g/>
,	,	kIx,
Gustava	Gustav	k1gMnSc2
Ranise	Ranise	k1gFnSc1
<g/>
,	,	kIx,
Keitha	Keitha	k1gFnSc1
Griffina	Griffina	k1gFnSc1
<g/>
,	,	kIx,
Sudhira	Sudhira	k1gFnSc1
Ananda	Ananda	k1gFnSc1
a	a	k8xC
Meghnada	Meghnada	k1gFnSc1
Desaie	Desaie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
spolupráci	spolupráce	k1gFnSc3
se	se	k3xPyFc4
připojil	připojit	k5eAaPmAgMnS
i	i	k9
nositel	nositel	k1gMnSc1
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
Amartya	Amartya	k1gMnSc1
Sen	sen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sen	sen	k1gInSc1
se	se	k3xPyFc4
obával	obávat	k5eAaImAgInS
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
náročné	náročný	k2eAgNnSc1d1
zachytit	zachytit	k5eAaPmF
celou	celý	k2eAgFnSc4d1
složitost	složitost	k1gFnSc4
lidských	lidský	k2eAgFnPc2d1
schopností	schopnost	k1gFnPc2
v	v	k7c6
jednom	jeden	k4xCgInSc6
indexu	index	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
Haq	Haq	k1gMnSc1
ho	on	k3xPp3gNnSc4
přesvědčil	přesvědčit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Těžiště	těžiště	k1gNnSc1
zpráv	zpráva	k1gFnPc2
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
hodnocení	hodnocení	k1gNnSc6
kvality	kvalita	k1gFnSc2
života	život	k1gInSc2
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
světa	svět	k1gInSc2
podle	podle	k7c2
žebříčku	žebříček	k1gInSc2
sestaveného	sestavený	k2eAgInSc2d1
na	na	k7c6
základě	základ	k1gInSc6
výsledků	výsledek	k1gInPc2
HDI	HDI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
zprávy	zpráva	k1gFnPc1
upozorňují	upozorňovat	k5eAaImIp3nP
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgInPc6,k3yRgInPc6,k3yQgInPc6
regionech	region	k1gInPc6
světa	svět	k1gInSc2
lidský	lidský	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
stagnuje	stagnovat	k5eAaImIp3nS
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yQgInPc6,k3yIgInPc6,k3yRgInPc6
se	se	k3xPyFc4
naopak	naopak	k6eAd1
zrychluje	zrychlovat	k5eAaImIp3nS
či	či	k8xC
zda	zda	k8xS
dochází	docházet	k5eAaImIp3nS
ke	k	k7c3
splývání	splývání	k1gNnSc3
rozdílů	rozdíl	k1gInPc2
mezi	mezi	k7c7
rozvinutými	rozvinutý	k2eAgFnPc7d1
a	a	k8xC
rozvíjejícími	rozvíjející	k2eAgFnPc7d1
se	se	k3xPyFc4
státy	stát	k1gInPc1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výpočet	výpočet	k1gInSc1
HDI	HDI	kA
</s>
<s>
Do	do	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
se	se	k3xPyFc4
používal	používat	k5eAaImAgInS
vzorec	vzorec	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
k	k	k7c3
výpočtu	výpočet	k1gInSc3
využíval	využívat	k5eAaPmAgMnS,k5eAaImAgMnS
tyto	tento	k3xDgInPc4
rozměry	rozměr	k1gInPc4
<g/>
:	:	kIx,
střední	střední	k2eAgFnSc1d1
délka	délka	k1gFnSc1
života	život	k1gInSc2
<g/>
,	,	kIx,
gramotnost	gramotnost	k1gFnSc1
dospělé	dospělý	k2eAgFnSc2d1
populace	populace	k1gFnSc2
<g/>
,	,	kIx,
poměr	poměr	k1gInSc1
hrubého	hrubý	k2eAgInSc2d1
zápisu	zápis	k1gInSc2
do	do	k7c2
škol	škola	k1gFnPc2
a	a	k8xC
hrubý	hrubý	k2eAgInSc4d1
domácí	domácí	k2eAgInSc4d1
produkt	produkt	k1gInSc4
na	na	k7c4
hlavu	hlava	k1gFnSc4
(	(	kIx(
<g/>
v	v	k7c6
dolarech	dolar	k1gInPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Nová	nový	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
výpočtu	výpočet	k1gInSc2
od	od	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
zahrnuje	zahrnovat	k5eAaImIp3nS
dva	dva	k4xCgInPc4
kroky	krok	k1gInPc4
<g/>
:	:	kIx,
Vytvoření	vytvoření	k1gNnSc1
rozměrových	rozměrový	k2eAgInPc2d1
indexů	index	k1gInPc2
a	a	k8xC
Agregace	agregace	k1gFnSc2
rozměrových	rozměrový	k2eAgInPc2d1
indexů	index	k1gInPc2
pro	pro	k7c4
výpočet	výpočet	k1gInSc4
HDI	HDI	kA
<g/>
.	.	kIx.
</s>
<s>
Vytvoření	vytvoření	k1gNnSc1
rozměrových	rozměrový	k2eAgInPc2d1
indexů	index	k1gInPc2
</s>
<s>
Byly	být	k5eAaImAgFnP
vytvořeny	vytvořit	k5eAaPmNgInP
indexy	index	k1gInPc1
zobrazující	zobrazující	k2eAgFnSc2d1
základní	základní	k2eAgFnSc2d1
dimenze	dimenze	k1gFnSc2
lidského	lidský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
(	(	kIx(
<g/>
zdraví	zdraví	k1gNnSc1
<g/>
,	,	kIx,
vzdělání	vzdělání	k1gNnSc1
a	a	k8xC
životní	životní	k2eAgInSc1d1
standard	standard	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
dimenze	dimenze	k1gFnPc4
<g/>
,	,	kIx,
byla	být	k5eAaImAgNnP
určena	určen	k2eAgNnPc1d1
minima	minimum	k1gNnPc1
a	a	k8xC
maxima	maxima	k1gFnSc1
těchto	tento	k3xDgInPc2
indexů	index	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Index	index	k1gInSc1
očekávané	očekávaný	k2eAgFnSc2d1
délky	délka	k1gFnSc2
života	život	k1gInSc2
(	(	kIx(
<g/>
Life	Life	k1gFnSc1
Expectancy	Expectanca	k1gFnSc2
Index	index	k1gInSc1
<g/>
,	,	kIx,
LEI	lei	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Index	index	k1gInSc1
vzdělání	vzdělání	k1gNnSc2
(	(	kIx(
<g/>
Educational	Educational	k1gFnSc1
Index	index	k1gInSc1
<g/>
,	,	kIx,
EI	EI	kA
<g/>
)	)	kIx)
</s>
<s>
Index	index	k1gInSc1
střední	střední	k2eAgFnSc2d1
délky	délka	k1gFnSc2
vzdělání	vzdělání	k1gNnSc1
(	(	kIx(
<g/>
Mean	Mean	k1gInSc1
Years	Years	k1gInSc1
of	of	k?
Schooling	Schooling	k1gInSc1
Index	index	k1gInSc1
<g/>
,	,	kIx,
MYSI	MYSI	kA
<g/>
)	)	kIx)
</s>
<s>
Index	index	k1gInSc1
očekávané	očekávaný	k2eAgFnSc2d1
délky	délka	k1gFnSc2
vzdělání	vzdělání	k1gNnSc1
(	(	kIx(
<g/>
Expeced	Expeced	k1gInSc1
Years	Years	k1gInSc1
of	of	k?
Schooling	Schooling	k1gInSc1
Index	index	k1gInSc1
<g/>
,	,	kIx,
EYSI	EYSI	kA
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Index	index	k1gInSc1
hrubého	hrubý	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
příjmu	příjem	k1gInSc2
(	(	kIx(
<g/>
Gross	Gross	k1gMnSc1
national	nationat	k5eAaPmAgMnS,k5eAaImAgMnS
income	incom	k1gInSc5
<g/>
,	,	kIx,
GNI	GNI	kA
<g/>
)	)	kIx)
</s>
<s>
Po	po	k7c6
určení	určení	k1gNnSc6
minima	minimum	k1gNnSc2
a	a	k8xC
maxima	maximum	k1gNnSc2
jsou	být	k5eAaImIp3nP
indexy	index	k1gInPc1
dimenzí	dimenze	k1gFnPc2
vypočítány	vypočítat	k5eAaPmNgInP
následovně	následovně	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
x	x	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
x	x	k?
<g/>
}	}	kIx)
</s>
<s>
-index	-index	k1gInSc1
=	=	kIx~
</s>
<s>
x	x	k?
</s>
<s>
−	−	k?
</s>
<s>
m	m	kA
</s>
<s>
i	i	k9
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
)	)	kIx)
</s>
<s>
m	m	kA
</s>
<s>
a	a	k8xC
</s>
<s>
x	x	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
)	)	kIx)
</s>
<s>
−	−	k?
</s>
<s>
m	m	kA
</s>
<s>
i	i	k9
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
x-min	x-min	k1gInSc1
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
)	)	kIx)
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
max	max	kA
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
)	)	kIx)
<g/>
-min	-min	k1gMnSc1
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
)	)	kIx)
<g/>
}}}	}}}	k?
</s>
<s>
kde	kde	k6eAd1
</s>
<s>
m	m	kA
</s>
<s>
i	i	k9
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
min	mina	k1gFnPc2
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
</s>
<s>
m	m	kA
</s>
<s>
a	a	k8xC
</s>
<s>
x	x	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
max	max	kA
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
jsou	být	k5eAaImIp3nP
nejnižší	nízký	k2eAgFnPc1d3
a	a	k8xC
nejvyšší	vysoký	k2eAgFnPc1d3
hodnoty	hodnota	k1gFnPc1
<g/>
,	,	kIx,
kterých	který	k3yRgFnPc2,k3yQgFnPc2,k3yIgFnPc2
může	moct	k5eAaImIp3nS
proměnná	proměnná	k1gFnSc1
</s>
<s>
x	x	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
x	x	k?
<g/>
}	}	kIx)
</s>
<s>
nabýt	nabýt	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Rozměr	rozměr	k1gInSc1
</s>
<s>
Indikátor	indikátor	k1gInSc1
</s>
<s>
Minimum	minimum	k1gNnSc1
</s>
<s>
Maximum	maximum	k1gNnSc1
</s>
<s>
Zdraví	zdraví	k1gNnSc1
</s>
<s>
délka	délka	k1gFnSc1
života	život	k1gInSc2
(	(	kIx(
<g/>
roky	rok	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
20	#num#	k4
</s>
<s>
85	#num#	k4
</s>
<s>
Vzdělání	vzdělání	k1gNnSc1
</s>
<s>
předpokládaná	předpokládaný	k2eAgFnSc1d1
délka	délka	k1gFnSc1
vzdělávání	vzdělávání	k1gNnSc4
(	(	kIx(
<g/>
roky	rok	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
0	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
průměrná	průměrný	k2eAgFnSc1d1
délka	délka	k1gFnSc1
vzdělávání	vzdělávání	k1gNnSc4
(	(	kIx(
<g/>
roky	rok	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
0	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
Životní	životní	k2eAgInSc1d1
standard	standard	k1gInSc1
</s>
<s>
hrubý	hrubý	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
příjem	příjem	k1gInSc1
(	(	kIx(
<g/>
HNP	HNP	kA
<g/>
)	)	kIx)
na	na	k7c4
hlavu	hlava	k1gFnSc4
(	(	kIx(
<g/>
v	v	k7c6
dolarech	dolar	k1gInPc6
<g/>
)	)	kIx)
</s>
<s>
100	#num#	k4
</s>
<s>
75000	#num#	k4
</s>
<s>
Vytvoření	vytvoření	k1gNnSc1
Indexu	index	k1gInSc2
lidského	lidský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
</s>
<s>
HDI	HDI	kA
je	být	k5eAaImIp3nS
geometrický	geometrický	k2eAgInSc1d1
průměr	průměr	k1gInSc1
tří	tři	k4xCgInPc2
indexů	index	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
HDI	HDI	kA
</s>
<s>
=	=	kIx~
</s>
<s>
(	(	kIx(
</s>
<s>
LEI	lei	k1gInSc1
</s>
<s>
×	×	k?
</s>
<s>
EI	EI	kA
</s>
<s>
×	×	k?
</s>
<s>
II	II	kA
</s>
<s>
)	)	kIx)
</s>
<s>
3	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
textrm	textrm	k1gInSc1
{	{	kIx(
<g/>
HDI	HDI	kA
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k5eAaPmF
<g/>
[	[	kIx(
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
]	]	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
textrm	textrm	k1gInSc1
{	{	kIx(
<g/>
LEI	lei	k1gInSc1
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
times	times	k1gInSc1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
textrm	textrm	k1gInSc1
{	{	kIx(
<g/>
EI	EI	kA
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
times	times	k1gInSc1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
textrm	textrm	k1gInSc1
{	{	kIx(
<g/>
II	II	kA
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
}}}	}}}	k?
</s>
<s>
LEI	lei	k1gInSc1
<g/>
:	:	kIx,
Index	index	k1gInSc1
očekávané	očekávaný	k2eAgFnSc2d1
délky	délka	k1gFnSc2
života	život	k1gInSc2
(	(	kIx(
<g/>
Life	Life	k1gFnSc1
Expectancy	Expectanca	k1gFnSc2
Index	index	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
EI	EI	kA
<g/>
:	:	kIx,
Index	index	k1gInSc1
vzdělání	vzdělání	k1gNnSc1
(	(	kIx(
<g/>
Education	Education	k1gInSc1
Index	index	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
II	II	kA
<g/>
:	:	kIx,
Index	index	k1gInSc1
HNP	HNP	kA
(	(	kIx(
<g/>
Income	Incom	k1gInSc5
Index	index	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Řazení	řazení	k1gNnSc1
států	stát	k1gInPc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
byl	být	k5eAaImAgInS
představen	představen	k2eAgInSc1d1
systém	systém	k1gInSc1
mezních	mezní	k2eAgInPc2d1
bodů	bod	k1gInPc2
(	(	kIx(
<g/>
COP	cop	k1gInSc1
<g/>
)	)	kIx)
pro	pro	k7c4
čtyři	čtyři	k4xCgInPc4
indikátory	indikátor	k1gInPc4
HDI	HDI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Body	bod	k1gInPc7
jsou	být	k5eAaImIp3nP
získány	získán	k2eAgInPc1d1
jako	jako	k8xC,k8xS
vypočtené	vypočtený	k2eAgInPc1d1
HDI	HDI	kA
hodnoty	hodnota	k1gFnPc4
s	s	k7c7
použitím	použití	k1gNnSc7
kvartilů	kvartil	k1gInPc2
distribuce	distribuce	k1gFnSc2
jednotlivých	jednotlivý	k2eAgInPc2d1
komponentů	komponent	k1gInPc2
indikátorů	indikátor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
body	bod	k1gInPc1
se	se	k3xPyFc4
vypočítávají	vypočítávat	k5eAaImIp3nP
následovně	následovně	k6eAd1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
LE	LE	kA
znamená	znamenat	k5eAaImIp3nS
index	index	k1gInSc1
očekávané	očekávaný	k2eAgFnSc2d1
délky	délka	k1gFnSc2
života	život	k1gInSc2
(	(	kIx(
<g/>
Life	Life	k1gFnSc1
Expectancy	Expectanca	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
MYS	mys	k1gInSc1
je	být	k5eAaImIp3nS
index	index	k1gInSc4
střední	střední	k2eAgFnSc2d1
délky	délka	k1gFnSc2
vzdělávání	vzdělávání	k1gNnSc2
(	(	kIx(
<g/>
Mean	Mean	k1gInSc1
Years	Years	k1gInSc1
of	of	k?
Schooling	Schooling	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
EYS	EYS	kA
znamená	znamenat	k5eAaImIp3nS
index	index	k1gInSc1
očekávané	očekávaný	k2eAgFnSc2d1
doby	doba	k1gFnSc2
vzdělávání	vzdělávání	k1gNnSc2
(	(	kIx(
<g/>
Expected	Expected	k1gInSc1
Years	Years	k1gInSc1
of	of	k?
Schooling	Schooling	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
GNI	GNI	kA
je	být	k5eAaImIp3nS
hrubý	hrubý	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
příjem	příjem	k1gInSc1
na	na	k7c4
jednoho	jeden	k4xCgMnSc4
obyvatele	obyvatel	k1gMnSc4
přepočítaný	přepočítaný	k2eAgInSc4d1
na	na	k7c4
americké	americký	k2eAgInPc4d1
dolary	dolar	k1gInPc4
(	(	kIx(
<g/>
Gross	Gross	k1gMnSc1
national	nationat	k5eAaImAgMnS,k5eAaPmAgMnS
income	incom	k1gInSc5
at	at	k?
purchasing	purchasing	k1gInSc1
power	powero	k1gNnPc2
parity	parita	k1gFnSc2
per	pero	k1gNnPc2
capita	capita	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
C	C	kA
</s>
<s>
O	o	k7c6
</s>
<s>
P	P	kA
</s>
<s>
q	q	k?
</s>
<s>
=	=	kIx~
</s>
<s>
H	H	kA
</s>
<s>
D	D	kA
</s>
<s>
I	i	k9
</s>
<s>
(	(	kIx(
</s>
<s>
L	L	kA
</s>
<s>
E	E	kA
</s>
<s>
q	q	k?
</s>
<s>
,	,	kIx,
</s>
<s>
M	M	kA
</s>
<s>
Y	Y	kA
</s>
<s>
S	s	k7c7
</s>
<s>
q	q	k?
</s>
<s>
,	,	kIx,
</s>
<s>
E	E	kA
</s>
<s>
Y	Y	kA
</s>
<s>
S	s	k7c7
</s>
<s>
q	q	k?
</s>
<s>
,	,	kIx,
</s>
<s>
G	G	kA
</s>
<s>
N	N	kA
</s>
<s>
I	i	k9
</s>
<s>
p	p	k?
</s>
<s>
c	c	k0
</s>
<s>
q	q	k?
</s>
<s>
)	)	kIx)
</s>
<s>
,	,	kIx,
</s>
<s>
q	q	k?
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
2	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
3	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
COP_	COP_	k1gFnSc6
<g/>
{	{	kIx(
<g/>
q	q	k?
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
HDI	HDI	kA
<g/>
(	(	kIx(
<g/>
LE_	LE_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
q	q	k?
<g/>
}	}	kIx)
<g/>
,	,	kIx,
<g/>
MYS_	MYS_	k1gFnSc1
<g/>
{	{	kIx(
<g/>
q	q	k?
<g/>
}	}	kIx)
<g/>
,	,	kIx,
<g/>
EYS_	EYS_	k1gFnSc1
<g/>
{	{	kIx(
<g/>
q	q	k?
<g/>
}	}	kIx)
<g/>
,	,	kIx,
<g/>
GNIpc_	GNIpc_	k1gFnSc1
<g/>
{	{	kIx(
<g/>
q	q	k?
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
q	q	k?
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
1,2	1,2	k4
<g/>
,3	,3	k4
<g/>
}	}	kIx)
</s>
<s>
Výsledné	výsledný	k2eAgInPc1d1
mezní	mezní	k2eAgInPc1d1
body	bod	k1gInPc1
pro	pro	k7c4
zařazení	zařazení	k1gNnSc4
států	stát	k1gInPc2
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
Velmi	velmi	k6eAd1
vysoký	vysoký	k2eAgInSc1d1
lidský	lidský	k2eAgInSc1d1
rozvoj	rozvoj	k1gInSc1
(	(	kIx(
<g/>
COP	cop	k1gInSc1
<g/>
3	#num#	k4
<g/>
)	)	kIx)
0.800	0.800	k4
</s>
<s>
Vysoký	vysoký	k2eAgInSc1d1
lidský	lidský	k2eAgInSc1d1
rozvoj	rozvoj	k1gInSc1
(	(	kIx(
<g/>
COP	cop	k1gInSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
0.700	0.700	k4
</s>
<s>
Střední	střední	k2eAgInSc1d1
lidský	lidský	k2eAgInSc1d1
rozvoj	rozvoj	k1gInSc1
(	(	kIx(
<g/>
COP	cop	k1gInSc1
<g/>
1	#num#	k4
<g/>
)	)	kIx)
0.550	0.550	k4
</s>
<s>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Umístění	umístění	k1gNnSc1
zemí	zem	k1gFnPc2
</s>
<s>
Pořadí	pořadí	k1gNnSc1
podle	podle	k7c2
zprávy	zpráva	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
2019	#num#	k4
</s>
<s>
Pořadí	pořadí	k1gNnSc1
podle	podle	k7c2
zprávy	zpráva	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
2019	#num#	k4
<g/>
,	,	kIx,
publikované	publikovaný	k2eAgFnSc2d1
9	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2019	#num#	k4
a	a	k8xC
založené	založený	k2eAgInPc4d1
na	na	k7c6
datech	datum	k1gNnPc6
z	z	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
▲	▲	k?
=	=	kIx~
lepší	dobrý	k2eAgNnSc1d2
umístění	umístění	k1gNnSc1
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
předchozím	předchozí	k2eAgInSc7d1
rokem	rok	k1gInSc7
</s>
<s>
▬	▬	k?
=	=	kIx~
stejné	stejný	k2eAgNnSc4d1
umístění	umístění	k1gNnSc4
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
předchozím	předchozí	k2eAgInSc7d1
rokem	rok	k1gInSc7
</s>
<s>
▼	▼	k?
=	=	kIx~
pokles	pokles	k1gInSc1
umístění	umístění	k1gNnSc2
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
předchozím	předchozí	k2eAgInSc7d1
rokem	rok	k1gInSc7
</s>
<s>
Pořadí	pořadí	k1gNnSc1
</s>
<s>
Země	země	k1gFnSc1
<g/>
,	,	kIx,
území	území	k1gNnSc1
</s>
<s>
HDI	HDI	kA
</s>
<s>
Zpráva	zpráva	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
2019	#num#	k4
(	(	kIx(
<g/>
data	datum	k1gNnPc4
za	za	k7c4
rok	rok	k1gInSc4
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Změna	změna	k1gFnSc1
oproti	oproti	k7c3
předchozímu	předchozí	k2eAgInSc3d1
roku	rok	k1gInSc3
</s>
<s>
Zpráva	zpráva	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
2019	#num#	k4
(	(	kIx(
<g/>
data	datum	k1gNnPc4
za	za	k7c4
rok	rok	k1gInSc4
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1
roční	roční	k2eAgInSc1d1
přírůstek	přírůstek	k1gInSc1
HDI	HDI	kA
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1.000	1.000	k4
Velmi	velmi	k6eAd1
vysoká	vysoký	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
lidského	lidský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
</s>
<s>
0.954	0.954	k4
10.000	10.000	k4
▬	▬	k?
<g/>
Norsko	Norsko	k1gNnSc4
Norsko	Norsko	k1gNnSc1
<g/>
0.9540	0.9540	k4
<g/>
.0016	.0016	k4
▲	▲	k?
0.16	0.16	k4
<g/>
%	%	kIx~
</s>
<s>
0.946	0.946	k4
20.000	20.000	k4
▬	▬	k?
<g/>
Švýcarsko	Švýcarsko	k1gNnSc4
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
0.9460	0.9460	k4
<g/>
.0018	.0018	k4
▲	▲	k?
0.18	0.18	k4
<g/>
%	%	kIx~
</s>
<s>
0.942	0.942	k4
30.000	30.000	k4
▬	▬	k?
<g/>
Irsko	Irsko	k1gNnSc4
Irsko	Irsko	k1gNnSc1
<g/>
0.9420	0.9420	k4
<g/>
.0071	.0071	k4
▲	▲	k?
0.71	0.71	k4
<g/>
%	%	kIx~
</s>
<s>
0.939	0.939	k4
40.000	40.000	k4
▬	▬	k?
<g/>
Německo	Německo	k1gNnSc4
Německo	Německo	k1gNnSc1
<g/>
0.9390	0.9390	k4
<g/>
.0025	.0025	k4
▲	▲	k?
0.25	0.25	k4
<g/>
%	%	kIx~
</s>
<s>
0.939	0.939	k4
40.002	40.002	k4
▲	▲	k?
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
Hongkong	Hongkong	k1gInSc1
Hongkong	Hongkong	k1gInSc1
<g/>
0.9390	0.9390	k4
<g/>
.0051	.0051	k4
▲	▲	k?
0.51	0.51	k4
<g/>
%	%	kIx~
</s>
<s>
0.938	0.938	k4
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0.001	0.001	k4
▼	▼	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Austrálie	Austrálie	k1gFnSc2
Austrálie	Austrálie	k1gFnSc2
<g/>
0.9380	0.9380	k4
<g/>
.0017	.0017	k4
▲	▲	k?
0.17	0.17	k4
<g/>
%	%	kIx~
</s>
<s>
0.938	0.938	k4
60.001	60.001	k4
▲	▲	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Island	Island	k1gInSc1
Island	Island	k1gInSc1
<g/>
0.9380	0.9380	k4
<g/>
.0064	.0064	k4
▲	▲	k?
0.64	0.64	k4
<g/>
%	%	kIx~
</s>
<s>
0.937	0.937	k4
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0.001	0.001	k4
▼	▼	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Švédsko	Švédsko	k1gNnSc4
Švédsko	Švédsko	k1gNnSc1
<g/>
0.9370	0.9370	k4
<g/>
.0042	.0042	k4
▲	▲	k?
0.42	0.42	k4
<g/>
%	%	kIx~
</s>
<s>
0.935	0.935	k4
90.000	90.000	k4
▬	▬	k?
<g/>
Singapur	Singapur	k1gInSc1
Singapur	Singapur	k1gInSc1
<g/>
0.9350	0.9350	k4
<g/>
.0035	.0035	k4
▲	▲	k?
0.35	0.35	k4
<g/>
%	%	kIx~
</s>
<s>
0.933	0.933	k4
100.000	100.000	k4
▬	▬	k?
<g/>
Nizozemsko	Nizozemsko	k1gNnSc4
Nizozemsko	Nizozemsko	k1gNnSc1
<g/>
0.9330	0.9330	k4
<g/>
.0031	.0031	k4
▲	▲	k?
0.31	0.31	k4
<g/>
%	%	kIx~
</s>
<s>
0.930	0.930	k4
110.000	110.000	k4
▬	▬	k?
<g/>
Dánsko	Dánsko	k1gNnSc4
Dánsko	Dánsko	k1gNnSc1
<g/>
0.9300	0.9300	k4
<g/>
.0027	.0027	k4
▲	▲	k?
0.27	0.27	k4
<g/>
%	%	kIx~
</s>
<s>
0.925	0.925	k4
120.000	120.000	k4
▬	▬	k?
<g/>
Finsko	Finsko	k1gNnSc4
Finsko	Finsko	k1gNnSc1
<g/>
0.9250	0.9250	k4
<g/>
.0030	.0030	k4
▲	▲	k?
0.30	0.30	k4
<g/>
%	%	kIx~
</s>
<s>
0.922	0.922	k4
130.000	130.000	k4
▬	▬	k?
<g/>
Kanada	Kanada	k1gFnSc1
Kanada	Kanada	k1gFnSc1
<g/>
0.9220	0.9220	k4
<g/>
.0038	.0038	k4
▲	▲	k?
0.38	0.38	k4
<g/>
%	%	kIx~
</s>
<s>
0.921	0.921	k4
140.000	140.000	k4
▬	▬	k?
<g/>
Nový	nový	k2eAgInSc4d1
Zéland	Zéland	k1gInSc4
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
<g/>
0.9210	0.9210	k4
<g/>
.0030	.0030	k4
▲	▲	k?
0.30	0.30	k4
<g/>
%	%	kIx~
</s>
<s>
0.920	0.920	k4
150.000	150.000	k4
▬	▬	k?
<g/>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
0.9200	0.9200	k4
<g/>
.0021	.0021	k4
▲	▲	k?
0.21	0.21	k4
<g/>
%	%	kIx~
</s>
<s>
0.920	0.920	k4
150.000	150.000	k4
▬	▬	k?
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
<g/>
0.9200	0.9200	k4
<g/>
.0012	.0012	k4
▲	▲	k?
0.12	0.12	k4
<g/>
%	%	kIx~
</s>
<s>
0.919	0.919	k4
170.000	170.000	k4
▬	▬	k?
<g/>
Belgie	Belgie	k1gFnSc1
Belgie	Belgie	k1gFnSc1
<g/>
0.9190	0.9190	k4
<g/>
.0022	.0022	k4
▲	▲	k?
0.22	0.22	k4
<g/>
%	%	kIx~
</s>
<s>
0.917	0.917	k4
180.000	180.000	k4
▬	▬	k?
<g/>
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc4
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
<g/>
0.9170	0.9170	k4
<g/>
.0017	.0017	k4
▲	▲	k?
0.17	0.17	k4
<g/>
%	%	kIx~
</s>
<s>
0.915	0.915	k4
190.000	190.000	k4
▬	▬	k?
<g/>
Japonsko	Japonsko	k1gNnSc4
Japonsko	Japonsko	k1gNnSc1
<g/>
0.9150	0.9150	k4
<g/>
.0042	.0042	k4
▲	▲	k?
0.42	0.42	k4
<g/>
%	%	kIx~
</s>
<s>
0.914	0.914	k4
200.000	200.000	k4
▬	▬	k?
<g/>
Rakousko	Rakousko	k1gNnSc4
Rakousko	Rakousko	k1gNnSc1
<g/>
0.9140	0.9140	k4
<g/>
.0026	.0026	k4
▲	▲	k?
0.26	0.26	k4
<g/>
%	%	kIx~
</s>
<s>
0.909	0.909	k4
210.000	210.000	k4
▬	▬	k?
<g/>
Lucembursko	Lucembursko	k1gNnSc4
Lucembursko	Lucembursko	k1gNnSc1
<g/>
0.9090	0.9090	k4
<g/>
.0022	.0022	k4
▲	▲	k?
0.22	0.22	k4
<g/>
%	%	kIx~
</s>
<s>
0.906	0.906	k4
220.000	220.000	k4
▬	▬	k?
<g/>
Izrael	Izrael	k1gInSc1
Izrael	Izrael	k1gInSc1
<g/>
0.9060	0.9060	k4
<g/>
.0027	.0027	k4
▲	▲	k?
0.27	0.27	k4
<g/>
%	%	kIx~
</s>
<s>
0.906	0.906	k4
220.000	220.000	k4
▬	▬	k?
<g/>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
<g/>
0.9060	0.9060	k4
<g/>
.0033	.0033	k4
▲	▲	k?
0.33	0.33	k4
<g/>
%	%	kIx~
</s>
<s>
0.902	0.902	k4
240.000	240.000	k4
▬	▬	k?
<g/>
Slovinsko	Slovinsko	k1gNnSc4
Slovinsko	Slovinsko	k1gNnSc1
<g/>
0.9020	0.9020	k4
<g/>
.0029	.0029	k4
▲	▲	k?
0.29	0.29	k4
<g/>
%	%	kIx~
</s>
<s>
0.893	0.893	k4
250.000	250.000	k4
▬	▬	k?
<g/>
Španělsko	Španělsko	k1gNnSc4
Španělsko	Španělsko	k1gNnSc1
<g/>
0.8930	0.8930	k4
<g/>
.0040	.0040	k4
▲	▲	k?
0.40	0.40	k4
<g/>
%	%	kIx~
</s>
<s>
0.891	0.891	k4
260.000	260.000	k4
▬	▬	k?
<g/>
Česko	Česko	k1gNnSc4
Česko	Česko	k1gNnSc1
<g/>
0.8920	0.8920	k4
<g/>
.0041	.0041	k4
▲	▲	k?
0.41	0.41	k4
<g/>
%	%	kIx~
</s>
<s>
0.891	0.891	k4
260.000	260.000	k4
▬	▬	k?
<g/>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc1
<g/>
0.8910	0.8910	k4
<g/>
.0027	.0027	k4
▲	▲	k?
0.27	0.27	k4
<g/>
%	%	kIx~
</s>
<s>
0.885	0.885	k4
280.000	280.000	k4
▬	▬	k?
<g/>
Malta	Malta	k1gFnSc1
Malta	Malta	k1gFnSc1
<g/>
0.8850	0.8850	k4
<g/>
.0055	.0055	k4
▲	▲	k?
0.55	0.55	k4
<g/>
%	%	kIx~
</s>
<s>
0.883	0.883	k4
290.000	290.000	k4
▬	▬	k?
<g/>
Itálie	Itálie	k1gFnSc2
Itálie	Itálie	k1gFnSc2
<g/>
0.8830	0.8830	k4
<g/>
.0017	.0017	k4
▲	▲	k?
0.17	0.17	k4
<g/>
%	%	kIx~
</s>
<s>
0.882	0.882	k4
300.000	300.000	k4
▬	▬	k?
<g/>
Estonsko	Estonsko	k1gNnSc4
Estonsko	Estonsko	k1gNnSc1
<g/>
0.8820	0.8820	k4
<g/>
.0054	.0054	k4
▲	▲	k?
0.54	0.54	k4
<g/>
%	%	kIx~
</s>
<s>
0.873	0.873	k4
310.000	310.000	k4
▬	▬	k?
<g/>
Kypr	Kypr	k1gInSc1
Kypr	Kypr	k1gInSc1
<g/>
0.8730	0.8730	k4
<g/>
.0034	.0034	k4
▲	▲	k?
0.34	0.34	k4
<g/>
%	%	kIx~
</s>
<s>
0.872	0.872	k4
320.000	320.000	k4
▬	▬	k?
<g/>
Řecko	Řecko	k1gNnSc4
Řecko	Řecko	k1gNnSc1
<g/>
0.8720	0.8720	k4
<g/>
.0022	.0022	k4
▲	▲	k?
0.22	0.22	k4
<g/>
%	%	kIx~
</s>
<s>
0.872	0.872	k4
320.001	320.001	k4
▲	▲	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Polsko	Polsko	k1gNnSc4
Polsko	Polsko	k1gNnSc1
<g/>
0.8720	0.8720	k4
<g/>
.0054	.0054	k4
▲	▲	k?
0.54	0.54	k4
<g/>
%	%	kIx~
</s>
<s>
0.869	0.869	k4
340.000	340.000	k4
▬	▬	k?
<g/>
Litva	Litva	k1gFnSc1
Litva	Litva	k1gFnSc1
<g/>
0.8690	0.8690	k4
<g/>
.0067	.0067	k4
▲	▲	k?
0.67	0.67	k4
<g/>
%	%	kIx~
</s>
<s>
0.866	0.866	k4
350.000	350.000	k4
▬	▬	k?
<g/>
Spojené	spojený	k2eAgInPc4d1
arabské	arabský	k2eAgInPc4d1
emiráty	emirát	k1gInPc4
Spojené	spojený	k2eAgInPc4d1
arabské	arabský	k2eAgInPc4d1
emiráty	emirát	k1gInPc4
<g/>
0.8660	0.8660	k4
<g/>
.0068	.0068	k4
▲	▲	k?
0.68	0.68	k4
<g/>
%	%	kIx~
</s>
<s>
0.857	0.857	k4
360.002	360.002	k4
▲	▲	k?
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
Andorra	Andorra	k1gFnSc1
Andorra	Andorra	k1gFnSc1
<g/>
0.8570	0.8570	k4
<g/>
.0043	.0043	k4
▲	▲	k?
0.43	0.43	k4
<g/>
%	%	kIx~
</s>
<s>
0.857	0.857	k4
360.000	360.000	k4
▬	▬	k?
<g/>
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
<g/>
0.8570	0.8570	k4
<g/>
.0071	.0071	k4
▲	▲	k?
0.71	0.71	k4
<g/>
%	%	kIx~
</s>
<s>
0.857	0.857	k4
360.002	360.002	k4
▲	▲	k?
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
Slovensko	Slovensko	k1gNnSc4
Slovensko	Slovensko	k1gNnSc1
<g/>
0.8570	0.8570	k4
<g/>
.0042	.0042	k4
▲	▲	k?
0.42	0.42	k4
<g/>
%	%	kIx~
</s>
<s>
0.854	0.854	k4
390.000	390.000	k4
▬	▬	k?
<g/>
Lotyšsko	Lotyšsko	k1gNnSc4
Lotyšsko	Lotyšsko	k1gNnSc1
<g/>
0.8540	0.8540	k4
<g/>
.0056	.0056	k4
▲	▲	k?
0.56	0.56	k4
<g/>
%	%	kIx~
</s>
<s>
0.850	0.850	k4
400.000	400.000	k4
▬	▬	k?
<g/>
Portugalsko	Portugalsko	k1gNnSc4
Portugalsko	Portugalsko	k1gNnSc1
<g/>
0.8500	0.8500	k4
<g/>
.0042	.0042	k4
▲	▲	k?
0.42	0.42	k4
<g/>
%	%	kIx~
</s>
<s>
0.848	0.848	k4
410.000	410.000	k4
▬	▬	k?
<g/>
Katar	katar	k1gMnSc1
Katar	katar	k1gMnSc1
<g/>
0.8480	0.8480	k4
<g/>
.0022	.0022	k4
▲	▲	k?
0.22	0.22	k4
<g/>
%	%	kIx~
</s>
<s>
0.847	0.847	k4
420.000	420.000	k4
▬	▬	k?
<g/>
Chile	Chile	k1gNnSc1
Chile	Chile	k1gNnSc2
<g/>
0.8470	0.8470	k4
<g/>
.0071	.0071	k4
▲	▲	k?
0.71	0.71	k4
<g/>
%	%	kIx~
</s>
<s>
0.845	0.845	k4
430.000	430.000	k4
▬	▬	k?
<g/>
Brunej	Brunej	k1gFnSc1
Brunej	Brunej	k1gFnSc1
<g/>
0.8450	0.8450	k4
<g/>
.0019	.0019	k4
▲	▲	k?
0.19	0.19	k4
<g/>
%	%	kIx~
</s>
<s>
0.845	0.845	k4
430.000	430.000	k4
▬	▬	k?
<g/>
Maďarsko	Maďarsko	k1gNnSc4
Maďarsko	Maďarsko	k1gNnSc1
<g/>
0.8450	0.8450	k4
<g/>
.0028	.0028	k4
▲	▲	k?
0.28	0.28	k4
<g/>
%	%	kIx~
</s>
<s>
0.838	0.838	k4
450.000	450.000	k4
▬	▬	k?
<g/>
Bahrajn	Bahrajn	k1gMnSc1
Bahrajn	Bahrajn	k1gMnSc1
<g/>
0.8380	0.8380	k4
<g/>
.0064	.0064	k4
▲	▲	k?
0.64	0.64	k4
<g/>
%	%	kIx~
</s>
<s>
0.837	0.837	k4
460.000	460.000	k4
▬	▬	k?
<g/>
Chorvatsko	Chorvatsko	k1gNnSc4
Chorvatsko	Chorvatsko	k1gNnSc1
<g/>
0.8370	0.8370	k4
<g/>
.0041	.0041	k4
▲	▲	k?
0.41	0.41	k4
<g/>
%	%	kIx~
</s>
<s>
0.834	0.834	k4
470.000	470.000	k4
▬	▬	k?
<g/>
Omán	Omán	k1gInSc1
Omán	Omán	k1gInSc1
<g/>
0.8340	0.8340	k4
<g/>
.0063	.0063	k4
▲	▲	k?
0.63	0.63	k4
<g/>
%	%	kIx~
</s>
<s>
0.830	0.830	k4
480.000	480.000	k4
▬	▬	k?
<g/>
Argentina	Argentina	k1gFnSc1
Argentina	Argentina	k1gFnSc1
<g/>
0.8300	0.8300	k4
<g/>
.0018	.0018	k4
▲	▲	k?
0.18	0.18	k4
<g/>
%	%	kIx~
</s>
<s>
0.824	0.824	k4
490.000	490.000	k4
▬	▬	k?
<g/>
Rusko	Rusko	k1gNnSc4
Rusko	Rusko	k1gNnSc1
<g/>
0.8240	0.8240	k4
<g/>
.0069	.0069	k4
▲	▲	k?
0.69	0.69	k4
<g/>
%	%	kIx~
</s>
<s>
0.817	0.817	k4
500.000	500.000	k4
▬	▬	k?
<g/>
Bělorusko	Bělorusko	k1gNnSc4
Bělorusko	Bělorusko	k1gNnSc1
<g/>
0.8170	0.8170	k4
<g/>
.0039	.0039	k4
▲	▲	k?
0.39	0.39	k4
<g/>
%	%	kIx~
</s>
<s>
0.817	0.817	k4
500.001	500.001	k4
▲	▲	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Kazachstán	Kazachstán	k1gInSc1
Kazachstán	Kazachstán	k1gInSc1
<g/>
0.8170	0.8170	k4
<g/>
.0084	.0084	k4
▲	▲	k?
0.84	0.84	k4
<g/>
%	%	kIx~
</s>
<s>
0.816	0.816	k4
52	#num#	k4
<g/>
-	-	kIx~
<g/>
0.001	0.001	k4
▼	▼	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Bulharsko	Bulharsko	k1gNnSc4
Bulharsko	Bulharsko	k1gNnSc1
<g/>
0.8160	0.8160	k4
<g/>
.0058	.0058	k4
▲	▲	k?
0.58	0.58	k4
<g/>
%	%	kIx~
</s>
<s>
0.816	0.816	k4
52	#num#	k4
<g/>
-	-	kIx~
<g/>
0.002	0.002	k4
▼	▼	k?
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
<g/>
0.8160	0.8160	k4
<g/>
.0036	.0036	k4
▲	▲	k?
0.36	0.36	k4
<g/>
%	%	kIx~
</s>
<s>
0.816	0.816	k4
52	#num#	k4
<g/>
-	-	kIx~
<g/>
0.001	0.001	k4
▼	▼	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Rumunsko	Rumunsko	k1gNnSc4
Rumunsko	Rumunsko	k1gNnSc1
<g/>
0.8160	0.8160	k4
<g/>
.0029	.0029	k4
▲	▲	k?
0.29	0.29	k4
<g/>
%	%	kIx~
</s>
<s>
0.814	0.814	k4
550.001	550.001	k4
▲	▲	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Palau	Palaus	k1gInSc2
Palau	Palaus	k1gInSc2
<g/>
0.8140	0.8140	k4
<g/>
.0060	.0060	k4
▲	▲	k?
0.60	0.60	k4
<g/>
%	%	kIx~
</s>
<s>
0.813	0.813	k4
56	#num#	k4
<g/>
-	-	kIx~
<g/>
0.005	0.005	k4
▼	▼	k?
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
Barbados	Barbados	k1gInSc1
Barbados	Barbados	k1gInSc1
<g/>
0.8130	0.8130	k4
<g/>
.0022	.0022	k4
▲	▲	k?
0.22	0.22	k4
<g/>
%	%	kIx~
</s>
<s>
0.808	0.808	k4
570.000	570.000	k4
▬	▬	k?
<g/>
Kuvajt	Kuvajt	k1gInSc1
Kuvajt	Kuvajt	k1gInSc1
<g/>
0.8080	0.8080	k4
<g/>
.0022	.0022	k4
▲	▲	k?
0.22	0.22	k4
<g/>
%	%	kIx~
</s>
<s>
0.808	0.808	k4
570.001	570.001	k4
▲	▲	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Uruguay	Uruguay	k1gFnSc1
Uruguay	Uruguay	k1gFnSc1
<g/>
0.8080	0.8080	k4
<g/>
.0054	.0054	k4
▲	▲	k?
0.54	0.54	k4
<g/>
%	%	kIx~
</s>
<s>
0.806	0.806	k4
590.000	590.000	k4
▬	▬	k?
<g/>
Turecko	Turecko	k1gNnSc4
Turecko	Turecko	k1gNnSc1
<g/>
0.8060	0.8060	k4
<g/>
.0103	.0103	k4
▲	▲	k?
1.03	1.03	k4
<g/>
%	%	kIx~
</s>
<s>
0.805	0.805	k4
600.000	600.000	k4
▬	▬	k?
<g/>
Bahamy	Bahamy	k1gFnPc1
Bahamy	Bahamy	k1gFnPc4
<g/>
0.8050	0.8050	k4
<g/>
.0016	.0016	k4
▲	▲	k?
0.16	0.16	k4
<g/>
%	%	kIx~
</s>
<s>
0.804	0.804	k4
610.000	610.000	k4
▬	▬	k?
<g/>
Malajsie	Malajsie	k1gFnSc1
Malajsie	Malajsie	k1gFnSc1
<g/>
0.8040	0.8040	k4
<g/>
.0049	.0049	k4
▲	▲	k?
0.49	0.49	k4
<g/>
%	%	kIx~
</s>
<s>
0.801	0.801	k4
620.000	620.000	k4
▬	▬	k?
<g/>
Seychely	Seychely	k1gFnPc1
Seychely	Seychely	k1gFnPc4
<g/>
0.8010	0.8010	k4
<g/>
.0063	.0063	k4
▲	▲	k?
0.63	0.63	k4
<g/>
%	%	kIx~
</s>
<s>
0.7995	0.7995	k4
Vysoká	vysoký	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
lidského	lidský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
</s>
<s>
0.799	0.799	k4
630.002	630.002	k4
▲	▲	k?
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
Srbsko	Srbsko	k1gNnSc4
Srbsko	Srbsko	k1gNnSc1
<g/>
0.7990	0.7990	k4
<g/>
.0060	.0060	k4
▲	▲	k?
0.60	0.60	k4
<g/>
%	%	kIx~
</s>
<s>
0.799	0.799	k4
630.000	630.000	k4
▬	▬	k?
<g/>
Trinidad	Trinidad	k1gInSc1
a	a	k8xC
Tobago	Tobago	k6eAd1
Trinidad	Trinidad	k1gInSc1
a	a	k8xC
Tobago	Tobago	k6eAd1
<g/>
0.7990	0.7990	k4
<g/>
.0017	.0017	k4
▲	▲	k?
0.17	0.17	k4
<g/>
%	%	kIx~
</s>
<s>
0.797	0.797	k4
65	#num#	k4
<g/>
-	-	kIx~
<g/>
0.002	0.002	k4
▼	▼	k?
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
Írán	Írán	k1gInSc1
Írán	Írán	k1gInSc1
<g/>
0.7970	0.7970	k4
<g/>
.0068	.0068	k4
▲	▲	k?
0.68	0.68	k4
<g/>
%	%	kIx~
</s>
<s>
0.796	0.796	k4
660.000	660.000	k4
▬	▬	k?
<g/>
Mauricius	Mauricius	k1gMnSc1
Mauricius	Mauricius	k1gMnSc1
<g/>
0.7960	0.7960	k4
<g/>
.0079	.0079	k4
▲	▲	k?
0.79	0.79	k4
<g/>
%	%	kIx~
</s>
<s>
0.795	0.795	k4
670.000	670.000	k4
▬	▬	k?
<g/>
Panama	panama	k2eAgFnSc1d1
Panama	Panama	k1gFnSc1
<g/>
0.7950	0.7950	k4
<g/>
.0060	.0060	k4
▲	▲	k?
0.60	0.60	k4
<g/>
%	%	kIx~
</s>
<s>
0.794	0.794	k4
680.000	680.000	k4
▬	▬	k?
<g/>
Kostarika	Kostarika	k1gFnSc1
Kostarika	Kostarika	k1gFnSc1
<g/>
0.7940	0.7940	k4
<g/>
.0064	.0064	k4
▲	▲	k?
0.64	0.64	k4
<g/>
%	%	kIx~
</s>
<s>
0.791	0.791	k4
690.000	690.000	k4
▬	▬	k?
<g/>
Albánie	Albánie	k1gFnSc1
Albánie	Albánie	k1gFnSc1
<g/>
0.7910	0.7910	k4
<g/>
.0084	.0084	k4
▲	▲	k?
0.84	0.84	k4
<g/>
%	%	kIx~
</s>
<s>
0.786	0.786	k4
700.000	700.000	k4
▬	▬	k?
<g/>
Georgie	Georgie	k1gFnSc1
Georgie	Georgie	k1gFnSc1
<g/>
0.7860	0.7860	k4
<g/>
.0091	.0091	k4
▲	▲	k?
0.91	0.91	k4
<g/>
%	%	kIx~
</s>
<s>
0.780	0.780	k4
710.001	710.001	k4
▲	▲	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Srí	Srí	k1gFnSc3
Lanka	lanko	k1gNnSc2
Srí	Srí	k1gFnSc2
Lanka	lanko	k1gNnSc2
<g/>
0.7800	0.7800	k4
<g/>
.0049	.0049	k4
▲	▲	k?
0.49	0.49	k4
<g/>
%	%	kIx~
</s>
<s>
0.778	0.778	k4
72	#num#	k4
<g/>
-	-	kIx~
<g/>
0.001	0.001	k4
▼	▼	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Kuba	Kuba	k1gFnSc1
Kuba	Kuba	k1gFnSc1
<g/>
0.7780	0.7780	k4
<g/>
.0002	.0002	k4
▲	▲	k?
0.02	0.02	k4
<g/>
%	%	kIx~
</s>
<s>
0.777	0.777	k4
730.001	730.001	k4
▲	▲	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Svatý	svatý	k2eAgMnSc1d1
Kryštof	Kryštof	k1gMnSc1
a	a	k8xC
Nevis	viset	k5eNaImRp2nS
Svatý	svatý	k1gMnSc1
Kryštof	Kryštof	k1gMnSc1
a	a	k8xC
Nevis	viset	k5eNaImRp2nS
<g/>
0.7770	0.7770	k4
<g/>
.0048	.0048	k4
▲	▲	k?
0.48	0.48	k4
<g/>
%	%	kIx~
</s>
<s>
0.776	0.776	k4
74	#num#	k4
<g/>
-	-	kIx~
<g/>
0.001	0.001	k4
▼	▼	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Antigua	Antigua	k1gMnSc1
a	a	k8xC
Barbuda	Barbuda	k1gMnSc1
Antigua	Antigua	k1gMnSc1
a	a	k8xC
Barbuda	Barbuda	k1gMnSc1
<g/>
0.7760	0.7760	k4
<g/>
.0008	.0008	k4
▲	▲	k?
0.08	0.08	k4
<g/>
%	%	kIx~
</s>
<s>
0.769	0.769	k4
750.000	750.000	k4
▬	▬	k?
<g/>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
<g/>
0.7690	0.7690	k4
<g/>
.0093	.0093	k4
▲	▲	k?
0.93	0.93	k4
<g/>
%	%	kIx~
</s>
<s>
0.767	0.767	k4
760.000	760.000	k4
▬	▬	k?
<g/>
Mexiko	Mexiko	k1gNnSc4
Mexiko	Mexiko	k1gNnSc1
<g/>
0.7670	0.7670	k4
<g/>
.0047	.0047	k4
▲	▲	k?
0.47	0.47	k4
<g/>
%	%	kIx~
</s>
<s>
0.765	0.765	k4
770.000	770.000	k4
▬	▬	k?
<g/>
Thajsko	Thajsko	k1gNnSc4
Thajsko	Thajsko	k1gNnSc1
<g/>
0.7650	0.7650	k4
<g/>
.0074	.0074	k4
▲	▲	k?
0.74	0.74	k4
<g/>
%	%	kIx~
</s>
<s>
0.763	0.763	k4
780.000	780.000	k4
▬	▬	k?
<g/>
Grenada	Grenada	k1gFnSc1
Grenada	Grenada	k1gFnSc1
<g/>
0.7630	0.7630	k4
<g/>
.0033	.0033	k4
▲	▲	k?
0.33	0.33	k4
<g/>
%	%	kIx~
</s>
<s>
0.761	0.761	k4
79	#num#	k4
<g/>
-	-	kIx~
<g/>
0.001	0.001	k4
▼	▼	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Brazílie	Brazílie	k1gFnSc1
Brazílie	Brazílie	k1gFnSc2
<g/>
0.7610	0.7610	k4
<g/>
.0059	.0059	k4
▲	▲	k?
0.59	0.59	k4
<g/>
%	%	kIx~
</s>
<s>
0.761	0.761	k4
79	#num#	k4
<g/>
-	-	kIx~
<g/>
0.001	0.001	k4
▼	▼	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Kolumbie	Kolumbie	k1gFnSc1
Kolumbie	Kolumbie	k1gFnSc1
<g/>
0.7610	0.7610	k4
<g/>
.0054	.0054	k4
▲	▲	k?
0.54	0.54	k4
<g/>
%	%	kIx~
</s>
<s>
0.760	0.760	k4
810.000	810.000	k4
▬	▬	k?
<g/>
Arménie	Arménie	k1gFnSc2
Arménie	Arménie	k1gFnSc2
<g/>
0.7600	0.7600	k4
<g/>
.0052	.0052	k4
▲	▲	k?
0.52	0.52	k4
<g/>
%	%	kIx~
</s>
<s>
0.759	0.759	k4
82	#num#	k4
<g/>
-	-	kIx~
<g/>
0.001	0.001	k4
▼	▼	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Alžírsko	Alžírsko	k1gNnSc4
Alžírsko	Alžírsko	k1gNnSc1
<g/>
0.7590	0.7590	k4
<g/>
.0049	.0049	k4
▲	▲	k?
0.49	0.49	k4
<g/>
%	%	kIx~
</s>
<s>
0.759	0.759	k4
82	#num#	k4
<g/>
-	-	kIx~
<g/>
0.001	0.001	k4
▼	▼	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Severní	severní	k2eAgFnSc2d1
Makedonie	Makedonie	k1gFnSc2
Severní	severní	k2eAgFnSc2d1
Makedonie	Makedonie	k1gFnSc2
<g/>
0.7590	0.7590	k4
<g/>
.0041	.0041	k4
▲	▲	k?
0.41	0.41	k4
<g/>
%	%	kIx~
</s>
<s>
0.759	0.759	k4
820.003	820.003	k4
▲	▲	k?
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
Peru	Peru	k1gNnSc6
Peru	prát	k5eAaImIp1nS
<g/>
0.7590	0.7590	k4
<g/>
.0065	.0065	k4
▲	▲	k?
0.65	0.65	k4
<g/>
%	%	kIx~
</s>
<s>
0.758	0.758	k4
850.001	850.001	k4
▲	▲	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Čína	Čína	k1gFnSc1
Čína	Čína	k1gFnSc1
<g/>
0.7580	0.7580	k4
<g/>
.0095	.0095	k4
▲	▲	k?
0.95	0.95	k4
<g/>
%	%	kIx~
</s>
<s>
0.758	0.758	k4
85	#num#	k4
<g/>
-	-	kIx~
<g/>
0.001	0.001	k4
▼	▼	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Ekvádor	Ekvádor	k1gInSc1
Ekvádor	Ekvádor	k1gInSc1
<g/>
0.7580	0.7580	k4
<g/>
.0071	.0071	k4
▲	▲	k?
0.71	0.71	k4
<g/>
%	%	kIx~
</s>
<s>
0.754	0.754	k4
870.000	870.000	k4
▬	▬	k?
<g/>
Ázerbájdžán	Ázerbájdžán	k1gInSc1
Ázerbájdžán	Ázerbájdžán	k1gInSc1
<g/>
0.7540	0.7540	k4
<g/>
.0036	.0036	k4
▲	▲	k?
0.36	0.36	k4
<g/>
%	%	kIx~
</s>
<s>
0.750	0.750	k4
880.000	880.000	k4
▬	▬	k?
<g/>
Ukrajina	Ukrajina	k1gFnSc1
Ukrajina	Ukrajina	k1gFnSc1
<g/>
0.7500	0.7500	k4
<g/>
.0029	.0029	k4
▲	▲	k?
0.29	0.29	k4
<g/>
%	%	kIx~
</s>
<s>
0.745	0.745	k4
890.001	890.001	k4
▲	▲	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Dominikánská	dominikánský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Dominikánská	dominikánský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
0.7450	0.7450	k4
<g/>
.0076	.0076	k4
▲	▲	k?
0.76	0.76	k4
<g/>
%	%	kIx~
</s>
<s>
0.745	0.745	k4
890.000	890.000	k4
▬	▬	k?
<g/>
Svatá	svatý	k2eAgFnSc1d1
Lucie	Lucie	k1gFnSc1
Svatá	svatý	k2eAgFnSc1d1
Lucie	Lucie	k1gFnSc1
<g/>
0.7450	0.7450	k4
<g/>
.0026	.0026	k4
▲	▲	k?
0.26	0.26	k4
<g/>
%	%	kIx~
</s>
<s>
0.739	0.739	k4
910.000	910.000	k4
▬	▬	k?
<g/>
Tunisko	Tunisko	k1gNnSc4
Tunisko	Tunisko	k1gNnSc1
<g/>
0.7390	0.7390	k4
<g/>
.0039	.0039	k4
▲	▲	k?
0.39	0.39	k4
<g/>
%	%	kIx~
</s>
<s>
0.735	0.735	k4
920.002	920.002	k4
▲	▲	k?
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
Mongolsko	Mongolsko	k1gNnSc4
Mongolsko	Mongolsko	k1gNnSc1
<g/>
0.7350	0.7350	k4
<g/>
.0066	.0066	k4
▲	▲	k?
0.66	0.66	k4
<g/>
%	%	kIx~
</s>
<s>
0.731	0.731	k4
—	—	k?
<g/>
0	#num#	k4
—	—	k?
<g/>
Svět	svět	k1gInSc1
<g/>
0.7310	0.7310	k4
<g/>
.60	.60	k4
▲	▲	k?
0.60	0.60	k4
<g/>
%	%	kIx~
</s>
<s>
0.730	0.730	k4
930.000	930.000	k4
▬	▬	k?
<g/>
Libanon	Libanon	k1gInSc1
Libanon	Libanon	k1gInSc1
<g/>
0.730	0.730	k4
<g/>
-	-	kIx~
<g/>
0.0036	0.0036	k4
▼	▼	k?
-0.36	-0.36	k4
<g/>
%	%	kIx~
</s>
<s>
0.728	0.728	k4
940.003	940.003	k4
▲	▲	k?
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
Botswana	Botswana	k1gFnSc1
Botswana	Botswana	k1gFnSc1
<g/>
0.7280	0.7280	k4
<g/>
.0122	.0122	k4
▲	▲	k?
1.22	1.22	k4
<g/>
%	%	kIx~
</s>
<s>
0.728	0.728	k4
94	#num#	k4
<g/>
-	-	kIx~
<g/>
0.001	0.001	k4
▼	▼	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Svatý	svatý	k2eAgMnSc1d1
Vincenc	Vincenc	k1gMnSc1
a	a	k8xC
Grenadiny	grenadina	k1gFnSc2
Svatý	svatý	k1gMnSc1
Vincenc	Vincenc	k1gMnSc1
a	a	k8xC
Grenadiny	grenadina	k1gFnPc1
<g/>
0.7280	0.7280	k4
<g/>
.0029	.0029	k4
▲	▲	k?
0.29	0.29	k4
<g/>
%	%	kIx~
</s>
<s>
0.726	0.726	k4
960.000	960.000	k4
▬	▬	k?
<g/>
Jamajka	Jamajka	k1gFnSc1
Jamajka	Jamajka	k1gFnSc1
<g/>
0.7260	0.7260	k4
<g/>
.0005	.0005	k4
▲	▲	k?
0.05	0.05	k4
<g/>
%	%	kIx~
</s>
<s>
0.726	0.726	k4
96	#num#	k4
<g/>
-	-	kIx~
<g/>
0.004	0.004	k4
▼	▼	k?
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
Venezuela	Venezuela	k1gFnSc1
Venezuela	Venezuela	k1gFnSc1
<g/>
0.726	0.726	k4
<g/>
-	-	kIx~
<g/>
0.0045	0.0045	k4
▼	▼	k?
-0.45	-0.45	k4
<g/>
%	%	kIx~
</s>
<s>
0.724	0.724	k4
980.000	980.000	k4
▬	▬	k?
<g/>
Dominika	Dominik	k1gMnSc4
Dominika	Dominik	k1gMnSc4
<g/>
0.724	0.724	k4
<g/>
-	-	kIx~
<g/>
0.0015	0.0015	k4
▼	▼	k?
-0.15	-0.15	k4
<g/>
%	%	kIx~
</s>
<s>
0.724	0.724	k4
980.004	980.004	k4
▲	▲	k?
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
Fidži	Fidž	k1gFnSc6
Fidži	Fidž	k1gFnSc6
<g/>
0.7240	0.7240	k4
<g/>
.0052	.0052	k4
▲	▲	k?
0.52	0.52	k4
<g/>
%	%	kIx~
</s>
<s>
0.724	0.724	k4
980.001	980.001	k4
▲	▲	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Paraguay	Paraguay	k1gFnSc1
Paraguay	Paraguay	k1gFnSc1
<g/>
0.7240	0.7240	k4
<g/>
.0056	.0056	k4
▲	▲	k?
0.56	0.56	k4
<g/>
%	%	kIx~
</s>
<s>
0.724	0.724	k4
980.001	980.001	k4
▲	▲	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Surinam	Surinam	k1gInSc1
Surinam	Surinam	k1gInSc1
<g/>
0.7240	0.7240	k4
<g/>
.0041	.0041	k4
▲	▲	k?
0.41	0.41	k4
<g/>
%	%	kIx~
</s>
<s>
0.723	0.723	k4
102	#num#	k4
<g/>
-	-	kIx~
<g/>
0.003	0.003	k4
▼	▼	k?
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
Jordánsko	Jordánsko	k1gNnSc4
Jordánsko	Jordánsko	k1gNnSc1
<g/>
0.723	0.723	k4
<g/>
-	-	kIx~
<g/>
0.0007	0.0007	k4
▼	▼	k?
-0.07	-0.07	k4
<g/>
%	%	kIx~
</s>
<s>
0.720	0.720	k4
1030.000	1030.000	k4
▬	▬	k?
<g/>
Belize	Belize	k1gFnSc1
Belize	Belize	k1gFnSc1
<g/>
0.7200	0.7200	k4
<g/>
.0049	.0049	k4
▲	▲	k?
0.49	0.49	k4
<g/>
%	%	kIx~
</s>
<s>
0.719	0.719	k4
1040.001	1040.001	k4
▲	▲	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Maledivy	Maledivy	k1gFnPc1
Maledivy	Maledivy	k1gFnPc4
<g/>
0.7190	0.7190	k4
<g/>
.0090	.0090	k4
▲	▲	k?
0.90	0.90	k4
<g/>
%	%	kIx~
</s>
<s>
0.717	0.717	k4
105	#num#	k4
<g/>
-	-	kIx~
<g/>
0.001	0.001	k4
▼	▼	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Tonga	Tonga	k1gFnSc1
Tonga	Tonga	k1gFnSc1
<g/>
0.7170	0.7170	k4
<g/>
.0045	.0045	k4
▲	▲	k?
0.45	0.45	k4
<g/>
%	%	kIx~
</s>
<s>
0.712	0.712	k4
1060.000	1060.000	k4
▬	▬	k?
<g/>
Filipíny	Filipíny	k1gFnPc1
Filipíny	Filipíny	k1gFnPc4
<g/>
0.7120	0.7120	k4
<g/>
.0073	.0073	k4
▲	▲	k?
0.73	0.73	k4
<g/>
%	%	kIx~
</s>
<s>
0.711	0.711	k4
107	#num#	k4
<g/>
-	-	kIx~
<g/>
0.001	0.001	k4
▼	▼	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Moldavsko	Moldavsko	k1gNnSc4
Moldavsko	Moldavsko	k1gNnSc1
<g/>
0.7110	0.7110	k4
<g/>
.0056	.0056	k4
▲	▲	k?
0.56	0.56	k4
<g/>
%	%	kIx~
</s>
<s>
0.710	0.710	k4
1080.000	1080.000	k4
▬	▬	k?
<g/>
Turkmenistán	Turkmenistán	k1gInSc1
Turkmenistán	Turkmenistán	k1gInSc1
<g/>
0.7100	0.7100	k4
<g/>
.0067	.0067	k4
▲	▲	k?
0.67	0.67	k4
<g/>
%	%	kIx~
</s>
<s>
0.710	0.710	k4
1080.001	1080.001	k4
▲	▲	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Uzbekistán	Uzbekistán	k1gInSc1
Uzbekistán	Uzbekistán	k1gInSc1
<g/>
0.7100	0.7100	k4
<g/>
.0083	.0083	k4
▲	▲	k?
0.83	0.83	k4
<g/>
%	%	kIx~
</s>
<s>
0.708	0.708	k4
1100.001	1100.001	k4
▲	▲	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Libye	Libye	k1gFnSc1
Libye	Libye	k1gFnSc1
<g/>
0.708	0.708	k4
<g/>
-	-	kIx~
<g/>
0.0084	0.0084	k4
▼	▼	k?
-0.84	-0.84	k4
<g/>
%	%	kIx~
</s>
<s>
0.707	0.707	k4
1110.000	1110.000	k4
▬	▬	k?
<g/>
Indonésie	Indonésie	k1gFnSc1
Indonésie	Indonésie	k1gFnSc1
<g/>
0.7070	0.7070	k4
<g/>
.0074	.0074	k4
▲	▲	k?
0.74	0.74	k4
<g/>
%	%	kIx~
</s>
<s>
0.707	0.707	k4
111	#num#	k4
<g/>
-	-	kIx~
<g/>
0.001	0.001	k4
▼	▼	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Samoa	Samoa	k1gFnSc1
Samoa	Samoa	k1gFnSc1
<g/>
0.7070	0.7070	k4
<g/>
.0030	.0030	k4
▲	▲	k?
0.30	0.30	k4
<g/>
%	%	kIx~
</s>
<s>
0.705	0.705	k4
113	#num#	k4
<g/>
-	-	kIx~
<g/>
0.002	0.002	k4
▼	▼	k?
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
0.7050	0.7050	k4
<g/>
.0078	.0078	k4
▲	▲	k?
0.78	0.78	k4
<g/>
%	%	kIx~
</s>
<s>
0.703	0.703	k4
1140.000	1140.000	k4
▬	▬	k?
<g/>
Bolívie	Bolívie	k1gFnSc1
Bolívie	Bolívie	k1gFnSc1
<g/>
0.7030	0.7030	k4
<g/>
.0088	.0088	k4
▲	▲	k?
0.88	0.88	k4
<g/>
%	%	kIx~
</s>
<s>
0.702	0.702	k4
1150.000	1150.000	k4
▬	▬	k?
<g/>
Gabon	Gabon	k1gMnSc1
Gabon	Gabon	k1gMnSc1
<g/>
0.7020	0.7020	k4
<g/>
.0081	.0081	k4
▲	▲	k?
0.81	0.81	k4
<g/>
%	%	kIx~
</s>
<s>
0.700	0.700	k4
1160.000	1160.000	k4
▬	▬	k?
<g/>
Egypt	Egypt	k1gInSc1
Egypt	Egypt	k1gInSc1
<g/>
0.7000	0.7000	k4
<g/>
.0062	.0062	k4
▲	▲	k?
0.62	0.62	k4
<g/>
%	%	kIx~
</s>
<s>
0.6995	0.6995	k4
Střední	střední	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
lidského	lidský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
</s>
<s>
0.698	0.698	k4
117	#num#	k4
<g/>
-	-	kIx~
<g/>
0.001	0.001	k4
▼	▼	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Marshallovy	Marshallův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
Marshallovy	Marshallův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
<g/>
0.698	0.698	k4
<g/>
NA	na	k7c4
<g/>
[	[	kIx(
<g/>
P	P	kA
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
0.693	0.693	k4
1180.000	1180.000	k4
▬	▬	k?
<g/>
Vietnam	Vietnam	k1gInSc1
Vietnam	Vietnam	k1gInSc1
<g/>
0.6930	0.6930	k4
<g/>
.0074	.0074	k4
▲	▲	k?
0.74	0.74	k4
<g/>
%	%	kIx~
</s>
<s>
0.690	0.690	k4
1190.000	1190.000	k4
▬	▬	k?
<g/>
Palestina	Palestina	k1gFnSc1
Palestina	Palestina	k1gFnSc1
<g/>
0.6900	0.6900	k4
<g/>
.0035	.0035	k4
▲	▲	k?
0.35	0.35	k4
<g/>
%	%	kIx~
</s>
<s>
0.689	0.689	k4
1200.000	1200.000	k4
▬	▬	k?
<g/>
Irák	Irák	k1gInSc1
Irák	Irák	k1gInSc1
<g/>
0.6890	0.6890	k4
<g/>
.0068	.0068	k4
▲	▲	k?
0.68	0.68	k4
<g/>
%	%	kIx~
</s>
<s>
0.676	0.676	k4
1210.000	1210.000	k4
▬	▬	k?
<g/>
Maroko	Maroko	k1gNnSc4
Maroko	Maroko	k1gNnSc1
<g/>
0.6760	0.6760	k4
<g/>
.0114	.0114	k4
▲	▲	k?
1.14	1.14	k4
<g/>
%	%	kIx~
</s>
<s>
0.674	0.674	k4
1220.000	1220.000	k4
▬	▬	k?
<g/>
Kyrgyzstán	Kyrgyzstán	k2eAgInSc4d1
Kyrgyzstán	Kyrgyzstán	k1gInSc4
<g/>
0.6740	0.6740	k4
<g/>
.0073	.0073	k4
▲	▲	k?
0.73	0.73	k4
<g/>
%	%	kIx~
</s>
<s>
0.670	0.670	k4
1230.000	1230.000	k4
▬	▬	k?
<g/>
Guyana	Guyana	k1gFnSc1
Guyana	Guyana	k1gFnSc1
<g/>
0.6700	0.6700	k4
<g/>
.0061	.0061	k4
▲	▲	k?
0.61	0.61	k4
<g/>
%	%	kIx~
</s>
<s>
0.667	0.667	k4
1240.000	1240.000	k4
▬	▬	k?
<g/>
Salvador	Salvador	k1gMnSc1
Salvador	Salvador	k1gMnSc1
<g/>
0.6670	0.6670	k4
<g/>
.0014	.0014	k4
▲	▲	k?
0.14	0.14	k4
<g/>
%	%	kIx~
</s>
<s>
0.656	0.656	k4
1250.001	1250.001	k4
▲	▲	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Tádžikistán	Tádžikistán	k1gInSc1
Tádžikistán	Tádžikistán	k1gInSc1
<g/>
0.6560	0.6560	k4
<g/>
.0050	.0050	k4
▲	▲	k?
0.50	0.50	k4
<g/>
%	%	kIx~
</s>
<s>
0.651	0.651	k4
1260.002	1260.002	k4
▲	▲	k?
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
Kapverdy	Kapverda	k1gFnSc2
Kapverdy	Kapverda	k1gFnSc2
<g/>
0.6510	0.6510	k4
<g/>
.0048	.0048	k4
▲	▲	k?
0.48	0.48	k4
<g/>
%	%	kIx~
</s>
<s>
0.651	0.651	k4
1260.001	1260.001	k4
▲	▲	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Guatemala	Guatemala	k1gFnSc1
Guatemala	Guatemala	k1gFnSc1
<g/>
0.6510	0.6510	k4
<g/>
.0098	.0098	k4
▲	▲	k?
0.98	0.98	k4
<g/>
%	%	kIx~
</s>
<s>
0.651	0.651	k4
126	#num#	k4
<g/>
-	-	kIx~
<g/>
0.001	0.001	k4
▼	▼	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Nikaragua	Nikaragua	k1gFnSc1
Nikaragua	Nikaragua	k1gFnSc1
<g/>
0.6510	0.6510	k4
<g/>
.0074	.0074	k4
▲	▲	k?
0.74	0.74	k4
<g/>
%	%	kIx~
</s>
<s>
0.647	0.647	k4
1290.000	1290.000	k4
▬	▬	k?
<g/>
Indie	Indie	k1gFnSc1
Indie	Indie	k1gFnSc2
<g/>
0.6470	0.6470	k4
<g/>
.0134	.0134	k4
▲	▲	k?
1.34	1.34	k4
<g/>
%	%	kIx~
</s>
<s>
0.645	0.645	k4
130	#num#	k4
<g/>
-	-	kIx~
<g/>
0.001	0.001	k4
▼	▼	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Namibie	Namibie	k1gFnSc1
Namibie	Namibie	k1gFnSc2
<g/>
0.6450	0.6450	k4
<g/>
.0117	.0117	k4
▲	▲	k?
1.17	1.17	k4
<g/>
%	%	kIx~
</s>
<s>
0.626	0.626	k4
1310.000	1310.000	k4
▬	▬	k?
<g/>
Východní	východní	k2eAgMnSc1d1
Timor	Timor	k1gMnSc1
Východní	východní	k2eAgInSc4d1
Timor	Timor	k1gInSc4
<g/>
0.6260	0.6260	k4
<g/>
.0013	.0013	k4
▲	▲	k?
0.13	0.13	k4
<g/>
%	%	kIx~
</s>
<s>
0.623	0.623	k4
1320.001	1320.001	k4
▲	▲	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Honduras	Honduras	k1gInSc1
Honduras	Honduras	k1gInSc1
<g/>
0.6230	0.6230	k4
<g/>
.0051	.0051	k4
▲	▲	k?
0.51	0.51	k4
<g/>
%	%	kIx~
</s>
<s>
0.623	0.623	k4
1320.000	1320.000	k4
▬	▬	k?
<g/>
Kiribati	Kiribati	k1gFnSc1
Kiribati	Kiribati	k1gFnSc1
<g/>
0.6230	0.6230	k4
<g/>
.0071	.0071	k4
▲	▲	k?
0.71	0.71	k4
<g/>
%	%	kIx~
</s>
<s>
0.617	0.617	k4
1340.000	1340.000	k4
▬	▬	k?
<g/>
Bhútán	Bhútán	k1gInSc1
Bhútán	Bhútán	k1gInSc1
<g/>
0.6170	0.6170	k4
<g/>
.0098	.0098	k4
▲	▲	k?
0.98	0.98	k4
<g/>
%	%	kIx~
</s>
<s>
0.614	0.614	k4
1350.001	1350.001	k4
▲	▲	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Bangladéš	Bangladéš	k1gInSc1
Bangladéš	Bangladéš	k1gInSc1
<g/>
0.6140	0.6140	k4
<g/>
.0140	.0140	k4
▲	▲	k?
1.40	1.40	k4
<g/>
%	%	kIx~
</s>
<s>
0.614	0.614	k4
1350.000	1350.000	k4
▬	▬	k?
<g/>
Mikronésie	Mikronésie	k1gFnSc1
Mikronésie	Mikronésie	k1gFnSc1
<g/>
0.6140	0.6140	k4
<g/>
.0041	.0041	k4
▲	▲	k?
0.41	0.41	k4
<g/>
%	%	kIx~
</s>
<s>
0.609	0.609	k4
1370.002	1370.002	k4
▲	▲	k?
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
Svatý	svatý	k2eAgMnSc1d1
Tomáš	Tomáš	k1gMnSc1
a	a	k8xC
Princův	princův	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
Svatý	svatý	k2eAgMnSc1d1
Tomáš	Tomáš	k1gMnSc1
a	a	k8xC
Princův	princův	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
<g/>
0.6090	0.6090	k4
<g/>
.0136	.0136	k4
▲	▲	k?
1.36	1.36	k4
<g/>
%	%	kIx~
</s>
<s>
0.608	0.608	k4
138	#num#	k4
<g/>
-	-	kIx~
<g/>
0.002	0.002	k4
▼	▼	k?
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
Konžská	konžský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Konžská	konžský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
0.6080	0.6080	k4
<g/>
.0112	.0112	k4
▲	▲	k?
1.12	1.12	k4
<g/>
%	%	kIx~
</s>
<s>
0.608	0.608	k4
1380.001	1380.001	k4
▲	▲	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Svazijsko	Svazijsko	k1gNnSc4
Svazijsko	Svazijsko	k1gNnSc1
<g/>
0.6080	0.6080	k4
<g/>
.0215	.0215	k4
▲	▲	k?
2.15	2.15	k4
<g/>
%	%	kIx~
</s>
<s>
0.604	0.604	k4
1400.000	1400.000	k4
▬	▬	k?
<g/>
Laos	Laos	k1gInSc1
Laos	Laos	k1gInSc1
<g/>
0.6040	0.6040	k4
<g/>
.0128	.0128	k4
▲	▲	k?
1.28	1.28	k4
<g/>
%	%	kIx~
</s>
<s>
0.597	0.597	k4
1410.000	1410.000	k4
▬	▬	k?
<g/>
Vanuatu	Vanuat	k1gInSc2
Vanuatu	Vanuat	k1gInSc2
<g/>
0.5970	0.5970	k4
<g/>
.0026	.0026	k4
▲	▲	k?
0.26	0.26	k4
<g/>
%	%	kIx~
</s>
<s>
0.596	0.596	k4
1420.000	1420.000	k4
▬	▬	k?
<g/>
Ghana	Ghana	k1gFnSc1
Ghana	Ghana	k1gFnSc1
<g/>
0.5960	0.5960	k4
<g/>
.0091	.0091	k4
▲	▲	k?
0.91	0.91	k4
<g/>
%	%	kIx~
</s>
<s>
0.591	0.591	k4
1430.001	1430.001	k4
▲	▲	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Zambie	Zambie	k1gFnSc1
Zambie	Zambie	k1gFnSc2
<g/>
0.5910	0.5910	k4
<g/>
.0135	.0135	k4
▲	▲	k?
1.35	1.35	k4
<g/>
%	%	kIx~
</s>
<s>
0.588	0.588	k4
144	#num#	k4
<g/>
-	-	kIx~
<g/>
0.001	0.001	k4
▼	▼	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Rovníková	rovníkový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
Rovníková	rovníkový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
<g/>
0.5880	0.5880	k4
<g/>
.0018	.0018	k4
▲	▲	k?
0.18	0.18	k4
<g/>
%	%	kIx~
</s>
<s>
0.584	0.584	k4
1450.001	1450.001	k4
▲	▲	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Myanmar	Myanmar	k1gInSc1
(	(	kIx(
<g/>
Barma	Barma	k1gFnSc1
<g/>
)	)	kIx)
Myanmar	Myanmar	k1gInSc1
(	(	kIx(
<g/>
Barma	Barma	k1gFnSc1
<g/>
)	)	kIx)
<g/>
0.5840	0.5840	k4
<g/>
.0139	.0139	k4
▲	▲	k?
1.39	1.39	k4
<g/>
%	%	kIx~
</s>
<s>
0.581	0.581	k4
146	#num#	k4
<g/>
-	-	kIx~
<g/>
0.001	0.001	k4
▼	▼	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Kambodža	Kambodža	k1gFnSc1
Kambodža	Kambodža	k1gFnSc1
<g/>
0.5810	0.5810	k4
<g/>
.0105	.0105	k4
▲	▲	k?
1.05	1.05	k4
<g/>
%	%	kIx~
</s>
<s>
0.579	0.579	k4
1470.001	1470.001	k4
▲	▲	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Keňa	Keňa	k1gFnSc1
Keňa	Keňa	k1gFnSc1
<g/>
0.5790	0.5790	k4
<g/>
.0104	.0104	k4
▲	▲	k?
1.04	1.04	k4
<g/>
%	%	kIx~
</s>
<s>
0.579	0.579	k4
1470.001	1470.001	k4
▲	▲	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Nepál	Nepál	k1gInSc1
Nepál	Nepál	k1gInSc1
<g/>
0.5790	0.5790	k4
<g/>
.0118	.0118	k4
▲	▲	k?
1.18	1.18	k4
<g/>
%	%	kIx~
</s>
<s>
0.574	0.574	k4
149	#num#	k4
<g/>
-	-	kIx~
<g/>
0.002	0.002	k4
▼	▼	k?
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
Angola	Angola	k1gFnSc1
Angola	Angola	k1gFnSc1
<g/>
0.5740	0.5740	k4
<g/>
.0150	.0150	k4
▲	▲	k?
1.50	1.50	k4
<g/>
%	%	kIx~
</s>
<s>
0.563	0.563	k4
1500.000	1500.000	k4
▬	▬	k?
<g/>
Kamerun	Kamerun	k1gInSc1
Kamerun	Kamerun	k1gInSc1
<g/>
0.5630	0.5630	k4
<g/>
.0226	.0226	k4
▲	▲	k?
2.26	2.26	k4
<g/>
%	%	kIx~
</s>
<s>
0.563	0.563	k4
1500.003	1500.003	k4
▲	▲	k?
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
Zimbabwe	Zimbabwe	k1gInSc1
Zimbabwe	Zimbabwe	k1gNnSc1
<g/>
0.5630	0.5630	k4
<g/>
.0222	.0222	k4
▲	▲	k?
2.22	2.22	k4
<g/>
%	%	kIx~
</s>
<s>
0.560	0.560	k4
152	#num#	k4
<g/>
-	-	kIx~
<g/>
0.001	0.001	k4
▼	▼	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Pákistán	Pákistán	k1gInSc1
Pákistán	Pákistán	k1gInSc1
<g/>
0.5600	0.5600	k4
<g/>
.0085	.0085	k4
▲	▲	k?
0.85	0.85	k4
<g/>
%	%	kIx~
</s>
<s>
0.557	0.557	k4
153	#num#	k4
<g/>
-	-	kIx~
<g/>
0.001	0.001	k4
▼	▼	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Šalomounovy	Šalomounův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
Šalomounovy	Šalomounův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
<g/>
0.5570	0.5570	k4
<g/>
.0078	.0078	k4
▲	▲	k?
0.78	0.78	k4
<g/>
%	%	kIx~
</s>
<s>
0.5495	0.5495	k4
Nízká	nízký	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
lidského	lidský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
</s>
<s>
0.549	0.549	k4
1540.000	1540.000	k4
▬	▬	k?
<g/>
Sýrie	Sýrie	k1gFnSc1
Sýrie	Sýrie	k1gFnSc1
<g/>
0.549	0.549	k4
<g/>
-	-	kIx~
<g/>
0.0198	0.0198	k4
▼	▼	k?
-1.98	-1.98	k4
<g/>
%	%	kIx~
</s>
<s>
0.543	0.543	k4
1550.000	1550.000	k4
▬	▬	k?
<g/>
Papua	Papu	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
Papua	Papu	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
<g/>
0.5430	0.5430	k4
<g/>
.0080	.0080	k4
▲	▲	k?
0.80	0.80	k4
<g/>
%	%	kIx~
</s>
<s>
0.538	0.538	k4
1560.000	1560.000	k4
▬	▬	k?
<g/>
Komory	komora	k1gFnSc2
Komory	komora	k1gFnSc2
<g/>
0.5380	0.5380	k4
<g/>
.0060	.0060	k4
▲	▲	k?
0.60	0.60	k4
<g/>
%	%	kIx~
</s>
<s>
0.536	0.536	k4
1570.001	1570.001	k4
▲	▲	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Rwanda	Rwanda	k1gFnSc1
Rwanda	Rwanda	k1gFnSc1
<g/>
0.5360	0.5360	k4
<g/>
.0119	.0119	k4
▲	▲	k?
1.19	1.19	k4
<g/>
%	%	kIx~
</s>
<s>
0.534	0.534	k4
158	#num#	k4
<g/>
-	-	kIx~
<g/>
0.001	0.001	k4
▼	▼	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Nigérie	Nigérie	k1gFnSc1
Nigérie	Nigérie	k1gFnSc1
<g/>
0.5340	0.5340	k4
<g/>
.0125	.0125	k4
▲	▲	k?
1.25	1.25	k4
<g/>
%	%	kIx~
</s>
<s>
0.528	0.528	k4
1590.001	1590.001	k4
▲	▲	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Tanzanie	Tanzanie	k1gFnSc1
Tanzanie	Tanzanie	k1gFnSc2
<g/>
0.5280	0.5280	k4
<g/>
.0103	.0103	k4
▲	▲	k?
1.03	1.03	k4
<g/>
%	%	kIx~
</s>
<s>
0.528	0.528	k4
1590.001	1590.001	k4
▲	▲	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Uganda	Uganda	k1gFnSc1
Uganda	Uganda	k1gFnSc1
<g/>
0.5280	0.5280	k4
<g/>
.0097	.0097	k4
▲	▲	k?
0.97	0.97	k4
<g/>
%	%	kIx~
</s>
<s>
0.527	0.527	k4
161	#num#	k4
<g/>
-	-	kIx~
<g/>
0.002	0.002	k4
▼	▼	k?
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
Mauritánie	Mauritánie	k1gFnSc1
Mauritánie	Mauritánie	k1gFnSc2
<g/>
0.5270	0.5270	k4
<g/>
.0091	.0091	k4
▲	▲	k?
0.91	0.91	k4
<g/>
%	%	kIx~
</s>
<s>
0.521	0.521	k4
1620.000	1620.000	k4
▬	▬	k?
<g/>
Madagaskar	Madagaskar	k1gInSc1
Madagaskar	Madagaskar	k1gInSc1
<g/>
0.5210	0.5210	k4
<g/>
.0042	.0042	k4
▲	▲	k?
0.42	0.42	k4
<g/>
%	%	kIx~
</s>
<s>
0.520	0.520	k4
1630.000	1630.000	k4
▬	▬	k?
<g/>
Benin	Benin	k2eAgInSc4d1
Benin	Benin	k1gInSc4
<g/>
0.5200	0.5200	k4
<g/>
.0119	.0119	k4
▲	▲	k?
1.19	1.19	k4
<g/>
%	%	kIx~
</s>
<s>
0.518	0.518	k4
1640.000	1640.000	k4
▬	▬	k?
<g/>
Lesotho	Lesot	k1gMnSc4
Lesotho	Lesot	k1gMnSc4
<g/>
0.5180	0.5180	k4
<g/>
.0146	.0146	k4
▲	▲	k?
1.46	1.46	k4
<g/>
%	%	kIx~
</s>
<s>
0.516	0.516	k4
1650.000	1650.000	k4
▬	▬	k?
<g/>
Pobřeží	pobřeží	k1gNnSc1
slonoviny	slonovina	k1gFnSc2
Pobřeží	pobřeží	k1gNnSc2
slonoviny	slonovina	k1gFnSc2
<g/>
0.5160	0.5160	k4
<g/>
.0161	.0161	k4
▲	▲	k?
1.61	1.61	k4
<g/>
%	%	kIx~
</s>
<s>
0.514	0.514	k4
1660.000	1660.000	k4
▬	▬	k?
<g/>
Senegal	Senegal	k1gInSc1
Senegal	Senegal	k1gInSc1
<g/>
0.5140	0.5140	k4
<g/>
.0117	.0117	k4
▲	▲	k?
1.17	1.17	k4
<g/>
%	%	kIx~
</s>
<s>
0.513	0.513	k4
1670.000	1670.000	k4
▬	▬	k?
<g/>
Togo	Togo	k1gNnSc4
Togo	Togo	k1gNnSc1
<g/>
0.5130	0.5130	k4
<g/>
.0116	.0116	k4
▲	▲	k?
1.16	1.16	k4
<g/>
%	%	kIx~
</s>
<s>
0.507	0.507	k4
1680.000	1680.000	k4
▬	▬	k?
<g/>
Súdán	Súdán	k1gInSc1
Súdán	Súdán	k1gInSc1
<g/>
0.5070	0.5070	k4
<g/>
.0093	.0093	k4
▲	▲	k?
0.93	0.93	k4
<g/>
%	%	kIx~
</s>
<s>
0.503	0.503	k4
1690.000	1690.000	k4
▬	▬	k?
<g/>
Haiti	Haiti	k1gNnSc1
Haiti	Haiti	k1gNnSc2
<g/>
0.5030	0.5030	k4
<g/>
.0092	.0092	k4
▲	▲	k?
0.92	0.92	k4
<g/>
%	%	kIx~
</s>
<s>
0.496	0.496	k4
1700.000	1700.000	k4
▬	▬	k?
<g/>
Afghánistán	Afghánistán	k1gInSc1
Afghánistán	Afghánistán	k1gInSc1
<g/>
0.4960	0.4960	k4
<g/>
.0083	.0083	k4
▲	▲	k?
0.83	0.83	k4
<g/>
%	%	kIx~
</s>
<s>
0.495	0.495	k4
1710.000	1710.000	k4
▬	▬	k?
<g/>
Džibutsko	Džibutsko	k1gNnSc4
Džibutsko	Džibutsko	k1gNnSc1
<g/>
0.4950	0.4950	k4
<g/>
.0132	.0132	k4
▲	▲	k?
1.32	1.32	k4
<g/>
%	%	kIx~
</s>
<s>
0.485	0.485	k4
1720.000	1720.000	k4
▬	▬	k?
<g/>
Malawi	Malawi	k1gNnSc1
Malawi	Malawi	k1gNnSc2
<g/>
0.4850	0.4850	k4
<g/>
.0132	.0132	k4
▲	▲	k?
1.32	1.32	k4
<g/>
%	%	kIx~
</s>
<s>
0.470	0.470	k4
1730.000	1730.000	k4
▬	▬	k?
<g/>
Etiopie	Etiopie	k1gFnSc1
Etiopie	Etiopie	k1gFnSc1
<g/>
0.4700	0.4700	k4
<g/>
.0166	.0166	k4
▲	▲	k?
1.66	1.66	k4
<g/>
%	%	kIx~
</s>
<s>
0.466	0.466	k4
1740.003	1740.003	k4
▲	▲	k?
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
Gambie	Gambie	k1gFnSc1
Gambie	Gambie	k1gFnSc2
<g/>
0.4660	0.4660	k4
<g/>
.0079	.0079	k4
▲	▲	k?
0.79	0.79	k4
<g/>
%	%	kIx~
</s>
<s>
0.466	0.466	k4
1740.002	1740.002	k4
▲	▲	k?
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
Guinea	Guinea	k1gFnSc1
Guinea	guinea	k1gFnSc2
<g/>
0.4660	0.4660	k4
<g/>
.0167	.0167	k4
▲	▲	k?
1.67	1.67	k4
<g/>
%	%	kIx~
</s>
<s>
0.465	0.465	k4
176	#num#	k4
<g/>
-	-	kIx~
<g/>
0.003	0.003	k4
▼	▼	k?
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
Libérie	Libérie	k1gFnSc1
Libérie	Libérie	k1gFnSc2
<g/>
0.4650	0.4650	k4
<g/>
.0067	.0067	k4
▲	▲	k?
0.67	0.67	k4
<g/>
%	%	kIx~
</s>
<s>
0.463	0.463	k4
177	#num#	k4
<g/>
-	-	kIx~
<g/>
0.002	0.002	k4
▼	▼	k?
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
Jemen	Jemen	k1gInSc1
Jemen	Jemen	k1gInSc1
<g/>
0.463	0.463	k4
<g/>
-	-	kIx~
<g/>
0.0094	0.0094	k4
▼	▼	k?
-0.94	-0.94	k4
<g/>
%	%	kIx~
</s>
<s>
0.461	0.461	k4
178	#num#	k4
<g/>
-	-	kIx~
<g/>
0.001	0.001	k4
▼	▼	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Guinea-Bissau	Guinea-Bissaus	k1gInSc2
Guinea-Bissau	Guinea-Bissaus	k1gInSc2
<g/>
0.4610	0.4610	k4
<g/>
.0101	.0101	k4
▲	▲	k?
1.01	1.01	k4
<g/>
%	%	kIx~
</s>
<s>
0.459	0.459	k4
1790.000	1790.000	k4
▬	▬	k?
<g/>
Konžská	konžský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Konžská	konžský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
0.4590	0.4590	k4
<g/>
.0124	.0124	k4
▲	▲	k?
1.24	1.24	k4
<g/>
%	%	kIx~
</s>
<s>
0.446	0.446	k4
1800.000	1800.000	k4
▬	▬	k?
<g/>
Mosambik	Mosambik	k1gInSc1
Mosambik	Mosambik	k1gInSc1
<g/>
0.4460	0.4460	k4
<g/>
.0151	.0151	k4
▲	▲	k?
1.51	1.51	k4
<g/>
%	%	kIx~
</s>
<s>
0.438	0.438	k4
1810.000	1810.000	k4
▬	▬	k?
<g/>
Sierra	Sierra	k1gFnSc1
Leone	Leo	k1gMnSc5
Sierra	Sierra	k1gFnSc1
Leone	Leo	k1gMnSc5
<g/>
0.4380	0.4380	k4
<g/>
.0145	.0145	k4
▲	▲	k?
1.45	1.45	k4
<g/>
%	%	kIx~
</s>
<s>
0.434	0.434	k4
1820.001	1820.001	k4
▲	▲	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Burkina	Burkina	k1gMnSc1
Faso	Faso	k1gMnSc1
Burkina	Burkina	k1gMnSc1
Faso	Faso	k1gMnSc1
<g/>
0.4340	0.4340	k4
<g/>
.0184	.0184	k4
▲	▲	k?
1.84	1.84	k4
<g/>
%	%	kIx~
</s>
<s>
0.434	0.434	k4
1820.000	1820.000	k4
▬	▬	k?
<g/>
Eritrea	Eritrea	k1gFnSc1
Eritrea	Eritrea	k1gFnSc1
<g/>
0.4340	0.4340	k4
<g/>
.0002	.0002	k4
▲	▲	k?
0.02	0.02	k4
<g/>
%	%	kIx~
</s>
<s>
0.427	0.427	k4
1840.000	1840.000	k4
▬	▬	k?
<g/>
Mali	Mali	k1gNnSc1
Mali	Mali	k1gNnSc2
<g/>
0.4270	0.4270	k4
<g/>
.0072	.0072	k4
▲	▲	k?
0.72	0.72	k4
<g/>
%	%	kIx~
</s>
<s>
0.423	0.423	k4
1850.000	1850.000	k4
▬	▬	k?
<g/>
Burundi	Burundi	k1gNnSc1
Burundi	Burundi	k1gNnSc2
<g/>
0.4230	0.4230	k4
<g/>
.0065	.0065	k4
▲	▲	k?
0.65	0.65	k4
<g/>
%	%	kIx~
</s>
<s>
0.413	0.413	k4
1860.000	1860.000	k4
▬	▬	k?
<g/>
Jižní	jižní	k2eAgInSc1d1
Súdán	Súdán	k1gInSc1
Jižní	jižní	k2eAgInSc1d1
Súdán	Súdán	k1gInSc1
<g/>
0.413	0.413	k4
<g/>
-	-	kIx~
<g/>
0.0035	0.0035	k4
▼	▼	k?
-0.35	-0.35	k4
<g/>
%	%	kIx~
</s>
<s>
0.401	0.401	k4
1870.000	1870.000	k4
▬	▬	k?
<g/>
Čad	Čad	k1gInSc1
Čad	Čad	k1gInSc1
<g/>
0.4010	0.4010	k4
<g/>
.0089	.0089	k4
▲	▲	k?
0.89	0.89	k4
<g/>
%	%	kIx~
</s>
<s>
0.381	0.381	k4
1880.000	1880.000	k4
▬	▬	k?
<g/>
Středoafrická	středoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Středoafrická	středoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
0.3810	0.3810	k4
<g/>
.0089	.0089	k4
▲	▲	k?
0.89	0.89	k4
<g/>
%	%	kIx~
</s>
<s>
0.377	0.377	k4
1890.000	1890.000	k4
▬	▬	k?
<g/>
Niger	Niger	k1gInSc1
Niger	Niger	k1gInSc1
<g/>
0.3770	0.3770	k4
<g/>
.0209	.0209	k4
▲	▲	k?
2.09	2.09	k4
<g/>
%	%	kIx~
</s>
<s>
Vývoj	vývoj	k1gInSc1
indexu	index	k1gInSc2
jednotlivých	jednotlivý	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
HDI	HDI	kA
pořadí	pořadí	k1gNnSc1
</s>
<s>
Stát	stát	k1gInSc1
</s>
<s>
1980	#num#	k4
</s>
<s>
1985	#num#	k4
</s>
<s>
1990	#num#	k4
</s>
<s>
2000	#num#	k4
</s>
<s>
2005	#num#	k4
</s>
<s>
2006	#num#	k4
</s>
<s>
2007	#num#	k4
</s>
<s>
2008	#num#	k4
</s>
<s>
2009	#num#	k4
</s>
<s>
2010	#num#	k4
</s>
<s>
2011	#num#	k4
</s>
<s>
2012	#num#	k4
</s>
<s>
2013	#num#	k4
</s>
<s>
..	..	k?
</s>
<s>
velmi	velmi	k6eAd1
vysoký	vysoký	k2eAgInSc1d1
(	(	kIx(
<g/>
rozvinuté	rozvinutý	k2eAgFnPc1d1
země	zem	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
0.757	0.757	k4
</s>
<s>
0.775	0.775	k4
</s>
<s>
0.798	0.798	k4
</s>
<s>
0.849	0.849	k4
</s>
<s>
0.870	0.870	k4
</s>
<s>
0.874	0.874	k4
</s>
<s>
0.877	0.877	k4
</s>
<s>
0.879	0.879	k4
</s>
<s>
0.880	0.880	k4
</s>
<s>
0.885	0.885	k4
</s>
<s>
0.887	0.887	k4
</s>
<s>
0.889	0.889	k4
</s>
<s>
0.890	0.890	k4
</s>
<s>
..	..	k?
</s>
<s>
vysoký	vysoký	k2eAgInSc1d1
(	(	kIx(
<g/>
rozvojové	rozvojový	k2eAgFnPc1d1
země	zem	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
0.534	0.534	k4
</s>
<s>
0.552	0.552	k4
</s>
<s>
0.593	0.593	k4
</s>
<s>
0.643	0.643	k4
</s>
<s>
0.682	0.682	k4
</s>
<s>
0.691	0.691	k4
</s>
<s>
0.701	0.701	k4
</s>
<s>
0.710	0.710	k4
</s>
<s>
0.715	0.715	k4
</s>
<s>
0.723	0.723	k4
</s>
<s>
0.729	0.729	k4
</s>
<s>
0.733	0.733	k4
</s>
<s>
0.735	0.735	k4
</s>
<s>
..	..	k?
</s>
<s>
střední	střední	k2eAgFnPc1d1
(	(	kIx(
<g/>
rozvojové	rozvojový	k2eAgFnPc1d1
země	zem	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
0.420	0.420	k4
</s>
<s>
0.448	0.448	k4
</s>
<s>
0.474	0.474	k4
</s>
<s>
0.528	0.528	k4
</s>
<s>
0.565	0.565	k4
</s>
<s>
0.573	0.573	k4
</s>
<s>
0.580	0.580	k4
</s>
<s>
0.587	0.587	k4
</s>
<s>
0.593	0.593	k4
</s>
<s>
0.601	0.601	k4
</s>
<s>
0.609	0.609	k4
</s>
<s>
0.612	0.612	k4
</s>
<s>
0.614	0.614	k4
</s>
<s>
..	..	k?
</s>
<s>
nízký	nízký	k2eAgInSc1d1
(	(	kIx(
<g/>
rozvojové	rozvojový	k2eAgFnPc1d1
země	zem	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
0.345	0.345	k4
</s>
<s>
0.365	0.365	k4
</s>
<s>
0.367	0.367	k4
</s>
<s>
0.403	0.403	k4
</s>
<s>
0.444	0.444	k4
</s>
<s>
0.456	0.456	k4
</s>
<s>
0.465	0.465	k4
</s>
<s>
0.471	0.471	k4
</s>
<s>
0.478	0.478	k4
</s>
<s>
0.479	0.479	k4
</s>
<s>
0.486	0.486	k4
</s>
<s>
0.490	0.490	k4
</s>
<s>
0.493	0.493	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Norsko	Norsko	k1gNnSc1
</s>
<s>
0.793	0.793	k4
</s>
<s>
0.814	0.814	k4
</s>
<s>
0.841	0.841	k4
</s>
<s>
0.910	0.910	k4
</s>
<s>
0.935	0.935	k4
</s>
<s>
0.938	0.938	k4
</s>
<s>
0.938	0.938	k4
</s>
<s>
0.937	0.937	k4
</s>
<s>
0.937	0.937	k4
</s>
<s>
0.939	0.939	k4
</s>
<s>
0.941	0.941	k4
</s>
<s>
0.943	0.943	k4
</s>
<s>
0.944	0.944	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
0.841	0.841	k4
</s>
<s>
0.853	0.853	k4
</s>
<s>
0.866	0.866	k4
</s>
<s>
0.898	0.898	k4
</s>
<s>
0.912	0.912	k4
</s>
<s>
0.915	0.915	k4
</s>
<s>
0.918	0.918	k4
</s>
<s>
0.922	0.922	k4
</s>
<s>
0.924	0.924	k4
</s>
<s>
0.926	0.926	k4
</s>
<s>
0.928	0.928	k4
</s>
<s>
0.931	0.931	k4
</s>
<s>
0.933	0.933	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
0.806	0.806	k4
</s>
<s>
0.813	0.813	k4
</s>
<s>
0.829	0.829	k4
</s>
<s>
0.886	0.886	k4
</s>
<s>
0.901	0.901	k4
</s>
<s>
0.905	0.905	k4
</s>
<s>
0.905	0.905	k4
</s>
<s>
0.903	0.903	k4
</s>
<s>
0.909	0.909	k4
</s>
<s>
0.915	0.915	k4
</s>
<s>
0.914	0.914	k4
</s>
<s>
0.916	0.916	k4
</s>
<s>
0.917	0.917	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
0.783	0.783	k4
</s>
<s>
0.796	0.796	k4
</s>
<s>
0.826	0.826	k4
</s>
<s>
0.874	0.874	k4
</s>
<s>
0.888	0.888	k4
</s>
<s>
0.895	0.895	k4
</s>
<s>
0.901	0.901	k4
</s>
<s>
0.901	0.901	k4
</s>
<s>
0.900	0.900	k4
</s>
<s>
0.904	0.904	k4
</s>
<s>
0.914	0.914	k4
</s>
<s>
0.915	0.915	k4
</s>
<s>
0.915	0.915	k4
</s>
<s>
5	#num#	k4
</s>
<s>
USA	USA	kA
</s>
<s>
0.825	0.825	k4
</s>
<s>
0.839	0.839	k4
</s>
<s>
0.858	0.858	k4
</s>
<s>
0.883	0.883	k4
</s>
<s>
0.897	0.897	k4
</s>
<s>
0.900	0.900	k4
</s>
<s>
0.903	0.903	k4
</s>
<s>
0.905	0.905	k4
</s>
<s>
0.905	0.905	k4
</s>
<s>
0.908	0.908	k4
</s>
<s>
0.911	0.911	k4
</s>
<s>
0.912	0.912	k4
</s>
<s>
0.914	0.914	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
0.739	0.739	k4
</s>
<s>
0.752	0.752	k4
</s>
<s>
0.782	0.782	k4
</s>
<s>
0.854	0.854	k4
</s>
<s>
0.887	0.887	k4
</s>
<s>
0.896	0.896	k4
</s>
<s>
0.899	0.899	k4
</s>
<s>
0.902	0.902	k4
</s>
<s>
0.901	0.901	k4
</s>
<s>
0.904	0.904	k4
</s>
<s>
0.908	0.908	k4
</s>
<s>
0.911	0.911	k4
</s>
<s>
0.911	0.911	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
</s>
<s>
0.793	0.793	k4
</s>
<s>
0.806	0.806	k4
</s>
<s>
0.821	0.821	k4
</s>
<s>
0.873	0.873	k4
</s>
<s>
0.894	0.894	k4
</s>
<s>
0.896	0.896	k4
</s>
<s>
0.899	0.899	k4
</s>
<s>
0.899	0.899	k4
</s>
<s>
0.903	0.903	k4
</s>
<s>
0.903	0.903	k4
</s>
<s>
0.904	0.904	k4
</s>
<s>
0.908	0.908	k4
</s>
<s>
0.910	0.910	k4
</s>
<s>
8	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
0.809	0.809	k4
</s>
<s>
0.822	0.822	k4
</s>
<s>
0.848	0.848	k4
</s>
<s>
0.867	0.867	k4
</s>
<s>
0.892	0.892	k4
</s>
<s>
0.894	0.894	k4
</s>
<s>
0.895	0.895	k4
</s>
<s>
0.896	0.896	k4
</s>
<s>
0.894	0.894	k4
</s>
<s>
0.896	0.896	k4
</s>
<s>
0.900	0.900	k4
</s>
<s>
0.901	0.901	k4
</s>
<s>
0.902	0.902	k4
</s>
<s>
9	#num#	k4
</s>
<s>
Singapur	Singapur	k1gInSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.744	0.744	k4
</s>
<s>
0.800	0.800	k4
</s>
<s>
0.840	0.840	k4
</s>
<s>
0.852	0.852	k4
</s>
<s>
0.862	0.862	k4
</s>
<s>
0.868	0.868	k4
</s>
<s>
0.868	0.868	k4
</s>
<s>
0.894	0.894	k4
</s>
<s>
0.896	0.896	k4
</s>
<s>
0.899	0.899	k4
</s>
<s>
0.901	0.901	k4
</s>
<s>
10	#num#	k4
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
</s>
<s>
0.781	0.781	k4
</s>
<s>
0.797	0.797	k4
</s>
<s>
0.806	0.806	k4
</s>
<s>
0.859	0.859	k4
</s>
<s>
0.891	0.891	k4
</s>
<s>
0.893	0.893	k4
</s>
<s>
0.895	0.895	k4
</s>
<s>
0.896	0.896	k4
</s>
<s>
0.895	0.895	k4
</s>
<s>
0.898	0.898	k4
</s>
<s>
0.899	0.899	k4
</s>
<s>
0.900	0.900	k4
</s>
<s>
0.900	0.900	k4
</s>
<s>
11	#num#	k4
</s>
<s>
Irsko	Irsko	k1gNnSc1
</s>
<s>
0.734	0.734	k4
</s>
<s>
0.751	0.751	k4
</s>
<s>
0.775	0.775	k4
</s>
<s>
0.862	0.862	k4
</s>
<s>
0.890	0.890	k4
</s>
<s>
0.895	0.895	k4
</s>
<s>
0.901	0.901	k4
</s>
<s>
0.902	0.902	k4
</s>
<s>
0.898	0.898	k4
</s>
<s>
0.899	0.899	k4
</s>
<s>
0.900	0.900	k4
</s>
<s>
0.901	0.901	k4
</s>
<s>
0.899	0.899	k4
</s>
<s>
12	#num#	k4
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
0.776	0.776	k4
</s>
<s>
0.786	0.786	k4
</s>
<s>
0.807	0.807	k4
</s>
<s>
0.889	0.889	k4
</s>
<s>
0.887	0.887	k4
</s>
<s>
0.889	0.889	k4
</s>
<s>
0.891	0.891	k4
</s>
<s>
0.891	0.891	k4
</s>
<s>
0.888	0.888	k4
</s>
<s>
0.895	0.895	k4
</s>
<s>
0.896	0.896	k4
</s>
<s>
0.897	0.897	k4
</s>
<s>
0.898	0.898	k4
</s>
<s>
13	#num#	k4
</s>
<s>
Island	Island	k1gInSc1
</s>
<s>
0.754	0.754	k4
</s>
<s>
0.776	0.776	k4
</s>
<s>
0.800	0.800	k4
</s>
<s>
0.858	0.858	k4
</s>
<s>
0.888	0.888	k4
</s>
<s>
0.890	0.890	k4
</s>
<s>
0.894	0.894	k4
</s>
<s>
0.886	0.886	k4
</s>
<s>
0.885	0.885	k4
</s>
<s>
0.886	0.886	k4
</s>
<s>
0.890	0.890	k4
</s>
<s>
0.893	0.893	k4
</s>
<s>
0.895	0.895	k4
</s>
<s>
14	#num#	k4
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
</s>
<s>
0.735	0.735	k4
</s>
<s>
0.747	0.747	k4
</s>
<s>
0.768	0.768	k4
</s>
<s>
0.863	0.863	k4
</s>
<s>
0.888	0.888	k4
</s>
<s>
0.885	0.885	k4
</s>
<s>
0.887	0.887	k4
</s>
<s>
0.890	0.890	k4
</s>
<s>
0.890	0.890	k4
</s>
<s>
0.895	0.895	k4
</s>
<s>
0.891	0.891	k4
</s>
<s>
0.890	0.890	k4
</s>
<s>
0.892	0.892	k4
</s>
<s>
15	#num#	k4
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
0.628	0.628	k4
</s>
<s>
0.681	0.681	k4
</s>
<s>
0.731	0.731	k4
</s>
<s>
0.819	0.819	k4
</s>
<s>
0.856	0.856	k4
</s>
<s>
0.862	0.862	k4
</s>
<s>
0.869	0.869	k4
</s>
<s>
0.874	0.874	k4
</s>
<s>
0.876	0.876	k4
</s>
<s>
0.882	0.882	k4
</s>
<s>
0.886	0.886	k4
</s>
<s>
0.888	0.888	k4
</s>
<s>
0.891	0.891	k4
</s>
<s>
15	#num#	k4
</s>
<s>
Hong	Hong	k1gInSc1
Kong	Kongo	k1gNnPc2
</s>
<s>
0.698	0.698	k4
</s>
<s>
0.741	0.741	k4
</s>
<s>
0.775	0.775	k4
</s>
<s>
0.810	0.810	k4
</s>
<s>
0.839	0.839	k4
</s>
<s>
0.847	0.847	k4
</s>
<s>
0.860	0.860	k4
</s>
<s>
0.877	0.877	k4
</s>
<s>
0.879	0.879	k4
</s>
<s>
0.882	0.882	k4
</s>
<s>
0.886	0.886	k4
</s>
<s>
0.889	0.889	k4
</s>
<s>
0.891	0.891	k4
</s>
<s>
17	#num#	k4
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
0.772	0.772	k4
</s>
<s>
0.794	0.794	k4
</s>
<s>
0.817	0.817	k4
</s>
<s>
0.858	0.858	k4
</s>
<s>
0.873	0.873	k4
</s>
<s>
0.877	0.877	k4
</s>
<s>
0.879	0.879	k4
</s>
<s>
0.881	0.881	k4
</s>
<s>
0.880	0.880	k4
</s>
<s>
0.884	0.884	k4
</s>
<s>
0.887	0.887	k4
</s>
<s>
0.888	0.888	k4
</s>
<s>
0.890	0.890	k4
</s>
<s>
18	#num#	k4
</s>
<s>
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.882	0.882	k4
</s>
<s>
0.887	0.887	k4
</s>
<s>
0.888	0.888	k4
</s>
<s>
0.889	0.889	k4
</s>
<s>
19	#num#	k4
</s>
<s>
Izrael	Izrael	k1gInSc1
</s>
<s>
0.749	0.749	k4
</s>
<s>
0.771	0.771	k4
</s>
<s>
0.785	0.785	k4
</s>
<s>
0.849	0.849	k4
</s>
<s>
0.869	0.869	k4
</s>
<s>
0.872	0.872	k4
</s>
<s>
0.877	0.877	k4
</s>
<s>
0.877	0.877	k4
</s>
<s>
0.878	0.878	k4
</s>
<s>
0.881	0.881	k4
</s>
<s>
0.885	0.885	k4
</s>
<s>
0.886	0.886	k4
</s>
<s>
0.888	0.888	k4
</s>
<s>
20	#num#	k4
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
0.722	0.722	k4
</s>
<s>
0.741	0.741	k4
</s>
<s>
0.779	0.779	k4
</s>
<s>
0.848	0.848	k4
</s>
<s>
0.867	0.867	k4
</s>
<s>
0.870	0.870	k4
</s>
<s>
0.873	0.873	k4
</s>
<s>
0.875	0.875	k4
</s>
<s>
0.876	0.876	k4
</s>
<s>
0.879	0.879	k4
</s>
<s>
0.882	0.882	k4
</s>
<s>
0.884	0.884	k4
</s>
<s>
0.884	0.884	k4
</s>
<s>
21	#num#	k4
</s>
<s>
Lucembursko	Lucembursko	k1gNnSc1
</s>
<s>
0.729	0.729	k4
</s>
<s>
0.762	0.762	k4
</s>
<s>
0.786	0.786	k4
</s>
<s>
0.866	0.866	k4
</s>
<s>
0.876	0.876	k4
</s>
<s>
0.877	0.877	k4
</s>
<s>
0.880	0.880	k4
</s>
<s>
0.882	0.882	k4
</s>
<s>
0.876	0.876	k4
</s>
<s>
0.881	0.881	k4
</s>
<s>
0.881	0.881	k4
</s>
<s>
0.880	0.880	k4
</s>
<s>
0.881	0.881	k4
</s>
<s>
21	#num#	k4
</s>
<s>
Belgie	Belgie	k1gFnSc1
</s>
<s>
0.753	0.753	k4
</s>
<s>
0.773	0.773	k4
</s>
<s>
0.805	0.805	k4
</s>
<s>
0.873	0.873	k4
</s>
<s>
0.865	0.865	k4
</s>
<s>
0.868	0.868	k4
</s>
<s>
0.871	0.871	k4
</s>
<s>
0.873	0.873	k4
</s>
<s>
0.873	0.873	k4
</s>
<s>
0.877	0.877	k4
</s>
<s>
0.880	0.880	k4
</s>
<s>
0.880	0.880	k4
</s>
<s>
0.881	0.881	k4
</s>
<s>
21	#num#	k4
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
0.736	0.736	k4
</s>
<s>
0.754	0.754	k4
</s>
<s>
0.786	0.786	k4
</s>
<s>
0.835	0.835	k4
</s>
<s>
0.851	0.851	k4
</s>
<s>
0.857	0.857	k4
</s>
<s>
0.861	0.861	k4
</s>
<s>
0.868	0.868	k4
</s>
<s>
0.870	0.870	k4
</s>
<s>
0.877	0.877	k4
</s>
<s>
0.879	0.879	k4
</s>
<s>
0.880	0.880	k4
</s>
<s>
0.881	0.881	k4
</s>
<s>
24	#num#	k4
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
0.752	0.752	k4
</s>
<s>
0.771	0.771	k4
</s>
<s>
0.792	0.792	k4
</s>
<s>
0.841	0.841	k4
</s>
<s>
0.869	0.869	k4
</s>
<s>
0.874	0.874	k4
</s>
<s>
0.877	0.877	k4
</s>
<s>
0.878	0.878	k4
</s>
<s>
0.873	0.873	k4
</s>
<s>
0.877	0.877	k4
</s>
<s>
0.879	0.879	k4
</s>
<s>
0.879	0.879	k4
</s>
<s>
0.879	0.879	k4
</s>
<s>
25	#num#	k4
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.769	0.769	k4
</s>
<s>
0.821	0.821	k4
</s>
<s>
0.855	0.855	k4
</s>
<s>
0.861	0.861	k4
</s>
<s>
0.865	0.865	k4
</s>
<s>
0.871	0.871	k4
</s>
<s>
0.875	0.875	k4
</s>
<s>
0.873	0.873	k4
</s>
<s>
0.874	0.874	k4
</s>
<s>
0.874	0.874	k4
</s>
<s>
0.874	0.874	k4
</s>
<s>
26	#num#	k4
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
0.718	0.718	k4
</s>
<s>
0.735	0.735	k4
</s>
<s>
0.763	0.763	k4
</s>
<s>
0.825	0.825	k4
</s>
<s>
0.858	0.858	k4
</s>
<s>
0.863	0.863	k4
</s>
<s>
0.867	0.867	k4
</s>
<s>
0.868	0.868	k4
</s>
<s>
0.866	0.866	k4
</s>
<s>
0.869	0.869	k4
</s>
<s>
0.872	0.872	k4
</s>
<s>
0.872	0.872	k4
</s>
<s>
0.872	0.872	k4
</s>
<s>
27	#num#	k4
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
0.702	0.702	k4
</s>
<s>
0.724	0.724	k4
</s>
<s>
0.755	0.755	k4
</s>
<s>
0.826	0.826	k4
</s>
<s>
0.844	0.844	k4
</s>
<s>
0.848	0.848	k4
</s>
<s>
0.852	0.852	k4
</s>
<s>
0.857	0.857	k4
</s>
<s>
0.858	0.858	k4
</s>
<s>
0.864	0.864	k4
</s>
<s>
0.868	0.868	k4
</s>
<s>
0.869	0.869	k4
</s>
<s>
0.869	0.869	k4
</s>
<s>
28	#num#	k4
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.762	0.762	k4
</s>
<s>
0.806	0.806	k4
</s>
<s>
0.845	0.845	k4
</s>
<s>
0.848	0.848	k4
</s>
<s>
0.853	0.853	k4
</s>
<s>
0.856	0.856	k4
</s>
<s>
0.856	0.856	k4
</s>
<s>
0.858	0.858	k4
</s>
<s>
0.861	0.861	k4
</s>
<s>
0.861	0.861	k4
</s>
<s>
0.861	0.861	k4
</s>
<s>
29	#num#	k4
</s>
<s>
Řecko	Řecko	k1gNnSc1
</s>
<s>
0.713	0.713	k4
</s>
<s>
0.739	0.739	k4
</s>
<s>
0.749	0.749	k4
</s>
<s>
0.798	0.798	k4
</s>
<s>
0.853	0.853	k4
</s>
<s>
0.859	0.859	k4
</s>
<s>
0.857	0.857	k4
</s>
<s>
0.858	0.858	k4
</s>
<s>
0.858	0.858	k4
</s>
<s>
0.856	0.856	k4
</s>
<s>
0.854	0.854	k4
</s>
<s>
0.854	0.854	k4
</s>
<s>
0.853	0.853	k4
</s>
<s>
30	#num#	k4
</s>
<s>
Brunej	Brunat	k5eAaImRp2nS,k5eAaPmRp2nS
</s>
<s>
0.740	0.740	k4
</s>
<s>
0.757	0.757	k4
</s>
<s>
0.786	0.786	k4
</s>
<s>
0.822	0.822	k4
</s>
<s>
0.838	0.838	k4
</s>
<s>
0.843	0.843	k4
</s>
<s>
0.844	0.844	k4
</s>
<s>
0.843	0.843	k4
</s>
<s>
0.844	0.844	k4
</s>
<s>
0.844	0.844	k4
</s>
<s>
0.846	0.846	k4
</s>
<s>
0.852	0.852	k4
</s>
<s>
0.852	0.852	k4
</s>
<s>
31	#num#	k4
</s>
<s>
Katar	katar	k1gMnSc1
</s>
<s>
0.729	0.729	k4
</s>
<s>
0.753	0.753	k4
</s>
<s>
0.756	0.756	k4
</s>
<s>
0.811	0.811	k4
</s>
<s>
0.840	0.840	k4
</s>
<s>
0.846	0.846	k4
</s>
<s>
0.852	0.852	k4
</s>
<s>
0.855	0.855	k4
</s>
<s>
0.850	0.850	k4
</s>
<s>
0.847	0.847	k4
</s>
<s>
0.843	0.843	k4
</s>
<s>
0.850	0.850	k4
</s>
<s>
0.851	0.851	k4
</s>
<s>
32	#num#	k4
</s>
<s>
Kypr	Kypr	k1gInSc1
</s>
<s>
0.661	0.661	k4
</s>
<s>
0.696	0.696	k4
</s>
<s>
0.726	0.726	k4
</s>
<s>
0.800	0.800	k4
</s>
<s>
0.828	0.828	k4
</s>
<s>
0.832	0.832	k4
</s>
<s>
0.838	0.838	k4
</s>
<s>
0.844	0.844	k4
</s>
<s>
0.852	0.852	k4
</s>
<s>
0.848	0.848	k4
</s>
<s>
0.850	0.850	k4
</s>
<s>
0.848	0.848	k4
</s>
<s>
0.845	0.845	k4
</s>
<s>
33	#num#	k4
</s>
<s>
Estonsko	Estonsko	k1gNnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.730	0.730	k4
</s>
<s>
0.776	0.776	k4
</s>
<s>
0.821	0.821	k4
</s>
<s>
0.827	0.827	k4
</s>
<s>
0.832	0.832	k4
</s>
<s>
0.832	0.832	k4
</s>
<s>
0.827	0.827	k4
</s>
<s>
0.830	0.830	k4
</s>
<s>
0.836	0.836	k4
</s>
<s>
0.839	0.839	k4
</s>
<s>
0.840	0.840	k4
</s>
<s>
34	#num#	k4
</s>
<s>
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
</s>
<s>
0.583	0.583	k4
</s>
<s>
0.623	0.623	k4
</s>
<s>
0.662	0.662	k4
</s>
<s>
0.744	0.744	k4
</s>
<s>
0.773	0.773	k4
</s>
<s>
0.779	0.779	k4
</s>
<s>
0.784	0.784	k4
</s>
<s>
0.791	0.791	k4
</s>
<s>
0.802	0.802	k4
</s>
<s>
0.815	0.815	k4
</s>
<s>
0.825	0.825	k4
</s>
<s>
0.833	0.833	k4
</s>
<s>
0.836	0.836	k4
</s>
<s>
35	#num#	k4
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
0.687	0.687	k4
</s>
<s>
0.694	0.694	k4
</s>
<s>
0.714	0.714	k4
</s>
<s>
0.784	0.784	k4
</s>
<s>
0.803	0.803	k4
</s>
<s>
0.808	0.808	k4
</s>
<s>
0.812	0.812	k4
</s>
<s>
0.817	0.817	k4
</s>
<s>
0.820	0.820	k4
</s>
<s>
0.826	0.826	k4
</s>
<s>
0.830	0.830	k4
</s>
<s>
0.833	0.833	k4
</s>
<s>
0.834	0.834	k4
</s>
<s>
35	#num#	k4
</s>
<s>
Litva	Litva	k1gFnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.737	0.737	k4
</s>
<s>
0.757	0.757	k4
</s>
<s>
0.806	0.806	k4
</s>
<s>
0.814	0.814	k4
</s>
<s>
0.820	0.820	k4
</s>
<s>
0.827	0.827	k4
</s>
<s>
0.833	0.833	k4
</s>
<s>
0.829	0.829	k4
</s>
<s>
0.828	0.828	k4
</s>
<s>
0.831	0.831	k4
</s>
<s>
0.834	0.834	k4
</s>
<s>
37	#num#	k4
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.747	0.747	k4
</s>
<s>
0.776	0.776	k4
</s>
<s>
0.803	0.803	k4
</s>
<s>
0.810	0.810	k4
</s>
<s>
0.817	0.817	k4
</s>
<s>
0.824	0.824	k4
</s>
<s>
0.826	0.826	k4
</s>
<s>
0.826	0.826	k4
</s>
<s>
0.827	0.827	k4
</s>
<s>
0.829	0.829	k4
</s>
<s>
0.830	0.830	k4
</s>
<s>
37	#num#	k4
</s>
<s>
Andorra	Andorra	k1gFnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.832	0.832	k4
</s>
<s>
0.831	0.831	k4
</s>
<s>
0.830	0.830	k4
</s>
<s>
0.830	0.830	k4
</s>
<s>
39	#num#	k4
</s>
<s>
Malta	Malta	k1gFnSc1
</s>
<s>
0.704	0.704	k4
</s>
<s>
0.717	0.717	k4
</s>
<s>
0.730	0.730	k4
</s>
<s>
0.770	0.770	k4
</s>
<s>
0.801	0.801	k4
</s>
<s>
0.801	0.801	k4
</s>
<s>
0.802	0.802	k4
</s>
<s>
0.809	0.809	k4
</s>
<s>
0.818	0.818	k4
</s>
<s>
0.821	0.821	k4
</s>
<s>
0.823	0.823	k4
</s>
<s>
0.827	0.827	k4
</s>
<s>
0.829	0.829	k4
</s>
<s>
40	#num#	k4
</s>
<s>
SAR	SAR	kA
</s>
<s>
0.640	0.640	k4
</s>
<s>
0.686	0.686	k4
</s>
<s>
0.725	0.725	k4
</s>
<s>
0.797	0.797	k4
</s>
<s>
0.823	0.823	k4
</s>
<s>
0.826	0.826	k4
</s>
<s>
0.829	0.829	k4
</s>
<s>
0.832	0.832	k4
</s>
<s>
0.826	0.826	k4
</s>
<s>
0.824	0.824	k4
</s>
<s>
0.824	0.824	k4
</s>
<s>
0.825	0.825	k4
</s>
<s>
0.827	0.827	k4
</s>
<s>
41	#num#	k4
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
0.643	0.643	k4
</s>
<s>
0.674	0.674	k4
</s>
<s>
0.708	0.708	k4
</s>
<s>
0.780	0.780	k4
</s>
<s>
0.790	0.790	k4
</s>
<s>
0.794	0.794	k4
</s>
<s>
0.800	0.800	k4
</s>
<s>
0.805	0.805	k4
</s>
<s>
0.809	0.809	k4
</s>
<s>
0.816	0.816	k4
</s>
<s>
0.819	0.819	k4
</s>
<s>
0.822	0.822	k4
</s>
<s>
0.822	0.822	k4
</s>
<s>
41	#num#	k4
</s>
<s>
Chile	Chile	k1gNnSc1
</s>
<s>
0.640	0.640	k4
</s>
<s>
0.664	0.664	k4
</s>
<s>
0.704	0.704	k4
</s>
<s>
0.753	0.753	k4
</s>
<s>
0.785	0.785	k4
</s>
<s>
0.784	0.784	k4
</s>
<s>
0.792	0.792	k4
</s>
<s>
0.805	0.805	k4
</s>
<s>
0.804	0.804	k4
</s>
<s>
0.808	0.808	k4
</s>
<s>
0.815	0.815	k4
</s>
<s>
0.819	0.819	k4
</s>
<s>
0.822	0.822	k4
</s>
<s>
43	#num#	k4
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
0.696	0.696	k4
</s>
<s>
0.707	0.707	k4
</s>
<s>
0.701	0.701	k4
</s>
<s>
0.774	0.774	k4
</s>
<s>
0.805	0.805	k4
</s>
<s>
0.810	0.810	k4
</s>
<s>
0.813	0.813	k4
</s>
<s>
0.814	0.814	k4
</s>
<s>
0.816	0.816	k4
</s>
<s>
0.817	0.817	k4
</s>
<s>
0.817	0.817	k4
</s>
<s>
0.817	0.817	k4
</s>
<s>
0.818	0.818	k4
</s>
<s>
44	#num#	k4
</s>
<s>
Kuba	Kuba	k1gFnSc1
</s>
<s>
0.681	0.681	k4
</s>
<s>
0.714	0.714	k4
</s>
<s>
0.729	0.729	k4
</s>
<s>
0.742	0.742	k4
</s>
<s>
0.786	0.786	k4
</s>
<s>
0.806	0.806	k4
</s>
<s>
0.822	0.822	k4
</s>
<s>
0.830	0.830	k4
</s>
<s>
0.832	0.832	k4
</s>
<s>
0.824	0.824	k4
</s>
<s>
0.819	0.819	k4
</s>
<s>
0.813	0.813	k4
</s>
<s>
0.815	0.815	k4
</s>
<s>
44	#num#	k4
</s>
<s>
Bahrajn	Bahrajn	k1gMnSc1
</s>
<s>
0.677	0.677	k4
</s>
<s>
0.718	0.718	k4
</s>
<s>
0.729	0.729	k4
</s>
<s>
0.784	0.784	k4
</s>
<s>
0.811	0.811	k4
</s>
<s>
0.808	0.808	k4
</s>
<s>
0.809	0.809	k4
</s>
<s>
0.810	0.810	k4
</s>
<s>
0.811	0.811	k4
</s>
<s>
0.812	0.812	k4
</s>
<s>
0.812	0.812	k4
</s>
<s>
0.813	0.813	k4
</s>
<s>
0.815	0.815	k4
</s>
<s>
46	#num#	k4
</s>
<s>
Kuvajt	Kuvajt	k1gInSc1
</s>
<s>
0.702	0.702	k4
</s>
<s>
0.739	0.739	k4
</s>
<s>
0.723	0.723	k4
</s>
<s>
0.804	0.804	k4
</s>
<s>
0.795	0.795	k4
</s>
<s>
0.795	0.795	k4
</s>
<s>
0.797	0.797	k4
</s>
<s>
0.800	0.800	k4
</s>
<s>
0.804	0.804	k4
</s>
<s>
0.807	0.807	k4
</s>
<s>
0.810	0.810	k4
</s>
<s>
0.813	0.813	k4
</s>
<s>
0.814	0.814	k4
</s>
<s>
47	#num#	k4
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.689	0.689	k4
</s>
<s>
0.748	0.748	k4
</s>
<s>
0.781	0.781	k4
</s>
<s>
0.788	0.788	k4
</s>
<s>
0.796	0.796	k4
</s>
<s>
0.801	0.801	k4
</s>
<s>
0.800	0.800	k4
</s>
<s>
0.806	0.806	k4
</s>
<s>
0.812	0.812	k4
</s>
<s>
0.812	0.812	k4
</s>
<s>
0.812	0.812	k4
</s>
<s>
48	#num#	k4
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.710	0.710	k4
</s>
<s>
0.729	0.729	k4
</s>
<s>
0.786	0.786	k4
</s>
<s>
0.796	0.796	k4
</s>
<s>
0.804	0.804	k4
</s>
<s>
0.813	0.813	k4
</s>
<s>
0.814	0.814	k4
</s>
<s>
0.809	0.809	k4
</s>
<s>
0.804	0.804	k4
</s>
<s>
0.808	0.808	k4
</s>
<s>
0.810	0.810	k4
</s>
<s>
49	#num#	k4
</s>
<s>
Argentina	Argentina	k1gFnSc1
</s>
<s>
0.665	0.665	k4
</s>
<s>
0.688	0.688	k4
</s>
<s>
0.694	0.694	k4
</s>
<s>
0.753	0.753	k4
</s>
<s>
0.758	0.758	k4
</s>
<s>
0.766	0.766	k4
</s>
<s>
0.771	0.771	k4
</s>
<s>
0.777	0.777	k4
</s>
<s>
0.789	0.789	k4
</s>
<s>
0.799	0.799	k4
</s>
<s>
0.804	0.804	k4
</s>
<s>
0.806	0.806	k4
</s>
<s>
0.808	0.808	k4
</s>
<s>
50	#num#	k4
</s>
<s>
Uruguay	Uruguay	k1gFnSc1
</s>
<s>
0.658	0.658	k4
</s>
<s>
0.661	0.661	k4
</s>
<s>
0.691	0.691	k4
</s>
<s>
0.740	0.740	k4
</s>
<s>
0.755	0.755	k4
</s>
<s>
0.759	0.759	k4
</s>
<s>
0.769	0.769	k4
</s>
<s>
0.773	0.773	k4
</s>
<s>
0.774	0.774	k4
</s>
<s>
0.779	0.779	k4
</s>
<s>
0.783	0.783	k4
</s>
<s>
0.787	0.787	k4
</s>
<s>
0.790	0.790	k4
</s>
<s>
51	#num#	k4
</s>
<s>
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.750	0.750	k4
</s>
<s>
0.760	0.760	k4
</s>
<s>
0.771	0.771	k4
</s>
<s>
0.780	0.780	k4
</s>
<s>
0.780	0.780	k4
</s>
<s>
0.784	0.784	k4
</s>
<s>
0.787	0.787	k4
</s>
<s>
0.787	0.787	k4
</s>
<s>
0.789	0.789	k4
</s>
<s>
51	#num#	k4
</s>
<s>
Bahamy	Bahamy	k1gFnPc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.766	0.766	k4
</s>
<s>
0.787	0.787	k4
</s>
<s>
0.789	0.789	k4
</s>
<s>
0.791	0.791	k4
</s>
<s>
0.791	0.791	k4
</s>
<s>
0.788	0.788	k4
</s>
<s>
0.788	0.788	k4
</s>
<s>
0.789	0.789	k4
</s>
<s>
0.788	0.788	k4
</s>
<s>
0.789	0.789	k4
</s>
<s>
53	#num#	k4
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.725	0.725	k4
</s>
<s>
0.738	0.738	k4
</s>
<s>
0.751	0.751	k4
</s>
<s>
0.764	0.764	k4
</s>
<s>
0.775	0.775	k4
</s>
<s>
0.779	0.779	k4
</s>
<s>
0.784	0.784	k4
</s>
<s>
0.785	0.785	k4
</s>
<s>
0.786	0.786	k4
</s>
<s>
54	#num#	k4
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
0.685	0.685	k4
</s>
<s>
0.701	0.701	k4
</s>
<s>
0.703	0.703	k4
</s>
<s>
0.706	0.706	k4
</s>
<s>
0.750	0.750	k4
</s>
<s>
0.759	0.759	k4
</s>
<s>
0.769	0.769	k4
</s>
<s>
0.781	0.781	k4
</s>
<s>
0.781	0.781	k4
</s>
<s>
0.779	0.779	k4
</s>
<s>
0.782	0.782	k4
</s>
<s>
0.782	0.782	k4
</s>
<s>
0.785	0.785	k4
</s>
<s>
55	#num#	k4
</s>
<s>
Libye	Libye	k1gFnSc1
</s>
<s>
0.641	0.641	k4
</s>
<s>
0.654	0.654	k4
</s>
<s>
0.684	0.684	k4
</s>
<s>
0.745	0.745	k4
</s>
<s>
0.772	0.772	k4
</s>
<s>
0.778	0.778	k4
</s>
<s>
0.784	0.784	k4
</s>
<s>
0.789	0.789	k4
</s>
<s>
0.794	0.794	k4
</s>
<s>
0.799	0.799	k4
</s>
<s>
0.753	0.753	k4
</s>
<s>
0.789	0.789	k4
</s>
<s>
0.784	0.784	k4
</s>
<s>
56	#num#	k4
</s>
<s>
Omán	Omán	k1gInSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.733	0.733	k4
</s>
<s>
0.693	0.693	k4
</s>
<s>
0.701	0.701	k4
</s>
<s>
0.714	0.714	k4
</s>
<s>
0.728	0.728	k4
</s>
<s>
0.780	0.780	k4
</s>
<s>
0.781	0.781	k4
</s>
<s>
0.781	0.781	k4
</s>
<s>
0.783	0.783	k4
</s>
<s>
57	#num#	k4
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.729	0.729	k4
</s>
<s>
0.717	0.717	k4
</s>
<s>
0.750	0.750	k4
</s>
<s>
0.757	0.757	k4
</s>
<s>
0.765	0.765	k4
</s>
<s>
0.770	0.770	k4
</s>
<s>
0.770	0.770	k4
</s>
<s>
0.773	0.773	k4
</s>
<s>
0.775	0.775	k4
</s>
<s>
0.777	0.777	k4
</s>
<s>
0.778	0.778	k4
</s>
<s>
58	#num#	k4
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
0.658	0.658	k4
</s>
<s>
0.679	0.679	k4
</s>
<s>
0.696	0.696	k4
</s>
<s>
0.714	0.714	k4
</s>
<s>
0.749	0.749	k4
</s>
<s>
0.753	0.753	k4
</s>
<s>
0.759	0.759	k4
</s>
<s>
0.766	0.766	k4
</s>
<s>
0.767	0.767	k4
</s>
<s>
0.773	0.773	k4
</s>
<s>
0.774	0.774	k4
</s>
<s>
0.776	0.776	k4
</s>
<s>
0.777	0.777	k4
</s>
<s>
59	#num#	k4
</s>
<s>
Barbados	Barbados	k1gMnSc1
</s>
<s>
0.658	0.658	k4
</s>
<s>
0.688	0.688	k4
</s>
<s>
0.706	0.706	k4
</s>
<s>
0.745	0.745	k4
</s>
<s>
0.761	0.761	k4
</s>
<s>
0.764	0.764	k4
</s>
<s>
0.771	0.771	k4
</s>
<s>
0.776	0.776	k4
</s>
<s>
0.782	0.782	k4
</s>
<s>
0.779	0.779	k4
</s>
<s>
0.780	0.780	k4
</s>
<s>
0.776	0.776	k4
</s>
<s>
0.776	0.776	k4
</s>
<s>
60	#num#	k4
</s>
<s>
Palau	Palau	k6eAd1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.741	0.741	k4
</s>
<s>
0.771	0.771	k4
</s>
<s>
0.773	0.773	k4
</s>
<s>
0.773	0.773	k4
</s>
<s>
0.772	0.772	k4
</s>
<s>
0.773	0.773	k4
</s>
<s>
0.768	0.768	k4
</s>
<s>
0.770	0.770	k4
</s>
<s>
0.773	0.773	k4
</s>
<s>
0.775	0.775	k4
</s>
<s>
61	#num#	k4
</s>
<s>
Antigua	Antigua	k1gFnSc1
a	a	k8xC
Barbuda	Barbuda	k1gFnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.778	0.778	k4
</s>
<s>
0.772	0.772	k4
</s>
<s>
0.773	0.773	k4
</s>
<s>
0.774	0.774	k4
</s>
<s>
62	#num#	k4
</s>
<s>
Malajsie	Malajsie	k1gFnSc1
</s>
<s>
0.577	0.577	k4
</s>
<s>
0.619	0.619	k4
</s>
<s>
0.641	0.641	k4
</s>
<s>
0.717	0.717	k4
</s>
<s>
0.747	0.747	k4
</s>
<s>
0.752	0.752	k4
</s>
<s>
0.757	0.757	k4
</s>
<s>
0.760	0.760	k4
</s>
<s>
0.761	0.761	k4
</s>
<s>
0.766	0.766	k4
</s>
<s>
0.768	0.768	k4
</s>
<s>
0.770	0.770	k4
</s>
<s>
0.773	0.773	k4
</s>
<s>
63	#num#	k4
</s>
<s>
Mauricius	Mauricius	k1gMnSc1
</s>
<s>
0.558	0.558	k4
</s>
<s>
0.575	0.575	k4
</s>
<s>
0.621	0.621	k4
</s>
<s>
0.686	0.686	k4
</s>
<s>
0.722	0.722	k4
</s>
<s>
0.728	0.728	k4
</s>
<s>
0.735	0.735	k4
</s>
<s>
0.741	0.741	k4
</s>
<s>
0.747	0.747	k4
</s>
<s>
0.753	0.753	k4
</s>
<s>
0.759	0.759	k4
</s>
<s>
0.769	0.769	k4
</s>
<s>
0.771	0.771	k4
</s>
<s>
64	#num#	k4
</s>
<s>
Trinidad	Trinidad	k1gInSc1
a	a	k8xC
Tobago	Tobago	k1gNnSc1
</s>
<s>
0.658	0.658	k4
</s>
<s>
0.663	0.663	k4
</s>
<s>
0.658	0.658	k4
</s>
<s>
0.697	0.697	k4
</s>
<s>
0.745	0.745	k4
</s>
<s>
0.751	0.751	k4
</s>
<s>
0.759	0.759	k4
</s>
<s>
0.764	0.764	k4
</s>
<s>
0.766	0.766	k4
</s>
<s>
0.764	0.764	k4
</s>
<s>
0.764	0.764	k4
</s>
<s>
0.765	0.765	k4
</s>
<s>
0.766	0.766	k4
</s>
<s>
65	#num#	k4
</s>
<s>
Panama	Panama	k1gFnSc1
</s>
<s>
0.627	0.627	k4
</s>
<s>
0.648	0.648	k4
</s>
<s>
0.651	0.651	k4
</s>
<s>
0.709	0.709	k4
</s>
<s>
0.728	0.728	k4
</s>
<s>
0.742	0.742	k4
</s>
<s>
0.747	0.747	k4
</s>
<s>
0.752	0.752	k4
</s>
<s>
0.754	0.754	k4
</s>
<s>
0.759	0.759	k4
</s>
<s>
0.757	0.757	k4
</s>
<s>
0.761	0.761	k4
</s>
<s>
0.765	0.765	k4
</s>
<s>
65	#num#	k4
</s>
<s>
Libanon	Libanon	k1gInSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.741	0.741	k4
</s>
<s>
0.738	0.738	k4
</s>
<s>
0.745	0.745	k4
</s>
<s>
0.750	0.750	k4
</s>
<s>
0.756	0.756	k4
</s>
<s>
0.759	0.759	k4
</s>
<s>
0.764	0.764	k4
</s>
<s>
0.764	0.764	k4
</s>
<s>
0.765	0.765	k4
</s>
<s>
67	#num#	k4
</s>
<s>
Venezuela	Venezuela	k1gFnSc1
</s>
<s>
0.639	0.639	k4
</s>
<s>
0.643	0.643	k4
</s>
<s>
0.644	0.644	k4
</s>
<s>
0.677	0.677	k4
</s>
<s>
0.716	0.716	k4
</s>
<s>
0.731	0.731	k4
</s>
<s>
0.748	0.748	k4
</s>
<s>
0.758	0.758	k4
</s>
<s>
0.757	0.757	k4
</s>
<s>
0.759	0.759	k4
</s>
<s>
0.761	0.761	k4
</s>
<s>
0.763	0.763	k4
</s>
<s>
0.764	0.764	k4
</s>
<s>
68	#num#	k4
</s>
<s>
Kostarika	Kostarika	k1gFnSc1
</s>
<s>
0.605	0.605	k4
</s>
<s>
0.624	0.624	k4
</s>
<s>
0.652	0.652	k4
</s>
<s>
0.705	0.705	k4
</s>
<s>
0.721	0.721	k4
</s>
<s>
0.729	0.729	k4
</s>
<s>
0.735	0.735	k4
</s>
<s>
0.744	0.744	k4
</s>
<s>
0.746	0.746	k4
</s>
<s>
0.750	0.750	k4
</s>
<s>
0.758	0.758	k4
</s>
<s>
0.761	0.761	k4
</s>
<s>
0.763	0.763	k4
</s>
<s>
69	#num#	k4
</s>
<s>
Turecko	Turecko	k1gNnSc1
</s>
<s>
0.496	0.496	k4
</s>
<s>
0.542	0.542	k4
</s>
<s>
0.576	0.576	k4
</s>
<s>
0.653	0.653	k4
</s>
<s>
0.687	0.687	k4
</s>
<s>
0.698	0.698	k4
</s>
<s>
0.706	0.706	k4
</s>
<s>
0.710	0.710	k4
</s>
<s>
0.716	0.716	k4
</s>
<s>
0.738	0.738	k4
</s>
<s>
0.752	0.752	k4
</s>
<s>
0.756	0.756	k4
</s>
<s>
0.759	0.759	k4
</s>
<s>
70	#num#	k4
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.686	0.686	k4
</s>
<s>
0.679	0.679	k4
</s>
<s>
0.734	0.734	k4
</s>
<s>
0.740	0.740	k4
</s>
<s>
0.743	0.743	k4
</s>
<s>
0.744	0.744	k4
</s>
<s>
0.746	0.746	k4
</s>
<s>
0.747	0.747	k4
</s>
<s>
0.750	0.750	k4
</s>
<s>
0.755	0.755	k4
</s>
<s>
0.757	0.757	k4
</s>
<s>
71	#num#	k4
</s>
<s>
Seychely	Seychely	k1gFnPc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.743	0.743	k4
</s>
<s>
0.757	0.757	k4
</s>
<s>
0.760	0.760	k4
</s>
<s>
0.763	0.763	k4
</s>
<s>
0.766	0.766	k4
</s>
<s>
0.744	0.744	k4
</s>
<s>
0.763	0.763	k4
</s>
<s>
0.749	0.749	k4
</s>
<s>
0.755	0.755	k4
</s>
<s>
0.756	0.756	k4
</s>
<s>
71	#num#	k4
</s>
<s>
Mexico	Mexico	k6eAd1
</s>
<s>
0.595	0.595	k4
</s>
<s>
0.631	0.631	k4
</s>
<s>
0.647	0.647	k4
</s>
<s>
0.699	0.699	k4
</s>
<s>
0.724	0.724	k4
</s>
<s>
0.732	0.732	k4
</s>
<s>
0.735	0.735	k4
</s>
<s>
0.739	0.739	k4
</s>
<s>
0.741	0.741	k4
</s>
<s>
0.748	0.748	k4
</s>
<s>
0.752	0.752	k4
</s>
<s>
0.755	0.755	k4
</s>
<s>
0.756	0.756	k4
</s>
<s>
73	#num#	k4
</s>
<s>
Srí	Srí	k?
Lanka	lanko	k1gNnPc1
</s>
<s>
0.569	0.569	k4
</s>
<s>
0.598	0.598	k4
</s>
<s>
0.620	0.620	k4
</s>
<s>
0.679	0.679	k4
</s>
<s>
0.710	0.710	k4
</s>
<s>
0.716	0.716	k4
</s>
<s>
0.721	0.721	k4
</s>
<s>
0.725	0.725	k4
</s>
<s>
0.728	0.728	k4
</s>
<s>
0.736	0.736	k4
</s>
<s>
0.740	0.740	k4
</s>
<s>
0.745	0.745	k4
</s>
<s>
0.750	0.750	k4
</s>
<s>
73	#num#	k4
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Kryštof	Kryštof	k1gMnSc1
a	a	k8xC
Nevis	viset	k5eNaImRp2nS
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.752	0.752	k4
</s>
<s>
0.747	0.747	k4
</s>
<s>
0.745	0.745	k4
</s>
<s>
0.749	0.749	k4
</s>
<s>
0.750	0.750	k4
</s>
<s>
75	#num#	k4
</s>
<s>
Írán	Írán	k1gInSc1
</s>
<s>
0.490	0.490	k4
</s>
<s>
0.502	0.502	k4
</s>
<s>
0.552	0.552	k4
</s>
<s>
0.652	0.652	k4
</s>
<s>
0.681	0.681	k4
</s>
<s>
0.691	0.691	k4
</s>
<s>
0.703	0.703	k4
</s>
<s>
0.711	0.711	k4
</s>
<s>
0.718	0.718	k4
</s>
<s>
0.725	0.725	k4
</s>
<s>
0.733	0.733	k4
</s>
<s>
0.749	0.749	k4
</s>
<s>
0.749	0.749	k4
</s>
<s>
76	#num#	k4
</s>
<s>
Ázerbájdžán	Ázerbájdžán	k1gInSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.639	0.639	k4
</s>
<s>
0.686	0.686	k4
</s>
<s>
0.703	0.703	k4
</s>
<s>
0.715	0.715	k4
</s>
<s>
0.724	0.724	k4
</s>
<s>
0.736	0.736	k4
</s>
<s>
0.743	0.743	k4
</s>
<s>
0.743	0.743	k4
</s>
<s>
0.745	0.745	k4
</s>
<s>
0.747	0.747	k4
</s>
<s>
77	#num#	k4
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.726	0.726	k4
</s>
<s>
0.713	0.713	k4
</s>
<s>
0.732	0.732	k4
</s>
<s>
0.735	0.735	k4
</s>
<s>
0.739	0.739	k4
</s>
<s>
0.743	0.743	k4
</s>
<s>
0.742	0.742	k4
</s>
<s>
0.743	0.743	k4
</s>
<s>
0.744	0.744	k4
</s>
<s>
0.743	0.743	k4
</s>
<s>
0.745	0.745	k4
</s>
<s>
77	#num#	k4
</s>
<s>
Jordánsko	Jordánsko	k1gNnSc1
</s>
<s>
0.587	0.587	k4
</s>
<s>
0.616	0.616	k4
</s>
<s>
0.622	0.622	k4
</s>
<s>
0.705	0.705	k4
</s>
<s>
0.733	0.733	k4
</s>
<s>
0.736	0.736	k4
</s>
<s>
0.741	0.741	k4
</s>
<s>
0.746	0.746	k4
</s>
<s>
0.746	0.746	k4
</s>
<s>
0.744	0.744	k4
</s>
<s>
0.744	0.744	k4
</s>
<s>
0.744	0.744	k4
</s>
<s>
0.745	0.745	k4
</s>
<s>
79	#num#	k4
</s>
<s>
Grenada	Grenada	k1gFnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.746	0.746	k4
</s>
<s>
0.747	0.747	k4
</s>
<s>
0.743	0.743	k4
</s>
<s>
0.744	0.744	k4
</s>
<s>
79	#num#	k4
</s>
<s>
Gruzie	Gruzie	k1gFnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.710	0.710	k4
</s>
<s>
0.714	0.714	k4
</s>
<s>
0.726	0.726	k4
</s>
<s>
0.730	0.730	k4
</s>
<s>
0.735	0.735	k4
</s>
<s>
0.733	0.733	k4
</s>
<s>
0.736	0.736	k4
</s>
<s>
0.741	0.741	k4
</s>
<s>
0.744	0.744	k4
</s>
<s>
79	#num#	k4
</s>
<s>
Brazílie	Brazílie	k1gFnSc1
</s>
<s>
0.545	0.545	k4
</s>
<s>
0.575	0.575	k4
</s>
<s>
0.612	0.612	k4
</s>
<s>
0.682	0.682	k4
</s>
<s>
0.705	0.705	k4
</s>
<s>
0.713	0.713	k4
</s>
<s>
0.721	0.721	k4
</s>
<s>
0.731	0.731	k4
</s>
<s>
0.732	0.732	k4
</s>
<s>
0.739	0.739	k4
</s>
<s>
0.740	0.740	k4
</s>
<s>
0.742	0.742	k4
</s>
<s>
0.744	0.744	k4
</s>
<s>
82	#num#	k4
</s>
<s>
Peru	Peru	k1gNnSc1
</s>
<s>
0.595	0.595	k4
</s>
<s>
0.616	0.616	k4
</s>
<s>
0.615	0.615	k4
</s>
<s>
0.682	0.682	k4
</s>
<s>
0.694	0.694	k4
</s>
<s>
0.696	0.696	k4
</s>
<s>
0.700	0.700	k4
</s>
<s>
0.707	0.707	k4
</s>
<s>
0.709	0.709	k4
</s>
<s>
0.722	0.722	k4
</s>
<s>
0.727	0.727	k4
</s>
<s>
0.734	0.734	k4
</s>
<s>
0.737	0.737	k4
</s>
<s>
83	#num#	k4
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.705	0.705	k4
</s>
<s>
0.668	0.668	k4
</s>
<s>
0.713	0.713	k4
</s>
<s>
0.720	0.720	k4
</s>
<s>
0.726	0.726	k4
</s>
<s>
0.729	0.729	k4
</s>
<s>
0.722	0.722	k4
</s>
<s>
0.726	0.726	k4
</s>
<s>
0.730	0.730	k4
</s>
<s>
0.733	0.733	k4
</s>
<s>
0.734	0.734	k4
</s>
<s>
84	#num#	k4
</s>
<s>
Makedonie	Makedonie	k1gFnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.699	0.699	k4
</s>
<s>
0.705	0.705	k4
</s>
<s>
0.708	0.708	k4
</s>
<s>
0.724	0.724	k4
</s>
<s>
0.725	0.725	k4
</s>
<s>
0.728	0.728	k4
</s>
<s>
0.730	0.730	k4
</s>
<s>
0.730	0.730	k4
</s>
<s>
0.732	0.732	k4
</s>
<s>
84	#num#	k4
</s>
<s>
Belize	Belize	k6eAd1
</s>
<s>
0.619	0.619	k4
</s>
<s>
0.620	0.620	k4
</s>
<s>
0.640	0.640	k4
</s>
<s>
0.675	0.675	k4
</s>
<s>
0.710	0.710	k4
</s>
<s>
0.708	0.708	k4
</s>
<s>
0.707	0.707	k4
</s>
<s>
0.710	0.710	k4
</s>
<s>
0.712	0.712	k4
</s>
<s>
0.714	0.714	k4
</s>
<s>
0.717	0.717	k4
</s>
<s>
0.731	0.731	k4
</s>
<s>
0.732	0.732	k4
</s>
<s>
86	#num#	k4
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.716	0.716	k4
</s>
<s>
0.719	0.719	k4
</s>
<s>
0.722	0.722	k4
</s>
<s>
0.727	0.727	k4
</s>
<s>
0.725	0.725	k4
</s>
<s>
0.726	0.726	k4
</s>
<s>
0.729	0.729	k4
</s>
<s>
0.729	0.729	k4
</s>
<s>
0.731	0.731	k4
</s>
<s>
87	#num#	k4
</s>
<s>
Arménie	Arménie	k1gFnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.632	0.632	k4
</s>
<s>
0.648	0.648	k4
</s>
<s>
0.693	0.693	k4
</s>
<s>
0.707	0.707	k4
</s>
<s>
0.721	0.721	k4
</s>
<s>
0.722	0.722	k4
</s>
<s>
0.717	0.717	k4
</s>
<s>
0.720	0.720	k4
</s>
<s>
0.724	0.724	k4
</s>
<s>
0.728	0.728	k4
</s>
<s>
0.730	0.730	k4
</s>
<s>
88	#num#	k4
</s>
<s>
Fidži	Fidzat	k5eAaPmIp1nS
</s>
<s>
0.587	0.587	k4
</s>
<s>
0.598	0.598	k4
</s>
<s>
0.619	0.619	k4
</s>
<s>
0.674	0.674	k4
</s>
<s>
0.694	0.694	k4
</s>
<s>
0.704	0.704	k4
</s>
<s>
0.707	0.707	k4
</s>
<s>
0.712	0.712	k4
</s>
<s>
0.717	0.717	k4
</s>
<s>
0.721	0.721	k4
</s>
<s>
0.722	0.722	k4
</s>
<s>
0.722	0.722	k4
</s>
<s>
0.724	0.724	k4
</s>
<s>
89	#num#	k4
</s>
<s>
Thajsko	Thajsko	k1gNnSc1
</s>
<s>
0.503	0.503	k4
</s>
<s>
0.542	0.542	k4
</s>
<s>
0.572	0.572	k4
</s>
<s>
0.649	0.649	k4
</s>
<s>
0.685	0.685	k4
</s>
<s>
0.685	0.685	k4
</s>
<s>
0.698	0.698	k4
</s>
<s>
0.704	0.704	k4
</s>
<s>
0.708	0.708	k4
</s>
<s>
0.715	0.715	k4
</s>
<s>
0.716	0.716	k4
</s>
<s>
0.720	0.720	k4
</s>
<s>
0.722	0.722	k4
</s>
<s>
90	#num#	k4
</s>
<s>
Tunisko	Tunisko	k1gNnSc1
</s>
<s>
0.484	0.484	k4
</s>
<s>
0.537	0.537	k4
</s>
<s>
0.567	0.567	k4
</s>
<s>
0.653	0.653	k4
</s>
<s>
0.687	0.687	k4
</s>
<s>
0.694	0.694	k4
</s>
<s>
0.700	0.700	k4
</s>
<s>
0.706	0.706	k4
</s>
<s>
0.710	0.710	k4
</s>
<s>
0.715	0.715	k4
</s>
<s>
0.716	0.716	k4
</s>
<s>
0.719	0.719	k4
</s>
<s>
0.721	0.721	k4
</s>
<s>
91	#num#	k4
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Vincenc	Vincenc	k1gMnSc1
a	a	k8xC
Grenadiny	grenadina	k1gFnPc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.717	0.717	k4
</s>
<s>
0.717	0.717	k4
</s>
<s>
0.715	0.715	k4
</s>
<s>
0.717	0.717	k4
</s>
<s>
0.719	0.719	k4
</s>
<s>
91	#num#	k4
</s>
<s>
Čína	Čína	k1gFnSc1
</s>
<s>
0.423	0.423	k4
</s>
<s>
0.457	0.457	k4
</s>
<s>
0.502	0.502	k4
</s>
<s>
0.591	0.591	k4
</s>
<s>
0.645	0.645	k4
</s>
<s>
0.657	0.657	k4
</s>
<s>
0.671	0.671	k4
</s>
<s>
0.682	0.682	k4
</s>
<s>
0.693	0.693	k4
</s>
<s>
0.701	0.701	k4
</s>
<s>
0.710	0.710	k4
</s>
<s>
0.715	0.715	k4
</s>
<s>
0.719	0.719	k4
</s>
<s>
93	#num#	k4
</s>
<s>
Dominika	Dominik	k1gMnSc4
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.691	0.691	k4
</s>
<s>
0.708	0.708	k4
</s>
<s>
0.708	0.708	k4
</s>
<s>
0.712	0.712	k4
</s>
<s>
0.712	0.712	k4
</s>
<s>
0.717	0.717	k4
</s>
<s>
0.717	0.717	k4
</s>
<s>
0.718	0.718	k4
</s>
<s>
0.716	0.716	k4
</s>
<s>
0.717	0.717	k4
</s>
<s>
93	#num#	k4
</s>
<s>
Alžírsko	Alžírsko	k1gNnSc1
</s>
<s>
0.509	0.509	k4
</s>
<s>
0.551	0.551	k4
</s>
<s>
0.576	0.576	k4
</s>
<s>
0.634	0.634	k4
</s>
<s>
0.675	0.675	k4
</s>
<s>
0.680	0.680	k4
</s>
<s>
0.686	0.686	k4
</s>
<s>
0.695	0.695	k4
</s>
<s>
0.703	0.703	k4
</s>
<s>
0.709	0.709	k4
</s>
<s>
0.715	0.715	k4
</s>
<s>
0.715	0.715	k4
</s>
<s>
0.717	0.717	k4
</s>
<s>
95	#num#	k4
</s>
<s>
Albánie	Albánie	k1gFnSc1
</s>
<s>
0.603	0.603	k4
</s>
<s>
0.602	0.602	k4
</s>
<s>
0.609	0.609	k4
</s>
<s>
0.655	0.655	k4
</s>
<s>
0.689	0.689	k4
</s>
<s>
0.694	0.694	k4
</s>
<s>
0.699	0.699	k4
</s>
<s>
0.703	0.703	k4
</s>
<s>
0.705	0.705	k4
</s>
<s>
0.708	0.708	k4
</s>
<s>
0.714	0.714	k4
</s>
<s>
0.714	0.714	k4
</s>
<s>
0.716	0.716	k4
</s>
<s>
96	#num#	k4
</s>
<s>
Jamajka	Jamajka	k1gFnSc1
</s>
<s>
0.614	0.614	k4
</s>
<s>
0.616	0.616	k4
</s>
<s>
0.638	0.638	k4
</s>
<s>
0.671	0.671	k4
</s>
<s>
0.700	0.700	k4
</s>
<s>
0.702	0.702	k4
</s>
<s>
0.707	0.707	k4
</s>
<s>
0.710	0.710	k4
</s>
<s>
0.709	0.709	k4
</s>
<s>
0.712	0.712	k4
</s>
<s>
0.714	0.714	k4
</s>
<s>
0.715	0.715	k4
</s>
<s>
0.715	0.715	k4
</s>
<s>
97	#num#	k4
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1
Lucie	Lucie	k1gFnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.720	0.720	k4
</s>
<s>
0.717	0.717	k4
</s>
<s>
0.718	0.718	k4
</s>
<s>
0.715	0.715	k4
</s>
<s>
0.714	0.714	k4
</s>
<s>
98	#num#	k4
</s>
<s>
Ekvádor	Ekvádor	k1gInSc1
</s>
<s>
0.605	0.605	k4
</s>
<s>
0.627	0.627	k4
</s>
<s>
0.643	0.643	k4
</s>
<s>
0.658	0.658	k4
</s>
<s>
0.687	0.687	k4
</s>
<s>
0.690	0.690	k4
</s>
<s>
0.692	0.692	k4
</s>
<s>
0.697	0.697	k4
</s>
<s>
0.698	0.698	k4
</s>
<s>
0.701	0.701	k4
</s>
<s>
0.705	0.705	k4
</s>
<s>
0.708	0.708	k4
</s>
<s>
0.711	0.711	k4
</s>
<s>
98	#num#	k4
</s>
<s>
Kolumbie	Kolumbie	k1gFnSc1
</s>
<s>
0.557	0.557	k4
</s>
<s>
0.574	0.574	k4
</s>
<s>
0.596	0.596	k4
</s>
<s>
0.655	0.655	k4
</s>
<s>
0.680	0.680	k4
</s>
<s>
0.685	0.685	k4
</s>
<s>
0.693	0.693	k4
</s>
<s>
0.700	0.700	k4
</s>
<s>
0.703	0.703	k4
</s>
<s>
0.706	0.706	k4
</s>
<s>
0.710	0.710	k4
</s>
<s>
0.708	0.708	k4
</s>
<s>
0.711	0.711	k4
</s>
<s>
100	#num#	k4
</s>
<s>
Tonga	Tonga	k1gFnSc1
</s>
<s>
0.602	0.602	k4
</s>
<s>
0.617	0.617	k4
</s>
<s>
0.631	0.631	k4
</s>
<s>
0.672	0.672	k4
</s>
<s>
0.695	0.695	k4
</s>
<s>
0.696	0.696	k4
</s>
<s>
0.697	0.697	k4
</s>
<s>
0.696	0.696	k4
</s>
<s>
0.698	0.698	k4
</s>
<s>
0.701	0.701	k4
</s>
<s>
0.702	0.702	k4
</s>
<s>
0.704	0.704	k4
</s>
<s>
0.705	0.705	k4
</s>
<s>
100	#num#	k4
</s>
<s>
Surinam	Surinam	k6eAd1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.672	0.672	k4
</s>
<s>
0.676	0.676	k4
</s>
<s>
0.690	0.690	k4
</s>
<s>
0.694	0.694	k4
</s>
<s>
0.696	0.696	k4
</s>
<s>
0.698	0.698	k4
</s>
<s>
0.701	0.701	k4
</s>
<s>
0.702	0.702	k4
</s>
<s>
0.705	0.705	k4
</s>
<s>
102	#num#	k4
</s>
<s>
Dominikánská	dominikánský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
0.527	0.527	k4
</s>
<s>
0.563	0.563	k4
</s>
<s>
0.589	0.589	k4
</s>
<s>
0.645	0.645	k4
</s>
<s>
0.668	0.668	k4
</s>
<s>
0.675	0.675	k4
</s>
<s>
0.681	0.681	k4
</s>
<s>
0.684	0.684	k4
</s>
<s>
0.686	0.686	k4
</s>
<s>
0.691	0.691	k4
</s>
<s>
0.695	0.695	k4
</s>
<s>
0.698	0.698	k4
</s>
<s>
0.700	0.700	k4
</s>
<s>
103	#num#	k4
</s>
<s>
Turkmenistán	Turkmenistán	k1gInSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.687	0.687	k4
</s>
<s>
0.690	0.690	k4
</s>
<s>
0.693	0.693	k4
</s>
<s>
0.698	0.698	k4
</s>
<s>
103	#num#	k4
</s>
<s>
Mongolsko	Mongolsko	k1gNnSc1
</s>
<s>
0.515	0.515	k4
</s>
<s>
0.523	0.523	k4
</s>
<s>
0.552	0.552	k4
</s>
<s>
0.580	0.580	k4
</s>
<s>
0.637	0.637	k4
</s>
<s>
0.646	0.646	k4
</s>
<s>
0.655	0.655	k4
</s>
<s>
0.665	0.665	k4
</s>
<s>
0.668	0.668	k4
</s>
<s>
0.671	0.671	k4
</s>
<s>
0.682	0.682	k4
</s>
<s>
0.692	0.692	k4
</s>
<s>
0.698	0.698	k4
</s>
<s>
103	#num#	k4
</s>
<s>
Maledivy	Maledivy	k1gFnPc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.599	0.599	k4
</s>
<s>
0.659	0.659	k4
</s>
<s>
0.661	0.661	k4
</s>
<s>
0.674	0.674	k4
</s>
<s>
0.675	0.675	k4
</s>
<s>
0.686	0.686	k4
</s>
<s>
0.688	0.688	k4
</s>
<s>
0.692	0.692	k4
</s>
<s>
0.695	0.695	k4
</s>
<s>
0.698	0.698	k4
</s>
<s>
106	#num#	k4
</s>
<s>
Samoa	Samoa	k1gFnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.654	0.654	k4
</s>
<s>
0.681	0.681	k4
</s>
<s>
0.681	0.681	k4
</s>
<s>
0.684	0.684	k4
</s>
<s>
0.683	0.683	k4
</s>
<s>
0.689	0.689	k4
</s>
<s>
0.688	0.688	k4
</s>
<s>
0.690	0.690	k4
</s>
<s>
0.693	0.693	k4
</s>
<s>
0.694	0.694	k4
</s>
<s>
107	#num#	k4
</s>
<s>
Palestina	Palestina	k1gFnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.649	0.649	k4
</s>
<s>
0.658	0.658	k4
</s>
<s>
0.668	0.668	k4
</s>
<s>
0.672	0.672	k4
</s>
<s>
0.679	0.679	k4
</s>
<s>
0.671	0.671	k4
</s>
<s>
0.679	0.679	k4
</s>
<s>
0.683	0.683	k4
</s>
<s>
0.686	0.686	k4
</s>
<s>
108	#num#	k4
</s>
<s>
Indonésie	Indonésie	k1gFnSc1
</s>
<s>
0.471	0.471	k4
</s>
<s>
0.510	0.510	k4
</s>
<s>
0.528	0.528	k4
</s>
<s>
0.609	0.609	k4
</s>
<s>
0.640	0.640	k4
</s>
<s>
0.645	0.645	k4
</s>
<s>
0.650	0.650	k4
</s>
<s>
0.654	0.654	k4
</s>
<s>
0.665	0.665	k4
</s>
<s>
0.671	0.671	k4
</s>
<s>
0.678	0.678	k4
</s>
<s>
0.681	0.681	k4
</s>
<s>
0.684	0.684	k4
</s>
<s>
109	#num#	k4
</s>
<s>
Botswana	Botswana	k1gFnSc1
</s>
<s>
0.470	0.470	k4
</s>
<s>
0.528	0.528	k4
</s>
<s>
0.583	0.583	k4
</s>
<s>
0.560	0.560	k4
</s>
<s>
0.610	0.610	k4
</s>
<s>
0.625	0.625	k4
</s>
<s>
0.643	0.643	k4
</s>
<s>
0.656	0.656	k4
</s>
<s>
0.662	0.662	k4
</s>
<s>
0.672	0.672	k4
</s>
<s>
0.678	0.678	k4
</s>
<s>
0.681	0.681	k4
</s>
<s>
0.683	0.683	k4
</s>
<s>
110	#num#	k4
</s>
<s>
Egypt	Egypt	k1gInSc1
</s>
<s>
0.452	0.452	k4
</s>
<s>
0.501	0.501	k4
</s>
<s>
0.546	0.546	k4
</s>
<s>
0.621	0.621	k4
</s>
<s>
0.645	0.645	k4
</s>
<s>
0.652	0.652	k4
</s>
<s>
0.659	0.659	k4
</s>
<s>
0.667	0.667	k4
</s>
<s>
0.672	0.672	k4
</s>
<s>
0.678	0.678	k4
</s>
<s>
0.679	0.679	k4
</s>
<s>
0.681	0.681	k4
</s>
<s>
0.682	0.682	k4
</s>
<s>
111	#num#	k4
</s>
<s>
Paraguay	Paraguay	k1gFnSc1
</s>
<s>
0.550	0.550	k4
</s>
<s>
0.567	0.567	k4
</s>
<s>
0.581	0.581	k4
</s>
<s>
0.625	0.625	k4
</s>
<s>
0.648	0.648	k4
</s>
<s>
0.648	0.648	k4
</s>
<s>
0.653	0.653	k4
</s>
<s>
0.661	0.661	k4
</s>
<s>
0.661	0.661	k4
</s>
<s>
0.669	0.669	k4
</s>
<s>
0.672	0.672	k4
</s>
<s>
0.670	0.670	k4
</s>
<s>
0.676	0.676	k4
</s>
<s>
112	#num#	k4
</s>
<s>
Gabon	Gabon	k1gMnSc1
</s>
<s>
0.540	0.540	k4
</s>
<s>
0.590	0.590	k4
</s>
<s>
0.619	0.619	k4
</s>
<s>
0.632	0.632	k4
</s>
<s>
0.644	0.644	k4
</s>
<s>
0.645	0.645	k4
</s>
<s>
0.651	0.651	k4
</s>
<s>
0.654	0.654	k4
</s>
<s>
0.659	0.659	k4
</s>
<s>
0.662	0.662	k4
</s>
<s>
0.666	0.666	k4
</s>
<s>
0.670	0.670	k4
</s>
<s>
0.674	0.674	k4
</s>
<s>
113	#num#	k4
</s>
<s>
Bolívie	Bolívie	k1gFnSc1
</s>
<s>
0.494	0.494	k4
</s>
<s>
0.510	0.510	k4
</s>
<s>
0.554	0.554	k4
</s>
<s>
0.615	0.615	k4
</s>
<s>
0.636	0.636	k4
</s>
<s>
0.640	0.640	k4
</s>
<s>
0.643	0.643	k4
</s>
<s>
0.649	0.649	k4
</s>
<s>
0.653	0.653	k4
</s>
<s>
0.658	0.658	k4
</s>
<s>
0.661	0.661	k4
</s>
<s>
0.663	0.663	k4
</s>
<s>
0.667	0.667	k4
</s>
<s>
114	#num#	k4
</s>
<s>
Moldava	Moldava	k1gFnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.645	0.645	k4
</s>
<s>
0.598	0.598	k4
</s>
<s>
0.639	0.639	k4
</s>
<s>
0.645	0.645	k4
</s>
<s>
0.646	0.646	k4
</s>
<s>
0.652	0.652	k4
</s>
<s>
0.646	0.646	k4
</s>
<s>
0.652	0.652	k4
</s>
<s>
0.656	0.656	k4
</s>
<s>
0.657	0.657	k4
</s>
<s>
0.663	0.663	k4
</s>
<s>
115	#num#	k4
</s>
<s>
Salvador	Salvador	k1gMnSc1
</s>
<s>
0.517	0.517	k4
</s>
<s>
0.524	0.524	k4
</s>
<s>
0.529	0.529	k4
</s>
<s>
0.607	0.607	k4
</s>
<s>
0.640	0.640	k4
</s>
<s>
0.644	0.644	k4
</s>
<s>
0.646	0.646	k4
</s>
<s>
0.648	0.648	k4
</s>
<s>
0.649	0.649	k4
</s>
<s>
0.652	0.652	k4
</s>
<s>
0.657	0.657	k4
</s>
<s>
0.660	0.660	k4
</s>
<s>
0.662	0.662	k4
</s>
<s>
116	#num#	k4
</s>
<s>
Uzbekistán	Uzbekistán	k1gInSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.626	0.626	k4
</s>
<s>
0.630	0.630	k4
</s>
<s>
0.635	0.635	k4
</s>
<s>
0.643	0.643	k4
</s>
<s>
0.645	0.645	k4
</s>
<s>
0.648	0.648	k4
</s>
<s>
0.653	0.653	k4
</s>
<s>
0.657	0.657	k4
</s>
<s>
0.661	0.661	k4
</s>
<s>
117	#num#	k4
</s>
<s>
Filipíny	Filipíny	k1gFnPc4
</s>
<s>
0.566	0.566	k4
</s>
<s>
0.578	0.578	k4
</s>
<s>
0.591	0.591	k4
</s>
<s>
0.619	0.619	k4
</s>
<s>
0.638	0.638	k4
</s>
<s>
0.638	0.638	k4
</s>
<s>
0.644	0.644	k4
</s>
<s>
0.648	0.648	k4
</s>
<s>
0.647	0.647	k4
</s>
<s>
0.651	0.651	k4
</s>
<s>
0.652	0.652	k4
</s>
<s>
0.656	0.656	k4
</s>
<s>
0.660	0.660	k4
</s>
<s>
118	#num#	k4
</s>
<s>
Sýrie	Sýrie	k1gFnSc1
</s>
<s>
0.528	0.528	k4
</s>
<s>
0.564	0.564	k4
</s>
<s>
0.570	0.570	k4
</s>
<s>
0.605	0.605	k4
</s>
<s>
0.653	0.653	k4
</s>
<s>
..	..	k?
</s>
<s>
0.658	0.658	k4
</s>
<s>
0.658	0.658	k4
</s>
<s>
0.662	0.662	k4
</s>
<s>
0.662	0.662	k4
</s>
<s>
0.662	0.662	k4
</s>
<s>
0.662	0.662	k4
</s>
<s>
0.658	0.658	k4
</s>
<s>
118	#num#	k4
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
0.569	0.569	k4
</s>
<s>
0.584	0.584	k4
</s>
<s>
0.619	0.619	k4
</s>
<s>
0.628	0.628	k4
</s>
<s>
0.608	0.608	k4
</s>
<s>
0.611	0.611	k4
</s>
<s>
0.617	0.617	k4
</s>
<s>
0.623	0.623	k4
</s>
<s>
0.631	0.631	k4
</s>
<s>
0.638	0.638	k4
</s>
<s>
0.646	0.646	k4
</s>
<s>
0.654	0.654	k4
</s>
<s>
0.658	0.658	k4
</s>
<s>
120	#num#	k4
</s>
<s>
Irák	Irák	k1gInSc1
</s>
<s>
0.500	0.500	k4
</s>
<s>
0.481	0.481	k4
</s>
<s>
0.508	0.508	k4
</s>
<s>
0.606	0.606	k4
</s>
<s>
0.621	0.621	k4
</s>
<s>
0.636	0.636	k4
</s>
<s>
0.632	0.632	k4
</s>
<s>
0.632	0.632	k4
</s>
<s>
0.637	0.637	k4
</s>
<s>
0.638	0.638	k4
</s>
<s>
0.639	0.639	k4
</s>
<s>
0.641	0.641	k4
</s>
<s>
0.642	0.642	k4
</s>
<s>
121	#num#	k4
</s>
<s>
Vietnam	Vietnam	k1gInSc1
</s>
<s>
0.463	0.463	k4
</s>
<s>
0.473	0.473	k4
</s>
<s>
0.476	0.476	k4
</s>
<s>
0.563	0.563	k4
</s>
<s>
0.598	0.598	k4
</s>
<s>
0.604	0.604	k4
</s>
<s>
0.611	0.611	k4
</s>
<s>
0.617	0.617	k4
</s>
<s>
0.622	0.622	k4
</s>
<s>
0.629	0.629	k4
</s>
<s>
0.632	0.632	k4
</s>
<s>
0.635	0.635	k4
</s>
<s>
0.638	0.638	k4
</s>
<s>
121	#num#	k4
</s>
<s>
Guyana	Guyana	k1gFnSc1
</s>
<s>
0.516	0.516	k4
</s>
<s>
0.505	0.505	k4
</s>
<s>
0.505	0.505	k4
</s>
<s>
0.570	0.570	k4
</s>
<s>
0.584	0.584	k4
</s>
<s>
0.589	0.589	k4
</s>
<s>
0.616	0.616	k4
</s>
<s>
0.621	0.621	k4
</s>
<s>
0.623	0.623	k4
</s>
<s>
0.626	0.626	k4
</s>
<s>
0.632	0.632	k4
</s>
<s>
0.635	0.635	k4
</s>
<s>
0.638	0.638	k4
</s>
<s>
123	#num#	k4
</s>
<s>
Kapverdy	Kapverdy	k6eAd1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.573	0.573	k4
</s>
<s>
0.589	0.589	k4
</s>
<s>
0.602	0.602	k4
</s>
<s>
0.607	0.607	k4
</s>
<s>
0.613	0.613	k4
</s>
<s>
0.617	0.617	k4
</s>
<s>
0.622	0.622	k4
</s>
<s>
0.631	0.631	k4
</s>
<s>
0.635	0.635	k4
</s>
<s>
0.636	0.636	k4
</s>
<s>
124	#num#	k4
</s>
<s>
Mikronésie	Mikronésie	k1gFnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.627	0.627	k4
</s>
<s>
0.627	0.627	k4
</s>
<s>
0.629	0.629	k4
</s>
<s>
0.630	0.630	k4
</s>
<s>
125	#num#	k4
</s>
<s>
Kyrgyzstán	Kyrgyzstán	k1gInSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.607	0.607	k4
</s>
<s>
0.586	0.586	k4
</s>
<s>
0.605	0.605	k4
</s>
<s>
0.609	0.609	k4
</s>
<s>
0.614	0.614	k4
</s>
<s>
0.617	0.617	k4
</s>
<s>
0.617	0.617	k4
</s>
<s>
0.614	0.614	k4
</s>
<s>
0.618	0.618	k4
</s>
<s>
0.621	0.621	k4
</s>
<s>
0.628	0.628	k4
</s>
<s>
125	#num#	k4
</s>
<s>
Guatemala	Guatemala	k1gFnSc1
</s>
<s>
0.445	0.445	k4
</s>
<s>
0.457	0.457	k4
</s>
<s>
0.483	0.483	k4
</s>
<s>
0.551	0.551	k4
</s>
<s>
0.576	0.576	k4
</s>
<s>
0.583	0.583	k4
</s>
<s>
0.595	0.595	k4
</s>
<s>
0.601	0.601	k4
</s>
<s>
0.606	0.606	k4
</s>
<s>
0.613	0.613	k4
</s>
<s>
0.620	0.620	k4
</s>
<s>
0.626	0.626	k4
</s>
<s>
0.628	0.628	k4
</s>
<s>
127	#num#	k4
</s>
<s>
Namibie	Namibie	k1gFnSc1
</s>
<s>
0.550	0.550	k4
</s>
<s>
0.564	0.564	k4
</s>
<s>
0.577	0.577	k4
</s>
<s>
0.556	0.556	k4
</s>
<s>
0.570	0.570	k4
</s>
<s>
0.580	0.580	k4
</s>
<s>
0.589	0.589	k4
</s>
<s>
0.598	0.598	k4
</s>
<s>
0.603	0.603	k4
</s>
<s>
0.610	0.610	k4
</s>
<s>
0.616	0.616	k4
</s>
<s>
0.620	0.620	k4
</s>
<s>
0.624	0.624	k4
</s>
<s>
128	#num#	k4
</s>
<s>
Východní	východní	k2eAgInSc1d1
Timor	Timor	k1gInSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.465	0.465	k4
</s>
<s>
0.505	0.505	k4
</s>
<s>
0.521	0.521	k4
</s>
<s>
0.552	0.552	k4
</s>
<s>
0.579	0.579	k4
</s>
<s>
0.599	0.599	k4
</s>
<s>
0.606	0.606	k4
</s>
<s>
0.606	0.606	k4
</s>
<s>
0.616	0.616	k4
</s>
<s>
0.620	0.620	k4
</s>
<s>
129	#num#	k4
</s>
<s>
Maroko	Maroko	k1gNnSc1
</s>
<s>
0.399	0.399	k4
</s>
<s>
0.436	0.436	k4
</s>
<s>
0.459	0.459	k4
</s>
<s>
0.526	0.526	k4
</s>
<s>
0.569	0.569	k4
</s>
<s>
0.575	0.575	k4
</s>
<s>
0.582	0.582	k4
</s>
<s>
0.588	0.588	k4
</s>
<s>
0.594	0.594	k4
</s>
<s>
0.603	0.603	k4
</s>
<s>
0.612	0.612	k4
</s>
<s>
0.614	0.614	k4
</s>
<s>
0.617	0.617	k4
</s>
<s>
129	#num#	k4
</s>
<s>
Honduras	Honduras	k1gInSc1
</s>
<s>
0.461	0.461	k4
</s>
<s>
0.490	0.490	k4
</s>
<s>
0.507	0.507	k4
</s>
<s>
0.558	0.558	k4
</s>
<s>
0.584	0.584	k4
</s>
<s>
0.590	0.590	k4
</s>
<s>
0.597	0.597	k4
</s>
<s>
0.604	0.604	k4
</s>
<s>
0.607	0.607	k4
</s>
<s>
0.612	0.612	k4
</s>
<s>
0.615	0.615	k4
</s>
<s>
0.616	0.616	k4
</s>
<s>
0.617	0.617	k4
</s>
<s>
131	#num#	k4
</s>
<s>
Vanuatu	Vanuata	k1gFnSc4
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.608	0.608	k4
</s>
<s>
0.616	0.616	k4
</s>
<s>
0.617	0.617	k4
</s>
<s>
0.618	0.618	k4
</s>
<s>
0.617	0.617	k4
</s>
<s>
0.616	0.616	k4
</s>
<s>
132	#num#	k4
</s>
<s>
Nikaragua	Nikaragua	k1gFnSc1
</s>
<s>
0.483	0.483	k4
</s>
<s>
0.491	0.491	k4
</s>
<s>
0.491	0.491	k4
</s>
<s>
0.554	0.554	k4
</s>
<s>
0.585	0.585	k4
</s>
<s>
0.590	0.590	k4
</s>
<s>
0.595	0.595	k4
</s>
<s>
0.599	0.599	k4
</s>
<s>
0.600	0.600	k4
</s>
<s>
0.604	0.604	k4
</s>
<s>
0.608	0.608	k4
</s>
<s>
0.611	0.611	k4
</s>
<s>
0.614	0.614	k4
</s>
<s>
133	#num#	k4
</s>
<s>
Tádžikistán	Tádžikistán	k1gInSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.610	0.610	k4
</s>
<s>
0.529	0.529	k4
</s>
<s>
0.572	0.572	k4
</s>
<s>
0.578	0.578	k4
</s>
<s>
0.583	0.583	k4
</s>
<s>
0.591	0.591	k4
</s>
<s>
0.592	0.592	k4
</s>
<s>
0.596	0.596	k4
</s>
<s>
0.600	0.600	k4
</s>
<s>
0.603	0.603	k4
</s>
<s>
0.607	0.607	k4
</s>
<s>
133	#num#	k4
</s>
<s>
Kiribati	Kiribat	k5eAaPmF,k5eAaImF
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.599	0.599	k4
</s>
<s>
0.599	0.599	k4
</s>
<s>
0.606	0.606	k4
</s>
<s>
0.607	0.607	k4
</s>
<s>
135	#num#	k4
</s>
<s>
Indie	Indie	k1gFnSc1
</s>
<s>
0.369	0.369	k4
</s>
<s>
0.404	0.404	k4
</s>
<s>
0.431	0.431	k4
</s>
<s>
0.483	0.483	k4
</s>
<s>
0.527	0.527	k4
</s>
<s>
0.537	0.537	k4
</s>
<s>
0.547	0.547	k4
</s>
<s>
0.554	0.554	k4
</s>
<s>
0.560	0.560	k4
</s>
<s>
0.570	0.570	k4
</s>
<s>
0.581	0.581	k4
</s>
<s>
0.583	0.583	k4
</s>
<s>
0.586	0.586	k4
</s>
<s>
136	#num#	k4
</s>
<s>
Kambodža	Kambodža	k1gFnSc1
</s>
<s>
0.251	0.251	k4
</s>
<s>
0.379	0.379	k4
</s>
<s>
0.403	0.403	k4
</s>
<s>
0.466	0.466	k4
</s>
<s>
0.536	0.536	k4
</s>
<s>
0.547	0.547	k4
</s>
<s>
0.558	0.558	k4
</s>
<s>
0.564	0.564	k4
</s>
<s>
0.566	0.566	k4
</s>
<s>
0.571	0.571	k4
</s>
<s>
0.575	0.575	k4
</s>
<s>
0.579	0.579	k4
</s>
<s>
0.584	0.584	k4
</s>
<s>
136	#num#	k4
</s>
<s>
Bhútán	Bhútán	k1gInSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.569	0.569	k4
</s>
<s>
0.579	0.579	k4
</s>
<s>
0.580	0.580	k4
</s>
<s>
0.584	0.584	k4
</s>
<s>
138	#num#	k4
</s>
<s>
Ghana	Ghana	k1gFnSc1
</s>
<s>
0.423	0.423	k4
</s>
<s>
0.433	0.433	k4
</s>
<s>
0.502	0.502	k4
</s>
<s>
0.487	0.487	k4
</s>
<s>
0.511	0.511	k4
</s>
<s>
0.521	0.521	k4
</s>
<s>
0.532	0.532	k4
</s>
<s>
0.544	0.544	k4
</s>
<s>
0.549	0.549	k4
</s>
<s>
0.556	0.556	k4
</s>
<s>
0.566	0.566	k4
</s>
<s>
0.571	0.571	k4
</s>
<s>
0.573	0.573	k4
</s>
<s>
139	#num#	k4
</s>
<s>
Laos	Laos	k1gInSc1
</s>
<s>
0.340	0.340	k4
</s>
<s>
0.398	0.398	k4
</s>
<s>
0.395	0.395	k4
</s>
<s>
0.473	0.473	k4
</s>
<s>
0.511	0.511	k4
</s>
<s>
0.517	0.517	k4
</s>
<s>
0.525	0.525	k4
</s>
<s>
0.533	0.533	k4
</s>
<s>
0.543	0.543	k4
</s>
<s>
0.549	0.549	k4
</s>
<s>
0.560	0.560	k4
</s>
<s>
0.565	0.565	k4
</s>
<s>
0.569	0.569	k4
</s>
<s>
140	#num#	k4
</s>
<s>
Kongo	Kongo	k1gNnSc1
</s>
<s>
0.542	0.542	k4
</s>
<s>
0.576	0.576	k4
</s>
<s>
0.553	0.553	k4
</s>
<s>
0.501	0.501	k4
</s>
<s>
0.525	0.525	k4
</s>
<s>
0.535	0.535	k4
</s>
<s>
0.535	0.535	k4
</s>
<s>
0.548	0.548	k4
</s>
<s>
0.559	0.559	k4
</s>
<s>
0.565	0.565	k4
</s>
<s>
0.549	0.549	k4
</s>
<s>
0.561	0.561	k4
</s>
<s>
0.564	0.564	k4
</s>
<s>
141	#num#	k4
</s>
<s>
Zambie	Zambie	k1gFnSc1
</s>
<s>
0.422	0.422	k4
</s>
<s>
0.420	0.420	k4
</s>
<s>
0.407	0.407	k4
</s>
<s>
0.423	0.423	k4
</s>
<s>
0.471	0.471	k4
</s>
<s>
0.480	0.480	k4
</s>
<s>
0.486	0.486	k4
</s>
<s>
0.505	0.505	k4
</s>
<s>
0.526	0.526	k4
</s>
<s>
0.530	0.530	k4
</s>
<s>
0.543	0.543	k4
</s>
<s>
0.554	0.554	k4
</s>
<s>
0.561	0.561	k4
</s>
<s>
142	#num#	k4
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Tomáš	Tomáš	k1gMnSc1
a	a	k8xC
Princův	princův	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.495	0.495	k4
</s>
<s>
0.520	0.520	k4
</s>
<s>
0.520	0.520	k4
</s>
<s>
0.531	0.531	k4
</s>
<s>
0.537	0.537	k4
</s>
<s>
0.543	0.543	k4
</s>
<s>
0.543	0.543	k4
</s>
<s>
0.548	0.548	k4
</s>
<s>
0.556	0.556	k4
</s>
<s>
0.558	0.558	k4
</s>
<s>
142	#num#	k4
</s>
<s>
Bangladéš	Bangladéš	k1gInSc1
</s>
<s>
0.336	0.336	k4
</s>
<s>
0.357	0.357	k4
</s>
<s>
0.382	0.382	k4
</s>
<s>
0.453	0.453	k4
</s>
<s>
0.494	0.494	k4
</s>
<s>
0.502	0.502	k4
</s>
<s>
0.510	0.510	k4
</s>
<s>
0.515	0.515	k4
</s>
<s>
0.527	0.527	k4
</s>
<s>
0.539	0.539	k4
</s>
<s>
0.549	0.549	k4
</s>
<s>
0.554	0.554	k4
</s>
<s>
0.558	0.558	k4
</s>
<s>
144	#num#	k4
</s>
<s>
Rovníková	rovníkový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.476	0.476	k4
</s>
<s>
0.517	0.517	k4
</s>
<s>
0.528	0.528	k4
</s>
<s>
0.533	0.533	k4
</s>
<s>
0.543	0.543	k4
</s>
<s>
0.543	0.543	k4
</s>
<s>
0.559	0.559	k4
</s>
<s>
0.553	0.553	k4
</s>
<s>
0.556	0.556	k4
</s>
<s>
0.556	0.556	k4
</s>
<s>
145	#num#	k4
</s>
<s>
Nepál	Nepál	k1gInSc1
</s>
<s>
0.286	0.286	k4
</s>
<s>
0.330	0.330	k4
</s>
<s>
0.388	0.388	k4
</s>
<s>
0.449	0.449	k4
</s>
<s>
0.477	0.477	k4
</s>
<s>
0.487	0.487	k4
</s>
<s>
0.492	0.492	k4
</s>
<s>
0.501	0.501	k4
</s>
<s>
0.513	0.513	k4
</s>
<s>
0.527	0.527	k4
</s>
<s>
0.533	0.533	k4
</s>
<s>
0.537	0.537	k4
</s>
<s>
0.540	0.540	k4
</s>
<s>
146	#num#	k4
</s>
<s>
Pákistán	Pákistán	k1gInSc1
</s>
<s>
0.356	0.356	k4
</s>
<s>
0.384	0.384	k4
</s>
<s>
0.402	0.402	k4
</s>
<s>
0.454	0.454	k4
</s>
<s>
0.504	0.504	k4
</s>
<s>
0.527	0.527	k4
</s>
<s>
0.535	0.535	k4
</s>
<s>
0.536	0.536	k4
</s>
<s>
0.545	0.545	k4
</s>
<s>
0.526	0.526	k4
</s>
<s>
0.531	0.531	k4
</s>
<s>
0.535	0.535	k4
</s>
<s>
0.537	0.537	k4
</s>
<s>
147	#num#	k4
</s>
<s>
Keňa	Keňa	k1gFnSc1
</s>
<s>
0.446	0.446	k4
</s>
<s>
0.459	0.459	k4
</s>
<s>
0.471	0.471	k4
</s>
<s>
0.455	0.455	k4
</s>
<s>
0.479	0.479	k4
</s>
<s>
0.487	0.487	k4
</s>
<s>
0.500	0.500	k4
</s>
<s>
0.508	0.508	k4
</s>
<s>
0.516	0.516	k4
</s>
<s>
0.522	0.522	k4
</s>
<s>
0.527	0.527	k4
</s>
<s>
0.531	0.531	k4
</s>
<s>
0.535	0.535	k4
</s>
<s>
148	#num#	k4
</s>
<s>
Svazijsko	Svazijsko	k1gNnSc1
</s>
<s>
0.477	0.477	k4
</s>
<s>
0.516	0.516	k4
</s>
<s>
0.538	0.538	k4
</s>
<s>
0.498	0.498	k4
</s>
<s>
0.498	0.498	k4
</s>
<s>
0.505	0.505	k4
</s>
<s>
0.513	0.513	k4
</s>
<s>
0.518	0.518	k4
</s>
<s>
0.523	0.523	k4
</s>
<s>
0.527	0.527	k4
</s>
<s>
0.530	0.530	k4
</s>
<s>
0.529	0.529	k4
</s>
<s>
0.530	0.530	k4
</s>
<s>
149	#num#	k4
</s>
<s>
Angola	Angola	k1gFnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.377	0.377	k4
</s>
<s>
0.446	0.446	k4
</s>
<s>
0.465	0.465	k4
</s>
<s>
0.478	0.478	k4
</s>
<s>
0.490	0.490	k4
</s>
<s>
0.491	0.491	k4
</s>
<s>
0.504	0.504	k4
</s>
<s>
0.521	0.521	k4
</s>
<s>
0.524	0.524	k4
</s>
<s>
0.526	0.526	k4
</s>
<s>
150	#num#	k4
</s>
<s>
Barma	Barma	k1gFnSc1
</s>
<s>
0.328	0.328	k4
</s>
<s>
0.355	0.355	k4
</s>
<s>
0.347	0.347	k4
</s>
<s>
0.421	0.421	k4
</s>
<s>
0.472	0.472	k4
</s>
<s>
0.483	0.483	k4
</s>
<s>
0.493	0.493	k4
</s>
<s>
0.500	0.500	k4
</s>
<s>
0.507	0.507	k4
</s>
<s>
0.514	0.514	k4
</s>
<s>
0.517	0.517	k4
</s>
<s>
0.520	0.520	k4
</s>
<s>
0.524	0.524	k4
</s>
<s>
151	#num#	k4
</s>
<s>
Rwanda	Rwanda	k1gFnSc1
</s>
<s>
0.291	0.291	k4
</s>
<s>
0.312	0.312	k4
</s>
<s>
0.238	0.238	k4
</s>
<s>
0.329	0.329	k4
</s>
<s>
0.391	0.391	k4
</s>
<s>
0.414	0.414	k4
</s>
<s>
0.423	0.423	k4
</s>
<s>
0.432	0.432	k4
</s>
<s>
0.443	0.443	k4
</s>
<s>
0.453	0.453	k4
</s>
<s>
0.463	0.463	k4
</s>
<s>
0.502	0.502	k4
</s>
<s>
0.506	0.506	k4
</s>
<s>
152	#num#	k4
</s>
<s>
Nigérie	Nigérie	k1gFnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.466	0.466	k4
</s>
<s>
0.471	0.471	k4
</s>
<s>
0.480	0.480	k4
</s>
<s>
0.483	0.483	k4
</s>
<s>
0.488	0.488	k4
</s>
<s>
0.492	0.492	k4
</s>
<s>
0.496	0.496	k4
</s>
<s>
0.500	0.500	k4
</s>
<s>
0.504	0.504	k4
</s>
<s>
152	#num#	k4
</s>
<s>
Kamerun	Kamerun	k1gInSc1
</s>
<s>
0.391	0.391	k4
</s>
<s>
0.422	0.422	k4
</s>
<s>
0.440	0.440	k4
</s>
<s>
0.433	0.433	k4
</s>
<s>
0.457	0.457	k4
</s>
<s>
0.459	0.459	k4
</s>
<s>
0.468	0.468	k4
</s>
<s>
0.477	0.477	k4
</s>
<s>
0.485	0.485	k4
</s>
<s>
0.493	0.493	k4
</s>
<s>
0.498	0.498	k4
</s>
<s>
0.501	0.501	k4
</s>
<s>
0.504	0.504	k4
</s>
<s>
154	#num#	k4
</s>
<s>
Jemen	Jemen	k1gInSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.390	0.390	k4
</s>
<s>
0.427	0.427	k4
</s>
<s>
0.462	0.462	k4
</s>
<s>
0.465	0.465	k4
</s>
<s>
0.468	0.468	k4
</s>
<s>
0.471	0.471	k4
</s>
<s>
0.478	0.478	k4
</s>
<s>
0.484	0.484	k4
</s>
<s>
0.497	0.497	k4
</s>
<s>
0.499	0.499	k4
</s>
<s>
0.500	0.500	k4
</s>
<s>
155	#num#	k4
</s>
<s>
Madagaskar	Madagaskar	k1gInSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.453	0.453	k4
</s>
<s>
0.470	0.470	k4
</s>
<s>
0.476	0.476	k4
</s>
<s>
0.480	0.480	k4
</s>
<s>
0.487	0.487	k4
</s>
<s>
0.496	0.496	k4
</s>
<s>
0.494	0.494	k4
</s>
<s>
0.495	0.495	k4
</s>
<s>
0.496	0.496	k4
</s>
<s>
0.498	0.498	k4
</s>
<s>
156	#num#	k4
</s>
<s>
Zimbabwe	Zimbabwe	k1gNnSc1
</s>
<s>
0.437	0.437	k4
</s>
<s>
0.517	0.517	k4
</s>
<s>
0.488	0.488	k4
</s>
<s>
0.428	0.428	k4
</s>
<s>
0.412	0.412	k4
</s>
<s>
0.417	0.417	k4
</s>
<s>
0.424	0.424	k4
</s>
<s>
0.422	0.422	k4
</s>
<s>
0.439	0.439	k4
</s>
<s>
0.459	0.459	k4
</s>
<s>
0.473	0.473	k4
</s>
<s>
0.484	0.484	k4
</s>
<s>
0.492	0.492	k4
</s>
<s>
157	#num#	k4
</s>
<s>
Šalomounovy	Šalomounův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.475	0.475	k4
</s>
<s>
0.483	0.483	k4
</s>
<s>
0.493	0.493	k4
</s>
<s>
0.498	0.498	k4
</s>
<s>
0.506	0.506	k4
</s>
<s>
0.500	0.500	k4
</s>
<s>
0.489	0.489	k4
</s>
<s>
0.494	0.494	k4
</s>
<s>
0.489	0.489	k4
</s>
<s>
0.491	0.491	k4
</s>
<s>
157	#num#	k4
</s>
<s>
Papua	Papu	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
</s>
<s>
0.323	0.323	k4
</s>
<s>
0.341	0.341	k4
</s>
<s>
0.363	0.363	k4
</s>
<s>
0.423	0.423	k4
</s>
<s>
0.441	0.441	k4
</s>
<s>
0.452	0.452	k4
</s>
<s>
0.454	0.454	k4
</s>
<s>
0.467	0.467	k4
</s>
<s>
0.474	0.474	k4
</s>
<s>
0.479	0.479	k4
</s>
<s>
0.484	0.484	k4
</s>
<s>
0.490	0.490	k4
</s>
<s>
0.491	0.491	k4
</s>
<s>
159	#num#	k4
</s>
<s>
Tanzanie	Tanzanie	k1gFnSc1
</s>
<s>
0.377	0.377	k4
</s>
<s>
0.366	0.366	k4
</s>
<s>
0.354	0.354	k4
</s>
<s>
0.376	0.376	k4
</s>
<s>
0.419	0.419	k4
</s>
<s>
0.430	0.430	k4
</s>
<s>
0.440	0.440	k4
</s>
<s>
0.451	0.451	k4
</s>
<s>
0.457	0.457	k4
</s>
<s>
0.464	0.464	k4
</s>
<s>
0.478	0.478	k4
</s>
<s>
0.484	0.484	k4
</s>
<s>
0.488	0.488	k4
</s>
<s>
159	#num#	k4
</s>
<s>
Komorský	Komorský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.464	0.464	k4
</s>
<s>
0.468	0.468	k4
</s>
<s>
0.471	0.471	k4
</s>
<s>
0.474	0.474	k4
</s>
<s>
0.476	0.476	k4
</s>
<s>
0.479	0.479	k4
</s>
<s>
0.483	0.483	k4
</s>
<s>
0.486	0.486	k4
</s>
<s>
0.488	0.488	k4
</s>
<s>
161	#num#	k4
</s>
<s>
Mauritánie	Mauritánie	k1gFnSc1
</s>
<s>
0.347	0.347	k4
</s>
<s>
0.355	0.355	k4
</s>
<s>
0.367	0.367	k4
</s>
<s>
0.433	0.433	k4
</s>
<s>
0.455	0.455	k4
</s>
<s>
0.465	0.465	k4
</s>
<s>
0.467	0.467	k4
</s>
<s>
0.466	0.466	k4
</s>
<s>
0.474	0.474	k4
</s>
<s>
0.475	0.475	k4
</s>
<s>
0.475	0.475	k4
</s>
<s>
0.485	0.485	k4
</s>
<s>
0.487	0.487	k4
</s>
<s>
162	#num#	k4
</s>
<s>
Lesotho	Lesotze	k6eAd1
</s>
<s>
0.443	0.443	k4
</s>
<s>
0.475	0.475	k4
</s>
<s>
0.493	0.493	k4
</s>
<s>
0.443	0.443	k4
</s>
<s>
0.437	0.437	k4
</s>
<s>
0.441	0.441	k4
</s>
<s>
0.448	0.448	k4
</s>
<s>
0.456	0.456	k4
</s>
<s>
0.464	0.464	k4
</s>
<s>
0.472	0.472	k4
</s>
<s>
0.476	0.476	k4
</s>
<s>
0.481	0.481	k4
</s>
<s>
0.486	0.486	k4
</s>
<s>
163	#num#	k4
</s>
<s>
Senegal	Senegal	k1gInSc1
</s>
<s>
0.333	0.333	k4
</s>
<s>
0.364	0.364	k4
</s>
<s>
0.384	0.384	k4
</s>
<s>
0.413	0.413	k4
</s>
<s>
0.451	0.451	k4
</s>
<s>
0.456	0.456	k4
</s>
<s>
0.466	0.466	k4
</s>
<s>
0.474	0.474	k4
</s>
<s>
0.478	0.478	k4
</s>
<s>
0.483	0.483	k4
</s>
<s>
0.483	0.483	k4
</s>
<s>
0.484	0.484	k4
</s>
<s>
0.485	0.485	k4
</s>
<s>
164	#num#	k4
</s>
<s>
Uganda	Uganda	k1gFnSc1
</s>
<s>
0.293	0.293	k4
</s>
<s>
0.309	0.309	k4
</s>
<s>
0.310	0.310	k4
</s>
<s>
0.392	0.392	k4
</s>
<s>
0.429	0.429	k4
</s>
<s>
0.437	0.437	k4
</s>
<s>
0.446	0.446	k4
</s>
<s>
0.458	0.458	k4
</s>
<s>
0.466	0.466	k4
</s>
<s>
0.472	0.472	k4
</s>
<s>
0.477	0.477	k4
</s>
<s>
0.480	0.480	k4
</s>
<s>
0.484	0.484	k4
</s>
<s>
165	#num#	k4
</s>
<s>
Benin	Benin	k1gMnSc1
</s>
<s>
0.287	0.287	k4
</s>
<s>
0.323	0.323	k4
</s>
<s>
0.342	0.342	k4
</s>
<s>
0.391	0.391	k4
</s>
<s>
0.432	0.432	k4
</s>
<s>
0.439	0.439	k4
</s>
<s>
0.446	0.446	k4
</s>
<s>
0.454	0.454	k4
</s>
<s>
0.461	0.461	k4
</s>
<s>
0.467	0.467	k4
</s>
<s>
0.471	0.471	k4
</s>
<s>
0.473	0.473	k4
</s>
<s>
0.476	0.476	k4
</s>
<s>
166	#num#	k4
</s>
<s>
Togo	Togo	k1gNnSc1
</s>
<s>
0.405	0.405	k4
</s>
<s>
0.389	0.389	k4
</s>
<s>
0.404	0.404	k4
</s>
<s>
0.430	0.430	k4
</s>
<s>
0.442	0.442	k4
</s>
<s>
0.448	0.448	k4
</s>
<s>
0.446	0.446	k4
</s>
<s>
0.447	0.447	k4
</s>
<s>
0.454	0.454	k4
</s>
<s>
0.460	0.460	k4
</s>
<s>
0.467	0.467	k4
</s>
<s>
0.470	0.470	k4
</s>
<s>
0.473	0.473	k4
</s>
<s>
166	#num#	k4
</s>
<s>
Súdán	Súdán	k1gInSc1
</s>
<s>
0.331	0.331	k4
</s>
<s>
0.333	0.333	k4
</s>
<s>
0.342	0.342	k4
</s>
<s>
0.385	0.385	k4
</s>
<s>
0.423	0.423	k4
</s>
<s>
0.430	0.430	k4
</s>
<s>
0.440	0.440	k4
</s>
<s>
0.447	0.447	k4
</s>
<s>
0.458	0.458	k4
</s>
<s>
0.463	0.463	k4
</s>
<s>
0.468	0.468	k4
</s>
<s>
0.472	0.472	k4
</s>
<s>
0.473	0.473	k4
</s>
<s>
168	#num#	k4
</s>
<s>
Haiti	Haiti	k1gNnSc1
</s>
<s>
0.352	0.352	k4
</s>
<s>
0.392	0.392	k4
</s>
<s>
0.413	0.413	k4
</s>
<s>
0.433	0.433	k4
</s>
<s>
0.447	0.447	k4
</s>
<s>
0.450	0.450	k4
</s>
<s>
0.455	0.455	k4
</s>
<s>
0.458	0.458	k4
</s>
<s>
0.463	0.463	k4
</s>
<s>
0.462	0.462	k4
</s>
<s>
0.466	0.466	k4
</s>
<s>
0.469	0.469	k4
</s>
<s>
0.471	0.471	k4
</s>
<s>
169	#num#	k4
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1
</s>
<s>
0.230	0.230	k4
</s>
<s>
0.271	0.271	k4
</s>
<s>
0.296	0.296	k4
</s>
<s>
0.341	0.341	k4
</s>
<s>
0.396	0.396	k4
</s>
<s>
0.406	0.406	k4
</s>
<s>
0.416	0.416	k4
</s>
<s>
0.430	0.430	k4
</s>
<s>
0.437	0.437	k4
</s>
<s>
0.453	0.453	k4
</s>
<s>
0.458	0.458	k4
</s>
<s>
0.466	0.466	k4
</s>
<s>
0.468	0.468	k4
</s>
<s>
170	#num#	k4
</s>
<s>
Džibutsko	Džibutsko	k6eAd1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.412	0.412	k4
</s>
<s>
0.417	0.417	k4
</s>
<s>
0.428	0.428	k4
</s>
<s>
0.438	0.438	k4
</s>
<s>
0.444	0.444	k4
</s>
<s>
0.452	0.452	k4
</s>
<s>
0.461	0.461	k4
</s>
<s>
0.465	0.465	k4
</s>
<s>
0.467	0.467	k4
</s>
<s>
171	#num#	k4
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1
slonoviny	slonovina	k1gFnSc2
</s>
<s>
0.377	0.377	k4
</s>
<s>
0.378	0.378	k4
</s>
<s>
0.380	0.380	k4
</s>
<s>
0.393	0.393	k4
</s>
<s>
0.407	0.407	k4
</s>
<s>
0.414	0.414	k4
</s>
<s>
0.422	0.422	k4
</s>
<s>
0.427	0.427	k4
</s>
<s>
0.433	0.433	k4
</s>
<s>
0.439	0.439	k4
</s>
<s>
0.443	0.443	k4
</s>
<s>
0.448	0.448	k4
</s>
<s>
0.452	0.452	k4
</s>
<s>
172	#num#	k4
</s>
<s>
Gambie	Gambie	k1gFnSc1
</s>
<s>
0.300	0.300	k4
</s>
<s>
0.310	0.310	k4
</s>
<s>
0.334	0.334	k4
</s>
<s>
0.383	0.383	k4
</s>
<s>
0.414	0.414	k4
</s>
<s>
0.418	0.418	k4
</s>
<s>
0.425	0.425	k4
</s>
<s>
0.432	0.432	k4
</s>
<s>
0.436	0.436	k4
</s>
<s>
0.440	0.440	k4
</s>
<s>
0.436	0.436	k4
</s>
<s>
0.438	0.438	k4
</s>
<s>
0.441	0.441	k4
</s>
<s>
173	#num#	k4
</s>
<s>
Ethiopie	Ethiopie	k1gFnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.284	0.284	k4
</s>
<s>
0.339	0.339	k4
</s>
<s>
0.356	0.356	k4
</s>
<s>
0.378	0.378	k4
</s>
<s>
0.394	0.394	k4
</s>
<s>
0.403	0.403	k4
</s>
<s>
0.409	0.409	k4
</s>
<s>
0.422	0.422	k4
</s>
<s>
0.429	0.429	k4
</s>
<s>
0.435	0.435	k4
</s>
<s>
174	#num#	k4
</s>
<s>
Malawi	Malawi	k1gNnSc1
</s>
<s>
0.270	0.270	k4
</s>
<s>
0.275	0.275	k4
</s>
<s>
0.283	0.283	k4
</s>
<s>
0.341	0.341	k4
</s>
<s>
0.368	0.368	k4
</s>
<s>
0.375	0.375	k4
</s>
<s>
0.383	0.383	k4
</s>
<s>
0.395	0.395	k4
</s>
<s>
0.407	0.407	k4
</s>
<s>
0.406	0.406	k4
</s>
<s>
0.411	0.411	k4
</s>
<s>
0.411	0.411	k4
</s>
<s>
0.414	0.414	k4
</s>
<s>
175	#num#	k4
</s>
<s>
Libérie	Libérie	k1gFnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.339	0.339	k4
</s>
<s>
0.335	0.335	k4
</s>
<s>
0.350	0.350	k4
</s>
<s>
0.364	0.364	k4
</s>
<s>
0.374	0.374	k4
</s>
<s>
0.386	0.386	k4
</s>
<s>
0.393	0.393	k4
</s>
<s>
0.402	0.402	k4
</s>
<s>
0.407	0.407	k4
</s>
<s>
0.412	0.412	k4
</s>
<s>
176	#num#	k4
</s>
<s>
Mali	Mali	k1gNnSc1
</s>
<s>
0.208	0.208	k4
</s>
<s>
0.213	0.213	k4
</s>
<s>
0.232	0.232	k4
</s>
<s>
0.309	0.309	k4
</s>
<s>
0.359	0.359	k4
</s>
<s>
0.367	0.367	k4
</s>
<s>
0.377	0.377	k4
</s>
<s>
0.385	0.385	k4
</s>
<s>
0.393	0.393	k4
</s>
<s>
0.398	0.398	k4
</s>
<s>
0.405	0.405	k4
</s>
<s>
0.406	0.406	k4
</s>
<s>
0.407	0.407	k4
</s>
<s>
177	#num#	k4
</s>
<s>
Guinea-Bissau	Guinea-Bissau	k6eAd1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.387	0.387	k4
</s>
<s>
0.393	0.393	k4
</s>
<s>
0.395	0.395	k4
</s>
<s>
0.397	0.397	k4
</s>
<s>
0.397	0.397	k4
</s>
<s>
0.401	0.401	k4
</s>
<s>
0.402	0.402	k4
</s>
<s>
0.396	0.396	k4
</s>
<s>
0.396	0.396	k4
</s>
<s>
178	#num#	k4
</s>
<s>
Mosambik	Mosambik	k1gInSc1
</s>
<s>
0.246	0.246	k4
</s>
<s>
0.205	0.205	k4
</s>
<s>
0.216	0.216	k4
</s>
<s>
0.285	0.285	k4
</s>
<s>
0.343	0.343	k4
</s>
<s>
0.348	0.348	k4
</s>
<s>
0.359	0.359	k4
</s>
<s>
0.366	0.366	k4
</s>
<s>
0.375	0.375	k4
</s>
<s>
0.380	0.380	k4
</s>
<s>
0.384	0.384	k4
</s>
<s>
0.389	0.389	k4
</s>
<s>
0.393	0.393	k4
</s>
<s>
179	#num#	k4
</s>
<s>
Guinea	Guinea	k1gFnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.366	0.366	k4
</s>
<s>
0.374	0.374	k4
</s>
<s>
0.377	0.377	k4
</s>
<s>
0.377	0.377	k4
</s>
<s>
0.376	0.376	k4
</s>
<s>
0.380	0.380	k4
</s>
<s>
0.387	0.387	k4
</s>
<s>
0.391	0.391	k4
</s>
<s>
0.392	0.392	k4
</s>
<s>
180	#num#	k4
</s>
<s>
Burundi	Burundi	k1gNnSc1
</s>
<s>
0.230	0.230	k4
</s>
<s>
0.270	0.270	k4
</s>
<s>
0.291	0.291	k4
</s>
<s>
0.290	0.290	k4
</s>
<s>
0.319	0.319	k4
</s>
<s>
0.339	0.339	k4
</s>
<s>
0.350	0.350	k4
</s>
<s>
0.362	0.362	k4
</s>
<s>
0.372	0.372	k4
</s>
<s>
0.381	0.381	k4
</s>
<s>
0.384	0.384	k4
</s>
<s>
0.386	0.386	k4
</s>
<s>
0.389	0.389	k4
</s>
<s>
181	#num#	k4
</s>
<s>
Burkina	Burkina	k1gMnSc1
Faso	Faso	k1gMnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.321	0.321	k4
</s>
<s>
0.328	0.328	k4
</s>
<s>
0.338	0.338	k4
</s>
<s>
0.349	0.349	k4
</s>
<s>
0.357	0.357	k4
</s>
<s>
0.367	0.367	k4
</s>
<s>
0.376	0.376	k4
</s>
<s>
0.385	0.385	k4
</s>
<s>
0.388	0.388	k4
</s>
<s>
182	#num#	k4
</s>
<s>
Eritrea	Eritrea	k1gFnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.373	0.373	k4
</s>
<s>
0.377	0.377	k4
</s>
<s>
0.380	0.380	k4
</s>
<s>
0.381	0.381	k4
</s>
<s>
183	#num#	k4
</s>
<s>
Sierra	Sierra	k1gFnSc1
Leone	Leo	k1gMnSc5
</s>
<s>
0.276	0.276	k4
</s>
<s>
0.279	0.279	k4
</s>
<s>
0.263	0.263	k4
</s>
<s>
0.297	0.297	k4
</s>
<s>
0.329	0.329	k4
</s>
<s>
0.335	0.335	k4
</s>
<s>
0.340	0.340	k4
</s>
<s>
0.346	0.346	k4
</s>
<s>
0.350	0.350	k4
</s>
<s>
0.353	0.353	k4
</s>
<s>
0.360	0.360	k4
</s>
<s>
0.368	0.368	k4
</s>
<s>
0.374	0.374	k4
</s>
<s>
184	#num#	k4
</s>
<s>
Čadská	čadský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
0.301	0.301	k4
</s>
<s>
0.324	0.324	k4
</s>
<s>
0.325	0.325	k4
</s>
<s>
0.331	0.331	k4
</s>
<s>
0.338	0.338	k4
</s>
<s>
0.343	0.343	k4
</s>
<s>
0.349	0.349	k4
</s>
<s>
0.365	0.365	k4
</s>
<s>
0.370	0.370	k4
</s>
<s>
0.372	0.372	k4
</s>
<s>
185	#num#	k4
</s>
<s>
Středoafrická	středoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
0.295	0.295	k4
</s>
<s>
0.310	0.310	k4
</s>
<s>
0.310	0.310	k4
</s>
<s>
0.314	0.314	k4
</s>
<s>
0.327	0.327	k4
</s>
<s>
0.332	0.332	k4
</s>
<s>
0.338	0.338	k4
</s>
<s>
0.344	0.344	k4
</s>
<s>
0.349	0.349	k4
</s>
<s>
0.355	0.355	k4
</s>
<s>
0.361	0.361	k4
</s>
<s>
0.365	0.365	k4
</s>
<s>
0.341	0.341	k4
</s>
<s>
186	#num#	k4
</s>
<s>
Kongo	Kongo	k1gNnSc1
</s>
<s>
0.336	0.336	k4
</s>
<s>
0.348	0.348	k4
</s>
<s>
0.319	0.319	k4
</s>
<s>
0.274	0.274	k4
</s>
<s>
0.292	0.292	k4
</s>
<s>
0.297	0.297	k4
</s>
<s>
0.301	0.301	k4
</s>
<s>
0.307	0.307	k4
</s>
<s>
0.313	0.313	k4
</s>
<s>
0.319	0.319	k4
</s>
<s>
0.323	0.323	k4
</s>
<s>
0.333	0.333	k4
</s>
<s>
0.338	0.338	k4
</s>
<s>
187	#num#	k4
</s>
<s>
Niger	Niger	k1gInSc1
</s>
<s>
0.191	0.191	k4
</s>
<s>
0.197	0.197	k4
</s>
<s>
0.218	0.218	k4
</s>
<s>
0.262	0.262	k4
</s>
<s>
0.293	0.293	k4
</s>
<s>
0.297	0.297	k4
</s>
<s>
0.303	0.303	k4
</s>
<s>
0.309	0.309	k4
</s>
<s>
0.312	0.312	k4
</s>
<s>
0.323	0.323	k4
</s>
<s>
0.328	0.328	k4
</s>
<s>
0.335	0.335	k4
</s>
<s>
0.337	0.337	k4
</s>
<s>
..	..	k?
</s>
<s>
Jižní	jižní	k2eAgInSc1d1
Súdán	Súdán	k1gInSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
Tuvalu	Tuvala	k1gFnSc4
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
Somálsko	Somálsko	k1gNnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
San	San	k?
Marino	Marina	k1gFnSc5
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
Nauru	Naura	k1gFnSc4
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
Monako	Monako	k1gNnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
Marshallovy	Marshallův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
Korejská	korejský	k2eAgFnSc1d1
lidově	lidově	k6eAd1
demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
</s>
<s>
..	..	k?
=	=	kIx~
data	datum	k1gNnPc1
nejsou	být	k5eNaImIp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
</s>
<s>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kritika	kritika	k1gFnSc1
</s>
<s>
Jeden	jeden	k4xCgInSc1
ze	z	k7c2
základních	základní	k2eAgInPc2d1
bodů	bod	k1gInPc2
kritiky	kritika	k1gFnSc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
HDI	HDI	kA
poutá	poutat	k5eAaImIp3nS
pozornost	pozornost	k1gFnSc4
na	na	k7c4
velmi	velmi	k6eAd1
široké	široký	k2eAgInPc4d1
a	a	k8xC
nejasné	jasný	k2eNgInPc4d1
pojmy	pojem	k1gInPc4
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
lidský	lidský	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnohem	mnohem	k6eAd1
jasněji	jasně	k6eAd2
definovaný	definovaný	k2eAgInSc1d1
je	být	k5eAaImIp3nS
například	například	k6eAd1
HDP	HDP	kA
(	(	kIx(
<g/>
Hrubý	hrubý	k2eAgInSc1d1
domácí	domácí	k2eAgInSc1d1
produkt	produkt	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
Streeten	Streeten	k2eAgInSc1d1
píše	psát	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
takovéto	takovýto	k3xDgInPc1
indexy	index	k1gInPc1
úspěšně	úspěšně	k6eAd1
upoutávají	upoutávat	k5eAaImIp3nP
pozornost	pozornost	k1gFnSc4
veřejnosti	veřejnost	k1gFnSc2
a	a	k8xC
zjednodušují	zjednodušovat	k5eAaImIp3nP
problém	problém	k1gInSc4
mnohem	mnohem	k6eAd1
více	hodně	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
dlouhý	dlouhý	k2eAgInSc1d1
výčet	výčet	k1gInSc1
mnoha	mnoho	k4c2
indikátorů	indikátor	k1gInPc2
spojených	spojený	k2eAgInPc2d1
s	s	k7c7
vědeckou	vědecký	k2eAgFnSc7d1
debatou	debata	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Dále	daleko	k6eAd2
se	se	k3xPyFc4
kritika	kritika	k1gFnSc1
ozývá	ozývat	k5eAaImIp3nS
na	na	k7c4
otázku	otázka	k1gFnSc4
těchto	tento	k3xDgInPc2
problémů	problém	k1gInPc2
<g/>
:	:	kIx,
nesprávná	správný	k2eNgNnPc1d1
data	datum	k1gNnPc1
<g/>
,	,	kIx,
zvolení	zvolení	k1gNnSc1
špatných	špatný	k2eAgInPc2d1
indikátorů	indikátor	k1gInPc2
<g/>
,	,	kIx,
obecné	obecný	k2eAgInPc1d1
problémy	problém	k1gInPc1
s	s	k7c7
vzorcem	vzorec	k1gInSc7
pro	pro	k7c4
výpočet	výpočet	k1gInSc4
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
nepřesný	přesný	k2eNgInSc4d1
výčet	výčet	k1gInSc4
příjmů	příjem	k1gInPc2
a	a	k8xC
výdajů	výdaj	k1gInPc2
a	a	k8xC
zbytečnost	zbytečnost	k1gFnSc4
indexu	index	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Index	index	k1gInSc4
je	být	k5eAaImIp3nS
kritizován	kritizovat	k5eAaImNgMnS
z	z	k7c2
důvodu	důvod	k1gInSc2
nepravidelného	pravidelný	k2eNgNnSc2d1
měření	měření	k1gNnSc2
<g/>
,	,	kIx,
možnosti	možnost	k1gFnSc2
nesprávného	správný	k2eNgNnSc2d1
nahlášení	nahlášení	k1gNnSc2
dat	datum	k1gNnPc2
a	a	k8xC
neúplného	úplný	k2eNgInSc2d1
sběru	sběr	k1gInSc2
dat	datum	k1gNnPc2
ve	v	k7c6
státu	stát	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
indikátory	indikátor	k1gInPc4
pro	pro	k7c4
výpočet	výpočet	k1gInSc4
(	(	kIx(
<g/>
míra	míra	k1gFnSc1
lidské	lidský	k2eAgFnSc2d1
svobody	svoboda	k1gFnSc2
<g/>
,	,	kIx,
přístup	přístup	k1gInSc1
k	k	k7c3
zdravotní	zdravotní	k2eAgFnSc3d1
péči	péče	k1gFnSc3
<g/>
,	,	kIx,
kvalita	kvalita	k1gFnSc1
vzdělání	vzdělání	k1gNnSc2
atd.	atd.	kA
<g/>
)	)	kIx)
nebo	nebo	k8xC
jsou	být	k5eAaImIp3nP
nesprávné	správný	k2eNgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Ozývá	ozývat	k5eAaImIp3nS
se	se	k3xPyFc4
kritika	kritika	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
vzoreček	vzoreček	k1gInSc1
pro	pro	k7c4
výpočet	výpočet	k1gInSc4
HDI	HDI	kA
je	být	k5eAaImIp3nS
nahodilý	nahodilý	k2eAgInSc1d1
a	a	k8xC
nesprávný	správný	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
složky	složka	k1gFnPc1
jsou	být	k5eAaImIp3nP
spojovány	spojovat	k5eAaImNgFnP
způsobem	způsob	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
přirovnán	přirovnán	k2eAgMnSc1d1
k	k	k7c3
“	“	k?
<g/>
sčítání	sčítání	k1gNnSc4
jablek	jablko	k1gNnPc2
a	a	k8xC
hrušek	hruška	k1gFnPc2
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konkrétním	konkrétní	k2eAgInSc7d1
případem	případ	k1gInSc7
je	být	k5eAaImIp3nS
nepřesnost	nepřesnost	k1gFnSc1
výpočtu	výpočet	k1gInSc2
příjmu	příjem	k1gInSc2
na	na	k7c4
jednotlivce	jednotlivec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
HDI	HDI	kA
neposkytuje	poskytovat	k5eNaImIp3nS
žádné	žádný	k3yNgFnPc4
nové	nový	k2eAgFnPc4d1
informace	informace	k1gFnPc4
než	než	k8xS
ty	ten	k3xDgFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
dostupné	dostupný	k2eAgInPc1d1
v	v	k7c6
HDP	HDP	kA
na	na	k7c4
jednotlivce	jednotlivec	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Data	datum	k1gNnSc2
za	za	k7c4
rok	rok	k1gInSc4
2010	#num#	k4
nejsou	být	k5eNaImIp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Human	Human	k1gMnSc1
Development	Development	k1gMnSc1
Report	report	k1gInSc4
2019	#num#	k4
–	–	k?
"	"	kIx"
<g/>
Human	Human	k1gMnSc1
Development	Development	k1gMnSc1
Indices	Indices	k1gMnSc1
and	and	k?
Indicators	Indicators	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
HDRO	HDRO	kA
(	(	kIx(
<g/>
Human	Human	k1gInSc1
Development	Development	k1gInSc1
Report	report	k1gInSc4
Office	Office	kA
<g/>
)	)	kIx)
United	United	k1gMnSc1
Nations	Nationsa	k1gFnPc2
Development	Development	k1gMnSc1
Programme	Programme	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
22	#num#	k4
<g/>
–	–	k?
<g/>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Human	Human	k1gInSc1
Development	Development	k1gMnSc1
Index	index	k1gInSc1
and	and	k?
its	its	k?
components	components	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Human	Human	k1gMnSc1
Development	Development	k1gMnSc1
Reports	Reports	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Indices	Indices	k1gInSc1
&	&	k?
Data	datum	k1gNnPc1
|	|	kIx~
Human	Human	k1gInSc1
Development	Development	k1gMnSc1
Reports	Reports	k1gInSc1
(	(	kIx(
<g/>
HDR	HDR	kA
<g/>
)	)	kIx)
|	|	kIx~
United	United	k1gMnSc1
Nations	Nationsa	k1gFnPc2
Development	Development	k1gMnSc1
Programme	Programme	k1gMnSc1
(	(	kIx(
<g/>
UNDP	UNDP	kA
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Technical	Technical	k1gFnSc1
notes	notes	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
UNDP	UNDP	kA
<g/>
,	,	kIx,
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
s.	s.	k?
2	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
http://hdr.undp.org/sites/default/files/hdr14_technical_notes.pdf	http://hdr.undp.org/sites/default/files/hdr14_technical_notes.pdf	k1gMnSc1
<g/>
↑	↑	k?
SYROVÁTKA	Syrovátka	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
(	(	kIx(
<g/>
ne	ne	k9
<g/>
)	)	kIx)
<g/>
měřit	měřit	k5eAaImF
kvalitu	kvalita	k1gFnSc4
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kritické	kritický	k2eAgInPc1d1
pohledy	pohled	k1gInPc1
na	na	k7c4
index	index	k1gInSc4
lidského	lidský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
http://www.development.upol.cz/uploads/dokumenty/Syrovatka_HDI.pdf.	http://www.development.upol.cz/uploads/dokumenty/Syrovatka_HDI.pdf.	k?
Bakalářská	bakalářský	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
PROCHÁZKA	Procházka	k1gMnSc1
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Index	index	k1gInSc1
lidského	lidský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
a	a	k8xC
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
využití	využití	k1gNnSc2
jako	jako	k8xC,k8xS
indikátoru	indikátor	k1gInSc2
mezinárodních	mezinárodní	k2eAgInPc2d1
rozdílů	rozdíl	k1gInPc2
v	v	k7c6
kvalitě	kvalita	k1gFnSc6
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
http://is.muni.cz/th/350697/prif_b/BP.pdf.	http://is.muni.cz/th/350697/prif_b/BP.pdf.	k4
Bakalářská	bakalářský	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
About	About	k1gMnSc1
Human	Human	k1gMnSc1
Development	Development	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
UNDP	UNDP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Human	Human	k1gMnSc1
Development	Development	k1gMnSc1
Report	report	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
http://hdr.undp.org/en/humandev	http://hdr.undp.org/en/humandev	k1gFnSc1
<g/>
↑	↑	k?
Technical	Technical	k1gFnSc1
notes	notes	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
UNDP	UNDP	kA
<g/>
,	,	kIx,
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
2	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
http://hdr.undp.org/sites/default/files/hdr14_technical_notes.pdf	http://hdr.undp.org/sites/default/files/hdr14_technical_notes.pdf	k1gMnSc1
<g/>
↑	↑	k?
Human	Human	k1gMnSc1
Development	Development	k1gMnSc1
Report	report	k1gInSc4
2019	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Rozvojový	rozvojový	k2eAgInSc1d1
program	program	k1gInSc1
OSN	OSN	kA
366	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
92	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
126439	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
300	#num#	k4
<g/>
-	-	kIx~
<g/>
303	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
UNDP	UNDP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Human	Human	k1gMnSc1
Development	Development	k1gMnSc1
Report	report	k1gInSc4
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
United	United	k1gMnSc1
Nations	Nationsa	k1gFnPc2
Development	Development	k1gMnSc1
Programme	Programme	k1gMnSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
92	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
126368	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
STANTON	STANTON	kA
<g/>
,	,	kIx,
Elizabeth	Elizabeth	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Human	Human	k1gMnSc1
Development	Development	k1gMnSc1
Index	index	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
History	Histor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amherts	Amhertsa	k1gFnPc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
s.	s.	k?
16	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
University	universita	k1gFnSc2
of	of	k?
Massachusetts-Amherst	Massachusetts-Amherst	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
států	stát	k1gInPc2
světa	svět	k1gInSc2
podle	podle	k7c2
indexu	index	k1gInSc2
lidského	lidský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
</s>
<s>
Seznam	seznam	k1gInSc1
států	stát	k1gInPc2
světa	svět	k1gInSc2
podle	podle	k7c2
narovnaného	narovnaný	k2eAgInSc2d1
indexu	index	k1gInSc2
lidského	lidský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
index	index	k1gInSc1
lidského	lidský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
World	World	k1gInSc1
Rank	rank	k1gInSc1
-	-	kIx~
Human	Human	k1gInSc1
developement	developement	k1gInSc1
index	index	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Human	Human	k1gInSc1
Development	Development	k1gInSc1
Report	report	k1gInSc4
Office	Office	kA
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
2014	#num#	k4
Zpráva	zpráva	k1gFnSc1
OSN	OSN	kA
o	o	k7c6
lidském	lidský	k2eAgInSc6d1
rozvoji	rozvoj	k1gInSc6
(	(	kIx(
<g/>
PDF	PDF	kA
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
2014	#num#	k4
Technical	Technical	k1gFnPc2
notes	notes	k1gInSc1
-	-	kIx~
Human	Human	k1gInSc1
Development	Development	k1gInSc1
Report	report	k1gInSc4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Seznam	seznam	k1gInSc1
zemí	zem	k1gFnPc2
podle	podle	k7c2
HDI	HDI	kA
na	na	k7c4
NationMaster	NationMaster	k1gInSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
HraoZemi	HraoZe	k1gFnPc7
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Index	index	k1gInSc1
lidského	lidský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4819478-5	4819478-5	k4
</s>
