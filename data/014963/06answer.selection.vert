<s>
Index	index	k1gInSc1
lidského	lidský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Human	Human	k1gMnSc1
development	development	k1gMnSc1
index	index	k1gInSc4
<g/>
,	,	kIx,
HDI	HDI	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
prostředek	prostředek	k1gInSc4
pro	pro	k7c4
srovnání	srovnání	k1gNnSc4
klíčových	klíčový	k2eAgInPc2d1
rozměrů	rozměr	k1gInPc2
lidského	lidský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
<g/>
,	,	kIx,
mezi	mezi	k7c4
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
patří	patřit	k5eAaImIp3nS
<g/>
:	:	kIx,
dlouhý	dlouhý	k2eAgInSc1d1
a	a	k8xC
zdravý	zdravý	k2eAgInSc1d1
život	život	k1gInSc1
<g/>
,	,	kIx,
přístup	přístup	k1gInSc1
ke	k	k7c3
vzdělání	vzdělání	k1gNnSc3
<g/>
,	,	kIx,
životní	životní	k2eAgInSc1d1
standard	standard	k1gInSc1
a	a	k8xC
celková	celkový	k2eAgFnSc1d1
vyspělost	vyspělost	k1gFnSc1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>