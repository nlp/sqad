<p>
<s>
Řád	řád	k1gInSc1	řád
Kanady	Kanada	k1gFnSc2	Kanada
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Order	Order	k1gInSc1	Order
of	of	k?	of
Canada	Canada	k1gFnSc1	Canada
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
civilní	civilní	k2eAgNnSc1d1	civilní
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
udílen	udílen	k2eAgInSc1d1	udílen
těm	ten	k3xDgFnPc3	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
dostáli	dostát	k5eAaPmAgMnP	dostát
mottu	motto	k1gNnSc6	motto
<g/>
:	:	kIx,	:
Desiderantes	Desiderantes	k1gMnSc1	Desiderantes
meliorem	melior	k1gInSc7	melior
patriam	patriam	k6eAd1	patriam
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
<g/>
]	]	kIx)	]
co	co	k8xS	co
se	se	k3xPyFc4	se
zasloužili	zasloužit	k5eAaPmAgMnP	zasloužit
o	o	k7c6	o
lepší	dobrý	k2eAgFnSc6d2	lepší
zemi	zem	k1gFnSc6	zem
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořen	vytvořen	k2eAgMnSc1d1	vytvořen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
ke	k	k7c3	k
stému	stý	k4xOgNnSc3	stý
výročí	výročí	k1gNnSc3	výročí
Kanady	Kanada	k1gFnSc2	Kanada
jako	jako	k8xS	jako
britského	britský	k2eAgNnSc2d1	Britské
dominia	dominion	k1gNnSc2	dominion
<g/>
,	,	kIx,	,
Řád	řád	k1gInSc1	řád
měl	mít	k5eAaImAgInS	mít
ocenit	ocenit	k5eAaPmF	ocenit
ty	ten	k3xDgMnPc4	ten
Kanaďany	Kanaďan	k1gMnPc4	Kanaďan
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
zasloužili	zasloužit	k5eAaPmAgMnP	zasloužit
o	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
vlasti	vlast	k1gFnSc2	vlast
<g/>
,	,	kIx,	,
a	a	k8xC	a
cizince	cizinec	k1gMnPc4	cizinec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přispěli	přispět	k5eAaPmAgMnP	přispět
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
představitelkou	představitelka	k1gFnSc7	představitelka
Řádu	řád	k1gInSc2	řád
Kanady	Kanada	k1gFnSc2	Kanada
je	být	k5eAaImIp3nS	být
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
formálně	formálně	k6eAd1	formálně
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
Generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
kancléřem	kancléř	k1gMnSc7	kancléř
Řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
jeho	on	k3xPp3gInSc2	on
vzniku	vznik	k1gInSc2	vznik
byl	být	k5eAaImAgInS	být
Řád	řád	k1gInSc1	řád
Kanady	Kanada	k1gFnSc2	Kanada
udělen	udělen	k2eAgInSc4d1	udělen
5	[number]	k4	5
268	[number]	k4	268
lidem	lid	k1gInSc7	lid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Řádu	řád	k1gInSc6	řád
Kanady	Kanada	k1gFnSc2	Kanada
existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgInPc4	tři
stupně	stupeň	k1gInPc4	stupeň
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
uděleny	udělen	k2eAgFnPc1d1	udělena
<g/>
:	:	kIx,	:
Rytíř	Rytíř	k1gMnSc1	Rytíř
(	(	kIx(	(
<g/>
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Důstojník	důstojník	k1gMnSc1	důstojník
a	a	k8xC	a
Člen	člen	k1gMnSc1	člen
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
stupňů	stupeň	k1gInPc2	stupeň
má	mít	k5eAaImIp3nS	mít
svoji	svůj	k3xOyFgFnSc4	svůj
zkratku	zkratka	k1gFnSc4	zkratka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
může	moct	k5eAaImIp3nS	moct
vyznamenaný	vyznamenaný	k2eAgInSc1d1	vyznamenaný
používat	používat	k5eAaImF	používat
za	za	k7c7	za
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Povýšení	povýšení	k1gNnSc1	povýšení
do	do	k7c2	do
jiného	jiný	k2eAgInSc2d1	jiný
stupně	stupeň	k1gInSc2	stupeň
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
tak	tak	k9	tak
neděje	dít	k5eNaImIp3nS	dít
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
po	po	k7c6	po
5	[number]	k4	5
letech	léto	k1gNnPc6	léto
od	od	k7c2	od
jmenování	jmenování	k1gNnSc2	jmenování
do	do	k7c2	do
prvního	první	k4xOgInSc2	první
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Úřadující	úřadující	k2eAgMnPc1d1	úřadující
politici	politik	k1gMnPc1	politik
a	a	k8xC	a
soudci	soudce	k1gMnPc1	soudce
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
vyloučeni	vyloučit	k5eAaPmNgMnP	vyloučit
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
bílé	bílý	k2eAgFnSc2d1	bílá
sněženky	sněženka	k1gFnSc2	sněženka
s	s	k7c7	s
javorovým	javorový	k2eAgInSc7d1	javorový
listem	list	k1gInSc7	list
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
stupně	stupeň	k1gInSc2	stupeň
červený	červený	k2eAgInSc1d1	červený
<g/>
,	,	kIx,	,
u	u	k7c2	u
druhého	druhý	k4xOgInSc2	druhý
stupně	stupeň	k1gInSc2	stupeň
zlatý	zlatý	k2eAgMnSc1d1	zlatý
a	a	k8xC	a
u	u	k7c2	u
třetího	třetí	k4xOgNnSc2	třetí
stříbrný	stříbrný	k2eAgMnSc1d1	stříbrný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rytíř	Rytíř	k1gMnSc1	Rytíř
==	==	k?	==
</s>
</p>
<p>
<s>
Rytíř	Rytíř	k1gMnSc1	Rytíř
Řádu	řád	k1gInSc2	řád
Kanady	Kanada	k1gFnSc2	Kanada
(	(	kIx(	(
<g/>
Companion	Companion	k1gInSc1	Companion
of	of	k?	of
the	the	k?	the
Order	Order	k1gInSc1	Order
of	of	k?	of
Canada	Canada	k1gFnSc1	Canada
<g/>
)	)	kIx)	)
–	–	k?	–
C.C.	C.C.	k1gFnSc1	C.C.
</s>
</p>
<p>
<s>
prokázal	prokázat	k5eAaPmAgInS	prokázat
největší	veliký	k2eAgFnSc4d3	veliký
zásluhy	zásluha	k1gFnPc4	zásluha
o	o	k7c4	o
Kanadu	Kanada	k1gFnSc4	Kanada
a	a	k8xC	a
humanitu	humanita	k1gFnSc4	humanita
na	na	k7c6	na
národní	národní	k2eAgFnSc6d1	národní
nebo	nebo	k8xC	nebo
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
scéně	scéna	k1gFnSc6	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
je	být	k5eAaImIp3nS	být
vyznamenáno	vyznamenat	k5eAaPmNgNnS	vyznamenat
maximálně	maximálně	k6eAd1	maximálně
15	[number]	k4	15
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
i	i	k9	i
limit	limit	k1gInSc4	limit
165	[number]	k4	165
žijících	žijící	k2eAgMnPc2d1	žijící
členů	člen	k1gMnPc2	člen
Řádu	řád	k1gInSc2	řád
Kanady	Kanada	k1gFnSc2	Kanada
tohoto	tento	k3xDgInSc2	tento
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Generálnímu	generální	k2eAgMnSc3d1	generální
guvernéru	guvernér	k1gMnSc3	guvernér
Kanady	Kanada	k1gFnSc2	Kanada
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
stupeň	stupeň	k1gInSc1	stupeň
propůjčen	propůjčen	k2eAgInSc1d1	propůjčen
automaticky	automaticky	k6eAd1	automaticky
při	při	k7c6	při
nástupu	nástup	k1gInSc6	nástup
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
a	a	k8xC	a
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
udělit	udělit	k5eAaPmF	udělit
tento	tento	k3xDgInSc4	tento
stupeň	stupeň	k1gInSc4	stupeň
i	i	k8xC	i
své	svůj	k3xOyFgFnSc3	svůj
manželce	manželka	k1gFnSc3	manželka
<g/>
/	/	kIx~	/
<g/>
lovi	lov	k1gInSc3	lov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Důstojník	důstojník	k1gMnSc1	důstojník
==	==	k?	==
</s>
</p>
<p>
<s>
Důstojník	důstojník	k1gMnSc1	důstojník
Řádu	řád	k1gInSc2	řád
Kanady	Kanada	k1gFnSc2	Kanada
(	(	kIx(	(
<g/>
Officer	Officer	k1gInSc1	Officer
of	of	k?	of
the	the	k?	the
Order	Order	k1gInSc1	Order
of	of	k?	of
Canada	Canada	k1gFnSc1	Canada
<g/>
)	)	kIx)	)
–	–	k?	–
O.C.	O.C.	k1gFnSc1	O.C.
</s>
</p>
<p>
<s>
prokázal	prokázat	k5eAaPmAgInS	prokázat
mimořádné	mimořádný	k2eAgFnPc4d1	mimořádná
zásluhy	zásluha	k1gFnPc4	zásluha
o	o	k7c4	o
Kanadu	Kanada	k1gFnSc4	Kanada
nebo	nebo	k8xC	nebo
Kanaďany	Kanaďan	k1gMnPc4	Kanaďan
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stupeň	stupeň	k1gInSc1	stupeň
je	být	k5eAaImIp3nS	být
ročně	ročně	k6eAd1	ročně
udělen	udělen	k2eAgMnSc1d1	udělen
až	až	k8xS	až
64	[number]	k4	64
lidem	lid	k1gInSc7	lid
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
stupeň	stupeň	k1gInSc1	stupeň
udělen	udělit	k5eAaPmNgInS	udělit
1	[number]	k4	1
006	[number]	k4	006
žijícím	žijící	k2eAgInSc7d1	žijící
lidem	lid	k1gInSc7	lid
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
zde	zde	k6eAd1	zde
žádný	žádný	k3yNgInSc1	žádný
početní	početní	k2eAgInSc1d1	početní
limit	limit	k1gInSc1	limit
žijících	žijící	k2eAgMnPc2d1	žijící
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Člen	člen	k1gMnSc1	člen
==	==	k?	==
</s>
</p>
<p>
<s>
Člen	člen	k1gMnSc1	člen
Řádu	řád	k1gInSc2	řád
Kanady	Kanada	k1gFnSc2	Kanada
(	(	kIx(	(
<g/>
Member	Member	k1gInSc1	Member
of	of	k?	of
the	the	k?	the
Order	Order	k1gInSc1	Order
of	of	k?	of
Canada	Canada	k1gFnSc1	Canada
<g/>
)	)	kIx)	)
–	–	k?	–
C.	C.	kA	C.
<g/>
M.	M.	kA	M.
</s>
</p>
<p>
<s>
prokázal	prokázat	k5eAaPmAgMnS	prokázat
mimořádný	mimořádný	k2eAgInSc4d1	mimořádný
přínos	přínos	k1gInSc4	přínos
Kanadě	Kanada	k1gFnSc3	Kanada
nebo	nebo	k8xC	nebo
Kanaďanům	Kanaďan	k1gMnPc3	Kanaďan
na	na	k7c6	na
lokální	lokální	k2eAgFnSc6d1	lokální
<g/>
,	,	kIx,	,
regionální	regionální	k2eAgFnSc6d1	regionální
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stupeň	stupeň	k1gInSc1	stupeň
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
udělen	udělit	k5eAaPmNgInS	udělit
až	až	k9	až
136	[number]	k4	136
lidem	člověk	k1gMnPc3	člověk
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
zde	zde	k6eAd1	zde
žádný	žádný	k3yNgInSc1	žádný
limit	limit	k1gInSc1	limit
počtu	počet	k1gInSc2	počet
žijících	žijící	k2eAgInPc2d1	žijící
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
stupeň	stupeň	k1gInSc1	stupeň
udělen	udělit	k5eAaPmNgInS	udělit
1	[number]	k4	1
964	[number]	k4	964
žijícím	žijící	k2eAgMnSc7d1	žijící
členům	člen	k1gMnPc3	člen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
řádů	řád	k1gInPc2	řád
a	a	k8xC	a
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
</s>
</p>
