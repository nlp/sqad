<s>
Řád	řád	k1gInSc1	řád
Kanady	Kanada	k1gFnSc2	Kanada
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Order	Order	k1gInSc1	Order
of	of	k?	of
Canada	Canada	k1gFnSc1	Canada
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
civilní	civilní	k2eAgNnSc1d1	civilní
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
udílen	udílen	k2eAgInSc1d1	udílen
těm	ten	k3xDgFnPc3	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
dostáli	dostát	k5eAaPmAgMnP	dostát
mottu	motto	k1gNnSc6	motto
<g/>
:	:	kIx,	:
Desiderantes	Desiderantes	k1gMnSc1	Desiderantes
meliorem	melior	k1gInSc7	melior
patriam	patriam	k6eAd1	patriam
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
<g/>
]	]	kIx)	]
co	co	k8xS	co
se	se	k3xPyFc4	se
zasloužili	zasloužit	k5eAaPmAgMnP	zasloužit
o	o	k7c6	o
lepší	dobrý	k2eAgFnSc6d2	lepší
zemi	zem	k1gFnSc6	zem
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
