<s>
Dennis	Dennis	k1gFnSc1
Rader	Radra	k1gFnPc2
</s>
<s>
Dennis	Dennis	k1gInSc1
Lynn	Lynn	k1gMnSc1
Rader	Rader	k1gMnSc1
</s>
<s>
Dennis	Dennis	k1gFnSc2
Lynn	Lynna	k1gFnPc2
RaderZákladní	RaderZákladný	k2eAgMnPc1d1
údaje	údaj	k1gInPc4
Alias	alias	k9
</s>
<s>
BTK	BTK	kA
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1945	#num#	k4
(	(	kIx(
<g/>
76	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Pittsburgh	Pittsburgh	k1gInSc1
<g/>
,	,	kIx,
Kansas	Kansas	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
Zatčení	zatčení	k1gNnSc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2005	#num#	k4
Výše	vysoce	k6eAd2
trestu	trest	k1gInSc3
</s>
<s>
Doživotí	doživotí	k1gNnSc1
bez	bez	k7c2
možnosti	možnost	k1gFnSc2
podmíněčného	podmíněčný	k2eAgNnSc2d1
propustění	propustění	k1gNnSc2
na	na	k7c4
175	#num#	k4
let	léto	k1gNnPc2
Oběť	oběť	k1gFnSc1
Modus	modus	k1gInSc4
operandi	operand	k1gMnPc5
</s>
<s>
svazování	svazování	k1gNnSc1
<g/>
,	,	kIx,
mučení	mučení	k1gNnSc1
Počet	počet	k1gInSc1
obětí	oběť	k1gFnPc2
</s>
<s>
10	#num#	k4
Období	období	k1gNnSc1
vraždění	vraždění	k1gNnSc4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1974	#num#	k4
–	–	k?
19	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1991	#num#	k4
Stát	stát	k1gInSc1
vraždění	vraždění	k1gNnSc2
</s>
<s>
USA	USA	kA
Místo	místo	k7c2
vraždy	vražda	k1gFnSc2
</s>
<s>
Wichita	Wichita	k1gFnSc1
<g/>
,	,	kIx,
Kansas	Kansas	k1gInSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dennis	Dennis	k1gInSc1
Lynn	Lynn	k1gMnSc1
Rader	Rader	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
9	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1945	#num#	k4
Pittsburgh	Pittsburgh	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
americký	americký	k2eAgMnSc1d1
sériový	sériový	k2eAgMnSc1d1
vrah	vrah	k1gMnSc1
známý	známý	k1gMnSc1
jako	jako	k8xC,k8xS
BTK	BTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rader	Rader	k1gMnSc1
se	se	k3xPyFc4
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
dopisech	dopis	k1gInPc6
adresovaných	adresovaný	k2eAgFnPc2d1
policii	policie	k1gFnSc3
podepisoval	podepisovat	k5eAaImAgMnS
písmeny	písmeno	k1gNnPc7
„	„	k?
<g/>
BTK	BTK	kA
<g/>
“	“	k?
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
pro	pro	k7c4
„	„	k?
<g/>
svázat	svázat	k5eAaPmF
<g/>
,	,	kIx,
mučit	mučit	k5eAaImF
<g/>
,	,	kIx,
zabíjet	zabíjet	k5eAaImF
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1974	#num#	k4
až	až	k9
1991	#num#	k4
Rader	Rader	k1gMnSc1
zabil	zabít	k5eAaPmAgMnS
10	#num#	k4
lidí	člověk	k1gMnPc2
ve	v	k7c6
městě	město	k1gNnSc6
Wichita	Wichitum	k1gNnSc2
<g/>
,	,	kIx,
Kansas	Kansas	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rader	Rader	k1gInSc1
posílal	posílat	k5eAaImAgInS
policistům	policista	k1gMnPc3
a	a	k8xC
novinářům	novinář	k1gMnPc3
výsměšné	výsměšný	k2eAgInPc1d1
dopisy	dopis	k1gInPc1
popisující	popisující	k2eAgFnSc2d1
podrobnosti	podrobnost	k1gFnSc2
jeho	jeho	k3xOp3gInPc2
zločinů	zločin	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
<g/>
,	,	kIx,
deset	deset	k4xCc1
let	léto	k1gNnPc2
po	po	k7c6
poslední	poslední	k2eAgFnSc6d1
vraždě	vražda	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c4
sebe	sebe	k3xPyFc4
začal	začít	k5eAaPmAgMnS
Rader	Rader	k1gMnSc1
znovu	znovu	k6eAd1
upozorňovat	upozorňovat	k5eAaImF
prostřednictvím	prostřednictvím	k7c2
dopisů	dopis	k1gInPc2
zaslaných	zaslaný	k2eAgInPc2d1
policistům	policista	k1gMnPc3
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
následně	následně	k6eAd1
dopomohly	dopomoct	k5eAaPmAgFnP
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
zatčení	zatčení	k1gNnSc3
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nápravném	nápravný	k2eAgInSc6d1
ústavu	ústav	k1gInSc6
v	v	k7c6
Kansasu	Kansas	k1gInSc6
si	se	k3xPyFc3
Rader	Rader	k1gInSc1
nyní	nyní	k6eAd1
odpykává	odpykávat	k5eAaImIp3nS
deset	deset	k4xCc4
po	po	k7c6
sobě	sebe	k3xPyFc6
jdoucích	jdoucí	k2eAgInPc2d1
doživotních	doživotní	k2eAgInPc2d1
trestů	trest	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
Dennis	Dennis	k1gInSc1
Rader	Rader	k1gInSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgInS
9	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
jedním	jeden	k4xCgMnSc7
ze	z	k7c2
čtyř	čtyři	k4xCgMnPc2
synů	syn	k1gMnPc2
matky	matka	k1gFnSc2
Dorothee	Dorothe	k1gMnSc2
a	a	k8xC
otce	otec	k1gMnSc2
Williama	William	k1gMnSc2
<g/>
;	;	kIx,
jeho	jeho	k3xOp3gMnPc1
bratři	bratr	k1gMnPc1
se	se	k3xPyFc4
jmenují	jmenovat	k5eAaImIp3nP,k5eAaBmIp3nP
Paul	Paul	k1gMnSc1
<g/>
,	,	kIx,
Bill	Bill	k1gMnSc1
a	a	k8xC
Jeff	Jeff	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Přestože	přestože	k8xS
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
v	v	k7c6
Pittsburgu	Pittsburg	k1gInSc6
v	v	k7c6
Kansasu	Kansas	k1gInSc6
<g/>
,	,	kIx,
vyrostl	vyrůst	k5eAaPmAgMnS
ve	v	k7c6
Wichitě	Wichita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
práci	práce	k1gFnSc3
se	se	k3xPyFc4
rodiče	rodič	k1gMnPc1
Raderovi	Raderův	k2eAgMnPc1d1
<g/>
,	,	kIx,
ani	ani	k8xC
jeho	jeho	k3xOp3gFnSc1
třem	tři	k4xCgMnPc3
bratrům	bratr	k1gMnPc3
<g/>
,	,	kIx,
příliš	příliš	k6eAd1
nevěnovali	věnovat	k5eNaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rader	Radra	k1gFnPc2
později	pozdě	k6eAd2
během	během	k7c2
vyšetřování	vyšetřování	k1gNnSc2
popsal	popsat	k5eAaPmAgMnS
zejména	zejména	k9
svůj	svůj	k3xOyFgInSc4
špatný	špatný	k2eAgInSc4d1
vztah	vztah	k1gInSc4
k	k	k7c3
matce	matka	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
mu	on	k3xPp3gMnSc3
nevěnovala	věnovat	k5eNaImAgFnS
dostatek	dostatek	k1gInSc4
pozornosti	pozornost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Už	už	k6eAd1
od	od	k7c2
mládí	mládí	k1gNnSc2
se	se	k3xPyFc4
v	v	k7c6
Raderovi	Rader	k1gMnSc6
projevovaly	projevovat	k5eAaImAgInP
sklony	sklon	k1gInPc4
k	k	k7c3
sadismu	sadismus	k1gInSc3
a	a	k8xC
měl	mít	k5eAaImAgMnS
sexuální	sexuální	k2eAgFnSc2d1
fantazie	fantazie	k1gFnSc2
o	o	k7c6
mučení	mučení	k1gNnSc6
„	„	k?
<g/>
uvězněných	uvězněný	k2eAgMnPc2d1
a	a	k8xC
bezmocných	bezmocný	k2eAgFnPc2d1
<g/>
“	“	k?
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Dále	daleko	k6eAd2
si	se	k3xPyFc3
také	také	k9
našel	najít	k5eAaPmAgMnS
zálibu	záliba	k1gFnSc4
v	v	k7c6
mučení	mučení	k1gNnSc6
<g/>
,	,	kIx,
zabíjení	zabíjení	k1gNnSc6
a	a	k8xC
věšení	věšení	k1gNnSc6
malých	malý	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Rader	Rader	k1gInSc1
projevoval	projevovat	k5eAaImAgInS
známky	známka	k1gFnSc2
několika	několik	k4yIc2
sexuálních	sexuální	k2eAgFnPc2d1
fetiší	fetiš	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
voyeurismu	voyeurismus	k1gInSc2
nebo	nebo	k8xC
uspokojování	uspokojování	k1gNnSc2
pomocí	pomocí	k7c2
autoerotického	autoerotický	k2eAgNnSc2d1
zadušení	zadušení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
tajně	tajně	k6eAd1
sledoval	sledovat	k5eAaImAgMnS
ženy	žena	k1gFnPc4
ze	z	k7c2
sousedství	sousedství	k1gNnSc2
<g/>
,	,	kIx,
během	během	k7c2
čehož	což	k3yQnSc2,k3yRnSc2
se	se	k3xPyFc4
sám	sám	k3xTgMnSc1
rád	rád	k2eAgMnSc1d1
oblékal	oblékat	k5eAaImAgMnS
do	do	k7c2
ženského	ženský	k2eAgNnSc2d1
oblečení	oblečení	k1gNnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
kradeného	kradený	k2eAgNnSc2d1
dámského	dámský	k2eAgNnSc2d1
spodního	spodní	k2eAgNnSc2d1
prádla	prádlo	k1gNnSc2
a	a	k8xC
masturboval	masturbovat	k5eAaImAgMnS
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
si	se	k3xPyFc3
vázal	vázat	k5eAaImAgMnS
provazy	provaz	k1gInPc4
kolem	kolem	k7c2
paží	paží	k1gNnSc2
a	a	k8xC
krku	krk	k1gInSc2
pro	pro	k7c4
ještě	ještě	k6eAd1
větší	veliký	k2eAgNnSc4d2
uspokojení	uspokojení	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
O	o	k7c4
několik	několik	k4yIc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
<g/>
,	,	kIx,
se	se	k3xPyFc4
v	v	k7c6
době	doba	k1gFnSc6
mezi	mezi	k7c7
vraždami	vražda	k1gFnPc7
fotil	fotit	k5eAaImAgInS
svázaný	svázaný	k2eAgInSc1d1
<g/>
,	,	kIx,
převlečený	převlečený	k2eAgInSc1d1
do	do	k7c2
ženského	ženský	k2eAgNnSc2d1
oblečení	oblečení	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
s	s	k7c7
maskou	maska	k1gFnSc7
představující	představující	k2eAgInSc1d1
ženský	ženský	k2eAgInSc1d1
obličej	obličej	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rader	Radra	k1gFnPc2
později	pozdě	k6eAd2
přiznal	přiznat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
jako	jako	k9
součást	součást	k1gFnSc1
sexuální	sexuální	k2eAgFnSc1d1
fantazie	fantazie	k1gFnSc1
si	se	k3xPyFc3
představoval	představovat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
ze	z	k7c2
svých	svůj	k3xOyFgFnPc2
obětí	oběť	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Rader	Radero	k1gNnPc2
ovšem	ovšem	k9
své	svůj	k3xOyFgInPc4
sexuální	sexuální	k2eAgInPc4d1
a	a	k8xC
sadistické	sadistický	k2eAgInPc4d1
sklony	sklon	k1gInPc4
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
skrýval	skrývat	k5eAaImAgInS
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
byl	být	k5eAaImAgInS
mezi	mezi	k7c7
známými	známý	k1gMnPc7
a	a	k8xC
sousedy	soused	k1gMnPc7
považován	považován	k2eAgInSc1d1
za	za	k7c2
přátelského	přátelský	k2eAgInSc2d1
a	a	k8xC
zdvořilého	zdvořilý	k2eAgInSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
střední	střední	k2eAgFnSc6d1
škole	škola	k1gFnSc6
šel	jít	k5eAaImAgMnS
Rader	Rader	k1gInSc4
na	na	k7c4
univerzitu	univerzita	k1gFnSc4
v	v	k7c6
Kansasu	Kansas	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
průměrné	průměrný	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
jej	on	k3xPp3gMnSc4
po	po	k7c6
roce	rok	k1gInSc6
studování	studování	k1gNnSc2
donutily	donutit	k5eAaPmAgFnP
univerzity	univerzita	k1gFnPc1
zanechat	zanechat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
mezi	mezi	k7c7
1966	#num#	k4
a	a	k8xC
1970	#num#	k4
byl	být	k5eAaImAgInS
součástí	součást	k1gFnSc7
v	v	k7c4
letectva	letectvo	k1gNnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
propuštění	propuštění	k1gNnSc6
se	se	k3xPyFc4
přestěhoval	přestěhovat	k5eAaPmAgInS
do	do	k7c2
Park	park	k1gInSc4
City	City	k1gFnSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
pracoval	pracovat	k5eAaImAgMnS
v	v	k7c6
supermarketu	supermarket	k1gInSc6
v	v	k7c6
oddělení	oddělení	k1gNnSc6
masa	maso	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
22	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1971	#num#	k4
se	se	k3xPyFc4
<g />
.	.	kIx.
</s>
<s hack="1">
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Paulou	Paula	k1gFnSc7
Dietz	Dietza	k1gFnPc2
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yIgFnSc7,k3yRgFnSc7,k3yQgFnSc7
má	mít	k5eAaImIp3nS
dvě	dva	k4xCgFnPc1
děti	dítě	k1gFnPc1
<g/>
,	,	kIx,
Kerri	Kerr	k1gFnPc1
a	a	k8xC
Briana	Briana	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Poté	poté	k6eAd1
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
zpátky	zpátky	k6eAd1
na	na	k7c4
školu	škola	k1gFnSc4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1973	#num#	k4
získal	získat	k5eAaPmAgInS
titul	titul	k1gInSc4
v	v	k7c6
oboru	obor	k1gInSc6
elektroniky	elektronika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
odpromoval	odpromovat	k5eAaPmAgMnS
na	na	k7c6
Státní	státní	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
ve	v	k7c6
Wichitě	Wichita	k1gFnSc6
s	s	k7c7
bakalářským	bakalářský	k2eAgInSc7d1
titulem	titul	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Rader	Rader	k1gMnSc1
pracoval	pracovat	k5eAaImAgMnS
jako	jako	k9
montér	montér	k1gMnSc1
pro	pro	k7c4
zásobovací	zásobovací	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
Coleman	Coleman	k1gMnSc1
Company	Compana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1974	#num#	k4
až	až	k9
1988	#num#	k4
pracoval	pracovat	k5eAaImAgInS
v	v	k7c6
kancelářích	kancelář	k1gFnPc6
ADT	ADT	kA
Security	Securit	k1gInPc1
Services	Services	k1gInSc1
se	s	k7c7
sídlem	sídlo	k1gNnSc7
ve	v	k7c6
Wichitě	Wichita	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
rámci	rámec	k1gInSc6
své	svůj	k3xOyFgFnSc2
práce	práce	k1gFnSc2
instaloval	instalovat	k5eAaBmAgMnS
bezpečnostní	bezpečnostní	k2eAgInPc4d1
systémy	systém	k1gInPc4
<g/>
,	,	kIx,
v	v	k7c6
mnoha	mnoho	k4c6
případech	případ	k1gInPc6
byl	být	k5eAaImAgInS
pro	pro	k7c4
jeho	jeho	k3xOp3gMnPc4
klienty	klient	k1gMnPc4
důvodem	důvod	k1gInSc7
právě	právě	k9
strach	strach	k1gInSc4
z	z	k7c2
vraha	vrah	k1gMnSc2
zvaného	zvaný	k2eAgInSc2d1
BTK	BTK	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
květnu	květen	k1gInSc6
roku	rok	k1gInSc2
1991	#num#	k4
byl	být	k5eAaImAgInS
Rader	Rader	k1gInSc1
pověřen	pověřit	k5eAaPmNgInS
odchytem	odchyt	k1gInSc7
psů	pes	k1gMnPc2
v	v	k7c4
Park	park	k1gInSc4
City	City	k1gFnSc2
a	a	k8xC
také	také	k9
ve	v	k7c6
městě	město	k1gNnSc6
dohlížel	dohlížet	k5eAaImAgInS
na	na	k7c4
dodržování	dodržování	k1gNnSc4
předpisů	předpis	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Sousedé	soused	k1gMnPc1
o	o	k7c6
Raderovi	Rader	k1gMnSc3
vypověděli	vypovědět	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
při	při	k7c6
práci	práce	k1gFnSc6
občas	občas	k6eAd1
přehnaně	přehnaně	k6eAd1
reagoval	reagovat	k5eAaBmAgInS
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
nepřiměřeně	přiměřeně	k6eNd1
přísný	přísný	k2eAgInSc1d1
a	a	k8xC
také	také	k9
se	se	k3xPyFc4
podivuhodně	podivuhodně	k6eAd1
vyžíval	vyžívat	k5eAaImAgMnS
v	v	k7c6
šikaně	šikana	k1gFnSc6
a	a	k8xC
obtěžování	obtěžování	k1gNnSc6
svobodných	svobodný	k2eAgFnPc2d1
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
sousedka	sousedka	k1gFnSc1
si	se	k3xPyFc3
stěžovala	stěžovat	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
bezdůvodně	bezdůvodně	k6eAd1
zabil	zabít	k5eAaPmAgMnS
jejího	její	k3xOp3gMnSc4
psa	pes	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vraždy	vražda	k1gFnPc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1974	#num#	k4
byli	být	k5eAaImAgMnP
ve	v	k7c6
Wichitě	Wichita	k1gFnSc6
v	v	k7c6
Kansasu	Kansas	k1gInSc6
zavražděni	zavraždit	k5eAaPmNgMnP
čtyři	čtyři	k4xCgMnPc1
členové	člen	k1gMnPc1
rodiny	rodina	k1gFnSc2
Oterových	Oterových	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Oběťmi	oběť	k1gFnPc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Joseph	Joseph	k1gInSc4
Otero	Otero	k1gNnSc4
s	s	k7c7
manželkou	manželka	k1gFnSc7
Julií	Julie	k1gFnSc7
a	a	k8xC
jejich	jejich	k3xOp3gNnSc7
dvěma	dva	k4xCgFnPc7
dětmi	dítě	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
těla	tělo	k1gNnSc2
objevil	objevit	k5eAaPmAgMnS
nejstarší	starý	k2eAgMnSc1d3
syn	syn	k1gMnSc1
Charlie	Charlie	k1gMnSc1
Otero	Otero	k1gNnSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
době	doba	k1gFnSc6
vraždy	vražda	k1gFnSc2
ve	v	k7c6
škole	škola	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
byl	být	k5eAaImAgInS
Rader	Rader	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
zatčený	zatčený	k1gMnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
k	k	k7c3
vraždě	vražda	k1gFnSc3
rodiny	rodina	k1gFnSc2
Oterových	Oterových	k2eAgMnSc1d1
přiznal	přiznat	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
Rader	Rader	k1gMnSc1
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
dopis	dopis	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
schoval	schovat	k5eAaPmAgInS
v	v	k7c6
knize	kniha	k1gFnSc6
uložené	uložený	k2eAgFnSc2d1
ve	v	k7c6
wichitské	wichitský	k2eAgFnSc6d1
veřejné	veřejný	k2eAgFnSc6d1
knihovně	knihovna	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
podrobně	podrobně	k6eAd1
popisuje	popisovat	k5eAaImIp3nS
detaily	detail	k1gInPc4
vraždy	vražda	k1gFnSc2
členů	člen	k1gMnPc2
rodiny	rodina	k1gFnSc2
Oterových	Oterových	k2eAgFnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
poslal	poslat	k5eAaPmAgInS
další	další	k2eAgInSc1d1
dopis	dopis	k1gInSc1
televizní	televizní	k2eAgFnSc4d1
stanici	stanice	k1gFnSc4
KAKE	KAKE	kA
ve	v	k7c6
Wichitě	Wichita	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
se	se	k3xPyFc4
přiznal	přiznat	k5eAaPmAgMnS
k	k	k7c3
vraždám	vražda	k1gFnPc3
Oterových	Oterová	k1gFnPc2
<g/>
,	,	kIx,
Kathryn	Kathryn	k1gNnSc4
Brightové	Brightová	k1gFnSc2
<g/>
,	,	kIx,
Shirley	Shirlea	k1gFnSc2
Vian	Viana	k1gFnPc2
Relfordové	Relfordová	k1gFnSc2
a	a	k8xC
Nancy	Nancy	k1gFnSc2
Foxové	Foxové	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Dále	daleko	k6eAd2
v	v	k7c6
tomto	tento	k3xDgInSc6
dopise	dopis	k1gInSc6
sám	sám	k3xTgMnSc1
pro	pro	k7c4
sebe	sebe	k3xPyFc4
navrhl	navrhnout	k5eAaPmAgMnS
mnoho	mnoho	k4c4
možných	možný	k2eAgFnPc2d1
přezdívek	přezdívka	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
zkratky	zkratka	k1gFnSc2
BTK	BTK	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
se	se	k3xPyFc4
Raderovi	Raderův	k2eAgMnPc1d1
nevydařila	vydařit	k5eNaPmAgFnS
plánovaná	plánovaný	k2eAgFnSc1d1
vražda	vražda	k1gFnSc1
Anny	Anna	k1gFnSc2
Williamsové	Williamsová	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
čekal	čekat	k5eAaImAgMnS
u	u	k7c2
ní	on	k3xPp3gFnSc2
doma	doma	k6eAd1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
Anna	Anna	k1gFnSc1
se	se	k3xPyFc4
zdržela	zdržet	k5eAaPmAgFnS
u	u	k7c2
přátel	přítel	k1gMnPc2
a	a	k8xC
Rader	Radra	k1gFnPc2
čekání	čekání	k1gNnSc4
vzdal	vzdát	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rader	Rader	k1gInSc1
u	u	k7c2
výpovědi	výpověď	k1gFnSc2
přiznal	přiznat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgInS
vzteky	vztek	k1gInPc4
bez	bez	k7c2
sebe	sebe	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobné	podobný	k2eAgInPc1d1
štěstí	štěstit	k5eAaImIp3nP
ovšem	ovšem	k9
neměla	mít	k5eNaImAgNnP
Marine	Marin	k1gInSc5
Hedgeová	Hedgeová	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
ve	v	k7c4
svých	svůj	k3xOyFgFnPc2
53	#num#	k4
letech	let	k1gInPc6
nalezena	nalézt	k5eAaBmNgFnS,k5eAaPmNgFnS
zavražděná	zavražděný	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rader	Rader	k1gInSc1
přenesl	přenést	k5eAaPmAgInS
její	její	k3xOp3gNnSc4
tělo	tělo	k1gNnSc4
do	do	k7c2
kostela	kostel	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
sám	sám	k3xTgMnSc1
navštěvoval	navštěvovat	k5eAaImAgMnS
<g/>
,	,	kIx,
a	a	k8xC
vyfotografoval	vyfotografovat	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
svázanou	svázaný	k2eAgFnSc4d1
v	v	k7c6
různých	různý	k2eAgFnPc6d1
pozicích	pozice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
bylo	být	k5eAaImAgNnS
vyšetřování	vyšetřování	k1gNnSc1
vraha	vrah	k1gMnSc2
BTK	BTK	kA
odloženo	odložit	k5eAaPmNgNnS
z	z	k7c2
důvodu	důvod	k1gInSc2
nedostatku	nedostatek	k1gInSc2
informací	informace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rader	Radra	k1gFnPc2
poté	poté	k6eAd1
ale	ale	k8xC
znovu	znovu	k6eAd1
začal	začít	k5eAaPmAgMnS
komunikovat	komunikovat	k5eAaImF
s	s	k7c7
médii	médium	k1gNnPc7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
následně	následně	k6eAd1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
zatčení	zatčení	k1gNnSc3
v	v	k7c6
únoru	únor	k1gInSc6
roku	rok	k1gInSc2
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jednom	jeden	k4xCgInSc6
z	z	k7c2
dopisů	dopis	k1gInPc2
se	se	k3xPyFc4
doznal	doznat	k5eAaPmAgInS
k	k	k7c3
vraždě	vražda	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
BTK	BTK	kA
ještě	ještě	k6eAd1
nebyla	být	k5eNaImAgFnS
přisuzována	přisuzovat	k5eAaImNgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Krátce	krátce	k6eAd1
před	před	k7c7
zatčením	zatčení	k1gNnSc7
byl	být	k5eAaImAgInS
Rader	Rader	k1gInSc1
velmi	velmi	k6eAd1
aktivní	aktivní	k2eAgMnSc1d1
a	a	k8xC
posílal	posílat	k5eAaImAgMnS
novinářům	novinář	k1gMnPc3
balíky	balík	k1gInPc4
s	s	k7c7
dopisy	dopis	k1gInPc7
popisující	popisující	k2eAgInPc4d1
detaily	detail	k1gInPc4
vražd	vražda	k1gFnPc2
a	a	k8xC
se	s	k7c7
znepokojujícími	znepokojující	k2eAgFnPc7d1
fotkami	fotka	k1gFnPc7
svázaných	svázaný	k2eAgFnPc2d1
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
veřejnost	veřejnost	k1gFnSc4
se	se	k3xPyFc4
později	pozdě	k6eAd2
dostaly	dostat	k5eAaPmAgFnP
i	i	k9
detaily	detail	k1gInPc4
z	z	k7c2
Raderovy	Raderův	k2eAgFnSc2d1
údajné	údajný	k2eAgFnSc2d1
autobiografie	autobiografie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
součástí	součást	k1gFnSc7
jednoho	jeden	k4xCgInSc2
z	z	k7c2
balíčků	balíček	k1gInPc2
<g/>
,	,	kIx,
obsahující	obsahující	k2eAgFnPc4d1
informace	informace	k1gFnPc4
z	z	k7c2
Raderova	Raderův	k2eAgInSc2d1
osobního	osobní	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Pohlaví	pohlaví	k1gNnSc1
</s>
<s>
Věk	věk	k1gInSc1
</s>
<s>
Datum	datum	k1gNnSc1
úmrtí	úmrtí	k1gNnSc2
</s>
<s>
Místo	místo	k7c2
vraždy	vražda	k1gFnSc2
</s>
<s>
Příčina	příčina	k1gFnSc1
smrti	smrt	k1gFnSc2
</s>
<s>
Použitá	použitý	k2eAgFnSc1d1
zbraň	zbraň	k1gFnSc1
</s>
<s>
Joseph	Joseph	k1gInSc1
Otero	Otero	k1gNnSc1
</s>
<s>
Muž	muž	k1gMnSc1
</s>
<s>
38	#num#	k4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1974	#num#	k4
</s>
<s>
803	#num#	k4
North	North	k1gMnSc1
Edgemoor	Edgemoor	k1gMnSc1
Street	Street	k1gMnSc1
<g/>
,	,	kIx,
Wichita	Wichita	k1gMnSc1
</s>
<s>
Udušení	udušení	k1gNnSc1
</s>
<s>
Plastový	plastový	k2eAgInSc1d1
pytel	pytel	k1gInSc1
</s>
<s>
Julia	Julius	k1gMnSc2
Maria	Mario	k1gMnSc2
Oterová	Oterová	k1gFnSc1
</s>
<s>
Žena	žena	k1gFnSc1
</s>
<s>
33	#num#	k4
</s>
<s>
Uškrcení	uškrcení	k1gNnSc1
</s>
<s>
Provaz	provaz	k1gInSc1
</s>
<s>
Joseph	Joseph	k1gMnSc1
Otero	Otero	k1gNnSc1
<g/>
,	,	kIx,
Ml.	ml.	kA
</s>
<s>
Muž	muž	k1gMnSc1
</s>
<s>
9	#num#	k4
</s>
<s>
Udušení	udušení	k1gNnSc1
</s>
<s>
Plastový	plastový	k2eAgInSc1d1
pytel	pytel	k1gInSc1
</s>
<s>
Josephine	Josephinout	k5eAaPmIp3nS
Oterová	Oterová	k1gFnSc1
</s>
<s>
Žena	žena	k1gFnSc1
</s>
<s>
11	#num#	k4
</s>
<s>
Oběšení	oběšení	k1gNnSc1
</s>
<s>
Lano	lano	k1gNnSc1
</s>
<s>
Kathryn	Kathryn	k1gInSc1
Doreen	Doreen	k1gInSc1
Brightová	Brightová	k1gFnSc1
</s>
<s>
Žena	žena	k1gFnSc1
</s>
<s>
21	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1974	#num#	k4
</s>
<s>
3217	#num#	k4
East	East	k1gInSc1
13	#num#	k4
<g/>
th	th	k?
Street	Street	k1gMnSc1
North	North	k1gMnSc1
<g/>
,	,	kIx,
Wichita	Wichita	k1gMnSc1
</s>
<s>
Tři	tři	k4xCgFnPc1
bodné	bodný	k2eAgFnPc1d1
rány	rána	k1gFnPc1
do	do	k7c2
břicha	břicho	k1gNnSc2
</s>
<s>
Nůž	nůž	k1gInSc1
</s>
<s>
Shirley	Shirley	k1gInPc1
Ruth	Ruth	k1gFnSc1
Vian	Vian	k1gInSc1
Relfordová	Relfordová	k1gFnSc1
</s>
<s>
Žena	žena	k1gFnSc1
</s>
<s>
24	#num#	k4
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1977	#num#	k4
</s>
<s>
1311	#num#	k4
South	Southa	k1gFnPc2
Hydraulic	Hydraulice	k1gFnPc2
Street	Street	k1gInSc1
<g/>
,	,	kIx,
Wichita	Wichita	k1gFnSc1
</s>
<s>
Uškrcení	uškrcení	k1gNnSc1
</s>
<s>
Lano	lano	k1gNnSc1
</s>
<s>
Nancy	Nancy	k1gFnSc1
Jo	jo	k9
Foxová	Foxový	k2eAgFnSc1d1
</s>
<s>
Žena	žena	k1gFnSc1
</s>
<s>
25	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1977	#num#	k4
</s>
<s>
843	#num#	k4
South	South	k1gInSc1
Pershing	Pershing	k1gInSc4
Street	Street	k1gMnSc1
<g/>
,	,	kIx,
Wichita	Wichita	k1gMnSc1
</s>
<s>
Uškrcení	uškrcení	k1gNnSc1
</s>
<s>
Opasek	opasek	k1gInSc1
</s>
<s>
Marine	Marinout	k5eAaPmIp3nS
Wallace	Wallace	k1gFnSc1
Hedgeová	Hedgeový	k2eAgFnSc1d1
</s>
<s>
Žena	žena	k1gFnSc1
</s>
<s>
53	#num#	k4
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1985	#num#	k4
</s>
<s>
6254	#num#	k4
North	North	k1gInSc1
Independence	Independence	k1gFnSc2
Street	Streeta	k1gFnPc2
<g/>
,	,	kIx,
</s>
<s>
Park	park	k1gInSc1
City	City	k1gFnSc2
</s>
<s>
Uškrcení	uškrcení	k1gNnSc1
</s>
<s>
Ruce	ruka	k1gFnPc1
</s>
<s>
Vicki	Vicki	k6eAd1
Lynn	Lynn	k1gInSc1
Wegerleová	Wegerleová	k1gFnSc1
</s>
<s>
Žena	žena	k1gFnSc1
</s>
<s>
28	#num#	k4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1986	#num#	k4
</s>
<s>
2404	#num#	k4
West	West	k1gInSc1
13	#num#	k4
<g/>
th	th	k?
Street	Street	k1gMnSc1
North	North	k1gMnSc1
<g/>
,	,	kIx,
Wichita	Wichita	k1gMnSc1
</s>
<s>
Uškrcení	uškrcení	k1gNnSc1
</s>
<s>
Silonová	silonový	k2eAgFnSc1d1
punčocha	punčocha	k1gFnSc1
</s>
<s>
Dolores	Dolores	k1gMnSc1
Earline	Earlin	k1gInSc5
Johnson	Johnson	k1gMnSc1
Davis	Davis	k1gFnSc1
</s>
<s>
Žena	žena	k1gFnSc1
</s>
<s>
62	#num#	k4
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
1991	#num#	k4
</s>
<s>
6226	#num#	k4
North	North	k1gInSc1
Hillside	Hillsid	k1gInSc5
Street	Street	k1gMnSc1
<g/>
,	,	kIx,
Wichita	Wichita	k1gMnSc1
</s>
<s>
Uškrcení	uškrcení	k1gNnSc1
</s>
<s>
Punčochy	punčocha	k1gFnPc1
</s>
<s>
K	k	k7c3
zatčení	zatčení	k1gNnSc3
nakonec	nakonec	k6eAd1
vedla	vést	k5eAaImAgNnP
data	datum	k1gNnPc1
ze	z	k7c2
smazaného	smazaný	k2eAgInSc2d1
dokumentu	dokument	k1gInSc2
na	na	k7c6
disketě	disketa	k1gFnSc6
zaslané	zaslaný	k2eAgFnSc6d1
v	v	k7c6
jednom	jeden	k4xCgMnSc6
z	z	k7c2
balíčků	balíček	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rader	Rader	k1gMnSc1
byl	být	k5eAaImAgMnS
zatčen	zatknout	k5eAaPmNgMnS
při	při	k7c6
řízení	řízení	k1gNnSc6
poblíž	poblíž	k6eAd1
svého	svůj	k3xOyFgInSc2
domu	dům	k1gInSc2
v	v	k7c4
Park	park	k1gInSc4
City	City	k1gFnSc2
krátce	krátce	k6eAd1
po	po	k7c6
poledni	poledne	k1gNnSc6
25	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2005	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Policisté	policista	k1gMnPc1
prohledali	prohledat	k5eAaPmAgMnP
Raderův	Raderův	k2eAgInSc4d1
dům	dům	k1gInSc4
a	a	k8xC
auto	auto	k1gNnSc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
objevili	objevit	k5eAaPmAgMnP
několik	několik	k4yIc4
důkazů	důkaz	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prohledán	prohledán	k2eAgInSc1d1
byl	být	k5eAaImAgInS
také	také	k9
kostel	kostel	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
Rader	Rader	k1gInSc1
navštěvoval	navštěvovat	k5eAaImAgInS
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
kancelář	kancelář	k1gFnSc1
na	na	k7c6
městské	městský	k2eAgFnSc6d1
radnici	radnice	k1gFnSc6
a	a	k8xC
hlavní	hlavní	k2eAgFnSc1d1
pobočka	pobočka	k1gFnSc1
knihovny	knihovna	k1gFnSc2
Park	park	k1gInSc1
City	City	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
tiskové	tiskový	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
následující	následující	k2eAgInSc4d1
den	den	k1gInSc4
ráno	ráno	k6eAd1
policejní	policejní	k2eAgMnSc1d1
náčelník	náčelník	k1gMnSc1
Norman	Norman	k1gMnSc1
Williams	Williams	k1gInSc4
oznámil	oznámit	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Sečteno	sečten	k2eAgNnSc1d1
a	a	k8xC
podtrženo	podtržen	k2eAgNnSc1d1
<g/>
:	:	kIx,
BTK	BTK	kA
je	být	k5eAaImIp3nS
zatčen	zatčen	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
“	“	k?
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2005	#num#	k4
byl	být	k5eAaImAgInS
Rader	Rader	k1gInSc1
obviněn	obvinit	k5eAaPmNgInS
z	z	k7c2
deseti	deset	k4xCc2
vražd	vražda	k1gFnPc2
prvního	první	k4xOgInSc2
stupně	stupeň	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Rader	Rader	k1gInSc1
během	během	k7c2
soudního	soudní	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
spolupracoval	spolupracovat	k5eAaImAgMnS
a	a	k8xC
ochotně	ochotně	k6eAd1
popsal	popsat	k5eAaPmAgMnS
detaily	detail	k1gInPc4
vražd	vražda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
byl	být	k5eAaImAgMnS
odsouzen	odsouzet	k5eAaImNgMnS,k5eAaPmNgMnS
k	k	k7c3
deseti	deset	k4xCc2
doživotním	doživotní	k2eAgInPc3d1
trestům	trest	k1gInPc3
<g/>
,	,	kIx,
tedy	tedy	k8xC
k	k	k7c3
nejméně	málo	k6eAd3
175	#num#	k4
rokům	rok	k1gInPc3
ve	v	k7c4
vězení	vězení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
si	se	k3xPyFc3
trest	trest	k1gInSc1
odpykává	odpykávat	k5eAaImIp3nS
v	v	k7c6
nápravném	nápravný	k2eAgNnSc6d1
zařízení	zařízení	k1gNnSc6
v	v	k7c6
Kansasu	Kansas	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
kultuře	kultura	k1gFnSc6
</s>
<s>
Rader	Rader	k1gMnSc1
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
postav	postava	k1gFnPc2
seriálu	seriál	k1gInSc2
Mindhunter	Mindhunter	k1gInSc4
internetové	internetový	k2eAgFnSc2d1
televize	televize	k1gFnSc2
Netflix	Netflix	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Americký	americký	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
Stephen	Stephna	k1gFnPc2
King	King	k1gMnSc1
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Rader	Rader	k1gInSc4
a	a	k8xC
jeho	jeho	k3xOp3gFnPc4
vraždy	vražda	k1gFnPc4
byli	být	k5eAaImAgMnP
inspirací	inspirace	k1gFnSc7
jeho	jeho	k3xOp3gFnSc2
povídky	povídka	k1gFnSc2
Dobré	dobrý	k2eAgNnSc1d1
manželství	manželství	k1gNnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
vyšla	vyjít	k5eAaPmAgFnS
v	v	k7c6
povídkové	povídkový	k2eAgFnSc6d1
sbírce	sbírka	k1gFnSc6
Černočerná	černočerný	k2eAgFnSc1d1
tma	tma	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Dennis	Dennis	k1gFnSc2
Rader	Radra	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
133	#num#	k4
<g/>
-	-	kIx~
<g/>
71052	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
4665	#num#	k4
<g/>
-	-	kIx~
<g/>
6186	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
285	#num#	k4
<g/>
-	-	kIx~
<g/>
40168	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Ramsland	Ramsland	k1gInSc1
<g/>
,	,	kIx,
pg	pg	k?
<g/>
.	.	kIx.
39	#num#	k4
<g/>
↑	↑	k?
www-	www-	k?
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
kansas	kansasa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
9	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1315370019	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1611689730	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.kansas.com	www.kansas.com	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
media	medium	k1gNnPc4
<g/>
.	.	kIx.
<g/>
kansas	kansasit	k5eAaPmRp2nS
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.kansas.com	www.kansas.com	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
137395	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.highbeam.com	www.highbeam.com	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
14	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.kansas.com	www.kansas.com	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2005	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
abcnews	abcnews	k1gInSc1
<g/>
.	.	kIx.
<g/>
go	go	k?
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
↑	↑	k?
Interview	interview	k1gNnSc1
with	with	k1gMnSc1
Misty	Mista	k1gMnSc2
King	King	k1gMnSc1
<g/>
;	;	kIx,
A	A	kA
<g/>
&	&	k?
<g/>
E	E	kA
Documentary	Documentara	k1gFnPc1
Special	Special	k1gMnSc1
<g/>
—	—	k?
<g/>
The	The	k1gMnSc1
BTK	BTK	kA
Killer	Killer	k1gInSc1
Speaks	Speaks	k1gInSc1
<g/>
1	#num#	k4
2	#num#	k4
www.kansas.com	www.kansas.com	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
62196	#num#	k4
<g/>
-	-	kIx~
<g/>
929	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
62196	#num#	k4
<g/>
-	-	kIx~
<g/>
929	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.scotsman.com	www.scotsman.com	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8389	#num#	k4
<g/>
-	-	kIx~
<g/>
1130	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.cnn.com	www.cnn.com	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
www	www	k?
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
ljworld	ljworlda	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KING	KING	kA
<g/>
,	,	kIx,
Stephen	Stephen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Černočerná	černočerný	k2eAgFnSc1d1
tma	tma	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Dobrovský	Dobrovský	k1gMnSc1
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7593	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
321	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
4840	#num#	k4
6457	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2005075769	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
21879103	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2005075769	#num#	k4
</s>
