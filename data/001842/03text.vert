<s>
Robert	Robert	k1gMnSc1	Robert
Plot	plot	k1gInSc1	plot
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1640	[number]	k4	1640
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1696	[number]	k4	1696
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
chemie	chemie	k1gFnSc2	chemie
a	a	k8xC	a
první	první	k4xOgMnSc1	první
ředitel	ředitel	k1gMnSc1	ředitel
Ashmoleova	Ashmoleův	k2eAgNnSc2d1	Ashmoleův
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Zajímal	zajímat	k5eAaImAgInS	zajímat
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c6	o
nově	nova	k1gFnSc6	nova
se	se	k3xPyFc4	se
rodící	rodící	k2eAgFnSc2d1	rodící
obory	obora	k1gFnSc2	obora
jako	jako	k8xS	jako
archeologie	archeologie	k1gFnSc2	archeologie
(	(	kIx(	(
<g/>
zaměnil	zaměnit	k5eAaPmAgMnS	zaměnit
však	však	k9	však
římské	římský	k2eAgFnPc4d1	římská
památky	památka	k1gFnPc4	památka
za	za	k7c4	za
anglosaské	anglosaský	k2eAgMnPc4d1	anglosaský
<g/>
)	)	kIx)	)
a	a	k8xC	a
budoucí	budoucí	k2eAgFnSc2d1	budoucí
paleontologie	paleontologie	k1gFnSc2	paleontologie
<g/>
.	.	kIx.	.
</s>
<s>
Pátral	pátral	k1gMnSc1	pátral
po	po	k7c6	po
přírodních	přírodní	k2eAgFnPc6d1	přírodní
kuriozitách	kuriozita	k1gFnPc6	kuriozita
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1676	[number]	k4	1676
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
poprvé	poprvé	k6eAd1	poprvé
vyobrazil	vyobrazit	k5eAaPmAgMnS	vyobrazit
obří	obří	k2eAgFnSc4d1	obří
kost	kost	k1gFnSc4	kost
dinosaura	dinosaurus	k1gMnSc2	dinosaurus
rodu	rod	k1gInSc2	rod
Megalosaurus	Megalosaurus	k1gMnSc1	Megalosaurus
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
ještě	ještě	k6eAd1	ještě
nepochopil	pochopit	k5eNaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
jakého	jaký	k3yQgMnSc4	jaký
tvora	tvor	k1gMnSc4	tvor
má	mít	k5eAaImIp3nS	mít
před	před	k7c7	před
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
výběr	výběr	k1gInSc4	výběr
The	The	k1gFnPc2	The
natural	natural	k?	natural
History	Histor	k1gInPc1	Histor
of	of	k?	of
Oxford-shire	Oxfordhir	k1gMnSc5	Oxford-shir
<g/>
.	.	kIx.	.
</s>
<s>
Being	Being	k1gMnSc1	Being
an	an	k?	an
essay	essay	k1gInPc1	essay
towards	towards	k1gInSc1	towards
the	the	k?	the
Natural	Natural	k?	Natural
History	Histor	k1gInPc1	Histor
of	of	k?	of
England	England	k1gInSc1	England
<g/>
,	,	kIx,	,
Minet	Minet	k1gInSc1	Minet
<g/>
,	,	kIx,	,
Chichelet	Chichelet	k1gInSc1	Chichelet
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<	<	kIx(	<
<g/>
Repr	Repra	k1gFnPc2	Repra
<g/>
.	.	kIx.	.
d.	d.	k?	d.
Ausg	Ausg	k1gInSc1	Ausg
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
<g/>
,	,	kIx,	,
1677	[number]	k4	1677
<g/>
>	>	kIx)	>
The	The	k1gFnSc1	The
natural	natural	k?	natural
history	histor	k1gInPc1	histor
of	of	k?	of
Stafford-shire	Staffordhir	k1gMnSc5	Stafford-shir
<g/>
,	,	kIx,	,
Morton	Morton	k1gInSc1	Morton
<g/>
,	,	kIx,	,
Manchester	Manchester	k1gInSc1	Manchester
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-901598-65-8	[number]	k4	0-901598-65-8
<	<	kIx(	<
<g/>
Repr	Repra	k1gFnPc2	Repra
<g/>
.	.	kIx.	.
d.	d.	k?	d.
Ausg	Ausg	k1gInSc1	Ausg
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
1686	[number]	k4	1686
<g/>
>	>	kIx)	>
De	De	k?	De
Origine	Origin	k1gInSc5	Origin
Fontium	Fontium	k1gNnSc1	Fontium
<g/>
,	,	kIx,	,
Tentamen	tentamen	k1gInSc1	tentamen
Philosophicum	Philosophicum	k1gInSc1	Philosophicum
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
praelectione	praelection	k1gInSc5	praelection
habita	habita	k1gMnSc1	habita
coram	coram	k1gInSc1	coram
Societate	Societat	k1gInSc5	Societat
Philosophica	Philosophicum	k1gNnSc2	Philosophicum
nuper	nuper	k1gInSc1	nuper
Oxonii	Oxonie	k1gFnSc4	Oxonie
instituta	institout	k5eAaPmNgFnS	institout
ad	ad	k7c4	ad
Scientiam	Scientiam	k1gInSc4	Scientiam
naturalem	naturalem	k?	naturalem
promovendam	promovendam	k1gInSc1	promovendam
<g/>
,	,	kIx,	,
Sheldon	Sheldon	k1gInSc1	Sheldon
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
<g/>
,	,	kIx,	,
1685	[number]	k4	1685
</s>
