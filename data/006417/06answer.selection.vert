<s>
Trhák	trhák	k1gInSc1	trhák
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
komediální	komediální	k2eAgInSc1d1	komediální
filmový	filmový	k2eAgInSc1d1	filmový
muzikál	muzikál	k1gInSc1	muzikál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Podskalský	podskalský	k2eAgMnSc1d1	podskalský
a	a	k8xC	a
kde	kde	k6eAd1	kde
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
tehdy	tehdy	k6eAd1	tehdy
populárních	populární	k2eAgInPc2d1	populární
československých	československý	k2eAgInPc2d1	československý
herců	herc	k1gInPc2	herc
i	i	k8xC	i
hvězd	hvězda	k1gFnPc2	hvězda
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
