<p>
<s>
Devadesátka	devadesátka	k1gFnSc1	devadesátka
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
kniha	kniha	k1gFnSc1	kniha
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
napsal	napsat	k5eAaPmAgMnS	napsat
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
<g/>
.	.	kIx.	.
</s>
<s>
Navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
Foglarovu	Foglarův	k2eAgFnSc4d1	Foglarova
předchozí	předchozí	k2eAgFnSc4d1	předchozí
knihu	kniha	k1gFnSc4	kniha
Pod	pod	k7c7	pod
junáckou	junácký	k2eAgFnSc7d1	Junácká
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
dalších	další	k2eAgNnPc6d1	další
dobrodružstvích	dobrodružství	k1gNnPc6	dobrodružství
skautského	skautský	k2eAgInSc2d1	skautský
oddílu	oddíl	k1gInSc2	oddíl
Devadesátky	devadesátka	k1gFnSc2	devadesátka
a	a	k8xC	a
chlapce	chlapec	k1gMnSc4	chlapec
Mirka	Mirek	k1gMnSc4	Mirek
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
členů	člen	k1gMnPc2	člen
oddílu	oddíl	k1gInSc2	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
knihy	kniha	k1gFnSc2	kniha
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
popis	popis	k1gInSc4	popis
dlouhodobé	dlouhodobý	k2eAgFnSc2d1	dlouhodobá
dobrodružné	dobrodružný	k2eAgFnSc2d1	dobrodružná
hry	hra	k1gFnSc2	hra
Alvarez	Alvareza	k1gFnPc2	Alvareza
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Foglar	Foglar	k1gMnSc1	Foglar
původně	původně	k6eAd1	původně
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
a	a	k8xC	a
odehrál	odehrát	k5eAaPmAgMnS	odehrát
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
oddílem	oddíl	k1gInSc7	oddíl
<g/>
,	,	kIx,	,
pražskou	pražský	k2eAgFnSc7d1	Pražská
Dvojkou	dvojka	k1gFnSc7	dvojka
v	v	k7c6	v
r.	r.	kA	r.
1942	[number]	k4	1942
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příběh	příběh	k1gInSc1	příběh
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
tábora	tábor	k1gInSc2	tábor
se	se	k3xPyFc4	se
oddíl	oddíl	k1gInSc1	oddíl
Devadesátka	devadesátka	k1gFnSc1	devadesátka
vrátí	vrátit	k5eAaPmIp3nS	vrátit
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
běžné	běžný	k2eAgFnSc3d1	běžná
činnosti	činnost	k1gFnSc3	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
chlapci	chlapec	k1gMnPc1	chlapec
z	z	k7c2	z
oddílu	oddíl	k1gInSc2	oddíl
však	však	k9	však
začnou	začít	k5eAaPmIp3nP	začít
své	svůj	k3xOyFgFnPc4	svůj
skautské	skautský	k2eAgFnPc4d1	skautská
zásady	zásada	k1gFnPc4	zásada
zanedbávat	zanedbávat	k5eAaImF	zanedbávat
a	a	k8xC	a
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
sporu	spor	k1gInSc6	spor
dokonce	dokonce	k9	dokonce
několik	několik	k4yIc1	několik
chlapců	chlapec	k1gMnPc2	chlapec
oddíl	oddíl	k1gInSc1	oddíl
opustí	opustit	k5eAaPmIp3nS	opustit
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
vůdce	vůdce	k1gMnSc2	vůdce
družiny	družina	k1gFnSc2	družina
Lišek	liška	k1gFnPc2	liška
<g/>
.	.	kIx.	.
</s>
<s>
Oddílový	oddílový	k2eAgMnSc1d1	oddílový
vedoucí	vedoucí	k1gMnSc1	vedoucí
Tapin	Tapin	k1gMnSc1	Tapin
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
zklamán	zklamán	k2eAgMnSc1d1	zklamán
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
vůdcem	vůdce	k1gMnSc7	vůdce
Lišek	liška	k1gFnPc2	liška
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
Mirek	Mirek	k1gMnSc1	Mirek
<g/>
,	,	kIx,	,
z	z	k7c2	z
nějž	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stal	stát	k5eAaPmAgMnS	stát
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvěrnějších	věrný	k2eAgMnPc2d3	nejvěrnější
členů	člen	k1gMnPc2	člen
oddílu	oddíl	k1gInSc2	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
však	však	k9	však
začne	začít	k5eAaPmIp3nS	začít
znovu	znovu	k6eAd1	znovu
vydírat	vydírat	k5eAaImF	vydírat
Ondra	Ondra	k1gMnSc1	Ondra
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
Mirek	Mirek	k1gMnSc1	Mirek
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
podepsal	podepsat	k5eAaPmAgMnS	podepsat
úpis	úpis	k1gInSc4	úpis
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
zavazuje	zavazovat	k5eAaImIp3nS	zavazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
skautském	skautský	k2eAgInSc6d1	skautský
oddílu	oddíl	k1gInSc6	oddíl
ukradne	ukradnout	k5eAaPmIp3nS	ukradnout
stan	stan	k1gInSc1	stan
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
Tapin	Tapin	k1gMnSc1	Tapin
už	už	k9	už
Mirkovi	Mirkův	k2eAgMnPc1d1	Mirkův
jeho	on	k3xPp3gInSc4	on
prohřešek	prohřešek	k1gInSc4	prohřešek
odpustil	odpustit	k5eAaPmAgMnS	odpustit
<g/>
,	,	kIx,	,
Ondra	Ondra	k1gMnSc1	Ondra
tentokrát	tentokrát	k6eAd1	tentokrát
vyhrožuje	vyhrožovat	k5eAaImIp3nS	vyhrožovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
úpis	úpis	k1gInSc1	úpis
ukáže	ukázat	k5eAaPmIp3nS	ukázat
jeho	jeho	k3xOp3gMnPc3	jeho
rodičům	rodič	k1gMnPc3	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Mirek	Mirek	k1gMnSc1	Mirek
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
získá	získat	k5eAaPmIp3nS	získat
zbylé	zbylý	k2eAgMnPc4d1	zbylý
dva	dva	k4xCgMnPc4	dva
chlapce	chlapec	k1gMnPc4	chlapec
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
bývalé	bývalý	k2eAgFnSc2d1	bývalá
party	parta	k1gFnSc2	parta
<g/>
,	,	kIx,	,
Josku	Josk	k1gInSc2	Josk
a	a	k8xC	a
Přemíka	Přemík	k1gMnSc2	Přemík
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
naváže	navázat	k5eAaPmIp3nS	navázat
přátelství	přátelství	k1gNnSc4	přátelství
a	a	k8xC	a
přesvědčí	přesvědčit	k5eAaPmIp3nS	přesvědčit
je	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Ondru	Ondra	k1gMnSc4	Ondra
opustili	opustit	k5eAaPmAgMnP	opustit
a	a	k8xC	a
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
rovněž	rovněž	k9	rovněž
do	do	k7c2	do
Devadesátky	devadesátka	k1gFnSc2	devadesátka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Josky	Joska	k1gFnSc2	Joska
získá	získat	k5eAaPmIp3nS	získat
Ondrovu	Ondrův	k2eAgFnSc4d1	Ondrova
tajnou	tajný	k2eAgFnSc4d1	tajná
obálku	obálka	k1gFnSc4	obálka
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
nalezne	nalézt	k5eAaBmIp3nS	nalézt
plánek	plánek	k1gInSc1	plánek
k	k	k7c3	k
místu	místo	k1gNnSc3	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
úpis	úpis	k1gInSc1	úpis
ukryt	ukryt	k2eAgInSc1d1	ukryt
<g/>
.	.	kIx.	.
</s>
<s>
Podaří	podařit	k5eAaPmIp3nS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
jej	on	k3xPp3gMnSc4	on
najít	najít	k5eAaPmF	najít
a	a	k8xC	a
spálit	spálit	k5eAaPmF	spálit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
od	od	k7c2	od
Ondry	Ondra	k1gMnSc2	Ondra
navždy	navždy	k6eAd1	navždy
pokoj	pokoj	k1gInSc4	pokoj
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
do	do	k7c2	do
Devadesátky	devadesátka	k1gFnSc2	devadesátka
vrátí	vrátit	k5eAaPmIp3nS	vrátit
jeho	jeho	k3xOp3gNnSc1	jeho
dříve	dříve	k6eAd2	dříve
nejbližší	blízký	k2eAgMnSc1d3	nejbližší
přítel	přítel	k1gMnSc1	přítel
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Přemík	Přemík	k1gMnSc1	Přemík
však	však	k9	však
po	po	k7c6	po
nějaké	nějaký	k3yIgFnSc6	nějaký
době	doba	k1gFnSc6	doba
oddíl	oddíl	k1gInSc1	oddíl
opustí	opustit	k5eAaPmIp3nP	opustit
a	a	k8xC	a
vrátí	vrátit	k5eAaPmIp3nP	vrátit
se	se	k3xPyFc4	se
k	k	k7c3	k
Ondrovi	Ondra	k1gMnSc3	Ondra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
oddílu	oddíl	k1gInSc2	oddíl
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
začne	začít	k5eAaPmIp3nS	začít
vkrádat	vkrádat	k5eAaImF	vkrádat
špatná	špatný	k2eAgFnSc1d1	špatná
nálada	nálada	k1gFnSc1	nálada
<g/>
,	,	kIx,	,
chlapci	chlapec	k1gMnPc1	chlapec
přestávají	přestávat	k5eAaImIp3nP	přestávat
mít	mít	k5eAaImF	mít
o	o	k7c4	o
oddíl	oddíl	k1gInSc4	oddíl
zájem	zájem	k1gInSc1	zájem
a	a	k8xC	a
vyvářejí	vyvářet	k5eAaImIp3nP	vyvářet
si	se	k3xPyFc3	se
uvnitř	uvnitř	k7c2	uvnitř
oddílu	oddíl	k1gInSc2	oddíl
uzavřené	uzavřený	k2eAgFnSc2d1	uzavřená
a	a	k8xC	a
skupinky	skupinka	k1gFnSc2	skupinka
a	a	k8xC	a
partičky	partička	k1gFnSc2	partička
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
Mirek	Mirek	k1gMnSc1	Mirek
i	i	k8xC	i
Tapin	Tapin	k1gMnSc1	Tapin
sledují	sledovat	k5eAaImIp3nP	sledovat
s	s	k7c7	s
nelibostí	nelibost	k1gFnSc7	nelibost
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
Jiří	Jiří	k1gMnSc1	Jiří
se	se	k3xPyFc4	se
blízce	blízce	k6eAd1	blízce
spřátelí	spřátelit	k5eAaPmIp3nS	spřátelit
s	s	k7c7	s
Joskou	Joska	k1gFnSc7	Joska
a	a	k8xC	a
Mirek	Mirek	k1gMnSc1	Mirek
postrádá	postrádat	k5eAaImIp3nS	postrádat
v	v	k7c6	v
oddílu	oddíl	k1gInSc6	oddíl
opravdového	opravdový	k2eAgMnSc2d1	opravdový
přítele	přítel	k1gMnSc2	přítel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aby	aby	kYmCp3nS	aby
Tapin	Tapin	k1gInSc1	Tapin
chlapce	chlapec	k1gMnSc4	chlapec
znovu	znovu	k6eAd1	znovu
získal	získat	k5eAaPmAgMnS	získat
pro	pro	k7c4	pro
oddílovou	oddílový	k2eAgFnSc4d1	oddílová
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
vymyslí	vymyslet	k5eAaPmIp3nS	vymyslet
si	se	k3xPyFc3	se
napínavou	napínavý	k2eAgFnSc4d1	napínavá
dlouhodobou	dlouhodobý	k2eAgFnSc4d1	dlouhodobá
hru	hra	k1gFnSc4	hra
Alvarez	Alvareza	k1gFnPc2	Alvareza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nesouhry	nesouhra	k1gFnPc1	nesouhra
v	v	k7c6	v
oddílu	oddíl	k1gInSc6	oddíl
se	se	k3xPyFc4	se
přenesou	přenést	k5eAaPmIp3nP	přenést
i	i	k9	i
na	na	k7c4	na
letní	letní	k2eAgInSc4d1	letní
tábor	tábor	k1gInSc4	tábor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
chlapci	chlapec	k1gMnPc1	chlapec
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
v	v	k7c6	v
Alvarezovi	Alvarez	k1gMnSc6	Alvarez
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
činnost	činnost	k1gFnSc1	činnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	on	k3xPp3gFnPc4	on
opravdu	opravdu	k6eAd1	opravdu
baví	bavit	k5eAaImIp3nP	bavit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jedné	jeden	k4xCgFnSc6	jeden
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mají	mít	k5eAaImIp3nP	mít
nalézt	nalézt	k5eAaBmF	nalézt
plechové	plechový	k2eAgNnSc4d1	plechové
pouzdro	pouzdro	k1gNnSc4	pouzdro
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
prudkého	prudký	k2eAgInSc2d1	prudký
potůčku	potůček	k1gInSc2	potůček
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Jiřímu	Jiří	k1gMnSc3	Jiří
s	s	k7c7	s
Joskou	Joska	k1gFnSc7	Joska
podaří	podařit	k5eAaPmIp3nS	podařit
splnit	splnit	k5eAaPmF	splnit
úkol	úkol	k1gInSc4	úkol
téměř	téměř	k6eAd1	téměř
okamžitě	okamžitě	k6eAd1	okamžitě
<g/>
,	,	kIx,	,
a	a	k8xC	a
Mirkova	Mirkův	k2eAgFnSc1d1	Mirkova
družina	družina	k1gFnSc1	družina
Lišek	liška	k1gFnPc2	liška
tak	tak	k9	tak
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
další	další	k2eAgFnSc6d1	další
noční	noční	k2eAgFnSc6d1	noční
soutěži	soutěž	k1gFnSc6	soutěž
však	však	k9	však
Mirek	Mirek	k1gMnSc1	Mirek
odhalí	odhalit	k5eAaPmIp3nS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
Joskou	Joská	k1gFnSc4	Joská
na	na	k7c4	na
tábor	tábor	k1gInSc4	tábor
přijel	přijet	k5eAaPmAgMnS	přijet
Ondra	Ondra	k1gMnSc1	Ondra
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
mu	on	k3xPp3gMnSc3	on
úkryt	úkryt	k1gInSc4	úkryt
pouzdra	pouzdro	k1gNnSc2	pouzdro
prozradil	prozradit	k5eAaPmAgMnS	prozradit
<g/>
.	.	kIx.	.
</s>
<s>
Mirek	Mirek	k1gMnSc1	Mirek
je	být	k5eAaImIp3nS	být
hořce	hořko	k6eAd1	hořko
zklamán	zklamat	k5eAaPmNgMnS	zklamat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Jiří	Jiří	k1gMnSc1	Jiří
a	a	k8xC	a
Joska	Joska	k1gMnSc1	Joska
spřátelili	spřátelit	k5eAaPmAgMnP	spřátelit
s	s	k7c7	s
Ondrou	Ondra	k1gMnSc7	Ondra
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
mu	on	k3xPp3gMnSc3	on
učinil	učinit	k5eAaPmAgMnS	učinit
tolik	tolik	k6eAd1	tolik
zlého	zlý	k1gMnSc4	zlý
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
uchýlili	uchýlit	k5eAaPmAgMnP	uchýlit
k	k	k7c3	k
podvodu	podvod	k1gInSc3	podvod
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
záměrně	záměrně	k6eAd1	záměrně
noční	noční	k2eAgFnSc4d1	noční
soutěž	soutěž	k1gFnSc4	soutěž
prohraje	prohrát	k5eAaPmIp3nS	prohrát
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
s	s	k7c7	s
Joskou	Joska	k1gMnSc7	Joska
si	se	k3xPyFc3	se
uvědomí	uvědomit	k5eAaPmIp3nP	uvědomit
svou	svůj	k3xOyFgFnSc4	svůj
špatnost	špatnost	k1gFnSc4	špatnost
a	a	k8xC	a
žádají	žádat	k5eAaImIp3nP	žádat
Mirka	Mirek	k1gMnSc4	Mirek
o	o	k7c4	o
odpuštění	odpuštění	k1gNnSc4	odpuštění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přichází	přicházet	k5eAaImIp3nS	přicházet
poslední	poslední	k2eAgInSc4d1	poslední
den	den	k1gInSc4	den
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
zásluhou	zásluhou	k7c2	zásluhou
Josky	Joska	k1gMnSc2	Joska
a	a	k8xC	a
Jiřího	Jiří	k1gMnSc2	Jiří
družina	družina	k1gFnSc1	družina
Lišek	liška	k1gFnPc2	liška
zvítězí	zvítězit	k5eAaPmIp3nS	zvítězit
<g/>
.	.	kIx.	.
</s>
<s>
Mirek	Mirek	k1gMnSc1	Mirek
chlapcům	chlapec	k1gMnPc3	chlapec
odpouští	odpouštět	k5eAaImIp3nP	odpouštět
a	a	k8xC	a
skautský	skautský	k2eAgInSc1d1	skautský
oddíl	oddíl	k1gInSc1	oddíl
se	se	k3xPyFc4	se
po	po	k7c6	po
dobrodružné	dobrodružný	k2eAgFnSc6d1	dobrodružná
hře	hra	k1gFnSc6	hra
vrací	vracet	k5eAaImIp3nS	vracet
domů	domů	k6eAd1	domů
usmířený	usmířený	k2eAgInSc1d1	usmířený
a	a	k8xC	a
jednotný	jednotný	k2eAgInSc1d1	jednotný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vydání	vydání	k1gNnSc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
poprvé	poprvé	k6eAd1	poprvé
vycházela	vycházet	k5eAaImAgFnS	vycházet
jako	jako	k9	jako
příběh	příběh	k1gInSc4	příběh
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Junák	junák	k1gMnSc1	junák
v	v	k7c6	v
l.	l.	k?	l.
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
její	její	k3xOp3gNnSc1	její
vydávání	vydávání	k1gNnSc1	vydávání
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
románu	román	k1gInSc2	román
byl	být	k5eAaImAgInS	být
dovyprávěn	dovyprávět	k5eAaPmNgInS	dovyprávět
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Vpřed	vpřed	k6eAd1	vpřed
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knižní	knižní	k2eAgFnSc6d1	knižní
podobě	podoba	k1gFnSc6	podoba
vyšla	vyjít	k5eAaPmAgFnS	vyjít
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
r.	r.	kA	r.
1969	[number]	k4	1969
a	a	k8xC	a
poté	poté	k6eAd1	poté
až	až	k9	až
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
l.	l.	k?	l.
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
a	a	k8xC	a
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
FOGLAR	FOGLAR	kA	FOGLAR
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Devadesátka	devadesátka	k1gFnSc1	devadesátka
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FOGLAR	FOGLAR	kA	FOGLAR
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Devadesátka	devadesátka	k1gFnSc1	devadesátka
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7033	[number]	k4	7033
<g/>
-	-	kIx~	-
<g/>
144	[number]	k4	144
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FOGLAR	FOGLAR	kA	FOGLAR
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Devadesátka	devadesátka	k1gFnSc1	devadesátka
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7033	[number]	k4	7033
<g/>
-	-	kIx~	-
<g/>
378	[number]	k4	378
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FOGLAR	FOGLAR	kA	FOGLAR
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Devadesátka	devadesátka	k1gFnSc1	devadesátka
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7033	[number]	k4	7033
<g/>
-	-	kIx~	-
<g/>
501	[number]	k4	501
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FOGLAR	FOGLAR	kA	FOGLAR
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Devadesátka	devadesátka	k1gFnSc1	devadesátka
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7033	[number]	k4	7033
<g/>
-	-	kIx~	-
<g/>
888	[number]	k4	888
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
