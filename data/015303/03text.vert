<s>
Pásmo	pásmo	k1gNnSc1
Gazy	Gaza	k1gFnSc2
</s>
<s>
Pásmo	pásmo	k1gNnSc1
Gazy	Gaza	k1gFnSc2
ق	ق	k?
غ	غ	k?
<g/>
(	(	kIx(
<g/>
Qiṭ	Qiṭ	k1gMnSc2
Ġ	Ġ	k1gMnSc2
<g/>
)	)	kIx)
Satelitní	satelitní	k2eAgInSc1d1
snímek	snímek	k1gInSc1
Pásma	pásmo	k1gNnSc2
Gaza	Gaz	k1gInSc2
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Gaza	Gaz	k2eAgFnSc1d1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
31	#num#	k4
<g/>
°	°	k?
<g/>
25	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
34	#num#	k4
<g/>
°	°	k?
<g/>
20	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
360	#num#	k4
km²	km²	k?
Nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
</s>
<s>
Abu	Abu	k?
Auda	Auda	k1gFnSc1
(	(	kIx(
<g/>
105	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
GMT	GMT	kA
<g/>
+	+	kIx~
<g/>
2	#num#	k4
Geodata	Geodata	k1gFnSc1
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
Obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
1	#num#	k4
816	#num#	k4
379	#num#	k4
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
5	#num#	k4
0	#num#	k4
<g/>
45,5	45,5	k4
obyv	obyva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Jazyk	jazyk	k1gInSc1
</s>
<s>
arabština	arabština	k1gFnSc1
Národnostní	národnostní	k2eAgNnSc1d1
složení	složení	k1gNnSc4
</s>
<s>
Arabové	Arab	k1gMnPc1
Náboženství	náboženství	k1gNnSc2
</s>
<s>
sunnitský	sunnitský	k2eAgInSc1d1
islám	islám	k1gInSc1
<g/>
,	,	kIx,
křesťanství	křesťanství	k1gNnSc4
Správa	správa	k1gFnSc1
regionu	region	k1gInSc2
Nadřazený	nadřazený	k2eAgInSc1d1
celek	celek	k1gInSc1
</s>
<s>
Stát	stát	k1gInSc1
Palestina	Palestina	k1gFnSc1
Prezident	prezident	k1gMnSc1
</s>
<s>
Mahmúd	Mahmúd	k1gInSc1
Abbás	Abbás	k1gInSc1
(	(	kIx(
<g/>
de	de	k?
iure	iure	k1gInSc1
<g/>
)	)	kIx)
Předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
Ismajl	Ismajl	k1gInSc1
Haníja	Haníj	k2eAgFnSc1d1
Měna	měna	k1gFnSc1
</s>
<s>
Egyptská	egyptský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
(	(	kIx(
<g/>
de	de	k?
facto	facto	k1gNnSc1
<g/>
)	)	kIx)
<g/>
Nový	nový	k2eAgInSc1d1
izraelský	izraelský	k2eAgInSc1d1
šekel	šekel	k1gInSc1
(	(	kIx(
<g/>
₪	₪	k?
<g/>
)	)	kIx)
Mezinárodní	mezinárodní	k2eAgFnSc1d1
identifikace	identifikace	k1gFnSc1
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
+970	+970	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Podrobná	podrobný	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
pásma	pásmo	k1gNnSc2
Gaza	Gaz	k1gInSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pásmo	pásmo	k1gNnSc1
Gaza	Gaz	k1gInSc2
(	(	kIx(
<g/>
též	též	k9
Pásmo	pásmo	k1gNnSc1
Gazy	Gaza	k1gFnSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
arabsky	arabsky	k6eAd1
<g/>
:	:	kIx,
ق	ق	k?
غ	غ	k?
<g/>
,	,	kIx,
Qittā	Qittā	k1gInSc1
Ghazah	Ghazaha	k1gFnPc2
<g/>
,	,	kIx,
hebrejsky	hebrejsky	k6eAd1
<g/>
:	:	kIx,
ר	ר	k?
ע	ע	k?
<g/>
,	,	kIx,
Recu	Recus	k1gInSc2
<g/>
'	'	kIx"
<g/>
at	at	k?
Aza	Aza	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
území	území	k1gNnSc4
při	při	k7c6
pobřeží	pobřeží	k1gNnSc6
Středozemního	středozemní	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
sousedí	sousedit	k5eAaImIp3nS
s	s	k7c7
Egyptem	Egypt	k1gInSc7
(	(	kIx(
<g/>
zde	zde	k6eAd1
Pásmo	pásmo	k1gNnSc1
Gaza	Gaz	k1gInSc2
končí	končit	k5eAaImIp3nS
na	na	k7c4
někdejší	někdejší	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
mezi	mezi	k7c7
Egyptem	Egypt	k1gInSc7
a	a	k8xC
Britským	britský	k2eAgInSc7d1
mandátem	mandát	k1gInSc7
Palestina	Palestina	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
na	na	k7c6
jihu	jih	k1gInSc6
<g/>
,	,	kIx,
východě	východ	k1gInSc6
a	a	k8xC
severu	sever	k1gInSc6
s	s	k7c7
Izraelem	Izrael	k1gInSc7
(	(	kIx(
<g/>
zde	zde	k6eAd1
Pásmo	pásmo	k1gNnSc1
Gaza	Gaz	k1gInSc2
končí	končit	k5eAaImIp3nS
na	na	k7c4
linii	linie	k1gFnSc4
příměří	příměří	k1gNnSc2
podle	podle	k7c2
dohody	dohoda	k1gFnSc2
o	o	k7c6
příměří	příměří	k1gNnSc6
mezi	mezi	k7c7
Egyptem	Egypt	k1gInSc7
a	a	k8xC
Izraelem	Izrael	k1gInSc7
<g/>
,	,	kIx,
uzavřené	uzavřený	k2eAgInPc4d1
24	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1949	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
41	#num#	k4
kilometrů	kilometr	k1gInPc2
dlouhé	dlouhý	k2eAgFnSc2d1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
šířka	šířka	k1gFnSc1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
od	od	k7c2
6	#num#	k4
do	do	k7c2
12	#num#	k4
kilometrů	kilometr	k1gInPc2
a	a	k8xC
celkově	celkově	k6eAd1
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
na	na	k7c4
360	#num#	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodním	mezinárodní	k2eAgNnSc7d1
společenstvím	společenství	k1gNnSc7
je	být	k5eAaImIp3nS
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
součást	součást	k1gFnSc4
státu	stát	k1gInSc2
Palestina	Palestina	k1gFnSc1
<g/>
,	,	kIx,
kam	kam	k6eAd1
kromě	kromě	k7c2
Pásma	pásmo	k1gNnSc2
Gaza	Gaz	k1gInSc2
patří	patřit	k5eAaImIp3nS
i	i	k9
(	(	kIx(
<g/>
územně	územně	k6eAd1
s	s	k7c7
ním	on	k3xPp3gInSc7
nesouvisející	související	k2eNgFnPc4d1
<g/>
)	)	kIx)
Západní	západní	k2eAgInSc4d1
břeh	břeh	k1gInSc4
Jordánu	Jordán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
je	být	k5eAaImIp3nS
v	v	k7c6
moci	moc	k1gFnSc6
militantní	militantní	k2eAgNnSc1d1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
islamistické	islamistický	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
Hamás	Hamása	k1gFnPc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
například	například	k6eAd1
Evropská	evropský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
a	a	k8xC
USA	USA	kA
řadí	řadit	k5eAaImIp3nS
mezi	mezi	k7c4
teroristické	teroristický	k2eAgFnPc4d1
skupiny	skupina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ta	ten	k3xDgFnSc1
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
vyhrála	vyhrát	k5eAaPmAgFnS
parlamentní	parlamentní	k2eAgFnPc4d1
volby	volba	k1gFnPc4
a	a	k8xC
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
de	de	k?
facto	facto	k1gNnSc4
prostřednictvím	prostřednictvím	k7c2
svých	svůj	k3xOyFgFnPc2
ozbrojených	ozbrojený	k2eAgFnPc2d1
milicí	milice	k1gFnPc2
převzala	převzít	k5eAaPmAgFnS
moc	moc	k6eAd1
v	v	k7c6
Pásmu	pásmo	k1gNnSc6
Gaza	Gaz	k1gInSc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
vyhnala	vyhnat	k5eAaPmAgFnS
úředníky	úředník	k1gMnPc4
a	a	k8xC
bezpečnostní	bezpečnostní	k2eAgFnPc4d1
složky	složka	k1gFnPc4
Palestinské	palestinský	k2eAgFnSc2d1
autonomie	autonomie	k1gFnSc2
a	a	k8xC
mnohé	mnohý	k2eAgMnPc4d1
civilní	civilní	k2eAgMnPc4d1
pracovníky	pracovník	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Proti	proti	k7c3
Pásmu	pásmo	k1gNnSc3
Gaza	Gaz	k1gInSc2
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
ze	z	k7c2
strany	strana	k1gFnSc2
Izraele	Izrael	k1gInSc2
uplatňována	uplatňovat	k5eAaImNgFnS
pozemní	pozemní	k2eAgFnSc1d1
<g/>
,	,	kIx,
námořní	námořní	k2eAgFnSc1d1
a	a	k8xC
vzdušná	vzdušný	k2eAgFnSc1d1
blokáda	blokáda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jižní	jižní	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
Gazy	Gaza	k1gFnSc2
uzavírá	uzavírat	k5eAaPmIp3nS,k5eAaImIp3nS
Egypt	Egypt	k1gInSc1
<g/>
,	,	kIx,
spojenec	spojenec	k1gMnSc1
Izraele	Izrael	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Jako	jako	k8xC,k8xS
důvod	důvod	k1gInSc1
k	k	k7c3
blokádě	blokáda	k1gFnSc3
Pásma	pásmo	k1gNnSc2
Gaza	Gaz	k1gInSc2
jsou	být	k5eAaImIp3nP
uváděny	uváděn	k2eAgFnPc1d1
„	„	k?
<g/>
obavy	obava	k1gFnPc1
o	o	k7c4
bezpečnost	bezpečnost	k1gFnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Blokáda	blokáda	k1gFnSc1
je	být	k5eAaImIp3nS
z	z	k7c2
humanitárních	humanitární	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
kritizována	kritizovat	k5eAaImNgFnS
ze	z	k7c2
strany	strana	k1gFnSc2
OSN	OSN	kA
a	a	k8xC
různých	různý	k2eAgFnPc2d1
lidskoprávních	lidskoprávní	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Název	název	k1gInSc1
území	území	k1gNnSc2
je	být	k5eAaImIp3nS
odvozen	odvozen	k2eAgMnSc1d1
od	od	k7c2
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Gazy	Gaza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
přibližně	přibližně	k6eAd1
1,8	1,8	k4
milionu	milion	k4xCgInSc2
Palestinců	Palestinec	k1gMnPc2
(	(	kIx(
<g/>
Gazanů	Gazan	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Většinou	většina	k1gFnSc7
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
uprchlíky	uprchlík	k1gMnPc4
či	či	k8xC
potomky	potomek	k1gMnPc4
uprchlíků	uprchlík	k1gMnPc2
palestinského	palestinský	k2eAgInSc2d1
exodu	exodus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přibližně	přibližně	k6eAd1
2000	#num#	k4
<g/>
–	–	k?
<g/>
3000	#num#	k4
(	(	kIx(
<g/>
0,7	0,7	k4
%	%	kIx~
<g/>
)	)	kIx)
obyvatel	obyvatel	k1gMnPc2
jsou	být	k5eAaImIp3nP
křesťané	křesťan	k1gMnPc1
a	a	k8xC
zbytek	zbytek	k1gInSc4
jsou	být	k5eAaImIp3nP
sunnitští	sunnitský	k2eAgMnPc1d1
muslimové	muslim	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Oblast	oblast	k1gFnSc1
dnešního	dnešní	k2eAgNnSc2d1
Pásma	pásmo	k1gNnSc2
Gaza	Gazum	k1gNnSc2
byla	být	k5eAaImAgNnP
v	v	k7c6
roce	rok	k1gInSc6
1517	#num#	k4
dobyta	dobyt	k2eAgFnSc1d1
Osmanskou	osmanský	k2eAgFnSc7d1
říší	říš	k1gFnSc7
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc7
součástí	součást	k1gFnSc7
následně	následně	k6eAd1
bylo	být	k5eAaImAgNnS
až	až	k9
do	do	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počátku	počátek	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
dominoval	dominovat	k5eAaImAgMnS
regionu	region	k1gInSc6
kulturně	kulturně	k6eAd1
Egypt	Egypt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ač	ač	k8xS
bylo	být	k5eAaImAgNnS
pásmo	pásmo	k1gNnSc4
stále	stále	k6eAd1
pod	pod	k7c7
nadvládou	nadvláda	k1gFnSc7
Osmanské	osmanský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
mnoho	mnoho	k4c1
obyvatel	obyvatel	k1gMnPc2
byli	být	k5eAaImAgMnP
právě	právě	k9
Egypťané	Egypťan	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
emigrovali	emigrovat	k5eAaBmAgMnP
kvůli	kvůli	k7c3
politickým	politický	k2eAgInPc3d1
nepokojům	nepokoj	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
(	(	kIx(
<g/>
1914	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
sloužilo	sloužit	k5eAaImAgNnS
pásmo	pásmo	k1gNnSc4
jako	jako	k8xC,k8xS
bojiště	bojiště	k1gNnSc4
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
se	se	k3xPyFc4
utkali	utkat	k5eAaPmAgMnP
Britové	Brit	k1gMnPc1
proti	proti	k7c3
Turkům	turek	k1gInPc3
podporovaným	podporovaný	k2eAgInPc3d1
Němci	Němec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
Třetí	třetí	k4xOgFnSc2
bitvy	bitva	k1gFnSc2
o	o	k7c4
Gazu	Gaza	k1gFnSc4
bylo	být	k5eAaImAgNnS
7	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1917	#num#	k4
pásmo	pásmo	k1gNnSc4
Brity	Brit	k1gMnPc4
obsazeno	obsazen	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
válce	válka	k1gFnSc6
se	se	k3xPyFc4
Gaza	Gaza	k1gFnSc1
stala	stát	k5eAaPmAgFnS
součástí	součást	k1gFnSc7
Britského	britský	k2eAgInSc2d1
mandátu	mandát	k1gInSc2
Palestina	Palestina	k1gFnSc1
pod	pod	k7c7
kontrolou	kontrola	k1gFnSc7
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Židé	Žid	k1gMnPc1
osidlovali	osidlovat	k5eAaImAgMnP
pásmo	pásmo	k1gNnSc4
v	v	k7c6
okolí	okolí	k1gNnSc6
Gazy	Gaza	k1gFnSc2
až	až	k9
do	do	k7c2
nepokojů	nepokoj	k1gInPc2
v	v	k7c6
Palestině	Palestina	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byli	být	k5eAaImAgMnP
donuceni	donucen	k2eAgMnPc1d1
toto	tento	k3xDgNnSc4
pásmo	pásmo	k1gNnSc4
opustit	opustit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
vydali	vydat	k5eAaPmAgMnP
Britové	Brit	k1gMnPc1
zákaz	zákaz	k1gInSc4
<g/>
,	,	kIx,
podle	podle	k7c2
něhož	jenž	k3xRgNnSc2
nesměli	smět	k5eNaImAgMnP
Židé	Žid	k1gMnPc1
v	v	k7c6
tomto	tento	k3xDgInSc6
pásmu	pásmo	k1gNnSc6
žít	žít	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přesto	přesto	k8xC
se	se	k3xPyFc4
někteří	některý	k3yIgMnPc1
Židé	Žid	k1gMnPc1
vrátili	vrátit	k5eAaPmAgMnP
a	a	k8xC
založili	založit	k5eAaPmAgMnP
kibuc	kibuc	k1gInSc4
Kfar	Kfara	k1gFnPc2
Darom	Darom	k1gInSc4
poblíž	poblíž	k7c2
hranice	hranice	k1gFnSc2
s	s	k7c7
Egyptem	Egypt	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
vytvořila	vytvořit	k5eAaPmAgFnS
komise	komise	k1gFnSc1
OSN	OSN	kA
plán	plán	k1gInSc4
na	na	k7c6
rozdělení	rozdělení	k1gNnSc6
mandátní	mandátní	k2eAgFnSc2d1
Palestiny	Palestina	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
29	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1947	#num#	k4
schválilo	schválit	k5eAaPmAgNnS
Valné	valný	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
OSN	OSN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
tohoto	tento	k3xDgInSc2
plánu	plán	k1gInSc2
měl	mít	k5eAaImAgInS
vzniknout	vzniknout	k5eAaPmF
arabský	arabský	k2eAgInSc1d1
a	a	k8xC
židovský	židovský	k2eAgInSc1d1
stát	stát	k1gInSc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
oblast	oblast	k1gFnSc1
dnešního	dnešní	k2eAgNnSc2d1
Pásma	pásmo	k1gNnSc2
Gaza	Gazum	k1gNnSc2
měla	mít	k5eAaImAgFnS
připadnout	připadnout	k5eAaPmF
budoucímu	budoucí	k2eAgInSc3d1
arabskému	arabský	k2eAgInSc3d1
státu	stát	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arabské	arabský	k2eAgInPc1d1
státy	stát	k1gInPc1
však	však	k8xC
plán	plán	k1gInSc1
odmítly	odmítnout	k5eAaPmAgFnP
a	a	k8xC
namísto	namísto	k7c2
toho	ten	k3xDgNnSc2
po	po	k7c6
vyhlášení	vyhlášení	k1gNnSc6
izraelské	izraelský	k2eAgFnSc2d1
nezávislosti	nezávislost	k1gFnSc2
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1948	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
první	první	k4xOgFnSc3
arabsko-izraelské	arabsko-izraelský	k2eAgFnSc3d1
válce	válka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izrael	Izrael	k1gMnSc1
válku	válka	k1gFnSc4
vyhrál	vyhrát	k5eAaPmAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
Pásmo	pásmo	k1gNnSc1
Gaza	Gaz	k1gInSc2
obsazené	obsazený	k2eAgNnSc1d1
egyptskou	egyptský	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
získal	získat	k5eAaPmAgInS
po	po	k7c6
dohodách	dohoda	k1gFnPc6
o	o	k7c6
příměří	příměří	k1gNnSc6
z	z	k7c2
roku	rok	k1gInSc2
1949	#num#	k4
Egypt	Egypt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
tomuto	tento	k3xDgNnSc3
území	území	k1gNnSc3
poté	poté	k6eAd1
vládl	vládnout	k5eAaImAgInS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1967	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jej	on	k3xPp3gInSc4
v	v	k7c6
šestidenní	šestidenní	k2eAgFnSc6d1
válce	válka	k1gFnSc6
dobyl	dobýt	k5eAaPmAgInS
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Pásmu	pásmo	k1gNnSc6
Gaza	Gaz	k1gInSc2
následně	následně	k6eAd1
vzniklo	vzniknout	k5eAaPmAgNnS
několik	několik	k4yIc1
izraelských	izraelský	k2eAgFnPc2d1
osad	osada	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátkem	počátkem	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
získala	získat	k5eAaPmAgFnS
v	v	k7c6
rámci	rámec	k1gInSc6
Oselských	oselský	k2eAgFnPc2d1
dohod	dohoda	k1gFnPc2
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
městy	město	k1gNnPc7
v	v	k7c6
pásmu	pásmo	k1gNnSc6
nově	nově	k6eAd1
vzniklá	vzniklý	k2eAgFnSc1d1
Palestinská	palestinský	k2eAgFnSc1d1
samospráva	samospráva	k1gFnSc1
<g/>
,	,	kIx,
Izrael	Izrael	k1gInSc1
nicméně	nicméně	k8xC
udržoval	udržovat	k5eAaImAgInS
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
místním	místní	k2eAgInSc7d1
leteckým	letecký	k2eAgInSc7d1
prostorem	prostor	k1gInSc7
<g/>
,	,	kIx,
hranicemi	hranice	k1gFnPc7
a	a	k8xC
teritoriálními	teritoriální	k2eAgFnPc7d1
vodami	voda	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
se	se	k3xPyFc4
Izrael	Izrael	k1gInSc1
za	za	k7c2
vlády	vláda	k1gFnSc2
Ariela	Ariel	k1gMnSc2
Šarona	Šaron	k1gMnSc2
z	z	k7c2
celého	celý	k2eAgNnSc2d1
Pásma	pásmo	k1gNnSc2
Gazy	Gaza	k1gMnSc2
jednostranně	jednostranně	k6eAd1
stáhl	stáhnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
evakuoval	evakuovat	k5eAaBmAgMnS
všechny	všechen	k3xTgMnPc4
své	svůj	k3xOyFgMnPc4
občany	občan	k1gMnPc4
(	(	kIx(
<g/>
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
necelých	celý	k2eNgNnPc2d1
devět	devět	k4xCc1
tisíc	tisíc	k4xCgInSc4
Izraelců	Izraelec	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
se	se	k3xPyFc4
v	v	k7c6
Pásmu	pásmo	k1gNnSc6
Gaza	Gazus	k1gMnSc2
konaly	konat	k5eAaImAgFnP
volby	volba	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
vyhrálo	vyhrát	k5eAaPmAgNnS
hnutí	hnutí	k1gNnSc1
Hamás	Hamás	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
nejprve	nejprve	k6eAd1
pokusilo	pokusit	k5eAaPmAgNnS
utvořit	utvořit	k5eAaPmF
vládu	vláda	k1gFnSc4
národní	národní	k2eAgFnSc2d1
jednoty	jednota	k1gFnSc2
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Ismajla	Ismajlo	k1gNnSc2
Haníji	Haníje	k1gFnSc4
<g/>
,	,	kIx,
zahrnující	zahrnující	k2eAgNnSc4d1
i	i	k8xC
poražené	poražený	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
Fatah	Fataha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
však	však	k9
kompletně	kompletně	k6eAd1
převzalo	převzít	k5eAaPmAgNnS
moc	moc	k6eAd1
po	po	k7c6
násilných	násilný	k2eAgInPc6d1
střetech	střet	k1gInPc6
s	s	k7c7
Fatahem	Fatah	k1gInSc7
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
z	z	k7c2
Pásma	pásmo	k1gNnSc2
Gaza	Gazum	k1gNnSc2
vyhnalo	vyhnat	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izrael	Izrael	k1gInSc4
a	a	k8xC
Egypt	Egypt	k1gInSc4
následně	následně	k6eAd1
uzavřely	uzavřít	k5eAaPmAgInP
hraniční	hraniční	k2eAgInPc1d1
přechody	přechod	k1gInPc1
s	s	k7c7
Pásmem	pásmo	k1gNnSc7
Gaza	Gaz	k1gInSc2
a	a	k8xC
Izrael	Izrael	k1gInSc1
zavedl	zavést	k5eAaPmAgInS
námořní	námořní	k2eAgFnSc4d1
blokádu	blokáda	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zamezil	zamezit	k5eAaPmAgMnS
přísun	přísun	k1gInSc4
zbraní	zbraň	k1gFnPc2
do	do	k7c2
Pásma	pásmo	k1gNnSc2
Gaza	Gaz	k1gInSc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2001	#num#	k4
ostřelován	ostřelovat	k5eAaImNgMnS
raketami	raketa	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řada	řada	k1gFnSc1
arabských	arabský	k2eAgInPc2d1
států	stát	k1gInPc2
odmítla	odmítnout	k5eAaPmAgFnS
převzetí	převzetí	k1gNnSc4
moci	moct	k5eAaImF
Hamásem	Hamás	k1gInSc7
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
uznaly	uznat	k5eAaPmAgFnP
Egypt	Egypt	k1gInSc4
<g/>
,	,	kIx,
Jordánsko	Jordánsko	k1gNnSc1
a	a	k8xC
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
jako	jako	k8xC,k8xS
jedinou	jediný	k2eAgFnSc4d1
legitimní	legitimní	k2eAgFnSc4d1
palestinskou	palestinský	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
tu	tu	k6eAd1
na	na	k7c6
Západním	západní	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Jordánu	Jordán	k1gInSc2
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Mahmúda	Mahmúd	k1gMnSc2
Abbáse	Abbás	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ozbrojené	ozbrojený	k2eAgFnPc1d1
akce	akce	k1gFnPc1
po	po	k7c6
jednostranném	jednostranný	k2eAgNnSc6d1
stažení	stažení	k1gNnSc6
Izraele	Izrael	k1gInSc2
z	z	k7c2
Pásma	pásmo	k1gNnSc2
Gaza	Gaz	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
</s>
<s>
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
palestinské	palestinský	k2eAgInPc4d1
ozbrojené	ozbrojený	k2eAgInPc4d1
útoky	útok	k1gInPc4
(	(	kIx(
<g/>
ostřelování	ostřelování	k1gNnSc2
raketami	raketa	k1gFnPc7
a	a	k8xC
minomety	minomet	k1gInPc7
<g/>
,	,	kIx,
teroristické	teroristický	k2eAgInPc4d1
útoky	útok	k1gInPc4
a	a	k8xC
únosy	únos	k1gInPc4
<g/>
)	)	kIx)
a	a	k8xC
pašování	pašování	k1gNnSc1
zbraní	zbraň	k1gFnPc2
do	do	k7c2
Pásma	pásmo	k1gNnSc2
Gaza	Gazum	k1gNnSc2
podnikl	podniknout	k5eAaPmAgMnS
Izrael	Izrael	k1gInSc4
v	v	k7c6
minulosti	minulost	k1gFnSc6
řadu	řad	k1gInSc2
vojenských	vojenský	k2eAgFnPc2d1
operací	operace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
Izrael	Izrael	k1gInSc1
z	z	k7c2
Pásma	pásmo	k1gNnSc2
Gaza	Gaz	k1gInSc2
jednostranně	jednostranně	k6eAd1
stáhl	stáhnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
na	na	k7c4
něj	on	k3xPp3gMnSc4
bylo	být	k5eAaImAgNnS
palestinskými	palestinský	k2eAgMnPc7d1
extremisty	extremista	k1gMnPc7
vypáleno	vypálen	k2eAgNnSc1d1
přes	přes	k7c4
jedenáct	jedenáct	k4xCc4
tisíc	tisíc	k4xCgInSc4
střel	střela	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
S	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
tuto	tento	k3xDgFnSc4
hrozbu	hrozba	k1gFnSc4
Izrael	Izrael	k1gInSc1
vyvinul	vyvinout	k5eAaPmAgInS
a	a	k8xC
začal	začít	k5eAaPmAgInS
využívat	využívat	k5eAaImF,k5eAaPmF
protiraketový	protiraketový	k2eAgInSc1d1
obranný	obranný	k2eAgInSc1d1
systém	systém	k1gInSc1
Železná	železný	k2eAgFnSc1d1
kopule	kopule	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
ničí	ničit	k5eAaImIp3nS
střely	střela	k1gFnPc4
mířící	mířící	k2eAgFnPc4d1
na	na	k7c6
obydlené	obydlený	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c7
účelem	účel	k1gInSc7
zastavení	zastavení	k1gNnSc2
ostřelování	ostřelování	k1gNnSc1
podnikl	podniknout	k5eAaPmAgMnS
několik	několik	k4yIc4
vojenských	vojenský	k2eAgFnPc2d1
operací	operace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
podnikl	podniknout	k5eAaPmAgMnS
operace	operace	k1gFnPc1
Duha	duha	k1gFnSc1
a	a	k8xC
Dny	den	k1gInPc1
pokání	pokání	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
operace	operace	k1gFnSc2
Letní	letní	k2eAgInSc1d1
déšť	déšť	k1gInSc1
a	a	k8xC
Podzimní	podzimní	k2eAgInPc1d1
mraky	mrak	k1gInPc1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
operaci	operace	k1gFnSc4
Horká	horký	k2eAgFnSc1d1
zima	zima	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
přelomu	přelom	k1gInSc6
let	léto	k1gNnPc2
2008	#num#	k4
a	a	k8xC
2009	#num#	k4
proběhla	proběhnout	k5eAaPmAgFnS
rozsáhlá	rozsáhlý	k2eAgFnSc1d1
ofenziva	ofenziva	k1gFnSc1
označovaná	označovaný	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
operace	operace	k1gFnSc1
Lité	litý	k2eAgNnSc1d1
olovo	olovo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
konfliktu	konflikt	k1gInSc2
si	se	k3xPyFc3
vyžádala	vyžádat	k5eAaPmAgFnS
asi	asi	k9
1400	#num#	k4
mrtvých	mrtvý	k1gMnPc2
a	a	k8xC
více	hodně	k6eAd2
než	než	k8xS
5000	#num#	k4
zraněných	zraněný	k2eAgMnPc2d1
<g/>
,	,	kIx,
přispěla	přispět	k5eAaPmAgFnS
však	však	k9
k	k	k7c3
dramatickému	dramatický	k2eAgInSc3d1
poklesu	pokles	k1gInSc3
počtu	počet	k1gInSc2
střel	střela	k1gFnPc2
vypálených	vypálený	k2eAgInPc2d1
na	na	k7c4
Izrael	Izrael	k1gInSc4
(	(	kIx(
<g/>
celkem	celkem	k6eAd1
o	o	k7c4
77	#num#	k4
%	%	kIx~
z	z	k7c2
3716	#num#	k4
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
na	na	k7c4
858	#num#	k4
v	v	k7c6
roce	rok	k1gInSc6
následujícím	následující	k2eAgInSc6d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc4d1
ofenzivu	ofenziva	k1gFnSc4
<g/>
,	,	kIx,
známou	známý	k2eAgFnSc4d1
jako	jako	k8xC,k8xS
operace	operace	k1gFnPc1
Pilíř	pilíř	k1gInSc1
obrany	obrana	k1gFnSc2
<g/>
,	,	kIx,
uskutečnil	uskutečnit	k5eAaPmAgInS
Izrael	Izrael	k1gInSc1
v	v	k7c6
listopadu	listopad	k1gInSc6
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zabil	zabít	k5eAaPmAgMnS
při	při	k7c6
ní	on	k3xPp3gFnSc6
vojenského	vojenský	k2eAgMnSc4d1
velitele	velitel	k1gMnSc4
Hamásu	Hamás	k1gInSc2
Ahmada	Ahmada	k1gFnSc1
Džabarího	Džabarí	k1gMnSc2
a	a	k8xC
zničil	zničit	k5eAaPmAgInS
sklady	sklad	k1gInPc4
a	a	k8xC
odpaliště	odpaliště	k1gNnSc4
modernějších	moderní	k2eAgFnPc2d2
íránských	íránský	k2eAgFnPc2d1
raket	raketa	k1gFnPc2
středního	střední	k2eAgInSc2d1
doletu	dolet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červenci	červenec	k1gInSc6
a	a	k8xC
srpnu	srpen	k1gInSc6
2014	#num#	k4
proběhla	proběhnout	k5eAaPmAgFnS
operace	operace	k1gFnSc1
Ochranné	ochranný	k2eAgNnSc4d1
ostří	ostří	k1gNnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
reakcí	reakce	k1gFnSc7
Izraele	Izrael	k1gInSc2
na	na	k7c4
opětovné	opětovný	k2eAgInPc4d1
raketové	raketový	k2eAgInPc4d1
útoky	útok	k1gInPc4
na	na	k7c6
jeho	jeho	k3xOp3gNnSc6
území	území	k1gNnSc6
ze	z	k7c2
strany	strana	k1gFnSc2
palestinských	palestinský	k2eAgFnPc2d1
militantních	militantní	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Administrativní	administrativní	k2eAgNnSc1d1
dělení	dělení	k1gNnSc1
</s>
<s>
Guvernoráty	Guvernorát	k1gInPc1
Pásma	pásmo	k1gNnSc2
Gaza	Gazum	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pásmo	pásmo	k1gNnSc1
Gaza	Gaz	k1gInSc2
je	být	k5eAaImIp3nS
rozděleno	rozdělit	k5eAaPmNgNnS
do	do	k7c2
5	#num#	k4
guvernorátů	guvernorát	k1gInPc2
(	(	kIx(
<g/>
seřazeno	seřazen	k2eAgNnSc4d1
ze	z	k7c2
severu	sever	k1gInSc2
na	na	k7c4
jih	jih	k1gInSc4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Severní	severní	k2eAgFnSc1d1
Gaza	Gaza	k1gFnSc1
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Gaza	Gaz	k1gInSc2
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Dajr	Dajr	k1gMnSc1
al-Balah	al-Balah	k1gMnSc1
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Chán	chán	k1gMnSc1
Júnis	Júnis	k1gFnSc1
</s>
<s>
Guvernorát	Guvernorát	k1gInSc1
Rafah	Rafaha	k1gFnPc2
</s>
<s>
Hraniční	hraniční	k2eAgInPc1d1
přechody	přechod	k1gInPc1
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Erez	Erez	k1gInSc1
(	(	kIx(
<g/>
gazsko-izraelský	gazsko-izraelský	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Karni	Karn	k1gMnPc1
(	(	kIx(
<g/>
gazsko-izraelský	gazsko-izraelský	k2eAgInSc4d1
<g/>
,	,	kIx,
trvale	trvale	k6eAd1
uzavřen	uzavřen	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Kerem	Kerem	k6eAd1
Šalom	šalom	k0
(	(	kIx(
<g/>
gazsko-izraelský	gazsko-izraelský	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Kisufim	Kisufim	k1gInSc1
(	(	kIx(
<g/>
gazsko-izraelský	gazsko-izraelský	k2eAgInSc1d1
<g/>
,	,	kIx,
trvale	trvale	k6eAd1
uzavřen	uzavřen	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Rafáh	Rafáh	k1gInSc1
(	(	kIx(
<g/>
gazsko-egyptský	gazsko-egyptský	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Sufa	Sufa	k1gFnSc1
(	(	kIx(
<g/>
gazsko-izraelský	gazsko-izraelský	k2eAgInSc1d1
<g/>
,	,	kIx,
trvale	trvale	k6eAd1
uzavřen	uzavřen	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Zajímavost	zajímavost	k1gFnSc1
</s>
<s>
Co	co	k9
lidé	člověk	k1gMnPc1
v	v	k7c6
Gaze	Gaze	k1gNnSc6
chtějí	chtít	k5eAaImIp3nP
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
otevření	otevření	k1gNnSc1
přechodů	přechod	k1gInPc2
<g/>
…	…	k?
nejen	nejen	k6eAd1
pro	pro	k7c4
zboží	zboží	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
lidi	člověk	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
[	[	kIx(
<g/>
Palestinci	Palestinec	k1gMnPc1
v	v	k7c6
pásmu	pásmo	k1gNnSc6
Gazy	Gaza	k1gFnSc2
<g/>
]	]	kIx)
žijí	žít	k5eAaImIp3nP
ve	v	k7c6
vězení	vězení	k1gNnSc6
pod	pod	k7c7
širým	širý	k2eAgNnSc7d1
nebem	nebe	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
—	—	k?
<g/>
John	John	k1gMnSc1
Holmes	Holmes	k1gMnSc1
<g/>
,	,	kIx,
OSN	OSN	kA
</s>
<s>
Podle	podle	k7c2
americké	americký	k2eAgFnSc2d1
diplomatické	diplomatický	k2eAgFnSc2d1
depeše	depeše	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
<g/>
,	,	kIx,
zveřejněné	zveřejněný	k2eAgFnSc6d1
serverem	server	k1gInSc7
WikiLeaks	WikiLeaksa	k1gFnPc2
<g/>
,	,	kIx,
usiloval	usilovat	k5eAaImAgInS
Izrael	Izrael	k1gInSc1
o	o	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byla	být	k5eAaImAgFnS
ekonomika	ekonomika	k1gFnSc1
v	v	k7c6
Gaze	Gaze	k1gFnSc6
„	„	k?
<g/>
stále	stále	k6eAd1
na	na	k7c6
pokraji	pokraj	k1gInSc6
kolapsu	kolaps	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2010	#num#	k4
Pásmo	pásmo	k1gNnSc4
Gaza	Gazum	k1gNnSc2
navštívil	navštívit	k5eAaPmAgMnS
šéf	šéf	k1gMnSc1
úřadu	úřad	k1gInSc2
pro	pro	k7c4
koordinaci	koordinace	k1gFnSc4
humanitárních	humanitární	k2eAgFnPc2d1
záležitostí	záležitost	k1gFnPc2
při	při	k7c6
OSN	OSN	kA
John	John	k1gMnSc1
Holmes	Holmes	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
bylo	být	k5eAaImAgNnS
na	na	k7c6
jihozápadním	jihozápadní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
Pásma	pásmo	k1gNnSc2
Gaza	Gaz	k1gInSc2
otevřeno	otevřen	k2eAgNnSc1d1
Letiště	letiště	k1gNnSc1
Jásira	Jásir	k1gMnSc2
Arafata	Arafat	k1gMnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
bylo	být	k5eAaImAgNnS
v	v	k7c6
provozu	provoz	k1gInSc6
do	do	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
izraelské	izraelský	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
zničilo	zničit	k5eAaPmAgNnS
radarovou	radarový	k2eAgFnSc4d1
stanici	stanice	k1gFnSc4
a	a	k8xC
řídicí	řídicí	k2eAgFnSc4d1
věž	věž	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dráha	dráha	k1gFnSc1
byla	být	k5eAaImAgFnS
zničena	zničit	k5eAaPmNgFnS
izraelskými	izraelský	k2eAgInPc7d1
buldozery	buldozer	k1gInPc7
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Gaza	Gaza	k1gMnSc1
Strip	Strip	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Ústav	ústav	k1gInSc1
pro	pro	k7c4
jazyk	jazyk	k1gInSc4
český	český	k2eAgInSc4d1
<g/>
.	.	kIx.
www.ujc.cas.cz	www.ujc.cas.cz	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Palestinské	palestinský	k2eAgFnSc2d1
volby	volba	k1gFnSc2
vyhrálo	vyhrát	k5eAaPmAgNnS
militantní	militantní	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
Hamas	Hamas	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
Czech	Czecha	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2006-01-26	2006-01-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://eur-lex.europa.eu/LexUriServ/site/en/oj/2005/l_340/l_34020051223en00640066.pdf	http://eur-lex.europa.eu/LexUriServ/site/en/oj/2005/l_340/l_34020051223en00640066.pdf	k1gInSc1
<g/>
↑	↑	k?
http://findarticles.com/p/articles/mi_qn4188/is_20030907/ai_n11405874	http://findarticles.com/p/articles/mi_qn4188/is_20030907/ai_n11405874	k4
<g/>
↑	↑	k?
http://www.state.gov/documents/organization/65463.pdf1	http://www.state.gov/documents/organization/65463.pdf1	k4
2	#num#	k4
The	The	k1gFnPc2
World	Worlda	k1gFnPc2
Factbook	Factbook	k1gInSc1
-	-	kIx~
Gaza	Gaza	k1gMnSc1
Strip	Strip	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Central	Central	k1gFnSc1
Intelligence	Intelligenec	k1gInSc2
Agency	Agenca	k1gFnSc2
(	(	kIx(
<g/>
CIA	CIA	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
TOAMEH	TOAMEH	kA
<g/>
,	,	kIx,
Khaled	Khaled	k1gMnSc1
Abu	Abu	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Egypt	Egypt	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Blockade	Blockad	k1gInSc5
of	of	k?
Gaza	Gazum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gatestone	Gateston	k1gInSc5
Institute	institut	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Gaza	Gaza	k1gFnSc1
Blockade	Blockad	k1gInSc5
|	|	kIx~
United	United	k1gInSc1
Nations	Nations	k1gInSc1
Office	Office	kA
for	forum	k1gNnPc2
the	the	k?
Coordination	Coordination	k1gInSc1
of	of	k?
Humanitarian	Humanitarian	k1gInSc1
Affairs	Affairs	k1gInSc1
-	-	kIx~
occupied	occupied	k1gInSc1
Palestinian	Palestinian	k1gInSc1
territory	territor	k1gInPc1
<g/>
.	.	kIx.
www.ochaopt.org	www.ochaopt.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
The	The	k1gMnSc1
Gaza	Gaza	k1gMnSc1
Strip	Strip	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Humanitarian	Humanitarian	k1gMnSc1
Impact	Impact	k1gMnSc1
of	of	k?
the	the	k?
Blockade	Blockad	k1gInSc5
|	|	kIx~
November	November	k1gInSc1
2016	#num#	k4
|	|	kIx~
United	United	k1gInSc1
Nations	Nations	k1gInSc1
Office	Office	kA
for	forum	k1gNnPc2
the	the	k?
Coordination	Coordination	k1gInSc1
of	of	k?
Humanitarian	Humanitarian	k1gInSc1
Affairs	Affairs	k1gInSc1
-	-	kIx~
occupied	occupied	k1gInSc1
Palestinian	Palestinian	k1gInSc1
territory	territor	k1gInPc1
<g/>
.	.	kIx.
www.ochaopt.org	www.ochaopt.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Crisis	Crisis	k1gInSc1
in	in	k?
Gaza	Gaza	k1gFnSc1
|	|	kIx~
Oxfam	Oxfam	k1gInSc1
International	International	k1gFnSc2
<g/>
.	.	kIx.
www.oxfam.org	www.oxfam.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Šéf	šéf	k1gMnSc1
OSN	OSN	kA
<g/>
:	:	kIx,
Izraelská	izraelský	k2eAgFnSc1d1
blokáda	blokáda	k1gFnSc1
Gazy	Gaza	k1gFnSc2
je	být	k5eAaImIp3nS
nespravedlivý	spravedlivý	k2eNgInSc1d1
kolektivní	kolektivní	k2eAgInSc1d1
trest	trest	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
TÝDEN	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Mubarak	Mubarak	k1gInSc1
calls	calls	k1gInSc1
Hamas	Hamas	k1gInSc1
<g/>
'	'	kIx"
takeover	takeover	k1gInSc1
of	of	k?
the	the	k?
Gaza	Gaza	k1gMnSc1
Strip	Strip	k1gMnSc1
a	a	k8xC
'	'	kIx"
<g/>
coup	coup	k1gInSc1
<g/>
'	'	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Haaretz	Haaretza	k1gFnPc2
<g/>
,	,	kIx,
2007-06-23	2007-06-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Reports	Reports	k1gInSc1
from	from	k1gMnSc1
Gaza	Gaza	k1gMnSc1
need	need	k1gMnSc1
a	a	k8xC
more	mor	k1gInSc5
balanced	balanced	k1gInSc1
perspective	perspectiv	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Australian	Australian	k1gMnSc1
<g/>
,	,	kIx,
2014-07-26	2014-07-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Rocket	Rocket	k1gInSc1
Attacks	Attacks	k1gInSc4
on	on	k3xPp3gMnSc1
Israel	Israel	k1gMnSc1
From	From	k1gMnSc1
Gaza	Gaza	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izraelské	izraelský	k2eAgFnPc1d1
obranné	obranný	k2eAgFnPc1d1
síly	síla	k1gFnPc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
GAZA	GAZA	kA
CROSSINGS	CROSSINGS	kA
<g/>
’	’	k?
OPERATIONS	OPERATIONS	kA
STATUS	status	k1gInSc1
<g/>
:	:	kIx,
MONTHLY	MONTHLY	kA
UPDATE	update	k1gInSc4
-	-	kIx~
JUNE	jun	k1gMnSc5
2019	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCHA	OCHA	kA
<g/>
,	,	kIx,
2019-07-17	2019-07-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
WikiLeaks	WikiLeaks	k1gInSc1
Latest	Latest	k1gMnSc1
Developments	Developments	k1gInSc1
<g/>
↑	↑	k?
https://www.thenational.ae/world/mena/gaza-is-open-air-prison-un-humanitarian-chief-1.553055	https://www.thenational.ae/world/mena/gaza-is-open-air-prison-un-humanitarian-chief-1.553055	k4
<g/>
↑	↑	k?
Years	Years	k1gInSc1
of	of	k?
delays	delays	k1gInSc1
at	at	k?
Gaza	Gaz	k1gInSc2
airport	airport	k1gInSc1
<g/>
.	.	kIx.
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Judea	Judea	k1gFnSc1
a	a	k8xC
Samaří	Samaří	k1gNnSc1
</s>
<s>
Západní	západní	k2eAgInSc1d1
břeh	břeh	k1gInSc1
Jordánu	Jordán	k1gInSc2
</s>
<s>
Izraelsko-palestinský	izraelsko-palestinský	k2eAgInSc1d1
konflikt	konflikt	k1gInSc1
</s>
<s>
Izraelský	izraelský	k2eAgInSc1d1
plán	plán	k1gInSc1
jednostranného	jednostranný	k2eAgNnSc2d1
stažení	stažení	k1gNnSc2
</s>
<s>
Blokáda	blokáda	k1gFnSc1
Pásma	pásmo	k1gNnSc2
Gazy	Gaza	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Pásmo	pásmo	k1gNnSc1
Gazy	Gaza	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Téma	téma	k1gNnSc1
Pásmo	pásmo	k1gNnSc1
Gazy	Gaza	k1gFnSc2
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Státy	stát	k1gInPc1
a	a	k8xC
území	území	k1gNnPc1
v	v	k7c6
Asii	Asie	k1gFnSc6
Nezávislé	závislý	k2eNgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1
</s>
<s>
Arménie	Arménie	k1gFnSc1
</s>
<s>
Ázerbájdžán	Ázerbájdžán	k1gInSc1
</s>
<s>
Bahrajn	Bahrajn	k1gMnSc1
</s>
<s>
Bangladéš	Bangladéš	k1gInSc1
</s>
<s>
Bhútán	Bhútán	k1gInSc1
</s>
<s>
Brunej	Brunat	k5eAaPmRp2nS,k5eAaImRp2nS
</s>
<s>
Čína	Čína	k1gFnSc1
</s>
<s>
Egypt	Egypt	k1gInSc1
</s>
<s>
Filipíny	Filipíny	k1gFnPc4
</s>
<s>
Gruzie	Gruzie	k1gFnSc1
</s>
<s>
Indie	Indie	k1gFnSc1
</s>
<s>
Indonésie	Indonésie	k1gFnSc1
</s>
<s>
Irák	Irák	k1gInSc1
</s>
<s>
Írán	Írán	k1gInSc1
</s>
<s>
Izrael	Izrael	k1gInSc1
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
Jemen	Jemen	k1gInSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Jordánsko	Jordánsko	k1gNnSc1
</s>
<s>
Kambodža	Kambodža	k1gFnSc1
</s>
<s>
Katar	katar	k1gMnSc1
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
Kuvajt	Kuvajt	k1gInSc1
</s>
<s>
Kypr	Kypr	k1gInSc1
</s>
<s>
Kyrgyzstán	Kyrgyzstán	k1gInSc1
</s>
<s>
Laos	Laos	k1gInSc1
</s>
<s>
Libanon	Libanon	k1gInSc1
</s>
<s>
Malajsie	Malajsie	k1gFnSc1
</s>
<s>
Maledivy	Maledivy	k1gFnPc1
</s>
<s>
Mongolsko	Mongolsko	k1gNnSc1
</s>
<s>
Myanmar	Myanmar	k1gInSc1
(	(	kIx(
<g/>
Barma	Barma	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Nepál	Nepál	k1gInSc1
</s>
<s>
Omán	Omán	k1gInSc1
</s>
<s>
Pákistán	Pákistán	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Singapur	Singapur	k1gInSc1
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
arabské	arabský	k2eAgInPc1d1
emiráty	emirát	k1gInPc1
</s>
<s>
Sýrie	Sýrie	k1gFnSc1
</s>
<s>
Srí	Srí	k?
Lanka	lanko	k1gNnPc1
</s>
<s>
Tádžikistán	Tádžikistán	k1gInSc1
</s>
<s>
Thajsko	Thajsko	k1gNnSc1
</s>
<s>
Turecko	Turecko	k1gNnSc1
</s>
<s>
Turkmenistán	Turkmenistán	k1gInSc1
</s>
<s>
Uzbekistán	Uzbekistán	k1gInSc1
</s>
<s>
Vietnam	Vietnam	k1gInSc1
</s>
<s>
Východní	východní	k2eAgInSc1d1
Timor	Timor	k1gInSc1
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
kolonie	kolonie	k1gFnSc2
azávislá	azávislý	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Ashmorův	Ashmorův	k2eAgInSc4d1
a	a	k8xC
Cartierův	Cartierův	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
</s>
<s>
Kokosové	kokosový	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
</s>
<s>
Vánoční	vánoční	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Čína	Čína	k1gFnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Hongkong	Hongkong	k1gInSc4
</s>
<s>
Macao	Macao	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Akrotiri	Akrotiri	k1gNnSc1
a	a	k8xC
Dekelia	Dekelia	k1gFnSc1
</s>
<s>
Britské	britský	k2eAgNnSc1d1
indickooceánské	indickooceánský	k2eAgNnSc1d1
území	území	k1gNnSc1
<g/>
)	)	kIx)
Územní	územní	k2eAgInPc1d1
celky	celek	k1gInPc1
se	se	k3xPyFc4
spornýmmezinárodním	spornýmmezinárodní	k2eAgNnSc7d1
postavením	postavení	k1gNnSc7
</s>
<s>
Abcházie	Abcházie	k1gFnSc1
</s>
<s>
Arcach	Arcach	k1gMnSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Osetie	Osetie	k1gFnSc1
</s>
<s>
Palestina	Palestina	k1gFnSc1
</s>
<s>
Severní	severní	k2eAgInSc1d1
Kypr	Kypr	k1gInSc1
</s>
<s>
Tchaj-wan	Tchaj-wan	k1gInSc1
</s>
<s>
Města	město	k1gNnSc2
a	a	k8xC
uprchlické	uprchlický	k2eAgInPc4d1
tábory	tábor	k1gInPc4
ve	v	k7c6
Státě	stát	k1gInSc6
Palestina	Palestina	k1gFnSc1
Západní	západní	k2eAgFnSc1d1
břeh	břeh	k1gInSc4
Jordánu	Jordán	k1gInSc2
</s>
<s>
Anabta	Anabta	k1gFnSc1
</s>
<s>
Bajt	bajt	k1gInSc1
Džalá	Džalý	k2eAgFnSc1d1
</s>
<s>
Betlém	Betlém	k1gInSc1
</s>
<s>
al-Bíra	al-Bír	k1gMnSc4
</s>
<s>
az-Záhiríja	az-Záhiríja	k6eAd1
</s>
<s>
Dúrá	Dúrat	k5eAaPmIp3nS,k5eAaBmIp3nS,k5eAaImIp3nS
</s>
<s>
Dženín	Dženín	k1gMnSc1
</s>
<s>
Hebron	Hebron	k1gMnSc1
</s>
<s>
Halhúl	Halhúl	k1gMnSc1
</s>
<s>
Jatta	Jatta	k1gFnSc1
</s>
<s>
Jericho	Jericho	k1gNnSc1
</s>
<s>
Kalkílija	Kalkílija	k6eAd1
</s>
<s>
Náblus	Náblus	k1gMnSc1
</s>
<s>
Rámaláh	Rámaláh	k1gMnSc1
</s>
<s>
Rawábí	Rawábit	k5eAaPmIp3nS
</s>
<s>
Túbás	Túbás	k6eAd1
</s>
<s>
Túlkarim	Túlkarim	k6eAd1
</s>
<s>
Salfít	Salfít	k5eAaPmF
Pásmo	pásmo	k1gNnSc4
Gazy	Gaza	k1gFnSc2
</s>
<s>
Baní	baně	k1gFnSc7
Suhajlá	Suhajlý	k2eAgNnPc4d1
</s>
<s>
Bajt	bajt	k1gInSc1
Hánún	Hánúna	k1gFnPc2
</s>
<s>
Bajt	bajt	k1gInSc1
Lahíja	Lahíj	k1gInSc2
</s>
<s>
Dajr	Dajr	k1gMnSc1
al-Balah	al-Balah	k1gMnSc1
</s>
<s>
Džabálijá	Džabálijat	k5eAaBmIp3nS,k5eAaImIp3nS,k5eAaPmIp3nS
</s>
<s>
Gaza	Gaza	k6eAd1
</s>
<s>
Chán	chán	k1gMnSc1
Júnis	Júnis	k1gFnSc2
</s>
<s>
Rafah	Rafah	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4019325-1	4019325-1	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
170148372	#num#	k4
</s>
