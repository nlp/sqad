<p>
<s>
Disneyland	Disneyland	k1gInSc1	Disneyland
je	být	k5eAaImIp3nS	být
tematický	tematický	k2eAgInSc1d1	tematický
zábavní	zábavní	k2eAgInSc1d1	zábavní
park	park	k1gInSc1	park
v	v	k7c6	v
Anaheimu	Anaheim	k1gInSc6	Anaheim
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
provozuje	provozovat	k5eAaImIp3nS	provozovat
společnost	společnost	k1gFnSc1	společnost
The	The	k1gFnSc2	The
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gFnSc2	Disnea
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
jej	on	k3xPp3gMnSc4	on
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
17,9	[number]	k4	17,9
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
od	od	k7c2	od
otevření	otevření	k1gNnSc2	otevření
přesáhla	přesáhnout	k5eAaPmAgFnS	přesáhnout
650	[number]	k4	650
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
má	mít	k5eAaImIp3nS	mít
hvězdu	hvězda	k1gFnSc4	hvězda
na	na	k7c6	na
Hollywoodském	hollywoodský	k2eAgInSc6d1	hollywoodský
chodníku	chodník	k1gInSc6	chodník
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
plánoval	plánovat	k5eAaImAgMnS	plánovat
Walt	Walt	k1gMnSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
postavit	postavit	k5eAaPmF	postavit
malý	malý	k2eAgInSc1d1	malý
zábavní	zábavní	k2eAgInSc1d1	zábavní
park	park	k1gInSc1	park
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
svých	svůj	k3xOyFgFnPc2	svůj
studií	studie	k1gFnPc2	studie
v	v	k7c4	v
Burbanku	Burbanka	k1gFnSc4	Burbanka
<g/>
,	,	kIx,	,
na	na	k7c6	na
pozemku	pozemek	k1gInSc6	pozemek
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
asi	asi	k9	asi
tří	tři	k4xCgInPc2	tři
hektarů	hektar	k1gInPc2	hektar
(	(	kIx(	(
<g/>
osm	osm	k4xCc4	osm
akrů	akr	k1gInPc2	akr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plán	plán	k1gInSc1	plán
byl	být	k5eAaImAgInS	být
odložen	odložit	k5eAaPmNgInS	odložit
vinou	vinou	k7c2	vinou
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
začal	začít	k5eAaPmAgInS	začít
Disney	Disnea	k1gFnSc2	Disnea
hledat	hledat	k5eAaImF	hledat
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
lokalitu	lokalita	k1gFnSc4	lokalita
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
už	už	k6eAd1	už
s	s	k7c7	s
požadovanou	požadovaný	k2eAgFnSc7d1	požadovaná
rozlohou	rozloha	k1gFnSc7	rozloha
100	[number]	k4	100
akrů	akr	k1gInPc2	akr
(	(	kIx(	(
<g/>
40	[number]	k4	40
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozemek	pozemek	k1gInSc1	pozemek
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
oblasti	oblast	k1gFnSc6	oblast
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
,	,	kIx,	,
dosažitelný	dosažitelný	k2eAgMnSc1d1	dosažitelný
po	po	k7c6	po
dálnici	dálnice	k1gFnSc6	dálnice
<g/>
.	.	kIx.	.
</s>
<s>
Vybraný	vybraný	k2eAgInSc4d1	vybraný
a	a	k8xC	a
zakoupený	zakoupený	k2eAgInSc4d1	zakoupený
pozemek	pozemek	k1gInSc4	pozemek
u	u	k7c2	u
Anaheimu	Anaheim	k1gInSc2	Anaheim
<g/>
,	,	kIx,	,
pomerančový	pomerančový	k2eAgInSc4d1	pomerančový
sad	sad	k1gInSc4	sad
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
nakonec	nakonec	k6eAd1	nakonec
rozlohu	rozloha	k1gFnSc4	rozloha
160	[number]	k4	160
akrů	akr	k1gInPc2	akr
(	(	kIx(	(
<g/>
65	[number]	k4	65
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
parku	park	k1gInSc2	park
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
a	a	k8xC	a
stála	stát	k5eAaImAgFnS	stát
17	[number]	k4	17
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Disney	Disney	k1gInPc1	Disney
pro	pro	k7c4	pro
park	park	k1gInSc4	park
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
pět	pět	k4xCc1	pět
tematicky	tematicky	k6eAd1	tematicky
odlišných	odlišný	k2eAgFnPc2d1	odlišná
oblastí	oblast	k1gFnPc2	oblast
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Main	Main	k1gNnSc1	Main
Street	Streeta	k1gFnPc2	Streeta
USA	USA	kA	USA
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
ulice	ulice	k1gFnSc1	ulice
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
Adventureland	Adventureland	k1gInSc1	Adventureland
<g/>
,	,	kIx,	,
exotická	exotický	k2eAgFnSc1d1	exotická
tropická	tropický	k2eAgFnSc1d1	tropická
oblast	oblast	k1gFnSc1	oblast
</s>
</p>
<p>
<s>
Frontierland	Frontierland	k1gInSc1	Frontierland
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
západ	západ	k1gInSc1	západ
v	v	k7c6	v
době	doba	k1gFnSc6	doba
pionýrských	pionýrský	k2eAgFnPc2d1	Pionýrská
výprav	výprava	k1gFnPc2	výprava
a	a	k8xC	a
osidlování	osidlování	k1gNnPc2	osidlování
</s>
</p>
<p>
<s>
Fantasyland	Fantasyland	k1gInSc1	Fantasyland
<g/>
,	,	kIx,	,
pohádková	pohádkový	k2eAgFnSc1d1	pohádková
oblast	oblast	k1gFnSc1	oblast
se	s	k7c7	s
zámkem	zámek	k1gInSc7	zámek
</s>
</p>
<p>
<s>
Tomorrowland	Tomorrowland	k1gInSc1	Tomorrowland
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
obsahovat	obsahovat	k5eAaImF	obsahovat
"	"	kIx"	"
<g/>
divy	diva	k1gFnPc1	diva
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
"	"	kIx"	"
<g/>
Disney	Disne	k2eAgFnPc1d1	Disne
osobně	osobně	k6eAd1	osobně
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
dohlížel	dohlížet	k5eAaImAgMnS	dohlížet
<g/>
,	,	kIx,	,
a	a	k8xC	a
staveniště	staveniště	k1gNnSc4	staveniště
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
několikrát	několikrát	k6eAd1	několikrát
týdně	týdně	k6eAd1	týdně
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
návrh	návrh	k1gInSc1	návrh
ostrova	ostrov	k1gInSc2	ostrov
Toma	Tom	k1gMnSc2	Tom
Sawyera	Sawyer	k1gMnSc2	Sawyer
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
jeho	jeho	k3xOp3gFnPc3	jeho
představám	představa	k1gFnPc3	představa
<g/>
,	,	kIx,	,
osobně	osobně	k6eAd1	osobně
plány	plán	k1gInPc4	plán
přepracoval	přepracovat	k5eAaPmAgMnS	přepracovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Park	park	k1gInSc1	park
byl	být	k5eAaImAgInS	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřen	otevřít	k5eAaPmNgInS	otevřít
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
ho	on	k3xPp3gMnSc4	on
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgInPc1d1	další
parky	park	k1gInPc1	park
provozované	provozovaný	k2eAgInPc1d1	provozovaný
Walt	Walt	k1gInSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
Company	Compana	k1gFnSc2	Compana
==	==	k?	==
</s>
</p>
<p>
<s>
Walt	Walt	k1gMnSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
World	World	k1gMnSc1	World
<g/>
,	,	kIx,	,
Orlando	Orlanda	k1gFnSc5	Orlanda
<g/>
,	,	kIx,	,
Florida	Florida	k1gFnSc1	Florida
<g/>
,	,	kIx,	,
USA	USA	kA	USA
</s>
</p>
<p>
<s>
Eurodisneyland	Eurodisneyland	k1gInSc1	Eurodisneyland
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
</s>
</p>
<p>
<s>
Hong	Hong	k1gInSc1	Hong
Kong	Kongo	k1gNnPc2	Kongo
Disneyland	Disneyland	k1gInSc4	Disneyland
<g/>
,	,	kIx,	,
Hong	Hong	k1gInSc4	Hong
Kong	Kongo	k1gNnPc2	Kongo
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
</s>
</p>
<p>
<s>
Tokyo	Tokyo	k6eAd1	Tokyo
Disneyland	Disneyland	k1gInSc1	Disneyland
<g/>
,	,	kIx,	,
Tokio	Tokio	k1gNnSc1	Tokio
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
</s>
</p>
<p>
<s>
Disneyland	Disneyland	k1gInSc1	Disneyland
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
obrázků	obrázek	k1gInPc2	obrázek
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Disneyland	Disneyland	k1gInSc1	Disneyland
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Disneyland	Disneyland	k1gInSc1	Disneyland
Paříž	Paříž	k1gFnSc1	Paříž
</s>
</p>
