<p>
<s>
Ukrajinský	ukrajinský	k2eAgInSc1d1	ukrajinský
jazyk	jazyk	k1gInSc1	jazyk
neboli	neboli	k8xC	neboli
ukrajinština	ukrajinština	k1gFnSc1	ukrajinština
(	(	kIx(	(
<g/>
zastarale	zastarale	k6eAd1	zastarale
rusínština	rusínština	k1gFnSc1	rusínština
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
východoslovanský	východoslovanský	k2eAgInSc4d1	východoslovanský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
náležící	náležící	k2eAgInSc4d1	náležící
mezi	mezi	k7c4	mezi
slovanské	slovanský	k2eAgInPc4d1	slovanský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
tzv.	tzv.	kA	tzv.
indoevropských	indoevropský	k2eAgInPc2d1	indoevropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
hlubším	hluboký	k2eAgNnSc7d2	hlubší
studiem	studio	k1gNnSc7	studio
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
filologicky	filologicky	k6eAd1	filologicky
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
obor	obor	k1gInSc1	obor
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
ukrajinistika	ukrajinistika	k1gFnSc1	ukrajinistika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
jazykové	jazykový	k2eAgFnSc6d1	jazyková
stránce	stránka	k1gFnSc6	stránka
je	být	k5eAaImIp3nS	být
nejvíce	nejvíce	k6eAd1	nejvíce
příbuzný	příbuzný	k2eAgMnSc1d1	příbuzný
s	s	k7c7	s
běloruštinou	běloruština	k1gFnSc7	běloruština
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
s	s	k7c7	s
polštinou	polština	k1gFnSc7	polština
a	a	k8xC	a
slovenštinou	slovenština	k1gFnSc7	slovenština
<g/>
.	.	kIx.	.
</s>
<s>
Vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
se	se	k3xPyFc4	se
z	z	k7c2	z
východních	východní	k2eAgNnPc2d1	východní
nářečí	nářečí	k1gNnPc2	nářečí
praslovanštiny	praslovanština	k1gFnSc2	praslovanština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Ukrajinština	ukrajinština	k1gFnSc1	ukrajinština
je	být	k5eAaImIp3nS	být
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
druhého	druhý	k4xOgInSc2	druhý
největšího	veliký	k2eAgInSc2d3	veliký
evropského	evropský	k2eAgInSc2d1	evropský
státu	stát	k1gInSc2	stát
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
jejího	její	k3xOp3gNnSc2	její
území	území	k1gNnSc2	území
si	se	k3xPyFc3	se
ovšem	ovšem	k9	ovšem
konkuruje	konkurovat	k5eAaImIp3nS	konkurovat
s	s	k7c7	s
ruštinou	ruština	k1gFnSc7	ruština
<g/>
,	,	kIx,	,
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
mluví	mluvit	k5eAaImIp3nS	mluvit
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
;	;	kIx,	;
na	na	k7c6	na
Donbasu	Donbas	k1gInSc6	Donbas
a	a	k8xC	a
na	na	k7c6	na
Krymu	Krym	k1gInSc6	Krym
je	být	k5eAaImIp3nS	být
používanější	používaný	k2eAgFnSc1d2	používanější
ruština	ruština	k1gFnSc1	ruština
(	(	kIx(	(
<g/>
na	na	k7c6	na
Krymu	Krym	k1gInSc6	Krym
ukrajinský	ukrajinský	k2eAgInSc1d1	ukrajinský
jazyk	jazyk	k1gInSc1	jazyk
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
mateřský	mateřský	k2eAgInSc4d1	mateřský
3,3	[number]	k4	3,3
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
především	především	k6eAd1	především
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
země	země	k1gFnSc1	země
ukrajinština	ukrajinština	k1gFnSc1	ukrajinština
převažuje	převažovat	k5eAaImIp3nS	převažovat
zcela	zcela	k6eAd1	zcela
jednoznačně	jednoznačně	k6eAd1	jednoznačně
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
má	mít	k5eAaImIp3nS	mít
ukrajinština	ukrajinština	k1gFnSc1	ukrajinština
status	status	k1gInSc1	status
oficiálního	oficiální	k2eAgInSc2d1	oficiální
jazyka	jazyk	k1gInSc2	jazyk
také	také	k9	také
v	v	k7c6	v
neuznané	uznaný	k2eNgFnSc6d1	neuznaná
separatistické	separatistický	k2eAgFnSc6d1	separatistická
republice	republika	k1gFnSc6	republika
Podněstří	Podněstří	k1gFnSc2	Podněstří
a	a	k8xC	a
v	v	k7c6	v
několika	několik	k4yIc6	několik
obcích	obec	k1gFnPc6	obec
ve	v	k7c6	v
Vojvodině	Vojvodina	k1gFnSc6	Vojvodina
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgMnSc2	ten
používá	používat	k5eAaImIp3nS	používat
ukrajinštinu	ukrajinština	k1gFnSc4	ukrajinština
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
menšina	menšina	k1gFnSc1	menšina
především	především	k9	především
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
v	v	k7c6	v
jihovýchodním	jihovýchodní	k2eAgNnSc6d1	jihovýchodní
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nářečí	nářečí	k1gNnSc2	nářečí
===	===	k?	===
</s>
</p>
<p>
<s>
Nářečí	nářečí	k1gNnPc1	nářečí
přecházejí	přecházet	k5eAaImIp3nP	přecházet
plynule	plynule	k6eAd1	plynule
do	do	k7c2	do
polštiny	polština	k1gFnSc2	polština
a	a	k8xC	a
běloruštiny	běloruština	k1gFnSc2	běloruština
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
jevem	jev	k1gInSc7	jev
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
suržyk	suržyk	k1gInSc1	suržyk
<g/>
,	,	kIx,	,
smíšený	smíšený	k2eAgInSc1d1	smíšený
jazyk	jazyk	k1gInSc1	jazyk
s	s	k7c7	s
převážně	převážně	k6eAd1	převážně
ukrajinskou	ukrajinský	k2eAgFnSc7d1	ukrajinská
gramatikou	gramatika	k1gFnSc7	gramatika
a	a	k8xC	a
výslovností	výslovnost	k1gFnSc7	výslovnost
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
ovlivněný	ovlivněný	k2eAgInSc1d1	ovlivněný
ruskou	ruský	k2eAgFnSc7d1	ruská
slovní	slovní	k2eAgFnSc7d1	slovní
zásobou	zásoba	k1gFnSc7	zásoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
na	na	k7c6	na
ukrajinském	ukrajinský	k2eAgNnSc6d1	ukrajinské
území	území	k1gNnSc6	území
(	(	kIx(	(
<g/>
Kyjevská	kyjevský	k2eAgFnSc1d1	Kyjevská
Rus	Rus	k1gFnSc1	Rus
<g/>
)	)	kIx)	)
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
křesťanstvím	křesťanství	k1gNnSc7	křesťanství
šířila	šířit	k5eAaImAgFnS	šířit
církevní	církevní	k2eAgFnSc1d1	církevní
slovanština	slovanština	k1gFnSc1	slovanština
a	a	k8xC	a
stávala	stávat	k5eAaImAgFnS	stávat
se	se	k3xPyFc4	se
úředním	úřední	k2eAgInSc7d1	úřední
a	a	k8xC	a
spisovným	spisovný	k2eAgInSc7d1	spisovný
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajinština	ukrajinština	k1gFnSc1	ukrajinština
pronikala	pronikat	k5eAaImAgFnS	pronikat
do	do	k7c2	do
církevních	církevní	k2eAgInPc2d1	církevní
textů	text	k1gInPc2	text
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
o	o	k7c6	o
století	století	k1gNnSc6	století
později	pozdě	k6eAd2	pozdě
už	už	k6eAd1	už
lze	lze	k6eAd1	lze
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
spisovném	spisovný	k2eAgInSc6d1	spisovný
jazyku	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
čerpající	čerpající	k2eAgFnSc1d1	čerpající
z	z	k7c2	z
lidové	lidový	k2eAgFnSc2d1	lidová
slovesnosti	slovesnost	k1gFnSc2	slovesnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
označována	označovat	k5eAaImNgFnS	označovat
ruskými	ruský	k2eAgMnPc7d1	ruský
i	i	k8xC	i
ukrajinskými	ukrajinský	k2eAgMnPc7d1	ukrajinský
autory	autor	k1gMnPc7	autor
termínem	termín	k1gInSc7	termín
maloruština	maloruština	k1gFnSc1	maloruština
nebo	nebo	k8xC	nebo
maloruské	maloruský	k2eAgNnSc1d1	maloruské
nářečí	nářečí	k1gNnSc1	nářečí
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
vycházel	vycházet	k5eAaImAgInS	vycházet
z	z	k7c2	z
koncepce	koncepce	k1gFnSc2	koncepce
např.	např.	kA	např.
A.	A.	kA	A.
I.	I.	kA	I.
Sobolenského	Sobolenský	k2eAgInSc2d1	Sobolenský
<g/>
,	,	kIx,	,
S.	S.	kA	S.
K.	K.	kA	K.
Buliče	Bulič	k1gInPc4	Bulič
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
N.	N.	kA	N.
S.	S.	kA	S.
Trubeckého	trubecký	k2eAgInSc2d1	trubecký
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yQgFnSc2	který
jsou	být	k5eAaImIp3nP	být
ukrajinské	ukrajinský	k2eAgInPc1d1	ukrajinský
jazyky	jazyk	k1gInPc1	jazyk
jen	jen	k9	jen
nářečím	nářečí	k1gNnSc7	nářečí
ruského	ruský	k2eAgInSc2d1	ruský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Filozofové	filozof	k1gMnPc1	filozof
a	a	k8xC	a
slavisté	slavista	k1gMnPc1	slavista
už	už	k6eAd1	už
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
považovali	považovat	k5eAaImAgMnP	považovat
ukrajinštinu	ukrajinština	k1gFnSc4	ukrajinština
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
jazyk	jazyk	k1gInSc4	jazyk
(	(	kIx(	(
<g/>
Vladimir	Vladimir	k1gMnSc1	Vladimir
Ivanovič	Ivanovič	k1gMnSc1	Ivanovič
Dal	dát	k5eAaPmAgMnS	dát
<g/>
,	,	kIx,	,
Pavlo	Pavla	k1gFnSc5	Pavla
Hnatovyč	Hnatovyč	k1gMnSc1	Hnatovyč
Žyteckyj	Žyteckyj	k1gMnSc1	Žyteckyj
<g/>
,	,	kIx,	,
Fedir	Fedir	k1gMnSc1	Fedir
Jevhenovyč	Jevhenovyč	k1gMnSc1	Jevhenovyč
Korš	Korš	k1gMnSc1	Korš
<g/>
,	,	kIx,	,
Alexej	Alexej	k1gMnSc1	Alexej
Alexandrovič	Alexandrovič	k1gMnSc1	Alexandrovič
Šachmatov	Šachmatov	k1gInSc1	Šachmatov
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Jozef	Jozef	k1gMnSc1	Jozef
Šafárik	Šafárik	k1gMnSc1	Šafárik
<g/>
,	,	kIx,	,
Franc	Franc	k1gMnSc1	Franc
Miklošič	Miklošič	k1gMnSc1	Miklošič
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
událostech	událost	k1gFnPc6	událost
<g/>
,	,	kIx,	,
spojených	spojený	k2eAgInPc6d1	spojený
s	s	k7c7	s
anexí	anexe	k1gFnSc7	anexe
Krymu	Krym	k1gInSc2	Krym
Ruskou	ruský	k2eAgFnSc7d1	ruská
federací	federace	k1gFnSc7	federace
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
radou	rada	k1gFnSc7	rada
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
přijat	přijmout	k5eAaPmNgInS	přijmout
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
omezující	omezující	k2eAgNnSc1d1	omezující
užívání	užívání	k1gNnSc1	užívání
ruštiny	ruština	k1gFnSc2	ruština
v	v	k7c4	v
ukrajinské	ukrajinský	k2eAgFnPc4d1	ukrajinská
celostátní	celostátní	k2eAgFnPc4d1	celostátní
<g/>
,	,	kIx,	,
místy	místo	k1gNnPc7	místo
i	i	k8xC	i
regionální	regionální	k2eAgFnSc4d1	regionální
televizi	televize	k1gFnSc4	televize
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
ukrajinštiny	ukrajinština	k1gFnSc2	ukrajinština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Abeceda	abeceda	k1gFnSc1	abeceda
a	a	k8xC	a
výslovnost	výslovnost	k1gFnSc1	výslovnost
==	==	k?	==
</s>
</p>
<p>
<s>
Ukrajinština	ukrajinština	k1gFnSc1	ukrajinština
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
cyrilicí	cyrilice	k1gFnSc7	cyrilice
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
abeceda	abeceda	k1gFnSc1	abeceda
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
písmena	písmeno	k1gNnPc4	písmeno
v	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
V	v	k7c6	v
písmu	písmo	k1gNnSc6	písmo
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
též	též	k9	též
apostrof	apostrof	k1gInSc1	apostrof
(	(	kIx(	(
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
obdobnou	obdobný	k2eAgFnSc4d1	obdobná
funkci	funkce	k1gFnSc4	funkce
jako	jako	k8xS	jako
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
znak	znak	k1gInSc4	znak
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
G	G	kA	G
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
na	na	k7c6	na
H	H	kA	H
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
označení	označení	k1gNnSc3	označení
skutečného	skutečný	k2eAgNnSc2d1	skutečné
G	G	kA	G
(	(	kIx(	(
<g/>
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
cizích	cizí	k2eAgNnPc6d1	cizí
slovech	slovo	k1gNnPc6	slovo
<g/>
)	)	kIx)	)
slouží	sloužit	k5eAaImIp3nS	sloužit
písmeno	písmeno	k1gNnSc1	písmeno
Ґ	Ґ	k?	Ґ
-	-	kIx~	-
např.	např.	kA	např.
ґ	ґ	k?	ґ
=	=	kIx~	=
grunt	grunt	k1gInSc1	grunt
<g/>
.	.	kIx.	.
</s>
<s>
Hláska	hláska	k1gFnSc1	hláska
H	H	kA	H
se	se	k3xPyFc4	se
před	před	k7c4	před
G	G	kA	G
upřednostňuje	upřednostňovat	k5eAaImIp3nS	upřednostňovat
až	až	k9	až
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
váží	vážit	k5eAaImIp3nS	vážit
na	na	k7c4	na
hramy	hrama	k1gFnPc4	hrama
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
cestovatel	cestovatel	k1gMnSc1	cestovatel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jede	jet	k5eAaImIp3nS	jet
na	na	k7c4	na
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
<g/>
,	,	kIx,	,
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
imihracijnu	imihracijen	k2eAgFnSc4d1	imihracijen
kartku	kartka	k1gFnSc4	kartka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
I	i	k9	i
(	(	kIx(	(
<g/>
ikavismus	ikavismus	k1gInSc1	ikavismus
<g/>
)	)	kIx)	)
tam	tam	k6eAd1	tam
kde	kde	k6eAd1	kde
jiné	jiný	k2eAgInPc1d1	jiný
slovanské	slovanský	k2eAgInPc1d1	slovanský
jazyky	jazyk	k1gInPc1	jazyk
mají	mít	k5eAaImIp3nP	mít
Ě	Ě	kA	Ě
<g/>
,	,	kIx,	,
O	O	kA	O
nebo	nebo	k8xC	nebo
E	E	kA	E
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
jednoslabičných	jednoslabičný	k2eAgNnPc6d1	jednoslabičné
slovech	slovo	k1gNnPc6	slovo
(	(	kIx(	(
<g/>
bis	bis	k?	bis
=	=	kIx~	=
běs	běs	k1gMnSc1	běs
<g/>
,	,	kIx,	,
pid	pid	k?	pid
=	=	kIx~	=
pod	pod	k7c7	pod
<g/>
,	,	kIx,	,
lid	lid	k1gInSc1	lid
=	=	kIx~	=
led	led	k1gInSc1	led
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
začínala	začínat	k5eAaImAgFnS	začínat
samohláskou	samohláska	k1gFnSc7	samohláska
O	O	kA	O
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
předsouvá	předsouvat	k5eAaImIp3nS	předsouvat
V	v	k7c6	v
(	(	kIx(	(
<g/>
jako	jako	k9	jako
v	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
češtině	čeština	k1gFnSc6	čeština
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
в	в	k?	в
=	=	kIx~	=
on	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ruštiny	ruština	k1gFnSc2	ruština
a	a	k8xC	a
běloruštiny	běloruština	k1gFnSc2	běloruština
se	se	k3xPyFc4	se
nepřízvučné	přízvučný	k2eNgFnSc2d1	nepřízvučná
O	O	kA	O
nemění	měnit	k5eNaImIp3nS	měnit
na	na	k7c6	na
A.	A.	kA	A.
</s>
</p>
<p>
<s>
==	==	k?	==
Slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
==	==	k?	==
</s>
</p>
<p>
<s>
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
má	mít	k5eAaImIp3nS	mít
blíže	blízce	k6eAd2	blízce
západoslovanským	západoslovanský	k2eAgInPc3d1	západoslovanský
jazykům	jazyk	k1gInPc3	jazyk
(	(	kIx(	(
<g/>
zejména	zejména	k6eAd1	zejména
polštině	polština	k1gFnSc3	polština
<g/>
)	)	kIx)	)
než	než	k8xS	než
ruština	ruština	k1gFnSc1	ruština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vzorový	vzorový	k2eAgInSc1d1	vzorový
text	text	k1gInSc1	text
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
deklarace	deklarace	k1gFnSc1	deklarace
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
ukrajinštiny	ukrajinština	k1gFnSc2	ukrajinština
s	s	k7c7	s
ruštinou	ruština	k1gFnSc7	ruština
je	být	k5eAaImIp3nS	být
text	text	k1gInSc1	text
uveden	uveden	k2eAgInSc1d1	uveden
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
písmu	písmo	k1gNnSc6	písmo
se	se	k3xPyFc4	se
od	od	k7c2	od
ruštiny	ruština	k1gFnSc2	ruština
dá	dát	k5eAaPmIp3nS	dát
snadno	snadno	k6eAd1	snadno
odlišit	odlišit	k5eAaPmF	odlišit
znakem	znak	k1gInSc7	znak
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
latince	latinka	k1gFnSc6	latinka
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
i.	i.	k?	i.
</s>
</p>
<p>
<s>
===	===	k?	===
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
deklarace	deklarace	k1gFnSc1	deklarace
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Suržyk	Suržyk	k6eAd1	Suržyk
–	–	k?	–
směs	směs	k1gFnSc1	směs
ukrajinštiny	ukrajinština	k1gFnSc2	ukrajinština
a	a	k8xC	a
ruštiny	ruština	k1gFnSc2	ruština
</s>
</p>
<p>
<s>
Rusínština	rusínština	k1gFnSc1	rusínština
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
spisovatelů	spisovatel	k1gMnPc2	spisovatel
</s>
</p>
<p>
<s>
Jazyková	jazykový	k2eAgFnSc1d1	jazyková
situace	situace	k1gFnSc1	situace
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ukrajinština	ukrajinština	k1gFnSc1	ukrajinština
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
ukrajinština	ukrajinština	k1gFnSc1	ukrajinština
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Ukrainians	Ukrainians	k1gInSc1	Ukrainians
<g/>
,	,	kIx,	,
Encyclopedia	Encyclopedium	k1gNnPc1	Encyclopedium
of	of	k?	of
Ukraine	Ukrain	k1gInSc5	Ukrain
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Slovanský	slovanský	k2eAgInSc1d1	slovanský
národopis	národopis	k1gInSc1	národopis
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Jozef	Jozef	k1gMnSc1	Jozef
Šafárik	Šafárik	k1gMnSc1	Šafárik
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
8	[number]	k4	8
–	–	k?	–
29	[number]	k4	29
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Geschichte	Geschicht	k1gInSc5	Geschicht
der	drát	k5eAaImRp2nS	drát
slawischen	slawischen	k2eAgMnSc1d1	slawischen
Sprache	Sprache	k1gFnSc7	Sprache
und	und	k?	und
Literatur	literatura	k1gFnPc2	literatura
nach	nach	k1gInSc4	nach
allen	allen	k1gInSc4	allen
Mundarten	Mundartno	k1gNnPc2	Mundartno
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Jozef	Jozef	k1gMnSc1	Jozef
Šafářik	Šafářik	k1gMnSc1	Šafářik
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.33	.33	k4	.33
<g/>
,	,	kIx,	,
34	[number]	k4	34
a	a	k8xC	a
23	[number]	k4	23
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
<g/>
)	)	kIx)	)
Historie	historie	k1gFnSc1	historie
ukrajinského	ukrajinský	k2eAgInSc2d1	ukrajinský
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
první	první	k4xOgInSc1	první
odstavec	odstavec	k1gInSc1	odstavec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
<g/>
)	)	kIx)	)
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
ukrajinština	ukrajinština	k1gFnSc1	ukrajinština
regionální	regionální	k2eAgFnSc2d1	regionální
úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
Srbska	Srbsko	k1gNnSc2	Srbsko
</s>
</p>
