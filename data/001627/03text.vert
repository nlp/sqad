<s>
Walter	Walter	k1gMnSc1	Walter
Houser	houser	k1gMnSc1	houser
Brattain	Brattain	k1gMnSc1	Brattain
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1902	[number]	k4	1902
<g/>
,	,	kIx,	,
Amoy	Amoa	k1gFnPc1	Amoa
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
-	-	kIx~	-
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
Seattle	Seattle	k1gFnSc1	Seattle
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Bardeenem	Bardeen	k1gMnSc7	Bardeen
a	a	k8xC	a
Williamem	William	k1gInSc7	William
Shockleym	Shockleym	k1gInSc1	Shockleym
objevil	objevit	k5eAaPmAgInS	objevit
tranzistor	tranzistor	k1gInSc4	tranzistor
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
objev	objev	k1gInSc4	objev
získali	získat	k5eAaPmAgMnP	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c4	v
Amoy	Amoa	k1gFnPc4	Amoa
<g/>
,	,	kIx,	,
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
rané	raný	k2eAgNnSc4d1	rané
dětství	dětství	k1gNnSc4	dětství
strávil	strávit	k5eAaPmAgMnS	strávit
ve	v	k7c6	v
Springfieldu	Springfield	k1gInSc6	Springfield
<g/>
,	,	kIx,	,
Oregonu	Oregon	k1gInSc6	Oregon
a	a	k8xC	a
Washingtonu	Washington	k1gInSc6	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
ve	v	k7c6	v
státu	stát	k1gInSc6	stát
Washington	Washington	k1gInSc1	Washington
na	na	k7c6	na
farmě	farma	k1gFnSc6	farma
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
Rosse	Rosse	k1gFnPc1	Rosse
R.	R.	kA	R.
Brattaina	Brattaina	k1gFnSc1	Brattaina
a	a	k8xC	a
Ottilie	Ottilie	k1gFnSc1	Ottilie
Houser	houser	k1gInSc1	houser
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
titul	titul	k1gInSc1	titul
získal	získat	k5eAaPmAgInS	získat
z	z	k7c2	z
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
matematiky	matematika	k1gFnSc2	matematika
na	na	k7c6	na
Whitman	Whitman	k1gMnSc1	Whitman
College	College	k1gNnSc2	College
in	in	k?	in
Walla	Walla	k1gMnSc1	Walla
Walla	Walla	k1gMnSc1	Walla
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
získal	získat	k5eAaPmAgMnS	získat
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
Minnesota	Minnesota	k1gFnSc1	Minnesota
magisterský	magisterský	k2eAgInSc1d1	magisterský
titul	titul	k1gInSc4	titul
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
univerzitě	univerzita	k1gFnSc6	univerzita
titul	titul	k1gInSc1	titul
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucím	vedoucí	k1gMnSc7	vedoucí
doktorandské	doktorandský	k2eAgFnSc2d1	doktorandská
práce	práce	k1gFnSc2	práce
byl	být	k5eAaImAgMnS	být
John	John	k1gMnSc1	John
T.	T.	kA	T.
Tate	Tate	k1gNnSc2	Tate
a	a	k8xC	a
tématem	téma	k1gNnSc7	téma
byl	být	k5eAaImAgInS	být
dopad	dopad	k1gInSc1	dopad
elektronů	elektron	k1gInPc2	elektron
na	na	k7c4	na
rtuťové	rtuťový	k2eAgInPc4d1	rtuťový
výpary	výpar	k1gInPc4	výpar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1928	[number]	k4	1928
až	až	k9	až
1929	[number]	k4	1929
pracoval	pracovat	k5eAaImAgInS	pracovat
v	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
úřadu	úřad	k1gInSc6	úřad
pro	pro	k7c4	pro
míry	míra	k1gFnPc4	míra
a	a	k8xC	a
váhy	váha	k1gFnPc4	váha
(	(	kIx(	(
<g/>
National	National	k1gFnSc4	National
Bureau	Bureaus	k1gInSc2	Bureaus
of	of	k?	of
Standards	Standards	k1gInSc1	Standards
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnSc6	D.C.
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
přešel	přejít	k5eAaPmAgInS	přejít
do	do	k7c2	do
Bellových	Bellův	k2eAgFnPc2d1	Bellova
laboratoří	laboratoř	k1gFnPc2	laboratoř
v	v	k7c6	v
New	New	k1gFnSc6	New
Yersey	Yersea	k1gFnSc2	Yersea
<g/>
.	.	kIx.	.
</s>
<s>
Brattainův	Brattainův	k2eAgInSc1d1	Brattainův
výzkum	výzkum	k1gInSc1	výzkum
v	v	k7c6	v
Bellových	Bellův	k2eAgFnPc6d1	Bellova
laboratořích	laboratoř	k1gFnPc6	laboratoř
před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
se	se	k3xPyFc4	se
nejdříve	dříve	k6eAd3	dříve
týkal	týkat	k5eAaImAgInS	týkat
fyziky	fyzika	k1gFnSc2	fyzika
povrchu	povrch	k1gInSc2	povrch
wolframu	wolfram	k1gInSc2	wolfram
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
povrchu	povrch	k1gInSc2	povrch
oxidů	oxid	k1gInPc2	oxid
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
povrchu	povrch	k1gInSc2	povrch
křemíku	křemík	k1gInSc2	křemík
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
rozvoji	rozvoj	k1gInSc3	rozvoj
metod	metoda	k1gFnPc2	metoda
odhalení	odhalení	k1gNnSc2	odhalení
ponorek	ponorka	k1gFnPc2	ponorka
pro	pro	k7c4	pro
Columbia	Columbia	k1gFnSc1	Columbia
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
Bellových	Bellův	k2eAgFnPc2d1	Bellova
laboratoří	laboratoř	k1gFnPc2	laboratoř
a	a	k8xC	a
připojil	připojit	k5eAaPmAgMnS	připojit
se	se	k3xPyFc4	se
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
vedené	vedený	k2eAgFnSc3d1	vedená
Williamem	William	k1gInSc7	William
Shockleym	Shockleym	k1gInSc1	Shockleym
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
vedl	vést	k5eAaImAgInS	vést
hlavní	hlavní	k2eAgInSc1d1	hlavní
výzkum	výzkum	k1gInSc1	výzkum
polovodičů	polovodič	k1gInPc2	polovodič
pro	pro	k7c4	pro
praktické	praktický	k2eAgNnSc4d1	praktické
vytvoření	vytvoření	k1gNnSc4	vytvoření
zesilovače	zesilovač	k1gInSc2	zesilovač
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
výzkum	výzkum	k1gInSc1	výzkum
vedl	vést	k5eAaImAgInS	vést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
k	k	k7c3	k
objevu	objev	k1gInSc3	objev
hrotového	hrotový	k2eAgInSc2d1	hrotový
tranzistoru	tranzistor	k1gInSc2	tranzistor
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
objev	objev	k1gInSc4	objev
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
Brattain	Brattain	k1gMnSc1	Brattain
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Bardeen	Bardeen	k1gInSc1	Bardeen
a	a	k8xC	a
William	William	k1gInSc1	William
Shockley	Shocklea	k1gFnSc2	Shocklea
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Walter	Walter	k1gMnSc1	Walter
Brattain	Brattain	k1gMnSc1	Brattain
zemřel	zemřít	k5eAaPmAgMnS	zemřít
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1987	[number]	k4	1987
v	v	k7c6	v
Seattlu	Seattl	k1gInSc6	Seattl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
John	John	k1gMnSc1	John
Bardeen	Bardena	k1gFnPc2	Bardena
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Walter	Walter	k1gMnSc1	Walter
Houser	houser	k1gMnSc1	houser
Brattain	Brattain	k2eAgMnSc1d1	Brattain
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Biography	Biographa	k1gFnSc2	Biographa
from	from	k1gMnSc1	from
the	the	k?	the
Nobel	Nobel	k1gMnSc1	Nobel
Foundation	Foundation	k1gInSc4	Foundation
</s>
