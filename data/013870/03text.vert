<s>
765	#num#	k4
(	(	kIx(
<g/>
číslo	číslo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
←	←	k?
764	#num#	k4
</s>
<s>
765	#num#	k4
</s>
<s>
766	#num#	k4
→	→	k?
</s>
<s>
Celé	celý	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
765	#num#	k4
</s>
<s>
sedm	sedm	k4xCc4
set	sto	k4xCgNnPc2
šedesát	šedesát	k4xCc1
pět	pět	k4xCc4
</s>
<s>
Rozklad	rozklad	k1gInSc1
</s>
<s>
32	#num#	k4
·	·	k?
5	#num#	k4
·	·	k?
17	#num#	k4
</s>
<s>
Dělitelé	dělitel	k1gMnPc1
</s>
<s>
1	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
9	#num#	k4
<g/>
,	,	kIx,
15	#num#	k4
<g/>
,	,	kIx,
17	#num#	k4
<g/>
,	,	kIx,
45	#num#	k4
<g/>
,	,	kIx,
51	#num#	k4
<g/>
,	,	kIx,
85	#num#	k4
<g/>
,	,	kIx,
153	#num#	k4
<g/>
,	,	kIx,
255	#num#	k4
<g/>
,	,	kIx,
765	#num#	k4
</s>
<s>
Římskými	římský	k2eAgFnPc7d1
číslicemi	číslice	k1gFnPc7
</s>
<s>
DCCLXV	DCCLXV	kA
</s>
<s>
Dvojkově	dvojkově	k6eAd1
</s>
<s>
1011111101	#num#	k4
</s>
<s>
Trojkově	trojkově	k6eAd1
</s>
<s>
1001100	#num#	k4
</s>
<s>
Čtyřkově	Čtyřkově	k6eAd1
</s>
<s>
23331	#num#	k4
</s>
<s>
Pětkově	pětkově	k6eAd1
</s>
<s>
11030	#num#	k4
</s>
<s>
Šestkově	šestkově	k6eAd1
</s>
<s>
3313	#num#	k4
</s>
<s>
Sedmičkově	Sedmičkově	k6eAd1
</s>
<s>
2142	#num#	k4
</s>
<s>
Osmičkově	osmičkově	k6eAd1
</s>
<s>
1375	#num#	k4
</s>
<s>
Dvanáctkově	dvanáctkově	k6eAd1
</s>
<s>
539	#num#	k4
</s>
<s>
Šestnáctkově	šestnáctkově	k6eAd1
</s>
<s>
2FD	2FD	k4
</s>
<s>
Sedm	sedm	k4xCc4
set	sto	k4xCgNnPc2
šedesát	šedesát	k4xCc1
pět	pět	k4xCc4
je	být	k5eAaImIp3nS
přirozené	přirozený	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římskými	římský	k2eAgFnPc7d1
číslicemi	číslice	k1gFnPc7
se	se	k3xPyFc4
zapisuje	zapisovat	k5eAaImIp3nS
DCCLXV	DCCLXV	kA
a	a	k8xC
řeckými	řecký	k2eAgFnPc7d1
číslicemi	číslice	k1gFnPc7
ψ	ψ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následuje	následovat	k5eAaImIp3nS
po	po	k7c6
čísle	číslo	k1gNnSc6
sedm	sedm	k4xCc4
set	sto	k4xCgNnPc2
šedesát	šedesát	k4xCc1
čtyři	čtyři	k4xCgMnPc1
a	a	k8xC
předchází	předcházet	k5eAaImIp3nP
číslu	číslo	k1gNnSc3
sedm	sedm	k4xCc4
set	sto	k4xCgNnPc2
šedesát	šedesát	k4xCc1
šest	šest	k4xCc1
<g/>
.	.	kIx.
</s>
<s>
Matematika	matematika	k1gFnSc1
</s>
<s>
765	#num#	k4
je	být	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
Deficientní	Deficientní	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
Složené	složený	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
Nešťastné	šťastný	k2eNgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
765	#num#	k4
</s>
<s>
765	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gNnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc4
<g/>
}	}	kIx)
Přirozená	přirozený	k2eAgNnPc4d1
čísla	číslo	k1gNnPc4
700	#num#	k4
<g/>
–	–	k?
<g/>
799	#num#	k4
</s>
<s>
700	#num#	k4
•	•	k?
701	#num#	k4
•	•	k?
702	#num#	k4
•	•	k?
703	#num#	k4
•	•	k?
704	#num#	k4
•	•	k?
705	#num#	k4
•	•	k?
706	#num#	k4
•	•	k?
707	#num#	k4
•	•	k?
708	#num#	k4
•	•	k?
709	#num#	k4
•	•	k?
710	#num#	k4
•	•	k?
711	#num#	k4
•	•	k?
712	#num#	k4
•	•	k?
713	#num#	k4
•	•	k?
714	#num#	k4
•	•	k?
715	#num#	k4
•	•	k?
716	#num#	k4
•	•	k?
717	#num#	k4
•	•	k?
718	#num#	k4
•	•	k?
719	#num#	k4
•	•	k?
720	#num#	k4
•	•	k?
721	#num#	k4
•	•	k?
722	#num#	k4
•	•	k?
723	#num#	k4
•	•	k?
724	#num#	k4
•	•	k?
725	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
726	#num#	k4
•	•	k?
727	#num#	k4
•	•	k?
728	#num#	k4
•	•	k?
729	#num#	k4
•	•	k?
730	#num#	k4
•	•	k?
731	#num#	k4
•	•	k?
732	#num#	k4
•	•	k?
733	#num#	k4
•	•	k?
734	#num#	k4
•	•	k?
735	#num#	k4
•	•	k?
736	#num#	k4
•	•	k?
737	#num#	k4
•	•	k?
738	#num#	k4
•	•	k?
739	#num#	k4
•	•	k?
740	#num#	k4
•	•	k?
741	#num#	k4
•	•	k?
742	#num#	k4
•	•	k?
743	#num#	k4
•	•	k?
744	#num#	k4
•	•	k?
745	#num#	k4
•	•	k?
746	#num#	k4
•	•	k?
747	#num#	k4
•	•	k?
748	#num#	k4
•	•	k?
749	#num#	k4
•	•	k?
750	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
751	#num#	k4
•	•	k?
752	#num#	k4
•	•	k?
753	#num#	k4
•	•	k?
754	#num#	k4
•	•	k?
755	#num#	k4
•	•	k?
756	#num#	k4
•	•	k?
757	#num#	k4
•	•	k?
758	#num#	k4
•	•	k?
759	#num#	k4
•	•	k?
760	#num#	k4
•	•	k?
761	#num#	k4
•	•	k?
762	#num#	k4
•	•	k?
763	#num#	k4
•	•	k?
764	#num#	k4
•	•	k?
765	#num#	k4
•	•	k?
766	#num#	k4
•	•	k?
767	#num#	k4
•	•	k?
768	#num#	k4
•	•	k?
769	#num#	k4
•	•	k?
770	#num#	k4
•	•	k?
771	#num#	k4
•	•	k?
772	#num#	k4
•	•	k?
773	#num#	k4
•	•	k?
774	#num#	k4
•	•	k?
775	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
776	#num#	k4
•	•	k?
777	#num#	k4
•	•	k?
778	#num#	k4
•	•	k?
779	#num#	k4
•	•	k?
780	#num#	k4
•	•	k?
781	#num#	k4
•	•	k?
782	#num#	k4
•	•	k?
783	#num#	k4
•	•	k?
784	#num#	k4
•	•	k?
785	#num#	k4
•	•	k?
786	#num#	k4
•	•	k?
787	#num#	k4
•	•	k?
788	#num#	k4
•	•	k?
789	#num#	k4
•	•	k?
790	#num#	k4
•	•	k?
791	#num#	k4
•	•	k?
792	#num#	k4
•	•	k?
793	#num#	k4
•	•	k?
794	#num#	k4
•	•	k?
795	#num#	k4
•	•	k?
796	#num#	k4
•	•	k?
797	#num#	k4
•	•	k?
798	#num#	k4
•	•	k?
799	#num#	k4
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
–	–	k?
<g/>
99	#num#	k4
•	•	k?
100	#num#	k4
<g/>
–	–	k?
<g/>
199	#num#	k4
•	•	k?
200	#num#	k4
<g/>
–	–	k?
<g/>
299	#num#	k4
•	•	k?
300	#num#	k4
<g/>
–	–	k?
<g/>
399	#num#	k4
•	•	k?
400	#num#	k4
<g/>
–	–	k?
<g/>
499	#num#	k4
•	•	k?
500	#num#	k4
<g/>
–	–	k?
<g/>
599	#num#	k4
•	•	k?
600	#num#	k4
<g/>
–	–	k?
<g/>
699	#num#	k4
•	•	k?
700	#num#	k4
<g/>
–	–	k?
<g/>
799	#num#	k4
•	•	k?
800	#num#	k4
<g/>
–	–	k?
<g/>
899	#num#	k4
•	•	k?
900	#num#	k4
<g/>
–	–	k?
<g/>
999	#num#	k4
•	•	k?
1000	#num#	k4
<g/>
–	–	k?
<g/>
1099	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Matematika	matematika	k1gFnSc1
</s>
