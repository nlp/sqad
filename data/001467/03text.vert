<s>
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Conrad	Conrada	k1gFnPc2	Conrada
Röntgen	Röntgen	k1gInSc1	Röntgen
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1845	[number]	k4	1845
Lennep	Lennep	k1gInSc1	Lennep
<g/>
,	,	kIx,	,
Prusko	Prusko	k1gNnSc1	Prusko
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1923	[number]	k4	1923
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
objevitel	objevitel	k1gMnSc1	objevitel
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgMnSc1d1	německý
fyzik	fyzik	k1gMnSc1	fyzik
s	s	k7c7	s
nizozemskými	nizozemský	k2eAgMnPc7d1	nizozemský
předky	předek	k1gMnPc7	předek
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1845	[number]	k4	1845
v	v	k7c4	v
Lennep	Lennep	k1gInSc4	Lennep
jako	jako	k8xS	jako
jediné	jediný	k2eAgNnSc4d1	jediné
dítě	dítě	k1gNnSc4	dítě
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
obchodníka	obchodník	k1gMnSc2	obchodník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
třech	tři	k4xCgInPc6	tři
letech	let	k1gInPc6	let
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
nizozemského	nizozemský	k2eAgInSc2d1	nizozemský
Apeldoornu	Apeldoorn	k1gInSc2	Apeldoorn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šestnácti	šestnáct	k4xCc6	šestnáct
letech	léto	k1gNnPc6	léto
ho	on	k3xPp3gMnSc4	on
rodiče	rodič	k1gMnPc1	rodič
poslali	poslat	k5eAaPmAgMnP	poslat
za	za	k7c7	za
dalším	další	k2eAgNnSc7d1	další
vzděláním	vzdělání	k1gNnSc7	vzdělání
do	do	k7c2	do
Utrechtu	Utrecht	k1gInSc2	Utrecht
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žil	žít	k5eAaImAgMnS	žít
ve	v	k7c6	v
spřátelené	spřátelený	k2eAgFnSc6d1	spřátelená
rodině	rodina	k1gFnSc6	rodina
u	u	k7c2	u
profesora	profesor	k1gMnSc2	profesor
chemie	chemie	k1gFnSc2	chemie
místní	místní	k2eAgFnSc2d1	místní
univerzity	univerzita	k1gFnSc2	univerzita
Jana	Jan	k1gMnSc2	Jan
Willema	Willem	k1gMnSc2	Willem
Guninga	Guning	k1gMnSc2	Guning
<g/>
.	.	kIx.	.
</s>
<s>
Návštěva	návštěva	k1gFnSc1	návštěva
utrechtské	utrechtský	k2eAgFnSc2d1	utrechtská
technické	technický	k2eAgFnSc2d1	technická
školy	škola	k1gFnSc2	škola
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
přípravou	příprava	k1gFnSc7	příprava
na	na	k7c4	na
Röntgenovo	Röntgenův	k2eAgNnSc4d1	Röntgenovo
povolání	povolání	k1gNnSc4	povolání
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
nebyla	být	k5eNaImAgFnS	být
tam	tam	k6eAd1	tam
vyučována	vyučován	k2eAgFnSc1d1	vyučována
latina	latina	k1gFnSc1	latina
ani	ani	k8xC	ani
řečtina	řečtina	k1gFnSc1	řečtina
<g/>
,	,	kIx,	,
u	u	k7c2	u
absolventů	absolvent	k1gMnPc2	absolvent
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
nepředpokládalo	předpokládat	k5eNaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
chtěli	chtít	k5eAaImAgMnP	chtít
později	pozdě	k6eAd2	pozdě
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Chybějící	chybějící	k2eAgFnPc1d1	chybějící
znalosti	znalost	k1gFnPc1	znalost
klasických	klasický	k2eAgInPc2d1	klasický
jazyků	jazyk	k1gInPc2	jazyk
působily	působit	k5eAaImAgFnP	působit
Röntgenovi	Röntgenův	k2eAgMnPc1d1	Röntgenův
v	v	k7c6	v
akademické	akademický	k2eAgFnSc6d1	akademická
kariéře	kariéra	k1gFnSc6	kariéra
nemalé	malý	k2eNgFnSc2d1	nemalá
těžkosti	těžkost	k1gFnSc2	těžkost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
známek	známka	k1gFnPc2	známka
na	na	k7c4	na
vysvědčení	vysvědčení	k1gNnSc4	vysvědčení
si	se	k3xPyFc3	se
vedl	vést	k5eAaImAgInS	vést
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
v	v	k7c6	v
technických	technický	k2eAgInPc6d1	technický
předmětech	předmět	k1gInPc6	předmět
<g/>
,	,	kIx,	,
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
,	,	kIx,	,
angličtině	angličtina	k1gFnSc6	angličtina
i	i	k8xC	i
"	"	kIx"	"
<g/>
hochdeutsch	hochdeutsch	k1gInSc1	hochdeutsch
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
studia	studio	k1gNnSc2	studio
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
karikatura	karikatura	k1gFnSc1	karikatura
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
pedagogů	pedagog	k1gMnPc2	pedagog
<g/>
,	,	kIx,	,
nakreslená	nakreslený	k2eAgFnSc1d1	nakreslená
křídou	křída	k1gFnSc7	křída
na	na	k7c6	na
zástěně	zástěna	k1gFnSc6	zástěna
ke	k	k7c3	k
kamnům	kamna	k1gNnPc3	kamna
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgInS	být
někdo	někdo	k3yInSc1	někdo
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
však	však	k9	však
byl	být	k5eAaImAgInS	být
Röntgen	Röntgen	k1gInSc1	Röntgen
ze	z	k7c2	z
studií	studie	k1gFnPc2	studie
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Událost	událost	k1gFnSc1	událost
měla	mít	k5eAaImAgFnS	mít
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
další	další	k2eAgInSc4d1	další
Röntgenův	Röntgenův	k2eAgInSc4d1	Röntgenův
osud	osud	k1gInSc4	osud
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
maturity	maturita	k1gFnSc2	maturita
se	se	k3xPyFc4	se
nemohl	moct	k5eNaImAgMnS	moct
totiž	totiž	k9	totiž
zapsat	zapsat	k5eAaPmF	zapsat
na	na	k7c4	na
žádnou	žádný	k3yNgFnSc4	žádný
německou	německý	k2eAgFnSc4d1	německá
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
nebránil	bránit	k5eNaImAgInS	bránit
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
ze	z	k7c2	z
zájmu	zájem	k1gInSc2	zájem
na	na	k7c6	na
utrechtské	utrechtský	k2eAgFnSc6d1	utrechtská
univerzitě	univerzita	k1gFnSc6	univerzita
přednášky	přednáška	k1gFnSc2	přednáška
z	z	k7c2	z
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
chemie	chemie	k1gFnSc2	chemie
<g/>
,	,	kIx,	,
zoologie	zoologie	k1gFnSc2	zoologie
a	a	k8xC	a
botaniky	botanika	k1gFnSc2	botanika
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Röntgen	Röntgen	k1gInSc1	Röntgen
vyloučení	vyloučení	k1gNnSc2	vyloučení
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
křivdu	křivda	k1gFnSc4	křivda
<g/>
,	,	kIx,	,
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
cíle	cíl	k1gInSc2	cíl
mít	mít	k5eAaImF	mít
maturitu	maturita	k1gFnSc4	maturita
neupustil	upustit	k5eNaPmAgInS	upustit
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
člen	člen	k1gMnSc1	člen
zkušební	zkušební	k2eAgFnSc2d1	zkušební
komise	komise	k1gFnSc2	komise
pro	pro	k7c4	pro
privátní	privátní	k2eAgFnSc4d1	privátní
zkoušku	zkouška	k1gFnSc4	zkouška
dospělosti	dospělost	k1gFnSc2	dospělost
ale	ale	k8xC	ale
náhle	náhle	k6eAd1	náhle
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
<g/>
;	;	kIx,	;
náhradní	náhradní	k2eAgMnSc1d1	náhradní
examinátor	examinátor	k1gMnSc1	examinátor
přišel	přijít	k5eAaPmAgMnS	přijít
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
Röntgena	Röntgena	k1gFnSc1	Röntgena
předtím	předtím	k6eAd1	předtím
vyloučila	vyloučit	k5eAaPmAgFnS	vyloučit
a	a	k8xC	a
uchazeč	uchazeč	k1gMnSc1	uchazeč
propadl	propadnout	k5eAaPmAgMnS	propadnout
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
podzimu	podzim	k1gInSc2	podzim
1865	[number]	k4	1865
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c6	na
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
technické	technický	k2eAgFnSc6d1	technická
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přijímala	přijímat	k5eAaImAgFnS	přijímat
zájemce	zájemce	k1gMnPc4	zájemce
i	i	k9	i
bez	bez	k7c2	bez
maturitního	maturitní	k2eAgNnSc2d1	maturitní
vysvědčení	vysvědčení	k1gNnSc2	vysvědčení
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vstupní	vstupní	k2eAgFnSc2d1	vstupní
zkoušky	zkouška	k1gFnSc2	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
šesti	šest	k4xCc6	šest
semestrech	semestr	k1gInPc6	semestr
studia	studio	k1gNnSc2	studio
stavby	stavba	k1gFnSc2	stavba
strojů	stroj	k1gInPc2	stroj
získal	získat	k5eAaPmAgInS	získat
diplom	diplom	k1gInSc1	diplom
strojního	strojní	k2eAgMnSc2d1	strojní
inženýra	inženýr	k1gMnSc2	inženýr
a	a	k8xC	a
rok	rok	k1gInSc4	rok
nato	nato	k6eAd1	nato
ještě	ještě	k6eAd1	ještě
doktorát	doktorát	k1gInSc4	doktorát
filozofie	filozofie	k1gFnSc2	filozofie
na	na	k7c6	na
curyšské	curyšský	k2eAgFnSc6d1	curyšská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
věnoval	věnovat	k5eAaImAgMnS	věnovat
experimentální	experimentální	k2eAgFnSc3d1	experimentální
fyzice	fyzika	k1gFnSc3	fyzika
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
mu	on	k3xPp3gMnSc3	on
inženýrská	inženýrský	k2eAgFnSc1d1	inženýrská
kvalifikace	kvalifikace	k1gFnSc1	kvalifikace
velmi	velmi	k6eAd1	velmi
užitečná	užitečný	k2eAgFnSc1d1	užitečná
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
měl	mít	k5eAaImAgInS	mít
znalosti	znalost	k1gFnSc3	znalost
konstruktéra	konstruktér	k1gMnSc2	konstruktér
i	i	k8xC	i
technologa	technolog	k1gMnSc2	technolog
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgInS	moct
většinu	většina	k1gFnSc4	většina
pokusů	pokus	k1gInPc2	pokus
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
<g/>
,	,	kIx,	,
také	také	k9	také
sám	sám	k3xTgMnSc1	sám
s	s	k7c7	s
běžnými	běžný	k2eAgInPc7d1	běžný
prostředky	prostředek	k1gInPc7	prostředek
realizovat	realizovat	k5eAaBmF	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
Röntgen	Röntgen	k1gInSc1	Röntgen
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgInS	oženit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1872	[number]	k4	1872
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
Berthou	Bertha	k1gFnSc7	Bertha
Ludwig	Ludwig	k1gInSc1	Ludwig
z	z	k7c2	z
Curychu	Curych	k1gInSc2	Curych
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
místního	místní	k2eAgMnSc2d1	místní
kavárníka	kavárník	k1gMnSc2	kavárník
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
sestřenicí	sestřenice	k1gFnSc7	sestřenice
básníka	básník	k1gMnSc2	básník
Otta	Otta	k1gMnSc1	Otta
Ludwiga	Ludwig	k1gMnSc2	Ludwig
<g/>
.	.	kIx.	.
</s>
<s>
Röntgen	Röntgen	k1gInSc1	Röntgen
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
neměl	mít	k5eNaImAgInS	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
adoptovali	adoptovat	k5eAaPmAgMnP	adoptovat
Josephinu	Josephina	k1gFnSc4	Josephina
Berthu	Berth	k1gInSc2	Berth
Ludwig	Ludwig	k1gInSc4	Ludwig
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
Annina	Annin	k2eAgMnSc2d1	Annin
bratra	bratr	k1gMnSc2	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc4d1	poslední
rok	rok	k1gInSc4	rok
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
přešel	přejít	k5eAaPmAgInS	přejít
do	do	k7c2	do
Mnichova	Mnichov	k1gInSc2	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
chudobě	chudoba	k1gFnSc6	chudoba
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1923	[number]	k4	1923
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
střev	střevo	k1gNnPc2	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
hrobě	hrob	k1gInSc6	hrob
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
v	v	k7c4	v
Giessenu	Giessen	k2eAgFnSc4d1	Giessen
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
památník	památník	k1gInSc1	památník
<g/>
.	.	kIx.	.
</s>
<s>
Röntgenův	Röntgenův	k2eAgInSc4d1	Röntgenův
život	život	k1gInSc4	život
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
setkání	setkání	k1gNnSc1	setkání
s	s	k7c7	s
profesorem	profesor	k1gMnSc7	profesor
fyziky	fyzika	k1gFnSc2	fyzika
Augustem	August	k1gMnSc7	August
Kundtem	Kundt	k1gMnSc7	Kundt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
asistentem	asistent	k1gMnSc7	asistent
<g/>
,	,	kIx,	,
po	po	k7c6	po
roční	roční	k2eAgFnSc6d1	roční
spolupráci	spolupráce	k1gFnSc6	spolupráce
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Würzburgu	Würzburg	k1gInSc2	Würzburg
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
)	)	kIx)	)
na	na	k7c4	na
nově	nově	k6eAd1	nově
zřízenou	zřízený	k2eAgFnSc4d1	zřízená
univerzitu	univerzita	k1gFnSc4	univerzita
ve	v	k7c6	v
Štrasburku	Štrasburk	k1gInSc6	Štrasburk
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
podal	podat	k5eAaPmAgMnS	podat
habilitační	habilitační	k2eAgFnSc4d1	habilitační
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
působit	působit	k5eAaImF	působit
jako	jako	k9	jako
soukromý	soukromý	k2eAgMnSc1d1	soukromý
docent	docent	k1gMnSc1	docent
univerzitního	univerzitní	k2eAgInSc2d1	univerzitní
ústavu	ústav	k1gInSc2	ústav
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
rok	rok	k1gInSc1	rok
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
profesora	profesor	k1gMnSc2	profesor
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
fyziky	fyzika	k1gFnSc2	fyzika
na	na	k7c6	na
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
zemědělské	zemědělský	k2eAgFnSc6d1	zemědělská
v	v	k7c6	v
Hehenheimu	Hehenheim	k1gInSc6	Hehenheim
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
mu	on	k3xPp3gMnSc3	on
tam	tam	k6eAd1	tam
ale	ale	k8xC	ale
chyběla	chybět	k5eAaImAgFnS	chybět
možnost	možnost	k1gFnSc4	možnost
pracovat	pracovat	k5eAaImF	pracovat
experimentálně	experimentálně	k6eAd1	experimentálně
<g/>
,	,	kIx,	,
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
jako	jako	k9	jako
mimořádný	mimořádný	k2eAgMnSc1d1	mimořádný
profesor	profesor	k1gMnSc1	profesor
matematické	matematický	k2eAgFnSc2d1	matematická
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
)	)	kIx)	)
už	už	k6eAd1	už
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
semestry	semestr	k1gInPc4	semestr
zase	zase	k9	zase
ke	k	k7c3	k
Kundtovi	Kundt	k1gMnSc3	Kundt
do	do	k7c2	do
Štrasburku	Štrasburk	k1gInSc2	Štrasburk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
pak	pak	k6eAd1	pak
povolán	povolat	k5eAaPmNgMnS	povolat
za	za	k7c4	za
řádného	řádný	k2eAgMnSc4d1	řádný
profesora	profesor	k1gMnSc4	profesor
a	a	k8xC	a
ředitele	ředitel	k1gMnSc4	ředitel
Fyzikálního	fyzikální	k2eAgInSc2d1	fyzikální
ústavu	ústav	k1gInSc2	ústav
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c4	v
Giessenu	Giessen	k2eAgFnSc4d1	Giessen
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
zůstal	zůstat	k5eAaPmAgInS	zůstat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgInPc2d1	další
dvanáct	dvanáct	k4xCc4	dvanáct
let	léto	k1gNnPc2	léto
působil	působit	k5eAaImAgMnS	působit
na	na	k7c6	na
würzburské	würzburský	k2eAgFnSc6d1	würzburský
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Řídil	řídit	k5eAaImAgInS	řídit
nejen	nejen	k6eAd1	nejen
její	její	k3xOp3gInSc1	její
nový	nový	k2eAgInSc1d1	nový
fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
ústav	ústav	k1gInSc1	ústav
a	a	k8xC	a
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
rektorem	rektor	k1gMnSc7	rektor
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
týkala	týkat	k5eAaImAgFnS	týkat
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
publikoval	publikovat	k5eAaBmAgMnS	publikovat
Röntgen	Röntgen	k1gInSc4	Röntgen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
práce	práce	k1gFnPc4	práce
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
piezoelektrických	piezoelektrický	k2eAgFnPc2d1	piezoelektrická
a	a	k8xC	a
pyroelektrických	pyroelektrický	k2eAgFnPc2d1	pyroelektrický
vlastností	vlastnost	k1gFnPc2	vlastnost
krystalů	krystal	k1gInPc2	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Röntgen	Röntgen	k1gInSc1	Röntgen
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
popsal	popsat	k5eAaPmAgMnS	popsat
jev	jev	k1gInSc4	jev
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
rotující	rotující	k2eAgNnSc4d1	rotující
dielektrikum	dielektrikum	k1gNnSc4	dielektrikum
v	v	k7c6	v
elektrickém	elektrický	k2eAgNnSc6d1	elektrické
poli	pole	k1gNnSc6	pole
má	mít	k5eAaImIp3nS	mít
magnetické	magnetický	k2eAgFnPc4d1	magnetická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
zjištění	zjištění	k1gNnSc1	zjištění
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
formulování	formulování	k1gNnSc3	formulování
Lorentzovy	Lorentzův	k2eAgFnSc2d1	Lorentzova
elektronové	elektronový	k2eAgFnSc2d1	elektronová
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
prací	práce	k1gFnSc7	práce
z	z	k7c2	z
giessenského	giessenský	k2eAgNnSc2d1	giessenský
období	období	k1gNnSc2	období
je	být	k5eAaImIp3nS	být
důkaz	důkaz	k1gInSc1	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
polarizovaného	polarizovaný	k2eAgNnSc2d1	polarizované
dielektrika	dielektrikum	k1gNnSc2	dielektrikum
vzniká	vznikat	k5eAaImIp3nS	vznikat
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
z	z	k7c2	z
velkých	velký	k2eAgMnPc2d1	velký
fyziků	fyzik	k1gMnPc2	fyzik
hodnotili	hodnotit	k5eAaImAgMnP	hodnotit
tuto	tento	k3xDgFnSc4	tento
práci	práce	k1gFnSc4	práce
jako	jako	k8xC	jako
objev	objev	k1gInSc4	objev
rentgenových	rentgenový	k2eAgInPc2d1	rentgenový
paprsků	paprsek	k1gInPc2	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Pohybující	pohybující	k2eAgMnSc1d1	pohybující
se	se	k3xPyFc4	se
polarizované	polarizovaný	k2eAgNnSc1d1	polarizované
dielektrikum	dielektrikum	k1gNnSc1	dielektrikum
bylo	být	k5eAaImAgNnS	být
holandským	holandský	k2eAgMnSc7d1	holandský
fyzikem	fyzik	k1gMnSc7	fyzik
Lorentzem	Lorentz	k1gMnSc7	Lorentz
nazváno	nazván	k2eAgNnSc4d1	nazváno
Röntgenův	Röntgenův	k2eAgInSc4d1	Röntgenův
proud	proud	k1gInSc4	proud
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Röntgenovo	Röntgenův	k2eAgNnSc1d1	Röntgenovo
jméno	jméno	k1gNnSc1	jméno
nesmrtelným	smrtelný	k2eNgInSc7d1	nesmrtelný
v	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
elektřiny	elektřina	k1gFnSc2	elektřina
a	a	k8xC	a
magnetismu	magnetismus	k1gInSc2	magnetismus
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Röntgenův	Röntgenův	k2eAgInSc4d1	Röntgenův
samotářský	samotářský	k2eAgInSc4d1	samotářský
způsob	způsob	k1gInSc4	způsob
práce	práce	k1gFnSc2	práce
je	být	k5eAaImIp3nS	být
příznačné	příznačný	k2eAgNnSc1d1	příznačné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc1	jeho
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
asistent	asistent	k1gMnSc1	asistent
dověděl	dovědět	k5eAaPmAgMnS	dovědět
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
objevu	objev	k1gInSc6	objev
teprve	teprve	k6eAd1	teprve
z	z	k7c2	z
odborného	odborný	k2eAgInSc2d1	odborný
tisku	tisk	k1gInSc2	tisk
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
<g/>
,	,	kIx,	,
netušil	tušit	k5eNaImAgMnS	tušit
<g/>
,	,	kIx,	,
o	o	k7c4	o
co	co	k3yInSc4	co
vlastně	vlastně	k9	vlastně
jde	jít	k5eAaImIp3nS	jít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
začal	začít	k5eAaPmAgInS	začít
Röntgen	Röntgen	k1gInSc1	Röntgen
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
würzburské	würzburský	k2eAgFnSc6d1	würzburský
laboratoři	laboratoř	k1gFnSc6	laboratoř
systematicky	systematicky	k6eAd1	systematicky
zkoumat	zkoumat	k5eAaImF	zkoumat
katodové	katodový	k2eAgInPc4d1	katodový
paprsky	paprsek	k1gInPc4	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Röntgen	Röntgen	k1gInSc1	Röntgen
byl	být	k5eAaImAgInS	být
neobyčejně	obyčejně	k6eNd1	obyčejně
důkladný	důkladný	k2eAgMnSc1d1	důkladný
experimentátor	experimentátor	k1gMnSc1	experimentátor
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgMnS	dát
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
oblasti	oblast	k1gFnSc6	oblast
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
nejdříve	dříve	k6eAd3	dříve
všechny	všechen	k3xTgInPc4	všechen
předešlé	předešlý	k2eAgInPc4d1	předešlý
pokusy	pokus	k1gInPc4	pokus
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc4	co
bylo	být	k5eAaImAgNnS	být
skutečným	skutečný	k2eAgInSc7d1	skutečný
cílem	cíl	k1gInSc7	cíl
Röntgenova	Röntgenův	k2eAgInSc2d1	Röntgenův
výzkumu	výzkum	k1gInSc2	výzkum
večer	večer	k6eAd1	večer
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1895	[number]	k4	1895
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Katodovou	katodový	k2eAgFnSc4d1	katodová
trubici	trubice	k1gFnSc4	trubice
obalil	obalit	k5eAaPmAgInS	obalit
černým	černý	k2eAgInSc7d1	černý
papírem	papír	k1gInSc7	papír
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ho	on	k3xPp3gMnSc4	on
při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
světelných	světelný	k2eAgInPc2d1	světelný
jevů	jev	k1gInPc2	jev
vyvolaných	vyvolaný	k2eAgFnPc2d1	vyvolaná
katodovými	katodový	k2eAgInPc7d1	katodový
paprsky	paprsek	k1gInPc7	paprsek
vystupujícími	vystupující	k2eAgInPc7d1	vystupující
z	z	k7c2	z
trubice	trubice	k1gFnSc2	trubice
tenkým	tenký	k2eAgNnSc7d1	tenké
hliníkovým	hliníkový	k2eAgNnSc7d1	hliníkové
okénkem	okénko	k1gNnSc7	okénko
nerušilo	rušit	k5eNaImAgNnS	rušit
světlo	světlo	k1gNnSc1	světlo
výboje	výboj	k1gInSc2	výboj
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
neprůsvitný	průsvitný	k2eNgInSc1d1	neprůsvitný
obal	obal	k1gInSc1	obal
nemohl	moct	k5eNaImAgInS	moct
žádné	žádný	k3yNgNnSc4	žádný
viditelné	viditelný	k2eAgNnSc4d1	viditelné
ani	ani	k8xC	ani
ultrafialové	ultrafialový	k2eAgNnSc4d1	ultrafialové
záření	záření	k1gNnSc4	záření
z	z	k7c2	z
výbojky	výbojka	k1gFnSc2	výbojka
propustit	propustit	k5eAaPmF	propustit
<g/>
,	,	kIx,	,
krystalky	krystalek	k1gInPc1	krystalek
platnatokyanidu	platnatokyanid	k1gInSc2	platnatokyanid
barnatého	barnatý	k2eAgInSc2d1	barnatý
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc2d1	ležící
na	na	k7c6	na
experimentátorově	experimentátorův	k2eAgInSc6d1	experimentátorův
stole	stol	k1gInSc6	stol
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
laboratoře	laboratoř	k1gFnSc2	laboratoř
bledězeleně	bledězeleně	k6eAd1	bledězeleně
rozzářily	rozzářit	k5eAaPmAgFnP	rozzářit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
na	na	k7c4	na
sklo	sklo	k1gNnSc4	sklo
výbojky	výbojka	k1gFnSc2	výbojka
dopadalo	dopadat	k5eAaImAgNnS	dopadat
katodové	katodový	k2eAgNnSc1d1	katodové
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
vznikaly	vznikat	k5eAaImAgInP	vznikat
dosud	dosud	k6eAd1	dosud
neznámé	známý	k2eNgInPc1d1	neznámý
paprsky	paprsek	k1gInPc1	paprsek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
dokázaly	dokázat	k5eAaPmAgInP	dokázat
prostupovat	prostupovat	k5eAaImF	prostupovat
neprůhlednými	průhledný	k2eNgFnPc7d1	neprůhledná
látkami	látka	k1gFnPc7	látka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prvním	první	k4xOgFnPc3	první
rentgenovým	rentgenový	k2eAgFnPc3d1	rentgenová
fotografiím	fotografia	k1gFnPc3	fotografia
patřily	patřit	k5eAaImAgInP	patřit
stínové	stínový	k2eAgInPc1d1	stínový
obrazy	obraz	k1gInPc1	obraz
ruky	ruka	k1gFnSc2	ruka
objevitelovy	objevitelův	k2eAgFnSc2d1	objevitelův
manželky	manželka	k1gFnSc2	manželka
a	a	k8xC	a
část	část	k1gFnSc1	část
hlavně	hlavně	k6eAd1	hlavně
lovecké	lovecký	k2eAgFnSc2d1	lovecká
pušky	puška	k1gFnSc2	puška
<g/>
.	.	kIx.	.
</s>
<s>
Neviditelné	viditelný	k2eNgInPc1d1	neviditelný
paprsky	paprsek	k1gInPc1	paprsek
začaly	začít	k5eAaPmAgInP	začít
tak	tak	k6eAd1	tak
sloužit	sloužit	k5eAaImF	sloužit
medicíně	medicína	k1gFnSc3	medicína
a	a	k8xC	a
technické	technický	k2eAgFnSc3d1	technická
defektoskopii	defektoskopie	k1gFnSc3	defektoskopie
už	už	k6eAd1	už
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
ještě	ještě	k6eAd1	ještě
označeny	označen	k2eAgFnPc1d1	označena
symbolem	symbol	k1gInSc7	symbol
X.	X.	kA	X.
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
při	při	k7c6	při
prvním	první	k4xOgInSc6	první
udílení	udílení	k1gNnSc6	udílení
Nobelových	Nobelových	k2eAgFnPc2d1	Nobelových
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nejvyššího	vysoký	k2eAgNnSc2d3	nejvyšší
uznání	uznání	k1gNnSc2	uznání
za	za	k7c4	za
vynález	vynález	k1gInSc4	vynález
nebo	nebo	k8xC	nebo
objev	objev	k1gInSc4	objev
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
fyziky	fyzika	k1gFnSc2	fyzika
dostalo	dostat	k5eAaPmAgNnS	dostat
právě	právě	k6eAd1	právě
objeviteli	objevitel	k1gMnPc7	objevitel
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
medailí	medaile	k1gFnSc7	medaile
a	a	k8xC	a
diplomem	diplom	k1gInSc7	diplom
mu	on	k3xPp3gInSc3	on
bylo	být	k5eAaImAgNnS	být
při	při	k7c6	při
slavnostním	slavnostní	k2eAgInSc6d1	slavnostní
ceremoniálu	ceremoniál	k1gInSc6	ceremoniál
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
předáno	předat	k5eAaPmNgNnS	předat
i	i	k9	i
150	[number]	k4	150
800	[number]	k4	800
švédských	švédský	k2eAgFnPc2d1	švédská
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Žádnou	žádný	k3yNgFnSc4	žádný
jinou	jiný	k2eAgFnSc4d1	jiná
finanční	finanční	k2eAgFnSc4d1	finanční
odměnu	odměna	k1gFnSc4	odměna
Röntgen	Röntgen	k1gInSc4	Röntgen
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
objevem	objev	k1gInSc7	objev
nepřijal	přijmout	k5eNaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dílo	dílo	k1gNnSc1	dílo
vykonané	vykonaný	k2eAgNnSc1d1	vykonané
na	na	k7c6	na
univerzitní	univerzitní	k2eAgFnSc6d1	univerzitní
půdě	půda	k1gFnSc6	půda
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
veřejných	veřejný	k2eAgInPc2d1	veřejný
prostředků	prostředek	k1gInPc2	prostředek
sloužilo	sloužit	k5eAaImAgNnS	sloužit
zdarma	zdarma	k6eAd1	zdarma
úplně	úplně	k6eAd1	úplně
všem	všecek	k3xTgMnPc3	všecek
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
nepožádal	požádat	k5eNaPmAgMnS	požádat
o	o	k7c6	o
patentování	patentování	k1gNnSc6	patentování
patentování	patentování	k1gNnSc2	patentování
objevu	objev	k1gInSc2	objev
a	a	k8xC	a
neměl	mít	k5eNaImAgMnS	mít
zájem	zájem	k1gInSc4	zájem
ani	ani	k8xC	ani
o	o	k7c4	o
nabídky	nabídka	k1gFnPc4	nabídka
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mu	on	k3xPp3gMnSc3	on
za	za	k7c4	za
spoluúčast	spoluúčast	k1gFnSc4	spoluúčast
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
lékařských	lékařský	k2eAgNnPc2d1	lékařské
diagnostických	diagnostický	k2eAgNnPc2d1	diagnostické
zařízení	zařízení	k1gNnPc2	zařízení
slibovaly	slibovat	k5eAaImAgFnP	slibovat
výhodné	výhodný	k2eAgFnPc1d1	výhodná
podmínky	podmínka	k1gFnPc1	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Röntgen	Röntgen	k1gInSc1	Röntgen
byl	být	k5eAaImAgInS	být
asketicky	asketicky	k6eAd1	asketicky
skromný	skromný	k2eAgMnSc1d1	skromný
<g/>
,	,	kIx,	,
odřekl	odřeknout	k5eAaPmAgMnS	odřeknout
dobře	dobře	k6eAd1	dobře
placenou	placený	k2eAgFnSc4d1	placená
hodnost	hodnost	k1gFnSc4	hodnost
akademika	akademik	k1gMnSc2	akademik
Berlínské	berlínský	k2eAgFnSc2d1	Berlínská
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
vedení	vedení	k1gNnSc1	vedení
tzv.	tzv.	kA	tzv.
Helmholtzovy	Helmholtzův	k2eAgFnSc2d1	Helmholtzova
katedry	katedra	k1gFnSc2	katedra
fyziky	fyzika	k1gFnSc2	fyzika
na	na	k7c6	na
berlínské	berlínský	k2eAgFnSc6d1	Berlínská
univerzitě	univerzita	k1gFnSc6	univerzita
nebo	nebo	k8xC	nebo
funkci	funkce	k1gFnSc6	funkce
prezidenta	prezident	k1gMnSc2	prezident
Fyzikálně-technického	Fyzikálněechnický	k2eAgInSc2d1	Fyzikálně-technický
říšského	říšský	k2eAgInSc2d1	říšský
ústavu	ústav	k1gInSc2	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Nepřijal	přijmout	k5eNaPmAgMnS	přijmout
dokonce	dokonce	k9	dokonce
ani	ani	k8xC	ani
Korunní	korunní	k2eAgInSc1d1	korunní
řád	řád	k1gInSc1	řád
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
princ	princ	k1gMnSc1	princ
Luitpold	Luitpold	k1gMnSc1	Luitpold
<g/>
,	,	kIx,	,
hlava	hlava	k1gFnSc1	hlava
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
<g/>
,	,	kIx,	,
povýšil	povýšit	k5eAaPmAgInS	povýšit
do	do	k7c2	do
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
má	mít	k5eAaImIp3nS	mít
seznam	seznam	k1gInSc4	seznam
medailí	medaile	k1gFnPc2	medaile
<g/>
,	,	kIx,	,
čestných	čestný	k2eAgMnPc2d1	čestný
titulů	titul	k1gInPc2	titul
a	a	k8xC	a
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
udělených	udělený	k2eAgMnPc2d1	udělený
mu	on	k3xPp3gMnSc3	on
vysokými	vysoký	k2eAgFnPc7d1	vysoká
školami	škola	k1gFnPc7	škola
a	a	k8xC	a
učenými	učený	k2eAgFnPc7d1	učená
společnostmi	společnost	k1gFnPc7	společnost
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
89	[number]	k4	89
položek	položka	k1gFnPc2	položka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
doktorát	doktorát	k1gInSc4	doktorát
Lékařské	lékařský	k2eAgFnSc2d1	lékařská
fakulty	fakulta	k1gFnSc2	fakulta
univerzity	univerzita	k1gFnSc2	univerzita
ve	v	k7c6	v
Würzburgu	Würzburg	k1gInSc6	Würzburg
<g/>
,	,	kIx,	,
čestné	čestný	k2eAgNnSc4d1	čestné
občanství	občanství	k1gNnSc4	občanství
rodného	rodný	k2eAgNnSc2d1	rodné
města	město	k1gNnSc2	město
Lennepe	Lennep	k1gInSc5	Lennep
<g/>
,	,	kIx,	,
dekret	dekret	k1gInSc1	dekret
dopisujícího	dopisující	k2eAgMnSc2d1	dopisující
člena	člen	k1gMnSc2	člen
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
akademie	akademie	k1gFnSc2	akademie
<g/>
,	,	kIx,	,
zlatá	zlatý	k2eAgFnSc1d1	zlatá
Rumfordova	Rumfordův	k2eAgFnSc1d1	Rumfordova
medaile	medaile	k1gFnSc1	medaile
od	od	k7c2	od
Royal	Royal	k1gInSc4	Royal
Society	societa	k1gFnPc1	societa
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
Bernardova	Bernardův	k2eAgFnSc1d1	Bernardova
medaile	medaile	k1gFnSc1	medaile
z	z	k7c2	z
Columbijské	Columbijský	k2eAgFnSc2d1	Columbijská
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
i	i	k9	i
první	první	k4xOgFnSc1	první
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Paprsky	paprsek	k1gInPc1	paprsek
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
ve	v	k7c6	v
dvacátém	dvacátý	k4xOgInSc6	dvacátý
století	století	k1gNnSc6	století
nástrojem	nástroj	k1gInSc7	nástroj
k	k	k7c3	k
výzkumům	výzkum	k1gInPc3	výzkum
<g/>
,	,	kIx,	,
za	za	k7c4	za
něž	jenž	k3xRgFnPc4	jenž
bylo	být	k5eAaImAgNnS	být
fyzikům	fyzik	k1gMnPc3	fyzik
<g/>
,	,	kIx,	,
chemikům	chemik	k1gMnPc3	chemik
i	i	k8xC	i
biologům	biolog	k1gMnPc3	biolog
uděleno	udělit	k5eAaPmNgNnS	udělit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
dalších	další	k2eAgFnPc2d1	další
Nobelových	Nobelových	k2eAgFnPc2d1	Nobelových
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
rentgenovému	rentgenový	k2eAgNnSc3d1	rentgenové
záření	záření	k1gNnSc3	záření
známe	znát	k5eAaImIp1nP	znát
nejen	nejen	k6eAd1	nejen
stavbu	stavba	k1gFnSc4	stavba
mnoha	mnoho	k4c2	mnoho
desítek	desítka	k1gFnPc2	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
anorganických	anorganický	k2eAgFnPc2d1	anorganická
krystalických	krystalický	k2eAgFnPc2d1	krystalická
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
slitin	slitina	k1gFnPc2	slitina
<g/>
,	,	kIx,	,
polovodičů	polovodič	k1gInPc2	polovodič
<g/>
,	,	kIx,	,
minerálů	minerál	k1gInPc2	minerál
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
např.	např.	kA	např.
i	i	k8xC	i
strukturu	struktura	k1gFnSc4	struktura
globulárních	globulární	k2eAgFnPc2d1	globulární
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
,	,	kIx,	,
nukleových	nukleový	k2eAgFnPc2d1	nukleová
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
,	,	kIx,	,
penicilinu	penicilin	k1gInSc2	penicilin
<g/>
,	,	kIx,	,
cholesterolu	cholesterol	k1gInSc2	cholesterol
<g/>
,	,	kIx,	,
vitaminu	vitamin	k1gInSc2	vitamin
B12	B12	k1gFnPc2	B12
nebo	nebo	k8xC	nebo
nukleoproteinů	nukleoprotein	k1gInPc2	nukleoprotein
<g/>
.	.	kIx.	.
</s>
