<p>
<s>
Sia	Sia	k?	Sia
Kangri	Kangri	k1gNnSc1	Kangri
(	(	kIx(	(
<g/>
také	také	k9	také
Ogre	Ogre	k1gNnSc2	Ogre
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
vysoká	vysoká	k1gFnSc1	vysoká
7	[number]	k4	7
442	[number]	k4	442
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
v	v	k7c6	v
Baltoro	Baltora	k1gFnSc5	Baltora
Muztagh	Muztagh	k1gInSc1	Muztagh
<g/>
,	,	kIx,	,
podhůří	podhůří	k1gNnSc1	podhůří
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Karákóram	Karákóram	k1gInSc4	Karákóram
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
pohraničí	pohraničí	k1gNnSc6	pohraničí
mezi	mezi	k7c7	mezi
pákistánskou	pákistánský	k2eAgFnSc7d1	pákistánská
autonomní	autonomní	k2eAgFnSc7d1	autonomní
oblastí	oblast	k1gFnSc7	oblast
Gilgit-Baltistán	Gilgit-Baltistán	k1gInSc1	Gilgit-Baltistán
<g/>
,	,	kIx,	,
indickým	indický	k2eAgInSc7d1	indický
svazovým	svazový	k2eAgInSc7d1	svazový
státem	stát	k1gInSc7	stát
Džammú	Džammú	k1gFnSc2	Džammú
a	a	k8xC	a
Kašmír	Kašmír	k1gInSc1	Kašmír
a	a	k8xC	a
Čínou	Čína	k1gFnSc7	Čína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prvovýstup	prvovýstup	k1gInSc4	prvovýstup
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
Sia	Sia	k1gFnSc2	Sia
Kangri	Kangr	k1gFnSc2	Kangr
poprvé	poprvé	k6eAd1	poprvé
vylezla	vylézt	k5eAaPmAgFnS	vylézt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
Himálajská	himálajský	k2eAgFnSc1d1	himálajská
expedice	expedice	k1gFnSc1	expedice
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vedl	vést	k5eAaImAgMnS	vést
švýcar	švýcar	k1gMnSc1	švýcar
<g/>
/	/	kIx~	/
<g/>
němec	němec	k1gMnSc1	němec
Günther	Günthra	k1gFnPc2	Günthra
Dyhrenfurth	Dyhrenfurth	k1gMnSc1	Dyhrenfurth
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
stanula	stanout	k5eAaPmAgFnS	stanout
také	také	k9	také
Hettie	Hettie	k1gFnSc1	Hettie
Dyhrenfurthová	Dyhrenfurthová	k1gFnSc1	Dyhrenfurthová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tak	tak	k6eAd1	tak
stanovila	stanovit	k5eAaPmAgFnS	stanovit
rekord	rekord	k1gInSc4	rekord
ženské	ženský	k2eAgFnSc2d1	ženská
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
trval	trvat	k5eAaImAgInS	trvat
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Sia	Sia	k1gFnSc2	Sia
Kangri	Kangr	k1gFnSc2	Kangr
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
