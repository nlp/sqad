<p>
<s>
Rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
je	být	k5eAaImIp3nS	být
kružnice	kružnice	k1gFnSc1	kružnice
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
koule	koule	k1gFnSc2	koule
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
zeměpisnou	zeměpisný	k2eAgFnSc7d1	zeměpisná
šířkou	šířka	k1gFnSc7	šířka
φ	φ	k?	φ
</s>
<s>
Je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
rovinou	rovina	k1gFnSc7	rovina
procházející	procházející	k2eAgInSc1d1	procházející
zvoleným	zvolený	k2eAgInSc7d1	zvolený
bodem	bod	k1gInSc7	bod
rovnoběžně	rovnoběžně	k6eAd1	rovnoběžně
s	s	k7c7	s
rovinou	rovina	k1gFnSc7	rovina
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Rovnoběžky	rovnoběžka	k1gFnPc1	rovnoběžka
se	se	k3xPyFc4	se
zkracují	zkracovat	k5eAaImIp3nP	zkracovat
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
(	(	kIx(	(
<g/>
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
<g/>
)	)	kIx)	)
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
pólům	pól	k1gInPc3	pól
(	(	kIx(	(
<g/>
bod	bod	k1gInSc4	bod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
60	[number]	k4	60
<g/>
.	.	kIx.	.
rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
je	být	k5eAaImIp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
délky	délka	k1gFnSc2	délka
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výpočet	výpočet	k1gInSc4	výpočet
délky	délka	k1gFnSc2	délka
rovnoběžky	rovnoběžka	k1gFnSc2	rovnoběžka
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
vzorce	vzorec	k1gInPc4	vzorec
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
φ	φ	k?	φ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
o	o	k7c4	o
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc4	cdot
cos	cos	kA	cos
<g/>
\	\	kIx~	\
<g/>
phi	phi	k?	phi
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
o	o	k7c4	o
je	být	k5eAaImIp3nS	být
obvod	obvod	k1gInSc1	obvod
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
40	[number]	k4	40
074	[number]	k4	074
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Φ	Φ	k?	Φ
je	být	k5eAaImIp3nS	být
zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
šířka	šířka	k1gFnSc1	šířka
pro	pro	k7c4	pro
kterou	který	k3yIgFnSc4	který
délku	délka	k1gFnSc4	délka
rovnoběžky	rovnoběžka	k1gFnSc2	rovnoběžka
počítáme	počítat	k5eAaImIp1nP	počítat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prahou	Praha	k1gFnSc7	Praha
prochází	procházet	k5eAaImIp3nS	procházet
padesátá	padesátý	k4xOgFnSc1	padesátý
rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
25	[number]	k4	25
760	[number]	k4	760
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významné	významný	k2eAgFnSc2d1	významná
rovnoběžky	rovnoběžka	k1gFnSc2	rovnoběžka
==	==	k?	==
</s>
</p>
<p>
<s>
Rovnoběžky	rovnoběžka	k1gFnPc1	rovnoběžka
se	se	k3xPyFc4	se
standardně	standardně	k6eAd1	standardně
označují	označovat	k5eAaImIp3nP	označovat
podle	podle	k7c2	podle
své	svůj	k3xOyFgFnSc2	svůj
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
šířky	šířka	k1gFnSc2	šířka
jako	jako	k8xC	jako
např.	např.	kA	např.
10	[number]	k4	10
<g/>
.	.	kIx.	.
rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
s.	s.	k?	s.
š.	š.	k?	š.
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
některé	některý	k3yIgFnPc1	některý
význačné	význačný	k2eAgFnPc1d1	význačná
mají	mít	k5eAaImIp3nP	mít
svá	svůj	k3xOyFgNnPc4	svůj
vlastní	vlastní	k2eAgNnPc4d1	vlastní
jména	jméno	k1gNnPc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
např.	např.	kA	např.
</s>
</p>
<p>
<s>
severní	severní	k2eAgInSc1d1	severní
pól	pól	k1gInSc1	pól
(	(	kIx(	(
<g/>
90	[number]	k4	90
<g/>
°	°	k?	°
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
severní	severní	k2eAgInSc1d1	severní
polární	polární	k2eAgInSc1d1	polární
kruh	kruh	k1gInSc1	kruh
(	(	kIx(	(
<g/>
rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
66	[number]	k4	66
<g/>
°	°	k?	°
<g/>
33	[number]	k4	33
<g/>
'	'	kIx"	'
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
obratník	obratník	k1gInSc1	obratník
Raka	rak	k1gMnSc2	rak
(	(	kIx(	(
<g/>
rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
23	[number]	k4	23
<g/>
°	°	k?	°
<g/>
27	[number]	k4	27
<g/>
'	'	kIx"	'
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
rovník	rovník	k1gInSc1	rovník
(	(	kIx(	(
<g/>
rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
0	[number]	k4	0
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
obratník	obratník	k1gInSc1	obratník
Kozoroha	Kozoroh	k1gMnSc2	Kozoroh
(	(	kIx(	(
<g/>
rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
23	[number]	k4	23
<g/>
°	°	k?	°
<g/>
27	[number]	k4	27
<g/>
'	'	kIx"	'
jižní	jižní	k2eAgFnSc2d1	jižní
šířky	šířka	k1gFnSc2	šířka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
jižní	jižní	k2eAgInSc1d1	jižní
polární	polární	k2eAgInSc1d1	polární
kruh	kruh	k1gInSc1	kruh
(	(	kIx(	(
<g/>
rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
66	[number]	k4	66
<g/>
°	°	k?	°
<g/>
33	[number]	k4	33
<g/>
'	'	kIx"	'
jižní	jižní	k2eAgFnSc2d1	jižní
šířky	šířka	k1gFnSc2	šířka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
jižní	jižní	k2eAgInSc1d1	jižní
pól	pól	k1gInSc1	pól
(	(	kIx(	(
<g/>
90	[number]	k4	90
<g/>
°	°	k?	°
jižní	jižní	k2eAgFnSc2d1	jižní
šířky	šířka	k1gFnSc2	šířka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
šířka	šířka	k1gFnSc1	šířka
a	a	k8xC	a
západy	západ	k1gInPc1	západ
Slunce	slunce	k1gNnSc2	slunce
==	==	k?	==
</s>
</p>
<p>
<s>
Pouze	pouze	k6eAd1	pouze
mezi	mezi	k7c7	mezi
obratníky	obratník	k1gInPc7	obratník
se	se	k3xPyFc4	se
Slunce	slunce	k1gNnSc1	slunce
během	během	k7c2	během
roku	rok	k1gInSc2	rok
alespoň	alespoň	k9	alespoň
jednou	jednou	k6eAd1	jednou
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
zenitu	zenit	k1gInSc2	zenit
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
severně	severně	k6eAd1	severně
od	od	k7c2	od
severního	severní	k2eAgInSc2d1	severní
polárního	polární	k2eAgInSc2d1	polární
kruhu	kruh	k1gInSc2	kruh
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
jižního	jižní	k2eAgInSc2d1	jižní
polárního	polární	k2eAgInSc2d1	polární
kruhu	kruh	k1gInSc2	kruh
<g/>
,	,	kIx,	,
Slunce	slunce	k1gNnSc2	slunce
alespoň	alespoň	k9	alespoň
jednou	jeden	k4xCgFnSc7	jeden
během	během	k7c2	během
roku	rok	k1gInSc2	rok
nezapadá	zapadat	k5eNaPmIp3nS	zapadat
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
polární	polární	k2eAgInSc1d1	polární
den	den	k1gInSc4	den
a	a	k8xC	a
polární	polární	k2eAgFnSc4d1	polární
noc	noc	k1gFnSc4	noc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Poledník	poledník	k1gMnSc1	poledník
</s>
</p>
<p>
<s>
Degree	Degree	k1gFnSc1	Degree
Confluence	Confluence	k1gFnSc2	Confluence
Project	Projecta	k1gFnPc2	Projecta
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
