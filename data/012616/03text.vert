<p>
<s>
Dolný	Dolný	k2eAgInSc1d1	Dolný
Harmanec	Harmanec	k1gInSc1	Harmanec
je	být	k5eAaImIp3nS	být
obec	obec	k1gFnSc1	obec
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	s	k7c7	s
12	[number]	k4	12
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Banské	banský	k2eAgFnSc2d1	Banská
Bystrice	Bystrica	k1gFnSc2	Bystrica
<g/>
,	,	kIx,	,
v	v	k7c6	v
úzké	úzký	k2eAgFnSc6d1	úzká
Harmanecké	Harmanecký	k2eAgFnSc6d1	Harmanecká
dolině	dolina	k1gFnSc6	dolina
<g/>
,	,	kIx,	,
na	na	k7c6	na
styku	styk	k1gInSc6	styk
Velké	velký	k2eAgFnSc2d1	velká
Fatry	Fatra	k1gFnSc2	Fatra
s	s	k7c7	s
Kremnickými	kremnický	k2eAgInPc7d1	kremnický
vrchy	vrch	k1gInPc7	vrch
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1540	[number]	k4	1540
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
hornická	hornický	k2eAgFnSc1d1	hornická
osada	osada	k1gFnSc1	osada
patřící	patřící	k2eAgFnSc1d1	patřící
Banské	banský	k2eAgFnSc3d1	Banská
Bystrici	Bystrica	k1gFnSc3	Bystrica
<g/>
.	.	kIx.	.
</s>
<s>
Blízko	blízko	k7c2	blízko
vesnice	vesnice	k1gFnSc2	vesnice
leží	ležet	k5eAaImIp3nS	ležet
Harmanecká	Harmanecký	k2eAgFnSc1d1	Harmanecká
jeskyně	jeskyně	k1gFnSc1	jeskyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ves	ves	k1gFnSc1	ves
je	být	k5eAaImIp3nS	být
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
sever	sever	k1gInSc4	sever
–	–	k?	–
jih	jih	k1gInSc1	jih
<g/>
,	,	kIx,	,
podél	podél	k7c2	podél
Bystrického	bystrický	k2eAgInSc2d1	bystrický
potoka	potok	k1gInSc2	potok
a	a	k8xC	a
státní	státní	k2eAgFnSc2d1	státní
silnice	silnice	k1gFnSc2	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Střed	střed	k1gInSc1	střed
obce	obec	k1gFnSc2	obec
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
486	[number]	k4	486
m	m	kA	m
<g/>
,	,	kIx,	,
katastr	katastr	k1gInSc4	katastr
obce	obec	k1gFnSc2	obec
ve	v	k7c6	v
výškovém	výškový	k2eAgNnSc6d1	výškové
rozmezí	rozmezí	k1gNnSc6	rozmezí
457	[number]	k4	457
–	–	k?	–
1377	[number]	k4	1377
m	m	kA	m
(	(	kIx(	(
<g/>
Králova	Králův	k2eAgFnSc1d1	Králova
studna	studna	k1gFnSc1	studna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
rozloha	rozloha	k1gFnSc1	rozloha
katastru	katastr	k1gInSc2	katastr
je	být	k5eAaImIp3nS	být
4452	[number]	k4	4452
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
žije	žít	k5eAaImIp3nS	žít
249	[number]	k4	249
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k6eAd1	uprostřed
stojí	stát	k5eAaImIp3nS	stát
římskokatolický	římskokatolický	k2eAgInSc1d1	římskokatolický
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Josefa	Josef	k1gMnSc2	Josef
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
obec	obec	k1gFnSc1	obec
patřila	patřit	k5eAaImAgFnS	patřit
do	do	k7c2	do
Zvolenské	zvolenský	k2eAgFnSc2d1	Zvolenská
župy	župa	k1gFnSc2	župa
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
do	do	k7c2	do
okresu	okres	k1gInSc2	okres
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
banskobystrického	banskobystrický	k2eAgInSc2d1	banskobystrický
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
obce	obec	k1gFnSc2	obec
==	==	k?	==
</s>
</p>
<p>
<s>
Obec	obec	k1gFnSc1	obec
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
asi	asi	k9	asi
v	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
písemně	písemně	k6eAd1	písemně
doložena	doložit	k5eAaPmNgFnS	doložit
r.	r.	kA	r.
1540	[number]	k4	1540
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
osada	osada	k1gFnSc1	osada
horníků	horník	k1gMnPc2	horník
<g/>
,	,	kIx,	,
hutníků	hutník	k1gMnPc2	hutník
a	a	k8xC	a
dřevorubců	dřevorubec	k1gMnPc2	dřevorubec
na	na	k7c6	na
území	území	k1gNnSc6	území
svobodného	svobodný	k2eAgNnSc2d1	svobodné
královského	královský	k2eAgNnSc2d1	královské
města	město	k1gNnSc2	město
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pojmenování	pojmenování	k1gNnSc4	pojmenování
obce	obec	k1gFnPc1	obec
==	==	k?	==
</s>
</p>
<p>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
obce	obec	k1gFnSc2	obec
Harmanec	Harmanec	k1gInSc1	Harmanec
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
jejího	její	k3xOp3gMnSc2	její
zakladatele	zakladatel	k1gMnSc2	zakladatel
Hermanna	Hermann	k1gMnSc2	Hermann
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
saské	saský	k2eAgFnSc2d1	saská
hornické	hornický	k2eAgFnSc2d1	hornická
kolonizace	kolonizace	k1gFnSc2	kolonizace
Banské	banský	k2eAgFnSc2d1	Banská
Bystrice	Bystrica	k1gFnSc2	Bystrica
a	a	k8xC	a
jejího	její	k3xOp3gNnSc2	její
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yRnSc4	což
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
i	i	k8xC	i
starý	starý	k2eAgInSc1d1	starý
německý	německý	k2eAgInSc1d1	německý
název	název	k1gInSc1	název
Hermannsdorf	Hermannsdorf	k1gInSc1	Hermannsdorf
<g/>
,	,	kIx,	,
maďarský	maďarský	k2eAgInSc1d1	maďarský
Hermand	Hermand	k1gInSc1	Hermand
nebo	nebo	k8xC	nebo
počeštěný	počeštěný	k2eAgInSc1d1	počeštěný
Hermanecz	Hermanecz	k1gInSc1	Hermanecz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
obce	obec	k1gFnSc2	obec
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1496	[number]	k4	1496
měli	mít	k5eAaImAgMnP	mít
v	v	k7c6	v
Harmanci	Harmanec	k1gInSc6	Harmanec
banskobystričtí	banskobystrický	k2eAgMnPc1d1	banskobystrický
těžaři	těžař	k1gMnPc1	těžař
šmelcovnu	šmelcovna	k1gFnSc4	šmelcovna
<g/>
.	.	kIx.	.
</s>
<s>
Novou	nový	k2eAgFnSc4d1	nová
huť	huť	k1gFnSc4	huť
(	(	kIx(	(
<g/>
8	[number]	k4	8
pecí	pec	k1gFnPc2	pec
<g/>
)	)	kIx)	)
postavila	postavit	k5eAaPmAgFnS	postavit
Thurzovsko-fuggerovská	Thurzovskouggerovský	k2eAgFnSc1d1	Thurzovsko-fuggerovský
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zpracovávala	zpracovávat	k5eAaImAgFnS	zpracovávat
měděnou	měděný	k2eAgFnSc4d1	měděná
rudu	ruda	k1gFnSc4	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
převzetí	převzetí	k1gNnSc6	převzetí
měďařských	měďařský	k2eAgInPc2d1	měďařský
podniků	podnik	k1gInPc2	podnik
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Banské	banský	k2eAgFnSc2d1	Banská
Bystrice	Bystrica	k1gFnSc2	Bystrica
hornickou	hornický	k2eAgFnSc7d1	hornická
komorou	komora	k1gFnSc7	komora
(	(	kIx(	(
<g/>
r.	r.	kA	r.
1546	[number]	k4	1546
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
odvrácení	odvrácení	k1gNnSc4	odvrácení
hrozícího	hrozící	k2eAgInSc2d1	hrozící
nedostatku	nedostatek	k1gInSc2	nedostatek
dřeva	dřevo	k1gNnSc2	dřevo
pro	pro	k7c4	pro
erární	erární	k2eAgInPc4d1	erární
doly	dol	k1gInPc4	dol
a	a	k8xC	a
hutě	huť	k1gFnSc2	huť
rezervoval	rezervovat	k5eAaBmAgMnS	rezervovat
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
II	II	kA	II
<g/>
.	.	kIx.	.
báňským	báňský	k2eAgInSc7d1	báňský
řádem	řád	k1gInSc7	řád
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1573	[number]	k4	1573
i	i	k9	i
lesy	les	k1gInPc1	les
báňských	báňský	k2eAgNnPc2d1	báňské
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Harmaneckých	Harmanecký	k2eAgInPc2d1	Harmanecký
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
potřebu	potřeba	k1gFnSc4	potřeba
dolů	dol	k1gInPc2	dol
<g/>
.	.	kIx.	.
</s>
<s>
Dřevo	dřevo	k1gNnSc1	dřevo
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
získat	získat	k5eAaPmF	získat
jen	jen	k9	jen
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
královské	královský	k2eAgFnSc2d1	královská
hornické	hornický	k2eAgFnSc2d1	hornická
komory	komora	k1gFnSc2	komora
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
soustředila	soustředit	k5eAaPmAgFnS	soustředit
svůj	svůj	k3xOyFgInSc4	svůj
zájem	zájem	k1gInSc4	zájem
především	především	k9	především
na	na	k7c4	na
oblast	oblast	k1gFnSc4	oblast
Španiej	Španiej	k1gInSc4	Španiej
Doliny	dolina	k1gFnSc2	dolina
a	a	k8xC	a
Starých	Starých	k2eAgFnPc2d1	Starých
Hor	hora	k1gFnPc2	hora
s	s	k7c7	s
bohatými	bohatý	k2eAgNnPc7d1	bohaté
ložisky	ložisko	k1gNnPc7	ložisko
měděné	měděný	k2eAgFnSc2d1	měděná
rudy	ruda	k1gFnSc2	ruda
a	a	k8xC	a
proto	proto	k8xC	proto
usměrňovala	usměrňovat	k5eAaImAgFnS	usměrňovat
požadavky	požadavek	k1gInPc4	požadavek
měšťanů	měšťan	k1gMnPc2	měšťan
na	na	k7c4	na
příděly	příděl	k1gInPc4	příděl
dřeva	dřevo	k1gNnSc2	dřevo
právě	právě	k6eAd1	právě
do	do	k7c2	do
Harmanecké	Harmanecký	k2eAgFnSc2d1	Harmanecká
<g/>
,	,	kIx,	,
na	na	k7c4	na
rudu	ruda	k1gFnSc4	ruda
chudé	chudý	k2eAgFnSc2d1	chudá
doliny	dolina	k1gFnSc2	dolina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1546	[number]	k4	1546
<g/>
–	–	k?	–
<g/>
1564	[number]	k4	1564
se	se	k3xPyFc4	se
Harmanecká	Harmanecký	k2eAgFnSc1d1	Harmanecká
huť	huť	k1gFnSc1	huť
stala	stát	k5eAaPmAgFnS	stát
erárním	erární	k2eAgInSc7d1	erární
podnikem	podnik	k1gInSc7	podnik
a	a	k8xC	a
pak	pak	k6eAd1	pak
ji	on	k3xPp3gFnSc4	on
opět	opět	k6eAd1	opět
převzali	převzít	k5eAaPmAgMnP	převzít
soukromí	soukromý	k2eAgMnPc1d1	soukromý
podnikatelé	podnikatel	k1gMnPc1	podnikatel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zde	zde	k6eAd1	zde
zpracovávali	zpracovávat	k5eAaImAgMnP	zpracovávat
rudu	ruda	k1gFnSc4	ruda
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
dolů	dol	k1gInPc2	dol
i	i	k8xC	i
materiál	materiál	k1gInSc4	materiál
ze	z	k7c2	z
starých	starý	k2eAgFnPc2d1	stará
hald	halda	k1gFnPc2	halda
až	až	k9	až
do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pak	pak	k6eAd1	pak
huť	huť	k1gFnSc4	huť
využívalo	využívat	k5eAaPmAgNnS	využívat
město	město	k1gNnSc4	město
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
na	na	k7c6	na
zpracovávání	zpracovávání	k1gNnSc6	zpracovávání
rudy	ruda	k1gFnSc2	ruda
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
dolů	dol	k1gInPc2	dol
v	v	k7c6	v
Richtárové	Richtárový	k2eAgFnSc6d1	Richtárová
a	a	k8xC	a
v	v	k7c6	v
Pieskách	Pieska	k1gFnPc6	Pieska
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
Harmanecké	Harmanecký	k2eAgFnPc1d1	Harmanecká
osady	osada	k1gFnPc1	osada
do	do	k7c2	do
vlivu	vliv	k1gInSc2	vliv
města	město	k1gNnSc2	město
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
někdejší	někdejší	k2eAgInSc4d1	někdejší
výskyt	výskyt	k1gInSc4	výskyt
zlatonosných	zlatonosný	k2eAgFnPc2d1	zlatonosná
žil	žíla	k1gFnPc2	žíla
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
zlato	zlato	k1gNnSc1	zlato
se	se	k3xPyFc4	se
těžilo	těžit	k5eAaImAgNnS	těžit
již	již	k9	již
v	v	k7c6	v
dávných	dávný	k2eAgFnPc6d1	dávná
dobách	doba	k1gFnPc6	doba
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
pokusy	pokus	k1gInPc1	pokus
těžbu	těžba	k1gFnSc4	těžba
obnovit	obnovit	k5eAaPmF	obnovit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
obci	obec	k1gFnSc3	obec
Dolný	Dolný	k2eAgInSc4d1	Dolný
Harmanec	Harmanec	k1gInSc4	Harmanec
připojena	připojit	k5eAaPmNgFnS	připojit
původně	původně	k6eAd1	původně
samostatná	samostatný	k2eAgFnSc1d1	samostatná
osada	osada	k1gFnSc1	osada
Horný	horný	k1gMnSc1	horný
Harmanec	Harmanec	k1gMnSc1	Harmanec
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
dozajista	dozajista	k6eAd1	dozajista
založili	založit	k5eAaPmAgMnP	založit
uhlíři	uhlíř	k1gMnPc1	uhlíř
<g/>
,	,	kIx,	,
pálící	pálící	k2eAgNnSc4d1	pálící
dřevěné	dřevěný	k2eAgNnSc4d1	dřevěné
uhlí	uhlí	k1gNnSc4	uhlí
v	v	k7c6	v
okolních	okolní	k2eAgInPc6d1	okolní
lesích	les	k1gInPc6	les
pro	pro	k7c4	pro
vysokou	vysoký	k2eAgFnSc4d1	vysoká
pec	pec	k1gFnSc4	pec
v	v	k7c4	v
Dolnom	Dolnom	k1gInSc4	Dolnom
Harmanci	Harmance	k1gFnSc4	Harmance
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
katastru	katastr	k1gInSc2	katastr
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1829	[number]	k4	1829
postavil	postavit	k5eAaPmAgMnS	postavit
banskobystrický	banskobystrický	k2eAgMnSc1d1	banskobystrický
obchodník	obchodník	k1gMnSc1	obchodník
F.	F.	kA	F.
S.	S.	kA	S.
Leicht	Leicht	k1gMnSc1	Leicht
papírnu	papírna	k1gFnSc4	papírna
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1842	[number]	k4	1842
zmodernizoval	zmodernizovat	k5eAaPmAgMnS	zmodernizovat
vídeňský	vídeňský	k2eAgMnSc1d1	vídeňský
podnikatel	podnikatel	k1gMnSc1	podnikatel
M.	M.	kA	M.
V.	V.	kA	V.
Schloss	Schlossa	k1gFnPc2	Schlossa
a	a	k8xC	a
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
největší	veliký	k2eAgFnSc7d3	veliký
strojovou	strojový	k2eAgFnSc7d1	strojová
papírnou	papírna	k1gFnSc7	papírna
v	v	k7c6	v
Uhersku	Uhersko	k1gNnSc6	Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
byla	být	k5eAaImAgFnS	být
téměř	téměř	k6eAd1	téměř
monopolním	monopolní	k2eAgMnSc7d1	monopolní
odběratelem	odběratel	k1gMnSc7	odběratel
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
vlákniny	vláknina	k1gFnSc2	vláknina
z	z	k7c2	z
městských	městský	k2eAgInPc2d1	městský
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
měla	mít	k5eAaImAgFnS	mít
papírna	papírna	k1gFnSc1	papírna
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
železniční	železniční	k2eAgFnSc4d1	železniční
vlečku	vlečka	k1gFnSc4	vlečka
z	z	k7c2	z
Banské	banský	k2eAgFnSc2d1	Banská
Bystrice	Bystrica	k1gFnSc2	Bystrica
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
a	a	k8xC	a
papírnu	papírna	k1gFnSc4	papírna
připojili	připojit	k5eAaPmAgMnP	připojit
na	na	k7c4	na
novou	nový	k2eAgFnSc4d1	nová
železniční	železniční	k2eAgFnSc4d1	železniční
trať	trať	k1gFnSc4	trať
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
papírny	papírna	k1gFnSc2	papírna
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
osada	osada	k1gFnSc1	osada
Harmanec	Harmanec	k1gMnSc1	Harmanec
-	-	kIx~	-
papírna	papírna	k1gFnSc1	papírna
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
oddělili	oddělit	k5eAaPmAgMnP	oddělit
od	od	k7c2	od
Dolního	dolní	k2eAgInSc2d1	dolní
Harmance	Harmanka	k1gFnSc6	Harmanka
jako	jako	k9	jako
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
politickou	politický	k2eAgFnSc4d1	politická
obec	obec	k1gFnSc4	obec
s	s	k7c7	s
názvem	název	k1gInSc7	název
Harmanec	Harmanec	k1gMnSc1	Harmanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
Harmanec	Harmanec	k1gInSc4	Harmanec
vedla	vést	k5eAaImAgFnS	vést
historická	historický	k2eAgFnSc1d1	historická
stezka	stezka	k1gFnSc1	stezka
do	do	k7c2	do
Turce	Turce	k1gFnSc2	Turce
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
později	pozdě	k6eAd2	pozdě
nahradila	nahradit	k5eAaPmAgFnS	nahradit
státní	státní	k2eAgFnSc1d1	státní
silnice	silnice	k1gFnSc1	silnice
klikatící	klikatící	k2eAgFnSc1d1	klikatící
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
horské	horský	k2eAgNnSc4d1	horské
sedlo	sedlo	k1gNnSc4	sedlo
Malý	Malý	k1gMnSc1	Malý
Šturec	Šturec	k1gMnSc1	Šturec
<g/>
,	,	kIx,	,
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Dolný	Dolný	k1gMnSc1	Dolný
Harmanec	Harmanec	k1gMnSc1	Harmanec
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
