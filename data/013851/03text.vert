<s>
Free	Free	k6eAd1
running	running	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Freerunner	Freerunner	k1gInSc1
v	v	k7c6
Brazílii	Brazílie	k1gFnSc6
</s>
<s>
Free	Free	k1gInSc1
running	running	k1gInSc1
(	(	kIx(
<g/>
angl.	angl.	k?
"	"	kIx"
<g/>
volný	volný	k2eAgInSc1d1
běh	běh	k1gInSc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
umění	umění	k1gNnSc1
pohybu	pohyb	k1gInSc2
<g/>
,	,	kIx,
akrobatická	akrobatický	k2eAgFnSc1d1
sportovní	sportovní	k2eAgFnSc1d1
disciplína	disciplína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Freerunnerům	Freerunner	k1gMnPc3
jde	jít	k5eAaImIp3nS
o	o	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
cestě	cesta	k1gFnSc6
hladce	hladko	k6eAd1
a	a	k8xC
plynule	plynule	k6eAd1
překonali	překonat	k5eAaPmAgMnP
překážku	překážka	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
pomocí	pomocí	k7c2
různých	různý	k2eAgInPc2d1
pohybů	pohyb	k1gInPc2
jako	jako	k8xC,k8xS
vaulty	vault	k1gInPc1
(	(	kIx(
<g/>
přeskoky	přeskok	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
skoky	skok	k1gInPc1
<g/>
,	,	kIx,
„	„	k?
<g/>
somersaults	somersaults	k1gInSc1
<g/>
“	“	k?
a	a	k8xC
jiné	jiný	k2eAgInPc1d1
akrobatické	akrobatický	k2eAgInPc1d1
pohyby	pohyb	k1gInPc1
<g/>
,	,	kIx,
vytvářející	vytvářející	k2eAgFnSc1d1
atleticky	atleticky	k6eAd1
a	a	k8xC
esteticky	esteticky	k6eAd1
vypadající	vypadající	k2eAgInSc4d1
způsob	způsob	k1gInSc4
pohybu	pohyb	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
parkouru	parkour	k1gInSc2
jsou	být	k5eAaImIp3nP
povoleny	povolen	k2eAgInPc1d1
akrobatické	akrobatický	k2eAgInPc1d1
pohyby	pohyb	k1gInPc1
a	a	k8xC
skoky	skok	k1gInPc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
freerunning	freerunning	k1gInSc1
je	být	k5eAaImIp3nS
více	hodně	k6eAd2
zaměřen	zaměřit	k5eAaPmNgInS
na	na	k7c4
estetiku	estetika	k1gFnSc4
a	a	k8xC
krásu	krása	k1gFnSc4
pohybu	pohyb	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tréninky	trénink	k1gInPc7
probíhají	probíhat	k5eAaImIp3nP
většinou	většinou	k6eAd1
v	v	k7c6
tělocvičnách	tělocvična	k1gFnPc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
v	v	k7c6
městských	městský	k2eAgInPc6d1
prostorech	prostor	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
plné	plný	k2eAgInPc1d1
různých	různý	k2eAgFnPc2d1
překážek	překážka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7
zakladatelem	zakladatel	k1gMnSc7
je	být	k5eAaImIp3nS
Francouz	Francouz	k1gMnSc1
Sébastien	Sébastien	k1gMnSc1
Foucan	Foucan	k1gMnSc1xF
<g/>
,	,	kIx,
který	který	k3yRgMnSc1
se	se	k3xPyFc4
také	také	k6eAd1
věnouval	věnouvat	k5eAaImAgMnS,k5eAaPmAgMnS
parkouru	parkour	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
free	free	k1gFnSc6
runningu	running	k1gInSc2
říká	říkat	k5eAaImIp3nS
<g/>
:	:	kIx,
„	„	k?
<g/>
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jiná	jiný	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
‚	‚	k?
<g/>
dělání	dělání	k1gNnSc1
<g/>
‘	‘	k?
parkouru	parkour	k1gInSc2
</s>
<s>
<g/>
“	“	k?
</s>
<s>
Byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
a	a	k8xC
inspirován	inspirován	k2eAgInSc1d1
podobným	podobný	k2eAgNnSc7d1
uměním	umění	k1gNnSc7
pohybu	pohyb	k1gInSc2
(	(	kIx(
<g/>
parkour	parkour	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
založil	založit	k5eAaPmAgMnS
David	David	k1gMnSc1
Belle	bell	k1gInSc5
<g/>
,	,	kIx,
Foucanův	Foucanův	k2eAgMnSc1d1
přítel	přítel	k1gMnSc1
z	z	k7c2
dětství	dětství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Free	Fre	k1gInSc2
running	running	k1gInSc1
spojuje	spojovat	k5eAaImIp3nS
prvky	prvek	k1gInPc4
triků	trik	k1gInPc2
a	a	k8xC
pouličních	pouliční	k2eAgInPc2d1
skoků	skok	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
parkour	parkour	k1gMnSc1
komunitou	komunita	k1gFnSc7
považovány	považován	k2eAgInPc1d1
za	za	k7c4
neefektní	efektní	k2eNgNnSc4d1
a	a	k8xC
není	být	k5eNaImIp3nS
to	ten	k3xDgNnSc1
parkour	parkour	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
byl	být	k5eAaImAgInS
free	free	k6eAd1
running	running	k1gInSc1
zaměňován	zaměňován	k2eAgInSc1d1
s	s	k7c7
parkourem	parkour	k1gInSc7
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
jakmile	jakmile	k8xS
se	se	k3xPyFc4
free	free	k6eAd1
runneři	runner	k1gMnPc1
začali	začít	k5eAaPmAgMnP
zajímat	zajímat	k5eAaImF
o	o	k7c4
estetiku	estetika	k1gFnSc4
i	i	k9
o	o	k7c4
užitečnost	užitečnost	k1gFnSc4
<g/>
,	,	kIx,
staly	stát	k5eAaPmAgInP
se	se	k3xPyFc4
z	z	k7c2
těchto	tento	k3xDgInPc2
dvou	dva	k4xCgInPc2
stylů	styl	k1gInPc2
rozdílné	rozdílný	k2eAgFnSc2d1
disciplíny	disciplína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pojem	pojem	k1gInSc1
free	free	k6eAd1
running	running	k1gInSc4
vymyslel	vymyslet	k5eAaPmAgMnS
Guillaume	Guillaum	k1gInSc5
Pelletier	Pelletier	k1gMnSc1
a	a	k8xC
Foucan	Foucan	k1gMnSc1
ho	on	k3xPp3gInSc4
přijal	přijmout	k5eAaPmAgMnS
jako	jako	k8xC,k8xS
svůj	svůj	k3xOyFgInSc4
způsob	způsob	k1gInSc4
jak	jak	k6eAd1
dělat	dělat	k5eAaImF
parkour	parkoura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Foucan	Foucan	k1gMnSc1
popisuje	popisovat	k5eAaImIp3nS
cíle	cíl	k1gInPc4
freeruningu	freeruning	k1gInSc2
jako	jako	k8xS,k8xC
používání	používání	k1gNnSc2
okolí	okolí	k1gNnSc2
k	k	k7c3
plynulému	plynulý	k2eAgNnSc3d1
a	a	k8xC
nepřerušovanému	přerušovaný	k2eNgInSc3d1
pohybu	pohyb	k1gInSc3
vpřed	vpřed	k6eAd1
<g/>
,	,	kIx,
nikdy	nikdy	k6eAd1
vzad	vzad	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Zatímco	zatímco	k8xS
freeruning	freeruning	k1gInSc1
a	a	k8xC
parkour	parkoura	k1gFnPc2
mají	mít	k5eAaImIp3nP
mnoho	mnoho	k4c4
stejných	stejný	k2eAgFnPc2d1
technik	technika	k1gFnPc2
<g/>
,	,	kIx,
podstatně	podstatně	k6eAd1
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
ve	v	k7c6
filozofii	filozofie	k1gFnSc6
a	a	k8xC
účelu	účel	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cíle	cíl	k1gInSc2
parkouru	parkour	k1gInSc2
jsou	být	k5eAaImIp3nP
širší	široký	k2eAgFnSc4d2
<g/>
,	,	kIx,
schopnost	schopnost	k1gFnSc4
rychle	rychle	k6eAd1
dosáhnout	dosáhnout	k5eAaPmF
oblastí	oblast	k1gFnSc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
jinak	jinak	k6eAd1
nedostupné	dostupný	k2eNgFnPc4d1
a	a	k8xC
utéct	utéct	k5eAaPmF
<g/>
,	,	kIx,
schopnost	schopnost	k1gFnSc4
uniknout	uniknout	k5eAaPmF
pronásledovatelům	pronásledovatel	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgNnSc2
vyplývá	vyplývat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
hlavním	hlavní	k2eAgInSc7d1
úkolem	úkol	k1gInSc7
je	být	k5eAaImIp3nS
opustit	opustit	k5eAaPmF
objekty	objekt	k1gInPc4
co	co	k9
nejdříve	dříve	k6eAd3
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
freerunning	freerunning	k1gInSc1
klade	klást	k5eAaImIp3nS
důraz	důraz	k1gInSc4
na	na	k7c4
vlastní	vlastní	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
stylem	styl	k1gInSc7
„	„	k?
<g/>
následuj	následovat	k5eAaImRp2nS
svoji	svůj	k3xOyFgFnSc4
cestu	cesta	k1gFnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Foucan	Foucan	k1gInSc1
často	často	k6eAd1
zmiňuje	zmiňovat	k5eAaImIp3nS
„	„	k?
<g/>
následování	následování	k1gNnSc4
vlastní	vlastní	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
<g/>
“	“	k?
v	v	k7c6
rozhovorech	rozhovor	k1gInPc6
a	a	k8xC
dokumentárních	dokumentární	k2eAgInPc6d1
filmech	film	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysvětluje	vysvětlovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
každý	každý	k3xTgMnSc1
má	mít	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
způsob	způsob	k1gInSc4
jak	jak	k6eAd1
dělat	dělat	k5eAaImF
parkour	parkour	k1gMnSc1
a	a	k8xC
neměl	mít	k5eNaImAgMnS
by	by	kYmCp3nP
napodobovat	napodobovat	k5eAaImF
ostatní	ostatní	k2eAgMnPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
dělat	dělat	k5eAaImF
ho	on	k3xPp3gMnSc4
podle	podle	k7c2
sebe	sebe	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základy	základ	k1gInPc1
freerunningu	freerunning	k1gInSc2
jsou	být	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
špatně	špatně	k6eAd1
vysvětlovány	vysvětlován	k2eAgFnPc1d1
jako	jako	k8xS,k8xC
by	by	kYmCp3nP
byly	být	k5eAaImAgInP
postaveny	postavit	k5eAaPmNgInP
výhradně	výhradně	k6eAd1
na	na	k7c6
estetice	estetika	k1gFnSc6
a	a	k8xC
kráse	krása	k1gFnSc6
daného	daný	k2eAgInSc2d1
vaultu	vault	k1gInSc2
<g/>
,	,	kIx,
skoku	skok	k1gInSc2
atd.	atd.	kA
Přestože	přestože	k8xS
hodně	hodně	k6eAd1
free	free	k6eAd1
runnerů	runner	k1gMnPc2
si	se	k3xPyFc3
dává	dávat	k5eAaImIp3nS
záležet	záležet	k5eAaImF
právě	právě	k9
na	na	k7c6
estetice	estetika	k1gFnSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
„	„	k?
<g/>
jejich	jejich	k3xOp3gInSc4
způsob	způsob	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c7
parkourovou	parkourův	k2eAgFnSc7d1
a	a	k8xC
free	freat	k5eAaPmIp3nS
running	running	k1gInSc1
komunitou	komunita	k1gFnSc7
vznikl	vzniknout	k5eAaPmAgInS
také	také	k9
konflikt	konflikt	k1gInSc1
kvůli	kvůli	k7c3
používání	používání	k1gNnSc3
různých	různý	k2eAgInPc2d1
termínů	termín	k1gInPc2
pro	pro	k7c4
stejné	stejný	k2eAgInPc4d1
vaulty	vault	k1gInPc4
(	(	kIx(
<g/>
přeskoky	přeskok	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parkour	Parkoura	k1gFnPc2
komunita	komunita	k1gFnSc1
používala	používat	k5eAaImAgFnS
pro	pro	k7c4
vaulty	vault	k1gInPc4
jejich	jejich	k3xOp3gFnSc2
francouzské	francouzský	k2eAgFnSc2d1
názvy	název	k1gInPc7
<g/>
,	,	kIx,
nebo	nebo	k8xC
je	on	k3xPp3gNnPc4
přeložili	přeložit	k5eAaPmAgMnP
do	do	k7c2
angličtiny	angličtina	k1gFnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
free	freat	k5eAaPmIp3nS
runing	runing	k1gInSc4
komunita	komunita	k1gFnSc1
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
webem	web	k1gInSc7
Urban	Urban	k1gMnSc1
Freeflow	Freeflow	k1gMnSc1
vytvořila	vytvořit	k5eAaPmAgFnS
nové	nový	k2eAgInPc4d1
názvy	název	k1gInPc4
jako	jako	k8xC,k8xS
„	„	k?
<g/>
kong	kong	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
monkey	monkea	k1gFnSc2
<g/>
“	“	k?
atp.	atp.	kA
Například	například	k6eAd1
přeskok	přeskok	k1gInSc4
při	při	k7c6
kterém	který	k3yRgNnSc6,k3yIgNnSc6,k3yQgNnSc6
se	se	k3xPyFc4
prostrčí	prostrčit	k5eAaPmIp3nP
<g />
.	.	kIx.
</s>
<s hack="1">
nohy	noha	k1gFnPc4
mezi	mezi	k7c7
rukama	ruka	k1gFnPc7
je	být	k5eAaImIp3nS
ve	v	k7c6
francouzštině	francouzština	k1gFnSc6
znám	znát	k5eAaImIp1nS
jako	jako	k9
„	„	k?
<g/>
Saut	Saut	k1gInSc1
de	de	k?
Chat	chata	k1gFnPc2
<g/>
“	“	k?
a	a	k8xC
v	v	k7c6
anglickém	anglický	k2eAgInSc6d1
překladu	překlad	k1gInSc6
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
„	„	k?
<g/>
Cat	Cat	k1gMnSc1
jump	jump	k1gMnSc1
<g/>
“	“	k?
a	a	k8xC
někteří	některý	k3yIgMnPc1
ho	on	k3xPp3gNnSc4
nazývají	nazývat	k5eAaImIp3nP
„	„	k?
<g/>
rabbit	rabbit	k5eAaPmF,k5eAaImF
vault	vault	k1gInSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
Ale	ale	k8xC
web	web	k1gInSc1
Urban	Urban	k1gMnSc1
Freeflow	Freeflow	k1gMnSc1
web	web	k1gInSc4
ho	on	k3xPp3gInSc4
přejmenoval	přejmenovat	k5eAaPmAgInS
na	na	k7c4
„	„	k?
<g/>
Kong	Kongo	k1gNnPc2
<g/>
“	“	k?
nebo	nebo	k8xC
„	„	k?
<g/>
Monkey	Monkea	k1gFnSc2
<g/>
“	“	k?
vault	vault	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1
otázka	otázka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
může	moct	k5eAaImIp3nS
způsobit	způsobit	k5eAaPmF
rozepři	rozepře	k1gFnSc4
mezi	mezi	k7c7
parkourovou	parkourův	k2eAgFnSc7d1
a	a	k8xC
free	free	k1gFnSc1
running	running	k1gInSc4
komunitou	komunita	k1gFnSc7
-	-	kIx~
anebo	anebo	k8xC
může	moct	k5eAaImIp3nS
posílit	posílit	k5eAaPmF
jejich	jejich	k3xOp3gNnSc4
pouto	pouto	k1gNnSc4
-	-	kIx~
je	být	k5eAaImIp3nS
profesionální	profesionální	k2eAgFnSc1d1
a	a	k8xC
amatérská	amatérský	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
od	od	k7c2
začátku	začátek	k1gInSc2
je	být	k5eAaImIp3nS
parkourová	parkourový	k2eAgFnSc1d1
komunita	komunita	k1gFnSc1
proti	proti	k7c3
seriozním	seriozní	k2eAgFnPc3d1
soutěžím	soutěž	k1gFnPc3
<g/>
,	,	kIx,
odporuje	odporovat	k5eAaImIp3nS
to	ten	k3xDgNnSc1
základům	základ	k1gInPc3
filozofie	filozofie	k1gFnSc2
parkouru	parkour	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Free	Fre	k1gInSc2
running	running	k1gInSc1
komunita	komunita	k1gFnSc1
není	být	k5eNaImIp3nS
tak	tak	k6eAd1
pevně	pevně	k6eAd1
rozhodnutá	rozhodnutý	k2eAgFnSc1d1
v	v	k7c6
této	tento	k3xDgFnSc6
otázce	otázka	k1gFnSc6
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
Foucan	Foucan	k1gInSc1
v	v	k7c6
interview	interview	k1gNnSc6
s	s	k7c7
Urban	Urban	k1gMnSc1
Freeflow	Freeflow	k1gMnSc1
zmiňuje	zmiňovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
nemá	mít	k5eNaImIp3nS
rád	rád	k6eAd1
soutěže	soutěž	k1gFnSc2
a	a	k8xC
není	být	k5eNaImIp3nS
to	ten	k3xDgNnSc1
„	„	k?
<g/>
jeho	jeho	k3xOp3gFnSc2
cesta	cesta	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
ale	ale	k8xC
může	moct	k5eAaImIp3nS
to	ten	k3xDgNnSc1
být	být	k5eAaImF
„	„	k?
<g/>
cesta	cesta	k1gFnSc1
někoho	někdo	k3yInSc2
jiného	jiný	k2eAgNnSc2d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konflikt	konflikt	k1gInSc1
mezi	mezi	k7c4
free	free	k1gFnSc4
runningem	running	k1gInSc7
a	a	k8xC
parkourem	parkour	k1gInSc7
vznikl	vzniknout	k5eAaPmAgInS
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
zakladatelé	zakladatel	k1gMnPc1
parkouru	parkoura	k1gFnSc4
<g/>
,	,	kIx,
David	David	k1gMnSc1
Belle	bell	k1gInSc5
a	a	k8xC
Sébastien	Sébastien	k2eAgInSc4d1
Foucan	Foucan	k1gInSc4
<g/>
,	,	kIx,
rozdělili	rozdělit	k5eAaPmAgMnP
a	a	k8xC
pokračovali	pokračovat	k5eAaImAgMnP
odděleně	odděleně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
David	David	k1gMnSc1
Belle	bell	k1gInSc5
především	především	k9
ulpěl	ulpět	k5eAaPmAgInS
na	na	k7c6
parkouru	parkour	k1gInSc6
kvůli	kvůli	k7c3
efektivitě	efektivita	k1gFnSc3
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Sebastien	Sebastien	k2eAgInSc1d1
Foucan	Foucan	k1gInSc1
se	se	k3xPyFc4
soustředil	soustředit	k5eAaPmAgInS
na	na	k7c4
volnost	volnost	k1gFnSc4
pohybu	pohyb	k1gInSc2
<g/>
,	,	kIx,
rozvoj	rozvoj	k1gInSc1
sebe	sebe	k3xPyFc4
sama	sám	k3xTgFnSc1
a	a	k8xC
estetickou	estetický	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
parkouru	parkour	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
dělá	dělat	k5eAaImIp3nS
free	free	k1gFnSc3
running	running	k1gInSc4
populárnější	populární	k2eAgInSc4d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
oba	dva	k4xCgInPc1
sporty	sport	k1gInPc1
definovány	definován	k2eAgInPc1d1
odlišně	odlišně	k6eAd1
<g/>
,	,	kIx,
sdílejí	sdílet	k5eAaImIp3nP
podobné	podobný	k2eAgInPc4d1
rysy	rys	k1gInPc4
<g/>
,	,	kIx,
protože	protože	k8xS
byly	být	k5eAaImAgFnP
založeny	založit	k5eAaPmNgFnP
dvěma	dva	k4xCgInPc7
kamarády	kamarád	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Freerunning	Freerunning	k1gInSc1
ve	v	k7c6
filmu	film	k1gInSc6
</s>
<s>
Ve	v	k7c6
filmu	film	k1gInSc6
Casino	Casin	k2eAgNnSc1d1
Royale	Royala	k1gFnSc6
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
předvádí	předvádět	k5eAaImIp3nS
Sébastien	Sébastien	k2eAgMnSc1d1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
uniknout	uniknout	k5eAaPmF
Bondovi	Bonda	k1gMnSc3
skrz	skrz	k6eAd1
složitou	složitý	k2eAgFnSc4d1
stavební	stavební	k2eAgFnSc4d1
konstrukci	konstrukce	k1gFnSc4
<g/>
:	:	kIx,
</s>
<s>
V	v	k7c6
dokumentu	dokument	k1gInSc6
Jump	Jump	k1gMnSc1
London	London	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
účinkují	účinkovat	k5eAaImIp3nP
francouzští	francouzský	k2eAgMnPc1d1
freerunneři	freerunner	k1gMnPc1
(	(	kIx(
<g/>
Sébastien	Sébastien	k2eAgMnSc1d1
Foucan	Foucan	k1gMnSc1
<g/>
,	,	kIx,
Johann	Johann	k1gMnSc1
Vigroux	Vigroux	k1gInSc1
<g/>
,	,	kIx,
Jérôme	Jérôm	k1gInSc5
Ben	Ben	k1gInSc4
Aoues	Aoues	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Jump	Jump	k1gMnSc1
Britain	Britain	k1gMnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pokračování	pokračování	k1gNnSc4
dokumentu	dokument	k1gInSc2
Jump	Jump	k1gMnSc1
London	London	k1gMnSc1
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
i	i	k9
s	s	k7c7
týmem	tým	k1gInSc7
Urban	Urban	k1gMnSc1
Freeflow	Freeflow	k1gMnSc1
</s>
<s>
Videoklip	videoklip	k1gInSc1
Jump	Jump	k1gInSc1
od	od	k7c2
Madonny	Madonna	k1gFnSc2
uvádí	uvádět	k5eAaImIp3nS
freerunning	freerunning	k1gInSc1
a	a	k8xC
parkour	parkour	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
ArtMoving	ArtMoving	k1gInSc1
-	-	kIx~
Asociace	asociace	k1gFnSc1
<g/>
,	,	kIx,
akademie	akademie	k1gFnSc1
a	a	k8xC
agentura	agentura	k1gFnSc1
složená	složený	k2eAgFnSc1d1
z	z	k7c2
profesionálních	profesionální	k2eAgMnPc2d1
českých	český	k2eAgMnPc2d1
atletů	atlet	k1gMnPc2
</s>
<s>
První	první	k4xOgNnSc1
české	český	k2eAgNnSc1d1
fórum	fórum	k1gNnSc1
o	o	k7c6
freerunningu	freerunning	k1gInSc6
</s>
<s>
Sans	Sansit	k5eAaPmRp2nS
Encombre	Encombr	k1gMnSc5
-	-	kIx~
profesionální	profesionální	k2eAgFnSc1d1
freerunová	freerunový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
z	z	k7c2
Východních	východní	k2eAgFnPc2d1
Čech	Čechy	k1gFnPc2
</s>
<s>
In	In	k?
Motion	Motion	k1gInSc1
-	-	kIx~
profesionální	profesionální	k2eAgInSc1d1
team	team	k1gInSc1
z	z	k7c2
Prahy	Praha	k1gFnSc2
</s>
<s>
Urban-Chasers	Urban-Chasers	k1gInSc1
-	-	kIx~
freerunový	freerunový	k2eAgInSc1d1
team	team	k1gInSc1
z	z	k7c2
Brna	Brno	k1gNnSc2
</s>
<s>
Team	team	k1gInSc1
Fatrion	Fatrion	k1gInSc1
-	-	kIx~
3	#num#	k4
<g/>
run	run	k1gInSc1
&	&	k?
PK	PK	kA
team	team	k1gInSc1
YouTube	YouTub	k1gInSc5
</s>
<s>
Hranice	hranice	k1gFnSc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
</s>
<s>
Salto	salto	k1gNnSc1
Mortale	Mortal	k1gMnSc5
-	-	kIx~
web	web	k1gInSc4
o	o	k7c4
free	free	k1gNnSc4
runningu	running	k1gInSc2
<g/>
,	,	kIx,
street	street	k5eAaImF,k5eAaPmF,k5eAaBmF
tricích	trik	k1gInPc6
a	a	k8xC
akrobacii	akrobacie	k1gFnSc6
</s>
<s>
Jim	on	k3xPp3gMnPc3
Dohnal	Dohnal	k1gMnSc1
-	-	kIx~
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
parkour	parkour	k1gMnSc1
a	a	k8xC
freerunning	freerunning	k1gInSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Free	Free	k1gInSc1
running	running	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
