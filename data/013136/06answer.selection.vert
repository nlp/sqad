<s>
Válečka	válečka	k1gFnSc1	válečka
prapořitá	prapořitat	k5eAaPmIp3nS	prapořitat
(	(	kIx(	(
<g/>
Brachypodium	Brachypodium	k1gNnSc1	Brachypodium
pinnatum	pinnatum	k1gNnSc1	pinnatum
<g/>
;	;	kIx,	;
synonyma	synonymum	k1gNnPc4	synonymum
-	-	kIx~	-
Festuca	Festuca	k1gFnSc1	Festuca
pinnata	pinnata	k1gFnSc1	pinnata
(	(	kIx(	(
<g/>
L.	L.	kA	L.
<g/>
)	)	kIx)	)
Huds	Huds	k1gInSc1	Huds
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Triticum	Triticum	k1gNnSc1	Triticum
pinnatum	pinnatum	k1gNnSc1	pinnatum
(	(	kIx(	(
<g/>
L.	L.	kA	L.
<g/>
)	)	kIx)	)
Lam	lama	k1gFnPc2	lama
<g/>
.	.	kIx.	.
et	et	k?	et
DC	DC	kA	DC
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tráva	tráva	k1gFnSc1	tráva
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
lipnicovitých	lipnicovitý	k2eAgFnPc2d1	lipnicovitý
(	(	kIx(	(
<g/>
Poaceae	Poaceae	k1gFnPc2	Poaceae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
