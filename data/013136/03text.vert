<p>
<s>
Válečka	válečka	k1gFnSc1	válečka
prapořitá	prapořitat	k5eAaPmIp3nS	prapořitat
(	(	kIx(	(
<g/>
Brachypodium	Brachypodium	k1gNnSc1	Brachypodium
pinnatum	pinnatum	k1gNnSc1	pinnatum
<g/>
;	;	kIx,	;
synonyma	synonymum	k1gNnPc4	synonymum
-	-	kIx~	-
Festuca	Festuca	k1gFnSc1	Festuca
pinnata	pinnata	k1gFnSc1	pinnata
(	(	kIx(	(
<g/>
L.	L.	kA	L.
<g/>
)	)	kIx)	)
Huds	Huds	k1gInSc1	Huds
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Triticum	Triticum	k1gNnSc1	Triticum
pinnatum	pinnatum	k1gNnSc1	pinnatum
(	(	kIx(	(
<g/>
L.	L.	kA	L.
<g/>
)	)	kIx)	)
Lam	lama	k1gFnPc2	lama
<g/>
.	.	kIx.	.
et	et	k?	et
DC	DC	kA	DC
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tráva	tráva	k1gFnSc1	tráva
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
lipnicovitých	lipnicovitý	k2eAgFnPc2d1	lipnicovitý
(	(	kIx(	(
<g/>
Poaceae	Poaceae	k1gFnPc2	Poaceae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Válečka	válečka	k1gFnSc1	válečka
prapořitá	prapořitý	k2eAgFnSc1d1	prapořitá
je	být	k5eAaImIp3nS	být
tráva	tráva	k1gFnSc1	tráva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
netvoří	tvořit	k5eNaImIp3nS	tvořit
trsy	trs	k1gInPc4	trs
<g/>
.	.	kIx.	.
</s>
<s>
Dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
výšky	výška	k1gFnPc4	výška
až	až	k9	až
1	[number]	k4	1
m.	m.	k?	m.
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
<g/>
,	,	kIx,	,
tuhé	tuhý	k2eAgInPc1d1	tuhý
<g/>
,	,	kIx,	,
hojně	hojně	k6eAd1	hojně
jsou	být	k5eAaImIp3nP	být
chlupaté	chlupatý	k2eAgFnPc1d1	chlupatá
<g/>
;	;	kIx,	;
kolínka	kolínko	k1gNnPc1	kolínko
jsou	být	k5eAaImIp3nP	být
taktéž	taktéž	k?	taktéž
mírně	mírně	k6eAd1	mírně
chlupatá	chlupatý	k2eAgFnSc1d1	chlupatá
<g/>
.	.	kIx.	.
</s>
<s>
Květenství	květenství	k1gNnSc1	květenství
je	být	k5eAaImIp3nS	být
vzpřímený	vzpřímený	k2eAgInSc4d1	vzpřímený
lichoklas	lichoklas	k1gInSc4	lichoklas
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
6-12	[number]	k4	6-12
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
válcovitých	válcovitý	k2eAgInPc2d1	válcovitý
klásků	klásek	k1gInPc2	klásek
<g/>
.	.	kIx.	.
</s>
<s>
Vzpřímené	vzpřímený	k2eAgInPc1d1	vzpřímený
klásky	klásek	k1gInPc1	klásek
v	v	k7c6	v
době	doba	k1gFnSc6	doba
květu	květ	k1gInSc2	květ
odstávají	odstávat	k5eAaImIp3nP	odstávat
<g/>
,	,	kIx,	,
osiny	osina	k1gFnPc1	osina
jsou	být	k5eAaImIp3nP	být
krátké	krátký	k2eAgFnPc1d1	krátká
<g/>
.	.	kIx.	.
</s>
<s>
Válečka	válečka	k1gFnSc1	válečka
prapořitá	prapořitat	k5eAaPmIp3nS	prapořitat
kvete	kvést	k5eAaImIp3nS	kvést
od	od	k7c2	od
června	červen	k1gInSc2	červen
do	do	k7c2	do
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stanoviště	stanoviště	k1gNnSc1	stanoviště
<g/>
,	,	kIx,	,
rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Rostlina	rostlina	k1gFnSc1	rostlina
je	být	k5eAaImIp3nS	být
hojná	hojný	k2eAgFnSc1d1	hojná
na	na	k7c6	na
slunných	slunný	k2eAgFnPc6d1	slunná
mezích	mez	k1gFnPc6	mez
a	a	k8xC	a
stráních	stráň	k1gFnPc6	stráň
<g/>
,	,	kIx,	,
v	v	k7c6	v
lesních	lesní	k2eAgFnPc6d1	lesní
světlinách	světlina	k1gFnPc6	světlina
od	od	k7c2	od
nížin	nížina	k1gFnPc2	nížina
do	do	k7c2	do
podhůří	podhůří	k1gNnSc2	podhůří
<g/>
.	.	kIx.	.
</s>
<s>
Válečka	válečka	k1gFnSc1	válečka
prapořitá	prapořitý	k2eAgFnSc1d1	prapořitá
preferuje	preferovat	k5eAaImIp3nS	preferovat
humózní	humózní	k2eAgFnSc1d1	humózní
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
i	i	k9	i
vápenité	vápenitý	k2eAgFnSc2d1	vápenitá
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Těžiště	těžiště	k1gNnSc1	těžiště
výskytu	výskyt	k1gInSc2	výskyt
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
čeledi	čeleď	k1gFnSc2	čeleď
lipnicovitých	lipnicovitý	k2eAgMnPc2d1	lipnicovitý
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
střední	střední	k2eAgFnSc1d1	střední
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc1d1	východní
a	a	k8xC	a
severní	severní	k2eAgFnSc1d1	severní
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druh	druh	k1gInSc1	druh
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
,	,	kIx,	,
druhotně	druhotně	k6eAd1	druhotně
byl	být	k5eAaImAgInS	být
zavlečen	zavleknout	k5eAaPmNgInS	zavleknout
na	na	k7c4	na
západní	západní	k2eAgNnSc4d1	západní
pobřeží	pobřeží	k1gNnSc4	pobřeží
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
válečka	válečka	k1gFnSc1	válečka
prapořitá	prapořitat	k5eAaPmIp3nS	prapořitat
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
