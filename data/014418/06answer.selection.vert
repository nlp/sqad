<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
pro	pro	k7c4
normalizaci	normalizace	k1gFnSc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
International	International	k1gFnSc1
Organization	Organization	k1gInSc1
for	forum	k1gNnPc2
Standardization	Standardization	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
označovaná	označovaný	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
ISO	ISO	kA
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
světovou	světový	k2eAgFnSc7d1
federací	federace	k1gFnSc7
národních	národní	k2eAgFnPc2d1
normalizačních	normalizační	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Ženevě	Ženeva	k1gFnSc6
<g/>
.	.	kIx.
</s>