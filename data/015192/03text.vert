<s>
NCQ	NCQ	kA
</s>
<s>
NCQ	NCQ	kA
umožňuje	umožňovat	k5eAaImIp3nS
zařízení	zařízení	k1gNnPc4
aby	aby	kYmCp3nS
si	se	k3xPyFc3
samo	sám	k3xTgNnSc4
určilo	určit	k5eAaPmAgNnS
optimální	optimální	k2eAgNnSc1d1
pořadí	pořadí	k1gNnSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
bude	být	k5eAaImBp3nS
přistupovat	přistupovat	k5eAaImF
k	k	k7c3
požadovaným	požadovaný	k2eAgFnPc3d1
informacím	informace	k1gFnPc3
na	na	k7c6
médiu	médium	k1gNnSc6
(	(	kIx(
<g/>
např.	např.	kA
harddisku	harddisk	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
umožní	umožnit	k5eAaPmIp3nS
přístup	přístup	k1gInSc1
k	k	k7c3
datům	datum	k1gNnPc3
během	během	k7c2
menšího	malý	k2eAgInSc2d2
počtu	počet	k1gInSc2
otáček	otáčka	k1gFnPc2
diskové	diskový	k2eAgFnSc2d1
plotny	plotna	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
sníží	snížit	k5eAaPmIp3nS
i	i	k9
časová	časový	k2eAgFnSc1d1
náročnost	náročnost	k1gFnSc1
operace	operace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
NCQ	NCQ	kA
je	být	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
z	z	k7c2
anglického	anglický	k2eAgInSc2d1
Native	Natiev	k1gFnSc2
Command	Commanda	k1gFnPc2
Queuing	Queuing	k1gInSc1
<g/>
,	,	kIx,
česky	česky	k6eAd1
přirozené	přirozený	k2eAgNnSc1d1
řazení	řazení	k1gNnSc1
příkazů	příkaz	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Princip	princip	k1gInSc1
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
technologii	technologie	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
umožňuje	umožňovat	k5eAaImIp3nS
zvýšit	zvýšit	k5eAaPmF
výkon	výkon	k1gInSc4
pevných	pevný	k2eAgInPc2d1
disků	disk	k1gInPc2
s	s	k7c7
rozhraním	rozhraní	k1gNnSc7
SATA	SATA	kA
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
použití	použití	k1gNnSc6
NCQ	NCQ	kA
pevný	pevný	k2eAgInSc1d1
disk	disk	k1gInSc1
sám	sám	k3xTgInSc1
optimalizuje	optimalizovat	k5eAaBmIp3nS
pořadí	pořadí	k1gNnSc4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
jsou	být	k5eAaImIp3nP
vykonány	vykonán	k2eAgInPc4d1
požadavky	požadavek	k1gInPc4
na	na	k7c4
zápis	zápis	k1gInSc4
nebo	nebo	k8xC
čtení	čtení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
optimalizace	optimalizace	k1gFnSc1
může	moct	k5eAaImIp3nS
redukovat	redukovat	k5eAaBmF
nadbytečný	nadbytečný	k2eAgInSc1d1
pohyb	pohyb	k1gInSc1
hlaviček	hlavička	k1gFnPc2
disku	disk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
zvýší	zvýšit	k5eAaPmIp3nS
rychlost	rychlost	k1gFnSc1
přenosu	přenos	k1gInSc2
dat	datum	k1gNnPc2
mezi	mezi	k7c7
řadičem	řadič	k1gInSc7
a	a	k8xC
diskem	disk	k1gInSc7
a	a	k8xC
také	také	k9
se	se	k3xPyFc4
mírně	mírně	k6eAd1
sníží	snížit	k5eAaPmIp3nS
opotřebení	opotřebení	k1gNnSc4
disku	disk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Optimalizace	optimalizace	k1gFnSc1
se	se	k3xPyFc4
dobře	dobře	k6eAd1
uplatní	uplatnit	k5eAaPmIp3nS
<g/>
,	,	kIx,
pokud	pokud	k8xS
se	se	k3xPyFc4
k	k	k7c3
disku	disco	k1gNnSc3
snaží	snažit	k5eAaImIp3nS
přistoupit	přistoupit	k5eAaPmF
současně	současně	k6eAd1
několik	několik	k4yIc4
procesů	proces	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současný	současný	k2eAgInSc1d1
požadavek	požadavek	k1gInSc1
na	na	k7c4
přístup	přístup	k1gInSc4
k	k	k7c3
disku	disco	k1gNnSc3
nastává	nastávat	k5eAaImIp3nS
často	často	k6eAd1
u	u	k7c2
serverů	server	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
i	i	k9
v	v	k7c6
domácím	domácí	k2eAgNnSc6d1
použití	použití	k1gNnSc6
<g/>
,	,	kIx,
například	například	k6eAd1
při	při	k7c6
práci	práce	k1gFnSc6
se	s	k7c7
spuštěnou	spuštěný	k2eAgFnSc7d1
antivirovou	antivirový	k2eAgFnSc7d1
kontrolou	kontrola	k1gFnSc7
disku	disk	k1gInSc2
anebo	anebo	k8xC
současném	současný	k2eAgNnSc6d1
kopírování	kopírování	k1gNnSc6
více	hodně	k6eAd2
souborů	soubor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Instalace	instalace	k1gFnSc1
v	v	k7c6
operačním	operační	k2eAgInSc6d1
systému	systém	k1gInSc6
</s>
<s>
Ke	k	k7c3
správné	správný	k2eAgFnSc3d1
funkci	funkce	k1gFnSc3
NCQ	NCQ	kA
je	být	k5eAaImIp3nS
zapotřebí	zapotřebí	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ji	on	k3xPp3gFnSc4
podporoval	podporovat	k5eAaImAgMnS
jak	jak	k8xC,k8xS
disk	disk	k1gInSc4
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
řadič	řadič	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
v	v	k7c6
operačním	operační	k2eAgInSc6d1
systému	systém	k1gInSc6
nainstalován	nainstalován	k2eAgInSc1d1
příslušný	příslušný	k2eAgInSc1d1
ovladač	ovladač	k1gInSc1
řadiče	řadič	k1gMnSc2
s	s	k7c7
NCQ	NCQ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
nových	nový	k2eAgMnPc2d1
řadičů	řadič	k1gMnPc2
dokáže	dokázat	k5eAaPmIp3nS
pracovat	pracovat	k5eAaImF
pod	pod	k7c7
rozhraním	rozhraní	k1gNnSc7
AHCI	AHCI	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Advanced	Advanced	k1gMnSc1
Host	host	k1gMnSc1
Controller	Controller	k1gMnSc1
Interface	interface	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
umožňuje	umožňovat	k5eAaImIp3nS
jednotné	jednotný	k2eAgNnSc4d1
ovládání	ovládání	k1gNnSc4
všech	všecek	k3xTgInPc2
SATA	SATA	kA
NCQ	NCQ	kA
řadičů	řadič	k1gInPc2
jedním	jeden	k4xCgInSc7
obecným	obecný	k2eAgInSc7d1
ovladačem	ovladač	k1gInSc7
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novější	nový	k2eAgFnSc1d2
Linuxová	linuxový	k2eAgFnSc1d1
jádra	jádro	k1gNnPc1
tento	tento	k3xDgInSc4
ovladač	ovladač	k1gInSc4
obsahují	obsahovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Windows	Windows	kA
XP	XP	kA
vyžadují	vyžadovat	k5eAaImIp3nP
instalaci	instalace	k1gFnSc4
speciálních	speciální	k2eAgInPc2d1
ovladačů	ovladač	k1gInPc2
i	i	k9
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
daný	daný	k2eAgInSc1d1
řadič	řadič	k1gInSc1
rozhraní	rozhraní	k1gNnSc2
AHCI	AHCI	kA
podporuje	podporovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Windows	Windows	kA
Vista	vista	k2eAgInSc4d1
už	už	k6eAd1
obecný	obecný	k2eAgInSc4d1
ovladač	ovladač	k1gInSc4
pro	pro	k7c4
řadiče	řadič	k1gInPc4
podporující	podporující	k2eAgFnSc2d1
AHCI	AHCI	kA
obsahují	obsahovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgFnSc1d1
technologie	technologie	k1gFnSc1
</s>
<s>
Starší	starý	k2eAgFnSc1d2
verze	verze	k1gFnSc1
rozhraní	rozhraní	k1gNnSc2
ATA	ATA	kA
(	(	kIx(
<g/>
PATA	pata	k1gFnSc1
nebo	nebo	k8xC
SATA	SATA	kA
I	i	k9
<g/>
)	)	kIx)
mohla	moct	k5eAaImAgFnS
obsahovat	obsahovat	k5eAaImF
určitou	určitý	k2eAgFnSc4d1
optimalizaci	optimalizace	k1gFnSc4
řazení	řazení	k1gNnSc2
požadavků	požadavek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
ATA	ATA	kA
TCQ	TCQ	kA
neboli	neboli	k8xC
ATA	ATA	kA
Tagged	Tagged	k1gInSc1
Command	Command	k1gInSc1
Queuing	Queuing	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
řešení	řešení	k1gNnSc1
se	se	k3xPyFc4
příliš	příliš	k6eAd1
nerozšířilo	rozšířit	k5eNaPmAgNnS
kvůli	kvůli	k7c3
jeho	jeho	k3xOp3gInSc3
technické	technický	k2eAgFnPc4d1
komplikovanosti	komplikovanost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
TCQ	TCQ	kA
bylo	být	k5eAaImAgNnS
navrženo	navržen	k2eAgNnSc1d1
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
fungovalo	fungovat	k5eAaImAgNnS
s	s	k7c7
libovolným	libovolný	k2eAgInSc7d1
ATA	ATA	kA
řadičem	řadič	k1gMnSc7
<g/>
,	,	kIx,
a	a	k8xC
zpracování	zpracování	k1gNnSc6
tagů	tag	k1gInPc2
řešil	řešit	k5eAaImAgInS
softwarový	softwarový	k2eAgInSc1d1
ovladač	ovladač	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
ke	k	k7c3
zpracování	zpracování	k1gNnSc3
každého	každý	k3xTgInSc2
příkazu	příkaz	k1gInSc2
byly	být	k5eAaImAgInP
potřeba	potřeba	k6eAd1
dvě	dva	k4xCgNnPc4
přerušení	přerušení	k1gNnPc4
<g/>
,	,	kIx,
jedno	jeden	k4xCgNnSc1
na	na	k7c6
nastavení	nastavení	k1gNnSc6
tabulky	tabulka	k1gFnSc2
adres	adresa	k1gFnPc2
pro	pro	k7c4
DMA	dmout	k5eAaImSgInS
přenos	přenos	k1gInSc1
a	a	k8xC
druhé	druhý	k4xOgNnSc4
po	po	k7c6
dokončení	dokončení	k1gNnSc6
přenosu	přenos	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvě	dva	k4xCgNnPc1
přerušení	přerušení	k1gNnPc1
(	(	kIx(
<g/>
oproti	oproti	k7c3
jednomu	jeden	k4xCgMnSc3
<g/>
,	,	kIx,
bez	bez	k7c2
TCQ	TCQ	kA
<g/>
)	)	kIx)
zatěžovala	zatěžovat	k5eAaImAgFnS
procesor	procesor	k1gInSc4
a	a	k8xC
v	v	k7c6
důsledku	důsledek	k1gInSc6
to	ten	k3xDgNnSc1
nevedlo	vést	k5eNaImAgNnS
k	k	k7c3
velkému	velký	k2eAgNnSc3d1
zlepšení	zlepšení	k1gNnSc3
výkonu	výkon	k1gInSc2
disku	disk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Technologie	technologie	k1gFnSc1
TCQ	TCQ	kA
byla	být	k5eAaImAgFnS
původně	původně	k6eAd1
vytvořena	vytvořit	k5eAaPmNgFnS
pro	pro	k7c4
sběrnici	sběrnice	k1gFnSc4
SCSI	SCSI	kA
</s>
<s>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
osvědčila	osvědčit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
SCSI	SCSI	kA
řadičů	řadič	k1gInPc2
používajících	používající	k2eAgInPc2d1
TCQ	TCQ	kA
a	a	k8xC
připojených	připojený	k2eAgInPc2d1
přes	přes	k7c4
sběrnici	sběrnice	k1gFnSc4
PCI	PCI	kA
(	(	kIx(
<g/>
případně	případně	k6eAd1
její	její	k3xOp3gMnSc1
nástupce	nástupce	k1gMnSc1
<g/>
)	)	kIx)
nastavovala	nastavovat	k5eAaImAgFnS
DMA	dmout	k5eAaImSgInS
tabulku	tabulka	k1gFnSc4
automaticky	automaticky	k6eAd1
a	a	k8xC
zatěžovala	zatěžovat	k5eAaImAgFnS
procesor	procesor	k1gInSc4
pouze	pouze	k6eAd1
jedním	jeden	k4xCgNnSc7
přerušením	přerušení	k1gNnSc7
na	na	k7c4
příkaz	příkaz	k1gInSc4
<g/>
,	,	kIx,
proto	proto	k8xC
tam	tam	k6eAd1
nedocházelo	docházet	k5eNaImAgNnS
ke	k	k7c3
zpomalování	zpomalování	k1gNnSc3
jako	jako	k8xS,k8xC
při	při	k7c6
ATA	ATA	kA
TCQ	TCQ	kA
<g/>
.	.	kIx.
</s>
<s>
Softwarová	softwarový	k2eAgFnSc1d1
obdoba	obdoba	k1gFnSc1
</s>
<s>
Problematiku	problematika	k1gFnSc4
optimálního	optimální	k2eAgNnSc2d1
pořadí	pořadí	k1gNnSc2
požadavků	požadavek	k1gInPc2
přístupu	přístup	k1gInSc2
k	k	k7c3
disku	disco	k1gNnSc3
v	v	k7c6
jednom	jeden	k4xCgInSc6
okamžiku	okamžik	k1gInSc6
lze	lze	k6eAd1
částečně	částečně	k6eAd1
řešit	řešit	k5eAaImF
také	také	k9
softwarově	softwarově	k6eAd1
<g/>
,	,	kIx,
zabudováním	zabudování	k1gNnSc7
do	do	k7c2
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
úrovni	úroveň	k1gFnSc6
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
není	být	k5eNaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
tolik	tolik	k4xDc4,k4yIc4
informací	informace	k1gFnPc2
<g/>
,	,	kIx,
kolik	kolik	k9
jich	on	k3xPp3gMnPc2
má	mít	k5eAaImIp3nS
disk	disk	k1gInSc1
sám	sám	k3xTgInSc1
o	o	k7c6
sobě	sebe	k3xPyFc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
lze	lze	k6eAd1
provádět	provádět	k5eAaImF
částečnou	částečný	k2eAgFnSc4d1
optimalizaci	optimalizace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
použitím	použití	k1gNnSc7
mezipaměti	mezipamět	k5eAaImF,k5eAaPmF
při	při	k7c6
kopírování	kopírování	k1gNnSc6
velkých	velký	k2eAgInPc2d1
bloků	blok	k1gInPc2
lze	lze	k6eAd1
snížit	snížit	k5eAaPmF
počet	počet	k1gInSc4
jednotlivých	jednotlivý	k2eAgNnPc2d1
čtení	čtení	k1gNnPc2
a	a	k8xC
zápisů	zápis	k1gInPc2
na	na	k7c4
disk	disk	k1gInSc4
a	a	k8xC
tím	ten	k3xDgNnSc7
i	i	k9
snížení	snížení	k1gNnSc1
počtu	počet	k1gInSc2
přesunů	přesun	k1gInPc2
hlaviček	hlavička	k1gFnPc2
disku	disk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Native	Natiev	k1gFnSc2
Command	Command	k1gInSc1
Queuing	Queuing	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Detailní	detailní	k2eAgFnSc1d1
dokumentace	dokumentace	k1gFnSc1
NCQ	NCQ	kA
od	od	k7c2
Seagate	Seagate	kA
</s>
