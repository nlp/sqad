<s>
Existencialismus	existencialismus	k1gInSc1	existencialismus
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
chybně	chybně	k6eAd1	chybně
existencionalismus	existencionalismus	k1gInSc4	existencionalismus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
filosofický	filosofický	k2eAgInSc4d1	filosofický
a	a	k8xC	a
umělecký	umělecký	k2eAgInSc4d1	umělecký
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
tohoto	tento	k3xDgInSc2	tento
směru	směr	k1gInSc2	směr
došlo	dojít	k5eAaPmAgNnS	dojít
především	především	k9	především
díky	díky	k7c3	díky
francouzským	francouzský	k2eAgMnPc3d1	francouzský
představitelům	představitel	k1gMnPc3	představitel
ve	v	k7c6	v
čtyřicátých	čtyřicátý	k4xOgNnPc6	čtyřicátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
existencialismus	existencialismus	k1gInSc1	existencialismus
stal	stát	k5eAaPmAgInS	stát
velice	velice	k6eAd1	velice
populární	populární	k2eAgFnSc7d1	populární
i	i	k8xC	i
módní	módní	k2eAgFnSc7d1	módní
filosofií	filosofie	k1gFnSc7	filosofie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
často	často	k6eAd1	často
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
zjednodušení	zjednodušení	k1gNnSc3	zjednodušení
a	a	k8xC	a
zjemnění	zjemnění	k1gNnSc3	zjemnění
myšlenek	myšlenka	k1gFnPc2	myšlenka
tohoto	tento	k3xDgInSc2	tento
směru	směr	k1gInSc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Filosofie	filosofie	k1gFnSc1	filosofie
tohoto	tento	k3xDgInSc2	tento
směru	směr	k1gInSc2	směr
se	se	k3xPyFc4	se
opírá	opírat	k5eAaImIp3nS	opírat
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
filosofii	filosofie	k1gFnSc4	filosofie
existence	existence	k1gFnSc2	existence
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
některé	některý	k3yIgFnPc4	některý
myšlenky	myšlenka	k1gFnPc4	myšlenka
F.	F.	kA	F.
Nietzscheho	Nietzsche	k1gMnSc2	Nietzsche
<g/>
,	,	kIx,	,
dánského	dánský	k2eAgMnSc2d1	dánský
myslitele	myslitel	k1gMnSc2	myslitel
S.	S.	kA	S.
Kierkegaarda	Kierkegaard	k1gMnSc2	Kierkegaard
(	(	kIx(	(
<g/>
duchovní	duchovní	k1gMnSc1	duchovní
předchůdce	předchůdce	k1gMnSc1	předchůdce
<g/>
:	:	kIx,	:
osobní	osobní	k2eAgFnSc1d1	osobní
víra	víra	k1gFnSc1	víra
a	a	k8xC	a
vědomí	vědomí	k1gNnSc1	vědomí
absurdity	absurdita	k1gFnSc2	absurdita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
E.	E.	kA	E.
Husserla	Husserla	k1gFnSc1	Husserla
a	a	k8xC	a
M.	M.	kA	M.
Heideggera	Heidegger	k1gMnSc2	Heidegger
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
směru	směr	k1gInSc3	směr
někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
<g/>
,	,	kIx,	,
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
přesně	přesně	k6eAd1	přesně
<g/>
,	,	kIx,	,
řazen	řadit	k5eAaImNgInS	řadit
(	(	kIx(	(
<g/>
odmítá	odmítat	k5eAaImIp3nS	odmítat
označení	označení	k1gNnSc1	označení
své	svůj	k3xOyFgFnSc2	svůj
filosofie	filosofie	k1gFnSc2	filosofie
za	za	k7c4	za
filosofii	filosofie	k1gFnSc4	filosofie
existence	existence	k1gFnSc2	existence
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existencialismus	existencialismus	k1gInSc4	existencialismus
výrazně	výrazně	k6eAd1	výrazně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
myšlenky	myšlenka	k1gFnPc1	myšlenka
přímo	přímo	k6eAd1	přímo
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
ruské	ruský	k2eAgFnSc2d1	ruská
literatury	literatura	k1gFnSc2	literatura
(	(	kIx(	(
<g/>
F.	F.	kA	F.
M.	M.	kA	M.
Dostojevskij	Dostojevskij	k1gMnSc1	Dostojevskij
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
i	i	k9	i
ovlivnění	ovlivnění	k1gNnSc2	ovlivnění
F.	F.	kA	F.
Kafkou	Kafka	k1gMnSc7	Kafka
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc7	jeho
povídkou	povídka	k1gFnSc7	povídka
Proměna	proměna	k1gFnSc1	proměna
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
bez	bez	k7c2	bez
důvodu	důvod	k1gInSc2	důvod
promění	proměnit	k5eAaPmIp3nS	proměnit
v	v	k7c4	v
odporného	odporný	k2eAgMnSc4d1	odporný
členovce	členovec	k1gMnSc4	členovec
a	a	k8xC	a
poté	poté	k6eAd1	poté
jen	jen	k9	jen
živoří	živořit	k5eAaImIp3nP	živořit
a	a	k8xC	a
pomalu	pomalu	k6eAd1	pomalu
umírá	umírat	k5eAaImIp3nS	umírat
zavřen	zavřen	k2eAgInSc1d1	zavřen
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
pokoji	pokoj	k1gInSc6	pokoj
<g/>
,	,	kIx,	,
všem	všecek	k3xTgNnPc3	všecek
odporný	odporný	k2eAgInSc1d1	odporný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
směr	směr	k1gInSc1	směr
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
jeho	jeho	k3xOp3gMnPc1	jeho
propagátoři	propagátor	k1gMnPc1	propagátor
<g/>
,	,	kIx,	,
neměli	mít	k5eNaImAgMnP	mít
jednotné	jednotný	k2eAgInPc4d1	jednotný
názory	názor	k1gInPc4	názor
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
myšlenky	myšlenka	k1gFnPc1	myšlenka
si	se	k3xPyFc3	se
i	i	k9	i
protiřečí	protiřečit	k5eAaImIp3nS	protiřečit
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
existencialismu	existencialismus	k1gInSc6	existencialismus
lze	lze	k6eAd1	lze
rozlišit	rozlišit	k5eAaPmF	rozlišit
několik	několik	k4yIc4	několik
základních	základní	k2eAgInPc2d1	základní
proudů	proud	k1gInPc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
proudy	proud	k1gInPc1	proud
uznávaly	uznávat	k5eAaImAgInP	uznávat
některé	některý	k3yIgFnPc4	některý
společné	společný	k2eAgFnPc4d1	společná
myšlenky	myšlenka	k1gFnPc4	myšlenka
a	a	k8xC	a
cíle	cíl	k1gInPc4	cíl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
o	o	k7c6	o
nich	on	k3xPp3gInPc6	on
nelze	lze	k6eNd1	lze
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
vykládány	vykládat	k5eAaImNgFnP	vykládat
jednoznačně	jednoznačně	k6eAd1	jednoznačně
<g/>
.	.	kIx.	.
</s>
<s>
Východiskem	východisko	k1gNnSc7	východisko
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
člověk	člověk	k1gMnSc1	člověk
jako	jako	k8xS	jako
jedinec	jedinec	k1gMnSc1	jedinec
izolovaný	izolovaný	k2eAgMnSc1d1	izolovaný
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
i	i	k8xC	i
dějinného	dějinný	k2eAgInSc2d1	dějinný
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
člověk	člověk	k1gMnSc1	člověk
bez	bez	k7c2	bez
vazeb	vazba	k1gFnPc2	vazba
<g/>
,	,	kIx,	,
soustředěný	soustředěný	k2eAgMnSc1d1	soustředěný
na	na	k7c4	na
svoje	svůj	k3xOyFgNnSc4	svůj
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
ego	ego	k1gNnSc4	ego
<g/>
,	,	kIx,	,
plný	plný	k2eAgInSc1d1	plný
úzkosti	úzkost	k1gFnSc3	úzkost
<g/>
,	,	kIx,	,
pocitu	pocit	k1gInSc3	pocit
nesmyslnosti	nesmyslnost	k1gFnSc2	nesmyslnost
existence	existence	k1gFnSc2	existence
a	a	k8xC	a
vědomí	vědomí	k1gNnSc2	vědomí
nevyhnutelnosti	nevyhnutelnost	k1gFnSc2	nevyhnutelnost
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
odcizení	odcizení	k1gNnSc2	odcizení
a	a	k8xC	a
naprosté	naprostý	k2eAgFnSc2d1	naprostá
osamělosti	osamělost	k1gFnSc2	osamělost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
"	"	kIx"	"
<g/>
nicotě	nicota	k1gFnSc6	nicota
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
člověk	člověk	k1gMnSc1	člověk
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
překonat	překonat	k5eAaPmF	překonat
své	svůj	k3xOyFgNnSc4	svůj
zoufalství	zoufalství	k1gNnSc4	zoufalství
<g/>
,	,	kIx,	,
dobírá	dobírat	k5eAaImIp3nS	dobírat
se	se	k3xPyFc4	se
sebepoznání	sebepoznání	k1gNnSc1	sebepoznání
a	a	k8xC	a
sebeuskutečnění	sebeuskutečnění	k1gNnSc1	sebeuskutečnění
<g/>
,	,	kIx,	,
volí	volit	k5eAaImIp3nS	volit
si	se	k3xPyFc3	se
svými	svůj	k3xOyFgMnPc7	svůj
činy	čina	k1gFnPc4	čina
své	svůj	k3xOyFgNnSc4	svůj
bytí	bytí	k1gNnSc4	bytí
jako	jako	k8xC	jako
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
takový	takový	k3xDgInSc4	takový
<g/>
,	,	kIx,	,
jakého	jaký	k3yQgInSc2	jaký
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
chce	chtít	k5eAaImIp3nS	chtít
mít	mít	k5eAaImF	mít
a	a	k8xC	a
jakým	jaký	k3yIgNnSc7	jaký
se	se	k3xPyFc4	se
činí	činit	k5eAaImIp3nS	činit
<g/>
.	.	kIx.	.
</s>
<s>
Politicky	politicky	k6eAd1	politicky
výrazní	výrazný	k2eAgMnPc1d1	výrazný
existencialisté	existencialista	k1gMnPc1	existencialista
tíhli	tíhnout	k5eAaImAgMnP	tíhnout
většinou	většinou	k6eAd1	většinou
doleva	doleva	k6eAd1	doleva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInPc1	jejich
filosofické	filosofický	k2eAgInPc1d1	filosofický
názory	názor	k1gInPc1	názor
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
pesimistické	pesimistický	k2eAgFnPc1d1	pesimistická
<g/>
.	.	kIx.	.
</s>
<s>
Domnívali	domnívat	k5eAaImAgMnP	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
existence	existence	k1gFnSc1	existence
předchází	předcházet	k5eAaImIp3nS	předcházet
podstatu	podstata	k1gFnSc4	podstata
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
kladen	klást	k5eAaImNgInS	klást
velký	velký	k2eAgInSc1d1	velký
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
aktivitu	aktivita	k1gFnSc4	aktivita
vůči	vůči	k7c3	vůči
budoucnosti	budoucnost	k1gFnSc3	budoucnost
a	a	k8xC	a
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	on	k3xPp3gMnPc4	on
dovedlo	dovést	k5eAaPmAgNnS	dovést
k	k	k7c3	k
popření	popření	k1gNnSc3	popření
Boha	bůh	k1gMnSc2	bůh
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
morálky	morálka	k1gFnSc2	morálka
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jediná	jediný	k2eAgFnSc1d1	jediná
jistota	jistota	k1gFnSc1	jistota
tohoto	tento	k3xDgInSc2	tento
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgInPc1d1	zajímavý
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
názory	názor	k1gInPc1	názor
některých	některý	k3yIgMnPc2	některý
existencialistických	existencialistický	k2eAgMnPc2d1	existencialistický
filosofů	filosof	k1gMnPc2	filosof
na	na	k7c4	na
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
si	se	k3xPyFc3	se
mysleli	myslet	k5eAaImAgMnP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
prakticky	prakticky	k6eAd1	prakticky
donucen	donutit	k5eAaPmNgMnS	donutit
(	(	kIx(	(
<g/>
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
stále	stále	k6eAd1	stále
mezi	mezi	k7c7	mezi
něčím	něco	k3yInSc7	něco
volit	volit	k5eAaImF	volit
a	a	k8xC	a
vybírat	vybírat	k5eAaImF	vybírat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
spisech	spis	k1gInPc6	spis
se	se	k3xPyFc4	se
zabývali	zabývat	k5eAaImAgMnP	zabývat
především	především	k6eAd1	především
otázkami	otázka	k1gFnPc7	otázka
existence	existence	k1gFnSc2	existence
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
rozvinuli	rozvinout	k5eAaPmAgMnP	rozvinout
myšlenky	myšlenka	k1gFnSc2	myšlenka
společné	společný	k2eAgFnPc1d1	společná
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
proudy	proud	k1gInPc4	proud
spojené	spojený	k2eAgInPc4d1	spojený
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
hnutím	hnutí	k1gNnSc7	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Existenci	existence	k1gFnSc4	existence
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
velice	velice	k6eAd1	velice
individuální	individuální	k2eAgInSc4d1	individuální
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
nelze	lze	k6eNd1	lze
odvodit	odvodit	k5eAaPmF	odvodit
z	z	k7c2	z
ničeho	nic	k3yNnSc2	nic
vyššího	vysoký	k2eAgNnSc2d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
teprve	teprve	k6eAd1	teprve
musí	muset	k5eAaImIp3nS	muset
stát	stát	k5eAaPmF	stát
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
čím	co	k3yQnSc7	co
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
ho	on	k3xPp3gInSc4	on
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
věčnými	věčný	k2eAgInPc7d1	věčný
<g/>
,	,	kIx,	,
předem	předem	k6eAd1	předem
formujícími	formující	k2eAgFnPc7d1	formující
kategoriemi	kategorie	k1gFnPc7	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
zdůraznění	zdůraznění	k1gNnSc3	zdůraznění
lidské	lidský	k2eAgFnSc2d1	lidská
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
,	,	kIx,	,
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
spojené	spojený	k2eAgFnPc4d1	spojená
absolutní	absolutní	k2eAgFnSc4d1	absolutní
odpovědnosti	odpovědnost	k1gFnPc4	odpovědnost
za	za	k7c4	za
své	své	k1gNnSc4	své
rozhodování	rozhodování	k1gNnSc2	rozhodování
a	a	k8xC	a
konání	konání	k1gNnSc2	konání
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
existencialističtí	existencialistický	k2eAgMnPc1d1	existencialistický
autoři	autor	k1gMnPc1	autor
kladou	klást	k5eAaImIp3nP	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
mezní	mezní	k2eAgFnPc4d1	mezní
situace	situace	k1gFnPc4	situace
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgFnPc1d1	častá
jsou	být	k5eAaImIp3nP	být
též	též	k9	též
nezvyklé	zvyklý	k2eNgFnPc1d1	nezvyklá
<g/>
,	,	kIx,	,
místy	místo	k1gNnPc7	místo
až	až	k6eAd1	až
provokativní	provokativní	k2eAgFnSc1d1	provokativní
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
některé	některý	k3yIgFnPc4	některý
společenské	společenský	k2eAgFnPc4d1	společenská
situace	situace	k1gFnPc4	situace
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
myšlenek	myšlenka	k1gFnPc2	myšlenka
vyplynula	vyplynout	k5eAaPmAgFnS	vyplynout
závislost	závislost	k1gFnSc1	závislost
bytí	bytí	k1gNnSc2	bytí
na	na	k7c6	na
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
existencialismus	existencialismus	k1gInSc4	existencialismus
zásadní	zásadní	k2eAgInSc4d1	zásadní
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k6eAd1	mnoho
způsobu	způsob	k1gInSc2	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
lze	lze	k6eAd1	lze
dělit	dělit	k5eAaImF	dělit
existencialismus	existencialismus	k1gInSc4	existencialismus
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
je	být	k5eAaImIp3nS	být
dělení	dělení	k1gNnSc1	dělení
podle	podle	k7c2	podle
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
německý	německý	k2eAgMnSc1d1	německý
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
existencialismus	existencialismus	k1gInSc1	existencialismus
často	často	k6eAd1	často
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
teistický	teistický	k2eAgInSc4d1	teistický
-	-	kIx~	-
menšinový	menšinový	k2eAgInSc4d1	menšinový
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
se	se	k3xPyFc4	se
neprosadil	prosadit	k5eNaPmAgMnS	prosadit
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
vlivný	vlivný	k2eAgMnSc1d1	vlivný
byl	být	k5eAaImAgInS	být
však	však	k9	však
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
o	o	k7c4	o
něco	něco	k3yInSc4	něco
méně	málo	k6eAd2	málo
v	v	k7c6	v
USA	USA	kA	USA
ateistický	ateistický	k2eAgInSc1d1	ateistický
-	-	kIx~	-
tento	tento	k3xDgInSc4	tento
směr	směr	k1gInSc4	směr
vyznávali	vyznávat	k5eAaImAgMnP	vyznávat
nejznámější	známý	k2eAgMnPc1d3	nejznámější
představitelé	představitel	k1gMnPc1	představitel
<g/>
,	,	kIx,	,
např.	např.	kA	např.
A.	A.	kA	A.
Camus	Camus	k1gInSc4	Camus
a	a	k8xC	a
J.	J.	kA	J.
<g/>
-	-	kIx~	-
<g/>
P.	P.	kA	P.
Sartre	Sartr	k1gInSc5	Sartr
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgMnSc1d1	samotný
Jean	Jean	k1gMnSc1	Jean
Paul	Paul	k1gMnSc1	Paul
Sartre	Sartr	k1gInSc5	Sartr
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
existencialismus	existencialismus	k1gInSc4	existencialismus
na	na	k7c4	na
křesťanský	křesťanský	k2eAgInSc4d1	křesťanský
(	(	kIx(	(
<g/>
G.	G.	kA	G.
Marcel	Marcel	k1gMnSc1	Marcel
<g/>
,	,	kIx,	,
K.	K.	kA	K.
Jaspers	Jaspers	k1gInSc1	Jaspers
<g/>
)	)	kIx)	)
a	a	k8xC	a
ateistický	ateistický	k2eAgMnSc1d1	ateistický
(	(	kIx(	(
<g/>
M.	M.	kA	M.
Heidegger	Heidegger	k1gInSc1	Heidegger
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
-	-	kIx~	-
<g/>
P.	P.	kA	P.
Sartre	Sartr	k1gInSc5	Sartr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvoirová	Beauvoirová	k1gFnSc1	Beauvoirová
Samuel	Samuel	k1gMnSc1	Samuel
Beckett	Beckett	k1gMnSc1	Beckett
Albert	Albert	k1gMnSc1	Albert
Camus	Camus	k1gMnSc1	Camus
André	André	k1gMnSc1	André
Gide	Gide	k1gFnPc2	Gide
Jan	Jan	k1gMnSc1	Jan
Čep	Čep	k1gMnSc1	Čep
Jan	Jan	k1gMnSc1	Jan
Kameníček	kameníček	k1gInSc4	kameníček
Eugè	Eugè	k1gMnSc1	Eugè
Ionesco	Ionesco	k1gMnSc1	Ionesco
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
František	František	k1gMnSc1	František
R.	R.	kA	R.
Kraus	Kraus	k1gMnSc1	Kraus
Raymond	Raymond	k1gMnSc1	Raymond
Queneau	Quenea	k1gMnSc3	Quenea
Jean-Paul	Jean-Paul	k1gInSc4	Jean-Paul
Sartre	Sartr	k1gInSc5	Sartr
Boris	Boris	k1gMnSc1	Boris
Vian	Viana	k1gFnPc2	Viana
Nikos	Nikos	k1gMnSc1	Nikos
Kazantzakis	Kazantzakis	k1gMnSc1	Kazantzakis
Robert	Robert	k1gMnSc1	Robert
Clark	Clark	k1gInSc4	Clark
Young	Young	k1gMnSc1	Young
Simone	Simon	k1gMnSc5	Simon
de	de	k?	de
Beauvoirová	Beauvoirová	k1gFnSc1	Beauvoirová
Henri	Henr	k1gFnSc2	Henr
Bergson	Bergson	k1gMnSc1	Bergson
Karl	Karl	k1gMnSc1	Karl
Jaspers	Jaspersa	k1gFnPc2	Jaspersa
Hans	Hans	k1gMnSc1	Hans
Jonas	Jonas	k1gMnSc1	Jonas
Walter	Walter	k1gMnSc1	Walter
Kaufmann	Kaufmann	k1gMnSc1	Kaufmann
Jean-Paul	Jean-Paul	k1gInSc4	Jean-Paul
Sartre	Sartr	k1gInSc5	Sartr
Max	Max	k1gMnSc1	Max
Stirner	Stirner	k1gInSc4	Stirner
</s>
