<s>
Existencialismus	existencialismus	k1gInSc1	existencialismus
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
chybně	chybně	k6eAd1	chybně
existencionalismus	existencionalismus	k1gInSc4	existencionalismus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
filosofický	filosofický	k2eAgInSc4d1	filosofický
a	a	k8xC	a
umělecký	umělecký	k2eAgInSc4d1	umělecký
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
