<s>
Ajmarština	Ajmarština	k1gFnSc1	Ajmarština
<g/>
,	,	kIx,	,
aymarština	aymarština	k1gFnSc1	aymarština
<g/>
,	,	kIx,	,
ajmara	ajmara	k1gFnSc1	ajmara
nebo	nebo	k8xC	nebo
aymara	aymara	k1gFnSc1	aymara
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
aimara	aimara	k1gFnSc1	aimara
<g/>
,	,	kIx,	,
aimará	aimarý	k2eAgFnSc1d1	aimarý
<g/>
,	,	kIx,	,
aymara	aymara	k1gFnSc1	aymara
či	či	k8xC	či
aymará	aymarý	k2eAgFnSc1d1	aymarý
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgInSc1d1	vlastní
název	název	k1gInSc1	název
aymar	aymara	k1gFnPc2	aymara
aru	ar	k1gInSc2	ar
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
indiánský	indiánský	k2eAgInSc1d1	indiánský
jazyk	jazyk	k1gInSc1	jazyk
z	z	k7c2	z
And	Anda	k1gFnPc2	Anda
<g/>
.	.	kIx.	.
</s>
<s>
Mluví	mluvit	k5eAaImIp3nS	mluvit
se	se	k3xPyFc4	se
jím	on	k3xPp3gNnSc7	on
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Bolívii	Bolívie	k1gFnSc6	Bolívie
<g/>
,	,	kIx,	,
jižním	jižní	k2eAgNnSc6d1	jižní
Peru	Peru	k1gNnSc6	Peru
a	a	k8xC	a
severním	severní	k2eAgNnSc6d1	severní
Chile	Chile	k1gNnSc6	Chile
<g/>
,	,	kIx,	,
další	další	k2eAgMnPc1d1	další
mluvčí	mluvčí	k1gMnPc1	mluvčí
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
Limě	Lima	k1gFnSc6	Lima
<g/>
,	,	kIx,	,
Santiagu	Santiago	k1gNnSc6	Santiago
de	de	k?	de
Chile	Chile	k1gNnPc6	Chile
a	a	k8xC	a
mezi	mezi	k7c7	mezi
bolivijskými	bolivijský	k2eAgMnPc7d1	bolivijský
imigranty	imigrant	k1gMnPc7	imigrant
v	v	k7c4	v
Buenos	Buenos	k1gInSc4	Buenos
Aires	Airesa	k1gFnPc2	Airesa
a	a	k8xC	a
na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
místech	místo	k1gNnPc6	místo
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bolívii	Bolívie	k1gFnSc6	Bolívie
a	a	k8xC	a
Peru	Peru	k1gNnSc6	Peru
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
úředních	úřední	k2eAgInPc2d1	úřední
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
počtu	počet	k1gInSc2	počet
mluvčích	mluvčí	k1gMnPc2	mluvčí
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
třetím	třetí	k4xOgInSc7	třetí
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
domorodým	domorodý	k2eAgInSc7d1	domorodý
jazykem	jazyk	k1gInSc7	jazyk
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
po	po	k7c6	po
kečuánštině	kečuánština	k1gFnSc6	kečuánština
a	a	k8xC	a
guaraníjštině	guaraníjština	k1gFnSc6	guaraníjština
<g/>
.	.	kIx.	.
</s>
<s>
Ajmarština	Ajmarština	k1gFnSc1	Ajmarština
je	být	k5eAaImIp3nS	být
rodným	rodný	k2eAgInSc7d1	rodný
jazykem	jazyk	k1gInSc7	jazyk
asi	asi	k9	asi
2,2	[number]	k4	2,2
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
název	název	k1gInSc1	název
jazyka	jazyk	k1gInSc2	jazyk
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
ve	v	k7c6	v
slovním	slovní	k2eAgNnSc6d1	slovní
spojení	spojení	k1gNnSc6	spojení
jaya	jayum	k1gNnSc2	jayum
mara	marus	k1gMnSc2	marus
aru	ar	k1gInSc2	ar
–	–	k?	–
starodávný	starodávný	k2eAgInSc1d1	starodávný
jazyk	jazyk	k1gInSc1	jazyk
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
daleký	daleký	k2eAgInSc1d1	daleký
<g/>
/	/	kIx~	/
<g/>
daleko	daleko	k6eAd1	daleko
rok	rok	k1gInSc1	rok
jazyk	jazyk	k1gInSc1	jazyk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
odborníci	odborník	k1gMnPc1	odborník
přiklánějí	přiklánět	k5eAaImIp3nP	přiklánět
spíše	spíše	k9	spíše
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
etnické	etnický	k2eAgFnSc2d1	etnická
skupiny	skupina	k1gFnSc2	skupina
Aymaraes	Aymaraesa	k1gFnPc2	Aymaraesa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
obývala	obývat	k5eAaImAgFnS	obývat
oblast	oblast	k1gFnSc1	oblast
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
nynějšího	nynější	k2eAgInSc2d1	nynější
peruánského	peruánský	k2eAgInSc2d1	peruánský
regionu	region	k1gInSc2	region
Apurímac	Apurímac	k1gFnSc1	Apurímac
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
význam	význam	k1gInSc1	význam
je	být	k5eAaImIp3nS	být
však	však	k9	však
nejasný	jasný	k2eNgInSc1d1	nejasný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
glotochronologické	glotochronologický	k2eAgFnSc2d1	glotochronologický
metody	metoda	k1gFnSc2	metoda
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
rozpadu	rozpad	k1gInSc3	rozpad
původního	původní	k2eAgInSc2d1	původní
jazyka	jazyk	k1gInSc2	jazyk
na	na	k7c4	na
centrální	centrální	k2eAgFnSc4d1	centrální
větev	větev	k1gFnSc4	větev
<g/>
,	,	kIx,	,
jaqaru	jaqara	k1gFnSc4	jaqara
a	a	k8xC	a
kawki	kawki	k1gNnSc1	kawki
došlo	dojít	k5eAaPmAgNnS	dojít
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
500	[number]	k4	500
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
K	k	k7c3	k
rozrůznění	rozrůznění	k1gNnSc3	rozrůznění
dialektů	dialekt	k1gInPc2	dialekt
v	v	k7c6	v
bolivijském	bolivijský	k2eAgInSc6d1	bolivijský
Altiplanu	Altiplan	k1gInSc6	Altiplan
došlo	dojít	k5eAaPmAgNnS	dojít
odhadem	odhad	k1gInSc7	odhad
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1500	[number]	k4	1500
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
kečujštiny	kečujština	k1gFnSc2	kečujština
jsou	být	k5eAaImIp3nP	být
ale	ale	k8xC	ale
ajmarské	ajmarský	k2eAgInPc1d1	ajmarský
dialekty	dialekt	k1gInPc1	dialekt
navzájem	navzájem	k6eAd1	navzájem
srozumitelné	srozumitelný	k2eAgInPc1d1	srozumitelný
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
ajmarsky	ajmarsky	k6eAd1	ajmarsky
hovořilo	hovořit	k5eAaImAgNnS	hovořit
na	na	k7c6	na
území	území	k1gNnSc6	území
kolem	kolem	k7c2	kolem
dnešního	dnešní	k2eAgNnSc2d1	dnešní
města	město	k1gNnSc2	město
Lima	limo	k1gNnSc2	limo
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
jazyk	jazyk	k1gInSc1	jazyk
šířil	šířit	k5eAaImAgInS	šířit
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
největšího	veliký	k2eAgInSc2d3	veliký
rozsahu	rozsah	k1gInSc2	rozsah
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
přibližně	přibližně	k6eAd1	přibližně
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1300	[number]	k4	1300
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
v	v	k7c6	v
části	část	k1gFnSc6	část
departamentu	departament	k1gInSc2	departament
Oruro	Oruro	k1gNnSc4	Oruro
nahrazen	nahrazen	k2eAgInSc4d1	nahrazen
kečujštinou	kečujština	k1gFnSc7	kečujština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bolívii	Bolívie	k1gFnSc6	Bolívie
tento	tento	k3xDgInSc4	tento
proces	proces	k1gInSc4	proces
asimilace	asimilace	k1gFnSc2	asimilace
na	na	k7c4	na
kečujštinu	kečujština	k1gFnSc4	kečujština
trvá	trvat	k5eAaImIp3nS	trvat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Ajmarské	Ajmarský	k2eAgInPc1d1	Ajmarský
dialekty	dialekt	k1gInPc1	dialekt
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
zejména	zejména	k9	zejména
na	na	k7c6	na
fonologické	fonologický	k2eAgFnSc6d1	fonologická
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
například	například	k6eAd1	například
alternují	alternovat	k5eAaImIp3nP	alternovat
fonémy	foném	k1gInPc1	foném
/	/	kIx~	/
<g/>
t	t	k?	t
<g/>
/	/	kIx~	/
a	a	k8xC	a
/	/	kIx~	/
<g/>
th	th	k?	th
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
/	/	kIx~	/
<g/>
j	j	k?	j
<g/>
/	/	kIx~	/
a	a	k8xC	a
/	/	kIx~	/
<g/>
x	x	k?	x
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
/	/	kIx~	/
<g/>
t	t	k?	t
<g/>
/	/	kIx~	/
a	a	k8xC	a
/	/	kIx~	/
<g/>
th	th	k?	th
<g/>
/	/	kIx~	/
(	(	kIx(	(
<g/>
a	a	k8xC	a
jiné	jiný	k2eAgNnSc4d1	jiné
s	s	k7c7	s
aspirací	aspirace	k1gFnSc7	aspirace
<g/>
)	)	kIx)	)
apod.	apod.	kA	apod.
Poměrně	poměrně	k6eAd1	poměrně
často	často	k6eAd1	často
také	také	k9	také
docházi	docháze	k1gFnSc4	docháze
k	k	k7c3	k
přesmyčce	přesmyčka	k1gFnSc3	přesmyčka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
u	u	k7c2	u
chawlla	chawllo	k1gNnSc2	chawllo
a	a	k8xC	a
challwa	challwum	k1gNnSc2	challwum
"	"	kIx"	"
<g/>
ryba	ryba	k1gFnSc1	ryba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
dialekty	dialekt	k1gInPc1	dialekt
také	také	k9	také
velmi	velmi	k6eAd1	velmi
zřídka	zřídka	k6eAd1	zřídka
užívají	užívat	k5eAaImIp3nP	užívat
sufixů	sufix	k1gInPc2	sufix
pro	pro	k7c4	pro
vyjádření	vyjádření	k1gNnSc4	vyjádření
aktuálního	aktuální	k2eAgNnSc2d1	aktuální
větného	větný	k2eAgNnSc2d1	větné
členění	členění	k1gNnSc2	členění
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
morfologie	morfologie	k1gFnSc2	morfologie
a	a	k8xC	a
syntaxe	syntax	k1gFnSc2	syntax
jsou	být	k5eAaImIp3nP	být
rozdíly	rozdíl	k1gInPc1	rozdíl
menší	malý	k2eAgInPc1d2	menší
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
dialektů	dialekt	k1gInPc2	dialekt
silně	silně	k6eAd1	silně
ovlivněných	ovlivněný	k2eAgFnPc2d1	ovlivněná
kečujštinou	kečujština	k1gFnSc7	kečujština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
munktti	munktti	k1gNnSc4	munktti
"	"	kIx"	"
<g/>
nechci	chtít	k5eNaImIp1nS	chtít
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
Paz	Paz	k1gFnSc1	Paz
<g/>
,	,	kIx,	,
Bolívie	Bolívie	k1gFnSc1	Bolívie
<g/>
)	)	kIx)	)
vs	vs	k?	vs
<g/>
.	.	kIx.	.
muntsa	muntsa	k1gFnSc1	muntsa
(	(	kIx(	(
<g/>
Colchane	Colchan	k1gMnSc5	Colchan
<g/>
,	,	kIx,	,
Chile	Chile	k1gNnSc2	Chile
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
manq	manq	k?	manq
<g/>
'	'	kIx"	'
<g/>
asktawa	asktawa	k1gMnSc1	asktawa
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
právě	právě	k6eAd1	právě
<g/>
)	)	kIx)	)
jíš	jíst	k5eAaImIp2nS	jíst
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
Paz	Paz	k1gFnPc2	Paz
<g/>
)	)	kIx)	)
vs	vs	k?	vs
<g/>
.	.	kIx.	.
manq	manq	k?	manq
<g/>
'	'	kIx"	'
<g/>
jtawa	jtawa	k1gMnSc1	jtawa
(	(	kIx(	(
<g/>
Colchane	Colchan	k1gMnSc5	Colchan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
regionálních	regionální	k2eAgFnPc2d1	regionální
variant	varianta	k1gFnPc2	varianta
existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
varianty	varianta	k1gFnPc4	varianta
sociální	sociální	k2eAgFnPc4d1	sociální
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
jaqi	jaqi	k6eAd1	jaqi
(	(	kIx(	(
<g/>
=	=	kIx~	=
<g/>
lidé	člověk	k1gMnPc1	člověk
<g/>
;	;	kIx,	;
zde	zde	k6eAd1	zde
znamená	znamenat	k5eAaImIp3nS	znamenat
indiáni	indián	k1gMnPc1	indián
<g/>
)	)	kIx)	)
a	a	k8xC	a
q	q	k?	q
<g/>
'	'	kIx"	'
<g/>
ara	ara	k1gMnSc1	ara
(	(	kIx(	(
<g/>
pejorativní	pejorativní	k2eAgNnSc1d1	pejorativní
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
bílé	bílý	k1gMnPc4	bílý
a	a	k8xC	a
mestice	mestic	k1gMnPc4	mestic
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
dvě	dva	k4xCgFnPc4	dva
varianty	varianta	k1gFnPc4	varianta
(	(	kIx(	(
<g/>
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
druhá	druhý	k4xOgFnSc1	druhý
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
zejména	zejména	k9	zejména
při	při	k7c6	při
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
příležitostech	příležitost	k1gFnPc6	příležitost
a	a	k8xC	a
v	v	k7c4	v
Radio	radio	k1gNnSc4	radio
Aymara	Aymara	k1gFnSc1	Aymara
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
kromě	kromě	k7c2	kromě
sémantiky	sémantika	k1gFnSc2	sémantika
některých	některý	k3yIgNnPc2	některý
sloves	sloveso	k1gNnPc2	sloveso
počtem	počet	k1gInSc7	počet
fonémů	foném	k1gInPc2	foném
a	a	k8xC	a
syntaktickou	syntaktický	k2eAgFnSc7d1	syntaktická
strukturou	struktura	k1gFnSc7	struktura
vět	věta	k1gFnPc2	věta
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
velkému	velký	k2eAgInSc3d1	velký
počtu	počet	k1gInSc3	počet
hispanismů	hispanismus	k1gInPc2	hispanismus
v	v	k7c6	v
q	q	k?	q
<g/>
'	'	kIx"	'
<g/>
ara	ara	k1gMnSc1	ara
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
code-switching	codewitching	k1gInSc4	code-switching
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
situaci	situace	k1gFnSc4	situace
přirovnat	přirovnat	k5eAaPmF	přirovnat
k	k	k7c3	k
jopará	joparý	k2eAgFnSc1d1	joparý
v	v	k7c6	v
Paraguayi	Paraguay	k1gFnSc6	Paraguay
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
rozdílem	rozdíl	k1gInSc7	rozdíl
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Paraguayi	Paraguay	k1gFnSc6	Paraguay
je	být	k5eAaImIp3nS	být
jopará	joparý	k2eAgFnSc1d1	joparý
běžně	běžně	k6eAd1	běžně
užívaným	užívaný	k2eAgInSc7d1	užívaný
jazykem	jazyk	k1gInSc7	jazyk
jazykem	jazyk	k1gInSc7	jazyk
většiny	většina	k1gFnSc2	většina
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
ajmarojazyčná	ajmarojazyčný	k2eAgFnSc1d1	ajmarojazyčný
populace	populace	k1gFnSc1	populace
v	v	k7c6	v
Bolívii	Bolívie	k1gFnSc6	Bolívie
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
konverzaci	konverzace	k1gFnSc6	konverzace
"	"	kIx"	"
<g/>
čistou	čistý	k2eAgFnSc4d1	čistá
<g/>
"	"	kIx"	"
variantu	varianta	k1gFnSc4	varianta
ajmarštiny	ajmarština	k1gFnSc2	ajmarština
<g/>
.	.	kIx.	.
</s>
<s>
Morfofonologická	Morfofonologický	k2eAgFnSc1d1	Morfofonologický
struktura	struktura	k1gFnSc1	struktura
ajmarštiny	ajmarština	k1gFnSc2	ajmarština
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
úplně	úplně	k6eAd1	úplně
převládá	převládat	k5eAaImIp3nS	převládat
typ	typ	k1gInSc1	typ
CV	CV	kA	CV
(	(	kIx(	(
<g/>
souhláska-samohláska	souhláskaamohlásek	k1gMnSc2	souhláska-samohlásek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
morfosyntakticky	morfosyntakticky	k6eAd1	morfosyntakticky
podmíněné	podmíněný	k2eAgFnSc3d1	podmíněná
elizi	elize	k1gFnSc3	elize
koncových	koncový	k2eAgFnPc2d1	koncová
samohlásek	samohláska	k1gFnPc2	samohláska
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
morfémů	morfém	k1gInPc2	morfém
může	moct	k5eAaImIp3nS	moct
ovšem	ovšem	k9	ovšem
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
výskytu	výskyt	k1gInSc3	výskyt
shluků	shluk	k1gInPc2	shluk
souhlásek	souhláska	k1gFnPc2	souhláska
(	(	kIx(	(
<g/>
consonant	consonant	k1gInSc1	consonant
clusters	clusters	k1gInSc1	clusters
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
yatktti	yatktť	k1gFnSc2	yatktť
(	(	kIx(	(
<g/>
morfofonologicky	morfofonologicky	k6eAd1	morfofonologicky
yati-ka-ta-ti	yatiaa	k5eAaPmF	yati-ka-ta-t
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Staré	Staré	k2eAgFnPc1d1	Staré
výpůjčky	výpůjčka	k1gFnPc1	výpůjčka
ze	z	k7c2	z
španělštiny	španělština	k1gFnSc2	španělština
přizpůsobily	přizpůsobit	k5eAaPmAgInP	přizpůsobit
strukturu	struktura	k1gFnSc4	struktura
slov	slovo	k1gNnPc2	slovo
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
fonémů	foném	k1gInPc2	foném
<g/>
)	)	kIx)	)
pravidlu	pravidlo	k1gNnSc3	pravidlo
CV	CV	kA	CV
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
palasa	palas	k1gMnSc2	palas
(	(	kIx(	(
<g/>
<	<	kIx(	<
plaza	plaz	k1gMnSc2	plaz
–	–	k?	–
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
phuruta	phurut	k2eAgFnSc1d1	phurut
(	(	kIx(	(
<g/>
<	<	kIx(	<
fruta	fruta	k1gFnSc1	fruta
–	–	k?	–
ovoce	ovoce	k1gNnSc1	ovoce
<g/>
)	)	kIx)	)
apod.	apod.	kA	apod.
Ajmarština	Ajmarština	k1gFnSc1	Ajmarština
je	být	k5eAaImIp3nS	být
polysyntetický	polysyntetický	k2eAgInSc4d1	polysyntetický
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
gramatické	gramatický	k2eAgFnSc2d1	gramatická
kategorie	kategorie	k1gFnSc2	kategorie
se	se	k3xPyFc4	se
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
převážně	převážně	k6eAd1	převážně
sufixy	sufix	k1gInPc1	sufix
(	(	kIx(	(
<g/>
skládajícími	skládající	k2eAgMnPc7d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
1-6	[number]	k4	1-6
slabik	slabika	k1gFnPc2	slabika
<g/>
)	)	kIx)	)
s	s	k7c7	s
pevně	pevně	k6eAd1	pevně
danou	daný	k2eAgFnSc7d1	daná
pozicí	pozice	k1gFnSc7	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Typický	typický	k2eAgInSc1d1	typický
slovosled	slovosled	k1gInSc1	slovosled
je	být	k5eAaImIp3nS	být
SOV	sova	k1gFnPc2	sova
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
podmět-předmět	podmětředmět	k5eAaImF	podmět-předmět
<g/>
/	/	kIx~	/
<g/>
komplement-přísudek	komplementřísudek	k1gInSc1	komplement-přísudek
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
nezná	znát	k5eNaImIp3nS	znát
ekvivalent	ekvivalent	k1gInSc4	ekvivalent
slovesa	sloveso	k1gNnSc2	sloveso
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
sufixy	sufix	k1gInPc1	sufix
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Ch	Ch	kA	Ch
<g/>
'	'	kIx"	'
<g/>
uqix	uqix	k1gInSc1	uqix
p	p	k?	p
<g/>
'	'	kIx"	'
<g/>
ujsawa	ujsawa	k1gMnSc1	ujsawa
<g/>
.	.	kIx.	.
–	–	k?	–
Brambory	brambor	k1gInPc1	brambor
jsou	být	k5eAaImIp3nP	být
měkké	měkký	k2eAgMnPc4d1	měkký
<g/>
/	/	kIx~	/
<g/>
moučnaté	moučnatý	k2eAgMnPc4d1	moučnatý
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
slovními	slovní	k2eAgInPc7d1	slovní
druhy	druh	k1gInPc7	druh
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
nejasné	jasný	k2eNgInPc1d1	nejasný
<g/>
,	,	kIx,	,
např.	např.	kA	např.
jayra	jayra	k6eAd1	jayra
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
lenoch	lenoch	k1gMnSc1	lenoch
<g/>
,	,	kIx,	,
líný	líný	k2eAgMnSc1d1	líný
<g/>
,	,	kIx,	,
lenošit	lenošit	k5eAaImF	lenošit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jallu	jallat	k5eAaPmIp1nS	jallat
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
déšť	déšť	k1gInSc1	déšť
<g/>
/	/	kIx~	/
<g/>
prší	pršet	k5eAaImIp3nS	pršet
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
panqara	panqara	k1gFnSc1	panqara
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
"	"	kIx"	"
i	i	k8xC	i
"	"	kIx"	"
<g/>
kvést	kvést	k5eAaImF	kvést
<g/>
"	"	kIx"	"
apod.	apod.	kA	apod.
Postavení	postavení	k1gNnSc1	postavení
slova	slovo	k1gNnSc2	slovo
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
zpravidla	zpravidla	k6eAd1	zpravidla
určují	určovat	k5eAaImIp3nP	určovat
až	až	k9	až
sufixy	sufix	k1gInPc1	sufix
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
jmenné	jmenný	k2eAgMnPc4d1	jmenný
<g/>
,	,	kIx,	,
slovesné	slovesný	k2eAgFnPc1d1	slovesná
a	a	k8xC	a
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
(	(	kIx(	(
<g/>
nevztahují	vztahovat	k5eNaImIp3nP	vztahovat
se	se	k3xPyFc4	se
ke	k	k7c3	k
slovnímu	slovní	k2eAgInSc3d1	slovní
druhu	druh	k1gInSc3	druh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
sufixy	sufix	k1gInPc1	sufix
mění	měnit	k5eAaImIp3nP	měnit
jmenný	jmenný	k2eAgInSc4d1	jmenný
charakter	charakter	k1gInSc4	charakter
slova	slovo	k1gNnSc2	slovo
na	na	k7c4	na
slovesný	slovesný	k2eAgInSc4d1	slovesný
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
několikrát	několikrát	k6eAd1	několikrát
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jednoho	jeden	k4xCgNnSc2	jeden
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
yati	yati	k6eAd1	yati
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
znalost	znalost	k1gFnSc1	znalost
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
vědět	vědět	k5eAaImF	vědět
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
yaticha	yatich	k1gMnSc4	yatich
(	(	kIx(	(
<g/>
>	>	kIx)	>
<g/>
V	V	kA	V
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
naučit	naučit	k5eAaPmF	naučit
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
sufix	sufix	k1gInSc1	sufix
-cha	h	k1gInSc2	-ch
je	být	k5eAaImIp3nS	být
slovesný	slovesný	k2eAgInSc1d1	slovesný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
yatichiri	yatichire	k1gFnSc4	yatichire
(	(	kIx(	(
<g/>
>	>	kIx)	>
<g/>
V	V	kA	V
<g/>
>	>	kIx)	>
<g/>
N	N	kA	N
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
učitel	učitel	k1gMnSc1	učitel
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
sufix	sufix	k1gInSc1	sufix
-iri	ri	k6eAd1	-iri
nominalizuje	nominalizovat	k5eAaBmIp3nS	nominalizovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
yatichirï	yatichirï	k?	yatichirï
(	(	kIx(	(
<g/>
>	>	kIx)	>
<g/>
V	V	kA	V
<g/>
>	>	kIx)	>
<g/>
N	N	kA	N
<g/>
>	>	kIx)	>
<g/>
V	V	kA	V
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
být	být	k5eAaImF	být
učitelem	učitel	k1gMnSc7	učitel
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
prodloužení	prodloužení	k1gNnSc4	prodloužení
koncové	koncový	k2eAgFnSc2d1	koncová
souhlásky	souhláska	k1gFnSc2	souhláska
je	být	k5eAaImIp3nS	být
fonemické	fonemický	k2eAgNnSc1d1	fonemický
a	a	k8xC	a
verbalizuje	verbalizovat	k5eAaBmIp3nS	verbalizovat
<g/>
)	)	kIx)	)
atd.	atd.	kA	atd.
Slovesné	slovesný	k2eAgInPc1d1	slovesný
sufixy	sufix	k1gInPc1	sufix
nejsou	být	k5eNaImIp3nP	být
morfosémanticky	morfosémanticky	k6eAd1	morfosémanticky
transparentní	transparentní	k2eAgMnPc1d1	transparentní
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
jeden	jeden	k4xCgInSc4	jeden
morfém	morfém	k1gInSc4	morfém
v	v	k7c6	v
sobě	se	k3xPyFc3	se
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
kategorii	kategorie	k1gFnSc4	kategorie
způsobu	způsob	k1gInSc2	způsob
<g/>
,	,	kIx,	,
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
osoby	osoba	k1gFnPc1	osoba
podmětu	podmět	k1gInSc2	podmět
a	a	k8xC	a
podle	podle	k7c2	podle
valence	valence	k1gFnSc2	valence
slovesa	sloveso	k1gNnSc2	sloveso
osoby	osoba	k1gFnSc2	osoba
dalšího	další	k2eAgInSc2d1	další
argumentu	argument	k1gInSc2	argument
(	(	kIx(	(
<g/>
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
vybrán	vybrat	k5eAaPmNgMnS	vybrat
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
Silversteinově	Silversteinův	k2eAgFnSc3d1	Silversteinova
hierarchii	hierarchie	k1gFnSc3	hierarchie
životnosti	životnost	k1gFnSc2	životnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ajmarština	Ajmarština	k1gFnSc1	Ajmarština
nezná	znát	k5eNaImIp3nS	znát
pasívum	pasívum	k1gNnSc4	pasívum
(	(	kIx(	(
<g/>
v	v	k7c6	v
indoevropském	indoevropský	k2eAgInSc6d1	indoevropský
smyslu	smysl	k1gInSc6	smysl
<g/>
)	)	kIx)	)
ani	ani	k8xC	ani
aplikativ	aplikativ	k1gInSc4	aplikativ
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
ale	ale	k9	ale
jiné	jiný	k2eAgInPc1d1	jiný
morfologicky	morfologicky	k6eAd1	morfologicky
derivační	derivační	k2eAgInPc1d1	derivační
procesy	proces	k1gInPc1	proces
měnící	měnící	k2eAgFnSc4d1	měnící
valenci	valence	k1gFnSc4	valence
sloves	sloveso	k1gNnPc2	sloveso
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
kauzativum	kauzativum	k1gNnSc1	kauzativum
<g/>
,	,	kIx,	,
benefaktiv	benefaktiv	k1gInSc1	benefaktiv
a	a	k8xC	a
malefaktiv	malefaktiv	k1gInSc1	malefaktiv
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
základní	základní	k2eAgInSc1d1	základní
imperativ	imperativ	k1gInSc1	imperativ
od	od	k7c2	od
slovesa	sloveso	k1gNnSc2	sloveso
alañ	alañ	k?	alañ
"	"	kIx"	"
<g/>
koupit	koupit	k5eAaPmF	koupit
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
alam	alam	k6eAd1	alam
"	"	kIx"	"
<g/>
kup	koupit	k5eAaPmRp2nS	koupit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
s	s	k7c7	s
vyjádřením	vyjádření	k1gNnSc7	vyjádření
druhotného	druhotný	k2eAgInSc2d1	druhotný
argumentu	argument	k1gInSc2	argument
sufixem	sufix	k1gInSc7	sufix
-ita	tum	k1gNnSc2	-itum
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
forma	forma	k1gFnSc1	forma
alita	alit	k1gInSc2	alit
"	"	kIx"	"
<g/>
kup	kupit	k5eAaImRp2nS	kupit
(	(	kIx(	(
<g/>
si	se	k3xPyFc3	se
<g/>
)	)	kIx)	)
ode	ode	k7c2	ode
mne	já	k3xPp1nSc2	já
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
s	s	k7c7	s
benefaktivním	benefaktivní	k2eAgInSc7d1	benefaktivní
sufixem	sufix	k1gInSc7	sufix
-rapi	ap	k1gFnSc2	-rap
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
forma	forma	k1gFnSc1	forma
alarapita	alarapita	k1gFnSc1	alarapita
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
kup	kup	k1gInSc1	kup
pro	pro	k7c4	pro
mě	já	k3xPp1nSc4	já
<g/>
"	"	kIx"	"
atd.	atd.	kA	atd.
Podobně	podobně	k6eAd1	podobně
existují	existovat	k5eAaImIp3nP	existovat
vedle	vedle	k7c2	vedle
sebe	se	k3xPyFc2	se
formy	forma	k1gFnPc4	forma
sartam	sartam	k6eAd1	sartam
"	"	kIx"	"
<g/>
vstaň	vstát	k5eAaPmRp2nS	vstát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
sartayam	sartayam	k6eAd1	sartayam
"	"	kIx"	"
<g/>
vzbuď	vzbudit	k5eAaPmRp2nS	vzbudit
ho	on	k3xPp3gMnSc4	on
(	(	kIx(	(
<g/>
kauzativum	kauzativum	k1gNnSc4	kauzativum
<g/>
:	:	kIx,	:
donuť	donutit	k5eAaPmRp2nS	donutit
ho	on	k3xPp3gInSc4	on
vstát	vstát	k5eAaPmF	vstát
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
sartayarapita	sartayarapit	k2eAgFnSc1d1	sartayarapit
"	"	kIx"	"
<g/>
vzbuď	vzbudit	k5eAaPmRp2nS	vzbudit
ho	on	k3xPp3gMnSc4	on
kvůli	kvůli	k7c3	kvůli
mně	já	k3xPp1nSc3	já
<g/>
"	"	kIx"	"
apod.	apod.	kA	apod.
Vztažné	vztažný	k2eAgFnPc1d1	vztažná
vedlejší	vedlejší	k2eAgFnPc1d1	vedlejší
věty	věta	k1gFnPc1	věta
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
nominální	nominální	k2eAgFnSc7d1	nominální
formou	forma	k1gFnSc7	forma
slovesa	sloveso	k1gNnSc2	sloveso
a	a	k8xC	a
příslušnými	příslušný	k2eAgInPc7d1	příslušný
závislými	závislý	k2eAgInPc7d1	závislý
členy	člen	k1gInPc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Nominální	nominální	k2eAgFnSc1d1	nominální
forma	forma	k1gFnSc1	forma
slovesa	sloveso	k1gNnSc2	sloveso
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
finitní	finitní	k2eAgFnSc2d1	finitní
nevyjadřuje	vyjadřovat	k5eNaImIp3nS	vyjadřovat
osobu	osoba	k1gFnSc4	osoba
předmětu	předmět	k1gInSc2	předmět
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
např.	např.	kA	např.
yatichañ	yatichañ	k?	yatichañ
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
bude	být	k5eAaImBp3nS	být
učit	učit	k5eAaImF	učit
mě	já	k3xPp1nSc4	já
<g/>
/	/	kIx~	/
<g/>
tebe	ty	k3xPp2nSc4	ty
<g/>
/	/	kIx~	/
<g/>
jeho	jeho	k3xOp3gFnPc2	jeho
<g/>
/	/	kIx~	/
<g/>
ji	on	k3xPp3gFnSc4	on
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
finitní	finitní	k2eAgFnPc1d1	finitní
formy	forma	k1gFnPc1	forma
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
<g/>
:	:	kIx,	:
yatitu	yatit	k2eAgFnSc4d1	yatit
<g/>
/	/	kIx~	/
<g/>
yattam	yattam	k6eAd1	yattam
<g/>
/	/	kIx~	/
<g/>
yati	yati	k6eAd1	yati
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
věty	věta	k1gFnSc2	věta
má	mít	k5eAaImIp3nS	mít
slovesná	slovesný	k2eAgFnSc1d1	slovesná
forma	forma	k1gFnSc1	forma
pád	pád	k1gInSc1	pád
<g/>
,	,	kIx,	,
např.	např.	kA	např.
yatiqañ	yatiqañ	k?	yatiqañ
"	"	kIx"	"
<g/>
aby	aby	kYmCp3nS	aby
ses	ses	k?	ses
mohl	moct	k5eAaImAgMnS	moct
učit	učit	k5eAaImF	učit
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
benefaktiv	benefaktiv	k1gInSc1	benefaktiv
<g/>
:	:	kIx,	:
-taki	aki	k1gNnSc1	-taki
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
yatiqañ	yatiqañ	k?	yatiqañ
"	"	kIx"	"
<g/>
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
budeš	být	k5eAaImBp2nS	být
učit	učit	k5eAaImF	učit
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
limitativ	limitativ	k1gInSc1	limitativ
<g/>
:	:	kIx,	:
-kama	ama	k1gFnSc1	-kama
<g/>
)	)	kIx)	)
apod.	apod.	kA	apod.
Osoba	osoba	k1gFnSc1	osoba
podmětu	podmět	k1gInSc2	podmět
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
v	v	k7c6	v
závislých	závislý	k2eAgFnPc6d1	závislá
větách	věta	k1gFnPc6	věta
posesivním	posesivní	k2eAgInSc7d1	posesivní
sufixem	sufix	k1gInSc7	sufix
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
sufixy	sufix	k1gInPc4	sufix
(	(	kIx(	(
<g/>
posesor	posesor	k1gMnSc1	posesor
<g/>
)	)	kIx)	)
a	a	k8xC	a
lokativem	lokativ	k1gInSc7	lokativ
(	(	kIx(	(
<g/>
závislý	závislý	k2eAgInSc1d1	závislý
člen	člen	k1gInSc1	člen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
utaja	utaj	k1gInSc2	utaj
"	"	kIx"	"
<g/>
můj	můj	k3xOp1gInSc1	můj
dům	dům	k1gInSc1	dům
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kullakan	kullakan	k1gInSc1	kullakan
utapa	utapa	k1gFnSc1	utapa
"	"	kIx"	"
<g/>
sestřin	sestřin	k2eAgInSc1d1	sestřin
dům	dům	k1gInSc1	dům
<g/>
"	"	kIx"	"
apod.	apod.	kA	apod.
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
závislý	závislý	k2eAgInSc1d1	závislý
člen	člen	k1gInSc1	člen
neživotný	životný	k2eNgInSc1d1	neživotný
<g/>
,	,	kIx,	,
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgMnPc7	dva
členy	člen	k1gMnPc7	člen
není	být	k5eNaImIp3nS	být
morfologicky	morfologicky	k6eAd1	morfologicky
vyjádřen	vyjádřen	k2eAgMnSc1d1	vyjádřen
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Wuliwya	Wuliwya	k1gFnSc1	Wuliwya
prisidinti	prisidinť	k1gFnSc2	prisidinť
"	"	kIx"	"
<g/>
prezident	prezident	k1gMnSc1	prezident
Bolívie	Bolívie	k1gFnSc2	Bolívie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
predikátu	predikát	k1gInSc6	predikát
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
sufixu	sufix	k1gInSc3	sufix
-ni	i	k?	-ni
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pä	pä	k?	pä
utanï	utanï	k?	utanï
"	"	kIx"	"
<g/>
mám	mít	k5eAaImIp1nS	mít
dva	dva	k4xCgInPc4	dva
domy	dům	k1gInPc4	dům
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
slovesa	sloveso	k1gNnSc2	sloveso
utjañ	utjañ	k?	utjañ
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pä	pä	k?	pä
utax	utax	k1gInSc1	utax
utjituwa	utjituwa	k1gFnSc1	utjituwa
"	"	kIx"	"
<g/>
mám	mít	k5eAaImIp1nS	mít
dva	dva	k4xCgInPc4	dva
domy	dům	k1gInPc4	dům
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
sufix	sufix	k1gInSc1	sufix
-itu	tu	k6eAd1	-itu
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
shodu	shoda	k1gFnSc4	shoda
v	v	k7c6	v
osobě	osoba	k1gFnSc6	osoba
s	s	k7c7	s
podmětem	podmět	k1gInSc7	podmět
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
a	a	k8xC	a
předmětem	předmět	k1gInSc7	předmět
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
lze	lze	k6eAd1	lze
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
predikátu	predikát	k1gInSc2	predikát
použít	použít	k5eAaPmF	použít
přímo	přímo	k6eAd1	přímo
posesora	posesor	k1gMnSc4	posesor
<g/>
:	:	kIx,	:
pä	pä	k?	pä
utax	utax	k1gInSc1	utax
nayankiwa	nayankiw	k1gInSc2	nayankiw
"	"	kIx"	"
<g/>
dva	dva	k4xCgInPc1	dva
domy	dům	k1gInPc1	dům
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
<g/>
)	)	kIx)	)
moje	můj	k3xOp1gNnSc4	můj
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
sufix	sufix	k1gInSc1	sufix
-nk	k	k?	-nk
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
lokativu	lokativ	k1gInSc2	lokativ
a	a	k8xC	a
verbalizuje	verbalizovat	k5eAaBmIp3nS	verbalizovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Posesivní	posesivní	k2eAgInPc1d1	posesivní
sufixy	sufix	k1gInPc1	sufix
<g/>
:	:	kIx,	:
Příklady	příklad	k1gInPc1	příklad
sufixů	sufix	k1gInPc2	sufix
pro	pro	k7c4	pro
slovesný	slovesný	k2eAgInSc4d1	slovesný
způsob	způsob	k1gInSc4	způsob
<g/>
:	:	kIx,	:
U	u	k7c2	u
číslovek	číslovka	k1gFnPc2	číslovka
ajmarština	ajmarština	k1gFnSc1	ajmarština
dříve	dříve	k6eAd2	dříve
používala	používat	k5eAaImAgFnS	používat
pětkovou	pětkový	k2eAgFnSc4d1	pětková
soustavu	soustava	k1gFnSc4	soustava
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
např.	např.	kA	např.
grónština	grónština	k1gFnSc1	grónština
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
prstů	prst	k1gInPc2	prst
na	na	k7c6	na
ruce	ruka	k1gFnSc6	ruka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
číslovek	číslovka	k1gFnPc2	číslovka
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
výpůjčkami	výpůjčka	k1gFnPc7	výpůjčka
z	z	k7c2	z
kečujštiny	kečujština	k1gFnSc2	kečujština
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
např.	např.	kA	např.
ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
paqallqu	paqallqus	k1gInSc2	paqallqus
"	"	kIx"	"
<g/>
sedm	sedm	k4xCc1	sedm
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
dva-pět	dvaět	k5eAaImF	dva-pět
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
kimsaqallqu	kimsaqallqa	k1gFnSc4	kimsaqallqa
"	"	kIx"	"
<g/>
osm	osm	k4xCc1	osm
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
tři-pět	třiět	k5eAaImF	tři-pět
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aktuální	aktuální	k2eAgNnSc1d1	aktuální
větné	větný	k2eAgNnSc1d1	větné
členění	členění	k1gNnSc1	členění
se	se	k3xPyFc4	se
v	v	k7c6	v
ajmarštině	ajmarština	k1gFnSc6	ajmarština
povinně	povinně	k6eAd1	povinně
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
sufixy	sufix	k1gInPc4	sufix
-xa	a	k?	-xa
(	(	kIx(	(
<g/>
téma	téma	k1gNnSc1	téma
<g/>
)	)	kIx)	)
a	a	k8xC	a
-wa	a	k?	-wa
(	(	kIx(	(
<g/>
réma	réma	k1gNnSc1	réma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sufix	sufix	k1gInSc1	sufix
-wa	a	k?	-wa
má	mít	k5eAaImIp3nS	mít
nulový	nulový	k2eAgInSc1d1	nulový
alomorf	alomorf	k1gInSc1	alomorf
u	u	k7c2	u
slov	slovo	k1gNnPc2	slovo
v	v	k7c6	v
akuzativu	akuzativ	k1gInSc6	akuzativ
<g/>
,	,	kIx,	,
nejedná	jednat	k5eNaImIp3nS	jednat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
o	o	k7c4	o
poslední	poslední	k2eAgNnSc4d1	poslední
slovo	slovo	k1gNnSc4	slovo
věty	věta	k1gFnSc2	věta
(	(	kIx(	(
<g/>
qut	qut	k?	qut
saräxa	saräxa	k1gMnSc1	saräxa
vs	vs	k?	vs
<g/>
.	.	kIx.	.
saräx	saräx	k1gInSc1	saräx
qutwa	qutw	k1gInSc2	qutw
"	"	kIx"	"
<g/>
půjdu	jít	k5eAaImIp1nS	jít
k	k	k7c3	k
jezeru	jezero	k1gNnSc3	jezero
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovosled	slovosled	k1gInSc1	slovosled
jmenných	jmenný	k2eAgFnPc2d1	jmenná
frází	fráze	k1gFnPc2	fráze
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
pevně	pevně	k6eAd1	pevně
<g/>
,	,	kIx,	,
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
klauze	klauze	k1gFnSc2	klauze
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
volný	volný	k2eAgInSc1d1	volný
(	(	kIx(	(
<g/>
převládá	převládat	k5eAaImIp3nS	převládat
SOV	sova	k1gFnPc2	sova
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přívlastek	přívlastek	k1gInSc4	přívlastek
a	a	k8xC	a
příslovečné	příslovečný	k2eAgNnSc4d1	příslovečné
určení	určení	k1gNnSc4	určení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
lokativní	lokativní	k2eAgFnSc1d1	lokativní
fráze	fráze	k1gFnSc1	fráze
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
odlišně	odlišně	k6eAd1	odlišně
<g/>
,	,	kIx,	,
např.	např.	kA	např.
věta	věta	k1gFnSc1	věta
muž	muž	k1gMnSc1	muž
v	v	k7c6	v
domě	dům	k1gInSc6	dům
spal	spát	k5eAaImAgMnS	spát
se	se	k3xPyFc4	se
přeloží	přeložit	k5eAaPmIp3nP	přeložit
buď	buď	k8xC	buď
utankir	utankir	k1gInSc4	utankir
chachax	chachax	k1gInSc1	chachax
ikiwa	ikiwa	k1gFnSc1	ikiwa
nebo	nebo	k8xC	nebo
utanx	utanx	k1gInSc1	utanx
chachax	chachax	k1gInSc1	chachax
ikiwa	ikiwa	k1gFnSc1	ikiwa
(	(	kIx(	(
<g/>
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
případě	případ	k1gInSc6	případ
závisí	záviset	k5eAaImIp3nS	záviset
utan	utan	k1gInSc1	utan
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
v	v	k7c6	v
domě	dům	k1gInSc6	dům
<g/>
"	"	kIx"	"
na	na	k7c6	na
podmětu	podmět	k1gInSc6	podmět
<g/>
,	,	kIx,	,
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
případě	případ	k1gInSc6	případ
na	na	k7c6	na
slovese	sloveso	k1gNnSc6	sloveso
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ajmarština	Ajmarština	k1gFnSc1	Ajmarština
sdílí	sdílet	k5eAaImIp3nS	sdílet
s	s	k7c7	s
kečujštinou	kečujština	k1gFnSc7	kečujština
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
<g/>
%	%	kIx~	%
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
<g/>
,	,	kIx,	,
strukturu	struktura	k1gFnSc4	struktura
gramatiky	gramatika	k1gFnSc2	gramatika
a	a	k8xC	a
také	také	k9	také
mnoho	mnoho	k4c4	mnoho
sufixů	sufix	k1gInPc2	sufix
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jazykovědců	jazykovědec	k1gMnPc2	jazykovědec
se	se	k3xPyFc4	se
přesto	přesto	k8xC	přesto
přiklání	přiklánět	k5eAaImIp3nS	přiklánět
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
výsledek	výsledek	k1gInSc4	výsledek
jazykového	jazykový	k2eAgInSc2d1	jazykový
kontaktu	kontakt	k1gInSc2	kontakt
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
že	že	k8xS	že
oba	dva	k4xCgInPc1	dva
jazyky	jazyk	k1gInPc1	jazyk
nejsou	být	k5eNaImIp3nP	být
blíže	blízce	k6eAd2	blízce
geneticky	geneticky	k6eAd1	geneticky
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
přehled	přehled	k1gInSc1	přehled
některých	některý	k3yIgInPc2	některý
rozdílů	rozdíl	k1gInPc2	rozdíl
<g/>
:	:	kIx,	:
pořadí	pořadí	k1gNnSc1	pořadí
sufixů	sufix	k1gInPc2	sufix
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ajmarštině	ajmarština	k1gFnSc6	ajmarština
"	"	kIx"	"
<g/>
plurál-posesor	plurálosesor	k1gInSc1	plurál-posesor
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
kečujštině	kečujština	k1gFnSc6	kečujština
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ajmarských	ajmarský	k2eAgInPc6d1	ajmarský
dialektech	dialekt	k1gInPc6	dialekt
sousedících	sousedící	k2eAgInPc6d1	sousedící
s	s	k7c7	s
kečujšinou	kečujšina	k1gFnSc7	kečujšina
je	být	k5eAaImIp3nS	být
pořadí	pořadí	k1gNnSc4	pořadí
opačné	opačný	k2eAgNnSc4d1	opačné
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
např.	např.	kA	např.
ajm	ajm	k?	ajm
<g/>
.	.	kIx.	.
uta-naka-ja	utaakaa	k6eAd1	uta-naka-ja
"	"	kIx"	"
<g/>
moje	můj	k3xOp1gInPc4	můj
domy	dům	k1gInPc4	dům
<g/>
"	"	kIx"	"
vs	vs	k?	vs
<g/>
.	.	kIx.	.
dial.	dial.	k?	dial.
(	(	kIx(	(
<g/>
a	a	k8xC	a
kečua	kečuus	k1gMnSc4	kečuus
<g/>
)	)	kIx)	)
uta-ja-naka	utaaak	k1gMnSc4	uta-ja-nak
<g/>
,	,	kIx,	,
ajmarština	ajmarština	k1gFnSc1	ajmarština
nemá	mít	k5eNaImIp3nS	mít
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
sloveso	sloveso	k1gNnSc4	sloveso
"	"	kIx"	"
<g/>
být	být	k5eAaImF	být
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
různé	různý	k2eAgInPc1d1	různý
sufixy	sufix	k1gInPc1	sufix
<g />
.	.	kIx.	.
</s>
<s>
s	s	k7c7	s
lokativním	lokativní	k2eAgInSc7d1	lokativní
a	a	k8xC	a
posesivním	posesivní	k2eAgInSc7d1	posesivní
významem	význam	k1gInSc7	význam
<g/>
,	,	kIx,	,
např.	např.	kA	např.
utajankta	utajankta	k1gFnSc1	utajankta
"	"	kIx"	"
<g/>
jsem	být	k5eAaImIp1nS	být
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domě	dům	k1gInSc6	dům
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jumankiwa	jumankiwa	k1gFnSc1	jumankiwa
"	"	kIx"	"
<g/>
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
tvoje	tvůj	k3xOp2gNnSc4	tvůj
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Wuliwyankirï	Wuliwyankirï	k1gFnSc1	Wuliwyankirï
"	"	kIx"	"
<g/>
jsi	být	k5eAaImIp2nS	být
z	z	k7c2	z
Bolívie	Bolívie	k1gFnSc2	Bolívie
<g/>
"	"	kIx"	"
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
předmětový	předmětový	k2eAgInSc1d1	předmětový
infinitiv	infinitiv	k1gInSc1	infinitiv
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
kečujštině	kečujština	k1gFnSc6	kečujština
sufix	sufix	k1gInSc1	sufix
akuzativu	akuzativ	k1gInSc2	akuzativ
-ta	a	k?	-ta
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
ajmarštině	ajmarština	k1gFnSc6	ajmarština
ne	ne	k9	ne
<g/>
,	,	kIx,	,
např.	např.	kA	např.
mikhuyta	mikhuyt	k1gInSc2	mikhuyt
munani	munaň	k1gFnSc3	munaň
vs	vs	k?	vs
<g/>
.	.	kIx.	.
manq	manq	k?	manq
<g/>
'	'	kIx"	'
<g/>
añ	añ	k?	añ
munta	munta	k1gFnSc1	munta
"	"	kIx"	"
<g/>
chci	chtít	k5eAaImIp1nS	chtít
jist	jist	k2eAgMnSc1d1	jist
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
relativní	relativní	k2eAgFnSc2d1	relativní
a	a	k8xC	a
doplňkové	doplňkový	k2eAgFnSc2d1	doplňková
klauze	klauze	k1gFnSc2	klauze
v	v	k7c6	v
ajmarština	ajmarština	k1gFnSc1	ajmarština
nevyjadřují	vyjadřovat	k5eNaImIp3nP	vyjadřovat
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
kečujštiny	kečujština	k1gFnSc2	kečujština
a	a	k8xC	a
ajmarských	ajmarský	k2eAgNnPc2d1	ajmarský
finitních	finitní	k2eAgNnPc2d1	finitní
sloves	sloveso	k1gNnPc2	sloveso
<g/>
)	)	kIx)	)
osobu	osoba	k1gFnSc4	osoba
předmětu	předmět	k1gInSc2	předmět
u	u	k7c2	u
přechodných	přechodný	k2eAgNnPc2d1	přechodné
sloves	sloveso	k1gNnPc2	sloveso
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
tj.	tj.	kA	tj.
např.	např.	kA	např.
uñ	uñ	k?	uñ
může	moct	k5eAaImIp3nS	moct
znamenat	znamenat	k5eAaImF	znamenat
"	"	kIx"	"
<g/>
vida	vida	k?	vida
<g/>
/	/	kIx~	/
<g/>
vidouc	vidět	k5eAaImSgFnS	vidět
<g/>
(	(	kIx(	(
<g/>
e	e	k0	e
<g/>
)	)	kIx)	)
tebe	ty	k3xPp2nSc4	ty
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
vida	vida	k?	vida
<g/>
/	/	kIx~	/
<g/>
vidouc	vidět	k5eAaImSgFnS	vidět
<g/>
(	(	kIx(	(
<g/>
e	e	k0	e
<g/>
)	)	kIx)	)
jeho	jeho	k3xOp3gFnPc2	jeho
<g/>
/	/	kIx~	/
<g/>
ji	on	k3xPp3gFnSc4	on
<g/>
"	"	kIx"	"
atd.	atd.	kA	atd.
Finitní	finitní	k2eAgFnPc4d1	finitní
formy	forma	k1gFnPc4	forma
mají	mít	k5eAaImIp3nP	mít
různé	různý	k2eAgInPc1d1	různý
sufixy	sufix	k1gInPc1	sufix
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
kombinaci	kombinace	k1gFnSc4	kombinace
osoby	osoba	k1gFnSc2	osoba
podmětu	podmět	k1gInSc2	podmět
a	a	k8xC	a
předmětu	předmět	k1gInSc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Kunas	Kunas	k1gMnSc1	Kunas
(	(	kIx(	(
<g/>
jupan	jupan	k1gMnSc1	jupan
<g/>
)	)	kIx)	)
sutipaxa	sutipaxa	k1gMnSc1	sutipaxa
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
Jupan	Jupan	k1gInSc1	Jupan
<g/>
)	)	kIx)	)
sutipax	sutipax	k1gInSc1	sutipax
Julyawa	Julyaw	k1gInSc2	Julyaw
<g/>
.	.	kIx.	.
–	–	k?	–
Jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
se	se	k3xPyFc4	se
Julie	Julie	k1gFnSc1	Julie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ajmarština	ajmarština	k1gFnSc1	ajmarština
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
ajmarština	ajmarština	k1gFnSc1	ajmarština
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
Ajmarština	Ajmarština	k1gFnSc1	Ajmarština
na	na	k7c6	na
webu	web	k1gInSc6	web
indianskejazyky	indianskejazyka	k1gFnSc2	indianskejazyka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
Instituto	Institut	k2eAgNnSc1d1	Instituto
de	de	k?	de
Lengua	Lengu	k2eAgFnSc1d1	Lengua
y	y	k?	y
Cultura	Cultura	k1gFnSc1	Cultura
Aymara	Aymara	k1gFnSc1	Aymara
Učebnice	učebnice	k1gFnSc1	učebnice
a	a	k8xC	a
kurzy	kurz	k1gInPc1	kurz
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
Kurz	kurz	k1gInSc1	kurz
Aymara	Aymar	k1gMnSc2	Aymar
on	on	k3xPp3gMnSc1	on
The	The	k1gMnSc1	The
Internet	Internet	k1gInSc1	Internet
(	(	kIx(	(
<g/>
lapazský	lapazský	k2eAgInSc1d1	lapazský
dialekt	dialekt	k1gInSc1	dialekt
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
Kurz	kurz	k1gInSc1	kurz
Ciberaymara	Ciberaymar	k1gMnSc2	Ciberaymar
(	(	kIx(	(
<g/>
lapazský	lapazský	k2eAgInSc1d1	lapazský
dialekt	dialekt	k1gInSc1	dialekt
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
Základy	základ	k1gInPc1	základ
ajmarštiny	ajmarština	k1gFnSc2	ajmarština
(	(	kIx(	(
<g/>
chilský	chilský	k2eAgInSc1d1	chilský
dialekt	dialekt	k1gInSc1	dialekt
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Základy	základ	k1gInPc1	základ
ajmarštiny	ajmarština	k1gFnSc2	ajmarština
(	(	kIx(	(
<g/>
chilský	chilský	k2eAgInSc1d1	chilský
dialekt	dialekt	k1gInSc1	dialekt
<g/>
)	)	kIx)	)
Gramatika	gramatika	k1gFnSc1	gramatika
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
M.	M.	kA	M.
J.	J.	kA	J.
Hardman	Hardman	k1gMnSc1	Hardman
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Vásquez	Vásquez	k1gInSc1	Vásquez
<g/>
,	,	kIx,	,
J.	J.	kA	J.
D.	D.	kA	D.
Yapita	Yapita	k1gMnSc1	Yapita
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
–	–	k?	–
Aymara	Aymara	k1gFnSc1	Aymara
<g/>
:	:	kIx,	:
Compendio	Compendio	k6eAd1	Compendio
de	de	k?	de
estructura	estructura	k1gFnSc1	estructura
fonológica	fonológica	k1gMnSc1	fonológica
y	y	k?	y
gramatical	gramaticat	k5eAaPmAgMnS	gramaticat
Slovníky	slovník	k1gInPc4	slovník
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
Revidovaný	revidovaný	k2eAgInSc1d1	revidovaný
španělsko-ajmarský	španělskojmarský	k2eAgInSc1d1	španělsko-ajmarský
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
ajmarsko-španělský	ajmarsko-španělský	k2eAgInSc1d1	ajmarsko-španělský
slovník	slovník	k1gInSc1	slovník
Ludovika	Ludovikum	k1gNnSc2	Ludovikum
Bertonia	Bertonium	k1gNnSc2	Bertonium
(	(	kIx(	(
<g/>
historický	historický	k2eAgInSc1d1	historický
dialekt	dialekt	k1gInSc1	dialekt
z	z	k7c2	z
Chucuita	Chucuitum	k1gNnSc2	Chucuitum
a	a	k8xC	a
Juli	Juli	k1gNnSc2	Juli
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
Španělsko-ajmarský	Španělskojmarský	k2eAgInSc1d1	Španělsko-ajmarský
slovník	slovník	k1gInSc1	slovník
Félixe	Félixe	k1gFnSc2	Félixe
Laymeho	Layme	k1gMnSc2	Layme
Pairumaniho	Pairumani	k1gMnSc2	Pairumani
(	(	kIx(	(
<g/>
lapazský	lapazský	k2eAgInSc1d1	lapazský
dialekt	dialekt	k1gInSc1	dialekt
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
Ajmarsko-španělský	Ajmarsko-španělský	k2eAgInSc1d1	Ajmarsko-španělský
slovník	slovník	k1gInSc1	slovník
(	(	kIx(	(
<g/>
chilský	chilský	k2eAgInSc1d1	chilský
dialekt	dialekt	k1gInSc1	dialekt
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
Ajmarsko-španělský	Ajmarsko-španělský	k2eAgInSc1d1	Ajmarsko-španělský
a	a	k8xC	a
španělsko-ajmarský	španělskojmarský	k2eAgInSc1d1	španělsko-ajmarský
slovník	slovník	k1gInSc1	slovník
(	(	kIx(	(
<g/>
lapazský	lapazský	k2eAgInSc1d1	lapazský
dialekt	dialekt	k1gInSc1	dialekt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jiná	jiný	k2eAgFnSc1d1	jiná
verze	verze	k1gFnSc1	verze
téhož	týž	k3xTgInSc2	týž
slovníku	slovník	k1gInSc2	slovník
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
Ajmarsko-španělský	Ajmarsko-španělský	k2eAgInSc1d1	Ajmarsko-španělský
a	a	k8xC	a
španělsko-ajmarský	španělskojmarský	k2eAgInSc1d1	španělsko-ajmarský
slovník	slovník	k1gInSc1	slovník
(	(	kIx(	(
<g/>
lapazský	lapazský	k2eAgInSc1d1	lapazský
dialekt	dialekt	k1gInSc1	dialekt
<g/>
)	)	kIx)	)
</s>
