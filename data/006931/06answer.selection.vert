<s>
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Harapes	Harapes	k1gMnSc1	Harapes
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1946	[number]	k4	1946
Droužkovice	Droužkovice	k1gFnSc1	Droužkovice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
tanečník	tanečník	k1gMnSc1	tanečník
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
choreograf	choreograf	k1gMnSc1	choreograf
<g/>
,	,	kIx,	,
taneční	taneční	k2eAgMnSc1d1	taneční
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
člen	člen	k1gMnSc1	člen
<g/>
,	,	kIx,	,
sólista	sólista	k1gMnSc1	sólista
baletu	balet	k1gInSc2	balet
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
umělecký	umělecký	k2eAgMnSc1d1	umělecký
ředitel	ředitel	k1gMnSc1	ředitel
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
konzervatoře	konzervatoř	k1gFnSc2	konzervatoř
Praha	Praha	k1gFnSc1	Praha
Po	po	k7c6	po
absolutoriu	absolutorium	k1gNnSc6	absolutorium
pražské	pražský	k2eAgFnSc2d1	Pražská
taneční	taneční	k2eAgFnSc2d1	taneční
konzervatoře	konzervatoř	k1gFnSc2	konzervatoř
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
získal	získat	k5eAaPmAgMnS	získat
angažmá	angažmá	k1gNnSc3	angažmá
v	v	k7c6	v
baletním	baletní	k2eAgInSc6d1	baletní
souboru	soubor	k1gInSc6	soubor
ND	ND	kA	ND
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
