<s>
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Harapes	Harapes	k1gMnSc1	Harapes
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1946	[number]	k4	1946
Droužkovice	Droužkovice	k1gFnSc1	Droužkovice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
tanečník	tanečník	k1gMnSc1	tanečník
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
choreograf	choreograf	k1gMnSc1	choreograf
<g/>
,	,	kIx,	,
taneční	taneční	k2eAgMnSc1d1	taneční
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
člen	člen	k1gMnSc1	člen
<g/>
,	,	kIx,	,
sólista	sólista	k1gMnSc1	sólista
baletu	balet	k1gInSc2	balet
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
umělecký	umělecký	k2eAgMnSc1d1	umělecký
ředitel	ředitel	k1gMnSc1	ředitel
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
konzervatoře	konzervatoř	k1gFnSc2	konzervatoř
Praha	Praha	k1gFnSc1	Praha
Po	po	k7c6	po
absolutoriu	absolutorium	k1gNnSc6	absolutorium
pražské	pražský	k2eAgFnSc2d1	Pražská
taneční	taneční	k2eAgFnSc2d1	taneční
konzervatoře	konzervatoř	k1gFnSc2	konzervatoř
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
získal	získat	k5eAaPmAgMnS	získat
angažmá	angažmá	k1gNnSc3	angažmá
v	v	k7c6	v
baletním	baletní	k2eAgInSc6d1	baletní
souboru	soubor	k1gInSc6	soubor
ND	ND	kA	ND
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgMnS	být
sólistou	sólista	k1gMnSc7	sólista
.	.	kIx.	.
</s>
<s>
Účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c4	v
50	[number]	k4	50
různých	různý	k2eAgFnPc2d1	různá
tanečních	taneční	k1gFnPc2	taneční
představeních	představení	k1gNnPc6	představení
naší	náš	k3xOp1gFnSc2	náš
první	první	k4xOgFnSc2	první
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Tanec	tanec	k1gInSc1	tanec
také	také	k9	také
studoval	studovat	k5eAaImAgInS	studovat
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
v	v	k7c6	v
baletní	baletní	k2eAgFnSc6d1	baletní
škole	škola	k1gFnSc6	škola
Malého	malé	k1gNnSc2	malé
akademického	akademický	k2eAgNnSc2d1	akademické
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Tančil	tančit	k5eAaImAgMnS	tančit
také	také	k9	také
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
světových	světový	k2eAgFnPc2d1	světová
baletních	baletní	k2eAgFnPc2d1	baletní
scén	scéna	k1gFnPc2	scéna
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
byl	být	k5eAaImAgInS	být
stálým	stálý	k2eAgMnSc7d1	stálý
hostem	host	k1gMnSc7	host
v	v	k7c6	v
západoněmecké	západoněmecký	k2eAgFnSc6d1	západoněmecká
porýnské	porýnský	k2eAgFnSc6d1	porýnská
opeře	opera	k1gFnSc6	opera
v	v	k7c6	v
Duisburgu	Duisburg	k1gInSc6	Duisburg
a	a	k8xC	a
Düsseldorfu	Düsseldorf	k1gInSc6	Düsseldorf
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
působil	působit	k5eAaImAgMnS	působit
jakožto	jakožto	k8xS	jakožto
choreograf	choreograf	k1gMnSc1	choreograf
a	a	k8xC	a
ředitel	ředitel	k1gMnSc1	ředitel
baletního	baletní	k2eAgInSc2d1	baletní
souboru	soubor	k1gInSc2	soubor
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
zde	zde	k6eAd1	zde
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
roli	role	k1gFnSc6	role
choreografa	choreograf	k1gMnSc4	choreograf
a	a	k8xC	a
baletního	baletní	k2eAgMnSc4d1	baletní
mistra	mistr	k1gMnSc4	mistr
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
také	také	k9	také
hostuje	hostovat	k5eAaImIp3nS	hostovat
v	v	k7c6	v
libereckém	liberecký	k2eAgNnSc6d1	liberecké
Divadle	divadlo	k1gNnSc6	divadlo
F.	F.	kA	F.
X.	X.	kA	X.
Šaldy	Šalda	k1gMnSc2	Šalda
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
také	také	k9	také
o	o	k7c4	o
úspěšného	úspěšný	k2eAgMnSc4d1	úspěšný
filmového	filmový	k2eAgMnSc4d1	filmový
herce	herec	k1gMnSc4	herec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
českých	český	k2eAgInPc2d1	český
snímků	snímek	k1gInPc2	snímek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Markéta	Markéta	k1gFnSc1	Markéta
Lazarová	Lazarová	k1gFnSc1	Lazarová
režiséra	režisér	k1gMnSc2	režisér
Františka	František	k1gMnSc2	František
Vláčila	vláčit	k5eAaImAgFnS	vláčit
nebo	nebo	k8xC	nebo
Jak	jak	k6eAd1	jak
vytrhnout	vytrhnout	k5eAaPmF	vytrhnout
velrybě	velryba	k1gFnSc3	velryba
stoličku	stolička	k1gFnSc4	stolička
režisérky	režisérka	k1gFnSc2	režisérka
Marie	Maria	k1gFnSc2	Maria
Poledňákové	Poledňáková	k1gFnSc2	Poledňáková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
však	však	k9	však
často	často	k6eAd1	často
předabován	předabovat	k5eAaPmNgInS	předabovat
jinými	jiný	k2eAgInPc7d1	jiný
herci	herc	k1gInPc7	herc
či	či	k8xC	či
dabéry	dabér	k1gInPc7	dabér
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Klem	Klema	k1gFnPc2	Klema
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Němec	Němec	k1gMnSc1	Němec
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Zahajský	Zahajský	k2eAgMnSc1d1	Zahajský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
čestným	čestný	k2eAgMnSc7d1	čestný
předsedou	předseda	k1gMnSc7	předseda
Hnutí	hnutí	k1gNnSc2	hnutí
speciálních	speciální	k2eAgFnPc2d1	speciální
olympiád	olympiáda	k1gFnPc2	olympiáda
<g/>
,	,	kIx,	,
členem	člen	k1gMnSc7	člen
rady	rada	k1gMnSc2	rada
Kiliánovy	Kiliánův	k2eAgFnSc2d1	Kiliánova
nadace	nadace	k1gFnSc2	nadace
<g/>
,	,	kIx,	,
čestným	čestný	k2eAgMnSc7d1	čestný
presidentem	president	k1gMnSc7	president
občanského	občanský	k2eAgNnSc2d1	občanské
sdružení	sdružení	k1gNnSc2	sdružení
Balet	balet	k1gInSc1	balet
Globa	Globa	k1gFnSc1	Globa
<g/>
.	.	kIx.	.
</s>
<s>
Klasický	klasický	k2eAgInSc1d1	klasický
tanec	tanec	k1gInSc1	tanec
také	také	k9	také
vyučoval	vyučovat	k5eAaImAgInS	vyučovat
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
na	na	k7c6	na
Taneční	taneční	k2eAgFnSc6d1	taneční
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
zasedá	zasedat	k5eAaImIp3nS	zasedat
jakožto	jakožto	k8xS	jakožto
porotce	porotce	k1gMnSc1	porotce
v	v	k7c6	v
renomovaných	renomovaný	k2eAgFnPc6d1	renomovaná
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
tanečních	taneční	k2eAgFnPc6d1	taneční
soutěžích	soutěž	k1gFnPc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Vidět	vidět	k5eAaImF	vidět
jsme	být	k5eAaImIp1nP	být
jej	on	k3xPp3gMnSc4	on
mohli	moct	k5eAaImAgMnP	moct
i	i	k9	i
jako	jako	k9	jako
porotce	porotce	k1gMnSc1	porotce
v	v	k7c6	v
taneční	taneční	k2eAgFnSc6d1	taneční
soutěži	soutěž	k1gFnSc6	soutěž
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Staance	Staance	k1gFnSc2	Staance
...	...	k?	...
<g/>
když	když	k8xS	když
hvězdy	hvězda	k1gFnPc1	hvězda
tančí	tančit	k5eAaImIp3nP	tančit
a	a	k8xC	a
ve	v	k7c6	v
slovenské	slovenský	k2eAgFnSc6d1	slovenská
soutěži	soutěž	k1gFnSc6	soutěž
Hvězdy	hvězda	k1gFnSc2	hvězda
na	na	k7c6	na
ledě	led	k1gInSc6	led
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
podepsal	podepsat	k5eAaPmAgMnS	podepsat
Antichartu	anticharta	k1gFnSc4	anticharta
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
PČR	PČR	kA	PČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
jako	jako	k9	jako
nestraník	nestraník	k1gMnSc1	nestraník
za	za	k7c2	za
hnutí	hnutí	k1gNnSc2	hnutí
ANO	ano	k9	ano
2011	[number]	k4	2011
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
č.	č.	k?	č.
25	[number]	k4	25
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
13,30	[number]	k4	13,30
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
nepostoupil	postoupit	k5eNaPmAgInS	postoupit
<g/>
.	.	kIx.	.
1976	[number]	k4	1976
titul	titul	k1gInSc1	titul
zasloužilý	zasloužilý	k2eAgMnSc1d1	zasloužilý
umělec	umělec	k1gMnSc1	umělec
1980	[number]	k4	1980
ocenění	ocenění	k1gNnPc2	ocenění
Zasloužilý	zasloužilý	k2eAgInSc1d1	zasloužilý
člen	člen	k1gInSc1	člen
ND	ND	kA	ND
1989	[number]	k4	1989
titul	titul	k1gInSc4	titul
národní	národní	k2eAgMnSc1d1	národní
umělec	umělec	k1gMnSc1	umělec
2011	[number]	k4	2011
Cena	cena	k1gFnSc1	cena
Thálie	Thálie	k1gFnSc2	Thálie
za	za	k7c4	za
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
mistrovství	mistrovství	k1gNnSc4	mistrovství
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
balet	balet	k1gInSc1	balet
1964	[number]	k4	1964
Starci	stařec	k1gMnPc7	stařec
na	na	k7c6	na
chmelu	chmel	k1gInSc6	chmel
1965	[number]	k4	1965
Odcházeti	odcházet	k5eAaImF	odcházet
s	s	k7c7	s
podzimem	podzim	k1gInSc7	podzim
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
1967	[number]	k4	1967
Marketa	Market	k2eAgFnSc1d1	Marketa
Lazarová	Lazarová	k1gFnSc1	Lazarová
(	(	kIx(	(
<g/>
německý	německý	k2eAgInSc1d1	německý
hlas	hlas	k1gInSc1	hlas
propůjčil	propůjčit	k5eAaPmAgInS	propůjčit
Klaus-Peter	Klaus-Peter	k1gInSc4	Klaus-Peter
Thiele	Thiel	k1gInSc2	Thiel
<g/>
)	)	kIx)	)
1968	[number]	k4	1968
Královský	královský	k2eAgInSc4d1	královský
omyl	omyl	k1gInSc4	omyl
1971	[number]	k4	1971
F.	F.	kA	F.
<g/>
L.	L.	kA	L.
<g/>
Věk	věk	k1gInSc1	věk
(	(	kIx(	(
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
1976	[number]	k4	1976
Den	den	k1gInSc1	den
pro	pro	k7c4	pro
mou	můj	k3xOp1gFnSc4	můj
lásku	láska	k1gFnSc4	láska
(	(	kIx(	(
<g/>
hlas	hlas	k1gInSc1	hlas
propůjčil	propůjčit	k5eAaPmAgMnS	propůjčit
František	František	k1gMnSc1	František
Němec	Němec	k1gMnSc1	Němec
<g/>
)	)	kIx)	)
1977	[number]	k4	1977
Jak	jak	k6eAd1	jak
vytrhnout	vytrhnout	k5eAaPmF	vytrhnout
velrybě	velryba	k1gFnSc3	velryba
stoličku	stolička	k1gFnSc4	stolička
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
hlas	hlas	k1gInSc4	hlas
propůjčil	propůjčit	k5eAaPmAgMnS	propůjčit
Jiří	Jiří	k1gMnSc1	Jiří
Klem	Klema	k1gFnPc2	Klema
<g/>
)	)	kIx)	)
1978	[number]	k4	1978
Jak	jak	k6eAd1	jak
dostat	dostat	k5eAaPmF	dostat
tatínka	tatínek	k1gMnSc4	tatínek
do	do	k7c2	do
polepšovny	polepšovna	k1gFnSc2	polepšovna
(	(	kIx(	(
<g/>
hlas	hlas	k1gInSc1	hlas
propůjčil	propůjčit	k5eAaPmAgMnS	propůjčit
Jiří	Jiří	k1gMnSc1	Jiří
Klem	Klema	k1gFnPc2	Klema
<g/>
)	)	kIx)	)
1978	[number]	k4	1978
Panna	Panna	k1gFnSc1	Panna
a	a	k8xC	a
netvor	netvor	k1gMnSc1	netvor
(	(	kIx(	(
<g/>
hlas	hlas	k1gInSc1	hlas
propůjčil	propůjčit	k5eAaPmAgMnS	propůjčit
Jiří	Jiří	k1gMnSc1	Jiří
Zahajský	Zahajský	k2eAgMnSc1d1	Zahajský
<g/>
)	)	kIx)	)
1986	[number]	k4	1986
Operace	operace	k1gFnSc1	operace
mé	můj	k3xOp1gFnPc4	můj
dcery	dcera	k1gFnPc4	dcera
2004	[number]	k4	2004
Bolero	bolero	k1gNnSc4	bolero
2004	[number]	k4	2004
Rodinná	rodinný	k2eAgNnPc4d1	rodinné
pouta	pouto	k1gNnPc4	pouto
(	(	kIx(	(
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
2006	[number]	k4	2006
Jak	jak	k8xC	jak
se	se	k3xPyFc4	se
krotí	krotit	k5eAaImIp3nP	krotit
krokodýli	krokodýl	k1gMnPc1	krokodýl
2008	[number]	k4	2008
Comeback	Comebacko	k1gNnPc2	Comebacko
(	(	kIx(	(
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
</s>
