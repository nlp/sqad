<s>
Podle	podle	k7c2	podle
židovské	židovský	k2eAgFnSc2d1	židovská
tradice	tradice	k1gFnSc2	tradice
byly	být	k5eAaImAgFnP	být
knihy	kniha	k1gFnPc1	kniha
Tóry	tóra	k1gFnSc2	tóra
věnovány	věnovat	k5eAaPmNgFnP	věnovat
Bohem	bůh	k1gMnSc7	bůh
Mojžíšovi	Mojžíš	k1gMnSc6	Mojžíš
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1220	[number]	k4	1220
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
exodu	exodus	k1gInSc6	exodus
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
