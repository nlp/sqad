<p>
<s>
Moskevská	moskevský	k2eAgFnSc1d1	Moskevská
státní	státní	k2eAgFnSc1d1	státní
univerzita	univerzita	k1gFnSc1	univerzita
M.	M.	kA	M.
V.	V.	kA	V.
Lomonosova	Lomonosův	k2eAgFnSc1d1	Lomonosova
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
М	М	k?	М
г	г	k?	г
у	у	k?	у
и	и	k?	и
М	М	k?	М
В	В	k?	В
Л	Л	k?	Л
<g/>
;	;	kIx,	;
Moskovskij	Moskovskij	k1gMnSc1	Moskovskij
gosudarstvennyj	gosudarstvennyj	k1gMnSc1	gosudarstvennyj
universitet	universitet	k1gInSc4	universitet
imeni	imeň	k1gMnSc3	imeň
M.	M.	kA	M.
V.	V.	kA	V.
Lomonosova	Lomonosův	k2eAgFnSc1d1	Lomonosova
<g/>
;	;	kIx,	;
zkratka	zkratka	k1gFnSc1	zkratka
MGU	MGU	kA	MGU
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Moscow	Moscow	k1gFnSc1	Moscow
State	status	k1gInSc5	status
University	universita	k1gFnSc2	universita
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
a	a	k8xC	a
největší	veliký	k2eAgFnSc1d3	veliký
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Imperátorská	imperátorský	k2eAgFnSc1d1	imperátorská
moskevská	moskevský	k2eAgFnSc1d1	Moskevská
univerzita	univerzita	k1gFnSc1	univerzita
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1755	[number]	k4	1755
na	na	k7c4	na
popud	popud	k1gInSc4	popud
carevny	carevna	k1gFnSc2	carevna
Alžběty	Alžběta	k1gFnSc2	Alžběta
Petrovny	Petrovna	k1gFnSc2	Petrovna
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
založení	založení	k1gNnSc1	založení
mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgMnPc7d1	jiný
inicioval	iniciovat	k5eAaBmAgInS	iniciovat
a	a	k8xC	a
značnou	značný	k2eAgFnSc7d1	značná
měrou	míra	k1gFnSc7wR	míra
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
participoval	participovat	k5eAaImAgMnS	participovat
ruský	ruský	k2eAgMnSc1d1	ruský
vědec	vědec	k1gMnSc1	vědec
<g/>
,	,	kIx,	,
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
a	a	k8xC	a
polyhistor	polyhistor	k1gMnSc1	polyhistor
Michail	Michail	k1gMnSc1	Michail
Vasiljevič	Vasiljevič	k1gMnSc1	Vasiljevič
Lomonosov	Lomonosov	k1gInSc4	Lomonosov
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
také	také	k9	také
jejím	její	k3xOp3gInPc3	její
prvním	první	k4xOgMnSc7	první
rektorem	rektor	k1gMnSc7	rektor
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c4	v
Götingen	Götingen	k1gInSc4	Götingen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
vychovávat	vychovávat	k5eAaImF	vychovávat
především	především	k9	především
mládež	mládež	k1gFnSc4	mládež
neurozeného	urozený	k2eNgInSc2d1	neurozený
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
tvořily	tvořit	k5eAaImAgFnP	tvořit
původně	původně	k6eAd1	původně
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
–	–	k?	–
filosofie	filosofie	k1gFnSc1	filosofie
<g/>
,	,	kIx,	,
práva	právo	k1gNnPc1	právo
a	a	k8xC	a
medicina	medicin	k2eAgFnSc1d1	medicina
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
univerzita	univerzita	k1gFnSc1	univerzita
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
vysoké	vysoký	k2eAgFnPc4d1	vysoká
úrovně	úroveň	k1gFnPc4	úroveň
především	především	k9	především
v	v	k7c6	v
přírodních	přírodní	k2eAgFnPc6d1	přírodní
vědách	věda	k1gFnPc6	věda
a	a	k8xC	a
matematice	matematika	k1gFnSc6	matematika
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejích	její	k3xOp3gInPc6	její
vědeckých	vědecký	k2eAgInPc6d1	vědecký
i	i	k8xC	i
výukových	výukový	k2eAgInPc6d1	výukový
programech	program	k1gInPc6	program
dominovaly	dominovat	k5eAaImAgFnP	dominovat
ideje	idea	k1gFnPc1	idea
osvícenství	osvícenství	k1gNnSc2	osvícenství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1804	[number]	k4	1804
nový	nový	k2eAgInSc1d1	nový
statut	statut	k1gInSc1	statut
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
autonomii	autonomie	k1gFnSc4	autonomie
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
;	;	kIx,	;
tu	tu	k6eAd1	tu
následně	následně	k6eAd1	následně
car	car	k1gMnSc1	car
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
I.	I.	kA	I.
omezil	omezit	k5eAaPmAgMnS	omezit
<g/>
,	,	kIx,	,
volnomyšlenkářské	volnomyšlenkářský	k2eAgFnPc1d1	volnomyšlenkářská
ideje	idea	k1gFnPc1	idea
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
však	však	k9	však
zůstaly	zůstat	k5eAaPmAgInP	zůstat
stále	stále	k6eAd1	stále
živé	živý	k2eAgInPc1d1	živý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
byl	být	k5eAaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
nový	nový	k2eAgInSc1d1	nový
statut	statut	k1gInSc1	statut
výuky	výuka	k1gFnSc2	výuka
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
bylo	být	k5eAaImAgNnS	být
jednak	jednak	k8xC	jednak
rozšířeno	rozšířen	k2eAgNnSc4d1	rozšířeno
spektrum	spektrum	k1gNnSc4	spektrum
vědeckých	vědecký	k2eAgInPc2d1	vědecký
výzkumů	výzkum	k1gInPc2	výzkum
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
změněn	změněn	k2eAgInSc1d1	změněn
systém	systém	k1gInSc1	systém
vedení	vedení	k1gNnSc2	vedení
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Reforma	reforma	k1gFnSc1	reforma
cara	car	k1gMnSc2	car
Alexandra	Alexandr	k1gMnSc2	Alexandr
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
opět	opět	k6eAd1	opět
universitě	universita	k1gFnSc3	universita
odebrala	odebrat	k5eAaPmAgFnS	odebrat
autonomii	autonomie	k1gFnSc4	autonomie
výuky	výuka	k1gFnSc2	výuka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
se	se	k3xPyFc4	se
universita	universita	k1gFnSc1	universita
stala	stát	k5eAaPmAgFnS	stát
státní	státní	k2eAgFnSc7d1	státní
školou	škola	k1gFnSc7	škola
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
podrobena	podrobit	k5eAaPmNgFnS	podrobit
reorganizaci	reorganizace	k1gFnSc3	reorganizace
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
na	na	k7c4	na
Moskevská	moskevský	k2eAgFnSc1d1	Moskevská
státní	státní	k2eAgFnSc1d1	státní
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
;	;	kIx,	;
později	pozdě	k6eAd2	pozdě
ještě	ještě	k9	ještě
několikrát	několikrát	k6eAd1	několikrát
byla	být	k5eAaImAgFnS	být
instituce	instituce	k1gFnSc1	instituce
přejmenována	přejmenován	k2eAgFnSc1d1	přejmenována
<g/>
:	:	kIx,	:
např.	např.	kA	např.
v	v	k7c6	v
letech	let	k1gInPc6	let
1932	[number]	k4	1932
<g/>
–	–	k?	–
<g/>
37	[number]	k4	37
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
Moskevská	moskevský	k2eAgFnSc1d1	Moskevská
státní	státní	k2eAgFnSc1d1	státní
univerzita	univerzita	k1gFnSc1	univerzita
M.	M.	kA	M.
N.	N.	kA	N.
Pokrovského	Pokrovský	k2eAgInSc2d1	Pokrovský
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
známého	známý	k2eAgMnSc2d1	známý
marxistického	marxistický	k2eAgMnSc2d1	marxistický
historika	historik	k1gMnSc2	historik
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
přijatý	přijatý	k2eAgInSc1d1	přijatý
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
slavného	slavný	k2eAgMnSc4d1	slavný
Lomonosova	Lomonosův	k2eAgMnSc4d1	Lomonosův
<g/>
,	,	kIx,	,
univerzita	univerzita	k1gFnSc1	univerzita
obdržela	obdržet	k5eAaPmAgFnS	obdržet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
roky	rok	k1gInPc7	rok
1955	[number]	k4	1955
<g/>
–	–	k?	–
<g/>
91	[number]	k4	91
ještě	ještě	k6eAd1	ještě
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
o	o	k7c4	o
čestné	čestný	k2eAgNnSc4d1	čestné
označení	označení	k1gNnSc4	označení
nositelka	nositelka	k1gFnSc1	nositelka
Leninova	Leninův	k2eAgInSc2d1	Leninův
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
Řádu	řád	k1gInSc2	řád
rudého	rudý	k2eAgInSc2d1	rudý
praporu	prapor	k1gInSc2	prapor
práce	práce	k1gFnSc2	práce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
do	do	k7c2	do
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
existence	existence	k1gFnSc1	existence
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
život	život	k1gInSc1	život
zcela	zcela	k6eAd1	zcela
podřízen	podřízen	k2eAgInSc1d1	podřízen
bolševické	bolševický	k2eAgFnSc3d1	bolševická
ideologii	ideologie	k1gFnSc3	ideologie
a	a	k8xC	a
politice	politika	k1gFnSc3	politika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
epoše	epocha	k1gFnSc6	epocha
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
společenského	společenský	k2eAgNnSc2d1	společenské
"	"	kIx"	"
<g/>
tání	tání	k1gNnSc2	tání
<g/>
"	"	kIx"	"
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
–	–	k?	–
byť	byť	k8xS	byť
omezená	omezený	k2eAgFnSc1d1	omezená
–	–	k?	–
liberalizace	liberalizace	k1gFnSc1	liberalizace
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
výuky	výuka	k1gFnSc2	výuka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
a	a	k8xC	a
konci	konec	k1gInSc3	konec
vlády	vláda	k1gFnSc2	vláda
komunistů	komunista	k1gMnPc2	komunista
<g/>
,	,	kIx,	,
vešel	vejít	k5eAaPmAgInS	vejít
v	v	k7c4	v
život	život	k1gInSc4	život
nový	nový	k2eAgInSc4d1	nový
statut	statut	k1gInSc4	statut
zajišťující	zajišťující	k2eAgInSc4d1	zajišťující
univerzitě	univerzita	k1gFnSc3	univerzita
plnou	plný	k2eAgFnSc4d1	plná
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
má	mít	k5eAaImIp3nS	mít
univerzita	univerzita	k1gFnSc1	univerzita
40	[number]	k4	40
fakult	fakulta	k1gFnPc2	fakulta
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
300	[number]	k4	300
kateder	katedra	k1gFnPc2	katedra
<g/>
,	,	kIx,	,
360	[number]	k4	360
laboratoří	laboratoř	k1gFnPc2	laboratoř
<g/>
,	,	kIx,	,
163	[number]	k4	163
studijních	studijní	k2eAgInPc2d1	studijní
kabinetů	kabinet	k1gInPc2	kabinet
a	a	k8xC	a
9	[number]	k4	9
vědeckovýzkumných	vědeckovýzkumný	k2eAgInPc2d1	vědeckovýzkumný
ústavů	ústav	k1gInPc2	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Rektorem	rektor	k1gMnSc7	rektor
je	být	k5eAaImIp3nS	být
matematik	matematik	k1gMnSc1	matematik
Viktor	Viktor	k1gMnSc1	Viktor
Antonovič	Antonovič	k1gMnSc1	Antonovič
Sadovničij	Sadovničij	k1gMnSc1	Sadovničij
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
měla	mít	k5eAaImAgFnS	mít
univerzita	univerzita	k1gFnSc1	univerzita
na	na	k7c4	na
32	[number]	k4	32
500	[number]	k4	500
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
4	[number]	k4	4
500	[number]	k4	500
doktorandů	doktorand	k1gMnPc2	doktorand
<g/>
,	,	kIx,	,
na	na	k7c4	na
10	[number]	k4	10
000	[number]	k4	000
posluchačů	posluchač	k1gMnPc2	posluchač
přípravného	přípravný	k2eAgNnSc2d1	přípravné
studia	studio	k1gNnSc2	studio
–	–	k?	–
dohromady	dohromady	k6eAd1	dohromady
na	na	k7c4	na
47	[number]	k4	47
000	[number]	k4	000
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
vzdělávalo	vzdělávat	k5eAaImAgNnS	vzdělávat
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
externích	externí	k2eAgInPc2d1	externí
<g/>
)	)	kIx)	)
na	na	k7c6	na
9	[number]	k4	9
000	[number]	k4	000
vysokoškolských	vysokoškolský	k2eAgMnPc2d1	vysokoškolský
pedagogických	pedagogický	k2eAgMnPc2d1	pedagogický
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
,	,	kIx,	,
dalších	další	k2eAgInPc2d1	další
cca	cca	kA	cca
15	[number]	k4	15
000	[number]	k4	000
osob	osoba	k1gFnPc2	osoba
pomocného	pomocný	k2eAgInSc2d1	pomocný
a	a	k8xC	a
obslužného	obslužný	k2eAgInSc2d1	obslužný
personálu	personál	k1gInSc2	personál
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
chod	chod	k1gInSc4	chod
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Lomonosovova	Lomonosovův	k2eAgFnSc1d1	Lomonosovova
univerzita	univerzita	k1gFnSc1	univerzita
pořád	pořád	k6eAd1	pořád
dominuje	dominovat	k5eAaImIp3nS	dominovat
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
podle	podle	k7c2	podle
agentury	agentura	k1gFnSc2	agentura
"	"	kIx"	"
<g/>
Expert	expert	k1gMnSc1	expert
RA	ra	k0	ra
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celkem	celkem	k6eAd1	celkem
11	[number]	k4	11
absolventů	absolvent	k1gMnPc2	absolvent
školy	škola	k1gFnSc2	škola
získalo	získat	k5eAaPmAgNnS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
(	(	kIx(	(
<g/>
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
18	[number]	k4	18
ruských	ruský	k2eAgMnPc2d1	ruský
vědců	vědec	k1gMnPc2	vědec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
cenu	cena	k1gFnSc4	cena
získali	získat	k5eAaPmAgMnP	získat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Budovy	budova	k1gFnPc4	budova
==	==	k?	==
</s>
</p>
<p>
<s>
Původním	původní	k2eAgNnSc7d1	původní
sídlem	sídlo	k1gNnSc7	sídlo
univerzity	univerzita	k1gFnSc2	univerzita
byla	být	k5eAaImAgFnS	být
budova	budova	k1gFnSc1	budova
na	na	k7c6	na
Rudém	rudý	k2eAgNnSc6d1	Rudé
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
Historického	historický	k2eAgNnSc2d1	historické
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
letech	let	k1gInPc6	let
1786	[number]	k4	1786
<g/>
–	–	k?	–
<g/>
1793	[number]	k4	1793
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
průsečíku	průsečík	k1gInSc6	průsečík
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Gercenovy	Gercenův	k2eAgFnSc2d1	Gercenova
ulice	ulice	k1gFnSc2	ulice
a	a	k8xC	a
Marxova	Marxův	k2eAgInSc2d1	Marxův
prospektu	prospekt	k1gInSc2	prospekt
nová	nový	k2eAgFnSc1d1	nová
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
budovaná	budovaný	k2eAgFnSc1d1	budovaná
v	v	k7c6	v
klasicistním	klasicistní	k2eAgInSc6d1	klasicistní
stylu	styl	k1gInSc6	styl
již	již	k6eAd1	již
jako	jako	k8xC	jako
objekt	objekt	k1gInSc4	objekt
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1812	[number]	k4	1812
při	při	k7c6	při
požáru	požár	k1gInSc6	požár
Moskvy	Moskva	k1gFnSc2	Moskva
za	za	k7c2	za
války	válka	k1gFnSc2	válka
s	s	k7c7	s
Napoleonem	napoleon	k1gInSc7	napoleon
vyhořela	vyhořet	k5eAaPmAgFnS	vyhořet
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1817	[number]	k4	1817
<g/>
–	–	k?	–
<g/>
1819	[number]	k4	1819
byla	být	k5eAaImAgFnS	být
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
typický	typický	k2eAgInSc4d1	typický
příklad	příklad	k1gInSc4	příklad
ruského	ruský	k2eAgInSc2d1	ruský
empírového	empírový	k2eAgInSc2d1	empírový
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1833	[number]	k4	1833
<g/>
–	–	k?	–
<g/>
1836	[number]	k4	1836
byly	být	k5eAaImAgFnP	být
vybudovány	vybudovat	k5eAaPmNgInP	vybudovat
nové	nový	k2eAgInPc1d1	nový
objekty	objekt	k1gInPc1	objekt
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
původní	původní	k2eAgFnSc2d1	původní
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
komplex	komplex	k1gInSc4	komplex
doplňoval	doplňovat	k5eAaImAgInS	doplňovat
univerzitní	univerzitní	k2eAgInSc1d1	univerzitní
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
přeměněný	přeměněný	k2eAgInSc1d1	přeměněný
na	na	k7c4	na
kulturní	kulturní	k2eAgInSc4d1	kulturní
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
novou	nový	k2eAgFnSc7d1	nová
univerzitní	univerzitní	k2eAgFnSc7d1	univerzitní
budovou	budova	k1gFnSc7	budova
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
Lomonosovův	Lomonosovův	k2eAgInSc1d1	Lomonosovův
pomník	pomník	k1gInSc1	pomník
<g/>
.	.	kIx.	.
</s>
<s>
Rotunda	rotunda	k1gFnSc1	rotunda
kryjící	kryjící	k2eAgNnSc4d1	kryjící
jedno	jeden	k4xCgNnSc4	jeden
křídlo	křídlo	k1gNnSc4	křídlo
stavby	stavba	k1gFnSc2	stavba
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
univerzitní	univerzitní	k2eAgFnSc2d1	univerzitní
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
centrální	centrální	k2eAgFnSc7d1	centrální
částí	část	k1gFnSc7	část
budovy	budova	k1gFnSc2	budova
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
pomníky	pomník	k1gInPc1	pomník
revolucionářů	revolucionář	k1gMnPc2	revolucionář
Alexandra	Alexandr	k1gMnSc4	Alexandr
Gercena	Gercen	k2eAgMnSc4d1	Gercen
a	a	k8xC	a
Nikolaje	Nikolaj	k1gMnSc4	Nikolaj
Ogareva	Ogarev	k1gMnSc4	Ogarev
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
studovali	studovat	k5eAaImAgMnP	studovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1948	[number]	k4	1948
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
získala	získat	k5eAaPmAgFnS	získat
univerzita	univerzita	k1gFnSc1	univerzita
zcela	zcela	k6eAd1	zcela
novou	nový	k2eAgFnSc4d1	nová
lokalizaci	lokalizace	k1gFnSc4	lokalizace
i	i	k8xC	i
nový	nový	k2eAgInSc4d1	nový
komplex	komplex	k1gInSc4	komplex
budov	budova	k1gFnPc2	budova
(	(	kIx(	(
<g/>
kampus	kampus	k1gInSc1	kampus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
Leninských	leninský	k2eAgFnPc6d1	leninská
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
tu	tu	k6eAd1	tu
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
na	na	k7c4	na
320	[number]	k4	320
hektarech	hektar	k1gInPc6	hektar
na	na	k7c4	na
30	[number]	k4	30
hlavních	hlavní	k2eAgFnPc2d1	hlavní
univerzitních	univerzitní	k2eAgFnPc2d1	univerzitní
budov	budova	k1gFnPc2	budova
a	a	k8xC	a
20	[number]	k4	20
budov	budova	k1gFnPc2	budova
obslužných	obslužný	k2eAgFnPc2d1	obslužná
<g/>
.	.	kIx.	.
</s>
<s>
Založen	založit	k5eAaPmNgInS	založit
byl	být	k5eAaImAgInS	být
též	též	k9	též
park	park	k1gInSc1	park
a	a	k8xC	a
sportovní	sportovní	k2eAgInSc1d1	sportovní
areál	areál	k1gInSc1	areál
<g/>
,	,	kIx,	,
botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
a	a	k8xC	a
astronomická	astronomický	k2eAgFnSc1d1	astronomická
observatoř	observatoř	k1gFnSc1	observatoř
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
budova	budova	k1gFnSc1	budova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
stala	stát	k5eAaPmAgFnS	stát
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
symbolů	symbol	k1gInPc2	symbol
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vystavěna	vystavět	k5eAaPmNgFnS	vystavět
v	v	k7c6	v
letech	let	k1gInPc6	let
1949	[number]	k4	1949
<g/>
–	–	k?	–
<g/>
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lomonosovova	Lomonosovův	k2eAgFnSc1d1	Lomonosovova
univerzita	univerzita	k1gFnSc1	univerzita
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
