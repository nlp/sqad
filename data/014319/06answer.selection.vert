<s desamb="1">
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
se	se	k3xPyFc4
dlouhodobě	dlouhodobě	k6eAd1
umísťuje	umísťovat	k5eAaImIp3nS
v	v	k7c6
žebříčku	žebříček	k1gInSc6
nejlepších	dobrý	k2eAgFnPc2d3
světových	světový	k2eAgFnPc2d1
univerzit	univerzita	k1gFnPc2
QS	QS	kA
TopUniversities	TopUniversities	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
roku	rok	k1gInSc2
2019	#num#	k4
je	být	k5eAaImIp3nS
jejím	její	k3xOp3gMnSc7
rektorem	rektor	k1gMnSc7
Martin	Martin	k1gMnSc1
Bareš	Bareš	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>