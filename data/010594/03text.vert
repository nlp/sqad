<p>
<s>
Franz	Franz	k1gMnSc1	Franz
Liszt	Liszt	k1gMnSc1	Liszt
(	(	kIx(	(
<g/>
křtěn	křtěn	k2eAgInSc1d1	křtěn
Franciscus	Franciscus	k1gInSc1	Franciscus
<g/>
,	,	kIx,	,
v	v	k7c6	v
maďarštině	maďarština	k1gFnSc6	maďarština
používané	používaný	k2eAgNnSc4d1	používané
jméno	jméno	k1gNnSc4	jméno
Ferenc	Ferenc	k1gMnSc1	Ferenc
<g/>
;	;	kIx,	;
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1811	[number]	k4	1811
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Raiding	Raiding	k1gInSc1	Raiding
/	/	kIx~	/
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
)	)	kIx)	)
Doborján	Doborján	k1gInSc1	Doborján
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
Rakouské	rakouský	k2eAgNnSc4d1	rakouské
císařství	císařství	k1gNnSc4	císařství
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Rakousko	Rakousko	k1gNnSc1	Rakousko
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1886	[number]	k4	1886
Bayreuth	Bayreutha	k1gFnPc2	Bayreutha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
rakousko-uherský	rakouskoherský	k2eAgMnSc1d1	rakousko-uherský
klavírní	klavírní	k2eAgMnSc1d1	klavírní
virtuos	virtuos	k1gMnSc1	virtuos
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Liszt	Liszt	k1gMnSc1	Liszt
byl	být	k5eAaImAgMnS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
klavírních	klavírní	k2eAgMnPc2d1	klavírní
virtuosů	virtuos	k1gMnPc2	virtuos
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejproduktivnějších	produktivní	k2eAgMnPc2d3	nejproduktivnější
hudebních	hudební	k2eAgMnPc2d1	hudební
skladatelů	skladatel	k1gMnPc2	skladatel
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
své	svůj	k3xOyFgFnSc2	svůj
skladatelské	skladatelský	k2eAgFnSc2d1	skladatelská
činnosti	činnost	k1gFnSc2	činnost
komponoval	komponovat	k5eAaImAgMnS	komponovat
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
stylech	styl	k1gInPc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
patřil	patřit	k5eAaImAgInS	patřit
mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgMnPc4d1	hlavní
představitele	představitel	k1gMnPc4	představitel
romantismu	romantismus	k1gInSc2	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgFnPc7	svůj
symfonickými	symfonický	k2eAgFnPc7d1	symfonická
básněmi	báseň	k1gFnPc7	báseň
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
průkopníků	průkopník	k1gMnPc2	průkopník
programní	programní	k2eAgFnSc2d1	programní
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
hlavní	hlavní	k2eAgNnPc1d1	hlavní
díla	dílo	k1gNnPc1	dílo
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
novoněmeckého	novoněmecký	k2eAgInSc2d1	novoněmecký
hudebního	hudební	k2eAgInSc2d1	hudební
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dětství	dětství	k1gNnSc4	dětství
a	a	k8xC	a
mládí	mládí	k1gNnSc4	mládí
===	===	k?	===
</s>
</p>
<p>
<s>
Lisztova	Lisztův	k2eAgFnSc1d1	Lisztova
matka	matka	k1gFnSc1	matka
Anna	Anna	k1gFnSc1	Anna
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Marie	Marie	k1gFnSc1	Marie
Anna	Anna	k1gFnSc1	Anna
Lager	Lager	k1gInSc1	Lager
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1788	[number]	k4	1788
<g/>
,	,	kIx,	,
Kremže	Kremže	k1gFnSc1	Kremže
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1866	[number]	k4	1866
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyrůstala	vyrůstat	k5eAaImAgFnS	vyrůstat
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
devíti	devět	k4xCc6	devět
letech	léto	k1gNnPc6	léto
ztratila	ztratit	k5eAaPmAgFnS	ztratit
rodiče	rodič	k1gMnSc4	rodič
a	a	k8xC	a
musela	muset	k5eAaImAgFnS	muset
se	se	k3xPyFc4	se
přestěhovat	přestěhovat	k5eAaPmF	přestěhovat
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k9	jako
služka	služka	k1gFnSc1	služka
jedenáct	jedenáct	k4xCc4	jedenáct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1811	[number]	k4	1811
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
Slováka	Slovák	k1gMnSc4	Slovák
Adama	Adam	k1gMnSc4	Adam
Lista	lista	k1gFnSc1	lista
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Liszta	Liszta	k1gFnSc1	Liszta
<g/>
)	)	kIx)	)
z	z	k7c2	z
obce	obec	k1gFnSc2	obec
Unterfrauenhaid	Unterfrauenhaida	k1gFnPc2	Unterfrauenhaida
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
Burgenlandsku	Burgenlandsek	k1gInSc6	Burgenlandsek
<g/>
,	,	kIx,	,
v	v	k7c6	v
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
slovenská	slovenský	k2eAgFnSc1d1	slovenská
<g/>
.	.	kIx.	.
</s>
<s>
Lisztův	Lisztův	k2eAgMnSc1d1	Lisztův
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
amatérský	amatérský	k2eAgMnSc1d1	amatérský
pianista	pianista	k1gMnSc1	pianista
a	a	k8xC	a
cellista	cellista	k1gMnSc1	cellista
a	a	k8xC	a
úředník	úředník	k1gMnSc1	úředník
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
knížete	kníže	k1gMnSc2	kníže
Nicholase	Nicholasa	k1gFnSc6	Nicholasa
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Esterházyho	Esterházyze	k6eAd1	Esterházyze
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
příjmení	příjmení	k1gNnSc1	příjmení
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
ve	v	k7c6	v
slovenském	slovenský	k2eAgNnSc6d1	slovenské
znění	znění	k1gNnSc6	znění
List	lista	k1gFnPc2	lista
i	i	k9	i
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
či	či	k8xC	či
maďarské	maďarský	k2eAgFnSc6d1	maďarská
verzi	verze	k1gFnSc6	verze
Liszt	Liszta	k1gFnPc2	Liszta
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
příjmení	příjmení	k1gNnSc4	příjmení
si	se	k3xPyFc3	se
Adam	Adam	k1gMnSc1	Adam
List	lista	k1gFnPc2	lista
začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
jako	jako	k9	jako
Liszt	Liszt	k2eAgMnSc1d1	Liszt
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
maďarštině	maďarština	k1gFnSc6	maďarština
nesprávně	správně	k6eNd1	správně
vyslovováno	vyslovován	k2eAgNnSc1d1	vyslovováno
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
lišt	lišta	k1gFnPc2	lišta
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Franz	Franz	k1gInSc1	Franz
byl	být	k5eAaImAgInS	být
pokřtěn	pokřtít	k5eAaPmNgInS	pokřtít
jako	jako	k9	jako
Franciscus	Franciscus	k1gInSc1	Franciscus
List	list	k1gInSc1	list
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
podepisoval	podepisovat	k5eAaImAgInS	podepisovat
jménem	jméno	k1gNnSc7	jméno
Franz	Franza	k1gFnPc2	Franza
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
slovenština	slovenština	k1gFnSc1	slovenština
tehdy	tehdy	k6eAd1	tehdy
nebyla	být	k5eNaImAgFnS	být
uznaným	uznaný	k2eAgInSc7d1	uznaný
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Liszt	Liszt	k1gMnSc1	Liszt
mluvil	mluvit	k5eAaImAgMnS	mluvit
slovensky	slovensky	k6eAd1	slovensky
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ještě	ještě	k9	ještě
lépe	dobře	k6eAd2	dobře
uměl	umět	k5eAaImAgMnS	umět
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Lisztově	Lisztův	k2eAgFnSc6d1	Lisztova
rodině	rodina	k1gFnSc6	rodina
se	se	k3xPyFc4	se
maďarsky	maďarsky	k6eAd1	maďarsky
nemluvilo	mluvit	k5eNaImAgNnS	mluvit
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
sám	sám	k3xTgMnSc1	sám
Franz	Franz	k1gMnSc1	Franz
tento	tento	k3xDgInSc4	tento
jazyk	jazyk	k1gInSc4	jazyk
nikdy	nikdy	k6eAd1	nikdy
dobře	dobře	k6eAd1	dobře
neovládal	ovládat	k5eNaImAgMnS	ovládat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Franz	Franz	k1gMnSc1	Franz
Liszt	Liszt	k1gMnSc1	Liszt
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
základům	základ	k1gInPc3	základ
klavírní	klavírní	k2eAgFnSc2d1	klavírní
hry	hra	k1gFnSc2	hra
učil	učit	k5eAaImAgInS	učit
již	již	k6eAd1	již
od	od	k7c2	od
šesti	šest	k4xCc2	šest
let	léto	k1gNnPc2	léto
jeho	jeho	k3xOp3gFnSc6	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
projevil	projevit	k5eAaPmAgInS	projevit
záhy	záhy	k6eAd1	záhy
výjimečné	výjimečný	k2eAgNnSc4d1	výjimečné
hudební	hudební	k2eAgNnSc4d1	hudební
nadání	nadání	k1gNnSc4	nadání
<g/>
.	.	kIx.	.
</s>
<s>
Hudbu	hudba	k1gFnSc4	hudba
skládal	skládat	k5eAaImAgMnS	skládat
od	od	k7c2	od
osmi	osm	k4xCc2	osm
let	léto	k1gNnPc2	léto
a	a	k8xC	a
první	první	k4xOgInPc4	první
koncerty	koncert	k1gInPc4	koncert
byl	být	k5eAaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
absolvovat	absolvovat	k5eAaPmF	absolvovat
již	již	k6eAd1	již
v	v	k7c6	v
devíti	devět	k4xCc6	devět
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
finanční	finanční	k2eAgFnSc3d1	finanční
podpoře	podpora	k1gFnSc3	podpora
uherské	uherský	k2eAgFnSc2d1	uherská
šlechty	šlechta	k1gFnSc2	šlechta
mohl	moct	k5eAaImAgInS	moct
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1821	[number]	k4	1821
studovat	studovat	k5eAaImF	studovat
hudbu	hudba	k1gFnSc4	hudba
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
učili	učít	k5eAaPmAgMnP	učít
Carl	Carl	k1gMnSc1	Carl
Czerny	Czerna	k1gFnSc2	Czerna
hře	hra	k1gFnSc6	hra
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
Antonio	Antonio	k1gMnSc1	Antonio
Salieri	Salier	k1gFnSc2	Salier
hudební	hudební	k2eAgFnSc4d1	hudební
kompozici	kompozice	k1gFnSc4	kompozice
<g/>
.	.	kIx.	.
</s>
<s>
Czerny	Czerna	k1gFnPc4	Czerna
předvedl	předvést	k5eAaPmAgInS	předvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1823	[number]	k4	1823
svého	svůj	k3xOyFgMnSc4	svůj
nadaného	nadaný	k2eAgMnSc4d1	nadaný
žáka	žák	k1gMnSc4	žák
Beethovenovi	Beethoven	k1gMnSc6	Beethoven
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
hře	hra	k1gFnSc6	hra
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
pochvalně	pochvalně	k6eAd1	pochvalně
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
odjel	odjet	k5eAaPmAgMnS	odjet
Liszt	Liszt	k1gInSc4	Liszt
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
samostudiu	samostudium	k1gNnSc6	samostudium
klavírní	klavírní	k2eAgFnSc2d1	klavírní
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Skladbu	skladba	k1gFnSc4	skladba
zde	zde	k6eAd1	zde
studoval	studovat	k5eAaImAgMnS	studovat
soukromě	soukromě	k6eAd1	soukromě
u	u	k7c2	u
Antonína	Antonín	k1gMnSc2	Antonín
Rejchy	Rejcha	k1gMnSc2	Rejcha
a	a	k8xC	a
operního	operní	k2eAgMnSc2d1	operní
skladatele	skladatel	k1gMnSc2	skladatel
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Paëra	Paër	k1gMnSc2	Paër
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
poznal	poznat	k5eAaPmAgMnS	poznat
řadu	řada	k1gFnSc4	řada
předních	přední	k2eAgMnPc2d1	přední
literátů	literát	k1gMnPc2	literát
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
Victora	Victor	k1gMnSc2	Victor
Huga	Hugo	k1gMnSc2	Hugo
a	a	k8xC	a
Heinricha	Heinrich	k1gMnSc2	Heinrich
Heina	Hein	k1gMnSc2	Hein
<g/>
.	.	kIx.	.
</s>
<s>
Značný	značný	k2eAgInSc4d1	značný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
měli	mít	k5eAaImAgMnP	mít
básník	básník	k1gMnSc1	básník
Alphonse	Alphonse	k1gFnSc2	Alphonse
de	de	k?	de
Lamartine	Lamartin	k1gInSc5	Lamartin
a	a	k8xC	a
sociální	sociální	k2eAgMnSc1d1	sociální
reformátor	reformátor	k1gMnSc1	reformátor
Félicité	Félicita	k1gMnPc1	Félicita
Robert	Robert	k1gMnSc1	Robert
de	de	k?	de
Lamennais	Lamennais	k1gInSc1	Lamennais
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
pařížského	pařížský	k2eAgNnSc2d1	pařížské
období	období	k1gNnSc2	období
spadají	spadat	k5eAaImIp3nP	spadat
i	i	k9	i
jeho	jeho	k3xOp3gInPc4	jeho
první	první	k4xOgInPc4	první
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
Chopinem	Chopin	k1gMnSc7	Chopin
a	a	k8xC	a
Berliozem	Berlioz	k1gMnSc7	Berlioz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1831	[number]	k4	1831
navštívil	navštívit	k5eAaPmAgInS	navštívit
koncert	koncert	k1gInSc1	koncert
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
dával	dávat	k5eAaImAgMnS	dávat
houslový	houslový	k2eAgMnSc1d1	houslový
virtuos	virtuos	k1gMnSc1	virtuos
Niccolò	Niccolò	k1gMnSc1	Niccolò
Paganini	Paganin	k2eAgMnPc1d1	Paganin
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jej	on	k3xPp3gMnSc4	on
motivovalo	motivovat	k5eAaBmAgNnS	motivovat
ke	k	k7c3	k
snaze	snaha	k1gFnSc3	snaha
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
klavíristou	klavírista	k1gMnSc7	klavírista
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
dojmem	dojem	k1gInSc7	dojem
tohoto	tento	k3xDgInSc2	tento
koncertu	koncert	k1gInSc2	koncert
napsal	napsat	k5eAaPmAgMnS	napsat
ještě	ještě	k6eAd1	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
etudy	etuda	k1gFnSc2	etuda
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
po	po	k7c4	po
Paganinim	Paganinim	k1gInSc4	Paganinim
a	a	k8xC	a
o	o	k7c4	o
20	[number]	k4	20
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
další	další	k2eAgInPc1d1	další
<g/>
,	,	kIx,	,
rozšířené	rozšířený	k2eAgInPc1d1	rozšířený
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
cvičil	cvičit	k5eAaImAgMnS	cvičit
o	o	k7c6	o
samotě	samota	k1gFnSc6	samota
pět	pět	k4xCc4	pět
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Léta	léto	k1gNnSc2	léto
1839	[number]	k4	1839
<g/>
–	–	k?	–
<g/>
1847	[number]	k4	1847
strávil	strávit	k5eAaPmAgMnS	strávit
na	na	k7c6	na
nepřetržitém	přetržitý	k2eNgNnSc6d1	nepřetržité
koncertním	koncertní	k2eAgNnSc6d1	koncertní
turné	turné	k1gNnSc6	turné
jako	jako	k9	jako
klavírista	klavírista	k1gMnSc1	klavírista
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
prakticky	prakticky	k6eAd1	prakticky
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
zemích	zem	k1gFnPc6	zem
<g/>
:	:	kIx,	:
od	od	k7c2	od
Lisabonu	Lisabon	k1gInSc2	Lisabon
po	po	k7c4	po
Moskvu	Moskva	k1gFnSc4	Moskva
<g/>
,	,	kIx,	,
od	od	k7c2	od
Dublinu	Dublin	k1gInSc2	Dublin
po	po	k7c4	po
Istanbul	Istanbul	k1gInSc4	Istanbul
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
proslulost	proslulost	k1gFnSc1	proslulost
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
byla	být	k5eAaImAgFnS	být
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
Paganiniově	Paganiniův	k2eAgFnSc3d1	Paganiniův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vztahy	vztah	k1gInPc1	vztah
k	k	k7c3	k
ženám	žena	k1gFnPc3	žena
===	===	k?	===
</s>
</p>
<p>
<s>
Liszt	Liszt	k1gInSc1	Liszt
byl	být	k5eAaImAgInS	být
známý	známý	k2eAgInSc1d1	známý
svými	svůj	k3xOyFgInPc7	svůj
početnými	početný	k2eAgInPc7d1	početný
milostnými	milostný	k2eAgInPc7d1	milostný
vztahy	vztah	k1gInPc7	vztah
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
dlouhodobou	dlouhodobý	k2eAgFnSc7d1	dlouhodobá
družkou	družka	k1gFnSc7	družka
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
stala	stát	k5eAaPmAgFnS	stát
hraběnka	hraběnka	k1gFnSc1	hraběnka
Marie	Maria	k1gFnSc2	Maria
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Agoult	Agoult	k1gMnSc1	Agoult
(	(	kIx(	(
<g/>
1805	[number]	k4	1805
<g/>
–	–	k?	–
<g/>
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
poznal	poznat	k5eAaPmAgMnS	poznat
snad	snad	k9	snad
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1832	[number]	k4	1832
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
salonu	salon	k1gInSc6	salon
markýzy	markýza	k1gFnSc2	markýza
le	le	k?	le
Vayer	Vayer	k1gMnSc1	Vayer
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
starší	starší	k1gMnSc1	starší
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Agoult	Agoulta	k1gFnPc2	Agoulta
byla	být	k5eAaImAgFnS	být
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
píšící	píšící	k2eAgFnSc1d1	píšící
pod	pod	k7c7	pod
synonymem	synonymum	k1gNnSc7	synonymum
Daniel	Daniel	k1gMnSc1	Daniel
Stern	sternum	k1gNnPc2	sternum
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
roku	rok	k1gInSc2	rok
1833	[number]	k4	1833
jej	on	k3xPp3gNnSc4	on
Marie	Marie	k1gFnSc1	Marie
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Agoult	Agoult	k1gMnSc1	Agoult
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
zvala	zvát	k5eAaImAgFnS	zvát
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1835	[number]	k4	1835
se	se	k3xPyFc4	se
rozvedla	rozvést	k5eAaPmAgFnS	rozvést
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
měla	mít	k5eAaImAgFnS	mít
dvě	dva	k4xCgFnPc4	dva
dcery	dcera	k1gFnPc4	dcera
<g/>
,	,	kIx,	,
a	a	k8xC	a
přijela	přijet	k5eAaPmAgFnS	přijet
za	za	k7c7	za
Lisztem	Liszt	k1gInSc7	Liszt
do	do	k7c2	do
Ženevy	Ženeva	k1gFnSc2	Ženeva
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
společně	společně	k6eAd1	společně
odjeli	odjet	k5eAaPmAgMnP	odjet
opět	opět	k6eAd1	opět
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
Nohant	Nohanta	k1gFnPc2	Nohanta
k	k	k7c3	k
Mariině	Mariin	k2eAgFnSc3d1	Mariina
přítelkyni	přítelkyně	k1gFnSc3	přítelkyně
Amandine	Amandin	k1gInSc5	Amandin
Dupin	Dupin	k2eAgInSc4d1	Dupin
de	de	k?	de
Francueil	Francueil	k1gInSc4	Francueil
<g/>
,	,	kIx,	,
spisovatelce	spisovatelka	k1gFnSc3	spisovatelka
píšící	píšící	k2eAgFnSc2d1	píšící
pod	pod	k7c7	pod
mužským	mužský	k2eAgInSc7d1	mužský
pseudonymem	pseudonym	k1gInSc7	pseudonym
George	George	k1gNnSc4	George
Sand	Sanda	k1gFnPc2	Sanda
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Agoult	Agoult	k1gMnSc1	Agoult
žil	žít	k5eAaImAgMnS	žít
Liszt	Liszt	k1gInSc4	Liszt
beze	beze	k7c2	beze
sňatku	sňatek	k1gInSc2	sňatek
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
a	a	k8xC	a
narodily	narodit	k5eAaPmAgFnP	narodit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
<g/>
:	:	kIx,	:
Blandine	Blandin	k1gInSc5	Blandin
(	(	kIx(	(
<g/>
1835	[number]	k4	1835
<g/>
–	–	k?	–
<g/>
1862	[number]	k4	1862
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Cosima	Cosima	k1gFnSc1	Cosima
(	(	kIx(	(
<g/>
1837	[number]	k4	1837
<g/>
–	–	k?	–
<g/>
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
provdaná	provdaný	k2eAgFnSc1d1	provdaná
později	pozdě	k6eAd2	pozdě
za	za	k7c2	za
Hanse	Hans	k1gMnSc2	Hans
von	von	k1gInSc4	von
Bülow	Bülow	k1gMnSc2	Bülow
a	a	k8xC	a
Richarda	Richard	k1gMnSc2	Richard
Wagnera	Wagner	k1gMnSc2	Wagner
<g/>
)	)	kIx)	)
a	a	k8xC	a
Daniel	Daniel	k1gMnSc1	Daniel
(	(	kIx(	(
<g/>
1839	[number]	k4	1839
<g/>
–	–	k?	–
<g/>
1859	[number]	k4	1859
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Agoult	Agoulta	k1gFnPc2	Agoulta
a	a	k8xC	a
Liszt	Liszta	k1gFnPc2	Liszta
ukončili	ukončit	k5eAaPmAgMnP	ukončit
svůj	svůj	k3xOyFgInSc4	svůj
vztah	vztah	k1gInSc4	vztah
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1843	[number]	k4	1843
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
Lisztovo	Lisztův	k2eAgNnSc1d1	Lisztovo
milostné	milostný	k2eAgNnSc1d1	milostné
dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
s	s	k7c7	s
herečkou	herečka	k1gFnSc7	herečka
Charlottou	Charlotta	k1gFnSc7	Charlotta
von	von	k1gInSc4	von
Hagn	Hagn	k1gNnSc1	Hagn
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
jejich	jejich	k3xOp3gFnPc4	jejich
společné	společný	k2eAgFnPc4d1	společná
děti	dítě	k1gFnPc4	dítě
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
starala	starat	k5eAaImAgFnS	starat
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
Lisztova	Lisztův	k2eAgFnSc1d1	Lisztova
matka	matka	k1gFnSc1	matka
Anna	Anna	k1gFnSc1	Anna
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Liszt	Liszt	k1gMnSc1	Liszt
cestoval	cestovat	k5eAaImAgMnS	cestovat
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1847	[number]	k4	1847
až	až	k9	až
1861	[number]	k4	1861
měl	mít	k5eAaImAgInS	mít
poměr	poměr	k1gInSc1	poměr
s	s	k7c7	s
vdanou	vdaný	k2eAgFnSc7d1	vdaná
šlechtičnou	šlechtična	k1gFnSc7	šlechtična
Carolyne	Carolyn	k1gInSc5	Carolyn
zu	zu	k?	zu
Sayn-Wittgenstein	Sayn-Wittgenstein	k1gInSc1	Sayn-Wittgenstein
(	(	kIx(	(
<g/>
1819	[number]	k4	1819
<g/>
–	–	k?	–
<g/>
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plným	plný	k2eAgNnSc7d1	plné
jménem	jméno	k1gNnSc7	jméno
Carolyne	Carolyn	k1gInSc5	Carolyn
Elisabeth	Elisabeth	k1gInSc1	Elisabeth
kněžna	kněžna	k1gFnSc1	kněžna
zu	zu	k?	zu
Sayn-Wittgenstein-Berleburg-Ludwigsburg	Sayn-Wittgenstein-Berleburg-Ludwigsburg	k1gInSc1	Sayn-Wittgenstein-Berleburg-Ludwigsburg
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
dáma	dáma	k1gFnSc1	dáma
polského	polský	k2eAgInSc2d1	polský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
rodem	rod	k1gInSc7	rod
Carolyne	Carolyn	k1gInSc5	Carolyn
Iwanowska	Iwanowsek	k1gMnSc2	Iwanowsek
<g/>
)	)	kIx)	)
měla	mít	k5eAaImAgFnS	mít
hluboký	hluboký	k2eAgInSc4d1	hluboký
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
hudbě	hudba	k1gFnSc3	hudba
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
nesdílel	sdílet	k5eNaImAgMnS	sdílet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
byla	být	k5eAaImAgFnS	být
její	její	k3xOp3gFnSc1	její
žádost	žádost	k1gFnSc1	žádost
o	o	k7c4	o
rozvod	rozvod	k1gInSc4	rozvod
u	u	k7c2	u
papeže	papež	k1gMnSc4	papež
Pia	Pius	k1gMnSc2	Pius
IX	IX	kA	IX
<g/>
.	.	kIx.	.
sice	sice	k8xC	sice
schválena	schválit	k5eAaPmNgFnS	schválit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dlouho	dlouho	k6eAd1	dlouho
toužená	toužený	k2eAgFnSc1d1	toužená
svatba	svatba	k1gFnSc1	svatba
ztroskotala	ztroskotat	k5eAaPmAgFnS	ztroskotat
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
intervence	intervence	k1gFnSc2	intervence
manželových	manželův	k2eAgMnPc2d1	manželův
příbuzných	příbuzný	k1gMnPc2	příbuzný
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yRgFnSc6	který
měla	mít	k5eAaImAgFnS	mít
kněžna	kněžna	k1gFnSc1	kněžna
Carolyne	Carolyn	k1gInSc5	Carolyn
opět	opět	k6eAd1	opět
předložit	předložit	k5eAaPmF	předložit
svůj	svůj	k3xOyFgInSc4	svůj
rozvodový	rozvodový	k2eAgInSc4d1	rozvodový
spis	spis	k1gInSc4	spis
Svatému	svatý	k2eAgInSc3d1	svatý
stolci	stolec	k1gInSc3	stolec
<g/>
.	.	kIx.	.
</s>
<s>
Carolyne	Carolynout	k5eAaPmIp3nS	Carolynout
zu	zu	k?	zu
Sayn-Wittgenstein	Sayn-Wittgenstein	k1gInSc4	Sayn-Wittgenstein
rezignovala	rezignovat	k5eAaBmAgFnS	rezignovat
a	a	k8xC	a
ona	onen	k3xDgFnSc1	onen
a	a	k8xC	a
Liszt	Liszt	k2eAgMnSc1d1	Liszt
se	se	k3xPyFc4	se
jako	jako	k9	jako
milenci	milenec	k1gMnPc1	milenec
rozešli	rozejít	k5eAaPmAgMnP	rozejít
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
jejího	její	k3xOp3gInSc2	její
popudu	popud	k1gInSc2	popud
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
pravý	pravý	k2eAgInSc1d1	pravý
důvod	důvod	k1gInSc1	důvod
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
však	však	k9	však
zůstali	zůstat	k5eAaPmAgMnP	zůstat
blízkými	blízký	k2eAgMnPc7d1	blízký
přáteli	přítel	k1gMnPc7	přítel
až	až	k6eAd1	až
do	do	k7c2	do
Lisztovy	Lisztův	k2eAgFnSc2d1	Lisztova
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Roky	rok	k1gInPc1	rok
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1841	[number]	k4	1841
se	se	k3xPyFc4	se
Liszt	Liszt	k1gMnSc1	Liszt
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	s	k7c7	s
sasko-výmarským	saskoýmarský	k2eAgMnSc7d1	sasko-výmarský
velkovévodou	velkovévoda	k1gMnSc7	velkovévoda
Karlem	Karel	k1gMnSc7	Karel
Alexandrem	Alexandr	k1gMnSc7	Alexandr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
žádost	žádost	k1gFnSc4	žádost
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
stal	stát	k5eAaPmAgMnS	stát
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
odešla	odejít	k5eAaPmAgFnS	odejít
i	i	k9	i
Carolyne	Carolyn	k1gInSc5	Carolyn
Wittgenstein	Wittgenstein	k1gInSc1	Wittgenstein
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
dcerou	dcera	k1gFnSc7	dcera
Marií	Maria	k1gFnSc7	Maria
<g/>
,	,	kIx,	,
dvorním	dvorní	k2eAgMnSc7d1	dvorní
kapelníkem	kapelník	k1gMnSc7	kapelník
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
zanechal	zanechat	k5eAaPmAgInS	zanechat
veřejného	veřejný	k2eAgNnSc2d1	veřejné
vystupování	vystupování	k1gNnSc2	vystupování
jako	jako	k8xS	jako
klavírista	klavírista	k1gMnSc1	klavírista
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
hudební	hudební	k2eAgFnSc3d1	hudební
kompozici	kompozice	k1gFnSc3	kompozice
a	a	k8xC	a
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
tamější	tamější	k2eAgInSc4d1	tamější
orchestr	orchestr	k1gInSc4	orchestr
při	při	k7c6	při
operních	operní	k2eAgNnPc6d1	operní
provedeních	provedení	k1gNnPc6	provedení
a	a	k8xC	a
koncertech	koncert	k1gInPc6	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
dával	dávat	k5eAaImAgMnS	dávat
lekce	lekce	k1gFnPc4	lekce
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
psal	psát	k5eAaImAgMnS	psát
články	článek	k1gInPc4	článek
o	o	k7c6	o
Hectoru	Hector	k1gMnSc6	Hector
Berliozovi	Berlioz	k1gMnSc6	Berlioz
a	a	k8xC	a
svém	svůj	k3xOyFgMnSc6	svůj
pozdějším	pozdní	k2eAgMnSc6d2	pozdější
zeti	zeť	k1gMnSc6	zeť
Wagnerovi	Wagner	k1gMnSc6	Wagner
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
také	také	k9	také
složil	složit	k5eAaPmAgMnS	složit
své	svůj	k3xOyFgFnPc4	svůj
nejoceňovanější	oceňovaný	k2eAgFnPc4d3	nejoceňovanější
orchestrální	orchestrální	k2eAgFnPc4d1	orchestrální
a	a	k8xC	a
sborové	sborový	k2eAgFnPc4d1	sborová
skladby	skladba	k1gFnPc4	skladba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Liszt	Liszt	k1gMnSc1	Liszt
měl	mít	k5eAaImAgMnS	mít
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
repertoár	repertoár	k1gInSc1	repertoár
koncertů	koncert	k1gInPc2	koncert
tamní	tamní	k2eAgFnSc2d1	tamní
filharmonie	filharmonie	k1gFnSc2	filharmonie
a	a	k8xC	a
osobně	osobně	k6eAd1	osobně
je	být	k5eAaImIp3nS	být
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
<g/>
.	.	kIx.	.
</s>
<s>
Značně	značně	k6eAd1	značně
změnil	změnit	k5eAaPmAgMnS	změnit
dramaturgii	dramaturgie	k1gFnSc4	dramaturgie
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
skladeb	skladba	k1gFnPc2	skladba
mistrů	mistr	k1gMnPc2	mistr
vídeňského	vídeňský	k2eAgInSc2d1	vídeňský
klasicismu	klasicismus	k1gInSc2	klasicismus
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
Haydna	Haydno	k1gNnSc2	Haydno
a	a	k8xC	a
Mozarta	Mozart	k1gMnSc4	Mozart
<g/>
)	)	kIx)	)
začal	začít	k5eAaPmAgMnS	začít
–	–	k?	–
k	k	k7c3	k
nelibosti	nelibost	k1gFnSc3	nelibost
publika	publikum	k1gNnSc2	publikum
–	–	k?	–
uvádět	uvádět	k5eAaImF	uvádět
i	i	k9	i
skladby	skladba	k1gFnPc1	skladba
současných	současný	k2eAgMnPc2d1	současný
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
svého	svůj	k3xOyFgMnSc4	svůj
přítele	přítel	k1gMnSc4	přítel
Richarda	Richard	k1gMnSc4	Richard
Wagnera	Wagner	k1gMnSc4	Wagner
<g/>
.	.	kIx.	.
</s>
<s>
Liszt	Liszt	k1gMnSc1	Liszt
musel	muset	k5eAaImAgMnS	muset
čelit	čelit	k5eAaImF	čelit
intrikám	intrika	k1gFnPc3	intrika
svého	svůj	k3xOyFgMnSc2	svůj
předchůdce	předchůdce	k1gMnSc2	předchůdce
Hippolyte	Hippolyt	k1gInSc5	Hippolyt
Chelarda	Chelard	k1gMnSc2	Chelard
a	a	k8xC	a
neochotě	neochota	k1gFnSc6	neochota
špatně	špatně	k6eAd1	špatně
placených	placený	k2eAgMnPc2d1	placený
hráčů	hráč	k1gMnPc2	hráč
dostát	dostát	k5eAaPmF	dostát
nárokům	nárok	k1gInPc3	nárok
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
nový	nový	k2eAgMnSc1d1	nový
kapelník	kapelník	k1gMnSc1	kapelník
kladl	klást	k5eAaImAgMnS	klást
<g/>
.	.	kIx.	.
</s>
<s>
Společensky	společensky	k6eAd1	společensky
mu	on	k3xPp3gInSc3	on
také	také	k9	také
příliš	příliš	k6eAd1	příliš
nepomohlo	pomoct	k5eNaPmAgNnS	pomoct
soužití	soužití	k1gNnSc4	soužití
se	s	k7c7	s
stále	stále	k6eAd1	stále
vdanou	vdaný	k2eAgFnSc7d1	vdaná
Carolynou	Carolynou	k1gFnSc7	Carolynou
von	von	k1gInSc4	von
Wittgenstein	Wittgenstein	k2eAgInSc4d1	Wittgenstein
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
do	do	k7c2	do
orchestru	orchestr	k1gInSc2	orchestr
angažovat	angažovat	k5eAaBmF	angažovat
Josepha	Joseph	k1gMnSc2	Joseph
Joachima	Joachim	k1gMnSc2	Joachim
<g/>
.	.	kIx.	.
</s>
<s>
Mladičký	mladičký	k2eAgInSc1d1	mladičký
virtuóz	virtuóza	k1gFnPc2	virtuóza
však	však	k9	však
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
vydržel	vydržet	k5eAaPmAgInS	vydržet
jen	jen	k9	jen
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Lisztova	Lisztův	k2eAgFnSc1d1	Lisztova
práce	práce	k1gFnSc1	práce
nicméně	nicméně	k8xC	nicméně
přinášela	přinášet	k5eAaImAgFnS	přinášet
ovoce	ovoce	k1gNnSc4	ovoce
a	a	k8xC	a
Výmar	Výmar	k1gInSc1	Výmar
začal	začít	k5eAaPmAgInS	začít
opět	opět	k6eAd1	opět
získávat	získávat	k5eAaImF	získávat
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
renomé	renomé	k1gNnSc4	renomé
<g/>
.	.	kIx.	.
</s>
<s>
Přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
uvádění	uvádění	k1gNnSc3	uvádění
takových	takový	k3xDgNnPc2	takový
děl	dělo	k1gNnPc2	dělo
jako	jako	k8xS	jako
byl	být	k5eAaImAgInS	být
Manfred	Manfred	k1gInSc1	Manfred
od	od	k7c2	od
Roberta	Robert	k1gMnSc2	Robert
Schumanna	Schumann	k1gMnSc2	Schumann
<g/>
,	,	kIx,	,
Benvenuto	Benvenut	k2eAgNnSc1d1	Benvenuto
Cellini	Cellin	k2eAgMnPc1d1	Cellin
od	od	k7c2	od
Hectora	Hector	k1gMnSc2	Hector
Berlioze	Berlioz	k1gMnSc2	Berlioz
nebo	nebo	k8xC	nebo
oper	opera	k1gFnPc2	opera
Richarda	Richard	k1gMnSc2	Richard
Wagnera	Wagner	k1gMnSc2	Wagner
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
car	car	k1gMnSc1	car
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
I.	I.	kA	I.
nedá	dát	k5eNaPmIp3nS	dát
souhlas	souhlas	k1gInSc4	souhlas
k	k	k7c3	k
rozvodu	rozvod	k1gInSc3	rozvod
Lisztovy	Lisztův	k2eAgFnSc2d1	Lisztova
přítelkyně	přítelkyně	k1gFnSc2	přítelkyně
Carolyny	Carolyna	k1gFnSc2	Carolyna
<g/>
,	,	kIx,	,
nastěhoval	nastěhovat	k5eAaPmAgMnS	nastěhovat
se	se	k3xPyFc4	se
skladatel	skladatel	k1gMnSc1	skladatel
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
do	do	k7c2	do
vily	vila	k1gFnSc2	vila
Altenburg	Altenburg	k1gInSc1	Altenburg
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
je	být	k5eAaImIp3nS	být
Lisztova	Lisztův	k2eAgFnSc1d1	Lisztova
konzervatoř	konzervatoř	k1gFnSc1	konzervatoř
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
ho	on	k3xPp3gNnSc4	on
navštěhovali	navštěhovat	k5eAaImAgMnP	navštěhovat
jeho	jeho	k3xOp3gMnPc1	jeho
žáci	žák	k1gMnPc1	žák
–	–	k?	–
Carl	Carl	k1gMnSc1	Carl
Tausig	Tausig	k1gMnSc1	Tausig
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
von	von	k1gInSc4	von
Bülow	Bülow	k1gFnSc2	Bülow
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
Klindworth	Klindworth	k1gMnSc1	Klindworth
či	či	k8xC	či
Julius	Julius	k1gMnSc1	Julius
Reubke	Reubk	k1gFnSc2	Reubk
<g/>
.	.	kIx.	.
</s>
<s>
Von	von	k1gInSc1	von
Bülow	Bülow	k1gMnPc2	Bülow
si	se	k3xPyFc3	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1857	[number]	k4	1857
vzal	vzít	k5eAaPmAgMnS	vzít
jeho	jeho	k3xOp3gFnSc4	jeho
dceru	dcera	k1gFnSc4	dcera
Cosimu	Cosim	k1gInSc2	Cosim
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
vdala	vdát	k5eAaPmAgFnS	vdát
i	i	k9	i
druhá	druhý	k4xOgFnSc1	druhý
Lisztova	Lisztův	k2eAgFnSc1d1	Lisztova
dcera	dcera	k1gFnSc1	dcera
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
měl	mít	k5eAaImAgMnS	mít
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Agoult	Agoult	k1gMnSc1	Agoult
<g/>
,	,	kIx,	,
Blandina	Blandin	k2eAgFnSc1d1	Blandina
<g/>
.	.	kIx.	.
</s>
<s>
Altenburg	Altenburg	k1gInSc1	Altenburg
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
společenským	společenský	k2eAgInSc7d1	společenský
salónem	salón	k1gInSc7	salón
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
scházela	scházet	k5eAaImAgFnS	scházet
intelektuální	intelektuální	k2eAgFnSc1d1	intelektuální
smetánka	smetánka	k1gFnSc1	smetánka
<g/>
.	.	kIx.	.
</s>
<s>
Patřili	patřit	k5eAaImAgMnP	patřit
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
např.	např.	kA	např.
Hans	Hans	k1gMnSc1	Hans
Christian	Christian	k1gMnSc1	Christian
Andersen	Andersen	k1gMnSc1	Andersen
<g/>
,	,	kIx,	,
Bettina	Bettina	k1gFnSc1	Bettina
von	von	k1gInSc1	von
Arnim	Arnim	k1gMnSc1	Arnim
či	či	k8xC	či
Gustav	Gustav	k1gMnSc1	Gustav
Freytag	Freytag	k1gMnSc1	Freytag
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1857	[number]	k4	1857
se	se	k3xPyFc4	se
generálním	generální	k2eAgMnSc7d1	generální
intendantem	intendant	k1gMnSc7	intendant
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
stal	stát	k5eAaPmAgInS	stát
Franz	Franz	k1gInSc1	Franz
von	von	k1gInSc1	von
Dingelstedt	Dingelstedt	k1gInSc4	Dingelstedt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dával	dávat	k5eAaImAgInS	dávat
přednost	přednost	k1gFnSc4	přednost
činohře	činohra	k1gFnSc3	činohra
a	a	k8xC	a
hudební	hudební	k2eAgInSc1d1	hudební
vkus	vkus	k1gInSc1	vkus
měl	mít	k5eAaImAgInS	mít
odlišný	odlišný	k2eAgInSc1d1	odlišný
od	od	k7c2	od
Liszta	Liszt	k1gInSc2	Liszt
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
z	z	k7c2	z
hudebního	hudební	k2eAgInSc2d1	hudební
života	život	k1gInSc2	život
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
stahoval	stahovat	k5eAaImAgMnS	stahovat
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
ho	on	k3xPp3gMnSc4	on
definitivně	definitivně	k6eAd1	definitivně
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
Lisztova	Lisztův	k2eAgNnSc2d1	Lisztovo
výmarského	výmarský	k2eAgNnSc2d1	výmarské
působení	působení	k1gNnSc2	působení
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
něj	on	k3xPp3gMnSc2	on
zformoval	zformovat	k5eAaPmAgInS	zformovat
okruh	okruh	k1gInSc4	okruh
modernistických	modernistický	k2eAgMnPc2d1	modernistický
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgMnPc4	jenž
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
název	název	k1gInSc1	název
Novoněmecká	novoněmecký	k2eAgFnSc1d1	novoněmecký
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
vzory	vzor	k1gInPc7	vzor
jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
Liszta	Liszt	k1gInSc2	Liszt
<g/>
,	,	kIx,	,
sloužila	sloužit	k5eAaImAgFnS	sloužit
díla	dílo	k1gNnSc2	dílo
Hectora	Hector	k1gMnSc2	Hector
Berlioze	Berlioz	k1gMnSc2	Berlioz
a	a	k8xC	a
Richarda	Richard	k1gMnSc2	Richard
Wagnera	Wagner	k1gMnSc2	Wagner
<g/>
.	.	kIx.	.
</s>
<s>
Konzervativnější	konzervativní	k2eAgFnSc4d2	konzervativnější
protiváhu	protiváha	k1gFnSc4	protiváha
představovali	představovat	k5eAaImAgMnP	představovat
houslista	houslista	k1gMnSc1	houslista
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
Joseph	Joseph	k1gMnSc1	Joseph
Joachim	Joachim	k1gMnSc1	Joachim
<g/>
,	,	kIx,	,
Johannes	Johannes	k1gMnSc1	Johannes
Brahms	Brahms	k1gMnSc1	Brahms
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
kritik	kritik	k1gMnSc1	kritik
Eduard	Eduard	k1gMnSc1	Eduard
Hanslick	Hanslick	k1gMnSc1	Hanslick
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
výmarského	výmarský	k2eAgNnSc2d1	výmarské
období	období	k1gNnSc2	období
spadá	spadat	k5eAaImIp3nS	spadat
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
Lisztova	Lisztův	k2eAgNnSc2d1	Lisztovo
zralého	zralý	k2eAgNnSc2d1	zralé
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
13	[number]	k4	13
symfonických	symfonický	k2eAgFnPc2d1	symfonická
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
hudebního	hudební	k2eAgInSc2d1	hudební
útvaru	útvar	k1gInSc2	útvar
<g/>
,	,	kIx,	,
k	k	k7c3	k
jehož	jehož	k3xOyRp3gMnPc3	jehož
zakladatelům	zakladatel	k1gMnPc3	zakladatel
Liszt	Liszt	k1gInSc1	Liszt
patřil	patřit	k5eAaImAgInS	patřit
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
sem	sem	k6eAd1	sem
rovněž	rovněž	k9	rovněž
jeho	jeho	k3xOp3gNnSc4	jeho
zásadní	zásadní	k2eAgNnSc4d1	zásadní
dílo	dílo	k1gNnSc4	dílo
pro	pro	k7c4	pro
sólový	sólový	k2eAgInSc4d1	sólový
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
sonáta	sonáta	k1gFnSc1	sonáta
h	h	k?	h
moll	moll	k1gNnSc1	moll
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poslední	poslední	k2eAgNnPc1d1	poslední
léta	léto	k1gNnPc1	léto
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
odešel	odejít	k5eAaPmAgMnS	odejít
Liszt	Liszt	k1gMnSc1	Liszt
na	na	k7c4	na
odpočinek	odpočinek	k1gInSc4	odpočinek
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
protagonistů	protagonista	k1gMnPc2	protagonista
1000	[number]	k4	1000
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc1	výročí
oslav	oslava	k1gFnPc2	oslava
příchodu	příchod	k1gInSc2	příchod
sv.	sv.	kA	sv.
Cyrila	Cyril	k1gMnSc2	Cyril
(	(	kIx(	(
<g/>
Konstantina	Konstantin	k1gMnSc2	Konstantin
<g/>
)	)	kIx)	)
a	a	k8xC	a
sv.	sv.	kA	sv.
Metoděje	Metoděj	k1gMnSc2	Metoděj
na	na	k7c4	na
Velkou	velký	k2eAgFnSc4d1	velká
Moravu	Morava	k1gFnSc4	Morava
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
přijal	přijmout	k5eAaPmAgMnS	přijmout
čtyři	čtyři	k4xCgNnPc1	čtyři
nižší	nízký	k2eAgNnPc1d2	nižší
svěcení	svěcení	k1gNnPc1	svěcení
beze	beze	k7c2	beze
slibu	slib	k1gInSc2	slib
celibátu	celibát	k1gInSc2	celibát
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgMnS	být
pak	pak	k6eAd1	pak
nazýván	nazýván	k2eAgMnSc1d1	nazýván
Abbé	abbé	k1gMnSc1	abbé
Liszt	Liszt	k1gMnSc1	Liszt
<g/>
)	)	kIx)	)
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
terciářem	terciář	k1gMnSc7	terciář
ve	v	k7c6	v
ve	v	k7c6	v
františkánském	františkánský	k2eAgInSc6d1	františkánský
řádu	řád	k1gInSc6	řád
<g/>
.	.	kIx.	.
</s>
<s>
Pobýval	pobývat	k5eAaImAgMnS	pobývat
jako	jako	k9	jako
host	host	k1gMnSc1	host
v	v	k7c6	v
několika	několik	k4yIc6	několik
římských	římský	k2eAgInPc6d1	římský
klášterech	klášter	k1gInPc6	klášter
a	a	k8xC	a
stýkal	stýkat	k5eAaImAgMnS	stýkat
se	se	k3xPyFc4	se
s	s	k7c7	s
příslušníky	příslušník	k1gMnPc7	příslušník
kléru	klér	k1gInSc2	klér
včetně	včetně	k7c2	včetně
papeže	papež	k1gMnSc4	papež
Pia	Pius	k1gMnSc2	Pius
IX	IX	kA	IX
<g/>
..	..	k?	..
V	v	k7c6	v
pozdějším	pozdní	k2eAgInSc6d2	pozdější
věku	věk	k1gInSc6	věk
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
začal	začít	k5eAaPmAgInS	začít
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c4	o
uherský	uherský	k2eAgInSc4d1	uherský
národní	národní	k2eAgInSc4d1	národní
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
angažoval	angažovat	k5eAaBmAgMnS	angažovat
už	už	k6eAd1	už
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
dělil	dělit	k5eAaImAgInS	dělit
svůj	svůj	k3xOyFgInSc4	svůj
čas	čas	k1gInSc4	čas
mezi	mezi	k7c4	mezi
Řím	Řím	k1gInSc4	Řím
a	a	k8xC	a
Výmar	Výmar	k1gInSc4	Výmar
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1875	[number]	k4	1875
až	až	k9	až
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
zranění	zranění	k1gNnSc2	zranění
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
(	(	kIx(	(
<g/>
upadl	upadnout	k5eAaPmAgMnS	upadnout
na	na	k7c6	na
schodech	schod	k1gInPc6	schod
do	do	k7c2	do
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
)	)	kIx)	)
rovněž	rovněž	k9	rovněž
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
učil	učit	k5eAaImAgMnS	učit
na	na	k7c6	na
Maďarské	maďarský	k2eAgFnSc6d1	maďarská
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
<g/>
.	.	kIx.	.
</s>
<s>
Ferenc	Ferenc	k1gMnSc1	Ferenc
Liszt	Liszt	k1gMnSc1	Liszt
je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
Bayreuthu	Bayreuth	k1gInSc6	Bayreuth
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zemřel	zemřít	k5eAaPmAgMnS	zemřít
u	u	k7c2	u
své	svůj	k3xOyFgFnSc2	svůj
dcery	dcera	k1gFnSc2	dcera
Cosimy	Cosima	k1gFnSc2	Cosima
Wagnerové	Wagnerová	k1gFnSc2	Wagnerová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Liszt	Liszta	k1gFnPc2	Liszta
<g/>
,	,	kIx,	,
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
===	===	k?	===
</s>
</p>
<p>
<s>
Skladatel	skladatel	k1gMnSc1	skladatel
pobýval	pobývat	k5eAaImAgMnS	pobývat
na	na	k7c4	na
území	území	k1gNnSc4	území
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
několikrát	několikrát	k6eAd1	několikrát
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
přijel	přijet	k5eAaPmAgMnS	přijet
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1840	[number]	k4	1840
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
šest	šest	k4xCc1	šest
koncertů	koncert	k1gInPc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
programu	program	k1gInSc6	program
toho	ten	k3xDgNnSc2	ten
prvního	první	k4xOgNnSc2	první
byla	být	k5eAaImAgNnP	být
díla	dílo	k1gNnPc1	dílo
Beethovenova	Beethovenův	k2eAgNnPc1d1	Beethovenovo
(	(	kIx(	(
<g/>
sonáta	sonáta	k1gFnSc1	sonáta
Měsíční	měsíční	k2eAgFnSc1d1	měsíční
svit	svit	k1gInSc1	svit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Schubertova	Schubertův	k2eAgFnSc1d1	Schubertova
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
Ave	ave	k1gNnSc1	ave
Maria	Maria	k1gFnSc1	Maria
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Weberova	Weberův	k2eAgFnSc1d1	Weberova
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
Vyzvání	vyzvánět	k5eAaImIp3nS	vyzvánět
k	k	k7c3	k
tanci	tanec	k1gInSc3	tanec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Paganiniho	Paganini	k1gMnSc4	Paganini
(	(	kIx(	(
<g/>
Campanella	Campanella	k1gMnSc1	Campanella
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rossiniho	Rossini	k1gMnSc2	Rossini
a	a	k8xC	a
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
Uherských	uherský	k2eAgFnPc2d1	uherská
národních	národní	k2eAgFnPc2d1	národní
melodií	melodie	k1gFnPc2	melodie
<g/>
.	.	kIx.	.
</s>
<s>
Kritika	kritika	k1gFnSc1	kritika
nešetřila	šetřit	k5eNaImAgFnS	šetřit
chválou	chvála	k1gFnSc7	chvála
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
onom	onen	k3xDgInSc6	onen
roce	rok	k1gInSc6	rok
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
také	také	k9	také
jediná	jediný	k2eAgFnSc1d1	jediná
jeho	jeho	k3xOp3gFnSc1	jeho
skladba	skladba	k1gFnSc1	skladba
inspirovaná	inspirovaný	k2eAgFnSc1d1	inspirovaná
českými	český	k2eAgFnPc7d1	Česká
dějinami	dějiny	k1gFnPc7	dějiny
<g/>
:	:	kIx,	:
Husitská	husitský	k2eAgFnSc1d1	husitská
píseň	píseň	k1gFnSc1	píseň
<g/>
,	,	kIx,	,
Searlovo	Searlův	k2eAgNnSc1d1	Searlův
číslo	číslo	k1gNnSc1	číslo
234	[number]	k4	234
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
pak	pak	k6eAd1	pak
koncertoval	koncertovat	k5eAaImAgMnS	koncertovat
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
řídil	řídit	k5eAaImAgMnS	řídit
své	svůj	k3xOyFgFnPc4	svůj
skladby	skladba	k1gFnPc4	skladba
také	také	k9	také
Berlioz	Berlioz	k1gMnSc1	Berlioz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1856	[number]	k4	1856
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
Liszt	Liszt	k1gMnSc1	Liszt
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
svou	svůj	k3xOyFgFnSc4	svůj
Ostřihomskou	ostřihomský	k2eAgFnSc4d1	ostřihomská
mši	mše	k1gFnSc4	mše
<g/>
.	.	kIx.	.
</s>
<s>
Lisztovy	Lisztův	k2eAgInPc4d1	Lisztův
pražské	pražský	k2eAgInPc4d1	pražský
pobyty	pobyt	k1gInPc4	pobyt
připomíná	připomínat	k5eAaImIp3nS	připomínat
busta	busta	k1gFnSc1	busta
na	na	k7c6	na
domě	dům	k1gInSc6	dům
čp.	čp.	k?	čp.
416	[number]	k4	416
v	v	k7c6	v
Martinské	martinský	k2eAgFnSc6d1	Martinská
ulici	ulice	k1gFnSc6	ulice
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
pražském	pražský	k2eAgNnSc6d1	Pražské
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
koncerty	koncert	k1gInPc1	koncert
pod	pod	k7c7	pod
Lisztovou	Lisztový	k2eAgFnSc7d1	Lisztový
taktovkou	taktovka	k1gFnSc7	taktovka
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
uskutečnily	uskutečnit	k5eAaPmAgInP	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1858	[number]	k4	1858
<g/>
.	.	kIx.	.
</s>
<s>
Liszt	Liszt	k1gInSc1	Liszt
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
oba	dva	k4xCgMnPc4	dva
své	svůj	k3xOyFgInPc4	svůj
klavírní	klavírní	k2eAgInPc4d1	klavírní
koncerty	koncert	k1gInPc4	koncert
a	a	k8xC	a
čerstvě	čerstvě	k6eAd1	čerstvě
napsaná	napsaný	k2eAgNnPc1d1	napsané
díla	dílo	k1gNnPc1	dílo
<g/>
:	:	kIx,	:
symfonické	symfonický	k2eAgFnPc4d1	symfonická
básně	báseň	k1gFnPc4	báseň
Tasso	Tassa	k1gFnSc5	Tassa
<g/>
,	,	kIx,	,
Ideály	ideál	k1gInPc4	ideál
a	a	k8xC	a
Symfonii	symfonie	k1gFnSc4	symfonie
Dante	Dante	k1gMnSc1	Dante
<g/>
.	.	kIx.	.
</s>
<s>
Uvádění	uvádění	k1gNnSc1	uvádění
děl	dělo	k1gNnPc2	dělo
soudobých	soudobý	k2eAgMnPc2d1	soudobý
skladatelů	skladatel	k1gMnPc2	skladatel
souviselo	souviset	k5eAaImAgNnS	souviset
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
přístupem	přístup	k1gInSc7	přístup
k	k	k7c3	k
moderní	moderní	k2eAgFnSc3d1	moderní
hudbě	hudba	k1gFnSc3	hudba
na	na	k7c6	na
Pražské	pražský	k2eAgFnSc6d1	Pražská
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zavedl	zavést	k5eAaPmAgMnS	zavést
její	její	k3xOp3gMnSc1	její
druhý	druhý	k4xOgMnSc1	druhý
ředitel	ředitel	k1gMnSc1	ředitel
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1843	[number]	k4	1843
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Bedřich	Bedřich	k1gMnSc1	Bedřich
Kittl	Kittl	k1gMnSc1	Kittl
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
Lisztova	Lisztův	k2eAgFnSc1d1	Lisztova
návštěva	návštěva	k1gFnSc1	návštěva
Prahy	Praha	k1gFnSc2	Praha
spadá	spadat	k5eAaImIp3nS	spadat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Bedřicha	Bedřich	k1gMnSc4	Bedřich
Smetanu	Smetana	k1gMnSc4	Smetana
a	a	k8xC	a
Jana	Jan	k1gMnSc4	Jan
Procházku	Procházka	k1gMnSc4	Procházka
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
Smetana	Smetana	k1gMnSc1	Smetana
měl	mít	k5eAaImAgMnS	mít
z	z	k7c2	z
českých	český	k2eAgMnPc2d1	český
skladatelů	skladatel	k1gMnPc2	skladatel
k	k	k7c3	k
Lisztovi	Liszt	k1gMnSc3	Liszt
nejblíže	blízce	k6eAd3	blízce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1840	[number]	k4	1840
byl	být	k5eAaImAgInS	být
mezi	mezi	k7c7	mezi
posluchači	posluchač	k1gMnPc7	posluchač
Lisztova	Lisztův	k2eAgInSc2d1	Lisztův
pražského	pražský	k2eAgInSc2d1	pražský
koncertu	koncert	k1gInSc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
ho	on	k3xPp3gInSc4	on
písemně	písemně	k6eAd1	písemně
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
půjčku	půjčka	k1gFnSc4	půjčka
400	[number]	k4	400
zlatých	zlatá	k1gFnPc2	zlatá
a	a	k8xC	a
věnoval	věnovat	k5eAaPmAgMnS	věnovat
mu	on	k3xPp3gMnSc3	on
svůj	svůj	k3xOyFgInSc4	svůj
klavírní	klavírní	k2eAgInSc4d1	klavírní
cyklus	cyklus	k1gInSc4	cyklus
Six	Six	k1gMnPc2	Six
morceaux	morceaux	k1gInSc1	morceaux
caractéristiques	caractéristiquesa	k1gFnPc2	caractéristiquesa
(	(	kIx(	(
<g/>
Šest	šest	k4xCc1	šest
charakteristických	charakteristický	k2eAgInPc2d1	charakteristický
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Především	především	k9	především
Vám	vy	k3xPp2nPc3	vy
vyslovuji	vyslovovat	k5eAaImIp1nS	vyslovovat
nejoddanější	oddaný	k2eAgInSc4d3	nejoddanější
dík	dík	k1gInSc4	dík
za	za	k7c4	za
věnování	věnování	k1gNnSc4	věnování
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
přijímám	přijímat	k5eAaImIp1nS	přijímat
s	s	k7c7	s
radostí	radost	k1gFnSc7	radost
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
že	že	k8xS	že
skladby	skladba	k1gFnPc1	skladba
skutečně	skutečně	k6eAd1	skutečně
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejznamenitějším	znamenitý	k2eAgFnPc3d3	nejznamenitější
<g/>
,	,	kIx,	,
krásně	krásně	k6eAd1	krásně
cítěným	cítěný	k2eAgMnPc3d1	cítěný
a	a	k8xC	a
nejjemněji	jemně	k6eAd3	jemně
propracovaným	propracovaný	k2eAgInSc7d1	propracovaný
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
jsem	být	k5eAaImIp1nS	být
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
poznal	poznat	k5eAaPmAgMnS	poznat
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
Liszt	Liszt	k1gMnSc1	Liszt
<g/>
.	.	kIx.	.
</s>
<s>
Peníze	peníz	k1gInPc4	peníz
mu	on	k3xPp3gMnSc3	on
nepůjčil	půjčit	k5eNaPmAgMnS	půjčit
<g/>
,	,	kIx,	,
zařídil	zařídit	k5eAaPmAgMnS	zařídit
však	však	k9	však
vydání	vydání	k1gNnSc4	vydání
cyklu	cyklus	k1gInSc2	cyklus
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
.	.	kIx.	.
</s>
<s>
Osobně	osobně	k6eAd1	osobně
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
komponisté	komponista	k1gMnPc1	komponista
seznámili	seznámit	k5eAaPmAgMnP	seznámit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
<g/>
,	,	kIx,	,
když	když	k8xS	když
Liszt	Liszt	k1gMnSc1	Liszt
přijel	přijet	k5eAaPmAgMnS	přijet
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
spolu	spolu	k6eAd1	spolu
hráli	hrát	k5eAaImAgMnP	hrát
čtyřručně	čtyřručně	k6eAd1	čtyřručně
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
i	i	k9	i
whist	whist	k1gInSc4	whist
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1856	[number]	k4	1856
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Liszt	Liszt	k1gMnSc1	Liszt
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
Ostřihomskou	ostřihomský	k2eAgFnSc4d1	ostřihomská
mši	mše	k1gFnSc4	mše
<g/>
,	,	kIx,	,
Smetana	Smetana	k1gMnSc1	Smetana
se	se	k3xPyFc4	se
také	také	k9	také
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
provedení	provedení	k1gNnSc4	provedení
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
Smetana	Smetana	k1gMnSc1	Smetana
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Göteborgu	Göteborg	k1gInSc2	Göteborg
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cesta	k1gFnSc7	cesta
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
göteborský	göteborský	k2eAgInSc4d1	göteborský
pobyt	pobyt	k1gInSc4	pobyt
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1857	[number]	k4	1857
i	i	k9	i
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
a	a	k8xC	a
dcerou	dcera	k1gFnSc7	dcera
zastavil	zastavit	k5eAaPmAgMnS	zastavit
u	u	k7c2	u
Liszta	Liszt	k1gInSc2	Liszt
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
vztahy	vztah	k1gInPc1	vztah
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
upevnily	upevnit	k5eAaPmAgFnP	upevnit
během	během	k7c2	během
dalších	další	k2eAgNnPc2d1	další
setkání	setkání	k1gNnPc2	setkání
koncem	koncem	k7c2	koncem
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
Smetanovi	Smetanův	k2eAgMnPc1d1	Smetanův
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nepodařilo	podařit	k5eNaPmAgNnS	podařit
Liszta	Liszt	k1gMnSc4	Liszt
přimět	přimět	k5eAaPmF	přimět
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
uvedl	uvést	k5eAaPmAgMnS	uvést
některou	některý	k3yIgFnSc4	některý
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
symfonických	symfonický	k2eAgFnPc2d1	symfonická
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
inspirovaných	inspirovaný	k2eAgFnPc2d1	inspirovaná
právě	právě	k9	právě
jeho	jeho	k3xOp3gFnSc7	jeho
tvorbou	tvorba	k1gFnSc7	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
dopisů	dopis	k1gInPc2	dopis
svému	svůj	k3xOyFgInSc3	svůj
velkému	velký	k2eAgInSc3d1	velký
vzoru	vzor	k1gInSc3	vzor
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pokládejte	pokládat	k5eAaImRp2nP	pokládat
mě	já	k3xPp1nSc4	já
za	za	k7c2	za
nejhorlivějšího	horlivý	k2eAgMnSc2d3	nejhorlivější
stoupence	stoupenec	k1gMnSc2	stoupenec
našeho	náš	k3xOp1gInSc2	náš
uměleckého	umělecký	k2eAgInSc2d1	umělecký
směru	směr	k1gInSc2	směr
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
slovem	slovem	k6eAd1	slovem
i	i	k9	i
činem	čin	k1gInSc7	čin
stojí	stát	k5eAaImIp3nP	stát
za	za	k7c7	za
jeho	jeho	k3xOp3gFnSc7	jeho
svatou	svatý	k2eAgFnSc7d1	svatá
pravdou	pravda	k1gFnSc7	pravda
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
napsal	napsat	k5eAaPmAgMnS	napsat
Lisztovi	Lisztův	k2eAgMnPc1d1	Lisztův
<g/>
.	.	kIx.	.
<g/>
S	s	k7c7	s
Lisztem	Liszt	k1gMnSc7	Liszt
se	se	k3xPyFc4	se
stýkal	stýkat	k5eAaImAgMnS	stýkat
také	také	k9	také
učitel	učitel	k1gMnSc1	učitel
Antonína	Antonín	k1gMnSc2	Antonín
Dvořáka	Dvořák	k1gMnSc2	Dvořák
na	na	k7c6	na
Varhanické	varhanický	k2eAgFnSc6d1	varhanická
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Josef	Josef	k1gMnSc1	Josef
Krejčí	Krejčí	k1gMnSc1	Krejčí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
logické	logický	k2eAgNnSc1d1	logické
<g/>
,	,	kIx,	,
že	že	k8xS	že
Dvořákova	Dvořákův	k2eAgFnSc1d1	Dvořákova
raná	raný	k2eAgFnSc1d1	raná
tvorba	tvorba	k1gFnSc1	tvorba
je	být	k5eAaImIp3nS	být
Lisztem	Liszt	k1gInSc7	Liszt
–	–	k?	–
a	a	k8xC	a
také	také	k9	také
Wagnerem	Wagner	k1gMnSc7	Wagner
–	–	k?	–
značně	značně	k6eAd1	značně
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
o	o	k7c6	o
jeho	jeho	k3xOp3gInPc6	jeho
třech	tři	k4xCgInPc6	tři
smyčcových	smyčcový	k2eAgInPc6d1	smyčcový
kvartetech	kvartet	k1gInPc6	kvartet
vzniklých	vzniklý	k2eAgInPc6d1	vzniklý
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
<g/>
:	:	kIx,	:
B	B	kA	B
dur	dur	k1gNnSc1	dur
<g/>
,	,	kIx,	,
D	D	kA	D
dur	dur	k1gNnSc2	dur
a	a	k8xC	a
e	e	k0	e
moll	moll	k1gNnPc1	moll
<g/>
,	,	kIx,	,
čísla	číslo	k1gNnPc1	číslo
Burghauserova	Burghauserův	k2eAgInSc2d1	Burghauserův
katalogu	katalog	k1gInSc2	katalog
17	[number]	k4	17
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
podal	podat	k5eAaPmAgMnS	podat
Dvořák	Dvořák	k1gMnSc1	Dvořák
podpůrnému	podpůrný	k2eAgInSc3d1	podpůrný
spolku	spolek	k1gInSc3	spolek
Svatobor	Svatobor	k1gInSc4	Svatobor
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
finanční	finanční	k2eAgInSc4d1	finanční
příspěvek	příspěvek	k1gInSc4	příspěvek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgMnS	moct
navštívit	navštívit	k5eAaPmF	navštívit
Liszta	Liszta	k1gMnSc1	Liszta
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
<g/>
.	.	kIx.	.
</s>
<s>
Spolek	spolek	k1gInSc1	spolek
však	však	k9	však
skladatelově	skladatelův	k2eAgFnSc3d1	skladatelova
prosbě	prosba	k1gFnSc3	prosba
nevyhověl	vyhovět	k5eNaPmAgInS	vyhovět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Začátkem	začátkem	k7c2	začátkem
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Liszt	Liszt	k1gInSc1	Liszt
spřátelil	spřátelit	k5eAaPmAgInS	spřátelit
s	s	k7c7	s
knížetem	kníže	k1gMnSc7	kníže
Felixem	Felix	k1gMnSc7	Felix
Lichnovským	Lichnovský	k2eAgMnSc7d1	Lichnovský
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ho	on	k3xPp3gNnSc4	on
občas	občas	k6eAd1	občas
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
na	na	k7c6	na
jeho	jeho	k3xOp3gFnPc6	jeho
cestách	cesta	k1gFnPc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Šlechtic	šlechtic	k1gMnSc1	šlechtic
ho	on	k3xPp3gNnSc4	on
také	také	k9	také
několikrát	několikrát	k6eAd1	několikrát
přivítal	přivítat	k5eAaPmAgMnS	přivítat
na	na	k7c6	na
rodinném	rodinný	k2eAgInSc6d1	rodinný
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
nad	nad	k7c7	nad
Moravicí	Moravice	k1gFnSc7	Moravice
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
<g/>
.	.	kIx.	.
</s>
<s>
Liszt	Liszt	k1gMnSc1	Liszt
zde	zde	k6eAd1	zde
zahrál	zahrát	k5eAaPmAgMnS	zahrát
několik	několik	k4yIc4	několik
koncertů	koncert	k1gInPc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
knížete	kníže	k1gMnSc2	kníže
Liszt	Liszt	k2eAgInSc1d1	Liszt
Hradec	Hradec	k1gInSc1	Hradec
ještě	ještě	k6eAd1	ještě
jednou	jeden	k4xCgFnSc7	jeden
navštívil	navštívit	k5eAaPmAgInS	navštívit
<g/>
;	;	kIx,	;
pracoval	pracovat	k5eAaImAgInS	pracovat
zde	zde	k6eAd1	zde
na	na	k7c6	na
symfonické	symfonický	k2eAgFnSc6d1	symfonická
básni	báseň	k1gFnSc6	báseň
Hungaria	Hungarium	k1gNnSc2	Hungarium
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
zde	zde	k6eAd1	zde
Lisztův	Lisztův	k2eAgInSc4d1	Lisztův
pobyt	pobyt	k1gInSc4	pobyt
připomíná	připomínat	k5eAaImIp3nS	připomínat
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
<g/>
Bonviván	bonviván	k1gMnSc1	bonviván
Liszt	Liszt	k1gMnSc1	Liszt
také	také	k9	také
neopomněl	opomnět	k5eNaPmAgMnS	opomnět
navštívit	navštívit	k5eAaPmF	navštívit
teplické	teplický	k2eAgFnPc4d1	Teplická
lázně	lázeň	k1gFnPc4	lázeň
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
za	za	k7c2	za
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
evropských	evropský	k2eAgNnPc2d1	Evropské
společenských	společenský	k2eAgNnPc2d1	společenské
center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1853	[number]	k4	1853
se	se	k3xPyFc4	se
ubytoval	ubytovat	k5eAaPmAgMnS	ubytovat
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
domů	dům	k1gInPc2	dům
v	v	k7c6	v
Dlouhé	Dlouhé	k2eAgFnSc6d1	Dlouhé
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hudební	hudební	k2eAgFnSc1d1	hudební
dráha	dráha	k1gFnSc1	dráha
a	a	k8xC	a
styl	styl	k1gInSc1	styl
==	==	k?	==
</s>
</p>
<p>
<s>
Liszt	Liszt	k1gInSc1	Liszt
po	po	k7c6	po
sobě	se	k3xPyFc3	se
zanechal	zanechat	k5eAaPmAgInS	zanechat
velmi	velmi	k6eAd1	velmi
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgMnSc2	jenž
velká	velká	k1gFnSc1	velká
část	část	k1gFnSc4	část
je	být	k5eAaImIp3nS	být
dosud	dosud	k6eAd1	dosud
živá	živý	k2eAgFnSc1d1	živá
a	a	k8xC	a
hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaPmAgInS	napsat
kolem	kolem	k7c2	kolem
350	[number]	k4	350
vlastních	vlastní	k2eAgFnPc2d1	vlastní
skladeb	skladba	k1gFnPc2	skladba
a	a	k8xC	a
přes	přes	k7c4	přes
200	[number]	k4	200
transkripcí	transkripce	k1gFnPc2	transkripce
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
děl	dělo	k1gNnPc2	dělo
jiných	jiný	k2eAgMnPc2d1	jiný
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
jeho	jeho	k3xOp3gMnPc2	jeho
žáků	žák	k1gMnPc2	žák
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
400	[number]	k4	400
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgMnS	napsat
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c6	na
osmi	osm	k4xCc6	osm
svazcích	svazek	k1gInPc6	svazek
prózy	próza	k1gFnSc2	próza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
komponoval	komponovat	k5eAaImAgInS	komponovat
převážně	převážně	k6eAd1	převážně
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
spíše	spíše	k9	spíše
orchestrální	orchestrální	k2eAgFnSc7d1	orchestrální
tvorbou	tvorba	k1gFnSc7	tvorba
a	a	k8xC	a
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
komponoval	komponovat	k5eAaImAgMnS	komponovat
duchovní	duchovní	k2eAgFnPc4d1	duchovní
skladby	skladba	k1gFnPc4	skladba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Liszt	Liszt	k1gMnSc1	Liszt
byl	být	k5eAaImAgMnS	být
tvůrcem	tvůrce	k1gMnSc7	tvůrce
mnoha	mnoho	k4c2	mnoho
technických	technický	k2eAgFnPc2d1	technická
inovací	inovace	k1gFnPc2	inovace
v	v	k7c6	v
klavírní	klavírní	k2eAgFnSc6d1	klavírní
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Hodně	hodně	k6eAd1	hodně
jeho	jeho	k3xOp3gFnPc2	jeho
skladeb	skladba	k1gFnPc2	skladba
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejnáročnější	náročný	k2eAgInPc4d3	nejnáročnější
v	v	k7c6	v
klavírním	klavírní	k2eAgInSc6d1	klavírní
repertoáru	repertoár	k1gInSc6	repertoár
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
používal	používat	k5eAaImAgMnS	používat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dílech	díl	k1gInPc6	díl
tematické	tematický	k2eAgFnSc2d1	tematická
obměny	obměna	k1gFnSc2	obměna
podobné	podobný	k2eAgFnSc2d1	podobná
variacím	variace	k1gFnPc3	variace
nebo	nebo	k8xC	nebo
pozdějšímu	pozdní	k2eAgInSc3d2	pozdější
leitmotivu	leitmotiv	k1gInSc3	leitmotiv
Richarda	Richard	k1gMnSc2	Richard
Wagnera	Wagner	k1gMnSc2	Wagner
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
též	též	k9	též
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
tvůrce	tvůrce	k1gMnPc4	tvůrce
symfonické	symfonický	k2eAgFnSc2d1	symfonická
básně	báseň	k1gFnSc2	báseň
<g/>
,	,	kIx,	,
řadící	řadící	k2eAgFnSc2d1	řadící
se	se	k3xPyFc4	se
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
programní	programní	k2eAgFnSc2d1	programní
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Liszt	Liszt	k1gMnSc1	Liszt
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
předchůdce	předchůdce	k1gMnSc1	předchůdce
v	v	k7c6	v
Beethovenově	Beethovenův	k2eAgFnSc6d1	Beethovenova
Pastorální	pastorální	k2eAgFnSc6d1	pastorální
symfonii	symfonie	k1gFnSc6	symfonie
či	či	k8xC	či
Berliozově	Berliozův	k2eAgFnSc3d1	Berliozova
Fantastické	fantastický	k2eAgFnSc3d1	fantastická
symfonii	symfonie	k1gFnSc3	symfonie
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
ale	ale	k9	ale
neopírala	opírat	k5eNaImAgFnS	opírat
o	o	k7c4	o
literární	literární	k2eAgFnSc4d1	literární
předlohu	předloha	k1gFnSc4	předloha
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
přišel	přijít	k5eAaPmAgInS	přijít
teprve	teprve	k6eAd1	teprve
Liszt	Liszt	k1gInSc1	Liszt
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
mimořádnému	mimořádný	k2eAgInSc3d1	mimořádný
zájmu	zájem	k1gInSc3	zájem
o	o	k7c4	o
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgInS	patřit
k	k	k7c3	k
prvním	první	k4xOgMnPc3	první
skladatelům	skladatel	k1gMnPc3	skladatel
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
dokonale	dokonale	k6eAd1	dokonale
skloubit	skloubit	k5eAaPmF	skloubit
báseň	báseň	k1gFnSc4	báseň
a	a	k8xC	a
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Styl	styl	k1gInSc1	styl
jeho	jeho	k3xOp3gFnSc2	jeho
hry	hra	k1gFnSc2	hra
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
převratný	převratný	k2eAgInSc1d1	převratný
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
všichni	všechen	k3xTgMnPc1	všechen
dřívější	dřívější	k2eAgMnPc1d1	dřívější
klavíristé	klavírista	k1gMnPc1	klavírista
i	i	k8xC	i
jeho	jeho	k3xOp3gMnPc1	jeho
současníci	současník	k1gMnPc1	současník
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Felix	Felix	k1gMnSc1	Felix
Mendelssohn-Bartholdy	Mendelssohn-Bartholda	k1gFnSc2	Mendelssohn-Bartholda
<g/>
,	,	kIx,	,
hráli	hrát	k5eAaImAgMnP	hrát
s	s	k7c7	s
rukama	ruka	k1gFnPc7	ruka
při	při	k7c6	při
klávesách	klávesa	k1gFnPc6	klávesa
a	a	k8xC	a
bez	bez	k7c2	bez
zbytečných	zbytečný	k2eAgInPc2d1	zbytečný
pohybů	pohyb	k1gInPc2	pohyb
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
koncertů	koncert	k1gInPc2	koncert
udělal	udělat	k5eAaPmAgInS	udělat
téměř	téměř	k6eAd1	téměř
divadelní	divadelní	k2eAgNnSc4d1	divadelní
představení	představení	k1gNnSc4	představení
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
nedbalou	dbalý	k2eNgFnSc7d1	nedbalá
poklonou	poklona	k1gFnSc7	poklona
<g/>
,	,	kIx,	,
zasedl	zasednout	k5eAaPmAgMnS	zasednout
ke	k	k7c3	k
klavíru	klavír	k1gInSc3	klavír
a	a	k8xC	a
s	s	k7c7	s
rozmachem	rozmach	k1gInSc7	rozmach
udeřil	udeřit	k5eAaPmAgMnS	udeřit
do	do	k7c2	do
nástroje	nástroj	k1gInSc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
klavíristů	klavírista	k1gMnPc2	klavírista
jeho	jeho	k3xOp3gInSc7	jeho
stylem	styl	k1gInSc7	styl
opovrhovalo	opovrhovat	k5eAaImAgNnS	opovrhovat
<g/>
,	,	kIx,	,
žádný	žádný	k3yNgInSc1	žádný
ovšem	ovšem	k9	ovšem
nemohl	moct	k5eNaImAgInS	moct
popřít	popřít	k5eAaPmF	popřít
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
virtuozita	virtuozita	k1gFnSc1	virtuozita
byla	být	k5eAaImAgFnS	být
mimořádná	mimořádný	k2eAgFnSc1d1	mimořádná
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
muzikální	muzikální	k2eAgNnSc1d1	muzikální
cítění	cítění	k1gNnSc1	cítění
geniální	geniální	k2eAgNnSc1d1	geniální
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
hudební	hudební	k2eAgInSc1d1	hudební
talent	talent	k1gInSc1	talent
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
svých	svůj	k3xOyFgFnPc6	svůj
koncertních	koncertní	k2eAgFnPc6d1	koncertní
cestách	cesta	k1gFnPc6	cesta
skládal	skládat	k5eAaImAgInS	skládat
také	také	k6eAd1	také
virtuózní	virtuózní	k2eAgFnPc4d1	virtuózní
transkripce	transkripce	k1gFnPc4	transkripce
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
jiných	jiný	k2eAgMnPc2d1	jiný
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
skladby	skladba	k1gFnPc1	skladba
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
spíše	spíše	k9	spíše
historickou	historický	k2eAgFnSc4d1	historická
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
pozdějším	pozdní	k2eAgInSc6d2	pozdější
věku	věk	k1gInSc6	věk
Liszt	Liszt	k1gMnSc1	Liszt
více	hodně	k6eAd2	hodně
experimentoval	experimentovat	k5eAaImAgMnS	experimentovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
stylu	styl	k1gInSc2	styl
jeho	jeho	k3xOp3gFnSc2	jeho
předchozí	předchozí	k2eAgFnSc2d1	předchozí
tvorby	tvorba	k1gFnSc2	tvorba
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
odchylovaly	odchylovat	k5eAaImAgFnP	odchylovat
například	například	k6eAd1	například
Atonální	atonální	k2eAgFnSc1d1	atonální
bagatela	bagatela	k1gFnSc1	bagatela
nebo	nebo	k8xC	nebo
skladba	skladba	k1gFnSc1	skladba
Nuages	Nuagesa	k1gFnPc2	Nuagesa
Gris	Grisa	k1gFnPc2	Grisa
(	(	kIx(	(
<g/>
Šedé	Šedé	k2eAgInPc1d1	Šedé
mraky	mrak	k1gInPc1	mrak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Lisztově	Lisztův	k2eAgFnSc6d1	Lisztova
tvorbě	tvorba	k1gFnSc6	tvorba
lze	lze	k6eAd1	lze
také	také	k9	také
poprvé	poprvé	k6eAd1	poprvé
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
hudebním	hudební	k2eAgInSc6d1	hudební
monotematismu	monotematismus	k1gInSc6	monotematismus
<g/>
.	.	kIx.	.
</s>
<s>
Znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
motivu	motiv	k1gInSc2	motiv
je	být	k5eAaImIp3nS	být
odvozený	odvozený	k2eAgInSc1d1	odvozený
celý	celý	k2eAgInSc1d1	celý
tematický	tematický	k2eAgInSc1d1	tematický
materiál	materiál	k1gInSc1	materiál
skladby	skladba	k1gFnSc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Harmonické	harmonický	k2eAgInPc1d1	harmonický
postupy	postup	k1gInPc1	postup
a	a	k8xC	a
forma	forma	k1gFnSc1	forma
jeho	jeho	k3xOp3gFnPc2	jeho
pozdních	pozdní	k2eAgFnPc2d1	pozdní
skladeb	skladba	k1gFnPc2	skladba
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
mnoho	mnoho	k6eAd1	mnoho
pozdějších	pozdní	k2eAgMnPc2d2	pozdější
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Arnolda	Arnold	k1gMnSc4	Arnold
Schoenberga	Schoenberg	k1gMnSc4	Schoenberg
a	a	k8xC	a
Bélu	Béla	k1gMnSc4	Béla
Bartóka	Bartók	k1gMnSc4	Bartók
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Soupis	soupis	k1gInSc4	soupis
Lisztova	Lisztův	k2eAgNnSc2d1	Lisztovo
díla	dílo	k1gNnSc2	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
roztřídil	roztřídit	k5eAaPmAgMnS	roztřídit
Humphrey	Humphrea	k1gFnPc4	Humphrea
Searle	Searle	k1gNnSc2	Searle
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
The	The	k1gMnSc1	The
music	music	k1gMnSc1	music
of	of	k?	of
Liszt	Liszt	k1gMnSc1	Liszt
Lisztovo	Lisztův	k2eAgNnSc4d1	Lisztovo
dílo	dílo	k1gNnSc4	dílo
podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
klasifikace	klasifikace	k1gFnSc2	klasifikace
byla	být	k5eAaImAgFnS	být
jednotlivým	jednotlivý	k2eAgFnPc3d1	jednotlivá
skladbám	skladba	k1gFnPc3	skladba
přiřazena	přiřadit	k5eAaPmNgFnS	přiřadit
čísla	číslo	k1gNnSc2	číslo
S.	S.	kA	S.
1	[number]	k4	1
(	(	kIx(	(
<g/>
opera	opera	k1gFnSc1	opera
Don	Don	k1gMnSc1	Don
Sanche	Sanche	k1gInSc1	Sanche
<g/>
)	)	kIx)	)
až	až	k9	až
S.	S.	kA	S.
768	[number]	k4	768
(	(	kIx(	(
<g/>
transkripce	transkripce	k1gFnSc2	transkripce
Schubertova	Schubertův	k2eAgInSc2d1	Schubertův
Der	drát	k5eAaImRp2nS	drát
ewige	ewige	k1gNnSc4	ewige
Jude	Jude	k1gInSc1	Jude
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
s	s	k7c7	s
čísly	číslo	k1gNnPc7	číslo
990	[number]	k4	990
až	až	k8xS	až
999	[number]	k4	999
jsou	být	k5eAaImIp3nP	být
vedeny	vést	k5eAaImNgInP	vést
další	další	k2eAgInPc1d1	další
<g/>
,	,	kIx,	,
typově	typově	k6eAd1	typově
blíže	blízce	k6eAd2	blízce
nezařaditelné	zařaditelný	k2eNgFnPc4d1	nezařaditelná
skladby	skladba	k1gFnPc4	skladba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
skladeb	skladba	k1gFnPc2	skladba
(	(	kIx(	(
<g/>
výběr	výběr	k1gInSc1	výběr
<g/>
,	,	kIx,	,
číslování	číslování	k1gNnSc1	číslování
podle	podle	k7c2	podle
Searlova	Searlův	k2eAgInSc2d1	Searlův
seznamu	seznam	k1gInSc2	seznam
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Duchovní	duchovní	k2eAgFnPc4d1	duchovní
skladby	skladba	k1gFnPc4	skladba
===	===	k?	===
</s>
</p>
<p>
<s>
9	[number]	k4	9
Missa	Missa	k1gFnSc1	Missa
solennis	solennis	k1gFnSc1	solennis
(	(	kIx(	(
<g/>
Ostřihomská	ostřihomský	k2eAgFnSc1d1	ostřihomská
mše	mše	k1gFnSc1	mše
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
,	,	kIx,	,
1857	[number]	k4	1857
<g/>
/	/	kIx~	/
<g/>
58	[number]	k4	58
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
10	[number]	k4	10
Missa	Missa	k1gFnSc1	Missa
choralis	choralis	k1gFnSc1	choralis
(	(	kIx(	(
<g/>
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Světské	světský	k2eAgFnPc1d1	světská
sborové	sborový	k2eAgFnPc1d1	sborová
skladby	skladba	k1gFnPc1	skladba
===	===	k?	===
</s>
</p>
<p>
<s>
67	[number]	k4	67
Slavnostní	slavnostní	k2eAgFnSc1d1	slavnostní
kantáta	kantáta	k1gFnSc1	kantáta
k	k	k7c3	k
odhalení	odhalení	k1gNnSc3	odhalení
Beethovenova	Beethovenův	k2eAgInSc2d1	Beethovenův
pomníku	pomník	k1gInSc2	pomník
v	v	k7c6	v
Bonnu	Bonn	k1gInSc6	Bonn
(	(	kIx(	(
<g/>
1845	[number]	k4	1845
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
70	[number]	k4	70
Umělcům	umělec	k1gMnPc3	umělec
(	(	kIx(	(
<g/>
1853	[number]	k4	1853
<g/>
,	,	kIx,	,
1856	[number]	k4	1856
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Opery	opera	k1gFnSc2	opera
===	===	k?	===
</s>
</p>
<p>
<s>
1	[number]	k4	1
Don	Don	k1gMnSc1	Don
Sanche	Sanch	k1gInSc2	Sanch
aneb	aneb	k?	aneb
zámek	zámek	k1gInSc1	zámek
lásky	láska	k1gFnSc2	láska
(	(	kIx(	(
<g/>
1825	[number]	k4	1825
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Orchestrální	orchestrální	k2eAgFnPc4d1	orchestrální
skladby	skladba	k1gFnPc4	skladba
===	===	k?	===
</s>
</p>
<p>
<s>
95	[number]	k4	95
<g/>
–	–	k?	–
<g/>
106	[number]	k4	106
Třináct	třináct	k4xCc1	třináct
symfonických	symfonický	k2eAgFnPc2d1	symfonická
básní	báseň	k1gFnPc2	báseň
<g/>
:	:	kIx,	:
Co	co	k3yQnSc1	co
slyšíme	slyšet	k5eAaImIp1nP	slyšet
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
(	(	kIx(	(
<g/>
Horská	horský	k2eAgFnSc1d1	horská
symfonie	symfonie	k1gFnSc1	symfonie
<g/>
,	,	kIx,	,
1848	[number]	k4	1848
<g/>
/	/	kIx~	/
<g/>
49	[number]	k4	49
<g/>
,	,	kIx,	,
1854	[number]	k4	1854
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tasso	Tassa	k1gFnSc5	Tassa
<g/>
,	,	kIx,	,
nářek	nářek	k1gInSc4	nářek
a	a	k8xC	a
triumf	triumf	k1gInSc4	triumf
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
,	,	kIx,	,
1854	[number]	k4	1854
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Preludia	preludium	k1gNnPc1	preludium
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1848	[number]	k4	1848
<g/>
,	,	kIx,	,
1854	[number]	k4	1854
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Orpheus	Orpheus	k1gMnSc1	Orpheus
(	(	kIx(	(
<g/>
1853	[number]	k4	1853
<g/>
/	/	kIx~	/
<g/>
1854	[number]	k4	1854
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Prometheus	Prometheus	k1gMnSc1	Prometheus
(	(	kIx(	(
<g/>
1850	[number]	k4	1850
<g/>
,	,	kIx,	,
1855	[number]	k4	1855
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mazeppa	Mazeppa	k1gFnSc1	Mazeppa
(	(	kIx(	(
<g/>
1851	[number]	k4	1851
<g/>
,	,	kIx,	,
1854	[number]	k4	1854
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Slavnostní	slavnostní	k2eAgInPc1d1	slavnostní
tóny	tón	k1gInPc1	tón
(	(	kIx(	(
<g/>
1853	[number]	k4	1853
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Pohřeb	pohřeb	k1gInSc1	pohřeb
hrdinův	hrdinův	k2eAgInSc1d1	hrdinův
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
/	/	kIx~	/
<g/>
50	[number]	k4	50
<g/>
,	,	kIx,	,
1854	[number]	k4	1854
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hungaria	Hungarium	k1gNnPc1	Hungarium
(	(	kIx(	(
<g/>
1854	[number]	k4	1854
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hamlet	Hamlet	k1gMnSc1	Hamlet
(	(	kIx(	(
<g/>
1858	[number]	k4	1858
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bitva	bitva	k1gFnSc1	bitva
Hunů	Hun	k1gMnPc2	Hun
(	(	kIx(	(
<g/>
1856	[number]	k4	1856
<g/>
/	/	kIx~	/
<g/>
57	[number]	k4	57
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ideály	ideál	k1gInPc1	ideál
(	(	kIx(	(
<g/>
1857	[number]	k4	1857
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Od	od	k7c2	od
kolébky	kolébka	k1gFnSc2	kolébka
ke	k	k7c3	k
hrobu	hrob	k1gInSc3	hrob
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
/	/	kIx~	/
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
108	[number]	k4	108
Symfonie	symfonie	k1gFnSc1	symfonie
Faust	Faust	k1gFnSc1	Faust
(	(	kIx(	(
<g/>
1854	[number]	k4	1854
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
109	[number]	k4	109
Symfonie	symfonie	k1gFnSc1	symfonie
Dantova	Dantův	k2eAgFnSc1d1	Dantova
Božská	božský	k2eAgFnSc1d1	božská
komedie	komedie	k1gFnSc1	komedie
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
/	/	kIx~	/
<g/>
56	[number]	k4	56
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Klavírní	klavírní	k2eAgInPc4d1	klavírní
koncerty	koncert	k1gInPc4	koncert
===	===	k?	===
</s>
</p>
<p>
<s>
124	[number]	k4	124
1	[number]	k4	1
<g/>
.	.	kIx.	.
klavírní	klavírní	k2eAgInSc1d1	klavírní
koncert	koncert	k1gInSc1	koncert
Es-dur	Esura	k1gFnPc2	Es-dura
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
125	[number]	k4	125
2	[number]	k4	2
<g/>
.	.	kIx.	.
klavírní	klavírní	k2eAgInSc1d1	klavírní
koncert	koncert	k1gInSc1	koncert
A-dur	Aura	k1gFnPc2	A-dura
(	(	kIx(	(
<g/>
1839	[number]	k4	1839
<g/>
,	,	kIx,	,
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Skladby	skladba	k1gFnSc2	skladba
pro	pro	k7c4	pro
sólový	sólový	k2eAgInSc4d1	sólový
klavír	klavír	k1gInSc4	klavír
===	===	k?	===
</s>
</p>
<p>
<s>
139	[number]	k4	139
12	[number]	k4	12
transcendentálních	transcendentální	k2eAgFnPc2d1	transcendentální
etud	etuda	k1gFnPc2	etuda
(	(	kIx(	(
<g/>
1851	[number]	k4	1851
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
141	[number]	k4	141
Velké	velký	k2eAgFnPc4d1	velká
etudy	etuda	k1gFnPc4	etuda
podle	podle	k7c2	podle
Paganiniho	Paganini	k1gMnSc2	Paganini
(	(	kIx(	(
<g/>
1851	[number]	k4	1851
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
154	[number]	k4	154
Poetické	poetický	k2eAgInPc1d1	poetický
a	a	k8xC	a
náboženské	náboženský	k2eAgFnSc2d1	náboženská
harmonie	harmonie	k1gFnSc2	harmonie
(	(	kIx(	(
<g/>
1833	[number]	k4	1833
<g/>
/	/	kIx~	/
<g/>
1835	[number]	k4	1835
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
161	[number]	k4	161
Léta	léto	k1gNnPc1	léto
putování	putování	k1gNnSc2	putování
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
rok	rok	k1gInSc1	rok
(	(	kIx(	(
<g/>
1837	[number]	k4	1837
<g/>
–	–	k?	–
<g/>
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
178	[number]	k4	178
Sonáta	sonáta	k1gFnSc1	sonáta
h	h	k?	h
moll	moll	k1gNnSc1	moll
(	(	kIx(	(
<g/>
1852	[number]	k4	1852
<g/>
/	/	kIx~	/
<g/>
1853	[number]	k4	1853
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
192	[number]	k4	192
Pět	pět	k4xCc1	pět
klavírních	klavírní	k2eAgInPc2d1	klavírní
kusů	kus	k1gInPc2	kus
(	(	kIx(	(
<g/>
1865	[number]	k4	1865
<g/>
,	,	kIx,	,
1873	[number]	k4	1873
<g/>
,	,	kIx,	,
1876	[number]	k4	1876
<g/>
,	,	kIx,	,
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
223	[number]	k4	223
Dvě	dva	k4xCgFnPc1	dva
polonézy	polonéza	k1gFnPc1	polonéza
(	(	kIx(	(
<g/>
1851	[number]	k4	1851
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
242	[number]	k4	242
Uherské	uherský	k2eAgFnPc1d1	uherská
národní	národní	k2eAgFnPc1d1	národní
melodie	melodie	k1gFnPc1	melodie
(	(	kIx(	(
<g/>
4	[number]	k4	4
svazky	svazek	k1gInPc1	svazek
<g/>
,	,	kIx,	,
1839	[number]	k4	1839
<g/>
–	–	k?	–
<g/>
1847	[number]	k4	1847
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
244	[number]	k4	244
19	[number]	k4	19
uherských	uherský	k2eAgFnPc2d1	uherská
rapsodií	rapsodie	k1gFnPc2	rapsodie
(	(	kIx(	(
<g/>
1846	[number]	k4	1846
<g/>
–	–	k?	–
<g/>
1853	[number]	k4	1853
<g/>
,	,	kIx,	,
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
514	[number]	k4	514
První	první	k4xOgInSc4	první
Mefistotelův	Mefistotelův	k2eAgInSc4d1	Mefistotelův
valčík	valčík	k1gInSc4	valčík
(	(	kIx(	(
<g/>
1859	[number]	k4	1859
<g/>
/	/	kIx~	/
<g/>
60	[number]	k4	60
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
541	[number]	k4	541
Sny	sen	k1gInPc1	sen
lásky	láska	k1gFnSc2	láska
(	(	kIx(	(
<g/>
3	[number]	k4	3
nokturna	nokturno	k1gNnSc2	nokturno
<g/>
,	,	kIx,	,
1850	[number]	k4	1850
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Skladby	skladba	k1gFnPc1	skladba
pro	pro	k7c4	pro
varhany	varhany	k1gInPc4	varhany
===	===	k?	===
</s>
</p>
<p>
<s>
259	[number]	k4	259
Fantazie	fantazie	k1gFnSc1	fantazie
a	a	k8xC	a
fuga	fuga	k1gFnSc1	fuga
na	na	k7c4	na
chorál	chorál	k1gInSc4	chorál
Ad	ad	k7c4	ad
nos	nos	k1gInSc4	nos
salutarem	salutar	k1gMnSc7	salutar
undam	undam	k6eAd1	undam
(	(	kIx(	(
<g/>
1850	[number]	k4	1850
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
260	[number]	k4	260
Preludium	preludium	k1gNnSc1	preludium
a	a	k8xC	a
fuga	fuga	k1gFnSc1	fuga
na	na	k7c4	na
jméno	jméno	k1gNnSc4	jméno
BACH	BACH	kA	BACH
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
,	,	kIx,	,
1870	[number]	k4	1870
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Písně	píseň	k1gFnPc1	píseň
===	===	k?	===
</s>
</p>
<p>
<s>
296	[number]	k4	296
Chtěl	chtít	k5eAaImAgMnS	chtít
bych	by	kYmCp1nS	by
zahynout	zahynout	k5eAaPmF	zahynout
(	(	kIx(	(
<g/>
1845	[number]	k4	1845
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
320	[number]	k4	320
Tři	tři	k4xCgMnPc4	tři
cikáni	cikán	k1gMnPc1	cikán
(	(	kIx(	(
<g/>
1860	[number]	k4	1860
<g/>
)	)	kIx)	)
<g/>
Liszt	Liszt	k1gInSc1	Liszt
kromě	kromě	k7c2	kromě
toho	ten	k3xDgMnSc2	ten
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
množství	množství	k1gNnSc4	množství
klavírních	klavírní	k2eAgFnPc2d1	klavírní
transkripcí	transkripce	k1gFnPc2	transkripce
symfonických	symfonický	k2eAgFnPc2d1	symfonická
skladeb	skladba	k1gFnPc2	skladba
Schubertových	Schubertová	k1gFnPc2	Schubertová
<g/>
,	,	kIx,	,
Mozartových	Mozartová	k1gFnPc2	Mozartová
<g/>
,	,	kIx,	,
Wagnerových	Wagnerová	k1gFnPc2	Wagnerová
<g/>
,	,	kIx,	,
Schumannových	Schumannová	k1gFnPc2	Schumannová
<g/>
,	,	kIx,	,
Donizettiho	Donizetti	k1gMnSc2	Donizetti
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ALTENBURG	ALTENBURG	kA	ALTENBURG
<g/>
,	,	kIx,	,
Detlef	Detlef	k1gMnSc1	Detlef
<g/>
,	,	kIx,	,
KLEINERTZ	KLEINERTZ	kA	KLEINERTZ
<g/>
,	,	kIx,	,
Rainer	Rainer	k1gInSc1	Rainer
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Franz	Franz	k1gMnSc1	Franz
Liszt	Liszt	k1gMnSc1	Liszt
<g/>
.	.	kIx.	.
</s>
<s>
Sämtliche	Sämtliche	k6eAd1	Sämtliche
Schriften	Schriften	k2eAgInSc1d1	Schriften
<g/>
,	,	kIx,	,
Wiesbaden	Wiesbaden	k1gInSc1	Wiesbaden
1989	[number]	k4	1989
ff	ff	kA	ff
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
sebrané	sebraný	k2eAgInPc4d1	sebraný
spisy	spis	k1gInPc4	spis
<g/>
:	:	kIx,	:
9	[number]	k4	9
svazků	svazek	k1gInPc2	svazek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BUCHNER	BUCHNER	kA	BUCHNER
<g/>
,	,	kIx,	,
Alexander	Alexandra	k1gFnPc2	Alexandra
<g/>
:	:	kIx,	:
Franz	Franz	k1gMnSc1	Franz
Liszt	Liszt	k1gMnSc1	Liszt
in	in	k?	in
Böhmen	Böhmen	k1gInSc1	Böhmen
<g/>
,	,	kIx,	,
Artia	Artia	k1gFnSc1	Artia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
211	[number]	k4	211
str	str	kA	str
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DEMKO	DEMKO	kA	DEMKO
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
:	:	kIx,	:
Franz	Franz	k1gMnSc1	Franz
Liszt	Liszt	k1gMnSc1	Liszt
compositeur	compositeur	k1gMnSc1	compositeur
Slovaque	Slovaque	k1gFnSc1	Slovaque
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Age	Age	k1gMnSc1	Age
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Homme	Homm	k1gMnSc5	Homm
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DÖMLING	DÖMLING	kA	DÖMLING
<g/>
,	,	kIx,	,
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
<g/>
:	:	kIx,	:
Franz	Franz	k1gMnSc1	Franz
Liszt	Liszt	k1gMnSc1	Liszt
und	und	k?	und
seine	seinout	k5eAaPmIp3nS	seinout
Zeit	Zeit	k1gInSc1	Zeit
<g/>
,	,	kIx,	,
Laaberverlag	Laaberverlag	k1gInSc1	Laaberverlag
<g/>
,	,	kIx,	,
Laber	Laber	k1gInSc1	Laber
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
336	[number]	k4	336
str	str	kA	str
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Franz	Franz	k1gMnSc1	Franz
Liszt	Liszt	k1gMnSc1	Liszt
<g/>
,	,	kIx,	,
ein	ein	k?	ein
Genie	genius	k1gMnPc4	genius
aus	aus	k?	aus
dem	dem	k?	dem
pannonischen	pannonischen	k2eAgInSc1d1	pannonischen
Raum	Raum	k1gInSc1	Raum
<g/>
,	,	kIx,	,
Katalog	katalog	k1gInSc1	katalog
der	drát	k5eAaImRp2nS	drát
Landessonderausstellung	Landessonderausstellung	k1gMnSc1	Landessonderausstellung
Franz	Franz	k1gMnSc1	Franz
List	list	k1gInSc1	list
im	im	k?	im
Burgenländischen	Burgenländischen	k2eAgInSc1d1	Burgenländischen
Landesmuseum	Landesmuseum	k1gInSc1	Landesmuseum
<g/>
,	,	kIx,	,
Eisenstadt	Eisenstadt	k1gInSc1	Eisenstadt
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
168	[number]	k4	168
str	str	kA	str
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HAMBURGER	hamburger	k1gInSc1	hamburger
<g/>
,	,	kIx,	,
Klára	Klára	k1gFnSc1	Klára
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Franz	Franz	k1gMnSc1	Franz
Liszt	Liszt	k1gMnSc1	Liszt
<g/>
.	.	kIx.	.
</s>
<s>
Briefwechsel	Briefwechsel	k1gMnSc1	Briefwechsel
mit	mit	k?	mit
seiner	seiner	k1gMnSc1	seiner
Mutter	Mutter	k1gMnSc1	Mutter
<g/>
,	,	kIx,	,
Eisenstadt	Eisenstadt	k2eAgMnSc1d1	Eisenstadt
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
901	[number]	k4	901
<g/>
-	-	kIx~	-
<g/>
51722	[number]	k4	51722
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KAPP	KAPP	kA	KAPP
<g/>
,	,	kIx,	,
Julius	Julius	k1gMnSc1	Julius
<g/>
:	:	kIx,	:
Liszt	Liszt	k1gMnSc1	Liszt
<g/>
.	.	kIx.	.
</s>
<s>
Eine	Eine	k1gFnSc1	Eine
Biographie	Biographie	k1gFnSc1	Biographie
<g/>
,	,	kIx,	,
Berlín	Berlín	k1gInSc1	Berlín
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
307	[number]	k4	307
str	str	kA	str
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KNOTIK	KNOTIK	kA	KNOTIK
<g/>
,	,	kIx,	,
Cornelia	Cornelium	k1gNnSc2	Cornelium
<g/>
:	:	kIx,	:
<g/>
Musik	musika	k1gFnPc2	musika
und	und	k?	und
Religion	religion	k1gInSc1	religion
im	im	k?	im
Zeitalter	Zeitalter	k1gInSc1	Zeitalter
des	des	k1gNnSc1	des
Historismus	historismus	k1gInSc1	historismus
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gMnSc1	Franz
Lists	Listsa	k1gFnPc2	Listsa
Wende	Wend	k1gInSc5	Wend
zum	zum	k?	zum
Oratorienschaffen	Oratorienschaffen	k2eAgInSc1d1	Oratorienschaffen
als	als	k?	als
ästetisches	ästetisches	k1gInSc1	ästetisches
Problem	Problo	k1gNnSc7	Problo
<g/>
,	,	kIx,	,
Burgenländisches	Burgenländisches	k1gInSc1	Burgenländisches
Landesmuseum	Landesmuseum	k1gInSc1	Landesmuseum
<g/>
,	,	kIx,	,
Eisenstadt	Eisenstadt	k1gInSc1	Eisenstadt
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
98	[number]	k4	98
str	str	kA	str
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ÓVÁRI	ÓVÁRI	kA	ÓVÁRI
<g/>
,	,	kIx,	,
Jószef	Jószef	k1gMnSc1	Jószef
<g/>
:	:	kIx,	:
Ferenz	Ferenz	k1gMnSc1	Ferenz
Liszt	Liszt	k1gMnSc1	Liszt
<g/>
.	.	kIx.	.
</s>
<s>
Eine	Eine	k6eAd1	Eine
leichtverständliche	leichtverständlichat	k5eAaPmIp3nS	leichtverständlichat
Biographie	Biographie	k1gFnPc4	Biographie
des	des	k1gNnPc2	des
grossen	grossen	k2eAgInSc1d1	grossen
ungarischen	ungarischen	k1gInSc1	ungarischen
Komponisten	Komponisten	k2eAgInSc1d1	Komponisten
<g/>
,	,	kIx,	,
Püski	Püsk	k1gInSc3	Püsk
Kiadó	Kiadó	k1gFnPc2	Kiadó
<g/>
,	,	kIx,	,
Budapest	Budapest	k1gFnSc1	Budapest
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
432	[number]	k4	432
str	str	kA	str
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
REHBERG	REHBERG	kA	REHBERG
<g/>
,	,	kIx,	,
Paula	Paula	k1gFnSc1	Paula
<g/>
:	:	kIx,	:
Liszt	Liszt	k1gInSc1	Liszt
<g/>
.	.	kIx.	.
</s>
<s>
Eine	Eine	k1gFnSc1	Eine
Biographie	Biographie	k1gFnSc1	Biographie
<g/>
,	,	kIx,	,
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
,	,	kIx,	,
Goldmann	Goldmann	k1gMnSc1	Goldmann
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
GmbH	GmbH	k1gMnSc1	GmbH
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
731	[number]	k4	731
str	str	kA	str
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
442	[number]	k4	442
<g/>
-	-	kIx~	-
<g/>
33005	[number]	k4	33005
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SEARLE	SEARLE	kA	SEARLE
<g/>
,	,	kIx,	,
Humphrey	Humphrea	k1gFnSc2	Humphrea
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Music	Music	k1gMnSc1	Music
of	of	k?	of
Liszt	Liszt	k1gMnSc1	Liszt
<g/>
,	,	kIx,	,
Courier	Courier	k1gMnSc1	Courier
Corporation	Corporation	k1gInSc1	Corporation
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
224	[number]	k4	224
str	str	kA	str
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-0-486-78640-7	[number]	k4	978-0-486-78640-7
(	(	kIx(	(
<g/>
seznam	seznam	k1gInSc1	seznam
díla	dílo	k1gNnSc2	dílo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WALKER	WALKER	kA	WALKER
<g/>
,	,	kIx,	,
Allan	Allan	k1gMnSc1	Allan
<g/>
:	:	kIx,	:
Franz	Franz	k1gMnSc1	Franz
Liszt	Liszt	k1gMnSc1	Liszt
<g/>
:	:	kIx,	:
<g/>
Sv.	sv.	kA	sv.
1	[number]	k4	1
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Virtuoso	Virtuosa	k1gFnSc5	Virtuosa
Years	Yearsa	k1gFnPc2	Yearsa
1811	[number]	k4	1811
<g/>
–	–	k?	–
<g/>
1847	[number]	k4	1847
<g/>
,	,	kIx,	,
Alfred	Alfred	k1gMnSc1	Alfred
A.	A.	kA	A.
Knopf	Knopf	k1gMnSc1	Knopf
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
480	[number]	k4	480
str	str	kA	str
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
307	[number]	k4	307
<g/>
-	-	kIx~	-
<g/>
83096	[number]	k4	83096
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sv.	sv.	kA	sv.
2	[number]	k4	2
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Weimar	Weimar	k1gInSc4	Weimar
Years	Years	k1gInSc1	Years
1848	[number]	k4	1848
<g/>
–	–	k?	–
<g/>
1861	[number]	k4	1861
<g/>
,	,	kIx,	,
Cornell	Cornell	k1gMnSc1	Cornell
University	universita	k1gFnSc2	universita
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
Ithaca	Ithaca	k1gFnSc1	Ithaca
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
656	[number]	k4	656
str	str	kA	str
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
801	[number]	k4	801
<g/>
-	-	kIx~	-
<g/>
49721	[number]	k4	49721
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sv.	sv.	kA	sv.
3	[number]	k4	3
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Final	Final	k1gInSc4	Final
Years	Years	k1gInSc1	Years
1861	[number]	k4	1861
<g/>
–	–	k?	–
<g/>
1886	[number]	k4	1886
<g/>
,	,	kIx,	,
Cornell	Cornell	k1gMnSc1	Cornell
University	universita	k1gFnSc2	universita
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
Ithaca	Ithaca	k1gFnSc1	Ithaca
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
624	[number]	k4	624
str	str	kA	str
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
801	[number]	k4	801
<g/>
-	-	kIx~	-
<g/>
48453	[number]	k4	48453
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Symfonická	symfonický	k2eAgFnSc1d1	symfonická
báseň	báseň	k1gFnSc1	báseň
</s>
</p>
<p>
<s>
Klavírní	klavírní	k2eAgFnSc1d1	klavírní
sonáta	sonáta	k1gFnSc1	sonáta
h	h	k?	h
moll	moll	k1gNnSc1	moll
(	(	kIx(	(
<g/>
Liszt	Liszt	k1gInSc1	Liszt
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Programní	programní	k2eAgFnSc1d1	programní
hudba	hudba	k1gFnSc1	hudba
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Wagner	Wagner	k1gMnSc1	Wagner
</s>
</p>
<p>
<s>
Hans	Hans	k1gMnSc1	Hans
von	von	k1gInSc4	von
Bülow	Bülow	k1gFnSc2	Bülow
</s>
</p>
<p>
<s>
Výmar	Výmar	k1gInSc1	Výmar
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ferenc	Ferenc	k1gMnSc1	Ferenc
Liszt	Liszt	k2eAgMnSc1d1	Liszt
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Ferenc	Ferenc	k1gMnSc1	Ferenc
Liszt	Liszt	k1gMnSc1	Liszt
</s>
</p>
<p>
<s>
Volně	volně	k6eAd1	volně
přístupné	přístupný	k2eAgFnPc4d1	přístupná
partitury	partitura	k1gFnPc4	partitura
děl	dít	k5eAaImAgMnS	dít
od	od	k7c2	od
F.	F.	kA	F.
Liszta	Liszt	k1gInSc2	Liszt
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
IMSLP	IMSLP	kA	IMSLP
</s>
</p>
<p>
<s>
Ferenc	Ferenc	k1gMnSc1	Ferenc
Liszt	Liszt	k1gMnSc1	Liszt
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Musopen	Musopen	k2eAgMnSc1d1	Musopen
</s>
</p>
<p>
<s>
Liszt	Liszt	k1gInSc1	Liszt
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
města	město	k1gNnSc2	město
Opavy	Opava	k1gFnSc2	Opava
</s>
</p>
<p>
<s>
Ferenc	Ferenc	k1gMnSc1	Ferenc
Liszt	Liszt	k1gMnSc1	Liszt
–	–	k?	–
životopis	životopis	k1gInSc1	životopis
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
J.	J.	kA	J.
Kratochvíla	Kratochvíl	k1gMnSc2	Kratochvíl
o	o	k7c6	o
provedení	provedení	k1gNnSc6	provedení
Lisztovy	Lisztův	k2eAgFnSc2d1	Lisztova
opery	opera	k1gFnSc2	opera
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
Vlasty	Vlasta	k1gFnSc2	Vlasta
Reitterové	Reitterová	k1gFnSc2	Reitterová
</s>
</p>
<p>
<s>
Portrét	portrét	k1gInSc1	portrét
umělce	umělec	k1gMnSc2	umělec
na	na	k7c6	na
serveru	server	k1gInSc6	server
muzicus	muzicus	k1gInSc1	muzicus
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
Lisztova	Lisztův	k2eAgNnSc2d1	Lisztovo
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
H.	H.	kA	H.
Stejskalové	Stejskalová	k1gFnSc2	Stejskalová
o	o	k7c6	o
skladatelově	skladatelův	k2eAgInSc6d1	skladatelův
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
ženám	žena	k1gFnPc3	žena
</s>
</p>
<p>
<s>
Heslo	heslo	k1gNnSc1	heslo
na	na	k7c4	na
encyklopedii	encyklopedie	k1gFnSc4	encyklopedie
Britannica	Britannic	k1gInSc2	Britannic
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Podrobný	podrobný	k2eAgInSc1d1	podrobný
životopis	životopis	k1gInSc1	životopis
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
