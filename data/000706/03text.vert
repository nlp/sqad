<s>
Vratné	vratný	k2eAgFnPc4d1	vratná
lahve	lahev	k1gFnPc4	lahev
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Jana	Jan	k1gMnSc2	Jan
Svěráka	Svěrák	k1gMnSc2	Svěrák
<g/>
,	,	kIx,	,
natočený	natočený	k2eAgInSc1d1	natočený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
podle	podle	k7c2	podle
scénáře	scénář	k1gInSc2	scénář
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Svěráka	Svěrák	k1gMnSc2	Svěrák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgInS	být
film	film	k1gInSc1	film
oceněn	oceněn	k2eAgInSc1d1	oceněn
3	[number]	k4	3
Českými	český	k2eAgInPc7d1	český
lvy	lev	k1gInPc7	lev
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Tkaloun	tkaloun	k1gInSc1	tkaloun
(	(	kIx(	(
<g/>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pětašedesátiletý	pětašedesátiletý	k2eAgMnSc1d1	pětašedesátiletý
učitel	učitel	k1gMnSc1	učitel
češtiny	čeština	k1gFnSc2	čeština
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
mu	on	k3xPp3gNnSc3	on
už	už	k9	už
počtvrté	počtvrté	k4xO	počtvrté
ujedou	ujet	k5eAaPmIp3nP	ujet
nervy	nerv	k1gInPc1	nerv
a	a	k8xC	a
drzého	drzý	k2eAgMnSc2d1	drzý
žáka	žák	k1gMnSc2	žák
zkrotí	zkrotit	k5eAaPmIp3nS	zkrotit
svéráznou	svérázný	k2eAgFnSc7d1	svérázná
metodou	metoda	k1gFnSc7	metoda
(	(	kIx(	(
<g/>
vyždímá	vyždímat	k5eAaPmIp3nS	vyždímat
mu	on	k3xPp3gMnSc3	on
houbu	houba	k1gFnSc4	houba
na	na	k7c6	na
tabuli	tabule	k1gFnSc6	tabule
nad	nad	k7c7	nad
hlavou	hlava	k1gFnSc7	hlava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
raději	rád	k6eAd2	rád
školu	škola	k1gFnSc4	škola
opouští	opouštět	k5eAaImIp3nS	opouštět
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
nevydrží	vydržet	k5eNaPmIp3nP	vydržet
jen	jen	k9	jen
tak	tak	k6eAd1	tak
nečinně	činně	k6eNd1	činně
užívat	užívat	k5eAaImF	užívat
si	se	k3xPyFc3	se
důchodu	důchod	k1gInSc2	důchod
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
si	se	k3xPyFc3	se
vzápětí	vzápětí	k6eAd1	vzápětí
ke	k	k7c3	k
zděšení	zděšení	k1gNnSc3	zděšení
manželky	manželka	k1gFnSc2	manželka
(	(	kIx(	(
<g/>
Daniela	Daniela	k1gFnSc1	Daniela
Kolářová	Kolářová	k1gFnSc1	Kolářová
<g/>
)	)	kIx)	)
nachází	nacházet	k5eAaImIp3nS	nacházet
novou	nový	k2eAgFnSc4d1	nová
práci	práce	k1gFnSc4	práce
-	-	kIx~	-
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
messengerem	messengero	k1gNnSc7	messengero
<g/>
,	,	kIx,	,
poslem	posel	k1gMnSc7	posel
na	na	k7c6	na
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Náledí	náledí	k1gNnSc4	náledí
a	a	k8xC	a
bezohledné	bezohledný	k2eAgNnSc4d1	bezohledné
auto	auto	k1gNnSc4	auto
však	však	k9	však
vykonají	vykonat	k5eAaPmIp3nP	vykonat
svoje	své	k1gNnSc1	své
-	-	kIx~	-
rychlý	rychlý	k2eAgMnSc1d1	rychlý
posel	posel	k1gMnSc1	posel
po	po	k7c6	po
krkolomném	krkolomný	k2eAgInSc6d1	krkolomný
pádu	pád	k1gInSc6	pád
končí	končit	k5eAaImIp3nS	končit
na	na	k7c6	na
lůžku	lůžko	k1gNnSc6	lůžko
<g/>
.	.	kIx.	.
</s>
<s>
Dřív	dříve	k6eAd2	dříve
než	než	k8xS	než
odloží	odložit	k5eAaPmIp3nS	odložit
berle	berle	k1gFnSc1	berle
<g/>
,	,	kIx,	,
opatří	opatřit	k5eAaPmIp3nS	opatřit
si	se	k3xPyFc3	se
tentokrát	tentokrát	k6eAd1	tentokrát
bezpečnější	bezpečný	k2eAgNnSc4d2	bezpečnější
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
-	-	kIx~	-
ve	v	k7c6	v
výkupu	výkup	k1gInSc6	výkup
lahví	lahev	k1gFnPc2	lahev
v	v	k7c6	v
supermarketu	supermarket	k1gInSc6	supermarket
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
okénka	okénko	k1gNnSc2	okénko
má	mít	k5eAaImIp3nS	mít
přehled	přehled	k1gInSc4	přehled
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
si	se	k3xPyFc3	se
popovídat	popovídat	k5eAaPmF	popovídat
se	s	k7c7	s
zákazníky	zákazník	k1gMnPc7	zákazník
a	a	k8xC	a
zákaznicemi	zákaznice	k1gFnPc7	zákaznice
a	a	k8xC	a
především	především	k6eAd1	především
dá	dát	k5eAaPmIp3nS	dát
dohromady	dohromady	k6eAd1	dohromady
svého	svůj	k3xOyFgMnSc4	svůj
spolupracovníka	spolupracovník	k1gMnSc4	spolupracovník
přezdívaného	přezdívaný	k2eAgInSc2d1	přezdívaný
Mluvka	mluvka	k1gMnSc1	mluvka
(	(	kIx(	(
<g/>
Pavel	Pavel	k1gMnSc1	Pavel
Landovský	Landovský	k2eAgMnSc1d1	Landovský
<g/>
)	)	kIx)	)
s	s	k7c7	s
paní	paní	k1gFnSc7	paní
Kvardovou	Kvardová	k1gFnSc7	Kvardová
<g/>
.	.	kIx.	.
</s>
<s>
Novou	nový	k2eAgFnSc4d1	nová
známost	známost	k1gFnSc4	známost
dohodí	dohodit	k5eAaPmIp3nS	dohodit
i	i	k9	i
své	svůj	k3xOyFgFnSc3	svůj
dceři	dcera	k1gFnSc3	dcera
Helence	Helenka	k1gFnSc3	Helenka
(	(	kIx(	(
<g/>
Tatiana	Tatiana	k1gFnSc1	Tatiana
Vilhelmová	Vilhelmová	k1gFnSc1	Vilhelmová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
opustil	opustit	k5eAaPmAgMnS	opustit
manžel	manžel	k1gMnSc1	manžel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vlastní	vlastní	k2eAgInSc1d1	vlastní
sexuální	sexuální	k2eAgInSc1d1	sexuální
život	život	k1gInSc1	život
se	se	k3xPyFc4	se
však	však	k9	však
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
už	už	k6eAd1	už
jen	jen	k9	jen
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
fantazii	fantazie	k1gFnSc6	fantazie
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
však	však	k9	však
nahrazen	nahradit	k5eAaPmNgInS	nahradit
automatickým	automatický	k2eAgInSc7d1	automatický
přijímačem	přijímač	k1gInSc7	přijímač
lahví	lahev	k1gFnPc2	lahev
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
i	i	k9	i
ze	z	k7c2	z
supermarketu	supermarket	k1gInSc2	supermarket
odejde	odejít	k5eAaPmIp3nS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
manželku	manželka	k1gFnSc4	manželka
zaskočí	zaskočit	k5eAaPmIp3nS	zaskočit
výletem	výlet	k1gInSc7	výlet
s	s	k7c7	s
překvapením	překvapení	k1gNnSc7	překvapení
k	k	k7c3	k
jejich	jejich	k3xOp3gMnPc3	jejich
čtyřicátému	čtyřicátý	k4xOgInSc3	čtyřicátý
výročí	výročí	k1gNnSc3	výročí
svatby	svatba	k1gFnSc2	svatba
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
o	o	k7c4	o
překvapení	překvapení	k1gNnSc4	překvapení
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
výletu	výlet	k1gInSc6	výlet
skutečně	skutečně	k6eAd1	skutečně
nebude	být	k5eNaImBp3nS	být
nouze	nouze	k1gFnSc1	nouze
<g/>
.	.	kIx.	.
</s>
<s>
Námět	námět	k1gInSc1	námět
<g/>
:	:	kIx,	:
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
Scénář	scénář	k1gInSc1	scénář
<g/>
:	:	kIx,	:
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
Hudba	hudba	k1gFnSc1	hudba
<g/>
:	:	kIx,	:
Ondřej	Ondřej	k1gMnSc1	Ondřej
Soukup	Soukup	k1gMnSc1	Soukup
Kamera	kamera	k1gFnSc1	kamera
<g/>
:	:	kIx,	:
Vladimír	Vladimír	k1gMnSc1	Vladimír
Smutný	Smutný	k1gMnSc1	Smutný
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Svěrák	Svěrák	k1gMnSc1	Svěrák
Další	další	k2eAgInPc4d1	další
údaje	údaj	k1gInPc4	údaj
<g/>
:	:	kIx,	:
barevný	barevný	k2eAgInSc4d1	barevný
<g/>
,	,	kIx,	,
95	[number]	k4	95
min	mina	k1gFnPc2	mina
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnSc2	komedie
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
původně	původně	k6eAd1	původně
natáčen	natáčet	k5eAaImNgInS	natáčet
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
Jan	Jan	k1gMnSc1	Jan
Svěrák	svěrák	k1gInSc4	svěrák
původní	původní	k2eAgInSc4d1	původní
otcův	otcův	k2eAgInSc4d1	otcův
scénář	scénář	k1gInSc4	scénář
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
spor	spor	k1gInSc4	spor
nad	nad	k7c7	nad
scénářem	scénář	k1gInSc7	scénář
Svěrákovi	Svěrákův	k2eAgMnPc1d1	Svěrákův
dokonce	dokonce	k9	dokonce
zveřejnili	zveřejnit	k5eAaPmAgMnP	zveřejnit
v	v	k7c6	v
portrétním	portrétní	k2eAgInSc6d1	portrétní
dokumentu	dokument	k1gInSc6	dokument
Tatínek	tatínek	k1gMnSc1	tatínek
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
po	po	k7c6	po
ročním	roční	k2eAgInSc6d1	roční
odkladu	odklad	k1gInSc6	odklad
napsal	napsat	k5eAaPmAgMnS	napsat
další	další	k2eAgFnSc4d1	další
<g/>
,	,	kIx,	,
pátou	pátý	k4xOgFnSc4	pátý
verzi	verze	k1gFnSc4	verze
scénáře	scénář	k1gInSc2	scénář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2005	[number]	k4	2005
se	se	k3xPyFc4	se
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
film	film	k1gInSc1	film
natočen	natočit	k5eAaBmNgInS	natočit
bude	být	k5eAaImBp3nS	být
a	a	k8xC	a
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2006	[number]	k4	2006
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
natáčet	natáčet	k5eAaImF	natáčet
<g/>
.	.	kIx.	.
</s>
<s>
Policie	policie	k1gFnSc1	policie
zadržela	zadržet	k5eAaPmAgFnS	zadržet
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pořídil	pořídit	k5eAaPmAgMnS	pořídit
kopii	kopie	k1gFnSc4	kopie
filmu	film	k1gInSc2	film
původně	původně	k6eAd1	původně
určenou	určený	k2eAgFnSc4d1	určená
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
společníkem	společník	k1gMnSc7	společník
umístili	umístit	k5eAaPmAgMnP	umístit
na	na	k7c4	na
internet	internet	k1gInSc4	internet
<g/>
.	.	kIx.	.
</s>
<s>
Stíhání	stíhání	k1gNnSc1	stíhání
obou	dva	k4xCgMnPc2	dva
mužů	muž	k1gMnPc2	muž
bylo	být	k5eAaImAgNnS	být
posléze	posléze	k6eAd1	posléze
zastaveno	zastavit	k5eAaPmNgNnS	zastavit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
k	k	k7c3	k
činu	čin	k1gInSc3	čin
přiznali	přiznat	k5eAaPmAgMnP	přiznat
a	a	k8xC	a
s	s	k7c7	s
vlastníky	vlastník	k1gMnPc7	vlastník
filmu	film	k1gInSc2	film
se	se	k3xPyFc4	se
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c6	na
finančním	finanční	k2eAgNnSc6d1	finanční
vyrovnání	vyrovnání	k1gNnSc6	vyrovnání
<g/>
.	.	kIx.	.
</s>
<s>
Soud	soud	k1gInSc1	soud
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zastavení	zastavení	k1gNnSc6	zastavení
stíhání	stíhání	k1gNnSc2	stíhání
bude	být	k5eAaImBp3nS	být
podmínečné	podmínečný	k2eAgNnSc1d1	podmínečné
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
18	[number]	k4	18
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
