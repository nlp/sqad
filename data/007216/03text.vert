<s>
Kontaktní	kontaktní	k2eAgFnSc1d1	kontaktní
čočka	čočka	k1gFnSc1	čočka
je	být	k5eAaImIp3nS	být
moderní	moderní	k2eAgFnSc1d1	moderní
optická	optický	k2eAgFnSc1d1	optická
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
pomůcka	pomůcka	k1gFnSc1	pomůcka
určená	určený	k2eAgFnSc1d1	určená
k	k	k7c3	k
nasazení	nasazení	k1gNnSc3	nasazení
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
oční	oční	k2eAgFnSc4d1	oční
rohovku	rohovka	k1gFnSc4	rohovka
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
především	především	k9	především
ke	k	k7c3	k
korekci	korekce	k1gFnSc3	korekce
optických	optický	k2eAgFnPc2d1	optická
vad	vada	k1gFnPc2	vada
zraku	zrak	k1gInSc2	zrak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
krátkozrakost	krátkozrakost	k1gFnSc4	krátkozrakost
<g/>
,	,	kIx,	,
dalekozrakost	dalekozrakost	k1gFnSc1	dalekozrakost
<g/>
,	,	kIx,	,
astigmatismus	astigmatismus	k1gInSc1	astigmatismus
nebo	nebo	k8xC	nebo
presbyopie	presbyopie	k1gFnSc1	presbyopie
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
někdy	někdy	k6eAd1	někdy
zrak	zrak	k1gInSc1	zrak
nefunguje	fungovat	k5eNaImIp3nS	fungovat
tak	tak	k6eAd1	tak
jak	jak	k6eAd1	jak
má	mít	k5eAaImIp3nS	mít
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
že	že	k8xS	že
světelné	světelný	k2eAgInPc1d1	světelný
paprsky	paprsek	k1gInPc1	paprsek
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
do	do	k7c2	do
oka	oko	k1gNnSc2	oko
vstupovat	vstupovat	k5eAaImF	vstupovat
panenkou	panenka	k1gFnSc7	panenka
a	a	k8xC	a
měly	mít	k5eAaImAgFnP	mít
by	by	kYmCp3nP	by
být	být	k5eAaImF	být
zaostřeny	zaostřit	k5eAaPmNgInP	zaostřit
rohovkou	rohovka	k1gFnSc7	rohovka
a	a	k8xC	a
čočkou	čočka	k1gFnSc7	čočka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
se	se	k3xPyFc4	se
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
nestane	stanout	k5eNaPmIp3nS	stanout
<g/>
.	.	kIx.	.
</s>
<s>
Čočka	čočka	k1gFnSc1	čočka
ohýbá	ohýbat	k5eAaImIp3nS	ohýbat
světlo	světlo	k1gNnSc4	světlo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
jí	on	k3xPp3gFnSc7	on
prochází	procházet	k5eAaImIp3nS	procházet
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
převrací	převracet	k5eAaImIp3nS	převracet
obraz	obraz	k1gInSc4	obraz
a	a	k8xC	a
zaostřuje	zaostřovat	k5eAaImIp3nS	zaostřovat
jej	on	k3xPp3gMnSc4	on
na	na	k7c6	na
sítnici	sítnice	k1gFnSc6	sítnice
<g/>
.	.	kIx.	.
</s>
<s>
Mozek	mozek	k1gInSc1	mozek
potom	potom	k6eAd1	potom
převrací	převracet	k5eAaImIp3nS	převracet
obraz	obraz	k1gInSc1	obraz
do	do	k7c2	do
správné	správný	k2eAgFnSc2d1	správná
polohy	poloha	k1gFnSc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
obraz	obraz	k1gInSc1	obraz
nedopadne	dopadnout	k5eNaPmIp3nS	dopadnout
do	do	k7c2	do
správného	správný	k2eAgInSc2d1	správný
bodu	bod	k1gInSc2	bod
na	na	k7c6	na
sítnici	sítnice	k1gFnSc6	sítnice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rozmazaný	rozmazaný	k2eAgMnSc1d1	rozmazaný
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
má	mít	k5eAaImIp3nS	mít
funkci	funkce	k1gFnSc4	funkce
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
při	při	k7c6	při
některých	některý	k3yIgNnPc6	některý
očních	oční	k2eAgNnPc6d1	oční
onemocněních	onemocnění	k1gNnPc6	onemocnění
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kosmetickou	kosmetický	k2eAgFnSc4d1	kosmetická
v	v	k7c6	v
případě	případ	k1gInSc6	případ
změny	změna	k1gFnSc2	změna
barvy	barva	k1gFnSc2	barva
oka	oko	k1gNnSc2	oko
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
barevné	barevný	k2eAgFnSc2d1	barevná
kontaktní	kontaktní	k2eAgFnSc2d1	kontaktní
čočky	čočka	k1gFnSc2	čočka
<g/>
.	.	kIx.	.
</s>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
zrakového	zrakový	k2eAgNnSc2d1	zrakové
ústrojí	ústrojí	k1gNnSc2	ústrojí
přijímá	přijímat	k5eAaImIp3nS	přijímat
člověk	člověk	k1gMnSc1	člověk
asi	asi	k9	asi
80	[number]	k4	80
<g/>
%	%	kIx~	%
všech	všecek	k3xTgFnPc2	všecek
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
kontaktní	kontaktní	k2eAgFnPc1d1	kontaktní
čočky	čočka	k1gFnPc1	čočka
velmi	velmi	k6eAd1	velmi
využívané	využívaný	k2eAgFnPc1d1	využívaná
a	a	k8xC	a
ulehčují	ulehčovat	k5eAaImIp3nP	ulehčovat
běžný	běžný	k2eAgInSc4d1	běžný
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
brýle	brýle	k1gFnPc4	brýle
si	se	k3xPyFc3	se
např.	např.	kA	např.
do	do	k7c2	do
bazénu	bazén	k1gInSc2	bazén
nevezmete	vzít	k5eNaPmIp2nP	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Kontaktní	kontaktní	k2eAgFnPc1d1	kontaktní
čočky	čočka	k1gFnPc1	čočka
existují	existovat	k5eAaImIp3nP	existovat
již	již	k6eAd1	již
od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
za	za	k7c4	za
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
díky	díky	k7c3	díky
modernějším	moderní	k2eAgFnPc3d2	modernější
technologiím	technologie	k1gFnPc3	technologie
značně	značně	k6eAd1	značně
změnily	změnit	k5eAaPmAgFnP	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Dají	dát	k5eAaPmIp3nP	dát
se	se	k3xPyFc4	se
dělit	dělit	k5eAaImF	dělit
podle	podle	k7c2	podle
mnoha	mnoho	k4c2	mnoho
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
nejzákladnější	základní	k2eAgNnSc1d3	nejzákladnější
dělení	dělení	k1gNnSc1	dělení
je	být	k5eAaImIp3nS	být
dělení	dělení	k1gNnSc4	dělení
podle	podle	k7c2	podle
materiálu	materiál	k1gInSc2	materiál
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
velké	velká	k1gFnPc4	velká
skupiny	skupina	k1gFnSc2	skupina
–	–	k?	–
tvrdé	tvrdý	k2eAgFnSc2d1	tvrdá
a	a	k8xC	a
měkké	měkký	k2eAgFnSc2d1	měkká
kontaktní	kontaktní	k2eAgFnSc2d1	kontaktní
čočky	čočka	k1gFnSc2	čočka
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
např.	např.	kA	např.
podle	podle	k7c2	podle
doby	doba	k1gFnSc2	doba
nošení	nošení	k1gNnSc2	nošení
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jak	jak	k8xS	jak
dlouho	dlouho	k6eAd1	dlouho
lze	lze	k6eAd1	lze
jednu	jeden	k4xCgFnSc4	jeden
čočku	čočka	k1gFnSc4	čočka
používat	používat	k5eAaImF	používat
<g/>
,	,	kIx,	,
a	a	k8xC	a
četnosti	četnost	k1gFnSc3	četnost
výměny	výměna	k1gFnSc2	výměna
-	-	kIx~	-
jak	jak	k8xS	jak
často	často	k6eAd1	často
se	se	k3xPyFc4	se
z	z	k7c2	z
oka	oko	k1gNnSc2	oko
musí	muset	k5eAaImIp3nS	muset
vyjímat	vyjímat	k5eAaImF	vyjímat
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
ji	on	k3xPp3gFnSc4	on
lze	lze	k6eAd1	lze
nosit	nosit	k5eAaImF	nosit
jen	jen	k9	jen
přes	přes	k7c4	přes
den	den	k1gInSc4	den
nebo	nebo	k8xC	nebo
kontinuálně	kontinuálně	k6eAd1	kontinuálně
i	i	k9	i
přes	přes	k7c4	přes
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Éra	éra	k1gFnSc1	éra
kontaktních	kontaktní	k2eAgFnPc2d1	kontaktní
čoček	čočka	k1gFnPc2	čočka
se	se	k3xPyFc4	se
započala	započnout	k5eAaPmAgFnS	započnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
výrobou	výroba	k1gFnSc7	výroba
první	první	k4xOgFnSc2	první
tvrdé	tvrdá	k1gFnSc2	tvrdá
kontaktní	kontaktní	k2eAgFnSc2d1	kontaktní
čočky	čočka	k1gFnSc2	čočka
ze	z	k7c2	z
skla	sklo	k1gNnSc2	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
byl	být	k5eAaImAgMnS	být
tento	tento	k3xDgInSc4	tento
model	model	k1gInSc4	model
skleněné	skleněný	k2eAgFnSc2d1	skleněná
čočky	čočka	k1gFnSc2	čočka
vylepšován	vylepšován	k2eAgMnSc1d1	vylepšován
<g/>
,	,	kIx,	,
např.	např.	kA	např.
podle	podle	k7c2	podle
odlitku	odlitek	k1gInSc2	odlitek
rohovky	rohovka	k1gFnSc2	rohovka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
čočka	čočka	k1gFnSc1	čočka
na	na	k7c6	na
oku	oko	k1gNnSc6	oko
lépe	dobře	k6eAd2	dobře
seděla	sedět	k5eAaImAgFnS	sedět
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
skleněné	skleněný	k2eAgFnPc1d1	skleněná
kontaktní	kontaktní	k2eAgFnPc1d1	kontaktní
čočky	čočka	k1gFnPc1	čočka
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
míru	mír	k1gInSc6	mír
pacientovi	pacient	k1gMnSc3	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vynálezu	vynález	k1gInSc6	vynález
plexiskla	plexisklo	k1gNnSc2	plexisklo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
se	se	k3xPyFc4	se
kontaktní	kontaktní	k2eAgFnPc1d1	kontaktní
čočky	čočka	k1gFnPc1	čočka
začaly	začít	k5eAaPmAgFnP	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
i	i	k9	i
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
lehčího	lehký	k2eAgInSc2d2	lehčí
materiálu	materiál	k1gInSc2	materiál
(	(	kIx(	(
<g/>
PMMA	PMMA	kA	PMMA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
napomohlo	napomoct	k5eAaPmAgNnS	napomoct
k	k	k7c3	k
lepší	dobrý	k2eAgFnSc3d2	lepší
centraci	centrace	k1gFnSc3	centrace
čočky	čočka	k1gFnSc2	čočka
na	na	k7c6	na
oku	oko	k1gNnSc6	oko
<g/>
,	,	kIx,	,
než	než	k8xS	než
jak	jak	k8xS	jak
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
doposud	doposud	k6eAd1	doposud
s	s	k7c7	s
těžšími	těžký	k2eAgFnPc7d2	těžší
skleněnými	skleněný	k2eAgFnPc7d1	skleněná
čočkami	čočka	k1gFnPc7	čočka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1952-55	[number]	k4	1952-55
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
pracovali	pracovat	k5eAaImAgMnP	pracovat
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
vhodných	vhodný	k2eAgInPc2d1	vhodný
polymerů	polymer	k1gInPc2	polymer
Otto	Otto	k1gMnSc1	Otto
Wichterle	Wichterl	k1gMnSc2	Wichterl
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
profesor	profesor	k1gMnSc1	profesor
na	na	k7c4	na
VŠCHT	VŠCHT	kA	VŠCHT
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
a	a	k8xC	a
Drahoslav	Drahoslava	k1gFnPc2	Drahoslava
Lím	Lím	k1gMnSc2	Lím
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
žák	žák	k1gMnSc1	žák
a	a	k8xC	a
asistent	asistent	k1gMnSc1	asistent
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
traduje	tradovat	k5eAaImIp3nS	tradovat
historka	historka	k1gFnSc1	historka
objevu	objev	k1gInSc2	objev
polymeru	polymer	k1gInSc2	polymer
pro	pro	k7c4	pro
kontaktní	kontaktní	k2eAgFnPc4d1	kontaktní
čočky	čočka	k1gFnPc4	čočka
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
Lím	Lím	k1gMnSc1	Lím
nestihl	stihnout	k5eNaPmAgMnS	stihnout
dokončit	dokončit	k5eAaPmF	dokončit
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
ten	ten	k3xDgInSc4	ten
den	den	k1gInSc4	den
určil	určit	k5eAaPmAgMnS	určit
<g/>
,	,	kIx,	,
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
syntetizovaný	syntetizovaný	k2eAgInSc4d1	syntetizovaný
produkt	produkt	k1gInSc4	produkt
nedodělal	dodělat	k5eNaPmAgMnS	dodělat
a	a	k8xC	a
pospíchal	pospíchat	k5eAaImAgMnS	pospíchat
na	na	k7c4	na
vlak	vlak	k1gInSc4	vlak
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
do	do	k7c2	do
Senohrab	Senohraba	k1gFnPc2	Senohraba
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
den	den	k1gInSc1	den
ráno	ráno	k6eAd1	ráno
zjistil	zjistit	k5eAaPmAgInS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
směs	směs	k1gFnSc1	směs
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
noc	noc	k1gFnSc4	noc
změnila	změnit	k5eAaPmAgFnS	změnit
v	v	k7c4	v
měkkou	měkký	k2eAgFnSc4d1	měkká
průhlednou	průhledný	k2eAgFnSc4d1	průhledná
hmotu	hmota	k1gFnSc4	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
základní	základní	k2eAgInSc1d1	základní
materiál	materiál	k1gInSc1	materiál
pro	pro	k7c4	pro
měkké	měkký	k2eAgFnPc4d1	měkká
kontaktní	kontaktní	k2eAgFnPc4d1	kontaktní
čočky	čočka	k1gFnPc4	čočka
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
již	již	k6eAd1	již
následoval	následovat	k5eAaImAgInS	následovat
patent	patent	k1gInSc1	patent
na	na	k7c4	na
hydrofilní	hydrofilní	k2eAgInSc4d1	hydrofilní
gel	gel	k1gInSc4	gel
zvaný	zvaný	k2eAgInSc4d1	zvaný
polyHEMA	polyHEMA	k?	polyHEMA
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nesl	nést	k5eAaImAgInS	nést
číslo	číslo	k1gNnSc4	číslo
91918	[number]	k4	91918
a	a	k8xC	a
datum	datum	k1gNnSc1	datum
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
následně	následně	k6eAd1	následně
Wichterle	Wichterle	k1gFnSc1	Wichterle
a	a	k8xC	a
Lím	Lím	k1gMnPc1	Lím
zavedli	zavést	k5eAaPmAgMnP	zavést
techniku	technika	k1gFnSc4	technika
výroby	výroba	k1gFnSc2	výroba
kontaktních	kontaktní	k2eAgFnPc2d1	kontaktní
čoček	čočka	k1gFnPc2	čočka
odstředivým	odstředivý	k2eAgNnSc7d1	odstředivé
litím	lití	k1gNnSc7	lití
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výrobní	výrobní	k2eAgInSc1d1	výrobní
postup	postup	k1gInSc1	postup
výrazně	výrazně	k6eAd1	výrazně
snížil	snížit	k5eAaPmAgInS	snížit
výrobní	výrobní	k2eAgInPc4d1	výrobní
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
kontaktní	kontaktní	k2eAgFnPc4d1	kontaktní
čočky	čočka	k1gFnPc4	čočka
a	a	k8xC	a
umožnil	umožnit	k5eAaPmAgInS	umožnit
jejich	jejich	k3xOp3gNnSc4	jejich
masové	masový	k2eAgNnSc1d1	masové
rozšíření	rozšíření	k1gNnSc1	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
měkké	měkký	k2eAgFnSc2d1	měkká
i	i	k8xC	i
tvrdé	tvrdý	k2eAgFnSc2d1	tvrdá
čočky	čočka	k1gFnSc2	čočka
rozvíjely	rozvíjet	k5eAaImAgInP	rozvíjet
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
objevy	objev	k1gInPc7	objev
nových	nový	k2eAgInPc2d1	nový
materiálů	materiál	k1gInPc2	materiál
<g/>
:	:	kIx,	:
tvrdé	tvrdý	k2eAgFnPc4d1	tvrdá
čočky	čočka	k1gFnPc4	čočka
s	s	k7c7	s
novými	nový	k2eAgInPc7d1	nový
plynopropustnými	plynopropustný	k2eAgInPc7d1	plynopropustný
materiály	materiál	k1gInPc7	materiál
<g/>
,	,	kIx,	,
měkké	měkký	k2eAgFnPc4d1	měkká
čočky	čočka	k1gFnPc4	čočka
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
vynalezenými	vynalezený	k2eAgInPc7d1	vynalezený
<g/>
,	,	kIx,	,
dokonalejšími	dokonalý	k2eAgInPc7d2	dokonalejší
polymery	polymer	k1gInPc7	polymer
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
<g/>
,	,	kIx,	,
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
kontaktní	kontaktní	k2eAgFnPc1d1	kontaktní
čočky	čočka	k1gFnPc1	čočka
dělí	dělit	k5eAaImIp3nP	dělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
velkých	velký	k2eAgFnPc2d1	velká
skupin	skupina	k1gFnPc2	skupina
–	–	k?	–
na	na	k7c6	na
tvrdé	tvrdý	k2eAgFnSc6d1	tvrdá
a	a	k8xC	a
měkké	měkký	k2eAgFnSc6d1	měkká
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
tyto	tento	k3xDgFnPc1	tento
skupiny	skupina	k1gFnPc1	skupina
mají	mít	k5eAaImIp3nP	mít
pak	pak	k6eAd1	pak
další	další	k2eAgFnPc1d1	další
specifické	specifický	k2eAgFnPc1d1	specifická
podskupiny	podskupina	k1gFnPc1	podskupina
<g/>
.	.	kIx.	.
</s>
<s>
PMMA	PMMA	kA	PMMA
(	(	kIx(	(
<g/>
tvrdé	tvrdý	k2eAgFnSc2d1	tvrdá
<g/>
,	,	kIx,	,
plynům	plyn	k1gInPc3	plyn
nepropustné	propustný	k2eNgInPc1d1	nepropustný
<g/>
)	)	kIx)	)
RGP	RGP	kA	RGP
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
rigid	rigid	k1gInSc1	rigid
gas	gas	k?	gas
permeable	permeable	k6eAd1	permeable
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
tvrdé	tvrdá	k1gFnPc1	tvrdá
plynopropustné	plynopropustný	k2eAgFnPc1d1	plynopropustný
<g/>
)	)	kIx)	)
Tvrdé	Tvrdé	k2eAgFnPc1d1	Tvrdé
kontaktní	kontaktní	k2eAgFnPc1d1	kontaktní
čočky	čočka	k1gFnPc1	čočka
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
určeny	určit	k5eAaPmNgInP	určit
pro	pro	k7c4	pro
dlouhodobější	dlouhodobý	k2eAgNnSc4d2	dlouhodobější
nošení	nošení	k1gNnSc4	nošení
<g/>
,	,	kIx,	,
např.	např.	kA	např.
roční	roční	k2eAgFnPc4d1	roční
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
výhodou	výhoda	k1gFnSc7	výhoda
tvrdých	tvrdý	k2eAgFnPc2d1	tvrdá
čoček	čočka	k1gFnPc2	čočka
je	být	k5eAaImIp3nS	být
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
stabilní	stabilní	k2eAgInSc4d1	stabilní
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
optické	optický	k2eAgFnPc1d1	optická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
se	se	k3xPyFc4	se
při	při	k7c6	při
aplikaci	aplikace	k1gFnSc6	aplikace
na	na	k7c4	na
oko	oko	k1gNnSc4	oko
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Tuhý	tuhý	k2eAgInSc1d1	tuhý
materiál	materiál	k1gInSc1	materiál
je	být	k5eAaImIp3nS	být
mechanicky	mechanicky	k6eAd1	mechanicky
odolnější	odolný	k2eAgMnSc1d2	odolnější
než	než	k8xS	než
měkký	měkký	k2eAgMnSc1d1	měkký
<g/>
,	,	kIx,	,
životnost	životnost	k1gFnSc1	životnost
RGP	RGP	kA	RGP
čoček	čočka	k1gFnPc2	čočka
je	být	k5eAaImIp3nS	být
až	až	k9	až
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Údržba	údržba	k1gFnSc1	údržba
je	být	k5eAaImIp3nS	být
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
alergie	alergie	k1gFnSc2	alergie
na	na	k7c4	na
měkké	měkký	k2eAgFnPc4d1	měkká
kontaktní	kontaktní	k2eAgFnPc4d1	kontaktní
čočky	čočka	k1gFnPc4	čočka
lze	lze	k6eAd1	lze
úspěšně	úspěšně	k6eAd1	úspěšně
nosit	nosit	k5eAaImF	nosit
RGP	RGP	kA	RGP
čočky	čočka	k1gFnPc4	čočka
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
také	také	k9	také
schopné	schopný	k2eAgFnPc1d1	schopná
korigovat	korigovat	k5eAaBmF	korigovat
přirozeně	přirozeně	k6eAd1	přirozeně
nepravidelný	pravidelný	k2eNgInSc1d1	nepravidelný
tvar	tvar	k1gInSc1	tvar
rohovky	rohovka	k1gFnSc2	rohovka
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
povrchem	povrch	k1gInSc7	povrch
čočky	čočka	k1gFnSc2	čočka
a	a	k8xC	a
rohovkou	rohovka	k1gFnSc7	rohovka
vzniká	vznikat	k5eAaImIp3nS	vznikat
slzná	slzný	k2eAgFnSc1d1	slzná
čočka	čočka	k1gFnSc1	čočka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
kompenzuje	kompenzovat	k5eAaBmIp3nS	kompenzovat
vady	vada	k1gFnPc4	vada
rohovky	rohovka	k1gFnSc2	rohovka
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
i	i	k9	i
obyčejná	obyčejný	k2eAgFnSc1d1	obyčejná
sférická	sférický	k2eAgFnSc1d1	sférická
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
čočka	čočka	k1gFnSc1	čočka
dobrou	dobrý	k2eAgFnSc4d1	dobrá
úroveň	úroveň	k1gFnSc4	úroveň
vidění	vidění	k1gNnSc2	vidění
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
astigmatismem	astigmatismus	k1gInSc7	astigmatismus
nebo	nebo	k8xC	nebo
narušeným	narušený	k2eAgInSc7d1	narušený
tvarem	tvar	k1gInSc7	tvar
rohovky	rohovka	k1gFnSc2	rohovka
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
keratoconus	keratoconus	k1gInSc1	keratoconus
(	(	kIx(	(
<g/>
vyklenutí	vyklenutí	k1gNnSc1	vyklenutí
rohovky	rohovka	k1gFnSc2	rohovka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdé	tvrdé	k1gNnSc1	tvrdé
čočky	čočka	k1gFnSc2	čočka
(	(	kIx(	(
<g/>
PMMA	PMMA	kA	PMMA
<g/>
)	)	kIx)	)
nepropouštějí	propouštět	k5eNaImIp3nP	propouštět
k	k	k7c3	k
rohovce	rohovka	k1gFnSc3	rohovka
žádný	žádný	k3yNgInSc1	žádný
kyslík	kyslík	k1gInSc1	kyslík
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
už	už	k6eAd1	už
dnes	dnes	k6eAd1	dnes
téměř	téměř	k6eAd1	téměř
nepoužívají	používat	k5eNaImIp3nP	používat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
první	první	k4xOgFnSc6	první
aplikaci	aplikace	k1gFnSc6	aplikace
jsou	být	k5eAaImIp3nP	být
tvrdé	tvrdý	k2eAgFnPc1d1	tvrdá
čočky	čočka	k1gFnPc1	čočka
často	často	k6eAd1	často
nepohodlné	pohodlný	k2eNgFnPc1d1	nepohodlná
<g/>
,	,	kIx,	,
uživatel	uživatel	k1gMnSc1	uživatel
si	se	k3xPyFc3	se
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
musí	muset	k5eAaImIp3nP	muset
postupně	postupně	k6eAd1	postupně
přivykat	přivykat	k5eAaImF	přivykat
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
jejich	jejich	k3xOp3gInSc4	jejich
tvar	tvar	k1gInSc4	tvar
upravovat	upravovat	k5eAaImF	upravovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zadní	zadní	k2eAgFnSc1d1	zadní
plocha	plocha	k1gFnSc1	plocha
čočky	čočka	k1gFnSc2	čočka
přesně	přesně	k6eAd1	přesně
kopírovala	kopírovat	k5eAaImAgFnS	kopírovat
rohovku	rohovka	k1gFnSc4	rohovka
a	a	k8xC	a
čočky	čočka	k1gFnPc4	čočka
dobře	dobře	k6eAd1	dobře
"	"	kIx"	"
<g/>
seděly	sedět	k5eAaImAgInP	sedět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
cena	cena	k1gFnSc1	cena
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
u	u	k7c2	u
měkkých	měkký	k2eAgFnPc2d1	měkká
čoček	čočka	k1gFnPc2	čočka
<g/>
.	.	kIx.	.
hydrogelové	hydrogelový	k2eAgFnPc1d1	hydrogelová
silikon-hydrogelové	silikonydrogelová	k1gFnPc1	silikon-hydrogelová
hybridní	hybridní	k2eAgFnPc4d1	hybridní
(	(	kIx(	(
<g/>
střed	střed	k1gInSc4	střed
jsou	být	k5eAaImIp3nP	být
RGP	RGP	kA	RGP
čočky	čočka	k1gFnPc1	čočka
<g/>
,	,	kIx,	,
okraj	okraj	k1gInSc1	okraj
je	být	k5eAaImIp3nS	být
měkký	měkký	k2eAgInSc1d1	měkký
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
málo	málo	k6eAd1	málo
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgInPc4	některý
druhy	druh	k1gInPc4	druh
silikon-hydrogelových	silikonydrogelův	k2eAgFnPc2d1	silikon-hydrogelův
kontaktních	kontaktní	k2eAgFnPc2d1	kontaktní
čoček	čočka	k1gFnPc2	čočka
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
propustností	propustnost	k1gFnSc7	propustnost
pro	pro	k7c4	pro
kyslík	kyslík	k1gInSc4	kyslík
schváleny	schválit	k5eAaPmNgInP	schválit
pro	pro	k7c4	pro
prodloužené	prodloužený	k2eAgNnSc4d1	prodloužené
nošení	nošení	k1gNnSc4	nošení
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
pro	pro	k7c4	pro
nošení	nošení	k1gNnSc4	nošení
i	i	k9	i
přes	přes	k7c4	přes
noc	noc	k1gFnSc4	noc
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
delší	dlouhý	k2eAgFnSc4d2	delší
než	než	k8xS	než
denní	denní	k2eAgFnSc4d1	denní
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
režim	režim	k1gInSc1	režim
nošení	nošení	k1gNnSc2	nošení
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
při	při	k7c6	při
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
nošení	nošení	k1gNnSc6	nošení
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgNnSc4d2	vyšší
riziko	riziko	k1gNnSc4	riziko
komplikací	komplikace	k1gFnPc2	komplikace
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
měkkých	měkký	k2eAgFnPc2d1	měkká
kontaktních	kontaktní	k2eAgFnPc2d1	kontaktní
čoček	čočka	k1gFnPc2	čočka
je	být	k5eAaImIp3nS	být
hlavně	hlavně	k9	hlavně
pohodlné	pohodlný	k2eAgNnSc1d1	pohodlné
nošení	nošení	k1gNnSc1	nošení
<g/>
,	,	kIx,	,
snadná	snadný	k2eAgFnSc1d1	snadná
aplikace	aplikace	k1gFnSc1	aplikace
a	a	k8xC	a
příznivá	příznivý	k2eAgFnSc1d1	příznivá
cena	cena	k1gFnSc1	cena
<g/>
.	.	kIx.	.
</s>
<s>
Silikon-hydrogelové	Silikonydrogelový	k2eAgFnPc1d1	Silikon-hydrogelový
kontaktní	kontaktní	k2eAgFnPc1d1	kontaktní
čočky	čočka	k1gFnPc1	čočka
mají	mít	k5eAaImIp3nP	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
propustnost	propustnost	k1gFnSc4	propustnost
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
dobré	dobrý	k2eAgNnSc4d1	dobré
dýchání	dýchání	k1gNnSc4	dýchání
rohovky	rohovka	k1gFnSc2	rohovka
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
mají	mít	k5eAaImIp3nP	mít
oproti	oproti	k7c3	oproti
hydrogelovým	hydrogelův	k2eAgFnPc3d1	hydrogelův
čočkám	čočka	k1gFnPc3	čočka
často	často	k6eAd1	často
horší	horšit	k5eAaImIp3nS	horšit
smáčenlivost	smáčenlivost	k1gFnSc4	smáčenlivost
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgFnSc4d2	vyšší
tuhost	tuhost	k1gFnSc4	tuhost
a	a	k8xC	a
tendenci	tendence	k1gFnSc4	tendence
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
lipidických	lipidický	k2eAgFnPc2d1	lipidická
usazenin	usazenina	k1gFnPc2	usazenina
<g/>
.	.	kIx.	.
</s>
<s>
Měkké	měkký	k2eAgFnPc1d1	měkká
kontaktní	kontaktní	k2eAgFnPc1d1	kontaktní
čočky	čočka	k1gFnPc1	čočka
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
"	"	kIx"	"
<g/>
náchylnějšího	náchylný	k2eAgInSc2d2	náchylnější
<g/>
"	"	kIx"	"
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nošením	nošení	k1gNnSc7	nošení
rychle	rychle	k6eAd1	rychle
zanáší	zanášet	k5eAaImIp3nS	zanášet
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
je	on	k3xPp3gFnPc4	on
pravidelně	pravidelně	k6eAd1	pravidelně
měnit	měnit	k5eAaImF	měnit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
tvoří	tvořit	k5eAaImIp3nP	tvořit
lipidové	lipid	k1gMnPc1	lipid
<g/>
,	,	kIx,	,
bílkovinné	bílkovinný	k2eAgFnPc1d1	bílkovinná
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
usazeniny	usazenina	k1gFnPc1	usazenina
ze	z	k7c2	z
slzného	slzný	k2eAgInSc2d1	slzný
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Nesprávným	správný	k2eNgInSc7d1	nesprávný
výběrem	výběr	k1gInSc7	výběr
čočky	čočka	k1gFnSc2	čočka
<g/>
,	,	kIx,	,
nedodržováním	nedodržování	k1gNnSc7	nedodržování
režimu	režim	k1gInSc2	režim
nošení	nošení	k1gNnSc2	nošení
(	(	kIx(	(
<g/>
přenášením	přenášení	k1gNnSc7	přenášení
<g/>
)	)	kIx)	)
a	a	k8xC	a
zanedbáváním	zanedbávání	k1gNnSc7	zanedbávání
péče	péče	k1gFnSc2	péče
mohou	moct	k5eAaImIp3nP	moct
nastat	nastat	k5eAaPmF	nastat
komplikace	komplikace	k1gFnPc4	komplikace
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
zánět	zánět	k1gInSc1	zánět
spojivek	spojivka	k1gFnPc2	spojivka
či	či	k8xC	či
neovaskularizace	neovaskularizace	k1gFnSc2	neovaskularizace
rohovky	rohovka	k1gFnSc2	rohovka
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
model	model	k1gInSc1	model
kontaktních	kontaktní	k2eAgFnPc2d1	kontaktní
čoček	čočka	k1gFnPc2	čočka
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
určitý	určitý	k2eAgInSc4d1	určitý
režim	režim	k1gInSc4	režim
nošení	nošení	k1gNnSc2	nošení
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
<g/>
,	,	kIx,	,
po	po	k7c4	po
jak	jak	k8xS	jak
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
čočka	čočka	k1gFnSc1	čočka
v	v	k7c6	v
kuse	kus	k1gInSc6	kus
nasazena	nasadit	k5eAaPmNgFnS	nasadit
na	na	k7c6	na
oku	oko	k1gNnSc6	oko
<g/>
.	.	kIx.	.
</s>
<s>
Čočka	čočka	k1gFnSc1	čočka
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
na	na	k7c6	na
oku	oko	k1gNnSc6	oko
nasazena	nasazen	k2eAgFnSc1d1	nasazena
max	max	kA	max
<g/>
.	.	kIx.	.
16	[number]	k4	16
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
(	(	kIx(	(
<g/>
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
modelů	model	k1gInPc2	model
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
doba	doba	k1gFnSc1	doba
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
nízké	nízký	k2eAgFnSc3d1	nízká
propustnosti	propustnost	k1gFnSc3	propustnost
pro	pro	k7c4	pro
kyslík	kyslík	k1gInSc4	kyslík
omezena	omezen	k2eAgFnSc1d1	omezena
i	i	k9	i
na	na	k7c4	na
10	[number]	k4	10
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kontaktní	kontaktní	k2eAgFnSc1d1	kontaktní
čočka	čočka	k1gFnSc1	čočka
s	s	k7c7	s
denním	denní	k2eAgInSc7d1	denní
režimem	režim	k1gInSc7	režim
nošení	nošení	k1gNnSc2	nošení
se	se	k3xPyFc4	se
ráno	ráno	k6eAd1	ráno
nasadí	nasadit	k5eAaPmIp3nS	nasadit
do	do	k7c2	do
oka	oko	k1gNnSc2	oko
a	a	k8xC	a
večer	večer	k6eAd1	večer
uloží	uložit	k5eAaPmIp3nS	uložit
do	do	k7c2	do
roztoku	roztok	k1gInSc2	roztok
pro	pro	k7c4	pro
opětovné	opětovný	k2eAgNnSc4d1	opětovné
nasazení	nasazení	k1gNnSc4	nasazení
druhý	druhý	k4xOgInSc1	druhý
den	den	k1gInSc1	den
<g/>
.	.	kIx.	.
</s>
<s>
Prodlouženého	prodloužený	k2eAgNnSc2d1	prodloužené
nošení	nošení	k1gNnSc2	nošení
je	být	k5eAaImIp3nS	být
povoleno	povolen	k2eAgNnSc1d1	povoleno
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
kontaktních	kontaktní	k2eAgFnPc2d1	kontaktní
čoček	čočka	k1gFnPc2	čočka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nP	nosit
v	v	k7c6	v
denním	denní	k2eAgInSc6d1	denní
režimu	režim	k1gInSc6	režim
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
propustnost	propustnost	k1gFnSc4	propustnost
pro	pro	k7c4	pro
kyslík	kyslík	k1gInSc4	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Čočky	čočka	k1gFnPc1	čočka
s	s	k7c7	s
prodlouženým	prodloužený	k2eAgInSc7d1	prodloužený
režimem	režim	k1gInSc7	režim
nošení	nošení	k1gNnSc2	nošení
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
nosit	nosit	k5eAaImF	nosit
i	i	k9	i
přes	přes	k7c4	přes
noc	noc	k1gFnSc4	noc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
maximálně	maximálně	k6eAd1	maximálně
6	[number]	k4	6
dní	den	k1gInPc2	den
v	v	k7c6	v
kuse	kus	k1gInSc6	kus
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
tímto	tento	k3xDgNnSc7	tento
nošením	nošení	k1gNnSc7	nošení
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
celkový	celkový	k2eAgInSc1d1	celkový
čas	čas	k1gInSc1	čas
použitelnosti	použitelnost	k1gFnSc2	použitelnost
čočky	čočka	k1gFnSc2	čočka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
měsíčních	měsíční	k2eAgFnPc2d1	měsíční
se	se	k3xPyFc4	se
o	o	k7c4	o
týden	týden	k1gInSc4	týden
zkrátí	zkrátit	k5eAaPmIp3nS	zkrátit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kontaktní	kontaktní	k2eAgFnPc1d1	kontaktní
čočky	čočka	k1gFnPc1	čočka
pro	pro	k7c4	pro
kontinuální	kontinuální	k2eAgNnPc4d1	kontinuální
nošení	nošení	k1gNnPc4	nošení
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nosit	nosit	k5eAaImF	nosit
přes	přes	k7c4	přes
den	den	k1gInSc4	den
i	i	k8xC	i
noc	noc	k1gFnSc4	noc
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
jejich	jejich	k3xOp3gFnSc2	jejich
použitelnosti	použitelnost	k1gFnSc2	použitelnost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
měsíční	měsíční	k2eAgInPc1d1	měsíční
30	[number]	k4	30
dní	den	k1gInPc2	den
a	a	k8xC	a
nocí	noc	k1gFnPc2	noc
nepřetržitě	přetržitě	k6eNd1	přetržitě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
nošení	nošení	k1gNnSc1	nošení
by	by	kYmCp3nS	by
určitě	určitě	k6eAd1	určitě
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
schváleno	schválit	k5eAaPmNgNnS	schválit
očním	oční	k2eAgMnSc7d1	oční
odborníkem	odborník	k1gMnSc7	odborník
<g/>
,	,	kIx,	,
nevyhovuje	vyhovovat	k5eNaImIp3nS	vyhovovat
každému	každý	k3xTgNnSc3	každý
oku	oko	k1gNnSc3	oko
<g/>
.	.	kIx.	.
</s>
<s>
Četnost	četnost	k1gFnSc1	četnost
výměny	výměna	k1gFnSc2	výměna
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
dlouho	dlouho	k6eAd1	dlouho
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
kontaktní	kontaktní	k2eAgFnSc1d1	kontaktní
čočka	čočka	k1gFnSc1	čočka
použitelná	použitelný	k2eAgFnSc1d1	použitelná
<g/>
,	,	kIx,	,
kolik	kolik	k4yQc4	kolik
dní	den	k1gInPc2	den
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ji	on	k3xPp3gFnSc4	on
nosit	nosit	k5eAaImF	nosit
<g/>
.	.	kIx.	.
</s>
<s>
Měkké	měkký	k2eAgFnPc1d1	měkká
kontaktní	kontaktní	k2eAgFnPc1d1	kontaktní
čočky	čočka	k1gFnPc1	čočka
mají	mít	k5eAaImIp3nP	mít
různé	různý	k2eAgFnPc1d1	různá
životnosti	životnost	k1gFnPc1	životnost
<g/>
:	:	kIx,	:
denní	denní	k2eAgFnPc1d1	denní
-	-	kIx~	-
ráno	ráno	k6eAd1	ráno
se	se	k3xPyFc4	se
nasadí	nasadit	k5eAaPmIp3nS	nasadit
a	a	k8xC	a
večer	večer	k6eAd1	večer
vyhodí	vyhodit	k5eAaPmIp3nP	vyhodit
<g/>
,	,	kIx,	,
čtrnáctidenní	čtrnáctidenní	k2eAgFnSc4d1	čtrnáctidenní
a	a	k8xC	a
měsíční	měsíční	k2eAgFnSc4d1	měsíční
-	-	kIx~	-
každý	každý	k3xTgInSc4	každý
večer	večer	k1gInSc4	večer
se	se	k3xPyFc4	se
z	z	k7c2	z
oka	oko	k1gNnSc2	oko
vyjímají	vyjímat	k5eAaImIp3nP	vyjímat
a	a	k8xC	a
vkládají	vkládat	k5eAaImIp3nP	vkládat
do	do	k7c2	do
roztoku	roztok	k1gInSc2	roztok
na	na	k7c4	na
kontaktní	kontaktní	k2eAgFnPc4d1	kontaktní
čočky	čočka	k1gFnPc4	čočka
pro	pro	k7c4	pro
desinfekci	desinfekce	k1gFnSc4	desinfekce
a	a	k8xC	a
opětovné	opětovný	k2eAgNnSc4d1	opětovné
získání	získání	k1gNnSc4	získání
vlhkosti	vlhkost	k1gFnSc2	vlhkost
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
nejsou	být	k5eNaImIp3nP	být
pro	pro	k7c4	pro
kontinuální	kontinuální	k2eAgNnPc4d1	kontinuální
nebo	nebo	k8xC	nebo
prodloužené	prodloužený	k2eAgNnSc4d1	prodloužené
nošení	nošení	k1gNnSc4	nošení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konvenční	konvenční	k2eAgFnPc1d1	konvenční
čočky	čočka	k1gFnPc1	čočka
čtvrtletní	čtvrtletní	k2eAgFnPc1d1	čtvrtletní
nebo	nebo	k8xC	nebo
i	i	k8xC	i
roční	roční	k2eAgMnSc1d1	roční
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
požívají	požívat	k5eAaImIp3nP	požívat
méně	málo	k6eAd2	málo
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
náročnější	náročný	k2eAgFnSc4d2	náročnější
údržbu	údržba	k1gFnSc4	údržba
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdé	tvrdé	k1gNnSc1	tvrdé
kontaktní	kontaktní	k2eAgFnSc2d1	kontaktní
čočky	čočka	k1gFnSc2	čočka
propustné	propustné	k1gNnSc1	propustné
pro	pro	k7c4	pro
plyny	plyn	k1gInPc4	plyn
jsou	být	k5eAaImIp3nP	být
velice	velice	k6eAd1	velice
trvanlivé	trvanlivý	k2eAgFnPc1d1	trvanlivá
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
vydržet	vydržet	k5eAaPmF	vydržet
i	i	k9	i
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
bez	bez	k7c2	bez
výměny	výměna	k1gFnSc2	výměna
<g/>
.	.	kIx.	.
</s>
<s>
Péčí	péče	k1gFnSc7	péče
o	o	k7c4	o
čočky	čočka	k1gFnPc4	čočka
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jejich	jejich	k3xOp3gNnPc2	jejich
čištění	čištění	k1gNnPc2	čištění
<g/>
,	,	kIx,	,
desinfekce	desinfekce	k1gFnSc1	desinfekce
případně	případně	k6eAd1	případně
lubrikace	lubrikace	k1gFnSc2	lubrikace
a	a	k8xC	a
udržování	udržování	k1gNnSc4	udržování
smáčenlivosti	smáčenlivost	k1gFnSc2	smáčenlivost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pomocí	pomocí	k7c2	pomocí
speciálních	speciální	k2eAgInPc2d1	speciální
roztoků	roztok	k1gInPc2	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Péče	péče	k1gFnSc1	péče
o	o	k7c4	o
čočky	čočka	k1gFnPc4	čočka
je	být	k5eAaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
při	při	k7c6	při
nošení	nošení	k1gNnSc6	nošení
všech	všecek	k3xTgInPc2	všecek
typů	typ	k1gInPc2	typ
čoček	čočka	k1gFnPc2	čočka
kromě	kromě	k7c2	kromě
čoček	čočka	k1gFnPc2	čočka
jednodenních	jednodenní	k2eAgFnPc2d1	jednodenní
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
nošení	nošení	k1gNnSc2	nošení
se	se	k3xPyFc4	se
na	na	k7c6	na
kontaktních	kontaktní	k2eAgFnPc6d1	kontaktní
čočkách	čočka	k1gFnPc6	čočka
postupně	postupně	k6eAd1	postupně
usazují	usazovat	k5eAaImIp3nP	usazovat
vápenaté	vápenatý	k2eAgInPc4d1	vápenatý
<g/>
,	,	kIx,	,
bílkovinné	bílkovinný	k2eAgInPc4d1	bílkovinný
<g/>
,	,	kIx,	,
lipidické	lipidický	k2eAgInPc4d1	lipidický
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
sedimenty	sediment	k1gInPc4	sediment
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
mohou	moct	k5eAaImIp3nP	moct
růst	růst	k5eAaImF	růst
mikroorganismy	mikroorganismus	k1gInPc1	mikroorganismus
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgFnPc4d1	nutná
čočky	čočka	k1gFnPc4	čočka
řádně	řádně	k6eAd1	řádně
čistit	čistit	k5eAaImF	čistit
vhodnými	vhodný	k2eAgInPc7d1	vhodný
roztoky	roztok	k1gInPc7	roztok
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
odstraní	odstranit	k5eAaPmIp3nS	odstranit
sedimenty	sediment	k1gInPc4	sediment
a	a	k8xC	a
vydesinfikují	vydesinfikovat	k5eAaPmIp3nP	vydesinfikovat
je	on	k3xPp3gMnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Čočky	čočka	k1gFnPc1	čočka
se	se	k3xPyFc4	se
čistí	čistit	k5eAaImIp3nP	čistit
speciálními	speciální	k2eAgInPc7d1	speciální
roztoky	roztok	k1gInPc7	roztok
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
složení	složení	k1gNnSc1	složení
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
čištění	čištění	k1gNnSc4	čištění
<g/>
,	,	kIx,	,
desinfekci	desinfekce	k1gFnSc4	desinfekce
<g/>
,	,	kIx,	,
hydrataci	hydratace	k1gFnSc4	hydratace
a	a	k8xC	a
případně	případně	k6eAd1	případně
i	i	k9	i
lubrikaci	lubrikace	k1gFnSc4	lubrikace
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
značky	značka	k1gFnPc1	značka
roztoků	roztok	k1gInPc2	roztok
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
svým	svůj	k3xOyFgNnSc7	svůj
složením	složení	k1gNnSc7	složení
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc1	všechen
však	však	k9	však
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
sterilní	sterilní	k2eAgFnPc4d1	sterilní
<g/>
,	,	kIx,	,
mít	mít	k5eAaImF	mít
vhodné	vhodný	k2eAgFnPc4d1	vhodná
pH	ph	kA	ph
a	a	k8xC	a
tonicitu	tonicita	k1gFnSc4	tonicita
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
zvlhčování	zvlhčování	k1gNnSc2	zvlhčování
roztokem	roztok	k1gInSc7	roztok
by	by	kYmCp3nP	by
kontaktní	kontaktní	k2eAgFnPc1d1	kontaktní
čočky	čočka	k1gFnPc1	čočka
vyschly	vyschnout	k5eAaPmAgFnP	vyschnout
<g/>
.	.	kIx.	.
</s>
<s>
Peroxidový	Peroxidový	k2eAgInSc1d1	Peroxidový
roztok	roztok	k1gInSc1	roztok
je	být	k5eAaImIp3nS	být
stabilizovaný	stabilizovaný	k2eAgInSc1d1	stabilizovaný
3	[number]	k4	3
<g/>
%	%	kIx~	%
roztok	roztok	k1gInSc1	roztok
peroxidu	peroxid	k1gInSc2	peroxid
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Účinek	účinek	k1gInSc1	účinek
peroxidického	peroxidický	k2eAgInSc2d1	peroxidický
roztoku	roztok	k1gInSc2	roztok
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
kontaktu	kontakt	k1gInSc6	kontakt
peroxidu	peroxid	k1gInSc2	peroxid
s	s	k7c7	s
katalyzátorem	katalyzátor	k1gInSc7	katalyzátor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
speciálního	speciální	k2eAgNnSc2d1	speciální
pouzdra	pouzdro	k1gNnSc2	pouzdro
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
přidá	přidat	k5eAaPmIp3nS	přidat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
tabletky	tabletka	k1gFnSc2	tabletka
či	či	k8xC	či
jiného	jiný	k2eAgInSc2d1	jiný
roztoku	roztok	k1gInSc2	roztok
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
peroxid	peroxid	k1gInSc1	peroxid
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
"	"	kIx"	"
<g/>
bublá	bublat	k5eAaImIp3nS	bublat
<g/>
"	"	kIx"	"
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
atomární	atomární	k2eAgInSc4d1	atomární
kyslík	kyslík	k1gInSc4	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
má	mít	k5eAaImIp3nS	mít
silné	silný	k2eAgInPc4d1	silný
oxidační	oxidační	k2eAgInPc4d1	oxidační
účinky	účinek	k1gInPc4	účinek
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
čočky	čočka	k1gFnSc2	čočka
desinfikuje	desinfikovat	k5eAaBmIp3nS	desinfikovat
i	i	k9	i
čistí	čistý	k2eAgMnPc1d1	čistý
<g/>
.	.	kIx.	.
</s>
<s>
Čočky	čočka	k1gFnPc4	čočka
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
nechat	nechat	k5eAaPmF	nechat
v	v	k7c6	v
pouzdře	pouzdro	k1gNnSc6	pouzdro
s	s	k7c7	s
roztokem	roztok	k1gInSc7	roztok
minimálně	minimálně	k6eAd1	minimálně
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
neskončí	skončit	k5eNaPmIp3nS	skončit
reakce	reakce	k1gFnSc1	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
hrozí	hrozit	k5eAaImIp3nS	hrozit
poleptání	poleptání	k1gNnSc1	poleptání
oka	oko	k1gNnSc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vyjmutím	vyjmutí	k1gNnSc7	vyjmutí
čoček	čočka	k1gFnPc2	čočka
z	z	k7c2	z
očí	oko	k1gNnPc2	oko
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
si	se	k3xPyFc3	se
omýt	omýt	k5eAaPmF	omýt
ruce	ruka	k1gFnPc4	ruka
a	a	k8xC	a
vylít	vylít	k5eAaPmF	vylít
použitý	použitý	k2eAgInSc4d1	použitý
roztok	roztok	k1gInSc4	roztok
z	z	k7c2	z
pouzdra	pouzdro	k1gNnSc2	pouzdro
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc4	dva
misky	miska	k1gFnPc4	miska
pouzdra	pouzdro	k1gNnSc2	pouzdro
na	na	k7c4	na
kontaktní	kontaktní	k2eAgFnPc4d1	kontaktní
čočky	čočka	k1gFnPc4	čočka
by	by	kYmCp3nS	by
pak	pak	k6eAd1	pak
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
vymyty	vymýt	k5eAaPmNgInP	vymýt
roztokem	roztok	k1gInSc7	roztok
a	a	k8xC	a
naplněny	naplněn	k2eAgMnPc4d1	naplněn
novým	nový	k2eAgInSc7d1	nový
roztokem	roztok	k1gInSc7	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyjmutí	vyjmutí	k1gNnSc6	vyjmutí
čoček	čočka	k1gFnPc2	čočka
z	z	k7c2	z
očí	oko	k1gNnPc2	oko
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
položit	položit	k5eAaPmF	položit
je	on	k3xPp3gNnSc4	on
na	na	k7c4	na
dlaň	dlaň	k1gFnSc4	dlaň
<g/>
,	,	kIx,	,
kápnout	kápnout	k5eAaPmF	kápnout
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
pár	pár	k4xCyI	pár
kapek	kapka	k1gFnPc2	kapka
roztoku	roztok	k1gInSc2	roztok
a	a	k8xC	a
promnout	promnout	k5eAaPmF	promnout
je	být	k5eAaImIp3nS	být
prstem	prst	k1gInSc7	prst
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
čočky	čočka	k1gFnPc1	čočka
kvůli	kvůli	k7c3	kvůli
nevhodnému	vhodný	k2eNgNnSc3d1	nevhodné
uložení	uložení	k1gNnSc3	uložení
oschly	oschnout	k5eAaPmAgFnP	oschnout
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
je	on	k3xPp3gFnPc4	on
opatrně	opatrně	k6eAd1	opatrně
znovu	znovu	k6eAd1	znovu
hydratovat	hydratovat	k5eAaBmF	hydratovat
<g/>
.	.	kIx.	.
</s>
<s>
Pouzdro	pouzdro	k1gNnSc1	pouzdro
na	na	k7c4	na
kontaktní	kontaktní	k2eAgFnPc4d1	kontaktní
čočky	čočka	k1gFnPc4	čočka
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
potenciálním	potenciální	k2eAgInSc7d1	potenciální
zdrojem	zdroj	k1gInSc7	zdroj
bakterií	bakterie	k1gFnPc2	bakterie
a	a	k8xC	a
plísní	plíseň	k1gFnPc2	plíseň
<g/>
.	.	kIx.	.
</s>
<s>
Nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
se	se	k3xPyFc4	se
pouzdro	pouzdro	k1gNnSc4	pouzdro
čistit	čistit	k5eAaImF	čistit
jiným	jiný	k2eAgInSc7d1	jiný
prostředkem	prostředek	k1gInSc7	prostředek
než	než	k8xS	než
roztokem	roztok	k1gInSc7	roztok
na	na	k7c4	na
čočky	čočka	k1gFnPc4	čočka
<g/>
.	.	kIx.	.
</s>
<s>
Pouzdro	pouzdro	k1gNnSc1	pouzdro
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
měnit	měnit	k5eAaImF	měnit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
čočkami	čočka	k1gFnPc7	čočka
<g/>
.	.	kIx.	.
</s>
<s>
Nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
voda	voda	k1gFnSc1	voda
z	z	k7c2	z
vodovodu	vodovod	k1gInSc2	vodovod
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
bakterie	bakterie	k1gFnSc1	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Komplikacemi	komplikace	k1gFnPc7	komplikace
způsobenými	způsobený	k2eAgFnPc7d1	způsobená
nošením	nošení	k1gNnSc7	nošení
kontaktních	kontaktní	k2eAgFnPc2d1	kontaktní
čoček	čočka	k1gFnPc2	čočka
je	být	k5eAaImIp3nS	být
postiženo	postihnout	k5eAaPmNgNnS	postihnout
asi	asi	k9	asi
4	[number]	k4	4
%	%	kIx~	%
jejich	jejich	k3xOp3gMnPc2	jejich
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
však	však	k9	však
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nedodržení	nedodržení	k1gNnSc4	nedodržení
základních	základní	k2eAgNnPc2d1	základní
pravidel	pravidlo	k1gNnPc2	pravidlo
hygieny	hygiena	k1gFnSc2	hygiena
nebo	nebo	k8xC	nebo
přenášení	přenášení	k1gNnSc2	přenášení
kontaktních	kontaktní	k2eAgFnPc2d1	kontaktní
čoček	čočka	k1gFnPc2	čočka
uživatelem	uživatel	k1gMnSc7	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
oční	oční	k2eAgFnPc1d1	oční
vady	vada	k1gFnPc1	vada
mají	mít	k5eAaImIp3nP	mít
specifické	specifický	k2eAgFnPc1d1	specifická
souvislosti	souvislost	k1gFnPc1	souvislost
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
kontaktních	kontaktní	k2eAgFnPc2d1	kontaktní
čoček	čočka	k1gFnPc2	čočka
<g/>
:	:	kIx,	:
krátkozrakost	krátkozrakost	k1gFnSc1	krátkozrakost
dalekozrakost	dalekozrakost	k1gFnSc1	dalekozrakost
astigmatismus	astigmatismus	k1gInSc1	astigmatismus
-	-	kIx~	-
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
úhlu	úhel	k1gInSc6	úhel
pootočení	pootočení	k1gNnSc2	pootočení
měkké	měkký	k2eAgFnSc2d1	měkká
čočky	čočka	k1gFnSc2	čočka
na	na	k7c6	na
rohovce	rohovka	k1gFnSc6	rohovka
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
jen	jen	k9	jen
několik	několik	k4yIc1	několik
málo	málo	k6eAd1	málo
správných	správný	k2eAgFnPc2d1	správná
poloh	poloha	k1gFnPc2	poloha
(	(	kIx(	(
<g/>
dvě	dva	k4xCgFnPc1	dva
<g/>
,	,	kIx,	,
jediná	jediný	k2eAgFnSc1d1	jediná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
presbyopie	presbyopie	k1gFnSc1	presbyopie
keratoconus	keratoconus	k1gInSc1	keratoconus
(	(	kIx(	(
<g/>
vyklenutí	vyklenutí	k1gNnSc1	vyklenutí
rohovky	rohovka	k1gFnSc2	rohovka
<g/>
)	)	kIx)	)
Jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
společností	společnost	k1gFnPc2	společnost
kolem	kolem	k7c2	kolem
kontaktních	kontaktní	k2eAgFnPc2d1	kontaktní
čoček	čočka	k1gFnPc2	čočka
je	být	k5eAaImIp3nS	být
Česká	český	k2eAgFnSc1d1	Česká
kontaktologická	kontaktologický	k2eAgFnSc1d1	kontaktologická
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
dobrovolné	dobrovolný	k2eAgNnSc1d1	dobrovolné
sdružení	sdružení	k1gNnSc1	sdružení
očních	oční	k2eAgMnPc2d1	oční
lékařů	lékař	k1gMnPc2	lékař
<g/>
,	,	kIx,	,
optometristů	optometrista	k1gMnPc2	optometrista
<g/>
,	,	kIx,	,
odborníků	odborník	k1gMnPc2	odborník
zabývajících	zabývající	k2eAgMnPc2d1	zabývající
se	se	k3xPyFc4	se
vývojem	vývoj	k1gInSc7	vývoj
<g/>
,	,	kIx,	,
výrobou	výroba	k1gFnSc7	výroba
<g/>
,	,	kIx,	,
distribucí	distribuce	k1gFnSc7	distribuce
a	a	k8xC	a
aplikací	aplikace	k1gFnSc7	aplikace
kontaktních	kontaktní	k2eAgFnPc2d1	kontaktní
čoček	čočka	k1gFnPc2	čočka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
světě	svět	k1gInSc6	svět
kontaktních	kontaktní	k2eAgFnPc2d1	kontaktní
čoček	čočka	k1gFnPc2	čočka
hraje	hrát	k5eAaImIp3nS	hrát
také	také	k9	také
Společenstvo	společenstvo	k1gNnSc1	společenstvo
českých	český	k2eAgMnPc2d1	český
optiků	optik	k1gMnPc2	optik
a	a	k8xC	a
optometristů	optometrista	k1gMnPc2	optometrista
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
nejen	nejen	k6eAd1	nejen
profesní	profesní	k2eAgFnSc6d1	profesní
stránce	stránka	k1gFnSc6	stránka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
legislativě	legislativa	k1gFnSc3	legislativa
<g/>
,	,	kIx,	,
vzdělávání	vzdělávání	k1gNnSc3	vzdělávání
a	a	k8xC	a
různým	různý	k2eAgInPc3d1	různý
kongresům	kongres	k1gInPc3	kongres
či	či	k8xC	či
veletrhům	veletrh	k1gInPc3	veletrh
české	český	k2eAgFnSc2d1	Česká
optiky	optika	k1gFnSc2	optika
a	a	k8xC	a
optometrie	optometrie	k1gFnSc2	optometrie
<g/>
.	.	kIx.	.
</s>
