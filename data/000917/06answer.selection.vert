<s>
San	San	k?	San
Marino	Marina	k1gFnSc5	Marina
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
<g/>
:	:	kIx,	:
Republika	republika	k1gFnSc1	republika
San	San	k1gFnSc2	San
Marino	Marina	k1gFnSc5	Marina
<g/>
,	,	kIx,	,
italsky	italsky	k6eAd1	italsky
zní	znět	k5eAaImIp3nS	znět
plný	plný	k2eAgInSc1d1	plný
název	název	k1gInSc1	název
Serenissima	serenissimus	k1gMnSc2	serenissimus
Repubblica	Repubblicus	k1gMnSc2	Repubblicus
di	di	k?	di
San	San	k1gMnSc2	San
Marino	Marina	k1gFnSc5	Marina
-	-	kIx~	-
Nejvznešenější	vznešený	k2eAgFnSc1d3	nejvznešenější
republika	republika	k1gFnSc1	republika
San	San	k1gFnSc2	San
Marino	Marina	k1gFnSc5	Marina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc1d1	malý
jihoevropský	jihoevropský	k2eAgInSc1d1	jihoevropský
stát	stát	k1gInSc1	stát
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnSc1	obyvatel
2	[number]	k4	2
<g/>
.	.	kIx.	.
nejmenší	malý	k2eAgInSc4d3	nejmenší
evropský	evropský	k2eAgInSc4d1	evropský
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
nejmenší	malý	k2eAgMnSc1d3	nejmenší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
obklopený	obklopený	k2eAgInSc4d1	obklopený
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
.	.	kIx.	.
</s>
