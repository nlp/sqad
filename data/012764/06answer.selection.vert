<s>
Dave	Dav	k1gInSc5	Dav
Mustaine	Mustain	k1gInSc5	Mustain
-	-	kIx~	-
hlavní	hlavní	k2eAgInSc1d1	hlavní
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
Dave	Dav	k1gInSc5	Dav
Ellefson	Ellefson	k1gNnSc1	Ellefson
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
Kiko	Kiko	k6eAd1	Kiko
Loureiro	Loureiro	k1gNnSc1	Loureiro
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
-současnost	oučasnost	k1gFnSc1	-současnost
<g/>
)	)	kIx)	)
Dirk	Dirk	k1gInSc1	Dirk
Verbeuren	Verbeurna	k1gFnPc2	Verbeurna
-	-	kIx~	-
bicí	bicí	k2eAgInPc1d1	bicí
<g/>
,	,	kIx,	,
perkuse	perkuse	k1gFnPc1	perkuse
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
-současnost	oučasnost	k1gFnSc4	-současnost
<g/>
)	)	kIx)	)
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diskografie	diskografie	k1gFnSc2	diskografie
Megadeth	Megadetha	k1gFnPc2	Megadetha
<g/>
.	.	kIx.	.
</s>
