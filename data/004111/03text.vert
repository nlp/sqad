<s>
Karel	Karel	k1gMnSc1	Karel
Daniel	Daniel	k1gMnSc1	Daniel
Gangloff	Gangloff	k1gMnSc1	Gangloff
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1809	[number]	k4	1809
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1879	[number]	k4	1879
v	v	k7c6	v
Rožmitále	Rožmitála	k1gFnSc6	Rožmitála
pod	pod	k7c7	pod
Třemšínem	Třemšín	k1gInSc7	Třemšín
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
lesník	lesník	k1gMnSc1	lesník
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
<g/>
.	.	kIx.	.
</s>
<s>
Vynalézal	vynalézat	k5eAaImAgInS	vynalézat
především	především	k9	především
přístroje	přístroj	k1gInPc4	přístroj
pro	pro	k7c4	pro
zeměměřičství	zeměměřičství	k1gNnSc4	zeměměřičství
a	a	k8xC	a
stroje	stroj	k1gInPc4	stroj
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
dřevní	dřevní	k2eAgFnSc2d1	dřevní
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
velký	velký	k2eAgInSc4d1	velký
rozsah	rozsah	k1gInSc4	rozsah
jeho	jeho	k3xOp3gInPc2	jeho
vynálezů	vynález	k1gInPc2	vynález
byl	být	k5eAaImAgInS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
také	také	k9	také
českým	český	k2eAgMnSc7d1	český
Archimédem	Archimédes	k1gMnSc7	Archimédes
<g/>
.	.	kIx.	.
</s>
<s>
K.	K.	kA	K.
G.	G.	kA	G.
Gangloff	Gangloff	k1gInSc1	Gangloff
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1809	[number]	k4	1809
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
jako	jako	k9	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
dvojčat	dvojče	k1gNnPc2	dvojče
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
arcibiskupského	arcibiskupský	k2eAgMnSc2d1	arcibiskupský
hofmistra	hofmistr	k1gMnSc2	hofmistr
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
polytechnice	polytechnika	k1gFnSc6	polytechnika
<g/>
,	,	kIx,	,
po	po	k7c6	po
studiích	studio	k1gNnPc6	studio
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
lesník	lesník	k1gMnSc1	lesník
v	v	k7c6	v
Rokytnici	Rokytnice	k1gFnSc6	Rokytnice
<g/>
,	,	kIx,	,
lesní	lesní	k2eAgMnSc1d1	lesní
měřič	měřič	k1gMnSc1	měřič
a	a	k8xC	a
kreslič	kreslič	k1gMnSc1	kreslič
v	v	k7c6	v
Rožmitále	Rožmitála	k1gFnSc6	Rožmitála
pod	pod	k7c7	pod
Třemšínem	Třemšín	k1gInSc7	Třemšín
<g/>
,	,	kIx,	,
polesný	polesný	k1gMnSc1	polesný
Pelhřimovska	Pelhřimovsko	k1gNnSc2	Pelhřimovsko
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gMnSc1	vedoucí
taxačního	taxační	k2eAgNnSc2d1	taxační
oddělení	oddělení	k1gNnSc2	oddělení
lesů	les	k1gInPc2	les
pražského	pražský	k2eAgNnSc2d1	Pražské
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
jako	jako	k8xC	jako
lesmistr	lesmistr	k1gMnSc1	lesmistr
v	v	k7c6	v
milovaném	milovaný	k2eAgInSc6d1	milovaný
Rožmitálsku	Rožmitálsek	k1gInSc6	Rožmitálsek
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
Rožmitále	Rožmitál	k1gInSc6	Rožmitál
<g/>
.	.	kIx.	.
planimetr	planimetr	k1gInSc4	planimetr
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
principu	princip	k1gInSc6	princip
přeměny	přeměna	k1gFnSc2	přeměna
mnohoúhelníkových	mnohoúhelníkový	k2eAgInPc2d1	mnohoúhelníkový
obrazů	obraz	k1gInPc2	obraz
až	až	k9	až
na	na	k7c4	na
trojúhelníky	trojúhelník	k1gInPc4	trojúhelník
o	o	k7c6	o
stejné	stejný	k2eAgFnSc6d1	stejná
ploše	plocha	k1gFnSc6	plocha
při	při	k7c6	při
konstantní	konstantní	k2eAgFnSc6d1	konstantní
výšce	výška	k1gFnSc6	výška
a	a	k8xC	a
měřené	měřený	k2eAgFnSc6d1	měřená
základně	základna	k1gFnSc6	základna
přístroj	přístroj	k1gInSc1	přístroj
na	na	k7c4	na
redukci	redukce	k1gFnSc4	redukce
délek	délka	k1gFnPc2	délka
měřených	měřený	k2eAgFnPc2d1	měřená
po	po	k7c6	po
svahu	svah	k1gInSc6	svah
dendrometr	dendrometr	k1gInSc1	dendrometr
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
průměru	průměr	k1gInSc2	průměr
kmenů	kmen	k1gInPc2	kmen
stromů	strom	k1gInPc2	strom
s	s	k7c7	s
výškoměrem	výškoměr	k1gInSc7	výškoměr
arkograf	arkograf	k1gInSc1	arkograf
–	–	k?	–
hranolový	hranolový	k2eAgInSc1d1	hranolový
bubínek	bubínek	k1gInSc1	bubínek
<g />
.	.	kIx.	.
</s>
<s>
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
natáčení	natáčení	k1gNnSc2	natáčení
zrcátek	zrcátko	k1gNnPc2	zrcátko
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
k	k	k7c3	k
vytyčování	vytyčování	k1gNnSc3	vytyčování
kružnicových	kružnicový	k2eAgInPc2d1	kružnicový
oblouků	oblouk	k1gInPc2	oblouk
(	(	kIx(	(
<g/>
uložen	uložen	k2eAgInSc1d1	uložen
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
)	)	kIx)	)
šindelka	šindelka	k1gFnSc1	šindelka
–	–	k?	–
stroj	stroj	k1gInSc1	stroj
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
šindelů	šindel	k1gInPc2	šindel
(	(	kIx(	(
<g/>
patent	patent	k1gInSc1	patent
roku	rok	k1gInSc2	rok
1856	[number]	k4	1856
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stroj	stroj	k1gInSc1	stroj
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
velký	velký	k2eAgInSc1d1	velký
úspěch	úspěch	k1gInSc1	úspěch
na	na	k7c6	na
světových	světový	k2eAgFnPc6d1	světová
výstavách	výstava	k1gFnPc6	výstava
i	i	k8xC	i
v	v	k7c6	v
zámoří	zámoří	k1gNnSc6	zámoří
stroj	stroj	k1gInSc1	stroj
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
zápalkových	zápalkový	k2eAgNnPc2d1	zápalkový
<g />
.	.	kIx.	.
</s>
<s>
dřívek	dřívko	k1gNnPc2	dřívko
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
přes	přes	k7c4	přes
jeden	jeden	k4xCgInSc4	jeden
milion	milion	k4xCgInSc4	milion
kusů	kus	k1gInPc2	kus
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
kruhový	kruhový	k2eAgInSc4d1	kruhový
stroj	stroj	k1gInSc4	stroj
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
tužkových	tužkový	k2eAgNnPc2d1	tužkové
dřívek	dřívko	k1gNnPc2	dřívko
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
)	)	kIx)	)
stroj	stroj	k1gInSc1	stroj
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
floků	flok	k1gInPc2	flok
do	do	k7c2	do
bot	bota	k1gFnPc2	bota
kapesní	kapesní	k2eAgInSc4d1	kapesní
aneroid	aneroid	k1gInSc4	aneroid
model	model	k1gInSc1	model
větrného	větrný	k2eAgInSc2d1	větrný
mlýna	mlýn	k1gInSc2	mlýn
větrný	větrný	k2eAgInSc1d1	větrný
regulační	regulační	k2eAgInSc1d1	regulační
motor	motor	k1gInSc1	motor
vlastní	vlastní	k2eAgInSc4d1	vlastní
způsob	způsob	k1gInSc4	způsob
hroudové	hroudový	k2eAgFnSc2d1	hroudový
sadby	sadba	k1gFnSc2	sadba
klučka	klučka	k6eAd1	klučka
k	k	k7c3	k
dobývání	dobývání	k1gNnSc3	dobývání
pařezů	pařez	k1gInPc2	pařez
kulobrokovnice	kulobrokovnice	k1gFnSc2	kulobrokovnice
jelení	jelení	k2eAgFnSc1d1	jelení
vábnice	vábnice	k1gFnSc1	vábnice
<g />
.	.	kIx.	.
</s>
<s>
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
z	z	k7c2	z
jeleního	jelení	k2eAgInSc2d1	jelení
hrtanu	hrtan	k1gInSc2	hrtan
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Karel	Karel	k1gMnSc1	Karel
Daniel	Daniel	k1gMnSc1	Daniel
Gangloff	Gangloff	k1gMnSc1	Gangloff
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Přírodní	přírodní	k2eAgInSc4d1	přírodní
park	park	k1gInSc4	park
Třemšín	Třemšín	k1gMnSc1	Třemšín
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Daniel	Daniel	k1gMnSc1	Daniel
Gangloff	Gangloff	k1gMnSc1	Gangloff
Prostor	prostor	k1gInSc1	prostor
-	-	kIx~	-
architektura	architektura	k1gFnSc1	architektura
<g/>
,	,	kIx,	,
interiér	interiér	k1gInSc1	interiér
<g/>
,	,	kIx,	,
design	design	k1gInSc1	design
<g/>
,	,	kIx,	,
ing.	ing.	kA	ing.
Karel	Karel	k1gMnSc1	Karel
Daniel	Daniel	k1gMnSc1	Daniel
Gangloff	Gangloff	k1gMnSc1	Gangloff
(	(	kIx(	(
<g/>
1809	[number]	k4	1809
<g/>
-	-	kIx~	-
<g/>
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
http://www.municipal.cz/osobnosti/gangloff.htm	[url]	k6eAd1	http://www.municipal.cz/osobnosti/gangloff.htm
</s>
