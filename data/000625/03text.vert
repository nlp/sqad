<s>
Synagoga	synagoga	k1gFnSc1	synagoga
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
σ	σ	k?	σ
<g/>
,	,	kIx,	,
synagógé	synagógé	k6eAd1	synagógé
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
hebrejsky	hebrejsky	k6eAd1	hebrejsky
ב	ב	k?	ב
כ	כ	k?	כ
<g/>
,	,	kIx,	,
bejt	bejt	k?	bejt
kneset	kneset	k1gInSc1	kneset
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
dům	dům	k1gInSc1	dům
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
ב	ב	k?	ב
ת	ת	k?	ת
<g/>
,	,	kIx,	,
bejt	bejt	k?	bejt
tfila	tfila	k1gFnSc1	tfila
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
dům	dům	k1gInSc1	dům
modlitby	modlitba	k1gFnSc2	modlitba
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
židovská	židovský	k2eAgFnSc1d1	židovská
modlitebna	modlitebna	k1gFnSc1	modlitebna
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
kromě	kromě	k7c2	kromě
místa	místo	k1gNnSc2	místo
pro	pro	k7c4	pro
bohoslužebná	bohoslužebný	k2eAgNnPc4d1	bohoslužebné
setkání	setkání	k1gNnPc4	setkání
i	i	k8xC	i
jako	jako	k9	jako
místo	místo	k7c2	místo
setkání	setkání	k1gNnSc2	setkání
společenských	společenský	k2eAgFnPc2d1	společenská
nebo	nebo	k8xC	nebo
náboženského	náboženský	k2eAgNnSc2d1	náboženské
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jidiš	jidiš	k1gNnSc6	jidiš
se	se	k3xPyFc4	se
synagoga	synagoga	k1gFnSc1	synagoga
nazývá	nazývat	k5eAaImIp3nS	nazývat
ש	ש	k?	ש
"	"	kIx"	"
<g/>
šul	šulit	k5eAaImRp2nS	šulit
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
škola	škola	k1gFnSc1	škola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c4	v
ladino	ladino	k1gNnSc4	ladino
"	"	kIx"	"
<g/>
esnoga	esnoga	k1gFnSc1	esnoga
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Komplex	komplex	k1gInSc1	komplex
synagogy	synagoga	k1gFnSc2	synagoga
dnes	dnes	k6eAd1	dnes
často	často	k6eAd1	často
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
další	další	k2eAgNnPc4d1	další
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgFnPc4d1	sloužící
židovské	židovská	k1gFnPc4	židovská
obci	obec	k1gFnSc3	obec
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
synagogy	synagoga	k1gFnSc2	synagoga
někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
i	i	k9	i
byt	byt	k1gInSc1	byt
rabína	rabín	k1gMnSc2	rabín
nebo	nebo	k8xC	nebo
šámese	šámese	k1gFnSc2	šámese
(	(	kIx(	(
<g/>
z	z	k7c2	z
jidiš	jidiš	k1gNnSc2	jidiš
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
sluha	sluha	k1gMnSc1	sluha
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
správce	správce	k1gMnSc1	správce
objektu	objekt	k1gInSc2	objekt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
modlitební	modlitební	k2eAgFnPc1d1	modlitební
místnosti	místnost	k1gFnPc1	místnost
plní	plnit	k5eAaImIp3nP	plnit
i	i	k9	i
funkci	funkce	k1gFnSc4	funkce
studovny	studovna	k1gFnSc2	studovna
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
zimní	zimní	k2eAgFnSc2d1	zimní
modlitebny	modlitebna	k1gFnSc2	modlitebna
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
stavěny	stavit	k5eAaImNgInP	stavit
uvnitř	uvnitř	k6eAd1	uvnitř
domů	domů	k6eAd1	domů
nebo	nebo	k8xC	nebo
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
synagogy	synagoga	k1gFnSc2	synagoga
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
menším	malý	k2eAgInSc6d2	menší
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
usnadnilo	usnadnit	k5eAaPmAgNnS	usnadnit
vytápění	vytápění	k1gNnSc1	vytápění
v	v	k7c6	v
zimních	zimní	k2eAgInPc6d1	zimní
měsících	měsíc	k1gInPc6	měsíc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
sále	sál	k1gInSc6	sál
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
bohoslužby	bohoslužba	k1gFnPc1	bohoslužba
<g/>
.	.	kIx.	.
</s>
<s>
Instituce	instituce	k1gFnSc1	instituce
synagogy	synagoga	k1gFnSc2	synagoga
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
babylonském	babylonský	k2eAgNnSc6d1	babylonské
zajetí	zajetí	k1gNnSc6	zajetí
po	po	k7c6	po
zboření	zboření	k1gNnSc6	zboření
prvního	první	k4xOgInSc2	první
jeruzalémského	jeruzalémský	k2eAgInSc2d1	jeruzalémský
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
druhého	druhý	k4xOgInSc2	druhý
Chrámu	chrám	k1gInSc2	chrám
již	již	k6eAd1	již
synagogy	synagoga	k1gFnPc1	synagoga
představovaly	představovat	k5eAaImAgFnP	představovat
náboženská	náboženský	k2eAgNnPc4d1	náboženské
i	i	k8xC	i
společenská	společenský	k2eAgNnPc4d1	společenské
centra	centrum	k1gNnPc4	centrum
života	život	k1gInSc2	život
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
význam	význam	k1gInSc4	význam
synagog	synagoga	k1gFnPc2	synagoga
výrazně	výrazně	k6eAd1	výrazně
stoupl	stoupnout	k5eAaPmAgInS	stoupnout
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
po	po	k7c6	po
zboření	zboření	k1gNnSc3	zboření
druhého	druhý	k4xOgInSc2	druhý
Chrámu	chrám	k1gInSc2	chrám
roku	rok	k1gInSc2	rok
70	[number]	k4	70
<g/>
.	.	kIx.	.
</s>
<s>
Synagoga	synagoga	k1gFnSc1	synagoga
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
modlitebna	modlitebna	k1gFnSc1	modlitebna
(	(	kIx(	(
<g/>
bejt	bejt	k?	bejt
tefila	tefila	k1gFnSc1	tefila
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
škola	škola	k1gFnSc1	škola
(	(	kIx(	(
<g/>
bejt	bejt	k?	bejt
midraš	midraš	k1gInSc1	midraš
<g/>
)	)	kIx)	)
i	i	k9	i
jako	jako	k9	jako
dějiště	dějiště	k1gNnSc1	dějiště
veřejných	veřejný	k2eAgNnPc2d1	veřejné
jednání	jednání	k1gNnPc2	jednání
(	(	kIx(	(
<g/>
bejt	bejt	k?	bejt
kneset	kneset	k1gInSc1	kneset
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějším	pozdní	k2eAgInSc6d2	pozdější
vývoji	vývoj	k1gInSc6	vývoj
převážila	převážit	k5eAaPmAgFnS	převážit
modlitební	modlitební	k2eAgFnSc1d1	modlitební
funkce	funkce	k1gFnSc1	funkce
synagogy	synagoga	k1gFnSc2	synagoga
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ostatní	ostatní	k2eAgFnPc1d1	ostatní
funkce	funkce	k1gFnPc1	funkce
přebíraly	přebírat	k5eAaImAgFnP	přebírat
zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
budovy	budova	k1gFnPc1	budova
<g/>
:	:	kIx,	:
midraš	midraš	k1gInSc1	midraš
a	a	k8xC	a
židovská	židovský	k2eAgFnSc1d1	židovská
radnice	radnice	k1gFnSc1	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Synagogy	synagoga	k1gFnPc1	synagoga
vycházejí	vycházet	k5eAaImIp3nP	vycházet
konstrukčně	konstrukčně	k6eAd1	konstrukčně
z	z	k7c2	z
blízkovýchodních	blízkovýchodní	k2eAgInPc2d1	blízkovýchodní
chrámů	chrám	k1gInPc2	chrám
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
mešity	mešita	k1gFnSc2	mešita
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
neexistuje	existovat	k5eNaImIp3nS	existovat
zde	zde	k6eAd1	zde
žádný	žádný	k3yNgInSc1	žádný
závazný	závazný	k2eAgInSc1d1	závazný
vzor	vzor	k1gInSc1	vzor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ortodoxních	ortodoxní	k2eAgFnPc6d1	ortodoxní
synagogách	synagoga	k1gFnPc6	synagoga
je	být	k5eAaImIp3nS	být
ženám	žena	k1gFnPc3	žena
tradičně	tradičně	k6eAd1	tradičně
vyhrazen	vyhrazen	k2eAgInSc4d1	vyhrazen
oddělený	oddělený	k2eAgInSc4d1	oddělený
prostor	prostor	k1gInSc4	prostor
za	za	k7c7	za
stěnou	stěna	k1gFnSc7	stěna
hlavního	hlavní	k2eAgInSc2d1	hlavní
sálu	sál	k1gInSc2	sál
či	či	k8xC	či
galerie	galerie	k1gFnSc2	galerie
v	v	k7c6	v
patře	patro	k1gNnSc6	patro
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
reformní	reformní	k2eAgFnPc1d1	reformní
synagogy	synagoga	k1gFnPc1	synagoga
se	s	k7c7	s
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
uspořádáním	uspořádání	k1gNnSc7	uspořádání
blíží	blížit	k5eAaImIp3nS	blížit
křesťanským	křesťanský	k2eAgInPc3d1	křesťanský
kostelům	kostel	k1gInPc3	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Výzdoba	výzdoba	k1gFnSc1	výzdoba
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
omezuje	omezovat	k5eAaImIp3nS	omezovat
na	na	k7c4	na
ornamentální	ornamentální	k2eAgFnPc4d1	ornamentální
malby	malba	k1gFnPc4	malba
<g/>
,	,	kIx,	,
mozaiky	mozaika	k1gFnPc4	mozaika
<g/>
,	,	kIx,	,
závěsy	závěsa	k1gFnPc4	závěsa
<g/>
,	,	kIx,	,
vitráže	vitráž	k1gFnPc4	vitráž
apod.	apod.	kA	apod.
Zpodobnění	zpodobnění	k1gNnPc1	zpodobnění
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
soch	socha	k1gFnPc2	socha
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
synagogách	synagoga	k1gFnPc6	synagoga
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
vykládána	vykládán	k2eAgFnSc1d1	vykládána
jako	jako	k8xS	jako
modloslužba	modloslužba	k1gFnSc1	modloslužba
<g/>
.	.	kIx.	.
</s>
<s>
Synagoga	synagoga	k1gFnSc1	synagoga
je	být	k5eAaImIp3nS	být
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Izraeli	Izrael	k1gInSc3	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
jsou	být	k5eAaImIp3nP	být
synagogy	synagoga	k1gFnSc2	synagoga
orientovány	orientovat	k5eAaBmNgFnP	orientovat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Jeruzalému	Jeruzalém	k1gInSc3	Jeruzalém
a	a	k8xC	a
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Chrámové	chrámový	k2eAgFnSc3d1	chrámová
hoře	hora	k1gFnSc3	hora
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
její	její	k3xOp3gFnSc1	její
podlaha	podlaha	k1gFnSc1	podlaha
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
okolního	okolní	k2eAgInSc2d1	okolní
terénu	terén	k1gInSc2	terén
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
chazana	chazan	k1gMnSc4	chazan
(	(	kIx(	(
<g/>
kantora	kantor	k1gMnSc4	kantor
<g/>
,	,	kIx,	,
zpěváka	zpěvák	k1gMnSc4	zpěvák
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vede	vést	k5eAaImIp3nS	vést
modlitbu	modlitba	k1gFnSc4	modlitba
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
podlahy	podlaha	k1gFnSc2	podlaha
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
naplnění	naplnění	k1gNnSc4	naplnění
verše	verš	k1gInSc2	verš
Žalmu	žalm	k1gInSc2	žalm
130,1	[number]	k4	130,1
"	"	kIx"	"
<g/>
Z	z	k7c2	z
hlubin	hlubina	k1gFnPc2	hlubina
bezedných	bezedný	k2eAgFnPc2d1	bezedná
tě	ty	k3xPp2nSc4	ty
volám	volat	k5eAaImIp1nS	volat
<g/>
,	,	kIx,	,
Hospodine	Hospodin	k1gMnSc5	Hospodin
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
k	k	k7c3	k
Jeruzalému	Jeruzalém	k1gInSc3	Jeruzalém
orientované	orientovaný	k2eAgFnSc3d1	orientovaná
<g/>
)	)	kIx)	)
stěně	stěna	k1gFnSc3	stěna
je	být	k5eAaImIp3nS	být
aron	aron	k1gNnSc1	aron
ha-Kodeš	ha-Kodat	k5eAaPmIp2nS	ha-Kodat
-	-	kIx~	-
svatostánek	svatostánek	k1gInSc1	svatostánek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
jsou	být	k5eAaImIp3nP	být
uloženy	uložen	k2eAgInPc1d1	uložen
svitky	svitek	k1gInPc1	svitek
Tóry	tóra	k1gFnSc2	tóra
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
synagoga	synagoga	k1gFnSc1	synagoga
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
má	mít	k5eAaImIp3nS	mít
synagoga	synagoga	k1gFnSc1	synagoga
svitek	svitek	k1gInSc4	svitek
jeden	jeden	k4xCgInSc4	jeden
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
několik	několik	k4yIc1	několik
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tradičního	tradiční	k2eAgNnSc2d1	tradiční
uspořádání	uspořádání	k1gNnSc2	uspořádání
bývá	bývat	k5eAaImIp3nS	bývat
uprostřed	uprostřed	k7c2	uprostřed
synagogy	synagoga	k1gFnSc2	synagoga
vyvýšené	vyvýšený	k2eAgNnSc4d1	vyvýšené
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
buď	buď	k8xC	buď
bima	bima	k1gMnSc1	bima
nebo	nebo	k8xC	nebo
almemor	almemor	k1gMnSc1	almemor
<g/>
.	.	kIx.	.
</s>
<s>
Obojí	obojí	k4xRgFnSc1	obojí
znamená	znamenat	k5eAaImIp3nS	znamenat
pódium	pódium	k1gNnSc4	pódium
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
pódiu	pódium	k1gNnSc6	pódium
je	být	k5eAaImIp3nS	být
pult	pult	k1gInSc1	pult
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
používá	používat	k5eAaImIp3nS	používat
chazan	chazan	k1gInSc4	chazan
k	k	k7c3	k
čtení	čtení	k1gNnSc3	čtení
modliteb	modlitba	k1gFnPc2	modlitba
a	a	k8xC	a
ke	k	k7c3	k
čtení	čtení	k1gNnSc3	čtení
z	z	k7c2	z
Tóry	tóra	k1gFnSc2	tóra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
synagogách	synagoga	k1gFnPc6	synagoga
postavených	postavený	k2eAgInPc2d1	postavený
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
bima	bima	k6eAd1	bima
často	často	k6eAd1	často
součástí	součást	k1gFnSc7	součást
vyvýšené	vyvýšený	k2eAgFnSc2d1	vyvýšená
plošiny	plošina	k1gFnSc2	plošina
u	u	k7c2	u
Aronu	Aron	k1gInSc2	Aron
<g/>
.	.	kIx.	.
</s>
<s>
Svitky	svitek	k1gInPc1	svitek
Tóry	tóra	k1gFnSc2	tóra
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
uchovávány	uchovávat	k5eAaImNgInP	uchovávat
ve	v	k7c6	v
svatostánku	svatostánek	k1gInSc6	svatostánek
<g/>
.	.	kIx.	.
</s>
<s>
Aron	Aron	k1gMnSc1	Aron
je	být	k5eAaImIp3nS	být
připomínkou	připomínka	k1gFnSc7	připomínka
schrány	schrána	k1gFnSc2	schrána
(	(	kIx(	(
<g/>
archy	archa	k1gFnSc2	archa
<g/>
)	)	kIx)	)
úmluvy	úmluva	k1gFnSc2	úmluva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
bývala	bývat	k5eAaImAgFnS	bývat
uložena	uložit	k5eAaPmNgFnS	uložit
ve	v	k7c6	v
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
svatyni	svatyně	k1gFnSc6	svatyně
Jeruzalémského	jeruzalémský	k2eAgInSc2d1	jeruzalémský
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
zakryt	zakrýt	k5eAaPmNgMnS	zakrýt
oponou	opona	k1gFnSc7	opona
(	(	kIx(	(
<g/>
parochet	parochet	k1gInSc1	parochet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
rovněž	rovněž	k9	rovněž
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
chrámovou	chrámový	k2eAgFnSc4d1	chrámová
oponu	opona	k1gFnSc4	opona
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
oddělovala	oddělovat	k5eAaImAgFnS	oddělovat
svatyni	svatyně	k1gFnSc4	svatyně
od	od	k7c2	od
velesvatyně	velesvatyně	k1gFnSc2	velesvatyně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
synagoze	synagoga	k1gFnSc6	synagoga
také	také	k9	také
bývá	bývat	k5eAaImIp3nS	bývat
věčné	věčný	k2eAgNnSc1d1	věčné
světlo	světlo	k1gNnSc1	světlo
(	(	kIx(	(
<g/>
ner	ner	k?	ner
tamid	tamid	k1gInSc1	tamid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
symbolizující	symbolizující	k2eAgInSc1d1	symbolizující
nepřetržitě	přetržitě	k6eNd1	přetržitě
hořící	hořící	k2eAgInSc1d1	hořící
menoru	menor	k1gInSc3	menor
jeruzalémského	jeruzalémský	k2eAgInSc2d1	jeruzalémský
chrámu	chrám	k1gInSc2	chrám
a	a	k8xC	a
přítomnost	přítomnost	k1gFnSc1	přítomnost
Boží	božit	k5eAaImIp3nS	božit
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
synagogou	synagoga	k1gFnSc7	synagoga
na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
Bejt	Bejt	k?	Bejt
ha-midraš	haidraš	k1gInSc1	ha-midraš
Ger	Gera	k1gFnPc2	Gera
náležící	náležící	k2eAgFnSc2d1	náležící
gerským	gerská	k1gFnPc3	gerská
chasidům	chasid	k1gMnPc3	chasid
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
více	hodně	k6eAd2	hodně
než	než	k8xS	než
8	[number]	k4	8
500	[number]	k4	500
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
údajů	údaj	k1gInPc2	údaj
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
Belz	Belz	k1gInSc1	Belz
Beis	Beisa	k1gFnPc2	Beisa
ha-midraš	haidraš	k5eAaPmIp2nS	ha-midraš
ha-gadol	haadol	k1gInSc1	ha-gadol
v	v	k7c4	v
Kirjat	Kirjat	k2eAgInSc4d1	Kirjat
Belz	Belz	k1gInSc4	Belz
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
náležící	náležící	k2eAgFnSc2d1	náležící
belzským	belzský	k2eAgMnPc3d1	belzský
chasidům	chasid	k1gMnPc3	chasid
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
6	[number]	k4	6
000	[number]	k4	000
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
synagogou	synagoga	k1gFnSc7	synagoga
mimo	mimo	k7c4	mimo
Izrael	Izrael	k1gInSc4	Izrael
je	být	k5eAaImIp3nS	být
synagoga	synagoga	k1gFnSc1	synagoga
Temple	templ	k1gInSc5	templ
Emanu-El	Emanu-El	k1gInSc4	Emanu-El
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
synagogou	synagoga	k1gFnSc7	synagoga
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
čtvrtou	čtvrtá	k1gFnSc4	čtvrtá
největší	veliký	k2eAgFnSc4d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
Velká	velký	k2eAgFnSc1d1	velká
synagoga	synagoga	k1gFnSc1	synagoga
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Dohány	dohán	k1gInPc4	dohán
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
současně	současně	k6eAd1	současně
druhou	druhý	k4xOgFnSc7	druhý
největší	veliký	k2eAgFnSc7d3	veliký
synagogou	synagoga	k1gFnSc7	synagoga
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
Velká	velký	k2eAgFnSc1d1	velká
synagoga	synagoga	k1gFnSc1	synagoga
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
stále	stále	k6eAd1	stále
sloužící	sloužící	k2eAgFnSc7d1	sloužící
synagogou	synagoga	k1gFnSc7	synagoga
na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
pražská	pražský	k2eAgFnSc1d1	Pražská
Staronová	staronový	k2eAgFnSc1d1	staronová
synagoga	synagoga	k1gFnSc1	synagoga
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
