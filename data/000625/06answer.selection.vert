<s>
Synagoga	synagoga	k1gFnSc1	synagoga
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
σ	σ	k?	σ
<g/>
,	,	kIx,	,
synagógé	synagógé	k6eAd1	synagógé
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
hebrejsky	hebrejsky	k6eAd1	hebrejsky
ב	ב	k?	ב
כ	כ	k?	כ
<g/>
,	,	kIx,	,
bejt	bejt	k?	bejt
kneset	kneset	k1gInSc1	kneset
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
dům	dům	k1gInSc1	dům
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
ב	ב	k?	ב
ת	ת	k?	ת
<g/>
,	,	kIx,	,
bejt	bejt	k?	bejt
tfila	tfila	k1gFnSc1	tfila
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
dům	dům	k1gInSc1	dům
modlitby	modlitba	k1gFnSc2	modlitba
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
židovská	židovský	k2eAgFnSc1d1	židovská
modlitebna	modlitebna	k1gFnSc1	modlitebna
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
kromě	kromě	k7c2	kromě
místa	místo	k1gNnSc2	místo
pro	pro	k7c4	pro
bohoslužebná	bohoslužebný	k2eAgNnPc4d1	bohoslužebné
setkání	setkání	k1gNnPc4	setkání
i	i	k8xC	i
jako	jako	k9	jako
místo	místo	k7c2	místo
setkání	setkání	k1gNnSc2	setkání
společenských	společenský	k2eAgFnPc2d1	společenská
nebo	nebo	k8xC	nebo
náboženského	náboženský	k2eAgNnSc2d1	náboženské
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
