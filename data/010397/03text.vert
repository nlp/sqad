<p>
<s>
Označení	označení	k1gNnSc1	označení
vazal	vazal	k1gMnSc1	vazal
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
lenních	lenní	k2eAgInPc2d1	lenní
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
tvořily	tvořit	k5eAaImAgFnP	tvořit
podstatu	podstata	k1gFnSc4	podstata
feudálního	feudální	k2eAgInSc2d1	feudální
státu	stát	k1gInSc2	stát
zejména	zejména	k9	zejména
ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Vazal	vazal	k1gMnSc1	vazal
bylo	být	k5eAaImAgNnS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
lenní	lenní	k2eAgMnSc1d1	lenní
pán	pán	k1gMnSc1	pán
(	(	kIx(	(
<g/>
též	též	k9	též
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xC	jako
vrchní	vrchní	k2eAgMnSc1d1	vrchní
vlastník	vlastník	k1gMnSc1	vlastník
nebo	nebo	k8xC	nebo
senior	senior	k1gMnSc1	senior
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
propůjčil	propůjčit	k5eAaPmAgInS	propůjčit
léno	léno	k1gNnSc4	léno
k	k	k7c3	k
užívání	užívání	k1gNnSc3	užívání
<g/>
.	.	kIx.	.
</s>
<s>
Lénem	léno	k1gNnSc7	léno
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
půda	půda	k1gFnSc1	půda
<g/>
,	,	kIx,	,
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
hodnost	hodnost	k1gFnSc4	hodnost
nebo	nebo	k8xC	nebo
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Vazal	vazal	k1gMnSc1	vazal
měl	mít	k5eAaImAgMnS	mít
právo	práv	k2eAgNnSc4d1	právo
léno	léno	k1gNnSc4	léno
užívat	užívat	k5eAaImF	užívat
jen	jen	k9	jen
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
lenního	lenní	k2eAgMnSc2d1	lenní
pána	pán	k1gMnSc2	pán
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
byl	být	k5eAaImAgInS	být
navíc	navíc	k6eAd1	navíc
vázán	vázat	k5eAaImNgInS	vázat
řadou	řada	k1gFnSc7	řada
povinností	povinnost	k1gFnPc2	povinnost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
poplatky	poplatek	k1gInPc1	poplatek
či	či	k8xC	či
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
službou	služba	k1gFnSc7	služba
<g/>
.	.	kIx.	.
</s>
<s>
Souhrn	souhrn	k1gInSc1	souhrn
povinností	povinnost	k1gFnPc2	povinnost
vazala	vazal	k1gMnSc4	vazal
vůči	vůči	k7c3	vůči
lennímu	lenní	k2eAgMnSc3d1	lenní
pánu	pán	k1gMnSc3	pán
se	se	k3xPyFc4	se
označoval	označovat	k5eAaImAgMnS	označovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
věrnost	věrnost	k1gFnSc1	věrnost
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
fidelitas	fidelitas	k1gInSc1	fidelitas
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
porušení	porušení	k1gNnSc1	porušení
bylo	být	k5eAaImAgNnS	být
důvodem	důvod	k1gInSc7	důvod
k	k	k7c3	k
odnětí	odnětí	k1gNnSc3	odnětí
léna	léno	k1gNnSc2	léno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Malý	Malý	k1gMnSc1	Malý
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
,	,	kIx,	,
Dějiny	dějiny	k1gFnPc1	dějiny
českého	český	k2eAgNnSc2d1	české
a	a	k8xC	a
československého	československý	k2eAgNnSc2d1	Československé
práva	právo	k1gNnSc2	právo
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
Linde	Lind	k1gInSc5	Lind
Praha	Praha	k1gFnSc1	Praha
2003	[number]	k4	2003
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7201	[number]	k4	7201
<g/>
-	-	kIx~	-
<g/>
433	[number]	k4	433
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Vasall	Vasalla	k1gFnPc2	Vasalla
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
