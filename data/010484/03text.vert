<p>
<s>
Kamenný	kamenný	k2eAgInSc1d1	kamenný
most	most	k1gInSc1	most
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
(	(	kIx(	(
<g/>
neoficiálně	neoficiálně	k6eAd1	neoficiálně
také	také	k9	také
Jelení	jelení	k2eAgInSc4d1	jelení
most	most	k1gInSc4	most
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejstarším	starý	k2eAgInSc7d3	nejstarší
dochovaným	dochovaný	k2eAgInSc7d1	dochovaný
mostem	most	k1gInSc7	most
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
(	(	kIx(	(
<g/>
historicky	historicky	k6eAd1	historicky
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
druhý	druhý	k4xOgInSc1	druhý
po	po	k7c6	po
zaniklém	zaniklý	k2eAgNnSc6d1	zaniklé
Juditině	Juditin	k2eAgNnSc6d1	Juditin
mostu	most	k1gInSc2	most
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgInSc1d1	postavený
v	v	k7c6	v
gotickém	gotický	k2eAgInSc6d1	gotický
slohu	sloh	k1gInSc6	sloh
<g/>
,	,	kIx,	,
překlenuje	překlenovat	k5eAaImIp3nS	překlenovat
řeku	řeka	k1gFnSc4	řeka
Otavu	Otava	k1gFnSc4	Otava
asi	asi	k9	asi
110	[number]	k4	110
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mostě	most	k1gInSc6	most
jsou	být	k5eAaImIp3nP	být
umístěny	umístěn	k2eAgFnPc1d1	umístěna
repliky	replika	k1gFnPc1	replika
barokních	barokní	k2eAgFnPc2d1	barokní
soch	socha	k1gFnPc2	socha
a	a	k8xC	a
kříže	kříž	k1gInSc2	kříž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Neoficiálně	neoficiálně	k6eAd1	neoficiálně
se	se	k3xPyFc4	se
most	most	k1gInSc1	most
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Jelení	jelení	k2eAgInSc1d1	jelení
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
jmenovat	jmenovat	k5eAaBmF	jmenovat
podle	podle	k7c2	podle
prvního	první	k4xOgNnSc2	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
přes	přes	k7c4	přes
něj	on	k3xPp3gMnSc4	on
přejde	přejít	k5eAaPmIp3nS	přejít
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
břeh	břeh	k1gInSc4	břeh
Otavy	Otava	k1gFnSc2	Otava
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
ale	ale	k9	ale
něco	něco	k3yInSc1	něco
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
nikdo	nikdo	k3yNnSc1	nikdo
nečekal	čekat	k5eNaImAgMnS	čekat
a	a	k8xC	a
prvním	první	k4xOgMnSc7	první
chodcem	chodec	k1gMnSc7	chodec
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jelen	jelen	k1gMnSc1	jelen
z	z	k7c2	z
blízkých	blízký	k2eAgInPc2d1	blízký
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
most	most	k1gInSc1	most
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
podle	podle	k7c2	podle
něho	on	k3xPp3gMnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
ale	ale	k8xC	ale
zavítáte	zavítat	k5eAaPmIp2nP	zavítat
do	do	k7c2	do
královského	královský	k2eAgNnSc2d1	královské
města	město	k1gNnSc2	město
Písek	Písek	k1gInSc1	Písek
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
ho	on	k3xPp3gMnSc4	on
bude	být	k5eAaImBp3nS	být
nazývat	nazývat	k5eAaImF	nazývat
"	"	kIx"	"
<g/>
starý	starý	k2eAgInSc4d1	starý
most	most	k1gInSc4	most
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
most	most	k1gInSc1	most
bude	být	k5eAaImBp3nS	být
oficiálně	oficiálně	k6eAd1	oficiálně
jmenovat	jmenovat	k5eAaImF	jmenovat
Kamenný	kamenný	k2eAgInSc4d1	kamenný
most	most	k1gInSc4	most
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
skončil	skončit	k5eAaPmAgMnS	skončit
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
spor	spor	k1gInSc4	spor
o	o	k7c4	o
název	název	k1gInSc4	název
památky	památka	k1gFnSc2	památka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavba	stavba	k1gFnSc1	stavba
==	==	k?	==
</s>
</p>
<p>
<s>
Most	most	k1gInSc1	most
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
zřejmě	zřejmě	k6eAd1	zřejmě
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
čtvrtině	čtvrtina	k1gFnSc6	čtvrtina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
během	během	k7c2	během
vlády	vláda	k1gFnSc2	vláda
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
písemné	písemný	k2eAgInPc4d1	písemný
záznamy	záznam	k1gInPc4	záznam
o	o	k7c4	o
mostu	most	k1gInSc2	most
však	však	k9	však
pocházejí	pocházet	k5eAaImIp3nP	pocházet
až	až	k9	až
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1348	[number]	k4	1348
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
výnosem	výnos	k1gInSc7	výnos
nařídil	nařídit	k5eAaPmAgMnS	nařídit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokuty	pokuta	k1gFnPc1	pokuta
vybírané	vybíraný	k2eAgFnPc1d1	vybíraná
v	v	k7c6	v
městě	město	k1gNnSc6	město
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
používány	používán	k2eAgFnPc1d1	používána
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
údržbu	údržba	k1gFnSc4	údržba
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
stavěn	stavit	k5eAaImNgInS	stavit
na	na	k7c6	na
suchu	sucho	k1gNnSc6	sucho
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
dostavbě	dostavba	k1gFnSc6	dostavba
byla	být	k5eAaImAgFnS	být
řeka	řeka	k1gFnSc1	řeka
uměle	uměle	k6eAd1	uměle
převedena	převést	k5eAaPmNgFnS	převést
do	do	k7c2	do
nového	nový	k2eAgNnSc2d1	nové
koryta	koryto	k1gNnSc2	koryto
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
procházelo	procházet	k5eAaImAgNnS	procházet
pod	pod	k7c7	pod
mostem	most	k1gInSc7	most
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
důležitou	důležitý	k2eAgFnSc7d1	důležitá
spojnicí	spojnice	k1gFnSc7	spojnice
na	na	k7c6	na
Zlaté	zlatý	k2eAgFnSc6d1	zlatá
stezce	stezka	k1gFnSc6	stezka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spojovala	spojovat	k5eAaImAgFnS	spojovat
baltské	baltský	k2eAgFnPc4d1	Baltská
oblasti	oblast	k1gFnPc4	oblast
se	s	k7c7	s
Středomořím	středomoří	k1gNnSc7	středomoří
a	a	k8xC	a
dovážela	dovážet	k5eAaImAgFnS	dovážet
se	se	k3xPyFc4	se
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
sůl	sůl	k1gFnSc1	sůl
z	z	k7c2	z
Bavor	Bavory	k1gInPc2	Bavory
do	do	k7c2	do
píseckého	písecký	k2eAgInSc2d1	písecký
solné	solný	k2eAgFnPc4d1	solná
skladu	sklad	k1gInSc2	sklad
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
královského	královský	k2eAgInSc2d1	královský
patentu	patent	k1gInSc2	patent
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1707	[number]	k4	1707
<g/>
.	.	kIx.	.
</s>
<s>
Patent	patent	k1gInSc1	patent
přikazoval	přikazovat	k5eAaImAgInS	přikazovat
prodávat	prodávat	k5eAaImF	prodávat
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
sůl	sůl	k1gFnSc4	sůl
výhradně	výhradně	k6eAd1	výhradně
z	z	k7c2	z
habsburských	habsburský	k2eAgFnPc2d1	habsburská
soliváren	solivárna	k1gFnPc2	solivárna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1797	[number]	k4	1797
vedla	vést	k5eAaImAgFnS	vést
po	po	k7c6	po
mostu	most	k1gInSc2	most
erární	erární	k2eAgFnPc1d1	erární
silnice	silnice	k1gFnPc1	silnice
z	z	k7c2	z
Vodňan	Vodňana	k1gFnPc2	Vodňana
do	do	k7c2	do
Blatné	blatný	k2eAgFnSc2d1	Blatná
a	a	k8xC	a
Nepomuku	Nepomuk	k1gInSc2	Nepomuk
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
erár	erár	k1gInSc1	erár
platil	platit	k5eAaImAgInS	platit
městu	město	k1gNnSc3	město
paušál	paušál	k1gInSc4	paušál
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
743	[number]	k4	743
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
most	most	k1gInSc1	most
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1825	[number]	k4	1825
převzal	převzít	k5eAaPmAgMnS	převzít
do	do	k7c2	do
státní	státní	k2eAgFnSc2d1	státní
režie	režie	k1gFnSc2	režie
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1827	[number]	k4	1827
do	do	k7c2	do
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Most	most	k1gInSc1	most
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
109,75	[number]	k4	109,75
m	m	kA	m
včetně	včetně	k7c2	včetně
zdí	zeď	k1gFnPc2	zeď
na	na	k7c6	na
levém	levý	k2eAgNnSc6d1	levé
předmostí	předmostí	k1gNnSc6	předmostí
a	a	k8xC	a
široký	široký	k2eAgInSc4d1	široký
6,25	[number]	k4	6,25
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vozovka	vozovka	k1gFnSc1	vozovka
4,5	[number]	k4	4,5
m.	m.	k?	m.
Most	most	k1gInSc1	most
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
6	[number]	k4	6
pilířích	pilíř	k1gInPc6	pilíř
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
7	[number]	k4	7
oblouky	oblouk	k1gInPc7	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Oblouky	oblouk	k1gInPc1	oblouk
mostu	most	k1gInSc2	most
byly	být	k5eAaImAgInP	být
často	často	k6eAd1	často
poškozovány	poškozován	k2eAgFnPc1d1	poškozována
povodněmi	povodeň	k1gFnPc7	povodeň
<g/>
,	,	kIx,	,
do	do	k7c2	do
dnešního	dnešní	k2eAgInSc2d1	dnešní
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
6	[number]	k4	6
původních	původní	k2eAgInPc2d1	původní
oblouků	oblouk	k1gInPc2	oblouk
o	o	k7c4	o
rozpětí	rozpětí	k1gNnSc4	rozpětí
7,0	[number]	k4	7,0
<g/>
-	-	kIx~	-
<g/>
8,2	[number]	k4	8,2
m	m	kA	m
<g/>
,	,	kIx,	,
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1768	[number]	k4	1768
pochází	pocházet	k5eAaImIp3nS	pocházet
segmentový	segmentový	k2eAgInSc4d1	segmentový
oblouk	oblouk	k1gInSc4	oblouk
o	o	k7c4	o
rozpětí	rozpětí	k1gNnSc4	rozpětí
13	[number]	k4	13
m.	m.	k?	m.
Tento	tento	k3xDgInSc4	tento
oblouk	oblouk	k1gInSc4	oblouk
byl	být	k5eAaImAgInS	být
určený	určený	k2eAgMnSc1d1	určený
k	k	k7c3	k
proplouvání	proplouvání	k1gNnSc3	proplouvání
vorů	vor	k1gInPc2	vor
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
most	most	k1gInSc1	most
míval	mívat	k5eAaImAgMnS	mívat
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
koncích	konec	k1gInPc6	konec
2	[number]	k4	2
mostní	mostní	k2eAgFnPc4d1	mostní
věže	věž	k1gFnPc4	věž
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
nedochovaly	dochovat	k5eNaPmAgInP	dochovat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
se	se	k3xPyFc4	se
zřítila	zřítit	k5eAaPmAgFnS	zřítit
roku	rok	k1gInSc2	rok
1768	[number]	k4	1768
během	během	k7c2	během
povodně	povodeň	k1gFnSc2	povodeň
i	i	k9	i
s	s	k7c7	s
hlásným	hlásný	k2eAgMnSc7d1	hlásný
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
byla	být	k5eAaImAgFnS	být
úmyslně	úmyslně	k6eAd1	úmyslně
stržena	strhnout	k5eAaPmNgFnS	strhnout
kvůli	kvůli	k7c3	kvůli
nárokům	nárok	k1gInPc3	nárok
na	na	k7c4	na
dopravu	doprava	k1gFnSc4	doprava
roku	rok	k1gInSc2	rok
1825	[number]	k4	1825
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
za	za	k7c7	za
mostem	most	k1gInSc7	most
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
si	se	k3xPyFc3	se
prohlédnout	prohlédnout	k5eAaPmF	prohlédnout
jejich	jejich	k3xOp3gInPc4	jejich
fragmenty	fragment	k1gInPc4	fragment
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
vyloveny	vylovit	k5eAaPmNgInP	vylovit
ze	z	k7c2	z
dna	dno	k1gNnSc2	dno
řeky	řeka	k1gFnSc2	řeka
během	během	k7c2	během
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
mnohých	mnohý	k2eAgFnPc2d1	mnohá
oprav	oprava	k1gFnPc2	oprava
mostu	most	k1gInSc2	most
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sochy	socha	k1gFnPc1	socha
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
mostě	most	k1gInSc6	most
můžeme	moct	k5eAaImIp1nP	moct
vidět	vidět	k5eAaImF	vidět
pískovcové	pískovcový	k2eAgFnPc4d1	pískovcová
repliky	replika	k1gFnPc4	replika
barokních	barokní	k2eAgFnPc2d1	barokní
soch	socha	k1gFnPc2	socha
ve	v	k7c6	v
čtyřech	čtyři	k4xCgNnPc6	čtyři
sousoších	sousoší	k1gNnPc6	sousoší
<g/>
.	.	kIx.	.
</s>
<s>
Sousoší	sousoší	k1gNnSc1	sousoší
Kalvárie	Kalvárie	k1gFnSc2	Kalvárie
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc1	století
z	z	k7c2	z
dílny	dílna	k1gFnSc2	dílna
čimelického	čimelický	k2eAgMnSc2d1	čimelický
umělce	umělec	k1gMnSc2	umělec
Jana	Jan	k1gMnSc2	Jan
Hammera	Hammer	k1gMnSc2	Hammer
<g/>
.	.	kIx.	.
</s>
<s>
Můžete	moct	k5eAaImIp2nP	moct
vidět	vidět	k5eAaImF	vidět
sochy	socha	k1gFnSc2	socha
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
Maří	mařit	k5eAaImIp3nP	mařit
Magdalény	Magdaléna	k1gFnSc2	Magdaléna
a	a	k8xC	a
apoštola	apoštol	k1gMnSc2	apoštol
Jana	Jan	k1gMnSc2	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Kříž	Kříž	k1gMnSc1	Kříž
s	s	k7c7	s
Kristem	Kristus	k1gMnSc7	Kristus
měří	měřit	k5eAaImIp3nS	měřit
6	[number]	k4	6
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
originál	originál	k1gInSc1	originál
ulil	ulejt	k5eAaPmAgMnS	ulejt
cínař	cínař	k1gMnSc1	cínař
Václav	Václav	k1gMnSc1	Václav
Hanzl	Hanzl	k1gMnSc1	Hanzl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Repliky	replika	k1gFnPc1	replika
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
sousoší	sousoší	k1gNnSc1	sousoší
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
s	s	k7c7	s
dvěma	dva	k4xCgMnPc7	dva
anděly	anděl	k1gMnPc7	anděl
(	(	kIx(	(
<g/>
originál	originál	k1gInSc1	originál
od	od	k7c2	od
neznámého	známý	k2eNgMnSc2d1	neznámý
autora	autor	k1gMnSc2	autor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
května	květen	k1gInSc2	květen
2004	[number]	k4	2004
je	být	k5eAaImIp3nS	být
kompletní	kompletní	k2eAgFnSc1d1	kompletní
se	s	k7c7	s
sochou	socha	k1gFnSc7	socha
anděla	anděl	k1gMnSc2	anděl
od	od	k7c2	od
sochaře	sochař	k1gMnSc2	sochař
Kačera	kačer	k1gMnSc2	kačer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následující	následující	k2eAgFnPc1d1	následující
sochy	socha	k1gFnPc1	socha
–	–	k?	–
Sv.	sv.	kA	sv.
Anny	Anna	k1gFnSc2	Anna
Samotřetí	Samotřetí	k1gNnSc2	Samotřetí
a	a	k8xC	a
sv.	sv.	kA	sv.
Antonína	Antonín	k1gMnSc2	Antonín
Paduánského	paduánský	k2eAgMnSc2d1	paduánský
(	(	kIx(	(
<g/>
originály	originál	k1gInPc1	originál
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1770	[number]	k4	1770
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
byly	být	k5eAaImAgFnP	být
sochy	socha	k1gFnPc1	socha
uloženy	uložit	k5eAaPmNgFnP	uložit
v	v	k7c4	v
koncertní	koncertní	k2eAgFnPc4d1	koncertní
síní	síň	k1gFnSc7	síň
Kostela	kostel	k1gInSc2	kostel
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
května	květen	k1gInSc2	květen
2010	[number]	k4	2010
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
restaurování	restaurování	k1gNnSc6	restaurování
uloženy	uložit	k5eAaPmNgInP	uložit
společně	společně	k6eAd1	společně
s	s	k7c7	s
rozměrnými	rozměrný	k2eAgInPc7d1	rozměrný
obrazy	obraz	k1gInPc7	obraz
českých	český	k2eAgMnPc2d1	český
panovníků	panovník	k1gMnPc2	panovník
v	v	k7c6	v
expozici	expozice	k1gFnSc6	expozice
Prácheňského	prácheňský	k2eAgNnSc2d1	Prácheňské
muzea	muzeum	k1gNnSc2	muzeum
ve	v	k7c6	v
sladovně	sladovna	k1gFnSc6	sladovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fotogalerie	Fotogalerie	k1gFnSc1	Fotogalerie
soch	socha	k1gFnPc2	socha
a	a	k8xC	a
sousoší	sousoší	k1gNnSc4	sousoší
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Generální	generální	k2eAgFnSc1d1	generální
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1994-1996	[number]	k4	1994-1996
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
stáří	stáří	k1gNnSc2	stáří
mostu	most	k1gInSc2	most
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
generální	generální	k2eAgFnSc3d1	generální
rekonstrukci	rekonstrukce	k1gFnSc3	rekonstrukce
mostu	most	k1gInSc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
fungovala	fungovat	k5eAaImAgFnS	fungovat
přes	přes	k7c4	přes
most	most	k1gInSc4	most
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
ukotvení	ukotvení	k1gNnSc3	ukotvení
základů	základ	k1gInPc2	základ
do	do	k7c2	do
skály	skála	k1gFnSc2	skála
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
tomu	ten	k3xDgMnSc3	ten
vydržel	vydržet	k5eAaPmAgMnS	vydržet
most	most	k1gInSc4	most
povodně	povodně	k6eAd1	povodně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c4	o
restaurování	restaurování	k1gNnSc4	restaurování
barokních	barokní	k2eAgFnPc2d1	barokní
soch	socha	k1gFnPc2	socha
a	a	k8xC	a
nahrazení	nahrazení	k1gNnSc2	nahrazení
replikami	replika	k1gFnPc7	replika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Povodně	povodeň	k1gFnPc1	povodeň
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
katastrofálních	katastrofální	k2eAgFnPc6d1	katastrofální
povodních	povodeň	k1gFnPc6	povodeň
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2002	[number]	k4	2002
servala	servat	k5eAaPmAgFnS	servat
voda	voda	k1gFnSc1	voda
z	z	k7c2	z
mostu	most	k1gInSc2	most
barokní	barokní	k2eAgFnSc4d1	barokní
sochu	socha	k1gFnSc4	socha
anděla	anděl	k1gMnSc2	anděl
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
kamenného	kamenný	k2eAgNnSc2d1	kamenné
zábradlí	zábradlí	k1gNnSc2	zábradlí
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
v	v	k7c6	v
době	doba	k1gFnSc6	doba
největší	veliký	k2eAgFnSc1d3	veliký
kulminace	kulminace	k1gFnSc1	kulminace
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
až	až	k6eAd1	až
2	[number]	k4	2
metry	metr	k1gInPc4	metr
nad	nad	k7c4	nad
vozovku	vozovka	k1gFnSc4	vozovka
a	a	k8xC	a
z	z	k7c2	z
mostu	most	k1gInSc2	most
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
pozorovat	pozorovat	k5eAaImF	pozorovat
pouze	pouze	k6eAd1	pouze
kříž	kříž	k1gInSc4	kříž
s	s	k7c7	s
Kristem	Kristus	k1gMnSc7	Kristus
a	a	k8xC	a
části	část	k1gFnSc2	část
soch	socha	k1gFnPc2	socha
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
naštěstí	naštěstí	k6eAd1	naštěstí
velkou	velký	k2eAgFnSc4d1	velká
vodu	voda	k1gFnSc4	voda
přežil	přežít	k5eAaPmAgMnS	přežít
bez	bez	k7c2	bez
většího	veliký	k2eAgNnSc2d2	veliký
poškození	poškození	k1gNnSc2	poškození
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
zakotven	zakotvit	k5eAaPmNgInS	zakotvit
do	do	k7c2	do
skály	skála	k1gFnSc2	skála
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
jeho	jeho	k3xOp3gFnSc1	jeho
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
2	[number]	k4	2
dny	den	k1gInPc4	den
po	po	k7c6	po
povodních	povodeň	k1gFnPc6	povodeň
byl	být	k5eAaImAgInS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
provizorním	provizorní	k2eAgNnSc7d1	provizorní
dřevěným	dřevěný	k2eAgNnSc7d1	dřevěné
zábradlím	zábradlí	k1gNnSc7	zábradlí
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
otevřen	otevřít	k5eAaPmNgInS	otevřít
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
následné	následný	k2eAgFnSc6d1	následná
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
byl	být	k5eAaImAgInS	být
technologicky	technologicky	k6eAd1	technologicky
zabezpečen	zabezpečit	k5eAaPmNgInS	zabezpečit
proti	proti	k7c3	proti
dalším	další	k2eAgFnPc3d1	další
povodním	povodeň	k1gFnPc3	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Stržené	stržený	k2eAgFnPc1d1	stržená
komponenty	komponenta	k1gFnPc1	komponenta
byly	být	k5eAaImAgFnP	být
vyloveny	vylovit	k5eAaPmNgFnP	vylovit
z	z	k7c2	z
řeky	řeka	k1gFnSc2	řeka
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
nainstalovány	nainstalovat	k5eAaPmNgInP	nainstalovat
<g/>
,	,	kIx,	,
nalezla	nalézt	k5eAaBmAgFnS	nalézt
se	se	k3xPyFc4	se
i	i	k9	i
většina	většina	k1gFnSc1	většina
dobových	dobový	k2eAgInPc2d1	dobový
kamenů	kámen	k1gInPc2	kámen
na	na	k7c6	na
zábradlí	zábradlí	k1gNnSc6	zábradlí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
most	most	k1gInSc1	most
utrpěl	utrpět	k5eAaPmAgInS	utrpět
i	i	k9	i
újmu	újma	k1gFnSc4	újma
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
nevratné	vratný	k2eNgFnSc2d1	nevratná
ztráty	ztráta	k1gFnSc2	ztráta
sochy	socha	k1gFnSc2	socha
anděla	anděl	k1gMnSc2	anděl
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
novější	nový	k2eAgFnSc7d2	novější
replikou	replika	k1gFnSc7	replika
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c4	pod
kamennou	kamenný	k2eAgFnSc4d1	kamenná
dlažbu	dlažba	k1gFnSc4	dlažba
bylo	být	k5eAaImAgNnS	být
umístěno	umístěn	k2eAgNnSc1d1	umístěno
betonové	betonový	k2eAgNnSc1d1	betonové
lože	lože	k1gNnSc1	lože
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
byl	být	k5eAaImAgInS	být
dále	daleko	k6eAd2	daleko
provrtán	provrtán	k2eAgInSc4d1	provrtán
piloty	pilot	k1gInPc4	pilot
a	a	k8xC	a
svázán	svázán	k2eAgInSc1d1	svázán
železobetonovými	železobetonový	k2eAgInPc7d1	železobetonový
pruty	prut	k1gInPc7	prut
<g/>
.	.	kIx.	.
</s>
<s>
Oprava	oprava	k1gFnSc1	oprava
mostu	most	k1gInSc2	most
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
díky	díky	k7c3	díky
veřejné	veřejný	k2eAgFnSc3d1	veřejná
sbírce	sbírka	k1gFnSc3	sbírka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
spontánně	spontánně	k6eAd1	spontánně
po	po	k7c6	po
povodni	povodeň	k1gFnSc6	povodeň
rozeběhla	rozeběhnout	k5eAaPmAgFnS	rozeběhnout
a	a	k8xC	a
pomohla	pomoct	k5eAaPmAgFnS	pomoct
tak	tak	k6eAd1	tak
městu	město	k1gNnSc3	město
most	most	k1gInSc4	most
rychle	rychle	k6eAd1	rychle
opravit	opravit	k5eAaPmF	opravit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
mohl	moct	k5eAaImAgInS	moct
honosit	honosit	k5eAaImF	honosit
touto	tento	k3xDgFnSc7	tento
nejznámější	známý	k2eAgFnSc7d3	nejznámější
píseckou	písecký	k2eAgFnSc7d1	Písecká
památkou	památka	k1gFnSc7	památka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Orkán	orkán	k1gInSc4	orkán
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2007	[number]	k4	2007
==	==	k?	==
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
při	při	k7c6	při
povodních	povodeň	k1gFnPc6	povodeň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
kříž	kříž	k1gInSc1	kříž
s	s	k7c7	s
Kristem	Kristus	k1gMnSc7	Kristus
vydržel	vydržet	k5eAaPmAgMnS	vydržet
<g/>
,	,	kIx,	,
orkán	orkán	k1gInSc1	orkán
Kyrill	Kyrill	k1gMnSc1	Kyrill
jej	on	k3xPp3gMnSc4	on
zlomil	zlomit	k5eAaPmAgMnS	zlomit
a	a	k8xC	a
strhl	strhnout	k5eAaPmAgMnS	strhnout
do	do	k7c2	do
Otavy	Otava	k1gFnSc2	Otava
<g/>
.	.	kIx.	.
</s>
<s>
Potápěčům	potápěč	k1gMnPc3	potápěč
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
Luboši	Luboš	k1gMnSc3	Luboš
Čukovi	Čuka	k1gMnSc3	Čuka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
v	v	k7c4	v
podvečer	podvečer	k1gInSc4	podvečer
podařilo	podařit	k5eAaPmAgNnS	podařit
najít	najít	k5eAaPmF	najít
kříž	kříž	k1gInSc4	kříž
i	i	k9	i
s	s	k7c7	s
80	[number]	k4	80
kg	kg	kA	kg
vážící	vážící	k2eAgFnSc7d1	vážící
cínovou	cínový	k2eAgFnSc7d1	cínová
sochou	socha	k1gFnSc7	socha
v	v	k7c6	v
kalné	kalný	k2eAgFnSc6d1	kalná
vodě	voda	k1gFnSc6	voda
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
šedesát	šedesát	k4xCc4	šedesát
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
mostu	most	k1gInSc2	most
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k7c2	uprostřed
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
hasiči	hasič	k1gMnPc7	hasič
jej	on	k3xPp3gMnSc4	on
vylovili	vylovit	k5eAaPmAgMnP	vylovit
<g/>
.	.	kIx.	.
</s>
<s>
Socha	socha	k1gFnSc1	socha
Krista	Kristus	k1gMnSc2	Kristus
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
nepoškozená	poškozený	k2eNgFnSc1d1	nepoškozená
<g/>
,	,	kIx,	,
jedinou	jediný	k2eAgFnSc7d1	jediná
újmou	újma	k1gFnSc7	újma
jsou	být	k5eAaImIp3nP	být
škrábance	škrábanec	k1gInPc1	škrábanec
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
dřením	dřeň	k1gFnPc3	dřeň
o	o	k7c4	o
dno	dno	k1gNnSc4	dno
<g/>
.	.	kIx.	.
</s>
<s>
Trnová	trnový	k2eAgFnSc1d1	Trnová
koruna	koruna	k1gFnSc1	koruna
byla	být	k5eAaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
ztracenou	ztracený	k2eAgFnSc4d1	ztracená
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
objevena	objeven	k2eAgFnSc1d1	objevena
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nález	nález	k1gInSc1	nález
dokumentů	dokument	k1gInPc2	dokument
v	v	k7c6	v
kříži	kříž	k1gInSc6	kříž
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
restaurátorských	restaurátorský	k2eAgFnPc2d1	restaurátorská
prací	práce	k1gFnPc2	práce
objevil	objevit	k5eAaPmAgMnS	objevit
restaurátor	restaurátor	k1gMnSc1	restaurátor
historické	historický	k2eAgInPc4d1	historický
dokumenty	dokument	k1gInPc4	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
zprávu	zpráva	k1gFnSc4	zpráva
sochaře	sochař	k1gMnSc2	sochař
Karla	Karel	k1gMnSc2	Karel
Vlačihy	vlačiha	k1gFnSc2	vlačiha
o	o	k7c6	o
opravě	oprava	k1gFnSc6	oprava
kříže	kříž	k1gInSc2	kříž
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1897	[number]	k4	1897
<g/>
-	-	kIx~	-
<g/>
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
výtisk	výtisk	k1gInSc1	výtisk
Rudého	rudý	k2eAgNnSc2d1	Rudé
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c4	o
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
(	(	kIx(	(
<g/>
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1957	[number]	k4	1957
<g/>
-	-	kIx~	-
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
a	a	k8xC	a
vzkaz	vzkaz	k1gInSc4	vzkaz
budoucím	budoucí	k2eAgFnPc3d1	budoucí
generacím	generace	k1gFnPc3	generace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Osazení	osazení	k1gNnSc1	osazení
kříže	kříž	k1gInSc2	kříž
na	na	k7c4	na
Kamenný	kamenný	k2eAgInSc4d1	kamenný
most	most	k1gInSc4	most
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2008	[number]	k4	2008
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
osazení	osazení	k1gNnSc3	osazení
kříže	kříž	k1gInSc2	kříž
na	na	k7c4	na
most	most	k1gInSc4	most
<g/>
.	.	kIx.	.
</s>
<s>
Restaurátor	restaurátor	k1gMnSc1	restaurátor
Robert	Robert	k1gMnSc1	Robert
Ritschel	Ritschel	k1gMnSc1	Ritschel
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
kříži	kříž	k1gInSc6	kříž
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
originál	originál	k1gInSc1	originál
uměleckého	umělecký	k2eAgNnSc2d1	umělecké
díla	dílo	k1gNnSc2	dílo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1791	[number]	k4	1791
vystaven	vystavit	k5eAaPmNgInS	vystavit
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
se	s	k7c7	s
všemi	všecek	k3xTgFnPc7	všecek
sochami	socha	k1gFnPc7	socha
z	z	k7c2	z
mostu	most	k1gInSc2	most
<g/>
)	)	kIx)	)
v	v	k7c6	v
expozici	expozice	k1gFnSc6	expozice
Prácheňského	prácheňský	k2eAgNnSc2d1	Prácheňské
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
písecké	písecký	k2eAgFnSc6d1	Písecká
Sladovně	sladovna	k1gFnSc6	sladovna
<g/>
,	,	kIx,	,
most	most	k1gInSc1	most
zdobí	zdobit	k5eAaImIp3nS	zdobit
pouze	pouze	k6eAd1	pouze
jeho	jeho	k3xOp3gFnSc1	jeho
věrná	věrný	k2eAgFnSc1d1	věrná
kopie	kopie	k1gFnSc1	kopie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Karlův	Karlův	k2eAgInSc1d1	Karlův
most	most	k1gInSc1	most
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
–	–	k?	–
druhý	druhý	k4xOgMnSc1	druhý
nejstarší	starý	k2eAgInSc1d3	nejstarší
dochovaný	dochovaný	k2eAgInSc1d1	dochovaný
most	most	k1gInSc1	most
v	v	k7c6	v
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kamenný	kamenný	k2eAgInSc4d1	kamenný
most	most	k1gInSc4	most
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://web.quick.cz/most/	[url]	k?	http://web.quick.cz/most/
</s>
</p>
<p>
<s>
http://www.radio.cz/cz/clanek/56823	[url]	k4	http://www.radio.cz/cz/clanek/56823
</s>
</p>
<p>
<s>
http://pisecky.denik.cz/zpravy_region/pi20080110kamenny_most_kriz.html	[url]	k1gMnSc1	http://pisecky.denik.cz/zpravy_region/pi20080110kamenny_most_kriz.html
</s>
</p>
<p>
<s>
Nábřeží	nábřeží	k1gNnSc1	nábřeží
u	u	k7c2	u
Kamenného	kamenný	k2eAgInSc2d1	kamenný
mostu	most	k1gInSc2	most
-	-	kIx~	-
zanikleobce	zanikleobec	k1gInSc2	zanikleobec
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Webkamera	Webkamera	k1gFnSc1	Webkamera
na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
Restaurace	restaurace	k1gFnSc2	restaurace
u	u	k7c2	u
Plechandy	Plechanda	k1gFnSc2	Plechanda
snímající	snímající	k2eAgInSc1d1	snímající
Kamenný	kamenný	k2eAgInSc1d1	kamenný
most	most	k1gInSc1	most
-	-	kIx~	-
webcam	webcam	k1gInSc1	webcam
<g/>
.	.	kIx.	.
<g/>
kozlovnauplechandy	kozlovnauplechanda	k1gFnPc1	kozlovnauplechanda
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
