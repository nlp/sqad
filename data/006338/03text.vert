<s>
James	James	k1gMnSc1	James
Augustine	Augustin	k1gMnSc5	Augustin
Aloysius	Aloysius	k1gInSc1	Aloysius
Joyce	Joyec	k1gInPc1	Joyec
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1882	[number]	k4	1882
Dublin	Dublin	k1gInSc1	Dublin
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1941	[number]	k4	1941
Curych	Curych	k1gInSc1	Curych
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
irský	irský	k2eAgMnSc1d1	irský
romanopisec	romanopisec	k1gMnSc1	romanopisec
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
dílem	díl	k1gInSc7	díl
náleží	náležet	k5eAaImIp3nP	náležet
mezi	mezi	k7c4	mezi
významné	významný	k2eAgMnPc4d1	významný
autory	autor	k1gMnPc4	autor
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
např.	např.	kA	např.
Marcela	Marcel	k1gMnSc2	Marcel
Prousta	Proust	k1gMnSc2	Proust
<g/>
,	,	kIx,	,
Roberta	Robert	k1gMnSc2	Robert
Musila	Musil	k1gMnSc2	Musil
či	či	k8xC	či
Virginie	Virginie	k1gFnSc2	Virginie
Woolfové	Woolfový	k2eAgFnSc2d1	Woolfová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
díle	dílo	k1gNnSc6	dílo
se	se	k3xPyFc4	se
často	často	k6eAd1	často
objevuje	objevovat	k5eAaImIp3nS	objevovat
motiv	motiv	k1gInSc4	motiv
jeho	on	k3xPp3gNnSc2	on
rodného	rodný	k2eAgNnSc2d1	rodné
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Dublinu	Dublin	k1gInSc6	Dublin
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
dospělého	dospělý	k2eAgInSc2d1	dospělý
života	život	k1gInSc2	život
prožil	prožít	k5eAaPmAgMnS	prožít
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
silným	silný	k2eAgInSc7d1	silný
motivem	motiv	k1gInSc7	motiv
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
díle	díl	k1gInSc6	díl
je	být	k5eAaImIp3nS	být
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
nejzřetelněji	zřetelně	k6eAd3	zřetelně
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
instituci	instituce	k1gFnSc3	instituce
vidět	vidět	k5eAaImF	vidět
v	v	k7c6	v
psychologii	psychologie	k1gFnSc6	psychologie
Štěpána	Štěpán	k1gMnSc2	Štěpán
Dedala	Dedal	k1gMnSc2	Dedal
<g/>
,	,	kIx,	,
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
v	v	k7c6	v
Odysseovi	Odysseus	k1gMnSc6	Odysseus
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
a	a	k8xC	a
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
rodině	rodina	k1gFnSc6	rodina
v	v	k7c6	v
Dublinu	Dublin	k1gInSc6	Dublin
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
šesti	šest	k4xCc2	šest
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
vzděláván	vzdělávat	k5eAaImNgInS	vzdělávat
na	na	k7c6	na
jezuitské	jezuitský	k2eAgFnSc6d1	jezuitská
koleji	kolej	k1gFnSc6	kolej
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
literárních	literární	k2eAgFnPc2d1	literární
inspirací	inspirace	k1gFnPc2	inspirace
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Dublinu	Dublin	k1gInSc6	Dublin
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
promoci	promoce	k1gFnSc6	promoce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zamýšlel	zamýšlet	k5eAaImAgMnS	zamýšlet
studovat	studovat	k5eAaImF	studovat
medicínu	medicína	k1gFnSc4	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Sesbíral	sesbírat	k5eAaPmAgMnS	sesbírat
zde	zde	k6eAd1	zde
mnoho	mnoho	k4c4	mnoho
zkušeností	zkušenost	k1gFnPc2	zkušenost
<g/>
,	,	kIx,	,
když	když	k8xS	když
pracoval	pracovat	k5eAaImAgMnS	pracovat
mj.	mj.	kA	mj.
jako	jako	k8xS	jako
novinář	novinář	k1gMnSc1	novinář
či	či	k8xC	či
učitel	učitel	k1gMnSc1	učitel
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Dublinu	Dublin	k1gInSc2	Dublin
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
pouze	pouze	k6eAd1	pouze
za	za	k7c7	za
svou	svůj	k3xOyFgFnSc7	svůj
umírající	umírající	k2eAgFnSc7d1	umírající
matkou	matka	k1gFnSc7	matka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
smrti	smrt	k1gFnSc6	smrt
znovu	znovu	k6eAd1	znovu
Irsko	Irsko	k1gNnSc4	Irsko
opustil	opustit	k5eAaPmAgMnS	opustit
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
dobrovolného	dobrovolný	k2eAgNnSc2d1	dobrovolné
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
<g/>
.	.	kIx.	.
</s>
<s>
Uchýlil	uchýlit	k5eAaPmAgInS	uchýlit
se	se	k3xPyFc4	se
do	do	k7c2	do
Terstu	Terst	k1gInSc2	Terst
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Rakousku-Uhersku	Rakousku-Uhersko	k1gNnSc6	Rakousku-Uhersko
a	a	k8xC	a
opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgMnS	živit
jako	jako	k9	jako
učitel	učitel	k1gMnSc1	učitel
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
do	do	k7c2	do
Irska	Irsko	k1gNnSc2	Irsko
ještě	ještě	k6eAd1	ještě
několikrát	několikrát	k6eAd1	několikrát
vrátil	vrátit	k5eAaPmAgMnS	vrátit
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
Dublinu	Dublin	k1gInSc6	Dublin
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
provozovat	provozovat	k5eAaImF	provozovat
kino	kino	k1gNnSc4	kino
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vždy	vždy	k6eAd1	vždy
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
čase	čas	k1gInSc6	čas
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
dospělého	dospělý	k2eAgInSc2d1	dospělý
života	život	k1gInSc2	život
tak	tak	k6eAd1	tak
strávil	strávit	k5eAaPmAgMnS	strávit
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kontinentu	kontinent	k1gInSc6	kontinent
cestoval	cestovat	k5eAaImAgMnS	cestovat
mezi	mezi	k7c7	mezi
Paříží	Paříž	k1gFnSc7	Paříž
a	a	k8xC	a
Curychem	Curych	k1gInSc7	Curych
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Irkou	Irka	k1gFnSc7	Irka
Norou	Nora	k1gFnSc7	Nora
Barnacleovou	Barnacleův	k2eAgFnSc7d1	Barnacleův
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgFnSc7d1	pocházející
z	z	k7c2	z
Galwaye	Galway	k1gFnSc2	Galway
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k9	jako
pokojská	pokojská	k1gFnSc1	pokojská
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Finn	Finna	k1gFnPc2	Finna
<g/>
,	,	kIx,	,
opustila	opustit	k5eAaPmAgFnS	opustit
Dublin	Dublin	k1gInSc4	Dublin
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnPc4	jeho
přátele	přítel	k1gMnPc4	přítel
patřili	patřit	k5eAaImAgMnP	patřit
mj.	mj.	kA	mj.
Carl	Carl	k1gMnSc1	Carl
Gustav	Gustav	k1gMnSc1	Gustav
Jung	Jung	k1gMnSc1	Jung
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgMnSc7	který
se	se	k3xPyFc4	se
stýkal	stýkat	k5eAaImAgMnS	stýkat
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
a	a	k8xC	a
který	který	k3yIgInSc1	který
léčil	léčit	k5eAaImAgInS	léčit
Joyceovu	Joyceův	k2eAgFnSc4d1	Joyceova
dceru	dcera	k1gFnSc4	dcera
Lucii	Lucie	k1gFnSc4	Lucie
<g/>
,	,	kIx,	,
či	či	k8xC	či
německy	německy	k6eAd1	německy
píšící	píšící	k2eAgMnSc1d1	píšící
spisovatel	spisovatel	k1gMnSc1	spisovatel
Hermann	Hermann	k1gMnSc1	Hermann
Broch	Broch	k1gMnSc1	Broch
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
přímluvu	přímluva	k1gFnSc4	přímluva
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
emigrovat	emigrovat	k5eAaBmF	emigrovat
z	z	k7c2	z
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Zachránil	zachránit	k5eAaPmAgMnS	zachránit
mu	on	k3xPp3gMnSc3	on
tím	ten	k3xDgNnSc7	ten
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1917	[number]	k4	1917
a	a	k8xC	a
1930	[number]	k4	1930
prodělal	prodělat	k5eAaPmAgInS	prodělat
několik	několik	k4yIc4	několik
očních	oční	k2eAgFnPc2d1	oční
operací	operace	k1gFnPc2	operace
a	a	k8xC	a
na	na	k7c4	na
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
zcela	zcela	k6eAd1	zcela
oslepl	oslepnout	k5eAaPmAgMnS	oslepnout
(	(	kIx(	(
<g/>
nezáměrná	záměrný	k2eNgFnSc1d1	nezáměrná
paralela	paralela	k1gFnSc1	paralela
s	s	k7c7	s
Homérem	Homér	k1gMnSc7	Homér
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
rovněž	rovněž	k9	rovněž
slepý	slepý	k2eAgInSc1d1	slepý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
se	se	k3xPyFc4	se
sv.	sv.	kA	sv.
Tomáši	Tomáš	k1gMnSc5	Tomáš
Akvinskému	Akvinský	k2eAgInSc3d1	Akvinský
a	a	k8xC	a
astronomovi	astronom	k1gMnSc6	astronom
Giordanu	Giordan	k1gMnSc6	Giordan
Brunovi	Bruna	k1gMnSc6	Bruna
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
literární	literární	k2eAgFnSc1d1	literární
tvorba	tvorba	k1gFnSc1	tvorba
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
písemnictví	písemnictví	k1gNnPc2	písemnictví
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
svou	svůj	k3xOyFgFnSc7	svůj
modernistickou	modernistický	k2eAgFnSc7d1	modernistická
experimentální	experimentální	k2eAgFnSc7d1	experimentální
formou	forma	k1gFnSc7	forma
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
román	román	k1gInSc1	román
Odysseus	Odysseus	k1gInSc1	Odysseus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chamber	Chamber	k1gMnSc1	Chamber
music	music	k1gMnSc1	music
<g/>
,	,	kIx,	,
1907	[number]	k4	1907
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Komorní	komorní	k2eAgFnSc1d1	komorní
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
přebásnil	přebásnit	k5eAaPmAgMnS	přebásnit
Petr	Petr	k1gMnSc1	Petr
Mikeš	Mikeš	k1gMnSc1	Mikeš
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Básně	báseň	k1gFnPc1	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Dubliners	Dubliners	k6eAd1	Dubliners
<g/>
,	,	kIx,	,
1914	[number]	k4	1914
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Dubliňané	Dubliňan	k1gMnPc1	Dubliňan
<g/>
,	,	kIx,	,
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Hrůša	Hrůš	k1gInSc2	Hrůš
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Urbánek	Urbánek	k1gMnSc1	Urbánek
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
Aloys	Aloys	k1gInSc4	Aloys
Skoumal	Skoumal	k1gMnSc1	Skoumal
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
a	a	k8xC	a
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Povídky	povídka	k1gFnPc1	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Exiles	Exiles	k1gMnSc1	Exiles
<g/>
,	,	kIx,	,
1915	[number]	k4	1915
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Vyhnanci	vyhnanec	k1gMnPc1	vyhnanec
<g/>
,	,	kIx,	,
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vařecha	vařecha	k1gFnSc1	vařecha
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Drama	drama	k1gNnSc1	drama
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
portrait	portrait	k1gMnSc1	portrait
of	of	k?	of
the	the	k?	the
artist	artist	k1gInSc1	artist
as	as	k1gInSc1	as
a	a	k8xC	a
young	young	k1gMnSc1	young
man	man	k1gMnSc1	man
<g/>
,	,	kIx,	,
1916	[number]	k4	1916
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Portrét	portrét	k1gInSc1	portrét
mladého	mladý	k2eAgMnSc2d1	mladý
umělce	umělec	k1gMnSc2	umělec
<g/>
,	,	kIx,	,
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Staša	Staša	k6eAd1	Staša
Jílovská	jílovský	k2eAgFnSc1d1	Jílovská
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
;	;	kIx,	;
Portrét	portrét	k1gInSc1	portrét
umělce	umělec	k1gMnSc2	umělec
v	v	k7c6	v
jinošských	jinošský	k2eAgNnPc6d1	jinošské
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
Aloys	Aloys	k1gInSc4	Aloys
Skoumal	Skoumal	k1gMnSc1	Skoumal
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
a	a	k8xC	a
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Román	Román	k1gMnSc1	Román
<g/>
.	.	kIx.	.
</s>
<s>
Ulysses	Ulysses	k1gMnSc1	Ulysses
<g/>
,	,	kIx,	,
1922	[number]	k4	1922
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Odysseus	Odysseus	k1gMnSc1	Odysseus
<g/>
,	,	kIx,	,
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Vymětal	Vymětal	k1gMnSc1	Vymětal
a	a	k8xC	a
Jarmila	Jarmila	k1gFnSc1	Jarmila
Fastrová	Fastrová	k1gFnSc1	Fastrová
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
;	;	kIx,	;
přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Aloys	Aloys	k6eAd1	Aloys
Skoumal	Skoumal	k1gMnSc1	Skoumal
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Román	Román	k1gMnSc1	Román
<g/>
.	.	kIx.	.
</s>
<s>
Pomes	Pomes	k1gMnSc1	Pomes
penyeach	penyeach	k1gMnSc1	penyeach
<g/>
,	,	kIx,	,
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
Básně	báseň	k1gFnPc1	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Finnegans	Finnegans	k1gInSc1	Finnegans
Wake	Wak	k1gFnSc2	Wak
<g/>
,	,	kIx,	,
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Román	Román	k1gMnSc1	Román
<g/>
.	.	kIx.	.
</s>
<s>
Aloys	Aloys	k6eAd1	Aloys
Skoumal	Skoumal	k1gMnSc1	Skoumal
Bloomsday	Bloomsdaa	k1gFnSc2	Bloomsdaa
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
James	Jamesa	k1gFnPc2	Jamesa
Joyce	Joyce	k1gFnSc2	Joyce
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
James	James	k1gInSc1	James
Joyce	Joyce	k1gFnSc2	Joyce
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Adolf	Adolf	k1gMnSc1	Adolf
Hoffmeister	Hoffmeister	k1gMnSc1	Hoffmeister
<g/>
:	:	kIx,	:
Osobnost	osobnost	k1gFnSc1	osobnost
James	James	k1gMnSc1	James
Joyce	Joyce	k1gMnSc1	Joyce
–	–	k?	–
rozhovor	rozhovor	k1gInSc1	rozhovor
Adolfa	Adolf	k1gMnSc2	Adolf
Hoffmeistera	Hoffmeister	k1gMnSc2	Hoffmeister
s	s	k7c7	s
Jamesem	James	k1gMnSc7	James
Joycem	Joyec	k1gInSc7	Joyec
<g/>
,	,	kIx,	,
časopis	časopis	k1gInSc1	časopis
Souvislosti	souvislost	k1gFnSc2	souvislost
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
2002	[number]	k4	2002
Martin	Martin	k1gMnSc1	Martin
Pokorný	Pokorný	k1gMnSc1	Pokorný
<g/>
,	,	kIx,	,
Odezvy	odezva	k1gFnPc1	odezva
a	a	k8xC	a
znaky	znak	k1gInPc1	znak
<g/>
:	:	kIx,	:
Homér	Homér	k1gMnSc1	Homér
<g/>
,	,	kIx,	,
Dante	Dante	k1gMnSc1	Dante
a	a	k8xC	a
Joyceův	Joyceův	k2eAgMnSc1d1	Joyceův
Odysseus	Odysseus	k1gMnSc1	Odysseus
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jitro	jitro	k1gNnSc1	jitro
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
