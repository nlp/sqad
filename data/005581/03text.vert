<s>
Biologická	biologický	k2eAgFnSc1d1	biologická
klasifikace	klasifikace	k1gFnSc1	klasifikace
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgNnSc7	jaký
biologové	biolog	k1gMnPc1	biolog
kategorizují	kategorizovat	k5eAaBmIp3nP	kategorizovat
existující	existující	k2eAgInPc4d1	existující
a	a	k8xC	a
vymřelé	vymřelý	k2eAgInPc4d1	vymřelý
druhy	druh	k1gInPc4	druh
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
způsob	způsob	k1gInSc1	způsob
třídění	třídění	k1gNnSc2	třídění
organismů	organismus	k1gInPc2	organismus
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
skupin	skupina	k1gFnPc2	skupina
do	do	k7c2	do
hierarchického	hierarchický	k2eAgInSc2d1	hierarchický
klasifikačního	klasifikační	k2eAgInSc2d1	klasifikační
systému	systém	k1gInSc2	systém
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
taxonomické	taxonomický	k2eAgNnSc1d1	taxonomické
zařazení	zařazení	k1gNnSc1	zařazení
<g/>
.	.	kIx.	.
</s>
<s>
Klasifikují	klasifikovat	k5eAaImIp3nP	klasifikovat
se	se	k3xPyFc4	se
existující	existující	k2eAgInPc1d1	existující
i	i	k8xC	i
vymřelé	vymřelý	k2eAgInPc1d1	vymřelý
druhy	druh	k1gInPc1	druh
organismů	organismus	k1gInPc2	organismus
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Biologická	biologický	k2eAgFnSc1d1	biologická
klasifikace	klasifikace	k1gFnSc1	klasifikace
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
postavena	postavit	k5eAaPmNgNnP	postavit
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
principech	princip	k1gInPc6	princip
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nalézt	nalézt	k5eAaPmF	nalézt
více	hodně	k6eAd2	hodně
biologických	biologický	k2eAgFnPc2d1	biologická
klasifikací	klasifikace	k1gFnPc2	klasifikace
a	a	k8xC	a
jim	on	k3xPp3gMnPc3	on
odpovídajících	odpovídající	k2eAgNnPc2d1	odpovídající
taxonomických	taxonomický	k2eAgNnPc2d1	taxonomické
zařazení	zařazení	k1gNnPc2	zařazení
<g/>
.	.	kIx.	.
</s>
<s>
Biologickou	biologický	k2eAgFnSc7d1	biologická
klasifikací	klasifikace	k1gFnSc7	klasifikace
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
systematická	systematický	k2eAgFnSc1d1	systematická
biologie	biologie	k1gFnSc1	biologie
<g/>
,	,	kIx,	,
fylogenetika	fylogenetika	k1gFnSc1	fylogenetika
a	a	k8xC	a
kladistika	kladistika	k1gFnSc1	kladistika
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
klasifikace	klasifikace	k1gFnSc1	klasifikace
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
kořeny	kořen	k1gInPc4	kořen
v	v	k7c6	v
systému	systém	k1gInSc6	systém
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
švédský	švédský	k2eAgMnSc1d1	švédský
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Carl	Carl	k1gMnSc1	Carl
Linné	Linné	k2eAgMnSc1d1	Linné
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
tehdy	tehdy	k6eAd1	tehdy
známé	známý	k2eAgInPc4d1	známý
druhy	druh	k1gInPc4	druh
organismů	organismus	k1gInPc2	organismus
do	do	k7c2	do
skupin	skupina	k1gFnPc2	skupina
podle	podle	k7c2	podle
fyzických	fyzický	k2eAgFnPc2d1	fyzická
charakteristik	charakteristika	k1gFnPc2	charakteristika
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
postupným	postupný	k2eAgNnSc7d1	postupné
prohlubováním	prohlubování	k1gNnSc7	prohlubování
znalostí	znalost	k1gFnPc2	znalost
o	o	k7c6	o
organismech	organismus	k1gInPc6	organismus
a	a	k8xC	a
principech	princip	k1gInPc6	princip
jejich	jejich	k3xOp3gInSc2	jejich
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
druhu	druh	k1gInSc2	druh
či	či	k8xC	či
jedince	jedinec	k1gMnSc2	jedinec
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
také	také	k9	také
způsob	způsob	k1gInSc1	způsob
taxonomického	taxonomický	k2eAgNnSc2d1	taxonomické
řazení	řazení	k1gNnSc2	řazení
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
biologické	biologický	k2eAgInPc1d1	biologický
klasifikační	klasifikační	k2eAgInPc1d1	klasifikační
systémy	systém	k1gInPc1	systém
procházejí	procházet	k5eAaImIp3nP	procházet
neustálým	neustálý	k2eAgInSc7d1	neustálý
vývojem	vývoj	k1gInSc7	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
prosté	prostý	k2eAgFnSc2d1	prostá
morfologické	morfologický	k2eAgFnSc2d1	morfologická
podobnosti	podobnost	k1gFnSc2	podobnost
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
prohlubujících	prohlubující	k2eAgInPc2d1	prohlubující
se	s	k7c7	s
znalostí	znalost	k1gFnSc7	znalost
postupně	postupně	k6eAd1	postupně
přechází	přecházet	k5eAaImIp3nS	přecházet
ke	k	k7c3	k
klasifikaci	klasifikace	k1gFnSc3	klasifikace
přirozené	přirozený	k2eAgFnSc3d1	přirozená
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
podle	podle	k7c2	podle
skutečné	skutečný	k2eAgFnSc2d1	skutečná
fylogenetické	fylogenetický	k2eAgFnSc2d1	fylogenetická
příbuznosti	příbuznost	k1gFnSc2	příbuznost
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
třídění	třídění	k1gNnSc2	třídění
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
morfometrických	morfometrický	k2eAgFnPc6d1	morfometrická
a	a	k8xC	a
numerických	numerický	k2eAgFnPc6d1	numerická
metodách	metoda	k1gFnPc6	metoda
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
výsledky	výsledek	k1gInPc1	výsledek
jsou	být	k5eAaImIp3nP	být
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k9	jako
fenetické	fenetický	k2eAgFnPc1d1	fenetický
klasifikace	klasifikace	k1gFnPc1	klasifikace
<g/>
.	.	kIx.	.
</s>
<s>
Organismy	organismus	k1gInPc1	organismus
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
skupiny	skupina	k1gFnPc1	skupina
jsou	být	k5eAaImIp3nP	být
tříděny	třídit	k5eAaImNgFnP	třídit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
fyzických	fyzický	k2eAgFnPc2d1	fyzická
vlastností	vlastnost	k1gFnPc2	vlastnost
a	a	k8xC	a
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
stejného	stejný	k2eAgInSc2d1	stejný
taxonu	taxon	k1gInSc2	taxon
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
mohou	moct	k5eAaImIp3nP	moct
dostat	dostat	k5eAaPmF	dostat
i	i	k9	i
organismy	organismus	k1gInPc1	organismus
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
příbuzné	příbuzný	k1gMnPc4	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
kladistická	kladistický	k2eAgFnSc1d1	kladistická
klasifikace	klasifikace	k1gFnSc1	klasifikace
(	(	kIx(	(
<g/>
fylogenetická	fylogenetický	k2eAgFnSc1d1	fylogenetická
klasifikace	klasifikace	k1gFnSc1	klasifikace
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
principech	princip	k1gInPc6	princip
odrážejících	odrážející	k2eAgFnPc2d1	odrážející
příbuznost	příbuznost	k1gFnSc1	příbuznost
organismů	organismus	k1gInPc2	organismus
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
evoluční	evoluční	k2eAgFnPc1d1	evoluční
klasifikace	klasifikace	k1gFnPc1	klasifikace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
postaveny	postavit	k5eAaPmNgInP	postavit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výsledků	výsledek	k1gInPc2	výsledek
fenetické	fenetický	k2eAgFnSc2d1	fenetický
a	a	k8xC	a
kladistické	kladistický	k2eAgFnSc2d1	kladistická
klasifikace	klasifikace	k1gFnSc2	klasifikace
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
klasifikační	klasifikační	k2eAgFnSc7d1	klasifikační
úrovní	úroveň	k1gFnSc7	úroveň
buněčných	buněčný	k2eAgInPc2d1	buněčný
organismů	organismus	k1gInPc2	organismus
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
biologii	biologie	k1gFnSc6	biologie
zpravidla	zpravidla	k6eAd1	zpravidla
tři	tři	k4xCgFnPc4	tři
tzv.	tzv.	kA	tzv.
domény	doména	k1gFnPc1	doména
<g/>
:	:	kIx,	:
archea	arche	k2eAgFnSc1d1	arche
(	(	kIx(	(
<g/>
Archaea	Archaea	k1gFnSc1	Archaea
<g/>
,	,	kIx,	,
zast.	zast.	k?	zast.
Archaebacteria	Archaebacterium	k1gNnSc2	Archaebacterium
<g/>
)	)	kIx)	)
bakterie	bakterie	k1gFnSc2	bakterie
(	(	kIx(	(
<g/>
Bacteria	Bacterium	k1gNnSc2	Bacterium
<g/>
,	,	kIx,	,
zast.	zast.	k?	zast.
Eubacteria	Eubacterium	k1gNnSc2	Eubacterium
<g/>
)	)	kIx)	)
a	a	k8xC	a
eukaryota	eukaryota	k1gFnSc1	eukaryota
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
jaderní	jaderní	k2eAgFnSc1d1	jaderní
(	(	kIx(	(
<g/>
Eukaryota	Eukaryota	k1gFnSc1	Eukaryota
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Eukarya	Eukarya	k1gFnSc1	Eukarya
<g/>
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Doména	doména	k1gFnSc1	doména
(	(	kIx(	(
<g/>
biologie	biologie	k1gFnSc1	biologie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
hlavní	hlavní	k2eAgFnPc4d1	hlavní
vývojové	vývojový	k2eAgFnPc4d1	vývojová
větve	větev	k1gFnPc4	větev
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
na	na	k7c6	na
stávající	stávající	k2eAgFnSc6d1	stávající
úrovni	úroveň	k1gFnSc6	úroveň
poznání	poznání	k1gNnSc2	poznání
nedokážou	dokázat	k5eNaPmIp3nP	dokázat
biologové	biolog	k1gMnPc1	biolog
s	s	k7c7	s
dostatečnou	dostatečný	k2eAgFnSc7d1	dostatečná
věrohodností	věrohodnost	k1gFnSc7	věrohodnost
prokázat	prokázat	k5eAaPmF	prokázat
jejich	jejich	k3xOp3gFnSc4	jejich
přirozenost	přirozenost	k1gFnSc4	přirozenost
–	–	k?	–
jednak	jednak	k8xC	jednak
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
úplně	úplně	k6eAd1	úplně
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
vypadal	vypadat	k5eAaImAgInS	vypadat
společný	společný	k2eAgInSc4d1	společný
předek	předek	k1gInSc4	předek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
domén	doména	k1gFnPc2	doména
mohla	moct	k5eAaImAgFnS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
až	až	k9	až
vývojem	vývoj	k1gInSc7	vývoj
uvnitř	uvnitř	k7c2	uvnitř
jiné	jiná	k1gFnSc2	jiná
(	(	kIx(	(
<g/>
parafyletismus	parafyletismus	k1gInSc1	parafyletismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
např.	např.	kA	např.
mnohé	mnohý	k2eAgFnPc4d1	mnohá
studie	studie	k1gFnPc4	studie
<g/>
,	,	kIx,	,
kladoucí	kladoucí	k2eAgInPc1d1	kladoucí
jaderné	jaderný	k2eAgInPc1d1	jaderný
organismy	organismus	k1gInPc1	organismus
dovnitř	dovnitř	k6eAd1	dovnitř
archeí	archeí	k6eAd1	archeí
<g/>
;	;	kIx,	;
naopak	naopak	k6eAd1	naopak
hypotéza	hypotéza	k1gFnSc1	hypotéza
společného	společný	k2eAgInSc2d1	společný
evolučního	evoluční	k2eAgInSc2d1	evoluční
vývoje	vývoj	k1gInSc2	vývoj
archeí	arche	k1gFnPc2	arche
a	a	k8xC	a
bakterií	bakterie	k1gFnPc2	bakterie
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
prokaryota	prokaryota	k1gFnSc1	prokaryota
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
věrohodností	věrohodnost	k1gFnSc7	věrohodnost
vyvrácena	vyvrátit	k5eAaPmNgFnS	vyvrátit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
výše	vysoce	k6eAd2	vysoce
uvedeným	uvedený	k2eAgFnPc3d1	uvedená
doménám	doména	k1gFnPc3	doména
bývá	bývat	k5eAaImIp3nS	bývat
řazena	řadit	k5eAaImNgFnS	řadit
ještě	ještě	k9	ještě
jedna	jeden	k4xCgFnSc1	jeden
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
nebuněčné	buněčný	k2eNgInPc1d1	nebuněčný
organismy	organismus	k1gInPc1	organismus
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
patří	patřit	k5eAaImIp3nP	patřit
viry	vir	k1gInPc1	vir
<g/>
,	,	kIx,	,
viroidy	viroida	k1gFnPc1	viroida
a	a	k8xC	a
podobné	podobný	k2eAgFnPc1d1	podobná
soustavy	soustava	k1gFnPc1	soustava
<g/>
,	,	kIx,	,
vykazující	vykazující	k2eAgFnPc1d1	vykazující
charakteristiky	charakteristika	k1gFnPc1	charakteristika
života	život	k1gInSc2	život
pouze	pouze	k6eAd1	pouze
společně	společně	k6eAd1	společně
s	s	k7c7	s
nějakým	nějaký	k3yIgInSc7	nějaký
buněčným	buněčný	k2eAgInSc7d1	buněčný
organismem	organismus	k1gInSc7	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
neexistuje	existovat	k5eNaImIp3nS	existovat
jednotný	jednotný	k2eAgInSc4d1	jednotný
fylogenetický	fylogenetický	k2eAgInSc4d1	fylogenetický
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
vznik	vznik	k1gInSc4	vznik
virů	vir	k1gInPc2	vir
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
jejich	jejich	k3xOp3gFnPc1	jejich
skupiny	skupina	k1gFnPc1	skupina
jsou	být	k5eAaImIp3nP	být
pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
vymřelých	vymřelý	k2eAgFnPc2d1	vymřelá
dalších	další	k2eAgFnPc2d1	další
domén	doména	k1gFnPc2	doména
buněčných	buněčný	k2eAgInPc2d1	buněčný
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
překvapení	překvapení	k1gNnSc1	překvapení
zasahující	zasahující	k2eAgFnSc2d1	zasahující
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
klasifikační	klasifikační	k2eAgFnSc2d1	klasifikační
úrovně	úroveň	k1gFnSc2	úroveň
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
přinést	přinést	k5eAaPmF	přinést
rozvíjející	rozvíjející	k2eAgFnPc4d1	rozvíjející
se	se	k3xPyFc4	se
exobiologie	exobiologie	k1gFnPc4	exobiologie
<g/>
,	,	kIx,	,
podaří	podařit	k5eAaPmIp3nS	podařit
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
nalézt	nalézt	k5eAaPmF	nalézt
mimozemský	mimozemský	k2eAgInSc4d1	mimozemský
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
či	či	k8xC	či
jeho	jeho	k3xOp3gInPc1	jeho
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
<g/>
.	.	kIx.	.
</s>
<s>
Klasifikace	klasifikace	k1gFnSc1	klasifikace
eukaryot	eukaryota	k1gFnPc2	eukaryota
Klasifikace	klasifikace	k1gFnSc2	klasifikace
hub	houba	k1gFnPc2	houba
Klasifikace	klasifikace	k1gFnSc2	klasifikace
rostlin	rostlina	k1gFnPc2	rostlina
Systém	systém	k1gInSc1	systém
APG	APG	kA	APG
III	III	kA	III
Klasifikace	klasifikace	k1gFnSc1	klasifikace
živočichů	živočich	k1gMnPc2	živočich
Klasifikace	klasifikace	k1gFnSc2	klasifikace
strunatců	strunatec	k1gMnPc2	strunatec
Taxonomie	taxonomie	k1gFnSc2	taxonomie
plžů	plž	k1gMnPc2	plž
(	(	kIx(	(
<g/>
Bouchet	Bouchet	k1gMnSc1	Bouchet
&	&	k?	&
Rocroi	Rocro	k1gFnSc2	Rocro
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Klasifikace	klasifikace	k1gFnSc1	klasifikace
virů	vir	k1gInPc2	vir
</s>
