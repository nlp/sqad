<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Meteorologie	meteorologie	k1gFnSc2	meteorologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
<s>
Meteorologie	meteorologie	k1gFnSc1	meteorologie
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Studuje	studovat	k5eAaImIp3nS	studovat
její	její	k3xOp3gNnSc4	její
složení	složení	k1gNnSc4	složení
<g/>
,	,	kIx,	,
stavbu	stavba	k1gFnSc4	stavba
<g/>
,	,	kIx,	,
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
jevy	jev	k1gInPc4	jev
a	a	k8xC	a
děje	dít	k5eAaImIp3nS	dít
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
probíhající	probíhající	k2eAgFnSc6d1	probíhající
<g/>
.	.	kIx.	.
</s>
</p>
