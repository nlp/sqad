<s>
Raetie	Raetie	k1gFnSc1	Raetie
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
latinsky	latinsky	k6eAd1	latinsky
Raetia	Raetia	k1gFnSc1	Raetia
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
provincie	provincie	k1gFnPc4	provincie
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
sahalo	sahat	k5eAaImAgNnS	sahat
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
Bodamského	bodamský	k2eAgNnSc2d1	Bodamské
jezera	jezero	k1gNnSc2	jezero
(	(	kIx(	(
<g/>
Lacus	Lacus	k1gInSc1	Lacus
Brigantinus	Brigantinus	k1gInSc1	Brigantinus
<g/>
)	)	kIx)	)
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Inn	Inn	k1gFnSc2	Inn
(	(	kIx(	(
<g/>
Aenus	Aenus	k1gInSc1	Aenus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc4d1	severní
hranici	hranice	k1gFnSc4	hranice
provincie	provincie	k1gFnSc2	provincie
tvořila	tvořit	k5eAaImAgFnS	tvořit
řeka	řeka	k1gFnSc1	řeka
Dunaj	Dunaj	k1gInSc1	Dunaj
(	(	kIx(	(
<g/>
Danuvius	Danuvius	k1gInSc1	Danuvius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c2	za
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
rozkládalo	rozkládat	k5eAaImAgNnS	rozkládat
neklidné	klidný	k2eNgNnSc1d1	neklidné
území	území	k1gNnSc1	území
"	"	kIx"	"
<g/>
svobodné	svobodný	k2eAgFnPc1d1	svobodná
Germánie	Germánie	k1gFnPc1	Germánie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Penninské	Penninský	k2eAgFnPc1d1	Penninský
Alpy	Alpy	k1gFnPc1	Alpy
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
oddělovaly	oddělovat	k5eAaImAgInP	oddělovat
Raetii	Raetie	k1gFnSc4	Raetie
od	od	k7c2	od
Horní	horní	k2eAgFnSc2d1	horní
Germánie	Germánie	k1gFnSc2	Germánie
a	a	k8xC	a
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
východě	východ	k1gInSc6	východ
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
Noricum	Noricum	k1gNnSc1	Noricum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
střední	střední	k2eAgNnSc1d1	střední
a	a	k8xC	a
východní	východní	k2eAgNnSc1d1	východní
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgNnSc1d1	jižní
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
<g/>
,	,	kIx,	,
jihovýchod	jihovýchod	k1gInSc1	jihovýchod
Bádenska-Württemberska	Bádenska-Württembersk	k1gInSc2	Bádenska-Württembersk
<g/>
,	,	kIx,	,
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc4	Lichtenštejnsko
<g/>
,	,	kIx,	,
Vorarlbersko	Vorarlbersko	k1gNnSc4	Vorarlbersko
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
jeho	jeho	k3xOp3gFnSc2	jeho
italské	italský	k2eAgFnSc2d1	italská
části	část	k1gFnSc2	část
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Raety	Raeta	k1gFnPc1	Raeta
jako	jako	k8xS	jako
nejmocnější	mocný	k2eAgInSc1d3	nejmocnější
alpský	alpský	k2eAgInSc1d1	alpský
kmen	kmen	k1gInSc1	kmen
poprvé	poprvé	k6eAd1	poprvé
připomínal	připomínat	k5eAaImAgInS	připomínat
Polybios	Polybios	k1gInSc1	Polybios
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
až	až	k9	až
do	do	k7c2	do
zániku	zánik	k1gInSc2	zánik
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
nich	on	k3xPp3gFnPc6	on
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
v	v	k7c6	v
dílech	díl	k1gInPc6	díl
římských	římský	k2eAgMnPc2d1	římský
historiků	historik	k1gMnPc2	historik
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
sporadicky	sporadicky	k6eAd1	sporadicky
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
historii	historie	k1gFnSc4	historie
Raetů	Raet	k1gInPc2	Raet
toho	ten	k3xDgInSc2	ten
tudíž	tudíž	k8xC	tudíž
není	být	k5eNaImIp3nS	být
mnoho	mnoho	k6eAd1	mnoho
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Livius	Livius	k1gInSc1	Livius
byl	být	k5eAaImAgInS	být
přesvědčen	přesvědčit	k5eAaPmNgInS	přesvědčit
o	o	k7c6	o
jejich	jejich	k3xOp3gInSc6	jejich
etruském	etruský	k2eAgInSc6d1	etruský
původu	původ	k1gInSc6	původ
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
domněnku	domněnka	k1gFnSc4	domněnka
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
sdíleli	sdílet	k5eAaImAgMnP	sdílet
také	také	k9	také
historikové	historik	k1gMnPc1	historik
Niebuhr	Niebuhr	k1gMnSc1	Niebuhr
a	a	k8xC	a
Mommsen	Mommsen	k2eAgMnSc1d1	Mommsen
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tradičního	tradiční	k2eAgInSc2d1	tradiční
výkladu	výklad	k1gInSc2	výklad
zaznamenaného	zaznamenaný	k2eAgInSc2d1	zaznamenaný
Justinem	Justin	k1gMnSc7	Justin
a	a	k8xC	a
Pliniem	Plinium	k1gNnSc7	Plinium
starším	starý	k2eAgNnSc7d2	starší
představovali	představovat	k5eAaImAgMnP	představovat
Raetové	Raetové	k2eAgFnSc4d1	Raetové
část	část	k1gFnSc4	část
Etrusků	Etrusk	k1gMnPc2	Etrusk
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sídlila	sídlit	k5eAaImAgFnS	sídlit
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
Pádu	Pád	k1gInSc2	Pád
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
byla	být	k5eAaImAgFnS	být
ale	ale	k9	ale
vypuzena	vypudit	k5eAaPmNgFnS	vypudit
Kelty	Kelt	k1gMnPc7	Kelt
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
5	[number]	k4	5
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Označení	označení	k1gNnSc4	označení
Raetové	Raetová	k1gFnSc2	Raetová
prý	prý	k9	prý
přijali	přijmout	k5eAaPmAgMnP	přijmout
podle	podle	k7c2	podle
svého	svůj	k3xOyFgMnSc2	svůj
náčelníka	náčelník	k1gMnSc2	náčelník
<g/>
,	,	kIx,	,
jménem	jméno	k1gNnSc7	jméno
Raetus	Raetus	k1gMnSc1	Raetus
<g/>
.	.	kIx.	.
</s>
<s>
Bližší	blízký	k2eAgFnSc3d2	bližší
realitě	realita	k1gFnSc3	realita
však	však	k9	však
zřejmě	zřejmě	k6eAd1	zřejmě
bude	být	k5eAaImBp3nS	být
hypotéza	hypotéza	k1gFnSc1	hypotéza
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
byl	být	k5eAaImAgInS	být
název	název	k1gInSc1	název
tohoto	tento	k3xDgInSc2	tento
kmene	kmen	k1gInSc2	kmen
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
keltského	keltský	k2eAgInSc2d1	keltský
výrazu	výraz	k1gInSc2	výraz
rait	rait	k5eAaPmF	rait
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
horská	horský	k2eAgFnSc1d1	horská
země	země	k1gFnSc1	země
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
přijmeme	přijmout	k5eAaPmIp1nP	přijmout
teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
etruském	etruský	k2eAgInSc6d1	etruský
původu	původ	k1gInSc6	původ
Raetů	Raet	k1gInPc2	Raet
<g/>
,	,	kIx,	,
skutečností	skutečnost	k1gFnSc7	skutečnost
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Římané	Říman	k1gMnPc1	Říman
navázali	navázat	k5eAaPmAgMnP	navázat
první	první	k4xOgInPc4	první
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
obyvateli	obyvatel	k1gMnPc7	obyvatel
Raetie	Raetie	k1gFnSc2	Raetie
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
země	země	k1gFnSc1	země
plně	plně	k6eAd1	plně
v	v	k7c6	v
moci	moc	k1gFnSc6	moc
keltských	keltský	k2eAgInPc2d1	keltský
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgFnSc1d1	předchozí
populace	populace	k1gFnSc1	populace
zcela	zcela	k6eAd1	zcela
splynula	splynout	k5eAaPmAgFnS	splynout
s	s	k7c7	s
Kelty	Kelt	k1gMnPc7	Kelt
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Raety	Raet	k1gInPc1	Raet
lze	lze	k6eAd1	lze
v	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
dobách	doba	k1gFnPc6	doba
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
ryze	ryze	k6eAd1	ryze
keltský	keltský	k2eAgInSc4d1	keltský
národ	národ	k1gInSc4	národ
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
žily	žít	k5eAaImAgFnP	žít
také	také	k9	také
některé	některý	k3yIgInPc1	některý
cizí	cizí	k2eAgInPc1d1	cizí
kmeny	kmen	k1gInPc1	kmen
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Lepontiové	Lepontiový	k2eAgNnSc1d1	Lepontiový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Raetové	Raet	k1gMnPc1	Raet
si	se	k3xPyFc3	se
udržovali	udržovat	k5eAaImAgMnP	udržovat
nezávislost	nezávislost	k1gFnSc4	nezávislost
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
15	[number]	k4	15
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byli	být	k5eAaImAgMnP	být
poraženi	poražen	k2eAgMnPc1d1	poražen
římským	římský	k2eAgNnSc7d1	římské
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
proniklo	proniknout	k5eAaPmAgNnS	proniknout
na	na	k7c4	na
sever	sever	k1gInSc4	sever
skrze	skrze	k?	skrze
Brennerský	Brennerský	k2eAgInSc4d1	Brennerský
průsmyk	průsmyk	k1gInSc4	průsmyk
<g/>
.	.	kIx.	.
</s>
<s>
Velitelem	velitel	k1gMnSc7	velitel
Římanů	Říman	k1gMnPc2	Říman
byl	být	k5eAaImAgMnS	být
Augustův	Augustův	k2eAgMnSc1d1	Augustův
nevlastní	vlastní	k2eNgMnSc1d1	nevlastní
syn	syn	k1gMnSc1	syn
Drusus	Drusus	k1gMnSc1	Drusus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
si	se	k3xPyFc3	se
Drusův	Drusův	k2eAgMnSc1d1	Drusův
bratr	bratr	k1gMnSc1	bratr
a	a	k8xC	a
pozdější	pozdní	k2eAgMnSc1d2	pozdější
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
Tiberius	Tiberius	k1gMnSc1	Tiberius
podrobil	podrobit	k5eAaPmAgMnS	podrobit
údolí	údolí	k1gNnSc4	údolí
horního	horní	k2eAgInSc2d1	horní
Rýna	Rýn	k1gInSc2	Rýn
a	a	k8xC	a
oblast	oblast	k1gFnSc4	oblast
Bodamského	bodamský	k2eAgNnSc2d1	Bodamské
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
nějž	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
rozkládala	rozkládat	k5eAaImAgFnS	rozkládat
sídla	sídlo	k1gNnSc2	sídlo
Vindeliků	Vindelik	k1gInPc2	Vindelik
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
kmen	kmen	k1gInSc4	kmen
přemohli	přemoct	k5eAaPmAgMnP	přemoct
Římané	Říman	k1gMnPc1	Říman
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
jezeře	jezero	k1gNnSc6	jezero
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
dnešního	dnešní	k2eAgInSc2d1	dnešní
ostrova	ostrov	k1gInSc2	ostrov
Reichenau	Reichenaus	k1gInSc2	Reichenaus
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
pak	pak	k6eAd1	pak
vítězům	vítěz	k1gMnPc3	vítěz
posloužil	posloužit	k5eAaPmAgMnS	posloužit
jako	jako	k8xC	jako
základna	základna	k1gFnSc1	základna
k	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
výbojům	výboj	k1gInPc3	výboj
do	do	k7c2	do
okolních	okolní	k2eAgMnPc2d1	okolní
krajů	kraj	k1gInPc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
bylo	být	k5eAaImAgNnS	být
celé	celý	k2eAgNnSc4d1	celé
území	území	k1gNnSc4	území
mezi	mezi	k7c7	mezi
řekami	řeka	k1gFnPc7	řeka
Dunajem	Dunaj	k1gInSc7	Dunaj
a	a	k8xC	a
Innem	Inn	k1gInSc7	Inn
začleněno	začlenit	k5eAaPmNgNnS	začlenit
do	do	k7c2	do
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
římské	římský	k2eAgFnSc2d1	římská
provincie	provincie	k1gFnSc2	provincie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
zpočátku	zpočátku	k6eAd1	zpočátku
spravovaná	spravovaný	k2eAgFnSc1d1	spravovaná
vojenským	vojenský	k2eAgMnSc7d1	vojenský
prefektem	prefekt	k1gMnSc7	prefekt
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
prokurátorem	prokurátor	k1gMnSc7	prokurátor
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
jezdeckého	jezdecký	k2eAgInSc2d1	jezdecký
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
komunikační	komunikační	k2eAgFnSc7d1	komunikační
trasou	trasa	k1gFnSc7	trasa
byla	být	k5eAaImAgFnS	být
silnice	silnice	k1gFnSc1	silnice
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
přes	přes	k7c4	přes
Brennerský	Brennerský	k2eAgInSc4d1	Brennerský
průsmyk	průsmyk	k1gInSc4	průsmyk
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
spojovala	spojovat	k5eAaImAgNnP	spojovat
dnešní	dnešní	k2eAgNnPc1d1	dnešní
města	město	k1gNnPc1	město
Veronu	Verona	k1gFnSc4	Verona
a	a	k8xC	a
Augsburg	Augsburg	k1gInSc4	Augsburg
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
raetských	raetský	k2eAgNnPc2d1	raetský
údolí	údolí	k1gNnPc2	údolí
bylo	být	k5eAaImAgNnS	být
poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
romanizováno	romanizován	k2eAgNnSc1d1	romanizován
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
důležitá	důležitý	k2eAgNnPc4d1	důležité
města	město	k1gNnPc4	město
Raetie	Raetie	k1gFnSc2	Raetie
náležela	náležet	k5eAaImAgFnS	náležet
Augusta	Augusta	k1gMnSc1	Augusta
Vindelicorum	Vindelicorum	k1gInSc1	Vindelicorum
(	(	kIx(	(
<g/>
Augsburg	Augsburg	k1gInSc1	Augsburg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Brigantium	Brigantium	k1gNnSc1	Brigantium
(	(	kIx(	(
<g/>
Bregenz	Bregenz	k1gInSc1	Bregenz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Castra	Castra	k1gFnSc1	Castra
Batavorum	Batavorum	k1gInSc1	Batavorum
(	(	kIx(	(
<g/>
Pasov	Pasov	k1gInSc1	Pasov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Castra	Castra	k1gFnSc1	Castra
Regina	Regina	k1gFnSc1	Regina
(	(	kIx(	(
<g/>
Řezno	Řezno	k1gNnSc1	Řezno
<g/>
)	)	kIx)	)
a	a	k8xC	a
Tridentum	Tridentum	k1gNnSc1	Tridentum
(	(	kIx(	(
<g/>
Trento	Trento	k1gNnSc1	Trento
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
před	před	k7c7	před
vpády	vpád	k1gInPc7	vpád
germánských	germánský	k2eAgInPc2d1	germánský
kmenů	kmen	k1gInPc2	kmen
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zbudován	zbudován	k2eAgMnSc1d1	zbudován
Limes	Limes	k1gMnSc1	Limes
Germanicus	Germanicus	k1gMnSc1	Germanicus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
166	[number]	k4	166
kilometrů	kilometr	k1gInPc2	kilometr
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
raetská	raetský	k2eAgFnSc1d1	raetský
část	část	k1gFnSc1	část
byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
silnou	silný	k2eAgFnSc7d1	silná
kamennou	kamenný	k2eAgFnSc7d1	kamenná
hradbou	hradba	k1gFnSc7	hradba
doplněnou	doplněný	k2eAgFnSc7d1	doplněná
strážními	strážní	k2eAgFnPc7d1	strážní
věžemi	věž	k1gFnPc7	věž
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
si	se	k3xPyFc3	se
Římané	Říman	k1gMnPc1	Říman
podmanili	podmanit	k5eAaPmAgMnP	podmanit
Agri	Agre	k1gFnSc4	Agre
Decumates	Decumatesa	k1gFnPc2	Decumatesa
(	(	kIx(	(
<g/>
území	území	k1gNnSc1	území
mezi	mezi	k7c7	mezi
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
,	,	kIx,	,
Mohanem	Mohan	k1gInSc7	Mohan
a	a	k8xC	a
Dunajem	Dunaj	k1gInSc7	Dunaj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
byla	být	k5eAaImAgFnS	být
Raetie	Raetie	k1gFnSc1	Raetie
částečně	částečně	k6eAd1	částečně
chráněna	chránit	k5eAaImNgFnS	chránit
před	před	k7c7	před
útoky	útok	k1gInPc7	útok
barbarů	barbar	k1gMnPc2	barbar
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
provincie	provincie	k1gFnSc2	provincie
nedisponovala	disponovat	k5eNaBmAgFnS	disponovat
stálou	stálý	k2eAgFnSc7d1	stálá
posádkou	posádka	k1gFnSc7	posádka
a	a	k8xC	a
spoléhala	spoléhat	k5eAaImAgFnS	spoléhat
se	se	k3xPyFc4	se
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c4	na
domorodé	domorodý	k2eAgInPc4d1	domorodý
pomocné	pomocný	k2eAgInPc4d1	pomocný
sbory	sbor	k1gInPc4	sbor
(	(	kIx(	(
<g/>
auxilia	auxilia	k1gFnSc1	auxilia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Marca	Marca	k1gFnSc1	Marca
Aurelia	Aurelia	k1gFnSc1	Aurelia
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
Raetie	Raetie	k1gFnSc2	Raetie
umístěna	umístěn	k2eAgFnSc1d1	umístěna
římská	římský	k2eAgFnSc1d1	římská
legie	legie	k1gFnSc1	legie
(	(	kIx(	(
<g/>
Legio	Legio	k1gMnSc1	Legio
III	III	kA	III
Italica	Italica	k1gMnSc1	Italica
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnSc1	jejíž
velitel	velitel	k1gMnSc1	velitel
působil	působit	k5eAaImAgMnS	působit
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
okamžiku	okamžik	k1gInSc2	okamžik
jako	jako	k8xS	jako
místodržitel	místodržitel	k1gMnSc1	místodržitel
provincie	provincie	k1gFnSc2	provincie
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
císařského	císařský	k2eAgMnSc2d1	císařský
legáta	legát	k1gMnSc2	legát
(	(	kIx(	(
<g/>
legatus	legatus	k1gMnSc1	legatus
Augusti	august	k1gMnPc1	august
pro	pro	k7c4	pro
praetore	praetor	k1gMnSc5	praetor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
Diocletiana	Diocletian	k1gMnSc4	Diocletian
byla	být	k5eAaImAgFnS	být
Raetie	Raetie	k1gFnSc1	Raetie
součástí	součást	k1gFnSc7	součást
diecéze	diecéze	k1gFnSc1	diecéze
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
provincie	provincie	k1gFnPc4	provincie
<g/>
:	:	kIx,	:
Raetia	Raetia	k1gFnSc1	Raetia
prima	prima	k1gFnSc1	prima
(	(	kIx(	(
<g/>
hornatější	hornatý	k2eAgFnSc1d2	hornatější
a	a	k8xC	a
jižněji	jižně	k6eAd2	jižně
položená	položený	k2eAgFnSc1d1	položená
<g/>
)	)	kIx)	)
a	a	k8xC	a
Raetia	Raetia	k1gFnSc1	Raetia
secunda	secunda	k1gFnSc1	secunda
(	(	kIx(	(
<g/>
totožná	totožný	k2eAgFnSc1d1	totožná
s	s	k7c7	s
někdejším	někdejší	k2eAgNnSc7d1	někdejší
územím	území	k1gNnSc7	území
Vindeliků	Vindelik	k1gInPc2	Vindelik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnPc1	jejich
správci	správce	k1gMnPc1	správce
byli	být	k5eAaImAgMnP	být
úředníci	úředník	k1gMnPc1	úředník
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
preasides	preasidesa	k1gFnPc2	preasidesa
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgFnPc3	jenž
byl	být	k5eAaImAgInS	být
nadřízen	nadřízen	k2eAgInSc1d1	nadřízen
dux	dux	k?	dux
zodpovědný	zodpovědný	k2eAgInSc1d1	zodpovědný
za	za	k7c7	za
obě	dva	k4xCgFnPc1	dva
tyto	tento	k3xDgFnPc1	tento
země	zem	k1gFnPc1	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
konečného	konečný	k2eAgInSc2d1	konečný
úpadku	úpadek	k1gInSc2	úpadek
západořímské	západořímský	k2eAgFnSc2d1	Západořímská
říše	říš	k1gFnSc2	říš
byla	být	k5eAaImAgFnS	být
Raetie	Raetie	k1gFnSc1	Raetie
sužována	sužovat	k5eAaImNgFnS	sužovat
vytrvalými	vytrvalý	k2eAgInPc7d1	vytrvalý
nájezdy	nájezd	k1gInPc7	nájezd
Germánů	Germán	k1gMnPc2	Germán
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Římané	Říman	k1gMnPc1	Říman
vyklidili	vyklidit	k5eAaPmAgMnP	vyklidit
Agri	Agre	k1gFnSc4	Agre
Decumates	Decumatesa	k1gFnPc2	Decumatesa
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
vzrůstat	vzrůstat	k5eAaImF	vzrůstat
tlak	tlak	k1gInSc4	tlak
germánských	germánský	k2eAgMnPc2d1	germánský
Alamanů	Alaman	k1gMnPc2	Alaman
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
severní	severní	k2eAgFnSc4d1	severní
a	a	k8xC	a
západní	západní	k2eAgFnSc4d1	západní
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
488	[number]	k4	488
nařídil	nařídit	k5eAaPmAgMnS	nařídit
správce	správce	k1gMnSc1	správce
Itálie	Itálie	k1gFnSc2	Itálie
Odoaker	Odoaker	k1gMnSc1	Odoaker
opuštění	opuštění	k1gNnSc2	opuštění
Raetie	Raetie	k1gFnSc2	Raetie
secundy	secunda	k1gFnSc2	secunda
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
usnadnilo	usnadnit	k5eAaPmAgNnS	usnadnit
pronikání	pronikání	k1gNnSc1	pronikání
Alamanů	Alaman	k1gMnPc2	Alaman
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
Markomanů	Markoman	k1gMnPc2	Markoman
přesunula	přesunout	k5eAaPmAgFnS	přesunout
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
východně	východně	k6eAd1	východně
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Lechu	lech	k1gInSc2	lech
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
později	pozdě	k6eAd2	pozdě
známi	znám	k2eAgMnPc1d1	znám
jako	jako	k8xS	jako
Bajuvarové	Bajuvarové	k2eAgMnPc1d1	Bajuvarové
(	(	kIx(	(
<g/>
Bavoři	Bavor	k1gMnPc1	Bavor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
politická	politický	k2eAgNnPc4d1	politické
a	a	k8xC	a
kulturní	kulturní	k2eAgNnPc4d1	kulturní
pouta	pouto	k1gNnPc4	pouto
Raetie	Raetie	k1gFnSc2	Raetie
primy	prima	k1gFnSc2	prima
vůči	vůči	k7c3	vůči
Itálii	Itálie	k1gFnSc3	Itálie
přetrvala	přetrvat	k5eAaPmAgFnS	přetrvat
ještě	ještě	k6eAd1	ještě
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ovládnutí	ovládnutí	k1gNnSc6	ovládnutí
Ostrogóty	Ostrogót	k1gMnPc4	Ostrogót
Theodoricha	Theodorich	k1gMnSc2	Theodorich
Velikého	veliký	k2eAgNnSc2d1	veliké
se	se	k3xPyFc4	se
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
opět	opět	k6eAd1	opět
pozdvihla	pozdvihnout	k5eAaPmAgFnS	pozdvihnout
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
bývalé	bývalý	k2eAgFnSc3d1	bývalá
prosperitě	prosperita	k1gFnSc3	prosperita
<g/>
.	.	kIx.	.
</s>
<s>
Latinský	latinský	k2eAgInSc1d1	latinský
jazyk	jazyk	k1gInSc1	jazyk
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
vulgarizovanější	vulgarizovaný	k2eAgFnSc6d2	vulgarizovaný
podobě	podoba	k1gFnSc6	podoba
(	(	kIx(	(
<g/>
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
dnešní	dnešní	k2eAgFnSc1d1	dnešní
rétorománština	rétorománština	k1gFnSc1	rétorománština
<g/>
)	)	kIx)	)
a	a	k8xC	a
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
víra	víra	k1gFnSc1	víra
zde	zde	k6eAd1	zde
proto	proto	k8xC	proto
přečkaly	přečkat	k5eAaPmAgFnP	přečkat
bez	bez	k7c2	bez
vážnějších	vážní	k2eAgInPc2d2	vážnější
otřesů	otřes	k1gInPc2	otřes
období	období	k1gNnSc4	období
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
Raetií	Raetie	k1gFnSc7	Raetie
nazývalo	nazývat	k5eAaImAgNnS	nazývat
území	území	k1gNnSc1	území
dřívější	dřívější	k2eAgFnSc2d1	dřívější
Raetie	Raetie	k1gFnSc2	Raetie
primy	prima	k1gFnSc2	prima
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
jméno	jméno	k1gNnSc1	jméno
užíváno	užívat	k5eAaImNgNnS	užívat
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgInSc6d1	švýcarský
kantonu	kanton	k1gInSc6	kanton
Graubünden	Graubündno	k1gNnPc2	Graubündno
dosud	dosud	k6eAd1	dosud
obývaném	obývaný	k2eAgMnSc6d1	obývaný
potomky	potomek	k1gMnPc7	potomek
romanizovaných	romanizovaný	k2eAgMnPc2d1	romanizovaný
keltských	keltský	k2eAgMnPc2d1	keltský
obyvatel	obyvatel	k1gMnPc2	obyvatel
Raetie	Raetie	k1gFnSc2	Raetie
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
Rétorománi	Rétoromán	k1gMnPc1	Rétoromán
<g/>
.	.	kIx.	.
</s>
<s>
Raetie	Raetie	k1gFnSc1	Raetie
byla	být	k5eAaImAgFnS	být
velice	velice	k6eAd1	velice
hornatá	hornatý	k2eAgFnSc1d1	hornatá
země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
způsobem	způsob	k1gInSc7	způsob
obživy	obživa	k1gFnSc2	obživa
jejích	její	k3xOp3gMnPc2	její
obyvatel	obyvatel	k1gMnPc2	obyvatel
byl	být	k5eAaImAgMnS	být
tudíž	tudíž	k8xC	tudíž
chov	chov	k1gInSc4	chov
dobytka	dobytek	k1gInSc2	dobytek
a	a	k8xC	a
těžba	těžba	k1gFnSc1	těžba
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Obdělávání	obdělávání	k1gNnSc1	obdělávání
půdy	půda	k1gFnSc2	půda
byla	být	k5eAaImAgFnS	být
věnována	věnovat	k5eAaImNgFnS	věnovat
jen	jen	k9	jen
malá	malý	k2eAgFnSc1d1	malá
pozornost	pozornost	k1gFnSc1	pozornost
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
některá	některý	k3yIgNnPc4	některý
úrodná	úrodný	k2eAgNnPc4d1	úrodné
údolí	údolí	k1gNnPc4	údolí
poskytovala	poskytovat	k5eAaImAgFnS	poskytovat
hojnost	hojnost	k1gFnSc1	hojnost
obilí	obilí	k1gNnSc2	obilí
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
Římanů	Říman	k1gMnPc2	Říman
nastal	nastat	k5eAaPmAgMnS	nastat
rozmach	rozmach	k1gInSc4	rozmach
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
dařilo	dařit	k5eAaImAgNnS	dařit
se	se	k3xPyFc4	se
též	též	k9	též
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgInPc1d1	místní
svahy	svah	k1gInPc1	svah
byly	být	k5eAaImAgInP	být
osázeny	osázen	k2eAgInPc4d1	osázen
vinohrady	vinohrad	k1gInPc4	vinohrad
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
produkovaly	produkovat	k5eAaImAgFnP	produkovat
víno	víno	k1gNnSc4	víno
považované	považovaný	k2eAgNnSc4d1	považované
ze	z	k7c2	z
stejně	stejně	k6eAd1	stejně
kvalitní	kvalitní	k2eAgFnSc2d1	kvalitní
jako	jako	k8xS	jako
italské	italský	k2eAgFnSc2d1	italská
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
sám	sám	k3xTgMnSc1	sám
Augustus	Augustus	k1gMnSc1	Augustus
prý	prý	k9	prý
dával	dávat	k5eAaImAgMnS	dávat
přednost	přednost	k1gFnSc4	přednost
raetskému	raetský	k2eAgNnSc3d1	raetský
vínu	víno	k1gNnSc3	víno
před	před	k7c7	před
kterýmkoli	kterýkoli	k3yIgMnSc7	kterýkoli
jiným	jiný	k2eAgMnSc7d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznačnějšími	význačný	k2eAgFnPc7d3	nejvýznačnější
obchodními	obchodní	k2eAgFnPc7d1	obchodní
komoditami	komodita	k1gFnPc7	komodita
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
získávány	získáván	k2eAgFnPc1d1	získávána
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
vosk	vosk	k1gInSc4	vosk
<g/>
,	,	kIx,	,
med	med	k1gInSc4	med
<g/>
,	,	kIx,	,
smůla	smůla	k1gFnSc1	smůla
a	a	k8xC	a
sýry	sýr	k1gInPc1	sýr
<g/>
.	.	kIx.	.
</s>
