<s>
Bublinkové	bublinkový	k2eAgNnSc1d1	bublinkové
řazení	řazení	k1gNnSc1	řazení
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
bubble	bubble	k6eAd1	bubble
sort	sorta	k1gFnPc2	sorta
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
řazení	řazení	k1gNnSc2	řazení
záměnou	záměna	k1gFnSc7	záměna
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
implementačně	implementačně	k6eAd1	implementačně
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
řadicí	řadicí	k2eAgInSc1d1	řadicí
algoritmus	algoritmus	k1gInSc1	algoritmus
<g/>
.	.	kIx.	.
</s>
