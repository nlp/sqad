<s>
Bublinkové	bublinkový	k2eAgNnSc1d1	bublinkové
řazení	řazení	k1gNnSc1	řazení
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
bubble	bubble	k6eAd1	bubble
sort	sorta	k1gFnPc2	sorta
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
řazení	řazení	k1gNnSc2	řazení
záměnou	záměna	k1gFnSc7	záměna
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
implementačně	implementačně	k6eAd1	implementačně
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
řadicí	řadicí	k2eAgInSc1d1	řadicí
algoritmus	algoritmus	k1gInSc1	algoritmus
<g/>
.	.	kIx.	.
</s>
<s>
Algoritmus	algoritmus	k1gInSc1	algoritmus
opakovaně	opakovaně	k6eAd1	opakovaně
prochází	procházet	k5eAaImIp3nS	procházet
seznam	seznam	k1gInSc4	seznam
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
porovnává	porovnávat	k5eAaImIp3nS	porovnávat
každé	každý	k3xTgInPc4	každý
dva	dva	k4xCgInPc4	dva
sousedící	sousedící	k2eAgInPc4d1	sousedící
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
nejsou	být	k5eNaImIp3nP	být
ve	v	k7c6	v
správném	správný	k2eAgNnSc6d1	správné
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
,	,	kIx,	,
prohodí	prohodit	k5eAaPmIp3nP	prohodit
je	on	k3xPp3gInPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
praktické	praktický	k2eAgInPc4d1	praktický
účely	účel	k1gInPc4	účel
je	být	k5eAaImIp3nS	být
neefektivní	efektivní	k2eNgMnSc1d1	neefektivní
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
výukové	výukový	k2eAgInPc4d1	výukový
účely	účel	k1gInPc4	účel
či	či	k8xC	či
v	v	k7c6	v
nenáročných	náročný	k2eNgFnPc6d1	nenáročná
aplikacích	aplikace	k1gFnPc6	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Algoritmus	algoritmus	k1gInSc1	algoritmus
je	být	k5eAaImIp3nS	být
univerzální	univerzální	k2eAgMnSc1d1	univerzální
(	(	kIx(	(
<g/>
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
porovnávání	porovnávání	k1gNnSc2	porovnávání
dvojic	dvojice	k1gFnPc2	dvojice
prvků	prvek	k1gInPc2	prvek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pracuje	pracovat	k5eAaImIp3nS	pracovat
lokálně	lokálně	k6eAd1	lokálně
(	(	kIx(	(
<g/>
nevyžaduje	vyžadovat	k5eNaImIp3nS	vyžadovat
pomocnou	pomocný	k2eAgFnSc4d1	pomocná
paměť	paměť	k1gFnSc4	paměť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stabilní	stabilní	k2eAgFnSc1d1	stabilní
(	(	kIx(	(
<g/>
prvkům	prvek	k1gInPc3	prvek
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
klíčem	klíč	k1gInSc7	klíč
nemění	měnit	k5eNaImIp3nS	měnit
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
polohu	poloha	k1gFnSc4	poloha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
přirozené	přirozený	k2eAgInPc4d1	přirozený
řadicí	řadicí	k2eAgInPc4d1	řadicí
algoritmy	algoritmus	k1gInPc4	algoritmus
(	(	kIx(	(
<g/>
částečně	částečně	k6eAd1	částečně
seřazený	seřazený	k2eAgInSc1d1	seřazený
seznam	seznam	k1gInSc1	seznam
zpracuje	zpracovat	k5eAaPmIp3nS	zpracovat
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
neseřazený	seřazený	k2eNgInSc1d1	seřazený
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
průběh	průběh	k1gInSc4	průběh
zpracování	zpracování	k1gNnSc2	zpracování
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
prvky	prvek	k1gInPc4	prvek
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
hodnotou	hodnota	k1gFnSc7	hodnota
"	"	kIx"	"
<g/>
probublávají	probublávat	k5eAaImIp3nP	probublávat
<g/>
"	"	kIx"	"
na	na	k7c4	na
konec	konec	k1gInSc4	konec
seznamu	seznam	k1gInSc2	seznam
<g/>
.	.	kIx.	.
</s>
<s>
Optimalizací	optimalizace	k1gFnSc7	optimalizace
algoritmu	algoritmus	k1gInSc2	algoritmus
je	být	k5eAaImIp3nS	být
detekce	detekce	k1gFnSc1	detekce
prohození	prohození	k1gNnSc2	prohození
prvků	prvek	k1gInPc2	prvek
v	v	k7c6	v
průchodu	průchod	k1gInSc6	průchod
seznamu	seznam	k1gInSc2	seznam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
algoritmus	algoritmus	k1gInSc1	algoritmus
v	v	k7c6	v
průchodu	průchod	k1gInSc6	průchod
neprohodil	prohodit	k5eNaPmAgMnS	prohodit
žádné	žádný	k3yNgInPc4	žádný
dva	dva	k4xCgInPc4	dva
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
žádné	žádný	k3yNgInPc4	žádný
další	další	k2eAgInPc4d1	další
prvky	prvek	k1gInPc4	prvek
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
neprohodí	prohodit	k5eNaPmIp3nS	prohodit
<g/>
.	.	kIx.	.
</s>
<s>
Tudíž	tudíž	k8xC	tudíž
řazení	řazený	k2eAgMnPc1d1	řazený
můžeme	moct	k5eAaImIp1nP	moct
ukončit	ukončit	k5eAaPmF	ukončit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
seznam	seznam	k1gInSc1	seznam
je	být	k5eAaImIp3nS	být
seřazen	seřazen	k2eAgInSc1d1	seřazen
<g/>
.	.	kIx.	.
</s>
<s>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
variací	variace	k1gFnPc2	variace
algoritmu	algoritmus	k1gInSc2	algoritmus
<g/>
.	.	kIx.	.
opakuj	opakovat	k5eAaImRp2nS	opakovat
bylo_seřazeno	bylo_seřazen	k2eAgNnSc4d1	bylo_seřazen
:	:	kIx,	:
<g/>
=	=	kIx~	=
ano	ano	k9	ano
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
i	i	k9	i
od	od	k7c2	od
1	[number]	k4	1
do	do	k7c2	do
(	(	kIx(	(
<g/>
počet_prvků	počet_prvek	k1gMnPc2	počet_prvek
-	-	kIx~	-
1	[number]	k4	1
<g/>
)	)	kIx)	)
opakuj	opakovat	k5eAaImRp2nS	opakovat
<g/>
:	:	kIx,	:
pokud	pokud	k8xS	pokud
seznam	seznam	k1gInSc4	seznam
<g/>
[	[	kIx(	[
<g/>
i	i	k9	i
<g/>
]	]	kIx)	]
>	>	kIx)	>
seznam	seznam	k1gInSc1	seznam
<g/>
[	[	kIx(	[
<g/>
i	i	k9	i
<g />
.	.	kIx.	.
</s>
<s>
+	+	kIx~	+
1	[number]	k4	1
<g/>
]	]	kIx)	]
zaměň	zaměnit	k5eAaPmRp2nS	zaměnit
<g/>
(	(	kIx(	(
<g/>
seznam	seznam	k1gInSc1	seznam
<g/>
[	[	kIx(	[
<g/>
i	i	k9	i
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
seznam	seznam	k1gInSc1	seznam
<g/>
[	[	kIx(	[
<g/>
i	i	k9	i
+	+	kIx~	+
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
bylo_seřazeno	bylo_seřazen	k2eAgNnSc1d1	bylo_seřazen
:	:	kIx,	:
<g/>
=	=	kIx~	=
ne	ne	k9	ne
<g/>
;	;	kIx,	;
dokud	dokud	k8xS	dokud
není	být	k5eNaImIp3nS	být
bylo_seřazeno	bylo_seřazen	k2eAgNnSc1d1	bylo_seřazen
==	==	k?	==
ano	ano	k9	ano
<g/>
;	;	kIx,	;
Alternativní	alternativní	k2eAgFnSc1d1	alternativní
verze	verze	k1gFnSc1	verze
<g/>
:	:	kIx,	:
limit	limit	k1gInSc1	limit
:	:	kIx,	:
<g/>
=	=	kIx~	=
<g />
.	.	kIx.	.
</s>
<s>
počet_prvků	počet_prvek	k1gMnPc2	počet_prvek
<g/>
;	;	kIx,	;
opakuj	opakovat	k5eAaImRp2nS	opakovat
bylo_seřazeno	bylo_seřazen	k2eAgNnSc4d1	bylo_seřazen
:	:	kIx,	:
<g/>
=	=	kIx~	=
ano	ano	k9	ano
<g/>
;	;	kIx,	;
limit	limit	k1gInSc1	limit
:	:	kIx,	:
<g/>
=	=	kIx~	=
limit	limit	k1gInSc1	limit
-	-	kIx~	-
1	[number]	k4	1
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
i	i	k9	i
od	od	k7c2	od
1	[number]	k4	1
do	do	k7c2	do
limit	limita	k1gFnPc2	limita
opakuj	opakovat	k5eAaImRp2nS	opakovat
<g/>
:	:	kIx,	:
pokud	pokud	k8xS	pokud
seznam	seznam	k1gInSc4	seznam
<g/>
[	[	kIx(	[
<g/>
i	i	k9	i
<g/>
]	]	kIx)	]
>	>	kIx)	>
seznam	seznam	k1gInSc1	seznam
<g/>
[	[	kIx(	[
<g/>
i	i	k9	i
+	+	kIx~	+
1	[number]	k4	1
<g/>
]	]	kIx)	]
zaměň	zaměnit	k5eAaPmRp2nS	zaměnit
<g/>
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
seznam	seznam	k1gInSc1	seznam
<g/>
[	[	kIx(	[
<g/>
i	i	k9	i
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
seznam	seznam	k1gInSc1	seznam
<g/>
[	[	kIx(	[
<g/>
i	i	k9	i
+	+	kIx~	+
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
bylo_seřazeno	bylo_seřazen	k2eAgNnSc1d1	bylo_seřazen
:	:	kIx,	:
<g/>
=	=	kIx~	=
ne	ne	k9	ne
<g/>
;	;	kIx,	;
dokud	dokud	k8xS	dokud
není	být	k5eNaImIp3nS	být
bylo_seřazeno	bylo_seřazen	k2eAgNnSc1d1	bylo_seřazen
==	==	k?	==
ano	ano	k9	ano
nebo	nebo	k8xC	nebo
limit	limit	k1gInSc1	limit
==	==	k?	==
1	[number]	k4	1
<g/>
;	;	kIx,	;
Výhodou	výhoda	k1gFnSc7	výhoda
alternativní	alternativní	k2eAgFnSc2d1	alternativní
verze	verze	k1gFnSc2	verze
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
vyšší	vysoký	k2eAgFnSc1d2	vyšší
efektivita	efektivita	k1gFnSc1	efektivita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
n	n	k0	n
opakováních	opakování	k1gNnPc6	opakování
je	on	k3xPp3gMnPc4	on
spodních	spodní	k2eAgNnPc6d1	spodní
n	n	k0	n
prvků	prvek	k1gInPc2	prvek
již	již	k6eAd1	již
seřazeno	seřazen	k2eAgNnSc1d1	seřazeno
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zbytečné	zbytečný	k2eAgNnSc1d1	zbytečné
je	on	k3xPp3gInPc4	on
znovu	znovu	k6eAd1	znovu
procházet	procházet	k5eAaImF	procházet
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
upravený	upravený	k2eAgInSc1d1	upravený
algoritmus	algoritmus	k1gInSc1	algoritmus
se	se	k3xPyFc4	se
nezacyklí	zacyklit	k5eNaPmIp3nS	zacyklit
v	v	k7c6	v
nekonečné	konečný	k2eNgFnSc6d1	nekonečná
smyčce	smyčka	k1gFnSc6	smyčka
ani	ani	k8xC	ani
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
bychom	by	kYmCp1nP	by
měli	mít	k5eAaImAgMnP	mít
špatně	špatně	k6eAd1	špatně
implementovanou	implementovaný	k2eAgFnSc4d1	implementovaná
funkci	funkce	k1gFnSc4	funkce
pro	pro	k7c4	pro
porovnávání	porovnávání	k1gNnSc4	porovnávání
prvků	prvek	k1gInPc2	prvek
seznamu	seznam	k1gInSc2	seznam
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
i	i	k8xC	i
nejhorší	zlý	k2eAgFnSc1d3	nejhorší
asymptotická	asymptotický	k2eAgFnSc1d1	asymptotická
složitost	složitost	k1gFnSc1	složitost
bublinkového	bublinkový	k2eAgNnSc2d1	bublinkové
řazení	řazení	k1gNnSc2	řazení
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
(	(	kIx(	(
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
O	o	k7c6	o
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
algoritmus	algoritmus	k1gInSc1	algoritmus
řazení	řazení	k1gNnSc2	řazení
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejpomalejších	pomalý	k2eAgNnPc2d3	nejpomalejší
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
horší	zlý	k2eAgMnSc1d2	horší
i	i	k8xC	i
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
algoritmy	algoritmus	k1gInPc7	algoritmus
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
asymptotickou	asymptotický	k2eAgFnSc7d1	asymptotická
složitostí	složitost	k1gFnSc7	složitost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
zápisů	zápis	k1gInPc2	zápis
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
neefektivně	efektivně	k6eNd1	efektivně
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
cache	cache	k1gFnSc7	cache
procesoru	procesor	k1gInSc2	procesor
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
jednoduché	jednoduchý	k2eAgFnSc3d1	jednoduchá
implementaci	implementace	k1gFnSc3	implementace
se	se	k3xPyFc4	se
však	však	k9	však
často	často	k6eAd1	často
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
úvodních	úvodní	k2eAgInPc6d1	úvodní
kurzech	kurz	k1gInPc6	kurz
programování	programování	k1gNnSc2	programování
<g/>
.	.	kIx.	.
</s>
<s>
Bublinkové	bublinkový	k2eAgNnSc1d1	bublinkové
řazení	řazení	k1gNnSc1	řazení
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
naprogramování	naprogramování	k1gNnSc2	naprogramování
nejjednodušším	jednoduchý	k2eAgInSc7d3	nejjednodušší
algoritmem	algoritmus	k1gInSc7	algoritmus
pro	pro	k7c4	pro
řazení	řazení	k1gNnSc4	řazení
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
rovněž	rovněž	k9	rovněž
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
stabilní	stabilní	k2eAgMnSc1d1	stabilní
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
nemění	měnit	k5eNaImIp3nS	měnit
pozici	pozice	k1gFnSc4	pozice
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
při	při	k7c6	při
porovnávání	porovnávání	k1gNnSc6	porovnávání
vyhodnoceny	vyhodnocen	k2eAgInPc1d1	vyhodnocen
jako	jako	k8xS	jako
ekvivalentní	ekvivalentní	k2eAgInPc1d1	ekvivalentní
<g/>
.	.	kIx.	.
</s>
<s>
Bublinkové	bublinkový	k2eAgNnSc1d1	bublinkové
řazení	řazení	k1gNnSc1	řazení
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
řadicích	řadicí	k2eAgInPc2d1	řadicí
algoritmů	algoritmus	k1gInPc2	algoritmus
<g/>
,	,	kIx,	,
kterému	který	k3yIgNnSc3	který
stačí	stačit	k5eAaBmIp3nS	stačit
sekvenční	sekvenční	k2eAgInSc1d1	sekvenční
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
datům	datum	k1gNnPc3	datum
(	(	kIx(	(
<g/>
algoritmus	algoritmus	k1gInSc1	algoritmus
nepotřebuje	potřebovat	k5eNaImIp3nS	potřebovat
v	v	k7c6	v
řazené	řazený	k2eAgFnSc6d1	řazená
posloupnosti	posloupnost	k1gFnSc6	posloupnost
provádět	provádět	k5eAaImF	provádět
žádné	žádný	k3yNgInPc4	žádný
skoky	skok	k1gInPc4	skok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
používal	používat	k5eAaImAgInS	používat
k	k	k7c3	k
řazení	řazení	k1gNnSc3	řazení
dat	datum	k1gNnPc2	datum
na	na	k7c6	na
páskových	páskový	k2eAgNnPc6d1	páskové
médiích	médium	k1gNnPc6	médium
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
jej	on	k3xPp3gInSc4	on
lze	lze	k6eAd1	lze
s	s	k7c7	s
výhodou	výhoda	k1gFnSc7	výhoda
použít	použít	k5eAaPmF	použít
například	například	k6eAd1	například
při	při	k7c6	při
řazení	řazení	k1gNnSc3	řazení
jednosměrně	jednosměrně	k6eAd1	jednosměrně
zřetězeného	zřetězený	k2eAgInSc2d1	zřetězený
spojového	spojový	k2eAgInSc2d1	spojový
seznamu	seznam	k1gInSc2	seznam
<g/>
.	.	kIx.	.
</s>
<s>
Bublinkové	bublinkový	k2eAgNnSc1d1	bublinkové
řazení	řazení	k1gNnSc1	řazení
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
například	například	k6eAd1	například
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
programování	programování	k1gNnSc2	programování
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
řazení	řazení	k1gNnSc4	řazení
malých	malý	k2eAgFnPc2d1	malá
polí	pole	k1gFnPc2	pole
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
polí	pole	k1gNnPc2	pole
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
částečně	částečně	k6eAd1	částečně
seřazena	seřazen	k2eAgFnSc1d1	seřazena
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysokému	vysoký	k2eAgInSc3d1	vysoký
výkonu	výkon	k1gInSc3	výkon
současných	současný	k2eAgMnPc2d1	současný
počítačů	počítač	k1gMnPc2	počítač
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
"	"	kIx"	"
<g/>
malé	malý	k2eAgNnSc4d1	malé
<g/>
"	"	kIx"	"
pole	pole	k1gNnSc4	pole
i	i	k9	i
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInSc4	tisíc
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pro	pro	k7c4	pro
řazení	řazení	k1gNnSc4	řazení
opravdu	opravdu	k6eAd1	opravdu
velkých	velký	k2eAgFnPc2d1	velká
polí	pole	k1gFnPc2	pole
je	být	k5eAaImIp3nS	být
bublinkové	bublinkový	k2eAgNnSc1d1	bublinkové
řazení	řazení	k1gNnSc1	řazení
naprosto	naprosto	k6eAd1	naprosto
nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
např.	např.	kA	např.
seřazení	seřazení	k1gNnSc4	seřazení
desetiprvkového	desetiprvkový	k2eAgNnSc2d1	desetiprvkový
pole	pole	k1gNnSc2	pole
trvá	trvat	k5eAaImIp3nS	trvat
jednotku	jednotka	k1gFnSc4	jednotka
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
při	při	k7c6	při
bublinkovém	bublinkový	k2eAgNnSc6d1	bublinkové
řazení	řazení	k1gNnSc6	řazení
stokrát	stokrát	k6eAd1	stokrát
delšího	dlouhý	k2eAgNnSc2d2	delší
(	(	kIx(	(
<g/>
tisíciprvkového	tisíciprvkový	k2eAgNnSc2d1	tisíciprvkový
pole	pole	k1gNnSc2	pole
<g/>
)	)	kIx)	)
spotřebujeme	spotřebovat	k5eAaPmIp1nP	spotřebovat
10000	[number]	k4	10000
jednotek	jednotka	k1gFnPc2	jednotka
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
kvalitní	kvalitní	k2eAgInSc1d1	kvalitní
algoritmus	algoritmus	k1gInSc1	algoritmus
by	by	kYmCp3nS	by
potřeboval	potřebovat	k5eAaImAgInS	potřebovat
pouze	pouze	k6eAd1	pouze
200	[number]	k4	200
jednotek	jednotka	k1gFnPc2	jednotka
času	čas	k1gInSc2	čas
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
jsou	být	k5eAaImIp3nP	být
zbytečná	zbytečný	k2eAgNnPc1d1	zbytečné
porovnání	porovnání	k1gNnPc1	porovnání
při	při	k7c6	při
řazení	řazení	k1gNnSc6	řazení
seznamu	seznam	k1gInSc2	seznam
s	s	k7c7	s
nejnižším	nízký	k2eAgInSc7d3	nejnižší
prvkem	prvek	k1gInSc7	prvek
na	na	k7c6	na
konci	konec	k1gInSc6	konec
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
uvažujeme	uvažovat	k5eAaImIp1nP	uvažovat
vzestupné	vzestupný	k2eAgNnSc4d1	vzestupné
řazení	řazení	k1gNnSc4	řazení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
nepomůže	pomoct	k5eNaPmIp3nS	pomoct
ani	ani	k8xC	ani
optimalizace	optimalizace	k1gFnSc1	optimalizace
pomocí	pomocí	k7c2	pomocí
detekce	detekce	k1gFnSc2	detekce
prohození	prohození	k1gNnSc2	prohození
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
tento	tento	k3xDgInSc1	tento
nejnižší	nízký	k2eAgInSc1d3	nejnižší
prvek	prvek	k1gInSc1	prvek
se	se	k3xPyFc4	se
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
průchodu	průchod	k1gInSc6	průchod
posune	posunout	k5eAaPmIp3nS	posunout
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
místo	místo	k1gNnSc4	místo
vlevo	vlevo	k6eAd1	vlevo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
problém	problém	k1gInSc1	problém
řeší	řešit	k5eAaImIp3nS	řešit
modifikace	modifikace	k1gFnSc1	modifikace
algoritmu	algoritmus	k1gInSc2	algoritmus
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Shaker	Shaker	k1gInSc1	Shaker
sort	sorta	k1gFnPc2	sorta
<g/>
.	.	kIx.	.
</s>
