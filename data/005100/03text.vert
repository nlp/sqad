<s>
Adrenalin	adrenalin	k1gInSc1	adrenalin
(	(	kIx(	(
<g/>
epinefrin	epinefrin	k1gInSc1	epinefrin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hormon	hormon	k1gInSc1	hormon
vyměšovaný	vyměšovaný	k2eAgInSc1d1	vyměšovaný
dření	dřeň	k1gFnSc7	dřeň
nadledvin	nadledvina	k1gFnPc2	nadledvina
<g/>
;	;	kIx,	;
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
katecholaminů	katecholamin	k1gInPc2	katecholamin
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
tělo	tělo	k1gNnSc4	tělo
na	na	k7c4	na
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
hormonem	hormon	k1gInSc7	hormon
stresové	stresový	k2eAgFnSc2d1	stresová
reakce	reakce	k1gFnSc2	reakce
"	"	kIx"	"
<g/>
útok	útok	k1gInSc1	útok
nebo	nebo	k8xC	nebo
útěk	útěk	k1gInSc1	útěk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Adrenalin	adrenalin	k1gInSc1	adrenalin
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
kortizolem	kortizol	k1gInSc7	kortizol
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c4	na
udržení	udržení	k1gNnSc4	udržení
organismu	organismus	k1gInSc2	organismus
při	při	k7c6	při
životě	život	k1gInSc6	život
při	při	k7c6	při
stresové	stresový	k2eAgFnSc6d1	stresová
reakci	reakce	k1gFnSc6	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
látky	látka	k1gFnPc1	látka
působí	působit	k5eAaImIp3nP	působit
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Adrenalin	adrenalin	k1gInSc1	adrenalin
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tělo	tělo	k1gNnSc1	tělo
podává	podávat	k5eAaImIp3nS	podávat
extrémní	extrémní	k2eAgInPc4d1	extrémní
výkony	výkon	k1gInPc4	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Kortizol	Kortizol	k1gInSc1	Kortizol
brání	bránit	k5eAaImIp3nS	bránit
před	před	k7c7	před
poškozením	poškození	k1gNnSc7	poškození
od	od	k7c2	od
účinků	účinek	k1gInPc2	účinek
adrenalinu	adrenalin	k1gInSc2	adrenalin
<g/>
.	.	kIx.	.
zúžení	zúžení	k1gNnSc4	zúžení
periferních	periferní	k2eAgFnPc2d1	periferní
cév	céva	k1gFnPc2	céva
díky	díky	k7c3	díky
působení	působení	k1gNnSc3	působení
na	na	k7c4	na
alfa	alfa	k1gNnSc4	alfa
receptory	receptor	k1gInPc4	receptor
a	a	k8xC	a
následné	následný	k2eAgFnSc6d1	následná
kontrakci	kontrakce	k1gFnSc6	kontrakce
hladkých	hladký	k2eAgInPc2d1	hladký
svalů	sval	k1gInPc2	sval
<g/>
,	,	kIx,	,
tím	ten	k3xDgInSc7	ten
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
centralizaci	centralizace	k1gFnSc3	centralizace
oběhu	oběh	k1gInSc2	oběh
<g/>
,	,	kIx,	,
krev	krev	k1gFnSc1	krev
se	se	k3xPyFc4	se
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
v	v	k7c6	v
životně	životně	k6eAd1	životně
důležitých	důležitý	k2eAgInPc6d1	důležitý
centrálních	centrální	k2eAgInPc6d1	centrální
orgánech	orgán	k1gInPc6	orgán
(	(	kIx(	(
<g/>
srdce	srdce	k1gNnSc4	srdce
<g/>
,	,	kIx,	,
mozek	mozek	k1gInSc1	mozek
<g/>
,	,	kIx,	,
plíce	plíce	k1gFnPc1	plíce
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
také	také	k9	také
pozitivně	pozitivně	k6eAd1	pozitivně
na	na	k7c4	na
srdeční	srdeční	k2eAgFnSc4d1	srdeční
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
tepu	tep	k1gInSc2	tep
<g/>
.	.	kIx.	.
urychlení	urychlení	k1gNnSc4	urychlení
srdeční	srdeční	k2eAgFnSc2d1	srdeční
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
zvýšení	zvýšení	k1gNnSc4	zvýšení
síly	síla	k1gFnSc2	síla
stahu	stah	k1gInSc2	stah
srdečního	srdeční	k2eAgInSc2d1	srdeční
svalu	sval	k1gInSc2	sval
působí	působit	k5eAaImIp3nS	působit
také	také	k9	také
na	na	k7c4	na
hladkou	hladký	k2eAgFnSc4d1	hladká
svalovinu	svalovina	k1gFnSc4	svalovina
průdušek	průduška	k1gFnPc2	průduška
–	–	k?	–
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
jejich	jejich	k3xOp3gInSc1	jejich
průsvit	průsvit	k1gInSc1	průsvit
(	(	kIx(	(
<g/>
bronchodilatace	bronchodilatace	k1gFnSc1	bronchodilatace
<g/>
)	)	kIx)	)
a	a	k8xC	a
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
tak	tak	k6eAd1	tak
ventilaci	ventilace	k1gFnSc3	ventilace
plic	plíce	k1gFnPc2	plíce
rozšíření	rozšíření	k1gNnSc2	rozšíření
zornic	zornice	k1gFnPc2	zornice
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
do	do	k7c2	do
očí	oko	k1gNnPc2	oko
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
více	hodně	k6eAd2	hodně
světla	světlo	k1gNnSc2	světlo
(	(	kIx(	(
<g/>
mydriáza	mydriáza	k1gFnSc1	mydriáza
<g/>
)	)	kIx)	)
aktivace	aktivace	k1gFnSc1	aktivace
potních	potní	k2eAgFnPc2d1	potní
žláz	žláza	k1gFnPc2	žláza
Dále	daleko	k6eAd2	daleko
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
glykémii	glykémie	k1gFnSc4	glykémie
tím	ten	k3xDgNnSc7	ten
že	že	k8xS	že
<g/>
:	:	kIx,	:
snižuje	snižovat	k5eAaImIp3nS	snižovat
hladinu	hladina	k1gFnSc4	hladina
inzulínu	inzulín	k1gInSc2	inzulín
–	–	k?	–
což	což	k3yRnSc4	což
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
trochu	trochu	k6eAd1	trochu
kontraproduktivní	kontraproduktivní	k2eAgNnSc1d1	kontraproduktivní
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
inzulín	inzulín	k1gInSc1	inzulín
transportuje	transportovat	k5eAaBmIp3nS	transportovat
cukr	cukr	k1gInSc4	cukr
do	do	k7c2	do
buněk	buňka	k1gFnPc2	buňka
(	(	kIx(	(
<g/>
i	i	k9	i
do	do	k7c2	do
těch	ten	k3xDgFnPc2	ten
svalových	svalový	k2eAgFnPc2d1	svalová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
inzulín	inzulín	k1gInSc1	inzulín
ukládá	ukládat	k5eAaImIp3nS	ukládat
cukr	cukr	k1gInSc4	cukr
do	do	k7c2	do
tuků	tuk	k1gInPc2	tuk
nebo	nebo	k8xC	nebo
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
tvoří	tvořit	k5eAaImIp3nP	tvořit
glykogen	glykogen	k1gInSc4	glykogen
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
při	při	k7c6	při
stresu	stres	k1gInSc6	stres
není	být	k5eNaImIp3nS	být
moc	moc	k6eAd1	moc
vhodné	vhodný	k2eAgNnSc1d1	vhodné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
syntézu	syntéza	k1gFnSc4	syntéza
glukagonu	glukagon	k1gInSc2	glukagon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
štěpí	štěpit	k5eAaImIp3nS	štěpit
glykogen	glykogen	k1gInSc4	glykogen
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
na	na	k7c4	na
glukózu	glukóza	k1gFnSc4	glukóza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
akutní	akutní	k2eAgFnSc6d1	akutní
medicíně	medicína	k1gFnSc6	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
mohutné	mohutný	k2eAgInPc4d1	mohutný
účinky	účinek	k1gInPc4	účinek
na	na	k7c4	na
srdce	srdce	k1gNnSc4	srdce
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
v	v	k7c6	v
resuscitaci	resuscitace	k1gFnSc6	resuscitace
<g/>
.	.	kIx.	.
</s>
<s>
Aktivity	aktivita	k1gFnPc1	aktivita
spojené	spojený	k2eAgFnPc1d1	spojená
s	s	k7c7	s
uvolňováním	uvolňování	k1gNnSc7	uvolňování
adrenalinu	adrenalin	k1gInSc2	adrenalin
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
pozitivně	pozitivně	k6eAd1	pozitivně
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
lidskou	lidský	k2eAgFnSc4d1	lidská
psychiku	psychika	k1gFnSc4	psychika
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sexuologa	sexuolog	k1gMnSc2	sexuolog
Radima	Radim	k1gMnSc2	Radim
Uzla	Uzlus	k1gMnSc2	Uzlus
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
adrenalin	adrenalin	k1gInSc4	adrenalin
chuť	chuť	k1gFnSc1	chuť
na	na	k7c4	na
sex	sex	k1gInSc4	sex
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
adrenalin	adrenalin	k1gInSc4	adrenalin
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Galerie	galerie	k1gFnSc2	galerie
adrenalin	adrenalin	k1gInSc4	adrenalin
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
adrenalin	adrenalin	k1gInSc1	adrenalin
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
