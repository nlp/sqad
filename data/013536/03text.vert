<s>
Gabino	Gabino	k1gNnSc1
Rey	Rea	k1gFnSc2
</s>
<s>
Gabino	Gabin	k2eAgNnSc1d1
ReyRodné	ReyRodný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Gabino	Gabin	k2eAgNnSc1d1
Victorio	Victorio	k1gNnSc1
Segundo	Segundo	k1gNnSc1
Rey	Rea	k1gFnSc2
de	de	k?
Santiago	Santiago	k1gNnSc1
Narození	narození	k1gNnSc2
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1928	#num#	k4
<g/>
Marín	Marína	k1gFnPc2
<g/>
,	,	kIx,
Pontevedra	Pontevedra	k1gFnSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2006	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
78	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Barcelona	Barcelona	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
malíř	malíř	k1gMnSc1
a	a	k8xC
umělec	umělec	k1gMnSc1
Děti	dítě	k1gFnPc4
</s>
<s>
Pablo	Pablo	k1gNnSc1
Rey	Rea	k1gFnSc2
Rodiče	rodič	k1gMnSc2
</s>
<s>
Agustín	Agustín	k1gInSc1
Rey	Rea	k1gFnSc2
Fonseca	Fonsec	k1gInSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Gabino	Gabino	k1gMnSc1
Rey	Rey	k1gMnSc1
Santiago	Santiago	k1gMnSc1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1928	#num#	k4
<g/>
,	,	kIx,
Marín	Marína	k1gFnPc2
<g/>
,	,	kIx,
Pontevedra	Pontevedra	k1gFnSc1
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2006	#num#	k4
<g/>
,	,	kIx,
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
španělský	španělský	k2eAgMnSc1d1
malíř	malíř	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znám	znát	k5eAaImIp1nS
byl	být	k5eAaImAgMnS
pod	pod	k7c7
uměleckým	umělecký	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Gabino	Gabino	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
V	v	k7c6
Galícii	Galície	k1gFnSc6
narozený	narozený	k2eAgMnSc1d1
Rey	Rea	k1gFnPc1
se	se	k3xPyFc4
během	během	k7c2
španělské	španělský	k2eAgFnSc2d1
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
(	(	kIx(
<g/>
1936	#num#	k4
až	až	k9
1939	#num#	k4
<g/>
)	)	kIx)
přestěhoval	přestěhovat	k5eAaPmAgInS
do	do	k7c2
Barcelony	Barcelona	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
byl	být	k5eAaImAgInS
žákem	žák	k1gMnSc7
španělského	španělský	k2eAgNnSc2d1
malíře	malíř	k1gMnSc2
Ramona	Ramona	k1gFnSc1
Rogenta	Rogenta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
svých	svůj	k3xOyFgNnPc6
dílech	dílo	k1gNnPc6
zobrazoval	zobrazovat	k5eAaImAgMnS
především	především	k9
krajinu	krajina	k1gFnSc4
<g/>
,	,	kIx,
květiny	květina	k1gFnPc4
a	a	k8xC
venkovský	venkovský	k2eAgInSc1d1
život	život	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
nejznámější	známý	k2eAgFnSc1d3
malba	malba	k1gFnSc1
„	„	k?
<g/>
El	Ela	k1gFnPc2
Port	port	k1gInSc4
<g/>
“	“	k?
vznikla	vzniknout	k5eAaPmAgFnS
roku	rok	k1gInSc2
1977	#num#	k4
v	v	k7c6
barcelonském	barcelonský	k2eAgInSc6d1
přístavu	přístav	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Reyova	Reyův	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
vynikají	vynikat	k5eAaImIp3nP
hlavně	hlavně	k9
uzpůsobením	uzpůsobení	k1gNnSc7
světla	světlo	k1gNnSc2
<g/>
,	,	kIx,
díky	díky	k7c3
čemuž	což	k3yQnSc3,k3yRnSc3
byl	být	k5eAaImAgInS
také	také	k6eAd1
nazýván	nazýván	k2eAgMnSc1d1
„	„	k?
<g/>
maestro	maestro	k1gMnSc1
de	de	k?
la	la	k1gNnSc2
luz	luza	k1gFnPc2
<g/>
“	“	k?
(	(	kIx(
<g/>
Mistr	mistr	k1gMnSc1
světla	světlo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vystavoval	vystavovat	k5eAaImAgMnS
v	v	k7c6
Madridu	Madrid	k1gInSc6
i	i	k8xC
Barceloně	Barcelona	k1gFnSc6
<g/>
,	,	kIx,
Londýně	Londýn	k1gInSc6
<g/>
,	,	kIx,
Paříži	Paříž	k1gFnSc6
a	a	k8xC
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
PUIG	PUIG	kA
<g/>
,	,	kIx,
Arnau	Arnaa	k1gFnSc4
<g/>
;	;	kIx,
REY	Rea	k1gFnSc2
SANTIAGO	Santiago	k1gNnSc1
<g/>
,	,	kIx,
Gabino	Gabino	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gabino	Gabino	k1gNnSc1
<g/>
:	:	kIx,
estudi	estudit	k5eAaPmRp2nS
d	d	k?
<g/>
'	'	kIx"
<g/>
un	un	k?
projecte	projéct	k5eAaPmRp2nPwK
pictò	pictò	k1gMnSc1
fet	fet	k?
sota	sota	k1gMnSc1
la	la	k1gNnSc2
consideració	consideració	k?
de	de	k?
la	la	k1gNnSc2
llum	lluma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
Mayo	Mayo	k1gMnSc1
S.	S.	kA
<g/>
A.	A.	kA
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
8486180309	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
katalánsky	katalánsky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
PUIG	PUIG	kA
<g/>
,	,	kIx,
Arnau	Arnaa	k1gFnSc4
<g/>
;	;	kIx,
REY	Rea	k1gFnSc2
SANTIAGO	Santiago	k1gNnSc1
<g/>
,	,	kIx,
Gabino	Gabino	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gabino	Gabino	k1gNnSc1
<g/>
:	:	kIx,
estudi	estudit	k5eAaPmRp2nS
d	d	k?
<g/>
'	'	kIx"
<g/>
un	un	k?
projecte	projéct	k5eAaPmRp2nPwK
pictò	pictò	k1gMnSc1
fet	fet	k?
sota	sota	k1gMnSc1
la	la	k1gNnSc2
consideració	consideració	k?
de	de	k?
la	la	k1gNnSc2
llum	lluma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
Mayo	Mayo	k1gMnSc1
S.	S.	kA
<g/>
A.	A.	kA
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
8486180309	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
katalánsky	katalánsky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0004	#num#	k4
1028	#num#	k4
1360	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
304822882	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Španělsko	Španělsko	k1gNnSc1
|	|	kIx~
Umění	umění	k1gNnSc1
</s>
