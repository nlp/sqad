<s>
Bjø	Bjø	k?	Bjø
Martinius	Martinius	k1gMnSc1	Martinius
Bjø	Bjø	k1gMnSc1	Bjø
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1832	[number]	k4	1832
<g/>
,	,	kIx,	,
Kvikne	kviknout	k5eAaPmIp3nS	kviknout
-	-	kIx~	-
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
norský	norský	k2eAgMnSc1d1	norský
kriticko-realistický	kritickoealistický	k2eAgMnSc1d1	kriticko-realistický
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
<g/>
.	.	kIx.	.
</s>
<s>
Bjø	Bjø	k?	Bjø
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
roku	rok	k1gInSc2	rok
1832	[number]	k4	1832
v	v	k7c6	v
Kvikne	kviknout	k5eAaPmIp3nS	kviknout
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
norské	norský	k2eAgFnSc6d1	norská
provincii	provincie	k1gFnSc6	provincie
Hedmark	Hedmark	k1gInSc4	Hedmark
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
pastorem	pastor	k1gMnSc7	pastor
(	(	kIx(	(
<g/>
luterským	luterský	k2eAgMnSc7d1	luterský
farářem	farář	k1gMnSc7	farář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Christianii	Christianie	k1gFnSc6	Christianie
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgNnSc1d1	dnešní
Oslo	Oslo	k1gNnSc1	Oslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
žurnalista	žurnalista	k1gMnSc1	žurnalista
a	a	k8xC	a
literární	literární	k2eAgMnSc1d1	literární
a	a	k8xC	a
divadelní	divadelní	k2eAgMnSc1d1	divadelní
kritik	kritik	k1gMnSc1	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecnou	všeobecný	k2eAgFnSc4d1	všeobecná
pozornost	pozornost	k1gFnSc4	pozornost
vzbudil	vzbudit	k5eAaPmAgMnS	vzbudit
roku	rok	k1gInSc2	rok
1857	[number]	k4	1857
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyšla	vyjít	k5eAaPmAgFnS	vyjít
jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
selská	selský	k2eAgFnSc1d1	selská
povídka	povídka	k1gFnSc1	povídka
Synnöve	Synnöev	k1gFnSc2	Synnöev
Solbakken	Solbakkna	k1gFnPc2	Solbakkna
<g/>
.	.	kIx.	.
</s>
<s>
Bjø	Bjø	k?	Bjø
Selské	selský	k2eAgFnSc2d1	selská
povídky	povídka	k1gFnSc2	povídka
(	(	kIx(	(
<g/>
Samlede	Samled	k1gInSc5	Samled
digter	digter	k1gInSc4	digter
<g/>
)	)	kIx)	)
souborně	souborně	k6eAd1	souborně
vydané	vydaný	k2eAgFnPc4d1	vydaná
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
jsou	být	k5eAaImIp3nP	být
formálně	formálně	k6eAd1	formálně
novátorské	novátorský	k2eAgFnPc1d1	novátorská
a	a	k8xC	a
křesťanskou	křesťanský	k2eAgFnSc7d1	křesťanská
etikou	etika	k1gFnSc7	etika
prodchnuté	prodchnutý	k2eAgInPc1d1	prodchnutý
příběhy	příběh	k1gInPc1	příběh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
novotou	novota	k1gFnSc7	novota
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
hloubkou	hloubka	k1gFnSc7	hloubka
citu	cit	k1gInSc2	cit
<g/>
,	,	kIx,	,
psychologickou	psychologický	k2eAgFnSc7d1	psychologická
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
a	a	k8xC	a
jazykem	jazyk	k1gInSc7	jazyk
plným	plný	k2eAgInSc7d1	plný
poezie	poezie	k1gFnSc2	poezie
a	a	k8xC	a
elementární	elementární	k2eAgFnSc2d1	elementární
síly	síla	k1gFnSc2	síla
liší	lišit	k5eAaImIp3nP	lišit
ode	ode	k7c2	ode
všeho	všecek	k3xTgNnSc2	všecek
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
bylo	být	k5eAaImAgNnS	být
doposud	doposud	k6eAd1	doposud
v	v	k7c6	v
norské	norský	k2eAgFnSc6d1	norská
literatuře	literatura	k1gFnSc6	literatura
vytvořeno	vytvořen	k2eAgNnSc1d1	vytvořeno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Selských	selský	k2eAgFnPc6d1	selská
povídkách	povídka	k1gFnPc6	povídka
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
najít	najít	k5eAaPmF	najít
i	i	k9	i
mnoho	mnoho	k4c4	mnoho
Bjø	Bjø	k1gFnPc2	Bjø
lyrických	lyrický	k2eAgFnPc2d1	lyrická
básní	báseň	k1gFnPc2	báseň
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Arne	Arne	k1gMnSc1	Arne
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
mnohé	mnohý	k2eAgFnPc1d1	mnohá
zlidověly	zlidovět	k5eAaPmAgFnP	zlidovět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
Bjø	Bjø	k1gFnSc6	Bjø
tvorbě	tvorba	k1gFnSc6	tvorba
zásadní	zásadní	k2eAgInSc4d1	zásadní
obrat	obrat	k1gInSc4	obrat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
hesla	heslo	k1gNnSc2	heslo
<g/>
,	,	kIx,	,
že	že	k8xS	že
literatura	literatura	k1gFnSc1	literatura
jen	jen	k9	jen
potud	potud	k6eAd1	potud
žije	žít	k5eAaImIp3nS	žít
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
problémy	problém	k1gInPc1	problém
řeší	řešit	k5eAaImIp3nP	řešit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Bjø	Bjø	k1gMnSc1	Bjø
začal	začít	k5eAaPmAgMnS	začít
věnovat	věnovat	k5eAaImF	věnovat
aktuálním	aktuální	k2eAgInPc3d1	aktuální
společenským	společenský	k2eAgInPc3d1	společenský
problémům	problém	k1gInPc3	problém
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
řešil	řešit	k5eAaImAgInS	řešit
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
divadelních	divadelní	k2eAgFnPc6d1	divadelní
hrách	hra	k1gFnPc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
společně	společně	k6eAd1	společně
s	s	k7c7	s
Henrikem	Henrik	k1gMnSc7	Henrik
Ibsenem	Ibsen	k1gMnSc7	Ibsen
považován	považován	k2eAgInSc4d1	považován
za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
norského	norský	k2eAgNnSc2d1	norské
národního	národní	k2eAgNnSc2d1	národní
dramatu	drama	k1gNnSc2	drama
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
tvorbě	tvorba	k1gFnSc6	tvorba
byl	být	k5eAaImAgMnS	být
kritický	kritický	k2eAgMnSc1d1	kritický
k	k	k7c3	k
morálce	morálka	k1gFnSc3	morálka
měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
zdůrazňoval	zdůrazňovat	k5eAaImAgInS	zdůrazňovat
mravní	mravní	k2eAgFnSc4d1	mravní
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznávanou	uznávaný	k2eAgFnSc7d1	uznávaná
osobností	osobnost	k1gFnSc7	osobnost
kulturního	kulturní	k2eAgInSc2d1	kulturní
a	a	k8xC	a
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Zasazoval	zasazovat	k5eAaImAgMnS	zasazovat
se	se	k3xPyFc4	se
o	o	k7c4	o
liberalismus	liberalismus	k1gInSc4	liberalismus
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc4d1	kulturní
i	i	k8xC	i
státní	státní	k2eAgFnSc4d1	státní
samostatnost	samostatnost	k1gFnSc4	samostatnost
Norska	Norsko	k1gNnSc2	Norsko
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc1	jeho
báseň	báseň	k1gFnSc1	báseň
Ano	ano	k9	ano
<g/>
,	,	kIx,	,
milujeme	milovat	k5eAaImIp1nP	milovat
tuto	tento	k3xDgFnSc4	tento
zemi	zem	k1gFnSc4	zem
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
norskou	norský	k2eAgFnSc7d1	norská
státní	státní	k2eAgFnSc7d1	státní
hymnou	hymna	k1gFnSc7	hymna
<g/>
)	)	kIx)	)
i	i	k9	i
o	o	k7c4	o
práva	právo	k1gNnPc4	právo
utlačovaných	utlačovaný	k2eAgInPc2d1	utlačovaný
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Slováků	Slovák	k1gMnPc2	Slovák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
"	"	kIx"	"
<g/>
...	...	k?	...
jako	jako	k8xC	jako
projev	projev	k1gInSc1	projev
uznání	uznání	k1gNnSc2	uznání
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
ušlechtilou	ušlechtilý	k2eAgFnSc4d1	ušlechtilá
<g/>
,	,	kIx,	,
velkolepou	velkolepý	k2eAgFnSc4d1	velkolepá
a	a	k8xC	a
mnohostrannou	mnohostranný	k2eAgFnSc4d1	mnohostranná
literární	literární	k2eAgFnSc4d1	literární
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vždycky	vždycky	k6eAd1	vždycky
vyznačovala	vyznačovat	k5eAaImAgFnS	vyznačovat
jak	jak	k6eAd1	jak
svěžestí	svěžest	k1gFnSc7	svěžest
inspirace	inspirace	k1gFnSc2	inspirace
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
vzácnou	vzácný	k2eAgFnSc7d1	vzácná
čistotou	čistota	k1gFnSc7	čistota
ducha	duch	k1gMnSc2	duch
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
citace	citace	k1gFnPc1	citace
z	z	k7c2	z
odůvodnění	odůvodnění	k1gNnSc2	odůvodnění
Švédské	švédský	k2eAgFnSc2d1	švédská
akademie	akademie	k1gFnSc2	akademie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bjø	Bjø	k?	Bjø
Bjø	Bjø	k1gMnSc1	Bjø
zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
mezi	mezi	k7c7	mezi
ulicí	ulice	k1gFnSc7	ulice
Veveří	veveří	k2eAgFnSc7d1	veveří
<g/>
,	,	kIx,	,
ulicí	ulice	k1gFnSc7	ulice
Zahradníkovou	Zahradníková	k1gFnSc7	Zahradníková
<g/>
,	,	kIx,	,
budovou	budova	k1gFnSc7	budova
právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
a	a	k8xC	a
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
budov	budova	k1gFnPc2	budova
Univerzity	univerzita	k1gFnSc2	univerzita
obrany	obrana	k1gFnSc2	obrana
(	(	kIx(	(
<g/>
zvanou	zvaný	k2eAgFnSc4d1	zvaná
"	"	kIx"	"
<g/>
Rohlík	Rohlík	k1gMnSc1	Rohlík
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Björnsonův	Björnsonův	k2eAgInSc1d1	Björnsonův
sad	sad	k1gInSc1	sad
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
s	s	k7c7	s
"	"	kIx"	"
<g/>
ö	ö	k?	ö
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
desce	deska	k1gFnSc6	deska
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
sadu	sad	k1gInSc2	sad
(	(	kIx(	(
<g/>
roh	roh	k1gInSc1	roh
Veveří	veveří	k2eAgFnSc2d1	veveří
a	a	k8xC	a
Zahradníkovy	Zahradníkův	k2eAgFnSc2d1	Zahradníkova
ul	ul	kA	ul
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
psáno	psát	k5eAaImNgNnS	psát
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
norský	norský	k2eAgMnSc1d1	norský
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
zastánce	zastánce	k1gMnSc1	zastánce
pravdy	pravda	k1gFnSc2	pravda
a	a	k8xC	a
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
přítel	přítel	k1gMnSc1	přítel
českého	český	k2eAgMnSc2d1	český
a	a	k8xC	a
slovenského	slovenský	k2eAgInSc2d1	slovenský
národa	národ	k1gInSc2	národ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Synnöve	Synnöev	k1gFnPc1	Synnöev
ze	z	k7c2	z
Slunečného	slunečný	k2eAgNnSc2d1	slunečné
návrší	návrší	k1gNnSc2	návrší
(	(	kIx(	(
<g/>
1857	[number]	k4	1857
<g/>
,	,	kIx,	,
Synnöve	Synnöev	k1gFnPc1	Synnöev
Solbakken	Solbakken	k1gInSc1	Solbakken
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Arne	Arne	k1gMnSc1	Arne
(	(	kIx(	(
<g/>
1859	[number]	k4	1859
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Veselý	Veselý	k1gMnSc1	Veselý
hoch	hoch	k1gMnSc1	hoch
(	(	kIx(	(
<g/>
1860	[number]	k4	1860
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Král	Král	k1gMnSc1	Král
Sverre	Sverr	k1gInSc5	Sverr
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
,	,	kIx,	,
Kong	Kongo	k1gNnPc2	Kongo
Sverre	Sverr	k1gMnSc5	Sverr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sigurd	Sigurd	k1gMnSc1	Sigurd
Slembe	Slemb	k1gInSc5	Slemb
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Marie	Maria	k1gFnSc2	Maria
Stuartovna	Stuartovna	k1gFnSc1	Stuartovna
Skotská	skotská	k1gFnSc5	skotská
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
,	,	kIx,	,
Maria	Mario	k1gMnSc2	Mario
Stuart	Stuarta	k1gFnPc2	Stuarta
<g/>
'	'	kIx"	'
i	i	k8xC	i
Skotland	Skotland	k1gInSc4	Skotland
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Novomanželé	novomanžel	k1gMnPc1	novomanžel
(	(	kIx(	(
<g/>
1865	[number]	k4	1865
<g/>
,	,	kIx,	,
De	De	k?	De
Nygifte	Nygift	k1gMnSc5	Nygift
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rybářské	rybářský	k2eAgNnSc4d1	rybářské
děvče	děvče	k1gNnSc4	děvče
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ano	ano	k9	ano
<g/>
,	,	kIx,	,
milujeme	milovat	k5eAaImIp1nP	milovat
tuto	tento	k3xDgFnSc4	tento
zemi	zem	k1gFnSc4	zem
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1870	[number]	k4	1870
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Arnljot	Arnljot	k1gMnSc1	Arnljot
Gelline	Gellin	k1gInSc5	Gellin
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sigurd	Sigurd	k1gMnSc1	Sigurd
Jorsalfar	Jorsalfar	k1gMnSc1	Jorsalfar
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Selské	selský	k2eAgFnPc1d1	selská
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
,	,	kIx,	,
Samlede	Samled	k1gInSc5	Samled
digter	digtrum	k1gNnPc2	digtrum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Úpadek	úpadek	k1gInSc1	úpadek
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
jako	jako	k9	jako
Bankrot	bankrot	k1gInSc1	bankrot
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Redaktor	redaktor	k1gMnSc1	redaktor
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
,	,	kIx,	,
Redaktören	Redaktörna	k1gFnPc2	Redaktörna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Král	Král	k1gMnSc1	Král
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
,	,	kIx,	,
Kongen	Kongen	k1gInSc1	Kongen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Magnhild	Magnhild	k1gMnSc1	Magnhild
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Setník	setník	k1gMnSc1	setník
Masana	Masan	k1gMnSc2	Masan
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
,	,	kIx,	,
Kaptejn	Kaptejn	k1gInSc1	Kaptejn
Mansana	Mansana	k1gFnSc1	Mansana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Leonarda	Leonardo	k1gMnSc2	Leonardo
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
systém	systém	k1gInSc1	systém
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Prach	prach	k1gInSc1	prach
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jednoženství	jednoženství	k1gNnSc6	jednoženství
a	a	k8xC	a
mnohoženství	mnohoženství	k1gNnSc6	mnohoženství
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rukavička	rukavička	k1gFnSc1	rukavička
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nad	nad	k7c4	nad
sílu	síla	k1gFnSc4	síla
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
,	,	kIx,	,
Über	Über	k1gInSc1	Über
die	die	k?	die
Kraft	Kraft	k1gInSc1	Kraft
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Vlajky	vlajka	k1gFnPc1	vlajka
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
a	a	k8xC	a
přístavem	přístav	k1gInSc7	přístav
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
,	,	kIx,	,
Det	Det	k1gMnSc1	Det
flager	flager	k1gMnSc1	flager
i	i	k8xC	i
byen	byen	k1gMnSc1	byen
og	og	k?	og
pa	pa	k0	pa
havnen	havnno	k1gNnPc2	havnno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zeměpis	zeměpis	k1gInSc1	zeměpis
a	a	k8xC	a
láska	láska	k1gFnSc1	láska
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Na	na	k7c6	na
božích	boží	k2eAgFnPc6d1	boží
cestách	cesta	k1gFnPc6	cesta
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Lange	Lang	k1gFnSc2	Lang
a	a	k8xC	a
Tore	Tor	k1gMnSc5	Tor
Parsbergová	Parsbergový	k2eAgFnSc1d1	Parsbergový
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Lange	Lang	k1gFnSc2	Lang
og	og	k?	og
Torra	Torra	k1gMnSc1	Torra
Parsberg	Parsberg	k1gMnSc1	Parsberg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Laboremus	Laboremus	k1gMnSc1	Laboremus
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Na	na	k7c4	na
Storhovu	Storhova	k1gFnSc4	Storhova
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Daglannet	Daglannet	k1gMnSc1	Daglannet
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mary	Mary	k1gFnSc1	Mary
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Když	když	k8xS	když
réva	réva	k1gFnSc1	réva
znovu	znovu	k6eAd1	znovu
kvete	kvést	k5eAaImIp3nS	kvést
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Selské	selské	k1gNnSc1	selské
novely	novela	k1gFnSc2	novela
<g/>
,	,	kIx,	,
Hynek	Hynek	k1gMnSc1	Hynek
Mejsnar	Mejsnar	k1gMnSc1	Mejsnar
<g/>
,	,	kIx,	,
Tábor	Tábor	k1gInSc1	Tábor
1873	[number]	k4	1873
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Hynek	Hynek	k1gMnSc1	Hynek
Mejsnar	Mejsnar	k1gMnSc1	Mejsnar
<g/>
,	,	kIx,	,
Novomanželé	novomanžel	k1gMnPc1	novomanžel
<g/>
,	,	kIx,	,
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
Urbánek	Urbánek	k1gMnSc1	Urbánek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1874	[number]	k4	1874
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Eliška	Eliška	k1gFnSc1	Eliška
Pešková	Pešková	k1gFnSc1	Pešková
<g/>
,	,	kIx,	,
Selské	selské	k1gNnSc1	selské
novely	novela	k1gFnSc2	novela
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
Urbánek	Urbánek	k1gMnSc1	Urbánek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1875	[number]	k4	1875
<g/>
,	,	kIx,	,
Bankrot	bankrot	k1gInSc1	bankrot
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1877	[number]	k4	1877
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
J.	J.	kA	J.
Bittner	Bittner	k1gMnSc1	Bittner
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
Časopis	časopis	k1gInSc1	časopis
českého	český	k2eAgNnSc2d1	české
studentstva	studentstvo	k1gNnSc2	studentstvo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Hugo	Hugo	k1gMnSc1	Hugo
Kosterka	Kosterka	k1gFnSc1	Kosterka
<g/>
,	,	kIx,	,
Dědictví	dědictví	k1gNnPc2	dědictví
Kurtův	Kurtův	k2eAgInSc1d1	Kurtův
<g/>
,	,	kIx,	,
Časopis	časopis	k1gInSc1	časopis
českého	český	k2eAgNnSc2d1	české
studentstva	studentstvo	k1gNnSc2	studentstvo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Hugo	Hugo	k1gMnSc1	Hugo
Kosterka	Kosterka	k1gFnSc1	Kosterka
<g/>
,	,	kIx,	,
Na	na	k7c6	na
<g />
.	.	kIx.	.
</s>
<s>
božích	boží	k2eAgFnPc6d1	boží
cestách	cesta	k1gFnPc6	cesta
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Hugo	Hugo	k1gMnSc1	Hugo
Kosterka	Kosterka	k1gFnSc1	Kosterka
<g/>
,	,	kIx,	,
Prach	prach	k1gInSc1	prach
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Šimáček	Šimáček	k1gMnSc1	Šimáček
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Hugo	Hugo	k1gMnSc1	Hugo
Kosterka	Kosterka	k1gFnSc1	Kosterka
<g/>
,	,	kIx,	,
Jednoženství	jednoženství	k1gNnSc1	jednoženství
a	a	k8xC	a
mnohoženství	mnohoženství	k1gNnSc1	mnohoženství
<g/>
,	,	kIx,	,
E.	E.	kA	E.
Beaufort	Beaufort	k1gInSc1	Beaufort
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Hugo	Hugo	k1gMnSc1	Hugo
Kosterka	Kosterka	k1gFnSc1	Kosterka
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Úpadek	úpadek	k1gInSc1	úpadek
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Knapp	Knapp	k1gMnSc1	Knapp
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
J.E.	J.E.	k1gMnSc1	J.E.
Šlechta	Šlechta	k1gMnSc1	Šlechta
<g/>
,	,	kIx,	,
Absolonovy	Absolonův	k2eAgInPc1d1	Absolonův
vlasy	vlas	k1gInPc1	vlas
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Hugo	Hugo	k1gMnSc1	Hugo
Kosterka	Kosterka	k1gFnSc1	Kosterka
<g/>
,	,	kIx,	,
Nad	nad	k7c4	nad
naši	náš	k3xOp1gFnSc4	náš
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kvapil	Kvapil	k1gMnSc1	Kvapil
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Nad	nad	k7c4	nad
naši	náš	k3xOp1gFnSc4	náš
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Hanuš	Hanuš	k1gMnSc1	Hanuš
Hackenschmied	Hackenschmied	k1gMnSc1	Hackenschmied
<g/>
,	,	kIx,	,
Rybářské	rybářský	k2eAgNnSc1d1	rybářské
děvče	děvče	k1gNnSc1	děvče
<g/>
,	,	kIx,	,
Magnhild	Magnhild	k1gMnSc1	Magnhild
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Milada	Milada	k1gFnSc1	Milada
Krausová	Krausová	k1gFnSc1	Krausová
<g/>
,	,	kIx,	,
Mary	Mary	k1gFnSc1	Mary
<g/>
,	,	kIx,	,
Nakladatelské	nakladatelský	k2eAgNnSc1d1	nakladatelské
družstvo	družstvo	k1gNnSc1	družstvo
Máje	máje	k1gFnSc1	máje
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1907	[number]	k4	1907
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Hugo	Hugo	k1gMnSc1	Hugo
Kosterka	Kosterka	k1gFnSc1	Kosterka
<g/>
,	,	kIx,	,
Železnice	železnice	k1gFnSc1	železnice
a	a	k8xC	a
hřbitov	hřbitov	k1gInSc1	hřbitov
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Milada	Milada	k1gFnSc1	Milada
Krausová	Krausová	k1gFnSc1	Krausová
<g/>
,	,	kIx,	,
Povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
tři	tři	k4xCgInPc4	tři
díly	díl	k1gInPc4	díl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1907	[number]	k4	1907
<g/>
-	-	kIx~	-
<g/>
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Milada	Milada	k1gFnSc1	Milada
Krausová	Krausová	k1gFnSc1	Krausová
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Setnik	Setnik	k1gInSc1	Setnik
Mansana	Mansana	k1gFnSc1	Mansana
<g/>
,	,	kIx,	,
Český	český	k2eAgMnSc1d1	český
čtenář	čtenář	k1gMnSc1	čtenář
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Milada	Milada	k1gFnSc1	Milada
Krausová	Krausová	k1gFnSc1	Krausová
<g/>
,	,	kIx,	,
Na	na	k7c6	na
božích	boží	k2eAgFnPc6d1	boží
cestách	cesta	k1gFnPc6	cesta
<g/>
,	,	kIx,	,
E.	E.	kA	E.
Beaufort	Beaufort	k1gInSc1	Beaufort
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1909	[number]	k4	1909
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Milada	Milada	k1gFnSc1	Milada
Krausová	Krausová	k1gFnSc1	Krausová
<g/>
,	,	kIx,	,
Vlajky	vlajka	k1gFnPc1	vlajka
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
i	i	k8xC	i
přístavem	přístav	k1gInSc7	přístav
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Reis	Reis	k1gInSc1	Reis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1909	[number]	k4	1909
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Pachmayer	Pachmayer	k1gMnSc1	Pachmayer
<g/>
,	,	kIx,	,
Když	když	k8xS	když
réva	réva	k1gFnSc1	réva
znovu	znovu	k6eAd1	znovu
rozkvete	rozkvést	k5eAaPmIp3nS	rozkvést
<g/>
,	,	kIx,	,
Šolc	Šolc	k1gMnSc1	Šolc
a	a	k8xC	a
Šimáček	Šimáček	k1gMnSc1	Šimáček
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Milada	Milada	k1gFnSc1	Milada
Krausová	Krausová	k1gFnSc1	Krausová
<g/>
,	,	kIx,	,
Veselý	Veselý	k1gMnSc1	Veselý
hoch	hoch	k1gMnSc1	hoch
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Milada	Milada	k1gFnSc1	Milada
Krausová	Krausová	k1gFnSc1	Krausová
<g/>
,	,	kIx,	,
Prach	prach	k1gInSc1	prach
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Oktábec	Oktábec	k1gMnSc1	Oktábec
<g/>
,	,	kIx,	,
Laborenus	Laborenus	k1gMnSc1	Laborenus
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Hanuš	Hanuš	k1gMnSc1	Hanuš
Hackenschmied	Hackenschmied	k1gMnSc1	Hackenschmied
<g/>
,	,	kIx,	,
Jednoženství	jednoženství	k1gNnSc1	jednoženství
a	a	k8xC	a
mnohoženství	mnohoženství	k1gNnSc1	mnohoženství
<g/>
,	,	kIx,	,
E.	E.	kA	E.
Beaufort	Beaufort	k1gInSc1	Beaufort
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Hugo	Hugo	k1gMnSc1	Hugo
Kosterka	Kosterka	k1gFnSc1	Kosterka
<g/>
,	,	kIx,	,
Synnöve	Synnöev	k1gFnPc1	Synnöev
Solbaken	Solbaken	k1gInSc1	Solbaken
Československé	československý	k2eAgInPc1d1	československý
podniky	podnik	k1gInPc1	podnik
tiskařské	tiskařský	k2eAgFnSc2d1	tiskařská
a	a	k8xC	a
vydavatelské	vydavatelský	k2eAgFnSc2d1	vydavatelská
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Viktor	Viktor	k1gMnSc1	Viktor
Šuman	Šuman	k1gMnSc1	Šuman
<g/>
,	,	kIx,	,
Rukavička	rukavička	k1gFnSc1	rukavička
<g/>
,	,	kIx,	,
Československé	československý	k2eAgInPc4d1	československý
podniky	podnik	k1gInPc4	podnik
tiskařské	tiskařský	k2eAgInPc4d1	tiskařský
a	a	k8xC	a
vydavatelské	vydavatelský	k2eAgInPc4d1	vydavatelský
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Viktor	Viktor	k1gMnSc1	Viktor
Šuman	Šuman	k1gMnSc1	Šuman
<g/>
,	,	kIx,	,
Vlajky	vlajka	k1gFnPc1	vlajka
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
i	i	k8xC	i
přístavem	přístav	k1gInSc7	přístav
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Hugo	Hugo	k1gMnSc1	Hugo
Kosterka	Kosterka	k1gFnSc1	Kosterka
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Mary	Mary	k1gFnSc1	Mary
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Hugo	Hugo	k1gMnSc1	Hugo
Kosterka	Kosterka	k1gFnSc1	Kosterka
<g/>
,	,	kIx,	,
Rybářské	rybářský	k2eAgNnSc1d1	rybářské
děvče	děvče	k1gNnSc1	děvče
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Milada	Milada	k1gFnSc1	Milada
Krausová	Krausová	k1gFnSc1	Krausová
<g/>
,	,	kIx,	,
Novomanželé	novomanžel	k1gMnPc1	novomanžel
<g/>
,	,	kIx,	,
Nakladatelské	nakladatelský	k2eAgNnSc1d1	nakladatelské
družstvo	družstvo	k1gNnSc1	družstvo
Máje	máje	k1gFnSc1	máje
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
a	a	k8xC	a
úvod	úvod	k1gInSc4	úvod
Milada	Milada	k1gFnSc1	Milada
Krausová	Krausová	k1gFnSc1	Krausová
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Veselý	veselý	k2eAgMnSc1d1	veselý
hoch	hoch	k1gMnSc1	hoch
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Božena	Božena	k1gFnSc1	Božena
Ehrmannová	Ehrmannová	k1gFnSc1	Ehrmannová
<g/>
,	,	kIx,	,
Selské	selský	k2eAgFnPc1d1	selská
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Božena	Božena	k1gFnSc1	Božena
Köllnová	Köllnová	k1gFnSc1	Köllnová
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
B.	B.	kA	B.
Michl	Michl	k1gMnSc1	Michl
<g/>
,	,	kIx,	,
Synnöve	Synnöev	k1gFnPc1	Synnöev
ze	z	k7c2	z
Slunečného	slunečný	k2eAgNnSc2d1	slunečné
návrší	návrší	k1gNnSc2	návrší
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Božena	Božena	k1gFnSc1	Božena
Köllnová	Köllnová	k1gFnSc1	Köllnová
<g/>
,	,	kIx,	,
KRAUS	Kraus	k1gMnSc1	Kraus
<g/>
,	,	kIx,	,
Arnošt	Arnošt	k1gMnSc1	Arnošt
Vilém	Vilém	k1gMnSc1	Vilém
<g/>
.	.	kIx.	.
</s>
<s>
Björnson	Björnson	k1gMnSc1	Björnson
a	a	k8xC	a
Ibsen	Ibsen	k1gMnSc1	Ibsen
<g/>
:	:	kIx,	:
kurs	kurs	k1gInSc1	kurs
šestipřednáškový	šestipřednáškový	k2eAgInSc1d1	šestipřednáškový
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
152	[number]	k4	152
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bjø	Bjø	k1gFnSc2	Bjø
Bjø	Bjø	k1gFnSc2	Bjø
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Autor	autor	k1gMnSc1	autor
Bjø	Bjø	k1gMnSc1	Bjø
Bjø	Bjø	k1gMnSc1	Bjø
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Bjø	Bjø	k1gFnSc1	Bjø
Bjø	Bjø	k1gFnSc1	Bjø
Osoba	osoba	k1gFnSc1	osoba
Bjø	Bjø	k1gFnSc1	Bjø
Bjø	Bjø	k1gFnSc1	Bjø
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Bjø	Bjø	k1gMnSc2	Bjø
Bjø	Bjø	k1gMnSc2	Bjø
-	-	kIx~	-
Biography	Biographa	k1gMnSc2	Biographa
Björnstjerne	Björnstjern	k1gInSc5	Björnstjern
Martinus	Martinus	k1gInSc4	Martinus
Björnson	Björnson	k1gMnSc1	Björnson
Biography	Biographa	k1gFnSc2	Biographa
from	from	k1gMnSc1	from
the	the	k?	the
Norwegian	Norwegian	k1gMnSc1	Norwegian
Ministry	ministr	k1gMnPc4	ministr
of	of	k?	of
Foreign	Foreign	k1gInSc4	Foreign
Affairs	Affairsa	k1gFnPc2	Affairsa
Britannica	Britannic	k1gInSc2	Britannic
Online	Onlin	k1gMnSc5	Onlin
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
</s>
