<s>
Pelíšky	pelíšek	k1gInPc7	pelíšek
jsou	být	k5eAaImIp3nP	být
česká	český	k2eAgFnSc1d1	Česká
komedie	komedie	k1gFnSc1	komedie
režiséra	režisér	k1gMnSc2	režisér
Jana	Jana	k1gFnSc1	Jana
Hřebejka	Hřebejka	k1gFnSc1	Hřebejka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc4	film
vyprávěný	vyprávěný	k2eAgInSc4d1	vyprávěný
očima	oko	k1gNnPc7	oko
dospívajícího	dospívající	k2eAgMnSc4d1	dospívající
chlapce	chlapec	k1gMnSc4	chlapec
(	(	kIx(	(
<g/>
Michael	Michael	k1gMnSc1	Michael
Beran	Beran	k1gMnSc1	Beran
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
chlapce	chlapec	k1gMnSc2	chlapec
(	(	kIx(	(
<g/>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Donutil	donutit	k5eAaPmAgMnS	donutit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
voják	voják	k1gMnSc1	voják
<g/>
,	,	kIx,	,
komunista	komunista	k1gMnSc1	komunista
a	a	k8xC	a
příliš	příliš	k6eAd1	příliš
svému	svůj	k3xOyFgMnSc3	svůj
synovi	syn	k1gMnSc3	syn
nerozumí	rozumět	k5eNaImIp3nS	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
Soused	soused	k1gMnSc1	soused
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Kodet	Kodet	k1gMnSc1	Kodet
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
voják	voják	k1gMnSc1	voják
<g/>
,	,	kIx,	,
nesnáší	snášet	k5eNaImIp3nS	snášet
komunisty	komunista	k1gMnPc7	komunista
a	a	k8xC	a
své	svůj	k3xOyFgFnSc3	svůj
dceři	dcera	k1gFnSc3	dcera
rovněž	rovněž	k9	rovněž
nerozumí	rozumět	k5eNaImIp3nS	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
generace	generace	k1gFnSc1	generace
má	mít	k5eAaImIp3nS	mít
jiné	jiný	k2eAgInPc4d1	jiný
ideály	ideál	k1gInPc4	ideál
než	než	k8xS	než
generace	generace	k1gFnPc4	generace
předchozí	předchozí	k2eAgFnPc4d1	předchozí
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgInPc1	tři
České	český	k2eAgInPc1d1	český
lvy	lev	k1gInPc1	lev
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
hlavní	hlavní	k2eAgInSc4d1	hlavní
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
Jiřího	Jiří	k1gMnSc2	Jiří
Kodeta	Kodet	k1gMnSc2	Kodet
<g/>
,	,	kIx,	,
plakát	plakát	k1gInSc1	plakát
a	a	k8xC	a
divácky	divácky	k6eAd1	divácky
nejúspěšnější	úspěšný	k2eAgInSc1d3	nejúspěšnější
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Ivy	Iva	k1gFnSc2	Iva
Janžurové	Janžurový	k2eAgFnSc2d1	Janžurová
byla	být	k5eAaImAgFnS	být
role	role	k1gFnPc4	role
matky	matka	k1gFnSc2	matka
Krausové	Krausové	k2eAgFnPc1d1	Krausové
nejprve	nejprve	k6eAd1	nejprve
nabídnuta	nabídnut	k2eAgNnPc4d1	nabídnuto
jí	on	k3xPp3gFnSc2	on
<g/>
;	;	kIx,	;
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
trvala	trvat	k5eAaImAgFnS	trvat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
její	její	k3xOp3gFnSc1	její
postava	postava	k1gFnSc1	postava
nezemřela	zemřít	k5eNaPmAgFnS	zemřít
<g/>
,	,	kIx,	,
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
Petr	Petr	k1gMnSc1	Petr
Jarchovský	Jarchovský	k2eAgMnSc1d1	Jarchovský
nebyl	být	k5eNaImAgMnS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
roli	role	k1gFnSc3	role
přepsat	přepsat	k5eAaPmF	přepsat
<g/>
,	,	kIx,	,
ke	k	k7c3	k
spolupráci	spolupráce	k1gFnSc3	spolupráce
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Realizace	realizace	k1gFnSc1	realizace
filmu	film	k1gInSc2	film
trvala	trvat	k5eAaImAgFnS	trvat
6	[number]	k4	6
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
mělo	mít	k5eAaImAgNnS	mít
jít	jít	k5eAaImF	jít
o	o	k7c4	o
dvoudílný	dvoudílný	k2eAgInSc4d1	dvoudílný
televizní	televizní	k2eAgInSc4d1	televizní
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Rodinná	rodinný	k2eAgFnSc1d1	rodinná
vila	vila	k1gFnSc1	vila
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Schodová	schodový	k2eAgFnSc1d1	Schodová
4	[number]	k4	4
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
-	-	kIx~	-
Košířích	Košíře	k1gInPc6	Košíře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Prokopském	prokopský	k2eAgNnSc6d1	Prokopské
údolí	údolí	k1gNnSc6	údolí
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jezírko	jezírko	k1gNnSc4	jezírko
s	s	k7c7	s
"	"	kIx"	"
<g/>
mužem	muž	k1gMnSc7	muž
s	s	k7c7	s
koženou	kožený	k2eAgFnSc7d1	kožená
brašnou	brašna	k1gFnSc7	brašna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pelíšky	pelíšek	k1gInPc1	pelíšek
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
Pelíšky	pelíšek	k1gInPc4	pelíšek
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
Pelíšky	pelíšek	k1gInPc4	pelíšek
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Místa	místo	k1gNnSc2	místo
natáčení	natáčení	k1gNnSc2	natáčení
na	na	k7c4	na
Filmová	filmový	k2eAgNnPc4d1	filmové
místa	místo	k1gNnPc4	místo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
