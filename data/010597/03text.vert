<p>
<s>
Žirafa	žirafa	k1gFnSc1	žirafa
(	(	kIx(	(
<g/>
Giraffa	Giraff	k1gMnSc2	Giraff
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
mohutných	mohutný	k2eAgMnPc2d1	mohutný
sudokopytníků	sudokopytník	k1gMnPc2	sudokopytník
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
suchozemských	suchozemský	k2eAgInPc2d1	suchozemský
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
zprávy	zpráva	k1gFnSc2	zpráva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
upozornění	upozornění	k1gNnSc1	upozornění
přírodovědce	přírodovědec	k1gMnSc2	přírodovědec
Davida	David	k1gMnSc2	David
Attenborougha	Attenborough	k1gMnSc2	Attenborough
jsou	být	k5eAaImIp3nP	být
žirafy	žirafa	k1gFnPc1	žirafa
velice	velice	k6eAd1	velice
ohroženým	ohrožený	k2eAgInSc7d1	ohrožený
druhem	druh	k1gInSc7	druh
<g/>
,	,	kIx,	,
v	v	k7c6	v
7	[number]	k4	7
státech	stát	k1gInPc6	stát
světa	svět	k1gInSc2	svět
již	již	k6eAd1	již
dokonce	dokonce	k9	dokonce
vyhynuly	vyhynout	k5eAaPmAgFnP	vyhynout
<g/>
.	.	kIx.	.
<g/>
Žirafa	žirafa	k1gFnSc1	žirafa
je	být	k5eAaImIp3nS	být
přežvýkavec	přežvýkavec	k1gMnSc1	přežvýkavec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
geneticky	geneticky	k6eAd1	geneticky
příbuzná	příbuzný	k2eAgFnSc1d1	příbuzná
s	s	k7c7	s
jelenem	jelen	k1gMnSc7	jelen
či	či	k8xC	či
krávou	kráva	k1gFnSc7	kráva
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
taxonomického	taxonomický	k2eAgNnSc2d1	taxonomické
hlediska	hledisko	k1gNnSc2	hledisko
náleží	náležet	k5eAaImIp3nS	náležet
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
přežvýkavců	přežvýkavec	k1gMnPc2	přežvýkavec
do	do	k7c2	do
vlastní	vlastní	k2eAgFnSc2d1	vlastní
čeledi	čeleď	k1gFnSc2	čeleď
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
sdílí	sdílet	k5eAaImIp3nS	sdílet
pouze	pouze	k6eAd1	pouze
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnPc3	svůj
nejbližším	blízký	k2eAgMnPc3d3	nejbližší
příbuzným	příbuzný	k1gMnPc3	příbuzný
okapi	okapi	k1gFnSc2	okapi
<g/>
.	.	kIx.	.
</s>
<s>
Žirafí	žirafí	k2eAgMnPc1d1	žirafí
samci	samec	k1gMnPc1	samec
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
velikosti	velikost	k1gFnSc3	velikost
od	od	k7c2	od
4,8	[number]	k4	4,8
až	až	k9	až
do	do	k7c2	do
5,5	[number]	k4	5,5
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
váží	vážit	k5eAaImIp3nP	vážit
až	až	k9	až
900	[number]	k4	900
kilogramů	kilogram	k1gInPc2	kilogram
(	(	kIx(	(
<g/>
rekordní	rekordní	k2eAgMnSc1d1	rekordní
jedinec	jedinec	k1gMnSc1	jedinec
<g/>
,	,	kIx,	,
zastřelený	zastřelený	k2eAgInSc1d1	zastřelený
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
v	v	k7c6	v
Keni	Keňa	k1gFnSc6	Keňa
byl	být	k5eAaImAgMnS	být
vysoký	vysoký	k2eAgMnSc1d1	vysoký
5,87	[number]	k4	5,87
metru	metr	k1gInSc2	metr
a	a	k8xC	a
vážil	vážit	k5eAaImAgInS	vážit
zhruba	zhruba	k6eAd1	zhruba
1000	[number]	k4	1000
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
trochu	trochu	k6eAd1	trochu
menší	malý	k2eAgMnSc1d2	menší
a	a	k8xC	a
lehčí	lehký	k2eAgMnSc1d2	lehčí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tradičně	tradičně	k6eAd1	tradičně
byla	být	k5eAaImAgFnS	být
žirafa	žirafa	k1gFnSc1	žirafa
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
druh	druh	k1gInSc4	druh
(	(	kIx(	(
<g/>
Giraffa	Giraff	k1gMnSc2	Giraff
camelopardalis	camelopardalis	k1gFnSc2	camelopardalis
<g/>
)	)	kIx)	)
s	s	k7c7	s
11	[number]	k4	11
poddruhy	poddruh	k1gInPc7	poddruh
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
uznává	uznávat	k5eAaImIp3nS	uznávat
devět	devět	k4xCc1	devět
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
ale	ale	k8xC	ale
přinesla	přinést	k5eAaPmAgFnS	přinést
zjištění	zjištění	k1gNnSc4	zjištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
druhy	druh	k1gInPc1	druh
žiraf	žirafa	k1gFnPc2	žirafa
jsou	být	k5eAaImIp3nP	být
čtyři	čtyři	k4xCgInPc1	čtyři
<g/>
:	:	kIx,	:
žirafa	žirafa	k1gFnSc1	žirafa
severní	severní	k2eAgFnSc1d1	severní
<g/>
,	,	kIx,	,
žirafa	žirafa	k1gFnSc1	žirafa
jižní	jižní	k2eAgFnSc1d1	jižní
<g/>
,	,	kIx,	,
žirafa	žirafa	k1gFnSc1	žirafa
masajská	masajský	k2eAgFnSc1d1	masajská
a	a	k8xC	a
žirafa	žirafa	k1gFnSc1	žirafa
síťovaná	síťovaný	k2eAgFnSc1d1	síťovaná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pojmenování	pojmenování	k1gNnPc1	pojmenování
==	==	k?	==
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
žirafa	žirafa	k1gFnSc1	žirafa
<g/>
,	,	kIx,	,
používané	používaný	k2eAgInPc1d1	používaný
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
obměnách	obměna	k1gFnPc6	obměna
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
evropských	evropský	k2eAgInPc2d1	evropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přejímkou	přejímka	k1gFnSc7	přejímka
z	z	k7c2	z
arabského	arabský	k2eAgInSc2d1	arabský
názvu	název	k1gInSc2	název
tohoto	tento	k3xDgNnSc2	tento
zvířete	zvíře	k1gNnSc2	zvíře
zarafa	zaraf	k1gMnSc4	zaraf
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
má	mít	k5eAaImIp3nS	mít
zřejmě	zřejmě	k6eAd1	zřejmě
původ	původ	k1gInSc4	původ
v	v	k7c6	v
somálském	somálský	k2eAgInSc6d1	somálský
názvu	název	k1gInSc6	název
žirafy	žirafa	k1gFnSc2	žirafa
gerri	gerr	k1gFnSc2	gerr
<g/>
.	.	kIx.	.
</s>
<s>
Latinský	latinský	k2eAgInSc1d1	latinský
název	název	k1gInSc1	název
camelopardus	camelopardus	k1gInSc1	camelopardus
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
a	a	k8xC	a
středověku	středověk	k1gInSc2	středověk
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
antické	antický	k2eAgFnSc2d1	antická
legendy	legenda	k1gFnSc2	legenda
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
je	být	k5eAaImIp3nS	být
žirafa	žirafa	k1gFnSc1	žirafa
křížencem	kříženec	k1gMnSc7	kříženec
velblouda	velbloud	k1gMnSc2	velbloud
-	-	kIx~	-
camelus	camelus	k1gMnSc1	camelus
a	a	k8xC	a
levharta	levhart	k1gMnSc2	levhart
-	-	kIx~	-
pardus	pardus	k1gInSc1	pardus
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
má	mít	k5eAaImIp3nS	mít
postavu	postava	k1gFnSc4	postava
velblouda	velbloud	k1gMnSc2	velbloud
a	a	k8xC	a
zbarvení	zbarvení	k1gNnSc1	zbarvení
skvrnité	skvrnitý	k2eAgNnSc1d1	skvrnité
jako	jako	k8xC	jako
levhart	levhart	k1gMnSc1	levhart
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgInSc1d1	jiný
starověký	starověký	k2eAgInSc1d1	starověký
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
anabus	anabus	k1gInSc4	anabus
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Kartáginců	Kartáginec	k1gMnPc2	Kartáginec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jej	on	k3xPp3gMnSc4	on
zřejmě	zřejmě	k6eAd1	zřejmě
převzali	převzít	k5eAaPmAgMnP	převzít
z	z	k7c2	z
khoisanských	khoisanský	k2eAgInPc2d1	khoisanský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Africe	Afrika	k1gFnSc6	Afrika
je	být	k5eAaImIp3nS	být
svahilsky	svahilsky	k6eAd1	svahilsky
nazývána	nazýván	k2eAgFnSc1d1	nazývána
twiga	twiga	k1gFnSc1	twiga
(	(	kIx(	(
<g/>
toto	tento	k3xDgNnSc4	tento
jméno	jméno	k1gNnSc4	jméno
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
pseudonym	pseudonym	k1gInSc4	pseudonym
modelky	modelka	k1gFnSc2	modelka
Twiggy	Twigga	k1gFnSc2	Twigga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kikujové	Kikujový	k2eAgFnSc3d1	Kikujový
žirafě	žirafa	k1gFnSc3	žirafa
říkají	říkat	k5eAaImIp3nP	říkat
nduida	nduid	k1gMnSc4	nduid
<g/>
,	,	kIx,	,
Hehové	Hehové	k2eAgFnSc4d1	Hehové
nudululu	nudulula	k1gFnSc4	nudulula
<g/>
,	,	kIx,	,
Masajové	Masajový	k2eAgFnPc1d1	Masajový
oloodo-kirragata	oloodoirragata	k1gFnSc1	oloodo-kirragata
a	a	k8xC	a
Šonové	Šonové	k2eAgFnSc1d1	Šonové
twiza	twiza	k1gFnSc1	twiza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Africe	Afrika	k1gFnSc6	Afrika
ji	on	k3xPp3gFnSc4	on
Jorubové	Joruba	k1gMnPc1	Joruba
nazývají	nazývat	k5eAaImIp3nP	nazývat
agunfon	agunfon	k1gInSc4	agunfon
a	a	k8xC	a
Hausové	Hausové	k2eAgFnSc1d1	Hausové
raƙ	raƙ	k?	raƙ
dawa	dawa	k1gFnSc1	dawa
<g/>
,	,	kIx,	,
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Africe	Afrika	k1gFnSc6	Afrika
ji	on	k3xPp3gFnSc4	on
Lingalové	Lingalová	k1gFnSc6	Lingalová
říkají	říkat	k5eAaImIp3nP	říkat
dikala	dikala	k1gFnSc1	dikala
a	a	k8xC	a
Loziové	Lozius	k1gMnPc1	Lozius
tutwa	tutwum	k1gNnSc2	tutwum
<g/>
,	,	kIx,	,
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
má	mít	k5eAaImIp3nS	mít
zulské	zulský	k2eAgNnSc4d1	zulský
jméno	jméno	k1gNnSc4	jméno
indlulamithi	indlulamith	k1gFnSc2	indlulamith
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Sothové	Soth	k1gMnPc1	Soth
<g/>
,	,	kIx,	,
Tswanové	Tswan	k1gMnPc1	Tswan
a	a	k8xC	a
Vendové	Vend	k1gMnPc1	Vend
ji	on	k3xPp3gFnSc4	on
nazývají	nazývat	k5eAaImIp3nP	nazývat
thuda	thudo	k1gNnPc1	thudo
<g/>
,	,	kIx,	,
thutlwa	thutlwum	k1gNnPc1	thutlwum
a	a	k8xC	a
Khoikhoiové	Khoikhoiový	k2eAgFnPc1d1	Khoikhoiový
!	!	kIx.	!
</s>
<s>
<g/>
anabu	anabat	k5eAaPmIp1nS	anabat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
chování	chování	k1gNnSc2	chování
==	==	k?	==
</s>
</p>
<p>
<s>
Přirozeným	přirozený	k2eAgNnSc7d1	přirozené
prostředím	prostředí	k1gNnSc7	prostředí
pro	pro	k7c4	pro
žirafy	žirafa	k1gFnPc4	žirafa
jsou	být	k5eAaImIp3nP	být
suché	suchý	k2eAgFnPc4d1	suchá
savany	savana	k1gFnPc4	savana
a	a	k8xC	a
otevřené	otevřený	k2eAgFnPc4d1	otevřená
pláně	pláň	k1gFnPc4	pláň
s	s	k7c7	s
řídkým	řídký	k2eAgNnSc7d1	řídké
stromovím	stromoví	k1gNnSc7	stromoví
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
v	v	k7c6	v
polopouštích	polopoušť	k1gFnPc6	polopoušť
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
řídkých	řídký	k2eAgInPc6d1	řídký
lesích	les	k1gInPc6	les
a	a	k8xC	a
buši	buš	k1gInSc6	buš
<g/>
.	.	kIx.	.
</s>
<s>
Spásá	spásat	k5eAaImIp3nS	spásat
vegetaci	vegetace	k1gFnSc4	vegetace
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
polohách	poloha	k1gFnPc6	poloha
než	než	k8xS	než
všichni	všechen	k3xTgMnPc1	všechen
ostatní	ostatní	k2eAgMnPc1d1	ostatní
savci	savec	k1gMnPc1	savec
<g/>
,	,	kIx,	,
především	především	k9	především
listy	list	k1gInPc1	list
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
keřů	keř	k1gInPc2	keř
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
akácie	akácie	k1gFnPc1	akácie
nebo	nebo	k8xC	nebo
keře	keř	k1gInPc1	keř
rodu	rod	k1gInSc2	rod
Commiphora	Commiphor	k1gMnSc2	Commiphor
<g/>
,	,	kIx,	,
Terminalia	Terminalius	k1gMnSc2	Terminalius
a	a	k8xC	a
Premna	Premn	k1gMnSc2	Premn
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
dlouhému	dlouhý	k2eAgInSc3d1	dlouhý
jazyku	jazyk	k1gInSc3	jazyk
<g/>
,	,	kIx,	,
šikovné	šikovný	k2eAgFnSc6d1	šikovná
stavbě	stavba	k1gFnSc6	stavba
páteře	páteř	k1gFnSc2	páteř
a	a	k8xC	a
ohebným	ohebný	k2eAgFnPc3d1	ohebná
nohám	noha	k1gFnPc3	noha
může	moct	k5eAaImIp3nS	moct
žirafa	žirafa	k1gFnSc1	žirafa
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
na	na	k7c4	na
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Typicky	typicky	k6eAd1	typicky
si	se	k3xPyFc3	se
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
jazykem	jazyk	k1gInSc7	jazyk
přitáhne	přitáhnout	k5eAaPmIp3nS	přitáhnout
větev	větev	k1gFnSc1	větev
a	a	k8xC	a
poté	poté	k6eAd1	poté
oddálí	oddálit	k5eAaPmIp3nS	oddálit
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
si	se	k3xPyFc3	se
shrábne	shrábnout	k5eAaPmIp3nS	shrábnout
listí	listí	k1gNnSc1	listí
hřebenovitými	hřebenovitý	k2eAgInPc7d1	hřebenovitý
zuby	zub	k1gInPc7	zub
<g/>
.	.	kIx.	.
</s>
<s>
Tuhý	tuhý	k2eAgInSc1d1	tuhý
jazyk	jazyk	k1gInSc1	jazyk
jí	on	k3xPp3gFnSc2	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
strhávat	strhávat	k5eAaImF	strhávat
větvičky	větvička	k1gFnPc4	větvička
a	a	k8xC	a
listy	list	k1gInPc4	list
i	i	k9	i
z	z	k7c2	z
trnitých	trnitý	k2eAgInPc2d1	trnitý
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
akácie	akácie	k1gFnPc1	akácie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pití	pití	k1gNnSc6	pití
žirafa	žirafa	k1gFnSc1	žirafa
sklání	sklánět	k5eAaImIp3nS	sklánět
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
doširoka	doširoka	k6eAd1	doširoka
rozkročí	rozkročit	k5eAaPmIp3nP	rozkročit
přední	přední	k2eAgFnPc4d1	přední
končetiny	končetina	k1gFnPc4	končetina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
lépe	dobře	k6eAd2	dobře
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
k	k	k7c3	k
vodě	voda	k1gFnSc3	voda
<g/>
.	.	kIx.	.
</s>
<s>
Konzumace	konzumace	k1gFnSc1	konzumace
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
pití	pití	k1gNnSc1	pití
zabere	zabrat	k5eAaPmIp3nS	zabrat
žirafě	žirafa	k1gFnSc3	žirafa
denně	denně	k6eAd1	denně
přibližně	přibližně	k6eAd1	přibližně
dvanáct	dvanáct	k4xCc4	dvanáct
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
žirafy	žirafa	k1gFnPc1	žirafa
kvalitní	kvalitní	k2eAgNnSc1d1	kvalitní
krmivo	krmivo	k1gNnSc1	krmivo
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
vojtěšku	vojtěška	k1gFnSc4	vojtěška
<g/>
,	,	kIx,	,
luční	luční	k2eAgNnSc4d1	luční
seno	seno	k1gNnSc4	seno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
granule	granule	k1gFnPc1	granule
a	a	k8xC	a
jablka	jablko	k1gNnPc1	jablko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žirafy	žirafa	k1gFnPc1	žirafa
odpočívají	odpočívat	k5eAaImIp3nP	odpočívat
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
ostatních	ostatní	k2eAgMnPc2d1	ostatní
sudokopytníků	sudokopytník	k1gMnPc2	sudokopytník
<g/>
,	,	kIx,	,
ve	v	k7c4	v
stoje	stoj	k1gInPc4	stoj
<g/>
,	,	kIx,	,
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
spí	spát	k5eAaImIp3nP	spát
vsedě	vsedě	k6eAd1	vsedě
s	s	k7c7	s
hlavou	hlava	k1gFnSc7	hlava
opřenou	opřený	k2eAgFnSc7d1	opřená
o	o	k7c4	o
zadek	zadek	k1gInSc4	zadek
<g/>
.	.	kIx.	.
</s>
<s>
Spánku	spánek	k1gInSc2	spánek
žirafa	žirafa	k1gFnSc1	žirafa
věnuje	věnovat	k5eAaPmIp3nS	věnovat
průměrně	průměrně	k6eAd1	průměrně
4-6	[number]	k4	4-6
hodin	hodina	k1gFnPc2	hodina
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
<g/>
Žirafa	žirafa	k1gFnSc1	žirafa
se	se	k3xPyFc4	se
zvukově	zvukově	k6eAd1	zvukově
projevuje	projevovat	k5eAaImIp3nS	projevovat
funěním	funění	k1gNnSc7	funění
<g/>
,	,	kIx,	,
starší	starý	k2eAgNnPc4d2	starší
zvířata	zvíře	k1gNnPc4	zvíře
a	a	k8xC	a
mláďata	mládě	k1gNnPc4	mládě
bečí	bečet	k5eAaImIp3nS	bečet
<g/>
.	.	kIx.	.
</s>
<s>
Dokáží	dokázat	k5eAaPmIp3nP	dokázat
běžet	běžet	k5eAaImF	běžet
rychlostí	rychlost	k1gFnSc7	rychlost
až	až	k9	až
51	[number]	k4	51
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
</s>
</p>
<p>
<s>
Žirafy	žirafa	k1gFnPc1	žirafa
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
smíšených	smíšený	k2eAgFnPc6d1	smíšená
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
20-50	[number]	k4	20-50
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
samice	samice	k1gFnPc1	samice
s	s	k7c7	s
mláďaty	mládě	k1gNnPc7	mládě
pohromadě	pohromadě	k6eAd1	pohromadě
se	s	k7c7	s
samci	samec	k1gMnPc7	samec
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgFnPc1d1	sociální
vazby	vazba	k1gFnPc1	vazba
mezi	mezi	k7c7	mezi
členy	člen	k1gMnPc7	člen
stáda	stádo	k1gNnSc2	stádo
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
slabé	slabý	k2eAgFnPc4d1	slabá
a	a	k8xC	a
zvířata	zvíře	k1gNnPc1	zvíře
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
stády	stádo	k1gNnPc7	stádo
volně	volně	k6eAd1	volně
přemisťují	přemisťovat	k5eAaImIp3nP	přemisťovat
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
žirafa	žirafa	k1gFnSc1	žirafa
za	za	k7c4	za
život	život	k1gInSc4	život
vystřídá	vystřídat	k5eAaPmIp3nS	vystřídat
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Žirafy	žirafa	k1gFnPc1	žirafa
nemají	mít	k5eNaImIp3nP	mít
stálá	stálý	k2eAgNnPc4d1	stálé
teritoria	teritorium	k1gNnPc4	teritorium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Anatomie	anatomie	k1gFnSc2	anatomie
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
význačné	význačný	k2eAgInPc4d1	význačný
znaky	znak	k1gInPc4	znak
patří	patřit	k5eAaImIp3nS	patřit
velké	velký	k2eAgNnSc1d1	velké
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
uši	ucho	k1gNnPc4	ucho
<g/>
,	,	kIx,	,
chůdovité	chůdovitý	k2eAgFnPc4d1	chůdovitý
nohy	noha	k1gFnPc4	noha
s	s	k7c7	s
velkými	velký	k2eAgNnPc7d1	velké
chodidly	chodidlo	k1gNnPc7	chodidlo
a	a	k8xC	a
tenký	tenký	k2eAgInSc4d1	tenký
ocas	ocas	k1gInSc4	ocas
s	s	k7c7	s
oháňkou	oháňka	k1gFnSc7	oháňka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
například	například	k6eAd1	například
k	k	k7c3	k
odhánění	odhánění	k1gNnSc3	odhánění
much	moucha	k1gFnPc2	moucha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
má	mít	k5eAaImIp3nS	mít
žirafa	žirafa	k1gFnSc1	žirafa
dva	dva	k4xCgInPc4	dva
až	až	k9	až
čtyři	čtyři	k4xCgInPc4	čtyři
růžky	růžek	k1gInPc4	růžek
(	(	kIx(	(
<g/>
osikony	osikon	k1gInPc4	osikon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
větší	veliký	k2eAgFnSc2d2	veliký
než	než	k8xS	než
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
růžky	růžek	k1gInPc1	růžek
jsou	být	k5eAaImIp3nP	být
zpočátku	zpočátku	k6eAd1	zpočátku
chrupavčité	chrupavčitý	k2eAgInPc1d1	chrupavčitý
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
zkostnatí	zkostnatět	k5eAaPmIp3nS	zkostnatět
<g/>
.	.	kIx.	.
</s>
<s>
Žádné	žádný	k3yNgFnPc1	žádný
dvě	dva	k4xCgFnPc1	dva
žirafy	žirafa	k1gFnPc1	žirafa
nemají	mít	k5eNaImIp3nP	mít
stejné	stejný	k2eAgFnPc1d1	stejná
kresby	kresba	k1gFnPc1	kresba
na	na	k7c6	na
srsti	srst	k1gFnSc6	srst
<g/>
.	.	kIx.	.
</s>
<s>
Hojně	hojně	k6eAd1	hojně
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
krční	krční	k2eAgFnSc1d1	krční
páteř	páteř	k1gFnSc1	páteř
žirafy	žirafa	k1gFnSc2	žirafa
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
pouze	pouze	k6eAd1	pouze
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
obratlů	obratel	k1gInPc2	obratel
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
ostatních	ostatní	k2eAgMnPc2d1	ostatní
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
chybný	chybný	k2eAgInSc1d1	chybný
<g/>
.	.	kIx.	.
</s>
<s>
Žirafa	žirafa	k1gFnSc1	žirafa
má	mít	k5eAaImIp3nS	mít
totiž	totiž	k9	totiž
jeden	jeden	k4xCgInSc4	jeden
krční	krční	k2eAgInSc4d1	krční
obratel	obratel	k1gInSc4	obratel
navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jeden	jeden	k4xCgInSc1	jeden
hrudní	hrudní	k2eAgInSc1d1	hrudní
jí	on	k3xPp3gFnSc3	on
vymizel	vymizet	k5eAaPmAgMnS	vymizet
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zřejmě	zřejmě	k6eAd1	zřejmě
mnohé	mnohý	k2eAgMnPc4d1	mnohý
badatele	badatel	k1gMnPc4	badatel
zmátlo	zmást	k5eAaPmAgNnS	zmást
<g/>
.	.	kIx.	.
</s>
<s>
Cévní	cévní	k2eAgInSc1d1	cévní
systém	systém	k1gInSc1	systém
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
bránit	bránit	k5eAaImF	bránit
průtoku	průtok	k1gInSc2	průtok
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
při	při	k7c6	při
sklonění	sklonění	k1gNnSc6	sklonění
hlavy	hlava	k1gFnSc2	hlava
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
mozku	mozek	k1gInSc2	mozek
hydrostatickým	hydrostatický	k2eAgInSc7d1	hydrostatický
tlakem	tlak	k1gInSc7	tlak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Žirafí	žirafí	k2eAgInSc1d1	žirafí
krk	krk	k1gInSc1	krk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
evoluce	evoluce	k1gFnSc1	evoluce
===	===	k?	===
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
příklad	příklad	k1gInSc1	příklad
lamarkistické	lamarkistický	k2eAgFnSc2d1	lamarkistický
evoluce	evoluce	k1gFnSc2	evoluce
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
uvádí	uvádět	k5eAaImIp3nS	uvádět
prodlužování	prodlužování	k1gNnSc3	prodlužování
žirafího	žirafí	k2eAgInSc2d1	žirafí
krku	krk	k1gInSc2	krk
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
byly	být	k5eAaImAgFnP	být
žirafy	žirafa	k1gFnPc1	žirafa
nejprve	nejprve	k6eAd1	nejprve
zvířata	zvíře	k1gNnPc1	zvíře
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
krátkým	krátký	k2eAgInSc7d1	krátký
krkem	krk	k1gInSc7	krk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
postupným	postupný	k2eAgNnSc7d1	postupné
natahováním	natahování	k1gNnSc7	natahování
během	během	k7c2	během
generací	generace	k1gFnPc2	generace
zvětšil	zvětšit	k5eAaPmAgInS	zvětšit
až	až	k9	až
na	na	k7c4	na
současnou	současný	k2eAgFnSc4d1	současná
<g/>
,	,	kIx,	,
optimální	optimální	k2eAgFnSc4d1	optimální
velikost	velikost	k1gFnSc4	velikost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velikost	velikost	k1gFnSc1	velikost
žirafího	žirafí	k2eAgInSc2d1	žirafí
krku	krk	k1gInSc2	krk
je	být	k5eAaImIp3nS	být
však	však	k9	však
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
výsledkem	výsledek	k1gInSc7	výsledek
pohlavního	pohlavní	k2eAgInSc2d1	pohlavní
výběru	výběr	k1gInSc2	výběr
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
a	a	k8xC	a
mohutnějším	mohutný	k2eAgInSc7d2	mohutnější
krkem	krk	k1gInSc7	krk
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
soubojích	souboj	k1gInPc6	souboj
úspěšnější	úspěšný	k2eAgFnSc1d2	úspěšnější
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
větší	veliký	k2eAgInSc4d2	veliký
úspěch	úspěch	k1gInSc4	úspěch
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
selekční	selekční	k2eAgInSc4d1	selekční
tlak	tlak	k1gInSc4	tlak
na	na	k7c4	na
samce	samec	k1gMnSc4	samec
s	s	k7c7	s
mohutným	mohutný	k2eAgInSc7d1	mohutný
krkem	krk	k1gInSc7	krk
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
hlava	hlava	k1gFnSc1	hlava
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
často	často	k6eAd1	často
výše	vysoce	k6eAd2	vysoce
<g/>
,	,	kIx,	,
než	než	k8xS	než
větve	větev	k1gFnPc1	větev
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Často	často	k6eAd1	často
uváděný	uváděný	k2eAgInSc1d1	uváděný
doklad	doklad	k1gInSc1	doklad
neschopnosti	neschopnost	k1gFnSc2	neschopnost
evoluce	evoluce	k1gFnSc2	evoluce
předvídat	předvídat	k5eAaImF	předvídat
dopředu	dopředu	k6eAd1	dopředu
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
je	být	k5eAaImIp3nS	být
inervován	inervován	k2eAgInSc1d1	inervován
larynx	larynx	k1gInSc1	larynx
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Rekurentní	rekurentní	k2eAgInSc1d1	rekurentní
laryngiální	laryngiální	k2eAgInSc1d1	laryngiální
nerv	nerv	k1gInSc1	nerv
je	být	k5eAaImIp3nS	být
veden	vést	k5eAaImNgInS	vést
po	po	k7c6	po
zadní	zadní	k2eAgFnSc6d1	zadní
straně	strana	k1gFnSc6	strana
aorty	aorta	k1gFnSc2	aorta
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
hrtanu	hrtan	k1gInSc2	hrtan
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
žiraf	žirafa	k1gFnPc2	žirafa
to	ten	k3xDgNnSc1	ten
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
několikametrovou	několikametrový	k2eAgFnSc4d1	několikametrová
smyčku	smyčka	k1gFnSc4	smyčka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
kolem	kolem	k7c2	kolem
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
pak	pak	k6eAd1	pak
stoupá	stoupat	k5eAaImIp3nS	stoupat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
krku	krk	k1gInSc2	krk
žirafy	žirafa	k1gFnSc2	žirafa
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
"	"	kIx"	"
<g/>
evoluční	evoluční	k2eAgFnSc1d1	evoluční
fušeřina	fušeřina	k1gFnSc1	fušeřina
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
evolutionary	evolutionar	k1gInPc4	evolutionar
tinkering	tinkering	k1gInSc1	tinkering
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc1	druh
a	a	k8xC	a
poddruhy	poddruh	k1gInPc1	poddruh
==	==	k?	==
</s>
</p>
<p>
<s>
Obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
uznáváno	uznáván	k2eAgNnSc1d1	uznáváno
9	[number]	k4	9
poddruhů	poddruh	k1gInPc2	poddruh
žiraf	žirafa	k1gFnPc2	žirafa
<g/>
,	,	kIx,	,
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
se	se	k3xPyFc4	se
především	především	k9	především
barvou	barva	k1gFnSc7	barva
<g/>
,	,	kIx,	,
vzorem	vzor	k1gInSc7	vzor
a	a	k8xC	a
místem	místo	k1gNnSc7	místo
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Vzor	vzor	k1gInSc1	vzor
skvrn	skvrna	k1gFnPc2	skvrna
je	být	k5eAaImIp3nS	být
dědičný	dědičný	k2eAgInSc1d1	dědičný
<g/>
.	.	kIx.	.
</s>
<s>
Poddruhy	poddruh	k1gInPc4	poddruh
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
severní	severní	k2eAgFnPc4d1	severní
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
bílou	bílý	k2eAgFnSc4d1	bílá
až	až	k9	až
našedlou	našedlý	k2eAgFnSc4d1	našedlá
podkladovou	podkladový	k2eAgFnSc4d1	podkladová
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
vždy	vždy	k6eAd1	vždy
bílé	bílý	k2eAgFnPc1d1	bílá
nohy	noha	k1gFnPc1	noha
a	a	k8xC	a
jižní	jižní	k2eAgInPc1d1	jižní
<g/>
,	,	kIx,	,
se	s	k7c7	s
skvrnitýma	skvrnitý	k2eAgFnPc7d1	skvrnitá
nohama	noha	k1gFnPc7	noha
<g/>
,	,	kIx,	,
menšími	malý	k2eAgFnPc7d2	menší
tmavými	tmavý	k2eAgFnPc7d1	tmavá
skvrnami	skvrna	k1gFnPc7	skvrna
a	a	k8xC	a
okrovou	okrový	k2eAgFnSc7d1	okrová
podkladovou	podkladový	k2eAgFnSc7d1	podkladová
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
analýz	analýza	k1gFnPc2	analýza
mitochondriální	mitochondriální	k2eAgFnSc2d1	mitochondriální
a	a	k8xC	a
jaderné	jaderný	k2eAgFnSc2d1	jaderná
DNA	DNA	kA	DNA
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
druhu	druh	k1gInSc2	druh
Giraffa	Giraff	k1gMnSc2	Giraff
camelopardalis	camelopardalis	k1gFnSc2	camelopardalis
některé	některý	k3yIgInPc1	některý
poddruhy	poddruh	k1gInPc1	poddruh
vyčleněny	vyčleněn	k2eAgInPc1d1	vyčleněn
do	do	k7c2	do
dalších	další	k2eAgInPc2d1	další
3	[number]	k4	3
samostatných	samostatný	k2eAgInPc2d1	samostatný
druhů	druh	k1gInPc2	druh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Druh	druh	k1gInSc1	druh
<g/>
:	:	kIx,	:
žirafa	žirafa	k1gFnSc1	žirafa
severní	severní	k2eAgFnSc1d1	severní
(	(	kIx(	(
<g/>
Giraffa	Giraff	k1gMnSc2	Giraff
camelopardalis	camelopardalis	k1gFnSc2	camelopardalis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Poddruh	poddruh	k1gInSc1	poddruh
<g/>
:	:	kIx,	:
žirafa	žirafa	k1gFnSc1	žirafa
kordofanská	kordofanský	k2eAgFnSc1d1	kordofanský
(	(	kIx(	(
<g/>
G.	G.	kA	G.
c.	c.	k?	c.
antiquorum	antiquorum	k1gInSc1	antiquorum
<g/>
)	)	kIx)	)
–	–	k?	–
menší	malý	k2eAgFnSc1d2	menší
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
pravidelné	pravidelný	k2eAgInPc1d1	pravidelný
tmavě	tmavě	k6eAd1	tmavě
hnědé	hnědý	k2eAgInPc1d1	hnědý
fleky	flek	k1gInPc1	flek
které	který	k3yQgMnPc4	který
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
část	část	k1gFnSc4	část
nohy	noha	k1gFnSc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
západní	západní	k2eAgInSc4d1	západní
a	a	k8xC	a
jihozápadní	jihozápadní	k2eAgInSc4d1	jihozápadní
Súdán	Súdán	k1gInSc4	Súdán
<g/>
,	,	kIx,	,
východní	východní	k2eAgInSc4d1	východní
Čad	Čad	k1gInSc4	Čad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poddruh	poddruh	k1gInSc1	poddruh
<g/>
:	:	kIx,	:
žirafa	žirafa	k1gFnSc1	žirafa
núbijská	núbijský	k2eAgFnSc1d1	núbijská
(	(	kIx(	(
<g/>
G.	G.	kA	G.
c.	c.	k?	c.
camelopardalis	camelopardalis	k1gInSc1	camelopardalis
<g/>
)	)	kIx)	)
–	–	k?	–
velké	velká	k1gFnPc4	velká
<g/>
,	,	kIx,	,
čtyřhranné	čtyřhranný	k2eAgInPc4d1	čtyřhranný
fleky	flek	k1gInPc4	flek
kaštanové	kaštanový	k2eAgFnSc2d1	Kaštanová
barvy	barva	k1gFnSc2	barva
na	na	k7c6	na
našedlém	našedlý	k2eAgNnSc6d1	našedlé
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
,	,	kIx,	,
žádné	žádný	k3yNgInPc4	žádný
fleky	flek	k1gInPc4	flek
na	na	k7c6	na
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
straně	strana	k1gFnSc6	strana
nohou	noha	k1gFnPc2	noha
nebo	nebo	k8xC	nebo
pod	pod	k7c7	pod
hleznem	hlezno	k1gNnSc7	hlezno
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
východní	východní	k2eAgInSc1d1	východní
Súdán	Súdán	k1gInSc1	Súdán
<g/>
,	,	kIx,	,
severovýchodní	severovýchodní	k2eAgNnSc1d1	severovýchodní
Kongo	Kongo	k1gNnSc1	Kongo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ekotypem	Ekotyp	k1gInSc7	Ekotyp
žirafy	žirafa	k1gFnSc2	žirafa
núbijské	núbijský	k2eAgFnSc2d1	núbijská
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
pokládaným	pokládaný	k2eAgMnSc7d1	pokládaný
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
poddruh	poddruh	k1gInSc4	poddruh
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
žirafa	žirafa	k1gFnSc1	žirafa
Rothschildova	Rothschildův	k2eAgFnSc1d1	Rothschildova
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
G.	G.	kA	G.
c.	c.	k?	c.
rothschildi	rothschild	k1gMnPc1	rothschild
<g/>
)	)	kIx)	)
–	–	k?	–
tmavě	tmavě	k6eAd1	tmavě
hnědé	hnědý	k2eAgFnPc1d1	hnědá
kaňkovité	kaňkovitý	k2eAgFnPc1d1	kaňkovitý
nebo	nebo	k8xC	nebo
obdélníkové	obdélníkový	k2eAgFnPc1d1	obdélníková
skvrny	skvrna	k1gFnPc1	skvrna
s	s	k7c7	s
chabě	chabě	k6eAd1	chabě
definovanými	definovaný	k2eAgFnPc7d1	definovaná
krémovými	krémový	k2eAgFnPc7d1	krémová
linkami	linka	k1gFnPc7	linka
<g/>
.	.	kIx.	.
</s>
<s>
Hlezno	hlezno	k1gNnSc1	hlezno
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
skvrnité	skvrnitý	k2eAgNnSc1d1	skvrnité
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poddruh	poddruh	k1gInSc1	poddruh
<g/>
:	:	kIx,	:
žirafa	žirafa	k1gFnSc1	žirafa
západoafrická	západoafrický	k2eAgFnSc1d1	západoafrická
(	(	kIx(	(
<g/>
G.	G.	kA	G.
c.	c.	k?	c.
peralta	peralta	k1gFnSc1	peralta
<g/>
)	)	kIx)	)
–	–	k?	–
mnoho	mnoho	k4c4	mnoho
malých	malá	k1gFnPc2	malá
<g/>
,	,	kIx,	,
kulatých	kulatý	k2eAgFnPc2d1	kulatá
<g/>
,	,	kIx,	,
bledě	bledě	k6eAd1	bledě
nažloutle	nažloutle	k6eAd1	nažloutle
červených	červený	k2eAgFnPc2d1	červená
skvrn	skvrna	k1gFnPc2	skvrna
na	na	k7c6	na
bílém	bílý	k2eAgNnSc6d1	bílé
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
Čad	Čad	k1gInSc1	Čad
<g/>
,	,	kIx,	,
Niger	Niger	k1gInSc1	Niger
<g/>
,	,	kIx,	,
Mali	Mali	k1gNnSc1	Mali
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druh	druh	k1gInSc1	druh
<g/>
:	:	kIx,	:
žirafa	žirafa	k1gFnSc1	žirafa
síťovaná	síťovaný	k2eAgFnSc1d1	síťovaná
(	(	kIx(	(
<g/>
Giraffa	Giraff	k1gMnSc2	Giraff
reticulata	reticule	k1gNnPc4	reticule
<g/>
)	)	kIx)	)
–	–	k?	–
kresba	kresba	k1gFnSc1	kresba
tvoří	tvořit	k5eAaImIp3nS	tvořit
velké	velká	k1gFnPc4	velká
polygonální	polygonální	k2eAgFnSc2d1	polygonální
skořicové	skořicový	k2eAgFnSc2d1	skořicová
nebo	nebo	k8xC	nebo
rezavé	rezavý	k2eAgFnSc2d1	rezavá
plochy	plocha	k1gFnSc2	plocha
ohraničené	ohraničený	k2eAgFnSc2d1	ohraničená
sítí	síť	k1gFnSc7	síť
světlých	světlý	k2eAgFnPc2d1	světlá
bílých	bílý	k2eAgFnPc2d1	bílá
čar	čára	k1gFnPc2	čára
<g/>
.	.	kIx.	.
</s>
<s>
Skvrny	skvrna	k1gFnPc1	skvrna
někdy	někdy	k6eAd1	někdy
vypadají	vypadat	k5eAaImIp3nP	vypadat
temně	temně	k6eAd1	temně
rudě	rudě	k6eAd1	rudě
a	a	k8xC	a
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
i	i	k9	i
nohy	noha	k1gFnPc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
severovýchodní	severovýchodní	k2eAgFnSc1d1	severovýchodní
Keňa	Keňa	k1gFnSc1	Keňa
<g/>
,	,	kIx,	,
Etiopie	Etiopie	k1gFnSc1	Etiopie
<g/>
,	,	kIx,	,
Somálsko	Somálsko	k1gNnSc1	Somálsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druh	druh	k1gInSc1	druh
<g/>
:	:	kIx,	:
žirafa	žirafa	k1gFnSc1	žirafa
jižní	jižní	k2eAgFnSc1d1	jižní
(	(	kIx(	(
<g/>
Giraffa	Giraff	k1gMnSc4	Giraff
giraffa	giraff	k1gMnSc4	giraff
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Poddruh	poddruh	k1gInSc1	poddruh
<g/>
:	:	kIx,	:
žirafa	žirafa	k1gFnSc1	žirafa
angolská	angolský	k2eAgFnSc1d1	angolská
(	(	kIx(	(
<g/>
G.	G.	kA	G.
g.	g.	k?	g.
angolensis	angolensis	k1gInSc1	angolensis
<g/>
)	)	kIx)	)
–	–	k?	–
velké	velká	k1gFnPc4	velká
<g/>
,	,	kIx,	,
rudohnědé	rudohnědý	k2eAgFnPc4d1	rudohnědá
skvrny	skvrna	k1gFnPc4	skvrna
a	a	k8xC	a
vroubky	vroubek	k1gInPc4	vroubek
kolem	kolem	k7c2	kolem
hran	hrana	k1gFnPc2	hrana
rozprostírající	rozprostírající	k2eAgFnSc2d1	rozprostírající
se	se	k3xPyFc4	se
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
noze	noha	k1gFnSc6	noha
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
Angola	Angola	k1gFnSc1	Angola
<g/>
,	,	kIx,	,
Zambie	Zambie	k1gFnSc1	Zambie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poddruh	poddruh	k1gInSc1	poddruh
<g/>
:	:	kIx,	:
žirafa	žirafa	k1gFnSc1	žirafa
kapská	kapský	k2eAgFnSc1d1	kapská
(	(	kIx(	(
<g/>
G.	G.	kA	G.
g.	g.	k?	g.
giraffa	giraff	k1gMnSc2	giraff
<g/>
)	)	kIx)	)
–	–	k?	–
zaoblené	zaoblený	k2eAgFnSc2d1	zaoblená
a	a	k8xC	a
kaňkovité	kaňkovitý	k2eAgFnSc2d1	kaňkovitý
skvrny	skvrna	k1gFnSc2	skvrna
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
hvězdicovitého	hvězdicovitý	k2eAgInSc2d1	hvězdicovitý
tvaru	tvar	k1gInSc2	tvar
na	na	k7c6	na
světlém	světlý	k2eAgNnSc6d1	světlé
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
Jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
Namibie	Namibie	k1gFnSc1	Namibie
<g/>
,	,	kIx,	,
Botswana	Botswana	k1gFnSc1	Botswana
<g/>
,	,	kIx,	,
Zimbabwe	Zimbabwe	k1gNnSc1	Zimbabwe
<g/>
,	,	kIx,	,
Mozambik	Mozambik	k1gInSc1	Mozambik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druh	druh	k1gInSc1	druh
<g/>
:	:	kIx,	:
žirafa	žirafa	k1gFnSc1	žirafa
masajská	masajský	k2eAgFnSc1d1	masajská
(	(	kIx(	(
<g/>
Giraffa	Giraff	k1gMnSc2	Giraff
tippelskirchi	tippelskirch	k1gFnSc2	tippelskirch
<g/>
)	)	kIx)	)
–	–	k?	–
má	mít	k5eAaImIp3nS	mít
drobnější	drobný	k2eAgFnPc4d2	drobnější
skvrny	skvrna	k1gFnPc4	skvrna
ze	z	k7c2	z
zubatými	zubatý	k2eAgInPc7d1	zubatý
okraji	okraj	k1gInPc7	okraj
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
vinného	vinný	k2eAgInSc2d1	vinný
listu	list	k1gInSc2	list
<g/>
,	,	kIx,	,
barva	barva	k1gFnSc1	barva
temně	temně	k6eAd1	temně
čokoládova	čokoládův	k2eAgFnSc1d1	čokoládův
s	s	k7c7	s
nažloutlým	nažloutlý	k2eAgNnSc7d1	nažloutlé
pozadím	pozadí	k1gNnSc7	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
střední	střední	k2eAgFnSc1d1	střední
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
Keňa	Keňa	k1gFnSc1	Keňa
<g/>
,	,	kIx,	,
Tanzanie	Tanzanie	k1gFnSc1	Tanzanie
<g/>
.	.	kIx.	.
</s>
<s>
Nápadně	nápadně	k6eAd1	nápadně
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
třetí	třetí	k4xOgInSc4	třetí
růžek	růžek	k1gInSc4	růžek
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
Uganda	Uganda	k1gFnSc1	Uganda
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc1d1	severní
až	až	k8xS	až
centrální	centrální	k2eAgFnSc1d1	centrální
Keňa	Keňa	k1gFnSc1	Keňa
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Někdy	někdy	k6eAd1	někdy
nesprávně	správně	k6eNd1	správně
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Rotschildova	Rotschildův	k2eAgFnSc1d1	Rotschildova
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ekotypem	Ekotyp	k1gInSc7	Ekotyp
žirafy	žirafa	k1gFnSc2	žirafa
masajské	masajský	k2eAgFnSc2d1	masajská
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
pokládaným	pokládaný	k2eAgMnSc7d1	pokládaný
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
poddruh	poddruh	k1gInSc4	poddruh
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
žirafa	žirafa	k1gFnSc1	žirafa
zambijská	zambijský	k2eAgFnSc1d1	zambijská
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
G.	G.	kA	G.
c.	c.	k?	c.
thornicrofti	thornicrofť	k1gFnSc2	thornicrofť
<g/>
)	)	kIx)	)
–	–	k?	–
malé	malý	k2eAgFnSc3d1	malá
<g/>
,	,	kIx,	,
tmavohnědé	tmavohnědý	k2eAgFnSc3d1	tmavohnědá
hvězdicovité	hvězdicovitý	k2eAgFnPc4d1	hvězdicovitá
nebo	nebo	k8xC	nebo
listnaté	listnatý	k2eAgFnPc4d1	listnatá
skvrny	skvrna	k1gFnPc4	skvrna
rozpínající	rozpínající	k2eAgFnPc4d1	rozpínající
se	se	k3xPyFc4	se
až	až	k6eAd1	až
k	k	k7c3	k
spodní	spodní	k2eAgFnSc3d1	spodní
části	část	k1gFnSc3	část
nohou	noha	k1gFnPc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Podklad	podklad	k1gInSc1	podklad
tmavší	tmavý	k2eAgInSc1d2	tmavší
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
pískový	pískový	k2eAgMnSc1d1	pískový
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
východní	východní	k2eAgFnSc2d1	východní
Zambie	Zambie	k1gFnSc2	Zambie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Samice	samice	k1gFnPc1	samice
se	se	k3xPyFc4	se
páří	pářit	k5eAaImIp3nP	pářit
s	s	k7c7	s
dominantním	dominantní	k2eAgInSc7d1	dominantní
samcem	samec	k1gInSc7	samec
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
lokalitě	lokalita	k1gFnSc6	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
o	o	k7c4	o
dominantu	dominanta	k1gFnSc4	dominanta
bojují	bojovat	k5eAaImIp3nP	bojovat
přetlačováním	přetlačování	k1gNnSc7	přetlačování
se	se	k3xPyFc4	se
krky	krk	k1gInPc7	krk
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
dokonce	dokonce	k9	dokonce
do	do	k7c2	do
sebe	se	k3xPyFc2	se
narážejí	narážet	k5eAaPmIp3nP	narážet
hlavami	hlava	k1gFnPc7	hlava
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
proplétají	proplétat	k5eAaImIp3nP	proplétat
krky	krk	k1gInPc4	krk
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
boji	boj	k1gInSc3	boj
dochází	docházet	k5eAaImIp3nS	docházet
často	často	k6eAd1	často
mezi	mezi	k7c7	mezi
mladými	mladý	k2eAgMnPc7d1	mladý
samci	samec	k1gMnPc7	samec
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
při	při	k7c6	při
příchodu	příchod	k1gInSc6	příchod
nového	nový	k2eAgInSc2d1	nový
samce	samec	k1gInSc2	samec
do	do	k7c2	do
teritoria	teritorium	k1gNnSc2	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyhraném	vyhraný	k2eAgInSc6d1	vyhraný
souboji	souboj	k1gInSc6	souboj
vítěz	vítěz	k1gMnSc1	vítěz
poníží	ponížit	k5eAaPmIp3nS	ponížit
soka	sok	k1gMnSc4	sok
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
předstírá	předstírat	k5eAaImIp3nS	předstírat
páření	páření	k1gNnSc1	páření
<g/>
.	.	kIx.	.
</s>
<s>
Březost	březost	k1gFnSc1	březost
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
trvá	trvat	k5eAaImIp3nS	trvat
457	[number]	k4	457
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterých	který	k3yQgFnPc6	který
se	se	k3xPyFc4	se
narodí	narodit	k5eAaPmIp3nS	narodit
jedno	jeden	k4xCgNnSc1	jeden
mládě	mládě	k1gNnSc1	mládě
<g/>
,	,	kIx,	,
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
se	se	k3xPyFc4	se
narodí	narodit	k5eAaPmIp3nP	narodit
dvojčata	dvojče	k1gNnPc1	dvojče
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
mláďata	mládě	k1gNnPc1	mládě
rodí	rodit	k5eAaImIp3nP	rodit
v	v	k7c6	v
období	období	k1gNnSc6	období
sucha	sucho	k1gNnSc2	sucho
<g/>
,	,	kIx,	,
těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
váží	vážit	k5eAaImIp3nP	vážit
mládě	mládě	k1gNnSc4	mládě
přibližně	přibližně	k6eAd1	přibližně
70	[number]	k4	70
kg	kg	kA	kg
a	a	k8xC	a
měří	měřit	k5eAaImIp3nS	měřit
okolo	okolo	k7c2	okolo
2	[number]	k4	2
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
dospívají	dospívat	k5eAaImIp3nP	dospívat
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
3	[number]	k4	3
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
se	se	k3xPyFc4	se
až	až	k9	až
v	v	k7c6	v
6	[number]	k4	6
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
žirafa	žirafa	k1gFnSc1	žirafa
dožívá	dožívat	k5eAaImIp3nS	dožívat
zhruba	zhruba	k6eAd1	zhruba
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
i	i	k9	i
přes	přes	k7c4	přes
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nepřátelé	nepřítel	k1gMnPc5	nepřítel
==	==	k?	==
</s>
</p>
<p>
<s>
Žirafa	žirafa	k1gFnSc1	žirafa
má	mít	k5eAaImIp3nS	mít
málo	málo	k4c4	málo
přirozených	přirozený	k2eAgMnPc2d1	přirozený
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
,	,	kIx,	,
dospělá	dospělý	k2eAgNnPc1d1	dospělé
zvířata	zvíře	k1gNnPc1	zvíře
loví	lovit	k5eAaImIp3nP	lovit
pouze	pouze	k6eAd1	pouze
lvi	lev	k1gMnPc1	lev
a	a	k8xC	a
krokodýli	krokodýl	k1gMnPc1	krokodýl
<g/>
,	,	kIx,	,
na	na	k7c4	na
mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
odvažují	odvažovat	k5eAaImIp3nP	odvažovat
i	i	k9	i
hyeny	hyena	k1gFnPc1	hyena
skvrnité	skvrnitý	k2eAgFnPc1d1	skvrnitá
a	a	k8xC	a
výjimečně	výjimečně	k6eAd1	výjimečně
i	i	k9	i
levharti	levhart	k1gMnPc1	levhart
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
však	však	k9	však
ohrožené	ohrožený	k2eAgNnSc1d1	ohrožené
mládě	mládě	k1gNnSc1	mládě
divoce	divoce	k6eAd1	divoce
brání	bránit	k5eAaImIp3nS	bránit
kopáním	kopání	k1gNnSc7	kopání
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgNnSc1d1	jediné
kopnutí	kopnutí	k1gNnSc1	kopnutí
dokáže	dokázat	k5eAaPmIp3nS	dokázat
usmrtit	usmrtit	k5eAaPmF	usmrtit
lvici	lvice	k1gFnSc4	lvice
nebo	nebo	k8xC	nebo
prorazit	prorazit	k5eAaPmF	prorazit
kapotu	kapota	k1gFnSc4	kapota
terénního	terénní	k2eAgInSc2d1	terénní
vozu	vůz	k1gInSc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Nejhoršími	zlý	k2eAgMnPc7d3	nejhorší
nepřáteli	nepřítel	k1gMnPc7	nepřítel
žiraf	žirafa	k1gFnPc2	žirafa
jsou	být	k5eAaImIp3nP	být
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Domorodci	domorodec	k1gMnPc1	domorodec
je	on	k3xPp3gInPc4	on
často	často	k6eAd1	často
loví	lovit	k5eAaImIp3nP	lovit
otrávenými	otrávený	k2eAgInPc7d1	otrávený
šípy	šíp	k1gInPc7	šíp
nebo	nebo	k8xC	nebo
jim	on	k3xPp3gMnPc3	on
přetínají	přetínat	k5eAaImIp3nP	přetínat
šlachy	šlacha	k1gFnPc4	šlacha
na	na	k7c6	na
zadních	zadní	k2eAgFnPc6d1	zadní
nohách	noha	k1gFnPc6	noha
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
její	její	k3xOp3gFnSc2	její
kůže	kůže	k1gFnSc2	kůže
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
štíty	štít	k1gInPc1	štít
nebo	nebo	k8xC	nebo
sandály	sandál	k1gInPc1	sandál
<g/>
,	,	kIx,	,
z	z	k7c2	z
žíní	žíně	k1gFnPc2	žíně
ocasu	ocas	k1gInSc2	ocas
náramky	náramek	k1gInPc4	náramek
a	a	k8xC	a
amulety	amulet	k1gInPc4	amulet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavost	zajímavost	k1gFnSc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Není	být	k5eNaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
žirafy	žirafa	k1gFnPc1	žirafa
umí	umět	k5eAaImIp3nP	umět
plavat	plavat	k5eAaImF	plavat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
chybí	chybět	k5eAaImIp3nS	chybět
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
kdokoliv	kdokoliv	k3yInSc1	kdokoliv
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
žirafu	žirafa	k1gFnSc4	žirafa
při	při	k7c6	při
plavání	plavání	k1gNnSc6	plavání
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
počítačových	počítačový	k2eAgFnPc2d1	počítačová
simulací	simulace	k1gFnPc2	simulace
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
ale	ale	k9	ale
patrně	patrně	k6eAd1	patrně
nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
udržet	udržet	k5eAaPmF	udržet
dokázala	dokázat	k5eAaPmAgFnS	dokázat
<g/>
.	.	kIx.	.
</s>
<s>
Hlubším	hluboký	k2eAgFnPc3d2	hlubší
vodám	voda	k1gFnPc3	voda
se	se	k3xPyFc4	se
žirafy	žirafa	k1gFnSc2	žirafa
nicméně	nicméně	k8xC	nicméně
vyhýbají	vyhýbat	k5eAaImIp3nP	vyhýbat
<g/>
,	,	kIx,	,
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dosáhnou	dosáhnout	k5eAaPmIp3nP	dosáhnout
pohodlně	pohodlně	k6eAd1	pohodlně
na	na	k7c4	na
dno	dno	k1gNnSc4	dno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chov	chov	k1gInSc1	chov
v	v	k7c6	v
zoo	zoo	k1gFnSc6	zoo
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Evropy	Evropa	k1gFnSc2	Evropa
jsou	být	k5eAaImIp3nP	být
chovány	chován	k2eAgFnPc1d1	chována
zejména	zejména	k9	zejména
žirafa	žirafa	k1gFnSc1	žirafa
Rothschildova	Rothschildův	k2eAgFnSc1d1	Rothschildova
(	(	kIx(	(
<g/>
severní	severní	k2eAgFnSc1d1	severní
núbijská	núbijský	k2eAgFnSc1d1	núbijská
<g/>
)	)	kIx)	)
a	a	k8xC	a
žirafa	žirafa	k1gFnSc1	žirafa
síťovaná	síťovaný	k2eAgFnSc1d1	síťovaná
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
žirafa	žirafa	k1gFnSc1	žirafa
do	do	k7c2	do
československých	československý	k2eAgFnPc2d1	Československá
zoo	zoo	k1gFnPc2	zoo
přišla	přijít	k5eAaPmAgFnS	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
do	do	k7c2	do
Zoo	zoo	k1gFnSc2	zoo
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
lze	lze	k6eAd1	lze
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
nalézt	nalézt	k5eAaPmF	nalézt
výhradně	výhradně	k6eAd1	výhradně
žirafu	žirafa	k1gFnSc4	žirafa
Rothschildovu	Rothschildův	k2eAgFnSc4d1	Rothschildova
(	(	kIx(	(
<g/>
žirafu	žirafa	k1gFnSc4	žirafa
severní	severní	k2eAgFnSc4d1	severní
núbijskou	núbijský	k2eAgFnSc4d1	núbijská
<g/>
)	)	kIx)	)
anebo	anebo	k8xC	anebo
žirafu	žirafa	k1gFnSc4	žirafa
síťovanou	síťovaný	k2eAgFnSc4d1	síťovaná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
žirafa	žirafa	k1gFnSc1	žirafa
Rothschildova	Rothschildův	k2eAgFnSc1d1	Rothschildova
(	(	kIx(	(
<g/>
severní	severní	k2eAgFnSc1d1	severní
núbijská	núbijský	k2eAgFnSc1d1	núbijská
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Dvůr	Dvůr	k1gInSc1	Dvůr
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
Zlín	Zlín	k1gInSc1	Zlín
</s>
</p>
<p>
<s>
žirafa	žirafa	k1gFnSc1	žirafa
síťovaná	síťovaný	k2eAgFnSc1d1	síťovaná
<g/>
:	:	kIx,	:
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Dvůr	Dvůr	k1gInSc1	Dvůr
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
JihlavaZoo	JihlavaZoo	k6eAd1	JihlavaZoo
Dvůr	Dvůr	k1gInSc1	Dvůr
Králové	Králová	k1gFnSc2	Králová
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
chovatelem	chovatel	k1gMnSc7	chovatel
žiraf	žirafa	k1gFnPc2	žirafa
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
žirafa	žirafa	k1gFnSc1	žirafa
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
žirafa	žirafa	k1gFnSc1	žirafa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
české	český	k2eAgFnPc1d1	Česká
stránky	stránka	k1gFnPc1	stránka
o	o	k7c6	o
žirafách	žirafa	k1gFnPc6	žirafa
</s>
</p>
<p>
<s>
české	český	k2eAgFnPc4d1	Česká
stránky	stránka	k1gFnPc4	stránka
věnující	věnující	k2eAgFnPc4d1	věnující
se	se	k3xPyFc4	se
žirafám	žirafa	k1gFnPc3	žirafa
a	a	k8xC	a
antilopám	antilopa	k1gFnPc3	antilopa
</s>
</p>
<p>
<s>
IUCN	IUCN	kA	IUCN
Červený	červený	k2eAgInSc1d1	červený
list	list	k1gInSc1	list
ohrožených	ohrožený	k2eAgNnPc2d1	ohrožené
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
