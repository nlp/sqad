<s>
Hnutí	hnutí	k1gNnSc1
22	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
</s>
<s>
Univerzita	univerzita	k1gFnSc1
v	v	k7c6
Nanterre	Nanterr	k1gMnSc5
</s>
<s>
Hnutí	hnutí	k1gNnSc1
22	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
(	(	kIx(
<g/>
fr.	fr.	k?
Mouvement	Mouvement	k1gInSc1
du	du	k?
22	#num#	k4
<g/>
-Mars	-Mars	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
název	název	k1gInSc1
studentského	studentský	k2eAgNnSc2d1
politického	politický	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
v	v	k7c4
pátek	pátek	k1gInSc4
22	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1968	#num#	k4
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Nanterre	Nanterr	k1gInSc5
poblíž	poblíž	k6eAd1
Paříže	Paříž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hnutí	hnutí	k1gNnSc1
bylo	být	k5eAaImAgNnS
ovlivněno	ovlivnit	k5eAaPmNgNnS
libertarianismem	libertarianismus	k1gInSc7
a	a	k8xC
bylo	být	k5eAaImAgNnS
jedním	jeden	k4xCgInSc7
z	z	k7c2
impulsů	impuls	k1gInPc2
následných	následný	k2eAgFnPc2d1
květnových	květnový	k2eAgFnPc2d1
událostí	událost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svou	svůj	k3xOyFgFnSc4
činnost	činnost	k1gFnSc4
ukončilo	ukončit	k5eAaPmAgNnS
v	v	k7c6
červnu	červen	k1gInSc6
1968	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1
politickým	politický	k2eAgMnSc7d1
vůdcem	vůdce	k1gMnSc7
hnutí	hnutí	k1gNnSc2
byl	být	k5eAaImAgMnS
tehdejší	tehdejší	k2eAgMnSc1d1
student	student	k1gMnSc1
Daniel	Daniel	k1gMnSc1
Cohn-Bendit	Cohn-Bendit	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hnutí	hnutí	k1gNnSc1
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
večer	večer	k6eAd1
22	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
během	během	k7c2
obsazení	obsazení	k1gNnSc2
univerzitní	univerzitní	k2eAgFnSc2d1
administrativní	administrativní	k2eAgFnSc2d1
budovy	budova	k1gFnSc2
142	#num#	k4
studenty	student	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osazení	osazení	k1gNnSc1
proběhlo	proběhnout	k5eAaPmAgNnS
na	na	k7c4
protest	protest	k1gInSc4
proti	proti	k7c3
zatčení	zatčení	k1gNnSc3
komunistů	komunista	k1gMnPc2
Xaviera	Xaviera	k1gFnSc1
Langlada	Langlada	k1gFnSc1
a	a	k8xC
Nicolase	Nicolasa	k1gFnSc3
Boulta	Boult	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hnutí	hnutí	k1gNnSc1
mělo	mít	k5eAaImAgNnS
kořeny	kořen	k1gInPc4
v	v	k7c4
rok	rok	k1gInSc4
staré	starý	k2eAgFnSc2d1
události	událost	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
21	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1967	#num#	k4
studenti	student	k1gMnPc1
v	v	k7c6
Nanterre	Nanterr	k1gInSc5
spontánně	spontánně	k6eAd1
obsadili	obsadit	k5eAaPmAgMnP
budovu	budova	k1gFnSc4
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovalo	následovat	k5eAaImAgNnS
násilné	násilný	k2eAgNnSc1d1
vyklizení	vyklizení	k1gNnSc1
budovy	budova	k1gFnSc2
policií	policie	k1gFnPc2
a	a	k8xC
postih	postih	k1gInSc1
studentů	student	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
byli	být	k5eAaImAgMnP
vyloučeni	vyloučit	k5eAaPmNgMnP
z	z	k7c2
kurzů	kurz	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Hnutí	hnutí	k1gNnSc1
22	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
se	se	k3xPyFc4
posléze	posléze	k6eAd1
rozšířilo	rozšířit	k5eAaPmAgNnS
i	i	k9
na	na	k7c4
jiné	jiný	k2eAgFnPc4d1
vysoké	vysoký	k2eAgFnPc4d1
školy	škola	k1gFnPc4
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
studenti	student	k1gMnPc1
organizovali	organizovat	k5eAaBmAgMnP
stávky	stávek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Studenti	student	k1gMnPc1
v	v	k7c6
rámci	rámec	k1gInSc6
hnutí	hnutí	k1gNnSc2
požadovali	požadovat	k5eAaImAgMnP
více	hodně	k6eAd2
svobody	svoboda	k1gFnSc2
v	v	k7c6
osobním	osobní	k2eAgInSc6d1
životě	život	k1gInSc6
(	(	kIx(
<g/>
sexuální	sexuální	k2eAgFnSc1d1
svoboda	svoboda	k1gFnSc1
<g/>
)	)	kIx)
i	i	k8xC
svobody	svoboda	k1gFnSc2
na	na	k7c6
univerzitách	univerzita	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
rozšířila	rozšířit	k5eAaPmAgFnS
zpráva	zpráva	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
Daniel	Daniel	k1gMnSc1
Cohn-Bendit	Cohn-Bendit	k1gFnSc2
bude	být	k5eAaImBp3nS
přeložen	přeložit	k5eAaPmNgInS
na	na	k7c4
jinou	jiný	k2eAgFnSc4d1
univerzitu	univerzita	k1gFnSc4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
svolána	svolat	k5eAaPmNgFnS
stávka	stávka	k1gFnSc1
<g/>
,	,	kIx,
především	především	k9
z	z	k7c2
řad	řada	k1gFnPc2
levicových	levicový	k2eAgMnPc2d1
studentů	student	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
v	v	k7c4
pátek	pátek	k1gInSc4
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1968	#num#	k4
nechal	nechat	k5eAaPmAgMnS
univerzitu	univerzita	k1gFnSc4
v	v	k7c6
Nanterre	Nanterr	k1gInSc5
uzavřít	uzavřít	k5eAaPmF
děkan	děkan	k1gMnSc1
Pierre	Pierr	k1gInSc5
Grappin	Grappin	k1gInSc1
<g/>
,	,	kIx,
hnutí	hnutí	k1gNnSc1
se	se	k3xPyFc4
přesunulo	přesunout	k5eAaPmAgNnS
do	do	k7c2
Paříže	Paříž	k1gFnSc2
na	na	k7c4
Sorbonnu	Sorbonna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
byly	být	k5eAaImAgFnP
zahájeny	zahájit	k5eAaPmNgFnP
květnové	květnový	k2eAgFnPc1d1
události	událost	k1gFnPc1
1968	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1
Hnutí	hnutí	k1gNnSc1
22	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
se	se	k3xPyFc4
rozpustilo	rozpustit	k5eAaPmAgNnS
začátkem	začátkem	k7c2
června	červen	k1gInSc2
1968	#num#	k4
na	na	k7c6
valné	valný	k2eAgFnSc6d1
hromadě	hromada	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Příběh	příběh	k1gInSc1
Hnutí	hnutí	k1gNnSc1
22	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
zpracoval	zpracovat	k5eAaPmAgMnS
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
francouzský	francouzský	k2eAgMnSc1d1
romanopisec	romanopisec	k1gMnSc1
Robert	Robert	k1gMnSc1
Merle	Merle	k1gFnSc1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
novele	novela	k1gFnSc6
Derriè	Derriè	k1gInSc5
la	la	k0
vitre	vitr	k1gMnSc5
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
vyšla	vyjít	k5eAaPmAgFnS
v	v	k7c6
češtině	čeština	k1gFnSc6
pod	pod	k7c7
názvem	název	k1gInSc7
Za	za	k7c7
sklem	sklo	k1gNnSc7
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Květen	květen	k1gInSc1
1968	#num#	k4
ve	v	k7c6
Francii	Francie	k1gFnSc6
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Paříž	Paříž	k1gFnSc1
X	X	kA
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Mouvement	Mouvement	k1gInSc1
du	du	k?
22	#num#	k4
<g/>
-Mars	-Marsa	k1gFnPc2
na	na	k7c6
francouzské	francouzský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Le	Le	k1gFnSc1
syndicalisme	syndicalismus	k1gInSc5
étudiant	étudiant	k1gMnSc1
à	à	k?
Nanterre	Nanterr	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Entretien	Entretien	k2eAgInSc1d1
avec	avec	k1gInSc1
Jean-François	Jean-François	k1gFnPc2
Godchau	Godchaus	k1gInSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
262956920	#num#	k4
</s>
