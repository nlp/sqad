<s>
Panna	Panna	k1gFnSc1	Panna
Cinková	Cinková	k1gFnSc1	Cinková
(	(	kIx(	(
<g/>
1711	[number]	k4	1711
Gemer	Gemer	k1gInSc1	Gemer
Uhersko	Uhersko	k1gNnSc1	Uhersko
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1772	[number]	k4	1772
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
uherská	uherský	k2eAgFnSc1d1	uherská
lidová	lidový	k2eAgFnSc1d1	lidová
houslistka	houslistka	k1gFnSc1	houslistka
romského	romský	k2eAgInSc2d1	romský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Cinka	Cinka	k1gFnSc1	Cinka
Panna	Panna	k1gFnSc1	Panna
(	(	kIx(	(
<g/>
v	v	k7c6	v
maďarském	maďarský	k2eAgInSc6d1	maďarský
jazyce	jazyk	k1gInSc6	jazyk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
legendární	legendární	k2eAgFnSc4d1	legendární
romskou	romský	k2eAgFnSc4d1	romská
primášku	primáška	k1gFnSc4	primáška
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Gemeru	Gemero	k1gNnSc6	Gemero
(	(	kIx(	(
<g/>
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
slovenský	slovenský	k2eAgInSc1d1	slovenský
okres	okres	k1gInSc1	okres
Rimavská	rimavský	k2eAgFnSc1d1	Rimavská
Sobota	sobota	k1gFnSc1	sobota
<g/>
)	)	kIx)	)
ve	v	k7c6	v
slavné	slavný	k2eAgFnSc6d1	slavná
romské	romský	k2eAgFnSc6d1	romská
muzikantské	muzikantský	k2eAgFnSc6d1	muzikantská
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
primáš	primáš	k1gMnSc1	primáš
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c4	o
dvorního	dvorní	k2eAgMnSc4d1	dvorní
muzikanta	muzikant	k1gMnSc4	muzikant
knížete	kníže	k1gMnSc4	kníže
Františka	František	k1gMnSc4	František
Rákocziho	Rákoczi	k1gMnSc4	Rákoczi
<g/>
.	.	kIx.	.
</s>
<s>
Jejímu	její	k3xOp3gInSc3	její
otci	otec	k1gMnSc3	otec
a	a	k8xC	a
bratru	bratru	k9	bratru
je	být	k5eAaImIp3nS	být
připisováno	připisován	k2eAgNnSc1d1	připisováno
autorství	autorství	k1gNnSc1	autorství
romských	romský	k2eAgFnPc2d1	romská
písní	píseň	k1gFnPc2	píseň
Rákocziho	Rákoczi	k1gMnSc2	Rákoczi
žalm	žalm	k1gInSc4	žalm
<g/>
,	,	kIx,	,
Rákocziho	Rákoczi	k1gMnSc2	Rákoczi
duma	duma	k1gFnSc1	duma
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
umělecké	umělecký	k2eAgNnSc1d1	umělecké
jméno	jméno	k1gNnSc1	jméno
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
jména	jméno	k1gNnSc2	jméno
Anna	Anna	k1gFnSc1	Anna
(	(	kIx(	(
<g/>
Annamária	Annamárium	k1gNnSc2	Annamárium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
obdržela	obdržet	k5eAaPmAgFnS	obdržet
při	při	k7c6	při
evangelickém	evangelický	k2eAgInSc6d1	evangelický
křtu	křest	k1gInSc6	křest
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
prameny	pramen	k1gInPc1	pramen
uvádějí	uvádět	k5eAaImIp3nP	uvádět
že	že	k8xS	že
toto	tento	k3xDgNnSc4	tento
jméno	jméno	k1gNnSc4	jméno
nosila	nosit	k5eAaImAgFnS	nosit
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
do	do	k7c2	do
svatby	svatba	k1gFnSc2	svatba
byla	být	k5eAaImAgFnS	být
panna	panna	k1gFnSc1	panna
<g/>
,	,	kIx,	,
další	další	k2eAgInSc1d1	další
zdroj	zdroj	k1gInSc1	zdroj
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
nazývali	nazývat	k5eAaImAgMnP	nazývat
Vászon	Vászon	k1gMnSc1	Vászon
Panna	Panna	k1gFnSc1	Panna
<g/>
.	.	kIx.	.
</s>
<s>
Vászon	Vászon	k1gInSc1	Vászon
v	v	k7c6	v
maďarštině	maďarština	k1gFnSc6	maďarština
znamená	znamenat	k5eAaImIp3nS	znamenat
plátno	plátno	k1gNnSc1	plátno
<g/>
,	,	kIx,	,
sukno	sukno	k1gNnSc1	sukno
<g/>
.	.	kIx.	.
</s>
<s>
Pannus	Pannus	k1gInSc1	Pannus
znamená	znamenat	k5eAaImIp3nS	znamenat
purpur	purpur	k1gInSc4	purpur
<g/>
,	,	kIx,	,
červeň	červeň	k1gFnSc4	červeň
a	a	k8xC	a
radost	radost	k1gFnSc4	radost
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
historici	historik	k1gMnPc1	historik
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
třetí	třetí	k4xOgInSc4	třetí
uvedený	uvedený	k2eAgInSc4d1	uvedený
význam	význam	k1gInSc4	význam
slova	slovo	k1gNnSc2	slovo
Pannus	Pannus	k1gInSc1	Pannus
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
vystihovat	vystihovat	k5eAaImF	vystihovat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
svoji	svůj	k3xOyFgFnSc4	svůj
přítomností	přítomnost	k1gFnSc7	přítomnost
a	a	k8xC	a
hudebním	hudební	k2eAgInSc7d1	hudební
projevem	projev	k1gInSc7	projev
rozdávala	rozdávat	k5eAaImAgFnS	rozdávat
radostnou	radostný	k2eAgFnSc4d1	radostná
náladu	nálada	k1gFnSc4	nálada
a	a	k8xC	a
nebo	nebo	k8xC	nebo
že	že	k8xS	že
prostě	prostě	k9	prostě
"	"	kIx"	"
<g/>
Vászon	Vászon	k1gNnSc1	Vászon
Panna	Panna	k1gFnSc1	Panna
<g/>
"	"	kIx"	"
znamená	znamenat	k5eAaImIp3nS	znamenat
Purpurové	purpurový	k2eAgNnSc4d1	purpurové
plátno	plátno	k1gNnSc4	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
svých	svůj	k3xOyFgNnPc2	svůj
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
tajně	tajně	k6eAd1	tajně
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
jiná	jiná	k1gFnSc1	jiná
děvčata	děvče	k1gNnPc4	děvče
ještě	ještě	k9	ještě
hrála	hrát	k5eAaImAgFnS	hrát
s	s	k7c7	s
panenkami	panenka	k1gFnPc7	panenka
<g/>
,	,	kIx,	,
zkoušela	zkoušet	k5eAaImAgFnS	zkoušet
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
otcovy	otcův	k2eAgFnPc4d1	otcova
housle	housle	k1gFnPc4	housle
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
prvotní	prvotní	k2eAgFnSc1d1	prvotní
tvorba	tvorba	k1gFnSc1	tvorba
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
motivy	motiv	k1gInPc4	motiv
pocházející	pocházející	k2eAgInPc4d1	pocházející
z	z	k7c2	z
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
imitovala	imitovat	k5eAaBmAgFnS	imitovat
například	například	k6eAd1	například
štěbetání	štěbetání	k1gNnSc4	štěbetání
zpěvných	zpěvný	k2eAgMnPc2d1	zpěvný
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
šum	šuma	k1gFnPc2	šuma
větru	vítr	k1gInSc2	vítr
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
naučila	naučit	k5eAaPmAgFnS	naučit
tklivé	tklivý	k2eAgFnPc4d1	tklivá
melodie	melodie	k1gFnPc4	melodie
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Gemeru	Gemer	k1gInSc2	Gemer
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc4	její
talent	talent	k1gInSc4	talent
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
místní	místní	k2eAgMnSc1d1	místní
šlechtic	šlechtic	k1gMnSc1	šlechtic
a	a	k8xC	a
zeměpán	zeměpán	k1gMnSc1	zeměpán
Lányi	Lány	k1gFnSc2	Lány
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
otce	otec	k1gMnSc2	otec
působil	působit	k5eAaImAgInS	působit
jako	jako	k9	jako
její	její	k3xOp3gMnSc1	její
mecenáš	mecenáš	k1gMnSc1	mecenáš
<g/>
.	.	kIx.	.
</s>
<s>
Podporoval	podporovat	k5eAaImAgMnS	podporovat
ji	on	k3xPp3gFnSc4	on
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	on	k3xPp3gFnSc3	on
zařídil	zařídit	k5eAaPmAgInS	zařídit
studium	studium	k1gNnSc4	studium
v	v	k7c6	v
Rožňavě	Rožňava	k1gFnSc6	Rožňava
u	u	k7c2	u
kapelníka	kapelník	k1gMnSc2	kapelník
J.	J.	kA	J.
Nepomuka	Nepomuk	k1gMnSc2	Nepomuk
Palackého	Palacký	k1gMnSc2	Palacký
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtrnácti	čtrnáct	k4xCc6	čtrnáct
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
jiné	jiný	k2eAgInPc4d1	jiný
zdroje	zdroj	k1gInPc4	zdroj
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
devatenácti	devatenáct	k4xCc6	devatenáct
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c2	za
místního	místní	k2eAgMnSc2d1	místní
romského	romský	k2eAgMnSc2d1	romský
kováře	kovář	k1gMnSc2	kovář
-	-	kIx~	-
romského	romský	k2eAgMnSc4d1	romský
hudebníka	hudebník	k1gMnSc4	hudebník
<g/>
,	,	kIx,	,
violoncelistu	violoncelista	k1gMnSc4	violoncelista
Dányiho	Dányi	k1gMnSc4	Dányi
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1725	[number]	k4	1725
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
bratry	bratr	k1gMnPc7	bratr
založil	založit	k5eAaPmAgMnS	založit
kapelu	kapela	k1gFnSc4	kapela
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
Uhersku	Uhersko	k1gNnSc6	Uhersko
<g/>
,	,	kIx,	,
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgMnSc7d1	nový
primášem	primáš	k1gMnSc7	primáš
této	tento	k3xDgFnSc2	tento
kapely	kapela	k1gFnSc2	kapela
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gFnSc1	jeho
mladičká	mladičký	k2eAgFnSc1d1	mladičká
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
historické	historický	k2eAgInPc1d1	historický
zdroje	zdroj	k1gInPc1	zdroj
ji	on	k3xPp3gFnSc4	on
popisují	popisovat	k5eAaImIp3nP	popisovat
jako	jako	k8xC	jako
krásnou	krásný	k2eAgFnSc4d1	krásná
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc4d1	jiný
prameny	pramen	k1gInPc4	pramen
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
opak	opak	k1gInSc1	opak
<g/>
.	.	kIx.	.
</s>
<s>
Poznatky	poznatek	k1gInPc4	poznatek
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
jejího	její	k3xOp3gInSc2	její
vzhledu	vzhled	k1gInSc2	vzhled
se	se	k3xPyFc4	se
ale	ale	k9	ale
shodují	shodovat	k5eAaImIp3nP	shodovat
v	v	k7c4	v
tvrzení	tvrzení	k1gNnSc4	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
oblékala	oblékat	k5eAaImAgFnS	oblékat
do	do	k7c2	do
mužských	mužský	k2eAgInPc2d1	mužský
šatů	šat	k1gInPc2	šat
a	a	k8xC	a
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
s	s	k7c7	s
fajfkou	fajfka	k1gFnSc7	fajfka
v	v	k7c6	v
ústech	ústa	k1gNnPc6	ústa
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vystoupení	vystoupení	k1gNnSc4	vystoupení
své	svůj	k3xOyFgFnSc2	svůj
kapely	kapela	k1gFnSc2	kapela
si	se	k3xPyFc3	se
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
kostým	kostým	k1gInSc4	kostým
z	z	k7c2	z
kurucké	kurucký	k2eAgFnSc2d1	kurucký
uniformy	uniforma	k1gFnSc2	uniforma
a	a	k8xC	a
gemerského	gemerský	k2eAgInSc2d1	gemerský
kroje	kroj	k1gInSc2	kroj
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
mužem	muž	k1gMnSc7	muž
měli	mít	k5eAaImAgMnP	mít
čtyři	čtyři	k4xCgMnPc4	čtyři
syny	syn	k1gMnPc4	syn
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
dceru	dcera	k1gFnSc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
měla	mít	k5eAaImAgFnS	mít
až	až	k9	až
deset	deset	k4xCc4	deset
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
synové	syn	k1gMnPc1	syn
později	pozdě	k6eAd2	pozdě
tvořili	tvořit	k5eAaImAgMnP	tvořit
celou	celý	k2eAgFnSc4d1	celá
její	její	k3xOp3gFnSc4	její
kapelu	kapela	k1gFnSc4	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Hrála	hrát	k5eAaImAgFnS	hrát
i	i	k9	i
na	na	k7c6	na
svadbě	svadba	k1gFnSc6	svadba
u	u	k7c2	u
Pálffyovců	Pálffyovec	k1gInPc2	Pálffyovec
na	na	k7c6	na
zámečku	zámeček	k1gInSc6	zámeček
v	v	k7c6	v
Kráľové	Kráľové	k2eAgInSc6d1	Kráľové
pri	pri	k?	pri
Senci	Senec	k1gInSc6	Senec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
taky	taky	k6eAd1	taky
v	v	k7c6	v
Balassovském	Balassovský	k2eAgInSc6d1	Balassovský
paláci	palác	k1gInSc6	palác
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
rodu	rod	k1gInSc2	rod
Bakošovců	Bakošovec	k1gMnPc2	Bakošovec
si	se	k3xPyFc3	se
konkurovali	konkurovat	k5eAaImAgMnP	konkurovat
v	v	k7c6	v
hře	hra	k1gFnSc6	hra
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
císařovny	císařovna	k1gFnSc2	císařovna
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
hrála	hrát	k5eAaImAgFnS	hrát
dědečkovy	dědečkův	k2eAgFnPc4d1	dědečkova
písně	píseň	k1gFnPc4	píseň
jako	jako	k8xS	jako
například	například	k6eAd1	například
písně	píseň	k1gFnPc4	píseň
<g/>
:	:	kIx,	:
Třista	Třista	k1gFnSc1	Třista
vdov	vdova	k1gFnPc2	vdova
<g/>
,	,	kIx,	,
Tanec	tanec	k1gInSc1	tanec
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Pieseň	Pieseň	k1gFnSc1	Pieseň
prastarých	prastarý	k2eAgNnPc2d1	prastaré
otcov	otcovo	k1gNnPc2	otcovo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Káldyho	Káldy	k1gMnSc2	Káldy
její	její	k3xOp3gNnSc1	její
nejúspěšnější	úspěšný	k2eAgFnPc1d3	nejúspěšnější
skladby	skladba	k1gFnPc1	skladba
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1735	[number]	k4	1735
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
skladby	skladba	k1gFnPc1	skladba
vyšly	vyjít	k5eAaPmAgFnP	vyjít
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Kuruc	kuruc	k1gMnSc1	kuruc
Dalok	Dalok	k1gInSc1	Dalok
<g/>
.	.	kIx.	.
</s>
<s>
Žánrově	žánrově	k6eAd1	žánrově
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sbírce	sbírka	k1gFnSc6	sbírka
převažují	převažovat	k5eAaImIp3nP	převažovat
verbunky	verbunka	k1gFnPc1	verbunka
z	z	k7c2	z
pozdějšího	pozdní	k2eAgNnSc2d2	pozdější
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Káldy	Káldy	k6eAd1	Káldy
ji	on	k3xPp3gFnSc4	on
dále	daleko	k6eAd2	daleko
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
průkopnici	průkopnice	k1gFnSc4	průkopnice
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
zakladatelek	zakladatelka	k1gFnPc2	zakladatelka
tvorby	tvorba	k1gFnSc2	tvorba
kompozic	kompozice	k1gFnPc2	kompozice
zvané	zvaný	k2eAgFnSc2d1	zvaná
halgató	halgató	k?	halgató
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
žánru	žánr	k1gInSc6	žánr
vynikla	vyniknout	k5eAaPmAgFnS	vyniknout
její	její	k3xOp3gFnSc1	její
skladba	skladba	k1gFnSc1	skladba
Pomalé	pomalý	k2eAgFnSc2d1	pomalá
uherské	uherský	k2eAgFnSc2d1	uherská
<g/>
.	.	kIx.	.
</s>
<s>
Složila	složit	k5eAaPmAgFnS	složit
také	také	k9	také
píseň	píseň	k1gFnSc1	píseň
Vlašťovičko	Vlašťovička	k1gFnSc5	Vlašťovička
leť	letět	k5eAaImRp2nS	letět
<g/>
.	.	kIx.	.
</s>
<s>
Svá	svůj	k3xOyFgNnPc1	svůj
největší	veliký	k2eAgNnPc1d3	veliký
díla	dílo	k1gNnPc1	dílo
zachytila	zachytit	k5eAaPmAgNnP	zachytit
v	v	k7c6	v
souboru	soubor	k1gInSc6	soubor
s	s	k7c7	s
názvem	název	k1gInSc7	název
Uherská	uherský	k2eAgFnSc1d1	uherská
sbírka	sbírka	k1gFnSc1	sbírka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1730	[number]	k4	1730
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
64	[number]	k4	64
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
tanců	tanec	k1gInPc2	tanec
v	v	k7c6	v
maďarštině	maďarština	k1gFnSc6	maďarština
<g/>
,	,	kIx,	,
15	[number]	k4	15
písní	píseň	k1gFnPc2	píseň
ve	v	k7c6	v
slovenštině	slovenština	k1gFnSc6	slovenština
<g/>
,	,	kIx,	,
12	[number]	k4	12
tzv.	tzv.	kA	tzv.
suitských	suitský	k2eAgInPc2d1	suitský
tanců	tanec	k1gInPc2	tanec
a	a	k8xC	a
6	[number]	k4	6
maďarských	maďarský	k2eAgFnPc2d1	maďarská
kuruckých	kurucký	k2eAgFnPc2d1	kurucký
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Panny	Panna	k1gFnSc2	Panna
Cinkové	Cinková	k1gFnSc2	Cinková
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
i	i	k9	i
v	v	k7c6	v
zahraničních	zahraniční	k2eAgFnPc6d1	zahraniční
hudebních	hudební	k2eAgFnPc6d1	hudební
encyklopediích	encyklopedie	k1gFnPc6	encyklopedie
<g/>
.	.	kIx.	.
</s>
<s>
Slovenský	slovenský	k2eAgMnSc1d1	slovenský
literát	literát	k1gMnSc1	literát
Ján	Ján	k1gMnSc1	Ján
Botto	Botto	k1gNnSc4	Botto
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
zavzpomínal	zavzpomínat	k5eAaPmAgMnS	zavzpomínat
v	v	k7c6	v
pověsti	pověst	k1gFnSc6	pověst
Báj	báj	k1gFnSc1	báj
Maginhradu	Maginhrad	k1gInSc2	Maginhrad
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
vícero	vícero	k1gNnSc1	vícero
maďarských	maďarský	k2eAgMnPc2d1	maďarský
básníků	básník	k1gMnPc2	básník
a	a	k8xC	a
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Mór	mór	k1gInSc4	mór
Jókai	Jóka	k1gFnSc2	Jóka
<g/>
,	,	kIx,	,
Zoltán	Zoltán	k1gMnSc1	Zoltán
Kodály	Kodála	k1gFnSc2	Kodála
<g/>
,	,	kIx,	,
a	a	k8xC	a
Endre	Endr	k1gInSc5	Endr
Dózsa	Dózsa	k1gFnSc1	Dózsa
ji	on	k3xPp3gFnSc4	on
zobrazili	zobrazit	k5eAaPmAgMnP	zobrazit
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
dílech	dílo	k1gNnPc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Víceré	vícerý	k4xRyIgInPc1	vícerý
hudební	hudební	k2eAgInPc1d1	hudební
kodexy	kodex	k1gInPc1	kodex
uvádějí	uvádět	k5eAaImIp3nP	uvádět
některé	některý	k3yIgInPc1	některý
další	další	k2eAgInPc1d1	další
hudební	hudební	k2eAgInPc1d1	hudební
nástroje	nástroj	k1gInPc1	nástroj
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
hrála	hrát	k5eAaImAgFnS	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
tradičních	tradiční	k2eAgInPc2d1	tradiční
nástrojů	nástroj	k1gInPc2	nástroj
lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
citeru	citera	k1gFnSc4	citera
<g/>
,	,	kIx,	,
píšťalu	píšťala	k1gFnSc4	píšťala
<g/>
,	,	kIx,	,
fleuru	fleura	k1gFnSc4	fleura
<g/>
,	,	kIx,	,
kolovrátek	kolovrátek	k1gInSc4	kolovrátek
<g/>
,	,	kIx,	,
dudy	dudy	k1gFnPc4	dudy
<g/>
,	,	kIx,	,
fajfarku	fajfarka	k1gFnSc4	fajfarka
<g/>
,	,	kIx,	,
zvonce	zvonec	k1gInPc4	zvonec
či	či	k8xC	či
mulitánku	mulitánka	k1gFnSc4	mulitánka
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1772	[number]	k4	1772
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
61	[number]	k4	61
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
smrti	smrt	k1gFnSc6	smrt
její	její	k3xOp3gMnPc1	její
ctitelé	ctitel	k1gMnPc1	ctitel
<g/>
,	,	kIx,	,
básníci	básník	k1gMnPc1	básník
Gerhard	Gerhard	k1gMnSc1	Gerhard
a	a	k8xC	a
Gvadányi	Gvadány	k1gMnPc1	Gvadány
oslavovali	oslavovat	k5eAaImAgMnP	oslavovat
její	její	k3xOp3gFnPc4	její
lidské	lidský	k2eAgFnPc4d1	lidská
a	a	k8xC	a
hudební	hudební	k2eAgFnPc4d1	hudební
kvality	kvalita	k1gFnPc4	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Mnohými	mnohý	k2eAgMnPc7d1	mnohý
básníky	básník	k1gMnPc7	básník
byla	být	k5eAaImAgFnS	být
posmrtně	posmrtně	k6eAd1	posmrtně
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
romskou	romský	k2eAgFnSc4d1	romská
Sapfó	Sapfó	k1gFnSc4	Sapfó
<g/>
.	.	kIx.	.
článek	článek	k1gInSc1	článek
Panna	Panna	k1gFnSc1	Panna
Cinková	Cinková	k1gFnSc1	Cinková
-	-	kIx~	-
autor	autor	k1gMnSc1	autor
<g/>
:	:	kIx,	:
Jozef	Jozef	k1gMnSc1	Jozef
Drenko	Drenko	k1gNnSc4	Drenko
<g/>
,	,	kIx,	,
časopis	časopis	k1gInSc4	časopis
História	Histórium	k1gNnSc2	Histórium
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
2002	[number]	k4	2002
A.	A.	kA	A.
Mann	Mann	k1gMnSc1	Mann
(	(	kIx(	(
<g/>
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Neznámi	znám	k2eNgMnPc1d1	neznám
Rómovia	Rómovium	k1gNnPc4	Rómovium
<g/>
:	:	kIx,	:
zo	zo	k?	zo
života	život	k1gInSc2	život
a	a	k8xC	a
kultúry	kultúra	k1gFnSc2	kultúra
Cigánov-Rómov	Cigánov-Rómovo	k1gNnPc2	Cigánov-Rómovo
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Ister	Ister	k1gInSc1	Ister
Science	Science	k1gFnSc2	Science
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
-	-	kIx~	-
207	[number]	k4	207
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
900486	[number]	k4	900486
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Panna	Panna	k1gFnSc1	Panna
Cinková	Cinková	k1gFnSc1	Cinková
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Cinka	Cinka	k1gFnSc1	Cinka
Panna	Panna	k1gFnSc1	Panna
-	-	kIx~	-
romove	romov	k1gInSc5	romov
<g/>
.	.	kIx.	.
<g/>
radio	radio	k1gNnSc1	radio
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Biblis	Biblis	k1gInSc1	Biblis
<g/>
.	.	kIx.	.
<g/>
kniznica-rv	kniznicav	k1gInSc1	kniznica-rv
<g/>
.	.	kIx.	.
<g/>
sk	sk	k?	sk
</s>
