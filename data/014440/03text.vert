<s>
AMR	AMR	kA
(	(	kIx(
<g/>
síť	síť	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
AMR	AMR	kA
(	(	kIx(
<g/>
také	také	k9
AMRAD	AMRAD	kA
<g/>
)	)	kIx)
(	(	kIx(
<g/>
automatizovaný	automatizovaný	k2eAgInSc1d1
městský	městský	k2eAgInSc1d1
radiotelefon	radiotelefon	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mobilní	mobilní	k2eAgFnSc1d1
síť	síť	k1gFnSc1
vyvinutá	vyvinutý	k2eAgFnSc1d1
v	v	k7c6
Československu	Československo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
vyvinuta	vyvinout	k5eAaPmNgFnS
v	v	k7c6
pardubické	pardubický	k2eAgFnSc6d1
Tesle	Tesla	k1gFnSc6
<g/>
,	,	kIx,
zcela	zcela	k6eAd1
nezávisle	závisle	k6eNd1
na	na	k7c6
vývoji	vývoj	k1gInSc6
mobilních	mobilní	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
ve	v	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc1
první	první	k4xOgFnSc1
mobilní	mobilní	k2eAgFnSc1d1
síť	síť	k1gFnSc1
v	v	k7c6
rámci	rámec	k1gInSc6
tzv.	tzv.	kA
východního	východní	k2eAgInSc2d1
bloku	blok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
AMR	AMR	kA
začal	začít	k5eAaPmAgInS
již	již	k9
polovině	polovina	k1gFnSc6
70	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
minulého	minulý	k2eAgNnSc2d1
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebyla	být	k5eNaImAgFnS
v	v	k7c6
žádném	žádný	k3yNgInSc6
případě	případ	k1gInSc6
určena	určit	k5eAaPmNgFnS
pro	pro	k7c4
širokou	široký	k2eAgFnSc4d1
veřejnost	veřejnost	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
byla	být	k5eAaImAgFnS
určena	určit	k5eAaPmNgFnS
pro	pro	k7c4
účely	účel	k1gInPc4
správy	správa	k1gFnSc2
pošt	pošta	k1gFnPc2
a	a	k8xC
telekomunikací	telekomunikace	k1gFnPc2
<g/>
,	,	kIx,
pro	pro	k7c4
komunikaci	komunikace	k1gFnSc4
mezi	mezi	k7c7
pracovníky	pracovník	k1gMnPc7
na	na	k7c6
cestách	cesta	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgInSc2
důvodu	důvod	k1gInSc2
měla	mít	k5eAaImAgNnP
oproti	oproti	k7c3
mobilním	mobilní	k2eAgFnPc3d1
sítím	síť	k1gFnPc3
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
jak	jak	k8xC,k8xS
je	on	k3xPp3gInPc4
známe	znát	k5eAaImIp1nP
dnes	dnes	k6eAd1
<g/>
,	,	kIx,
mnoho	mnoho	k4c4
omezení	omezení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
chyběla	chybět	k5eAaImAgFnS
automatická	automatický	k2eAgFnSc1d1
lokalizace	lokalizace	k1gFnSc1
účastníka	účastník	k1gMnSc2
<g/>
,	,	kIx,
proto	proto	k8xC
bylo	být	k5eAaImAgNnS
při	při	k7c6
volání	volání	k1gNnSc6
nutné	nutný	k2eAgNnSc1d1
zadat	zadat	k5eAaPmF
UTO	UTO	kA
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
se	se	k3xPyFc4
účastník	účastník	k1gMnSc1
nachází	nacházet	k5eAaImIp3nS
</s>
<s>
pracovala	pracovat	k5eAaImAgFnS
pouze	pouze	k6eAd1
se	s	k7c7
4	#num#	k4
číselným	číselný	k2eAgInSc7d1
číslovacím	číslovací	k2eAgInSc7d1
prostorem	prostor	k1gInSc7
<g/>
,	,	kIx,
tzn.	tzn.	kA
maximální	maximální	k2eAgInSc1d1
počet	počet	k1gInSc1
účastníků	účastník	k1gMnPc2
byl	být	k5eAaImAgInS
9999	#num#	k4
</s>
<s>
nebyly	být	k5eNaImAgFnP
připravovány	připravovat	k5eAaImNgFnP
žádné	žádný	k3yNgFnPc1
jiné	jiný	k2eAgFnPc1d1
funkce	funkce	k1gFnPc1
než	než	k8xS
příchozí	příchozí	k1gMnPc4
a	a	k8xC
odchozí	odchozí	k2eAgInPc4d1
hovory	hovor	k1gInPc4
</s>
<s>
systém	systém	k1gInSc1
neposkytoval	poskytovat	k5eNaImAgInS
zahraniční	zahraniční	k2eAgNnPc4d1
telefonní	telefonní	k2eAgNnPc4d1
spojení	spojení	k1gNnPc4
</s>
<s>
systém	systém	k1gInSc1
nepodporoval	podporovat	k5eNaImAgInS
účtování	účtování	k1gNnSc4
hovorů	hovor	k1gInPc2
</s>
<s>
Díky	díky	k7c3
těmto	tento	k3xDgInPc3
omezením	omezení	k1gNnSc7
byl	být	k5eAaImAgInS
systém	systém	k1gInSc1
poměrně	poměrně	k6eAd1
jednoduchý	jednoduchý	k2eAgInSc1d1
a	a	k8xC
bylo	být	k5eAaImAgNnS
jej	on	k3xPp3gNnSc4
možné	možný	k2eAgNnSc1d1
rychle	rychle	k6eAd1
implementovat	implementovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Přesto	přesto	k8xC
že	že	k8xS
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
jednu	jeden	k4xCgFnSc4
síť	síť	k1gFnSc4
<g/>
,	,	kIx,
existovaly	existovat	k5eAaImAgInP
v	v	k7c6
určité	určitý	k2eAgFnSc6d1
době	doba	k1gFnSc6
tři	tři	k4xCgFnPc1
takové	takový	k3xDgFnPc1
sítě	síť	k1gFnPc1
</s>
<s>
experimentální	experimentální	k2eAgFnSc1d1
<g/>
,	,	kIx,
pracující	pracující	k2eAgFnSc1d1
na	na	k7c6
frekvenčním	frekvenční	k2eAgNnSc6d1
pásmu	pásmo	k1gNnSc6
162	#num#	k4
<g/>
/	/	kIx~
<g/>
167	#num#	k4
MHz	Mhz	kA
</s>
<s>
celorepubliková	celorepublikový	k2eAgFnSc1d1
(	(	kIx(
<g/>
161	#num#	k4
<g/>
/	/	kIx~
<g/>
165	#num#	k4
MHz	Mhz	kA
<g/>
)	)	kIx)
</s>
<s>
oblastní	oblastní	k2eAgFnSc1d1
(	(	kIx(
<g/>
152	#num#	k4
<g/>
/	/	kIx~
<g/>
157	#num#	k4
MHz	Mhz	kA
<g/>
)	)	kIx)
</s>
<s>
Přepínaní	přepínaný	k2eAgMnPc1d1
z	z	k7c2
jedné	jeden	k4xCgFnSc2
sítě	síť	k1gFnSc2
do	do	k7c2
druhé	druhý	k4xOgFnSc2
nebylo	být	k5eNaImAgNnS
možné	možný	k2eAgNnSc1d1
bez	bez	k7c2
výměny	výměna	k1gFnSc2
celého	celý	k2eAgInSc2d1
radiového	radiový	k2eAgInSc2d1
bloku	blok	k1gInSc2
zařízení	zařízení	k1gNnSc2
(	(	kIx(
<g/>
ovládací	ovládací	k2eAgFnSc1d1
sříňka	sříňka	k1gFnSc1
mohla	moct	k5eAaImAgFnS
zůstat	zůstat	k5eAaPmF
původní	původní	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
porovnání	porovnání	k1gNnSc6
se	s	k7c7
současnými	současný	k2eAgFnPc7d1
sítěmi	síť	k1gFnPc7
<g/>
,	,	kIx,
měla	mít	k5eAaImAgFnS
AMR	AMR	kA
síť	síť	k1gFnSc1
několik	několik	k4yIc4
zvláštností	zvláštnost	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Protože	protože	k8xS
bylo	být	k5eAaImAgNnS
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
někdo	někdo	k3yInSc1
nepovolaný	povolaný	k2eNgMnSc1d1
odposlouchával	odposlouchávat	k5eAaImAgInS
provoz	provoz	k1gInSc1
sítě	síť	k1gFnSc2
AMR	AMR	kA
<g/>
,	,	kIx,
nebyl	být	k5eNaImAgInS
tedy	tedy	k9
kmitočtově	kmitočtově	k6eAd1
modulovaný	modulovaný	k2eAgInSc1d1
radiový	radiový	k2eAgInSc1d1
signál	signál	k1gInSc1
nijak	nijak	k6eAd1
šifrován	šifrovat	k5eAaBmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
vhodnou	vhodný	k2eAgFnSc7d1
radiostanicí	radiostanice	k1gFnSc7
či	či	k8xC
přijímačem	přijímač	k1gInSc7
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
AMR	AMR	kA
odposlouchávat	odposlouchávat	k5eAaImF
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
později	pozdě	k6eAd2
NMT	NMT	kA
systém	systém	k1gInSc4
Eurotelu	Eurotel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Stejně	stejně	k6eAd1
tak	tak	k6eAd1
bylo	být	k5eAaImAgNnS
velmi	velmi	k6eAd1
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
někdo	někdo	k3yInSc1
nepovolaný	povolaný	k2eNgMnSc1d1
by	by	kYmCp3nS
vlastnil	vlastnit	k5eAaImAgMnS
radiostanici	radiostanice	k1gFnSc4
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
v	v	k7c6
provedení	provedení	k1gNnSc6
<g/>
,	,	kIx,
potřebném	potřebné	k1gNnSc6
pro	pro	k7c4
AMR	AMR	kA
(	(	kIx(
<g/>
duplexní	duplexní	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
síť	síť	k1gFnSc1
nepodporovala	podporovat	k5eNaImAgFnS
žádný	žádný	k3yNgInSc4
způsob	způsob	k1gInSc4
autentizace	autentizace	k1gFnSc2
účastníka	účastník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgNnPc1
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgNnP
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgInSc1d1
účel	účel	k1gInSc1
systému	systém	k1gInSc2
AMR	AMR	kA
totiž	totiž	k9
spočíval	spočívat	k5eAaImAgInS
především	především	k6eAd1
v	v	k7c6
zajištění	zajištění	k1gNnSc6
vzájemného	vzájemný	k2eAgNnSc2d1
spojení	spojení	k1gNnSc2
mezi	mezi	k7c7
spojovými	spojový	k2eAgFnPc7d1
organizacemi	organizace	k1gFnPc7
a	a	k8xC
jejími	její	k3xOp3gMnPc7
pracovníky	pracovník	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
důvodu	důvod	k1gInSc2
nebyla	být	k5eNaImAgFnS
v	v	k7c6
systému	systém	k1gInSc6
nikdy	nikdy	k6eAd1
zavedena	zaveden	k2eAgFnSc1d1
tarifikace	tarifikace	k1gFnSc1
hovorů	hovor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
po	po	k7c6
zahájení	zahájení	k1gNnSc6
provozu	provoz	k1gInSc2
celostátní	celostátní	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
se	se	k3xPyFc4
radiotelefony	radiotelefon	k1gInPc1
AMR	AMR	kA
rozšířily	rozšířit	k5eAaPmAgInP
i	i	k9
mezi	mezi	k7c4
mimoresortní	mimoresortní	k2eAgMnPc4d1
účastníky	účastník	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
proto	proto	k8xC
zaveden	zavést	k5eAaPmNgInS
poplatek	poplatek	k1gInSc1
za	za	k7c4
poskytovanou	poskytovaný	k2eAgFnSc4d1
službu	služba	k1gFnSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
obsahoval	obsahovat	k5eAaImAgInS
i	i	k9
hovorový	hovorový	k2eAgInSc4d1
paušál	paušál	k1gInSc4
a	a	k8xC
účastníkům	účastník	k1gMnPc3
byly	být	k5eAaImAgFnP
stanice	stanice	k1gFnPc4
pronajímány	pronajímán	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
byla	být	k5eAaImAgFnS
zavedena	zavést	k5eAaPmNgFnS
jakási	jakýsi	k3yIgFnSc1
"	"	kIx"
<g/>
autentikace	autentikace	k1gFnSc1
<g/>
"	"	kIx"
mobilních	mobilní	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
<g/>
,	,	kIx,
komplikující	komplikující	k2eAgNnSc1d1
zneužití	zneužití	k1gNnSc1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
autentikace	autentikace	k1gFnSc1
ale	ale	k9
byla	být	k5eAaImAgFnS
u	u	k7c2
všech	všecek	k3xTgFnPc2
stanic	stanice	k1gFnPc2
stejná	stejný	k2eAgFnSc1d1
a	a	k8xC
tak	tak	k6eAd1
opět	opět	k6eAd1
nesloužila	sloužit	k5eNaImAgFnS
k	k	k7c3
určení	určení	k1gNnSc3
konkrétního	konkrétní	k2eAgMnSc2d1
účastníka	účastník	k1gMnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
jen	jen	k9
k	k	k7c3
ochraně	ochrana	k1gFnSc3
systému	systém	k1gInSc2
před	před	k7c7
zneužitím	zneužití	k1gNnSc7
neoprávněnými	oprávněný	k2eNgNnPc7d1
voláními	volání	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
Několik	několik	k4yIc1
základních	základní	k2eAgFnPc2d1
technických	technický	k2eAgFnPc2d1
informací	informace	k1gFnPc2
o	o	k7c6
systému	systém	k1gInSc6
<g/>
:	:	kIx,
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
tehdejší	tehdejší	k2eAgFnSc3d1
naprosté	naprostý	k2eAgFnSc3d1
nedostupnosti	nedostupnost	k1gFnSc3
integrovaných	integrovaný	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
pro	pro	k7c4
DTMF	DTMF	kA
volbu	volba	k1gFnSc4
v	v	k7c6
zemích	zem	k1gFnPc6
RVHP	RVHP	kA
a	a	k8xC
zároveň	zároveň	k6eAd1
výborným	výborný	k2eAgFnPc3d1
zkušenostem	zkušenost	k1gFnPc3
s	s	k7c7
tónovou	tónový	k2eAgFnSc7d1
signalizací	signalizace	k1gFnSc7
Tesla	Tesla	k1gMnSc1
SELECTIC	SELECTIC	kA
používal	používat	k5eAaImAgMnS
systém	systém	k1gInSc4
naprosto	naprosto	k6eAd1
ojedinělou	ojedinělý	k2eAgFnSc4d1
signalizaci	signalizace	k1gFnSc4
a	a	k8xC
přenos	přenos	k1gInSc4
čísla	číslo	k1gNnSc2
(	(	kIx(
<g/>
postupnými	postupný	k2eAgInPc7d1
tóny	tón	k1gInPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vozidlová	vozidlový	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
pracovala	pracovat	k5eAaImAgFnS
s	s	k7c7
duplexním	duplexní	k2eAgInSc7d1
odstupem	odstup	k1gInSc7
-4,5	-4,5	k4
MHz	Mhz	kA
a	a	k8xC
VF	VF	kA
výkonem	výkon	k1gInSc7
10	#num#	k4
W.	W.	kA
Používané	používaný	k2eAgFnSc2d1
duplexery	duplexera	k1gFnSc2
(	(	kIx(
<g/>
duplexní	duplexní	k2eAgInPc1d1
filtry	filtr	k1gInPc1
<g/>
)	)	kIx)
byly	být	k5eAaImAgFnP
maďarské	maďarský	k2eAgFnPc1d1
výroby	výroba	k1gFnPc1
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
robustní	robustní	k2eAgFnPc1d1
a	a	k8xC
z	z	k7c2
kvalitních	kvalitní	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
jedné	jeden	k4xCgFnSc2
sítě	síť	k1gFnSc2
a	a	k8xC
jedné	jeden	k4xCgFnSc2
základnové	základnový	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
běželo	běžet	k5eAaImAgNnS
vždy	vždy	k6eAd1
několik	několik	k4yIc1
tzv.	tzv.	kA
rádiových	rádiový	k2eAgInPc2d1
stvolů	stvol	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
musel	muset	k5eAaImAgInS
operátor	operátor	k1gInSc1
telefonu	telefon	k1gInSc2
ručně	ručně	k6eAd1
"	"	kIx"
<g/>
zkontrolovat	zkontrolovat	k5eAaPmF
<g/>
"	"	kIx"
pomocí	pomocí	k7c2
voliče	volič	k1gInSc2
stvolů	stvol	k1gInPc2
na	na	k7c6
ovládací	ovládací	k2eAgFnSc6d1
skříňce	skříňka	k1gFnSc6
a	a	k8xC
teprve	teprve	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
našel	najít	k5eAaPmAgMnS
volný	volný	k2eAgInSc4d1
stvol	stvol	k1gInSc4
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
volat	volat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Mobilní	mobilní	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
po	po	k7c6
rádiu	rádius	k1gInSc6
potvrzovala	potvrzovat	k5eAaImAgFnS
příchozí	příchozí	k1gFnSc1
vyzvánění	vyzvánění	k1gNnSc2
<g/>
,	,	kIx,
takže	takže	k8xS
volající	volající	k2eAgMnSc1d1
byl	být	k5eAaImAgInS
informován	informovat	k5eAaBmNgMnS
<g/>
,	,	kIx,
zda	zda	k8xS
volání	volání	k1gNnSc1
"	"	kIx"
<g/>
dorazilo	dorazit	k5eAaPmAgNnS
<g/>
"	"	kIx"
do	do	k7c2
vozidla	vozidlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Mobilní	mobilní	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
indikovala	indikovat	k5eAaBmAgFnS
ztrátu	ztráta	k1gFnSc4
spojení	spojení	k1gNnSc3
se	s	k7c7
základnou	základna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
provozem	provoz	k1gInSc7
experimentální	experimentální	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
se	se	k3xPyFc4
začalo	začít	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1983	#num#	k4
byla	být	k5eAaImAgFnS
spuštěna	spustit	k5eAaPmNgFnS
celorepubliková	celorepublikový	k2eAgFnSc1d1
síť	síť	k1gFnSc1
AMR	AMR	kA
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
byly	být	k5eAaImAgFnP
zprovozněny	zprovoznit	k5eAaPmNgInP
"	"	kIx"
<g/>
oblastní	oblastní	k2eAgFnSc2d1
<g/>
"	"	kIx"
sítě	síť	k1gFnSc2
(	(	kIx(
<g/>
cca	cca	kA
o	o	k7c6
rozměru	rozměr	k1gInSc6
okresů	okres	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
ukončen	ukončit	k5eAaPmNgInS
provoz	provoz	k1gInSc1
experimentální	experimentální	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
devadesátých	devadesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
mobilní	mobilní	k2eAgFnPc1d1
stanice	stanice	k1gFnPc1
dočkaly	dočkat	k5eAaPmAgFnP
"	"	kIx"
<g/>
inovace	inovace	k1gFnSc1
<g/>
"	"	kIx"
modernějšími	moderní	k2eAgFnPc7d2
ovládacími	ovládací	k2eAgFnPc7d1
skříňkami	skříňka	k1gFnPc7
<g/>
,	,	kIx,
s	s	k7c7
displejem	displej	k1gInSc7
a	a	k8xC
řízenými	řízený	k2eAgInPc7d1
mikroprocesorem	mikroprocesor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc4
však	však	k9
už	už	k6eAd1
jen	jen	k9
labutí	labutí	k2eAgFnSc4d1
píseň	píseň	k1gFnSc4
systému	systém	k1gInSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc7
Achillovou	Achillův	k2eAgFnSc7d1
patou	pata	k1gFnSc7
byla	být	k5eAaImAgFnS
celková	celkový	k2eAgFnSc1d1
zastaralost	zastaralost	k1gFnSc1
návrhu	návrh	k1gInSc2
(	(	kIx(
<g/>
málo	málo	k1gNnSc1
kanálů	kanál	k1gInPc2
<g/>
,	,	kIx,
chybějící	chybějící	k2eAgNnSc1d1
zabezpečení	zabezpečení	k1gNnSc1
a	a	k8xC
tarifikace	tarifikace	k1gFnSc1
<g/>
,	,	kIx,
malá	malý	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
číslování	číslování	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Provoz	provoz	k1gInSc1
systému	systém	k1gInSc2
AMR	AMR	kA
byl	být	k5eAaImAgInS
definitivně	definitivně	k6eAd1
ukončen	ukončit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Informace	informace	k1gFnPc1
o	o	k7c6
systému	systém	k1gInSc6
TESLA	Tesla	k1gMnSc1
AMR	AMR	kA
a	a	k8xC
o	o	k7c6
radiostanicích	radiostanice	k1gFnPc6
tesla	tesla	k1gFnSc1
byly	být	k5eAaImAgFnP
čerpány	čerpat	k5eAaImNgFnP
z	z	k7c2
<g/>
:	:	kIx,
</s>
<s>
Stránky	stránka	k1gFnPc1
p.	p.	k?
Kováče	Kováč	k1gMnSc4
<g/>
,	,	kIx,
http://www.kovacradio.estranky.cz/	http://www.kovacradio.estranky.cz/	k?
</s>
<s>
Jan	Jan	k1gMnSc1
Bednář	Bednář	k1gMnSc1
<g/>
,	,	kIx,
http://amr.nazory.cz/	http://amr.nazory.cz/	k?
</s>
<s>
informace	informace	k1gFnPc1
OK2DV	OK2DV	k1gFnSc2
</s>
<s>
informace	informace	k1gFnPc1
OK1UHU	OK1UHU	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Standardy	standard	k1gInPc1
mobilních	mobilní	k2eAgInPc2d1
telefonů	telefon	k1gInPc2
0G	0G	k4
</s>
<s>
PTT	PTT	kA
</s>
<s>
MTS	MTS	kA
</s>
<s>
IMTS	IMTS	kA
</s>
<s>
AMTS	AMTS	kA
</s>
<s>
AMR	AMR	kA
</s>
<s>
0.5	0.5	k4
<g/>
G	G	kA
<g/>
:	:	kIx,
Autotel	Autotel	k1gMnSc1
<g/>
/	/	kIx~
<g/>
PALM	PALM	kA
</s>
<s>
ARP	ARP	kA
1G	1G	k4
</s>
<s>
NMT	NMT	kA
</s>
<s>
AMPS	AMPS	kA
2G	2G	k4
</s>
<s>
GSM	GSM	kA
</s>
<s>
cdmaOne	cdmaOnout	k5eAaImIp3nS,k5eAaPmIp3nS
</s>
<s>
iDEN	iDEN	k?
</s>
<s>
D-AMPS	D-AMPS	k?
<g/>
/	/	kIx~
<g/>
IS-	IS-	k1gFnSc1
<g/>
136	#num#	k4
<g/>
/	/	kIx~
<g/>
TDMA	TDMA	kA
</s>
<s>
PDC	PDC	kA
</s>
<s>
2.5	2.5	k4
<g/>
G	G	kA
<g/>
:	:	kIx,
GPRS	GPRS	kA
</s>
<s>
2.75	2.75	k4
<g/>
G	G	kA
<g/>
:	:	kIx,
CDMA2000	CDMA2000	k1gFnSc1
1	#num#	k4
<g/>
xRTT	xRTT	k?
</s>
<s>
EDGE	EDGE	kA
</s>
<s>
EGPRS	EGPRS	kA
3G	3G	k4
</s>
<s>
W-CDMA	W-CDMA	k?
</s>
<s>
UMTS	UMTS	kA
</s>
<s>
FOMA	FOMA	kA
</s>
<s>
CDMA2000	CDMA2000	k4
1	#num#	k4
<g/>
xEV	xEV	k?
</s>
<s>
TD-SCDMA	TD-SCDMA	k?
</s>
<s>
3.5	3.5	k4
<g/>
G	G	kA
<g/>
:	:	kIx,
HSDPA	HSDPA	kA
</s>
<s>
3.75	3.75	k4
<g/>
G	G	kA
<g/>
:	:	kIx,
HSUPA	HSUPA	kA
Pre-	Pre-	k1gFnSc1
<g/>
4	#num#	k4
<g/>
G	G	kA
</s>
<s>
Mobile	mobile	k1gNnSc1
WiMAX	WiMAX	k1gFnSc2
(	(	kIx(
<g/>
IEEE	IEEE	kA
802.16	802.16	k4
<g/>
e	e	k0
<g/>
)	)	kIx)
</s>
<s>
LTE	LTE	kA
(	(	kIx(
<g/>
E-UTRA	E-UTRA	k1gMnSc1
<g/>
)	)	kIx)
4G	4G	k4
</s>
<s>
WiMAX-Advanced	WiMAX-Advanced	k1gMnSc1
(	(	kIx(
<g/>
IEEE	IEEE	kA
802.16	802.16	k4
<g/>
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
LTE	LTE	kA
Advanced	Advanced	k1gInSc4
5G	5G	k4
</s>
<s>
eMBB	eMBB	k?
</s>
<s>
URLLC	URLLC	kA
</s>
<s>
MMTC	MMTC	kA
</s>
