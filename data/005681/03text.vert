<s>
Vitraj	Vitraj	k1gFnSc1	Vitraj
(	(	kIx(	(
<g/>
z	z	k7c2	z
franc	franc	k1gFnSc1	franc
<g/>
.	.	kIx.	.
vitrail	vitrail	k1gInSc1	vitrail
stejného	stejný	k2eAgInSc2d1	stejný
významu	význam	k1gInSc2	význam
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
nepřesné	přesný	k2eNgFnPc1d1	nepřesná
(	(	kIx(	(
<g/>
hovorové	hovorový	k2eAgFnPc1d1	hovorová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mnohem	mnohem	k6eAd1	mnohem
běžnější	běžný	k2eAgFnSc1d2	běžnější
vitráž	vitráž	k1gFnSc1	vitráž
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
psané	psaný	k2eAgFnSc2d1	psaná
vitrage	vitrag	k1gFnSc2	vitrag
znamená	znamenat	k5eAaImIp3nS	znamenat
zasklení	zasklení	k1gNnSc1	zasklení
<g/>
,	,	kIx,	,
prosklená	prosklený	k2eAgFnSc1d1	prosklená
plocha	plocha	k1gFnSc1	plocha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
výtvarně	výtvarně	k6eAd1	výtvarně
pojednaná	pojednaný	k2eAgFnSc1d1	pojednaná
skleněná	skleněný	k2eAgFnSc1d1	skleněná
mozaiková	mozaikový	k2eAgFnSc1d1	mozaiková
výplň	výplň	k1gFnSc1	výplň
skládaná	skládaný	k2eAgFnSc1d1	skládaná
do	do	k7c2	do
olova	olovo	k1gNnSc2	olovo
z	z	k7c2	z
obvykle	obvykle	k6eAd1	obvykle
barevných	barevný	k2eAgNnPc2d1	barevné
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
i	i	k9	i
malovaných	malovaný	k2eAgNnPc2d1	malované
malých	malý	k2eAgNnPc2d1	malé
skel	sklo	k1gNnPc2	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
dekorativní	dekorativní	k2eAgFnSc1d1	dekorativní
alternativa	alternativa	k1gFnSc1	alternativa
skleněných	skleněný	k2eAgFnPc2d1	skleněná
tabulí	tabule	k1gFnPc2	tabule
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
kostelních	kostelní	k2eAgNnPc6d1	kostelní
oknech	okno	k1gNnPc6	okno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
jde	jít	k5eAaImIp3nS	jít
také	také	k9	také
o	o	k7c4	o
název	název	k1gInSc4	název
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
výrobě	výroba	k1gFnSc3	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Barevné	barevný	k2eAgFnPc1d1	barevná
mozaikové	mozaikový	k2eAgFnPc1d1	mozaiková
výplně	výplň	k1gFnPc1	výplň
oken	okno	k1gNnPc2	okno
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
již	již	k6eAd1	již
od	od	k7c2	od
antiky	antika	k1gFnSc2	antika
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
rozvoj	rozvoj	k1gInSc4	rozvoj
klasické	klasický	k2eAgFnSc2d1	klasická
vitráže	vitráž	k1gFnSc2	vitráž
však	však	k9	však
souvisí	souviset	k5eAaImIp3nS	souviset
zejména	zejména	k9	zejména
se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
středověkých	středověký	k2eAgFnPc2d1	středověká
katedrál	katedrála	k1gFnPc2	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
technologie	technologie	k1gFnSc1	technologie
neumožňovala	umožňovat	k5eNaImAgFnS	umožňovat
výrobu	výroba	k1gFnSc4	výroba
velkých	velký	k2eAgFnPc2d1	velká
skleněných	skleněný	k2eAgFnPc2d1	skleněná
tabulí	tabule	k1gFnPc2	tabule
<g/>
,	,	kIx,	,
rozměrné	rozměrný	k2eAgInPc4d1	rozměrný
otvory	otvor	k1gInPc4	otvor
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
vyplňovat	vyplňovat	k5eAaImF	vyplňovat
tabulemi	tabule	k1gFnPc7	tabule
ze	z	k7c2	z
spojovaných	spojovaný	k2eAgFnPc2d1	spojovaná
skel	sklo	k1gNnPc2	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
jen	jen	k9	jen
vynucen	vynucen	k2eAgInSc1d1	vynucen
technickou	technický	k2eAgFnSc7d1	technická
nevyspělostí	nevyspělost	k1gFnSc7	nevyspělost
<g/>
,	,	kIx,	,
velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc1	význam
vitráží	vitráž	k1gFnPc2	vitráž
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
např.	např.	kA	např.
u	u	k7c2	u
středověkého	středověký	k2eAgMnSc2d1	středověký
ideologa	ideolog	k1gMnSc2	ideolog
katedrální	katedrální	k2eAgFnSc2d1	katedrální
architektury	architektura	k1gFnSc2	architektura
<g/>
,	,	kIx,	,
opata	opat	k1gMnSc2	opat
Sugera	Suger	k1gMnSc2	Suger
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
přikládal	přikládat	k5eAaImAgMnS	přikládat
světlu	světlo	k1gNnSc3	světlo
a	a	k8xC	a
barvě	barva	k1gFnSc3	barva
z	z	k7c2	z
velkých	velký	k2eAgNnPc2d1	velké
oken	okno	k1gNnPc2	okno
mimořádný	mimořádný	k2eAgInSc4d1	mimořádný
teologický	teologický	k2eAgInSc4d1	teologický
a	a	k8xC	a
liturgický	liturgický	k2eAgInSc4d1	liturgický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Spojování	spojování	k1gNnSc1	spojování
barevných	barevný	k2eAgNnPc2d1	barevné
skel	sklo	k1gNnPc2	sklo
se	se	k3xPyFc4	se
vyprofilovalo	vyprofilovat	k5eAaPmAgNnS	vyprofilovat
jako	jako	k9	jako
samostatné	samostatný	k2eAgNnSc1d1	samostatné
umělecké	umělecký	k2eAgNnSc1d1	umělecké
řemeslo	řemeslo	k1gNnSc1	řemeslo
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
technika	technika	k1gFnSc1	technika
vitráže	vitráž	k1gFnSc2	vitráž
používala	používat	k5eAaImAgFnS	používat
převážně	převážně	k6eAd1	převážně
pro	pro	k7c4	pro
chrámy	chrám	k1gInPc4	chrám
<g/>
,	,	kIx,	,
kláštery	klášter	k1gInPc4	klášter
a	a	k8xC	a
kostely	kostel	k1gInPc4	kostel
<g/>
,	,	kIx,	,
v	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
pronikla	proniknout	k5eAaPmAgFnS	proniknout
i	i	k9	i
do	do	k7c2	do
světských	světský	k2eAgFnPc2d1	světská
prostor	prostora	k1gFnPc2	prostora
a	a	k8xC	a
domácností	domácnost	k1gFnPc2	domácnost
–	–	k?	–
zdobí	zdobit	k5eAaImIp3nS	zdobit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
okna	okno	k1gNnPc4	okno
a	a	k8xC	a
dveře	dveře	k1gFnPc4	dveře
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnPc4	jejich
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
s	s	k7c7	s
menšími	malý	k2eAgInPc7d2	menší
díly	díl	k1gInPc7	díl
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc1	rozvoj
jemnějších	jemný	k2eAgFnPc2d2	jemnější
<g/>
,	,	kIx,	,
přesnějších	přesný	k2eAgFnPc2d2	přesnější
technik	technika	k1gFnPc2	technika
vedlo	vést	k5eAaImAgNnS	vést
mj.	mj.	kA	mj.
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
různých	různý	k2eAgInPc2d1	různý
dekorativních	dekorativní	k2eAgInPc2d1	dekorativní
předmětů	předmět	k1gInPc2	předmět
a	a	k8xC	a
šperků	šperk	k1gInPc2	šperk
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
klasické	klasický	k2eAgFnSc2d1	klasická
vitráže	vitráž	k1gFnSc2	vitráž
se	se	k3xPyFc4	se
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
díly	díl	k1gInPc1	díl
řežou	řezat	k5eAaImIp3nP	řezat
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
skel	sklo	k1gNnPc2	sklo
podle	podle	k7c2	podle
výtvarného	výtvarný	k2eAgInSc2d1	výtvarný
návrhu	návrh	k1gInSc2	návrh
(	(	kIx(	(
<g/>
kartónu	kartón	k1gInSc2	kartón
<g/>
)	)	kIx)	)
a	a	k8xC	a
sesazují	sesazovat	k5eAaImIp3nP	sesazovat
do	do	k7c2	do
olověných	olověný	k2eAgInPc2d1	olověný
pásků	pásek	k1gInPc2	pásek
s	s	k7c7	s
průřezem	průřez	k1gInSc7	průřez
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
písmene	písmeno	k1gNnSc2	písmeno
H	H	kA	H
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
spojích	spoj	k1gFnPc6	spoj
pájí	pájet	k5eAaImIp3nP	pájet
cínem	cín	k1gInSc7	cín
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
novějších	nový	k2eAgFnPc2d2	novější
vitráží	vitráž	k1gFnPc2	vitráž
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
trvanlivosti	trvanlivost	k1gFnSc3	trvanlivost
povrch	povrch	k7c2wR	povrch
olověných	olověný	k2eAgInPc2d1	olověný
pásků	pásek	k1gInPc2	pásek
cínuje	cínovat	k5eAaImIp3nS	cínovat
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
profilů	profil	k1gInPc2	profil
z	z	k7c2	z
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
mosazi	mosaz	k1gFnSc2	mosaz
nebo	nebo	k8xC	nebo
zinku	zinek	k1gInSc2	zinek
<g/>
.	.	kIx.	.
</s>
<s>
Osazované	osazovaný	k2eAgInPc1d1	osazovaný
otvory	otvor	k1gInPc1	otvor
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
přizpůsobeny	přizpůsobit	k5eAaPmNgInP	přizpůsobit
větší	veliký	k2eAgFnSc1d2	veliký
tloušťce	tloušťka	k1gFnSc3	tloušťka
a	a	k8xC	a
váze	váha	k1gFnSc3	váha
vitráží	vitráž	k1gFnPc2	vitráž
<g/>
.	.	kIx.	.
</s>
<s>
Technika	technika	k1gFnSc1	technika
Tiffany	Tiffana	k1gFnSc2	Tiffana
není	být	k5eNaImIp3nS	být
vitráž	vitráž	k1gFnSc1	vitráž
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
výsledek	výsledek	k1gInSc1	výsledek
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
výroby	výroba	k1gFnSc2	výroba
přišel	přijít	k5eAaPmAgInS	přijít
Louis	louis	k1gInSc1	louis
Comfort	Comfort	k1gInSc4	Comfort
Tiffany	Tiffana	k1gFnSc2	Tiffana
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
olověného	olověný	k2eAgInSc2d1	olověný
profilu	profil	k1gInSc2	profil
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
měděná	měděný	k2eAgFnSc1d1	měděná
fólie	fólie	k1gFnSc1	fólie
-	-	kIx~	-
páska	páska	k1gFnSc1	páska
různých	různý	k2eAgFnPc2d1	různá
šířek	šířka	k1gFnPc2	šířka
<g/>
,	,	kIx,	,
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
je	být	k5eAaImIp3nS	být
opatřena	opatřit	k5eAaPmNgFnS	opatřit
silným	silný	k2eAgNnSc7d1	silné
lepidlem	lepidlo	k1gNnSc7	lepidlo
(	(	kIx(	(
<g/>
páska	pásek	k1gMnSc4	pásek
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pod	pod	k7c7	pod
lepidlem	lepidlo	k1gNnSc7	lepidlo
i	i	k8xC	i
barvená	barvený	k2eAgFnSc1d1	barvená
<g/>
,	,	kIx,	,
černá	černý	k2eAgFnSc1d1	černá
nebo	nebo	k8xC	nebo
postříbřená	postříbřený	k2eAgFnSc1d1	postříbřená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spojování	spojování	k1gNnSc1	spojování
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
opáskovaných	opáskovaný	k2eAgInPc2d1	opáskovaný
dílků	dílek	k1gInPc2	dílek
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
pomocí	pomocí	k7c2	pomocí
cínu	cín	k1gInSc2	cín
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
použít	použít	k5eAaPmF	použít
i	i	k9	i
menší	malý	k2eAgInPc4d2	menší
kousky	kousek	k1gInPc4	kousek
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
výsledný	výsledný	k2eAgInSc1d1	výsledný
předmět	předmět	k1gInSc1	předmět
má	mít	k5eAaImIp3nS	mít
jemnější	jemný	k2eAgFnPc4d2	jemnější
–	–	k?	–
tenčí	tenký	k2eAgFnPc4d2	tenčí
spoje	spoj	k1gFnPc4	spoj
<g/>
.	.	kIx.	.
</s>
<s>
Barevná	barevný	k2eAgNnPc1d1	barevné
skla	sklo	k1gNnPc1	sklo
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
techniku	technika	k1gFnSc4	technika
přesně	přesně	k6eAd1	přesně
zabroušená	zabroušený	k2eAgFnSc1d1	zabroušená
(	(	kIx(	(
<g/>
speciální	speciální	k2eAgFnSc7d1	speciální
bruskou	bruska	k1gFnSc7	bruska
na	na	k7c4	na
sklo	sklo	k1gNnSc4	sklo
s	s	k7c7	s
hlavou	hlava	k1gFnSc7	hlava
s	s	k7c7	s
diamantovým	diamantový	k2eAgInSc7d1	diamantový
prachem	prach	k1gInSc7	prach
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skla	sklo	k1gNnPc1	sklo
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
čirá	čirý	k2eAgFnSc1d1	čirá
<g/>
,	,	kIx,	,
opálová	opálový	k2eAgFnSc1d1	opálová
<g/>
,	,	kIx,	,
poloopálová	poloopálový	k2eAgFnSc1d1	poloopálový
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
s	s	k7c7	s
různým	různý	k2eAgInSc7d1	různý
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
techniky	technika	k1gFnSc2	technika
Tiffany	Tiffana	k1gFnSc2	Tiffana
lze	lze	k6eAd1	lze
vytvořit	vytvořit	k5eAaPmF	vytvořit
i	i	k9	i
prostorové	prostorový	k2eAgInPc1d1	prostorový
objekty	objekt	k1gInPc1	objekt
(	(	kIx(	(
<g/>
např.	např.	kA	např.
lampy	lampa	k1gFnSc2	lampa
nebo	nebo	k8xC	nebo
menší	malý	k2eAgInPc4d2	menší
závěsné	závěsný	k2eAgInPc4d1	závěsný
předměty	předmět	k1gInPc4	předmět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
běžného	běžný	k2eAgNnSc2d1	běžné
skla	sklo	k1gNnSc2	sklo
lze	lze	k6eAd1	lze
různým	různý	k2eAgInSc7d1	různý
způsobem	způsob	k1gInSc7	způsob
aplikovat	aplikovat	k5eAaBmF	aplikovat
imitaci	imitace	k1gFnSc4	imitace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zdánlivě	zdánlivě	k6eAd1	zdánlivě
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
efekt	efekt	k1gInSc1	efekt
klasické	klasický	k2eAgFnSc2d1	klasická
vitráže	vitráž	k1gFnSc2	vitráž
<g/>
.	.	kIx.	.
</s>
<s>
Nepravá	pravý	k2eNgFnSc1d1	nepravá
(	(	kIx(	(
<g/>
falešná	falešný	k2eAgFnSc1d1	falešná
<g/>
)	)	kIx)	)
vitráž	vitráž	k1gFnSc1	vitráž
se	se	k3xPyFc4	se
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
pouze	pouze	k6eAd1	pouze
pomocí	pomocí	k7c2	pomocí
barev	barva	k1gFnPc2	barva
na	na	k7c4	na
sklo	sklo	k1gNnSc4	sklo
a	a	k8xC	a
nalepovací	nalepovací	k2eAgFnPc4d1	nalepovací
olověné	olověný	k2eAgFnPc4d1	olověná
kontury	kontura	k1gFnPc4	kontura
nebo	nebo	k8xC	nebo
kontury	kontura	k1gFnPc4	kontura
z	z	k7c2	z
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
princip	princip	k1gInSc1	princip
vitráže	vitráž	k1gFnSc2	vitráž
(	(	kIx(	(
<g/>
skládání	skládání	k1gNnSc4	skládání
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
kousků	kousek	k1gInPc2	kousek
skla	sklo	k1gNnSc2	sklo
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
tedy	tedy	k9	tedy
neuplatňuje	uplatňovat	k5eNaImIp3nS	uplatňovat
<g/>
.	.	kIx.	.
</s>
