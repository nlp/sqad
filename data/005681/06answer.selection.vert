<s>
Vitraj	Vitraj	k1gFnSc1	Vitraj
(	(	kIx(	(
<g/>
z	z	k7c2	z
franc	franc	k1gFnSc1	franc
<g/>
.	.	kIx.	.
vitrail	vitrail	k1gInSc1	vitrail
stejného	stejný	k2eAgInSc2d1	stejný
významu	význam	k1gInSc2	význam
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
nepřesné	přesný	k2eNgFnPc1d1	nepřesná
(	(	kIx(	(
<g/>
hovorové	hovorový	k2eAgFnPc1d1	hovorová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mnohem	mnohem	k6eAd1	mnohem
běžnější	běžný	k2eAgFnSc1d2	běžnější
vitráž	vitráž	k1gFnSc1	vitráž
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
psané	psaný	k2eAgFnSc2d1	psaná
vitrage	vitrag	k1gFnSc2	vitrag
znamená	znamenat	k5eAaImIp3nS	znamenat
zasklení	zasklení	k1gNnSc1	zasklení
<g/>
,	,	kIx,	,
prosklená	prosklený	k2eAgFnSc1d1	prosklená
plocha	plocha	k1gFnSc1	plocha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
výtvarně	výtvarně	k6eAd1	výtvarně
pojednaná	pojednaný	k2eAgFnSc1d1	pojednaná
skleněná	skleněný	k2eAgFnSc1d1	skleněná
mozaiková	mozaikový	k2eAgFnSc1d1	mozaiková
výplň	výplň	k1gFnSc1	výplň
skládaná	skládaný	k2eAgFnSc1d1	skládaná
do	do	k7c2	do
olova	olovo	k1gNnSc2	olovo
z	z	k7c2	z
obvykle	obvykle	k6eAd1	obvykle
barevných	barevný	k2eAgNnPc2d1	barevné
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
i	i	k9	i
malovaných	malovaný	k2eAgNnPc2d1	malované
malých	malý	k2eAgNnPc2d1	malé
skel	sklo	k1gNnPc2	sklo
<g/>
.	.	kIx.	.
</s>
