<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
je	být	k5eAaImIp3nS	být
geometrická	geometrický	k2eAgFnSc1d1	geometrická
teorie	teorie	k1gFnSc1	teorie
gravitace	gravitace	k1gFnSc1	gravitace
<g/>
,	,	kIx,	,
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
Albertem	Albert	k1gMnSc7	Albert
Einsteinem	Einstein	k1gMnSc7	Einstein
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
aktuální	aktuální	k2eAgInSc4d1	aktuální
popis	popis	k1gInSc4	popis
gravitace	gravitace	k1gFnSc2	gravitace
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
fyzice	fyzika	k1gFnSc6	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
zobecňuje	zobecňovat	k5eAaImIp3nS	zobecňovat
speciální	speciální	k2eAgFnSc4d1	speciální
relativitu	relativita	k1gFnSc4	relativita
a	a	k8xC	a
Newtonův	Newtonův	k2eAgInSc4d1	Newtonův
gravitační	gravitační	k2eAgInSc4d1	gravitační
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
jednotný	jednotný	k2eAgInSc4d1	jednotný
popis	popis	k1gInSc4	popis
gravitace	gravitace	k1gFnSc2	gravitace
jako	jako	k8xS	jako
geometrické	geometrický	k2eAgFnPc4d1	geometrická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
času	čas	k1gInSc2	čas
neboli	neboli	k8xC	neboli
časoprostoru	časoprostor	k1gInSc2	časoprostor
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
zakřivení	zakřivení	k1gNnSc1	zakřivení
časoprostoru	časoprostor	k1gInSc2	časoprostor
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
energii	energie	k1gFnSc3	energie
a	a	k8xC	a
hybnosti	hybnost	k1gFnSc3	hybnost
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
hmoty	hmota	k1gFnSc2	hmota
nebo	nebo	k8xC	nebo
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
Einsteinovými	Einsteinův	k2eAgFnPc7d1	Einsteinova
rovnicemi	rovnice	k1gFnPc7	rovnice
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
systémem	systém	k1gInSc7	systém
parciálních	parciální	k2eAgFnPc2d1	parciální
diferenciálních	diferenciální	k2eAgFnPc2d1	diferenciální
rovnic	rovnice	k1gFnPc2	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
předpovědi	předpověď	k1gFnPc1	předpověď
obecné	obecný	k2eAgFnSc2d1	obecná
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
klasické	klasický	k2eAgFnSc2d1	klasická
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
plynutí	plynutí	k1gNnSc4	plynutí
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
geometrii	geometrie	k1gFnSc4	geometrie
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
pohyb	pohyb	k1gInSc4	pohyb
těles	těleso	k1gNnPc2	těleso
při	při	k7c6	při
volném	volný	k2eAgInSc6d1	volný
pádu	pád	k1gInSc6	pád
a	a	k8xC	a
šíření	šíření	k1gNnSc6	šíření
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
příkladům	příklad	k1gInPc3	příklad
takových	takový	k3xDgInPc2	takový
rozdílů	rozdíl	k1gInPc2	rozdíl
patří	patřit	k5eAaImIp3nS	patřit
gravitační	gravitační	k2eAgFnSc1d1	gravitační
dilatace	dilatace	k1gFnSc1	dilatace
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
gravitační	gravitační	k2eAgFnSc2d1	gravitační
čočky	čočka	k1gFnSc2	čočka
<g/>
,	,	kIx,	,
gravitační	gravitační	k2eAgInSc1d1	gravitační
rudý	rudý	k2eAgInSc1d1	rudý
posuv	posuv	k1gInSc1	posuv
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
gravitační	gravitační	k2eAgNnSc4d1	gravitační
časové	časový	k2eAgNnSc4d1	časové
zpoždění	zpoždění	k1gNnSc4	zpoždění
<g/>
.	.	kIx.	.
</s>
<s>
Předpovědi	předpověď	k1gFnPc1	předpověď
obecné	obecný	k2eAgFnSc2d1	obecná
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
byly	být	k5eAaImAgInP	být
potvrzeny	potvrdit	k5eAaPmNgInP	potvrdit
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
pozorováních	pozorování	k1gNnPc6	pozorování
a	a	k8xC	a
pokusech	pokus	k1gInPc6	pokus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
doposud	doposud	k6eAd1	doposud
provedeny	provést	k5eAaPmNgInP	provést
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
není	být	k5eNaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
relativistická	relativistický	k2eAgFnSc1d1	relativistická
teorie	teorie	k1gFnSc1	teorie
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
nejjednodušší	jednoduchý	k2eAgFnSc1d3	nejjednodušší
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
experimentálními	experimentální	k2eAgNnPc7d1	experimentální
daty	datum	k1gNnPc7	datum
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
i	i	k9	i
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
nezodpovězené	zodpovězený	k2eNgFnPc1d1	nezodpovězená
otázky	otázka	k1gFnPc1	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Nejpodstatnější	podstatný	k2eAgMnSc1d3	nejpodstatnější
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
zákony	zákon	k1gInPc7	zákon
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
mohli	moct	k5eAaImAgMnP	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
kompletní	kompletní	k2eAgFnSc4d1	kompletní
konzistentní	konzistentní	k2eAgFnSc4d1	konzistentní
teorii	teorie	k1gFnSc4	teorie
kvantové	kvantový	k2eAgFnSc2d1	kvantová
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
Einsteinova	Einsteinův	k2eAgFnSc1d1	Einsteinova
teorie	teorie	k1gFnSc1	teorie
má	mít	k5eAaImIp3nS	mít
důležité	důležitý	k2eAgInPc4d1	důležitý
astrofyzikální	astrofyzikální	k2eAgInPc4d1	astrofyzikální
důsledky	důsledek	k1gInPc4	důsledek
<g/>
.	.	kIx.	.
</s>
<s>
Implikuje	implikovat	k5eAaImIp3nS	implikovat
například	například	k6eAd1	například
<g/>
,	,	kIx,	,
že	že	k8xS	že
existují	existovat	k5eAaImIp3nP	existovat
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
-	-	kIx~	-
oblastí	oblast	k1gFnSc7	oblast
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
prostor	prostor	k1gInSc4	prostor
a	a	k8xC	a
čas	čas	k1gInSc4	čas
jsou	být	k5eAaImIp3nP	být
zkřiveny	zkřiven	k2eAgFnPc1d1	zkřivena
takovým	takový	k3xDgInSc7	takový
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
že	že	k8xS	že
nic	nic	k3yNnSc1	nic
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
nemohou	moct	k5eNaImIp3nP	moct
uniknout	uniknout	k5eAaPmF	uniknout
-	-	kIx~	-
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
konečný	konečný	k2eAgInSc4d1	konečný
stav	stav	k1gInSc4	stav
pro	pro	k7c4	pro
hmotné	hmotný	k2eAgFnPc4d1	hmotná
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
dostatek	dostatek	k1gInSc4	dostatek
důkazů	důkaz	k1gInPc2	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
intenzivní	intenzivní	k2eAgNnSc1d1	intenzivní
záření	záření	k1gNnSc1	záření
vydávané	vydávaný	k2eAgFnSc2d1	vydávaná
některými	některý	k3yIgFnPc7	některý
druhy	druh	k1gInPc1	druh
astronomických	astronomický	k2eAgInPc2d1	astronomický
objektů	objekt	k1gInPc2	objekt
je	být	k5eAaImIp3nS	být
kvůli	kvůli	k7c3	kvůli
černým	černý	k2eAgFnPc3d1	černá
dírám	díra	k1gFnPc3	díra
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
mikrokvasary	mikrokvasar	k1gInPc1	mikrokvasar
a	a	k8xC	a
aktivní	aktivní	k2eAgNnPc1d1	aktivní
galaktická	galaktický	k2eAgNnPc1d1	Galaktické
jádra	jádro	k1gNnPc1	jádro
vyplývají	vyplývat	k5eAaImIp3nP	vyplývat
z	z	k7c2	z
přítomnosti	přítomnost	k1gFnSc2	přítomnost
hvězdných	hvězdný	k2eAgFnPc2d1	hvězdná
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
a	a	k8xC	a
obřích	obří	k2eAgFnPc2d1	obří
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
<g/>
.	.	kIx.	.
</s>
<s>
Ohýbání	ohýbání	k1gNnSc1	ohýbání
světla	světlo	k1gNnSc2	světlo
gravitací	gravitace	k1gFnPc2	gravitace
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
jev	jev	k1gInSc4	jev
gravitační	gravitační	k2eAgFnSc2d1	gravitační
čočky	čočka	k1gFnSc2	čočka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
nebi	nebe	k1gNnSc6	nebe
viditelné	viditelný	k2eAgFnSc2d1	viditelná
více	hodně	k6eAd2	hodně
obrazů	obraz	k1gInPc2	obraz
stejně	stejně	k6eAd1	stejně
vzdáleného	vzdálený	k2eAgInSc2d1	vzdálený
astronomického	astronomický	k2eAgInSc2d1	astronomický
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
relativita	relativita	k1gFnSc1	relativita
také	také	k9	také
předpovídá	předpovídat	k5eAaImIp3nS	předpovídat
existenci	existence	k1gFnSc4	existence
gravitačních	gravitační	k2eAgFnPc2d1	gravitační
vln	vlna	k1gFnPc2	vlna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byly	být	k5eAaImAgFnP	být
fyziky	fyzika	k1gFnPc1	fyzika
přímo	přímo	k6eAd1	přímo
pozorovány	pozorovat	k5eAaImNgFnP	pozorovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
zařízení	zařízení	k1gNnSc2	zařízení
LIGO	liga	k1gFnSc5	liga
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
obecná	obecná	k1gFnSc1	obecná
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
základem	základ	k1gInSc7	základ
současných	současný	k2eAgInPc2d1	současný
kosmologických	kosmologický	k2eAgInPc2d1	kosmologický
modelů	model	k1gInPc2	model
trvale	trvale	k6eAd1	trvale
se	se	k3xPyFc4	se
rozpínajícího	rozpínající	k2eAgInSc2d1	rozpínající
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
zveřejnění	zveřejnění	k1gNnSc6	zveřejnění
speciální	speciální	k2eAgFnSc2d1	speciální
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
Einstein	Einstein	k1gMnSc1	Einstein
přemýšlet	přemýšlet	k5eAaImF	přemýšlet
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
začlenit	začlenit	k5eAaPmF	začlenit
gravitaci	gravitace	k1gFnSc4	gravitace
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
nového	nový	k2eAgInSc2d1	nový
relativistického	relativistický	k2eAgInSc2d1	relativistický
rámce	rámec	k1gInSc2	rámec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
začal	začít	k5eAaPmAgInS	začít
s	s	k7c7	s
jednoduchým	jednoduchý	k2eAgInSc7d1	jednoduchý
myšlenkovým	myšlenkový	k2eAgInSc7d1	myšlenkový
experimentem	experiment	k1gInSc7	experiment
zahrnující	zahrnující	k2eAgFnSc2d1	zahrnující
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
během	během	k7c2	během
volného	volný	k2eAgInSc2d1	volný
pádu	pád	k1gInSc2	pád
<g/>
,	,	kIx,	,
a	a	k8xC	a
pustil	pustit	k5eAaPmAgMnS	pustit
se	se	k3xPyFc4	se
tak	tak	k9	tak
do	do	k7c2	do
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
bylo	být	k5eAaImAgNnS	být
následným	následný	k2eAgNnSc7d1	následné
osmiletým	osmiletý	k2eAgNnSc7d1	osmileté
hledáním	hledání	k1gNnSc7	hledání
relativistické	relativistický	k2eAgFnSc2d1	relativistická
teorie	teorie	k1gFnSc2	teorie
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
četných	četný	k2eAgFnPc6d1	četná
oklikách	oklika	k1gFnPc6	oklika
a	a	k8xC	a
špatných	špatný	k2eAgInPc6d1	špatný
startech	start	k1gInPc6	start
jeho	jeho	k3xOp3gFnSc2	jeho
práce	práce	k1gFnSc2	práce
kulminovala	kulminovat	k5eAaImAgFnS	kulminovat
v	v	k7c6	v
prezentaci	prezentace	k1gFnSc6	prezentace
pro	pro	k7c4	pro
Pruskou	pruský	k2eAgFnSc4d1	pruská
akademii	akademie	k1gFnSc4	akademie
věd	věda	k1gFnPc2	věda
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1915	[number]	k4	1915
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
známo	znám	k2eAgNnSc1d1	známo
jako	jako	k9	jako
Einsteinovy	Einsteinův	k2eAgFnPc4d1	Einsteinova
rovnice	rovnice	k1gFnPc4	rovnice
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
rovnice	rovnice	k1gFnPc1	rovnice
určují	určovat	k5eAaImIp3nP	určovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
geometrie	geometrie	k1gFnSc1	geometrie
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
času	čas	k1gInSc2	čas
ovlivněna	ovlivněn	k2eAgFnSc1d1	ovlivněna
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
není	být	k5eNaImIp3nS	být
přítomna	přítomen	k2eAgFnSc1d1	přítomna
hmota	hmota	k1gFnSc1	hmota
ani	ani	k8xC	ani
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
jádro	jádro	k1gNnSc1	jádro
Einsteinovy	Einsteinův	k2eAgFnSc2d1	Einsteinova
obecné	obecný	k2eAgFnSc2d1	obecná
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Einsteinovy	Einsteinův	k2eAgFnPc1d1	Einsteinova
rovnice	rovnice	k1gFnPc1	rovnice
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
jsou	být	k5eAaImIp3nP	být
nelineární	lineární	k2eNgFnPc1d1	nelineární
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
obtížně	obtížně	k6eAd1	obtížně
řešitelné	řešitelný	k2eAgNnSc1d1	řešitelné
<g/>
.	.	kIx.	.
</s>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
používal	používat	k5eAaImAgMnS	používat
metody	metoda	k1gFnPc4	metoda
aproximace	aproximace	k1gFnSc2	aproximace
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
na	na	k7c6	na
počátečních	počáteční	k2eAgFnPc6d1	počáteční
předpovědích	předpověď	k1gFnPc6	předpověď
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
již	již	k6eAd1	již
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
astrofyzik	astrofyzik	k1gMnSc1	astrofyzik
Karl	Karl	k1gMnSc1	Karl
Schwarzschild	Schwarzschild	k1gMnSc1	Schwarzschild
našel	najít	k5eAaPmAgMnS	najít
první	první	k4xOgNnSc4	první
netriviální	triviální	k2eNgNnSc4d1	netriviální
přesné	přesný	k2eAgNnSc4d1	přesné
řešení	řešení	k1gNnSc4	řešení
Einsteinových	Einsteinových	k2eAgFnPc2d1	Einsteinových
rovnic	rovnice	k1gFnPc2	rovnice
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Schwarzschildovu	Schwarzschildův	k2eAgFnSc4d1	Schwarzschildova
metriku	metrika	k1gFnSc4	metrika
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
řešení	řešení	k1gNnSc1	řešení
položilo	položit	k5eAaPmAgNnS	položit
základy	základ	k1gInPc4	základ
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
konečné	konečný	k2eAgFnSc2d1	konečná
fáze	fáze	k1gFnSc2	fáze
gravitačního	gravitační	k2eAgInSc2d1	gravitační
kolapsu	kolaps	k1gInSc2	kolaps
<g/>
,	,	kIx,	,
objektů	objekt	k1gInPc2	objekt
známých	známý	k2eAgInPc2d1	známý
dnes	dnes	k6eAd1	dnes
jako	jako	k8xC	jako
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
byly	být	k5eAaImAgInP	být
podniknuty	podniknut	k2eAgInPc1d1	podniknut
první	první	k4xOgInPc1	první
kroky	krok	k1gInPc1	krok
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
zobecnění	zobecnění	k1gNnSc3	zobecnění
Schwarzschildova	Schwarzschildův	k2eAgNnSc2d1	Schwarzschildovo
řešení	řešení	k1gNnSc2	řešení
na	na	k7c4	na
elektricky	elektricky	k6eAd1	elektricky
nabité	nabitý	k2eAgInPc4d1	nabitý
objekty	objekt	k1gInPc4	objekt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nakonec	nakonec	k6eAd1	nakonec
vyústily	vyústit	k5eAaPmAgFnP	vyústit
v	v	k7c4	v
Reissner-Nordströmovo	Reissner-Nordströmův	k2eAgNnSc4d1	Reissner-Nordströmův
řešení	řešení	k1gNnSc4	řešení
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
elektricky	elektricky	k6eAd1	elektricky
nabitými	nabitý	k2eAgFnPc7d1	nabitá
černými	černý	k2eAgFnPc7d1	černá
děrami	děra	k1gFnPc7	děra
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
Einstein	Einstein	k1gMnSc1	Einstein
aplikoval	aplikovat	k5eAaBmAgMnS	aplikovat
svoji	svůj	k3xOyFgFnSc4	svůj
teorii	teorie	k1gFnSc4	teorie
na	na	k7c4	na
vesmír	vesmír	k1gInSc4	vesmír
jako	jako	k8xS	jako
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
tak	tak	k9	tak
oblast	oblast	k1gFnSc4	oblast
relativistické	relativistický	k2eAgFnSc2d1	relativistická
kosmologie	kosmologie	k1gFnSc2	kosmologie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
tehdejším	tehdejší	k2eAgNnSc7d1	tehdejší
moderním	moderní	k2eAgNnSc7d1	moderní
myšlením	myšlení	k1gNnSc7	myšlení
předpokládal	předpokládat	k5eAaImAgInS	předpokládat
existenci	existence	k1gFnSc4	existence
statického	statický	k2eAgInSc2d1	statický
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
přidal	přidat	k5eAaPmAgMnS	přidat
nový	nový	k2eAgInSc4d1	nový
parametr	parametr	k1gInSc4	parametr
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
původních	původní	k2eAgFnPc2d1	původní
rovnic	rovnice	k1gFnPc2	rovnice
<g/>
,	,	kIx,	,
<g/>
-	-	kIx~	-
kosmologickou	kosmologický	k2eAgFnSc4d1	kosmologická
konstantu	konstanta	k1gFnSc4	konstanta
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
pozorovací	pozorovací	k2eAgFnSc1d1	pozorovací
domněnce	domněnka	k1gFnSc3	domněnka
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
práce	práce	k1gFnSc2	práce
Edwina	Edwino	k1gNnSc2	Edwino
Hubbla	Hubbla	k1gFnSc2	Hubbla
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
náš	náš	k3xOp1gInSc1	náš
vesmír	vesmír	k1gInSc1	vesmír
rozpíná	rozpínat	k5eAaImIp3nS	rozpínat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
snadno	snadno	k6eAd1	snadno
popsatelné	popsatelný	k2eAgNnSc1d1	popsatelné
pomocí	pomocí	k7c2	pomocí
expandujícího	expandující	k2eAgNnSc2d1	expandující
kosmologického	kosmologický	k2eAgNnSc2d1	kosmologické
řešení	řešení	k1gNnSc2	řešení
nalezeného	nalezený	k2eAgNnSc2d1	nalezené
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Fridmanem	Fridman	k1gMnSc7	Fridman
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nevyžaduje	vyžadovat	k5eNaImIp3nS	vyžadovat
kosmologickou	kosmologický	k2eAgFnSc4d1	kosmologická
konstantu	konstanta	k1gFnSc4	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Georges	Georges	k1gMnSc1	Georges
Lemaître	Lemaîtr	k1gInSc5	Lemaîtr
použil	použít	k5eAaPmAgMnS	použít
tato	tento	k3xDgNnPc4	tento
řešení	řešení	k1gNnPc4	řešení
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
formuloval	formulovat	k5eAaImAgMnS	formulovat
nejstarší	starý	k2eAgFnSc4d3	nejstarší
verzi	verze	k1gFnSc4	verze
modelů	model	k1gInPc2	model
Velkého	velký	k2eAgInSc2d1	velký
třesku	třesk	k1gInSc2	třesk
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
náš	náš	k3xOp1gInSc1	náš
vesmír	vesmír	k1gInSc1	vesmír
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
z	z	k7c2	z
extrémně	extrémně	k6eAd1	extrémně
horkého	horký	k2eAgInSc2d1	horký
a	a	k8xC	a
hustého	hustý	k2eAgInSc2d1	hustý
dřívějšího	dřívější	k2eAgInSc2d1	dřívější
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
později	pozdě	k6eAd2	pozdě
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kosmologická	kosmologický	k2eAgFnSc1d1	kosmologická
konstanta	konstanta	k1gFnSc1	konstanta
byla	být	k5eAaImAgFnS	být
největší	veliký	k2eAgFnSc1d3	veliký
hrubá	hrubý	k2eAgFnSc1d1	hrubá
chyba	chyba	k1gFnSc1	chyba
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
obecná	obecná	k1gFnSc1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
zůstala	zůstat	k5eAaPmAgFnS	zůstat
něco	něco	k3yInSc1	něco
jako	jako	k8xC	jako
kuriozita	kuriozita	k1gFnSc1	kuriozita
mezi	mezi	k7c7	mezi
fyzikálními	fyzikální	k2eAgFnPc7d1	fyzikální
teoriemi	teorie	k1gFnPc7	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
jasně	jasně	k6eAd1	jasně
lepší	dobrý	k2eAgInSc4d2	lepší
než	než	k8xS	než
Newtonův	Newtonův	k2eAgInSc4d1	Newtonův
gravitační	gravitační	k2eAgInSc4d1	gravitační
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
speciální	speciální	k2eAgFnSc7d1	speciální
teorií	teorie	k1gFnSc7	teorie
relativity	relativita	k1gFnSc2	relativita
a	a	k8xC	a
vyřešila	vyřešit	k5eAaPmAgFnS	vyřešit
několik	několik	k4yIc4	několik
efektů	efekt	k1gInPc2	efekt
nevysvětlitelných	vysvětlitelný	k2eNgInPc2d1	nevysvětlitelný
podle	podle	k7c2	podle
Newtonovy	Newtonův	k2eAgFnSc2d1	Newtonova
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
sám	sám	k3xTgMnSc1	sám
ukázal	ukázat	k5eAaPmAgMnS	ukázat
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jeho	jeho	k3xOp3gFnSc1	jeho
teorie	teorie	k1gFnSc1	teorie
vysvětli	vysvětlit	k5eAaPmRp2nS	vysvětlit
anomálii	anomálie	k1gFnSc4	anomálie
ve	v	k7c6	v
stáčení	stáčení	k1gNnSc6	stáčení
perihelia	perihelium	k1gNnSc2	perihelium
planety	planeta	k1gFnSc2	planeta
Merkur	Merkur	k1gInSc1	Merkur
bez	bez	k7c2	bez
jakýchkoliv	jakýkoliv	k3yIgInPc2	jakýkoliv
svévolných	svévolný	k2eAgInPc2d1	svévolný
parametrů	parametr	k1gInPc2	parametr
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
zfalšované	zfalšovaný	k2eAgNnSc1d1	zfalšované
<g/>
/	/	kIx~	/
zfušované	zfušovaný	k2eAgInPc1d1	zfušovaný
faktory	faktor	k1gInPc1	faktor
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Expedice	expedice	k1gFnSc1	expedice
vedená	vedený	k2eAgFnSc1d1	vedená
Arthurem	Arthur	k1gInSc7	Arthur
Eddingtonem	Eddington	k1gInSc7	Eddington
podobně	podobně	k6eAd1	podobně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
předpověď	předpověď	k1gFnSc1	předpověď
obecné	obecný	k2eAgFnSc2d1	obecná
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
pro	pro	k7c4	pro
stáčení	stáčení	k1gNnSc4	stáčení
paprsků	paprsek	k1gInPc2	paprsek
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
během	během	k7c2	během
úplného	úplný	k2eAgNnSc2d1	úplné
zatmění	zatmění	k1gNnSc2	zatmění
Slunce	slunce	k1gNnSc2	slunce
dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
udělalo	udělat	k5eAaPmAgNnS	udělat
Einsteina	Einstein	k1gMnSc4	Einstein
okamžitě	okamžitě	k6eAd1	okamžitě
slavným	slavný	k2eAgInPc3d1	slavný
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
hlavního	hlavní	k2eAgInSc2d1	hlavní
proudu	proud	k1gInSc2	proud
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
astrofyziky	astrofyzika	k1gFnSc2	astrofyzika
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
rozvojem	rozvoj	k1gInSc7	rozvoj
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
přibližně	přibližně	k6eAd1	přibližně
1960	[number]	k4	1960
a	a	k8xC	a
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
známé	známý	k2eAgFnPc1d1	známá
jako	jako	k8xC	jako
zlatý	zlatý	k2eAgInSc1d1	zlatý
věk	věk	k1gInSc1	věk
obecné	obecný	k2eAgFnSc2d1	obecná
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikové	fyzik	k1gMnPc1	fyzik
začali	začít	k5eAaPmAgMnP	začít
chápat	chápat	k5eAaImF	chápat
pojem	pojem	k1gInSc4	pojem
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
a	a	k8xC	a
identifikovat	identifikovat	k5eAaBmF	identifikovat
kvasary	kvasar	k1gInPc4	kvasar
jako	jako	k8xS	jako
jedny	jeden	k4xCgInPc1	jeden
z	z	k7c2	z
astrofyzikálních	astrofyzikální	k2eAgInPc2d1	astrofyzikální
projevů	projev	k1gInPc2	projev
těchto	tento	k3xDgInPc2	tento
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
přesnější	přesný	k2eAgInPc1d2	přesnější
testy	test	k1gInPc1	test
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
potvrdily	potvrdit	k5eAaPmAgInP	potvrdit
predikční	predikční	k2eAgFnSc4d1	predikční
sílu	síla	k1gFnSc4	síla
teorie	teorie	k1gFnSc2	teorie
a	a	k8xC	a
relativistická	relativistický	k2eAgFnSc1d1	relativistická
kosmologie	kosmologie	k1gFnSc1	kosmologie
se	se	k3xPyFc4	se
také	také	k9	také
stala	stát	k5eAaPmAgFnS	stát
přístupnou	přístupný	k2eAgFnSc4d1	přístupná
pro	pro	k7c4	pro
přímé	přímý	k2eAgFnPc4d1	přímá
pozorovací	pozorovací	k2eAgFnPc4d1	pozorovací
testy	testa	k1gFnPc4	testa
<g/>
.	.	kIx.	.
</s>
<s>
Obecnou	obecný	k2eAgFnSc4d1	obecná
relativitu	relativita	k1gFnSc4	relativita
lze	lze	k6eAd1	lze
chápat	chápat	k5eAaImF	chápat
také	také	k9	také
jako	jako	k9	jako
rozšíření	rozšíření	k1gNnSc4	rozšíření
speciální	speciální	k2eAgFnSc2d1	speciální
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnSc1d2	starší
teorie	teorie	k1gFnSc1	teorie
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
správný	správný	k2eAgInSc4d1	správný
popis	popis	k1gInSc4	popis
elektrodynamiky	elektrodynamika	k1gFnSc2	elektrodynamika
a	a	k8xC	a
šíření	šíření	k1gNnSc2	šíření
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
inerciálních	inerciální	k2eAgFnPc6d1	inerciální
vztažných	vztažný	k2eAgFnPc6d1	vztažná
soustavách	soustava	k1gFnPc6	soustava
a	a	k8xC	a
opravuje	opravovat	k5eAaImIp3nS	opravovat
nepřesnosti	nepřesnost	k1gFnPc4	nepřesnost
Newtonovy	Newtonův	k2eAgFnSc2d1	Newtonova
mechaniky	mechanika	k1gFnSc2	mechanika
při	při	k7c6	při
vysokých	vysoký	k2eAgFnPc6d1	vysoká
rychlostech	rychlost	k1gFnPc6	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
relativita	relativita	k1gFnSc1	relativita
navíc	navíc	k6eAd1	navíc
hraje	hrát	k5eAaImIp3nS	hrát
mezi	mezi	k7c7	mezi
fyzikálními	fyzikální	k2eAgFnPc7d1	fyzikální
teoriemi	teorie	k1gFnPc7	teorie
jedinečnou	jedinečný	k2eAgFnSc4d1	jedinečná
roli	role	k1gFnSc4	role
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
vykládá	vykládat	k5eAaImIp3nS	vykládat
gravitační	gravitační	k2eAgNnSc1d1	gravitační
pole	pole	k1gNnSc1	pole
jako	jako	k8xC	jako
geometrický	geometrický	k2eAgInSc1d1	geometrický
fenomén	fenomén	k1gInSc1	fenomén
<g/>
.	.	kIx.	.
</s>
<s>
Přesněji	přesně	k6eAd2	přesně
řečeno	řečen	k2eAgNnSc1d1	řečeno
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
libovolný	libovolný	k2eAgInSc4d1	libovolný
objekt	objekt	k1gInSc4	objekt
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
hmotností	hmotnost	k1gFnSc7	hmotnost
zakřivuje	zakřivovat	k5eAaImIp3nS	zakřivovat
časoprostor	časoprostor	k1gInSc1	časoprostor
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
,	,	kIx,	,
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
zakřivení	zakřivení	k1gNnSc1	zakřivení
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
jako	jako	k9	jako
gravitace	gravitace	k1gFnSc1	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
Abychom	aby	kYmCp1nP	aby
pochopili	pochopit	k5eAaPmAgMnP	pochopit
tuto	tento	k3xDgFnSc4	tento
rovnost	rovnost	k1gFnSc4	rovnost
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
uvažovat	uvažovat	k5eAaImF	uvažovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
gravitace	gravitace	k1gFnSc1	gravitace
způsobovala	způsobovat	k5eAaImAgFnS	způsobovat
nebo	nebo	k8xC	nebo
byla	být	k5eAaImAgFnS	být
způsobována	způsobovat	k5eAaImNgFnS	způsobovat
zakřivením	zakřivení	k1gNnSc7	zakřivení
časoprostoru	časoprostor	k1gInSc2	časoprostor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
<g/>
,	,	kIx,	,
že	že	k8xS	že
gravitace	gravitace	k1gFnSc1	gravitace
je	být	k5eAaImIp3nS	být
zakřivení	zakřivení	k1gNnSc4	zakřivení
časoprostoru	časoprostor	k1gInSc2	časoprostor
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
formulování	formulování	k1gNnSc2	formulování
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
dodnes	dodnes	k6eAd1	dodnes
přežila	přežít	k5eAaPmAgFnS	přežít
všechny	všechen	k3xTgInPc4	všechen
experimenty	experiment	k1gInPc4	experiment
pokoušející	pokoušející	k2eAgInPc4d1	pokoušející
se	se	k3xPyFc4	se
o	o	k7c4	o
její	její	k3xOp3gNnSc4	její
vyvrácení	vyvrácení	k1gNnSc4	vyvrácení
<g/>
.	.	kIx.	.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
bývá	bývat	k5eAaImIp3nS	bývat
také	také	k9	také
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
Einsteinova	Einsteinův	k2eAgFnSc1d1	Einsteinova
gravitační	gravitační	k2eAgFnSc1d1	gravitační
teorie	teorie	k1gFnSc1	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Obecnou	obecný	k2eAgFnSc4d1	obecná
teorii	teorie	k1gFnSc4	teorie
relativity	relativita	k1gFnSc2	relativita
lze	lze	k6eAd1	lze
postavit	postavit	k5eAaPmF	postavit
na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
postulátech	postulát	k1gInPc6	postulát
<g/>
:	:	kIx,	:
Všechny	všechen	k3xTgInPc1	všechen
fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
zákony	zákon	k1gInPc1	zákon
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
vztažných	vztažný	k2eAgFnPc6d1	vztažná
soustavách	soustava	k1gFnPc6	soustava
mají	mít	k5eAaImIp3nP	mít
stejný	stejný	k2eAgInSc4d1	stejný
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
stejnými	stejný	k2eAgFnPc7d1	stejná
rovnicemi	rovnice	k1gFnPc7	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Gravitační	gravitační	k2eAgFnPc1d1	gravitační
a	a	k8xC	a
setrvačné	setrvačný	k2eAgFnPc1d1	setrvačná
síly	síla	k1gFnPc1	síla
mají	mít	k5eAaImIp3nP	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
fyzikální	fyzikální	k2eAgFnSc4d1	fyzikální
podstatu	podstata	k1gFnSc4	podstata
a	a	k8xC	a
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
ně	on	k3xPp3gInPc4	on
stejné	stejný	k2eAgInPc4d1	stejný
fyzikální	fyzikální	k2eAgInPc4d1	fyzikální
zákony	zákon	k1gInPc4	zákon
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
princip	princip	k1gInSc1	princip
ekvivalence	ekvivalence	k1gFnSc1	ekvivalence
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
postuláty	postulát	k1gInPc1	postulát
bývají	bývat	k5eAaImIp3nP	bývat
také	také	k9	také
formulovány	formulovat	k5eAaImNgInP	formulovat
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
podobě	podoba	k1gFnSc6	podoba
<g/>
:	:	kIx,	:
Obecný	obecný	k2eAgInSc1d1	obecný
princip	princip	k1gInSc1	princip
relativity	relativita	k1gFnSc2	relativita
<g/>
:	:	kIx,	:
Fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
zákony	zákon	k1gInPc1	zákon
jsou	být	k5eAaImIp3nP	být
stejné	stejný	k2eAgInPc1d1	stejný
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
obecné	obecný	k2eAgFnSc2d1	obecná
kovariance	kovariance	k1gFnSc2	kovariance
<g/>
:	:	kIx,	:
Fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
zákony	zákon	k1gInPc1	zákon
mají	mít	k5eAaImIp3nP	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
formu	forma	k1gFnSc4	forma
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
vztažné	vztažný	k2eAgFnSc6d1	vztažná
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Volné	volný	k2eAgFnPc1d1	volná
testovací	testovací	k2eAgFnPc1d1	testovací
částice	částice	k1gFnPc1	částice
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
po	po	k7c6	po
geodetikách	geodetikách	k?	geodetikách
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
lokální	lokální	k2eAgFnSc1d1	lokální
lorentzovské	lorentzovské	k2eAgFnSc1d1	lorentzovské
invariance	invariance	k1gFnSc1	invariance
<g/>
:	:	kIx,	:
Pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
volné	volný	k2eAgMnPc4d1	volný
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
platí	platit	k5eAaImIp3nP	platit
lokálně	lokálně	k6eAd1	lokálně
zákony	zákon	k1gInPc4	zákon
speciální	speciální	k2eAgFnSc2d1	speciální
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Časoprostor	časoprostor	k1gInSc1	časoprostor
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
zakřivený	zakřivený	k2eAgInSc1d1	zakřivený
<g/>
.	.	kIx.	.
</s>
<s>
Zakřivení	zakřivení	k1gNnSc1	zakřivení
časoprostoru	časoprostor	k1gInSc2	časoprostor
je	být	k5eAaImIp3nS	být
udáváno	udávat	k5eAaImNgNnS	udávat
rozložením	rozložení	k1gNnSc7	rozložení
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
hybnosti	hybnost	k1gFnSc2	hybnost
v	v	k7c6	v
časoprostoru	časoprostor	k1gInSc6	časoprostor
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
zakřivením	zakřivení	k1gNnSc7	zakřivení
časoprostoru	časoprostor	k1gInSc2	časoprostor
a	a	k8xC	a
rozložením	rozložení	k1gNnSc7	rozložení
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
hybnosti	hybnost	k1gFnSc2	hybnost
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
Einsteinovy	Einsteinův	k2eAgFnPc1d1	Einsteinova
rovnice	rovnice	k1gFnPc1	rovnice
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Rovnice	rovnice	k1gFnSc1	rovnice
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
fyzikálnímu	fyzikální	k2eAgNnSc3d1	fyzikální
poli	pole	k1gNnSc3	pole
lze	lze	k6eAd1	lze
přiřadit	přiřadit	k5eAaPmF	přiřadit
symetrický	symetrický	k2eAgInSc4d1	symetrický
tenzor	tenzor	k1gInSc4	tenzor
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
hybnosti	hybnost	k1gFnSc2	hybnost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T	T	kA	T
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
v	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
gravitační	gravitační	k2eAgNnPc1d1	gravitační
pole	pole	k1gNnPc1	pole
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
bodě	bod	k1gInSc6	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
λ	λ	k?	λ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
popsat	popsat	k5eAaPmF	popsat
deseti	deset	k4xCc7	deset
funkcemi	funkce	k1gFnPc7	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
λ	λ	k?	λ
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ι	ι	k?	ι
,	,	kIx,	,
κ	κ	k?	κ
=	=	kIx~	=
0	[number]	k4	0
,	,	kIx,	,
1	[number]	k4	1
,	,	kIx,	,
2	[number]	k4	2
,	,	kIx,	,
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
iota	iotum	k1gNnPc4	iotum
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnSc2	kappa
=	=	kIx~	=
<g/>
0,1	[number]	k4	0,1
<g/>
,2	,2	k4	,2
<g/>
,3	,3	k4	,3
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
metrický	metrický	k2eAgInSc1d1	metrický
tenzor	tenzor	k1gInSc1	tenzor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Einsteinovy	Einsteinův	k2eAgFnSc2d1	Einsteinova
rovnice	rovnice	k1gFnSc2	rovnice
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zapsat	zapsat	k5eAaPmF	zapsat
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
G	G	kA	G
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
μ	μ	k?	μ
ν	ν	k?	ν
,	,	kIx,	,
π	π	k?	π
ρ	ρ	k?	ρ
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
μ	μ	k?	μ
ν	ν	k?	ν
,	,	kIx,	,
π	π	k?	π
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
μ	μ	k?	μ
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
κ	κ	k?	κ
:	:	kIx,	:
T	T	kA	T
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
μ	μ	k?	μ
ν	ν	k?	ν
,	,	kIx,	,
ρ	ρ	k?	ρ
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
μ	μ	k?	μ
ν	ν	k?	ν
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
φ	φ	k?	φ
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
g_	g_	k?	g_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
\	\	kIx~	\
<g/>
nu	nu	k9	nu
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
<g/>
rho	rho	k?	rho
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
g_	g_	k?	g_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gNnSc3	on
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
g_	g_	k?	g_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
varkappa	varkappa	k1gFnSc1	varkappa
T	T	kA	T
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
g_	g_	k?	g_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
\	\	kIx~	\
<g/>
nu	nu	k9	nu
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
g_	g_	k?	g_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
phi	phi	k?	phi
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T	T	kA	T
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iota	k1gFnSc1	iota
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnSc1	kappa
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
tenzor	tenzor	k1gInSc4	tenzor
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
G	G	kA	G
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iota	k1gFnSc1	iota
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnSc2	kappa
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
Einsteinův	Einsteinův	k2eAgInSc4d1	Einsteinův
tenzor	tenzor	k1gInSc4	tenzor
a	a	k8xC	a
symbol	symbol	k1gInSc4	symbol
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
phi	phi	k?	phi
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
označením	označení	k1gNnSc7	označení
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc4	všechen
ostatní	ostatní	k2eAgNnPc4d1	ostatní
fyzikální	fyzikální	k2eAgNnPc4d1	fyzikální
pole	pole	k1gNnPc4	pole
čistě	čistě	k6eAd1	čistě
negeometrické	geometrický	k2eNgFnSc2d1	geometrický
povahy	povaha	k1gFnSc2	povaha
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
jejich	jejich	k3xOp3gFnPc2	jejich
derivací	derivace	k1gFnPc2	derivace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
hmotný	hmotný	k2eAgInSc4d1	hmotný
prach	prach	k1gInSc4	prach
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
tekutina	tekutina	k1gFnSc1	tekutina
nebo	nebo	k8xC	nebo
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
pole	pole	k1gNnSc1	pole
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varkappa	varkappa	k1gFnSc1	varkappa
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
Einsteinova	Einsteinův	k2eAgFnSc1d1	Einsteinova
gravitační	gravitační	k2eAgFnSc1d1	gravitační
konstanta	konstanta	k1gFnSc1	konstanta
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
κ	κ	k?	κ
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
8	[number]	k4	8
π	π	k?	π
G	G	kA	G
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varkappa	varkappa	k1gFnSc1	varkappa
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
8	[number]	k4	8
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
G	G	kA	G
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}}}}	}}}}	k?	}}}}
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
vzorci	vzorec	k1gInSc6	vzorec
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
G	G	kA	G
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
}	}	kIx)	}
Newtonova	Newtonův	k2eAgFnSc1d1	Newtonova
gravitační	gravitační	k2eAgFnSc1d1	gravitační
konstanta	konstanta	k1gFnSc1	konstanta
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Einsteinovu	Einsteinův	k2eAgInSc3d1	Einsteinův
tenzoru	tenzor	k1gInSc3	tenzor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
G	G	kA	G
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iota	k1gFnSc1	iota
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnSc1	kappa
}}	}}	k?	}}
lze	lze	k6eAd1	lze
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
závisí	záviset	k5eAaImIp3nS	záviset
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
metrickém	metrický	k2eAgInSc6d1	metrický
tenzoru	tenzor	k1gInSc6	tenzor
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc6	jeho
parciálních	parciální	k2eAgFnPc6d1	parciální
derivacích	derivace	k1gFnPc6	derivace
podle	podle	k7c2	podle
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
λ	λ	k?	λ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}}	}}	k?	}}
nejvýše	vysoce	k6eAd3	vysoce
do	do	k7c2	do
druhého	druhý	k4xOgInSc2	druhý
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
také	také	k9	také
požaduje	požadovat	k5eAaImIp3nS	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
G	G	kA	G
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iota	k1gFnSc1	iota
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnSc1	kappa
}}	}}	k?	}}
záviselo	záviset	k5eAaImAgNnS	záviset
na	na	k7c6	na
druhých	druhý	k4xOgFnPc6	druhý
derivacích	derivace	k1gFnPc6	derivace
metrického	metrický	k2eAgInSc2d1	metrický
tenzoru	tenzor	k1gInSc2	tenzor
lineárně	lineárně	k6eAd1	lineárně
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
lze	lze	k6eAd1	lze
zapsat	zapsat	k5eAaPmF	zapsat
jako	jako	k8xS	jako
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
G	G	kA	G
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
ρ	ρ	k?	ρ
σ	σ	k?	σ
,	,	kIx,	,
τ	τ	k?	τ
μ	μ	k?	μ
:	:	kIx,	:
:	:	kIx,	:
∂	∂	k?	∂
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
α	α	k?	α
β	β	k?	β
,	,	kIx,	,
γ	γ	k?	γ
δ	δ	k?	δ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
G	G	kA	G
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
g_	g_	k?	g_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rho	rho	k?	rho
\	\	kIx~	\
<g/>
sigma	sigma	k1gNnSc1	sigma
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
tau	tau	k1gNnSc2	tau
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
g_	g_	k?	g_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
zachování	zachování	k1gNnSc2	zachování
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
hybnosti	hybnost	k1gFnSc2	hybnost
omezuje	omezovat	k5eAaImIp3nS	omezovat
pravou	pravý	k2eAgFnSc4d1	pravá
stranu	strana	k1gFnSc4	strana
Einsteinových	Einsteinových	k2eAgFnPc2d1	Einsteinových
rovnic	rovnice	k1gFnPc2	rovnice
podmínkou	podmínka	k1gFnSc7	podmínka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
;	;	kIx,	;
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T_	T_	k1gMnPc6	T_
<g/>
{	{	kIx(	{
<g/>
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnSc1	kappa
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Divergence	divergence	k1gFnSc1	divergence
levé	levý	k2eAgFnSc2d1	levá
strany	strana	k1gFnSc2	strana
Einsteinových	Einsteinových	k2eAgFnPc2d1	Einsteinových
rovnic	rovnice	k1gFnPc2	rovnice
tedy	tedy	k9	tedy
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
identicky	identicky	k6eAd1	identicky
nulová	nulový	k2eAgFnSc1d1	nulová
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
G	G	kA	G
:	:	kIx,	:
;	;	kIx,	;
ι	ι	k?	ι
:	:	kIx,	:
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G_	G_	k1gMnPc6	G_
<g/>
{	{	kIx(	{
<g/>
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
iota	iota	k6eAd1	iota
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
G	G	kA	G
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}}	}}	k?	}}
záviset	záviset	k5eAaImF	záviset
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
metrickém	metrický	k2eAgInSc6d1	metrický
tenzoru	tenzor	k1gInSc6	tenzor
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc6	jeho
derivacích	derivace	k1gFnPc6	derivace
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
tvar	tvar	k1gInSc1	tvar
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
G	G	kA	G
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}}	}}	k?	}}
určen	určit	k5eAaPmNgInS	určit
až	až	k9	až
na	na	k7c4	na
konstanty	konstanta	k1gFnPc4	konstanta
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
jako	jako	k8xS	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
G	G	kA	G
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
G	G	kA	G
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
R	R	kA	R
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
Rg	Rg	k1gFnSc1	Rg
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
g	g	kA	g
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}}	}}	k?	}}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R	R	kA	R
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iota	k1gFnSc1	iota
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnSc1	kappa
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
Ricciho	Ricci	k1gMnSc2	Ricci
tenzor	tenzor	k1gInSc1	tenzor
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R	R	kA	R
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
skalární	skalární	k2eAgFnSc1d1	skalární
křivost	křivost	k1gFnSc1	křivost
<g/>
.	.	kIx.	.
</s>
<s>
Srovnáním	srovnání	k1gNnSc7	srovnání
tohoto	tento	k3xDgInSc2	tento
vztahu	vztah	k1gInSc2	vztah
se	s	k7c7	s
zúženými	zúžený	k2eAgFnPc7d1	zúžená
formami	forma	k1gFnPc7	forma
Riemannova	Riemannův	k2eAgInSc2d1	Riemannův
tenzoru	tenzor	k1gInSc2	tenzor
lze	lze	k6eAd1	lze
dojit	dojit	k5eAaImF	dojit
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
můžeme	moct	k5eAaImIp1nP	moct
položit	položit	k5eAaPmF	položit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
-	-	kIx~	-
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Konstanta	konstanta	k1gFnSc1	konstanta
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
neurčena	určen	k2eNgFnSc1d1	neurčena
<g/>
.	.	kIx.	.
</s>
<s>
Zavedeme	zavést	k5eAaPmIp1nP	zavést
<g/>
-li	i	k?	-li
novou	nový	k2eAgFnSc4d1	nová
konstantu	konstanta	k1gFnSc4	konstanta
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Λ	Λ	k?	Λ
=	=	kIx~	=
-	-	kIx~	-
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Lambda	lambda	k1gNnSc7	lambda
=	=	kIx~	=
<g/>
-a_	_	k?	-a_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
rovnici	rovnice	k1gFnSc4	rovnice
popisující	popisující	k2eAgInSc4d1	popisující
gravitační	gravitační	k2eAgInSc4d1	gravitační
zákon	zákon	k1gInSc4	zákon
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k8xS	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
ι	ι	k?	ι
<g />
.	.	kIx.	.
</s>
<s hack="1">
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
Λ	Λ	k?	Λ
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
κ	κ	k?	κ
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R	R	kA	R
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
iota	iota	k1gFnSc1	iota
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnSc1	kappa
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
Rg	Rg	k1gFnSc1	Rg
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
Lambda	lambda	k1gNnSc1	lambda
g	g	kA	g
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
varkappa	varkappa	k1gFnSc1	varkappa
T	T	kA	T
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}}	}}	k?	}}
:	:	kIx,	:
Konstanta	konstanta	k1gFnSc1	konstanta
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Λ	Λ	k?	Λ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Lambda	lambda	k1gNnPc6	lambda
}	}	kIx)	}
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
kosmologická	kosmologický	k2eAgFnSc1d1	kosmologická
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Konstanta	konstanta	k1gFnSc1	konstanta
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Λ	Λ	k?	Λ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Lambda	lambda	k1gNnPc6	lambda
}	}	kIx)	}
hraje	hrát	k5eAaImIp3nS	hrát
úlohu	úloha	k1gFnSc4	úloha
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
kosmologických	kosmologický	k2eAgNnPc6d1	kosmologické
měřítkách	měřítko	k1gNnPc6	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
řešíme	řešit	k5eAaImIp1nP	řešit
problémy	problém	k1gInPc1	problém
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nejsou	být	k5eNaImIp3nP	být
kosmologického	kosmologický	k2eAgInSc2d1	kosmologický
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
klademe	klást	k5eAaImIp1nP	klást
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Λ	Λ	k?	Λ
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Lambda	lambda	k1gNnPc6	lambda
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
κ	κ	k?	κ
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R	R	kA	R
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
Rg	Rg	k1gFnSc6	Rg
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
varkappa	varkapp	k1gMnSc2	varkapp
T	T	kA	T
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}}	}}	k?	}}
:	:	kIx,	:
Zúžením	zúžení	k1gNnSc7	zúžení
této	tento	k3xDgFnSc3	tento
dostaneme	dostat	k5eAaPmIp1nP	dostat
skalární	skalární	k2eAgFnSc4d1	skalární
rovnici	rovnice	k1gFnSc4	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
<g />
.	.	kIx.	.
</s>
<s hack="1">
=	=	kIx~	=
κ	κ	k?	κ
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R	R	kA	R
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
varkappa	varkappa	k1gFnSc1	varkappa
T	T	kA	T
<g/>
}	}	kIx)	}
:	:	kIx,	:
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
této	tento	k3xDgFnSc2	tento
rovnice	rovnice	k1gFnSc2	rovnice
lze	lze	k6eAd1	lze
předchozí	předchozí	k2eAgFnSc4d1	předchozí
rovnici	rovnice	k1gFnSc4	rovnice
upravit	upravit	k5eAaPmF	upravit
na	na	k7c4	na
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
κ	κ	k?	κ
(	(	kIx(	(
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R	R	kA	R
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
varkappa	varkappa	k1gFnSc1	varkappa
(	(	kIx(	(
<g/>
T	T	kA	T
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
Tg	tg	kA	tg
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
V	v	k7c6	v
prázdném	prázdný	k2eAgInSc6d1	prázdný
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
tedy	tedy	k8xC	tedy
v	v	k7c6	v
dokonalém	dokonalý	k2eAgNnSc6d1	dokonalé
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T	T	kA	T
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
:	:	kIx,	:
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
platí	platit	k5eAaImIp3nS	platit
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R	R	kA	R
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
Odtud	odtud	k6eAd1	odtud
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
prázdném	prázdný	k2eAgInSc6d1	prázdný
prostoru	prostor	k1gInSc6	prostor
se	se	k3xPyFc4	se
rovnice	rovnice	k1gFnSc1	rovnice
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
redukují	redukovat	k5eAaBmIp3nP	redukovat
na	na	k7c4	na
tvar	tvar	k1gInSc4	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
ι	ι	k?	ι
κ	κ	k?	κ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R	R	kA	R
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
iota	iot	k2eAgNnPc4d1	iot
\	\	kIx~	\
<g/>
kappa	kappa	k1gNnPc4	kappa
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
:	:	kIx,	:
Einsteinovy	Einsteinův	k2eAgFnPc1d1	Einsteinova
rovnice	rovnice	k1gFnPc1	rovnice
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
představují	představovat	k5eAaImIp3nP	představovat
systém	systém	k1gInSc4	systém
deseti	deset	k4xCc2	deset
nelineárních	lineární	k2eNgFnPc2d1	nelineární
parciálních	parciální	k2eAgFnPc2d1	parciální
diferenciálních	diferenciální	k2eAgFnPc2d1	diferenciální
rovnic	rovnice	k1gFnPc2	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
rovnice	rovnice	k1gFnPc1	rovnice
tvoří	tvořit	k5eAaImIp3nP	tvořit
základ	základ	k1gInSc4	základ
obecné	obecný	k2eAgFnSc2d1	obecná
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
rovnice	rovnice	k1gFnPc1	rovnice
jsou	být	k5eAaImIp3nP	být
nelineární	lineární	k2eNgFnPc1d1	nelineární
<g/>
,	,	kIx,	,
neplatí	platit	k5eNaImIp3nP	platit
v	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
princip	princip	k1gInSc4	princip
superpozice	superpozice	k1gFnSc2	superpozice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
relativitě	relativita	k1gFnSc6	relativita
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
širší	široký	k2eAgNnSc4d2	širší
zavedení	zavedení	k1gNnSc4	zavedení
časoprostoru	časoprostor	k1gInSc2	časoprostor
<g/>
,	,	kIx,	,
než	než	k8xS	než
ve	v	k7c6	v
speciální	speciální	k2eAgFnSc6d1	speciální
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
je	být	k5eAaImIp3nS	být
časoprostor	časoprostor	k1gInSc1	časoprostor
<g/>
:	:	kIx,	:
zakřivený	zakřivený	k2eAgInSc1d1	zakřivený
<g/>
:	:	kIx,	:
má	mít	k5eAaImIp3nS	mít
neeuklidovskou	euklidovský	k2eNgFnSc4d1	neeuklidovská
geometrii	geometrie	k1gFnSc4	geometrie
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
ve	v	k7c6	v
speciální	speciální	k2eAgFnSc6d1	speciální
relativitě	relativita	k1gFnSc6	relativita
je	být	k5eAaImIp3nS	být
časoprostor	časoprostor	k1gInSc4	časoprostor
plochý	plochý	k2eAgInSc4d1	plochý
<g/>
.	.	kIx.	.
lorentzovský	lorentzovský	k2eAgInSc4d1	lorentzovský
<g/>
:	:	kIx,	:
Metrika	metrika	k1gFnSc1	metrika
časoprostoru	časoprostor	k1gInSc2	časoprostor
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
smíšenou	smíšený	k2eAgFnSc4d1	smíšená
signaturu	signatura	k1gFnSc4	signatura
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
stejné	stejný	k2eAgNnSc1d1	stejné
jako	jako	k9	jako
ve	v	k7c6	v
speciální	speciální	k2eAgFnSc6d1	speciální
relativitě	relativita	k1gFnSc6	relativita
<g/>
.	.	kIx.	.
čtyřrozměrný	čtyřrozměrný	k2eAgMnSc1d1	čtyřrozměrný
<g/>
:	:	kIx,	:
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgInP	být
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
jak	jak	k8xS	jak
tři	tři	k4xCgInPc4	tři
prostorové	prostorový	k2eAgInPc4d1	prostorový
rozměry	rozměr	k1gInPc4	rozměr
tak	tak	k8xS	tak
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
převzaté	převzatý	k2eAgNnSc4d1	převzaté
ze	z	k7c2	z
speciální	speciální	k2eAgFnSc2d1	speciální
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Zakřivení	zakřivení	k1gNnSc1	zakřivení
časoprostoru	časoprostor	k1gInSc2	časoprostor
(	(	kIx(	(
<g/>
způsobené	způsobený	k2eAgFnPc1d1	způsobená
přítomností	přítomnost	k1gFnSc7	přítomnost
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
názorně	názorně	k6eAd1	názorně
představit	představit	k5eAaPmF	představit
např.	např.	kA	např.
následujícím	následující	k2eAgInSc7d1	následující
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Umístíme	umístit	k5eAaPmIp1nP	umístit
<g/>
-li	i	k?	-li
těžký	těžký	k2eAgInSc1d1	těžký
předmět	předmět	k1gInSc1	předmět
(	(	kIx(	(
<g/>
např.	např.	kA	např.
bowlingovou	bowlingový	k2eAgFnSc4d1	bowlingová
kouli	koule	k1gFnSc4	koule
<g/>
)	)	kIx)	)
na	na	k7c4	na
trampolínu	trampolína	k1gFnSc4	trampolína
<g/>
,	,	kIx,	,
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
prohlubeň	prohlubeň	k1gFnSc1	prohlubeň
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
povrch	povrch	k7c2wR	povrch
trampolíny	trampolína	k1gFnSc2	trampolína
zakřivuje	zakřivovat	k5eAaImIp3nS	zakřivovat
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
přítomnost	přítomnost	k1gFnSc1	přítomnost
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
hmoty	hmota	k1gFnSc2	hmota
zakřivuje	zakřivovat	k5eAaImIp3nS	zakřivovat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
blízkém	blízký	k2eAgNnSc6d1	blízké
okolí	okolí	k1gNnSc6	okolí
časoprostor	časoprostor	k1gInSc4	časoprostor
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ilustruje	ilustrovat	k5eAaBmIp3nS	ilustrovat
obrázek	obrázek	k1gInSc4	obrázek
výše	výše	k1gFnSc2	výše
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
přitom	přitom	k6eAd1	přitom
těleso	těleso	k1gNnSc1	těleso
hmotnější	hmotný	k2eAgNnSc1d2	hmotnější
<g/>
,	,	kIx,	,
zakřivuje	zakřivovat	k5eAaImIp3nS	zakřivovat
časoprostor	časoprostor	k1gInSc1	časoprostor
ve	v	k7c6	v
větším	veliký	k2eAgInSc6d2	veliký
rozsahu	rozsah	k1gInSc6	rozsah
a	a	k8xC	a
více	hodně	k6eAd2	hodně
(	(	kIx(	(
<g/>
srovnejme	srovnat	k5eAaPmRp1nP	srovnat
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
analogii	analogie	k1gFnSc6	analogie
s	s	k7c7	s
trampolínou	trampolína	k1gFnSc7	trampolína
např.	např.	kA	např.
zakřivení	zakřivení	k1gNnSc1	zakřivení
způsobené	způsobený	k2eAgNnSc1d1	způsobené
bowlingovou	bowlingový	k2eAgFnSc7d1	bowlingová
koulí	koule	k1gFnSc7	koule
a	a	k8xC	a
tenisovým	tenisový	k2eAgInSc7d1	tenisový
míčkem	míček	k1gInSc7	míček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
zakřivení	zakřivení	k1gNnSc1	zakřivení
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
hustotě	hustota	k1gFnSc6	hustota
(	(	kIx(	(
<g/>
kulička	kulička	k1gFnSc1	kulička
ze	z	k7c2	z
železa	železo	k1gNnSc2	železo
zakřiví	zakřivit	k5eAaPmIp3nS	zakřivit
trampolínu	trampolína	k1gFnSc4	trampolína
více	hodně	k6eAd2	hodně
než	než	k8xS	než
stejně	stejně	k6eAd1	stejně
velká	velký	k2eAgFnSc1d1	velká
kulička	kulička	k1gFnSc1	kulička
z	z	k7c2	z
plastu	plast	k1gInSc2	plast
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Pokud	pokud	k8xS	pokud
cvrnkneme	cvrnknout	k5eAaPmIp1nP	cvrnknout
do	do	k7c2	do
takto	takto	k6eAd1	takto
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
důlku	důlek	k1gInSc2	důlek
malou	malý	k2eAgFnSc4d1	malá
kuličku	kulička	k1gFnSc4	kulička
správnou	správný	k2eAgFnSc7d1	správná
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
"	"	kIx"	"
<g/>
obíhat	obíhat	k5eAaImF	obíhat
<g/>
"	"	kIx"	"
kolem	kolem	k7c2	kolem
bowlingové	bowlingový	k2eAgFnSc2d1	bowlingová
koule	koule	k1gFnSc2	koule
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
analogické	analogický	k2eAgNnSc1d1	analogické
s	s	k7c7	s
obíháním	obíhání	k1gNnSc7	obíhání
planet	planeta	k1gFnPc2	planeta
v	v	k7c6	v
gravitačním	gravitační	k2eAgNnSc6d1	gravitační
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
rovněž	rovněž	k9	rovněž
patrná	patrný	k2eAgFnSc1d1	patrná
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
obecná	obecný	k2eAgFnSc1d1	obecná
relativita	relativita	k1gFnSc1	relativita
neuvažuje	uvažovat	k5eNaImIp3nS	uvažovat
s	s	k7c7	s
působením	působení	k1gNnSc7	působení
síly	síla	k1gFnSc2	síla
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
u	u	k7c2	u
Newtonovy	Newtonův	k2eAgFnSc2d1	Newtonova
teorie	teorie	k1gFnSc2	teorie
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
testovací	testovací	k2eAgFnSc1d1	testovací
částice	částice	k1gFnSc1	částice
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
zakřivení	zakřivení	k1gNnSc4	zakřivení
časoprostoru	časoprostor	k1gInSc2	časoprostor
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
po	po	k7c6	po
nejpřímější	přímý	k2eAgFnSc6d3	nejpřímější
dráze	dráha	k1gFnSc6	dráha
(	(	kIx(	(
<g/>
speciálně	speciálně	k6eAd1	speciálně
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
dráha	dráha	k1gFnSc1	dráha
nejpřímější	přímý	k2eAgFnSc1d3	nejpřímější
z	z	k7c2	z
analogie	analogie	k1gFnSc2	analogie
vidět	vidět	k5eAaImF	vidět
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
jen	jen	k9	jen
v	v	k7c6	v
časoprostoru	časoprostor	k1gInSc6	časoprostor
se	s	k7c7	s
smíšenou	smíšený	k2eAgFnSc7d1	smíšená
signaturou	signatura	k1gFnSc7	signatura
metriky	metrika	k1gFnSc2	metrika
<g/>
)	)	kIx)	)
a	a	k8xC	a
zakřivení	zakřivení	k1gNnSc1	zakřivení
časoprostoru	časoprostor	k1gInSc2	časoprostor
zpětně	zpětně	k6eAd1	zpětně
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
rozložení	rozložení	k1gNnSc4	rozložení
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Stáčení	stáčení	k1gNnSc1	stáčení
perihelia	perihelium	k1gNnSc2	perihelium
Merkuru	Merkur	k1gInSc2	Merkur
Ohyb	ohyb	k1gInSc1	ohyb
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
v	v	k7c6	v
gravitačním	gravitační	k2eAgNnSc6d1	gravitační
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
Gravitační	gravitační	k2eAgFnSc1d1	gravitační
čočka	čočka	k1gFnSc1	čočka
Gravitační	gravitační	k2eAgFnSc2d1	gravitační
rudý	rudý	k2eAgInSc4d1	rudý
posuv	posuv	k1gInSc4	posuv
Shapirův	Shapirův	k2eAgInSc1d1	Shapirův
test	test	k1gInSc1	test
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
Shapirův	Shapirův	k2eAgInSc1d1	Shapirův
efekt	efekt	k1gInSc1	efekt
</s>
