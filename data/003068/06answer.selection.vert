<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
je	být	k5eAaImIp3nS	být
geometrická	geometrický	k2eAgFnSc1d1	geometrická
teorie	teorie	k1gFnSc1	teorie
gravitace	gravitace	k1gFnSc1	gravitace
<g/>
,	,	kIx,	,
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
Albertem	Albert	k1gMnSc7	Albert
Einsteinem	Einstein	k1gMnSc7	Einstein
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
aktuální	aktuální	k2eAgInSc4d1	aktuální
popis	popis	k1gInSc4	popis
gravitace	gravitace	k1gFnSc2	gravitace
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
fyzice	fyzika	k1gFnSc6	fyzika
<g/>
.	.	kIx.	.
</s>
