<s>
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1316	[number]	k4	1316
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1378	[number]	k4	1378
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
jedenáctý	jedenáctý	k4xOgMnSc1	jedenáctý
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
vládnoucí	vládnoucí	k2eAgMnSc1d1	vládnoucí
jako	jako	k9	jako
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
1346	[number]	k4	1346
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1378	[number]	k4	1378
<g/>
.	.	kIx.	.
</s>
