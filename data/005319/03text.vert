<s>
Moře	moře	k1gNnSc1	moře
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
plocha	plocha	k1gFnSc1	plocha
slané	slaný	k2eAgFnSc2d1	slaná
či	či	k8xC	či
brakické	brakický	k2eAgFnSc2d1	brakická
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
spojená	spojený	k2eAgFnSc1d1	spojená
se	se	k3xPyFc4	se
světovým	světový	k2eAgInSc7d1	světový
oceánem	oceán	k1gInSc7	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Moře	moře	k1gNnSc1	moře
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
oceánů	oceán	k1gInPc2	oceán
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
ohraničené	ohraničený	k2eAgFnSc2d1	ohraničená
pevninou	pevnina	k1gFnSc7	pevnina
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
souostrovími	souostroví	k1gNnPc7	souostroví
či	či	k8xC	či
poloostrovy	poloostrov	k1gInPc7	poloostrov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
okrajová	okrajový	k2eAgNnPc1d1	okrajové
moře	moře	k1gNnPc1	moře
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Severní	severní	k2eAgNnSc1d1	severní
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
Karibské	karibský	k2eAgNnSc1d1	Karibské
moře	moře	k1gNnSc1	moře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Okrajová	okrajový	k2eAgNnPc1d1	okrajové
moře	moře	k1gNnPc1	moře
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
systémy	systém	k1gInPc7	systém
proudů	proud	k1gInPc2	proud
<g/>
,	,	kIx,	,
slaností	slanost	k1gFnPc2	slanost
vody	voda	k1gFnSc2	voda
či	či	k8xC	či
usazeninami	usazenina	k1gFnPc7	usazenina
příliš	příliš	k6eAd1	příliš
neliší	lišit	k5eNaImIp3nP	lišit
od	od	k7c2	od
zbytku	zbytek	k1gInSc2	zbytek
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
<g/>
-li	i	k?	-li
pevnina	pevnina	k1gFnSc1	pevnina
moře	moře	k1gNnSc2	moře
téměř	téměř	k6eAd1	téměř
zcela	zcela	k6eAd1	zcela
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ně	on	k3xPp3gNnSc4	on
označení	označení	k1gNnSc4	označení
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
moře	moře	k1gNnSc4	moře
(	(	kIx(	(
<g/>
Středozemní	středozemní	k2eAgNnSc4d1	středozemní
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
Baltské	baltský	k2eAgNnSc1d1	Baltské
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
Rudé	rudý	k2eAgNnSc1d1	Rudé
moře	moře	k1gNnSc1	moře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
moře	moře	k1gNnSc4	moře
mívají	mívat	k5eAaImIp3nP	mívat
kvůli	kvůli	k7c3	kvůli
nadměrnému	nadměrný	k2eAgNnSc3d1	nadměrné
vypařování	vypařování	k1gNnSc3	vypařování
a	a	k8xC	a
malému	malý	k2eAgInSc3d1	malý
přítoku	přítok	k1gInSc3	přítok
říční	říční	k2eAgFnSc2d1	říční
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
vyšší	vysoký	k2eAgFnSc1d2	vyšší
(	(	kIx(	(
<g/>
Rudé	rudý	k2eAgNnSc1d1	Rudé
moře	moře	k1gNnSc1	moře
<g/>
)	)	kIx)	)
či	či	k8xC	či
nižší	nízký	k2eAgFnSc1d2	nižší
(	(	kIx(	(
<g/>
Baltské	baltský	k2eAgNnSc1d1	Baltské
moře	moře	k1gNnSc1	moře
<g/>
)	)	kIx)	)
slanost	slanost	k1gFnSc1	slanost
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
obvyklých	obvyklý	k2eAgInPc2d1	obvyklý
35	[number]	k4	35
‰	‰	k?	‰
a	a	k8xC	a
i	i	k9	i
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgInPc6d1	další
ohledech	ohled	k1gInPc6	ohled
se	se	k3xPyFc4	se
od	od	k7c2	od
okrajových	okrajový	k2eAgNnPc2d1	okrajové
moří	moře	k1gNnPc2	moře
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
<g/>
.	.	kIx.	.
</s>
<s>
Slanost	slanost	k1gFnSc1	slanost
(	(	kIx(	(
<g/>
salinitu	salinita	k1gFnSc4	salinita
<g/>
)	)	kIx)	)
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
sledují	sledovat	k5eAaImIp3nP	sledovat
oceánologové	oceánolog	k1gMnPc1	oceánolog
velice	velice	k6eAd1	velice
bedlivě	bedlivě	k6eAd1	bedlivě
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
případné	případný	k2eAgInPc1d1	případný
rychlé	rychlý	k2eAgInPc1d1	rychlý
výkyvy	výkyv	k1gInPc1	výkyv
množství	množství	k1gNnSc4	množství
soli	sůl	k1gFnSc2	sůl
ve	v	k7c6	v
sledovaných	sledovaný	k2eAgFnPc6d1	sledovaná
oblastech	oblast	k1gFnPc6	oblast
signalizují	signalizovat	k5eAaImIp3nP	signalizovat
potenciální	potenciální	k2eAgFnPc1d1	potenciální
významné	významný	k2eAgFnPc1d1	významná
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
mořských	mořský	k2eAgInPc6d1	mořský
biotopech	biotop	k1gInPc6	biotop
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
sledování	sledování	k1gNnSc2	sledování
a	a	k8xC	a
vyhodnocování	vyhodnocování	k1gNnSc2	vyhodnocování
slanosti	slanost	k1gFnSc2	slanost
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
salinitní	salinitní	k2eAgFnSc2d1	salinitní
mapy	mapa	k1gFnSc2	mapa
generované	generovaný	k2eAgInPc4d1	generovaný
přístroji	přístroj	k1gInPc7	přístroj
zvanými	zvaný	k2eAgInPc7d1	zvaný
solnografy	solnograf	k1gInPc7	solnograf
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
rozborem	rozbor	k1gInSc7	rozbor
lze	lze	k6eAd1	lze
poměrně	poměrně	k6eAd1	poměrně
přesně	přesně	k6eAd1	přesně
predikovat	predikovat	k5eAaBmF	predikovat
změny	změna	k1gFnPc4	změna
mořské	mořský	k2eAgFnSc2d1	mořská
fauny	fauna	k1gFnSc2	fauna
a	a	k8xC	a
flóry	flóra	k1gFnSc2	flóra
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jako	jako	k9	jako
moře	moře	k1gNnSc4	moře
tradičně	tradičně	k6eAd1	tradičně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepřesně	přesně	k6eNd1	přesně
označeno	označit	k5eAaPmNgNnS	označit
též	též	k6eAd1	též
velké	velký	k2eAgNnSc1d1	velké
vnitrozemské	vnitrozemský	k2eAgNnSc1d1	vnitrozemské
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
slané	slaný	k2eAgNnSc1d1	slané
<g/>
)	)	kIx)	)
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
kterému	který	k3yRgNnSc3	který
schází	scházet	k5eAaImIp3nS	scházet
přirozený	přirozený	k2eAgInSc1d1	přirozený
odtok	odtok	k1gInSc1	odtok
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Kaspické	kaspický	k2eAgNnSc1d1	Kaspické
moře	moře	k1gNnSc1	moře
nebo	nebo	k8xC	nebo
Mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
moře	moře	k1gNnSc1	moře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
však	však	k9	však
není	být	k5eNaImIp3nS	být
splněna	splnit	k5eAaPmNgFnS	splnit
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
spojitost	spojitost	k1gFnSc4	spojitost
s	s	k7c7	s
ostatními	ostatní	k2eAgNnPc7d1	ostatní
moři	moře	k1gNnPc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Moře	moře	k1gNnSc1	moře
a	a	k8xC	a
oceány	oceán	k1gInPc1	oceán
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
celkem	celkem	k6eAd1	celkem
361	[number]	k4	361
milionů	milion	k4xCgInPc2	milion
km2	km2	k4	km2
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
71	[number]	k4	71
%	%	kIx~	%
povrchu	povrch	k1gInSc2	povrch
planety	planeta	k1gFnSc2	planeta
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
objem	objem	k1gInSc1	objem
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
1	[number]	k4	1
370	[number]	k4	370
milionů	milion	k4xCgInPc2	milion
km3	km3	k4	km3
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Mořská	mořský	k2eAgFnSc1d1	mořská
voda	voda	k1gFnSc1	voda
tak	tak	k6eAd1	tak
představuje	představovat	k5eAaImIp3nS	představovat
96,5	[number]	k4	96,5
%	%	kIx~	%
planetárního	planetární	k2eAgNnSc2d1	planetární
vodstva	vodstvo	k1gNnSc2	vodstvo
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
hloubka	hloubka	k1gFnSc1	hloubka
světového	světový	k2eAgInSc2d1	světový
oceánu	oceán	k1gInSc2	oceán
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
790	[number]	k4	790
m.	m.	k?	m.
Alboránské	Alboránský	k2eAgNnSc4d1	Alboránský
moře	moře	k1gNnSc4	moře
Azovské	azovský	k2eAgNnSc1d1	Azovské
moře	moře	k1gNnSc1	moře
Baleárské	baleárský	k2eAgNnSc1d1	Baleárské
moře	moře	k1gNnSc1	moře
<g/>
/	/	kIx~	/
<g/>
Katalánské	katalánský	k2eAgNnSc1d1	katalánské
moře	moře	k1gNnSc1	moře
Baltské	baltský	k2eAgNnSc1d1	Baltské
moře	moře	k1gNnSc1	moře
Černé	Černá	k1gFnSc2	Černá
moře	moře	k1gNnSc2	moře
Egejské	egejský	k2eAgNnSc4d1	Egejské
moře	moře	k1gNnSc4	moře
Irské	irský	k2eAgNnSc1d1	irské
moře	moře	k1gNnSc1	moře
Irmingerovo	Irmingerův	k2eAgNnSc1d1	Irmingerův
moře	moře	k1gNnSc1	moře
Jaderské	jaderský	k2eAgNnSc1d1	Jaderské
moře	moře	k1gNnSc1	moře
Jónské	jónský	k2eAgNnSc1d1	Jónské
moře	moře	k1gNnSc1	moře
Kantaberské	Kantaberský	k2eAgNnSc1d1	Kantaberské
moře	moře	k1gNnSc1	moře
<g/>
/	/	kIx~	/
<g/>
Biskajský	biskajský	k2eAgInSc4d1	biskajský
záliv	záliv	k1gInSc4	záliv
Karibské	karibský	k2eAgNnSc1d1	Karibské
moře	moře	k1gNnSc1	moře
Keltské	keltský	k2eAgNnSc1d1	keltské
moře	moře	k1gNnSc1	moře
Krétské	krétský	k2eAgNnSc1d1	krétské
moře	moře	k1gNnSc1	moře
Labradorské	labradorský	k2eAgNnSc1d1	labradorský
moře	moře	k1gNnSc1	moře
Levantské	levantský	k2eAgNnSc1d1	levantský
<g />
.	.	kIx.	.
</s>
<s>
moře	moře	k1gNnSc1	moře
Ligurské	ligurský	k2eAgNnSc1d1	Ligurské
moře	moře	k1gNnSc1	moře
Libyjské	libyjský	k2eAgNnSc1d1	Libyjské
moře	moře	k1gNnSc1	moře
Marmarské	Marmarský	k2eAgNnSc1d1	Marmarské
moře	moře	k1gNnSc1	moře
Myrtské	Myrtský	k2eAgNnSc1d1	Myrtský
moře	moře	k1gNnSc1	moře
Sargasové	sargasový	k2eAgNnSc1d1	Sargasové
moře	moře	k1gNnSc2	moře
Severní	severní	k2eAgNnSc4d1	severní
moře	moře	k1gNnSc4	moře
Středozemní	středozemní	k2eAgNnSc4d1	středozemní
moře	moře	k1gNnSc4	moře
Thrácké	thrácký	k2eAgNnSc1d1	thrácké
moře	moře	k1gNnSc1	moře
Tyrhénské	tyrhénský	k2eAgNnSc1d1	Tyrhénské
moře	moře	k1gNnSc1	moře
Baffinovo	Baffinův	k2eAgNnSc1d1	Baffinův
moře	moře	k1gNnSc1	moře
Barentsovo	Barentsův	k2eAgNnSc1d1	Barentsovo
moře	moře	k1gNnSc1	moře
Beaufortovo	Beaufortův	k2eAgNnSc1d1	Beaufortovo
moře	moře	k1gNnSc1	moře
Bílé	bílý	k2eAgNnSc1d1	bílé
moře	moře	k1gNnSc1	moře
Čukotské	čukotský	k2eAgNnSc1d1	Čukotské
moře	moře	k1gNnSc1	moře
Grónské	grónský	k2eAgNnSc1d1	grónské
moře	moře	k1gNnSc1	moře
Karské	karský	k2eAgNnSc1d1	Karské
moře	moře	k1gNnSc1	moře
Lincolnovo	Lincolnův	k2eAgNnSc1d1	Lincolnovo
moře	moře	k1gNnSc1	moře
moře	moře	k1gNnSc2	moře
Laptěvů	Laptěv	k1gInPc2	Laptěv
Norské	norský	k2eAgNnSc4d1	norské
moře	moře	k1gNnSc4	moře
Pečorské	Pečorský	k2eAgNnSc1d1	Pečorský
moře	moře	k1gNnSc1	moře
Východosibiřské	východosibiřský	k2eAgNnSc1d1	Východosibiřské
moře	moře	k1gNnSc1	moře
Andamanské	Andamanský	k2eAgNnSc1d1	Andamanské
moře	moře	k1gNnSc1	moře
Arabské	arabský	k2eAgNnSc1d1	arabské
moře	moře	k1gNnSc1	moře
Davisovo	Davisův	k2eAgNnSc1d1	Davisovo
moře	moře	k1gNnSc1	moře
Lakadivské	lakadivský	k2eAgNnSc1d1	lakadivský
<g />
.	.	kIx.	.
</s>
<s>
moře	moře	k1gNnSc1	moře
Rudé	rudý	k2eAgNnSc1d1	Rudé
moře	moře	k1gNnSc1	moře
Arafurské	Arafurský	k2eAgNnSc1d1	Arafurské
moře	moře	k1gNnSc1	moře
Balijské	Balijský	k2eAgNnSc1d1	Balijský
moře	moře	k1gNnSc1	moře
Bandské	Bandský	k2eAgNnSc1d1	Bandský
moře	moře	k1gNnSc1	moře
Beringovo	Beringův	k2eAgNnSc1d1	Beringovo
moře	moře	k1gNnSc1	moře
Bismarckovo	Bismarckův	k2eAgNnSc1d1	Bismarckovo
moře	moře	k1gNnSc1	moře
<g/>
/	/	kIx~	/
<g/>
Novoguinejské	novoguinejský	k2eAgNnSc1d1	novoguinejský
moře	moře	k1gNnSc1	moře
Boholské	Boholský	k2eAgNnSc1d1	Boholský
moře	moře	k1gNnSc1	moře
<g/>
/	/	kIx~	/
<g/>
Mindanajské	Mindanajský	k2eAgNnSc1d1	Mindanajský
moře	moře	k1gNnSc1	moře
Camotské	Camotský	k2eAgNnSc1d1	Camotský
moře	moře	k1gNnSc1	moře
Celebeské	celebeský	k2eAgNnSc1d1	celebeský
moře	moře	k1gNnSc1	moře
Fidžijské	Fidžijský	k2eAgNnSc1d1	Fidžijský
moře	moře	k1gNnSc1	moře
Filipínské	filipínský	k2eAgNnSc1d1	filipínské
moře	moře	k1gNnSc1	moře
Floreské	Floreský	k2eAgNnSc1d1	Floreský
moře	moře	k1gNnSc1	moře
Halmaherské	Halmaherský	k2eAgNnSc1d1	Halmaherský
moře	moře	k1gNnSc1	moře
Jávské	jávský	k2eAgNnSc1d1	jávské
moře	moře	k1gNnSc1	moře
Japonské	japonský	k2eAgNnSc1d1	Japonské
moře	moře	k1gNnSc1	moře
<g/>
/	/	kIx~	/
<g/>
Východní	východní	k2eAgNnSc1d1	východní
moře	moře	k1gNnSc1	moře
Jihočínské	jihočínský	k2eAgNnSc1d1	Jihočínské
moře	moře	k1gNnSc1	moře
Korálové	korálový	k2eAgNnSc1d1	korálové
moře	moře	k1gNnSc1	moře
<g />
.	.	kIx.	.
</s>
<s>
Korské	Korský	k2eAgNnSc1d1	Korský
moře	moře	k1gNnSc1	moře
Ochotské	ochotský	k2eAgNnSc1d1	Ochotské
moře	moře	k1gNnSc1	moře
Pochajské	Pochajský	k2eAgNnSc1d1	Pochajský
moře	moře	k1gNnSc1	moře
Samarské	samarský	k2eAgNnSc1d1	samarský
moře	moře	k1gNnSc1	moře
Sawuské	Sawuský	k2eAgNnSc1d1	Sawuský
moře	moře	k1gNnSc1	moře
<g/>
/	/	kIx~	/
<g/>
Savuské	Savuský	k2eAgNnSc1d1	Savuský
moře	moře	k1gNnSc1	moře
Seramské	Seramský	k2eAgNnSc1d1	Seramský
moře	moře	k1gNnSc1	moře
Sibuyanské	Sibuyanský	k2eAgNnSc1d1	Sibuyanský
moře	moře	k1gNnSc1	moře
Suluské	Suluský	k2eAgNnSc1d1	Suluské
moře	moře	k1gNnSc1	moře
Šalamounovo	Šalamounův	k2eAgNnSc1d1	Šalamounovo
moře	moře	k1gNnSc1	moře
Tasmanovo	Tasmanův	k2eAgNnSc1d1	Tasmanovo
moře	moře	k1gNnSc1	moře
Timorské	Timorský	k2eAgNnSc1d1	Timorské
moře	moře	k1gNnSc1	moře
Visayské	Visayské	k2eAgNnSc2d1	Visayské
moře	moře	k1gNnSc2	moře
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
moře	moře	k1gNnSc1	moře
Východočínské	východočínský	k2eAgNnSc1d1	Východočínské
moře	moře	k1gNnSc1	moře
Žluté	žlutý	k2eAgNnSc1d1	žluté
moře	moře	k1gNnSc1	moře
Amundsenovo	Amundsenův	k2eAgNnSc1d1	Amundsenovo
moře	moře	k1gNnSc1	moře
Bellingshausenovo	Bellingshausenův	k2eAgNnSc1d1	Bellingshausenův
moře	moře	k1gNnSc1	moře
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Urvillovo	Urvillův	k2eAgNnSc1d1	Urvillův
moře	moře	k1gNnSc1	moře
Lazarevovo	Lazarevův	k2eAgNnSc1d1	Lazarevův
moře	moře	k1gNnSc1	moře
Mawsonovo	Mawsonův	k2eAgNnSc1d1	Mawsonův
moře	moře	k1gNnSc1	moře
moře	moře	k1gNnSc2	moře
Kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
moře	moře	k1gNnSc2	moře
Scotia	Scotium	k1gNnSc2	Scotium
moře	moře	k1gNnSc2	moře
Spolupráce	spolupráce	k1gFnSc1	spolupráce
Riiser-Larsenovo	Riiser-Larsenův	k2eAgNnSc1d1	Riiser-Larsenův
moře	moře	k1gNnSc4	moře
Rossovo	Rossův	k2eAgNnSc4d1	Rossovo
moře	moře	k1gNnSc4	moře
Weddellovo	Weddellův	k2eAgNnSc4d1	Weddellovo
moře	moře	k1gNnSc4	moře
Takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
měsíční	měsíční	k2eAgNnSc4d1	měsíční
moře	moře	k1gNnSc4	moře
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
maria	maria	k1gFnSc1	maria
<g/>
,	,	kIx,	,
j.	j.	k?	j.
č.	č.	k?	č.
mare	mar	k1gInSc2	mar
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
čedičové	čedičový	k2eAgFnPc4d1	čedičová
pláně	pláň	k1gFnPc4	pláň
<g/>
,	,	kIx,	,
pokrývající	pokrývající	k2eAgInPc4d1	pokrývající
asi	asi	k9	asi
16	[number]	k4	16
%	%	kIx~	%
povrchu	povrch	k1gInSc3	povrch
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Temné	temný	k2eAgFnPc1d1	temná
plochy	plocha	k1gFnPc1	plocha
<g/>
,	,	kIx,	,
vytvořené	vytvořený	k2eAgFnPc1d1	vytvořená
kdysi	kdysi	k6eAd1	kdysi
rozsáhlými	rozsáhlý	k2eAgInPc7d1	rozsáhlý
výlevy	výlev	k1gInPc7	výlev
magmatu	magma	k1gNnSc2	magma
po	po	k7c6	po
dopadech	dopad	k1gInPc6	dopad
meteoritů	meteorit	k1gInPc2	meteorit
<g/>
,	,	kIx,	,
považovali	považovat	k5eAaImAgMnP	považovat
totiž	totiž	k9	totiž
první	první	k4xOgMnPc1	první
astronomové	astronom	k1gMnPc1	astronom
za	za	k7c4	za
skutečná	skutečný	k2eAgNnPc4d1	skutečné
moře	moře	k1gNnPc4	moře
<g/>
,	,	kIx,	,
podobná	podobný	k2eAgNnPc4d1	podobné
těm	ten	k3xDgMnPc3	ten
pozemským	pozemský	k2eAgMnPc3d1	pozemský
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
představu	představa	k1gFnSc4	představa
upomínají	upomínat	k5eAaImIp3nP	upomínat
jen	jen	k9	jen
tradiční	tradiční	k2eAgNnPc4d1	tradiční
latinská	latinský	k2eAgNnPc4d1	latinské
pojmenování	pojmenování	k1gNnSc4	pojmenování
měsíčních	měsíční	k2eAgNnPc2d1	měsíční
"	"	kIx"	"
<g/>
moří	moře	k1gNnPc2	moře
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
oceánů	oceán	k1gInPc2	oceán
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
zálivů	záliv	k1gInPc2	záliv
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kupříkladu	kupříkladu	k6eAd1	kupříkladu
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1969	[number]	k4	1969
přistála	přistát	k5eAaImAgFnS	přistát
výprava	výprava	k1gFnSc1	výprava
Apolla	Apollo	k1gNnSc2	Apollo
11	[number]	k4	11
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc1	název
Mare	Mar	k1gFnSc2	Mar
Tranquillitatis	Tranquillitatis	k1gFnSc2	Tranquillitatis
–	–	k?	–
"	"	kIx"	"
<g/>
Moře	moře	k1gNnSc1	moře
Klidu	klid	k1gInSc2	klid
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Analogicky	analogicky	k6eAd1	analogicky
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
moře	moře	k1gNnSc4	moře
označují	označovat	k5eAaImIp3nP	označovat
podobné	podobný	k2eAgFnPc4d1	podobná
temnější	temný	k2eAgFnPc4d2	temnější
oblasti	oblast	k1gFnPc4	oblast
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Merkuru	Merkur	k1gInSc2	Merkur
a	a	k8xC	a
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
