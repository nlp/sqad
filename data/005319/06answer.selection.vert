<s>
Střední	střední	k2eAgFnSc1d1	střední
hloubka	hloubka	k1gFnSc1	hloubka
světového	světový	k2eAgInSc2d1	světový
oceánu	oceán	k1gInSc2	oceán
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
790	[number]	k4	790
m.	m.	k?	m.
Alboránské	Alboránský	k2eAgNnSc4d1	Alboránský
moře	moře	k1gNnSc4	moře
Azovské	azovský	k2eAgNnSc1d1	Azovské
moře	moře	k1gNnSc1	moře
Baleárské	baleárský	k2eAgNnSc1d1	Baleárské
moře	moře	k1gNnSc1	moře
<g/>
/	/	kIx~	/
<g/>
Katalánské	katalánský	k2eAgNnSc1d1	katalánské
moře	moře	k1gNnSc1	moře
Baltské	baltský	k2eAgNnSc1d1	Baltské
moře	moře	k1gNnSc1	moře
Černé	Černá	k1gFnSc2	Černá
moře	moře	k1gNnSc2	moře
Egejské	egejský	k2eAgNnSc4d1	Egejské
moře	moře	k1gNnSc4	moře
Irské	irský	k2eAgNnSc1d1	irské
moře	moře	k1gNnSc1	moře
Irmingerovo	Irmingerův	k2eAgNnSc1d1	Irmingerův
moře	moře	k1gNnSc1	moře
Jaderské	jaderský	k2eAgNnSc1d1	Jaderské
moře	moře	k1gNnSc1	moře
Jónské	jónský	k2eAgNnSc1d1	Jónské
moře	moře	k1gNnSc1	moře
Kantaberské	Kantaberský	k2eAgNnSc1d1	Kantaberské
moře	moře	k1gNnSc1	moře
<g/>
/	/	kIx~	/
<g/>
Biskajský	biskajský	k2eAgInSc4d1	biskajský
záliv	záliv	k1gInSc4	záliv
Karibské	karibský	k2eAgNnSc1d1	Karibské
moře	moře	k1gNnSc1	moře
Keltské	keltský	k2eAgNnSc1d1	keltské
moře	moře	k1gNnSc1	moře
Krétské	krétský	k2eAgNnSc1d1	krétské
moře	moře	k1gNnSc1	moře
Labradorské	labradorský	k2eAgNnSc1d1	labradorský
moře	moře	k1gNnSc1	moře
Levantské	levantský	k2eAgNnSc1d1	levantský
<g />
.	.	kIx.	.
</s>
