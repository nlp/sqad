<s>
Automatic	Automatice	k1gFnPc2
Warning	Warning	k1gInSc1
System	Syst	k1gMnSc7
</s>
<s>
Indikátor	indikátor	k1gInSc1
AWS	AWS	kA
na	na	k7c6
stanovišti	stanoviště	k1gNnSc6
strojvedoucího	strojvedoucí	k1gMnSc4
</s>
<s>
Automatic	Automatice	k1gFnPc2
Warning	Warning	k1gInSc1
System	Syst	k1gMnSc7
(	(	kIx(
<g/>
AWS	AWS	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vlakový	vlakový	k2eAgInSc4d1
zabezpečovač	zabezpečovač	k1gInSc4
zavedený	zavedený	k2eAgInSc4d1
do	do	k7c2
provozu	provoz	k1gInSc2
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
roku	rok	k1gInSc2
1948	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
založen	založit	k5eAaPmNgMnS
na	na	k7c6
systému	systém	k1gInSc6
“	“	k?
<g/>
Strowger-Hudd	Strowger-Hudd	k1gInSc1
<g/>
”	”	k?
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
roku	rok	k1gInSc2
1930	#num#	k4
vyvinul	vyvinout	k5eAaPmAgMnS
Alfred	Alfred	k1gMnSc1
Ernest	Ernest	k1gMnSc1
Hudd	Hudd	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
nahradil	nahradit	k5eAaPmAgInS
dřívější	dřívější	k2eAgInSc1d1
systém	systém	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1906	#num#	k4
zavedený	zavedený	k2eAgInSc1d1
u	u	k7c2
Great	Great	k2eAgInSc4d1
Western	Western	kA
Railway	Railwaa	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
na	na	k7c6
galvanickém	galvanický	k2eAgInSc6d1
kontaktu	kontakt	k1gInSc6
mezi	mezi	k7c7
vozidlovou	vozidlový	k2eAgFnSc7d1
a	a	k8xC
traťovou	traťový	k2eAgFnSc7d1
částí	část	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1956	#num#	k4
je	být	k5eAaImIp3nS
standardně	standardně	k6eAd1
používán	používat	k5eAaImNgInS
pro	pro	k7c4
celou	celý	k2eAgFnSc4d1
železniční	železniční	k2eAgFnSc4d1
síť	síť	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc4
systém	systém	k1gInSc4
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
i	i	k9
jiné	jiné	k1gNnSc4
železniční	železniční	k2eAgFnSc2d1
správy	správa	k1gFnSc2
-	-	kIx~
např.	např.	kA
indické	indický	k2eAgFnPc4d1
železnice	železnice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
British	British	k1gMnSc1
Rail	Rail	k1gMnSc1
AWS	AWS	kA
</s>
<s>
Induktor	induktor	k1gInSc1
AWS	AWS	kA
</s>
<s>
Traťová	traťový	k2eAgFnSc1d1
část	část	k1gFnSc1
BR	br	k0
AWS	AWS	kA
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
</s>
<s>
permanentního	permanentní	k2eAgInSc2d1
magnetu	magnet	k1gInSc2
v	v	k7c6
ose	osa	k1gFnSc6
koleje	kolej	k1gFnSc2
</s>
<s>
elektromagnetu	elektromagnet	k1gInSc3
s	s	k7c7
opačným	opačný	k2eAgNnSc7d1
pólem	pólo	k1gNnSc7
v	v	k7c6
ose	osa	k1gFnSc6
koleje	kolej	k1gFnSc2
za	za	k7c7
permanentím	permanentí	k2eAgInSc7d1
magnetem	magnet	k1gInSc7
ve	v	k7c6
směru	směr	k1gInSc6
jízdy	jízda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vozidlová	vozidlový	k2eAgFnSc1d1
část	část	k1gFnSc1
BR	br	k0
AWS	AWS	kA
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
</s>
<s>
ukazatele	ukazatel	k1gInPc1
na	na	k7c6
stanovišti	stanoviště	k1gNnSc6
strojvedoucího	strojvedoucí	k1gMnSc4
v	v	k7c6
podobě	podoba	k1gFnSc6
žlutého	žlutý	k2eAgNnSc2d1
světla	světlo	k1gNnSc2
s	s	k7c7
černým	černý	k2eAgInSc7d1
ornamentem	ornament	k1gInSc7
označovaného	označovaný	k2eAgInSc2d1
jako	jako	k8xC,k8xS
„	„	k?
<g/>
AWS	AWS	kA
slunečnice	slunečnice	k1gFnSc2
<g/>
“	“	k?
</s>
<s>
řídící	řídící	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
s	s	k7c7
vazbou	vazba	k1gFnSc7
na	na	k7c4
vlakovou	vlakový	k2eAgFnSc4d1
brzdu	brzda	k1gFnSc4
</s>
<s>
potvrzujícího	potvrzující	k2eAgNnSc2d1
tlačítka	tlačítko	k1gNnSc2
na	na	k7c6
stanovišti	stanoviště	k1gNnSc6
strojvedoucího	strojvedoucí	k1gMnSc4
</s>
<s>
ovládacího	ovládací	k2eAgInSc2d1
panelu	panel	k1gInSc2
</s>
<s>
Popis	popis	k1gInSc1
funkce	funkce	k1gFnSc2
</s>
<s>
AWS	AWS	kA
předává	předávat	k5eAaImIp3nS
strojvedoucímu	strojvedoucí	k1gMnSc3
informaci	informace	k1gFnSc4
o	o	k7c4
postavení	postavení	k1gNnSc4
následujícího	následující	k2eAgNnSc2d1
návěstidla	návěstidlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
přenosu	přenos	k1gInSc3
této	tento	k3xDgFnSc2
informace	informace	k1gFnSc1
dochází	docházet	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
cca	cca	kA
180	#num#	k4
m	m	kA
(	(	kIx(
<g/>
200	#num#	k4
yardů	yard	k1gInPc2
<g/>
)	)	kIx)
před	před	k7c7
návěstidlem	návěstidlo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
přenosu	přenos	k1gInSc3
dochází	docházet	k5eAaImIp3nS
magnetickou	magnetický	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
ze	z	k7c2
schránky	schránka	k1gFnSc2
umístěné	umístěný	k2eAgFnSc2d1
v	v	k7c6
ose	osa	k1gFnSc6
koleje	kolej	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
schránka	schránka	k1gFnSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
AWS	AWS	kA
induktor	induktor	k1gInSc1
nebo	nebo	k8xC
lidově	lidově	k6eAd1
AWS	AWS	kA
rampa	rampa	k1gFnSc1
podle	podle	k7c2
ochranných	ochranný	k2eAgInPc2d1
náběhů	náběh	k1gInPc2
-	-	kIx~
ramp	rampa	k1gFnPc2
na	na	k7c6
jejích	její	k3xOp3gInPc6
koncích	konec	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
následující	následující	k2eAgNnSc4d1
návěstidlo	návěstidlo	k1gNnSc4
ukazuje	ukazovat	k5eAaImIp3nS
návěst	návěst	k1gFnSc4
volno	volno	k6eAd1
<g/>
,	,	kIx,
elektromagnetem	elektromagnet	k1gInSc7
prochází	procházet	k5eAaImIp3nS
proud	proud	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
průjezdu	průjezd	k1gInSc6
nad	nad	k7c7
AWS	AWS	kA
rampou	rampa	k1gFnSc7
přijde	přijít	k5eAaPmIp3nS
nejprve	nejprve	k6eAd1
od	od	k7c2
permanentního	permanentní	k2eAgInSc2d1
magnetu	magnet	k1gInSc2
signál	signál	k1gInSc1
ke	k	k7c3
spuštění	spuštění	k1gNnSc3
výstrahy	výstraha	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
vzápětí	vzápětí	k6eAd1
zrušen	zrušit	k5eAaPmNgInS
elektromagnetem	elektromagnet	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomuto	tento	k3xDgNnSc3
zrušení	zrušení	k1gNnSc3
musí	muset	k5eAaImIp3nP
dojít	dojít	k5eAaPmF
během	během	k7c2
1	#num#	k4
s	s	k7c7
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
dojde	dojít	k5eAaPmIp3nS
ke	k	k7c3
spuštění	spuštění	k1gNnSc3
výstrahy	výstraha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
průjezd	průjezd	k1gInSc4
přes	přes	k7c4
rampu	rampa	k1gFnSc4
AWS	AWS	kA
signalizující	signalizující	k2eAgNnSc1d1
volno	volno	k1gNnSc1
je	být	k5eAaImIp3nS
strojvedoucí	strojvedoucí	k1gMnSc1
upozorněn	upozornit	k5eAaPmNgMnS
zvonkem	zvonek	k1gInSc7
<g/>
,	,	kIx,
nemusí	muset	k5eNaImIp3nS
však	však	k9
nijak	nijak	k6eAd1
reagovat	reagovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Optický	optický	k2eAgInSc1d1
ukazatel	ukazatel	k1gInSc1
zůstává	zůstávat	k5eAaImIp3nS
zhaslý	zhaslý	k2eAgInSc1d1
<g/>
,	,	kIx,
případně	případně	k6eAd1
zhasne	zhasnout	k5eAaPmIp3nS
<g/>
,	,	kIx,
pokud	pokud	k8xS
svítil	svítit	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
na	na	k7c6
následujícím	následující	k2eAgNnSc6d1
návěstidle	návěstidlo	k1gNnSc6
návěst	návěst	k1gFnSc4
omezující	omezující	k2eAgFnSc4d1
jízdu	jízda	k1gFnSc4
<g/>
,	,	kIx,
elektromagnet	elektromagnet	k1gInSc1
je	být	k5eAaImIp3nS
odpojen	odpojit	k5eAaPmNgInS
a	a	k8xC
tedy	tedy	k8xC
nedojde	dojít	k5eNaPmIp3nS
ke	k	k7c3
zrušení	zrušení	k1gNnSc3
výstrahy	výstraha	k1gFnSc2
spuštěné	spuštěný	k2eAgFnSc2d1
permanentním	permanentní	k2eAgNnSc7d1
magnetem	magneto	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jedné	jeden	k4xCgFnSc6
sekundě	sekunda	k1gFnSc6
se	se	k3xPyFc4
rozezní	rozeznět	k5eAaPmIp3nS
houkačka	houkačka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strojvedoucí	strojvedoucí	k1gMnSc1
musí	muset	k5eAaImIp3nS
během	běh	k1gInSc7
následující	následující	k2eAgNnSc4d1
1,5	1,5	k4
s	s	k7c7
obsloužit	obsloužit	k5eAaPmF
tlačítko	tlačítko	k1gNnSc4
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
samočinnému	samočinný	k2eAgNnSc3d1
zaúčinkování	zaúčinkování	k1gNnSc3
vlakové	vlakový	k2eAgFnSc2d1
brzdy	brzda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
obsloužení	obsloužení	k1gNnSc6
tlačítka	tlačítko	k1gNnSc2
houkačka	houkačka	k1gFnSc1
zmlkne	zmlknout	k5eAaPmIp3nS
a	a	k8xC
rozsvítí	rozsvítit	k5eAaPmIp3nS
se	se	k3xPyFc4
optický	optický	k2eAgInSc4d1
ukazatel	ukazatel	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
strojvedoucímu	strojvedoucí	k1gMnSc3
připomíná	připomínat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jede	jet	k5eAaImIp3nS
na	na	k7c4
návěst	návěst	k1gFnSc4
výstraha	výstraha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
pracuje	pracovat	k5eAaImIp3nS
spolehlivě	spolehlivě	k6eAd1
až	až	k9
do	do	k7c2
rychlosti	rychlost	k1gFnSc2
3	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
,	,	kIx,
při	při	k7c6
rychlosti	rychlost	k1gFnSc6
nižší	nízký	k2eAgFnSc1d2
se	se	k3xPyFc4
houkačka	houkačka	k1gFnSc1
rozezní	rozeznět	k5eAaPmIp3nS
i	i	k9
v	v	k7c6
poloze	poloha	k1gFnSc6
volno	volno	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
zmlkne	zmlknout	k5eAaPmIp3nS
při	při	k7c6
průjezdu	průjezd	k1gInSc6
nad	nad	k7c7
elektromagnetem	elektromagnet	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukazatel	ukazatel	k1gInSc1
přitom	přitom	k6eAd1
zůstane	zůstat	k5eAaPmIp3nS
zhaslý	zhaslý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Systém	systém	k1gInSc1
je	být	k5eAaImIp3nS
bezpečný	bezpečný	k2eAgInSc1d1
při	při	k7c6
poruše	porucha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
selhání	selhání	k1gNnSc4
elektromagnetu	elektromagnet	k1gInSc2
dostane	dostat	k5eAaPmIp3nS
výstrahu	výstraha	k1gFnSc4
každé	každý	k3xTgNnSc1
vozidlo	vozidlo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
trpí	trpět	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
nevýhodou	nevýhoda	k1gFnSc7
-	-	kIx~
při	při	k7c6
použití	použití	k1gNnSc6
na	na	k7c6
jednokolejných	jednokolejný	k2eAgFnPc6d1
tratích	trať	k1gFnPc6
nebo	nebo	k8xC
při	při	k7c6
jízdě	jízda	k1gFnSc6
vlaku	vlak	k1gInSc2
po	po	k7c6
nesprávné	správný	k2eNgFnSc6d1
koleji	kolej	k1gFnSc6
dostává	dostávat	k5eAaImIp3nS
strojvůdce	strojvůdce	k1gMnSc1
výstrahu	výstraha	k1gFnSc4
při	při	k7c6
průjezdu	průjezd	k1gInSc6
přes	přes	k7c4
každou	každý	k3xTgFnSc4
AWS	AWS	kA
rampu	rampa	k1gFnSc4
opačného	opačný	k2eAgInSc2d1
směru	směr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
nevýhodu	nevýhoda	k1gFnSc4
odstraňuje	odstraňovat	k5eAaImIp3nS
další	další	k2eAgInSc1d1
elektromagnet	elektromagnet	k1gInSc1
umístěný	umístěný	k2eAgInSc1d1
u	u	k7c2
permanentního	permanentní	k2eAgInSc2d1
magnetu	magnet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
nabuzen	nabuzen	k2eAgMnSc1d1
<g/>
,	,	kIx,
ruší	rušit	k5eAaImIp3nS
magnetické	magnetický	k2eAgNnSc1d1
pole	pole	k1gNnSc1
permanentního	permanentní	k2eAgInSc2d1
magnetu	magnet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Levnější	levný	k2eAgFnSc7d2
alternativou	alternativa	k1gFnSc7
je	být	k5eAaImIp3nS
umístění	umístění	k1gNnSc1
traťových	traťový	k2eAgFnPc2d1
značek	značka	k1gFnPc2
upozorňujících	upozorňující	k2eAgFnPc2d1
na	na	k7c4
neplatnost	neplatnost	k1gFnSc4
výstrahy	výstraha	k1gFnSc2
pro	pro	k7c4
daný	daný	k2eAgInSc4d1
směr	směr	k1gInSc4
-	-	kIx~
tyto	tento	k3xDgFnPc1
značky	značka	k1gFnPc1
mají	mít	k5eAaImIp3nP
podobu	podoba	k1gFnSc4
modré	modrý	k2eAgFnSc2d1
tabule	tabule	k1gFnSc2
s	s	k7c7
bílým	bílý	k2eAgInSc7d1
ondřejským	ondřejský	k2eAgInSc7d1
křížem	kříž	k1gInSc7
<g/>
,	,	kIx,
případně	případně	k6eAd1
žluté	žlutý	k2eAgFnPc4d1
desky	deska	k1gFnPc4
s	s	k7c7
černým	černý	k2eAgInSc7d1
křížem	kříž	k1gInSc7
<g/>
,	,	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
toto	tento	k3xDgNnSc1
spojeno	spojen	k2eAgNnSc1d1
s	s	k7c7
dočasným	dočasný	k2eAgNnSc7d1
omezením	omezení	k1gNnSc7
rychlosti	rychlost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dříve	dříve	k6eAd2
byl	být	k5eAaImAgInS
AWS	AWS	kA
systém	systém	k1gInSc4
instalován	instalovat	k5eAaBmNgInS
pouze	pouze	k6eAd1
u	u	k7c2
předvěstí	předvěst	k1gFnPc2
<g/>
,	,	kIx,
při	při	k7c6
používání	používání	k1gNnSc6
víceznakých	víceznaký	k2eAgNnPc2d1
návěstidel	návěstidlo	k1gNnPc2
je	být	k5eAaImIp3nS
umístěn	umístěn	k2eAgInSc1d1
před	před	k7c7
každým	každý	k3xTgNnSc7
návěstidlem	návěstidlo	k1gNnSc7
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
i	i	k9
před	před	k7c7
zastávkami	zastávka	k1gFnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
návěsti	návěst	k1gFnPc1
kromě	kromě	k7c2
volna	volno	k1gNnSc2
spustí	spustit	k5eAaPmIp3nS
výstrahu	výstraha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Nevýhody	nevýhoda	k1gFnPc1
systému	systém	k1gInSc2
</s>
<s>
Tento	tento	k3xDgInSc1
systém	systém	k1gInSc1
indikuje	indikovat	k5eAaBmIp3nS
pouze	pouze	k6eAd1
dva	dva	k4xCgInPc1
stavy	stav	k1gInPc1
návěstidla	návěstidlo	k1gNnSc2
-	-	kIx~
je	být	k5eAaImIp3nS
volno	volno	k1gNnSc4
a	a	k8xC
není	být	k5eNaImIp3nS
volno	volno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
snadno	snadno	k6eAd1
překonán	překonat	k5eAaPmNgInS
navyklou	navyklý	k2eAgFnSc7d1
reakcí	reakce	k1gFnSc7
strojvedoucí	strojvedoucí	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ho	on	k3xPp3gMnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
při	při	k7c6
jízdě	jízda	k1gFnSc6
„	„	k?
<g/>
na	na	k7c4
dvě	dva	k4xCgFnPc4
žluté	žlutý	k2eAgFnPc4d1
<g/>
“	“	k?
(	(	kIx(
<g/>
t.	t.	k?
zn.	zn.	kA
přespříští	přespříští	k2eAgNnSc1d1
návěstidlo	návěstidlo	k1gNnSc1
v	v	k7c6
poloze	poloha	k1gFnSc6
stůj	stát	k5eAaImRp2nS
<g/>
)	)	kIx)
v	v	k7c6
několika	několik	k4yIc6
následujících	následující	k2eAgInPc6d1
úsecích	úsek	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
mělo	mít	k5eAaImAgNnS
za	za	k7c4
následek	následek	k1gInSc4
již	již	k9
mnoho	mnoho	k4c4
těžkých	těžký	k2eAgFnPc2d1
nehod	nehoda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
nedojde	dojít	k5eNaPmIp3nS
k	k	k7c3
samočinnému	samočinný	k2eAgNnSc3d1
zastavení	zastavení	k1gNnSc3
vlaku	vlak	k1gInSc2
při	při	k7c6
projetí	projetí	k1gNnSc6
návěsti	návěst	k1gFnSc2
stůj	stát	k5eAaImRp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
nevýhodu	nevýhoda	k1gFnSc4
odstraňuje	odstraňovat	k5eAaImIp3nS
až	až	k9
novější	nový	k2eAgInSc4d2
systém	systém	k1gInSc4
TPWS	TPWS	kA
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Automatic_warning_system_	Automatic_warning_system_	k1gFnSc2
<g/>
(	(	kIx(
<g/>
railways	railways	k6eAd1
<g/>
)	)	kIx)
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
