<p>
<s>
První	první	k4xOgFnSc1	první
parta	parta	k1gFnSc1	parta
je	být	k5eAaImIp3nS	být
román	román	k1gInSc4	román
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
o	o	k7c6	o
lidské	lidský	k2eAgFnSc6d1	lidská
statečnosti	statečnost	k1gFnSc6	statečnost
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc1	příběh
hrstky	hrstka	k1gFnSc2	hrstka
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
v	v	k7c6	v
extrémních	extrémní	k2eAgFnPc6d1	extrémní
podmínkách	podmínka	k1gFnPc6	podmínka
musejí	muset	k5eAaImIp3nP	muset
zmobilizovat	zmobilizovat	k5eAaPmF	zmobilizovat
veškerou	veškerý	k3xTgFnSc4	veškerý
svou	svůj	k3xOyFgFnSc4	svůj
odvahu	odvaha	k1gFnSc4	odvaha
<g/>
,	,	kIx,	,
kamarádství	kamarádství	k1gNnSc4	kamarádství
a	a	k8xC	a
solidaritu	solidarita	k1gFnSc4	solidarita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dvou	dva	k4xCgInPc2	dva
až	až	k9	až
tří	tři	k4xCgInPc2	tři
dnů	den	k1gInPc2	den
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
omezen	omezit	k5eAaPmNgInS	omezit
na	na	k7c4	na
zážitky	zážitek	k1gInPc4	zážitek
příslušníků	příslušník	k1gMnPc2	příslušník
první	první	k4xOgFnSc2	první
záchranné	záchranný	k2eAgFnSc2d1	záchranná
čety	četa	k1gFnSc2	četa
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
důlního	důlní	k2eAgNnSc2d1	důlní
neštěstí	neštěstí	k1gNnSc2	neštěstí
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
se	se	k3xPyFc4	se
záměrně	záměrně	k6eAd1	záměrně
nesoustředil	soustředit	k5eNaPmAgInS	soustředit
na	na	k7c4	na
detailní	detailní	k2eAgFnSc4d1	detailní
kresbu	kresba	k1gFnSc4	kresba
hornického	hornický	k2eAgNnSc2d1	Hornické
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
na	na	k7c4	na
sociální	sociální	k2eAgInPc4d1	sociální
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
pozornost	pozornost	k1gFnSc1	pozornost
se	se	k3xPyFc4	se
soustředila	soustředit	k5eAaPmAgFnS	soustředit
především	především	k9	především
na	na	k7c4	na
vykreslení	vykreslení	k1gNnSc4	vykreslení
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
vztahů	vztah	k1gInPc2	vztah
uvnitř	uvnitř	k7c2	uvnitř
této	tento	k3xDgFnSc2	tento
různorodé	různorodý	k2eAgFnSc2d1	různorodá
lidské	lidský	k2eAgFnSc2d1	lidská
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
nebývá	bývat	k5eNaImIp3nS	bývat
řazena	řadit	k5eAaImNgFnS	řadit
mezi	mezi	k7c4	mezi
Čapkova	Čapkův	k2eAgNnPc4d1	Čapkovo
nejzdařilejší	zdařilý	k2eAgNnPc4d3	nejzdařilejší
díla	dílo	k1gNnPc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgNnSc7d1	významné
svědectvím	svědectví	k1gNnSc7	svědectví
o	o	k7c6	o
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
panovaly	panovat	k5eAaImAgFnP	panovat
v	v	k7c6	v
uhelných	uhelný	k2eAgInPc6d1	uhelný
dolech	dol	k1gInPc6	dol
meziválečného	meziválečný	k2eAgNnSc2d1	meziválečné
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Materiál	materiál	k1gInSc1	materiál
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
románu	román	k1gInSc6	román
získával	získávat	k5eAaImAgMnS	získávat
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
pobytem	pobyt	k1gInSc7	pobyt
mezi	mezi	k7c4	mezi
horníky	horník	k1gMnPc4	horník
v	v	k7c6	v
dolech	dol	k1gInPc6	dol
na	na	k7c6	na
Kladensku	Kladensko	k1gNnSc6	Kladensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příběh	příběh	k1gInSc1	příběh
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
románu	román	k1gInSc2	román
je	být	k5eAaImIp3nS	být
Standa	Standa	k1gMnSc1	Standa
Půlpán	půlpán	k1gMnSc1	půlpán
<g/>
,	,	kIx,	,
sirotek	sirotek	k1gMnSc1	sirotek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
financí	finance	k1gFnPc2	finance
musí	muset	k5eAaImIp3nP	muset
zanechat	zanechat	k5eAaPmF	zanechat
školy	škola	k1gFnPc4	škola
a	a	k8xC	a
odchází	odcházet	k5eAaImIp3nS	odcházet
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
vozač	vozač	k1gMnSc1	vozač
v	v	k7c6	v
dole	dol	k1gInSc6	dol
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
havárii	havárie	k1gFnSc3	havárie
a	a	k8xC	a
v	v	k7c6	v
dole	dol	k1gInSc6	dol
jsou	být	k5eAaImIp3nP	být
uvězněni	uvězněn	k2eAgMnPc1d1	uvězněn
tři	tři	k4xCgMnPc1	tři
horníci	horník	k1gMnPc1	horník
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
otcové	otec	k1gMnPc1	otec
od	od	k7c2	od
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Standa	Standa	k1gMnSc1	Standa
se	se	k3xPyFc4	se
jako	jako	k9	jako
první	první	k4xOgMnPc1	první
přihlásí	přihlásit	k5eAaPmIp3nP	přihlásit
do	do	k7c2	do
záchranné	záchranný	k2eAgFnSc2d1	záchranná
akce	akce	k1gFnSc2	akce
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
statečnost	statečnost	k1gFnSc1	statečnost
vidí	vidět	k5eAaImIp3nP	vidět
další	další	k2eAgMnPc1d1	další
horníci	horník	k1gMnPc1	horník
<g/>
,	,	kIx,	,
přihlásí	přihlásit	k5eAaPmIp3nP	přihlásit
se	se	k3xPyFc4	se
také	také	k9	také
a	a	k9	a
první	první	k4xOgFnSc1	první
parta	parta	k1gFnSc1	parta
se	se	k3xPyFc4	se
pustí	pustit	k5eAaPmIp3nS	pustit
do	do	k7c2	do
zasypané	zasypaný	k2eAgFnSc2d1	zasypaná
šachty	šachta	k1gFnSc2	šachta
<g/>
.	.	kIx.	.
</s>
<s>
Standa	Standa	k1gMnSc1	Standa
nejprve	nejprve	k6eAd1	nejprve
překonává	překonávat	k5eAaImIp3nS	překonávat
strach	strach	k1gInSc4	strach
a	a	k8xC	a
pocity	pocit	k1gInPc4	pocit
stísněnosti	stísněnost	k1gFnSc2	stísněnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postupně	postupně	k6eAd1	postupně
ho	on	k3xPp3gInSc4	on
práce	práce	k1gFnSc1	práce
začne	začít	k5eAaPmIp3nS	začít
bavit	bavit	k5eAaImF	bavit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
ozývat	ozývat	k5eAaImF	ozývat
ťukání	ťukání	k1gNnSc4	ťukání
<g/>
,	,	kIx,	,
signál	signál	k1gInSc4	signál
od	od	k7c2	od
zasypaných	zasypaný	k2eAgMnPc2d1	zasypaný
horníků	horník	k1gMnPc2	horník
<g/>
,	,	kIx,	,
Standa	Standa	k1gFnSc1	Standa
je	být	k5eAaImIp3nS	být
hrdý	hrdý	k2eAgInSc1d1	hrdý
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
v	v	k7c6	v
hospodě	hospodě	k?	hospodě
se	se	k3xPyFc4	se
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
kolektivu	kolektiv	k1gInSc2	kolektiv
stane	stanout	k5eAaPmIp3nS	stanout
opravdová	opravdový	k2eAgFnSc1d1	opravdová
parta	parta	k1gFnSc1	parta
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
den	den	k1gInSc1	den
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
směny	směna	k1gFnSc2	směna
zbortí	zbortit	k5eAaPmIp3nS	zbortit
strop	strop	k1gInSc1	strop
a	a	k8xC	a
zavalí	zavalit	k5eAaPmIp3nS	zavalit
Standovi	Standa	k1gMnSc3	Standa
ruku	ruka	k1gFnSc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
mu	on	k3xPp3gMnSc3	on
musí	muset	k5eAaImIp3nS	muset
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
ruce	ruka	k1gFnSc6	ruka
amputovat	amputovat	k5eAaBmF	amputovat
několik	několik	k4yIc4	několik
článků	článek	k1gInPc2	článek
prstů	prst	k1gInPc2	prst
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Standu	Standa	k1gMnSc4	Standa
propustí	propustit	k5eAaPmIp3nS	propustit
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
se	se	k3xPyFc4	se
podívat	podívat	k5eAaPmF	podívat
do	do	k7c2	do
dolu	dol	k1gInSc2	dol
na	na	k7c4	na
konec	konec	k1gInSc4	konec
směny	směna	k1gFnSc2	směna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
areálu	areál	k1gInSc6	areál
je	být	k5eAaImIp3nS	být
podivné	podivný	k2eAgNnSc1d1	podivné
ticho	ticho	k1gNnSc1	ticho
a	a	k8xC	a
Standa	Standa	k1gFnSc1	Standa
se	se	k3xPyFc4	se
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
probíjení	probíjení	k1gNnSc6	probíjení
poslední	poslední	k2eAgFnSc2d1	poslední
části	část	k1gFnSc2	část
chodby	chodba	k1gFnSc2	chodba
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
sesypal	sesypat	k5eAaPmAgInS	sesypat
strop	strop	k1gInSc1	strop
a	a	k8xC	a
zavalil	zavalit	k5eAaPmAgInS	zavalit
i	i	k9	i
jeho	jeho	k3xOp3gNnSc1	jeho
kolegu	kolega	k1gMnSc4	kolega
Adama	Adam	k1gMnSc4	Adam
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgNnSc2	jenž
Standa	Standa	k1gMnSc1	Standa
bydlel	bydlet	k5eAaImAgMnS	bydlet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vydání	vydání	k1gNnSc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
parta	parta	k1gFnSc1	parta
v	v	k7c6	v
Digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
v	v	k7c6	v
elektronické	elektronický	k2eAgFnSc6d1	elektronická
podobě	podoba	k1gFnSc6	podoba
na	na	k7c6	na
webu	web	k1gInSc6	web
Městské	městský	k2eAgFnSc2d1	městská
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
OKD	OKD	kA	OKD
</s>
</p>
<p>
<s>
Johanesville	Johanesville	k1gFnSc1	Johanesville
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
</s>
</p>
