<s>
Měď	měď	k1gFnSc1	měď
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Cu	Cu	k1gFnSc2	Cu
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Cuprum	Cuprum	k1gInSc1	Cuprum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ušlechtilý	ušlechtilý	k2eAgInSc4d1	ušlechtilý
kovový	kovový	k2eAgInSc4d1	kovový
prvek	prvek	k1gInSc4	prvek
načervenalé	načervenalý	k2eAgFnSc2d1	načervenalá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc4d1	používaný
člověkem	člověk	k1gMnSc7	člověk
již	již	k6eAd1	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
