<s>
Neris	Neris	k1gFnSc1
</s>
<s>
Neris	Neris	k1gFnSc1
řeka	řeka	k1gFnSc1
ve	v	k7c6
VilniusuZákladní	VilniusuZákladný	k2eAgMnPc1d1
informace	informace	k1gFnSc1
Délka	délka	k1gFnSc1
toku	tok	k1gInSc2
</s>
<s>
510	#num#	k4
km	km	kA
Plocha	plocha	k1gFnSc1
povodí	povodit	k5eAaPmIp3nS
</s>
<s>
25100	#num#	k4
km²	km²	k?
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Hydrologické	hydrologický	k2eAgNnSc1d1
pořadí	pořadí	k1gNnSc4
</s>
<s>
12010001	#num#	k4
Pramen	pramen	k1gInSc1
</s>
<s>
severně	severně	k6eAd1
od	od	k7c2
Minsku	Minsk	k1gInSc2
54	#num#	k4
<g/>
°	°	k?
<g/>
41	#num#	k4
<g/>
′	′	k?
<g/>
22,63	22,63	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
27	#num#	k4
<g/>
°	°	k?
<g/>
52	#num#	k4
<g/>
′	′	k?
<g/>
2,96	2,96	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Ústí	ústí	k1gNnSc1
</s>
<s>
Němen	Němen	k1gInSc1
54	#num#	k4
<g/>
°	°	k?
<g/>
54	#num#	k4
<g/>
′	′	k?
<g/>
1,41	1,41	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
23	#num#	k4
<g/>
°	°	k?
<g/>
52	#num#	k4
<g/>
′	′	k?
<g/>
18,96	18,96	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Protéká	protékat	k5eAaImIp3nS
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc1
Bělorusko	Bělorusko	k1gNnSc1
(	(	kIx(
<g/>
Hrodenská	Hrodenský	k2eAgFnSc1d1
<g/>
,	,	kIx,
Minská	minský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Litva	Litva	k1gFnSc1
Litva	Litva	k1gFnSc1
(	(	kIx(
<g/>
Kaunaský	Kaunaský	k2eAgInSc1d1
<g/>
,	,	kIx,
Vilniuský	vilniuský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
)	)	kIx)
Úmoří	úmoří	k1gNnSc1
<g/>
,	,	kIx,
povodí	povodí	k1gNnSc1
</s>
<s>
Atlantský	atlantský	k2eAgInSc1d1
oceán	oceán	k1gInSc1
<g/>
,	,	kIx,
Baltské	baltský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
,	,	kIx,
Němen	Němen	k1gInSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Neris	Neris	k1gFnSc1
(	(	kIx(
<g/>
bělorusky	bělorusky	k6eAd1
В	В	kA
<g/>
,	,	kIx,
polsky	polsky	k6eAd1
Wilia	Wilia	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
v	v	k7c6
Bělorusku	Bělorusko	k1gNnSc6
(	(	kIx(
<g/>
Hrodenská	Hrodenský	k2eAgFnSc1d1
<g/>
,	,	kIx,
Minská	Minský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
Litvě	Litva	k1gFnSc6
(	(	kIx(
<g/>
Kaunaský	Kaunaský	k2eAgInSc4d1
<g/>
,	,	kIx,
Vilniuský	vilniuský	k2eAgInSc4d1
kraj	kraj	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Bělorusku	Bělorusko	k1gNnSc6
se	se	k3xPyFc4
jmenuje	jmenovat	k5eAaImIp3nS,k5eAaBmIp3nS
В	В	k?
<g/>
,	,	kIx,
v	v	k7c6
Litvě	Litva	k1gFnSc6
až	až	k9
do	do	k7c2
soutoku	soutok	k1gInSc2
s	s	k7c7
řekou	řeka	k1gFnSc7
Žeimenou	Žeimený	k2eAgFnSc7d1
Vilija	Vilij	k2eAgNnPc1d1
a	a	k8xC
teprve	teprve	k6eAd1
od	od	k7c2
tohoto	tento	k3xDgInSc2
soutoku	soutok	k1gInSc2
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
Neris	Neris	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
dlouhá	dlouhý	k2eAgFnSc1d1
510	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povodí	povodí	k1gNnSc1
má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
25	#num#	k4
100	#num#	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
toku	tok	k1gInSc2
</s>
<s>
Řeka	řeka	k1gFnSc1
pramení	pramenit	k5eAaImIp3nS
uprostřed	uprostřed	k7c2
bažin	bažina	k1gFnPc2
severně	severně	k6eAd1
od	od	k7c2
Minsku	Minsk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
horním	horní	k2eAgInSc6d1
toku	tok	k1gInSc6
teče	téct	k5eAaImIp3nS
bažinatou	bažinatý	k2eAgFnSc7d1
krajinou	krajina	k1gFnSc7
v	v	k7c6
nevýrazném	výrazný	k2eNgNnSc6d1
údolí	údolí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
středním	střední	k2eAgInSc6d1
a	a	k8xC
dolním	dolní	k2eAgInSc6d1
toku	tok	k1gInSc6
má	mít	k5eAaImIp3nS
údolí	údolí	k1gNnSc4
vysoké	vysoký	k2eAgInPc4d1
svahy	svah	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
korytě	koryto	k1gNnSc6
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
slapy	slap	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řeka	řeka	k1gFnSc1
je	být	k5eAaImIp3nS
pravým	pravý	k2eAgInSc7d1
přítokem	přítok	k1gInSc7
Němenu	Němen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Protéká	protékat	k5eAaImIp3nS
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
Vilniem	Vilnium	k1gNnSc7
a	a	k8xC
na	na	k7c6
soutoku	soutok	k1gInSc6
s	s	k7c7
Němenem	Němen	k1gInSc7
leží	ležet	k5eAaImIp3nS
město	město	k1gNnSc1
Kaunas	Kaunasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Přítoky	přítok	k1gInPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
přítoků	přítok	k1gInPc2
Nerisu	Neris	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
levé	levý	k2eAgNnSc4d1
</s>
<s>
tekoucí	tekoucí	k2eAgMnPc1d1
v	v	k7c6
Bělorusku	Bělorusko	k1gNnSc6
<g/>
:	:	kIx,
Dzvinasa	Dzvinasa	k1gFnSc1
<g/>
,	,	kIx,
Ilija	Ilija	k1gMnSc1
<g/>
,	,	kIx,
Uša	Uša	k1gMnSc1
(	(	kIx(
<g/>
Vilija	Vilija	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Ašmjanka	Ašmjanka	k1gFnSc1
</s>
<s>
tekoucí	tekoucí	k2eAgMnSc1d1
v	v	k7c6
Litvě	Litva	k1gFnSc6
<g/>
:	:	kIx,
Papunžė	Papunžė	k1gFnSc2
<g/>
,	,	kIx,
Vilnia	Vilnium	k1gNnSc2
<g/>
,	,	kIx,
Vokė	Vokė	k1gMnSc1
<g/>
,	,	kIx,
Vosylytė	Vosylytė	k1gMnSc1
<g/>
,	,	kIx,
Bražuolė	Bražuolė	k1gMnSc1
<g/>
,	,	kIx,
Aliosa	Aliosa	k1gFnSc1
<g/>
,	,	kIx,
Žiežmara	Žiežmara	k1gFnSc1
<g/>
,	,	kIx,
Laukysta	Laukysta	k1gFnSc1
<g/>
,	,	kIx,
Lomena	lomen	k2eAgFnSc1d1
<g/>
,	,	kIx,
Varpė	Varpė	k1gFnSc1
<g/>
,	,	kIx,
Šešuva	Šešuva	k1gFnSc1
<g/>
,	,	kIx,
Zversa	Zversa	k1gFnSc1
</s>
<s>
pravé	pravý	k2eAgNnSc1d1
</s>
<s>
tekoucí	tekoucí	k2eAgMnPc1d1
v	v	k7c6
Bělorusku	Bělorusko	k1gNnSc6
<g/>
:	:	kIx,
Servač	Servač	k1gMnSc1
<g/>
,	,	kIx,
Narač	Narač	k1gMnSc1
<g/>
,	,	kIx,
Strača	Strača	k1gMnSc1
</s>
<s>
tekoucí	tekoucí	k2eAgMnSc1d1
v	v	k7c6
Litvě	Litva	k1gFnSc6
<g/>
,	,	kIx,
přítoky	přítok	k1gInPc1
Vilije	Vilije	k1gFnPc2
<g/>
:	:	kIx,
Baluoša	Baluoša	k1gFnSc1
<g/>
,	,	kIx,
Perčina	Perčina	k1gFnSc1
<g/>
,	,	kIx,
Žeimena	Žeimen	k2eAgFnSc1d1
</s>
<s>
tekoucí	tekoucí	k2eAgMnSc1d1
v	v	k7c6
Litvě	Litva	k1gFnSc6
<g/>
,	,	kIx,
přítoky	přítok	k1gInPc1
Nerisu	Neris	k1gInSc2
<g/>
:	:	kIx,
Nemenčia	Nemenčia	k1gFnSc1
<g/>
,	,	kIx,
Žalesa	Žalesa	k1gFnSc1
<g/>
,	,	kIx,
Reišė	Reišė	k1gMnSc4
<g/>
,	,	kIx,
Čekonė	Čekonė	k1gMnSc4
<g/>
,	,	kIx,
Dū	Dū	k1gMnSc4
<g/>
,	,	kIx,
Vingrė	Vingrė	k1gMnSc4
<g/>
,	,	kIx,
Musė	Musė	k1gMnSc4
<g/>
,	,	kIx,
Lietauka	Lietauk	k1gMnSc4
<g/>
,	,	kIx,
Šventoji	Šventoj	k1gInSc3
<g/>
,	,	kIx,
Lokys	Lokys	k1gInSc4
</s>
<s>
Vodní	vodní	k2eAgInSc1d1
režim	režim	k1gInSc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
vody	voda	k1gFnSc2
je	být	k5eAaImIp3nS
smíšený	smíšený	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zamrzá	zamrzat	k5eAaImIp3nS
v	v	k7c6
prosinci	prosinec	k1gInSc6
a	a	k8xC
rozmrzá	rozmrzat	k5eAaImIp3nS
v	v	k7c6
březnu	březen	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Částečně	částečně	k6eAd1
je	být	k5eAaImIp3nS
splavná	splavný	k2eAgFnSc1d1
na	na	k7c6
oddělených	oddělený	k2eAgInPc6d1
úsecích	úsek	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
na	na	k7c6
ní	on	k3xPp3gFnSc6
města	město	k1gNnSc2
Vilnius	Vilnius	k1gInSc1
a	a	k8xC
v	v	k7c6
ústí	ústí	k1gNnSc6
Kaunas	Kaunasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nad	nad	k7c7
městem	město	k1gNnSc7
Vilejka	Vilejka	k1gFnSc1
byla	být	k5eAaImAgFnS
vybudována	vybudovat	k5eAaPmNgFnS
přehradní	přehradní	k2eAgFnSc1d1
nádrž	nádrž	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
zásobování	zásobování	k1gNnSc3
Minsku	Minsk	k1gInSc2
vodou	voda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Lingvistické	lingvistický	k2eAgFnPc1d1
souvislosti	souvislost	k1gFnPc1
</s>
<s>
Předpoládá	Předpoládat	k5eAaPmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
název	název	k1gInSc1
Neris	Neris	k1gFnSc2
je	být	k5eAaImIp3nS
odvozen	odvodit	k5eAaPmNgInS
od	od	k7c2
slova	slovo	k1gNnSc2
"	"	kIx"
<g/>
nerti	nerti	k1gNnSc1
<g/>
"	"	kIx"
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
náhle	náhle	k6eAd1
se	se	k3xPyFc4
potopit	potopit	k5eAaPmF
<g/>
,	,	kIx,
"	"	kIx"
<g/>
zajet	zajet	k5eAaPmF
pod	pod	k7c4
hladinu	hladina	k1gFnSc4
<g/>
"	"	kIx"
<g/>
,	,	kIx,
plavat	plavat	k5eAaImF
(	(	kIx(
<g/>
nebo	nebo	k8xC
též	též	k9
proudit	proudit	k5eAaImF,k5eAaPmF
<g/>
)	)	kIx)
pod	pod	k7c7
hladinou	hladina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovanský	slovanský	k2eAgInSc1d1
název	název	k1gInSc1
Vilija	Vilijum	k1gNnSc2
(	(	kIx(
<g/>
bělorusky	bělorusky	k6eAd1
В	В	k?
a	a	k8xC
podobné	podobný	k2eAgNnSc4d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
odvozeno	odvodit	k5eAaPmNgNnS
od	od	k7c2
staršího	starý	k2eAgInSc2d2
tvaru	tvar	k1gInSc2
Velja	Veljum	k1gNnSc2
<g/>
,	,	kIx,
souvisejícího	související	k2eAgNnSc2d1
s	s	k7c7
přídavným	přídavný	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
starých	starý	k2eAgMnPc2d1
slovanů	slovan	k1gMnPc2
в	в	k?
(	(	kIx(
<g/>
"	"	kIx"
<g/>
veliká	veliký	k2eAgFnSc1d1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existence	existence	k1gFnSc1
dvojího	dvojí	k4xRgInSc2
názvu	název	k1gInSc2
Vijija-Neris	Vijija-Neris	k1gFnSc2
souvisí	souviset	k5eAaImIp3nS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
po	po	k7c6
soutoku	soutok	k1gInSc6
Narače	Narač	k1gInSc2
(	(	kIx(
<g/>
také	také	k9
od	od	k7c2
slova	slovo	k1gNnSc2
"	"	kIx"
<g/>
nerti	nerti	k1gNnSc2
<g/>
"	"	kIx"
–	–	k?
poběloruštěno	poběloruštěn	k2eAgNnSc1d1
<g/>
)	)	kIx)
s	s	k7c7
Vilijí	Vilijí	k1gFnSc7
se	se	k3xPyFc4
předkové	předek	k1gMnPc1
Bělorusů	Bělorus	k1gMnPc2
a	a	k8xC
Litevců	Litevec	k1gMnPc2
nemohli	moct	k5eNaImAgMnP
dohodnout	dohodnout	k5eAaPmF
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
z	z	k7c2
těch	ten	k3xDgInPc2
názvů	název	k1gInPc2
použít	použít	k5eAaPmF
pro	pro	k7c4
horní	horní	k2eAgInSc4d1
tok	tok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Pseudonym	pseudonym	k1gInSc1
litevské	litevský	k2eAgFnSc2d1
básnířky	básnířka	k1gFnSc2
Salomė	Salomė	k1gFnSc3
Bačinskaitė	Bačinskaitė	k1gFnSc3
<g/>
,	,	kIx,
provdané	provdaný	k2eAgFnSc3d1
Bučienė	Bučienė	k1gFnSc3
Salomė	Salomė	k2eAgFnSc1d1
Nė	Nė	k1gFnSc1
a	a	k8xC
název	název	k1gInSc1
řeky	řeka	k1gFnSc2
Neris	Neris	k1gFnSc2
spolu	spolu	k6eAd1
částečně	částečně	k6eAd1
souvisí	souviset	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
mají	mít	k5eAaImIp3nP
odlišný	odlišný	k2eAgInSc4d1
pravopis	pravopis	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Skloňování	skloňování	k1gNnSc1
</s>
<s>
V	v	k7c6
litevštině	litevština	k1gFnSc6
je	být	k5eAaImIp3nS
Neris	Neris	k1gFnSc1
rodu	rod	k1gInSc2
ženského	ženský	k2eAgInSc2d1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
jednotné	jednotný	k2eAgNnSc1d1
<g/>
,	,	kIx,
měkké	měkký	k2eAgNnSc1d1
skloňování	skloňování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
češtině	čeština	k1gFnSc6
je	být	k5eAaImIp3nS
obvyklejší	obvyklý	k2eAgNnSc1d2
skloňování	skloňování	k1gNnSc1
podle	podle	k7c2
vzoru	vzor	k1gInSc2
hrad	hrad	k1gInSc1
(	(	kIx(
<g/>
liší	lišit	k5eAaImIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
rod	rod	k1gInSc1
i	i	k8xC
měkkost	měkkost	k1gFnSc1
<g/>
/	/	kIx~
<g/>
tvrdost	tvrdost	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
Neris	Neris	k1gFnSc6
<g/>
,	,	kIx,
Nerisu	Neris	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgFnP
použity	použit	k2eAgFnPc1d1
informace	informace	k1gFnPc1
z	z	k7c2
Velké	velký	k2eAgFnSc2d1
sovětské	sovětský	k2eAgFnSc2d1
encyklopedie	encyklopedie	k1gFnSc2
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
„	„	k?
<g/>
В	В	k?
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Neris	Neris	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
