<s>
Země	zem	k1gFnSc2	zem
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
devět	devět	k4xCc4	devět
známých	známá	k1gFnPc2	známá
kvazisatelitů	kvazisatelit	k1gInPc2	kvazisatelit
<g/>
:	:	kIx,	:
3753	[number]	k4	3753
Cruithne	Cruithne	k1gFnPc2	Cruithne
2002	[number]	k4	2002
AA29	AA29	k1gFnPc2	AA29
2003	[number]	k4	2003
YN107	YN107	k1gFnPc2	YN107
2004	[number]	k4	2004
GU9	GU9	k1gFnPc2	GU9
(	(	kIx(	(
<g/>
164207	[number]	k4	164207
<g/>
)	)	kIx)	)
2006	[number]	k4	2006
FV35	FV35	k1gFnSc1	FV35
2010	[number]	k4	2010
SO16	SO16	k1gFnSc1	SO16
2013	[number]	k4	2013
LX28	LX28	k1gFnSc1	LX28
2015	[number]	k4	2015
SO2	SO2	k1gFnSc1	SO2
2016	[number]	k4	2016
HO3	HO3	k1gFnSc1	HO3
Tyto	tento	k3xDgInPc4	tento
objekty	objekt	k1gInPc4	objekt
si	se	k3xPyFc3	se
drží	držet	k5eAaImIp3nS	držet
svoji	svůj	k3xOyFgFnSc4	svůj
kvazisatelitní	kvazisatelitní	k2eAgFnSc4d1	kvazisatelitní
dráhu	dráha	k1gFnSc4	dráha
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
desítek	desítka	k1gFnPc2	desítka
až	až	k8xS	až
stovek	stovka	k1gFnPc2	stovka
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
