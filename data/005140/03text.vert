<s>
Kvazisatelit	Kvazisatelit	k5eAaPmF	Kvazisatelit
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Quasi-satellite	Quasiatellit	k1gInSc5	Quasi-satellit
<g/>
,	,	kIx,	,
též	též	k9	též
"	"	kIx"	"
<g/>
Quasi-satelit	Quasiatelit	k1gMnSc6	Quasi-satelit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
téměř-satelit	téměřatelit	k5eAaImF	téměř-satelit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
či	či	k8xC	či
"	"	kIx"	"
<g/>
téměř-družice	téměřružice	k1gFnSc1	téměř-družice
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
objekt	objekt	k1gInSc1	objekt
s	s	k7c7	s
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
dráhovou	dráhový	k2eAgFnSc7d1	dráhová
rezonancí	rezonance	k1gFnSc7	rezonance
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
blízko	blízko	k7c2	blízko
planety	planeta	k1gFnSc2	planeta
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
několika	několik	k4yIc2	několik
oběhů	oběh	k1gInPc2	oběh
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Kvazisatelitu	Kvazisatelita	k1gFnSc4	Kvazisatelita
trvá	trvat	k5eAaImIp3nS	trvat
jedna	jeden	k4xCgFnSc1	jeden
cesta	cesta	k1gFnSc1	cesta
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
planetě	planeta	k1gFnSc3	planeta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
dráha	dráha	k1gFnSc1	dráha
má	mít	k5eAaImIp3nS	mít
rozdílnou	rozdílný	k2eAgFnSc4d1	rozdílná
excentricitu	excentricita	k1gFnSc4	excentricita
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
ukázáno	ukázat	k5eAaPmNgNnS	ukázat
na	na	k7c6	na
obrázku	obrázek	k1gInSc6	obrázek
vpravo	vpravo	k6eAd1	vpravo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
z	z	k7c2	z
planety	planeta	k1gFnSc2	planeta
se	se	k3xPyFc4	se
dráha	dráha	k1gFnSc1	dráha
objektu	objekt	k1gInSc2	objekt
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
podlouhlá	podlouhlý	k2eAgFnSc1d1	podlouhlá
retrográdní	retrográdní	k2eAgFnSc1d1	retrográdní
smyčka	smyčka	k1gFnSc1	smyčka
okolo	okolo	k7c2	okolo
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
"	"	kIx"	"
<g/>
plnohodnotných	plnohodnotný	k2eAgInPc2d1	plnohodnotný
<g/>
"	"	kIx"	"
satelitů	satelit	k1gInPc2	satelit
<g/>
,	,	kIx,	,
dráhy	dráha	k1gFnPc1	dráha
kvazisatelitů	kvazisatelit	k1gInPc2	kvazisatelit
leží	ležet	k5eAaImIp3nP	ležet
mimo	mimo	k7c4	mimo
Hillovu	Hillův	k2eAgFnSc4d1	Hillova
sféru	sféra	k1gFnSc4	sféra
<g/>
,	,	kIx,	,
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
nestabilní	stabilní	k2eNgFnPc1d1	nestabilní
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
času	čas	k1gInSc2	čas
mají	mít	k5eAaImIp3nP	mít
sklon	sklon	k1gInSc4	sklon
vyvinout	vyvinout	k5eAaPmF	vyvinout
se	se	k3xPyFc4	se
do	do	k7c2	do
jiných	jiný	k2eAgInPc2d1	jiný
typů	typ	k1gInPc2	typ
resonantního	resonantní	k2eAgInSc2d1	resonantní
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
už	už	k6eAd1	už
nemusí	muset	k5eNaImIp3nS	muset
zůstat	zůstat	k5eAaPmF	zůstat
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
s	s	k7c7	s
časem	čas	k1gInSc7	čas
vrátit	vrátit	k5eAaPmF	vrátit
na	na	k7c4	na
původní	původní	k2eAgFnSc4d1	původní
či	či	k8xC	či
podobnou	podobný	k2eAgFnSc4d1	podobná
dráhu	dráha	k1gFnSc4	dráha
jako	jako	k8xS	jako
kvazisatelit	kvazisatelit	k5eAaImF	kvazisatelit
atd.	atd.	kA	atd.
Jiné	jiný	k2eAgInPc1d1	jiný
typy	typ	k1gInPc1	typ
dráhy	dráha	k1gFnSc2	dráha
se	s	k7c7	s
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
rezonancí	rezonance	k1gFnPc2	rezonance
s	s	k7c7	s
planetou	planeta	k1gFnSc7	planeta
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
podkovové	podkovový	k2eAgFnPc1d1	podkovový
a	a	k8xC	a
pulčí	pulčí	k2eAgFnPc1d1	pulčí
dráhy	dráha	k1gFnPc1	dráha
kolem	kolem	k7c2	kolem
libračních	librační	k2eAgFnPc2d1	librační
center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
objekty	objekt	k1gInPc1	objekt
na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
drahách	draha	k1gFnPc6	draha
nezůstávají	zůstávat	k5eNaImIp3nP	zůstávat
blízko	blízko	k7c2	blízko
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
délky	délka	k1gFnSc2	délka
planety	planeta	k1gFnSc2	planeta
mnoho	mnoho	k6eAd1	mnoho
orbitů	orbita	k1gMnPc2	orbita
okolo	okolo	k7c2	okolo
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Objekty	objekt	k1gInPc1	objekt
s	s	k7c7	s
podkovovou	podkovový	k2eAgFnSc7d1	podkovový
dráhou	dráha	k1gFnSc7	dráha
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
pro	pro	k7c4	pro
občasnou	občasný	k2eAgFnSc4d1	občasná
změnu	změna	k1gFnSc4	změna
dráhy	dráha	k1gFnSc2	dráha
na	na	k7c4	na
relativně	relativně	k6eAd1	relativně
krátkou	krátký	k2eAgFnSc4d1	krátká
kvazisatelitní	kvazisatelitní	k2eAgFnSc4d1	kvazisatelitní
<g/>
,	,	kIx,	,
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
občas	občas	k6eAd1	občas
zaměňovány	zaměňovat	k5eAaImNgFnP	zaměňovat
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
takového	takový	k3xDgInSc2	takový
objektu	objekt	k1gInSc2	objekt
je	být	k5eAaImIp3nS	být
2002	[number]	k4	2002
AA29	AA29	k1gFnSc2	AA29
Země	zem	k1gFnSc2	zem
</s>
<s>
Země	zem	k1gFnSc2	zem
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
devět	devět	k4xCc4	devět
známých	známá	k1gFnPc2	známá
kvazisatelitů	kvazisatelit	k1gInPc2	kvazisatelit
<g/>
:	:	kIx,	:
3753	[number]	k4	3753
Cruithne	Cruithne	k1gFnPc2	Cruithne
2002	[number]	k4	2002
AA29	AA29	k1gFnPc2	AA29
2003	[number]	k4	2003
YN107	YN107	k1gFnPc2	YN107
2004	[number]	k4	2004
GU9	GU9	k1gFnPc2	GU9
(	(	kIx(	(
<g/>
164207	[number]	k4	164207
<g/>
)	)	kIx)	)
2006	[number]	k4	2006
FV35	FV35	k1gFnSc1	FV35
2010	[number]	k4	2010
SO16	SO16	k1gFnSc1	SO16
2013	[number]	k4	2013
LX28	LX28	k1gFnSc1	LX28
2015	[number]	k4	2015
SO2	SO2	k1gFnSc1	SO2
2016	[number]	k4	2016
HO3	HO3	k1gFnSc1	HO3
Tyto	tento	k3xDgInPc4	tento
objekty	objekt	k1gInPc4	objekt
si	se	k3xPyFc3	se
drží	držet	k5eAaImIp3nS	držet
svoji	svůj	k3xOyFgFnSc4	svůj
kvazisatelitní	kvazisatelitní	k2eAgFnSc4d1	kvazisatelitní
dráhu	dráha	k1gFnSc4	dráha
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
desítek	desítka	k1gFnPc2	desítka
až	až	k8xS	až
stovek	stovka	k1gFnPc2	stovka
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
I	i	k8xC	i
Venuše	Venuše	k1gFnSc1	Venuše
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
kvazisatelit	kvazisatelit	k5eAaImF	kvazisatelit
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
VE	v	k7c4	v
<g/>
68	[number]	k4	68
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
asteroid	asteroid	k1gInSc1	asteroid
kříží	křížit	k5eAaImIp3nS	křížit
i	i	k9	i
dráhy	dráha	k1gFnPc4	dráha
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
Merkuru	Merkur	k1gInSc2	Merkur
<g/>
;	;	kIx,	;
zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
souputníkem	souputník	k1gMnSc7	souputník
Venuše	Venuše	k1gFnSc2	Venuše
jen	jen	k8xS	jen
posledních	poslední	k2eAgNnPc2d1	poslední
7000	[number]	k4	7000
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
osudem	osud	k1gInSc7	osud
je	být	k5eAaImIp3nS	být
opuštění	opuštění	k1gNnSc1	opuštění
své	svůj	k3xOyFgFnSc2	svůj
kvazisatelitní	kvazisatelitní	k2eAgFnSc2d1	kvazisatelitní
dráhy	dráha	k1gFnSc2	dráha
přibližně	přibližně	k6eAd1	přibližně
za	za	k7c4	za
500	[number]	k4	500
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
planety	planeta	k1gFnPc1	planeta
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
simulací	simulace	k1gFnPc2	simulace
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Uran	Uran	k1gInSc1	Uran
a	a	k8xC	a
Neptun	Neptun	k1gInSc1	Neptun
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
kvazisatelity	kvazisatelit	k1gInPc4	kvazisatelit
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
existence	existence	k1gFnSc2	existence
Sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
(	(	kIx(	(
<g/>
cca	cca	kA	cca
4,5	[number]	k4	4,5
mld.	mld.	k?	mld.
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
Jupiteru	Jupiter	k1gInSc2	Jupiter
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
objekt	objekt	k1gInSc4	objekt
kvazisatelitní	kvazisatelitní	k2eAgInSc4d1	kvazisatelitní
dráhu	dráha	k1gFnSc4	dráha
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
kolem	kolem	k7c2	kolem
Saturnu	Saturn	k1gInSc2	Saturn
pouhých	pouhý	k2eAgInPc2d1	pouhý
100	[number]	k4	100
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
jediný	jediný	k2eAgInSc1d1	jediný
kvazisatelit	kvazisatelit	k5eAaPmF	kvazisatelit
kolem	kolem	k7c2	kolem
jiné	jiný	k2eAgFnSc2d1	jiná
planety	planeta	k1gFnSc2	planeta
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Umělé	umělý	k2eAgNnSc1d1	umělé
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
sovětská	sovětský	k2eAgFnSc1d1	sovětská
sonda	sonda	k1gFnSc1	sonda
Fobos	Fobos	k1gInSc1	Fobos
2	[number]	k4	2
stala	stát	k5eAaPmAgFnS	stát
kvazisatelitem	kvazisatelit	k1gInSc7	kvazisatelit
Phobosu	Phobos	k1gInSc2	Phobos
<g/>
,	,	kIx,	,
měsíce	měsíc	k1gInPc4	měsíc
Marsu	Mars	k1gInSc2	Mars
<g/>
,	,	kIx,	,
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
poloosou	poloosa	k1gFnSc7	poloosa
dráhy	dráha	k1gFnSc2	dráha
100	[number]	k4	100
km	km	kA	km
od	od	k7c2	od
Phobosu	Phobos	k1gInSc2	Phobos
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
výpočtům	výpočet	k1gInPc3	výpočet
mohla	moct	k5eAaImAgFnS	moct
sonda	sonda	k1gFnSc1	sonda
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
dráze	dráha	k1gFnSc6	dráha
vydržet	vydržet	k5eAaPmF	vydržet
měsíce	měsíc	k1gInPc4	měsíc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
poruše	porucha	k1gFnSc3	porucha
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
systému	systém	k1gInSc2	systém
bylo	být	k5eAaImAgNnS	být
se	s	k7c7	s
sondou	sonda	k1gFnSc7	sonda
ztraceno	ztracen	k2eAgNnSc4d1	ztraceno
spojení	spojení	k1gNnSc4	spojení
<g/>
.	.	kIx.	.
měsíc	měsíc	k1gInSc4	měsíc
(	(	kIx(	(
<g/>
satelit	satelit	k1gInSc4	satelit
<g/>
)	)	kIx)	)
asteroidní	asteroidní	k2eAgInSc4d1	asteroidní
měsíc	měsíc	k1gInSc4	měsíc
doba	doba	k1gFnSc1	doba
oběhu	oběh	k1gInSc2	oběh
dráhová	dráhový	k2eAgFnSc1d1	dráhová
rezonance	rezonance	k1gFnSc1	rezonance
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Quasi-satellite	Quasiatellit	k1gInSc5	Quasi-satellit
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Quasi-satellite	Quasiatellit	k1gInSc5	Quasi-satellit
Information	Information	k1gInSc1	Information
Page	Page	k1gNnSc6	Page
Earth	Earth	k1gInSc1	Earth
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
New	New	k1gFnSc7	New
Travelling	Travelling	k1gInSc4	Travelling
Companion	Companion	k1gInSc4	Companion
<g/>
:	:	kIx,	:
Quasi-Satellite	Quasi-Satellit	k1gInSc5	Quasi-Satellit
Discovered	Discovered	k1gInSc1	Discovered
Astronomy	astronom	k1gMnPc4	astronom
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
:	:	kIx,	:
A	a	k9	a
new	new	k?	new
"	"	kIx"	"
<g/>
moon	moon	k1gInSc1	moon
<g/>
"	"	kIx"	"
for	forum	k1gNnPc2	forum
Earth	Earth	k1gMnSc1	Earth
Discovery	Discovera	k1gFnSc2	Discovera
of	of	k?	of
the	the	k?	the
first	first	k1gInSc1	first
quasi-satellite	quasiatellit	k1gInSc5	quasi-satellit
of	of	k?	of
Venus	Venus	k1gInSc1	Venus
–	–	k?	–
University	universita	k1gFnSc2	universita
of	of	k?	of
Turku	turek	k1gInSc3	turek
news	ws	k6eNd1	ws
release	release	k6eAd1	release
(	(	kIx(	(
<g/>
August	August	k1gMnSc1	August
17	[number]	k4	17
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
