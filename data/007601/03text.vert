<s>
Jazykový	jazykový	k2eAgInSc1d1	jazykový
korpus	korpus	k1gInSc1	korpus
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
většinou	většina	k1gFnSc7	většina
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
<g/>
)	)	kIx)	)
soubor	soubor	k1gInSc4	soubor
textů	text	k1gInPc2	text
určitého	určitý	k2eAgInSc2d1	určitý
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jednak	jednak	k8xC	jednak
pro	pro	k7c4	pro
lingvistický	lingvistický	k2eAgInSc4d1	lingvistický
výzkum	výzkum	k1gInSc4	výzkum
jazykové	jazykový	k2eAgFnSc2d1	jazyková
praxe	praxe	k1gFnSc2	praxe
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
jako	jako	k9	jako
datová	datový	k2eAgFnSc1d1	datová
základna	základna	k1gFnSc1	základna
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
slovníků	slovník	k1gInPc2	slovník
<g/>
,	,	kIx,	,
korektorů	korektor	k1gInPc2	korektor
<g/>
,	,	kIx,	,
překladačů	překladač	k1gInPc2	překladač
apod.	apod.	kA	apod.
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
mají	mít	k5eAaImIp3nP	mít
korpusy	korpus	k1gInPc1	korpus
digitální	digitální	k2eAgFnSc4d1	digitální
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
výrazně	výrazně	k6eAd1	výrazně
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
sběr	sběr	k1gInSc4	sběr
dat	datum	k1gNnPc2	datum
i	i	k8xC	i
jejich	jejich	k3xOp3gNnSc1	jejich
zpracování	zpracování	k1gNnSc1	zpracování
<g/>
:	:	kIx,	:
speciální	speciální	k2eAgInPc1d1	speciální
programy	program	k1gInPc1	program
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
slovních	slovní	k2eAgNnPc2d1	slovní
spojení	spojení	k1gNnPc2	spojení
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
<g/>
,	,	kIx,	,
zjištění	zjištění	k1gNnSc3	zjištění
frekvence	frekvence	k1gFnSc2	frekvence
výskytu	výskyt	k1gInSc2	výskyt
v	v	k7c6	v
korpusu	korpus	k1gInSc6	korpus
i	i	k9	i
zjištění	zjištění	k1gNnSc1	zjištění
původního	původní	k2eAgInSc2d1	původní
zdroje	zdroj	k1gInSc2	zdroj
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Korpusy	korpus	k1gInPc1	korpus
slouží	sloužit	k5eAaImIp3nP	sloužit
zejména	zejména	k9	zejména
jako	jako	k9	jako
lexikologický	lexikologický	k2eAgInSc4d1	lexikologický
a	a	k8xC	a
lexikografický	lexikografický	k2eAgInSc4d1	lexikografický
nástroj	nástroj	k1gInSc4	nástroj
a	a	k8xC	a
stávají	stávat	k5eAaImIp3nP	stávat
se	se	k3xPyFc4	se
mj.	mj.	kA	mj.
zdrojem	zdroj	k1gInSc7	zdroj
pro	pro	k7c4	pro
zpracování	zpracování	k1gNnSc4	zpracování
jednojazyčných	jednojazyčný	k2eAgInPc2d1	jednojazyčný
výkladových	výkladový	k2eAgInPc2d1	výkladový
slovníků	slovník	k1gInPc2	slovník
a	a	k8xC	a
automatických	automatický	k2eAgInPc2d1	automatický
korektorů	korektor	k1gInPc2	korektor
nebo	nebo	k8xC	nebo
vícejazyčných	vícejazyčný	k2eAgInPc2d1	vícejazyčný
překladových	překladový	k2eAgInPc2d1	překladový
slovníků	slovník	k1gInPc2	slovník
a	a	k8xC	a
automatických	automatický	k2eAgInPc2d1	automatický
překladačů	překladač	k1gInPc2	překladač
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
lingvistů	lingvista	k1gMnPc2	lingvista
korpusy	korpus	k1gInPc1	korpus
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
využívají	využívat	k5eAaPmIp3nP	využívat
i	i	k9	i
redaktoři	redaktor	k1gMnPc1	redaktor
<g/>
,	,	kIx,	,
překladatelé	překladatel	k1gMnPc1	překladatel
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
tvůrci	tvůrce	k1gMnPc1	tvůrce
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
učitelé	učitel	k1gMnPc1	učitel
a	a	k8xC	a
studenti	student	k1gMnPc1	student
cizích	cizí	k2eAgInPc2d1	cizí
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Texty	text	k1gInPc1	text
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
různé	různý	k2eAgFnSc6d1	různá
míře	míra	k1gFnSc6	míra
opatřeny	opatřen	k2eAgInPc1d1	opatřen
metajazykovými	metajazykový	k2eAgFnPc7d1	metajazyková
značkami	značka	k1gFnPc7	značka
vypovídajícími	vypovídající	k2eAgFnPc7d1	vypovídající
o	o	k7c6	o
samotném	samotný	k2eAgInSc6d1	samotný
textu	text	k1gInSc6	text
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
<g/>
,	,	kIx,	,
rok	rok	k1gInSc4	rok
vydání	vydání	k1gNnSc4	vydání
<g/>
,	,	kIx,	,
žánr	žánr	k1gInSc4	žánr
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c6	o
zařazení	zařazení	k1gNnSc6	zařazení
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
slov	slovo	k1gNnPc2	slovo
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
slovních	slovní	k2eAgInPc2d1	slovní
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
o	o	k7c4	o
frekvenci	frekvence	k1gFnSc4	frekvence
slova	slovo	k1gNnSc2	slovo
v	v	k7c6	v
korpusu	korpus	k1gInSc6	korpus
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
dalších	další	k2eAgInPc6d1	další
lingvistických	lingvistický	k2eAgInPc6d1	lingvistický
a	a	k8xC	a
frekvenčních	frekvenční	k2eAgInPc6d1	frekvenční
aspektech	aspekt	k1gInPc6	aspekt
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
formátování	formátování	k1gNnSc4	formátování
textů	text	k1gInPc2	text
a	a	k8xC	a
vkládání	vkládání	k1gNnSc1	vkládání
značek	značka	k1gFnPc2	značka
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
zejména	zejména	k9	zejména
standardizovaného	standardizovaný	k2eAgInSc2d1	standardizovaný
jazyka	jazyk	k1gInSc2	jazyk
XML	XML	kA	XML
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
staršího	starý	k2eAgInSc2d2	starší
SGML	SGML	kA	SGML
<g/>
.	.	kIx.	.
</s>
<s>
Referenční	referenční	k2eAgInSc1d1	referenční
korpus	korpus	k1gInSc1	korpus
je	být	k5eAaImIp3nS	být
stálý	stálý	k2eAgInSc1d1	stálý
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
opakované	opakovaný	k2eAgInPc1d1	opakovaný
dotazy	dotaz	k1gInPc1	dotaz
dávají	dávat	k5eAaImIp3nP	dávat
vždy	vždy	k6eAd1	vždy
stejné	stejný	k2eAgInPc4d1	stejný
výsledky	výsledek	k1gInPc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
nereferenční	referenční	k2eNgInSc4d1	nereferenční
korpus	korpus	k1gInSc4	korpus
je	být	k5eAaImIp3nS	být
průběžně	průběžně	k6eAd1	průběžně
aktualizován	aktualizován	k2eAgInSc1d1	aktualizován
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
jednou	jeden	k4xCgFnSc7	jeden
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
korpusy	korpus	k1gInPc1	korpus
jsou	být	k5eAaImIp3nP	být
budovány	budovat	k5eAaImNgInP	budovat
jako	jako	k9	jako
vyvážené	vyvážený	k2eAgFnPc1d1	vyvážená
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
obsahovat	obsahovat	k5eAaImF	obsahovat
vyvážený	vyvážený	k2eAgInSc4d1	vyvážený
podíl	podíl	k1gInSc4	podíl
textů	text	k1gInPc2	text
tříděných	tříděný	k2eAgInPc2d1	tříděný
podle	podle	k7c2	podle
žánrovosti	žánrovost	k1gFnSc2	žánrovost
<g/>
,	,	kIx,	,
doby	doba	k1gFnSc2	doba
vzniku	vznik	k1gInSc2	vznik
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
dalších	další	k2eAgNnPc2d1	další
hledisek	hledisko	k1gNnPc2	hledisko
(	(	kIx(	(
<g/>
mluvenost	mluvenost	k1gFnSc1	mluvenost
<g/>
,	,	kIx,	,
psanost	psanost	k1gFnSc1	psanost
<g/>
,	,	kIx,	,
regionálnost	regionálnost	k1gFnSc1	regionálnost
<g/>
,	,	kIx,	,
užívanost	užívanost	k1gFnSc1	užívanost
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Synchronní	synchronní	k2eAgInPc4d1	synchronní
korpusy	korpus	k1gInPc4	korpus
jsou	být	k5eAaImIp3nP	být
budované	budovaný	k2eAgInPc4d1	budovaný
jako	jako	k8xS	jako
reprezentativní	reprezentativní	k2eAgInPc4d1	reprezentativní
a	a	k8xC	a
vyvážené	vyvážený	k2eAgInPc4d1	vyvážený
otisky	otisk	k1gInPc4	otisk
jazyka	jazyk	k1gInSc2	jazyk
v	v	k7c6	v
určitém	určitý	k2eAgNnSc6d1	určité
relativně	relativně	k6eAd1	relativně
krátkém	krátký	k2eAgNnSc6d1	krátké
časovém	časový	k2eAgNnSc6d1	časové
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgMnSc2	jenž
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
jazyk	jazyk	k1gInSc4	jazyk
za	za	k7c4	za
neměnný	neměnný	k2eAgInSc4d1	neměnný
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
korpusy	korpus	k1gInPc4	korpus
současného	současný	k2eAgInSc2d1	současný
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Diachronní	diachronní	k2eAgInPc1d1	diachronní
korpusy	korpus	k1gInPc1	korpus
zachycují	zachycovat	k5eAaImIp3nP	zachycovat
jazyk	jazyk	k1gInSc4	jazyk
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
vývojových	vývojový	k2eAgFnPc6d1	vývojová
fázích	fáze	k1gFnPc6	fáze
a	a	k8xC	a
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
tudíž	tudíž	k8xC	tudíž
texty	text	k1gInPc4	text
z	z	k7c2	z
rozsáhlejších	rozsáhlý	k2eAgNnPc2d2	rozsáhlejší
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dalšího	další	k2eAgNnSc2d1	další
kritéria	kritérion	k1gNnSc2	kritérion
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
také	také	k9	také
korpusy	korpus	k1gInPc4	korpus
jednojazyčné	jednojazyčný	k2eAgInPc4d1	jednojazyčný
a	a	k8xC	a
vícejazyčné	vícejazyčný	k2eAgInPc4d1	vícejazyčný
<g/>
.	.	kIx.	.
</s>
<s>
Vícejazyčný	vícejazyčný	k2eAgInSc1d1	vícejazyčný
korpus	korpus	k1gInSc1	korpus
se	se	k3xPyFc4	se
také	také	k9	také
nazývá	nazývat	k5eAaImIp3nS	nazývat
paralelní	paralelní	k2eAgInSc4d1	paralelní
korpus	korpus	k1gInSc4	korpus
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
stejné	stejný	k2eAgInPc4d1	stejný
texty	text	k1gInPc4	text
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
jazycích	jazyk	k1gInPc6	jazyk
zobrazené	zobrazený	k2eAgInPc1d1	zobrazený
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Budováním	budování	k1gNnSc7	budování
korpusů	korpus	k1gInPc2	korpus
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
se	se	k3xPyFc4	se
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
zabývá	zabývat	k5eAaImIp3nS	zabývat
Ústav	ústav	k1gInSc1	ústav
Českého	český	k2eAgInSc2d1	český
národního	národní	k2eAgInSc2d1	národní
korpusu	korpus	k1gInSc2	korpus
(	(	kIx(	(
<g/>
ÚČNK	ÚČNK	kA	ÚČNK
<g/>
)	)	kIx)	)
při	při	k7c6	při
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
UK	UK	kA	UK
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
založil	založit	k5eAaPmAgMnS	založit
lingvista	lingvista	k1gMnSc1	lingvista
František	František	k1gMnSc1	František
Čermák	Čermák	k1gMnSc1	Čermák
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vyhledávat	vyhledávat	k5eAaImF	vyhledávat
v	v	k7c6	v
korpusech	korpus	k1gInPc6	korpus
ÚČNK	ÚČNK	kA	ÚČNK
pomocí	pomocí	k7c2	pomocí
korpusového	korpusový	k2eAgInSc2d1	korpusový
manažeru	manažer	k1gInSc2	manažer
KonText	kontext	k1gInSc1	kontext
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
českých	český	k2eAgInPc2d1	český
korpusů	korpus	k1gInPc2	korpus
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
patří	patřit	k5eAaImIp3nP	patřit
jak	jak	k6eAd1	jak
synchronní	synchronní	k2eAgInPc1d1	synchronní
korpusy	korpus	k1gInPc1	korpus
psané	psaný	k2eAgFnSc2d1	psaná
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
synchronní	synchronní	k2eAgInPc1d1	synchronní
mluvené	mluvený	k2eAgInPc1d1	mluvený
korpusy	korpus	k1gInPc1	korpus
a	a	k8xC	a
diachronní	diachronní	k2eAgInPc1d1	diachronní
korpusy	korpus	k1gInPc1	korpus
<g/>
:	:	kIx,	:
Korpusy	korpus	k1gInPc1	korpus
řady	řada	k1gFnSc2	řada
SYN	syn	k1gMnSc1	syn
–	–	k?	–
všechny	všechen	k3xTgFnPc4	všechen
synchronní	synchronní	k2eAgInPc1d1	synchronní
psané	psaný	k2eAgInPc1d1	psaný
korpusy	korpus	k1gInPc1	korpus
této	tento	k3xDgFnSc2	tento
řady	řada	k1gFnSc2	řada
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
ve	v	k7c4	v
stejnojmenný	stejnojmenný	k2eAgInSc4d1	stejnojmenný
korpus	korpus	k1gInSc4	korpus
SYN	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Dohromady	dohromady	k6eAd1	dohromady
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
přes	přes	k7c4	přes
dva	dva	k4xCgInPc4	dva
tisíce	tisíc	k4xCgInPc4	tisíc
testových	testový	k2eAgNnPc2d1	testové
slov	slovo	k1gNnPc2	slovo
(	(	kIx(	(
<g/>
tokenů	token	k1gInPc2	token
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Korpus	korpus	k1gInSc1	korpus
není	být	k5eNaImIp3nS	být
reprezentativní	reprezentativní	k2eAgFnPc4d1	reprezentativní
–	–	k?	–
sice	sice	k8xC	sice
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
spojeny	spojit	k5eAaPmNgInP	spojit
reprezentativní	reprezentativní	k2eAgInPc1d1	reprezentativní
korpusy	korpus	k1gInPc1	korpus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
sloučení	sloučení	k1gNnSc6	sloučení
převažují	převažovat	k5eAaImIp3nP	převažovat
texty	text	k1gInPc1	text
publicistické	publicistický	k2eAgInPc1d1	publicistický
(	(	kIx(	(
<g/>
SYN	syn	k1gMnSc1	syn
<g/>
2006	[number]	k4	2006
<g/>
PUB	PUB	kA	PUB
<g/>
,	,	kIx,	,
SYN2009PUB	SYN2009PUB	k1gMnSc1	SYN2009PUB
a	a	k8xC	a
SYN	syn	k1gMnSc1	syn
<g/>
2013	[number]	k4	2013
<g/>
PUB	PUB	kA	PUB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Publicistická	publicistický	k2eAgFnSc1d1	publicistická
část	část	k1gFnSc1	část
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
texty	text	k1gInPc4	text
nejznámějších	známý	k2eAgInPc2d3	nejznámější
celostátních	celostátní	k2eAgInPc2d1	celostátní
deníků	deník	k1gInPc2	deník
a	a	k8xC	a
nespecializovaných	specializovaný	k2eNgInPc2d1	nespecializovaný
časopisů	časopis	k1gInPc2	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
korpusy	korpus	k1gInPc1	korpus
(	(	kIx(	(
<g/>
SYN	syn	k1gMnSc1	syn
<g/>
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
SYN	syn	k1gMnSc1	syn
<g/>
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
SYN	syn	k1gMnSc1	syn
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
žánrově	žánrově	k6eAd1	žánrově
vyvážené	vyvážený	k2eAgFnPc1d1	vyvážená
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
tedy	tedy	k9	tedy
jak	jak	k6eAd1	jak
texty	text	k1gInPc1	text
z	z	k7c2	z
publicistického	publicistický	k2eAgNnSc2d1	publicistické
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
z	z	k7c2	z
beletrie	beletrie	k1gFnSc2	beletrie
nebo	nebo	k8xC	nebo
odborné	odborný	k2eAgFnSc2d1	odborná
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Korpus	korpus	k1gInSc1	korpus
soukromé	soukromý	k2eAgFnSc2d1	soukromá
korespondence	korespondence	k1gFnSc2	korespondence
(	(	kIx(	(
<g/>
KSK-dopisy	KSKopis	k1gInPc1	KSK-dopis
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
2000	[number]	k4	2000
elektronických	elektronický	k2eAgInPc2d1	elektronický
přepisů	přepis	k1gInPc2	přepis
ručně	ručně	k6eAd1	ručně
psané	psaný	k2eAgFnSc2d1	psaná
korespondence	korespondence	k1gFnSc2	korespondence
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Pisatelé	pisatel	k1gMnPc1	pisatel
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
částí	část	k1gFnPc2	část
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
dopisech	dopis	k1gInPc6	dopis
dobře	dobře	k6eAd1	dobře
zachovány	zachován	k2eAgInPc1d1	zachován
idiolekty	idiolekt	k1gInPc1	idiolekt
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
shromážděné	shromážděný	k2eAgInPc1d1	shromážděný
texty	text	k1gInPc1	text
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
sociologické	sociologický	k2eAgFnPc4d1	sociologická
charakteristiky	charakteristika	k1gFnPc4	charakteristika
pisatelů	pisatel	k1gMnPc2	pisatel
<g/>
.	.	kIx.	.
</s>
<s>
Korpus	korpus	k1gInSc1	korpus
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
v	v	k7c6	v
Ústavu	ústav	k1gInSc6	ústav
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Zdeňky	Zdeňka	k1gFnSc2	Zdeňka
Hladké	Hladká	k1gFnSc2	Hladká
<g/>
.	.	kIx.	.
</s>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
mluvený	mluvený	k2eAgInSc1d1	mluvený
korpus	korpus	k1gInSc1	korpus
(	(	kIx(	(
<g/>
PMK	PMK	kA	PMK
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc4	první
korpus	korpus	k1gInSc4	korpus
mluvené	mluvený	k2eAgFnSc2d1	mluvená
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
autentickou	autentický	k2eAgFnSc4d1	autentická
mluvenou	mluvený	k2eAgFnSc4d1	mluvená
češtinu	čeština	k1gFnSc4	čeština
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Anonymních	anonymní	k2eAgFnPc2d1	anonymní
304	[number]	k4	304
nahrávek	nahrávka	k1gFnPc2	nahrávka
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1988	[number]	k4	1988
<g/>
–	–	k?	–
<g/>
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Brněnský	brněnský	k2eAgInSc1d1	brněnský
mluvený	mluvený	k2eAgInSc1d1	mluvený
korpus	korpus	k1gInSc1	korpus
(	(	kIx(	(
<g/>
BMK	BMK	kA	BMK
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ČNK	ČNK	kA	ČNK
prvním	první	k4xOgInSc7	první
korpusem	korpus	k1gInSc7	korpus
mluvené	mluvený	k2eAgFnSc2d1	mluvená
češtiny	čeština	k1gFnSc2	čeština
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
autentickou	autentický	k2eAgFnSc4d1	autentická
tematicky	tematicky	k6eAd1	tematicky
nespecializovanou	specializovaný	k2eNgFnSc4d1	nespecializovaná
mluvu	mluva	k1gFnSc4	mluva
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
BMK	BMK	kA	BMK
je	být	k5eAaImIp3nS	být
elektronickým	elektronický	k2eAgInSc7d1	elektronický
přepisem	přepis	k1gInSc7	přepis
250	[number]	k4	250
anonymních	anonymní	k2eAgFnPc2d1	anonymní
magnetofonových	magnetofonový	k2eAgFnPc2d1	magnetofonová
nahrávek	nahrávka	k1gFnPc2	nahrávka
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
596	[number]	k4	596
009	[number]	k4	009
pozic	pozice	k1gFnPc2	pozice
<g/>
)	)	kIx)	)
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1994-1999	[number]	k4	1994-1999
zachycujících	zachycující	k2eAgInPc2d1	zachycující
294	[number]	k4	294
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
ORAL	orat	k5eAaImAgInS	orat
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
spontánní	spontánní	k2eAgFnSc4d1	spontánní
konverzaci	konverzace	k1gFnSc4	konverzace
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
neformálních	formální	k2eNgFnPc6d1	neformální
komunikačních	komunikační	k2eAgFnPc6d1	komunikační
situacích	situace	k1gFnPc6	situace
<g/>
.	.	kIx.	.
</s>
<s>
Korpus	korpus	k1gInSc1	korpus
ORAL2013	ORAL2013	k1gMnPc2	ORAL2013
již	již	k9	již
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
celé	celý	k2eAgNnSc4d1	celé
území	území	k1gNnSc4	území
České	český	k2eAgFnSc2d1	Česká
republikya	republikya	k6eAd1	republikya
přepis	přepis	k1gInSc1	přepis
je	být	k5eAaImIp3nS	být
propojen	propojen	k2eAgInSc1d1	propojen
se	s	k7c7	s
zvukovou	zvukový	k2eAgFnSc7d1	zvuková
stopou	stopa	k1gFnSc7	stopa
<g/>
.	.	kIx.	.
</s>
<s>
SCHOLA2010	SCHOLA2010	k4	SCHOLA2010
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mluvu	mluva	k1gFnSc4	mluva
učitelů	učitel	k1gMnPc2	učitel
i	i	k8xC	i
žáků	žák	k1gMnPc2	žák
zaznamenanou	zaznamenaný	k2eAgFnSc4d1	zaznamenaná
ve	v	k7c6	v
vyučovacích	vyučovací	k2eAgFnPc6d1	vyučovací
hodinách	hodina	k1gFnPc6	hodina
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
kolem	kolem	k7c2	kolem
milionu	milion	k4xCgInSc2	milion
pozic	pozice	k1gFnPc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
sociolingvisticky	sociolingvisticky	k6eAd1	sociolingvisticky
nevyjádřený	vyjádřený	k2eNgInSc1d1	nevyjádřený
korpus	korpus	k1gInSc1	korpus
formální	formální	k2eAgFnSc2d1	formální
i	i	k8xC	i
poloformální	poloformální	k2eAgFnSc2d1	poloformální
mluvené	mluvený	k2eAgFnSc2d1	mluvená
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
interdisciplinárnosti	interdisciplinárnost	k1gFnSc3	interdisciplinárnost
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
využit	využít	k5eAaPmNgInS	využít
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
lingvistické	lingvistický	k2eAgNnSc4d1	lingvistické
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
psychologické	psychologický	k2eAgNnSc4d1	psychologické
<g/>
,	,	kIx,	,
sociologické	sociologický	k2eAgNnSc4d1	sociologické
a	a	k8xC	a
další	další	k2eAgNnSc4d1	další
bádání	bádání	k1gNnSc4	bádání
<g/>
.	.	kIx.	.
</s>
<s>
CzeSL-plain	CzeSLlain	k1gInSc1	CzeSL-plain
(	(	kIx(	(
<g/>
Czech	Czech	k1gInSc1	Czech
as	as	k1gInSc1	as
a	a	k8xC	a
Second	Second	k1gInSc1	Second
Language	language	k1gFnSc2	language
<g/>
)	)	kIx)	)
-	-	kIx~	-
neanotovaný	anotovaný	k2eNgInSc1d1	anotovaný
žákovský	žákovský	k2eAgInSc1d1	žákovský
korpus	korpus	k1gInSc1	korpus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tři	tři	k4xCgInPc4	tři
subkorpusy	subkorpus	k1gInPc4	subkorpus
-	-	kIx~	-
ciz	cizit	k5eAaImRp2nS	cizit
(	(	kIx(	(
<g/>
písemné	písemný	k2eAgFnSc2d1	písemná
práce	práce	k1gFnSc2	práce
nerodilých	rodilý	k2eNgMnPc2d1	nerodilý
mluvčích	mluvčí	k1gMnPc2	mluvčí
různé	různý	k2eAgFnSc2d1	různá
úrovně	úroveň	k1gFnSc2	úroveň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kval	kval	k1gMnSc1	kval
(	(	kIx(	(
<g/>
odborné	odborný	k2eAgFnSc2d1	odborná
práce	práce	k1gFnSc2	práce
nerodilých	rodilý	k2eNgMnPc2d1	nerodilý
mlivčích	mlivčí	k1gMnPc2	mlivčí
<g/>
)	)	kIx)	)
a	a	k8xC	a
rom	rom	k?	rom
(	(	kIx(	(
<g/>
písemné	písemný	k2eAgFnSc2d1	písemná
práce	práce	k1gFnSc2	práce
romských	romský	k2eAgMnPc2d1	romský
žáků	žák	k1gMnPc2	žák
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
sociálním	sociální	k2eAgNnSc7d1	sociální
vyloučením	vyloučení	k1gNnSc7	vyloučení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
DIAKORP	DIAKORP	kA	DIAKORP
je	být	k5eAaImIp3nS	být
diachronní	diachronní	k2eAgInSc4d1	diachronní
korpus	korpus	k1gInSc4	korpus
obsahující	obsahující	k2eAgInPc1d1	obsahující
texty	text	k1gInPc1	text
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
nejmladší	mladý	k2eAgMnPc1d3	nejmladší
pak	pak	k6eAd1	pak
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
velkému	velký	k2eAgNnSc3d1	velké
časovému	časový	k2eAgNnSc3d1	časové
rozpětí	rozpětí	k1gNnSc3	rozpětí
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
korpus	korpus	k1gInSc1	korpus
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
trochu	trochu	k6eAd1	trochu
odlišně	odlišně	k6eAd1	odlišně
od	od	k7c2	od
výše	vysoce	k6eAd2	vysoce
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
<g/>
.	.	kIx.	.
</s>
<s>
Texty	text	k1gInPc1	text
jsou	být	k5eAaImIp3nP	být
transkribovány	transkribován	k2eAgFnPc1d1	transkribována
a	a	k8xC	a
značkování	značkování	k1gNnSc1	značkování
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
i	i	k9	i
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
lingvistických	lingvistický	k2eAgFnPc2d1	lingvistická
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
transkripcí	transkripce	k1gFnSc7	transkripce
zaniknout	zaniknout	k5eAaPmF	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
další	další	k2eAgNnPc1d1	další
univerzitní	univerzitní	k2eAgNnPc1d1	univerzitní
pracoviště	pracoviště	k1gNnPc1	pracoviště
budují	budovat	k5eAaImIp3nP	budovat
vlastní	vlastní	k2eAgInPc1d1	vlastní
korpusy	korpus	k1gInPc1	korpus
<g/>
:	:	kIx,	:
Korpus	korpus	k1gInSc1	korpus
českého	český	k2eAgInSc2d1	český
verše	verš	k1gInSc2	verš
Ústavu	ústav	k1gInSc2	ústav
pro	pro	k7c4	pro
českou	český	k2eAgFnSc4d1	Česká
literaturu	literatura	k1gFnSc4	literatura
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
lemmatizovaný	lemmatizovaný	k2eAgMnSc1d1	lemmatizovaný
<g/>
,	,	kIx,	,
foneticky	foneticky	k6eAd1	foneticky
<g/>
,	,	kIx,	,
morfologicky	morfologicky	k6eAd1	morfologicky
<g/>
,	,	kIx,	,
metricky	metricky	k6eAd1	metricky
a	a	k8xC	a
stroficky	stroficky	k6eAd1	stroficky
anotovaný	anotovaný	k2eAgInSc1d1	anotovaný
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
obsah	obsah	k1gInSc1	obsah
je	být	k5eAaImIp3nS	být
čerpán	čerpat	k5eAaImNgInS	čerpat
z	z	k7c2	z
české	český	k2eAgFnSc2d1	Česká
poezie	poezie	k1gFnSc2	poezie
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
velká	velký	k2eAgFnSc1d1	velká
řada	řada	k1gFnSc1	řada
on-line	onin	k1gInSc5	on-lin
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
s	s	k7c7	s
texty	text	k1gInPc7	text
pracovat	pracovat	k5eAaImF	pracovat
či	či	k8xC	či
si	se	k3xPyFc3	se
procvičovat	procvičovat	k5eAaImF	procvičovat
rozpoznávání	rozpoznávání	k1gNnSc4	rozpoznávání
meter	metro	k1gNnPc2	metro
<g/>
.	.	kIx.	.
czTenTen	czTenTen	k1gInSc1	czTenTen
<g/>
12	[number]	k4	12
–	–	k?	–
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
to	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
korpusů	korpus	k1gInPc2	korpus
řady	řada	k1gFnSc2	řada
TenTen	TenTen	k2eAgMnSc1d1	TenTen
<g/>
,	,	kIx,	,
i	i	k8xC	i
zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
texty	text	k1gInPc1	text
postahovány	postahován	k2eAgInPc1d1	postahován
z	z	k7c2	z
internetu	internet	k1gInSc2	internet
(	(	kIx(	(
<g/>
pomocí	pomocí	k7c2	pomocí
nástroje	nástroj	k1gInSc2	nástroj
Spiderling	Spiderling	k1gInSc1	Spiderling
a	a	k8xC	a
Heritrix	Heritrix	k1gInSc1	Heritrix
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
přes	přes	k7c4	přes
5,4	[number]	k4	5,4
miliard	miliarda	k4xCgFnPc2	miliarda
tokenů	token	k1gInPc2	token
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
dělá	dělat	k5eAaImIp3nS	dělat
největší	veliký	k2eAgInSc1d3	veliký
český	český	k2eAgInSc1d1	český
textový	textový	k2eAgInSc1d1	textový
korpus	korpus	k1gInSc1	korpus
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
korpusem	korpus	k1gInSc7	korpus
CzechParl	CzechParl	k1gInSc1	CzechParl
(	(	kIx(	(
<g/>
Korpus	korpus	k1gInSc1	korpus
stenografických	stenografický	k2eAgInPc2d1	stenografický
záznamů	záznam	k1gInPc2	záznam
českého	český	k2eAgInSc2d1	český
parlamentu	parlament	k1gInSc2	parlament
<g/>
)	)	kIx)	)
a	a	k8xC	a
Desam	Desam	k1gInSc4	Desam
(	(	kIx(	(
<g/>
morfologicky	morfologicky	k6eAd1	morfologicky
označkovaný	označkovaný	k2eAgInSc1d1	označkovaný
korpus	korpus	k1gInSc1	korpus
českých	český	k2eAgInPc2d1	český
textů	text	k1gInPc2	text
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dostupný	dostupný	k2eAgInSc1d1	dostupný
přes	přes	k7c4	přes
korpusový	korpusový	k2eAgInSc4d1	korpusový
manažer	manažer	k1gInSc4	manažer
Sketch	Sketcha	k1gFnPc2	Sketcha
Engine	Engin	k1gInSc5	Engin
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
společnost	společnost	k1gFnSc4	společnost
Lexical	Lexical	k1gFnSc2	Lexical
Computing	Computing	k1gInSc1	Computing
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Centrem	centr	k1gMnSc7	centr
zpracování	zpracování	k1gNnSc2	zpracování
přirozeného	přirozený	k2eAgInSc2d1	přirozený
jazyka	jazyk	k1gInSc2	jazyk
při	při	k7c6	při
Fakultě	fakulta	k1gFnSc6	fakulta
informatiky	informatika	k1gFnSc2	informatika
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
mluvený	mluvený	k2eAgInSc1d1	mluvený
korpus	korpus	k1gInSc1	korpus
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
vzniká	vznikat	k5eAaImIp3nS	vznikat
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
dr	dr	kA	dr
<g/>
.	.	kIx.	.
P.	P.	kA	P.
Pořízky	pořízek	k1gInPc1	pořízek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
základem	základ	k1gInSc7	základ
jsou	být	k5eAaImIp3nP	být
foneticky	foneticky	k6eAd1	foneticky
přepsané	přepsaný	k2eAgFnPc1d1	přepsaná
nahrávky	nahrávka	k1gFnPc1	nahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Brown	Brown	k1gMnSc1	Brown
University	universita	k1gFnSc2	universita
Standard	standard	k1gInSc1	standard
Corpus	corpus	k1gInSc1	corpus
of	of	k?	of
Present-Day	Present-Daa	k1gFnSc2	Present-Daa
American	American	k1gMnSc1	American
English	English	k1gMnSc1	English
neboli	neboli	k8xC	neboli
zkráceně	zkráceně	k6eAd1	zkráceně
Brown	Brown	k1gNnSc1	Brown
Corpus	corpus	k1gNnSc2	corpus
je	být	k5eAaImIp3nS	být
dílem	díl	k1gInSc7	díl
dvou	dva	k4xCgInPc2	dva
autorů	autor	k1gMnPc2	autor
–	–	k?	–
Henryho	Henry	k1gMnSc2	Henry
Kučery	Kučera	k1gMnSc2	Kučera
(	(	kIx(	(
<g/>
původem	původ	k1gInSc7	původ
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Karlově	Karlův	k2eAgFnSc6d1	Karlova
<g/>
)	)	kIx)	)
a	a	k8xC	a
W.	W.	kA	W.
N.	N.	kA	N.
Francise	Francise	k1gFnSc1	Francise
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
působili	působit	k5eAaImAgMnP	působit
na	na	k7c6	na
Brownově	Brownův	k2eAgFnSc6d1	Brownova
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
korpus	korpus	k1gInSc4	korpus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikal	vznikat	k5eAaImAgInS	vznikat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1963	[number]	k4	1963
<g/>
–	–	k?	–
<g/>
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
texty	text	k1gInPc4	text
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
zachytit	zachytit	k5eAaPmF	zachytit
jazyk	jazyk	k1gInSc4	jazyk
v	v	k7c6	v
určitém	určitý	k2eAgNnSc6d1	určité
období	období	k1gNnSc6	období
(	(	kIx(	(
<g/>
trend	trend	k1gInSc1	trend
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
u	u	k7c2	u
tvorby	tvorba	k1gFnSc2	tvorba
korpusů	korpus	k1gInPc2	korpus
volí	volit	k5eAaImIp3nS	volit
i	i	k9	i
dnes	dnes	k6eAd1	dnes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
zkoumání	zkoumání	k1gNnSc2	zkoumání
je	být	k5eAaImIp3nS	být
psaná	psaný	k2eAgFnSc1d1	psaná
americká	americký	k2eAgFnSc1d1	americká
angličtina	angličtina	k1gFnSc1	angličtina
rodilých	rodilý	k2eAgMnPc2d1	rodilý
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
korpusu	korpus	k1gInSc6	korpus
se	se	k3xPyFc4	se
neuchovávají	uchovávat	k5eNaImIp3nP	uchovávat
celé	celý	k2eAgInPc1d1	celý
texty	text	k1gInPc1	text
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
vzorky	vzorek	k1gInPc4	vzorek
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
korpus	korpus	k1gInSc1	korpus
vyvážený	vyvážený	k2eAgInSc1d1	vyvážený
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
využito	využít	k5eAaPmNgNnS	využít
15	[number]	k4	15
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yQgInPc4	který
patří	patřit	k5eAaImIp3nP	patřit
časopisy	časopis	k1gInPc1	časopis
<g/>
,	,	kIx,	,
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
odborná	odborný	k2eAgFnSc1d1	odborná
literatura	literatura	k1gFnSc1	literatura
i	i	k8xC	i
beletrie	beletrie	k1gFnSc1	beletrie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
každého	každý	k3xTgInSc2	každý
textu	text	k1gInSc2	text
je	být	k5eAaImIp3nS	být
vybrán	vybrán	k2eAgInSc1d1	vybrán
vzorek	vzorek	k1gInSc1	vzorek
2	[number]	k4	2
000	[number]	k4	000
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
vzorků	vzorek	k1gInPc2	vzorek
se	se	k3xPyFc4	se
rovnal	rovnat	k5eAaImAgMnS	rovnat
500	[number]	k4	500
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
rozsah	rozsah	k1gInSc1	rozsah
byl	být	k5eAaImAgInS	být
kolem	kolem	k7c2	kolem
jednoho	jeden	k4xCgInSc2	jeden
milionu	milion	k4xCgInSc2	milion
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Korpus	korpus	k1gInSc1	korpus
Brown	Browna	k1gFnPc2	Browna
je	být	k5eAaImIp3nS	být
morfologicky	morfologicky	k6eAd1	morfologicky
označkován	označkován	k2eAgMnSc1d1	označkován
<g/>
,	,	kIx,	,
využito	využít	k5eAaPmNgNnS	využít
je	být	k5eAaImIp3nS	být
80	[number]	k4	80
kategorií	kategorie	k1gFnPc2	kategorie
a	a	k8xC	a
značkovala	značkovat	k5eAaImAgFnS	značkovat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
interpunkce	interpunkce	k1gFnSc2	interpunkce
i	i	k8xC	i
speciální	speciální	k2eAgInPc4d1	speciální
znaky	znak	k1gInPc4	znak
<g/>
.	.	kIx.	.
</s>
<s>
Korpus	korpus	k1gInSc1	korpus
Brown	Browna	k1gFnPc2	Browna
velice	velice	k6eAd1	velice
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
další	další	k2eAgFnPc4d1	další
generace	generace	k1gFnPc4	generace
lingvistů	lingvista	k1gMnPc2	lingvista
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
svým	svůj	k3xOyFgNnSc7	svůj
rozvržením	rozvržení	k1gNnSc7	rozvržení
vzorem	vzor	k1gInSc7	vzor
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgInPc2d1	další
korpusů	korpus	k1gInPc2	korpus
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
The	The	k1gFnSc7	The
Freiburg-LOB	Freiburg-LOB	k1gMnPc2	Freiburg-LOB
corpus	corpus	k1gNnSc2	corpus
of	of	k?	of
American	American	k1gMnSc1	American
English	English	k1gMnSc1	English
(	(	kIx(	(
<g/>
Frown	Frown	k1gMnSc1	Frown
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
obdobou	obdoba	k1gFnSc7	obdoba
korpusu	korpus	k1gInSc2	korpus
Brown	Brown	k1gMnSc1	Brown
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c6	na
Freiburské	freiburský	k2eAgFnSc6d1	Freiburská
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
úplně	úplně	k6eAd1	úplně
stejnou	stejný	k2eAgFnSc4d1	stejná
strukturu	struktura	k1gFnSc4	struktura
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
zachytit	zachytit	k5eAaPmF	zachytit
britskou	britský	k2eAgFnSc4d1	britská
angličtinu	angličtina	k1gFnSc4	angličtina
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
značkování	značkování	k1gNnSc6	značkování
byl	být	k5eAaImAgInS	být
doplněn	doplnit	k5eAaPmNgInS	doplnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
korpusu	korpus	k1gInSc6	korpus
Brown	Browna	k1gFnPc2	Browna
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
několik	několik	k4yIc1	několik
publikací	publikace	k1gFnPc2	publikace
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnPc1d3	nejznámější
z	z	k7c2	z
děl	dělo	k1gNnPc2	dělo
založených	založený	k2eAgNnPc2d1	založené
na	na	k7c6	na
korpusu	korpus	k1gInSc6	korpus
vůbec	vůbec	k9	vůbec
je	být	k5eAaImIp3nS	být
Computional	Computional	k1gMnSc1	Computional
Analysis	Analysis	k1gFnSc2	Analysis
of	of	k?	of
Present-Day	Present-Daa	k1gFnSc2	Present-Daa
American	American	k1gMnSc1	American
English	English	k1gMnSc1	English
od	od	k7c2	od
autorů	autor	k1gMnPc2	autor
korpusu	korpus	k1gInSc2	korpus
(	(	kIx(	(
<g/>
Kučera	Kučera	k1gMnSc1	Kučera
<g/>
,	,	kIx,	,
Francis	Francis	k1gFnSc1	Francis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
statistickou	statistický	k2eAgFnSc4d1	statistická
studii	studie	k1gFnSc4	studie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
lingvistika	lingvistika	k1gFnSc1	lingvistika
<g/>
,	,	kIx,	,
psychologie	psychologie	k1gFnSc1	psychologie
a	a	k8xC	a
statistika	statistika	k1gFnSc1	statistika
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
korpus	korpus	k1gInSc1	korpus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vznikal	vznikat	k5eAaImAgInS	vznikat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1970	[number]	k4	1970
<g/>
–	–	k?	–
<g/>
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
britským	britský	k2eAgInSc7d1	britský
protějškem	protějšek	k1gInSc7	protějšek
ke	k	k7c3	k
korpusu	korpus	k1gInSc3	korpus
Brown	Browna	k1gFnPc2	Browna
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
strukturu	struktura	k1gFnSc4	struktura
(	(	kIx(	(
<g/>
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
500	[number]	k4	500
vzorků	vzorek	k1gInPc2	vzorek
po	po	k7c4	po
2	[number]	k4	2
000	[number]	k4	000
slovech	slovo	k1gNnPc6	slovo
<g/>
,	,	kIx,	,
15	[number]	k4	15
žánrů	žánr	k1gInPc2	žánr
<g/>
)	)	kIx)	)
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
taktéž	taktéž	k?	taktéž
zachytit	zachytit	k5eAaPmF	zachytit
jazyk	jazyk	k1gInSc4	jazyk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
se	se	k3xPyFc4	se
však	však	k9	však
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
britskou	britský	k2eAgFnSc4d1	britská
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
spoluautorů	spoluautor	k1gMnPc2	spoluautor
je	být	k5eAaImIp3nS	být
i	i	k9	i
Geoffrey	Geoffrey	k1gInPc4	Geoffrey
Neil	Neila	k1gFnPc2	Neila
Leech	Leo	k1gMnPc6	Leo
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
korpusu	korpus	k1gInSc3	korpus
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
obdoba	obdoba	k1gFnSc1	obdoba
označená	označený	k2eAgFnSc1d1	označená
jako	jako	k8xC	jako
The	The	k1gFnSc1	The
Freiburg-LOB	Freiburg-LOB	k1gMnPc2	Freiburg-LOB
Corpus	corpus	k1gNnSc2	corpus
of	of	k?	of
British	British	k1gMnSc1	British
English	English	k1gMnSc1	English
(	(	kIx(	(
<g/>
FLOB	FLOB	kA	FLOB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zveřejněn	zveřejněn	k2eAgMnSc1d1	zveřejněn
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
a	a	k8xC	a
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
britskou	britský	k2eAgFnSc4d1	britská
angličtinu	angličtina	k1gFnSc4	angličtina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
British	British	k1gMnSc1	British
National	National	k1gMnSc2	National
Corpus	corpus	k1gNnSc2	corpus
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
BNC	BNC	kA	BNC
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
korpus	korpus	k1gInSc1	korpus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vznikal	vznikat	k5eAaImAgInS	vznikat
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
tří	tři	k4xCgMnPc2	tři
nakladatelů	nakladatel	k1gMnPc2	nakladatel
(	(	kIx(	(
<g/>
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
Longman	Longman	k1gMnSc1	Longman
a	a	k8xC	a
W.	W.	kA	W.
&	&	k?	&
R.	R.	kA	R.
Chambers	Chambers	k1gInSc1	Chambers
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvou	dva	k4xCgFnPc2	dva
univerzit	univerzita	k1gFnPc2	univerzita
(	(	kIx(	(
<g/>
Oxfordské	oxfordský	k2eAgFnSc2d1	Oxfordská
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Lancasteru	Lancaster	k1gInSc6	Lancaster
<g/>
)	)	kIx)	)
a	a	k8xC	a
britské	britský	k2eAgFnPc1d1	britská
národní	národní	k2eAgFnPc1d1	národní
knihovny	knihovna	k1gFnPc1	knihovna
v	v	k7c6	v
letech	let	k1gInPc6	let
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zveřejněn	zveřejněn	k2eAgMnSc1d1	zveřejněn
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
korpus	korpus	k1gInSc4	korpus
se	s	k7c7	s
100	[number]	k4	100
miliony	milion	k4xCgInPc7	milion
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
rozvětvenou	rozvětvený	k2eAgFnSc7d1	rozvětvená
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
korpus	korpus	k1gInSc1	korpus
je	být	k5eAaImIp3nS	být
vyvážený	vyvážený	k2eAgInSc1d1	vyvážený
<g/>
,	,	kIx,	,
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
jak	jak	k6eAd1	jak
časopisy	časopis	k1gInPc4	časopis
<g/>
,	,	kIx,	,
noviny	novina	k1gFnPc4	novina
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
odbornou	odborný	k2eAgFnSc4d1	odborná
literaturu	literatura	k1gFnSc4	literatura
i	i	k8xC	i
beletrii	beletrie	k1gFnSc4	beletrie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
korpusu	korpus	k1gInSc6	korpus
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
vzorky	vzorek	k1gInPc4	vzorek
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
od	od	k7c2	od
jednoho	jeden	k4xCgMnSc2	jeden
autora	autor	k1gMnSc2	autor
maximálně	maximálně	k6eAd1	maximálně
45	[number]	k4	45
tisíc	tisíc	k4xCgInPc2	tisíc
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
britskou	britský	k2eAgFnSc4d1	britská
angličtinu	angličtina	k1gFnSc4	angličtina
(	(	kIx(	(
<g/>
z	z	k7c2	z
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
vznikal	vznikat	k5eAaImAgMnS	vznikat
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
a	a	k8xC	a
kromě	kromě	k7c2	kromě
psaných	psaný	k2eAgInPc2d1	psaný
textů	text	k1gInPc2	text
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
i	i	k9	i
mluvenou	mluvený	k2eAgFnSc4d1	mluvená
angličtinu	angličtina	k1gFnSc4	angličtina
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
9	[number]	k4	9
:	:	kIx,	:
1	[number]	k4	1
(	(	kIx(	(
<g/>
psaná	psaný	k2eAgFnSc1d1	psaná
:	:	kIx,	:
mluvená	mluvený	k2eAgFnSc1d1	mluvená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mluvená	mluvený	k2eAgFnSc1d1	mluvená
angličtina	angličtina	k1gFnSc1	angličtina
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
korpusu	korpus	k1gInSc2	korpus
převedena	převést	k5eAaPmNgFnS	převést
pomocí	pomocí	k7c2	pomocí
ortografické	ortografický	k2eAgFnSc2d1	ortografická
transkripce	transkripce	k1gFnSc2	transkripce
<g/>
.	.	kIx.	.
</s>
<s>
BNC	BNC	kA	BNC
je	být	k5eAaImIp3nS	být
zafixovaný	zafixovaný	k2eAgMnSc1d1	zafixovaný
a	a	k8xC	a
nic	nic	k3yNnSc1	nic
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
nepřidává	přidávat	k5eNaImIp3nS	přidávat
<g/>
;	;	kIx,	;
jediné	jediný	k2eAgNnSc4d1	jediné
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
značkování	značkování	k1gNnSc1	značkování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
BNC	BNC	kA	BNC
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc4	dva
menší	malý	k2eAgInPc4d2	menší
subkorpusy	subkorpus	k1gInPc4	subkorpus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
pro	pro	k7c4	pro
zkoumání	zkoumání	k1gNnSc4	zkoumání
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
BNC	BNC	kA	BNC
Sampler	Sampler	k1gInSc1	Sampler
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jeden	jeden	k4xCgMnSc1	jeden
milion	milion	k4xCgInSc1	milion
mluvené	mluvený	k2eAgMnPc4d1	mluvený
angličtiny	angličtina	k1gFnSc2	angličtina
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
milion	milion	k4xCgInSc4	milion
psaných	psaný	k2eAgInPc2d1	psaný
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
je	být	k5eAaImIp3nS	být
BNC	BNC	kA	BNC
Baby	baba	k1gFnSc2	baba
<g/>
,	,	kIx,	,
do	do	k7c2	do
nějž	jenž	k3xRgInSc2	jenž
jsou	být	k5eAaImIp3nP	být
zahrnuty	zahrnut	k2eAgInPc1d1	zahrnut
čtyři	čtyři	k4xCgInPc1	čtyři
milionové	milionový	k2eAgInPc1d1	milionový
vzorky	vzorek	k1gInPc1	vzorek
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
různých	různý	k2eAgInPc2d1	různý
žánrů	žánr	k1gInPc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
korpusu	korpus	k1gInSc6	korpus
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
několik	několik	k4yIc1	několik
publikací	publikace	k1gFnPc2	publikace
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
článek	článek	k1gInSc4	článek
100	[number]	k4	100
Million	Million	k1gInSc1	Million
Words	Words	k1gInSc4	Words
of	of	k?	of
English	English	k1gInSc1	English
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
napsal	napsat	k5eAaBmAgMnS	napsat
G.	G.	kA	G.
N.	N.	kA	N.
Leech	Leo	k1gMnPc6	Leo
<g/>
,	,	kIx,	,
článek	článek	k1gInSc4	článek
Corpus	corpus	k1gInSc2	corpus
Design	design	k1gInSc1	design
Criteria	Criterium	k1gNnSc2	Criterium
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc7	jehož
autory	autor	k1gMnPc7	autor
jsou	být	k5eAaImIp3nP	být
S.	S.	kA	S.
Atkins	Atkins	k1gInSc1	Atkins
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Clear	Clear	k1gInSc1	Clear
a	a	k8xC	a
N.	N.	kA	N.
Ostler	Ostler	k1gInSc1	Ostler
a	a	k8xC	a
který	který	k3yRgMnSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
strukturu	struktura	k1gFnSc4	struktura
BNC	BNC	kA	BNC
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
kniha	kniha	k1gFnSc1	kniha
Corpus	corpus	k1gNnSc2	corpus
<g/>
:	:	kIx,	:
An	An	k1gFnSc1	An
Introduction	Introduction	k1gInSc1	Introduction
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
napsali	napsat	k5eAaPmAgMnP	napsat
T.	T.	kA	T.
McEnery	McEner	k1gMnPc7	McEner
a	a	k8xC	a
A.	A.	kA	A.
Wilson	Wilson	k1gInSc1	Wilson
<g/>
.	.	kIx.	.
</s>
