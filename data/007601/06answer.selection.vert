<s>
Jazykový	jazykový	k2eAgInSc1d1	jazykový
korpus	korpus	k1gInSc1	korpus
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
většinou	většina	k1gFnSc7	většina
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
<g/>
)	)	kIx)	)
soubor	soubor	k1gInSc4	soubor
textů	text	k1gInPc2	text
určitého	určitý	k2eAgInSc2d1	určitý
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jednak	jednak	k8xC	jednak
pro	pro	k7c4	pro
lingvistický	lingvistický	k2eAgInSc4d1	lingvistický
výzkum	výzkum	k1gInSc4	výzkum
jazykové	jazykový	k2eAgFnSc2d1	jazyková
praxe	praxe	k1gFnSc2	praxe
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
jako	jako	k9	jako
datová	datový	k2eAgFnSc1d1	datová
základna	základna	k1gFnSc1	základna
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
slovníků	slovník	k1gInPc2	slovník
<g/>
,	,	kIx,	,
korektorů	korektor	k1gInPc2	korektor
<g/>
,	,	kIx,	,
překladačů	překladač	k1gInPc2	překladač
apod.	apod.	kA	apod.
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
mají	mít	k5eAaImIp3nP	mít
korpusy	korpus	k1gInPc1	korpus
digitální	digitální	k2eAgFnSc4d1	digitální
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
výrazně	výrazně	k6eAd1	výrazně
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
sběr	sběr	k1gInSc4	sběr
dat	datum	k1gNnPc2	datum
i	i	k8xC	i
jejich	jejich	k3xOp3gNnSc1	jejich
zpracování	zpracování	k1gNnSc1	zpracování
<g/>
:	:	kIx,	:
speciální	speciální	k2eAgInPc1d1	speciální
programy	program	k1gInPc1	program
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
slovních	slovní	k2eAgNnPc2d1	slovní
spojení	spojení	k1gNnPc2	spojení
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
<g/>
,	,	kIx,	,
zjištění	zjištění	k1gNnSc3	zjištění
frekvence	frekvence	k1gFnSc2	frekvence
výskytu	výskyt	k1gInSc2	výskyt
v	v	k7c6	v
korpusu	korpus	k1gInSc6	korpus
i	i	k9	i
zjištění	zjištění	k1gNnSc1	zjištění
původního	původní	k2eAgInSc2d1	původní
zdroje	zdroj	k1gInSc2	zdroj
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
