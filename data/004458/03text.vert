<s>
ŠKODA	škoda	k6eAd1	škoda
AUTO	auto	k1gNnSc1	auto
a.s.	a.s.	k?	a.s.
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgMnSc1d3	veliký
český	český	k2eAgMnSc1d1	český
výrobce	výrobce	k1gMnSc1	výrobce
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc1	sídlo
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Mladé	mladý	k2eAgFnSc6d1	mladá
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
i	i	k9	i
největší	veliký	k2eAgInSc4d3	veliký
výrobní	výrobní	k2eAgInSc4d1	výrobní
závod	závod	k1gInSc4	závod
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ČR	ČR	kA	ČR
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
další	další	k2eAgInPc4d1	další
výrobní	výrobní	k2eAgInPc4d1	výrobní
závody	závod	k1gInPc4	závod
v	v	k7c6	v
Kvasinách	Kvasina	k1gFnPc6	Kvasina
a	a	k8xC	a
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
koncernu	koncern	k1gInSc2	koncern
Volkswagen	volkswagen	k1gInSc1	volkswagen
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
českou	český	k2eAgFnSc7d1	Česká
firmou	firma	k1gFnSc7	firma
podle	podle	k7c2	podle
tržeb	tržba	k1gFnPc2	tržba
<g/>
,	,	kIx,	,
největším	veliký	k2eAgMnSc7d3	veliký
českým	český	k2eAgMnSc7d1	český
exportérem	exportér	k1gMnSc7	exportér
a	a	k8xC	a
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
českých	český	k2eAgMnPc2d1	český
zaměstnavatelů	zaměstnavatel	k1gMnPc2	zaměstnavatel
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
historicky	historicky	k6eAd1	historicky
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
společnost	společnost	k1gFnSc4	společnost
Laurin	Laurin	k1gInSc1	Laurin
&	&	k?	&
Klement	Klement	k1gMnSc1	Klement
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
strojírenského	strojírenský	k2eAgInSc2d1	strojírenský
koncernu	koncern	k1gInSc2	koncern
Akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Škodovy	Škodův	k2eAgInPc1d1	Škodův
závody	závod	k1gInPc1	závod
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
nesla	nést	k5eAaImAgFnS	nést
název	název	k1gInSc4	název
Akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
automobilový	automobilový	k2eAgInSc4d1	automobilový
průmysl	průmysl	k1gInSc4	průmysl
(	(	kIx(	(
<g/>
ASAP	ASAP	kA	ASAP
<g/>
)	)	kIx)	)
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
mateřskou	mateřský	k2eAgFnSc7d1	mateřská
firmou	firma	k1gFnSc7	firma
se	se	k3xPyFc4	se
v	v	k7c6	v
období	období	k1gNnSc6	období
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
nacistického	nacistický	k2eAgInSc2d1	nacistický
gigantu	gigant	k1gInSc2	gigant
Reichswerke	Reichswerk	k1gFnSc2	Reichswerk
AG	AG	kA	AG
für	für	k?	für
Erzbergbau	Erzbergbaus	k1gInSc2	Erzbergbaus
und	und	k?	und
Eisenhütten	Eisenhüttno	k1gNnPc2	Eisenhüttno
"	"	kIx"	"
<g/>
Hermann	Hermann	k1gMnSc1	Hermann
Göring	Göring	k1gInSc1	Göring
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
totální	totální	k2eAgFnSc2d1	totální
reorganizace	reorganizace	k1gFnSc2	reorganizace
a	a	k8xC	a
zestátnění	zestátnění	k1gNnSc4	zestátnění
průmyslu	průmysl	k1gInSc2	průmysl
osamostatnila	osamostatnit	k5eAaPmAgFnS	osamostatnit
pod	pod	k7c7	pod
novým	nový	k2eAgInSc7d1	nový
názvem	název	k1gInSc7	název
Automobilové	automobilový	k2eAgInPc4d1	automobilový
závody	závod	k1gInPc4	závod
<g/>
,	,	kIx,	,
národní	národní	k2eAgInSc4d1	národní
podnik	podnik	k1gInSc4	podnik
(	(	kIx(	(
<g/>
AZNP	AZNP	kA	AZNP
<g/>
)	)	kIx)	)
a	a	k8xC	a
značku	značka	k1gFnSc4	značka
Škoda	škoda	k6eAd1	škoda
používala	používat	k5eAaImAgFnS	používat
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
výrobky	výrobek	k1gInPc4	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
centrálně	centrálně	k6eAd1	centrálně
řízeného	řízený	k2eAgNnSc2d1	řízené
hospodářství	hospodářství	k1gNnSc2	hospodářství
se	se	k3xPyFc4	se
podnik	podnik	k1gInSc1	podnik
vrátil	vrátit	k5eAaPmAgInS	vrátit
k	k	k7c3	k
historickému	historický	k2eAgInSc3d1	historický
názvu	název	k1gInSc3	název
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
jako	jako	k8xC	jako
Automobilový	automobilový	k2eAgInSc1d1	automobilový
koncern	koncern	k1gInSc1	koncern
ŠKODA	škoda	k1gFnSc1	škoda
a.s.	a.s.	k?	a.s.
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
po	po	k7c6	po
privatizaci	privatizace	k1gFnSc6	privatizace
německým	německý	k2eAgInSc7d1	německý
koncernem	koncern	k1gInSc7	koncern
Volkswagen	volkswagen	k1gInSc1	volkswagen
Group	Group	k1gInSc4	Group
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
ŠKODA	škoda	k1gFnSc1	škoda
<g/>
,	,	kIx,	,
automobilová	automobilový	k2eAgFnSc1d1	automobilová
a.s.	a.s.	k?	a.s.
Současný	současný	k2eAgInSc1d1	současný
název	název	k1gInSc1	název
užívá	užívat	k5eAaImIp3nS	užívat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
společnosti	společnost	k1gFnSc2	společnost
Škoda	škoda	k1gFnSc1	škoda
začíná	začínat	k5eAaImIp3nS	začínat
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dva	dva	k4xCgMnPc1	dva
cyklisté	cyklista	k1gMnPc1	cyklista
-	-	kIx~	-
mechanik	mechanik	k1gMnSc1	mechanik
Václav	Václav	k1gMnSc1	Václav
Laurin	Laurin	k1gInSc1	Laurin
a	a	k8xC	a
knihkupec	knihkupec	k1gMnSc1	knihkupec
Václav	Václav	k1gMnSc1	Václav
Klement	Klement	k1gMnSc1	Klement
založili	založit	k5eAaPmAgMnP	založit
malý	malý	k2eAgInSc4d1	malý
podnik	podnik	k1gInSc4	podnik
Laurin	Laurin	k1gInSc1	Laurin
&	&	k?	&
Klement	Klement	k1gMnSc1	Klement
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
jízdních	jízdní	k2eAgNnPc2d1	jízdní
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Podnik	podnik	k1gInSc1	podnik
založili	založit	k5eAaPmAgMnP	založit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
Václavu	Václav	k1gMnSc3	Václav
Klementovi	Klement	k1gMnSc3	Klement
rozbilo	rozbít	k5eAaPmAgNnS	rozbít
nové	nový	k2eAgNnSc1d1	nové
kolo	kolo	k1gNnSc1	kolo
Germania	germanium	k1gNnSc2	germanium
<g/>
,	,	kIx,	,
na	na	k7c4	na
reklamační	reklamační	k2eAgInSc4d1	reklamační
dopis	dopis	k1gInSc4	dopis
psaný	psaný	k2eAgInSc4d1	psaný
česky	česky	k6eAd1	česky
přišla	přijít	k5eAaPmAgFnS	přijít
reakce	reakce	k1gFnSc1	reakce
ústeckého	ústecký	k2eAgMnSc2d1	ústecký
zastupitele	zastupitel	k1gMnSc2	zastupitel
německého	německý	k2eAgMnSc2d1	německý
výrobce	výrobce	k1gMnSc2	výrobce
Seidel	Seidel	k1gMnSc1	Seidel
&	&	k?	&
Naumann	Naumann	k1gMnSc1	Naumann
s	s	k7c7	s
poznámkou	poznámka	k1gFnSc7	poznámka
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
pisatel	pisatel	k1gMnSc1	pisatel
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
něco	něco	k3yInSc4	něco
chce	chtít	k5eAaImIp3nS	chtít
<g/>
,	,	kIx,	,
použije	použít	k5eAaPmIp3nS	použít
příště	příště	k6eAd1	příště
srozumitelný	srozumitelný	k2eAgInSc4d1	srozumitelný
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Nespokojen	spokojen	k2eNgMnSc1d1	nespokojen
s	s	k7c7	s
odpovědí	odpověď	k1gFnSc7	odpověď
se	se	k3xPyFc4	se
Klement	Klement	k1gMnSc1	Klement
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
založit	založit	k5eAaPmF	založit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
firmu	firma	k1gFnSc4	firma
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
otevřel	otevřít	k5eAaPmAgMnS	otevřít
společně	společně	k6eAd1	společně
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
Laurinem	Laurin	k1gInSc7	Laurin
v	v	k7c6	v
Mladé	mladý	k2eAgFnSc6d1	mladá
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
začíná	začínat	k5eAaImIp3nS	začínat
továrna	továrna	k1gFnSc1	továrna
Laurin	Laurin	k1gInSc1	Laurin
&	&	k?	&
Klement	Klement	k1gMnSc1	Klement
vyrábět	vyrábět	k5eAaImF	vyrábět
i	i	k9	i
motocykly	motocykl	k1gInPc1	motocykl
Slavia	Slavia	k1gFnSc1	Slavia
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k8xC	tak
první	první	k4xOgFnSc7	první
motocyklovou	motocyklový	k2eAgFnSc7d1	motocyklová
továrnou	továrna	k1gFnSc7	továrna
v	v	k7c6	v
Rakousko-Uhersku	Rakousko-Uhersko	k1gNnSc6	Rakousko-Uhersko
i	i	k8xC	i
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
vyrábět	vyrábět	k5eAaImF	vyrábět
první	první	k4xOgInSc1	první
model	model	k1gInSc1	model
automobilu	automobil	k1gInSc2	automobil
<g/>
:	:	kIx,	:
Voiturette	Voiturett	k1gInSc5	Voiturett
A	a	k9	a
<g/>
;	;	kIx,	;
okamžitě	okamžitě	k6eAd1	okamžitě
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
prodejním	prodejní	k2eAgInSc7d1	prodejní
trhákem	trhák	k1gInSc7	trhák
<g/>
.	.	kIx.	.
</s>
<s>
Komerční	komerční	k2eAgInSc1d1	komerční
úspěch	úspěch	k1gInSc1	úspěch
působí	působit	k5eAaImIp3nS	působit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
automobilka	automobilka	k1gFnSc1	automobilka
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
akciovou	akciový	k2eAgFnSc4d1	akciová
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
je	být	k5eAaImIp3nS	být
podnik	podnik	k1gInSc1	podnik
součástí	součást	k1gFnPc2	součást
válečné	válečný	k2eAgFnSc2d1	válečná
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
nadále	nadále	k6eAd1	nadále
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
osobních	osobní	k2eAgNnPc2d1	osobní
vozidel	vozidlo	k1gNnPc2	vozidlo
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
i	i	k9	i
nákladní	nákladní	k2eAgNnSc1d1	nákladní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
např.	např.	kA	např.
letecké	letecký	k2eAgInPc4d1	letecký
motory	motor	k1gInPc4	motor
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
podporovat	podporovat	k5eAaImF	podporovat
další	další	k2eAgInSc4d1	další
rozvoj	rozvoj	k1gInSc4	rozvoj
(	(	kIx(	(
<g/>
a	a	k8xC	a
překonat	překonat	k5eAaPmF	překonat
problémy	problém	k1gInPc4	problém
následující	následující	k2eAgInSc4d1	následující
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
požár	požár	k1gInSc4	požár
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
společnost	společnost	k1gFnSc1	společnost
hledá	hledat	k5eAaImIp3nS	hledat
silného	silný	k2eAgMnSc4d1	silný
partnera	partner	k1gMnSc4	partner
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
pak	pak	k6eAd1	pak
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
se	se	k3xPyFc4	se
strojírenským	strojírenský	k2eAgInSc7d1	strojírenský
koncernem	koncern	k1gInSc7	koncern
Škoda	škoda	k1gFnSc1	škoda
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
postupný	postupný	k2eAgInSc4d1	postupný
zánik	zánik	k1gInSc4	zánik
značky	značka	k1gFnSc2	značka
Laurin	Laurin	k1gInSc1	Laurin
&	&	k?	&
Klement	Klement	k1gMnSc1	Klement
<g/>
,	,	kIx,	,
u	u	k7c2	u
nově	nově	k6eAd1	nově
vyvíjených	vyvíjený	k2eAgInPc2d1	vyvíjený
typů	typ	k1gInPc2	typ
se	se	k3xPyFc4	se
už	už	k6eAd1	už
přechází	přecházet	k5eAaImIp3nS	přecházet
na	na	k7c4	na
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
znak	znak	k1gInSc4	znak
Škoda	škoda	k1gFnSc1	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
typů	typ	k1gInPc2	typ
byly	být	k5eAaImAgFnP	být
vyvíjeny	vyvíjet	k5eAaImNgFnP	vyvíjet
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
italskou	italský	k2eAgFnSc7d1	italská
firmou	firma	k1gFnSc7	firma
Pellegati	Pellegat	k1gMnPc1	Pellegat
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
konstruktéra	konstruktér	k1gMnSc2	konstruktér
Oldřicha	Oldřich	k1gMnSc2	Oldřich
Meduny	Meduna	k1gFnSc2	Meduna
však	však	k9	však
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
zpackaný	zpackaný	k2eAgInSc4d1	zpackaný
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
korupční	korupční	k2eAgInSc1d1	korupční
kontrakt	kontrakt	k1gInSc1	kontrakt
plzeňské	plzeňský	k2eAgFnSc2d1	Plzeňská
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Pellegati	Pellegat	k1gMnPc1	Pellegat
dodala	dodat	k5eAaPmAgFnS	dodat
výkresy	výkres	k1gInPc4	výkres
bez	bez	k7c2	bez
tolerancí	tolerance	k1gFnPc2	tolerance
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
určení	určení	k1gNnSc2	určení
materiálů	materiál	k1gInPc2	materiál
atd.	atd.	kA	atd.
–	–	k?	–
šlo	jít	k5eAaImAgNnS	jít
totiž	totiž	k9	totiž
jen	jen	k9	jen
o	o	k7c4	o
obkreslené	obkreslený	k2eAgInPc4d1	obkreslený
díly	díl	k1gInPc4	díl
z	z	k7c2	z
Fiatu	fiat	k1gInSc2	fiat
<g/>
.	.	kIx.	.
</s>
<s>
Automobilka	automobilka	k1gFnSc1	automobilka
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
navíc	navíc	k6eAd1	navíc
byla	být	k5eAaImAgFnS	být
pouhým	pouhý	k2eAgInSc7d1	pouhý
výrobním	výrobní	k2eAgInSc7d1	výrobní
závodem	závod	k1gInSc7	závod
plzeňského	plzeňský	k2eAgInSc2d1	plzeňský
koncernu	koncern	k1gInSc2	koncern
<g/>
,	,	kIx,	,
její	její	k3xOp3gNnSc1	její
ředitelství	ředitelství	k1gNnSc1	ředitelství
i	i	k8xC	i
konstrukce	konstrukce	k1gFnPc1	konstrukce
přesídlily	přesídlit	k5eAaPmAgFnP	přesídlit
do	do	k7c2	do
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Mladoboleslavský	mladoboleslavský	k2eAgInSc1d1	mladoboleslavský
závod	závod	k1gInSc1	závod
díky	díky	k7c3	díky
úvěru	úvěr	k1gInSc3	úvěr
220	[number]	k4	220
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
prošel	projít	k5eAaPmAgInS	projít
výraznou	výrazný	k2eAgFnSc7d1	výrazná
reorganizací	reorganizace	k1gFnSc7	reorganizace
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
velkosériové	velkosériový	k2eAgFnSc2d1	velkosériová
pásové	pásový	k2eAgFnSc2d1	pásová
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
automobilky	automobilka	k1gFnSc2	automobilka
(	(	kIx(	(
<g/>
o	o	k7c6	o
ploše	plocha	k1gFnSc6	plocha
215.700	[number]	k4	215.700
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
mj.	mj.	kA	mj.
nový	nový	k2eAgInSc1d1	nový
čtyřpatrový	čtyřpatrový	k2eAgInSc1d1	čtyřpatrový
objekt	objekt	k1gInSc1	objekt
karosárny	karosárna	k1gFnSc2	karosárna
a	a	k8xC	a
sedlárny	sedlárna	k1gFnSc2	sedlárna
nebo	nebo	k8xC	nebo
nová	nový	k2eAgFnSc1d1	nová
uhelná	uhelný	k2eAgFnSc1d1	uhelná
elektrárna	elektrárna	k1gFnSc1	elektrárna
o	o	k7c6	o
elektrickém	elektrický	k2eAgInSc6d1	elektrický
výkonu	výkon	k1gInSc6	výkon
3,7	[number]	k4	3,7
MW	MW	kA	MW
<g/>
.	.	kIx.	.
</s>
<s>
Zůstala	zůstat	k5eAaPmAgFnS	zůstat
však	však	k9	však
značná	značný	k2eAgFnSc1d1	značná
těžkopádnost	těžkopádnost	k1gFnSc1	těžkopádnost
řízení	řízení	k1gNnSc2	řízení
a	a	k8xC	a
vysoké	vysoký	k2eAgInPc4d1	vysoký
režijní	režijní	k2eAgInPc4d1	režijní
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
management	management	k1gInSc4	management
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
k	k	k7c3	k
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
udělat	udělat	k5eAaPmF	udělat
z	z	k7c2	z
automobilky	automobilka	k1gFnSc2	automobilka
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
organizaci	organizace	k1gFnSc4	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1930	[number]	k4	1930
tak	tak	k9	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
samostatná	samostatný	k2eAgFnSc1d1	samostatná
Akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
automobilový	automobilový	k2eAgInSc4d1	automobilový
průmysl	průmysl	k1gInSc4	průmysl
(	(	kIx(	(
<g/>
ASAP	ASAP	kA	ASAP
<g/>
)	)	kIx)	)
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
o	o	k7c6	o
vlastním	vlastní	k2eAgNnSc6d1	vlastní
jmění	jmění	k1gNnSc6	jmění
20	[number]	k4	20
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
automobilka	automobilka	k1gFnSc1	automobilka
organizačně	organizačně	k6eAd1	organizačně
a	a	k8xC	a
správně	správně	k6eAd1	správně
osamostatnila	osamostatnit	k5eAaPmAgFnS	osamostatnit
vůči	vůči	k7c3	vůči
koncernu	koncern	k1gInSc2	koncern
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
plném	plný	k2eAgNnSc6d1	plné
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
automobilce	automobilka	k1gFnSc6	automobilka
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
pracovalo	pracovat	k5eAaImAgNnS	pracovat
3750	[number]	k4	3750
dělníků	dělník	k1gMnPc2	dělník
a	a	k8xC	a
500	[number]	k4	500
úředníků	úředník	k1gMnPc2	úředník
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k8xC	i
ASAP	ASAP	kA	ASAP
však	však	k9	však
postihla	postihnout	k5eAaPmAgFnS	postihnout
velká	velký	k2eAgFnSc1d1	velká
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
redukcí	redukce	k1gFnSc7	redukce
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
velkým	velký	k2eAgNnSc7d1	velké
propouštěním	propouštění	k1gNnSc7	propouštění
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Snaha	snaha	k1gFnSc1	snaha
čelit	čelit	k5eAaImF	čelit
krizi	krize	k1gFnSc4	krize
pomocí	pomocí	k7c2	pomocí
fúze	fúze	k1gFnSc2	fúze
největších	veliký	k2eAgFnPc2d3	veliký
československých	československý	k2eAgFnPc2d1	Československá
automobilek	automobilka	k1gFnPc2	automobilka
(	(	kIx(	(
<g/>
Praga	Praga	k1gFnSc1	Praga
z	z	k7c2	z
koncernu	koncern	k1gInSc2	koncern
ČKD	ČKD	kA	ČKD
<g/>
,	,	kIx,	,
Tatra	Tatra	k1gFnSc1	Tatra
z	z	k7c2	z
koncernu	koncern	k1gInSc2	koncern
Ringhoffer	Ringhoffero	k1gNnPc2	Ringhoffero
a	a	k8xC	a
ASAP	ASAP	kA	ASAP
z	z	k7c2	z
koncernu	koncern	k1gInSc2	koncern
Škoda	škoda	k1gFnSc1	škoda
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
ukázala	ukázat	k5eAaPmAgFnS	ukázat
jako	jako	k9	jako
velký	velký	k2eAgInSc4d1	velký
manažerský	manažerský	k2eAgInSc4d1	manažerský
omyl	omyl	k1gInSc4	omyl
<g/>
:	:	kIx,	:
nová	nový	k2eAgFnSc1d1	nová
a.s.	a.s.	k?	a.s.
Motor	motor	k1gInSc1	motor
spojující	spojující	k2eAgFnSc4d1	spojující
Pragu	Praga	k1gFnSc4	Praga
a	a	k8xC	a
ASAP	ASAP	kA	ASAP
(	(	kIx(	(
<g/>
Tatra	Tatra	k1gFnSc1	Tatra
z	z	k7c2	z
projektu	projekt	k1gInSc2	projekt
<g />
.	.	kIx.	.
</s>
<s>
vycouvala	vycouvat	k5eAaPmAgFnS	vycouvat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nS	by
musela	muset	k5eAaImAgFnS	muset
výhledově	výhledově	k6eAd1	výhledově
opustit	opustit	k5eAaPmF	opustit
svou	svůj	k3xOyFgFnSc4	svůj
koncepci	koncepce	k1gFnSc4	koncepce
vzduchem	vzduch	k1gInSc7	vzduch
chlazených	chlazený	k2eAgInPc2d1	chlazený
motorů	motor	k1gInPc2	motor
a	a	k8xC	a
páteřových	páteřový	k2eAgInPc2d1	páteřový
rámů	rám	k1gInPc2	rám
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
ustavena	ustavit	k5eAaPmNgFnS	ustavit
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
nepřekonatelné	překonatelný	k2eNgInPc4d1	nepřekonatelný
finanční	finanční	k2eAgInPc4d1	finanční
problémy	problém	k1gInPc4	problém
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Praga	Praga	k1gFnSc1	Praga
vykazovala	vykazovat	k5eAaImAgFnS	vykazovat
dvojnásobné	dvojnásobný	k2eAgInPc4d1	dvojnásobný
prodeje	prodej	k1gInPc4	prodej
oproti	oproti	k7c3	oproti
Škodě	škoda	k1gFnSc3	škoda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zisk	zisk	k1gInSc1	zisk
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
dělit	dělit	k5eAaImF	dělit
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
tiše	tiš	k1gFnSc2	tiš
zanikla	zaniknout	k5eAaPmAgNnP	zaniknout
koncem	koncem	k7c2	koncem
února	únor	k1gInSc2	únor
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Společná	společný	k2eAgFnSc1d1	společná
konstrukce	konstrukce	k1gFnSc1	konstrukce
nového	nový	k2eAgInSc2d1	nový
osobního	osobní	k2eAgInSc2d1	osobní
vozu	vůz	k1gInSc2	vůz
byla	být	k5eAaImAgFnS	být
zastavena	zastavit	k5eAaPmNgFnS	zastavit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
budoucnost	budoucnost	k1gFnSc4	budoucnost
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
společná	společný	k2eAgFnSc1d1	společná
technická	technický	k2eAgFnSc1d1	technická
normalizace	normalizace	k1gFnSc1	normalizace
a	a	k8xC	a
dobré	dobrý	k2eAgInPc1d1	dobrý
vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
vztahy	vztah	k1gInPc1	vztah
mnohých	mnohý	k2eAgMnPc2d1	mnohý
konstruktérů	konstruktér	k1gMnPc2	konstruktér
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
v	v	k7c6	v
pozdějších	pozdní	k2eAgInPc6d2	pozdější
letech	let	k1gInPc6	let
velmi	velmi	k6eAd1	velmi
hodilo	hodit	k5eAaPmAgNnS	hodit
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
ASAP	ASAP	kA	ASAP
klesl	klesnout	k5eAaPmAgMnS	klesnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
na	na	k7c4	na
pouhých	pouhý	k2eAgInPc2d1	pouhý
1550	[number]	k4	1550
(	(	kIx(	(
<g/>
třetinový	třetinový	k2eAgInSc1d1	třetinový
stav	stav	k1gInSc1	stav
oproti	oproti	k7c3	oproti
roku	rok	k1gInSc3	rok
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyrobilo	vyrobit	k5eAaPmAgNnS	vyrobit
se	s	k7c7	s
1607	[number]	k4	1607
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
prodalo	prodat	k5eAaPmAgNnS	prodat
se	se	k3xPyFc4	se
1233	[number]	k4	1233
osobních	osobní	k2eAgInPc2d1	osobní
automobilů	automobil	k1gInPc2	automobil
a	a	k8xC	a
ASAP	ASAP	kA	ASAP
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgNnP	být
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
14	[number]	k4	14
<g/>
%	%	kIx~	%
tržním	tržní	k2eAgInSc7d1	tržní
podílem	podíl	k1gInSc7	podíl
v	v	k7c6	v
tuzemských	tuzemský	k2eAgFnPc6d1	tuzemská
prodejích	prodej	k1gFnPc6	prodej
na	na	k7c6	na
třetím	třetí	k4xOgNnSc6	třetí
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
trvající	trvající	k2eAgFnSc6d1	trvající
krizi	krize	k1gFnSc6	krize
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
představit	představit	k5eAaPmF	představit
vlastní	vlastní	k2eAgInSc1d1	vlastní
lehký	lehký	k2eAgInSc1d1	lehký
<g/>
,	,	kIx,	,
úsporný	úsporný	k2eAgInSc1d1	úsporný
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
levný	levný	k2eAgInSc4d1	levný
automobil	automobil	k1gInSc4	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Šéfkonstruktér	šéfkonstruktér	k1gMnSc1	šéfkonstruktér
osobních	osobní	k2eAgInPc2d1	osobní
vozů	vůz	k1gInPc2	vůz
Ing.	ing.	kA	ing.
Vladimír	Vladimír	k1gMnSc1	Vladimír
Matouš	Matouš	k1gMnSc1	Matouš
prosadil	prosadit	k5eAaPmAgMnS	prosadit
ideu	idea	k1gFnSc4	idea
pevného	pevný	k2eAgInSc2d1	pevný
páteřového	páteřový	k2eAgInSc2d1	páteřový
nosníku	nosník	k1gInSc2	nosník
s	s	k7c7	s
lehkou	lehký	k2eAgFnSc7d1	lehká
karosérií	karosérie	k1gFnSc7	karosérie
(	(	kIx(	(
<g/>
obdoba	obdoba	k1gFnSc1	obdoba
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
staré	starý	k2eAgFnSc2d1	stará
konkurenční	konkurenční	k2eAgFnSc2d1	konkurenční
konstrukce	konstrukce	k1gFnSc2	konstrukce
Tatra	Tatra	k1gFnSc1	Tatra
11	[number]	k4	11
od	od	k7c2	od
Hanse	Hans	k1gMnSc2	Hans
Ledwinky	Ledwinka	k1gFnSc2	Ledwinka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
malá	malý	k2eAgFnSc1d1	malá
Škoda	škoda	k1gFnSc1	škoda
420	[number]	k4	420
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vážila	vážit	k5eAaImAgFnS	vážit
pouhých	pouhý	k2eAgInPc2d1	pouhý
650	[number]	k4	650
kg	kg	kA	kg
(	(	kIx(	(
<g/>
o	o	k7c4	o
350	[number]	k4	350
kg	kg	kA	kg
méně	málo	k6eAd2	málo
než	než	k8xS	než
stejně	stejně	k6eAd1	stejně
velký	velký	k2eAgMnSc1d1	velký
předchůdce	předchůdce	k1gMnSc1	předchůdce
Škoda	Škoda	k1gMnSc1	Škoda
422	[number]	k4	422
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
vozy	vůz	k1gInPc1	vůz
měly	mít	k5eAaImAgInP	mít
oproti	oproti	k7c3	oproti
původním	původní	k2eAgInPc3d1	původní
modelům	model	k1gInPc3	model
lepší	dobrý	k2eAgFnSc2d2	lepší
jízdní	jízdní	k2eAgFnSc2d1	jízdní
vlastnosti	vlastnost	k1gFnSc2	vlastnost
i	i	k9	i
nižší	nízký	k2eAgFnSc4d2	nižší
spotřebu	spotřeba	k1gFnSc4	spotřeba
a	a	k8xC	a
zejména	zejména	k9	zejména
byly	být	k5eAaImAgInP	být
mnohem	mnohem	k6eAd1	mnohem
levnější	levný	k2eAgInPc1d2	levnější
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
i	i	k8xC	i
při	při	k7c6	při
prodeji	prodej	k1gInSc6	prodej
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
Škoda	škoda	k1gFnSc1	škoda
420	[number]	k4	420
tak	tak	k9	tak
znamenala	znamenat	k5eAaImAgFnS	znamenat
pro	pro	k7c4	pro
automobilku	automobilka	k1gFnSc4	automobilka
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
vzpruhu	vzpruha	k1gFnSc4	vzpruha
–	–	k?	–
její	její	k3xOp3gFnSc6	její
koncepce	koncepce	k1gFnSc1	koncepce
se	se	k3xPyFc4	se
osvědčila	osvědčit	k5eAaPmAgFnS	osvědčit
a	a	k8xC	a
veřejnost	veřejnost	k1gFnSc1	veřejnost
i	i	k8xC	i
automobilka	automobilka	k1gFnSc1	automobilka
ji	on	k3xPp3gFnSc4	on
brzy	brzy	k6eAd1	brzy
zaslouženě	zaslouženě	k6eAd1	zaslouženě
přejmenovaly	přejmenovat	k5eAaPmAgFnP	přejmenovat
na	na	k7c4	na
Popular	Popular	k1gInSc4	Popular
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
koncepčně	koncepčně	k6eAd1	koncepčně
obdobně	obdobně	k6eAd1	obdobně
řešená	řešený	k2eAgFnSc1d1	řešená
limuzína	limuzína	k1gFnSc1	limuzína
Škoda	Škoda	k1gMnSc1	Škoda
Superb	Superb	k1gMnSc1	Superb
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mezeru	mezera	k1gFnSc4	mezera
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
vyplnily	vyplnit	k5eAaPmAgInP	vyplnit
typy	typ	k1gInPc1	typ
Škoda	škoda	k1gFnSc1	škoda
Rapid	rapid	k1gInSc1	rapid
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
a	a	k8xC	a
Škoda	Škoda	k1gMnSc1	Škoda
Favorit	favorit	k1gMnSc1	favorit
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
těmto	tento	k3xDgInPc3	tento
čtyřem	čtyři	k4xCgInPc3	čtyři
modelům	model	k1gInPc3	model
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
obsadila	obsadit	k5eAaPmAgFnS	obsadit
Škoda	škoda	k1gFnSc1	škoda
v	v	k7c6	v
prodejích	prodej	k1gFnPc6	prodej
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
se	s	k7c7	s
4990	[number]	k4	4990
vozy	vůz	k1gInPc7	vůz
(	(	kIx(	(
<g/>
39	[number]	k4	39
%	%	kIx~	%
trhu	trh	k1gInSc2	trh
<g/>
)	)	kIx)	)
a	a	k8xC	a
exportovala	exportovat	k5eAaBmAgFnS	exportovat
dalších	další	k2eAgInPc2d1	další
2180	[number]	k4	2180
vozů	vůz	k1gInPc2	vůz
(	(	kIx(	(
<g/>
30	[number]	k4	30
%	%	kIx~	%
produkce	produkce	k1gFnSc2	produkce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Investice	investice	k1gFnPc1	investice
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
automobilce	automobilka	k1gFnSc3	automobilka
začaly	začít	k5eAaPmAgFnP	začít
vracet	vracet	k5eAaImF	vracet
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
cena	cena	k1gFnSc1	cena
nového	nový	k2eAgInSc2d1	nový
vozu	vůz	k1gInSc2	vůz
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
zhruba	zhruba	k6eAd1	zhruba
poloviční	poloviční	k2eAgFnSc1d1	poloviční
oproti	oproti	k7c3	oproti
srovnatelnému	srovnatelný	k2eAgInSc3d1	srovnatelný
typu	typ	k1gInSc3	typ
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
–	–	k?	–
nejlevnější	levný	k2eAgInSc4d3	nejlevnější
dvoumístný	dvoumístný	k2eAgInSc4d1	dvoumístný
Popular	Popular	k1gInSc4	Popular
roadster	roadster	k1gInSc1	roadster
stál	stát	k5eAaImAgInS	stát
17	[number]	k4	17
500	[number]	k4	500
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
čtyřmístný	čtyřmístný	k2eAgMnSc1d1	čtyřmístný
Popular	Popular	k1gMnSc1	Popular
kabriolet	kabriolet	k1gInSc4	kabriolet
18	[number]	k4	18
800	[number]	k4	800
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
do	do	k7c2	do
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
konečně	konečně	k6eAd1	konečně
vrátila	vrátit	k5eAaPmAgFnS	vrátit
zpět	zpět	k6eAd1	zpět
z	z	k7c2	z
Plzně	Plzeň	k1gFnSc2	Plzeň
konstrukce	konstrukce	k1gFnSc2	konstrukce
osobních	osobní	k2eAgInPc2d1	osobní
i	i	k8xC	i
nákladních	nákladní	k2eAgInPc2d1	nákladní
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
do	do	k7c2	do
výroby	výroba	k1gFnSc2	výroba
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
výrobně	výrobně	k6eAd1	výrobně
i	i	k8xC	i
provozně	provozně	k6eAd1	provozně
hospodárný	hospodárný	k2eAgInSc4d1	hospodárný
a	a	k8xC	a
výkonný	výkonný	k2eAgInSc4d1	výkonný
nákladní	nákladní	k2eAgInSc4d1	nákladní
vůz	vůz	k1gInSc4	vůz
Škoda	škoda	k1gFnSc1	škoda
706	[number]	k4	706
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
trolejbusy	trolejbus	k1gInPc1	trolejbus
Škoda	škoda	k1gFnSc1	škoda
1	[number]	k4	1
<g/>
Tr	Tr	k1gFnPc2	Tr
a	a	k8xC	a
Škoda	škoda	k1gFnSc1	škoda
2	[number]	k4	2
<g/>
Tr	Tr	k1gFnPc2	Tr
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
tak	tak	k9	tak
konečně	konečně	k6eAd1	konečně
progresivní	progresivní	k2eAgFnSc1d1	progresivní
a	a	k8xC	a
moderní	moderní	k2eAgFnSc1d1	moderní
automobilka	automobilka	k1gFnSc1	automobilka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
konstruovala	konstruovat	k5eAaImAgFnS	konstruovat
<g/>
,	,	kIx,	,
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
a	a	k8xC	a
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
osobní	osobní	k2eAgInPc4d1	osobní
automobily	automobil	k1gInPc4	automobil
<g/>
,	,	kIx,	,
nákladní	nákladní	k2eAgInPc1d1	nákladní
automobily	automobil	k1gInPc1	automobil
<g/>
,	,	kIx,	,
autobusy	autobus	k1gInPc1	autobus
a	a	k8xC	a
trolejbusy	trolejbus	k1gInPc1	trolejbus
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
továrna	továrna	k1gFnSc1	továrna
součástí	součást	k1gFnPc2	součást
koncernu	koncern	k1gInSc2	koncern
Reichswerke	Reichswerke	k1gFnSc1	Reichswerke
Hermann	Hermann	k1gMnSc1	Hermann
Göring	Göring	k1gInSc1	Göring
a	a	k8xC	a
sloužila	sloužit	k5eAaImAgFnS	sloužit
německému	německý	k2eAgNnSc3d1	německé
válečnému	válečný	k2eAgNnSc3d1	válečné
úsilí	úsilí	k1gNnSc3	úsilí
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
konstrukční	konstrukční	k2eAgFnSc7d1	konstrukční
kanceláří	kancelář	k1gFnSc7	kancelář
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Porscheho	Porsche	k1gMnSc2	Porsche
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnPc1d1	původní
konstrukce	konstrukce	k1gFnPc1	konstrukce
byly	být	k5eAaImAgFnP	být
upozaděny	upozadit	k5eAaPmNgFnP	upozadit
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
výroby	výroba	k1gFnSc2	výroba
pro	pro	k7c4	pro
Wehrmacht	wehrmacht	k1gInSc4	wehrmacht
–	–	k?	–
vozidla	vozidlo	k1gNnSc2	vozidlo
KdF	KdF	k1gFnPc2	KdF
<g/>
,	,	kIx,	,
obří	obří	k2eAgInSc1d1	obří
tahač	tahač	k1gInSc1	tahač
RSO	RSO	kA	RSO
<g/>
,	,	kIx,	,
součásti	součást	k1gFnPc1	součást
a	a	k8xC	a
celky	celek	k1gInPc1	celek
pro	pro	k7c4	pro
terénní	terénní	k2eAgNnPc4d1	terénní
vozidla	vozidlo	k1gNnPc4	vozidlo
<g/>
,	,	kIx,	,
vojenská	vojenský	k2eAgNnPc4d1	vojenské
letadla	letadlo	k1gNnPc4	letadlo
<g/>
,	,	kIx,	,
zbraně	zbraň	k1gFnPc4	zbraň
i	i	k8xC	i
nábojnice	nábojnice	k1gFnPc4	nábojnice
<g/>
.	.	kIx.	.
</s>
<s>
Vzdušné	vzdušný	k2eAgFnPc1d1	vzdušná
síly	síla	k1gFnPc1	síla
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
USA	USA	kA	USA
bombardovaly	bombardovat	k5eAaImAgFnP	bombardovat
automobilku	automobilka	k1gFnSc4	automobilka
opakovaně	opakovaně	k6eAd1	opakovaně
<g/>
.	.	kIx.	.
</s>
<s>
Masivní	masivní	k2eAgInSc1d1	masivní
nálet	nálet	k1gInSc1	nálet
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
a	a	k8xC	a
vyústil	vyústit	k5eAaPmAgInS	vyústit
v	v	k7c4	v
téměř	téměř	k6eAd1	téměř
úplné	úplný	k2eAgNnSc4d1	úplné
zničení	zničení	k1gNnSc4	zničení
automobilky	automobilka	k1gFnSc2	automobilka
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
1000	[number]	k4	1000
mrtvých	mrtvý	k1gMnPc2	mrtvý
a	a	k8xC	a
zraněných	zraněný	k1gMnPc2	zraněný
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1945	[number]	k4	1945
provedla	provést	k5eAaPmAgFnS	provést
také	také	k9	také
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
nálet	nálet	k1gInSc4	nálet
na	na	k7c4	na
Mladou	mladá	k1gFnSc4	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
automobilce	automobilka	k1gFnSc6	automobilka
zničena	zničit	k5eAaPmNgFnS	zničit
většina	většina	k1gFnSc1	většina
zbylých	zbylý	k2eAgFnPc2d1	zbylá
výrobních	výrobní	k2eAgFnPc2d1	výrobní
prostor	prostora	k1gFnPc2	prostora
<g/>
,	,	kIx,	,
technologií	technologie	k1gFnPc2	technologie
i	i	k8xC	i
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
nebylo	být	k5eNaImAgNnS	být
myslitelné	myslitelný	k2eAgNnSc1d1	myslitelné
obnovení	obnovení	k1gNnSc1	obnovení
původního	původní	k2eAgInSc2d1	původní
výrobního	výrobní	k2eAgInSc2d1	výrobní
programu	program	k1gInSc2	program
v	v	k7c6	v
celé	celá	k1gFnSc6	celá
jeho	jeho	k3xOp3gFnSc6	jeho
šíři	šíř	k1gFnSc6	šíř
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
když	když	k8xS	když
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zestátnění	zestátnění	k1gNnSc3	zestátnění
velkých	velký	k2eAgInPc2d1	velký
podniků	podnik	k1gInPc2	podnik
a	a	k8xC	a
následné	následný	k2eAgFnSc6d1	následná
reorganizaci	reorganizace	k1gFnSc6	reorganizace
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
byl	být	k5eAaImAgInS	být
původní	původní	k2eAgInSc1d1	původní
koncern	koncern	k1gInSc1	koncern
Škoda	škoda	k6eAd1	škoda
direktivně	direktivně	k6eAd1	direktivně
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
a	a	k8xC	a
dosavadní	dosavadní	k2eAgNnSc1d1	dosavadní
účelné	účelný	k2eAgNnSc1d1	účelné
vertikální	vertikální	k2eAgNnSc1d1	vertikální
propojení	propojení	k1gNnSc1	propojení
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
závodů	závod	k1gInPc2	závod
bylo	být	k5eAaImAgNnS	být
nuceně	nuceně	k6eAd1	nuceně
převedeno	převést	k5eAaPmNgNnS	převést
na	na	k7c4	na
horizontální	horizontální	k2eAgNnSc4d1	horizontální
propojení	propojení	k1gNnSc4	propojení
podniků	podnik	k1gInPc2	podnik
s	s	k7c7	s
příbuzným	příbuzný	k2eAgInSc7d1	příbuzný
oborem	obor	k1gInSc7	obor
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Bývalá	bývalý	k2eAgFnSc1d1	bývalá
ASAP	ASAP	kA	ASAP
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
změnila	změnit	k5eAaPmAgFnS	změnit
na	na	k7c4	na
hlavní	hlavní	k2eAgNnSc4d1	hlavní
ředitelství	ředitelství	k1gNnSc4	ředitelství
národního	národní	k2eAgInSc2d1	národní
podniku	podnik	k1gInSc2	podnik
Automobilové	automobilový	k2eAgInPc1d1	automobilový
závody	závod	k1gInPc1	závod
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
za	za	k7c4	za
výrobu	výroba	k1gFnSc4	výroba
osobních	osobní	k2eAgInPc2d1	osobní
automobilů	automobil	k1gInPc2	automobil
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
účelové	účelový	k2eAgFnSc6d1	účelová
reorganizaci	reorganizace	k1gFnSc6	reorganizace
a	a	k8xC	a
delimitaci	delimitace	k1gFnSc6	delimitace
výrobního	výrobní	k2eAgInSc2d1	výrobní
programu	program	k1gInSc2	program
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
Mladé	mladý	k2eAgFnSc6d1	mladá
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
mohli	moct	k5eAaImAgMnP	moct
soustředit	soustředit	k5eAaPmF	soustředit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
malého	malý	k2eAgInSc2d1	malý
lidového	lidový	k2eAgInSc2d1	lidový
automobilu	automobil	k1gInSc2	automobil
Škoda	škoda	k1gFnSc1	škoda
1101	[number]	k4	1101
<g/>
/	/	kIx~	/
<g/>
1102	[number]	k4	1102
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
Tudoru	tudor	k1gInSc6	tudor
<g/>
"	"	kIx"	"
=	=	kIx~	=
de	de	k?	de
facto	facto	k1gNnSc1	facto
modernizovaného	modernizovaný	k2eAgInSc2d1	modernizovaný
Popularu	Popular	k1gInSc2	Popular
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnPc1d1	ostatní
typové	typový	k2eAgFnPc1d1	typová
řady	řada	k1gFnPc1	řada
byly	být	k5eAaImAgFnP	být
opuštěny	opuštěn	k2eAgInPc1d1	opuštěn
po	po	k7c6	po
smontování	smontování	k1gNnSc6	smontování
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
kusů	kus	k1gInPc2	kus
převážně	převážně	k6eAd1	převážně
ze	z	k7c2	z
zbylých	zbylý	k2eAgInPc2d1	zbylý
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
nákladních	nákladní	k2eAgInPc2d1	nákladní
automobilů	automobil	k1gInPc2	automobil
Škoda	škoda	k6eAd1	škoda
706	[number]	k4	706
R	R	kA	R
byla	být	k5eAaImAgFnS	být
převedena	převést	k5eAaPmNgFnS	převést
do	do	k7c2	do
továrny	továrna	k1gFnSc2	továrna
Avia	Avia	k1gFnSc1	Avia
Letňany	Letňan	k1gMnPc4	Letňan
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
oslabenou	oslabený	k2eAgFnSc4d1	oslabená
leteckou	letecký	k2eAgFnSc4d1	letecká
výrobu	výroba	k1gFnSc4	výroba
a	a	k8xC	a
proto	proto	k8xC	proto
převzala	převzít	k5eAaPmAgFnS	převzít
i	i	k9	i
vývoj	vývoj	k1gInSc4	vývoj
dalších	další	k2eAgInPc2d1	další
autobusů	autobus	k1gInPc2	autobus
se	s	k7c7	s
značkou	značka	k1gFnSc7	značka
Škoda	škoda	k1gFnSc1	škoda
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
byly	být	k5eAaImAgFnP	být
všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
aktivity	aktivita	k1gFnPc1	aktivita
převedeny	převést	k5eAaPmNgFnP	převést
na	na	k7c4	na
budoucí	budoucí	k2eAgInSc4d1	budoucí
podnik	podnik	k1gInSc4	podnik
LIAZ	liaz	k1gInSc1	liaz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
však	však	k9	však
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
měsících	měsíc	k1gInPc6	měsíc
fungoval	fungovat	k5eAaImAgInS	fungovat
jen	jen	k6eAd1	jen
jako	jako	k8xC	jako
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
závody	závod	k1gInPc1	závod
AZNP	AZNP	kA	AZNP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgInSc1d1	malý
nákladní	nákladní	k2eAgInSc1d1	nákladní
automobil	automobil	k1gInSc1	automobil
Škoda	škoda	k1gFnSc1	škoda
150	[number]	k4	150
byl	být	k5eAaImAgInS	být
převeden	převést	k5eAaPmNgInS	převést
do	do	k7c2	do
továrny	továrna	k1gFnSc2	továrna
Aero	aero	k1gNnSc1	aero
Vysočany	Vysočany	k1gInPc1	Vysočany
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
přidělené	přidělený	k2eAgFnSc6d1	přidělená
automobilce	automobilka	k1gFnSc6	automobilka
Praga	Praga	k1gFnSc1	Praga
náhradou	náhrada	k1gFnSc7	náhrada
za	za	k7c4	za
vybombardovaný	vybombardovaný	k2eAgInSc4d1	vybombardovaný
závod	závod	k1gInSc4	závod
v	v	k7c6	v
Libni	Libeň	k1gFnSc6	Libeň
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
automobil	automobil	k1gInSc1	automobil
byl	být	k5eAaImAgInS	být
označován	označovat	k5eAaImNgInS	označovat
i	i	k9	i
jako	jako	k8xC	jako
Aero	aero	k1gNnSc1	aero
150	[number]	k4	150
nebo	nebo	k8xC	nebo
Praga	Praga	k1gFnSc1	Praga
150	[number]	k4	150
<g/>
)	)	kIx)	)
a	a	k8xC	a
výrobu	výroba	k1gFnSc4	výroba
trolejbusů	trolejbus	k1gInPc2	trolejbus
začal	začít	k5eAaPmAgInS	začít
zabezpečovat	zabezpečovat	k5eAaImF	zabezpečovat
pouze	pouze	k6eAd1	pouze
plzeňský	plzeňský	k2eAgInSc4d1	plzeňský
podnik	podnik	k1gInSc4	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výrobním	výrobní	k2eAgInSc6d1	výrobní
programu	program	k1gInSc6	program
AZNP	AZNP	kA	AZNP
pak	pak	k6eAd1	pak
zůstal	zůstat	k5eAaPmAgInS	zůstat
jediný	jediný	k2eAgInSc1d1	jediný
typ	typ	k1gInSc1	typ
užitkového	užitkový	k2eAgInSc2d1	užitkový
vozu	vůz	k1gInSc2	vůz
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
Škoda	škoda	k1gFnSc1	škoda
256	[number]	k4	256
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
AZNP	AZNP	kA	AZNP
začaly	začít	k5eAaPmAgFnP	začít
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
i	i	k9	i
výrobu	výroba	k1gFnSc4	výroba
v	v	k7c6	v
dříve	dříve	k6eAd2	dříve
nezávislých	závislý	k2eNgFnPc6d1	nezávislá
karosárnách	karosárna	k1gFnPc6	karosárna
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Petera	Peter	k1gMnSc2	Peter
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
a	a	k8xC	a
JAWA	jawa	k1gFnSc1	jawa
Kvasiny	Kvasina	k1gFnSc2	Kvasina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zabezpečovaly	zabezpečovat	k5eAaImAgFnP	zabezpečovat
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
výrobu	výroba	k1gFnSc4	výroba
některých	některý	k3yIgFnPc2	některý
modifikací	modifikace	k1gFnPc2	modifikace
vozů	vůz	k1gInPc2	vůz
Škoda	škoda	k6eAd1	škoda
1101	[number]	k4	1101
atd.	atd.	kA	atd.
Protože	protože	k8xS	protože
malý	malý	k2eAgInSc1d1	malý
automobil	automobil	k1gInSc1	automobil
nepostačoval	postačovat	k5eNaImAgInS	postačovat
potřebám	potřeba	k1gFnPc3	potřeba
zákazníků	zákazník	k1gMnPc2	zákazník
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
záhy	záhy	k6eAd1	záhy
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
silnější	silný	k2eAgMnSc1d2	silnější
a	a	k8xC	a
prostornější	prostorný	k2eAgInSc1d2	prostornější
automobil	automobil	k1gInSc1	automobil
Škoda	škoda	k1gFnSc1	škoda
1200	[number]	k4	1200
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
Sedan	sedan	k1gInSc1	sedan
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sotva	sotva	k6eAd1	sotva
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
začalo	začít	k5eAaPmAgNnS	začít
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
výrobou	výroba	k1gFnSc7	výroba
<g/>
,	,	kIx,	,
dostaly	dostat	k5eAaPmAgFnP	dostat
AZNP	AZNP	kA	AZNP
příkaz	příkaz	k1gInSc4	příkaz
tento	tento	k3xDgInSc4	tento
typ	typ	k1gInSc4	typ
zastavit	zastavit	k5eAaPmF	zastavit
kvůli	kvůli	k7c3	kvůli
přípravě	příprava	k1gFnSc3	příprava
na	na	k7c4	na
možný	možný	k2eAgInSc4d1	možný
válečný	válečný	k2eAgInSc4d1	válečný
konflikt	konflikt	k1gInSc4	konflikt
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
vývoj	vývoj	k1gInSc1	vývoj
vojenských	vojenský	k2eAgInPc2d1	vojenský
automobilů	automobil	k1gInPc2	automobil
vlastní	vlastní	k2eAgFnSc2d1	vlastní
konstrukce	konstrukce	k1gFnSc2	konstrukce
Škoda	škoda	k1gFnSc1	škoda
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejúspěšnější	úspěšný	k2eAgFnSc1d3	nejúspěšnější
byla	být	k5eAaImAgFnS	být
typová	typový	k2eAgFnSc1d1	typová
řada	řada	k1gFnSc1	řada
Škoda	škoda	k1gFnSc1	škoda
973	[number]	k4	973
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Babeta	Babeta	k1gFnSc1	Babeta
<g/>
"	"	kIx"	"
díky	díky	k7c3	díky
filmu	film	k1gInSc3	film
Kdyby	kdyby	k9	kdyby
tisíc	tisíc	k4xCgInPc2	tisíc
klarinetů	klarinet	k1gInPc2	klarinet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
měla	mít	k5eAaImAgFnS	mít
ambice	ambice	k1gFnPc4	ambice
stát	stát	k5eAaPmF	stát
se	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
armádním	armádní	k2eAgNnSc7d1	armádní
vozidlem	vozidlo	k1gNnSc7	vozidlo
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
oplývala	oplývat	k5eAaImAgFnS	oplývat
nejlepšími	dobrý	k2eAgFnPc7d3	nejlepší
jízdními	jízdní	k2eAgFnPc7d1	jízdní
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
porovnávaných	porovnávaný	k2eAgInPc2d1	porovnávaný
automobilů	automobil	k1gInPc2	automobil
(	(	kIx(	(
<g/>
konkurenty	konkurent	k1gMnPc7	konkurent
byly	být	k5eAaImAgFnP	být
zejména	zejména	k9	zejména
GAZ	GAZ	kA	GAZ
69	[number]	k4	69
a	a	k8xC	a
Tatra	Tatra	k1gFnSc1	Tatra
803	[number]	k4	803
<g/>
/	/	kIx~	/
<g/>
Tatra	Tatra	k1gFnSc1	Tatra
804	[number]	k4	804
<g/>
)	)	kIx)	)
–	–	k?	–
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
však	však	k9	však
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
dodávána	dodávat	k5eAaImNgFnS	dodávat
v	v	k7c6	v
desetitisícových	desetitisícový	k2eAgFnPc6d1	desetitisícová
sériích	série	k1gFnPc6	série
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
"	"	kIx"	"
<g/>
lidově	lidově	k6eAd1	lidově
demokratické	demokratický	k2eAgFnSc2d1	demokratická
armády	armáda	k1gFnSc2	armáda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
znamenalo	znamenat	k5eAaImAgNnS	znamenat
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
úplný	úplný	k2eAgInSc1d1	úplný
konec	konec	k1gInSc1	konec
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
jiné	jiný	k2eAgFnSc2d1	jiná
výroby	výroba	k1gFnSc2	výroba
v	v	k7c6	v
AZNP	AZNP	kA	AZNP
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
tedy	tedy	k9	tedy
i	i	k9	i
tyto	tento	k3xDgInPc1	tento
kapacitní	kapacitní	k2eAgInPc1d1	kapacitní
problémy	problém	k1gInPc1	problém
přispěly	přispět	k5eAaPmAgInP	přispět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
GAZ	GAZ	kA	GAZ
69	[number]	k4	69
a	a	k8xC	a
AZNP	AZNP	kA	AZNP
zůstala	zůstat	k5eAaPmAgFnS	zůstat
civilní	civilní	k2eAgFnSc1d1	civilní
výroba	výroba	k1gFnSc1	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
stranických	stranický	k2eAgMnPc2d1	stranický
a	a	k8xC	a
státních	státní	k2eAgMnPc2d1	státní
představitelů	představitel	k1gMnPc2	představitel
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
také	také	k9	také
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
limuzína	limuzína	k1gFnSc1	limuzína
Škoda	škoda	k6eAd1	škoda
VOS	vosa	k1gFnPc2	vosa
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
pro	pro	k7c4	pro
pancéřování	pancéřování	k1gNnSc4	pancéřování
lze	lze	k6eAd1	lze
také	také	k9	také
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
částečně	částečně	k6eAd1	částečně
vojenský	vojenský	k2eAgInSc4d1	vojenský
automobil	automobil	k1gInSc4	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Tatra	Tatra	k1gFnSc1	Tatra
Kopřivnice	Kopřivnice	k1gFnSc2	Kopřivnice
potřebovala	potřebovat	k5eAaImAgFnS	potřebovat
uvolnit	uvolnit	k5eAaPmF	uvolnit
výrobní	výrobní	k2eAgFnPc4d1	výrobní
kapacity	kapacita	k1gFnPc4	kapacita
pro	pro	k7c4	pro
vojenské	vojenský	k2eAgInPc4d1	vojenský
automobily	automobil	k1gInPc4	automobil
Tatra	Tatra	k1gFnSc1	Tatra
128	[number]	k4	128
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
AZNP	AZNP	kA	AZNP
dočasně	dočasně	k6eAd1	dočasně
převedena	převést	k5eAaPmNgFnS	převést
nejprve	nejprve	k6eAd1	nejprve
výroba	výroba	k1gFnSc1	výroba
automobilů	automobil	k1gInPc2	automobil
Tatra	Tatra	k1gFnSc1	Tatra
600	[number]	k4	600
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1951	[number]	k4	1951
až	až	k9	až
1953	[number]	k4	1953
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
2100	[number]	k4	2100
vozů	vůz	k1gInPc2	vůz
<g/>
)	)	kIx)	)
a	a	k8xC	a
Tatra	Tatra	k1gFnSc1	Tatra
805	[number]	k4	805
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1952	[number]	k4	1952
až	až	k9	až
1955	[number]	k4	1955
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
6410	[number]	k4	6410
vozů	vůz	k1gInPc2	vůz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
napětí	napětí	k1gNnSc1	napětí
povolilo	povolit	k5eAaPmAgNnS	povolit
<g/>
,	,	kIx,	,
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
výroba	výroba	k1gFnSc1	výroba
modelu	model	k1gInSc2	model
Škoda	škoda	k1gFnSc1	škoda
1200	[number]	k4	1200
a	a	k8xC	a
souběžně	souběžně	k6eAd1	souběžně
se	se	k3xPyFc4	se
připravoval	připravovat	k5eAaImAgInS	připravovat
moderní	moderní	k2eAgInSc1d1	moderní
"	"	kIx"	"
<g/>
mezityp	mezityp	k1gInSc1	mezityp
lidového	lidový	k2eAgInSc2d1	lidový
automobilu	automobil	k1gInSc2	automobil
<g/>
"	"	kIx"	"
Škoda	škoda	k1gFnSc1	škoda
440	[number]	k4	440
"	"	kIx"	"
<g/>
Spartak	Spartak	k1gInSc1	Spartak
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
modernizovaným	modernizovaný	k2eAgInSc7d1	modernizovaný
typem	typ	k1gInSc7	typ
Octavia	octavia	k1gFnSc1	octavia
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
se	se	k3xPyFc4	se
při	při	k7c6	při
reorganizaci	reorganizace	k1gFnSc6	reorganizace
československého	československý	k2eAgInSc2d1	československý
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
vzniku	vznik	k1gInSc2	vznik
VHJ	VHJ	kA	VHJ
staly	stát	k5eAaPmAgInP	stát
dosud	dosud	k6eAd1	dosud
samostatné	samostatný	k2eAgInPc1d1	samostatný
podniky	podnik	k1gInPc1	podnik
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
a	a	k8xC	a
v	v	k7c6	v
Kvasinách	Kvasina	k1gFnPc6	Kvasina
plně	plně	k6eAd1	plně
podřízenými	podřízený	k2eAgInPc7d1	podřízený
závody	závod	k1gInPc7	závod
AZNP	AZNP	kA	AZNP
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
dostali	dostat	k5eAaPmAgMnP	dostat
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
užitkových	užitkový	k2eAgFnPc6d1	užitková
modifikacích	modifikace	k1gFnPc6	modifikace
výběhového	výběhový	k2eAgInSc2d1	výběhový
typu	typ	k1gInSc2	typ
Škoda	škoda	k1gFnSc1	škoda
1200	[number]	k4	1200
<g/>
/	/	kIx~	/
<g/>
1201	[number]	k4	1201
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jak	jak	k8xC	jak
prostorný	prostorný	k2eAgInSc1d1	prostorný
"	"	kIx"	"
<g/>
stejšn	stejšn	k1gInSc1	stejšn
<g/>
"	"	kIx"	"
Škoda	škoda	k1gFnSc1	škoda
1202	[number]	k4	1202
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
–	–	k?	–
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
první	první	k4xOgMnSc1	první
a	a	k8xC	a
jediný	jediný	k2eAgInSc1d1	jediný
československý	československý	k2eAgInSc1d1	československý
trambusový	trambusový	k2eAgInSc1d1	trambusový
dodávkový	dodávkový	k2eAgInSc1d1	dodávkový
automobil	automobil	k1gInSc1	automobil
Škoda	škoda	k1gFnSc1	škoda
1203	[number]	k4	1203
(	(	kIx(	(
<g/>
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
1968	[number]	k4	1968
–	–	k?	–
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
v	v	k7c6	v
TAZ	TAZ	kA	TAZ
Trnava	Trnava	k1gFnSc1	Trnava
1973	[number]	k4	1973
–	–	k?	–
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Závod	závod	k1gInSc1	závod
Kvasiny	Kvasina	k1gFnSc2	Kvasina
krátce	krátce	k6eAd1	krátce
předtím	předtím	k6eAd1	předtím
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
roadster	roadster	k1gInSc1	roadster
Škoda	škoda	k1gFnSc1	škoda
450	[number]	k4	450
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
Škoda	škoda	k1gFnSc1	škoda
Felicia	felicia	k1gFnSc1	felicia
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
–	–	k?	–
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
celé	celý	k2eAgFnSc2d1	celá
AZNP	AZNP	kA	AZNP
stále	stále	k6eAd1	stále
nedokázaly	dokázat	k5eNaPmAgInP	dokázat
plně	plně	k6eAd1	plně
motorizovat	motorizovat	k5eAaBmF	motorizovat
Československo	Československo	k1gNnSc4	Československo
<g/>
,	,	kIx,	,
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
vláda	vláda	k1gFnSc1	vláda
o	o	k7c6	o
výstavbě	výstavba	k1gFnSc6	výstavba
nového	nový	k2eAgInSc2d1	nový
závodu	závod	k1gInSc2	závod
v	v	k7c6	v
Mladé	mladý	k2eAgFnSc6d1	mladá
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
automobilka	automobilka	k1gFnSc1	automobilka
byla	být	k5eAaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
zvýšit	zvýšit	k5eAaPmF	zvýšit
denní	denní	k2eAgFnSc4d1	denní
výrobu	výroba	k1gFnSc4	výroba
z	z	k7c2	z
50-60	[number]	k4	50-60
vozů	vůz	k1gInPc2	vůz
na	na	k7c4	na
400	[number]	k4	400
až	až	k9	až
500	[number]	k4	500
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
roční	roční	k2eAgFnSc1d1	roční
kapacita	kapacita	k1gFnSc1	kapacita
přes	přes	k7c4	přes
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
začala	začít	k5eAaPmAgFnS	začít
velkorysá	velkorysý	k2eAgFnSc1d1	velkorysá
výstavba	výstavba	k1gFnSc1	výstavba
nové	nový	k2eAgFnSc2d1	nová
automobilky	automobilka	k1gFnSc2	automobilka
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
cca	cca	kA	cca
80	[number]	k4	80
hektarů	hektar	k1gInPc2	hektar
<g/>
,	,	kIx,	,
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
elektrárnou	elektrárna	k1gFnSc7	elektrárna
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
nákladním	nákladní	k2eAgNnSc7d1	nákladní
nádražím	nádraží	k1gNnSc7	nádraží
<g/>
,	,	kIx,	,
délkou	délka	k1gFnSc7	délka
vleček	vlečka	k1gFnPc2	vlečka
10	[number]	k4	10
km	km	kA	km
a	a	k8xC	a
délkou	délka	k1gFnSc7	délka
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
komunikací	komunikace	k1gFnPc2	komunikace
13	[number]	k4	13
km	km	kA	km
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
spojovaly	spojovat	k5eAaImAgInP	spojovat
40	[number]	k4	40
nových	nový	k2eAgFnPc2d1	nová
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
investici	investice	k1gFnSc6	investice
se	se	k3xPyFc4	se
podílelo	podílet	k5eAaImAgNnS	podílet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
dodavatelů	dodavatel	k1gMnPc2	dodavatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
134	[number]	k4	134
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
včetně	včetně	k7c2	včetně
těch	ten	k3xDgMnPc2	ten
z	z	k7c2	z
nesocialistických	socialistický	k2eNgFnPc2d1	nesocialistická
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
indukční	indukční	k2eAgFnSc2d1	indukční
pece	pec	k1gFnSc2	pec
Siemens	siemens	k1gInSc1	siemens
a	a	k8xC	a
ACEC	ACEC	kA	ACEC
<g/>
,	,	kIx,	,
formovací	formovací	k2eAgInPc4d1	formovací
stroje	stroj	k1gInPc4	stroj
Colleman	Colleman	k1gMnSc1	Colleman
<g/>
,	,	kIx,	,
tlakové	tlakový	k2eAgInPc1d1	tlakový
licí	licí	k2eAgInPc1d1	licí
stroje	stroj	k1gInPc1	stroj
Triulzi	Triulze	k1gFnSc3	Triulze
<g/>
,	,	kIx,	,
obráběcí	obráběcí	k2eAgInPc4d1	obráběcí
automaty	automat	k1gInPc4	automat
Renault	renault	k1gInSc1	renault
<g/>
,	,	kIx,	,
lisovna	lisovna	k1gFnSc1	lisovna
Chausson	Chausson	k1gInSc1	Chausson
<g/>
,	,	kIx,	,
svařovací	svařovací	k2eAgInPc1d1	svařovací
automaty	automat	k1gInPc1	automat
Languepin	Languepina	k1gFnPc2	Languepina
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k6eAd1	tak
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
moderní	moderní	k2eAgInSc1d1	moderní
průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
provoz	provoz	k1gInSc1	provoz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
představoval	představovat	k5eAaImAgInS	představovat
naprostou	naprostý	k2eAgFnSc4d1	naprostá
špičku	špička	k1gFnSc4	špička
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
RVHP	RVHP	kA	RVHP
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejmodernějších	moderní	k2eAgFnPc2d3	nejmodernější
továren	továrna	k1gFnPc2	továrna
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
už	už	k6eAd1	už
probíhal	probíhat	k5eAaImAgInS	probíhat
vývoj	vývoj	k1gInSc1	vývoj
nového	nový	k2eAgInSc2d1	nový
<g/>
,	,	kIx,	,
lehkého	lehký	k2eAgInSc2d1	lehký
a	a	k8xC	a
prostorného	prostorný	k2eAgInSc2d1	prostorný
lidového	lidový	k2eAgInSc2d1	lidový
automobilu	automobil	k1gInSc2	automobil
se	s	k7c7	s
samonosnou	samonosný	k2eAgFnSc7d1	samonosná
karosérií	karosérie	k1gFnSc7	karosérie
a	a	k8xC	a
s	s	k7c7	s
motorem	motor	k1gInSc7	motor
vzadu	vzadu	k6eAd1	vzadu
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
interním	interní	k2eAgNnSc7d1	interní
typovým	typový	k2eAgNnSc7d1	typové
označením	označení	k1gNnSc7	označení
Škoda	škoda	k1gFnSc1	škoda
988	[number]	k4	988
<g/>
,	,	kIx,	,
též	též	k9	též
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
NOV	nov	k1gInSc1	nov
(	(	kIx(	(
<g/>
nový	nový	k2eAgInSc1d1	nový
osobní	osobní	k2eAgInSc1d1	osobní
vůz	vůz	k1gInSc1	vůz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
začal	začít	k5eAaPmAgInS	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1964	[number]	k4	1964
pod	pod	k7c7	pod
obchodním	obchodní	k2eAgNnSc7d1	obchodní
označením	označení	k1gNnSc7	označení
Škoda	škoda	k1gFnSc1	škoda
1000	[number]	k4	1000
MB	MB	kA	MB
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
také	také	k9	také
automobilka	automobilka	k1gFnSc1	automobilka
oficiálně	oficiálně	k6eAd1	oficiálně
slavila	slavit	k5eAaImAgFnS	slavit
70	[number]	k4	70
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
založení	založení	k1gNnSc2	založení
a	a	k8xC	a
nový	nový	k2eAgInSc1d1	nový
automobil	automobil	k1gInSc1	automobil
tak	tak	k6eAd1	tak
přestavoval	přestavovat	k5eAaImAgInS	přestavovat
"	"	kIx"	"
<g/>
dárek	dárek	k1gInSc1	dárek
<g/>
"	"	kIx"	"
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
výročí	výročí	k1gNnSc3	výročí
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
to	ten	k3xDgNnSc4	ten
byla	být	k5eAaImAgFnS	být
už	už	k6eAd1	už
tehdy	tehdy	k6eAd1	tehdy
lehce	lehko	k6eAd1	lehko
vyvratitelná	vyvratitelný	k2eAgFnSc1d1	vyvratitelná
propagandistická	propagandistický	k2eAgFnSc1d1	propagandistická
lež	lež	k1gFnSc1	lež
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
se	se	k3xPyFc4	se
zakladatelé	zakladatel	k1gMnPc1	zakladatel
ještě	ještě	k6eAd1	ještě
ani	ani	k8xC	ani
neznali	neznat	k5eAaImAgMnP	neznat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nekoncepčnost	Nekoncepčnost	k1gFnSc1	Nekoncepčnost
státního	státní	k2eAgNnSc2d1	státní
vedení	vedení	k1gNnSc2	vedení
AZNP	AZNP	kA	AZNP
si	se	k3xPyFc3	se
však	však	k9	však
dál	daleko	k6eAd2	daleko
vybírala	vybírat	k5eAaImAgFnS	vybírat
svou	svůj	k3xOyFgFnSc4	svůj
daň	daň	k1gFnSc4	daň
<g/>
:	:	kIx,	:
nový	nový	k2eAgInSc1d1	nový
model	model	k1gInSc1	model
se	se	k3xPyFc4	se
zpočátku	zpočátku	k6eAd1	zpočátku
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
jen	jen	k9	jen
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
směně	směna	k1gFnSc6	směna
a	a	k8xC	a
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
nepřevyšujícím	převyšující	k2eNgInSc6d1	nepřevyšující
10	[number]	k4	10
kusů	kus	k1gInPc2	kus
denně	denně	k6eAd1	denně
(	(	kIx(	(
<g/>
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
navyšovaným	navyšovaný	k2eAgNnSc7d1	navyšované
na	na	k7c4	na
170	[number]	k4	170
kusů	kus	k1gInPc2	kus
denně	denně	k6eAd1	denně
(	(	kIx(	(
<g/>
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
500	[number]	k4	500
vozů	vůz	k1gInPc2	vůz
denně	denně	k6eAd1	denně
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
až	až	k9	až
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
vinu	vina	k1gFnSc4	vina
nesl	nést	k5eAaImAgInS	nést
i	i	k9	i
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
výrobní	výrobní	k2eAgInSc1d1	výrobní
program	program	k1gInSc1	program
v	v	k7c6	v
Kvasinách	Kvasina	k1gFnPc6	Kvasina
i	i	k8xC	i
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
vycházel	vycházet	k5eAaImAgMnS	vycházet
ze	z	k7c2	z
starých	starý	k2eAgInPc2d1	starý
modelů	model	k1gInPc2	model
<g/>
,	,	kIx,	,
příčemž	příčemž	k6eAd1	příčemž
technické	technický	k2eAgFnPc4d1	technická
podpory	podpora	k1gFnPc4	podpora
této	tento	k3xDgFnSc2	tento
výroby	výroba	k1gFnSc2	výroba
se	se	k3xPyFc4	se
v	v	k7c6	v
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
nemohli	moct	k5eNaImAgMnP	moct
jen	jen	k9	jen
tak	tak	k6eAd1	tak
zbavit	zbavit	k5eAaPmF	zbavit
<g/>
:	:	kIx,	:
litinové	litinový	k2eAgInPc1d1	litinový
motory	motor	k1gInPc1	motor
Škoda	škoda	k1gFnSc1	škoda
1221	[number]	k4	1221
cm3	cm3	k4	cm3
sice	sice	k8xC	sice
nakonec	nakonec	k6eAd1	nakonec
převzal	převzít	k5eAaPmAgMnS	převzít
do	do	k7c2	do
výroby	výroba	k1gFnSc2	výroba
n.	n.	k?	n.
<g/>
p.	p.	k?	p.
Kovosmalt	kovosmalt	k1gInSc4	kovosmalt
Trnava	Trnava	k1gFnSc1	Trnava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
(	(	kIx(	(
<g/>
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
tak	tak	k6eAd1	tak
učiněn	učiněn	k2eAgInSc4d1	učiněn
první	první	k4xOgInSc4	první
krok	krok	k1gInSc4	krok
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
Trnavských	trnavský	k2eAgInPc2d1	trnavský
automobilových	automobilový	k2eAgInPc2d1	automobilový
závodů	závod	k1gInPc2	závod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
například	například	k6eAd1	například
páteřové	páteřový	k2eAgInPc4d1	páteřový
podvozky	podvozek	k1gInPc4	podvozek
musela	muset	k5eAaImAgFnS	muset
mateřská	mateřský	k2eAgFnSc1d1	mateřská
automobilka	automobilka	k1gFnSc1	automobilka
dodávat	dodávat	k5eAaImF	dodávat
do	do	k7c2	do
Kvasin	Kvasina	k1gFnPc2	Kvasina
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
teprve	teprve	k6eAd1	teprve
skončila	skončit	k5eAaPmAgFnS	skončit
výroba	výroba	k1gFnSc1	výroba
typu	typ	k1gInSc2	typ
Octavia	octavia	k1gFnSc1	octavia
Combi	Comb	k1gFnSc2	Comb
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
už	už	k9	už
také	také	k9	také
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
motor	motor	k1gInSc1	motor
vzadu	vzadu	k6eAd1	vzadu
nemá	mít	k5eNaImIp3nS	mít
další	další	k2eAgFnSc4d1	další
perspektivu	perspektiva	k1gFnSc4	perspektiva
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
projekt	projekt	k1gInSc4	projekt
nového	nový	k2eAgMnSc2d1	nový
ředitele	ředitel	k1gMnSc2	ředitel
AZNP	AZNP	kA	AZNP
Ing.	ing.	kA	ing.
Josefa	Josef	k1gMnSc2	Josef
Šimona	Šimon	k1gMnSc2	Šimon
"	"	kIx"	"
<g/>
Rozvoj	rozvoj	k1gInSc1	rozvoj
integrované	integrovaný	k2eAgFnSc2d1	integrovaná
výroby	výroba	k1gFnSc2	výroba
osobních	osobní	k2eAgInPc2d1	osobní
vozů	vůz	k1gInPc2	vůz
v	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
<g/>
"	"	kIx"	"
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
počítal	počítat	k5eAaImAgInS	počítat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nástupce	nástupce	k1gMnSc1	nástupce
kompaktního	kompaktní	k2eAgInSc2d1	kompaktní
rodinného	rodinný	k2eAgInSc2d1	rodinný
vozu	vůz	k1gInSc2	vůz
1000	[number]	k4	1000
<g />
.	.	kIx.	.
</s>
<s>
MB	MB	kA	MB
pod	pod	k7c7	pod
typovým	typový	k2eAgNnSc7d1	typové
označením	označení	k1gNnSc7	označení
Škoda	škoda	k1gFnSc1	škoda
740	[number]	k4	740
(	(	kIx(	(
<g/>
obchodní	obchodní	k2eAgNnPc4d1	obchodní
označení	označení	k1gNnPc4	označení
Škoda	škoda	k6eAd1	škoda
900	[number]	k4	900
a	a	k8xC	a
Škoda	škoda	k1gFnSc1	škoda
1100	[number]	k4	1100
<g/>
)	)	kIx)	)
vrátí	vrátit	k5eAaPmIp3nS	vrátit
ke	k	k7c3	k
koncepci	koncepce	k1gFnSc3	koncepce
s	s	k7c7	s
motorem	motor	k1gInSc7	motor
vpředu	vpředu	k6eAd1	vpředu
a	a	k8xC	a
pohonem	pohon	k1gInSc7	pohon
zadních	zadní	k2eAgNnPc2d1	zadní
kol	kolo	k1gNnPc2	kolo
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
stejnou	stejný	k2eAgFnSc4d1	stejná
koncepci	koncepce	k1gFnSc4	koncepce
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
i	i	k9	i
vyšší	vysoký	k2eAgFnSc1d2	vyšší
modelová	modelový	k2eAgFnSc1d1	modelová
řada	řada	k1gFnSc1	řada
NOV	nov	k1gInSc1	nov
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
zadána	zadat	k5eAaPmNgFnS	zadat
k	k	k7c3	k
řešení	řešení	k1gNnSc3	řešení
jako	jako	k8xC	jako
typová	typový	k2eAgFnSc1d1	typová
řada	řada	k1gFnSc1	řada
Škoda	škoda	k1gFnSc1	škoda
720	[number]	k4	720
(	(	kIx(	(
<g/>
obchodní	obchodní	k2eAgNnPc4d1	obchodní
označení	označení	k1gNnPc4	označení
Škoda	škoda	k6eAd1	škoda
1250	[number]	k4	1250
a	a	k8xC	a
Škoda	škoda	k1gFnSc1	škoda
1500	[number]	k4	1500
<g/>
)	)	kIx)	)
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
stylistické	stylistický	k2eAgNnSc4d1	stylistické
řešení	řešení	k1gNnSc4	řešení
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
Giorgetto	Giorgetto	k1gNnSc1	Giorgetto
Giugiaro	Giugiara	k1gFnSc5	Giugiara
<g/>
.	.	kIx.	.
</s>
<s>
Počátek	počátek	k1gInSc1	počátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
a	a	k8xC	a
politická	politický	k2eAgFnSc1d1	politická
normalizace	normalizace	k1gFnSc1	normalizace
však	však	k9	však
opět	opět	k6eAd1	opět
byly	být	k5eAaImAgFnP	být
jen	jen	k9	jen
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
tyto	tento	k3xDgInPc1	tento
projekty	projekt	k1gInPc1	projekt
nedošly	dojít	k5eNaPmAgFnP	dojít
realizace	realizace	k1gFnPc1	realizace
–	–	k?	–
problém	problém	k1gInSc1	problém
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
už	už	k6eAd1	už
v	v	k7c6	v
samotném	samotný	k2eAgNnSc6d1	samotné
nekoncepčním	koncepční	k2eNgNnSc6d1	nekoncepční
zadání	zadání	k1gNnSc6	zadání
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
projekty	projekt	k1gInPc1	projekt
720	[number]	k4	720
a	a	k8xC	a
740	[number]	k4	740
neměly	mít	k5eNaImAgFnP	mít
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
ani	ani	k8xC	ani
se	s	k7c7	s
stávajícími	stávající	k2eAgInPc7d1	stávající
modely	model	k1gInPc7	model
a	a	k8xC	a
technologiemi	technologie	k1gFnPc7	technologie
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
navzájem	navzájem	k6eAd1	navzájem
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
realizace	realizace	k1gFnSc1	realizace
by	by	kYmCp3nS	by
tak	tak	k6eAd1	tak
vyžadovala	vyžadovat	k5eAaImAgFnS	vyžadovat
investici	investice	k1gFnSc4	investice
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
ani	ani	k9	ani
pomyšlení	pomyšlení	k1gNnSc4	pomyšlení
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
AZNP	AZNP	kA	AZNP
dosud	dosud	k6eAd1	dosud
splácely	splácet	k5eAaImAgInP	splácet
úvěr	úvěr	k1gInSc4	úvěr
na	na	k7c4	na
dosavadní	dosavadní	k2eAgFnSc4d1	dosavadní
výstavbu	výstavba	k1gFnSc4	výstavba
<g/>
.	.	kIx.	.
</s>
<s>
Třetím	třetí	k4xOgInSc7	třetí
důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
snaha	snaha	k1gFnSc1	snaha
postavit	postavit	k5eAaPmF	postavit
plnohodnotnou	plnohodnotný	k2eAgFnSc4d1	plnohodnotná
automobilku	automobilka	k1gFnSc4	automobilka
ve	v	k7c6	v
slovenské	slovenský	k2eAgFnSc6d1	slovenská
části	část	k1gFnSc6	část
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yQnSc6	což
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
federální	federální	k2eAgFnSc1d1	federální
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1971	[number]	k4	1971
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
typová	typový	k2eAgFnSc1d1	typová
řada	řada	k1gFnSc1	řada
720	[number]	k4	720
bude	být	k5eAaImBp3nS	být
výrobním	výrobní	k2eAgInSc7d1	výrobní
programem	program	k1gInSc7	program
automobilky	automobilka	k1gFnSc2	automobilka
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
a	a	k8xC	a
že	že	k8xS	že
AZNP	AZNP	kA	AZNP
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
a	a	k8xC	a
výrobě	výroba	k1gFnSc6	výroba
malých	malý	k2eAgInPc2d1	malý
automobilů	automobil	k1gInPc2	automobil
orientovat	orientovat	k5eAaBmF	orientovat
na	na	k7c6	na
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
východoněmeckými	východoněmecký	k2eAgFnPc7d1	východoněmecká
automobilkami	automobilka	k1gFnPc7	automobilka
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
dán	dán	k2eAgInSc4d1	dán
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
typové	typový	k2eAgFnSc2d1	typová
řady	řada	k1gFnSc2	řada
Škoda	škoda	k1gFnSc1	škoda
760	[number]	k4	760
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
nedočkala	dočkat	k5eNaPmAgFnS	dočkat
realizace	realizace	k1gFnSc1	realizace
<g/>
:	:	kIx,	:
zatímco	zatímco	k8xS	zatímco
AZNP	AZNP	kA	AZNP
se	se	k3xPyFc4	se
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
krok	krok	k1gInSc4	krok
technologicky	technologicky	k6eAd1	technologicky
a	a	k8xC	a
organizačně	organizačně	k6eAd1	organizačně
připravily	připravit	k5eAaPmAgFnP	připravit
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
nedostatek	nedostatek	k1gInSc1	nedostatek
vůle	vůle	k1gFnSc2	vůle
a	a	k8xC	a
investic	investice	k1gFnPc2	investice
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
tamní	tamní	k2eAgMnPc1d1	tamní
představitelé	představitel	k1gMnPc1	představitel
přiznali	přiznat	k5eAaPmAgMnP	přiznat
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
Znamenalo	znamenat	k5eAaImAgNnS	znamenat
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
zakonzervování	zakonzervování	k1gNnSc1	zakonzervování
výroby	výroba	k1gFnSc2	výroba
modelů	model	k1gInPc2	model
s	s	k7c7	s
motorem	motor	k1gInSc7	motor
vzadu	vzadu	k6eAd1	vzadu
v	v	k7c6	v
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
(	(	kIx(	(
<g/>
faceliftem	facelifto	k1gNnSc7	facelifto
modelu	model	k1gInSc2	model
1000	[number]	k4	1000
<g/>
/	/	kIx~	/
<g/>
1100	[number]	k4	1100
MB	MB	kA	MB
na	na	k7c4	na
model	model	k1gInSc4	model
100	[number]	k4	100
<g/>
/	/	kIx~	/
<g/>
110	[number]	k4	110
<g/>
)	)	kIx)	)
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
osvojení	osvojení	k1gNnSc1	osvojení
i	i	k9	i
v	v	k7c6	v
Kvasinách	Kvasina	k1gFnPc6	Kvasina
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
pohledné	pohledný	k2eAgNnSc4d1	pohledné
kupé	kupé	k1gNnSc4	kupé
Škoda	škoda	k6eAd1	škoda
110	[number]	k4	110
R	R	kA	R
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Napohled	napohled	k6eAd1	napohled
bezvýchodnou	bezvýchodný	k2eAgFnSc4d1	bezvýchodná
situaci	situace	k1gFnSc4	situace
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyráběli	vyrábět	k5eAaImAgMnP	vyrábět
užitkový	užitkový	k2eAgInSc4d1	užitkový
vůz	vůz	k1gInSc4	vůz
Škoda	škoda	k1gFnSc1	škoda
1203	[number]	k4	1203
prakticky	prakticky	k6eAd1	prakticky
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
zbytku	zbytek	k1gInSc6	zbytek
AZNP	AZNP	kA	AZNP
<g/>
,	,	kIx,	,
vyřešil	vyřešit	k5eAaPmAgMnS	vyřešit
tamní	tamní	k2eAgMnSc1d1	tamní
závodní	závodní	k2eAgMnSc1d1	závodní
ředitel	ředitel	k1gMnSc1	ředitel
Miroslav	Miroslav	k1gMnSc1	Miroslav
Zapadlo	zapadnout	k5eAaPmAgNnS	zapadnout
téměř	téměř	k6eAd1	téměř
partyzánsky	partyzánsky	k6eAd1	partyzánsky
provedenou	provedený	k2eAgFnSc7d1	provedená
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
výrobních	výrobní	k2eAgFnPc2d1	výrobní
hal	hala	k1gFnPc2	hala
a	a	k8xC	a
účetními	účetní	k2eAgFnPc7d1	účetní
repasemi	repase	k1gFnPc7	repase
strojů	stroj	k1gInPc2	stroj
tak	tak	k8xC	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
tamní	tamní	k2eAgFnPc1d1	tamní
výrobní	výrobní	k2eAgFnPc1d1	výrobní
linky	linka	k1gFnPc1	linka
mohly	moct	k5eAaImAgFnP	moct
začít	začít	k5eAaPmF	začít
sestavovat	sestavovat	k5eAaImF	sestavovat
prakticky	prakticky	k6eAd1	prakticky
libovolný	libovolný	k2eAgInSc4d1	libovolný
automobil	automobil	k1gInSc4	automobil
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
tak	tak	k6eAd1	tak
připraveno	připravit	k5eAaPmNgNnS	připravit
osvojení	osvojení	k1gNnSc1	osvojení
výroby	výroba	k1gFnSc2	výroba
některých	některý	k3yIgFnPc2	některý
verzí	verze	k1gFnPc2	verze
typové	typový	k2eAgFnSc2d1	typová
řady	řada	k1gFnSc2	řada
760	[number]	k4	760
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
spuštění	spuštění	k1gNnSc1	spuštění
se	se	k3xPyFc4	se
však	však	k9	však
oddalovalo	oddalovat	k5eAaImAgNnS	oddalovat
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
zrušilo	zrušit	k5eAaPmAgNnS	zrušit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
už	už	k6eAd1	už
zastaralé	zastaralý	k2eAgInPc1d1	zastaralý
typy	typ	k1gInPc1	typ
100	[number]	k4	100
<g/>
/	/	kIx~	/
<g/>
110	[number]	k4	110
nahradili	nahradit	k5eAaPmAgMnP	nahradit
v	v	k7c6	v
Mladé	mladý	k2eAgFnSc6d1	mladá
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
moderním	moderní	k2eAgInSc7d1	moderní
typem	typ	k1gInSc7	typ
Škoda	škoda	k1gFnSc1	škoda
105	[number]	k4	105
<g/>
/	/	kIx~	/
<g/>
120	[number]	k4	120
s	s	k7c7	s
elegantní	elegantní	k2eAgFnSc7d1	elegantní
Giugiarovou	Giugiarův	k2eAgFnSc7d1	Giugiarova
karoserií	karoserie	k1gFnSc7	karoserie
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
vrchlabský	vrchlabský	k2eAgInSc4d1	vrchlabský
závod	závod	k1gInSc4	závod
bez	bez	k7c2	bez
větších	veliký	k2eAgFnPc2d2	veliký
obtíží	obtíž	k1gFnPc2	obtíž
připravil	připravit	k5eAaPmAgMnS	připravit
produkci	produkce	k1gFnSc4	produkce
nejluxusnějších	luxusní	k2eAgInPc2d3	nejluxusnější
modelů	model	k1gInPc2	model
Škoda	škoda	k6eAd1	škoda
120	[number]	k4	120
GLS	GLS	kA	GLS
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
105	[number]	k4	105
GL	GL	kA	GL
<g/>
,	,	kIx,	,
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
skříňových	skříňový	k2eAgFnPc2d1	skříňová
verzí	verze	k1gFnPc2	verze
Škoda	škoda	k6eAd1	škoda
1203	[number]	k4	1203
COM	COM	kA	COM
<g/>
,	,	kIx,	,
mikrobus	mikrobus	k1gInSc1	mikrobus
a	a	k8xC	a
sanita	sanita	k1gFnSc1	sanita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kvasinách	Kvasina	k1gFnPc6	Kvasina
později	pozdě	k6eAd2	pozdě
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
kupé	kupé	k1gNnSc2	kupé
Škoda	škoda	k6eAd1	škoda
Garde	garde	k1gNnSc2	garde
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Rapid	rapid	k1gInSc1	rapid
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
už	už	k9	už
byla	být	k5eAaImAgFnS	být
ale	ale	k9	ale
situace	situace	k1gFnSc1	situace
neudržitelná	udržitelný	k2eNgFnSc1d1	neudržitelná
a	a	k8xC	a
automobily	automobil	k1gInPc1	automobil
s	s	k7c7	s
motorem	motor	k1gInSc7	motor
vzadu	vzadu	k6eAd1	vzadu
rychle	rychle	k6eAd1	rychle
ztrácely	ztrácet	k5eAaImAgInP	ztrácet
trhy	trh	k1gInPc1	trh
zejména	zejména	k9	zejména
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
kterýžto	kterýžto	k?	kterýžto
vývoz	vývoz	k1gInSc1	vývoz
byl	být	k5eAaImAgInS	být
jinak	jinak	k6eAd1	jinak
velmi	velmi	k6eAd1	velmi
dobrým	dobrý	k2eAgInSc7d1	dobrý
zdrojem	zdroj	k1gInSc7	zdroj
deviz	deviza	k1gFnPc2	deviza
pro	pro	k7c4	pro
československé	československý	k2eAgNnSc4d1	Československé
hospodářství	hospodářství	k1gNnSc4	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
samozřejmě	samozřejmě	k6eAd1	samozřejmě
hrála	hrát	k5eAaImAgFnS	hrát
i	i	k9	i
kolísající	kolísající	k2eAgFnSc1d1	kolísající
jakost	jakost	k1gFnSc1	jakost
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
již	již	k6eAd1	již
nevrátila	vrátit	k5eNaPmAgFnS	vrátit
k	k	k7c3	k
drastické	drastický	k2eAgFnSc3d1	drastická
nekvalitě	nekvalita	k1gFnSc3	nekvalita
poloviny	polovina	k1gFnSc2	polovina
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
zaostávala	zaostávat	k5eAaImAgFnS	zaostávat
za	za	k7c7	za
jinými	jiný	k2eAgFnPc7d1	jiná
značkami	značka	k1gFnPc7	značka
a	a	k8xC	a
malým	malý	k2eAgInPc3d1	malý
automobilům	automobil	k1gInPc3	automobil
značky	značka	k1gFnSc2	značka
Volkswagen	volkswagen	k1gInSc1	volkswagen
<g/>
,	,	kIx,	,
Peugeot	peugeot	k1gInSc1	peugeot
či	či	k8xC	či
Renault	renault	k1gInSc1	renault
tak	tak	k8xC	tak
vozy	vůz	k1gInPc1	vůz
Škoda	škoda	k6eAd1	škoda
konkurovaly	konkurovat	k5eAaImAgInP	konkurovat
hlavně	hlavně	k9	hlavně
cenově	cenově	k6eAd1	cenově
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nedostatek	nedostatek	k1gInSc4	nedostatek
investic	investice	k1gFnPc2	investice
tehdy	tehdy	k6eAd1	tehdy
hledali	hledat	k5eAaImAgMnP	hledat
konstruktéři	konstruktér	k1gMnPc1	konstruktér
AZNP	AZNP	kA	AZNP
<g/>
,	,	kIx,	,
BAZ	BAZ	kA	BAZ
i	i	k8xC	i
ÚVMV	ÚVMV	kA	ÚVMV
východisko	východisko	k1gNnSc1	východisko
zejména	zejména	k9	zejména
v	v	k7c6	v
důkladných	důkladný	k2eAgFnPc6d1	důkladná
modifikacích	modifikace	k1gFnPc6	modifikace
typu	typ	k1gInSc2	typ
105	[number]	k4	105
<g/>
/	/	kIx~	/
<g/>
120	[number]	k4	120
<g/>
,	,	kIx,	,
s	s	k7c7	s
přemístěním	přemístění	k1gNnSc7	přemístění
motoru	motor	k1gInSc2	motor
nad	nad	k7c4	nad
přední	přední	k2eAgFnSc4d1	přední
nápravu	náprava	k1gFnSc4	náprava
a	a	k8xC	a
s	s	k7c7	s
pohonem	pohon	k1gInSc7	pohon
přední	přední	k2eAgFnSc2d1	přední
nebo	nebo	k8xC	nebo
zadní	zadní	k2eAgFnSc2d1	zadní
nápravy	náprava	k1gFnSc2	náprava
–	–	k?	–
důraz	důraz	k1gInSc1	důraz
byl	být	k5eAaImAgInS	být
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
maximální	maximální	k2eAgFnSc4d1	maximální
unifikaci	unifikace	k1gFnSc4	unifikace
se	s	k7c7	s
sériovými	sériový	k2eAgInPc7d1	sériový
modely	model	k1gInPc7	model
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
na	na	k7c4	na
využití	využití	k1gNnSc4	využití
současných	současný	k2eAgFnPc2d1	současná
součástek	součástka	k1gFnPc2	součástka
<g/>
,	,	kIx,	,
minimální	minimální	k2eAgInPc1d1	minimální
zásahy	zásah	k1gInPc1	zásah
do	do	k7c2	do
kabinové	kabinový	k2eAgFnSc2d1	kabinová
klece	klec	k1gFnSc2	klec
a	a	k8xC	a
do	do	k7c2	do
vnějších	vnější	k2eAgInPc2d1	vnější
panelů	panel	k1gInPc2	panel
karosérie	karosérie	k1gFnSc2	karosérie
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
okolo	okolo	k7c2	okolo
deseti	deset	k4xCc2	deset
různých	různý	k2eAgInPc2d1	různý
funkčních	funkční	k2eAgInPc2d1	funkční
vzorků	vzorek	k1gInPc2	vzorek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
všechny	všechen	k3xTgMnPc4	všechen
vynikaly	vynikat	k5eAaImAgFnP	vynikat
nad	nad	k7c7	nad
sériovými	sériový	k2eAgInPc7d1	sériový
modely	model	k1gInPc7	model
jak	jak	k8xC	jak
jízdními	jízdní	k2eAgFnPc7d1	jízdní
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
užitnou	užitný	k2eAgFnSc7d1	užitná
hodnotou	hodnota	k1gFnSc7	hodnota
a	a	k8xC	a
využitím	využití	k1gNnSc7	využití
obestavěného	obestavěný	k2eAgInSc2d1	obestavěný
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Vznila	Vznila	k1gFnSc1	Vznila
i	i	k8xC	i
řada	řada	k1gFnSc1	řada
karosářských	karosářský	k2eAgInPc2d1	karosářský
návrhů	návrh	k1gInPc2	návrh
využívajících	využívající	k2eAgInPc2d1	využívající
shodný	shodný	k2eAgInSc4d1	shodný
ponton	ponton	k1gInSc4	ponton
karosérie	karosérie	k1gFnSc2	karosérie
pro	pro	k7c4	pro
verze	verze	k1gFnPc4	verze
se	s	k7c7	s
stupňovitou	stupňovitý	k2eAgFnSc7d1	stupňovitá
<g/>
,	,	kIx,	,
splývavou	splývavý	k2eAgFnSc7d1	splývavá
i	i	k8xC	i
svislou	svislý	k2eAgFnSc7d1	svislá
zádí	záď	k1gFnSc7	záď
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
variace	variace	k1gFnPc1	variace
však	však	k9	však
stále	stále	k6eAd1	stále
konzervovaly	konzervovat	k5eAaBmAgFnP	konzervovat
karosářskou	karosářský	k2eAgFnSc4d1	karosářská
i	i	k8xC	i
technickou	technický	k2eAgFnSc4d1	technická
realitu	realita	k1gFnSc4	realita
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
proto	proto	k8xC	proto
přijala	přijmout	k5eAaPmAgFnS	přijmout
federální	federální	k2eAgFnSc1d1	federální
vláda	vláda	k1gFnSc1	vláda
usnesení	usnesení	k1gNnSc2	usnesení
č.	č.	k?	č.
282	[number]	k4	282
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ukládalo	ukládat	k5eAaImAgNnS	ukládat
AZNP	AZNP	kA	AZNP
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
vyvinout	vyvinout	k5eAaPmF	vyvinout
a	a	k8xC	a
připravit	připravit	k5eAaPmF	připravit
do	do	k7c2	do
výroby	výroba	k1gFnSc2	výroba
zcela	zcela	k6eAd1	zcela
nový	nový	k2eAgInSc4d1	nový
typ	typ	k1gInSc4	typ
kompaktního	kompaktní	k2eAgInSc2d1	kompaktní
rodinného	rodinný	k2eAgInSc2d1	rodinný
vozu	vůz	k1gInSc2	vůz
s	s	k7c7	s
motorem	motor	k1gInSc7	motor
vpředu	vpředu	k6eAd1	vpředu
a	a	k8xC	a
s	s	k7c7	s
pětidveřovou	pětidveřový	k2eAgFnSc7d1	pětidveřová
karosérií	karosérie	k1gFnSc7	karosérie
italského	italský	k2eAgInSc2d1	italský
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Automobilka	automobilka	k1gFnSc1	automobilka
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
nového	nový	k2eAgMnSc2d1	nový
ředitele	ředitel	k1gMnSc2	ředitel
Jana	Jan	k1gMnSc2	Jan
Banýra	Banýr	k1gMnSc2	Banýr
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
tomuto	tento	k3xDgInSc3	tento
úkolu	úkol	k1gInSc3	úkol
dostát	dostát	k5eAaPmF	dostát
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nebyl	být	k5eNaImAgMnS	být
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1983	[number]	k4	1983
ustaven	ustaven	k2eAgInSc1d1	ustaven
nový	nový	k2eAgInSc1d1	nový
Výzkumně-vývojový	Výzkumněývojový	k2eAgInSc1d1	Výzkumně-vývojový
ústav	ústav	k1gInSc1	ústav
AZNP	AZNP	kA	AZNP
a	a	k8xC	a
do	do	k7c2	do
jeho	on	k3xPp3gNnSc2	on
čela	čelo	k1gNnSc2	čelo
postaven	postaven	k2eAgMnSc1d1	postaven
Petr	Petr	k1gMnSc1	Petr
Hrdlička	Hrdlička	k1gMnSc1	Hrdlička
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
prvorepublikového	prvorepublikový	k2eAgMnSc2d1	prvorepublikový
ředitele	ředitel	k1gMnSc2	ředitel
ASAP	ASAP	kA	ASAP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
VVÚ	VVÚ	kA	VVÚ
AZNP	AZNP	kA	AZNP
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
nového	nový	k2eAgMnSc2d1	nový
ředitele	ředitel	k1gMnSc2	ředitel
nasadil	nasadit	k5eAaPmAgMnS	nasadit
obrovské	obrovský	k2eAgNnSc4d1	obrovské
tempo	tempo	k1gNnSc4	tempo
při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
typu	typ	k1gInSc2	typ
781	[number]	k4	781
<g/>
,	,	kIx,	,
designem	design	k1gInSc7	design
pro	pro	k7c4	pro
základní	základní	k2eAgFnSc4d1	základní
karosérii	karosérie	k1gFnSc4	karosérie
pověřil	pověřit	k5eAaPmAgInS	pověřit
firmu	firma	k1gFnSc4	firma
Stile	Stil	k1gMnSc2	Stil
Italia	Italius	k1gMnSc2	Italius
Nuccio	Nuccio	k6eAd1	Nuccio
Bertone	Berton	k1gInSc5	Berton
a	a	k8xC	a
při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
všech	všecek	k3xTgFnPc2	všecek
částí	část	k1gFnPc2	část
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
s	s	k7c7	s
předními	přední	k2eAgFnPc7d1	přední
vývojovými	vývojový	k2eAgFnPc7d1	vývojová
a	a	k8xC	a
výrobními	výrobní	k2eAgInPc7d1	výrobní
podniky	podnik	k1gInPc7	podnik
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
i	i	k8xC	i
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
typu	typ	k1gInSc2	typ
pod	pod	k7c7	pod
obchodním	obchodní	k2eAgNnSc7d1	obchodní
označením	označení	k1gNnSc7	označení
Škoda	Škoda	k1gMnSc1	Škoda
Favorit	favorit	k1gMnSc1	favorit
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
šibeničním	šibeniční	k2eAgInSc6d1	šibeniční
termínu	termín	k1gInSc6	termín
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1985	[number]	k4	1985
skutečně	skutečně	k6eAd1	skutečně
dokončit	dokončit	k5eAaPmF	dokončit
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
byl	být	k5eAaImAgInS	být
automobil	automobil	k1gInSc1	automobil
představen	představit	k5eAaPmNgInS	představit
veřejnosti	veřejnost	k1gFnSc3	veřejnost
na	na	k7c6	na
veletrhu	veletrh	k1gInSc6	veletrh
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
zahájena	zahájen	k2eAgFnSc1d1	zahájena
výroba	výroba	k1gFnSc1	výroba
první	první	k4xOgFnSc2	první
série	série	k1gFnSc2	série
v	v	k7c6	v
základním	základní	k2eAgNnSc6d1	základní
provedení	provedení	k1gNnSc6	provedení
136	[number]	k4	136
L	L	kA	L
(	(	kIx(	(
<g/>
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
i	i	k9	i
karosárna	karosárna	k1gFnSc1	karosárna
v	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
závodě	závod	k1gInSc6	závod
v	v	k7c6	v
Mladé	mladý	k2eAgFnSc6d1	mladá
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
<g/>
)	)	kIx)	)
a	a	k8xC	a
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
byly	být	k5eAaImAgInP	být
vozy	vůz	k1gInPc1	vůz
dodávány	dodávat	k5eAaImNgInP	dodávat
výhradně	výhradně	k6eAd1	výhradně
organizacím	organizace	k1gFnPc3	organizace
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
náročném	náročný	k2eAgInSc6d1	náročný
provozu	provoz	k1gInSc6	provoz
stihla	stihnout	k5eAaPmAgNnP	stihnout
odhalit	odhalit	k5eAaPmF	odhalit
slabá	slabý	k2eAgNnPc1d1	slabé
konstrukční	konstrukční	k2eAgNnPc1d1	konstrukční
místa	místo	k1gNnPc1	místo
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
technická	technický	k2eAgNnPc1d1	technické
řešení	řešení	k1gNnPc1	řešení
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
výrazně	výrazně	k6eAd1	výrazně
modernizovaný	modernizovaný	k2eAgInSc1d1	modernizovaný
motor	motor	k1gInSc1	motor
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
uplatněna	uplatnit	k5eAaPmNgFnS	uplatnit
i	i	k9	i
u	u	k7c2	u
modelů	model	k1gInPc2	model
s	s	k7c7	s
motorem	motor	k1gInSc7	motor
vzadu	vzadu	k6eAd1	vzadu
(	(	kIx(	(
<g/>
Škoda	škoda	k1gFnSc1	škoda
135	[number]	k4	135
<g/>
/	/	kIx~	/
<g/>
136	[number]	k4	136
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Organizační	organizační	k2eAgInPc1d1	organizační
a	a	k8xC	a
kvalitářské	kvalitářský	k2eAgInPc1d1	kvalitářský
problémy	problém	k1gInPc1	problém
vedly	vést	k5eAaImAgInP	vést
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
k	k	k7c3	k
odvolání	odvolání	k1gNnSc3	odvolání
ředitele	ředitel	k1gMnSc2	ředitel
Banýra	Banýr	k1gMnSc2	Banýr
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
podle	podle	k7c2	podle
nového	nový	k2eAgInSc2d1	nový
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
státním	státní	k2eAgInSc6d1	státní
podniku	podnik	k1gInSc6	podnik
byly	být	k5eAaImAgFnP	být
uspořádány	uspořádat	k5eAaPmNgFnP	uspořádat
volby	volba	k1gFnPc1	volba
nového	nový	k2eAgMnSc2d1	nový
ředitele	ředitel	k1gMnSc2	ředitel
AZNP	AZNP	kA	AZNP
<g/>
.	.	kIx.	.
</s>
<s>
Zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
Petr	Petr	k1gMnSc1	Petr
Dědek	Dědek	k1gMnSc1	Dědek
<g/>
,	,	kIx,	,
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
podnikový	podnikový	k2eAgMnSc1d1	podnikový
ředitel	ředitel	k1gMnSc1	ředitel
n.	n.	k?	n.
<g/>
p.	p.	k?	p.
LIAZ	liaz	k1gInSc1	liaz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
svým	svůj	k3xOyFgInSc7	svůj
nekompromisním	kompromisní	k2eNgInSc7d1	nekompromisní
stylem	styl	k1gInSc7	styl
řízení	řízení	k1gNnSc2	řízení
zvedl	zvednout	k5eAaPmAgMnS	zvednout
kvalitu	kvalita	k1gFnSc4	kvalita
řízení	řízení	k1gNnSc2	řízení
i	i	k8xC	i
výroby	výroba	k1gFnSc2	výroba
během	během	k7c2	během
několika	několik	k4yIc2	několik
měsíců	měsíc	k1gInPc2	měsíc
na	na	k7c4	na
žádoucí	žádoucí	k2eAgFnSc4d1	žádoucí
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
Favorita	favorit	k1gMnSc2	favorit
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
pořízením	pořízení	k1gNnSc7	pořízení
nových	nový	k2eAgFnPc2d1	nová
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
však	však	k9	však
přišel	přijít	k5eAaPmAgMnS	přijít
AZNP	AZNP	kA	AZNP
na	na	k7c4	na
cca	cca	kA	cca
2,5	[number]	k4	2,5
miliardy	miliarda	k4xCgFnSc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
poklesem	pokles	k1gInSc7	pokles
prodejů	prodej	k1gInPc2	prodej
typu	typ	k1gInSc2	typ
105	[number]	k4	105
<g/>
/	/	kIx~	/
<g/>
120	[number]	k4	120
<g/>
/	/	kIx~	/
<g/>
130	[number]	k4	130
přineslo	přinést	k5eAaPmAgNnS	přinést
automobilce	automobilka	k1gFnSc6	automobilka
vážné	vážný	k2eAgInPc4d1	vážný
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
problémy	problém	k1gInPc4	problém
spojené	spojený	k2eAgInPc4d1	spojený
s	s	k7c7	s
nedostatkem	nedostatek	k1gInSc7	nedostatek
dalších	další	k2eAgFnPc2d1	další
investic	investice	k1gFnPc2	investice
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
na	na	k7c6	na
dokončení	dokončení	k1gNnSc6	dokončení
vývoje	vývoj	k1gInSc2	vývoj
a	a	k8xC	a
začátek	začátek	k1gInSc4	začátek
výroby	výroba	k1gFnSc2	výroba
verzí	verze	k1gFnPc2	verze
kombi	kombi	k1gNnSc1	kombi
a	a	k8xC	a
pick-up	pickp	k1gInSc1	pick-up
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politické	politický	k2eAgFnPc1d1	politická
změny	změna	k1gFnPc1	změna
a	a	k8xC	a
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
amnestie	amnestie	k1gFnSc1	amnestie
pak	pak	k6eAd1	pak
znamenala	znamenat	k5eAaImAgFnS	znamenat
i	i	k9	i
výrazné	výrazný	k2eAgInPc4d1	výrazný
provozní	provozní	k2eAgInPc4d1	provozní
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Automobilka	automobilka	k1gFnSc1	automobilka
i	i	k8xC	i
politici	politik	k1gMnPc1	politik
proto	proto	k8xC	proto
hledali	hledat	k5eAaImAgMnP	hledat
vhodného	vhodný	k2eAgMnSc4d1	vhodný
zahraničního	zahraniční	k2eAgMnSc4d1	zahraniční
partnera	partner	k1gMnSc4	partner
–	–	k?	–
vláda	vláda	k1gFnSc1	vláda
Petra	Petra	k1gFnSc1	Petra
Pitharta	Pitharta	k1gFnSc1	Pitharta
dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1990	[number]	k4	1990
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
tímto	tento	k3xDgMnSc7	tento
partnerem	partner	k1gMnSc7	partner
bude	být	k5eAaImBp3nS	být
německý	německý	k2eAgInSc1d1	německý
koncern	koncern	k1gInSc1	koncern
Volkswagen	volkswagen	k1gInSc1	volkswagen
(	(	kIx(	(
<g/>
druhým	druhý	k4xOgNnSc7	druhý
finalistou	finalista	k1gMnSc7	finalista
byl	být	k5eAaImAgInS	být
Renault	renault	k1gInSc1	renault
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
dalších	další	k2eAgInPc2d1	další
22	[number]	k4	22
zájemců	zájemce	k1gMnPc2	zájemce
patřily	patřit	k5eAaImAgInP	patřit
např.	např.	kA	např.
firmy	firma	k1gFnSc2	firma
BMW	BMW	kA	BMW
<g/>
,	,	kIx,	,
Fiat	fiat	k1gInSc1	fiat
<g/>
,	,	kIx,	,
General	General	k1gFnSc1	General
<g />
.	.	kIx.	.
</s>
<s>
Motors	Motors	k1gInSc1	Motors
<g/>
)	)	kIx)	)
Koncern	koncern	k1gInSc1	koncern
Volkswagen	volkswagen	k1gInSc1	volkswagen
totiž	totiž	k9	totiž
přišel	přijít	k5eAaPmAgInS	přijít
s	s	k7c7	s
nejvíce	nejvíce	k6eAd1	nejvíce
velkorysou	velkorysý	k2eAgFnSc7d1	velkorysá
nabídkou	nabídka	k1gFnSc7	nabídka
<g/>
,	,	kIx,	,
příslibem	příslib	k1gInSc7	příslib
masivních	masivní	k2eAgFnPc2d1	masivní
investic	investice	k1gFnPc2	investice
o	o	k7c6	o
objemu	objem	k1gInSc6	objem
9	[number]	k4	9
miliard	miliarda	k4xCgFnPc2	miliarda
marek	marka	k1gFnPc2	marka
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
–	–	k?	–
na	na	k7c4	na
oddlužení	oddlužení	k1gNnSc4	oddlužení
Škody	škoda	k1gFnSc2	škoda
<g/>
,	,	kIx,	,
na	na	k7c4	na
modernizaci	modernizace	k1gFnSc4	modernizace
Favorita	favorit	k1gMnSc2	favorit
<g/>
,	,	kIx,	,
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
nových	nový	k2eAgInPc2d1	nový
modelů	model	k1gInPc2	model
<g/>
,	,	kIx,	,
na	na	k7c4	na
stavební	stavební	k2eAgFnPc4d1	stavební
a	a	k8xC	a
energetické	energetický	k2eAgFnPc4d1	energetická
rekonstrukce	rekonstrukce	k1gFnPc4	rekonstrukce
a	a	k8xC	a
na	na	k7c4	na
obnovu	obnova	k1gFnSc4	obnova
a	a	k8xC	a
doplnění	doplnění	k1gNnSc4	doplnění
technologií	technologie	k1gFnPc2	technologie
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
kapacita	kapacita	k1gFnSc1	kapacita
všech	všecek	k3xTgInPc2	všecek
tří	tři	k4xCgInPc2	tři
závodů	závod	k1gInPc2	závod
automobilky	automobilka	k1gFnSc2	automobilka
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
zdvojnásobila	zdvojnásobit	k5eAaPmAgFnS	zdvojnásobit
na	na	k7c4	na
cca	cca	kA	cca
400	[number]	k4	400
tisíc	tisíc	k4xCgInPc2	tisíc
vozů	vůz	k1gInPc2	vůz
ročně	ročně	k6eAd1	ročně
(	(	kIx(	(
<g/>
Renault	renault	k1gInSc1	renault
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgMnSc3	ten
nabízel	nabízet	k5eAaImAgInS	nabízet
ukončení	ukončení	k1gNnSc4	ukončení
výroby	výroba	k1gFnSc2	výroba
favoritů	favorit	k1gInPc2	favorit
a	a	k8xC	a
zavedení	zavedení	k1gNnSc4	zavedení
modelu	model	k1gInSc2	model
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
typu	typ	k1gInSc2	typ
R18	R18	k1gFnSc1	R18
nebo	nebo	k8xC	nebo
Twinga	Twinga	k1gFnSc1	Twinga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přejmenování	přejmenování	k1gNnSc3	přejmenování
automobilky	automobilka	k1gFnSc2	automobilka
a	a	k8xC	a
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
března	březen	k1gInSc2	březen
a	a	k8xC	a
dubna	duben	k1gInSc2	duben
1991	[number]	k4	1991
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Škoda	škoda	k1gFnSc1	škoda
<g/>
,	,	kIx,	,
automobilová	automobilový	k2eAgFnSc1d1	automobilová
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
Škody	škoda	k1gFnSc2	škoda
s	s	k7c7	s
koncernem	koncern	k1gInSc7	koncern
VW	VW	kA	VW
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Volkswagenu	volkswagen	k1gInSc6	volkswagen
připadl	připadnout	k5eAaPmAgInS	připadnout
podíl	podíl	k1gInSc1	podíl
30	[number]	k4	30
<g/>
%	%	kIx~	%
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Škoda	škoda	k1gFnSc1	škoda
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
stala	stát	k5eAaPmAgFnS	stát
čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
značkou	značka	k1gFnSc7	značka
koncernu	koncern	k1gInSc2	koncern
(	(	kIx(	(
<g/>
vedle	vedle	k7c2	vedle
značek	značka	k1gFnPc2	značka
VW	VW	kA	VW
<g/>
,	,	kIx,	,
Audi	Audi	k1gNnSc1	Audi
a	a	k8xC	a
Seat	Seat	k1gInSc1	Seat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
Volkswagen	volkswagen	k1gInSc1	volkswagen
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
svůj	svůj	k3xOyFgInSc4	svůj
akciový	akciový	k2eAgInSc4d1	akciový
podíl	podíl	k1gInSc4	podíl
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1994	[number]	k4	1994
na	na	k7c4	na
60,3	[number]	k4	60,3
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1995	[number]	k4	1995
na	na	k7c4	na
70	[number]	k4	70
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2000	[number]	k4	2000
už	už	k9	už
Volkswagen	volkswagen	k1gInSc1	volkswagen
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
100	[number]	k4	100
%	%	kIx~	%
podíl	podíl	k1gInSc4	podíl
Škody	škoda	k1gFnSc2	škoda
Auto	auto	k1gNnSc4	auto
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
podotknout	podotknout	k5eAaPmF	podotknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgInPc4	první
roky	rok	k1gInPc4	rok
pod	pod	k7c7	pod
VW	VW	kA	VW
nebyly	být	k5eNaImAgFnP	být
pro	pro	k7c4	pro
Škodu	škoda	k1gFnSc4	škoda
nijak	nijak	k6eAd1	nijak
přívětivé	přívětivý	k2eAgNnSc1d1	přívětivé
<g/>
,	,	kIx,	,
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
propouštění	propouštění	k1gNnSc3	propouštění
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
a	a	k8xC	a
hned	hned	k6eAd1	hned
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
byly	být	k5eAaImAgInP	být
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
odbytem	odbyt	k1gInSc7	odbyt
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Způsobily	způsobit	k5eAaPmAgInP	způsobit
to	ten	k3xDgNnSc1	ten
soukromé	soukromý	k2eAgInPc1d1	soukromý
dovozy	dovoz	k1gInPc1	dovoz
automobilů	automobil	k1gInPc2	automobil
ze	z	k7c2	z
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
celní	celní	k2eAgFnSc2d1	celní
či	či	k8xC	či
technické	technický	k2eAgFnSc2d1	technická
kontroly	kontrola	k1gFnSc2	kontrola
se	se	k3xPyFc4	se
dovážely	dovážet	k5eAaImAgFnP	dovážet
denně	denně	k6eAd1	denně
stovky	stovka	k1gFnPc1	stovka
ojetých	ojetý	k2eAgInPc2d1	ojetý
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
v	v	k7c6	v
technickém	technický	k2eAgInSc6d1	technický
stavu	stav	k1gInSc6	stav
blízkém	blízký	k2eAgInSc6d1	blízký
definici	definice	k1gFnSc4	definice
vraku	vrak	k1gInSc2	vrak
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
právě	právě	k6eAd1	právě
proto	proto	k8xC	proto
výrazně	výrazně	k6eAd1	výrazně
levnějších	levný	k2eAgInPc2d2	levnější
než	než	k8xS	než
nová	nový	k2eAgFnSc1d1	nová
škodovka	škodovka	k1gFnSc1	škodovka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
vyrobilo	vyrobit	k5eAaPmAgNnS	vyrobit
pouze	pouze	k6eAd1	pouze
170	[number]	k4	170
tisíc	tisíc	k4xCgInPc2	tisíc
automobilů	automobil	k1gInPc2	automobil
značky	značka	k1gFnSc2	značka
Škoda	škoda	k1gFnSc1	škoda
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
prodalo	prodat	k5eAaPmAgNnS	prodat
pouze	pouze	k6eAd1	pouze
27	[number]	k4	27
tisíc	tisíc	k4xCgInPc2	tisíc
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
slíbenými	slíbený	k2eAgFnPc7d1	slíbená
investicemi	investice	k1gFnPc7	investice
nastal	nastat	k5eAaPmAgInS	nastat
problém	problém	k1gInSc1	problém
v	v	k7c6	v
září	září	k1gNnSc6	září
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
když	když	k8xS	když
sesterský	sesterský	k2eAgInSc1d1	sesterský
podnik	podnik	k1gInSc1	podnik
SEAT	SEAT	kA	SEAT
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
oznámil	oznámit	k5eAaPmAgInS	oznámit
ztrátu	ztráta	k1gFnSc4	ztráta
1,5	[number]	k4	1,5
miliardy	miliarda	k4xCgFnPc4	miliarda
DM	dm	kA	dm
a	a	k8xC	a
vedení	vedení	k1gNnSc1	vedení
koncernu	koncern	k1gInSc2	koncern
vzápětí	vzápětí	k6eAd1	vzápětí
odřeklo	odřeknout	k5eAaPmAgNnS	odřeknout
plánovanou	plánovaný	k2eAgFnSc4d1	plánovaná
půjčku	půjčka	k1gFnSc4	půjčka
1,4	[number]	k4	1,4
miliard	miliarda	k4xCgFnPc2	miliarda
DM	dm	kA	dm
pro	pro	k7c4	pro
Škodu	škoda	k1gFnSc4	škoda
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
česká	český	k2eAgFnSc1d1	Česká
automobilka	automobilka	k1gFnSc1	automobilka
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
muset	muset	k5eAaImF	muset
začít	začít	k5eAaPmF	začít
při	při	k7c6	při
investicích	investice	k1gFnPc6	investice
víc	hodně	k6eAd2	hodně
spoléhat	spoléhat	k5eAaImF	spoléhat
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
sešlo	sejít	k5eAaPmAgNnS	sejít
například	například	k6eAd1	například
ze	z	k7c2	z
stavby	stavba	k1gFnSc2	stavba
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
nové	nový	k2eAgFnSc2d1	nová
motorárny	motorárna	k1gFnSc2	motorárna
a	a	k8xC	a
do	do	k7c2	do
budoucích	budoucí	k2eAgInPc2d1	budoucí
nových	nový	k2eAgInPc2d1	nový
modelů	model	k1gInPc2	model
se	se	k3xPyFc4	se
namísto	namísto	k7c2	namísto
připraveného	připravený	k2eAgInSc2d1	připravený
lehkého	lehký	k2eAgInSc2d1	lehký
moderního	moderní	k2eAgInSc2d1	moderní
motoru	motor	k1gInSc2	motor
Škoda	škoda	k1gFnSc1	škoda
790.16	[number]	k4	790.16
OHC	OHC	kA	OHC
musely	muset	k5eAaImAgInP	muset
montovat	montovat	k5eAaImF	montovat
obstarožní	obstarožní	k2eAgInPc1d1	obstarožní
litinové	litinový	k2eAgInPc1d1	litinový
motory	motor	k1gInPc1	motor
Volkswagen	volkswagen	k1gInSc4	volkswagen
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
Škoda	škoda	k1gFnSc1	škoda
byla	být	k5eAaImAgFnS	být
jediná	jediný	k2eAgFnSc1d1	jediná
značka	značka	k1gFnSc1	značka
koncernu	koncern	k1gInSc2	koncern
<g/>
,	,	kIx,	,
vykazující	vykazující	k2eAgMnSc1d1	vykazující
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
stabilní	stabilní	k2eAgInSc4d1	stabilní
růst	růst	k1gInSc4	růst
prodejů	prodej	k1gInPc2	prodej
i	i	k8xC	i
při	při	k7c6	při
zvyšující	zvyšující	k2eAgFnSc6d1	zvyšující
se	se	k3xPyFc4	se
prodejní	prodejní	k2eAgFnSc3d1	prodejní
ceně	cena	k1gFnSc3	cena
<g/>
.	.	kIx.	.
</s>
<s>
Objem	objem	k1gInSc1	objem
investic	investice	k1gFnPc2	investice
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
1995	[number]	k4	1995
přesto	přesto	k8xC	přesto
činil	činit	k5eAaImAgInS	činit
1,4	[number]	k4	1,4
miliardy	miliarda	k4xCgFnPc4	miliarda
DM	dm	kA	dm
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představovalo	představovat	k5eAaImAgNnS	představovat
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
všech	všecek	k3xTgFnPc2	všecek
přímých	přímý	k2eAgFnPc2d1	přímá
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
investic	investice	k1gFnPc2	investice
do	do	k7c2	do
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1994	[number]	k4	1994
oslavila	oslavit	k5eAaPmAgFnS	oslavit
Škoda	škoda	k6eAd1	škoda
miliontý	miliontý	k4xOgInSc4	miliontý
vyrobený	vyrobený	k2eAgInSc4d1	vyrobený
automobil	automobil	k1gInSc4	automobil
typové	typový	k2eAgFnSc2d1	typová
řady	řada	k1gFnSc2	řada
Favorit	favorit	k1gInSc1	favorit
a	a	k8xC	a
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1994	[number]	k4	1994
jej	on	k3xPp3gMnSc4	on
nahradila	nahradit	k5eAaPmAgFnS	nahradit
typem	typ	k1gInSc7	typ
Felicia	felicia	k1gFnSc1	felicia
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
které	který	k3yQgFnSc3	který
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
prodala	prodat	k5eAaPmAgFnS	prodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
tisíc	tisíc	k4xCgInPc2	tisíc
vozů	vůz	k1gInPc2	vůz
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
šly	jít	k5eAaImAgFnP	jít
na	na	k7c4	na
export	export	k1gInSc4	export
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
představovalo	představovat	k5eAaImAgNnS	představovat
4,2	[number]	k4	4,2
<g />
.	.	kIx.	.
</s>
<s>
%	%	kIx~	%
celkového	celkový	k2eAgInSc2d1	celkový
exportu	export	k1gInSc2	export
z	z	k7c2	z
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
definitivně	definitivně	k6eAd1	definitivně
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
překonána	překonat	k5eAaPmNgFnS	překonat
krize	krize	k1gFnSc1	krize
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1995	[number]	k4	1995
byl	být	k5eAaImAgInS	být
položen	položit	k5eAaPmNgInS	položit
základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
nového	nový	k2eAgInSc2d1	nový
montážního	montážní	k2eAgInSc2d1	montážní
závodu	závod	k1gInSc2	závod
v	v	k7c6	v
Mladé	mladý	k2eAgFnSc6d1	mladá
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
začal	začít	k5eAaPmAgMnS	začít
montovat	montovat	k5eAaImF	montovat
nový	nový	k2eAgInSc4d1	nový
osobní	osobní	k2eAgInSc4d1	osobní
automobil	automobil	k1gInSc4	automobil
Škoda	škoda	k1gFnSc1	škoda
Octavia	octavia	k1gFnSc1	octavia
<g/>
.	.	kIx.	.
</s>
<s>
Automobily	automobil	k1gInPc1	automobil
Škoda	škoda	k1gFnSc1	škoda
se	se	k3xPyFc4	se
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
úspěšně	úspěšně	k6eAd1	úspěšně
prodávají	prodávat	k5eAaImIp3nP	prodávat
na	na	k7c6	na
trzích	trh	k1gInPc6	trh
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Automobilka	automobilka	k1gFnSc1	automobilka
provozuje	provozovat	k5eAaImIp3nS	provozovat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
(	(	kIx(	(
<g/>
ŠAVŠ	ŠAVŠ	kA	ŠAVŠ
<g/>
)	)	kIx)	)
a	a	k8xC	a
odborná	odborný	k2eAgNnPc4d1	odborné
učiliště	učiliště	k1gNnPc4	učiliště
<g/>
.	.	kIx.	.
</s>
<s>
Automobily	automobil	k1gInPc1	automobil
Škoda	škoda	k1gFnSc1	škoda
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
automobilových	automobilový	k2eAgFnPc2d1	automobilová
soutěží	soutěž	k1gFnPc2	soutěž
rallye	rallye	k1gFnSc2	rallye
(	(	kIx(	(
<g/>
se	s	k7c7	s
závodním	závodní	k2eAgInSc7d1	závodní
speciálem	speciál	k1gInSc7	speciál
Fabia	fabia	k1gFnSc1	fabia
S	s	k7c7	s
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2006	[number]	k4	2006
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
Škoda	škoda	k1gFnSc1	škoda
Auto	auto	k1gNnSc4	auto
desetimiliontý	desetimiliontý	k2eAgInSc1d1	desetimiliontý
vůz	vůz	k1gInSc1	vůz
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
značky	značka	k1gFnSc2	značka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
vzrostly	vzrůst	k5eAaPmAgFnP	vzrůst
prodeje	prodej	k1gFnPc1	prodej
Škody	škoda	k1gFnSc2	škoda
o	o	k7c4	o
12,7	[number]	k4	12,7
<g/>
%	%	kIx~	%
a	a	k8xC	a
prodala	prodat	k5eAaPmAgFnS	prodat
tak	tak	k9	tak
rekordních	rekordní	k2eAgInPc2d1	rekordní
1	[number]	k4	1
037	[number]	k4	037
200	[number]	k4	200
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Tržby	tržba	k1gFnPc1	tržba
vzrostly	vzrůst	k5eAaPmAgFnP	vzrůst
o	o	k7c4	o
14	[number]	k4	14
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Škoda	škoda	k1gFnSc1	škoda
Auto	auto	k1gNnSc1	auto
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
představila	představit	k5eAaPmAgFnS	představit
velké	velká	k1gFnPc4	velká
SUV	SUV	kA	SUV
Škoda	škoda	k1gFnSc1	škoda
Kodiaq	Kodiaq	k1gFnSc1	Kodiaq
<g/>
.	.	kIx.	.
</s>
<s>
Předsedou	předseda	k1gMnSc7	předseda
představenstva	představenstvo	k1gNnSc2	představenstvo
společnosti	společnost	k1gFnSc2	společnost
Škoda	Škoda	k1gMnSc1	Škoda
Auto	auto	k1gNnSc1	auto
a.s.	a.s.	k?	a.s.
a	a	k8xC	a
jejím	její	k3xOp3gMnSc7	její
výkonným	výkonný	k2eAgMnSc7d1	výkonný
ředitelem	ředitel	k1gMnSc7	ředitel
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
září	září	k1gNnSc4	září
2010	[number]	k4	2010
německý	německý	k2eAgInSc1d1	německý
manažer	manažer	k1gInSc1	manažer
Winfried	Winfried	k1gMnSc1	Winfried
Vahland	Vahland	k1gInSc1	Vahland
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
následků	následek	k1gInPc2	následek
tzv.	tzv.	kA	tzv.
výfukového	výfukový	k2eAgInSc2d1	výfukový
skandálu	skandál	k1gInSc2	skandál
(	(	kIx(	(
<g/>
Dieselgate	Dieselgat	k1gInSc5	Dieselgat
<g/>
)	)	kIx)	)
ve	v	k7c4	v
Volkswagen	volkswagen	k1gInSc4	volkswagen
Group	Group	k1gInSc4	Group
bylo	být	k5eAaImAgNnS	být
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
nového	nový	k2eAgNnSc2d1	nové
vedení	vedení	k1gNnSc2	vedení
koncernu	koncern	k1gInSc2	koncern
Volkswagen	volkswagen	k1gInSc1	volkswagen
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
předsedou	předseda	k1gMnSc7	předseda
představenstva	představenstvo	k1gNnSc2	představenstvo
Matthiasem	Matthias	k1gMnSc7	Matthias
Müllerem	Müller	k1gMnSc7	Müller
<g/>
,	,	kIx,	,
že	že	k8xS	že
Winfried	Winfried	k1gInSc1	Winfried
Vahland	Vahlanda	k1gFnPc2	Vahlanda
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
stát	stát	k5eAaPmF	stát
generálním	generální	k2eAgMnSc7d1	generální
ředitelem	ředitel	k1gMnSc7	ředitel
velké	velký	k2eAgFnSc2d1	velká
pobočky	pobočka	k1gFnSc2	pobočka
koncernu	koncern	k1gInSc2	koncern
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
by	by	kYmCp3nS	by
zastupoval	zastupovat	k5eAaImAgInS	zastupovat
koncern	koncern	k1gInSc1	koncern
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
severoamerickém	severoamerický	k2eAgInSc6d1	severoamerický
regionu	region	k1gInSc6	region
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgInSc2	který
by	by	kYmCp3nP	by
patřily	patřit	k5eAaImAgFnP	patřit
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
a	a	k8xC	a
Kanada	Kanada	k1gFnSc1	Kanada
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
tam	tam	k6eAd1	tam
působícími	působící	k2eAgInPc7d1	působící
výrobními	výrobní	k2eAgInPc7d1	výrobní
podniky	podnik	k1gInPc7	podnik
Volkswagenu	volkswagen	k1gInSc2	volkswagen
<g/>
.	.	kIx.	.
</s>
<s>
Vahland	Vahland	k1gInSc1	Vahland
však	však	k9	však
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2015	[number]	k4	2015
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejen	nejen	k6eAd1	nejen
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
nenastoupí	nastoupit	k5eNaPmIp3nP	nastoupit
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
že	že	k8xS	že
zcela	zcela	k6eAd1	zcela
odchází	odcházet	k5eAaImIp3nS	odcházet
od	od	k7c2	od
Volkswagen	volkswagen	k1gInSc4	volkswagen
Group	Group	k1gInSc1	Group
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgMnSc7d1	nový
předsedou	předseda	k1gMnSc7	předseda
představenstva	představenstvo	k1gNnSc2	představenstvo
Škody	škoda	k1gFnSc2	škoda
Auto	auto	k1gNnSc1	auto
a.s.	a.s.	k?	a.s.
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
stal	stát	k5eAaPmAgMnS	stát
německý	německý	k2eAgMnSc1d1	německý
manažer	manažer	k1gMnSc1	manažer
Bernhard	Bernhard	k1gMnSc1	Bernhard
Maier	Maier	k1gMnSc1	Maier
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
vůz	vůz	k1gInSc1	vůz
Škoda	Škoda	k1gMnSc1	Škoda
Superb	Superb	k1gMnSc1	Superb
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
prestižním	prestižní	k2eAgInSc7d1	prestižní
Top	topit	k5eAaImRp2nS	topit
Gear	Gear	k1gInSc1	Gear
Magazinem	Magazin	k1gInSc7	Magazin
titulem	titul	k1gInSc7	titul
Luxury	Luxura	k1gFnPc1	Luxura
Car	car	k1gMnSc1	car
of	of	k?	of
the	the	k?	the
Year	Year	k1gInSc1	Year
(	(	kIx(	(
<g/>
Luxusní	luxusní	k2eAgNnSc1d1	luxusní
auto	auto	k1gNnSc1	auto
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
a	a	k8xC	a
Škoda	Škoda	k1gMnSc1	Škoda
Yeti	yeti	k1gMnSc1	yeti
titulem	titul	k1gInSc7	titul
Family	Famila	k1gFnPc1	Famila
Car	car	k1gMnSc1	car
of	of	k?	of
the	the	k?	the
Year	Year	k1gInSc1	Year
(	(	kIx(	(
<g/>
Rodinné	rodinný	k2eAgNnSc1d1	rodinné
auto	auto	k1gNnSc1	auto
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Škoda	škoda	k1gFnSc1	škoda
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
ocenění	ocenění	k1gNnPc2	ocenění
(	(	kIx(	(
<g/>
již	již	k9	již
po	po	k7c4	po
11	[number]	k4	11
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgNnSc6d1	švýcarské
Auto	auto	k1gNnSc1	auto
Illustrierte	Illustriert	k1gInSc5	Illustriert
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgNnPc4d3	nejlepší
auta	auto	k1gNnPc4	auto
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
cena	cena	k1gFnSc1	cena
<g/>
/	/	kIx~	/
<g/>
kvalita	kvalita	k1gFnSc1	kvalita
a	a	k8xC	a
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
časopise	časopis	k1gInSc6	časopis
Auto	auto	k1gNnSc1	auto
Motor	motor	k1gInSc1	motor
und	und	k?	und
Sport	sport	k1gInSc1	sport
získal	získat	k5eAaPmAgMnS	získat
Superb	Superb	k1gMnSc1	Superb
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
nejlepších	dobrý	k2eAgNnPc2d3	nejlepší
aut	auto	k1gNnPc2	auto
2012	[number]	k4	2012
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
22	[number]	k4	22
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
dovezených	dovezený	k2eAgInPc2d1	dovezený
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
Škoda	škoda	k1gFnSc1	škoda
zvolena	zvolit	k5eAaPmNgFnS	zvolit
nejspolehlivější	spolehlivý	k2eAgFnSc7d3	nejspolehlivější
značkou	značka	k1gFnSc7	značka
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
byla	být	k5eAaImAgFnS	být
celkem	celkem	k6eAd1	celkem
čtrnáctkrát	čtrnáctkrát	k6eAd1	čtrnáctkrát
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
českou	český	k2eAgFnSc7d1	Česká
firmou	firma	k1gFnSc7	firma
roku	rok	k1gInSc2	rok
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
Českých	český	k2eAgInPc2d1	český
100	[number]	k4	100
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
umístění	umístění	k1gNnSc4	umístění
vozů	vůz	k1gInPc2	vůz
značky	značka	k1gFnSc2	značka
Škoda	škoda	k1gFnSc1	škoda
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Evropské	evropský	k2eAgNnSc1d1	Evropské
auto	auto	k1gNnSc1	auto
roku	rok	k1gInSc2	rok
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
ŠKODA	škoda	k6eAd1	škoda
AUTO	auto	k1gNnSc1	auto
a.s.	a.s.	k?	a.s.
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
8	[number]	k4	8
modelových	modelový	k2eAgFnPc2d1	modelová
řad	řada	k1gFnPc2	řada
<g/>
:	:	kIx,	:
</s>
<s>
Citigo	Citigo	k1gMnSc1	Citigo
–	–	k?	–
miniautomobil	miniautomobit	k5eAaImAgMnS	miniautomobit
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
</s>
<s>
Fabia	fabia	k1gFnSc1	fabia
–	–	k?	–
malý	malý	k2eAgInSc4d1	malý
automobil	automobil	k1gInSc4	automobil
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
(	(	kIx(	(
<g/>
současná	současný	k2eAgFnSc1d1	současná
III	III	kA	III
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
od	od	k7c2	od
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
<s>
Rapid	rapid	k1gInSc1	rapid
/	/	kIx~	/
Rapid	rapid	k1gInSc1	rapid
(	(	kIx(	(
<g/>
Indie	Indie	k1gFnSc1	Indie
<g/>
)	)	kIx)	)
–	–	k?	–
nižší	nízký	k2eAgFnSc7d2	nižší
střední	střední	k2eAgFnSc7d1	střední
<g />
.	.	kIx.	.
</s>
<s>
třída	třída	k1gFnSc1	třída
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
</s>
<s>
Octavia	octavia	k1gFnSc1	octavia
–	–	k?	–
nižší	nízký	k2eAgFnSc1d2	nižší
střední	střední	k2eAgFnSc1d1	střední
třída	třída	k1gFnSc1	třída
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
(	(	kIx(	(
<g/>
současná	současný	k2eAgFnSc1d1	současná
III	III	kA	III
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
<s>
Superb	Superb	k1gInSc1	Superb
–	–	k?	–
střední	střední	k2eAgFnSc1d1	střední
třída	třída	k1gFnSc1	třída
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
(	(	kIx(	(
<g/>
současná	současný	k2eAgFnSc1d1	současná
III	III	kA	III
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
<s>
Yeti	yeti	k1gMnSc1	yeti
–	–	k?	–
kompaktní	kompaktní	k2eAgMnSc1d1	kompaktní
SUV	SUV	kA	SUV
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
</s>
<s>
Kodiaq	Kodiaq	k1gFnSc2	Kodiaq
–	–	k?	–
SUV	SUV	kA	SUV
<g/>
,	,	kIx,	,
uveden	uveden	k2eAgInSc1d1	uveden
do	do	k7c2	do
prodeje	prodej	k1gInSc2	prodej
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
</s>
<s>
Karoq	Karoq	k1gFnSc2	Karoq
–	–	k?	–
SUV	SUV	kA	SUV
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
–	–	k?	–
Slavia	Slavia	k1gFnSc1	Slavia
–	–	k?	–
bicykl	bicykl	k1gInSc1	bicykl
<g/>
)	)	kIx)	)
1905	[number]	k4	1905
–	–	k?	–
Laurin	Laurin	k1gInSc1	Laurin
&	&	k?	&
Klement	Klement	k1gMnSc1	Klement
A	A	kA	A
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Voiturette	Voiturett	k1gMnSc5	Voiturett
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1906	[number]	k4	1906
–	–	k?	–
Laurin	Laurin	k1gInSc1	Laurin
&	&	k?	&
Klement	Klement	k1gMnSc1	Klement
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
,	,	kIx,	,
E	E	kA	E
1907	[number]	k4	1907
–	–	k?	–
Laurin	Laurin	k1gInSc1	Laurin
&	&	k?	&
Klement	Klement	k1gMnSc1	Klement
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
F	F	kA	F
<g/>
,	,	kIx,	,
FF	ff	kA	ff
<g/>
,	,	kIx,	,
FC	FC	kA	FC
<g/>
,	,	kIx,	,
HO	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
HL	HL	kA	HL
1908	[number]	k4	1908
–	–	k?	–
Laurin	Laurin	k1gInSc1	Laurin
<g />
.	.	kIx.	.
</s>
<s>
&	&	k?	&
Klement	Klement	k1gMnSc1	Klement
BS	BS	kA	BS
<g/>
,	,	kIx,	,
FCS	FCS	kA	FCS
<g/>
,	,	kIx,	,
G	G	kA	G
1909	[number]	k4	1909
–	–	k?	–
Laurin	Laurin	k1gInSc1	Laurin
&	&	k?	&
Klement	Klement	k1gMnSc1	Klement
EN	EN	kA	EN
<g/>
,	,	kIx,	,
DL	DL	kA	DL
<g/>
,	,	kIx,	,
DO	do	k7c2	do
<g/>
,	,	kIx,	,
FDL	FDL	kA	FDL
<g/>
,	,	kIx,	,
FDO	FDO	kA	FDO
<g/>
,	,	kIx,	,
FN	FN	kA	FN
<g/>
,	,	kIx,	,
GDV	GDV	kA	GDV
<g/>
,	,	kIx,	,
RC	RC	kA	RC
<g/>
,	,	kIx,	,
FCR	FCR	kA	FCR
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
,	,	kIx,	,
LO	LO	kA	LO
1910	[number]	k4	1910
–	–	k?	–
Laurin	Laurin	k1gInSc1	Laurin
&	&	k?	&
Klement	Klement	k1gMnSc1	Klement
<g />
.	.	kIx.	.
</s>
<s>
ENS	ENS	kA	ENS
1911	[number]	k4	1911
–	–	k?	–
L	L	kA	L
<g/>
&	&	k?	&
<g/>
K	k	k7c3	k
K	K	kA	K
<g/>
,	,	kIx,	,
Kb	kb	kA	kb
<g/>
,	,	kIx,	,
LOKb	LOKb	k1gMnSc1	LOKb
<g/>
,	,	kIx,	,
LK	LK	kA	LK
<g/>
,	,	kIx,	,
S	s	k7c7	s
<g/>
,	,	kIx,	,
Sa	Sa	k1gFnSc1	Sa
1912	[number]	k4	1912
–	–	k?	–
L	L	kA	L
<g/>
&	&	k?	&
<g/>
K	k	k7c3	k
DN	DN	kA	DN
<g/>
,	,	kIx,	,
RK	RK	kA	RK
<g/>
,	,	kIx,	,
Sb	sb	kA	sb
<g/>
,	,	kIx,	,
Sc	Sc	k1gFnSc1	Sc
1913	[number]	k4	1913
–	–	k?	–
L	L	kA	L
<g/>
&	&	k?	&
<g/>
K	K	kA	K
M	M	kA	M
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Mb	Mb	k1gFnPc7	Mb
<g/>
,	,	kIx,	,
MO	MO	kA	MO
<g/>
,	,	kIx,	,
MK	MK	kA	MK
<g/>
,	,	kIx,	,
400	[number]	k4	400
<g/>
,	,	kIx,	,
O	O	kA	O
<g/>
,	,	kIx,	,
OK	oka	k1gFnPc2	oka
<g/>
,	,	kIx,	,
Sd	Sd	k1gFnPc2	Sd
<g/>
,	,	kIx,	,
Se	s	k7c7	s
<g/>
,	,	kIx,	,
Sg	Sg	k1gFnPc7	Sg
<g/>
,	,	kIx,	,
Sk	Sk	kA	Sk
1914	[number]	k4	1914
–	–	k?	–
Laurin	Laurin	k1gInSc1	Laurin
&	&	k?	&
Klement	Klement	k1gMnSc1	Klement
T	T	kA	T
/	/	kIx~	/
Ta	ten	k3xDgFnSc1	ten
Ms	Ms	k1gFnSc1	Ms
<g/>
,	,	kIx,	,
Sh	Sh	k1gFnSc1	Sh
<g/>
,	,	kIx,	,
Sk	Sk	kA	Sk
1916	[number]	k4	1916
–	–	k?	–
L	L	kA	L
<g/>
&	&	k?	&
<g/>
<g />
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
Sl	Sl	k1gMnPc3	Sl
<g/>
,	,	kIx,	,
Sm	Sm	k1gFnSc3	Sm
<g/>
,	,	kIx,	,
So	So	kA	So
<g/>
,	,	kIx,	,
200	[number]	k4	200
<g/>
,	,	kIx,	,
205	[number]	k4	205
1917	[number]	k4	1917
–	–	k?	–
L	L	kA	L
<g/>
&	&	k?	&
<g/>
K	k	k7c3	k
Md	Md	k1gFnPc3	Md
<g/>
,	,	kIx,	,
Me	Me	k1gMnSc1	Me
<g/>
,	,	kIx,	,
Mf	Mf	k1gMnSc1	Mf
<g/>
,	,	kIx,	,
Mg	mg	kA	mg
<g/>
,	,	kIx,	,
Mh	Mh	k1gFnPc4	Mh
<g/>
,	,	kIx,	,
Mi	já	k3xPp1nSc3	já
<g/>
,	,	kIx,	,
Ml	ml	kA	ml
<g/>
,	,	kIx,	,
300	[number]	k4	300
<g/>
,	,	kIx,	,
305	[number]	k4	305
1920	[number]	k4	1920
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
L	L	kA	L
<g/>
&	&	k?	&
<g/>
K	k	k7c3	k
MS	MS	kA	MS
<g/>
,	,	kIx,	,
540	[number]	k4	540
<g/>
,	,	kIx,	,
545	[number]	k4	545
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
&	&	k?	&
<g/>
K	k	k7c3	k
–	–	k?	–
Škoda	škoda	k6eAd1	škoda
545	[number]	k4	545
1921	[number]	k4	1921
–	–	k?	–
Laurin	Laurin	k1gInSc1	Laurin
&	&	k?	&
Klement	Klement	k1gMnSc1	Klement
MK6	MK6	k1gMnSc1	MK6
/	/	kIx~	/
445	[number]	k4	445
/	/	kIx~	/
450	[number]	k4	450
1922	[number]	k4	1922
–	–	k?	–
Laurin	Laurin	k1gInSc1	Laurin
&	&	k?	&
Klement	Klement	k1gMnSc1	Klement
A	A	kA	A
/	/	kIx~	/
100	[number]	k4	100
1923	[number]	k4	1923
–	–	k?	–
Laurin	Laurin	k1gInSc1	Laurin
&	&	k?	&
Klement	Klement	k1gMnSc1	Klement
105	[number]	k4	105
500	[number]	k4	500
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
505	[number]	k4	505
<g/>
,	,	kIx,	,
Sp	Sp	k1gFnSc1	Sp
<g/>
,	,	kIx,	,
210	[number]	k4	210
<g/>
,	,	kIx,	,
150	[number]	k4	150
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
&	&	k?	&
<g/>
K	K	kA	K
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
500	[number]	k4	500
<g/>
,	,	kIx,	,
505	[number]	k4	505
<g/>
,	,	kIx,	,
505	[number]	k4	505
N	N	kA	N
<g/>
,	,	kIx,	,
150	[number]	k4	150
1925	[number]	k4	1925
–	–	k?	–
Laurin	Laurin	k1gInSc1	Laurin
&	&	k?	&
Klement	Klement	k1gMnSc1	Klement
350	[number]	k4	350
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
120	[number]	k4	120
<g/>
,	,	kIx,	,
110	[number]	k4	110
<g/>
,	,	kIx,	,
115	[number]	k4	115
<g/>
,	,	kIx,	,
350	[number]	k4	350
1926	[number]	k4	1926
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Laurin	Laurin	k1gInSc1	Laurin
&	&	k?	&
Klement	Klement	k1gMnSc1	Klement
360	[number]	k4	360
L	L	kA	L
<g/>
&	&	k?	&
<g/>
K	K	kA	K
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
550	[number]	k4	550
<g/>
,	,	kIx,	,
550	[number]	k4	550
N	N	kA	N
<g/>
,	,	kIx,	,
Škoda	škoda	k1gFnSc1	škoda
–	–	k?	–
Hispano	Hispana	k1gFnSc5	Hispana
Suiza	Suiz	k1gMnSc4	Suiz
1927	[number]	k4	1927
–	–	k?	–
L	L	kA	L
<g/>
&	&	k?	&
<g/>
K	k	k7c3	k
–	–	k?	–
Škoda	škoda	k6eAd1	škoda
125	[number]	k4	125
1928	[number]	k4	1928
–	–	k?	–
Škoda	Škoda	k1gMnSc1	Škoda
4	[number]	k4	4
R	R	kA	R
1929	[number]	k4	1929
–	–	k?	–
Škoda	Škoda	k1gMnSc1	Škoda
6	[number]	k4	6
R	R	kA	R
<g/>
,	,	kIx,	,
430	[number]	k4	430
<g/>
,	,	kIx,	,
430	[number]	k4	430
D	D	kA	D
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
645	[number]	k4	645
<g/>
,	,	kIx,	,
860	[number]	k4	860
<g/>
,	,	kIx,	,
154	[number]	k4	154
<g/>
,	,	kIx,	,
104	[number]	k4	104
<g/>
,	,	kIx,	,
104	[number]	k4	104
<g/>
/	/	kIx~	/
<g/>
II	II	kA	II
<g/>
,	,	kIx,	,
304	[number]	k4	304
<g/>
,	,	kIx,	,
304	[number]	k4	304
N	N	kA	N
<g/>
,	,	kIx,	,
304	[number]	k4	304
ND	ND	kA	ND
<g/>
,	,	kIx,	,
306	[number]	k4	306
<g/>
,	,	kIx,	,
306	[number]	k4	306
N	N	kA	N
<g/>
,	,	kIx,	,
306	[number]	k4	306
ND	ND	kA	ND
<g/>
,	,	kIx,	,
306	[number]	k4	306
K	K	kA	K
<g/>
,	,	kIx,	,
306	[number]	k4	306
NK	NK	kA	NK
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
504	[number]	k4	504
<g/>
,	,	kIx,	,
506	[number]	k4	506
<g/>
,	,	kIx,	,
506	[number]	k4	506
N	N	kA	N
<g/>
,	,	kIx,	,
506	[number]	k4	506
ND	ND	kA	ND
1930	[number]	k4	1930
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
422	[number]	k4	422
<g/>
,	,	kIx,	,
206	[number]	k4	206
<g/>
,	,	kIx,	,
206	[number]	k4	206
D	D	kA	D
<g/>
,	,	kIx,	,
206	[number]	k4	206
DN	DN	kA	DN
<g/>
,	,	kIx,	,
404	[number]	k4	404
D	D	kA	D
<g/>
,	,	kIx,	,
404	[number]	k4	404
DN	DN	kA	DN
<g/>
,	,	kIx,	,
404	[number]	k4	404
DND	DND	kA	DND
<g/>
,	,	kIx,	,
404	[number]	k4	404
<g/>
,	,	kIx,	,
404	[number]	k4	404
N	N	kA	N
<g/>
,	,	kIx,	,
404	[number]	k4	404
Nd	Nd	k1gFnSc1	Nd
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
606	[number]	k4	606
D	D	kA	D
<g/>
,	,	kIx,	,
606	[number]	k4	606
DN	DN	kA	DN
<g/>
,	,	kIx,	,
606	[number]	k4	606
DND	DND	kA	DND
1931	[number]	k4	1931
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
633	[number]	k4	633
1932	[number]	k4	1932
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
637	[number]	k4	637
<g/>
,	,	kIx,	,
637	[number]	k4	637
D	D	kA	D
<g/>
,	,	kIx,	,
637	[number]	k4	637
K	K	kA	K
<g/>
,	,	kIx,	,
650	[number]	k4	650
<g/>
,	,	kIx,	,
932	[number]	k4	932
1933	[number]	k4	1933
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
420	[number]	k4	420
–	–	k?	–
Standard	standard	k1gInSc1	standard
1934	[number]	k4	1934
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
420	[number]	k4	420
–	–	k?	–
Rapid	rapid	k1gInSc1	rapid
<g/>
,	,	kIx,	,
418	[number]	k4	418
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Popular	Popular	k1gMnSc1	Popular
<g/>
,	,	kIx,	,
420	[number]	k4	420
–	–	k?	–
Popular	Popular	k1gMnSc1	Popular
<g/>
,	,	kIx,	,
640	[number]	k4	640
–	–	k?	–
Superb	Superb	k1gMnSc1	Superb
<g/>
,	,	kIx,	,
406	[number]	k4	406
D	D	kA	D
<g/>
,	,	kIx,	,
406	[number]	k4	406
DN	DN	kA	DN
<g/>
,	,	kIx,	,
406	[number]	k4	406
DND	DND	kA	DND
<g/>
,	,	kIx,	,
406	[number]	k4	406
<g/>
,	,	kIx,	,
406	[number]	k4	406
N	N	kA	N
<g/>
,	,	kIx,	,
406	[number]	k4	406
Nd	Nd	k1gFnPc2	Nd
<g/>
,	,	kIx,	,
656	[number]	k4	656
D	D	kA	D
1935	[number]	k4	1935
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
935	[number]	k4	935
<g/>
,	,	kIx,	,
Popular	Popular	k1gInSc1	Popular
Sport	sport	k1gInSc1	sport
–	–	k?	–
Monte	Mont	k1gMnSc5	Mont
Carlo	Carla	k1gMnSc5	Carla
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
typ	typ	k1gInSc1	typ
909	[number]	k4	909
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rapid	rapid	k1gInSc1	rapid
Six	Six	k1gFnSc2	Six
(	(	kIx(	(
<g/>
910	[number]	k4	910
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rapid	rapid	k1gInSc1	rapid
(	(	kIx(	(
<g/>
901	[number]	k4	901
<g/>
,	,	kIx,	,
914	[number]	k4	914
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
254	[number]	k4	254
D	D	kA	D
<g/>
,	,	kIx,	,
254	[number]	k4	254
G	G	kA	G
<g/>
,	,	kIx,	,
806	[number]	k4	806
D	D	kA	D
1936	[number]	k4	1936
–	–	k?	–
Škoda	Škoda	k1gMnSc1	Škoda
Favorit	favorit	k1gMnSc1	favorit
(	(	kIx(	(
<g/>
904	[number]	k4	904
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Favorit	favorit	k1gInSc1	favorit
2000	[number]	k4	2000
<g />
.	.	kIx.	.
</s>
<s>
OHV	OHV	kA	OHV
(	(	kIx(	(
<g/>
923	[number]	k4	923
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Superb	Superb	k1gMnSc1	Superb
(	(	kIx(	(
<g/>
902	[number]	k4	902
<g/>
,	,	kIx,	,
913	[number]	k4	913
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
903	[number]	k4	903
1937	[number]	k4	1937
–	–	k?	–
Škoda	Škoda	k1gMnSc1	Škoda
Sagitta	Sagitta	k1gMnSc1	Sagitta
(	(	kIx(	(
<g/>
911	[number]	k4	911
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Popular	Popular	k1gMnSc1	Popular
OHV	OHV	kA	OHV
(	(	kIx(	(
<g/>
912	[number]	k4	912
<g/>
)	)	kIx)	)
1938	[number]	k4	1938
–	–	k?	–
Škoda	Škoda	k1gMnSc1	Škoda
Popular	Popular	k1gMnSc1	Popular
1100	[number]	k4	1100
OHV	OHV	kA	OHV
(	(	kIx(	(
<g/>
927	[number]	k4	927
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Rapid	rapid	k1gInSc1	rapid
OHV	OHV	kA	OHV
(	(	kIx(	(
<g/>
922	[number]	k4	922
<g/>
,	,	kIx,	,
939	[number]	k4	939
<g/>
R	R	kA	R
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Superb	Superb	k1gMnSc1	Superb
OHV	OHV	kA	OHV
(	(	kIx(	(
<g/>
924	[number]	k4	924
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
532	[number]	k4	532
1939	[number]	k4	1939
–	–	k?	–
Škoda	Škoda	k1gMnSc1	Škoda
Popular	Popular	k1gMnSc1	Popular
995	[number]	k4	995
(	(	kIx(	(
<g/>
937	[number]	k4	937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Superb	Superb	k1gInSc1	Superb
4000	[number]	k4	4000
(	(	kIx(	(
<g/>
919	[number]	k4	919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
100	[number]	k4	100
(	(	kIx(	(
<g/>
942	[number]	k4	942
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
150	[number]	k4	150
(	(	kIx(	(
<g/>
943	[number]	k4	943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
256	[number]	k4	256
B	B	kA	B
<g/>
,	,	kIx,	,
256	[number]	k4	256
G	G	kA	G
<g/>
,	,	kIx,	,
536	[number]	k4	536
<g/>
,	,	kIx,	,
706	[number]	k4	706
<g/>
,	,	kIx,	,
706	[number]	k4	706
N	N	kA	N
<g/>
,	,	kIx,	,
706	[number]	k4	706
ND	ND	kA	ND
1940	[number]	k4	1940
–	–	k?	–
Škoda	Škoda	k1gMnSc1	Škoda
Popular	Popular	k1gMnSc1	Popular
1101	[number]	k4	1101
(	(	kIx(	(
<g/>
938	[number]	k4	938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
903	[number]	k4	903
1941	[number]	k4	1941
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
Rapid	rapid	k1gInSc1	rapid
2200	[number]	k4	2200
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
953	[number]	k4	953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Superb	Superb	k1gInSc1	Superb
3000	[number]	k4	3000
–	–	k?	–
KFZ	KFZ	kA	KFZ
15	[number]	k4	15
(	(	kIx(	(
<g/>
952	[number]	k4	952
<g/>
)	)	kIx)	)
1942	[number]	k4	1942
–	–	k?	–
Škoda	Škoda	k1gMnSc1	Škoda
RSO	RSO	kA	RSO
–	–	k?	–
Radschlepper	Radschlepper	k1gInSc1	Radschlepper
OST	OST	kA	OST
(	(	kIx(	(
<g/>
Porsche	Porsche	k1gNnSc4	Porsche
175	[number]	k4	175
<g/>
)	)	kIx)	)
1946	[number]	k4	1946
–	–	k?	–
Škoda	Škoda	k1gMnSc1	Škoda
706	[number]	k4	706
R	R	kA	R
<g/>
,	,	kIx,	,
706	[number]	k4	706
RO	RO	kA	RO
<g/>
,	,	kIx,	,
Tudor	tudor	k1gInSc1	tudor
1101	[number]	k4	1101
(	(	kIx(	(
<g/>
938	[number]	k4	938
<g/>
)	)	kIx)	)
1948	[number]	k4	1948
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Škoda	škoda	k1gFnSc1	škoda
VOS	vosa	k1gFnPc2	vosa
<g/>
,	,	kIx,	,
Škoda	škoda	k1gFnSc1	škoda
VOS-L	VOS-L	k1gFnSc1	VOS-L
1949	[number]	k4	1949
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
Sport	sport	k1gInSc1	sport
(	(	kIx(	(
<g/>
966	[number]	k4	966
<g/>
)	)	kIx)	)
1950	[number]	k4	1950
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
Supersport	supersport	k1gInSc1	supersport
(	(	kIx(	(
<g/>
966	[number]	k4	966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tudor	tudor	k1gInSc1	tudor
1102	[number]	k4	1102
1951	[number]	k4	1951
–	–	k?	–
Škoda	Škoda	k1gMnSc1	Škoda
MOŽ-2	MOŽ-2	k1gMnSc1	MOŽ-2
(	(	kIx(	(
<g/>
972	[number]	k4	972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tatra	Tatra	k1gFnSc1	Tatra
600	[number]	k4	600
(	(	kIx(	(
<g/>
Tatraplan	Tatraplana	k1gFnPc2	Tatraplana
<g/>
)	)	kIx)	)
1952	[number]	k4	1952
–	–	k?	–
Tatra	Tatra	k1gFnSc1	Tatra
805	[number]	k4	805
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Škoda	škoda	k1gFnSc1	škoda
973	[number]	k4	973
<g/>
,	,	kIx,	,
1200	[number]	k4	1200
(	(	kIx(	(
<g/>
955	[number]	k4	955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1201	[number]	k4	1201
(	(	kIx(	(
<g/>
980	[number]	k4	980
<g/>
)	)	kIx)	)
1955	[number]	k4	1955
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
440	[number]	k4	440
(	(	kIx(	(
<g/>
970	[number]	k4	970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
445	[number]	k4	445
(	(	kIx(	(
<g/>
983	[number]	k4	983
<g/>
)	)	kIx)	)
1957	[number]	k4	1957
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
450	[number]	k4	450
(	(	kIx(	(
<g/>
984	[number]	k4	984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1101	[number]	k4	1101
OHC	OHC	kA	OHC
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
968	[number]	k4	968
<g/>
)	)	kIx)	)
1959	[number]	k4	1959
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
Octavia	octavia	k1gFnSc1	octavia
(	(	kIx(	(
<g/>
985	[number]	k4	985
<g/>
,	,	kIx,	,
702	[number]	k4	702
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Octavia	octavia	k1gFnSc1	octavia
Super	super	k1gInSc2	super
(	(	kIx(	(
<g/>
993	[number]	k4	993
<g/>
,	,	kIx,	,
703	[number]	k4	703
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Felicia	felicia	k1gFnSc1	felicia
(	(	kIx(	(
<g/>
994	[number]	k4	994
<g/>
)	)	kIx)	)
1960	[number]	k4	1960
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
Octavia	octavia	k1gFnSc1	octavia
TS	ts	k0	ts
(	(	kIx(	(
<g/>
995	[number]	k4	995
<g/>
)	)	kIx)	)
1961	[number]	k4	1961
–	–	k?	–
Škoda	Škoda	k1gMnSc1	Škoda
<g />
.	.	kIx.	.
</s>
<s>
Octavia	octavia	k1gFnSc1	octavia
1200	[number]	k4	1200
TS	ts	k0	ts
(	(	kIx(	(
<g/>
999	[number]	k4	999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Škoda	škoda	k1gFnSc1	škoda
Octavia	octavia	k1gFnSc1	octavia
Combi	Comb	k1gFnSc2	Comb
(	(	kIx(	(
<g/>
993	[number]	k4	993
C	C	kA	C
<g/>
,	,	kIx,	,
703	[number]	k4	703
C	C	kA	C
<g/>
,	,	kIx,	,
704	[number]	k4	704
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Škoda	škoda	k1gFnSc1	škoda
1202	[number]	k4	1202
(	(	kIx(	(
<g/>
981	[number]	k4	981
<g/>
)	)	kIx)	)
1963	[number]	k4	1963
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
Felicia	felicia	k1gFnSc1	felicia
Super	super	k2eAgFnSc1d1	super
(	(	kIx(	(
<g/>
996	[number]	k4	996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
–	–	k?	–
Škoda	Škoda	k1gMnSc1	Škoda
<g />
.	.	kIx.	.
</s>
<s>
F	F	kA	F
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
Škoda	škoda	k1gFnSc1	škoda
1000	[number]	k4	1000
MB	MB	kA	MB
(	(	kIx(	(
<g/>
990	[number]	k4	990
<g/>
,	,	kIx,	,
721	[number]	k4	721
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1000	[number]	k4	1000
MB	MB	kA	MB
de	de	k?	de
Luxe	Lux	k1gMnSc2	Lux
(	(	kIx(	(
<g/>
990	[number]	k4	990
<g/>
,	,	kIx,	,
721	[number]	k4	721
<g/>
)	)	kIx)	)
1966	[number]	k4	1966
–	–	k?	–
Škoda	Škoda	k1gMnSc1	Škoda
1000	[number]	k4	1000
MBG	MBG	kA	MBG
de	de	k?	de
Luxe	Lux	k1gMnSc2	Lux
(	(	kIx(	(
<g/>
710	[number]	k4	710
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1000	[number]	k4	1000
MBX	MBX	kA	MBX
de	de	k?	de
Luxe	Lux	k1gMnSc2	Lux
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
990	[number]	k4	990
T	T	kA	T
<g/>
)	)	kIx)	)
1967	[number]	k4	1967
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
720	[number]	k4	720
<g/>
,	,	kIx,	,
Škoda	škoda	k1gFnSc1	škoda
1100	[number]	k4	1100
MB	MB	kA	MB
de	de	k?	de
Luxe	Lux	k1gMnSc2	Lux
(	(	kIx(	(
<g/>
715	[number]	k4	715
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1100	[number]	k4	1100
MBX	MBX	kA	MBX
de	de	k?	de
Luxe	Lux	k1gMnSc2	Lux
(	(	kIx(	(
<g/>
723	[number]	k4	723
T	T	kA	T
<g/>
)	)	kIx)	)
1968	[number]	k4	1968
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
1203	[number]	k4	1203
(	(	kIx(	(
<g/>
997	[number]	k4	997
<g/>
,	,	kIx,	,
776	[number]	k4	776
<g/>
)	)	kIx)	)
1969	[number]	k4	1969
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
100	[number]	k4	100
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
100	[number]	k4	100
L	L	kA	L
(	(	kIx(	(
<g/>
722	[number]	k4	722
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
110	[number]	k4	110
L	L	kA	L
(	(	kIx(	(
<g/>
717	[number]	k4	717
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
110	[number]	k4	110
LS	LS	kA	LS
(	(	kIx(	(
<g/>
719	[number]	k4	719
<g/>
)	)	kIx)	)
1970	[number]	k4	1970
–	–	k?	–
Škoda	Škoda	k1gMnSc1	Škoda
110	[number]	k4	110
R	R	kA	R
(	(	kIx(	(
<g/>
718	[number]	k4	718
<g/>
-K	-K	k?	-K
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Škoda	škoda	k1gFnSc1	škoda
1100	[number]	k4	1100
GT	GT	kA	GT
1971	[number]	k4	1971
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
120	[number]	k4	120
S	s	k7c7	s
<g/>
,	,	kIx,	,
120	[number]	k4	120
<g />
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Rallye	rallye	k1gFnSc7	rallye
(	(	kIx(	(
<g/>
728	[number]	k4	728
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Škoda	škoda	k1gFnSc1	škoda
110	[number]	k4	110
Super	super	k1gInPc2	super
Sport	sport	k1gInSc1	sport
1973	[number]	k4	1973
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
180	[number]	k4	180
<g/>
/	/	kIx~	/
<g/>
200	[number]	k4	200
RS	RS	kA	RS
(	(	kIx(	(
<g/>
734	[number]	k4	734
<g/>
)	)	kIx)	)
1975	[number]	k4	1975
–	–	k?	–
Škoda	Škoda	k1gMnSc1	Škoda
130	[number]	k4	130
RS	RS	kA	RS
(	(	kIx(	(
<g/>
735	[number]	k4	735
<g/>
)	)	kIx)	)
1976	[number]	k4	1976
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
742	[number]	k4	742
105	[number]	k4	105
S	s	k7c7	s
<g/>
,	,	kIx,	,
105	[number]	k4	105
L	L	kA	L
<g/>
,	,	kIx,	,
105	[number]	k4	105
<g />
.	.	kIx.	.
</s>
<s>
GL	GL	kA	GL
<g/>
,	,	kIx,	,
120	[number]	k4	120
<g/>
,	,	kIx,	,
120	[number]	k4	120
L	L	kA	L
<g/>
,	,	kIx,	,
120	[number]	k4	120
LE	LE	kA	LE
<g/>
,	,	kIx,	,
120	[number]	k4	120
LS	LS	kA	LS
<g/>
,	,	kIx,	,
120	[number]	k4	120
GLS	GLS	kA	GLS
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
Garde	garde	k1gFnSc1	garde
(	(	kIx(	(
<g/>
743	[number]	k4	743
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rapid	rapid	k1gInSc1	rapid
(	(	kIx(	(
<g/>
743	[number]	k4	743
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rapid	rapid	k1gInSc1	rapid
130	[number]	k4	130
(	(	kIx(	(
<g/>
747	[number]	k4	747
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
135	[number]	k4	135
Rapid	rapid	k1gInSc1	rapid
(	(	kIx(	(
<g/>
747	[number]	k4	747
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
136	[number]	k4	136
Rapid	rapid	k1gInSc1	rapid
(	(	kIx(	(
<g/>
747	[number]	k4	747
<g/>
)	)	kIx)	)
<g/>
,135	,135	k4	,135
<g/>
i	i	k8xC	i
Rapid	rapid	k1gInSc1	rapid
RiC	RiC	k1gFnSc1	RiC
1984	[number]	k4	1984
–	–	k?	–
Škoda	Škoda	k1gMnSc1	Škoda
(	(	kIx(	(
<g/>
742	[number]	k4	742
M	M	kA	M
<g/>
)	)	kIx)	)
105	[number]	k4	105
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
105	[number]	k4	105
<g/>
L	L	kA	L
<g/>
,	,	kIx,	,
105	[number]	k4	105
<g/>
SP	SP	kA	SP
<g/>
,	,	kIx,	,
105	[number]	k4	105
<g/>
GL	GL	kA	GL
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
120	[number]	k4	120
<g/>
L	L	kA	L
<g/>
,	,	kIx,	,
120	[number]	k4	120
<g/>
GL	GL	kA	GL
<g/>
,	,	kIx,	,
120	[number]	k4	120
<g/>
LS	LS	kA	LS
<g/>
,	,	kIx,	,
120	[number]	k4	120
<g/>
GLS	GLS	kA	GLS
<g/>
,	,	kIx,	,
120	[number]	k4	120
<g/>
LX	LX	kA	LX
<g/>
,	,	kIx,	,
125	[number]	k4	125
<g/>
L	L	kA	L
<g/>
,	,	kIx,	,
130	[number]	k4	130
LR	LR	kA	LR
<g/>
,	,	kIx,	,
130	[number]	k4	130
L	L	kA	L
<g/>
,	,	kIx,	,
130	[number]	k4	130
GL	GL	kA	GL
<g/>
,	,	kIx,	,
135	[number]	k4	135
L	L	kA	L
<g/>
,	,	kIx,	,
136	[number]	k4	136
L	L	kA	L
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
135	[number]	k4	135
GL	GL	kA	GL
<g/>
,135	,135	k4	,135
GLi	GLi	k1gFnPc2	GLi
<g/>
,	,	kIx,	,
136	[number]	k4	136
GL	GL	kA	GL
(	(	kIx(	(
<g/>
745	[number]	k4	745
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
120	[number]	k4	120
<g/>
,	,	kIx,	,
130	[number]	k4	130
L	L	kA	L
<g/>
,	,	kIx,	,
130	[number]	k4	130
GL	GL	kA	GL
,	,	kIx,	,
135	[number]	k4	135
L	L	kA	L
<g/>
,	,	kIx,	,
136	[number]	k4	136
L	L	kA	L
<g/>
,	,	kIx,	,
135	[number]	k4	135
GL	GL	kA	GL
<g/>
,	,	kIx,	,
135	[number]	k4	135
<g/>
GLi	GLi	k1gFnPc2	GLi
<g/>
,	,	kIx,	,
136	[number]	k4	136
GL	GL	kA	GL
1988	[number]	k4	1988
–	–	k?	–
Škoda	Škoda	k1gMnSc1	Škoda
Favorit	favorit	k1gMnSc1	favorit
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
781	[number]	k4	781
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
136	[number]	k4	136
L	L	kA	L
<g/>
,	,	kIx,	,
136	[number]	k4	136
LS	LS	kA	LS
<g/>
,	,	kIx,	,
135	[number]	k4	135
L	L	kA	L
<g/>
,	,	kIx,	,
135	[number]	k4	135
LS	LS	kA	LS
<g/>
)	)	kIx)	)
1991	[number]	k4	1991
–	–	k?	–
Škoda	Škoda	k1gMnSc1	Škoda
Favorit	favorit	k1gMnSc1	favorit
(	(	kIx(	(
<g/>
781	[number]	k4	781
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Škoda	Škoda	k1gMnSc1	Škoda
Forman	Forman	k1gMnSc1	Forman
(	(	kIx(	(
<g/>
785	[number]	k4	785
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
136	[number]	k4	136
L	L	kA	L
<g/>
,	,	kIx,	,
136	[number]	k4	136
LS	LS	kA	LS
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
135	[number]	k4	135
L	L	kA	L
<g/>
,	,	kIx,	,
135	[number]	k4	135
LS	LS	kA	LS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pick-up	Pickp	k1gMnSc1	Pick-up
<g/>
,	,	kIx,	,
Praktik	praktik	k1gMnSc1	praktik
<g/>
(	(	kIx(	(
<g/>
781	[number]	k4	781
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Van	van	k1gInSc1	van
Plus	plus	k1gInSc1	plus
1992	[number]	k4	1992
–	–	k?	–
Škoda	Škoda	k1gMnSc1	Škoda
Favorit	favorit	k1gMnSc1	favorit
(	(	kIx(	(
<g/>
781	[number]	k4	781
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Škoda	Škoda	k1gMnSc1	Škoda
Forman	Forman	k1gMnSc1	Forman
(	(	kIx(	(
<g/>
785	[number]	k4	785
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
135	[number]	k4	135
L	L	kA	L
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
LS	LS	kA	LS
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc1	Le
<g/>
,	,	kIx,	,
LSe	LSe	k1gMnSc1	LSe
<g/>
)	)	kIx)	)
1993	[number]	k4	1993
–	–	k?	–
Škoda	Škoda	k1gMnSc1	Škoda
Favorit	favorit	k1gMnSc1	favorit
(	(	kIx(	(
<g/>
781	[number]	k4	781
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Škoda	Škoda	k1gMnSc1	Škoda
Forman	Forman	k1gMnSc1	Forman
(	(	kIx(	(
<g/>
785	[number]	k4	785
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
LX	LX	kA	LX
<g/>
,	,	kIx,	,
GLX	GLX	kA	GLX
<g/>
,	,	kIx,	,
LXi	LXi	k1gFnSc1	LXi
<g/>
,	,	kIx,	,
GLXi	GLXi	k1gNnSc1	GLXi
<g/>
)	)	kIx)	)
1994	[number]	k4	1994
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
Felicia	felicia	k1gFnSc1	felicia
(	(	kIx(	(
<g/>
LX	LX	kA	LX
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
LXI	LXI	kA	LXI
<g/>
,	,	kIx,	,
GLX	GLX	kA	GLX
<g/>
,	,	kIx,	,
GLXI	GLXI	kA	GLXI
<g/>
)	)	kIx)	)
1995	[number]	k4	1995
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
Felicia	felicia	k1gFnSc1	felicia
Combi	Comb	k1gFnSc2	Comb
(	(	kIx(	(
<g/>
LX	LX	kA	LX
<g/>
,	,	kIx,	,
<g/>
LXI	LXI	kA	LXI
<g/>
,	,	kIx,	,
GLX	GLX	kA	GLX
<g/>
,	,	kIx,	,
<g/>
GLXI	GLXI	kA	GLXI
<g/>
,	,	kIx,	,
<g/>
Laurin	Laurin	k1gInSc1	Laurin
a	a	k8xC	a
Klement	Klement	k1gMnSc1	Klement
<g/>
)	)	kIx)	)
1996	[number]	k4	1996
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
Octavia	octavia	k1gFnSc1	octavia
(	(	kIx(	(
<g/>
LX	LX	kA	LX
<g/>
/	/	kIx~	/
<g/>
Classic	Classic	k1gMnSc1	Classic
<g/>
,	,	kIx,	,
GLX	GLX	kA	GLX
<g />
.	.	kIx.	.
</s>
<s>
<g/>
/	/	kIx~	/
<g/>
Ambiente	Ambient	k1gMnSc5	Ambient
<g/>
,	,	kIx,	,
SLX	SLX	kA	SLX
<g/>
/	/	kIx~	/
<g/>
Elegance	elegance	k1gFnSc1	elegance
<g/>
,	,	kIx,	,
RS	RS	kA	RS
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
Laurin	Laurin	k1gInSc1	Laurin
a	a	k8xC	a
Klement	Klement	k1gMnSc1	Klement
<g/>
,	,	kIx,	,
Tour	Tour	k1gMnSc1	Tour
<g/>
)	)	kIx)	)
1997	[number]	k4	1997
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
Felicia	felicia	k1gFnSc1	felicia
Fun	Fun	k1gFnSc1	Fun
1.3	[number]	k4	1.3
<g/>
,	,	kIx,	,
1.6	[number]	k4	1.6
1997	[number]	k4	1997
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
Felicia	felicia	k1gFnSc1	felicia
(	(	kIx(	(
<g/>
GLX	GLX	kA	GLX
1.6	[number]	k4	1.6
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
1998	[number]	k4	1998
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
Octavia	octavia	k1gFnSc1	octavia
Combi	Comb	k1gFnSc2	Comb
(	(	kIx(	(
<g/>
LX	LX	kA	LX
<g/>
/	/	kIx~	/
<g/>
Classic	Classic	k1gMnSc1	Classic
<g/>
,	,	kIx,	,
GLX	GLX	kA	GLX
<g/>
/	/	kIx~	/
<g/>
Ambiente	Ambient	k1gMnSc5	Ambient
<g/>
,	,	kIx,	,
SLX	SLX	kA	SLX
<g/>
/	/	kIx~	/
<g/>
Elegance	elegance	k1gFnSc1	elegance
<g/>
,	,	kIx,	,
RS	RS	kA	RS
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
Laurin	Laurin	k1gInSc1	Laurin
a	a	k8xC	a
Klement	Klement	k1gMnSc1	Klement
<g/>
)	)	kIx)	)
1999	[number]	k4	1999
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
Felicia	felicia	k1gFnSc1	felicia
Pick-up	Pickp	k1gInSc1	Pick-up
Laureta	Laureta	k1gFnSc1	Laureta
1999	[number]	k4	1999
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
Fabia	fabia	k1gFnSc1	fabia
(	(	kIx(	(
<g/>
Classic	Classic	k1gMnSc1	Classic
<g/>
,	,	kIx,	,
Comfort	Comfort	k1gInSc4	Comfort
<g/>
/	/	kIx~	/
<g/>
Ambiente	Ambient	k1gMnSc5	Ambient
<g/>
,	,	kIx,	,
Elegance	elegance	k1gFnSc2	elegance
<g/>
,	,	kIx,	,
RS	RS	kA	RS
<g/>
)	)	kIx)	)
2000	[number]	k4	2000
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
Fabia	fabia	k1gFnSc1	fabia
Combi	Comb	k1gFnSc2	Comb
(	(	kIx(	(
<g/>
Classic	Classic	k1gMnSc1	Classic
<g/>
,	,	kIx,	,
Comfort	Comfort	k1gInSc4	Comfort
<g/>
/	/	kIx~	/
<g/>
Ambiente	Ambient	k1gMnSc5	Ambient
<g/>
,	,	kIx,	,
Elegance	elegance	k1gFnSc1	elegance
<g/>
)	)	kIx)	)
2001	[number]	k4	2001
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
Fabia	fabia	k1gFnSc1	fabia
Sedan	sedan	k1gInSc1	sedan
(	(	kIx(	(
<g/>
Classic	Classic	k1gMnSc1	Classic
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Comfort	Comfort	k1gInSc4	Comfort
<g/>
/	/	kIx~	/
<g/>
Ambiente	Ambient	k1gMnSc5	Ambient
<g/>
,	,	kIx,	,
Elegance	elegance	k1gFnSc1	elegance
<g/>
)	)	kIx)	)
2001	[number]	k4	2001
–	–	k?	–
Škoda	Škoda	k1gMnSc1	Škoda
Superb	Superb	k1gMnSc1	Superb
(	(	kIx(	(
<g/>
Classic	Classic	k1gMnSc1	Classic
<g/>
,	,	kIx,	,
Comfort	Comfort	k1gInSc1	Comfort
<g/>
,	,	kIx,	,
Elegance	elegance	k1gFnSc1	elegance
<g/>
,	,	kIx,	,
Laurin	Laurin	k1gInSc1	Laurin
<g/>
&	&	k?	&
<g/>
Klement	Klement	k1gMnSc1	Klement
<g/>
)	)	kIx)	)
2004	[number]	k4	2004
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
Octavia	octavia	k1gFnSc1	octavia
II	II	kA	II
(	(	kIx(	(
<g/>
Classic	Classic	k1gMnSc1	Classic
<g/>
,	,	kIx,	,
Ambiente	Ambient	k1gMnSc5	Ambient
<g/>
,	,	kIx,	,
Elegance	elegance	k1gFnSc1	elegance
<g/>
,	,	kIx,	,
Laurin	Laurin	k1gInSc1	Laurin
<g />
.	.	kIx.	.
</s>
<s>
<g/>
&	&	k?	&
<g/>
Klement	Klement	k1gMnSc1	Klement
<g/>
,	,	kIx,	,
RS	RS	kA	RS
<g/>
,	,	kIx,	,
Tour	Tour	k1gMnSc1	Tour
<g/>
)	)	kIx)	)
2005	[number]	k4	2005
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
Octavia	octavia	k1gFnSc1	octavia
II	II	kA	II
Combi	Comb	k1gFnSc2	Comb
(	(	kIx(	(
<g/>
Classic	Classic	k1gMnSc1	Classic
<g/>
,	,	kIx,	,
Ambiente	Ambient	k1gMnSc5	Ambient
<g/>
,	,	kIx,	,
Elegance	elegance	k1gFnSc1	elegance
<g/>
,	,	kIx,	,
Laurin	Laurin	k1gInSc1	Laurin
<g/>
&	&	k?	&
<g/>
Klement	Klement	k1gMnSc1	Klement
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
Scout	Scout	k1gInSc1	Scout
<g/>
,	,	kIx,	,
RS	RS	kA	RS
<g/>
,	,	kIx,	,
Tour	Tour	k1gMnSc1	Tour
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
2006	[number]	k4	2006
–	–	k?	–
Škoda	Škoda	k1gMnSc1	Škoda
Roomster	Roomster	k1gMnSc1	Roomster
(	(	kIx(	(
<g/>
Roomster	Roomster	k1gMnSc1	Roomster
<g/>
,	,	kIx,	,
Style	styl	k1gInSc5	styl
<g/>
,	,	kIx,	,
Sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
Comfort	Comfort	k1gInSc1	Comfort
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
Fabia	fabia	k1gFnSc1	fabia
II	II	kA	II
<g/>
,	,	kIx,	,
Praktik	praktik	k1gMnSc1	praktik
2008	[number]	k4	2008
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
Fabia	fabia	k1gFnSc1	fabia
II	II	kA	II
Combi	Comb	k1gFnSc2	Comb
(	(	kIx(	(
<g/>
Fabia	fabia	k1gFnSc1	fabia
<g/>
,	,	kIx,	,
Classic	Classic	k1gMnSc1	Classic
<g/>
,	,	kIx,	,
Ambiente	Ambient	k1gMnSc5	Ambient
<g/>
,	,	kIx,	,
Sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
Elegance	elegance	k1gFnSc1	elegance
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
2008	[number]	k4	2008
–	–	k?	–
Škoda	Škoda	k1gMnSc1	Škoda
Superb	Superb	k1gMnSc1	Superb
II	II	kA	II
2009	[number]	k4	2009
–	–	k?	–
Škoda	Škoda	k1gMnSc1	Škoda
Yeti	yeti	k1gMnSc1	yeti
<g/>
,	,	kIx,	,
Škoda	Škoda	k1gMnSc1	Škoda
Superb	Superba	k1gFnPc2	Superba
II	II	kA	II
Combi	Comb	k1gFnSc2	Comb
2011	[number]	k4	2011
–	–	k?	–
Škoda	Škoda	k1gMnSc1	Škoda
Citigo	Citigo	k1gMnSc1	Citigo
2011	[number]	k4	2011
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
Rapid	rapid	k1gInSc1	rapid
–	–	k?	–
Indie	Indie	k1gFnSc1	Indie
2012	[number]	k4	2012
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
Rapid	rapid	k1gInSc1	rapid
–	–	k?	–
Evropa	Evropa	k1gFnSc1	Evropa
2013	[number]	k4	2013
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
Octavia	octavia	k1gFnSc1	octavia
III	III	kA	III
2014	[number]	k4	2014
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
Fabia	fabia	k1gFnSc1	fabia
III	III	kA	III
2015	[number]	k4	2015
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
Fabia	fabia	k1gFnSc1	fabia
III	III	kA	III
Combi	Comb	k1gFnSc2	Comb
2015	[number]	k4	2015
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Škoda	škoda	k1gFnSc1	škoda
Superb	Superb	k1gInSc1	Superb
III	III	kA	III
2015	[number]	k4	2015
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
Superb	Superb	k1gInSc1	Superb
III	III	kA	III
Combi	Comb	k1gFnSc2	Comb
2016	[number]	k4	2016
–	–	k?	–
Škoda	škoda	k1gFnSc1	škoda
Kodiaq	Kodiaq	k1gFnSc1	Kodiaq
2017	[number]	k4	2017
–	–	k?	–
Škoda	Škoda	k1gMnSc1	Škoda
Karoq	Karoq	k1gMnSc1	Karoq
(	(	kIx(	(
<g/>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
přehledu	přehled	k1gInSc6	přehled
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
pouze	pouze	k6eAd1	pouze
prototypy	prototyp	k1gInPc1	prototyp
a	a	k8xC	a
koncepty	koncept	k1gInPc1	koncept
z	z	k7c2	z
vlastního	vlastní	k2eAgInSc2d1	vlastní
vývoje	vývoj	k1gInSc2	vývoj
automobilky	automobilka	k1gFnSc2	automobilka
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
např.	např.	kA	např.
BAZ	BAZ	kA	BAZ
<g/>
,	,	kIx,	,
ÚVMV	ÚVMV	kA	ÚVMV
<g/>
,	,	kIx,	,
Metalexu	Metalex	k1gInSc2	Metalex
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Škoda	škoda	k1gFnSc1	škoda
1000	[number]	k4	1000
MB	MB	kA	MB
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
–	–	k?	–
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
rozšíření	rozšíření	k1gNnSc1	rozšíření
řady	řada	k1gFnSc2	řada
sedanů	sedan	k1gInPc2	sedan
(	(	kIx(	(
<g/>
MB	MB	kA	MB
<g/>
)	)	kIx)	)
a	a	k8xC	a
tudorů	tudor	k1gInPc2	tudor
(	(	kIx(	(
<g/>
MBX	MBX	kA	MBX
<g/>
)	)	kIx)	)
o	o	k7c4	o
kabriolet	kabriolet	k1gInSc4	kabriolet
(	(	kIx(	(
<g/>
+	+	kIx~	+
hardtop	hardtop	k1gInSc1	hardtop
<g/>
)	)	kIx)	)
a	a	k8xC	a
kombi	kombi	k1gNnSc7	kombi
Škoda	škoda	k1gFnSc1	škoda
720	[number]	k4	720
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
–	–	k?	–
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
nový	nový	k2eAgInSc1d1	nový
sedan	sedan	k1gInSc1	sedan
a	a	k8xC	a
kombi	kombi	k1gNnSc1	kombi
střední	střední	k2eAgFnSc2d1	střední
třídy	třída	k1gFnSc2	třída
Škoda	škoda	k6eAd1	škoda
Winnetou	Winnetý	k2eAgFnSc7d1	Winnetý
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
prototyp	prototyp	k1gInSc1	prototyp
sportovního	sportovní	k2eAgInSc2d1	sportovní
automobilu	automobil	k1gInSc2	automobil
Škoda	škoda	k1gFnSc1	škoda
740	[number]	k4	740
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
–	–	k?	–
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
nový	nový	k2eAgInSc1d1	nový
sedan	sedan	k1gInSc1	sedan
a	a	k8xC	a
kombi	kombi	k1gNnSc1	kombi
nižší	nízký	k2eAgFnSc2d2	nižší
třídy	třída	k1gFnSc2	třída
Škoda	škoda	k6eAd1	škoda
110	[number]	k4	110
Super	super	k2eAgInSc1d1	super
Sport	sport	k1gInSc1	sport
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
prototyp	prototyp	k1gInSc1	prototyp
sportovního	sportovní	k2eAgInSc2d1	sportovní
automobilu	automobil	k1gInSc2	automobil
Škoda	škoda	k1gFnSc1	škoda
760	[number]	k4	760
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
–	–	k?	–
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
ucelená	ucelený	k2eAgFnSc1d1	ucelená
typová	typový	k2eAgFnSc1d1	typová
řada	řada	k1gFnSc1	řada
vozů	vůz	k1gInPc2	vůz
nižší	nízký	k2eAgFnSc2d2	nižší
střední	střední	k2eAgFnSc2d1	střední
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
vyvíjená	vyvíjený	k2eAgFnSc1d1	vyvíjená
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
východoněmeckými	východoněmecký	k2eAgFnPc7d1	východoněmecká
automobilkami	automobilka	k1gFnPc7	automobilka
Škoda	škoda	k6eAd1	škoda
180	[number]	k4	180
<g/>
/	/	kIx~	/
<g/>
200	[number]	k4	200
RS	RS	kA	RS
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
závodní	závodní	k2eAgNnSc1d1	závodní
kupé	kupé	k1gNnSc1	kupé
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
typu	typ	k1gInSc2	typ
Škoda	škoda	k1gFnSc1	škoda
110	[number]	k4	110
<g />
.	.	kIx.	.
</s>
<s>
R	R	kA	R
Škoda	škoda	k1gFnSc1	škoda
Felicia	felicia	k1gFnSc1	felicia
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
tříprostorová	tříprostorový	k2eAgFnSc1d1	tříprostorová
verze	verze	k1gFnSc1	verze
typu	typ	k1gInSc3	typ
Škoda	Škoda	k1gMnSc1	Škoda
Favorit	favorit	k1gMnSc1	favorit
<g/>
,	,	kIx,	,
interní	interní	k2eAgFnSc4d1	interní
typové	typový	k2eAgNnSc4d1	typové
označení	označení	k1gNnSc4	označení
Škoda	škoda	k6eAd1	škoda
782	[number]	k4	782
Škoda	Škoda	k1gMnSc1	Škoda
Favorit	favorit	k1gMnSc1	favorit
Coupé	Coupý	k2eAgFnSc2d1	Coupý
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
čtyřmístná	čtyřmístný	k2eAgFnSc1d1	čtyřmístná
dvoudveřová	dvoudveřový	k2eAgFnSc1d1	dvoudveřová
verze	verze	k1gFnSc1	verze
typu	typ	k1gInSc3	typ
Škoda	Škoda	k1gMnSc1	Škoda
Favorit	favorit	k1gMnSc1	favorit
<g/>
,	,	kIx,	,
interní	interní	k2eAgFnSc4d1	interní
typové	typový	k2eAgNnSc4d1	typové
označení	označení	k1gNnSc4	označení
Škoda	škoda	k1gFnSc1	škoda
783	[number]	k4	783
Škoda	škoda	k1gFnSc1	škoda
Savana	savana	k1gFnSc1	savana
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
pětimístný	pětimístný	k2eAgInSc1d1	pětimístný
třídveřový	třídveřový	k2eAgInSc1d1	třídveřový
prosklený	prosklený	k2eAgInSc1d1	prosklený
furgon	furgon	k1gInSc1	furgon
<g/>
,	,	kIx,	,
odvozený	odvozený	k2eAgInSc1d1	odvozený
od	od	k7c2	od
verze	verze	k1gFnSc2	verze
typu	typ	k1gInSc2	typ
Škoda	škoda	k1gFnSc1	škoda
Pick-up	Pickp	k1gMnSc1	Pick-up
<g/>
,	,	kIx,	,
interní	interní	k2eAgFnSc4d1	interní
typové	typový	k2eAgNnSc4d1	typové
označení	označení	k1gNnSc4	označení
Škoda	škoda	k6eAd1	škoda
784	[number]	k4	784
Škoda	Škoda	k1gMnSc1	Škoda
Favorit	favorit	k1gMnSc1	favorit
sanita	sanita	k1gFnSc1	sanita
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
verze	verze	k1gFnSc1	verze
typu	typ	k1gInSc3	typ
Škoda	Škoda	k1gMnSc1	Škoda
Forman	Forman	k1gMnSc1	Forman
<g/>
,	,	kIx,	,
interní	interní	k2eAgFnSc4d1	interní
typové	typový	k2eAgNnSc4d1	typové
označení	označení	k1gNnSc4	označení
Škoda	škoda	k6eAd1	škoda
788	[number]	k4	788
Škoda	Škoda	k1gMnSc1	Škoda
Favorit	favorit	k1gMnSc1	favorit
Tremp	tremp	k1gMnSc1	tremp
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Škoda	škoda	k1gFnSc1	škoda
Favorit	favorit	k1gInSc1	favorit
Varia	varia	k1gNnPc1	varia
<g/>
,	,	kIx,	,
Forman	Forman	k1gMnSc1	Forman
Varia	varia	k1gNnPc1	varia
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
prodloužení	prodloužení	k1gNnSc4	prodloužení
základních	základní	k2eAgInPc2d1	základní
typů	typ	k1gInPc2	typ
pomocí	pomocí	k7c2	pomocí
laminátové	laminátový	k2eAgFnSc2d1	laminátová
nástavby	nástavba	k1gFnSc2	nástavba
namísto	namísto	k7c2	namísto
pátých	pátý	k4xOgFnPc2	pátý
dveří	dveře	k1gFnPc2	dveře
Škoda	škoda	k1gFnSc1	škoda
Felicia	felicia	k1gFnSc1	felicia
Golden	Goldno	k1gNnPc2	Goldno
Prague	Pragu	k1gFnSc2	Pragu
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
speciální	speciální	k2eAgNnPc4d1	speciální
provedení	provedení	k1gNnPc4	provedení
sériového	sériový	k2eAgInSc2d1	sériový
vozu	vůz	k1gInSc2	vůz
Škoda	škoda	k1gFnSc1	škoda
Ahoj	ahoj	k0	ahoj
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Škoda	škoda	k1gFnSc1	škoda
Tudor	tudor	k1gInSc1	tudor
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
dvoudveřové	dvoudveřový	k2eAgNnSc4d1	dvoudveřové
provedení	provedení	k1gNnSc4	provedení
limuzíny	limuzína	k1gFnSc2	limuzína
Škoda	Škoda	k1gMnSc1	Škoda
Superb	Superb	k1gMnSc1	Superb
Škoda	Škoda	k1gMnSc1	Škoda
Fabia	fabia	k1gFnSc1	fabia
Paris	Paris	k1gMnSc1	Paris
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
speciální	speciální	k2eAgNnPc4d1	speciální
provedení	provedení	k1gNnPc4	provedení
sériového	sériový	k2eAgInSc2d1	sériový
vozu	vůz	k1gInSc2	vůz
Škoda	Škoda	k1gMnSc1	Škoda
Yeti	yeti	k1gMnSc1	yeti
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
sportovně	sportovně	k6eAd1	sportovně
–	–	k?	–
užitkový	užitkový	k2eAgInSc4d1	užitkový
vůz	vůz	k1gInSc4	vůz
Škoda	Škoda	k1gMnSc1	Škoda
Joyster	Joyster	k1gMnSc1	Joyster
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
vůz	vůz	k1gInSc1	vůz
pro	pro	k7c4	pro
volný	volný	k2eAgInSc4d1	volný
čas	čas	k1gInSc4	čas
Škoda	škoda	k1gFnSc1	škoda
Vision	vision	k1gInSc1	vision
D	D	kA	D
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
koncept	koncept	k1gInSc4	koncept
budoucího	budoucí	k2eAgNnSc2d1	budoucí
designérského	designérský	k2eAgNnSc2d1	designérské
směřování	směřování	k1gNnSc2	směřování
všech	všecek	k3xTgInPc2	všecek
typů	typ	k1gInPc2	typ
Škoda	Škoda	k1gMnSc1	Škoda
MissionL	MissionL	k1gMnSc1	MissionL
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
limuzína	limuzína	k1gFnSc1	limuzína
<g/>
,	,	kIx,	,
koncept	koncept	k1gInSc1	koncept
budoucího	budoucí	k2eAgNnSc2d1	budoucí
designérského	designérský	k2eAgNnSc2d1	designérské
směřování	směřování	k1gNnSc2	směřování
všech	všecek	k3xTgInPc2	všecek
typů	typ	k1gInPc2	typ
Škoda	škoda	k1gFnSc1	škoda
Rapid	rapid	k1gInSc1	rapid
Sport	sport	k1gInSc1	sport
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
přepracovaná	přepracovaný	k2eAgFnSc1d1	přepracovaná
Škoda	škoda	k1gFnSc1	škoda
Rapid	rapid	k1gInSc4	rapid
pro	pro	k7c4	pro
sportovní	sportovní	k2eAgNnSc4d1	sportovní
setkání	setkání	k1gNnSc4	setkání
Volkswagenu	volkswagen	k1gInSc2	volkswagen
Škoda	škoda	k6eAd1	škoda
Auto	auto	k1gNnSc1	auto
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgInPc4	tři
hlavní	hlavní	k2eAgInPc4d1	hlavní
výrobní	výrobní	k2eAgInPc4d1	výrobní
závody	závod	k1gInPc4	závod
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
závodů	závod	k1gInPc2	závod
v	v	k7c6	v
ČR	ČR	kA	ČR
vznikají	vznikat	k5eAaImIp3nP	vznikat
vozy	vůz	k1gInPc1	vůz
Škoda	škoda	k6eAd1	škoda
i	i	k9	i
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
Číně	Čína	k1gFnSc6	Čína
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
továrnách	továrna	k1gFnPc6	továrna
koncernu	koncern	k1gInSc2	koncern
VW	VW	kA	VW
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgNnSc1d2	menší
množství	množství	k1gNnSc1	množství
automobilů	automobil	k1gInPc2	automobil
se	se	k3xPyFc4	se
montuje	montovat	k5eAaImIp3nS	montovat
u	u	k7c2	u
smluvních	smluvní	k2eAgMnPc2d1	smluvní
partnerů	partner	k1gMnPc2	partner
v	v	k7c6	v
Kazachstánu	Kazachstán	k1gInSc6	Kazachstán
(	(	kIx(	(
<g/>
JSC	JSC	kA	JSC
Azia	Azia	k1gMnSc1	Azia
Avto	Avto	k1gMnSc1	Avto
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
Öskemen	Öskemen	k1gInSc1	Öskemen
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
(	(	kIx(	(
<g/>
firma	firma	k1gFnSc1	firma
ZAT	ZAT	kA	ZAT
Evrocar	Evrocar	k1gInSc4	Evrocar
<g/>
,	,	kIx,	,
obec	obec	k1gFnSc4	obec
Solomonovo	Solomonův	k2eAgNnSc1d1	Solomonovo
<g/>
,	,	kIx,	,
Zakarpatská	zakarpatský	k2eAgFnSc1d1	Zakarpatská
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
výrobní	výrobní	k2eAgInPc4d1	výrobní
závody	závod	k1gInPc4	závod
a	a	k8xC	a
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
vyráběné	vyráběný	k2eAgInPc1d1	vyráběný
modely	model	k1gInPc1	model
<g/>
:	:	kIx,	:
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
čítala	čítat	k5eAaImAgFnS	čítat
dealerská	dealerský	k2eAgFnSc1d1	dealerská
síť	síť	k1gFnSc1	síť
společnosti	společnost	k1gFnSc2	společnost
Škoda	škoda	k1gFnSc1	škoda
Auto	auto	k1gNnSc1	auto
a.s.	a.s.	k?	a.s.
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
celkem	celkem	k6eAd1	celkem
273	[number]	k4	273
autorizovaných	autorizovaný	k2eAgMnPc2d1	autorizovaný
prodejců	prodejce	k1gMnPc2	prodejce
a	a	k8xC	a
servisních	servisní	k2eAgMnPc2d1	servisní
partnerů	partner	k1gMnPc2	partner
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
automobilka	automobilka	k1gFnSc1	automobilka
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
prodaných	prodaný	k2eAgInPc2d1	prodaný
vozů	vůz	k1gInPc2	vůz
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
společnost	společnost	k1gFnSc1	společnost
Hyundai	Hyunda	k1gFnSc2	Hyunda
Motor	motor	k1gInSc1	motor
Czech	Czech	k1gMnSc1	Czech
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
disponovala	disponovat	k5eAaBmAgFnS	disponovat
v	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
sítí	síť	k1gFnPc2	síť
159	[number]	k4	159
prodejen	prodejna	k1gFnPc2	prodejna
a	a	k8xC	a
servisních	servisní	k2eAgNnPc2d1	servisní
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
dealery	dealer	k1gMnPc4	dealer
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
počtem	počet	k1gInSc7	počet
prodaných	prodaný	k2eAgInPc2d1	prodaný
vozů	vůz	k1gInPc2	vůz
patří	patřit	k5eAaImIp3nS	patřit
společnosti	společnost	k1gFnSc2	společnost
Auto	auto	k1gNnSc1	auto
Jarov	Jarov	k1gInSc1	Jarov
<g/>
,	,	kIx,	,
Autosalon	autosalon	k1gInSc1	autosalon
Klokočka	Klokočka	k1gFnSc1	Klokočka
<g/>
,	,	kIx,	,
NH	NH	kA	NH
Car	car	k1gMnSc1	car
<g/>
,	,	kIx,	,
Porsche	Porsche	k1gNnSc4	Porsche
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
Auto	auto	k1gNnSc1	auto
Heller	Heller	k1gMnSc1	Heller
<g/>
.	.	kIx.	.
</s>
<s>
Vozy	vůz	k1gInPc1	vůz
Škoda	škoda	k1gFnSc1	škoda
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dostání	dostání	k1gNnSc3	dostání
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
vrátily	vrátit	k5eAaPmAgFnP	vrátit
na	na	k7c4	na
trh	trh	k1gInSc4	trh
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
se	se	k3xPyFc4	se
vyvážely	vyvážet	k5eAaImAgInP	vyvážet
Škodovky	škodovka	k1gFnSc2	škodovka
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
do	do	k7c2	do
USA	USA	kA	USA
modely	model	k1gInPc4	model
Felicia	felicia	k1gFnSc1	felicia
a	a	k8xC	a
440	[number]	k4	440
<g/>
/	/	kIx~	/
<g/>
Octavia	octavia	k1gFnSc1	octavia
a	a	k8xC	a
do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
navíc	navíc	k6eAd1	navíc
Škoda	škoda	k6eAd1	škoda
120	[number]	k4	120
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
provozuje	provozovat	k5eAaImIp3nS	provozovat
v	v	k7c6	v
Mladé	mladý	k2eAgFnSc6d1	mladá
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
střední	střední	k2eAgNnSc1d1	střední
odborné	odborný	k2eAgNnSc1d1	odborné
učiliště	učiliště	k1gNnSc1	učiliště
ŠKODA	škoda	k1gFnSc1	škoda
AUTO	auto	k1gNnSc1	auto
a.s.	a.s.	k?	a.s.
Střední	střední	k2eAgNnSc1d1	střední
odborné	odborný	k2eAgNnSc1d1	odborné
učiliště	učiliště	k1gNnSc1	učiliště
strojírenské	strojírenský	k2eAgNnSc1d1	strojírenské
<g/>
,	,	kIx,	,
odštěpný	odštěpný	k2eAgInSc4d1	odštěpný
závod	závod	k1gInSc4	závod
a	a	k8xC	a
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
Škoda	škoda	k6eAd1	škoda
Auto	auto	k1gNnSc4	auto
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
vzniku	vznik	k1gInSc2	vznik
ASAP	ASAP	kA	ASAP
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
se	se	k3xPyFc4	se
datují	datovat	k5eAaImIp3nP	datovat
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
sjednocení	sjednocení	k1gNnSc4	sjednocení
vzhledu	vzhled	k1gInSc2	vzhled
různých	různý	k2eAgInPc2d1	různý
výrobků	výrobek	k1gInPc2	výrobek
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
jasná	jasný	k2eAgFnSc1d1	jasná
příslušnost	příslušnost	k1gFnSc1	příslušnost
k	k	k7c3	k
jedné	jeden	k4xCgFnSc3	jeden
značce	značka	k1gFnSc3	značka
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
společných	společný	k2eAgInPc2d1	společný
designérských	designérský	k2eAgInPc2d1	designérský
prvků	prvek	k1gInPc2	prvek
u	u	k7c2	u
osobních	osobní	k2eAgInPc2d1	osobní
i	i	k8xC	i
nákladních	nákladní	k2eAgInPc2d1	nákladní
automobilů	automobil	k1gInPc2	automobil
Škoda	škoda	k6eAd1	škoda
byl	být	k5eAaImAgInS	být
"	"	kIx"	"
<g/>
žemlový	žemlový	k2eAgInSc1d1	žemlový
<g/>
"	"	kIx"	"
tvar	tvar	k1gInSc1	tvar
téměř	téměř	k6eAd1	téměř
svislé	svislý	k2eAgFnSc2d1	svislá
masky	maska	k1gFnSc2	maska
chladiče	chladič	k1gInSc2	chladič
<g/>
,	,	kIx,	,
opticky	opticky	k6eAd1	opticky
svisle	svisle	k6eAd1	svisle
půlené	půlený	k2eAgFnPc1d1	půlená
<g/>
,	,	kIx,	,
se	s	k7c7	s
svislými	svislý	k2eAgInPc7d1	svislý
žebry	žebr	k1gInPc7	žebr
<g/>
.	.	kIx.	.
</s>
<s>
Navazující	navazující	k2eAgInPc1d1	navazující
křídlové	křídlový	k2eAgInPc1d1	křídlový
kapoty	kapot	k1gInPc1	kapot
měly	mít	k5eAaImAgInP	mít
svisle	svisle	k6eAd1	svisle
orientované	orientovaný	k2eAgInPc1d1	orientovaný
výdechy	výdech	k1gInPc1	výdech
<g/>
,	,	kIx,	,
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
pouhým	pouhý	k2eAgNnSc7d1	pouhé
prolisováním	prolisování	k1gNnSc7	prolisování
<g/>
.	.	kIx.	.
</s>
<s>
Reflektory	reflektor	k1gInPc1	reflektor
byly	být	k5eAaImAgInP	být
umístěny	umístit	k5eAaPmNgInP	umístit
na	na	k7c6	na
příčné	příčný	k2eAgFnSc6d1	příčná
obloukovité	obloukovitý	k2eAgFnSc6d1	obloukovitá
konzole	konzola	k1gFnSc6	konzola
před	před	k7c7	před
chladičem	chladič	k1gInSc7	chladič
<g/>
.	.	kIx.	.
</s>
<s>
Typickými	typický	k2eAgMnPc7d1	typický
představiteli	představitel	k1gMnPc7	představitel
jsou	být	k5eAaImIp3nP	být
vozy	vůz	k1gInPc1	vůz
Škoda	škoda	k1gFnSc1	škoda
633	[number]	k4	633
<g/>
,	,	kIx,	,
Škoda	škoda	k1gFnSc1	škoda
420	[number]	k4	420
či	či	k8xC	či
Škoda	škoda	k1gFnSc1	škoda
418	[number]	k4	418
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1931	[number]	k4	1931
a	a	k8xC	a
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
i	i	k9	i
u	u	k7c2	u
autobusu	autobus	k1gInSc2	autobus
Škoda	škoda	k1gFnSc1	škoda
206	[number]	k4	206
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
postupným	postupný	k2eAgNnSc7d1	postupné
zaváděním	zavádění	k1gNnSc7	zavádění
zcela	zcela	k6eAd1	zcela
nové	nový	k2eAgFnSc2d1	nová
konstrukce	konstrukce	k1gFnSc2	konstrukce
automobilů	automobil	k1gInPc2	automobil
s	s	k7c7	s
páteřovým	páteřový	k2eAgInSc7d1	páteřový
rámem	rám	k1gInSc7	rám
se	se	k3xPyFc4	se
design	design	k1gInSc4	design
upravoval	upravovat	k5eAaImAgInS	upravovat
a	a	k8xC	a
uhlazoval	uhlazovat	k5eAaImAgInS	uhlazovat
<g/>
.	.	kIx.	.
</s>
<s>
Maska	maska	k1gFnSc1	maska
chladiče	chladič	k1gInSc2	chladič
se	se	k3xPyFc4	se
lehce	lehko	k6eAd1	lehko
zešikmila	zešikmit	k5eAaPmAgFnS	zešikmit
a	a	k8xC	a
dostala	dostat	k5eAaPmAgFnS	dostat
výrazně	výrazně	k6eAd1	výrazně
mandlovitý	mandlovitý	k2eAgInSc4d1	mandlovitý
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
výdechové	výdechový	k2eAgInPc4d1	výdechový
prolisy	prolis	k1gInPc4	prolis
na	na	k7c6	na
křídlové	křídlový	k2eAgFnSc6d1	křídlová
kapotě	kapota	k1gFnSc6	kapota
už	už	k9	už
také	také	k9	také
nebyly	být	k5eNaImAgFnP	být
svislé	svislý	k2eAgFnPc1d1	svislá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zešikmené	zešikmený	k2eAgFnPc1d1	zešikmená
rovnoběžně	rovnoběžně	k6eAd1	rovnoběžně
s	s	k7c7	s
maskou	maska	k1gFnSc7	maska
chladiče	chladič	k1gInSc2	chladič
<g/>
.	.	kIx.	.
</s>
<s>
Reflektory	reflektor	k1gInPc1	reflektor
<g/>
,	,	kIx,	,
umístěné	umístěný	k2eAgFnPc1d1	umístěná
přibližně	přibližně	k6eAd1	přibližně
uprostřed	uprostřed	k7c2	uprostřed
osy	osa	k1gFnSc2	osa
předních	přední	k2eAgInPc2d1	přední
blatníků	blatník	k1gInPc2	blatník
<g/>
,	,	kIx,	,
dostaly	dostat	k5eAaPmAgFnP	dostat
elegantně	elegantně	k6eAd1	elegantně
protáhlý	protáhlý	k2eAgInSc4d1	protáhlý
kapkovitý	kapkovitý	k2eAgInSc4d1	kapkovitý
tvar	tvar	k1gInSc4	tvar
nosné	nosný	k2eAgFnSc2d1	nosná
paraboly	parabola	k1gFnSc2	parabola
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
prvky	prvek	k1gInPc7	prvek
se	se	k3xPyFc4	se
setkáme	setkat	k5eAaPmIp1nP	setkat
například	například	k6eAd1	například
u	u	k7c2	u
vozů	vůz	k1gInPc2	vůz
Škoda	škoda	k6eAd1	škoda
Sagitta	Sagitt	k1gInSc2	Sagitt
<g/>
,	,	kIx,	,
Škoda	škoda	k1gFnSc1	škoda
Popular	Populara	k1gFnPc2	Populara
či	či	k8xC	či
Škoda	škoda	k1gFnSc1	škoda
Rapid	rapid	k1gInSc4	rapid
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
letech	let	k1gInPc6	let
1934	[number]	k4	1934
<g/>
–	–	k?	–
<g/>
36	[number]	k4	36
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
lehce	lehko	k6eAd1	lehko
přepracován	přepracován	k2eAgMnSc1d1	přepracován
<g/>
,	,	kIx,	,
maska	maska	k1gFnSc1	maska
chladiče	chladič	k1gInSc2	chladič
byla	být	k5eAaImAgFnS	být
více	hodně	k6eAd2	hodně
vyklenuta	vyklenut	k2eAgFnSc1d1	vyklenuta
(	(	kIx(	(
<g/>
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
typů	typ	k1gInPc2	typ
doplněna	doplnit	k5eAaPmNgFnS	doplnit
širokou	široký	k2eAgFnSc7d1	široká
středovou	středový	k2eAgFnSc7d1	středová
ozdobnou	ozdobný	k2eAgFnSc7d1	ozdobná
lištou	lišta	k1gFnSc7	lišta
<g/>
)	)	kIx)	)
a	a	k8xC	a
zejména	zejména	k9	zejména
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
úpravě	úprava	k1gFnSc3	úprava
výdechů	výdech	k1gInPc2	výdech
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
teď	teď	k6eAd1	teď
prolisovány	prolisovat	k5eAaPmNgInP	prolisovat
dovnitř	dovnitř	k6eAd1	dovnitř
a	a	k8xC	a
zvenčí	zvenčí	k6eAd1	zvenčí
přepásány	přepásán	k2eAgInPc1d1	přepásán
ozdobnou	ozdobný	k2eAgFnSc7d1	ozdobná
lištou	lišta	k1gFnSc7	lišta
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
typy	typ	k1gInPc1	typ
dostaly	dostat	k5eAaPmAgInP	dostat
i	i	k9	i
částečně	částečně	k6eAd1	částečně
zapuštěné	zapuštěný	k2eAgInPc1d1	zapuštěný
reflektory	reflektor	k1gInPc1	reflektor
do	do	k7c2	do
elegantně	elegantně	k6eAd1	elegantně
prodloužených	prodloužený	k2eAgInPc2d1	prodloužený
blatníků	blatník	k1gInPc2	blatník
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
design	design	k1gInSc4	design
najdeme	najít	k5eAaPmIp1nP	najít
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
sériových	sériový	k2eAgInPc2d1	sériový
osobních	osobní	k2eAgInPc2d1	osobní
vozů	vůz	k1gInPc2	vůz
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1936	[number]	k4	1936
<g/>
–	–	k?	–
<g/>
38	[number]	k4	38
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
u	u	k7c2	u
užitkových	užitkový	k2eAgInPc2d1	užitkový
vozů	vůz	k1gInPc2	vůz
Škoda	škoda	k6eAd1	škoda
254	[number]	k4	254
<g/>
,	,	kIx,	,
Škoda	škoda	k1gFnSc1	škoda
406	[number]	k4	406
a	a	k8xC	a
Škoda	škoda	k1gFnSc1	škoda
706	[number]	k4	706
z	z	k7c2	z
pozdější	pozdní	k2eAgFnSc2d2	pozdější
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
osobní	osobní	k2eAgInPc4d1	osobní
automobily	automobil	k1gInPc4	automobil
použil	použít	k5eAaPmAgInS	použít
nový	nový	k2eAgInSc1d1	nový
prvek	prvek	k1gInSc1	prvek
<g/>
:	:	kIx,	:
zvedací	zvedací	k2eAgNnPc1d1	zvedací
<g/>
,	,	kIx,	,
jednodílná	jednodílný	k2eAgNnPc1d1	jednodílné
<g/>
,	,	kIx,	,
vzadu	vzadu	k6eAd1	vzadu
ukotvená	ukotvený	k2eAgFnSc1d1	ukotvená
kapota	kapota	k1gFnSc1	kapota
motoru	motor	k1gInSc2	motor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
podstatné	podstatný	k2eAgFnSc3d1	podstatná
designérské	designérský	k2eAgFnSc3d1	designérská
změně	změna	k1gFnSc3	změna
<g/>
,	,	kIx,	,
maska	maska	k1gFnSc1	maska
chladiče	chladič	k1gInSc2	chladič
dostala	dostat	k5eAaPmAgFnS	dostat
tvar	tvar	k1gInSc4	tvar
svislého	svislý	k2eAgInSc2d1	svislý
válce	válec	k1gInSc2	válec
s	s	k7c7	s
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
žebry	žebr	k1gInPc7	žebr
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yQgFnSc6	který
svým	svůj	k3xOyFgNnPc3	svůj
olištováním	olištování	k1gNnPc3	olištování
přímo	přímo	k6eAd1	přímo
navazovaly	navazovat	k5eAaImAgFnP	navazovat
i	i	k9	i
boční	boční	k2eAgInPc4d1	boční
výdechy	výdech	k1gInPc4	výdech
<g/>
.	.	kIx.	.
</s>
<s>
Reflektory	reflektor	k1gInPc1	reflektor
byly	být	k5eAaImAgInP	být
již	již	k6eAd1	již
téměř	téměř	k6eAd1	téměř
zapuštěny	zapustit	k5eAaPmNgInP	zapustit
do	do	k7c2	do
blatníků	blatník	k1gInPc2	blatník
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
"	"	kIx"	"
<g/>
podmračený	podmračený	k2eAgInSc1d1	podmračený
<g/>
"	"	kIx"	"
design	design	k1gInSc1	design
je	být	k5eAaImIp3nS	být
společný	společný	k2eAgInSc1d1	společný
všem	všecek	k3xTgInPc3	všecek
osobním	osobní	k2eAgInPc3d1	osobní
automobilům	automobil	k1gInPc3	automobil
Škoda	škoda	k6eAd1	škoda
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
je	být	k5eAaImIp3nS	být
uplatněn	uplatnit	k5eAaPmNgInS	uplatnit
i	i	k9	i
na	na	k7c6	na
vojenských	vojenský	k2eAgInPc6d1	vojenský
a	a	k8xC	a
malých	malý	k2eAgInPc6d1	malý
užitkových	užitkový	k2eAgInPc6d1	užitkový
automobilech	automobil	k1gInPc6	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
koncern	koncern	k1gInSc1	koncern
Škoda	škoda	k6eAd1	škoda
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vazby	vazba	k1gFnPc1	vazba
nešlo	jít	k5eNaImAgNnS	jít
snadno	snadno	k6eAd1	snadno
přetrhat	přetrhat	k5eAaPmF	přetrhat
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
doba	doba	k1gFnSc1	doba
a	a	k8xC	a
nový	nový	k2eAgInSc1d1	nový
design	design	k1gInSc1	design
přinesly	přinést	k5eAaPmAgInP	přinést
svislou	svislý	k2eAgFnSc4d1	svislá
<g/>
,	,	kIx,	,
vysokou	vysoký	k2eAgFnSc4d1	vysoká
čelní	čelní	k2eAgFnSc4d1	čelní
stěnu	stěna	k1gFnSc4	stěna
v	v	k7c6	v
"	"	kIx"	"
<g/>
americkém	americký	k2eAgInSc6d1	americký
<g/>
"	"	kIx"	"
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
se	s	k7c7	s
zcela	zcela	k6eAd1	zcela
zapuštěnými	zapuštěný	k2eAgInPc7d1	zapuštěný
reflektory	reflektor	k1gInPc7	reflektor
v	v	k7c6	v
protažené	protažený	k2eAgFnSc6d1	protažená
horní	horní	k2eAgFnSc6d1	horní
hraně	hrana	k1gFnSc6	hrana
blatníků	blatník	k1gInPc2	blatník
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
několika	několik	k4yIc7	několik
různě	různě	k6eAd1	různě
dlouhými	dlouhý	k2eAgFnPc7d1	dlouhá
vodorovnými	vodorovný	k2eAgFnPc7d1	vodorovná
ozdobnými	ozdobný	k2eAgFnPc7d1	ozdobná
lištami	lišta	k1gFnPc7	lišta
v	v	k7c6	v
masce	maska	k1gFnSc6	maska
chladiče	chladič	k1gInSc2	chladič
pod	pod	k7c7	pod
zašpičatělou	zašpičatělý	k2eAgFnSc7d1	zašpičatělá
kapotou	kapota	k1gFnSc7	kapota
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
prvek	prvek	k1gInSc1	prvek
je	být	k5eAaImIp3nS	být
uplatněn	uplatnit	k5eAaPmNgInS	uplatnit
jak	jak	k6eAd1	jak
na	na	k7c6	na
vozech	vůz	k1gInPc6	vůz
Škoda	škoda	k6eAd1	škoda
1101	[number]	k4	1101
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
52	[number]	k4	52
<g/>
)	)	kIx)	)
či	či	k8xC	či
Škoda	Škoda	k1gMnSc1	Škoda
Superb	Superb	k1gMnSc1	Superb
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
–	–	k?	–
<g/>
49	[number]	k4	49
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
částečně	částečně	k6eAd1	částečně
i	i	k9	i
u	u	k7c2	u
vozu	vůz	k1gInSc2	vůz
Škoda	Škoda	k1gMnSc1	Škoda
VOS	vosa	k1gFnPc2	vosa
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
–	–	k?	–
<g/>
52	[number]	k4	52
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
obdobu	obdoba	k1gFnSc4	obdoba
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
i	i	k9	i
na	na	k7c6	na
autobusu	autobus	k1gInSc6	autobus
Škoda	škoda	k1gFnSc1	škoda
706	[number]	k4	706
RO	RO	kA	RO
z	z	k7c2	z
Avie	Avia	k1gFnSc2	Avia
Letňany	Letňan	k1gMnPc4	Letňan
či	či	k8xC	či
u	u	k7c2	u
trolejbusu	trolejbus	k1gInSc2	trolejbus
Škoda	škoda	k6eAd1	škoda
6	[number]	k4	6
<g/>
Tr	Tr	k1gFnPc2	Tr
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
z	z	k7c2	z
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
když	když	k8xS	když
AZNP	AZNP	kA	AZNP
výrazně	výrazně	k6eAd1	výrazně
omezily	omezit	k5eAaPmAgFnP	omezit
počet	počet	k1gInSc4	počet
vyráběných	vyráběný	k2eAgInPc2d1	vyráběný
typů	typ	k1gInPc2	typ
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgInS	být
společný	společný	k2eAgInSc1d1	společný
designový	designový	k2eAgInSc1d1	designový
prvek	prvek	k1gInSc1	prvek
nutný	nutný	k2eAgInSc1d1	nutný
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
najdeme	najít	k5eAaPmIp1nP	najít
drobnou	drobný	k2eAgFnSc4d1	drobná
paralelu	paralela	k1gFnSc4	paralela
mezi	mezi	k7c7	mezi
chladiči	chladič	k1gInPc7	chladič
vozů	vůz	k1gInPc2	vůz
Škoda	škoda	k1gFnSc1	škoda
Octavia	octavia	k1gFnSc1	octavia
z	z	k7c2	z
AZNP	AZNP	kA	AZNP
a	a	k8xC	a
Škoda	škoda	k1gFnSc1	škoda
706	[number]	k4	706
RT	RT	kA	RT
z	z	k7c2	z
LIAZu	liaz	k1gInSc2	liaz
<g/>
:	:	kIx,	:
svisle	svisle	k6eAd1	svisle
orientované	orientovaný	k2eAgFnSc2d1	orientovaná
štěrbiny	štěrbina	k1gFnSc2	štěrbina
masky	maska	k1gFnSc2	maska
chladiče	chladič	k1gInSc2	chladič
přepásané	přepásaný	k2eAgFnPc4d1	přepásaná
oválnou	oválný	k2eAgFnSc7d1	oválná
lištou	lišta	k1gFnSc7	lišta
se	s	k7c7	s
znakem	znak	k1gInSc7	znak
Škoda	škoda	k6eAd1	škoda
uprostřed	uprostřed	k7c2	uprostřed
<g/>
.	.	kIx.	.
</s>
<s>
Stejné	stejná	k1gFnSc3	stejná
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
u	u	k7c2	u
jabloneckého	jablonecký	k2eAgInSc2d1	jablonecký
prototypu	prototyp	k1gInSc2	prototyp
autobusu	autobus	k1gInSc2	autobus
Škoda	škoda	k1gFnSc1	škoda
706	[number]	k4	706
RTO	RTO	kA	RTO
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
návrh	návrh	k1gInSc4	návrh
z	z	k7c2	z
vysokomýtské	vysokomýtský	k2eAgFnSc2d1	vysokomýtská
Karosy	karosa	k1gFnSc2	karosa
<g/>
.	.	kIx.	.
</s>
<s>
Trolejbus	trolejbus	k1gInSc1	trolejbus
Škoda	škoda	k1gFnSc1	škoda
9	[number]	k4	9
<g/>
Tr	Tr	k1gFnPc2	Tr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
chladič	chladič	k1gInSc1	chladič
nemá	mít	k5eNaImIp3nS	mít
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
ponechal	ponechat	k5eAaPmAgMnS	ponechat
alespoň	alespoň	k9	alespoň
tu	ten	k3xDgFnSc4	ten
lištu	lišta	k1gFnSc4	lišta
se	s	k7c7	s
znakem	znak	k1gInSc7	znak
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
se	se	k3xPyFc4	se
automobilka	automobilka	k1gFnSc1	automobilka
vrátila	vrátit	k5eAaPmAgFnS	vrátit
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
více	hodně	k6eAd2	hodně
typových	typový	k2eAgFnPc2d1	typová
řad	řada	k1gFnPc2	řada
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
vozu	vůz	k1gInSc3	vůz
Škoda	škoda	k1gFnSc1	škoda
Octavia	octavia	k1gFnSc1	octavia
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
na	na	k7c6	na
předku	předek	k1gInSc6	předek
nesla	nést	k5eAaImAgFnS	nést
dominantní	dominantní	k2eAgFnSc4d1	dominantní
masku	maska	k1gFnSc4	maska
chladiče	chladič	k1gInSc2	chladič
–	–	k?	–
ležatý	ležatý	k2eAgInSc1d1	ležatý
obdélník	obdélník	k1gInSc1	obdélník
se	s	k7c7	s
svisle	svisle	k6eAd1	svisle
orientovanými	orientovaný	k2eAgInPc7d1	orientovaný
jemnými	jemný	k2eAgInPc7d1	jemný
žebry	žebr	k1gInPc7	žebr
a	a	k8xC	a
s	s	k7c7	s
výrazně	výrazně	k6eAd1	výrazně
chromovaným	chromovaný	k2eAgInSc7d1	chromovaný
rámečkem	rámeček	k1gInSc7	rámeček
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
zvaný	zvaný	k2eAgInSc1d1	zvaný
"	"	kIx"	"
<g/>
gril	gril	k1gInSc1	gril
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgInSc2	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
integrován	integrován	k2eAgInSc1d1	integrován
znak	znak	k1gInSc1	znak
Škoda	škoda	k1gFnSc1	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
dostala	dostat	k5eAaPmAgFnS	dostat
obdobnou	obdobný	k2eAgFnSc4d1	obdobná
masku	maska	k1gFnSc4	maska
i	i	k9	i
faceliftovaná	faceliftovaný	k2eAgFnSc1d1	faceliftovaná
Škoda	škoda	k1gFnSc1	škoda
Felicia	felicia	k1gFnSc1	felicia
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
všechny	všechen	k3xTgInPc1	všechen
další	další	k2eAgInPc1d1	další
nové	nový	k2eAgInPc1d1	nový
modely	model	k1gInPc1	model
automobilů	automobil	k1gInPc2	automobil
Škoda	škoda	k6eAd1	škoda
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tvar	tvar	k1gInSc4	tvar
se	se	k3xPyFc4	se
přizpůsoboval	přizpůsobovat	k5eAaImAgMnS	přizpůsobovat
tvaru	tvar	k1gInSc3	tvar
vozu	vůz	k1gInSc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Společným	společný	k2eAgInSc7d1	společný
prvkem	prvek	k1gInSc7	prvek
byl	být	k5eAaImAgInS	být
i	i	k9	i
dvojitý	dvojitý	k2eAgInSc1d1	dvojitý
vystupující	vystupující	k2eAgInSc1d1	vystupující
prolis	prolis	k1gInSc1	prolis
kapoty	kapota	k1gFnSc2	kapota
<g/>
,	,	kIx,	,
navazující	navazující	k2eAgFnSc2d1	navazující
na	na	k7c4	na
masku	maska	k1gFnSc4	maska
a	a	k8xC	a
znak	znak	k1gInSc4	znak
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
mají	mít	k5eAaImIp3nP	mít
škodovky	škodovka	k1gFnPc4	škodovka
společný	společný	k2eAgInSc4d1	společný
znak	znak	k1gInSc4	znak
i	i	k9	i
vzadu	vzadu	k6eAd1	vzadu
<g/>
:	:	kIx,	:
velkoplošné	velkoplošný	k2eAgFnPc1d1	velkoplošná
zadní	zadní	k2eAgFnPc1d1	zadní
svítilny	svítilna	k1gFnPc1	svítilna
mají	mít	k5eAaImIp3nP	mít
obvodová	obvodový	k2eAgNnPc4d1	obvodové
koncová	koncový	k2eAgNnPc4d1	koncové
světla	světlo	k1gNnPc4	světlo
a	a	k8xC	a
proto	proto	k8xC	proto
svítí	svítit	k5eAaImIp3nS	svítit
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
písmene	písmeno	k1gNnSc2	písmeno
C	C	kA	C
(	(	kIx(	(
<g/>
vlevo	vlevo	k6eAd1	vlevo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
obráceného	obrácený	k2eAgInSc2d1	obrácený
C	C	kA	C
(	(	kIx(	(
<g/>
vpravo	vpravo	k6eAd1	vpravo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
vozů	vůz	k1gInPc2	vůz
Škoda	Škoda	k1gMnSc1	Škoda
Yeti	yeti	k1gMnSc1	yeti
a	a	k8xC	a
modernizací	modernizace	k1gFnPc2	modernizace
ostatních	ostatní	k2eAgInPc2d1	ostatní
typů	typ	k1gInPc2	typ
dostaly	dostat	k5eAaPmAgFnP	dostat
škodovky	škodovka	k1gFnPc1	škodovka
třetí	třetí	k4xOgInSc1	třetí
společný	společný	k2eAgInSc1d1	společný
rys	rys	k1gInSc1	rys
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
zboku	zboku	k6eAd1	zboku
<g/>
:	:	kIx,	:
okna	okno	k1gNnPc4	okno
u	u	k7c2	u
B-sloupků	Bloupek	k1gInPc2	B-sloupek
mají	mít	k5eAaImIp3nP	mít
nahoře	nahoře	k6eAd1	nahoře
ostré	ostrý	k2eAgInPc1d1	ostrý
a	a	k8xC	a
dole	dole	k6eAd1	dole
oblé	oblý	k2eAgFnPc4d1	oblá
hrany	hrana	k1gFnPc4	hrana
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
ještě	ještě	k6eAd1	ještě
opticky	opticky	k6eAd1	opticky
zmohutnily	zmohutnit	k5eAaPmAgFnP	zmohutnit
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
novým	nový	k2eAgInSc7d1	nový
vozem	vůz	k1gInSc7	vůz
Škoda	Škoda	k1gMnSc1	Škoda
Rapid	rapid	k1gInSc4	rapid
zahájil	zahájit	k5eAaPmAgMnS	zahájit
návrhář	návrhář	k1gMnSc1	návrhář
Jozef	Jozef	k1gMnSc1	Jozef
Kabaň	Kabaň	k1gMnSc1	Kabaň
odklon	odklon	k1gInSc4	odklon
designu	design	k1gInSc2	design
Škody	škoda	k1gFnSc2	škoda
od	od	k7c2	od
robustnosti	robustnost	k1gFnSc2	robustnost
k	k	k7c3	k
čistým	čistý	k2eAgFnPc3d1	čistá
liniím	linie	k1gFnPc3	linie
<g/>
,	,	kIx,	,
zjednodušení	zjednodušení	k1gNnSc4	zjednodušení
tvaru	tvar	k1gInSc2	tvar
reflektorů	reflektor	k1gInPc2	reflektor
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
roviny	rovina	k1gFnSc2	rovina
a	a	k8xC	a
přemístění	přemístění	k1gNnSc2	přemístění
znaku	znak	k1gInSc2	znak
automobilky	automobilka	k1gFnSc2	automobilka
z	z	k7c2	z
masky	maska	k1gFnSc2	maska
chladiče	chladič	k1gInSc2	chladič
do	do	k7c2	do
výběžku	výběžek	k1gInSc2	výběžek
přední	přední	k2eAgFnSc2d1	přední
kapoty	kapota	k1gFnSc2	kapota
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
typové	typový	k2eAgInPc1d1	typový
řady	řad	k1gInPc1	řad
budou	být	k5eAaImBp3nP	být
přizpůsobeny	přizpůsobit	k5eAaPmNgInP	přizpůsobit
při	při	k7c6	při
postupných	postupný	k2eAgInPc6d1	postupný
faceliftech	facelift	k1gInPc6	facelift
<g/>
.	.	kIx.	.
</s>
