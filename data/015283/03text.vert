<s>
Steven	Steven	k2eAgInSc1d1
Wayne	Wayn	k1gInSc5
Lindsey	Lindsea	k1gFnPc1
</s>
<s>
Steven	Steven	k2eAgInSc1d1
Wayne	Wayn	k1gInSc5
Lindsey	Lindsey	k1gInPc1
Steven	Steven	k2eAgInSc1d1
Wayne	Wayn	k1gInSc5
LindseyAstronaut	LindseyAstronaut	k1gInSc4
NASA	NASA	kA
Státní	státní	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
</s>
<s>
USA	USA	kA
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1960	#num#	k4
(	(	kIx(
<g/>
60	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Arcadia	Arcadium	k1gNnPc1
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc1
Předchozízaměstnání	Předchozízaměstnání	k1gNnSc2
</s>
<s>
pilot	pilot	k1gMnSc1
Hodnost	hodnost	k1gFnSc1
</s>
<s>
podplukovník	podplukovník	k1gMnSc1
Čas	čas	k1gInSc1
ve	v	k7c6
vesmíru	vesmír	k1gInSc6
</s>
<s>
62	#num#	k4
dní	den	k1gInPc2
<g/>
,	,	kIx,
22	#num#	k4
hodin	hodina	k1gFnPc2
a	a	k8xC
33	#num#	k4
minut	minuta	k1gFnPc2
Kosmonaut	kosmonaut	k1gMnSc1
od	od	k7c2
</s>
<s>
1994	#num#	k4
Mise	mise	k1gFnSc1
</s>
<s>
STS-	STS-	k?
<g/>
87	#num#	k4
<g/>
,	,	kIx,
STS-	STS-	k1gFnSc1
<g/>
95	#num#	k4
<g/>
,	,	kIx,
STS-	STS-	k1gFnSc1
<g/>
104	#num#	k4
<g/>
,	,	kIx,
STS-	STS-	k1gFnSc1
<g/>
121	#num#	k4
<g/>
,	,	kIx,
STS-133	STS-133	k1gFnSc2
Znaky	znak	k1gInPc1
misí	mise	k1gFnPc2
</s>
<s>
Kosmonaut	kosmonaut	k1gMnSc1
do	do	k7c2
</s>
<s>
2011	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Steven	Steven	k2eAgInSc1d1
Wayne	Wayn	k1gInSc5
Lindsey	Lindseum	k1gNnPc7
(	(	kIx(
<g/>
*	*	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1960	#num#	k4
v	v	k7c6
Arcadii	Arcadie	k1gFnSc6
<g/>
,	,	kIx,
stát	stát	k1gInSc1
Kalifornie	Kalifornie	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
americký	americký	k2eAgMnSc1d1
pilot	pilot	k1gMnSc1
<g/>
,	,	kIx,
důstojník	důstojník	k1gMnSc1
a	a	k8xC
kosmonaut	kosmonaut	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vesmíru	vesmír	k1gInSc6
byl	být	k5eAaImAgInS
pětkrát	pětkrát	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Studium	studium	k1gNnSc1
a	a	k8xC
zaměstnání	zaměstnání	k1gNnSc1
</s>
<s>
Absolvoval	absolvovat	k5eAaPmAgMnS
střední	střední	k2eAgFnSc4d1
školu	škola	k1gFnSc4
Temple	templ	k1gInSc5
City	city	k1gNnSc1
High	Higha	k1gFnPc2
School	Schoola	k1gFnPc2
v	v	k7c6
městě	město	k1gNnSc6
Temple	templ	k1gInSc5
City	city	k1gNnSc1
(	(	kIx(
<g/>
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
po	po	k7c6
jejím	její	k3xOp3gNnSc6
ukončení	ukončení	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
pokračoval	pokračovat	k5eAaImAgInS
ve	v	k7c6
studiu	studio	k1gNnSc6
na	na	k7c6
armádní	armádní	k2eAgFnSc6d1
letecké	letecký	k2eAgFnSc6d1
akademii	akademie	k1gFnSc6
USAF	USAF	kA
Academy	Academa	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukončil	ukončit	k5eAaPmAgMnS
jej	on	k3xPp3gNnSc4
v	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
si	se	k3xPyFc3
doplnil	doplnit	k5eAaPmAgMnS
vzdělání	vzdělání	k1gNnSc4
na	na	k7c4
USAF	USAF	kA
Institute	institut	k1gInSc5
of	of	k?
Technology	technolog	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Vojenským	vojenský	k2eAgInSc7d1
pilotem	pilot	k1gInSc7
byl	být	k5eAaImAgInS
v	v	k7c6
letech	léto	k1gNnPc6
1982	#num#	k4
až	až	k9
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
se	se	k3xPyFc4
zapojil	zapojit	k5eAaPmAgInS
do	do	k7c2
výcviku	výcvik	k1gInSc2
budoucích	budoucí	k2eAgMnPc2d1
kosmonautů	kosmonaut	k1gMnPc2
v	v	k7c6
Houstonu	Houston	k1gInSc6
<g/>
,	,	kIx,
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
tamní	tamní	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
kosmonautů	kosmonaut	k1gMnPc2
v	v	k7c6
NASA	NASA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zůstal	zůstat	k5eAaPmAgMnS
zde	zde	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Oženil	oženit	k5eAaPmAgInS
se	se	k3xPyFc4
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc7
manželkou	manželka	k1gFnSc7
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
Diane	Dian	k1gInSc5
Renee	Renee	k1gFnSc6
rozená	rozený	k2eAgFnSc1d1
Trujilo	Trujilo	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mají	mít	k5eAaImIp3nP
spolu	spolu	k6eAd1
tři	tři	k4xCgFnPc4
děti	dítě	k1gFnPc4
<g/>
,	,	kIx,
Jessicu	Jessica	k1gFnSc4
<g/>
,	,	kIx,
Jasona	Jasona	k1gFnSc1
a	a	k8xC
Jill	Jill	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Lety	let	k1gInPc1
do	do	k7c2
vesmíru	vesmír	k1gInSc2
</s>
<s>
Na	na	k7c4
oběžnou	oběžný	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
se	se	k3xPyFc4
v	v	k7c6
raketoplánu	raketoplán	k1gInSc6
dostal	dostat	k5eAaPmAgMnS
pětkrát	pětkrát	k6eAd1
<g/>
,	,	kIx,
pracoval	pracovat	k5eAaImAgMnS
na	na	k7c6
orbitální	orbitální	k2eAgFnSc6d1
stanici	stanice	k1gFnSc6
ISS	ISS	kA
a	a	k8xC
strávil	strávit	k5eAaPmAgInS
ve	v	k7c6
vesmíru	vesmír	k1gInSc6
62	#num#	k4
dní	den	k1gInPc2
<g/>
,	,	kIx,
22	#num#	k4
hodin	hodina	k1gFnPc2
a	a	k8xC
33	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
365	#num#	k4
člověkem	člověk	k1gMnSc7
ve	v	k7c6
vesmíru	vesmír	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
STS-87	STS-87	k4
Columbia	Columbia	k1gFnSc1
(	(	kIx(
<g/>
19	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1997	#num#	k4
–	–	k?
5	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1997	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pilot	pilot	k1gMnSc1
</s>
<s>
STS-95	STS-95	k4
Discovery	Discovera	k1gFnPc1
(	(	kIx(
<g/>
29	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
1998	#num#	k4
–	–	k?
7	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
1998	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pilot	pilot	k1gMnSc1
</s>
<s>
STS-104	STS-104	k4
Atlantis	Atlantis	k1gFnSc1
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2001	#num#	k4
-	-	kIx~
25	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
velitel	velitel	k1gMnSc1
</s>
<s>
STS-121	STS-121	k4
Discovery	Discovera	k1gFnPc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2006	#num#	k4
–	–	k?
17	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1998	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
velitel	velitel	k1gMnSc1
</s>
<s>
STS-133	STS-133	k4
Discovery	Discovera	k1gFnPc1
(	(	kIx(
<g/>
24	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2011	#num#	k4
–	–	k?
9	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1998	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
velitel	velitel	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Steven	Stevna	k1gFnPc2
Wayne	Wayn	k1gInSc5
Lindsey	Lindse	k2eAgFnPc4d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Na	na	k7c6
webu	web	k1gInSc6
Space	Space	k1gFnSc2
</s>
<s>
Na	na	k7c6
webu	web	k1gInSc6
MEK-Kosmo	MEK-Kosma	k1gFnSc5
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kosmonautika	kosmonautika	k1gFnSc1
</s>
