<s>
Caroline	Carolin	k1gInSc5	Carolin
Wozniacká	Wozniacký	k2eAgFnSc1d1	Wozniacká
<g/>
,	,	kIx,	,
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Caroline	Carolin	k1gInSc5	Carolin
Wozniacki	Wozniacki	k1gNnPc7	Wozniacki
(	(	kIx(	(
<g/>
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1990	[number]	k4	1990
Odense	Odense	k1gFnSc1	Odense
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dánská	dánský	k2eAgFnSc1d1	dánská
profesionální	profesionální	k2eAgFnSc1d1	profesionální
tenistka	tenistka	k1gFnSc1	tenistka
polského	polský	k2eAgInSc2d1	polský
původu	původ	k1gInSc2	původ
a	a	k8xC	a
bývalá	bývalý	k2eAgFnSc1d1	bývalá
světová	světový	k2eAgFnSc1d1	světová
jednička	jednička	k1gFnSc1	jednička
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2010	[number]	k4	2010
jako	jako	k8xS	jako
dvacátá	dvacátý	k4xOgFnSc1	dvacátý
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
a	a	k8xC	a
první	první	k4xOgMnSc1	první
ze	z	k7c2	z
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
strávila	strávit	k5eAaPmAgFnS	strávit
celkově	celkově	k6eAd1	celkově
67	[number]	k4	67
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
když	když	k8xS	když
vrchol	vrchol	k1gInSc4	vrchol
naposledy	naposledy	k6eAd1	naposledy
opustila	opustit	k5eAaPmAgFnS	opustit
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
profesionálkami	profesionálka	k1gFnPc7	profesionálka
debutovala	debutovat	k5eAaBmAgFnS	debutovat
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc2d1	následující
sezóny	sezóna	k1gFnSc2	sezóna
pokaždé	pokaždé	k6eAd1	pokaždé
zlepšovala	zlepšovat	k5eAaImAgFnS	zlepšovat
své	svůj	k3xOyFgNnSc4	svůj
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
žebříčku	žebříček	k1gInSc6	žebříček
WTA	WTA	kA	WTA
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
propracovala	propracovat	k5eAaPmAgFnS	propracovat
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
ji	on	k3xPp3gFnSc4	on
trénuje	trénovat	k5eAaImIp3nS	trénovat
otec	otec	k1gMnSc1	otec
Piotr	Piotr	k1gMnSc1	Piotr
Wozniacki	Wozniacke	k1gFnSc4	Wozniacke
<g/>
;	;	kIx,	;
mezi	mezi	k7c7	mezi
dubnem	duben	k1gInSc7	duben
až	až	k8xS	až
srpnem	srpen	k1gInSc7	srpen
2016	[number]	k4	2016
ji	on	k3xPp3gFnSc4	on
koučoval	koučovat	k5eAaImAgInS	koučovat
Čech	Čech	k1gMnSc1	Čech
David	David	k1gMnSc1	David
Kotyza	Kotyza	k1gFnSc1	Kotyza
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
dánský	dánský	k2eAgMnSc1d1	dánský
tenista	tenista	k1gMnSc1	tenista
se	se	k3xPyFc4	se
probojovala	probojovat	k5eAaPmAgFnS	probojovat
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
grandslamového	grandslamový	k2eAgInSc2d1	grandslamový
turnaje	turnaj	k1gInSc2	turnaj
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
éře	éra	k1gFnSc6	éra
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1968	[number]	k4	1968
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
již	již	k6eAd1	již
objevil	objevit	k5eAaPmAgMnS	objevit
krajan	krajan	k1gMnSc1	krajan
Kurt	Kurt	k1gMnSc1	Kurt
Nielsen	Nielsno	k1gNnPc2	Nielsno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
US	US	kA	US
Open	Open	k1gInSc4	Open
2009	[number]	k4	2009
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
Belgičanku	Belgičanka	k1gFnSc4	Belgičanka
Kim	Kim	k1gFnSc4	Kim
Clijstersovou	Clijstersová	k1gFnSc4	Clijstersová
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
na	na	k7c4	na
US	US	kA	US
Open	Open	k1gInSc4	Open
2014	[number]	k4	2014
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
Američance	Američanka	k1gFnSc3	Američanka
Sereně	Serena	k1gFnSc3	Serena
Williamsové	Williamsová	k1gFnSc3	Williamsová
<g/>
.	.	kIx.	.
</s>
<s>
Zařadila	zařadit	k5eAaPmAgFnS	zařadit
se	se	k3xPyFc4	se
tak	tak	k9	tak
mezi	mezi	k7c4	mezi
pět	pět	k4xCc4	pět
světových	světový	k2eAgFnPc2d1	světová
jedniček	jednička	k1gFnPc2	jednička
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
usedly	usednout	k5eAaPmAgFnP	usednout
na	na	k7c4	na
"	"	kIx"	"
<g/>
tenisový	tenisový	k2eAgInSc4d1	tenisový
trůn	trůn	k1gInSc4	trůn
<g/>
"	"	kIx"	"
bez	bez	k7c2	bez
grandslamového	grandslamový	k2eAgInSc2d1	grandslamový
titulu	titul	k1gInSc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2010	[number]	k4	2010
a	a	k8xC	a
2011	[number]	k4	2011
zakončila	zakončit	k5eAaPmAgFnS	zakončit
sezónu	sezóna	k1gFnSc4	sezóna
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
juniorské	juniorský	k2eAgFnSc6d1	juniorská
kategorii	kategorie	k1gFnSc6	kategorie
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
WTA	WTA	kA	WTA
Tour	Tour	k1gMnSc1	Tour
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
dvacet	dvacet	k4xCc4	dvacet
pět	pět	k4xCc4	pět
turnajů	turnaj	k1gInPc2	turnaj
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
šest	šest	k4xCc1	šest
singlových	singlový	k2eAgMnPc2d1	singlový
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2011	[number]	k4	2011
<g/>
..	..	k?	..
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Turnaje	turnaj	k1gInSc2	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
2010	[number]	k4	2010
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
Clijstersové	Clijstersové	k2eAgFnSc1d1	Clijstersové
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
okruhu	okruh	k1gInSc2	okruh
ITF	ITF	kA	ITF
získala	získat	k5eAaPmAgFnS	získat
čtyři	čtyři	k4xCgInPc4	čtyři
tituly	titul	k1gInPc4	titul
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dánském	dánský	k2eAgInSc6d1	dánský
fedcupovém	fedcupový	k2eAgInSc6d1	fedcupový
týmu	tým	k1gInSc6	tým
debutovala	debutovat	k5eAaBmAgFnS	debutovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
utkáním	utkání	k1gNnSc7	utkání
základního	základní	k2eAgInSc2d1	základní
bloku	blok	k1gInSc2	blok
1	[number]	k4	1
<g/>
.	.	kIx.	.
skupiny	skupina	k1gFnSc2	skupina
zóny	zóna	k1gFnSc2	zóna
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Afriky	Afrika	k1gFnSc2	Afrika
proti	proti	k7c3	proti
Srbsku	Srbsko	k1gNnSc3	Srbsko
a	a	k8xC	a
Černé	Černá	k1gFnSc3	Černá
Hoře	hora	k1gFnSc3	hora
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
prohrála	prohrát	k5eAaPmAgFnS	prohrát
dvouhru	dvouhra	k1gFnSc4	dvouhra
s	s	k7c7	s
Anou	Anoa	k1gMnSc4	Anoa
Jovanovićovou	Jovanovićový	k2eAgFnSc7d1	Jovanovićový
<g/>
.	.	kIx.	.
</s>
<s>
Dánky	Dánka	k1gFnPc1	Dánka
odešly	odejít	k5eAaPmAgFnP	odejít
poraženy	poražen	k2eAgInPc4d1	poražen
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
k	k	k7c3	k
dvaadvaceti	dvaadvacet	k4xCc3	dvaadvacet
mezistátním	mezistátní	k2eAgNnPc3d1	mezistátní
utkáním	utkání	k1gNnSc7	utkání
s	s	k7c7	s
bilancí	bilance	k1gFnSc7	bilance
17	[number]	k4	17
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
a	a	k8xC	a
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Dánsko	Dánsko	k1gNnSc4	Dánsko
reprezentovala	reprezentovat	k5eAaImAgFnS	reprezentovat
na	na	k7c6	na
londýnských	londýnský	k2eAgFnPc6d1	londýnská
Hrách	hra	k1gFnPc6	hra
XXX	XXX	kA	XXX
<g/>
.	.	kIx.	.
olympiády	olympiáda	k1gFnSc2	olympiáda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
prohrála	prohrát	k5eAaPmAgFnS	prohrát
jako	jako	k9	jako
nasazená	nasazený	k2eAgFnSc1d1	nasazená
osmička	osmička	k1gFnSc1	osmička
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
s	s	k7c7	s
pozdější	pozdní	k2eAgFnSc7d2	pozdější
vítězkou	vítězka	k1gFnSc7	vítězka
Serenou	Serený	k2eAgFnSc4d1	Serený
Williamsovou	Williamsová	k1gFnSc4	Williamsová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Pekingských	pekingský	k2eAgFnPc6d1	Pekingská
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2008	[number]	k4	2008
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
singlu	singl	k1gInSc2	singl
opět	opět	k6eAd1	opět
s	s	k7c7	s
vítězkou	vítězka	k1gFnSc7	vítězka
turnaje	turnaj	k1gInSc2	turnaj
Jelenou	Jelena	k1gFnSc7	Jelena
Dementěvovou	Dementěvová	k1gFnSc7	Dementěvová
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dánské	dánský	k2eAgNnSc4d1	dánské
družstvo	družstvo	k1gNnSc4	družstvo
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
také	také	k9	také
do	do	k7c2	do
Hopmanova	Hopmanův	k2eAgInSc2d1	Hopmanův
poháru	pohár	k1gInSc2	pohár
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
páru	pár	k1gInSc6	pár
s	s	k7c7	s
Frederikem	Frederik	k1gMnSc7	Frederik
Nielsenem	Nielsen	k1gMnSc7	Nielsen
obsadili	obsadit	k5eAaPmAgMnP	obsadit
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
základní	základní	k2eAgFnSc2d1	základní
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zahajovacím	zahajovací	k2eAgInSc6d1	zahajovací
ceremoniálu	ceremoniál	k1gInSc6	ceremoniál
Letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
2016	[number]	k4	2016
v	v	k7c4	v
Riu	Riu	k1gFnSc4	Riu
de	de	k?	de
Janeiru	Janeira	k1gFnSc4	Janeira
byla	být	k5eAaImAgFnS	být
vlajkonoškou	vlajkonoška	k1gFnSc7	vlajkonoška
dánské	dánský	k2eAgFnSc2d1	dánská
výpravy	výprava	k1gFnSc2	výprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
singlu	singl	k1gInSc6	singl
pak	pak	k6eAd1	pak
skončila	skončit	k5eAaPmAgFnS	skončit
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
Petry	Petra	k1gFnSc2	Petra
Kvitové	Kvitová	k1gFnSc2	Kvitová
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
v	v	k7c6	v
dánském	dánský	k2eAgInSc6d1	dánský
Odense	Odens	k1gInSc6	Odens
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
polského	polský	k2eAgMnSc2d1	polský
fotbalisty	fotbalista	k1gMnSc2	fotbalista
Piotra	Piotr	k1gMnSc2	Piotr
Wozniackého	Wozniacký	k1gMnSc2	Wozniacký
a	a	k8xC	a
bývalé	bývalý	k2eAgFnSc2d1	bývalá
polské	polský	k2eAgFnSc2d1	polská
reprezentantky	reprezentantka	k1gFnSc2	reprezentantka
ve	v	k7c6	v
volejbalu	volejbal	k1gInSc6	volejbal
Anny	Anna	k1gFnSc2	Anna
Wozniacké	Wozniacký	k2eAgFnSc2d1	Wozniacká
<g/>
.	.	kIx.	.
</s>
<s>
Bratr	bratr	k1gMnSc1	bratr
Patrick	Patrick	k1gMnSc1	Patrick
Wozniacki	Wozniacki	k1gNnSc4	Wozniacki
je	být	k5eAaImIp3nS	být
profesionální	profesionální	k2eAgMnSc1d1	profesionální
fotbalista	fotbalista	k1gMnSc1	fotbalista
<g/>
,	,	kIx,	,
nastupující	nastupující	k2eAgFnSc1d1	nastupující
za	za	k7c4	za
kodaňský	kodaňský	k2eAgInSc4d1	kodaňský
klub	klub	k1gInSc4	klub
Frem	Frema	k1gFnPc2	Frema
<g/>
.	.	kIx.	.
</s>
<s>
Plynně	plynně	k6eAd1	plynně
hovoří	hovořit	k5eAaImIp3nS	hovořit
polsky	polsky	k6eAd1	polsky
<g/>
,	,	kIx,	,
dánsky	dánsky	k6eAd1	dánsky
a	a	k8xC	a
anglicky	anglicky	k6eAd1	anglicky
<g/>
.	.	kIx.	.
</s>
<s>
Tříletý	tříletý	k2eAgInSc4d1	tříletý
kontrakt	kontrakt	k1gInSc4	kontrakt
s	s	k7c7	s
leteckou	letecký	k2eAgFnSc7d1	letecká
společností	společnost	k1gFnSc7	společnost
Turkish	Turkisha	k1gFnPc2	Turkisha
Airlines	Airlinesa	k1gFnPc2	Airlinesa
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
náplní	náplň	k1gFnSc7	náplň
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
podpora	podpora	k1gFnSc1	podpora
business	business	k1gInSc4	business
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
k	k	k7c3	k
fanouškům	fanoušek	k1gMnPc3	fanoušek
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
klubu	klub	k1gInSc2	klub
Liverpool	Liverpool	k1gInSc1	Liverpool
FC	FC	kA	FC
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
turnaje	turnaj	k1gInSc2	turnaj
Qatar	Qatar	k1gMnSc1	Qatar
Ladies	Ladies	k1gMnSc1	Ladies
Open	Open	k1gInSc4	Open
2011	[number]	k4	2011
hrála	hrát	k5eAaImAgFnS	hrát
s	s	k7c7	s
kšiltovkou	kšiltovka	k1gFnSc7	kšiltovka
liverpoolského	liverpoolský	k2eAgInSc2d1	liverpoolský
týmu	tým	k1gInSc2	tým
podepsanou	podepsaný	k2eAgFnSc4d1	podepsaná
Stevenem	Steven	k1gMnSc7	Steven
Gerrardem	Gerrard	k1gMnSc7	Gerrard
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
časopisu	časopis	k1gInSc2	časopis
Forbes	forbes	k1gInSc1	forbes
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
stala	stát	k5eAaPmAgFnS	stát
druhou	druhý	k4xOgFnSc4	druhý
nejlépe	dobře	k6eAd3	dobře
vydělávající	vydělávající	k2eAgFnSc7d1	vydělávající
sportovkyní	sportovkyně	k1gFnSc7	sportovkyně
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Britský	britský	k2eAgInSc1d1	britský
měsíčník	měsíčník	k1gInSc1	měsíčník
SportsPro	SportsPro	k1gNnSc1	SportsPro
ji	on	k3xPp3gFnSc4	on
ve	v	k7c6	v
vydání	vydání	k1gNnSc6	vydání
z	z	k7c2	z
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
zařadil	zařadit	k5eAaPmAgInS	zařadit
na	na	k7c4	na
deváté	devátý	k4xOgNnSc4	devátý
místo	místo	k1gNnSc4	místo
sportovkyň	sportovkyně	k1gFnPc2	sportovkyně
s	s	k7c7	s
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
zpeněžitelným	zpeněžitelný	k2eAgInSc7d1	zpeněžitelný
potenciálem	potenciál	k1gInSc7	potenciál
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2011	[number]	k4	2011
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
udržovala	udržovat	k5eAaImAgFnS	udržovat
partnerský	partnerský	k2eAgInSc4d1	partnerský
vztah	vztah	k1gInSc4	vztah
se	s	k7c7	s
severoirským	severoirský	k2eAgMnSc7d1	severoirský
golfistou	golfista	k1gMnSc7	golfista
Rorym	Rorym	k1gInSc1	Rorym
McIlroyem	McIlroy	k1gInSc7	McIlroy
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
něho	on	k3xPp3gNnSc2	on
byli	být	k5eAaImAgMnP	být
oba	dva	k4xCgMnPc1	dva
partneři	partner	k1gMnPc1	partner
světovými	světový	k2eAgFnPc7d1	světová
jedničkami	jednička	k1gFnPc7	jednička
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
sportech	sport	k1gInPc6	sport
<g/>
.	.	kIx.	.
</s>
<s>
Zasnoubení	zasnoubení	k1gNnPc4	zasnoubení
Wozniacká	Wozniacký	k2eAgFnSc1d1	Wozniacká
oznámila	oznámit	k5eAaPmAgFnS	oznámit
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
McIlroyově	McIlroyův	k2eAgNnSc6d1	McIlroyův
zrušení	zrušení	k1gNnSc6	zrušení
zásnub	zásnuba	k1gFnPc2	zásnuba
byla	být	k5eAaImAgFnS	být
medializována	medializovat	k5eAaImNgFnS	medializovat
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
když	když	k8xS	když
golfista	golfista	k1gMnSc1	golfista
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Problém	problém	k1gInSc1	problém
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
mé	můj	k3xOp1gFnSc6	můj
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
tento	tento	k3xDgInSc4	tento
víkend	víkend	k1gInSc4	víkend
vyšla	vyjít	k5eAaPmAgFnS	vyjít
svatební	svatební	k2eAgNnSc4d1	svatební
oznámení	oznámení	k1gNnSc4	oznámení
<g/>
,	,	kIx,	,
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejsem	být	k5eNaImIp1nS	být
připraven	připravit	k5eAaPmNgMnS	připravit
na	na	k7c4	na
všechno	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
manželství	manželství	k1gNnSc1	manželství
obnáší	obnášet	k5eAaImIp3nS	obnášet
<g/>
.	.	kIx.	.
</s>
<s>
Caroline	Carolin	k1gInSc5	Carolin
přeji	přát	k5eAaImIp1nS	přát
hodně	hodně	k6eAd1	hodně
štěstí	štěstí	k1gNnSc1	štěstí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
si	se	k3xPyFc3	se
zaslouží	zasloužit	k5eAaPmIp3nS	zasloužit
<g/>
,	,	kIx,	,
a	a	k8xC	a
děkuji	děkovat	k5eAaImIp1nS	děkovat
jí	jíst	k5eAaImIp3nS	jíst
za	za	k7c4	za
nádherný	nádherný	k2eAgInSc4d1	nádherný
čas	čas	k1gInSc4	čas
prožitý	prožitý	k2eAgInSc4d1	prožitý
spolu	spolu	k6eAd1	spolu
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
24	[number]	k4	24
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
Newyorský	newyorský	k2eAgInSc4d1	newyorský
maraton	maraton	k1gInSc4	maraton
za	za	k7c4	za
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
26,33	[number]	k4	26,33
hod	hod	k1gInSc1	hod
(	(	kIx(	(
<g/>
tři	tři	k4xCgFnPc1	tři
hodiny	hodina	k1gFnPc1	hodina
dvacet	dvacet	k4xCc1	dvacet
šest	šest	k4xCc1	šest
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
třicet	třicet	k4xCc1	třicet
tři	tři	k4xCgFnPc4	tři
sekund	sekunda	k1gFnPc2	sekunda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naplnila	naplnit	k5eAaPmAgFnS	naplnit
tak	tak	k9	tak
svá	svůj	k3xOyFgNnPc4	svůj
očekávání	očekávání	k1gNnPc4	očekávání
času	čas	k1gInSc2	čas
pod	pod	k7c4	pod
tři	tři	k4xCgInPc4	tři
a	a	k8xC	a
půl	půl	k1xP	půl
hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
doběhu	doběh	k1gInSc6	doběh
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nic	nic	k3yNnSc1	nic
tak	tak	k9	tak
těžkého	těžký	k2eAgNnSc2d1	těžké
jsem	být	k5eAaImIp1nS	být
ještě	ještě	k6eAd1	ještě
nezkusila	zkusit	k5eNaPmAgFnS	zkusit
...	...	k?	...
Po	po	k7c6	po
třicátém	třicátý	k4xOgInSc6	třicátý
kilometru	kilometr	k1gInSc6	kilometr
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
říkala	říkat	k5eAaImAgFnS	říkat
<g/>
,	,	kIx,	,
něco	něco	k3yInSc1	něco
takového	takový	k3xDgNnSc2	takový
už	už	k9	už
nikdy	nikdy	k6eAd1	nikdy
nepoběžím	běžet	k5eNaImIp1nS	běžet
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
cíli	cíl	k1gInSc6	cíl
ji	on	k3xPp3gFnSc4	on
očekávala	očekávat	k5eAaImAgFnS	očekávat
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
z	z	k7c2	z
profesionálního	profesionální	k2eAgInSc2d1	profesionální
okruhu	okruh	k1gInSc2	okruh
a	a	k8xC	a
úřadující	úřadující	k2eAgFnSc1d1	úřadující
světová	světový	k2eAgFnSc1d1	světová
jednička	jednička	k1gFnSc1	jednička
Serena	Serena	k1gFnSc1	Serena
Williamsová	Williamsová	k1gFnSc1	Williamsová
<g/>
.	.	kIx.	.
</s>
<s>
Wozniacká	Wozniacký	k2eAgFnSc1d1	Wozniacká
běžela	běžet	k5eAaImAgFnS	běžet
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
1	[number]	k4	1
700	[number]	k4	700
<g/>
členného	členný	k2eAgNnSc2d1	členné
charitativního	charitativní	k2eAgNnSc2d1	charitativní
družstva	družstvo	k1gNnSc2	družstvo
"	"	kIx"	"
<g/>
Team	team	k1gInSc1	team
for	forum	k1gNnPc2	forum
Kids	Kids	k1gInSc1	Kids
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
stálým	stálý	k2eAgMnSc7d1	stálý
trenérem	trenér	k1gMnSc7	trenér
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
jejích	její	k3xOp3gNnPc2	její
čtrnácti	čtrnáct	k4xCc2	čtrnáct
let	léto	k1gNnPc2	léto
otec	otec	k1gMnSc1	otec
Piotr	Piotr	k1gMnSc1	Piotr
Wozniacki	Wozniack	k1gMnSc3	Wozniack
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
vede	vést	k5eAaImIp3nS	vést
i	i	k9	i
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
kouči	kouč	k1gMnPc7	kouč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zapojila	zapojit	k5eAaPmAgFnS	zapojit
do	do	k7c2	do
rozvojového	rozvojový	k2eAgInSc2d1	rozvojový
programu	program	k1gInSc2	program
firmy	firma	k1gFnSc2	firma
Adidas	Adidasa	k1gFnPc2	Adidasa
pro	pro	k7c4	pro
zlepšování	zlepšování	k1gNnSc4	zlepšování
talentů	talent	k1gInPc2	talent
(	(	kIx(	(
<g/>
Adidas	Adidasa	k1gFnPc2	Adidasa
Player	Player	k1gInSc1	Player
Development	Development	k1gMnSc1	Development
Program	program	k1gInSc1	program
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
převzal	převzít	k5eAaPmAgInS	převzít
roli	role	k1gFnSc4	role
kouče	kouč	k1gMnSc2	kouč
Nizozemec	Nizozemec	k1gMnSc1	Nizozemec
Sven	Sven	k1gMnSc1	Sven
Groeneveld	Groeneveld	k1gMnSc1	Groeneveld
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
ji	on	k3xPp3gFnSc4	on
také	také	k9	také
vedli	vést	k5eAaImAgMnP	vést
Ricardo	Ricardo	k1gNnSc4	Ricardo
Sanchez	Sanchez	k1gMnSc1	Sanchez
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
a	a	k8xC	a
Thomas	Thomas	k1gMnSc1	Thomas
Johansson	Johansson	k1gMnSc1	Johansson
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2013	[number]	k4	2013
si	se	k3xPyFc3	se
najala	najmout	k5eAaPmAgFnS	najmout
Thomase	Thomas	k1gMnSc4	Thomas
Högstedta	Högstedt	k1gMnSc4	Högstedt
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
ji	on	k3xPp3gFnSc4	on
připravoval	připravovat	k5eAaImAgMnS	připravovat
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc4d1	stejný
měsíc	měsíc	k1gInSc4	měsíc
se	s	k7c7	s
hlavním	hlavní	k2eAgMnSc7d1	hlavní
trenérem	trenér	k1gMnSc7	trenér
stal	stát	k5eAaPmAgMnS	stát
bývalý	bývalý	k2eAgMnSc1d1	bývalý
dánský	dánský	k2eAgMnSc1d1	dánský
tenista	tenista	k1gMnSc1	tenista
Michael	Michael	k1gMnSc1	Michael
Mortensen	Mortensno	k1gNnPc2	Mortensno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2014	[number]	k4	2014
však	však	k9	však
oba	dva	k4xCgInPc4	dva
spolupráci	spolupráce	k1gFnSc6	spolupráce
ukončili	ukončit	k5eAaPmAgMnP	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dubna	duben	k1gInSc2	duben
2016	[number]	k4	2016
jej	on	k3xPp3gInSc4	on
však	však	k9	však
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
nahradil	nahradit	k5eAaPmAgMnS	nahradit
český	český	k2eAgMnSc1d1	český
trenér	trenér	k1gMnSc1	trenér
David	David	k1gMnSc1	David
Kotyza	Kotyza	k1gFnSc1	Kotyza
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
kouč	kouč	k1gMnSc1	kouč
Petry	Petra	k1gFnSc2	Petra
Kvitové	Kvitová	k1gFnSc2	Kvitová
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Statistiky	statistika	k1gFnSc2	statistika
WTA	WTA	kA	WTA
Tour	Tour	k1gInSc4	Tour
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
získala	získat	k5eAaPmAgFnS	získat
šest	šest	k4xCc4	šest
titulů	titul	k1gInPc2	titul
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Petrou	Petra	k1gFnSc7	Petra
Kvitovou	Kvitový	k2eAgFnSc7d1	Kvitová
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
tenistek	tenistka	k1gFnPc2	tenistka
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc4	rok
začala	začít	k5eAaPmAgFnS	začít
prohrou	prohra	k1gFnSc7	prohra
na	na	k7c4	na
Medibank	Medibank	k1gInSc4	Medibank
International	International	k1gFnSc2	International
Sydney	Sydney	k1gNnSc4	Sydney
s	s	k7c7	s
Dominikou	Dominika	k1gFnSc7	Dominika
Cibulkovou	Cibulková	k1gFnSc7	Cibulková
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
probojovala	probojovat	k5eAaPmAgFnS	probojovat
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
za	za	k7c2	za
sebou	se	k3xPyFc7	se
jdoucích	jdoucí	k2eAgNnPc2d1	jdoucí
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
Dubai	Dubai	k1gNnSc2	Dubai
Tennis	Tennis	k1gFnPc2	Tennis
Championships	Championshipsa	k1gFnPc2	Championshipsa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
zdolala	zdolat	k5eAaPmAgFnS	zdolat
Světlanu	Světlana	k1gFnSc4	Světlana
Kuzněcovovu	Kuzněcovův	k2eAgFnSc4d1	Kuzněcovův
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Qatar	Qatar	k1gMnSc1	Qatar
Ladies	Ladies	k1gMnSc1	Ladies
Open	Open	k1gMnSc1	Open
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
Věru	Věra	k1gFnSc4	Věra
Zvonarevovou	Zvonarevový	k2eAgFnSc4d1	Zvonarevová
po	po	k7c6	po
setech	set	k1gInPc6	set
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
v	v	k7c4	v
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wells	k1gInSc4	Wells
přehrála	přehrát	k5eAaPmAgFnS	přehrát
Marion	Marion	k1gInSc4	Marion
Bartoliovou	Bartoliový	k2eAgFnSc4d1	Bartoliová
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc4	třetí
titul	titul	k1gInSc4	titul
sezóny	sezóna	k1gFnSc2	sezóna
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
na	na	k7c4	na
Family	Famil	k1gMnPc4	Famil
Circle	Circle	k1gFnSc2	Circle
Cupu	cup	k1gInSc2	cup
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
porazila	porazit	k5eAaPmAgFnS	porazit
nenasazenou	nasazený	k2eNgFnSc4d1	nenasazená
Rusku	Ruska	k1gFnSc4	Ruska
Jelenu	Jelena	k1gFnSc4	Jelena
Vesninovou	Vesninový	k2eAgFnSc4d1	Vesninová
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Němkou	Němka	k1gFnSc7	Němka
Julií	Julie	k1gFnPc2	Julie
Görgeosovou	Görgeosový	k2eAgFnSc4d1	Görgeosový
prohrála	prohrát	k5eAaPmAgFnS	prohrát
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
na	na	k7c6	na
události	událost	k1gFnSc6	událost
Porsche	Porsche	k1gNnSc2	Porsche
Tennis	Tennis	k1gFnSc2	Tennis
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
výhru	výhra	k1gFnSc4	výhra
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
na	na	k7c6	na
úvodním	úvodní	k2eAgInSc6d1	úvodní
ročníku	ročník	k1gInSc6	ročník
Brussels	Brusselsa	k1gFnPc2	Brusselsa
Open	Open	k1gNnSc1	Open
<g/>
,	,	kIx,	,
po	po	k7c6	po
třísetové	třísetový	k2eAgFnSc6d1	třísetová
finálové	finálový	k2eAgFnSc6d1	finálová
bitvě	bitva	k1gFnSc6	bitva
s	s	k7c7	s
Číňankou	Číňanka	k1gFnSc7	Číňanka
Pcheng	Pcheng	k1gMnSc1	Pcheng
Šuaj	Šuaj	k1gMnSc1	Šuaj
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Pátý	pátý	k4xOgInSc1	pátý
vavřín	vavřín	k1gInSc1	vavřín
si	se	k3xPyFc3	se
odvezla	odvézt	k5eAaPmAgFnS	odvézt
z	z	k7c2	z
domácího	domácí	k2eAgInSc2d1	domácí
dánského	dánský	k2eAgInSc2d1	dánský
turnaje	turnaj	k1gInSc2	turnaj
e-Boks	e-Boksa	k1gFnPc2	e-Boksa
Sony	Sony	kA	Sony
Ericsson	Ericsson	kA	Ericsson
Open	Opena	k1gFnPc2	Opena
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
porazila	porazit	k5eAaPmAgFnS	porazit
Lucii	Lucie	k1gFnSc4	Lucie
Šafářovou	Šafářová	k1gFnSc4	Šafářová
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letní	letní	k2eAgFnSc6d1	letní
části	část	k1gFnSc6	část
okruhu	okruh	k1gInSc2	okruh
prohrála	prohrát	k5eAaPmAgFnS	prohrát
tři	tři	k4xCgInPc4	tři
zápasy	zápas	k1gInPc4	zápas
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
získala	získat	k5eAaPmAgFnS	získat
poslední	poslední	k2eAgFnSc1d1	poslední
šesté	šestý	k4xOgNnSc1	šestý
vítězství	vítězství	k1gNnSc1	vítězství
sezóny	sezóna	k1gFnSc2	sezóna
a	a	k8xC	a
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
turnajový	turnajový	k2eAgInSc4d1	turnajový
titul	titul	k1gInSc4	titul
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
New	New	k1gMnSc2	New
Haven	Havno	k1gNnPc2	Havno
Open	Openo	k1gNnPc2	Openo
at	at	k?	at
Yale	Yal	k1gFnSc2	Yal
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
poradila	poradit	k5eAaPmAgFnS	poradit
s	s	k7c7	s
kvalifikantkou	kvalifikantka	k1gFnSc7	kvalifikantka
Petrou	Petra	k1gFnSc7	Petra
Cetkovskou	Cetkovský	k2eAgFnSc4d1	Cetkovská
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
a	a	k8xC	a
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
grandslamech	grandslam	k1gInPc6	grandslam
postoupila	postoupit	k5eAaPmAgFnS	postoupit
do	do	k7c2	do
dvou	dva	k4xCgNnPc2	dva
semifinále	semifinále	k1gNnPc2	semifinále
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
na	na	k7c6	na
úvodním	úvodní	k2eAgInSc6d1	úvodní
Australian	Australian	k1gInSc1	Australian
Open	Open	k1gInSc4	Open
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
také	také	k9	také
na	na	k7c6	na
US	US	kA	US
Open	Open	k1gNnSc1	Open
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
případě	případ	k1gInSc6	případ
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
Li	li	k8xS	li
Na	na	k7c4	na
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
proměnila	proměnit	k5eAaPmAgFnS	proměnit
mečbol	mečbol	k1gInSc4	mečbol
a	a	k8xC	a
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
případě	případ	k1gInSc6	případ
na	na	k7c4	na
Serenu	Seren	k2eAgFnSc4d1	Serena
Williamsovou	Williamsová	k1gFnSc4	Williamsová
po	po	k7c6	po
prohře	prohra	k1gFnSc6	prohra
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
French	French	k1gInSc4	French
Open	Open	k1gInSc4	Open
nepřešla	přejít	k5eNaPmAgFnS	přejít
přes	přes	k7c4	přes
třetí	třetí	k4xOgNnSc4	třetí
kolo	kolo	k1gNnSc4	kolo
<g/>
,	,	kIx,	,
když	když	k8xS	když
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
Daniele	Daniela	k1gFnSc3	Daniela
Hantuchové	Hantuchový	k2eAgNnSc4d1	Hantuchové
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
a	a	k8xC	a
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
ji	on	k3xPp3gFnSc4	on
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
v	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
další	další	k2eAgFnSc1d1	další
Slovenka	Slovenka	k1gFnSc1	Slovenka
Dominika	Dominik	k1gMnSc2	Dominik
Cibulková	Cibulková	k1gFnSc1	Cibulková
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
sezónu	sezóna	k1gFnSc4	sezóna
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
zakončila	zakončit	k5eAaPmAgFnS	zakončit
jako	jako	k9	jako
světová	světový	k2eAgFnSc1d1	světová
jednička	jednička	k1gFnSc1	jednička
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
zahrála	zahrát	k5eAaPmAgFnS	zahrát
tři	tři	k4xCgNnPc4	tři
finále	finále	k1gNnPc2	finále
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
na	na	k7c4	na
Istanbul	Istanbul	k1gInSc4	Istanbul
Cupu	cup	k1gInSc2	cup
proměnila	proměnit	k5eAaPmAgFnS	proměnit
v	v	k7c4	v
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
se	se	k3xPyFc4	se
probojovala	probojovat	k5eAaPmAgFnS	probojovat
do	do	k7c2	do
boje	boj	k1gInSc2	boj
o	o	k7c4	o
grandslamový	grandslamový	k2eAgInSc4d1	grandslamový
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
opět	opět	k6eAd1	opět
na	na	k7c6	na
US	US	kA	US
Open	Opena	k1gFnPc2	Opena
neuspěla	uspět	k5eNaPmAgNnP	uspět
<g/>
,	,	kIx,	,
když	když	k8xS	když
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
Serenu	Seren	k2eAgFnSc4d1	Serena
Williamsovou	Williamsová	k1gFnSc4	Williamsová
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
novou	nový	k2eAgFnSc7d1	nová
sezónou	sezóna	k1gFnSc7	sezóna
změnila	změnit	k5eAaPmAgFnS	změnit
značku	značka	k1gFnSc4	značka
rakety	raketa	k1gFnSc2	raketa
<g/>
,	,	kIx,	,
když	když	k8xS	když
opustila	opustit	k5eAaPmAgFnS	opustit
Yonex	Yonex	k1gInSc4	Yonex
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
hrát	hrát	k5eAaImF	hrát
s	s	k7c7	s
Babolatem	Babolat	k1gInSc7	Babolat
<g/>
.	.	kIx.	.
</s>
<s>
Úvodním	úvodní	k2eAgInSc7d1	úvodní
turnajem	turnaj	k1gInSc7	turnaj
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Apia	Apia	k1gMnSc1	Apia
International	International	k1gMnSc2	International
Sydney	Sydney	k1gNnSc2	Sydney
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
ji	on	k3xPp3gFnSc4	on
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
Lucie	Lucie	k1gFnSc1	Lucie
Šafářová	Šafářová	k1gFnSc1	Šafářová
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
fáze	fáze	k1gFnSc1	fáze
Australian	Australiana	k1gFnPc2	Australiana
Open	Open	k1gMnSc1	Open
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
stala	stát	k5eAaPmAgFnS	stát
konečnou	konečný	k2eAgFnSc7d1	konečná
<g/>
,	,	kIx,	,
když	když	k8xS	když
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
Garbiñ	Garbiñ	k1gFnSc4	Garbiñ
Muguruzaové	Muguruzaová	k1gFnSc2	Muguruzaová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Qatar	Qatar	k1gInSc4	Qatar
Total	totat	k5eAaImAgMnS	totat
Open	Open	k1gMnSc1	Open
prohrála	prohrát	k5eAaPmAgFnS	prohrát
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
duel	duel	k1gInSc4	duel
proti	proti	k7c3	proti
Yanině	Yanin	k2eAgFnSc3d1	Yanina
Wickmayerové	Wickmayerová	k1gFnSc3	Wickmayerová
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
prošla	projít	k5eAaPmAgFnS	projít
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
Dubai	Duba	k1gFnSc2	Duba
Tennis	Tennis	k1gFnSc2	Tennis
Championships	Championshipsa	k1gFnPc2	Championshipsa
a	a	k8xC	a
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
nezvládla	zvládnout	k5eNaPmAgFnS	zvládnout
střetnutí	střetnutí	k1gNnSc4	střetnutí
s	s	k7c7	s
Venus	Venus	k1gInSc1	Venus
Williamsovou	Williamsová	k1gFnSc4	Williamsová
<g/>
.	.	kIx.	.
</s>
<s>
Březnový	březnový	k2eAgInSc1d1	březnový
BNP	BNP	kA	BNP
Paribas	Paribas	k1gInSc1	Paribas
Open	Open	k1gInSc1	Open
znamenal	znamenat	k5eAaImAgInS	znamenat
osmifinálovou	osmifinálový	k2eAgFnSc4d1	osmifinálová
prohru	prohra	k1gFnSc4	prohra
s	s	k7c7	s
Jelenou	Jelena	k1gFnSc7	Jelena
Jankovićovou	Jankovićová	k1gFnSc7	Jankovićová
<g/>
.	.	kIx.	.
</s>
<s>
Ztráta	ztráta	k1gFnSc1	ztráta
bodů	bod	k1gInPc2	bod
způsobila	způsobit	k5eAaPmAgFnS	způsobit
pád	pád	k1gInSc4	pád
na	na	k7c4	na
18	[number]	k4	18
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
<g/>
,	,	kIx,	,
nejnižší	nízký	k2eAgNnSc4d3	nejnižší
postavení	postavení	k1gNnSc4	postavení
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1	čtvrtfinále
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
na	na	k7c6	na
Sony	Sony	kA	Sony
Open	Open	k1gNnSc1	Open
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
Li	li	k9	li
Na	na	k7c6	na
<g/>
.	.	kIx.	.
</s>
<s>
Semifinále	semifinále	k1gNnSc4	semifinále
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
debutu	debut	k1gInSc6	debut
na	na	k7c4	na
Monterry	Monterra	k1gFnPc4	Monterra
Open	Opena	k1gFnPc2	Opena
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
turnaj	turnaj	k1gInSc4	turnaj
opustila	opustit	k5eAaPmAgFnS	opustit
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
od	od	k7c2	od
Any	Any	k1gFnSc2	Any
Ivanovićové	Ivanovićová	k1gFnSc2	Ivanovićová
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
antukového	antukový	k2eAgNnSc2d1	antukové
Porsche	Porsche	k1gNnSc2	Porsche
Tennis	Tennis	k1gFnSc2	Tennis
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
odstoupila	odstoupit	k5eAaPmAgFnS	odstoupit
pro	pro	k7c4	pro
zranění	zranění	k1gNnSc4	zranění
zápěstí	zápěstí	k1gNnSc2	zápěstí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
Mutua	Mutua	k1gFnSc1	Mutua
Madrid	Madrid	k1gInSc1	Madrid
Open	Open	k1gInSc4	Open
nenašla	najít	k5eNaPmAgFnS	najít
recept	recept	k1gInSc4	recept
na	na	k7c4	na
Robertu	Roberta	k1gFnSc4	Roberta
Vinciovou	Vinciový	k2eAgFnSc4d1	Vinciová
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zdravotních	zdravotní	k2eAgInPc6d1	zdravotní
problémech	problém	k1gInPc6	problém
nezvládla	zvládnout	k5eNaPmAgFnS	zvládnout
úvodní	úvodní	k2eAgInSc4d1	úvodní
duel	duel	k1gInSc4	duel
na	na	k7c4	na
French	French	k1gInSc4	French
Open	Opena	k1gFnPc2	Opena
proti	proti	k7c3	proti
Wickmayerové	Wickmayerová	k1gFnSc3	Wickmayerová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
travnatého	travnatý	k2eAgNnSc2d1	travnaté
AEGON	AEGON	kA	AEGON
International	International	k1gMnSc3	International
ji	on	k3xPp3gFnSc4	on
vystavila	vystavit	k5eAaPmAgFnS	vystavit
stopku	stopka	k1gFnSc4	stopka
Angelique	Angelique	k1gFnSc1	Angelique
Kerberová	Kerberová	k1gFnSc1	Kerberová
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
pak	pak	k6eAd1	pak
došla	dojít	k5eAaPmAgFnS	dojít
mezi	mezi	k7c4	mezi
poslední	poslední	k2eAgFnSc4d1	poslední
šestnáctku	šestnáctka	k1gFnSc4	šestnáctka
hráček	hráčka	k1gFnPc2	hráčka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
přehrála	přehrát	k5eAaPmAgFnS	přehrát
Barbora	Barbora	k1gFnSc1	Barbora
Záhlavová-Strýcová	Záhlavová-Strýcová	k1gFnSc1	Záhlavová-Strýcová
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgInSc4d1	jediný
titul	titul	k1gInSc4	titul
sezóny	sezóna	k1gFnSc2	sezóna
dobyla	dobýt	k5eAaPmAgFnS	dobýt
na	na	k7c4	na
Istanbul	Istanbul	k1gInSc4	Istanbul
Cupu	cup	k1gInSc2	cup
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
uštědřila	uštědřit	k5eAaPmAgFnS	uštědřit
dva	dva	k4xCgInPc4	dva
"	"	kIx"	"
<g/>
kanáry	kanár	k1gInPc4	kanár
<g/>
"	"	kIx"	"
Belindě	Belinda	k1gFnSc3	Belinda
Bencicové	Bencicová	k1gFnSc2	Bencicová
a	a	k8xC	a
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
deklasovala	deklasovat	k5eAaBmAgFnS	deklasovat
Robertu	Roberta	k1gFnSc4	Roberta
Vinciova	Vinciův	k2eAgFnSc1d1	Vinciův
po	po	k7c6	po
ztrátě	ztráta	k1gFnSc6	ztráta
pouhých	pouhý	k2eAgFnPc2d1	pouhá
dvou	dva	k4xCgFnPc2	dva
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
amerických	americký	k2eAgInPc6d1	americký
betonech	beton	k1gInPc6	beton
ji	on	k3xPp3gFnSc4	on
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
Rogers	Rogers	k1gInSc4	Rogers
Cupu	cup	k1gInSc2	cup
zastavila	zastavit	k5eAaPmAgFnS	zastavit
Serena	Seren	k2eAgFnSc1d1	Serena
Williamsová	Williamsová	k1gFnSc1	Williamsová
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
měla	mít	k5eAaImAgFnS	mít
set	set	k1gInSc4	set
a	a	k8xC	a
break	break	k1gInSc4	break
k	k	k7c3	k
dobru	dobro	k1gNnSc3	dobro
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Western	Western	kA	Western
&	&	k?	&
Southern	Southern	k1gInSc1	Southern
Open	Opena	k1gFnPc2	Opena
se	se	k3xPyFc4	se
probojovala	probojovat	k5eAaPmAgFnS	probojovat
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
opět	opět	k6eAd1	opět
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
s	s	k7c7	s
mladší	mladý	k2eAgMnSc1d2	mladší
ze	z	k7c2	z
sester	sestra	k1gFnPc2	sestra
Williamsových	Williamsová	k1gFnPc2	Williamsová
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
nasazená	nasazený	k2eAgFnSc1d1	nasazená
desítka	desítka	k1gFnSc1	desítka
zavítala	zavítat	k5eAaPmAgFnS	zavítat
na	na	k7c4	na
US	US	kA	US
Open	Open	k1gInSc4	Open
a	a	k8xC	a
po	po	k7c6	po
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
podruhé	podruhé	k6eAd1	podruhé
prošla	projít	k5eAaPmAgFnS	projít
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
grandslamu	grandslam	k1gInSc2	grandslam
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
ve	v	k7c4	v
Flushing	Flushing	k1gInSc4	Flushing
Meadows	Meadowsa	k1gFnPc2	Meadowsa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
kole	kolo	k1gNnSc6	kolo
porazila	porazit	k5eAaPmAgFnS	porazit
Šarapovovou	Šarapovová	k1gFnSc4	Šarapovová
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
Saru	Sar	k2eAgFnSc4d1	Sara
Erraniovou	Erraniový	k2eAgFnSc4d1	Erraniová
a	a	k8xC	a
v	v	k7c4	v
semifinále	semifinále	k1gNnSc4	semifinále
překvapení	překvapení	k1gNnSc1	překvapení
turnaje	turnaj	k1gInSc2	turnaj
Pcheng	Pcheng	k1gMnSc1	Pcheng
Šuaj	Šuaj	k1gMnSc1	Šuaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
však	však	k9	však
nenašla	najít	k5eNaPmAgFnS	najít
recept	recept	k1gInSc4	recept
na	na	k7c4	na
Serenu	Seren	k2eAgFnSc4d1	Serena
Williamsovou	Williamsová	k1gFnSc4	Williamsová
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
na	na	k7c4	na
třetí	třetí	k4xOgFnPc4	třetí
události	událost	k1gFnPc4	událost
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Asijskou	asijský	k2eAgFnSc4d1	asijská
túru	túra	k1gFnSc4	túra
rozehrála	rozehrát	k5eAaPmAgFnS	rozehrát
tokijským	tokijský	k2eAgFnPc3d1	Tokijská
Toray	Toraa	k1gFnSc2	Toraa
Pan	Pan	k1gMnSc1	Pan
Pacific	Pacific	k1gMnSc1	Pacific
Open	Open	k1gMnSc1	Open
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zužitkovala	zužitkovat	k5eAaPmAgFnS	zužitkovat
dobrou	dobrý	k2eAgFnSc4d1	dobrá
formu	forma	k1gFnSc4	forma
postupem	postup	k1gInSc7	postup
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
ji	on	k3xPp3gFnSc4	on
zastavila	zastavit	k5eAaPmAgFnS	zastavit
Ana	Ana	k1gFnSc1	Ana
Ivanovićová	Ivanovićová	k1gFnSc1	Ivanovićová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
čínském	čínský	k2eAgInSc6d1	čínský
Wuhan	Wuhan	k1gInSc1	Wuhan
Open	Open	k1gNnSc4	Open
pak	pak	k6eAd1	pak
dohrála	dohrát	k5eAaPmAgFnS	dohrát
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
porážkou	porážka	k1gFnSc7	porážka
od	od	k7c2	od
Genie	genius	k1gMnPc4	genius
Bouchardové	Bouchardový	k2eAgNnSc1d1	Bouchardový
<g/>
.	.	kIx.	.
</s>
<s>
Říjnový	říjnový	k2eAgInSc1d1	říjnový
China	China	k1gFnSc1	China
Open	Open	k1gInSc1	Open
opustila	opustit	k5eAaPmAgFnS	opustit
po	po	k7c6	po
volném	volný	k2eAgInSc6d1	volný
losu	los	k1gInSc6	los
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
fázi	fáze	k1gFnSc6	fáze
<g/>
,	,	kIx,	,
když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
Sam	Sam	k1gMnSc1	Sam
Stosurová	stosurový	k2eAgNnPc1d1	stosurový
<g/>
.	.	kIx.	.
</s>
