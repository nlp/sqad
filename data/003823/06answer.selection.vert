<s>
Caroline	Carolin	k1gInSc5	Carolin
Wozniacká	Wozniacký	k2eAgFnSc1d1	Wozniacká
<g/>
,	,	kIx,	,
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Caroline	Carolin	k1gInSc5	Carolin
Wozniacki	Wozniacki	k1gNnPc7	Wozniacki
(	(	kIx(	(
<g/>
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1990	[number]	k4	1990
Odense	Odense	k1gFnSc1	Odense
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dánská	dánský	k2eAgFnSc1d1	dánská
profesionální	profesionální	k2eAgFnSc1d1	profesionální
tenistka	tenistka	k1gFnSc1	tenistka
polského	polský	k2eAgInSc2d1	polský
původu	původ	k1gInSc2	původ
a	a	k8xC	a
bývalá	bývalý	k2eAgFnSc1d1	bývalá
světová	světový	k2eAgFnSc1d1	světová
jednička	jednička	k1gFnSc1	jednička
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2010	[number]	k4	2010
jako	jako	k8xC	jako
dvacátá	dvacátý	k4xOgFnSc1	dvacátý
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
a	a	k8xC	a
první	první	k4xOgMnSc1	první
ze	z	k7c2	z
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
.	.	kIx.	.
</s>
