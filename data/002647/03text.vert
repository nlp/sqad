<s>
Vladimir	Vladimir	k1gInSc1	Vladimir
Iljič	Iljič	k1gInSc1	Iljič
Uljanov	Uljanov	k1gInSc4	Uljanov
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
В	В	k?	В
<g/>
́	́	k?	́
<g/>
м	м	k?	м
И	И	k?	И
<g/>
́	́	k?	́
<g/>
ч	ч	k?	ч
У	У	k?	У
<g/>
́	́	k?	́
<g/>
н	н	k?	н
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
přijal	přijmout	k5eAaPmAgMnS	přijmout
přezdívku	přezdívka	k1gFnSc4	přezdívka
Lenin	Lenin	k2eAgMnSc1d1	Lenin
(	(	kIx(	(
<g/>
Л	Л	k?	Л
<g/>
́	́	k?	́
<g/>
н	н	k?	н
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
/	/	kIx~	/
22	[number]	k4	22
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
dubna	duben	k1gInSc2	duben
1870	[number]	k4	1870
v	v	k7c6	v
Simbirsku	Simbirsek	k1gInSc6	Simbirsek
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Uljanovsk	Uljanovsk	k1gInSc1	Uljanovsk
<g/>
)	)	kIx)	)
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1924	[number]	k4	1924
Gorki	Gorki	k1gFnPc4	Gorki
u	u	k7c2	u
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
komunistický	komunistický	k2eAgMnSc1d1	komunistický
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
revolucionář	revolucionář	k1gMnSc1	revolucionář
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnSc1	vůdce
Bolševické	bolševický	k2eAgFnSc2d1	bolševická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
Sovětského	sovětský	k2eAgNnSc2d1	sovětské
Ruska	Rusko	k1gNnSc2	Rusko
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
-	-	kIx~	-
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
-	-	kIx~	-
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
teoretikem	teoretik	k1gMnSc7	teoretik
leninismu	leninismus	k1gInSc2	leninismus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
definoval	definovat	k5eAaBmAgInS	definovat
jako	jako	k9	jako
adaptaci	adaptace	k1gFnSc4	adaptace
marxismu	marxismus	k1gInSc2	marxismus
v	v	k7c6	v
období	období	k1gNnSc6	období
imperialismu	imperialismus	k1gInSc2	imperialismus
<g/>
.	.	kIx.	.
</s>
<s>
Stál	stát	k5eAaImAgMnS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Velké	velký	k2eAgFnSc2d1	velká
říjnové	říjnový	k2eAgFnSc2d1	říjnová
socialistické	socialistický	k2eAgFnSc2d1	socialistická
revoluce	revoluce	k1gFnSc2	revoluce
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
de	de	k?	de
facto	facto	k1gNnSc4	facto
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
představitelem	představitel	k1gMnSc7	představitel
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
za	za	k7c4	za
jehož	jenž	k3xRgMnSc4	jenž
zakladatele	zakladatel	k1gMnSc4	zakladatel
je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
označován	označovat	k5eAaImNgInS	označovat
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc4	některý
své	svůj	k3xOyFgInPc4	svůj
spisy	spis	k1gInPc4	spis
publikoval	publikovat	k5eAaBmAgMnS	publikovat
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Lenin	Lenin	k1gMnSc1	Lenin
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
teorií	teorie	k1gFnPc2	teorie
o	o	k7c6	o
původu	původ	k1gInSc6	původ
jeho	on	k3xPp3gNnSc2	on
jména	jméno	k1gNnSc2	jméno
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
nikdy	nikdy	k6eAd1	nikdy
neprozradil	prozradit	k5eNaPmAgMnS	prozradit
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
si	se	k3xPyFc3	se
je	on	k3xPp3gNnSc4	on
vybral	vybrat	k5eAaPmAgMnS	vybrat
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
vyloučeno	vyloučit	k5eAaPmNgNnS	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
Lenin	Lenin	k1gMnSc1	Lenin
<g/>
"	"	kIx"	"
zvolil	zvolit	k5eAaPmAgMnS	zvolit
na	na	k7c4	na
důkaz	důkaz	k1gInSc4	důkaz
opozice	opozice	k1gFnSc2	opozice
vůči	vůči	k7c3	vůči
Georgiji	Georgiji	k1gMnSc3	Georgiji
Plechanovovi	Plechanova	k1gMnSc3	Plechanova
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
který	který	k3yQgInSc4	který
používal	používat	k5eAaImAgInS	používat
pseudonym	pseudonym	k1gInSc1	pseudonym
Volgin	Volgin	k2eAgInSc1d1	Volgin
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
řeky	řeka	k1gFnSc2	řeka
Volhy	Volha	k1gFnSc2	Volha
<g/>
.	.	kIx.	.
</s>
<s>
Uljanov	Uljanov	k1gInSc1	Uljanov
si	se	k3xPyFc3	se
zvolil	zvolit	k5eAaPmAgInS	zvolit
Lenu	Lena	k1gFnSc4	Lena
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
delší	dlouhý	k2eAgFnSc1d2	delší
a	a	k8xC	a
teče	téct	k5eAaImIp3nS	téct
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgInS	mít
Plechanov	Plechanov	k1gInSc1	Plechanov
v	v	k7c6	v
dotyčném	dotyčný	k2eAgNnSc6d1	dotyčné
období	období	k1gNnSc6	období
života	život	k1gInSc2	život
na	na	k7c4	na
Lenina	Lenin	k1gMnSc4	Lenin
velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
<g/>
,	,	kIx,	,
věrohodnost	věrohodnost	k1gFnSc1	věrohodnost
vysvětlení	vysvětlení	k1gNnSc2	vysvětlení
je	být	k5eAaImIp3nS	být
diskutabilní	diskutabilní	k2eAgMnSc1d1	diskutabilní
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Vladimir	Vladimir	k1gMnSc1	Vladimir
Iljič	Iljič	k1gMnSc1	Iljič
Lenin	Lenin	k1gMnSc1	Lenin
patrně	patrně	k6eAd1	patrně
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
spojením	spojení	k1gNnSc7	spojení
jmen	jméno	k1gNnPc2	jméno
V.	V.	kA	V.
I.	I.	kA	I.
Uljanov	Uljanov	k1gInSc4	Uljanov
a	a	k8xC	a
N.	N.	kA	N.
N.	N.	kA	N.
Lenin	Lenin	k1gMnSc1	Lenin
<g/>
.	.	kIx.	.
</s>
<s>
Lenin	Lenin	k1gMnSc1	Lenin
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
ruském	ruský	k2eAgInSc6d1	ruský
Simbirsku	Simbirsek	k1gInSc6	Simbirsek
jako	jako	k8xC	jako
syn	syn	k1gMnSc1	syn
Ilji	Ilja	k1gMnSc2	Ilja
Nikolajeviče	Nikolajevič	k1gMnSc2	Nikolajevič
Uljanova	Uljanův	k2eAgMnSc2d1	Uljanův
(	(	kIx(	(
<g/>
1831	[number]	k4	1831
<g/>
-	-	kIx~	-
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zámožného	zámožný	k2eAgMnSc2d1	zámožný
gymnaziálního	gymnaziální	k2eAgMnSc2d1	gymnaziální
ředitele	ředitel	k1gMnSc2	ředitel
čuvašského	čuvašský	k2eAgInSc2d1	čuvašský
původu	původ	k1gInSc2	původ
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
státního	státní	k2eAgMnSc2d1	státní
rady	rada	k1gMnSc2	rada
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgInS	být
i	i	k9	i
povýšen	povýšen	k2eAgMnSc1d1	povýšen
na	na	k7c4	na
šlechtice	šlechtic	k1gMnSc4	šlechtic
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
demokratizaci	demokratizace	k1gFnSc4	demokratizace
a	a	k8xC	a
vzdělání	vzdělání	k1gNnSc4	vzdělání
pro	pro	k7c4	pro
veškeré	veškerý	k3xTgNnSc4	veškerý
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Ruska	Ruska	k1gFnSc1	Ruska
a	a	k8xC	a
vykonal	vykonat	k5eAaPmAgMnS	vykonat
mnohé	mnohý	k2eAgInPc4d1	mnohý
pro	pro	k7c4	pro
vzdělání	vzdělání	k1gNnSc4	vzdělání
neruských	ruský	k2eNgNnPc2d1	neruské
etnik	etnikum	k1gNnPc2	etnikum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Povolží	Povolží	k1gNnSc2	Povolží
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
liberální	liberální	k2eAgFnPc1d1	liberální
ženy	žena	k1gFnPc1	žena
Marie	Maria	k1gFnSc2	Maria
Alexandrovny	Alexandrovna	k1gFnSc2	Alexandrovna
Blankové	Blanková	k1gFnSc2	Blanková
(	(	kIx(	(
<g/>
1835	[number]	k4	1835
<g/>
-	-	kIx~	-
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
dědeček	dědeček	k1gMnSc1	dědeček
Moše	mocha	k1gFnSc6	mocha
Ickovič	Ickovič	k1gMnSc1	Ickovič
Blank	Blank	k1gMnSc1	Blank
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
М	М	k?	М
И	И	k?	И
Б	Б	k?	Б
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
jeho	jeho	k3xOp3gFnSc2	jeho
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
žid	žid	k1gMnSc1	žid
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
konvertoval	konvertovat	k5eAaBmAgMnS	konvertovat
k	k	k7c3	k
pravoslaví	pravoslaví	k1gNnSc3	pravoslaví
(	(	kIx(	(
<g/>
čímž	což	k3yQnSc7	což
přestal	přestat	k5eAaPmAgInS	přestat
být	být	k5eAaImF	být
logicky	logicky	k6eAd1	logicky
židem	žid	k1gMnSc7	žid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prababička	prababička	k1gFnSc1	prababička
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
strany	strana	k1gFnSc2	strana
byla	být	k5eAaImAgFnS	být
kalmycko-mongolského	kalmyckoongolský	k2eAgInSc2d1	kalmycko-mongolský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
babička	babička	k1gFnSc1	babička
jeho	jeho	k3xOp3gFnSc2	jeho
matky	matka	k1gFnSc2	matka
byla	být	k5eAaImAgFnS	být
Povolžská	povolžský	k2eAgFnSc1d1	Povolžská
Němka	Němka	k1gFnSc1	Němka
luteránského	luteránský	k2eAgNnSc2d1	luteránské
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Lenin	Lenin	k1gMnSc1	Lenin
byl	být	k5eAaImAgMnS	být
pokřtěn	pokřtít	k5eAaPmNgMnS	pokřtít
jako	jako	k8xC	jako
pravoslavný	pravoslavný	k2eAgMnSc1d1	pravoslavný
křesťan	křesťan	k1gMnSc1	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Lenin	Lenin	k1gMnSc1	Lenin
se	se	k3xPyFc4	se
pilně	pilně	k6eAd1	pilně
vzdělával	vzdělávat	k5eAaImAgMnS	vzdělávat
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
a	a	k8xC	a
řečtině	řečtina	k1gFnSc6	řečtina
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jeho	on	k3xPp3gNnSc2	on
dospívání	dospívání	k1gNnSc2	dospívání
zasáhly	zasáhnout	k5eAaPmAgFnP	zasáhnout
dvě	dva	k4xCgFnPc4	dva
tragédie	tragédie	k1gFnPc4	tragédie
<g/>
:	:	kIx,	:
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
mu	on	k3xPp3gMnSc3	on
zemřel	zemřít	k5eAaPmAgMnS	zemřít
otec	otec	k1gMnSc1	otec
na	na	k7c4	na
mozkovou	mozkový	k2eAgFnSc4d1	mozková
mrtvici	mrtvice	k1gFnSc4	mrtvice
<g/>
,	,	kIx,	,
a	a	k8xC	a
následující	následující	k2eAgInSc1d1	následující
rok	rok	k1gInSc1	rok
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1887	[number]	k4	1887
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
popraven	popravit	k5eAaPmNgMnS	popravit
jeho	jeho	k3xOp3gFnSc7	jeho
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
Alexandr	Alexandr	k1gMnSc1	Alexandr
Uljanov	Uljanov	k1gInSc4	Uljanov
za	za	k7c4	za
účast	účast	k1gFnSc4	účast
při	při	k7c6	při
atentátu	atentát	k1gInSc6	atentát
na	na	k7c4	na
cara	car	k1gMnSc4	car
Alexandra	Alexandr	k1gMnSc2	Alexandr
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
Lenina	Lenin	k1gMnSc2	Lenin
radikalizovala	radikalizovat	k5eAaBmAgFnS	radikalizovat
a	a	k8xC	a
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
čtyřech	čtyři	k4xCgInPc6	čtyři
měsících	měsíc	k1gInPc6	měsíc
studia	studio	k1gNnSc2	studio
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
z	z	k7c2	z
právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Kazani	Kazaň	k1gFnSc6	Kazaň
pro	pro	k7c4	pro
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
studentských	studentský	k2eAgInPc6d1	studentský
protestech	protest	k1gInPc6	protest
a	a	k8xC	a
vypovězen	vypovědět	k5eAaPmNgMnS	vypovědět
pod	pod	k7c4	pod
neveřejný	veřejný	k2eNgInSc4d1	neveřejný
policejní	policejní	k2eAgInSc4d1	policejní
dozor	dozor	k1gInSc4	dozor
do	do	k7c2	do
vesnice	vesnice	k1gFnSc2	vesnice
Kokuškino	Kokuškino	k1gNnSc1	Kokuškino
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Hegelovými	Hegelův	k2eAgInPc7d1	Hegelův
díly	díl	k1gInPc7	díl
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
získal	získat	k5eAaPmAgMnS	získat
ponětí	ponětí	k1gNnSc3	ponětí
o	o	k7c6	o
dialektické	dialektický	k2eAgFnSc6d1	dialektická
metodě	metoda	k1gFnSc6	metoda
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
lépe	dobře	k6eAd2	dobře
pochopit	pochopit	k5eAaPmF	pochopit
teorie	teorie	k1gFnPc4	teorie
Karla	Karel	k1gMnSc2	Karel
Marxe	Marx	k1gMnSc2	Marx
<g/>
.	.	kIx.	.
</s>
<s>
Práva	právo	k1gNnPc4	právo
dostudoval	dostudovat	k5eAaPmAgMnS	dostudovat
dálkově	dálkově	k6eAd1	dálkově
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
získal	získat	k5eAaPmAgInS	získat
povolení	povolení	k1gNnSc4	povolení
vykonávat	vykonávat	k5eAaImF	vykonávat
právnickou	právnický	k2eAgFnSc4d1	právnická
profesi	profese	k1gFnSc4	profese
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
příjezdu	příjezd	k1gInSc6	příjezd
do	do	k7c2	do
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
obvyklého	obvyklý	k2eAgNnSc2d1	obvyklé
povolání	povolání	k1gNnSc2	povolání
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
revolučního	revoluční	k2eAgNnSc2d1	revoluční
propagandistického	propagandistický	k2eAgNnSc2d1	propagandistické
hnutí	hnutí	k1gNnSc2	hnutí
a	a	k8xC	a
studia	studio	k1gNnSc2	studio
marxismu	marxismus	k1gInSc2	marxismus
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
vůdcem	vůdce	k1gMnSc7	vůdce
místních	místní	k2eAgMnPc2d1	místní
marxistů	marxista	k1gMnPc2	marxista
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
založil	založit	k5eAaPmAgInS	založit
levicový	levicový	k2eAgInSc1d1	levicový
"	"	kIx"	"
<g/>
Svaz	svaz	k1gInSc1	svaz
boje	boj	k1gInSc2	boj
za	za	k7c4	za
osvobození	osvobození	k1gNnSc4	osvobození
dělnické	dělnický	k2eAgFnSc2d1	Dělnická
třídy	třída	k1gFnSc2	třída
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
zárodek	zárodek	k1gInSc1	zárodek
radikální	radikální	k2eAgFnSc2d1	radikální
levicové	levicový	k2eAgFnSc2d1	levicová
marxistické	marxistický	k2eAgFnSc2d1	marxistická
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1895	[number]	k4	1895
byl	být	k5eAaImAgMnS	být
Lenin	Lenin	k1gMnSc1	Lenin
uvězněn	uvězněn	k2eAgMnSc1d1	uvězněn
a	a	k8xC	a
po	po	k7c6	po
14	[number]	k4	14
měsících	měsíc	k1gInPc6	měsíc
poslán	poslat	k5eAaPmNgMnS	poslat
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
do	do	k7c2	do
sibiřské	sibiřský	k2eAgFnSc2d1	sibiřská
vesnice	vesnice	k1gFnSc2	vesnice
Šušenskoje	Šušenskoj	k1gInSc2	Šušenskoj
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
přijela	přijet	k5eAaPmAgFnS	přijet
do	do	k7c2	do
Šušenského	Šušenský	k2eAgInSc2d1	Šušenský
i	i	k8xC	i
levicová	levicový	k2eAgFnSc1d1	levicová
aktivistka	aktivistka	k1gFnSc1	aktivistka
Naděžda	Naděžda	k1gFnSc1	Naděžda
Krupská	Krupský	k2eAgFnSc1d1	Krupská
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1898	[number]	k4	1898
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
a	a	k8xC	a
setrvala	setrvat	k5eAaPmAgFnS	setrvat
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1899	[number]	k4	1899
vydal	vydat	k5eAaPmAgInS	vydat
Vývoj	vývoj	k1gInSc1	vývoj
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
odešel	odejít	k5eAaPmAgMnS	odejít
Lenin	Lenin	k1gMnSc1	Lenin
do	do	k7c2	do
emigrace	emigrace	k1gFnSc2	emigrace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
vydával	vydávat	k5eAaPmAgInS	vydávat
levicový	levicový	k2eAgInSc1d1	levicový
časopis	časopis	k1gInSc1	časopis
Iskra	Iskro	k1gNnSc2	Iskro
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Jiskra	jiskra	k1gFnSc1	jiskra
<g/>
)	)	kIx)	)
a	a	k8xC	a
řadu	řada	k1gFnSc4	řada
spisů	spis	k1gInPc2	spis
a	a	k8xC	a
knih	kniha	k1gFnPc2	kniha
vztahujících	vztahující	k2eAgFnPc2d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
revolučnímu	revoluční	k2eAgNnSc3d1	revoluční
hnutí	hnutí	k1gNnSc3	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
vedoucí	vedoucí	k2eAgFnSc7d1	vedoucí
postavou	postava	k1gFnSc7	postava
extrémně	extrémně	k6eAd1	extrémně
levicového	levicový	k2eAgNnSc2d1	levicové
hnutí	hnutí	k1gNnSc2	hnutí
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
aktivním	aktivní	k2eAgInSc7d1	aktivní
členem	člen	k1gInSc7	člen
Sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgFnSc2d1	demokratická
dělnické	dělnický	k2eAgFnSc2d1	Dělnická
strany	strana	k1gFnSc2	strana
Ruska	Rusko	k1gNnSc2	Rusko
(	(	kIx(	(
<g/>
SDRSR	SDRSR	kA	SDRSR
<g/>
)	)	kIx)	)
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
se	se	k3xPyFc4	se
po	po	k7c6	po
rozchodu	rozchod	k1gInSc6	rozchod
s	s	k7c7	s
menševiky	menševik	k1gMnPc7	menševik
stává	stávat	k5eAaImIp3nS	stávat
vůdcem	vůdce	k1gMnSc7	vůdce
bolševické	bolševický	k2eAgFnSc2d1	bolševická
frakce	frakce	k1gFnSc2	frakce
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
je	být	k5eAaImIp3nS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
SDRSR	SDRSR	kA	SDRSR
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lenin	Lenin	k1gMnSc1	Lenin
vedl	vést	k5eAaImAgMnS	vést
radikální	radikální	k2eAgFnSc3d1	radikální
frakci	frakce	k1gFnSc3	frakce
SDRSR	SDRSR	kA	SDRSR
(	(	kIx(	(
<g/>
kterou	který	k3yQgFnSc4	který
nazval	nazvat	k5eAaBmAgMnS	nazvat
bolševickou	bolševický	k2eAgFnSc7d1	bolševická
-	-	kIx~	-
většinovou	většinový	k2eAgFnSc7d1	většinová
<g/>
)	)	kIx)	)
a	a	k8xC	a
i	i	k9	i
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
rámci	rámec	k1gInSc6	rámec
představoval	představovat	k5eAaImAgMnS	představovat
radikála	radikál	k1gMnSc4	radikál
prosazujícího	prosazující	k2eAgInSc2d1	prosazující
extrémní	extrémní	k2eAgInPc4d1	extrémní
postoje	postoj	k1gInPc4	postoj
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
kázeň	kázeň	k1gFnSc4	kázeň
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
ač	ač	k8xS	ač
dosud	dosud	k6eAd1	dosud
v	v	k7c6	v
menšině	menšina	k1gFnSc6	menšina
<g/>
)	)	kIx)	)
opakovaně	opakovaně	k6eAd1	opakovaně
ignoroval	ignorovat	k5eAaImAgMnS	ignorovat
ta	ten	k3xDgNnPc4	ten
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgMnPc7	který
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
svým	svůj	k3xOyFgMnPc3	svůj
odpůrcům	odpůrce	k1gMnPc3	odpůrce
bojoval	bojovat	k5eAaImAgInS	bojovat
tvrdě	tvrdě	k6eAd1	tvrdě
a	a	k8xC	a
nemilosrdně	milosrdně	k6eNd1	milosrdně
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
bez	bez	k7c2	bez
usmiřování	usmiřování	k1gNnSc2	usmiřování
<g/>
.	.	kIx.	.
</s>
<s>
Viktor	Viktor	k1gMnSc1	Viktor
Černov	Černov	k1gInSc1	Černov
(	(	kIx(	(
<g/>
vůdce	vůdce	k1gMnSc1	vůdce
eserů	eser	k1gMnPc2	eser
<g/>
)	)	kIx)	)
jej	on	k3xPp3gNnSc4	on
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c2	za
člověka	člověk	k1gMnSc2	člověk
"	"	kIx"	"
<g/>
s	s	k7c7	s
pravdou	pravda	k1gFnSc7	pravda
v	v	k7c6	v
kapse	kapsa	k1gFnSc6	kapsa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
s	s	k7c7	s
pravdou	pravda	k1gFnSc7	pravda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
za	za	k7c2	za
všech	všecek	k3xTgFnPc2	všecek
okolností	okolnost	k1gFnPc2	okolnost
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
se	se	k3xPyFc4	se
z	z	k7c2	z
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
důvodů	důvod	k1gInPc2	důvod
přesunuje	přesunovat	k5eAaImIp3nS	přesunovat
do	do	k7c2	do
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
cestuje	cestovat	k5eAaImIp3nS	cestovat
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
účastní	účastnit	k5eAaImIp3nS	účastnit
se	se	k3xPyFc4	se
mnoha	mnoho	k4c2	mnoho
levicových	levicový	k2eAgNnPc2d1	levicové
setkání	setkání	k1gNnPc2	setkání
a	a	k8xC	a
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Zimmerwaldské	Zimmerwaldský	k2eAgFnSc2d1	Zimmerwaldský
konference	konference	k1gFnSc2	konference
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
sledován	sledovat	k5eAaImNgInS	sledovat
carskou	carský	k2eAgFnSc7d1	carská
tajnou	tajný	k2eAgFnSc7d1	tajná
policií	policie	k1gFnSc7	policie
i	i	k8xC	i
soukromým	soukromý	k2eAgMnSc7d1	soukromý
detektivem	detektiv	k1gMnSc7	detektiv
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
si	se	k3xPyFc3	se
najali	najmout	k5eAaPmAgMnP	najmout
jeho	jeho	k3xOp3gMnPc1	jeho
političtí	politický	k2eAgMnPc1d1	politický
odpůrci	odpůrce	k1gMnPc1	odpůrce
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Inessa	Inessa	k1gFnSc1	Inessa
Armandová	Armandový	k2eAgFnSc1d1	Armandová
emigrovala	emigrovat	k5eAaBmAgFnS	emigrovat
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
usadila	usadit	k5eAaPmAgFnS	usadit
se	se	k3xPyFc4	se
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
setkala	setkat	k5eAaPmAgFnS	setkat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
s	s	k7c7	s
Leninem	Lenin	k1gMnSc7	Lenin
a	a	k8xC	a
jinými	jiný	k2eAgMnPc7d1	jiný
bolševiky	bolševik	k1gMnPc7	bolševik
žijícími	žijící	k2eAgMnPc7d1	žijící
zde	zde	k6eAd1	zde
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
<g/>
.	.	kIx.	.
</s>
<s>
Inessa	Inessa	k1gFnSc1	Inessa
Armandová	Armandový	k2eAgFnSc1d1	Armandová
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stala	stát	k5eAaPmAgFnS	stát
Leninovou	Leninův	k2eAgFnSc7d1	Leninova
milenkou	milenka	k1gFnSc7	milenka
<g/>
.	.	kIx.	.
</s>
<s>
Lenin	Lenin	k1gMnSc1	Lenin
se	se	k3xPyFc4	se
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
snažil	snažit	k5eAaImAgMnS	snažit
získat	získat	k5eAaPmF	získat
finanční	finanční	k2eAgFnSc4d1	finanční
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
revoluční	revoluční	k2eAgFnSc4d1	revoluční
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
dařilo	dařit	k5eAaImAgNnS	dařit
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gMnSc3	jeho
spolubojovníkovi	spolubojovník	k1gMnSc3	spolubojovník
-	-	kIx~	-
revolucionářovi	revolucionář	k1gMnSc3	revolucionář
Alexandru	Alexandr	k1gMnSc3	Alexandr
Izrailovi	Izrail	k1gMnSc3	Izrail
Lazarevičovi	Lazarevič	k1gMnSc3	Lazarevič
(	(	kIx(	(
<g/>
zvanému	zvaný	k2eAgInSc3d1	zvaný
též	též	k9	též
"	"	kIx"	"
<g/>
Helphand	Helphand	k1gInSc1	Helphand
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
Pavrus	Pavrus	k1gInSc1	Pavrus
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
dokonce	dokonce	k9	dokonce
pro	pro	k7c4	pro
revoluční	revoluční	k2eAgFnSc4d1	revoluční
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
postupně	postupně	k6eAd1	postupně
získával	získávat	k5eAaImAgInS	získávat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
finanční	finanční	k2eAgFnSc2d1	finanční
částky	částka	k1gFnSc2	částka
i	i	k9	i
od	od	k7c2	od
Berlínského	berlínský	k2eAgNnSc2d1	berlínské
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
financí	finance	k1gFnPc2	finance
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
totiž	totiž	k9	totiž
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
padne	padnout	k5eAaPmIp3nS	padnout
carský	carský	k2eAgInSc4d1	carský
režim	režim	k1gInSc4	režim
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
nebudou	být	k5eNaImBp3nP	být
muset	muset	k5eAaImF	muset
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
nadále	nadále	k6eAd1	nadále
válčit	válčit	k5eAaImF	válčit
<g/>
,	,	kIx,	,
a	a	k8xC	a
uvolněné	uvolněný	k2eAgMnPc4d1	uvolněný
vojáky	voják	k1gMnPc4	voják
budou	být	k5eAaImBp3nP	být
moci	moct	k5eAaImF	moct
přesunout	přesunout	k5eAaPmF	přesunout
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
fronty	fronta	k1gFnSc2	fronta
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
Lenin	Lenin	k1gMnSc1	Lenin
přestával	přestávat	k5eAaImAgMnS	přestávat
věřit	věřit	k5eAaImF	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dožije	dožít	k5eAaPmIp3nS	dožít
porážky	porážka	k1gFnSc2	porážka
carské	carský	k2eAgFnSc2d1	carská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Svržení	svržení	k1gNnSc1	svržení
cara	car	k1gMnSc2	car
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1917	[number]	k4	1917
exilové	exilový	k2eAgInPc4d1	exilový
revolucionáře	revolucionář	k1gMnPc4	revolucionář
šokovalo	šokovat	k5eAaBmAgNnS	šokovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
bez	bez	k7c2	bez
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Lenina	Lenin	k1gMnSc2	Lenin
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
moci	moct	k5eAaImF	moct
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
ujala	ujmout	k5eAaPmAgFnS	ujmout
buržoazie	buržoazie	k1gFnSc1	buržoazie
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
revolucionáři	revolucionář	k1gMnPc1	revolucionář
zkusí	zkusit	k5eAaPmIp3nP	zkusit
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
a	a	k8xC	a
udělat	udělat	k5eAaPmF	udělat
revoluci	revoluce	k1gFnSc4	revoluce
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1917	[number]	k4	1917
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
revolucionáři	revolucionář	k1gMnPc7	revolucionář
a	a	k8xC	a
také	také	k6eAd1	také
Armandovou	Armandový	k2eAgFnSc4d1	Armandová
<g/>
)	)	kIx)	)
vlakem	vlak	k1gInSc7	vlak
ze	z	k7c2	z
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
přes	přes	k7c4	přes
Švédsko	Švédsko	k1gNnSc4	Švédsko
do	do	k7c2	do
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Vagón	vagón	k1gInSc1	vagón
měl	mít	k5eAaImAgInS	mít
právo	právo	k1gNnSc4	právo
exteritoriality	exteritorialita	k1gFnSc2	exteritorialita
a	a	k8xC	a
na	na	k7c6	na
německém	německý	k2eAgNnSc6d1	německé
území	území	k1gNnSc6	území
ho	on	k3xPp3gMnSc4	on
střežili	střežit	k5eAaImAgMnP	střežit
němečtí	německý	k2eAgMnPc1d1	německý
vojáci	voják	k1gMnPc1	voják
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
cestě	cesta	k1gFnSc6	cesta
Lenina	Lenin	k1gMnSc2	Lenin
přes	přes	k7c4	přes
Německo	Německo	k1gNnSc4	Německo
německá	německý	k2eAgFnSc1d1	německá
vláda	vláda	k1gFnSc1	vláda
věděla	vědět	k5eAaImAgFnS	vědět
a	a	k8xC	a
legendy	legenda	k1gFnSc2	legenda
o	o	k7c6	o
právu	právo	k1gNnSc6	právo
exteritoriality	exteritorialita	k1gFnSc2	exteritorialita
a	a	k8xC	a
o	o	k7c6	o
"	"	kIx"	"
<g/>
zapečetěném	zapečetěný	k2eAgInSc6d1	zapečetěný
vagónu	vagón	k1gInSc6	vagón
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
jen	jen	k6eAd1	jen
dodatečným	dodatečný	k2eAgNnSc7d1	dodatečné
bolševickým	bolševický	k2eAgNnSc7d1	bolševické
kamuflováním	kamuflování	k1gNnSc7	kamuflování
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lenin	Lenin	k1gMnSc1	Lenin
jako	jako	k8xC	jako
příslušník	příslušník	k1gMnSc1	příslušník
nepřátelského	přátelský	k2eNgInSc2d1	nepřátelský
státu	stát	k1gInSc2	stát
byl	být	k5eAaImAgInS	být
s	s	k7c7	s
vědomím	vědomí	k1gNnSc7	vědomí
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
za	za	k7c4	za
německé	německý	k2eAgInPc4d1	německý
peníze	peníz	k1gInPc4	peníz
a	a	k8xC	a
s	s	k7c7	s
posláním	poslání	k1gNnSc7	poslání
od	od	k7c2	od
Němců	Němec	k1gMnPc2	Němec
přepraven	přepraven	k2eAgMnSc1d1	přepraven
ze	z	k7c2	z
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Císař	Císař	k1gMnSc1	Císař
Vilém	Vilém	k1gMnSc1	Vilém
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Pruský	pruský	k2eAgMnSc1d1	pruský
očekával	očekávat	k5eAaImAgMnS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lenin	Lenin	k1gMnSc1	Lenin
svojí	svůj	k3xOyFgFnSc7	svůj
revolucí	revoluce	k1gFnSc7	revoluce
paralyzuje	paralyzovat	k5eAaBmIp3nS	paralyzovat
bojeschopnost	bojeschopnost	k1gFnSc1	bojeschopnost
ruské	ruský	k2eAgFnSc2d1	ruská
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
ukončí	ukončit	k5eAaPmIp3nS	ukončit
válku	válka	k1gFnSc4	válka
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Leninovi	Lenin	k1gMnSc6	Lenin
pak	pak	k6eAd1	pak
viděl	vidět	k5eAaImAgInS	vidět
jen	jen	k9	jen
relativně	relativně	k6eAd1	relativně
bezvýznamnou	bezvýznamný	k2eAgFnSc4d1	bezvýznamná
postavu	postava	k1gFnSc4	postava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
ztratí	ztratit	k5eAaPmIp3nS	ztratit
moc	moc	k6eAd1	moc
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Do	do	k7c2	do
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
Lenin	Lenin	k1gMnSc1	Lenin
dorazil	dorazit	k5eAaPmAgMnS	dorazit
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
stává	stávat	k5eAaImIp3nS	stávat
se	s	k7c7	s
zde	zde	k6eAd1	zde
vedoucím	vedoucí	k2eAgMnSc7d1	vedoucí
představitelem	představitel	k1gMnSc7	představitel
bolševického	bolševický	k2eAgNnSc2d1	bolševické
hnutí	hnutí	k1gNnSc2	hnutí
a	a	k8xC	a
publikuje	publikovat	k5eAaBmIp3nS	publikovat
Dubnové	dubnový	k2eAgFnPc4d1	dubnová
teze	teze	k1gFnPc4	teze
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
dorazil	dorazit	k5eAaPmAgMnS	dorazit
též	též	k9	též
další	další	k2eAgMnSc1d1	další
revolucionář	revolucionář	k1gMnSc1	revolucionář
Lev	Lev	k1gMnSc1	Lev
Trockij	Trockij	k1gMnSc1	Trockij
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
Leninovou	Leninův	k2eAgFnSc7d1	Leninova
pravou	pravý	k2eAgFnSc7d1	pravá
rukou	ruka	k1gFnSc7	ruka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgMnPc7	dva
muži	muž	k1gMnPc7	muž
byly	být	k5eAaImAgInP	být
často	často	k6eAd1	často
spory	spor	k1gInPc1	spor
ohledně	ohledně	k7c2	ohledně
metod	metoda	k1gFnPc2	metoda
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1917	[number]	k4	1917
Lenin	Lenin	k1gMnSc1	Lenin
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
čtyřčlenné	čtyřčlenný	k2eAgNnSc4d1	čtyřčlenné
užší	úzký	k2eAgNnSc4d2	užší
vedení	vedení	k1gNnSc4	vedení
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
známé	známý	k2eAgNnSc1d1	známé
jako	jako	k8xS	jako
politické	politický	k2eAgNnSc1d1	politické
byro	byro	k1gNnSc1	byro
<g/>
,	,	kIx,	,
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
Lenin	Lenin	k1gMnSc1	Lenin
<g/>
,	,	kIx,	,
Zinověv	Zinověv	k1gMnSc1	Zinověv
<g/>
,	,	kIx,	,
Kameněv	Kameněv	k1gMnSc1	Kameněv
a	a	k8xC	a
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
poprvé	poprvé	k6eAd1	poprvé
dostal	dostat	k5eAaPmAgMnS	dostat
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěšném	úspěšný	k2eNgNnSc6d1	neúspěšné
červencovém	červencový	k2eAgNnSc6d1	červencové
dělnickém	dělnický	k2eAgNnSc6d1	dělnické
povstání	povstání	k1gNnSc6	povstání
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
jeho	jeho	k3xOp3gMnPc4	jeho
vůdce	vůdce	k1gMnPc4	vůdce
vydán	vydán	k2eAgInSc1d1	vydán
zatykač	zatykač	k1gInSc1	zatykač
<g/>
,	,	kIx,	,
Kameněv	Kameněv	k1gMnSc1	Kameněv
a	a	k8xC	a
Trocký	Trocký	k1gMnSc1	Trocký
byli	být	k5eAaImAgMnP	být
zatčeni	zatknout	k5eAaPmNgMnP	zatknout
<g/>
,	,	kIx,	,
Lenin	Lenin	k1gMnSc1	Lenin
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
do	do	k7c2	do
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Vrací	vracet	k5eAaImIp3nS	vracet
se	se	k3xPyFc4	se
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaPmIp3nS	využívat
slabosti	slabost	k1gFnSc2	slabost
Kerenského	Kerenský	k2eAgInSc2d1	Kerenský
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
začíná	začínat	k5eAaImIp3nS	začínat
intenzivně	intenzivně	k6eAd1	intenzivně
připravovat	připravovat	k5eAaImF	připravovat
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
nakonec	nakonec	k6eAd1	nakonec
pod	pod	k7c7	pod
heslem	heslo	k1gNnSc7	heslo
"	"	kIx"	"
<g/>
Všechnu	všechen	k3xTgFnSc4	všechen
moc	moc	k1gFnSc4	moc
sovětům	sovět	k1gInPc3	sovět
<g/>
"	"	kIx"	"
uskutečnil	uskutečnit	k5eAaPmAgMnS	uskutečnit
počátkem	počátkem	k7c2	počátkem
listopadu	listopad	k1gInSc2	listopad
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Velká	velký	k2eAgFnSc1d1	velká
říjnová	říjnový	k2eAgFnSc1d1	říjnová
socialistická	socialistický	k2eAgFnSc1d1	socialistická
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
tak	tak	k9	tak
Sovětské	sovětský	k2eAgNnSc1d1	sovětské
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
představy	představa	k1gFnPc4	představa
o	o	k7c6	o
vládě	vláda	k1gFnSc6	vláda
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
v	v	k7c6	v
eseji	esej	k1gFnSc6	esej
"	"	kIx"	"
<g/>
Stát	stát	k1gInSc1	stát
a	a	k8xC	a
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
pro	pro	k7c4	pro
novou	nový	k2eAgFnSc4d1	nová
formu	forma	k1gFnSc4	forma
vlády	vláda	k1gFnSc2	vláda
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c4	na
existenci	existence	k1gFnSc4	existence
dělnických	dělnický	k2eAgFnPc2d1	Dělnická
rad	rada	k1gFnPc2	rada
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
sovětů	sovět	k1gInPc2	sovět
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgFnPc7	první
dekrety	dekret	k1gInPc7	dekret
sovětské	sovětský	k2eAgFnSc2d1	sovětská
moci	moc	k1gFnSc2	moc
byly	být	k5eAaImAgInP	být
Dekret	dekret	k1gInSc4	dekret
o	o	k7c6	o
míru	mír	k1gInSc6	mír
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vyzýval	vyzývat	k5eAaImAgMnS	vyzývat
všechny	všechen	k3xTgFnPc4	všechen
válčící	válčící	k2eAgFnPc4d1	válčící
strany	strana	k1gFnPc4	strana
k	k	k7c3	k
ukončení	ukončení	k1gNnSc3	ukončení
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
a	a	k8xC	a
Dekret	dekret	k1gInSc1	dekret
o	o	k7c6	o
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
dával	dávat	k5eAaImAgMnS	dávat
miliónům	milión	k4xCgInPc3	milión
bezzemků	bezzemek	k1gMnPc2	bezzemek
půdu	půda	k1gFnSc4	půda
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgMnPc4	který
léta	léto	k1gNnPc1	léto
pracovali	pracovat	k5eAaImAgMnP	pracovat
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
je	být	k5eAaImIp3nS	být
Lenin	Lenin	k1gMnSc1	Lenin
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
rady	rada	k1gMnSc2	rada
lidových	lidový	k2eAgMnPc2d1	lidový
komisařů	komisař	k1gMnPc2	komisař
<g/>
.	.	kIx.	.
</s>
<s>
Tváří	tvářet	k5eAaImIp3nS	tvářet
v	v	k7c4	v
tvář	tvář	k1gFnSc4	tvář
hrozbě	hrozba	k1gFnSc3	hrozba
německé	německý	k2eAgFnSc2d1	německá
invaze	invaze	k1gFnSc2	invaze
usiloval	usilovat	k5eAaImAgMnS	usilovat
Lenin	Lenin	k1gMnSc1	Lenin
o	o	k7c4	o
podepsání	podepsání	k1gNnSc4	podepsání
okamžité	okamžitý	k2eAgFnSc2d1	okamžitá
mírové	mírový	k2eAgFnSc2d1	mírová
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
bolševičtí	bolševický	k2eAgMnPc1d1	bolševický
vůdci	vůdce	k1gMnPc1	vůdce
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Bucharin	Bucharin	k1gInSc4	Bucharin
<g/>
,	,	kIx,	,
obhajovali	obhajovat	k5eAaImAgMnP	obhajovat
pokračování	pokračování	k1gNnSc4	pokračování
války	válka	k1gFnSc2	válka
jako	jako	k8xS	jako
prostředku	prostředek	k1gInSc2	prostředek
podněcujícího	podněcující	k2eAgInSc2d1	podněcující
revoluci	revoluce	k1gFnSc3	revoluce
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Lev	Lev	k1gMnSc1	Lev
Trockij	Trockij	k1gMnSc1	Trockij
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vedl	vést	k5eAaImAgMnS	vést
vyjednávání	vyjednávání	k1gNnSc4	vyjednávání
<g/>
,	,	kIx,	,
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
kompromis	kompromis	k1gInSc4	kompromis
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
mírové	mírový	k2eAgFnSc2d1	mírová
smlouvy	smlouva	k1gFnSc2	smlouva
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nulových	nulový	k2eAgInPc2d1	nulový
územních	územní	k2eAgInPc2d1	územní
nároků	nárok	k1gInPc2	nárok
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
kolapsu	kolaps	k1gInSc6	kolaps
vyjednávání	vyjednávání	k1gNnSc2	vyjednávání
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
Německo	Německo	k1gNnSc1	Německo
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
invazi	invaze	k1gFnSc4	invaze
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
důsledku	důsledek	k1gInSc6	důsledek
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
Rusko	Rusko	k1gNnSc1	Rusko
obrovská	obrovský	k2eAgNnPc4d1	obrovské
území	území	k1gNnSc4	území
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
těchto	tento	k3xDgFnPc2	tento
událostí	událost	k1gFnPc2	událost
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
posílení	posílení	k1gNnSc3	posílení
Leninových	Leninových	k2eAgFnPc2d1	Leninových
pozic	pozice	k1gFnPc2	pozice
v	v	k7c6	v
bolševické	bolševický	k2eAgFnSc6d1	bolševická
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1918	[number]	k4	1918
Lenin	Lenin	k1gMnSc1	Lenin
nařídil	nařídit	k5eAaPmAgMnS	nařídit
svým	svůj	k3xOyFgMnPc3	svůj
emisarům	emisar	k1gMnPc3	emisar
přijmout	přijmout	k5eAaPmF	přijmout
německý	německý	k2eAgInSc4d1	německý
návrh	návrh	k1gInSc4	návrh
na	na	k7c6	na
ukončení	ukončení	k1gNnSc6	ukončení
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Brestlitevský	brestlitevský	k2eAgInSc1d1	brestlitevský
mír	mír	k1gInSc1	mír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
splnil	splnit	k5eAaPmAgMnS	splnit
slib	slib	k1gInSc4	slib
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pomohl	pomoct	k5eAaPmAgMnS	pomoct
jeho	jeho	k3xOp3gFnSc3	jeho
straně	strana	k1gFnSc3	strana
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
soustředit	soustředit	k5eAaPmF	soustředit
na	na	k7c4	na
domácí	domácí	k2eAgInPc4d1	domácí
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
konečně	konečně	k6eAd1	konečně
mohl	moct	k5eAaImAgInS	moct
začít	začít	k5eAaPmF	začít
organizovat	organizovat	k5eAaBmF	organizovat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
čeho	co	k3yQnSc2	co
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
nemohla	moct	k5eNaImAgFnS	moct
žádná	žádný	k3yNgFnSc1	žádný
revoluce	revoluce	k1gFnSc1	revoluce
dospět	dospět	k5eAaPmF	dospět
k	k	k7c3	k
pozitivnímu	pozitivní	k2eAgInSc3d1	pozitivní
výsledku	výsledek	k1gInSc3	výsledek
<g/>
:	:	kIx,	:
teror	teror	k1gInSc1	teror
jakobínského	jakobínský	k2eAgInSc2d1	jakobínský
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
nechal	nechat	k5eAaPmAgInS	nechat
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
zřídit	zřídit	k5eAaPmF	zřídit
Čeku	čeka	k1gFnSc4	čeka
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
zavedl	zavést	k5eAaPmAgInS	zavést
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
zrušený	zrušený	k2eAgInSc4d1	zrušený
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
prosadil	prosadit	k5eAaPmAgMnS	prosadit
popravy	poprava	k1gFnPc4	poprava
"	"	kIx"	"
<g/>
nepřátelských	přátelský	k2eNgInPc2d1	nepřátelský
elementů	element	k1gInPc2	element
<g/>
"	"	kIx"	"
bez	bez	k7c2	bez
soudu	soud	k1gInSc2	soud
jako	jako	k8xS	jako
standard	standard	k1gInSc4	standard
<g/>
.	.	kIx.	.
</s>
<s>
Obhajoval	obhajovat	k5eAaImAgInS	obhajovat
to	ten	k3xDgNnSc1	ten
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jestliže	jestliže	k8xS	jestliže
nemůžeme	moct	k5eNaImIp1nP	moct
zastřelit	zastřelit	k5eAaPmF	zastřelit
bělogvardějského	bělogvardějský	k2eAgMnSc4d1	bělogvardějský
sabotéra	sabotér	k1gMnSc4	sabotér
<g/>
,	,	kIx,	,
jaká	jaký	k3yRgFnSc1	jaký
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
potom	potom	k6eAd1	potom
velká	velký	k2eAgFnSc1d1	velká
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
bylo	být	k5eAaImAgNnS	být
naloženo	naložit	k5eAaPmNgNnS	naložit
také	také	k9	také
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
carskou	carský	k2eAgFnSc7d1	carská
rodinou	rodina	k1gFnSc7	rodina
-	-	kIx~	-
"	"	kIx"	"
<g/>
popraven	popraven	k2eAgMnSc1d1	popraven
<g/>
"	"	kIx"	"
bez	bez	k7c2	bez
soudu	soud	k1gInSc2	soud
byl	být	k5eAaImAgMnS	být
car	car	k1gMnSc1	car
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
a	a	k8xC	a
všech	všecek	k3xTgFnPc2	všecek
pět	pět	k4xCc1	pět
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
osobní	osobní	k2eAgMnSc1d1	osobní
lékař	lékař	k1gMnSc1	lékař
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
služebnictvo	služebnictvo	k1gNnSc4	služebnictvo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
jim	on	k3xPp3gMnPc3	on
zbylo	zbýt	k5eAaPmAgNnS	zbýt
<g/>
.	.	kIx.	.
</s>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
důkaz	důkaz	k1gInSc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jeho	jeho	k3xOp3gInSc2	jeho
přímého	přímý	k2eAgInSc2d1	přímý
rozkazu	rozkaz	k1gInSc2	rozkaz
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jisté	jistý	k2eAgNnSc1d1	jisté
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc4	tento
řešení	řešení	k1gNnSc4	řešení
podporoval	podporovat	k5eAaImAgInS	podporovat
(	(	kIx(	(
<g/>
opakovaně	opakovaně	k6eAd1	opakovaně
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
vyhlazení	vyhlazení	k1gNnSc4	vyhlazení
celého	celý	k2eAgInSc2d1	celý
rodu	rod	k1gInSc2	rod
Romanovců	Romanovec	k1gMnPc2	Romanovec
<g/>
)	)	kIx)	)
a	a	k8xC	a
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
jeho	jeho	k3xOp3gFnPc7	jeho
instrukcemi	instrukce	k1gFnPc7	instrukce
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
historici	historik	k1gMnPc1	historik
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nanejvýš	nanejvýš	k6eAd1	nanejvýš
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1	nepravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
někdo	někdo	k3yInSc1	někdo
opovážil	opovážit	k5eAaPmAgMnS	opovážit
popravit	popravit	k5eAaPmF	popravit
cara	car	k1gMnSc4	car
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
rodinu	rodina	k1gFnSc4	rodina
bez	bez	k7c2	bez
jeho	on	k3xPp3gNnSc2	on
vědomí	vědomí	k1gNnSc2	vědomí
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
předběžného	předběžný	k2eAgInSc2d1	předběžný
souhlasu	souhlas	k1gInSc2	souhlas
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
byl	být	k5eAaImAgMnS	být
zastřelen	zastřelen	k2eAgMnSc1d1	zastřelen
náčelník	náčelník	k1gMnSc1	náčelník
Petrohradské	petrohradský	k2eAgFnSc2d1	Petrohradská
Čeky	čeka	k1gFnSc2	čeka
Mojsej	Mojsej	k1gMnSc1	Mojsej
Urickij	Urickij	k1gMnSc1	Urickij
<g/>
.	.	kIx.	.
</s>
<s>
Lenin	Lenin	k1gMnSc1	Lenin
se	se	k3xPyFc4	se
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
události	událost	k1gFnSc6	událost
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
v	v	k7c6	v
Michelsonově	Michelsonův	k2eAgFnSc6d1	Michelsonův
továrně	továrna	k1gFnSc6	továrna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgInS	mít
proslov	proslov	k1gInSc1	proslov
(	(	kIx(	(
<g/>
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
československým	československý	k2eAgMnPc3d1	československý
legionářům	legionář	k1gMnPc3	legionář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
došel	dojít	k5eAaPmAgInS	dojít
k	k	k7c3	k
autu	auto	k1gNnSc3	auto
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
obklopil	obklopit	k5eAaPmAgInS	obklopit
hlouček	hlouček	k1gInSc1	hlouček
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
třeskly	třesknout	k5eAaPmAgInP	třesknout
tři	tři	k4xCgInPc1	tři
výstřely	výstřel	k1gInPc1	výstřel
<g/>
.	.	kIx.	.
</s>
<s>
Lenin	Lenin	k1gMnSc1	Lenin
byl	být	k5eAaImAgMnS	být
zasažen	zasáhnout	k5eAaPmNgMnS	zasáhnout
do	do	k7c2	do
klíční	klíční	k2eAgFnSc2d1	klíční
kosti	kost	k1gFnSc2	kost
a	a	k8xC	a
zčásti	zčásti	k6eAd1	zčásti
do	do	k7c2	do
plíce	plíce	k1gFnSc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Atentátnicí	atentátnice	k1gFnSc7	atentátnice
byla	být	k5eAaImAgFnS	být
členka	členka	k1gFnSc1	členka
strany	strana	k1gFnSc2	strana
Eserů	eser	k1gMnPc2	eser
Fanny	Fanna	k1gFnSc2	Fanna
Kaplanová	Kaplanová	k1gFnSc1	Kaplanová
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
popravena	popraven	k2eAgFnSc1d1	popravena
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
Leninovy	Leninův	k2eAgFnSc2d1	Leninova
iniciativy	iniciativa	k1gFnSc2	iniciativa
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
internacionála	internacionála	k1gFnSc1	internacionála
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
měla	mít	k5eAaImAgFnS	mít
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
revoluce	revoluce	k1gFnSc2	revoluce
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
expandovala	expandovat	k5eAaImAgFnS	expandovat
do	do	k7c2	do
sousedních	sousední	k2eAgFnPc2d1	sousední
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
tažení	tažení	k1gNnSc1	tažení
Polskem	Polsko	k1gNnSc7	Polsko
dávalo	dávat	k5eAaImAgNnS	dávat
naději	naděje	k1gFnSc4	naděje
na	na	k7c4	na
spojení	spojení	k1gNnSc4	spojení
se	se	k3xPyFc4	se
nejen	nejen	k6eAd1	nejen
s	s	k7c7	s
maďarskou	maďarský	k2eAgFnSc7d1	maďarská
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
a	a	k8xC	a
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
revolucí	revoluce	k1gFnSc7	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
bitvou	bitva	k1gFnSc7	bitva
u	u	k7c2	u
Varšavy	Varšava	k1gFnSc2	Varšava
poslal	poslat	k5eAaPmAgInS	poslat
Lenin	Lenin	k2eAgInSc1d1	Lenin
vzkaz	vzkaz	k1gInSc1	vzkaz
italským	italský	k2eAgMnPc3d1	italský
komunistům	komunista	k1gMnPc3	komunista
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
bezodkladně	bezodkladně	k6eAd1	bezodkladně
připravit	připravit	k5eAaPmF	připravit
revoluci	revoluce	k1gFnSc4	revoluce
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
usnadnila	usnadnit	k5eAaPmAgFnS	usnadnit
Rudé	rudý	k2eAgMnPc4d1	rudý
armádě	armáda	k1gFnSc6	armáda
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
Říma	Řím	k1gInSc2	Řím
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bude	být	k5eAaImBp3nS	být
sovětizováno	sovětizován	k2eAgNnSc1d1	sovětizován
Československo	Československo	k1gNnSc1	Československo
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
a	a	k8xC	a
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
plány	plán	k1gInPc1	plán
ovšem	ovšem	k9	ovšem
přišly	přijít	k5eAaPmAgInP	přijít
vniveč	vniveč	k6eAd1	vniveč
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Varšavy	Varšava	k1gFnSc2	Varšava
rozdrcena	rozdrtit	k5eAaPmNgFnS	rozdrtit
<g/>
.	.	kIx.	.
</s>
<s>
Lenin	Lenin	k1gMnSc1	Lenin
sám	sám	k3xTgMnSc1	sám
tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
rozhořčen	rozhořčen	k2eAgInSc1d1	rozhořčen
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
bral	brát	k5eAaImAgInS	brát
to	ten	k3xDgNnSc1	ten
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
odklad	odklad	k1gInSc4	odklad
nevyhnutelného	vyhnutelný	k2eNgInSc2d1	nevyhnutelný
<g/>
.	.	kIx.	.
</s>
<s>
Pevně	pevně	k6eAd1	pevně
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
co	co	k3yQnSc1	co
nevyšlo	vyjít	k5eNaPmAgNnS	vyjít
nyní	nyní	k6eAd1	nyní
<g/>
,	,	kIx,	,
vyjde	vyjít	k5eAaPmIp3nS	vyjít
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vědecky	vědecky	k6eAd1	vědecky
předpovězená	předpovězený	k2eAgFnSc1d1	předpovězená
nutnost	nutnost	k1gFnSc1	nutnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
příkaz	příkaz	k1gInSc4	příkaz
SSSR	SSSR	kA	SSSR
jako	jako	k8xS	jako
první	první	k4xOgInSc4	první
stát	stát	k1gInSc4	stát
na	na	k7c6	na
světě	svět	k1gInSc6	svět
zlegalizoval	zlegalizovat	k5eAaPmAgMnS	zlegalizovat
umělé	umělý	k2eAgInPc4d1	umělý
potraty	potrat	k1gInPc4	potrat
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
těhotenství	těhotenství	k1gNnSc2	těhotenství
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
nechal	nechat	k5eAaPmAgMnS	nechat
Lenin	Lenin	k1gMnSc1	Lenin
brutálně	brutálně	k6eAd1	brutálně
potlačit	potlačit	k5eAaPmF	potlačit
vzpouru	vzpoura	k1gFnSc4	vzpoura
kronštadtských	kronštadtský	k2eAgMnPc2d1	kronštadtský
námořníků	námořník	k1gMnPc2	námořník
a	a	k8xC	a
několik	několik	k4yIc4	několik
rolnických	rolnický	k2eAgNnPc2d1	rolnické
povstání	povstání	k1gNnPc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
vydal	vydat	k5eAaPmAgInS	vydat
sérii	série	k1gFnSc4	série
brutálních	brutální	k2eAgInPc2d1	brutální
rozkazů	rozkaz	k1gInPc2	rozkaz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
udělaly	udělat	k5eAaPmAgFnP	udělat
z	z	k7c2	z
poprav	poprava	k1gFnPc2	poprava
bez	bez	k7c2	bez
soudu	soud	k1gInSc2	soud
standardní	standardní	k2eAgFnSc4d1	standardní
metodu	metoda	k1gFnSc4	metoda
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
vydal	vydat	k5eAaPmAgMnS	vydat
směrnice	směrnice	k1gFnSc2	směrnice
pro	pro	k7c4	pro
boj	boj	k1gInSc4	boj
s	s	k7c7	s
církvemi	církev	k1gFnPc7	církev
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
s	s	k7c7	s
pravoslavnou	pravoslavný	k2eAgFnSc7d1	pravoslavná
církví	církev	k1gFnSc7	církev
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
nařídil	nařídit	k5eAaPmAgInS	nařídit
odstranit	odstranit	k5eAaPmF	odstranit
cenné	cenný	k2eAgFnPc4d1	cenná
věci	věc	k1gFnPc4	věc
z	z	k7c2	z
kostelů	kostel	k1gInPc2	kostel
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
přitom	přitom	k6eAd1	přitom
k	k	k7c3	k
vraždění	vraždění	k1gNnSc3	vraždění
kněží	kněz	k1gMnPc1	kněz
(	(	kIx(	(
<g/>
z	z	k7c2	z
Leninových	Leninových	k2eAgFnPc2d1	Leninových
instrukcí	instrukce	k1gFnPc2	instrukce
Čece	čeka	k1gFnSc6	čeka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Čím	co	k3yInSc7	co
větší	veliký	k2eAgNnSc1d2	veliký
množství	množství	k1gNnSc1	množství
reakcionářských	reakcionářský	k2eAgMnPc2d1	reakcionářský
kněží	kněz	k1gMnPc2	kněz
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
reakcionářské	reakcionářský	k2eAgFnPc4d1	reakcionářská
buržoazie	buržoazie	k1gFnPc4	buržoazie
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
záležitosti	záležitost	k1gFnSc6	záležitost
zastřeleno	zastřelen	k2eAgNnSc1d1	zastřeleno
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
lépe	dobře	k6eAd2	dobře
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Když	když	k8xS	když
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
protesty	protest	k1gInPc1	protest
proti	proti	k7c3	proti
popravám	poprava	k1gFnPc3	poprava
a	a	k8xC	a
krutému	krutý	k2eAgNnSc3d1	kruté
zacházení	zacházení	k1gNnSc3	zacházení
zejména	zejména	k6eAd1	zejména
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
<g/>
,	,	kIx,	,
Lenin	Lenin	k1gMnSc1	Lenin
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ať	ať	k9	ať
psíčci	psíček	k1gMnPc1	psíček
buržoazní	buržoazní	k2eAgFnSc2d1	buržoazní
společnosti	společnost	k1gFnSc2	společnost
<g/>
...	...	k?	...
poštěkávají	poštěkávat	k5eAaImIp3nP	poštěkávat
a	a	k8xC	a
kňourají	kňourat	k5eAaImIp3nP	kňourat
nad	nad	k7c7	nad
utracením	utracení	k1gNnSc7	utracení
každého	každý	k3xTgNnSc2	každý
nežádoucího	žádoucí	k2eNgNnSc2d1	nežádoucí
štěněte	štěně	k1gNnSc2	štěně
<g/>
.	.	kIx.	.
</s>
<s>
My	my	k3xPp1nPc1	my
kácíme	kácet	k5eAaImIp1nP	kácet
velký	velký	k2eAgInSc4d1	velký
starý	starý	k2eAgInSc4d1	starý
prales	prales	k1gInSc4	prales
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
politice	politika	k1gFnSc6	politika
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
kolektivizaci	kolektivizace	k1gFnSc4	kolektivizace
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
tvorbu	tvorba	k1gFnSc4	tvorba
kolchozů	kolchoz	k1gInPc2	kolchoz
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
elektrifikaci	elektrifikace	k1gFnSc4	elektrifikace
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
přestavbu	přestavba	k1gFnSc4	přestavba
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
relativně	relativně	k6eAd1	relativně
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
lze	lze	k6eAd1	lze
zhodnotit	zhodnotit	k5eAaPmF	zhodnotit
pouze	pouze	k6eAd1	pouze
projekt	projekt	k1gInSc4	projekt
elektrifikace	elektrifikace	k1gFnSc2	elektrifikace
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
skončil	skončit	k5eAaPmAgInS	skončit
katastrofou	katastrofa	k1gFnSc7	katastrofa
a	a	k8xC	a
vedl	vést	k5eAaImAgInS	vést
ke	k	k7c3	k
zbídačení	zbídačení	k1gNnSc3	zbídačení
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Lenin	Lenin	k1gMnSc1	Lenin
musel	muset	k5eAaImAgMnS	muset
nakonec	nakonec	k6eAd1	nakonec
přistoupit	přistoupit	k5eAaPmF	přistoupit
na	na	k7c4	na
politiku	politika	k1gFnSc4	politika
tzv.	tzv.	kA	tzv.
NEPu	Nep	k1gInSc2	Nep
(	(	kIx(	(
<g/>
povolení	povolení	k1gNnSc4	povolení
určité	určitý	k2eAgFnSc2d1	určitá
formy	forma	k1gFnSc2	forma
drobného	drobný	k2eAgNnSc2d1	drobné
ekonomického	ekonomický	k2eAgNnSc2d1	ekonomické
podnikání	podnikání	k1gNnSc2	podnikání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zabránil	zabránit	k5eAaPmAgMnS	zabránit
úplnému	úplný	k2eAgNnSc3d1	úplné
zhroucení	zhroucení	k1gNnSc3	zhroucení
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
od	od	k7c2	od
kolektivizace	kolektivizace	k1gFnSc2	kolektivizace
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1922	[number]	k4	1922
ranila	ranit	k5eAaPmAgFnS	ranit
Lenina	Lenin	k2eAgFnSc1d1	Lenina
první	první	k4xOgFnSc1	první
mozková	mozkový	k2eAgFnSc1d1	mozková
mrtvice	mrtvice	k1gFnSc1	mrtvice
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
trpěl	trpět	k5eAaImAgMnS	trpět
syfilidou	syfilida	k1gFnSc7	syfilida
v	v	k7c6	v
pokročilém	pokročilý	k2eAgNnSc6d1	pokročilé
stadiu	stadion	k1gNnSc6	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
se	se	k3xPyFc4	se
po	po	k7c6	po
infarktu	infarkt	k1gInSc6	infarkt
sice	sice	k8xC	sice
zotavil	zotavit	k5eAaPmAgMnS	zotavit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neustále	neustále	k6eAd1	neustále
si	se	k3xPyFc3	se
stěžoval	stěžovat	k5eAaImAgMnS	stěžovat
na	na	k7c4	na
nedostatek	nedostatek	k1gInSc4	nedostatek
duševního	duševní	k2eAgInSc2d1	duševní
klidu	klid	k1gInSc2	klid
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
psychika	psychika	k1gFnSc1	psychika
byla	být	k5eAaImAgFnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
vážně	vážně	k6eAd1	vážně
narušena	narušit	k5eAaPmNgFnS	narušit
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
stále	stále	k6eAd1	stále
zastával	zastávat	k5eAaImAgMnS	zastávat
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
i	i	k9	i
fakticky	fakticky	k6eAd1	fakticky
<g/>
)	)	kIx)	)
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
úlohu	úloha	k1gFnSc4	úloha
ve	v	k7c6	v
státu	stát	k1gInSc6	stát
až	až	k6eAd1	až
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc1	jeho
stav	stav	k1gInSc1	stav
opět	opět	k6eAd1	opět
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
prosincem	prosinec	k1gInSc7	prosinec
a	a	k8xC	a
březnem	březen	k1gInSc7	březen
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
sepsal	sepsat	k5eAaPmAgMnS	sepsat
tzv.	tzv.	kA	tzv.
závěť	závěť	k1gFnSc1	závěť
(	(	kIx(	(
<g/>
Dopis	dopis	k1gInSc1	dopis
sjezdu	sjezd	k1gInSc2	sjezd
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgMnPc4	který
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
varoval	varovat	k5eAaImAgMnS	varovat
před	před	k7c7	před
Stalinem	Stalin	k1gMnSc7	Stalin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
dostal	dostat	k5eAaPmAgMnS	dostat
další	další	k2eAgFnSc4d1	další
mrtvici	mrtvice	k1gFnSc4	mrtvice
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
už	už	k6eAd1	už
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
zbyla	zbýt	k5eAaPmAgFnS	zbýt
pouze	pouze	k6eAd1	pouze
lidská	lidský	k2eAgFnSc1d1	lidská
troska	troska	k1gFnSc1	troska
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Leninovo	Leninův	k2eAgNnSc1d1	Leninovo
tělo	tělo	k1gNnSc1	tělo
bylo	být	k5eAaImAgNnS	být
nabalzamováno	nabalzamovat	k5eAaPmNgNnS	nabalzamovat
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
vystavováno	vystavovat	k5eAaImNgNnS	vystavovat
v	v	k7c6	v
Leninově	Leninův	k2eAgNnSc6d1	Leninovo
mauzoleu	mauzoleum	k1gNnSc6	mauzoleum
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Lenina	Lenin	k1gMnSc2	Lenin
usilovali	usilovat	k5eAaImAgMnP	usilovat
tito	tento	k3xDgMnPc1	tento
muži	muž	k1gMnPc1	muž
o	o	k7c4	o
absolutní	absolutní	k2eAgFnSc4d1	absolutní
moc	moc	k1gFnSc4	moc
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
:	:	kIx,	:
Josif	Josif	k1gMnSc1	Josif
Vissarionovič	Vissarionovič	k1gMnSc1	Vissarionovič
Stalin	Stalin	k1gMnSc1	Stalin
Lev	Lev	k1gMnSc1	Lev
Davidovič	Davidovič	k1gMnSc1	Davidovič
Trockij	Trockij	k1gMnSc1	Trockij
Grigorij	Grigorij	k1gMnSc1	Grigorij
Zinovjev	Zinovjev	k1gMnSc1	Zinovjev
Lev	Lev	k1gMnSc1	Lev
Kameněv	Kameněv	k1gMnSc1	Kameněv
Alexej	Alexej	k1gMnSc1	Alexej
Rykov	Rykov	k1gInSc1	Rykov
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Bucharin	Bucharin	k1gInSc1	Bucharin
Michail	Michail	k1gMnSc1	Michail
Tomskij	Tomskij	k1gMnSc1	Tomskij
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
posléze	posléze	k6eAd1	posléze
nechal	nechat	k5eAaPmAgMnS	nechat
všechny	všechen	k3xTgMnPc4	všechen
své	svůj	k3xOyFgMnPc4	svůj
zde	zde	k6eAd1	zde
jmenované	jmenovaný	k2eAgMnPc4d1	jmenovaný
soupeře	soupeř	k1gMnPc4	soupeř
po	po	k7c6	po
získání	získání	k1gNnSc4	získání
moci	moc	k1gFnSc2	moc
fyzicky	fyzicky	k6eAd1	fyzicky
zlikvidovat	zlikvidovat	k5eAaPmF	zlikvidovat
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc4	co
dělat	dělat	k5eAaImF	dělat
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
-	-	kIx~	-
konkrétní	konkrétní	k2eAgInPc4d1	konkrétní
návrhy	návrh	k1gInPc4	návrh
revoluční	revoluční	k2eAgFnSc2d1	revoluční
organizace	organizace	k1gFnSc2	organizace
a	a	k8xC	a
strategie	strategie	k1gFnSc2	strategie
Materialismus	materialismus	k1gInSc1	materialismus
a	a	k8xC	a
empiriokriticismus	empiriokriticismus	k1gInSc1	empiriokriticismus
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
-	-	kIx~	-
filosofický	filosofický	k2eAgInSc1d1	filosofický
spis	spis	k1gInSc1	spis
<g/>
,	,	kIx,	,
teorie	teorie	k1gFnSc1	teorie
poznání	poznání	k1gNnSc2	poznání
(	(	kIx(	(
<g/>
gnozeologie	gnozeologie	k1gFnSc1	gnozeologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dialektický	dialektický	k2eAgInSc1d1	dialektický
materialismus	materialismus	k1gInSc1	materialismus
<g/>
;	;	kIx,	;
polemika	polemika	k1gFnSc1	polemika
s	s	k7c7	s
dobovou	dobový	k2eAgFnSc7d1	dobová
filosofií	filosofie	k1gFnSc7	filosofie
Ernsta	Ernst	k1gMnSc2	Ernst
Macha	Mach	k1gMnSc2	Mach
Imperialismus	imperialismus	k1gInSc4	imperialismus
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
stádium	stádium	k1gNnSc4	stádium
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
-	-	kIx~	-
spis	spis	k1gInSc1	spis
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yIgNnSc2	který
kapitalismus	kapitalismus	k1gInSc1	kapitalismus
přišel	přijít	k5eAaPmAgInS	přijít
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
závěrečné	závěrečný	k2eAgFnSc2d1	závěrečná
fáze	fáze	k1gFnSc2	fáze
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
vládnou	vládnout	k5eAaImIp3nP	vládnout
monopoly	monopol	k1gInPc1	monopol
a	a	k8xC	a
finanční	finanční	k2eAgInSc1d1	finanční
kapitál	kapitál	k1gInSc1	kapitál
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
světa	svět	k1gInSc2	svět
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgFnPc4d3	veliký
kapitalistické	kapitalistický	k2eAgFnPc4d1	kapitalistická
země	zem	k1gFnPc4	zem
<g/>
;	;	kIx,	;
tuto	tento	k3xDgFnSc4	tento
fázi	fáze	k1gFnSc4	fáze
Lenin	Lenin	k1gMnSc1	Lenin
nazval	nazvat	k5eAaPmAgInS	nazvat
imperialismus	imperialismus	k1gInSc4	imperialismus
Stát	stát	k5eAaPmF	stát
a	a	k8xC	a
revoluce	revoluce	k1gFnSc2	revoluce
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
"	"	kIx"	"
<g/>
marxistické	marxistický	k2eAgNnSc1d1	marxistické
učení	učení	k1gNnSc1	učení
o	o	k7c6	o
státě	stát	k1gInSc6	stát
a	a	k8xC	a
úkolech	úkol	k1gInPc6	úkol
proletariátu	proletariát	k1gInSc2	proletariát
v	v	k7c4	v
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
údajná	údajný	k2eAgFnSc1d1	údajná
obrana	obrana	k1gFnSc1	obrana
Marxe	Marx	k1gMnSc2	Marx
a	a	k8xC	a
Engelse	Engels	k1gMnSc2	Engels
proti	proti	k7c3	proti
sociálnědemokratickým	sociálnědemokratický	k2eAgMnPc3d1	sociálnědemokratický
reformistům	reformista	k1gMnPc3	reformista
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
Karl	Karl	k1gMnSc1	Karl
Kautsky	Kautsky	k1gMnSc1	Kautsky
<g/>
)	)	kIx)	)
Dětská	dětský	k2eAgFnSc1d1	dětská
nemoc	nemoc	k1gFnSc1	nemoc
"	"	kIx"	"
<g/>
levičátství	levičátství	k1gNnSc1	levičátství
<g/>
"	"	kIx"	"
v	v	k7c6	v
komunismu	komunismus	k1gInSc6	komunismus
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
-	-	kIx~	-
komunistická	komunistický	k2eAgFnSc1d1	komunistická
strategie	strategie	k1gFnSc1	strategie
<g/>
,	,	kIx,	,
kritika	kritika	k1gFnSc1	kritika
části	část	k1gFnSc2	část
komunistů	komunista	k1gMnPc2	komunista
<g/>
,	,	kIx,	,
členů	člen	k1gInPc2	člen
Třetí	třetí	k4xOgFnSc2	třetí
Internacionály	Internacionála	k1gFnSc2	Internacionála
</s>
