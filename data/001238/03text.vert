<s>
George	Georg	k1gMnSc2	Georg
Herbert	Herbert	k1gMnSc1	Herbert
Walker	Walker	k1gMnSc1	Walker
Bush	Bush	k1gMnSc1	Bush
(	(	kIx(	(
<g/>
*	*	kIx~	*
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
41	[number]	k4	41
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1989	[number]	k4	1989
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Zastával	zastávat	k5eAaImAgInS	zastávat
řadu	řada	k1gFnSc4	řada
vysokých	vysoký	k2eAgFnPc2d1	vysoká
politických	politický	k2eAgFnPc2d1	politická
a	a	k8xC	a
diplomatických	diplomatický	k2eAgFnPc2d1	diplomatická
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
kongresman	kongresman	k1gMnSc1	kongresman
za	za	k7c4	za
stát	stát	k1gInSc4	stát
Texas	Texas	k1gInSc1	Texas
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
-	-	kIx~	-
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
USA	USA	kA	USA
při	při	k7c6	při
OSN	OSN	kA	OSN
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
-	-	kIx~	-
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
Republikánů	republikán	k1gMnPc2	republikán
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
-	-	kIx~	-
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gMnSc1	vedoucí
styčného	styčný	k2eAgNnSc2d1	styčné
<g />
.	.	kIx.	.
</s>
<s>
úřadu	úřad	k1gInSc3	úřad
USA	USA	kA	USA
v	v	k7c6	v
Čínské	čínský	k2eAgFnSc6d1	čínská
lidové	lidový	k2eAgFnSc6d1	lidová
republice	republika	k1gFnSc6	republika
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
-	-	kIx~	-
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
CIA	CIA	kA	CIA
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
-	-	kIx~	-
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
a	a	k8xC	a
viceprezident	viceprezident	k1gMnSc1	viceprezident
USA	USA	kA	USA
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Ronalda	Ronald	k1gMnSc2	Ronald
Reagana	Reagan	k1gMnSc2	Reagan
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1985	[number]	k4	1985
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
dle	dle	k7c2	dle
XXV	XXV	kA	XXV
<g/>
.	.	kIx.	.
dodatku	dodatek	k1gInSc2	dodatek
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
po	po	k7c6	po
vyšetření	vyšetření	k1gNnSc6	vyšetření
prezidenta	prezident	k1gMnSc2	prezident
Reagana	Reagan	k1gMnSc2	Reagan
kolonoskopií	kolonoskopie	k1gFnSc7	kolonoskopie
úřadujícím	úřadující	k2eAgMnSc7d1	úřadující
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
syny	syn	k1gMnPc7	syn
jsou	být	k5eAaImIp3nP	být
43	[number]	k4	43
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
George	George	k1gNnSc2	George
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
guvernér	guvernér	k1gMnSc1	guvernér
Floridy	Florida	k1gFnSc2	Florida
Jeb	Jeb	k1gMnSc1	Jeb
Bush	Bush	k1gMnSc1	Bush
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc4	Československo
navštívil	navštívit	k5eAaPmAgMnS	navštívit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgInS	přijmout
čestné	čestný	k2eAgNnSc4d1	čestné
členství	členství	k1gNnSc4	členství
Masarykova	Masarykův	k2eAgNnSc2d1	Masarykovo
demokratického	demokratický	k2eAgNnSc2d1	demokratické
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
10	[number]	k4	10
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
"	"	kIx"	"
<g/>
sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
"	"	kIx"	"
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
převzal	převzít	k5eAaPmAgInS	převzít
též	též	k9	též
čestnou	čestný	k2eAgFnSc4d1	čestná
medaili	medaile	k1gFnSc4	medaile
T.G.	T.G.	k1gMnSc2	T.G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nositelem	nositel	k1gMnSc7	nositel
Řádu	řád	k1gInSc2	řád
Bílého	bílý	k2eAgInSc2d1	bílý
lva	lev	k1gInSc2	lev
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
a	a	k8xC	a
ocenění	ocenění	k1gNnSc1	ocenění
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
Freedom	Freedom	k1gInSc4	Freedom
Award	Award	k1gInSc1	Award
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
letadlová	letadlový	k2eAgFnSc1d1	letadlová
loď	loď	k1gFnSc1	loď
USS	USS	kA	USS
George	George	k1gNnSc2	George
H.	H.	kA	H.
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
(	(	kIx(	(
<g/>
CVN-	CVN-	k1gFnSc1	CVN-
<g/>
77	[number]	k4	77
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ve	v	k7c6	v
službě	služba	k1gFnSc6	služba
od	od	k7c2	od
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Invaze	invaze	k1gFnSc2	invaze
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
do	do	k7c2	do
Panamy	Panama	k1gFnSc2	Panama
<g/>
.	.	kIx.	.
</s>
<s>
Manuel	Manuel	k1gMnSc1	Manuel
Noriega	Norieg	k1gMnSc2	Norieg
byl	být	k5eAaImAgInS	být
administrativou	administrativa	k1gFnSc7	administrativa
spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
podezříván	podezřívat	k5eAaImNgMnS	podezřívat
ze	z	k7c2	z
špionáže	špionáž	k1gFnSc2	špionáž
pro	pro	k7c4	pro
Fidéla	Fidél	k1gMnSc4	Fidél
Castra	Castr	k1gMnSc4	Castr
<g/>
,	,	kIx,	,
vydírání	vydírání	k1gNnSc4	vydírání
a	a	k8xC	a
pašování	pašování	k1gNnSc4	pašování
drog	droga	k1gFnPc2	droga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1989	[number]	k4	1989
se	se	k3xPyFc4	se
v	v	k7c6	v
Panamě	Panama	k1gFnSc6	Panama
konaly	konat	k5eAaImAgFnP	konat
demokratické	demokratický	k2eAgFnPc1d1	demokratická
volby	volba	k1gFnPc1	volba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
opozice	opozice	k1gFnSc1	opozice
vedená	vedený	k2eAgFnSc1d1	vedená
Guillermo	Guillerma	k1gFnSc5	Guillerma
Endaraem	Endara	k1gInSc7	Endara
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
výsledky	výsledek	k1gInPc4	výsledek
vláda	vláda	k1gFnSc1	vláda
vedená	vedený	k2eAgFnSc1d1	vedená
Noriegou	Noriega	k1gFnSc7	Noriega
zamítla	zamítnout	k5eAaPmAgFnS	zamítnout
<g/>
.	.	kIx.	.
</s>
<s>
Noriega	Noriega	k1gFnSc1	Noriega
následně	následně	k6eAd1	následně
potlačil	potlačit	k5eAaPmAgInS	potlačit
vojenský	vojenský	k2eAgInSc1d1	vojenský
převrat	převrat	k1gInSc1	převrat
a	a	k8xC	a
masové	masový	k2eAgInPc1d1	masový
protesty	protest	k1gInPc1	protest
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Bush	Bush	k1gMnSc1	Bush
nařídil	nařídit	k5eAaPmAgMnS	nařídit
invazi	invaze	k1gFnSc4	invaze
do	do	k7c2	do
Panamy	Panama	k1gFnSc2	Panama
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zatknout	zatknout	k5eAaPmF	zatknout
samozvaného	samozvaný	k2eAgMnSc4d1	samozvaný
diktátora	diktátor	k1gMnSc4	diktátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1990	[number]	k4	1990
byl	být	k5eAaImAgInS	být
Noriega	Norieg	k1gMnSc2	Norieg
zatčen	zatčen	k2eAgMnSc1d1	zatčen
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
ke	k	k7c3	k
30	[number]	k4	30
letům	let	k1gInPc3	let
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Podporu	podpora	k1gFnSc4	podpora
nové	nový	k2eAgFnSc3d1	nová
vládě	vláda	k1gFnSc3	vláda
pak	pak	k6eAd1	pak
Bush	Bush	k1gMnSc1	Bush
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
během	během	k7c2	během
oficiální	oficiální	k2eAgFnSc2d1	oficiální
návštěvy	návštěva	k1gFnSc2	návštěva
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Rozpad	rozpad	k1gInSc1	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
George	Georg	k1gMnSc2	Georg
Bush	Bush	k1gMnSc1	Bush
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
jako	jako	k8xC	jako
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
za	za	k7c2	za
jehož	jenž	k3xRgNnSc2	jenž
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
skončila	skončit	k5eAaPmAgFnS	skončit
Studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
se	se	k3xPyFc4	se
Železná	železný	k2eAgFnSc1d1	železná
opona	opona	k1gFnSc1	opona
<g/>
.	.	kIx.	.
</s>
<s>
Dokončil	dokončit	k5eAaPmAgMnS	dokončit
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
započal	započnout	k5eAaPmAgMnS	započnout
jeho	jeho	k3xOp3gMnSc1	jeho
předchůdce	předchůdce	k1gMnSc1	předchůdce
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
schůzce	schůzka	k1gFnSc6	schůzka
s	s	k7c7	s
Gorbačovem	Gorbačov	k1gInSc7	Gorbačov
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
na	na	k7c6	na
Maltě	Malta	k1gFnSc6	Malta
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
3.12	[number]	k4	3.12
<g/>
.	.	kIx.	.
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c6	na
rozsáhlém	rozsáhlý	k2eAgNnSc6d1	rozsáhlé
odzbrojení	odzbrojení	k1gNnSc6	odzbrojení
a	a	k8xC	a
ukončení	ukončení	k1gNnSc6	ukončení
amerického	americký	k2eAgNnSc2d1	americké
embarga	embargo	k1gNnSc2	embargo
na	na	k7c6	na
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
podepsali	podepsat	k5eAaPmAgMnP	podepsat
strategickou	strategický	k2eAgFnSc4d1	strategická
smlouvu	smlouva	k1gFnSc4	smlouva
START	start	k1gInSc1	start
I.	I.	kA	I.
o	o	k7c4	o
omezení	omezení	k1gNnSc4	omezení
zbrojení	zbrojení	k1gNnSc2	zbrojení
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1990	[number]	k4	1990
irácký	irácký	k2eAgMnSc1d1	irácký
prezident	prezident	k1gMnSc1	prezident
Saddám	Saddám	k1gMnSc1	Saddám
Husajn	Husajn	k1gMnSc1	Husajn
anektoval	anektovat	k5eAaBmAgMnS	anektovat
Kuvajt	Kuvajt	k1gInSc4	Kuvajt
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
akce	akce	k1gFnSc1	akce
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
pohoršení	pohoršení	k1gNnSc4	pohoršení
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
George	Georg	k1gMnSc2	Georg
Bush	Bush	k1gMnSc1	Bush
dal	dát	k5eAaPmAgMnS	dát
dohromady	dohromady	k6eAd1	dohromady
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
koalici	koalice	k1gFnSc4	koalice
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
poté	poté	k6eAd1	poté
s	s	k7c7	s
mandátem	mandát	k1gInSc7	mandát
OSN	OSN	kA	OSN
zahájila	zahájit	k5eAaPmAgFnS	zahájit
Operaci	operace	k1gFnSc4	operace
Pouštní	pouštní	k2eAgFnSc2d1	pouštní
bouře	bouř	k1gFnSc2	bouř
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
donutit	donutit	k5eAaPmF	donutit
Iráčany	Iráčan	k1gMnPc4	Iráčan
k	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
z	z	k7c2	z
Kuvajtu	Kuvajt	k1gInSc2	Kuvajt
<g/>
.	.	kIx.	.
</s>
<s>
Aliance	aliance	k1gFnSc1	aliance
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
kvůli	kvůli	k7c3	kvůli
vysoké	vysoký	k2eAgFnSc3d1	vysoká
letecké	letecký	k2eAgFnSc3d1	letecká
převaze	převaha	k1gFnSc3	převaha
(	(	kIx(	(
<g/>
4000	[number]	k4	4000
Iráckých	irácký	k2eAgInPc2d1	irácký
tanků	tank	k1gInPc2	tank
bylo	být	k5eAaImAgNnS	být
zničeno	zničit	k5eAaPmNgNnS	zničit
během	během	k7c2	během
jediného	jediný	k2eAgInSc2d1	jediný
dne	den	k1gInSc2	den
<g/>
)	)	kIx)	)
a	a	k8xC	a
osvobodila	osvobodit	k5eAaPmAgFnS	osvobodit
tak	tak	k9	tak
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
NAFTA	nafta	k1gFnSc1	nafta
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prezidentství	prezidentství	k1gNnPc2	prezidentství
George	Georg	k1gMnSc2	Georg
Walkera	Walker	k1gMnSc2	Walker
Bushe	Bush	k1gMnSc2	Bush
byly	být	k5eAaImAgFnP	být
vyjednány	vyjednán	k2eAgInPc4d1	vyjednán
základní	základní	k2eAgInPc4d1	základní
kameny	kámen	k1gInPc4	kámen
Severoamerické	severoamerický	k2eAgFnSc2d1	severoamerická
dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c6	o
volném	volný	k2eAgInSc6d1	volný
obchodu	obchod	k1gInSc6	obchod
<g/>
.	.	kIx.	.
</s>
