<s>
Zemětřesení	zemětřesení	k1gNnSc1
v	v	k7c6
Petrinji	Petrinj	k1gInSc6
2020	#num#	k4
</s>
<s>
Zemětřesení	zemětřesení	k1gNnSc1
v	v	k7c6
Petrinji	Petrinj	k1gInSc6
2020	#num#	k4
</s>
<s>
Zemětřesení	zemětřesení	k1gNnSc1
v	v	k7c4
PetrinjiDatum	PetrinjiDatum	k1gNnSc4
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2020	#num#	k4
11	#num#	k4
<g/>
:	:	kIx,
<g/>
19	#num#	k4
UTC	UTC	kA
Síla	síla	k1gFnSc1
</s>
<s>
6,4	6,4	k4
Mw	Mw	k1gFnPc2
Epicentrum	epicentrum	k1gNnSc4
</s>
<s>
45	#num#	k4
<g/>
°	°	k?
<g/>
25	#num#	k4
<g/>
′	′	k?
<g/>
19	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
15	#num#	k4
<g/>
′	′	k?
<g/>
18	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Zasažené	zasažený	k2eAgFnSc2d1
země	zem	k1gFnSc2
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
Chorvatsko	Chorvatsko	k1gNnSc1
<g/>
,	,	kIx,
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
<g/>
,	,	kIx,
Slovinsko	Slovinsko	k1gNnSc1
Slovinsko	Slovinsko	k1gNnSc1
<g/>
,	,	kIx,
Maďarsko	Maďarsko	k1gNnSc1
Maďarsko	Maďarsko	k1gNnSc1
Oběti	oběť	k1gFnSc2
</s>
<s>
7	#num#	k4
mrtvých	mrtvý	k2eAgMnPc2d1
<g/>
,	,	kIx,
26	#num#	k4
zraněných	zraněný	k1gMnPc2
</s>
<s>
Článek	článek	k1gInSc1
o	o	k7c4
zemětřesení	zemětřesení	k1gNnSc4
na	na	k7c6
Wikizprávách	Wikizpráva	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
zemětřesení	zemětřesení	k1gNnSc3
ve	v	k7c6
středním	střední	k2eAgNnSc6d1
Chorvatsku	Chorvatsko	k1gNnSc6
u	u	k7c2
města	město	k1gNnSc2
Petrinja	Petrinja	k1gFnSc1
došlo	dojít	k5eAaPmAgNnS
29	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2020	#num#	k4
v	v	k7c4
12	#num#	k4
<g/>
:	:	kIx,
<g/>
19	#num#	k4
SEČ	SEČ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosáhlo	dosáhnout	k5eAaPmAgNnS
magnituda	magnitudo	k1gNnPc4
6,4	6,4	k4
a	a	k8xC
stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
tak	tak	k9
jedním	jeden	k4xCgNnSc7
z	z	k7c2
nejsilnějších	silný	k2eAgNnPc2d3
zaznamenaných	zaznamenaný	k2eAgNnPc2d1
zemětřesení	zemětřesení	k1gNnPc2
v	v	k7c6
dějinách	dějiny	k1gFnPc6
Chorvatska	Chorvatsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřelo	zemřít	k5eAaPmAgNnS
sedm	sedm	k4xCc1
lidí	člověk	k1gMnPc2
a	a	k8xC
přes	přes	k7c4
20	#num#	k4
lidí	člověk	k1gMnPc2
utrpělo	utrpět	k5eAaPmAgNnS
zranění	zranění	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předtřesy	Předtřesa	k1gFnPc1
</s>
<s>
O	o	k7c4
den	den	k1gInSc4
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
28	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
oblasti	oblast	k1gFnSc6
poblíž	poblíž	k6eAd1
Petrinje	Petrinj	k1gInSc2
a	a	k8xC
Sisaku	Sisak	k1gInSc2
ke	k	k7c3
dvěma	dva	k4xCgMnPc3
silným	silný	k2eAgMnPc3d1
předtřesům	předtřes	k1gMnPc3
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
z	z	k7c2
nich	on	k3xPp3gFnPc2
dosáhl	dosáhnout	k5eAaPmAgMnS
magnituda	magnituda	k1gMnSc1
5,2	5,2	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
hodinu	hodina	k1gFnSc4
později	pozdě	k6eAd2
následoval	následovat	k5eAaImAgInS
druhý	druhý	k4xOgMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
dosáhl	dosáhnout	k5eAaPmAgMnS
magnituda	magnituda	k1gMnSc1
4,9	4,9	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
otřesy	otřes	k1gInPc4
způsobily	způsobit	k5eAaPmAgFnP
v	v	k7c6
blízkých	blízký	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
škody	škoda	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
spíše	spíše	k9
lehčího	lehký	k2eAgInSc2d2
charakteru	charakter	k1gInSc2
(	(	kIx(
<g/>
menší	malý	k2eAgFnPc1d2
praskliny	prasklina	k1gFnPc1
ve	v	k7c6
zdech	zeď	k1gFnPc6
<g/>
,	,	kIx,
poškození	poškození	k1gNnSc6
střech	střecha	k1gFnPc2
a	a	k8xC
komínů	komín	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nikdo	nikdo	k3yNnSc1
nebyl	být	k5eNaImAgMnS
zraněn	zranit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s>
Následně	následně	k6eAd1
se	se	k3xPyFc4
vyskytlo	vyskytnout	k5eAaPmAgNnS
několik	několik	k4yIc1
slabých	slabý	k2eAgInPc2d1
otřesů	otřes	k1gInPc2
o	o	k7c6
magnitudech	magnitud	k1gInPc6
1,5	1,5	k4
až	až	k9
2,5	2,5	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ty	k3xPp2nSc5
sice	sice	k8xC
byly	být	k5eAaImAgFnP
některými	některý	k3yIgMnPc7
obyvateli	obyvatel	k1gMnPc7
poblíž	poblíž	k7c2
epicentra	epicentrum	k1gNnSc2
pocítěny	pocítěn	k2eAgInPc4d1
<g/>
,	,	kIx,
ale	ale	k8xC
kvůli	kvůli	k7c3
své	svůj	k3xOyFgFnSc3
nízké	nízký	k2eAgFnSc3d1
síle	síla	k1gFnSc3
nezpůsobily	způsobit	k5eNaPmAgFnP
žádné	žádný	k3yNgFnPc1
škody	škoda	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1
otřes	otřes	k1gInSc1
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
v	v	k7c6
12	#num#	k4
<g/>
:	:	kIx,
<g/>
19	#num#	k4
SEČ	SEČ	kA
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
hlavnímu	hlavní	k2eAgNnSc3d1
zemětřesení	zemětřesení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
United	United	k1gMnSc1
States	Statesa	k1gFnPc2
Geological	Geological	k1gMnSc1
Survey	Survea	k1gFnSc2
(	(	kIx(
<g/>
USGS	USGS	kA
<g/>
)	)	kIx)
i	i	k8xC
Evropsko-středozemního	evropsko-středozemní	k2eAgNnSc2d1
seismologického	seismologický	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
(	(	kIx(
<g/>
EMSC	EMSC	kA
<g/>
)	)	kIx)
dosáhlo	dosáhnout	k5eAaPmAgNnS
zemětřesení	zemětřesení	k1gNnSc1
6,4	6,4	k4
Mw	Mw	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
nízké	nízký	k2eAgFnSc3d1
hloubce	hloubka	k1gFnSc3
(	(	kIx(
<g/>
5	#num#	k4
až	až	k9
10	#num#	k4
km	km	kA
<g/>
)	)	kIx)
způsobilo	způsobit	k5eAaPmAgNnS
velké	velký	k2eAgFnPc4d1
škody	škoda	k1gFnPc4
a	a	k8xC
bylo	být	k5eAaImAgNnS
pocítěno	pocítit	k5eAaPmNgNnS
i	i	k9
v	v	k7c6
poměrně	poměrně	k6eAd1
vzdálených	vzdálený	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
i	i	k9
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemětřesení	zemětřesení	k1gNnSc1
bylo	být	k5eAaImAgNnS
pocítěno	pocítit	k5eAaPmNgNnS
ve	v	k7c6
všech	všecek	k3xTgFnPc6
oblastech	oblast	k1gFnPc6
ČR	ČR	kA
<g/>
,	,	kIx,
zejména	zejména	k9
však	však	k9
na	na	k7c6
jihu	jih	k1gInSc6
<g/>
,	,	kIx,
blíže	blízce	k6eAd2
epicentru	epicentrum	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Geofyzikální	geofyzikální	k2eAgInSc1d1
ústav	ústav	k1gInSc1
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
vyzval	vyzvat	k5eAaPmAgMnS
obyvatele	obyvatel	k1gMnPc4
Česka	Česko	k1gNnSc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
zemětřesení	zemětřesení	k1gNnSc4
pocítili	pocítit	k5eAaPmAgMnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
vyplnili	vyplnit	k5eAaPmAgMnP
Makroseismický	makroseismický	k2eAgInSc4d1
dotazník	dotazník	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dotřesy	Dotřesa	k1gFnPc1
</s>
<s>
Vyskytlo	vyskytnout	k5eAaPmAgNnS
se	se	k3xPyFc4
několik	několik	k4yIc1
výrazných	výrazný	k2eAgInPc2d1
dotřesů	dotřes	k1gInPc2
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
se	se	k3xPyFc4
vyskytl	vyskytnout	k5eAaPmAgMnS
hodinu	hodina	k1gFnSc4
po	po	k7c6
hlavním	hlavní	k2eAgNnSc6d1
zemětřesení	zemětřesení	k1gNnSc6
a	a	k8xC
dosáhl	dosáhnout	k5eAaPmAgMnS
magnituda	magnituda	k1gMnSc1
4,4	4,4	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k1gNnPc1
zemětřesení	zemětřesení	k1gNnSc2
byla	být	k5eAaImAgNnP
zaznamenána	zaznamenat	k5eAaPmNgNnP
den	den	k1gInSc4
poté	poté	k6eAd1
30	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
o	o	k7c6
silách	síla	k1gFnPc6
4,8	4,8	k4
a	a	k8xC
4,7	4,7	k4
Richterovy	Richterův	k2eAgFnSc2d1
škály	škála	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Silné	silný	k2eAgInPc1d1
otřesy	otřes	k1gInPc1
kolem	kolem	k7c2
4,5	4,5	k4
Mw	Mw	k1gFnPc2
se	se	k3xPyFc4
odehrály	odehrát	k5eAaPmAgFnP
i	i	k9
v	v	k7c6
lednu	leden	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Otřesy	otřes	k1gInPc1
pravděpodobně	pravděpodobně	k6eAd1
probudily	probudit	k5eAaPmAgInP
seismickou	seismický	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
v	v	k7c6
Záhřebu	Záhřeb	k1gInSc6
a	a	k8xC
jejím	její	k3xOp3gNnSc6
okolí	okolí	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
vyskytlo	vyskytnout	k5eAaPmAgNnS
několik	několik	k4yIc1
pocítěných	pocítěný	k2eAgNnPc2d1
zemětřesení	zemětřesení	k1gNnPc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
nikoliv	nikoliv	k9
natolik	natolik	k6eAd1
silných	silný	k2eAgFnPc2d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
způsobila	způsobit	k5eAaPmAgFnS
škody	škoda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Seismicita	seismicita	k1gFnSc1
v	v	k7c6
regionu	region	k1gInSc6
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
patří	patřit	k5eAaImIp3nS
k	k	k7c3
zemím	zem	k1gFnPc3
jižní	jižní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
obecně	obecně	k6eAd1
často	často	k6eAd1
postihovány	postihován	k2eAgInPc4d1
silnějšími	silný	k2eAgInPc7d2
otřesy	otřes	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Chorvatsku	Chorvatsko	k1gNnSc6
samotném	samotný	k2eAgNnSc6d1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
silným	silný	k2eAgNnPc3d1
zemětřesením	zemětřesení	k1gNnPc3
v	v	k7c6
letech	let	k1gInPc6
1667	#num#	k4
(	(	kIx(
<g/>
~	~	kIx~
<g/>
7,0	7,0	k4
Mw	Mw	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1880	#num#	k4
(	(	kIx(
<g/>
6,3	6,3	k4
ML	ml	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1909	#num#	k4
(	(	kIx(
<g/>
5,7	5,7	k4
Mw	Mw	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1962	#num#	k4
(	(	kIx(
<g/>
6,1	6,1	k4
Mw	Mw	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
2020	#num#	k4
(	(	kIx(
<g/>
5,4	5,4	k4
Mw	Mw	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
reakce	reakce	k1gFnSc1
</s>
<s>
Albánie	Albánie	k1gFnSc1
Albánie	Albánie	k1gFnSc2
–	–	k?
Albánská	albánský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
poslala	poslat	k5eAaPmAgFnS
do	do	k7c2
Chorvatska	Chorvatsko	k1gNnSc2
na	na	k7c4
humanitární	humanitární	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
a	a	k8xC
rekonstrukci	rekonstrukce	k1gFnSc4
budov	budova	k1gFnPc2
250	#num#	k4
tisíc	tisíc	k4xCgInPc2
eur	euro	k1gNnPc2
(	(	kIx(
<g/>
~	~	kIx~
<g/>
6,5	6,5	k4
milionů	milion	k4xCgInPc2
Kč	Kč	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
–	–	k?
Ministr	ministr	k1gMnSc1
vnitra	vnitro	k1gNnSc2
Jan	Jan	k1gMnSc1
Hamáček	Hamáček	k1gMnSc1
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
do	do	k7c2
Chorvatska	Chorvatsko	k1gNnSc2
bude	být	k5eAaImBp3nS
z	z	k7c2
ČR	ČR	kA
posláno	poslán	k2eAgNnSc1d1
vybavení	vybavení	k1gNnSc1
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
pět	pět	k4xCc1
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
Severní	severní	k2eAgFnSc2d1
Makedonie	Makedonie	k1gFnSc2
–	–	k?
Zoran	Zoran	k1gMnSc1
Zaev	Zaev	k1gMnSc1
<g/>
,	,	kIx,
premiér	premiér	k1gMnSc1
Severní	severní	k2eAgFnSc2d1
Makedonie	Makedonie	k1gFnSc2
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
do	do	k7c2
Chorvatska	Chorvatsko	k1gNnSc2
posláno	poslat	k5eAaPmNgNnS
6	#num#	k4
milionů	milion	k4xCgInPc2
makedonských	makedonský	k2eAgInPc2d1
denárů	denár	k1gInPc2
(	(	kIx(
<g/>
~	~	kIx~
<g/>
2,6	2,6	k4
milionů	milion	k4xCgInPc2
Kč	Kč	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
Slovinsko	Slovinsko	k1gNnSc1
–	–	k?
Janez	Janez	k1gMnSc1
Janša	Janša	k1gMnSc1
<g/>
,	,	kIx,
premiér	premiér	k1gMnSc1
Slovinska	Slovinsko	k1gNnSc2
<g/>
,	,	kIx,
nabídl	nabídnout	k5eAaPmAgInS
zaslání	zaslání	k1gNnSc4
stanů	stan	k1gInPc2
a	a	k8xC
jiného	jiný	k2eAgNnSc2d1
vybavení	vybavení	k1gNnSc2
na	na	k7c4
pomoc	pomoc	k1gFnSc4
lidem	člověk	k1gMnPc3
zasažených	zasažený	k2eAgFnPc2d1
tímto	tento	k3xDgNnSc7
zemětřesením	zemětřesení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nabídl	nabídnout	k5eAaPmAgMnS
také	také	k9
<g/>
,	,	kIx,
že	že	k8xS
pošle	poslat	k5eAaPmIp3nS
speciální	speciální	k2eAgInSc4d1
tým	tým	k1gInSc4
na	na	k7c4
pomoc	pomoc	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
Srbsko	Srbsko	k1gNnSc1
–	–	k?
Aleksandar	Aleksandar	k1gMnSc1
Vučić	Vučić	k1gMnSc1
<g/>
,	,	kIx,
prezident	prezident	k1gMnSc1
Srbska	Srbsko	k1gNnSc2
<g/>
,	,	kIx,
požádal	požádat	k5eAaPmAgMnS
vládu	vláda	k1gFnSc4
své	svůj	k3xOyFgFnSc2
země	zem	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Chorvatsku	Chorvatsko	k1gNnSc6
převedla	převést	k5eAaPmAgFnS
1	#num#	k4
milion	milion	k4xCgInSc4
eur	euro	k1gNnPc2
(	(	kIx(
<g/>
~	~	kIx~
<g/>
26,4	26,4	k4
milionů	milion	k4xCgInPc2
Kč	Kč	kA
<g/>
)	)	kIx)
v	v	k7c6
hotovosti	hotovost	k1gFnSc6
i	i	k8xC
materiálních	materiální	k2eAgInPc6d1
darech	dar	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Poničené	poničený	k2eAgFnPc1d1
budovy	budova	k1gFnPc1
ve	v	k7c6
městě	město	k1gNnSc6
Petrinja	Petrinj	k1gInSc2
</s>
<s>
Kostel	kostel	k1gInSc1
ve	v	k7c6
vesnici	vesnice	k1gFnSc6
Žažina	Žažina	k1gMnSc1
byl	být	k5eAaImAgMnS
během	během	k7c2
zemětřesení	zemětřesení	k1gNnSc2
zcela	zcela	k6eAd1
zničen	zničen	k2eAgInSc1d1
</s>
<s>
Poničené	poničený	k2eAgFnPc1d1
budovy	budova	k1gFnPc1
ve	v	k7c6
městě	město	k1gNnSc6
Petrinja	Petrinj	k1gInSc2
</s>
<s>
Poničené	poničený	k2eAgFnPc1d1
budovy	budova	k1gFnPc1
ve	v	k7c6
městě	město	k1gNnSc6
Petrinja	Petrinj	k1gInSc2
</s>
<s>
Poničené	poničený	k2eAgFnPc1d1
budovy	budova	k1gFnPc1
ve	v	k7c6
městě	město	k1gNnSc6
Petrinja	Petrinj	k1gInSc2
</s>
<s>
Poničené	poničený	k2eAgFnPc1d1
budovy	budova	k1gFnPc1
ve	v	k7c6
městě	město	k1gNnSc6
Petrinja	Petrinj	k1gInSc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Magnitude	Magnitud	k1gMnSc5
6.3	6.3	k4
earthquake	earthquak	k1gMnSc2
strikes	strikes	k1gInSc4
Croatia	Croatium	k1gNnSc2
<g/>
;	;	kIx,
1	#num#	k4
death	death	k1gMnSc1
reported	reported	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
WHBQ	WHBQ	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Zemětřesení	zemětřesení	k1gNnSc1
v	v	k7c6
Chorvatsku	Chorvatsko	k1gNnSc6
<g/>
,	,	kIx,
citelné	citelný	k2eAgInPc1d1
i	i	k9
v	v	k7c6
ČR	ČR	kA
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
již	již	k6eAd1
sedm	sedm	k4xCc4
obětí	oběť	k1gFnPc2
|	|	kIx~
ČeskéNoviny	ČeskéNovina	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.ceskenoviny.cz	www.ceskenoviny.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Velmi	velmi	k6eAd1
silné	silný	k2eAgNnSc4d1
zemětřesení	zemětřesení	k1gNnSc4
v	v	k7c6
Chorvatsku	Chorvatsko	k1gNnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-12-29	2020-12-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Makroseismický	makroseismický	k2eAgInSc4d1
dotazník	dotazník	k1gInSc4
|	|	kIx~
Geofyzikální	geofyzikální	k2eAgInSc4d1
ústav	ústav	k1gInSc4
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
ČR	ČR	kA
<g/>
,	,	kIx,
v.v.i.	v.v.i.	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geofyzikální	geofyzikální	k2eAgInSc1d1
ústav	ústav	k1gInSc1
AVČR	AVČR	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Seizmičnost	Seizmičnost	k1gFnSc1
Hrvatske	Hrvatske	k1gFnSc1
-	-	kIx~
Geofizički	Geofizički	k1gNnSc1
odsjek	odsjka	k1gFnPc2
<g/>
.	.	kIx.
www.pmf.unizg.hr	www.pmf.unizg.hr	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Shqipëria	Shqipërium	k1gNnSc2
i	i	k8xC
jep	jep	k?
250	#num#	k4
mijë	mijë	k?
euro	euro	k1gNnSc4
ndihmë	ndihmë	k?
Kroacisë	Kroacisë	k1gMnSc1
pas	pas	k6eAd1
tërmetit	tërmetit	k5eAaPmF
tragjik	tragjik	k1gInSc4
<g/>
.	.	kIx.
euronews	euronews	k1gInSc4
<g/>
.	.	kIx.
<g/>
al	ala	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
albánsky	albánsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Chorvatsko	Chorvatsko	k1gNnSc1
zasáhlo	zasáhnout	k5eAaPmAgNnS
silné	silný	k2eAgNnSc1d1
zemětřesení	zemětřesení	k1gNnSc1
<g/>
,	,	kIx,
vyžádalo	vyžádat	k5eAaPmAgNnS
si	se	k3xPyFc3
několik	několik	k4yIc4
obětí	oběť	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česko	Česko	k1gNnSc1
pošle	poslat	k5eAaPmIp3nS
pomoc	pomoc	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-12-29	2020-12-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Jutarnji	Jutarnj	k1gFnSc3
list	list	k1gInSc1
-	-	kIx~
Cijela	Cijela	k1gFnSc1
regija	regijus	k1gMnSc2
uz	uz	k?
Hrvatsku	Hrvatska	k1gFnSc4
<g/>
,	,	kIx,
javili	javit	k5eAaImAgMnP,k5eAaBmAgMnP,k5eAaPmAgMnP
se	se	k3xPyFc4
Vučić	Vučić	k1gFnSc4
<g/>
,	,	kIx,
Đukanović	Đukanović	k1gMnSc1
<g/>
,	,	kIx,
Janša	Janša	k1gMnSc1
i	i	k8xC
Pahor	Pahor	k?
<g/>
;	;	kIx,
Zaev	Zaev	k1gMnSc1
<g/>
:	:	kIx,
‘	‘	k?
<g/>
Naša	Naša	k?
vlada	vlada	k1gFnSc1
šalje	šalje	k1gFnSc1
novac	novac	k1gFnSc1
<g/>
‘	‘	k?
<g/>
.	.	kIx.
www.jutarnji.hr	www.jutarnji.hr	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-12-29	2020-12-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
chorvatsky	chorvatsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Do	do	k7c2
Chorvatska	Chorvatsko	k1gNnSc2
proudí	proudit	k5eAaImIp3nP,k5eAaPmIp3nP
pomoc	pomoc	k1gFnSc4
i	i	k9
od	od	k7c2
úhlavních	úhlavní	k2eAgMnPc2d1
nepřátel	nepřítel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-01-05	2021-01-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Zpráva	zpráva	k1gFnSc1
Chorvatsko	Chorvatsko	k1gNnSc1
zasáhlo	zasáhnout	k5eAaPmAgNnS
silné	silný	k2eAgNnSc1d1
zemětřesení	zemětřesení	k1gNnSc1
o	o	k7c6
síle	síla	k1gFnSc6
6,4	6,4	k4
ve	v	k7c6
Wikizprávách	Wikizpráva	k1gFnPc6
</s>
