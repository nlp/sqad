<s>
Ninkó	Ninkó	k?
</s>
<s>
Ninkó	Ninkó	k?
Narození	narození	k1gNnSc1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1800	#num#	k4
<g/>
Kjóto	Kjóto	k1gNnSc4
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1846	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
45	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Kjóto	Kjót	k2eAgNnSc1d1
Místo	místo	k1gNnSc1
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Tsuki	Tsuki	k6eAd1
no	no	k9
wa	wa	k?
no	no	k9
misasagi	misasagi	k1gNnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
panovník	panovník	k1gMnSc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Naoko	naoko	k6eAd1
ÓgimačiCuneko	ÓgimačiCuneko	k1gNnSc1
HašimotoCunako	HašimotoCunako	k1gNnSc1
Takacukasa	Takacukasa	k1gFnSc1
Děti	dítě	k1gFnPc1
</s>
<s>
KómeiKacura-no-mija	KómeiKacura-no-mij	k2eAgFnSc1d1
Misahito-šinnóKacura-no-mija	Misahito-šinnóKacura-no-mija	k1gFnSc1
Sumiko-naišinnóKazu-no-mija	Sumiko-naišinnóKazu-no-mijus	k1gMnSc2
Čikako-naišinnó	Čikako-naišinnó	k1gMnSc2
Rodiče	rodič	k1gMnSc2
</s>
<s>
Kókaku	Kókak	k1gMnSc3
a	a	k8xC
Kadžúdži	Kadžúdž	k1gFnSc3
Tadako	Tadako	k1gNnSc1
Rod	rod	k1gInSc4
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1
císařská	císařský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Masuhito-šinnó	Masuhito-šinnó	k?
a	a	k8xC
Kacura-no-mija	Kacura-no-mij	k2eAgFnSc1d1
Takehito-šinnó	Takehito-šinnó	k1gFnSc1
(	(	kIx(
<g/>
sourozenci	sourozenec	k1gMnPc1
<g/>
)	)	kIx)
<g/>
Kuni	Kun	k1gMnPc1
Asahiko	Asahika	k1gFnSc5
(	(	kIx(
<g/>
adopce	adopce	k1gFnSc2
<g/>
)	)	kIx)
<g/>
Meidži	Meidž	k1gFnSc3
(	(	kIx(
<g/>
vnuk	vnuk	k1gMnSc1
<g/>
)	)	kIx)
Funkce	funkce	k1gFnSc1
</s>
<s>
japonský	japonský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
(	(	kIx(
<g/>
1817	#num#	k4
<g/>
–	–	k?
<g/>
1846	#num#	k4
<g/>
)	)	kIx)
Podpis	podpis	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ninkó	Ninkó	k1gMnSc1
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
<g/>
:	:	kIx,
仁	仁	k?
<g/>
,	,	kIx,
Ninkó-tennō	Ninkó-tennō	k1gFnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
16	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1800	#num#	k4
–	–	k?
21	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1846	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
120	#num#	k4
<g/>
.	.	kIx.
japonským	japonský	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládl	vládnout	k5eAaImAgMnS
od	od	k7c2
roku	rok	k1gInSc2
1817	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1846	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
osobní	osobní	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
bylo	být	k5eAaImAgNnS
Ajahito-šinnó	Ajahito-šinnó	k1gFnSc7
(	(	kIx(
<g/>
恵	恵	k?
<g/>
,	,	kIx,
Ajahito-šinnó	Ajahito-šinnó	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ninkó	Ninkó	k?
byl	být	k5eAaImAgMnS
čtvrtým	čtvrtý	k4xOgMnSc7
synem	syn	k1gMnSc7
císaře	císař	k1gMnSc2
Kókaku	Kókak	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
měl	mít	k5eAaImAgInS
celkem	celkem	k6eAd1
8	#num#	k4
synů	syn	k1gMnPc2
a	a	k8xC
10	#num#	k4
dcer	dcera	k1gFnPc2
<g/>
,	,	kIx,
16	#num#	k4
z	z	k7c2
nich	on	k3xPp3gMnPc2
od	od	k7c2
různých	různý	k2eAgFnPc2d1
(	(	kIx(
<g/>
celkem	celkem	k6eAd1
7	#num#	k4
<g/>
)	)	kIx)
konkubín	konkubína	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korunním	korunní	k2eAgMnSc7d1
princem	princ	k1gMnSc7
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
v	v	k7c6
roce	rok	k1gInSc6
1809	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládl	vládnout	k5eAaImAgMnS
od	od	k7c2
16	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1817	#num#	k4
do	do	k7c2
21	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1846	#num#	k4
</s>
<s>
Éry	éra	k1gFnPc1
za	za	k7c2
vlády	vláda	k1gFnSc2
císaře	císař	k1gMnSc2
Ninkó	Ninkó	k1gMnSc2
</s>
<s>
Bunka	Bunka	k1gFnSc1
(	(	kIx(
<g/>
文	文	k?
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1804	#num#	k4
-	-	kIx~
1818	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bunsei	Bunsei	k1gNnSc1
(	(	kIx(
<g/>
文	文	k?
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1818	#num#	k4
-	-	kIx~
1830	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tempó	Tempó	k?
(	(	kIx(
<g/>
天	天	k?
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1830	#num#	k4
-	-	kIx~
1844	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kóka	Kóka	k1gFnSc1
(	(	kIx(
<g/>
弘	弘	k?
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1844	#num#	k4
-	-	kIx~
1848	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Ninkó	Ninkó	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Japonští	japonský	k2eAgMnPc1d1
císaři	císař	k1gMnPc1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
Kókaku	Kókak	k1gInSc2
</s>
<s>
1817	#num#	k4
<g/>
–	–	k?
<g/>
1846	#num#	k4
Ninkó	Ninkó	k1gFnSc1
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
Kómei	Kómei	k1gNnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Japonští	japonský	k2eAgMnPc1d1
císařové	císař	k1gMnPc1
seznam	seznam	k1gInSc4
japonských	japonský	k2eAgMnPc2d1
císařů	císař	k1gMnPc2
Legendární	legendární	k2eAgFnSc2d1
</s>
<s>
Džómon	Džómon	k1gMnSc1
</s>
<s>
660	#num#	k4
ACN	ACN	kA
<g/>
–	–	k?
<g/>
291	#num#	k4
ACN	ACN	kA
</s>
<s>
Džimmu	Džimmat	k5eAaPmIp1nS
</s>
<s>
Suizei	Suizei	k6eAd1
</s>
<s>
Annei	Annei	k6eAd1
</s>
<s>
Itoku	Itoku	k6eAd1
</s>
<s>
Kóšó	Kóšó	k?
</s>
<s>
Kóan	Kóan	k1gMnSc1
</s>
<s>
Jajoi	Jajoi	k6eAd1
</s>
<s>
290	#num#	k4
ACN	ACN	kA
<g/>
–	–	k?
<g/>
269	#num#	k4
AD	ad	k7c4
</s>
<s>
Kórei	Kórei	k6eAd1
</s>
<s>
Kógen	Kógen	k1gInSc1
</s>
<s>
Kaika	Kaika	k6eAd1
</s>
<s>
Sudžin	Sudžin	k1gMnSc1
</s>
<s>
Suinin	Suinin	k1gInSc1
</s>
<s>
Keikó	Keikó	k?
</s>
<s>
Seimu	Seimat	k5eAaPmIp1nS
</s>
<s>
Čúai	Čúai	k6eAd1
</s>
<s>
Džingu	Džing	k1gInSc2
Kógó	Kógó	k1gFnSc7
<g/>
**	**	k?
</s>
<s>
Znak	znak	k1gInSc4
japonského	japonský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
Jamato	Jamat	k2eAgNnSc4d1
</s>
<s>
Kofun	Kofun	k1gMnSc1
</s>
<s>
269	#num#	k4
<g/>
–	–	k?
<g/>
539	#num#	k4
</s>
<s>
Ódžin	Ódžin	k1gMnSc1
</s>
<s>
Nintoku	Nintok	k1gInSc3
</s>
<s>
Ričú	Ričú	k?
</s>
<s>
Hanzei	Hanzei	k6eAd1
</s>
<s>
Ingjó	Ingjó	k?
</s>
<s>
Ankó	Ankó	k?
</s>
<s>
Júrjaku	Júrjak	k1gMnSc3
</s>
<s>
Seinei	Seinei	k6eAd1
</s>
<s>
Kenzó	Kenzó	k?
</s>
<s>
Ninken	Ninken	k1gInSc1
</s>
<s>
Burecu	Burecu	k6eAd1
</s>
<s>
Keitai	Keitai	k6eAd1
</s>
<s>
Ankan	Ankan	k1gMnSc1
</s>
<s>
Senka	Senka	k6eAd1
</s>
<s>
Asuka	Asuka	k6eAd1
</s>
<s>
539	#num#	k4
<g/>
–	–	k?
<g/>
710	#num#	k4
</s>
<s>
Kinmei	Kinmei	k6eAd1
</s>
<s>
Bidacu	Bidacu	k6eAd1
</s>
<s>
Jómei	Jómei	k6eAd1
</s>
<s>
Sušun	Sušun	k1gMnSc1
</s>
<s>
Suiko	Suiko	k1gNnSc1
<g/>
*	*	kIx~
</s>
<s>
Džomei	Džomei	k6eAd1
</s>
<s>
Kógjoku	Kógjok	k1gInSc3
<g/>
*	*	kIx~
</s>
<s>
Kótoku	Kótok	k1gInSc3
</s>
<s>
Saimei	Saimei	k1gNnSc1
<g/>
*	*	kIx~
</s>
<s>
Tendži	Tendzat	k5eAaPmIp1nS
</s>
<s>
Kóbun	Kóbun	k1gMnSc1
</s>
<s>
Temmu	Temmat	k5eAaPmIp1nS
</s>
<s>
Džitó	Džitó	k?
</s>
<s>
Monmu	Monmat	k5eAaPmIp1nS
<g/>
*	*	kIx~
</s>
<s>
Gemmei	Gemmei	k1gNnSc1
<g/>
*	*	kIx~
</s>
<s>
Nara	Nara	k6eAd1
</s>
<s>
710	#num#	k4
<g/>
–	–	k?
<g/>
794	#num#	k4
</s>
<s>
Gemmei	Gemmei	k1gNnSc1
<g/>
*	*	kIx~
</s>
<s>
Genšó	Genšó	k?
<g/>
*	*	kIx~
</s>
<s>
Šómu	Šómu	k6eAd1
</s>
<s>
Kóken	Kóken	k1gInSc1
<g/>
*	*	kIx~
</s>
<s>
Džunnin	Džunnin	k1gInSc1
</s>
<s>
Šótoku	Šótok	k1gInSc3
<g/>
*	*	kIx~
</s>
<s>
Kónin	Kónin	k1gMnSc1
</s>
<s>
Kammu	Kammat	k5eAaPmIp1nS
</s>
<s>
Heian	Heian	k1gMnSc1
</s>
<s>
794	#num#	k4
<g/>
–	–	k?
<g/>
1185	#num#	k4
</s>
<s>
Kammu	Kammat	k5eAaPmIp1nS
</s>
<s>
Heizei	Heizei	k6eAd1
</s>
<s>
Saga	Saga	k6eAd1
</s>
<s>
Džunna	Džunna	k6eAd1
</s>
<s>
Ninmjó	Ninmjó	k?
</s>
<s>
Montoku	Montok	k1gInSc3
</s>
<s>
Seiwa	Seiwa	k6eAd1
</s>
<s>
Józei	Józei	k6eAd1
</s>
<s>
Kókó	Kókó	k?
</s>
<s>
Uda	Uda	k?
</s>
<s>
Daigo	Daigo	k6eAd1
</s>
<s>
Suzaku	Suzak	k1gMnSc3
</s>
<s>
Murakami	Muraka	k1gFnPc7
</s>
<s>
Reizei	Reizei	k6eAd1
</s>
<s>
En	En	k?
<g/>
'	'	kIx"
<g/>
ju	ju	k0
</s>
<s>
Kazan	Kazan	k1gMnSc1
</s>
<s>
Ichidžó	Ichidžó	k?
</s>
<s>
Sandžó	Sandžó	k?
</s>
<s>
Go-Ichidžó	Go-Ichidžó	k?
</s>
<s>
Go-Suzaku	Go-Suzak	k1gMnSc3
</s>
<s>
Go-Reizei	Go-Reizei	k6eAd1
</s>
<s>
Go-Sandžó	Go-Sandžó	k?
</s>
<s>
Širakawa	Širakawa	k6eAd1
</s>
<s>
Horikawa	Horikawa	k6eAd1
</s>
<s>
Toba	Toba	k6eAd1
</s>
<s>
Sutoku	Sutok	k1gInSc3
</s>
<s>
Konoe	Konoe	k6eAd1
</s>
<s>
Go-Širakawa	Go-Širakawa	k6eAd1
</s>
<s>
Nidžó	Nidžó	k?
</s>
<s>
Rokudžó	Rokudžó	k?
</s>
<s>
Takakura	Takakura	k1gFnSc1
</s>
<s>
Antoku	Antok	k1gInSc3
</s>
<s>
Go-Toba	Go-Toba	k1gFnSc1
</s>
<s>
Kamakura	Kamakura	k1gFnSc1
</s>
<s>
1185	#num#	k4
<g/>
–	–	k?
<g/>
1333	#num#	k4
</s>
<s>
Go-Toba	Go-Toba	k1gFnSc1
</s>
<s>
Cučimikado	Cučimikada	k1gFnSc5
</s>
<s>
Džuntoku	Džuntok	k1gInSc3
</s>
<s>
Čúkjó	Čúkjó	k?
</s>
<s>
Go-Horikawa	Go-Horikawa	k6eAd1
</s>
<s>
Šidžó	Šidžó	k?
</s>
<s>
Go-Saga	Go-Saga	k1gFnSc1
</s>
<s>
Go-Fukakusa	Go-Fukakus	k1gMnSc4
</s>
<s>
Kamejama	Kamejama	k?
</s>
<s>
Go-Uda	Go-Uda	k1gFnSc1
</s>
<s>
Fušimi	Fuši	k1gFnPc7
</s>
<s>
Go-Fušimi	Go-Fuši	k1gFnPc7
</s>
<s>
Go-Nidžó	Go-Nidžó	k?
</s>
<s>
Hanazono	Hanazona	k1gFnSc5
</s>
<s>
Go-Daigo	Go-Daigo	k6eAd1
</s>
<s>
Severní	severní	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
</s>
<s>
1333	#num#	k4
<g/>
–	–	k?
<g/>
1392	#num#	k4
</s>
<s>
Kógon	Kógon	k1gMnSc1
</s>
<s>
Kómjó	Kómjó	k?
</s>
<s>
Sukó	Sukó	k?
</s>
<s>
Go-Kógon	Go-Kógon	k1gMnSc1
</s>
<s>
Go-En	Go-En	k1gNnSc1
<g/>
'	'	kIx"
<g/>
jú	jú	k0
</s>
<s>
Go-Komacu	Go-Komacu	k6eAd1
</s>
<s>
Muromači	Muromač	k1gMnPc1
</s>
<s>
1333	#num#	k4
<g/>
–	–	k?
<g/>
1573	#num#	k4
</s>
<s>
Go-Murakami	Go-Muraka	k1gFnPc7
</s>
<s>
Čókei	Čókei	k6eAd1
</s>
<s>
Go-Kamejama	Go-Kamejama	k1gFnSc1
</s>
<s>
Go-Komacu	Go-Komacu	k6eAd1
</s>
<s>
Šókó	Šókó	k?
</s>
<s>
Go-Hanazono	Go-Hanazona	k1gFnSc5
</s>
<s>
Go-Cučimikado	Go-Cučimikada	k1gFnSc5
</s>
<s>
Go-Kašiwabara	Go-Kašiwabara	k1gFnSc1
</s>
<s>
Go-Nara	Go-Nara	k1gFnSc1
</s>
<s>
Ógimači	Ógimač	k1gMnPc1
</s>
<s>
Azuči-Momojama	Azuči-Momojama	k1gFnSc1
</s>
<s>
1573	#num#	k4
<g/>
–	–	k?
<g/>
1603	#num#	k4
</s>
<s>
Ógimači	Ógimač	k1gMnPc1
</s>
<s>
Go-Józei	Go-Józei	k6eAd1
</s>
<s>
Edo	Eda	k1gMnSc5
</s>
<s>
1603	#num#	k4
<g/>
–	–	k?
<g/>
1868	#num#	k4
</s>
<s>
Go-Józei	Go-Józei	k6eAd1
</s>
<s>
Go-Mizunó	Go-Mizunó	k?
</s>
<s>
Meišó	Meišó	k?
<g/>
*	*	kIx~
</s>
<s>
Go-Kómjó	Go-Kómjó	k?
</s>
<s>
Go-Sai	Go-Sai	k6eAd1
</s>
<s>
Reigen	Reigen	k1gInSc1
</s>
<s>
Higašijama	Higašijama	k1gFnSc1
</s>
<s>
Nakamikado	Nakamikada	k1gFnSc5
</s>
<s>
Sakuramači	Sakuramač	k1gMnPc1
</s>
<s>
Momozono	Momozona	k1gFnSc5
</s>
<s>
Go-Sakuramači	Go-Sakuramač	k1gMnPc1
<g/>
*	*	kIx~
</s>
<s>
Go-Momozono	Go-Momozona	k1gFnSc5
</s>
<s>
Kókaku	Kókak	k1gMnSc3
</s>
<s>
Ninkó	Ninkó	k?
</s>
<s>
Kómei	Kómei	k6eAd1
</s>
<s>
Meidži	Meidzat	k5eAaPmIp1nS
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
</s>
<s>
1868	#num#	k4
<g/>
–	–	k?
<g/>
1947	#num#	k4
</s>
<s>
Meidži	Meidzat	k5eAaPmIp1nS
</s>
<s>
Taišó	Taišó	k?
</s>
<s>
Šówa	Šówa	k6eAd1
</s>
<s>
Moderní	moderní	k2eAgNnSc1d1
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
1947	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
</s>
<s>
Šówa	Šówa	k6eAd1
</s>
<s>
Akihito	Akihit	k2eAgNnSc1d1
</s>
<s>
Naruhito	Naruhit	k2eAgNnSc1d1
</s>
<s>
vládnoucí	vládnoucí	k2eAgFnPc1d1
císařovny	císařovna	k1gFnPc1
jsou	být	k5eAaImIp3nP
označeny	označit	k5eAaPmNgInP
hvězdičkou	hvězdička	k1gFnSc7
*	*	kIx~
/	/	kIx~
císařská	císařský	k2eAgFnSc1d1
choť	choť	k1gFnSc1
a	a	k8xC
regentka	regentka	k1gFnSc1
Džingu	Džing	k1gInSc2
Kógó	Kógó	k1gFnSc2
se	se	k3xPyFc4
tradičně	tradičně	k6eAd1
neuvádí	uvádět	k5eNaImIp3nS
**	**	k?
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
119205017	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
4699	#num#	k4
956X	956X	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2007077941	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
57419666	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2007077941	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Japonsko	Japonsko	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Novověk	novověk	k1gInSc1
</s>
