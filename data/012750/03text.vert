<p>
<s>
HammerFall	HammerFall	k1gInSc1	HammerFall
je	být	k5eAaImIp3nS	být
švédská	švédský	k2eAgFnSc1d1	švédská
heavymetalová	heavymetalový	k2eAgFnSc1d1	heavymetalová
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
ji	on	k3xPp3gFnSc4	on
Oscar	Oscar	k1gMnSc1	Oscar
Dronjak	Dronjak	k1gMnSc1	Dronjak
a	a	k8xC	a
Jesper	Jesper	k1gMnSc1	Jesper
Strömblad	Strömblad	k1gInSc1	Strömblad
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
zprvu	zprvu	k6eAd1	zprvu
představovala	představovat	k5eAaImAgFnS	představovat
typ	typ	k1gInSc4	typ
lokálních	lokální	k2eAgFnPc2d1	lokální
kapel	kapela	k1gFnPc2	kapela
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nemají	mít	k5eNaImIp3nP	mít
stálé	stálý	k2eAgNnSc4d1	stálé
obsazení	obsazení	k1gNnSc4	obsazení
a	a	k8xC	a
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
se	se	k3xPyFc4	se
členové	člen	k1gMnPc1	člen
střídají	střídat	k5eAaImIp3nP	střídat
jako	jako	k9	jako
na	na	k7c6	na
běžícím	běžící	k2eAgInSc6d1	běžící
pásu	pás	k1gInSc6	pás
<g/>
.	.	kIx.	.
</s>
<s>
Zlom	zlom	k1gInSc1	zlom
pro	pro	k7c4	pro
kapelu	kapela	k1gFnSc4	kapela
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
z	z	k7c2	z
osobních	osobní	k2eAgInPc2d1	osobní
důvodů	důvod	k1gInPc2	důvod
odešel	odejít	k5eAaPmAgMnS	odejít
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
zpěvák	zpěvák	k1gMnSc1	zpěvák
Mikael	Mikael	k1gMnSc1	Mikael
Stanne	Stann	k1gInSc5	Stann
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
zabral	zabrat	k5eAaPmAgMnS	zabrat
Joacim	Joacim	k1gMnSc1	Joacim
Cans	Cans	k1gInSc4	Cans
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sestava	sestava	k1gFnSc1	sestava
==	==	k?	==
</s>
</p>
<p>
<s>
Joacim	Joacim	k6eAd1	Joacim
Cans	Cans	k1gInSc1	Cans
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oscar	Oscar	k1gMnSc1	Oscar
Dronjak	Dronjak	k1gMnSc1	Dronjak
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pontus	Pontus	k1gInSc1	Pontus
Norgren	Norgrna	k1gFnPc2	Norgrna
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fredrik	Fredrik	k1gMnSc1	Fredrik
Larsson	Larsson	k1gMnSc1	Larsson
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Wallin	Wallina	k1gFnPc2	Wallina
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc1	současnost
<g/>
)	)	kIx)	)
<g/>
Bývalí	bývalý	k2eAgMnPc1d1	bývalý
členovéJohann	členovéJohann	k1gMnSc1	členovéJohann
Larsson	Larsson	k1gMnSc1	Larsson
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Niklas	Niklas	k1gInSc1	Niklas
Sundin	Sundin	k2eAgInSc1d1	Sundin
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mikael	Mikaelit	k5eAaPmRp2nS	Mikaelit
Stanne	Stann	k1gMnSc5	Stann
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jesper	Jesper	k1gInSc1	Jesper
Strömblad	Strömblad	k1gInSc1	Strömblad
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Glenn	Glenn	k1gMnSc1	Glenn
Ljungström	Ljungström	k1gMnSc1	Ljungström
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Patrik	Patrik	k1gMnSc1	Patrik
Räfling	Räfling	k1gInSc1	Räfling
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Magnus	Magnus	k1gInSc1	Magnus
Rosén	Rosén	k1gInSc1	Rosén
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stefan	Stefan	k1gMnSc1	Stefan
Elmgren	Elmgrna	k1gFnPc2	Elmgrna
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Anders	Anders	k6eAd1	Anders
Johansson	Johansson	k1gMnSc1	Johansson
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Johan	Johan	k1gMnSc1	Johan
Koleberg	Koleberg	k1gMnSc1	Koleberg
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
–	–	k?	–
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
Glory	Glor	k1gMnPc4	Glor
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Brave	brav	k1gInSc5	brav
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Legacy	Legacy	k1gInPc1	Legacy
of	of	k?	of
Kings	Kings	k1gInSc1	Kings
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Renegade	Renegást	k5eAaPmIp3nS	Renegást
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Crimson	Crimson	k1gMnSc1	Crimson
Thunder	Thunder	k1gMnSc1	Thunder
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Chapter	Chapter	k1gMnSc1	Chapter
V	V	kA	V
<g/>
:	:	kIx,	:
Unbent	Unbent	k1gMnSc1	Unbent
<g/>
,	,	kIx,	,
Unbowed	Unbowed	k1gMnSc1	Unbowed
<g/>
,	,	kIx,	,
Unbroken	Unbroken	k2eAgMnSc1d1	Unbroken
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Threshold	Threshold	k1gInSc1	Threshold
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
No	no	k9	no
Sacrifice	Sacrifice	k1gFnSc1	Sacrifice
<g/>
,	,	kIx,	,
No	no	k9	no
Victory	Victor	k1gMnPc4	Victor
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Infected	Infected	k1gInSc1	Infected
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
r	r	kA	r
<g/>
)	)	kIx)	)
<g/>
Evolution	Evolution	k1gInSc1	Evolution
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Built	Built	k1gInSc1	Built
to	ten	k3xDgNnSc1	ten
Last	Last	k2eAgMnSc1d1	Last
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dominion	dominion	k1gNnSc1	dominion
(	(	kIx(	(
<g/>
2019	[number]	k4	2019
<g/>
)	)	kIx)	)
<g/>
Koncertní	koncertní	k2eAgMnSc1d1	koncertní
albaOne	albaOnout	k5eAaPmIp3nS	albaOnout
Crimson	Crimson	k1gMnSc1	Crimson
Night	Night	k1gMnSc1	Night
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gates	Gates	k1gMnSc1	Gates
of	of	k?	of
Dalhalla	Dalhalla	k1gMnSc1	Dalhalla
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
DVDThe	DVDTh	k1gInSc2	DVDTh
First	First	k1gInSc1	First
Crusade	Crusad	k1gInSc5	Crusad
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Templar	Templar	k1gInSc1	Templar
Renegade	Renegad	k1gInSc5	Renegad
Crusades	Crusades	k1gMnSc1	Crusades
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hearts	Hearts	k6eAd1	Hearts
on	on	k3xPp3gInSc1	on
Fire	Fire	k1gInSc1	Fire
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
One	One	k?	One
Crimson	Crimson	k1gMnSc1	Crimson
Night	Night	k1gMnSc1	Night
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rebels	Rebels	k6eAd1	Rebels
with	with	k1gInSc1	with
a	a	k8xC	a
Cause	causa	k1gFnSc3	causa
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gates	Gates	k1gMnSc1	Gates
of	of	k?	of
Dalhalla	Dalhalla	k1gMnSc1	Dalhalla
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
KompilaceMetallic	KompilaceMetallice	k1gFnPc2	KompilaceMetallice
Emotions	Emotions	k1gInSc1	Emotions
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Steel	Steel	k1gInSc1	Steel
Meets	Meets	k1gInSc1	Meets
Steel	Steel	k1gInSc1	Steel
-	-	kIx~	-
Ten	ten	k3xDgInSc1	ten
Years	Years	k1gInSc1	Years
Of	Of	k1gFnSc2	Of
Glory	Glora	k1gFnSc2	Glora
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Best	Best	k1gMnSc1	Best
Of	Of	k1gMnSc1	Of
<g/>
"	"	kIx"	"
kompilace	kompilace	k1gFnSc1	kompilace
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Masterpieces	Masterpieces	k1gInSc1	Masterpieces
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
HammerFall	HammerFalla	k1gFnPc2	HammerFalla
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
kapely	kapela	k1gFnSc2	kapela
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
České	český	k2eAgFnPc1d1	Česká
fan	fana	k1gFnPc2	fana
stránky	stránka	k1gFnPc4	stránka
</s>
</p>
<p>
<s>
HammerFall	HammerFall	k1gInSc1	HammerFall
na	na	k7c6	na
facebooku	facebook	k1gInSc6	facebook
</s>
</p>
