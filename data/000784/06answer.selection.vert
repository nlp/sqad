<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
94321	[number]	k4	94321
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
Indiana	Indiana	k1gFnSc1	Indiana
38	[number]	k4	38
<g/>
.	.	kIx.	.
největším	veliký	k2eAgInSc7d3	veliký
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
6,6	[number]	k4	6,6
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
šestnáctým	šestnáctý	k4xOgInSc7	šestnáctý
nejlidnatějším	lidnatý	k2eAgInSc7d3	nejlidnatější
státem	stát	k1gInSc7	stát
a	a	k8xC	a
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
hustoty	hustota	k1gFnSc2	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
70	[number]	k4	70
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c6	na
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
na	na	k7c6	na
šestnáctém	šestnáctý	k4xOgInSc6	šestnáctý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
