<s>
Indiana	Indiana	k1gFnSc1	Indiana
(	(	kIx(	(
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
ɪ	ɪ	k?	ɪ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
State	status	k1gInSc5	status
of	of	k?	of
Indiana	Indiana	k1gFnSc1	Indiana
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc4	stát
nacházející	nacházející	k2eAgInSc4d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
východních	východní	k2eAgInPc2d1	východní
severních	severní	k2eAgInPc2d1	severní
států	stát	k1gInPc2	stát
ve	v	k7c6	v
středozápadním	středozápadní	k2eAgInSc6d1	středozápadní
regionu	region	k1gInSc6	region
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Indiana	Indiana	k1gFnSc1	Indiana
hraničí	hraničit	k5eAaImIp3nS	hraničit
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Michiganem	Michigan	k1gInSc7	Michigan
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Ohiem	Ohio	k1gNnSc7	Ohio
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Kentucky	Kentuck	k1gInPc7	Kentuck
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Illinois	Illinois	k1gFnSc7	Illinois
<g/>
.	.	kIx.	.
</s>
<s>
Severozápadní	severozápadní	k2eAgNnSc1d1	severozápadní
ohraničení	ohraničení	k1gNnSc1	ohraničení
tvoří	tvořit	k5eAaImIp3nS	tvořit
Michiganské	michiganský	k2eAgNnSc1d1	Michiganské
jezero	jezero	k1gNnSc1	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
94321	[number]	k4	94321
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
Indiana	Indiana	k1gFnSc1	Indiana
38	[number]	k4	38
<g/>
.	.	kIx.	.
největším	veliký	k2eAgInSc7d3	veliký
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
6,6	[number]	k4	6,6
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
šestnáctým	šestnáctý	k4xOgInSc7	šestnáctý
nejlidnatějším	lidnatý	k2eAgInSc7d3	nejlidnatější
státem	stát	k1gInSc7	stát
a	a	k8xC	a
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
hustoty	hustota	k1gFnSc2	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
70	[number]	k4	70
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c6	na
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
na	na	k7c6	na
šestnáctém	šestnáctý	k4xOgInSc6	šestnáctý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
a	a	k8xC	a
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Indianapolis	Indianapolis	k1gInSc1	Indianapolis
s	s	k7c7	s
850	[number]	k4	850
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
největšími	veliký	k2eAgMnPc7d3	veliký
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
Fort	Fort	k?	Fort
Wayne	Wayn	k1gInSc5	Wayn
(	(	kIx(	(
<g/>
250	[number]	k4	250
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Evansville	Evansville	k1gFnSc1	Evansville
(	(	kIx(	(
<g/>
120	[number]	k4	120
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
South	South	k1gMnSc1	South
Bend	Bend	k1gMnSc1	Bend
(	(	kIx(	(
<g/>
100	[number]	k4	100
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Indianě	Indiana	k1gFnSc3	Indiana
patří	patřit	k5eAaImIp3nS	patřit
72	[number]	k4	72
km	km	kA	km
pobřeží	pobřeží	k1gNnSc4	pobřeží
Michiganského	michiganský	k2eAgNnSc2d1	Michiganské
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc1	vrchol
Hoosier	Hoosier	k1gMnSc1	Hoosier
Hill	Hill	k1gMnSc1	Hill
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
383	[number]	k4	383
m	m	kA	m
ve	v	k7c6	v
středovýchodě	středovýchoda	k1gFnSc6	středovýchoda
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
toky	tok	k1gInPc7	tok
jsou	být	k5eAaImIp3nP	být
řeky	řeka	k1gFnSc2	řeka
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgFnSc4d1	tvořící
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Kentucky	Kentuck	k1gInPc7	Kentuck
<g/>
,	,	kIx,	,
Wabash	Wabash	k1gInSc1	Wabash
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
část	část	k1gFnSc4	část
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
Illinois	Illinois	k1gFnSc7	Illinois
<g/>
,	,	kIx,	,
a	a	k8xC	a
White	Whit	k1gInSc5	Whit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Indiany	Indiana	k1gFnSc2	Indiana
dorazili	dorazit	k5eAaPmAgMnP	dorazit
první	první	k4xOgMnPc1	první
Evropané	Evropan	k1gMnPc1	Evropan
<g/>
,	,	kIx,	,
francouzští	francouzský	k2eAgMnPc1d1	francouzský
průzkumníci	průzkumník	k1gMnPc1	průzkumník
<g/>
,	,	kIx,	,
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
oblast	oblast	k1gFnSc1	oblast
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Nové	Nové	k2eAgFnSc1d1	Nové
Francie	Francie	k1gFnSc1	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
se	se	k3xPyFc4	se
v	v	k7c6	v
regionu	region	k1gInSc6	region
objevili	objevit	k5eAaPmAgMnP	objevit
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
skupinami	skupina	k1gFnPc7	skupina
kolonistů	kolonista	k1gMnPc2	kolonista
probíhaly	probíhat	k5eAaImAgInP	probíhat
boje	boj	k1gInPc1	boj
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
region	region	k1gInSc1	region
nakonec	nakonec	k6eAd1	nakonec
coby	coby	k?	coby
výsledek	výsledek	k1gInSc4	výsledek
sedmileté	sedmiletý	k2eAgFnSc2d1	sedmiletá
války	válka	k1gFnSc2	válka
připadl	připadnout	k5eAaPmAgInS	připadnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1763	[number]	k4	1763
britské	britský	k2eAgFnSc6d1	britská
koruně	koruna	k1gFnSc6	koruna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
oblast	oblast	k1gFnSc1	oblast
západně	západně	k6eAd1	západně
od	od	k7c2	od
Appalačského	Appalačský	k2eAgNnSc2d1	Appalačské
pohoří	pohoří	k1gNnSc2	pohoří
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
za	za	k7c4	za
území	území	k1gNnSc4	území
indiánů	indián	k1gMnPc2	indián
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
název	název	k1gInSc1	název
Indiana	Indiana	k1gFnSc1	Indiana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
nově	nově	k6eAd1	nově
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jej	on	k3xPp3gInSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1787	[number]	k4	1787
začlenily	začlenit	k5eAaPmAgFnP	začlenit
do	do	k7c2	do
právě	právě	k6eAd1	právě
zřízeného	zřízený	k2eAgNnSc2d1	zřízené
Severozápadního	severozápadní	k2eAgNnSc2d1	severozápadní
teritoria	teritorium	k1gNnSc2	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyčlenění	vyčlenění	k1gNnSc6	vyčlenění
Ohia	Ohio	k1gNnSc2	Ohio
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
indianské	indianský	k2eAgNnSc4d1	indianský
teritorium	teritorium	k1gNnSc4	teritorium
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
dále	daleko	k6eAd2	daleko
zmenšováno	zmenšován	k2eAgNnSc1d1	zmenšováno
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
zbytek	zbytek	k1gInSc1	zbytek
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
stal	stát	k5eAaPmAgMnS	stát
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Indiana	Indiana	k1gFnSc1	Indiana
jako	jako	k9	jako
19	[number]	k4	19
<g/>
.	.	kIx.	.
stát	stát	k1gInSc1	stát
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
ratifikovala	ratifikovat	k5eAaBmAgFnS	ratifikovat
Ústavu	ústava	k1gFnSc4	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
došlo	dojít	k5eAaPmAgNnS	dojít
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1816	[number]	k4	1816
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
severu	sever	k1gInSc2	sever
ji	on	k3xPp3gFnSc4	on
ohraničují	ohraničovat	k5eAaImIp3nP	ohraničovat
jezero	jezero	k1gNnSc1	jezero
a	a	k8xC	a
stát	stát	k1gInSc1	stát
Michigan	Michigan	k1gInSc1	Michigan
<g/>
,	,	kIx,	,
z	z	k7c2	z
východu	východ	k1gInSc2	východ
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
,	,	kIx,	,
z	z	k7c2	z
jihovýchodu	jihovýchod	k1gInSc2	jihovýchod
a	a	k8xC	a
jihu	jih	k1gInSc2	jih
Kentucky	Kentucka	k1gFnSc2	Kentucka
a	a	k8xC	a
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
Illinois	Illinois	k1gFnSc2	Illinois
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnPc3d1	hlavní
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Indianapolis	Indianapolis	k1gFnSc1	Indianapolis
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
odhadovalo	odhadovat	k5eAaImAgNnS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgMnPc1	první
indiánští	indiánský	k2eAgMnPc1d1	indiánský
obyvatelé	obyvatel	k1gMnPc1	obyvatel
osídlili	osídlit	k5eAaPmAgMnP	osídlit
území	území	k1gNnSc4	území
Indiany	Indiana	k1gFnSc2	Indiana
někdy	někdy	k6eAd1	někdy
8	[number]	k4	8
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
poslední	poslední	k2eAgInPc1d1	poslední
archeologické	archeologický	k2eAgInPc1d1	archeologický
výzkumy	výzkum	k1gInPc1	výzkum
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
patrně	patrně	k6eAd1	patrně
stalo	stát	k5eAaPmAgNnS	stát
už	už	k6eAd1	už
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
tisíce	tisíc	k4xCgInPc4	tisíc
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
900	[number]	k4	900
zde	zde	k6eAd1	zde
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
poměrně	poměrně	k6eAd1	poměrně
vyspělou	vyspělý	k2eAgFnSc4d1	vyspělá
civilizaci	civilizace	k1gFnSc4	civilizace
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
byla	být	k5eAaImAgFnS	být
až	až	k9	až
30	[number]	k4	30
tisícová	tisícový	k2eAgNnPc4d1	tisícové
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kultura	kultura	k1gFnSc1	kultura
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
z	z	k7c2	z
neznámých	známý	k2eNgFnPc2d1	neznámá
příčin	příčina	k1gFnPc2	příčina
o	o	k7c4	o
550	[number]	k4	550
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgMnPc7	první
Evropany	Evropan	k1gMnPc7	Evropan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
na	na	k7c6	na
území	území	k1gNnSc6	území
Indiany	Indiana	k1gFnSc2	Indiana
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
za	za	k7c4	za
území	území	k1gNnSc4	území
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
posléze	posléze	k6eAd1	posléze
ztratila	ztratit	k5eAaPmAgFnS	ztratit
ve	v	k7c6	v
francouzsko-indiánské	francouzskondiánský	k2eAgFnSc6d1	francouzsko-indiánská
válce	válka	k1gFnSc6	válka
se	s	k7c7	s
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
však	však	k9	však
toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
zrodily	zrodit	k5eAaPmAgInP	zrodit
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1800	[number]	k4	1800
bylo	být	k5eAaImAgNnS	být
zřízeno	zřízen	k2eAgNnSc1d1	zřízeno
teritorium	teritorium	k1gNnSc1	teritorium
Indiana	Indiana	k1gFnSc1	Indiana
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1816	[number]	k4	1816
stal	stát	k5eAaPmAgInS	stát
19	[number]	k4	19
<g/>
.	.	kIx.	.
stát	stát	k1gInSc1	stát
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Indiany	Indiana	k1gFnSc2	Indiana
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
Hoosiers	Hoosiers	k1gInSc4	Hoosiers
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
6	[number]	k4	6
483	[number]	k4	483
802	[number]	k4	802
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
84,3	[number]	k4	84,3
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
9,1	[number]	k4	9,1
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,3	[number]	k4	0,3
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
1,6	[number]	k4	1,6
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,0	[number]	k4	0,0
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
2,7	[number]	k4	2,7
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
2,0	[number]	k4	2,0
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
6,0	[number]	k4	6,0
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Převážná	převážný	k2eAgFnSc1d1	převážná
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
k	k	k7c3	k
protestantům	protestant	k1gMnPc3	protestant
<g/>
,	,	kIx,	,
značný	značný	k2eAgInSc1d1	značný
vliv	vliv	k1gInSc1	vliv
však	však	k9	však
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
přítomnost	přítomnost	k1gFnSc1	přítomnost
je	být	k5eAaImIp3nS	být
zvýrazněna	zvýrazněn	k2eAgFnSc1d1	zvýrazněna
i	i	k8xC	i
existencí	existence	k1gFnSc7	existence
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
katolických	katolický	k2eAgFnPc2d1	katolická
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
University	universita	k1gFnPc1	universita
of	of	k?	of
Notre	Notr	k1gMnSc5	Notr
Dame	Damus	k1gMnSc5	Damus
ve	v	k7c6	v
městě	město	k1gNnSc6	město
South	South	k1gMnSc1	South
Bend	Bend	k1gMnSc1	Bend
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
městech	město	k1gNnPc6	město
dominují	dominovat	k5eAaImIp3nP	dominovat
největší	veliký	k2eAgInPc1d3	veliký
protestantské	protestantský	k2eAgInPc1d1	protestantský
proudy	proud	k1gInPc1	proud
a	a	k8xC	a
katolíci	katolík	k1gMnPc1	katolík
<g/>
,	,	kIx,	,
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
mají	mít	k5eAaImIp3nP	mít
silnou	silný	k2eAgFnSc4d1	silná
pozici	pozice	k1gFnSc4	pozice
evangelikálové	evangelikálový	k2eAgInPc4d1	evangelikálový
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
(	(	kIx(	(
<g/>
ultra	ultra	k2eAgInPc4d1	ultra
<g/>
)	)	kIx)	)
<g/>
konzervativní	konzervativní	k2eAgInPc4d1	konzervativní
protestantské	protestantský	k2eAgInPc4d1	protestantský
směry	směr	k1gInPc4	směr
<g/>
.	.	kIx.	.
</s>
<s>
Křesťané	křesťan	k1gMnPc1	křesťan
–	–	k?	–
82	[number]	k4	82
%	%	kIx~	%
protestanti	protestant	k1gMnPc1	protestant
–	–	k?	–
62	[number]	k4	62
%	%	kIx~	%
baptisté	baptista	k1gMnPc1	baptista
–	–	k?	–
15	[number]	k4	15
%	%	kIx~	%
metodisté	metodista	k1gMnPc1	metodista
–	–	k?	–
10	[number]	k4	10
%	%	kIx~	%
luteráni	luterán	k1gMnPc1	luterán
-	-	kIx~	-
6	[number]	k4	6
%	%	kIx~	%
letniční	letniční	k2eAgFnSc2d1	letniční
církve	církev	k1gFnSc2	církev
–	–	k?	–
3	[number]	k4	3
%	%	kIx~	%
menonité	menonitý	k2eAgFnSc2d1	menonitý
-	-	kIx~	-
1	[number]	k4	1
%	%	kIx~	%
jiní	jiný	k2eAgMnPc1d1	jiný
protestanti	protestant	k1gMnPc1	protestant
-	-	kIx~	-
23	[number]	k4	23
%	%	kIx~	%
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
–	–	k?	–
19	[number]	k4	19
%	%	kIx~	%
jiní	jiný	k2eAgMnPc1d1	jiný
křesťané	křesťan	k1gMnPc1	křesťan
–	–	k?	–
1	[number]	k4	1
%	%	kIx~	%
jiná	jiný	k2eAgNnPc1d1	jiné
náboženství	náboženství	k1gNnPc1	náboženství
–	–	k?	–
<	<	kIx(	<
<g/>
1	[number]	k4	1
%	%	kIx~	%
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
–	–	k?	–
17	[number]	k4	17
%	%	kIx~	%
Mottem	motto	k1gNnSc7	motto
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Crossroads	Crossroadsa	k1gFnPc2	Crossroadsa
of	of	k?	of
America	America	k1gMnSc1	America
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
květinou	květina	k1gFnSc7	květina
pivoňka	pivoňka	k1gFnSc1	pivoňka
<g/>
,	,	kIx,	,
stromem	strom	k1gInSc7	strom
liliovník	liliovník	k1gInSc1	liliovník
tulipánokvětý	tulipánokvětý	k2eAgInSc1d1	tulipánokvětý
<g/>
,	,	kIx,	,
ptákem	pták	k1gMnSc7	pták
kardinál	kardinál	k1gMnSc1	kardinál
a	a	k8xC	a
písní	píseň	k1gFnPc2	píseň
On	on	k3xPp3gInSc1	on
the	the	k?	the
Banks	Banks	k1gInSc1	Banks
of	of	k?	of
the	the	k?	the
Wabash	Wabash	k1gInSc1	Wabash
<g/>
.	.	kIx.	.
</s>
