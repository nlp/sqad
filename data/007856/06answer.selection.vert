<s>
Čeština	čeština	k1gFnSc1	čeština
má	mít	k5eAaImIp3nS	mít
7	[number]	k4	7
pádů	pád	k1gInPc2	pád
(	(	kIx(	(
<g/>
nominativ	nominativ	k1gInSc1	nominativ
<g/>
,	,	kIx,	,
genitiv	genitiv	k1gInSc1	genitiv
<g/>
,	,	kIx,	,
dativ	dativ	k1gInSc1	dativ
<g/>
,	,	kIx,	,
akuzativ	akuzativ	k1gInSc1	akuzativ
<g/>
,	,	kIx,	,
vokativ	vokativ	k1gInSc1	vokativ
<g/>
,	,	kIx,	,
lokál	lokál	k1gInSc1	lokál
<g/>
,	,	kIx,	,
instrumentál	instrumentál	k1gInSc1	instrumentál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
při	při	k7c6	při
skloňování	skloňování	k1gNnSc6	skloňování
podstatných	podstatný	k2eAgFnPc2d1	podstatná
a	a	k8xC	a
přídavných	přídavný	k2eAgFnPc2d1	přídavná
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
zájmen	zájmeno	k1gNnPc2	zájmeno
a	a	k8xC	a
číslovek	číslovka	k1gFnPc2	číslovka
<g/>
.	.	kIx.	.
</s>
