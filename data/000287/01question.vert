<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
prkno	prkno	k1gNnSc1	prkno
pevně	pevně	k6eAd1	pevně
připnuté	připnutý	k2eAgNnSc1d1	připnuté
k	k	k7c3	k
nohám	noha	k1gFnPc3	noha
jezdce	jezdec	k1gInSc2	jezdec
<g/>
,	,	kIx,	,
určené	určený	k2eAgInPc1d1	určený
k	k	k7c3	k
sportovnímu	sportovní	k2eAgNnSc3d1	sportovní
využití	využití	k1gNnSc3	využití
na	na	k7c6	na
sněhu	sníh	k1gInSc6	sníh
<g/>
?	?	kIx.	?
</s>
