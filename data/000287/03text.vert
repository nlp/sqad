<s>
Snowboard	snowboard	k1gInSc1	snowboard
je	být	k5eAaImIp3nS	být
prkno	prkno	k1gNnSc1	prkno
pevně	pevně	k6eAd1	pevně
připnuté	připnutý	k2eAgNnSc1d1	připnuté
k	k	k7c3	k
nohám	noha	k1gFnPc3	noha
jezdce	jezdec	k1gInSc2	jezdec
<g/>
,	,	kIx,	,
určené	určený	k2eAgInPc1d1	určený
k	k	k7c3	k
sportovnímu	sportovní	k2eAgNnSc3d1	sportovní
využití	využití	k1gNnSc3	využití
na	na	k7c6	na
sněhu	sníh	k1gInSc6	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhé	k2eAgNnSc1d1	Dlouhé
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
140-170	[number]	k4	140-170
cm	cm	kA	cm
(	(	kIx(	(
<g/>
mělo	mít	k5eAaImAgNnS	mít
by	by	kYmCp3nP	by
jezdci	jezdec	k1gMnPc1	jezdec
sahat	sahat	k5eAaImF	sahat
k	k	k7c3	k
bradě	brada	k1gFnSc3	brada
<g/>
)	)	kIx)	)
a	a	k8xC	a
široké	široký	k2eAgNnSc4d1	široké
cca	cca	kA	cca
25	[number]	k4	25
cm	cm	kA	cm
(	(	kIx(	(
<g/>
dostatečně	dostatečně	k6eAd1	dostatečně
aby	aby	kYmCp3nS	aby
přes	přes	k7c4	přes
hrany	hran	k1gInPc4	hran
"	"	kIx"	"
<g/>
nepřesahovaly	přesahovat	k5eNaImAgFnP	přesahovat
<g/>
"	"	kIx"	"
špičky	špička	k1gFnPc4	špička
a	a	k8xC	a
paty	pata	k1gFnPc4	pata
bot	bota	k1gFnPc2	bota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Snowboardy	snowboard	k1gInPc1	snowboard
jsou	být	k5eAaImIp3nP	být
konstruovány	konstruován	k2eAgInPc1d1	konstruován
buď	buď	k8xC	buď
s	s	k7c7	s
dřevěným	dřevěný	k2eAgNnSc7d1	dřevěné
nebo	nebo	k8xC	nebo
pěnovým	pěnový	k2eAgNnSc7d1	pěnové
jádrem	jádro	k1gNnSc7	jádro
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dřevo	dřevo	k1gNnSc1	dřevo
je	být	k5eAaImIp3nS	být
dražší	drahý	k2eAgMnSc1d2	dražší
a	a	k8xC	a
kvalitnější	kvalitní	k2eAgMnSc1d2	kvalitnější
<g/>
.	.	kIx.	.
</s>
<s>
Snowboard	snowboard	k1gInSc1	snowboard
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
sněhová	sněhový	k2eAgFnSc1d1	sněhová
verze	verze	k1gFnSc1	verze
<g/>
"	"	kIx"	"
skateboardu	skateboard	k1gInSc2	skateboard
či	či	k8xC	či
vodního	vodní	k2eAgInSc2d1	vodní
surfu	surf	k1gInSc2	surf
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
rozdílem	rozdíl	k1gInSc7	rozdíl
<g/>
,	,	kIx,	,
že	že	k8xS	že
jezdcovy	jezdcův	k2eAgFnPc4d1	jezdcova
nohy	noha	k1gFnPc4	noha
jsou	být	k5eAaImIp3nP	být
vázáním	vázání	k1gNnSc7	vázání
pevně	pevně	k6eAd1	pevně
fixovány	fixovat	k5eAaImNgInP	fixovat
k	k	k7c3	k
prknu	prkno	k1gNnSc3	prkno
<g/>
.	.	kIx.	.
</s>
<s>
Systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
kterými	který	k3yIgNnPc7	který
je	být	k5eAaImIp3nS	být
jezdec	jezdec	k1gMnSc1	jezdec
upnut	upnout	k5eAaPmNgMnS	upnout
k	k	k7c3	k
prknu	prkno	k1gNnSc3	prkno
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
různé	různý	k2eAgInPc1d1	různý
<g/>
,	,	kIx,	,
principiálně	principiálně	k6eAd1	principiálně
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
tvrdé	tvrdý	k2eAgInPc1d1	tvrdý
(	(	kIx(	(
<g/>
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
podobné	podobný	k2eAgFnPc1d1	podobná
boty	bota	k1gFnPc1	bota
jako	jako	k8xC	jako
při	při	k7c6	při
lyžování	lyžování	k1gNnSc6	lyžování
<g/>
)	)	kIx)	)
či	či	k8xC	či
tzv.	tzv.	kA	tzv.
měkké	měkký	k2eAgFnPc1d1	měkká
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
speciální	speciální	k2eAgFnPc4d1	speciální
snowboardové	snowboardový	k2eAgFnPc4d1	snowboardová
boty	bota	k1gFnPc4	bota
k	k	k7c3	k
prknu	prkno	k1gNnSc3	prkno
připevněny	připevněn	k2eAgInPc1d1	připevněn
upínacími	upínací	k2eAgInPc7d1	upínací
pásky	pásek	k1gInPc7	pásek
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
variant	varianta	k1gFnPc2	varianta
měkého	měkého	k2eAgNnSc2d1	měkého
vázání	vázání	k1gNnSc2	vázání
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
systém	systém	k1gInSc4	systém
step-in	stepna	k1gFnPc2	step-ina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
zapíná	zapínat	k5eAaImIp3nS	zapínat
nášlapným	nášlapný	k2eAgInSc7d1	nášlapný
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Novou	nový	k2eAgFnSc7d1	nová
variantou	varianta	k1gFnSc7	varianta
měkkého	měkký	k2eAgNnSc2d1	měkké
vázání	vázání	k1gNnSc2	vázání
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
flow	flow	k?	flow
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
bota	bota	k1gFnSc1	bota
do	do	k7c2	do
vázání	vázání	k1gNnSc2	vázání
vkládá	vkládat	k5eAaImIp3nS	vkládat
zezadu	zezadu	k6eAd1	zezadu
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečnostní	bezpečnostní	k2eAgNnSc4d1	bezpečnostní
vázání	vázání	k1gNnSc4	vázání
které	který	k3yIgNnSc4	který
by	by	kYmCp3nS	by
v	v	k7c6	v
případě	případ	k1gInSc6	případ
pádu	pád	k1gInSc2	pád
snowboard	snowboard	k1gInSc4	snowboard
uvolnilo	uvolnit	k5eAaPmAgNnS	uvolnit
(	(	kIx(	(
<g/>
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
běžně	běžně	k6eAd1	běžně
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
<g/>
)	)	kIx)	)
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zatím	zatím	k6eAd1	zatím
nebyl	být	k5eNaImAgInS	být
vynalezen	vynalezen	k2eAgInSc1d1	vynalezen
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
by	by	kYmCp3nS	by
uvolnil	uvolnit	k5eAaPmAgMnS	uvolnit
obě	dva	k4xCgFnPc4	dva
nohy	noha	k1gFnPc4	noha
zároveň	zároveň	k6eAd1	zároveň
a	a	k8xC	a
při	při	k7c6	při
uvolnění	uvolnění	k1gNnSc6	uvolnění
jen	jen	k9	jen
jedné	jeden	k4xCgFnSc2	jeden
nohy	noha	k1gFnSc2	noha
by	by	kYmCp3nP	by
váha	váha	k1gFnSc1	váha
snowboardu	snowboard	k1gInSc2	snowboard
mohla	moct	k5eAaImAgFnS	moct
jezdci	jezdec	k1gMnSc3	jezdec
zlomit	zlomit	k5eAaPmF	zlomit
druhou	druhý	k4xOgFnSc4	druhý
nohu	noha	k1gFnSc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výraznému	výrazný	k2eAgNnSc3d1	výrazné
rozšíření	rozšíření	k1gNnSc3	rozšíření
tohoto	tento	k3xDgInSc2	tento
sportu	sport	k1gInSc2	sport
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
mezi	mezi	k7c7	mezi
mladou	mladý	k2eAgFnSc7d1	mladá
generací	generace	k1gFnSc7	generace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
drtivé	drtivý	k2eAgFnSc6d1	drtivá
většině	většina	k1gFnSc6	většina
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
používá	používat	k5eAaImIp3nS	používat
měkké	měkký	k2eAgNnSc1d1	měkké
vázaní	vázaný	k2eAgMnPc1d1	vázaný
(	(	kIx(	(
<g/>
které	který	k3yRgNnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
při	při	k7c6	při
jízdě	jízda	k1gFnSc6	jízda
provádět	provádět	k5eAaImF	provádět
různé	různý	k2eAgInPc4d1	různý
skoky	skok	k1gInPc4	skok
a	a	k8xC	a
triky	trik	k1gInPc4	trik
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
vázání	vázání	k1gNnSc1	vázání
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
carvingovou	carvingový	k2eAgFnSc4d1	carvingová
jízdu	jízda	k1gFnSc4	jízda
a	a	k8xC	a
závodní	závodní	k2eAgInSc1d1	závodní
snowboarding	snowboarding	k1gInSc1	snowboarding
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
lepší	dobrý	k2eAgFnSc4d2	lepší
kontrolu	kontrola	k1gFnSc4	kontrola
boardu	boardu	k6eAd1	boardu
během	během	k7c2	během
jízdy	jízda	k1gFnSc2	jízda
<g/>
.	.	kIx.	.
</s>
<s>
Regular	Regular	k1gInSc1	Regular
-	-	kIx~	-
postavení	postavení	k1gNnSc1	postavení
s	s	k7c7	s
levou	levý	k2eAgFnSc7d1	levá
nohou	noha	k1gFnSc7	noha
vpředu	vpředu	k6eAd1	vpředu
Goofy	Goof	k1gInPc1	Goof
-	-	kIx~	-
postavení	postavení	k1gNnSc1	postavení
s	s	k7c7	s
pravou	pravý	k2eAgFnSc7d1	pravá
nohou	noha	k1gFnSc7	noha
vpředu	vpříst	k5eAaPmIp1nS	vpříst
Regular	Regular	k1gInSc1	Regular
či	či	k8xC	či
Goofy	Goof	k1gInPc1	Goof
závisí	záviset	k5eAaImIp3nP	záviset
na	na	k7c6	na
preferenci	preference	k1gFnSc6	preference
každého	každý	k3xTgMnSc2	každý
jezdce	jezdec	k1gMnSc2	jezdec
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
praváctví	praváctví	k1gNnSc2	praváctví
<g/>
/	/	kIx~	/
<g/>
leváctví	leváctví	k1gNnSc1	leváctví
je	být	k5eAaImIp3nS	být
rozložení	rozložení	k1gNnSc4	rozložení
Regular	Regulara	k1gFnPc2	Regulara
<g/>
/	/	kIx~	/
<g/>
Goofy	Goof	k1gInPc7	Goof
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
zhruba	zhruba	k6eAd1	zhruba
50	[number]	k4	50
<g/>
:	:	kIx,	:
<g/>
50	[number]	k4	50
<g/>
.	.	kIx.	.
</s>
<s>
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
jezdec	jezdec	k1gMnSc1	jezdec
zvládá	zvládat	k5eAaImIp3nS	zvládat
jízdu	jízda	k1gFnSc4	jízda
na	na	k7c4	na
obě	dva	k4xCgFnPc4	dva
strany	strana	k1gFnPc4	strana
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
platí	platit	k5eAaImIp3nS	platit
především	především	k9	především
pro	pro	k7c4	pro
triky	trik	k1gInPc4	trik
a	a	k8xC	a
skoky	skok	k1gInPc4	skok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
změna	změna	k1gFnSc1	změna
stran	strana	k1gFnPc2	strana
jízdy	jízda	k1gFnSc2	jízda
bývá	bývat	k5eAaImIp3nS	bývat
občas	občas	k6eAd1	občas
nezbytná	nezbytný	k2eAgFnSc1d1	nezbytná
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
vídáme	vídat	k5eAaImIp1nP	vídat
někdy	někdy	k6eAd1	někdy
také	také	k9	také
postavení	postavení	k1gNnSc1	postavení
nohou	noha	k1gFnPc2	noha
Duckfoot	Duckfoota	k1gFnPc2	Duckfoota
-	-	kIx~	-
obě	dva	k4xCgFnPc1	dva
nohy	noha	k1gFnPc1	noha
jsou	být	k5eAaImIp3nP	být
vychýleny	vychýlen	k2eAgFnPc1d1	vychýlena
špičkou	špička	k1gFnSc7	špička
ven	ven	k6eAd1	ven
a	a	k8xC	a
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
průběžnou	průběžný	k2eAgFnSc4d1	průběžná
změnu	změna	k1gFnSc4	změna
stran	strana	k1gFnPc2	strana
za	za	k7c2	za
jízdy	jízda	k1gFnSc2	jízda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
pokusy	pokus	k1gInPc1	pokus
o	o	k7c6	o
"	"	kIx"	"
<g/>
surfování	surfování	k1gNnSc6	surfování
<g/>
"	"	kIx"	"
na	na	k7c6	na
sněhu	sníh	k1gInSc6	sníh
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
objevily	objevit	k5eAaPmAgFnP	objevit
již	již	k6eAd1	již
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
snowboarding	snowboarding	k1gInSc4	snowboarding
je	být	k5eAaImIp3nS	být
zlomový	zlomový	k2eAgInSc4d1	zlomový
rok	rok	k1gInSc4	rok
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Sherman	Sherman	k1gMnSc1	Sherman
Poppen	Poppen	k2eAgMnSc1d1	Poppen
spojil	spojit	k5eAaPmAgMnS	spojit
dvě	dva	k4xCgFnPc4	dva
lyže	lyže	k1gFnPc4	lyže
<g/>
,	,	kIx,	,
ke	k	k7c3	k
špičce	špička	k1gFnSc3	špička
přimontoval	přimontovat	k5eAaPmAgInS	přimontovat
provázek	provázek	k1gInSc1	provázek
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
tím	ten	k3xDgNnSc7	ten
první	první	k4xOgFnSc6	první
model	model	k1gInSc1	model
snowboardu	snowboard	k1gInSc2	snowboard
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
"	"	kIx"	"
<g/>
snurfer	snurfer	k1gInSc1	snurfer
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jízda	jízda	k1gFnSc1	jízda
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
se	se	k3xPyFc4	se
podobala	podobat	k5eAaImAgFnS	podobat
spíše	spíše	k9	spíše
surfování	surfování	k1gNnSc4	surfování
na	na	k7c6	na
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Inspirován	inspirován	k2eAgInSc1d1	inspirován
Shermannovým	Shermannův	k2eAgInSc7d1	Shermannův
úspěchem	úspěch	k1gInSc7	úspěch
začal	začít	k5eAaPmAgInS	začít
Jake	Jake	k1gFnSc4	Jake
Burton	Burton	k1gInSc1	Burton
Carpenter	Carpenter	k1gMnSc1	Carpenter
vyrábět	vyrábět	k5eAaImF	vyrábět
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
snowboardy	snowboard	k1gInPc4	snowboard
<g/>
.	.	kIx.	.
</s>
<s>
Rozpoznal	rozpoznat	k5eAaPmAgMnS	rozpoznat
chyby	chyba	k1gFnPc4	chyba
a	a	k8xC	a
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
snurferu	snurfer	k1gInSc2	snurfer
<g/>
,	,	kIx,	,
opravil	opravit	k5eAaPmAgMnS	opravit
je	být	k5eAaImIp3nS	být
a	a	k8xC	a
přimontoval	přimontovat	k5eAaPmAgMnS	přimontovat
na	na	k7c4	na
desku	deska	k1gFnSc4	deska
první	první	k4xOgNnSc4	první
vázání	vázání	k1gNnSc4	vázání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
několika	několik	k4yIc7	několik
pásky	páska	k1gFnSc2	páska
pevně	pevně	k6eAd1	pevně
drželo	držet	k5eAaImAgNnS	držet
nohy	noha	k1gFnSc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
založil	založit	k5eAaPmAgMnS	založit
ve	v	k7c6	v
Vermontu	Vermont	k1gInSc6	Vermont
firmu	firma	k1gFnSc4	firma
Burton	Burton	k1gInSc4	Burton
Snowboards	Snowboardsa	k1gFnPc2	Snowboardsa
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
objevil	objevit	k5eAaPmAgMnS	objevit
Dmitrij	Dmitrij	k1gMnSc1	Dmitrij
Milovich	Milovich	k1gMnSc1	Milovich
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
přišel	přijít	k5eAaPmAgInS	přijít
s	s	k7c7	s
prknem	prkno	k1gNnSc7	prkno
odlitým	odlitý	k2eAgFnPc3d1	odlitá
z	z	k7c2	z
polyesteru	polyester	k1gInSc2	polyester
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
prkna	prkno	k1gNnPc1	prkno
měla	mít	k5eAaImAgNnP	mít
nízkou	nízký	k2eAgFnSc7d1	nízká
životnost	životnost	k1gFnSc4	životnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přesto	přesto	k8xC	přesto
si	se	k3xPyFc3	se
Milovich	Milovich	k1gMnSc1	Milovich
nechal	nechat	k5eAaPmAgMnS	nechat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
tento	tento	k3xDgInSc4	tento
výrobek	výrobek	k1gInSc1	výrobek
patentovat	patentovat	k5eAaBmF	patentovat
a	a	k8xC	a
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
založil	založit	k5eAaPmAgInS	založit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
firmu	firma	k1gFnSc4	firma
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
nakonec	nakonec	k6eAd1	nakonec
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
již	již	k6eAd1	již
vylepšené	vylepšený	k2eAgFnPc4d1	vylepšená
verze	verze	k1gFnPc4	verze
boardů	board	k1gInPc2	board
-	-	kIx~	-
měly	mít	k5eAaImAgInP	mít
laminátovou	laminátový	k2eAgFnSc4d1	laminátová
konstrukci	konstrukce	k1gFnSc4	konstrukce
a	a	k8xC	a
skluznici	skluznice	k1gFnSc4	skluznice
z	z	k7c2	z
P-texu	Pex	k1gInSc2	P-tex
<g/>
.	.	kIx.	.
</s>
<s>
Tom	Tom	k1gMnSc1	Tom
Sims	Simsa	k1gFnPc2	Simsa
o	o	k7c4	o
pár	pár	k4xCyI	pár
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
zdokonalování	zdokonalování	k1gNnSc6	zdokonalování
snowboardu	snowboard	k1gInSc2	snowboard
<g/>
;	;	kIx,	;
původně	původně	k6eAd1	původně
byla	být	k5eAaImAgNnP	být
jeho	jeho	k3xOp3gFnSc4	jeho
prkna	prkno	k1gNnPc1	prkno
podobná	podobný	k2eAgNnPc4d1	podobné
těm	ten	k3xDgFnPc3	ten
Milovichovým	Milovichová	k1gFnPc3	Milovichová
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
začal	začít	k5eAaPmAgInS	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
prkna	prkno	k1gNnPc4	prkno
s	s	k7c7	s
laminátovou	laminátový	k2eAgFnSc7d1	laminátová
konstrukcí	konstrukce	k1gFnSc7	konstrukce
s	s	k7c7	s
dřevěným	dřevěný	k2eAgNnSc7d1	dřevěné
jádrem	jádro	k1gNnSc7	jádro
(	(	kIx(	(
<g/>
vyšší	vysoký	k2eAgFnSc4d2	vyšší
odolnost	odolnost	k1gFnSc4	odolnost
prkna	prkno	k1gNnSc2	prkno
<g/>
,	,	kIx,	,
lepší	dobrý	k2eAgFnPc1d2	lepší
jízdní	jízdní	k2eAgFnPc1d1	jízdní
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
)	)	kIx)	)
+	+	kIx~	+
ocelové	ocelový	k2eAgFnPc1d1	ocelová
hrany	hrana	k1gFnPc1	hrana
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
nimž	jenž	k3xRgFnPc3	jenž
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
používat	používat	k5eAaImF	používat
snowboard	snowboard	k1gInSc4	snowboard
i	i	k9	i
na	na	k7c6	na
tvrdém	tvrdý	k2eAgInSc6d1	tvrdý
sněhu	sníh	k1gInSc6	sníh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
přišel	přijít	k5eAaPmAgMnS	přijít
Jeff	Jeff	k1gMnSc1	Jeff
Grell	Grell	k1gInSc4	Grell
s	s	k7c7	s
prvním	první	k4xOgInSc7	první
skutečně	skutečně	k6eAd1	skutečně
pevným	pevný	k2eAgNnSc7d1	pevné
vázáním	vázání	k1gNnSc7	vázání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
stabilně	stabilně	k6eAd1	stabilně
spojovalo	spojovat	k5eAaImAgNnS	spojovat
nohy	noha	k1gFnSc2	noha
jezdce	jezdec	k1gMnSc2	jezdec
s	s	k7c7	s
prknem	prkno	k1gNnSc7	prkno
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
vázáním	vázání	k1gNnSc7	vázání
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
ovládat	ovládat	k5eAaImF	ovládat
prkno	prkno	k1gNnSc4	prkno
i	i	k9	i
na	na	k7c6	na
tvrdém	tvrdý	k2eAgInSc6d1	tvrdý
a	a	k8xC	a
nerovném	rovný	k2eNgInSc6d1	nerovný
sněhu	sníh	k1gInSc6	sníh
a	a	k8xC	a
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
rychlostech	rychlost	k1gFnPc6	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
se	se	k3xPyFc4	se
snowboard	snowboard	k1gInSc1	snowboard
objevil	objevit	k5eAaPmAgInS	objevit
i	i	k9	i
v	v	k7c6	v
Bondovce	Bondovce	k?	Bondovce
s	s	k7c7	s
názvem	název	k1gInSc7	název
Vyhlídka	vyhlídka	k1gFnSc1	vyhlídka
na	na	k7c4	na
vraždu	vražda	k1gFnSc4	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
povědomí	povědomí	k1gNnSc2	povědomí
široké	široký	k2eAgFnSc2d1	široká
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Snowboarding	snowboarding	k1gInSc1	snowboarding
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
brán	brát	k5eAaImNgMnS	brát
nejprve	nejprve	k6eAd1	nejprve
jako	jako	k8xC	jako
módní	módní	k2eAgInSc4d1	módní
výstřelek	výstřelek	k1gInSc4	výstřelek
či	či	k8xC	či
exhibicionismus	exhibicionismus	k1gInSc4	exhibicionismus
několika	několik	k4yIc2	několik
málo	málo	k4c1	málo
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgInS	být
uznávaný	uznávaný	k2eAgMnSc1d1	uznávaný
jako	jako	k8xC	jako
seriózní	seriózní	k2eAgInSc1d1	seriózní
sport	sport	k1gInSc1	sport
a	a	k8xC	a
zimní	zimní	k2eAgNnPc1d1	zimní
střediska	středisko	k1gNnPc1	středisko
nedovolovala	dovolovat	k5eNaImAgNnP	dovolovat
vstup	vstup	k1gInSc4	vstup
snowboardistů	snowboardista	k1gMnPc2	snowboardista
na	na	k7c4	na
vleky	vlek	k1gInPc4	vlek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
snowboarding	snowboarding	k1gInSc1	snowboarding
stává	stávat	k5eAaImIp3nS	stávat
masovou	masový	k2eAgFnSc7d1	masová
záležitostí	záležitost	k1gFnSc7	záležitost
a	a	k8xC	a
vývoje	vývoj	k1gInSc2	vývoj
nových	nový	k2eAgFnPc2d1	nová
technologií	technologie	k1gFnPc2	technologie
se	se	k3xPyFc4	se
chopily	chopit	k5eAaPmAgFnP	chopit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
i	i	k8xC	i
zavedené	zavedený	k2eAgInPc4d1	zavedený
lyžařské	lyžařský	k2eAgInPc4d1	lyžařský
koncerny	koncern	k1gInPc4	koncern
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
nimž	jenž	k3xRgFnPc3	jenž
se	se	k3xPyFc4	se
vybavení	vybavení	k1gNnSc1	vybavení
pro	pro	k7c4	pro
snowboarding	snowboarding	k1gInSc4	snowboarding
rozrůstá	rozrůstat	k5eAaImIp3nS	rozrůstat
s	s	k7c7	s
nebývalou	bývalý	k2eNgFnSc7d1	bývalý
dynamikou	dynamika	k1gFnSc7	dynamika
<g/>
.	.	kIx.	.
</s>
<s>
Snowboarding	snowboarding	k1gInSc1	snowboarding
Snowkiting	Snowkiting	k1gInSc4	Snowkiting
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Snowboard	snowboard	k1gInSc1	snowboard
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
snowboard	snowboard	k1gInSc1	snowboard
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Galerie	galerie	k1gFnSc2	galerie
Snowboard	snowboard	k1gInSc1	snowboard
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
FisSnowboardWorldCup	FisSnowboardWorldCup	k1gInSc1	FisSnowboardWorldCup
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
FIS	FIS	kA	FIS
Světový	světový	k2eAgInSc4d1	světový
pohár	pohár	k1gInSc4	pohár
ve	v	k7c4	v
Snowboard	snowboard	k1gInSc4	snowboard
-	-	kIx~	-
oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
FIS	fis	k1gNnSc2	fis
http://www.snowboarding.cz	[url]	k1gFnPc2	http://www.snowboarding.cz
AČS	AČS	kA	AČS
-	-	kIx~	-
Asociace	asociace	k1gFnSc1	asociace
českého	český	k2eAgInSc2d1	český
snowboardingu	snowboarding	k1gInSc2	snowboarding
http://www.freeride.cz/snowboard	[url]	k1gMnSc1	http://www.freeride.cz/snowboard
Freeride	Freerid	k1gInSc5	Freerid
-	-	kIx~	-
Nejrozsáhlejší	rozsáhlý	k2eAgInSc1d3	nejrozsáhlejší
český	český	k2eAgInSc1d1	český
web	web	k1gInSc1	web
o	o	k7c6	o
snowboardingu	snowboarding	k1gInSc6	snowboarding
</s>
