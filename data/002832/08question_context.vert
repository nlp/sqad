<s>
Národní	národní	k2eAgMnSc1d1	národní
umělec	umělec	k1gMnSc1	umělec
Vladislav	Vladislav	k1gMnSc1	Vladislav
Vančura	Vančura	k1gMnSc1	Vančura
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1891	[number]	k4	1891
Háj	háj	k1gInSc4	háj
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
původním	původní	k2eAgNnSc7d1	původní
povoláním	povolání	k1gNnSc7	povolání
lékař	lékař	k1gMnSc1	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Háji	háj	k1gInSc6	háj
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
nemohl	moct	k5eNaImAgMnS	moct
uplatnit	uplatnit	k5eAaPmF	uplatnit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
často	často	k6eAd1	často
stěhovali	stěhovat	k5eAaImAgMnP	stěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Dětství	dětství	k1gNnSc4	dětství
prožil	prožít	k5eAaPmAgMnS	prožít
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgMnS	získat
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
přírodě	příroda	k1gFnSc3	příroda
a	a	k8xC	a
koním	koní	k2eAgInPc3d1	koní
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
studijní	studijní	k2eAgInPc1d1	studijní
výsledky	výsledek	k1gInPc1	výsledek
nebyly	být	k5eNaImAgInP	být
příliš	příliš	k6eAd1	příliš
dobré	dobrý	k2eAgInPc1d1	dobrý
a	a	k8xC	a
tak	tak	k6eAd1	tak
středoškolská	středoškolský	k2eAgNnPc4d1	středoškolské
studia	studio	k1gNnPc4	studio
dokončil	dokončit	k5eAaPmAgInS	dokončit
maturitou	maturita	k1gFnSc7	maturita
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
studoval	studovat	k5eAaImAgMnS	studovat
nejdříve	dříve	k6eAd3	dříve
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
těžké	těžký	k2eAgFnSc6d1	těžká
nemoci	nemoc	k1gFnSc6	nemoc
matky	matka	k1gFnSc2	matka
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
na	na	k7c4	na
medicínu	medicína	k1gFnSc4	medicína
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
kolegyní	kolegyně	k1gFnSc7	kolegyně
ze	z	k7c2	z
studií-lékařkou	studiíékařka	k1gFnSc7	studií-lékařka
Ludmilou	Ludmila	k1gFnSc7	Ludmila
Tuhou	tuha	k1gFnSc7	tuha
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
měl	mít	k5eAaImAgMnS	mít
společnou	společný	k2eAgFnSc4d1	společná
ordinaci	ordinace	k1gFnSc4	ordinace
ve	v	k7c6	v
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
se	se	k3xPyFc4	se
manželům	manžel	k1gMnPc3	manžel
Vančurovým	Vančurová	k1gFnPc3	Vančurová
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Alena	Alena	k1gFnSc1	Alena
(	(	kIx(	(
<g/>
provdaná	provdaný	k2eAgFnSc1d1	provdaná
Santarová	Santarová	k1gFnSc1	Santarová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgFnSc1d2	pozdější
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
si	se	k3xPyFc3	se
ve	v	k7c6	v
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
postavil	postavit	k5eAaPmAgMnS	postavit
dům	dům	k1gInSc4	dům
a	a	k8xC	a
věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
výhradně	výhradně	k6eAd1	výhradně
spisovatelské	spisovatelský	k2eAgFnSc2d1	spisovatelská
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
bratrancem	bratranec	k1gMnSc7	bratranec
byl	být	k5eAaImAgMnS	být
Jiří	Jiří	k1gMnSc1	Jiří
Mahen	Mahna	k1gFnPc2	Mahna
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
členem	člen	k1gMnSc7	člen
a	a	k8xC	a
prvním	první	k4xOgMnSc6	první
předsedou	předseda	k1gMnSc7	předseda
Devětsilu	Devětsil	k1gInSc2	Devětsil
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přívrženec	přívrženec	k1gMnSc1	přívrženec
poetismu	poetismus	k1gInSc2	poetismus
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
Levé	levý	k2eAgFnSc2d1	levá
fronty	fronta	k1gFnSc2	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
časopisech	časopis	k1gInPc6	časopis
-	-	kIx~	-
Červen	červen	k1gInSc1	červen
<g/>
,	,	kIx,	,
Kmen	kmen	k1gInSc1	kmen
<g/>
,	,	kIx,	,
Host	host	k1gMnSc1	host
a	a	k8xC	a
Panorama	panorama	k1gNnSc1	panorama
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
byl	být	k5eAaImAgInS	být
členem	člen	k1gMnSc7	člen
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
však	však	k9	však
podepsal	podepsat	k5eAaPmAgMnS	podepsat
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
šesti	šest	k4xCc7	šest
dalšími	další	k2eAgFnPc7d1	další
komunistickými	komunistický	k2eAgFnPc7d1	komunistická
umělci	umělec	k1gMnSc3	umělec
tzv.	tzv.	kA	tzv.
Manifest	manifest	k1gInSc1	manifest
sedmi	sedm	k4xCc2	sedm
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
Gottwaldovo	Gottwaldův	k2eAgNnSc4d1	Gottwaldovo
vedení	vedení	k1gNnSc4	vedení
<g/>
,	,	kIx,	,
a	a	k8xC	a
následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
signatáři	signatář	k1gMnPc7	signatář
z	z	k7c2	z
KSČ	KSČ	kA	KSČ
vyloučen	vyloučit	k5eAaPmNgInS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgInPc7	svůj
postoji	postoj	k1gInPc7	postoj
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
nadále	nadále	k6eAd1	nadále
hlásil	hlásit	k5eAaImAgMnS	hlásit
k	k	k7c3	k
marxistické	marxistický	k2eAgFnSc3d1	marxistická
levici	levice	k1gFnSc3	levice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
období	období	k1gNnSc6	období
března	březen	k1gInSc2	březen
až	až	k8xS	až
května	květen	k1gInSc2	květen
jako	jako	k8xS	jako
divadelní	divadelní	k2eAgMnSc1d1	divadelní
kritik	kritik	k1gMnSc1	kritik
v	v	k7c6	v
deníku	deník	k1gInSc6	deník
"	"	kIx"	"
<g/>
Československá	československý	k2eAgFnSc1d1	Československá
samostatnost	samostatnost	k1gFnSc1	samostatnost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
přejmenovaného	přejmenovaný	k2eAgInSc2d1	přejmenovaný
na	na	k7c4	na
"	"	kIx"	"
<g/>
Národní	národní	k2eAgNnSc4d1	národní
osvobození	osvobození	k1gNnSc4	osvobození
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
převzal	převzít	k5eAaPmAgInS	převzít
místo	místo	k1gNnSc4	místo
po	po	k7c6	po
Jindřichu	Jindřich	k1gMnSc6	Jindřich
Vodákovi	Vodák	k1gMnSc6	Vodák
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
divadelní	divadelní	k2eAgFnSc2d1	divadelní
rubriky	rubrika	k1gFnSc2	rubrika
"	"	kIx"	"
<g/>
Českého	český	k2eAgNnSc2d1	české
slova	slovo	k1gNnSc2	slovo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kritiky	kritik	k1gMnPc4	kritik
podepisoval	podepisovat	k5eAaImAgInS	podepisovat
šifrou	šifra	k1gFnSc7	šifra
-jv	v	k?	-jv
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
nebo	nebo	k8xC	nebo
-W-	-W-	k?	-W-
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
zde	zde	k6eAd1	zde
připravil	připravit	k5eAaPmAgInS	připravit
21	[number]	k4	21
divadelních	divadelní	k2eAgFnPc2d1	divadelní
kritik	kritika	k1gFnPc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
členem	člen	k1gMnSc7	člen
redakční	redakční	k2eAgFnSc2d1	redakční
rady	rada	k1gFnSc2	rada
Družstevní	družstevní	k2eAgFnSc2d1	družstevní
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgInPc1d1	jiný
vyšly	vyjít	k5eAaPmAgInP	vyjít
i	i	k9	i
jeho	jeho	k3xOp3gInPc1	jeho
Obrazy	obraz	k1gInPc1	obraz
z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
národa	národ	k1gInSc2	národ
českého	český	k2eAgInSc2d1	český
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
Československé	československý	k2eAgFnSc2d1	Československá
filmové	filmový	k2eAgFnSc2d1	filmová
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
okupace	okupace	k1gFnSc2	okupace
vedl	vést	k5eAaImAgInS	vést
spisovatelskou	spisovatelský	k2eAgFnSc4d1	spisovatelská
sekci	sekce	k1gFnSc4	sekce
Výboru	výbor	k1gInSc2	výbor
inteligence	inteligence	k1gFnSc2	inteligence
-	-	kIx~	-
ilegální	ilegální	k2eAgFnSc2d1	ilegální
odbojové	odbojový	k2eAgFnSc2d1	odbojová
organizace	organizace	k1gFnSc2	organizace
při	při	k7c6	při
komunistickém	komunistický	k2eAgInSc6d1	komunistický
Ústředním	ústřední	k2eAgInSc6d1	ústřední
národním	národní	k2eAgInSc6d1	národní
revolučním	revoluční	k2eAgInSc6d1	revoluční
výboru	výbor	k1gInSc6	výbor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
na	na	k7c6	na
Kobyliské	kobyliský	k2eAgFnSc6d1	Kobyliská
střelnici	střelnice	k1gFnSc6	střelnice
popraven	popraven	k2eAgInSc1d1	popraven
<g/>
.	.	kIx.	.
</s>
