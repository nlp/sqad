<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
úředním	úřední	k2eAgInSc7d1	úřední
názvem	název	k1gInSc7	název
Korejská	korejský	k2eAgFnSc1d1	Korejská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
Tehan	Tehan	k1gMnSc1	Tehan
minguk	minguk	k1gMnSc1	minguk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
zaujímající	zaujímající	k2eAgFnSc4d1	zaujímající
jižní	jižní	k2eAgFnSc4d1	jižní
polovinu	polovina	k1gFnSc4	polovina
Korejského	korejský	k2eAgInSc2d1	korejský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
