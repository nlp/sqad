<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
úředním	úřední	k2eAgInSc7d1	úřední
názvem	název	k1gInSc7	název
Korejská	korejský	k2eAgFnSc1d1	Korejská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
Tehan	Tehan	k1gMnSc1	Tehan
minguk	minguk	k1gMnSc1	minguk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaImF	stát
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
zaujímající	zaujímající	k2eAgFnSc4d1	zaujímající
jižní	jižní	k2eAgFnSc4d1	jižní
polovinu	polovina	k1gFnSc4	polovina
Korejského	korejský	k2eAgInSc2d1	korejský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
sousedem	soused	k1gMnSc7	soused
je	být	k5eAaImIp3nS	být
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgMnPc3d3	veliký
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Soul	Soul	k1gInSc1	Soul
<g/>
.	.	kIx.	.
</s>
<s>
Úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
korejština	korejština	k1gFnSc1	korejština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
korejštině	korejština	k1gFnSc6	korejština
zní	znět	k5eAaImIp3nS	znět
celý	celý	k2eAgInSc4d1	celý
název	název	k1gInSc4	název
Tehan	Tehan	k1gMnSc1	Tehan
minguk	minguk	k1gMnSc1	minguk
(	(	kIx(	(
<g/>
대	대	k?	대
Republika	republika	k1gFnSc1	republika
velkého	velký	k2eAgInSc2d1	velký
národa	národ	k1gInSc2	národ
Han	Hana	k1gFnPc2	Hana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
zkrácené	zkrácený	k2eAgFnSc6d1	zkrácená
podobě	podoba	k1gFnSc6	podoba
Hanguk	Hanguk	k1gInSc1	Hanguk
(	(	kIx(	(
<g/>
한	한	k?	한
Země	zem	k1gFnSc2	zem
Han	Hana	k1gFnPc2	Hana
-	-	kIx~	-
jihokorejský	jihokorejský	k2eAgInSc1d1	jihokorejský
společný	společný	k2eAgInSc1d1	společný
název	název	k1gInSc1	název
pro	pro	k7c4	pro
Severní	severní	k2eAgFnSc4d1	severní
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc4d1	jižní
Koreu	Korea	k1gFnSc4	Korea
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Namhan	Namhan	k1gInSc1	Namhan
(	(	kIx(	(
<g/>
남	남	k?	남
-	-	kIx~	-
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
je	být	k5eAaImIp3nS	být
zřízením	zřízení	k1gNnSc7	zřízení
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
republika	republika	k1gFnSc1	republika
s	s	k7c7	s
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
formou	forma	k1gFnSc7	forma
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
po	po	k7c6	po
korejské	korejský	k2eAgFnSc6d1	Korejská
válce	válka	k1gFnSc6	válka
patřila	patřit	k5eAaImAgFnS	patřit
mezi	mezi	k7c4	mezi
nejchudší	chudý	k2eAgInPc4d3	nejchudší
státy	stát	k1gInPc4	stát
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
její	její	k3xOp3gFnSc1	její
ekonomika	ekonomika	k1gFnSc1	ekonomika
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
rapidní	rapidní	k2eAgInSc4d1	rapidní
růst	růst	k1gInSc4	růst
a	a	k8xC	a
zařadila	zařadit	k5eAaPmAgFnS	zařadit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
asijské	asijský	k2eAgMnPc4d1	asijský
tygry	tygr	k1gMnPc4	tygr
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
plně	plně	k6eAd1	plně
vyspělým	vyspělý	k2eAgInSc7d1	vyspělý
státem	stát	k1gInSc7	stát
s	s	k7c7	s
ekonomikou	ekonomika	k1gFnSc7	ekonomika
světového	světový	k2eAgInSc2d1	světový
významu	význam	k1gInSc2	význam
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
nejvyšším	vysoký	k2eAgFnPc3d3	nejvyšší
HDP	HDP	kA	HDP
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
Korea	Korea	k1gFnSc1	Korea
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
podél	podél	k7c2	podél
38	[number]	k4	38
<g/>
.	.	kIx.	.
rovnoběžky	rovnoběžka	k1gFnSc2	rovnoběžka
na	na	k7c4	na
severní	severní	k2eAgFnSc4d1	severní
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
okupována	okupovat	k5eAaBmNgFnS	okupovat
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
jižní	jižní	k2eAgFnSc4d1	jižní
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
okupována	okupovat	k5eAaBmNgFnS	okupovat
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
založily	založit	k5eAaPmAgFnP	založit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
prokomunistické	prokomunistický	k2eAgNnSc1d1	prokomunistické
povstání	povstání	k1gNnSc1	povstání
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Čedžu	Čedžu	k1gFnSc2	Čedžu
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ale	ale	k9	ale
bylo	být	k5eAaImAgNnS	být
jihokorejskou	jihokorejský	k2eAgFnSc7d1	jihokorejská
vládou	vláda	k1gFnSc7	vláda
tvrdě	tvrdě	k6eAd1	tvrdě
potlačeno	potlačen	k2eAgNnSc1d1	potlačeno
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
desetitisíce	desetitisíce	k1gInPc4	desetitisíce
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Korejská	korejský	k2eAgFnSc1d1	Korejská
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1950	[number]	k4	1950
překročila	překročit	k5eAaPmAgFnS	překročit
vojska	vojsko	k1gNnPc1	vojsko
KLDR	KLDR	kA	KLDR
hraniční	hraniční	k2eAgFnSc4d1	hraniční
rovnoběžku	rovnoběžka	k1gFnSc4	rovnoběžka
a	a	k8xC	a
zahájila	zahájit	k5eAaPmAgFnS	zahájit
tak	tak	k6eAd1	tak
Korejskou	korejský	k2eAgFnSc4d1	Korejská
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
KLDR	KLDR	kA	KLDR
vyslala	vyslat	k5eAaPmAgFnS	vyslat
do	do	k7c2	do
války	válka	k1gFnSc2	válka
své	svůj	k3xOyFgMnPc4	svůj
vojáky	voják	k1gMnPc4	voják
komunistická	komunistický	k2eAgFnSc1d1	komunistická
Čína	Čína	k1gFnSc1	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
a	a	k8xC	a
OSN	OSN	kA	OSN
podporovaný	podporovaný	k2eAgInSc4d1	podporovaný
jih	jih	k1gInSc4	jih
a	a	k8xC	a
komunistickým	komunistický	k2eAgInSc7d1	komunistický
blokem	blok	k1gInSc7	blok
zaštiťovaný	zaštiťovaný	k2eAgInSc1d1	zaštiťovaný
sever	sever	k1gInSc1	sever
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
slepé	slepý	k2eAgFnSc2d1	slepá
uličky	ulička	k1gFnSc2	ulička
a	a	k8xC	a
musely	muset	k5eAaImAgFnP	muset
podepsat	podepsat	k5eAaPmF	podepsat
příměří	příměří	k1gNnSc4	příměří
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
Korejský	korejský	k2eAgInSc1d1	korejský
poloostrov	poloostrov	k1gInSc1	poloostrov
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
demilitarizovanou	demilitarizovaný	k2eAgFnSc7d1	demilitarizovaná
zónou	zóna	k1gFnSc7	zóna
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
původní	původní	k2eAgFnSc2d1	původní
demarkační	demarkační	k2eAgFnSc2d1	demarkační
linie	linie	k1gFnSc2	linie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
nebo	nebo	k8xC	nebo
pohřešováno	pohřešovat	k5eAaImNgNnS	pohřešovat
až	až	k6eAd1	až
3,5	[number]	k4	3,5
milionů	milion	k4xCgInPc2	milion
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
oznámila	oznámit	k5eAaPmAgFnS	oznámit
že	že	k8xS	že
se	s	k7c7	s
příměřím	příměří	k1gNnSc7	příměří
již	již	k6eAd1	již
necítí	cítit	k5eNaImIp3nS	cítit
vázána	vázán	k2eAgFnSc1d1	vázána
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
březen	březen	k1gInSc4	březen
2013	[number]	k4	2013
Korejská	korejský	k2eAgFnSc1d1	Korejská
lidově	lidově	k6eAd1	lidově
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
oficiálně	oficiálně	k6eAd1	oficiálně
oznamuje	oznamovat	k5eAaImIp3nS	oznamovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
korejská	korejský	k2eAgFnSc1d1	Korejská
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
příměří	příměří	k1gNnSc6	příměří
se	se	k3xPyFc4	se
s	s	k7c7	s
okamžitou	okamžitý	k2eAgFnSc7d1	okamžitá
platností	platnost	k1gFnSc7	platnost
ruší	rušit	k5eAaImIp3nS	rušit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
autokratická	autokratický	k2eAgFnSc1d1	autokratická
vláda	vláda	k1gFnSc1	vláda
I	i	k9	i
Sung-mana	Sungan	k1gMnSc4	Sung-man
odstavena	odstaven	k2eAgMnSc4d1	odstaven
od	od	k7c2	od
moci	moc	k1gFnSc2	moc
studentskými	studentský	k2eAgInPc7d1	studentský
nepokoji	nepokoj	k1gInPc7	nepokoj
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
byla	být	k5eAaImAgFnS	být
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
krátká	krátký	k2eAgFnSc1d1	krátká
doba	doba	k1gFnSc1	doba
občanské	občanský	k2eAgFnSc2d1	občanská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
vojenský	vojenský	k2eAgInSc1d1	vojenský
převrat	převrat	k1gInSc1	převrat
vedený	vedený	k2eAgInSc1d1	vedený
generálem	generál	k1gMnSc7	generál
Pak	pak	k8xC	pak
Čong-huiem	Čonguius	k1gMnSc7	Čong-huius
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
nastolil	nastolit	k5eAaPmAgMnS	nastolit
diktaturu	diktatura	k1gFnSc4	diktatura
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
trvala	trvat	k5eAaImAgFnS	trvat
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
ekonomika	ekonomika	k1gFnSc1	ekonomika
prudce	prudko	k6eAd1	prudko
posílila	posílit	k5eAaPmAgFnS	posílit
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
byl	být	k5eAaImAgMnS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
a	a	k8xC	a
dalším	další	k2eAgInSc7d1	další
překvapivým	překvapivý	k2eAgInSc7d1	překvapivý
převratem	převrat	k1gInSc7	převrat
se	se	k3xPyFc4	se
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
moci	moct	k5eAaImF	moct
generál	generál	k1gMnSc1	generál
Čon	Čon	k1gMnSc1	Čon
Tu-hwan	Tuwan	k1gMnSc1	Tu-hwan
<g/>
.	.	kIx.	.
</s>
<s>
Masivní	masivní	k2eAgFnPc1d1	masivní
studentské	studentský	k2eAgFnPc1d1	studentská
demonstrace	demonstrace	k1gFnPc1	demonstrace
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
měly	mít	k5eAaImAgFnP	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
vojenský	vojenský	k2eAgInSc4d1	vojenský
zákrok	zákrok	k1gInSc4	zákrok
a	a	k8xC	a
masakr	masakr	k1gInSc4	masakr
v	v	k7c6	v
Kwangdžu	Kwangdžu	k1gFnSc6	Kwangdžu
(	(	kIx(	(
<g/>
Povstání	povstání	k1gNnSc1	povstání
v	v	k7c6	v
Kwangdžu	Kwangdžu	k1gFnSc6	Kwangdžu
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Občanské	občanský	k2eAgInPc1d1	občanský
nepokoje	nepokoj	k1gInPc1	nepokoj
způsobily	způsobit	k5eAaPmAgInP	způsobit
konec	konec	k1gInSc4	konec
vojenské	vojenský	k2eAgFnSc2d1	vojenská
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
demokratické	demokratický	k2eAgFnSc2d1	demokratická
reformy	reforma	k1gFnSc2	reforma
postupně	postupně	k6eAd1	postupně
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
Ro	Ro	k1gMnSc2	Ro
Tche-ua	Tcheus	k1gMnSc2	Tche-uus
<g/>
,	,	kIx,	,
Kim	Kim	k1gMnSc2	Kim
Jong-sama	Jongam	k1gMnSc2	Jong-sam
a	a	k8xC	a
Kim	Kim	k1gMnSc2	Kim
Te-džunga	Težung	k1gMnSc2	Te-džung
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
velkých	velký	k2eAgFnPc2d1	velká
světových	světový	k2eAgFnPc2d1	světová
ekonomik	ekonomika	k1gFnPc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
se	se	k3xPyFc4	se
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
připojila	připojit	k5eAaPmAgFnS	připojit
k	k	k7c3	k
OECD	OECD	kA	OECD
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
národ	národ	k1gInSc1	národ
velice	velice	k6eAd1	velice
ekonomicky	ekonomicky	k6eAd1	ekonomicky
strádal	strádat	k5eAaImAgMnS	strádat
během	během	k7c2	během
Asijské	asijský	k2eAgFnSc2d1	asijská
finanční	finanční	k2eAgFnSc2d1	finanční
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
plně	plně	k6eAd1	plně
fungující	fungující	k2eAgFnSc7d1	fungující
moderní	moderní	k2eAgFnSc7d1	moderní
demokracií	demokracie	k1gFnSc7	demokracie
a	a	k8xC	a
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
asijských	asijský	k2eAgMnPc2d1	asijský
tygrů	tygr	k1gMnPc2	tygr
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
Korejského	korejský	k2eAgInSc2d1	korejský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
západu	západ	k1gInSc2	západ
je	být	k5eAaImIp3nS	být
omývána	omývat	k5eAaImNgFnS	omývat
vodami	voda	k1gFnPc7	voda
Žlutého	žlutý	k2eAgNnSc2d1	žluté
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
vodami	voda	k1gFnPc7	voda
Japonského	japonský	k2eAgNnSc2d1	Japonské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
poválečná	poválečný	k2eAgFnSc1d1	poválečná
nevraživost	nevraživost	k1gFnSc1	nevraživost
vůči	vůči	k7c3	vůči
Japonsku	Japonsko	k1gNnSc3	Japonsko
se	se	k3xPyFc4	se
promítla	promítnout	k5eAaPmAgNnP	promítnout
i	i	k9	i
do	do	k7c2	do
geografických	geografický	k2eAgInPc2d1	geografický
názvů	název	k1gInPc2	název
<g/>
:	:	kIx,	:
Korejci	Korejec	k1gMnPc1	Korejec
Žluté	žlutý	k2eAgFnSc2d1	žlutá
moře	moře	k1gNnSc2	moře
nazývají	nazývat	k5eAaImIp3nP	nazývat
Západní	západní	k2eAgNnSc4d1	západní
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
Japonské	japonský	k2eAgNnSc4d1	Japonské
moře	moře	k1gNnSc4	moře
pak	pak	k6eAd1	pak
nazývají	nazývat	k5eAaImIp3nP	nazývat
Východní	východní	k2eAgNnSc4d1	východní
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Terén	terén	k1gInSc1	terén
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Koreji	Korea	k1gFnSc6	Korea
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
hornatý	hornatý	k2eAgMnSc1d1	hornatý
(	(	kIx(	(
<g/>
pohoří	pohořet	k5eAaPmIp3nP	pohořet
Diamantové	diamantový	k2eAgFnPc1d1	Diamantová
hory	hora	k1gFnPc1	hora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
půdy	půda	k1gFnSc2	půda
není	být	k5eNaImIp3nS	být
obdělávatelná	obdělávatelný	k2eAgFnSc1d1	obdělávatelná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
a	a	k8xC	a
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
země	zem	k1gFnSc2	zem
jsou	být	k5eAaImIp3nP	být
nížiny	nížina	k1gFnPc1	nížina
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
ale	ale	k8xC	ale
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
jen	jen	k9	jen
30	[number]	k4	30
<g/>
%	%	kIx~	%
plochy	plocha	k1gFnSc2	plocha
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Korejského	korejský	k2eAgInSc2d1	korejský
poloostrova	poloostrov	k1gInSc2	poloostrov
je	být	k5eAaImIp3nS	být
také	také	k9	také
asi	asi	k9	asi
3	[number]	k4	3
000	[number]	k4	000
menších	malý	k2eAgInPc2d2	menší
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
největší	veliký	k2eAgInSc1d3	veliký
je	být	k5eAaImIp3nS	být
ostrov	ostrov	k1gInSc1	ostrov
Čedžu	Čedžu	k1gFnSc2	Čedžu
(	(	kIx(	(
<g/>
1	[number]	k4	1
845	[number]	k4	845
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
asi	asi	k9	asi
100	[number]	k4	100
km	km	kA	km
od	od	k7c2	od
jižního	jižní	k2eAgNnSc2d1	jižní
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
ostrově	ostrov	k1gInSc6	ostrov
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
vrchol	vrchol	k1gInSc4	vrchol
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
<g/>
,	,	kIx,	,
vyhaslá	vyhaslý	k2eAgFnSc1d1	vyhaslá
sopka	sopka	k1gFnSc1	sopka
Halla-san	Hallaan	k1gMnSc1	Halla-san
(	(	kIx(	(
<g/>
1	[number]	k4	1
950	[number]	k4	950
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
mírné	mírný	k2eAgNnSc1d1	mírné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
nastává	nastávat	k5eAaImIp3nS	nastávat
krátké	krátký	k2eAgNnSc4d1	krátké
období	období	k1gNnSc4	období
dešťů	dešť	k1gInPc2	dešť
<g/>
,	,	kIx,	,
zimy	zima	k1gFnPc1	zima
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
tuhé	tuhý	k2eAgNnSc1d1	tuhé
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
lednová	lednový	k2eAgFnSc1d1	lednová
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
Soulu	Soul	k1gInSc6	Soul
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
-7	-7	k4	-7
°	°	k?	°
<g/>
C	C	kA	C
do	do	k7c2	do
+1	+1	k4	+1
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
22	[number]	k4	22
°	°	k?	°
<g/>
C	C	kA	C
po	po	k7c6	po
29	[number]	k4	29
°	°	k?	°
<g/>
C.	C.	kA	C.
Zimní	zimní	k2eAgFnPc4d1	zimní
teploty	teplota	k1gFnPc4	teplota
jsou	být	k5eAaImIp3nP	být
nižší	nízký	k2eAgInPc1d2	nižší
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
srážek	srážka	k1gFnPc2	srážka
spadne	spadnout	k5eAaPmIp3nS	spadnout
v	v	k7c6	v
letním	letní	k2eAgNnSc6d1	letní
období	období	k1gNnSc6	období
dešťů	dešť	k1gInPc2	dešť
<g/>
.	.	kIx.	.
pohoří	pohořet	k5eAaPmIp3nP	pohořet
Diamantové	diamantový	k2eAgFnPc1d1	Diamantová
hory	hora	k1gFnPc1	hora
Hangang	Hanganga	k1gFnPc2	Hanganga
Kumgang	Kumganga	k1gFnPc2	Kumganga
Korejská	korejský	k2eAgFnSc1d1	Korejská
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
zřízením	zřízení	k1gNnSc7	zřízení
prezidentskou	prezidentský	k2eAgFnSc7d1	prezidentská
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
představitelem	představitel	k1gMnSc7	představitel
výkonné	výkonný	k2eAgFnSc2d1	výkonná
moci	moc	k1gFnSc2	moc
je	být	k5eAaImIp3nS	být
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
volený	volený	k2eAgInSc1d1	volený
ve	v	k7c6	v
všeobecných	všeobecný	k2eAgFnPc6d1	všeobecná
volbách	volba	k1gFnPc6	volba
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
každých	každý	k3xTgInPc2	každý
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
prezidentské	prezidentský	k2eAgFnPc1d1	prezidentská
volby	volba	k1gFnPc1	volba
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
nynější	nynější	k2eAgFnSc1d1	nynější
prezidentka	prezidentka	k1gFnSc1	prezidentka
Pak	pak	k6eAd1	pak
Kun-hje	Kunj	k1gFnSc2	Kun-hj
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
premiéra	premiér	k1gMnSc4	premiér
a	a	k8xC	a
celý	celý	k2eAgInSc4d1	celý
vládní	vládní	k2eAgInSc4d1	vládní
kabinet	kabinet	k1gInSc4	kabinet
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
musí	muset	k5eAaImIp3nS	muset
odsouhlasit	odsouhlasit	k5eAaPmF	odsouhlasit
parlament	parlament	k1gInSc1	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
složkou	složka	k1gFnSc7	složka
moci	moc	k1gFnSc2	moc
je	být	k5eAaImIp3nS	být
jednokomorový	jednokomorový	k2eAgInSc4d1	jednokomorový
parlament	parlament	k1gInSc4	parlament
Kukhwe	Kukhw	k1gFnSc2	Kukhw
(	(	kIx(	(
<g/>
국	국	k?	국
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
do	do	k7c2	do
korejského	korejský	k2eAgInSc2d1	korejský
parlamentu	parlament	k1gInSc2	parlament
(	(	kIx(	(
<g/>
299	[number]	k4	299
míst	místo	k1gNnPc2	místo
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
ve	v	k7c6	v
všeobecných	všeobecný	k2eAgFnPc6d1	všeobecná
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
každé	každý	k3xTgFnSc2	každý
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Parlamentní	parlamentní	k2eAgInSc1d1	parlamentní
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
několika	několik	k4yIc6	několik
politických	politický	k2eAgFnPc6d1	politická
stranách	strana	k1gFnPc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
ústava	ústava	k1gFnSc1	ústava
Korejské	korejský	k2eAgFnSc2d1	Korejská
republiky	republika	k1gFnSc2	republika
byla	být	k5eAaImAgFnS	být
schválena	schválen	k2eAgFnSc1d1	schválena
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
prošla	projít	k5eAaPmAgFnS	projít
pěti	pět	k4xCc7	pět
velkými	velký	k2eAgFnPc7d1	velká
změnami	změna	k1gFnPc7	změna
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
a	a	k8xC	a
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
změnou	změna	k1gFnSc7	změna
ústavy	ústava	k1gFnSc2	ústava
nastala	nastat	k5eAaPmAgFnS	nastat
i	i	k9	i
změna	změna	k1gFnSc1	změna
ve	v	k7c4	v
vládnutí	vládnutí	k1gNnSc4	vládnutí
a	a	k8xC	a
tyto	tento	k3xDgInPc1	tento
hraniční	hraniční	k2eAgInPc1d1	hraniční
roky	rok	k1gInPc1	rok
jsou	být	k5eAaImIp3nP	být
zároveň	zároveň	k6eAd1	zároveň
roky	rok	k1gInPc1	rok
následujících	následující	k2eAgInPc2d1	následující
"	"	kIx"	"
<g/>
republik	republika	k1gFnPc2	republika
<g/>
"	"	kIx"	"
-	-	kIx~	-
momentálně	momentálně	k6eAd1	momentálně
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tzv.	tzv.	kA	tzv.
6	[number]	k4	6
<g/>
.	.	kIx.	.
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
dodnes	dodnes	k6eAd1	dodnes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
sjednocení	sjednocení	k1gNnSc2	sjednocení
země	zem	k1gFnSc2	zem
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
významným	významný	k2eAgNnSc7d1	významné
politickým	politický	k2eAgNnSc7d1	politické
tématem	téma	k1gNnSc7	téma
<g/>
;	;	kIx,	;
přesto	přesto	k8xC	přesto
nebyla	být	k5eNaImAgFnS	být
podepsána	podepsat	k5eAaPmNgFnS	podepsat
se	s	k7c7	s
Severem	sever	k1gInSc7	sever
žádná	žádný	k3yNgFnSc1	žádný
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
historicky	historicky	k6eAd1	historicky
první	první	k4xOgNnSc4	první
jednání	jednání	k1gNnSc4	jednání
mezi	mezi	k7c7	mezi
Severní	severní	k2eAgFnSc7d1	severní
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc7d1	jižní
Koreou	Korea	k1gFnSc7	Korea
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Jih	jih	k1gInSc1	jih
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
sluneční	sluneční	k2eAgFnSc3d1	sluneční
politice	politika	k1gFnSc3	politika
<g/>
"	"	kIx"	"
směřující	směřující	k2eAgInSc1d1	směřující
k	k	k7c3	k
dohodě	dohoda	k1gFnSc3	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pravidelné	pravidelný	k2eAgInPc1d1	pravidelný
kontakty	kontakt	k1gInPc1	kontakt
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
opatrnému	opatrný	k2eAgNnSc3d1	opatrné
oteplování	oteplování	k1gNnSc3	oteplování
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgMnSc1d1	průměrný
obyvatel	obyvatel	k1gMnSc1	obyvatel
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
si	se	k3xPyFc3	se
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
překotné	překotný	k2eAgNnSc1d1	překotné
sjednocení	sjednocení	k1gNnSc1	sjednocení
se	s	k7c7	s
Severem	sever	k1gInSc7	sever
příliš	příliš	k6eAd1	příliš
nepřeje	přát	k5eNaImIp3nS	přát
<g/>
.	.	kIx.	.
</s>
<s>
Korejci	Korejec	k1gMnPc1	Korejec
si	se	k3xPyFc3	se
jsou	být	k5eAaImIp3nP	být
vědomi	vědom	k2eAgMnPc1d1	vědom
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nastala	nastat	k5eAaPmAgFnS	nastat
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
někdejšího	někdejší	k2eAgNnSc2d1	někdejší
východního	východní	k2eAgNnSc2d1	východní
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
a	a	k8xC	a
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
nutná	nutný	k2eAgFnSc1d1	nutná
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
pomoc	pomoc	k1gFnSc1	pomoc
Severu	sever	k1gInSc2	sever
po	po	k7c6	po
sjednocení	sjednocení	k1gNnSc6	sjednocení
značně	značně	k6eAd1	značně
zatěžovala	zatěžovat	k5eAaImAgFnS	zatěžovat
státní	státní	k2eAgInSc4d1	státní
rozpočet	rozpočet	k1gInSc4	rozpočet
i	i	k9	i
kapsy	kapsa	k1gFnSc2	kapsa
daňových	daňový	k2eAgMnPc2d1	daňový
poplatníků	poplatník	k1gMnPc2	poplatník
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
situaci	situace	k1gFnSc3	situace
v	v	k7c6	v
rozděleném	rozdělený	k2eAgNnSc6d1	rozdělené
Německu	Německo	k1gNnSc6	Německo
je	být	k5eAaImIp3nS	být
však	však	k9	však
rozdíl	rozdíl	k1gInSc1	rozdíl
daleko	daleko	k6eAd1	daleko
drastičtější	drastický	k2eAgMnSc1d2	drastičtější
<g/>
:	:	kIx,	:
HDP	HDP	kA	HDP
Západního	západní	k2eAgNnSc2d1	západní
Německa	Německo	k1gNnSc2	Německo
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
sjednocení	sjednocení	k1gNnSc2	sjednocení
třikrát	třikrát	k6eAd1	třikrát
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
v	v	k7c6	v
NDR	NDR	kA	NDR
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
HDP	HDP	kA	HDP
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
osmnáctkrát	osmnáctkrát	k6eAd1	osmnáctkrát
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
v	v	k7c6	v
KLDR	KLDR	kA	KLDR
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
Korejci	Korejec	k1gMnPc1	Korejec
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
vidí	vidět	k5eAaImIp3nS	vidět
sjednocení	sjednocení	k1gNnSc1	sjednocení
jako	jako	k8xC	jako
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
padesátileté	padesátiletý	k2eAgFnSc3d1	padesátiletá
izolaci	izolace	k1gFnSc3	izolace
mají	mít	k5eAaImIp3nP	mít
dnes	dnes	k6eAd1	dnes
severní	severní	k2eAgMnPc1d1	severní
Korejci	Korejec	k1gMnPc1	Korejec
podstatně	podstatně	k6eAd1	podstatně
jiný	jiný	k2eAgInSc4d1	jiný
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
zvyky	zvyk	k1gInPc4	zvyk
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
svébytným	svébytný	k2eAgInSc7d1	svébytný
národem	národ	k1gInSc7	národ
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
padesáti	padesát	k4xCc6	padesát
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
definitivně	definitivně	k6eAd1	definitivně
zpřetrhávají	zpřetrhávat	k5eAaImIp3nP	zpřetrhávat
i	i	k9	i
poslední	poslední	k2eAgNnPc4d1	poslední
pouta	pouto	k1gNnPc4	pouto
rozdělených	rozdělený	k2eAgFnPc2d1	rozdělená
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
země	zem	k1gFnPc1	zem
tak	tak	k6eAd1	tak
již	již	k6eAd1	již
spojuje	spojovat	k5eAaImIp3nS	spojovat
hlavně	hlavně	k9	hlavně
společná	společný	k2eAgFnSc1d1	společná
historie	historie	k1gFnSc1	historie
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
vnitrokorejských	vnitrokorejský	k2eAgInPc2d1	vnitrokorejský
sporů	spor	k1gInPc2	spor
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
i	i	k9	i
pojmenování	pojmenování	k1gNnSc1	pojmenování
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
obě	dva	k4xCgFnPc4	dva
země	zem	k1gFnPc4	zem
společný	společný	k2eAgInSc1d1	společný
název	název	k1gInSc1	název
Hanguk	Hanguka	k1gFnPc2	Hanguka
(	(	kIx(	(
<g/>
한	한	k?	한
<g/>
,	,	kIx,	,
země	země	k1gFnSc1	země
lidu	lid	k1gInSc2	lid
Han	Hana	k1gFnPc2	Hana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
potřeba	potřeba	k6eAd1	potřeba
rozlišit	rozlišit	k5eAaPmF	rozlišit
mezi	mezi	k7c7	mezi
jižní	jižní	k2eAgFnSc7d1	jižní
a	a	k8xC	a
severní	severní	k2eAgFnSc7d1	severní
částí	část	k1gFnSc7	část
<g/>
,	,	kIx,	,
použije	použít	k5eAaPmIp3nS	použít
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
Jižní	jižní	k2eAgFnSc4d1	jižní
Koreu	Korea	k1gFnSc4	Korea
název	název	k1gInSc1	název
Namhan	Namhan	k1gMnSc1	Namhan
(	(	kIx(	(
<g/>
남	남	k?	남
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgMnPc1d1	jižní
Han	Hana	k1gFnPc2	Hana
<g/>
)	)	kIx)	)
a	a	k8xC	a
pro	pro	k7c4	pro
KLDR	KLDR	kA	KLDR
Pukhan	Pukhan	k1gInSc4	Pukhan
(	(	kIx(	(
<g/>
북	북	k?	북
<g/>
,	,	kIx,	,
severní	severní	k2eAgMnPc1d1	severní
Han	Hana	k1gFnPc2	Hana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Koreji	Korea	k1gFnSc6	Korea
je	být	k5eAaImIp3nS	být
situace	situace	k1gFnSc1	situace
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
,	,	kIx,	,
severokorejci	severokorejec	k1gMnPc1	severokorejec
používají	používat	k5eAaImIp3nP	používat
starý	starý	k2eAgInSc4d1	starý
historický	historický	k2eAgInSc4d1	historický
název	název	k1gInSc4	název
Čoson	Čoson	k1gInSc1	Čoson
(	(	kIx(	(
<g/>
조	조	k?	조
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
pro	pro	k7c4	pro
upřesnění	upřesnění	k1gNnSc4	upřesnění
Pukčoson	Pukčosona	k1gFnPc2	Pukčosona
(	(	kIx(	(
<g/>
북	북	k?	북
<g/>
)	)	kIx)	)
a	a	k8xC	a
Namčoson	Namčoson	k1gInSc1	Namčoson
(	(	kIx(	(
<g/>
남	남	k?	남
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
někdo	někdo	k3yInSc1	někdo
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
Koreji	Korea	k1gFnSc6	Korea
obecně	obecně	k6eAd1	obecně
a	a	k8xC	a
použije	použít	k5eAaPmIp3nS	použít
název	název	k1gInSc4	název
Čoson	Čoson	k1gNnSc4	Čoson
<g/>
,	,	kIx,	,
vše	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
<g/>
,	,	kIx,	,
žádný	žádný	k3yNgInSc1	žádný
problém	problém	k1gInSc1	problém
nevznikne	vzniknout	k5eNaPmIp3nS	vzniknout
<g/>
,	,	kIx,	,
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
v	v	k7c6	v
nejhorším	zlý	k2eAgInSc6d3	Nejhorší
případě	případ	k1gInSc6	případ
sklidí	sklidit	k5eAaPmIp3nS	sklidit
salvu	salva	k1gFnSc4	salva
smíchu	smích	k1gInSc2	smích
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Koreji	Korea	k1gFnSc6	Korea
někdo	někdo	k3yInSc1	někdo
(	(	kIx(	(
<g/>
lhostejno	lhostejno	k6eAd1	lhostejno
kdo	kdo	k3yRnSc1	kdo
<g/>
)	)	kIx)	)
byť	byť	k8xS	byť
jen	jen	k9	jen
opovážil	opovážit	k5eAaPmAgMnS	opovážit
vyslovit	vyslovit	k5eAaPmF	vyslovit
slovo	slovo	k1gNnSc4	slovo
Hanguk	Hanguka	k1gFnPc2	Hanguka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zle	zle	k6eAd1	zle
<g/>
,	,	kIx,	,
nastane	nastat	k5eAaPmIp3nS	nastat
trapné	trapný	k2eAgNnSc1d1	trapné
ticho	ticho	k1gNnSc1	ticho
a	a	k8xC	a
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
se	se	k3xPyFc4	se
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
riziku	riziko	k1gNnSc3	riziko
fyzické	fyzický	k2eAgFnSc2d1	fyzická
inzultace	inzultace	k1gFnSc2	inzultace
<g/>
.	.	kIx.	.
</s>
<s>
Severokorejci	Severokorejec	k1gMnPc1	Severokorejec
tento	tento	k3xDgInSc4	tento
název	název	k1gInSc4	název
odmítají	odmítat	k5eAaImIp3nP	odmítat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
naposledy	naposledy	k6eAd1	naposledy
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
zemi	zem	k1gFnSc4	zem
používán	používat	k5eAaImNgInS	používat
v	v	k7c6	v
krátkém	krátký	k2eAgNnSc6d1	krátké
období	období	k1gNnSc6	období
před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
japonské	japonský	k2eAgFnSc2d1	japonská
okupace	okupace	k1gFnSc2	okupace
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
napjaté	napjatý	k2eAgFnSc2d1	napjatá
situace	situace	k1gFnSc2	situace
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
se	s	k7c7	s
Severní	severní	k2eAgFnSc7d1	severní
Koreou	Korea	k1gFnSc7	Korea
je	být	k5eAaImIp3nS	být
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
obzvláště	obzvláště	k6eAd1	obzvláště
citlivá	citlivý	k2eAgFnSc1d1	citlivá
na	na	k7c4	na
jakékoli	jakýkoli	k3yIgNnSc4	jakýkoli
dění	dění	k1gNnSc4	dění
související	související	k2eAgNnPc4d1	související
s	s	k7c7	s
maličkým	maličký	k2eAgNnSc7d1	maličké
souostrovím	souostroví	k1gNnSc7	souostroví
Tokdo	Tokdo	k1gNnSc1	Tokdo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
strategickém	strategický	k2eAgNnSc6d1	strategické
místě	místo	k1gNnSc6	místo
asi	asi	k9	asi
na	na	k7c6	na
půli	půle	k1gFnSc6	půle
cesty	cesta	k1gFnSc2	cesta
mezi	mezi	k7c7	mezi
Koreou	Korea	k1gFnSc7	Korea
a	a	k8xC	a
Japonskem	Japonsko	k1gNnSc7	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ostrovy	ostrov	k1gInPc4	ostrov
si	se	k3xPyFc3	se
dělají	dělat	k5eAaImIp3nP	dělat
nárok	nárok	k1gInSc4	nárok
obě	dva	k4xCgFnPc1	dva
země	zem	k1gFnPc1	zem
<g/>
:	:	kIx,	:
Korea	Korea	k1gFnSc1	Korea
jej	on	k3xPp3gMnSc4	on
klasifikuje	klasifikovat	k5eAaImIp3nS	klasifikovat
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
souostroví	souostroví	k1gNnSc2	souostroví
Ullung	Ullunga	k1gFnPc2	Ullunga
<g/>
,	,	kIx,	,
ležícího	ležící	k2eAgMnSc2d1	ležící
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Severní	severní	k2eAgFnSc2d1	severní
Kjŏ	Kjŏ	k1gFnSc2	Kjŏ
<g/>
;	;	kIx,	;
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Takešima	Takešimum	k1gNnSc2	Takešimum
zařazen	zařadit	k5eAaPmNgMnS	zařadit
jako	jako	k9	jako
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
Okinošima	Okinošimum	k1gNnSc2	Okinošimum
<g/>
,	,	kIx,	,
distrikt	distrikt	k1gInSc1	distrikt
Oki	Oki	k1gFnSc1	Oki
<g/>
,	,	kIx,	,
prefektura	prefektura	k1gFnSc1	prefektura
Šimane	Šiman	k1gMnSc5	Šiman
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
tohoto	tento	k3xDgInSc2	tento
konfliktu	konflikt	k1gInSc2	konflikt
je	být	k5eAaImIp3nS	být
určitá	určitý	k2eAgFnSc1d1	určitá
nejednoznačnost	nejednoznačnost	k1gFnSc1	nejednoznačnost
japonských	japonský	k2eAgFnPc2d1	japonská
poválečných	poválečný	k2eAgFnPc2d1	poválečná
smluv	smlouva	k1gFnPc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
jednoho	jeden	k4xCgNnSc2	jeden
speciálního	speciální	k2eAgNnSc2d1	speciální
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgNnSc2	jeden
speciálního	speciální	k2eAgNnSc2d1	speciální
autonomního	autonomní	k2eAgNnSc2d1	autonomní
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
šesti	šest	k4xCc2	šest
metropolitních	metropolitní	k2eAgNnPc2d1	metropolitní
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
osmi	osm	k4xCc2	osm
provincií	provincie	k1gFnPc2	provincie
a	a	k8xC	a
jedné	jeden	k4xCgFnSc2	jeden
speciální	speciální	k2eAgFnSc2d1	speciální
autonomní	autonomní	k2eAgFnSc2d1	autonomní
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Soul	Soul	k1gInSc1	Soul
(	(	kIx(	(
<g/>
서	서	k?	서
<g/>
;	;	kIx,	;
서	서	k?	서
<g/>
;	;	kIx,	;
Soul	Soul	k1gInSc1	Soul
tchukpjolši	tchukpjolsat	k5eAaPmIp1nSwK	tchukpjolsat
<g/>
)	)	kIx)	)
Sedžong	Sedžong	k1gInSc1	Sedžong
(	(	kIx(	(
<g/>
세	세	k?	세
<g/>
;	;	kIx,	;
世	世	k?	世
<g/>
;	;	kIx,	;
Sedžong	Sedžong	k1gMnSc1	Sedžong
tchukpjolčačchiši	tchukpjolčačchisat	k5eAaPmIp1nSwK	tchukpjolčačchisat
<g/>
)	)	kIx)	)
Inčchon	Inčchon	k1gNnSc1	Inčchon
(	(	kIx(	(
<g/>
인	인	k?	인
<g/>
;	;	kIx,	;
仁	仁	k?	仁
<g/>
;	;	kIx,	;
Inčchon	Inčchon	k1gMnSc1	Inčchon
kwangjokši	kwangjoksat	k5eAaPmIp1nSwK	kwangjoksat
<g/>
)	)	kIx)	)
Tegu	Teg	k2eAgFnSc4d1	Teg
(	(	kIx(	(
<g/>
대	대	k?	대
<g/>
;	;	kIx,	;
大	大	k?	大
<g/>
;	;	kIx,	;
Tegu	Teg	k2eAgFnSc4d1	Teg
kwangjokši	kwangjokše	k1gFnSc4	kwangjokše
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Ulsan	Ulsana	k1gFnPc2	Ulsana
(	(	kIx(	(
<g/>
울	울	k?	울
<g/>
;	;	kIx,	;
蔚	蔚	k?	蔚
<g/>
;	;	kIx,	;
Ulsan	Ulsan	k1gMnSc1	Ulsan
kwangjokši	kwangjoksat	k5eAaPmIp1nSwK	kwangjoksat
<g/>
)	)	kIx)	)
Pusan	Pusan	k1gInSc1	Pusan
(	(	kIx(	(
<g/>
부	부	k?	부
<g/>
;	;	kIx,	;
釜	釜	k?	釜
<g/>
;	;	kIx,	;
Pusan	Pusan	k1gMnSc1	Pusan
kwangjokši	kwangjoksat	k5eAaPmIp1nSwK	kwangjoksat
<g/>
)	)	kIx)	)
Kwangdžu	Kwangdžu	k1gFnSc1	Kwangdžu
(	(	kIx(	(
<g/>
광	광	k?	광
<g/>
;	;	kIx,	;
光	光	k?	光
<g/>
;	;	kIx,	;
Kwangdžu	Kwangdžu	k1gMnSc1	Kwangdžu
kwangjokši	kwangjoksat	k5eAaPmIp1nSwK	kwangjoksat
<g/>
)	)	kIx)	)
Tedžon	Tedžon	k1gNnSc1	Tedžon
(	(	kIx(	(
<g/>
대	대	k?	대
<g/>
;	;	kIx,	;
大	大	k?	大
<g/>
;	;	kIx,	;
<g />
.	.	kIx.	.
</s>
<s>
Tedžon	Tedžon	k1gInSc1	Tedžon
kwangjokši	kwangjoksat	k5eAaPmIp1nSwK	kwangjoksat
<g/>
)	)	kIx)	)
Kjonggi	Kjonggi	k1gNnSc7	Kjonggi
(	(	kIx(	(
<g/>
경	경	k?	경
<g/>
,	,	kIx,	,
京	京	k?	京
<g/>
)	)	kIx)	)
Kangwon	Kangwon	k1gMnSc1	Kangwon
(	(	kIx(	(
<g/>
강	강	k?	강
<g/>
,	,	kIx,	,
江	江	k?	江
<g/>
)	)	kIx)	)
Severní	severní	k2eAgInSc1d1	severní
Kjongsang	Kjongsang	k1gInSc1	Kjongsang
(	(	kIx(	(
<g/>
경	경	k?	경
<g/>
,	,	kIx,	,
慶	慶	k?	慶
<g/>
)	)	kIx)	)
Jižní	jižní	k2eAgInSc1d1	jižní
Kjongsang	Kjongsang	k1gInSc1	Kjongsang
(	(	kIx(	(
<g/>
경	경	k?	경
<g/>
,	,	kIx,	,
慶	慶	k?	慶
<g/>
)	)	kIx)	)
Jižní	jižní	k2eAgFnSc1d1	jižní
Čolla	Čolla	k1gFnSc1	Čolla
(	(	kIx(	(
<g/>
전	전	k?	전
<g/>
,	,	kIx,	,
全	全	k?	全
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Severní	severní	k2eAgFnSc1d1	severní
Čolla	Čolla	k1gFnSc1	Čolla
(	(	kIx(	(
<g/>
전	전	k?	전
<g/>
,	,	kIx,	,
全	全	k?	全
<g/>
)	)	kIx)	)
Jižní	jižní	k2eAgInSc1d1	jižní
Čchungčchong	Čchungčchong	k1gInSc1	Čchungčchong
(	(	kIx(	(
<g/>
충	충	k?	충
<g/>
,	,	kIx,	,
忠	忠	k?	忠
<g/>
)	)	kIx)	)
Severní	severní	k2eAgInSc1d1	severní
Čchungčchong	Čchungčchong	k1gInSc1	Čchungčchong
(	(	kIx(	(
<g/>
충	충	k?	충
<g/>
,	,	kIx,	,
忠	忠	k?	忠
<g/>
)	)	kIx)	)
Čedžu	Čedžu	k1gMnSc1	Čedžu
(	(	kIx(	(
<g/>
제	제	k?	제
<g/>
,	,	kIx,	,
濟	濟	k?	濟
<g/>
)	)	kIx)	)
Po	po	k7c6	po
konci	konec	k1gInSc6	konec
korejské	korejský	k2eAgFnSc2d1	Korejská
války	válka	k1gFnSc2	válka
patřila	patřit	k5eAaImAgFnS	patřit
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
k	k	k7c3	k
nejchudším	chudý	k2eAgInPc3d3	nejchudší
státům	stát	k1gInPc3	stát
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
činil	činit	k5eAaImAgInS	činit
79	[number]	k4	79
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
méně	málo	k6eAd2	málo
než	než	k8xS	než
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
jihoamerických	jihoamerický	k2eAgMnPc2d1	jihoamerický
a	a	k8xC	a
některých	některý	k3yIgFnPc6	některý
afrických	africký	k2eAgFnPc6d1	africká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
se	se	k3xPyFc4	se
chopil	chopit	k5eAaPmAgMnS	chopit
moci	moct	k5eAaImF	moct
diktátor	diktátor	k1gMnSc1	diktátor
Pak	pak	k6eAd1	pak
Čong-hui	Čongu	k1gFnSc2	Čong-hu
<g/>
.	.	kIx.	.
</s>
<s>
Provedl	provést	k5eAaPmAgInS	provést
zásadní	zásadní	k2eAgFnSc4d1	zásadní
reformy	reforma	k1gFnPc4	reforma
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
vybudovat	vybudovat	k5eAaPmF	vybudovat
silně	silně	k6eAd1	silně
exportní	exportní	k2eAgFnSc4d1	exportní
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k6eAd1	právě
jemu	on	k3xPp3gMnSc3	on
je	být	k5eAaImIp3nS	být
připisována	připisován	k2eAgFnSc1d1	připisována
proměna	proměna	k1gFnSc1	proměna
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
v	v	k7c4	v
asijského	asijský	k2eAgMnSc4d1	asijský
tygra	tygr	k1gMnSc4	tygr
<g/>
.	.	kIx.	.
</s>
<s>
Infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
financoval	financovat	k5eAaBmAgInS	financovat
především	především	k6eAd1	především
z	z	k7c2	z
japonských	japonský	k2eAgFnPc2d1	japonská
investic	investice	k1gFnPc2	investice
<g/>
,	,	kIx,	,
s	s	k7c7	s
čímž	což	k3yRnSc7	což
byla	být	k5eAaImAgFnS	být
spojena	spojit	k5eAaPmNgFnS	spojit
normalizace	normalizace	k1gFnSc1	normalizace
korejsko-japonských	korejskoaponský	k2eAgInPc2d1	korejsko-japonský
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
smlouvou	smlouva	k1gFnSc7	smlouva
o	o	k7c6	o
vzájemných	vzájemný	k2eAgInPc6d1	vzájemný
vztazích	vztah	k1gInPc6	vztah
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
také	také	k9	také
tomuto	tento	k3xDgInSc3	tento
ekonomickému	ekonomický	k2eAgInSc3d1	ekonomický
zázraku	zázrak	k1gInSc3	zázrak
také	také	k9	také
říká	říkat	k5eAaImIp3nS	říkat
Zázrak	zázrak	k1gInSc4	zázrak
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Han	Hana	k1gFnPc2	Hana
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
růst	růst	k1gInSc1	růst
byl	být	k5eAaImAgInS	být
poněkud	poněkud	k6eAd1	poněkud
zbrzděn	zbrzdit	k5eAaPmNgInS	zbrzdit
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
krizí	krize	k1gFnSc7	krize
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
HDP	HDP	kA	HDP
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
na	na	k7c4	na
kupní	kupní	k2eAgFnSc4d1	kupní
sílu	síla	k1gFnSc4	síla
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
30	[number]	k4	30
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
rovná	rovnat	k5eAaImIp3nS	rovnat
HDP	HDP	kA	HDP
zemí	zem	k1gFnPc2	zem
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
OECD	OECD	kA	OECD
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářské	hospodářský	k2eAgInPc1d1	hospodářský
ukazatele	ukazatel	k1gInPc1	ukazatel
HDP	HDP	kA	HDP
-	-	kIx~	-
1,55	[number]	k4	1,55
bilionu	bilion	k4xCgInSc2	bilion
USD	USD	kA	USD
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Růst	růst	k1gInSc1	růst
HDP	HDP	kA	HDP
-	-	kIx~	-
3,9	[number]	k4	3,9
<g/>
%	%	kIx~	%
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
HDP	HDP	kA	HDP
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
-	-	kIx~	-
31.800	[number]	k4	31.800
USD	USD	kA	USD
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
-	-	kIx~	-
3,3	[number]	k4	3,3
<g/>
%	%	kIx~	%
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Veřejný	veřejný	k2eAgInSc1d1	veřejný
dluh	dluh	k1gInSc1	dluh
-	-	kIx~	-
23,7	[number]	k4	23,7
<g/>
%	%	kIx~	%
<g />
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Inflace	inflace	k1gFnSc1	inflace
-	-	kIx~	-
4,5	[number]	k4	4,5
<g/>
%	%	kIx~	%
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Hodnota	hodnota	k1gFnSc1	hodnota
exportu	export	k1gInSc2	export
-	-	kIx~	-
466,3	[number]	k4	466,3
mld.	mld.	k?	mld.
USD	USD	kA	USD
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Hodnota	hodnota	k1gFnSc1	hodnota
importu	import	k1gInSc2	import
-	-	kIx~	-
417,9	[number]	k4	417,9
mld.	mld.	k?	mld.
USD	USD	kA	USD
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
strojírenský	strojírenský	k2eAgInSc1d1	strojírenský
automobilový	automobilový	k2eAgInSc1d1	automobilový
(	(	kIx(	(
<g/>
Hyundai	Hyunda	k1gFnPc1	Hyunda
<g/>
,	,	kIx,	,
Kia	Kia	k1gMnSc1	Kia
<g/>
,	,	kIx,	,
Ssang-Yong	Ssang-Yong	k1gMnSc1	Ssang-Yong
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Daewoo	Daewoo	k1gNnSc1	Daewoo
<g/>
)	)	kIx)	)
textilní	textilní	k2eAgInSc1d1	textilní
potravinářský	potravinářský	k2eAgInSc1d1	potravinářský
(	(	kIx(	(
<g/>
Jinro	Jinro	k1gNnSc1	Jinro
Distillers	Distillers	k1gInSc1	Distillers
Co	co	k3yQnSc1	co
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Ltd	ltd	kA	ltd
<g/>
)	)	kIx)	)
chemický	chemický	k2eAgInSc4d1	chemický
elektrotechnika	elektrotechnik	k1gMnSc4	elektrotechnik
(	(	kIx(	(
<g/>
Samsung	Samsung	kA	Samsung
<g/>
,	,	kIx,	,
LG	LG	kA	LG
<g/>
,	,	kIx,	,
iRiver	iRiver	k1gMnSc1	iRiver
<g/>
,	,	kIx,	,
iAudio	iAudio	k1gMnSc1	iAudio
(	(	kIx(	(
<g/>
Cowon	Cowon	k1gMnSc1	Cowon
<g/>
))	))	k?	))
wolfram	wolfram	k1gInSc1	wolfram
železná	železný	k2eAgFnSc1d1	železná
ruda	ruda	k1gFnSc1	ruda
tuha	tuha	k1gFnSc1	tuha
zlato	zlato	k1gNnSc1	zlato
černé	černá	k1gFnSc2	černá
uhlí	uhlí	k1gNnSc2	uhlí
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Chudoba	Chudoba	k1gMnSc1	Chudoba
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Chudobou	chudoba	k1gFnSc7	chudoba
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
podle	podle	k7c2	podle
oficiálního	oficiální	k2eAgInSc2d1	oficiální
odhadu	odhad	k1gInSc2	odhad
postiženo	postihnout	k5eAaPmNgNnS	postihnout
asi	asi	k9	asi
15	[number]	k4	15
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
OECD	OECD	kA	OECD
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2012	[number]	k4	2012
poznamenala	poznamenat	k5eAaPmAgFnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
zlepšení	zlepšení	k1gNnSc3	zlepšení
sociální	sociální	k2eAgFnSc2d1	sociální
soudržnosti	soudržnost	k1gFnSc2	soudržnost
snižováním	snižování	k1gNnSc7	snižování
nerovnosti	nerovnost	k1gFnSc2	nerovnost
a	a	k8xC	a
relativní	relativní	k2eAgFnSc2d1	relativní
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
klíčových	klíčový	k2eAgInPc2d1	klíčový
problémů	problém	k1gInPc2	problém
<g/>
,	,	kIx,	,
kterým	který	k3yQgInPc3	který
čelí	čelit	k5eAaImIp3nS	čelit
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Chudoba	chudoba	k1gFnSc1	chudoba
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
především	především	k9	především
starší	starý	k2eAgMnSc1d2	starší
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
polovina	polovina	k1gFnSc1	polovina
starších	starý	k2eAgMnPc2d2	starší
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
relativní	relativní	k2eAgFnSc6d1	relativní
chudobě	chudoba	k1gFnSc6	chudoba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
podíl	podíl	k1gInSc1	podíl
mezi	mezi	k7c7	mezi
zeměmi	zem	k1gFnPc7	zem
OECD	OECD	kA	OECD
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
jihokorejské	jihokorejský	k2eAgFnPc1d1	jihokorejská
důchodkyně	důchodkyně	k1gFnPc1	důchodkyně
si	se	k3xPyFc3	se
musí	muset	k5eAaImIp3nP	muset
přivydělávat	přivydělávat	k5eAaImF	přivydělávat
prostitucí	prostituce	k1gFnSc7	prostituce
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
99	[number]	k4	99
<g/>
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
Korejské	korejský	k2eAgFnSc2d1	Korejská
republiky	republika	k1gFnSc2	republika
tvoří	tvořit	k5eAaImIp3nP	tvořit
Korejci	Korejec	k1gMnPc1	Korejec
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
počtem	počet	k1gInSc7	počet
50,2	[number]	k4	50,2
milionů	milion	k4xCgInPc2	milion
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
26	[number]	k4	26
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
hustotou	hustota	k1gFnSc7	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
501	[number]	k4	501
osob	osoba	k1gFnPc2	osoba
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejhustěji	husto	k6eAd3	husto
zalidněných	zalidněný	k2eAgFnPc2d1	zalidněná
zemí	zem	k1gFnPc2	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
měnily	měnit	k5eAaImAgFnP	měnit
všechny	všechen	k3xTgFnPc1	všechen
sféry	sféra	k1gFnPc1	sféra
korejské	korejský	k2eAgFnSc2d1	Korejská
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
prudce	prudko	k6eAd1	prudko
měnilo	měnit	k5eAaImAgNnS	měnit
i	i	k9	i
složení	složení	k1gNnSc4	složení
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
agrární	agrární	k2eAgFnSc1d1	agrární
země	země	k1gFnSc1	země
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
zaměstnaných	zaměstnaný	k2eAgMnPc2d1	zaměstnaný
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
až	až	k6eAd1	až
60	[number]	k4	60
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městech	město	k1gNnPc6	město
žilo	žít	k5eAaImAgNnS	žít
20	[number]	k4	20
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
na	na	k7c4	na
vysoce	vysoce	k6eAd1	vysoce
urbanizovanou	urbanizovaný	k2eAgFnSc4d1	urbanizovaná
(	(	kIx(	(
<g/>
ve	v	k7c6	v
městech	město	k1gNnPc6	město
dnes	dnes	k6eAd1	dnes
žije	žít	k5eAaImIp3nS	žít
75	[number]	k4	75
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
zemi	zem	k1gFnSc4	zem
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
6,4	[number]	k4	6,4
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
na	na	k7c6	na
postindustriální	postindustriální	k2eAgFnSc6d1	postindustriální
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vysoký	vysoký	k2eAgInSc4d1	vysoký
stupeň	stupeň	k1gInSc4	stupeň
urbanizace	urbanizace	k1gFnSc2	urbanizace
jsou	být	k5eAaImIp3nP	být
zodpovědné	zodpovědný	k2eAgMnPc4d1	zodpovědný
zejména	zejména	k9	zejména
tyto	tento	k3xDgInPc1	tento
faktory	faktor	k1gInPc1	faktor
<g/>
:	:	kIx,	:
Situace	situace	k1gFnSc1	situace
po	po	k7c6	po
korejské	korejský	k2eAgFnSc6d1	Korejská
válce	válka	k1gFnSc6	válka
-	-	kIx~	-
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
uprchlíků	uprchlík	k1gMnPc2	uprchlík
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
Od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
prudký	prudký	k2eAgInSc4d1	prudký
průmyslový	průmyslový	k2eAgInSc4d1	průmyslový
rozvoj	rozvoj	k1gInSc4	rozvoj
soustředěný	soustředěný	k2eAgInSc4d1	soustředěný
ve	v	k7c6	v
městech	město	k1gNnPc6	město
V	v	k7c4	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
hnutí	hnutí	k1gNnSc2	hnutí
"	"	kIx"	"
<g/>
Nová	nový	k2eAgFnSc1d1	nová
vesnice	vesnice	k1gFnSc1	vesnice
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Semaul	Semaul	k1gInSc1	Semaul
undong	undong	k1gInSc1	undong
새	새	k?	새
운	운	k?	운
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Největší	veliký	k2eAgFnPc4d3	veliký
města	město	k1gNnSc2	město
mající	mající	k2eAgInSc1d1	mající
více	hodně	k6eAd2	hodně
než	než	k8xS	než
milion	milion	k4xCgInSc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
jsou	být	k5eAaImIp3nP	být
Soul	Soul	k1gInSc4	Soul
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
11	[number]	k4	11
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pusan	Pusan	k1gMnSc1	Pusan
(	(	kIx(	(
<g/>
3,7	[number]	k4	3,7
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Inčchon	Inčchon	k1gMnSc1	Inčchon
(	(	kIx(	(
<g/>
2,5	[number]	k4	2,5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tegu	Tega	k1gFnSc4	Tega
(	(	kIx(	(
<g/>
2,5	[number]	k4	2,5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ulsan	Ulsan	k1gMnSc1	Ulsan
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tedžon	Tedžon	k1gMnSc1	Tedžon
(	(	kIx(	(
<g/>
1,4	[number]	k4	1,4
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kwangdžu	Kwangdžu	k1gMnSc1	Kwangdžu
(	(	kIx(	(
<g/>
1,4	[number]	k4	1,4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
60	[number]	k4	60
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
charakterizována	charakterizovat	k5eAaBmNgFnS	charakterizovat
vysokým	vysoký	k2eAgInSc7d1	vysoký
přirozeným	přirozený	k2eAgInSc7d1	přirozený
přírůstkem	přírůstek	k1gInSc7	přírůstek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
2,6	[number]	k4	2,6
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Klesající	klesající	k2eAgInSc1d1	klesající
trend	trend	k1gInSc1	trend
posledních	poslední	k2eAgNnPc2d1	poslední
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
pokles	pokles	k1gInSc1	pokles
na	na	k7c4	na
1	[number]	k4	1
<g/>
%	%	kIx~	%
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
nejvíce	nejvíce	k6eAd1	nejvíce
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přirozený	přirozený	k2eAgInSc1d1	přirozený
přírůstek	přírůstek	k1gInSc1	přírůstek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
pouze	pouze	k6eAd1	pouze
0,62	[number]	k4	0,62
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
ohrožení	ohrožení	k1gNnSc4	ohrožení
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Náboženství	náboženství	k1gNnSc2	náboženství
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Náboženská	náboženský	k2eAgFnSc1d1	náboženská
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
je	být	k5eAaImIp3nS	být
složitá	složitý	k2eAgFnSc1d1	složitá
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znesnadňuje	znesnadňovat	k5eAaImIp3nS	znesnadňovat
její	její	k3xOp3gInSc4	její
popis	popis	k1gInSc4	popis
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgNnPc1d1	tradiční
asijská	asijský	k2eAgNnPc1d1	asijské
náboženství	náboženství	k1gNnPc1	náboženství
si	se	k3xPyFc3	se
dosud	dosud	k6eAd1	dosud
uchovala	uchovat	k5eAaPmAgFnS	uchovat
určitý	určitý	k2eAgInSc4d1	určitý
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ale	ale	k9	ale
zřetelně	zřetelně	k6eAd1	zřetelně
upadá	upadat	k5eAaPmIp3nS	upadat
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
křesťanství	křesťanství	k1gNnSc2	křesťanství
a	a	k8xC	a
agnosticismu	agnosticismus	k1gInSc2	agnosticismus
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
statistik	statistika	k1gFnPc2	statistika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
sestavených	sestavený	k2eAgFnPc2d1	sestavená
vládou	vláda	k1gFnSc7	vláda
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
polovina	polovina	k1gFnSc1	polovina
obyvatel	obyvatel	k1gMnPc2	obyvatel
nehlásí	hlásit	k5eNaImIp3nS	hlásit
k	k	k7c3	k
žádnému	žádný	k3yNgNnSc3	žádný
náboženství	náboženství	k1gNnSc3	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Početně	početně	k6eAd1	početně
nejvíce	nejvíce	k6eAd1	nejvíce
zastoupeným	zastoupený	k2eAgNnSc7d1	zastoupené
náboženstvím	náboženství	k1gNnSc7	náboženství
je	být	k5eAaImIp3nS	být
křesťanství	křesťanství	k1gNnSc1	křesťanství
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
necelých	celý	k2eNgNnPc2d1	necelé
30	[number]	k4	30
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
asi	asi	k9	asi
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
protestantů	protestant	k1gMnPc2	protestant
a	a	k8xC	a
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
katolíků	katolík	k1gMnPc2	katolík
<g/>
)	)	kIx)	)
a	a	k8xC	a
až	až	k9	až
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
následuje	následovat	k5eAaImIp3nS	následovat
tradičnější	tradiční	k2eAgInSc1d2	tradičnější
buddhismus	buddhismus	k1gInSc1	buddhismus
(	(	kIx(	(
<g/>
necelých	celý	k2eNgInPc2d1	necelý
23	[number]	k4	23
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podíly	podíl	k1gInPc1	podíl
příslušníků	příslušník	k1gMnPc2	příslušník
jiných	jiný	k2eAgNnPc2d1	jiné
náboženství	náboženství	k1gNnPc2	náboženství
už	už	k6eAd1	už
nepřesahují	přesahovat	k5eNaImIp3nP	přesahovat
0,5	[number]	k4	0,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
)	)	kIx)	)
-	-	kIx~	-
začátek	začátek	k1gInSc1	začátek
nového	nový	k2eAgInSc2d1	nový
roku	rok	k1gInSc2	rok
podle	podle	k7c2	podle
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
Sollal	Sollal	k1gFnSc2	Sollal
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
den	den	k1gInSc1	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
měsíce	měsíc	k1gInSc2	měsíc
-	-	kIx~	-
začátek	začátek	k1gInSc1	začátek
nového	nový	k2eAgInSc2d1	nový
roku	rok	k1gInSc2	rok
podle	podle	k7c2	podle
lunárního	lunární	k2eAgInSc2d1	lunární
kalendáře	kalendář	k1gInSc2	kalendář
-	-	kIx~	-
tradiční	tradiční	k2eAgInSc1d1	tradiční
začátek	začátek	k1gInSc1	začátek
roku	rok	k1gInSc2	rok
ve	v	k7c6	v
Východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
Samil	Samil	k1gMnSc1	Samil
undong	undong	k1gMnSc1	undong
(	(	kIx(	(
<g/>
삼	삼	k?	삼
운	운	k?	운
<g/>
)	)	kIx)	)
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
-	-	kIx~	-
oslava	oslava	k1gFnSc1	oslava
výročí	výročí	k1gNnSc2	výročí
Hnutí	hnutí	k1gNnSc2	hnutí
nezávislosti	nezávislost	k1gFnSc2	nezávislost
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
poloostrově	poloostrov	k1gInSc6	poloostrov
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
lidové	lidový	k2eAgNnSc1d1	lidové
hnutí	hnutí	k1gNnSc1	hnutí
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
Den	den	k1gInSc1	den
sázení	sázení	k1gNnSc2	sázení
stromů	strom	k1gInPc2	strom
Šikmogil	Šikmogila	k1gFnPc2	Šikmogila
(	(	kIx(	(
<g/>
식	식	k?	식
<g/>
)	)	kIx)	)
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
-	-	kIx~	-
v	v	k7c6	v
Korejské	korejský	k2eAgFnSc6d1	Korejská
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
hromadně	hromadně	k6eAd1	hromadně
<g />
.	.	kIx.	.
</s>
<s>
sázejí	sázet	k5eAaImIp3nP	sázet
stromy	strom	k1gInPc1	strom
Den	dna	k1gFnPc2	dna
dětí	dítě	k1gFnPc2	dítě
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
-	-	kIx~	-
velké	velký	k2eAgFnPc1d1	velká
oslavy	oslava	k1gFnPc1	oslava
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
Svátek	Svátek	k1gMnSc1	Svátek
Tano	Tano	k1gMnSc1	Tano
(	(	kIx(	(
<g/>
단	단	k?	단
<g/>
)	)	kIx)	)
Buddhovy	Buddhův	k2eAgFnPc4d1	Buddhova
narozeniny	narozeniny	k1gFnPc4	narozeniny
(	(	kIx(	(
<g/>
석	석	k?	석
탄	탄	k?	탄
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
den	den	k1gInSc1	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
měsíce	měsíc	k1gInSc2	měsíc
<g/>
)	)	kIx)	)
-	-	kIx~	-
podle	podle	k7c2	podle
lunárního	lunární	k2eAgInSc2d1	lunární
kalendáře	kalendář	k1gInSc2	kalendář
-	-	kIx~	-
ulice	ulice	k1gFnSc1	ulice
<g />
.	.	kIx.	.
</s>
<s>
jsou	být	k5eAaImIp3nP	být
vyzdobeny	vyzdoben	k2eAgFnPc1d1	vyzdobena
<g/>
,	,	kIx,	,
buddhisté	buddhista	k1gMnPc1	buddhista
oslavují	oslavovat	k5eAaImIp3nP	oslavovat
v	v	k7c6	v
svatyních	svatyně	k1gFnPc6	svatyně
Památka	památka	k1gFnSc1	památka
padlých	padlý	k1gMnPc2	padlý
v	v	k7c6	v
boji	boj	k1gInSc6	boj
(	(	kIx(	(
<g/>
현	현	k?	현
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
)	)	kIx)	)
-	-	kIx~	-
celonárodní	celonárodní	k2eAgFnSc1d1	celonárodní
slavnost	slavnost	k1gFnSc1	slavnost
oslavy	oslava	k1gFnSc2	oslava
památky	památka	k1gFnSc2	památka
všech	všecek	k3xTgMnPc2	všecek
Korejců	Korejec	k1gMnPc2	Korejec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
padli	padnout	k5eAaImAgMnP	padnout
v	v	k7c6	v
boji	boj	k1gInSc6	boj
za	za	k7c4	za
vlast	vlast	k1gFnSc4	vlast
Den	den	k1gInSc4	den
ústavy	ústava	k1gFnSc2	ústava
Korejské	korejský	k2eAgFnSc2d1	Korejská
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
제	제	k?	제
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
)	)	kIx)	)
-	-	kIx~	-
výročí	výročí	k1gNnSc4	výročí
první	první	k4xOgFnSc2	první
ústavy	ústava	k1gFnSc2	ústava
Korejské	korejský	k2eAgFnSc2d1	Korejská
republiky	republika	k1gFnSc2	republika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
Den	den	k1gInSc1	den
osvobození	osvobození	k1gNnSc2	osvobození
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
광	광	k?	광
<g/>
)	)	kIx)	)
-	-	kIx~	-
výročí	výročí	k1gNnSc1	výročí
osvobození	osvobození	k1gNnSc2	osvobození
zpod	zpod	k7c2	zpod
čtyřicetileté	čtyřicetiletý	k2eAgFnSc2d1	čtyřicetiletá
japonské	japonský	k2eAgFnSc2d1	japonská
okupace	okupace	k1gFnSc2	okupace
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
a	a	k8xC	a
výročí	výročí	k1gNnSc4	výročí
vzniku	vznik	k1gInSc2	vznik
Korejské	korejský	k2eAgFnSc2d1	Korejská
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g />
.	.	kIx.	.
</s>
<s>
1948	[number]	k4	1948
Den	den	k1gInSc1	den
díkůvzdání	díkůvzdání	k1gNnSc2	díkůvzdání
Čchusok	Čchusok	k1gInSc1	Čchusok
(	(	kIx(	(
<g/>
추	추	k?	추
<g/>
)	)	kIx)	)
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
den	den	k1gInSc1	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
měsíce	měsíc	k1gInSc2	měsíc
-	-	kIx~	-
podle	podle	k7c2	podle
lunárního	lunární	k2eAgInSc2d1	lunární
kalendáře	kalendář	k1gInSc2	kalendář
-	-	kIx~	-
největší	veliký	k2eAgInSc1d3	veliký
a	a	k8xC	a
nejtradičnější	tradiční	k2eAgInSc1d3	nejtradičnější
korejský	korejský	k2eAgInSc1d1	korejský
svátek	svátek	k1gInSc1	svátek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
Korejci	Korejec	k1gMnPc1	Korejec
vrací	vracet	k5eAaImIp3nP	vracet
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
rodiště	rodiště	k1gNnSc2	rodiště
<g/>
,	,	kIx,	,
návštěvují	návštěvovat	k5eAaImIp3nP	návštěvovat
hroby	hrob	k1gInPc4	hrob
předků	předek	k1gInPc2	předek
<g/>
,	,	kIx,	,
vzdávají	vzdávat	k5eAaImIp3nP	vzdávat
jim	on	k3xPp3gMnPc3	on
vděk	vděk	k1gInSc1	vděk
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
<g />
.	.	kIx.	.
</s>
<s>
rodina	rodina	k1gFnSc1	rodina
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
oslavuje	oslavovat	k5eAaImIp3nS	oslavovat
Den	den	k1gInSc1	den
založení	založení	k1gNnSc2	založení
národa	národ	k1gInSc2	národ
(	(	kIx(	(
<g/>
개	개	k?	개
철	철	k?	철
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
)	)	kIx)	)
-	-	kIx~	-
podle	podle	k7c2	podle
mýtu	mýtus	k1gInSc2	mýtus
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2333	[number]	k4	2333
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Tangun	Tangun	k1gMnSc1	Tangun
založil	založit	k5eAaPmAgMnS	založit
korejský	korejský	k2eAgInSc4d1	korejský
národ	národ	k1gInSc4	národ
Ježíšovy	Ježíšův	k2eAgFnPc4d1	Ježíšova
narozeniny	narozeniny	k1gFnPc4	narozeniny
(	(	kIx(	(
<g/>
성	성	k?	성
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
)	)	kIx)	)
-	-	kIx~	-
oslava	oslava	k1gFnSc1	oslava
Vánoc	Vánoce	k1gFnPc2	Vánoce
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
slaví	slavit	k5eAaImIp3nP	slavit
je	on	k3xPp3gInPc4	on
všichni	všechen	k3xTgMnPc1	všechen
Korejci	Korejec	k1gMnPc1	Korejec
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
náboženství	náboženství	k1gNnSc4	náboženství
ECKERT	ECKERT	kA	ECKERT
<g/>
,	,	kIx,	,
J	J	kA	J
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Koreje	Korea	k1gFnSc2	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
580	[number]	k4	580
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Dvanáctkrát	dvanáctkrát	k6eAd1	dvanáctkrát
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
<g/>
:	:	kIx,	:
velký	velký	k2eAgInSc1d1	velký
audio	audio	k2eAgInSc1d1	audio
i	i	k8xC	i
obrazový	obrazový	k2eAgInSc1d1	obrazový
seriál	seriál	k1gInSc1	seriál
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Články	článek	k1gInPc1	článek
o	o	k7c6	o
Jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
Severní	severní	k2eAgFnSc6d1	severní
Koreji	Korea	k1gFnSc6	Korea
<g/>
,	,	kIx,	,
historie	historie	k1gFnSc1	historie
<g/>
,	,	kIx,	,
politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
zajímavosti	zajímavost	k1gFnSc6	zajímavost
Reportáž	reportáž	k1gFnSc1	reportáž
Korea	Korea	k1gFnSc1	Korea
je	být	k5eAaImIp3nS	být
most	most	k1gInSc1	most
Základní	základní	k2eAgFnSc1d1	základní
informace	informace	k1gFnSc1	informace
o	o	k7c6	o
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
ve	v	k7c6	v
slovenském	slovenský	k2eAgInSc6d1	slovenský
jazyce	jazyk	k1gInSc6	jazyk
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
z	z	k7c2	z
cesty	cesta	k1gFnSc2	cesta
po	po	k7c6	po
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
South	Southa	k1gFnPc2	Southa
Korea	Korea	k1gFnSc1	Korea
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gMnPc2	International
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
South	South	k1gInSc1	South
Korea	Korea	k1gFnSc1	Korea
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc4	Freedom
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
South	South	k1gInSc1	South
Korea	Korea	k1gFnSc1	Korea
Country	country	k2eAgInPc2d1	country
Report	report	k1gInSc4	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bureau	Burea	k1gMnSc3	Burea
of	of	k?	of
East	East	k1gMnSc1	East
Asian	Asian	k1gMnSc1	Asian
and	and	k?	and
Pacific	Pacific	k1gMnSc1	Pacific
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
South	South	k1gInSc1	South
Korea	Korea	k1gFnSc1	Korea
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
South	South	k1gInSc1	South
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
REV	REV	kA	REV
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Library	Librar	k1gInPc1	Librar
of	of	k?	of
Congress	Congress	k1gInSc1	Congress
<g/>
.	.	kIx.	.
</s>
<s>
Country	country	k2eAgFnPc1d1	country
Profile	profil	k1gInSc5	profil
<g/>
:	:	kIx,	:
South	South	k1gInSc1	South
Korea	Korea	k1gFnSc1	Korea
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Soulu	Soul	k1gInSc6	Soul
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Korejská	korejský	k2eAgFnSc1d1	Korejská
republika	republika	k1gFnSc1	republika
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
29	[number]	k4	29
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
HAHN	HAHN	kA	HAHN
<g/>
,	,	kIx,	,
Bae-ho	Bae-	k1gMnSc2	Bae-
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
South	South	k1gInSc1	South
Korea	Korea	k1gFnSc1	Korea
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
