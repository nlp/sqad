<s>
Stonek	stonek	k1gInSc1	stonek
(	(	kIx(	(
<g/>
cauloma	cauloma	k1gFnSc1	cauloma
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nadzemní	nadzemní	k2eAgInSc1d1	nadzemní
rostlinný	rostlinný	k2eAgInSc1d1	rostlinný
orgán	orgán	k1gInSc1	orgán
spojující	spojující	k2eAgInSc1d1	spojující
kořen	kořen	k1gInSc4	kořen
s	s	k7c7	s
listy	list	k1gInPc7	list
a	a	k8xC	a
květy	květ	k1gInPc7	květ
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
neomezeného	omezený	k2eNgInSc2d1	neomezený
růstu	růst	k1gInSc2	růst
<g/>
,	,	kIx,	,
rozčleněný	rozčleněný	k2eAgInSc4d1	rozčleněný
na	na	k7c4	na
uzliny	uzlina	k1gFnPc4	uzlina
(	(	kIx(	(
<g/>
nody	noda	k1gFnPc4	noda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
listy	list	k1gInPc1	list
a	a	k8xC	a
články	článek	k1gInPc1	článek
<g/>
)	)	kIx)	)
a	a	k8xC	a
internodia	internodium	k1gNnSc2	internodium
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stonek	stonek	k1gInSc1	stonek
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stonek	stonek	k1gInSc1	stonek
s	s	k7c7	s
listy	list	k1gInPc7	list
se	se	k3xPyFc4	se
dohromady	dohromady	k6eAd1	dohromady
nazývají	nazývat	k5eAaImIp3nP	nazývat
prýt	prýt	k1gInSc4	prýt
<g/>
.	.	kIx.	.
</s>
<s>
Stonek	stonek	k1gInSc1	stonek
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
orgánem	orgán	k1gInSc7	orgán
velké	velký	k2eAgFnSc2d1	velká
většiny	většina	k1gFnSc2	většina
cévnatých	cévnatý	k2eAgFnPc2d1	cévnatá
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
takový	takový	k3xDgInSc1	takový
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
některé	některý	k3yIgFnPc4	některý
zásadní	zásadní	k2eAgFnPc4d1	zásadní
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Nese	nést	k5eAaImIp3nS	nést
orgány	orgán	k1gInPc4	orgán
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
(	(	kIx(	(
<g/>
listy	list	k1gInPc1	list
<g/>
)	)	kIx)	)
a	a	k8xC	a
orgány	orgán	k1gInPc1	orgán
pohlavního	pohlavní	k2eAgNnSc2d1	pohlavní
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
(	(	kIx(	(
<g/>
květy	květ	k1gInPc4	květ
<g/>
,	,	kIx,	,
plody	plod	k1gInPc4	plod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stonek	stonek	k1gInSc1	stonek
dále	daleko	k6eAd2	daleko
rozvádí	rozvádět	k5eAaImIp3nS	rozvádět
roztoky	roztok	k1gInPc4	roztok
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
z	z	k7c2	z
kořenů	kořen	k1gInPc2	kořen
do	do	k7c2	do
listů	list	k1gInPc2	list
a	a	k8xC	a
organické	organický	k2eAgInPc1d1	organický
asimiláty	asimilát	k1gInPc1	asimilát
z	z	k7c2	z
listů	list	k1gInPc2	list
do	do	k7c2	do
růstových	růstový	k2eAgNnPc2d1	růstové
pletiv	pletivo	k1gNnPc2	pletivo
<g/>
.	.	kIx.	.
</s>
<s>
Nejméně	málo	k6eAd3	málo
důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
i	i	k9	i
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
růst	růst	k1gInSc4	růst
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
,	,	kIx,	,
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
podílejí	podílet	k5eAaImIp3nP	podílet
dělivá	dělivý	k2eAgNnPc1d1	dělivé
pletiva	pletivo	k1gNnPc1	pletivo
<g/>
,	,	kIx,	,
umístěná	umístěný	k2eAgNnPc1d1	umístěné
ve	v	k7c6	v
vzrostném	vzrostný	k2eAgInSc6d1	vzrostný
vrcholu	vrchol	k1gInSc6	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Stonek	stonek	k1gInSc1	stonek
také	také	k9	také
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
orientovat	orientovat	k5eAaBmF	orientovat
listy	list	k1gInPc4	list
ke	k	k7c3	k
světlu	světlo	k1gNnSc3	světlo
a	a	k8xC	a
vyzdvihuje	vyzdvihovat	k5eAaImIp3nS	vyzdvihovat
květy	květ	k1gInPc4	květ
nad	nad	k7c4	nad
ostatní	ostatní	k2eAgFnPc4d1	ostatní
rostlinné	rostlinný	k2eAgFnPc4d1	rostlinná
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
mají	mít	k5eAaImIp3nP	mít
stonky	stonek	k1gInPc4	stonek
i	i	k8xC	i
další	další	k2eAgFnPc4d1	další
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
významné	významný	k2eAgFnPc4d1	významná
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgInPc4d1	mladý
stonky	stonek	k1gInPc4	stonek
s	s	k7c7	s
chlorenchymem	chlorenchymum	k1gNnSc7	chlorenchymum
pod	pod	k7c7	pod
pokožkou	pokožka	k1gFnSc7	pokožka
mají	mít	k5eAaImIp3nP	mít
například	například	k6eAd1	například
funkci	funkce	k1gFnSc4	funkce
asimilační	asimilační	k2eAgFnSc4d1	asimilační
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
modifikace	modifikace	k1gFnPc1	modifikace
stonku	stonek	k1gInSc2	stonek
mají	mít	k5eAaImIp3nP	mít
zásobní	zásobní	k2eAgFnSc4d1	zásobní
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
Dužnaté	dužnatý	k2eAgFnSc2d1	dužnatá
stonky	stonka	k1gFnSc2	stonka
Lodyha	lodyha	k1gFnSc1	lodyha
(	(	kIx(	(
<g/>
caulis	caulis	k1gInSc1	caulis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylinný	bylinný	k2eAgInSc1d1	bylinný
stonek	stonek	k1gInSc1	stonek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nese	nést	k5eAaImIp3nS	nést
listy	list	k1gInPc4	list
<g/>
,	,	kIx,	,
Stvol	stvol	k1gInSc1	stvol
<g/>
,	,	kIx,	,
stonek	stonek	k1gInSc1	stonek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nenese	nést	k5eNaImIp3nS	nést
listy	list	k1gInPc7	list
Stéblo	stéblo	k1gNnSc1	stéblo
(	(	kIx(	(
<g/>
culmus	culmus	k1gInSc1	culmus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dutý	dutý	k2eAgInSc1d1	dutý
stonek	stonek	k1gInSc1	stonek
trav	tráva	k1gFnPc2	tráva
s	s	k7c7	s
kolénky	kolénko	k1gNnPc7	kolénko
kmen	kmen	k1gInSc1	kmen
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgInSc1d1	hlavní
stonek	stonek	k1gInSc1	stonek
stromů	strom	k1gInPc2	strom
Lodyžka	lodyžka	k1gFnSc1	lodyžka
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
stonek	stonek	k1gInSc1	stonek
<g/>
"	"	kIx"	"
některých	některý	k3yIgInPc2	některý
mechorostů	mechorost	k1gInPc2	mechorost
Dřevnaté	dřevnatý	k2eAgFnSc2d1	dřevnatá
stonky	stonka	k1gFnSc2	stonka
Dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
dřevnatění	dřevnatění	k1gNnSc3	dřevnatění
původně	původně	k6eAd1	původně
dužnatého	dužnatý	k2eAgInSc2d1	dužnatý
stonku	stonek	k1gInSc2	stonek
<g/>
.	.	kIx.	.
keř	keř	k1gInSc1	keř
(	(	kIx(	(
<g/>
šípek	šípek	k1gInSc1	šípek
<g/>
)	)	kIx)	)
strom	strom	k1gInSc1	strom
(	(	kIx(	(
<g/>
javor	javor	k1gInSc1	javor
<g/>
)	)	kIx)	)
polokeř	polokeř	k1gInSc1	polokeř
(	(	kIx(	(
<g/>
borůvka	borůvka	k1gFnSc1	borůvka
<g/>
)	)	kIx)	)
keřík	keřík	k1gInSc1	keřík
Podle	podle	k7c2	podle
polohy	poloha	k1gFnSc2	poloha
přímý	přímý	k2eAgInSc1d1	přímý
poléhavý	poléhavý	k2eAgInSc1d1	poléhavý
plazivý	plazivý	k2eAgInSc1d1	plazivý
vystoupavý	vystoupavý	k2eAgInSc1d1	vystoupavý
ovíjivý	ovíjivý	k2eAgInSc1d1	ovíjivý
popínavý	popínavý	k2eAgInSc1d1	popínavý
Podle	podle	k7c2	podle
specializovaných	specializovaný	k2eAgFnPc2d1	specializovaná
funkcí	funkce	k1gFnPc2	funkce
se	se	k3xPyFc4	se
často	často	k6eAd1	často
mění	měnit	k5eAaImIp3nS	měnit
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
uspořádání	uspořádání	k1gNnSc4	uspořádání
stonku	stonek	k1gInSc2	stonek
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
různé	různý	k2eAgInPc1d1	různý
metamorfozy	metamorfoz	k1gInPc1	metamorfoz
<g/>
:	:	kIx,	:
oddenek	oddenek	k1gInSc1	oddenek
<g/>
:	:	kIx,	:
podzemní	podzemní	k2eAgInSc1d1	podzemní
ztlustlý	ztlustlý	k2eAgInSc1d1	ztlustlý
stonek	stonek	k1gInSc1	stonek
<g/>
,	,	kIx,	,
např.	např.	kA	např.
přeslička	přeslička	k1gFnSc1	přeslička
šlahoun	šlahoun	k1gInSc1	šlahoun
<g/>
:	:	kIx,	:
nadzemní	nadzemní	k2eAgInPc1d1	nadzemní
výběžky	výběžek	k1gInPc1	výběžek
např.	např.	kA	např.
<g/>
:	:	kIx,	:
jahodník	jahodník	k1gInSc1	jahodník
oddenková	oddenkový	k2eAgFnSc1d1	oddenkový
hlíza	hlíza	k1gFnSc1	hlíza
<g/>
:	:	kIx,	:
podzemní	podzemní	k2eAgFnSc1d1	podzemní
část	část	k1gFnSc1	část
stonku	stonek	k1gInSc2	stonek
ztloustne	ztloustnout	k5eAaPmIp3nS	ztloustnout
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
funkci	funkce	k1gFnSc4	funkce
zásobní	zásobní	k2eAgFnSc1d1	zásobní
<g/>
,	,	kIx,	,
např.	např.	kA	např.
brambor	brambora	k1gFnPc2	brambora
stonková	stonkový	k2eAgFnSc1d1	stonková
hlíza	hlíza	k1gFnSc1	hlíza
<g/>
:	:	kIx,	:
ztlustlá	ztlustlý	k2eAgFnSc1d1	ztlustlá
nadzemní	nadzemní	k2eAgFnSc1d1	nadzemní
část	část	k1gFnSc1	část
stonku	stonek	k1gInSc2	stonek
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
např.	např.	kA	např.
kedluben	kedluben	k1gInSc4	kedluben
úponky	úponka	k1gFnSc2	úponka
<g/>
:	:	kIx,	:
k	k	k7c3	k
zachytávání	zachytávání	k1gNnSc3	zachytávání
se	se	k3xPyFc4	se
na	na	k7c6	na
podkladě	podklad	k1gInSc6	podklad
<g/>
,	,	kIx,	,
popínavé	popínavý	k2eAgFnPc1d1	popínavá
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
réva	réva	k1gFnSc1	réva
vinná	vinný	k2eAgFnSc1d1	vinná
stonkové	stonkový	k2eAgInPc1d1	stonkový
trny	trn	k1gInPc1	trn
–	–	k?	–
kolce	kolec	k1gInSc2	kolec
<g/>
:	:	kIx,	:
ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c7	před
býložravci	býložravec	k1gMnPc7	býložravec
<g/>
,	,	kIx,	,
např.	např.	kA	např.
trnka	trnka	k1gFnSc1	trnka
zdužnatělé	zdužnatělý	k2eAgFnSc2d1	zdužnatělá
stonky	stonka	k1gFnSc2	stonka
<g/>
:	:	kIx,	:
zásobárna	zásobárna	k1gFnSc1	zásobárna
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
např.	např.	kA	např.
u	u	k7c2	u
kaktusů	kaktus	k1gInPc2	kaktus
rozšiřování	rozšiřování	k1gNnSc2	rozšiřování
stonku	stonek	k1gInSc2	stonek
do	do	k7c2	do
plochy	plocha	k1gFnSc2	plocha
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
asimilační	asimilační	k2eAgFnSc7d1	asimilační
funkcí	funkce	k1gFnSc7	funkce
<g/>
,	,	kIx,	,
mívá	mívat	k5eAaImIp3nS	mívat
podobu	podoba	k1gFnSc4	podoba
listů	list	k1gInPc2	list
(	(	kIx(	(
<g/>
listnatec	listnatec	k1gMnSc1	listnatec
<g/>
)	)	kIx)	)
brachyblast	brachyblast	k1gInSc1	brachyblast
<g/>
:	:	kIx,	:
např.	např.	kA	např.
<g/>
:	:	kIx,	:
modřín	modřín	k1gInSc1	modřín
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
modifikace	modifikace	k1gFnSc1	modifikace
<g/>
,	,	kIx,	,
nemění	měnit	k5eNaImIp3nS	měnit
se	se	k3xPyFc4	se
funkce	funkce	k1gFnSc1	funkce
<g/>
)	)	kIx)	)
Růst	růst	k1gInSc1	růst
stonku	stonek	k1gInSc2	stonek
se	se	k3xPyFc4	se
uskutečňuje	uskutečňovat	k5eAaImIp3nS	uskutečňovat
dělením	dělení	k1gNnSc7	dělení
buněk	buňka	k1gFnPc2	buňka
na	na	k7c6	na
vegetačním	vegetační	k2eAgInSc6d1	vegetační
vrcholu	vrchol	k1gInSc6	vrchol
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
zvětšováním	zvětšování	k1gNnSc7	zvětšování
buněk	buňka	k1gFnPc2	buňka
v	v	k7c6	v
internodiích	internodium	k1gNnPc6	internodium
<g/>
.	.	kIx.	.
</s>
<s>
Tloustnutí	tloustnutí	k1gNnSc1	tloustnutí
stonku	stonek	k1gInSc2	stonek
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
buď	buď	k8xC	buď
zvětšováním	zvětšování	k1gNnSc7	zvětšování
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vytvářením	vytváření	k1gNnSc7	vytváření
druhotných	druhotný	k2eAgNnPc2d1	druhotné
pletiv	pletivo	k1gNnPc2	pletivo
<g/>
,	,	kIx,	,
produkovaných	produkovaný	k2eAgNnPc2d1	produkované
kambiem	kambium	k1gNnSc7	kambium
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
stonku	stonek	k1gInSc2	stonek
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
různorodá	různorodý	k2eAgFnSc1d1	různorodá
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
vůbec	vůbec	k9	vůbec
stonek	stonek	k1gInSc1	stonek
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
smyslu	smysl	k1gInSc6	smysl
nevytváří	vytvářit	k5eNaPmIp3nS	vytvářit
(	(	kIx(	(
<g/>
např.	např.	kA	např.
okřehek	okřehek	k1gInSc1	okřehek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
stonek	stonek	k1gInSc4	stonek
má	mít	k5eAaImIp3nS	mít
liána	liána	k1gFnSc1	liána
rotan	rotan	k1gMnSc1	rotan
rákosovitý	rákosovitý	k2eAgMnSc1d1	rákosovitý
(	(	kIx(	(
<g/>
Calamus	Calamus	k1gInSc1	Calamus
rotang	rotang	k1gInSc1	rotang
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
200	[number]	k4	200
<g/>
–	–	k?	–
<g/>
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Stonek	stonek	k1gInSc1	stonek
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
zejména	zejména	k9	zejména
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
částí	část	k1gFnPc2	část
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
příčný	příčný	k2eAgInSc4d1	příčný
řez	řez	k1gInSc4	řez
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
centrální	centrální	k2eAgFnSc1d1	centrální
dřeň	dřeň	k1gFnSc1	dřeň
slouží	sloužit	k5eAaImIp3nS	sloužit
hlavně	hlavně	k9	hlavně
jako	jako	k9	jako
zásobárna	zásobárna	k1gFnSc1	zásobárna
živin	živina	k1gFnPc2	živina
kůra	kůra	k1gFnSc1	kůra
stonek	stonek	k1gInSc1	stonek
chrání	chránit	k5eAaImIp3nS	chránit
provazce	provazec	k1gInPc4	provazec
sklerenchymu	sklerenchym	k1gInSc2	sklerenchym
a	a	k8xC	a
kolenchymu	kolenchym	k1gInSc2	kolenchym
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
pevnost	pevnost	k1gFnSc4	pevnost
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
větvení	větvení	k1gNnPc2	větvení
<g/>
.	.	kIx.	.
</s>
<s>
Stonek	stonek	k1gInSc1	stonek
se	se	k3xPyFc4	se
často	často	k6eAd1	často
větví	větvit	k5eAaImIp3nS	větvit
<g/>
:	:	kIx,	:
vidličnaté	vidličnatý	k2eAgNnSc4d1	vidličnatý
(	(	kIx(	(
<g/>
dichotomické	dichotomický	k2eAgNnSc4d1	dichotomické
<g/>
)	)	kIx)	)
větvení	větvení	k1gNnSc4	větvení
–	–	k?	–
hlavně	hlavně	k6eAd1	hlavně
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
cévnatých	cévnatý	k2eAgFnPc2d1	cévnatá
výtrusných	výtrusný	k2eAgFnPc2d1	výtrusná
rostlin	rostlina	k1gFnPc2	rostlina
(	(	kIx(	(
<g/>
plavuně	plavuň	k1gFnPc1	plavuň
<g/>
,	,	kIx,	,
vranečky	vraneček	k1gInPc1	vraneček
<g/>
)	)	kIx)	)
postranní	postranní	k2eAgFnSc3d1	postranní
(	(	kIx(	(
<g/>
holoblastické	holoblastický	k2eAgFnSc3d1	holoblastický
<g/>
)	)	kIx)	)
větvení	větvení	k1gNnSc1	větvení
<g/>
.	.	kIx.	.
monopodiální	monopodiální	k2eAgMnSc1d1	monopodiální
–	–	k?	–
hlavní	hlavní	k2eAgInSc1d1	hlavní
stonek	stonek	k1gInSc1	stonek
stále	stále	k6eAd1	stále
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
<g/>
,	,	kIx,	,
postranní	postranní	k2eAgFnPc1d1	postranní
větve	větev	k1gFnPc1	větev
jsou	být	k5eAaImIp3nP	být
kratší	krátký	k2eAgInPc1d2	kratší
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgInPc1d1	hlavní
nepřerůstají	přerůstat	k5eNaImIp3nP	přerůstat
<g/>
;	;	kIx,	;
sympodiální	sympodiální	k2eAgFnPc1d1	sympodiální
–	–	k?	–
postranní	postranní	k2eAgFnPc1d1	postranní
větve	větev	k1gFnPc1	větev
mohutnější	mohutný	k2eAgFnPc1d2	mohutnější
a	a	k8xC	a
přerůstají	přerůstat	k5eAaImIp3nP	přerůstat
hlavní	hlavní	k2eAgInSc4d1	hlavní
stonek	stonek	k1gInSc4	stonek
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
postranní	postranní	k2eAgFnSc4d1	postranní
větev	větev	k1gFnSc4	větev
zatlačí	zatlačit	k5eAaPmIp3nS	zatlačit
hlavní	hlavní	k2eAgInSc1d1	hlavní
stonek	stonek	k1gInSc1	stonek
do	do	k7c2	do
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
jeho	jeho	k3xOp3gInSc2	jeho
růstu	růst	k1gInSc2	růst
(	(	kIx(	(
<g/>
réva	réva	k1gFnSc1	réva
vinná	vinný	k2eAgFnSc1d1	vinná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dužnatý	dužnatý	k2eAgInSc1d1	dužnatý
–	–	k?	–
pokožka	pokožka	k1gFnSc1	pokožka
a	a	k8xC	a
dužnina	dužnina	k1gFnSc1	dužnina
s	s	k7c7	s
cévními	cévní	k2eAgInPc7d1	cévní
svazky	svazek	k1gInPc7	svazek
(	(	kIx(	(
<g/>
žilky	žilka	k1gFnPc4	žilka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dřevnatý	dřevnatý	k2eAgInSc1d1	dřevnatý
–	–	k?	–
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
je	být	k5eAaImIp3nS	být
kůra	kůra	k1gFnSc1	kůra
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
u	u	k7c2	u
borovice	borovice	k1gFnSc2	borovice
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
odumřelou	odumřelý	k2eAgFnSc4d1	odumřelá
část	část	k1gFnSc4	část
stonku	stonek	k1gInSc2	stonek
–	–	k?	–
borku	borek	k1gInSc2	borek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
následuje	následovat	k5eAaImIp3nS	následovat
lýko	lýko	k1gNnSc1	lýko
<g/>
,	,	kIx,	,
dřevo	dřevo	k1gNnSc1	dřevo
a	a	k8xC	a
dřeň	dřeň	k1gFnSc1	dřeň
<g/>
.	.	kIx.	.
</s>
<s>
Dřevo	dřevo	k1gNnSc1	dřevo
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
řidší	řídký	k2eAgMnSc1d2	řidší
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
hustší	hustý	k2eAgMnSc1d2	hustší
(	(	kIx(	(
<g/>
letokruhy	letokruh	k1gInPc1	letokruh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Filipec	Filipec	k1gMnSc1	Filipec
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
,	,	kIx,	,
Daneš	Daneš	k1gMnSc1	Daneš
<g/>
,	,	kIx,	,
F.	F.	kA	F.
<g/>
,	,	kIx,	,
Machač	Machač	k1gMnSc1	Machač
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
,	,	kIx,	,
Mejstřík	Mejstřík	k1gMnSc1	Mejstřík
<g/>
,	,	kIx,	,
V.	V.	kA	V.
<g/>
,	,	kIx,	,
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Slovník	slovník	k1gInSc1	slovník
spisovné	spisovný	k2eAgFnSc2d1	spisovná
češtiny	čeština	k1gFnSc2	čeština
pro	pro	k7c4	pro
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
:	:	kIx,	:
s	s	k7c7	s
Dodatkem	dodatek	k1gInSc7	dodatek
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
tělovýchovy	tělovýchova	k1gFnSc2	tělovýchova
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
opr	opr	k?	opr
<g/>
.	.	kIx.	.
a	a	k8xC	a
dopl	dopl	k1gInSc1	dopl
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-200-0493-9	[number]	k4	80-200-0493-9
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Stonek	stonka	k1gFnPc2	stonka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
stonek	stonek	k1gInSc1	stonek
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
