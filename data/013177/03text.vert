<p>
<s>
Drama	drama	k1gNnSc1	drama
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
δ	δ	k?	δ
–	–	k?	–
dělání	dělání	k1gNnSc1	dělání
<g/>
,	,	kIx,	,
čin	čin	k1gInSc1	čin
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
lyrikou	lyrika	k1gFnSc7	lyrika
a	a	k8xC	a
epikou	epika	k1gFnSc7	epika
mezi	mezi	k7c4	mezi
základní	základní	k2eAgInPc4d1	základní
literární	literární	k2eAgInPc4d1	literární
druhy	druh	k1gInPc4	druh
a	a	k8xC	a
žánry	žánr	k1gInPc4	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Drama	drama	k1gNnSc1	drama
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
příběh	příběh	k1gInSc1	příběh
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
pomocí	pomocí	k7c2	pomocí
dialogů	dialog	k1gInPc2	dialog
a	a	k8xC	a
monologů	monolog	k1gInPc2	monolog
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
bylo	být	k5eAaImAgNnS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
především	především	k6eAd1	především
divadelní	divadelní	k2eAgNnSc1d1	divadelní
scénické	scénický	k2eAgNnSc1d1	scénické
provedení	provedení	k1gNnSc1	provedení
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
v	v	k7c6	v
hraném	hraný	k2eAgInSc6d1	hraný
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozhlase	rozhlas	k1gInSc6	rozhlas
a	a	k8xC	a
televizi	televize	k1gFnSc6	televize
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
formách	forma	k1gFnPc6	forma
audiovizuálních	audiovizuální	k2eAgFnPc2d1	audiovizuální
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
drama	drama	k1gNnSc1	drama
<g/>
"	"	kIx"	"
původně	původně	k6eAd1	původně
znamenalo	znamenat	k5eAaImAgNnS	znamenat
tanec	tanec	k1gInSc4	tanec
satyrů	satyr	k1gMnPc2	satyr
<g/>
,	,	kIx,	,
ztělesňující	ztělesňující	k2eAgInSc4d1	ztělesňující
určitý	určitý	k2eAgInSc4d1	určitý
děj	děj	k1gInSc4	děj
<g/>
,	,	kIx,	,
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
jevištní	jevištní	k2eAgNnPc1d1	jevištní
představení	představení	k1gNnPc1	představení
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
tragédii	tragédie	k1gFnSc6	tragédie
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInPc1d1	základní
teoretické	teoretický	k2eAgInPc1d1	teoretický
principy	princip	k1gInPc1	princip
dramatu	drama	k1gNnSc2	drama
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
Poetice	poetika	k1gFnSc6	poetika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
tragédii	tragédie	k1gFnSc6	tragédie
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
dramatu	drama	k1gNnSc2	drama
se	se	k3xPyFc4	se
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
přímé	přímý	k2eAgFnSc2d1	přímá
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
veršované	veršovaný	k2eAgNnSc1d1	veršované
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
převažuje	převažovat	k5eAaImIp3nS	převažovat
próza	próza	k1gFnSc1	próza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výstavba	výstavba	k1gFnSc1	výstavba
dramatu	drama	k1gNnSc2	drama
podle	podle	k7c2	podle
Gustava	Gustav	k1gMnSc2	Gustav
Freytaga	Freytag	k1gMnSc2	Freytag
==	==	k?	==
</s>
</p>
<p>
<s>
expozice	expozice	k1gFnSc1	expozice
–	–	k?	–
uvedení	uvedení	k1gNnSc1	uvedení
do	do	k7c2	do
děje	děj	k1gInSc2	děj
(	(	kIx(	(
<g/>
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
nezbytné	nezbytný	k2eAgInPc4d1	nezbytný
předpoklady	předpoklad	k1gInPc4	předpoklad
k	k	k7c3	k
porozumění	porozumění	k1gNnSc3	porozumění
ději	dít	k5eAaImIp1nS	dít
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kolize	kolize	k1gFnSc1	kolize
–	–	k?	–
zařazení	zařazení	k1gNnSc1	zařazení
dramatického	dramatický	k2eAgInSc2d1	dramatický
prvku	prvek	k1gInSc2	prvek
(	(	kIx(	(
<g/>
rozpor	rozpor	k1gInSc1	rozpor
nebo	nebo	k8xC	nebo
konflikt	konflikt	k1gInSc1	konflikt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
dramatické	dramatický	k2eAgNnSc4d1	dramatické
napětí	napětí	k1gNnSc4	napětí
a	a	k8xC	a
potřebu	potřeba	k1gFnSc4	potřeba
jej	on	k3xPp3gMnSc4	on
vyřešit	vyřešit	k5eAaPmF	vyřešit
</s>
</p>
<p>
<s>
krize	krize	k1gFnSc1	krize
–	–	k?	–
vyvrcholení	vyvrcholení	k1gNnSc1	vyvrcholení
dramatu	drama	k1gNnSc2	drama
</s>
</p>
<p>
<s>
peripetie	peripetie	k1gFnSc1	peripetie
–	–	k?	–
obrat	obrat	k1gInSc4	obrat
a	a	k8xC	a
možnosti	možnost	k1gFnPc4	možnost
řešení	řešení	k1gNnPc2	řešení
</s>
</p>
<p>
<s>
katastrofa	katastrofa	k1gFnSc1	katastrofa
–	–	k?	–
rozuzlení	rozuzlení	k1gNnSc1	rozuzlení
a	a	k8xC	a
očista	očista	k1gFnSc1	očista
(	(	kIx(	(
<g/>
katarze	katarze	k1gFnSc1	katarze
<g/>
)	)	kIx)	)
<g/>
Klasické	klasický	k2eAgNnSc1d1	klasické
divadelní	divadelní	k2eAgNnSc1d1	divadelní
drama	drama	k1gNnSc1	drama
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
jednotu	jednota	k1gFnSc4	jednota
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
času	čas	k1gInSc2	čas
a	a	k8xC	a
děje	děj	k1gInSc2	děj
<g/>
.	.	kIx.	.
</s>
<s>
Aristotelés	Aristotelés	k6eAd1	Aristotelés
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
Poetice	poetika	k1gFnSc6	poetika
popisuje	popisovat	k5eAaImIp3nS	popisovat
potřebu	potřeba	k1gFnSc4	potřeba
jednoty	jednota	k1gFnSc2	jednota
a	a	k8xC	a
ucelenosti	ucelenost	k1gFnSc2	ucelenost
děje	děj	k1gInSc2	děj
<g/>
.	.	kIx.	.
</s>
<s>
Jednota	jednota	k1gFnSc1	jednota
času	čas	k1gInSc2	čas
a	a	k8xC	a
místa	místo	k1gNnSc2	místo
je	být	k5eAaImIp3nS	být
však	však	k9	však
až	až	k6eAd1	až
středověkou	středověký	k2eAgFnSc7d1	středověká
interpretací	interpretace	k1gFnSc7	interpretace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
tragédie	tragédie	k1gFnSc1	tragédie
</s>
</p>
<p>
<s>
komedie	komedie	k1gFnSc1	komedie
</s>
</p>
<p>
<s>
hudební	hudební	k2eAgFnPc1d1	hudební
formy	forma	k1gFnPc1	forma
<g/>
:	:	kIx,	:
melodrama	melodrama	k?	melodrama
<g/>
,	,	kIx,	,
muzikál	muzikál	k1gInSc1	muzikál
<g/>
,	,	kIx,	,
opereta	opereta	k1gFnSc1	opereta
<g/>
,	,	kIx,	,
kabaret	kabaret	k1gInSc1	kabaret
<g/>
,	,	kIx,	,
opera	opera	k1gFnSc1	opera
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
dramatu	drama	k1gNnSc2	drama
==	==	k?	==
</s>
</p>
<p>
<s>
Drama	drama	k1gNnSc1	drama
se	se	k3xPyFc4	se
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
ve	v	k7c6	v
starověkém	starověký	k2eAgNnSc6d1	starověké
Řecku	Řecko	k1gNnSc6	Řecko
z	z	k7c2	z
oslav	oslava	k1gFnPc2	oslava
boha	bůh	k1gMnSc2	bůh
Dionýsa	Dionýsos	k1gMnSc2	Dionýsos
(	(	kIx(	(
<g/>
dithyramby	dithyramba	k1gFnPc1	dithyramba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Náměty	námět	k1gInPc1	námět
byly	být	k5eAaImAgInP	být
čerpány	čerpat	k5eAaImNgInP	čerpat
z	z	k7c2	z
mytologie	mytologie	k1gFnSc2	mytologie
(	(	kIx(	(
<g/>
tragedie	tragedie	k1gFnSc1	tragedie
<g/>
)	)	kIx)	)
i	i	k9	i
ze	z	k7c2	z
současného	současný	k2eAgInSc2d1	současný
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
komedie	komedie	k1gFnSc2	komedie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějšími	významný	k2eAgMnPc7d3	nejvýznamnější
starověkými	starověký	k2eAgMnPc7d1	starověký
dramatiky	dramatik	k1gMnPc7	dramatik
byli	být	k5eAaImAgMnP	být
Aischylos	Aischylos	k1gMnSc1	Aischylos
<g/>
,	,	kIx,	,
Sofoklés	Sofoklés	k1gInSc1	Sofoklés
<g/>
,	,	kIx,	,
Euripidés	Euripidés	k1gInSc1	Euripidés
<g/>
,	,	kIx,	,
Aristofanes	Aristofanes	k1gMnSc1	Aristofanes
a	a	k8xC	a
Říman	Říman	k1gMnSc1	Říman
Titus	Titus	k1gMnSc1	Titus
Maccius	Maccius	k1gMnSc1	Maccius
Plautus	Plautus	k1gMnSc1	Plautus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Říma	Řím	k1gInSc2	Řím
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nový	nový	k2eAgInSc1d1	nový
typ	typ	k1gInSc1	typ
dramatu	drama	k1gNnSc2	drama
spojený	spojený	k2eAgInSc1d1	spojený
s	s	k7c7	s
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
dramatizace	dramatizace	k1gFnPc4	dramatizace
biblických	biblický	k2eAgInPc2d1	biblický
příběhů	příběh	k1gInPc2	příběh
a	a	k8xC	a
legend	legenda	k1gFnPc2	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Církevními	církevní	k2eAgFnPc7d1	církevní
hrami	hra	k1gFnPc7	hra
byla	být	k5eAaImAgFnS	být
mysteria	mysterium	k1gNnSc2	mysterium
<g/>
,	,	kIx,	,
miracula	miraculum	k1gNnSc2	miraculum
a	a	k8xC	a
pašije	pašije	k1gFnSc2	pašije
<g/>
.	.	kIx.	.
</s>
<s>
Rozvíjelo	rozvíjet	k5eAaImAgNnS	rozvíjet
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
světské	světský	k2eAgNnSc4d1	světské
drama	drama	k1gNnSc4	drama
zejména	zejména	k9	zejména
satirické	satirický	k2eAgFnPc4d1	satirická
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
frašky	fraška	k1gFnPc4	fraška
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
díla	dílo	k1gNnPc4	dílo
antická	antický	k2eAgNnPc4d1	antické
dramata	drama	k1gNnPc4	drama
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejichž	jejichž	k3xOyRp3gNnSc3	jejichž
oživení	oživení	k1gNnSc3	oživení
dochází	docházet	k5eAaImIp3nS	docházet
až	až	k9	až
v	v	k7c6	v
renesanci	renesance	k1gFnSc6	renesance
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
dramatikem	dramatik	k1gMnSc7	dramatik
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
byl	být	k5eAaImAgMnS	být
Angličan	Angličan	k1gMnSc1	Angličan
William	William	k1gInSc4	William
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalšími	další	k2eAgMnPc7d1	další
významnými	významný	k2eAgMnPc7d1	významný
dramatiky	dramatik	k1gMnPc7	dramatik
byli	být	k5eAaImAgMnP	být
Moliè	Moliè	k1gMnSc1	Moliè
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Goethe	Goethe	k1gFnSc1	Goethe
<g/>
,	,	kIx,	,
Friedrich	Friedrich	k1gMnSc1	Friedrich
Schiller	Schiller	k1gMnSc1	Schiller
<g/>
,	,	kIx,	,
Victor	Victor	k1gMnSc1	Victor
Hugo	Hugo	k1gMnSc1	Hugo
<g/>
,	,	kIx,	,
Alexander	Alexandra	k1gFnPc2	Alexandra
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Puškin	Puškin	k1gMnSc1	Puškin
<g/>
,	,	kIx,	,
Henrik	Henrik	k1gMnSc1	Henrik
Ibsen	Ibsen	k1gMnSc1	Ibsen
a	a	k8xC	a
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Vasiljevič	Vasiljevič	k1gMnSc1	Vasiljevič
Gogol	Gogol	k1gMnSc1	Gogol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozvolnění	rozvolnění	k1gNnSc3	rozvolnění
dramatické	dramatický	k2eAgFnSc2d1	dramatická
formy	forma	k1gFnSc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Dramatici	dramatik	k1gMnPc1	dramatik
experimentují	experimentovat	k5eAaImIp3nP	experimentovat
s	s	k7c7	s
psychologickým	psychologický	k2eAgNnSc7d1	psychologické
prokreslením	prokreslení	k1gNnSc7	prokreslení
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
subjektivním	subjektivní	k2eAgNnSc7d1	subjektivní
pojetím	pojetí	k1gNnSc7	pojetí
dramatu	drama	k1gNnSc2	drama
<g/>
.	.	kIx.	.
</s>
<s>
Těmito	tento	k3xDgMnPc7	tento
experimentátory	experimentátor	k1gMnPc7	experimentátor
byli	být	k5eAaImAgMnP	být
Anton	Anton	k1gMnSc1	Anton
Pavlovič	Pavlovič	k1gMnSc1	Pavlovič
Čechov	Čechov	k1gMnSc1	Čechov
<g/>
,	,	kIx,	,
Maxim	Maxim	k1gMnSc1	Maxim
Gorkij	Gorkij	k1gMnSc1	Gorkij
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Bernard	Bernard	k1gMnSc1	Bernard
Shaw	Shaw	k1gMnSc1	Shaw
<g/>
,	,	kIx,	,
Oscar	Oscar	k1gMnSc1	Oscar
Wilde	Wild	k1gInSc5	Wild
a	a	k8xC	a
Bertolt	Bertolt	k2eAgMnSc1d1	Bertolt
Brecht	Brecht	k1gMnSc1	Brecht
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
epickým	epický	k2eAgNnSc7d1	epické
divadlem	divadlo	k1gNnSc7	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
absurdního	absurdní	k2eAgNnSc2d1	absurdní
dramatu	drama	k1gNnSc2	drama
(	(	kIx(	(
<g/>
Samuel	Samuel	k1gMnSc1	Samuel
Beckett	Beckett	k1gMnSc1	Beckett
<g/>
,	,	kIx,	,
Eugè	Eugè	k1gMnSc1	Eugè
Ionesco	Ionesco	k1gMnSc1	Ionesco
nebo	nebo	k8xC	nebo
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Struktura	struktura	k1gFnSc1	struktura
==	==	k?	==
</s>
</p>
<p>
<s>
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
člení	členit	k5eAaImIp3nS	členit
do	do	k7c2	do
jednání	jednání	k1gNnSc2	jednání
čili	čili	k8xC	čili
aktů	akt	k1gInPc2	akt
<g/>
,	,	kIx,	,
velkých	velký	k2eAgInPc2d1	velký
ucelených	ucelený	k2eAgInPc2d1	ucelený
úseků	úsek	k1gInPc2	úsek
děje	děj	k1gInSc2	děj
<g/>
,	,	kIx,	,
kterých	který	k3yIgInPc2	který
většinou	většinou	k6eAd1	většinou
nebývá	bývat	k5eNaImIp3nS	bývat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
pět	pět	k4xCc4	pět
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
jednoaktovka	jednoaktovka	k1gFnSc1	jednoaktovka
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
krátká	krátký	k2eAgFnSc1d1	krátká
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
jedno	jeden	k4xCgNnSc1	jeden
jednání	jednání	k1gNnSc1	jednání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednání	jednání	k1gNnSc1	jednání
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
do	do	k7c2	do
scén	scéna	k1gFnPc2	scéna
či	či	k8xC	či
obrazů	obraz	k1gInPc2	obraz
<g/>
;	;	kIx,	;
během	během	k7c2	během
jedné	jeden	k4xCgFnSc2	jeden
scény	scéna	k1gFnSc2	scéna
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nemění	měnit	k5eNaImIp3nP	měnit
herci	herec	k1gMnPc1	herec
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
ani	ani	k8xC	ani
samo	sám	k3xTgNnSc1	sám
jeviště	jeviště	k1gNnSc1	jeviště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Scéna	scéna	k1gFnSc1	scéna
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
replik	replika	k1gFnPc2	replika
<g/>
,	,	kIx,	,
výroků	výrok	k1gInPc2	výrok
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
herců	herc	k1gInPc2	herc
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
herec	herec	k1gMnSc1	herec
přednáší	přednášet	k5eAaImIp3nS	přednášet
nepřerušovanou	přerušovaný	k2eNgFnSc4d1	nepřerušovaná
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
řeč	řeč	k1gFnSc4	řeč
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
monolog	monolog	k1gInSc1	monolog
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Absurdní	absurdní	k2eAgNnSc1d1	absurdní
drama	drama	k1gNnSc1	drama
</s>
</p>
<p>
<s>
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
fotografie	fotografie	k1gFnSc1	fotografie
</s>
</p>
<p>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
</s>
</p>
<p>
<s>
Dramatizace	dramatizace	k1gFnSc1	dramatizace
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
dramatická	dramatický	k2eAgFnSc1d1	dramatická
forma	forma	k1gFnSc1	forma
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
drama	drama	k1gNnSc4	drama
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
