<s>
Červená	červený	k2eAgFnSc1d1
karkulka	karkulka	k1gFnSc1
</s>
<s>
Červená	červený	k2eAgFnSc1d1
Karkulka	Karkulka	k1gFnSc1
a	a	k8xC
vlk	vlk	k1gMnSc1
</s>
<s>
Henry	Henry	k1gMnSc1
Peach	Peach	k1gMnSc1
Robinson	Robinson	k1gMnSc1
<g/>
:	:	kIx,
Červená	červený	k2eAgFnSc1d1
Karkulka	Karkulka	k1gFnSc1
dorazila	dorazit	k5eAaPmAgFnS
ke	k	k7c3
dveřím	dveře	k1gFnPc3
své	svůj	k3xOyFgFnSc2
babičky	babička	k1gFnSc2
<g/>
,	,	kIx,
albuminový	albuminový	k2eAgInSc1d1
tisk	tisk	k1gInSc1
<g/>
,	,	kIx,
23,3	23,3	k4
<g/>
×	×	k?
<g/>
18,7	18,7	k4
cm	cm	kA
<g/>
,	,	kIx,
1858	#num#	k4
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1
Otto	Otto	k1gMnSc1
Kubel	Kubel	k1gMnSc1
<g/>
,	,	kIx,
kolem	kolem	k7c2
roku	rok	k1gInSc2
1930	#num#	k4
</s>
<s>
Lewis	Lewis	k1gInSc1
Carroll	Carroll	k1gInSc1
pořídil	pořídit	k5eAaPmAgInS
fotografii	fotografia	k1gFnSc4
Červené	Červené	k2eAgFnSc2d1
Karkulky	Karkulka	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1857	#num#	k4
</s>
<s>
Červená	červený	k2eAgFnSc1d1
karkulka	karkulka	k1gFnSc1
je	být	k5eAaImIp3nS
pohádka	pohádka	k1gFnSc1
o	o	k7c6
setkání	setkání	k1gNnSc6
mladé	mladý	k2eAgFnSc2d1
dívky	dívka	k1gFnSc2
s	s	k7c7
vlkem	vlk	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
příběh	příběh	k1gInSc1
se	se	k3xPyFc4
během	během	k7c2
své	svůj	k3xOyFgFnSc2
historie	historie	k1gFnSc2
velmi	velmi	k6eAd1
měnil	měnit	k5eAaImAgMnS
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	s	k7c7
předlohou	předloha	k1gFnSc7
pro	pro	k7c4
značné	značný	k2eAgNnSc4d1
množství	množství	k1gNnSc4
moderních	moderní	k2eAgFnPc2d1
adaptací	adaptace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejstarší	starý	k2eAgFnSc1d3
psaná	psaný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
pera	pero	k1gNnSc2
Charlese	Charles	k1gMnSc2
Perraulta	Perrault	k1gMnSc2
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
asi	asi	k9
nejrozšířenější	rozšířený	k2eAgFnSc1d3
verze	verze	k1gFnSc1
je	být	k5eAaImIp3nS
založena	založit	k5eAaPmNgFnS
na	na	k7c4
zpracování	zpracování	k1gNnSc4
bratří	bratr	k1gMnPc2
Grimmů	Grimm	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Slovo	slovo	k1gNnSc1
karkulka	karkulka	k1gFnSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
latinského	latinský	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
carracalla	carracallo	k1gNnSc2
a	a	k8xC
znamená	znamenat	k5eAaImIp3nS
čepeček	čepeček	k1gInSc4
<g/>
,	,	kIx,
čapku	čapka	k1gFnSc4
<g/>
,	,	kIx,
či	či	k8xC
pokrývku	pokrývka	k1gFnSc4
hlavy	hlava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1
českého	český	k2eAgInSc2d1
pravopisu	pravopis	k1gInSc2
tolerují	tolerovat	k5eAaImIp3nP
podobu	podoba	k1gFnSc4
„	„	k?
<g/>
Červená	červený	k2eAgFnSc1d1
karkulka	karkulka	k1gFnSc1
<g/>
“	“	k?
i	i	k9
„	„	k?
<g/>
Červená	červený	k2eAgFnSc1d1
Karkulka	Karkulka	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Příběh	příběh	k1gInSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
historie	historie	k1gFnSc1
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Příběh	příběh	k1gInSc1
vypráví	vyprávět	k5eAaImIp3nS
o	o	k7c6
dívce	dívka	k1gFnSc6
přezdívané	přezdívaný	k2eAgFnSc6d1
Červená	červenat	k5eAaImIp3nS
karkulka	karkulka	k1gFnSc1
<g/>
,	,	kIx,
podle	podle	k7c2
karkulky	karkulka	k1gFnSc2
(	(	kIx(
<g/>
karkule	karkule	k1gFnSc1
byla	být	k5eAaImAgFnS
druh	druh	k1gInSc4
čepce	čepec	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
stále	stále	k6eAd1
nosí	nosit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgFnPc6
verzích	verze	k1gFnPc6
jde	jít	k5eAaImIp3nS
o	o	k7c4
kapuci	kapuce	k1gFnSc4
nebo	nebo	k8xC
kápi	kápě	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dívka	dívka	k1gFnSc1
jde	jít	k5eAaImIp3nS
lesem	les	k1gInSc7
za	za	k7c7
svou	svůj	k3xOyFgFnSc7
babičkou	babička	k1gFnSc7
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
nese	nést	k5eAaImIp3nS
něco	něco	k3yInSc4
k	k	k7c3
jídlu	jídlo	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlk	Vlk	k1gMnSc1
chce	chtít	k5eAaImIp3nS
dívku	dívka	k1gFnSc4
sežrat	sežrat	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
bojí	bát	k5eAaImIp3nS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
udělat	udělat	k5eAaPmF
přímo	přímo	k6eAd1
v	v	k7c6
lese	les	k1gInSc6
(	(	kIx(
<g/>
v	v	k7c6
některých	některý	k3yIgFnPc6
verzích	verze	k1gFnPc6
přihlížejí	přihlížet	k5eAaImIp3nP
setkání	setkání	k1gNnSc3
dřevorubci	dřevorubec	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dá	dát	k5eAaPmIp3nS
se	se	k3xPyFc4
proto	proto	k8xC
s	s	k7c7
dívkou	dívka	k1gFnSc7
do	do	k7c2
řeči	řeč	k1gFnSc2
a	a	k8xC
ta	ten	k3xDgFnSc1
mu	on	k3xPp3gMnSc3
naivně	naivně	k6eAd1
prozradí	prozradit	k5eAaPmIp3nS
<g/>
,	,	kIx,
kam	kam	k6eAd1
má	mít	k5eAaImIp3nS
namířeno	namířen	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navrhne	navrhnout	k5eAaPmIp3nS
jí	on	k3xPp3gFnSc3
tedy	tedy	k9
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nasbírala	nasbírat	k5eAaPmAgFnS
kytici	kytice	k1gFnSc4
květin	květina	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
udělá	udělat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezitím	mezitím	k6eAd1
vlk	vlk	k1gMnSc1
přiběhne	přiběhnout	k5eAaPmIp3nS
k	k	k7c3
domu	dům	k1gInSc3
babičky	babička	k1gFnSc2
<g/>
,	,	kIx,
předstírá	předstírat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
Karkulka	Karkulka	k1gFnSc1
a	a	k8xC
vloudí	vloudit	k5eAaPmIp3nS
se	se	k3xPyFc4
dovnitř	dovnitř	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Babičku	babička	k1gFnSc4
sežere	sežrat	k5eAaPmIp3nS
<g/>
,	,	kIx,
oblékne	obléknout	k5eAaPmIp3nS
se	se	k3xPyFc4
do	do	k7c2
jejích	její	k3xOp3gInPc2
šatů	šat	k1gInPc2
a	a	k8xC
čeká	čekat	k5eAaImIp3nS
na	na	k7c4
dívku	dívka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
Karkulka	Karkulka	k1gFnSc1
přijde	přijít	k5eAaPmIp3nS
<g/>
,	,	kIx,
sežere	sežrat	k5eAaPmIp3nS
ji	on	k3xPp3gFnSc4
také	také	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
přichází	přicházet	k5eAaImIp3nS
dřevorubec	dřevorubec	k1gMnSc1
<g/>
,	,	kIx,
rozřízne	rozříznout	k5eAaPmIp3nS
vlkovi	vlk	k1gMnSc3
břicho	břicho	k1gNnSc4
a	a	k8xC
babičku	babička	k1gFnSc4
s	s	k7c7
Karkulkou	Karkulka	k1gFnSc7
<g/>
,	,	kIx,
obě	dva	k4xCgFnPc1
zcela	zcela	k6eAd1
v	v	k7c6
pořádku	pořádek	k1gInSc6
<g/>
,	,	kIx,
zachrání	zachránit	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
naplní	naplnit	k5eAaPmIp3nS
vlkovo	vlkův	k2eAgNnSc4d1
břicho	břicho	k1gNnSc4
kameny	kámen	k1gInPc4
<g/>
,	,	kIx,
takže	takže	k8xS
má	mít	k5eAaImIp3nS
vlk	vlk	k1gMnSc1
po	po	k7c6
probuzení	probuzení	k1gNnSc6
těžké	těžký	k2eAgNnSc4d1
břicho	břicho	k1gNnSc4
a	a	k8xC
když	když	k8xS
se	se	k3xPyFc4
chce	chtít	k5eAaImIp3nS
u	u	k7c2
studny	studna	k1gFnSc2
napít	napít	k5eAaPmF,k5eAaBmF
<g/>
,	,	kIx,
kameny	kámen	k1gInPc1
ho	on	k3xPp3gNnSc2
převáží	převážet	k5eAaImIp3nS,k5eAaPmIp3nS
a	a	k8xC
on	on	k3xPp3gMnSc1
se	se	k3xPyFc4
utopí	utopit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
jiných	jiný	k2eAgFnPc2d1
verzí	verze	k1gFnPc2
příběhu	příběh	k1gInSc2
vlk	vlk	k1gMnSc1
babičku	babička	k1gFnSc4
nesežere	sežrat	k5eNaPmIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
zavře	zavřít	k5eAaPmIp3nS
ji	on	k3xPp3gFnSc4
do	do	k7c2
komory	komora	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
některých	některý	k3yIgInPc6
je	být	k5eAaImIp3nS
zase	zase	k9
Karkulka	Karkulka	k1gFnSc1
zachráněna	zachráněn	k2eAgFnSc1d1
dřevorubcem	dřevorubec	k1gMnSc7
ještě	ještě	k9
před	před	k7c7
sežráním	sežrání	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Příběh	příběh	k1gInSc1
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
jasný	jasný	k2eAgInSc1d1
kontrast	kontrast	k1gInSc1
mezi	mezi	k7c7
bezpečným	bezpečný	k2eAgInSc7d1
světem	svět	k1gInSc7
vesnice	vesnice	k1gFnSc2
a	a	k8xC
nebezpečným	bezpečný	k2eNgInSc7d1
temným	temný	k2eAgInSc7d1
lesem	les	k1gInSc7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
pojetí	pojetí	k1gNnSc1
v	v	k7c6
zásadě	zásada	k1gFnSc6
středověké	středověký	k2eAgFnSc2d1
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
nejsou	být	k5eNaImIp3nP
známé	známý	k2eAgInPc4d1
žádné	žádný	k3yNgInPc4
tak	tak	k6eAd1
staré	starý	k2eAgFnPc1d1
verze	verze	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zřejmý	zřejmý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
také	také	k9
morální	morální	k2eAgInSc1d1
důraz	důraz	k1gInSc1
–	–	k?
jak	jak	k8xC,k8xS
je	být	k5eAaImIp3nS
důležité	důležitý	k2eAgNnSc1d1
nesejít	sejít	k5eNaPmF
ze	z	k7c2
stezky	stezka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Motiv	motiv	k1gInSc1
vlka	vlk	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
svou	svůj	k3xOyFgFnSc4
kořist	kořist	k1gFnSc4
spolkne	spolknout	k5eAaPmIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
ta	ten	k3xDgFnSc1
pak	pak	k6eAd1
vyvázne	vyváznout	k5eAaPmIp3nS
z	z	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
břicha	břich	k1gInSc2
nezraněna	zraněn	k2eNgFnSc1d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
také	také	k9
v	v	k7c6
ruském	ruský	k2eAgInSc6d1
příběhu	příběh	k1gInSc6
Petr	Petr	k1gMnSc1
a	a	k8xC
vlk	vlk	k1gMnSc1
a	a	k8xC
v	v	k7c6
dalším	další	k2eAgInSc6d1
příběhu	příběh	k1gInSc6
bratří	bratr	k1gMnPc2
Grimmů	Grimm	k1gMnPc2
O	o	k7c6
vlku	vlk	k1gMnSc6
a	a	k8xC
sedmi	sedm	k4xCc6
kůzlátkách	kůzlátko	k1gNnPc6
<g/>
,	,	kIx,
v	v	k7c6
podstatě	podstata	k1gFnSc6
jde	jít	k5eAaImIp3nS
o	o	k7c4
téma	téma	k1gNnSc4
staré	starý	k2eAgNnSc4d1
nejméně	málo	k6eAd3
jako	jako	k8xC,k8xS
Jonáš	Jonáš	k1gMnSc1
a	a	k8xC
velryba	velryba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Před	před	k7c7
Perraultem	Perrault	k1gInSc7
</s>
<s>
Přestože	přestože	k8xS
nejsou	být	k5eNaImIp3nP
známé	známý	k2eAgFnPc1d1
žádné	žádný	k3yNgFnPc1
psané	psaný	k2eAgFnPc1d1
formy	forma	k1gFnPc1
časově	časově	k6eAd1
předcházející	předcházející	k2eAgNnSc4d1
zpracování	zpracování	k1gNnSc4
Charlese	Charles	k1gMnSc2
Perraulta	Perrault	k1gMnSc2
<g/>
,	,	kIx,
prameny	pramen	k1gInPc1
příběhu	příběh	k1gInSc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
vystopovat	vystopovat	k5eAaPmF
v	v	k7c6
ústní	ústní	k2eAgFnSc6d1
lidové	lidový	k2eAgFnSc6d1
slovesnosti	slovesnost	k1gFnSc6
mnoha	mnoho	k4c2
evropských	evropský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
z	z	k7c2
nich	on	k3xPp3gMnPc2
stále	stále	k6eAd1
existují	existovat	k5eAaImIp3nP
a	a	k8xC
velmi	velmi	k6eAd1
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
od	od	k7c2
všeobecně	všeobecně	k6eAd1
rozšířené	rozšířený	k2eAgFnSc2d1
verze	verze	k1gFnSc2
inspirované	inspirovaný	k2eAgFnSc2d1
Grimmovými	Grimmův	k2eAgInPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příběh	příběh	k1gInSc1
byl	být	k5eAaImAgInS
vyprávěn	vyprávět	k5eAaImNgInS
francouzskými	francouzský	k2eAgMnPc7d1
rolníky	rolník	k1gMnPc7
už	už	k6eAd1
ve	v	k7c6
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
tak	tak	k9
v	v	k7c6
Itálii	Itálie	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
stále	stále	k6eAd1
ještě	ještě	k6eAd1
existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc1
verzí	verze	k1gFnPc2
<g/>
,	,	kIx,
např.	např.	kA
„	„	k?
<g/>
La	la	k1gNnSc2
finta	finta	k1gFnSc1
nonna	nonna	k1gFnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Falešná	falešný	k2eAgFnSc1d1
babička	babička	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
tato	tento	k3xDgFnSc1
pohádka	pohádka	k1gFnSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
velmi	velmi	k6eAd1
podobných	podobný	k2eAgInPc2d1
orientálních	orientální	k2eAgInPc2d1
příběhů	příběh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
analýzy	analýza	k1gFnSc2
vývoje	vývoj	k1gInSc2
příběhu	příběh	k1gInSc2
pochází	pocházet	k5eAaImIp3nS
příběh	příběh	k1gInSc1
již	již	k6eAd1
ze	z	k7c2
starověku	starověk	k1gInSc2
a	a	k8xC
evropská	evropský	k2eAgFnSc1d1
verze	verze	k1gFnSc1
se	se	k3xPyFc4
oddělila	oddělit	k5eAaPmAgFnS
asi	asi	k9
před	před	k7c7
tisícem	tisíc	k4xCgInSc7
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prvotní	prvotní	k2eAgFnSc1d1
variace	variace	k1gFnSc1
příběhu	příběh	k1gInSc2
se	se	k3xPyFc4
od	od	k7c2
toho	ten	k3xDgNnSc2
dnes	dnes	k6eAd1
známého	známý	k1gMnSc4
odlišují	odlišovat	k5eAaImIp3nP
v	v	k7c6
řadě	řada	k1gFnSc6
znaků	znak	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záporný	záporný	k2eAgMnSc1d1
hrdina	hrdina	k1gMnSc1
není	být	k5eNaImIp3nS
vždy	vždy	k6eAd1
vlk	vlk	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
někdy	někdy	k6eAd1
jde	jít	k5eAaImIp3nS
o	o	k7c4
obra	obr	k1gMnSc4
(	(	kIx(
<g/>
„	„	k?
<g/>
ogre	ogre	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
nebo	nebo	k8xC
vlkodlaka	vlkodlak	k1gMnSc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
se	se	k3xPyFc4
příběh	příběh	k1gInSc1
dotýká	dotýkat	k5eAaImIp3nS
tehdy	tehdy	k6eAd1
pořádaných	pořádaný	k2eAgInPc2d1
procesů	proces	k1gInPc2
s	s	k7c7
vlkodlaky	vlkodlak	k1gMnPc7
(	(	kIx(
<g/>
podobným	podobný	k2eAgInPc3d1
procesům	proces	k1gInPc3
s	s	k7c7
čarodějnicemi	čarodějnice	k1gFnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlk	Vlk	k1gMnSc1
obvykle	obvykle	k6eAd1
nechá	nechat	k5eAaPmIp3nS
dívku	dívka	k1gFnSc4
vypít	vypít	k5eAaPmF
babiččinu	babiččin	k2eAgFnSc4d1
krev	krev	k1gFnSc4
a	a	k8xC
sníst	sníst	k5eAaPmF
její	její	k3xOp3gNnSc4
maso	maso	k1gNnSc4
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
Karkulka	Karkulka	k1gFnSc1
na	na	k7c6
vlastní	vlastní	k2eAgFnSc6d1
babičce	babička	k1gFnSc6
bezděčně	bezděčně	k6eAd1
dopustí	dopustit	k5eAaPmIp3nS
kanibalismu	kanibalismus	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
verze	verze	k1gFnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
dívka	dívka	k1gFnSc1
ulehne	ulehnout	k5eAaPmIp3nS
k	k	k7c3
vlkovi	vlk	k1gMnSc3
do	do	k7c2
postele	postel	k1gFnSc2
<g/>
,	,	kIx,
prohlédne	prohlédnout	k5eAaPmIp3nS
jeho	jeho	k3xOp3gNnSc4
přestrojení	přestrojení	k1gNnSc4
a	a	k8xC
snaží	snažit	k5eAaImIp3nS
se	se	k3xPyFc4
utéct	utéct	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
výmluvu	výmluva	k1gFnSc4
„	„	k?
<g/>
babičce	babička	k1gFnSc3
<g/>
“	“	k?
použije	použít	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
potřebuje	potřebovat	k5eAaImIp3nS
vyprázdnit	vyprázdnit	k5eAaPmF
a	a	k8xC
opravdu	opravdu	k6eAd1
by	by	kYmCp3nS
nechtěla	chtít	k5eNaImAgFnS
přímo	přímo	k6eAd1
v	v	k7c6
posteli	postel	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlk	Vlk	k1gMnSc1
neochotně	ochotně	k6eNd1
souhlasí	souhlasit	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
přiváže	přivázat	k5eAaPmIp3nS
ji	on	k3xPp3gFnSc4
na	na	k7c4
provaz	provaz	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nemohla	moct	k5eNaImAgFnS
utéci	utéct	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dívka	dívka	k1gFnSc1
ale	ale	k8xC
provázek	provázek	k1gInSc1
uváže	uvázat	k5eAaPmIp3nS
na	na	k7c4
něco	něco	k3yInSc4
jiného	jiný	k2eAgNnSc2d1
a	a	k8xC
uteče	utéct	k5eAaPmIp3nS
mu	on	k3xPp3gMnSc3
k	k	k7c3
řece	řeka	k1gFnSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
dospělé	dospělý	k2eAgFnPc1d1
ženy	žena	k1gFnPc1
perou	prát	k5eAaImIp3nP
prádlo	prádlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karkulka	Karkulka	k1gFnSc1
uteče	utéct	k5eAaPmIp3nS
na	na	k7c4
druhý	druhý	k4xOgInSc4
břeh	břeh	k1gInSc4
a	a	k8xC
když	když	k8xS
se	se	k3xPyFc4
ji	on	k3xPp3gFnSc4
vlk	vlk	k1gMnSc1
snaží	snažit	k5eAaImIp3nS
dostihnout	dostihnout	k5eAaPmF
<g/>
,	,	kIx,
ženy	žena	k1gFnPc1
ho	on	k3xPp3gNnSc4
utopí	utopit	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s>
Charles	Charles	k1gMnSc1
Perrault	Perrault	k1gMnSc1
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3
známá	známý	k2eAgFnSc1d1
tištěná	tištěný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
jménem	jméno	k1gNnSc7
Le	Le	k1gFnSc2
Petit	petit	k1gInSc1
Chaperon	Chaperon	k1gMnSc1
Rouge	rouge	k1gFnPc2
pramení	pramenit	k5eAaImIp3nS
z	z	k7c2
francouzského	francouzský	k2eAgInSc2d1
folklóru	folklór	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
vydána	vydat	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1697	#num#	k4
ve	v	k7c6
sbírce	sbírka	k1gFnSc6
Pohádky	pohádka	k1gFnSc2
matky	matka	k1gFnSc2
Husy	Husa	k1gMnSc2
Charlese	Charles	k1gMnSc2
Perraulta	Perrault	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
verze	verze	k1gFnSc1
byla	být	k5eAaImAgFnS
daleko	daleko	k6eAd1
temnější	temný	k2eAgFnSc1d2
a	a	k8xC
také	také	k9
daleko	daleko	k6eAd1
zřetelněji	zřetelně	k6eAd2
moralizující	moralizující	k2eAgNnSc1d1
než	než	k8xS
ty	ty	k3xPp2nSc1
pozdější	pozdní	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Perrault	Perrault	k1gInSc1
do	do	k7c2
příběhu	příběh	k1gInSc2
vložil	vložit	k5eAaPmAgMnS
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
právě	právě	k6eAd1
červenou	červený	k2eAgFnSc4d1
čepičku	čepička	k1gFnSc4
<g/>
,	,	kIx,
resp.	resp.	kA
kapuci	kapuce	k1gFnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
mnohé	mnohé	k1gNnSc1
z	z	k7c2
výkladů	výklad	k1gInPc2
příběhu	příběh	k1gInSc2
připisují	připisovat	k5eAaImIp3nP
symbolický	symbolický	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
příběhu	příběh	k1gInSc6
prozradí	prozradit	k5eAaPmIp3nS
půvabná	půvabný	k2eAgFnSc1d1
a	a	k8xC
dobře	dobře	k6eAd1
vychovaná	vychovaný	k2eAgFnSc1d1
vesnická	vesnický	k2eAgFnSc1d1
dívka	dívka	k1gFnSc1
vlkovi	vlk	k1gMnSc6
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
potřebuje	potřebovat	k5eAaImIp3nS
vědět	vědět	k5eAaImF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
k	k	k7c3
domku	domek	k1gInSc6
její	její	k3xOp3gFnSc2
babičky	babička	k1gFnSc2
a	a	k8xC
tu	tu	k6eAd1
sežral	sežrat	k5eAaPmAgMnS
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nP
ho	on	k3xPp3gMnSc4
viděli	vidět	k5eAaImAgMnP
dřevorubci	dřevorubec	k1gMnPc1
pracující	pracující	k2eAgMnPc1d1
v	v	k7c6
nedalekém	daleký	k2eNgInSc6d1
lese	les	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příběh	příběh	k1gInSc1
pokračuje	pokračovat	k5eAaImIp3nS
nastražením	nastražení	k1gNnSc7
pasti	past	k1gFnSc2
na	na	k7c4
Červenou	červený	k2eAgFnSc4d1
karkulku	karkulka	k1gFnSc4
<g/>
,	,	kIx,
vlk	vlk	k1gMnSc1
ji	on	k3xPp3gFnSc4
sežere	sežrat	k5eAaPmIp3nS
a	a	k8xC
tím	ten	k3xDgNnSc7
příběh	příběh	k1gInSc1
končí	končit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlk	Vlk	k1gMnSc1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
vítězem	vítěz	k1gMnSc7
střetnutí	střetnutí	k1gNnSc2
a	a	k8xC
žádný	žádný	k1gMnSc1
happy	happa	k1gFnSc2
end	end	k?
se	se	k3xPyFc4
nekoná	konat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
osvícenství	osvícenství	k1gNnSc2
<g/>
,	,	kIx,
Perraultovy	Perraultův	k2eAgFnSc2d1
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
motivy	motiv	k1gInPc1
pohádek	pohádka	k1gFnPc2
silně	silně	k6eAd1
upravovány	upravován	k2eAgMnPc4d1
a	a	k8xC
přizpůsobovány	přizpůsobován	k2eAgMnPc4d1
módnímu	módní	k2eAgInSc3d1
rokokovému	rokokový	k2eAgInSc3d1
stylu	styl	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgFnP
prezentovány	prezentovat	k5eAaBmNgFnP
spíše	spíše	k9
s	s	k7c7
ironickým	ironický	k2eAgInSc7d1
odstupem	odstup	k1gInSc7
a	a	k8xC
s	s	k7c7
násilně	násilně	k6eAd1
připojovanými	připojovaný	k2eAgFnPc7d1
moralitami	moralita	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
verze	verze	k1gFnSc1
je	být	k5eAaImIp3nS
adaptací	adaptace	k1gFnSc7
příběhu	příběh	k1gInSc2
pro	pro	k7c4
kulturu	kultura	k1gFnSc4
francouzských	francouzský	k2eAgInPc2d1
salónů	salón	k1gInPc2
konce	konec	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
tedy	tedy	k9
pro	pro	k7c4
úplně	úplně	k6eAd1
jiné	jiný	k2eAgMnPc4d1
posluchače	posluchač	k1gMnPc4
než	než	k8xS
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
poučným	poučný	k2eAgInSc7d1
příběhem	příběh	k1gInSc7
<g/>
,	,	kIx,
varujícím	varující	k2eAgInSc7d1
ženy	žena	k1gFnPc1
před	před	k7c7
sbližováním	sbližování	k1gNnSc7
s	s	k7c7
muži	muž	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Bratři	bratr	k1gMnPc1
Grimmové	Grimmová	k1gFnSc2
</s>
<s>
V	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byly	být	k5eAaImAgFnP
J.	J.	kA
a	a	k8xC
V.	V.	kA
Grimmům	Grimm	k1gMnPc3
vyprávěny	vyprávěn	k2eAgFnPc4d1
dvě	dva	k4xCgFnPc4
různé	různý	k2eAgFnPc4d1
německé	německý	k2eAgFnPc4d1
verze	verze	k1gFnPc4
<g/>
;	;	kIx,
první	první	k4xOgFnSc1
Jeanette	Jeanett	k1gInSc5
Hassenpflugovou	Hassenpflugův	k2eAgFnSc7d1
(	(	kIx(
<g/>
1791	#num#	k4
<g/>
–	–	k?
<g/>
1860	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
druhá	druhý	k4xOgFnSc1
Marií	Maria	k1gFnSc7
Hassenpflugovou	Hassenpflugův	k2eAgFnSc7d1
(	(	kIx(
<g/>
1788	#num#	k4
<g/>
–	–	k?
<g/>
1856	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratři	bratr	k1gMnPc1
z	z	k7c2
první	první	k4xOgFnSc2
verze	verze	k1gFnSc2
vytvořili	vytvořit	k5eAaPmAgMnP
obsah	obsah	k1gInSc4
příběhu	příběh	k1gInSc2
a	a	k8xC
z	z	k7c2
druhé	druhý	k4xOgFnSc2
dodatek	dodatek	k1gInSc1
k	k	k7c3
němu	on	k3xPp3gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příběh	příběh	k1gInSc1
vyšel	vyjít	k5eAaPmAgInS
pod	pod	k7c7
jménem	jméno	k1gNnSc7
Rotkäppchen	Rotkäppchna	k1gFnPc2
v	v	k7c6
prvním	první	k4xOgNnSc6
vydání	vydání	k1gNnSc6
jejich	jejich	k3xOp3gFnSc2
sbírky	sbírka	k1gFnSc2
Kinder-	Kinder-	k1gMnSc7
und	und	k?
Hausmärchen	Hausmärchen	k2eAgMnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1812	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
část	část	k1gFnSc1
příběhu	příběh	k1gInSc2
se	se	k3xPyFc4
natolik	natolik	k6eAd1
kryje	krýt	k5eAaImIp3nS
s	s	k7c7
Perraultovou	Perraultový	k2eAgFnSc7d1
verzí	verze	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
z	z	k7c2
něj	on	k3xPp3gNnSc2
Grimmové	Grimmové	k2eAgNnSc2d1
téměř	téměř	k6eAd1
jistě	jistě	k6eAd1
čerpali	čerpat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konec	konec	k1gInSc1
je	být	k5eAaImIp3nS
ale	ale	k9
jiný	jiný	k2eAgInSc1d1
–	–	k?
dívka	dívka	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gFnSc1
babička	babička	k1gFnSc1
jsou	být	k5eAaImIp3nP
zachráněny	zachráněn	k2eAgInPc1d1
lovcem	lovec	k1gMnSc7
<g/>
;	;	kIx,
konec	konec	k1gInSc1
je	být	k5eAaImIp3nS
identický	identický	k2eAgMnSc1d1
se	s	k7c7
zakončením	zakončení	k1gNnSc7
příběhu	příběh	k1gInSc2
O	o	k7c6
vlku	vlk	k1gMnSc6
a	a	k8xC
sedmi	sedm	k4xCc6
kůzlátkách	kůzlátko	k1gNnPc6
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
je	být	k5eAaImIp3nS
zřejmě	zřejmě	k6eAd1
zdrojem	zdroj	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
druhé	druhý	k4xOgFnSc6
části	část	k1gFnSc6
příběhu	příběh	k1gInSc2
dívka	dívka	k1gFnSc1
a	a	k8xC
babička	babička	k1gFnSc1
chytí	chytit	k5eAaPmIp3nS
a	a	k8xC
zabijí	zabít	k5eAaPmIp3nP
jiného	jiný	k2eAgMnSc4d1
vlka	vlk	k1gMnSc4
<g/>
,	,	kIx,
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
přitom	přitom	k6eAd1
zkušenosti	zkušenost	k1gFnPc1
z	z	k7c2
předchozího	předchozí	k2eAgInSc2d1
zápasu	zápas	k1gInSc2
a	a	k8xC
předvídají	předvídat	k5eAaImIp3nP
jeho	jeho	k3xOp3gNnSc4
jednání	jednání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dívka	dívka	k1gFnSc1
mluvila	mluvit	k5eAaImAgFnS
s	s	k7c7
vlkem	vlk	k1gMnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
nedbala	nedbala	k1gMnSc1
jeho	jeho	k3xOp3gFnSc2
rady	rada	k1gFnSc2
a	a	k8xC
z	z	k7c2
pěšiny	pěšina	k1gFnSc2
nesešla	sejít	k5eNaPmAgFnS
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
nejrychleji	rychle	k6eAd3
běžela	běžet	k5eAaImAgFnS
k	k	k7c3
babičce	babička	k1gFnSc3
a	a	k8xC
společně	společně	k6eAd1
se	se	k3xPyFc4
zamkly	zamknout	k5eAaPmAgInP
v	v	k7c6
domku	domek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlk	Vlk	k1gMnSc1
ale	ale	k9
vylezl	vylézt	k5eAaPmAgMnS
na	na	k7c4
střechu	střecha	k1gFnSc4
a	a	k8xC
čekal	čekat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Babička	babička	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
obávala	obávat	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
si	se	k3xPyFc3
na	na	k7c4
Karkulku	Karkulka	k1gFnSc4
počíhal	počíhat	k5eAaPmAgMnS
na	na	k7c6
zpáteční	zpáteční	k2eAgFnSc6d1
cestě	cesta	k1gFnSc6
<g/>
,	,	kIx,
nechala	nechat	k5eAaPmAgFnS
dívku	dívka	k1gFnSc4
do	do	k7c2
necek	necky	k1gFnPc2
před	před	k7c7
domem	dům	k1gInSc7
nanosit	nanosit	k5eAaBmF
vodu	voda	k1gFnSc4
z	z	k7c2
jitrnic	jitrnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlk	Vlk	k1gMnSc1
čenichal	čenichat	k5eAaImAgMnS
<g/>
,	,	kIx,
nakláněl	naklánět	k5eAaImAgMnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
až	až	k8xS
spadl	spadnout	k5eAaPmAgInS
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
neckách	necky	k1gFnPc6
se	se	k3xPyFc4
utopil	utopit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
pozdějších	pozdní	k2eAgNnPc6d2
vydáních	vydání	k1gNnPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
editaci	editace	k1gFnSc3
pohádek	pohádka	k1gFnPc2
přebral	přebrat	k5eAaPmAgMnS
po	po	k7c6
Jacobovi	Jacob	k1gMnSc6
bratr	bratr	k1gMnSc1
Wilhelm	Wilhelm	k1gMnSc1
<g/>
,	,	kIx,
prošla	projít	k5eAaPmAgFnS
většina	většina	k1gFnSc1
příběhů	příběh	k1gInPc2
drobnými	drobný	k2eAgFnPc7d1
úpravami	úprava	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	s	k7c7
především	především	k6eAd1
o	o	k7c4
sjednocení	sjednocení	k1gNnSc4
stylu	styl	k1gInSc2
<g/>
,	,	kIx,
přidání	přidání	k1gNnSc4
přímé	přímý	k2eAgFnSc2d1
řeči	řeč	k1gFnSc2
a	a	k8xC
zjemnění	zjemnění	k1gNnSc2
temných	temný	k2eAgInPc2d1
motivů	motiv	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1857	#num#	k4
dosáhl	dosáhnout	k5eAaPmAgInS
příběh	příběh	k1gInSc1
dnešní	dnešní	k2eAgFnSc2d1
podoby	podoba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
této	tento	k3xDgFnSc2
verze	verze	k1gFnSc2
byl	být	k5eAaImAgInS
také	také	k9
pořízen	pořízen	k2eAgInSc1d1
český	český	k2eAgInSc1d1
překlad	překlad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Adaptace	adaptace	k1gFnSc1
</s>
<s>
Příběh	příběh	k1gInSc1
byl	být	k5eAaImAgInS
vícekrát	vícekrát	k6eAd1
zfilmován	zfilmovat	k5eAaPmNgInS
jako	jako	k8xS,k8xC
hraný	hraný	k2eAgInSc1d1
či	či	k8xC
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tři	tři	k4xCgMnPc1
bratři	bratr	k1gMnPc1
(	(	kIx(
<g/>
film	film	k1gInSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
česká	český	k2eAgFnSc1d1
filmová	filmový	k2eAgFnSc1d1
pohádka	pohádka	k1gFnSc1
režiséra	režisér	k1gMnSc2
Jana	Jan	k1gMnSc2
Svěráka	Svěrák	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
podle	podle	k7c2
dětských	dětský	k2eAgFnPc2d1
minioper	miniopera	k1gFnPc2
Zdeňka	Zdeněk	k1gMnSc2
Svěráka	Svěrák	k1gMnSc2
a	a	k8xC
Jaroslava	Jaroslav	k1gMnSc2
Uhlíře	Uhlíř	k1gMnSc2
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Volnou	volný	k2eAgFnSc7d1
parafrází	parafráze	k1gFnSc7
motivu	motiv	k1gInSc2
je	být	k5eAaImIp3nS
Karcoolka	Karcoolka	k1gFnSc1
(	(	kIx(
<g/>
Hoodwinked	Hoodwinked	k1gMnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
animovaná	animovaný	k2eAgFnSc1d1
parodie	parodie	k1gFnSc1
ve	v	k7c6
stylu	styl	k1gInSc6
saturninovské	saturninovský	k2eAgFnSc2d1
Kanceláře	kancelář	k1gFnSc2
pro	pro	k7c4
uvádění	uvádění	k1gNnSc4
románových	románový	k2eAgInPc2d1
příběhů	příběh	k1gInPc2
na	na	k7c4
pravou	pravý	k2eAgFnSc4d1
míru	míra	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
postupným	postupný	k2eAgNnSc7d1
opakováním	opakování	k1gNnSc7
děje	děj	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gNnSc7
doplňováním	doplňování	k1gNnSc7
z	z	k7c2
pohledu	pohled	k1gInSc2
dalších	další	k2eAgFnPc2d1
postav	postava	k1gFnPc2
ukazuje	ukazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
vše	všechen	k3xTgNnSc1
úplně	úplně	k6eAd1
jinak	jinak	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Motiv	motiv	k1gInSc1
červené	červený	k2eAgFnSc2d1
Karkulky	Karkulka	k1gFnSc2
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
Once	Onc	k1gInSc2
Upon	Upono	k1gNnPc2
a	a	k8xC
Time	Time	k1gFnPc2
(	(	kIx(
<g/>
u	u	k7c2
nás	my	k3xPp1nPc2
jako	jako	k8xC,k8xS
Bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
nebylo	být	k5eNaImAgNnS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
bere	brát	k5eAaImIp3nS
v	v	k7c4
potaz	potaz	k1gInSc4
skutečnost	skutečnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
Karkulka	Karkulka	k1gFnSc1
byla	být	k5eAaImAgFnS
doopravdy	doopravdy	k6eAd1
vlk	vlk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Inspirace	inspirace	k1gFnSc1
</s>
<s>
Postavou	postava	k1gFnSc7
Červené	Červené	k2eAgFnSc2d1
karkulky	karkulka	k1gFnSc2
se	se	k3xPyFc4
inspiroval	inspirovat	k5eAaBmAgMnS
malíř	malíř	k1gMnSc1
a	a	k8xC
fotograf	fotograf	k1gMnSc1
Anton	Anton	k1gMnSc1
Solomucha	Solomucha	k1gFnSc1
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
cyklech	cyklus	k1gInPc6
Červená	červený	k2eAgFnSc1d1
karkulka	karkulka	k1gFnSc1
na	na	k7c6
návštěvě	návštěva	k1gFnSc6
Louvru	Louvre	k1gInSc2
a	a	k8xC
Červená	červený	k2eAgFnSc1d1
karkulka	karkulka	k1gFnSc1
na	na	k7c6
návštěvě	návštěva	k1gFnSc6
Černobylu	Černobyl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anton	Anton	k1gMnSc1
Solomucha	Solomuch	k1gMnSc2
metodou	metoda	k1gFnSc7
klasického	klasický	k2eAgNnSc2d1
umění	umění	k1gNnSc2
a	a	k8xC
"	"	kIx"
<g/>
ironickou	ironický	k2eAgFnSc7d1
alegorií	alegorie	k1gFnSc7
<g/>
"	"	kIx"
vytvořil	vytvořit	k5eAaPmAgMnS
díla	dílo	k1gNnSc2
ve	v	k7c6
stylu	styl	k1gInSc6
připomínající	připomínající	k2eAgFnSc2d1
malby	malba	k1gFnSc2
v	v	k7c6
Louvru	Louvre	k1gInSc6
a	a	k8xC
zároveň	zároveň	k6eAd1
zobrazil	zobrazit	k5eAaPmAgInS
morbidní	morbidní	k2eAgFnPc4d1
scény	scéna	k1gFnPc4
černobylské	černobylský	k2eAgFnSc2d1
katastrofy	katastrofa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Básník	básník	k1gMnSc1
Jan	Jan	k1gMnSc1
Susa	Susa	k1gMnSc1
dosadil	dosadit	k5eAaPmAgMnS
Červené	Červené	k2eAgFnPc4d1
Karkulky	Karkulka	k1gFnPc4
v	v	k7c6
knize	kniha	k1gFnSc6
Červené	Červené	k2eAgFnSc2d1
Karkulky	Karkulka	k1gFnSc2
(	(	kIx(
<g/>
Amulet	amulet	k1gInSc1
2002	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
ilustracích	ilustrace	k1gFnPc6
Miloše	Miloš	k1gMnSc2
Nesvadby	Nesvadba	k1gMnSc2
do	do	k7c2
různých	různý	k2eAgInPc2d1
světadílů	světadíl	k1gInPc2
a	a	k8xC
zemí	zem	k1gFnPc2
a	a	k8xC
místo	místo	k7c2
vlka	vlk	k1gMnSc2
použil	použít	k5eAaPmAgMnS
místní	místní	k2eAgMnPc4d1
dravce	dravec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Internetová	internetový	k2eAgFnSc1d1
jazyková	jazykový	k2eAgFnSc1d1
příručka	příručka	k1gFnSc1
<g/>
:	:	kIx,
Jména	jméno	k1gNnPc4
s	s	k7c7
velkým	velký	k2eAgNnSc7d1
počátečním	počáteční	k2eAgNnSc7d1
písmenem	písmeno	k1gNnSc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústav	ústav	k1gInSc1
pro	pro	k7c4
jazyk	jazyk	k1gInSc4
česky	česky	k6eAd1
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2008-2020	2008-2020	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://www.osel.cz/index.php?clanek=7272	http://www.osel.cz/index.php?clanek=7272	k4
-	-	kIx~
Evoluční	evoluční	k2eAgInSc1d1
příběh	příběh	k1gInSc1
Červené	Červené	k2eAgFnSc2d1
karkulky	karkulka	k1gFnSc2
<g/>
↑	↑	k?
Filmová	filmový	k2eAgFnSc1d1
databáze	databáze	k1gFnSc1
FDb	FDb	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
vyhledávání	vyhledávání	k1gNnSc1
"	"	kIx"
<g/>
Červená	červený	k2eAgFnSc1d1
karkulka	karkulka	k1gFnSc1
<g/>
"	"	kIx"
<g/>
↑	↑	k?
http://www.csfd.cz/film/345205-tri-bratri/	http://www.csfd.cz/film/345205-tri-bratri/	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Červená	červený	k2eAgFnSc1d1
karkulka	karkulka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1
heslo	heslo	k1gNnSc4
Červená	červený	k2eAgFnSc1d1
karkulka	karkulka	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Dílo	dílo	k1gNnSc1
Červená	červenat	k5eAaImIp3nS
karkulka	karkulka	k1gFnSc1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
88005840	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
88005840	#num#	k4
</s>
