<p>
<s>
Organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
spolupráci	spolupráce	k1gFnSc4	spolupráce
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
OECD	OECD	kA	OECD
z	z	k7c2	z
angl.	angl.	k?	angl.
Organisation	Organisation	k1gInSc1	Organisation
for	forum	k1gNnPc2	forum
Economic	Economice	k1gInPc2	Economice
Co-operation	Coperation	k1gInSc4	Co-operation
and	and	k?	and
Development	Development	k1gInSc1	Development
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mezivládní	mezivládní	k2eAgFnSc1d1	mezivládní
organizace	organizace	k1gFnSc1	organizace
36	[number]	k4	36
velmi	velmi	k6eAd1	velmi
ekonomicky	ekonomicky	k6eAd1	ekonomicky
rozvinutých	rozvinutý	k2eAgInPc2d1	rozvinutý
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
přijaly	přijmout	k5eAaPmAgInP	přijmout
principy	princip	k1gInPc4	princip
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
tržní	tržní	k2eAgFnSc2d1	tržní
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
OECD	OECD	kA	OECD
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
(	(	kIx(	(
<g/>
zakládací	zakládací	k2eAgInSc1d1	zakládací
dokument	dokument	k1gInSc1	dokument
byl	být	k5eAaImAgInS	být
podepsán	podepsat	k5eAaPmNgInS	podepsat
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
transformací	transformace	k1gFnSc7	transformace
Organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
evropskou	evropský	k2eAgFnSc4d1	Evropská
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
spolupráci	spolupráce	k1gFnSc4	spolupráce
(	(	kIx(	(
<g/>
OEEC	OEEC	kA	OEEC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
zřízena	zřízen	k2eAgFnSc1d1	zřízena
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
k	k	k7c3	k
administraci	administrace	k1gFnSc3	administrace
poválečného	poválečný	k2eAgInSc2d1	poválečný
Marshallova	Marshallův	k2eAgInSc2d1	Marshallův
plánu	plán	k1gInSc2	plán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
OECD	OECD	kA	OECD
koordinuje	koordinovat	k5eAaBmIp3nS	koordinovat
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
a	a	k8xC	a
sociálně-politickou	sociálněolitický	k2eAgFnSc4d1	sociálně-politická
spolupráci	spolupráce	k1gFnSc4	spolupráce
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
zprostředkovává	zprostředkovávat	k5eAaImIp3nS	zprostředkovávat
nové	nový	k2eAgFnPc4d1	nová
investice	investice	k1gFnPc4	investice
<g/>
,	,	kIx,	,
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
liberalizaci	liberalizace	k1gFnSc4	liberalizace
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
OECD	OECD	kA	OECD
je	být	k5eAaImIp3nS	být
napomáhat	napomáhat	k5eAaImF	napomáhat
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
ekonomickému	ekonomický	k2eAgInSc3d1	ekonomický
rozvoji	rozvoj	k1gInSc3	rozvoj
<g/>
,	,	kIx,	,
potlačení	potlačení	k1gNnSc3	potlačení
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
<g/>
,	,	kIx,	,
stabilizaci	stabilizace	k1gFnSc6	stabilizace
a	a	k8xC	a
rozvoji	rozvoj	k1gInSc6	rozvoj
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
finančních	finanční	k2eAgInPc2d1	finanční
trhů	trh	k1gInPc2	trh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgInPc4d1	hlavní
orgány	orgán	k1gInPc4	orgán
OECD	OECD	kA	OECD
patří	patřit	k5eAaImIp3nS	patřit
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
ministrů	ministr	k1gMnPc2	ministr
<g/>
,	,	kIx,	,
coby	coby	k?	coby
zástupců	zástupce	k1gMnPc2	zástupce
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
výkonný	výkonný	k2eAgInSc1d1	výkonný
výbor	výbor	k1gInSc1	výbor
<g/>
,	,	kIx,	,
sekretariát	sekretariát	k1gInSc1	sekretariát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
generálním	generální	k2eAgMnSc7d1	generální
tajemníkem	tajemník	k1gMnSc7	tajemník
a	a	k8xC	a
několik	několik	k4yIc4	několik
odborných	odborný	k2eAgFnPc2d1	odborná
komisí	komise	k1gFnPc2	komise
<g/>
.	.	kIx.	.
</s>
<s>
Sídlem	sídlo	k1gNnSc7	sídlo
sekretariátu	sekretariát	k1gInSc2	sekretariát
OECD	OECD	kA	OECD
je	být	k5eAaImIp3nS	být
Château	château	k1gNnSc4	château
de	de	k?	de
la	la	k1gNnSc2	la
Muette	Muett	k1gInSc5	Muett
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Předchůdcem	předchůdce	k1gMnSc7	předchůdce
OECD	OECD	kA	OECD
byla	být	k5eAaImAgFnS	být
Organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
Evropskou	evropský	k2eAgFnSc4d1	Evropská
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
spolupráci	spolupráce	k1gFnSc4	spolupráce
(	(	kIx(	(
<g/>
Organisation	Organisation	k1gInSc1	Organisation
for	forum	k1gNnPc2	forum
European	Europeana	k1gFnPc2	Europeana
Economic	Economice	k1gFnPc2	Economice
Cooperation	Cooperation	k1gInSc1	Cooperation
<g/>
,	,	kIx,	,
OEEC	OEEC	kA	OEEC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
účelem	účel	k1gInSc7	účel
bylo	být	k5eAaImAgNnS	být
zejména	zejména	k9	zejména
rozdělování	rozdělování	k1gNnSc4	rozdělování
prostředků	prostředek	k1gInPc2	prostředek
Marshallova	Marshallův	k2eAgInSc2d1	Marshallův
plánu	plán	k1gInSc2	plán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
a	a	k8xC	a
měnová	měnový	k2eAgFnSc1d1	měnová
spolupráce	spolupráce	k1gFnSc1	spolupráce
a	a	k8xC	a
odstraňování	odstraňování	k1gNnSc1	odstraňování
obchodních	obchodní	k2eAgFnPc2d1	obchodní
překážek	překážka	k1gFnPc2	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Organizaci	organizace	k1gFnSc4	organizace
založilo	založit	k5eAaPmAgNnS	založit
17	[number]	k4	17
západoevropských	západoevropský	k2eAgMnPc2d1	západoevropský
států	stát	k1gInPc2	stát
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
německé	německý	k2eAgFnPc4d1	německá
okupační	okupační	k2eAgFnPc4d1	okupační
zóny	zóna	k1gFnPc4	zóna
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
státy	stát	k1gInPc1	stát
přijímající	přijímající	k2eAgFnSc4d1	přijímající
pomoc	pomoc	k1gFnSc4	pomoc
z	z	k7c2	z
Marshallova	Marshallův	k2eAgInSc2d1	Marshallův
plánu	plán	k1gInSc2	plán
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
nich	on	k3xPp3gFnPc2	on
Marshallův	Marshallův	k2eAgInSc4d1	Marshallův
plán	plán	k1gInSc4	plán
přijala	přijmout	k5eAaPmAgFnS	přijmout
také	také	k9	také
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
a	a	k8xC	a
Svobodné	svobodný	k2eAgNnSc1d1	svobodné
území	území	k1gNnSc1	území
Terst	Terst	k1gInSc1	Terst
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
připojilo	připojit	k5eAaPmAgNnS	připojit
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Cíle	cíl	k1gInPc1	cíl
a	a	k8xC	a
činnost	činnost	k1gFnSc1	činnost
==	==	k?	==
</s>
</p>
<p>
<s>
OECD	OECD	kA	OECD
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
prostředí	prostředí	k1gNnSc1	prostředí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
mohou	moct	k5eAaImIp3nP	moct
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
vlády	vláda	k1gFnPc1	vláda
vzájemně	vzájemně	k6eAd1	vzájemně
porovnávat	porovnávat	k5eAaImF	porovnávat
svoje	svůj	k3xOyFgFnPc4	svůj
zkušenosti	zkušenost	k1gFnPc4	zkušenost
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
typy	typ	k1gInPc7	typ
politik	politika	k1gFnPc2	politika
<g/>
,	,	kIx,	,
hledat	hledat	k5eAaImF	hledat
odpovědi	odpověď	k1gFnPc4	odpověď
na	na	k7c4	na
běžné	běžný	k2eAgInPc4d1	běžný
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
poznat	poznat	k5eAaPmF	poznat
dobře	dobře	k6eAd1	dobře
fungující	fungující	k2eAgFnSc4d1	fungující
praxi	praxe	k1gFnSc4	praxe
a	a	k8xC	a
koordinovat	koordinovat	k5eAaBmF	koordinovat
domácí	domácí	k2eAgFnSc4d1	domácí
a	a	k8xC	a
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
Mandát	mandát	k1gInSc1	mandát
OECD	OECD	kA	OECD
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
široký	široký	k2eAgInSc1d1	široký
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
řadu	řada	k1gFnSc4	řada
záležitostí	záležitost	k1gFnPc2	záležitost
vztahujících	vztahující	k2eAgFnPc2d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
ekonomice	ekonomika	k1gFnSc3	ekonomika
<g/>
,	,	kIx,	,
životnímu	životní	k2eAgNnSc3d1	životní
prostředí	prostředí	k1gNnSc3	prostředí
a	a	k8xC	a
sociální	sociální	k2eAgFnSc3d1	sociální
politice	politika	k1gFnSc3	politika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
fórum	fórum	k1gNnSc1	fórum
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
tlak	tlak	k1gInSc1	tlak
může	moct	k5eAaImIp3nS	moct
působit	působit	k5eAaImF	působit
jako	jako	k9	jako
silný	silný	k2eAgInSc4d1	silný
motivační	motivační	k2eAgInSc4d1	motivační
prostředek	prostředek	k1gInSc4	prostředek
k	k	k7c3	k
zavádění	zavádění	k1gNnSc3	zavádění
tzv.	tzv.	kA	tzv.
měkkého	měkký	k2eAgNnSc2d1	měkké
práva	právo	k1gNnSc2	právo
–	–	k?	–
nesvazujících	svazující	k2eNgInPc2d1	svazující
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
někdy	někdy	k6eAd1	někdy
vést	vést	k5eAaImF	vést
i	i	k9	i
k	k	k7c3	k
závazným	závazný	k2eAgFnPc3d1	závazná
dohodám	dohoda	k1gFnPc3	dohoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spolupráce	spolupráce	k1gFnSc1	spolupráce
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
vládami	vláda	k1gFnPc7	vláda
pramení	pramenit	k5eAaImIp3nS	pramenit
z	z	k7c2	z
informací	informace	k1gFnPc2	informace
a	a	k8xC	a
analýz	analýza	k1gFnPc2	analýza
<g/>
,	,	kIx,	,
poskytovaných	poskytovaný	k2eAgFnPc2d1	poskytovaná
sekretariátem	sekretariát	k1gInSc7	sekretariát
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
shromažďuje	shromažďovat	k5eAaImIp3nS	shromažďovat
data	datum	k1gNnPc1	datum
<g/>
,	,	kIx,	,
monitoruje	monitorovat	k5eAaImIp3nS	monitorovat
trendy	trend	k1gInPc4	trend
a	a	k8xC	a
analyzuje	analyzovat	k5eAaImIp3nS	analyzovat
a	a	k8xC	a
předpovídá	předpovídat	k5eAaImIp3nS	předpovídat
další	další	k2eAgInSc4d1	další
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
také	také	k9	také
zachytit	zachytit	k5eAaPmF	zachytit
sociální	sociální	k2eAgFnPc4d1	sociální
změny	změna	k1gFnPc4	změna
nebo	nebo	k8xC	nebo
rozvoj	rozvoj	k1gInSc4	rozvoj
v	v	k7c6	v
obchodu	obchod	k1gInSc6	obchod
<g/>
,	,	kIx,	,
ochraně	ochrana	k1gFnSc6	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
technologiích	technologie	k1gFnPc6	technologie
<g/>
,	,	kIx,	,
zdanění	zdanění	k1gNnSc6	zdanění
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
OECD	OECD	kA	OECD
je	být	k5eAaImIp3nS	být
také	také	k9	také
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
statistických	statistický	k2eAgFnPc2d1	statistická
agentur	agentura	k1gFnPc2	agentura
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
publikuje	publikovat	k5eAaBmIp3nS	publikovat
velmi	velmi	k6eAd1	velmi
zajímavé	zajímavý	k2eAgInPc4d1	zajímavý
průzkumy	průzkum	k1gInPc4	průzkum
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
informací	informace	k1gFnPc2	informace
a	a	k8xC	a
širokým	široký	k2eAgNnSc7d1	široké
zaměřením	zaměření	k1gNnSc7	zaměření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
uplynulém	uplynulý	k2eAgNnSc6d1	uplynulé
desetiletí	desetiletí	k1gNnSc6	desetiletí
OECD	OECD	kA	OECD
vyřešilo	vyřešit	k5eAaPmAgNnS	vyřešit
řadu	řada	k1gFnSc4	řada
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
<g/>
,	,	kIx,	,
sociálních	sociální	k2eAgFnPc2d1	sociální
a	a	k8xC	a
environmentálních	environmentální	k2eAgFnPc2d1	environmentální
záležitostí	záležitost	k1gFnPc2	záležitost
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
dále	daleko	k6eAd2	daleko
prohlubovalo	prohlubovat	k5eAaImAgNnS	prohlubovat
svoje	svůj	k3xOyFgNnSc1	svůj
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
podniky	podnik	k1gInPc7	podnik
<g/>
,	,	kIx,	,
obchodními	obchodní	k2eAgFnPc7d1	obchodní
uniemi	unie	k1gFnPc7	unie
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
zástupci	zástupce	k1gMnPc7	zástupce
civilní	civilní	k2eAgFnSc2d1	civilní
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Vyjednávání	vyjednávání	k1gNnSc1	vyjednávání
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
OECD	OECD	kA	OECD
ohledně	ohledně	k7c2	ohledně
zdanění	zdanění	k1gNnSc2	zdanění
a	a	k8xC	a
celních	celní	k2eAgInPc2d1	celní
poplatků	poplatek	k1gInPc2	poplatek
například	například	k6eAd1	například
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
mnoha	mnoho	k4c2	mnoho
bilaterálních	bilaterální	k2eAgFnPc2d1	bilaterální
daňových	daňový	k2eAgFnPc2d1	daňová
dohod	dohoda	k1gFnPc2	dohoda
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
si	se	k3xPyFc3	se
vzalo	vzít	k5eAaPmAgNnS	vzít
OECD	OECD	kA	OECD
také	také	k9	také
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
koordinovat	koordinovat	k5eAaBmF	koordinovat
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
boj	boj	k1gInSc4	boj
s	s	k7c7	s
politickou	politický	k2eAgFnSc7d1	politická
korupcí	korupce	k1gFnSc7	korupce
a	a	k8xC	a
úplatkářstvím	úplatkářství	k1gNnSc7	úplatkářství
a	a	k8xC	a
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
účely	účel	k1gInPc4	účel
Dohodu	dohoda	k1gFnSc4	dohoda
proti	proti	k7c3	proti
úplatkářství	úplatkářství	k1gNnSc3	úplatkářství
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
OECD	OECD	kA	OECD
Anti-Bribery	Anti-Briber	k1gInPc1	Anti-Briber
Convention	Convention	k1gInSc1	Convention
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vešla	vejít	k5eAaPmAgFnS	vejít
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
OECD	OECD	kA	OECD
ustavilo	ustavit	k5eAaPmAgNnS	ustavit
také	také	k6eAd1	také
pracovní	pracovní	k2eAgInSc4d1	pracovní
tým	tým	k1gInSc4	tým
zabývající	zabývající	k2eAgInSc4d1	zabývající
se	s	k7c7	s
spamem	spam	k1gInSc7	spam
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
detailní	detailní	k2eAgFnSc4d1	detailní
zprávu	zpráva	k1gFnSc4	zpráva
s	s	k7c7	s
několika	několik	k4yIc7	několik
poměrně	poměrně	k6eAd1	poměrně
zajímavými	zajímavý	k2eAgFnPc7d1	zajímavá
informacemi	informace	k1gFnPc7	informace
<g/>
,	,	kIx,	,
vztahujícími	vztahující	k2eAgFnPc7d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
problematice	problematika	k1gFnSc3	problematika
(	(	kIx(	(
<g/>
např.	např.	kA	např.
stav	stav	k1gInSc1	stav
v	v	k7c6	v
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
nebo	nebo	k8xC	nebo
nejlepší	dobrý	k2eAgFnPc1d3	nejlepší
praktiky	praktika	k1gFnPc1	praktika
pro	pro	k7c4	pro
ISP	ISP	kA	ISP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členské	členský	k2eAgInPc1d1	členský
státy	stát	k1gInPc1	stát
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
má	mít	k5eAaImIp3nS	mít
OECD	OECD	kA	OECD
36	[number]	k4	36
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
OECD	OECD	kA	OECD
blízce	blízce	k6eAd1	blízce
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
v	v	k7c6	v
režimu	režim	k1gInSc6	režim
rozšířené	rozšířený	k2eAgFnSc2d1	rozšířená
spolupráce	spolupráce	k1gFnSc2	spolupráce
také	také	k9	také
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Indonésie	Indonésie	k1gFnSc1	Indonésie
<g/>
,	,	kIx,	,
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
OECD	OECD	kA	OECD
také	také	k9	také
zvažuje	zvažovat	k5eAaImIp3nS	zvažovat
žádosti	žádost	k1gFnPc4	žádost
o	o	k7c4	o
členství	členství	k1gNnSc4	členství
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
,	,	kIx,	,
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
,	,	kIx,	,
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
,	,	kIx,	,
Peru	Peru	k1gNnSc2	Peru
a	a	k8xC	a
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgInPc2d1	další
25	[number]	k4	25
států	stát	k1gInPc2	stát
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
nebo	nebo	k8xC	nebo
pozorovatelem	pozorovatel	k1gMnSc7	pozorovatel
v	v	k7c6	v
odborných	odborný	k2eAgFnPc6d1	odborná
komisích	komise	k1gFnPc6	komise
OECD	OECD	kA	OECD
<g/>
,	,	kIx,	,
dalších	další	k2eAgMnPc2d1	další
50	[number]	k4	50
pak	pak	k6eAd1	pak
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
aktivitách	aktivita	k1gFnPc6	aktivita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Organisation	Organisation	k1gInSc4	Organisation
for	forum	k1gNnPc2	forum
Economic	Economice	k1gInPc2	Economice
Co-operation	Coperation	k1gInSc4	Co-operation
and	and	k?	and
Development	Development	k1gInSc1	Development
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
spolupráci	spolupráce	k1gFnSc4	spolupráce
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://www.oecd.org	[url]	k1gMnSc1	http://www.oecd.org
</s>
</p>
<p>
<s>
stručná	stručný	k2eAgFnSc1d1	stručná
videoprezentace	videoprezentace	k1gFnSc1	videoprezentace
tamtéž	tamtéž	k6eAd1	tamtéž
(	(	kIx(	(
<g/>
aj	aj	kA	aj
<g/>
)	)	kIx)	)
http://www.oecd.org/pages/0,3417,en_36734052_36734103_1_1_1_1_1,00.html	[url]	k1gMnSc1	http://www.oecd.org/pages/0,3417,en_36734052_36734103_1_1_1_1_1,00.html
</s>
</p>
<p>
<s>
zakládací	zakládací	k2eAgInSc1d1	zakládací
dokument	dokument	k1gInSc1	dokument
(	(	kIx(	(
<g/>
aj	aj	kA	aj
<g/>
)	)	kIx)	)
Convention	Convention	k1gInSc1	Convention
on	on	k3xPp3gInSc1	on
the	the	k?	the
Organisation	Organisation	k1gInSc1	Organisation
for	forum	k1gNnPc2	forum
Economic	Economice	k1gInPc2	Economice
Co-operation	Coperation	k1gInSc4	Co-operation
and	and	k?	and
Development	Development	k1gInSc1	Development
</s>
</p>
<p>
<s>
Stálá	stálý	k2eAgFnSc1d1	stálá
mise	mise	k1gFnSc1	mise
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
při	při	k7c6	při
OECD	OECD	kA	OECD
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
</s>
</p>
<p>
<s>
Stručný	stručný	k2eAgInSc1d1	stručný
článek	článek	k1gInSc1	článek
na	na	k7c6	na
serveru	server	k1gInSc6	server
Svazu	svaz	k1gInSc2	svaz
průmyslu	průmysl	k1gInSc2	průmysl
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
na	na	k7c6	na
serveru	server	k1gInSc6	server
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
"	"	kIx"	"
<g/>
o	o	k7c6	o
aktivitách	aktivita	k1gFnPc6	aktivita
OECD	OECD	kA	OECD
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
ochrany	ochrana	k1gFnSc2	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
trvale	trvale	k6eAd1	trvale
udržitelného	udržitelný	k2eAgInSc2d1	udržitelný
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Články	článek	k1gInPc1	článek
na	na	k7c6	na
serveru	server	k1gInSc6	server
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
o	o	k7c6	o
OECD	OECD	kA	OECD
</s>
</p>
