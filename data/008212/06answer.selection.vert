<s>
Organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
spolupráci	spolupráce	k1gFnSc4	spolupráce
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
OECD	OECD	kA	OECD
z	z	k7c2	z
angl.	angl.	k?	angl.
Organisation	Organisation	k1gInSc1	Organisation
for	forum	k1gNnPc2	forum
Economic	Economice	k1gInPc2	Economice
Co-operation	Coperation	k1gInSc4	Co-operation
and	and	k?	and
Development	Development	k1gInSc1	Development
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mezivládní	mezivládní	k2eAgFnSc1d1	mezivládní
organizace	organizace	k1gFnSc1	organizace
36	[number]	k4	36
velmi	velmi	k6eAd1	velmi
ekonomicky	ekonomicky	k6eAd1	ekonomicky
rozvinutých	rozvinutý	k2eAgInPc2d1	rozvinutý
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
přijaly	přijmout	k5eAaPmAgInP	přijmout
principy	princip	k1gInPc4	princip
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
tržní	tržní	k2eAgFnSc2d1	tržní
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
