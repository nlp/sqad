<s>
Bezbarvý	bezbarvý	k2eAgInSc1d1	bezbarvý
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
chuti	chuť	k1gFnSc2	chuť
a	a	k8xC	a
zápachu	zápach	k1gInSc2	zápach
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
zcela	zcela	k6eAd1	zcela
inertní	inertní	k2eAgNnPc1d1	inertní
-	-	kIx~	-
helium	helium	k1gNnSc1	helium
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
sloučeniny	sloučenina	k1gFnSc2	sloučenina
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
fullereny	fulleren	k1gInPc7	fulleren
a	a	k8xC	a
se	s	k7c7	s
rtutí	rtuť	k1gFnSc7	rtuť
(	(	kIx(	(
<g/>
helidy	helida	k1gFnSc2	helida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
