<p>
<s>
Stejnopohlavní	Stejnopohlavní	k2eAgNnSc1d1	Stejnopohlavní
manželství	manželství	k1gNnSc1	manželství
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
legální	legální	k2eAgFnSc1d1	legální
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
legalizaci	legalizace	k1gFnSc6	legalizace
zpracovaný	zpracovaný	k2eAgMnSc1d1	zpracovaný
vládou	vláda	k1gFnSc7	vláda
Helle	Helle	k1gFnSc2	Helle
Thorningové-Schmidtové	Thorningové-Schmidtová	k1gFnSc2	Thorningové-Schmidtová
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
Folketingem	Folketing	k1gInSc7	Folketing
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
a	a	k8xC	a
schválen	schválit	k5eAaPmNgInS	schválit
královnou	královna	k1gFnSc7	královna
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Homosexuální	homosexuální	k2eAgInPc1d1	homosexuální
páry	pár	k1gInPc1	pár
mohly	moct	k5eAaImAgInP	moct
už	už	k6eAd1	už
předtím	předtím	k6eAd1	předtím
uzavírat	uzavírat	k5eAaPmF	uzavírat
registrované	registrovaný	k2eAgNnSc4d1	registrované
partnerství	partnerství	k1gNnSc4	partnerství
<g/>
.	.	kIx.	.
</s>
<s>
Dánsko	Dánsko	k1gNnSc1	Dánsko
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
jedenáctou	jedenáctý	k4xOgFnSc7	jedenáctý
zemí	zem	k1gFnSc7	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
legalizovala	legalizovat	k5eAaBmAgFnS	legalizovat
stejnopohlavní	stejnopohlavní	k2eAgInPc4d1	stejnopohlavní
sňatky	sňatek	k1gInPc4	sňatek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Postavení	postavení	k1gNnSc1	postavení
manželství	manželství	k1gNnSc2	manželství
je	být	k5eAaImIp3nS	být
různé	různý	k2eAgNnSc1d1	různé
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Dánsko	Dánsko	k1gNnSc1	Dánsko
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgNnPc4	dva
autonomní	autonomní	k2eAgNnPc4d1	autonomní
území	území	k1gNnPc4	území
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Grónský	grónský	k2eAgInSc1d1	grónský
parlament	parlament	k1gInSc1	parlament
přijal	přijmout	k5eAaPmAgInS	přijmout
zákon	zákon	k1gInSc4	zákon
o	o	k7c6	o
stejnopohlavním	stejnopohlavní	k2eAgNnSc6d1	stejnopohlavní
manželství	manželství	k1gNnSc6	manželství
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Dánský	dánský	k2eAgInSc1d1	dánský
parlament	parlament	k1gInSc1	parlament
Folketing	Folketing	k1gInSc1	Folketing
ratifikoval	ratifikovat	k5eAaBmAgInS	ratifikovat
tuto	tento	k3xDgFnSc4	tento
legislativu	legislativa	k1gFnSc4	legislativa
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Účinnosti	účinnost	k1gFnSc3	účinnost
nabyla	nabýt	k5eAaPmAgFnS	nabýt
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubnu	duben	k1gInSc3	duben
2016	[number]	k4	2016
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Faerských	Faerský	k2eAgInPc6d1	Faerský
ostrovech	ostrov	k1gInPc6	ostrov
byl	být	k5eAaImAgInS	být
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
stejnopohlavním	stejnopohlavní	k2eAgNnSc6d1	stejnopohlavní
manželství	manželství	k1gNnSc6	manželství
přijat	přijat	k2eAgMnSc1d1	přijat
Lø	Lø	k1gMnSc1	Lø
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
nová	nový	k2eAgFnSc1d1	nová
legislativa	legislativa	k1gFnSc1	legislativa
stane	stanout	k5eAaPmIp3nS	stanout
účinnou	účinný	k2eAgFnSc4d1	účinná
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
přijatá	přijatý	k2eAgNnPc4d1	přijaté
dánským	dánský	k2eAgInSc7d1	dánský
parlamentem	parlament	k1gInSc7	parlament
a	a	k8xC	a
podepsána	podepsat	k5eAaPmNgFnS	podepsat
královnou	královna	k1gFnSc7	královna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dánsko	Dánsko	k1gNnSc4	Dánsko
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Registrované	registrovaný	k2eAgNnSc1d1	registrované
partnerství	partnerství	k1gNnSc1	partnerství
====	====	k?	====
</s>
</p>
<p>
<s>
Dánsko	Dánsko	k1gNnSc1	Dánsko
přijalo	přijmout	k5eAaPmAgNnS	přijmout
zákon	zákon	k1gInSc4	zákon
o	o	k7c6	o
registrovaném	registrovaný	k2eAgNnSc6d1	registrované
partnerství	partnerství	k1gNnSc6	partnerství
(	(	kIx(	(
<g/>
dánsky	dánsky	k6eAd1	dánsky
<g/>
:	:	kIx,	:
registretet	registretet	k1gMnSc1	registretet
partnerskab	partnerskab	k1gMnSc1	partnerskab
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
první	první	k4xOgFnSc2	první
země	zem	k1gFnSc2	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Účinnosti	účinnost	k1gFnPc4	účinnost
nabyl	nabýt	k5eAaPmAgInS	nabýt
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2003	[number]	k4	2003
a	a	k8xC	a
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2006	[number]	k4	2006
byly	být	k5eAaImAgInP	být
uskutečněny	uskutečněn	k2eAgInPc1d1	uskutečněn
tři	tři	k4xCgInPc1	tři
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
rozšíření	rozšíření	k1gNnSc4	rozšíření
tohoto	tento	k3xDgInSc2	tento
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
neprošel	projít	k5eNaPmAgInS	projít
parlamentem	parlament	k1gInSc7	parlament
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
úspěšnému	úspěšný	k2eAgInSc3d1	úspěšný
rozšíření	rozšíření	k1gNnSc3	rozšíření
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2009	[number]	k4	2009
a	a	k8xC	a
květnu	květen	k1gInSc6	květen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
<g/>
Registrované	registrovaný	k2eAgNnSc1d1	registrované
partnerství	partnerství	k1gNnSc1	partnerství
se	se	k3xPyFc4	se
obsahem	obsah	k1gInSc7	obsah
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
povinností	povinnost	k1gFnPc2	povinnost
téměř	téměř	k6eAd1	téměř
nelišilo	lišit	k5eNaImAgNnS	lišit
od	od	k7c2	od
manželství	manželství	k1gNnSc2	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Veškerá	veškerý	k3xTgNnPc1	veškerý
práva	právo	k1gNnPc1	právo
a	a	k8xC	a
povinnosti	povinnost	k1gFnPc1	povinnost
mezi	mezi	k7c7	mezi
partnery	partner	k1gMnPc7	partner
a	a	k8xC	a
majetková	majetkový	k2eAgNnPc4d1	majetkové
práva	právo	k1gNnPc4	právo
byla	být	k5eAaImAgFnS	být
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k9	jako
u	u	k7c2	u
heterosexuálních	heterosexuální	k2eAgMnPc2d1	heterosexuální
manželů	manžel	k1gMnPc2	manžel
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
dvou	dva	k4xCgFnPc2	dva
výjimek	výjimka	k1gFnPc2	výjimka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
zákon	zákon	k1gInSc1	zákon
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
manželství	manželství	k1gNnSc2	manželství
nezmiňuje	zmiňovat	k5eNaImIp3nS	zmiňovat
o	o	k7c4	o
pohlaví	pohlaví	k1gNnPc4	pohlaví
partnerů	partner	k1gMnPc2	partner
</s>
</p>
<p>
<s>
nevztahovaly	vztahovat	k5eNaImAgFnP	vztahovat
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
všechny	všechen	k3xTgFnPc4	všechen
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
smlouvy	smlouva	k1gFnPc4	smlouva
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
daný	daný	k2eAgMnSc1d1	daný
signatář	signatář	k1gMnSc1	signatář
nesouhlasilRozvod	nesouhlasilRozvoda	k1gFnPc2	nesouhlasilRozvoda
registrovaného	registrovaný	k2eAgNnSc2d1	registrované
partnerství	partnerství	k1gNnSc2	partnerství
se	se	k3xPyFc4	se
řídil	řídit	k5eAaImAgMnS	řídit
stejnými	stejný	k2eAgNnPc7d1	stejné
pravidly	pravidlo	k1gNnPc7	pravidlo
jako	jako	k9	jako
manželství	manželství	k1gNnSc1	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Partneři	partner	k1gMnPc1	partner
museli	muset	k5eAaImAgMnP	muset
splnit	splnit	k5eAaPmF	splnit
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
následujících	následující	k2eAgInPc2d1	následující
požadavků	požadavek	k1gInPc2	požadavek
trvalého	trvalý	k2eAgInSc2d1	trvalý
pobytu	pobyt	k1gInSc2	pobyt
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
být	být	k5eAaImF	být
registrováni	registrovat	k5eAaBmNgMnP	registrovat
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
partnerů	partner	k1gMnPc2	partner
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
dánským	dánský	k2eAgMnSc7d1	dánský
občanem	občan	k1gMnSc7	občan
a	a	k8xC	a
mít	mít	k5eAaImF	mít
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
trvalý	trvalý	k2eAgInSc4d1	trvalý
pobyt	pobyt	k1gInSc4	pobyt
nebo	nebo	k8xC	nebo
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
oba	dva	k4xCgMnPc1	dva
partneři	partner	k1gMnPc1	partner
musejí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
trvalý	trvalý	k2eAgInSc4d1	trvalý
pobyt	pobyt	k1gInSc4	pobyt
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
trvání	trvání	k1gNnSc2	trvání
minimálně	minimálně	k6eAd1	minimálně
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
občany	občan	k1gMnPc7	občan
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
Islandu	Island	k1gInSc2	Island
a	a	k8xC	a
Norska	Norsko	k1gNnSc2	Norsko
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
účely	účel	k1gInPc4	účel
zacházelo	zacházet	k5eAaImAgNnS	zacházet
stejně	stejně	k9	stejně
jako	jako	k9	jako
by	by	kYmCp3nP	by
byli	být	k5eAaImAgMnP	být
Dánové	Dán	k1gMnPc1	Dán
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
mohlo	moct	k5eAaImAgNnS	moct
podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
zákona	zákon	k1gInSc2	zákon
udělit	udělit	k5eAaPmF	udělit
občanovi	občan	k1gMnSc3	občan
jakékoli	jakýkoli	k3yIgFnSc3	jakýkoli
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
zákonem	zákon	k1gInSc7	zákon
podobného	podobný	k2eAgInSc2d1	podobný
charakteru	charakter	k1gInSc2	charakter
udělit	udělit	k5eAaPmF	udělit
status	status	k1gInSc4	status
dánského	dánský	k2eAgMnSc2d1	dánský
občana	občan	k1gMnSc2	občan
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
r.	r.	kA	r.
2006	[number]	k4	2006
získaly	získat	k5eAaPmAgFnP	získat
lesbické	lesbický	k2eAgFnPc1d1	lesbická
ženy	žena	k1gFnPc1	žena
a	a	k8xC	a
páry	pára	k1gFnPc1	pára
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
asistované	asistovaný	k2eAgFnSc3d1	asistovaná
reprodukci	reprodukce	k1gFnSc3	reprodukce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2009	[number]	k4	2009
zpracoval	zpracovat	k5eAaPmAgInS	zpracovat
Folketing	Folketing	k1gInSc4	Folketing
návrh	návrh	k1gInSc1	návrh
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
by	by	kYmCp3nS	by
homosexuálním	homosexuální	k2eAgInSc7d1	homosexuální
párům	pár	k1gInPc3	pár
žijícím	žijící	k2eAgInPc3d1	žijící
v	v	k7c6	v
registrovaném	registrovaný	k2eAgNnSc6d1	registrované
partnerství	partnerství	k1gNnSc6	partnerství
umožnil	umožnit	k5eAaPmAgInS	umožnit
společné	společný	k2eAgNnSc4d1	společné
osvojení	osvojení	k1gNnSc4	osvojení
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2010	[number]	k4	2010
a	a	k8xC	a
účinnosti	účinnost	k1gFnSc2	účinnost
nabyl	nabýt	k5eAaPmAgInS	nabýt
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Registrované	registrovaný	k2eAgNnSc1d1	registrované
partnerství	partnerství	k1gNnSc1	partnerství
mělo	mít	k5eAaImAgNnS	mít
sice	sice	k8xC	sice
pouze	pouze	k6eAd1	pouze
status	status	k1gInSc4	status
civilního	civilní	k2eAgInSc2d1	civilní
obřadu	obřad	k1gInSc2	obřad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dánské	dánský	k2eAgFnPc1d1	dánská
církve	církev	k1gFnPc1	církev
umožnily	umožnit	k5eAaPmAgFnP	umožnit
svým	svůj	k3xOyFgMnPc3	svůj
duchovním	duchovnět	k5eAaImIp1nS	duchovnět
žehnat	žehnat	k5eAaImF	žehnat
párům	pár	k1gInPc3	pár
stejného	stejný	k2eAgNnSc2d1	stejné
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.15	.15	k4	.15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
registrovaném	registrovaný	k2eAgNnSc6d1	registrované
partnerství	partnerství	k1gNnSc6	partnerství
zrušen	zrušit	k5eAaPmNgMnS	zrušit
a	a	k8xC	a
nahrazen	nahradit	k5eAaPmNgInS	nahradit
manželstvím	manželství	k1gNnSc7	manželství
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Stejnopohlavní	Stejnopohlavní	k2eAgNnPc1d1	Stejnopohlavní
manželství	manželství	k1gNnPc1	manželství
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
r.	r.	kA	r.
2006	[number]	k4	2006
zpracovalo	zpracovat	k5eAaPmAgNnS	zpracovat
pět	pět	k4xCc1	pět
sociálně	sociálně	k6eAd1	sociálně
liberálních	liberální	k2eAgMnPc2d1	liberální
poslanců	poslanec	k1gMnPc2	poslanec
rezoluci	rezoluce	k1gFnSc4	rezoluce
s	s	k7c7	s
žádostí	žádost	k1gFnSc7	žádost
o	o	k7c4	o
vládní	vládní	k2eAgInSc4d1	vládní
návrh	návrh	k1gInSc4	návrh
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
genderově-neutrálním	genderověeutrální	k2eAgNnSc6d1	genderově-neutrální
manželství	manželství	k1gNnSc6	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Rezoluce	rezoluce	k1gFnSc1	rezoluce
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
diskutována	diskutován	k2eAgFnSc1d1	diskutována
a	a	k8xC	a
odmítnutá	odmítnutý	k2eAgFnSc1d1	odmítnutá
členy	člen	k1gMnPc7	člen
konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
koalice	koalice	k1gFnSc2	koalice
<g/>
.	.	kIx.	.
</s>
<s>
Ministryně	ministryně	k1gFnSc1	ministryně
rodiny	rodina	k1gFnSc2	rodina
Carina	Carina	k1gFnSc1	Carina
Christensenová	Christensenový	k2eAgFnSc1d1	Christensenový
argumentovala	argumentovat	k5eAaImAgFnS	argumentovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
registrovaní	registrovaný	k2eAgMnPc1d1	registrovaný
partneři	partner	k1gMnPc1	partner
mají	mít	k5eAaImIp3nP	mít
stejná	stejný	k2eAgNnPc1d1	stejné
práva	právo	k1gNnPc1	právo
jako	jako	k8xC	jako
manželé	manžel	k1gMnPc1	manžel
<g/>
,	,	kIx,	,
vyjma	vyjma	k7c2	vyjma
církevního	církevní	k2eAgInSc2d1	církevní
sňatku	sňatek	k1gInSc2	sňatek
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
je	být	k5eAaImIp3nS	být
genderově-neutrální	genderověeutrální	k2eAgNnSc1d1	genderově-neutrální
manželství	manželství	k1gNnSc1	manželství
nezbytné	zbytný	k2eNgNnSc1d1	nezbytné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2008	[number]	k4	2008
se	se	k3xPyFc4	se
zpravodajka	zpravodajka	k1gFnSc1	zpravodajka
rovných	rovný	k2eAgFnPc2d1	rovná
příležitostí	příležitost	k1gFnPc2	příležitost
Sociálně	sociálně	k6eAd1	sociálně
liberální	liberální	k2eAgFnSc2d1	liberální
strany	strana	k1gFnSc2	strana
Lone	Lon	k1gFnSc2	Lon
Dybkjæ	Dybkjæ	k1gFnSc2	Dybkjæ
ještě	ještě	k9	ještě
jednou	jeden	k4xCgFnSc7	jeden
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
stejnopohlavní	stejnopohlavní	k2eAgNnPc4d1	stejnopohlavní
manželství	manželství	k1gNnPc4	manželství
(	(	kIx(	(
<g/>
kø	kø	k?	kø
æ	æ	k?	æ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kodaňská	kodaňský	k2eAgFnSc1d1	Kodaňská
starostka	starostka	k1gFnSc1	starostka
pro	pro	k7c4	pro
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
rekreaci	rekreace	k1gFnSc4	rekreace
Pia	Pius	k1gMnSc2	Pius
Allersleová	Allersleový	k2eAgFnSc1d1	Allersleový
z	z	k7c2	z
liberální	liberální	k2eAgFnSc2d1	liberální
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
strany	strana	k1gFnSc2	strana
Venstre	Venstr	k1gInSc5	Venstr
se	se	k3xPyFc4	se
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
veřejně	veřejně	k6eAd1	veřejně
pro	pro	k7c4	pro
stejnopohlavní	stejnopohlavní	k2eAgNnPc4d1	stejnopohlavní
manželství	manželství	k1gNnPc4	manželství
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
politička	politička	k1gFnSc1	politička
Ritt	Ritt	k1gInSc1	Ritt
Bjerregaardová	Bjerregaardová	k1gFnSc1	Bjerregaardová
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2010	[number]	k4	2010
projednával	projednávat	k5eAaImAgInS	projednávat
parlament	parlament	k1gInSc1	parlament
návrh	návrh	k1gInSc4	návrh
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
stejnopohlavním	stejnopohlavní	k2eAgNnSc6d1	stejnopohlavní
manželství	manželství	k1gNnSc6	manželství
ještě	ještě	k6eAd1	ještě
jednou	jeden	k4xCgFnSc7	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Překladateli	překladatel	k1gMnPc7	překladatel
byly	být	k5eAaImAgFnP	být
opoziční	opoziční	k2eAgFnPc4d1	opoziční
strany	strana	k1gFnPc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Odmítnutý	odmítnutý	k2eAgMnSc1d1	odmítnutý
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
hlasů	hlas	k1gInPc2	hlas
52	[number]	k4	52
<g/>
:	:	kIx,	:
<g/>
57	[number]	k4	57
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
návrh	návrh	k1gInSc1	návrh
byl	být	k5eAaImAgInS	být
taktéž	taktéž	k?	taktéž
zamítnut	zamítnut	k2eAgInSc1d1	zamítnut
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2011	[number]	k4	2011
oznámil	oznámit	k5eAaPmAgMnS	oznámit
ministr	ministr	k1gMnSc1	ministr
genderové	genderový	k2eAgFnSc2d1	genderová
rovnosti	rovnost	k1gFnSc2	rovnost
a	a	k8xC	a
církevních	církevní	k2eAgFnPc2d1	církevní
záležitostí	záležitost	k1gFnPc2	záležitost
Manu	manout	k5eAaImIp1nS	manout
Sareen	Sareen	k1gInSc1	Sareen
nově	nově	k6eAd1	nově
zvolené	zvolený	k2eAgFnSc2d1	zvolená
vlády	vláda	k1gFnSc2	vláda
Helle	Helle	k1gFnSc2	Helle
Thorningové-Schmidtové	Thorningové-Schmidtová	k1gFnSc2	Thorningové-Schmidtová
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
stejnopohlavním	stejnopohlavní	k2eAgNnSc6d1	stejnopohlavní
manželství	manželství	k1gNnSc6	manželství
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
vláda	vláda	k1gFnSc1	vláda
dva	dva	k4xCgInPc4	dva
návrhy	návrh	k1gInPc4	návrh
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
měnil	měnit	k5eAaImAgMnS	měnit
definici	definice	k1gFnSc4	definice
manželství	manželství	k1gNnSc2	manželství
na	na	k7c4	na
genderově-neutrální	genderověeutrální	k2eAgInSc4d1	genderově-neutrální
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
stejnopohlavním	stejnopohlavní	k2eAgInPc3d1	stejnopohlavní
párům	pár	k1gInPc3	pár
uzavření	uzavření	k1gNnSc2	uzavření
buď	buď	k8xC	buď
občanského	občanský	k2eAgNnSc2d1	občanské
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
církevního	církevní	k2eAgInSc2d1	církevní
sňatku	sňatek	k1gInSc2	sňatek
<g/>
.	.	kIx.	.
</s>
<s>
Stávající	stávající	k2eAgNnSc1d1	stávající
registrované	registrovaný	k2eAgNnSc1d1	registrované
partnerství	partnerství	k1gNnSc1	partnerství
můžou	můžou	k?	můžou
být	být	k5eAaImF	být
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
změněná	změněný	k2eAgFnSc1d1	změněná
na	na	k7c6	na
manželství	manželství	k1gNnSc6	manželství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žádná	žádný	k3yNgFnSc1	žádný
nová	nový	k2eAgFnSc1d1	nová
již	již	k6eAd1	již
nepůjde	jít	k5eNaImIp3nS	jít
uzavřít	uzavřít	k5eAaPmF	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dodatečného	dodatečný	k2eAgInSc2d1	dodatečný
návrhu	návrh	k1gInSc2	návrh
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
církevní	církevní	k2eAgMnPc1d1	církevní
hodnostáři	hodnostář	k1gMnPc1	hodnostář
právo	právo	k1gNnSc4	právo
odmítnout	odmítnout	k5eAaPmF	odmítnout
oddat	oddat	k5eAaPmF	oddat
homosexuální	homosexuální	k2eAgInSc1d1	homosexuální
pár	pár	k1gInSc1	pár
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
náboženské	náboženský	k2eAgFnPc1d1	náboženská
komunity	komunita	k1gFnPc1	komunita
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
oprávněny	oprávněn	k2eAgFnPc1d1	oprávněna
oddávat	oddávat	k5eAaImF	oddávat
takové	takový	k3xDgInPc4	takový
páry	pár	k1gInPc4	pár
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
dobrovolnosti	dobrovolnost	k1gFnSc2	dobrovolnost
<g/>
.	.	kIx.	.
</s>
<s>
Návrhy	návrh	k1gInPc1	návrh
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
projednávat	projednávat	k5eAaImF	projednávat
od	od	k7c2	od
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2012.14	[number]	k4	2012.14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
vládní	vládní	k2eAgInSc1d1	vládní
návrh	návrh	k1gInSc1	návrh
prezentován	prezentovat	k5eAaBmNgInS	prezentovat
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
byly	být	k5eAaImAgInP	být
všechny	všechen	k3xTgInPc1	všechen
návrhy	návrh	k1gInPc1	návrh
schváleny	schválit	k5eAaPmNgInP	schválit
a	a	k8xC	a
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
podepsány	podepsán	k2eAgFnPc1d1	podepsána
královnou	královna	k1gFnSc7	královna
<g/>
.	.	kIx.	.
</s>
<s>
Účinnosti	účinnost	k1gFnPc1	účinnost
nabyly	nabýt	k5eAaPmAgFnP	nabýt
k	k	k7c3	k
15	[number]	k4	15
<g/>
.	.	kIx.	.
červnu	červen	k1gInSc3	červen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Novou	nový	k2eAgFnSc4d1	nová
legislativu	legislativa	k1gFnSc4	legislativa
odmítaly	odmítat	k5eAaImAgFnP	odmítat
Dánská	dánský	k2eAgFnSc1d1	dánská
lidová	lidový	k2eAgFnSc1d1	lidová
strana	strana	k1gFnSc1	strana
a	a	k8xC	a
Křesťanští	křesťanský	k2eAgMnPc1d1	křesťanský
demokraté	demokrat	k1gMnPc1	demokrat
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
status	status	k1gInSc4	status
náboženských	náboženský	k2eAgFnPc2d1	náboženská
konzervativních	konzervativní	k2eAgFnPc2d1	konzervativní
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
politickým	politický	k2eAgInSc7d1	politický
vlivem	vliv	k1gInSc7	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
jsou	být	k5eAaImIp3nP	být
ministři	ministr	k1gMnPc1	ministr
oprávněni	oprávnit	k5eAaPmNgMnP	oprávnit
odmítnout	odmítnout	k5eAaPmF	odmítnout
konání	konání	k1gNnSc4	konání
homosexuálních	homosexuální	k2eAgFnPc2d1	homosexuální
svateb	svatba	k1gFnPc2	svatba
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
místní	místní	k2eAgMnPc1d1	místní
biskupové	biskup	k1gMnPc1	biskup
jsou	být	k5eAaImIp3nP	být
povinni	povinen	k2eAgMnPc1d1	povinen
přenechávat	přenechávat	k5eAaImF	přenechávat
kostelní	kostelní	k2eAgFnPc4d1	kostelní
budovy	budova	k1gFnPc4	budova
k	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
<g/>
Hlasování	hlasování	k1gNnSc1	hlasování
dánského	dánský	k2eAgInSc2d1	dánský
parlamentu	parlament	k1gInSc2	parlament
7	[number]	k4	7
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
2012	[number]	k4	2012
</s>
</p>
<p>
<s>
===	===	k?	===
Grónsko	Grónsko	k1gNnSc4	Grónsko
===	===	k?	===
</s>
</p>
<p>
<s>
Dánský	dánský	k2eAgInSc1d1	dánský
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
registrovaném	registrovaný	k2eAgNnSc6d1	registrované
partnerství	partnerství	k1gNnSc6	partnerství
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
Grónska	Grónsko	k1gNnSc2	Grónsko
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
dánský	dánský	k2eAgInSc1d1	dánský
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
manželství	manželství	k1gNnSc6	manželství
byl	být	k5eAaImAgMnS	být
podporován	podporován	k2eAgMnSc1d1	podporován
i	i	k9	i
grónskou	grónský	k2eAgFnSc7d1	grónská
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jej	on	k3xPp3gMnSc4	on
chtěla	chtít	k5eAaImAgFnS	chtít
předložit	předložit	k5eAaPmF	předložit
parlamentu	parlament	k1gInSc2	parlament
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
nakonec	nakonec	k9	nakonec
nedošlo	dojít	k5eNaPmAgNnS	dojít
kvůli	kvůli	k7c3	kvůli
předčasným	předčasný	k2eAgFnPc3d1	předčasná
parlamentním	parlamentní	k2eAgFnPc3d1	parlamentní
volbám	volba	k1gFnPc3	volba
<g/>
.	.	kIx.	.
</s>
<s>
Legislativa	legislativa	k1gFnSc1	legislativa
umožňující	umožňující	k2eAgFnSc1d1	umožňující
homosexuálním	homosexuální	k2eAgInSc7d1	homosexuální
párům	pár	k1gInPc3	pár
uzavírání	uzavírání	k1gNnSc3	uzavírání
sňatků	sňatek	k1gInPc2	sňatek
a	a	k8xC	a
adopce	adopce	k1gFnSc1	adopce
dětí	dítě	k1gFnPc2	dítě
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
prvního	první	k4xOgNnSc2	první
čtení	čtení	k1gNnSc2	čtení
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
druhému	druhý	k4xOgInSc3	druhý
čtení	čtení	k1gNnSc6	čtení
došlo	dojít	k5eAaPmAgNnS	dojít
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2015	[number]	k4	2015
a	a	k8xC	a
zákon	zákon	k1gInSc1	zákon
byl	být	k5eAaImAgInS	být
jednomyslně	jednomyslně	k6eAd1	jednomyslně
schválen	schválit	k5eAaPmNgInS	schválit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ratifikaci	ratifikace	k1gFnSc3	ratifikace
nové	nový	k2eAgFnSc2d1	nová
legislativy	legislativa	k1gFnSc2	legislativa
byl	být	k5eAaImAgInS	být
nutný	nutný	k2eAgInSc1d1	nutný
souhlas	souhlas	k1gInSc1	souhlas
dánského	dánský	k2eAgInSc2d1	dánský
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
dán	dát	k5eAaPmNgInS	dát
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
účinným	účinný	k2eAgInSc7d1	účinný
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
<g/>
Grónský	grónský	k2eAgInSc1d1	grónský
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
registrovaném	registrovaný	k2eAgNnSc6d1	registrované
partnerství	partnerství	k1gNnSc6	partnerství
byl	být	k5eAaImAgInS	být
v	v	k7c4	v
den	den	k1gInSc4	den
účinnosti	účinnost	k1gFnSc2	účinnost
nabytí	nabytí	k1gNnSc4	nabytí
zákona	zákon	k1gInSc2	zákon
o	o	k7c4	o
manželství	manželství	k1gNnSc4	manželství
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
zrušen	zrušen	k2eAgInSc1d1	zrušen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Faerské	Faerský	k2eAgInPc1d1	Faerský
ostrovy	ostrov	k1gInPc1	ostrov
===	===	k?	===
</s>
</p>
<p>
<s>
Dánské	dánský	k2eAgNnSc1d1	dánské
registrované	registrovaný	k2eAgNnSc1d1	registrované
partnerství	partnerství	k1gNnSc1	partnerství
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nepovedlo	povést	k5eNaPmAgNnS	povést
rozšířit	rozšířit	k5eAaPmF	rozšířit
na	na	k7c4	na
Faerské	Faerský	k2eAgInPc4d1	Faerský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
tak	tak	k6eAd1	tak
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
posledním	poslední	k2eAgInSc7d1	poslední
severským	severský	k2eAgInSc7d1	severský
národem	národ	k1gInSc7	národ
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
neuznává	uznávat	k5eNaImIp3nS	uznávat
stejnopohlavní	stejnopohlavní	k2eAgInPc4d1	stejnopohlavní
svazky	svazek	k1gInPc4	svazek
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
bylo	být	k5eAaImAgNnS	být
faerskému	faerský	k2eAgInSc3d1	faerský
parlamentu	parlament	k1gInSc3	parlament
Lø	Lø	k1gFnSc2	Lø
prezentováno	prezentovat	k5eAaBmNgNnS	prezentovat
několik	několik	k4yIc1	několik
návrhů	návrh	k1gInPc2	návrh
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
genderově-neutrálním	genderověeutrální	k2eAgNnSc6d1	genderově-neutrální
manželství	manželství	k1gNnSc6	manželství
zpracovaných	zpracovaný	k2eAgInPc2d1	zpracovaný
podle	podle	k7c2	podle
dánského	dánský	k2eAgInSc2d1	dánský
vzoru	vzor	k1gInSc2	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
odmítnul	odmítnout	k5eAaPmAgMnS	odmítnout
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
čtení	čtení	k1gNnSc6	čtení
dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
září	září	k1gNnSc6	září
2015	[number]	k4	2015
byly	být	k5eAaImAgFnP	být
předloženy	předložit	k5eAaPmNgFnP	předložit
parlamentu	parlament	k1gInSc6	parlament
dva	dva	k4xCgInPc4	dva
návrhy	návrh	k1gInPc4	návrh
o	o	k7c6	o
stejnopohlavním	stejnopohlavní	k2eAgNnSc6d1	stejnopohlavní
manželství	manželství	k1gNnSc1	manželství
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgMnSc1	jeden
upravující	upravující	k2eAgMnSc1d1	upravující
stejnopohlavní	stejnopohlavní	k2eAgNnSc4d1	stejnopohlavní
manželství	manželství	k1gNnSc4	manželství
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
rozvody	rozvod	k1gInPc4	rozvod
stejnopohlavních	stejnopohlavní	k2eAgInPc2d1	stejnopohlavní
párů	pár	k1gInPc2	pár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Návrhy	návrh	k1gInPc1	návrh
prošly	projít	k5eAaPmAgFnP	projít
prvním	první	k4xOgMnSc6	první
čtením	čtení	k1gNnSc7	čtení
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubnu	duben	k1gInSc3	duben
2016	[number]	k4	2016
následujíc	následovat	k5eAaImSgFnS	následovat
po	po	k7c6	po
značném	značný	k2eAgInSc6d1	značný
parlamentním	parlamentní	k2eAgInSc6d1	parlamentní
tlaku	tlak	k1gInSc6	tlak
byl	být	k5eAaImAgInS	být
návrh	návrh	k1gInSc1	návrh
o	o	k7c6	o
stejnopohlavním	stejnopohlavní	k2eAgNnSc6d1	stejnopohlavní
manželství	manželství	k1gNnSc6	manželství
přijat	přijmout	k5eAaPmNgInS	přijmout
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
čtení	čtení	k1gNnSc6	čtení
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
hlasů	hlas	k1gInPc2	hlas
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
pak	pak	k6eAd1	pak
prošel	projít	k5eAaPmAgInS	projít
finálním	finální	k2eAgNnSc7d1	finální
hlasováním	hlasování	k1gNnSc7	hlasování
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2016	[number]	k4	2016
a	a	k8xC	a
momentálně	momentálně	k6eAd1	momentálně
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c6	na
schválení	schválení	k1gNnSc6	schválení
dánským	dánský	k2eAgInSc7d1	dánský
parlamentem	parlament	k1gInSc7	parlament
a	a	k8xC	a
královnou	královna	k1gFnSc7	královna
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
účinným	účinný	k2eAgInSc7d1	účinný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Veřejné	veřejný	k2eAgNnSc4d1	veřejné
mínění	mínění	k1gNnSc4	mínění
==	==	k?	==
</s>
</p>
<p>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
YouGov	YouGov	k1gInSc1	YouGov
spuštěný	spuštěný	k2eAgInSc1d1	spuštěný
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
shledal	shledat	k5eAaPmAgMnS	shledat
<g/>
,	,	kIx,	,
že	že	k8xS	že
79	[number]	k4	79
%	%	kIx~	%
zkoumaných	zkoumaný	k2eAgMnPc2d1	zkoumaný
Dánů	Dán	k1gMnPc2	Dán
podporuje	podporovat	k5eAaImIp3nS	podporovat
stejnopohlavní	stejnopohlavní	k2eAgNnSc1d1	stejnopohlavní
manželství	manželství	k1gNnSc1	manželství
a	a	k8xC	a
16	[number]	k4	16
%	%	kIx~	%
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
<g/>
.	.	kIx.	.
</s>
<s>
Zbylých	zbylý	k2eAgNnPc2d1	zbylé
6	[number]	k4	6
%	%	kIx~	%
nemělo	mít	k5eNaImAgNnS	mít
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
otázku	otázka	k1gFnSc4	otázka
jednoznačný	jednoznačný	k2eAgInSc4d1	jednoznačný
názor	názor	k1gInSc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
výzkum	výzkum	k1gInSc1	výzkum
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
adopci	adopce	k1gFnSc4	adopce
dětí	dítě	k1gFnPc2	dítě
homosexuálními	homosexuální	k2eAgFnPc7d1	homosexuální
páry	pár	k1gInPc1	pár
podporuje	podporovat	k5eAaImIp3nS	podporovat
59	[number]	k4	59
%	%	kIx~	%
respondentů	respondent	k1gMnPc2	respondent
<g/>
,	,	kIx,	,
31	[number]	k4	31
%	%	kIx~	%
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
a	a	k8xC	a
11	[number]	k4	11
%	%	kIx~	%
nemá	mít	k5eNaImIp3nS	mít
jednoznačný	jednoznačný	k2eAgInSc1d1	jednoznačný
názor	názor	k1gInSc1	názor
<g/>
.	.	kIx.	.
<g/>
Anketa	anketa	k1gFnSc1	anketa
Gallup	Gallup	k1gInSc4	Gallup
z	z	k7c2	z
května	květen	k1gInSc2	květen
2013	[number]	k4	2013
shledala	shledat	k5eAaPmAgFnS	shledat
<g/>
,	,	kIx,	,
že	že	k8xS	že
68	[number]	k4	68
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Faerských	Faerský	k2eAgInPc2d1	Faerský
ostrovů	ostrov	k1gInPc2	ostrov
podporuje	podporovat	k5eAaImIp3nS	podporovat
občanský	občanský	k2eAgInSc1d1	občanský
sňatek	sňatek	k1gInSc1	sňatek
pro	pro	k7c4	pro
homosexuální	homosexuální	k2eAgInPc4d1	homosexuální
páry	pár	k1gInPc4	pár
<g/>
,	,	kIx,	,
27	[number]	k4	27
%	%	kIx~	%
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
a	a	k8xC	a
5	[number]	k4	5
%	%	kIx~	%
nerozhodnuto	rozhodnut	k2eNgNnSc1d1	nerozhodnuto
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
regiony	region	k1gInPc1	region
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
většinovou	většinový	k2eAgFnSc4d1	většinová
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
v	v	k7c6	v
žádné	žádný	k3yNgFnSc6	žádný
věkové	věkový	k2eAgFnSc6d1	věková
skupině	skupina	k1gFnSc6	skupina
nepřevažují	převažovat	k5eNaImIp3nP	převažovat
odpůrci	odpůrce	k1gMnPc1	odpůrce
podporovatele	podporovatel	k1gMnSc4	podporovatel
<g/>
.	.	kIx.	.
<g/>
Jiný	jiný	k2eAgInSc4d1	jiný
výzkum	výzkum	k1gInSc4	výzkum
mínění	mínění	k1gNnSc2	mínění
na	na	k7c6	na
Faerských	Faerský	k2eAgInPc6d1	Faerský
ostrovech	ostrov	k1gInPc6	ostrov
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
62	[number]	k4	62
%	%	kIx~	%
respondentů	respondent	k1gMnPc2	respondent
podporuje	podporovat	k5eAaImIp3nS	podporovat
stejnopohlavní	stejnopohlavní	k2eAgNnSc1d1	stejnopohlavní
manželství	manželství	k1gNnSc1	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Regionální	regionální	k2eAgFnSc1d1	regionální
různorodost	různorodost	k1gFnSc1	různorodost
postojů	postoj	k1gInPc2	postoj
byla	být	k5eAaImAgFnS	být
značná	značný	k2eAgFnSc1d1	značná
<g/>
.	.	kIx.	.
</s>
<s>
Podpora	podpora	k1gFnSc1	podpora
byla	být	k5eAaImAgFnS	být
větší	veliký	k2eAgFnSc1d2	veliký
na	na	k7c4	na
Streymoy	Streymoa	k1gFnPc4	Streymoa
(	(	kIx(	(
<g/>
71	[number]	k4	71
%	%	kIx~	%
na	na	k7c6	na
Norð	Norð	k1gFnSc6	Norð
<g/>
;	;	kIx,	;
78	[number]	k4	78
%	%	kIx~	%
na	na	k7c6	na
Suð	Suð	k1gFnSc6	Suð
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Tórshavn	Tórshavna	k1gFnPc2	Tórshavna
<g/>
,	,	kIx,	,
než	než	k8xS	než
na	na	k7c6	na
Norð	Norð	k1gFnSc6	Norð
(	(	kIx(	(
<g/>
42	[number]	k4	42
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Eysturoy	Eysturoy	k1gInPc1	Eysturoy
(	(	kIx(	(
<g/>
48	[number]	k4	48
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
další	další	k2eAgInSc1d1	další
výzkum	výzkum	k1gInSc1	výzkum
Faerských	Faerský	k2eAgInPc2d1	Faerský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
bylo	být	k5eAaImAgNnS	být
dotázáno	dotázat	k5eAaPmNgNnS	dotázat
600	[number]	k4	600
respondentů	respondent	k1gMnPc2	respondent
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
práva	právo	k1gNnSc2	právo
homosexuálním	homosexuální	k2eAgInSc7d1	homosexuální
párů	pár	k1gInPc2	pár
na	na	k7c4	na
občanský	občanský	k2eAgInSc4d1	občanský
sňatek	sňatek	k1gInSc4	sňatek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
600	[number]	k4	600
zkoumaných	zkoumaný	k2eAgInPc2d1	zkoumaný
se	s	k7c7	s
61	[number]	k4	61
%	%	kIx~	%
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
pro	pro	k7c4	pro
<g/>
,	,	kIx,	,
32	[number]	k4	32
%	%	kIx~	%
proti	proti	k7c3	proti
a	a	k8xC	a
7	[number]	k4	7
%	%	kIx~	%
nemělo	mít	k5eNaImAgNnS	mít
jednoznačný	jednoznačný	k2eAgInSc4d1	jednoznačný
názor	názor	k1gInSc4	názor
<g/>
.	.	kIx.	.
<g/>
Průzkum	průzkum	k1gInSc1	průzkum
Pew	Pew	k1gMnSc1	Pew
Research	Research	k1gMnSc1	Research
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
mezi	mezi	k7c7	mezi
dubnem	duben	k1gInSc7	duben
a	a	k8xC	a
srpnem	srpen	k1gInSc7	srpen
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
a	a	k8xC	a
jehož	jehož	k3xOyRp3gInPc1	jehož
výsledky	výsledek	k1gInPc1	výsledek
byly	být	k5eAaImAgInP	být
publikovány	publikovat	k5eAaBmNgInP	publikovat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
86	[number]	k4	86
%	%	kIx~	%
Dánů	Dán	k1gMnPc2	Dán
podporuje	podporovat	k5eAaImIp3nS	podporovat
stejnopohlavní	stejnopohlavní	k2eAgNnSc1d1	stejnopohlavní
manželství	manželství	k1gNnSc1	manželství
<g/>
,	,	kIx,	,
9	[number]	k4	9
%	%	kIx~	%
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
a	a	k8xC	a
5	[number]	k4	5
%	%	kIx~	%
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
odpovědět	odpovědět	k5eAaPmF	odpovědět
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
náboženství	náboženství	k1gNnSc1	náboženství
tak	tak	k9	tak
pro	pro	k7c4	pro
bylo	být	k5eAaImAgNnS	být
92	[number]	k4	92
%	%	kIx~	%
respondentů	respondent	k1gMnPc2	respondent
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
87	[number]	k4	87
%	%	kIx~	%
nepraktikujících	praktikující	k2eNgMnPc2d1	nepraktikující
křesťanů	křesťan	k1gMnPc2	křesťan
a	a	k8xC	a
74	[number]	k4	74
%	%	kIx~	%
praktikujících	praktikující	k2eAgMnPc2d1	praktikující
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Same-sex	Sameex	k1gInSc1	Same-sex
marriage	marriag	k1gInPc1	marriag
in	in	k?	in
Denmark	Denmark	k1gInSc1	Denmark
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Axel	Axel	k1gMnSc1	Axel
a	a	k8xC	a
Eigil	Eigil	k1gMnSc1	Eigil
Axgil	Axgil	k1gMnSc1	Axgil
</s>
</p>
<p>
<s>
LGBT	LGBT	kA	LGBT
práva	práv	k2eAgFnSc1d1	práva
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
</s>
</p>
<p>
<s>
LGBT	LGBT	kA	LGBT
práva	práv	k2eAgFnSc1d1	práva
na	na	k7c6	na
Faerských	Faerský	k2eAgInPc6d1	Faerský
ostrovech	ostrov	k1gInPc6	ostrov
</s>
</p>
<p>
<s>
LGBT	LGBT	kA	LGBT
práva	práv	k2eAgFnSc1d1	práva
v	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
</s>
</p>
<p>
<s>
Stejnopohlavní	Stejnopohlavní	k2eAgNnPc1d1	Stejnopohlavní
soužití	soužití	k1gNnPc1	soužití
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
</s>
</p>
