<s>
Quasi-Zenith	Quasi-Zenith	k1gMnSc1
Satellite	Satellit	k1gInSc5
System	Syst	k1gMnSc7
</s>
<s>
Quasi-Zenith	Quasi-Zenith	k1gMnSc1
Satellite	Satellit	k1gInSc5
System	Syst	k1gMnSc7
(	(	kIx(
<g/>
QZSS	QZSS	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vyvíjený	vyvíjený	k2eAgInSc1d1
třídružicový	třídružicový	k2eAgInSc1d1
regionální	regionální	k2eAgInSc1d1
navigační	navigační	k2eAgInSc1d1
systém	systém	k1gInSc1
pro	pro	k7c4
Japonsko	Japonsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
plošně	plošně	k6eAd1
omezenou	omezený	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
globálního	globální	k2eAgInSc2d1
pozičního	poziční	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
například	například	k6eAd1
GPS	GPS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
družice	družice	k1gFnSc1
Michibiki	Michibik	k1gFnSc2
byla	být	k5eAaImAgFnS
vypuštěna	vypuštěn	k2eAgFnSc1d1
11	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2010	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Plný	plný	k2eAgInSc1d1
operační	operační	k2eAgInSc1d1
stav	stav	k1gInSc1
byl	být	k5eAaImAgInS
předpokládán	předpokládat	k5eAaImNgInS
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
avšak	avšak	k8xC
v	v	k7c6
březnu	březen	k1gInSc6
2013	#num#	k4
japonská	japonský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
oznámila	oznámit	k5eAaPmAgFnS
rozšíření	rozšíření	k1gNnSc4
ze	z	k7c2
tří	tři	k4xCgInPc2
na	na	k7c4
čtyři	čtyři	k4xCgInPc4
satelity	satelit	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kontrakt	kontrakt	k1gInSc1
na	na	k7c4
stavbu	stavba	k1gFnSc4
tří	tři	k4xCgFnPc2
družic	družice	k1gFnPc2
za	za	k7c4
526	#num#	k4
miliónů	milión	k4xCgInPc2
dolarů	dolar	k1gInPc2
získala	získat	k5eAaPmAgFnS
firma	firma	k1gFnSc1
Mitsubishi	mitsubishi	k1gNnSc2
Electric	Electrice	k1gInPc2
s	s	k7c7
předpokládaným	předpokládaný	k2eAgNnSc7d1
vypuštěním	vypuštění	k1gNnSc7
do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Základní	základní	k2eAgInSc1d1
čtyřdružicový	čtyřdružicový	k2eAgInSc1d1
systém	systém	k1gInSc1
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
funkční	funkční	k2eAgInSc1d1
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Launch	Launch	k1gMnSc1
Result	Result	k1gMnSc1
of	of	k?
the	the	k?
First	First	k1gMnSc1
Quasi-Zenith	Quasi-Zenith	k1gMnSc1
Satellite	Satellit	k1gInSc5
'	'	kIx"
<g/>
MICHIBIKI	MICHIBIKI	kA
<g/>
'	'	kIx"
by	by	kYmCp3nS
H-IIA	H-IIA	k1gFnSc1
Launch	Launch	k1gInSc4
Vehicle	Vehicle	k1gFnSc2
No	no	k9
<g/>
.	.	kIx.
18	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-09-11	2010-09-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
QZSS	QZSS	kA
in	in	k?
2010	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asian	Asian	k1gInSc1
Surveying	Surveying	k1gInSc4
and	and	k?
Mapping	Mapping	k1gInSc1
<g/>
,	,	kIx,
2009-05-07	2009-05-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
GNSS	GNSS	kA
All	All	k1gMnSc1
Over	Over	k1gMnSc1
the	the	k?
World	World	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GPS	GPS	kA
World	Worlda	k1gFnPc2
Online	Onlin	k1gInSc5
<g/>
,	,	kIx,
2007-11-01	2007-11-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
http://www.spaceflightnow.com/news/n1304/04qzss/	http://www.spaceflightnow.com/news/n1304/04qzss/	k4
Japan	japan	k1gInSc1
to	ten	k3xDgNnSc4
build	build	k6eAd1
fleet	fleet	k1gInSc1
of	of	k?
navigation	navigation	k1gInSc1
satellites	satellites	k1gMnSc1
2013-04-04	2013-04-04	k4
Retrieved	Retrieved	k1gInSc1
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
↑	↑	k?
Service	Service	k1gFnSc1
Overview	Overview	k1gFnSc1
-	-	kIx~
What	What	k1gInSc1
is	is	k?
the	the	k?
QZSS	QZSS	kA
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cabinet	Cabinet	k1gInSc1
Office	Office	kA
<g/>
,	,	kIx,
Government	Government	k1gInSc1
of	of	k?
Japan	japan	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gNnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc4
<g/>
}	}	kIx)
Globální	globální	k2eAgInPc4d1
družicové	družicový	k2eAgInPc4d1
polohové	polohový	k2eAgInPc4d1
systémy	systém	k1gInPc4
(	(	kIx(
<g/>
GNSS	GNSS	kA
<g/>
)	)	kIx)
Zastaralé	zastaralý	k2eAgFnSc2d1
</s>
<s>
Transit	transit	k1gInSc1
•	•	k?
PARUS	PARUS	kA
•	•	k?
CIKADA	CIKADA	kA
Provozované	provozovaný	k2eAgFnSc3d1
</s>
<s>
GPS	GPS	kA
•	•	k?
GLONASS	GLONASS	kA
•	•	k?
DORIS	DORIS	kA
Ve	v	k7c6
vývoji	vývoj	k1gInSc6
</s>
<s>
Galileo	Galilea	k1gFnSc5
•	•	k?
BeiDou	BeiDa	k1gMnSc7
•	•	k?
QZSS	QZSS	kA
•	•	k?
IRNSS	IRNSS	kA
Rozšiřující	rozšiřující	k2eAgFnSc1d1
</s>
<s>
EGNOS	EGNOS	kA
•	•	k?
WAAS	WAAS	kA
•	•	k?
MSAS	MSAS	kA
•	•	k?
GAGAN	GAGAN	kA
•	•	k?
CWAAS	CWAAS	kA
Technologie	technologie	k1gFnSc2
</s>
<s>
SBAS	SBAS	kA
<g/>
,	,	kIx,
LAAS	LAAS	kA
<g/>
,	,	kIx,
DGPS	DGPS	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kosmonautika	kosmonautika	k1gFnSc1
</s>
