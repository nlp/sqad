<s>
Mor	mor	k1gInSc1	mor
skotu	skot	k1gInSc2	skot
(	(	kIx(	(
<g/>
Rinderpest	Rinderpest	k1gInSc1	Rinderpest
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
akutní	akutní	k2eAgMnSc1d1	akutní
<g/>
,	,	kIx,	,
vysoce	vysoce	k6eAd1	vysoce
nakažlivé	nakažlivý	k2eAgNnSc4d1	nakažlivé
virové	virový	k2eAgNnSc4d1	virové
onemocnění	onemocnění	k1gNnSc4	onemocnění
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
dalších	další	k2eAgMnPc2d1	další
zástupců	zástupce	k1gMnPc2	zástupce
z	z	k7c2	z
čeledí	čeleď	k1gFnPc2	čeleď
turovitých	turovití	k1gMnPc2	turovití
<g/>
,	,	kIx,	,
jelenovitých	jelenovití	k1gMnPc2	jelenovití
a	a	k8xC	a
prasatovitých	prasatovitý	k2eAgMnPc2d1	prasatovitý
<g/>
.	.	kIx.	.
</s>
<s>
Původcem	původce	k1gMnSc7	původce
je	být	k5eAaImIp3nS	být
RNA	RNA	kA	RNA
virus	virus	k1gInSc1	virus
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
Paramyxoviridae	Paramyxovirida	k1gFnSc2	Paramyxovirida
<g/>
.	.	kIx.	.
</s>
<s>
Mortalita	mortalita	k1gFnSc1	mortalita
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
až	až	k9	až
100	[number]	k4	100
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgFnPc6	první
fázích	fáze	k1gFnPc6	fáze
nemoci	nemoc	k1gFnSc2	nemoc
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
horečka	horečka	k1gFnSc1	horečka
<g/>
,	,	kIx,	,
průjem	průjem	k1gInSc1	průjem
<g/>
,	,	kIx,	,
malátnost	malátnost	k1gFnSc1	malátnost
<g/>
,	,	kIx,	,
nechutenství	nechutenství	k1gNnSc1	nechutenství
<g/>
,	,	kIx,	,
výtok	výtok	k1gInSc1	výtok
z	z	k7c2	z
nosu	nos	k1gInSc2	nos
a	a	k8xC	a
očí	oko	k1gNnPc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
další	další	k2eAgFnSc6d1	další
fázi	fáze	k1gFnSc6	fáze
se	se	k3xPyFc4	se
na	na	k7c6	na
zarudlých	zarudlý	k2eAgFnPc6d1	zarudlá
<g/>
,	,	kIx,	,
překrvených	překrvený	k2eAgFnPc6d1	překrvená
sliznicích	sliznice	k1gFnPc6	sliznice
objevují	objevovat	k5eAaImIp3nP	objevovat
krváceniny	krvácenina	k1gFnPc1	krvácenina
<g/>
,	,	kIx,	,
eroze	eroze	k1gFnPc1	eroze
až	až	k8xS	až
nekrózy	nekróza	k1gFnPc1	nekróza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2010	[number]	k4	2010
Organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
výživu	výživa	k1gFnSc4	výživa
a	a	k8xC	a
zemědělství	zemědělství	k1gNnSc4	zemědělství
(	(	kIx(	(
<g/>
FAO	FAO	kA	FAO
<g/>
)	)	kIx)	)
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mor	mor	k1gInSc1	mor
skotu	skot	k1gInSc2	skot
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
zcela	zcela	k6eAd1	zcela
vymýcen	vymýcen	k2eAgInSc1d1	vymýcen
<g/>
;	;	kIx,	;
Světová	světový	k2eAgFnSc1d1	světová
organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
zdraví	zdraví	k1gNnSc4	zdraví
zvířat	zvíře	k1gNnPc2	zvíře
vydala	vydat	k5eAaPmAgFnS	vydat
oficiální	oficiální	k2eAgNnSc4d1	oficiální
potvrzení	potvrzení	k1gNnSc4	potvrzení
o	o	k7c6	o
úplném	úplný	k2eAgNnSc6d1	úplné
vymýcení	vymýcení	k1gNnSc6	vymýcení
nemoci	nemoc	k1gFnSc2	nemoc
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Rinderpest	Rinderpest	k1gInSc1	Rinderpest
virus	virus	k1gInSc1	virus
(	(	kIx(	(
<g/>
RPV	RPV	kA	RPV
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
morbillivirus	morbillivirus	k1gInSc4	morbillivirus
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
virus	virus	k1gInSc1	virus
psinky	psinka	k1gFnSc2	psinka
nebo	nebo	k8xC	nebo
moru	mora	k1gFnSc4	mora
malých	malý	k2eAgMnPc2d1	malý
přežvýkavců	přežvýkavec	k1gMnPc2	přežvýkavec
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
virus	virus	k1gInSc4	virus
s	s	k7c7	s
genomem	genom	k1gInSc7	genom
jednovláknové	jednovláknový	k2eAgFnSc2d1	jednovláknová
RNA	RNA	kA	RNA
s	s	k7c7	s
negativní	negativní	k2eAgFnSc7d1	negativní
polaritou	polarita	k1gFnSc7	polarita
<g/>
.	.	kIx.	.
</s>
<s>
Kapsida	Kapsida	k1gFnSc1	Kapsida
viru	vir	k1gInSc2	vir
je	být	k5eAaImIp3nS	být
obalená	obalený	k2eAgNnPc4d1	obalené
glykoproteinovým	glykoproteinův	k2eAgInSc7d1	glykoproteinův
obalem	obal	k1gInSc7	obal
<g/>
.	.	kIx.	.
</s>
<s>
Virus	virus	k1gInSc1	virus
moru	mor	k1gInSc2	mor
skotu	skot	k1gInSc2	skot
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
jen	jen	k9	jen
v	v	k7c6	v
jediném	jediný	k2eAgInSc6d1	jediný
sérotypu	sérotyp	k1gInSc6	sérotyp
<g/>
,	,	kIx,	,
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
však	však	k9	však
řadu	řada	k1gFnSc4	řada
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
svou	svůj	k3xOyFgFnSc7	svůj
patogenitou	patogenita	k1gFnSc7	patogenita
a	a	k8xC	a
hostitelskám	hostitelskat	k5eAaPmIp1nS	hostitelskat
spektrem	spektrum	k1gNnSc7	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
přežívá	přežívat	k5eAaImIp3nS	přežívat
ve	v	k7c6	v
zchlazených	zchlazený	k2eAgFnPc6d1	zchlazená
či	či	k8xC	či
zmražených	zmražený	k2eAgFnPc6d1	zmražená
tkáních	tkáň	k1gFnPc6	tkáň
a	a	k8xC	a
orgánech	orgán	k1gInPc6	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
ho	on	k3xPp3gInSc4	on
vysoká	vysoký	k2eAgFnSc1d1	vysoká
horečka	horečka	k1gFnSc1	horečka
(	(	kIx(	(
<g/>
40	[number]	k4	40
-	-	kIx~	-
42	[number]	k4	42
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
závažné	závažný	k2eAgInPc1d1	závažný
silné	silný	k2eAgInPc1d1	silný
průjmy	průjem	k1gInPc1	průjem
vedoucí	vedoucí	k2eAgInPc1d1	vedoucí
k	k	k7c3	k
dehydrataci	dehydratace	k1gFnSc3	dehydratace
a	a	k8xC	a
následné	následný	k2eAgNnSc4d1	následné
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Průjmy	průjem	k1gInPc1	průjem
jsou	být	k5eAaImIp3nP	být
nejprve	nejprve	k6eAd1	nejprve
vodnaté	vodnatý	k2eAgFnPc1d1	vodnatá
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
krvavé	krvavý	k2eAgNnSc1d1	krvavé
s	s	k7c7	s
příměsí	příměs	k1gFnSc7	příměs
fibrinu	fibrin	k1gInSc2	fibrin
(	(	kIx(	(
<g/>
difteroidní	difteroidní	k2eAgInSc1d1	difteroidní
zánět	zánět	k1gInSc1	zánět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zaznamenány	zaznamenán	k2eAgFnPc1d1	zaznamenána
byli	být	k5eAaImAgMnP	být
také	také	k9	také
eroze	eroze	k1gFnSc2	eroze
až	až	k8xS	až
nekrózy	nekróza	k1gFnSc2	nekróza
v	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
ústní	ústní	k2eAgInSc4d1	ústní
a	a	k8xC	a
hleno-hnisavý	hlenonisavý	k2eAgInSc4d1	hleno-hnisavý
výtok	výtok	k1gInSc4	výtok
z	z	k7c2	z
očí	oko	k1gNnPc2	oko
postižených	postižený	k1gMnPc2	postižený
zánětem	zánět	k1gInSc7	zánět
spojivek	spojivka	k1gFnPc2	spojivka
<g/>
.	.	kIx.	.
</s>
<s>
Mortalita	mortalita	k1gFnSc1	mortalita
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
90	[number]	k4	90
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Morbidita	morbidita	k1gFnSc1	morbidita
onemocnění	onemocnění	k1gNnSc2	onemocnění
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
100	[number]	k4	100
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
až	až	k9	až
21	[number]	k4	21
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc1	průběh
onemocnění	onemocnění	k1gNnSc2	onemocnění
perakutní	perakutní	k2eAgFnSc2d1	perakutní
<g/>
,	,	kIx,	,
akutní	akutní	k2eAgFnSc2d1	akutní
<g/>
,	,	kIx,	,
subakutní	subakutní	k2eAgMnSc1d1	subakutní
nebo	nebo	k8xC	nebo
atypický	atypický	k2eAgMnSc1d1	atypický
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
patologické	patologický	k2eAgInPc4d1	patologický
nálezy	nález	k1gInPc4	nález
patří	patřit	k5eAaImIp3nP	patřit
nekrotická	nekrotický	k2eAgNnPc1d1	nekrotické
ložiska	ložisko	k1gNnPc1	ložisko
na	na	k7c6	na
sliznici	sliznice	k1gFnSc6	sliznice
celého	celý	k2eAgInSc2d1	celý
trávicího	trávicí	k2eAgInSc2d1	trávicí
traktu	trakt	k1gInSc2	trakt
<g/>
.	.	kIx.	.
</s>
<s>
Diagnostikovat	diagnostikovat	k5eAaBmF	diagnostikovat
lze	lze	k6eAd1	lze
virus	virus	k1gInSc4	virus
pomocí	pomocí	k7c2	pomocí
PCR	PCR	kA	PCR
<g/>
.	.	kIx.	.
</s>
<s>
Terapie	terapie	k1gFnSc1	terapie
se	se	k3xPyFc4	se
neprovádí	provádět	k5eNaImIp3nS	provádět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemích	zem	k1gFnPc6	zem
s	s	k7c7	s
enzootickým	enzootický	k2eAgInSc7d1	enzootický
výskytem	výskyt	k1gInSc7	výskyt
moru	mor	k1gInSc2	mor
skotu	skot	k1gInSc2	skot
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
prováděla	provádět	k5eAaImAgFnS	provádět
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
vakcinace	vakcinace	k1gFnSc1	vakcinace
živou	živý	k2eAgFnSc7d1	živá
atenuovanou	atenuovaný	k2eAgFnSc7d1	atenuovaná
vakcínou	vakcína	k1gFnSc7	vakcína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
výskytu	výskyt	k1gInSc2	výskyt
moru	mor	k1gInSc2	mor
by	by	kYmCp3nS	by
následovala	následovat	k5eAaImAgFnS	následovat
radikální	radikální	k2eAgFnSc1d1	radikální
likvidace	likvidace	k1gFnSc1	likvidace
všech	všecek	k3xTgNnPc2	všecek
nemocných	mocný	k2eNgNnPc2d1	nemocné
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
také	také	k6eAd1	také
podezřelých	podezřelý	k2eAgMnPc2d1	podezřelý
z	z	k7c2	z
nakažení	nakažení	k1gNnSc2	nakažení
i	i	k8xC	i
vnímavých	vnímavý	k2eAgInPc2d1	vnímavý
v	v	k7c6	v
ohnisku	ohnisko	k1gNnSc6	ohnisko
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mor	mora	k1gFnPc2	mora
skotu	skot	k1gInSc2	skot
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Mor	mor	k1gInSc1	mor
skotu	skot	k1gInSc2	skot
(	(	kIx(	(
<g/>
přednáška	přednáška	k1gFnSc1	přednáška
v	v	k7c6	v
Powerpointu	Powerpoint	k1gInSc6	Powerpoint
<g/>
)	)	kIx)	)
ŠTĚRBA	štěrba	k1gFnSc1	štěrba
O.	O.	kA	O.
Virové	virový	k2eAgFnSc2d1	virová
choroby	choroba	k1gFnSc2	choroba
spárkaté	spárkatý	k2eAgFnSc2d1	spárkatá
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
VFU	VFU	kA	VFU
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
TREML	TREML	kA	TREML
A	a	k9	a
KOL	kol	k7c2	kol
<g/>
..	..	k?	..
INFEKČNÍ	infekční	k2eAgFnSc2d1	infekční
CHOROBY	choroba	k1gFnSc2	choroba
ZVÍŘAT	zvíře	k1gNnPc2	zvíře
II	II	kA	II
virové	virový	k2eAgFnPc4d1	virová
a	a	k8xC	a
prionové	prionový	k2eAgFnPc4d1	prionová
infekce	infekce	k1gFnPc4	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
VFU	VFU	kA	VFU
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
96	[number]	k4	96
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7305	[number]	k4	7305
<g/>
-	-	kIx~	-
<g/>
705	[number]	k4	705
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Přednášky	přednáška	k1gFnPc1	přednáška
z	z	k7c2	z
epizootologie	epizootologie	k1gFnSc2	epizootologie
<g/>
,	,	kIx,	,
VFU	VFU	kA	VFU
Brno	Brno	k1gNnSc1	Brno
</s>
