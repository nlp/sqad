<s>
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
(	(	kIx(	(
<g/>
též	též	k9	též
alfa	alfa	k1gNnSc1	alfa
Centauri	Centaur	k1gFnSc2	Centaur
C	C	kA	C
a	a	k8xC	a
V	v	k7c6	v
645	[number]	k4	645
Centauri	Centauri	k1gNnSc6	Centauri
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hvězda	hvězda	k1gFnSc1	hvězda
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
4,22	[number]	k4	4,22
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
přibližně	přibližně	k6eAd1	přibližně
1,3	[number]	k4	1,3
parseku	parsec	k1gInSc2	parsec
nebo	nebo	k8xC	nebo
3,99	[number]	k4	3,99
<g/>
×	×	k?	×
<g/>
1013	[number]	k4	1013
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
)	)	kIx)	)
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
