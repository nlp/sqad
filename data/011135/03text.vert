<p>
<s>
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
(	(	kIx(	(
<g/>
též	též	k9	též
alfa	alfa	k1gNnSc1	alfa
Centauri	Centaur	k1gFnSc2	Centaur
C	C	kA	C
a	a	k8xC	a
V	v	k7c6	v
645	[number]	k4	645
Centauri	Centauri	k1gNnSc6	Centauri
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hvězda	hvězda	k1gFnSc1	hvězda
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
4,22	[number]	k4	4,22
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
přibližně	přibližně	k6eAd1	přibližně
1,3	[number]	k4	1,3
parseku	parsec	k1gInSc2	parsec
nebo	nebo	k8xC	nebo
3,99	[number]	k4	3,99
<g/>
×	×	k?	×
<g/>
1013	[number]	k4	1013
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
)	)	kIx)	)
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
nejbližší	blízký	k2eAgFnSc7d3	nejbližší
hvězdou	hvězda	k1gFnSc7	hvězda
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
červeného	červený	k2eAgMnSc4d1	červený
trpaslíka	trpaslík	k1gMnSc4	trpaslík
spektrální	spektrální	k2eAgFnSc2d1	spektrální
třídy	třída	k1gFnSc2	třída
M	M	kA	M
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Kentaura	kentaur	k1gMnSc2	kentaur
<g/>
,	,	kIx,	,
v	v	k7c6	v
úhlové	úhlový	k2eAgFnSc6d1	úhlová
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
<g/>
°	°	k?	°
od	od	k7c2	od
nejjasnější	jasný	k2eAgFnSc2d3	nejjasnější
hvězdy	hvězda	k1gFnSc2	hvězda
souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
<g/>
,	,	kIx,	,
alfy	alfa	k1gFnSc2	alfa
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
astrofyzikální	astrofyzikální	k2eAgFnSc6d1	astrofyzikální
stránce	stránka	k1gFnSc6	stránka
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nepravidelnou	pravidelný	k2eNgFnSc4d1	nepravidelná
eruptivní	eruptivní	k2eAgFnSc4d1	eruptivní
proměnnou	proměnný	k2eAgFnSc4d1	proměnná
hvězdu	hvězda	k1gFnSc4	hvězda
typu	typ	k1gInSc2	typ
UV	UV	kA	UV
Ceti	Ceti	k1gNnSc7	Ceti
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
poloze	poloha	k1gFnSc3	poloha
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
pozemské	pozemský	k2eAgFnSc6d1	pozemská
obloze	obloha	k1gFnSc6	obloha
není	být	k5eNaImIp3nS	být
Proxima	Proxima	k1gFnSc1	Proxima
severně	severně	k6eAd1	severně
od	od	k7c2	od
27	[number]	k4	27
<g/>
.	.	kIx.	.
rovnoběžky	rovnoběžka	k1gFnPc1	rovnoběžka
pozorovatelná	pozorovatelný	k2eAgFnSc1d1	pozorovatelná
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ani	ani	k8xC	ani
v	v	k7c6	v
nejjižnější	jižní	k2eAgFnSc6d3	nejjižnější
části	část	k1gFnSc6	část
Evropy	Evropa	k1gFnSc2	Evropa
nevychází	vycházet	k5eNaImIp3nS	vycházet
nad	nad	k7c4	nad
obzor	obzor	k1gInSc4	obzor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
pozemského	pozemský	k2eAgMnSc4d1	pozemský
pozorovatele	pozorovatel	k1gMnSc4	pozorovatel
se	se	k3xPyFc4	se
po	po	k7c6	po
Slunci	slunce	k1gNnSc6	slunce
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nejbližší	blízký	k2eAgFnSc4d3	nejbližší
známou	známý	k2eAgFnSc4d1	známá
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	hvězda	k1gFnSc1	hvězda
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
blízkost	blízkost	k1gFnSc4	blízkost
zdánlivou	zdánlivý	k2eAgFnSc4d1	zdánlivá
hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
velikost	velikost	k1gFnSc4	velikost
jen	jen	k9	jen
11	[number]	k4	11
<g/>
m	m	kA	m
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
100	[number]	k4	100
<g/>
krát	krát	k6eAd1	krát
slabší	slabý	k2eAgFnSc1d2	slabší
než	než	k8xS	než
nejslabší	slabý	k2eAgFnSc1d3	nejslabší
hvězda	hvězda	k1gFnSc1	hvězda
viditelná	viditelný	k2eAgFnSc1d1	viditelná
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
Robert	Robert	k1gMnSc1	Robert
Innes	Innes	k1gMnSc1	Innes
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
–	–	k?	–
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
tehdy	tehdy	k6eAd1	tehdy
ředitelem	ředitel	k1gMnSc7	ředitel
Union	union	k1gInSc1	union
Observatory	Observator	k1gMnPc7	Observator
v	v	k7c6	v
Johannesburgu	Johannesburg	k1gInSc6	Johannesburg
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pozorování	pozorování	k1gNnSc4	pozorování
hvězdy	hvězda	k1gFnSc2	hvězda
je	být	k5eAaImIp3nS	být
nutný	nutný	k2eAgInSc1d1	nutný
teleskop	teleskop	k1gInSc1	teleskop
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
minimálně	minimálně	k6eAd1	minimálně
8	[number]	k4	8
cm	cm	kA	cm
a	a	k8xC	a
i	i	k9	i
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
hvězdu	hvězda	k1gFnSc4	hvězda
pozorovat	pozorovat	k5eAaImF	pozorovat
jen	jen	k9	jen
za	za	k7c2	za
ideálních	ideální	k2eAgFnPc2d1	ideální
podmínek	podmínka	k1gFnPc2	podmínka
(	(	kIx(	(
<g/>
jasná	jasný	k2eAgFnSc1d1	jasná
obloha	obloha	k1gFnSc1	obloha
<g/>
,	,	kIx,	,
bezměsíčná	bezměsíčný	k2eAgFnSc1d1	bezměsíčná
noc	noc	k1gFnSc1	noc
<g/>
,	,	kIx,	,
hvězda	hvězda	k1gFnSc1	hvězda
vysoko	vysoko	k6eAd1	vysoko
nad	nad	k7c7	nad
obzorem	obzor	k1gInSc7	obzor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
13	[number]	k4	13
000	[number]	k4	000
astronomických	astronomický	k2eAgFnPc2d1	astronomická
jednotek	jednotka	k1gFnPc2	jednotka
(	(	kIx(	(
<g/>
0,21	[number]	k4	0,21
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
od	od	k7c2	od
těžiště	těžiště	k1gNnSc2	těžiště
soustavy	soustava	k1gFnSc2	soustava
A	A	kA	A
a	a	k8xC	a
B	B	kA	B
dvojhvězdy	dvojhvězda	k1gFnSc2	dvojhvězda
alfy	alfa	k1gFnSc2	alfa
Centauri	Centaur	k1gFnSc2	Centaur
a	a	k8xC	a
mohla	moct	k5eAaImAgFnS	moct
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
nacházet	nacházet	k5eAaImF	nacházet
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
o	o	k7c6	o
periodě	perioda	k1gFnSc6	perioda
přibližně	přibližně	k6eAd1	přibližně
500	[number]	k4	500
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
bývá	bývat	k5eAaImIp3nS	bývat
Proxima	Proxima	k1gFnSc1	Proxima
značena	značen	k2eAgFnSc1d1	značena
také	také	k9	také
jako	jako	k8xS	jako
alfa	alfa	k1gNnSc1	alfa
Centauri	Centauri	k1gNnSc2	Centauri
C.	C.	kA	C.
Přesná	přesný	k2eAgNnPc4d1	přesné
astrometrická	astrometrický	k2eAgNnPc4d1	astrometrický
měření	měření	k1gNnPc4	měření
družice	družice	k1gFnSc2	družice
Hipparcos	Hipparcosa	k1gFnPc2	Hipparcosa
však	však	k9	však
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
Proxima	Proxima	k1gNnSc1	Proxima
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
složky	složka	k1gFnPc1	složka
dvojhvězdy	dvojhvězda	k1gFnSc2	dvojhvězda
alfa	alfa	k1gNnSc2	alfa
Centauri	Centaur	k1gFnSc2	Centaur
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
gravitačně	gravitačně	k6eAd1	gravitačně
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
od	od	k7c2	od
sebe	se	k3xPyFc2	se
vzdálí	vzdálit	k5eAaPmIp3nP	vzdálit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
z	z	k7c2	z
alfy	alfa	k1gFnSc2	alfa
Centauri	Centaur	k1gFnSc2	Centaur
A	A	kA	A
nebo	nebo	k8xC	nebo
B	B	kA	B
by	by	kYmCp3nP	by
Proxima	Proximum	k1gNnPc1	Proximum
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
vypadala	vypadat	k5eAaPmAgFnS	vypadat
jako	jako	k9	jako
hvězda	hvězda	k1gFnSc1	hvězda
zdánlivé	zdánlivý	k2eAgFnSc2d1	zdánlivá
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
velikosti	velikost	k1gFnSc2	velikost
4,5	[number]	k4	4,5
<g/>
m.	m.	k?	m.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byl	být	k5eAaImAgInS	být
optickou	optický	k2eAgFnSc7d1	optická
interferometrií	interferometrie	k1gFnSc7	interferometrie
přímo	přímo	k6eAd1	přímo
změřen	změřen	k2eAgInSc1d1	změřen
její	její	k3xOp3gInSc1	její
zdánlivý	zdánlivý	k2eAgInSc1d1	zdánlivý
průměr	průměr	k1gInSc1	průměr
1,02	[number]	k4	1,02
±	±	k?	±
0,08	[number]	k4	0,08
tisícin	tisícina	k1gFnPc2	tisícina
obloukové	obloukový	k2eAgFnSc2d1	oblouková
vteřiny	vteřina	k1gFnSc2	vteřina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
planeta	planeta	k1gFnSc1	planeta
Jupiter	Jupiter	k1gInSc1	Jupiter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
byla	být	k5eAaImAgFnS	být
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
malé	malý	k2eAgFnSc3d1	malá
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
často	často	k6eAd1	často
navrhována	navrhován	k2eAgFnSc1d1	navrhována
jako	jako	k8xC	jako
první	první	k4xOgInSc4	první
cíl	cíl	k1gInSc4	cíl
průkopnického	průkopnický	k2eAgInSc2d1	průkopnický
mezihvězdného	mezihvězdný	k2eAgInSc2d1	mezihvězdný
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
oznámila	oznámit	k5eAaPmAgFnS	oznámit
Evropská	evropský	k2eAgFnSc1d1	Evropská
jižní	jižní	k2eAgFnSc1d1	jižní
observatoř	observatoř	k1gFnSc1	observatoř
objev	objev	k1gInSc1	objev
Proximy	Proxima	k1gFnSc2	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
b	b	k?	b
<g/>
,	,	kIx,	,
planety	planeta	k1gFnPc1	planeta
obíhající	obíhající	k2eAgFnSc4d1	obíhající
hvězdu	hvězda	k1gFnSc4	hvězda
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
přibližně	přibližně	k6eAd1	přibližně
0,05	[number]	k4	0,05
astronomické	astronomický	k2eAgFnSc2d1	astronomická
jednotky	jednotka	k1gFnSc2	jednotka
(	(	kIx(	(
<g/>
7	[number]	k4	7
500	[number]	k4	500
000	[number]	k4	000
km	km	kA	km
<g/>
)	)	kIx)	)
s	s	k7c7	s
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
dobou	doba	k1gFnSc7	doba
přibližně	přibližně	k6eAd1	přibližně
11,2	[number]	k4	11,2
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
odhadovaná	odhadovaný	k2eAgFnSc1d1	odhadovaná
hmotnost	hmotnost	k1gFnSc1	hmotnost
nejméně	málo	k6eAd3	málo
1,3	[number]	k4	1,3
<g/>
násobkem	násobek	k1gInSc7	násobek
hmotnosti	hmotnost	k1gFnPc1	hmotnost
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Rovnovážná	rovnovážný	k2eAgFnSc1d1	rovnovážná
teplota	teplota	k1gFnSc1	teplota
Proximy	Proxima	k1gFnSc2	Proxima
b	b	k?	b
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
takovém	takový	k3xDgNnSc6	takový
rozmezí	rozmezí	k1gNnSc6	rozmezí
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
povrchu	povrch	k1gInSc6	povrch
mohlo	moct	k5eAaImAgNnS	moct
umožňovat	umožňovat	k5eAaImF	umožňovat
existenci	existence	k1gFnSc4	existence
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
kapalném	kapalný	k2eAgNnSc6d1	kapalné
skupenství	skupenství	k1gNnSc6	skupenství
<g/>
,	,	kIx,	,
planeta	planeta	k1gFnSc1	planeta
tedy	tedy	k9	tedy
obíhá	obíhat	k5eAaImIp3nS	obíhat
v	v	k7c6	v
obyvatelné	obyvatelný	k2eAgFnSc6d1	obyvatelná
zóně	zóna	k1gFnSc6	zóna
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
je	být	k5eAaImIp3nS	být
červený	červený	k2eAgMnSc1d1	červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
sporné	sporný	k2eAgNnSc1d1	sporné
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
na	na	k7c6	na
jejích	její	k3xOp3gFnPc6	její
planetách	planeta	k1gFnPc6	planeta
mohl	moct	k5eAaImAgInS	moct
existovat	existovat	k5eAaImF	existovat
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgNnSc1d1	předchozí
hledání	hledání	k1gNnSc1	hledání
planet	planeta	k1gFnPc2	planeta
vyloučilo	vyloučit	k5eAaPmAgNnS	vyloučit
přítomnost	přítomnost	k1gFnSc4	přítomnost
hnědých	hnědý	k2eAgMnPc2d1	hnědý
trpaslíků	trpaslík	k1gMnPc2	trpaslík
a	a	k8xC	a
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
detekoval	detekovat	k5eAaImAgInS	detekovat
radioteleskop	radioteleskop	k1gInSc4	radioteleskop
ALMA	alma	k1gFnSc1	alma
prach	prach	k1gInSc1	prach
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Proximy	Proxima	k1gFnSc2	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
.	.	kIx.	.
</s>
<s>
Pozorovaný	pozorovaný	k2eAgInSc1d1	pozorovaný
protoplanetární	protoplanetární	k2eAgInSc1d1	protoplanetární
disk	disk	k1gInSc1	disk
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
pásu	pás	k1gInSc6	pás
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
několika	několik	k4yIc2	několik
set	set	k1gInSc4	set
milionů	milion	k4xCgInPc2	milion
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
mohl	moct	k5eAaImAgInS	moct
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
známkou	známka	k1gFnSc7	známka
přítomnosti	přítomnost	k1gFnSc2	přítomnost
komplexního	komplexní	k2eAgInSc2d1	komplexní
planetárního	planetární	k2eAgInSc2d1	planetární
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
jména	jméno	k1gNnSc2	jméno
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
hvězdy	hvězda	k1gFnSc2	hvězda
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
proxima	proxim	k1gMnSc2	proxim
<g/>
)	)	kIx)	)
–	–	k?	–
nejbližší	blízký	k2eAgFnSc7d3	nejbližší
v	v	k7c6	v
ženském	ženský	k2eAgInSc6d1	ženský
rodu	rod	k1gInSc6	rod
a	a	k8xC	a
názvu	název	k1gInSc3	název
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
v	v	k7c6	v
genitivu	genitiv	k1gInSc6	genitiv
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
jména	jméno	k1gNnSc2	jméno
hvězdy	hvězda	k1gFnSc2	hvězda
je	být	k5eAaImIp3nS	být
Nejbližší	blízký	k2eAgFnSc1d3	nejbližší
(	(	kIx(	(
<g/>
z	z	k7c2	z
<g/>
)	)	kIx)	)
Kentaura	kentaur	k1gMnSc2	kentaur
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
V	v	k7c6	v
<g/>
645	[number]	k4	645
Centauri	Centauri	k1gNnPc6	Centauri
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
pravidly	pravidlo	k1gNnPc7	pravidlo
pro	pro	k7c4	pro
pojmenování	pojmenování	k1gNnSc4	pojmenování
proměnných	proměnný	k2eAgFnPc2d1	proměnná
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
645	[number]	k4	645
<g/>
.	.	kIx.	.
proměnnou	proměnná	k1gFnSc4	proměnná
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Kentaura	kentaur	k1gMnSc2	kentaur
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
vlastnosti	vlastnost	k1gFnPc4	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
je	být	k5eAaImIp3nS	být
červeným	červený	k2eAgMnSc7d1	červený
trpaslíkem	trpaslík	k1gMnSc7	trpaslík
spektrální	spektrální	k2eAgFnSc2d1	spektrální
třídy	třída	k1gFnSc2	třída
M	M	kA	M
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
hlavní	hlavní	k2eAgFnSc3d1	hlavní
posloupnosti	posloupnost	k1gFnSc3	posloupnost
hvězd	hvězda	k1gFnPc2	hvězda
v	v	k7c6	v
Hertzsprung-Russellově	Hertzsprung-Russellův	k2eAgInSc6d1	Hertzsprung-Russellův
diagramu	diagram	k1gInSc6	diagram
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	Hvězda	k1gMnSc1	Hvězda
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
klasifikována	klasifikován	k2eAgFnSc1d1	klasifikována
jako	jako	k8xC	jako
pozdní	pozdní	k2eAgFnSc1d1	pozdní
M	M	kA	M
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Klasifikace	klasifikace	k1gFnSc1	klasifikace
M	M	kA	M
<g/>
5,5	[number]	k4	5,5
Ve	v	k7c6	v
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
málo	málo	k6eAd1	málo
hmotného	hmotný	k2eAgMnSc4d1	hmotný
trpaslíka	trpaslík	k1gMnSc4	trpaslík
třídy	třída	k1gFnSc2	třída
M.	M.	kA	M.
</s>
</p>
<p>
<s>
Se	s	k7c7	s
sotva	sotva	k6eAd1	sotva
12,3	[number]	k4	12,3
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc3	hmotnost
Slunce	slunce	k1gNnSc2	slunce
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
129	[number]	k4	129
hmotností	hmotnost	k1gFnSc7	hmotnost
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
hvězda	hvězda	k1gFnSc1	hvězda
přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
blízkost	blízkost	k1gFnSc4	blízkost
zdánlivou	zdánlivý	k2eAgFnSc4d1	zdánlivá
hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
velikost	velikost	k1gFnSc4	velikost
11,05	[number]	k4	11,05
<g/>
m.	m.	k?	m.
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
stokrát	stokrát	k6eAd1	stokrát
méně	málo	k6eAd2	málo
než	než	k8xS	než
nejslabší	slabý	k2eAgInSc1d3	nejslabší
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
pozorovatelná	pozorovatelný	k2eAgFnSc1d1	pozorovatelná
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Absolutní	absolutní	k2eAgFnSc1d1	absolutní
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
velikost	velikost	k1gFnSc1	velikost
(	(	kIx(	(
<g/>
jasnost	jasnost	k1gFnSc1	jasnost
<g/>
,	,	kIx,	,
jakou	jaký	k3yQgFnSc4	jaký
má	mít	k5eAaImIp3nS	mít
hvězda	hvězda	k1gFnSc1	hvězda
ze	z	k7c2	z
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
10	[number]	k4	10
parseků	parsec	k1gInPc2	parsec
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
15,5	[number]	k4	15,5
<g/>
m.	m.	k?	m.
Jestliže	jestliže	k8xS	jestliže
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
by	by	kYmCp3nS	by
jen	jen	k9	jen
stokrát	stokrát	k6eAd1	stokrát
jasnější	jasný	k2eAgFnSc1d2	jasnější
než	než	k8xS	než
Měsíc	měsíc	k1gInSc1	měsíc
v	v	k7c6	v
úplňku	úplněk	k1gInSc6	úplněk
<g/>
.	.	kIx.	.
</s>
<s>
Planety	planeta	k1gFnPc1	planeta
by	by	k9	by
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
nebyly	být	k5eNaImAgFnP	být
viditelné	viditelný	k2eAgFnPc1d1	viditelná
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
zářila	zářit	k5eAaImAgFnS	zářit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
své	svůj	k3xOyFgFnSc2	svůj
největší	veliký	k2eAgFnSc2d3	veliký
jasnosti	jasnost	k1gFnSc2	jasnost
jako	jako	k8xC	jako
hvězda	hvězda	k1gFnSc1	hvězda
šesté	šestý	k4xOgFnSc2	šestý
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Úplněk	úplněk	k1gInSc1	úplněk
by	by	kYmCp3nS	by
zářil	zářit	k5eAaImAgInS	zářit
jako	jako	k9	jako
matný	matný	k2eAgInSc1d1	matný
červený	červený	k2eAgInSc1d1	červený
kotouč	kotouč	k1gInSc1	kotouč
o	o	k7c6	o
zdánlivé	zdánlivý	k2eAgFnSc6d1	zdánlivá
velikosti	velikost	k1gFnSc6	velikost
−	−	k?	−
<g/>
m.	m.	k?	m.
<g/>
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
energie	energie	k1gFnSc2	energie
než	než	k8xS	než
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
záření	záření	k1gNnSc1	záření
vydává	vydávat	k5eAaImIp3nS	vydávat
hvězda	hvězda	k1gFnSc1	hvězda
v	v	k7c6	v
infračervené	infračervený	k2eAgFnSc6d1	infračervená
oblasti	oblast	k1gFnSc6	oblast
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
na	na	k7c6	na
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
1,2	[number]	k4	1,2
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
viditelné	viditelný	k2eAgFnSc6d1	viditelná
oblasti	oblast	k1gFnSc6	oblast
spektra	spektrum	k1gNnSc2	spektrum
má	mít	k5eAaImIp3nS	mít
hvězda	hvězda	k1gFnSc1	hvězda
jen	jen	k9	jen
0,056	[number]	k4	0,056
%	%	kIx~	%
svítivosti	svítivost	k1gFnSc3	svítivost
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Chromosféra	chromosféra	k1gFnSc1	chromosféra
hvězdy	hvězda	k1gFnSc2	hvězda
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgMnSc1d1	aktivní
a	a	k8xC	a
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
emise	emise	k1gFnSc1	emise
jednou	jednou	k6eAd1	jednou
ionizovaného	ionizovaný	k2eAgInSc2d1	ionizovaný
hořčíku	hořčík	k1gInSc2	hořčík
na	na	k7c6	na
280	[number]	k4	280
nanometrech	nanometr	k1gInPc6	nanometr
<g/>
.	.	kIx.	.
<g/>
Jestliže	jestliže	k8xS	jestliže
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
Proxima	Proxima	k1gFnSc1	Proxima
o	o	k7c4	o
třetinu	třetina	k1gFnSc4	třetina
menší	malý	k2eAgFnSc1d2	menší
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
,	,	kIx,	,
nemohla	moct	k5eNaImAgFnS	moct
by	by	kYmCp3nS	by
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
probíhat	probíhat	k5eAaImF	probíhat
již	již	k6eAd1	již
fúze	fúze	k1gFnSc1	fúze
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
nespadala	spadat	k5eNaPmAgFnS	spadat
by	by	kYmCp3nS	by
pod	pod	k7c4	pod
definici	definice	k1gFnSc4	definice
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
by	by	kYmCp3nS	by
pouze	pouze	k6eAd1	pouze
hnědým	hnědý	k2eAgMnSc7d1	hnědý
trpaslíkem	trpaslík	k1gMnSc7	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Gravitace	gravitace	k1gFnSc1	gravitace
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
hvězdy	hvězda	k1gFnSc2	hvězda
je	být	k5eAaImIp3nS	být
5,20	[number]	k4	5,20
log	log	kA	log
<g/>
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
astrofyzice	astrofyzika	k1gFnSc6	astrofyzika
je	být	k5eAaImIp3nS	být
povrchová	povrchový	k2eAgFnSc1d1	povrchová
gravitace	gravitace	k1gFnSc1	gravitace
vyjádřená	vyjádřený	k2eAgFnSc1d1	vyjádřená
v	v	k7c6	v
log	log	kA	log
g.	g.	k?	g.
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
hustota	hustota	k1gFnSc1	hustota
hvězdy	hvězda	k1gFnSc2	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
stoupá	stoupat	k5eAaImIp3nS	stoupat
se	s	k7c7	s
snižující	snižující	k2eAgFnSc7d1	snižující
se	se	k3xPyFc4	se
hmotností	hmotnost	k1gFnSc7	hmotnost
hvězdy	hvězda	k1gFnPc1	hvězda
a	a	k8xC	a
Proxima	Proxima	k1gFnSc1	Proxima
není	být	k5eNaImIp3nS	být
mezi	mezi	k7c7	mezi
hvězdami	hvězda	k1gFnPc7	hvězda
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	hvězda	k1gFnSc1	hvězda
má	mít	k5eAaImIp3nS	mít
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
hustotu	hustota	k1gFnSc4	hustota
56	[number]	k4	56
800	[number]	k4	800
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
Slunce	slunce	k1gNnSc2	slunce
má	mít	k5eAaImIp3nS	mít
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
hustotu	hustota	k1gFnSc4	hustota
1	[number]	k4	1
409	[number]	k4	409
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
relativně	relativně	k6eAd1	relativně
slabý	slabý	k2eAgInSc4d1	slabý
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
vítr	vítr	k1gInSc4	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Úbytek	úbytek	k1gInSc1	úbytek
hmotnosti	hmotnost	k1gFnSc2	hmotnost
hvězdy	hvězda	k1gFnSc2	hvězda
nepřekračuje	překračovat	k5eNaImIp3nS	překračovat
20	[number]	k4	20
%	%	kIx~	%
procentuálního	procentuální	k2eAgInSc2d1	procentuální
úbytku	úbytek	k1gInSc2	úbytek
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
slunečním	sluneční	k2eAgInSc7d1	sluneční
větrem	vítr	k1gInSc7	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	Hvězda	k1gMnSc1	Hvězda
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
úbytek	úbytek	k1gInSc4	úbytek
hmotnosti	hmotnost	k1gFnSc2	hmotnost
na	na	k7c4	na
jednotku	jednotka	k1gFnSc4	jednotka
plochy	plocha	k1gFnSc2	plocha
osmkrát	osmkrát	k6eAd1	osmkrát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
u	u	k7c2	u
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
<g/>
Vlivem	vlivem	k7c2	vlivem
své	svůj	k3xOyFgFnSc2	svůj
blízkosti	blízkost	k1gFnSc2	blízkost
hvězdy	hvězda	k1gFnSc2	hvězda
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
změřen	změřen	k2eAgInSc1d1	změřen
úhlový	úhlový	k2eAgInSc1d1	úhlový
průměr	průměr	k1gInSc1	průměr
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Dalekohledy	dalekohled	k1gInPc1	dalekohled
VLT	VLT	kA	VLT
v	v	k7c4	v
Observatoře	observatoř	k1gFnPc4	observatoř
Paranal	Paranal	k1gFnSc2	Paranal
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
změřily	změřit	k5eAaPmAgInP	změřit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
optického	optický	k2eAgInSc2d1	optický
interferometru	interferometr	k1gInSc2	interferometr
úhlový	úhlový	k2eAgInSc1d1	úhlový
průměr	průměr	k1gInSc1	průměr
Proxima	Proximum	k1gNnSc2	Proximum
Centauri	Centaur	k1gFnSc2	Centaur
na	na	k7c4	na
1,02	[number]	k4	1,02
±	±	k?	±
0,08	[number]	k4	0,08
milivteřin	milivteřina	k1gFnPc2	milivteřina
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
známé	známý	k2eAgFnSc2d1	známá
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
hvězdy	hvězda	k1gFnSc2	hvězda
byl	být	k5eAaImAgInS	být
vypočítán	vypočítán	k2eAgInSc1d1	vypočítán
průměr	průměr	k1gInSc1	průměr
hvězdy	hvězda	k1gFnSc2	hvězda
na	na	k7c4	na
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
sedmina	sedmina	k1gFnSc1	sedmina
průměru	průměr	k1gInSc2	průměr
Slunce	slunce	k1gNnSc2	slunce
nebo	nebo	k8xC	nebo
1,5	[number]	k4	1,5
násobek	násobek	k1gInSc1	násobek
průměru	průměr	k1gInSc2	průměr
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Eruptivní	eruptivní	k2eAgFnSc1d1	eruptivní
proměnná	proměnný	k2eAgFnSc1d1	proměnná
hvězda	hvězda	k1gFnSc1	hvězda
===	===	k?	===
</s>
</p>
<p>
<s>
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
eruptivních	eruptivní	k2eAgFnPc2d1	eruptivní
proměnných	proměnný	k2eAgFnPc2d1	proměnná
hvězd	hvězda	k1gFnPc2	hvězda
typu	typ	k1gInSc2	typ
UV	UV	kA	UV
Ceti	Cete	k1gFnSc4	Cete
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
jasnost	jasnost	k1gFnSc1	jasnost
kvůli	kvůli	k7c3	kvůli
magnetické	magnetický	k2eAgFnSc3d1	magnetická
aktivitě	aktivita	k1gFnSc3	aktivita
hvězdy	hvězda	k1gFnSc2	hvězda
stoupá	stoupat	k5eAaImIp3nS	stoupat
nad	nad	k7c4	nad
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
malé	malý	k2eAgFnSc2d1	malá
hmotnosti	hmotnost	k1gFnSc2	hmotnost
hvězdy	hvězda	k1gFnSc2	hvězda
je	být	k5eAaImIp3nS	být
nitro	nitro	k1gNnSc1	nitro
hvězdy	hvězda	k1gFnSc2	hvězda
plně	plně	k6eAd1	plně
konvektivní	konvektivní	k2eAgInSc1d1	konvektivní
(	(	kIx(	(
<g/>
vytvořené	vytvořený	k2eAgNnSc1d1	vytvořené
teplo	teplo	k1gNnSc1	teplo
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
vnitřku	vnitřek	k1gInSc2	vnitřek
hvězdy	hvězda	k1gFnSc2	hvězda
ven	ven	k6eAd1	ven
přenášené	přenášený	k2eAgNnSc1d1	přenášené
prouděním	proudění	k1gNnSc7	proudění
plazmatu	plazma	k1gNnSc2	plazma
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
zářením	záření	k1gNnSc7	záření
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konvekce	konvekce	k1gFnSc1	konvekce
je	být	k5eAaImIp3nS	být
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
vytvářením	vytváření	k1gNnSc7	vytváření
a	a	k8xC	a
přenosem	přenos	k1gInSc7	přenos
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
hvězdy	hvězda	k1gFnSc2	hvězda
je	být	k5eAaImIp3nS	být
magnetická	magnetický	k2eAgFnSc1d1	magnetická
energie	energie	k1gFnSc1	energie
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
hvězdy	hvězda	k1gFnSc2	hvězda
uvolněna	uvolněn	k2eAgFnSc1d1	uvolněna
erupcemi	erupce	k1gFnPc7	erupce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
zdvojnásobit	zdvojnásobit	k5eAaPmF	zdvojnásobit
jasnost	jasnost	k1gFnSc4	jasnost
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
zvýšení	zvýšení	k1gNnSc1	zvýšení
jasnosti	jasnost	k1gFnSc2	jasnost
hvězdy	hvězda	k1gFnSc2	hvězda
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
magnitudu	magnituda	k1gFnSc4	magnituda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Erupce	erupce	k1gFnPc1	erupce
mohou	moct	k5eAaImIp3nP	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
velikosti	velikost	k1gFnPc4	velikost
hvězdy	hvězda	k1gFnPc4	hvězda
a	a	k8xC	a
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
teploty	teplota	k1gFnSc2	teplota
až	až	k9	až
2	[number]	k4	2
miliónů	milión	k4xCgInPc2	milión
Kelvinů	kelvin	k1gInPc2	kelvin
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
vysoké	vysoký	k2eAgFnSc2d1	vysoká
teploty	teplota	k1gFnSc2	teplota
erupcí	erupce	k1gFnPc2	erupce
může	moct	k5eAaImIp3nS	moct
hvězda	hvězda	k1gFnSc1	hvězda
zářit	zářit	k5eAaImF	zářit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
stejnou	stejný	k2eAgFnSc7d1	stejná
jasností	jasnost	k1gFnSc7	jasnost
jako	jako	k8xC	jako
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
hodnota	hodnota	k1gFnSc1	hodnota
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
největší	veliký	k2eAgFnSc2d3	veliký
erupce	erupce	k1gFnSc2	erupce
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k9	až
1028	[number]	k4	1028
erg	erg	k1gInSc1	erg
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
<g/>
Až	až	k9	až
88	[number]	k4	88
%	%	kIx~	%
povrchu	povrch	k1gInSc2	povrch
hvězdy	hvězda	k1gFnSc2	hvězda
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgInSc4d2	veliký
podíl	podíl	k1gInSc4	podíl
než	než	k8xS	než
u	u	k7c2	u
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gFnSc2	jeho
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
aktivity	aktivita	k1gFnSc2	aktivita
slunečních	sluneční	k2eAgFnPc2d1	sluneční
skvrn	skvrna	k1gFnPc2	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
v	v	k7c6	v
klidných	klidný	k2eAgFnPc6d1	klidná
dobách	doba	k1gFnPc6	doba
s	s	k7c7	s
málo	málo	k6eAd1	málo
nebo	nebo	k8xC	nebo
žádnými	žádný	k3yNgFnPc7	žádný
erupcemi	erupce	k1gFnPc7	erupce
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
tato	tento	k3xDgFnSc1	tento
aktivita	aktivita	k1gFnSc1	aktivita
teplotu	teplota	k1gFnSc4	teplota
korony	korona	k1gFnSc2	korona
Proximy	Proxima	k1gFnSc2	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
až	až	k9	až
na	na	k7c4	na
3,5	[number]	k4	3,5
miliónů	milión	k4xCgInPc2	milión
Kelvinů	kelvin	k1gInPc2	kelvin
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
teplota	teplota	k1gFnSc1	teplota
sluneční	sluneční	k2eAgFnSc2d1	sluneční
koróny	koróna	k1gFnSc2	koróna
je	být	k5eAaImIp3nS	být
maximálně	maximálně	k6eAd1	maximálně
2	[number]	k4	2
miliónů	milión	k4xCgInPc2	milión
Kelvinů	kelvin	k1gInPc2	kelvin
<g/>
.	.	kIx.	.
<g/>
Celková	celkový	k2eAgFnSc1d1	celková
aktivita	aktivita	k1gFnSc1	aktivita
Proximy	Proxima	k1gFnSc2	Proxima
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
červenými	červený	k2eAgInPc7d1	červený
trpaslíky	trpaslík	k1gMnPc4	trpaslík
označována	označován	k2eAgFnSc1d1	označována
jako	jako	k8xS	jako
relativně	relativně	k6eAd1	relativně
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
odhadovanému	odhadovaný	k2eAgInSc3d1	odhadovaný
věku	věk	k1gInSc3	věk
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Aktivita	aktivita	k1gFnSc1	aktivita
hvězdy	hvězda	k1gFnSc2	hvězda
se	se	k3xPyFc4	se
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
ubývající	ubývající	k2eAgFnSc2d1	ubývající
rychlosti	rychlost	k1gFnSc2	rychlost
rotace	rotace	k1gFnSc1	rotace
hvězdy	hvězda	k1gFnSc2	hvězda
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
<g/>
Blízkost	blízkost	k1gFnSc1	blízkost
hvězdy	hvězda	k1gFnSc2	hvězda
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
dobře	dobře	k6eAd1	dobře
pozorovat	pozorovat	k5eAaImF	pozorovat
její	její	k3xOp3gFnPc4	její
erupce	erupce	k1gFnPc4	erupce
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
proměnnosti	proměnnost	k1gFnSc2	proměnnost
hvězdy	hvězda	k1gFnSc2	hvězda
je	být	k5eAaImIp3nS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
442	[number]	k4	442
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdný	hvězdný	k2eAgInSc1d1	hvězdný
cyklus	cyklus	k1gInSc1	cyklus
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
7	[number]	k4	7
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
sonda	sonda	k1gFnSc1	sonda
Einstein	Einstein	k1gMnSc1	Einstein
Observatory	Observator	k1gInPc4	Observator
(	(	kIx(	(
<g/>
High	High	k1gMnSc1	High
Energy	Energ	k1gMnPc7	Energ
Astronomy	astronom	k1gMnPc4	astronom
Observatory	Observator	k1gInPc1	Observator
2	[number]	k4	2
<g/>
)	)	kIx)	)
přesnou	přesný	k2eAgFnSc4d1	přesná
křivku	křivka	k1gFnSc4	křivka
rentgenové	rentgenový	k2eAgFnSc2d1	rentgenová
energie	energie	k1gFnSc2	energie
její	její	k3xOp3gFnSc2	její
erupce	erupce	k1gFnSc2	erupce
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc4d1	další
pozorování	pozorování	k1gNnSc4	pozorování
erupcí	erupce	k1gFnPc2	erupce
provedly	provést	k5eAaPmAgFnP	provést
sondy	sonda	k1gFnPc1	sonda
EXOSAT	EXOSAT	kA	EXOSAT
a	a	k8xC	a
ROSAT	ROSAT	kA	ROSAT
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc1	měření
emisí	emise	k1gFnPc2	emise
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
menších	malý	k2eAgFnPc2d2	menší
erupcí	erupce	k1gFnPc2	erupce
<g/>
,	,	kIx,	,
podobných	podobný	k2eAgFnPc2d1	podobná
slunečním	sluneční	k2eAgNnSc7d1	sluneční
<g/>
,	,	kIx,	,
provedla	provést	k5eAaPmAgFnS	provést
japonská	japonský	k2eAgFnSc1d1	japonská
sonda	sonda	k1gFnSc1	sonda
ASCA	ASCA	kA	ASCA
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
cílem	cíl	k1gInSc7	cíl
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
sond	sonda	k1gFnPc2	sonda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
pozorují	pozorovat	k5eAaImIp3nP	pozorovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
XMM-Newton	XMM-Newton	k1gInSc1	XMM-Newton
a	a	k8xC	a
Chandra	chandra	k1gFnSc1	chandra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgInSc1d1	další
vývoj	vývoj	k1gInSc1	vývoj
hvězdy	hvězda	k1gFnSc2	hvězda
===	===	k?	===
</s>
</p>
<p>
<s>
Vlivem	vliv	k1gInSc7	vliv
relativně	relativně	k6eAd1	relativně
nízké	nízký	k2eAgFnSc6d1	nízká
produkci	produkce	k1gFnSc6	produkce
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
promíchávání	promíchávání	k1gNnSc2	promíchávání
prvků	prvek	k1gInPc2	prvek
konvekcí	konvekce	k1gFnPc2	konvekce
uvnitř	uvnitř	k7c2	uvnitř
hvězdy	hvězda	k1gFnSc2	hvězda
bude	být	k5eAaImBp3nS	být
červený	červený	k2eAgMnSc1d1	červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
Proximy	Proxima	k1gFnSc2	Proxima
Centauri	Centauri	k1gNnSc2	Centauri
přibližně	přibližně	k6eAd1	přibližně
4	[number]	k4	4
bilióny	bilión	k4xCgInPc7	bilión
let	léto	k1gNnPc2	léto
hvězdou	hvězda	k1gFnSc7	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
,	,	kIx,	,
daleko	daleko	k6eAd1	daleko
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
ostatní	ostatní	k2eAgFnPc1d1	ostatní
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
přibližně	přibližně	k6eAd1	přibližně
300	[number]	k4	300
násobku	násobek	k1gInSc2	násobek
věku	věk	k1gInSc2	věk
dnešního	dnešní	k2eAgInSc2d1	dnešní
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Proxima	Proxima	k1gNnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
jako	jako	k9	jako
všichni	všechen	k3xTgMnPc1	všechen
ostatní	ostatní	k2eAgMnPc1d1	ostatní
červení	červený	k2eAgMnPc1d1	červený
trpaslíci	trpaslík	k1gMnPc1	trpaslík
uskutečňuje	uskutečňovat	k5eAaImIp3nS	uskutečňovat
přenos	přenos	k1gInSc1	přenos
tepla	teplo	k1gNnSc2	teplo
konvekcí	konvekce	k1gFnPc2	konvekce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
helium	helium	k1gNnSc1	helium
produkované	produkovaný	k2eAgNnSc1d1	produkované
termojadernou	termojaderný	k2eAgFnSc7d1	termojaderná
fúzí	fúze	k1gFnSc7	fúze
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
rozdělené	rozdělená	k1gFnSc2	rozdělená
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
hvězdě	hvězda	k1gFnSc6	hvězda
a	a	k8xC	a
neshromažďuje	shromažďovat	k5eNaImIp3nS	shromažďovat
se	se	k3xPyFc4	se
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Slunci	slunce	k1gNnSc6	slunce
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
shromažďuje	shromažďovat	k5eAaImIp3nS	shromažďovat
helium	helium	k1gNnSc4	helium
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
jádru	jádro	k1gNnSc6	jádro
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
spotřebováno	spotřebován	k2eAgNnSc1d1	spotřebováno
jadernou	jaderný	k2eAgFnSc7d1	jaderná
fúzí	fúze	k1gFnSc7	fúze
jen	jen	k9	jen
10	[number]	k4	10
%	%	kIx~	%
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
má	mít	k5eAaImIp3nS	mít
hvězda	hvězda	k1gFnSc1	hvězda
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
,	,	kIx,	,
když	když	k8xS	když
opustí	opustit	k5eAaPmIp3nS	opustit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
posloupnost	posloupnost	k1gFnSc4	posloupnost
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
Proxima	Proxima	k1gFnSc1	Proxima
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
vodíku	vodík	k1gInSc2	vodík
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
opustí	opustit	k5eAaPmIp3nS	opustit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
posloupnost	posloupnost	k1gFnSc4	posloupnost
a	a	k8xC	a
skončí	skončit	k5eAaPmIp3nS	skončit
termonukleární	termonukleární	k2eAgFnSc1d1	termonukleární
fúze	fúze	k1gFnSc1	fúze
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
<g/>
Když	když	k8xS	když
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
fúzi	fúze	k1gFnSc3	fúze
vodíku	vodík	k1gInSc2	vodík
ve	v	k7c6	v
hvězdě	hvězda	k1gFnSc6	hvězda
shromáždí	shromáždit	k5eAaPmIp3nS	shromáždit
více	hodně	k6eAd2	hodně
helia	helium	k1gNnPc4	helium
<g/>
,	,	kIx,	,
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
hvězda	hvězda	k1gFnSc1	hvězda
menší	malý	k2eAgFnSc1d2	menší
a	a	k8xC	a
teplejší	teplý	k2eAgFnSc1d2	teplejší
a	a	k8xC	a
změní	změnit	k5eAaPmIp3nS	změnit
svojí	svůj	k3xOyFgFnSc3	svůj
barvu	barva	k1gFnSc4	barva
z	z	k7c2	z
červené	červená	k1gFnSc2	červená
na	na	k7c4	na
modrou	modrý	k2eAgFnSc4d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
stádiu	stádium	k1gNnSc6	stádium
vývoje	vývoj	k1gInSc2	vývoj
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
hvězda	hvězda	k1gFnSc1	hvězda
zřetelně	zřetelně	k6eAd1	zřetelně
jasnější	jasný	k2eAgFnSc1d2	jasnější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
2,5	[number]	k4	2,5
%	%	kIx~	%
dnešní	dnešní	k2eAgFnSc2d1	dnešní
svítivosti	svítivost	k1gFnSc2	svítivost
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
několik	několik	k4yIc4	několik
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
objektech	objekt	k1gInPc6	objekt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
obíhají	obíhat	k5eAaImIp3nP	obíhat
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
vyčerpají	vyčerpat	k5eAaPmIp3nP	vyčerpat
zásoby	zásoba	k1gFnPc4	zásoba
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
vývojové	vývojový	k2eAgFnSc2d1	vývojová
fáze	fáze	k1gFnSc2	fáze
červeného	červený	k2eAgMnSc2d1	červený
obra	obr	k1gMnSc2	obr
<g/>
,	,	kIx,	,
bílým	bílý	k2eAgMnSc7d1	bílý
trpaslíkem	trpaslík	k1gMnSc7	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
bude	být	k5eAaImBp3nS	být
hvězda	hvězda	k1gFnSc1	hvězda
pomalu	pomalu	k6eAd1	pomalu
ztrácet	ztrácet	k5eAaImF	ztrácet
zbývající	zbývající	k2eAgNnSc4d1	zbývající
teplo	teplo	k1gNnSc4	teplo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Astrometrie	astrometrie	k1gFnSc2	astrometrie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Galaktická	galaktický	k2eAgFnSc1d1	Galaktická
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
===	===	k?	===
</s>
</p>
<p>
<s>
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
obíhá	obíhat	k5eAaImIp3nS	obíhat
centrum	centrum	k1gNnSc1	centrum
Mléčné	mléčný	k2eAgFnSc2d1	mléčná
dráhy	dráha	k1gFnSc2	dráha
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
kolísá	kolísat	k5eAaImIp3nS	kolísat
mezí	mez	k1gFnSc7	mez
8,313	[number]	k4	8,313
až	až	k9	až
9,546	[number]	k4	9,546
kpc	kpc	k?	kpc
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
excentricitu	excentricita	k1gFnSc4	excentricita
0,069	[number]	k4	0,069
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgInSc1d1	vlastní
pohyb	pohyb	k1gInSc1	pohyb
hvězdy	hvězda	k1gFnSc2	hvězda
po	po	k7c6	po
obloze	obloha	k1gFnSc6	obloha
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
malé	malý	k2eAgFnSc3d1	malá
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
hvězdy	hvězda	k1gFnSc2	hvězda
relativně	relativně	k6eAd1	relativně
velký	velký	k2eAgInSc1d1	velký
3,85	[number]	k4	3,85
<g/>
''	''	k?	''
<g/>
/	/	kIx~	/
<g/>
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
sotva	sotva	k6eAd1	sotva
pěti	pět	k4xCc2	pět
set	sto	k4xCgNnPc2	sto
let	léto	k1gNnPc2	léto
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
překoná	překonat	k5eAaPmIp3nS	překonat
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
úhlového	úhlový	k2eAgInSc2d1	úhlový
průměru	průměr	k1gInSc2	průměr
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
===	===	k?	===
</s>
</p>
<p>
<s>
U	u	k7c2	u
hvězdy	hvězda	k1gFnSc2	hvězda
byla	být	k5eAaImAgFnS	být
změřena	změřen	k2eAgFnSc1d1	změřena
paralaxa	paralaxa	k1gFnSc1	paralaxa
772,3	[number]	k4	772,3
±	±	k?	±
2,4	[number]	k4	2,4
obloukových	obloukový	k2eAgFnPc2d1	oblouková
milisekund	milisekunda	k1gFnPc2	milisekunda
družicí	družice	k1gFnSc7	družice
Hipparcos	Hipparcosa	k1gFnPc2	Hipparcosa
<g/>
,	,	kIx,	,
a	a	k8xC	a
ještě	ještě	k9	ještě
přesnější	přesný	k2eAgFnSc1d2	přesnější
hodnota	hodnota	k1gFnSc1	hodnota
768,7	[number]	k4	768,7
±	±	k?	±
0,3	[number]	k4	0,3
obloukových	obloukový	k2eAgFnPc2d1	oblouková
milisekund	milisekunda	k1gFnPc2	milisekunda
byla	být	k5eAaImAgFnS	být
získána	získat	k5eAaPmNgFnS	získat
přístrojem	přístroj	k1gInSc7	přístroj
Fine	Fin	k1gMnSc5	Fin
Guidance	Guidanec	k1gMnPc4	Guidanec
Sensor	Sensor	k1gInSc1	Sensor
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byla	být	k5eAaImAgFnS	být
potvrzena	potvrzen	k2eAgFnSc1d1	potvrzena
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
Proximy	Proxima	k1gFnSc2	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
na	na	k7c4	na
4,2	[number]	k4	4,2
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
270	[number]	k4	270
000	[number]	k4	000
AU	au	k0	au
<g/>
)	)	kIx)	)
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
<g/>
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
je	být	k5eAaImIp3nS	být
32	[number]	k4	32
000	[number]	k4	000
let	léto	k1gNnPc2	léto
hvězdou	hvězda	k1gFnSc7	hvězda
nejbližší	blízký	k2eAgMnPc1d3	nejbližší
Slunci	slunce	k1gNnSc3	slunce
a	a	k8xC	a
ještě	ještě	k9	ještě
dalších	další	k2eAgInPc2d1	další
30	[number]	k4	30
000	[number]	k4	000
let	léto	k1gNnPc2	léto
jí	on	k3xPp3gFnSc3	on
zůstane	zůstat	k5eAaPmIp3nS	zůstat
<g/>
,	,	kIx,	,
než	než	k8xS	než
jí	on	k3xPp3gFnSc3	on
vystřídá	vystřídat	k5eAaPmIp3nS	vystřídat
Ross	Ross	k1gInSc4	Ross
248	[number]	k4	248
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
26	[number]	k4	26
700	[number]	k4	700
let	léto	k1gNnPc2	léto
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
3,11	[number]	k4	3,11
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
nejbližšího	blízký	k2eAgNnSc2d3	nejbližší
přiblížení	přiblížení	k1gNnSc2	přiblížení
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příslušnost	příslušnost	k1gFnSc4	příslušnost
k	k	k7c3	k
systému	systém	k1gInSc3	systém
Alfa	alfa	k1gFnSc1	alfa
Centauri	Centaur	k1gFnSc2	Centaur
==	==	k?	==
</s>
</p>
<p>
<s>
Příslušnost	příslušnost	k1gFnSc1	příslušnost
Proximy	Proxima	k1gFnSc2	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
k	k	k7c3	k
systému	systém	k1gInSc3	systém
alfy	alfa	k1gFnSc2	alfa
Centauri	Centauri	k1gNnSc2	Centauri
byla	být	k5eAaImAgFnS	být
objasněna	objasnit	k5eAaPmNgFnS	objasnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
<g/>
Úhlová	úhlový	k2eAgFnSc1d1	úhlová
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
Proximy	Proxima	k1gFnSc2	Proxima
k	k	k7c3	k
Alfě	alfa	k1gFnSc3	alfa
Centauri	Centaur	k1gFnSc2	Centaur
je	být	k5eAaImIp3nS	být
2,2	[number]	k4	2,2
stupně	stupeň	k1gInSc2	stupeň
(	(	kIx(	(
<g/>
čtyři	čtyři	k4xCgInPc1	čtyři
úhlové	úhlový	k2eAgInPc1d1	úhlový
průměry	průměr	k1gInPc1	průměr
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tímto	tento	k3xDgInSc7	tento
vzdálena	vzdálit	k5eAaPmNgFnS	vzdálit
přibližně	přibližně	k6eAd1	přibližně
15	[number]	k4	15
000	[number]	k4	000
±	±	k?	±
700	[number]	k4	700
AU	au	k0	au
nebo	nebo	k8xC	nebo
0,21	[number]	k4	0,21
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
dvojhvězdy	dvojhvězda	k1gFnSc2	dvojhvězda
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
20	[number]	k4	20
své	svůj	k3xOyFgFnSc2	svůj
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
k	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
tisíci	tisíc	k4xCgInSc6	tisíc
násobku	násobek	k1gInSc6	násobek
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
mezi	mezi	k7c7	mezi
alfou	alfa	k1gFnSc7	alfa
Centauri	Centaur	k1gFnSc2	Centaur
A	A	kA	A
a	a	k8xC	a
alfou	alfa	k1gFnSc7	alfa
Centauri	Centaur	k1gFnSc2	Centaur
B	B	kA	B
nebo	nebo	k8xC	nebo
pěti	pět	k4xCc2	pět
set	set	k1gInSc4	set
násobku	násobek	k1gInSc2	násobek
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
mezi	mezi	k7c7	mezi
Neptunem	Neptun	k1gInSc7	Neptun
a	a	k8xC	a
Sluncem	slunce	k1gNnSc7	slunce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Astrometrická	Astrometrický	k2eAgNnPc1d1	Astrometrický
měření	měření	k1gNnPc1	měření
družice	družice	k1gFnSc2	družice
Hipparcos	Hipparcos	k1gInSc4	Hipparcos
prakticky	prakticky	k6eAd1	prakticky
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
domněnku	domněnka	k1gFnSc4	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
kolem	kolem	k7c2	kolem
dvojhvězdy	dvojhvězda	k1gFnSc2	dvojhvězda
<g/>
,	,	kIx,	,
s	s	k7c7	s
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
dobou	doba	k1gFnSc7	doba
řádově	řádově	k6eAd1	řádově
kolem	kolem	k7c2	kolem
500	[number]	k4	500
000	[number]	k4	000
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
údaje	údaj	k1gInPc1	údaj
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
data	datum	k1gNnPc4	datum
v	v	k7c6	v
řádech	řád	k1gInPc6	řád
sta	sto	k4xCgNnPc4	sto
tisíce	tisíc	k4xCgInPc1	tisíc
až	až	k9	až
k	k	k7c3	k
několika	několik	k4yIc3	několik
miliónům	milión	k4xCgInPc3	milión
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
Proxima	Proxima	k1gFnSc1	Proxima
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xS	jako
alfa	alfa	k1gNnSc1	alfa
Centauri	Centauri	k1gNnSc2	Centauri
C.	C.	kA	C.
Data	datum	k1gNnPc1	datum
udávají	udávat	k5eAaImIp3nP	udávat
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
s	s	k7c7	s
minimální	minimální	k2eAgFnSc7d1	minimální
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
1000	[number]	k4	1000
AU	au	k0	au
a	a	k8xC	a
s	s	k7c7	s
maximální	maximální	k2eAgFnSc7d1	maximální
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
20	[number]	k4	20
000	[number]	k4	000
AU	au	k0	au
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
dvojhvězdy	dvojhvězda	k1gFnSc2	dvojhvězda
jako	jako	k8xC	jako
extrémně	extrémně	k6eAd1	extrémně
excentrickou	excentrický	k2eAgFnSc7d1	excentrická
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
Proxima	Proxima	k1gNnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
poblíž	poblíž	k7c2	poblíž
svého	svůj	k3xOyFgInSc2	svůj
apoastronu	apoastron	k1gInSc2	apoastron
(	(	kIx(	(
<g/>
nejvzdálenějšího	vzdálený	k2eAgInSc2d3	nejvzdálenější
bodu	bod	k1gInSc2	bod
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
od	od	k7c2	od
alfy	alfa	k1gFnSc2	alfa
Centauri	Centaur	k1gFnSc2	Centaur
A	A	kA	A
a	a	k8xC	a
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
nutná	nutný	k2eAgNnPc1d1	nutné
přesnější	přesný	k2eAgNnPc1d2	přesnější
měření	měření	k1gNnPc1	měření
radiální	radiální	k2eAgFnSc2d1	radiální
rychlosti	rychlost	k1gFnSc2	rychlost
hvězdy	hvězda	k1gFnSc2	hvězda
pro	pro	k7c4	pro
potvrzení	potvrzení	k1gNnSc4	potvrzení
uvedených	uvedený	k2eAgInPc2d1	uvedený
údajů	údaj	k1gInPc2	údaj
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
Matthews	Matthewsa	k1gFnPc2	Matthewsa
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
malou	malý	k2eAgFnSc4d1	malá
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
a	a	k8xC	a
podobný	podobný	k2eAgInSc4d1	podobný
vlastní	vlastní	k2eAgInSc4d1	vlastní
pohyb	pohyb	k1gInSc4	pohyb
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
pozorované	pozorovaný	k2eAgNnSc1d1	pozorované
rozmístění	rozmístění	k1gNnSc1	rozmístění
hvězd	hvězda	k1gFnPc2	hvězda
je	být	k5eAaImIp3nS	být
náhodné	náhodný	k2eAgNnSc1d1	náhodné
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
jedna	jeden	k4xCgFnSc1	jeden
ku	k	k7c3	k
milionu	milion	k4xCgInSc2	milion
<g/>
.	.	kIx.	.
<g/>
Určitá	určitý	k2eAgNnPc1d1	určité
měření	měření	k1gNnPc1	měření
radiální	radiální	k2eAgFnSc2d1	radiální
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c4	v
Glieseho	Gliese	k1gMnSc4	Gliese
katalogu	katalog	k1gInSc2	katalog
<g/>
,	,	kIx,	,
neodpovídají	odpovídat	k5eNaImIp3nP	odpovídat
vázanému	vázaný	k2eAgInSc3d1	vázaný
hvězdnému	hvězdný	k2eAgInSc3d1	hvězdný
systému	systém	k1gInSc3	systém
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nelze	lze	k6eNd1	lze
vyloučit	vyloučit	k5eAaPmF	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
náhodné	náhodný	k2eAgNnSc4d1	náhodné
setkání	setkání	k1gNnSc4	setkání
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
domněnky	domněnka	k1gFnPc1	domněnka
jsou	být	k5eAaImIp3nP	být
podporované	podporovaný	k2eAgInPc4d1	podporovaný
simulačními	simulační	k2eAgInPc7d1	simulační
výpočty	výpočet	k1gInPc7	výpočet
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
vazební	vazební	k2eAgFnSc1d1	vazební
energie	energie	k1gFnSc1	energie
systému	systém	k1gInSc2	systém
dává	dávat	k5eAaImIp3nS	dávat
jen	jen	k9	jen
ve	v	k7c4	v
44	[number]	k4	44
%	%	kIx~	%
možností	možnost	k1gFnPc2	možnost
gravitačně	gravitačně	k6eAd1	gravitačně
vázaný	vázaný	k2eAgInSc1d1	vázaný
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
<g/>
Pozorování	pozorování	k1gNnSc1	pozorování
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
dvojhvězdou	dvojhvězda	k1gFnSc7	dvojhvězda
a	a	k8xC	a
devíti	devět	k4xCc7	devět
dalšími	další	k2eAgFnPc7d1	další
hvězdami	hvězda	k1gFnPc7	hvězda
tvoří	tvořit	k5eAaImIp3nS	tvořit
pohybovou	pohybový	k2eAgFnSc4d1	pohybová
skupinu	skupina	k1gFnSc4	skupina
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
by	by	kYmCp3nS	by
Proxima	Proxim	k1gMnSc4	Proxim
neuskutečňovala	uskutečňovat	k5eNaImAgFnS	uskutečňovat
oběh	oběh	k1gInSc4	oběh
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
soustavy	soustava	k1gFnSc2	soustava
vázaným	vázaný	k2eAgInSc7d1	vázaný
pohybem	pohyb	k1gInSc7	pohyb
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gFnSc1	její
dráha	dráha	k1gFnSc1	dráha
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
dvojhvězdou	dvojhvězda	k1gFnSc7	dvojhvězda
hyperbolicky	hyperbolicky	k6eAd1	hyperbolicky
rušená	rušený	k2eAgFnSc1d1	rušená
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Proxima	Proxima	k1gNnSc1	Proxima
neuskuteční	uskutečnit	k5eNaPmIp3nP	uskutečnit
celý	celý	k2eAgInSc4d1	celý
oběh	oběh	k1gInSc4	oběh
okolo	okolo	k7c2	okolo
Alfa	alfa	k1gNnSc2	alfa
Centauri	Centaur	k1gFnSc2	Centaur
A	A	kA	A
a	a	k8xC	a
B.	B.	kA	B.
</s>
</p>
<p>
<s>
==	==	k?	==
Okolí	okolí	k1gNnSc1	okolí
hvězdy	hvězda	k1gFnSc2	hvězda
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
Proximy	Proxima	k1gFnSc2	Proxima
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
dvojhvězda	dvojhvězda	k1gFnSc1	dvojhvězda
alfa	alfa	k1gNnSc2	alfa
Centauri	Centauri	k1gNnSc2	Centauri
A	A	kA	A
a	a	k8xC	a
B	B	kA	B
jako	jako	k8xS	jako
velice	velice	k6eAd1	velice
jasná	jasný	k2eAgFnSc1d1	jasná
hvězda	hvězda	k1gFnSc1	hvězda
o	o	k7c6	o
zdánlivé	zdánlivý	k2eAgFnSc6d1	zdánlivá
velikosti	velikost	k1gFnSc6	velikost
−	−	k?	−
<g/>
m.	m.	k?	m.
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
poloze	poloha	k1gFnSc6	poloha
hvězdy	hvězda	k1gFnSc2	hvězda
A	A	kA	A
a	a	k8xC	a
B	B	kA	B
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
oběžných	oběžný	k2eAgFnPc6d1	oběžná
drahách	draha	k1gFnPc6	draha
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
dvojhvězda	dvojhvězda	k1gFnSc1	dvojhvězda
snadno	snadno	k6eAd1	snadno
rozlišitelná	rozlišitelný	k2eAgFnSc1d1	rozlišitelná
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Alfa	alfa	k1gFnSc1	alfa
Centauri	Centaur	k1gFnSc2	Centaur
A	a	k9	a
má	mít	k5eAaImIp3nS	mít
jasnost	jasnost	k1gFnSc1	jasnost
−	−	k?	−
<g/>
m	m	kA	m
<g/>
,	,	kIx,	,
B	B	kA	B
má	mít	k5eAaImIp3nS	mít
jasnost	jasnost	k1gFnSc1	jasnost
−	−	k?	−
<g/>
m.	m.	k?	m.
Po	po	k7c6	po
centrální	centrální	k2eAgFnSc6d1	centrální
dvojhvězdě	dvojhvězda	k1gFnSc6	dvojhvězda
a	a	k8xC	a
Slunci	slunce	k1gNnSc6	slunce
je	být	k5eAaImIp3nS	být
další	další	k2eAgInSc1d1	další
nejbližší	blízký	k2eAgFnSc7d3	nejbližší
hvězdou	hvězda	k1gFnSc7	hvězda
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
6,6	[number]	k4	6,6
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
Barnardova	Barnardův	k2eAgFnSc1d1	Barnardova
šipka	šipka	k1gFnSc1	šipka
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
má	mít	k5eAaImIp3nS	mít
z	z	k7c2	z
Proximy	Proxima	k1gFnSc2	Proxima
zdánlivou	zdánlivý	k2eAgFnSc4d1	zdánlivá
jasnost	jasnost	k1gFnSc4	jasnost
0,4	[number]	k4	0,4
<g/>
m	m	kA	m
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Kasiopeji	Kasiopej	k1gInSc6	Kasiopej
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
alfy	alfa	k1gFnSc2	alfa
Centauri	Centaur	k1gFnSc2	Centaur
je	být	k5eAaImIp3nS	být
Proxima	Proxima	k1gFnSc1	Proxima
přes	přes	k7c4	přes
svoji	svůj	k3xOyFgFnSc4	svůj
blízkost	blízkost	k1gFnSc4	blízkost
(	(	kIx(	(
<g/>
čtvrt	čtvrt	k1gFnSc4	čtvrt
světelného	světelný	k2eAgInSc2d1	světelný
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
vidět	vidět	k5eAaImF	vidět
jako	jako	k9	jako
hvězda	hvězda	k1gFnSc1	hvězda
o	o	k7c6	o
jasnosti	jasnost	k1gFnSc6	jasnost
4,5	[number]	k4	4,5
<g/>
m.	m.	k?	m.
To	to	k9	to
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
slabou	slabý	k2eAgFnSc7d1	slabá
hvězdou	hvězda	k1gFnSc7	hvězda
Proxima	Proximum	k1gNnSc2	Proximum
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lze	lze	k6eAd1	lze
si	se	k3xPyFc3	se
představit	představit	k5eAaPmF	představit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
dokáže	dokázat	k5eAaPmIp3nS	dokázat
v	v	k7c6	v
periastronu	periastron	k1gInSc6	periastron
odklonit	odklonit	k5eAaPmF	odklonit
některé	některý	k3yIgFnPc4	některý
komety	kometa	k1gFnPc4	kometa
Oortova	Oortův	k2eAgInSc2d1	Oortův
oblaku	oblak	k1gInSc2	oblak
systému	systém	k1gInSc2	systém
dvojhvězdy	dvojhvězda	k1gFnSc2	dvojhvězda
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
případné	případný	k2eAgFnSc2d1	případná
terestrické	terestrický	k2eAgFnSc2d1	terestrická
planety	planeta	k1gFnSc2	planeta
zásobit	zásobit	k5eAaPmF	zásobit
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
Proxima	Proxima	k1gFnSc1	Proxima
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
vázaná	vázaný	k2eAgFnSc1d1	vázaná
systémem	systém	k1gInSc7	systém
alfa	alfa	k1gNnSc2	alfa
Centauri	Centauri	k1gNnSc2	Centauri
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
stejný	stejný	k2eAgInSc4d1	stejný
podíl	podíl	k1gInSc4	podíl
chemických	chemický	k2eAgInPc2d1	chemický
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Dodatečně	dodatečně	k6eAd1	dodatečně
pak	pak	k6eAd1	pak
mohla	moct	k5eAaImAgFnS	moct
gravitace	gravitace	k1gFnSc1	gravitace
alfy	alfa	k1gFnSc2	alfa
Centauri	Centaur	k1gFnSc2	Centaur
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
protoplanetární	protoplanetární	k2eAgInSc4d1	protoplanetární
disk	disk	k1gInSc4	disk
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
do	do	k7c2	do
systému	systém	k1gInSc2	systém
dostal	dostat	k5eAaPmAgInS	dostat
vodní	vodní	k2eAgInSc1d1	vodní
led	led	k1gInSc1	led
<g/>
.	.	kIx.	.
</s>
<s>
Hypotetická	hypotetický	k2eAgFnSc1d1	hypotetická
planeta	planeta	k1gFnSc1	planeta
pozemského	pozemský	k2eAgInSc2d1	pozemský
typu	typ	k1gInSc2	typ
by	by	kYmCp3nS	by
takto	takto	k6eAd1	takto
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
zásobena	zásobit	k5eAaPmNgFnS	zásobit
materiálem	materiál	k1gInSc7	materiál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Planetární	planetární	k2eAgInSc1d1	planetární
systém	systém	k1gInSc1	systém
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInPc1	první
údaje	údaj	k1gInPc1	údaj
o	o	k7c6	o
exoplanetě	exoplaneta	k1gFnSc6	exoplaneta
nalezl	nalézt	k5eAaBmAgInS	nalézt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
Mikko	Mikko	k1gNnSc4	Mikko
Tuomi	Tuo	k1gFnPc7	Tuo
z	z	k7c2	z
University	universita	k1gFnSc2	universita
of	of	k?	of
Hertfordshire	Hertfordshir	k1gInSc5	Hertfordshir
z	z	k7c2	z
archivních	archivní	k2eAgNnPc6d1	archivní
datech	datum	k1gNnPc6	datum
pozorování	pozorování	k1gNnSc2	pozorování
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	k9	aby
Evropská	evropský	k2eAgFnSc1d1	Evropská
jižní	jižní	k2eAgFnSc1d1	jižní
observatoř	observatoř	k1gFnSc1	observatoř
mohla	moct	k5eAaImAgFnS	moct
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
případný	případný	k2eAgInSc4d1	případný
objev	objev	k1gInSc4	objev
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
zahájila	zahájit	k5eAaPmAgFnS	zahájit
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2016	[number]	k4	2016
projekt	projekt	k1gInSc1	projekt
Pale	pal	k1gInSc5	pal
Red	Red	k1gFnPc1	Red
Dot	Dot	k1gMnPc1	Dot
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2016	[number]	k4	2016
tým	tým	k1gInSc1	tým
31	[number]	k4	31
vědců	vědec	k1gMnPc2	vědec
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
vedený	vedený	k2eAgInSc1d1	vedený
Guillemem	Guillem	k1gInSc7	Guillem
Angladou-Escudou	Angladou-Escudý	k2eAgFnSc4d1	Angladou-Escudý
z	z	k7c2	z
Queen	Quena	k1gFnPc2	Quena
Mary	Mary	k1gFnSc2	Mary
University	universita	k1gFnSc2	universita
of	of	k?	of
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
existenci	existence	k1gFnSc4	existence
Proximy	Proxima	k1gFnSc2	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
b	b	k?	b
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
recenzovaného	recenzovaný	k2eAgInSc2d1	recenzovaný
článku	článek	k1gInSc2	článek
publikovaného	publikovaný	k2eAgInSc2d1	publikovaný
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Nature	Natur	k1gMnSc5	Natur
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc1	měření
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
pomocí	pomocí	k7c2	pomocí
dvou	dva	k4xCgFnPc2	dva
spektrografů	spektrograf	k1gInPc2	spektrograf
<g/>
,	,	kIx,	,
HARPS	HARPS	kA	HARPS
s	s	k7c7	s
dalekohledem	dalekohled	k1gInSc7	dalekohled
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
3,6	[number]	k4	3,6
metru	metr	k1gInSc2	metr
v	v	k7c6	v
observatoři	observatoř	k1gFnSc6	observatoř
La	la	k1gNnSc2	la
Silla	Sillo	k1gNnSc2	Sillo
a	a	k8xC	a
UVES	UVES	kA	UVES
připojenému	připojený	k2eAgInSc3d1	připojený
k	k	k7c3	k
dalekohledu	dalekohled	k1gInSc3	dalekohled
VLT	VLT	kA	VLT
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
8	[number]	k4	8
metrů	metr	k1gInPc2	metr
v	v	k7c6	v
Observatoři	observatoř	k1gFnSc6	observatoř
Paranal	Paranal	k1gFnSc1	Paranal
<g/>
.	.	kIx.	.
<g/>
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
b	b	k?	b
obíhá	obíhat	k5eAaImIp3nS	obíhat
hvězdu	hvězda	k1gFnSc4	hvězda
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
zhruba	zhruba	k6eAd1	zhruba
0,05	[number]	k4	0,05
AU	au	k0	au
(	(	kIx(	(
<g/>
7	[number]	k4	7
500	[number]	k4	500
000	[number]	k4	000
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dobu	doba	k1gFnSc4	doba
přibližně	přibližně	k6eAd1	přibližně
11,2	[number]	k4	11,2
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
odhadovaná	odhadovaný	k2eAgFnSc1d1	odhadovaná
hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
nejméně	málo	k6eAd3	málo
1,3	[number]	k4	1,3
<g/>
násobkem	násobek	k1gInSc7	násobek
hmotnosti	hmotnost	k1gFnPc1	hmotnost
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rovnovážná	rovnovážný	k2eAgFnSc1d1	rovnovážná
teplota	teplota	k1gFnSc1	teplota
Proxima	Proxima	k1gFnSc1	Proxima
b	b	k?	b
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
povrchu	povrch	k1gInSc6	povrch
mohla	moct	k5eAaImAgFnS	moct
existovat	existovat	k5eAaImF	existovat
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
kapalném	kapalný	k2eAgNnSc6d1	kapalné
skupenství	skupenství	k1gNnSc6	skupenství
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
obíhá	obíhat	k5eAaImIp3nS	obíhat
v	v	k7c6	v
obyvatelné	obyvatelný	k2eAgFnSc6d1	obyvatelná
zóně	zóna	k1gFnSc6	zóna
Proximy	Proxima	k1gFnSc2	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
detekován	detekovat	k5eAaImNgInS	detekovat
i	i	k9	i
druhý	druhý	k4xOgInSc1	druhý
signál	signál	k1gInSc1	signál
v	v	k7c6	v
periodě	perioda	k1gFnSc6	perioda
60	[number]	k4	60
až	až	k9	až
500	[number]	k4	500
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
povaha	povaha	k1gFnSc1	povaha
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
nejasná	jasný	k2eNgFnSc1d1	nejasná
kvůli	kvůli	k7c3	kvůli
aktivitě	aktivita	k1gFnSc3	aktivita
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
<g/>
Před	před	k7c7	před
objevem	objev	k1gInSc7	objev
planety	planeta	k1gFnSc2	planeta
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
měření	měření	k1gNnSc2	měření
radiální	radiální	k2eAgFnSc2d1	radiální
rychlosti	rychlost	k1gFnSc2	rychlost
hvězdy	hvězda	k1gFnSc2	hvězda
omezilo	omezit	k5eAaPmAgNnS	omezit
maximální	maximální	k2eAgFnSc4d1	maximální
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
mít	mít	k5eAaImF	mít
planeta	planeta	k1gFnSc1	planeta
u	u	k7c2	u
Proximy	Proxima	k1gFnSc2	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
.	.	kIx.	.
</s>
<s>
Aktivita	aktivita	k1gFnSc1	aktivita
hvězdy	hvězda	k1gFnSc2	hvězda
přidává	přidávat	k5eAaImIp3nS	přidávat
šum	šum	k1gInSc1	šum
do	do	k7c2	do
měření	měření	k1gNnSc2	měření
radiální	radiální	k2eAgFnSc2d1	radiální
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
komplikuje	komplikovat	k5eAaBmIp3nS	komplikovat
detekci	detekce	k1gFnSc4	detekce
společníka	společník	k1gMnSc2	společník
pomocí	pomocí	k7c2	pomocí
této	tento	k3xDgFnSc2	tento
metody	metoda	k1gFnSc2	metoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
průzkumem	průzkum	k1gInSc7	průzkum
Proximy	Proxima	k1gFnPc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
pomocí	pomoc	k1gFnPc2	pomoc
Faint	Faint	k1gMnSc1	Faint
Object	Object	k1gMnSc1	Object
Spectrograph	Spectrograph	k1gMnSc1	Spectrograph
na	na	k7c6	na
Hubbleově	Hubbleův	k2eAgInSc6d1	Hubbleův
vesmírném	vesmírný	k2eAgInSc6d1	vesmírný
dalekohledu	dalekohled	k1gInSc6	dalekohled
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
důkazy	důkaz	k1gInPc1	důkaz
o	o	k7c6	o
družici	družice	k1gFnSc6	družice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obíhá	obíhat	k5eAaImIp3nS	obíhat
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
přibližně	přibližně	k6eAd1	přibližně
0,5	[number]	k4	0,5
AU	au	k0	au
<g/>
.	.	kIx.	.
</s>
<s>
Následným	následný	k2eAgNnSc7d1	následné
vyhledáváním	vyhledávání	k1gNnSc7	vyhledávání
pomocí	pomocí	k7c2	pomocí
Wide	Wid	k1gFnSc2	Wid
Field	Field	k1gInSc4	Field
Planetary	Planetara	k1gFnSc2	Planetara
Camera	Camera	k1gFnSc1	Camera
2	[number]	k4	2
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
nalézt	nalézt	k5eAaBmF	nalézt
žádný	žádný	k3yNgInSc4	žádný
objekt	objekt	k1gInSc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Astrometrická	Astrometrický	k2eAgNnPc1d1	Astrometrický
měření	měření	k1gNnPc1	měření
na	na	k7c4	na
Cerro	Cerro	k1gNnSc4	Cerro
Tololo	Tolola	k1gFnSc5	Tolola
Inter-American	Inter-American	k1gMnSc1	Inter-American
Observatory	Observator	k1gInPc1	Observator
vyloučily	vyloučit	k5eAaPmAgInP	vyloučit
planetu	planeta	k1gFnSc4	planeta
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
Jupitera	Jupiter	k1gMnSc2	Jupiter
s	s	k7c7	s
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
dobou	doba	k1gFnSc7	doba
2	[number]	k4	2
až	až	k9	až
12	[number]	k4	12
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
společně	společně	k6eAd1	společně
s	s	k7c7	s
Alfou	alfa	k1gFnSc7	alfa
Centauri	Centaur	k1gFnSc2	Centaur
A	A	kA	A
a	a	k8xC	a
B	B	kA	B
patřila	patřit	k5eAaImAgFnS	patřit
prvním	první	k4xOgMnSc6	první
k	k	k7c3	k
cílům	cíl	k1gInPc3	cíl
mise	mise	k1gFnSc2	mise
Space	Space	k1gFnSc2	Space
Interferometry	interferometr	k1gInPc1	interferometr
Mission	Mission	k1gInSc4	Mission
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mohla	moct	k5eAaImAgFnS	moct
najít	najít	k5eAaPmF	najít
planety	planeta	k1gFnPc4	planeta
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
menší	malý	k2eAgFnSc2d2	menší
než	než	k8xS	než
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
projekt	projekt	k1gInSc1	projekt
však	však	k9	však
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Planetární	planetární	k2eAgInSc4d1	planetární
disk	disk	k1gInSc4	disk
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
detekoval	detekovat	k5eAaImAgInS	detekovat
radioteleskop	radioteleskop	k1gInSc1	radioteleskop
ALMA	alma	k1gFnSc1	alma
Evropské	evropský	k2eAgFnSc2d1	Evropská
jižní	jižní	k2eAgFnSc2d1	jižní
observatoře	observatoř	k1gFnSc2	observatoř
pracující	pracující	k2eAgFnSc2d1	pracující
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
prach	prach	k1gInSc1	prach
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Proximy	Proxima	k1gFnSc2	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
.	.	kIx.	.
</s>
<s>
Pozorovaný	pozorovaný	k2eAgInSc1d1	pozorovaný
prach	prach	k1gInSc1	prach
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
pásu	pás	k1gInSc6	pás
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
několika	několik	k4yIc2	několik
set	set	k1gInSc4	set
milionů	milion	k4xCgInPc2	milion
kilometrů	kilometr	k1gInPc2	kilometr
(	(	kIx(	(
<g/>
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
by	by	kYmCp3nS	by
sahal	sahat	k5eAaImAgInS	sahat
přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
Země	zem	k1gFnSc2	zem
až	až	k9	až
téměř	téměř	k6eAd1	téměř
k	k	k7c3	k
Jupiteru	Jupiter	k1gInSc3	Jupiter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
odhadovaná	odhadovaný	k2eAgFnSc1d1	odhadovaná
hmotnost	hmotnost	k1gFnSc1	hmotnost
představuje	představovat	k5eAaImIp3nS	představovat
asi	asi	k9	asi
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
100	[number]	k4	100
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
částic	částice	k1gFnPc2	částice
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
kolem	kolem	k7c2	kolem
-230	-230	k4	-230
°	°	k?	°
<g/>
C.	C.	kA	C.
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
Proximy	Proxima	k1gFnSc2	Proxima
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ještě	ještě	k6eAd1	ještě
jeden	jeden	k4xCgMnSc1	jeden
o	o	k7c4	o
něco	něco	k3yInSc4	něco
chladnější	chladný	k2eAgInSc1d2	chladnější
vnější	vnější	k2eAgInSc4d1	vnější
prachový	prachový	k2eAgInSc4d1	prachový
pás	pás	k1gInSc4	pás
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgFnPc1	tento
oblasti	oblast	k1gFnPc1	oblast
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
známkou	známka	k1gFnSc7	známka
přítomnosti	přítomnost	k1gFnSc2	přítomnost
komplexního	komplexní	k2eAgInSc2d1	komplexní
planetárního	planetární	k2eAgInSc2d1	planetární
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jde	jít	k5eAaImIp3nS	jít
zřejmě	zřejmě	k6eAd1	zřejmě
o	o	k7c4	o
zbytky	zbytek	k1gInPc4	zbytek
látky	látka	k1gFnPc1	látka
nespotřebované	spotřebovaný	k2eNgFnPc1d1	nespotřebovaná
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
a	a	k8xC	a
vývoji	vývoj	k1gInSc6	vývoj
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
pásy	pás	k1gInPc1	pás
se	se	k3xPyFc4	se
však	však	k9	však
nacházejí	nacházet	k5eAaImIp3nP	nacházet
mnohem	mnohem	k6eAd1	mnohem
dále	daleko	k6eAd2	daleko
od	od	k7c2	od
Proximy	Proxima	k1gFnSc2	Proxima
<g/>
,	,	kIx,	,
než	než	k8xS	než
planeta	planeta	k1gFnSc1	planeta
Proxima	Proxima	k1gFnSc1	Proxima
b	b	k?	b
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ji	on	k3xPp3gFnSc4	on
obíhá	obíhat	k5eAaImIp3nS	obíhat
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
jen	jen	k9	jen
několika	několik	k4yIc2	několik
milionů	milion	k4xCgInPc2	milion
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Možnost	možnost	k1gFnSc4	možnost
života	život	k1gInSc2	život
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
modelů	model	k1gInPc2	model
vychází	vycházet	k5eAaImIp3nS	vycházet
<g/>
,	,	kIx,	,
že	že	k8xS	že
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yQgFnSc6	který
by	by	kYmCp3nP	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
tekutém	tekutý	k2eAgNnSc6d1	tekuté
skupenství	skupenství	k1gNnSc6	skupenství
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
od	od	k7c2	od
Proximy	Proxima	k1gFnSc2	Proxima
nesměla	smět	k5eNaImAgFnS	smět
být	být	k5eAaImF	být
dále	daleko	k6eAd2	daleko
než	než	k8xS	než
0,032	[number]	k4	0,032
AU	au	k0	au
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nP	kdyby
planeta	planeta	k1gFnSc1	planeta
obíhala	obíhat	k5eAaImAgFnS	obíhat
v	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
blízkosti	blízkost	k1gFnSc6	blízkost
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
by	by	kYmCp3nS	by
mít	mít	k5eAaImF	mít
vlivem	vlivem	k7c2	vlivem
slapových	slapový	k2eAgFnPc2d1	slapová
sil	síla	k1gFnPc2	síla
vázanou	vázaný	k2eAgFnSc4d1	vázaná
rotaci	rotace	k1gFnSc4	rotace
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
strana	strana	k1gFnSc1	strana
planety	planeta	k1gFnSc2	planeta
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
stále	stále	k6eAd1	stále
natočená	natočený	k2eAgFnSc1d1	natočená
k	k	k7c3	k
hvězdě	hvězda	k1gFnSc3	hvězda
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
slunce	slunce	k1gNnSc1	slunce
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
stále	stále	k6eAd1	stále
stejném	stejný	k2eAgNnSc6d1	stejné
místě	místo	k1gNnSc6	místo
oblohy	obloha	k1gFnSc2	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
by	by	kYmCp3nS	by
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
hvězdy	hvězda	k1gFnSc2	hvězda
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
sotva	sotva	k6eAd1	sotva
6,3	[number]	k4	6,3
pozemského	pozemský	k2eAgInSc2d1	pozemský
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tak	tak	k6eAd1	tak
pomalá	pomalý	k2eAgFnSc1d1	pomalá
rotace	rotace	k1gFnSc1	rotace
by	by	kYmCp3nS	by
stačila	stačit	k5eAaBmAgFnS	stačit
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
a	a	k8xC	a
jádro	jádro	k1gNnSc1	jádro
planety	planeta	k1gFnSc2	planeta
by	by	kYmCp3nS	by
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
roztavené	roztavený	k2eAgNnSc1d1	roztavené
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
by	by	kYmCp3nP	by
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
planety	planeta	k1gFnSc2	planeta
bylo	být	k5eAaImAgNnS	být
slabé	slabý	k2eAgNnSc1d1	slabé
<g/>
,	,	kIx,	,
nestačilo	stačit	k5eNaBmAgNnS	stačit
by	by	kYmCp3nS	by
ochránit	ochránit	k5eAaPmF	ochránit
její	její	k3xOp3gFnSc4	její
atmosféru	atmosféra	k1gFnSc4	atmosféra
před	před	k7c7	před
výbuchy	výbuch	k1gInPc7	výbuch
korony	korona	k1gFnSc2	korona
a	a	k8xC	a
planeta	planeta	k1gFnSc1	planeta
by	by	kYmCp3nS	by
ztrácela	ztrácet	k5eAaImAgFnS	ztrácet
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
.	.	kIx.	.
<g/>
Protože	protože	k8xS	protože
na	na	k7c6	na
Proximě	Proxima	k1gFnSc6	Proxima
neustále	neustále	k6eAd1	neustále
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
protuberancím	protuberance	k1gFnPc3	protuberance
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
takové	takový	k3xDgFnSc6	takový
planetě	planeta	k1gFnSc6	planeta
život	život	k1gInSc1	život
sotva	sotva	k6eAd1	sotva
možný	možný	k2eAgInSc1d1	možný
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
minut	minuta	k1gFnPc2	minuta
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
svítivost	svítivost	k1gFnSc1	svítivost
hvězdy	hvězda	k1gFnSc2	hvězda
zdvojnásobit	zdvojnásobit	k5eAaPmF	zdvojnásobit
až	až	k8xS	až
trojnásobit	trojnásobit	k5eAaImF	trojnásobit
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
protuberance	protuberance	k1gFnPc1	protuberance
by	by	kYmCp3nP	by
zničily	zničit	k5eAaPmAgFnP	zničit
atmosféru	atmosféra	k1gFnSc4	atmosféra
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
obyvatelné	obyvatelný	k2eAgFnSc6d1	obyvatelná
zóně	zóna	k1gFnSc6	zóna
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mezihvězdná	mezihvězdný	k2eAgFnSc1d1	mezihvězdná
cesta	cesta	k1gFnSc1	cesta
==	==	k?	==
</s>
</p>
<p>
<s>
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
malé	malý	k2eAgFnSc3d1	malá
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
navrhována	navrhován	k2eAgFnSc1d1	navrhována
jako	jako	k8xS	jako
smysluplný	smysluplný	k2eAgInSc1d1	smysluplný
cíl	cíl	k1gInSc1	cíl
prvního	první	k4xOgInSc2	první
mezihvězdného	mezihvězdný	k2eAgInSc2d1	mezihvězdný
letu	let	k1gInSc2	let
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
eruptivní	eruptivní	k2eAgFnSc1d1	eruptivní
proměnná	proměnný	k2eAgFnSc1d1	proměnná
hvězda	hvězda	k1gFnSc1	hvězda
nepředstavuje	představovat	k5eNaImIp3nS	představovat
lehký	lehký	k2eAgInSc4d1	lehký
cíl	cíl	k1gInSc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dnešních	dnešní	k2eAgFnPc6d1	dnešní
dosažitelných	dosažitelný	k2eAgFnPc6d1	dosažitelná
rychlostech	rychlost	k1gFnPc6	rychlost
by	by	kYmCp3nS	by
mezihvězdná	mezihvězdný	k2eAgFnSc1d1	mezihvězdná
sonda	sonda	k1gFnSc1	sonda
letěla	letět	k5eAaImAgFnS	letět
4	[number]	k4	4
světelné	světelný	k2eAgInPc1d1	světelný
roky	rok	k1gInPc1	rok
32	[number]	k4	32
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
projektem	projekt	k1gInSc7	projekt
Longshot	Longshot	k1gInSc1	Longshot
existuje	existovat	k5eAaImIp3nS	existovat
koncept	koncept	k1gInSc4	koncept
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
Proximy	Proxima	k1gFnPc4	Proxima
a	a	k8xC	a
sousední	sousední	k2eAgFnSc4d1	sousední
soustavu	soustava	k1gFnSc4	soustava
alfy	alfa	k1gFnSc2	alfa
Centauri	Centaur	k1gFnSc2	Centaur
A	A	kA	A
a	a	k8xC	a
B	B	kA	B
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
během	během	k7c2	během
100	[number]	k4	100
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Objev	objevit	k5eAaPmRp2nS	objevit
hvězdy	hvězda	k1gFnSc2	hvězda
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
byla	být	k5eAaImAgFnS	být
alfa	alfa	k1gFnSc1	alfa
Centauri	Centaur	k1gFnSc2	Centaur
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
hvězdu	hvězda	k1gFnSc4	hvězda
nejbližší	blízký	k2eAgFnSc6d3	nejbližší
Sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
Robert	Robert	k1gMnSc1	Robert
Innes	Innes	k1gMnSc1	Innes
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ředitel	ředitel	k1gMnSc1	ředitel
hvězdárny	hvězdárna	k1gFnSc2	hvězdárna
v	v	k7c6	v
Johannesburgu	Johannesburg	k1gInSc6	Johannesburg
<g/>
,	,	kIx,	,
porovnáním	porovnání	k1gNnSc7	porovnání
fotografických	fotografický	k2eAgFnPc2d1	fotografická
desek	deska	k1gFnPc2	deska
objevil	objevit	k5eAaPmAgInS	objevit
maličkou	maličký	k2eAgFnSc4d1	maličká
hvězdičku	hvězdička	k1gFnSc4	hvězdička
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
alfy	alfa	k1gFnSc2	alfa
Centauri	Centaur	k1gFnSc2	Centaur
a	a	k8xC	a
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hvězdy	hvězda	k1gFnPc1	hvězda
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
stejný	stejný	k2eAgInSc4d1	stejný
pohyb	pohyb	k1gInSc4	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
změřil	změřit	k5eAaPmAgMnS	změřit
holandský	holandský	k2eAgMnSc1d1	holandský
astronom	astronom	k1gMnSc1	astronom
J.	J.	kA	J.
Voû	Voû	k1gFnSc1	Voû
na	na	k7c4	na
Royal	Royal	k1gInSc4	Royal
Observatory	Observator	k1gInPc4	Observator
na	na	k7c6	na
Mysu	mys	k1gInSc6	mys
Dobré	dobrý	k2eAgFnSc2d1	dobrá
naděje	naděje	k1gFnSc2	naděje
trigonometrickou	trigonometrický	k2eAgFnSc4d1	trigonometrická
paralaxu	paralaxa	k1gFnSc4	paralaxa
a	a	k8xC	a
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hvězda	hvězda	k1gFnSc1	hvězda
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
jako	jako	k9	jako
alfa	alfa	k1gNnSc1	alfa
Centauri	Centaur	k1gFnSc2	Centaur
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
Proxima	Proxima	k1gFnSc1	Proxima
stala	stát	k5eAaPmAgFnS	stát
tehdy	tehdy	k6eAd1	tehdy
hvězdou	hvězda	k1gFnSc7	hvězda
s	s	k7c7	s
nejmenší	malý	k2eAgFnSc7d3	nejmenší
známou	známý	k2eAgFnSc7d1	známá
svítivostí	svítivost	k1gFnSc7	svítivost
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
vešlo	vejít	k5eAaPmAgNnS	vejít
ve	v	k7c4	v
známost	známost	k1gFnSc4	známost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Proxima	Proxima	k1gFnSc1	Proxima
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
o	o	k7c4	o
trošku	troška	k1gFnSc4	troška
blíže	blíž	k1gFnSc2	blíž
<g/>
,	,	kIx,	,
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
Innes	Innes	k1gInSc1	Innes
jméno	jméno	k1gNnSc1	jméno
hvězdy	hvězda	k1gFnSc2	hvězda
Proxima	Proximum	k1gNnSc2	Proximum
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
Harlow	Harlow	k1gFnSc4	Harlow
Shapley	Shaplea	k1gFnSc2	Shaplea
publikoval	publikovat	k5eAaBmAgMnS	publikovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
je	být	k5eAaImIp3nS	být
eruptivní	eruptivní	k2eAgFnSc7d1	eruptivní
proměnnou	proměnný	k2eAgFnSc7d1	proměnná
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Porovnání	porovnání	k1gNnSc1	porovnání
fotografických	fotografický	k2eAgFnPc2d1	fotografická
desek	deska	k1gFnPc2	deska
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
8	[number]	k4	8
%	%	kIx~	%
pozorování	pozorování	k1gNnPc2	pozorování
je	být	k5eAaImIp3nS	být
hvězda	hvězda	k1gFnSc1	hvězda
jasnější	jasný	k2eAgFnSc1d2	jasnější
než	než	k8xS	než
obvykle	obvykle	k6eAd1	obvykle
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
stala	stát	k5eAaPmAgFnS	stát
nejaktivnější	aktivní	k2eAgFnSc4d3	nejaktivnější
eruptivní	eruptivní	k2eAgFnSc4d1	eruptivní
proměnnou	proměnná	k1gFnSc4	proměnná
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Proxima	Proxima	k1gNnSc4	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
v	v	k7c6	v
science	scienka	k1gFnSc6	scienka
fiction	fiction	k1gInSc1	fiction
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
"	"	kIx"	"
<g/>
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
"	"	kIx"	"
od	od	k7c2	od
Murraye	Murray	k1gInSc2	Murray
Leinstera	Leinster	k1gMnSc2	Leinster
publikované	publikovaný	k2eAgFnSc2d1	publikovaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
se	se	k3xPyFc4	se
k	k	k7c3	k
Proximě	Proxima	k1gFnSc3	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
blíží	blížit	k5eAaImIp3nS	blížit
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
loď	loď	k1gFnSc1	loď
Adastra	Adastra	k1gFnSc1	Adastra
<g/>
.	.	kIx.	.
</s>
<s>
Posádka	posádka	k1gFnSc1	posádka
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
objeví	objevit	k5eAaPmIp3nS	objevit
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kolem	kolem	k7c2	kolem
Proximy	Proxima	k1gFnSc2	Proxima
krouží	kroužit	k5eAaImIp3nP	kroužit
planety	planeta	k1gFnPc1	planeta
<g/>
,	,	kIx,	,
také	také	k9	také
velký	velký	k2eAgInSc1d1	velký
rozžhavený	rozžhavený	k2eAgInSc1d1	rozžhavený
prstenec	prstenec	k1gInSc1	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
Osídlení	osídlení	k1gNnSc1	osídlení
systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
zpomalováno	zpomalovat	k5eAaImNgNnS	zpomalovat
kvůli	kvůli	k7c3	kvůli
inteligentním	inteligentní	k2eAgFnPc3d1	inteligentní
rostlinám	rostlina	k1gFnPc3	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
vyšel	vyjít	k5eAaPmAgInS	vyjít
scifi	scifi	k1gFnSc2	scifi
román	román	k1gInSc1	román
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Babuly	babula	k1gFnSc2	babula
"	"	kIx"	"
<g/>
Planeta	planeta	k1gFnSc1	planeta
tří	tři	k4xCgInPc2	tři
sluncí	slunce	k1gNnPc2	slunce
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
soustavy	soustava	k1gFnSc2	soustava
a	a	k8xC	a
setkání	setkání	k1gNnSc2	setkání
s	s	k7c7	s
mimozemšťany	mimozemšťan	k1gMnPc7	mimozemšťan
na	na	k7c6	na
tamní	tamní	k2eAgFnSc6d1	tamní
planetě	planeta	k1gFnSc6	planeta
Kvarta	kvarta	k1gFnSc1	kvarta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
doslovu	doslov	k1gInSc6	doslov
jsou	být	k5eAaImIp3nP	být
podrobné	podrobný	k2eAgInPc1d1	podrobný
poznatky	poznatek	k1gInPc1	poznatek
sovětských	sovětský	k2eAgMnPc2d1	sovětský
vědců	vědec	k1gMnPc2	vědec
k	k	k7c3	k
reálnosti	reálnost	k1gFnSc3	reálnost
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
<g/>
Jak	jak	k8xS	jak
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Roberta	Robert	k1gMnSc2	Robert
A.	A.	kA	A.
Heinleina	Heinlein	k1gMnSc2	Heinlein
"	"	kIx"	"
<g/>
Orphans	Orphans	k1gInSc1	Orphans
of	of	k?	of
the	the	k?	the
Sky	Sky	k1gFnSc2	Sky
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Sirotci	Sirotek	k1gMnPc1	Sirotek
oblohy	obloha	k1gFnSc2	obloha
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Harryho	Harry	k1gMnSc2	Harry
Harrisona	Harrison	k1gMnSc2	Harrison
"	"	kIx"	"
<g/>
Captive	Captiv	k1gInSc5	Captiv
Universe	Universe	k1gFnPc4	Universe
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Zajatý	zajatý	k2eAgInSc4d1	zajatý
vesmír	vesmír	k1gInSc4	vesmír
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vypraví	vypravit	k5eAaPmIp3nS	vypravit
generační	generační	k2eAgFnSc2d1	generační
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodě	loď	k1gFnSc2	loď
k	k	k7c3	k
Proximě	Proxima	k1gFnSc3	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
.	.	kIx.	.
<g/>
Také	také	k9	také
povídka	povídka	k1gFnSc1	povídka
"	"	kIx"	"
<g/>
Fotonová	fotonový	k2eAgFnSc1d1	fotonová
kosmická	kosmický	k2eAgFnSc1d1	kosmická
loď	loď	k1gFnSc1	loď
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
bulharského	bulharský	k2eAgMnSc2d1	bulharský
autora	autor	k1gMnSc2	autor
<g/>
[	[	kIx(	[
<g/>
čí	čí	k3xOyRgNnSc4	čí
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
popisuje	popisovat	k5eAaImIp3nS	popisovat
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
Proximě	Proxima	k1gFnSc3	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polský	polský	k2eAgMnSc1d1	polský
autor	autor	k1gMnSc1	autor
Stanislaw	Stanislaw	k1gMnSc1	Stanislaw
Lem	lem	k1gInSc4	lem
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
svůj	svůj	k3xOyFgInSc4	svůj
velkolepý	velkolepý	k2eAgInSc4d1	velkolepý
román	román	k1gInSc4	román
"	"	kIx"	"
<g/>
K	k	k7c3	k
mrakům	mrak	k1gInPc3	mrak
Magellanovým	Magellanův	k2eAgInPc3d1	Magellanův
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
popisuje	popisovat	k5eAaImIp3nS	popisovat
i	i	k9	i
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
Proximě	Proxima	k1gFnSc3	Proxima
a	a	k8xC	a
k	k	k7c3	k
Alfě	alfa	k1gFnSc3	alfa
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
.	.	kIx.	.
</s>
<s>
Kosmická	kosmický	k2eAgFnSc1d1	kosmická
loď	loď	k1gFnSc1	loď
Gea	Gea	k1gFnSc2	Gea
v	v	k7c6	v
románu	román	k1gInSc6	román
letí	letět	k5eAaImIp3nS	letět
maximální	maximální	k2eAgFnSc7d1	maximální
rychlostí	rychlost	k1gFnSc7	rychlost
180	[number]	k4	180
000	[number]	k4	000
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
v	v	k7c6	v
době	doba	k1gFnSc6	doba
osmi	osm	k4xCc2	osm
let	léto	k1gNnPc2	léto
k	k	k7c3	k
sousednímu	sousední	k2eAgInSc3d1	sousední
hvězdnému	hvězdný	k2eAgInSc3d1	hvězdný
systému	systém	k1gInSc3	systém
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
důležitých	důležitý	k2eAgInPc6d1	důležitý
problémech	problém	k1gInPc6	problém
mezihvězdných	mezihvězdný	k2eAgFnPc2d1	mezihvězdná
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
astronomů	astronom	k1gMnPc2	astronom
je	být	k5eAaImIp3nS	být
ještě	ještě	k6eAd1	ještě
bližší	blízký	k2eAgFnSc7d2	bližší
hvězdou	hvězda	k1gFnSc7	hvězda
temný	temný	k2eAgMnSc1d1	temný
červený	červený	k2eAgMnSc1d1	červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
Nemesis	Nemesis	k1gFnSc2	Nemesis
<g/>
,	,	kIx,	,
obíhající	obíhající	k2eAgMnSc1d1	obíhající
přímo	přímo	k6eAd1	přímo
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
způsobující	způsobující	k2eAgNnPc1d1	způsobující
pravidelná	pravidelný	k2eAgNnPc1d1	pravidelné
vymírání	vymírání	k1gNnPc1	vymírání
s	s	k7c7	s
periodou	perioda	k1gFnSc7	perioda
asi	asi	k9	asi
26	[number]	k4	26
-	-	kIx~	-
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
gravitačně	gravitačně	k6eAd1	gravitačně
způsobeným	způsobený	k2eAgNnSc7d1	způsobené
vyvrhováním	vyvrhování	k1gNnSc7	vyvrhování
asteroidů	asteroid	k1gInPc2	asteroid
a	a	k8xC	a
komet	kometa	k1gFnPc2	kometa
z	z	k7c2	z
Oortova	Oortův	k2eAgInSc2d1	Oortův
oblaku	oblak	k1gInSc2	oblak
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hypotéza	hypotéza	k1gFnSc1	hypotéza
však	však	k9	však
již	již	k6eAd1	již
byla	být	k5eAaImAgFnS	být
prakticky	prakticky	k6eAd1	prakticky
vyvrácena	vyvrátit	k5eAaPmNgFnS	vyvrátit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Proxima	Proximum	k1gNnSc2	Proximum
Centauri	Centaur	k1gFnSc2	Centaur
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
<g/>
Infobox	Infobox	k1gInSc1	Infobox
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
<g/>
KALMANČOK	KALMANČOK	kA	KALMANČOK
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
;	;	kIx,	;
PITTICH	PITTICH	kA	PITTICH
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
<g/>
.	.	kIx.	.
</s>
<s>
Obloha	obloha	k1gFnSc1	obloha
na	na	k7c6	na
dlani	dlaň	k1gFnSc6	dlaň
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Obzor	obzor	k1gInSc1	obzor
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
205	[number]	k4	205
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
<g/>
RÜKL	RÜKL	kA	RÜKL
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Obrazy	obraz	k1gInPc1	obraz
z	z	k7c2	z
hlubin	hlubina	k1gFnPc2	hlubina
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Atria	atrium	k1gNnPc4	atrium
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
196	[number]	k4	196
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
<g/>
KLECZEK	KLECZEK	kA	KLECZEK
<g/>
,	,	kIx,	,
Josip	Josip	k1gInSc1	Josip
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gNnSc1	náš
souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
327	[number]	k4	327
až	až	k9	až
329	[number]	k4	329
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
hvězd	hvězda	k1gFnPc2	hvězda
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
nejbližších	blízký	k2eAgFnPc2d3	nejbližší
hvězd	hvězda	k1gFnPc2	hvězda
</s>
</p>
<p>
<s>
Proměnná	proměnný	k2eAgFnSc1d1	proměnná
hvězda	hvězda	k1gFnSc1	hvězda
</s>
</p>
<p>
<s>
Řádová	řádový	k2eAgFnSc1d1	řádová
velikost	velikost	k1gFnSc1	velikost
</s>
</p>
<p>
<s>
Alfa	alfa	k1gFnSc1	alfa
Centauri	Centaur	k1gFnSc2	Centaur
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Proxima	Proximum	k1gNnSc2	Proximum
Centauri	Centaur	k1gFnSc2	Centaur
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
nejbližších	blízký	k2eAgFnPc2d3	nejbližší
hvězd	hvězda	k1gFnPc2	hvězda
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
:	:	kIx,	:
Nejbližší	blízký	k2eAgFnSc1d3	nejbližší
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
Astronomický	astronomický	k2eAgInSc1d1	astronomický
snímek	snímek	k1gInSc1	snímek
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
2002	[number]	k4	2002
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Alpha	Alpha	k1gFnSc1	Alpha
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Closest	Closest	k1gFnSc1	Closest
Star	Star	kA	Star
System	Systo	k1gNnSc7	Systo
<g/>
,	,	kIx,	,
Astronomy	astronom	k1gMnPc7	astronom
Picture	Pictur	k1gMnSc5	Pictur
of	of	k?	of
the	the	k?	the
Day	Day	k1gMnSc1	Day
<g/>
,	,	kIx,	,
May	May	k1gMnSc1	May
26	[number]	k4	26
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
–	–	k?	–
Astronomie	astronomie	k1gFnSc1	astronomie
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
:	:	kIx,	:
<g/>
The	The	k1gMnSc1	The
Nearest	Nearest	k1gMnSc1	Nearest
Star	Star	kA	Star
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Sun	sun	k1gInSc1	sun
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Alpha	Alpha	k1gFnSc1	Alpha
Centauri	Centaur	k1gFnSc2	Centaur
3	[number]	k4	3
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Nejbližší	blízký	k2eAgFnPc1d3	nejbližší
hvězdy	hvězda	k1gFnPc1	hvězda
na	na	k7c6	na
astronomickém	astronomický	k2eAgInSc6d1	astronomický
serveru	server	k1gInSc6	server
fakulty	fakulta	k1gFnSc2	fakulta
pedagogické	pedagogický	k2eAgFnSc2d1	pedagogická
ZČU	ZČU	kA	ZČU
V	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
</s>
</p>
