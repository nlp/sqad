<s>
Mulhacén	Mulhacén	k1gInSc1
</s>
<s>
Mulhacén	Mulhacén	k1gInSc1
Mulhacén	Mulhacén	k1gInSc1
od	od	k7c2
Punta	punto	k1gNnSc2
de	de	k?
Loma	Lom	k2eAgFnSc1d1
Pelada	Pelada	k1gFnSc1
</s>
<s>
Vrchol	vrchol	k1gInSc1
</s>
<s>
3478,6	3478,6	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Prominence	prominence	k1gFnSc2
</s>
<s>
3285	#num#	k4
m	m	kA
Izolace	izolace	k1gFnSc1
</s>
<s>
527	#num#	k4
km	km	kA
→	→	k?
Ayachi	Ayachi	k1gNnSc1
<g/>
,	,	kIx,
Maroko	Maroko	k1gNnSc1
Seznamy	seznam	k1gInPc1
</s>
<s>
Nejvyšší	vysoký	k2eAgFnPc1d3
hory	hora	k1gFnPc1
evropských	evropský	k2eAgFnPc2d1
zemíUltraprominentní	zemíUltraprominentní	k2eAgNnSc4d1
hory	hora	k1gFnPc4
Evropy	Evropa	k1gFnSc2
#	#	kIx~
<g/>
3	#num#	k4
Poznámka	poznámka	k1gFnSc1
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
Pyrenejského	pyrenejský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
Poloha	poloha	k1gFnSc1
Světadíl	světadíl	k1gInSc4
</s>
<s>
Evropa	Evropa	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
Španělsko	Španělsko	k1gNnSc4
Pohoří	pohořet	k5eAaPmIp3nS
</s>
<s>
Sierra	Sierra	k1gFnSc1
Nevada	Nevada	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
37	#num#	k4
<g/>
°	°	k?
<g/>
3	#num#	k4
<g/>
′	′	k?
<g/>
15	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
3	#num#	k4
<g/>
°	°	k?
<g/>
18	#num#	k4
<g/>
′	′	k?
<g/>
38	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
</s>
<s>
Mulhacén	Mulhacén	k1gInSc1
</s>
<s>
Prvovýstup	prvovýstup	k1gInSc1
</s>
<s>
neznámý	známý	k2eNgMnSc1d1
(	(	kIx(
<g/>
před	před	k7c7
rokem	rok	k1gInSc7
1500	#num#	k4
<g/>
)	)	kIx)
Povodí	povodí	k1gNnSc2
</s>
<s>
Genil	Genil	k1gMnSc1
→	→	k?
Guadalquivir	Guadalquivir	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mulhacén	Mulhacén	k1gInSc1
je	být	k5eAaImIp3nS
španělská	španělský	k2eAgFnSc1d1
hora	hora	k1gFnSc1
<g/>
,	,	kIx,
ležící	ležící	k2eAgFnSc1d1
v	v	k7c6
pohoří	pohoří	k1gNnSc6
Sierra	Sierra	k1gFnSc1
Nevada	Nevada	k1gFnSc1
na	na	k7c6
jihu	jih	k1gInSc6
Pyrenejského	pyrenejský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
v	v	k7c6
provincii	provincie	k1gFnSc6
Granada	Granada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
nadmořskou	nadmořský	k2eAgFnSc7d1
výškou	výška	k1gFnSc7
3478,6	3478,6	k4
m	m	kA
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnSc7d3
horou	hora	k1gFnSc7
pevninského	pevninský	k2eAgNnSc2d1
Španělska	Španělsko	k1gNnSc2
(	(	kIx(
<g/>
nejvyšší	vysoký	k2eAgFnSc7d3
horou	hora	k1gFnSc7
celého	celý	k2eAgNnSc2d1
Španělského	španělský	k2eAgNnSc2d1
království	království	k1gNnSc2
je	být	k5eAaImIp3nS
Pico	Pico	k6eAd1
de	de	k7
Teide	Teide	k1gInSc1
na	na	k7c6
Kanárských	kanárský	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
<g/>
)	)	kIx)
i	i	k8xC
celého	celý	k2eAgInSc2d1
Pyrenejského	pyrenejský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnSc7d3
nealpskou	alpský	k2eNgFnSc7d1
horou	hora	k1gFnSc7
Evropy	Evropa	k1gFnSc2
a	a	k8xC
s	s	k7c7
prominencí	prominence	k1gFnSc7
3285	#num#	k4
m	m	kA
i	i	k8xC
třetí	třetí	k4xOgFnSc7
nejprominentnější	prominentní	k2eAgFnSc7d3
horou	hora	k1gFnSc7
celé	celý	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
(	(	kIx(
<g/>
po	po	k7c4
Mont	Mont	k1gInSc4
Blancu	Blanc	k1gMnSc3
a	a	k8xC
Etně	Etna	k1gFnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
strmém	strmý	k2eAgInSc6d1
severním	severní	k2eAgInSc6d1
svahu	svah	k1gInSc6
pramení	pramenit	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
Genil	Genila	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
jejímž	jejíž	k3xOyRp3gNnSc6
povodí	povodí	k1gNnSc6
se	se	k3xPyFc4
Mulhacén	Mulhacén	k1gInSc1
nachází	nacházet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Mulhacén	Mulhacén	k1gInSc1
je	být	k5eAaImIp3nS
pojmenován	pojmenovat	k5eAaPmNgInS
po	po	k7c4
Muley	Mule	k1gMnPc4
Hacénovi	Hacénův	k2eAgMnPc1d1
(	(	kIx(
<g/>
pánu	pán	k1gMnSc3
Hasanovi	Hasan	k1gMnSc3
<g/>
,	,	kIx,
arabsky	arabsky	k6eAd1
<g/>
:	:	kIx,
Abū	Abū	k1gFnSc1
ul-Ḥ	ul-Ḥ	k1gInSc1
ʿ	ʿ	k?
ibn	ibn	k?
Saʿ	Saʿ	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
muslimském	muslimský	k2eAgMnSc6d1
králi	král	k1gMnSc6
Granady	Granada	k1gFnSc2
v	v	k7c6
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
podle	podle	k7c2
legendy	legenda	k1gFnSc2
na	na	k7c6
vrcholu	vrchol	k1gInSc6
pohřben	pohřbít	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovo	slovo	k1gNnSc1
muley	mule	k2eAgFnPc1d1
(	(	kIx(
<g/>
mawley	mawlea	k1gFnPc1
<g/>
,	,	kIx,
moulay	moulaa	k1gFnPc1
<g/>
)	)	kIx)
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
arabštiny	arabština	k1gFnSc2
a	a	k8xC
znamená	znamenat	k5eAaImIp3nS
čestný	čestný	k2eAgInSc4d1
titul	titul	k1gInSc4
"	"	kIx"
<g/>
(	(	kIx(
<g/>
náš	náš	k3xOp1gMnSc1
<g/>
)	)	kIx)
pán	pán	k1gMnSc1
<g/>
"	"	kIx"
stejně	stejně	k6eAd1
jako	jako	k9
"	"	kIx"
<g/>
sidi	sidi	k6eAd1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
obdoba	obdoba	k1gFnSc1
anglického	anglický	k2eAgInSc2d1
titulu	titul	k1gInSc2
lord	lord	k1gMnSc1
nebo	nebo	k8xC
spíše	spíše	k9
sir	sir	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Výstup	výstup	k1gInSc1
na	na	k7c4
vrchol	vrchol	k1gInSc4
</s>
<s>
Na	na	k7c4
vrchol	vrchol	k1gInSc4
Mulhacénu	Mulhacén	k1gInSc2
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
vystoupat	vystoupat	k5eAaPmF
ze	z	k7c2
tří	tři	k4xCgInPc2
směrů	směr	k1gInPc2
-	-	kIx~
od	od	k7c2
západu	západ	k1gInSc2
<g/>
,	,	kIx,
jihu	jih	k1gInSc2
nebo	nebo	k8xC
od	od	k7c2
jihovýchodu	jihovýchod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cesta	cesta	k1gFnSc1
od	od	k7c2
západu	západ	k1gInSc2
vede	vést	k5eAaImIp3nS
od	od	k7c2
konečné	konečný	k2eAgFnSc2d1
lanovky	lanovka	k1gFnSc2
(	(	kIx(
<g/>
3300	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
z	z	k7c2
horského	horský	k2eAgNnSc2d1
střediska	středisko	k1gNnSc2
Pradollano	Pradollana	k1gFnSc5
(	(	kIx(
<g/>
přístupné	přístupný	k2eAgFnPc4d1
po	po	k7c6
silnici	silnice	k1gFnSc6
A-395	A-395	k1gMnSc2
z	z	k7c2
Granady	Granada	k1gFnSc2
<g/>
)	)	kIx)
na	na	k7c6
Veletu	Velet	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cesta	cesta	k1gFnSc1
pokračuje	pokračovat	k5eAaImIp3nS
po	po	k7c6
hřebeni	hřeben	k1gInSc6
směrem	směr	k1gInSc7
na	na	k7c4
východ	východ	k1gInSc4
a	a	k8xC
po	po	k7c4
celkem	celkem	k6eAd1
6	#num#	k4
km	km	kA
od	od	k7c2
konečné	konečný	k2eAgFnSc2d1
lanovky	lanovka	k1gFnSc2
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
horské	horský	k2eAgFnSc3d1
chatě	chata	k1gFnSc3
Refugio	Refugio	k6eAd1
Vivac	Vivac	k1gFnSc1
de	de	k?
la	la	k1gNnSc1
Caldera	Calder	k1gMnSc2
nad	nad	k7c7
plesem	ples	k1gInSc7
Laguna	laguna	k1gFnSc1
de	de	k?
la	la	k1gNnSc1
Caldera	Caldero	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
chaty	chata	k1gFnSc2
vede	vést	k5eAaImIp3nS
na	na	k7c4
východ	východ	k1gInSc4
závěrečné	závěrečný	k2eAgNnSc1d1
stoupání	stoupání	k1gNnSc1
na	na	k7c4
vrchol	vrchol	k1gInSc4
(	(	kIx(
<g/>
1,5	1,5	k4
km	km	kA
s	s	k7c7
převýšením	převýšení	k1gNnSc7
400	#num#	k4
metrů	metr	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
jihu	jih	k1gInSc2
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
jít	jít	k5eAaImF
z	z	k7c2
vesnice	vesnice	k1gFnSc2
Capileira	Capileir	k1gInSc2
k	k	k7c3
chatě	chata	k1gFnSc3
Refugio	Refugio	k1gMnSc1
de	de	k?
Poqueira	Poqueira	k1gMnSc1
ve	v	k7c6
výšce	výška	k1gFnSc6
2500	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
(	(	kIx(
<g/>
9	#num#	k4
km	km	kA
s	s	k7c7
převýšením	převýšení	k1gNnSc7
1100	#num#	k4
metrů	metr	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
přespat	přespat	k5eAaPmF
a	a	k8xC
druhý	druhý	k4xOgInSc4
den	den	k1gInSc4
pokračovat	pokračovat	k5eAaImF
na	na	k7c4
vrchol	vrchol	k1gInSc4
(	(	kIx(
<g/>
dalších	další	k2eAgFnPc2d1
5	#num#	k4
km	km	kA
s	s	k7c7
převýšením	převýšení	k1gNnSc7
1000	#num#	k4
metrů	metr	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
jihovýchodu	jihovýchod	k1gInSc2
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
jít	jít	k5eAaImF
z	z	k7c2
vesnice	vesnice	k1gFnSc2
Trevélez	Trevélez	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
kemp	kemp	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc2
možností	možnost	k1gFnPc2
je	být	k5eAaImIp3nS
prudké	prudký	k2eAgNnSc1d1
stoupání	stoupání	k1gNnSc1
na	na	k7c4
západ	západ	k1gInSc4
na	na	k7c4
předvrchol	předvrchol	k1gInSc4
Alto	Alto	k1gMnSc1
del	del	k?
Chorrillo	Chorrillo	k1gNnSc4
a	a	k8xC
z	z	k7c2
něj	on	k3xPp3gInSc2
na	na	k7c4
sever	sever	k1gInSc4
na	na	k7c4
vrchol	vrchol	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhou	druhý	k4xOgFnSc7
možností	možnost	k1gFnSc7
je	být	k5eAaImIp3nS
mírnější	mírný	k2eAgFnSc1d2
(	(	kIx(
<g/>
ale	ale	k8xC
delší	dlouhý	k2eAgMnSc1d2
<g/>
)	)	kIx)
stoupání	stoupání	k1gNnSc1
na	na	k7c4
severozápad	severozápad	k1gInSc4
na	na	k7c4
krásnou	krásný	k2eAgFnSc4d1
plošinu	plošina	k1gFnSc4
Siete	Siet	k1gInSc5
Lagunas	Lagunas	k1gInSc4
(	(	kIx(
<g/>
česky	česky	k6eAd1
Sedm	sedm	k4xCc1
lagun	laguna	k1gFnPc2
<g/>
,	,	kIx,
největší	veliký	k2eAgFnSc1d3
je	být	k5eAaImIp3nS
Laguna	laguna	k1gFnSc1
Hondera	Hondera	k1gFnSc1
ve	v	k7c6
výšce	výška	k1gFnSc6
2900	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ze	z	k7c2
které	který	k3yQgFnSc2,k3yIgFnSc2,k3yRgFnSc2
cesta	cesta	k1gFnSc1
stoupá	stoupat	k5eAaImIp3nS
nejprve	nejprve	k6eAd1
na	na	k7c4
jihozápad	jihozápad	k1gInSc4
a	a	k8xC
pak	pak	k6eAd1
na	na	k7c4
sever	sever	k1gInSc4
<g/>
,	,	kIx,
přes	přes	k7c4
další	další	k2eAgInSc4d1
předvrchol	předvrchol	k1gInSc4
Mulhacén	Mulhacén	k1gInSc1
II	II	kA
až	až	k9
na	na	k7c4
vrchol	vrchol	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Siete	Siet	k1gInSc5
Lagunas	Lagunas	k1gInSc4
lze	lze	k6eAd1
vcelku	vcelku	k6eAd1
pohodlně	pohodlně	k6eAd1
pod	pod	k7c7
širým	širý	k2eAgNnSc7d1
nebem	nebe	k1gNnSc7
přečkat	přečkat	k5eAaPmF
noc	noc	k1gFnSc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
se	se	k3xPyFc4
odsud	odsud	k6eAd1
dá	dát	k5eAaPmIp3nS
vystoupat	vystoupat	k5eAaPmF
na	na	k7c4
Alcazabu	Alcazaba	k1gFnSc4
<g/>
,	,	kIx,
třetí	třetí	k4xOgFnSc4
nejvyšší	vysoký	k2eAgFnSc4d3
horu	hora	k1gFnSc4
Sierra	Sierra	k1gFnSc1
Nevady	Nevada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mulhacén	Mulhacén	k1gInSc1
v	v	k7c6
zimě	zima	k1gFnSc6
</s>
<s>
Mulhacén	Mulhacén	k1gInSc1
od	od	k7c2
Laguna	laguna	k1gFnSc1
Hondera	Honder	k1gMnSc2
</s>
<s>
Mulhacén	Mulhacén	k1gInSc1
z	z	k7c2
Alcazaby	Alcazaba	k1gFnSc2
</s>
<s>
Strmá	strmý	k2eAgFnSc1d1
severní	severní	k2eAgFnSc1d1
stěna	stěna	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Mulhacén	Mulhacén	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Výstup	výstup	k1gInSc1
na	na	k7c4
Mulhacén	Mulhacén	k1gInSc4
na	na	k7c4
HoryEvropy	HoryEvrop	k1gInPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Výstup	výstup	k1gInSc1
na	na	k7c4
Mulhacén	Mulhacén	k1gInSc4
na	na	k7c6
Cestovani	Cestovaň	k1gFnSc6
<g/>
.	.	kIx.
<g/>
idnes	idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Mulhacén	Mulhacén	k1gInSc1
na	na	k7c4
SummitPost	SummitPost	k1gFnSc4
<g/>
.	.	kIx.
<g/>
org	org	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Zimní	zimní	k2eAgInSc4d1
výstup	výstup	k1gInSc4
na	na	k7c4
Mulhacén	Mulhacén	k1gInSc4
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
a	a	k8xC
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
Evropy	Evropa	k1gFnSc2
Země	zem	k1gFnSc2
</s>
<s>
Albánie	Albánie	k1gFnSc1
</s>
<s>
Andorra	Andorra	k1gFnSc1
</s>
<s>
Arménie	Arménie	k1gFnSc1
</s>
<s>
Ázerbájdžán	Ázerbájdžán	k1gInSc1
</s>
<s>
Belgie	Belgie	k1gFnSc1
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc1
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
</s>
<s>
Estonsko	Estonsko	k1gNnSc1
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Gruzie	Gruzie	k1gFnSc1
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
Irsko	Irsko	k1gNnSc1
</s>
<s>
Island	Island	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
Kypr	Kypr	k1gInSc1
</s>
<s>
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
</s>
<s>
Litva	Litva	k1gFnSc1
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1
</s>
<s>
Lucembursko	Lucembursko	k1gNnSc1
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
Malta	Malta	k1gFnSc1
</s>
<s>
Moldavsko	Moldavsko	k1gNnSc1
</s>
<s>
Monako	Monako	k1gNnSc1
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
Norsko	Norsko	k1gNnSc1
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Řecko	Řecko	k1gNnSc1
</s>
<s>
San	San	k?
Marino	Marina	k1gFnSc5
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Turecko	Turecko	k1gNnSc1
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
Vatikán	Vatikán	k1gInSc1
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
koloniea	koloniea	k6eAd1
zámořská	zámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Akrotiri	Akrotiri	k6eAd1
a	a	k8xC
Dekelia	Dekelia	k1gFnSc1
(	(	kIx(
<g/>
GB	GB	kA
<g/>
)	)	kIx)
</s>
<s>
Athos	Athos	k1gMnSc1
(	(	kIx(
<g/>
GR	GR	kA
<g/>
)	)	kIx)
</s>
<s>
Alandy	Alanda	k1gFnPc1
(	(	kIx(
<g/>
FI	fi	k0
<g/>
)	)	kIx)
</s>
<s>
Faerské	Faerský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
DK	DK	kA
<g/>
)	)	kIx)
</s>
<s>
Gibraltar	Gibraltar	k1gInSc1
(	(	kIx(
<g/>
GB	GB	kA
<g/>
)	)	kIx)
</s>
<s>
Guernsey	Guernsea	k1gFnPc1
(	(	kIx(
<g/>
GB	GB	kA
<g/>
)	)	kIx)
</s>
<s>
Jan	Jan	k1gMnSc1
Mayen	Mayna	k1gFnPc2
(	(	kIx(
<g/>
NO	no	k9
<g/>
)	)	kIx)
</s>
<s>
Jersey	Jersea	k1gFnPc1
(	(	kIx(
<g/>
GB	GB	kA
<g/>
)	)	kIx)
</s>
<s>
Man	Man	k1gMnSc1
(	(	kIx(
<g/>
GB	GB	kA
<g/>
)	)	kIx)
</s>
<s>
Špicberky	Špicberky	k1gFnPc1
(	(	kIx(
<g/>
NO	no	k9
<g/>
)	)	kIx)
Území	území	k1gNnSc1
se	s	k7c7
sporným	sporný	k2eAgNnSc7d1
postavením	postavení	k1gNnSc7
</s>
<s>
Kosovo	Kosův	k2eAgNnSc1d1
</s>
<s>
Arcach	Arcach	k1gMnSc1
</s>
<s>
Podněstří	Podněstřit	k5eAaPmIp3nS
</s>
<s>
Severní	severní	k2eAgInSc1d1
Kypr	Kypr	k1gInSc1
</s>
<s>
Abcházie	Abcházie	k1gFnSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Osetie	Osetie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
316429584	#num#	k4
</s>
