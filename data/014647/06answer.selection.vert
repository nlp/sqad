<s desamb="1">
S	s	k7c7
nadmořskou	nadmořský	k2eAgFnSc7d1
výškou	výška	k1gFnSc7
3478,6	3478,6	k4
m	m	kA
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnSc7d3
horou	hora	k1gFnSc7
pevninského	pevninský	k2eAgNnSc2d1
Španělska	Španělsko	k1gNnSc2
(	(	kIx(
<g/>
nejvyšší	vysoký	k2eAgFnSc7d3
horou	hora	k1gFnSc7
celého	celý	k2eAgNnSc2d1
Španělského	španělský	k2eAgNnSc2d1
království	království	k1gNnSc2
je	být	k5eAaImIp3nS
Pico	Pico	k6eAd1
de	de	k7
Teide	Teide	k1gInSc1
na	na	k7c6
Kanárských	kanárský	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
<g/>
)	)	kIx)
i	i	k8xC
celého	celý	k2eAgInSc2d1
Pyrenejského	pyrenejský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
<g/>
.	.	kIx.
</s>