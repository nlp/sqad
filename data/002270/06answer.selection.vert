<s>
Polsko	Polsko	k1gNnSc1	Polsko
hraničí	hraničit	k5eAaImIp3nS	hraničit
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
s	s	k7c7	s
Českem	Česko	k1gNnSc7	Česko
a	a	k8xC	a
Slovenskem	Slovensko	k1gNnSc7	Slovensko
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
Běloruskem	Bělorusko	k1gNnSc7	Bělorusko
a	a	k8xC	a
Ukrajinou	Ukrajina	k1gFnSc7	Ukrajina
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
s	s	k7c7	s
Litvou	Litva	k1gFnSc7	Litva
a	a	k8xC	a
Ruskem	Rusko	k1gNnSc7	Rusko
(	(	kIx(	(
<g/>
Kaliningradská	kaliningradský	k2eAgFnSc1d1	Kaliningradská
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
