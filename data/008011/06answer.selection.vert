<s>
Německo	Německo	k1gNnSc1	Německo
(	(	kIx(	(
<g/>
oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
Spolková	spolkový	k2eAgFnSc1d1	spolková
republika	republika	k1gFnSc1	republika
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
SRN	SRN	kA	SRN
<g/>
;	;	kIx,	;
německy	německy	k6eAd1	německy
<g/>
:	:	kIx,	:
Bundesrepublik	Bundesrepublika	k1gFnPc2	Bundesrepublika
Deutschland	Deutschland	k1gInSc1	Deutschland
<g/>
,	,	kIx,	,
neoficiální	neoficiální	k2eAgFnSc1d1	neoficiální
německá	německý	k2eAgFnSc1d1	německá
zkratka	zkratka	k1gFnSc1	zkratka
BRD	Brdy	k1gInPc2	Brdy
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středoevropský	středoevropský	k2eAgInSc4d1	středoevropský
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
rozdělený	rozdělený	k2eAgInSc4d1	rozdělený
na	na	k7c4	na
16	[number]	k4	16
spolkových	spolkový	k2eAgFnPc2d1	spolková
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
