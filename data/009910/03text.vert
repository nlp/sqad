<p>
<s>
Přímořský	přímořský	k2eAgInSc1d1	přímořský
stát	stát	k1gInSc1	stát
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
území	území	k1gNnSc4	území
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
rozumí	rozumět	k5eAaImIp3nS	rozumět
moře	moře	k1gNnSc4	moře
jako	jako	k8xC	jako
součást	součást	k1gFnSc4	součást
světového	světový	k2eAgInSc2d1	světový
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nikoliv	nikoliv	k9	nikoliv
třeba	třeba	k6eAd1	třeba
Kaspické	kaspický	k2eAgNnSc4d1	Kaspické
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Opakem	opak	k1gInSc7	opak
přímořského	přímořský	k2eAgInSc2d1	přímořský
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
vnitrozemský	vnitrozemský	k2eAgInSc1d1	vnitrozemský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
většina	většina	k1gFnSc1	většina
přímořských	přímořský	k2eAgFnPc2d1	přímořská
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
přes	přes	k7c4	přes
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc4d1	velký
strategický	strategický	k2eAgInSc4d1	strategický
význam	význam	k1gInSc4	význam
ohledně	ohledně	k7c2	ohledně
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
obchodu	obchod	k1gInSc2	obchod
(	(	kIx(	(
<g/>
námořní	námořní	k2eAgFnSc1d1	námořní
doprava	doprava	k1gFnSc1	doprava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zdroj	zdroj	k1gInSc1	zdroj
surovin	surovina	k1gFnPc2	surovina
a	a	k8xC	a
často	často	k6eAd1	často
zásadně	zásadně	k6eAd1	zásadně
přispívá	přispívat	k5eAaImIp3nS	přispívat
turistickému	turistický	k2eAgInSc3d1	turistický
ruchu	ruch	k1gInSc3	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
efektivitu	efektivita	k1gFnSc4	efektivita
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
má	mít	k5eAaImIp3nS	mít
nicméně	nicméně	k8xC	nicméně
značný	značný	k2eAgInSc4d1	značný
vliv	vliv	k1gInSc4	vliv
charakter	charakter	k1gInSc1	charakter
pobřeží	pobřeží	k1gNnSc1	pobřeží
(	(	kIx(	(
<g/>
zda	zda	k8xS	zda
je	on	k3xPp3gInPc4	on
strmé	strmý	k2eAgInPc4d1	strmý
nebo	nebo	k8xC	nebo
ploché	plochý	k2eAgInPc4d1	plochý
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
-li	i	k?	-li
přirozená	přirozený	k2eAgNnPc1d1	přirozené
přístaviště	přístaviště	k1gNnPc1	přístaviště
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
místní	místní	k2eAgNnSc4d1	místní
klima	klima	k1gNnSc4	klima
(	(	kIx(	(
<g/>
zda	zda	k8xS	zda
a	a	k8xC	a
na	na	k7c6	na
jak	jak	k6eAd1	jak
dlouho	dlouho	k6eAd1	dlouho
moře	moře	k1gNnSc1	moře
zamrzá	zamrzat	k5eAaImIp3nS	zamrzat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
bohaté	bohatý	k2eAgNnSc1d1	bohaté
na	na	k7c4	na
ryby	ryba	k1gFnPc4	ryba
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
dostupnost	dostupnost	k1gFnSc1	dostupnost
daného	daný	k2eAgNnSc2d1	dané
moře	moře	k1gNnSc2	moře
z	z	k7c2	z
volného	volný	k2eAgInSc2d1	volný
oceánu	oceán	k1gInSc2	oceán
či	či	k8xC	či
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kategorizace	kategorizace	k1gFnSc1	kategorizace
==	==	k?	==
</s>
</p>
<p>
<s>
Přímořské	přímořský	k2eAgInPc1d1	přímořský
státy	stát	k1gInPc1	stát
lze	lze	k6eAd1	lze
podle	podle	k7c2	podle
různých	různý	k2eAgInPc2d1	různý
aspektů	aspekt	k1gInPc2	aspekt
dále	daleko	k6eAd2	daleko
třídit	třídit	k5eAaImF	třídit
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
pobřeží	pobřeží	k1gNnSc4	pobřeží
spojité	spojitý	k2eAgNnSc4d1	spojité
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
případů	případ	k1gInPc2	případ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přerušené	přerušený	k2eAgNnSc1d1	přerušené
cizím	cizí	k2eAgNnSc7d1	cizí
státním	státní	k2eAgNnSc7d1	státní
územím	území	k1gNnSc7	území
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Malajsie	Malajsie	k1gFnSc2	Malajsie
x	x	k?	x
Brunej	Brunej	k1gFnSc2	Brunej
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
široký	široký	k2eAgInSc1d1	široký
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
úzký	úzký	k2eAgInSc1d1	úzký
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
uměle	uměle	k6eAd1	uměle
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
diplomacie	diplomacie	k1gFnSc2	diplomacie
-	-	kIx~	-
např.	např.	kA	např.
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
<g/>
,	,	kIx,	,
Irák	Irák	k1gInSc1	Irák
<g/>
,	,	kIx,	,
Jordánsko	Jordánsko	k1gNnSc1	Jordánsko
<g/>
,	,	kIx,	,
Dem	Dem	k?	Dem
<g/>
.	.	kIx.	.
rep	rep	k?	rep
<g/>
.	.	kIx.	.
</s>
<s>
Kongo	Kongo	k1gNnSc1	Kongo
<g/>
,	,	kIx,	,
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
kritérium	kritérium	k1gNnSc1	kritérium
lze	lze	k6eAd1	lze
kvantifikovat	kvantifikovat	k5eAaBmF	kvantifikovat
jako	jako	k8xC	jako
poměr	poměr	k1gInSc1	poměr
délky	délka	k1gFnSc2	délka
pobřeží	pobřeží	k1gNnSc2	pobřeží
k	k	k7c3	k
délce	délka	k1gFnSc3	délka
celé	celý	k2eAgFnSc2d1	celá
státní	státní	k2eAgFnSc2d1	státní
hranice	hranice	k1gFnSc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
státy	stát	k1gInPc1	stát
mají	mít	k5eAaImIp3nP	mít
hranici	hranice	k1gFnSc4	hranice
pouze	pouze	k6eAd1	pouze
mořskou	mořský	k2eAgFnSc4d1	mořská
(	(	kIx(	(
<g/>
vesměs	vesměs	k6eAd1	vesměs
ostrovní	ostrovní	k2eAgInPc1d1	ostrovní
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
kritériem	kritérion	k1gNnSc7	kritérion
je	být	k5eAaImIp3nS	být
délka	délka	k1gFnSc1	délka
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
;	;	kIx,	;
relativně	relativně	k6eAd1	relativně
nejdelší	dlouhý	k2eAgNnSc4d3	nejdelší
pobřeží	pobřeží	k1gNnSc4	pobřeží
(	(	kIx(	(
<g/>
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
rozloze	rozloha	k1gFnSc3	rozloha
státu	stát	k1gInSc2	stát
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
státy	stát	k1gInPc1	stát
na	na	k7c6	na
drobných	drobný	k2eAgNnPc6d1	drobné
souostrovích	souostroví	k1gNnPc6	souostroví
(	(	kIx(	(
<g/>
Maledivy	Maledivy	k1gFnPc4	Maledivy
<g/>
,	,	kIx,	,
Tokelau	Tokelaa	k1gFnSc4	Tokelaa
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
absolutně	absolutně	k6eAd1	absolutně
nejdelší	dlouhý	k2eAgInPc1d3	nejdelší
pak	pak	k6eAd1	pak
větší	veliký	k2eAgInPc1d2	veliký
státy	stát	k1gInPc1	stát
se	se	k3xPyFc4	se
členitým	členitý	k2eAgNnSc7d1	členité
pobřežím	pobřeží	k1gNnSc7	pobřeží
(	(	kIx(	(
<g/>
Filipíny	Filipíny	k1gFnPc4	Filipíny
<g/>
,	,	kIx,	,
Indonésie	Indonésie	k1gFnSc1	Indonésie
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stát	stát	k1gInSc1	stát
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
jednomu	jeden	k4xCgInSc3	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
mořím	mořit	k5eAaImIp1nS	mořit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
více	hodně	k6eAd2	hodně
oceánům	oceán	k1gInPc3	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
všem	všecek	k3xTgInPc3	všecek
čtyřem	čtyři	k4xCgInPc3	čtyři
oceánům	oceán	k1gInPc3	oceán
nemá	mít	k5eNaImIp3nS	mít
přístup	přístup	k1gInSc1	přístup
žádný	žádný	k3yNgInSc4	žádný
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
naposledy	naposledy	k6eAd1	naposledy
takový	takový	k3xDgInSc4	takový
mělo	mít	k5eAaImAgNnS	mít
Britské	britský	k2eAgNnSc1d1	Britské
impérium	impérium	k1gNnSc1	impérium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Státy	stát	k1gInPc1	stát
s	s	k7c7	s
přístupem	přístup	k1gInSc7	přístup
k	k	k7c3	k
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jednomu	jeden	k4xCgInSc3	jeden
oceánu	oceán	k1gInSc3	oceán
==	==	k?	==
</s>
</p>
<p>
<s>
3	[number]	k4	3
oceány	oceán	k1gInPc7	oceán
</s>
</p>
