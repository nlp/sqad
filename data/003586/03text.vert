<s>
Informatika	informatika	k1gFnSc1	informatika
je	být	k5eAaImIp3nS	být
obor	obor	k1gInSc4	obor
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
zpracováním	zpracování	k1gNnSc7	zpracování
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
množství	množství	k1gNnSc1	množství
specializovaných	specializovaný	k2eAgInPc2d1	specializovaný
vědních	vědní	k2eAgInPc2d1	vědní
a	a	k8xC	a
technických	technický	k2eAgInPc2d1	technický
oborů	obor	k1gInPc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
Matematická	matematický	k2eAgFnSc1d1	matematická
informatika	informatika	k1gFnSc1	informatika
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
specializovanější	specializovaný	k2eAgFnSc1d2	specializovanější
teoretická	teoretický	k2eAgFnSc1d1	teoretická
informatika	informatika	k1gFnSc1	informatika
studují	studovat	k5eAaImIp3nP	studovat
složité	složitý	k2eAgInPc1d1	složitý
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
zpracování	zpracování	k1gNnSc1	zpracování
informací	informace	k1gFnPc2	informace
a	a	k8xC	a
používání	používání	k1gNnSc1	používání
počítačů	počítač	k1gMnPc2	počítač
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
technik	technikum	k1gNnPc2	technikum
aplikované	aplikovaný	k2eAgFnSc2d1	aplikovaná
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
elektrotechniky	elektrotechnika	k1gFnSc2	elektrotechnika
a	a	k8xC	a
softwarového	softwarový	k2eAgNnSc2d1	softwarové
inženýrství	inženýrství	k1gNnSc2	inženýrství
<g/>
.	.	kIx.	.
</s>
<s>
Informatik	informatik	k1gMnSc1	informatik
je	být	k5eAaImIp3nS	být
vědec	vědec	k1gMnSc1	vědec
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
informatikou	informatika	k1gFnSc7	informatika
<g/>
,	,	kIx,	,
teoretickou	teoretický	k2eAgFnSc7d1	teoretická
vědou	věda	k1gFnSc7	věda
o	o	k7c6	o
informacích	informace	k1gFnPc6	informace
a	a	k8xC	a
práci	práce	k1gFnSc3	práce
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Informační	informační	k2eAgFnPc1d1	informační
technologie	technologie	k1gFnPc1	technologie
studují	studovat	k5eAaImIp3nP	studovat
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
fungování	fungování	k1gNnSc1	fungování
počítačů	počítač	k1gMnPc2	počítač
po	po	k7c6	po
technické	technický	k2eAgFnSc6d1	technická
stránce	stránka	k1gFnSc6	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
informace	informace	k1gFnSc2	informace
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
počítače	počítač	k1gInPc1	počítač
nepracují	pracovat	k5eNaImIp3nP	pracovat
s	s	k7c7	s
ničím	nic	k3yNnSc7	nic
jiným	jiný	k2eAgNnSc7d1	jiné
<g/>
,	,	kIx,	,
než	než	k8xS	než
s	s	k7c7	s
daty	datum	k1gNnPc7	datum
(	(	kIx(	(
<g/>
informacemi	informace	k1gFnPc7	informace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
informace	informace	k1gFnSc2	informace
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
spojující	spojující	k2eAgFnSc4d1	spojující
aplikovanou	aplikovaný	k2eAgFnSc4d1	aplikovaná
matematiku	matematika	k1gFnSc4	matematika
a	a	k8xC	a
elektrotechniku	elektrotechnika	k1gFnSc4	elektrotechnika
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
kvantitativního	kvantitativní	k2eAgNnSc2d1	kvantitativní
vyjádření	vyjádření	k1gNnSc2	vyjádření
informace	informace	k1gFnSc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
bezeztrátovou	bezeztrátový	k2eAgFnSc7d1	bezeztrátová
kompresí	komprese	k1gFnSc7	komprese
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ZIP	zip	k1gInSc1	zip
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ztrátovou	ztrátový	k2eAgFnSc7d1	ztrátová
kompresí	komprese	k1gFnSc7	komprese
(	(	kIx(	(
<g/>
např.	např.	kA	např.
MP	MP	kA	MP
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kapacitou	kapacita	k1gFnSc7	kapacita
přenosového	přenosový	k2eAgInSc2d1	přenosový
kanálu	kanál	k1gInSc2	kanál
(	(	kIx(	(
<g/>
např.	např.	kA	např.
DSL	DSL	kA	DSL
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Informační	informační	k2eAgFnSc1d1	informační
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
studium	studium	k1gNnSc1	studium
zpracování	zpracování	k1gNnSc2	zpracování
<g/>
,	,	kIx,	,
správy	správa	k1gFnSc2	správa
a	a	k8xC	a
získávání	získávání	k1gNnSc2	získávání
informace	informace	k1gFnSc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Bioinformatika	Bioinformatika	k1gFnSc1	Bioinformatika
je	být	k5eAaImIp3nS	být
vědní	vědní	k2eAgFnSc1d1	vědní
disciplína	disciplína	k1gFnSc1	disciplína
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
metodami	metoda	k1gFnPc7	metoda
pro	pro	k7c4	pro
shromažďování	shromažďování	k1gNnSc4	shromažďování
<g/>
,	,	kIx,	,
analýzu	analýza	k1gFnSc4	analýza
a	a	k8xC	a
vizualizaci	vizualizace	k1gFnSc4	vizualizace
rozsáhlých	rozsáhlý	k2eAgInPc2d1	rozsáhlý
souborů	soubor	k1gInPc2	soubor
biologických	biologický	k2eAgNnPc2d1	biologické
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
dat	datum	k1gNnPc2	datum
molekulárně-biologických	molekulárněiologický	k2eAgMnPc2d1	molekulárně-biologický
<g/>
.	.	kIx.	.
</s>
<s>
Chemoinformatika	Chemoinformatika	k1gFnSc1	Chemoinformatika
spojuje	spojovat	k5eAaImIp3nS	spojovat
vědecký	vědecký	k2eAgInSc4d1	vědecký
výzkum	výzkum	k1gInSc4	výzkum
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
chemie	chemie	k1gFnSc2	chemie
a	a	k8xC	a
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Geoinformatika	Geoinformatika	k1gFnSc1	Geoinformatika
je	být	k5eAaImIp3nS	být
geoinformační	geoinformační	k2eAgFnSc1d1	geoinformační
(	(	kIx(	(
<g/>
geografie	geografie	k1gFnSc1	geografie
+	+	kIx~	+
informatika	informatika	k1gFnSc1	informatika
<g/>
)	)	kIx)	)
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgMnPc1d1	zabývající
se	se	k3xPyFc4	se
vývojem	vývoj	k1gInSc7	vývoj
a	a	k8xC	a
aplikací	aplikace	k1gFnSc7	aplikace
metod	metoda	k1gFnPc2	metoda
pro	pro	k7c4	pro
řešení	řešení	k1gNnSc4	řešení
problémů	problém	k1gInPc2	problém
geověd	geovědo	k1gNnPc2	geovědo
a	a	k8xC	a
příbuzných	příbuzný	k2eAgInPc2d1	příbuzný
oborů	obor	k1gInPc2	obor
<g/>
,	,	kIx,	,
se	s	k7c7	s
specifickým	specifický	k2eAgInSc7d1	specifický
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
geografickou	geografický	k2eAgFnSc4d1	geografická
polohu	poloha	k1gFnSc4	poloha
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Lékařská	lékařský	k2eAgFnSc1d1	lékařská
informatika	informatika	k1gFnSc1	informatika
(	(	kIx(	(
<g/>
též	též	k9	též
biomedicínská	biomedicínský	k2eAgFnSc1d1	biomedicínská
informatika	informatika	k1gFnSc1	informatika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vědní	vědní	k2eAgInSc1d1	vědní
obor	obor	k1gInSc1	obor
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
informační	informační	k2eAgFnSc2d1	informační
vědy	věda	k1gFnSc2	věda
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
též	též	k9	též
jako	jako	k9	jako
informatiky	informatika	k1gFnSc2	informatika
<g/>
)	)	kIx)	)
a	a	k8xC	a
medicíny	medicína	k1gFnSc2	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Neuroinformatika	Neuroinformatika	k1gFnSc1	Neuroinformatika
je	být	k5eAaImIp3nS	být
vědní	vědní	k2eAgInSc4d1	vědní
obor	obor	k1gInSc4	obor
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgInSc4d1	zabývající
se	s	k7c7	s
zpracováním	zpracování	k1gNnSc7	zpracování
a	a	k8xC	a
analýzou	analýza	k1gFnSc7	analýza
dat	datum	k1gNnPc2	datum
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
neurověd	neurověda	k1gFnPc2	neurověda
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
prostředků	prostředek	k1gInPc2	prostředek
informatiky	informatika	k1gFnSc2	informatika
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgFnSc1d1	sociální
informatika	informatika	k1gFnSc1	informatika
je	být	k5eAaImIp3nS	být
transdisciplinární	transdisciplinární	k2eAgInSc4d1	transdisciplinární
obor	obor	k1gInSc4	obor
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgInSc4d1	zabývající
se	s	k7c7	s
studiem	studio	k1gNnSc7	studio
designu	design	k1gInSc2	design
<g/>
,	,	kIx,	,
využití	využití	k1gNnSc2	využití
a	a	k8xC	a
důsledků	důsledek	k1gInPc2	důsledek
informačních	informační	k2eAgFnPc2d1	informační
a	a	k8xC	a
komunikačních	komunikační	k2eAgFnPc2d1	komunikační
technologií	technologie	k1gFnPc2	technologie
v	v	k7c6	v
institucionálním	institucionální	k2eAgInSc6d1	institucionální
a	a	k8xC	a
kulturním	kulturní	k2eAgInSc6d1	kulturní
kontextu	kontext	k1gInSc6	kontext
<g/>
.	.	kIx.	.
</s>
<s>
Spojuje	spojovat	k5eAaImIp3nS	spojovat
počítačovou	počítačový	k2eAgFnSc4d1	počítačová
vědu	věda	k1gFnSc4	věda
<g/>
,	,	kIx,	,
informační	informační	k2eAgFnSc4d1	informační
vědu	věda	k1gFnSc4	věda
<g/>
,	,	kIx,	,
informační	informační	k2eAgInPc4d1	informační
systémy	systém	k1gInPc4	systém
a	a	k8xC	a
sociální	sociální	k2eAgFnPc4d1	sociální
vědy	věda	k1gFnPc4	věda
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
informatika	informatika	k1gFnSc1	informatika
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
mnoha	mnoho	k4c7	mnoho
českými	český	k2eAgMnPc7d1	český
mluvčími	mluvčí	k1gMnPc7	mluvčí
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
zastřešující	zastřešující	k2eAgInSc4d1	zastřešující
pojem	pojem	k1gInSc4	pojem
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
jakékoli	jakýkoli	k3yIgNnSc4	jakýkoli
cílené	cílený	k2eAgNnSc4d1	cílené
zpracování	zpracování	k1gNnSc4	zpracování
informací	informace	k1gFnPc2	informace
(	(	kIx(	(
<g/>
i	i	k9	i
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
počítačů	počítač	k1gInPc2	počítač
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
určité	určitý	k2eAgFnSc6d1	určitá
oblasti	oblast	k1gFnSc6	oblast
používání	používání	k1gNnSc2	používání
počítačů	počítač	k1gInPc2	počítač
a	a	k8xC	a
telekomunikací	telekomunikace	k1gFnPc2	telekomunikace
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
pojetí	pojetí	k1gNnSc1	pojetí
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
informatika	informatika	k1gFnSc1	informatika
<g/>
"	"	kIx"	"
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
poměrně	poměrně	k6eAd1	poměrně
přesně	přesně	k6eAd1	přesně
německému	německý	k2eAgMnSc3d1	německý
die	die	k?	die
Informatik	informatika	k1gFnPc2	informatika
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jazycích	jazyk	k1gInPc6	jazyk
však	však	k9	však
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
nelze	lze	k6eNd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
vhodný	vhodný	k2eAgInSc4d1	vhodný
překlad	překlad	k1gInSc4	překlad
<g/>
.	.	kIx.	.
</s>
<s>
Anglické	anglický	k2eAgNnSc1d1	anglické
slovo	slovo	k1gNnSc1	slovo
informatics	informatics	k6eAd1	informatics
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
některými	některý	k3yIgFnPc7	některý
mluvčími	mluvčí	k1gMnPc7	mluvčí
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
synonymum	synonymum	k1gNnSc4	synonymum
computer	computer	k1gInSc1	computer
science	science	k1gFnPc4	science
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
vědní	vědní	k2eAgInSc1d1	vědní
obor	obor	k1gInSc1	obor
matematická	matematický	k2eAgFnSc1d1	matematická
informatika	informatika	k1gFnSc1	informatika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
vědecké	vědecký	k2eAgFnSc6d1	vědecká
komunitě	komunita	k1gFnSc6	komunita
však	však	k9	však
panuje	panovat	k5eAaImIp3nS	panovat
širší	široký	k2eAgFnSc1d2	širší
shoda	shoda	k1gFnSc1	shoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
anglické	anglický	k2eAgNnSc1d1	anglické
slovo	slovo	k1gNnSc1	slovo
informatics	informatics	k6eAd1	informatics
je	být	k5eAaImIp3nS	být
ekvivalentem	ekvivalent	k1gInSc7	ekvivalent
slova	slovo	k1gNnSc2	slovo
computing	computing	k1gInSc1	computing
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
souhrn	souhrn	k1gInSc4	souhrn
všech	všecek	k3xTgFnPc2	všecek
disciplín	disciplína	k1gFnPc2	disciplína
zabývajících	zabývající	k2eAgFnPc2d1	zabývající
se	se	k3xPyFc4	se
výpočetní	výpočetní	k2eAgFnSc7d1	výpočetní
technikou	technika	k1gFnSc7	technika
či	či	k8xC	či
zpracováním	zpracování	k1gNnSc7	zpracování
informací	informace	k1gFnPc2	informace
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
počítačů	počítač	k1gMnPc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
informatics	informatics	k6eAd1	informatics
není	být	k5eNaImIp3nS	být
prostým	prostý	k2eAgNnSc7d1	prosté
synonymem	synonymum	k1gNnSc7	synonymum
ke	k	k7c3	k
computer	computer	k1gInSc1	computer
science	scienka	k1gFnSc3	scienka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
dílčích	dílčí	k2eAgFnPc2d1	dílčí
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
neméně	málo	k6eNd2	málo
důležitými	důležitý	k2eAgFnPc7d1	důležitá
oblastmi	oblast	k1gFnPc7	oblast
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
informační	informační	k2eAgInPc4d1	informační
systémy	systém	k1gInPc4	systém
a	a	k8xC	a
informační	informační	k2eAgFnPc4d1	informační
technologie	technologie	k1gFnPc4	technologie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
více	hodně	k6eAd2	hodně
aplikační	aplikační	k2eAgInSc4d1	aplikační
charakter	charakter	k1gInSc4	charakter
a	a	k8xC	a
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
typicky	typicky	k6eAd1	typicky
vnímány	vnímat	k5eAaImNgFnP	vnímat
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
aplikované	aplikovaný	k2eAgFnSc2d1	aplikovaná
informatiky	informatika	k1gFnSc2	informatika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
pojmenování	pojmenování	k1gNnSc3	pojmenování
teorie	teorie	k1gFnSc2	teorie
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
informací	informace	k1gFnPc2	informace
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
termínem	termín	k1gInSc7	termín
informatika	informatika	k1gFnSc1	informatika
(	(	kIx(	(
<g/>
též	též	k9	též
vědní	vědní	k2eAgFnSc1d1	vědní
informatika	informatika	k1gFnSc1	informatika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
dalším	další	k2eAgFnPc3d1	další
historickým	historický	k2eAgFnPc3d1	historická
<g/>
,	,	kIx,	,
společenským	společenský	k2eAgFnPc3d1	společenská
a	a	k8xC	a
terminologickým	terminologický	k2eAgFnPc3d1	terminologická
okolnostem	okolnost	k1gFnPc3	okolnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
informatika	informatika	k1gFnSc1	informatika
v	v	k7c6	v
českém	český	k2eAgMnSc6d1	český
a	a	k8xC	a
slovenském	slovenský	k2eAgNnSc6d1	slovenské
prostředí	prostředí	k1gNnSc6	prostředí
někdy	někdy	k6eAd1	někdy
zaměňována	zaměňován	k2eAgFnSc1d1	zaměňována
či	či	k8xC	či
ztotožňována	ztotožňován	k2eAgFnSc1d1	ztotožňována
s	s	k7c7	s
informační	informační	k2eAgFnSc7d1	informační
vědou	věda	k1gFnSc7	věda
<g/>
.	.	kIx.	.
</s>
<s>
Informatika	informatika	k1gFnSc1	informatika
i	i	k9	i
informační	informační	k2eAgFnSc1d1	informační
věda	věda	k1gFnSc1	věda
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
i	i	k8xC	i
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
chápány	chápat	k5eAaImNgInP	chápat
jako	jako	k9	jako
zcela	zcela	k6eAd1	zcela
samostatné	samostatný	k2eAgFnPc4d1	samostatná
vědní	vědní	k2eAgFnPc4d1	vědní
a	a	k8xC	a
studijní	studijní	k2eAgFnPc4d1	studijní
obory	obora	k1gFnPc4	obora
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
smysl	smysl	k1gInSc4	smysl
<g/>
,	,	kIx,	,
náplň	náplň	k1gFnSc4	náplň
a	a	k8xC	a
poslání	poslání	k1gNnSc4	poslání
se	se	k3xPyFc4	se
tak	tak	k9	tak
značně	značně	k6eAd1	značně
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
předmětem	předmět	k1gInSc7	předmět
a	a	k8xC	a
východiskem	východisko	k1gNnSc7	východisko
informatiky	informatika	k1gFnSc2	informatika
je	být	k5eAaImIp3nS	být
počítač	počítač	k1gInSc1	počítač
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
technické	technický	k2eAgNnSc1d1	technické
a	a	k8xC	a
programové	programový	k2eAgNnSc1d1	programové
vybavení	vybavení	k1gNnSc1	vybavení
<g/>
,	,	kIx,	,
informační	informační	k2eAgFnSc1d1	informační
věda	věda	k1gFnSc1	věda
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
informacemi	informace	k1gFnPc7	informace
a	a	k8xC	a
psycho-sociálními	psychoociální	k2eAgInPc7d1	psycho-sociální
aspekty	aspekt	k1gInPc7	aspekt
jejich	jejich	k3xOp3gNnSc2	jejich
působení	působení	k1gNnSc2	působení
jako	jako	k8xC	jako
takovými	takový	k3xDgFnPc7	takový
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	s	k7c7	s
zákonitostmi	zákonitost	k1gFnPc7	zákonitost
procesů	proces	k1gInPc2	proces
vzniku	vznik	k1gInSc2	vznik
<g/>
,	,	kIx,	,
zpracování	zpracování	k1gNnSc2	zpracování
<g/>
,	,	kIx,	,
měření	měření	k1gNnSc2	měření
<g/>
,	,	kIx,	,
kódování	kódování	k1gNnSc2	kódování
<g/>
,	,	kIx,	,
ukládání	ukládání	k1gNnSc2	ukládání
<g/>
,	,	kIx,	,
transformace	transformace	k1gFnSc2	transformace
<g/>
,	,	kIx,	,
distribuce	distribuce	k1gFnSc2	distribuce
a	a	k8xC	a
recepce	recepce	k1gFnSc2	recepce
informací	informace	k1gFnPc2	informace
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Informační	informační	k2eAgFnSc1d1	informační
věda	věda	k1gFnSc1	věda
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
ostatní	ostatní	k2eAgFnPc4d1	ostatní
neinformatické	informatický	k2eNgFnPc4d1	informatický
společenskovědní	společenskovědní	k2eAgFnPc4d1	společenskovědní
<g/>
,	,	kIx,	,
humanitní	humanitní	k2eAgFnPc4d1	humanitní
i	i	k8xC	i
přírodovědné	přírodovědný	k2eAgFnPc4d1	přírodovědná
obory	obora	k1gFnPc4	obora
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
využívá	využívat	k5eAaImIp3nS	využívat
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
výsledků	výsledek	k1gInPc2	výsledek
informatiky	informatika	k1gFnSc2	informatika
(	(	kIx(	(
<g/>
počítačové	počítačový	k2eAgFnSc2d1	počítačová
vědy	věda	k1gFnSc2	věda
<g/>
)	)	kIx)	)
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
využívání	využívání	k1gNnSc2	využívání
hardware	hardware	k1gInSc1	hardware
a	a	k8xC	a
software	software	k1gInSc1	software
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
informatika	informatika	k1gFnSc1	informatika
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
<g/>
,	,	kIx,	,
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
zkoumání	zkoumání	k1gNnSc2	zkoumání
informací	informace	k1gFnPc2	informace
a	a	k8xC	a
psychosociálních	psychosociální	k2eAgInPc2d1	psychosociální
aspektů	aspekt	k1gInPc2	aspekt
jejich	jejich	k3xOp3gInSc2	jejich
vzniku	vznik	k1gInSc2	vznik
<g/>
,	,	kIx,	,
zpracování	zpracování	k1gNnSc2	zpracování
<g/>
,	,	kIx,	,
měření	měření	k1gNnSc2	měření
<g/>
,	,	kIx,	,
kódování	kódování	k1gNnSc2	kódování
<g/>
,	,	kIx,	,
ukládání	ukládání	k1gNnSc2	ukládání
<g/>
,	,	kIx,	,
transformace	transformace	k1gFnSc2	transformace
<g/>
,	,	kIx,	,
distribuce	distribuce	k1gFnSc2	distribuce
a	a	k8xC	a
recepce	recepce	k1gFnSc2	recepce
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Informační	informační	k2eAgFnSc1d1	informační
věda	věda	k1gFnSc1	věda
a	a	k8xC	a
informatika	informatika	k1gFnSc1	informatika
(	(	kIx(	(
<g/>
počítačová	počítačový	k2eAgFnSc1d1	počítačová
věda	věda	k1gFnSc1	věda
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
terminologicky	terminologicky	k6eAd1	terminologicky
rozlišovány	rozlišován	k2eAgInPc1d1	rozlišován
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
:	:	kIx,	:
informační	informační	k2eAgFnSc1d1	informační
věda	věda	k1gFnSc1	věda
jako	jako	k8xS	jako
Information	Information	k1gInSc1	Information
science	scienec	k1gMnSc2	scienec
informatika	informatik	k1gMnSc2	informatik
jako	jako	k8xS	jako
Informatics	Informatics	k1gInSc4	Informatics
matematická	matematický	k2eAgFnSc1d1	matematická
informatika	informatika	k1gFnSc1	informatika
(	(	kIx(	(
<g/>
počítačová	počítačový	k2eAgFnSc1d1	počítačová
věda	věda	k1gFnSc1	věda
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
jako	jako	k8xC	jako
Computer	computer	k1gInSc1	computer
science	science	k1gFnSc2	science
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
:	:	kIx,	:
informační	informační	k2eAgFnSc1d1	informační
věda	věda	k1gFnSc1	věda
jako	jako	k8xS	jako
Informationswissenschaft	Informationswissenschaft	k1gInSc1	Informationswissenschaft
informatika	informatika	k1gFnSc1	informatika
(	(	kIx(	(
<g/>
počítačová	počítačový	k2eAgFnSc1d1	počítačová
věda	věda	k1gFnSc1	věda
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Informatik	informatik	k1gMnSc1	informatik
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
:	:	kIx,	:
informační	informační	k2eAgFnSc1d1	informační
věda	věda	k1gFnSc1	věda
jako	jako	k8xC	jako
Science	Science	k1gFnSc1	Science
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
information	information	k1gInSc1	information
nebo	nebo	k8xC	nebo
Sciences	Sciences	k1gInSc1	Sciences
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
information	information	k1gInSc1	information
et	et	k?	et
des	des	k1gNnSc2	des
bibliothè	bibliothè	k?	bibliothè
či	či	k8xC	či
Sciences	Sciences	k1gInSc1	Sciences
de	de	k?	de
l	l	kA	l
<g />
.	.	kIx.	.
</s>
<s>
<g/>
'	'	kIx"	'
<g/>
information	information	k1gInSc1	information
<g/>
,	,	kIx,	,
documentation	documentation	k1gInSc1	documentation
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
Sciences	Sciences	k1gInSc1	Sciences
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
information	information	k1gInSc1	information
et	et	k?	et
de	de	k?	de
la	la	k1gNnPc2	la
communication	communication	k1gInSc1	communication
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
vědy	věda	k1gFnSc2	věda
o	o	k7c6	o
informacích	informace	k1gFnPc6	informace
a	a	k8xC	a
knihovnách	knihovna	k1gFnPc6	knihovna
či	či	k8xC	či
vědy	věda	k1gFnSc2	věda
o	o	k7c6	o
informacích	informace	k1gFnPc6	informace
a	a	k8xC	a
dokumentaci	dokumentace	k1gFnSc6	dokumentace
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
vědy	věda	k1gFnSc2	věda
o	o	k7c6	o
informacích	informace	k1gFnPc6	informace
a	a	k8xC	a
komunikaci	komunikace	k1gFnSc6	komunikace
<g/>
)	)	kIx)	)
informatika	informatika	k1gFnSc1	informatika
(	(	kIx(	(
<g/>
počítačová	počítačový	k2eAgFnSc1d1	počítačová
věda	věda	k1gFnSc1	věda
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
informatique	informatiqu	k1gMnSc2	informatiqu
ve	v	k7c6	v
slovenštině	slovenština	k1gFnSc6	slovenština
<g/>
:	:	kIx,	:
informační	informační	k2eAgFnSc1d1	informační
věda	věda	k1gFnSc1	věda
jako	jako	k8xS	jako
Informačná	Informačný	k2eAgFnSc1d1	Informačná
veda	vést	k5eAaImSgInS	vést
informatika	informatika	k1gFnSc1	informatika
(	(	kIx(	(
<g/>
počítačová	počítačový	k2eAgFnSc1d1	počítačová
věda	věda	k1gFnSc1	věda
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Informatika	informatika	k1gFnSc1	informatika
nebo	nebo	k8xC	nebo
Veda	vést	k5eAaImSgInS	vést
o	o	k7c4	o
počítačoch	počítačoch	k1gInSc4	počítačoch
aj.	aj.	kA	aj.
Informační	informační	k2eAgFnSc2d1	informační
technologie	technologie	k1gFnSc2	technologie
(	(	kIx(	(
<g/>
IT	IT	kA	IT
<g/>
)	)	kIx)	)
–	–	k?	–
anglicky	anglicky	k6eAd1	anglicky
information	information	k1gInSc1	information
technology	technolog	k1gMnPc7	technolog
Informační	informační	k2eAgFnSc2d1	informační
technologie	technologie	k1gFnSc2	technologie
studují	studovat	k5eAaImIp3nP	studovat
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
fungování	fungování	k1gNnSc1	fungování
počítačů	počítač	k1gMnPc2	počítač
po	po	k7c6	po
technické	technický	k2eAgFnSc6d1	technická
stránce	stránka	k1gFnSc6	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
informace	informace	k1gFnSc2	informace
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
počítače	počítač	k1gInPc1	počítač
nepracují	pracovat	k5eNaImIp3nP	pracovat
s	s	k7c7	s
ničím	nic	k3yNnSc7	nic
jiným	jiný	k2eAgNnSc7d1	jiné
<g/>
,	,	kIx,	,
než	než	k8xS	než
s	s	k7c7	s
daty	datum	k1gNnPc7	datum
(	(	kIx(	(
<g/>
informacemi	informace	k1gFnPc7	informace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
informace	informace	k1gFnSc2	informace
–	–	k?	–
anglicky	anglicky	k6eAd1	anglicky
information	information	k1gInSc4	information
theory	theora	k1gFnSc2	theora
Teorie	teorie	k1gFnSc1	teorie
informace	informace	k1gFnSc2	informace
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
spojující	spojující	k2eAgFnSc4d1	spojující
aplikovanou	aplikovaný	k2eAgFnSc4d1	aplikovaná
matematiku	matematika	k1gFnSc4	matematika
a	a	k8xC	a
elektrotechniku	elektrotechnika	k1gFnSc4	elektrotechnika
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
kvantitativního	kvantitativní	k2eAgNnSc2d1	kvantitativní
vyjádření	vyjádření	k1gNnSc2	vyjádření
informace	informace	k1gFnSc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
bezeztrátovou	bezeztrátový	k2eAgFnSc7d1	bezeztrátová
kompresí	komprese	k1gFnSc7	komprese
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ZIP	zip	k1gInSc1	zip
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ztrátovou	ztrátový	k2eAgFnSc7d1	ztrátová
kompresí	komprese	k1gFnSc7	komprese
(	(	kIx(	(
<g/>
např.	např.	kA	např.
MP	MP	kA	MP
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kapacitou	kapacita	k1gFnSc7	kapacita
přenosového	přenosový	k2eAgInSc2d1	přenosový
kanálu	kanál	k1gInSc2	kanál
(	(	kIx(	(
<g/>
např.	např.	kA	např.
DSL	DSL	kA	DSL
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
informatika	informatik	k1gMnSc2	informatik
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
informatika	informatika	k1gFnSc1	informatika
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Petr	Petr	k1gMnSc1	Petr
Říha	Říha	k1gMnSc1	Říha
<g/>
:	:	kIx,	:
Slovník	slovník	k1gInSc1	slovník
počítačové	počítačový	k2eAgFnSc2d1	počítačová
informatiky	informatika	k1gFnSc2	informatika
<g/>
,	,	kIx,	,
a-č	a-č	k?	a-č
výkladový	výkladový	k2eAgInSc1d1	výkladový
slovník	slovník	k1gInSc1	slovník
<g/>
,	,	kIx,	,
3500	[number]	k4	3500
hesel	heslo	k1gNnPc2	heslo
<g/>
,	,	kIx,	,
nakl	nakla	k1gFnPc2	nakla
<g/>
.	.	kIx.	.
</s>
<s>
Montanex	Montanex	k1gInSc1	Montanex
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Online	Onlinout	k5eAaPmIp3nS	Onlinout
verze	verze	k1gFnSc1	verze
je	být	k5eAaImIp3nS	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
o	o	k7c4	o
hesla	heslo	k1gNnPc4	heslo
dalších	další	k2eAgMnPc2d1	další
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
