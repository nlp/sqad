<s>
Fredi	Fredi	k1gMnSc1
Bobic	Bobic	k1gMnSc1
</s>
<s>
Fredi	Fredi	k1gMnSc1
Bobic	Bobic	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1971	#num#	k4
(	(	kIx(
<g/>
49	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Maribor	Maribor	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
fotbalista	fotbalista	k1gMnSc1
Web	web	k1gInSc4
</s>
<s>
www.fredibobic.de	www.fredibobic.de	k6eAd1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
</s>
<s>
ME	ME	kA
1996	#num#	k4
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
německá	německý	k2eAgFnSc1d1
Bundesliga	bundesliga	k1gFnSc1
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
</s>
<s>
Borussia	Borussia	k1gFnSc1
Dortmund	Dortmund	k1gInSc1
</s>
<s>
Fredi	Fredi	k1gMnSc1
Bobic	Bobic	k1gMnSc1
<g/>
,	,	kIx,
slovinsky	slovinsky	k6eAd1
Fredi	Fredi	k1gMnSc1
Bobič	Bobič	k1gMnSc1
<g/>
,	,	kIx,
chorvatsky	chorvatsky	k6eAd1
Fredi	Fredi	k1gMnSc1
Bobić	Bobić	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
30	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
1971	#num#	k4
<g/>
,	,	kIx,
Maribor	Maribor	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgMnSc1d1
německý	německý	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
slovinského	slovinský	k2eAgMnSc2d1
a	a	k8xC
chorvatského	chorvatský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hrával	hrávat	k5eAaImAgInS
na	na	k7c4
pozici	pozice	k1gFnSc4
útočníka	útočník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
německou	německý	k2eAgFnSc7d1
reprezentací	reprezentace	k1gFnSc7
získal	získat	k5eAaPmAgInS
zlatou	zlatý	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
na	na	k7c6
mistrovství	mistrovství	k1gNnSc6
Evropy	Evropa	k1gFnSc2
1996	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Hrál	hrát	k5eAaImAgInS
i	i	k9
na	na	k7c6
Euru	euro	k1gNnSc6
2004	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Celkem	celkem	k6eAd1
za	za	k7c4
národní	národní	k2eAgInSc4d1
tým	tým	k1gInSc4
odehrál	odehrát	k5eAaPmAgMnS
37	#num#	k4
utkání	utkání	k1gNnPc4
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc2
vstřelil	vstřelit	k5eAaPmAgMnS
10	#num#	k4
gólů	gól	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
S	s	k7c7
Borussií	Borussie	k1gFnSc7
Dortmund	Dortmund	k1gInSc4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
mistrem	mistr	k1gMnSc7
Německa	Německo	k1gNnSc2
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s	s	k7c7
VfB	VfB	k1gFnSc7
Stuttgart	Stuttgart	k1gInSc1
vyhrál	vyhrát	k5eAaPmAgInS
německý	německý	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dresu	dres	k1gInSc6
HNK	HNK	kA
Rijeka	Rijeka	k1gFnSc1
získal	získat	k5eAaPmAgInS
chorvatský	chorvatský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
sezóně	sezóna	k1gFnSc6
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
se	s	k7c7
17	#num#	k4
brankami	branka	k1gFnPc7
nejlepším	dobrý	k2eAgMnSc7d3
střelcem	střelec	k1gMnSc7
německé	německý	k2eAgFnSc2d1
Bundesligy	Bundesliga	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
skončení	skončení	k1gNnSc6
hráčské	hráčský	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
se	se	k3xPyFc4
dal	dát	k5eAaPmAgInS
na	na	k7c4
trenérskou	trenérský	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Bernd	Bernd	k1gMnSc1
Rohr	Rohr	k1gMnSc1
<g/>
,	,	kIx,
Günter	Günter	k1gMnSc1
Simon	Simon	k1gMnSc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
<g/>
:	:	kIx,
velký	velký	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
<g/>
,	,	kIx,
Grada	Grada	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2006	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
247	#num#	k4
<g/>
-	-	kIx~
<g/>
1158	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
71	#num#	k4
<g/>
↑	↑	k?
http://www.rsssf.com/tables/96e-f.html	http://www.rsssf.com/tables/96e-f.html	k1gMnSc1
<g/>
↑	↑	k?
http://www.rsssf.com/tables/04e-ext.html#fin	http://www.rsssf.com/tables/04e-ext.html#fin	k1gMnSc1
<g/>
↑	↑	k?
http://www.rsssf.com/miscellaneous/bobic-intlg.html	http://www.rsssf.com/miscellaneous/bobic-intlg.html	k1gMnSc1
<g/>
↑	↑	k?
Profil	profil	k1gInSc1
hráče	hráč	k1gMnSc2
<g/>
,	,	kIx,
eu-football	eu-football	k1gInSc1
<g/>
.	.	kIx.
<g/>
info	info	k6eAd1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
http://www.national-football-teams.com/player/2562/Fredi_Bobic.html	http://www.national-football-teams.com/player/2562/Fredi_Bobic.html	k1gInSc1
<g/>
↑	↑	k?
http://www.rsssf.com/tablesd/duittops.html	http://www.rsssf.com/tablesd/duittops.html	k1gInSc1
<g/>
↑	↑	k?
http://www.transfermarkt.de/fredi-bobic/profil/trainer/9584	http://www.transfermarkt.de/fredi-bobic/profil/trainer/9584	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Fredi	Fredi	k1gMnSc1
Bobic	Bobice	k1gInPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Medailisté	medailista	k1gMnPc1
–	–	k?
Mistrovství	mistrovství	k1gNnSc2
Evropy	Evropa	k1gFnSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
1996	#num#	k4
Německo	Německo	k1gNnSc1
</s>
<s>
Andreas	Andreas	k1gMnSc1
Köpke	Köpk	k1gFnSc2
•	•	k?
Oliver	Oliver	k1gMnSc1
Kahn	Kahn	k1gMnSc1
•	•	k?
Oliver	Oliver	k1gMnSc1
Reck	Reck	k1gMnSc1
•	•	k?
Stefan	Stefan	k1gMnSc1
Reuter	Reuter	k1gMnSc1
•	•	k?
Thomas	Thomas	k1gMnSc1
Helmer	Helmer	k1gMnSc1
•	•	k?
Matthias	Matthias	k1gMnSc1
Sammer	Sammer	k1gMnSc1
•	•	k?
Markus	Markus	k1gMnSc1
Babbel	Babbel	k1gMnSc1
•	•	k?
Jürgen	Jürgen	k1gInSc1
Kohler	Kohler	k1gMnSc1
•	•	k?
René	René	k1gMnSc1
Schneider	Schneider	k1gMnSc1
•	•	k?
Marco	Marco	k6eAd1
Bode	bůst	k5eAaImIp3nS
•	•	k?
Steffen	Steffen	k1gInSc1
Freund	Freund	k1gMnSc1
•	•	k?
Andreas	Andreas	k1gMnSc1
Möller	Möller	k1gMnSc1
•	•	k?
Mehmet	Mehmet	k1gMnSc1
Scholl	Scholl	k1gMnSc1
•	•	k?
Thomas	Thomas	k1gMnSc1
Häßler	Häßler	k1gMnSc1
•	•	k?
Mario	Mario	k1gMnSc1
Basler	Basler	k1gMnSc1
•	•	k?
Christian	Christian	k1gMnSc1
Ziege	Zieg	k1gFnSc2
•	•	k?
Thomas	Thomas	k1gMnSc1
Strunz	Strunz	k1gMnSc1
•	•	k?
Dieter	Dieter	k1gInSc1
Eilts	Eilts	k1gInSc1
•	•	k?
Jürgen	Jürgen	k1gInSc1
Klinsmann	Klinsmann	k1gMnSc1
•	•	k?
Oliver	Oliver	k1gMnSc1
Bierhoff	Bierhoff	k1gMnSc1
•	•	k?
Stefan	Stefan	k1gMnSc1
Kuntz	Kuntz	k1gMnSc1
•	•	k?
Fredi	Fredi	k1gMnSc1
Bobic	Bobic	k1gMnSc1
•	•	k?
Jens	Jens	k1gInSc1
Todt	Todt	k1gMnSc1
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Berti	Berti	k1gNnSc7
Vogts	Vogts	k1gInSc1
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Petr	Petr	k1gMnSc1
Kouba	Kouba	k1gMnSc1
•	•	k?
Pavel	Pavel	k1gMnSc1
Srniček	srnička	k1gFnPc2
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Maier	Maier	k1gMnSc1
•	•	k?
Miroslav	Miroslav	k1gMnSc1
Kadlec	Kadlec	k1gMnSc1
•	•	k?
Luboš	Luboš	k1gMnSc1
Kubík	Kubík	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
Rada	Rada	k1gMnSc1
•	•	k?
Pavel	Pavel	k1gMnSc1
Novotný	Novotný	k1gMnSc1
•	•	k?
Michal	Michal	k1gMnSc1
Horňák	Horňák	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Suchopárek	Suchopárek	k1gMnSc1
•	•	k?
Radoslav	Radoslav	k1gMnSc1
Látal	Látal	k1gMnSc1
•	•	k?
Radek	Radek	k1gMnSc1
Bejbl	Bejbl	k1gMnSc1
•	•	k?
Martin	Martin	k1gMnSc1
Frýdek	frýdka	k1gFnPc2
•	•	k?
Patrik	Patrik	k1gMnSc1
Berger	Berger	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Němec	Němec	k1gMnSc1
•	•	k?
Pavel	Pavel	k1gMnSc1
Nedvěd	Nedvěd	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
Poborský	Poborský	k2eAgMnSc1d1
•	•	k?
Radek	Radek	k1gMnSc1
Drulák	Drulák	k1gMnSc1
•	•	k?
Pavel	Pavel	k1gMnSc1
Kuka	Kuka	k1gMnSc1
•	•	k?
Václav	Václav	k1gMnSc1
Němeček	Němeček	k1gMnSc1
•	•	k?
Vladimír	Vladimír	k1gMnSc1
Šmicer	Šmicer	k1gMnSc1
•	•	k?
Martin	Martin	k1gMnSc1
Kotůlek	Kotůlek	k1gMnSc1
•	•	k?
Milan	Milan	k1gMnSc1
Kerbr	Kerbr	k1gMnSc1
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Dušan	Dušan	k1gMnSc1
Uhrin	Uhrin	k1gMnSc1
Francie	Francie	k1gFnSc2
</s>
<s>
Bernard	Bernard	k1gMnSc1
Lama	lama	k1gMnSc1
•	•	k?
Jocelyn	Jocelyn	k1gMnSc1
Angloma	Anglom	k1gMnSc2
•	•	k?
Éric	Éric	k1gInSc1
Di	Di	k1gMnSc1
Meco	Meco	k1gMnSc1
•	•	k?
Frank	Frank	k1gMnSc1
Leboeuf	Leboeuf	k1gMnSc1
•	•	k?
Laurent	Laurent	k1gMnSc1
Blanc	Blanc	k1gMnSc1
•	•	k?
Vincent	Vincent	k1gMnSc1
Guérin	Guérin	k1gInSc1
•	•	k?
Didier	Didier	k1gInSc1
Deschamps	Deschamps	k1gInSc1
•	•	k?
Marcel	Marcel	k1gMnSc1
Desailly	Desailla	k1gFnSc2
•	•	k?
Youri	Your	k1gFnSc2
Djorkaeff	Djorkaeff	k1gMnSc1
•	•	k?
Zinédine	Zinédin	k1gMnSc5
Zidane	Zidan	k1gMnSc5
•	•	k?
Patrice	patrice	k1gFnSc2
Loko	Loko	k1gMnSc1
•	•	k?
Bixente	Bixent	k1gInSc5
Lizarazu	Lizaraz	k1gInSc3
•	•	k?
Christophe	Christoph	k1gMnSc2
Dugarry	Dugarra	k1gMnSc2
•	•	k?
Sabri	Sabr	k1gFnSc2
Lamouchi	Lamouchi	k1gNnSc2
•	•	k?
Lilian	Liliana	k1gFnPc2
Thuram	Thuram	k1gInSc4
•	•	k?
Fabien	Fabien	k2eAgMnSc1d1
Barthez	Barthez	k1gMnSc1
•	•	k?
Mickaël	Mickaël	k1gMnSc1
Madar	Madar	k1gMnSc1
•	•	k?
Reynald	Reynald	k1gInSc1
Pedros	Pedrosa	k1gFnPc2
•	•	k?
Christian	Christian	k1gMnSc1
Karembeu	Karembeus	k1gInSc2
•	•	k?
Alain	Alain	k2eAgInSc1d1
Roche	Roche	k1gInSc1
•	•	k?
Corentin	Corentin	k2eAgInSc1d1
Martins	Martins	k1gInSc1
•	•	k?
Bruno	Bruno	k1gMnSc1
Martini	martini	k1gNnSc2
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Aimé	Aimá	k1gFnPc1
Jacquet	Jacquet	k1gInSc4
Anglie	Anglie	k1gFnSc2
</s>
<s>
David	David	k1gMnSc1
Seaman	Seaman	k1gMnSc1
•	•	k?
Gary	Gary	k1gInPc1
Neville	Neville	k1gInSc1
•	•	k?
Stuart	Stuart	k1gInSc1
Pearce	Pearce	k1gMnSc1
•	•	k?
Paul	Paul	k1gMnSc1
Ince	Inka	k1gFnSc3
•	•	k?
Tony	Tony	k1gFnSc3
Adams	Adamsa	k1gFnPc2
•	•	k?
Gareth	Gareth	k1gMnSc1
Southgate	Southgat	k1gInSc5
•	•	k?
David	David	k1gMnSc1
Platt	Platt	k1gMnSc1
•	•	k?
Paul	Paul	k1gMnSc1
Gascoigne	Gascoign	k1gInSc5
•	•	k?
Alan	Alan	k1gMnSc1
Shearer	Shearer	k1gMnSc1
•	•	k?
Teddy	Tedd	k1gInPc1
Sheringham	Sheringham	k1gInSc1
•	•	k?
Darren	Darrno	k1gNnPc2
Anderton	Anderton	k1gInSc1
•	•	k?
Steve	Steve	k1gMnSc1
Howey	Howea	k1gFnSc2
•	•	k?
Ian	Ian	k1gMnSc1
Walker	Walker	k1gMnSc1
•	•	k?
Nick	Nick	k1gMnSc1
Barmby	Barmba	k1gFnSc2
•	•	k?
Jamie	Jamie	k1gFnSc2
Redknapp	Redknapp	k1gMnSc1
•	•	k?
Sol	sol	k1gInSc1
Campbell	Campbell	k1gMnSc1
•	•	k?
Steve	Steve	k1gMnSc1
McManaman	McManaman	k1gMnSc1
•	•	k?
Les	les	k1gInSc1
Ferdinand	Ferdinand	k1gMnSc1
•	•	k?
Phil	Phil	k1gMnSc1
Neville	Neville	k1gFnSc2
•	•	k?
Steve	Steve	k1gMnSc1
Stone	ston	k1gInSc5
•	•	k?
Robbie	Robbie	k1gFnSc2
Fowler	Fowler	k1gMnSc1
•	•	k?
Tim	Tim	k?
Flowers	Flowers	k1gInSc1
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Terry	Terra	k1gMnSc2
Venables	Venables	k1gMnSc1
</s>
<s>
Německá	německý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
Bundesliga	bundesliga	k1gFnSc1
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
–	–	k?
Borussia	Borussia	k1gFnSc1
Dortmund	Dortmund	k1gInSc1
Brankář	brankář	k1gMnSc1
</s>
<s>
Jens	Jens	k6eAd1
Lehmann	Lehmann	k1gInSc1
•	•	k?
Philipp	Philipp	k1gInSc1
Laux	Laux	k1gInSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Christian	Christian	k1gMnSc1
Wörns	Wörnsa	k1gFnPc2
•	•	k?
Dedê	Dedê	k1gMnSc1
•	•	k?
Stefan	Stefan	k1gMnSc1
Reuter	Reuter	k1gMnSc1
•	•	k?
Christoph	Christoph	k1gMnSc1
Metzelder	Metzelder	k1gMnSc1
•	•	k?
Jürgen	Jürgen	k1gInSc1
Kohler	Kohler	k1gMnSc1
•	•	k?
Ahmed	Ahmed	k1gMnSc1
Madouni	Madoun	k1gMnPc1
•	•	k?
Tomáš	Tomáš	k1gMnSc1
Rosický	rosický	k2eAgMnSc1d1
•	•	k?
Lars	Lars	k1gInSc1
Ricken	Ricken	k1gInSc1
•	•	k?
Evanilson	Evanilson	k1gMnSc1
•	•	k?
Miroslav	Miroslav	k1gMnSc1
Stević	Stević	k1gMnSc1
•	•	k?
Sunday	Sundaa	k1gFnSc2
Oliseh	Oliseh	k1gMnSc1
•	•	k?
Jörg	Jörg	k1gMnSc1
Heinrich	Heinrich	k1gMnSc1
•	•	k?
Sebastian	Sebastian	k1gMnSc1
Kehl	Kehl	k1gMnSc1
•	•	k?
Otto	Otto	k1gMnSc1
Addo	Addo	k1gMnSc1
•	•	k?
Francis	Francis	k1gFnSc1
Bugri	Bugr	k1gFnSc2
•	•	k?
Jan	Jan	k1gMnSc1
Koller	Koller	k1gMnSc1
•	•	k?
Márcio	Márcio	k1gMnSc1
Amoroso	amoroso	k1gNnSc1
•	•	k?
Ewerthon	Ewerthon	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Derek	Derek	k1gMnSc1
Sø	Sø	k2eAgMnSc1d1
•	•	k?
Heiko	Heiko	k1gNnSc1
Herrlich	Herrlich	k1gMnSc1
•	•	k?
Giuseppe	Giusepp	k1gInSc5
Reina	Rein	k2eAgFnSc1d1
•	•	k?
David	David	k1gMnSc1
Odonkor	Odonkor	k1gMnSc1
•	•	k?
Fredi	Fredi	k1gMnSc1
Bobic	Bobic	k1gMnSc1
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Matthias	Matthias	k1gMnSc1
Sammer	Sammer	k1gMnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgMnPc1d3
střelci	střelec	k1gMnPc1
německé	německý	k2eAgFnSc2d1
fotbalové	fotbalový	k2eAgFnSc2d1
Bundesligy	bundesliga	k1gFnSc2
*	*	kIx~
</s>
<s>
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
<g/>
:	:	kIx,
Seeler	Seeler	k1gInSc1
(	(	kIx(
<g/>
30	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
<g/>
:	:	kIx,
Brunnenmeier	Brunnenmeira	k1gFnPc2
(	(	kIx(
<g/>
24	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
<g/>
:	:	kIx,
Emmerich	Emmericha	k1gFnPc2
(	(	kIx(
<g/>
31	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
<g/>
:	:	kIx,
Emmerich	Emmericha	k1gFnPc2
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
G.	G.	kA
Müller	Müller	k1gMnSc1
(	(	kIx(
<g/>
28	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
<g/>
:	:	kIx,
Löhr	Löhra	k1gFnPc2
(	(	kIx(
<g/>
27	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
<g/>
:	:	kIx,
G.	G.	kA
Müller	Müller	k1gMnSc1
(	(	kIx(
<g/>
30	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
<g/>
:	:	kIx,
G.	G.	kA
Müller	Müller	k1gMnSc1
(	(	kIx(
<g/>
38	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
<g/>
:	:	kIx,
Kobluhn	Kobluhna	k1gFnPc2
(	(	kIx(
<g/>
24	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
<g/>
:	:	kIx,
G.	G.	kA
Müller	Müller	k1gMnSc1
(	(	kIx(
<g/>
40	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
<g/>
:	:	kIx,
G.	G.	kA
Müller	Müller	k1gMnSc1
(	(	kIx(
<g/>
36	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
74	#num#	k4
<g/>
:	:	kIx,
Heynckes	Heynckes	k1gMnSc1
/	/	kIx~
G.	G.	kA
Müller	Müller	k1gMnSc1
(	(	kIx(
<g/>
30	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
<g/>
:	:	kIx,
Heynckes	Heynckesa	k1gFnPc2
(	(	kIx(
<g/>
27	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
<g/>
:	:	kIx,
Fischer	Fischer	k1gMnSc1
(	(	kIx(
<g/>
29	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
<g/>
:	:	kIx,
D.	D.	kA
Müller	Müller	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
34	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
<g/>
:	:	kIx,
D.	D.	kA
Müller	Müller	k1gMnSc1
/	/	kIx~
G.	G.	kA
Müller	Müller	k1gMnSc1
(	(	kIx(
<g/>
24	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
<g/>
:	:	kIx,
K.	K.	kA
Allofs	Allofs	k1gInSc1
(	(	kIx(
<g/>
22	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
<g/>
:	:	kIx,
Rummenigge	Rummenigg	k1gInSc2
(	(	kIx(
<g/>
26	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
<g/>
:	:	kIx,
Rummenigge	Rummenigg	k1gInSc2
(	(	kIx(
<g/>
29	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
<g/>
:	:	kIx,
Hrubesch	Hrubescha	k1gFnPc2
(	(	kIx(
<g/>
27	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
<g/>
:	:	kIx,
Völler	Völler	k1gInSc1
(	(	kIx(
<g/>
23	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
Rummenigge	Rummenigg	k1gInSc2
(	(	kIx(
<g/>
26	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
<g/>
:	:	kIx,
K.	K.	kA
Allofs	Allofs	k1gInSc1
(	(	kIx(
<g/>
26	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
<g/>
:	:	kIx,
Kuntz	Kuntz	k1gMnSc1
(	(	kIx(
<g/>
22	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
<g/>
:	:	kIx,
Rahn	Rahna	k1gFnPc2
(	(	kIx(
<g/>
24	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
<g/>
:	:	kIx,
Klinsmann	Klinsmann	k1gMnSc1
(	(	kIx(
<g/>
19	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
<g/>
:	:	kIx,
T.	T.	kA
Allofs	Allofs	k1gInSc1
/	/	kIx~
Wohlfarth	Wohlfarth	k1gInSc1
(	(	kIx(
<g/>
17	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
<g/>
:	:	kIx,
Andersen	Andersen	k1gMnSc1
(	(	kIx(
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1990	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
91	#num#	k4
<g/>
:	:	kIx,
Wohlfarth	Wohlfartha	k1gFnPc2
(	(	kIx(
<g/>
21	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
<g/>
:	:	kIx,
Walter	Walter	k1gMnSc1
(	(	kIx(
<g/>
22	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
:	:	kIx,
Kirsten	Kirsten	k2eAgInSc1d1
/	/	kIx~
Yeboah	Yeboah	k1gInSc1
(	(	kIx(
<g/>
20	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
:	:	kIx,
Kuntz	Kuntz	k1gMnSc1
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
Yeboah	Yeboah	k1gInSc1
(	(	kIx(
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
<g/>
:	:	kIx,
Basler	Basler	k1gMnSc1
/	/	kIx~
Herrlich	Herrlich	k1gMnSc1
(	(	kIx(
<g/>
20	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
:	:	kIx,
Bobic	Bobice	k1gFnPc2
(	(	kIx(
<g/>
17	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
<g/>
:	:	kIx,
Kirsten	Kirsten	k2eAgMnSc1d1
(	(	kIx(
<g/>
22	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
:	:	kIx,
Kirsten	Kirsten	k2eAgMnSc1d1
(	(	kIx(
<g/>
22	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
:	:	kIx,
Preetz	Preetza	k1gFnPc2
(	(	kIx(
<g/>
23	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
Max	max	kA
(	(	kIx(
<g/>
19	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1	#num#	k4
<g/>
:	:	kIx,
Barbarez	Barbarez	k1gMnSc1
/	/	kIx~
Sand	Sand	k1gMnSc1
(	(	kIx(
<g/>
22	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
Amoroso	amoroso	k1gNnSc1
/	/	kIx~
Max	max	kA
(	(	kIx(
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
Christiansen	Christiansen	k1gInSc1
/	/	kIx~
Élber	Élber	k1gInSc1
(	(	kIx(
<g/>
21	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2003	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
Aílton	Aílton	k1gInSc1
(	(	kIx(
<g/>
28	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
Mintál	Mintála	k1gFnPc2
(	(	kIx(
<g/>
24	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
Klose	Klose	k1gFnSc2
(	(	kIx(
<g/>
25	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2006	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
:	:	kIx,
Gekas	Gekasa	k1gFnPc2
(	(	kIx(
<g/>
20	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
:	:	kIx,
Toni	Toni	k1gFnSc1
(	(	kIx(
<g/>
24	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
:	:	kIx,
Grafite	grafit	k1gInSc5
(	(	kIx(
<g/>
28	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
Džeko	Džeko	k1gNnSc4
(	(	kIx(
<g/>
22	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
:	:	kIx,
Gómez	Gómeza	k1gFnPc2
(	(	kIx(
<g/>
28	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
:	:	kIx,
Huntelaar	Huntelaara	k1gFnPc2
(	(	kIx(
<g/>
29	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
:	:	kIx,
Kießling	Kießling	k1gInSc1
(	(	kIx(
<g/>
25	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
:	:	kIx,
Lewandowski	Lewandowski	k1gNnSc6
(	(	kIx(
<g/>
20	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
:	:	kIx,
Meier	Meira	k1gFnPc2
(	(	kIx(
<g/>
19	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
:	:	kIx,
Lewandowski	Lewandowski	k1gNnSc6
(	(	kIx(
<g/>
30	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Aubameyang	Aubameyang	k1gInSc1
(	(	kIx(
<g/>
31	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
:	:	kIx,
Lewandowski	Lewandowski	k1gNnSc6
(	(	kIx(
<g/>
29	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
:	:	kIx,
Lewandowski	Lewandowski	k1gNnSc6
(	(	kIx(
<g/>
22	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
:	:	kIx,
Lewandowski	Lewandowski	k1gNnSc6
(	(	kIx(
<g/>
34	#num#	k4
<g/>
)	)	kIx)
*	*	kIx~
v	v	k7c6
závorce	závorka	k1gFnSc6
je	být	k5eAaImIp3nS
uveden	uveden	k2eAgInSc1d1
počet	počet	k1gInSc1
gólů	gól	k1gInPc2
nejlepších	dobrý	k2eAgMnPc2d3
střelců	střelec	k1gMnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1035228726	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0004	#num#	k4
0849	#num#	k4
5917	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
301208718	#num#	k4
</s>
