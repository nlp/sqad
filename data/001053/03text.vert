<p>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
(	(	kIx(	(
<g/>
*	*	kIx~	*
28.	[number]	k4	28.
září	zářit	k5eAaImIp3nS	zářit
1944	[number]	k4	1944
Kolín	Kolín	k1gInSc1	Kolín
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
ekonom	ekonom	k1gMnSc1	ekonom
<g/>
,	,	kIx,	,
prognostik	prognostik	k1gMnSc1	prognostik
a	a	k8xC	a
třetí	třetí	k4xOgMnSc1	třetí
prezident	prezident	k1gMnSc1	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Úřadu	úřad	k1gInSc2	úřad
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
ujal	ujmout	k5eAaPmAgMnS	ujmout
složením	složení	k1gNnSc7	složení
slibu	slib	k1gInSc2	slib
8.	[number]	k4	8.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
a	a	k8xC	a
druhé	druhý	k4xOgNnSc4	druhý
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
zahájil	zahájit	k5eAaPmAgInS	zahájit
přesně	přesně	k6eAd1	přesně
o	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
předseda	předseda	k1gMnSc1	předseda
České	český	k2eAgFnSc2d1	Česká
strany	strana	k1gFnSc2	strana
sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgFnPc1d1	demokratická
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
předsedy	předseda	k1gMnSc2	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
následující	následující	k2eAgInPc4d1	následující
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
sociálnědemokratické	sociálnědemokratický	k2eAgFnSc2d1	sociálnědemokratická
vlády	vláda	k1gFnSc2	vláda
menšinového	menšinový	k2eAgInSc2d1	menšinový
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
existence	existence	k1gFnSc1	existence
byla	být	k5eAaImAgFnS	být
umožněna	umožnit	k5eAaPmNgFnS	umožnit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
opoziční	opoziční	k2eAgFnSc2d1	opoziční
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
"	"	kIx"	"
s	s	k7c7	s
Občanskou	občanský	k2eAgFnSc7d1	občanská
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
neúspěšné	úspěšný	k2eNgFnSc6d1	neúspěšná
kandidatuře	kandidatura	k1gFnSc6	kandidatura
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
z	z	k7c2	z
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2010	[number]	k4	2010
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
nově	nově	k6eAd1	nově
založené	založený	k2eAgFnSc2d1	založená
Strany	strana	k1gFnSc2	strana
Práv	právo	k1gNnPc2	právo
Občanů	občan	k1gMnPc2	občan
ZEMANOVCI	zemanovec	k1gMnPc7	zemanovec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
funkci	funkce	k1gFnSc6	funkce
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
po	po	k7c6	po
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
se	se	k3xPyFc4	se
strana	strana	k1gFnSc1	strana
nedostala	dostat	k5eNaPmAgFnS	dostat
do	do	k7c2	do
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
přímé	přímý	k2eAgFnSc6d1	přímá
volbě	volba	k1gFnSc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
třetím	třetí	k4xOgInSc7	třetí
prezidentem	prezident	k1gMnSc7	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
historicky	historicky	k6eAd1	historicky
prvním	první	k4xOgMnSc7	první
českým	český	k2eAgMnSc7d1	český
prezidentem	prezident	k1gMnSc7	prezident
zvoleným	zvolený	k2eAgInSc7d1	zvolený
přímou	přímý	k2eAgFnSc7d1	přímá
volbou	volba	k1gFnSc7	volba
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc4	první
kolo	kolo	k1gNnSc4	kolo
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
a	a	k8xC	a
v	v	k7c6	v
rozhodujícím	rozhodující	k2eAgMnSc6d1	rozhodující
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
nad	nad	k7c7	nad
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
ministrem	ministr	k1gMnSc7	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Karlem	Karel	k1gMnSc7	Karel
Schwarzenbergem	Schwarzenberg	k1gMnSc7	Schwarzenberg
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
post	post	k1gInSc4	post
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
obhajovat	obhajovat	k5eAaImF	obhajovat
v	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
postoupil	postoupit	k5eAaPmAgMnS	postoupit
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Drahošem	Drahoš	k1gMnSc7	Drahoš
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
27.	[number]	k4	27.
ledna	leden	k1gInSc2	leden
2018	[number]	k4	2018
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
výsledkem	výsledek	k1gInSc7	výsledek
51,36	[number]	k4	51,36
%	%	kIx~	%
a	a	k8xC	a
obhájil	obhájit	k5eAaPmAgInS	obhájit
prezidentský	prezidentský	k2eAgInSc1d1	prezidentský
úřad	úřad	k1gInSc1	úřad
na	na	k7c4	na
dalších	další	k2eAgNnPc2d1	další
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Dětství	dětství	k1gNnSc4	dětství
a	a	k8xC	a
mládí	mládí	k1gNnSc4	mládí
===	===	k?	===
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
28.	[number]	k4	28.
září	září	k1gNnSc4	září
1944	[number]	k4	1944
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Zemanová	Zemanová	k1gFnSc1	Zemanová
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
učitelka	učitelka	k1gFnSc1	učitelka
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
poštovní	poštovní	k2eAgMnSc1d1	poštovní
úředník	úředník	k1gMnSc1	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
dvou	dva	k4xCgInPc6	dva
letech	let	k1gInPc6	let
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
vyrůstal	vyrůstat	k5eAaImAgInS	vyrůstat
jen	jen	k9	jen
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
a	a	k8xC	a
babičkou	babička	k1gFnSc7	babička
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgMnSc7	který
se	se	k3xPyFc4	se
nestýkal	stýkat	k5eNaImAgMnS	stýkat
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
třináct	třináct	k4xCc4	třináct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Studium	studium	k1gNnSc1	studium
a	a	k8xC	a
povolání	povolání	k1gNnSc1	povolání
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
dostudoval	dostudovat	k5eAaPmAgInS	dostudovat
střední	střední	k2eAgFnSc4d1	střední
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
hrozilo	hrozit	k5eAaImAgNnS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
bude	být	k5eAaImBp3nS	být
odepřena	odepřen	k2eAgFnSc1d1	odepřena
možnost	možnost	k1gFnSc1	možnost
složit	složit	k5eAaPmF	složit
maturitu	maturita	k1gFnSc4	maturita
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kvůli	kvůli	k7c3	kvůli
referátu	referát	k1gInSc3	referát
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
oslavoval	oslavovat	k5eAaImAgMnS	oslavovat
knihu	kniha	k1gFnSc4	kniha
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
Hovory	hovora	k1gMnSc2	hovora
s	s	k7c7	s
T.	T.	kA	T.
G.	G.	kA	G.
Masarykem	Masaryk	k1gMnSc7	Masaryk
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
komunistickým	komunistický	k2eAgInSc7d1	komunistický
režimem	režim	k1gInSc7	režim
zakázána	zakázat	k5eAaPmNgFnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgInS	být
připuštěn	připustit	k5eAaPmNgInS	připustit
k	k	k7c3	k
maturitě	maturita	k1gFnSc3	maturita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebylo	být	k5eNaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
vystaveno	vystaven	k2eAgNnSc1d1	vystaveno
doporučení	doporučení	k1gNnSc1	doporučení
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
postupu	postup	k1gInSc2	postup
později	pozdě	k6eAd2	pozdě
obvinil	obvinit	k5eAaPmAgMnS	obvinit
svou	svůj	k3xOyFgFnSc4	svůj
učitelku	učitelka	k1gFnSc4	učitelka
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
vinu	vina	k1gFnSc4	vina
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
maturitě	maturita	k1gFnSc6	maturita
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
účtárně	účtárna	k1gFnSc6	účtárna
závodu	závod	k1gInSc2	závod
Tatra	Tatra	k1gFnSc1	Tatra
Kolín	Kolín	k1gInSc1	Kolín
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
spolupracovníci	spolupracovník	k1gMnPc1	spolupracovník
později	pozdě	k6eAd2	pozdě
vzpomínali	vzpomínat	k5eAaImAgMnP	vzpomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gNnSc4	on
toto	tento	k3xDgNnSc4	tento
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
nebavilo	bavit	k5eNaImAgNnS	bavit
a	a	k8xC	a
k	k	k7c3	k
ostatním	ostatní	k2eAgMnPc3d1	ostatní
byl	být	k5eAaImAgInS	být
přezíravý	přezíravý	k2eAgMnSc1d1	přezíravý
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
praxi	praxe	k1gFnSc3	praxe
v	v	k7c6	v
závodu	závod	k1gInSc6	závod
Tatra	Tatra	k1gFnSc1	Tatra
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
získal	získat	k5eAaPmAgInS	získat
doporučení	doporučení	k1gNnSc4	doporučení
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
studiu	studio	k1gNnSc3	studio
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
kvůli	kvůli	k7c3	kvůli
srdeční	srdeční	k2eAgFnSc3d1	srdeční
vadě	vada	k1gFnSc3	vada
získal	získat	k5eAaPmAgInS	získat
tzv.	tzv.	kA	tzv.
modrou	modrý	k2eAgFnSc4d1	modrá
knížku	knížka	k1gFnSc4	knížka
a	a	k8xC	a
nemusel	muset	k5eNaImAgMnS	muset
absolvovat	absolvovat	k5eAaPmF	absolvovat
základní	základní	k2eAgFnSc4d1	základní
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
službu	služba	k1gFnSc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
začal	začít	k5eAaPmAgInS	začít
dálkově	dálkově	k6eAd1	dálkově
studovat	studovat	k5eAaImF	studovat
Národohospodářskou	národohospodářský	k2eAgFnSc4d1	Národohospodářská
fakultu	fakulta	k1gFnSc4	fakulta
VŠE	všechen	k3xTgNnSc1	všechen
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studoval	studovat	k5eAaImAgMnS	studovat
obor	obor	k1gInSc4	obor
národohospodářského	národohospodářský	k2eAgNnSc2d1	národohospodářské
plánování	plánování	k1gNnSc2	plánování
<g/>
,	,	kIx,	,
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
přešel	přejít	k5eAaPmAgInS	přejít
na	na	k7c4	na
denní	denní	k2eAgNnSc4d1	denní
studium	studium	k1gNnSc4	studium
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
zakončil	zakončit	k5eAaPmAgMnS	zakončit
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
zde	zde	k6eAd1	zde
také	také	k9	také
rok	rok	k1gInSc4	rok
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
studia	studio	k1gNnSc2	studio
bydlel	bydlet	k5eAaImAgMnS	bydlet
na	na	k7c6	na
koleji	kolej	k1gFnSc6	kolej
Jarov	Jarov	k1gInSc4	Jarov
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
spoluzaložil	spoluzaložit	k5eAaPmAgInS	spoluzaložit
diskusní	diskusní	k2eAgInSc1d1	diskusní
Studentský	studentský	k2eAgInSc1d1	studentský
futurologický	futurologický	k2eAgInSc1d1	futurologický
klub	klub	k1gInSc1	klub
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
pomocnou	pomocný	k2eAgFnSc7d1	pomocná
vědeckou	vědecký	k2eAgFnSc7d1	vědecká
silou	síla	k1gFnSc7	síla
profesora	profesor	k1gMnSc2	profesor
Pavla	Pavel	k1gMnSc2	Pavel
Hrubého	Hrubý	k1gMnSc2	Hrubý
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
diplomovou	diplomový	k2eAgFnSc4d1	Diplomová
práci	práce	k1gFnSc4	práce
napsal	napsat	k5eAaPmAgMnS	napsat
u	u	k7c2	u
profesora	profesor	k1gMnSc2	profesor
Věňka	Věňek	k1gMnSc2	Věňek
Šilhána	šilhán	k2eAgFnSc1d1	Šilhána
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
tehdejší	tehdejší	k2eAgMnPc1d1	tehdejší
známí	známý	k1gMnPc1	známý
ho	on	k3xPp3gNnSc4	on
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
nadaného	nadaný	k2eAgMnSc4d1	nadaný
studenta	student	k1gMnSc4	student
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
namyšleného	namyšlený	k2eAgMnSc4d1	namyšlený
samotáře	samotář	k1gMnSc4	samotář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Rodinný	rodinný	k2eAgInSc1d1	rodinný
život	život	k1gInSc1	život
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
manželství	manželství	k1gNnSc2	manželství
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
–	–	k?	–
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
s	s	k7c7	s
Blankou	Blanka	k1gFnSc7	Blanka
Zemanovou	Zemanův	k2eAgFnSc7d1	Zemanova
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc4	jeho
středoškolskou	středoškolský	k2eAgFnSc4d1	středoškolská
spolužákyní	spolužákyně	k1gFnSc7	spolužákyně
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
syna	syn	k1gMnSc4	syn
Davida	David	k1gMnSc4	David
Zemana	Zeman	k1gMnSc4	Zeman
(	(	kIx(	(
<g/>
*	*	kIx~	*
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
lékař	lékař	k1gMnSc1	lékař
v	v	k7c6	v
oborech	obor	k1gInPc6	obor
klinické	klinický	k2eAgFnSc2d1	klinická
biochemie	biochemie	k1gFnSc2	biochemie
a	a	k8xC	a
neurologie	neurologie	k1gFnSc2	neurologie
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
sňatek	sňatek	k1gInSc1	sňatek
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1993	[number]	k4	1993
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
asistentkou	asistentka	k1gFnSc7	asistentka
Ivanou	Ivana	k1gFnSc7	Ivana
Bednarčíkovou	Bednarčíkův	k2eAgFnSc7d1	Bednarčíkův
(	(	kIx(	(
<g/>
*	*	kIx~	*
29.	[number]	k4	29.
dubna	duben	k1gInSc2	duben
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
manželství	manželství	k1gNnSc2	manželství
se	se	k3xPyFc4	se
1.	[number]	k4	1.
ledna	leden	k1gInSc2	leden
1994	[number]	k4	1994
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Kateřina	Kateřina	k1gFnSc1	Kateřina
Zemanová	Zemanová	k1gFnSc1	Zemanová
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
druhou	druhý	k4xOgFnSc7	druhý
ženou	žena	k1gFnSc7	žena
se	se	k3xPyFc4	se
Zeman	Zeman	k1gMnSc1	Zeman
seznámil	seznámit	k5eAaPmAgMnS	seznámit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jí	on	k3xPp3gFnSc3	on
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
práci	práce	k1gFnSc4	práce
asistentky	asistentka	k1gFnSc2	asistentka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
kvůli	kvůli	k7c3	kvůli
tomuto	tento	k3xDgNnSc3	tento
zaměstnání	zaměstnání	k1gNnSc3	zaměstnání
nakonec	nakonec	k6eAd1	nakonec
nedostudovala	dostudovat	k5eNaPmAgFnS	dostudovat
romanistiku	romanistika	k1gFnSc4	romanistika
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mateřské	mateřský	k2eAgFnSc6d1	mateřská
dovolené	dovolená	k1gFnSc6	dovolená
pracovala	pracovat	k5eAaImAgFnS	pracovat
v	v	k7c6	v
několika	několik	k4yIc6	několik
firmách	firma	k1gFnPc6	firma
včetně	včetně	k7c2	včetně
Mold	Molda	k1gFnPc2	Molda
Vin	vina	k1gFnPc2	vina
CZ	CZ	kA	CZ
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vede	vést	k5eAaImIp3nS	vést
Zemanův	Zemanův	k2eAgMnSc1d1	Zemanův
známý	známý	k2eAgMnSc1d1	známý
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Zbytek	zbytek	k1gInSc1	zbytek
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
pracovala	pracovat	k5eAaImAgFnS	pracovat
na	na	k7c6	na
ředitelství	ředitelství	k1gNnSc6	ředitelství
pražského	pražský	k2eAgNnSc2d1	Pražské
Výstaviště	výstaviště	k1gNnSc2	výstaviště
či	či	k8xC	či
na	na	k7c6	na
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
kultury	kultura	k1gFnSc2	kultura
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zaměstnána	zaměstnat	k5eAaPmNgFnS	zaměstnat
jako	jako	k8xC	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
referentka	referentka	k1gFnSc1	referentka
ve	v	k7c6	v
Správě	správa	k1gFnSc6	správa
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
až	až	k9	až
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
zvolení	zvolení	k1gNnSc2	zvolení
prezidentem	prezident	k1gMnSc7	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2013	[number]	k4	2013
trávil	trávit	k5eAaImAgMnS	trávit
Zeman	Zeman	k1gMnSc1	Zeman
svůj	svůj	k3xOyFgInSc4	svůj
čas	čas	k1gInSc4	čas
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
bytě	byt	k1gInSc6	byt
v	v	k7c6	v
historické	historický	k2eAgFnSc6d1	historická
tvrzi	tvrz	k1gFnSc6	tvrz
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Veselí	veselí	k1gNnSc6	veselí
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Žďár	Žďár	k1gInSc1	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
je	být	k5eAaImIp3nS	být
spolumajitelem	spolumajitel	k1gMnSc7	spolumajitel
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gInPc7	jeho
zájmy	zájem	k1gInPc7	zájem
patří	patřit	k5eAaImIp3nS	patřit
pěší	pěší	k2eAgFnSc3d1	pěší
a	a	k8xC	a
lyžařská	lyžařský	k2eAgFnSc1d1	lyžařská
turistika	turistika	k1gFnSc1	turistika
<g/>
,	,	kIx,	,
četba	četba	k1gFnSc1	četba
nejrůznější	různý	k2eAgFnSc2d3	nejrůznější
literatury	literatura	k1gFnSc2	literatura
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
science	science	k1gFnSc1	science
fiction	fiction	k1gInSc1	fiction
<g/>
)	)	kIx)	)
a	a	k8xC	a
hra	hra	k1gFnSc1	hra
v	v	k7c4	v
šachy	šach	k1gInPc4	šach
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
fanouškem	fanoušek	k1gMnSc7	fanoušek
švédské	švédský	k2eAgFnSc2d1	švédská
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
ABBA	ABBA	kA	ABBA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
a	a	k8xC	a
životospráva	životospráva	k1gFnSc1	životospráva
===	===	k?	===
</s>
</p>
<p>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
je	být	k5eAaImIp3nS	být
náruživým	náruživý	k2eAgMnSc7d1	náruživý
kuřákem	kuřák	k1gMnSc7	kuřák
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
kampaně	kampaň	k1gFnSc2	kampaň
při	při	k7c6	při
volbě	volba	k1gFnSc6	volba
prezidenta	prezident	k1gMnSc2	prezident
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
sám	sám	k3xTgInSc1	sám
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
denně	denně	k6eAd1	denně
vypije	vypít	k5eAaPmIp3nS	vypít
šest	šest	k4xCc1	šest
sklenic	sklenice	k1gFnPc2	sklenice
vína	víno	k1gNnSc2	víno
a	a	k8xC	a
tři	tři	k4xCgMnPc1	tři
"	"	kIx"	"
<g/>
panáky	panák	k1gInPc4	panák
<g/>
"	"	kIx"	"
tvrdého	tvrdé	k1gNnSc2	tvrdé
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
bývalého	bývalý	k2eAgMnSc2d1	bývalý
ministra	ministr	k1gMnSc2	ministr
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
a	a	k8xC	a
vedoucího	vedoucí	k1gMnSc2	vedoucí
Zemanova	Zemanův	k2eAgNnSc2d1	Zemanovo
lékařského	lékařský	k2eAgNnSc2d1	lékařské
konzilia	konzilium	k1gNnSc2	konzilium
Martina	Martina	k1gFnSc1	Martina
Holcáta	Holcáta	k1gFnSc1	Holcáta
toto	tento	k3xDgNnSc4	tento
množství	množství	k1gNnSc4	množství
není	být	k5eNaImIp3nS	být
extrémní	extrémní	k2eAgNnSc1d1	extrémní
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
doporučil	doporučit	k5eAaPmAgMnS	doporučit
Zemanovi	Zeman	k1gMnSc3	Zeman
<g/>
,	,	kIx,	,
i	i	k9	i
kvůli	kvůli	k7c3	kvůli
zjištěné	zjištěný	k2eAgFnSc3d1	zjištěná
cukrovce	cukrovka	k1gFnSc3	cukrovka
<g/>
,	,	kIx,	,
omezení	omezení	k1gNnSc3	omezení
konzumace	konzumace	k1gFnSc2	konzumace
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
<g/>
Zeman	Zeman	k1gMnSc1	Zeman
trpí	trpět	k5eAaImIp3nS	trpět
cukrovkou	cukrovka	k1gFnSc7	cukrovka
a	a	k8xC	a
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
spojeným	spojený	k2eAgInPc3d1	spojený
onemocněním	onemocnění	k1gNnPc3	onemocnění
nervů	nerv	k1gInPc2	nerv
nohou	noha	k1gFnSc7	noha
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
kterému	který	k3yIgMnSc3	který
nemá	mít	k5eNaImIp3nS	mít
cit	cit	k1gInSc4	cit
v	v	k7c6	v
nohou	noha	k1gFnPc6	noha
od	od	k7c2	od
kotníků	kotník	k1gInPc2	kotník
dolů	dol	k1gInPc2	dol
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zdolávání	zdolávání	k1gNnSc6	zdolávání
schodů	schod	k1gInPc2	schod
mu	on	k3xPp3gMnSc3	on
proto	proto	k8xC	proto
musí	muset	k5eAaImIp3nS	muset
pomáhat	pomáhat	k5eAaImF	pomáhat
doprovod	doprovod	k1gInSc1	doprovod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
6.	[number]	k4	6.
<g/>
11.2017	[number]	k4	11.2017
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
"	"	kIx"	"
<g/>
cukrovka	cukrovka	k1gFnSc1	cukrovka
odešla	odejít	k5eAaPmAgFnS	odejít
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
jí	jíst	k5eAaImIp3nS	jíst
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
sladká	sladký	k2eAgNnPc4d1	sladké
jídla	jídlo	k1gNnPc4	jídlo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Prezidentův	prezidentův	k2eAgMnSc1d1	prezidentův
ošetřující	ošetřující	k2eAgMnSc1d1	ošetřující
lékař	lékař	k1gMnSc1	lékař
Miloslav	Miloslav	k1gMnSc1	Miloslav
Kalaš	Kalaš	k1gMnSc1	Kalaš
z	z	k7c2	z
pražské	pražský	k2eAgFnSc2d1	Pražská
Nemocnice	nemocnice	k1gFnSc2	nemocnice
Na	na	k7c6	na
Homolce	homolka	k1gFnSc6	homolka
to	ten	k3xDgNnSc1	ten
později	pozdě	k6eAd2	pozdě
dementoval	dementovat	k5eAaBmAgInS	dementovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2013	[number]	k4	2013
vzbudil	vzbudit	k5eAaPmAgMnS	vzbudit
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
značnou	značný	k2eAgFnSc4d1	značná
pozornost	pozornost	k1gFnSc4	pozornost
veřejnosti	veřejnost	k1gFnSc2	veřejnost
během	během	k7c2	během
otevírání	otevírání	k1gNnSc2	otevírání
komory	komora	k1gFnSc2	komora
s	s	k7c7	s
korunovačními	korunovační	k2eAgInPc7d1	korunovační
klenoty	klenot	k1gInPc7	klenot
ve	v	k7c6	v
Svatovítské	svatovítský	k2eAgFnSc6d1	Svatovítská
katedrále	katedrála	k1gFnSc6	katedrála
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
klíčníků	klíčník	k1gMnPc2	klíčník
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
se	se	k3xPyFc4	se
pohyboval	pohybovat	k5eAaImAgMnS	pohybovat
nekoordinovaně	koordinovaně	k6eNd1	koordinovaně
<g/>
,	,	kIx,	,
přidržoval	přidržovat	k5eAaImAgInS	přidržovat
se	se	k3xPyFc4	se
stěn	stěn	k1gInSc1	stěn
a	a	k8xC	a
působil	působit	k5eAaImAgInS	působit
dojmem	dojem	k1gInSc7	dojem
opilého	opilý	k2eAgMnSc4d1	opilý
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Podezření	podezření	k1gNnSc4	podezření
na	na	k7c4	na
opilost	opilost	k1gFnSc4	opilost
později	pozdě	k6eAd2	pozdě
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
účastnice	účastnice	k1gFnSc1	účastnice
ceremoniálu	ceremoniál	k1gInSc2	ceremoniál
Miroslava	Miroslava	k1gFnSc1	Miroslava
Němcová	Němcová	k1gFnSc1	Němcová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
stála	stát	k5eAaImAgFnS	stát
během	během	k7c2	během
události	událost	k1gFnSc2	událost
přímo	přímo	k6eAd1	přímo
vedle	vedle	k7c2	vedle
Zemana	Zeman	k1gMnSc2	Zeman
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Němcové	Němcová	k1gFnSc2	Němcová
byl	být	k5eAaImAgMnS	být
Zeman	Zeman	k1gMnSc1	Zeman
viditelně	viditelně	k6eAd1	viditelně
opilý	opilý	k2eAgMnSc1d1	opilý
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
cítit	cítit	k5eAaImF	cítit
z	z	k7c2	z
jeho	on	k3xPp3gInSc2	on
dechu	dech	k1gInSc2	dech
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
před	před	k7c7	před
touto	tento	k3xDgFnSc7	tento
událostí	událost	k1gFnSc7	událost
se	se	k3xPyFc4	se
prezident	prezident	k1gMnSc1	prezident
Zeman	Zeman	k1gMnSc1	Zeman
účastnil	účastnit	k5eAaImAgMnS	účastnit
recepce	recepce	k1gFnPc4	recepce
na	na	k7c6	na
Ruském	ruský	k2eAgNnSc6d1	ruské
velvyslanectví	velvyslanectví	k1gNnSc6	velvyslanectví
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
novináři	novinář	k1gMnSc3	novinář
zachycen	zachycen	k2eAgInSc4d1	zachycen
při	při	k7c6	při
popíjení	popíjení	k1gNnSc6	popíjení
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
účastník	účastník	k1gMnSc1	účastník
ceremonie	ceremonie	k1gFnSc2	ceremonie
Milan	Milan	k1gMnSc1	Milan
Štěch	Štěch	k1gMnSc1	Štěch
naopak	naopak	k6eAd1	naopak
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ze	z	k7c2	z
Zemana	Zeman	k1gMnSc2	Zeman
alkohol	alkohol	k1gInSc1	alkohol
cítit	cítit	k5eAaImF	cítit
nebyl	být	k5eNaImAgMnS	být
<g/>
,	,	kIx,	,
a	a	k8xC	a
obvinil	obvinit	k5eAaPmAgMnS	obvinit
Němcovou	Němcová	k1gFnSc4	Němcová
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
zviditelnit	zviditelnit	k5eAaPmF	zviditelnit
<g/>
.	.	kIx.	.
</s>
<s>
Zemanovu	Zemanův	k2eAgFnSc4d1	Zemanova
opilost	opilost	k1gFnSc4	opilost
popřel	popřít	k5eAaPmAgMnS	popřít
také	také	k9	také
jeho	jeho	k3xOp3gMnSc1	jeho
předchůdce	předchůdce	k1gMnSc1	předchůdce
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
recepce	recepce	k1gFnPc1	recepce
na	na	k7c6	na
ruském	ruský	k2eAgNnSc6d1	ruské
velvyslanectví	velvyslanectví	k1gNnSc6	velvyslanectví
rovněž	rovněž	k9	rovněž
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
prý	prý	k9	prý
v	v	k7c6	v
Zemanově	Zemanův	k2eAgFnSc6d1	Zemanova
přítomnosti	přítomnost	k1gFnSc6	přítomnost
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
a	a	k8xC	a
Zeman	Zeman	k1gMnSc1	Zeman
vypil	vypít	k5eAaPmAgMnS	vypít
snad	snad	k9	snad
jen	jen	k9	jen
jednu	jeden	k4xCgFnSc4	jeden
sklenku	sklenka	k1gFnSc4	sklenka
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgMnSc1d1	samotný
Zeman	Zeman	k1gMnSc1	Zeman
vysvětloval	vysvětlovat	k5eAaImAgMnS	vysvětlovat
své	své	k1gNnSc4	své
vrávorání	vrávorání	k1gNnSc2	vrávorání
virózou	viróza	k1gFnSc7	viróza
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
na	na	k7c4	na
30.	[number]	k4	30.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
utrpěl	utrpět	k5eAaPmAgInS	utrpět
úraz	úraz	k1gInSc1	úraz
kolene	kolen	k1gInSc5	kolen
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
slov	slovo	k1gNnPc2	slovo
jeho	on	k3xPp3gMnSc2	on
ošetřujícího	ošetřující	k2eAgMnSc2d1	ošetřující
lékaře	lékař	k1gMnSc2	lékař
<g/>
,	,	kIx,	,
a	a	k8xC	a
toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
úřednického	úřednický	k2eAgMnSc2d1	úřednický
ministra	ministr	k1gMnSc2	ministr
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
Martina	Martin	k1gMnSc2	Martin
Holcáta	Holcát	k1gMnSc2	Holcát
<g/>
,	,	kIx,	,
Zeman	Zeman	k1gMnSc1	Zeman
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
zakopl	zakopnout	k5eAaPmAgMnS	zakopnout
o	o	k7c4	o
shrnutý	shrnutý	k2eAgInSc4d1	shrnutý
koberec	koberec	k1gInSc4	koberec
a	a	k8xC	a
upadl	upadnout	k5eAaPmAgMnS	upadnout
na	na	k7c4	na
koleno	koleno	k1gNnSc4	koleno
<g/>
.	.	kIx.	.
</s>
<s>
Holcát	Holcát	k1gMnSc1	Holcát
dále	daleko	k6eAd2	daleko
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
léčba	léčba	k1gFnSc1	léčba
potrvá	trvat	k5eAaImIp3nS	trvat
až	až	k9	až
šest	šest	k4xCc1	šest
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterých	který	k3yIgFnPc2	který
bude	být	k5eAaImBp3nS	být
Zeman	Zeman	k1gMnSc1	Zeman
chodit	chodit	k5eAaImF	chodit
maximálně	maximálně	k6eAd1	maximálně
o	o	k7c6	o
berlích	berle	k1gFnPc6	berle
<g/>
.	.	kIx.	.
</s>
<s>
Kancelář	kancelář	k1gFnSc1	kancelář
prezidenta	prezident	k1gMnSc2	prezident
proto	proto	k8xC	proto
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
termínu	termín	k1gInSc6	termín
zrušila	zrušit	k5eAaPmAgFnS	zrušit
všechna	všechen	k3xTgNnPc4	všechen
plánovaná	plánovaný	k2eAgNnPc4d1	plánované
vystoupení	vystoupení	k1gNnPc4	vystoupení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
tiskové	tiskový	k2eAgFnSc2d1	tisková
konference	konference	k1gFnSc2	konference
31.	[number]	k4	31.
srpna	srpen	k1gInSc2	srpen
2015	[number]	k4	2015
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
lékaři	lékař	k1gMnPc1	lékař
diagnostikovali	diagnostikovat	k5eAaBmAgMnP	diagnostikovat
40	[number]	k4	40
<g/>
%	%	kIx~	%
ztrátu	ztráta	k1gFnSc4	ztráta
sluchu	sluch	k1gInSc2	sluch
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nespecifikoval	specifikovat	k5eNaBmAgMnS	specifikovat
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
tento	tento	k3xDgInSc4	tento
handicap	handicap	k1gInSc4	handicap
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tiskové	tiskový	k2eAgFnSc2d1	tisková
konference	konference	k1gFnSc2	konference
několikrát	několikrát	k6eAd1	několikrát
přeslechl	přeslechnout	k5eAaPmAgMnS	přeslechnout
položenou	položený	k2eAgFnSc4d1	položená
otázku	otázka	k1gFnSc4	otázka
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
faktickému	faktický	k2eAgNnSc3d1	faktické
rozpuštění	rozpuštění	k1gNnSc3	rozpuštění
lékařského	lékařský	k2eAgNnSc2d1	lékařské
konzilia	konzilium	k1gNnSc2	konzilium
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
Zemanově	Zemanův	k2eAgFnSc6d1	Zemanova
návštěvě	návštěva	k1gFnSc6	návštěva
Číny	Čína	k1gFnSc2	Čína
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2017	[number]	k4	2017
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
další	další	k2eAgFnPc1d1	další
pochybnosti	pochybnost	k1gFnPc1	pochybnost
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
zdravotním	zdravotní	k2eAgInSc6d1	zdravotní
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
společné	společný	k2eAgNnSc4d1	společné
focení	focení	k1gNnSc4	focení
hlav	hlava	k1gFnPc2	hlava
států	stát	k1gInPc2	stát
na	na	k7c6	na
summitu	summit	k1gInSc6	summit
k	k	k7c3	k
Hedvábné	hedvábný	k2eAgFnSc3d1	hedvábná
stezce	stezka	k1gFnSc3	stezka
se	se	k3xPyFc4	se
dostavil	dostavit	k5eAaPmAgMnS	dostavit
se	s	k7c7	s
zpožděním	zpoždění	k1gNnSc7	zpoždění
téměř	téměř	k6eAd1	téměř
dvou	dva	k4xCgFnPc2	dva
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnSc1d1	ostatní
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
mezitím	mezitím	k6eAd1	mezitím
čekali	čekat	k5eAaImAgMnP	čekat
<g/>
.	.	kIx.	.
</s>
<s>
Českého	český	k2eAgMnSc4d1	český
prezidenta	prezident	k1gMnSc4	prezident
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
dopravil	dopravit	k5eAaPmAgInS	dopravit
golfový	golfový	k2eAgInSc1d1	golfový
vozík	vozík	k1gInSc1	vozík
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Ovčáček	ovčáček	k1gMnSc1	ovčáček
v	v	k7c6	v
následném	následný	k2eAgNnSc6d1	následné
prohlášení	prohlášení	k1gNnSc6	prohlášení
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
únavu	únava	k1gFnSc4	únava
z	z	k7c2	z
náročného	náročný	k2eAgInSc2d1	náročný
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
vyloučil	vyloučit	k5eAaPmAgInS	vyloučit
zhoršení	zhoršení	k1gNnSc4	zhoršení
zdravotního	zdravotní	k2eAgInSc2d1	zdravotní
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Můj	můj	k3xOp1gInSc1	můj
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
malými	malý	k2eAgInPc7d1	malý
výkyvy	výkyv	k1gInPc7	výkyv
totožný	totožný	k2eAgMnSc1d1	totožný
minimálně	minimálně	k6eAd1	minimálně
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
výkonu	výkon	k1gInSc2	výkon
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
Zeman	Zeman	k1gMnSc1	Zeman
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
později	pozdě	k6eAd2	pozdě
při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
V	v	k7c6	v
mém	můj	k3xOp1gInSc6	můj
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc4d1	jediný
důvod	důvod	k1gInSc4	důvod
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
špatně	špatně	k6eAd1	špatně
chodím	chodit	k5eAaImIp1nS	chodit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
nemilé	milý	k2eNgNnSc1d1	nemilé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
jsem	být	k5eAaImIp1nS	být
jednou	jednou	k6eAd1	jednou
říkal	říkat	k5eAaImAgMnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
politika	politika	k1gFnSc1	politika
se	se	k3xPyFc4	se
nedělá	dělat	k5eNaImIp3nS	dělat
nohama	noha	k1gFnPc7	noha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mozkem	mozek	k1gInSc7	mozek
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
kardiochirurga	kardiochirurg	k1gMnSc2	kardiochirurg
z	z	k7c2	z
pražského	pražský	k2eAgInSc2d1	pražský
IKEMu	IKEMus	k1gInSc2	IKEMus
Jana	Jan	k1gMnSc2	Jan
Pirka	Pirek	k1gMnSc2	Pirek
by	by	kYmCp3nS	by
Zeman	Zeman	k1gMnSc1	Zeman
neměl	mít	k5eNaImAgMnS	mít
znovu	znovu	k6eAd1	znovu
kandidovat	kandidovat	k5eAaImF	kandidovat
a	a	k8xC	a
raději	rád	k6eAd2	rád
si	se	k3xPyFc3	se
odpočinout	odpočinout	k5eAaPmF	odpočinout
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Kdyby	kdyby	kYmCp3nS	kdyby
dělal	dělat	k5eAaImAgMnS	dělat
jakékoliv	jakýkoliv	k3yIgNnSc4	jakýkoliv
jiné	jiný	k2eAgNnSc4d1	jiné
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
by	by	kYmCp3nS	by
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
invalidním	invalidní	k2eAgInSc6d1	invalidní
důchodu	důchod	k1gInSc6	důchod
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgMnS	být
by	by	kYmCp3nS	by
schopen	schopen	k2eAgMnSc1d1	schopen
jej	on	k3xPp3gMnSc4	on
vykonávat	vykonávat	k5eAaImF	vykonávat
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
řekl	říct	k5eAaPmAgMnS	říct
Pirk	Pirk	k1gMnSc1	Pirk
<g/>
.	.	kIx.	.
</s>
<s>
Pochybnosti	pochybnost	k1gFnPc1	pochybnost
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
zdravotním	zdravotní	k2eAgInSc6d1	zdravotní
stavu	stav	k1gInSc6	stav
vyvraceli	vyvracet	k5eAaImAgMnP	vyvracet
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2017	[number]	k4	2017
prezidentův	prezidentův	k2eAgMnSc1d1	prezidentův
mluvčí	mluvčí	k1gMnSc1	mluvčí
Jiří	Jiří	k1gMnSc1	Jiří
Ovčáček	ovčáček	k1gMnSc1	ovčáček
a	a	k8xC	a
šéf	šéf	k1gMnSc1	šéf
lékařského	lékařský	k2eAgNnSc2d1	lékařské
konzilia	konzilium	k1gNnSc2	konzilium
Martin	Martin	k1gMnSc1	Martin
Holcát	Holcát	k1gMnSc1	Holcát
<g/>
.	.	kIx.	.
<g/>
Lékaři	lékař	k1gMnPc1	lékař
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
1.	[number]	k4	1.
kolem	kolem	k7c2	kolem
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2018	[number]	k4	2018
uvedli	uvést	k5eAaPmAgMnP	uvést
v	v	k7c6	v
prohlášení	prohlášení	k1gNnSc6	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
kromě	kromě	k7c2	kromě
neuropatie	neuropatie	k1gFnSc2	neuropatie
nohou	noha	k1gFnPc2	noha
se	se	k3xPyFc4	se
Zeman	Zeman	k1gMnSc1	Zeman
těší	těšit	k5eAaImIp3nS	těšit
dobrému	dobrý	k2eAgNnSc3d1	dobré
fyzickému	fyzický	k2eAgNnSc3d1	fyzické
i	i	k8xC	i
psychickému	psychický	k2eAgNnSc3d1	psychické
zdraví	zdraví	k1gNnSc3	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mezidobí	mezidobí	k1gNnSc6	mezidobí
před	před	k7c7	před
2.	[number]	k4	2.
kolem	kolem	k7c2	kolem
voleb	volba	k1gFnPc2	volba
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
podstoupil	podstoupit	k5eAaPmAgMnS	podstoupit
lékařský	lékařský	k2eAgInSc4d1	lékařský
zákrok	zákrok	k1gInSc4	zákrok
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Nemocnici	nemocnice	k1gFnSc6	nemocnice
Na	na	k7c6	na
Homolce	homolka	k1gFnSc6	homolka
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
vlastního	vlastní	k2eAgNnSc2d1	vlastní
vyjádření	vyjádření	k1gNnSc2	vyjádření
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
malý	malý	k2eAgInSc4d1	malý
zánět	zánět	k1gInSc4	zánět
v	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
ústní	ústní	k2eAgInSc4d1	ústní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Majetkové	majetkový	k2eAgInPc1d1	majetkový
poměry	poměr	k1gInPc1	poměr
===	===	k?	===
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kmenta	Kment	k1gMnSc2	Kment
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
citoval	citovat	k5eAaBmAgMnS	citovat
policejní	policejní	k2eAgMnSc1d1	policejní
odposlechy	odposlech	k1gInPc1	odposlech
Františka	František	k1gMnSc2	František
Mrázka	Mrázek	k1gMnSc2	Mrázek
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
zde	zde	k6eAd1	zde
označoval	označovat	k5eAaImAgMnS	označovat
jako	jako	k8xS	jako
kmotra	kmotra	k1gFnSc1	kmotra
českého	český	k2eAgNnSc2d1	české
podsvětí	podsvětí	k1gNnSc2	podsvětí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
uvedl	uvést	k5eAaPmAgMnS	uvést
mj.	mj.	kA	mj.
ke	k	k7c3	k
korupčnímu	korupční	k2eAgInSc3d1	korupční
potenciálu	potenciál	k1gInSc3	potenciál
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
i	i	k9	i
následující	následující	k2eAgInPc1d1	následující
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
'	'	kIx"	'
<g/>
Nejhorší	zlý	k2eAgMnSc1d3	nejhorší
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mlhovi	Mlhův	k2eAgMnPc1d1	Mlhův
[	[	kIx(	[
<g/>
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
]	]	kIx)	]
peníze	peníz	k1gInPc1	peníz
nic	nic	k6eAd1	nic
neříkají	říkat	k5eNaImIp3nP	říkat
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Prófa	prófa	k1gMnSc1	prófa
[	[	kIx(	[
<g/>
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
]	]	kIx)	]
je	být	k5eAaImIp3nS	být
má	mít	k5eAaImIp3nS	mít
rád	rád	k6eAd1	rád
<g/>
.	.	kIx.	.
</s>
<s>
Mlha	mlha	k1gFnSc1	mlha
tak	tak	k6eAd1	tak
akorát	akorát	k6eAd1	akorát
chleba	chléb	k1gInSc2	chléb
se	s	k7c7	s
sádlem	sádlo	k1gNnSc7	sádlo
a	a	k8xC	a
tři	tři	k4xCgInPc4	tři
okurky	okurek	k1gInPc4	okurek
<g/>
.	.	kIx.	.
</s>
<s>
Mlha	mlha	k1gFnSc1	mlha
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
lidi	člověk	k1gMnPc4	člověk
měli	mít	k5eAaImAgMnP	mít
rádi	rád	k2eAgMnPc1d1	rád
<g/>
,	,	kIx,	,
<g/>
'	'	kIx"	'
postěžoval	postěžovat	k5eAaPmAgMnS	postěžovat
si	se	k3xPyFc3	se
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2000	[number]	k4	2000
Mrázek	mrázek	k1gInSc4	mrázek
svému	svůj	k3xOyFgMnSc3	svůj
příteli	přítel	k1gMnSc3	přítel
[	[	kIx(	[
<g/>
Igoru	Igor	k1gMnSc3	Igor
<g/>
]	]	kIx)	]
Šafránkovi	Šafránek	k1gMnSc3	Šafránek
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Mrázek	mrázek	k1gInSc1	mrázek
byl	být	k5eAaImAgInS	být
nešťastný	šťastný	k2eNgMnSc1d1	nešťastný
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nedaří	dařit	k5eNaImIp3nS	dařit
prosadit	prosadit	k5eAaPmF	prosadit
u	u	k7c2	u
bankéřů	bankéř	k1gMnPc2	bankéř
a	a	k8xC	a
politiků	politik	k1gMnPc2	politik
obrovský	obrovský	k2eAgInSc1d1	obrovský
<g />
.	.	kIx.	.
</s>
<s>
byznys	byznys	k1gInSc1	byznys
za	za	k7c4	za
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvě	dva	k4xCgFnPc4	dva
miliardy	miliarda	k4xCgFnPc4	miliarda
korun	koruna	k1gFnPc2	koruna
-	-	kIx~	-
ovládnutí	ovládnutí	k1gNnSc1	ovládnutí
největší	veliký	k2eAgFnSc2d3	veliký
české	český	k2eAgFnSc2d1	Česká
stavební	stavební	k2eAgFnSc2d1	stavební
firmy	firma	k1gFnSc2	firma
<g/>
,	,	kIx,	,
IPS	IPS	kA	IPS
<g/>
,	,	kIx,	,
Inženýrské	inženýrský	k2eAgFnSc2d1	inženýrská
a	a	k8xC	a
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
stavby	stavba	k1gFnSc2	stavba
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2013	[number]	k4	2013
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
založil	založit	k5eAaPmAgMnS	založit
Prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
fond	fond	k1gInSc4	fond
(	(	kIx(	(
<g/>
Nadační	nadační	k2eAgInSc4d1	nadační
fond	fond	k1gInSc4	fond
Prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
Ing.	ing.	kA	ing.
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
<g/>
)	)	kIx)	)
a	a	k8xC	a
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
bude	být	k5eAaImBp3nS	být
každý	každý	k3xTgInSc1	každý
měsíc	měsíc	k1gInSc1	měsíc
až	až	k9	až
do	do	k7c2	do
března	březen	k1gInSc2	březen
2018	[number]	k4	2018
vkládat	vkládat	k5eAaImF	vkládat
třetinu	třetina	k1gFnSc4	třetina
svého	svůj	k3xOyFgInSc2	svůj
platu	plat	k1gInSc2	plat
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
60	[number]	k4	60
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Fond	fond	k1gInSc1	fond
měl	mít	k5eAaImAgInS	mít
mít	mít	k5eAaImF	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
umoření	umoření	k1gNnSc2	umoření
státního	státní	k2eAgInSc2d1	státní
dluhu	dluh	k1gInSc2	dluh
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
doposud	doposud	k6eAd1	doposud
tak	tak	k9	tak
použit	použit	k2eAgMnSc1d1	použit
nebyl	být	k5eNaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2016	[number]	k4	2016
na	na	k7c4	na
přímý	přímý	k2eAgInSc4d1	přímý
dotaz	dotaz	k1gInSc4	dotaz
<g/>
,	,	kIx,	,
kolik	kolik	k4yRc4	kolik
má	mít	k5eAaImIp3nS	mít
zhruba	zhruba	k6eAd1	zhruba
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
osobním	osobní	k2eAgInSc6d1	osobní
účtu	účet	k1gInSc6	účet
<g/>
,	,	kIx,	,
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kč	Kč	kA	Kč
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
celoživotní	celoživotní	k2eAgFnPc1d1	celoživotní
úspory	úspora	k1gFnPc1	úspora
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
začal	začít	k5eAaPmAgMnS	začít
ukládat	ukládat	k5eAaImF	ukládat
na	na	k7c4	na
vkladní	vkladní	k2eAgFnSc4d1	vkladní
knížku	knížka	k1gFnSc4	knížka
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
současné	současný	k2eAgFnSc2d1	současná
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mně	já	k3xPp1nSc3	já
tam	tam	k6eAd1	tam
kumulujou	kumulovat	k5eAaImIp3nP	kumulovat
prostředky	prostředek	k1gInPc4	prostředek
z	z	k7c2	z
platu	plat	k1gInSc2	plat
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
podotýkám	podotýkat	k5eAaImIp1nS	podotýkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
třetinu	třetina	k1gFnSc4	třetina
tohoto	tento	k3xDgInSc2	tento
platu	plat	k1gInSc2	plat
dávám	dávat	k5eAaImIp1nS	dávat
na	na	k7c4	na
Fond	fond	k1gInSc4	fond
ohrožených	ohrožený	k2eAgFnPc2d1	ohrožená
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
fondu	fond	k1gInSc6	fond
nashromáždily	nashromáždit	k5eAaPmAgInP	nashromáždit
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
různých	různý	k2eAgMnPc2d1	různý
přispěvatelů	přispěvatel	k1gMnPc2	přispěvatel
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
přes	přes	k7c4	přes
3	[number]	k4	3
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2019	[number]	k4	2019
koupil	koupit	k5eAaPmAgInS	koupit
za	za	k7c4	za
2,5	[number]	k4	2,5
milionu	milion	k4xCgInSc2	milion
korun	koruna	k1gFnPc2	koruna
pozemek	pozemka	k1gFnPc2	pozemka
v	v	k7c6	v
Lánech	lán	k1gInPc6	lán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Politická	politický	k2eAgFnSc1d1	politická
činnost	činnost	k1gFnSc1	činnost
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1989	[number]	k4	1989
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
pražského	pražský	k2eAgNnSc2d1	Pražské
jara	jaro	k1gNnSc2	jaro
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
byl	být	k5eAaImAgInS	být
dle	dle	k7c2	dle
svých	svůj	k3xOyFgNnPc2	svůj
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
kvůli	kvůli	k7c3	kvůli
nesouhlasu	nesouhlas	k1gInSc3	nesouhlas
s	s	k7c7	s
okupací	okupace	k1gFnSc7	okupace
vojsk	vojsko	k1gNnPc2	vojsko
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1971	[number]	k4	1971
až	až	k9	až
1984	[number]	k4	1984
pracoval	pracovat	k5eAaImAgInS	pracovat
v	v	k7c6	v
tělovýchovném	tělovýchovný	k2eAgInSc6d1	tělovýchovný
podniku	podnik	k1gInSc6	podnik
Sportpropag	Sportpropaga	k1gFnPc2	Sportpropaga
v	v	k7c6	v
oddělení	oddělení	k1gNnSc6	oddělení
komplexního	komplexní	k2eAgNnSc2d1	komplexní
modelování	modelování	k1gNnSc2	modelování
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
přednášek	přednáška	k1gFnPc2	přednáška
Vědeckotechnické	vědeckotechnický	k2eAgFnSc2d1	vědeckotechnická
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
zaměstnán	zaměstnat	k5eAaPmNgMnS	zaměstnat
v	v	k7c6	v
podniku	podnik	k1gInSc6	podnik
Agrodat	Agrodat	k1gFnSc2	Agrodat
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
simulačními	simulační	k2eAgMnPc7d1	simulační
modely	model	k1gInPc7	model
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
31.	[number]	k4	31.
ledna	leden	k1gInSc2	leden
1984	[number]	k4	1984
vedla	vést	k5eAaImAgFnS	vést
Správa	správa	k1gFnSc1	správa
kontrarozvědky	kontrarozvědka	k1gFnSc2	kontrarozvědka
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
ekonomiky	ekonomika	k1gFnSc2	ekonomika
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc1	součást
Státní	státní	k2eAgFnSc2d1	státní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
)	)	kIx)	)
Miloše	Miloš	k1gMnSc4	Miloš
Zemana	Zeman	k1gMnSc4	Zeman
jako	jako	k8xC	jako
prověřovanou	prověřovaný	k2eAgFnSc4d1	prověřovaná
osobu	osoba	k1gFnSc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
bylo	být	k5eAaImAgNnS	být
důvodem	důvod	k1gInSc7	důvod
sledování	sledování	k1gNnSc1	sledování
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
byl	být	k5eAaImAgInS	být
skartován	skartovat	k5eAaBmNgInS	skartovat
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
prosince	prosinec	k1gInSc2	prosinec
roku	rok	k1gInSc2	rok
1989.	[number]	k4	1989.
<g/>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1989	[number]	k4	1989
vyšel	vyjít	k5eAaPmAgInS	vyjít
jeho	on	k3xPp3gInSc4	on
článek	článek	k1gInSc4	článek
Prognostika	prognostika	k1gFnSc1	prognostika
a	a	k8xC	a
přestavba	přestavba	k1gFnSc1	přestavba
v	v	k7c6	v
Technickém	technický	k2eAgInSc6d1	technický
magazínu	magazín	k1gInSc6	magazín
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vědeckých	vědecký	k2eAgInPc2d1	vědecký
poznatků	poznatek	k1gInPc2	poznatek
kritizoval	kritizovat	k5eAaImAgInS	kritizovat
tehdejší	tehdejší	k2eAgInSc1d1	tehdejší
režim	režim	k1gInSc1	režim
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc4	jeho
účinek	účinek	k1gInSc4	účinek
umocnily	umocnit	k5eAaPmAgFnP	umocnit
provokativní	provokativní	k2eAgFnPc1d1	provokativní
kresby	kresba	k1gFnPc1	kresba
ilustrátora	ilustrátor	k1gMnSc2	ilustrátor
Pavla	Pavel	k1gMnSc2	Pavel
Matušky	Matuška	k1gMnSc2	Matuška
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
článek	článek	k1gInSc4	článek
byl	být	k5eAaImAgInS	být
propuštěn	propustit	k5eAaPmNgInS	propustit
z	z	k7c2	z
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
v	v	k7c6	v
Agrodatu	Agrodat	k1gInSc6	Agrodat
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
vznikal	vznikat	k5eAaImAgInS	vznikat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
původně	původně	k6eAd1	původně
pro	pro	k7c4	pro
časopis	časopis	k1gInSc4	časopis
Vesmír	vesmír	k1gInSc1	vesmír
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
redaktor	redaktor	k1gMnSc1	redaktor
ho	on	k3xPp3gMnSc4	on
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
publikovat	publikovat	k5eAaBmF	publikovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
srpnu	srpen	k1gInSc6	srpen
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
článek	článek	k1gInSc1	článek
v	v	k7c6	v
Rudém	rudý	k2eAgNnSc6d1	Rudé
právu	právo	k1gNnSc6	právo
zkritizován	zkritizovat	k5eAaPmNgMnS	zkritizovat
a	a	k8xC	a
Zeman	Zeman	k1gMnSc1	Zeman
byl	být	k5eAaImAgMnS	být
nařknut	nařknout	k5eAaPmNgMnS	nařknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
manipuloval	manipulovat	k5eAaImAgMnS	manipulovat
se	s	k7c7	s
statistickými	statistický	k2eAgNnPc7d1	statistické
daty	datum	k1gNnPc7	datum
<g/>
.	.	kIx.	.
</s>
<s>
Reagoval	reagovat	k5eAaBmAgMnS	reagovat
žalobou	žaloba	k1gFnSc7	žaloba
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
za	za	k7c2	za
něj	on	k3xPp3gNnSc2	on
podal	podat	k5eAaPmAgMnS	podat
Pavel	Pavel	k1gMnSc1	Pavel
Rychetský	Rychetský	k1gMnSc1	Rychetský
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
spor	spor	k1gInSc1	spor
byl	být	k5eAaImAgInS	být
dořešen	dořešit	k5eAaPmNgInS	dořešit
až	až	k9	až
po	po	k7c6	po
Sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
tištěnou	tištěný	k2eAgFnSc7d1	tištěná
omluvou	omluva	k1gFnSc7	omluva
šéfredaktora	šéfredaktor	k1gMnSc2	šéfredaktor
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Hořeního	hoření	k2eAgMnSc2d1	hoření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
s	s	k7c7	s
probíhající	probíhající	k2eAgFnSc7d1	probíhající
přestavbou	přestavba	k1gFnSc7	přestavba
dostává	dostávat	k5eAaImIp3nS	dostávat
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
skutečně	skutečně	k6eAd1	skutečně
prognostika	prognostika	k1gFnSc1	prognostika
poprvé	poprvé	k6eAd1	poprvé
normální	normální	k2eAgFnSc2d1	normální
podmínky	podmínka	k1gFnSc2	podmínka
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
existenci	existence	k1gFnSc4	existence
<g/>
.	.	kIx.	.
</s>
<s>
Mizí	mizet	k5eAaImIp3nP	mizet
nadšené	nadšený	k2eAgInPc4d1	nadšený
chvalozpěvy	chvalozpěv	k1gInPc4	chvalozpěv
na	na	k7c4	na
světlou	světlý	k2eAgFnSc4d1	světlá
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nám	my	k3xPp1nPc3	my
bude	být	k5eAaImBp3nS	být
darována	darovat	k5eAaPmNgFnS	darovat
díky	díky	k7c3	díky
moudrému	moudrý	k2eAgNnSc3d1	moudré
a	a	k8xC	a
neomylnému	omylný	k2eNgNnSc3d1	neomylné
vedení	vedení	k1gNnSc3	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
střízlivý	střízlivý	k2eAgInSc4d1	střízlivý
rozbor	rozbor	k1gInSc4	rozbor
budoucích	budoucí	k2eAgFnPc2d1	budoucí
příležitostí	příležitost	k1gFnPc2	příležitost
a	a	k8xC	a
rizik	riziko	k1gNnPc2	riziko
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgNnSc6	který
často	často	k6eAd1	často
s	s	k7c7	s
hrůzou	hrůza	k1gFnSc7	hrůza
zjišťujeme	zjišťovat	k5eAaImIp1nP	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejvýraznější	výrazný	k2eAgNnPc1d3	nejvýraznější
rizika	riziko	k1gNnPc1	riziko
mohou	moct	k5eAaImIp3nP	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
jako	jako	k8xC	jako
důsledek	důsledek	k1gInSc1	důsledek
nekvalifikovaných	kvalifikovaný	k2eNgMnPc2d1	nekvalifikovaný
(	(	kIx(	(
<g/>
ať	ať	k8xS	ať
již	již	k6eAd1	již
uskutečněných	uskutečněný	k2eAgInPc2d1	uskutečněný
nebo	nebo	k8xC	nebo
projektovaných	projektovaný	k2eAgInPc2d1	projektovaný
<g/>
)	)	kIx)	)
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
tohoto	tento	k3xDgNnSc2	tento
vedení	vedení	k1gNnSc2	vedení
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
–	–	k?	–
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
Prognostika	prognostika	k1gFnSc1	prognostika
a	a	k8xC	a
přestavba	přestavba	k1gFnSc1	přestavba
<g/>
,	,	kIx,	,
srpen	srpen	k1gInSc1	srpen
1989Po	1989Po	k6eAd1	1989Po
otištění	otištění	k1gNnSc4	otištění
článku	článek	k1gInSc2	článek
byl	být	k5eAaImAgMnS	být
Janem	Jan	k1gMnSc7	Jan
Martinkem	Martinek	k1gMnSc7	Martinek
<g/>
,	,	kIx,	,
vedoucím	vedoucí	k2eAgMnSc7d1	vedoucí
redaktorem	redaktor	k1gMnSc7	redaktor
Hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
zápisníku	zápisník	k1gInSc2	zápisník
Československé	československý	k2eAgFnSc2d1	Československá
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
pozván	pozvat	k5eAaPmNgMnS	pozvat
do	do	k7c2	do
uvedeného	uvedený	k2eAgInSc2d1	uvedený
pořadu	pořad	k1gInSc2	pořad
jako	jako	k8xC	jako
host	host	k1gMnSc1	host
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
Zeman	Zeman	k1gMnSc1	Zeman
otevřeně	otevřeně	k6eAd1	otevřeně
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
tehdejší	tehdejší	k2eAgInSc4d1	tehdejší
režim	režim	k1gInSc4	režim
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
záznam	záznam	k1gInSc1	záznam
nakonec	nakonec	k6eAd1	nakonec
odvysílán	odvysílat	k5eAaPmNgInS	odvysílat
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
mj.	mj.	kA	mj.
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Asi	asi	k9	asi
vás	vy	k3xPp2nPc4	vy
nepotěším	potěšit	k5eNaPmIp1nS	potěšit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
čtyřicet	čtyřicet	k4xCc4	čtyřicet
let	léto	k1gNnPc2	léto
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
z	z	k7c2	z
desátého	desátý	k4xOgNnSc2	desátý
místa	místo	k1gNnSc2	místo
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
na	na	k7c4	na
čtyřicáté	čtyřicátý	k4xOgNnSc4	čtyřicátý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ještě	ještě	k6eAd1	ještě
horší	zlý	k2eAgMnSc1d2	horší
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
ve	v	k7c6	v
vědeckotechnickém	vědeckotechnický	k2eAgInSc6d1	vědeckotechnický
rozvoji	rozvoj	k1gInSc6	rozvoj
podle	podle	k7c2	podle
oficiálně	oficiálně	k6eAd1	oficiálně
publikovaných	publikovaný	k2eAgFnPc2d1	publikovaná
informací	informace	k1gFnPc2	informace
jsme	být	k5eAaImIp1nP	být
dnes	dnes	k6eAd1	dnes
zhruba	zhruba	k6eAd1	zhruba
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
nebo	nebo	k8xC	nebo
Peru	Peru	k1gNnSc2	Peru
a	a	k8xC	a
hluboko	hluboko	k6eAd1	hluboko
pod	pod	k7c7	pod
Portugalskem	Portugalsko	k1gNnSc7	Portugalsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
nejzaostalejší	zaostalý	k2eAgFnSc4d3	nejzaostalejší
zemi	zem	k1gFnSc4	zem
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Já	já	k3xPp1nSc1	já
sám	sám	k3xTgMnSc1	sám
si	se	k3xPyFc3	se
tento	tento	k3xDgInSc4	tento
vývoj	vývoj	k1gInSc4	vývoj
vysvětluji	vysvětlovat	k5eAaImIp1nS	vysvětlovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsme	být	k5eAaImIp1nP	být
ekonomická	ekonomický	k2eAgNnPc4d1	ekonomické
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
<g />
.	.	kIx.	.
</s>
<s>
přijímali	přijímat	k5eAaImAgMnP	přijímat
nepromyšleně	promyšleně	k6eNd1	promyšleně
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
nepromyšlenost	nepromyšlenost	k1gFnSc1	nepromyšlenost
vyplývala	vyplývat	k5eAaImAgFnS	vyplývat
především	především	k9	především
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
neexistovala	existovat	k5eNaImAgFnS	existovat
zdravá	zdravý	k2eAgFnSc1d1	zdravá
soutěž	soutěž	k1gFnSc1	soutěž
alternativních	alternativní	k2eAgNnPc2d1	alternativní
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
a	a	k8xC	a
že	že	k8xS	že
i	i	k9	i
rozhodnutí	rozhodnutí	k1gNnPc1	rozhodnutí
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
velice	velice	k6eAd1	velice
varovné	varovný	k2eAgInPc4d1	varovný
důsledky	důsledek	k1gInPc4	důsledek
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
přijímána	přijímat	k5eAaImNgNnP	přijímat
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
existovala	existovat	k5eAaImAgFnS	existovat
jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
společenská	společenský	k2eAgFnSc1d1	společenská
kontrola	kontrola	k1gFnSc1	kontrola
jejich	jejich	k3xOp3gFnSc2	jejich
efektivity	efektivita	k1gFnSc2	efektivita
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Nikde	nikde	k6eAd1	nikde
není	být	k5eNaImIp3nS	být
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
bez	bez	k7c2	bez
změny	změna	k1gFnSc2	změna
řízení	řízení	k1gNnSc2	řízení
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
skutečné	skutečný	k2eAgFnSc2d1	skutečná
přestavby	přestavba	k1gFnSc2	přestavba
by	by	kYmCp3nS	by
u	u	k7c2	u
nás	my	k3xPp1nPc4	my
ve	v	k7c6	v
velice	velice	k6eAd1	velice
dohledné	dohledný	k2eAgFnSc6d1	dohledná
budoucnosti	budoucnost	k1gFnSc6	budoucnost
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
prázdným	prázdný	k2eAgInPc3d1	prázdný
obchodům	obchod	k1gInPc3	obchod
<g/>
,	,	kIx,	,
inflaci	inflace	k1gFnSc3	inflace
<g/>
,	,	kIx,	,
stávkám	stávka	k1gFnPc3	stávka
<g/>
,	,	kIx,	,
manifestacím	manifestace	k1gFnPc3	manifestace
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
všechno	všechen	k3xTgNnSc1	všechen
jsou	být	k5eAaImIp3nP	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
nikdo	nikdo	k3yNnSc1	nikdo
nechce	chtít	k5eNaImIp3nS	chtít
slyšet	slyšet	k5eAaImF	slyšet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prognostik	prognostik	k1gMnSc1	prognostik
je	být	k5eAaImIp3nS	být
tady	tady	k6eAd1	tady
právě	právě	k9	právě
od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
varoval	varovat	k5eAaImAgMnS	varovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Sametová	sametový	k2eAgFnSc1d1	sametová
revoluce	revoluce	k1gFnSc1	revoluce
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
17.	[number]	k4	17.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
a	a	k8xC	a
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
dnech	den	k1gInPc6	den
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
pražských	pražský	k2eAgFnPc2d1	Pražská
demonstrací	demonstrace	k1gFnPc2	demonstrace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc4	jeho
přímou	přímý	k2eAgFnSc4d1	přímá
účast	účast	k1gFnSc4	účast
na	na	k7c4	na
demonstraci	demonstrace	k1gFnSc4	demonstrace
na	na	k7c6	na
Národní	národní	k2eAgFnSc6d1	národní
třídě	třída	k1gFnSc6	třída
17.	[number]	k4	17.
listopadu	listopad	k1gInSc2	listopad
někteří	některý	k3yIgMnPc1	některý
vyvrací	vyvracet	k5eAaImIp3nP	vyvracet
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
prognostik	prognostik	k1gMnSc1	prognostik
předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
pád	pád	k1gInSc4	pád
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
v	v	k7c6	v
termínu	termín	k1gInSc6	termín
jaro	jaro	k1gNnSc1	jaro
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
svým	svůj	k3xOyFgInSc7	svůj
článkem	článek	k1gInSc7	článek
s	s	k7c7	s
názvem	název	k1gInSc7	název
Prognostika	prognostika	k1gFnSc1	prognostika
a	a	k8xC	a
přestavba	přestavba	k1gFnSc1	přestavba
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
sepsal	sepsat	k5eAaPmAgMnS	sepsat
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
publikoval	publikovat	k5eAaBmAgMnS	publikovat
jej	on	k3xPp3gMnSc4	on
až	až	k8xS	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
v	v	k7c6	v
Technickém	technický	k2eAgInSc6d1	technický
magazínu	magazín	k1gInSc6	magazín
<g/>
,	,	kIx,	,
možná	možná	k9	možná
aktivně	aktivně	k6eAd1	aktivně
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
Sametové	sametový	k2eAgFnSc3d1	sametová
revoluci	revoluce	k1gFnSc3	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
událostí	událost	k1gFnPc2	událost
z	z	k7c2	z
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
činnosti	činnost	k1gFnSc2	činnost
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
ho	on	k3xPp3gMnSc4	on
přivedl	přivést	k5eAaPmAgMnS	přivést
novinář	novinář	k1gMnSc1	novinář
Svobodného	svobodný	k2eAgNnSc2d1	svobodné
slova	slovo	k1gNnSc2	slovo
Petr	Petr	k1gMnSc1	Petr
Kučera	Kučera	k1gMnSc1	Kučera
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
krátký	krátký	k2eAgInSc1d1	krátký
projev	projev	k1gInSc1	projev
na	na	k7c6	na
Letenské	letenský	k2eAgFnSc6d1	Letenská
pláni	pláň	k1gFnSc6	pláň
25.	[number]	k4	25.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
srovnával	srovnávat	k5eAaImAgInS	srovnávat
životní	životní	k2eAgFnSc4d1	životní
úroveň	úroveň	k1gFnSc4	úroveň
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
např.	např.	kA	např.
s	s	k7c7	s
africkými	africký	k2eAgInPc7d1	africký
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
u	u	k7c2	u
účastníků	účastník	k1gMnPc2	účastník
obrovský	obrovský	k2eAgInSc4d1	obrovský
ohlas	ohlas	k1gInSc4	ohlas
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
přímých	přímý	k2eAgMnPc2d1	přímý
účastníků	účastník	k1gMnPc2	účastník
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
jediná	jediný	k2eAgFnSc1d1	jediná
akce	akce	k1gFnSc1	akce
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
na	na	k7c6	na
činnosti	činnost	k1gFnSc6	činnost
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
podílel	podílet	k5eAaImAgMnS	podílet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Poslanec	poslanec	k1gMnSc1	poslanec
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1990	[number]	k4	1990
zasedl	zasednout	k5eAaPmAgMnS	zasednout
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
procesu	proces	k1gInSc2	proces
kooptací	kooptace	k1gFnPc2	kooptace
do	do	k7c2	do
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
jako	jako	k9	jako
bezpartijní	bezpartijní	k1gMnSc1	bezpartijní
poslanec	poslanec	k1gMnSc1	poslanec
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
poslanec	poslanec	k1gMnSc1	poslanec
za	za	k7c4	za
OF	OF	kA	OF
<g/>
,	,	kIx,	,
do	do	k7c2	do
české	český	k2eAgFnSc2d1	Česká
části	část	k1gFnSc2	část
Sněmovny	sněmovna	k1gFnSc2	sněmovna
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
volební	volební	k2eAgInSc1d1	volební
obvod	obvod	k1gInSc1	obvod
č	č	k0	č
<g/>
.	.	kIx.	.
3	[number]	k4	3
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
3	[number]	k4	3
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mandát	mandát	k1gInSc1	mandát
obhájil	obhájit	k5eAaPmAgInS	obhájit
za	za	k7c4	za
OF	OF	kA	OF
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozkladu	rozklad	k1gInSc6	rozklad
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
přešel	přejít	k5eAaPmAgInS	přejít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
do	do	k7c2	do
Občanského	občanský	k2eAgNnSc2d1	občanské
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
1992	[number]	k4	1992
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
klubu	klub	k1gInSc2	klub
Klub	klub	k1gInSc1	klub
Sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgFnSc2d1	demokratická
orientace	orientace	k1gFnSc2	orientace
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
předsedy	předseda	k1gMnSc2	předseda
rozpočtového	rozpočtový	k2eAgInSc2d1	rozpočtový
výboru	výbor	k1gInSc2	výbor
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
v	v	k7c6	v
letech	let	k1gInPc6	let
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
vědecký	vědecký	k2eAgMnSc1d1	vědecký
pracovník	pracovník	k1gMnSc1	pracovník
Prognostického	prognostický	k2eAgInSc2d1	prognostický
ústavu	ústav	k1gInSc2	ústav
ČSAV	ČSAV	kA	ČSAV
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yIgInSc2	který
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
rovněž	rovněž	k9	rovněž
spoluzakládal	spoluzakládat	k5eAaImAgInS	spoluzakládat
československou	československý	k2eAgFnSc4d1	Československá
pobočku	pobočka	k1gFnSc4	pobočka
Římského	římský	k2eAgInSc2d1	římský
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
poslanec	poslanec	k1gMnSc1	poslanec
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
vysídlením	vysídlení	k1gNnSc7	vysídlení
Němců	Němec	k1gMnPc2	Němec
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
porušení	porušení	k1gNnSc4	porušení
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
přirovnal	přirovnat	k5eAaPmAgInS	přirovnat
ho	on	k3xPp3gMnSc4	on
ke	k	k7c3	k
stalinským	stalinský	k2eAgFnPc3d1	stalinská
nuceným	nucený	k2eAgFnPc3d1	nucená
migracím	migrace	k1gFnPc3	migrace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
1990	[number]	k4	1990
je	být	k5eAaImIp3nS	být
profesně	profesně	k6eAd1	profesně
uváděn	uvádět	k5eAaImNgMnS	uvádět
jako	jako	k8xS	jako
vědecký	vědecký	k2eAgMnSc1d1	vědecký
pracovník	pracovník	k1gMnSc1	pracovník
Prognostického	prognostický	k2eAgInSc2d1	prognostický
ústavu	ústav	k1gInSc2	ústav
ČSAV	ČSAV	kA	ČSAV
<g/>
,	,	kIx,	,
bytem	byt	k1gInSc7	byt
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
přešel	přejít	k5eAaPmAgMnS	přejít
do	do	k7c2	do
Sněmovny	sněmovna	k1gFnSc2	sněmovna
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
jako	jako	k9	jako
poslanec	poslanec	k1gMnSc1	poslanec
za	za	k7c4	za
Českou	český	k2eAgFnSc4d1	Česká
stranu	strana	k1gFnSc4	strana
sociálně	sociálně	k6eAd1	sociálně
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
(	(	kIx(	(
<g/>
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
volební	volební	k2eAgInSc1d1	volební
obvod	obvod	k1gInSc1	obvod
Jihočeský	jihočeský	k2eAgInSc1d1	jihočeský
kraj	kraj	k1gInSc1	kraj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Špidly	Špidla	k1gMnSc2	Špidla
byl	být	k5eAaImAgMnS	být
iniciátorem	iniciátor	k1gMnSc7	iniciátor
zapojení	zapojení	k1gNnSc2	zapojení
Miloše	Miloš	k1gMnSc4	Miloš
Zemana	Zeman	k1gMnSc4	Zeman
do	do	k7c2	do
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
na	na	k7c4	na
kandidátku	kandidátka	k1gFnSc4	kandidátka
této	tento	k3xDgFnSc2	tento
strany	strana	k1gFnSc2	strana
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
jihočeský	jihočeský	k2eAgMnSc1d1	jihočeský
politik	politik	k1gMnSc1	politik
Václav	Václav	k1gMnSc1	Václav
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Kandidátce	kandidátka	k1gFnSc3	kandidátka
ČSSD	ČSSD	kA	ČSSD
totiž	totiž	k8xC	totiž
tehdy	tehdy	k6eAd1	tehdy
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
Čechách	Čechy	k1gFnPc6	Čechy
prý	prý	k9	prý
chyběla	chybět	k5eAaImAgFnS	chybět
výrazná	výrazný	k2eAgFnSc1d1	výrazná
osobnost	osobnost	k1gFnSc1	osobnost
a	a	k8xC	a
Svoboda	Svoboda	k1gMnSc1	Svoboda
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
oslovit	oslovit	k5eAaPmF	oslovit
tehdy	tehdy	k6eAd1	tehdy
politicky	politicky	k6eAd1	politicky
nezařazeného	zařazený	k2eNgMnSc4d1	nezařazený
Miloše	Miloš	k1gMnSc4	Miloš
Zemana	Zeman	k1gMnSc4	Zeman
<g/>
.	.	kIx.	.
</s>
<s>
Emisaři	emisar	k1gMnPc1	emisar
ČSSD	ČSSD	kA	ČSSD
náhodou	náhodou	k6eAd1	náhodou
dorazili	dorazit	k5eAaPmAgMnP	dorazit
za	za	k7c7	za
Zemanem	Zeman	k1gMnSc7	Zeman
jen	jen	k9	jen
o	o	k7c4	o
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
se	se	k3xPyFc4	se
stejnou	stejný	k2eAgFnSc7d1	stejná
nabídkou	nabídka	k1gFnSc7	nabídka
objevili	objevit	k5eAaPmAgMnP	objevit
i	i	k9	i
vyslanci	vyslanec	k1gMnPc1	vyslanec
dalšího	další	k2eAgInSc2d1	další
politického	politický	k2eAgInSc2d1	politický
subjektu	subjekt	k1gInSc2	subjekt
<g/>
,	,	kIx,	,
Liberálně	liberálně	k6eAd1	liberálně
sociální	sociální	k2eAgFnSc2d1	sociální
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Zeman	Zeman	k1gMnSc1	Zeman
dal	dát	k5eAaPmAgMnS	dát
přednost	přednost	k1gFnSc4	přednost
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
formaci	formace	k1gFnSc6	formace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Federálním	federální	k2eAgNnSc6d1	federální
shromáždění	shromáždění	k1gNnSc6	shromáždění
setrval	setrvat	k5eAaPmAgMnS	setrvat
do	do	k7c2	do
zániku	zánik	k1gInSc2	zánik
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Federálním	federální	k2eAgNnSc6d1	federální
shromáždění	shromáždění	k1gNnSc6	shromáždění
se	se	k3xPyFc4	se
profiloval	profilovat	k5eAaImAgMnS	profilovat
jako	jako	k9	jako
zastánce	zastánce	k1gMnSc4	zastánce
udržení	udržení	k1gNnSc2	udržení
společného	společný	k2eAgInSc2d1	společný
státu	stát	k1gInSc2	stát
a	a	k8xC	a
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
vznik	vznik	k1gInSc4	vznik
česko-slovenské	českolovenský	k2eAgFnSc2d1	česko-slovenská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
návrh	návrh	k1gInSc1	návrh
původně	původně	k6eAd1	původně
schválen	schválit	k5eAaPmNgInS	schválit
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgMnS	být
pak	pak	k6eAd1	pak
dále	daleko	k6eAd2	daleko
rozvinut	rozvinut	k2eAgInSc1d1	rozvinut
<g/>
.	.	kIx.	.
</s>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
za	za	k7c4	za
demokratické	demokratický	k2eAgNnSc4d1	demokratické
Slovensko	Slovensko	k1gNnSc4	Slovensko
(	(	kIx(	(
<g/>
HZDS	HZDS	kA	HZDS
<g/>
)	)	kIx)	)
myšlenku	myšlenka	k1gFnSc4	myšlenka
unie	unie	k1gFnSc2	unie
podpořilo	podpořit	k5eAaPmAgNnS	podpořit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
jejího	její	k3xOp3gMnSc2	její
koaličního	koaliční	k2eAgMnSc2d1	koaliční
partnera	partner	k1gMnSc2	partner
na	na	k7c6	na
federální	federální	k2eAgFnSc6d1	federální
úrovni	úroveň	k1gFnSc6	úroveň
ODS	ODS	kA	ODS
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
podpory	podpora	k1gFnSc2	podpora
později	pozdě	k6eAd2	pozdě
ustoupilo	ustoupit	k5eAaPmAgNnS	ustoupit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Předseda	předseda	k1gMnSc1	předseda
ČSSD	ČSSD	kA	ČSSD
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
Zeman	Zeman	k1gMnSc1	Zeman
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
městské	městský	k2eAgFnSc2d1	městská
stranické	stranický	k2eAgFnSc2d1	stranická
organizace	organizace	k1gFnSc2	organizace
ČSSD	ČSSD	kA	ČSSD
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
i	i	k9	i
předsedou	předseda	k1gMnSc7	předseda
celé	celý	k2eAgFnSc2d1	celá
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
předsedy	předseda	k1gMnSc2	předseda
ČSSD	ČSSD	kA	ČSSD
působil	působit	k5eAaImAgMnS	působit
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
politické	politický	k2eAgFnSc3d1	politická
straně	strana	k1gFnSc3	strana
s	s	k7c7	s
dlouholetou	dlouholetý	k2eAgFnSc7d1	dlouholetá
tradicí	tradice	k1gFnSc7	tradice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
byla	být	k5eAaImAgFnS	být
jen	jen	k9	jen
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
politických	politický	k2eAgInPc2d1	politický
subjektů	subjekt	k1gInPc2	subjekt
<g/>
,	,	kIx,	,
dopomohl	dopomoct	k5eAaPmAgMnS	dopomoct
Zeman	Zeman	k1gMnSc1	Zeman
během	během	k7c2	během
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
nejprve	nejprve	k6eAd1	nejprve
k	k	k7c3	k
pozici	pozice	k1gFnSc3	pozice
nejsilnější	silný	k2eAgFnSc2d3	nejsilnější
opoziční	opoziční	k2eAgFnSc2d1	opoziční
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
k	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Předseda	předseda	k1gMnSc1	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
===	===	k?	===
</s>
</p>
<p>
<s>
Poslancem	poslanec	k1gMnSc7	poslanec
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
stal	stát	k5eAaPmAgInS	stát
až	až	k9	až
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
české	český	k2eAgFnSc2d1	Česká
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
předvolební	předvolební	k2eAgFnSc2d1	předvolební
kampaně	kampaň	k1gFnSc2	kampaň
projížděl	projíždět	k5eAaImAgInS	projíždět
republikou	republika	k1gFnSc7	republika
v	v	k7c6	v
autobuse	autobus	k1gInSc6	autobus
Zemák	zemák	k1gInSc1	zemák
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgInS	být
nápad	nápad	k1gInSc1	nápad
jeho	jeho	k3xOp3gMnSc4	jeho
přítele	přítel	k1gMnSc4	přítel
Miroslava	Miroslav	k1gMnSc4	Miroslav
Šloufa	Šlouf	k1gMnSc4	Šlouf
<g/>
.	.	kIx.	.
</s>
<s>
ČSSD	ČSSD	kA	ČSSD
pod	pod	k7c7	pod
Zemanovým	Zemanův	k2eAgNnSc7d1	Zemanovo
vedením	vedení	k1gNnSc7	vedení
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
historického	historický	k2eAgInSc2d1	historický
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
strana	strana	k1gFnSc1	strana
získala	získat	k5eAaPmAgFnS	získat
pouhých	pouhý	k2eAgNnPc6d1	pouhé
6,53	[number]	k4	6,53
%	%	kIx~	%
<g/>
,	,	kIx,	,
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
jí	jíst	k5eAaImIp3nS	jíst
zisk	zisk	k1gInSc4	zisk
26,44	[number]	k4	26,44
%	%	kIx~	%
vynesl	vynést	k5eAaPmAgMnS	vynést
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
ČSSD	ČSSD	kA	ČSSD
tak	tak	k6eAd1	tak
za	za	k7c4	za
vítěznou	vítězný	k2eAgFnSc4d1	vítězná
ODS	ODS	kA	ODS
zaostala	zaostat	k5eAaPmAgFnS	zaostat
o	o	k7c4	o
pouhá	pouhý	k2eAgNnPc4d1	pouhé
tři	tři	k4xCgNnPc4	tři
procenta	procento	k1gNnPc4	procento
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znemožnilo	znemožnit	k5eAaPmAgNnS	znemožnit
tehdejšímu	tehdejší	k2eAgMnSc3d1	tehdejší
předsedovi	předseda	k1gMnSc3	předseda
občanských	občanský	k2eAgNnPc2d1	občanské
demokratů	demokrat	k1gMnPc2	demokrat
Václavu	Václav	k1gMnSc3	Václav
Klausovi	Klaus	k1gMnSc3	Klaus
sestavit	sestavit	k5eAaPmF	sestavit
většinovou	většinový	k2eAgFnSc4d1	většinová
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
toleranci	tolerance	k1gFnSc4	tolerance
menšinové	menšinový	k2eAgFnSc2d1	menšinová
vlády	vláda	k1gFnSc2	vláda
získal	získat	k5eAaPmAgMnS	získat
křeslo	křeslo	k1gNnSc4	křeslo
předsedy	předseda	k1gMnSc2	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc2	který
zaujímal	zaujímat	k5eAaImAgMnS	zaujímat
až	až	k9	až
do	do	k7c2	do
předčasných	předčasný	k2eAgFnPc2d1	předčasná
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998.	[number]	k4	1998.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
14.	[number]	k4	14.
září	září	k1gNnSc4	září
1997	[number]	k4	1997
(	(	kIx(	(
<g/>
60	[number]	k4	60
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
úmrtí	úmrtí	k1gNnSc2	úmrtí
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
)	)	kIx)	)
obdržel	obdržet	k5eAaPmAgInS	obdržet
v	v	k7c6	v
Rudolfinu	Rudolfinum	k1gNnSc6	Rudolfinum
"	"	kIx"	"
<g/>
Čestnou	čestný	k2eAgFnSc4d1	čestná
medaili	medaile	k1gFnSc4	medaile
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
"	"	kIx"	"
Masarykova	Masarykův	k2eAgNnSc2d1	Masarykovo
demokratického	demokratický	k2eAgNnSc2d1	demokratické
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
Pithartem	Pithart	k1gMnSc7	Pithart
a	a	k8xC	a
Václavem	Václav	k1gMnSc7	Václav
Klausem	Klaus	k1gMnSc7	Klaus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
22.	[number]	k4	22.
července	červenec	k1gInSc2	červenec
1998	[number]	k4	1998
se	se	k3xPyFc4	se
po	po	k7c6	po
vítězných	vítězný	k2eAgFnPc6d1	vítězná
volbách	volba	k1gFnPc6	volba
stal	stát	k5eAaPmAgInS	stát
českým	český	k2eAgMnSc7d1	český
premiérem	premiér	k1gMnSc7	premiér
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
byl	být	k5eAaImAgMnS	být
pak	pak	k6eAd1	pak
po	po	k7c4	po
celé	celý	k2eAgNnSc4d1	celé
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
až	až	k9	až
do	do	k7c2	do
12.	[number]	k4	12.
července	červenec	k1gInSc2	červenec
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
jednobarevná	jednobarevný	k2eAgFnSc1d1	jednobarevná
menšinová	menšinový	k2eAgFnSc1d1	menšinová
vláda	vláda	k1gFnSc1	vláda
byla	být	k5eAaImAgFnS	být
tolerována	tolerovat	k5eAaImNgFnS	tolerovat
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
ODS	ODS	kA	ODS
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tzv.	tzv.	kA	tzv.
opoziční	opoziční	k2eAgFnSc2d1	opoziční
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
ve	v	k7c6	v
volebním	volební	k2eAgNnSc6d1	volební
období	období	k1gNnSc6	období
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
zasedal	zasedat	k5eAaImAgInS	zasedat
jako	jako	k9	jako
poslanec	poslanec	k1gMnSc1	poslanec
v	v	k7c6	v
Poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc6	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
oznámil	oznámit	k5eAaPmAgMnS	oznámit
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
nebude	být	k5eNaImBp3nS	být
obhajovat	obhajovat	k5eAaImF	obhajovat
pozici	pozice	k1gFnSc4	pozice
předsedy	předseda	k1gMnSc2	předseda
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
a	a	k8xC	a
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
přenechal	přenechat	k5eAaPmAgInS	přenechat
Vladimíru	Vladimír	k1gMnSc3	Vladimír
Špidlovi	Špidla	k1gMnSc3	Špidla
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
po	po	k7c6	po
dalších	další	k2eAgFnPc6d1	další
vítězných	vítězný	k2eAgFnPc6d1	vítězná
volbách	volba	k1gFnPc6	volba
ČSSD	ČSSD	kA	ČSSD
převzal	převzít	k5eAaPmAgMnS	převzít
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
i	i	k9	i
funkci	funkce	k1gFnSc4	funkce
premiéra	premiér	k1gMnSc2	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
do	do	k7c2	do
politického	politický	k2eAgNnSc2d1	politické
ústraní	ústraní	k1gNnSc2	ústraní
a	a	k8xC	a
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
politiku	politika	k1gFnSc4	politika
nadobro	nadobro	k6eAd1	nadobro
opouští	opouštět	k5eAaImIp3nS	opouštět
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
média	médium	k1gNnPc1	médium
i	i	k8xC	i
část	část	k1gFnSc1	část
politické	politický	k2eAgFnSc2d1	politická
scény	scéna	k1gFnSc2	scéna
předpovídaly	předpovídat	k5eAaImAgFnP	předpovídat
jeho	jeho	k3xOp3gInSc4	jeho
budoucí	budoucí	k2eAgInSc4d1	budoucí
návrat	návrat	k1gInSc4	návrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
Zemanova	Zemanův	k2eAgFnSc1d1	Zemanova
vláda	vláda	k1gFnSc1	vláda
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
o	o	k7c6	o
zahájení	zahájení	k1gNnSc6	zahájení
privatizace	privatizace	k1gFnSc2	privatizace
majoritního	majoritní	k2eAgInSc2d1	majoritní
státního	státní	k2eAgInSc2d1	státní
podílu	podíl	k1gInSc2	podíl
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
Unipetrol	Unipetrola	k1gFnPc2	Unipetrola
<g/>
.	.	kIx.	.
</s>
<s>
Parametry	parametr	k1gInPc1	parametr
privatizace	privatizace	k1gFnSc2	privatizace
představovaly	představovat	k5eAaImAgInP	představovat
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
cena	cena	k1gFnSc1	cena
a	a	k8xC	a
strategický	strategický	k2eAgInSc1d1	strategický
zájem	zájem	k1gInSc1	zájem
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
mezirezortní	mezirezortní	k2eAgFnSc1d1	mezirezortní
privatizační	privatizační	k2eAgFnSc1d1	privatizační
komise	komise	k1gFnSc1	komise
doporučila	doporučit	k5eAaPmAgFnS	doporučit
jako	jako	k9	jako
vítěze	vítěz	k1gMnSc4	vítěz
britskou	britský	k2eAgFnSc4d1	britská
společnost	společnost	k1gFnSc4	společnost
Rotch	Rotch	k1gMnSc1	Rotch
Energy	Energ	k1gInPc4	Energ
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
444	[number]	k4	444
miliónů	milión	k4xCgInPc2	milión
eur	euro	k1gNnPc2	euro
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
asi	asi	k9	asi
14,5	[number]	k4	14,5
miliardy	miliarda	k4xCgFnSc2	miliarda
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2001	[number]	k4	2001
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
o	o	k7c6	o
prodeji	prodej	k1gInSc6	prodej
společnosti	společnost	k1gFnSc2	společnost
Agrofert	Agrofert	k1gInSc1	Agrofert
<g/>
,	,	kIx,	,
za	za	k7c4	za
361	[number]	k4	361
miliónů	milión	k4xCgInPc2	milión
eur	euro	k1gNnPc2	euro
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
asi	asi	k9	asi
11,7	[number]	k4	11,7
miliardy	miliarda	k4xCgFnSc2	miliarda
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Agrofert	Agrofert	k1gMnSc1	Agrofert
však	však	k9	však
nezaplatil	zaplatit	k5eNaPmAgMnS	zaplatit
a	a	k8xC	a
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c6	o
ukončení	ukončení	k1gNnSc6	ukončení
privatizační	privatizační	k2eAgFnSc2d1	privatizační
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2002	[number]	k4	2002
tak	tak	k6eAd1	tak
Špidlova	Špidlův	k2eAgFnSc1d1	Špidlova
vláda	vláda	k1gFnSc1	vláda
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
o	o	k7c6	o
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
nové	nový	k2eAgFnSc2d1	nová
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Agrofert	Agrofert	k1gInSc1	Agrofert
následně	následně	k6eAd1	následně
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
kontrakt	kontrakt	k1gInSc1	kontrakt
s	s	k7c7	s
PKN	PKN	kA	PKN
Orlen	Orlen	k1gInSc1	Orlen
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yIgInSc2	který
měl	mít	k5eAaImAgMnS	mít
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
společnosti	společnost	k1gFnSc2	společnost
dopomoci	dopomoc	k1gFnSc2	dopomoc
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
Unipetrolu	Unipetrol	k1gInSc2	Unipetrol
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
výhodně	výhodně	k6eAd1	výhodně
koupené	koupený	k2eAgInPc4d1	koupený
podíly	podíl	k1gInPc4	podíl
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
dceřiných	dceřin	k2eAgFnPc6d1	dceřina
chemičkách	chemička	k1gFnPc6	chemička
a	a	k8xC	a
rafinériích	rafinérie	k1gFnPc6	rafinérie
<g/>
.	.	kIx.	.
</s>
<s>
PKN	PKN	kA	PKN
Orlen	Orlen	k2eAgInSc4d1	Orlen
Unipetrol	Unipetrol	k1gInSc4	Unipetrol
za	za	k7c4	za
14,7	[number]	k4	14,7
miliardy	miliarda	k4xCgFnSc2	miliarda
získal	získat	k5eAaPmAgMnS	získat
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
ze	z	k7c2	z
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
Babišem	Babiš	k1gMnSc7	Babiš
jednostranně	jednostranně	k6eAd1	jednostranně
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
a	a	k8xC	a
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
zaplatit	zaplatit	k5eAaPmF	zaplatit
smluvní	smluvní	k2eAgFnSc4d1	smluvní
pokutu	pokuta	k1gFnSc4	pokuta
<g/>
.	.	kIx.	.
</s>
<s>
Babiš	Babiš	k1gMnSc1	Babiš
následně	následně	k6eAd1	následně
podal	podat	k5eAaPmAgMnS	podat
čtyři	čtyři	k4xCgNnPc1	čtyři
trestní	trestní	k2eAgNnPc1d1	trestní
oznámení	oznámení	k1gNnPc1	oznámení
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
soud	soud	k1gInSc1	soud
vyhověl	vyhovět	k5eAaPmAgInS	vyhovět
pouze	pouze	k6eAd1	pouze
jednomu	jeden	k4xCgNnSc3	jeden
<g/>
.	.	kIx.	.
<g/>
Zeman	Zeman	k1gMnSc1	Zeman
při	při	k7c6	při
představování	představování	k1gNnSc6	představování
volebního	volební	k2eAgInSc2d1	volební
programu	program	k1gInSc2	program
SPOZ	SPOZ	kA	SPOZ
15.	[number]	k4	15.
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pana	pan	k1gMnSc4	pan
Babiše	Babiš	k1gMnSc4	Babiš
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jsem	být	k5eAaImIp1nS	být
přímým	přímý	k2eAgMnSc7d1	přímý
účastníkem	účastník	k1gMnSc7	účastník
podpisu	podpis	k1gInSc2	podpis
dohody	dohoda	k1gFnSc2	dohoda
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
při	při	k7c6	při
privatizaci	privatizace	k1gFnSc6	privatizace
Unipetrolu	Unipetrol	k1gInSc2	Unipetrol
a	a	k8xC	a
konstatuji	konstatovat	k5eAaBmIp1nS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pan	pan	k1gMnSc1	pan
Babiš	Babiš	k1gMnSc1	Babiš
tuto	tento	k3xDgFnSc4	tento
podepsanou	podepsaný	k2eAgFnSc4d1	podepsaná
dohodu	dohoda	k1gFnSc4	dohoda
nedodržel	dodržet	k5eNaPmAgMnS	dodržet
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
mě	já	k3xPp1nSc4	já
dostatečný	dostatečný	k2eAgInSc1d1	dostatečný
důkaz	důkaz	k1gInSc1	důkaz
nevěrohodnosti	nevěrohodnost	k1gFnSc2	nevěrohodnost
pana	pan	k1gMnSc2	pan
Babiše	Babiš	k1gMnSc2	Babiš
<g/>
.	.	kIx.	.
</s>
<s>
Nehledě	hledět	k5eNaImSgMnS	hledět
na	na	k7c4	na
jistá	jistý	k2eAgNnPc4d1	jisté
další	další	k2eAgNnPc4d1	další
podezření	podezření	k1gNnPc4	podezření
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
doufám	doufat	k5eAaImIp1nS	doufat
<g/>
,	,	kIx,	,
časem	časem	k6eAd1	časem
prošetří	prošetřit	k5eAaPmIp3nS	prošetřit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Návraty	návrat	k1gInPc7	návrat
do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
===	===	k?	===
</s>
</p>
<p>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
Zeman	Zeman	k1gMnSc1	Zeman
o	o	k7c4	o
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
skutečně	skutečně	k6eAd1	skutečně
pokusil	pokusit	k5eAaPmAgMnS	pokusit
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c6	na
základě	základ	k1gInSc6	základ
svého	svůj	k3xOyFgNnSc2	svůj
vítězství	vítězství	k1gNnSc2	vítězství
ve	v	k7c6	v
vnitrostranickém	vnitrostranický	k2eAgNnSc6d1	vnitrostranické
referendu	referendum	k1gNnSc6	referendum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
na	na	k7c6	na
funkci	funkce	k1gFnSc6	funkce
prezidenta	prezident	k1gMnSc2	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
volbě	volba	k1gFnSc6	volba
prezidenta	prezident	k1gMnSc2	prezident
dne	den	k1gInSc2	den
24.	[number]	k4	24.
ledna	leden	k1gInSc2	leden
2003	[number]	k4	2003
však	však	k9	však
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
vyřazen	vyřadit	k5eAaPmNgMnS	vyřadit
již	již	k6eAd1	již
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tajné	tajný	k2eAgFnSc6d1	tajná
volbě	volba	k1gFnSc6	volba
prohrál	prohrát	k5eAaPmAgMnS	prohrát
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
s	s	k7c7	s
bývalým	bývalý	k2eAgMnSc7d1	bývalý
předsedou	předseda	k1gMnSc7	předseda
ODS	ODS	kA	ODS
Václavem	Václav	k1gMnSc7	Václav
Klausem	Klaus	k1gMnSc7	Klaus
a	a	k8xC	a
senátorkou	senátorka	k1gFnSc7	senátorka
Jaroslavou	Jaroslava	k1gFnSc7	Jaroslava
Moserovou	Moserová	k1gFnSc7	Moserová
<g/>
.	.	kIx.	.
</s>
<s>
Překvapivě	překvapivě	k6eAd1	překvapivě
nízký	nízký	k2eAgInSc1d1	nízký
počet	počet	k1gInSc1	počet
hlasů	hlas	k1gInPc2	hlas
dokazoval	dokazovat	k5eAaImAgInS	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
nevolili	volit	k5eNaImAgMnP	volit
ani	ani	k8xC	ani
někteří	některý	k3yIgMnPc1	některý
zástupci	zástupce	k1gMnPc1	zástupce
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
fiasku	fiasko	k1gNnSc6	fiasko
se	se	k3xPyFc4	se
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
a	a	k8xC	a
politické	politický	k2eAgNnSc4d1	politické
dění	dění	k1gNnSc4	dění
již	již	k6eAd1	již
pouze	pouze	k6eAd1	pouze
příležitostně	příležitostně	k6eAd1	příležitostně
glosoval	glosovat	k5eAaBmAgMnS	glosovat
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
nadále	nadále	k6eAd1	nadále
ovlivňoval	ovlivňovat	k5eAaImAgMnS	ovlivňovat
politickou	politický	k2eAgFnSc4d1	politická
scénu	scéna	k1gFnSc4	scéna
zejména	zejména	k9	zejména
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
svých	svůj	k3xOyFgMnPc2	svůj
stále	stále	k6eAd1	stále
četných	četný	k2eAgMnPc2d1	četný
příznivců	příznivec	k1gMnPc2	příznivec
v	v	k7c6	v
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Škromacha	Škromach	k1gMnSc2	Škromach
<g/>
.	.	kIx.	.
</s>
<s>
Ostře	ostro	k6eAd1	ostro
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
působení	působení	k1gNnSc4	působení
jak	jak	k8xC	jak
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Špidly	Špidla	k1gMnSc2	Špidla
(	(	kIx(	(
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
jej	on	k3xPp3gMnSc4	on
původně	původně	k6eAd1	původně
doporučil	doporučit	k5eAaPmAgMnS	doporučit
jako	jako	k8xS	jako
svého	svůj	k3xOyFgMnSc2	svůj
následovníka	následovník	k1gMnSc2	následovník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jeho	jeho	k3xOp3gMnSc4	jeho
nástupce	nástupce	k1gMnSc4	nástupce
Stanislava	Stanislav	k1gMnSc4	Stanislav
Grosse	Gross	k1gMnSc4	Gross
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
hlavního	hlavní	k2eAgMnSc4d1	hlavní
viníka	viník	k1gMnSc4	viník
svého	svůj	k3xOyFgInSc2	svůj
neúspěchu	neúspěch	k1gInSc2	neúspěch
při	při	k7c6	při
prezidentské	prezidentský	k2eAgFnSc6d1	prezidentská
kandidatuře	kandidatura	k1gFnSc6	kandidatura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Smír	smír	k1gInSc1	smír
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
až	až	k9	až
s	s	k7c7	s
dalším	další	k2eAgMnSc7d1	další
předsedou	předseda	k1gMnSc7	předseda
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
Jiřím	Jiří	k1gMnSc7	Jiří
Paroubkem	Paroubek	k1gMnSc7	Paroubek
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
Paroubek	Paroubek	k1gInSc4	Paroubek
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
patřil	patřit	k5eAaImAgMnS	patřit
mezi	mezi	k7c4	mezi
Zemanovy	Zemanův	k2eAgMnPc4d1	Zemanův
ostré	ostrý	k2eAgMnPc4d1	ostrý
názorové	názorový	k2eAgMnPc4d1	názorový
oponenty	oponent	k1gMnPc4	oponent
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
Paroubkem	Paroubek	k1gMnSc7	Paroubek
však	však	k8xC	však
výrazně	výrazně	k6eAd1	výrazně
ochladly	ochladnout	k5eAaPmAgFnP	ochladnout
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Zeman	Zeman	k1gMnSc1	Zeman
s	s	k7c7	s
Paroubkem	Paroubek	k1gInSc7	Paroubek
zásadně	zásadně	k6eAd1	zásadně
rozešli	rozejít	k5eAaPmAgMnP	rozejít
v	v	k7c6	v
názorech	názor	k1gInPc6	názor
na	na	k7c4	na
nejvhodnější	vhodný	k2eAgFnSc4d3	nejvhodnější
strategii	strategie	k1gFnSc4	strategie
povolebního	povolební	k2eAgNnSc2d1	povolební
vyjednávání	vyjednávání	k1gNnSc2	vyjednávání
<g/>
,	,	kIx,	,
zkomplikovaného	zkomplikovaný	k2eAgNnSc2d1	zkomplikované
patovou	patový	k2eAgFnSc7d1	patová
situací	situace	k1gFnSc7	situace
v	v	k7c6	v
Poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc6	sněmovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Spory	spor	k1gInPc1	spor
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Paroubkem	Paroubek	k1gMnSc7	Paroubek
se	se	k3xPyFc4	se
vyhrotily	vyhrotit	k5eAaPmAgInP	vyhrotit
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Paroubek	Paroubek	k1gInSc1	Paroubek
Zemana	Zeman	k1gMnSc2	Zeman
obvinil	obvinit	k5eAaPmAgInS	obvinit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
nevýhodnou	výhodný	k2eNgFnSc4d1	nevýhodná
mandátní	mandátní	k2eAgFnSc4d1	mandátní
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
advokátem	advokát	k1gMnSc7	advokát
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Altnerem	Altner	k1gMnSc7	Altner
<g/>
.	.	kIx.	.
</s>
<s>
Předmětem	předmět	k1gInSc7	předmět
smlouvy	smlouva	k1gFnSc2	smlouva
bylo	být	k5eAaImAgNnS	být
zastupování	zastupování	k1gNnSc1	zastupování
ČSSD	ČSSD	kA	ČSSD
ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
s	s	k7c7	s
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
financí	finance	k1gFnPc2	finance
o	o	k7c6	o
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
Lidového	lidový	k2eAgInSc2d1	lidový
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
sídla	sídlo	k1gNnSc2	sídlo
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Altner	Altner	k1gInSc1	Altner
spor	spor	k1gInSc1	spor
sice	sice	k8xC	sice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
ČSSD	ČSSD	kA	ČSSD
neuhradila	uhradit	k5eNaPmAgFnS	uhradit
odměnu	odměna	k1gFnSc4	odměna
sjednanou	sjednaný	k2eAgFnSc4d1	sjednaná
ve	v	k7c6	v
smlouvě	smlouva	k1gFnSc6	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Úroky	úrok	k1gInPc1	úrok
z	z	k7c2	z
údajně	údajně	k6eAd1	údajně
nezaplacené	zaplacený	k2eNgFnSc2d1	nezaplacená
odměny	odměna	k1gFnSc2	odměna
mezitím	mezitím	k6eAd1	mezitím
narostly	narůst	k5eAaPmAgInP	narůst
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
svou	svůj	k3xOyFgFnSc7	svůj
výší	výše	k1gFnSc7	výše
začaly	začít	k5eAaPmAgFnP	začít
ohrožovat	ohrožovat	k5eAaImF	ohrožovat
existenci	existence	k1gFnSc4	existence
ČSSD	ČSSD	kA	ČSSD
jako	jako	k8xC	jako
takové	takový	k3xDgNnSc1	takový
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
pravomocné	pravomocný	k2eAgNnSc4d1	pravomocné
uznání	uznání	k1gNnSc4	uznání
nároku	nárok	k1gInSc2	nárok
soudem	soud	k1gInSc7	soud
by	by	kYmCp3nS	by
nejspíše	nejspíše	k9	nejspíše
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
konkursu	konkurs	k1gInSc3	konkurs
a	a	k8xC	a
následnému	následný	k2eAgInSc3d1	následný
pravděpodobnému	pravděpodobný	k2eAgInSc3d1	pravděpodobný
zániku	zánik	k1gInSc3	zánik
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
mezi	mezi	k7c7	mezi
Jiřím	Jiří	k1gMnSc7	Jiří
Paroubkem	Paroubek	k1gMnSc7	Paroubek
a	a	k8xC	a
Milošem	Miloš	k1gMnSc7	Miloš
Zemanem	Zeman	k1gMnSc7	Zeman
vyvrcholil	vyvrcholit	k5eAaPmAgMnS	vyvrcholit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
na	na	k7c4	na
Miloše	Miloš	k1gMnSc4	Miloš
Zemana	Zeman	k1gMnSc4	Zeman
podal	podat	k5eAaPmAgMnS	podat
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
kauzou	kauza	k1gFnSc7	kauza
trestní	trestní	k2eAgNnSc4d1	trestní
oznámení	oznámení	k1gNnSc4	oznámení
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
pod	pod	k7c7	pod
přezdívkou	přezdívka	k1gFnSc7	přezdívka
Večerníček	večerníček	k1gInSc4	večerníček
<g/>
.	.	kIx.	.
</s>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
to	ten	k3xDgNnSc4	ten
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
skrytou	skrytý	k2eAgFnSc4d1	skrytá
aktivitu	aktivita	k1gFnSc4	aktivita
Jiřího	Jiří	k1gMnSc4	Jiří
Paroubka	Paroubek	k1gMnSc4	Paroubek
a	a	k8xC	a
21.	[number]	k4	21.
března	březen	k1gInSc2	březen
2007	[number]	k4	2007
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
z	z	k7c2	z
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
náznakem	náznak	k1gInSc7	náznak
návratu	návrat	k1gInSc2	návrat
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
bylo	být	k5eAaImAgNnS	být
otevření	otevření	k1gNnSc1	otevření
pražské	pražský	k2eAgFnSc2d1	Pražská
kanceláře	kancelář	k1gFnSc2	kancelář
jeho	jeho	k3xOp3gMnPc1	jeho
příznivci	příznivec	k1gMnPc1	příznivec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
iniciátorů	iniciátor	k1gMnPc2	iniciátor
–	–	k?	–
Miroslav	Miroslav	k1gMnSc1	Miroslav
Šlouf	Šlouf	k1gMnSc1	Šlouf
–	–	k?	–
otevřeně	otevřeně	k6eAd1	otevřeně
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
cílem	cíl	k1gInSc7	cíl
této	tento	k3xDgFnSc2	tento
akce	akce	k1gFnSc2	akce
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
představitelé	představitel	k1gMnPc1	představitel
ČSSD	ČSSD	kA	ČSSD
to	ten	k3xDgNnSc4	ten
však	však	k9	však
nepovažovali	považovat	k5eNaImAgMnP	považovat
za	za	k7c4	za
reálné	reálný	k2eAgNnSc4d1	reálné
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
vedle	vedle	k7c2	vedle
sdružení	sdružení	k1gNnSc2	sdružení
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
také	také	k9	také
Strana	strana	k1gFnSc1	strana
Práv	právo	k1gNnPc2	právo
Občanů	občan	k1gMnPc2	občan
ZEMANOVCI	zemanovec	k1gMnPc7	zemanovec
<g/>
,	,	kIx,	,
za	za	k7c7	za
kterou	který	k3yIgFnSc7	který
ohlásil	ohlásit	k5eAaPmAgMnS	ohlásit
kandidaturu	kandidatura	k1gFnSc4	kandidatura
v	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
jako	jako	k8xS	jako
lídr	lídr	k1gMnSc1	lídr
v	v	k7c6	v
Ústeckém	ústecký	k2eAgInSc6d1	ústecký
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
významným	významný	k2eAgMnSc7d1	významný
sponzorem	sponzor	k1gMnSc7	sponzor
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
zástupce	zástupce	k1gMnSc1	zástupce
ruské	ruský	k2eAgFnSc2d1	ruská
ropné	ropný	k2eAgFnSc2d1	ropná
společnosti	společnost	k1gFnSc2	společnost
Lukoil	Lukoila	k1gFnPc2	Lukoila
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
ZEMANOVCI	zemanovec	k1gMnPc1	zemanovec
nepřesáhli	přesáhnout	k5eNaPmAgMnP	přesáhnout
5	[number]	k4	5
%	%	kIx~	%
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nutné	nutný	k2eAgFnPc1d1	nutná
pro	pro	k7c4	pro
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
oznámil	oznámit	k5eAaPmAgMnS	oznámit
svou	svůj	k3xOyFgFnSc4	svůj
rezignaci	rezignace	k1gFnSc4	rezignace
na	na	k7c4	na
post	post	k1gInSc4	post
předsedy	předseda	k1gMnSc2	předseda
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
straně	strana	k1gFnSc6	strana
svou	svůj	k3xOyFgFnSc4	svůj
podporu	podpora	k1gFnSc4	podpora
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
kampaních	kampaň	k1gFnPc6	kampaň
jako	jako	k8xC	jako
například	například	k6eAd1	například
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
obcí	obec	k1gFnSc7	obec
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
13.	[number]	k4	13.
listopadu	listopad	k1gInSc2	listopad
2010	[number]	k4	2010
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
čestným	čestný	k2eAgMnSc7d1	čestný
předsedou	předseda	k1gMnSc7	předseda
SPO	SPO	kA	SPO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Prezidentské	prezidentský	k2eAgFnPc1d1	prezidentská
volby	volba	k1gFnPc1	volba
2013	[number]	k4	2013
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
kandidovat	kandidovat	k5eAaImF	kandidovat
<g/>
,	,	kIx,	,
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
Strany	strana	k1gFnSc2	strana
Práv	právo	k1gNnPc2	právo
Občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
v	v	k7c6	v
prezidentské	prezidentský	k2eAgFnSc6d1	prezidentská
volbě	volba	k1gFnSc6	volba
konané	konaný	k2eAgFnSc6d1	konaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
nasbíral	nasbírat	k5eAaPmAgMnS	nasbírat
potřebných	potřebný	k2eAgFnPc2d1	potřebná
50 000	[number]	k4	50 000
podpisů	podpis	k1gInPc2	podpis
občanů	občan	k1gMnPc2	občan
podporujících	podporující	k2eAgInPc6d1	podporující
jeho	jeho	k3xOp3gInSc6	jeho
kandidaturu	kandidatura	k1gFnSc4	kandidatura
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
předvolební	předvolební	k2eAgFnSc2d1	předvolební
kampaně	kampaň	k1gFnSc2	kampaň
pro	pro	k7c4	pro
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
volby	volba	k1gFnPc4	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
požádal	požádat	k5eAaPmAgInS	požádat
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
další	další	k2eAgMnSc1d1	další
kandidát	kandidát	k1gMnSc1	kandidát
Jiří	Jiří	k1gMnSc1	Jiří
Dienstbier	Dienstbier	k1gMnSc1	Dienstbier
<g/>
,	,	kIx,	,
o	o	k7c4	o
podporu	podpora	k1gFnSc4	podpora
vedení	vedení	k1gNnSc2	vedení
KSČM	KSČM	kA	KSČM
<g/>
.	.	kIx.	.
</s>
<s>
KSČM	KSČM	kA	KSČM
nakonec	nakonec	k6eAd1	nakonec
podpořila	podpořit	k5eAaPmAgFnS	podpořit
oba	dva	k4xCgMnPc4	dva
kandidáty	kandidát	k1gMnPc4	kandidát
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
1.	[number]	k4	1.
kole	kolo	k1gNnSc6	kolo
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
v	v	k7c6	v
termínech	termín	k1gInPc6	termín
11.	[number]	k4	11.
a	a	k8xC	a
12.	[number]	k4	12.
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
se	se	k3xPyFc4	se
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
24,21	[number]	k4	24,21
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
postoupil	postoupit	k5eAaPmAgInS	postoupit
společně	společně	k6eAd1	společně
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Schwarzenbergem	Schwarzenberg	k1gMnSc7	Schwarzenberg
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
25.	[number]	k4	25.
a	a	k8xC	a
26.	[number]	k4	26.
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
54,8	[number]	k4	54,8
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
novým	nový	k2eAgMnSc7d1	nový
zvoleným	zvolený	k2eAgMnSc7d1	zvolený
prezidentem	prezident	k1gMnSc7	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
krajích	kraj	k1gInPc6	kraj
kromě	kromě	k7c2	kromě
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
zmínek	zmínka	k1gFnPc2	zmínka
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
tisku	tisk	k1gInSc6	tisk
respektive	respektive	k9	respektive
jeho	jeho	k3xOp3gNnPc6	jeho
onlinových	onlinův	k2eAgNnPc6d1	onlinův
vydáních	vydání	k1gNnPc6	vydání
byl	být	k5eAaImAgMnS	být
zvolený	zvolený	k2eAgMnSc1d1	zvolený
prezident	prezident	k1gMnSc1	prezident
Zeman	Zeman	k1gMnSc1	Zeman
v	v	k7c6	v
zahraničních	zahraniční	k2eAgNnPc6d1	zahraniční
médiích	médium	k1gNnPc6	médium
po	po	k7c6	po
volbě	volba	k1gFnSc6	volba
označován	označovat	k5eAaImNgMnS	označovat
za	za	k7c4	za
politika	politik	k1gMnSc4	politik
neomaleného	omalený	k2eNgInSc2d1	neomalený
stylu	styl	k1gInSc2	styl
s	s	k7c7	s
populistickým	populistický	k2eAgInSc7d1	populistický
akcentem	akcent	k1gInSc7	akcent
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
byla	být	k5eAaImAgFnS	být
dne	den	k1gInSc2	den
26.	[number]	k4	26.
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
vyjádření	vyjádření	k1gNnSc2	vyjádření
francouzského	francouzský	k2eAgInSc2d1	francouzský
deníku	deník	k1gInSc2	deník
Le	Le	k1gMnSc5	Le
Monde	Mond	k1gMnSc5	Mond
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
také	také	k9	také
německých	německý	k2eAgFnPc2d1	německá
novin	novina	k1gFnPc2	novina
Die	Die	k1gMnSc1	Die
Welt	Welt	k1gMnSc1	Welt
a	a	k8xC	a
rakouského	rakouský	k2eAgInSc2d1	rakouský
deníku	deník	k1gInSc2	deník
Kurier	Kurira	k1gFnPc2	Kurira
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
Zeman	Zeman	k1gMnSc1	Zeman
označen	označit	k5eAaPmNgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
levicového	levicový	k2eAgMnSc4d1	levicový
populistu	populista	k1gMnSc4	populista
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
ve	v	k7c6	v
vysílání	vysílání	k1gNnSc6	vysílání
BBC	BBC	kA	BBC
<g/>
.	.	kIx.	.
</s>
<s>
Britský	britský	k2eAgInSc1d1	britský
server	server	k1gInSc1	server
The	The	k1gMnSc1	The
Telegraph	Telegraph	k1gMnSc1	Telegraph
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
Zemanovu	Zemanův	k2eAgFnSc4d1	Zemanova
prezidentskou	prezidentský	k2eAgFnSc4d1	prezidentská
kampaň	kampaň	k1gFnSc4	kampaň
financovala	financovat	k5eAaBmAgFnS	financovat
ruská	ruský	k2eAgFnSc1d1	ruská
společnost	společnost	k1gFnSc1	společnost
Lukoil	Lukoila	k1gFnPc2	Lukoila
<g/>
.	.	kIx.	.
<g/>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
správní	správní	k2eAgInSc1d1	správní
soud	soud	k1gInSc1	soud
obdržel	obdržet	k5eAaPmAgInS	obdržet
po	po	k7c6	po
prezidentské	prezidentský	k2eAgFnSc6d1	prezidentská
volbě	volba	k1gFnSc6	volba
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
návrhů	návrh	k1gInPc2	návrh
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
neplatnost	neplatnost	k1gFnSc4	neplatnost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
zprávě	zpráva	k1gFnSc6	zpráva
konstatoval	konstatovat	k5eAaBmAgMnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
volební	volební	k2eAgInSc1d1	volební
tým	tým	k1gInSc1	tým
jednali	jednat	k5eAaImAgMnP	jednat
protizákonně	protizákonně	k6eAd1	protizákonně
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dopustili	dopustit	k5eAaPmAgMnP	dopustit
několika	několik	k4yIc2	několik
nepravdivých	pravdivý	k2eNgNnPc2d1	nepravdivé
tvrzení	tvrzení	k1gNnPc2	tvrzení
a	a	k8xC	a
lží	lež	k1gFnPc2	lež
na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
svého	svůj	k3xOyFgMnSc2	svůj
protikandidáta	protikandidát	k1gMnSc2	protikandidát
Karla	Karel	k1gMnSc2	Karel
Schwarzenberga	Schwarzenberg	k1gMnSc2	Schwarzenberg
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
obvinil	obvinit	k5eAaPmAgMnS	obvinit
Schwarzenberga	Schwarzenberg	k1gMnSc4	Schwarzenberg
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyhnal	vyhnat	k5eAaPmAgMnS	vyhnat
učitelku	učitelka	k1gFnSc4	učitelka
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
zámku	zámek	k1gInSc2	zámek
v	v	k7c6	v
Čimelicích	Čimelice	k1gFnPc6	Čimelice
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dotyčná	dotyčný	k2eAgFnSc1d1	dotyčná
sama	sám	k3xTgFnSc1	sám
popřela	popřít	k5eAaPmAgFnS	popřít
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Když	když	k8xS	když
jste	být	k5eAaImIp2nP	být
restituoval	restituovat	k5eAaBmAgMnS	restituovat
zámek	zámek	k1gInSc4	zámek
v	v	k7c6	v
Čimelicích	Čimelice	k1gFnPc6	Čimelice
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
tam	tam	k6eAd1	tam
učiliště	učiliště	k1gNnSc2	učiliště
pro	pro	k7c4	pro
zdravotně	zdravotně	k6eAd1	zdravotně
postižené	postižený	k2eAgFnPc4d1	postižená
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgNnSc3	tento
učilišti	učiliště	k1gNnSc3	učiliště
jste	být	k5eAaImIp2nP	být
nasadil	nasadit	k5eAaPmAgMnS	nasadit
takové	takový	k3xDgInPc4	takový
nájemné	nájemný	k2eAgInPc4d1	nájemný
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
muselo	muset	k5eAaImAgNnS	muset
vystěhovat	vystěhovat	k5eAaPmF	vystěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Zadruhé	zadruhé	k4xO	zadruhé
<g/>
,	,	kIx,	,
Táňa	Táňa	k1gFnSc1	Táňa
Bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
učitelka	učitelka	k1gFnSc1	učitelka
na	na	k7c6	na
učilišti	učiliště	k1gNnSc6	učiliště
<g/>
,	,	kIx,	,
přišla	přijít	k5eAaPmAgFnS	přijít
o	o	k7c4	o
služební	služební	k2eAgInSc4d1	služební
byt	byt	k1gInSc4	byt
a	a	k8xC	a
musela	muset	k5eAaImAgFnS	muset
k	k	k7c3	k
příbuzným	příbuzný	k1gMnPc3	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
ochrana	ochrana	k1gFnSc1	ochrana
slabých	slabý	k2eAgMnPc2d1	slabý
a	a	k8xC	a
chudých	chudý	k2eAgMnPc2d1	chudý
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Dále	daleko	k6eAd2	daleko
podle	podle	k7c2	podle
Zemana	Zeman	k1gMnSc2	Zeman
předseda	předseda	k1gMnSc1	předseda
sudetoněmeckého	sudetoněmecký	k2eAgInSc2d1	sudetoněmecký
Landsmannschaftu	Landsmannschaft	k1gInSc2	Landsmannschaft
Bernd	Bernd	k1gMnSc1	Bernd
Posselt	Posselt	k1gMnSc1	Posselt
veřejně	veřejně	k6eAd1	veřejně
podpořil	podpořit	k5eAaPmAgMnS	podpořit
Karla	Karel	k1gMnSc4	Karel
Schwarzenberga	Schwarzenberg	k1gMnSc4	Schwarzenberg
v	v	k7c6	v
listu	list	k1gInSc6	list
Süddeutsche	Süddeutsche	k1gFnSc1	Süddeutsche
Zeitung	Zeitung	k1gInSc1	Zeitung
"	"	kIx"	"
<g/>
s	s	k7c7	s
nadějí	naděje	k1gFnSc7	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
obnoví	obnovit	k5eAaPmIp3nS	obnovit
či	či	k8xC	či
rozšíří	rozšířit	k5eAaPmIp3nS	rozšířit
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
krajanským	krajanský	k2eAgNnSc7d1	krajanské
sdružením	sdružení	k1gNnSc7	sdružení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
výroku	výrok	k1gInSc2	výrok
se	se	k3xPyFc4	se
distancovaly	distancovat	k5eAaBmAgFnP	distancovat
Britské	britský	k2eAgFnPc1d1	britská
listy	lista	k1gFnPc1	lista
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgInPc6	který
údajně	údajně	k6eAd1	údajně
Zeman	Zeman	k1gMnSc1	Zeman
tuto	tento	k3xDgFnSc4	tento
informaci	informace	k1gFnSc4	informace
získal	získat	k5eAaPmAgMnS	získat
<g/>
,	,	kIx,	,
i	i	k8xC	i
Posselt	Posselt	k1gMnSc1	Posselt
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
na	na	k7c6	na
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
stránkách	stránka	k1gFnPc6	stránka
Zemanovy	Zemanův	k2eAgFnSc2d1	Zemanova
kampaně	kampaň	k1gFnSc2	kampaň
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
výrok	výrok	k1gInSc1	výrok
<g/>
,	,	kIx,	,
opřený	opřený	k2eAgInSc1d1	opřený
údajně	údajně	k6eAd1	údajně
o	o	k7c4	o
článek	článek	k1gInSc4	článek
Adama	Adam	k1gMnSc2	Adam
B.	B.	kA	B.
Bartoše	Bartoš	k1gMnSc2	Bartoš
<g/>
,	,	kIx,	,
že	že	k8xS	že
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
"	"	kIx"	"
<g/>
Cíleně	cíleně	k6eAd1	cíleně
zamlčel	zamlčet	k5eAaPmAgMnS	zamlčet
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
měli	mít	k5eAaImAgMnP	mít
kolem	kolem	k7c2	kolem
sebe	sebe	k3xPyFc4	sebe
samé	samý	k3xTgMnPc4	samý
nacisty	nacista	k1gMnPc4	nacista
a	a	k8xC	a
že	že	k8xS	že
dokonce	dokonce	k9	dokonce
v	v	k7c6	v
rodovém	rodový	k2eAgNnSc6d1	rodové
sídle	sídlo	k1gNnSc6	sídlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pobývá	pobývat	k5eAaImIp3nS	pobývat
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Therese	Therese	k1gFnSc2	Therese
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
visí	viset	k5eAaImIp3nS	viset
obrazy	obraz	k1gInPc4	obraz
s	s	k7c7	s
hákovými	hákový	k2eAgInPc7d1	hákový
kříži	kříž	k1gInPc7	kříž
a	a	k8xC	a
hajlujícími	hajlující	k2eAgMnPc7d1	hajlující
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Zeman	Zeman	k1gMnSc1	Zeman
se	se	k3xPyFc4	se
za	za	k7c4	za
výroky	výrok	k1gInPc4	výrok
omluvil	omluvit	k5eAaPmAgMnS	omluvit
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
správní	správní	k2eAgInSc1d1	správní
soud	soud	k1gInSc1	soud
dospěl	dochvít	k5eAaPmAgInS	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
že	že	k8xS	že
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
volební	volební	k2eAgFnSc2d1	volební
kampaně	kampaň	k1gFnSc2	kampaň
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
nezákonnostem	nezákonnost	k1gFnPc3	nezákonnost
schopným	schopný	k2eAgFnPc3d1	schopná
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
volební	volební	k2eAgInSc1d1	volební
výsledek	výsledek	k1gInSc1	výsledek
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nikoliv	nikoliv	k9	nikoliv
tak	tak	k6eAd1	tak
intenzivním	intenzivní	k2eAgInSc7d1	intenzivní
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
konečný	konečný	k2eAgInSc1d1	konečný
výsledek	výsledek	k1gInSc1	výsledek
změnit	změnit	k5eAaPmF	změnit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
První	první	k4xOgInSc1	první
mandát	mandát	k1gInSc1	mandát
prezidenta	prezident	k1gMnSc2	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
10	[number]	k4	10
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
17	[number]	k4	17
minut	minuta	k1gFnPc2	minuta
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
8.	[number]	k4	8.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
složil	složit	k5eAaPmAgMnS	složit
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
slib	slib	k1gInSc4	slib
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
třetím	třetí	k4xOgMnSc7	třetí
prezidentem	prezident	k1gMnSc7	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
titulu	titul	k1gInSc2	titul
mu	on	k3xPp3gMnSc3	on
přísluší	příslušet	k5eAaImIp3nS	příslušet
řádové	řádový	k2eAgFnPc4d1	řádová
insignie	insignie	k1gFnPc4	insignie
dvou	dva	k4xCgFnPc2	dva
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
státních	státní	k2eAgFnPc2d1	státní
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
<g/>
,	,	kIx,	,
Řádu	řád	k1gInSc2	řád
Bílého	bílý	k2eAgInSc2d1	bílý
lva	lev	k1gInSc2	lev
a	a	k8xC	a
Řádu	řád	k1gInSc2	řád
Tomáše	Tomáš	k1gMnSc2	Tomáš
Garrigua	Garriguus	k1gMnSc2	Garriguus
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Domácí	domácí	k2eAgFnSc1d1	domácí
politická	politický	k2eAgFnSc1d1	politická
scéna	scéna	k1gFnSc1	scéna
===	===	k?	===
</s>
</p>
<p>
<s>
Úřad	úřad	k1gInSc1	úřad
hlavy	hlava	k1gFnSc2	hlava
státu	stát	k1gInSc2	stát
chtěl	chtít	k5eAaImAgMnS	chtít
Zeman	Zeman	k1gMnSc1	Zeman
podle	podle	k7c2	podle
svého	svůj	k3xOyFgInSc2	svůj
inauguračního	inaugurační	k2eAgInSc2d1	inaugurační
projevu	projev	k1gInSc2	projev
využít	využít	k5eAaPmF	využít
pro	pro	k7c4	pro
roli	role	k1gFnSc4	role
prostředníka	prostředník	k1gMnSc2	prostředník
vyjednávání	vyjednávání	k1gNnSc2	vyjednávání
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Nabízím	nabízet	k5eAaImIp1nS	nabízet
prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
úřad	úřad	k1gInSc4	úřad
jako	jako	k8xC	jako
neutrální	neutrální	k2eAgNnSc4d1	neutrální
pole	pole	k1gNnSc4	pole
pro	pro	k7c4	pro
dialog	dialog	k1gInSc4	dialog
všech	všecek	k3xTgFnPc2	všecek
parlamentních	parlamentní	k2eAgFnPc2d1	parlamentní
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
dalších	další	k2eAgFnPc2d1	další
významných	významný	k2eAgFnPc2d1	významná
společenských	společenský	k2eAgFnPc2d1	společenská
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
odbory	odbor	k1gInPc1	odbor
<g/>
,	,	kIx,	,
zaměstnavatelské	zaměstnavatelský	k2eAgInPc1d1	zaměstnavatelský
svazy	svaz	k1gInPc1	svaz
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
instituce	instituce	k1gFnPc1	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
není	být	k5eNaImIp3nS	být
přímo	přímo	k6eAd1	přímo
ani	ani	k8xC	ani
nepřímo	přímo	k6eNd1	přímo
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
žádnou	žádný	k3yNgFnSc7	žádný
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
stranou	strana	k1gFnSc7	strana
<g/>
,	,	kIx,	,
nabízím	nabízet	k5eAaImIp1nS	nabízet
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
<g/>
-li	i	k?	-li
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc1	ten
zúčastněné	zúčastněný	k2eAgFnPc1d1	zúčastněná
strany	strana	k1gFnPc1	strana
přát	přát	k5eAaImF	přát
<g/>
,	,	kIx,	,
roli	role	k1gFnSc4	role
prostředníka	prostředník	k1gMnSc2	prostředník
a	a	k8xC	a
roli	role	k1gFnSc4	role
moderátora	moderátor	k1gMnSc2	moderátor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
nikoli	nikoli	k9	nikoli
roli	role	k1gFnSc4	role
soudce	soudce	k1gMnSc2	soudce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tato	tento	k3xDgFnSc1	tento
role	role	k1gFnSc1	role
prezidentu	prezident	k1gMnSc3	prezident
republiky	republika	k1gFnSc2	republika
evidentně	evidentně	k6eAd1	evidentně
nepřísluší	příslušet	k5eNaImIp3nS	příslušet
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
podala	podat	k5eAaPmAgFnS	podat
demisi	demise	k1gFnSc4	demise
vláda	vláda	k1gFnSc1	vláda
Petra	Petr	k1gMnSc4	Petr
Nečase	Nečas	k1gMnSc4	Nečas
<g/>
,	,	kIx,	,
pověřil	pověřit	k5eAaPmAgMnS	pověřit
prezident	prezident	k1gMnSc1	prezident
Zeman	Zeman	k1gMnSc1	Zeman
sestavením	sestavení	k1gNnSc7	sestavení
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
Jiřího	Jiří	k1gMnSc2	Jiří
Rusnoka	Rusnoek	k1gMnSc2	Rusnoek
<g/>
.	.	kIx.	.
</s>
<s>
Nevyslyšel	vyslyšet	k5eNaPmAgInS	vyslyšet
tak	tak	k9	tak
požadavek	požadavek	k1gInSc1	požadavek
vládní	vládní	k2eAgFnSc2d1	vládní
koalice	koalice	k1gFnSc2	koalice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
jmenováním	jmenování	k1gNnSc7	jmenování
přišla	přijít	k5eAaPmAgFnS	přijít
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
komoře	komora	k1gFnSc6	komora
drží	držet	k5eAaImIp3nP	držet
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
většinu	většina	k1gFnSc4	většina
101	[number]	k4	101
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
na	na	k7c4	na
premiérský	premiérský	k2eAgInSc4d1	premiérský
post	post	k1gInSc4	post
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
Miroslavu	Miroslava	k1gFnSc4	Miroslava
Němcovou	Němcův	k2eAgFnSc7d1	Němcova
<g/>
.	.	kIx.	.
</s>
<s>
Jmenování	jmenování	k1gNnSc1	jmenování
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
následně	následně	k6eAd1	následně
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
místopředseda	místopředseda	k1gMnSc1	místopředseda
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
Othmar	Othmar	k1gMnSc1	Othmar
Karas	Karas	k1gMnSc1	Karas
a	a	k8xC	a
šéf	šéf	k1gMnSc1	šéf
tamního	tamní	k2eAgInSc2d1	tamní
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
výboru	výbor	k1gInSc2	výbor
Elmar	Elmar	k1gMnSc1	Elmar
Brok	Brok	k1gMnSc1	Brok
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
požadovali	požadovat	k5eAaImAgMnP	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
"	"	kIx"	"
<g/>
rychle	rychle	k6eAd1	rychle
prověřila	prověřit	k5eAaPmAgFnS	prověřit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
vznik	vznik	k1gInSc1	vznik
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
ČR	ČR	kA	ČR
neodporuje	odporovat	k5eNaImIp3nS	odporovat
unijním	unijní	k2eAgFnPc3d1	unijní
hodnotám	hodnota	k1gFnPc3	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Zeman	Zeman	k1gMnSc1	Zeman
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
reagoval	reagovat	k5eAaBmAgMnS	reagovat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
nejprve	nejprve	k6eAd1	nejprve
seznámit	seznámit	k5eAaPmF	seznámit
s	s	k7c7	s
českou	český	k2eAgFnSc7d1	Česká
ústavou	ústava	k1gFnSc7	ústava
a	a	k8xC	a
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ústava	ústava	k1gFnSc1	ústava
ČR	ČR	kA	ČR
jasně	jasně	k6eAd1	jasně
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
předsedu	předseda	k1gMnSc4	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
stanovila	stanovit	k5eAaPmAgFnS	stanovit
podmínku	podmínka	k1gFnSc4	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
tuto	tento	k3xDgFnSc4	tento
vládu	vláda	k1gFnSc4	vláda
musí	muset	k5eAaImIp3nS	muset
předem	předem	k6eAd1	předem
schválit	schválit	k5eAaPmF	schválit
sněmovna	sněmovna	k1gFnSc1	sněmovna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Před	před	k7c7	před
hlasováním	hlasování	k1gNnSc7	hlasování
o	o	k7c6	o
důvěře	důvěra	k1gFnSc6	důvěra
Rusnokově	Rusnokův	k2eAgFnSc6d1	Rusnokova
vládě	vláda	k1gFnSc6	vláda
7.	[number]	k4	7.
srpna	srpen	k1gInSc2	srpen
opustili	opustit	k5eAaPmAgMnP	opustit
poslanci	poslanec	k1gMnPc1	poslanec
Tomáš	Tomáš	k1gMnSc1	Tomáš
Úlehla	Úlehla	k1gMnSc1	Úlehla
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Florián	Florián	k1gMnSc1	Florián
a	a	k8xC	a
Karolína	Karolína	k1gFnSc1	Karolína
Peake	Peake	k1gFnSc3	Peake
sněmovnu	sněmovna	k1gFnSc4	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Vládě	Vláďa	k1gFnSc3	Vláďa
důvěra	důvěra	k1gFnSc1	důvěra
vyslovena	vyslovit	k5eAaPmNgFnS	vyslovit
nebyla	být	k5eNaImAgFnS	být
<g/>
,	,	kIx,	,
absencí	absence	k1gFnSc7	absence
výše	vysoce	k6eAd2	vysoce
zmíněných	zmíněný	k2eAgMnPc2d1	zmíněný
poslanců	poslanec	k1gMnPc2	poslanec
však	však	k9	však
koalice	koalice	k1gFnSc1	koalice
přišla	přijít	k5eAaPmAgFnS	přijít
o	o	k7c4	o
většinu	většina	k1gFnSc4	většina
101	[number]	k4	101
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
de	de	k?	de
facto	facto	k1gNnSc4	facto
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Rusnok	Rusnok	k1gInSc4	Rusnok
předal	předat	k5eAaPmAgMnS	předat
Zemanovi	Zeman	k1gMnSc3	Zeman
demisi	demise	k1gFnSc4	demise
vlády	vláda	k1gFnSc2	vláda
dne	den	k1gInSc2	den
13.	[number]	k4	13.
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
pak	pak	k6eAd1	pak
jeho	jeho	k3xOp3gFnSc4	jeho
vládu	vláda	k1gFnSc4	vláda
pověřil	pověřit	k5eAaPmAgInS	pověřit
pokračováním	pokračování	k1gNnSc7	pokračování
ve	v	k7c6	v
výkonu	výkon	k1gInSc6	výkon
funkce	funkce	k1gFnSc2	funkce
v	v	k7c6	v
demisi	demise	k1gFnSc6	demise
až	až	k9	až
do	do	k7c2	do
složení	složení	k1gNnSc2	složení
vlády	vláda	k1gFnSc2	vláda
nové	nový	k2eAgFnSc2d1	nová
<g/>
.	.	kIx.	.
28.	[number]	k4	28.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
prezident	prezident	k1gMnSc1	prezident
Zeman	Zeman	k1gMnSc1	Zeman
podepsal	podepsat	k5eAaPmAgMnS	podepsat
rozpuštění	rozpuštění	k1gNnSc1	rozpuštění
sněmovny	sněmovna	k1gFnSc2	sněmovna
a	a	k8xC	a
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
termín	termín	k1gInSc1	termín
předčasných	předčasný	k2eAgFnPc2d1	předčasná
voleb	volba	k1gFnPc2	volba
na	na	k7c4	na
25.	[number]	k4	25.
a	a	k8xC	a
26.	[number]	k4	26.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
volbách	volba	k1gFnPc6	volba
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
Česká	český	k2eAgFnSc1d1	Česká
strana	strana	k1gFnSc1	strana
sociálně	sociálně	k6eAd1	sociálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
před	před	k7c7	před
hnutím	hnutí	k1gNnSc7	hnutí
ANO	ano	k9	ano
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
zasedli	zasednout	k5eAaPmAgMnP	zasednout
také	také	k9	také
poslanci	poslanec	k1gMnPc1	poslanec
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
hnutí	hnutí	k1gNnSc2	hnutí
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
<g/>
,	,	kIx,	,
Občanské	občanský	k2eAgFnSc2d1	občanská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
hnutí	hnutí	k1gNnSc1	hnutí
Úsvit	úsvit	k1gInSc4	úsvit
přímé	přímý	k2eAgFnSc2d1	přímá
demokracie	demokracie	k1gFnSc2	demokracie
Tomia	Tomium	k1gNnSc2	Tomium
Okamury	Okamura	k1gFnSc2	Okamura
a	a	k8xC	a
Křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
a	a	k8xC	a
demokratické	demokratický	k2eAgFnSc2d1	demokratická
unie	unie	k1gFnSc2	unie
–	–	k?	–
Československé	československý	k2eAgFnSc2d1	Československá
strany	strana	k1gFnSc2	strana
lidové	lidový	k2eAgFnSc2d1	lidová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
předčasných	předčasný	k2eAgFnPc6d1	předčasná
volbách	volba	k1gFnPc6	volba
si	se	k3xPyFc3	se
na	na	k7c4	na
den	den	k1gInSc4	den
26.	[number]	k4	26.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
do	do	k7c2	do
zámku	zámek	k1gInSc2	zámek
Lány	lán	k1gInPc1	lán
pozval	pozvat	k5eAaPmAgMnS	pozvat
vedení	vedení	k1gNnSc3	vedení
ČSSD	ČSSD	kA	ČSSD
(	(	kIx(	(
<g/>
Haška	Hašek	k1gMnSc4	Hašek
<g/>
,	,	kIx,	,
Tejce	tejcit	k5eAaImSgInS	tejcit
<g/>
,	,	kIx,	,
Škromacha	Škromacha	k1gMnSc1	Škromacha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
řádně	řádně	k6eAd1	řádně
zvolený	zvolený	k2eAgMnSc1d1	zvolený
a	a	k8xC	a
úřadující	úřadující	k2eAgMnSc1d1	úřadující
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Sobotka	Sobotka	k1gMnSc1	Sobotka
pozván	pozvat	k5eAaPmNgMnS	pozvat
nebyl	být	k5eNaImAgMnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Hašek	Hašek	k1gMnSc1	Hašek
i	i	k9	i
další	další	k2eAgMnPc1d1	další
účastníci	účastník	k1gMnPc1	účastník
několikrát	několikrát	k6eAd1	několikrát
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
popřeli	popřít	k5eAaPmAgMnP	popřít
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
schůzka	schůzka	k1gFnSc1	schůzka
konala	konat	k5eAaImAgFnS	konat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
pod	pod	k7c7	pod
mediálním	mediální	k2eAgInSc7d1	mediální
tlakem	tlak	k1gInSc7	tlak
schůzku	schůzka	k1gFnSc4	schůzka
potvrdili	potvrdit	k5eAaPmAgMnP	potvrdit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
jejich	jejich	k3xOp3gFnPc3	jejich
rezignacím	rezignace	k1gFnPc3	rezignace
na	na	k7c4	na
vrcholné	vrcholný	k2eAgFnPc4d1	vrcholná
stranické	stranický	k2eAgFnPc4d1	stranická
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
poté	poté	k6eAd1	poté
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
komické	komický	k2eAgNnSc4d1	komické
utajovat	utajovat	k5eAaImF	utajovat
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
schůzku	schůzka	k1gFnSc4	schůzka
a	a	k8xC	a
současně	současně	k6eAd1	současně
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
převrat	převrat	k1gInSc4	převrat
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
<g/>
V	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
27.	[number]	k4	27.
února	únor	k1gInSc2	únor
2015	[number]	k4	2015
uctil	uctít	k5eAaPmAgMnS	uctít
památku	památka	k1gFnSc4	památka
obětí	oběť	k1gFnSc7	oběť
střelby	střelba	k1gFnSc2	střelba
v	v	k7c6	v
Uherském	uherský	k2eAgInSc6d1	uherský
Brodě	Brod	k1gInSc6	Brod
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
výročí	výročí	k1gNnSc4	výročí
sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
<g />
.	.	kIx.	.
</s>
<s>
dne	den	k1gInSc2	den
17.	[number]	k4	17.
listopadu	listopad	k1gInSc2	listopad
2015	[number]	k4	2015
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
s	s	k7c7	s
projevem	projev	k1gInSc7	projev
na	na	k7c6	na
demonstraci	demonstrace	k1gFnSc6	demonstrace
pořádané	pořádaný	k2eAgFnSc6d1	pořádaná
hnutím	hnutí	k1gNnSc7	hnutí
Blok	blok	k1gInSc1	blok
proti	proti	k7c3	proti
islámu	islám	k1gInSc3	islám
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
projevem	projev	k1gInSc7	projev
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
mimo	mimo	k6eAd1	mimo
jiné	jiná	k1gFnSc2	jiná
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Když	když	k8xS	když
se	se	k3xPyFc4	se
ptám	ptat	k5eAaImIp1nS	ptat
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
migraci	migrace	k1gFnSc3	migrace
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
vím	vědět	k5eAaImIp1nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
velká	velký	k2eAgFnSc1d1	velká
většina	většina	k1gFnSc1	většina
těchto	tento	k3xDgMnPc2	tento
ilegálních	ilegální	k2eAgMnPc2d1	ilegální
migrantů	migrant	k1gMnPc2	migrant
jsou	být	k5eAaImIp3nP	být
mladí	mladý	k2eAgMnPc1d1	mladý
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
živení	živený	k2eAgMnPc1d1	živený
<g/>
,	,	kIx,	,
muži	muž	k1gMnPc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
já	já	k3xPp1nSc1	já
se	se	k3xPyFc4	se
ptám	ptat	k5eAaImIp1nS	ptat
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
tito	tento	k3xDgMnPc1	tento
muži	muž	k1gMnPc1	muž
nebojují	bojovat	k5eNaImIp3nP	bojovat
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
proti	proti	k7c3	proti
Islámskému	islámský	k2eAgInSc3d1	islámský
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
pokud	pokud	k8xS	pokud
přicházejí	přicházet	k5eAaImIp3nP	přicházet
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nebojuje	bojovat	k5eNaImIp3nS	bojovat
<g/>
,	,	kIx,	,
ptám	ptat	k5eAaImIp1nS	ptat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
nepracují	pracovat	k5eNaImIp3nP	pracovat
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
vlast	vlast	k1gFnSc4	vlast
a	a	k8xC	a
pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
zvelebení	zvelebení	k1gNnSc4	zvelebení
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jejich	jejich	k3xOp3gFnSc1	jejich
země	země	k1gFnSc1	země
překonala	překonat	k5eAaPmAgFnS	překonat
dosavadní	dosavadní	k2eAgFnSc4d1	dosavadní
zaostalost	zaostalost	k1gFnSc4	zaostalost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
výstup	výstup	k1gInSc4	výstup
byl	být	k5eAaImAgInS	být
kritizován	kritizovat	k5eAaImNgInS	kritizovat
premiérem	premiér	k1gMnSc7	premiér
Sobotkou	Sobotka	k1gMnSc7	Sobotka
<g/>
,	,	kIx,	,
ministrem	ministr	k1gMnSc7	ministr
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
Dienstbierem	Dienstbier	k1gInSc7	Dienstbier
a	a	k8xC	a
lidoveckými	lidovecký	k2eAgInPc7d1	lidovecký
europoslanci	europoslanec	k1gInPc7	europoslanec
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgFnPc2	jenž
podporuje	podporovat	k5eAaImIp3nS	podporovat
fašizující	fašizující	k2eAgNnPc4d1	fašizující
hnutí	hnutí	k1gNnPc4	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Předmětem	předmět	k1gInSc7	předmět
kritiky	kritika	k1gFnSc2	kritika
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
zejména	zejména	k9	zejména
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezident	prezident	k1gMnSc1	prezident
stál	stát	k5eAaImAgMnS	stát
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
vedle	vedle	k7c2	vedle
Martina	Martin	k1gMnSc2	Martin
Konvičky	konvička	k1gFnSc2	konvička
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgInS	být
druhý	druhý	k4xOgInSc1	druhý
den	den	k1gInSc1	den
obviněn	obvinit	k5eAaPmNgInS	obvinit
z	z	k7c2	z
šíření	šíření	k1gNnSc2	šíření
nenávisti	nenávist	k1gFnSc2	nenávist
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
prezident	prezident	k1gMnSc1	prezident
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
době	doba	k1gFnSc6	doba
konání	konání	k1gNnSc2	konání
mítinku	mítink	k1gInSc2	mítink
Martina	Martin	k1gMnSc4	Martin
Konvičku	konvička	k1gFnSc4	konvička
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc4	jeho
názory	názor	k1gInPc4	názor
neznal	neznat	k5eAaImAgInS	neznat
a	a	k8xC	a
vyloučil	vyloučit	k5eAaPmAgInS	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
přijal	přijmout	k5eAaPmAgMnS	přijmout
jakékoli	jakýkoli	k3yIgFnPc4	jakýkoli
pozvánky	pozvánka	k1gFnPc4	pozvánka
na	na	k7c4	na
mítinky	mítink	k1gInPc4	mítink
Bloku	blok	k1gInSc2	blok
proti	proti	k7c3	proti
islámu	islám	k1gInSc3	islám
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
z	z	k7c2	z
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
prezident	prezident	k1gMnSc1	prezident
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
podporu	podpora	k1gFnSc4	podpora
Martinu	Martin	k1gMnSc3	Martin
Konvičkovi	Konvička	k1gMnSc3	Konvička
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc3	jeho
názorům	názor	k1gInPc3	názor
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
Martina	Martin	k1gMnSc4	Martin
Konvičku	konvička	k1gFnSc4	konvička
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc4	jeho
názory	názor	k1gInPc4	názor
znal	znát	k5eAaImAgInS	znát
před	před	k7c7	před
17.	[number]	k4	17.
listopadem	listopad	k1gInSc7	listopad
2015.	[number]	k4	2015.
<g/>
Senát	senát	k1gInSc1	senát
3.	[number]	k4	3.
března	březen	k1gInSc2	březen
2016	[number]	k4	2016
těsnou	těsný	k2eAgFnSc7d1	těsná
většinou	většina	k1gFnSc7	většina
přijal	přijmout	k5eAaPmAgMnS	přijmout
usnesení	usnesení	k1gNnSc4	usnesení
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
nabádal	nabádat	k5eAaBmAgMnS	nabádat
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
vyjádřeních	vyjádření	k1gNnPc6	vyjádření
zdrženlivý	zdrženlivý	k2eAgInSc1d1	zdrženlivý
<g/>
,	,	kIx,	,
neměl	mít	k5eNaImAgInS	mít
by	by	kYmCp3nS	by
ohrožovat	ohrožovat	k5eAaImF	ohrožovat
a	a	k8xC	a
poškozovat	poškozovat	k5eAaImF	poškozovat
vážnost	vážnost	k1gFnSc4	vážnost
jiných	jiný	k2eAgInPc2d1	jiný
ústavních	ústavní	k2eAgInPc2d1	ústavní
orgánů	orgán	k1gInPc2	orgán
a	a	k8xC	a
rozdělovat	rozdělovat	k5eAaImF	rozdělovat
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byly	být	k5eAaImAgInP	být
Zemanovy	Zemanův	k2eAgInPc1d1	Zemanův
výroky	výrok	k1gInPc1	výrok
proti	proti	k7c3	proti
sankcím	sankce	k1gFnPc3	sankce
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
vůči	vůči	k7c3	vůči
Rusku	Rusko	k1gNnSc3	Rusko
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
anexi	anexe	k1gFnSc4	anexe
Krymu	Krym	k1gInSc2	Krym
<g/>
,	,	kIx,	,
nejmenování	nejmenování	k1gNnSc1	nejmenování
některých	některý	k3yIgMnPc2	některý
kandidátů	kandidát	k1gMnPc2	kandidát
na	na	k7c4	na
profesory	profesor	k1gMnPc4	profesor
<g/>
,	,	kIx,	,
účast	účast	k1gFnSc1	účast
na	na	k7c4	na
demonstraci	demonstrace	k1gFnSc4	demonstrace
extremistického	extremistický	k2eAgInSc2d1	extremistický
Bloku	blok	k1gInSc2	blok
proti	proti	k7c3	proti
islámu	islám	k1gInSc2	islám
nebo	nebo	k8xC	nebo
výroky	výrok	k1gInPc4	výrok
na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
premiéra	premiér	k1gMnSc2	premiér
včetně	včetně	k7c2	včetně
příměru	příměr	k1gInSc2	příměr
s	s	k7c7	s
kalašnikovem	kalašnikov	k1gInSc7	kalašnikov
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
4.	[number]	k4	4.
května	květen	k1gInSc2	květen
2017	[number]	k4	2017
nechal	nechat	k5eAaPmAgMnS	nechat
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
připravit	připravit	k5eAaPmF	připravit
sál	sál	k1gInSc4	sál
na	na	k7c4	na
akt	akt	k1gInSc4	akt
podání	podání	k1gNnSc4	podání
demise	demise	k1gFnSc2	demise
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Sobotka	Sobotka	k1gMnSc1	Sobotka
však	však	k9	však
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c6	o
probíhající	probíhající	k2eAgFnSc6d1	probíhající
vládní	vládní	k2eAgFnSc6d1	vládní
krizi	krize	k1gFnSc6	krize
diskutovat	diskutovat	k5eAaImF	diskutovat
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
podat	podat	k5eAaPmF	podat
demisi	demise	k1gFnSc4	demise
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
tento	tento	k3xDgInSc4	tento
fakt	fakt	k1gInSc4	fakt
ignoroval	ignorovat	k5eAaImAgMnS	ignorovat
<g/>
,	,	kIx,	,
pronesl	pronést	k5eAaPmAgMnS	pronést
připravenou	připravený	k2eAgFnSc4d1	připravená
řeč	řeč	k1gFnSc4	řeč
a	a	k8xC	a
během	během	k7c2	během
proslovu	proslov	k1gInSc2	proslov
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Sobotky	Sobotka	k1gMnSc2	Sobotka
odešel	odejít	k5eAaPmAgMnS	odejít
ze	z	k7c2	z
sálu	sál	k1gInSc2	sál
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
9.	[number]	k4	9.
května	květen	k1gInSc2	květen
2017	[number]	k4	2017
předseda	předseda	k1gMnSc1	předseda
Senátu	senát	k1gInSc2	senát
Milan	Milan	k1gMnSc1	Milan
Štěch	Štěch	k1gMnSc1	Štěch
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
prezident	prezident	k1gMnSc1	prezident
Zeman	Zeman	k1gMnSc1	Zeman
do	do	k7c2	do
týdne	týden	k1gInSc2	týden
neodvolá	odvolat	k5eNaPmIp3nS	odvolat
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
a	a	k8xC	a
předsedu	předseda	k1gMnSc4	předseda
ANO	ano	k9	ano
Andreje	Andrej	k1gMnSc4	Andrej
Babiše	Babiš	k1gMnSc4	Babiš
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Senát	senát	k1gInSc1	senát
připraven	připraven	k2eAgInSc1d1	připraven
podat	podat	k5eAaPmF	podat
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
ústavní	ústavní	k2eAgFnSc4d1	ústavní
žalobu	žaloba	k1gFnSc4	žaloba
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentův	prezidentův	k2eAgMnSc1d1	prezidentův
mluvčí	mluvčí	k1gMnSc1	mluvčí
Jiří	Jiří	k1gMnSc1	Jiří
Ovčáček	ovčáček	k1gMnSc1	ovčáček
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
vyhrožování	vyhrožování	k1gNnSc1	vyhrožování
prezidentu	prezident	k1gMnSc3	prezident
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
demokratické	demokratický	k2eAgFnSc6d1	demokratická
společnosti	společnost	k1gFnSc6	společnost
nepřijatelné	přijatelný	k2eNgNnSc1d1	nepřijatelné
a	a	k8xC	a
neakceptovatelné	akceptovatelný	k2eNgNnSc1d1	neakceptovatelné
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
podporuje	podporovat	k5eAaImIp3nS	podporovat
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
diplomacii	diplomacie	k1gFnSc4	diplomacie
<g/>
.	.	kIx.	.
</s>
<s>
Stran	stran	k7c2	stran
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
Zeman	Zeman	k1gMnSc1	Zeman
nejednou	jednou	k6eNd1	jednou
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
hodlá	hodlat	k5eAaImIp3nS	hodlat
prosazovat	prosazovat	k5eAaImF	prosazovat
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
politiku	politika	k1gFnSc4	politika
všech	všecek	k3xTgInPc2	všecek
azimutů	azimut	k1gInPc2	azimut
<g/>
"	"	kIx"	"
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
Charlese	Charles	k1gMnSc2	Charles
de	de	k?	de
Gaulla	Gaull	k1gMnSc2	Gaull
<g/>
;	;	kIx,	;
upřesnil	upřesnit	k5eAaPmAgMnS	upřesnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pod	pod	k7c7	pod
tímto	tento	k3xDgInSc7	tento
rozumí	rozumět	k5eAaImIp3nS	rozumět
stoupence	stoupenec	k1gMnPc4	stoupenec
evropské	evropský	k2eAgFnSc2d1	Evropská
spolupráce	spolupráce	k1gFnSc2	spolupráce
i	i	k8xC	i
spolupráce	spolupráce	k1gFnSc2	spolupráce
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
americkými	americký	k2eAgMnPc7d1	americký
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
neznamená	znamenat	k5eNaImIp3nS	znamenat
nic	nic	k6eAd1	nic
<g />
.	.	kIx.	.
</s>
<s>
jiného	jiné	k1gNnSc2	jiné
než	než	k8xS	než
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
při	při	k7c6	při
zachování	zachování	k1gNnSc6	zachování
všech	všecek	k3xTgInPc2	všecek
spojeneckých	spojenecký	k2eAgInPc2d1	spojenecký
závazků	závazek	k1gInPc2	závazek
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
udržovat	udržovat	k5eAaImF	udržovat
přátelské	přátelský	k2eAgInPc4d1	přátelský
a	a	k8xC	a
zejména	zejména	k9	zejména
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
styky	styk	k1gInPc4	styk
prakticky	prakticky	k6eAd1	prakticky
se	s	k7c7	s
všemi	všecek	k3xTgFnPc7	všecek
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Minimálně	minimálně	k6eAd1	minimálně
se	s	k7c7	s
všemi	všecek	k3xTgFnPc7	všecek
velkými	velký	k2eAgFnPc7d1	velká
zeměmi	zem	k1gFnPc7	zem
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
ovšem	ovšem	k9	ovšem
při	při	k7c6	při
"	"	kIx"	"
<g/>
zachování	zachování	k1gNnSc6	zachování
všech	všecek	k3xTgInPc2	všecek
spojeneckých	spojenecký	k2eAgInPc2d1	spojenecký
závazků	závazek	k1gInPc2	závazek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
máme	mít	k5eAaImIp1nP	mít
jak	jak	k6eAd1	jak
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
dne	den	k1gInSc2	den
3.	[number]	k4	3.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
ratifikoval	ratifikovat	k5eAaBmAgInS	ratifikovat
dodatek	dodatek	k1gInSc1	dodatek
Lisabonské	lisabonský	k2eAgFnSc2d1	Lisabonská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vznik	vznik	k1gInSc4	vznik
záchranného	záchranný	k2eAgInSc2d1	záchranný
fondu	fond	k1gInSc2	fond
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Eurovalu	Euroval	k1gInSc2	Euroval
<g/>
)	)	kIx)	)
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
ohroženým	ohrožený	k2eAgFnPc3d1	ohrožená
ekonomikám	ekonomika	k1gFnPc3	ekonomika
eurozóny	eurozóna	k1gFnSc2	eurozóna
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
nechal	nechat	k5eAaPmAgMnS	nechat
vztyčit	vztyčit	k5eAaPmF	vztyčit
vlajku	vlajka	k1gFnSc4	vlajka
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Oběma	dva	k4xCgMnPc3	dva
aktům	akt	k1gInPc3	akt
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
bránil	bránit	k5eAaImAgMnS	bránit
předchozí	předchozí	k2eAgMnSc1d1	předchozí
prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
EU	EU	kA	EU
tím	ten	k3xDgNnSc7	ten
nad	nad	k7c7	nad
sídlem	sídlo	k1gNnSc7	sídlo
prezidenta	prezident	k1gMnSc2	prezident
zavlála	zavlát	k5eAaPmAgFnS	zavlát
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
vstupu	vstup	k1gInSc2	vstup
Česka	Česko	k1gNnSc2	Česko
do	do	k7c2	do
Unie	unie	k1gFnSc2	unie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004.	[number]	k4	2004.
<g/>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2014	[number]	k4	2014
vykonal	vykonat	k5eAaPmAgInS	vykonat
státní	státní	k2eAgFnSc4d1	státní
návštěvu	návštěva	k1gFnSc4	návštěva
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
návštěvy	návštěva	k1gFnSc2	návštěva
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
majitelem	majitel	k1gMnSc7	majitel
společnosti	společnost	k1gFnSc2	společnost
PPF	PPF	kA	PPF
Petrem	Petr	k1gMnSc7	Petr
Kellnerem	Kellner	k1gMnSc7	Kellner
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
čínským	čínský	k2eAgMnSc7d1	čínský
prezidentem	prezident	k1gMnSc7	prezident
Si	se	k3xPyFc3	se
Ťin-pchingem	Ťinching	k1gInSc7	Ťin-pching
<g/>
.	.	kIx.	.
</s>
<s>
Poskytl	poskytnout	k5eAaPmAgInS	poskytnout
rozhovor	rozhovor	k1gInSc4	rozhovor
čínské	čínský	k2eAgInPc1d1	čínský
státní	státní	k2eAgFnSc4d1	státní
televizi	televize	k1gFnSc4	televize
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
mimo	mimo	k6eAd1	mimo
jiné	jiná	k1gFnSc2	jiná
propagoval	propagovat	k5eAaImAgInS	propagovat
postavu	postava	k1gFnSc4	postava
českého	český	k2eAgMnSc2d1	český
krtečka	krteček	k1gMnSc2	krteček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zasahovat	zasahovat	k5eAaImF	zasahovat
do	do	k7c2	do
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
záležitostí	záležitost	k1gFnPc2	záležitost
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Neučíme	učit	k5eNaImIp1nP	učit
vás	vy	k3xPp2nPc4	vy
tržní	tržní	k2eAgInPc1d1	tržní
ekonomiku	ekonomika	k1gFnSc4	ekonomika
ani	ani	k8xC	ani
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
od	od	k7c2	od
vás	vy	k3xPp2nPc2	vy
chceme	chtít	k5eAaImIp1nP	chtít
učit	učit	k5eAaImF	učit
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
řekl	říct	k5eAaPmAgMnS	říct
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Jsem	být	k5eAaImIp1nS	být
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
urychlit	urychlit	k5eAaPmF	urychlit
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
růst	růst	k1gInSc4	růst
nebo	nebo	k8xC	nebo
stabilizovat	stabilizovat	k5eAaBmF	stabilizovat
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Setkal	setkat	k5eAaPmAgInS	setkat
se	se	k3xPyFc4	se
také	také	k9	také
s	s	k7c7	s
představiteli	představitel	k1gMnPc7	představitel
čínské	čínský	k2eAgFnSc2d1	čínská
občanské	občanský	k2eAgFnSc2d1	občanská
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgMnPc7	jenž
otevřel	otevřít	k5eAaPmAgMnS	otevřít
otázku	otázka	k1gFnSc4	otázka
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
soukromým	soukromý	k2eAgNnSc7d1	soukromé
letadlem	letadlo	k1gNnSc7	letadlo
pronajatým	pronajatý	k2eAgNnSc7d1	pronajaté
finanční	finanční	k2eAgInSc4d1	finanční
skupinou	skupina	k1gFnSc7	skupina
PPF	PPF	kA	PPF
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
sklidil	sklidit	k5eAaPmAgInS	sklidit
od	od	k7c2	od
politiků	politik	k1gMnPc2	politik
kritiku	kritika	k1gFnSc4	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
důvod	důvod	k1gInSc4	důvod
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
stihnout	stihnout	k5eAaPmF	stihnout
oslavy	oslava	k1gFnPc4	oslava
28.	[number]	k4	28.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
dekorování	dekorování	k1gNnSc1	dekorování
nových	nový	k2eAgMnPc2d1	nový
generálů	generál	k1gMnPc2	generál
<g/>
.	.	kIx.	.
</s>
<s>
Přiletěl	přiletět	k5eAaPmAgMnS	přiletět
tak	tak	k9	tak
do	do	k7c2	do
ČR	ČR	kA	ČR
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
vládní	vládní	k2eAgNnSc1d1	vládní
letadlo	letadlo	k1gNnSc1	letadlo
totiž	totiž	k9	totiž
muselo	muset	k5eAaImAgNnS	muset
kvůli	kvůli	k7c3	kvůli
tankování	tankování	k1gNnSc3	tankování
přistát	přistát	k5eAaImF	přistát
v	v	k7c6	v
mongolském	mongolský	k2eAgInSc6d1	mongolský
Ulánbátaru	Ulánbátar	k1gInSc6	Ulánbátar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
27.	[number]	k4	27.
ledna	leden	k1gInSc2	leden
2015	[number]	k4	2015
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
fóru	fórum	k1gNnSc6	fórum
"	"	kIx"	"
<g/>
Let	let	k1gInSc1	let
My	my	k3xPp1nPc1	my
People	People	k1gMnSc4	People
Live	Live	k1gFnSc6	Live
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
konaném	konaný	k2eAgInSc6d1	konaný
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
s	s	k7c7	s
projevem	projev	k1gInSc7	projev
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
přirovnal	přirovnat	k5eAaPmAgInS	přirovnat
Islámský	islámský	k2eAgInSc1d1	islámský
stát	stát	k1gInSc1	stát
k	k	k7c3	k
nacistickému	nacistický	k2eAgNnSc3d1	nacistické
Německu	Německo	k1gNnSc3	Německo
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
k	k	k7c3	k
ozbrojené	ozbrojený	k2eAgFnSc3d1	ozbrojená
mezinárodní	mezinárodní	k2eAgFnSc3d1	mezinárodní
akci	akce	k1gFnSc3	akce
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
sklidil	sklidit	k5eAaPmAgInS	sklidit
kritiku	kritika	k1gFnSc4	kritika
od	od	k7c2	od
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Lubomíra	Lubomír	k1gMnSc2	Lubomír
Zaorálka	Zaorálek	k1gMnSc2	Zaorálek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
eventuální	eventuální	k2eAgMnSc1d1	eventuální
vojenské	vojenský	k2eAgNnSc4d1	vojenské
tažení	tažení	k1gNnSc4	tažení
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
křížovou	křížový	k2eAgFnSc4d1	křížová
výpravu	výprava	k1gFnSc4	výprava
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
naopak	naopak	k6eAd1	naopak
Zaorálkova	Zaorálkův	k2eAgNnSc2d1	Zaorálkovo
slova	slovo	k1gNnSc2	slovo
označil	označit	k5eAaPmAgInS	označit
za	za	k7c4	za
politiku	politika	k1gFnSc4	politika
appeasementu	appeasement	k1gInSc2	appeasement
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
při	při	k7c6	při
nejbližší	blízký	k2eAgFnSc6d3	nejbližší
příležitosti	příležitost	k1gFnSc6	příležitost
věnovat	věnovat	k5eAaPmF	věnovat
symbolický	symbolický	k2eAgInSc4d1	symbolický
Chamberlainův	Chamberlainův	k2eAgInSc4d1	Chamberlainův
deštník	deštník	k1gInSc4	deštník
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
státní	státní	k2eAgFnSc2d1	státní
návštěvy	návštěva	k1gFnSc2	návštěva
Jordánska	Jordánsko	k1gNnSc2	Jordánsko
11.	[number]	k4	11.
února	únor	k1gInSc2	únor
vyjádřil	vyjádřit	k5eAaPmAgInS	vyjádřit
český	český	k2eAgInSc1d1	český
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
rozvoji	rozvoj	k1gInSc6	rozvoj
jordánské	jordánský	k2eAgFnSc2d1	Jordánská
jaderné	jaderný	k2eAgFnSc2d1	jaderná
energetiky	energetika	k1gFnSc2	energetika
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
při	při	k7c6	při
státní	státní	k2eAgFnSc6d1	státní
návštěvě	návštěva	k1gFnSc6	návštěva
Spojených	spojený	k2eAgInPc2d1	spojený
arabských	arabský	k2eAgInPc2d1	arabský
emirátů	emirát	k1gInPc2	emirát
otevřel	otevřít	k5eAaPmAgInS	otevřít
nové	nový	k2eAgNnSc4d1	nové
sídlo	sídlo	k1gNnSc4	sídlo
českého	český	k2eAgNnSc2d1	české
velvyslanectví	velvyslanectví	k1gNnSc2	velvyslanectví
v	v	k7c6	v
Abú	abú	k1gMnSc6	abú
Zabí	Zabí	k1gFnSc2	Zabí
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
1.	[number]	k4	1.
března	březen	k1gInSc2	březen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
prezident	prezident	k1gMnSc1	prezident
odletěl	odletět	k5eAaPmAgMnS	odletět
na	na	k7c4	na
pracovní	pracovní	k2eAgFnSc4d1	pracovní
návštěvu	návštěva	k1gFnSc4	návštěva
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jako	jako	k8xC	jako
historicky	historicky	k6eAd1	historicky
první	první	k4xOgMnSc1	první
evropský	evropský	k2eAgMnSc1d1	evropský
státník	státník	k1gMnSc1	státník
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
lobistické	lobistický	k2eAgFnSc2d1	lobistická
skupiny	skupina	k1gFnSc2	skupina
Americko-izraelský	americkozraelský	k2eAgInSc1d1	americko-izraelský
výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
veřejné	veřejný	k2eAgFnPc4d1	veřejná
záležitosti	záležitost	k1gFnPc4	záležitost
(	(	kIx(	(
<g/>
AIPAC	AIPAC	kA	AIPAC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
přednesl	přednést	k5eAaPmAgMnS	přednést
projev	projev	k1gInSc4	projev
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
pro	pro	k7c4	pro
rozhodnou	rozhodný	k2eAgFnSc4d1	rozhodná
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
akci	akce	k1gFnSc4	akce
proti	proti	k7c3	proti
islámskému	islámský	k2eAgInSc3d1	islámský
terorismu	terorismus	k1gInSc3	terorismus
s	s	k7c7	s
mandátem	mandát	k1gInSc7	mandát
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Publikum	publikum	k1gNnSc1	publikum
jeho	jeho	k3xOp3gInSc4	jeho
projev	projev	k1gInSc4	projev
několikrát	několikrát	k6eAd1	několikrát
přerušilo	přerušit	k5eAaPmAgNnS	přerušit
potleskem	potlesk	k1gInSc7	potlesk
ve	v	k7c4	v
stoje	stoj	k1gInPc4	stoj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
se	se	k3xPyFc4	se
také	také	k9	také
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
premiérem	premiér	k1gMnSc7	premiér
Izraele	Izrael	k1gInSc2	Izrael
Benjaminem	Benjamin	k1gMnSc7	Benjamin
Netanjahuem	Netanjahu	k1gMnSc7	Netanjahu
a	a	k8xC	a
zástupci	zástupce	k1gMnPc7	zástupce
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
Den	den	k1gInSc4	den
vítězství	vítězství	k1gNnSc4	vítězství
9.	[number]	k4	9.
května	květen	k1gInSc2	květen
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Moskvu	Moskva	k1gFnSc4	Moskva
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
ruským	ruský	k2eAgMnSc7d1	ruský
prezidentem	prezident	k1gMnSc7	prezident
Vladimirem	Vladimir	k1gMnSc7	Vladimir
Putinem	Putin	k1gMnSc7	Putin
a	a	k8xC	a
ministrem	ministr	k1gMnSc7	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Sergejem	Sergej	k1gMnSc7	Sergej
Lavrovem	Lavrov	k1gInSc7	Lavrov
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgMnS	být
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
pravicovou	pravicový	k2eAgFnSc7d1	pravicová
opozicí	opozice	k1gFnSc7	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
tam	tam	k6eAd1	tam
nejedu	jet	k5eNaImIp1nS	jet
kvůli	kvůli	k7c3	kvůli
přehlídce	přehlídka	k1gFnSc3	přehlídka
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
tam	tam	k6eAd1	tam
jedu	jet	k5eAaImIp1nS	jet
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
úctu	úcta	k1gFnSc4	úcta
těm	ten	k3xDgMnPc3	ten
padlým	padlý	k2eAgMnPc3d1	padlý
150	[number]	k4	150
tisícům	tisíc	k4xCgInPc3	tisíc
vojáků	voják	k1gMnPc2	voják
.	.	kIx.	.
<g/>
..	..	k?	..
jako	jako	k8xS	jako
výrazu	výraz	k1gInSc3	výraz
vděčnosti	vděčnost	k1gFnSc2	vděčnost
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zemi	zem	k1gFnSc6	zem
nemusíme	muset	k5eNaImIp1nP	muset
mluvit	mluvit	k5eAaImF	mluvit
německy	německy	k6eAd1	německy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
bychom	by	kYmCp1nP	by
byli	být	k5eAaImAgMnP	být
poslušní	poslušný	k2eAgMnPc1d1	poslušný
kolaboranti	kolaborant	k1gMnPc1	kolaborant
árijského	árijský	k2eAgInSc2d1	árijský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Vladimir	Vladimir	k1gInSc1	Vladimir
Putin	putin	k2eAgInSc1d1	putin
Zemanovi	Zeman	k1gMnSc3	Zeman
slíbil	slíbit	k5eAaPmAgMnS	slíbit
pomoc	pomoc	k1gFnSc4	pomoc
při	při	k7c6	při
vymáhání	vymáhání	k1gNnSc6	vymáhání
několikamiliardových	několikamiliardový	k2eAgFnPc2d1	několikamiliardová
pohledávek	pohledávka	k1gFnPc2	pohledávka
českých	český	k2eAgFnPc2d1	Česká
firem	firma	k1gFnPc2	firma
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
vietnamským	vietnamský	k2eAgMnSc7d1	vietnamský
prezidentem	prezident	k1gMnSc7	prezident
Trư	Trư	k1gMnSc7	Trư
Tấ	Tấ	k1gMnSc7	Tấ
Sangem	Sang	k1gMnSc7	Sang
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
přijal	přijmout	k5eAaPmAgMnS	přijmout
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
prezidenta	prezident	k1gMnSc2	prezident
autonomního	autonomní	k2eAgInSc2d1	autonomní
Iráckého	irácký	k2eAgInSc2d1	irácký
Kurdistánu	Kurdistán	k1gInSc2	Kurdistán
Masúda	Masúd	k1gMnSc4	Masúd
Bárzáního	Bárzání	k1gMnSc4	Bárzání
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
poděkoval	poděkovat	k5eAaPmAgInS	poděkovat
za	za	k7c4	za
kurdský	kurdský	k2eAgInSc4d1	kurdský
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
Islámskému	islámský	k2eAgInSc3d1	islámský
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
3.	[number]	k4	3.
září	září	k1gNnSc2	září
2015	[number]	k4	2015
se	se	k3xPyFc4	se
Zeman	Zeman	k1gMnSc1	Zeman
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
vojenské	vojenský	k2eAgFnSc2d1	vojenská
přehlídky	přehlídka	k1gFnSc2	přehlídka
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
70.	[number]	k4	70.
výročí	výročí	k1gNnSc2	výročí
konce	konec	k1gInSc2	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Tichomoří	Tichomoří	k1gNnSc6	Tichomoří
<g/>
.	.	kIx.	.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2016	[number]	k4	2016
v	v	k7c6	v
diskusi	diskuse	k1gFnSc6	diskuse
s	s	k7c7	s
občany	občan	k1gMnPc7	občan
v	v	k7c6	v
Kornaticích	Kornatik	k1gMnPc6	Kornatik
na	na	k7c6	na
Rokycansku	Rokycansko	k1gNnSc6	Rokycansko
přiznal	přiznat	k5eAaPmAgInS	přiznat
výši	výše	k1gFnSc4	výše
výkupného	výkupné	k1gNnSc2	výkupné
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
stát	stát	k1gInSc1	stát
vyplatil	vyplatit	k5eAaPmAgInS	vyplatit
za	za	k7c4	za
dvě	dva	k4xCgFnPc4	dva
české	český	k2eAgFnPc4d1	Česká
dívky	dívka	k1gFnPc4	dívka
unesené	unesený	k2eAgFnPc4d1	unesená
v	v	k7c6	v
Balúčistánu	Balúčistán	k1gInSc6	Balúčistán
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
expert	expert	k1gMnSc1	expert
na	na	k7c4	na
terorismus	terorismus	k1gInSc4	terorismus
a	a	k8xC	a
vyjednávání	vyjednávání	k1gNnSc1	vyjednávání
s	s	k7c7	s
únosci	únosce	k1gMnPc7	únosce
prof.	prof.	kA	prof.
Adam	Adam	k1gMnSc1	Adam
Dolník	Dolník	k1gMnSc1	Dolník
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
strašně	strašně	k6eAd1	strašně
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
<g/>
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tím	ten	k3xDgNnSc7	ten
"	"	kIx"	"
<g/>
stát	stát	k1gInSc1	stát
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
očekávání	očekávání	k1gNnSc4	očekávání
únosců	únosce	k1gMnPc2	únosce
<g/>
,	,	kIx,	,
kolik	kolik	k9	kolik
tak	tak	k9	tak
mohou	moct	k5eAaImIp3nP	moct
dostat	dostat	k5eAaPmF	dostat
peněz	peníze	k1gInPc2	peníze
za	za	k7c4	za
občana	občan	k1gMnSc4	občan
toho	ten	k3xDgInSc2	ten
daného	daný	k2eAgInSc2d1	daný
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
ten	ten	k3xDgMnSc1	ten
člověk	člověk	k1gMnSc1	člověk
bude	být	k5eAaImBp3nS	být
unesen	unést	k5eAaPmNgMnS	unést
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
Velikonoční	velikonoční	k2eAgNnSc4d1	velikonoční
pondělí	pondělí	k1gNnSc4	pondělí
28.	[number]	k4	28.
března	březen	k1gInSc2	březen
2016	[number]	k4	2016
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
přiletěl	přiletět	k5eAaPmAgMnS	přiletět
na	na	k7c4	na
státní	státní	k2eAgFnSc4d1	státní
návštěvu	návštěva	k1gFnSc4	návštěva
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
čínský	čínský	k2eAgMnSc1d1	čínský
prezident	prezident	k1gMnSc1	prezident
Si	se	k3xPyFc3	se
Ťin-pching	Ťinching	k1gInSc4	Ťin-pching
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
třídenní	třídenní	k2eAgFnSc6d1	třídenní
návštěvě	návštěva	k1gFnSc6	návštěva
bylo	být	k5eAaImAgNnS	být
podepsáno	podepsat	k5eAaPmNgNnS	podepsat
30	[number]	k4	30
obchodních	obchodní	k2eAgFnPc2d1	obchodní
dohod	dohoda	k1gFnPc2	dohoda
za	za	k7c4	za
téměř	téměř	k6eAd1	téměř
300	[number]	k4	300
miliard	miliarda	k4xCgFnPc2	miliarda
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
částka	částka	k1gFnSc1	částka
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
velmi	velmi	k6eAd1	velmi
rozdílné	rozdílný	k2eAgFnPc4d1	rozdílná
transakce	transakce	k1gFnPc4	transakce
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
byly	být	k5eAaImAgFnP	být
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
zárodečné	zárodečný	k2eAgFnSc6d1	zárodečná
fázi	fáze	k1gFnSc6	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Čínský	čínský	k2eAgMnSc1d1	čínský
prezident	prezident	k1gMnSc1	prezident
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
spokojenost	spokojenost	k1gFnSc1	spokojenost
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
Cítím	cítit	k5eAaImIp1nS	cítit
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
jako	jako	k8xC	jako
doma	doma	k6eAd1	doma
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pochvalně	pochvalně	k6eAd1	pochvalně
se	se	k3xPyFc4	se
také	také	k9	také
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
o	o	k7c6	o
české	český	k2eAgFnSc6d1	Česká
historii	historie	k1gFnSc6	historie
a	a	k8xC	a
krajině	krajina	k1gFnSc6	krajina
<g/>
.	.	kIx.	.
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
přijel	přijet	k5eAaPmAgMnS	přijet
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
Klausem	Klaus	k1gMnSc7	Klaus
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc7	jejich
doprovodem	doprovod	k1gInSc7	doprovod
se	s	k7c7	s
zpožděním	zpoždění	k1gNnSc7	zpoždění
na	na	k7c4	na
státní	státní	k2eAgInSc4d1	státní
pohřeb	pohřeb	k1gInSc4	pohřeb
bývalého	bývalý	k2eAgMnSc2d1	bývalý
slovenského	slovenský	k2eAgMnSc2d1	slovenský
prezidenta	prezident	k1gMnSc2	prezident
Michala	Michal	k1gMnSc2	Michal
Kováče	Kováč	k1gMnSc2	Kováč
a	a	k8xC	a
na	na	k7c4	na
samotný	samotný	k2eAgInSc4d1	samotný
obřad	obřad	k1gInSc4	obřad
už	už	k6eAd1	už
nebyli	být	k5eNaImAgMnP	být
vpuštěni	vpuštěn	k2eAgMnPc1d1	vpuštěn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
účastnili	účastnit	k5eAaImAgMnP	účastnit
se	se	k3xPyFc4	se
až	až	k9	až
pozdějších	pozdní	k2eAgFnPc2d2	pozdější
částí	část	k1gFnPc2	část
pohřbu	pohřeb	k1gInSc2	pohřeb
<g/>
.	.	kIx.	.
</s>
<s>
Mluvčí	mluvčí	k1gMnSc1	mluvčí
prezidenta	prezident	k1gMnSc2	prezident
Jiří	Jiří	k1gMnSc1	Jiří
Ovčáček	ovčáček	k1gMnSc1	ovčáček
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
důvod	důvod	k1gInSc4	důvod
zpoždění	zpoždění	k1gNnSc2	zpoždění
špatné	špatný	k2eAgNnSc4d1	špatné
počasí	počasí	k1gNnSc4	počasí
a	a	k8xC	a
chybu	chyba	k1gFnSc4	chyba
letových	letový	k2eAgMnPc2d1	letový
dispečerů	dispečer	k1gMnPc2	dispečer
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
důvody	důvod	k1gInPc1	důvod
byly	být	k5eAaImAgInP	být
vyvráceny	vyvrátit	k5eAaPmNgInP	vyvrátit
a	a	k8xC	a
22.	[number]	k4	22.
října	říjen	k1gInSc2	říjen
se	se	k3xPyFc4	se
Ovčáček	ovčáček	k1gMnSc1	ovčáček
za	za	k7c4	za
svá	svůj	k3xOyFgNnPc4	svůj
slova	slovo	k1gNnPc4	slovo
adresovaná	adresovaný	k2eAgNnPc4d1	adresované
letovým	letový	k2eAgInSc7d1	letový
dispečerům	dispečer	k1gMnPc3	dispečer
omluvil	omluvit	k5eAaPmAgMnS	omluvit
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
18.	[number]	k4	18.
října	říjen	k1gInSc2	říjen
2016	[number]	k4	2016
byl	být	k5eAaImAgMnS	být
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
signatářů	signatář	k1gMnPc2	signatář
"	"	kIx"	"
<g/>
Prohlášení	prohlášení	k1gNnSc1	prohlášení
čtyř	čtyři	k4xCgMnPc2	čtyři
nejvyšších	vysoký	k2eAgMnPc2d3	nejvyšší
ústavních	ústavní	k2eAgMnPc2d1	ústavní
činitelů	činitel	k1gMnPc2	činitel
<g/>
"	"	kIx"	"
o	o	k7c6	o
územní	územní	k2eAgFnSc6d1	územní
celistvosti	celistvost	k1gFnSc6	celistvost
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
návštěvou	návštěva	k1gFnSc7	návštěva
dalajlámy	dalajláma	k1gMnSc2	dalajláma
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
prohlášení	prohlášení	k1gNnSc1	prohlášení
bylo	být	k5eAaImAgNnS	být
kritizováno	kritizovat	k5eAaImNgNnS	kritizovat
mnohými	mnohý	k2eAgMnPc7d1	mnohý
politiky	politik	k1gMnPc7	politik
<g/>
,	,	kIx,	,
novináři	novinář	k1gMnPc7	novinář
i	i	k8xC	i
veřejností	veřejnost	k1gFnSc7	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
vysoké	vysoký	k2eAgFnPc1d1	vysoká
školy	škola	k1gFnPc1	škola
a	a	k8xC	a
univerzity	univerzita	k1gFnPc1	univerzita
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
prohlášení	prohlášení	k1gNnSc3	prohlášení
vyvěsily	vyvěsit	k5eAaPmAgFnP	vyvěsit
tibetské	tibetský	k2eAgFnPc1d1	tibetská
vlajky	vlajka	k1gFnPc1	vlajka
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2016	[number]	k4	2016
kancelář	kancelář	k1gFnSc1	kancelář
prezidenta	prezident	k1gMnSc2	prezident
informovala	informovat	k5eAaBmAgFnS	informovat
o	o	k7c4	o
pozvání	pozvání	k1gNnSc4	pozvání
od	od	k7c2	od
dezignovaného	dezignovaný	k2eAgMnSc2d1	dezignovaný
amerického	americký	k2eAgMnSc2d1	americký
prezidenta	prezident	k1gMnSc2	prezident
Donalda	Donald	k1gMnSc2	Donald
Trumpa	Trump	k1gMnSc2	Trump
<g/>
,	,	kIx,	,
nemuselo	muset	k5eNaImAgNnS	muset
se	se	k3xPyFc4	se
však	však	k9	však
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
oficiální	oficiální	k2eAgFnSc4d1	oficiální
pozvánku	pozvánka	k1gFnSc4	pozvánka
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
nový	nový	k2eAgMnSc1d1	nový
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
v	v	k7c6	v
USA	USA	kA	USA
Hynek	Hynek	k1gMnSc1	Hynek
Kmoníček	Kmoníček	k1gMnSc1	Kmoníček
informoval	informovat	k5eAaBmAgMnS	informovat
o	o	k7c4	o
odložení	odložení	k1gNnSc4	odložení
schůzky	schůzka	k1gFnSc2	schůzka
<g/>
.	.	kIx.	.
</s>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
prezidenta	prezident	k1gMnSc2	prezident
Trumpa	Trump	k1gMnSc2	Trump
dostal	dostat	k5eAaPmAgMnS	dostat
dopis	dopis	k1gInSc4	dopis
s	s	k7c7	s
omluvou	omluva	k1gFnSc7	omluva
za	za	k7c4	za
posunutí	posunutí	k1gNnSc4	posunutí
termínu	termín	k1gInSc2	termín
schůzky	schůzka	k1gFnSc2	schůzka
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
16.	[number]	k4	16.
června	červen	k1gInSc2	červen
2017	[number]	k4	2017
přijala	přijmout	k5eAaPmAgFnS	přijmout
Miloše	Miloš	k1gMnSc4	Miloš
Zemana	Zeman	k1gMnSc4	Zeman
i	i	k9	i
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
a	a	k8xC	a
dcerou	dcera	k1gFnSc7	dcera
britská	britský	k2eAgFnSc1d1	britská
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
závěrem	závěrem	k7c2	závěrem
jeho	jeho	k3xOp3gFnSc2	jeho
pětidenní	pětidenní	k2eAgFnSc2d1	pětidenní
státní	státní	k2eAgFnSc2d1	státní
návštěvy	návštěva	k1gFnSc2	návštěva
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
obdržel	obdržet	k5eAaPmAgMnS	obdržet
prezident	prezident	k1gMnSc1	prezident
Zeman	Zeman	k1gMnSc1	Zeman
čestný	čestný	k2eAgInSc4d1	čestný
doktorát	doktorát	k1gInSc4	doktorát
od	od	k7c2	od
Uralské	uralský	k2eAgFnSc2d1	Uralská
federální	federální	k2eAgFnSc2d1	federální
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
ruských	ruský	k2eAgMnPc2d1	ruský
vysokoškolských	vysokoškolský	k2eAgMnPc2d1	vysokoškolský
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
výzkumných	výzkumný	k2eAgNnPc2d1	výzkumné
pracovišť	pracoviště	k1gNnPc2	pracoviště
–	–	k?	–
titul	titul	k1gInSc4	titul
prezidentovi	prezident	k1gMnSc3	prezident
udělila	udělit	k5eAaPmAgFnS	udělit
Vysoká	vysoká	k1gFnSc1	vysoká
škola	škola	k1gFnSc1	škola
ekonomiky	ekonomika	k1gFnSc2	ekonomika
a	a	k8xC	a
managementu	management	k1gInSc2	management
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
univerzitních	univerzitní	k2eAgFnPc2d1	univerzitní
institucí	instituce	k1gFnPc2	instituce
<g/>
;	;	kIx,	;
rektor	rektor	k1gMnSc1	rektor
mu	on	k3xPp3gMnSc3	on
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
poděkoval	poděkovat	k5eAaPmAgMnS	poděkovat
za	za	k7c4	za
jeho	on	k3xPp3gInSc4	on
"	"	kIx"	"
<g/>
neocenitelný	ocenitelný	k2eNgInSc4d1	neocenitelný
přínos	přínos	k1gInSc4	přínos
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
přátelského	přátelský	k2eAgInSc2d1	přátelský
vztahů	vztah	k1gInPc2	vztah
a	a	k8xC	a
vzájemně	vzájemně	k6eAd1	vzájemně
prospěšné	prospěšný	k2eAgFnSc2d1	prospěšná
spolupráce	spolupráce	k1gFnSc2	spolupráce
mezi	mezi	k7c7	mezi
Českou	český	k2eAgFnSc7d1	Česká
republikou	republika	k1gFnSc7	republika
a	a	k8xC	a
Ruskou	ruský	k2eAgFnSc7d1	ruská
federací	federace	k1gFnSc7	federace
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Jmenování	jmenování	k1gNnSc2	jmenování
===	===	k?	===
</s>
</p>
<p>
<s>
Kroky	krok	k1gInPc1	krok
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
při	při	k7c6	při
jmenování	jmenování	k1gNnSc6	jmenování
členů	člen	k1gInPc2	člen
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
a	a	k8xC	a
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
byly	být	k5eAaImAgFnP	být
odbornou	odborný	k2eAgFnSc7d1	odborná
veřejností	veřejnost	k1gFnSc7	veřejnost
hodnoceny	hodnotit	k5eAaImNgInP	hodnotit
kladně	kladně	k6eAd1	kladně
<g/>
.	.	kIx.	.
</s>
<s>
Jmenování	jmenování	k1gNnSc4	jmenování
první	první	k4xOgFnSc2	první
instituce	instituce	k1gFnSc2	instituce
Zeman	Zeman	k1gMnSc1	Zeman
konzultoval	konzultovat	k5eAaImAgMnS	konzultovat
s	s	k7c7	s
jejím	její	k3xOp3gMnSc7	její
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
předsedou	předseda	k1gMnSc7	předseda
Pavlem	Pavel	k1gMnSc7	Pavel
Rychetským	Rychetský	k1gMnSc7	Rychetský
<g/>
,	,	kIx,	,
u	u	k7c2	u
výběru	výběr	k1gInSc2	výběr
druhé	druhý	k4xOgFnSc2	druhý
přihlédl	přihlédnout	k5eAaPmAgMnS	přihlédnout
k	k	k7c3	k
radám	rada	k1gFnPc3	rada
Jiřího	Jiří	k1gMnSc4	Jiří
Rusnoka	Rusnoka	k1gFnSc1	Rusnoka
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Jeho	jeho	k3xOp3gFnSc1	jeho
nominace	nominace	k1gFnSc1	nominace
členů	člen	k1gMnPc2	člen
bankovní	bankovní	k2eAgFnSc2d1	bankovní
rady	rada	k1gFnSc2	rada
ČNB	ČNB	kA	ČNB
nesou	nést	k5eAaImIp3nP	nést
punc	punc	k1gInSc4	punc
dílny	dílna	k1gFnSc2	dílna
Jiřího	Jiří	k1gMnSc2	Jiří
Rusnoka	Rusnoek	k1gMnSc2	Rusnoek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odborně	odborně	k6eAd1	odborně
ani	ani	k8xC	ani
politicky	politicky	k6eAd1	politicky
nejsou	být	k5eNaImIp3nP	být
zpochybnitelné	zpochybnitelný	k2eAgFnPc1d1	zpochybnitelná
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
komentoval	komentovat	k5eAaBmAgMnS	komentovat
výběr	výběr	k1gInSc4	výběr
kandidátů	kandidát	k1gMnPc2	kandidát
vedoucí	vedoucí	k1gMnSc1	vedoucí
analytik	analytik	k1gMnSc1	analytik
UniCredit	UniCredit	k1gMnSc1	UniCredit
Bank	banka	k1gFnPc2	banka
Pavel	Pavel	k1gMnSc1	Pavel
Sobíšek	Sobíšek	k1gMnSc1	Sobíšek
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
ústavního	ústavní	k2eAgMnSc2d1	ústavní
soudce	soudce	k1gMnSc2	soudce
Zeman	Zeman	k1gMnSc1	Zeman
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
i	i	k9	i
Vojtěcha	Vojtěch	k1gMnSc4	Vojtěch
Šimíčka	Šimíček	k1gMnSc4	Šimíček
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
při	při	k7c6	při
schvalování	schvalování	k1gNnSc6	schvalování
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
ocenil	ocenit	k5eAaPmAgMnS	ocenit
i	i	k9	i
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnPc4	jeho
kritická	kritický	k2eAgNnPc4d1	kritické
vyjádření	vyjádření	k1gNnPc4	vyjádření
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
bych	by	kYmCp1nS	by
chtěl	chtít	k5eAaImAgMnS	chtít
ocenit	ocenit	k5eAaPmF	ocenit
jeho	jeho	k3xOp3gFnSc4	jeho
(	(	kIx(	(
<g/>
Šimíčkovu	Šimíčkův	k2eAgFnSc4d1	Šimíčkova
<g/>
)	)	kIx)	)
odvahu	odvaha	k1gFnSc4	odvaha
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
již	již	k6eAd1	již
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
některé	některý	k3yIgInPc4	některý
aspekty	aspekt	k1gInPc4	aspekt
mé	můj	k3xOp1gFnSc2	můj
volební	volební	k2eAgFnSc2d1	volební
kampaně	kampaň	k1gFnSc2	kampaň
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Senát	senát	k1gInSc1	senát
neschválil	schválit	k5eNaPmAgInS	schválit
prezidentovy	prezidentův	k2eAgInPc4d1	prezidentův
návrhy	návrh	k1gInPc4	návrh
jmenovat	jmenovat	k5eAaBmF	jmenovat
ústavními	ústavní	k2eAgFnPc7d1	ústavní
soudci	soudce	k1gMnPc7	soudce
Jana	Jan	k1gMnSc2	Jan
Sváčka	Sváček	k1gMnSc2	Sváček
<g/>
,	,	kIx,	,
Jiřího	Jiří	k1gMnSc2	Jiří
Nykodýma	Nykodý	k2eAgFnPc7d1	Nykodý
a	a	k8xC	a
Miroslava	Miroslav	k1gMnSc4	Miroslav
Výborného	Výborný	k1gMnSc4	Výborný
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnPc1d1	poslední
dva	dva	k4xCgMnPc1	dva
se	se	k3xPyFc4	se
jako	jako	k9	jako
dosavadní	dosavadní	k2eAgMnPc1d1	dosavadní
ústavní	ústavní	k2eAgMnPc1d1	ústavní
soudci	soudce	k1gMnPc1	soudce
podíleli	podílet	k5eAaImAgMnP	podílet
na	na	k7c6	na
zamítnutí	zamítnutí	k1gNnSc6	zamítnutí
návrhu	návrh	k1gInSc2	návrh
ČSSD	ČSSD	kA	ČSSD
na	na	k7c6	na
zrušení	zrušení	k1gNnSc6	zrušení
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
církevních	církevní	k2eAgFnPc6d1	církevní
restitucích	restituce	k1gFnPc6	restituce
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
často	často	k6eAd1	často
zmiňováno	zmiňovat	k5eAaImNgNnS	zmiňovat
jako	jako	k8xC	jako
důvod	důvod	k1gInSc1	důvod
jejich	jejich	k3xOp3gFnSc2	jejich
nízké	nízký	k2eAgFnSc2d1	nízká
podpory	podpora	k1gFnSc2	podpora
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
<g/>
.	.	kIx.	.
<g/>
Již	již	k6eAd1	již
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2013	[number]	k4	2013
započal	započnout	k5eAaPmAgInS	započnout
spor	spor	k1gInSc1	spor
mezi	mezi	k7c7	mezi
Zemanem	Zeman	k1gMnSc7	Zeman
a	a	k8xC	a
ministrem	ministr	k1gMnSc7	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Karlem	Karel	k1gMnSc7	Karel
Schwarzenbergem	Schwarzenberg	k1gMnSc7	Schwarzenberg
o	o	k7c4	o
post	post	k1gInSc4	post
velvyslance	velvyslanec	k1gMnSc4	velvyslanec
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
postu	post	k1gInSc6	post
rád	rád	k6eAd1	rád
viděl	vidět	k5eAaImAgMnS	vidět
bývalou	bývalý	k2eAgFnSc4d1	bývalá
první	první	k4xOgFnSc4	první
dámu	dáma	k1gFnSc4	dáma
Livii	Livie	k1gFnSc4	Livie
Klausovou	Klausová	k1gFnSc4	Klausová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ho	on	k3xPp3gMnSc4	on
podporovala	podporovat	k5eAaImAgFnS	podporovat
během	během	k7c2	během
jeho	jeho	k3xOp3gFnSc2	jeho
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
kampaně	kampaň	k1gFnSc2	kampaň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2013	[number]	k4	2013
Zeman	Zeman	k1gMnSc1	Zeman
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
případně	případně	k6eAd1	případně
obejde	obejít	k5eAaPmIp3nS	obejít
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
a	a	k8xC	a
prosadí	prosadit	k5eAaPmIp3nS	prosadit
Klausovou	Klausová	k1gFnSc4	Klausová
přímo	přímo	k6eAd1	přímo
při	při	k7c6	při
jednání	jednání	k1gNnSc6	jednání
s	s	k7c7	s
premiérem	premiér	k1gMnSc7	premiér
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
neučinil	učinit	k5eNaPmAgMnS	učinit
<g/>
.	.	kIx.	.
</s>
<s>
Jmenování	jmenování	k1gNnSc1	jmenování
Klausové	Klausové	k2eAgFnPc2d1	Klausové
velvyslankyní	velvyslankyně	k1gFnPc2	velvyslankyně
schválila	schválit	k5eAaPmAgFnS	schválit
až	až	k6eAd1	až
Rusnokova	Rusnokův	k2eAgFnSc1d1	Rusnokova
vláda	vláda	k1gFnSc1	vláda
31.	[number]	k4	31.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
v	v	k7c6	v
tajném	tajný	k2eAgNnSc6d1	tajné
hlasování	hlasování	k1gNnSc6	hlasování
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
sám	sám	k3xTgMnSc1	sám
Rusnok	Rusnok	k1gInSc4	Rusnok
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
bez	bez	k7c2	bez
důvěry	důvěra	k1gFnSc2	důvěra
tento	tento	k3xDgInSc1	tento
krok	krok	k1gInSc1	krok
neuskuteční	uskutečnit	k5eNaPmIp3nS	uskutečnit
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2013	[number]	k4	2013
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
odmítá	odmítat	k5eAaImIp3nS	odmítat
jmenovat	jmenovat	k5eAaBmF	jmenovat
literárního	literární	k2eAgMnSc4d1	literární
historika	historik	k1gMnSc4	historik
a	a	k8xC	a
kritika	kritik	k1gMnSc4	kritik
Martina	Martin	k1gMnSc4	Martin
C.	C.	kA	C.
Putnu	putna	k1gFnSc4	putna
profesorem	profesor	k1gMnSc7	profesor
<g/>
,	,	kIx,	,
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
Karlovou	Karlův	k2eAgFnSc7d1	Karlova
univerzitou	univerzita	k1gFnSc7	univerzita
a	a	k8xC	a
splnil	splnit	k5eAaPmAgMnS	splnit
podmínky	podmínka	k1gFnPc4	podmínka
profesury	profesura	k1gFnPc4	profesura
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
své	svůj	k3xOyFgNnSc4	svůj
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
zdůvodnit	zdůvodnit	k5eAaPmF	zdůvodnit
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
možného	možný	k2eAgNnSc2d1	možné
ponížení	ponížení	k1gNnSc2	ponížení
Putny	putna	k1gFnSc2	putna
<g/>
.	.	kIx.	.
</s>
<s>
Média	médium	k1gNnPc1	médium
následně	následně	k6eAd1	následně
spekulovala	spekulovat	k5eAaImAgNnP	spekulovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
důvodem	důvod	k1gInSc7	důvod
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
častá	častý	k2eAgFnSc1d1	častá
kritika	kritika	k1gFnSc1	kritika
Putny	putna	k1gFnSc2	putna
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
Zemana	Zeman	k1gMnSc2	Zeman
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
však	však	k9	však
sám	sám	k3xTgMnSc1	sám
Zeman	Zeman	k1gMnSc1	Zeman
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Putna	putna	k1gFnSc1	putna
nakonec	nakonec	k6eAd1	nakonec
profesorem	profesor	k1gMnSc7	profesor
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
byl	být	k5eAaImAgInS	být
<g/>
,	,	kIx,	,
jmenovací	jmenovací	k2eAgInSc4d1	jmenovací
titul	titul	k1gInSc4	titul
ale	ale	k8xC	ale
nepřevzal	převzít	k5eNaPmAgMnS	převzít
od	od	k7c2	od
Zemana	Zeman	k1gMnSc2	Zeman
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
od	od	k7c2	od
ministra	ministr	k1gMnSc2	ministr
školství	školství	k1gNnSc2	školství
Petra	Petr	k1gMnSc2	Petr
Fialy	Fiala	k1gMnSc2	Fiala
<g/>
.	.	kIx.	.
</s>
<s>
Fiala	Fiala	k1gMnSc1	Fiala
a	a	k8xC	a
Zeman	Zeman	k1gMnSc1	Zeman
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
připraví	připravit	k5eAaPmIp3nS	připravit
novelu	novela	k1gFnSc4	novela
vysokoškolského	vysokoškolský	k2eAgInSc2d1	vysokoškolský
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yQgFnSc2	který
bude	být	k5eAaImBp3nS	být
profesory	profesor	k1gMnPc4	profesor
jmenovat	jmenovat	k5eAaImF	jmenovat
ministr	ministr	k1gMnSc1	ministr
školství	školství	k1gNnSc2	školství
<g/>
.	.	kIx.	.
</s>
<s>
Novela	novela	k1gFnSc1	novela
vysokoškolského	vysokoškolský	k2eAgInSc2d1	vysokoškolský
zákona	zákon	k1gInSc2	zákon
schválená	schválený	k2eAgFnSc1d1	schválená
Poslaneckou	poslanecký	k2eAgFnSc7d1	Poslanecká
sněmovnou	sněmovna	k1gFnSc7	sněmovna
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2016	[number]	k4	2016
nicméně	nicméně	k8xC	nicméně
jmenování	jmenování	k1gNnSc4	jmenování
profesorů	profesor	k1gMnPc2	profesor
prezidentem	prezident	k1gMnSc7	prezident
zachovala	zachovat	k5eAaPmAgFnS	zachovat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2014	[number]	k4	2014
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
oznámil	oznámit	k5eAaPmAgMnS	oznámit
své	svůj	k3xOyFgMnPc4	svůj
kandidáty	kandidát	k1gMnPc4	kandidát
na	na	k7c4	na
post	post	k1gInSc4	post
ombudsmana	ombudsman	k1gMnSc2	ombudsman
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
rezignaci	rezignace	k1gFnSc6	rezignace
Pavla	Pavel	k1gMnSc2	Pavel
Varvařovského	Varvařovský	k1gMnSc2	Varvařovský
nedlouho	dlouho	k6eNd1	dlouho
předtím	předtím	k6eAd1	předtím
uvolnil	uvolnit	k5eAaPmAgInS	uvolnit
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Stanislava	Stanislav	k1gMnSc4	Stanislav
Křečka	křeček	k1gMnSc4	křeček
a	a	k8xC	a
Pavla	Pavel	k1gMnSc4	Pavel
Zářeckého	zářecký	k2eAgMnSc4d1	zářecký
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgMnSc1	druhý
jmenovaný	jmenovaný	k1gMnSc1	jmenovaný
později	pozdě	k6eAd2	pozdě
nominaci	nominace	k1gFnSc4	nominace
z	z	k7c2	z
rodinných	rodinný	k2eAgInPc2d1	rodinný
důvodů	důvod	k1gInPc2	důvod
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
hlasy	hlas	k1gInPc1	hlas
37	[number]	k4	37
svých	svůj	k3xOyFgInPc2	svůj
členů	člen	k1gInPc2	člen
potom	potom	k8xC	potom
Poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc6	sněmovna
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
jako	jako	k8xS	jako
svoji	svůj	k3xOyFgFnSc4	svůj
kandidátku	kandidátka	k1gFnSc4	kandidátka
Annu	Anna	k1gFnSc4	Anna
Šabatovou	Šabatová	k1gFnSc4	Šabatová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
volby	volba	k1gFnSc2	volba
13.	[number]	k4	13.
února	únor	k1gInSc2	únor
2014	[number]	k4	2014
získala	získat	k5eAaPmAgFnS	získat
více	hodně	k6eAd2	hodně
hlasů	hlas	k1gInPc2	hlas
Šabatová	Šabatová	k1gFnSc1	Šabatová
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
většinu	většina	k1gFnSc4	většina
hlasujících	hlasující	k1gMnPc2	hlasující
nutnou	nutný	k2eAgFnSc4d1	nutná
ke	k	k7c3	k
zvolení	zvolení	k1gNnSc3	zvolení
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
druhé	druhý	k4xOgNnSc1	druhý
kolo	kolo	k1gNnSc1	kolo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
již	již	k6eAd1	již
Šabatová	Šabatová	k1gFnSc1	Šabatová
získala	získat	k5eAaPmAgFnS	získat
85	[number]	k4	85
hlasů	hlas	k1gInPc2	hlas
od	od	k7c2	od
168	[number]	k4	168
přítomných	přítomný	k2eAgMnPc2d1	přítomný
poslanců	poslanec	k1gMnPc2	poslanec
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
veřejnou	veřejný	k2eAgFnSc7d1	veřejná
ochránkyní	ochránkyně	k1gFnSc7	ochránkyně
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Křeček	křeček	k1gMnSc1	křeček
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
stal	stát	k5eAaPmAgMnS	stát
jejím	její	k3xOp3gMnSc7	její
zástupcem	zástupce	k1gMnSc7	zástupce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
5.	[number]	k4	5.
května	květen	k1gInSc2	květen
2015	[number]	k4	2015
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
podepsat	podepsat	k5eAaPmF	podepsat
jmenování	jmenování	k1gNnSc4	jmenování
tří	tři	k4xCgMnPc2	tři
nových	nový	k2eAgMnPc2d1	nový
profesorů	profesor	k1gMnPc2	profesor
<g/>
,	,	kIx,	,
dosavadních	dosavadní	k2eAgMnPc2d1	dosavadní
docentů	docent	k1gMnPc2	docent
Jiřího	Jiří	k1gMnSc2	Jiří
Fajta	Fajt	k1gMnSc2	Fajt
<g/>
,	,	kIx,	,
Ivana	Ivan	k1gMnSc2	Ivan
Ošťádala	ošťádat	k5eAaImAgFnS	ošťádat
a	a	k8xC	a
Jana	Jana	k1gFnSc1	Jana
Eichlera	Eichler	k1gMnSc2	Eichler
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
odůvodnění	odůvodnění	k1gNnSc4	odůvodnění
uvedl	uvést	k5eAaPmAgMnS	uvést
tiskový	tiskový	k2eAgMnSc1d1	tiskový
mluvčí	mluvčí	k1gMnSc1	mluvčí
prezidenta	prezident	k1gMnSc2	prezident
Ovčáček	ovčáček	k1gMnSc1	ovčáček
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Fajta	Fajt	k1gInSc2	Fajt
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yRgFnSc2	který
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
je	být	k5eAaImIp3nS	být
Fajt	Fajt	k1gMnSc1	Fajt
ředitelem	ředitel	k1gMnSc7	ředitel
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
navrhovala	navrhovat	k5eAaImAgFnS	navrhovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
milion	milion	k4xCgInSc1	milion
korun	koruna	k1gFnPc2	koruna
z	z	k7c2	z
několikamilionového	několikamilionový	k2eAgInSc2d1	několikamilionový
sponzorského	sponzorský	k2eAgInSc2d1	sponzorský
daru	dar	k1gInSc2	dar
Komerční	komerční	k2eAgFnSc2d1	komerční
banky	banka	k1gFnSc2	banka
sloužil	sloužit	k5eAaImAgInS	sloužit
jako	jako	k9	jako
příspěvek	příspěvek	k1gInSc1	příspěvek
k	k	k7c3	k
tabulkovému	tabulkový	k2eAgInSc3d1	tabulkový
platu	plat	k1gInSc3	plat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
docenta	docent	k1gMnSc2	docent
Eichlera	Eichler	k1gMnSc2	Eichler
bylo	být	k5eAaImAgNnS	být
jako	jako	k9	jako
důvod	důvod	k1gInSc4	důvod
uvedeno	uveden	k2eAgNnSc1d1	uvedeno
působení	působení	k1gNnSc1	působení
v	v	k7c6	v
propagandistickém	propagandistický	k2eAgInSc6d1	propagandistický
aparátu	aparát	k1gInSc6	aparát
Československé	československý	k2eAgFnSc2d1	Československá
lidové	lidový	k2eAgFnSc2d1	lidová
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Ošťádala	ošťádat	k5eAaImAgFnS	ošťádat
předlistopadové	předlistopadový	k2eAgInPc4d1	předlistopadový
kontakty	kontakt	k1gInPc4	kontakt
na	na	k7c4	na
StB	StB	k1gFnSc4	StB
<g/>
.	.	kIx.	.
</s>
<s>
Rektor	rektor	k1gMnSc1	rektor
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
Tomáš	Tomáš	k1gMnSc1	Tomáš
Zima	zima	k6eAd1	zima
označil	označit	k5eAaPmAgMnS	označit
tento	tento	k3xDgInSc4	tento
krok	krok	k1gInSc4	krok
za	za	k7c4	za
znevážení	znevážení	k1gNnSc4	znevážení
akademické	akademický	k2eAgFnSc2d1	akademická
obce	obec	k1gFnSc2	obec
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
nepřijatelný	přijatelný	k2eNgInSc1d1	nepřijatelný
zásah	zásah	k1gInSc1	zásah
do	do	k7c2	do
principů	princip	k1gInPc2	princip
akademických	akademický	k2eAgFnPc2d1	akademická
svobod	svoboda	k1gFnPc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
konference	konference	k1gFnSc1	konference
rektorů	rektor	k1gMnPc2	rektor
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
předeslala	předeslat	k5eAaPmAgFnS	předeslat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ceremoniálu	ceremoniál	k1gInSc2	ceremoniál
předávání	předávání	k1gNnSc2	předávání
dekretů	dekret	k1gInPc2	dekret
nezúčastní	zúčastnit	k5eNaPmIp3nS	zúčastnit
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
následující	následující	k2eAgInSc1d1	následující
den	den	k1gInSc1	den
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
ministr	ministr	k1gMnSc1	ministr
školství	školství	k1gNnSc2	školství
Marcel	Marcel	k1gMnSc1	Marcel
Chládek	Chládek	k1gMnSc1	Chládek
zrušil	zrušit	k5eAaPmAgMnS	zrušit
<g/>
.	.	kIx.	.
<g/>
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
a	a	k8xC	a
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
se	s	k7c7	s
třemi	tři	k4xCgMnPc7	tři
kandidáty	kandidát	k1gMnPc7	kandidát
na	na	k7c4	na
profesory	profesor	k1gMnPc4	profesor
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
Zeman	Zeman	k1gMnSc1	Zeman
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
jmenovat	jmenovat	k5eAaBmF	jmenovat
<g/>
,	,	kIx,	,
podaly	podat	k5eAaPmAgFnP	podat
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
správní	správní	k2eAgFnSc4d1	správní
žalobu	žaloba	k1gFnSc4	žaloba
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gInSc2	jejich
názoru	názor	k1gInSc2	názor
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
nepřipouští	připouštět	k5eNaImIp3nS	připouštět
možnost	možnost	k1gFnSc1	možnost
nepodepsat	podepsat	k5eNaPmF	podepsat
jmenovací	jmenovací	k2eAgInSc4d1	jmenovací
dekret	dekret	k1gInSc4	dekret
<g/>
.	.	kIx.	.
</s>
<s>
Navrhly	navrhnout	k5eAaPmAgFnP	navrhnout
Městskému	městský	k2eAgInSc3d1	městský
soudu	soud	k1gInSc3	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
Miloši	Miloš	k1gMnSc3	Miloš
Zemanovi	Zeman	k1gMnSc3	Zeman
jmenování	jmenování	k1gNnSc4	jmenování
profesorů	profesor	k1gMnPc2	profesor
uložil	uložit	k5eAaPmAgMnS	uložit
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Eichler	Eichler	k1gMnSc1	Eichler
žalobu	žaloba	k1gFnSc4	žaloba
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Soud	soud	k1gInSc1	soud
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2016	[number]	k4	2016
návrhu	návrh	k1gInSc2	návrh
nevyhověl	vyhovět	k5eNaPmAgMnS	vyhovět
<g/>
,	,	kIx,	,
Fajt	Fajt	k1gMnSc1	Fajt
a	a	k8xC	a
Ošťádal	ošťádat	k5eAaImAgMnS	ošťádat
se	se	k3xPyFc4	se
ale	ale	k9	ale
hodlají	hodlat	k5eAaImIp3nP	hodlat
dovolat	dovolat	k5eAaPmF	dovolat
k	k	k7c3	k
Nejvyššímu	vysoký	k2eAgInSc3d3	Nejvyšší
správnímu	správní	k2eAgInSc3d1	správní
soudu	soud	k1gInSc3	soud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Vetování	vetování	k1gNnPc2	vetování
zákonů	zákon	k1gInPc2	zákon
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Ohledně	ohledně	k7c2	ohledně
prezidentského	prezidentský	k2eAgNnSc2d1	prezidentské
veta	veto	k1gNnSc2	veto
se	se	k3xPyFc4	se
Zeman	Zeman	k1gMnSc1	Zeman
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
parlamentu	parlament	k1gInSc3	parlament
návrhy	návrh	k1gInPc4	návrh
zákonů	zákon	k1gInPc2	zákon
vracel	vracet	k5eAaImAgMnS	vracet
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
"	"	kIx"	"
<g/>
legislativní	legislativní	k2eAgInPc4d1	legislativní
zmetky	zmetek	k1gInPc4	zmetek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
veto	veto	k1gNnSc1	veto
během	během	k7c2	během
7.	[number]	k4	7.
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
na	na	k7c4	na
navrhovaný	navrhovaný	k2eAgInSc4d1	navrhovaný
zákon	zákon	k1gInSc4	zákon
o	o	k7c6	o
státní	státní	k2eAgFnSc6d1	státní
službě	služba	k1gFnSc6	služba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vláda	vláda	k1gFnSc1	vláda
návrh	návrh	k1gInSc1	návrh
připravila	připravit	k5eAaPmAgFnS	připravit
<g/>
,	,	kIx,	,
Zeman	Zeman	k1gMnSc1	Zeman
tento	tento	k3xDgInSc4	tento
zákon	zákon	k1gInSc4	zákon
zkritizoval	zkritizovat	k5eAaPmAgMnS	zkritizovat
a	a	k8xC	a
politické	politický	k2eAgMnPc4d1	politický
náměstky	náměstek	k1gMnPc4	náměstek
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgInPc7	který
tento	tento	k3xDgInSc4	tento
zákon	zákon	k1gInSc1	zákon
počítá	počítat	k5eAaImIp3nS	počítat
<g/>
,	,	kIx,	,
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
budižkničemy	budižkničem	k1gInPc4	budižkničem
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
ve	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
předeslal	předeslat	k5eAaPmAgMnS	předeslat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
vláda	vláda	k1gFnSc1	vláda
politické	politický	k2eAgFnSc2d1	politická
náměstky	náměstka	k1gFnSc2	náměstka
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
neodstraní	odstranit	k5eNaPmIp3nS	odstranit
<g/>
,	,	kIx,	,
zákon	zákon	k1gInSc1	zákon
nechá	nechat	k5eAaPmIp3nS	nechat
vetovat	vetovat	k5eAaBmF	vetovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
jestli	jestli	k8xS	jestli
Sněmovna	sněmovna	k1gFnSc1	sněmovna
jeho	jeho	k3xOp3gNnSc4	jeho
veto	veto	k1gNnSc4	veto
přehlasuje	přehlasovat	k5eAaBmIp3nS	přehlasovat
<g/>
,	,	kIx,	,
podá	podat	k5eAaPmIp3nS	podat
stížnost	stížnost	k1gFnSc1	stížnost
na	na	k7c4	na
Ústavní	ústavní	k2eAgInSc4d1	ústavní
soud	soud	k1gInSc4	soud
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
Zeman	Zeman	k1gMnSc1	Zeman
zákon	zákon	k1gInSc1	zákon
skutečně	skutečně	k6eAd1	skutečně
vetoval	vetovat	k5eAaBmAgInS	vetovat
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
Sněmovna	sněmovna	k1gFnSc1	sněmovna
jeho	jeho	k3xOp3gNnSc4	jeho
veto	veto	k1gNnSc4	veto
přehlasovala	přehlasovat	k5eAaBmAgFnS	přehlasovat
<g/>
.	.	kIx.	.
7.	[number]	k4	7.
listopadu	listopad	k1gInSc2	listopad
podal	podat	k5eAaPmAgMnS	podat
prezident	prezident	k1gMnSc1	prezident
Zeman	Zeman	k1gMnSc1	Zeman
k	k	k7c3	k
Ústavnímu	ústavní	k2eAgInSc3d1	ústavní
soudu	soud	k1gInSc3	soud
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
zrušení	zrušení	k1gNnSc4	zrušení
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
státní	státní	k2eAgFnSc6d1	státní
službě	služba	k1gFnSc6	služba
<g/>
.	.	kIx.	.
</s>
<s>
Soud	soud	k1gInSc1	soud
však	však	k9	však
30.	[number]	k4	30.
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
zrušil	zrušit	k5eAaPmAgMnS	zrušit
jen	jen	k9	jen
jedno	jeden	k4xCgNnSc4	jeden
jeho	jeho	k3xOp3gFnSc4	jeho
ustanovení	ustanovení	k1gNnPc1	ustanovení
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
návrh	návrh	k1gInSc1	návrh
zamítl	zamítnout	k5eAaPmAgInS	zamítnout
a	a	k8xC	a
zákon	zákon	k1gInSc1	zákon
ponechal	ponechat	k5eAaPmAgInS	ponechat
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
rovněž	rovněž	k9	rovněž
vetoval	vetovat	k5eAaBmAgMnS	vetovat
například	například	k6eAd1	například
zákon	zákon	k1gInSc4	zákon
o	o	k7c6	o
střetu	střet	k1gInSc6	střet
zájmů	zájem	k1gInPc2	zájem
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
mediálně	mediálně	k6eAd1	mediálně
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
"	"	kIx"	"
<g/>
lex	lex	k?	lex
Babiš	Babiš	k1gInSc1	Babiš
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Milosti	milost	k1gFnSc2	milost
a	a	k8xC	a
amnestie	amnestie	k1gFnSc2	amnestie
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
poinauguračním	poinaugurační	k2eAgNnSc6d1	poinaugurační
prohlášení	prohlášení	k1gNnSc6	prohlášení
<g/>
,	,	kIx,	,
předneseném	přednesený	k2eAgNnSc6d1	přednesené
na	na	k7c6	na
Hradčanském	hradčanský	k2eAgNnSc6d1	Hradčanské
náměstí	náměstí	k1gNnSc6	náměstí
vedle	vedle	k7c2	vedle
sochy	socha	k1gFnSc2	socha
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
,	,	kIx,	,
stran	stran	k7c2	stran
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
milosti	milost	k1gFnSc2	milost
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
i	i	k9	i
v	v	k7c6	v
Ústavě	ústav	k1gInSc6	ústav
ČR	ČR	kA	ČR
existují	existovat	k5eAaImIp3nP	existovat
zbytečné	zbytečný	k2eAgInPc1d1	zbytečný
monarchistické	monarchistický	k2eAgInPc1d1	monarchistický
prvky	prvek	k1gInPc1	prvek
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
právo	právo	k1gNnSc1	právo
prezidenta	prezident	k1gMnSc2	prezident
ČR	ČR	kA	ČR
udělovat	udělovat	k5eAaImF	udělovat
amnestii	amnestie	k1gFnSc4	amnestie
nebo	nebo	k8xC	nebo
milosti	milost	k1gFnPc4	milost
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
za	za	k7c4	za
sebe	sebe	k3xPyFc4	sebe
se	se	k3xPyFc4	se
těchto	tento	k3xDgNnPc2	tento
práv	právo	k1gNnPc2	právo
dobrovolně	dobrovolně	k6eAd1	dobrovolně
vzdávám	vzdávat	k5eAaImIp1nS	vzdávat
a	a	k8xC	a
přál	přát	k5eAaImAgMnS	přát
bych	by	kYmCp1nS	by
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
změněna	změněn	k2eAgFnSc1d1	změněna
Ústava	ústava	k1gFnSc1	ústava
takovým	takový	k3xDgInSc7	takový
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jakýkoli	jakýkoli	k3yIgMnSc1	jakýkoli
můj	můj	k3xOp1gMnSc1	můj
nástupce	nástupce	k1gMnSc1	nástupce
nemohl	moct	k5eNaImAgMnS	moct
institut	institut	k1gInSc4	institut
amnestie	amnestie	k1gFnSc2	amnestie
nebo	nebo	k8xC	nebo
milosti	milost	k1gFnSc2	milost
jakýmkoli	jakýkoli	k3yIgInSc7	jakýkoli
způsobem	způsob	k1gInSc7	způsob
zneužít	zneužít	k5eAaPmF	zneužít
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
mých	můj	k3xOp1gInPc2	můj
závazků	závazek	k1gInPc2	závazek
mé	můj	k3xOp1gFnSc2	můj
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Svoji	svůj	k3xOyFgFnSc4	svůj
pravomoc	pravomoc	k1gFnSc4	pravomoc
provádět	provádět	k5eAaImF	provádět
řízení	řízení	k1gNnSc4	řízení
o	o	k7c6	o
žádostech	žádost	k1gFnPc6	žádost
o	o	k7c4	o
milost	milost	k1gFnSc4	milost
a	a	k8xC	a
zamítat	zamítat	k5eAaImF	zamítat
žádosti	žádost	k1gFnPc4	žádost
převedl	převést	k5eAaPmAgInS	převést
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2013	[number]	k4	2013
na	na	k7c4	na
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
,	,	kIx,	,
sobě	se	k3xPyFc3	se
ponechal	ponechat	k5eAaPmAgInS	ponechat
výjimku	výjimka	k1gFnSc4	výjimka
posouzení	posouzení	k1gNnSc2	posouzení
"	"	kIx"	"
<g/>
žádostí	žádost	k1gFnSc7	žádost
osob	osoba	k1gFnPc2	osoba
trpících	trpící	k2eAgFnPc2d1	trpící
závažnou	závažný	k2eAgFnSc7d1	závažná
chorobou	choroba	k1gFnSc7	choroba
nebo	nebo	k8xC	nebo
nevyléčitelnou	vyléčitelný	k2eNgFnSc7d1	nevyléčitelná
chorobou	choroba	k1gFnSc7	choroba
bezprostředně	bezprostředně	k6eAd1	bezprostředně
ohrožující	ohrožující	k2eAgMnSc1d1	ohrožující
<g />
.	.	kIx.	.
</s>
<s>
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nebudou	být	k5eNaImBp3nP	být
soudem	soud	k1gInSc7	soud
shledány	shledán	k2eAgFnPc4d1	shledána
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
užití	užití	k1gNnSc4	užití
ustanovení	ustanovení	k1gNnSc2	ustanovení
§	§	k?	§
327	[number]	k4	327
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
4	[number]	k4	4
trestního	trestní	k2eAgInSc2d1	trestní
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
De	De	k?	De
facto	facto	k1gNnSc1	facto
tak	tak	k8xS	tak
prezident	prezident	k1gMnSc1	prezident
bude	být	k5eAaImBp3nS	být
určovat	určovat	k5eAaImF	určovat
případ	případ	k1gInSc4	případ
od	od	k7c2	od
případu	případ	k1gInSc2	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
sám	sám	k3xTgMnSc1	sám
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
bude	být	k5eAaImBp3nS	být
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
<g/>
.	.	kIx.	.
<g/>
Zeman	Zeman	k1gMnSc1	Zeman
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
prezidentství	prezidentství	k1gNnSc2	prezidentství
udělil	udělit	k5eAaPmAgInS	udělit
oproti	oproti	k7c3	oproti
svým	svůj	k3xOyFgMnPc3	svůj
předchůdcům	předchůdce	k1gMnPc3	předchůdce
milostí	milost	k1gFnPc2	milost
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
milost	milost	k1gFnSc4	milost
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2016	[number]	k4	2016
muž	muž	k1gMnSc1	muž
odsouzený	odsouzený	k1gMnSc1	odsouzený
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
za	za	k7c4	za
méně	málo	k6eAd2	málo
závažnou	závažný	k2eAgFnSc4d1	závažná
majetkovou	majetkový	k2eAgFnSc4d1	majetková
trestnou	trestný	k2eAgFnSc4d1	trestná
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
podmíněně	podmíněně	k6eAd1	podmíněně
propustili	propustit	k5eAaPmAgMnP	propustit
do	do	k7c2	do
nemocničního	nemocniční	k2eAgNnSc2d1	nemocniční
ošetřování	ošetřování	k1gNnSc2	ošetřování
<g/>
.	.	kIx.	.
</s>
<s>
Druhému	druhý	k4xOgMnSc3	druhý
omilostněnému	omilostněný	k2eAgMnSc3d1	omilostněný
prominul	prominout	k5eAaPmAgInS	prominout
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
zbytek	zbytek	k1gInSc1	zbytek
jednoho	jeden	k4xCgInSc2	jeden
trestu	trest	k1gInSc2	trest
a	a	k8xC	a
tři	tři	k4xCgInPc4	tři
celé	celý	k2eAgInPc4d1	celý
tresty	trest	k1gInPc4	trest
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
v	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
výši	výše	k1gFnSc6	výše
zhruba	zhruba	k6eAd1	zhruba
29	[number]	k4	29
měsíců	měsíc	k1gInPc2	měsíc
za	za	k7c4	za
maření	maření	k1gNnSc4	maření
výkonu	výkon	k1gInSc2	výkon
úředního	úřední	k2eAgNnSc2d1	úřední
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
a	a	k8xC	a
vykázání	vykázání	k1gNnSc2	vykázání
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
třetího	třetí	k4xOgNnSc2	třetí
zastavil	zastavit	k5eAaPmAgInS	zastavit
září	září	k1gNnSc4	září
2016	[number]	k4	2016
trestní	trestní	k2eAgInSc1d1	trestní
stíhání	stíhání	k1gNnSc3	stíhání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
papeže	papež	k1gMnSc2	papež
Františka	František	k1gMnSc2	František
nechal	nechat	k5eAaPmAgMnS	nechat
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2016	[number]	k4	2016
propustit	propustit	k5eAaPmF	propustit
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
mladou	mladý	k2eAgFnSc4d1	mladá
ženu	žena	k1gFnSc4	žena
starající	starající	k2eAgFnSc4d1	starající
se	se	k3xPyFc4	se
o	o	k7c4	o
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Žena	žena	k1gFnSc1	žena
odsouzená	odsouzená	k1gFnSc1	odsouzená
ze	z	k7c2	z
zpronevěry	zpronevěra	k1gFnSc2	zpronevěra
nesplňovala	splňovat	k5eNaImAgFnS	splňovat
prezidentovu	prezidentův	k2eAgFnSc4d1	prezidentova
podmínku	podmínka	k1gFnSc4	podmínka
zdravotního	zdravotní	k2eAgInSc2d1	zdravotní
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
měsíce	měsíc	k1gInSc2	měsíc
Zeman	Zeman	k1gMnSc1	Zeman
omilostnil	omilostnit	k5eAaPmAgMnS	omilostnit
i	i	k9	i
ženu	žena	k1gFnSc4	žena
trestanou	trestaný	k2eAgFnSc4d1	trestaná
za	za	k7c4	za
podvod	podvod	k1gInSc4	podvod
<g/>
,	,	kIx,	,
zpronevěru	zpronevěra	k1gFnSc4	zpronevěra
a	a	k8xC	a
trestnou	trestný	k2eAgFnSc4d1	trestná
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgMnS	dočkat
milosti	milost	k1gFnSc2	milost
muž	muž	k1gMnSc1	muž
trpící	trpící	k2eAgMnSc1d1	trpící
onkologických	onkologický	k2eAgInPc2d1	onkologický
onemocněním	onemocnění	k1gNnPc3	onemocnění
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
mluvčí	mluvčí	k1gMnSc1	mluvčí
Ovčáček	ovčáček	k1gMnSc1	ovčáček
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
jako	jako	k9	jako
o	o	k7c6	o
"	"	kIx"	"
<g/>
značném	značný	k2eAgMnSc6d1	značný
recidivistovi	recidivista	k1gMnSc6	recidivista
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sedmá	sedmý	k4xOgFnSc1	sedmý
milost	milost	k1gFnSc1	milost
se	se	k3xPyFc4	se
týkala	týkat	k5eAaImAgFnS	týkat
muže	muž	k1gMnSc4	muž
odsouzeného	odsouzený	k1gMnSc4	odsouzený
za	za	k7c4	za
majetkové	majetkový	k2eAgInPc4d1	majetkový
zločiny	zločin	k1gInPc4	zločin
a	a	k8xC	a
nedovolené	dovolený	k2eNgNnSc4d1	nedovolené
ozbrojování	ozbrojování	k1gNnSc4	ozbrojování
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
rakovinu	rakovina	k1gFnSc4	rakovina
<g/>
.	.	kIx.	.
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2017	[number]	k4	2017
udělil	udělit	k5eAaPmAgMnS	udělit
milost	milost	k1gFnSc4	milost
doživotně	doživotně	k6eAd1	doživotně
odsouzenému	odsouzený	k2eAgMnSc3d1	odsouzený
dvojnásobnému	dvojnásobný	k2eAgMnSc3d1	dvojnásobný
vrahovi	vrah	k1gMnSc3	vrah
Jiřímu	Jiří	k1gMnSc3	Jiří
Kajínkovi	Kajínek	k1gMnSc3	Kajínek
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tento	tento	k3xDgInSc1	tento
záměr	záměr	k1gInSc1	záměr
ohlásil	ohlásit	k5eAaPmAgInS	ohlásit
již	již	k6eAd1	již
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
dubnové	dubnový	k2eAgFnSc2d1	dubnová
návštěvy	návštěva	k1gFnSc2	návštěva
Čáslavi	Čáslav	k1gFnSc2	Čáslav
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
návštěvě	návštěva	k1gFnSc6	návštěva
Blovic	Blovice	k1gInPc2	Blovice
na	na	k7c6	na
Plzeňsku	Plzeňsko	k1gNnSc6	Plzeňsko
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2016	[number]	k4	2016
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
ještě	ještě	k9	ještě
milost	milost	k1gFnSc4	milost
pro	pro	k7c4	pro
Kajínka	Kajínek	k1gMnSc4	Kajínek
vylučoval	vylučovat	k5eAaImAgMnS	vylučovat
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Pan	Pan	k1gMnSc1	Pan
Kajínek	Kajínek	k1gMnSc1	Kajínek
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
a	a	k8xC	a
vážně	vážně	k6eAd1	vážně
nemocný	mocný	k2eNgMnSc1d1	nemocný
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mu	on	k3xPp3gMnSc3	on
milost	milost	k1gFnSc1	milost
neudělím	udělit	k5eNaPmIp1nS	udělit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Udělení	udělení	k1gNnSc4	udělení
milosti	milost	k1gFnSc2	milost
Zeman	Zeman	k1gMnSc1	Zeman
vysvětloval	vysvětlovat	k5eAaImAgMnS	vysvětlovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
případu	případ	k1gInSc6	případ
existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
nejasností	nejasnost	k1gFnPc2	nejasnost
a	a	k8xC	a
že	že	k8xS	že
Kajínek	Kajínek	k1gMnSc1	Kajínek
vykonal	vykonat	k5eAaPmAgMnS	vykonat
už	už	k6eAd1	už
23	[number]	k4	23
let	léto	k1gNnPc2	léto
trestu	trest	k1gInSc2	trest
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
agentury	agentura	k1gFnSc2	agentura
Median	Mediana	k1gFnPc2	Mediana
s	s	k7c7	s
amnestií	amnestie	k1gFnSc7	amnestie
zcela	zcela	k6eAd1	zcela
nebo	nebo	k8xC	nebo
spíše	spíše	k9	spíše
souhlasilo	souhlasit	k5eAaImAgNnS	souhlasit
54	[number]	k4	54
%	%	kIx~	%
dotázaných	dotázaný	k2eAgFnPc2d1	dotázaná
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
pohnutku	pohnutka	k1gFnSc4	pohnutka
prezidenta	prezident	k1gMnSc2	prezident
uvedlo	uvést	k5eAaPmAgNnS	uvést
39	[number]	k4	39
%	%	kIx~	%
dotazovaných	dotazovaný	k2eAgFnPc6d1	dotazovaná
snahu	snaha	k1gFnSc4	snaha
o	o	k7c4	o
zvýšení	zvýšení	k1gNnSc4	zvýšení
popularity	popularita	k1gFnSc2	popularita
<g/>
,	,	kIx,	,
34	[number]	k4	34
%	%	kIx~	%
víru	víra	k1gFnSc4	víra
prezidenta	prezident	k1gMnSc2	prezident
v	v	k7c4	v
jeho	jeho	k3xOp3gFnSc4	jeho
nevinu	nevina	k1gFnSc4	nevina
<g/>
,	,	kIx,	,
29	[number]	k4	29
%	%	kIx~	%
dotazovaných	dotazovaný	k2eAgMnPc2d1	dotazovaný
si	se	k3xPyFc3	se
potom	potom	k8xC	potom
myslelo	myslet	k5eAaImAgNnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
tím	ten	k3xDgNnSc7	ten
chce	chtít	k5eAaImIp3nS	chtít
prezident	prezident	k1gMnSc1	prezident
odlákat	odlákat	k5eAaPmF	odlákat
pozornost	pozornost	k1gFnSc4	pozornost
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tehdy	tehdy	k6eAd1	tehdy
probíhající	probíhající	k2eAgFnSc2d1	probíhající
vládní	vládní	k2eAgFnSc2d1	vládní
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
<g/>
Devátou	devátý	k4xOgFnSc4	devátý
milost	milost	k1gFnSc4	milost
udělil	udělit	k5eAaPmAgMnS	udělit
1.	[number]	k4	1.
března	březen	k1gInSc2	březen
2018	[number]	k4	2018
muži	muž	k1gMnPc7	muž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
odpykával	odpykávat	k5eAaImAgMnS	odpykávat
trest	trest	k1gInSc4	trest
za	za	k7c4	za
ohrožení	ohrožení	k1gNnSc4	ohrožení
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
návykové	návykový	k2eAgFnSc2d1	návyková
látky	látka	k1gFnSc2	látka
a	a	k8xC	a
usmrcení	usmrcení	k1gNnSc2	usmrcení
z	z	k7c2	z
nedbalosti	nedbalost	k1gFnSc2	nedbalost
<g/>
.	.	kIx.	.
</s>
<s>
Odsouzený	odsouzený	k1gMnSc1	odsouzený
způsobil	způsobit	k5eAaPmAgMnS	způsobit
dopravní	dopravní	k2eAgFnSc4d1	dopravní
nehodu	nehoda	k1gFnSc4	nehoda
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
zahynula	zahynout	k5eAaPmAgFnS	zahynout
jeho	jeho	k3xOp3gMnPc3	jeho
manželka	manželka	k1gFnSc1	manželka
a	a	k8xC	a
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
udělení	udělení	k1gNnSc4	udělení
milosti	milost	k1gFnSc2	milost
byl	být	k5eAaImAgMnS	být
druhý	druhý	k4xOgMnSc1	druhý
nezletilý	zletilý	k2eNgMnSc1d1	nezletilý
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
při	při	k7c6	při
nehodě	nehoda	k1gFnSc6	nehoda
a	a	k8xC	a
uvězněním	uvěznění	k1gNnSc7	uvěznění
otce	otec	k1gMnSc2	otec
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
nejbližší	blízký	k2eAgMnPc4d3	nejbližší
rodinné	rodinný	k2eAgMnPc4d1	rodinný
příslušníky	příslušník	k1gMnPc4	příslušník
<g/>
.	.	kIx.	.
</s>
<s>
Novinky.cz	Novinky.cz	k1gInSc4	Novinky.cz
připomněly	připomnět	k5eAaPmAgFnP	připomnět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
již	již	k6eAd1	již
o	o	k7c4	o
třetí	třetí	k4xOgNnSc4	třetí
porušení	porušení	k1gNnSc4	porušení
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
udělování	udělování	k1gNnSc4	udělování
milostí	milost	k1gFnPc2	milost
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
stanovil	stanovit	k5eAaPmAgInS	stanovit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Státní	státní	k2eAgNnSc1d1	státní
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
===	===	k?	===
</s>
</p>
<p>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
by	by	kYmCp3nS	by
chtěl	chtít	k5eAaImAgMnS	chtít
rozšířit	rozšířit	k5eAaPmF	rozšířit
počet	počet	k1gInSc4	počet
udělovaných	udělovaný	k2eAgNnPc2d1	udělované
státních	státní	k2eAgNnPc2d1	státní
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
svojí	svojit	k5eAaImIp3nS	svojit
celoživotní	celoživotní	k2eAgFnSc7d1	celoživotní
prací	práce	k1gFnSc7	práce
přispěli	přispět	k5eAaPmAgMnP	přispět
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
takové	takový	k3xDgNnSc1	takový
ocenění	ocenění	k1gNnSc1	ocenění
zaslouží	zasloužit	k5eAaPmIp3nS	zasloužit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Dne	den	k1gInSc2	den
21.	[number]	k4	21.
října	říjen	k1gInSc2	říjen
2016	[number]	k4	2016
přinesla	přinést	k5eAaPmAgFnS	přinést
média	médium	k1gNnSc2	médium
informaci	informace	k1gFnSc4	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
udělit	udělit	k5eAaPmF	udělit
Řád	řád	k1gInSc4	řád
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
Jiřímu	Jiří	k1gMnSc3	Jiří
Bradymu	Bradym	k1gInSc6	Bradym
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
odmítnutí	odmítnutí	k1gNnSc4	odmítnutí
vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
schůzka	schůzka	k1gFnSc1	schůzka
ministra	ministr	k1gMnSc2	ministr
kultury	kultura	k1gFnSc2	kultura
ČR	ČR	kA	ČR
Daniela	Daniel	k1gMnSc2	Daniel
Hermana	Herman	k1gMnSc2	Herman
<g/>
,	,	kIx,	,
Bradyho	Brady	k1gMnSc2	Brady
příbuzného	příbuzný	k1gMnSc2	příbuzný
<g/>
,	,	kIx,	,
s	s	k7c7	s
dalajlámou	dalajláma	k1gMnSc7	dalajláma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
někteří	některý	k3yIgMnPc1	některý
čeští	český	k2eAgMnPc1d1	český
politici	politik	k1gMnPc1	politik
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
svou	svůj	k3xOyFgFnSc4	svůj
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
slavnostním	slavnostní	k2eAgInSc6d1	slavnostní
ceremoniálu	ceremoniál	k1gInSc6	ceremoniál
předávání	předávání	k1gNnSc2	předávání
státních	státní	k2eAgNnPc2d1	státní
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
28.	[number]	k4	28.
října	říjen	k1gInSc2	říjen
2016.	[number]	k4	2016.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
ceremoniálu	ceremoniál	k1gInSc6	ceremoniál
Zeman	Zeman	k1gMnSc1	Zeman
obvinil	obvinit	k5eAaPmAgMnS	obvinit
amerického	americký	k2eAgMnSc4d1	americký
velvyslance	velvyslanec	k1gMnSc4	velvyslanec
Andrewa	Andrewus	k1gMnSc2	Andrewus
Schapira	Schapir	k1gMnSc2	Schapir
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
jako	jako	k9	jako
téměř	téměř	k6eAd1	téměř
jediný	jediný	k2eAgMnSc1d1	jediný
z	z	k7c2	z
velvyslanců	velvyslanec	k1gMnPc2	velvyslanec
<g/>
"	"	kIx"	"
neúčastnil	účastnit	k5eNaImAgInS	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
Shapirovu	Shapirův	k2eAgFnSc4d1	Shapirův
účast	účast	k1gFnSc4	účast
nicméně	nicméně	k8xC	nicméně
potvrdily	potvrdit	k5eAaPmAgInP	potvrdit
televizní	televizní	k2eAgInPc1d1	televizní
záběry	záběr	k1gInPc1	záběr
i	i	k9	i
německý	německý	k2eAgMnSc1d1	německý
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vedle	vedle	k7c2	vedle
něho	on	k3xPp3gNnSc2	on
seděl	sedět	k5eAaImAgMnS	sedět
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
se	se	k3xPyFc4	se
za	za	k7c4	za
výrok	výrok	k1gInSc4	výrok
omluvil	omluvit	k5eAaPmAgMnS	omluvit
<g/>
.	.	kIx.	.
<g/>
Seznam	seznam	k1gInSc1	seznam
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
kterým	který	k3yQgFnPc3	který
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
udělil	udělit	k5eAaPmAgMnS	udělit
státní	státní	k2eAgNnSc4d1	státní
vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
a	a	k8xC	a
zahrnul	zahrnout	k5eAaPmAgMnS	zahrnout
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c4	mezi
své	svůj	k3xOyFgMnPc4	svůj
oblíbence	oblíbenec	k1gMnPc4	oblíbenec
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
usvědčené	usvědčený	k2eAgMnPc4d1	usvědčený
bývalé	bývalý	k2eAgMnPc4d1	bývalý
spolupracovníky	spolupracovník	k1gMnPc4	spolupracovník
StB	StB	k1gMnPc4	StB
<g/>
,	,	kIx,	,
komunistické	komunistický	k2eAgMnPc4d1	komunistický
nomenklaturní	nomenklaturní	k2eAgMnPc4d1	nomenklaturní
kádry	kádr	k1gMnPc4	kádr
<g/>
,	,	kIx,	,
milicionáře	milicionář	k1gMnPc4	milicionář
<g/>
,	,	kIx,	,
umělce	umělec	k1gMnPc4	umělec
a	a	k8xC	a
novináře	novinář	k1gMnPc4	novinář
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
kolaborovali	kolaborovat	k5eAaImAgMnP	kolaborovat
s	s	k7c7	s
komunistickým	komunistický	k2eAgInSc7d1	komunistický
režimem	režim	k1gInSc7	režim
nebo	nebo	k8xC	nebo
současné	současný	k2eAgFnSc2d1	současná
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
prokremelských	prokremelský	k2eAgInPc2d1	prokremelský
a	a	k8xC	a
dezinformačních	dezinformační	k2eAgInPc2d1	dezinformační
webů	web	k1gInPc2	web
(	(	kIx(	(
<g/>
Petr	Petr	k1gMnSc1	Petr
Žantovský	Žantovský	k1gMnSc1	Žantovský
<g/>
,	,	kIx,	,
Erik	Erik	k1gMnSc1	Erik
Best	Best	k1gMnSc1	Best
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Udělení	udělení	k1gNnSc1	udělení
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
nebo	nebo	k8xC	nebo
otevřená	otevřený	k2eAgFnSc1d1	otevřená
podpora	podpora	k1gFnSc1	podpora
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
má	mít	k5eAaImIp3nS	mít
negativní	negativní	k2eAgInSc4d1	negativní
ohlas	ohlas	k1gInSc4	ohlas
zejména	zejména	k9	zejména
v	v	k7c6	v
akademické	akademický	k2eAgFnSc6d1	akademická
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc6d1	kulturní
sféře	sféra	k1gFnSc6	sféra
a	a	k8xC	a
pro	pro	k7c4	pro
vyznamenaného	vyznamenaný	k2eAgNnSc2d1	vyznamenané
může	moct	k5eAaImIp3nS	moct
znamenat	znamenat	k5eAaImF	znamenat
zvrat	zvrat	k1gInSc4	zvrat
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
dosavadní	dosavadní	k2eAgFnSc6d1	dosavadní
kariéře	kariéra	k1gFnSc6	kariéra
(	(	kIx(	(
<g/>
Michal	Michal	k1gMnSc1	Michal
Macháček	Macháček	k1gMnSc1	Macháček
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Návštěvy	návštěva	k1gFnPc4	návštěva
krajů	kraj	k1gInPc2	kraj
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
mandátu	mandát	k1gInSc2	mandát
koná	konat	k5eAaImIp3nS	konat
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
třídenní	třídenní	k2eAgFnSc2d1	třídenní
návštěvy	návštěva	k1gFnSc2	návštěva
všech	všecek	k3xTgInPc2	všecek
krajů	kraj	k1gInPc2	kraj
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
Prahy	Praha	k1gFnSc2	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgInPc6	který
beseduje	besedovat	k5eAaImIp3nS	besedovat
s	s	k7c7	s
občany	občan	k1gMnPc7	občan
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
závazek	závazek	k1gInSc4	závazek
si	se	k3xPyFc3	se
vytkl	vytknout	k5eAaPmAgMnS	vytknout
navštívit	navštívit	k5eAaPmF	navštívit
všechny	všechen	k3xTgFnPc4	všechen
obce	obec	k1gFnPc4	obec
nad	nad	k7c4	nad
5 000	[number]	k4	5 000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
místa	místo	k1gNnPc1	místo
tak	tak	k6eAd1	tak
navštívil	navštívit	k5eAaPmAgMnS	navštívit
jako	jako	k9	jako
vůbec	vůbec	k9	vůbec
první	první	k4xOgMnSc1	první
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgInPc1	veškerý
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
cesty	cesta	k1gFnPc4	cesta
nesou	nést	k5eAaImIp3nP	nést
kraje	kraj	k1gInPc1	kraj
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
hejtmani	hejtman	k1gMnPc1	hejtman
proto	proto	k8xC	proto
navrhovali	navrhovat	k5eAaImAgMnP	navrhovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
finančně	finančně	k6eAd1	finančně
podílela	podílet	k5eAaImAgFnS	podílet
i	i	k9	i
Kancelář	kancelář	k1gFnSc1	kancelář
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Důvěra	důvěra	k1gFnSc1	důvěra
společnosti	společnost	k1gFnSc2	společnost
===	===	k?	===
</s>
</p>
<p>
<s>
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
zpracovávalo	zpracovávat	k5eAaImAgNnS	zpracovávat
průzkumy	průzkum	k1gInPc4	průzkum
názorů	názor	k1gInPc2	názor
ohledně	ohledně	k7c2	ohledně
důvěry	důvěra	k1gFnSc2	důvěra
občanů	občan	k1gMnPc2	občan
vůči	vůči	k7c3	vůči
ústavním	ústavní	k2eAgFnPc3d1	ústavní
institucím	instituce	k1gFnPc3	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Důvěra	důvěra	k1gFnSc1	důvěra
prezidentu	prezident	k1gMnSc3	prezident
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
mandátu	mandát	k1gInSc2	mandát
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
se	se	k3xPyFc4	se
vyvíjela	vyvíjet	k5eAaImAgNnP	vyvíjet
takto	takto	k6eAd1	takto
(	(	kIx(	(
<g/>
v	v	k7c6	v
procentech	procento	k1gNnPc6	procento
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Prezidentské	prezidentský	k2eAgFnPc1d1	prezidentská
volby	volba	k1gFnPc1	volba
2018	[number]	k4	2018
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
9.	[number]	k4	9.
března	březen	k1gInSc2	březen
2017	[number]	k4	2017
svým	svůj	k3xOyFgMnPc3	svůj
podporovatelům	podporovatel	k1gMnPc3	podporovatel
veřejně	veřejně	k6eAd1	veřejně
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
o	o	k7c4	o
post	post	k1gInSc4	post
hlavy	hlava	k1gFnSc2	hlava
státu	stát	k1gInSc2	stát
bude	být	k5eAaImBp3nS	být
ucházet	ucházet	k5eAaImF	ucházet
znova	znova	k6eAd1	znova
i	i	k9	i
v	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
volebním	volební	k2eAgNnSc6d1	volební
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
volbu	volba	k1gFnSc4	volba
prezidenta	prezident	k1gMnSc2	prezident
2018	[number]	k4	2018
zmínil	zmínit	k5eAaPmAgInS	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nehodlá	hodlat	k5eNaImIp3nS	hodlat
účastnit	účastnit	k5eAaImF	účastnit
předvolebních	předvolební	k2eAgFnPc2d1	předvolební
debat	debata	k1gFnPc2	debata
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
kandidáty	kandidát	k1gMnPc7	kandidát
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
osobně	osobně	k6eAd1	osobně
již	již	k6eAd1	již
nevést	vést	k5eNaImF	vést
kampaň	kampaň	k1gFnSc4	kampaň
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
prý	prý	k9	prý
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
svého	svůj	k3xOyFgMnSc2	svůj
přítele	přítel	k1gMnSc2	přítel
<g/>
,	,	kIx,	,
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
rakouského	rakouský	k2eAgMnSc2d1	rakouský
prezidenta	prezident	k1gMnSc2	prezident
Heinze	Heinze	k1gFnSc2	Heinze
Fischera	Fischer	k1gMnSc2	Fischer
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yRgNnSc2	který
za	za	k7c4	za
prezidenta	prezident	k1gMnSc4	prezident
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
podruhé	podruhé	k6eAd1	podruhé
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
mluvit	mluvit	k5eAaImF	mluvit
jeho	jeho	k3xOp3gInPc4	jeho
činy	čin	k1gInPc4	čin
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
slibu	slib	k1gInSc6	slib
se	se	k3xPyFc4	se
nedá	dát	k5eNaPmIp3nS	dát
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
Zeman	Zeman	k1gMnSc1	Zeman
dodržel	dodržet	k5eAaPmAgMnS	dodržet
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
využíval	využívat	k5eAaPmAgMnS	využívat
svých	svůj	k3xOyFgFnPc2	svůj
návštěv	návštěva	k1gFnPc2	návštěva
krajů	kraj	k1gInPc2	kraj
pro	pro	k7c4	pro
propagaci	propagace	k1gFnSc4	propagace
<g/>
,	,	kIx,	,
oslovoval	oslovovat	k5eAaImAgMnS	oslovovat
voliče	volič	k1gMnPc4	volič
pomocí	pomocí	k7c2	pomocí
webu	web	k1gInSc2	web
<g/>
,	,	kIx,	,
objevoval	objevovat	k5eAaImAgInS	objevovat
se	se	k3xPyFc4	se
na	na	k7c6	na
plakátech	plakát	k1gInPc6	plakát
billboardů	billboard	k1gInPc2	billboard
<g/>
,	,	kIx,	,
na	na	k7c6	na
letácích	leták	k1gInPc6	leták
roznášených	roznášený	k2eAgInPc6d1	roznášený
do	do	k7c2	do
poštovních	poštovní	k2eAgFnPc2d1	poštovní
schránek	schránka	k1gFnPc2	schránka
<g/>
,	,	kIx,	,
v	v	k7c6	v
TV	TV	kA	TV
Barrandov	Barrandov	k1gInSc4	Barrandov
i	i	k8xC	i
na	na	k7c6	na
webu	web	k1gInSc6	web
Blesk.cz	Blesk.cza	k1gFnPc2	Blesk.cza
<g/>
.	.	kIx.	.
<g/>
Začátkem	začátkem	k7c2	začátkem
srpna	srpen	k1gInSc2	srpen
2017	[number]	k4	2017
sdělila	sdělit	k5eAaPmAgFnS	sdělit
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Ivana	Ivana	k1gFnSc1	Ivana
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
nasbíral	nasbírat	k5eAaPmAgMnS	nasbírat
potřebný	potřebný	k2eAgInSc4d1	potřebný
počet	počet	k1gInSc4	počet
podpisů	podpis	k1gInPc2	podpis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2017	[number]	k4	2017
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
podpoře	podpora	k1gFnSc3	podpora
103 817	[number]	k4	103 817
podpisů	podpis	k1gInPc2	podpis
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
započetlo	započíst	k5eAaPmAgNnS	započíst
na	na	k7c4	na
petici	petice	k1gFnSc4	petice
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
devíti	devět	k4xCc2	devět
prezidentských	prezidentský	k2eAgMnPc2d1	prezidentský
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
kolo	kolo	k1gNnSc1	kolo
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
Zeman	Zeman	k1gMnSc1	Zeman
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
38,56	[number]	k4	38,56
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
se	se	k3xPyFc4	se
tak	tak	k9	tak
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Drahošem	Drahoš	k1gMnSc7	Drahoš
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
obdržel	obdržet	k5eAaPmAgMnS	obdržet
od	od	k7c2	od
voličů	volič	k1gInPc2	volič
26,6	[number]	k4	26,6
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
<g/>
Hned	hned	k9	hned
první	první	k4xOgInSc1	první
den	den	k1gInSc1	den
voleb	volba	k1gFnPc2	volba
byl	být	k5eAaImAgInS	být
poznamenán	poznamenat	k5eAaPmNgInS	poznamenat
incidentem	incident	k1gInSc7	incident
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
na	na	k7c4	na
samotného	samotný	k2eAgMnSc4d1	samotný
Miloše	Miloš	k1gMnSc4	Miloš
<g />
.	.	kIx.	.
</s>
<s>
Zemana	Zeman	k1gMnSc4	Zeman
ve	v	k7c6	v
volební	volební	k2eAgFnSc6d1	volební
místnosti	místnost	k1gFnSc6	místnost
v	v	k7c6	v
pražských	pražský	k2eAgFnPc6d1	Pražská
Lužinách	lužina	k1gFnPc6	lužina
vrhla	vrhnout	k5eAaPmAgFnS	vrhnout
polonahá	polonahý	k2eAgFnSc1d1	polonahá
aktivistka	aktivistka	k1gFnSc1	aktivistka
skupiny	skupina	k1gFnSc2	skupina
Femen	Femna	k1gFnPc2	Femna
Andželina	Andželina	k1gFnSc1	Andželina
Diašová	Diašová	k1gFnSc1	Diašová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
anglicky	anglicky	k6eAd1	anglicky
provolávala	provolávat	k5eAaImAgFnS	provolávat
slova	slovo	k1gNnPc4	slovo
"	"	kIx"	"
<g/>
Zeman	Zeman	k1gMnSc1	Zeman
Putin	putin	k2eAgMnSc1d1	putin
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
slut	slut	k2eAgMnSc1d1	slut
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Zeman	Zeman	k1gMnSc1	Zeman
je	být	k5eAaImIp3nS	být
Putinova	Putinův	k2eAgFnSc1d1	Putinova
děvka	děvka	k1gFnSc1	děvka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prezidentova	prezidentův	k2eAgFnSc1d1	prezidentova
ochranka	ochranka	k1gFnSc1	ochranka
ji	on	k3xPp3gFnSc4	on
zklidnila	zklidnit	k5eAaPmAgFnS	zklidnit
a	a	k8xC	a
prezidenta	prezident	k1gMnSc2	prezident
odvedla	odvést	k5eAaPmAgFnS	odvést
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
do	do	k7c2	do
místnosti	místnost	k1gFnSc2	místnost
o	o	k7c4	o
pár	pár	k4xCyI	pár
minut	minuta	k1gFnPc2	minuta
později	pozdě	k6eAd2	pozdě
vrátil	vrátit	k5eAaPmAgMnS	vrátit
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
odvolil	odvolit	k5eAaPmAgInS	odvolit
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
projevu	projev	k1gInSc6	projev
po	po	k7c6	po
sečtení	sečtení	k1gNnSc6	sečtení
hlasů	hlas	k1gInPc2	hlas
prvního	první	k4xOgNnSc2	první
kola	kolo	k1gNnSc2	kolo
uvedl	uvést	k5eAaPmAgInS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
rád	rád	k6eAd1	rád
vyhoví	vyhovit	k5eAaPmIp3nS	vyhovit
prosbě	prosba	k1gFnSc3	prosba
Jiřího	Jiří	k1gMnSc2	Jiří
Drahoše	Drahoš	k1gMnSc2	Drahoš
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
diskutovat	diskutovat	k5eAaImF	diskutovat
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
při	při	k7c6	při
oznámení	oznámení	k1gNnSc6	oznámení
své	svůj	k3xOyFgFnSc2	svůj
kandidatury	kandidatura	k1gFnSc2	kandidatura
zavázal	zavázat	k5eAaPmAgMnS	zavázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nezúčastní	zúčastnit	k5eNaPmIp3nS	zúčastnit
žádné	žádný	k3yNgFnSc2	žádný
rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
ani	ani	k8xC	ani
televizní	televizní	k2eAgFnSc2d1	televizní
diskuse	diskuse	k1gFnSc2	diskuse
<g/>
.	.	kIx.	.
<g/>
Zeman	Zeman	k1gMnSc1	Zeman
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
čtyř	čtyři	k4xCgFnPc2	čtyři
televizních	televizní	k2eAgFnPc2d1	televizní
diskusí	diskuse	k1gFnPc2	diskuse
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
na	na	k7c6	na
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
a	a	k8xC	a
TV	TV	kA	TV
Barrandov	Barrandov	k1gInSc1	Barrandov
bez	bez	k7c2	bez
Drahoše	Drahoš	k1gMnSc2	Drahoš
<g/>
,	,	kIx,	,
na	na	k7c6	na
Primě	prima	k1gFnSc6	prima
a	a	k8xC	a
v	v	k7c6	v
ČT	ČT	kA	ČT
společně	společně	k6eAd1	společně
s	s	k7c7	s
Drahošem	Drahoš	k1gMnSc7	Drahoš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Druhý	druhý	k4xOgInSc1	druhý
mandát	mandát	k1gInSc1	mandát
prezidenta	prezident	k1gMnSc2	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c4	v
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
8.	[number]	k4	8.
března	březen	k1gInSc2	březen
2018	[number]	k4	2018
složil	složit	k5eAaPmAgMnS	složit
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
slib	slib	k1gInSc4	slib
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
inaugurován	inaugurovat	k5eAaBmNgMnS	inaugurovat
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
druhé	druhý	k4xOgNnSc4	druhý
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Událost	událost	k1gFnSc4	událost
provázely	provázet	k5eAaImAgFnP	provázet
kontroverze	kontroverze	k1gFnPc1	kontroverze
ohledně	ohledně	k7c2	ohledně
výběru	výběr	k1gInSc2	výběr
pozvaných	pozvaný	k2eAgFnPc2d1	pozvaná
a	a	k8xC	a
nepozvaných	pozvaný	k2eNgFnPc2d1	nepozvaná
osobností	osobnost	k1gFnPc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
inauguračního	inaugurační	k2eAgInSc2d1	inaugurační
ceremoniálu	ceremoniál	k1gInSc2	ceremoniál
a	a	k8xC	a
prezidentova	prezidentův	k2eAgMnSc2d1	prezidentův
projevu	projev	k1gInSc6	projev
někteří	některý	k3yIgMnPc1	některý
politici	politik	k1gMnPc1	politik
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
obsahu	obsah	k1gInSc3	obsah
projevu	projev	k1gInSc2	projev
<g/>
,	,	kIx,	,
např.	např.	kA	např.
napadání	napadání	k1gNnSc1	napadání
nezávislých	závislý	k2eNgNnPc2d1	nezávislé
médií	médium	k1gNnPc2	médium
a	a	k8xC	a
novinářů	novinář	k1gMnPc2	novinář
<g/>
,	,	kIx,	,
opustili	opustit	k5eAaPmAgMnP	opustit
sál	sál	k1gInSc4	sál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2018	[number]	k4	2018
mnozí	mnohý	k2eAgMnPc1d1	mnohý
politici	politik	k1gMnPc1	politik
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
záměr	záměr	k1gInSc4	záměr
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
nevystoupit	vystoupit	k5eNaPmF	vystoupit
s	s	k7c7	s
projevem	projev	k1gInSc7	projev
ani	ani	k8xC	ani
se	se	k3xPyFc4	se
nezúčastnit	zúčastnit	k5eNaPmF	zúčastnit
žádné	žádný	k3yNgFnPc4	žádný
pietní	pietní	k2eAgFnPc4d1	pietní
akce	akce	k1gFnPc4	akce
k	k	k7c3	k
výročí	výročí	k1gNnSc3	výročí
50	[number]	k4	50
let	léto	k1gNnPc2	léto
po	po	k7c6	po
invazi	invaze	k1gFnSc6	invaze
vojsk	vojsko	k1gNnPc2	vojsko
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
Andrejem	Andrej	k1gMnSc7	Andrej
Kiskou	Kiska	k1gMnSc7	Kiska
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
projev	projev	k1gInSc4	projev
ve	v	k7c4	v
veřejnoprávní	veřejnoprávní	k2eAgFnSc4d1	veřejnoprávní
RTVS	RTVS	kA	RTVS
na	na	k7c6	na
21.	[number]	k4	21.
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Mluvčí	mluvčí	k1gFnSc1	mluvčí
prezidenta	prezident	k1gMnSc2	prezident
Zemana	Zeman	k1gMnSc2	Zeman
Jiří	Jiří	k1gMnSc1	Jiří
Ovčáček	ovčáček	k1gMnSc1	ovčáček
hájil	hájit	k5eAaImAgMnS	hájit
krok	krok	k1gInSc4	krok
prezidenta	prezident	k1gMnSc2	prezident
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
svůj	svůj	k3xOyFgInSc4	svůj
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
Zeman	Zeman	k1gMnSc1	Zeman
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
už	už	k6eAd1	už
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
normalizace	normalizace	k1gFnSc2	normalizace
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
28.	[number]	k4	28.
října	říjen	k1gInSc2	říjen
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
stého	stý	k4xOgNnSc2	stý
výročí	výročí	k1gNnSc2	výročí
vzniku	vznik	k1gInSc2	vznik
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Zeman	Zeman	k1gMnSc1	Zeman
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
pietního	pietní	k2eAgInSc2d1	pietní
aktu	akt	k1gInSc2	akt
v	v	k7c6	v
památníku	památník	k1gInSc6	památník
na	na	k7c6	na
Vítkově	Vítkov	k1gInSc6	Vítkov
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c6	na
Evropské	evropský	k2eAgFnSc6d1	Evropská
třídě	třída	k1gFnSc6	třída
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
probíhala	probíhat	k5eAaImAgFnS	probíhat
slavnostní	slavnostní	k2eAgFnSc1d1	slavnostní
přehlídka	přehlídka	k1gFnSc1	přehlídka
jednotek	jednotka	k1gFnPc2	jednotka
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
záchranné	záchranný	k2eAgFnSc2d1	záchranná
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
hasičů	hasič	k1gMnPc2	hasič
<g/>
,	,	kIx,	,
Hradní	hradní	k2eAgFnPc1d1	hradní
stráže	stráž	k1gFnPc1	stráž
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
společného	společný	k2eAgInSc2d1	společný
televizního	televizní	k2eAgInSc2d1	televizní
rozhovoru	rozhovor	k1gInSc2	rozhovor
s	s	k7c7	s
premiérem	premiér	k1gMnSc7	premiér
Andrejem	Andrej	k1gMnSc7	Andrej
Babišem	Babiš	k1gMnSc7	Babiš
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
jak	jak	k6eAd1	jak
k	k	k7c3	k
dějiným	dějin	k2eAgFnPc3d1	dějin
událostem	událost	k1gFnPc3	událost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
k	k	k7c3	k
současné	současný	k2eAgFnSc3d1	současná
politické	politický	k2eAgFnSc3d1	politická
situaci	situace	k1gFnSc3	situace
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
ve	v	k7c6	v
Vladislavském	vladislavský	k2eAgInSc6d1	vladislavský
sále	sál	k1gInSc6	sál
udílel	udílet	k5eAaImAgMnS	udílet
a	a	k8xC	a
propůjčoval	propůjčovat	k5eAaImAgMnS	propůjčovat
státní	státní	k2eAgNnSc4d1	státní
vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
3.	[number]	k4	3.
dubna	duben	k1gInSc2	duben
2019	[number]	k4	2019
Zemana	Zeman	k1gMnSc2	Zeman
přijal	přijmout	k5eAaPmAgMnS	přijmout
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
rakouský	rakouský	k2eAgMnSc1d1	rakouský
prezident	prezident	k1gMnSc1	prezident
Alexander	Alexandra	k1gFnPc2	Alexandra
Van	van	k1gInSc4	van
der	drát	k5eAaImRp2nS	drát
Bellen	Bellen	k1gInSc4	Bellen
<g/>
.	.	kIx.	.
</s>
<s>
Pohoršení	pohoršení	k1gNnPc1	pohoršení
pak	pak	k6eAd1	pak
vzbudila	vzbudit	k5eAaPmAgNnP	vzbudit
Zemanova	Zemanův	k2eAgNnPc1d1	Zemanovo
kritická	kritický	k2eAgNnPc1d1	kritické
slova	slovo	k1gNnPc1	slovo
vůči	vůči	k7c3	vůči
bývalému	bývalý	k2eAgMnSc3d1	bývalý
primátorovi	primátor	k1gMnSc3	primátor
Vídně	Vídeň	k1gFnSc2	Vídeň
Michaelu	Michael	k1gMnSc3	Michael
Häuplovi	Häupl	k1gMnSc3	Häupl
za	za	k7c4	za
údajné	údajný	k2eAgNnSc4d1	údajné
nefinancování	nefinancování	k1gNnSc4	nefinancování
české	český	k2eAgFnSc2d1	Česká
školy	škola	k1gFnSc2	škola
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
být	být	k5eAaImF	být
nepodloženými	podložený	k2eNgFnPc7d1	nepodložená
<g/>
.	.	kIx.	.
<g/>
Před	před	k7c7	před
koncem	konec	k1gInSc7	konec
dubna	duben	k1gInSc2	duben
2019	[number]	k4	2019
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
již	již	k6eAd1	již
pátou	pátý	k4xOgFnSc4	pátý
pracovní	pracovní	k2eAgFnSc4d1	pracovní
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
kam	kam	k6eAd1	kam
ho	on	k3xPp3gNnSc2	on
doprovázelo	doprovázet	k5eAaImAgNnS	doprovázet
pět	pět	k4xCc1	pět
ministrů	ministr	k1gMnPc2	ministr
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
delegace	delegace	k1gFnSc2	delegace
asi	asi	k9	asi
60	[number]	k4	60
podnikatelů	podnikatel	k1gMnPc2	podnikatel
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
prezidenta	prezident	k1gMnSc2	prezident
Hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
komory	komora	k1gFnPc1	komora
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
společnosti	společnost	k1gFnSc2	společnost
Škoda	škoda	k1gFnSc1	škoda
China	China	k1gFnSc1	China
<g/>
,	,	kIx,	,
sportovci	sportovec	k1gMnPc1	sportovec
Jaromír	Jaromír	k1gMnSc1	Jaromír
Jágr	Jágr	k1gMnSc1	Jágr
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
Nedvěd	Nedvěd	k1gMnSc1	Nedvěd
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
finančníci	finančník	k1gMnPc1	finančník
Pavel	Pavel	k1gMnSc1	Pavel
Tykač	Tykač	k1gMnSc1	Tykač
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Šmejc	Šmejc	k1gFnSc1	Šmejc
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
otevření	otevření	k1gNnSc4	otevření
české	český	k2eAgFnSc2d1	Česká
expozice	expozice	k1gFnSc2	expozice
na	na	k7c4	na
Beijing	Beijing	k1gInSc4	Beijing
Expo	Expo	k1gNnSc1	Expo
nebo	nebo	k8xC	nebo
summitu	summit	k1gInSc2	summit
o	o	k7c6	o
projektu	projekt	k1gInSc6	projekt
Nové	Nové	k2eAgFnSc2d1	Nové
Hedvábné	hedvábný	k2eAgFnSc2d1	hedvábná
stezky	stezka	k1gFnSc2	stezka
kam	kam	k6eAd1	kam
přijelo	přijet	k5eAaPmAgNnS	přijet
okolo	okolo	k7c2	okolo
čtyřiceti	čtyřicet	k4xCc2	čtyřicet
prezidentů	prezident	k1gMnPc2	prezident
či	či	k8xC	či
premiérů	premiér	k1gMnPc2	premiér
<g/>
,	,	kIx,	,
setkal	setkat	k5eAaPmAgMnS	setkat
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
s	s	k7c7	s
čínským	čínský	k2eAgMnSc7d1	čínský
prezidentem	prezident	k1gMnSc7	prezident
Si	se	k3xPyFc3	se
Ťin-pchingem	Ťinching	k1gInSc7	Ťin-pching
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
i	i	k9	i
s	s	k7c7	s
ruským	ruský	k2eAgMnSc7d1	ruský
prezidentem	prezident	k1gMnSc7	prezident
Putinem	Putin	k1gMnSc7	Putin
<g/>
,	,	kIx,	,
na	na	k7c6	na
česko-čínském	česko-čínský	k2eAgNnSc6d1	česko-čínský
hospodářském	hospodářský	k2eAgNnSc6d1	hospodářské
fóru	fórum	k1gNnSc6	fórum
se	se	k3xPyFc4	se
zastal	zastat	k5eAaPmAgInS	zastat
společnosti	společnost	k1gFnSc3	společnost
Huawai	Huawae	k1gFnSc4	Huawae
<g/>
,	,	kIx,	,
považované	považovaný	k2eAgInPc4d1	považovaný
některými	některý	k3yIgFnPc7	některý
západními	západní	k2eAgFnPc7d1	západní
zeměmi	zem	k1gFnPc7	zem
za	za	k7c4	za
bezpečnostní	bezpečnostní	k2eAgNnSc4d1	bezpečnostní
riziko	riziko	k1gNnSc4	riziko
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
napojení	napojení	k1gNnSc3	napojení
na	na	k7c4	na
čínskou	čínský	k2eAgFnSc4d1	čínská
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
požadavku	požadavek	k1gInSc2	požadavek
na	na	k7c4	na
státní	státní	k2eAgInSc4d1	státní
rozpočet	rozpočet	k1gInSc4	rozpočet
měla	mít	k5eAaImAgFnS	mít
prezidentova	prezidentův	k2eAgFnSc1d1	prezidentova
čínská	čínský	k2eAgFnSc1d1	čínská
cesta	cesta	k1gFnSc1	cesta
stát	stát	k1gInSc1	stát
8,8	[number]	k4	8,8
milionu	milion	k4xCgInSc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
o	o	k7c4	o
3	[number]	k4	3
miliony	milion	k4xCgInPc4	milion
více	hodně	k6eAd2	hodně
než	než	k8xS	než
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
30.	[number]	k4	30.
dubna	duben	k1gInSc2	duben
2019	[number]	k4	2019
Zeman	Zeman	k1gMnSc1	Zeman
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
tři	tři	k4xCgInPc4	tři
nové	nový	k2eAgInPc4d1	nový
členy	člen	k1gInPc4	člen
Babišovy	Babišův	k2eAgFnSc2d1	Babišova
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
patrně	patrně	k6eAd1	patrně
nejkontroverznější	kontroverzní	k2eAgNnSc1d3	nejkontroverznější
bylo	být	k5eAaImAgNnS	být
dosazení	dosazení	k1gNnSc1	dosazení
Marie	Maria	k1gFnSc2	Maria
Benešové	Benešová	k1gFnSc2	Benešová
<g/>
,	,	kIx,	,
členky	členka	k1gFnSc2	členka
prezidentova	prezidentův	k2eAgInSc2d1	prezidentův
poradního	poradní	k2eAgInSc2d1	poradní
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
na	na	k7c4	na
post	post	k1gInSc4	post
ministrině	ministrin	k2eAgFnSc3d1	ministrin
spravedlnosti	spravedlnost	k1gFnSc3	spravedlnost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
občanské	občanský	k2eAgInPc4d1	občanský
protesty	protest	k1gInPc4	protest
a	a	k8xC	a
demonstrace	demonstrace	k1gFnPc4	demonstrace
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
Den	den	k1gInSc4	den
vítězství	vítězství	k1gNnSc4	vítězství
8.	[number]	k4	8.
května	květen	k1gInSc2	květen
2019	[number]	k4	2019
prezident	prezident	k1gMnSc1	prezident
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
15	[number]	k4	15
mužů	muž	k1gMnPc2	muž
do	do	k7c2	do
generálských	generálský	k2eAgFnPc2d1	generálská
hodností	hodnost	k1gFnPc2	hodnost
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
hodnosti	hodnost	k1gFnSc2	hodnost
armádního	armádní	k2eAgMnSc2d1	armádní
generála	generál	k1gMnSc2	generál
ve	v	k7c6	v
výslužbě	výslužba	k1gFnSc6	výslužba
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
veterána	veterán	k1gMnSc2	veterán
z	z	k7c2	z
2.	[number]	k4	2.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
Emila	Emil	k1gMnSc2	Emil
Bočka	Boček	k1gMnSc2	Boček
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
ani	ani	k8xC	ani
potřetí	potřetí	k4xO	potřetí
neudělil	udělit	k5eNaPmAgInS	udělit
generálskou	generálský	k2eAgFnSc4d1	generálská
hodnost	hodnost	k1gFnSc4	hodnost
vládou	vláda	k1gFnSc7	vláda
navrženému	navržený	k2eAgMnSc3d1	navržený
Michalu	Michal	k1gMnSc3	Michal
Koudelkovi	Koudelka	k1gMnSc3	Koudelka
<g/>
,	,	kIx,	,
řediteli	ředitel	k1gMnSc3	ředitel
Bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
informační	informační	k2eAgFnSc2d1	informační
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
4.	[number]	k4	4.
června	červen	k1gInSc2	červen
2019	[number]	k4	2019
se	se	k3xPyFc4	se
prezident	prezident	k1gMnSc1	prezident
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
barmskou	barmský	k2eAgFnSc7d1	barmská
vůdkyní	vůdkyně	k1gFnSc7	vůdkyně
<g/>
,	,	kIx,	,
nositelkou	nositelka	k1gFnSc7	nositelka
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
a	a	k8xC	a
někdejší	někdejší	k2eAgInSc1d1	někdejší
disidentkou	disidentka	k1gFnSc7	disidentka
Aun	Aun	k1gFnSc1	Aun
Schan	Schan	k1gMnSc1	Schan
Su	Su	k?	Su
Ťij	Ťij	k1gFnSc1	Ťij
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přijela	přijet	k5eAaPmAgFnS	přijet
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
otevřít	otevřít	k5eAaPmF	otevřít
ambasádu	ambasáda	k1gFnSc4	ambasáda
Myanmaru	Myanmar	k1gInSc2	Myanmar
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentův	prezidentův	k2eAgMnSc1d1	prezidentův
mluvčí	mluvčí	k1gMnSc1	mluvčí
Ovčáček	ovčáček	k1gMnSc1	ovčáček
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Zeman	Zeman	k1gMnSc1	Zeman
se	s	k7c7	s
Su	Su	k?	Su
Ťij	Ťij	k1gMnPc1	Ťij
shodli	shodnout	k5eAaPmAgMnP	shodnout
na	na	k7c6	na
přátelských	přátelský	k2eAgInPc6d1	přátelský
vztazích	vztah	k1gInPc6	vztah
s	s	k7c7	s
Izraelem	Izrael	k1gInSc7	Izrael
a	a	k8xC	a
"	"	kIx"	"
<g/>
také	také	k9	také
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
se	s	k7c7	s
Su	Su	k?	Su
Ťij	Ťij	k1gMnSc1	Ťij
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
podporu	podpora	k1gFnSc4	podpora
Myanmaru	Myanmar	k1gInSc2	Myanmar
v	v	k7c6	v
boji	boj	k1gInSc6	boj
s	s	k7c7	s
terorismem	terorismus	k1gInSc7	terorismus
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
ústavní	ústavní	k2eAgFnSc7d1	ústavní
krizí	krize	k1gFnSc7	krize
kolem	kolem	k7c2	kolem
výměny	výměna	k1gFnSc2	výměna
ministra	ministr	k1gMnSc2	ministr
kultury	kultura	k1gFnSc2	kultura
Staňka	Staněk	k1gMnSc2	Staněk
i	i	k9	i
dalších	další	k2eAgInPc2d1	další
sporných	sporný	k2eAgInPc2d1	sporný
postupů	postup	k1gInPc2	postup
prezidenta	prezident	k1gMnSc2	prezident
Zemana	Zeman	k1gMnSc2	Zeman
dne	den	k1gInSc2	den
24.	[number]	k4	24.
července	červenec	k1gInSc2	červenec
2019	[number]	k4	2019
přijal	přijmout	k5eAaPmAgInS	přijmout
Senát	senát	k1gInSc1	senát
PČR	PČR	kA	PČR
ústavní	ústavní	k2eAgFnSc4d1	ústavní
žalobu	žaloba	k1gFnSc4	žaloba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Sponzoring	sponzoring	k1gInSc1	sponzoring
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
a	a	k8xC	a
SPO	SPO	kA	SPO
===	===	k?	===
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
všech	všecek	k3xTgInPc2	všecek
darů	dar	k1gInPc2	dar
pro	pro	k7c4	pro
Stranu	strana	k1gFnSc4	strana
Práv	právo	k1gNnPc2	právo
Občanů	občan	k1gMnPc2	občan
(	(	kIx(	(
<g/>
Zemanovce	zemanovec	k1gMnSc2	zemanovec
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
provázané	provázaný	k2eAgFnSc2d1	provázaná
skupiny	skupina	k1gFnSc2	skupina
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
podle	podle	k7c2	podle
Aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
ovládá	ovládat	k5eAaImIp3nS	ovládat
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
právník	právník	k1gMnSc1	právník
Fabio	Fabio	k1gMnSc1	Fabio
Delco	Delco	k1gMnSc1	Delco
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xC	jako
správce	správce	k1gMnSc1	správce
firem	firma	k1gFnPc2	firma
patřících	patřící	k2eAgFnPc2d1	patřící
dlouholetému	dlouholetý	k2eAgInSc3d1	dlouholetý
příteli	přítel	k1gMnSc3	přítel
Vladimíra	Vladimíra	k1gFnSc1	Vladimíra
Putina	putin	k2eAgFnSc1d1	Putina
Sergeji	Sergej	k1gMnSc3	Sergej
Rolduginovi	Roldugin	k1gMnSc3	Roldugin
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pere	prát	k5eAaImIp3nS	prát
v	v	k7c6	v
offshorových	offshorův	k2eAgFnPc6d1	offshorův
daňových	daňový	k2eAgFnPc6d1	daňová
rájích	ráje	k1gFnPc6	ráje
miliardové	miliardový	k2eAgFnSc2d1	miliardová
částky	částka	k1gFnSc2	částka
nelegálně	legálně	k6eNd1	legálně
vyvedené	vyvedený	k2eAgInPc1d1	vyvedený
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Delcově	Delcův	k2eAgInSc6d1	Delcův
účtu	účet	k1gInSc6	účet
skončilo	skončit	k5eAaPmAgNnS	skončit
také	také	k9	také
300	[number]	k4	300
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
korun	koruna	k1gFnPc2	koruna
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
solárních	solární	k2eAgFnPc2d1	solární
elektráren	elektrárna	k1gFnPc2	elektrárna
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
Delco	Delco	k6eAd1	Delco
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
lichtenštejnské	lichtenštejnský	k2eAgFnSc2d1	lichtenštejnská
firmy	firma	k1gFnSc2	firma
Norwalk	Norwalk	k1gMnSc1	Norwalk
Holding	holding	k1gInSc1	holding
spoluvlastníkem	spoluvlastník	k1gMnSc7	spoluvlastník
společnosti	společnost	k1gFnSc2	společnost
Stegenato	Stegenat	k2eAgNnSc1d1	Stegenat
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
podnikatele	podnikatel	k1gMnSc2	podnikatel
a	a	k8xC	a
Zemanova	Zemanův	k2eAgMnSc2d1	Zemanův
sponzora	sponzor	k1gMnSc2	sponzor
Michala	Michal	k1gMnSc2	Michal
Pechana	Pechan	k1gMnSc2	Pechan
<g/>
.	.	kIx.	.
</s>
<s>
Delco	Delco	k6eAd1	Delco
řídí	řídit	k5eAaImIp3nS	řídit
i	i	k9	i
švýcarskou	švýcarský	k2eAgFnSc4d1	švýcarská
firmu	firma	k1gFnSc4	firma
Waren	Waren	k2eAgInSc4d1	Waren
Partners	Partners	k1gInSc4	Partners
Ventures	Venturesa	k1gFnPc2	Venturesa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spravuje	spravovat	k5eAaImIp3nS	spravovat
dalšího	další	k2eAgMnSc4d1	další
sponzora	sponzor	k1gMnSc4	sponzor
SPO	SPO	kA	SPO
–	–	k?	–
Adamovské	Adamovské	k2eAgFnSc2d1	Adamovské
strojírny	strojírna	k1gFnSc2	strojírna
(	(	kIx(	(
<g/>
a	a	k8xC	a
podřízené	podřízený	k2eAgFnPc1d1	podřízená
firmy	firma	k1gFnPc1	firma
Adast	Adast	k1gFnSc1	Adast
a	a	k8xC	a
Benepro	Benepro	k1gNnSc1	Benepro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc4	dva
větve	větev	k1gFnPc4	větev
propojuje	propojovat	k5eAaImIp3nS	propojovat
ostravský	ostravský	k2eAgMnSc1d1	ostravský
právník	právník	k1gMnSc1	právník
Daniel	Daniel	k1gMnSc1	Daniel
Tomíček	Tomíček	k1gMnSc1	Tomíček
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
několika	několik	k4yIc2	několik
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
sponzorujících	sponzorující	k2eAgInPc2d1	sponzorující
SPO	SPO	kA	SPO
<g/>
,	,	kIx,	,
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
brněnské	brněnský	k2eAgFnSc2d1	brněnská
advokátní	advokátní	k2eAgFnSc2d1	advokátní
kanceláře	kancelář	k1gFnSc2	kancelář
Daniel	Daniel	k1gMnSc1	Daniel
Janda	Janda	k1gMnSc1	Janda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Projevy	projev	k1gInPc1	projev
a	a	k8xC	a
názory	názor	k1gInPc1	názor
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Rozhovory	rozhovor	k1gInPc1	rozhovor
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2013	[number]	k4	2013
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
nenaváže	navázat	k5eNaPmIp3nS	navázat
na	na	k7c4	na
tradici	tradice	k1gFnSc4	tradice
prezidentských	prezidentský	k2eAgInPc2d1	prezidentský
novoročních	novoroční	k2eAgInPc2d1	novoroční
projevů	projev	k1gInPc2	projev
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
26.	[number]	k4	26.
prosince	prosinec	k1gInSc2	prosinec
vánoční	vánoční	k2eAgNnSc4d1	vánoční
poselství	poselství	k1gNnSc4	poselství
ze	z	k7c2	z
zámku	zámek	k1gInSc2	zámek
v	v	k7c6	v
Lánech	lán	k1gInPc6	lán
<g/>
.	.	kIx.	.
</s>
<s>
Chce	chtít	k5eAaImIp3nS	chtít
tak	tak	k6eAd1	tak
navázat	navázat	k5eAaPmF	navázat
na	na	k7c4	na
zvyk	zvyk	k1gInSc4	zvyk
z	z	k7c2	z
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
podobné	podobný	k2eAgInPc4d1	podobný
projevy	projev	k1gInPc4	projev
z	z	k7c2	z
období	období	k1gNnSc2	období
Vánoc	Vánoce	k1gFnPc2	Vánoce
měl	mít	k5eAaImAgMnS	mít
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigu	k1gFnSc2	Garrigu
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
.	.	kIx.	.
<g/>
Zeman	Zeman	k1gMnSc1	Zeman
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
k	k	k7c3	k
aktuálnímu	aktuální	k2eAgNnSc3d1	aktuální
politickému	politický	k2eAgNnSc3d1	politické
dění	dění	k1gNnSc3	dění
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
23.	[number]	k4	23.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
vysílal	vysílat	k5eAaImAgInS	vysílat
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
čtvrt	čtvrt	k1gFnSc4	čtvrt
roku	rok	k1gInSc2	rok
pořad	pořad	k1gInSc1	pořad
Hovory	hovora	k1gMnSc2	hovora
z	z	k7c2	z
Lán	lán	k1gInSc4	lán
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
hovořili	hovořit	k5eAaImAgMnP	hovořit
Jan	Jan	k1gMnSc1	Jan
Pokorný	Pokorný	k1gMnSc1	Pokorný
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
Nováček	Nováček	k1gMnSc1	Nováček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2014	[number]	k4	2014
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
použil	použít	k5eAaPmAgMnS	použít
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
několik	několik	k4yIc4	několik
hrubých	hrubý	k2eAgInPc2d1	hrubý
vulgarismů	vulgarismus	k1gInPc2	vulgarismus
na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
ruské	ruský	k2eAgFnSc2d1	ruská
punkové	punkový	k2eAgFnSc2d1	punková
skupiny	skupina	k1gFnSc2	skupina
Pussy	Pussa	k1gFnSc2	Pussa
Riot	Riot	k1gInSc1	Riot
(	(	kIx(	(
<g/>
kauza	kauza	k1gFnSc1	kauza
Hovory	hovora	k1gMnSc2	hovora
z	z	k7c2	z
Lán	lán	k1gInSc1	lán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
projev	projev	k1gInSc4	projev
sklidil	sklidit	k5eAaPmAgMnS	sklidit
ostrou	ostrý	k2eAgFnSc4d1	ostrá
kritiku	kritika	k1gFnSc4	kritika
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
politiků	politik	k1gMnPc2	politik
a	a	k8xC	a
komentátorů	komentátor	k1gMnPc2	komentátor
<g/>
,	,	kIx,	,
Rada	rada	k1gFnSc1	rada
pro	pro	k7c4	pro
rozhlasové	rozhlasový	k2eAgNnSc4d1	rozhlasové
a	a	k8xC	a
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
obdržela	obdržet	k5eAaPmAgFnS	obdržet
několik	několik	k4yIc4	několik
stovek	stovka	k1gFnPc2	stovka
stížností	stížnost	k1gFnPc2	stížnost
na	na	k7c6	na
vysílaní	vysílaný	k2eAgMnPc1d1	vysílaný
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
Pořad	pořad	k1gInSc1	pořad
se	se	k3xPyFc4	se
už	už	k6eAd1	už
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nevysílal	vysílat	k5eNaImAgInS	vysílat
<g/>
,	,	kIx,	,
rozhovory	rozhovor	k1gInPc1	rozhovor
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
začala	začít	k5eAaPmAgFnS	začít
od	od	k7c2	od
4.	[number]	k4	4.
ledna	leden	k1gInSc2	leden
připravovat	připravovat	k5eAaImF	připravovat
stanice	stanice	k1gFnSc1	stanice
Frekvence	frekvence	k1gFnSc1	frekvence
1	[number]	k4	1
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
pressklub	pressklub	k1gInSc4	pressklub
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
února	únor	k1gInSc2	únor
2015	[number]	k4	2015
probíhá	probíhat	k5eAaImIp3nS	probíhat
cyklus	cyklus	k1gInSc1	cyklus
rozhovorů	rozhovor	k1gInPc2	rozhovor
S	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
v	v	k7c6	v
Lánech	lán	k1gInPc6	lán
pro	pro	k7c4	pro
bulvární	bulvární	k2eAgInSc4d1	bulvární
deník	deník	k1gInSc4	deník
Blesk	blesk	k1gInSc1	blesk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2017	[number]	k4	2017
začal	začít	k5eAaPmAgInS	začít
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
čtvrteční	čtvrteční	k2eAgInSc1d1	čtvrteční
diskuzní	diskuzní	k2eAgInSc1d1	diskuzní
pořad	pořad	k1gInSc1	pořad
s	s	k7c7	s
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
na	na	k7c6	na
televizní	televizní	k2eAgFnSc6d1	televizní
stanici	stanice	k1gFnSc6	stanice
TV	TV	kA	TV
Barrandov	Barrandov	k1gInSc1	Barrandov
<g/>
.	.	kIx.	.
</s>
<s>
Třiceti	třicet	k4xCc3	třicet
pětiminutový	pětiminutový	k2eAgInSc1d1	pětiminutový
pořad	pořad	k1gInSc1	pořad
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc1	název
TÝDEN	týden	k1gInSc1	týden
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2015	[number]	k4	2015
poprvé	poprvé	k6eAd1	poprvé
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
Pressklub	Pressklub	k1gInSc4	Pressklub
na	na	k7c6	na
rádiu	rádius	k1gInSc6	rádius
Frekvence	frekvence	k1gFnSc2	frekvence
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgNnSc6	tento
vystoupení	vystoupení	k1gNnSc6	vystoupení
požadoval	požadovat	k5eAaImAgMnS	požadovat
prezident	prezident	k1gMnSc1	prezident
Zeman	Zeman	k1gMnSc1	Zeman
odchod	odchod	k1gInSc4	odchod
Řecka	Řecko	k1gNnSc2	Řecko
z	z	k7c2	z
Evropské	evropský	k2eAgFnSc2d1	Evropská
měnové	měnový	k2eAgFnSc2d1	měnová
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gFnPc2	jeho
informací	informace	k1gFnPc2	informace
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
pomocí	pomocí	k7c2	pomocí
zfalšování	zfalšování	k1gNnSc2	zfalšování
požadovaných	požadovaný	k2eAgInPc2d1	požadovaný
statistických	statistický	k2eAgInPc2d1	statistický
ukazatelů	ukazatel	k1gInPc2	ukazatel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
rozhovoru	rozhovor	k1gInSc6	rozhovor
také	také	k9	také
ostře	ostro	k6eAd1	ostro
zkritizoval	zkritizovat	k5eAaPmAgMnS	zkritizovat
pochodňové	pochodňový	k2eAgInPc4d1	pochodňový
průvody	průvod	k1gInPc4	průvod
zorganizované	zorganizovaný	k2eAgFnSc2d1	zorganizovaná
jako	jako	k8xC	jako
připomínka	připomínka	k1gFnSc1	připomínka
narození	narození	k1gNnSc2	narození
ukrajinského	ukrajinský	k2eAgMnSc2d1	ukrajinský
nacionalisty	nacionalista	k1gMnSc2	nacionalista
Stepana	Stepan	k1gMnSc2	Stepan
Bandery	Bandera	k1gFnSc2	Bandera
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
přirovnal	přirovnat	k5eAaPmAgInS	přirovnat
k	k	k7c3	k
říšskému	říšský	k2eAgMnSc3d1	říšský
protektorovi	protektor	k1gMnSc3	protektor
Reinhardu	Reinhard	k1gMnSc3	Reinhard
Heydrichovi	Heydrich	k1gMnSc3	Heydrich
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
8.	[number]	k4	8.
února	únor	k1gInSc2	únor
2015	[number]	k4	2015
se	se	k3xPyFc4	se
Zeman	Zeman	k1gMnSc1	Zeman
v	v	k7c6	v
online	onlin	k1gInSc5	onlin
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
Blesk	blesk	k1gInSc4	blesk
TV	TV	kA	TV
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
ke	k	k7c3	k
kauze	kauza	k1gFnSc3	kauza
rodiny	rodina	k1gFnSc2	rodina
Michalákových	Michalákův	k2eAgInPc2d1	Michalákův
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
přirovnal	přirovnat	k5eAaPmAgMnS	přirovnat
norskou	norský	k2eAgFnSc4d1	norská
sociální	sociální	k2eAgFnSc4d1	sociální
službu	služba	k1gFnSc4	služba
Barnevernet	Barneverneta	k1gFnPc2	Barneverneta
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
děti	dítě	k1gFnPc4	dítě
odebrala	odebrat	k5eAaPmAgFnS	odebrat
<g/>
,	,	kIx,	,
k	k	k7c3	k
nacistickému	nacistický	k2eAgInSc3d1	nacistický
programu	program	k1gInSc3	program
Lebensborn	Lebensborna	k1gFnPc2	Lebensborna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
2.	[number]	k4	2.
srpna	srpen	k1gInSc2	srpen
2015	[number]	k4	2015
se	se	k3xPyFc4	se
Zeman	Zeman	k1gMnSc1	Zeman
v	v	k7c6	v
online	onlin	k1gInSc5	onlin
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
Blesk	blesk	k1gInSc4	blesk
TV	TV	kA	TV
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
k	k	k7c3	k
uprchlické	uprchlický	k2eAgFnSc3d1	uprchlická
krizi	krize	k1gFnSc3	krize
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vzkázal	vzkázat	k5eAaPmAgMnS	vzkázat
uprchlíkům	uprchlík	k1gMnPc3	uprchlík
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
Česka	Česko	k1gNnSc2	Česko
nikdo	nikdo	k3yNnSc1	nikdo
nezval	zvát	k5eNaImAgMnS	zvát
<g/>
.	.	kIx.	.
</s>
<s>
Uprchlíci	uprchlík	k1gMnPc1	uprchlík
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
musí	muset	k5eAaImIp3nP	muset
respektovat	respektovat	k5eAaImF	respektovat
pravidla	pravidlo	k1gNnSc2	pravidlo
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
vám	vy	k3xPp2nPc3	vy
to	ten	k3xDgNnSc1	ten
nelíbí	líbit	k5eNaImIp3nS	líbit
<g/>
,	,	kIx,	,
běžte	běžet	k5eAaImRp2nP	běžet
pryč	pryč	k6eAd1	pryč
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
označil	označit	k5eAaPmAgMnS	označit
podnikatele	podnikatel	k1gMnSc4	podnikatel
Zdeňka	Zdeněk	k1gMnSc4	Zdeněk
Bakalu	Bakal	k1gMnSc6	Bakal
za	za	k7c4	za
"	"	kIx"	"
<g/>
velmi	velmi	k6eAd1	velmi
neúspěšného	úspěšný	k2eNgMnSc4d1	neúspěšný
podnikatele	podnikatel	k1gMnSc4	podnikatel
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
lidskou	lidský	k2eAgFnSc4d1	lidská
hyenu	hyena	k1gFnSc4	hyena
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jeho	jeho	k3xOp3gFnSc1	jeho
firma	firma	k1gFnSc1	firma
RPG	RPG	kA	RPG
Byty	byt	k1gInPc4	byt
jedná	jednat	k5eAaImIp3nS	jednat
s	s	k7c7	s
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
běžence	běženka	k1gFnSc3	běženka
umístilo	umístit	k5eAaPmAgNnS	umístit
v	v	k7c6	v
bytech	byt	k1gInPc6	byt
po	po	k7c6	po
hornících	horník	k1gMnPc6	horník
v	v	k7c6	v
Havířově	Havířov	k1gInSc6	Havířov
za	za	k7c4	za
200	[number]	k4	200
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2015	[number]	k4	2015
Zeman	Zeman	k1gMnSc1	Zeman
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
vánočním	vánoční	k2eAgNnSc6d1	vánoční
poselství	poselství	k1gNnSc6	poselství
přivítal	přivítat	k5eAaPmAgInS	přivítat
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
růst	růst	k1gInSc1	růst
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
ocenil	ocenit	k5eAaPmAgMnS	ocenit
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
i	i	k9	i
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
odkazem	odkaz	k1gInSc7	odkaz
na	na	k7c4	na
exprezidenta	exprezident	k1gMnSc4	exprezident
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
skončila	skončit	k5eAaPmAgFnS	skončit
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
blbá	blbý	k2eAgFnSc1d1	blbá
nálada	nálada	k1gFnSc1	nálada
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
také	také	k9	také
v	v	k7c6	v
projevu	projev	k1gInSc6	projev
odvysílaném	odvysílaný	k2eAgInSc6d1	odvysílaný
ze	z	k7c2	z
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Lánech	lán	k1gInPc6	lán
znovu	znovu	k6eAd1	znovu
varoval	varovat	k5eAaImAgMnS	varovat
před	před	k7c7	před
migrační	migrační	k2eAgFnSc7d1	migrační
vlnou	vlna	k1gFnSc7	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
tahle	tenhle	k3xDgFnSc1	tenhle
země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
naše	náš	k3xOp1gNnSc1	náš
a	a	k8xC	a
tahle	tenhle	k3xDgFnSc1	tenhle
země	země	k1gFnSc1	země
není	být	k5eNaImIp3nS	být
a	a	k8xC	a
ani	ani	k8xC	ani
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2016	[number]	k4	2016
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
Český	český	k2eAgInSc4d1	český
rozhlas	rozhlas	k1gInSc4	rozhlas
Plus	plus	k6eAd1	plus
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
migrační	migrační	k2eAgFnSc4d1	migrační
vlnu	vlna	k1gFnSc4	vlna
organizuje	organizovat	k5eAaBmIp3nS	organizovat
Muslimské	muslimský	k2eAgNnSc4d1	muslimské
bratrstvo	bratrstvo	k1gNnSc4	bratrstvo
<g/>
,	,	kIx,	,
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
opřel	opřít	k5eAaPmAgMnS	opřít
o	o	k7c4	o
informace	informace	k1gFnPc4	informace
od	od	k7c2	od
marockého	marocký	k2eAgMnSc2d1	marocký
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Selahedína	Selahedín	k1gMnSc2	Selahedín
Mezuára	Mezuár	k1gMnSc2	Mezuár
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
nedávno	nedávno	k6eAd1	nedávno
hovořil	hovořit	k5eAaImAgMnS	hovořit
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
Sobotkovu	Sobotkův	k2eAgFnSc4d1	Sobotkova
vládu	vláda	k1gFnSc4	vláda
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ČR	ČR	kA	ČR
nevyčerpá	vyčerpat	k5eNaPmIp3nS	vyčerpat
35	[number]	k4	35
miliard	miliarda	k4xCgFnPc2	miliarda
z	z	k7c2	z
evropských	evropský	k2eAgFnPc2d1	Evropská
dotací	dotace	k1gFnPc2	dotace
<g/>
,	,	kIx,	,
mluvil	mluvit	k5eAaImAgMnS	mluvit
o	o	k7c6	o
laxnosti	laxnost	k1gFnSc6	laxnost
a	a	k8xC	a
neschopnosti	neschopnost	k1gFnSc6	neschopnost
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
online	onlinout	k5eAaPmIp3nS	onlinout
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
Blesk	blesk	k1gInSc4	blesk
TV	TV	kA	TV
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
norskou	norský	k2eAgFnSc4d1	norská
sociální	sociální	k2eAgFnSc4d1	sociální
službu	služba	k1gFnSc4	služba
Barnevernet	Barneverneta	k1gFnPc2	Barneverneta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
"	"	kIx"	"
<g/>
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
unáší	unášet	k5eAaImIp3nP	unášet
děti	dítě	k1gFnPc1	dítě
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
činnost	činnost	k1gFnSc1	činnost
je	být	k5eAaImIp3nS	být
gangsterismus	gangsterismus	k1gInSc1	gangsterismus
nejhrubšího	hrubý	k2eAgNnSc2d3	nejhrubší
zrna	zrno	k1gNnSc2	zrno
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Reagoval	reagovat	k5eAaBmAgMnS	reagovat
tak	tak	k9	tak
na	na	k7c4	na
kauzu	kauza	k1gFnSc4	kauza
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
české	český	k2eAgFnSc3d1	Česká
matce	matka	k1gFnSc3	matka
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
odebrána	odebrán	k2eAgFnSc1d1	odebrána
dcera	dcera	k1gFnSc1	dcera
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Partie	partie	k1gFnSc1	partie
televize	televize	k1gFnSc1	televize
Prima	prima	k1gFnSc1	prima
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
připravený	připravený	k2eAgInSc4d1	připravený
návrh	návrh	k1gInSc4	návrh
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
by	by	kYmCp3nP	by
děti	dítě	k1gFnPc1	dítě
s	s	k7c7	s
českým	český	k2eAgNnSc7d1	české
občanstvím	občanství	k1gNnSc7	občanství
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
vydávány	vydáván	k2eAgInPc1d1	vydáván
svým	svůj	k3xOyFgMnPc3	svůj
rodičům	rodič	k1gMnPc3	rodič
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
března	březen	k1gInSc2	březen
a	a	k8xC	a
dubna	duben	k1gInSc2	duben
2017	[number]	k4	2017
vzbudilo	vzbudit	k5eAaPmAgNnS	vzbudit
i	i	k9	i
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
pozornost	pozornost	k1gFnSc4	pozornost
jeho	jeho	k3xOp3gNnSc2	jeho
prohlášení	prohlášení	k1gNnSc2	prohlášení
pro	pro	k7c4	pro
Frekvenci	frekvence	k1gFnSc4	frekvence
1	[number]	k4	1
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
hackeři	hacker	k1gMnPc1	hacker
z	z	k7c2	z
Alabamy	Alabam	k1gInPc7	Alabam
na	na	k7c4	na
počítač	počítač	k1gInSc4	počítač
v	v	k7c6	v
Lánech	lán	k1gInPc6	lán
nainstalovali	nainstalovat	k5eAaPmAgMnP	nainstalovat
dětskou	dětský	k2eAgFnSc4d1	dětská
pornografii	pornografie	k1gFnSc4	pornografie
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
chtěla	chtít	k5eAaImAgFnS	chtít
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
kancelář	kancelář	k1gFnSc1	kancelář
nechat	nechat	k5eAaPmF	nechat
záležitost	záležitost	k1gFnSc4	záležitost
prošetřit	prošetřit	k5eAaPmF	prošetřit
Národním	národní	k2eAgInSc7d1	národní
bezpečnostním	bezpečnostní	k2eAgInSc7d1	bezpečnostní
úřadem	úřad	k1gInSc7	úřad
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
ustoupila	ustoupit	k5eAaPmAgFnS	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
Zprávy	zpráva	k1gFnSc2	zpráva
uvedly	uvést	k5eAaPmAgInP	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
k	k	k7c3	k
pornografii	pornografie	k1gFnSc3	pornografie
prezident	prezident	k1gMnSc1	prezident
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
dostal	dostat	k5eAaPmAgMnS	dostat
omylem	omylem	k6eAd1	omylem
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
zásahu	zásah	k1gInSc2	zásah
hackerů	hacker	k1gMnPc2	hacker
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
na	na	k7c6	na
TV	TV	kA	TV
Barrandov	Barrandov	k1gInSc1	Barrandov
Bezpečnostní	bezpečnostní	k2eAgFnSc4d1	bezpečnostní
informační	informační	k2eAgFnSc4d1	informační
službu	služba	k1gFnSc4	služba
(	(	kIx(	(
<g/>
BIS	BIS	kA	BIS
<g/>
)	)	kIx)	)
za	za	k7c4	za
jejich	jejich	k3xOp3gFnSc4	jejich
výroční	výroční	k2eAgFnSc4d1	výroční
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
z	z	k7c2	z
<g/>
]	]	kIx)	]
<g/>
a	a	k8xC	a
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
není	být	k5eNaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
údaj	údaj	k1gInSc1	údaj
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
odhalit	odhalit	k5eAaPmF	odhalit
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
jen	jen	k9	jen
jediného	jediný	k2eAgMnSc4d1	jediný
ruského	ruský	k2eAgMnSc4d1	ruský
nebo	nebo	k8xC	nebo
čínského	čínský	k2eAgMnSc4d1	čínský
špióna	špión	k1gMnSc4	špión
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Toto	tento	k3xDgNnSc1	tento
tvrzení	tvrzení	k1gNnSc1	tvrzení
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jako	jako	k9	jako
nepravdivé	pravdivý	k2eNgNnSc1d1	nepravdivé
<g/>
.	.	kIx.	.
</s>
<s>
BIS	BIS	kA	BIS
odhalila	odhalit	k5eAaPmAgFnS	odhalit
ruské	ruský	k2eAgMnPc4d1	ruský
špiony	špion	k1gMnPc4	špion
například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Ředitel	ředitel	k1gMnSc1	ředitel
BIS	BIS	kA	BIS
Michal	Michal	k1gMnSc1	Michal
Koudelka	Koudelka	k1gMnSc1	Koudelka
v	v	k7c6	v
tiskové	tiskový	k2eAgFnSc6d1	tisková
zprávě	zpráva	k1gFnSc6	zpráva
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
zamezili	zamezit	k5eAaPmAgMnP	zamezit
desítkám	desítka	k1gFnPc3	desítka
ruských	ruský	k2eAgMnPc2d1	ruský
a	a	k8xC	a
čínských	čínský	k2eAgMnPc2d1	čínský
zpravodajských	zpravodajský	k2eAgMnPc2d1	zpravodajský
důstojníků	důstojník	k1gMnPc2	důstojník
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
činnosti	činnost	k1gFnSc6	činnost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Domácí	domácí	k2eAgFnSc1d1	domácí
politika	politika	k1gFnSc1	politika
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
levicového	levicový	k2eAgMnSc4d1	levicový
politika	politik	k1gMnSc4	politik
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
tolerantního	tolerantní	k2eAgMnSc4d1	tolerantní
ateistu	ateista	k1gMnSc4	ateista
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
však	však	k9	však
byl	být	k5eAaImAgInS	být
římským	římský	k2eAgMnSc7d1	římský
katolíkem	katolík	k1gMnSc7	katolík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
ekonomie	ekonomie	k1gFnSc2	ekonomie
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
keynesiánce	keynesiánec	k1gMnPc4	keynesiánec
<g/>
.	.	kIx.	.
</s>
<s>
Vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
církevním	církevní	k2eAgFnPc3d1	církevní
restitucím	restituce	k1gFnPc3	restituce
<g/>
.	.	kIx.	.
<g/>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
90.	[number]	k4	90.
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgMnS	být
hlasitým	hlasitý	k2eAgMnSc7d1	hlasitý
kritikem	kritik	k1gMnSc7	kritik
referenda	referendum	k1gNnSc2	referendum
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Současně	současně	k6eAd1	současně
bych	by	kYmCp1nS	by
však	však	k9	však
chtěl	chtít	k5eAaImAgMnS	chtít
varovat	varovat	k5eAaImF	varovat
před	před	k7c7	před
obecně	obecně	k6eAd1	obecně
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
a	a	k8xC	a
dalo	dát	k5eAaPmAgNnS	dát
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
říci	říct	k5eAaPmF	říct
populistickou	populistický	k2eAgFnSc7d1	populistická
iluzí	iluze	k1gFnSc7	iluze
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
názoru	názor	k1gInSc2	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlas	hlas	k1gInSc1	hlas
lidu	lid	k1gInSc2	lid
je	být	k5eAaImIp3nS	být
hlasem	hlas	k1gInSc7	hlas
božím	boží	k2eAgInSc7d1	boží
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
nespraví	spravit	k5eNaPmIp3nS	spravit
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
spraví	spravit	k5eAaPmIp3nS	spravit
občané	občan	k1gMnPc1	občan
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Zeman	Zeman	k1gMnSc1	Zeman
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
společnosti	společnost	k1gFnSc2	společnost
nemá	mít	k5eNaImIp3nS	mít
takové	takový	k3xDgFnPc4	takový
rozumové	rozumový	k2eAgFnPc4d1	rozumová
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
porozuměla	porozumět	k5eAaPmAgFnS	porozumět
komplexnosti	komplexnost	k1gFnSc3	komplexnost
problému	problém	k1gInSc2	problém
a	a	k8xC	a
nebyla	být	k5eNaImAgFnS	být
tak	tak	k6eAd1	tak
odkázána	odkázat	k5eAaPmNgFnS	odkázat
jen	jen	k9	jen
na	na	k7c4	na
zjednodušené	zjednodušený	k2eAgNnSc4d1	zjednodušené
černobílé	černobílý	k2eAgNnSc4d1	černobílé
vidění	vidění	k1gNnSc4	vidění
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
odkázal	odkázat	k5eAaPmAgInS	odkázat
na	na	k7c4	na
citát	citát	k1gInSc4	citát
Cyrila	Cyril	k1gMnSc2	Cyril
Höschla	Höschla	k1gMnSc2	Höschla
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Třetina	třetina	k1gFnSc1	třetina
obyvatel	obyvatel	k1gMnPc2	obyvatel
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
slabá	slabý	k2eAgFnSc1d1	slabá
duchem	duch	k1gMnSc7	duch
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
sedmý	sedmý	k4xOgMnSc1	sedmý
občan	občan	k1gMnSc1	občan
je	být	k5eAaImIp3nS	být
debilní	debilní	k2eAgMnSc1d1	debilní
nebo	nebo	k8xC	nebo
dementní	dementní	k2eAgMnSc1d1	dementní
nebo	nebo	k8xC	nebo
alkoholik	alkoholik	k1gMnSc1	alkoholik
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
polovina	polovina	k1gFnSc1	polovina
obyvatel	obyvatel	k1gMnPc2	obyvatel
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
má	mít	k5eAaImIp3nS	mít
podprůměrný	podprůměrný	k2eAgInSc1d1	podprůměrný
intelekt	intelekt	k1gInSc1	intelekt
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
proto	proto	k8xC	proto
přání	přání	k1gNnSc4	přání
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
aby	aby	kYmCp3nS	aby
k	k	k7c3	k
referendu	referendum	k1gNnSc3	referendum
–	–	k?	–
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
–	–	k?	–
nemuselo	muset	k5eNaImAgNnS	muset
dojít	dojít	k5eAaPmF	dojít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
úřadu	úřad	k1gInSc2	úřad
podporuje	podporovat	k5eAaImIp3nS	podporovat
přímou	přímý	k2eAgFnSc4d1	přímá
demokracii	demokracie	k1gFnSc4	demokracie
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
většího	veliký	k2eAgNnSc2d2	veliký
uplatnění	uplatnění	k1gNnSc2	uplatnění
referend	referendum	k1gNnPc2	referendum
<g/>
,	,	kIx,	,
zavedení	zavedení	k1gNnSc1	zavedení
určité	určitý	k2eAgFnSc2d1	určitá
podoby	podoba	k1gFnSc2	podoba
sociálního	sociální	k2eAgInSc2d1	sociální
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
podporuje	podporovat	k5eAaImIp3nS	podporovat
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
státní	státní	k2eAgFnSc4d1	státní
sociální	sociální	k2eAgFnSc4d1	sociální
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
důchodce	důchodce	k1gMnPc4	důchodce
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
progresivní	progresivní	k2eAgFnSc1d1	progresivní
daň	daň	k1gFnSc1	daň
z	z	k7c2	z
příjmů	příjem	k1gInPc2	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Chce	chtít	k5eAaImIp3nS	chtít
se	se	k3xPyFc4	se
také	také	k9	také
zasadit	zasadit	k5eAaPmF	zasadit
o	o	k7c4	o
zavedení	zavedení	k1gNnSc4	zavedení
povinného	povinný	k2eAgNnSc2d1	povinné
prokázání	prokázání	k1gNnSc2	prokázání
nabytí	nabytí	k1gNnSc2	nabytí
majetku	majetek	k1gInSc2	majetek
včetně	včetně	k7c2	včetně
případné	případný	k2eAgFnSc2d1	případná
konfiskace	konfiskace	k1gFnSc2	konfiskace
nelegálně	legálně	k6eNd1	legálně
získaného	získaný	k2eAgInSc2d1	získaný
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
volebního	volební	k2eAgInSc2d1	volební
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
u	u	k7c2	u
parlamentních	parlamentní	k2eAgFnPc2d1	parlamentní
voleb	volba	k1gFnPc2	volba
umožnila	umožnit	k5eAaPmAgFnS	umožnit
vybírat	vybírat	k5eAaImF	vybírat
kandidáty	kandidát	k1gMnPc4	kandidát
z	z	k7c2	z
několika	několik	k4yIc2	několik
kandidátek	kandidátka	k1gFnPc2	kandidátka
zároveň	zároveň	k6eAd1	zároveň
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
panašování	panašování	k1gNnSc2	panašování
<g/>
.	.	kIx.	.
</s>
<s>
Podporuje	podporovat	k5eAaImIp3nS	podporovat
zavedení	zavedení	k1gNnSc4	zavedení
institutu	institut	k1gInSc2	institut
referenda	referendum	k1gNnSc2	referendum
dle	dle	k7c2	dle
švýcarského	švýcarský	k2eAgInSc2d1	švýcarský
modelu	model	k1gInSc2	model
a	a	k8xC	a
odvolatelnost	odvolatelnost	k1gFnSc4	odvolatelnost
poslanců	poslanec	k1gMnPc2	poslanec
lidovým	lidový	k2eAgNnSc7d1	lidové
hlasováním	hlasování	k1gNnSc7	hlasování
<g/>
.	.	kIx.	.
</s>
<s>
Uvítal	uvítat	k5eAaPmAgMnS	uvítat
by	by	kYmCp3nS	by
transformaci	transformace	k1gFnSc4	transformace
Senátu	senát	k1gInSc2	senát
z	z	k7c2	z
přímo	přímo	k6eAd1	přímo
volené	volený	k2eAgFnSc2d1	volená
komory	komora	k1gFnSc2	komora
na	na	k7c4	na
orgán	orgán	k1gInSc4	orgán
tvořený	tvořený	k2eAgInSc4d1	tvořený
reprezentacemi	reprezentace	k1gFnPc7	reprezentace
krajských	krajský	k2eAgNnPc2d1	krajské
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
otázce	otázka	k1gFnSc3	otázka
přijetí	přijetí	k1gNnSc2	přijetí
eura	euro	k1gNnSc2	euro
je	být	k5eAaImIp3nS	být
otevřený	otevřený	k2eAgInSc1d1	otevřený
<g/>
,	,	kIx,	,
chce	chtít	k5eAaImIp3nS	chtít
ho	on	k3xPp3gMnSc4	on
přijmout	přijmout	k5eAaPmF	přijmout
<g/>
,	,	kIx,	,
co	co	k9	co
nejdříve	dříve	k6eAd3	dříve
to	ten	k3xDgNnSc1	ten
bude	být	k5eAaImBp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
reálnou	reálný	k2eAgFnSc4d1	reálná
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
zavedení	zavedení	k1gNnSc4	zavedení
eura	euro	k1gNnSc2	euro
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vidí	vidět	k5eAaImIp3nS	vidět
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2017.	[number]	k4	2017.
<g/>
Podporuje	podporovat	k5eAaImIp3nS	podporovat
již	již	k6eAd1	již
zavedené	zavedený	k2eAgNnSc1d1	zavedené
zvýšení	zvýšení	k1gNnSc1	zvýšení
základní	základní	k2eAgFnSc2d1	základní
sazby	sazba	k1gFnSc2	sazba
DPH	DPH	kA	DPH
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
snížena	snížit	k5eAaPmNgFnS	snížit
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
snížená	snížený	k2eAgFnSc1d1	snížená
sazba	sazba	k1gFnSc1	sazba
<g/>
"	"	kIx"	"
na	na	k7c4	na
základní	základní	k2eAgFnPc4d1	základní
potraviny	potravina	k1gFnPc4	potravina
a	a	k8xC	a
léky	lék	k1gInPc4	lék
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
zavedení	zavedení	k1gNnSc3	zavedení
školného	školné	k1gNnSc2	školné
na	na	k7c6	na
veřejných	veřejný	k2eAgFnPc6d1	veřejná
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podpořil	podpořit	k5eAaPmAgInS	podpořit
by	by	k9	by
zavedení	zavedení	k1gNnSc4	zavedení
pokut	pokuta	k1gFnPc2	pokuta
pro	pro	k7c4	pro
neúspěšné	úspěšný	k2eNgMnPc4d1	neúspěšný
studenty	student	k1gMnPc4	student
<g/>
.	.	kIx.	.
</s>
<s>
Odmítavý	odmítavý	k2eAgInSc1d1	odmítavý
postoj	postoj	k1gInSc1	postoj
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
i	i	k9	i
ohledně	ohledně	k7c2	ohledně
legalizace	legalizace	k1gFnSc2	legalizace
měkkých	měkký	k2eAgFnPc2d1	měkká
i	i	k8xC	i
tvrdých	tvrdý	k2eAgFnPc2d1	tvrdá
drog	droga	k1gFnPc2	droga
na	na	k7c4	na
jiné	jiný	k2eAgInPc4d1	jiný
než	než	k8xS	než
lékařské	lékařský	k2eAgInPc4d1	lékařský
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
podporuje	podporovat	k5eAaImIp3nS	podporovat
zrovnoprávnění	zrovnoprávnění	k1gNnSc4	zrovnoprávnění
homosexuálních	homosexuální	k2eAgInPc2d1	homosexuální
párů	pár	k1gInPc2	pár
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
adopce	adopce	k1gFnSc2	adopce
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
<g/>
Jako	jako	k8xS	jako
prezident	prezident	k1gMnSc1	prezident
podporuje	podporovat	k5eAaImIp3nS	podporovat
dokončení	dokončení	k1gNnSc4	dokončení
vodního	vodní	k2eAgInSc2d1	vodní
koridoru	koridor	k1gInSc2	koridor
Dunaj-Odra-Labe	Dunaj-Odra-Lab	k1gMnSc5	Dunaj-Odra-Lab
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
===	===	k?	===
</s>
</p>
<p>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
je	být	k5eAaImIp3nS	být
zastáncem	zastánce	k1gMnSc7	zastánce
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
eurofederalistou	eurofederalista	k1gMnSc7	eurofederalista
<g/>
,	,	kIx,	,
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
tedy	tedy	k9	tedy
proti	proti	k7c3	proti
extrémnímu	extrémní	k2eAgInSc3d1	extrémní
centralismu	centralismus	k1gInSc3	centralismus
v	v	k7c6	v
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
se	se	k3xPyFc4	se
vstupem	vstup	k1gInSc7	vstup
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
do	do	k7c2	do
EU	EU	kA	EU
a	a	k8xC	a
v	v	k7c6	v
EU	EU	kA	EU
si	se	k3xPyFc3	se
dle	dle	k7c2	dle
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
dokáže	dokázat	k5eAaPmIp3nS	dokázat
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
představit	představit	k5eAaPmF	představit
i	i	k9	i
Srbsko	Srbsko	k1gNnSc1	Srbsko
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Odmítavě	odmítavě	k6eAd1	odmítavě
jako	jako	k9	jako
o	o	k7c6	o
nové	nový	k2eAgFnSc6d1	nová
členské	členský	k2eAgFnSc6d1	členská
zemi	zem	k1gFnSc6	zem
EU	EU	kA	EU
naopak	naopak	k6eAd1	naopak
hovořil	hovořit	k5eAaImAgMnS	hovořit
o	o	k7c6	o
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
také	také	k9	také
uznání	uznání	k1gNnSc4	uznání
Kosova	Kosův	k2eAgInSc2d1	Kosův
samostatným	samostatný	k2eAgInSc7d1	samostatný
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jej	on	k3xPp3gMnSc4	on
označil	označit	k5eAaPmAgMnS	označit
"	"	kIx"	"
<g/>
za	za	k7c4	za
teroristickou	teroristický	k2eAgFnSc4d1	teroristická
diktaturu	diktatura	k1gFnSc4	diktatura
financovanou	financovaný	k2eAgFnSc4d1	financovaná
narkomafiemi	narkomafie	k1gFnPc7	narkomafie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Společnou	společný	k2eAgFnSc4d1	společná
fiskální	fiskální	k2eAgFnSc4d1	fiskální
politiku	politika	k1gFnSc4	politika
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
srbský	srbský	k2eAgInSc4d1	srbský
deník	deník	k1gInSc4	deník
Večernje	Večernj	k1gInSc2	Večernj
novosti	novost	k1gFnSc2	novost
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
uznání	uznání	k1gNnSc1	uznání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Kosova	Kosův	k2eAgInSc2d1	Kosův
otevřelo	otevřít	k5eAaPmAgNnS	otevřít
Pandořinu	Pandořin	k2eAgFnSc4d1	Pandořina
skříňku	skříňka	k1gFnSc4	skříňka
a	a	k8xC	a
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
nebezpečný	bezpečný	k2eNgInSc1d1	nebezpečný
proces	proces	k1gInSc1	proces
překreslování	překreslování	k1gNnSc2	překreslování
hranic	hranice	k1gFnPc2	hranice
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Současného	současný	k2eAgMnSc2d1	současný
kosovského	kosovský	k2eAgMnSc2d1	kosovský
vicepremiéra	vicepremiér	k1gMnSc2	vicepremiér
Hashima	Hashim	k1gMnSc2	Hashim
Thaçiho	Thaçi	k1gMnSc2	Thaçi
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
90.	[number]	k4	90.
letech	léto	k1gNnPc6	léto
stál	stát	k5eAaImAgMnS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Kosovské	kosovský	k2eAgFnSc2d1	Kosovská
osvobozenské	osvobozenský	k2eAgFnSc2d1	osvobozenský
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
která	který	k3yIgFnSc1	který
na	na	k7c6	na
černém	černý	k2eAgInSc6d1	černý
trhu	trh	k1gInSc6	trh
údajně	údajně	k6eAd1	údajně
prodávala	prodávat	k5eAaImAgFnS	prodávat
orgány	orgán	k1gInPc4	orgán
svých	svůj	k3xOyFgMnPc2	svůj
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
,	,	kIx,	,
nazval	nazvat	k5eAaBmAgMnS	nazvat
válečným	válečný	k2eAgMnSc7d1	válečný
zločincem	zločinec	k1gMnSc7	zločinec
<g/>
,	,	kIx,	,
Kosovo	Kosův	k2eAgNnSc1d1	Kosovo
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
velmi	velmi	k6eAd1	velmi
podivný	podivný	k2eAgInSc1d1	podivný
stát	stát	k1gInSc1	stát
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
vlivem	vliv	k1gInSc7	vliv
narkomafií	narkomafie	k1gFnPc2	narkomafie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
osobně	osobně	k6eAd1	osobně
nezávislost	nezávislost	k1gFnSc1	nezávislost
Kosova	Kosův	k2eAgFnSc1d1	Kosova
nikdy	nikdy	k6eAd1	nikdy
neuznal	uznat	k5eNaPmAgMnS	uznat
<g/>
.	.	kIx.	.
<g/>
Zeman	Zeman	k1gMnSc1	Zeman
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
Polska	Polsko	k1gNnSc2	Polsko
ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
Polska	Polsko	k1gNnSc2	Polsko
s	s	k7c7	s
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
polským	polský	k2eAgMnSc7d1	polský
prezidentem	prezident	k1gMnSc7	prezident
Andrzejem	Andrzej	k1gMnSc7	Andrzej
Dudou	Duda	k1gMnSc7	Duda
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2016	[number]	k4	2016
Zeman	Zeman	k1gMnSc1	Zeman
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Polská	polský	k2eAgFnSc1d1	polská
vláda	vláda	k1gFnSc1	vláda
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
ze	z	k7c2	z
svobodných	svobodný	k2eAgFnPc2d1	svobodná
voleb	volba	k1gFnPc2	volba
má	mít	k5eAaImIp3nS	mít
plné	plný	k2eAgNnSc4d1	plné
právo	právo	k1gNnSc4	právo
uskutečňovat	uskutečňovat	k5eAaImF	uskutečňovat
opatření	opatření	k1gNnSc4	opatření
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jí	on	k3xPp3gFnSc3	on
dává	dávat	k5eAaImIp3nS	dávat
mandát	mandát	k1gInSc4	mandát
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Neměla	mít	k5eNaImAgFnS	mít
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
vystavena	vystavit	k5eAaPmNgFnS	vystavit
moralizující	moralizující	k2eAgFnSc3d1	moralizující
kritice	kritika	k1gFnSc3	kritika
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
měla	mít	k5eAaImAgFnS	mít
konečně	konečně	k6eAd1	konečně
soustředit	soustředit	k5eAaPmF	soustředit
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
základní	základní	k2eAgInSc4d1	základní
úkol	úkol	k1gInSc4	úkol
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
ochrana	ochrana	k1gFnSc1	ochrana
vnějších	vnější	k2eAgFnPc2d1	vnější
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Zahraniční	zahraniční	k2eAgInPc1d1	zahraniční
vztahy	vztah	k1gInPc1	vztah
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
USA	USA	kA	USA
====	====	k?	====
</s>
</p>
<p>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
jako	jako	k9	jako
nesmyslné	smyslný	k2eNgNnSc1d1	nesmyslné
přirovnávat	přirovnávat	k5eAaImF	přirovnávat
průjezd	průjezd	k1gInSc4	průjezd
amerického	americký	k2eAgInSc2d1	americký
konvoje	konvoj	k1gInSc2	konvoj
vracejícího	vracející	k2eAgNnSc2d1	vracející
se	se	k3xPyFc4	se
z	z	k7c2	z
vojenského	vojenský	k2eAgNnSc2d1	vojenské
cvičení	cvičení	k1gNnSc2	cvičení
NATO	NATO	kA	NATO
v	v	k7c6	v
Pobaltí	Pobaltí	k1gNnSc6	Pobaltí
k	k	k7c3	k
okupaci	okupace	k1gFnSc3	okupace
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
měsících	měsíc	k1gInPc6	měsíc
bojuji	bojovat	k5eAaImIp1nS	bojovat
proti	proti	k7c3	proti
antiruským	antiruský	k2eAgMnPc3d1	antiruský
bláznům	blázen	k1gMnPc3	blázen
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
přibyl	přibýt	k5eAaPmAgInS	přibýt
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
antiamerickým	antiamerický	k2eAgMnPc3d1	antiamerický
bláznům	blázen	k1gMnPc3	blázen
<g/>
.	.	kIx.	.
</s>
<s>
Nesouhlasím	souhlasit	k5eNaImIp1nS	souhlasit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
americká	americký	k2eAgNnPc1d1	americké
vojska	vojsko	k1gNnPc1	vojsko
označovala	označovat	k5eAaImAgNnP	označovat
jako	jako	k8xC	jako
okupační	okupační	k2eAgFnSc1d1	okupační
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
prostého	prostý	k2eAgInSc2d1	prostý
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
minulém	minulý	k2eAgNnSc6d1	Minulé
století	století	k1gNnSc6	století
jsme	být	k5eAaImIp1nP	být
okupaci	okupace	k1gFnSc4	okupace
zažili	zažít	k5eAaPmAgMnP	zažít
již	již	k6eAd1	již
dvakrát	dvakrát	k6eAd1	dvakrát
[	[	kIx(	[
<g/>
1939	[number]	k4	1939
a	a	k8xC	a
1968	[number]	k4	1968
<g/>
]	]	kIx)	]
a	a	k8xC	a
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
vypadá	vypadat	k5eAaImIp3nS	vypadat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Zeman	Zeman	k1gMnSc1	Zeman
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
si	se	k3xPyFc3	se
v	v	k7c6	v
amerických	americký	k2eAgFnPc6d1	americká
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
přál	přát	k5eAaImAgMnS	přát
vítězství	vítězství	k1gNnSc4	vítězství
republikánského	republikánský	k2eAgMnSc2d1	republikánský
kandidáta	kandidát	k1gMnSc2	kandidát
Donalda	Donald	k1gMnSc2	Donald
Trumpa	Trump	k1gMnSc2	Trump
<g/>
.	.	kIx.	.
</s>
<s>
Amerického	americký	k2eAgMnSc2d1	americký
prezidenta	prezident	k1gMnSc2	prezident
Baracka	Baracko	k1gNnSc2	Baracko
Obamu	Obam	k1gInSc2	Obam
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
svojí	svůj	k3xOyFgFnSc7	svůj
politikou	politika	k1gFnSc7	politika
přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
destabilizaci	destabilizace	k1gFnSc3	destabilizace
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
kandidátka	kandidátka	k1gFnSc1	kandidátka
Hillary	Hillara	k1gFnSc2	Hillara
Clintonová	Clintonová	k1gFnSc1	Clintonová
by	by	kYmCp3nS	by
podle	podle	k7c2	podle
Zemana	Zeman	k1gMnSc2	Zeman
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
Obamově	Obamův	k2eAgFnSc6d1	Obamova
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
====	====	k?	====
</s>
</p>
<p>
<s>
Při	při	k7c6	při
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
německým	německý	k2eAgMnSc7d1	německý
prezidentem	prezident	k1gMnSc7	prezident
Frankem-Walterem	Frankem-Walter	k1gMnSc7	Frankem-Walter
Steinmeierem	Steinmeier	k1gMnSc7	Steinmeier
v	v	k7c6	v
září	září	k1gNnSc6	září
2017	[number]	k4	2017
se	se	k3xPyFc4	se
prezidenti	prezident	k1gMnPc1	prezident
neshodli	shodnout	k5eNaPmAgMnP	shodnout
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
uprchlických	uprchlický	k2eAgFnPc2d1	uprchlická
kvót	kvóta	k1gFnPc2	kvóta
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
oba	dva	k4xCgMnPc1	dva
prezidenti	prezident	k1gMnPc1	prezident
chválili	chválit	k5eAaImAgMnP	chválit
současné	současný	k2eAgInPc4d1	současný
česko-německé	českoěmecký	k2eAgInPc4d1	česko-německý
vztahy	vztah	k1gInPc4	vztah
a	a	k8xC	a
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
překonávání	překonávání	k1gNnSc4	překonávání
temných	temný	k2eAgFnPc2d1	temná
kapitol	kapitola	k1gFnPc2	kapitola
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
<g/>
Německá	německý	k2eAgFnSc1d1	německá
kancléřka	kancléřka	k1gFnSc1	kancléřka
Angela	Angela	k1gFnSc1	Angela
Merkelová	Merkelová	k1gFnSc1	Merkelová
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2018	[number]	k4	2018
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
vysídlení	vysídlení	k1gNnSc4	vysídlení
Němců	Němec	k1gMnPc2	Němec
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
"	"	kIx"	"
<g/>
neexistovalo	existovat	k5eNaImAgNnS	existovat
ani	ani	k9	ani
morální	morální	k2eAgNnSc1d1	morální
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
politické	politický	k2eAgNnSc1d1	politické
ospravedlnění	ospravedlnění	k1gNnSc1	ospravedlnění
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Zeman	Zeman	k1gMnSc1	Zeman
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
jejím	její	k3xOp3gInSc7	její
výrokem	výrok	k1gInSc7	výrok
"	"	kIx"	"
<g/>
hluboce	hluboko	k6eAd1	hluboko
nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Zeman	Zeman	k1gMnSc1	Zeman
nevystoupil	vystoupit	k5eNaPmAgMnS	vystoupit
s	s	k7c7	s
projevem	projev	k1gInSc7	projev
ani	ani	k8xC	ani
se	se	k3xPyFc4	se
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
žádné	žádný	k3yNgMnPc4	žádný
<g />
.	.	kIx.	.
</s>
<s>
pietní	pietní	k2eAgFnSc1d1	pietní
akce	akce	k1gFnSc1	akce
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
80.	[number]	k4	80.
výročí	výročí	k1gNnSc2	výročí
německé	německý	k2eAgFnSc2d1	německá
okupace	okupace	k1gFnSc2	okupace
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
15.	[number]	k4	15.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
ČTK	ČTK	kA	ČTK
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Mohu	moct	k5eAaImIp1nS	moct
vám	vy	k3xPp2nPc3	vy
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
a	a	k8xC	a
říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
to	ten	k3xDgNnSc1	ten
těžko	těžko	k6eAd1	těžko
<g/>
,	,	kIx,	,
že	že	k8xS	že
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
už	už	k6eAd1	už
nedalo	dát	k5eNaPmAgNnS	dát
nic	nic	k3yNnSc1	nic
dělat	dělat	k5eAaImF	dělat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
dohoda	dohoda	k1gFnSc1	dohoda
vydala	vydat	k5eAaPmAgFnS	vydat
naše	náš	k3xOp1gNnSc4	náš
pohraniční	pohraniční	k2eAgNnSc4d1	pohraniční
opevnění	opevnění	k1gNnSc4	opevnění
a	a	k8xC	a
demoralizovala	demoralizovat	k5eAaBmAgFnS	demoralizovat
náš	náš	k3xOp1gInSc4	náš
národ	národ	k1gInSc4	národ
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
veškeré	veškerý	k3xTgInPc1	veškerý
výkřiky	výkřik	k1gInPc1	výkřik
o	o	k7c6	o
obraně	obrana	k1gFnSc6	obrana
na	na	k7c6	na
okleštěném	okleštěný	k2eAgNnSc6d1	okleštěné
území	území	k1gNnSc6	území
jsou	být	k5eAaImIp3nP	být
patetické	patetický	k2eAgFnPc1d1	patetická
fráze	fráze	k1gFnPc1	fráze
a	a	k8xC	a
nic	nic	k3yNnSc1	nic
víc	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
považuji	považovat	k5eAaImIp1nS	považovat
za	za	k7c4	za
nejdůležitější	důležitý	k2eAgNnSc4d3	nejdůležitější
a	a	k8xC	a
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
nerado	nerad	k2eAgNnSc1d1	nerado
připomíná	připomínat	k5eAaImIp3nS	připomínat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zrada	zrada	k1gFnSc1	zrada
našich	náš	k3xOp1gMnPc2	náš
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Brutální	brutální	k2eAgFnSc1d1	brutální
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
hloupá	hloupý	k2eAgFnSc1d1	hloupá
zrada	zrada	k1gFnSc1	zrada
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
2.	[number]	k4	2.
dubna	duben	k1gInSc2	duben
2019	[number]	k4	2019
prezident	prezident	k1gMnSc1	prezident
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Vídeň	Vídeň	k1gFnSc4	Vídeň
a	a	k8xC	a
setkal	setkat	k5eAaPmAgMnS	setkat
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
s	s	k7c7	s
tamními	tamní	k2eAgMnPc7d1	tamní
krajany	krajan	k1gMnPc7	krajan
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
projevu	projev	k1gInSc6	projev
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
nařkl	nařknout	k5eAaPmAgInS	nařknout
bývalého	bývalý	k2eAgMnSc4d1	bývalý
starostu	starosta	k1gMnSc4	starosta
Vídně	Vídeň	k1gFnSc2	Vídeň
Michaela	Michael	k1gMnSc4	Michael
Häupla	Häupla	k1gMnSc4	Häupla
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c4	po
celé	celý	k2eAgNnSc4d1	celé
období	období	k1gNnSc4	období
jeho	jeho	k3xOp3gFnSc2	jeho
vedení	vedení	k1gNnSc1	vedení
město	město	k1gNnSc4	město
nedalo	dát	k5eNaPmAgNnS	dát
místní	místní	k2eAgFnSc3d1	místní
českojazyčné	českojazyčný	k2eAgFnSc3d1	českojazyčná
Komenského	Komenského	k2eAgFnSc3d1	Komenského
škole	škola	k1gFnSc3	škola
ani	ani	k8xC	ani
šilink	šilink	k1gInSc1	šilink
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
prezidentovu	prezidentův	k2eAgNnSc3d1	prezidentovo
tvrzení	tvrzení	k1gNnSc3	tvrzení
ohradil	ohradit	k5eAaPmAgMnS	ohradit
ředitel	ředitel	k1gMnSc1	ředitel
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
česká	český	k2eAgNnPc1d1	české
média	médium	k1gNnPc1	médium
pak	pak	k6eAd1	pak
Zemanův	Zemanův	k2eAgInSc4d1	Zemanův
výrok	výrok	k1gInSc4	výrok
rozporovala	rozporovat	k5eAaImAgFnS	rozporovat
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
projev	projev	k1gInSc4	projev
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
také	také	k9	také
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Petříček	Petříček	k1gMnSc1	Petříček
a	a	k8xC	a
část	část	k1gFnSc1	část
české	český	k2eAgFnSc2d1	Česká
parlamentní	parlamentní	k2eAgFnSc2d1	parlamentní
opozice	opozice	k1gFnSc2	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Mluvčí	mluvčí	k1gMnSc1	mluvčí
Ovčáček	ovčáček	k1gMnSc1	ovčáček
prezidentovo	prezidentův	k2eAgNnSc4d1	prezidentovo
tvrzení	tvrzení	k1gNnSc4	tvrzení
zdůvodnil	zdůvodnit	k5eAaPmAgMnS	zdůvodnit
podklady	podklad	k1gInPc4	podklad
dodanými	dodaná	k1gFnPc7	dodaná
českou	český	k2eAgFnSc7d1	Česká
velvyslankyní	velvyslankyně	k1gFnSc7	velvyslankyně
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
Ivanou	Ivana	k1gFnSc7	Ivana
Červenkovou	Červenková	k1gFnSc7	Červenková
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
Vídeň	Vídeň	k1gFnSc1	Vídeň
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
jednorázovou	jednorázový	k2eAgFnSc4d1	jednorázová
dotaci	dotace	k1gFnSc4	dotace
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
jednoho	jeden	k4xCgInSc2	jeden
milionu	milion	k4xCgInSc2	milion
eur	euro	k1gNnPc2	euro
na	na	k7c4	na
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
město	město	k1gNnSc1	město
v	v	k7c6	v
době	doba	k1gFnSc6	doba
prezidentovy	prezidentův	k2eAgFnSc2d1	prezidentova
návštěvy	návštěva	k1gFnSc2	návštěva
finančně	finančně	k6eAd1	finančně
podporovalo	podporovat	k5eAaImAgNnS	podporovat
provoz	provoz	k1gInSc4	provoz
české	český	k2eAgFnSc2d1	Česká
mateřské	mateřský	k2eAgFnSc2d1	mateřská
školky	školka	k1gFnSc2	školka
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
ovšem	ovšem	k9	ovšem
podklady	podklad	k1gInPc7	podklad
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
dostal	dostat	k5eAaPmAgInS	dostat
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
nějak	nějak	k6eAd1	nějak
nepochopil	pochopit	k5eNaPmAgMnS	pochopit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
4.	[number]	k4	4.
dubna	duben	k1gInSc2	duben
Zeman	Zeman	k1gMnSc1	Zeman
vzkázal	vzkázat	k5eAaPmAgMnS	vzkázat
Häuplovi	Häupl	k1gMnSc3	Häupl
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
nového	nový	k2eAgMnSc2d1	nový
vídeňského	vídeňský	k2eAgMnSc2d1	vídeňský
starosty	starosta	k1gMnSc2	starosta
svou	svůj	k3xOyFgFnSc4	svůj
omluvu	omluva	k1gFnSc4	omluva
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
druhou	druhý	k4xOgFnSc4	druhý
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
30leté	30letý	k2eAgFnSc6d1	30letý
politické	politický	k2eAgFnSc6d1	politická
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Rakušané	Rakušan	k1gMnPc1	Rakušan
v	v	k7c6	v
diskuzi	diskuze	k1gFnSc6	diskuze
na	na	k7c6	na
portálu	portál	k1gInSc6	portál
největšího	veliký	k2eAgInSc2d3	veliký
rakouského	rakouský	k2eAgInSc2d1	rakouský
deníku	deník	k1gInSc2	deník
Der	drát	k5eAaImRp2nS	drát
Standard	standard	k1gInSc4	standard
drsně	drsně	k6eAd1	drsně
zkritizovali	zkritizovat	k5eAaPmAgMnP	zkritizovat
Miloše	Miloš	k1gMnSc4	Miloš
Zemana	Zeman	k1gMnSc4	Zeman
za	za	k7c4	za
jeho	jeho	k3xOp3gNnSc4	jeho
chování	chování	k1gNnSc4	chování
během	během	k7c2	během
státní	státní	k2eAgFnSc2d1	státní
návštěvy	návštěva	k1gFnSc2	návštěva
jejich	jejich	k3xOp3gFnSc2	jejich
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2014	[number]	k4	2014
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
zpřísnila	zpřísnit	k5eAaPmAgFnS	zpřísnit
protiruské	protiruský	k2eAgFnPc4d1	protiruská
sankce	sankce	k1gFnPc4	sankce
a	a	k8xC	a
sankce	sankce	k1gFnPc4	sankce
odsouhlasila	odsouhlasit	k5eAaPmAgFnS	odsouhlasit
i	i	k9	i
vláda	vláda	k1gFnSc1	vláda
premiéra	premiér	k1gMnSc2	premiér
Sobotky	Sobotka	k1gFnSc2	Sobotka
<g/>
,	,	kIx,	,
Zeman	Zeman	k1gMnSc1	Zeman
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ČR	ČR	kA	ČR
by	by	kYmCp3nS	by
neměla	mít	k5eNaImAgFnS	mít
přerušovat	přerušovat	k5eAaImF	přerušovat
dobré	dobrý	k2eAgInPc4d1	dobrý
obchodní	obchodní	k2eAgInPc4d1	obchodní
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2015	[number]	k4	2015
Zeman	Zeman	k1gMnSc1	Zeman
komentoval	komentovat	k5eAaBmAgMnS	komentovat
protesty	protest	k1gInPc4	protest
proti	proti	k7c3	proti
průjezdu	průjezd	k1gInSc3	průjezd
amerického	americký	k2eAgInSc2d1	americký
konvoje	konvoj	k1gInSc2	konvoj
přes	přes	k7c4	přes
ČR	ČR	kA	ČR
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
měsících	měsíc	k1gInPc6	měsíc
bojuji	bojovat	k5eAaImIp1nS	bojovat
proti	proti	k7c3	proti
antiruským	antiruský	k2eAgMnPc3d1	antiruský
bláznům	blázen	k1gMnPc3	blázen
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
přibyl	přibýt	k5eAaPmAgInS	přibýt
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
antiamerickým	antiamerický	k2eAgMnPc3d1	antiamerický
bláznům	blázen	k1gMnPc3	blázen
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
září	září	k1gNnSc2	září
2014	[number]	k4	2014
navštívil	navštívit	k5eAaPmAgMnS	navštívit
konferenci	konference	k1gFnSc4	konference
Dialog	dialog	k1gInSc4	dialog
civilizací	civilizace	k1gFnSc7	civilizace
na	na	k7c6	na
Rhodu	Rhodos	k1gInSc6	Rhodos
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
pořádal	pořádat	k5eAaImAgMnS	pořádat
ruský	ruský	k2eAgMnSc1d1	ruský
oligarcha	oligarcha	k1gMnSc1	oligarcha
Vladimir	Vladimir	k1gMnSc1	Vladimir
Jakunin	Jakunin	k2eAgMnSc1d1	Jakunin
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
projevu	projev	k1gInSc6	projev
opět	opět	k6eAd1	opět
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
sankce	sankce	k1gFnSc2	sankce
uvalené	uvalený	k2eAgFnSc2d1	uvalená
na	na	k7c4	na
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
"	"	kIx"	"
<g/>
brání	bránit	k5eAaImIp3nS	bránit
dialogu	dialog	k1gInSc2	dialog
civilizací	civilizace	k1gFnPc2	civilizace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
projevu	projev	k1gInSc2	projev
označil	označit	k5eAaPmAgInS	označit
islámský	islámský	k2eAgInSc1d1	islámský
stát	stát	k5eAaPmF	stát
za	za	k7c4	za
"	"	kIx"	"
<g/>
rakovinu	rakovina	k1gFnSc4	rakovina
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
o	o	k7c6	o
válce	válka	k1gFnSc6	válka
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
se	se	k3xPyFc4	se
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
jako	jako	k9	jako
o	o	k7c6	o
"	"	kIx"	"
<g/>
chřipce	chřipka	k1gFnSc6	chřipka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Zeman	Zeman	k1gMnSc1	Zeman
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2015	[number]	k4	2015
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
slavnostní	slavnostní	k2eAgFnSc6d1	slavnostní
vojenské	vojenský	k2eAgFnSc6d1	vojenská
přehlídce	přehlídka	k1gFnSc6	přehlídka
v	v	k7c6	v
ruském	ruský	k2eAgNnSc6d1	ruské
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Moskvě	Moskva	k1gFnSc6	Moskva
k	k	k7c3	k
70.	[number]	k4	70.
výročí	výročí	k1gNnSc3	výročí
konce	konec	k1gInSc2	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgMnS	být
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
pravicovou	pravicový	k2eAgFnSc7d1	pravicová
opozicí	opozice	k1gFnSc7	opozice
<g/>
,	,	kIx,	,
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
jej	on	k3xPp3gMnSc4	on
vyzvala	vyzvat	k5eAaPmAgFnS	vyzvat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
své	svůj	k3xOyFgNnSc4	svůj
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
ještě	ještě	k9	ještě
rozmyslel	rozmyslet	k5eAaPmAgMnS	rozmyslet
<g/>
.	.	kIx.	.
</s>
<s>
Poslankyně	poslankyně	k1gFnSc1	poslankyně
Miroslava	Miroslava	k1gFnSc1	Miroslava
Němcová	Němcová	k1gFnSc1	Němcová
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vláda	vláda	k1gFnSc1	vláda
cestu	cesta	k1gFnSc4	cesta
neproplatila	proplatit	k5eNaPmAgFnS	proplatit
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
proplacení	proplacení	k1gNnSc3	proplacení
nákladů	náklad	k1gInPc2	náklad
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
schválí	schválit	k5eAaPmIp3nS	schválit
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
prezidentem	prezident	k1gMnSc7	prezident
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
účast	účast	k1gFnSc4	účast
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
<g/>
,	,	kIx,	,
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
v	v	k7c6	v
březnovém	březnový	k2eAgInSc6d1	březnový
Prezidentském	prezidentský	k2eAgInSc6d1	prezidentský
Pressklubu	Pressklub	k1gInSc6	Pressklub
na	na	k7c6	na
rádiu	rádius	k1gInSc6	rádius
Frekvence	frekvence	k1gFnSc2	frekvence
1	[number]	k4	1
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
bude	být	k5eAaImBp3nS	být
"	"	kIx"	"
<g/>
výrazem	výraz	k1gInSc7	výraz
vděčnosti	vděčnost	k1gFnSc2	vděčnost
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zemi	zem	k1gFnSc6	zem
nemusíme	muset	k5eNaImIp1nP	muset
mluvit	mluvit	k5eAaImF	mluvit
německy	německy	k6eAd1	německy
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
pojede	pojet	k5eAaPmIp3nS	pojet
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
úctu	úcta	k1gFnSc4	úcta
150	[number]	k4	150
tisícům	tisíc	k4xCgInPc3	tisíc
sovětských	sovětský	k2eAgMnPc2d1	sovětský
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
padli	padnout	k5eAaImAgMnP	padnout
na	na	k7c6	na
československém	československý	k2eAgNnSc6d1	Československé
území	území	k1gNnSc6	území
během	během	k7c2	během
osvobozování	osvobozování	k1gNnSc2	osvobozování
od	od	k7c2	od
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
<g/>
.	.	kIx.	.
</s>
<s>
Předeslal	předeslat	k5eAaPmAgMnS	předeslat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
nepodá	podat	k5eNaPmIp3nS	podat
ruku	ruka	k1gFnSc4	ruka
se	s	k7c7	s
severokorejským	severokorejský	k2eAgMnSc7d1	severokorejský
diktátorem	diktátor	k1gMnSc7	diktátor
Kim	Kim	k1gMnSc7	Kim
Čong-unem	Čongn	k1gMnSc7	Čong-un
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
oslav	oslava	k1gFnPc2	oslava
také	také	k6eAd1	také
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
svou	svůj	k3xOyFgFnSc4	svůj
účast	účast	k1gFnSc4	účast
odvolal	odvolat	k5eAaPmAgMnS	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
ostře	ostro	k6eAd1	ostro
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
výroky	výrok	k1gInPc1	výrok
amerického	americký	k2eAgMnSc2d1	americký
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
Andrewa	Andrewus	k1gMnSc2	Andrewus
Schapira	Schapir	k1gMnSc2	Schapir
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jeho	jeho	k3xOp3gFnSc4	jeho
plánovanou	plánovaný	k2eAgFnSc4d1	plánovaná
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
oslavě	oslava	k1gFnSc6	oslava
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
prekérní	prekérní	k2eAgNnSc4d1	prekérní
<g/>
"	"	kIx"	"
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
bojkotu	bojkot	k1gInSc3	bojkot
události	událost	k1gFnSc2	událost
většinou	většinou	k6eAd1	většinou
evropských	evropský	k2eAgMnPc2d1	evropský
státníků	státník	k1gMnPc2	státník
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
prohlášení	prohlášení	k1gNnSc6	prohlášení
má	mít	k5eAaImIp3nS	mít
Schapiro	Schapiro	k1gNnSc1	Schapiro
dveře	dveře	k1gFnPc4	dveře
na	na	k7c4	na
Hrad	hrad	k1gInSc4	hrad
zavřené	zavřený	k2eAgFnPc1d1	zavřená
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
vyjádření	vyjádření	k1gNnSc4	vyjádření
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
a	a	k8xC	a
komentovala	komentovat	k5eAaBmAgFnS	komentovat
i	i	k9	i
četná	četný	k2eAgNnPc4d1	četné
zahraniční	zahraniční	k2eAgNnPc4d1	zahraniční
média	médium	k1gNnPc4	médium
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
USA	USA	kA	USA
se	se	k3xPyFc4	se
postavily	postavit	k5eAaPmAgInP	postavit
za	za	k7c2	za
Schapira	Schapir	k1gInSc2	Schapir
<g/>
,	,	kIx,	,
ruský	ruský	k2eAgMnSc1d1	ruský
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Sergej	Sergej	k1gMnSc1	Sergej
Lavrov	Lavrov	k1gInSc4	Lavrov
ocenil	ocenit	k5eAaPmAgMnS	ocenit
Zemanovu	Zemanův	k2eAgFnSc4d1	Zemanova
vlastní	vlastní	k2eAgFnSc4d1	vlastní
důstojnost	důstojnost	k1gFnSc4	důstojnost
a	a	k8xC	a
hrdost	hrdost	k1gFnSc4	hrdost
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
místo	místo	k7c2	místo
vojenské	vojenský	k2eAgFnSc2d1	vojenská
přehlídky	přehlídka	k1gFnSc2	přehlídka
se	se	k3xPyFc4	se
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
sejde	sejít	k5eAaPmIp3nS	sejít
k	k	k7c3	k
rozhovorům	rozhovor	k1gInPc3	rozhovor
se	se	k3xPyFc4	se
slovenským	slovenský	k2eAgMnSc7d1	slovenský
premiérem	premiér	k1gMnSc7	premiér
Robertem	Robert	k1gMnSc7	Robert
Ficem	Fic	k1gMnSc7	Fic
<g/>
,	,	kIx,	,
jednali	jednat	k5eAaImAgMnP	jednat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
o	o	k7c4	o
propojení	propojení	k1gNnSc4	propojení
řek	řeka	k1gFnPc2	řeka
Váh	Váh	k1gInSc1	Váh
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
posléze	posléze	k6eAd1	posléze
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ficem	Fic	k1gMnSc7	Fic
a	a	k8xC	a
ostatními	ostatní	k2eAgMnPc7d1	ostatní
státníky	státník	k1gMnPc7	státník
položil	položit	k5eAaPmAgMnS	položit
9.	[number]	k4	9.
května	květen	k1gInSc2	květen
věnec	věnec	k1gInSc1	věnec
k	k	k7c3	k
památníku	památník	k1gInSc3	památník
Neznámého	známý	k2eNgMnSc2d1	neznámý
vojína	vojín	k1gMnSc2	vojín
u	u	k7c2	u
moskevského	moskevský	k2eAgInSc2d1	moskevský
Kremlu	Kreml	k1gInSc2	Kreml
k	k	k7c3	k
uctění	uctění	k1gNnSc3	uctění
sovětských	sovětský	k2eAgMnPc2d1	sovětský
vojáků	voják	k1gMnPc2	voják
padlých	padlý	k1gMnPc2	padlý
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
<g/>
Americký	americký	k2eAgInSc1d1	americký
list	list	k1gInSc1	list
Washington	Washington	k1gInSc1	Washington
Post	post	k1gInSc1	post
označil	označit	k5eAaPmAgMnS	označit
Zemana	Zeman	k1gMnSc4	Zeman
za	za	k7c4	za
jeho	jeho	k3xOp3gInPc4	jeho
proruské	proruský	k2eAgInPc4d1	proruský
postoje	postoj	k1gInPc4	postoj
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
faktického	faktický	k2eAgMnSc2d1	faktický
mluvčího	mluvčí	k1gMnSc2	mluvčí
Kremlu	Kreml	k1gInSc2	Kreml
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vlivný	vlivný	k2eAgMnSc1d1	vlivný
senátor	senátor	k1gMnSc1	senátor
John	John	k1gMnSc1	John
McCain	McCain	k1gMnSc1	McCain
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
znepokojení	znepokojení	k1gNnSc4	znepokojení
ze	z	k7c2	z
Zemanova	Zemanův	k2eAgInSc2d1	Zemanův
postoje	postoj	k1gInSc2	postoj
k	k	k7c3	k
Rusku	Rusko	k1gNnSc3	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
diplomata	diplomat	k1gMnSc2	diplomat
Petra	Petr	k1gMnSc2	Petr
Koláře	Kolář	k1gMnSc2	Kolář
se	se	k3xPyFc4	se
Zeman	Zeman	k1gMnSc1	Zeman
"	"	kIx"	"
<g/>
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
velmi	velmi	k6eAd1	velmi
užitečným	užitečný	k2eAgInSc7d1	užitečný
nástrojem	nástroj	k1gInSc7	nástroj
ruské	ruský	k2eAgFnSc2d1	ruská
propagandy	propaganda	k1gFnSc2	propaganda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
šíří	šířit	k5eAaImIp3nS	šířit
prokremelské	prokremelský	k2eAgFnPc4d1	prokremelská
dezinformace	dezinformace	k1gFnPc4	dezinformace
-	-	kIx~	-
např.	např.	kA	např.
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2014	[number]	k4	2014
český	český	k2eAgMnSc1d1	český
prezident	prezident	k1gMnSc1	prezident
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
čínskou	čínský	k2eAgFnSc4d1	čínská
státní	státní	k2eAgFnSc4d1	státní
televizi	televize	k1gFnSc4	televize
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
doslovnou	doslovný	k2eAgFnSc4d1	doslovná
lež	lež	k1gFnSc4	lež
vyrobenou	vyrobený	k2eAgFnSc4d1	vyrobená
Kremlem	Kreml	k1gInSc7	Kreml
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
přítomnosti	přítomnost	k1gFnSc2	přítomnost
ruských	ruský	k2eAgMnPc2d1	ruský
vojáků	voják	k1gMnPc2	voják
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
mu	on	k3xPp3gMnSc3	on
švédský	švédský	k2eAgMnSc1d1	švédský
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Karl	Karl	k1gMnSc1	Karl
Bildt	Bildt	k1gMnSc1	Bildt
doporučil	doporučit	k5eAaPmAgMnS	doporučit
věnovat	věnovat	k5eAaImF	věnovat
větší	veliký	k2eAgFnSc4d2	veliký
pozornost	pozornost	k1gFnSc4	pozornost
například	například	k6eAd1	například
informacím	informace	k1gFnPc3	informace
české	český	k2eAgFnSc2d1	Česká
zpravodajské	zpravodajský	k2eAgFnPc4d1	zpravodajská
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
tvrzení	tvrzení	k1gNnSc2	tvrzení
v	v	k7c6	v
The	The	k1gFnSc6	The
American	American	k1gMnSc1	American
Interest	Interest	k1gMnSc1	Interest
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
Zemanova	Zemanův	k2eAgFnSc1d1	Zemanova
hradní	hradní	k2eAgFnSc1d1	hradní
kancelář	kancelář	k1gFnSc1	kancelář
údajně	údajně	k6eAd1	údajně
vydírá	vydírat	k5eAaImIp3nS	vydírat
premiéra	premiéra	k1gFnSc1	premiéra
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
za	za	k7c4	za
příslib	příslib	k1gInSc4	příslib
podpory	podpora	k1gFnSc2	podpora
během	během	k7c2	během
vládní	vládní	k2eAgFnSc2d1	vládní
<g />
.	.	kIx.	.
</s>
<s>
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
vynutit	vynutit	k5eAaPmF	vynutit
si	se	k3xPyFc3	se
jeho	on	k3xPp3gInSc4	on
souhlas	souhlas	k1gInSc4	souhlas
aby	aby	kYmCp3nS	aby
ruský	ruský	k2eAgInSc1d1	ruský
Rosatom	Rosatom	k1gInSc1	Rosatom
získal	získat	k5eAaPmAgInS	získat
kontrakt	kontrakt	k1gInSc4	kontrakt
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
150-200	[number]	k4	150-200
miliard	miliarda	k4xCgFnPc2	miliarda
na	na	k7c4	na
dostavbu	dostavba	k1gFnSc4	dostavba
jaderných	jaderný	k2eAgInPc2d1	jaderný
bloků	blok	k1gInPc2	blok
v	v	k7c6	v
Dukovanech	Dukovany	k1gInPc6	Dukovany
<g/>
.	.	kIx.	.
<g/>
Počátkem	počátkem	k7c2	počátkem
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
Zeman	Zeman	k1gMnSc1	Zeman
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
revizionistický	revizionistický	k2eAgInSc4d1	revizionistický
dokument	dokument	k1gInSc4	dokument
ruské	ruský	k2eAgFnSc2d1	ruská
televize	televize	k1gFnSc2	televize
Rossija	Rossij	k1gInSc2	Rossij
1	[number]	k4	1
o	o	k7c6	o
sovětské	sovětský	k2eAgFnSc6d1	sovětská
invazi	invaze	k1gFnSc6	invaze
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
invazi	invaze	k1gFnSc4	invaze
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
zločin	zločin	k1gInSc4	zločin
a	a	k8xC	a
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
ruská	ruský	k2eAgFnSc1d1	ruská
televize	televize	k1gFnSc1	televize
lže	lhát	k5eAaImIp3nS	lhát
a	a	k8xC	a
žádný	žádný	k3yNgInSc4	žádný
jiný	jiný	k2eAgInSc4d1	jiný
komentář	komentář	k1gInSc4	komentář
<g/>
,	,	kIx,	,
než	než	k8xS	než
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
novinářská	novinářský	k2eAgFnSc1d1	novinářská
lež	lež	k1gFnSc1	lež
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
říci	říct	k5eAaPmF	říct
nedá	dát	k5eNaPmIp3nS	dát
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Čínou	Čína	k1gFnSc7	Čína
====	====	k?	====
</s>
</p>
<p>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
se	se	k3xPyFc4	se
zasazuje	zasazovat	k5eAaImIp3nS	zasazovat
za	za	k7c4	za
otevřenější	otevřený	k2eAgFnSc4d2	otevřenější
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
Čínou	Čína	k1gFnSc7	Čína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2014	[number]	k4	2014
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
na	na	k7c6	na
státní	státní	k2eAgFnSc6d1	státní
návštěvě	návštěva	k1gFnSc6	návštěva
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
respektuje	respektovat	k5eAaImIp3nS	respektovat
územní	územní	k2eAgFnSc1d1	územní
integritu	integrita	k1gFnSc4	integrita
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
neuznává	uznávat	k5eNaImIp3nS	uznávat
tibetskou	tibetský	k2eAgFnSc4d1	tibetská
exilovou	exilový	k2eAgFnSc4d1	exilová
vládu	vláda	k1gFnSc4	vláda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
si	se	k3xPyFc3	se
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
kritiku	kritika	k1gFnSc4	kritika
od	od	k7c2	od
některých	některý	k3yIgMnPc2	některý
pravicových	pravicový	k2eAgMnPc2d1	pravicový
politiků	politik	k1gMnPc2	politik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
3.	[number]	k4	3.
září	září	k1gNnSc2	září
2015	[number]	k4	2015
se	se	k3xPyFc4	se
Zeman	Zeman	k1gMnSc1	Zeman
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
vojenské	vojenský	k2eAgFnSc2d1	vojenská
přehlídky	přehlídka	k1gFnSc2	přehlídka
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
70.	[number]	k4	70.
výročí	výročí	k1gNnSc2	výročí
konce	konec	k1gInSc2	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Tichomoří	Tichomoří	k1gNnSc6	Tichomoří
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2017	[number]	k4	2017
navštívil	navštívit	k5eAaPmAgInS	navštívit
památník	památník	k1gInSc1	památník
obětí	oběť	k1gFnPc2	oběť
nankingského	nankingský	k2eAgInSc2d1	nankingský
masakru	masakr	k1gInSc2	masakr
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgNnSc6	který
vojáci	voják	k1gMnPc1	voják
Japonského	japonský	k2eAgNnSc2d1	Japonské
císařství	císařství	k1gNnSc2	císařství
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
zavraždili	zavraždit	k5eAaPmAgMnP	zavraždit
okolo	okolo	k7c2	okolo
300 000	[number]	k4	300 000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
začala	začít	k5eAaPmAgFnS	začít
fakticky	fakticky	k6eAd1	fakticky
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
čínsko-japonské	čínskoaponský	k2eAgFnSc2d1	čínsko-japonská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
třídenní	třídenní	k2eAgFnSc2d1	třídenní
návštěvy	návštěva	k1gFnSc2	návštěva
čínského	čínský	k2eAgMnSc2d1	čínský
prezidenta	prezident	k1gMnSc2	prezident
Si	se	k3xPyFc3	se
Ťin-pchinga	Ťinchinga	k1gFnSc1	Ťin-pchinga
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2016	[number]	k4	2016
prezident	prezident	k1gMnSc1	prezident
Zeman	Zeman	k1gMnSc1	Zeman
přivítal	přivítat	k5eAaPmAgMnS	přivítat
Si	se	k3xPyFc3	se
Ťin-pchinga	Ťinchinga	k1gFnSc1	Ťin-pchinga
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Chtěl	chtít	k5eAaImAgMnS	chtít
bych	by	kYmCp1nS	by
se	se	k3xPyFc4	se
vám	vy	k3xPp2nPc3	vy
omluvit	omluvit	k5eAaPmF	omluvit
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemám	mít	k5eNaImIp1nS	mít
funkční	funkční	k2eAgInSc1d1	funkční
zlatý	zlatý	k2eAgInSc1d1	zlatý
kočár	kočár	k1gInSc1	kočár
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
bych	by	kYmCp1nS	by
vás	vy	k3xPp2nPc4	vy
svezl	svézt	k5eAaPmAgMnS	svézt
tak	tak	k8xS	tak
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
vás	vy	k3xPp2nPc4	vy
svezla	svézt	k5eAaPmAgFnS	svézt
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
bych	by	kYmCp1nS	by
se	se	k3xPyFc4	se
omluvit	omluvit	k5eAaPmF	omluvit
i	i	k9	i
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nevlastním	vlastnit	k5eNaImIp1nS	vlastnit
Bílý	bílý	k2eAgInSc1d1	bílý
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jste	být	k5eAaImIp2nP	být
se	se	k3xPyFc4	se
nedávno	nedávno	k6eAd1	nedávno
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
Obamou	Obama	k1gMnSc7	Obama
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
bych	by	kYmCp1nS	by
jenom	jenom	k9	jenom
poznamenat	poznamenat	k5eAaPmF	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
a	a	k8xC	a
zlaté	zlatý	k2eAgNnSc1d1	Zlaté
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
zlatý	zlatý	k2eAgInSc1d1	zlatý
kočár	kočár	k1gInSc1	kočár
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Zeman	Zeman	k1gMnSc1	Zeman
prezidenta	prezident	k1gMnSc4	prezident
Si	se	k3xPyFc3	se
Ťin-pchinga	Ťinching	k1gMnSc4	Ťin-pching
pozval	pozvat	k5eAaPmAgMnS	pozvat
do	do	k7c2	do
letního	letní	k2eAgNnSc2d1	letní
sídla	sídlo	k1gNnSc2	sídlo
českých	český	k2eAgMnPc2d1	český
prezidentů	prezident	k1gMnPc2	prezident
v	v	k7c6	v
Lánech	lán	k1gInPc6	lán
<g/>
.	.	kIx.	.
<g/>
Zeman	Zeman	k1gMnSc1	Zeman
podporuje	podporovat	k5eAaImIp3nS	podporovat
působení	působení	k1gNnSc4	působení
čínské	čínský	k2eAgFnSc2d1	čínská
společnosti	společnost	k1gFnSc2	společnost
CEFC	CEFC	kA	CEFC
China	China	k1gFnSc1	China
Energy	Energ	k1gInPc4	Energ
Company	Compana	k1gFnSc2	Compana
Limited	limited	k2eAgInPc4d1	limited
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
podíl	podíl	k1gInSc4	podíl
v	v	k7c6	v
několika	několik	k4yIc6	několik
významných	významný	k2eAgFnPc6d1	významná
českých	český	k2eAgFnPc6d1	Česká
firmách	firma	k1gFnPc6	firma
<g/>
.	.	kIx.	.
</s>
<s>
CEFC	CEFC	kA	CEFC
drží	držet	k5eAaImIp3nS	držet
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
podíly	podíl	k1gInPc4	podíl
ve	v	k7c6	v
firmách	firma	k1gFnPc6	firma
Lobkowicz	Lobkowicz	k1gMnSc1	Lobkowicz
Group	Group	k1gMnSc1	Group
<g/>
,	,	kIx,	,
Travel	Travel	k1gMnSc1	Travel
Service	Service	k1gFnSc2	Service
<g/>
,	,	kIx,	,
investovala	investovat	k5eAaBmAgFnS	investovat
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
Médea	Médea	k1gFnSc1	Médea
Group	Group	k1gMnSc1	Group
a	a	k8xC	a
Empresa	Empresa	k1gFnSc1	Empresa
Media	medium	k1gNnSc2	medium
či	či	k8xC	či
do	do	k7c2	do
realit	realita	k1gFnPc2	realita
a	a	k8xC	a
vlastní	vlastní	k2eAgFnSc4d1	vlastní
fotbalovou	fotbalový	k2eAgFnSc4d1	fotbalová
Slavii	slavie	k1gFnSc4	slavie
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
prezidentových	prezidentův	k2eAgMnPc2d1	prezidentův
oponentů	oponent	k1gMnPc2	oponent
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
přes	přes	k7c4	přes
uvedené	uvedený	k2eAgFnPc4d1	uvedená
Zemanovy	Zemanův	k2eAgFnPc4d1	Zemanova
snahy	snaha	k1gFnPc4	snaha
o	o	k7c4	o
podporu	podpora	k1gFnSc4	podpora
hospodářských	hospodářský	k2eAgInPc2d1	hospodářský
vztahů	vztah	k1gInPc2	vztah
s	s	k7c7	s
Čínou	Čína	k1gFnSc7	Čína
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
český	český	k2eAgInSc1d1	český
export	export	k1gInSc1	export
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
vývozem	vývoz	k1gInSc7	vývoz
do	do	k7c2	do
EU	EU	kA	EU
jen	jen	k9	jen
o	o	k7c4	o
0,1	[number]	k4	0,1
procenta	procento	k1gNnSc2	procento
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
<g/>
Strmý	strmý	k2eAgInSc1d1	strmý
vzestup	vzestup	k1gInSc1	vzestup
a	a	k8xC	a
následný	následný	k2eAgInSc1d1	následný
prudký	prudký	k2eAgInSc1d1	prudký
pád	pád	k1gInSc1	pád
společnosti	společnost	k1gFnSc2	společnost
CEFC	CEFC	kA	CEFC
(	(	kIx(	(
<g/>
výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
její	její	k3xOp3gNnSc1	její
zestátnění	zestátnění	k1gNnSc1	zestátnění
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
dostaly	dostat	k5eAaPmAgFnP	dostat
na	na	k7c4	na
titulní	titulní	k2eAgFnPc4d1	titulní
strany	strana	k1gFnPc4	strana
předních	přední	k2eAgNnPc2d1	přední
světových	světový	k2eAgNnPc2d1	světové
médií	médium	k1gNnPc2	médium
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
byly	být	k5eAaImAgFnP	být
tyto	tento	k3xDgFnPc1	tento
zprávy	zpráva	k1gFnPc1	zpráva
doprovázeny	doprovázet	k5eAaImNgFnP	doprovázet
fotografií	fotografia	k1gFnPc2	fotografia
předsedy	předseda	k1gMnSc2	předseda
CEFC	CEFC	kA	CEFC
po	po	k7c6	po
boku	bok	k1gInSc6	bok
českého	český	k2eAgMnSc2d1	český
prezidenta	prezident	k1gMnSc2	prezident
jako	jako	k8xC	jako
ilustrace	ilustrace	k1gFnPc1	ilustrace
Jie	Jie	k1gFnSc2	Jie
Ťien-mingových	Ťieningový	k2eAgFnPc2d1	Ťien-mingový
snah	snaha	k1gFnPc2	snaha
o	o	k7c6	o
vytváření	vytváření	k1gNnSc6	vytváření
vlastního	vlastní	k2eAgInSc2d1	vlastní
image	image	k1gInSc2	image
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Islám	islám	k1gInSc4	islám
a	a	k8xC	a
Blízký	blízký	k2eAgInSc4d1	blízký
východ	východ	k1gInSc4	východ
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
způsobil	způsobit	k5eAaPmAgMnS	způsobit
diplomatický	diplomatický	k2eAgInSc4d1	diplomatický
incident	incident	k1gInSc4	incident
se	s	k7c7	s
Saúdskou	saúdský	k2eAgFnSc7d1	Saúdská
Arábií	Arábie	k1gFnSc7	Arábie
<g/>
,	,	kIx,	,
když	když	k8xS	když
při	při	k7c6	při
oslavě	oslava	k1gFnSc6	oslava
izraelského	izraelský	k2eAgInSc2d1	izraelský
dne	den	k1gInSc2	den
nezávislosti	nezávislost	k1gFnSc2	nezávislost
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2014	[number]	k4	2014
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
násilnými	násilný	k2eAgInPc7d1	násilný
akty	akt	k1gInPc7	akt
jako	jako	k8xC	jako
byl	být	k5eAaImAgInS	být
útok	útok	k1gInSc1	útok
v	v	k7c6	v
bruselském	bruselský	k2eAgNnSc6d1	bruselské
židovském	židovský	k2eAgNnSc6d1	Židovské
muzeu	muzeum	k1gNnSc6	muzeum
stojí	stát	k5eAaImIp3nS	stát
"	"	kIx"	"
<g/>
islámská	islámský	k2eAgFnSc1d1	islámská
ideologie	ideologie	k1gFnSc1	ideologie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
si	se	k3xPyFc3	se
předvolala	předvolat	k5eAaPmAgFnS	předvolat
českého	český	k2eAgMnSc4d1	český
velvyslance	velvyslanec	k1gMnSc4	velvyslanec
v	v	k7c6	v
Rijádu	Rijád	k1gInSc6	Rijád
a	a	k8xC	a
předložila	předložit	k5eAaPmAgFnS	předložit
soupis	soupis	k1gInSc4	soupis
všeho	všecek	k3xTgNnSc2	všecek
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
kdy	kdy	k6eAd1	kdy
Zeman	Zeman	k1gMnSc1	Zeman
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
islám	islám	k1gInSc1	islám
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
řekl	říct	k5eAaPmAgMnS	říct
<g/>
.	.	kIx.	.
<g/>
Zeman	Zeman	k1gMnSc1	Zeman
podporuje	podporovat	k5eAaImIp3nS	podporovat
Izrael	Izrael	k1gInSc4	Izrael
a	a	k8xC	a
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2015	[number]	k4	2015
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
AIPAC	AIPAC	kA	AIPAC
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
prvorepublikové	prvorepublikový	k2eAgNnSc1d1	prvorepublikové
Československo	Československo	k1gNnSc1	Československo
bylo	být	k5eAaImAgNnS	být
"	"	kIx"	"
<g/>
jediným	jediný	k2eAgInSc7d1	jediný
ostrovem	ostrov	k1gInSc7	ostrov
demokracie	demokracie	k1gFnSc2	demokracie
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
"	"	kIx"	"
a	a	k8xC	a
Izrael	Izrael	k1gInSc1	Izrael
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
v	v	k7c6	v
podobném	podobný	k2eAgNnSc6d1	podobné
postavení	postavení	k1gNnSc6	postavení
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
mezi	mezi	k7c7	mezi
Českem	Česko	k1gNnSc7	Česko
a	a	k8xC	a
Izraelem	Izrael	k1gInSc7	Izrael
musí	muset	k5eAaImIp3nS	muset
existovat	existovat	k5eAaImF	existovat
solidarita	solidarita	k1gFnSc1	solidarita
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
státní	státní	k2eAgFnSc2d1	státní
návštěvy	návštěva	k1gFnSc2	návštěva
Jordánska	Jordánsko	k1gNnSc2	Jordánsko
vyznamenal	vyznamenat	k5eAaPmAgInS	vyznamenat
dne	den	k1gInSc2	den
11.	[number]	k4	11.
února	únor	k1gInSc2	únor
2015	[number]	k4	2015
krále	král	k1gMnSc2	král
Abdalláha	Abdalláh	k1gMnSc2	Abdalláh
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Řádem	řád	k1gInSc7	řád
Bílého	bílý	k2eAgInSc2d1	bílý
lva	lev	k1gInSc2	lev
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
roli	role	k1gFnSc4	role
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
Islámskému	islámský	k2eAgInSc3d1	islámský
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
obdržel	obdržet	k5eAaPmAgMnS	obdržet
jordánské	jordánský	k2eAgNnSc4d1	Jordánské
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
<g/>
.	.	kIx.	.
<g/>
Negativně	negativně	k6eAd1	negativně
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
o	o	k7c6	o
islámu	islám	k1gInSc6	islám
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
označil	označit	k5eAaPmAgInS	označit
za	za	k7c4	za
anticivilizaci	anticivilizace	k1gFnSc4	anticivilizace
financovanou	financovaný	k2eAgFnSc4d1	financovaná
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
drog	droga	k1gFnPc2	droga
<g/>
.	.	kIx.	.
</s>
<s>
Odmítá	odmítat	k5eAaImIp3nS	odmítat
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
mezi	mezi	k7c7	mezi
umírněnými	umírněný	k2eAgMnPc7d1	umírněný
a	a	k8xC	a
radikálními	radikální	k2eAgMnPc7d1	radikální
muslimy	muslim	k1gMnPc7	muslim
a	a	k8xC	a
nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
se	se	k3xPyFc4	se
stavbou	stavba	k1gFnSc7	stavba
mešit	mešita	k1gFnPc2	mešita
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
Palestina	Palestina	k1gFnSc1	Palestina
suverénním	suverénní	k2eAgInSc7d1	suverénní
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
budoucího	budoucí	k2eAgNnSc2d1	budoucí
rozšiřování	rozšiřování	k1gNnSc2	rozšiřování
NATO	NATO	kA	NATO
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
přijetí	přijetí	k1gNnSc4	přijetí
Izraele	Izrael	k1gInSc2	Izrael
spíše	spíše	k9	spíše
než	než	k8xS	než
pro	pro	k7c4	pro
přijeti	přijet	k5eAaPmF	přijet
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
nebo	nebo	k8xC	nebo
Gruzie	Gruzie	k1gFnSc2	Gruzie
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2015	[number]	k4	2015
Zeman	Zeman	k1gMnSc1	Zeman
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
evropské	evropský	k2eAgInPc1d1	evropský
státy	stát	k1gInPc1	stát
přijmou	přijmout	k5eAaPmIp3nP	přijmout
vlnu	vlna	k1gFnSc4	vlna
uprchlíků	uprchlík	k1gMnPc2	uprchlík
z	z	k7c2	z
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
a	a	k8xC	a
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
tím	ten	k3xDgNnSc7	ten
usnadní	usnadnit	k5eAaPmIp3nS	usnadnit
expanzi	expanze	k1gFnSc4	expanze
Islámského	islámský	k2eAgInSc2d1	islámský
státu	stát	k1gInSc2	stát
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
vraždu	vražda	k1gFnSc4	vražda
saúdského	saúdský	k2eAgMnSc4d1	saúdský
opozičního	opoziční	k2eAgMnSc4d1	opoziční
novináře	novinář	k1gMnSc4	novinář
Džamála	Džamál	k1gMnSc4	Džamál
Chášukdžího	Chášukdží	k1gMnSc4	Chášukdží
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zmizel	zmizet	k5eAaPmAgMnS	zmizet
na	na	k7c6	na
saudském	saudský	k2eAgInSc6d1	saudský
konzulátu	konzulát	k1gInSc6	konzulát
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
<g/>
,	,	kIx,	,
Zeman	Zeman	k1gMnSc1	Zeman
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
celosvětového	celosvětový	k2eAgNnSc2d1	celosvětové
embarga	embargo	k1gNnSc2	embargo
na	na	k7c4	na
vývoz	vývoz	k1gInSc4	vývoz
zbraní	zbraň	k1gFnPc2	zbraň
do	do	k7c2	do
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
bych	by	kYmCp1nS	by
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
embargu	embargo	k1gNnSc3	embargo
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
připojila	připojit	k5eAaPmAgFnS	připojit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
jen	jen	k9	jen
izolovaná	izolovaný	k2eAgFnSc1d1	izolovaná
akce	akce	k1gFnSc1	akce
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zatraceně	zatraceně	k6eAd1	zatraceně
dobře	dobře	k6eAd1	dobře
vím	vědět	k5eAaImIp1nS	vědět
–	–	k?	–
a	a	k8xC	a
vy	vy	k3xPp2nPc1	vy
to	ten	k3xDgNnSc4	ten
víte	vědět	k5eAaImIp2nP	vědět
taky	taky	k6eAd1	taky
–	–	k?	–
že	že	k8xS	že
tam	tam	k6eAd1	tam
okamžitě	okamžitě	k6eAd1	okamžitě
vystartuje	vystartovat	k5eAaPmIp3nS	vystartovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
západních	západní	k2eAgFnPc2d1	západní
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vyplní	vyplnit	k5eAaPmIp3nP	vyplnit
mezeru	mezera	k1gFnSc4	mezera
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Zeman	Zeman	k1gMnSc1	Zeman
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
vstupu	vstup	k1gInSc2	vstup
Turecka	Turecko	k1gNnSc2	Turecko
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Zemana	Zeman	k1gMnSc2	Zeman
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Turecko	Turecko	k1gNnSc1	Turecko
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Erdoğ	Erdoğ	k1gFnSc2	Erdoğ
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
autoritářské	autoritářský	k2eAgNnSc1d1	autoritářské
a	a	k8xC	a
provádí	provádět	k5eAaImIp3nS	provádět
politické	politický	k2eAgFnPc4d1	politická
čistky	čistka	k1gFnPc4	čistka
<g/>
,	,	kIx,	,
nebude	být	k5eNaImBp3nS	být
mít	mít	k5eAaImF	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
"	"	kIx"	"
<g/>
NATO	NATO	kA	NATO
je	být	k5eAaImIp3nS	být
vojenská	vojenský	k2eAgFnSc1d1	vojenská
organizace	organizace	k1gFnSc1	organizace
a	a	k8xC	a
vojenská	vojenský	k2eAgFnSc1d1	vojenská
organizace	organizace	k1gFnSc1	organizace
se	se	k3xPyFc4	se
neřídí	řídit	k5eNaImIp3nP	řídit
politickými	politický	k2eAgNnPc7d1	politické
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
hodnotovými	hodnotový	k2eAgNnPc7d1	hodnotové
pravidly	pravidlo	k1gNnPc7	pravidlo
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
Zeman	Zeman	k1gMnSc1	Zeman
přivítal	přivítat	k5eAaPmAgMnS	přivítat
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
amerického	americký	k2eAgMnSc2d1	americký
prezidenta	prezident	k1gMnSc2	prezident
Donalda	Donald	k1gMnSc2	Donald
Trumpa	Trump	k1gMnSc2	Trump
uznat	uznat	k5eAaPmF	uznat
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
za	za	k7c4	za
izraelské	izraelský	k2eAgNnSc4d1	izraelské
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
a	a	k8xC	a
přesunout	přesunout	k5eAaPmF	přesunout
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
americkou	americký	k2eAgFnSc4d1	americká
ambasádu	ambasáda	k1gFnSc4	ambasáda
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
východní	východní	k2eAgInSc1d1	východní
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
Izraelem	Izrael	k1gInSc7	Izrael
nelegálně	legálně	k6eNd1	legálně
anektován	anektován	k2eAgInSc4d1	anektován
po	po	k7c6	po
vojenské	vojenský	k2eAgFnSc6d1	vojenská
okupaci	okupace	k1gFnSc6	okupace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Stát	stát	k1gInSc1	stát
Palestina	Palestina	k1gFnSc1	Palestina
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
to	ten	k3xDgNnSc4	ten
okomentoval	okomentovat	k5eAaPmAgMnS	okomentovat
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
mám	mít	k5eAaImIp1nS	mít
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
velkou	velký	k2eAgFnSc4d1	velká
radost	radost	k1gFnSc4	radost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
byl	být	k5eAaImAgMnS	být
asi	asi	k9	asi
před	před	k7c7	před
čtyřmi	čtyři	k4xCgInPc7	čtyři
roky	rok	k1gInPc7	rok
na	na	k7c6	na
návštěvě	návštěva	k1gFnSc6	návštěva
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jsem	být	k5eAaImIp1nS	být
říkal	říkat	k5eAaImAgMnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bych	by	kYmCp1nS	by
velmi	velmi	k6eAd1	velmi
uvítal	uvítat	k5eAaPmAgMnS	uvítat
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
česká	český	k2eAgFnSc1d1	Česká
ambasáda	ambasáda	k1gFnSc1	ambasáda
přesunula	přesunout	k5eAaPmAgFnS	přesunout
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Zeman	Zeman	k1gMnSc1	Zeman
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
Bezpečnostní	bezpečnostní	k2eAgFnSc4d1	bezpečnostní
informační	informační	k2eAgFnSc4d1	informační
službu	služba	k1gFnSc4	služba
(	(	kIx(	(
<g/>
BIS	BIS	kA	BIS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
ignoruje	ignorovat	k5eAaImIp3nS	ignorovat
hrozbu	hrozba	k1gFnSc4	hrozba
Islámského	islámský	k2eAgInSc2d1	islámský
státu	stát	k1gInSc2	stát
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
včas	včas	k6eAd1	včas
neodhalila	odhalit	k5eNaPmAgFnS	odhalit
činnost	činnost	k1gFnSc1	činnost
pražského	pražský	k2eAgMnSc2d1	pražský
imáma	imám	k1gMnSc2	imám
Sámera	Sámer	k1gMnSc2	Sámer
Shehadeha	Shehadeh	k1gMnSc2	Shehadeh
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
podpory	podpora	k1gFnSc2	podpora
terorismu	terorismus	k1gInSc2	terorismus
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
čučkaři	čučkař	k1gMnPc1	čučkař
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mají	mít	k5eAaImIp3nP	mít
najít	najít	k5eAaPmF	najít
teroristu	terorista	k1gMnSc4	terorista
<g/>
,	,	kIx,	,
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
opravdu	opravdu	k6eAd1	opravdu
existoval	existovat	k5eAaImAgInS	existovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
ostatně	ostatně	k6eAd1	ostatně
i	i	k9	i
Národní	národní	k2eAgFnSc1d1	národní
centrála	centrála	k1gFnSc1	centrála
pro	pro	k7c4	pro
odhalování	odhalování	k1gNnSc4	odhalování
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
zločinu	zločin	k1gInSc2	zločin
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
jsou	být	k5eAaImIp3nP	být
teroristé	terorista	k1gMnPc1	terorista
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
ho	on	k3xPp3gNnSc4	on
nenašla	najít	k5eNaPmAgFnS	najít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2019	[number]	k4	2019
Zeman	Zeman	k1gMnSc1	Zeman
obvinil	obvinit	k5eAaPmAgMnS	obvinit
Turecko	Turecko	k1gNnSc4	Turecko
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
chovalo	chovat	k5eAaImAgNnS	chovat
jako	jako	k9	jako
faktický	faktický	k2eAgMnSc1d1	faktický
spojenec	spojenec	k1gMnSc1	spojenec
Islámského	islámský	k2eAgInSc2d1	islámský
statu	status	k1gInSc2	status
<g/>
,	,	kIx,	,
kterému	který	k3yQgNnSc3	který
pomáhalo	pomáhat	k5eAaImAgNnS	pomáhat
s	s	k7c7	s
vývozem	vývoz	k1gInSc7	vývoz
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Turecký	turecký	k2eAgMnSc1d1	turecký
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Zemanovy	Zemanův	k2eAgInPc4d1	Zemanův
výroky	výrok	k1gInPc4	výrok
odsoudil	odsoudit	k5eAaPmAgInS	odsoudit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
nešťastné	šťastný	k2eNgFnSc2d1	nešťastná
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
nepřijatelné	přijatelný	k2eNgFnSc3d1	nepřijatelná
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Tomáš	Tomáš	k1gMnSc1	Tomáš
Petříček	Petříček	k1gMnSc1	Petříček
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
Zemanova	Zemanův	k2eAgNnPc4d1	Zemanovo
slova	slovo	k1gNnPc4	slovo
připomněl	připomnět	k5eAaPmAgInS	připomnět
význam	význam	k1gInSc1	význam
Turecka	Turecko	k1gNnSc2	Turecko
jako	jako	k8xS	jako
spojence	spojenec	k1gMnSc2	spojenec
v	v	k7c6	v
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Krize	krize	k1gFnSc1	krize
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
ukrajinskou	ukrajinský	k2eAgFnSc7d1	ukrajinská
krizí	krize	k1gFnSc7	krize
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
naplno	naplno	k6eAd1	naplno
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2014	[number]	k4	2014
a	a	k8xC	a
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
spornou	sporný	k2eAgFnSc4d1	sporná
stala	stát	k5eAaPmAgFnS	stát
ruská	ruský	k2eAgFnSc1d1	ruská
anexe	anexe	k1gFnSc1	anexe
Krymu	Krym	k1gInSc2	Krym
<g/>
,	,	kIx,	,
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
Zeman	Zeman	k1gMnSc1	Zeman
o	o	k7c6	o
krymském	krymský	k2eAgNnSc6d1	krymské
referendu	referendum	k1gNnSc6	referendum
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
ukrajinskou	ukrajinský	k2eAgFnSc7d1	ukrajinská
ústavou	ústava	k1gFnSc7	ústava
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
je	být	k5eAaImIp3nS	být
projevem	projev	k1gInSc7	projev
nesouhlasu	nesouhlas	k1gInSc2	nesouhlas
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
se	se	k3xPyFc4	se
současnou	současný	k2eAgFnSc7d1	současná
situací	situace	k1gFnSc7	situace
<g/>
.	.	kIx.	.
</s>
<s>
Krym	Krym	k1gInSc1	Krym
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
prezidenta	prezident	k1gMnSc2	prezident
již	již	k6eAd1	již
ztracen	ztracen	k2eAgInSc1d1	ztracen
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
Evropskou	evropský	k2eAgFnSc4d1	Evropská
unii	unie	k1gFnSc4	unie
a	a	k8xC	a
NATO	NATO	kA	NATO
k	k	k7c3	k
razantní	razantní	k2eAgFnSc3d1	razantní
preventivní	preventivní	k2eAgFnSc3d1	preventivní
akci	akce	k1gFnSc3	akce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
Rusko	Rusko	k1gNnSc4	Rusko
zastrašila	zastrašit	k5eAaPmAgFnS	zastrašit
před	před	k7c7	před
dalšími	další	k2eAgInPc7d1	další
vojenskými	vojenský	k2eAgInPc7d1	vojenský
výpady	výpad	k1gInPc7	výpad
na	na	k7c4	na
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
Rusko	Rusko	k1gNnSc1	Rusko
pokusilo	pokusit	k5eAaPmAgNnS	pokusit
anektovat	anektovat	k5eAaBmF	anektovat
další	další	k2eAgFnSc4d1	další
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
do	do	k7c2	do
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
však	však	k9	však
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
proti	proti	k7c3	proti
uvalování	uvalování	k1gNnSc3	uvalování
blokád	blokáda	k1gFnPc2	blokáda
<g/>
,	,	kIx,	,
sankcí	sankce	k1gFnPc2	sankce
a	a	k8xC	a
embarg	embargo	k1gNnPc2	embargo
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3.	[number]	k4	3.
září	září	k1gNnSc4	září
2014	[number]	k4	2014
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
Frekvenci	frekvence	k1gFnSc4	frekvence
1	[number]	k4	1
uvedl	uvést	k5eAaPmAgInS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
zatím	zatím	k6eAd1	zatím
nebylo	být	k5eNaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
je	být	k5eAaImIp3nS	být
ruská	ruský	k2eAgFnSc1d1	ruská
invazní	invazní	k2eAgFnSc1d1	invazní
armáda	armáda	k1gFnSc1	armáda
<g/>
"	"	kIx"	"
a	a	k8xC	a
že	že	k8xS	že
"	"	kIx"	"
<g/>
bere	brát	k5eAaImIp3nS	brát
vážně	vážně	k6eAd1	vážně
prohlášení	prohlášení	k1gNnSc1	prohlášení
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Lavrova	Lavrův	k2eAgFnSc1d1	Lavrova
<g/>
,	,	kIx,	,
že	že	k8xS	že
tam	tam	k6eAd1	tam
žádní	žádný	k3yNgMnPc1	žádný
ruští	ruský	k2eAgMnPc1d1	ruský
vojáci	voják	k1gMnPc1	voják
nejsou	být	k5eNaImIp3nP	být
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5.	[number]	k4	5.
září	září	k1gNnSc2	září
pak	pak	k6eAd1	pak
během	během	k7c2	během
summitu	summit	k1gInSc2	summit
NATO	NATO	kA	NATO
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
není	být	k5eNaImIp3nS	být
pochyb	pochyba	k1gFnPc2	pochyba
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
čelí	čelit	k5eAaImIp3nS	čelit
ruské	ruský	k2eAgFnSc3d1	ruská
agresi	agrese	k1gFnSc3	agrese
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
"	"	kIx"	"
<g/>
nepochybuje	pochybovat	k5eNaImIp3nS	pochybovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
agrese	agrese	k1gFnSc1	agrese
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
podob	podoba	k1gFnPc2	podoba
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
například	například	k6eAd1	například
dodávky	dodávka	k1gFnPc4	dodávka
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
dodávky	dodávka	k1gFnSc2	dodávka
<g />
.	.	kIx.	.
</s>
<s>
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nejsou	být	k5eNaImIp3nP	být
dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
situaci	situace	k1gFnSc4	situace
nepovažuje	považovat	k5eNaImIp3nS	považovat
za	za	k7c4	za
přímou	přímý	k2eAgFnSc4d1	přímá
invazi	invaze	k1gFnSc4	invaze
<g/>
.	.	kIx.	.
9.	[number]	k4	9.
října	říjen	k1gInSc2	říjen
Zeman	Zeman	k1gMnSc1	Zeman
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
před	před	k7c7	před
novináři	novinář	k1gMnPc7	novinář
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
oslav	oslava	k1gFnPc2	oslava
25.	[number]	k4	25.
výročí	výročí	k1gNnPc2	výročí
pádu	pád	k1gInSc2	pád
komunismu	komunismus	k1gInSc2	komunismus
<g/>
,	,	kIx,	,
že	že	k8xS	že
válka	válka	k1gFnSc1	válka
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
je	být	k5eAaImIp3nS	být
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
s	s	k7c7	s
občanskou	občanský	k2eAgFnSc7d1	občanská
válkou	válka	k1gFnSc7	válka
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
ve	v	k7c6	v
30.	[number]	k4	30.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
byla	být	k5eAaImAgFnS	být
také	také	k9	také
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
intervenovali	intervenovat	k5eAaImAgMnP	intervenovat
Rusové	Rus	k1gMnPc1	Rus
a	a	k8xC	a
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
,	,	kIx,	,
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
strany	strana	k1gFnSc2	strana
intervenovali	intervenovat	k5eAaImAgMnP	intervenovat
Němci	Němec	k1gMnPc1	Němec
a	a	k8xC	a
Italové	Ital	k1gMnPc1	Ital
<g/>
.	.	kIx.	.
.	.	kIx.	.
<g/>
..	..	k?	..
Bojoval	bojovat	k5eAaImAgMnS	bojovat
tam	tam	k6eAd1	tam
Španěl	Španěl	k1gMnSc1	Španěl
proti	proti	k7c3	proti
Španělovi	Španěl	k1gMnSc3	Španěl
<g/>
,	,	kIx,	,
samozřejmě	samozřejmě	k6eAd1	samozřejmě
za	za	k7c2	za
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
pomoci	pomoc	k1gFnSc2	pomoc
svých	svůj	k3xOyFgMnPc2	svůj
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
prezident	prezident	k1gMnSc1	prezident
Zeman	Zeman	k1gMnSc1	Zeman
rozhovor	rozhovor	k1gInSc4	rozhovor
Prvnímu	první	k4xOgInSc3	první
kanálu	kanál	k1gInSc3	kanál
ruské	ruský	k2eAgFnSc2d1	ruská
státní	státní	k2eAgFnSc2d1	státní
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
mimo	mimo	k6eAd1	mimo
jiné	jiná	k1gFnSc2	jiná
označil	označit	k5eAaPmAgMnS	označit
konflikt	konflikt	k1gInSc4	konflikt
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
</s>
<s>
Ukrajině	Ukrajina	k1gFnSc3	Ukrajina
za	za	k7c4	za
občanskou	občanský	k2eAgFnSc4d1	občanská
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
pomoc	pomoc	k1gFnSc4	pomoc
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
nesmyslnou	smyslný	k2eNgFnSc4d1	nesmyslná
<g/>
,	,	kIx,	,
dokud	dokud	k6eAd1	dokud
nebude	být	k5eNaImBp3nS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
mírové	mírový	k2eAgFnPc4d1	mírová
dohody	dohoda	k1gFnSc2	dohoda
<g/>
.	.	kIx.	.
20.	[number]	k4	20.
listopadu	listopad	k1gInSc2	listopad
předvolalo	předvolat	k5eAaPmAgNnS	předvolat
ukrajinské	ukrajinský	k2eAgNnSc1d1	ukrajinské
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
českého	český	k2eAgMnSc2d1	český
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
Ivana	Ivan	k1gMnSc2	Ivan
Počucha	Počuch	k1gMnSc2	Počuch
a	a	k8xC	a
sdělilo	sdělit	k5eAaPmAgNnS	sdělit
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezidentovy	prezidentův	k2eAgInPc4d1	prezidentův
výroky	výrok	k1gInPc4	výrok
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
nepřijatelné	přijatelný	k2eNgNnSc4d1	nepřijatelné
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
neodpovídají	odpovídat	k5eNaImIp3nP	odpovídat
duchu	duch	k1gMnSc3	duch
tradičně	tradičně	k6eAd1	tradičně
přátelských	přátelský	k2eAgInPc2d1	přátelský
česko	česko	k1gNnSc4	česko
<g/>
–	–	k?	–
<g/>
ukrajinských	ukrajinský	k2eAgInPc2d1	ukrajinský
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
3.	[number]	k4	3.
ledna	leden	k1gInSc2	leden
2015	[number]	k4	2015
poskytl	poskytnout	k5eAaPmAgInS	poskytnout
rozhovor	rozhovor	k1gInSc1	rozhovor
deníku	deník	k1gInSc2	deník
Právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
ukrajinského	ukrajinský	k2eAgMnSc2d1	ukrajinský
premiéra	premiér	k1gMnSc2	premiér
Arsenije	Arsenije	k1gMnSc2	Arsenije
Jaceňuka	Jaceňuk	k1gMnSc2	Jaceňuk
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
války	válka	k1gFnSc2	válka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
odmítá	odmítat	k5eAaImIp3nS	odmítat
mírové	mírový	k2eAgNnSc4d1	Mírové
řešení	řešení	k1gNnSc4	řešení
ukrajinského	ukrajinský	k2eAgInSc2d1	ukrajinský
konfliktu	konflikt	k1gInSc2	konflikt
<g/>
.	.	kIx.	.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
masové	masový	k2eAgMnPc4d1	masový
vrahy	vrah	k1gMnPc4	vrah
Poláků	Polák	k1gMnPc2	Polák
a	a	k8xC	a
Židů	Žid	k1gMnPc2	Žid
vůdce	vůdce	k1gMnSc2	vůdce
Ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
povstalecké	povstalecký	k2eAgFnSc2d1	povstalecká
armády	armáda	k1gFnSc2	armáda
(	(	kIx(	(
<g/>
UPA	UPA	kA	UPA
<g/>
)	)	kIx)	)
Romana	Roman	k1gMnSc2	Roman
Šuchevyče	Šuchevyč	k1gInSc2	Šuchevyč
a	a	k8xC	a
Stepana	Stepana	k1gFnSc1	Stepana
Banderu	Bander	k1gInSc2	Bander
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
Ukrajinců	Ukrajinec	k1gMnPc2	Ukrajinec
národní	národní	k2eAgMnPc1d1	národní
hrdinové	hrdina	k1gMnPc1	hrdina
a	a	k8xC	a
kterým	který	k3yIgMnSc7	který
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2015	[number]	k4	2015
přiznán	přiznán	k2eAgInSc4d1	přiznán
"	"	kIx"	"
<g/>
status	status	k1gInSc4	status
bojovníků	bojovník	k1gMnPc2	bojovník
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
"	"	kIx"	"
<g/>
Nemohu	moct	k5eNaImIp1nS	moct
bohužel	bohužel	k9	bohužel
blahopřát	blahopřát	k5eAaImF	blahopřát
Ukrajině	Ukrajina	k1gFnSc3	Ukrajina
k	k	k7c3	k
takovým	takový	k3xDgMnPc3	takový
národním	národní	k2eAgMnPc3d1	národní
hrdinům	hrdina	k1gMnPc3	hrdina
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2019	[number]	k4	2019
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Tomáše	Tomáš	k1gMnSc2	Tomáš
Petříčka	Petříček	k1gMnSc2	Petříček
z	z	k7c2	z
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
mu	on	k3xPp3gMnSc3	on
Zeman	Zeman	k1gMnSc1	Zeman
vytkl	vytknout	k5eAaPmAgMnS	vytknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
"	"	kIx"	"
<g/>
dostatečně	dostatečně	k6eAd1	dostatečně
nezmínil	zmínit	k5eNaPmAgInS	zmínit
problém	problém	k1gInSc1	problém
banderovců	banderovec	k1gMnPc2	banderovec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
nově	nově	k6eAd1	nově
přijatý	přijatý	k2eAgInSc1d1	přijatý
ukrajinský	ukrajinský	k2eAgInSc1d1	ukrajinský
zákon	zákon	k1gInSc1	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
sociální	sociální	k2eAgFnPc4d1	sociální
výhody	výhoda	k1gFnPc4	výhoda
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2018	[number]	k4	2018
bylo	být	k5eAaImAgNnS	být
Petříčkovo	Petříčkův	k2eAgNnSc1d1	Petříčkův
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
kritizováno	kritizovat	k5eAaImNgNnS	kritizovat
prezidentovým	prezidentův	k2eAgMnSc7d1	prezidentův
mluvčím	mluvčí	k1gMnSc7	mluvčí
Ovčáčkem	ovčáček	k1gMnSc7	ovčáček
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
ukrajinskému	ukrajinský	k2eAgInSc3d1	ukrajinský
zákonu	zákon	k1gInSc3	zákon
o	o	k7c6	o
banderovcích	banderovec	k1gMnPc6	banderovec
"	"	kIx"	"
<g/>
zbaběle	zbaběle	k6eAd1	zbaběle
mlčí	mlčet	k5eAaImIp3nS	mlčet
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
předtím	předtím	k6eAd1	předtím
vydalo	vydat	k5eAaPmAgNnS	vydat
prohlášení	prohlášení	k1gNnSc1	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
záležitost	záležitost	k1gFnSc4	záležitost
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
a	a	k8xC	a
úkolem	úkol	k1gInSc7	úkol
diplomacie	diplomacie	k1gFnSc2	diplomacie
není	být	k5eNaImIp3nS	být
"	"	kIx"	"
<g/>
hodnotit	hodnotit	k5eAaImF	hodnotit
nebo	nebo	k8xC	nebo
zkoumat	zkoumat	k5eAaImF	zkoumat
historii	historie	k1gFnSc4	historie
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
záležitost	záležitost	k1gFnSc1	záležitost
historiků	historik	k1gMnPc2	historik
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
===	===	k?	===
</s>
</p>
<p>
<s>
K	k	k7c3	k
hrozbě	hrozba	k1gFnSc3	hrozba
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
podobně	podobně	k6eAd1	podobně
odmítavý	odmítavý	k2eAgInSc1d1	odmítavý
postoj	postoj	k1gInSc1	postoj
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gMnSc1	jeho
předchůdce	předchůdce	k1gMnSc1	předchůdce
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
,	,	kIx,	,
když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
hysterii	hysterie	k1gFnSc4	hysterie
vyvolanou	vyvolaný	k2eAgFnSc4d1	vyvolaná
ekologickými	ekologický	k2eAgFnPc7d1	ekologická
lobbisty	lobbista	k1gMnPc4	lobbista
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
pro	pro	k7c4	pro
agenturu	agentura	k1gFnSc4	agentura
Mediafax	Mediafax	k1gInSc4	Mediafax
dále	daleko	k6eAd2	daleko
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Globální	globální	k2eAgNnSc1d1	globální
oteplování	oteplování	k1gNnSc1	oteplování
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
historii	historie	k1gFnSc6	historie
po	po	k7c4	po
tisící	tisící	k4xOgInPc4	tisící
prvé	prvý	k4xOgInPc4	prvý
<g/>
.	.	kIx.	.
</s>
<s>
Globální	globální	k2eAgNnSc1d1	globální
oteplování	oteplování	k1gNnSc1	oteplování
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
planety	planeta	k1gFnSc2	planeta
po	po	k7c4	po
miliónté	milióntý	k4xOgInPc4	milióntý
prvé	prvý	k4xOgInPc4	prvý
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
po	po	k7c4	po
tisící	tisící	k4xOgInPc4	tisící
prvé	prvý	k4xOgInPc4	prvý
a	a	k8xC	a
miliónté	milióntý	k4xOgInPc4	milióntý
prvé	prvý	k4xOgNnSc4	prvý
globální	globální	k2eAgNnSc4d1	globální
ochlazování	ochlazování	k1gNnSc4	ochlazování
<g/>
.	.	kIx.	.
</s>
<s>
Pokládám	pokládat	k5eAaImIp1nS	pokládat
teorii	teorie	k1gFnSc4	teorie
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
za	za	k7c4	za
tvrzení	tvrzení	k1gNnSc4	tvrzení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
lidské	lidský	k2eAgFnSc6d1	lidská
pýše	pýcha	k1gFnSc6	pýcha
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidstvo	lidstvo	k1gNnSc1	lidstvo
svojí	svojit	k5eAaImIp3nS	svojit
emisí	emise	k1gFnSc7	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
dramatické	dramatický	k2eAgNnSc4d1	dramatické
kolísání	kolísání	k1gNnSc4	kolísání
teplot	teplota	k1gFnPc2	teplota
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Zeman	Zeman	k1gMnSc1	Zeman
též	též	k9	též
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
s	s	k7c7	s
prolomením	prolomení	k1gNnSc7	prolomení
těžebních	těžební	k2eAgInPc2d1	těžební
limitů	limit	k1gInPc2	limit
hnědouhelných	hnědouhelný	k2eAgInPc2d1	hnědouhelný
dolů	dol	k1gInPc2	dol
v	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojeným	spojený	k2eAgNnSc7d1	spojené
bouráním	bourání	k1gNnSc7	bourání
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Zastává	zastávat	k5eAaImIp3nS	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Šumava	Šumava	k1gFnSc1	Šumava
je	být	k5eAaImIp3nS	být
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
les	les	k1gInSc4	les
jako	jako	k8xS	jako
každý	každý	k3xTgMnSc1	každý
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odpůrcem	odpůrce	k1gMnSc7	odpůrce
solárních	solární	k2eAgInPc2d1	solární
a	a	k8xC	a
větrných	větrný	k2eAgFnPc2d1	větrná
elektráren	elektrárna	k1gFnPc2	elektrárna
a	a	k8xC	a
práva	právo	k1gNnSc2	právo
nevládních	vládní	k2eNgFnPc2d1	nevládní
(	(	kIx(	(
<g/>
občanských	občanský	k2eAgFnPc2d1	občanská
<g/>
)	)	kIx)	)
organizací	organizace	k1gFnPc2	organizace
na	na	k7c4	na
účast	účast	k1gFnSc4	účast
ve	v	k7c6	v
správních	správní	k2eAgNnPc6d1	správní
řízeních	řízení	k1gNnPc6	řízení
s	s	k7c7	s
vlivem	vliv	k1gInSc7	vliv
na	na	k7c4	na
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
přemrštěná	přemrštěný	k2eAgFnSc1d1	přemrštěná
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podporuje	podporovat	k5eAaImIp3nS	podporovat
naopak	naopak	k6eAd1	naopak
jadernou	jaderný	k2eAgFnSc4d1	jaderná
energetiku	energetika	k1gFnSc4	energetika
a	a	k8xC	a
dostavbu	dostavba	k1gFnSc4	dostavba
elektrárny	elektrárna	k1gFnSc2	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Inkluzivní	Inkluzivní	k2eAgNnSc1d1	Inkluzivní
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
návštěvě	návštěva	k1gFnSc6	návštěva
Pardubického	pardubický	k2eAgInSc2d1	pardubický
kraje	kraj	k1gInSc2	kraj
dne	den	k1gInSc2	den
14.	[number]	k4	14.
ledna	leden	k1gInSc2	leden
2015	[number]	k4	2015
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nejsem	být	k5eNaImIp1nS	být
zastáncem	zastánce	k1gMnSc7	zastánce
názoru	názor	k1gInSc2	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
určitým	určitý	k2eAgInSc7d1	určitý
způsobem	způsob	k1gInSc7	způsob
handicapované	handicapovaný	k2eAgFnSc2d1	handicapovaná
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
umisťované	umisťovaný	k2eAgInPc1d1	umisťovaný
do	do	k7c2	do
tříd	třída	k1gFnPc2	třída
s	s	k7c7	s
nehandicapovanými	handicapovaný	k2eNgMnPc7d1	nehandicapovaný
žáky	žák	k1gMnPc7	žák
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
neštěstí	neštěstí	k1gNnSc1	neštěstí
pro	pro	k7c4	pro
oba	dva	k4xCgMnPc4	dva
.	.	kIx.	.
<g/>
..	..	k?	..
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
duševní	duševní	k2eAgFnSc2d1	duševní
pohody	pohoda	k1gFnSc2	pohoda
je	být	k5eAaImIp3nS	být
daleko	daleko	k6eAd1	daleko
lepší	dobrý	k2eAgNnSc1d2	lepší
<g/>
,	,	kIx,	,
když	když	k8xS	když
budou	být	k5eAaImBp3nP	být
existovat	existovat	k5eAaImF	existovat
praktické	praktický	k2eAgFnPc4d1	praktická
třídy	třída	k1gFnPc4	třída
<g/>
.	.	kIx.	.
</s>
<s>
Vůbec	vůbec	k9	vůbec
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
inkluze	inkluze	k1gFnSc1	inkluze
nelíbí	líbit	k5eNaImIp3nS	líbit
a	a	k8xC	a
jsem	být	k5eAaImIp1nS	být
proti	proti	k7c3	proti
ní	on	k3xPp3gFnSc3	on
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
načež	načež	k6eAd1	načež
jej	on	k3xPp3gMnSc4	on
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
výrok	výrok	k1gInSc4	výrok
zkritizovala	zkritizovat	k5eAaPmAgFnS	zkritizovat
Národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
osob	osoba	k1gFnPc2	osoba
se	s	k7c7	s
zdravotním	zdravotní	k2eAgNnSc7d1	zdravotní
postižením	postižení	k1gNnSc7	postižení
i	i	k8xC	i
ombudsmanka	ombudsmanka	k1gFnSc1	ombudsmanka
Anna	Anna	k1gFnSc1	Anna
Šabatová	Šabatová	k1gFnSc1	Šabatová
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentův	prezidentův	k2eAgInSc4d1	prezidentův
výrok	výrok	k1gInSc4	výrok
zkritizoval	zkritizovat	k5eAaPmAgMnS	zkritizovat
také	také	k9	také
Petr	Petr	k1gMnSc1	Petr
Hrubý	Hrubý	k1gMnSc1	Hrubý
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
Jedličkova	Jedličkův	k2eAgInSc2d1	Jedličkův
ústavu	ústav	k1gInSc2	ústav
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
poukázal	poukázat	k5eAaPmAgMnS	poukázat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dětem	dítě	k1gFnPc3	dítě
třeba	třeba	k6eAd1	třeba
přistupovat	přistupovat	k5eAaImF	přistupovat
individuálně	individuálně	k6eAd1	individuálně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Koncesionářské	koncesionářský	k2eAgInPc1d1	koncesionářský
poplatky	poplatek	k1gInPc1	poplatek
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
návštěvy	návštěva	k1gFnSc2	návštěva
Plzeňského	plzeňský	k2eAgInSc2d1	plzeňský
kraje	kraj	k1gInSc2	kraj
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
pro	pro	k7c4	pro
zrušení	zrušení	k1gNnSc4	zrušení
koncesionářských	koncesionářský	k2eAgInPc2d1	koncesionářský
poplatků	poplatek	k1gInPc2	poplatek
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
televizi	televize	k1gFnSc4	televize
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
stala	stát	k5eAaPmAgFnS	stát
"	"	kIx"	"
<g/>
špatnou	špatný	k2eAgFnSc7d1	špatná
komerční	komerční	k2eAgFnSc7d1	komerční
televizí	televize	k1gFnSc7	televize
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
ohradila	ohradit	k5eAaPmAgFnS	ohradit
a	a	k8xC	a
Zemanův	Zemanův	k2eAgInSc1d1	Zemanův
výrok	výrok	k1gInSc1	výrok
označila	označit	k5eAaPmAgFnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
bezprecedentní	bezprecedentní	k2eAgInSc4d1	bezprecedentní
útok	útok	k1gInSc4	útok
na	na	k7c4	na
nezávislost	nezávislost	k1gFnSc4	nezávislost
veřejnoprávní	veřejnoprávní	k2eAgFnSc2d1	veřejnoprávní
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
její	její	k3xOp3gFnSc2	její
existence	existence	k1gFnSc2	existence
v	v	k7c6	v
demokratické	demokratický	k2eAgFnSc6d1	demokratická
společnosti	společnost	k1gFnSc6	společnost
nemá	mít	k5eNaImIp3nS	mít
obdoby	obdoba	k1gFnSc2	obdoba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
poté	poté	k6eAd1	poté
o	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
neodvysílala	odvysílat	k5eNaPmAgFnS	odvysílat
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
tiskový	tiskový	k2eAgInSc4d1	tiskový
brífink	brífink	k1gInSc4	brífink
mluvčího	mluvčí	k1gMnSc2	mluvčí
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
Jiřího	Jiří	k1gMnSc2	Jiří
Ovčáčka	ovčáček	k1gMnSc2	ovčáček
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
hájila	hájit	k5eAaImAgFnS	hájit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hrad	hrad	k1gInSc1	hrad
pro	pro	k7c4	pro
brífink	brífink	k1gInSc4	brífink
neohlásil	ohlásit	k5eNaPmAgMnS	ohlásit
žádná	žádný	k3yNgNnPc4	žádný
zásadní	zásadní	k2eAgNnPc4d1	zásadní
sdělení	sdělení	k1gNnPc4	sdělení
<g/>
,	,	kIx,	,
a	a	k8xC	a
televize	televize	k1gFnSc1	televize
proto	proto	k8xC	proto
dala	dát	k5eAaPmAgFnS	dát
přednost	přednost	k1gFnSc4	přednost
aktuálním	aktuální	k2eAgNnPc3d1	aktuální
tématům	téma	k1gNnPc3	téma
<g/>
,	,	kIx,	,
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
kancelář	kancelář	k1gFnSc1	kancelář
"	"	kIx"	"
<g/>
považuje	považovat	k5eAaImIp3nS	považovat
tento	tento	k3xDgInSc4	tento
přístup	přístup	k1gInSc4	přístup
za	za	k7c4	za
dětinskou	dětinský	k2eAgFnSc4d1	dětinská
pomstu	pomsta	k1gFnSc4	pomsta
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
za	za	k7c4	za
názory	názor	k1gInPc4	názor
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
"	"	kIx"	"
a	a	k8xC	a
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
podala	podat	k5eAaPmAgFnS	podat
stížnost	stížnost	k1gFnSc4	stížnost
Radě	rada	k1gFnSc3	rada
pro	pro	k7c4	pro
rozhlasové	rozhlasový	k2eAgNnSc4d1	rozhlasové
a	a	k8xC	a
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
však	však	k9	však
byla	být	k5eAaImAgFnS	být
zamítnuta	zamítnout	k5eAaPmNgFnS	zamítnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Kritika	kritika	k1gFnSc1	kritika
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Hodnocení	hodnocení	k1gNnSc1	hodnocení
veřejností	veřejnost	k1gFnPc2	veřejnost
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
pravidelných	pravidelný	k2eAgInPc2d1	pravidelný
ročních	roční	k2eAgInPc2d1	roční
průzkumů	průzkum	k1gInPc2	průzkum
společnosti	společnost	k1gFnSc2	společnost
CVVM	CVVM	kA	CVVM
si	se	k3xPyFc3	se
občané	občan	k1gMnPc1	občan
nejvíce	nejvíce	k6eAd1	nejvíce
cení	cenit	k5eAaImIp3nS	cenit
prezidentova	prezidentův	k2eAgInSc2d1	prezidentův
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
a	a	k8xC	a
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
jejich	jejich	k3xOp3gInPc4	jejich
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2014	[number]	k4	2014
tento	tento	k3xDgInSc4	tento
aspekt	aspekt	k1gInSc4	aspekt
hodnotilo	hodnotit	k5eAaImAgNnS	hodnotit
velmi	velmi	k6eAd1	velmi
nebo	nebo	k8xC	nebo
spíše	spíše	k9	spíše
dobře	dobře	k6eAd1	dobře
52	[number]	k4	52
%	%	kIx~	%
dotazovaných	dotazovaný	k2eAgFnPc2d1	dotazovaná
<g/>
,	,	kIx,	,
po	po	k7c6	po
lednu	leden	k1gInSc6	leden
2016	[number]	k4	2016
o	o	k7c6	o
tom	ten	k3xDgMnSc6	ten
bylo	být	k5eAaImAgNnS	být
přesvědčeno	přesvědčit	k5eAaPmNgNnS	přesvědčit
již	již	k9	již
70	[number]	k4	70
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
kladných	kladný	k2eAgNnPc2d1	kladné
hodnocení	hodnocení	k1gNnPc2	hodnocení
mírně	mírně	k6eAd1	mírně
ubylo	ubýt	k5eAaPmAgNnS	ubýt
na	na	k7c4	na
66	[number]	k4	66
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
svým	svůj	k3xOyFgMnPc3	svůj
předchůdcům	předchůdce	k1gMnPc3	předchůdce
si	se	k3xPyFc3	se
tedy	tedy	k9	tedy
výrazně	výrazně	k6eAd1	výrazně
polepšil	polepšit	k5eAaPmAgMnS	polepšit
<g/>
.	.	kIx.	.
</s>
<s>
Václava	Václav	k1gMnSc4	Václav
Klause	Klaus	k1gMnSc4	Klaus
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
na	na	k7c6	na
konci	konec	k1gInSc6	konec
jeho	jeho	k3xOp3gFnSc2	jeho
prvního	první	k4xOgNnSc2	první
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
hodnotilo	hodnotit	k5eAaImAgNnS	hodnotit
kladně	kladně	k6eAd1	kladně
36	[number]	k4	36
%	%	kIx~	%
dotázaných	dotázaný	k2eAgFnPc2d1	dotázaná
<g/>
,	,	kIx,	,
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
situaci	situace	k1gFnSc6	situace
dokonce	dokonce	k9	dokonce
pouhých	pouhý	k2eAgInPc2d1	pouhý
25	[number]	k4	25
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
si	se	k3xPyFc3	se
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
vedl	vést	k5eAaImAgMnS	vést
i	i	k9	i
v	v	k7c6	v
názoru	názor	k1gInSc6	názor
na	na	k7c6	na
plnění	plnění	k1gNnSc6	plnění
funkcí	funkce	k1gFnPc2	funkce
svěřených	svěřený	k2eAgInPc2d1	svěřený
mu	on	k3xPp3gMnSc3	on
Ústavou	ústava	k1gFnSc7	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
počátečních	počáteční	k2eAgNnPc2d1	počáteční
52	[number]	k4	52
%	%	kIx~	%
vystoupal	vystoupat	k5eAaPmAgInS	vystoupat
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2016	[number]	k4	2016
na	na	k7c4	na
68	[number]	k4	68
%	%	kIx~	%
<g/>
,	,	kIx,	,
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2017	[number]	k4	2017
ho	on	k3xPp3gMnSc4	on
dotazovaní	dotazovaný	k2eAgMnPc1d1	dotazovaný
hodnotili	hodnotit	k5eAaImAgMnP	hodnotit
velmi	velmi	k6eAd1	velmi
nebo	nebo	k8xC	nebo
spíše	spíše	k9	spíše
dobře	dobře	k6eAd1	dobře
v	v	k7c6	v
63	[number]	k4	63
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
otázce	otázka	k1gFnSc6	otázka
si	se	k3xPyFc3	se
vede	vést	k5eAaImIp3nS	vést
hůře	zle	k6eAd2	zle
než	než	k8xS	než
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
úřad	úřad	k1gInSc1	úřad
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2007	[number]	k4	2007
hodnotilo	hodnotit	k5eAaImAgNnS	hodnotit
kladně	kladně	k6eAd1	kladně
celých	celý	k2eAgNnPc2d1	celé
80	[number]	k4	80
%	%	kIx~	%
respondentů	respondent	k1gMnPc2	respondent
<g/>
,	,	kIx,	,
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgNnSc7	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1998	[number]	k4	1998
spokojeno	spokojen	k2eAgNnSc1d1	spokojeno
66	[number]	k4	66
%	%	kIx~	%
oslovených	oslovený	k2eAgFnPc2d1	oslovená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prezidentovo	prezidentův	k2eAgNnSc1d1	prezidentovo
ovlivňování	ovlivňování	k1gNnSc1	ovlivňování
vnitropolitického	vnitropolitický	k2eAgInSc2d1	vnitropolitický
života	život	k1gInSc2	život
hodnotilo	hodnotit	k5eAaImAgNnS	hodnotit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2014	[number]	k4	2014
velmi	velmi	k6eAd1	velmi
nebo	nebo	k8xC	nebo
spíše	spíše	k9	spíše
dobře	dobře	k6eAd1	dobře
41	[number]	k4	41
%	%	kIx~	%
respondentů	respondent	k1gMnPc2	respondent
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
předchozích	předchozí	k2eAgFnPc6d1	předchozí
otázkách	otázka	k1gFnPc6	otázka
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2016	[number]	k4	2016
nárůst	nárůst	k1gInSc1	nárůst
na	na	k7c4	na
59	[number]	k4	59
%	%	kIx~	%
kladných	kladný	k2eAgInPc2d1	kladný
ohlasů	ohlas	k1gInPc2	ohlas
a	a	k8xC	a
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
mírný	mírný	k2eAgInSc4d1	mírný
pokles	pokles	k1gInSc4	pokles
na	na	k7c4	na
52	[number]	k4	52
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
předchozí	předchozí	k2eAgMnPc1d1	předchozí
prezidenti	prezident	k1gMnPc1	prezident
se	se	k3xPyFc4	se
vedli	vést	k5eAaImAgMnP	vést
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
konci	konec	k1gInSc6	konec
prvního	první	k4xOgNnSc2	první
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
jejich	jejich	k3xOp3gFnSc4	jejich
se	se	k3xPyFc4	se
hodnocení	hodnocení	k1gNnSc1	hodnocení
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
otázce	otázka	k1gFnSc6	otázka
pohybovalo	pohybovat	k5eAaImAgNnS	pohybovat
kolem	kolem	k6eAd1	kolem
55	[number]	k4	55
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2014	[number]	k4	2014
se	se	k3xPyFc4	se
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
reprezentaci	reprezentace	k1gFnSc4	reprezentace
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
dívalo	dívat	k5eAaImAgNnS	dívat
velmi	velmi	k6eAd1	velmi
nebo	nebo	k8xC	nebo
spíše	spíše	k9	spíše
negativně	negativně	k6eAd1	negativně
63	[number]	k4	63
%	%	kIx~	%
dotázaných	dotázaný	k2eAgFnPc2d1	dotázaná
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
tento	tento	k3xDgInSc4	tento
aspekt	aspekt	k1gInSc4	aspekt
připadal	připadat	k5eAaPmAgMnS	připadat
u	u	k7c2	u
prezidenta	prezident	k1gMnSc2	prezident
negativní	negativní	k2eAgNnSc4d1	negativní
67	[number]	k4	67
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2016	[number]	k4	2016
většina	většina	k1gFnSc1	většina
respondentů	respondent	k1gMnPc2	respondent
uvedla	uvést	k5eAaPmAgFnS	uvést
kladný	kladný	k2eAgInSc4d1	kladný
výsledek	výsledek	k1gInSc4	výsledek
<g/>
,	,	kIx,	,
nespokojeno	spokojit	k5eNaPmNgNnS	spokojit
bylo	být	k5eAaImAgNnS	být
48	[number]	k4	48
%	%	kIx~	%
dotazovaných	dotazovaný	k2eAgFnPc2d1	dotazovaná
<g/>
.	.	kIx.	.
</s>
<s>
Spokojenost	spokojenost	k1gFnSc1	spokojenost
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2017 47	[number]	k4	2017 47
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
předchůdci	předchůdce	k1gMnPc1	předchůdce
si	se	k3xPyFc3	se
stran	stran	k7c2	stran
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
reprezentace	reprezentace	k1gFnSc2	reprezentace
vedli	vést	k5eAaImAgMnP	vést
lépe	dobře	k6eAd2	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997 81	[number]	k4	1997 81
%	%	kIx~	%
odpovídajících	odpovídající	k2eAgFnPc6d1	odpovídající
<g/>
,	,	kIx,	,
Václava	Václav	k1gMnSc4	Václav
Klause	Klaus	k1gMnSc4	Klaus
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2007	[number]	k4	2007
oceňovalo	oceňovat	k5eAaImAgNnS	oceňovat
84	[number]	k4	84
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Autoritu	autorita	k1gFnSc4	autorita
Miloše	Miloš	k1gMnSc4	Miloš
Zemana	Zeman	k1gMnSc4	Zeman
u	u	k7c2	u
občanů	občan	k1gMnPc2	občan
pociťovalo	pociťovat	k5eAaImAgNnS	pociťovat
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2014 31	[number]	k4	2014 31
%	%	kIx~	%
dotázaných	dotázaný	k2eAgFnPc2d1	dotázaná
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
Zeman	Zeman	k1gMnSc1	Zeman
mírně	mírně	k6eAd1	mírně
propadl	propadnout	k5eAaPmAgMnS	propadnout
na	na	k7c4	na
27	[number]	k4	27
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
ledna	leden	k1gInSc2	leden
2016	[number]	k4	2016
si	se	k3xPyFc3	se
polepšil	polepšit	k5eAaPmAgMnS	polepšit
na	na	k7c4	na
51	[number]	k4	51
%	%	kIx~	%
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
mírně	mírně	k6eAd1	mírně
klesl	klesnout	k5eAaPmAgInS	klesnout
na	na	k7c4	na
45	[number]	k4	45
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
trend	trend	k1gInSc1	trend
sledoval	sledovat	k5eAaImAgInS	sledovat
pocit	pocit	k1gInSc4	pocit
respondentů	respondent	k1gMnPc2	respondent
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
dbá	dbát	k5eAaImIp3nS	dbát
o	o	k7c4	o
vážnost	vážnost	k1gFnSc4	vážnost
a	a	k8xC	a
důstojnost	důstojnost	k1gFnSc4	důstojnost
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
nebo	nebo	k8xC	nebo
spíše	spíše	k9	spíše
nespokojeno	spokojen	k2eNgNnSc1d1	nespokojeno
65	[number]	k4	65
%	%	kIx~	%
<g/>
,	,	kIx,	,
po	po	k7c6	po
lednu	leden	k1gInSc6	leden
2015	[number]	k4	2015
dokonce	dokonce	k9	dokonce
74	[number]	k4	74
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Lépe	dobře	k6eAd2	dobře
si	se	k3xPyFc3	se
Zeman	Zeman	k1gMnSc1	Zeman
vedl	vést	k5eAaImAgMnS	vést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
s	s	k7c7	s
48	[number]	k4	48
%	%	kIx~	%
<g/>
,	,	kIx,	,
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
přesvědčil	přesvědčit	k5eAaPmAgInS	přesvědčit
39	[number]	k4	39
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Vážnost	vážnost	k1gFnSc1	vážnost
a	a	k8xC	a
důstojnost	důstojnost	k1gFnSc1	důstojnost
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
úřadu	úřad	k1gInSc2	úřad
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
během	během	k7c2	během
prvního	první	k4xOgNnSc2	první
období	období	k1gNnSc2	období
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
téměř	téměř	k6eAd1	téměř
90	[number]	k4	90
%	%	kIx~	%
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
dbal	dbát	k5eAaImAgMnS	dbát
dobře	dobře	k6eAd1	dobře
o	o	k7c4	o
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
64	[number]	k4	64
%	%	kIx~	%
dotazovaných	dotazovaný	k2eAgFnPc2d1	dotazovaná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pozitivně	pozitivně	k6eAd1	pozitivně
hodnotili	hodnotit	k5eAaImAgMnP	hodnotit
Miloše	Miloš	k1gMnSc4	Miloš
Zemana	Zeman	k1gMnSc4	Zeman
podle	podle	k7c2	podle
svých	svůj	k3xOyFgNnPc2	svůj
slov	slovo	k1gNnPc2	slovo
voliči	volič	k1gMnPc7	volič
sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
a	a	k8xC	a
komunistů	komunista	k1gMnPc2	komunista
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
důchodovém	důchodový	k2eAgInSc6d1	důchodový
věku	věk	k1gInSc6	věk
a	a	k8xC	a
starší	starší	k1gMnSc1	starší
60	[number]	k4	60
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Kritičtější	kritický	k2eAgInSc1d2	kritičtější
pohled	pohled	k1gInSc1	pohled
panoval	panovat	k5eAaImAgInS	panovat
u	u	k7c2	u
voličů	volič	k1gMnPc2	volič
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
<g/>
,	,	kIx,	,
Strany	strana	k1gFnPc1	strana
zelených	zelené	k1gNnPc2	zelené
a	a	k8xC	a
lidovců	lidovec	k1gMnPc2	lidovec
<g/>
.	.	kIx.	.
</s>
<s>
Kladné	kladný	k2eAgFnPc1d1	kladná
reakce	reakce	k1gFnPc1	reakce
převládají	převládat	k5eAaImIp3nP	převládat
u	u	k7c2	u
obyvatel	obyvatel	k1gMnPc2	obyvatel
Jihomoravského	jihomoravský	k2eAgMnSc2d1	jihomoravský
a	a	k8xC	a
Moravskoslezského	moravskoslezský	k2eAgInSc2d1	moravskoslezský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
spíše	spíše	k9	spíše
negativní	negativní	k2eAgMnSc1d1	negativní
je	být	k5eAaImIp3nS	být
názor	názor	k1gInSc1	názor
obyvatel	obyvatel	k1gMnPc2	obyvatel
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
vyšším	vysoký	k2eAgNnSc7d2	vyšší
vzděláním	vzdělání	k1gNnSc7	vzdělání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
TNS	TNS	kA	TNS
Aisa	Aisa	k1gFnSc1	Aisa
zpracovala	zpracovat	k5eAaPmAgFnS	zpracovat
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
televizi	televize	k1gFnSc4	televize
průzkumy	průzkum	k1gInPc4	průzkum
hodnocení	hodnocení	k1gNnSc2	hodnocení
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2014	[number]	k4	2014
a	a	k8xC	a
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
sledovaných	sledovaný	k2eAgFnPc6d1	sledovaná
oblastech	oblast	k1gFnPc6	oblast
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
si	se	k3xPyFc3	se
Zeman	Zeman	k1gMnSc1	Zeman
mezi	mezi	k7c7	mezi
2014	[number]	k4	2014
a	a	k8xC	a
2017	[number]	k4	2017
polepšil	polepšit	k5eAaPmAgMnS	polepšit
<g/>
.	.	kIx.	.
</s>
<s>
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
jako	jako	k8xS	jako
prezidenta	prezident	k1gMnSc2	prezident
všech	všecek	k3xTgInPc2	všecek
občanů	občan	k1gMnPc2	občan
</s>
<s>
vnímalo	vnímat	k5eAaImAgNnS	vnímat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014 41	[number]	k4	2014 41
%	%	kIx~	%
dotázaných	dotázaný	k2eAgFnPc6d1	dotázaná
<g/>
,	,	kIx,	,
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
již	již	k9	již
většina	většina	k1gFnSc1	většina
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
52	[number]	k4	52
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
vhodně	vhodně	k6eAd1	vhodně
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
27	[number]	k4	27
%	%	kIx~	%
respondentů	respondent	k1gMnPc2	respondent
(	(	kIx(	(
<g/>
představu	představa	k1gFnSc4	představa
o	o	k7c6	o
tomto	tento	k3xDgNnSc6	tento
kritériu	kritérion	k1gNnSc6	kritérion
splnil	splnit	k5eAaPmAgMnS	splnit
u	u	k7c2	u
26	[number]	k4	26
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2017	[number]	k4	2017
zcela	zcela	k6eAd1	zcela
nebo	nebo	k8xC	nebo
spíše	spíše	k9	spíše
souhlasilo	souhlasit	k5eAaImAgNnS	souhlasit
47	[number]	k4	47
%	%	kIx~	%
(	(	kIx(	(
<g/>
představu	představa	k1gFnSc4	představa
splnil	splnit	k5eAaPmAgMnS	splnit
u	u	k7c2	u
45	[number]	k4	45
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentův	prezidentův	k2eAgInSc4d1	prezidentův
závazek	závazek	k1gInSc4	závazek
sjednocovat	sjednocovat	k5eAaImF	sjednocovat
cítilo	cítit	k5eAaImAgNnS	cítit
splněný	splněný	k2eAgInSc4d1	splněný
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
průzkumu	průzkum	k1gInSc6	průzkum
27	[number]	k4	27
%	%	kIx~	%
dotázaných	dotázaný	k2eAgFnPc2d1	dotázaná
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhém	druhý	k4xOgMnSc6	druhý
již	již	k6eAd1	již
43	[number]	k4	43
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Dobrý	dobrý	k2eAgInSc1d1	dobrý
obraz	obraz	k1gInSc1	obraz
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
zprostředkovával	zprostředkovávat	k5eAaImAgInS	zprostředkovávat
nejprve	nejprve	k6eAd1	nejprve
pro	pro	k7c4	pro
23	[number]	k4	23
%	%	kIx~	%
<g/>
,	,	kIx,	,
napodruhé	napodruhé	k6eAd1	napodruhé
pro	pro	k7c4	pro
42	[number]	k4	42
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Zemanovy	Zemanův	k2eAgInPc1d1	Zemanův
zásahy	zásah	k1gInPc1	zásah
do	do	k7c2	do
legislativního	legislativní	k2eAgInSc2d1	legislativní
procesu	proces	k1gInSc2	proces
našly	najít	k5eAaPmAgFnP	najít
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2014	[number]	k4	2014
úplný	úplný	k2eAgInSc4d1	úplný
nebo	nebo	k8xC	nebo
částečný	částečný	k2eAgInSc4d1	částečný
souhlas	souhlas	k1gInSc4	souhlas
u	u	k7c2	u
40	[number]	k4	40
%	%	kIx~	%
respondentů	respondent	k1gMnPc2	respondent
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
tento	tento	k3xDgInSc4	tento
podíl	podíl	k1gInSc4	podíl
na	na	k7c4	na
58	[number]	k4	58
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
Zemanovými	Zemanův	k2eAgInPc7d1	Zemanův
postoji	postoj	k1gInPc7	postoj
k	k	k7c3	k
Rusku	Rusko	k1gNnSc3	Rusko
během	během	k7c2	během
prvního	první	k4xOgNnSc2	první
dotazování	dotazování	k1gNnSc2	dotazování
souhlasilo	souhlasit	k5eAaImAgNnS	souhlasit
28	[number]	k4	28
%	%	kIx~	%
<g/>
,	,	kIx,	,
při	při	k7c6	při
druhém	druhý	k4xOgInSc6	druhý
průzkumu	průzkum	k1gInSc6	průzkum
odpovědělo	odpovědět	k5eAaPmAgNnS	odpovědět
zcela	zcela	k6eAd1	zcela
nebo	nebo	k8xC	nebo
částečně	částečně	k6eAd1	částečně
kladně	kladně	k6eAd1	kladně
41	[number]	k4	41
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
agentury	agentura	k1gFnSc2	agentura
CVVM	CVVM	kA	CVVM
z	z	k7c2	z
ledna	leden	k1gInSc2	leden
2016	[number]	k4	2016
byl	být	k5eAaImAgInS	být
nejdůvěryhodnějším	důvěryhodný	k2eAgNnSc7d3	nejdůvěryhodnější
českým	český	k2eAgNnSc7d1	české
politikem	politikum	k1gNnSc7	politikum
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
předstihl	předstihnout	k5eAaPmAgMnS	předstihnout
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nejoblíbenějšího	oblíbený	k2eAgMnSc4d3	nejoblíbenější
Andreje	Andrej	k1gMnSc4	Andrej
Babiše	Babiš	k1gMnSc4	Babiš
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2016	[number]	k4	2016
podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
agentury	agentura	k1gFnSc2	agentura
SANEP	SANEP	kA	SANEP
hodnotilo	hodnotit	k5eAaImAgNnS	hodnotit
Zemana	Zeman	k1gMnSc4	Zeman
kladně	kladně	k6eAd1	kladně
70,2	[number]	k4	70,2
%	%	kIx~	%
dotázaných	dotázaný	k2eAgMnPc2d1	dotázaný
obyvatel	obyvatel	k1gMnPc2	obyvatel
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
roku	rok	k1gInSc2	rok
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
popularita	popularita	k1gFnSc1	popularita
klesla	klesnout	k5eAaPmAgFnS	klesnout
o	o	k7c4	o
10	[number]	k4	10
procentních	procentní	k2eAgInPc2d1	procentní
bodů	bod	k1gInPc2	bod
na	na	k7c4	na
60	[number]	k4	60
%	%	kIx~	%
respondentů	respondent	k1gMnPc2	respondent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Hodnocení	hodnocení	k1gNnSc1	hodnocení
historiků	historik	k1gMnPc2	historik
<g/>
,	,	kIx,	,
politologů	politolog	k1gMnPc2	politolog
a	a	k8xC	a
právníků	právník	k1gMnPc2	právník
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2015	[number]	k4	2015
několik	několik	k4yIc4	několik
ukrajinistů	ukrajinista	k1gMnPc2	ukrajinista
a	a	k8xC	a
historiků	historik	k1gMnPc2	historik
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
historika	historik	k1gMnSc2	historik
Davida	David	k1gMnSc2	David
Svobody	Svoboda	k1gMnSc2	Svoboda
z	z	k7c2	z
Ústavu	ústav	k1gInSc2	ústav
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
totalitních	totalitní	k2eAgInPc2d1	totalitní
režimů	režim	k1gInPc2	režim
<g/>
,	,	kIx,	,
ukrajinistky	ukrajinistka	k1gFnSc2	ukrajinistka
a	a	k8xC	a
analytičky	analytička	k1gFnSc2	analytička
think-tanku	thinkanka	k1gFnSc4	think-tanka
Evropské	evropský	k2eAgFnSc2d1	Evropská
hodnoty	hodnota	k1gFnSc2	hodnota
Lenky	Lenka	k1gFnSc2	Lenka
Víchové	Víchová	k1gFnSc2	Víchová
nebo	nebo	k8xC	nebo
politického	politický	k2eAgMnSc2d1	politický
geografa	geograf	k1gMnSc2	geograf
Michaela	Michael	k1gMnSc2	Michael
Romancova	Romancův	k2eAgFnSc1d1	Romancova
<g/>
,	,	kIx,	,
v	v	k7c6	v
otevřeném	otevřený	k2eAgInSc6d1	otevřený
dopise	dopis	k1gInSc6	dopis
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
Zemanovu	Zemanův	k2eAgFnSc4d1	Zemanova
kritiku	kritika	k1gFnSc4	kritika
vůdců	vůdce	k1gMnPc2	vůdce
Organizace	organizace	k1gFnSc2	organizace
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
nacionalistů	nacionalista	k1gMnPc2	nacionalista
(	(	kIx(	(
<g/>
OUN	OUN	kA	OUN
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
Ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
povstalecké	povstalecký	k2eAgFnSc2d1	povstalecká
armády	armáda	k1gFnSc2	armáda
(	(	kIx(	(
<g/>
UPA	UPA	kA	UPA
<g/>
)	)	kIx)	)
Romana	Roman	k1gMnSc2	Roman
Šuchevyče	Šuchevyč	k1gInSc2	Šuchevyč
a	a	k8xC	a
Stepana	Stepana	k1gFnSc1	Stepana
Bandery	Bandera	k1gFnSc2	Bandera
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
druhého	druhý	k4xOgNnSc2	druhý
volebního	volební	k2eAgNnSc2d1	volební
období	období	k1gNnSc2	období
a	a	k8xC	a
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
vydáním	vydání	k1gNnSc7	vydání
údajného	údajný	k2eAgMnSc2d1	údajný
ruského	ruský	k2eAgMnSc2d1	ruský
hackera	hacker	k1gMnSc2	hacker
Nikulina	Nikulin	k2eAgMnSc2d1	Nikulin
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
Petr	Petr	k1gMnSc1	Petr
Kolář	Kolář	k1gMnSc1	Kolář
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
i	i	k8xC	i
USA	USA	kA	USA
<g/>
,	,	kIx,	,
o	o	k7c6	o
působení	působení	k1gNnSc6	působení
Miloše	Miloš	k1gMnSc2	Miloš
<g />
.	.	kIx.	.
</s>
<s>
Zemana	Zeman	k1gMnSc2	Zeman
<g/>
,	,	kIx,	,
že	že	k8xS	že
jako	jako	k8xC	jako
prezident	prezident	k1gMnSc1	prezident
země	zem	k1gFnSc2	zem
nehájí	hájit	k5eNaImIp3nS	hájit
zájmy	zájem	k1gInPc4	zájem
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
jako	jako	k8xC	jako
člena	člen	k1gMnSc4	člen
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
postupuje	postupovat	k5eAaImIp3nS	postupovat
proti	proti	k7c3	proti
našim	náš	k3xOp1gInPc3	náš
zájmům	zájem	k1gInPc3	zájem
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
a	a	k8xC	a
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
pochyb	pochyba	k1gFnPc2	pochyba
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hájí	hájit	k5eAaImIp3nP	hájit
zájmy	zájem	k1gInPc1	zájem
ruské	ruský	k2eAgInPc1d1	ruský
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
pak	pak	k6eAd1	pak
čínské	čínský	k2eAgFnPc1d1	čínská
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
vládní	vládní	k2eAgFnSc7d1	vládní
krizí	krize	k1gFnSc7	krize
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Zeman	Zeman	k1gMnSc1	Zeman
způsobil	způsobit	k5eAaPmAgMnS	způsobit
svou	svůj	k3xOyFgFnSc7	svůj
neochotou	neochota	k1gFnSc7	neochota
odvolat	odvolat	k5eAaPmF	odvolat
ministra	ministr	k1gMnSc4	ministr
kultury	kultura	k1gFnSc2	kultura
Antonína	Antonín	k1gMnSc4	Antonín
Staňka	Staněk	k1gMnSc4	Staněk
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
ministr	ministr	k1gMnSc1	ministr
podal	podat	k5eAaPmAgMnS	podat
demisi	demise	k1gFnSc4	demise
a	a	k8xC	a
premiér	premiér	k1gMnSc1	premiér
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
odvolání	odvolání	k1gNnSc4	odvolání
<g/>
,	,	kIx,	,
sílí	sílet	k5eAaImIp3nS	sílet
pochyby	pochyba	k1gFnPc4	pochyba
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
přímá	přímý	k2eAgFnSc1d1	přímá
volba	volba	k1gFnSc1	volba
prezidenta	prezident	k1gMnSc2	prezident
byla	být	k5eAaImAgFnS	být
správným	správný	k2eAgNnSc7d1	správné
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
Profesor	profesor	k1gMnSc1	profesor
ústavního	ústavní	k2eAgNnSc2d1	ústavní
práva	právo	k1gNnSc2	právo
Jan	Jan	k1gMnSc1	Jan
Kysela	Kysela	k1gMnSc1	Kysela
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
Miloše	Miloš	k1gMnSc4	Miloš
Zemana	Zeman	k1gMnSc4	Zeman
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
podána	podat	k5eAaPmNgFnS	podat
kompetenční	kompetenční	k2eAgFnSc1d1	kompetenční
žaloba	žaloba	k1gFnSc1	žaloba
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
ústavní	ústavní	k2eAgFnSc1d1	ústavní
žaloba	žaloba	k1gFnSc1	žaloba
pro	pro	k7c4	pro
hrubé	hrubý	k2eAgNnSc4d1	hrubé
porušování	porušování	k1gNnSc4	porušování
Ústavy	ústava	k1gFnSc2	ústava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Spolupracovníci	spolupracovník	k1gMnPc5	spolupracovník
===	===	k?	===
</s>
</p>
<p>
<s>
Politické	politický	k2eAgNnSc1d1	politické
působení	působení	k1gNnSc1	působení
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
je	být	k5eAaImIp3nS	být
spojováno	spojován	k2eAgNnSc1d1	spojováno
s	s	k7c7	s
několika	několik	k4yIc7	několik
aférami	aféra	k1gFnPc7	aféra
<g/>
.	.	kIx.	.
</s>
<s>
Kritizován	kritizovat	k5eAaImNgInS	kritizovat
byl	být	k5eAaImAgInS	být
i	i	k9	i
za	za	k7c4	za
výběr	výběr	k1gInSc4	výběr
nejbližších	blízký	k2eAgMnPc2d3	nejbližší
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
šéfa	šéf	k1gMnSc4	šéf
svých	svůj	k3xOyFgMnPc2	svůj
poradců	poradce	k1gMnPc2	poradce
Miroslava	Miroslav	k1gMnSc2	Miroslav
Šloufa	Šlouf	k1gMnSc2	Šlouf
<g/>
,	,	kIx,	,
politického	politický	k2eAgMnSc4d1	politický
představitele	představitel	k1gMnSc4	představitel
předlistopadového	předlistopadový	k2eAgInSc2d1	předlistopadový
SSM	SSM	kA	SSM
i	i	k8xC	i
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
jednal	jednat	k5eAaImAgInS	jednat
tajně	tajně	k6eAd1	tajně
s	s	k7c7	s
podnikatelem	podnikatel	k1gMnSc7	podnikatel
Janem	Jan	k1gMnSc7	Jan
Vízkem	Vízek	k1gMnSc7	Vízek
(	(	kIx(	(
<g/>
podrobněji	podrobně	k6eAd2	podrobně
Aféra	aféra	k1gFnSc1	aféra
Bamberg	Bamberg	k1gMnSc1	Bamberg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
zase	zase	k9	zase
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
vydírání	vydírání	k1gNnSc1	vydírání
správce	správce	k1gMnSc2	správce
Štiřínského	Štiřínský	k2eAgInSc2d1	Štiřínský
zámku	zámek	k1gInSc2	zámek
Václava	Václava	k1gFnSc1	Václava
Hrubého	Hrubého	k2eAgMnSc7d1	Hrubého
Zemanovým	Zemanův	k2eAgMnSc7d1	Zemanův
poradcem	poradce	k1gMnSc7	poradce
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Novotným	Novotný	k1gMnSc7	Novotný
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
po	po	k7c6	po
Hrubém	hrubý	k2eAgInSc6d1	hrubý
požadoval	požadovat	k5eAaImAgMnS	požadovat
vyrobení	vyrobení	k1gNnSc3	vyrobení
falešných	falešný	k2eAgMnPc2d1	falešný
důkazů	důkaz	k1gInPc2	důkaz
na	na	k7c4	na
bývalého	bývalý	k2eAgMnSc4d1	bývalý
ministra	ministr	k1gMnSc4	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Zielence	Zielence	k1gFnSc2	Zielence
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Štiřín	Štiřín	k1gInSc4	Štiřín
bylo	být	k5eAaImAgNnS	být
funkční	funkční	k2eAgNnSc1d1	funkční
odposlechové	odposlechový	k2eAgNnSc1d1	odposlechové
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2000	[number]	k4	2000
bylo	být	k5eAaImAgNnS	být
odhaleno	odhalen	k2eAgNnSc1d1	odhaleno
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
Zemanových	Zemanových	k2eAgMnPc2d1	Zemanových
poradců	poradce	k1gMnPc2	poradce
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
materiál	materiál	k1gInSc1	materiál
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
Olovo	olovo	k1gNnSc4	olovo
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
zdiskreditovat	zdiskreditovat	k5eAaPmF	zdiskreditovat
ministryni	ministryně	k1gFnSc4	ministryně
školství	školství	k1gNnSc2	školství
a	a	k8xC	a
Zemanovu	Zemanův	k2eAgFnSc4d1	Zemanova
stranickou	stranický	k2eAgFnSc4d1	stranická
kolegyni	kolegyně	k1gFnSc4	kolegyně
Petru	Petra	k1gFnSc4	Petra
Buzkovou	Buzková	k1gFnSc4	Buzková
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	on	k3xPp3gNnSc2	on
působení	působení	k1gNnSc2	působení
byla	být	k5eAaImAgNnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
Opoziční	opoziční	k2eAgFnSc1d1	opoziční
smlouva	smlouva	k1gFnSc1	smlouva
s	s	k7c7	s
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
za	za	k7c2	za
jejíhož	jejíž	k3xOyRp3gNnSc2	jejíž
trvání	trvání	k1gNnSc2	trvání
došlo	dojít	k5eAaPmAgNnS	dojít
podle	podle	k7c2	podle
dokumentárního	dokumentární	k2eAgInSc2d1	dokumentární
filmu	film	k1gInSc2	film
Vládneme	vládnout	k5eAaImIp1nP	vládnout
<g/>
,	,	kIx,	,
nerušit	rušit	k5eNaImF	rušit
(	(	kIx(	(
<g/>
natočeného	natočený	k2eAgInSc2d1	natočený
dle	dle	k7c2	dle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
knihy	kniha	k1gFnSc2	kniha
Erika	Erik	k1gMnSc2	Erik
Taberyho	Tabery	k1gMnSc2	Tabery
<g/>
)	)	kIx)	)
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
politické	politický	k2eAgFnSc2d1	politická
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
zvýšení	zvýšení	k1gNnSc1	zvýšení
míry	míra	k1gFnSc2	míra
korupce	korupce	k1gFnSc2	korupce
<g/>
,	,	kIx,	,
k	k	k7c3	k
výhrůžkám	výhrůžka	k1gFnPc3	výhrůžka
a	a	k8xC	a
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
vraždu	vražda	k1gFnSc4	vražda
novinářky	novinářka	k1gFnSc2	novinářka
Sabiny	Sabina	k1gFnSc2	Sabina
Slonkové	Slonková	k1gFnSc2	Slonková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velké	velký	k2eAgFnPc1d1	velká
emoce	emoce	k1gFnPc1	emoce
po	po	k7c6	po
celé	celá	k1gFnSc6	celá
jeho	on	k3xPp3gNnSc2	on
působení	působení	k1gNnSc2	působení
vzbuzují	vzbuzovat	k5eAaImIp3nP	vzbuzovat
komentáře	komentář	k1gInPc1	komentář
Zemanova	Zemanův	k2eAgMnSc2d1	Zemanův
mluvčího	mluvčí	k1gMnSc2	mluvčí
Jiřího	Jiří	k1gMnSc2	Jiří
Ovčáčka	ovčáček	k1gMnSc2	ovčáček
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
politicky	politicky	k6eAd1	politicky
nekorektního	korektní	k2eNgInSc2d1	nekorektní
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
negativně	negativně	k6eAd1	negativně
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
o	o	k7c6	o
kriticích	kritik	k1gMnPc6	kritik
prezidenta	prezident	k1gMnSc2	prezident
Zemana	Zeman	k1gMnSc4	Zeman
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vedle	vedle	k7c2	vedle
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Sobotky	Sobotka	k1gMnSc2	Sobotka
<g/>
,	,	kIx,	,
Miroslava	Miroslav	k1gMnSc2	Miroslav
Kalouska	Kalousek	k1gMnSc2	Kalousek
a	a	k8xC	a
TOP09	TOP09	k1gMnSc1	TOP09
má	mít	k5eAaImIp3nS	mít
dominovat	dominovat	k5eAaImF	dominovat
především	především	k9	především
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
pražská	pražský	k2eAgFnSc1d1	Pražská
kavárna	kavárna	k1gFnSc1	kavárna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
"	"	kIx"	"
<g/>
praktiky	praktika	k1gFnPc4	praktika
<g/>
"	"	kIx"	"
přirovnává	přirovnávat	k5eAaImIp3nS	přirovnávat
k	k	k7c3	k
druhé	druhý	k4xOgFnSc3	druhý
republice	republika	k1gFnSc3	republika
<g/>
,	,	kIx,	,
působení	působení	k1gNnSc4	působení
fašistů	fašista	k1gMnPc2	fašista
i	i	k8xC	i
k	k	k7c3	k
50.	[number]	k4	50.
letům	léto	k1gNnPc3	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ovčáčkovi	ovčáček	k1gMnSc3	ovčáček
je	být	k5eAaImIp3nS	být
vytýkáno	vytýkán	k2eAgNnSc1d1	vytýkáno
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
úplně	úplně	k6eAd1	úplně
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
v	v	k7c6	v
jaké	jaký	k3yIgFnSc6	jaký
chvíli	chvíle	k1gFnSc6	chvíle
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
názory	názor	k1gInPc4	názor
Miloše	Miloš	k1gMnSc4	Miloš
Zemana	Zeman	k1gMnSc4	Zeman
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
ty	ty	k3xPp2nSc1	ty
své	svůj	k3xOyFgNnSc4	svůj
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Moje	můj	k3xOp1gFnSc1	můj
úloha	úloha	k1gFnSc1	úloha
je	být	k5eAaImIp3nS	být
trojjediná	trojjediný	k2eAgFnSc1d1	trojjediná
<g/>
.	.	kIx.	.
</s>
<s>
Nejenom	nejenom	k6eAd1	nejenom
že	že	k8xS	že
prezentuji	prezentovat	k5eAaBmIp1nS	prezentovat
stanoviska	stanovisko	k1gNnPc4	stanovisko
pana	pan	k1gMnSc2	pan
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
bráním	bránit	k5eAaImIp1nS	bránit
jeho	jeho	k3xOp3gNnSc2	jeho
stanoviska	stanovisko	k1gNnSc2	stanovisko
a	a	k8xC	a
bráním	bránit	k5eAaImIp1nS	bránit
i	i	k9	i
osobu	osoba	k1gFnSc4	osoba
pana	pan	k1gMnSc2	pan
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Vím	vědět	k5eAaImIp1nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
nelíbí	líbit	k5eNaImIp3nS	líbit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
budu	být	k5eAaImBp1nS	být
to	ten	k3xDgNnSc1	ten
dělat	dělat	k5eAaImF	dělat
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
uvedl	uvést	k5eAaPmAgMnS	uvést
Ovčáček	ovčáček	k1gMnSc1	ovčáček
<g/>
.	.	kIx.	.
</s>
<s>
Nejasnosti	nejasnost	k1gFnPc1	nejasnost
panovaly	panovat	k5eAaImAgFnP	panovat
kolem	kolem	k7c2	kolem
twitterového	twitterový	k2eAgInSc2d1	twitterový
účtu	účet	k1gInSc2	účet
@	@	kIx~	@
<g/>
PREZIDENTmluvci	PREZIDENTmluvek	k1gMnPc1	PREZIDENTmluvek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
mnozí	mnohý	k2eAgMnPc1d1	mnohý
lidé	člověk	k1gMnPc1	člověk
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
oficiální	oficiální	k2eAgInSc4d1	oficiální
kanál	kanál	k1gInSc4	kanál
Hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Kancelář	kancelář	k1gFnSc1	kancelář
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
nicméně	nicméně	k8xC	nicméně
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
"	"	kIx"	"
<g/>
soukromý	soukromý	k2eAgInSc4d1	soukromý
profil	profil	k1gInSc4	profil
Jiřího	Jiří	k1gMnSc2	Jiří
Ovčáčka	ovčáček	k1gMnSc2	ovčáček
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Ovčáček	ovčáček	k1gMnSc1	ovčáček
nedostává	dostávat	k5eNaImIp3nS	dostávat
za	za	k7c4	za
vedení	vedení	k1gNnSc4	vedení
účtu	účet	k1gInSc2	účet
žádnou	žádný	k3yNgFnSc4	žádný
odměnu	odměna	k1gFnSc4	odměna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
na	na	k7c4	na
Hrad	hrad	k1gInSc4	hrad
po	po	k7c6	po
prezidentské	prezidentský	k2eAgFnSc6d1	prezidentská
volbě	volba	k1gFnSc6	volba
přivede	přivést	k5eAaPmIp3nS	přivést
pouze	pouze	k6eAd1	pouze
tajemníka	tajemník	k1gMnSc4	tajemník
a	a	k8xC	a
šoféra	šofér	k1gMnSc4	šofér
<g/>
,	,	kIx,	,
objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gMnPc7	jeho
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
jména	jméno	k1gNnPc1	jméno
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
minulosti	minulost	k1gFnSc2	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
zaměstnal	zaměstnat	k5eAaPmAgMnS	zaměstnat
jako	jako	k9	jako
vedoucího	vedoucí	k1gMnSc4	vedoucí
analytického	analytický	k2eAgInSc2d1	analytický
odboru	odbor	k1gInSc2	odbor
Radka	Radek	k1gMnSc2	Radek
Augustina	Augustin	k1gMnSc2	Augustin
<g/>
,	,	kIx,	,
bývalého	bývalý	k2eAgMnSc4d1	bývalý
místopředsedu	místopředseda	k1gMnSc4	místopředseda
Strany	strana	k1gFnSc2	strana
práv	právo	k1gNnPc2	právo
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
ředitele	ředitel	k1gMnSc2	ředitel
sekretariátu	sekretariát	k1gInSc2	sekretariát
zvolil	zvolit	k5eAaPmAgMnS	zvolit
Jaroslava	Jaroslav	k1gMnSc4	Jaroslav
Hlinovského	Hlinovský	k2eAgMnSc4d1	Hlinovský
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
během	během	k7c2	během
kampaně	kampaň	k1gFnSc2	kampaň
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
Zemanův	Zemanův	k2eAgMnSc1d1	Zemanův
tajemník	tajemník	k1gMnSc1	tajemník
<g/>
,	,	kIx,	,
za	za	k7c4	za
šéfku	šéfka	k1gFnSc4	šéfka
tiskového	tiskový	k2eAgInSc2d1	tiskový
odboru	odbor	k1gInSc2	odbor
si	se	k3xPyFc3	se
vybral	vybrat	k5eAaPmAgMnS	vybrat
Hanu	Hana	k1gFnSc4	Hana
Burianovou	Burianová	k1gFnSc4	Burianová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
mluvčí	mluvčí	k1gFnPc4	mluvčí
SPO	SPO	kA	SPO
i	i	k8xC	i
Zemanovy	Zemanův	k2eAgFnSc2d1	Zemanova
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
kampaně	kampaň	k1gFnSc2	kampaň
a	a	k8xC	a
jako	jako	k9	jako
poradce	poradce	k1gMnSc4	poradce
pro	pro	k7c4	pro
energetiku	energetika	k1gFnSc4	energetika
Martina	Martin	k1gMnSc4	Martin
Nejedlého	Nejedlý	k1gMnSc4	Nejedlý
<g/>
,	,	kIx,	,
místopředsedu	místopředseda	k1gMnSc4	místopředseda
SPO	SPO	kA	SPO
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Nejedlého	Nejedlý	k1gMnSc2	Nejedlý
je	být	k5eAaImIp3nS	být
kritizováno	kritizovat	k5eAaImNgNnS	kritizovat
jeho	jeho	k3xOp3gNnSc1	jeho
nejasné	jasný	k2eNgNnSc1d1	nejasné
postavení	postavení	k1gNnSc1	postavení
na	na	k7c6	na
Hradě	hrad	k1gInSc6	hrad
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
minulost	minulost	k1gFnSc1	minulost
<g/>
,	,	kIx,	,
kritizovány	kritizován	k2eAgFnPc1d1	kritizována
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
jeho	jeho	k3xOp3gFnPc1	jeho
možné	možný	k2eAgFnPc1d1	možná
vazby	vazba	k1gFnPc1	vazba
na	na	k7c4	na
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Kritice	kritika	k1gFnSc3	kritika
čelí	čelit	k5eAaImIp3nS	čelit
i	i	k9	i
kancléř	kancléř	k1gMnSc1	kancléř
Vratislav	Vratislav	k1gMnSc1	Vratislav
Mynář	Mynář	k1gMnSc1	Mynář
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nezískal	získat	k5eNaPmAgMnS	získat
bezpečnostní	bezpečnostní	k2eAgFnSc4d1	bezpečnostní
prověrku	prověrka	k1gFnSc4	prověrka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
podmínil	podmínit	k5eAaPmAgMnS	podmínit
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
jeho	jeho	k3xOp3gNnSc4	jeho
působení	působení	k1gNnSc4	působení
na	na	k7c6	na
Hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Vratislav	Vratislav	k1gMnSc1	Vratislav
Mynář	Mynář	k1gMnSc1	Mynář
si	se	k3xPyFc3	se
především	především	k9	především
bude	být	k5eAaImBp3nS	být
muset	muset	k5eAaImF	muset
urychleně	urychleně	k6eAd1	urychleně
udělat	udělat	k5eAaPmF	udělat
prověrku	prověrka	k1gFnSc4	prověrka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
conditio	conditio	k6eAd1	conditio
sine	sinus	k1gInSc5	sinus
qua	qua	k?	qua
non	non	k?	non
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Nastupují	nastupovat	k5eAaImIp3nP	nastupovat
dvě	dva	k4xCgFnPc4	dva
možnosti	možnost	k1gFnPc4	možnost
–	–	k?	–
buď	buď	k8xC	buď
si	se	k3xPyFc3	se
tu	ten	k3xDgFnSc4	ten
prověrku	prověrka	k1gFnSc4	prověrka
dodělá	dodělat	k5eAaPmIp3nS	dodělat
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
dostane	dostat	k5eAaPmIp3nS	dostat
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
zůstane	zůstat	k5eAaPmIp3nS	zůstat
kancléřem	kancléř	k1gMnSc7	kancléř
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
nedodělá	dodělat	k5eNaPmIp3nS	dodělat
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
přestane	přestat	k5eAaPmIp3nS	přestat
být	být	k5eAaImF	být
kancléřem	kancléř	k1gMnSc7	kancléř
a	a	k8xC	a
já	já	k3xPp1nSc1	já
si	se	k3xPyFc3	se
budu	být	k5eAaImBp1nS	být
shánět	shánět	k5eAaImF	shánět
kancléře	kancléř	k1gMnSc4	kancléř
nového	nový	k2eAgMnSc4d1	nový
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2016	[number]	k4	2016
byla	být	k5eAaImAgFnS	být
zveřejněna	zveřejněn	k2eAgFnSc1d1	zveřejněna
informace	informace	k1gFnSc1	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
kancléř	kancléř	k1gMnSc1	kancléř
nechává	nechávat	k5eAaImIp3nS	nechávat
vzdávat	vzdávat	k5eAaImF	vzdávat
poctu	pocta	k1gFnSc4	pocta
Hradní	hradní	k2eAgFnSc7d1	hradní
stráží	stráž	k1gFnSc7	stráž
a	a	k8xC	a
vydává	vydávat	k5eAaPmIp3nS	vydávat
zmatečné	zmatečný	k2eAgInPc4d1	zmatečný
a	a	k8xC	a
nelogické	logický	k2eNgInPc4d1	nelogický
příkazy	příkaz	k1gInPc4	příkaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Vyjadřování	vyjadřování	k1gNnSc2	vyjadřování
===	===	k?	===
</s>
</p>
<p>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
se	se	k3xPyFc4	se
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
politického	politický	k2eAgNnSc2d1	politické
působení	působení	k1gNnSc2	působení
dopustil	dopustit	k5eAaPmAgInS	dopustit
hned	hned	k6eAd1	hned
několika	několik	k4yIc2	několik
lživých	lživý	k2eAgInPc2d1	lživý
výroků	výrok	k1gInPc2	výrok
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterých	který	k3yRgMnPc6	který
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
pravomocným	pravomocný	k2eAgNnSc7d1	pravomocné
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
soudu	soud	k1gInSc2	soud
uložena	uložen	k2eAgFnSc1d1	uložena
omluva	omluva	k1gFnSc1	omluva
a	a	k8xC	a
pokuta	pokuta	k1gFnSc1	pokuta
<g/>
.	.	kIx.	.
<g/>
Milanu	Milan	k1gMnSc3	Milan
Hruškovi	Hruška	k1gMnSc3	Hruška
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
zaplatil	zaplatit	k5eAaPmAgMnS	zaplatit
20 000	[number]	k4	20 000
Kč	Kč	kA	Kč
za	za	k7c4	za
výrok	výrok	k1gInSc4	výrok
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hruška	Hruška	k1gMnSc1	Hruška
měl	mít	k5eAaImAgMnS	mít
"	"	kIx"	"
<g/>
s	s	k7c7	s
jistými	jistý	k2eAgFnPc7d1	jistá
obtížemi	obtíž	k1gFnPc7	obtíž
absolvovat	absolvovat	k5eAaPmF	absolvovat
<g />
.	.	kIx.	.
</s>
<s>
sedm	sedm	k4xCc1	sedm
tříd	třída	k1gFnPc2	třída
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1998	[number]	k4	1998
Městský	městský	k2eAgInSc1d1	městský
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Zeman	Zeman	k1gMnSc1	Zeman
musí	muset	k5eAaImIp3nS	muset
omluvit	omluvit	k5eAaPmF	omluvit
někdejšímu	někdejší	k2eAgMnSc3d1	někdejší
spolustraníkovi	spolustraník	k1gMnSc3	spolustraník
Jozefu	Jozef	k1gMnSc3	Jozef
Wagnerovi	Wagner	k1gMnSc3	Wagner
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yIgNnSc6	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
po	po	k7c4	po
vyloučení	vyloučení	k1gNnSc4	vyloučení
z	z	k7c2	z
ČSSD	ČSSD	kA	ČSSD
nechtěli	chtít	k5eNaImAgMnP	chtít
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
řad	řada	k1gFnPc2	řada
přijmout	přijmout	k5eAaPmF	přijmout
ani	ani	k8xC	ani
komunističtí	komunistický	k2eAgMnPc1d1	komunistický
poslanci	poslanec	k1gMnPc1	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
ignorování	ignorování	k1gNnSc4	ignorování
rozsudku	rozsudek	k1gInSc2	rozsudek
dostal	dostat	k5eAaPmAgMnS	dostat
Zeman	Zeman	k1gMnSc1	Zeman
pokutu	pokuta	k1gFnSc4	pokuta
20 000	[number]	k4	20 000
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
za	za	k7c4	za
výrok	výrok	k1gInSc4	výrok
se	se	k3xPyFc4	se
omluvil	omluvit	k5eAaPmAgMnS	omluvit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
omlouval	omlouvat	k5eAaImAgMnS	omlouvat
i	i	k8xC	i
Miroslavu	Miroslava	k1gFnSc4	Miroslava
Mackovi	Mackův	k2eAgMnPc1d1	Mackův
za	za	k7c4	za
výroky	výrok	k1gInPc4	výrok
z	z	k7c2	z
pořadu	pořad	k1gInSc2	pořad
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
7	[number]	k4	7
čili	čili	k8xC	čili
sedm	sedm	k4xCc4	sedm
dní	den	k1gInPc2	den
z	z	k7c2	z
dubna	duben	k1gInSc2	duben
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
kde	kde	k6eAd1	kde
označil	označit	k5eAaPmAgMnS	označit
Mackovu	Mackův	k2eAgFnSc4d1	Mackova
privatizaci	privatizace	k1gFnSc4	privatizace
Knižního	knižní	k2eAgInSc2d1	knižní
velkoobchodu	velkoobchod	k1gInSc2	velkoobchod
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
za	za	k7c4	za
"	"	kIx"	"
<g/>
jednoznačné	jednoznačný	k2eAgNnSc4d1	jednoznačné
tunelování	tunelování	k1gNnSc4	tunelování
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
obvinil	obvinit	k5eAaPmAgMnS	obvinit
bývalého	bývalý	k2eAgMnSc2d1	bývalý
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Josefa	Josef	k1gMnSc2	Josef
Zielenience	Zielenienec	k1gMnSc2	Zielenienec
z	z	k7c2	z
uzavírání	uzavírání	k1gNnSc2	uzavírání
smluv	smlouva	k1gFnPc2	smlouva
na	na	k7c6	na
osobní	osobní	k2eAgFnSc6d1	osobní
prezentaci	prezentace	k1gFnSc6	prezentace
(	(	kIx(	(
<g/>
během	během	k7c2	během
Zeleniecova	Zeleniecův	k2eAgNnSc2d1	Zeleniecův
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
pak	pak	k6eAd1	pak
z	z	k7c2	z
uplácení	uplácení	k1gNnSc2	uplácení
novinářů	novinář	k1gMnPc2	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vycházel	vycházet	k5eAaImAgInS	vycházet
z	z	k7c2	z
nepodložených	podložený	k2eNgFnPc2d1	nepodložená
informací	informace	k1gFnPc2	informace
od	od	k7c2	od
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Jana	Jan	k1gMnSc2	Jan
Kavana	Kavan	k1gMnSc2	Kavan
<g/>
.	.	kIx.	.
</s>
<s>
Kavan	Kavan	k1gMnSc1	Kavan
se	se	k3xPyFc4	se
Zeleniecovi	Zelenieec	k1gMnSc3	Zelenieec
omluvil	omluvit	k5eAaPmAgMnS	omluvit
<g/>
,	,	kIx,	,
Zeman	Zeman	k1gMnSc1	Zeman
se	se	k3xPyFc4	se
k	k	k7c3	k
omluvě	omluva	k1gFnSc3	omluva
přidal	přidat	k5eAaPmAgMnS	přidat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
pravomocně	pravomocně	k6eAd1	pravomocně
odsouzen	odsouzet	k5eAaImNgInS	odsouzet
k	k	k7c3	k
omluvě	omluva	k1gFnSc3	omluva
a	a	k8xC	a
pokutě	pokuta	k1gFnSc6	pokuta
50 000	[number]	k4	50 000
Kč	Kč	kA	Kč
za	za	k7c4	za
lživé	lživý	k2eAgFnPc4d1	lživá
nařčení	nařčení	k1gNnPc4	nařčení
novináře	novinář	k1gMnSc2	novinář
Ivana	Ivan	k1gMnSc2	Ivan
Breziny	Brezina	k1gMnSc2	Brezina
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yRgInSc2	který
měl	mít	k5eAaImAgMnS	mít
Brezina	Brezina	k1gMnSc1	Brezina
psát	psát	k5eAaImF	psát
za	za	k7c4	za
úplatu	úplata	k1gFnSc4	úplata
články	článek	k1gInPc1	článek
podporující	podporující	k2eAgFnSc4d1	podporující
dostavbu	dostavba	k1gFnSc4	dostavba
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
<g />
.	.	kIx.	.
</s>
<s>
<g/>
<g/>
Soud	soud	k1gInSc1	soud
naopak	naopak	k6eAd1	naopak
nevyhověl	vyhovět	k5eNaPmAgInS	vyhovět
žalobě	žaloba	k1gFnSc3	žaloba
bývalého	bývalý	k2eAgMnSc2d1	bývalý
ředitele	ředitel	k1gMnSc2	ředitel
BIS	BIS	kA	BIS
Stanislava	Stanislav	k1gMnSc2	Stanislav
Devátého	devátý	k4xOgInSc2	devátý
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
Zeman	Zeman	k1gMnSc1	Zeman
omluvit	omluvit	k5eAaPmF	omluvit
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ČSSD	ČSSD	kA	ČSSD
zaplatit	zaplatit	k5eAaPmF	zaplatit
odškodné	odškodné	k1gNnSc4	odškodné
milión	milión	k4xCgInSc4	milión
korun	koruna	k1gFnPc2	koruna
za	za	k7c4	za
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
BIS	BIS	kA	BIS
a	a	k8xC	a
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
operativní	operativní	k2eAgFnSc4d1	operativní
skupinu	skupina	k1gFnSc4	skupina
sledující	sledující	k2eAgFnSc2d1	sledující
různé	různý	k2eAgFnSc2d1	různá
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
<g/>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
Zemanovi	Zeman	k1gMnSc3	Zeman
vytýkáno	vytýkán	k2eAgNnSc1d1	vytýkáno
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
vnesl	vnést	k5eAaPmAgMnS	vnést
nebývalou	bývalý	k2eNgFnSc4d1	bývalý
míru	míra	k1gFnSc4	míra
hrubosti	hrubost	k1gFnSc2	hrubost
a	a	k8xC	a
arogance	arogance	k1gFnSc2	arogance
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
arogantní	arogantní	k2eAgNnSc1d1	arogantní
jen	jen	k6eAd1	jen
vůči	vůči	k7c3	vůči
hlupákům	hlupák	k1gMnPc3	hlupák
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgFnPc1d1	známá
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
jeho	jeho	k3xOp3gFnPc1	jeho
četné	četný	k2eAgFnPc1d1	četná
urážky	urážka	k1gFnPc1	urážka
novinářů	novinář	k1gMnPc2	novinář
a	a	k8xC	a
politických	politický	k2eAgMnPc2d1	politický
protivníků	protivník	k1gMnPc2	protivník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
například	například	k6eAd1	například
druhou	druhý	k4xOgFnSc4	druhý
Klausovu	Klausův	k2eAgFnSc4d1	Klausova
vládu	vláda	k1gFnSc4	vláda
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
vládu	vláda	k1gFnSc4	vláda
zlodějů	zloděj	k1gMnPc2	zloděj
a	a	k8xC	a
tunelářů	tunelář	k1gMnPc2	tunelář
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
zase	zase	k9	zase
na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
novinářů	novinář	k1gMnPc2	novinář
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Za	za	k7c4	za
celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
nesetkal	setkat	k5eNaPmAgMnS	setkat
s	s	k7c7	s
blbějším	blbý	k2eAgNnSc7d2	blbější
a	a	k8xC	a
závistivějším	závistivý	k2eAgNnSc7d2	závistivý
stvořením	stvoření	k1gNnSc7	stvoření
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
novinář	novinář	k1gMnSc1	novinář
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
má	mít	k5eAaImIp3nS	mít
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
bonmoty	bonmot	k1gInPc4	bonmot
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
novináři	novinář	k1gMnPc1	novinář
nechápou	chápat	k5eNaImIp3nP	chápat
<g/>
.	.	kIx.	.
<g/>
Občas	občas	k6eAd1	občas
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
projevu	projev	k1gInSc6	projev
používá	používat	k5eAaImIp3nS	používat
vulgarismy	vulgarismus	k1gInPc4	vulgarismus
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
Lidové	lidový	k2eAgFnPc4d1	lidová
noviny	novina	k1gFnPc4	novina
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
A	a	k9	a
když	když	k8xS	když
se	se	k3xPyFc4	se
zeptáte	zeptat	k5eAaPmIp2nP	zeptat
Čecha	Čech	k1gMnSc4	Čech
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
kdyby	kdyby	k9	kdyby
minutu	minuta	k1gFnSc4	minuta
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
milion	milion	k4xCgInSc4	milion
v	v	k7c6	v
loterii	loterie	k1gFnSc6	loterie
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
vám	vy	k3xPp2nPc3	vy
odpoví	odpovědět	k5eAaPmIp3nS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
hovno	hovno	k1gNnSc4	hovno
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
Kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgInSc3	svůj
slovníku	slovník	k1gInSc3	slovník
si	se	k3xPyFc3	se
dle	dle	k7c2	dle
serveru	server	k1gInSc2	server
Novinky.cz	Novinky.cz	k1gMnSc1	Novinky.cz
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
přízvisko	přízvisko	k1gNnSc4	přízvisko
"	"	kIx"	"
<g/>
vulgární	vulgární	k2eAgMnSc1d1	vulgární
premiér	premiér	k1gMnSc1	premiér
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
předvolební	předvolební	k2eAgFnSc2d1	předvolební
debaty	debata	k1gFnSc2	debata
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
Prima	prima	k2eAgMnSc1d1	prima
Zeman	Zeman	k1gMnSc1	Zeman
pronesl	pronést	k5eAaPmAgMnS	pronést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Knížata	kníže	k1gNnPc1	kníže
měla	mít	k5eAaImAgNnP	mít
právo	právo	k1gNnSc4	právo
první	první	k4xOgFnSc2	první
noci	noc	k1gFnSc2	noc
a	a	k8xC	a
díky	díky	k7c3	díky
tomu	ten	k3xDgMnSc3	ten
zdegenerovala	zdegenerovat	k5eAaPmAgFnS	zdegenerovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
své	svůj	k3xOyFgFnPc4	svůj
nevolnice	nevolnice	k1gFnPc4	nevolnice
nemuseli	muset	k5eNaImAgMnP	muset
znásilňovat	znásilňovat	k5eAaImF	znásilňovat
<g/>
,	,	kIx,	,
nemuseli	muset	k5eNaImAgMnP	muset
tedy	tedy	k9	tedy
vydávat	vydávat	k5eAaPmF	vydávat
energii	energie	k1gFnSc4	energie
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
znásilňování	znásilňování	k1gNnSc4	znásilňování
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
my	my	k3xPp1nPc1	my
zemani	zeman	k1gMnPc1	zeman
jsme	být	k5eAaImIp1nP	být
si	se	k3xPyFc3	se
svého	svůj	k3xOyFgNnSc2	svůj
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
nejenom	nejenom	k6eAd1	nejenom
v	v	k7c6	v
sexuální	sexuální	k2eAgFnSc6d1	sexuální
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
vždycky	vždycky	k6eAd1	vždycky
toto	tento	k3xDgNnSc4	tento
právo	právo	k1gNnSc4	právo
museli	muset	k5eAaImAgMnP	muset
těžce	těžce	k6eAd1	těžce
vybojovat	vybojovat	k5eAaPmF	vybojovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jsme	být	k5eAaImIp1nP	být
nezdegenerovali	zdegenerovat	k5eNaPmAgMnP	zdegenerovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
pobouřilo	pobouřit	k5eAaPmAgNnS	pobouřit
množství	množství	k1gNnSc1	množství
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
tento	tento	k3xDgInSc4	tento
výrok	výrok	k1gInSc4	výrok
vnímali	vnímat	k5eAaImAgMnP	vnímat
jako	jako	k8xC	jako
zlehčování	zlehčování	k1gNnSc4	zlehčování
aktu	akt	k1gInSc2	akt
znásilnění	znásilnění	k1gNnPc2	znásilnění
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
říjnové	říjnový	k2eAgFnSc6d1	říjnová
návštěvě	návštěva	k1gFnSc6	návštěva
kutnohorské	kutnohorský	k2eAgFnSc2d1	Kutnohorská
tabákové	tabákový	k2eAgFnSc2d1	tabáková
firmy	firma	k1gFnSc2	firma
Phillip	Phillip	k1gMnSc1	Phillip
Morris	Morris	k1gFnSc1	Morris
během	během	k7c2	během
projevu	projev	k1gInSc2	projev
řekl	říct	k5eAaPmAgInS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Chtěl	chtít	k5eAaImAgMnS	chtít
bych	by	kYmCp1nS	by
vás	vy	k3xPp2nPc4	vy
upozornit	upozornit	k5eAaPmF	upozornit
<g/>
,	,	kIx,	,
že	že	k8xS	že
já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
začal	začít	k5eAaPmAgMnS	začít
kouřit	kouřit	k5eAaImF	kouřit
až	až	k9	až
ve	v	k7c6	v
27	[number]	k4	27
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
můj	můj	k3xOp1gInSc1	můj
organismus	organismus	k1gInSc1	organismus
plně	plně	k6eAd1	plně
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
a	a	k8xC	a
tabák	tabák	k1gInSc1	tabák
vůči	vůči	k7c3	vůči
němu	on	k3xPp3gMnSc3	on
nebyl	být	k5eNaImAgMnS	být
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
<g/>
.	.	kIx.	.
</s>
<s>
Dovolte	dovolit	k5eAaPmRp2nP	dovolit
mi	já	k3xPp1nSc3	já
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
vašim	váš	k3xOp2gMnPc3	váš
dětem	dítě	k1gFnPc3	dítě
doporučil	doporučit	k5eAaPmAgInS	doporučit
obdobný	obdobný	k2eAgInSc1d1	obdobný
postup	postup	k1gInSc1	postup
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
počkaly	počkat	k5eAaPmAgInP	počkat
do	do	k7c2	do
27	[number]	k4	27
let	léto	k1gNnPc2	léto
a	a	k8xC	a
potom	potom	k6eAd1	potom
kouřily	kouřit	k5eAaImAgInP	kouřit
zcela	zcela	k6eAd1	zcela
bez	bez	k7c2	bez
rizika	riziko	k1gNnSc2	riziko
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Česká	český	k2eAgFnSc1d1	Česká
pneumologická	pneumologický	k2eAgFnSc1d1	pneumologická
a	a	k8xC	a
ftizeologická	ftizeologický	k2eAgFnSc1d1	ftizeologická
společnost	společnost	k1gFnSc1	společnost
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
vydala	vydat	k5eAaPmAgFnS	vydat
stanovisko	stanovisko	k1gNnSc4	stanovisko
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
označila	označit	k5eAaPmAgFnS	označit
bagatelizování	bagatelizování	k1gNnSc3	bagatelizování
a	a	k8xC	a
ironizování	ironizování	k1gNnSc3	ironizování
problému	problém	k1gInSc2	problém
za	za	k7c4	za
"	"	kIx"	"
<g/>
nezodpovědný	zodpovědný	k2eNgInSc4d1	nezodpovědný
vzkaz	vzkaz	k1gInSc4	vzkaz
pro	pro	k7c4	pro
příští	příští	k2eAgFnPc4d1	příští
generace	generace	k1gFnPc4	generace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
že	že	k8xS	že
kouření	kouření	k1gNnSc1	kouření
po	po	k7c6	po
27	[number]	k4	27
letech	let	k1gInPc6	let
věku	věk	k1gInSc2	věk
neškodí	škodit	k5eNaImIp3nS	škodit
<g/>
,	,	kIx,	,
vyvrací	vyvracet	k5eAaImIp3nP	vyvracet
stovky	stovka	k1gFnPc1	stovka
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kouření	kouření	k1gNnSc1	kouření
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
život	život	k1gInSc4	život
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
věku	věk	k1gInSc6	věk
a	a	k8xC	a
poškozuje	poškozovat	k5eAaImIp3nS	poškozovat
zdraví	zdraví	k1gNnSc4	zdraví
především	především	k9	především
v	v	k7c6	v
době	doba	k1gFnSc6	doba
časné	časný	k2eAgFnSc2d1	časná
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
fixovat	fixovat	k5eAaImF	fixovat
experimentování	experimentování	k1gNnPc4	experimentování
s	s	k7c7	s
cigaretami	cigareta	k1gFnPc7	cigareta
u	u	k7c2	u
mladistvých	mladistvý	k2eAgMnPc2d1	mladistvý
a	a	k8xC	a
dospívajících	dospívající	k2eAgMnPc2d1	dospívající
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
zlozvyk	zlozvyk	k1gInSc1	zlozvyk
<g />
.	.	kIx.	.
</s>
<s>
přerůstá	přerůstat	k5eAaImIp3nS	přerůstat
v	v	k7c4	v
těžkou	těžký	k2eAgFnSc4d1	těžká
závislost	závislost	k1gFnSc4	závislost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2013	[number]	k4	2013
se	se	k3xPyFc4	se
Zeman	Zeman	k1gMnSc1	Zeman
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Bohuslavu	Bohuslav	k1gMnSc3	Bohuslav
Sobotkovi	Sobotka	k1gMnSc3	Sobotka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pouze	pouze	k6eAd1	pouze
ho	on	k3xPp3gMnSc4	on
upozorním	upozornit	k5eAaPmIp1nS	upozornit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
bych	by	kYmCp1nS	by
ho	on	k3xPp3gMnSc4	on
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
premiérem	premiér	k1gMnSc7	premiér
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
bude	být	k5eAaImBp3nS	být
muset	muset	k5eAaImF	muset
počkat	počkat	k5eAaPmF	počkat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
poněkud	poněkud	k6eAd1	poněkud
důstojnější	důstojný	k2eAgFnSc1d2	důstojnější
záležitost	záležitost	k1gFnSc1	záležitost
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
nemohu	moct	k5eNaImIp1nS	moct
řešit	řešit	k5eAaImF	řešit
z	z	k7c2	z
invalidního	invalidní	k2eAgInSc2d1	invalidní
vozíku	vozík	k1gInSc2	vozík
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
výrokem	výrok	k1gInSc7	výrok
však	však	k9	však
urazil	urazit	k5eAaPmAgMnS	urazit
Radu	rada	k1gFnSc4	rada
zdravotně	zdravotně	k6eAd1	zdravotně
postižených	postižený	k1gMnPc2	postižený
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
to	ten	k3xDgNnSc4	ten
vnímala	vnímat	k5eAaImAgFnS	vnímat
jako	jako	k9	jako
znevážení	znevážení	k1gNnSc4	znevážení
invalidních	invalidní	k2eAgFnPc2d1	invalidní
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
<g/>
Především	především	k9	především
za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
podporu	podpora	k1gFnSc4	podpora
projektu	projekt	k1gInSc2	projekt
vodního	vodní	k2eAgInSc2d1	vodní
kanálu	kanál	k1gInSc2	kanál
Dunaj	Dunaj	k1gInSc1	Dunaj
<g/>
–	–	k?	–
<g/>
Odra	Odra	k1gFnSc1	Odra
<g/>
–	–	k?	–
<g/>
Labe	Labe	k1gNnSc2	Labe
získal	získat	k5eAaPmAgInS	získat
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2013	[number]	k4	2013
anticenu	anticen	k2eAgFnSc4d1	anticena
Ropák	ropák	k1gMnSc1	ropák
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
získal	získat	k5eAaPmAgMnS	získat
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
výrok	výrok	k1gInSc4	výrok
"	"	kIx"	"
<g/>
Jako	jako	k8xS	jako
prezident	prezident	k1gMnSc1	prezident
udělám	udělat	k5eAaPmIp1nS	udělat
vše	všechen	k3xTgNnSc4	všechen
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zločiny	zločin	k1gInPc1	zločin
ekologických	ekologický	k2eAgMnPc2d1	ekologický
nebo	nebo	k8xC	nebo
zelených	zelený	k2eAgMnPc2d1	zelený
fanatiků	fanatik	k1gMnPc2	fanatik
dále	daleko	k6eAd2	daleko
nepokračovaly	pokračovat	k5eNaImAgInP	pokračovat
<g/>
"	"	kIx"	"
antiocenění	antiocenění	k1gNnPc2	antiocenění
Zelená	zelenat	k5eAaImIp3nS	zelenat
perla	perla	k1gFnSc1	perla
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2014	[number]	k4	2014
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Hovory	hovor	k1gInPc1	hovor
z	z	k7c2	z
Lán	lán	k1gInSc4	lán
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vláda	vláda	k1gFnSc1	vláda
podle	podle	k7c2	podle
mého	můj	k3xOp1gInSc2	můj
názoru	názor	k1gInSc2	názor
dosud	dosud	k6eAd1	dosud
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
konání	konání	k1gNnSc6	konání
učinila	učinit	k5eAaImAgFnS	učinit
jednu	jeden	k4xCgFnSc4	jeden
základní	základní	k2eAgFnSc4d1	základní
chybu	chyba	k1gFnSc4	chyba
<g/>
,	,	kIx,	,
že	že	k8xS	že
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
panu	pan	k1gMnSc3	pan
Kalouskovi	Kalousek	k1gMnSc3	Kalousek
a	a	k8xC	a
zkurvila	zkurvit	k5eAaPmAgFnS	zkurvit
služební	služební	k2eAgInSc4d1	služební
zákon	zákon	k1gInSc4	zákon
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sprostá	sprostý	k2eAgNnPc1d1	sprosté
slova	slovo	k1gNnPc1	slovo
zazněla	zaznět	k5eAaImAgNnP	zaznět
také	také	k9	také
při	při	k7c6	při
Zemanově	Zemanův	k2eAgInSc6d1	Zemanův
"	"	kIx"	"
<g/>
překladu	překlad	k1gInSc6	překlad
<g/>
"	"	kIx"	"
názvu	název	k1gInSc2	název
a	a	k8xC	a
údajných	údajný	k2eAgInPc2d1	údajný
textů	text	k1gInPc2	text
ruské	ruský	k2eAgFnSc2d1	ruská
punkové	punkový	k2eAgFnSc2d1	punková
kapely	kapela	k1gFnSc2	kapela
Pussy	Pussa	k1gFnSc2	Pussa
Riot	Riota	k1gFnPc2	Riota
<g/>
,	,	kIx,	,
s	s	k7c7	s
poukazem	poukaz	k1gInSc7	poukaz
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nebude	být	k5eNaImBp3nS	být
zastávat	zastávat	k5eAaImF	zastávat
"	"	kIx"	"
<g/>
kurev	kurva	k1gFnPc2	kurva
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Takže	takže	k9	takže
s	s	k7c7	s
prominutím	prominutí	k1gNnSc7	prominutí
v	v	k7c6	v
textech	text	k1gInPc6	text
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
je	být	k5eAaImIp3nS	být
kunda	kunda	k1gFnSc1	kunda
sem	sem	k6eAd1	sem
<g/>
,	,	kIx,	,
kunda	kunda	k1gFnSc1	kunda
tam	tam	k6eAd1	tam
<g/>
.	.	kIx.	.
</s>
<s>
Opravdu	opravdu	k6eAd1	opravdu
dokonalý	dokonalý	k2eAgInSc1d1	dokonalý
případ	případ	k1gInSc1	případ
politického	politický	k2eAgMnSc2d1	politický
vězně	vězeň	k1gMnSc2	vězeň
jako	jako	k8xS	jako
vyšitej	vyšitej	k?	vyšitej
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
komentoval	komentovat	k5eAaBmAgMnS	komentovat
působení	působení	k1gNnSc4	působení
skupiny	skupina	k1gFnSc2	skupina
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
.	.	kIx.	.
</s>
<s>
Pussy	Puss	k1gInPc4	Puss
Riot	Riota	k1gFnPc2	Riota
označil	označit	k5eAaPmAgInS	označit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
pornografickou	pornografický	k2eAgFnSc4d1	pornografická
skupinku	skupinka	k1gFnSc4	skupinka
<g/>
"	"	kIx"	"
vinnou	vinný	k2eAgFnSc4d1	vinná
výtržnictvím	výtržnictví	k1gNnPc3	výtržnictví
v	v	k7c6	v
pravoslavném	pravoslavný	k2eAgInSc6d1	pravoslavný
chrámu	chrám	k1gInSc6	chrám
a	a	k8xC	a
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
řazení	řazení	k1gNnSc4	řazení
členek	členka	k1gFnPc2	členka
kapely	kapela	k1gFnSc2	kapela
mezi	mezi	k7c4	mezi
politické	politický	k2eAgMnPc4d1	politický
vězně	vězeň	k1gMnPc4	vězeň
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kategorie	kategorie	k1gFnSc2	kategorie
politických	politický	k2eAgMnPc2d1	politický
vězňů	vězeň	k1gMnPc2	vězeň
vyjmul	vyjmout	k5eAaPmAgInS	vyjmout
také	také	k9	také
Michaila	Michail	k1gMnSc2	Michail
Chodorkovského	Chodorkovský	k2eAgMnSc2d1	Chodorkovský
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c2	za
spravedlivě	spravedlivě	k6eAd1	spravedlivě
odsouzeného	odsouzený	k2eAgMnSc2d1	odsouzený
tuneláře	tunelář	k1gMnSc2	tunelář
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentův	prezidentův	k2eAgMnSc1d1	prezidentův
mluvčí	mluvčí	k1gMnSc1	mluvčí
Jiří	Jiří	k1gMnSc1	Jiří
Ovčáček	ovčáček	k1gMnSc1	ovčáček
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
přiblížit	přiblížit	k5eAaPmF	přiblížit
světu	svět	k1gInSc3	svět
svých	svůj	k3xOyFgMnPc2	svůj
odpůrců	odpůrce	k1gMnPc2	odpůrce
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
doposud	doposud	k6eAd1	doposud
blahosklonně	blahosklonně	k6eAd1	blahosklonně
přihlíželi	přihlížet	k5eAaImAgMnP	přihlížet
jadrným	jadrný	k2eAgInPc3d1	jadrný
výrazům	výraz	k1gInPc3	výraz
z	z	k7c2	z
úst	ústa	k1gNnPc2	ústa
jiných	jiný	k2eAgMnPc2d1	jiný
politiků	politik	k1gMnPc2	politik
<g/>
,	,	kIx,	,
a	a	k8xC	a
otevřel	otevřít	k5eAaPmAgMnS	otevřít
klíčové	klíčový	k2eAgNnSc4d1	klíčové
téma	téma	k1gNnSc4	téma
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
přípustné	přípustný	k2eAgNnSc1d1	přípustné
takovéto	takovýto	k3xDgNnSc4	takovýto
výrazivo	výrazivo	k1gNnSc4	výrazivo
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
využívat	využívat	k5eAaImF	využívat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
návštěvy	návštěva	k1gFnSc2	návštěva
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
se	se	k3xPyFc4	se
Marija	Marij	k2eAgFnSc1d1	Marija
Aljochinová	Aljochinová	k1gFnSc1	Aljochinová
a	a	k8xC	a
Naděžda	Naděžda	k1gFnSc1	Naděžda
Tolokonnikovová	Tolokonnikovová	k1gFnSc1	Tolokonnikovová
k	k	k7c3	k
jeho	jeho	k3xOp3gMnPc3	jeho
výrokům	výrok	k1gInPc3	výrok
vyjádřily	vyjádřit	k5eAaPmAgFnP	vyjádřit
<g/>
.	.	kIx.	.
</s>
<s>
Prohlásily	prohlásit	k5eAaPmAgInP	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Zeman	Zeman	k1gMnSc1	Zeman
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
evropských	evropský	k2eAgMnPc2d1	evropský
levicových	levicový	k2eAgMnPc2d1	levicový
vůdců	vůdce	k1gMnPc2	vůdce
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nerozumí	rozumět	k5eNaImIp3nS	rozumět
tomu	ten	k3xDgNnSc3	ten
co	co	k9	co
se	se	k3xPyFc4	se
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
děje	děj	k1gInSc2	děj
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
nerozumí	rozumět	k5eNaImIp3nS	rozumět
punku	punk	k1gInSc2	punk
<g/>
,	,	kIx,	,
pročež	pročež	k6eAd1	pročež
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
produkci	produkce	k1gFnSc3	produkce
neměl	mít	k5eNaImAgMnS	mít
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
označily	označit	k5eAaPmAgFnP	označit
jednání	jednání	k1gNnSc6	jednání
prezidenta	prezident	k1gMnSc2	prezident
za	za	k7c4	za
projev	projev	k1gInSc4	projev
patriarchálního	patriarchální	k2eAgMnSc2d1	patriarchální
hloupého	hloupý	k2eAgMnSc2d1	hloupý
chlapa	chlap	k1gMnSc2	chlap
<g/>
.	.	kIx.	.
<g/>
Zemanova	Zemanův	k2eAgFnSc1d1	Zemanova
vulgarita	vulgarita	k1gFnSc1	vulgarita
měla	mít	k5eAaImAgFnS	mít
ohlas	ohlas	k1gInSc4	ohlas
i	i	k9	i
v	v	k7c6	v
zahraničním	zahraniční	k2eAgInSc6d1	zahraniční
tisku	tisk	k1gInSc6	tisk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Krnově	Krnov	k1gInSc6	Krnov
byli	být	k5eAaImAgMnP	být
dva	dva	k4xCgMnPc1	dva
kritici	kritik	k1gMnPc1	kritik
Zemana	Zeman	k1gMnSc2	Zeman
zadrženi	zadržet	k5eAaPmNgMnP	zadržet
policií	policie	k1gFnSc7	policie
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
byli	být	k5eAaImAgMnP	být
zahaleni	zahalen	k2eAgMnPc1d1	zahalen
kuklami	kukla	k1gFnPc7	kukla
a	a	k8xC	a
poté	poté	k6eAd1	poté
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
prokázat	prokázat	k5eAaPmF	prokázat
totožnost	totožnost	k1gFnSc4	totožnost
občanskými	občanský	k2eAgInPc7d1	občanský
průkazy	průkaz	k1gInPc7	průkaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Na	na	k7c4	na
výročí	výročí	k1gNnSc4	výročí
sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
dne	den	k1gInSc2	den
17.	[number]	k4	17.
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
se	se	k3xPyFc4	se
na	na	k7c6	na
Národní	národní	k2eAgFnSc6d1	národní
třídě	třída	k1gFnSc6	třída
sešlo	sejít	k5eAaPmAgNnS	sejít
několik	několik	k4yIc1	několik
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
demonstraci	demonstrace	k1gFnSc6	demonstrace
proti	proti	k7c3	proti
osobě	osoba	k1gFnSc3	osoba
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Účastníci	účastník	k1gMnPc1	účastník
akce	akce	k1gFnSc2	akce
Chci	chtít	k5eAaImIp1nS	chtít
si	se	k3xPyFc3	se
s	s	k7c7	s
vámi	vy	k3xPp2nPc7	vy
promluvit	promluvit	k5eAaPmF	promluvit
<g/>
,	,	kIx,	,
pane	pan	k1gMnSc5	pan
prezidente	prezident	k1gMnSc5	prezident
ukázali	ukázat	k5eAaPmAgMnP	ukázat
svůj	svůj	k3xOyFgInSc4	svůj
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
zdvižením	zdvižení	k1gNnSc7	zdvižení
červených	červený	k2eAgFnPc2d1	červená
karet	kareta	k1gFnPc2	kareta
<g/>
.	.	kIx.	.
</s>
<s>
Protestující	protestující	k2eAgMnPc1d1	protestující
Zemanovi	Zemanův	k2eAgMnPc1d1	Zemanův
vyčítali	vyčítat	k5eAaImAgMnP	vyčítat
vstřícný	vstřícný	k2eAgInSc4d1	vstřícný
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
Rusku	Rusko	k1gNnSc3	Rusko
a	a	k8xC	a
Číně	Čína	k1gFnSc3	Čína
i	i	k8xC	i
jeho	jeho	k3xOp3gInPc1	jeho
vulgární	vulgární	k2eAgInPc1d1	vulgární
verbální	verbální	k2eAgInPc1d1	verbální
projevy	projev	k1gInPc1	projev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Část	část	k1gFnSc1	část
demonstrantů	demonstrant	k1gMnPc2	demonstrant
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
přesunula	přesunout	k5eAaPmAgFnS	přesunout
na	na	k7c4	na
Albertov	Albertov	k1gInSc4	Albertov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
prezidenta	prezident	k1gMnSc2	prezident
Zemana	Zeman	k1gMnSc2	Zeman
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
kolegů	kolega	k1gMnPc2	kolega
konalo	konat	k5eAaImAgNnS	konat
slavnostní	slavnostní	k2eAgNnSc4d1	slavnostní
odhalení	odhalení	k1gNnSc4	odhalení
pamětní	pamětní	k2eAgFnSc2d1	pamětní
desky	deska	k1gFnSc2	deska
k	k	k7c3	k
25.	[number]	k4	25.
výročí	výročí	k1gNnSc3	výročí
události	událost	k1gFnSc2	událost
17.	[number]	k4	17.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
Zemana	Zeman	k1gMnSc4	Zeman
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
projevu	projev	k1gInSc6	projev
vypískali	vypískat	k5eAaPmAgMnP	vypískat
a	a	k8xC	a
házeli	házet	k5eAaImAgMnP	házet
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
vajíčka	vajíčko	k1gNnPc4	vajíčko
<g/>
,	,	kIx,	,
chlebíčky	chlebíček	k1gInPc4	chlebíček
a	a	k8xC	a
rajčata	rajče	k1gNnPc4	rajče
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zasáhli	zasáhnout	k5eAaPmAgMnP	zasáhnout
skořápkou	skořápka	k1gFnSc7	skořápka
německého	německý	k2eAgMnSc2d1	německý
prezidenta	prezident	k1gMnSc2	prezident
Joachima	Joachima	k1gFnSc1	Joachima
Gaucka	Gaucka	k1gFnSc1	Gaucka
<g/>
.	.	kIx.	.
</s>
<s>
Mluvčí	mluvčí	k1gFnSc1	mluvčí
prezidenta	prezident	k1gMnSc2	prezident
událost	událost	k1gFnSc1	událost
popsal	popsat	k5eAaPmAgInS	popsat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
Gauck	Gauck	k1gInSc1	Gauck
zasažen	zasáhnout	k5eAaPmNgInS	zasáhnout
vajíčkem	vajíčko	k1gNnSc7	vajíčko
do	do	k7c2	do
spánku	spánek	k1gInSc2	spánek
a	a	k8xC	a
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
otřesen	otřást	k5eAaPmNgInS	otřást
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
německé	německý	k2eAgFnSc2d1	německá
ambasády	ambasáda	k1gFnSc2	ambasáda
Gaucka	Gaucka	k1gFnSc1	Gaucka
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
jen	jen	k9	jen
skořápka	skořápka	k1gFnSc1	skořápka
a	a	k8xC	a
prakticky	prakticky	k6eAd1	prakticky
si	se	k3xPyFc3	se
toho	ten	k3xDgNnSc2	ten
nevšiml	všimnout	k5eNaPmAgMnS	všimnout
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
projevech	projev	k1gInPc6	projev
ostatních	ostatní	k2eAgMnPc2d1	ostatní
prezidentů	prezident	k1gMnPc2	prezident
lidé	člověk	k1gMnPc1	člověk
ztichli	ztichnout	k5eAaPmAgMnP	ztichnout
a	a	k8xC	a
tleskali	tleskat	k5eAaImAgMnP	tleskat
<g/>
.	.	kIx.	.
</s>
<s>
Házení	házení	k1gNnSc4	házení
vajíček	vajíčko	k1gNnPc2	vajíčko
na	na	k7c6	na
Albertově	Albertův	k2eAgFnSc6d1	Albertova
odsoudili	odsoudit	k5eAaPmAgMnP	odsoudit
čeští	český	k2eAgMnPc1d1	český
politici	politik	k1gMnPc1	politik
i	i	k9	i
část	část	k1gFnSc4	část
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
pak	pak	k6eAd1	pak
protest	protest	k1gInSc4	protest
na	na	k7c6	na
Albertově	Albertův	k2eAgFnSc6d1	Albertova
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
ostudu	ostuda	k1gFnSc4	ostuda
a	a	k8xC	a
pokračování	pokračování	k1gNnSc4	pokračování
předvolební	předvolební	k2eAgFnSc2d1	předvolební
kampaně	kampaň	k1gFnSc2	kampaň
z	z	k7c2	z
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
volby	volba	k1gFnSc2	volba
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
prezidentovo	prezidentův	k2eAgNnSc4d1	prezidentovo
chování	chování	k1gNnSc4	chování
během	během	k7c2	během
jeho	on	k3xPp3gNnSc2	on
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
satirická	satirický	k2eAgFnSc1d1	satirická
počítačová	počítačový	k2eAgFnSc1d1	počítačová
hra	hra	k1gFnSc1	hra
s	s	k7c7	s
názvem	název	k1gInSc7	název
PussyWalk	PussyWalka	k1gFnPc2	PussyWalka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
prezidenta	prezident	k1gMnSc2	prezident
představila	představit	k5eAaPmAgFnS	představit
jako	jako	k8xC	jako
hlavního	hlavní	k2eAgMnSc4d1	hlavní
hrdinu	hrdina	k1gMnSc4	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hra	hra	k1gFnSc1	hra
si	se	k3xPyFc3	se
získala	získat	k5eAaPmAgFnS	získat
popularitu	popularita	k1gFnSc4	popularita
i	i	k9	i
mimo	mimo	k7c4	mimo
Česko	Česko	k1gNnSc4	Česko
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2016	[number]	k4	2016
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
pozdvižení	pozdvižení	k1gNnSc4	pozdvižení
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c4	na
dotaz	dotaz	k1gInSc4	dotaz
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
účastnic	účastnice	k1gFnPc2	účastnice
diskuse	diskuse	k1gFnSc2	diskuse
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgFnPc1	jaký
jsou	být	k5eAaImIp3nP	být
možnosti	možnost	k1gFnPc4	možnost
zbavit	zbavit	k5eAaPmF	zbavit
se	se	k3xPyFc4	se
premiéra	premiér	k1gMnSc2	premiér
Sobotky	Sobotka	k1gMnSc2	Sobotka
<g/>
,	,	kIx,	,
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Chcete	chtít	k5eAaImIp2nP	chtít
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
zbavit	zbavit	k5eAaPmF	zbavit
jakéhokoli	jakýkoli	k3yIgNnSc2	jakýkoli
politika	politikum	k1gNnSc2	politikum
<g/>
,	,	kIx,	,
prezidenta	prezident	k1gMnSc2	prezident
nevyjímaje	nevyjímaje	k7c4	nevyjímaje
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
jediná	jediný	k2eAgFnSc1d1	jediná
demokratická	demokratický	k2eAgFnSc1d1	demokratická
cesta	cesta	k1gFnSc1	cesta
-	-	kIx~	-
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
svobodné	svobodný	k2eAgFnPc1d1	svobodná
volby	volba	k1gFnPc1	volba
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
...	...	k?	...
<g/>
]	]	kIx)	]
A	a	k9	a
pak	pak	k6eAd1	pak
existuje	existovat	k5eAaImIp3nS	existovat
nedemokratická	demokratický	k2eNgFnSc1d1	nedemokratická
cesta	cesta	k1gFnSc1	cesta
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
kalašnikov	kalašnikov	k1gInSc1	kalašnikov
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Podle	podle	k7c2	podle
mnoha	mnoho	k4c2	mnoho
komentátorů	komentátor	k1gMnPc2	komentátor
a	a	k8xC	a
politiků	politik	k1gMnPc2	politik
napříč	napříč	k7c7	napříč
politickým	politický	k2eAgNnSc7d1	politické
spektrem	spektrum	k1gNnSc7	spektrum
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
"	"	kIx"	"
<g/>
hloupý	hloupý	k2eAgInSc4d1	hloupý
a	a	k8xC	a
nevkusný	vkusný	k2eNgInSc4d1	nevkusný
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
trapný	trapný	k2eAgInSc4d1	trapný
a	a	k8xC	a
nepatřičný	patřičný	k2eNgInSc4d1	nepatřičný
<g/>
"	"	kIx"	"
bonmot	bonmot	k1gInSc4	bonmot
či	či	k8xC	či
žert	žert	k1gInSc4	žert
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
ostře	ostro	k6eAd1	ostro
odsoudili	odsoudit	k5eAaPmAgMnP	odsoudit
<g/>
.	.	kIx.	.
</s>
<s>
Mluvčí	mluvčí	k1gMnSc1	mluvčí
Ovčáček	ovčáček	k1gMnSc1	ovčáček
výrok	výrok	k1gInSc4	výrok
hájil	hájit	k5eAaImAgMnS	hájit
jako	jako	k8xC	jako
nadsázku	nadsázka	k1gFnSc4	nadsázka
<g/>
.	.	kIx.	.
</s>
<s>
Miroslava	Miroslava	k1gFnSc1	Miroslava
Němcová	Němcová	k1gFnSc1	Němcová
(	(	kIx(	(
<g/>
ODS	ODS	kA	ODS
<g/>
)	)	kIx)	)
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovně	sněmovna	k1gFnSc3	sněmovna
přijmout	přijmout	k5eAaPmF	přijmout
usnesení	usnesení	k1gNnSc4	usnesení
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
by	by	kYmCp3nS	by
výroky	výrok	k1gInPc4	výrok
ostře	ostro	k6eAd1	ostro
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
a	a	k8xC	a
žádala	žádat	k5eAaImAgFnS	žádat
po	po	k7c6	po
prezidentovi	prezident	k1gMnSc6	prezident
omluvu	omluva	k1gFnSc4	omluva
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
však	však	k9	však
v	v	k7c6	v
hlasování	hlasování	k1gNnSc6	hlasování
schváleno	schválen	k2eAgNnSc1d1	schváleno
<g/>
.	.	kIx.	.
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Šarapatka	šarapatka	k1gFnSc1	šarapatka
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
prezidenta	prezident	k1gMnSc4	prezident
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnSc3	jeho
aroganci	arogance	k1gFnSc3	arogance
<g/>
,	,	kIx,	,
podal	podat	k5eAaPmAgMnS	podat
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
trestní	trestní	k2eAgNnSc1d1	trestní
oznámení	oznámení	k1gNnSc1	oznámení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
prezidenta	prezident	k1gMnSc2	prezident
ti	ten	k3xDgMnPc1	ten
politici	politik	k1gMnPc1	politik
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
bonmot	bonmot	k1gInSc4	bonmot
"	"	kIx"	"
<g/>
hystericky	hystericky	k6eAd1	hystericky
reagovali	reagovat	k5eAaBmAgMnP	reagovat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ze	z	k7c2	z
sebe	sebe	k3xPyFc4	sebe
udělali	udělat	k5eAaPmAgMnP	udělat
šašky	šaška	k1gFnSc2	šaška
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
prezidentově	prezidentův	k2eAgFnSc6d1	prezidentova
únorové	únorový	k2eAgFnSc6d1	únorová
návštěvě	návštěva	k1gFnSc6	návštěva
Libereckého	liberecký	k2eAgInSc2d1	liberecký
kraje	kraj	k1gInSc2	kraj
mu	on	k3xPp3gNnSc3	on
nefunkční	funkční	k2eNgInSc4d1	nefunkční
exemplář	exemplář	k1gInSc4	exemplář
této	tento	k3xDgFnSc2	tento
zbraně	zbraň	k1gFnSc2	zbraň
věnoval	věnovat	k5eAaImAgInS	věnovat
jako	jako	k9	jako
soukromý	soukromý	k2eAgInSc1d1	soukromý
dar	dar	k1gInSc1	dar
hejtman	hejtman	k1gMnSc1	hejtman
Martin	Martin	k1gMnSc1	Martin
Půta	Půta	k1gMnSc1	Půta
(	(	kIx(	(
<g/>
STAN	stan	k1gInSc1	stan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
25.	[number]	k4	25.
října	říjen	k1gInSc2	říjen
2016	[number]	k4	2016
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
natáčení	natáčení	k1gNnSc2	natáčení
Show	show	k1gFnSc2	show
Jana	Jan	k1gMnSc2	Jan
Krause	Kraus	k1gMnSc2	Kraus
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vysílá	vysílat	k5eAaImIp3nS	vysílat
televizní	televizní	k2eAgFnSc1d1	televizní
stanice	stanice	k1gFnSc1	stanice
Prima	prima	k2eAgFnSc1d1	prima
<g/>
,	,	kIx,	,
vystoupily	vystoupit	k5eAaPmAgFnP	vystoupit
některé	některý	k3yIgNnSc1	některý
české	český	k2eAgFnPc1d1	Česká
osobnosti	osobnost	k1gFnPc1	osobnost
s	s	k7c7	s
kritikou	kritika	k1gFnSc7	kritika
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
neudělením	neudělení	k1gNnSc7	neudělení
vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
Jiřímu	Jiří	k1gMnSc3	Jiří
Bradymu	Bradym	k1gInSc2	Bradym
(	(	kIx(	(
<g/>
Kauza	kauza	k1gFnSc1	kauza
Brady	brada	k1gFnSc2	brada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
hostem	host	k1gMnSc7	host
toho	ten	k3xDgInSc2	ten
dílu	díl	k1gInSc2	díl
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
v	v	k7c6	v
říjnovém	říjnový	k2eAgInSc6d1	říjnový
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
Český	český	k2eAgInSc4d1	český
rozhlas	rozhlas	k1gInSc4	rozhlas
prezident	prezident	k1gMnSc1	prezident
Zeman	Zeman	k1gMnSc1	Zeman
navázal	navázat	k5eAaPmAgMnS	navázat
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
dřívější	dřívější	k2eAgInSc4d1	dřívější
vulgární	vulgární	k2eAgInSc4d1	vulgární
projev	projev	k1gInSc4	projev
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
a	a	k8xC	a
hovořil	hovořit	k5eAaImAgInS	hovořit
o	o	k7c6	o
"	"	kIx"	"
<g/>
ekonomických	ekonomický	k2eAgInPc6d1	ekonomický
zmrdech	zmrd	k1gInPc6	zmrd
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
označení	označení	k1gNnSc4	označení
adresoval	adresovat	k5eAaBmAgMnS	adresovat
konkrétně	konkrétně	k6eAd1	konkrétně
na	na	k7c4	na
Viktora	Viktor	k1gMnSc4	Viktor
Koženého	Kožený	k1gMnSc4	Kožený
<g/>
,	,	kIx,	,
Zdeňka	Zdeněk	k1gMnSc4	Zdeněk
Bakalu	Bakal	k1gMnSc3	Bakal
<g/>
,	,	kIx,	,
skupinu	skupina	k1gFnSc4	skupina
solárních	solární	k2eAgMnPc2d1	solární
baronů	baron	k1gMnPc2	baron
a	a	k8xC	a
na	na	k7c4	na
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
zapletli	zaplést	k5eAaPmAgMnP	zaplést
do	do	k7c2	do
lehkých	lehký	k2eAgInPc2d1	lehký
topných	topný	k2eAgInPc2d1	topný
olejů	olej	k1gInPc2	olej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Postoj	postoj	k1gInSc1	postoj
vůči	vůči	k7c3	vůči
novinářům	novinář	k1gMnPc3	novinář
===	===	k?	===
</s>
</p>
<p>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
pozornost	pozornost	k1gFnSc4	pozornost
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
rozhovor	rozhovor	k1gInSc1	rozhovor
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
s	s	k7c7	s
Vladimirem	Vladimir	k1gMnSc7	Vladimir
Putinem	Putin	k1gMnSc7	Putin
<g/>
,	,	kIx,	,
uskutečněný	uskutečněný	k2eAgInSc4d1	uskutečněný
během	během	k7c2	během
prezidentovy	prezidentův	k2eAgFnSc2d1	prezidentova
návštěvy	návštěva	k1gFnSc2	návštěva
Pekingu	Peking	k1gInSc2	Peking
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2017.	[number]	k4	2017.
"	"	kIx"	"
<g/>
Tady	tady	k6eAd1	tady
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k9	ještě
další	další	k2eAgMnPc1d1	další
novináři	novinář	k1gMnPc1	novinář
<g/>
?	?	kIx.	?
</s>
<s>
Novinářů	novinář	k1gMnPc2	novinář
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
likvidovat	likvidovat	k5eAaBmF	likvidovat
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
pronesl	pronést	k5eAaPmAgMnS	pronést
Zeman	Zeman	k1gMnSc1	Zeman
během	během	k7c2	během
procházení	procházení	k1gNnSc2	procházení
kolem	kolem	k7c2	kolem
hloučku	hlouček	k1gInSc2	hlouček
novinářů	novinář	k1gMnPc2	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Ruský	ruský	k2eAgMnSc1d1	ruský
prezident	prezident	k1gMnSc1	prezident
odvětil	odvětit	k5eAaPmAgMnS	odvětit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
likvidovat	likvidovat	k5eAaBmF	likvidovat
je	on	k3xPp3gInPc4	on
není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
omezit	omezit	k5eAaPmF	omezit
jejich	jejich	k3xOp3gInSc4	jejich
počet	počet	k1gInSc4	počet
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Mluvčí	mluvčí	k1gMnSc1	mluvčí
Jiří	Jiří	k1gMnSc1	Jiří
Ovčáček	ovčáček	k1gMnSc1	ovčáček
výrok	výrok	k1gInSc4	výrok
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
bonmot	bonmot	k1gInSc4	bonmot
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Novináři	novinář	k1gMnPc1	novinář
zásadně	zásadně	k6eAd1	zásadně
nikdy	nikdy	k6eAd1	nikdy
bonmoty	bonmot	k1gInPc1	bonmot
nechápou	chápat	k5eNaImIp3nP	chápat
<g/>
.	.	kIx.	.
</s>
<s>
Očekávám	očekávat	k5eAaImIp1nS	očekávat
teď	teď	k6eAd1	teď
trapně	trapně	k6eAd1	trapně
pohoršené	pohoršený	k2eAgInPc4d1	pohoršený
komentáře	komentář	k1gInPc4	komentář
a	a	k8xC	a
rozhořčené	rozhořčený	k2eAgFnPc4d1	rozhořčená
reakce	reakce	k1gFnPc4	reakce
politiků	politik	k1gMnPc2	politik
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
k	k	k7c3	k
médiím	médium	k1gNnPc3	médium
rádi	rád	k2eAgMnPc1d1	rád
lísají	lísat	k5eAaImIp3nP	lísat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Řada	řada	k1gFnSc1	řada
politiků	politik	k1gMnPc2	politik
výrok	výrok	k1gInSc4	výrok
kritizovala	kritizovat	k5eAaImAgFnS	kritizovat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2017	[number]	k4	2017
na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
při	při	k7c6	při
ukončení	ukončení	k1gNnSc6	ukončení
návštěvy	návštěva	k1gFnSc2	návštěva
Plzeňského	plzeňský	k2eAgInSc2d1	plzeňský
kraje	kraj	k1gInSc2	kraj
se	se	k3xPyFc4	se
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
s	s	k7c7	s
darem	dar	k1gInSc7	dar
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
dřevěného	dřevěný	k2eAgInSc2d1	dřevěný
samopalu	samopal	k1gInSc2	samopal
"	"	kIx"	"
<g/>
na	na	k7c4	na
novináře	novinář	k1gMnPc4	novinář
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2018	[number]	k4	2018
reprodukoval	reprodukovat	k5eAaBmAgMnS	reprodukovat
"	"	kIx"	"
<g/>
vtip	vtip	k1gInSc4	vtip
<g/>
"	"	kIx"	"
využívající	využívající	k2eAgFnSc4d1	využívající
vraždu	vražda	k1gFnSc4	vražda
saúdskoarabského	saúdskoarabský	k2eAgMnSc2d1	saúdskoarabský
novináře	novinář	k1gMnSc2	novinář
Džamála	Džamál	k1gMnSc2	Džamál
Chášukdžího	Chášukdží	k1gMnSc2	Chášukdží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Kauza	kauza	k1gFnSc1	kauza
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Peroutka	Peroutka	k1gMnSc1	Peroutka
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
projevu	projev	k1gInSc6	projev
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
Let	léto	k1gNnPc2	léto
my	my	k3xPp1nPc1	my
people	people	k6eAd1	people
live	live	k6eAd1	live
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2015	[number]	k4	2015
označil	označit	k5eAaPmAgMnS	označit
Zeman	Zeman	k1gMnSc1	Zeman
novináře	novinář	k1gMnSc2	novinář
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Peroutku	peroutka	k1gFnSc4	peroutka
za	za	k7c4	za
autora	autor	k1gMnSc4	autor
článku	článek	k1gInSc2	článek
s	s	k7c7	s
titulkem	titulek	k1gInSc7	titulek
"	"	kIx"	"
<g/>
Hitler	Hitler	k1gMnSc1	Hitler
je	být	k5eAaImIp3nS	být
gentleman	gentleman	k1gMnSc1	gentleman
<g/>
"	"	kIx"	"
a	a	k8xC	a
výroku	výrok	k1gInSc2	výrok
"	"	kIx"	"
<g/>
Nemůžeme	moct	k5eNaImIp1nP	moct
<g/>
-li	i	k?	-li
zpívat	zpívat	k5eAaImF	zpívat
s	s	k7c7	s
anděly	anděl	k1gMnPc7	anděl
<g/>
,	,	kIx,	,
musíme	muset	k5eAaImIp1nP	muset
výti	výt	k5eAaImF	výt
s	s	k7c7	s
vlky	vlk	k1gMnPc7	vlk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
směřován	směřován	k2eAgInSc1d1	směřován
na	na	k7c4	na
zahraničně-politické	zahraničněolitický	k2eAgNnSc4d1	zahraničně-politické
zaměření	zaměření	k1gNnSc4	zaměření
tzv.	tzv.	kA	tzv.
druhé	druhý	k4xOgFnSc2	druhý
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
po	po	k7c6	po
Mnichovské	mnichovský	k2eAgFnSc6d1	Mnichovská
dohodě	dohoda	k1gFnSc6	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
část	část	k1gFnSc4	část
projevu	projev	k1gInSc2	projev
sklidil	sklidit	k5eAaPmAgMnS	sklidit
velkou	velký	k2eAgFnSc4d1	velká
kritiku	kritika	k1gFnSc4	kritika
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
historiků	historik	k1gMnPc2	historik
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
redaktorem	redaktor	k1gMnSc7	redaktor
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
Petrem	Petr	k1gMnSc7	Petr
Zídkem	Zídek	k1gMnSc7	Zídek
a	a	k8xC	a
zástupců	zástupce	k1gMnPc2	zástupce
Sdružení	sdružení	k1gNnSc2	sdružení
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Peroutky	Peroutka	k1gMnSc2	Peroutka
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
se	se	k3xPyFc4	se
bránil	bránit	k5eAaImAgMnS	bránit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Zeman	Zeman	k1gMnSc1	Zeman
dokument	dokument	k1gInSc4	dokument
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
četl	číst	k5eAaImAgMnS	číst
a	a	k8xC	a
že	že	k8xS	že
ho	on	k3xPp3gInSc4	on
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
tiskového	tiskový	k2eAgInSc2d1	tiskový
odboru	odbor	k1gInSc2	odbor
dohledají	dohledat	k5eAaPmIp3nP	dohledat
v	v	k7c6	v
archivech	archiv	k1gInPc6	archiv
<g/>
.	.	kIx.	.
</s>
<s>
Výrok	výrok	k1gInSc1	výrok
"	"	kIx"	"
<g/>
Nemůžeme	moct	k5eNaImIp1nP	moct
<g/>
-li	i	k?	-li
zpívat	zpívat	k5eAaImF	zpívat
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
zanedlouho	zanedlouho	k6eAd1	zanedlouho
nalezen	nalézt	k5eAaBmNgInS	nalézt
v	v	k7c6	v
textu	text	k1gInSc6	text
Jana	Jan	k1gMnSc2	Jan
Stránského	Stránský	k1gMnSc2	Stránský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2015	[number]	k4	2015
Peroutkova	Peroutkův	k2eAgFnSc1d1	Peroutkova
vnučka	vnučka	k1gFnSc1	vnučka
Terezie	Terezie	k1gFnSc1	Terezie
Kaslová	Kaslová	k1gFnSc1	Kaslová
podala	podat	k5eAaPmAgFnS	podat
za	za	k7c4	za
výroky	výrok	k1gInPc4	výrok
prezidenta	prezident	k1gMnSc2	prezident
na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
jejího	její	k3xOp3gMnSc2	její
dědečka	dědeček	k1gMnSc2	dědeček
žalobu	žaloba	k1gFnSc4	žaloba
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
podle	podle	k7c2	podle
prezidenta	prezident	k1gMnSc2	prezident
Zemana	Zeman	k1gMnSc2	Zeman
jeho	jeho	k3xOp3gMnSc1	jeho
mluvčí	mluvčí	k1gMnSc1	mluvčí
objevil	objevit	k5eAaPmAgMnS	objevit
při	při	k7c6	při
prohledávání	prohledávání	k1gNnSc6	prohledávání
písemností	písemnost	k1gFnPc2	písemnost
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Peroutky	peroutka	k1gFnSc2	peroutka
jiná	jiný	k2eAgNnPc4d1	jiné
vyjádření	vyjádření	k1gNnPc4	vyjádření
<g/>
,	,	kIx,	,
než	než	k8xS	než
prezident	prezident	k1gMnSc1	prezident
citoval	citovat	k5eAaBmAgMnS	citovat
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
fóru	fórum	k1gNnSc6	fórum
<g/>
,	,	kIx,	,
a	a	k8xC	a
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
dubna	duben	k1gInSc2	duben
začal	začít	k5eAaPmAgInS	začít
postupně	postupně	k6eAd1	postupně
zveřejňovat	zveřejňovat	k5eAaImF	zveřejňovat
některé	některý	k3yIgInPc4	některý
kontroverznější	kontroverzní	k2eAgInPc4d2	kontroverznější
Peroutkovy	Peroutkův	k2eAgInPc4d1	Peroutkův
články	článek	k1gInPc4	článek
z	z	k7c2	z
období	období	k1gNnSc2	období
druhé	druhý	k4xOgFnSc2	druhý
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
počátku	počátek	k1gInSc2	počátek
Protektorátu	protektorát	k1gInSc2	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
3.	[number]	k4	3.
května	květen	k1gInSc2	květen
se	se	k3xPyFc4	se
Zeman	Zeman	k1gMnSc1	Zeman
v	v	k7c6	v
online	onlin	k1gInSc5	onlin
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
Blesk	blesk	k1gInSc4	blesk
TV	TV	kA	TV
ke	k	k7c3	k
kauze	kauza	k1gFnSc3	kauza
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
.	.	kIx.	.
</s>
<s>
Řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
mluvčí	mluvčí	k1gMnSc1	mluvčí
Hradu	hrad	k1gInSc2	hrad
Jiří	Jiří	k1gMnSc1	Jiří
Ovčáček	ovčáček	k1gMnSc1	ovčáček
nenajde	najít	k5eNaPmIp3nS	najít
do	do	k7c2	do
konce	konec	k1gInSc2	konec
června	červen	k1gInSc2	červen
článek	článek	k1gInSc1	článek
"	"	kIx"	"
<g/>
Hitler	Hitler	k1gMnSc1	Hitler
je	být	k5eAaImIp3nS	být
gentleman	gentleman	k1gMnSc1	gentleman
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vypíše	vypsat	k5eAaPmIp3nS	vypsat
Zeman	Zeman	k1gMnSc1	Zeman
z	z	k7c2	z
vlastního	vlastní	k2eAgInSc2d1	vlastní
odměnu	odměna	k1gFnSc4	odměna
pro	pro	k7c4	pro
toho	ten	k3xDgMnSc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
ho	on	k3xPp3gMnSc4	on
najde	najít	k5eAaPmIp3nS	najít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
také	také	k9	také
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
oslavný	oslavný	k2eAgInSc1d1	oslavný
článek	článek	k1gInSc1	článek
na	na	k7c4	na
Třetí	třetí	k4xOgFnSc4	třetí
říši	říše	k1gFnSc4	říše
za	za	k7c2	za
války	válka	k1gFnSc2	válka
napsal	napsat	k5eAaPmAgMnS	napsat
i	i	k9	i
spisovatel	spisovatel	k1gMnSc1	spisovatel
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
<g/>
.	.	kIx.	.
<g/>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
Zeman	Zeman	k1gMnSc1	Zeman
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ovčáček	ovčáček	k1gMnSc1	ovčáček
s	s	k7c7	s
hledáním	hledání	k1gNnSc7	hledání
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
,	,	kIx,	,
a	a	k8xC	a
slíbil	slíbit	k5eAaPmAgMnS	slíbit
odměnu	odměna	k1gFnSc4	odměna
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
případnému	případný	k2eAgMnSc3d1	případný
nálezci	nálezce	k1gMnSc3	nálezce
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
omluvil	omluvit	k5eAaPmAgMnS	omluvit
za	za	k7c4	za
nenalezení	nenalezení	k1gNnSc4	nenalezení
článku	článek	k1gInSc2	článek
<g/>
,	,	kIx,	,
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
existenci	existence	k1gFnSc4	existence
přesto	přesto	k8xC	přesto
nadále	nadále	k6eAd1	nadále
trvá	trvat	k5eAaImIp3nS	trvat
<g/>
.	.	kIx.	.
<g/>
Obvodní	obvodní	k2eAgInSc1d1	obvodní
soud	soud	k1gInSc1	soud
pro	pro	k7c4	pro
Prahu	Praha	k1gFnSc4	Praha
1	[number]	k4	1
počátkem	počátkem	k7c2	počátkem
března	březen	k1gInSc2	březen
2016	[number]	k4	2016
nepravomocně	pravomocně	k6eNd1	pravomocně
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
kancelář	kancelář	k1gFnSc1	kancelář
prezidenta	prezident	k1gMnSc2	prezident
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
Terezii	Terezie	k1gFnSc3	Terezie
Kaslové	Kaslová	k1gFnSc2	Kaslová
omluvit	omluvit	k5eAaPmF	omluvit
osobním	osobní	k2eAgInSc7d1	osobní
dopisem	dopis	k1gInSc7	dopis
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
se	se	k3xPyFc4	se
k	k	k7c3	k
rozsudku	rozsudek	k1gInSc3	rozsudek
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
v	v	k7c6	v
diskuzním	diskuzní	k2eAgInSc6d1	diskuzní
pořadu	pořad	k1gInSc6	pořad
televize	televize	k1gFnSc1	televize
Prima	prima	k1gFnSc1	prima
Partie	partie	k1gFnSc1	partie
6.	[number]	k4	6.
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
dotčeného	dotčený	k2eAgInSc2d1	dotčený
článku	článek	k1gInSc2	článek
a	a	k8xC	a
přidal	přidat	k5eAaPmAgMnS	přidat
příměr	příměr	k1gInSc4	příměr
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Boha	bůh	k1gMnSc2	bůh
také	také	k9	také
nikdo	nikdo	k3yNnSc1	nikdo
neviděl	vidět	k5eNaImAgMnS	vidět
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nepovažoval	považovat	k5eNaImAgMnS	považovat
se	se	k3xPyFc4	se
za	za	k7c4	za
natolik	natolik	k6eAd1	natolik
senilního	senilní	k2eAgMnSc4d1	senilní
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ztrácel	ztrácet	k5eAaImAgMnS	ztrácet
paměť	paměť	k1gFnSc4	paměť
<g/>
.	.	kIx.	.
<g/>
Senát	senát	k1gInSc1	senát
Městského	městský	k2eAgInSc2d1	městský
soudu	soud	k1gInSc2	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
soudce	soudce	k1gMnSc2	soudce
Tomáše	Tomáš	k1gMnSc2	Tomáš
Novosada	Novosad	k1gMnSc2	Novosad
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
odvolání	odvolání	k1gNnSc2	odvolání
v	v	k7c6	v
září	září	k1gNnSc6	září
2016	[number]	k4	2016
částečně	částečně	k6eAd1	částečně
změnil	změnit	k5eAaPmAgInS	změnit
rozsudek	rozsudek	k1gInSc4	rozsudek
soudu	soud	k1gInSc2	soud
prvního	první	k4xOgInSc2	první
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Dovolání	dovolání	k1gNnSc2	dovolání
k	k	k7c3	k
Nejvyššímu	vysoký	k2eAgInSc3d3	Nejvyšší
soudu	soud	k1gInSc3	soud
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
nemá	mít	k5eNaImIp3nS	mít
odkladný	odkladný	k2eAgInSc1d1	odkladný
účinek	účinek	k1gInSc1	účinek
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
podaly	podat	k5eAaPmAgFnP	podat
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Kancelář	kancelář	k1gFnSc1	kancelář
přezidenta	přezident	k1gMnSc2	přezident
republiky	republika	k1gFnSc2	republika
měla	mít	k5eAaImAgFnS	mít
omluvu	omluva	k1gFnSc4	omluva
poslat	poslat	k5eAaPmF	poslat
dopisem	dopis	k1gInSc7	dopis
do	do	k7c2	do
23.	[number]	k4	23.
září	září	k1gNnSc2	září
2016	[number]	k4	2016
a	a	k8xC	a
vyvěsit	vyvěsit	k5eAaPmF	vyvěsit
ji	on	k3xPp3gFnSc4	on
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
30	[number]	k4	30
dnů	den	k1gInPc2	den
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
nestalo	stát	k5eNaPmAgNnS	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Kaslová	Kaslová	k1gFnSc1	Kaslová
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
začala	začít	k5eAaPmAgFnS	začít
omluvu	omluva	k1gFnSc4	omluva
vymáhat	vymáhat	k5eAaImF	vymáhat
exekučně	exekučně	k6eAd1	exekučně
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
24.	[number]	k4	24.
října	říjen	k1gInSc2	říjen
exekutor	exekutor	k1gMnSc1	exekutor
pověřený	pověřený	k2eAgMnSc1d1	pověřený
Obvodním	obvodní	k2eAgInSc7d1	obvodní
soudem	soud	k1gInSc7	soud
pro	pro	k7c4	pro
Prahu	Praha	k1gFnSc4	Praha
1	[number]	k4	1
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kancelář	kancelář	k1gFnSc1	kancelář
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
musí	muset	k5eAaImIp3nP	muset
zaplatit	zaplatit	k5eAaPmF	zaplatit
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
dnů	den	k1gInPc2	den
pokutu	pokuta	k1gFnSc4	pokuta
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Kancléř	kancléř	k1gMnSc1	kancléř
Vratislav	Vratislav	k1gMnSc1	Vratislav
Mynář	Mynář	k1gMnSc1	Mynář
však	však	k9	však
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokud	dokud	k8xS	dokud
ale	ale	k8xC	ale
nerozhodne	rozhodnout	k5eNaPmIp3nS	rozhodnout
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
o	o	k7c6	o
podaném	podaný	k2eAgNnSc6d1	podané
dovolání	dovolání	k1gNnSc6	dovolání
<g/>
,	,	kIx,	,
nechce	chtít	k5eNaImIp3nS	chtít
se	se	k3xPyFc4	se
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
kancelář	kancelář	k1gFnSc1	kancelář
omlouvat	omlouvat	k5eAaImF	omlouvat
ani	ani	k8xC	ani
platit	platit	k5eAaImF	platit
pokutu	pokuta	k1gFnSc4	pokuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
kritika	kritika	k1gFnSc1	kritika
===	===	k?	===
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
Human	Humana	k1gFnPc2	Humana
Rights	Rights	k1gInSc4	Rights
Watch	Watch	k1gInSc4	Watch
kritizovala	kritizovat	k5eAaImAgFnS	kritizovat
Miloše	Miloš	k1gMnSc4	Miloš
Zemana	Zeman	k1gMnSc4	Zeman
za	za	k7c4	za
"	"	kIx"	"
<g/>
šíření	šíření	k1gNnSc4	šíření
nevědomosti	nevědomost	k1gFnSc2	nevědomost
a	a	k8xC	a
paniky	panika	k1gFnSc2	panika
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k9	co
na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
31.	[number]	k4	31.
srpna	srpen	k1gInSc2	srpen
2015	[number]	k4	2015
přirovnal	přirovnat	k5eAaPmAgMnS	přirovnat
probíhající	probíhající	k2eAgFnSc4d1	probíhající
uprchlickou	uprchlický	k2eAgFnSc4d1	uprchlická
krizi	krize	k1gFnSc4	krize
k	k	k7c3	k
vlně	vlna	k1gFnSc3	vlna
tsunami	tsunami	k1gNnSc2	tsunami
<g/>
.	.	kIx.	.
</s>
<s>
Upozornila	upozornit	k5eAaPmAgFnS	upozornit
také	také	k9	také
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezident	prezident	k1gMnSc1	prezident
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
řeči	řeč	k1gFnSc6	řeč
přijal	přijmout	k5eAaPmAgInS	přijmout
rétoriku	rétorika	k1gFnSc4	rétorika
nenávistných	návistný	k2eNgFnPc2d1	nenávistná
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
je	být	k5eAaImIp3nS	být
Zeman	Zeman	k1gMnSc1	Zeman
islamofob	islamofoba	k1gFnPc2	islamofoba
<g/>
.	.	kIx.	.
</s>
<s>
Zemanovy	Zemanův	k2eAgInPc1d1	Zemanův
protiislámské	protiislámský	k2eAgInPc1d1	protiislámský
výroky	výrok	k1gInPc1	výrok
kritizovaly	kritizovat	k5eAaImAgInP	kritizovat
také	také	k9	také
Organizace	organizace	k1gFnSc2	organizace
islámské	islámský	k2eAgFnSc2d1	islámská
spolupráce	spolupráce	k1gFnSc2	spolupráce
(	(	kIx(	(
<g/>
OIC	OIC	kA	OIC
<g/>
)	)	kIx)	)
a	a	k8xC	a
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
pozastavila	pozastavit	k5eAaPmAgFnS	pozastavit
své	svůj	k3xOyFgFnPc4	svůj
aktivity	aktivita	k1gFnPc4	aktivita
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2015	[number]	k4	2015
šéf	šéf	k1gMnSc1	šéf
socialistické	socialistický	k2eAgFnSc2d1	socialistická
frakce	frakce	k1gFnSc2	frakce
v	v	k7c6	v
Evropském	evropský	k2eAgInSc6d1	evropský
parlamentu	parlament	k1gInSc6	parlament
Gianni	Gianň	k1gMnPc7	Gianň
Pittella	Pittella	k1gMnSc1	Pittella
ostře	ostro	k6eAd1	ostro
zkritizoval	zkritizovat	k5eAaPmAgMnS	zkritizovat
"	"	kIx"	"
<g/>
ostudná	ostudný	k2eAgNnPc4d1	ostudné
a	a	k8xC	a
blouznivá	blouznivý	k2eAgNnPc4d1	blouznivé
prohlášení	prohlášení	k1gNnPc4	prohlášení
<g/>
"	"	kIx"	"
českého	český	k2eAgMnSc2d1	český
prezidenta	prezident	k1gMnSc2	prezident
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
evropské	evropský	k2eAgFnSc2d1	Evropská
migrační	migrační	k2eAgFnSc2d1	migrační
krize	krize	k1gFnSc2	krize
otištěné	otištěný	k2eAgFnSc2d1	otištěná
v	v	k7c6	v
deníku	deník	k1gInSc6	deník
Financial	Financial	k1gMnSc1	Financial
Times	Times	k1gMnSc1	Times
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
totiž	totiž	k9	totiž
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
Financial	Financial	k1gInSc4	Financial
Times	Times	k1gMnSc1	Times
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ekonomičtí	ekonomický	k2eAgMnPc1d1	ekonomický
migranti	migrant	k1gMnPc1	migrant
byli	být	k5eAaImAgMnP	být
deportováni	deportovat	k5eAaBmNgMnP	deportovat
na	na	k7c4	na
"	"	kIx"	"
<g/>
neobydlené	obydlený	k2eNgInPc4d1	neobydlený
řecké	řecký	k2eAgInPc4d1	řecký
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
prázdná	prázdný	k2eAgNnPc4d1	prázdné
místa	místo	k1gNnPc4	místo
<g/>
"	"	kIx"	"
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
<g/>
Před	před	k7c7	před
prezidentskými	prezidentský	k2eAgFnPc7d1	prezidentská
volbami	volba	k1gFnPc7	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
označily	označit	k5eAaPmAgInP	označit
zahraniční	zahraniční	k2eAgInPc1d1	zahraniční
deníky	deník	k1gInPc1	deník
The	The	k1gMnPc2	The
Guardian	Guardiany	k1gInPc2	Guardiany
a	a	k8xC	a
Der	drát	k5eAaImRp2nS	drát
Tagesspiegel	Tagesspiegel	k1gInSc4	Tagesspiegel
prezidenta	prezident	k1gMnSc2	prezident
Zemana	Zeman	k1gMnSc2	Zeman
za	za	k7c4	za
populistu	populista	k1gMnSc4	populista
a	a	k8xC	a
hulváta	hulvát	k1gMnSc4	hulvát
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
německého	německý	k2eAgInSc2d1	německý
deníku	deník	k1gInSc2	deník
Der	drát	k5eAaImRp2nS	drát
Tagesspiegel	Tagesspiegel	k1gInSc4	Tagesspiegel
podněcuje	podněcovat	k5eAaImIp3nS	podněcovat
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
strach	strach	k1gInSc4	strach
pomocí	pomocí	k7c2	pomocí
konspiračních	konspirační	k2eAgFnPc2d1	konspirační
teorií	teorie	k1gFnPc2	teorie
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
je	být	k5eAaImIp3nS	být
konkrétně	konkrétně	k6eAd1	konkrétně
zmiňován	zmiňován	k2eAgInSc4d1	zmiňován
Zemanův	Zemanův	k2eAgInSc4d1	Zemanův
výrok	výrok	k1gInSc4	výrok
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
migrační	migrační	k2eAgFnSc7d1	migrační
krizí	krize	k1gFnSc7	krize
stojí	stát	k5eAaImIp3nS	stát
Muslimské	muslimský	k2eAgNnSc4d1	muslimské
bratrstvo	bratrstvo	k1gNnSc4	bratrstvo
<g/>
.	.	kIx.	.
<g/>
Švédský	švédský	k2eAgMnSc1d1	švédský
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Carl	Carl	k1gMnSc1	Carl
Bildt	Bildt	k1gMnSc1	Bildt
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
slova	slovo	k1gNnSc2	slovo
českého	český	k2eAgMnSc2d1	český
prezidenta	prezident	k1gMnSc2	prezident
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
o	o	k7c4	o
krizi	krize	k1gFnSc4	krize
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
chce	chtít	k5eAaImIp3nS	chtít
vidět	vidět	k5eAaImF	vidět
jasné	jasný	k2eAgInPc4d1	jasný
důkazy	důkaz	k1gInPc4	důkaz
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rusko	Rusko	k1gNnSc1	Rusko
posílá	posílat	k5eAaImIp3nS	posílat
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
separatistů	separatista	k1gMnPc2	separatista
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
vojáky	voják	k1gMnPc4	voják
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
švédského	švédský	k2eAgMnSc2d1	švédský
ministra	ministr	k1gMnSc2	ministr
takové	takový	k3xDgFnSc6	takový
důkazy	důkaz	k1gInPc4	důkaz
již	již	k9	již
existují	existovat	k5eAaImIp3nP	existovat
<g/>
.	.	kIx.	.
<g/>
Zahraniční	zahraniční	k2eAgFnPc1d1	zahraniční
i	i	k8xC	i
domácí	domácí	k2eAgFnPc1d1	domácí
nevládní	vládní	k2eNgFnPc1d1	nevládní
organizace	organizace	k1gFnPc1	organizace
zabývající	zabývající	k2eAgFnPc1d1	zabývající
se	se	k3xPyFc4	se
lidskými	lidský	k2eAgNnPc7d1	lidské
právy	právo	k1gNnPc7	právo
kritizují	kritizovat	k5eAaImIp3nP	kritizovat
pozvání	pozvání	k1gNnPc1	pozvání
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
prezidenta	prezident	k1gMnSc2	prezident
Uzbekistánu	Uzbekistán	k1gInSc2	Uzbekistán
Islama	Islam	k1gMnSc2	Islam
Karimova	Karimův	k2eAgMnSc2d1	Karimův
na	na	k7c4	na
státní	státní	k2eAgFnSc4d1	státní
návštěvu	návštěva	k1gFnSc4	návštěva
do	do	k7c2	do
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnPc1	organizace
upozorňují	upozorňovat	k5eAaImIp3nP	upozorňovat
na	na	k7c4	na
porušování	porušování	k1gNnPc4	porušování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
režimem	režim	k1gInSc7	režim
prezidenta	prezident	k1gMnSc2	prezident
Karimova	Karimův	k2eAgFnSc1d1	Karimova
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
vládne	vládnout	k5eAaImIp3nS	vládnout
nepřetržitě	přetržitě	k6eNd1	přetržitě
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
vzbudil	vzbudit	k5eAaPmAgMnS	vzbudit
velké	velký	k2eAgFnPc4d1	velká
vášně	vášeň	k1gFnPc4	vášeň
prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
prohlášením	prohlášení	k1gNnSc7	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
anexe	anexe	k1gFnSc1	anexe
Krymu	Krym	k1gInSc2	Krym
je	být	k5eAaImIp3nS	být
hotovou	hotový	k2eAgFnSc7d1	hotová
záležitostí	záležitost	k1gFnSc7	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Vystoupení	vystoupení	k1gNnSc1	vystoupení
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
kritiku	kritika	k1gFnSc4	kritika
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
i	i	k8xC	i
českých	český	k2eAgMnPc2d1	český
politiků	politik	k1gMnPc2	politik
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
analytika	analytik	k1gMnSc2	analytik
výzkumného	výzkumný	k2eAgNnSc2d1	výzkumné
centra	centrum	k1gNnSc2	centrum
Asociace	asociace	k1gFnSc2	asociace
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
otázky	otázka	k1gFnPc4	otázka
Pavla	Pavel	k1gMnSc2	Pavel
Havlíčka	Havlíček	k1gMnSc2	Havlíček
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
Zemanových	Zemanových	k2eAgNnPc2d1	Zemanových
slov	slovo	k1gNnPc2	slovo
patrná	patrný	k2eAgFnSc1d1	patrná
jasná	jasný	k2eAgFnSc1d1	jasná
proruská	proruský	k2eAgFnSc1d1	proruská
orientace	orientace	k1gFnSc1	orientace
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
amerického	americký	k2eAgMnSc2d1	americký
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
Schapira	Schapiro	k1gNnSc2	Schapiro
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
téměř	téměř	k6eAd1	téměř
jediný	jediný	k2eAgMnSc1d1	jediný
z	z	k7c2	z
velvyslanců	velvyslanec	k1gMnPc2	velvyslanec
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
státního	státní	k2eAgInSc2d1	státní
svátku	svátek	k1gInSc2	svátek
28.	[number]	k4	28.
října	říjen	k1gInSc2	říjen
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
slova	slovo	k1gNnPc1	slovo
nebyla	být	k5eNaImAgFnS	být
pravdivá	pravdivý	k2eAgNnPc1d1	pravdivé
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
nim	on	k3xPp3gInPc3	on
ohradila	ohradit	k5eAaPmAgFnS	ohradit
americká	americký	k2eAgFnSc1d1	americká
ambasáda	ambasáda	k1gFnSc1	ambasáda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
Schapiro	Schapiro	k1gNnSc4	Schapiro
byl	být	k5eAaImAgMnS	být
28.	[number]	k4	28.
října	říjen	k1gInSc2	říjen
mezi	mezi	k7c4	mezi
hosty	host	k1gMnPc4	host
ve	v	k7c6	v
Vladislavském	vladislavský	k2eAgInSc6d1	vladislavský
sále	sál	k1gInSc6	sál
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
také	také	k9	také
fotografie	fotografia	k1gFnPc4	fotografia
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
<g/>
.	.	kIx.	.
<g/>
Řada	řada	k1gFnSc1	řada
zahraničních	zahraniční	k2eAgNnPc2d1	zahraniční
médií	médium	k1gNnPc2	médium
si	se	k3xPyFc3	se
všimla	všimnout	k5eAaPmAgFnS	všimnout
výroku	výrok	k1gInSc3	výrok
prezidenta	prezident	k1gMnSc2	prezident
Miloše	Miloš	k1gMnSc2	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
o	o	k7c6	o
"	"	kIx"	"
<g/>
likvidaci	likvidace	k1gFnSc6	likvidace
novinářů	novinář	k1gMnPc2	novinář
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pronesl	pronést	k5eAaPmAgMnS	pronést
během	během	k7c2	během
setkání	setkání	k1gNnSc2	setkání
s	s	k7c7	s
Vladimirem	Vladimir	k1gMnSc7	Vladimir
Putinem	Putin	k1gMnSc7	Putin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
ZEMAN	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
.	.	kIx.	.
</s>
<s>
Futurologický	futurologický	k2eAgInSc1d1	futurologický
sborník	sborník	k1gInSc1	sborník
<g/>
.	.	kIx.	.
</s>
<s>
D.	D.	kA	D.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Futurologická	futurologický	k2eAgFnSc1d1	futurologická
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
1968.	[number]	k4	1968.
185	[number]	k4	185
s	s	k7c7	s
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZEMAN	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
.	.	kIx.	.
</s>
<s>
Komplexní	komplexní	k2eAgNnSc4d1	komplexní
prognostické	prognostický	k2eAgNnSc4d1	prognostické
modelování	modelování	k1gNnSc4	modelování
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Institut	institut	k1gInSc1	institut
pro	pro	k7c4	pro
výchovu	výchova	k1gFnSc4	výchova
vedoucích	vedoucí	k2eAgMnPc2d1	vedoucí
pracovníků	pracovník	k1gMnPc2	pracovník
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
průmyslu	průmysl	k1gInSc2	průmysl
ČSR	ČSR	kA	ČSR
<g/>
,,	,,	k?	,,
1975.	[number]	k4	1975.
104	[number]	k4	104
s	s	k7c7	s
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZEMAN	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
.	.	kIx.	.
</s>
<s>
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
systémové	systémový	k2eAgFnSc2d1	systémová
prognostiky	prognostika	k1gFnSc2	prognostika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Institut	institut	k1gInSc1	institut
pro	pro	k7c4	pro
výchovu	výchova	k1gFnSc4	výchova
vedoucích	vedoucí	k2eAgMnPc2d1	vedoucí
pracovníků	pracovník	k1gMnPc2	pracovník
MP	MP	kA	MP
ČSR	ČSR	kA	ČSR
<g/>
,	,	kIx,	,
1985.	[number]	k4	1985.
87	[number]	k4	87
s	s	k7c7	s
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZEMAN	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
;	;	kIx,	;
ŠEBELÍK	ŠEBELÍK	kA	ŠEBELÍK
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Simulační	simulační	k2eAgInSc1d1	simulační
model	model	k1gInSc1	model
zemědělského	zemědělský	k2eAgInSc2d1	zemědělský
podniku	podnik	k1gInSc2	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SZN	SZN	kA	SZN
<g/>
,	,	kIx,	,
1988.	[number]	k4	1988.
143	[number]	k4	143
s	s	k7c7	s
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZEMAN	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
.	.	kIx.	.
</s>
<s>
Analytické	analytický	k2eAgFnPc1d1	analytická
a	a	k8xC	a
simulační	simulační	k2eAgFnPc1d1	simulační
hry	hra	k1gFnPc1	hra
na	na	k7c6	na
počítačovém	počítačový	k2eAgInSc6d1	počítačový
modelu	model	k1gInSc6	model
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SZN	SZN	kA	SZN
<g/>
,	,	kIx,	,
1989.	[number]	k4	1989.
88	[number]	k4	88
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-85000-48-2	[number]	k4	80-85000-48-2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZEMAN	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gFnSc1	náš
posttotalitní	posttotalitní	k2eAgFnSc1d1	posttotalitní
krize	krize	k1gFnSc1	krize
a	a	k8xC	a
její	její	k3xOp3gNnPc4	její
možná	možný	k2eAgNnPc4d1	možné
východiska	východisko	k1gNnPc4	východisko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Alternativy	alternativa	k1gFnSc2	alternativa
<g/>
,	,	kIx,	,
1991.	[number]	k4	1991.
82	[number]	k4	82
s	s	k7c7	s
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZEMAN	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
;	;	kIx,	;
BAUER	Bauer	k1gMnSc1	Bauer
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Zpověď	zpověď	k1gFnSc1	zpověď
bývalého	bývalý	k2eAgMnSc2d1	bývalý
prognostika	prognostik	k1gMnSc2	prognostik
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Dekon	Dekon	k1gNnSc1	Dekon
<g/>
,	,	kIx,	,
1995.	[number]	k4	1995.
211	[number]	k4	211
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-85814-23-4	[number]	k4	80-85814-23-4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZEMAN	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
.	.	kIx.	.
</s>
<s>
Varovná	varovný	k2eAgFnSc1d1	varovná
prognostika	prognostika	k1gFnSc1	prognostika
<g/>
:	:	kIx,	:
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
nevyšla	vyjít	k5eNaPmAgFnS	vyjít
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Horizont	horizont	k1gInSc1	horizont
<g/>
,	,	kIx,	,
1998.	[number]	k4	1998.
199	[number]	k4	199
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7012-095-9	[number]	k4	80-7012-095-9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZEMAN	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yQnSc1	kdo
je	být	k5eAaImIp3nS	být
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
exkluzivní	exkluzivní	k2eAgFnSc4d1	exkluzivní
výpověď	výpověď	k1gFnSc4	výpověď
/	/	kIx~	/
otázky	otázka	k1gFnSc2	otázka
kladl	klást	k5eAaImAgMnS	klást
Josef	Josef	k1gMnSc1	Josef
Brož	Brož	k1gMnSc1	Brož
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Rybka	Rybka	k1gMnSc1	Rybka
<g/>
,	,	kIx,	,
1998.	[number]	k4	1998.
143	[number]	k4	143
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-86182-08-8	[number]	k4	80-86182-08-8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZEMAN	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
;	;	kIx,	;
JÜNGLING	JÜNGLING	kA	JÜNGLING
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
ŽANTOVSKÝ	Žantovský	k1gMnSc1	Žantovský
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
pravil	pravit	k5eAaImAgMnS	pravit
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
aneb	aneb	k?	aneb
Prognostikovo	prognostikův	k2eAgNnSc1d1	prognostikův
pozdní	pozdní	k2eAgNnSc1d1	pozdní
odpoledne	odpoledne	k1gNnSc1	odpoledne
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Votobia	Votobia	k1gFnSc1	Votobia
<g/>
,	,	kIx,	,
2001.	[number]	k4	2001.
139	[number]	k4	139
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7220-104-2	[number]	k4	80-7220-104-2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZEMAN	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
mýlil	mýlit	k5eAaImAgMnS	mýlit
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakadatelství	nakadatelství	k1gNnSc1	nakadatelství
<g/>
,	,	kIx,	,
2005.	[number]	k4	2005.
344	[number]	k4	344
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7360-260-1	[number]	k4	80-7360-260-1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZEMAN	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
.	.	kIx.	.
</s>
<s>
Vzestup	vzestup	k1gInSc1	vzestup
a	a	k8xC	a
pád	pád	k1gInSc1	pád
české	český	k2eAgFnSc2d1	Česká
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Andrej	Andrej	k1gMnSc1	Andrej
Št	Št	k1gMnSc1	Št
<g/>
'	'	kIx"	'
<g/>
astný	astný	k1gMnSc1	astný
<g/>
,	,	kIx,	,
2006.	[number]	k4	2006.
279	[number]	k4	279
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-86739-22-8	[number]	k4	80-86739-22-8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZEMAN	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
;	;	kIx,	;
DZURINDA	DZURINDA	kA	DZURINDA
<g/>
,	,	kIx,	,
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
<g/>
.	.	kIx.	.
</s>
<s>
Rozhovory	rozhovor	k1gInPc1	rozhovor
bez	bez	k7c2	bez
hraníc	hranit	k5eAaImSgNnS	hranit
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Inštitút	Inštitút	k1gMnSc1	Inštitút
pre	pre	k?	pre
moderné	moderný	k2eAgNnSc1d1	moderné
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
,	,	kIx,	,
2010.	[number]	k4	2010.
281	[number]	k4	281
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-89359-18-9	[number]	k4	978-80-89359-18-9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZEMAN	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
;	;	kIx,	;
ŽANTOVSKÝ	Žantovský	k1gMnSc1	Žantovský
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
:	:	kIx,	:
zpověď	zpověď	k1gFnSc1	zpověď
informovaného	informovaný	k2eAgMnSc2d1	informovaný
optimisty	optimista	k1gMnSc2	optimista
<g/>
:	:	kIx,	:
rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
Žantovským	Žantovský	k1gMnSc7	Žantovský
<g/>
.	.	kIx.	.
</s>
<s>
Řitka	Řitka	k1gFnSc1	Řitka
<g/>
:	:	kIx,	:
Čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
2012.	[number]	k4	2012.
221	[number]	k4	221
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-87470-85-5	[number]	k4	978-80-87470-85-5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZEMAN	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
;	;	kIx,	;
OVČÁČEK	ovčáček	k1gMnSc1	ovčáček
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
PANENKA	panenka	k1gFnSc1	panenka
<g/>
,	,	kIx,	,
Radim	Radim	k1gMnSc1	Radim
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
naše	náš	k3xOp1gFnSc1	náš
<g/>
:	:	kIx,	:
dvacet	dvacet	k4xCc1	dvacet
pět	pět	k4xCc1	pět
rozhovorů	rozhovor	k1gInPc2	rozhovor
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
Milošem	Miloš	k1gMnSc7	Miloš
Zemanem	Zeman	k1gMnSc7	Zeman
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgInPc1d1	velký
Přílepy	přílep	k1gInPc1	přílep
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
2016.	[number]	k4	2016.
300	[number]	k4	300
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-7376-442-5	[number]	k4	978-80-7376-442-5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZEMAN	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
.	.	kIx.	.
</s>
<s>
Moje	můj	k3xOp1gFnSc1	můj
Vysočina	vysočina	k1gFnSc1	vysočina
pohledem	pohled	k1gInSc7	pohled
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Atypo	Atypa	k1gFnSc5	Atypa
<g/>
,	,	kIx,	,
2016.	[number]	k4	2016.
128	[number]	k4	128
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-87759-02-8	[number]	k4	978-80-87759-02-8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odraz	odraz	k1gInSc4	odraz
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
režisér	režisér	k1gMnSc1	režisér
Robert	Robert	k1gMnSc1	Robert
Sedláček	Sedláček	k1gMnSc1	Sedláček
natočil	natočit	k5eAaBmAgMnS	natočit
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
televizi	televize	k1gFnSc4	televize
Brno	Brno	k1gNnSc1	Brno
dokument	dokument	k1gInSc1	dokument
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
–	–	k?	–
nekrolog	nekrolog	k1gInSc1	nekrolog
politika	politika	k1gFnSc1	politika
a	a	k8xC	a
oslava	oslava	k1gFnSc1	oslava
Vysočiny	vysočina	k1gFnSc2	vysočina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
student	student	k1gMnSc1	student
FAMU	FAMU	kA	FAMU
Robin	robin	k2eAgMnSc1d1	robin
Kvapil	Kvapil	k1gMnSc1	Kvapil
natočil	natočit	k5eAaBmAgMnS	natočit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
cyklu	cyklus	k1gInSc2	cyklus
Expremiéři	expremiér	k1gMnPc1	expremiér
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
portrét	portrét	k1gInSc4	portrét
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
–	–	k?	–
Kronos	Kronos	k1gMnSc1	Kronos
český	český	k2eAgMnSc1d1	český
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Především	především	k9	především
jako	jako	k9	jako
reakci	reakce	k1gFnSc4	reakce
na	na	k7c4	na
kauzu	kauza	k1gFnSc4	kauza
Brady	brada	k1gFnSc2	brada
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
plzeňský	plzeňský	k2eAgMnSc1d1	plzeňský
sochař	sochař	k1gMnSc1	sochař
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Netrval	trvat	k5eNaImAgMnS	trvat
karikatury	karikatura	k1gFnPc4	karikatura
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
<g/>
,	,	kIx,	,
prezidentova	prezidentův	k2eAgMnSc2d1	prezidentův
mluvčího	mluvčí	k1gMnSc2	mluvčí
Jiřího	Jiří	k1gMnSc2	Jiří
Ovčáčka	ovčáček	k1gMnSc2	ovčáček
a	a	k8xC	a
ministra	ministr	k1gMnSc2	ministr
Adreje	Adrej	k1gMnSc2	Adrej
Babiše	Babiš	k1gMnSc2	Babiš
<g/>
.	.	kIx.	.
</s>
<s>
Výstava	výstava	k1gFnSc1	výstava
politických	politický	k2eAgFnPc2d1	politická
karikatur	karikatura	k1gFnPc2	karikatura
v	v	k7c6	v
plzeňské	plzeňský	k2eAgFnSc6d1	Plzeňská
Visio	Visio	k6eAd1	Visio
Art	Art	k1gFnSc7	Art
Gallery	Galler	k1gInPc4	Galler
<g/>
,	,	kIx,	,
zahájená	zahájený	k2eAgNnPc1d1	zahájené
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
silnou	silný	k2eAgFnSc4d1	silná
kritiku	kritika	k1gFnSc4	kritika
příznivců	příznivec	k1gMnPc2	příznivec
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
někteří	některý	k3yIgMnPc1	některý
vyhrožovali	vyhrožovat	k5eAaImAgMnP	vyhrožovat
autorovi	autorův	k2eAgMnPc1d1	autorův
karikatur	karikatura	k1gFnPc2	karikatura
smrtí	smrt	k1gFnPc2	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Kdo	kdo	k3yRnSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yQnSc1	kdo
:	:	kIx,	:
91/92	[number]	k4	91/92
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
federální	federální	k2eAgInPc4d1	federální
orgány	orgán	k1gInPc4	orgán
ČSFR	ČSFR	kA	ČSFR
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
2	[number]	k4	2
<g/>
,	,	kIx,	,
N	N	kA	N
<g/>
–	–	k?	–
<g/>
Ž.	Ž.	kA	Ž.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Kdo	kdo	k3yRnSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yRnSc1	kdo
<g/>
,	,	kIx,	,
1991.	[number]	k4	1991.
637	[number]	k4	637
<g/>
–	–	k?	–
<g/>
1298	[number]	k4	1298
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-901103-0-4	[number]	k4	80-901103-0-4
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
1130.	[number]	k4	1130.
</s>
</p>
<p>
<s>
Kdo	kdo	k3yQnSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yRnSc1	kdo
=	=	kIx~	=
Who	Who	k1gMnPc1	Who
is	is	k?	is
who	who	k?	who
:	:	kIx,	:
osobnosti	osobnost	k1gFnPc4	osobnost
české	český	k2eAgFnSc2d1	Česká
současnosti	současnost	k1gFnSc2	současnost
:	:	kIx,	:
5000	[number]	k4	5000
životopisů	životopis	k1gInPc2	životopis
/	/	kIx~	/
(	(	kIx(	(
<g/>
Michael	Michael	k1gMnSc1	Michael
Třeštík	Třeštík	k1gMnSc1	Třeštík
editor	editor	k1gMnSc1	editor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
5.	[number]	k4	5.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Agentura	agentura	k1gFnSc1	agentura
Kdo	kdo	k3yQnSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yQnSc1	kdo
<g/>
,	,	kIx,	,
2005.	[number]	k4	2005.
775	[number]	k4	775
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-902586-9-7	[number]	k4	80-902586-9-7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
759.	[number]	k4	759.
</s>
</p>
<p>
<s>
Osobnosti	osobnost	k1gFnPc1	osobnost
-	-	kIx~	-
Česko	Česko	k1gNnSc1	Česko
:	:	kIx,	:
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2008.	[number]	k4	2008.
823	[number]	k4	823
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-7360-796-8	[number]	k4	978-80-7360-796-8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
811.	[number]	k4	811.
</s>
</p>
<p>
<s>
Zeman	Zeman	k1gMnSc1	Zeman
:	:	kIx,	:
skutečný	skutečný	k2eAgInSc1d1	skutečný
příběh	příběh	k1gInSc1	příběh
českého	český	k2eAgMnSc2d1	český
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ústav	ústav	k1gInSc1	ústav
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
žurnalistiky	žurnalistika	k1gFnSc2	žurnalistika
<g/>
,	,	kIx,	,
z	z	k7c2	z
<g/>
.	.	kIx.	.
<g/>
ú	ú	k0	ú
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2017.	[number]	k4	2017.
102	[number]	k4	102
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-270-1727-0	[number]	k4	978-80-270-1727-0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČERMÁK	Čermák	k1gMnSc1	Čermák
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Pane	Pan	k1gMnSc5	Pan
prezidente	prezident	k1gMnSc5	prezident
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
uděláte	udělat	k5eAaPmIp2nP	udělat
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Imagination	Imagination	k1gInSc1	Imagination
of	of	k?	of
People	People	k1gFnSc2	People
<g/>
,	,	kIx,	,
2013.	[number]	k4	2013.
191	[number]	k4	191
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-87685-12-9	[number]	k4	978-80-87685-12-9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOMEŠ	Tomeš	k1gMnSc1	Tomeš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
XX	XX	kA	XX
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
:	:	kIx,	:
III	III	kA	III
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
:	:	kIx,	:
Q	Q	kA	Q
<g/>
–	–	k?	–
<g/>
Ž.	Ž.	kA	Ž.
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
;	;	kIx,	;
Petr	Petr	k1gMnSc1	Petr
Meissner	Meissner	k1gMnSc1	Meissner
<g/>
,	,	kIx,	,
1999.	[number]	k4	1999.
587	[number]	k4	587
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7185-247-3	[number]	k4	80-7185-247-3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
559	[number]	k4	559
<g/>
–	–	k?	–
<g/>
560	[number]	k4	560
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Inaugurace	inaugurace	k1gFnPc1	inaugurace
prezidenta	prezident	k1gMnSc2	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
předsedů	předseda	k1gMnPc2	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
premiérů	premiér	k1gMnPc2	premiér
Česka	Česko	k1gNnSc2	Česko
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
prezidentů	prezident	k1gMnPc2	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
</s>
</p>
<p>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
–	–	k?	–
Oficiální	oficiální	k2eAgInSc4d1	oficiální
životopis	životopis	k1gInSc4	životopis
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Úřadu	úřad	k1gInSc2	úřad
vlády	vláda	k1gFnSc2	vláda
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
vlada.cz	vlada.cz	k1gMnSc1	vlada.cz
</s>
</p>
<p>
<s>
Životní	životní	k2eAgInSc1d1	životní
příběh	příběh	k1gInSc1	příběh
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
<g/>
,	,	kIx,	,
zemannahrad.cz	zemannahrad.cz	k1gInSc1	zemannahrad.cz
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
</s>
</p>
<p>
<s>
Milan	Milan	k1gMnSc1	Milan
Churaň	Churaň	k1gMnSc1	Churaň
<g/>
:	:	kIx,	:
ZEMAN	Zeman	k1gMnSc1	Zeman
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
v	v	k7c6	v
encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
Kdo	kdo	k3yQnSc1	kdo
byl	být	k5eAaImAgMnS	být
kdo	kdo	k3yInSc1	kdo
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
dějinách	dějiny	k1gFnPc6	dějiny
ve	v	k7c6	v
20.	[number]	k4	20.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
libri.cz	libri.cz	k1gMnSc1	libri.cz
</s>
</p>
<p>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
–	–	k?	–
nekrolog	nekrolog	k1gInSc1	nekrolog
politika	politika	k1gFnSc1	politika
a	a	k8xC	a
oslava	oslava	k1gFnSc1	oslava
Vysočiny	vysočina	k1gFnSc2	vysočina
<g/>
,	,	kIx,	,
27.	[number]	k4	27.
12.	[number]	k4	12.
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
ceskatelevize.cz	ceskatelevize.cz	k1gInSc1	ceskatelevize.cz
</s>
</p>
<p>
<s>
Velké	velký	k2eAgNnSc4d1	velké
politické	politický	k2eAgNnSc4d1	politické
retro	retro	k1gNnSc4	retro
–	–	k?	–
pořad	pořad	k1gInSc1	pořad
Naostro	naostro	k6eAd1	naostro
ČT	ČT	kA	ČT
z	z	k7c2	z
9.	[number]	k4	9.
6.	[number]	k4	6.
2002	[number]	k4	2002
(	(	kIx(	(
<g/>
televizní	televizní	k2eAgFnSc1d1	televizní
debata	debata	k1gFnSc1	debata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ceskatelevize.cz	ceskatelevize.cz	k1gMnSc1	ceskatelevize.cz
</s>
</p>
<p>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
:	:	kIx,	:
the	the	k?	the
hardline	hardlin	k1gInSc5	hardlin
Czech	Czech	k1gMnSc1	Czech
leader	leader	k1gMnSc1	leader
fanning	fanning	k1gInSc4	fanning
hostility	hostilita	k1gFnSc2	hostilita
to	ten	k3xDgNnSc1	ten
refugees	refugees	k1gInSc1	refugees
–	–	k?	–
rozhovor	rozhovor	k1gInSc4	rozhovor
pro	pro	k7c4	pro
Guardian	Guardian	k1gInSc4	Guardian
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
