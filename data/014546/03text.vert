<s>
Univerzita	univerzita	k1gFnSc1
Tomáše	Tomáš	k1gMnSc2
Bati	Baťa	k1gMnSc2
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Tomáše	Tomáš	k1gMnSc2
Bati	Baťa	k1gMnSc2
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
Logo	logo	k1gNnSc4
Budova	budova	k1gFnSc1
rektorátu	rektorát	k1gInSc2
a	a	k8xC
univerzitní	univerzitní	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
Zkratka	zkratka	k1gFnSc1
</s>
<s>
UTB	UTB	kA
Rok	rok	k1gInSc1
založení	založení	k1gNnSc2
</s>
<s>
2001	#num#	k4
Typ	typ	k1gInSc1
školy	škola	k1gFnSc2
</s>
<s>
veřejná	veřejný	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vedení	vedení	k1gNnSc1
Rektor	rektor	k1gMnSc1
</s>
<s>
prof.	prof.	kA
Ing.	ing.	kA
Vladimír	Vladimír	k1gMnSc1
Sedlařík	Sedlařík	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Kvestor	kvestor	k1gMnSc1
</s>
<s>
RNDr.	RNDr.	kA
Alexander	Alexandra	k1gFnPc2
Černý	Černý	k1gMnSc1
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
tvůrčí	tvůrčí	k2eAgFnPc4d1
činnosti	činnost	k1gFnPc4
</s>
<s>
prof.	prof.	kA
Ing.	ing.	kA
Petr	Petr	k1gMnSc1
Sáha	sáha	k1gFnSc1
<g/>
,	,	kIx,
CSc.	CSc.	kA
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
pedagogickou	pedagogický	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
</s>
<s>
Ing.	ing.	kA
Lubomír	Lubomír	k1gMnSc1
Beníček	Beníček	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
vnější	vnější	k2eAgInPc4d1
a	a	k8xC
vnitřní	vnitřní	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
</s>
<s>
doc.	doc.	kA
Ing.	ing.	kA
Adriana	Adriana	k1gFnSc1
Knápková	Knápková	k1gFnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
internacionalizaci	internacionalizace	k1gFnSc4
</s>
<s>
Ing.	ing.	kA
Michaela	Michaela	k1gFnSc1
Blahová	Blahová	k1gFnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
kvalitu	kvalita	k1gFnSc4
</s>
<s>
Mgr.	Mgr.	kA
Jan	Jan	k1gMnSc1
Kalenda	Kalenda	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Počty	počet	k1gInPc1
akademiků	akademik	k1gMnPc2
(	(	kIx(
<g/>
k	k	k7c3
roku	rok	k1gInSc3
2016	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
Počet	počet	k1gInSc1
bakalářských	bakalářský	k2eAgMnPc2d1
studentů	student	k1gMnPc2
</s>
<s>
6	#num#	k4
123	#num#	k4
Počet	počet	k1gInSc4
magisterských	magisterský	k2eAgMnPc2d1
studentů	student	k1gMnPc2
</s>
<s>
2	#num#	k4
668	#num#	k4
Počet	počet	k1gInSc4
doktorandů	doktorand	k1gMnPc2
</s>
<s>
422	#num#	k4
Počet	počet	k1gInSc4
akademických	akademický	k2eAgMnPc2d1
pracovníků	pracovník	k1gMnPc2
</s>
<s>
513	#num#	k4
Další	další	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Počet	počet	k1gInSc1
fakult	fakulta	k1gFnPc2
</s>
<s>
6	#num#	k4
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Zlín	Zlín	k1gInSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Nám.	Nám.	k?
T.	T.	kA
G.	G.	kA
Masaryka	Masaryk	k1gMnSc2
5555	#num#	k4
<g/>
,	,	kIx,
Zlín	Zlín	k1gInSc1
<g/>
,	,	kIx,
760	#num#	k4
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
13	#num#	k4
<g/>
′	′	k?
<g/>
21,56	21,56	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
39	#num#	k4
<g/>
′	′	k?
<g/>
53,48	53,48	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
http://www.utb.cz/	http://www.utb.cz/	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Tomáše	Tomáš	k1gMnSc2
Bati	Baťa	k1gMnSc2
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
(	(	kIx(
<g/>
UTB	UTB	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
veřejná	veřejný	k2eAgFnSc1d1
vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
univerzitního	univerzitní	k2eAgInSc2d1
typu	typ	k1gInSc2
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
šest	šest	k4xCc4
samostatných	samostatný	k2eAgFnPc2d1
fakult	fakulta	k1gFnPc2
v	v	k7c6
rámci	rámec	k1gInSc6
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nabízí	nabízet	k5eAaImIp3nS
studium	studium	k1gNnSc4
technických	technický	k2eAgInPc2d1
a	a	k8xC
technologických	technologický	k2eAgInPc2d1
<g/>
,	,	kIx,
ekonomických	ekonomický	k2eAgInPc2d1
<g/>
,	,	kIx,
uměleckých	umělecký	k2eAgInPc2d1
<g/>
,	,	kIx,
informatických	informatický	k2eAgInPc2d1
<g/>
,	,	kIx,
humanitních	humanitní	k2eAgInPc2d1
i	i	k8xC
zdravotnických	zdravotnický	k2eAgInPc2d1
oborů	obor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
Tomáše	Tomáš	k1gMnSc2
Bati	Baťa	k1gMnSc2
navazuje	navazovat	k5eAaImIp3nS
na	na	k7c4
tradici	tradice	k1gFnSc4
Fakulty	fakulta	k1gFnSc2
technologické	technologický	k2eAgFnSc2d1
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
Vysokého	vysoký	k2eAgNnSc2d1
učení	učení	k1gNnSc2
technického	technický	k2eAgNnSc2d1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
zde	zde	k6eAd1
existovala	existovat	k5eAaImAgFnS
od	od	k7c2
roku	rok	k1gInSc2
1969	#num#	k4
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
2001	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
zákonem	zákon	k1gInSc7
404	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
vznikla	vzniknout	k5eAaPmAgFnS
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc6
2001	#num#	k4
samostatná	samostatný	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pojmenována	pojmenován	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
po	po	k7c6
zakladateli	zakladatel	k1gMnSc6
obuvnické	obuvnický	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
Tomáši	Tomáš	k1gMnSc3
Baťovi	Baťa	k1gMnSc3
(	(	kIx(
<g/>
1876	#num#	k4
<g/>
-	-	kIx~
<g/>
1932	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
šesti	šest	k4xCc6
fakultách	fakulta	k1gFnPc6
UTB	UTB	kA
studuje	studovat	k5eAaImIp3nS
přes	přes	k7c4
9000	#num#	k4
posluchačů	posluchač	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodní	mezinárodní	k2eAgInSc1d1
prostředí	prostředí	k1gNnSc2
pomáhají	pomáhat	k5eAaImIp3nP
vytvářet	vytvářet	k5eAaImF
zahraniční	zahraniční	k2eAgMnPc1d1
studenti	student	k1gMnPc1
<g/>
,	,	kIx,
jichž	jenž	k3xRgMnPc2
je	být	k5eAaImIp3nS
na	na	k7c6
UTB	UTB	kA
10	#num#	k4
%	%	kIx~
a	a	k8xC
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Škola	škola	k1gFnSc1
je	být	k5eAaImIp3nS
pravidelně	pravidelně	k6eAd1
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
hodnocena	hodnotit	k5eAaImNgFnS
v	v	k7c6
mezinárodním	mezinárodní	k2eAgNnSc6d1
srovnání	srovnání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
prestižní	prestižní	k2eAgFnSc2d1
britské	britský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
Times	Times	k1gMnSc1
Higher	Highra	k1gFnPc2
Education	Education	k1gInSc4
patří	patřit	k5eAaImIp3nS
UTB	UTB	kA
mezi	mezi	k7c4
40	#num#	k4
nejlepších	dobrý	k2eAgFnPc2d3
světových	světový	k2eAgFnPc2d1
univerzit	univerzita	k1gFnPc2
vzniklých	vzniklý	k2eAgFnPc2d1
po	po	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
žebříčku	žebříček	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
zpracovala	zpracovat	k5eAaPmAgFnS
britská	britský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
QS	QS	kA
Ranking	Ranking	k1gInSc4
<g/>
,	,	kIx,
patří	patřit	k5eAaImIp3nS
univerzitě	univerzita	k1gFnSc6
92	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
mezi	mezi	k7c7
univerzitami	univerzita	k1gFnPc7
střední	střední	k2eAgFnSc2d1
a	a	k8xC
východní	východní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
a	a	k8xC
Střední	střední	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Univerzita	univerzita	k1gFnSc1
nabízí	nabízet	k5eAaImIp3nS
třístupňové	třístupňový	k2eAgNnSc1d1
studium	studium	k1gNnSc1
založené	založený	k2eAgNnSc1d1
na	na	k7c6
kreditovém	kreditový	k2eAgInSc6d1
systému	systém	k1gInSc6
ECTS	ECTS	kA
(	(	kIx(
<g/>
European	European	k1gMnSc1
Credit	Credit	k1gMnSc1
Transfer	transfer	k1gInSc4
System	Syst	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
absolventi	absolvent	k1gMnPc1
UTB	UTB	kA
získávají	získávat	k5eAaImIp3nP
celoevropsky	celoevropsky	k6eAd1
uznávaný	uznávaný	k2eAgMnSc1d1
Diploma	Diplom	k1gMnSc4
supplement	supplement	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
získala	získat	k5eAaPmAgFnS
od	od	k7c2
Evropské	evropský	k2eAgFnSc2d1
komise	komise	k1gFnSc2
certifikát	certifikát	k1gInSc1
Diploma	Diploma	k1gFnSc1
Supplement	Supplement	k1gMnSc1
Label	Label	k1gMnSc1
a	a	k8xC
má	mít	k5eAaImIp3nS
též	též	k9
certifikát	certifikát	k1gInSc1
ECTS	ECTS	kA
Label	Label	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Univerzitu	univerzita	k1gFnSc4
charakterizuje	charakterizovat	k5eAaBmIp3nS
rozvinutá	rozvinutý	k2eAgFnSc1d1
věda	věda	k1gFnSc1
a	a	k8xC
výzkum	výzkum	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgInPc6
směrech	směr	k1gInPc6
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
polymerní	polymerní	k2eAgNnSc1d1
inženýrství	inženýrství	k1gNnSc1
<g/>
,	,	kIx,
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
automatizace	automatizace	k1gFnSc1
a	a	k8xC
řízení	řízení	k1gNnSc1
technologických	technologický	k2eAgInPc2d1
procesů	proces	k1gInPc2
nebo	nebo	k8xC
kreativní	kreativní	k2eAgFnPc4d1
obory	obora	k1gFnPc4
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
UTB	UTB	kA
kredit	kredit	k1gInSc1
i	i	k8xC
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
vznikla	vzniknout	k5eAaPmAgFnS
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
pobočka	pobočka	k1gFnSc1
strojní	strojní	k2eAgFnSc2d1
a	a	k8xC
chemické	chemický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Slovenské	slovenský	k2eAgFnSc2d1
vysoké	vysoký	k2eAgFnSc2d1
školy	škola	k1gFnSc2
technické	technický	k2eAgFnSc2d1
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1963	#num#	k4
přešlo	přejít	k5eAaPmAgNnS
pod	pod	k7c4
Vysoké	vysoký	k2eAgNnSc4d1
učení	učení	k1gNnSc4
technické	technický	k2eAgNnSc4d1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
15	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1969	#num#	k4
byla	být	k5eAaImAgFnS
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
založena	založit	k5eAaPmNgFnS
Fakulta	fakulta	k1gFnSc1
technologická	technologický	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
součást	součást	k1gFnSc1
Vysokého	vysoký	k2eAgNnSc2d1
učení	učení	k1gNnSc2
technického	technický	k2eAgNnSc2d1
v	v	k7c6
Brně	Brno	k1gNnSc6
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
vznikla	vzniknout	k5eAaPmAgFnS
Fakulta	fakulta	k1gFnSc1
managementu	management	k1gInSc2
a	a	k8xC
ekonomiky	ekonomika	k1gFnSc2
opět	opět	k6eAd1
v	v	k7c6
rámci	rámec	k1gInSc6
VUT	VUT	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
pak	pak	k6eAd1
Institut	institut	k1gInSc1
reklamní	reklamní	k2eAgFnSc2d1
tvorby	tvorba	k1gFnSc2
a	a	k8xC
marketingových	marketingový	k2eAgFnPc2d1
komunikací	komunikace	k1gFnPc2
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
2002	#num#	k4
Fakulta	fakulta	k1gFnSc1
multimediálních	multimediální	k2eAgFnPc2d1
komunikací	komunikace	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2000	#num#	k4
podepsal	podepsat	k5eAaPmAgMnS
prezident	prezident	k1gMnSc1
Václav	Václav	k1gMnSc1
Havel	Havel	k1gMnSc1
zákon	zákon	k1gInSc4
404	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
zřízení	zřízení	k1gNnSc6
Univerzity	univerzita	k1gFnSc2
Tomáše	Tomáš	k1gMnSc2
Bati	Baťa	k1gMnSc2
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
oficiálně	oficiálně	k6eAd1
vznikla	vzniknout	k5eAaPmAgFnS
univerzita	univerzita	k1gFnSc1
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc6
2001	#num#	k4
vyčleněním	vyčlenění	k1gNnSc7
majetku	majetek	k1gInSc2
obou	dva	k4xCgFnPc2
fakult	fakulta	k1gFnPc2
z	z	k7c2
vlastnictví	vlastnictví	k1gNnSc2
Vysokého	vysoký	k2eAgNnSc2d1
učení	učení	k1gNnSc2
technického	technický	k2eAgNnSc2d1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Univerzita	univerzita	k1gFnSc1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
rozrůstala	rozrůstat	k5eAaImAgFnS
od	od	k7c2
roku	rok	k1gInSc2
2002	#num#	k4
o	o	k7c4
Fakultu	fakulta	k1gFnSc4
multimediálních	multimediální	k2eAgFnPc2d1
komunikací	komunikace	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
r.	r.	kA
2006	#num#	k4
vznikla	vzniknout	k5eAaPmAgFnS
Fakulta	fakulta	k1gFnSc1
aplikované	aplikovaný	k2eAgFnSc2d1
informatiky	informatika	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
r.	r.	kA
2007	#num#	k4
Fakulta	fakulta	k1gFnSc1
humanitních	humanitní	k2eAgFnPc2d1
studií	studie	k1gFnPc2
a	a	k8xC
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2009	#num#	k4
funguje	fungovat	k5eAaImIp3nS
také	také	k9
Fakulta	fakulta	k1gFnSc1
logistiky	logistika	k1gFnSc2
a	a	k8xC
krizového	krizový	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Uherském	uherský	k2eAgNnSc6d1
Hradišti	Hradiště	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Fakulty	fakulta	k1gFnPc1
</s>
<s>
Fakulta	fakulta	k1gFnSc1
technologická	technologický	k2eAgFnSc1d1
(	(	kIx(
<g/>
založena	založen	k2eAgFnSc1d1
15	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1969	#num#	k4
<g/>
,	,	kIx,
součást	součást	k1gFnSc1
UTB	UTB	kA
od	od	k7c2
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fakulta	fakulta	k1gFnSc1
managementu	management	k1gInSc2
a	a	k8xC
ekonomiky	ekonomika	k1gFnSc2
(	(	kIx(
<g/>
založena	založen	k2eAgFnSc1d1
27	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1995	#num#	k4
<g/>
,	,	kIx,
součást	součást	k1gFnSc1
UTB	UTB	kA
od	od	k7c2
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fakulta	fakulta	k1gFnSc1
multimediálních	multimediální	k2eAgFnPc2d1
komunikací	komunikace	k1gFnPc2
(	(	kIx(
<g/>
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fakulta	fakulta	k1gFnSc1
aplikované	aplikovaný	k2eAgFnSc2d1
informatiky	informatika	k1gFnSc2
(	(	kIx(
<g/>
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fakulta	fakulta	k1gFnSc1
humanitních	humanitní	k2eAgFnPc2d1
studií	studie	k1gFnPc2
(	(	kIx(
<g/>
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fakulta	fakulta	k1gFnSc1
logistiky	logistika	k1gFnSc2
a	a	k8xC
krizového	krizový	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
sídlící	sídlící	k2eAgFnSc1d1
v	v	k7c6
Uherském	uherský	k2eAgNnSc6d1
Hradišti	Hradiště	k1gNnSc6
(	(	kIx(
<g/>
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ubytování	ubytování	k1gNnSc1
studentů	student	k1gMnPc2
</s>
<s>
Pro	pro	k7c4
studenty	student	k1gMnPc4
nabízí	nabízet	k5eAaImIp3nS
univerzita	univerzita	k1gFnSc1
kolejní	kolejní	k2eAgNnSc1d1
ubytování	ubytování	k1gNnSc4
v	v	k7c6
jedno-	jedno-	k?
,	,	kIx,
dvou-	dvou-	k?
nebo	nebo	k8xC
třílůžkových	třílůžkový	k2eAgInPc6d1
pokojích	pokoj	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc4
rok	rok	k1gInSc4
se	se	k3xPyFc4
kapacita	kapacita	k1gFnSc1
zvyšuje	zvyšovat	k5eAaImIp3nS
a	a	k8xC
jsou	být	k5eAaImIp3nP
uvolňovány	uvolňován	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
na	na	k7c4
jejich	jejich	k3xOp3gFnSc4
modernizaci	modernizace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapacita	kapacita	k1gFnSc1
kolejí	kolej	k1gFnPc2
je	být	k5eAaImIp3nS
858	#num#	k4
lůžek	lůžko	k1gNnPc2
pro	pro	k7c4
studenty	student	k1gMnPc4
<g/>
,	,	kIx,
2	#num#	k4
lůžka	lůžko	k1gNnSc2
pro	pro	k7c4
imobilní	imobilní	k2eAgNnSc4d1
<g/>
,	,	kIx,
32	#num#	k4
lůžek	lůžko	k1gNnPc2
pro	pro	k7c4
zaměstnance	zaměstnanec	k1gMnPc4
UTB	UTB	kA
a	a	k8xC
6	#num#	k4
lůžek	lůžko	k1gNnPc2
pro	pro	k7c4
hosty	host	k1gMnPc4
UTB	UTB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Internet	Internet	k1gInSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
všech	všecek	k3xTgFnPc6
kolejích	kolej	k1gFnPc6
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kolej	kolej	k1gFnSc1
Štefánikova	Štefánikův	k2eAgFnSc1d1
-	-	kIx~
196	#num#	k4
lůžek	lůžko	k1gNnPc2
</s>
<s>
Kolej	kolej	k1gFnSc1
Antonínova	Antonínův	k2eAgFnSc1d1
-	-	kIx~
328	#num#	k4
lůžek	lůžko	k1gNnPc2
</s>
<s>
Kolej	kolej	k1gFnSc1
nám.	nám.	k?
T.G.	T.G.	k1gMnSc2
<g/>
Masaryka	Masaryk	k1gMnSc2
-	-	kIx~
334	#num#	k4
lůžek	lůžko	k1gNnPc2
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stravování	stravování	k1gNnSc1
studentů	student	k1gMnPc2
</s>
<s>
Menza	menza	k1gFnSc1
Štefánikova	Štefánikův	k2eAgFnSc1d1
-	-	kIx~
kapacita	kapacita	k1gFnSc1
200	#num#	k4
osob	osoba	k1gFnPc2
</s>
<s>
Menza	menza	k1gFnSc1
Jižní	jižní	k2eAgFnSc1d1
svahy	svah	k1gInPc4
U5	U5	k1gMnPc2
-	-	kIx~
kapacita	kapacita	k1gFnSc1
100	#num#	k4
osob	osoba	k1gFnPc2
(	(	kIx(
<g/>
i	i	k9
s	s	k7c7
venkovním	venkovní	k2eAgNnSc7d1
posezením	posezení	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
Restaurace	restaurace	k1gFnSc1
U13	U13	k1gFnSc1
-	-	kIx~
60	#num#	k4
osob	osoba	k1gFnPc2
(	(	kIx(
<g/>
s	s	k7c7
obsluhou	obsluha	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
Do	do	k7c2
stravovacích	stravovací	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
jsou	být	k5eAaImIp3nP
také	také	k9
zahrnuty	zahrnout	k5eAaPmNgInP
dva	dva	k4xCgInPc1
bufety	bufet	k1gInPc1
na	na	k7c6
Mostní	mostní	k2eAgFnSc6d1
U2	U2	k1gFnSc6
a	a	k8xC
Jižních	jižní	k2eAgInPc6d1
svazích	svah	k1gInPc6
U	u	k7c2
<g/>
5	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Knihovna	knihovna	k1gFnSc1
UTB	UTB	kA
</s>
<s>
Kongresové	kongresový	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
zlín	zlína	k1gFnPc2
(	(	kIx(
<g/>
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
univerzitní	univerzitní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
(	(	kIx(
<g/>
vpravo	vpravo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Knihovna	knihovna	k1gFnSc1
UTB	UTB	kA
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
je	být	k5eAaImIp3nS
veřejně	veřejně	k6eAd1
přístupnou	přístupný	k2eAgFnSc7d1
knihovnou	knihovna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgFnSc2
služby	služba	k1gFnSc2
poskytuje	poskytovat	k5eAaImIp3nS
zejména	zejména	k9
pedagogům	pedagog	k1gMnPc3
a	a	k8xC
studentům	student	k1gMnPc3
UTB	UTB	kA
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
odborné	odborný	k2eAgFnSc3d1
veřejnosti	veřejnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jejích	její	k3xOp3gInPc6
fondech	fond	k1gInPc6
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
knihy	kniha	k1gFnPc1
<g/>
,	,	kIx,
odborné	odborný	k2eAgInPc1d1
časopisy	časopis	k1gInPc1
<g/>
,	,	kIx,
denní	denní	k2eAgInSc1d1
tisk	tisk	k1gInSc1
<g/>
,	,	kIx,
CD	CD	kA
<g/>
,	,	kIx,
audiokazety	audiokazeta	k1gFnSc2
<g/>
,	,	kIx,
videokazety	videokazeta	k1gFnSc2
<g/>
,	,	kIx,
diplomové	diplomový	k2eAgFnPc4d1
a	a	k8xC
disertační	disertační	k2eAgFnPc4d1
práce	práce	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
elektronické	elektronický	k2eAgInPc4d1
informační	informační	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Knihovna	knihovna	k1gFnSc1
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
budově	budova	k1gFnSc6
Univerzitního	univerzitní	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
na	na	k7c6
náměstí	náměstí	k1gNnSc6
T.	T.	kA
G.	G.	kA
Masaryka	Masaryk	k1gMnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
bylo	být	k5eAaImAgNnS
postaveno	postavit	k5eAaPmNgNnS
v	v	k7c6
letech	let	k1gInPc6
2006	#num#	k4
–	–	k?
2008	#num#	k4
podle	podle	k7c2
návrhu	návrh	k1gInSc2
architektky	architektka	k1gFnSc2
a	a	k8xC
zlínské	zlínský	k2eAgFnSc2d1
rodačky	rodačka	k1gFnSc2
Evy	Eva	k1gFnSc2
Jiřičné	Jiřičný	k2eAgFnSc2d1
a	a	k8xC
společně	společně	k6eAd1
s	s	k7c7
přilehlou	přilehlý	k2eAgFnSc7d1
budovou	budova	k1gFnSc7
Kongresového	kongresový	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
tvoří	tvořit	k5eAaImIp3nS
architektonický	architektonický	k2eAgInSc4d1
komplex	komplex	k1gInSc4
v	v	k7c6
centru	centrum	k1gNnSc6
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uživatelům	uživatel	k1gMnPc3
je	být	k5eAaImIp3nS
v	v	k7c6
knihovně	knihovna	k1gFnSc6
k	k	k7c3
dispozici	dispozice	k1gFnSc3
232	#num#	k4
počítačů	počítač	k1gMnPc2
a	a	k8xC
další	další	k2eAgNnPc4d1
přípojná	přípojný	k2eAgNnPc4d1
místa	místo	k1gNnPc4
pro	pro	k7c4
notebooky	notebook	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Studentské	studentský	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
</s>
<s>
Na	na	k7c6
UTB	UTB	kA
působí	působit	k5eAaImIp3nS
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
studentských	studentský	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
včetně	včetně	k7c2
studentské	studentský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Univerzitní	univerzitní	k2eAgFnSc1d1
internetová	internetový	k2eAgFnSc1d1
televize	televize	k1gFnSc1
Neontv	Neontva	k1gFnPc2
</s>
<s>
Na	na	k7c4
tuto	tento	k3xDgFnSc4
kapitolu	kapitola	k1gFnSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
NeonTV	NeonTV	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1
cílem	cíl	k1gInSc7
televize	televize	k1gFnSc2
Neontv	Neontv	k1gInSc1
je	být	k5eAaImIp3nS
nabídnout	nabídnout	k5eAaPmF
návštěvníkům	návštěvník	k1gMnPc3
stránek	stránka	k1gFnPc2
audiovizuální	audiovizuální	k2eAgFnSc4d1
produkci	produkce	k1gFnSc4
z	z	k7c2
dílny	dílna	k1gFnSc2
zlínských	zlínský	k2eAgMnPc2d1
studentů	student	k1gMnPc2
audiovizuálních	audiovizuální	k2eAgInPc2d1
oborů	obor	k1gInPc2
<g/>
,	,	kIx,
online	onlinout	k5eAaPmIp3nS
přenosy	přenos	k1gInPc4
kulturních	kulturní	k2eAgFnPc2d1
<g/>
,	,	kIx,
společenských	společenský	k2eAgFnPc2d1
a	a	k8xC
sportovních	sportovní	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
a	a	k8xC
další	další	k2eAgFnSc4d1
televizní	televizní	k2eAgFnSc4d1
produkci	produkce	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pobočka	pobočka	k1gFnSc1
studentské	studentský	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
AIESEC	AIESEC	kA
</s>
<s>
AIESEC	AIESEC	kA
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
působí	působit	k5eAaImIp3nS
již	již	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajišťuje	zajišťovat	k5eAaImIp3nS
pro	pro	k7c4
studenty	student	k1gMnPc4
a	a	k8xC
absolventy	absolvent	k1gMnPc4
možnost	možnost	k1gFnSc1
účastnit	účastnit	k5eAaImF
se	se	k3xPyFc4
profesních	profesní	k2eAgFnPc2d1
nebo	nebo	k8xC
kulturních	kulturní	k2eAgFnPc2d1
stáží	stáž	k1gFnPc2
po	po	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
studentům	student	k1gMnPc3
nabízí	nabízet	k5eAaImIp3nP
možnost	možnost	k1gFnSc4
získání	získání	k1gNnSc3
praktických	praktický	k2eAgFnPc2d1
zkušeností	zkušenost	k1gFnPc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
prodeje	prodej	k1gInSc2
<g/>
,	,	kIx,
organizace	organizace	k1gFnSc2
projektů	projekt	k1gInPc2
<g/>
,	,	kIx,
marketingu	marketing	k1gInSc2
nebo	nebo	k8xC
financí	finance	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
IAESTE	IAESTE	kA
LC	LC	kA
Zlín	Zlín	k1gInSc1
</s>
<s>
IAESTE	IAESTE	kA
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
pořádá	pořádat	k5eAaImIp3nS
regionální	regionální	k2eAgInSc4d1
veletrh	veletrh	k1gInSc4
pracovních	pracovní	k2eAgFnPc2d1
příležitostí	příležitost	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Studentská	studentský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
</s>
<s>
Na	na	k7c4
tuto	tento	k3xDgFnSc4
kapitolu	kapitola	k1gFnSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Studentská	studentská	k1gFnSc1
unie	unie	k1gFnSc1
(	(	kIx(
<g/>
Univerzita	univerzita	k1gFnSc1
Tomáše	Tomáš	k1gMnSc2
Bati	Baťa	k1gMnSc2
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Studentská	studentský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
(	(	kIx(
<g/>
SU	SU	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
dobrovolnická	dobrovolnický	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
studentů	student	k1gMnPc2
Univerzity	univerzita	k1gFnSc2
Tomáše	Tomáš	k1gMnSc2
Bati	Baťa	k1gMnSc2
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
vytvářet	vytvářet	k5eAaImF
co	co	k9
nejlepší	dobrý	k2eAgFnPc4d3
podmínky	podmínka	k1gFnPc4
pro	pro	k7c4
studenty	student	k1gMnPc4
i	i	k9
zaměstnance	zaměstnanec	k1gMnPc4
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
SU	SU	k?
vznikla	vzniknout	k5eAaPmAgFnS
15	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studentská	studentská	k1gFnSc1
unie	unie	k1gFnSc2
pořádá	pořádat	k5eAaImIp3nS
nejméně	málo	k6eAd3
16	#num#	k4
projektů	projekt	k1gInPc2
ročně	ročně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
bavit	bavit	k5eAaImF
studenty	student	k1gMnPc4
univerzity	univerzita	k1gFnSc2
<g/>
,	,	kIx,
vzdělávat	vzdělávat	k5eAaImF
je	on	k3xPp3gInPc4
a	a	k8xC
zpříjemňovat	zpříjemňovat	k5eAaImF
jim	on	k3xPp3gMnPc3
studium	studium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Suport	suport	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Portál	portál	k1gInSc1
SU	SU	k?
<g/>
,	,	kIx,
obsahující	obsahující	k2eAgInPc1d1
školní	školní	k2eAgInPc1d1
materiály	materiál	k1gInPc1
<g/>
,	,	kIx,
fóra	fórum	k1gNnPc1
o	o	k7c6
problémech	problém	k1gInPc6
studentů	student	k1gMnPc2
<g/>
,	,	kIx,
informace	informace	k1gFnPc4
o	o	k7c6
unii	unie	k1gFnSc6
a	a	k8xC
v	v	k7c6
neposlední	poslední	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
lifestylový	lifestylový	k2eAgInSc4d1
magazín	magazín	k1gInSc4
a	a	k8xC
informace	informace	k1gFnPc4
o	o	k7c6
aktuálním	aktuální	k2eAgNnSc6d1
dění	dění	k1gNnSc6
na	na	k7c6
škole	škola	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Vítání	vítání	k1gNnSc1
prváků	prvák	k1gInPc2
-	-	kIx~
Ceremonie	ceremonie	k1gFnPc1
pasování	pasování	k1gNnSc2
„	„	k?
<g/>
prváků	prvák	k1gInPc2
<g/>
“	“	k?
mezi	mezi	k7c4
studenty	student	k1gMnPc4
slavnostní	slavnostní	k2eAgInPc1d1
přísahou	přísaha	k1gFnSc7
na	na	k7c4
pravou	pravý	k2eAgFnSc4d1
botu	bota	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1
den	den	k1gInSc1
studentstva	studentstvo	k1gNnSc2
-	-	kIx~
Pietní	pietní	k2eAgNnSc1d1
připomenutí	připomenutí	k1gNnSc1
studentských	studentský	k2eAgInPc2d1
bojů	boj	k1gInPc2
za	za	k7c4
svobodu	svoboda	k1gFnSc4
a	a	k8xC
vlastní	vlastní	k2eAgInSc4d1
projev	projev	k1gInSc4
z	z	k7c2
roku	rok	k1gInSc2
1939	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnPc2
je	být	k5eAaImIp3nS
kulturní	kulturní	k2eAgInSc1d1
program	program	k1gInSc1
(	(	kIx(
<g/>
Běh	běh	k1gInSc1
do	do	k7c2
schodů	schod	k1gInPc2
<g/>
,	,	kIx,
promítání	promítání	k1gNnSc4
filmů	film	k1gInPc2
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Rozsvícení	rozsvícení	k1gNnSc1
vánočního	vánoční	k2eAgInSc2d1
stromečku	stromeček	k1gInSc2
-	-	kIx~
Tradiční	tradiční	k2eAgNnSc1d1
vítání	vítání	k1gNnSc1
adventu	advent	k1gInSc2
<g/>
,	,	kIx,
společná	společný	k2eAgFnSc1d1
oslava	oslava	k1gFnSc1
studentů	student	k1gMnPc2
a	a	k8xC
zaměstnanců	zaměstnanec	k1gMnPc2
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Antiples	Antiples	k1gInSc1
-	-	kIx~
Jakožto	jakožto	k8xS
opak	opak	k1gInSc4
oficiálního	oficiální	k2eAgInSc2d1
univerzitního	univerzitní	k2eAgInSc2d1
plesu	ples	k1gInSc2
v	v	k7c6
róbách	róba	k1gFnPc6
je	být	k5eAaImIp3nS
Antiples	Antiples	k1gMnSc1
studentským	studentský	k2eAgInSc7d1
večírkem	večírek	k1gInSc7
v	v	k7c6
maskách	maska	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnSc7
Antiplesu	Antiples	k1gInSc2
je	být	k5eAaImIp3nS
soutěž	soutěž	k1gFnSc1
o	o	k7c4
nejoriginálnější	originální	k2eAgInSc4d3
převlek	převlek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Pivní	pivní	k2eAgFnSc1d1
spirála	spirála	k1gFnSc1
-	-	kIx~
Dvoučlenné	dvoučlenný	k2eAgInPc1d1
až	až	k8xS
tříčlenné	tříčlenný	k2eAgInPc1d1
týmy	tým	k1gInPc1
studentů	student	k1gMnPc2
mají	mít	k5eAaImIp3nP
za	za	k7c4
úkol	úkol	k1gInSc4
vypít	vypít	k5eAaPmF
vždy	vždy	k6eAd1
2	#num#	k4
velká	velký	k2eAgFnSc1d1
a	a	k8xC
jedno	jeden	k4xCgNnSc1
malé	malý	k2eAgNnSc1d1
pivo	pivo	k1gNnSc1
v	v	k7c6
10	#num#	k4
podnicích	podnik	k1gInPc6
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
a	a	k8xC
Otrokovicích	Otrokovice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zakázány	zakázán	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
jakékoliv	jakýkoliv	k3yIgInPc1
vlastní	vlastní	k2eAgInPc1d1
dopravní	dopravní	k2eAgInPc1d1
prostředky	prostředek	k1gInPc1
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
MHD	MHD	kA
<g/>
.	.	kIx.
</s>
<s>
Galavečer	galavečer	k1gInSc1
-	-	kIx~
Každoroční	každoroční	k2eAgNnSc1d1
ocenění	ocenění	k1gNnSc1
výjimečných	výjimečný	k2eAgMnPc2d1
studentů	student	k1gMnPc2
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnSc7
oceňování	oceňování	k1gNnSc2
je	být	k5eAaImIp3nS
kategorie	kategorie	k1gFnPc4
Bláznivý	bláznivý	k2eAgInSc1d1
počin	počin	k1gInSc1
<g/>
,	,	kIx,
reprezentující	reprezentující	k2eAgInSc1d1
studentský	studentský	k2eAgInSc1d1
život	život	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
SU	SU	k?
v	v	k7c6
parku	park	k1gInSc6
-	-	kIx~
Jednodenní	jednodenní	k2eAgInSc4d1
oddechový	oddechový	k2eAgInSc4d1
den	den	k1gInSc4
ve	v	k7c6
zlínském	zlínský	k2eAgInSc6d1
parku	park	k1gInSc6
na	na	k7c4
závěr	závěr	k1gInSc4
letního	letní	k2eAgInSc2d1
semestru	semestr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
akce	akce	k1gFnSc1
je	být	k5eAaImIp3nS
relaxovat	relaxovat	k5eAaBmF
u	u	k7c2
muziky	muzika	k1gFnSc2
a	a	k8xC
her	hra	k1gFnPc2
(	(	kIx(
<g/>
pétanque	pétanque	k1gInSc1
<g/>
,	,	kIx,
kriket	kriket	k1gInSc1
<g/>
,	,	kIx,
Twister	Twister	k1gInSc1
<g/>
…	…	k?
<g/>
)	)	kIx)
před	před	k7c7
zkouškovým	zkouškový	k2eAgNnSc7d1
obdobím	období	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Námořnická	námořnický	k2eAgFnSc1d1
unie	unie	k1gFnSc1
</s>
<s>
Na	na	k7c4
tuto	tento	k3xDgFnSc4
kapitolu	kapitola	k1gFnSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc4d1
heslo	heslo	k1gNnSc4
Námořnická	námořnický	k2eAgFnSc1d1
unie	unie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Námořnická	námořnický	k2eAgFnSc1d1
Unie	unie	k1gFnSc1
je	být	k5eAaImIp3nS
zájmové	zájmový	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
studentů	student	k1gMnPc2
Univerzity	univerzita	k1gFnSc2
Tomáše	Tomáš	k1gMnSc2
Bati	Baťa	k1gMnSc2
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
<g/>
,	,	kIx,
pedagogů	pedagog	k1gMnPc2
<g/>
,	,	kIx,
absolventů	absolvent	k1gMnPc2
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
také	také	k9
dalších	další	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
nějakým	nějaký	k3yIgInSc7
způsobem	způsob	k1gInSc7
přispívají	přispívat	k5eAaImIp3nP
k	k	k7c3
myšlence	myšlenka	k1gFnSc3
a	a	k8xC
filozofii	filozofie	k1gFnSc4
NU	nu	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filozofie	filozofie	k1gFnSc1
NU	nu	k9
je	být	k5eAaImIp3nS
sdružovat	sdružovat	k5eAaImF
lidi	člověk	k1gMnPc4
z	z	k7c2
nejrůznějších	různý	k2eAgInPc2d3
oborů	obor	k1gInPc2
<g/>
,	,	kIx,
nejrůznějšího	různý	k2eAgInSc2d3
věku	věk	k1gInSc2
<g/>
,	,	kIx,
náboženství	náboženství	k1gNnSc2
a	a	k8xC
nejrůznějších	různý	k2eAgInPc2d3
názorů	názor	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
pomocí	pomocí	k7c2
pořádání	pořádání	k1gNnSc2
kulturních	kulturní	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc1d1
myšlenka	myšlenka	k1gFnSc1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
všichni	všechen	k3xTgMnPc1
lidé	člověk	k1gMnPc1
jsou	být	k5eAaImIp3nP
jedna	jeden	k4xCgFnSc1
velká	velký	k2eAgFnSc1d1
rodina	rodina	k1gFnSc1
<g/>
,	,	kIx,
všichni	všechen	k3xTgMnPc1
plují	plout	k5eAaImIp3nP
na	na	k7c6
stejné	stejný	k2eAgFnSc6d1
lodi	loď	k1gFnSc6
a	a	k8xC
někdy	někdy	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
člověk	člověk	k1gMnSc1
„	„	k?
<g/>
topí	topit	k5eAaImIp3nS
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
jde	jít	k5eAaImIp3nS
ke	k	k7c3
dnu	dno	k1gNnSc3
<g/>
“	“	k?
<g/>
,	,	kIx,
potřebuje	potřebovat	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mu	on	k3xPp3gMnSc3
někdo	někdo	k3yInSc1
„	„	k?
<g/>
hodil	hodit	k5eAaImAgInS,k5eAaPmAgInS
záchranný	záchranný	k2eAgInSc1d1
kruh	kruh	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohl	moct	k5eAaImAgInS
„	„	k?
<g/>
plout	plout	k5eAaImF
dál	daleko	k6eAd2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
NU	nu	k9
se	se	k3xPyFc4
apriorně	apriorně	k6eAd1
nesnaží	snažit	k5eNaImIp3nS
o	o	k7c4
nezřízené	zřízený	k2eNgNnSc4d1
pití	pití	k1gNnSc4
rumu	rum	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
k	k	k7c3
námořníkům	námořník	k1gMnPc3
neodmyslitelně	odmyslitelně	k6eNd1
patří	patřit	k5eAaImIp3nS
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
o	o	k7c4
myšlenkový	myšlenkový	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
členů	člen	k1gMnPc2
i	i	k8xC
přes	přes	k7c4
požití	požití	k1gNnSc4
alkoholických	alkoholický	k2eAgInPc2d1
nápojů	nápoj	k1gInPc2
a	a	k8xC
veřejnému	veřejný	k2eAgInSc3d1
důkazu	důkaz	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
nejsilnější	silný	k2eAgFnSc7d3
zbraní	zbraň	k1gFnSc7
jsou	být	k5eAaImIp3nP
myšlenky	myšlenka	k1gFnPc1
<g/>
,	,	kIx,
kterými	který	k3yIgFnPc7,k3yQgFnPc7,k3yRgFnPc7
se	se	k3xPyFc4
skutečně	skutečně	k6eAd1
dá	dát	k5eAaPmIp3nS
„	„	k?
<g/>
prorazit	prorazit	k5eAaPmF
do	do	k7c2
světa	svět	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
podstatě	podstata	k1gFnSc6
NU	nu	k9
nabádá	nabádat	k5eAaImIp3nS,k5eAaBmIp3nS
ke	k	k7c3
studiu	studio	k1gNnSc3
nejen	nejen	k6eAd1
přírodních	přírodní	k2eAgFnPc2d1
a	a	k8xC
humanitních	humanitní	k2eAgFnPc2d1
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
tak	tak	k9
jak	jak	k8xC,k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
na	na	k7c6
technických	technický	k2eAgFnPc6d1
a	a	k8xC
humanitních	humanitní	k2eAgFnPc6d1
školách	škola	k1gFnPc6
zvykem	zvyk	k1gInSc7
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
ke	k	k7c3
studiu	studio	k1gNnSc3
umění	umění	k1gNnSc2
jak	jak	k6eAd1
divadelnímu	divadelní	k2eAgMnSc3d1
<g/>
,	,	kIx,
tak	tak	k6eAd1
hudebnímu	hudební	k2eAgMnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
kulturně-zábavně-vědecko-vzdělávací	kulturně-zábavně-vědecko-vzdělávací	k2eAgFnPc4d1
akce	akce	k1gFnPc4
patří	patřit	k5eAaImIp3nS
open-air	open-air	k1gInSc1
festival	festival	k1gInSc1
Apráles	Apráles	k1gInSc1
<g/>
,	,	kIx,
Kotva	kotva	k1gFnSc1
<g/>
,	,	kIx,
Kosatka	kosatka	k1gFnSc1
<g/>
,	,	kIx,
Kruh	kruh	k1gInSc1
<g/>
,	,	kIx,
Námořnický	námořnický	k2eAgInSc1d1
bál	bál	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
participace	participace	k1gFnPc4
na	na	k7c6
Noci	noc	k1gFnSc6
vědců	vědec	k1gMnPc2
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
nebo	nebo	k8xC
na	na	k7c6
akademickém	akademický	k2eAgInSc6d1
koštu	košt	k1gInSc6
slivovice	slivovice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Buddy	Buddy	k6eAd1
System	Syst	k1gInSc7
Zlín	Zlín	k1gInSc4
</s>
<s>
Na	na	k7c4
tuto	tento	k3xDgFnSc4
kapitolu	kapitola	k1gFnSc4
jsou	být	k5eAaImIp3nP
přesměrována	přesměrován	k2eAgNnPc1d1
hesla	heslo	k1gNnPc1
Buddy	Budda	k1gFnSc2
System	Syst	k1gInSc7
Zlín	Zlín	k1gInSc1
a	a	k8xC
BSZ	BSZ	kA
<g/>
.	.	kIx.
</s>
<s>
Buddy	Buddy	k6eAd1
System	Syst	k1gInSc7
Zlín	Zlín	k1gInSc1
(	(	kIx(
<g/>
BSZ	BSZ	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
studentská	studentský	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
mezinárodním	mezinárodní	k2eAgNnSc7d1
oddělením	oddělení	k1gNnSc7
UTB	UTB	kA
pracuje	pracovat	k5eAaImIp3nS
s	s	k7c7
přijíždějícími	přijíždějící	k2eAgMnPc7d1
zahraničními	zahraniční	k2eAgMnPc7d1
studenty	student	k1gMnPc7
a	a	k8xC
dává	dávat	k5eAaImIp3nS
si	se	k3xPyFc3
za	za	k7c4
úkol	úkol	k1gInSc4
usnadnit	usnadnit	k5eAaPmF
jim	on	k3xPp3gMnPc3
příjezd	příjezd	k1gInSc4
do	do	k7c2
města	město	k1gNnSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc1
poznání	poznání	k1gNnSc1
<g/>
,	,	kIx,
nástup	nástup	k1gInSc1
na	na	k7c4
univerzitu	univerzita	k1gFnSc4
<g/>
;	;	kIx,
dále	daleko	k6eAd2
pro	pro	k7c4
studenty	student	k1gMnPc4
organizuje	organizovat	k5eAaBmIp3nS
poznávací	poznávací	k2eAgInPc4d1
výlety	výlet	k1gInPc4
a	a	k8xC
volnočasové	volnočasový	k2eAgFnPc4d1
aktivity	aktivita	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
BSZ	BSZ	kA
je	být	k5eAaImIp3nS
členem	člen	k1gInSc7
mezinárodní	mezinárodní	k2eAgFnSc2d1
studentské	studentská	k1gFnSc2
sitě	sitě	k1gMnSc4
Erasmus	Erasmus	k1gMnSc1
Student	student	k1gMnSc1
Network	network	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Univerzitní	univerzitní	k2eAgFnSc1d1
mateřská	mateřský	k2eAgFnSc1d1
školka	školka	k1gFnSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
disponuje	disponovat	k5eAaBmIp3nS
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2011	#num#	k4
univerzitní	univerzitní	k2eAgFnSc6d1
školkou	školka	k1gFnSc7
pro	pro	k7c4
studenty	student	k1gMnPc4
a	a	k8xC
zaměstnance	zaměstnanec	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sport	sport	k1gInSc1
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
</s>
<s>
Součástí	součást	k1gFnSc7
univerzity	univerzita	k1gFnSc2
je	být	k5eAaImIp3nS
sport	sport	k1gInSc1
a	a	k8xC
propagace	propagace	k1gFnSc1
moderního	moderní	k2eAgInSc2d1
životního	životní	k2eAgInSc2d1
stylu	styl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústav	ústav	k1gInSc1
tělesné	tělesný	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
a	a	k8xC
sportu	sport	k1gInSc2
zabezpečuje	zabezpečovat	k5eAaImIp3nS
sportovní	sportovní	k2eAgFnPc4d1
aktivity	aktivita	k1gFnPc4
na	na	k7c6
všech	všecek	k3xTgFnPc6
fakultách	fakulta	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Sportovní	sportovní	k2eAgFnPc1d1
aktivity	aktivita	k1gFnPc1
</s>
<s>
Golf	golf	k1gInSc1
<g/>
,	,	kIx,
K2	K2	k1gFnSc1
hiking	hiking	k1gInSc1
<g/>
,	,	kIx,
taekwon-do	taekwon-do	k1gNnSc1
<g/>
,	,	kIx,
volejbal	volejbal	k1gInSc1
<g/>
,	,	kIx,
indoor	indoor	k1gInSc1
cycling	cycling	k1gInSc1
<g/>
,	,	kIx,
lyžování	lyžování	k1gNnSc1
<g/>
,	,	kIx,
snowboarding	snowboarding	k1gInSc1
<g/>
,	,	kIx,
posilovna	posilovna	k1gFnSc1
<g/>
,	,	kIx,
horolezectví	horolezectví	k1gNnSc1
<g/>
,	,	kIx,
squash	squash	k1gInSc1
<g/>
,	,	kIx,
plavání	plavání	k1gNnSc1
<g/>
,	,	kIx,
aerobik	aerobik	k1gInSc1
<g/>
,	,	kIx,
tanec	tanec	k1gInSc1
<g/>
,	,	kIx,
posilování	posilování	k1gNnSc1
<g/>
,	,	kIx,
softball	softball	k1gInSc1
<g/>
,	,	kIx,
sálová	sálový	k2eAgFnSc1d1
kopaná	kopaná	k1gFnSc1
<g/>
,	,	kIx,
tenis	tenis	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Každoroční	každoroční	k2eAgFnSc1d1
akce	akce	k1gFnSc1
</s>
<s>
Zimní	zimní	k2eAgInSc1d1
výcvikový	výcvikový	k2eAgInSc1d1
kurz	kurz	k1gInSc1
-	-	kIx~
možnost	možnost	k1gFnSc1
zúčastnit	zúčastnit	k5eAaPmF
se	se	k3xPyFc4
lyžařského	lyžařský	k2eAgInSc2d1
kurzu	kurz	k1gInSc2
</s>
<s>
Den	den	k1gInSc1
sportu	sport	k1gInSc2
-	-	kIx~
Rektorský	rektorský	k2eAgInSc1d1
den	den	k1gInSc1
sportu	sport	k1gInSc2
pořádá	pořádat	k5eAaImIp3nS
Ústav	ústav	k1gInSc1
telesné	telesný	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
UTB	UTB	kA
jednou	jednou	k6eAd1
ročně	ročně	k6eAd1
v	v	k7c6
letním	letní	k2eAgInSc6d1
semestru	semestr	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
výšlapu	výšlap	k1gInSc2
okolím	okolí	k1gNnSc7
Zlína	Zlín	k1gInSc2
se	se	k3xPyFc4
studenti	student	k1gMnPc1
mohou	moct	k5eAaImIp3nP
zapojit	zapojit	k5eAaPmF
do	do	k7c2
mnoha	mnoho	k4c2
sportovních	sportovní	k2eAgInPc2d1
turnajů	turnaj	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
golfový	golfový	k2eAgMnSc1d1
<g/>
,	,	kIx,
fotbalový	fotbalový	k2eAgInSc1d1
či	či	k8xC
squashový	squashový	k2eAgInSc1d1
turnaj	turnaj	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Letní	letní	k2eAgInSc1d1
výcvikový	výcvikový	k2eAgInSc1d1
kurz	kurz	k1gInSc1
-	-	kIx~
V	v	k7c6
areálu	areál	k1gInSc6
Luhačovic	Luhačovice	k1gFnPc2
je	být	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
využít	využít	k5eAaPmF
kurzů	kurz	k1gInPc2
cykloturistiky	cykloturistika	k1gFnSc2
<g/>
,	,	kIx,
turistiky	turistika	k1gFnSc2
<g/>
,	,	kIx,
skálolezení	skálolezení	k1gNnSc2
<g/>
,	,	kIx,
míčových	míčový	k2eAgFnPc2d1
her	hra	k1gFnPc2
<g/>
,	,	kIx,
softballu	softball	k1gInSc2
<g/>
,	,	kIx,
kanoistice	kanoistika	k1gFnSc6
<g/>
,	,	kIx,
windsurfingu	windsurfing	k1gInSc6
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Udělené	udělený	k2eAgInPc1d1
čestné	čestný	k2eAgInPc1d1
doktoráty	doktorát	k1gInPc1
</s>
<s>
Tituly	titul	k1gInPc4
doctor	doctor	k1gMnSc1
honoris	honoris	k1gFnSc2
causa	causa	k1gFnSc1
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
získali	získat	k5eAaPmAgMnP
<g/>
:	:	kIx,
</s>
<s>
José	José	k6eAd1
Manuel	Manuel	k1gMnSc1
Durã	Durã	k1gMnSc1
Barroso	Barrosa	k1gFnSc5
(	(	kIx(
<g/>
uděleno	udělit	k5eAaPmNgNnS
dne	den	k1gInSc2
22	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2009	#num#	k4
za	za	k7c2
jeho	jeho	k3xOp3gFnSc2
osobní	osobní	k2eAgFnSc2d1
zásluhy	zásluha	k1gFnSc2
o	o	k7c4
připojení	připojení	k1gNnSc4
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
k	k	k7c3
Evropské	evropský	k2eAgFnSc3d1
unii	unie	k1gFnSc3
a	a	k8xC
o	o	k7c6
budování	budování	k1gNnSc6
nové	nový	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Eva	Eva	k1gFnSc1
Jiřičná	Jiřičný	k2eAgFnSc1d1
(	(	kIx(
<g/>
uděleno	udělen	k2eAgNnSc1d1
dne	den	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2011	#num#	k4
za	za	k7c4
excelentní	excelentní	k2eAgFnSc4d1
prezentaci	prezentace	k1gFnSc4
univerzity	univerzita	k1gFnSc2
a	a	k8xC
šíření	šíření	k1gNnSc2
dobrého	dobrý	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
města	město	k1gNnSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sonja	Sonj	k2eAgFnSc1d1
Bata	Bata	k1gFnSc1
(	(	kIx(
<g/>
uděleno	udělit	k5eAaPmNgNnS
dne	den	k1gInSc2
26	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2012	#num#	k4
za	za	k7c4
dlouholetou	dlouholetý	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
UTB	UTB	kA
-	-	kIx~
od	od	k7c2
jejího	její	k3xOp3gNnSc2
založení	založení	k1gNnSc2
až	až	k9
do	do	k7c2
současnosti	současnost	k1gFnSc2
<g/>
,	,	kIx,
za	za	k7c4
její	její	k3xOp3gFnSc4
propagaci	propagace	k1gFnSc4
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
a	a	k8xC
za	za	k7c4
osobní	osobní	k2eAgInSc4d1
přínos	přínos	k1gInSc4
k	k	k7c3
rozvoji	rozvoj	k1gInSc3
mezinárodních	mezinárodní	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
UTB	UTB	kA
<g/>
)	)	kIx)
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Zikmund	Zikmund	k1gMnSc1
(	(	kIx(
<g/>
uděleno	udělit	k5eAaPmNgNnS
dne	den	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2014	#num#	k4
za	za	k7c4
obdivuhodné	obdivuhodný	k2eAgNnSc4d1
celoživotní	celoživotní	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
<g/>
,	,	kIx,
kterým	který	k3yRgNnSc7,k3yIgNnSc7,k3yQgNnSc7
společně	společně	k6eAd1
se	se	k3xPyFc4
svým	svůj	k3xOyFgMnSc7
dlouholetým	dlouholetý	k2eAgMnSc7d1
kolegou	kolega	k1gMnSc7
a	a	k8xC
přítelem	přítel	k1gMnSc7
Jiřím	Jiří	k1gMnSc7
Hanzelkou	Hanzelka	k1gMnSc7
výrazně	výrazně	k6eAd1
obohatili	obohatit	k5eAaPmAgMnP
a	a	k8xC
rozšířili	rozšířit	k5eAaPmAgMnP
mnoho	mnoho	k4c4
oblastí	oblast	k1gFnPc2
lidského	lidský	k2eAgNnSc2d1
vědění	vědění	k1gNnSc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Přehled	přehled	k1gInSc1
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
v	v	k7c6
ČR	ČR	kA
-	-	kIx~
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veřejné	veřejný	k2eAgFnPc1d1
vysoké	vysoký	k2eAgFnPc1d1
školy	škola	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerstvo	ministerstvo	k1gNnSc1
školství	školství	k1gNnSc2
<g/>
,	,	kIx,
mládeže	mládež	k1gFnSc2
a	a	k8xC
tělovýchovy	tělovýchova	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
-	-	kIx~
MŠMT	MŠMT	kA
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
o	o	k7c6
činnosti	činnost	k1gFnSc6
za	za	k7c4
rok	rok	k1gInSc4
2016	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zlín	Zlín	k1gInSc1
<g/>
:	:	kIx,
Univerzita	univerzita	k1gFnSc1
Tomáše	Tomáš	k1gMnSc2
Bati	Baťa	k1gMnSc2
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
41	#num#	k4
<g/>
,	,	kIx,
43	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
Zákon	zákon	k1gInSc1
č.	č.	k?
404	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
zřízení	zřízení	k1gNnSc6
Univerzity	univerzita	k1gFnSc2
Tomáše	Tomáš	k1gMnSc2
Bati	Baťa	k1gMnSc2
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
z	z	k7c2
19	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2000	#num#	k4
<g/>
↑	↑	k?
Pozitivní	pozitivní	k2eAgFnSc2d1
zprávy	zpráva	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-06-20	2018-06-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
QS	QS	kA
University	universita	k1gFnSc2
Rankings	Rankings	k1gInSc1
<g/>
:	:	kIx,
EECA	EECA	kA
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Top	topit	k5eAaImRp2nS
Universities	Universities	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-10-25	2018-10-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Reprezentační	reprezentační	k2eAgFnSc1d1
brožura	brožura	k1gFnSc1
UTB	UTB	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
Tomáše	Tomáš	k1gMnSc2
Bati	Baťa	k1gMnSc2
<g/>
,	,	kIx,
2017-04-12	2017-04-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
</s>
<s>
Vznik	vznik	k1gInSc1
UTB	UTB	kA
v	v	k7c6
datech	datum	k1gNnPc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
Tomáše	Tomáš	k1gMnSc2
Bati	Baťa	k1gMnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
</s>
<s>
Seznam	seznam	k1gInSc1
kolejních	kolejní	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
Tomáše	Tomáš	k1gMnSc2
Bati	Baťa	k1gMnSc2
<g/>
,	,	kIx,
2011-02-10	2011-02-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Stravování	stravování	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
Tomáše	Tomáš	k1gMnSc2
Bati	Baťa	k1gMnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
</s>
<s>
O	o	k7c4
nás	my	k3xPp1nPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neontv	Neontv	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
</s>
<s>
Aktuality	aktualita	k1gFnPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
AIESEC	AIESEC	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
LC	LC	kA
IAESTE	IAESTE	kA
při	při	k7c6
UTB	UTB	kA
Zlín	Zlín	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
IAESTE	IAESTE	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
</s>
<s>
Akce	akce	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Support	support	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Webové	webový	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
UMŠ	UMŠ	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
Tomáše	Tomáš	k1gMnSc2
Bati	Baťa	k1gMnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
NOVOTNÁ	Novotná	k1gFnSc1
<g/>
,	,	kIx,
Anna	Anna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předseda	předseda	k1gMnSc1
Evropské	evropský	k2eAgFnSc2d1
komise	komise	k1gFnSc2
dostal	dostat	k5eAaPmAgMnS
čestný	čestný	k2eAgInSc4d1
doktorát	doktorát	k1gInSc4
na	na	k7c6
UTB	UTB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-04-23	2009-04-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
FUKSOVÁ	Fuksová	k1gFnSc1
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překvapená	překvapený	k2eAgFnSc1d1
architektka	architektka	k1gFnSc1
Jiřičná	Jiřičný	k2eAgFnSc1d1
dostala	dostat	k5eAaPmAgFnS
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
čestný	čestný	k2eAgInSc4d1
doktorát	doktorát	k1gInSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-05-18	2011-05-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Hra	hra	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
provede	provést	k5eAaPmIp3nS
Univerzitou	univerzita	k1gFnSc7
Tomáše	Tomáš	k1gMnSc2
Bati	Baťa	k1gMnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Univerzita	univerzita	k1gFnSc1
Tomáše	Tomáš	k1gMnSc2
Bati	Baťa	k1gMnSc2
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
Fakulty	fakulta	k1gFnSc2
</s>
<s>
Fakulta	fakulta	k1gFnSc1
technologická	technologický	k2eAgFnSc1d1
•	•	k?
Fakulta	fakulta	k1gFnSc1
managementu	management	k1gInSc2
a	a	k8xC
ekonomiky	ekonomika	k1gFnSc2
•	•	k?
Fakulta	fakulta	k1gFnSc1
multimediálních	multimediální	k2eAgInPc2d1
komunikací	komunikace	k1gFnSc7
•	•	k?
Fakulta	fakulta	k1gFnSc1
aplikované	aplikovaný	k2eAgFnPc1d1
informatiky	informatika	k1gFnPc1
•	•	k?
Fakulta	fakulta	k1gFnSc1
humanitních	humanitní	k2eAgInPc2d1
studií	studie	k1gFnSc7
•	•	k?
Fakulta	fakulta	k1gFnSc1
logistiky	logistika	k1gFnSc2
a	a	k8xC
krizového	krizový	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
Další	další	k2eAgFnPc1d1
součásti	součást	k1gFnPc1
univerzity	univerzita	k1gFnSc2
</s>
<s>
Centrum	centrum	k1gNnSc1
polymerních	polymerní	k2eAgInPc2d1
systémů	systém	k1gInPc2
</s>
<s>
Veřejné	veřejný	k2eAgFnPc1d1
vysoké	vysoký	k2eAgFnPc1d1
školy	škola	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
Praha	Praha	k1gFnSc1
</s>
<s>
Akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Akademie	akademie	k1gFnSc2
výtvarných	výtvarný	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Česká	český	k2eAgFnSc1d1
zemědělská	zemědělský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
České	český	k2eAgFnSc6d1
vysoké	vysoká	k1gFnSc6
učení	učení	k1gNnSc4
technické	technický	k2eAgNnSc4d1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgFnSc1d1
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
ekonomická	ekonomický	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
chemicko-technologická	chemicko-technologický	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
uměleckoprůmyslová	uměleckoprůmyslový	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
</s>
<s>
Jihočeská	jihočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
technická	technický	k2eAgFnSc1d1
a	a	k8xC
ekonomická	ekonomický	k2eAgFnSc1d1
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
Plzeň	Plzeň	k1gFnSc1
</s>
<s>
Západočeská	západočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
Ústí	ústí	k1gNnSc2
nad	nad	k7c7
Labem	Labe	k1gNnSc7
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Jana	Jan	k1gMnSc2
Evangelisty	evangelista	k1gMnSc2
Purkyně	Purkyně	k1gFnSc1
v	v	k7c6
Ústí	ústí	k1gNnSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
Liberec	Liberec	k1gInSc1
</s>
<s>
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
Pardubice	Pardubice	k1gInPc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Pardubice	Pardubice	k1gInPc1
Jihlava	Jihlava	k1gFnSc1
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
polytechnická	polytechnický	k2eAgFnSc1d1
Jihlava	Jihlava	k1gFnSc1
Brno	Brno	k1gNnSc1
</s>
<s>
Janáčkova	Janáčkův	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
•	•	k?
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
•	•	k?
Mendelova	Mendelův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
•	•	k?
Veterinární	veterinární	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Brno	Brno	k1gNnSc1
•	•	k?
Vysoké	vysoký	k2eAgNnSc1d1
učení	učení	k1gNnSc4
technické	technický	k2eAgNnSc4d1
v	v	k7c6
Brně	Brno	k1gNnSc6
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Palackého	Palacký	k1gMnSc2
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Ostravská	ostravský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
báňská	báňský	k2eAgFnSc1d1
–	–	k?
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Ostrava	Ostrava	k1gFnSc1
Opava	Opava	k1gFnSc1
</s>
<s>
Slezská	slezský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Opavě	Opava	k1gFnSc6
Zlín	Zlín	k1gInSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Tomáše	Tomáš	k1gMnSc2
Bati	Baťa	k1gMnSc2
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
11099	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
10190930-5	10190930-5	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1504	#num#	k4
2033	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2002074387	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
134724654	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2002074387	#num#	k4
</s>
