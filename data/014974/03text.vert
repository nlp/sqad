<s>
Kermit	Kermit	k1gInSc1
Driscoll	Driscolla	k1gFnPc2
</s>
<s>
Kermit	Kermit	k1gInSc1
DriscollZákladní	DriscollZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Narození	narozený	k2eAgMnPc5d1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1956	#num#	k4
(	(	kIx(
<g/>
65	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Kearney	Kearnea	k1gFnPc1
Žánry	žánr	k1gInPc1
</s>
<s>
jazz	jazz	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
hudebník	hudebník	k1gMnSc1
Nástroje	nástroj	k1gInSc2
</s>
<s>
kontrabas	kontrabas	k1gInSc1
<g/>
,	,	kIx,
baskytara	baskytara	k1gFnSc1
Web	web	k1gInSc1
</s>
<s>
http://www.kermitdriscoll.com/	http://www.kermitdriscoll.com/	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kermit	Kermit	k1gInSc1
Driscoll	Driscoll	k1gInSc1
(	(	kIx(
<g/>
*	*	kIx~
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1956	#num#	k4
Kearney	Kearnea	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
americký	americký	k2eAgMnSc1d1
kontrabasista	kontrabasista	k1gMnSc1
a	a	k8xC
baskytarista	baskytarista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svých	svůj	k3xOyFgInPc6
pěti	pět	k4xCc6
letech	let	k1gInPc6
začal	začít	k5eAaPmAgMnS
hrát	hrát	k5eAaImF
na	na	k7c4
klavír	klavír	k1gInSc4
<g/>
,	,	kIx,
později	pozdě	k6eAd2
na	na	k7c4
saxofon	saxofon	k1gInSc4
a	a	k8xC
ve	v	k7c6
třinácti	třináct	k4xCc6
na	na	k7c4
baskytaru	baskytara	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
rockové	rockový	k2eAgFnSc2d1
kapely	kapela	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1975	#num#	k4
až	až	k9
1978	#num#	k4
studoval	studovat	k5eAaImAgInS
na	na	k7c6
Berklee	Berklee	k1gNnSc6
College	Colleg	k1gInSc2
of	of	k?
Music	Music	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
začal	začít	k5eAaPmAgMnS
spolupracovat	spolupracovat	k5eAaImF
s	s	k7c7
kytaristou	kytarista	k1gMnSc7
Billem	Bill	k1gMnSc7
Frisellem	Frisell	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členem	člen	k1gMnSc7
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
kvartetu	kvartet	k1gInSc2
byl	být	k5eAaImAgInS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
se	s	k7c7
členy	člen	k1gMnPc7
souboru	soubor	k1gInSc2
Soldier	Soldier	k1gInSc1
String	String	k1gInSc1
Quartet	Quartet	k1gInSc4
nahrál	nahrát	k5eAaPmAgInS,k5eAaBmAgInS
originální	originální	k2eAgFnPc4d1
hudbu	hudba	k1gFnSc4
skladatele	skladatel	k1gMnSc2
Johna	John	k1gMnSc2
Calea	Caleus	k1gMnSc2
pro	pro	k7c4
film	film	k1gInSc4
Střelila	střelit	k5eAaPmAgFnS
jsem	být	k5eAaImIp1nS
Andyho	Andy	k1gMnSc4
Warhola	Warhola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
své	svůj	k3xOyFgFnSc2
kariéry	kariéra	k1gFnSc2
spolupracoval	spolupracovat	k5eAaImAgMnS
s	s	k7c7
mnoha	mnoho	k4c7
dalšími	další	k2eAgMnPc7d1
hudebníky	hudebník	k1gMnPc7
<g/>
,	,	kIx,
mezi	mezi	k7c4
něž	jenž	k3xRgFnPc4
patří	patřit	k5eAaImIp3nS
například	například	k6eAd1
Wayne	Wayn	k1gInSc5
Horvitz	Horvitz	k1gMnSc1
<g/>
,	,	kIx,
Henry	Henry	k1gMnSc1
Kaiser	Kaiser	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
Zorn	Zorn	k1gMnSc1
a	a	k8xC
Emil	Emil	k1gMnSc1
Viklický	Viklický	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
vydal	vydat	k5eAaPmAgInS
vlastní	vlastní	k2eAgNnSc4d1
album	album	k1gNnSc4
Reveille	Reveille	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
podíleli	podílet	k5eAaImAgMnP
kytarista	kytarista	k1gMnSc1
Bill	Bill	k1gMnSc1
Frisell	Frisell	k1gMnSc1
<g/>
,	,	kIx,
klavírista	klavírista	k1gMnSc1
Kris	kris	k1gInSc4
Davis	Davis	k1gFnSc2
a	a	k8xC
bubeník	bubeník	k1gMnSc1
Vinnie	Vinnie	k1gFnSc1
Colaiuta	Colaiut	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
51769	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
134806875	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1862	#num#	k4
7280	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
91115319	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
100322041	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
91115319	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
