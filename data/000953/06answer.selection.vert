<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Ioannes	Ioannes	k1gMnSc1	Ioannes
Paulus	Paulus	k1gMnSc1	Paulus
<g/>
,	,	kIx,	,
italsky	italsky	k6eAd1	italsky
Giovanni	Giovann	k1gMnPc1	Giovann
Paolo	Paolo	k1gNnSc1	Paolo
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Karol	Karol	k1gInSc1	Karol
Józef	Józef	k1gInSc1	Józef
Wojtyła	Wojtyła	k1gFnSc1	Wojtyła
[	[	kIx(	[
<g/>
karol	karol	k1gInSc1	karol
juzef	juzef	k1gMnSc1	juzef
vojtyua	vojtyua	k1gMnSc1	vojtyua
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
1920	[number]	k4	1920
Wadowice	Wadowice	k1gFnSc1	Wadowice
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
duben	duben	k1gInSc4	duben
2005	[number]	k4	2005
Vatikán	Vatikán	k1gInSc1	Vatikán
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
polský	polský	k2eAgInSc1d1	polský
<g />
.	.	kIx.	.
katolický	katolický	k2eAgInSc1d1	katolický
duchovní	duchovní	k2eAgInSc1d1	duchovní
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stal	stát	k5eAaPmAgInS	stát
pomocným	pomocný	k2eAgMnSc7d1	pomocný
biskupem	biskup	k1gMnSc7	biskup
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
-	-	kIx~	-
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
krakovským	krakovský	k2eAgInSc7d1	krakovský
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
-	-	kIx~	-
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kardinálem	kardinál	k1gMnSc7	kardinál
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1978	[number]	k4	1978
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
prvním	první	k4xOgMnSc6	první
slovanským	slovanský	k2eAgInSc7d1	slovanský
a	a	k8xC	a
po	po	k7c6	po
455	[number]	k4	455
letech	léto	k1gNnPc6	léto
prvním	první	k4xOgMnSc6	první
neitalským	italský	k2eNgMnSc7d1	neitalský
papežem	papež	k1gMnSc7	papež
<g/>
.	.	kIx.	.
</s>
