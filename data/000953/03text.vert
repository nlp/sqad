<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Ioannes	Ioannes	k1gMnSc1	Ioannes
Paulus	Paulus	k1gMnSc1	Paulus
<g/>
,	,	kIx,	,
italsky	italsky	k6eAd1	italsky
Giovanni	Giovann	k1gMnPc1	Giovann
Paolo	Paolo	k1gNnSc1	Paolo
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Karol	Karol	k1gInSc1	Karol
Józef	Józef	k1gInSc1	Józef
Wojtyła	Wojtyła	k1gFnSc1	Wojtyła
[	[	kIx(	[
<g/>
karol	karol	k1gInSc1	karol
juzef	juzef	k1gMnSc1	juzef
vojtyua	vojtyua	k1gMnSc1	vojtyua
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
1920	[number]	k4	1920
Wadowice	Wadowice	k1gFnSc1	Wadowice
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
duben	duben	k1gInSc4	duben
2005	[number]	k4	2005
Vatikán	Vatikán	k1gInSc1	Vatikán
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
polský	polský	k2eAgInSc1d1	polský
<g />
.	.	kIx.	.
katolický	katolický	k2eAgInSc1d1	katolický
duchovní	duchovní	k2eAgInSc1d1	duchovní
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stal	stát	k5eAaPmAgInS	stát
pomocným	pomocný	k2eAgMnSc7d1	pomocný
biskupem	biskup	k1gMnSc7	biskup
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
-	-	kIx~	-
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
krakovským	krakovský	k2eAgInSc7d1	krakovský
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
-	-	kIx~	-
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kardinálem	kardinál	k1gMnSc7	kardinál
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1978	[number]	k4	1978
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
prvním	první	k4xOgMnSc6	první
slovanským	slovanský	k2eAgInSc7d1	slovanský
a	a	k8xC	a
po	po	k7c6	po
455	[number]	k4	455
letech	léto	k1gNnPc6	léto
prvním	první	k4xOgMnSc6	první
neitalským	italský	k2eNgMnSc7d1	neitalský
papežem	papež	k1gMnSc7	papež
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
svatý	svatý	k2eAgMnSc1d1	svatý
otec	otec	k1gMnSc1	otec
hrál	hrát	k5eAaImAgMnS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
ve	v	k7c6	v
světové	světový	k2eAgFnSc6d1	světová
politice	politika	k1gFnSc6	politika
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
přisuzován	přisuzovat	k5eAaImNgInS	přisuzovat
podíl	podíl	k1gInSc1	podíl
na	na	k7c4	na
zhroucení	zhroucení	k1gNnSc4	zhroucení
komunistických	komunistický	k2eAgInPc2d1	komunistický
režimů	režim	k1gInPc2	režim
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
redefinoval	redefinovat	k5eAaImAgInS	redefinovat
vztah	vztah	k1gInSc1	vztah
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
k	k	k7c3	k
judaismu	judaismus	k1gInSc3	judaismus
a	a	k8xC	a
nabádal	nabádat	k5eAaBmAgInS	nabádat
protestantské	protestantský	k2eAgMnPc4d1	protestantský
a	a	k8xC	a
ortodoxní	ortodoxní	k2eAgMnPc4d1	ortodoxní
křesťany	křesťan	k1gMnPc4	křesťan
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pomohli	pomoct	k5eAaPmAgMnP	pomoct
přetvářet	přetvářet	k5eAaImF	přetvářet
papežství	papežství	k1gNnSc4	papežství
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
sloužilo	sloužit	k5eAaImAgNnS	sloužit
potřebám	potřeba	k1gFnPc3	potřeba
všech	všecek	k3xTgMnPc2	všecek
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
26	[number]	k4	26
let	léto	k1gNnPc2	léto
trvajícího	trvající	k2eAgInSc2d1	trvající
pontifikátu	pontifikát	k1gInSc2	pontifikát
svatořečil	svatořečit	k5eAaBmAgInS	svatořečit
(	(	kIx(	(
<g/>
482	[number]	k4	482
osob	osoba	k1gFnPc2	osoba
<g/>
)	)	kIx)	)
a	a	k8xC	a
blahořečil	blahořečit	k5eAaImAgMnS	blahořečit
(	(	kIx(	(
<g/>
1338	[number]	k4	1338
osob	osoba	k1gFnPc2	osoba
<g/>
)	)	kIx)	)
více	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
než	než	k8xS	než
všichni	všechen	k3xTgMnPc1	všechen
jeho	jeho	k3xOp3gMnPc1	jeho
předchůdci	předchůdce	k1gMnPc1	předchůdce
dohromady	dohromady	k6eAd1	dohromady
a	a	k8xC	a
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
104	[number]	k4	104
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
papežských	papežský	k2eAgFnPc2d1	Papežská
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
více	hodně	k6eAd2	hodně
než	než	k8xS	než
kterýkoliv	kterýkoliv	k3yIgMnSc1	kterýkoliv
jiný	jiný	k2eAgMnSc1d1	jiný
svatý	svatý	k2eAgMnSc1d1	svatý
otec	otec	k1gMnSc1	otec
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
hlavy	hlava	k1gFnSc2	hlava
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
výrazně	výrazně	k6eAd1	výrazně
podporoval	podporovat	k5eAaImAgMnS	podporovat
mariánskou	mariánský	k2eAgFnSc4d1	Mariánská
úctu	úcta	k1gFnSc4	úcta
a	a	k8xC	a
zdůrazňoval	zdůrazňovat	k5eAaImAgMnS	zdůrazňovat
povolání	povolání	k1gNnSc4	povolání
ke	k	k7c3	k
svatosti	svatost	k1gFnSc3	svatost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
sexuální	sexuální	k2eAgFnSc2d1	sexuální
morálky	morálka	k1gFnSc2	morálka
a	a	k8xC	a
ochrany	ochrana	k1gFnSc2	ochrana
lidského	lidský	k2eAgInSc2d1	lidský
života	život	k1gInSc2	život
navázal	navázat	k5eAaPmAgInS	navázat
na	na	k7c4	na
učení	učení	k1gNnSc4	učení
svých	svůj	k3xOyFgMnPc2	svůj
předchůdců	předchůdce	k1gMnPc2	předchůdce
a	a	k8xC	a
zejména	zejména	k9	zejména
v	v	k7c6	v
katechezích	katecheze	k1gFnPc6	katecheze
publikovaných	publikovaný	k2eAgFnPc2d1	publikovaná
v	v	k7c6	v
souboru	soubor	k1gInSc6	soubor
Teologie	teologie	k1gFnSc2	teologie
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
v	v	k7c6	v
encyklice	encyklika	k1gFnSc6	encyklika
Evangelium	evangelium	k1gNnSc1	evangelium
vitae	vitae	k6eAd1	vitae
jednoznačně	jednoznačně	k6eAd1	jednoznačně
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
tradiční	tradiční	k2eAgNnSc4d1	tradiční
církevní	církevní	k2eAgNnSc4d1	církevní
stanovisko	stanovisko	k1gNnSc4	stanovisko
morální	morální	k2eAgFnSc2d1	morální
nepřípustnosti	nepřípustnost	k1gFnSc2	nepřípustnost
interrupce	interrupce	k1gFnSc2	interrupce
<g/>
,	,	kIx,	,
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
a	a	k8xC	a
eutanazie	eutanazie	k1gFnSc2	eutanazie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
mnoha	mnoho	k4c7	mnoho
jím	on	k3xPp3gMnSc7	on
svatořečenými	svatořečený	k2eAgFnPc7d1	svatořečená
a	a	k8xC	a
blahořečenými	blahořečený	k2eAgFnPc7d1	blahořečená
osobami	osoba	k1gFnPc7	osoba
bylo	být	k5eAaImAgNnS	být
i	i	k8xC	i
několik	několik	k4yIc1	několik
osobností	osobnost	k1gFnPc2	osobnost
z	z	k7c2	z
české	český	k2eAgFnSc2d1	Česká
historie	historie	k1gFnSc2	historie
<g/>
:	:	kIx,	:
sv.	sv.	kA	sv.
Anežka	Anežka	k1gFnSc1	Anežka
Česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Zdislava	Zdislava	k1gFnSc1	Zdislava
z	z	k7c2	z
Lemberka	Lemberka	k1gFnSc1	Lemberka
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Jan	Jan	k1gMnSc1	Jan
Sarkander	Sarkander	k1gMnSc1	Sarkander
<g/>
,	,	kIx,	,
bl.	bl.	k?	bl.
Marie	Maria	k1gFnPc1	Maria
Antonína	Antonín	k1gMnSc2	Antonín
Kratochvílová	Kratochvílová	k1gFnSc1	Kratochvílová
<g/>
,	,	kIx,	,
bl.	bl.	k?	bl.
Marie	Maria	k1gFnSc2	Maria
Restituta	Restitut	k2eAgFnSc1d1	Restituta
Kafková	Kafková	k1gFnSc1	Kafková
a	a	k8xC	a
bl.	bl.	k?	bl.
Metoděj	Metoděj	k1gMnSc1	Metoděj
Dominik	Dominik	k1gMnSc1	Dominik
Trčka	Trčka	k1gMnSc1	Trčka
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgMnS	být
papežem	papež	k1gMnSc7	papež
Františkem	František	k1gMnSc7	František
kanonizován	kanonizován	k2eAgMnSc1d1	kanonizován
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Karol	Karol	k1gInSc1	Karol
Józef	Józef	k1gMnSc1	Józef
Wojtyła	Wojtyła	k1gMnSc1	Wojtyła
mladší	mladý	k2eAgMnSc1d2	mladší
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1920	[number]	k4	1920
ve	v	k7c6	v
Wadowicích	Wadowik	k1gInPc6	Wadowik
jako	jako	k9	jako
třetí	třetí	k4xOgMnSc1	třetí
potomek	potomek	k1gMnSc1	potomek
armádního	armádní	k2eAgMnSc2d1	armádní
důstojníka	důstojník	k1gMnSc2	důstojník
Karola	Karola	k1gFnSc1	Karola
Wojtyły	Wojtyła	k1gMnSc2	Wojtyła
staršího	starší	k1gMnSc2	starší
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
-	-	kIx~	-
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
ženy	žena	k1gFnPc1	žena
Emilie	Emilie	k1gFnSc2	Emilie
<g/>
,	,	kIx,	,
rozené	rozený	k2eAgFnSc2d1	rozená
Kaczorowské	Kaczorowská	k1gFnSc2	Kaczorowská
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
-	-	kIx~	-
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
města	město	k1gNnSc2	město
Bielsko	Bielsko	k1gNnSc1	Bielsko
<g/>
-	-	kIx~	-
<g/>
Białé	Białé	k1gNnPc4	Białé
<g/>
,	,	kIx,	,
stojícího	stojící	k2eAgMnSc4d1	stojící
zhruba	zhruba	k6eAd1	zhruba
60	[number]	k4	60
kilometrů	kilometr	k1gInPc2	kilometr
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Krakova	Krakov	k1gInSc2	Krakov
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Karol	Karola	k1gFnPc2	Karola
dostal	dostat	k5eAaPmAgMnS	dostat
budoucí	budoucí	k2eAgMnSc1d1	budoucí
papež	papež	k1gMnSc1	papež
po	po	k7c6	po
otci	otec	k1gMnSc6	otec
<g/>
,	,	kIx,	,
Józef	Józef	k1gInSc4	Józef
snad	snad	k9	snad
po	po	k7c6	po
maršálu	maršál	k1gMnSc6	maršál
Józefu	Józef	k1gMnSc6	Józef
Piłsudském	Piłsudský	k2eAgMnSc6d1	Piłsudský
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
deset	deset	k4xCc4	deset
dní	den	k1gInPc2	den
před	před	k7c7	před
jeho	jeho	k3xOp3gNnSc7	jeho
narozením	narození	k1gNnSc7	narození
porazil	porazit	k5eAaPmAgInS	porazit
bolševická	bolševický	k2eAgNnPc4d1	bolševické
vojska	vojsko	k1gNnPc4	vojsko
u	u	k7c2	u
Kyjeva	Kyjev	k1gInSc2	Kyjev
nebo	nebo	k8xC	nebo
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gFnSc6	jehož
armádě	armáda	k1gFnSc6	armáda
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
dříve	dříve	k6eAd2	dříve
sloužil	sloužit	k5eAaImAgMnS	sloužit
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
starších	starý	k2eAgMnPc2d2	starší
sourozenců	sourozenec	k1gMnPc2	sourozenec
Karol	Karola	k1gFnPc2	Karola
Wojtyła	Wojtył	k1gInSc2	Wojtył
zažil	zažít	k5eAaPmAgMnS	zažít
pouze	pouze	k6eAd1	pouze
bratra	bratr	k1gMnSc4	bratr
Edmunda	Edmund	k1gMnSc4	Edmund
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
-	-	kIx~	-
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sestra	sestra	k1gFnSc1	sestra
Olga	Olga	k1gFnSc1	Olga
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
-	-	kIx~	-
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
zemřela	zemřít	k5eAaPmAgFnS	zemřít
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1920	[number]	k4	1920
jej	on	k3xPp3gInSc4	on
ve	v	k7c6	v
wadowickém	wadowický	k2eAgInSc6d1	wadowický
kostele	kostel	k1gInSc6	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
pokřtil	pokřtít	k5eAaPmAgMnS	pokřtít
vojenský	vojenský	k2eAgMnSc1d1	vojenský
kaplan	kaplan	k1gMnSc1	kaplan
Franciszek	Franciszka	k1gFnPc2	Franciszka
Żak	Żak	k1gMnSc1	Żak
<g/>
.	.	kIx.	.
</s>
<s>
Kmotry	kmotr	k1gMnPc4	kmotr
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
jeho	jeho	k3xOp3gMnPc7	jeho
teta	teta	k1gFnSc1	teta
a	a	k8xC	a
strýc	strýc	k1gMnSc1	strýc
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
Maria	Maria	k1gFnSc1	Maria
Wiadrowska	Wiadrowska	k1gFnSc1	Wiadrowska
a	a	k8xC	a
Józef	Józef	k1gMnSc1	Józef
Kuczmierczyk	Kuczmierczyk	k1gMnSc1	Kuczmierczyk
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
dětství	dětství	k1gNnSc4	dětství
Karol	Karola	k1gFnPc2	Karola
Wojtyła	Wojtyła	k1gMnSc1	Wojtyła
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
nejbližšího	blízký	k2eAgNnSc2d3	nejbližší
okolí	okolí	k1gNnSc2	okolí
oslovovali	oslovovat	k5eAaImAgMnP	oslovovat
Lolek	Lolek	k1gMnSc1	Lolek
<g/>
,	,	kIx,	,
prožil	prožít	k5eAaPmAgMnS	prožít
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
poschodí	poschodí	k1gNnSc6	poschodí
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
2	[number]	k4	2
na	na	k7c6	na
Rynku	rynek	k1gInSc6	rynek
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Kościelna	Kościelna	k1gFnSc1	Kościelna
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
naproti	naproti	k7c3	naproti
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
pokřtěn	pokřtěn	k2eAgInSc1d1	pokřtěn
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
žila	žít	k5eAaImAgFnS	žít
skromně	skromně	k6eAd1	skromně
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
středního	střední	k2eAgInSc2d1	střední
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
stálým	stálý	k2eAgInSc7d1	stálý
zdrojem	zdroj	k1gInSc7	zdroj
obživy	obživa	k1gFnSc2	obživa
byla	být	k5eAaImAgFnS	být
mzda	mzda	k1gFnSc1	mzda
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dříve	dříve	k6eAd2	dříve
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
úředník	úředník	k1gMnSc1	úředník
na	na	k7c6	na
Okresním	okresní	k2eAgNnSc6d1	okresní
doplňovacím	doplňovací	k2eAgNnSc6d1	doplňovací
velitelství	velitelství	k1gNnSc6	velitelství
rakousko-uherské	rakouskoherský	k2eAgFnSc2d1	rakousko-uherská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Polska	Polsko	k1gNnSc2	Polsko
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
jako	jako	k9	jako
poručík	poručík	k1gMnSc1	poručík
polské	polský	k2eAgFnSc2d1	polská
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
do	do	k7c2	do
zálohy	záloha	k1gFnSc2	záloha
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
kapitána	kapitán	k1gMnSc2	kapitán
odešel	odejít	k5eAaPmAgMnS	odejít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
zručná	zručný	k2eAgFnSc1d1	zručná
vyšívačka	vyšívačka	k1gFnSc1	vyšívačka
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
příležitostně	příležitostně	k6eAd1	příležitostně
nosila	nosit	k5eAaImAgFnS	nosit
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
Karolův	Karolův	k2eAgMnSc1d1	Karolův
bratr	bratr	k1gMnSc1	bratr
Edmund	Edmund	k1gMnSc1	Edmund
studoval	studovat	k5eAaImAgMnS	studovat
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
wadowického	wadowický	k2eAgNnSc2d1	wadowický
gymnázia	gymnázium	k1gNnSc2	gymnázium
medicínu	medicína	k1gFnSc4	medicína
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chlapeckém	chlapecký	k2eAgInSc6d1	chlapecký
věku	věk	k1gInSc6	věk
se	se	k3xPyFc4	se
Karol	Karol	k1gInSc1	Karol
Wojtyła	Wojtyłum	k1gNnSc2	Wojtyłum
rád	rád	k6eAd1	rád
věnoval	věnovat	k5eAaImAgInS	věnovat
kopané	kopaná	k1gFnSc2	kopaná
<g/>
,	,	kIx,	,
plavání	plavání	k1gNnSc2	plavání
nebo	nebo	k8xC	nebo
bruslení	bruslení	k1gNnSc2	bruslení
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
i	i	k9	i
pěších	pěší	k2eAgInPc2d1	pěší
výletů	výlet	k1gInPc2	výlet
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
Wadowic	Wadowice	k1gFnPc2	Wadowice
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
první	první	k4xOgFnSc2	první
třídy	třída	k1gFnSc2	třída
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1926	[number]	k4	1926
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
základní	základní	k2eAgFnSc6d1	základní
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
umístěná	umístěný	k2eAgFnSc1d1	umístěná
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
patře	patro	k1gNnSc6	patro
městské	městský	k2eAgFnSc2d1	městská
radnice	radnice	k1gFnSc2	radnice
<g/>
,	,	kIx,	,
vzdálené	vzdálený	k2eAgFnSc2d1	vzdálená
zhruba	zhruba	k6eAd1	zhruba
minutu	minuta	k1gFnSc4	minuta
chůze	chůze	k1gFnSc2	chůze
od	od	k7c2	od
bytu	byt	k1gInSc2	byt
rodiny	rodina	k1gFnSc2	rodina
Wojtyłových	Wojtyłová	k1gFnPc2	Wojtyłová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
třetí	třetí	k4xOgFnSc2	třetí
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
45	[number]	k4	45
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
vleklý	vleklý	k2eAgInSc4d1	vleklý
zánět	zánět	k1gInSc4	zánět
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
ledvin	ledvina	k1gFnPc2	ledvina
Karolova	Karolův	k2eAgFnSc1d1	Karolův
matka	matka	k1gFnSc1	matka
Emilie	Emilie	k1gFnSc2	Emilie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
byl	být	k5eAaImAgInS	být
Karol	Karol	k1gInSc1	Karol
Wojtyła	Wojtył	k1gInSc2	Wojtył
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c4	na
Státní	státní	k2eAgNnSc4d1	státní
chlapecké	chlapecký	k2eAgNnSc4d1	chlapecké
gymnázium	gymnázium	k1gNnSc4	gymnázium
Marcina	Marcin	k2eAgMnSc2d1	Marcin
Wadowity	Wadowita	k1gMnSc2	Wadowita
v	v	k7c6	v
Mickiewiczově	Mickiewiczův	k2eAgFnSc6d1	Mickiewiczova
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
škola	škola	k1gFnSc1	škola
mu	on	k3xPp3gMnSc3	on
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
kvalitní	kvalitní	k2eAgNnSc4d1	kvalitní
klasické	klasický	k2eAgNnSc4d1	klasické
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
navíc	navíc	k6eAd1	navíc
doplnil	doplnit	k5eAaPmAgInS	doplnit
studiem	studio	k1gNnSc7	studio
řečtiny	řečtina	k1gFnSc2	řečtina
a	a	k8xC	a
latiny	latina	k1gFnSc2	latina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gFnSc1	jeho
celoživotní	celoživotní	k2eAgFnSc1d1	celoživotní
zálibou	záliba	k1gFnSc7	záliba
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výukou	výuka	k1gFnSc7	výuka
neměl	mít	k5eNaImAgMnS	mít
výraznější	výrazný	k2eAgInSc4d2	výraznější
problémy	problém	k1gInPc4	problém
a	a	k8xC	a
vždy	vždy	k6eAd1	vždy
dosahoval	dosahovat	k5eAaImAgMnS	dosahovat
výborného	výborný	k2eAgInSc2d1	výborný
prospěchu	prospěch	k1gInSc2	prospěch
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
školu	škola	k1gFnSc4	škola
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
i	i	k8xC	i
řadě	řada	k1gFnSc6	řada
dalších	další	k2eAgFnPc2d1	další
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
patřilo	patřit	k5eAaImAgNnS	patřit
například	například	k6eAd1	například
členství	členství	k1gNnSc1	členství
v	v	k7c6	v
bratrstvu	bratrstvo	k1gNnSc6	bratrstvo
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
kaplanem	kaplan	k1gMnSc7	kaplan
bratrstva	bratrstvo	k1gNnSc2	bratrstvo
otcem	otec	k1gMnSc7	otec
Zacharem	Zachar	k1gMnSc7	Zachar
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
denním	denní	k2eAgInSc6d1	denní
rozvrhu	rozvrh	k1gInSc6	rozvrh
Karola	Karola	k1gFnSc1	Karola
Wojtyły	Wojtyła	k1gFnSc2	Wojtyła
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
docházky	docházka	k1gFnSc2	docházka
do	do	k7c2	do
gymnázia	gymnázium	k1gNnSc2	gymnázium
jeho	jeho	k3xOp3gNnSc1	jeho
životopisec	životopisec	k1gMnSc1	životopisec
George	Georg	k1gInSc2	Georg
Weigel	Weigel	k1gMnSc1	Weigel
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
:	:	kIx,	:
Díky	díky	k7c3	díky
otci	otec	k1gMnSc3	otec
se	se	k3xPyFc4	se
Karol	Karol	k1gInSc1	Karol
Wojtyła	Wojtyłum	k1gNnSc2	Wojtyłum
seznámil	seznámit	k5eAaPmAgInS	seznámit
s	s	k7c7	s
polským	polský	k2eAgInSc7d1	polský
romantismem	romantismus	k1gInSc7	romantismus
a	a	k8xC	a
doma	doma	k6eAd1	doma
i	i	k9	i
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
intenzivně	intenzivně	k6eAd1	intenzivně
pročítal	pročítat	k5eAaImAgMnS	pročítat
klasiky	klasika	k1gFnSc2	klasika
polské	polský	k2eAgFnSc2d1	polská
literární	literární	k2eAgFnSc2d1	literární
tradice	tradice	k1gFnSc2	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zájem	zájem	k1gInSc1	zájem
prohloubil	prohloubit	k5eAaPmAgInS	prohloubit
i	i	k8xC	i
aktivní	aktivní	k2eAgFnSc7d1	aktivní
činností	činnost	k1gFnSc7	činnost
v	v	k7c6	v
divadelních	divadelní	k2eAgInPc6d1	divadelní
spolcích	spolek	k1gInPc6	spolek
působících	působící	k2eAgMnPc2d1	působící
při	při	k7c6	při
kostele	kostel	k1gInSc6	kostel
a	a	k8xC	a
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
své	svůj	k3xOyFgFnSc2	svůj
herecké	herecký	k2eAgFnSc2d1	herecká
a	a	k8xC	a
režisérské	režisérský	k2eAgFnSc2d1	režisérská
kariéry	kariéra	k1gFnSc2	kariéra
se	se	k3xPyFc4	se
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
stal	stát	k5eAaPmAgMnS	stát
součástí	součást	k1gFnSc7	součást
něčeho	něco	k3yInSc2	něco
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
výrazně	výrazně	k6eAd1	výrazně
přesahovalo	přesahovat	k5eAaImAgNnS	přesahovat
estetické	estetický	k2eAgNnSc1d1	estetické
a	a	k8xC	a
intelektuální	intelektuální	k2eAgFnSc1d1	intelektuální
hranice	hranice	k1gFnSc1	hranice
běžného	běžný	k2eAgNnSc2d1	běžné
ochotnického	ochotnický	k2eAgNnSc2d1	ochotnické
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1932	[number]	k4	1932
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Edmund	Edmund	k1gMnSc1	Edmund
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
pacienta	pacient	k1gMnSc2	pacient
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
nakazil	nakazit	k5eAaPmAgMnS	nakazit
spálou	spála	k1gFnSc7	spála
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
si	se	k3xPyFc3	se
byli	být	k5eAaImAgMnP	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
velmi	velmi	k6eAd1	velmi
blízcí	blízký	k2eAgMnPc1d1	blízký
a	a	k8xC	a
podle	podle	k7c2	podle
otce	otec	k1gMnSc2	otec
Kazimieze	Kazimieze	k1gFnSc2	Kazimieze
Sudera	Suder	k1gMnSc2	Suder
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
s	s	k7c7	s
Karolem	Karol	k1gMnSc7	Karol
Wojtyłou	Wojtyła	k1gMnSc7	Wojtyła
setkal	setkat	k5eAaPmAgMnS	setkat
v	v	k7c6	v
semináři	seminář	k1gInSc6	seminář
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
na	na	k7c4	na
chlapce	chlapec	k1gMnPc4	chlapec
tato	tento	k3xDgFnSc1	tento
tragická	tragický	k2eAgFnSc1d1	tragická
událost	událost	k1gFnSc1	událost
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgInSc4d2	veliký
dopad	dopad	k1gInSc4	dopad
<g/>
,	,	kIx,	,
než	než	k8xS	než
dřívější	dřívější	k2eAgFnSc4d1	dřívější
smrt	smrt	k1gFnSc4	smrt
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Biřmování	biřmování	k1gNnPc4	biřmování
Karol	Karol	k1gInSc1	Karol
Wojtyła	Wojtył	k1gInSc2	Wojtył
přijal	přijmout	k5eAaPmAgInS	přijmout
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Gymnázium	gymnázium	k1gNnSc4	gymnázium
ukončil	ukončit	k5eAaPmAgInS	ukončit
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
s	s	k7c7	s
maturitním	maturitní	k2eAgNnSc7d1	maturitní
vysvědčením	vysvědčení	k1gNnSc7	vysvědčení
s	s	k7c7	s
nejlepšími	dobrý	k2eAgFnPc7d3	nejlepší
známkami	známka	k1gFnPc7	známka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
zahájit	zahájit	k5eAaPmF	zahájit
studia	studio	k1gNnPc4	studio
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
skládal	skládat	k5eAaImAgMnS	skládat
přijímací	přijímací	k2eAgFnPc4d1	přijímací
zkoušky	zkouška	k1gFnPc4	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
obor	obor	k1gInSc4	obor
si	se	k3xPyFc3	se
zvolil	zvolit	k5eAaPmAgMnS	zvolit
polonistiku	polonistika	k1gFnSc4	polonistika
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
Jagellonské	jagellonský	k2eAgFnSc2d1	Jagellonská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1938	[number]	k4	1938
se	se	k3xPyFc4	se
Karol	Karol	k1gInSc1	Karol
Wojtyła	Wojtyłum	k1gNnSc2	Wojtyłum
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
do	do	k7c2	do
suterénního	suterénní	k2eAgInSc2d1	suterénní
bytu	byt	k1gInSc2	byt
v	v	k7c6	v
Tyniecké	Tyniecký	k2eAgFnSc6d1	Tyniecký
ulici	ulice	k1gFnSc6	ulice
čp.	čp.	k?	čp.
10	[number]	k4	10
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc4	dům
postavil	postavit	k5eAaPmAgMnS	postavit
bratr	bratr	k1gMnSc1	bratr
jeho	jeho	k3xOp3gFnSc2	jeho
matky	matka	k1gFnSc2	matka
Emilie	Emilie	k1gFnSc1	Emilie
Robert	Robert	k1gMnSc1	Robert
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
vzdálen	vzdálit	k5eAaPmNgMnS	vzdálit
zhruba	zhruba	k6eAd1	zhruba
dvacet	dvacet	k4xCc4	dvacet
minut	minuta	k1gFnPc2	minuta
chůze	chůze	k1gFnSc2	chůze
od	od	k7c2	od
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
začal	začít	k5eAaPmAgInS	začít
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
počátkem	počátkem	k7c2	počátkem
měsíce	měsíc	k1gInSc2	měsíc
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
studia	studio	k1gNnSc2	studio
chodil	chodit	k5eAaImAgInS	chodit
na	na	k7c4	na
výuku	výuka	k1gFnSc4	výuka
polské	polský	k2eAgFnSc2d1	polská
etymologie	etymologie	k1gFnSc2	etymologie
<g/>
,	,	kIx,	,
fonetiky	fonetika	k1gFnSc2	fonetika
a	a	k8xC	a
tvarosloví	tvarosloví	k1gNnSc2	tvarosloví
<g/>
,	,	kIx,	,
účastnil	účastnit	k5eAaImAgInS	účastnit
se	se	k3xPyFc4	se
přednášek	přednáška	k1gFnPc2	přednáška
o	o	k7c4	o
interpretaci	interpretace	k1gFnSc4	interpretace
literárního	literární	k2eAgInSc2d1	literární
textu	text	k1gInSc2	text
<g/>
,	,	kIx,	,
na	na	k7c4	na
úvod	úvod	k1gInSc4	úvod
do	do	k7c2	do
středověké	středověký	k2eAgFnSc2d1	středověká
<g/>
,	,	kIx,	,
novověké	novověký	k2eAgFnSc2d1	novověká
a	a	k8xC	a
současné	současný	k2eAgFnSc2d1	současná
polské	polský	k2eAgFnSc2d1	polská
poezie	poezie	k1gFnSc2	poezie
<g/>
,	,	kIx,	,
dramatu	drama	k1gNnSc2	drama
a	a	k8xC	a
prózy	próza	k1gFnSc2	próza
a	a	k8xC	a
úvodu	úvod	k1gInSc2	úvod
do	do	k7c2	do
ruského	ruský	k2eAgInSc2d1	ruský
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
staroslověnštiny	staroslověnština	k1gFnSc2	staroslověnština
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
divadelní	divadelní	k2eAgFnSc2d1	divadelní
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
několika	několik	k4yIc2	několik
recitačních	recitační	k2eAgFnPc2d1	recitační
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
členem	člen	k1gInSc7	člen
Kroužku	kroužek	k1gInSc2	kroužek
studentů	student	k1gMnPc2	student
polonistiky	polonistika	k1gFnSc2	polonistika
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
vzdělávat	vzdělávat	k5eAaImF	vzdělávat
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
jeho	jeho	k3xOp3gFnPc1	jeho
aktivity	aktivita	k1gFnPc1	aktivita
včetně	včetně	k7c2	včetně
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
studia	studio	k1gNnSc2	studio
však	však	k9	však
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
přerušila	přerušit	k5eAaPmAgFnS	přerušit
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
se	se	k3xPyFc4	se
otec	otec	k1gMnSc1	otec
a	a	k8xC	a
syn	syn	k1gMnSc1	syn
Wojtyłovi	Wojtyłův	k2eAgMnPc1d1	Wojtyłův
pokusili	pokusit	k5eAaPmAgMnP	pokusit
uprchnout	uprchnout	k5eAaPmF	uprchnout
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
sovětská	sovětský	k2eAgFnSc1d1	sovětská
blokáda	blokáda	k1gFnSc1	blokáda
polské	polský	k2eAgFnSc2d1	polská
hranice	hranice	k1gFnSc2	hranice
je	on	k3xPp3gInPc4	on
přinutila	přinutit	k5eAaPmAgFnS	přinutit
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Krakova	Krakov	k1gInSc2	Krakov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1940	[number]	k4	1940
si	se	k3xPyFc3	se
Karol	Karol	k1gInSc4	Karol
Wojtyła	Wojtyła	k1gFnSc1	Wojtyła
musel	muset	k5eAaImAgInS	muset
naléhavě	naléhavě	k6eAd1	naléhavě
sehnat	sehnat	k5eAaPmF	sehnat
trvalé	trvalý	k2eAgNnSc4d1	trvalé
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nebyl	být	k5eNaImAgInS	být
deportován	deportován	k2eAgInSc4d1	deportován
k	k	k7c3	k
nuceným	nucený	k2eAgFnPc3d1	nucená
pracím	práce	k1gFnPc3	práce
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
uplatnění	uplatnění	k1gNnSc4	uplatnění
nalezl	naleznout	k5eAaPmAgMnS	naleznout
v	v	k7c6	v
kamenolomu	kamenolom	k1gInSc6	kamenolom
v	v	k7c6	v
krakovské	krakovský	k2eAgFnSc6d1	Krakovská
čtvrti	čtvrt	k1gFnSc6	čtvrt
Zakrzywek	Zakrzywek	k6eAd1	Zakrzywek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
těžil	těžit	k5eAaImAgInS	těžit
vápenec	vápenec	k1gInSc1	vápenec
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
sody	soda	k1gFnSc2	soda
v	v	k7c6	v
chemičce	chemička	k1gFnSc6	chemička
Solvay	Solvaa	k1gFnSc2	Solvaa
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
nakládal	nakládat	k5eAaImAgInS	nakládat
vápenec	vápenec	k1gInSc1	vápenec
do	do	k7c2	do
důlních	důlní	k2eAgInPc2d1	důlní
vozíků	vozík	k1gInPc2	vozík
nebo	nebo	k8xC	nebo
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
brzdař	brzdař	k1gMnSc1	brzdař
na	na	k7c6	na
důlních	důlní	k2eAgInPc6d1	důlní
vlacích	vlak	k1gInPc6	vlak
<g/>
,	,	kIx,	,
po	po	k7c6	po
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
však	však	k9	však
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
do	do	k7c2	do
pozice	pozice	k1gFnSc2	pozice
pomocníka	pomocník	k1gMnSc4	pomocník
odstřelovače	odstřelovač	k1gMnSc4	odstřelovač
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
upoutalo	upoutat	k5eAaPmAgNnS	upoutat
na	na	k7c4	na
lůžko	lůžko	k1gNnSc4	lůžko
onemocnění	onemocnění	k1gNnSc2	onemocnění
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1941	[number]	k4	1941
byl	být	k5eAaImAgInS	být
Karol	Karol	k1gInSc1	Karol
Wojtyła	Wojtyłum	k1gNnSc2	Wojtyłum
převelen	převelen	k2eAgInSc1d1	převelen
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
chemičky	chemička	k1gFnSc2	chemička
Solvay	Solvaa	k1gFnSc2	Solvaa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
u	u	k7c2	u
čističky	čistička	k1gFnSc2	čistička
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
zde	zde	k6eAd1	zde
nebyla	být	k5eNaImAgFnS	být
tolik	tolik	k6eAd1	tolik
namáhavá	namáhavý	k2eAgFnSc1d1	namáhavá
jako	jako	k8xS	jako
v	v	k7c6	v
kamenolomu	kamenolom	k1gInSc6	kamenolom
a	a	k8xC	a
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
mu	on	k3xPp3gMnSc3	on
čtení	čtení	k1gNnSc3	čtení
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
této	tento	k3xDgFnSc3	tento
okolnosti	okolnost	k1gFnSc3	okolnost
dobrovolně	dobrovolně	k6eAd1	dobrovolně
sloužil	sloužit	k5eAaImAgMnS	sloužit
noční	noční	k2eAgFnPc4d1	noční
směny	směna	k1gFnPc4	směna
<g/>
,	,	kIx,	,
během	během	k7c2	během
nichž	jenž	k3xRgInPc2	jenž
dospěl	dochvít	k5eAaPmAgInS	dochvít
k	k	k7c3	k
novému	nový	k2eAgNnSc3d1	nové
pochopení	pochopení	k1gNnSc3	pochopení
mariánského	mariánský	k2eAgInSc2d1	mariánský
kultu	kult	k1gInSc2	kult
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
typických	typický	k2eAgInPc2d1	typický
rysů	rys	k1gInPc2	rys
polského	polský	k2eAgInSc2d1	polský
katolicismu	katolicismus	k1gInSc2	katolicismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
bytě	byt	k1gInSc6	byt
v	v	k7c6	v
Tyniecké	Tyniecký	k2eAgFnSc6d1	Tyniecký
ulici	ulice	k1gFnSc6	ulice
ukryl	ukrýt	k5eAaPmAgMnS	ukrýt
před	před	k7c7	před
Němci	Němec	k1gMnPc7	Němec
přítele	přítel	k1gMnSc2	přítel
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
divadelních	divadelní	k2eAgFnPc2d1	divadelní
aktivit	aktivita	k1gFnPc2	aktivita
ve	v	k7c6	v
Wadowicích	Wadowice	k1gFnPc6	Wadowice
Miedzysłava	Miedzysłava	k1gFnSc1	Miedzysłava
Kotlarczyka	Kotlarczyka	k1gFnSc1	Kotlarczyka
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
manželku	manželka	k1gFnSc4	manželka
Zofiu	Zofius	k1gMnSc3	Zofius
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Kotlarczykem	Kotlarczyk	k1gInSc7	Kotlarczyk
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
založili	založit	k5eAaPmAgMnP	založit
Rapsodické	rapsodický	k2eAgNnSc4d1	Rapsodické
divadlo	divadlo	k1gNnSc4	divadlo
(	(	kIx(	(
<g/>
Teatr	Teatr	k1gInSc1	Teatr
Rapsodyczny	Rapsodyczna	k1gFnSc2	Rapsodyczna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
první	první	k4xOgNnSc1	první
představení	představení	k1gNnSc1	představení
<g/>
,	,	kIx,	,
Słowackého	Słowackého	k2eAgMnSc1d1	Słowackého
Král	Král	k1gMnSc1	Král
Duch	duch	k1gMnSc1	duch
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
utajení	utajení	k1gNnSc6	utajení
konalo	konat	k5eAaImAgNnS	konat
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
měsících	měsíc	k1gInPc6	měsíc
se	se	k3xPyFc4	se
členové	člen	k1gMnPc1	člen
Rapsodického	rapsodický	k2eAgNnSc2d1	Rapsodické
divadla	divadlo	k1gNnSc2	divadlo
spojili	spojit	k5eAaPmAgMnP	spojit
s	s	k7c7	s
podzemní	podzemní	k2eAgFnSc7d1	podzemní
organizací	organizace	k1gFnSc7	organizace
UNIA	UNIA	kA	UNIA
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
svázána	svázat	k5eAaPmNgFnS	svázat
s	s	k7c7	s
katolickým	katolický	k2eAgNnSc7d1	katolické
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
nástrojem	nástroj	k1gInSc7	nástroj
ideologického	ideologický	k2eAgInSc2d1	ideologický
a	a	k8xC	a
kulturního	kulturní	k2eAgInSc2d1	kulturní
odpoje	odpoj	k1gInSc2	odpoj
proti	proti	k7c3	proti
nacistickým	nacistický	k2eAgMnPc3d1	nacistický
okupantům	okupant	k1gMnPc3	okupant
a	a	k8xC	a
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
chránit	chránit	k5eAaImF	chránit
pronásledované	pronásledovaný	k2eAgMnPc4d1	pronásledovaný
Židy	Žid	k1gMnPc4	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
1941	[number]	k4	1941
až	až	k9	až
do	do	k7c2	do
půlky	půlka	k1gFnSc2	půlka
roku	rok	k1gInSc2	rok
následujícího	následující	k2eAgInSc2d1	následující
<g/>
,	,	kIx,	,
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
smrtí	smrt	k1gFnSc7	smrt
otce	otec	k1gMnSc2	otec
i	i	k8xC	i
dramatickými	dramatický	k2eAgFnPc7d1	dramatická
událostmi	událost	k1gFnPc7	událost
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
<g/>
,	,	kIx,	,
prodělával	prodělávat	k5eAaImAgMnS	prodělávat
Karol	Karol	k1gInSc4	Karol
Wojtyła	Wojtył	k1gInSc2	Wojtył
psychologický	psychologický	k2eAgInSc4d1	psychologický
zápas	zápas	k1gInSc4	zápas
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
později	pozdě	k6eAd2	pozdě
definoval	definovat	k5eAaBmAgInS	definovat
jako	jako	k9	jako
proces	proces	k1gInSc1	proces
"	"	kIx"	"
<g/>
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
osvícení	osvícení	k1gNnSc2	osvícení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
dozrálo	dozrát	k5eAaPmAgNnS	dozrát
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
vyvolen	vyvolit	k5eAaPmNgMnS	vyvolit
pro	pro	k7c4	pro
kněžskou	kněžský	k2eAgFnSc4d1	kněžská
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
důvodů	důvod	k1gInPc2	důvod
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
zažádal	zažádat	k5eAaPmAgMnS	zažádat
v	v	k7c6	v
rezidenci	rezidence	k1gFnSc6	rezidence
krakovského	krakovský	k2eAgMnSc2d1	krakovský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
o	o	k7c4	o
přijetí	přijetí	k1gNnSc4	přijetí
za	za	k7c4	za
kandidáta	kandidát	k1gMnSc4	kandidát
kněžství	kněžství	k1gNnSc2	kněžství
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
rektor	rektor	k1gMnSc1	rektor
semináře	seminář	k1gInSc2	seminář
Jana	Jana	k1gFnSc1	Jana
Piwowarczyk	Piwowarczyk	k1gMnSc1	Piwowarczyk
jeho	jeho	k3xOp3gFnSc2	jeho
žádosti	žádost	k1gFnSc2	žádost
vyhověl	vyhovět	k5eAaPmAgInS	vyhovět
a	a	k8xC	a
Karol	Karol	k1gInSc1	Karol
Wojtyła	Wojtył	k1gInSc2	Wojtył
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
deseti	deset	k4xCc2	deset
seminaristů	seminarista	k1gMnPc2	seminarista
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
studium	studium	k1gNnSc1	studium
muselo	muset	k5eAaImAgNnS	muset
kvůli	kvůli	k7c3	kvůli
nacistickým	nacistický	k2eAgFnPc3d1	nacistická
represáliím	represálie	k1gFnPc3	represálie
probíhat	probíhat	k5eAaImF	probíhat
v	v	k7c6	v
ilegalitě	ilegalita	k1gFnSc6	ilegalita
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
osudové	osudový	k2eAgNnSc1d1	osudové
potvrzení	potvrzení	k1gNnSc1	potvrzení
svého	svůj	k3xOyFgNnSc2	svůj
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
stát	stát	k5eAaPmF	stát
se	s	k7c7	s
knězem	kněz	k1gMnSc7	kněz
vnímal	vnímat	k5eAaImAgMnS	vnímat
i	i	k9	i
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
29	[number]	k4	29
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1944	[number]	k4	1944
přežil	přežít	k5eAaPmAgMnS	přežít
nehodu	nehoda	k1gFnSc4	nehoda
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
ho	on	k3xPp3gMnSc4	on
srazil	srazit	k5eAaPmAgMnS	srazit
a	a	k8xC	a
vážně	vážně	k6eAd1	vážně
poranil	poranit	k5eAaPmAgMnS	poranit
německý	německý	k2eAgInSc4d1	německý
nákladní	nákladní	k2eAgInSc4d1	nákladní
automobil	automobil	k1gInSc4	automobil
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
strávit	strávit	k5eAaPmF	strávit
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
postupem	postup	k1gInSc7	postup
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1944	[number]	k4	1944
v	v	k7c6	v
polské	polský	k2eAgFnSc6d1	polská
metropoli	metropol	k1gFnSc6	metropol
Varšavě	Varšava	k1gFnSc6	Varšava
protiněmecké	protiněmecký	k2eAgNnSc4d1	protiněmecké
povstání	povstání	k1gNnSc4	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
akce	akce	k1gFnPc4	akce
Zemské	zemský	k2eAgFnSc2d1	zemská
armády	armáda	k1gFnSc2	armáda
provedly	provést	k5eAaPmAgFnP	provést
o	o	k7c4	o
šest	šest	k4xCc4	šest
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
oddíly	oddíl	k1gInPc1	oddíl
wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
dislokované	dislokovaný	k2eAgInPc1d1	dislokovaný
v	v	k7c6	v
Krakově	krakův	k2eAgFnSc6d1	Krakova
razii	razie	k1gFnSc6	razie
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
nalézt	nalézt	k5eAaPmF	nalézt
osoby	osoba	k1gFnPc4	osoba
protinacistického	protinacistický	k2eAgInSc2d1	protinacistický
odboje	odboj	k1gInSc2	odboj
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
od	od	k7c2	od
15	[number]	k4	15
do	do	k7c2	do
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vojáci	voják	k1gMnPc1	voják
provedli	provést	k5eAaPmAgMnP	provést
prohlídku	prohlídka	k1gFnSc4	prohlídka
i	i	k8xC	i
v	v	k7c6	v
domě	dům	k1gInSc6	dům
čp.	čp.	k?	čp.
10	[number]	k4	10
v	v	k7c6	v
Tyniecké	Tyniecký	k2eAgFnSc6d1	Tyniecký
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
Karola	Karola	k1gFnSc1	Karola
Wojtyłu	Wojtyłus	k1gInSc2	Wojtyłus
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
nalézt	nalézt	k5eAaBmF	nalézt
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
přehlédli	přehlédnout	k5eAaPmAgMnP	přehlédnout
dveře	dveře	k1gFnPc4	dveře
za	za	k7c7	za
schodištěm	schodiště	k1gNnSc7	schodiště
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vedly	vést	k5eAaImAgInP	vést
do	do	k7c2	do
jeho	jeho	k3xOp3gInSc2	jeho
bytu	byt	k1gInSc2	byt
(	(	kIx(	(
<g/>
Karolovi	Karolův	k2eAgMnPc1d1	Karolův
přátelé	přítel	k1gMnPc1	přítel
mu	on	k3xPp3gMnSc3	on
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
polohu	poloha	k1gFnSc4	poloha
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
ulice	ulice	k1gFnSc2	ulice
říkali	říkat	k5eAaImAgMnP	říkat
katakomby	katakomby	k1gFnPc1	katakomby
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
se	se	k3xPyFc4	se
však	však	k9	však
záhy	záhy	k6eAd1	záhy
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
hledaných	hledaný	k2eAgFnPc2d1	hledaná
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
na	na	k7c4	na
výzvu	výzva	k1gFnSc4	výzva
krakovského	krakovský	k2eAgMnSc2d1	krakovský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
monsignore	monsignore	k1gMnSc1	monsignore
Adama	Adam	k1gMnSc2	Adam
Sapieha	Sapieha	k1gMnSc1	Sapieha
dostavil	dostavit	k5eAaPmAgMnS	dostavit
do	do	k7c2	do
arcibiskupského	arcibiskupský	k2eAgInSc2d1	arcibiskupský
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
kvůli	kvůli	k7c3	kvůli
stávajícím	stávající	k2eAgFnPc3d1	stávající
okolnostem	okolnost	k1gFnPc3	okolnost
přeměněn	přeměněn	k2eAgInSc4d1	přeměněn
na	na	k7c4	na
podzemní	podzemní	k2eAgInSc4d1	podzemní
seminář	seminář	k1gInSc4	seminář
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
důvodů	důvod	k1gInPc2	důvod
pak	pak	k6eAd1	pak
studenti	student	k1gMnPc1	student
semináře	seminář	k1gInSc2	seminář
i	i	k8xC	i
jejich	jejich	k3xOp3gFnSc4	jejich
profesoři	profesor	k1gMnPc1	profesor
setrvávali	setrvávat	k5eAaImAgMnP	setrvávat
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
Karol	Karol	k1gInSc1	Karol
Wojtyła	Wojtył	k1gInSc2	Wojtył
dvě	dva	k4xCgNnPc4	dva
nižší	nízký	k2eAgNnPc4d2	nižší
svěcení	svěcení	k1gNnPc4	svěcení
a	a	k8xC	a
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
středověkým	středověký	k2eAgInSc7d1	středověký
obřadem	obřad	k1gInSc7	obřad
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
vyholena	vyholen	k2eAgFnSc1d1	vyholena
tonzura	tonzura	k1gFnSc1	tonzura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1945	[number]	k4	1945
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
posledního	poslední	k2eAgInSc2d1	poslední
ročníku	ročník	k1gInSc2	ročník
teologie	teologie	k1gFnSc2	teologie
na	na	k7c6	na
znovuotevřené	znovuotevřený	k2eAgFnSc6d1	znovuotevřená
Jagellonské	jagellonský	k2eAgFnSc6d1	Jagellonská
universitě	universita	k1gFnSc6	universita
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
si	se	k3xPyFc3	se
přivydělávat	přivydělávat	k5eAaImF	přivydělávat
jako	jako	k9	jako
odborný	odborný	k2eAgMnSc1d1	odborný
asistent	asistent	k1gMnSc1	asistent
v	v	k7c6	v
nižších	nízký	k2eAgInPc6d2	nižší
ročnících	ročník	k1gInPc6	ročník
(	(	kIx(	(
<g/>
přednášel	přednášet	k5eAaImAgMnS	přednášet
zde	zde	k6eAd1	zde
systematickou	systematický	k2eAgFnSc4d1	systematická
teologii	teologie	k1gFnSc4	teologie
a	a	k8xC	a
dějiny	dějiny	k1gFnPc4	dějiny
věrouky	věrouka	k1gFnSc2	věrouka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
pak	pak	k6eAd1	pak
získal	získat	k5eAaPmAgMnS	získat
další	další	k2eAgNnSc4d1	další
nižší	nízký	k2eAgNnSc4d2	nižší
svěcení	svěcení	k1gNnSc4	svěcení
a	a	k8xC	a
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
i	i	k9	i
sutanu	sutana	k1gFnSc4	sutana
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
června	červen	k1gInSc2	červen
a	a	k8xC	a
července	červenec	k1gInSc2	červenec
1946	[number]	k4	1946
úspěšně	úspěšně	k6eAd1	úspěšně
složil	složit	k5eAaPmAgMnS	složit
teologické	teologický	k2eAgFnPc4d1	teologická
zkoušky	zkouška	k1gFnPc4	zkouška
vyžadované	vyžadovaný	k2eAgFnPc1d1	vyžadovaná
ke	k	k7c3	k
kněžskému	kněžský	k2eAgNnSc3d1	kněžské
svěcení	svěcení	k1gNnSc3	svěcení
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
byl	být	k5eAaImAgInS	být
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
kardinálem	kardinál	k1gMnSc7	kardinál
Sapiehou	Sapieha	k1gMnSc7	Sapieha
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
zrychleného	zrychlený	k2eAgInSc2d1	zrychlený
plánu	plán	k1gInSc2	plán
<g/>
,	,	kIx,	,
vysvěcen	vysvěcen	k2eAgMnSc1d1	vysvěcen
na	na	k7c4	na
podjáhna	podjáhen	k1gMnSc4	podjáhen
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
vysvěcen	vysvěcen	k2eAgInSc1d1	vysvěcen
na	na	k7c4	na
jáhna	jáhen	k1gMnSc4	jáhen
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
)	)	kIx)	)
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1946	[number]	k4	1946
se	se	k3xPyFc4	se
po	po	k7c6	po
tradičním	tradiční	k2eAgInSc6d1	tradiční
obřadu	obřad	k1gInSc6	obřad
stal	stát	k5eAaPmAgInS	stát
knězem	kněz	k1gMnSc7	kněz
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
pak	pak	k6eAd1	pak
otec	otec	k1gMnSc1	otec
Wojtyła	Wojtyła	k1gMnSc1	Wojtyła
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgMnS	mít
po	po	k7c4	po
následující	následující	k2eAgInPc4d1	následující
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
doktorském	doktorský	k2eAgNnSc6d1	doktorské
studiu	studio	k1gNnSc6	studio
teologie	teologie	k1gFnSc2	teologie
na	na	k7c6	na
papežském	papežský	k2eAgInSc6d1	papežský
Athenaeu	Athenaeus	k1gInSc6	Athenaeus
sv.	sv.	kA	sv.
Tomáše	Tomáš	k1gMnSc2	Tomáš
Akvinského	Akvinský	k2eAgMnSc2d1	Akvinský
<g/>
.	.	kIx.	.
</s>
<s>
Magisterské	magisterský	k2eAgFnPc1d1	magisterská
zkoušky	zkouška	k1gFnPc1	zkouška
s	s	k7c7	s
výsledkem	výsledek	k1gInSc7	výsledek
čtyřicet	čtyřicet	k4xCc4	čtyřicet
bodů	bod	k1gInPc2	bod
z	z	k7c2	z
možných	možný	k2eAgInPc2d1	možný
čtyřiceti	čtyřicet	k4xCc2	čtyřicet
vykonal	vykonat	k5eAaPmAgInS	vykonat
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
doktorské	doktorský	k2eAgFnPc4d1	doktorská
zkoušky	zkouška	k1gFnPc4	zkouška
pak	pak	k6eAd1	pak
složil	složit	k5eAaPmAgMnS	složit
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc4	titul
doktor	doktor	k1gMnSc1	doktor
teologie	teologie	k1gFnSc2	teologie
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
byl	být	k5eAaImAgInS	být
udělen	udělit	k5eAaPmNgInS	udělit
až	až	k9	až
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
na	na	k7c6	na
Jagellonské	jagellonský	k2eAgFnSc6d1	Jagellonská
universitě	universita	k1gFnSc6	universita
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1948	[number]	k4	1948
se	se	k3xPyFc4	se
Karol	Karol	k1gInSc1	Karol
Wojtyła	Wojtyłum	k1gNnSc2	Wojtyłum
vrátil	vrátit	k5eAaPmAgInS	vrátit
z	z	k7c2	z
Říma	Řím	k1gInSc2	Řím
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
vesnice	vesnice	k1gFnSc2	vesnice
Niegowce	Niegowce	k1gFnSc2	Niegowce
(	(	kIx(	(
<g/>
asi	asi	k9	asi
30	[number]	k4	30
km	km	kA	km
od	od	k7c2	od
Krakova	Krakov	k1gInSc2	Krakov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
kaplana	kaplan	k1gMnSc2	kaplan
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
působiště	působiště	k1gNnSc2	působiště
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1949	[number]	k4	1949
odvolán	odvolat	k5eAaPmNgInS	odvolat
do	do	k7c2	do
farnosti	farnost	k1gFnSc2	farnost
sv.	sv.	kA	sv.
Floriána	Florián	k1gMnSc2	Florián
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
Niegowci	Niegowce	k1gFnSc6	Niegowce
věnoval	věnovat	k5eAaPmAgMnS	věnovat
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
mládeží	mládež	k1gFnSc7	mládež
<g/>
,	,	kIx,	,
pořádal	pořádat	k5eAaImAgInS	pořádat
diskusní	diskusní	k2eAgInPc4d1	diskusní
semináře	seminář	k1gInPc4	seminář
pro	pro	k7c4	pro
studenty	student	k1gMnPc4	student
<g/>
,	,	kIx,	,
zahájil	zahájit	k5eAaPmAgInS	zahájit
vůbec	vůbec	k9	vůbec
první	první	k4xOgInSc1	první
program	program	k1gInSc1	program
předmanželské	předmanželský	k2eAgFnSc2d1	předmanželská
přípravy	příprava	k1gFnSc2	příprava
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
krakovské	krakovský	k2eAgFnSc2d1	Krakovská
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
vedoucím	vedoucí	k1gMnSc7	vedoucí
skupiny	skupina	k1gFnSc2	skupina
<g />
.	.	kIx.	.
</s>
<s>
Środowisko	Środowisko	k1gNnSc1	Środowisko
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
členové	člen	k1gMnPc1	člen
ho	on	k3xPp3gMnSc4	on
začali	začít	k5eAaPmAgMnP	začít
oslovovat	oslovovat	k5eAaImF	oslovovat
Wujku	Wujek	k1gMnSc3	Wujek
(	(	kIx(	(
<g/>
strýčku	strýček	k1gMnSc3	strýček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
nového	nový	k2eAgMnSc2d1	nový
krakovského	krakovský	k2eAgMnSc2d1	krakovský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
Baziaka	Baziak	k1gMnSc2	Baziak
<g/>
,	,	kIx,	,
odešel	odejít	k5eAaPmAgMnS	odejít
otec	otec	k1gMnSc1	otec
Wojtyła	Wojtył	k1gInSc2	Wojtył
na	na	k7c4	na
dvouletou	dvouletý	k2eAgFnSc4d1	dvouletá
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
dovolenou	dovolená	k1gFnSc4	dovolená
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
naplno	naplno	k6eAd1	naplno
věnovat	věnovat	k5eAaPmF	věnovat
habilitační	habilitační	k2eAgFnSc4d1	habilitační
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
názvem	název	k1gInSc7	název
Zhodnocení	zhodnocení	k1gNnSc1	zhodnocení
možnosti	možnost	k1gFnSc2	možnost
vybudování	vybudování	k1gNnSc2	vybudování
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
etiky	etika	k1gFnSc2	etika
na	na	k7c6	na
základě	základ	k1gInSc6	základ
filozofického	filozofický	k2eAgInSc2d1	filozofický
systému	systém	k1gInSc2	systém
Maxe	Max	k1gMnSc2	Max
Schelera	Scheler	k1gMnSc2	Scheler
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
měl	mít	k5eAaImAgMnS	mít
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
odpovídající	odpovídající	k2eAgInSc4d1	odpovídající
klid	klid	k1gInSc4	klid
<g/>
,	,	kIx,	,
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
21	[number]	k4	21
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Kanonicza	Kanonicz	k1gMnSc2	Kanonicz
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
iniciativy	iniciativa	k1gFnSc2	iniciativa
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
sloužit	sloužit	k5eAaImF	sloužit
mše	mše	k1gFnSc1	mše
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Kateřiny	Kateřina	k1gFnSc2	Kateřina
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Kazimierz	Kazimierza	k1gFnPc2	Kazimierza
<g/>
.	.	kIx.	.
</s>
<s>
Habilitační	habilitační	k2eAgFnSc4d1	habilitační
práci	práce	k1gFnSc4	práce
otce	otec	k1gMnSc2	otec
Wojtyły	Wojtyła	k1gMnSc2	Wojtyła
jednohlasně	jednohlasně	k6eAd1	jednohlasně
schválila	schválit	k5eAaPmAgFnS	schválit
tříčlenná	tříčlenný	k2eAgFnSc1d1	tříčlenná
porota	porota	k1gFnSc1	porota
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc4	jejíž
doporučení	doporučení	k1gNnSc4	doporučení
Rada	rada	k1gFnSc1	rada
teologické	teologický	k2eAgFnSc2d1	teologická
fakulty	fakulta	k1gFnSc2	fakulta
akceptovala	akceptovat	k5eAaBmAgFnS	akceptovat
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
doktorský	doktorský	k2eAgInSc1d1	doktorský
titul	titul	k1gInSc1	titul
obdržel	obdržet	k5eAaPmAgInS	obdržet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
profesorskou	profesorský	k2eAgFnSc4d1	profesorská
dráhu	dráha	k1gFnSc4	dráha
zahájil	zahájit	k5eAaPmAgMnS	zahájit
jako	jako	k8xC	jako
docent	docent	k1gMnSc1	docent
filozofické	filozofický	k2eAgFnSc2d1	filozofická
etiky	etika	k1gFnSc2	etika
na	na	k7c6	na
filosofické	filosofický	k2eAgFnSc6d1	filosofická
fakultě	fakulta	k1gFnSc6	fakulta
Katolické	katolický	k2eAgFnSc2d1	katolická
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Lublinu	Lublin	k1gInSc6	Lublin
<g/>
.	.	kIx.	.
</s>
<s>
Přednášky	přednáška	k1gFnPc1	přednáška
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
konal	konat	k5eAaImAgInS	konat
jednou	jednou	k6eAd1	jednou
týdně	týdně	k6eAd1	týdně
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k9	jako
kaplan	kaplan	k1gMnSc1	kaplan
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
učitel	učitel	k1gMnSc1	učitel
byl	být	k5eAaImAgMnS	být
nebývale	nebývale	k6eAd1	nebývale
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
přístupný	přístupný	k2eAgInSc1d1	přístupný
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
nebo	nebo	k8xC	nebo
mimo	mimo	k7c4	mimo
ni	on	k3xPp3gFnSc4	on
a	a	k8xC	a
studenti	student	k1gMnPc1	student
jej	on	k3xPp3gMnSc4	on
hodnotili	hodnotit	k5eAaImAgMnP	hodnotit
jako	jako	k8xC	jako
svého	svůj	k3xOyFgNnSc2	svůj
nejoblíbenějšího	oblíbený	k2eAgNnSc2d3	nejoblíbenější
a	a	k8xC	a
nejsvatějšího	svatý	k2eAgMnSc2d3	nejsvatější
profesora	profesor	k1gMnSc2	profesor
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
běžného	běžný	k2eAgInSc2d1	běžný
profesorského	profesorský	k2eAgInSc2d1	profesorský
úvazku	úvazek	k1gInSc2	úvazek
současně	současně	k6eAd1	současně
vedl	vést	k5eAaImAgInS	vést
i	i	k9	i
doktorandský	doktorandský	k2eAgInSc1d1	doktorandský
seminář	seminář	k1gInSc1	seminář
z	z	k7c2	z
filosofické	filosofický	k2eAgFnSc2d1	filosofická
etiky	etika	k1gFnSc2	etika
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnPc1	jeho
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
žáci	žák	k1gMnPc1	žák
připravovali	připravovat	k5eAaImAgMnP	připravovat
na	na	k7c4	na
své	své	k1gNnSc4	své
disertační	disertační	k2eAgFnSc2d1	disertační
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1958	[number]	k4	1958
papež	papež	k1gMnSc1	papež
Pius	Pius	k1gMnSc1	Pius
XII	XII	kA	XII
<g/>
.	.	kIx.	.
nečekaně	nečekaně	k6eAd1	nečekaně
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
otce	otka	k1gFnSc6	otka
Karola	Karola	k1gFnSc1	Karola
Wojtyłu	Wojtyłus	k1gInSc2	Wojtyłus
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
pomocného	pomocný	k2eAgMnSc2d1	pomocný
biskupa	biskup	k1gMnSc2	biskup
krakovského	krakovský	k2eAgMnSc2d1	krakovský
a	a	k8xC	a
současně	současně	k6eAd1	současně
titulárním	titulární	k2eAgMnSc7d1	titulární
biskupem	biskup	k1gMnSc7	biskup
v	v	k7c6	v
Ombi	Omb	k1gFnSc6	Omb
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
nového	nový	k2eAgInSc2d1	nový
úřadu	úřad	k1gInSc2	úřad
byl	být	k5eAaImAgInS	být
vysvěcen	vysvětit	k5eAaPmNgInS	vysvětit
ve	v	k7c6	v
Wawelské	Wawelský	k2eAgFnSc6d1	Wawelská
katedrále	katedrála	k1gFnSc6	katedrála
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1958	[number]	k4	1958
a	a	k8xC	a
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
38	[number]	k4	38
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
tak	tak	k9	tak
stal	stát	k5eAaPmAgInS	stát
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
biskupem	biskup	k1gMnSc7	biskup
v	v	k7c6	v
polských	polský	k2eAgFnPc6d1	polská
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Služba	služba	k1gFnSc1	služba
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
pozici	pozice	k1gFnSc6	pozice
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
znamenala	znamenat	k5eAaImAgFnS	znamenat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
nových	nový	k2eAgFnPc2d1	nová
pastoračních	pastorační	k2eAgFnPc2d1	pastorační
povinností	povinnost	k1gFnPc2	povinnost
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
i	i	k9	i
nadále	nadále	k6eAd1	nadále
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
již	již	k6eAd1	již
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
<g/>
,	,	kIx,	,
učil	učít	k5eAaPmAgMnS	učít
na	na	k7c6	na
lublaňské	lublaňský	k2eAgFnSc6d1	Lublaňská
univerzitě	univerzita	k1gFnSc6	univerzita
a	a	k8xC	a
i	i	k9	i
přes	přes	k7c4	přes
diagnostikovanou	diagnostikovaný	k2eAgFnSc4d1	diagnostikovaná
chudokrevnost	chudokrevnost	k1gFnSc4	chudokrevnost
měl	mít	k5eAaImAgInS	mít
rozpracováno	rozpracovat	k5eAaPmNgNnS	rozpracovat
několik	několik	k4yIc1	několik
projektů	projekt	k1gInPc2	projekt
najednou	najednou	k6eAd1	najednou
a	a	k8xC	a
věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
řadě	řada	k1gFnSc3	řada
dalších	další	k2eAgFnPc2d1	další
okrajových	okrajový	k2eAgFnPc2d1	okrajová
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
projektů	projekt	k1gInPc2	projekt
byla	být	k5eAaImAgFnS	být
i	i	k9	i
práce	práce	k1gFnSc1	práce
na	na	k7c6	na
knize	kniha	k1gFnSc6	kniha
Láska	láska	k1gFnSc1	láska
a	a	k8xC	a
odpovědnost	odpovědnost	k1gFnSc1	odpovědnost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vydal	vydat	k5eAaPmAgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
a	a	k8xC	a
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
problémy	problém	k1gInPc7	problém
manželského	manželský	k2eAgInSc2d1	manželský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1962	[number]	k4	1962
zemřel	zemřít	k5eAaPmAgMnS	zemřít
krakovský	krakovský	k2eAgMnSc1d1	krakovský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Eugenius	Eugenius	k1gMnSc1	Eugenius
Baziak	Baziak	k1gMnSc1	Baziak
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
nominaci	nominace	k1gFnSc3	nominace
<g/>
,	,	kIx,	,
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
i	i	k9	i
k	k	k7c3	k
volbě	volba	k1gFnSc3	volba
pomocného	pomocný	k2eAgMnSc2d1	pomocný
biskupa	biskup	k1gMnSc2	biskup
Karola	Karola	k1gFnSc1	Karola
Wojtyły	Wojtyły	k1gInPc1	Wojtyły
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
kapitulního	kapitulní	k2eAgMnSc2d1	kapitulní
vikáře	vikář	k1gMnSc2	vikář
krakovské	krakovský	k2eAgFnSc2d1	Krakovská
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
na	na	k7c4	na
první	první	k4xOgNnSc4	první
zasedání	zasedání	k1gNnSc4	zasedání
Druhého	druhý	k4xOgInSc2	druhý
vatikánského	vatikánský	k2eAgInSc2d1	vatikánský
koncilu	koncil	k1gInSc2	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
tímto	tento	k3xDgInSc7	tento
podzimem	podzim	k1gInSc7	podzim
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
účastnil	účastnit	k5eAaImAgInS	účastnit
všech	všecek	k3xTgInPc2	všecek
čtyř	čtyři	k4xCgNnPc2	čtyři
zasedání	zasedání	k1gNnPc2	zasedání
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgInPc6	jenž
živě	živě	k6eAd1	živě
vstupoval	vstupovat	k5eAaImAgMnS	vstupovat
do	do	k7c2	do
diskusí	diskuse	k1gFnPc2	diskuse
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c6	na
důležitých	důležitý	k2eAgNnPc6d1	důležité
rozhodnutích	rozhodnutí	k1gNnPc6	rozhodnutí
začleněných	začleněný	k2eAgNnPc6d1	začleněné
v	v	k7c6	v
dokumentech	dokument	k1gInPc6	dokument
Dignitatis	Dignitatis	k1gFnSc1	Dignitatis
humanae	humanae	k1gInSc1	humanae
a	a	k8xC	a
Gaudium	gaudium	k1gNnSc1	gaudium
et	et	k?	et
Spes	Spesa	k1gFnPc2	Spesa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
přijížděl	přijíždět	k5eAaImAgMnS	přijíždět
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
jako	jako	k8xS	jako
neznámý	známý	k2eNgMnSc1d1	neznámý
biskup	biskup	k1gMnSc1	biskup
<g/>
,	,	kIx,	,
po	po	k7c6	po
zakončení	zakončení	k1gNnSc6	zakončení
koncilu	koncil	k1gInSc2	koncil
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
otevřeně	otevřeně	k6eAd1	otevřeně
interpretovaným	interpretovaný	k2eAgInPc3d1	interpretovaný
názorům	názor	k1gInPc3	názor
a	a	k8xC	a
kouzlu	kouzlo	k1gNnSc3	kouzlo
osobnosti	osobnost	k1gFnSc2	osobnost
stal	stát	k5eAaPmAgInS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
postav	postava	k1gFnPc2	postava
církve	církev	k1gFnSc2	církev
jak	jak	k8xC	jak
mezi	mezi	k7c7	mezi
svými	svůj	k3xOyFgMnPc7	svůj
kolegy	kolega	k1gMnPc7	kolega
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
mezi	mezi	k7c7	mezi
novináři	novinář	k1gMnPc7	novinář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
koncilu	koncil	k1gInSc2	koncil
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
papež	papež	k1gMnSc1	papež
Pavel	Pavel	k1gMnSc1	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
kapitulního	kapitulní	k2eAgMnSc2d1	kapitulní
vikáře	vikář	k1gMnSc2	vikář
Karola	Karola	k1gFnSc1	Karola
Wojtyłu	Wojtyła	k1gFnSc4	Wojtyła
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
krakovským	krakovský	k2eAgMnSc7d1	krakovský
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
<g/>
.	.	kIx.	.
</s>
<s>
Veřejně	veřejně	k6eAd1	veřejně
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
o	o	k7c4	o
necelé	celý	k2eNgInPc4d1	necelý
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Wawelské	Wawelský	k2eAgFnSc6d1	Wawelská
katedrále	katedrála	k1gFnSc6	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
polská	polský	k2eAgFnSc1d1	polská
komunistická	komunistický	k2eAgFnSc1d1	komunistická
vláda	vláda	k1gFnSc1	vláda
naznačila	naznačit	k5eAaPmAgFnS	naznačit
vatikánským	vatikánský	k2eAgMnPc3d1	vatikánský
představitelům	představitel	k1gMnPc3	představitel
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
nebyla	být	k5eNaImAgFnS	být
proti	proti	k7c3	proti
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nP	kdyby
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
působil	působit	k5eAaImAgMnS	působit
druhý	druhý	k4xOgMnSc1	druhý
kardinál	kardinál	k1gMnSc1	kardinál
<g/>
.	.	kIx.	.
</s>
<s>
Úmyslem	úmysl	k1gInSc7	úmysl
vládních	vládní	k2eAgMnPc2d1	vládní
činitelů	činitel	k1gMnPc2	činitel
patrně	patrně	k6eAd1	patrně
bylo	být	k5eAaImAgNnS	být
rozpoltit	rozpoltit	k5eAaPmF	rozpoltit
polskou	polský	k2eAgFnSc4d1	polská
církev	církev	k1gFnSc4	církev
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
v	v	k7c6	v
čelem	čelo	k1gNnSc7	čelo
s	s	k7c7	s
novým	nový	k2eAgMnSc7d1	nový
kardinálem	kardinál	k1gMnSc7	kardinál
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc4	druhý
s	s	k7c7	s
primasem	primas	k1gMnSc7	primas
Wyszyńskim	Wyszyńskima	k1gFnPc2	Wyszyńskima
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
tohoto	tento	k3xDgInSc2	tento
požadavku	požadavek	k1gInSc2	požadavek
tedy	tedy	k9	tedy
papež	papež	k1gMnSc1	papež
Pavel	Pavel	k1gMnSc1	Pavel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1967	[number]	k4	1967
oznámil	oznámit	k5eAaPmAgMnS	oznámit
kardinálskému	kardinálský	k2eAgNnSc3d1	kardinálské
kolegiu	kolegium	k1gNnSc3	kolegium
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
kardinála	kardinál	k1gMnSc2	kardinál
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
arcibiskupa	arcibiskup	k1gMnSc4	arcibiskup
Karola	Karola	k1gFnSc1	Karola
Wojtyłu	Wojtyłus	k1gInSc2	Wojtyłus
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
tohoto	tento	k3xDgNnSc2	tento
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
byla	být	k5eAaImAgFnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
arcibiskupova	arcibiskupův	k2eAgFnSc1d1	arcibiskupova
intenzivní	intenzivní	k2eAgFnSc1d1	intenzivní
pastorační	pastorační	k2eAgFnSc1d1	pastorační
činnost	činnost	k1gFnSc1	činnost
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokázal	dokázat	k5eAaPmAgMnS	dokázat
srozumitelně	srozumitelně	k6eAd1	srozumitelně
<g/>
,	,	kIx,	,
otevřeně	otevřeně	k6eAd1	otevřeně
a	a	k8xC	a
neobyčejně	obyčejně	k6eNd1	obyčejně
adresně	adresně	k6eAd1	adresně
šířit	šířit	k5eAaImF	šířit
evangelickou	evangelický	k2eAgFnSc4d1	evangelická
zvěst	zvěst	k1gFnSc4	zvěst
<g/>
.	.	kIx.	.
</s>
<s>
Kardinálský	kardinálský	k2eAgInSc1d1	kardinálský
klobouk	klobouk	k1gInSc1	klobouk
od	od	k7c2	od
papeže	papež	k1gMnSc2	papež
obdržel	obdržet	k5eAaPmAgMnS	obdržet
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
při	při	k7c6	při
obřadu	obřad	k1gInSc6	obřad
prováděném	prováděný	k2eAgInSc6d1	prováděný
v	v	k7c6	v
Sixtinské	sixtinský	k2eAgFnSc6d1	Sixtinská
kapli	kaple	k1gFnSc6	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
člen	člen	k1gMnSc1	člen
duchovenstva	duchovenstvo	k1gNnSc2	duchovenstvo
římské	římský	k2eAgFnSc2d1	římská
diecéze	diecéze	k1gFnSc2	diecéze
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
úřadu	úřad	k1gInSc3	úřad
získal	získat	k5eAaPmAgMnS	získat
titulární	titulární	k2eAgNnSc4d1	titulární
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c6	v
kostelíku	kostelík	k1gInSc6	kostelík
sv.	sv.	kA	sv.
Cesara	Cesar	k1gMnSc2	Cesar
in	in	k?	in
Palatio	Palatio	k1gNnSc1	Palatio
na	na	k7c4	na
Via	via	k7c4	via
Porta	porta	k1gFnSc1	porta
Latina	latina	k1gFnSc1	latina
nedaleko	nedaleko	k7c2	nedaleko
Caracallových	Caracallův	k2eAgFnPc2d1	Caracallova
lázní	lázeň	k1gFnPc2	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1967	[number]	k4	1967
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
Vatikánu	Vatikán	k1gInSc2	Vatikán
papežem	papež	k1gMnSc7	papež
Pavlem	Pavel	k1gMnSc7	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
svolána	svolán	k2eAgFnSc1d1	svolána
synoda	synoda	k1gFnSc1	synoda
biskupů	biskup	k1gMnPc2	biskup
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
dát	dát	k5eAaPmF	dát
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
podobu	podoba	k1gFnSc4	podoba
Dogmatické	dogmatický	k2eAgFnSc3d1	dogmatická
konstituci	konstituce	k1gFnSc3	konstituce
o	o	k7c6	o
církvi	církev	k1gFnSc6	církev
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
vypracovaná	vypracovaný	k2eAgFnSc1d1	vypracovaná
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
vatikánském	vatikánský	k2eAgInSc6d1	vatikánský
koncilu	koncil	k1gInSc6	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
byl	být	k5eAaImAgMnS	být
kardinál	kardinál	k1gMnSc1	kardinál
Wojtyła	Wojtyła	k1gMnSc1	Wojtyła
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
zasedání	zasedání	k1gNnSc2	zasedání
vyslán	vyslat	k5eAaPmNgInS	vyslat
polským	polský	k2eAgInSc7d1	polský
episkopátem	episkopát	k1gInSc7	episkopát
<g/>
,	,	kIx,	,
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
neodcestoval	odcestovat	k5eNaPmAgMnS	odcestovat
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
solidární	solidární	k2eAgNnSc4d1	solidární
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
odůvodnil	odůvodnit	k5eAaPmAgMnS	odůvodnit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
synodu	synod	k1gInSc6	synod
může	moct	k5eAaImIp3nS	moct
odjet	odjet	k5eAaPmF	odjet
pouze	pouze	k6eAd1	pouze
společně	společně	k6eAd1	společně
s	s	k7c7	s
kardinálem	kardinál	k1gMnSc7	kardinál
Wyszyńskim	Wyszyńskim	k1gInSc4	Wyszyńskim
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
komunistické	komunistický	k2eAgInPc1d1	komunistický
úřady	úřad	k1gInPc1	úřad
odmítly	odmítnout	k5eAaPmAgInP	odmítnout
vydat	vydat	k5eAaPmF	vydat
povolení	povolení	k1gNnPc4	povolení
k	k	k7c3	k
výjezdu	výjezd	k1gInSc3	výjezd
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
a	a	k8xC	a
září	zářit	k5eAaImIp3nS	zářit
1969	[number]	k4	1969
poprvé	poprvé	k6eAd1	poprvé
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Kanadu	Kanada	k1gFnSc4	Kanada
a	a	k8xC	a
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
především	především	k9	především
navazoval	navazovat	k5eAaImAgMnS	navazovat
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
polskými	polský	k2eAgInPc7d1	polský
krajanskými	krajanský	k2eAgInPc7d1	krajanský
spolky	spolek	k1gInPc7	spolek
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
pak	pak	k6eAd1	pak
z	z	k7c2	z
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
odletěl	odletět	k5eAaPmAgMnS	odletět
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
již	již	k9	již
společně	společně	k6eAd1	společně
s	s	k7c7	s
kardinálem	kardinál	k1gMnSc7	kardinál
Wyszyńskim	Wyszyńskima	k1gFnPc2	Wyszyńskima
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
"	"	kIx"	"
<g/>
výjimečného	výjimečný	k2eAgNnSc2d1	výjimečné
<g/>
"	"	kIx"	"
setkání	setkání	k1gNnSc2	setkání
Biskupské	biskupský	k2eAgFnSc2d1	biskupská
synody	synoda	k1gFnSc2	synoda
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecnou	všeobecný	k2eAgFnSc4d1	všeobecná
pozornost	pozornost	k1gFnSc4	pozornost
zde	zde	k6eAd1	zde
vzbudil	vzbudit	k5eAaPmAgMnS	vzbudit
svým	svůj	k3xOyFgInSc7	svůj
projevem	projev	k1gInSc7	projev
o	o	k7c6	o
křesťanských	křesťanský	k2eAgFnPc6d1	křesťanská
komunitách	komunita	k1gFnPc6	komunita
a	a	k8xC	a
skupinách	skupina	k1gFnPc6	skupina
a	a	k8xC	a
potřebě	potřeba	k1gFnSc6	potřeba
"	"	kIx"	"
<g/>
kolegiálních	kolegiální	k2eAgFnPc2d1	kolegiální
struktur	struktura	k1gFnPc2	struktura
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
čerpal	čerpat	k5eAaImAgInS	čerpat
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
zkušeností	zkušenost	k1gFnPc2	zkušenost
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
fázi	fáze	k1gFnSc6	fáze
tohoto	tento	k3xDgNnSc2	tento
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
,	,	kIx,	,
kterého	který	k3yQgNnSc2	který
se	se	k3xPyFc4	se
účastnily	účastnit	k5eAaImAgFnP	účastnit
hlavy	hlava	k1gFnPc1	hlava
episkopátů	episkopát	k1gInPc2	episkopát
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
požádán	požádat	k5eAaPmNgMnS	požádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
komise	komise	k1gFnSc2	komise
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
vypracovat	vypracovat	k5eAaPmF	vypracovat
závěrečnou	závěrečný	k2eAgFnSc4d1	závěrečná
deklaraci	deklarace	k1gFnSc4	deklarace
shrnující	shrnující	k2eAgFnSc4d1	shrnující
celou	celý	k2eAgFnSc4d1	celá
diskusi	diskuse	k1gFnSc4	diskuse
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1971	[number]	k4	1971
by	by	kYmCp3nS	by
kardinál	kardinál	k1gMnSc1	kardinál
Wojtyła	Wojtyła	k1gMnSc1	Wojtyła
ve	v	k7c6	v
Vatikánu	Vatikán	k1gInSc6	Vatikán
přítomen	přítomen	k2eAgMnSc1d1	přítomen
zahájení	zahájení	k1gNnSc3	zahájení
druhé	druhý	k4xOgFnSc2	druhý
biskupské	biskupský	k2eAgFnSc2d1	biskupská
synody	synoda	k1gFnSc2	synoda
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
průběhu	průběh	k1gInSc6	průběh
byl	být	k5eAaImAgInS	být
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
zvolen	zvolit	k5eAaPmNgInS	zvolit
za	za	k7c2	za
člena	člen	k1gMnSc2	člen
Rady	rada	k1gFnSc2	rada
generálního	generální	k2eAgInSc2d1	generální
sekretariátu	sekretariát	k1gInSc2	sekretariát
(	(	kIx(	(
<g/>
115	[number]	k4	115
hlasy	hlas	k1gInPc7	hlas
ze	z	k7c2	z
184	[number]	k4	184
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
třetím	třetí	k4xOgMnSc7	třetí
nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
členem	člen	k1gMnSc7	člen
stálého	stálý	k2eAgInSc2d1	stálý
synodního	synodní	k2eAgInSc2d1	synodní
sekretariátu	sekretariát	k1gInSc2	sekretariát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
další	další	k2eAgNnPc4d1	další
zasedání	zasedání	k1gNnPc4	zasedání
dorazil	dorazit	k5eAaPmAgInS	dorazit
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
a	a	k8xC	a
jednoznačně	jednoznačně	k6eAd1	jednoznačně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
v	v	k7c6	v
konfrontaci	konfrontace	k1gFnSc6	konfrontace
s	s	k7c7	s
modernistickými	modernistický	k2eAgInPc7d1	modernistický
návrhy	návrh	k1gInPc7	návrh
holandských	holandský	k2eAgInPc2d1	holandský
biskupů	biskup	k1gInPc2	biskup
postavil	postavit	k5eAaPmAgInS	postavit
za	za	k7c4	za
zachování	zachování	k1gNnSc4	zachování
kněžského	kněžský	k2eAgInSc2d1	kněžský
celibátu	celibát	k1gInSc2	celibát
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1972	[number]	k4	1972
byla	být	k5eAaImAgFnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
zahájena	zahájit	k5eAaPmNgFnS	zahájit
Krakovská	krakovský	k2eAgFnSc1d1	Krakovská
synoda	synoda	k1gFnSc1	synoda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
realizace	realizace	k1gFnSc1	realizace
se	se	k3xPyFc4	se
v	v	k7c6	v
hlavě	hlava	k1gFnSc6	hlava
kardinála	kardinál	k1gMnSc2	kardinál
Wojtyły	Wojtyła	k1gMnSc2	Wojtyła
zrodila	zrodit	k5eAaPmAgFnS	zrodit
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
knihy	kniha	k1gFnSc2	kniha
Zdroje	zdroj	k1gInSc2	zdroj
obnovy	obnova	k1gFnSc2	obnova
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1973	[number]	k4	1973
Karol	Karol	k1gInSc1	Karol
Wojtyła	Wojtyłum	k1gNnSc2	Wojtyłum
odcestoval	odcestovat	k5eAaPmAgInS	odcestovat
přes	přes	k7c4	přes
filipínskou	filipínský	k2eAgFnSc4d1	filipínská
Manilu	Manila	k1gFnSc4	Manila
do	do	k7c2	do
australského	australský	k2eAgNnSc2d1	Australské
Melbourne	Melbourne	k1gNnSc2	Melbourne
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
polské	polský	k2eAgMnPc4d1	polský
katolíky	katolík	k1gMnPc4	katolík
na	na	k7c6	na
Mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
eucharistickém	eucharistický	k2eAgInSc6d1	eucharistický
kongresu	kongres	k1gInSc6	kongres
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
Františkem	František	k1gMnSc7	František
Tomáškem	Tomášek	k1gMnSc7	Tomášek
<g/>
,	,	kIx,	,
vídeňským	vídeňský	k2eAgMnSc7d1	vídeňský
kardinálem	kardinál	k1gMnSc7	kardinál
Franzem	Franz	k1gMnSc7	Franz
Königem	König	k1gMnSc7	König
a	a	k8xC	a
berlínským	berlínský	k2eAgMnSc7d1	berlínský
kardinálem	kardinál	k1gMnSc7	kardinál
Alfredem	Alfred	k1gMnSc7	Alfred
Bengschem	Bengsch	k1gMnSc7	Bengsch
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
litoměřického	litoměřický	k2eAgInSc2d1	litoměřický
pohřbu	pohřeb	k1gInSc2	pohřeb
tragicky	tragicky	k6eAd1	tragicky
zesnulého	zesnulý	k2eAgMnSc2d1	zesnulý
kardinála	kardinál	k1gMnSc2	kardinál
Štěpána	Štěpán	k1gMnSc2	Štěpán
Trochty	Trochty	k?	Trochty
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
zákaz	zákaz	k1gInSc4	zákaz
komunistické	komunistický	k2eAgFnSc2d1	komunistická
státní	státní	k2eAgFnSc2d1	státní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
na	na	k7c6	na
pohřbu	pohřeb	k1gInSc6	pohřeb
také	také	k9	také
veřejně	veřejně	k6eAd1	veřejně
promluvil	promluvit	k5eAaPmAgMnS	promluvit
Z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
posléze	posléze	k6eAd1	posléze
přes	přes	k7c4	přes
Vídeň	Vídeň	k1gFnSc4	Vídeň
opět	opět	k6eAd1	opět
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
přítomen	přítomen	k2eAgMnSc1d1	přítomen
u	u	k7c2	u
řady	řada	k1gFnSc2	řada
slavností	slavnost	k1gFnPc2	slavnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
700	[number]	k4	700
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
úmrtí	úmrtí	k1gNnSc2	úmrtí
sv.	sv.	kA	sv.
Tomáše	Tomáš	k1gMnSc4	Tomáš
Akvinského	Akvinský	k2eAgMnSc4d1	Akvinský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1976	[number]	k4	1976
papež	papež	k1gMnSc1	papež
Pavel	Pavel	k1gMnSc1	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
projevil	projevit	k5eAaPmAgMnS	projevit
Karolu	Karola	k1gFnSc4	Karola
Wojtyłovi	Wojtyła	k1gMnSc3	Wojtyła
čest	čest	k1gFnSc4	čest
<g/>
,	,	kIx,	,
když	když	k8xS	když
jej	on	k3xPp3gMnSc4	on
pověřil	pověřit	k5eAaPmAgMnS	pověřit
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
prvního	první	k4xOgInSc2	první
postního	postní	k2eAgInSc2d1	postní
týdne	týden	k1gInSc2	týden
kázat	kázat	k5eAaImF	kázat
členům	člen	k1gMnPc3	člen
římské	římský	k2eAgFnPc4d1	římská
kurie	kurie	k1gFnPc4	kurie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
přípravu	příprava	k1gFnSc4	příprava
těchto	tento	k3xDgNnPc2	tento
tradičně	tradičně	k6eAd1	tradičně
konaných	konaný	k2eAgNnPc2d1	konané
dvaadvaceti	dvaadvacet	k4xCc2	dvaadvacet
cvičení	cvičení	k1gNnPc2	cvičení
duchovní	duchovní	k2eAgFnSc2d1	duchovní
obnovy	obnova	k1gFnSc2	obnova
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgInPc2	jenž
hovořil	hovořit	k5eAaImAgMnS	hovořit
italsky	italsky	k6eAd1	italsky
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
i	i	k9	i
přes	přes	k7c4	přes
tento	tento	k3xDgInSc4	tento
krátký	krátký	k2eAgInSc4d1	krátký
termín	termín	k1gInSc4	termín
se	se	k3xPyFc4	se
před	před	k7c7	před
vysoce	vysoce	k6eAd1	vysoce
postavenými	postavený	k2eAgInPc7d1	postavený
preláty	prelát	k1gInPc7	prelát
projevil	projevit	k5eAaPmAgMnS	projevit
jako	jako	k9	jako
schopný	schopný	k2eAgMnSc1d1	schopný
teolog	teolog	k1gMnSc1	teolog
a	a	k8xC	a
rétorik	rétorik	k1gMnSc1	rétorik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
opět	opět	k6eAd1	opět
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
svého	svůj	k3xOyFgInSc2	svůj
šestitýdenního	šestitýdenní	k2eAgInSc2d1	šestitýdenní
programu	program	k1gInSc2	program
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Harvardovu	Harvardův	k2eAgFnSc4d1	Harvardova
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
vedl	vést	k5eAaImAgMnS	vést
přednášku	přednáška	k1gFnSc4	přednáška
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
Účast	účast	k1gFnSc1	účast
nebo	nebo	k8xC	nebo
odcizení	odcizení	k1gNnSc1	odcizení
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Bostonu	Boston	k1gInSc2	Boston
posléze	posléze	k6eAd1	posléze
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
zamířil	zamířit	k5eAaPmAgMnS	zamířit
do	do	k7c2	do
Washingtonu	Washington	k1gInSc2	Washington
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přednášel	přednášet	k5eAaImAgMnS	přednášet
na	na	k7c6	na
Katolické	katolický	k2eAgFnSc6d1	katolická
univerzitě	univerzita	k1gFnSc6	univerzita
americké	americký	k2eAgFnSc2d1	americká
školy	škola	k1gFnSc2	škola
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
jeho	jeho	k3xOp3gFnSc1	jeho
cesta	cesta	k1gFnSc1	cesta
vedla	vést	k5eAaImAgFnS	vést
do	do	k7c2	do
Filadelfie	Filadelfie	k1gFnSc2	Filadelfie
na	na	k7c4	na
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
eucharistický	eucharistický	k2eAgInSc4d1	eucharistický
kongres	kongres	k1gInSc4	kongres
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
vybrán	vybrat	k5eAaPmNgMnS	vybrat
<g/>
,	,	kIx,	,
aby	aby	k9	aby
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
vedl	vést	k5eAaImAgMnS	vést
mši	mše	k1gFnSc4	mše
<g/>
,	,	kIx,	,
podtitulem	podtitul	k1gInSc7	podtitul
jejíž	jejíž	k3xOyRp3gFnSc2	jejíž
homilie	homilie	k1gFnSc2	homilie
bylo	být	k5eAaImAgNnS	být
téma	téma	k1gNnSc4	téma
"	"	kIx"	"
<g/>
eucharistie	eucharistie	k1gFnPc4	eucharistie
a	a	k8xC	a
potřeby	potřeba	k1gFnPc4	potřeba
lidské	lidský	k2eAgFnSc2d1	lidská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Pavla	Pavel	k1gMnSc2	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
kardinál	kardinál	k1gMnSc1	kardinál
<g />
.	.	kIx.	.
</s>
<s>
Karol	Karol	k1gInSc1	Karol
Wojtyła	Wojtyła	k1gFnSc1	Wojtyła
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1978	[number]	k4	1978
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
konkláve	konkláve	k1gNnSc4	konkláve
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
již	již	k6eAd1	již
prvního	první	k4xOgInSc2	první
dne	den	k1gInSc2	den
konání	konání	k1gNnSc2	konání
zvolilo	zvolit	k5eAaPmAgNnS	zvolit
novým	nový	k2eAgMnSc7d1	nový
papežem	papež	k1gMnSc7	papež
benátského	benátský	k2eAgNnSc2d1	benátské
patriarchu	patriarcha	k1gMnSc4	patriarcha
Albina	Albin	k2eAgMnSc2d1	Albin
Lucianiho	Luciani	k1gMnSc2	Luciani
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
pontifikát	pontifikát	k1gInSc4	pontifikát
přijal	přijmout	k5eAaPmAgMnS	přijmout
jméno	jméno	k1gNnSc4	jméno
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
I.	I.	kA	I.
O	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
společně	společně	k6eAd1	společně
s	s	k7c7	s
primasem	primas	k1gMnSc7	primas
Wyszyńskim	Wyszyńskima	k1gFnPc2	Wyszyńskima
navštívil	navštívit	k5eAaPmAgMnS	navštívit
<g />
.	.	kIx.	.
</s>
<s>
NSR	NSR	kA	NSR
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
za	za	k7c4	za
jeho	jeho	k3xOp3gFnPc4	jeho
výrazné	výrazný	k2eAgFnPc4d1	výrazná
snahy	snaha	k1gFnPc4	snaha
a	a	k8xC	a
přispění	přispění	k1gNnSc4	přispění
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
oficiálnímu	oficiální	k2eAgNnSc3d1	oficiální
usmíření	usmíření	k1gNnSc3	usmíření
polského	polský	k2eAgInSc2d1	polský
a	a	k8xC	a
západoněmeckého	západoněmecký	k2eAgInSc2d1	západoněmecký
episkopátu	episkopát	k1gInSc2	episkopát
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
nečekaně	nečekaně	k6eAd1	nečekaně
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
I.	I.	kA	I.
a	a	k8xC	a
Karol	Karol	k1gInSc1	Karol
Wojtyła	Wojtył	k1gInSc2	Wojtył
v	v	k7c6	v
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Vatikánu	Vatikán	k1gInSc2	Vatikán
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
konkláve	konkláve	k1gNnSc3	konkláve
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
ovšem	ovšem	k9	ovšem
jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
předních	přední	k2eAgMnPc2d1	přední
kandidátů	kandidát	k1gMnPc2	kandidát
na	na	k7c4	na
papežský	papežský	k2eAgInSc4d1	papežský
úřad	úřad	k1gInSc4	úřad
(	(	kIx(	(
<g/>
papabile	papabile	k6eAd1	papabile
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
podpora	podpora	k1gFnSc1	podpora
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
mu	on	k3xPp3gMnSc3	on
již	již	k6eAd1	již
při	při	k7c6	při
předchozí	předchozí	k2eAgFnSc6d1	předchozí
papežské	papežský	k2eAgFnSc6d1	Papežská
volbě	volba	k1gFnSc6	volba
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
rakouský	rakouský	k2eAgMnSc1d1	rakouský
primas	primas	k1gMnSc1	primas
kardinál	kardinál	k1gMnSc1	kardinál
Franz	Franz	k1gMnSc1	Franz
König	König	k1gMnSc1	König
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
preláti	prelát	k1gMnPc1	prelát
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
chtěli	chtít	k5eAaImAgMnP	chtít
vyhnout	vyhnout	k5eAaPmF	vyhnout
volebnímu	volební	k2eAgNnSc3d1	volební
zápolení	zápolení	k1gNnSc3	zápolení
mezi	mezi	k7c7	mezi
italskými	italský	k2eAgMnPc7d1	italský
kandidáty	kandidát	k1gMnPc7	kandidát
nebo	nebo	k8xC	nebo
mezi	mezi	k7c7	mezi
frakcemi	frakce	k1gFnPc7	frakce
<g/>
,	,	kIx,	,
zastávajícími	zastávající	k2eAgInPc7d1	zastávající
pokrokovou	pokrokový	k2eAgFnSc4d1	pokroková
či	či	k8xC	či
konzervativní	konzervativní	k2eAgFnSc4d1	konzervativní
církevní	církevní	k2eAgFnSc4d1	církevní
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
Wojtyłův	Wojtyłův	k2eAgInSc4d1	Wojtyłův
prospěch	prospěch	k1gInSc4	prospěch
hovořila	hovořit	k5eAaImAgFnS	hovořit
i	i	k9	i
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
otevřeně	otevřeně	k6eAd1	otevřeně
nestranil	stranit	k5eNaImAgMnS	stranit
žádné	žádný	k3yNgFnPc4	žádný
z	z	k7c2	z
ideových	ideový	k2eAgFnPc2d1	ideová
klik	klika	k1gFnPc2	klika
a	a	k8xC	a
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
scéně	scéna	k1gFnSc6	scéna
i	i	k9	i
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
kurii	kurie	k1gFnSc4	kurie
byl	být	k5eAaImAgInS	být
známou	známý	k2eAgFnSc7d1	známá
osobností	osobnost	k1gFnSc7	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
volbě	volba	k1gFnSc6	volba
tradičně	tradičně	k6eAd1	tradičně
konané	konaný	k2eAgFnPc4d1	konaná
v	v	k7c6	v
Apoštolském	apoštolský	k2eAgInSc6d1	apoštolský
paláci	palác	k1gInSc6	palác
obýval	obývat	k5eAaImAgMnS	obývat
celu	cela	k1gFnSc4	cela
č.	č.	k?	č.
91	[number]	k4	91
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prvním	první	k4xOgInSc6	první
volebním	volební	k2eAgInSc6d1	volební
dnu	den	k1gInSc6	den
konkláve	konkláve	k1gNnSc2	konkláve
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
bylo	být	k5eAaImAgNnS	být
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
výsledek	výsledek	k1gInSc1	výsledek
hlasování	hlasování	k1gNnSc2	hlasování
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
rozdělen	rozdělen	k2eAgInSc1d1	rozdělen
mezi	mezi	k7c4	mezi
dva	dva	k4xCgMnPc4	dva
hlavní	hlavní	k2eAgMnPc4d1	hlavní
italské	italský	k2eAgMnPc4d1	italský
kandidáty	kandidát	k1gMnPc4	kandidát
janovského	janovský	k2eAgInSc2d1	janovský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
Giuseppe	Giusepp	k1gInSc5	Giusepp
Siriho	Siri	k1gMnSc4	Siri
a	a	k8xC	a
florentského	florentský	k2eAgMnSc4d1	florentský
arcibiskupa	arcibiskup	k1gMnSc4	arcibiskup
Giovanni	Giovaneň	k1gFnSc3	Giovaneň
Benelliho	Benelli	k1gMnSc2	Benelli
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
nebyli	být	k5eNaImAgMnP	být
členové	člen	k1gMnPc1	člen
konkláve	konkláve	k1gNnSc2	konkláve
schopni	schopen	k2eAgMnPc1d1	schopen
nalézt	nalézt	k5eAaPmF	nalézt
vzájemný	vzájemný	k2eAgInSc4d1	vzájemný
kompromis	kompromis	k1gInSc4	kompromis
ani	ani	k8xC	ani
adekvátní	adekvátní	k2eAgFnSc4d1	adekvátní
alternativu	alternativa	k1gFnSc4	alternativa
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
italských	italský	k2eAgMnPc2d1	italský
prelátů	prelát	k1gMnPc2	prelát
<g/>
,	,	kIx,	,
po	po	k7c6	po
návrhu	návrh	k1gInSc6	návrh
kardinála	kardinál	k1gMnSc2	kardinál
Franze	Franze	k1gFnSc2	Franze
Königa	König	k1gMnSc2	König
zvolilo	zvolit	k5eAaPmAgNnS	zvolit
99	[number]	k4	99
ze	z	k7c2	z
111	[number]	k4	111
hlasujících	hlasující	k2eAgInPc2d1	hlasující
novým	nový	k2eAgInSc7d1	nový
papežem	papež	k1gMnSc7	papež
Karola	Karola	k1gFnSc1	Karola
Wojtyłu	Wojtyłus	k1gInSc2	Wojtyłus
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
po	po	k7c6	po
osmém	osmý	k4xOgInSc6	osmý
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
výsledek	výsledek	k1gInSc1	výsledek
byl	být	k5eAaImAgInS	být
věřícím	věřící	k2eAgInSc7d1	věřící
shromážděným	shromážděný	k2eAgInSc7d1	shromážděný
na	na	k7c6	na
Svatopetrském	svatopetrský	k2eAgNnSc6d1	Svatopetrské
náměstí	náměstí	k1gNnSc6	náměstí
oznámen	oznámit	k5eAaPmNgInS	oznámit
v	v	k7c6	v
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
43	[number]	k4	43
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tradičním	tradiční	k2eAgInSc6d1	tradiční
obřadu	obřad	k1gInSc6	obřad
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
průběhu	průběh	k1gInSc6	průběh
nový	nový	k2eAgMnSc1d1	nový
papež	papež	k1gMnSc1	papež
akceptoval	akceptovat	k5eAaBmAgMnS	akceptovat
své	svůj	k3xOyFgNnSc4	svůj
zvolení	zvolení	k1gNnSc4	zvolení
<g/>
,	,	kIx,	,
Karol	Karol	k1gInSc1	Karol
Wojtyła	Wojtyłum	k1gNnSc2	Wojtyłum
přijal	přijmout	k5eAaPmAgInS	přijmout
na	na	k7c4	na
důkaz	důkaz	k1gInSc4	důkaz
oddanosti	oddanost	k1gFnSc2	oddanost
k	k	k7c3	k
Pavlu	Pavel	k1gMnSc3	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
jako	jako	k9	jako
projev	projev	k1gInSc4	projev
náklonnosti	náklonnost	k1gFnSc2	náklonnost
k	k	k7c3	k
Janu	Jan	k1gMnSc3	Jan
Pavlu	Pavel	k1gMnSc3	Pavel
I.	I.	kA	I.
nové	nový	k2eAgNnSc1d1	nové
jméno	jméno	k1gNnSc1	jméno
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
při	při	k7c6	při
mši	mše	k1gFnSc6	mše
koncelebrované	koncelebrovaný	k2eAgFnSc2d1	koncelebrovaná
v	v	k7c6	v
konkláve	konkláve	k1gNnSc6	konkláve
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
úkol	úkol	k1gInSc4	úkol
dokončit	dokončit	k5eAaPmF	dokončit
realizaci	realizace	k1gFnSc4	realizace
Druhého	druhý	k4xOgInSc2	druhý
vatikánského	vatikánský	k2eAgInSc2d1	vatikánský
koncilu	koncil	k1gInSc2	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgFnSc1d1	slavnostní
intronizace	intronizace	k1gFnSc1	intronizace
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
na	na	k7c4	na
papežský	papežský	k2eAgInSc4d1	papežský
stolec	stolec	k1gInSc4	stolec
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
a	a	k8xC	a
účastnilo	účastnit	k5eAaImAgNnS	účastnit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
na	na	k7c4	na
300	[number]	k4	300
000	[number]	k4	000
přihlížejících	přihlížející	k2eAgInPc2d1	přihlížející
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
dnech	den	k1gInPc6	den
starého	starý	k2eAgMnSc2d1	starý
a	a	k8xC	a
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
dnech	den	k1gInPc6	den
nového	nový	k2eAgInSc2d1	nový
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
se	se	k3xPyFc4	se
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
stal	stát	k5eAaPmAgMnS	stát
prostředníkem	prostředník	k1gMnSc7	prostředník
mezi	mezi	k7c7	mezi
Chile	Chile	k1gNnSc7	Chile
a	a	k8xC	a
Argentinou	Argentina	k1gFnSc7	Argentina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vyhroceném	vyhrocený	k2eAgInSc6d1	vyhrocený
sporu	spor	k1gInSc6	spor
o	o	k7c6	o
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
průlivu	průliv	k1gInSc6	průliv
Beagle	Beagle	k1gFnSc2	Beagle
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
necelý	celý	k2eNgInSc4d1	necelý
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
)	)	kIx)	)
odletěl	odletět	k5eAaPmAgMnS	odletět
přes	přes	k7c4	přes
Dominikánskou	dominikánský	k2eAgFnSc4d1	Dominikánská
republiku	republika	k1gFnSc4	republika
na	na	k7c4	na
pětidenní	pětidenní	k2eAgFnSc4d1	pětidenní
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
papežskou	papežský	k2eAgFnSc4d1	Papežská
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
po	po	k7c6	po
neoficiálním	neoficiální	k2eAgInSc6d1	neoficiální
a	a	k8xC	a
chladném	chladný	k2eAgInSc6d1	chladný
přivítání	přivítání	k1gNnSc2	přivítání
prezidentem	prezident	k1gMnSc7	prezident
Portillou	Portilla	k1gMnSc7	Portilla
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
konference	konference	k1gFnSc2	konference
latinskoamerických	latinskoamerický	k2eAgMnPc2d1	latinskoamerický
biskupů	biskup	k1gMnPc2	biskup
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
odeslal	odeslat	k5eAaPmAgMnS	odeslat
dopis	dopis	k1gInSc4	dopis
do	do	k7c2	do
komunistického	komunistický	k2eAgNnSc2d1	komunistické
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
místní	místní	k2eAgMnPc4d1	místní
katolíky	katolík	k1gMnPc4	katolík
podpořit	podpořit	k5eAaPmF	podpořit
v	v	k7c6	v
odporu	odpor	k1gInSc6	odpor
proti	proti	k7c3	proti
tyranii	tyranie	k1gFnSc3	tyranie
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
)	)	kIx)	)
publikoval	publikovat	k5eAaBmAgMnS	publikovat
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
encykliku	encyklika	k1gFnSc4	encyklika
Redemptor	Redemptor	k1gInSc1	Redemptor
hominis	hominis	k1gFnPc1	hominis
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
místem	místo	k1gNnSc7	místo
člověka	člověk	k1gMnSc2	člověk
v	v	k7c6	v
moderním	moderní	k2eAgInSc6d1	moderní
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Zelený	zelený	k2eAgInSc4d1	zelený
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
napsal	napsat	k5eAaPmAgMnS	napsat
dopis	dopis	k1gInSc4	dopis
všem	všecek	k3xTgMnPc3	všecek
katolickým	katolický	k2eAgMnPc3d1	katolický
kněžím	kněz	k1gMnPc3	kněz
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
chtěl	chtít	k5eAaImAgMnS	chtít
znovu	znovu	k6eAd1	znovu
rozdmychat	rozdmychat	k5eAaPmF	rozdmychat
u	u	k7c2	u
svých	svůj	k3xOyFgInPc2	svůj
bratří-kněží	bratříněžit	k5eAaPmIp3nS	bratří-kněžit
pocit	pocit	k1gInSc1	pocit
náboženského	náboženský	k2eAgNnSc2d1	náboženské
dramatu	drama	k1gNnSc2	drama
jejich	jejich	k3xOp3gNnSc2	jejich
povolání	povolání	k1gNnSc2	povolání
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
do	do	k7c2	do
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
pobýval	pobývat	k5eAaImAgMnS	pobývat
při	při	k7c6	při
další	další	k2eAgFnSc6d1	další
apoštolské	apoštolský	k2eAgFnSc6d1	apoštolská
cestě	cesta	k1gFnSc6	cesta
v	v	k7c6	v
rodném	rodný	k2eAgNnSc6d1	rodné
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Krakov	Krakov	k1gInSc4	Krakov
<g/>
,	,	kIx,	,
Nowu	Now	k2eAgFnSc4d1	Nowa
Hutu	Huta	k1gFnSc4	Huta
<g/>
,	,	kIx,	,
Čenstochovou	Čenstochová	k1gFnSc4	Čenstochová
<g/>
,	,	kIx,	,
Jasnou	jasný	k2eAgFnSc4d1	jasná
Góru	Góra	k1gFnSc4	Góra
<g/>
,	,	kIx,	,
Osvětim	Osvětim	k1gFnSc4	Osvětim
a	a	k8xC	a
rodné	rodný	k2eAgInPc4d1	rodný
Wadowice	Wadowice	k1gInPc4	Wadowice
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
cest	cesta	k1gFnPc2	cesta
vedla	vést	k5eAaImAgFnS	vést
do	do	k7c2	do
Irska	Irsko	k1gNnSc2	Irsko
odkud	odkud	k6eAd1	odkud
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
odletěl	odletět	k5eAaPmAgMnS	odletět
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
promluvil	promluvit	k5eAaPmAgMnS	promluvit
o	o	k7c6	o
neodcizitelných	odcizitelný	k2eNgNnPc6d1	odcizitelný
právech	právo	k1gNnPc6	právo
člověka	člověk	k1gMnSc4	člověk
před	před	k7c4	před
34	[number]	k4	34
<g/>
.	.	kIx.	.
generálním	generální	k2eAgNnSc7d1	generální
shromážděním	shromáždění	k1gNnSc7	shromáždění
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
večera	večer	k1gInSc2	večer
pak	pak	k6eAd1	pak
sloužil	sloužit	k5eAaImAgInS	sloužit
mši	mše	k1gFnSc4	mše
pro	pro	k7c4	pro
75	[number]	k4	75
000	[number]	k4	000
věřících	věřící	k1gFnPc2	věřící
na	na	k7c4	na
Yankee	yankee	k1gMnPc4	yankee
Stadionu	stadion	k1gInSc2	stadion
a	a	k8xC	a
následujícího	následující	k2eAgNnSc2d1	následující
dopoledne	dopoledne	k1gNnSc2	dopoledne
se	se	k3xPyFc4	se
na	na	k7c4	na
Madison	Madison	k1gInSc4	Madison
Square	square	k1gInSc1	square
Garden	Gardna	k1gFnPc2	Gardna
setkal	setkat	k5eAaPmAgInS	setkat
s	s	k7c7	s
několika	několik	k4yIc7	několik
desítkami	desítka	k1gFnPc7	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
žáků	žák	k1gMnPc2	žák
místních	místní	k2eAgFnPc2d1	místní
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
mladých	mladý	k2eAgMnPc2d1	mladý
členů	člen	k1gMnPc2	člen
různých	různý	k2eAgFnPc2d1	různá
katolických	katolický	k2eAgFnPc2d1	katolická
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návštěvách	návštěva	k1gFnPc6	návštěva
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
<g/>
,	,	kIx,	,
Iowě	Iowa	k1gFnSc6	Iowa
<g/>
,	,	kIx,	,
Chicagu	Chicago	k1gNnSc6	Chicago
a	a	k8xC	a
Waschingtonu	Waschington	k1gInSc6	Waschington
D.C.	D.C.	k1gFnSc2	D.C.
se	se	k3xPyFc4	se
svatý	svatý	k2eAgMnSc1d1	svatý
otec	otec	k1gMnSc1	otec
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
vrátil	vrátit	k5eAaPmAgMnS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Vatikánu	Vatikán	k1gInSc2	Vatikán
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
svolal	svolat	k5eAaPmAgInS	svolat
inovované	inovovaný	k2eAgNnSc4d1	inovované
"	"	kIx"	"
<g/>
plenární	plenární	k2eAgNnSc4d1	plenární
zasedání	zasedání	k1gNnSc4	zasedání
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterého	který	k3yRgNnSc2	který
se	se	k3xPyFc4	se
účastnilo	účastnit	k5eAaImAgNnS	účastnit
120	[number]	k4	120
kardinálů	kardinál	k1gMnPc2	kardinál
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
posléze	posléze	k6eAd1	posléze
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zahájil	zahájit	k5eAaPmAgMnS	zahájit
teologický	teologický	k2eAgInSc4d1	teologický
dialog	dialog	k1gInSc4	dialog
s	s	k7c7	s
pravoslavnou	pravoslavný	k2eAgFnSc7d1	pravoslavná
církví	církev	k1gFnSc7	církev
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
ekumenickým	ekumenický	k2eAgMnSc7d1	ekumenický
patriarchou	patriarcha	k1gMnSc7	patriarcha
Dimitriem	Dimitrium	k1gNnSc7	Dimitrium
a	a	k8xC	a
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
papež	papež	k1gMnSc1	papež
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
navštívil	navštívit	k5eAaPmAgMnS	navštívit
pravoslavnou	pravoslavný	k2eAgFnSc4d1	pravoslavná
liturgii	liturgie	k1gFnSc4	liturgie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1980	[number]	k4	1980
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
ve	v	k7c6	v
Vatikánu	Vatikán	k1gInSc6	Vatikán
uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Zvláští	Zvláský	k1gMnPc1	Zvláský
synodu	synod	k1gInSc2	synod
pro	pro	k7c4	pro
Holandsko	Holandsko	k1gNnSc4	Holandsko
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
průběhu	průběh	k1gInSc6	průběh
byla	být	k5eAaImAgFnS	být
urovnaná	urovnaný	k2eAgFnSc1d1	urovnaná
krize	krize	k1gFnSc1	krize
v	v	k7c6	v
holandské	holandský	k2eAgFnSc6d1	holandská
církvi	církev	k1gFnSc6	církev
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
přední	přední	k2eAgMnPc1d1	přední
představitelé	představitel	k1gMnPc1	představitel
mj.	mj.	kA	mj.
požadovali	požadovat	k5eAaImAgMnP	požadovat
kněžské	kněžský	k2eAgNnSc4d1	kněžské
svěcení	svěcení	k1gNnSc4	svěcení
žen	žena	k1gFnPc2	žena
či	či	k8xC	či
možnost	možnost	k1gFnSc4	možnost
uzavírání	uzavírání	k1gNnSc2	uzavírání
sňatků	sňatek	k1gInPc2	sňatek
pro	pro	k7c4	pro
kněze	kněz	k1gMnPc4	kněz
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
poutní	poutní	k2eAgFnSc4d1	poutní
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rozvrhu	rozvrh	k1gInSc2	rozvrh
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
do	do	k7c2	do
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Zaire	Zair	k1gInSc5	Zair
<g/>
,	,	kIx,	,
Kongo	Kongo	k1gNnSc4	Kongo
<g/>
,	,	kIx,	,
Keňu	keňu	k1gNnSc4	keňu
<g/>
,	,	kIx,	,
Ghanu	Ghana	k1gFnSc4	Ghana
<g/>
,	,	kIx,	,
Horní	horní	k2eAgFnSc4d1	horní
Voltu	Volta	k1gFnSc4	Volta
a	a	k8xC	a
Pobřeží	pobřeží	k1gNnSc4	pobřeží
Slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
odletěl	odletět	k5eAaPmAgMnS	odletět
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gInSc4	on
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
přivítal	přivítat	k5eAaPmAgMnS	přivítat
prezident	prezident	k1gMnSc1	prezident
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Estaing	Estaing	k1gInSc1	Estaing
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
své	svůj	k3xOyFgFnSc2	svůj
návštěvy	návštěva	k1gFnSc2	návštěva
ukončené	ukončený	k2eAgNnSc1d1	ukončené
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
navštívil	navštívit	k5eAaPmAgMnS	navštívit
sídlo	sídlo	k1gNnSc4	sídlo
UNESCO	Unesco	k1gNnSc1	Unesco
<g/>
,	,	kIx,	,
aby	aby	k9	aby
na	na	k7c4	na
109	[number]	k4	109
<g/>
.	.	kIx.	.
výkonné	výkonný	k2eAgFnSc6d1	výkonná
radě	rada	k1gFnSc6	rada
pronesl	pronést	k5eAaPmAgMnS	pronést
podle	podle	k7c2	podle
vlastního	vlastní	k2eAgInSc2d1	vlastní
názoru	názor	k1gInSc2	názor
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
projevů	projev	k1gInPc2	projev
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
téhož	týž	k3xTgInSc2	týž
měsíce	měsíc	k1gInSc2	měsíc
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
na	na	k7c4	na
dvanáctidenní	dvanáctidenní	k2eAgFnSc4d1	dvanáctidenní
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
největší	veliký	k2eAgFnSc2d3	veliký
katolické	katolický	k2eAgFnSc2d1	katolická
země	zem	k1gFnSc2	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
Náročný	náročný	k2eAgInSc1d1	náročný
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
urazil	urazit	k5eAaPmAgMnS	urazit
30	[number]	k4	30
000	[number]	k4	000
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
zahrnoval	zahrnovat	k5eAaImAgMnS	zahrnovat
projev	projev	k1gInSc4	projev
k	k	k7c3	k
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
mladých	mladý	k2eAgMnPc2d1	mladý
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
Belo	Bela	k1gFnSc5	Bela
Horizonte	horizont	k1gInSc5	horizont
<g/>
,	,	kIx,	,
schůzku	schůzka	k1gFnSc4	schůzka
se	s	k7c7	s
150	[number]	k4	150
000	[number]	k4	000
dělníky	dělník	k1gMnPc7	dělník
v	v	k7c6	v
Sã	Sã	k1gFnSc6	Sã
Paulu	Paula	k1gFnSc4	Paula
<g/>
,	,	kIx,	,
návštěvu	návštěva	k1gFnSc4	návštěva
chudinské	chudinský	k2eAgFnSc2d1	chudinská
čtvrtě	čtvrt	k1gFnSc2	čtvrt
v	v	k7c6	v
Rio	Rio	k1gFnSc6	Rio
de	de	k?	de
Janeiru	Janeira	k1gMnSc4	Janeira
nebo	nebo	k8xC	nebo
rozhovor	rozhovor	k1gInSc4	rozhovor
se	se	k3xPyFc4	se
zástupci	zástupce	k1gMnPc1	zástupce
indiánů	indián	k1gMnPc2	indián
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
obviňovali	obviňovat	k5eAaImAgMnP	obviňovat
vládu	vláda	k1gFnSc4	vláda
z	z	k7c2	z
genocidy	genocida	k1gFnSc2	genocida
<g/>
,	,	kIx,	,
na	na	k7c6	na
slavnosti	slavnost	k1gFnSc6	slavnost
konané	konaný	k2eAgFnSc6d1	konaná
hluboko	hluboko	k6eAd1	hluboko
v	v	k7c6	v
amazonské	amazonský	k2eAgFnSc6d1	Amazonská
džungli	džungle	k1gFnSc6	džungle
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
další	další	k2eAgFnSc1d1	další
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
cesta	cesta	k1gFnSc1	cesta
vedla	vést	k5eAaImAgFnS	vést
do	do	k7c2	do
NSR	NSR	kA	NSR
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
hodinách	hodina	k1gFnPc6	hodina
pobytu	pobyt	k1gInSc2	pobyt
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Augustusburg	Augustusburg	k1gMnSc1	Augustusburg
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
Carstensem	Carstens	k1gMnSc7	Carstens
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
dnech	den	k1gInPc6	den
pak	pak	k6eAd1	pak
mj.	mj.	kA	mj.
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
promluvil	promluvit	k5eAaPmAgInS	promluvit
k	k	k7c3	k
6000	[number]	k4	6000
profesorů	profesor	k1gMnPc2	profesor
a	a	k8xC	a
studentů	student	k1gMnPc2	student
nebo	nebo	k8xC	nebo
pronesl	pronést	k5eAaPmAgMnS	pronést
projev	projev	k1gInSc4	projev
k	k	k7c3	k
místním	místní	k2eAgMnPc3d1	místní
biskupům	biskup	k1gMnPc3	biskup
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1981	[number]	k4	1981
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
nejtroufalejší	troufalý	k2eAgNnSc4d3	troufalý
jmenování	jmenování	k1gNnSc4	jmenování
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
pontifikátu	pontifikát	k1gInSc2	pontifikát
<g/>
,	,	kIx,	,
když	když	k8xS	když
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
pařížského	pařížský	k2eAgMnSc2d1	pařížský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
orleánského	orleánský	k2eAgMnSc4d1	orleánský
biskupa	biskup	k1gMnSc4	biskup
a	a	k8xC	a
židovského	židovský	k2eAgMnSc4d1	židovský
konvertitu	konvertita	k1gMnSc4	konvertita
Arona	Aron	k1gMnSc4	Aron
Lustigera	Lustiger	k1gMnSc4	Lustiger
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
čtrnáct	čtrnáct	k4xCc4	čtrnáct
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
)	)	kIx)	)
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
na	na	k7c4	na
papežskou	papežský	k2eAgFnSc4d1	Papežská
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
zahájil	zahájit	k5eAaPmAgMnS	zahájit
ve	v	k7c6	v
filipínské	filipínský	k2eAgFnSc6d1	filipínská
Manile	Manila	k1gFnSc6	Manila
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přivítání	přivítání	k1gNnSc6	přivítání
prezidentem	prezident	k1gMnSc7	prezident
Ul-Haqem	Ul-Haqem	k1gInSc1	Ul-Haqem
jej	on	k3xPp3gMnSc4	on
čekal	čekat	k5eAaImAgInS	čekat
nabitý	nabitý	k2eAgInSc1d1	nabitý
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
průběhu	průběh	k1gInSc6	průběh
inicioval	iniciovat	k5eAaBmAgInS	iniciovat
proces	proces	k1gInSc1	proces
blahořečení	blahořečení	k1gNnSc2	blahořečení
Lorenza	Lorenza	k?	Lorenza
Ruize	Ruize	k1gFnSc2	Ruize
<g/>
,	,	kIx,	,
ukřižovaného	ukřižovaný	k2eAgInSc2d1	ukřižovaný
v	v	k7c4	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Nagasaki	Nagasaki	k1gNnSc6	Nagasaki
<g/>
.	.	kIx.	.
</s>
<s>
Obřadu	obřad	k1gInSc2	obřad
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
po	po	k7c6	po
papežově	papežův	k2eAgInSc6d1	papežův
příletu	přílet	k1gInSc6	přílet
<g/>
,	,	kIx,	,
přihlížel	přihlížet	k5eAaImAgMnS	přihlížet
zhruba	zhruba	k6eAd1	zhruba
milion	milion	k4xCgInSc4	milion
Filipínců	Filipínec	k1gMnPc2	Filipínec
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
,	,	kIx,	,
po	po	k7c6	po
předchozím	předchozí	k2eAgInSc6d1	předchozí
dni	den	k1gInSc6	den
stráveném	strávený	k2eAgInSc6d1	strávený
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Guam	Guama	k1gFnPc2	Guama
<g/>
,	,	kIx,	,
dorazil	dorazit	k5eAaPmAgMnS	dorazit
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
u	u	k7c2	u
vchodu	vchod	k1gInSc2	vchod
do	do	k7c2	do
císařského	císařský	k2eAgInSc2d1	císařský
paláce	palác	k1gInSc2	palác
přivítal	přivítat	k5eAaPmAgMnS	přivítat
císař	císař	k1gMnSc1	císař
Hirohito	Hirohit	k2eAgNnSc1d1	Hirohito
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
panovníkem	panovník	k1gMnSc7	panovník
v	v	k7c6	v
japonské	japonský	k2eAgFnSc6d1	japonská
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
přijal	přijmout	k5eAaPmAgMnS	přijmout
představitele	představitel	k1gMnPc4	představitel
jiného	jiné	k1gNnSc2	jiné
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
svatý	svatý	k2eAgMnSc1d1	svatý
otec	otec	k1gMnSc1	otec
přednesl	přednést	k5eAaPmAgMnS	přednést
devítijazyčnou	devítijazyčný	k2eAgFnSc4d1	devítijazyčný
řeč	řeč	k1gFnSc4	řeč
u	u	k7c2	u
Památníku	památník	k1gInSc2	památník
míru	mír	k1gInSc2	mír
v	v	k7c6	v
Hirošimě	Hirošima	k1gFnSc6	Hirošima
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
projevu	projev	k1gInSc6	projev
ke	k	k7c3	k
studentům	student	k1gMnPc3	student
Univerzity	univerzita	k1gFnSc2	univerzita
OSN	OSN	kA	OSN
odcestoval	odcestovat	k5eAaPmAgInS	odcestovat
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
japonského	japonský	k2eAgInSc2d1	japonský
katolicismu	katolicismus	k1gInSc2	katolicismus
Nagasaki	Nagasaki	k1gNnSc2	Nagasaki
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cesta	k1gFnSc7	cesta
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
letadlo	letadlo	k1gNnSc4	letadlo
s	s	k7c7	s
papežem	papež	k1gMnSc7	papež
přeletělo	přeletět	k5eAaPmAgNnS	přeletět
přes	přes	k7c4	přes
severní	severní	k2eAgInSc4d1	severní
pól	pól	k1gInSc4	pól
a	a	k8xC	a
pro	pro	k7c4	pro
doplnění	doplnění	k1gNnSc4	doplnění
paliva	palivo	k1gNnSc2	palivo
přistálo	přistát	k5eAaPmAgNnS	přistát
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
mši	mše	k1gFnSc4	mše
celebrovanou	celebrovaný	k2eAgFnSc4d1	celebrovaná
pod	pod	k7c7	pod
širým	širý	k2eAgNnSc7d1	širé
nebem	nebe	k1gNnSc7	nebe
v	v	k7c4	v
Delaney	Delane	k1gMnPc4	Delane
Park	park	k1gInSc1	park
Strip	Strip	k1gInSc4	Strip
dorazilo	dorazit	k5eAaPmAgNnS	dorazit
50	[number]	k4	50
000	[number]	k4	000
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
při	při	k7c6	při
tradiční	tradiční	k2eAgFnSc6d1	tradiční
středeční	středeční	k2eAgFnSc6d1	středeční
projížďce	projížďka	k1gFnSc6	projížďka
v	v	k7c6	v
papamobilu	papamobil	k1gInSc6	papamobil
po	po	k7c6	po
Svatopetrském	svatopetrský	k2eAgNnSc6d1	Svatopetrské
náměstí	náměstí	k1gNnSc6	náměstí
na	na	k7c4	na
Jana	Jan	k1gMnSc4	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
spáchán	spáchat	k5eAaPmNgInS	spáchat
atentát	atentát	k1gInSc1	atentát
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
původcem	původce	k1gMnSc7	původce
byl	být	k5eAaImAgMnS	být
Turek	Turek	k1gMnSc1	Turek
Mehmet	Mehmet	k1gMnSc1	Mehmet
Ali	Ali	k1gMnSc1	Ali
Ağ	Ağ	k1gMnSc1	Ağ
<g/>
.	.	kIx.	.
</s>
<s>
Kulka	kulka	k1gFnSc1	kulka
browning	browning	k1gInSc1	browning
ráže	ráže	k1gFnSc1	ráže
9	[number]	k4	9
mm	mm	kA	mm
prošla	projít	k5eAaPmAgFnS	projít
břichem	břicho	k1gNnSc7	břicho
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
způsobila	způsobit	k5eAaPmAgNnP	způsobit
četná	četný	k2eAgNnPc1d1	četné
poranění	poranění	k1gNnPc1	poranění
střev	střevo	k1gNnPc2	střevo
a	a	k8xC	a
jen	jen	k9	jen
o	o	k7c4	o
několik	několik	k4yIc4	několik
milimetrů	milimetr	k1gInPc2	milimetr
minula	minout	k5eAaImAgFnS	minout
hlavní	hlavní	k2eAgFnSc4d1	hlavní
břišní	břišní	k2eAgFnSc4d1	břišní
tepnu	tepna	k1gFnSc4	tepna
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
byl	být	k5eAaImAgMnS	být
připravenou	připravený	k2eAgFnSc7d1	připravená
sanitkou	sanitka	k1gFnSc7	sanitka
ihned	ihned	k6eAd1	ihned
převezen	převézt	k5eAaPmNgMnS	převézt
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
Gemelli	Gemelle	k1gFnSc4	Gemelle
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zanedlouho	zanedlouho	k6eAd1	zanedlouho
ztratil	ztratit	k5eAaPmAgMnS	ztratit
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
pětihodnovou	pětihodnový	k2eAgFnSc7d1	pětihodnový
operací	operace	k1gFnSc7	operace
mu	on	k3xPp3gNnSc3	on
kardinál	kardinál	k1gMnSc1	kardinál
Stanisław	Stanisław	k1gMnSc1	Stanisław
Dziwisz	Dziwisz	k1gMnSc1	Dziwisz
udělil	udělit	k5eAaPmAgMnS	udělit
poslední	poslední	k2eAgNnSc4d1	poslední
pomazání	pomazání	k1gNnSc4	pomazání
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
hned	hned	k6eAd1	hned
druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
ráno	ráno	k6eAd1	ráno
se	se	k3xPyFc4	se
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
probral	probrat	k5eAaPmAgMnS	probrat
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
byl	být	k5eAaImAgMnS	být
převezen	převézt	k5eAaPmNgMnS	převézt
z	z	k7c2	z
JIP	JIP	kA	JIP
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
soukromého	soukromý	k2eAgInSc2d1	soukromý
pokoje	pokoj	k1gInSc2	pokoj
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
deset	deset	k4xCc4	deset
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
papež	papež	k1gMnSc1	papež
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
očekávanou	očekávaný	k2eAgFnSc4d1	očekávaná
tragickou	tragický	k2eAgFnSc4d1	tragická
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemřel	zemřít	k5eAaPmAgMnS	zemřít
polský	polský	k2eAgMnSc1d1	polský
primas	primas	k1gMnSc1	primas
kardinál	kardinál	k1gMnSc1	kardinál
Wyszyński	Wyszyńske	k1gFnSc4	Wyszyńske
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
svatému	svatý	k1gMnSc3	svatý
otci	otec	k1gMnSc3	otec
lékaři	lékař	k1gMnSc3	lékař
povolili	povolit	k5eAaPmAgMnP	povolit
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
Apoštolského	apoštolský	k2eAgInSc2d1	apoštolský
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
příštích	příští	k2eAgInPc6d1	příští
dnech	den	k1gInPc6	den
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
značně	značně	k6eAd1	značně
přitížilo	přitížit	k5eAaPmAgNnS	přitížit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jej	on	k3xPp3gMnSc4	on
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
přinutilo	přinutit	k5eAaPmAgNnS	přinutit
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
laboratorními	laboratorní	k2eAgFnPc7d1	laboratorní
testy	test	k1gInPc4	test
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
při	při	k7c6	při
transfúzi	transfúze	k1gFnSc6	transfúze
krve	krev	k1gFnSc2	krev
napaden	napaden	k2eAgInSc4d1	napaden
cytomegalovirózou	cytomegaloviróza	k1gFnSc7	cytomegaloviróza
<g/>
.	.	kIx.	.
</s>
<s>
Zlepšení	zlepšení	k1gNnSc1	zlepšení
jeho	jeho	k3xOp3gInSc2	jeho
stavu	stav	k1gInSc2	stav
nastalo	nastat	k5eAaPmAgNnS	nastat
čtyři	čtyři	k4xCgFnPc4	čtyři
dny	dna	k1gFnPc4	dna
po	po	k7c6	po
zahájení	zahájení	k1gNnSc6	zahájení
podpůrné	podpůrný	k2eAgFnSc2d1	podpůrná
terapie	terapie	k1gFnSc2	terapie
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
operativně	operativně	k6eAd1	operativně
zbaven	zbaven	k2eAgMnSc1d1	zbaven
kolostomie	kolostomie	k1gFnSc2	kolostomie
a	a	k8xC	a
čtrnáctého	čtrnáctý	k4xOgInSc2	čtrnáctý
dne	den	k1gInSc2	den
téhož	týž	k3xTgInSc2	týž
měsíce	měsíc	k1gInSc2	měsíc
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Vatikánu	Vatikán	k1gInSc2	Vatikán
<g/>
.	.	kIx.	.
</s>
<s>
Atentátník	atentátník	k1gMnSc1	atentátník
Ali	Ali	k1gMnSc1	Ali
Ağ	Ağ	k1gMnSc1	Ağ
byl	být	k5eAaImAgMnS	být
22	[number]	k4	22
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
odsouzen	odsoudit	k5eAaPmNgInS	odsoudit
na	na	k7c4	na
doživotí	doživotí	k1gNnSc4	doživotí
<g/>
,	,	kIx,	,
motiv	motiv	k1gInSc1	motiv
jeho	jeho	k3xOp3gInSc2	jeho
činu	čin	k1gInSc2	čin
se	se	k3xPyFc4	se
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nepodařilo	podařit	k5eNaPmAgNnS	podařit
jednoznačně	jednoznačně	k6eAd1	jednoznačně
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
publikoval	publikovat	k5eAaBmAgMnS	publikovat
encykliku	encyklika	k1gFnSc4	encyklika
Laborem	Labor	k1gInSc7	Labor
exercens	exercens	k1gInSc1	exercens
(	(	kIx(	(
<g/>
O	o	k7c6	o
lidské	lidský	k2eAgFnSc6d1	lidská
práci	práce	k1gFnSc6	práce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
navazovala	navazovat	k5eAaImAgFnS	navazovat
na	na	k7c4	na
moderní	moderní	k2eAgFnSc4d1	moderní
katolické	katolický	k2eAgNnSc4d1	katolické
sociální	sociální	k2eAgNnSc4d1	sociální
učení	učení	k1gNnSc4	učení
encykliky	encyklika	k1gFnSc2	encyklika
Rerum	Rerum	k1gNnSc1	Rerum
novarum	novarum	k1gNnSc1	novarum
z	z	k7c2	z
pera	pero	k1gNnSc2	pero
papeže	papež	k1gMnSc2	papež
Lva	lev	k1gInSc2	lev
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
znění	znění	k1gNnSc6	znění
textu	text	k1gInSc2	text
pracoval	pracovat	k5eAaImAgMnS	pracovat
ve	v	k7c6	v
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zotavoval	zotavovat	k5eAaImAgMnS	zotavovat
z	z	k7c2	z
utrpěného	utrpěný	k2eAgNnSc2d1	utrpěné
zranění	zranění	k1gNnSc2	zranění
a	a	k8xC	a
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
úvahy	úvaha	k1gFnPc1	úvaha
<g/>
,	,	kIx,	,
že	že	k8xS	že
skrze	skrze	k?	skrze
práci	práce	k1gFnSc3	práce
lidé	člověk	k1gMnPc1	člověk
naplňují	naplňovat	k5eAaImIp3nP	naplňovat
počáteční	počáteční	k2eAgInSc4d1	počáteční
Boží	boží	k2eAgInSc4d1	boží
příkaz	příkaz	k1gInSc4	příkaz
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Milujte	milovat	k5eAaImRp2nP	milovat
se	se	k3xPyFc4	se
a	a	k8xC	a
množte	množit	k5eAaImRp2nP	množit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
naplňte	naplnit	k5eAaPmRp2nP	naplnit
zemi	zem	k1gFnSc3	zem
a	a	k8xC	a
podrobte	podrobit	k5eAaPmRp2nP	podrobit
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
pooperační	pooperační	k2eAgFnSc4d1	pooperační
rekonvalescenci	rekonvalescence	k1gFnSc4	rekonvalescence
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Castel	Castel	k1gMnSc1	Castel
Gandolfo	Gandolfo	k1gMnSc1	Gandolfo
a	a	k8xC	a
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
návratu	návrat	k1gInSc6	návrat
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
otce	otec	k1gMnSc4	otec
Paola	Paol	k1gMnSc4	Paol
Dezzu	Dezza	k1gFnSc4	Dezza
<g/>
,	,	kIx,	,
S.	S.	kA	S.
<g/>
J.	J.	kA	J.
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
řádu	řád	k1gInSc2	řád
jezuitů	jezuita	k1gMnPc2	jezuita
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
pak	pak	k6eAd1	pak
opakovaně	opakovaně	k6eAd1	opakovaně
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
podporu	podpora	k1gFnSc4	podpora
odborovému	odborový	k2eAgInSc3d1	odborový
hnutí	hnutí	k1gNnPc1	hnutí
Solidarita	solidarita	k1gFnSc1	solidarita
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
rodné	rodný	k2eAgFnSc6d1	rodná
zemi	zem	k1gFnSc6	zem
aktivně	aktivně	k6eAd1	aktivně
postavilo	postavit	k5eAaPmAgNnS	postavit
proti	proti	k7c3	proti
komunistickému	komunistický	k2eAgInSc3d1	komunistický
režimu	režim	k1gInSc3	režim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1982	[number]	k4	1982
začal	začít	k5eAaPmAgMnS	začít
papež	papež	k1gMnSc1	papež
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sedmičlennou	sedmičlenný	k2eAgFnSc7d1	sedmičlenná
skupinou	skupina	k1gFnSc7	skupina
expertů	expert	k1gMnPc2	expert
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
církevního	církevní	k2eAgNnSc2d1	církevní
práva	právo	k1gNnSc2	právo
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
znění	znění	k1gNnSc6	znění
nového	nový	k2eAgInSc2d1	nový
kodexu	kodex	k1gInSc2	kodex
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
měl	mít	k5eAaImAgInS	mít
nahradil	nahradit	k5eAaPmAgInS	nahradit
kodex	kodex	k1gInSc1	kodex
církevního	církevní	k2eAgNnSc2d1	církevní
práva	právo	k1gNnSc2	právo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
téhož	týž	k3xTgInSc2	týž
měsíce	měsíc	k1gInSc2	měsíc
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
desáté	desátý	k4xOgFnSc6	desátý
papežské	papežský	k2eAgFnSc6d1	Papežská
cestě	cesta	k1gFnSc6	cesta
opět	opět	k6eAd1	opět
zavítal	zavítat	k5eAaPmAgMnS	zavítat
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Nigérii	Nigérie	k1gFnSc4	Nigérie
<g/>
,	,	kIx,	,
Benin	Benin	k1gInSc4	Benin
<g/>
,	,	kIx,	,
Gabon	Gabon	k1gNnSc4	Gabon
a	a	k8xC	a
Govníkovou	Govníkový	k2eAgFnSc4d1	Govníkový
Guineu	Guinea	k1gFnSc4	Guinea
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Vatikánu	Vatikán	k1gInSc2	Vatikán
se	s	k7c7	s
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
oficiálně	oficiálně	k6eAd1	oficiálně
postavil	postavit	k5eAaPmAgMnS	postavit
proti	proti	k7c3	proti
vměšování	vměšování	k1gNnSc3	vměšování
kléru	klér	k1gInSc2	klér
do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
o	o	k7c4	o
necelé	celý	k2eNgInPc4d1	necelý
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
chtěl	chtít	k5eAaImAgMnS	chtít
mj.	mj.	kA	mj.
ve	v	k7c6	v
Fátimě	Fátima	k1gFnSc6	Fátima
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
poděkování	poděkování	k1gNnSc4	poděkování
<g/>
,	,	kIx,	,
že	že	k8xS	že
přežil	přežít	k5eAaPmAgMnS	přežít
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
atentát	atentát	k1gInSc4	atentát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
přesně	přesně	k6eAd1	přesně
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
spáchal	spáchat	k5eAaPmAgMnS	spáchat
Mehmet	Mehmet	k1gMnSc1	Mehmet
Ali	Ali	k1gMnSc1	Ali
Ağ	Ağ	k1gMnSc1	Ağ
<g/>
.	.	kIx.	.
</s>
<s>
Symbolické	symbolický	k2eAgNnSc1d1	symbolické
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
gesto	gesto	k1gNnSc1	gesto
zejména	zejména	k9	zejména
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
útok	útok	k1gInSc1	útok
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
před	před	k7c7	před
pětašedesáti	pětašedesát	k4xCc2	pětašedesát
lety	léto	k1gNnPc7	léto
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
Fátimě	Fátima	k1gFnSc6	Fátima
zjevila	zjevit	k5eAaPmAgFnS	zjevit
Panna	Panna	k1gFnSc1	Panna
Marie	Marie	k1gFnSc1	Marie
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
již	již	k6eAd1	již
prvního	první	k4xOgInSc2	první
dne	den	k1gInSc2	den
pobytu	pobyt	k1gInSc2	pobyt
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
se	se	k3xPyFc4	se
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
pokusil	pokusit	k5eAaPmAgMnS	pokusit
zavraždit	zavraždit	k5eAaPmF	zavraždit
fundamentalistický	fundamentalistický	k2eAgMnSc1d1	fundamentalistický
španělský	španělský	k2eAgMnSc1d1	španělský
kněz	kněz	k1gMnSc1	kněz
otec	otec	k1gMnSc1	otec
Juan	Juan	k1gMnSc1	Juan
Fernández	Fernández	k1gMnSc1	Fernández
Krohn	Krohn	k1gMnSc1	Krohn
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
<g/>
,	,	kIx,	,
dřív	dříve	k6eAd2	dříve
než	než	k8xS	než
jej	on	k3xPp3gInSc4	on
stačila	stačit	k5eAaBmAgFnS	stačit
zneškodnit	zneškodnit	k5eAaPmF	zneškodnit
papežova	papežův	k2eAgFnSc1d1	papežova
osobní	osobní	k2eAgFnSc1d1	osobní
stráž	stráž	k1gFnSc1	stráž
<g/>
,	,	kIx,	,
poranil	poranit	k5eAaPmAgMnS	poranit
svatého	svatý	k2eAgMnSc4d1	svatý
otce	otec	k1gMnSc4	otec
bajonetem	bajonet	k1gInSc7	bajonet
na	na	k7c6	na
ruce	ruka	k1gFnSc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Motivem	motiv	k1gInSc7	motiv
Krohnova	Krohnův	k2eAgInSc2d1	Krohnův
činu	čin	k1gInSc2	čin
byl	být	k5eAaImAgMnS	být
odpor	odpor	k1gInSc4	odpor
vůči	vůči	k7c3	vůči
Druhému	druhý	k4xOgInSc3	druhý
vatikánskému	vatikánský	k2eAgInSc3d1	vatikánský
koncilu	koncil	k1gInSc3	koncil
a	a	k8xC	a
následujícím	následující	k2eAgFnPc3d1	následující
změnám	změna	k1gFnPc3	změna
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
a	a	k8xC	a
osobní	osobní	k2eAgNnPc4d1	osobní
přesvědčení	přesvědčení	k1gNnPc4	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
komunistou	komunista	k1gMnSc7	komunista
a	a	k8xC	a
agentem	agent	k1gMnSc7	agent
KGB	KGB	kA	KGB
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
rozvrátit	rozvrátit	k5eAaPmF	rozvrátit
církev	církev	k1gFnSc4	církev
zevnitř	zevnitř	k6eAd1	zevnitř
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
utrpěné	utrpěný	k2eAgFnPc4d1	utrpěná
zranění	zranění	k1gNnPc4	zranění
papež	papež	k1gMnSc1	papež
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
programu	program	k1gInSc6	program
(	(	kIx(	(
<g/>
událost	událost	k1gFnSc4	událost
navíc	navíc	k6eAd1	navíc
odmítal	odmítat	k5eAaImAgMnS	odmítat
zveřejnit	zveřejnit	k5eAaPmF	zveřejnit
<g/>
)	)	kIx)	)
a	a	k8xC	a
setkal	setkat	k5eAaPmAgMnS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
Lucií	Lucie	k1gFnSc7	Lucie
dos	dos	k?	dos
Santosovou	Santosový	k2eAgFnSc4d1	Santosový
<g/>
,	,	kIx,	,
posledním	poslední	k2eAgMnSc7d1	poslední
žijícím	žijící	k2eAgMnSc7d1	žijící
svědkem	svědek	k1gMnSc7	svědek
fátimského	fátimský	k2eAgNnSc2d1	fátimský
zjevení	zjevení	k1gNnSc2	zjevení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
falklandské	falklandský	k2eAgFnSc2d1	falklandská
krize	krize	k1gFnSc2	krize
měl	mít	k5eAaImAgInS	mít
od	od	k7c2	od
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1982	[number]	k4	1982
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
poprvé	poprvé	k6eAd1	poprvé
zavítal	zavítat	k5eAaPmAgMnS	zavítat
do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
cesta	cesta	k1gFnSc1	cesta
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
stavěla	stavět	k5eAaImAgFnS	stavět
do	do	k7c2	do
nepříjemné	příjemný	k2eNgFnSc2d1	nepříjemná
diplomatické	diplomatický	k2eAgFnSc2d1	diplomatická
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
agresor	agresor	k1gMnSc1	agresor
konfliktu	konflikt	k1gInSc2	konflikt
Argentina	Argentina	k1gFnSc1	Argentina
byla	být	k5eAaImAgFnS	být
formálně	formálně	k6eAd1	formálně
tradiční	tradiční	k2eAgFnSc7d1	tradiční
katolickou	katolický	k2eAgFnSc7d1	katolická
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
většina	většina	k1gFnSc1	většina
britské	britský	k2eAgFnSc2d1	britská
populace	populace	k1gFnSc2	populace
se	se	k3xPyFc4	se
hlásila	hlásit	k5eAaImAgFnS	hlásit
k	k	k7c3	k
anglikánské	anglikánský	k2eAgFnSc3d1	anglikánská
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Vatikánští	vatikánský	k2eAgMnPc1d1	vatikánský
diplomaté	diplomat	k1gMnPc1	diplomat
se	se	k3xPyFc4	se
obávali	obávat	k5eAaImAgMnP	obávat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nebyla	být	k5eNaImAgFnS	být
porušena	porušit	k5eAaPmNgFnS	porušit
neutralita	neutralita	k1gFnSc1	neutralita
Svatého	svatý	k2eAgInSc2d1	svatý
Stolce	stolec	k1gInSc2	stolec
<g/>
,	,	kIx,	,
po	po	k7c6	po
poradě	porada	k1gFnSc6	porada
s	s	k7c7	s
argentinským	argentinský	k2eAgInSc7d1	argentinský
episkopátem	episkopát	k1gInSc7	episkopát
proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
papež	papež	k1gMnSc1	papež
navštívit	navštívit	k5eAaPmF	navštívit
obě	dva	k4xCgFnPc1	dva
zainteresované	zainteresovaný	k2eAgFnPc1d1	zainteresovaná
země	zem	k1gFnPc1	zem
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
vyostřenou	vyostřený	k2eAgFnSc4d1	vyostřená
situaci	situace	k1gFnSc4	situace
byla	být	k5eAaImAgFnS	být
cesta	cesta	k1gFnSc1	cesta
po	po	k7c6	po
Británii	Británie	k1gFnSc6	Británie
velkým	velký	k2eAgInSc7d1	velký
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
završen	završit	k5eAaPmNgInS	završit
společnou	společný	k2eAgFnSc7d1	společná
ekumenickou	ekumenický	k2eAgFnSc7d1	ekumenická
bohoslužbou	bohoslužba	k1gFnSc7	bohoslužba
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
vedli	vést	k5eAaImAgMnP	vést
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
anglikánský	anglikánský	k2eAgMnSc1d1	anglikánský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Runcie	Runcie	k1gFnSc2	Runcie
v	v	k7c6	v
Canterburské	Canterburský	k2eAgFnSc6d1	Canterburská
katedrále	katedrála	k1gFnSc6	katedrála
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c6	po
obřadu	obřad	k1gInSc6	obřad
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
podepsáním	podepsání	k1gNnSc7	podepsání
společné	společný	k2eAgFnSc2d1	společná
deklarace	deklarace	k1gFnSc2	deklarace
o	o	k7c6	o
jednotě	jednota	k1gFnSc6	jednota
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
anglikánští	anglikánský	k2eAgMnPc1d1	anglikánský
a	a	k8xC	a
římskokatoličtí	římskokatolický	k2eAgMnPc1d1	římskokatolický
představitelé	představitel	k1gMnPc1	představitel
hodnotili	hodnotit	k5eAaImAgMnP	hodnotit
ekumenický	ekumenický	k2eAgInSc4d1	ekumenický
dialog	dialog	k1gInSc4	dialog
od	od	k7c2	od
Druhého	druhý	k4xOgInSc2	druhý
vatikánského	vatikánský	k2eAgInSc2d1	vatikánský
koncilu	koncil	k1gInSc2	koncil
a	a	k8xC	a
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
naději	naděje	k1gFnSc4	naděje
v	v	k7c4	v
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Londýna	Londýn	k1gInSc2	Londýn
svatý	svatý	k2eAgMnSc1d1	svatý
otec	otec	k1gMnSc1	otec
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
dnech	den	k1gInPc6	den
navštívil	navštívit	k5eAaPmAgMnS	navštívit
i	i	k9	i
Coventry	Coventr	k1gMnPc4	Coventr
<g/>
,	,	kIx,	,
Liverpool	Liverpool	k1gInSc1	Liverpool
<g/>
,	,	kIx,	,
Manchester	Manchester	k1gInSc1	Manchester
<g/>
,	,	kIx,	,
York	York	k1gInSc1	York
nebo	nebo	k8xC	nebo
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Vatikánu	Vatikán	k1gInSc2	Vatikán
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
a	a	k8xC	a
o	o	k7c4	o
pět	pět	k4xCc4	pět
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
půdě	půda	k1gFnSc6	půda
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
Ronaldem	Ronald	k1gMnSc7	Ronald
Reaganem	Reagan	k1gMnSc7	Reagan
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
se	se	k3xPyFc4	se
shodl	shodnout	k5eAaPmAgMnS	shodnout
na	na	k7c6	na
nesouhlasu	nesouhlas	k1gInSc6	nesouhlas
s	s	k7c7	s
dohodou	dohoda	k1gFnSc7	dohoda
o	o	k7c4	o
rozdělení	rozdělení	k1gNnSc4	rozdělení
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
Jaltě	Jalta	k1gFnSc6	Jalta
<g/>
.	.	kIx.	.
</s>
<s>
Desátého	desátý	k4xOgInSc2	desátý
dne	den	k1gInSc2	den
téhož	týž	k3xTgInSc2	týž
měsíce	měsíc	k1gInSc2	měsíc
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
na	na	k7c4	na
plánovanou	plánovaný	k2eAgFnSc4d1	plánovaná
dvoudenní	dvoudenní	k2eAgFnSc4d1	dvoudenní
návštěvu	návštěva	k1gFnSc4	návštěva
do	do	k7c2	do
Buenos	Buenosa	k1gFnPc2	Buenosa
Aires	Airesa	k1gFnPc2	Airesa
<g/>
.	.	kIx.	.
</s>
<s>
Formálně	formálně	k6eAd1	formálně
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
cesta	cesta	k1gFnSc1	cesta
prezentovaná	prezentovaný	k2eAgFnSc1d1	prezentovaná
jako	jako	k8xS	jako
pouť	pouť	k1gFnSc1	pouť
do	do	k7c2	do
svatyně	svatyně	k1gFnSc2	svatyně
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Lujanské	Lujanský	k2eAgFnSc2d1	Lujanský
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
narychlo	narychlo	k6eAd1	narychlo
svolané	svolaný	k2eAgNnSc1d1	svolané
shromáždění	shromáždění	k1gNnSc1	shromáždění
Rady	rada	k1gFnSc2	rada
latinskoamerických	latinskoamerický	k2eAgFnPc2d1	latinskoamerická
biskupských	biskupský	k2eAgFnPc2d1	biskupská
konferencí	konference	k1gFnPc2	konference
(	(	kIx(	(
<g/>
CELAM	CELAM	kA	CELAM
<g/>
)	)	kIx)	)
mělo	mít	k5eAaImAgNnS	mít
celý	celý	k2eAgInSc4d1	celý
kontinent	kontinent	k1gInSc4	kontinent
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
papežova	papežův	k2eAgFnSc1d1	papežova
přítomnost	přítomnost	k1gFnSc1	přítomnost
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
je	být	k5eAaImIp3nS	být
křížovou	křížový	k2eAgFnSc7d1	křížová
výpravou	výprava	k1gFnSc7	výprava
za	za	k7c4	za
dosažení	dosažení	k1gNnSc4	dosažení
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Švýcarsko	Švýcarsko	k1gNnSc4	Švýcarsko
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
)	)	kIx)	)
a	a	k8xC	a
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
San	San	k1gFnSc2	San
Marino	Marina	k1gFnSc5	Marina
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
se	se	k3xPyFc4	se
ve	v	k7c6	v
Vatikánu	Vatikán	k1gInSc6	Vatikán
při	při	k7c6	při
dvacetiminutové	dvacetiminutový	k2eAgFnSc6d1	dvacetiminutová
audienci	audience	k1gFnSc6	audience
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
vůdcem	vůdce	k1gMnSc7	vůdce
OOP	OOP	kA	OOP
Jásirem	Jásir	k1gMnSc7	Jásir
Arafatem	Arafat	k1gMnSc7	Arafat
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
uznal	uznat	k5eAaPmAgMnS	uznat
právo	právo	k1gNnSc4	právo
Palestinců	Palestinec	k1gMnPc2	Palestinec
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
vlast	vlast	k1gFnSc4	vlast
i	i	k8xC	i
právo	právo	k1gNnSc4	právo
Izraele	Izrael	k1gInSc2	Izrael
<g/>
,	,	kIx,	,
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Židovských	židovský	k2eAgMnPc2d1	židovský
představitelů	představitel	k1gMnPc2	představitel
a	a	k8xC	a
izraelských	izraelský	k2eAgInPc2d1	izraelský
vládních	vládní	k2eAgInPc2d1	vládní
činitelů	činitel	k1gInPc2	činitel
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
schůzka	schůzka	k1gFnSc1	schůzka
naprostým	naprostý	k2eAgInSc7d1	naprostý
skandálem	skandál	k1gInSc7	skandál
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
pak	pak	k9	pak
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
za	za	k7c2	za
poněkud	poněkud	k6eAd1	poněkud
kontroverzních	kontroverzní	k2eAgFnPc2d1	kontroverzní
okolností	okolnost	k1gFnPc2	okolnost
kanonizoval	kanonizovat	k5eAaBmAgInS	kanonizovat
polského	polský	k2eAgMnSc4d1	polský
kněze	kněz	k1gMnSc4	kněz
Maximiliána	Maximilián	k1gMnSc4	Maximilián
Kolbeho	Kolbe	k1gMnSc4	Kolbe
a	a	k8xC	a
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
odletěl	odletět	k5eAaPmAgMnS	odletět
na	na	k7c4	na
desetidenní	desetidenní	k2eAgFnSc4d1	desetidenní
oficiální	oficiální	k2eAgFnSc4d1	oficiální
návštěvu	návštěva	k1gFnSc4	návštěva
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
vydal	vydat	k5eAaPmAgMnS	vydat
další	další	k2eAgFnSc4d1	další
papežskou	papežský	k2eAgFnSc4d1	Papežská
konstituci	konstituce	k1gFnSc4	konstituce
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
založil	založit	k5eAaPmAgMnS	založit
personální	personální	k2eAgFnSc4d1	personální
prelaturu	prelatura	k1gFnSc4	prelatura
církevní	církevní	k2eAgFnSc2d1	církevní
organizace	organizace	k1gFnSc2	organizace
Opus	opus	k1gInSc1	opus
Dei	Dei	k1gFnSc4	Dei
<g/>
.	.	kIx.	.
</s>
<s>
Vůči	vůči	k7c3	vůči
tomuto	tento	k3xDgNnSc3	tento
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
vyjadřovali	vyjadřovat	k5eAaImAgMnP	vyjadřovat
někteří	některý	k3yIgMnPc1	některý
biskupové	biskup	k1gMnPc1	biskup
značný	značný	k2eAgInSc1d1	značný
odpor	odpor	k1gInSc4	odpor
a	a	k8xC	a
téma	téma	k1gNnSc4	téma
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
sporným	sporný	k2eAgInSc7d1	sporný
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
papežova	papežův	k2eAgInSc2d1	papežův
pontifikátu	pontifikát	k1gInSc2	pontifikát
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgInS	pokusit
vyřešit	vyřešit	k5eAaPmF	vyřešit
společensko-církevní	společenskoírkevní	k2eAgFnSc4d1	společensko-církevní
krizi	krize	k1gFnSc4	krize
<g/>
,	,	kIx,	,
typickou	typický	k2eAgFnSc4d1	typická
pro	pro	k7c4	pro
období	období	k1gNnSc4	období
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
znamenala	znamenat	k5eAaImAgFnS	znamenat
citelnou	citelný	k2eAgFnSc4d1	citelná
ztrátu	ztráta	k1gFnSc4	ztráta
zájmu	zájem	k1gInSc2	zájem
veřejnosti	veřejnost	k1gFnSc2	veřejnost
o	o	k7c4	o
dění	dění	k1gNnSc4	dění
v	v	k7c6	v
církevním	církevní	k2eAgInSc6d1	církevní
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
papež	papež	k1gMnSc1	papež
propagoval	propagovat	k5eAaImAgMnS	propagovat
realistický	realistický	k2eAgInSc4d1	realistický
přístup	přístup	k1gInSc4	přístup
<g/>
,	,	kIx,	,
konzervatismus	konzervatismus	k1gInSc4	konzervatismus
a	a	k8xC	a
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
církevní	církevní	k2eAgFnSc3d1	církevní
autoritě	autorita	k1gFnSc3	autorita
<g/>
.	.	kIx.	.
</s>
<s>
Vyplnil	vyplnit	k5eAaPmAgMnS	vyplnit
tak	tak	k9	tak
podstatu	podstata	k1gFnSc4	podstata
jevů	jev	k1gInPc2	jev
a	a	k8xC	a
činů	čin	k1gInPc2	čin
typických	typický	k2eAgInPc2d1	typický
pro	pro	k7c4	pro
přicházející	přicházející	k2eAgNnSc4d1	přicházející
období	období	k1gNnSc4	období
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
svého	svůj	k3xOyFgInSc2	svůj
pontifikátu	pontifikát	k1gInSc2	pontifikát
věnoval	věnovat	k5eAaPmAgMnS	věnovat
velkou	velký	k2eAgFnSc4d1	velká
pozornost	pozornost	k1gFnSc4	pozornost
zahraničním	zahraniční	k2eAgFnPc3d1	zahraniční
cestám	cesta	k1gFnPc3	cesta
(	(	kIx(	(
<g/>
104	[number]	k4	104
-	-	kIx~	-
poslední	poslední	k2eAgFnSc2d1	poslední
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cestám	cesta	k1gFnPc3	cesta
po	po	k7c6	po
Itálii	Itálie	k1gFnSc6	Itálie
(	(	kIx(	(
<g/>
146	[number]	k4	146
<g/>
)	)	kIx)	)
i	i	k8xC	i
po	po	k7c6	po
římské	římský	k2eAgFnSc6d1	římská
diecézi	diecéze	k1gFnSc6	diecéze
(	(	kIx(	(
<g/>
748	[number]	k4	748
<g/>
)	)	kIx)	)
a	a	k8xC	a
aktivnímu	aktivní	k2eAgInSc3d1	aktivní
zájmu	zájem	k1gInSc3	zájem
o	o	k7c4	o
světové	světový	k2eAgNnSc4d1	světové
politické	politický	k2eAgNnSc4d1	politické
dění	dění	k1gNnSc4	dění
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
hlavních	hlavní	k2eAgFnPc2d1	hlavní
charakteristik	charakteristika	k1gFnPc2	charakteristika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
význam	význam	k1gInSc1	význam
v	v	k7c4	v
řešení	řešení	k1gNnSc4	řešení
otázek	otázka	k1gFnPc2	otázka
vztahu	vztah	k1gInSc2	vztah
západního	západní	k2eAgInSc2d1	západní
a	a	k8xC	a
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
je	být	k5eAaImIp3nS	být
nepřehlédnutelný	přehlédnutelný	k2eNgInSc1d1	nepřehlédnutelný
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jeho	jeho	k3xOp3gFnSc1	jeho
aktivní	aktivní	k2eAgFnSc1d1	aktivní
role	role	k1gFnSc1	role
v	v	k7c6	v
pádu	pád	k1gInSc6	pád
komunistických	komunistický	k2eAgInPc2d1	komunistický
režimů	režim	k1gInPc2	režim
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Významně	významně	k6eAd1	významně
se	se	k3xPyFc4	se
také	také	k9	také
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
mezináboženském	mezináboženský	k2eAgInSc6d1	mezináboženský
dialogu	dialog	k1gInSc6	dialog
s	s	k7c7	s
příslušníky	příslušník	k1gMnPc7	příslušník
jinověrných	jinověrný	k2eAgFnPc2d1	jinověrný
církví	církev	k1gFnPc2	církev
a	a	k8xC	a
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
uvědomění	uvědomění	k1gNnSc3	uvědomění
a	a	k8xC	a
toleranci	tolerance	k1gFnSc4	tolerance
vůči	vůči	k7c3	vůči
ostatním	ostatní	k2eAgNnPc3d1	ostatní
náboženstvím	náboženství	k1gNnPc3	náboženství
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
v	v	k7c6	v
případě	případ	k1gInSc6	případ
ožehavého	ožehavý	k2eAgInSc2d1	ožehavý
islámu	islám	k1gInSc2	islám
<g/>
.	.	kIx.	.
</s>
<s>
Provedl	provést	k5eAaPmAgMnS	provést
i	i	k9	i
změnu	změna	k1gFnSc4	změna
v	v	k7c6	v
organizaci	organizace	k1gFnSc6	organizace
a	a	k8xC	a
pravidlech	pravidlo	k1gNnPc6	pravidlo
papežské	papežský	k2eAgFnSc2d1	Papežská
volby	volba	k1gFnSc2	volba
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejzávažnější	závažný	k2eAgFnSc7d3	nejzávažnější
změnou	změna	k1gFnSc7	změna
bylo	být	k5eAaImAgNnS	být
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
ani	ani	k8xC	ani
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
sériích	série	k1gFnPc6	série
skrutinií	skrutinium	k1gNnPc2	skrutinium
(	(	kIx(	(
<g/>
33	[number]	k4	33
<g/>
-	-	kIx~	-
<g/>
34	[number]	k4	34
kol	kolo	k1gNnPc2	kolo
<g/>
)	)	kIx)	)
nikdo	nikdo	k3yNnSc1	nikdo
nezíská	získat	k5eNaPmIp3nS	získat
dvě	dva	k4xCgFnPc4	dva
třetiny	třetina	k1gFnPc4	třetina
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
stačí	stačit	k5eAaBmIp3nS	stačit
ke	k	k7c3	k
zvolení	zvolení	k1gNnSc3	zvolení
papeže	papež	k1gMnSc2	papež
prostá	prostý	k2eAgFnSc1d1	prostá
většina	většina	k1gFnSc1	většina
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
byla	být	k5eAaImAgFnS	být
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
jako	jako	k8xS	jako
faktické	faktický	k2eAgNnSc1d1	faktické
totální	totální	k2eAgNnSc1d1	totální
zrušení	zrušení	k1gNnSc1	zrušení
potřeby	potřeba	k1gFnSc2	potřeba
dvoutřetinové	dvoutřetinový	k2eAgFnSc2d1	dvoutřetinová
většiny	většina	k1gFnSc2	většina
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2007	[number]	k4	2007
zrušil	zrušit	k5eAaPmAgMnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
letech	léto	k1gNnPc6	léto
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
exkomunikaci	exkomunikace	k1gFnSc3	exkomunikace
Mons	Mons	k1gInSc4	Mons
<g/>
.	.	kIx.	.
</s>
<s>
Marcela	Marcel	k1gMnSc2	Marcel
Lefebvra	Lefebvr	k1gMnSc2	Lefebvr
<g/>
,	,	kIx,	,
zakladatele	zakladatel	k1gMnSc2	zakladatel
hnutí	hnutí	k1gNnSc2	hnutí
Kněžského	kněžský	k2eAgNnSc2d1	kněžské
Bratrstva	bratrstvo	k1gNnSc2	bratrstvo
sv.	sv.	kA	sv.
Pia	Pius	k1gMnSc2	Pius
X.	X.	kA	X.
FSSPX	FSSPX	kA	FSSPX
a	a	k8xC	a
čtyř	čtyři	k4xCgMnPc2	čtyři
jím	on	k3xPp3gMnSc7	on
nově	nova	k1gFnSc3	nova
vysvěcených	vysvěcený	k2eAgMnPc2d1	vysvěcený
biskupů	biskup	k1gMnPc2	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Těm	ten	k3xDgMnPc3	ten
později	pozdě	k6eAd2	pozdě
papež	papež	k1gMnSc1	papež
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
účinky	účinek	k1gInPc1	účinek
exkomunikace	exkomunikace	k1gFnSc2	exkomunikace
prominul	prominout	k5eAaPmAgInS	prominout
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
papežů	papež	k1gMnPc2	papež
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
prováděl	provádět	k5eAaImAgInS	provádět
třikrát	třikrát	k6eAd1	třikrát
veřejný	veřejný	k2eAgInSc4d1	veřejný
exorcismus	exorcismus	k1gInSc4	exorcismus
<g/>
.	.	kIx.	.
</s>
<s>
Posledních	poslední	k2eAgNnPc2d1	poslední
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
měl	mít	k5eAaImAgInS	mít
vážné	vážný	k2eAgInPc4d1	vážný
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
problémy	problém	k1gInPc4	problém
(	(	kIx(	(
<g/>
Parkinsonova	Parkinsonův	k2eAgFnSc1d1	Parkinsonova
choroba	choroba	k1gFnSc1	choroba
<g/>
,	,	kIx,	,
artritida	artritida	k1gFnSc1	artritida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
spekulace	spekulace	k1gFnPc1	spekulace
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
možné	možný	k2eAgFnSc6d1	možná
rezignaci	rezignace	k1gFnSc6	rezignace
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
vážný	vážný	k2eAgInSc4d1	vážný
zdravotní	zdravotní	k2eAgInSc4d1	zdravotní
stav	stav	k1gInSc4	stav
však	však	k9	však
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
vytrval	vytrvat	k5eAaPmAgMnS	vytrvat
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
papeže	papež	k1gMnSc2	papež
až	až	k6eAd1	až
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nS	by
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
velkou	velký	k2eAgFnSc4d1	velká
autoritu	autorita	k1gFnSc4	autorita
část	část	k1gFnSc1	část
věřících	věřící	k1gMnPc2	věřící
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
o	o	k7c4	o
vzdání	vzdání	k1gNnSc4	vzdání
se	se	k3xPyFc4	se
pontifikátu	pontifikát	k1gInSc3	pontifikát
nemusela	muset	k5eNaImAgFnS	muset
akceptovat	akceptovat	k5eAaBmF	akceptovat
a	a	k8xC	a
uznávali	uznávat	k5eAaImAgMnP	uznávat
by	by	kYmCp3nP	by
ho	on	k3xPp3gMnSc4	on
stále	stále	k6eAd1	stále
za	za	k7c2	za
papeže	papež	k1gMnSc2	papež
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
by	by	kYmCp3nP	by
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
hrozil	hrozit	k5eAaImAgInS	hrozit
rozkol	rozkol	k1gInSc1	rozkol
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
také	také	k9	také
uváděly	uvádět	k5eAaImAgFnP	uvádět
informace	informace	k1gFnPc1	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
také	také	k9	také
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
chtěl	chtít	k5eAaImAgInS	chtít
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
nemocemi	nemoc	k1gFnPc7	nemoc
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
bojovat	bojovat	k5eAaImF	bojovat
a	a	k8xC	a
nevzdávat	vzdávat	k5eNaImF	vzdávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
příkladem	příklad	k1gInSc7	příklad
pro	pro	k7c4	pro
další	další	k2eAgMnPc4d1	další
nemocné	nemocný	k2eAgMnPc4d1	nemocný
věřící	věřící	k1gMnPc4	věřící
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
Vatikán	Vatikán	k1gInSc1	Vatikán
podrobný	podrobný	k2eAgInSc4d1	podrobný
popis	popis	k1gInSc4	popis
posledních	poslední	k2eAgInPc2d1	poslední
měsíců	měsíc	k1gInPc2	měsíc
papežova	papežův	k2eAgInSc2d1	papežův
života	život	k1gInSc2	život
-	-	kIx~	-
sahá	sahat	k5eAaImIp3nS	sahat
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c4	po
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
220	[number]	k4	220
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
těchto	tento	k3xDgInPc2	tento
zápisů	zápis	k1gInPc2	zápis
byla	být	k5eAaImAgNnP	být
poslední	poslední	k2eAgNnPc1d1	poslední
papežova	papežův	k2eAgNnPc1d1	papežovo
slova	slovo	k1gNnPc1	slovo
"	"	kIx"	"
<g/>
Pozwólcie	Pozwólcie	k1gFnSc1	Pozwólcie
mi	já	k3xPp1nSc3	já
iść	iść	k?	iść
do	do	k7c2	do
domu	dům	k1gInSc2	dům
Ojca	Ojc	k1gInSc2	Ojc
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Nechte	nechat	k5eAaPmRp2nP	nechat
mne	já	k3xPp1nSc4	já
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
Otcova	otcův	k2eAgInSc2d1	otcův
domu	dům	k1gInSc2	dům
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
rozšířily	rozšířit	k5eAaPmAgInP	rozšířit
požadavky	požadavek	k1gInPc1	požadavek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
okamžitě	okamžitě	k6eAd1	okamžitě
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
svatého	svatý	k1gMnSc4	svatý
(	(	kIx(	(
<g/>
již	již	k9	již
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
pohřbu	pohřeb	k1gInSc6	pohřeb
volal	volat	k5eAaImAgInS	volat
dav	dav	k1gInSc1	dav
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Santo	Sant	k2eAgNnSc1d1	Santo
Subito	subito	k6eAd1	subito
<g/>
"	"	kIx"	"
-	-	kIx~	-
"	"	kIx"	"
<g/>
Ať	ať	k9	ať
je	být	k5eAaImIp3nS	být
svatý	svatý	k2eAgInSc1d1	svatý
ihned	ihned	k6eAd1	ihned
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
však	však	k9	však
podle	podle	k7c2	podle
církevního	církevní	k2eAgNnSc2d1	církevní
práva	právo	k1gNnSc2	právo
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vyžadováno	vyžadován	k2eAgNnSc1d1	vyžadováno
splnění	splnění	k1gNnSc1	splnění
přísných	přísný	k2eAgFnPc2d1	přísná
podmínek	podmínka	k1gFnPc2	podmínka
a	a	k8xC	a
složitý	složitý	k2eAgInSc4d1	složitý
beatifikační	beatifikační	k2eAgInSc4d1	beatifikační
a	a	k8xC	a
kanonizační	kanonizační	k2eAgInSc4d1	kanonizační
proces	proces	k1gInSc4	proces
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2005	[number]	k4	2005
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
"	"	kIx"	"
<g/>
výjimečným	výjimečný	k2eAgFnPc3d1	výjimečná
okolnostem	okolnost	k1gFnPc3	okolnost
<g/>
"	"	kIx"	"
bude	být	k5eAaImBp3nS	být
ihned	ihned	k6eAd1	ihned
zahájen	zahájit	k5eAaPmNgInS	zahájit
beatifikační	beatifikační	k2eAgInSc1d1	beatifikační
proces	proces	k1gInSc1	proces
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
předepsanou	předepsaný	k2eAgFnSc4d1	předepsaná
pětiletou	pětiletý	k2eAgFnSc4d1	pětiletá
prodlevu	prodleva	k1gFnSc4	prodleva
mezi	mezi	k7c7	mezi
smrtí	smrt	k1gFnSc7	smrt
kandidáta	kandidát	k1gMnSc2	kandidát
a	a	k8xC	a
zahájením	zahájení	k1gNnSc7	zahájení
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
složitý	složitý	k2eAgInSc1d1	složitý
proces	proces	k1gInSc1	proces
beatifikace	beatifikace	k1gFnSc2	beatifikace
a	a	k8xC	a
kanonizace	kanonizace	k1gFnSc2	kanonizace
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
nijak	nijak	k6eAd1	nijak
zjednodušen	zjednodušit	k5eAaPmNgInS	zjednodušit
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
papež	papež	k1gMnSc1	papež
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
svého	svůj	k3xOyFgMnSc4	svůj
předchůdce	předchůdce	k1gMnSc4	předchůdce
za	za	k7c2	za
"	"	kIx"	"
<g/>
Ctihodného	ctihodný	k2eAgNnSc2d1	ctihodné
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vedl	vést	k5eAaImAgMnS	vést
"	"	kIx"	"
<g/>
hrdinský	hrdinský	k2eAgInSc4d1	hrdinský
a	a	k8xC	a
ctnostný	ctnostný	k2eAgInSc4d1	ctnostný
život	život	k1gInSc4	život
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
splnění	splnění	k1gNnSc3	splnění
všech	všecek	k3xTgFnPc2	všecek
nezbytných	nezbytný	k2eAgFnPc2d1	nezbytná
podmínek	podmínka	k1gFnPc2	podmínka
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
blahoslaven	blahoslavit	k5eAaImNgInS	blahoslavit
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
nejrychleji	rychle	k6eAd3	rychle
blahoslaveným	blahoslavený	k2eAgMnSc7d1	blahoslavený
papežem	papež	k1gMnSc7	papež
v	v	k7c6	v
církevních	církevní	k2eAgFnPc6d1	církevní
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Papežův	Papežův	k2eAgMnSc1d1	Papežův
blízký	blízký	k2eAgMnSc1d1	blízký
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
kardinál	kardinál	k1gMnSc1	kardinál
Stanisław	Stanisław	k1gMnSc1	Stanisław
Dziwisz	Dziwisz	k1gMnSc1	Dziwisz
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Bude	být	k5eAaImBp3nS	být
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
nás	my	k3xPp1nPc4	my
velká	velký	k2eAgFnSc1d1	velká
radost	radost	k1gFnSc1	radost
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
bude	být	k5eAaImBp3nS	být
oficiálně	oficiálně	k6eAd1	oficiálně
blahořečen	blahořečen	k2eAgMnSc1d1	blahořečen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
teď	teď	k6eAd1	teď
ho	on	k3xPp3gMnSc4	on
pokládáme	pokládat	k5eAaImIp1nP	pokládat
za	za	k7c4	za
svatého	svatý	k2eAgMnSc4d1	svatý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Liturgické	liturgický	k2eAgNnSc1d1	liturgické
slavení	slavení	k1gNnSc1	slavení
památky	památka	k1gFnSc2	památka
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
jako	jako	k8xS	jako
papež	papež	k1gMnSc1	papež
uveden	uvést	k5eAaPmNgMnS	uvést
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
Vatikán	Vatikán	k1gInSc1	Vatikán
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
bude	být	k5eAaImBp3nS	být
svatořečen	svatořečen	k2eAgMnSc1d1	svatořečen
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
svatého	svatý	k1gMnSc2	svatý
byl	být	k5eAaImAgMnS	být
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
svatým	svatý	k2eAgMnSc7d1	svatý
otcem	otec	k1gMnSc7	otec
Františkem	František	k1gMnSc7	František
(	(	kIx(	(
<g/>
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
emeritního	emeritní	k2eAgMnSc2d1	emeritní
papeže	papež	k1gMnSc2	papež
Benedikta	Benedikt	k1gMnSc2	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Vatikánu	Vatikán	k1gInSc6	Vatikán
společně	společně	k6eAd1	společně
s	s	k7c7	s
papežem	papež	k1gMnSc7	papež
Janem	Jan	k1gMnSc7	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
o	o	k7c6	o
druhé	druhý	k4xOgFnSc6	druhý
neděli	neděle	k1gFnSc6	neděle
velikonoční	velikonoční	k2eAgMnPc1d1	velikonoční
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
nedělí	dělit	k5eNaImIp3nS	dělit
Božího	boží	k2eAgNnSc2d1	boží
milosrdenství	milosrdenství	k1gNnSc2	milosrdenství
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
navrhovanou	navrhovaný	k2eAgFnSc7d1	navrhovaná
otázkou	otázka	k1gFnSc7	otázka
je	být	k5eAaImIp3nS	být
udělení	udělení	k1gNnSc1	udělení
čestného	čestný	k2eAgInSc2d1	čestný
titulu	titul	k1gInSc2	titul
"	"	kIx"	"
<g/>
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
Magnus	Magnus	k1gInSc1	Magnus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dosud	dosud	k6eAd1	dosud
přísluší	příslušet	k5eAaImIp3nS	příslušet
pouze	pouze	k6eAd1	pouze
třem	tři	k4xCgMnPc3	tři
papežům	papež	k1gMnPc3	papež
<g/>
,	,	kIx,	,
Lvu	lev	k1gInSc3	lev
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Řehoři	Řehoř	k1gMnSc3	Řehoř
I.	I.	kA	I.
a	a	k8xC	a
Mikuláši	Mikuláš	k1gMnPc1	Mikuláš
I.	I.	kA	I.
Problém	problém	k1gInSc1	problém
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
užívání	užívání	k1gNnSc1	užívání
tohoto	tento	k3xDgInSc2	tento
titulu	titul	k1gInSc2	titul
není	být	k5eNaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
kanonickým	kanonický	k2eAgNnSc7d1	kanonické
právem	právo	k1gNnSc7	právo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obecnou	obecný	k2eAgFnSc7d1	obecná
zvyklostí	zvyklost	k1gFnSc7	zvyklost
-	-	kIx~	-
patří	patřit	k5eAaImIp3nP	patřit
zkrátka	zkrátka	k6eAd1	zkrátka
těm	ten	k3xDgMnPc3	ten
papežům	papež	k1gMnPc3	papež
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgFnPc2	který
se	se	k3xPyFc4	se
ustálilo	ustálit	k5eAaPmAgNnS	ustálit
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
<g/>
.	.	kIx.	.
</s>
<s>
Nejde	jít	k5eNaImIp3nS	jít
tedy	tedy	k9	tedy
o	o	k7c4	o
otázku	otázka	k1gFnSc4	otázka
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
udělení	udělení	k1gNnSc2	udělení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
vžitost	vžitost	k1gFnSc4	vžitost
tohoto	tento	k3xDgInSc2	tento
titulu	titul	k1gInSc2	titul
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
již	již	k6eAd1	již
teď	teď	k6eAd1	teď
používají	používat	k5eAaImIp3nP	používat
některá	některý	k3yIgNnPc1	některý
periodika	periodikum	k1gNnPc1	periodikum
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
slovo	slovo	k1gNnSc1	slovo
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
předchůdcem	předchůdce	k1gMnSc7	předchůdce
i	i	k8xC	i
sám	sám	k3xTgMnSc1	sám
papež	papež	k1gMnSc1	papež
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
institucí	instituce	k1gFnPc2	instituce
a	a	k8xC	a
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
jde	jít	k5eAaImIp3nS	jít
například	například	k6eAd1	například
o	o	k7c4	o
Mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
<g/>
,	,	kIx,	,
Poloostrov	poloostrov	k1gInSc4	poloostrov
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
na	na	k7c6	na
Livingstonově	Livingstonův	k2eAgInSc6d1	Livingstonův
ostrově	ostrov	k1gInSc6	ostrov
či	či	k8xC	či
Katolická	katolický	k2eAgFnSc1d1	katolická
univerzita	univerzita	k1gFnSc1	univerzita
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Velikého	veliký	k2eAgInSc2d1	veliký
v	v	k7c6	v
San	San	k1gFnSc6	San
Diegu	Dieg	k1gInSc2	Dieg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
jde	jít	k5eAaImIp3nS	jít
například	například	k6eAd1	například
o	o	k7c6	o
CSZŠ	CSZŠ	kA	CSZŠ
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
..	..	k?	..
Jmenují	jmenovat	k5eAaBmIp3nP	jmenovat
se	se	k3xPyFc4	se
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
náměstí	náměstí	k1gNnSc6	náměstí
například	například	k6eAd1	například
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
a	a	k8xC	a
Žilině	Žilina	k1gFnSc6	Žilina
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
jeho	jeho	k3xOp3gFnSc2	jeho
návštěvy	návštěva	k1gFnSc2	návštěva
<g/>
.	.	kIx.	.
</s>
<s>
Janu	Jan	k1gMnSc3	Jan
Pavlu	Pavel	k1gMnSc3	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
zasvěcena	zasvěcen	k2eAgFnSc1d1	zasvěcena
kaple	kaple	k1gFnSc1	kaple
v	v	k7c6	v
prachatickém	prachatický	k2eAgInSc6d1	prachatický
hospicu	hospicus	k1gInSc6	hospicus
a	a	k8xC	a
od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kříži	kříž	k1gInSc6	kříž
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
umístěna	umístit	k5eAaPmNgFnS	umístit
relikvie	relikvie	k1gFnSc1	relikvie
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
krví	krev	k1gFnSc7	krev
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podnět	podnět	k1gInSc4	podnět
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
Jana	Jan	k1gMnSc2	Jan
Graubnera	Graubner	k1gMnSc2	Graubner
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
Janu	Jan	k1gMnSc3	Jan
Pavlu	Pavel	k1gMnSc3	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
zasvěcen	zasvětit	k5eAaPmNgInS	zasvětit
i	i	k9	i
nový	nový	k2eAgInSc1d1	nový
kostel	kostel	k1gInSc1	kostel
v	v	k7c6	v
Bukovanech	Bukovan	k1gMnPc6	Bukovan
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
původně	původně	k6eAd1	původně
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
zasvěcen	zasvětit	k5eAaPmNgInS	zasvětit
svatému	svatý	k1gMnSc3	svatý
Martinu	Martin	k1gMnSc3	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
zvolení	zvolení	k1gNnSc6	zvolení
papežem	papež	k1gMnSc7	papež
řekl	říct	k5eAaPmAgMnS	říct
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
kardinálům	kardinál	k1gMnPc3	kardinál
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ho	on	k3xPp3gMnSc4	on
zvolili	zvolit	k5eAaPmAgMnP	zvolit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc7	jeho
hlavní	hlavní	k2eAgFnSc7d1	hlavní
úlohou	úloha	k1gFnSc7	úloha
bude	být	k5eAaImBp3nS	být
zrealizovat	zrealizovat	k5eAaPmF	zrealizovat
učení	učení	k1gNnSc4	učení
Druhého	druhý	k4xOgInSc2	druhý
vatikánského	vatikánský	k2eAgInSc2d1	vatikánský
koncilu	koncil	k1gInSc2	koncil
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
všeobecné	všeobecný	k2eAgNnSc1d1	všeobecné
povolání	povolání	k1gNnSc1	povolání
ke	k	k7c3	k
svatosti	svatost	k1gFnSc3	svatost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
také	také	k9	také
stalo	stát	k5eAaPmAgNnS	stát
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
kanonizaci	kanonizace	k1gFnSc4	kanonizace
mnoha	mnoho	k4c2	mnoho
svatých	svatá	k1gFnPc2	svatá
vykonávajících	vykonávající	k2eAgFnPc2d1	vykonávající
různá	různý	k2eAgNnPc4d1	různé
povolání	povolání	k1gNnSc4	povolání
a	a	k8xC	a
podporu	podpora	k1gFnSc4	podpora
organizace	organizace	k1gFnSc2	organizace
Opus	opus	k1gInSc1	opus
Dei	Dei	k1gFnSc4	Dei
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
úlohou	úloha	k1gFnSc7	úloha
je	být	k5eAaImIp3nS	být
rozšířit	rozšířit	k5eAaPmF	rozšířit
toto	tento	k3xDgNnSc4	tento
povolání	povolání	k1gNnSc4	povolání
mezi	mezi	k7c4	mezi
laickou	laický	k2eAgFnSc4d1	laická
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ho	on	k3xPp3gMnSc4	on
také	také	k6eAd1	také
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
Katechismu	katechismus	k1gInSc2	katechismus
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
připraveného	připravený	k2eAgMnSc4d1	připravený
po	po	k7c6	po
Druhém	druhý	k4xOgInSc6	druhý
vatikánském	vatikánský	k2eAgInSc6d1	vatikánský
koncilu	koncil	k1gInSc6	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Vydání	vydání	k1gNnSc1	vydání
tohoto	tento	k3xDgInSc2	tento
katechismu	katechismus	k1gInSc2	katechismus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
základní	základní	k2eAgFnSc7d1	základní
učebnicí	učebnice	k1gFnSc7	učebnice
katolické	katolický	k2eAgFnSc2d1	katolická
nauky	nauka	k1gFnSc2	nauka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
bestsellerem	bestseller	k1gInSc7	bestseller
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
velkým	velký	k2eAgNnPc3d1	velké
úspěchem	úspěch	k1gInSc7	úspěch
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Záměrem	záměr	k1gInSc7	záměr
jeho	jeho	k3xOp3gNnSc2	jeho
vydání	vydání	k1gNnSc2	vydání
bylo	být	k5eAaImAgNnS	být
podle	podle	k7c2	podle
apoštolské	apoštolský	k2eAgFnSc2d1	apoštolská
konstituce	konstituce	k1gFnSc2	konstituce
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Fidei	Fidei	k6eAd1	Fidei
Depositum	depositum	k1gNnSc4	depositum
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1992	[number]	k4	1992
"	"	kIx"	"
<g/>
vyjádření	vyjádření	k1gNnSc4	vyjádření
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
učení	učení	k1gNnSc2	učení
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
potvrzení	potvrzení	k1gNnSc4	potvrzení
a	a	k8xC	a
objasnění	objasnění	k1gNnSc4	objasnění
Bible	bible	k1gFnSc2	bible
apoštolskou	apoštolský	k2eAgFnSc7d1	apoštolská
tradicí	tradice	k1gFnSc7	tradice
a	a	k8xC	a
církevní	církevní	k2eAgFnSc7d1	církevní
autoritou	autorita	k1gFnSc7	autorita
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Úmyslem	úmysl	k1gInSc7	úmysl
a	a	k8xC	a
cílem	cíl	k1gInSc7	cíl
Druhého	druhý	k4xOgInSc2	druhý
vatikánského	vatikánský	k2eAgInSc2d1	vatikánský
koncilu	koncil	k1gInSc2	koncil
bylo	být	k5eAaImAgNnS	být
přiměřeně	přiměřeně	k6eAd1	přiměřeně
objasnit	objasnit	k5eAaPmF	objasnit
apoštolské	apoštolský	k2eAgNnSc4d1	apoštolské
a	a	k8xC	a
pastorální	pastorální	k2eAgNnSc4d1	pastorální
poslání	poslání	k1gNnSc4	poslání
Církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
vhodněji	vhodně	k6eAd2	vhodně
vykládat	vykládat	k5eAaImF	vykládat
obsah	obsah	k1gInSc4	obsah
katolického	katolický	k2eAgNnSc2d1	katolické
učení	učení	k1gNnSc2	učení
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
přístupnější	přístupný	k2eAgInSc1d2	přístupnější
lidem	lid	k1gInSc7	lid
žijícím	žijící	k2eAgInSc7d1	žijící
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Katechismus	katechismus	k1gInSc1	katechismus
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
odpovídal	odpovídat	k5eAaImAgMnS	odpovídat
těmto	tento	k3xDgInPc3	tento
požadavkům	požadavek	k1gInPc3	požadavek
<g/>
,	,	kIx,	,
přebírá	přebírat	k5eAaImIp3nS	přebírat
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
starodávné	starodávný	k2eAgNnSc1d1	starodávné
zažité	zažitý	k2eAgNnSc1d1	zažité
uspořádání	uspořádání	k1gNnSc1	uspořádání
<g/>
,	,	kIx,	,
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
přidržel	přidržet	k5eAaPmAgMnS	přidržet
už	už	k9	už
katechismus	katechismus	k1gInSc4	katechismus
papeže	papež	k1gMnSc2	papež
Pia	Pius	k1gMnSc2	Pius
V.	V.	kA	V.
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
obsah	obsah	k1gInSc1	obsah
podává	podávat	k5eAaImIp3nS	podávat
novým	nový	k2eAgInSc7d1	nový
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odpovídal	odpovídat	k5eAaImAgMnS	odpovídat
na	na	k7c4	na
otázky	otázka	k1gFnPc4	otázka
nové	nový	k2eAgFnSc2d1	nová
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Vydání	vydání	k1gNnSc1	vydání
Katechismu	katechismus	k1gInSc2	katechismus
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
písemnostmi	písemnost	k1gFnPc7	písemnost
byl	být	k5eAaImAgInS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
krok	krok	k1gInSc1	krok
po	po	k7c6	po
zmatku	zmatek	k1gInSc6	zmatek
v	v	k7c6	v
učení	učení	k1gNnSc6	učení
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
přišel	přijít	k5eAaPmAgMnS	přijít
během	během	k7c2	během
krize	krize	k1gFnSc2	krize
po	po	k7c6	po
Druhém	druhý	k4xOgInSc6	druhý
vatikánském	vatikánský	k2eAgInSc6d1	vatikánský
koncilu	koncil	k1gInSc6	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
když	když	k8xS	když
stovky	stovka	k1gFnPc1	stovka
kněží	kněz	k1gMnPc2	kněz
<g/>
,	,	kIx,	,
řeholníků	řeholník	k1gMnPc2	řeholník
a	a	k8xC	a
věřících	věřící	k1gMnPc2	věřící
opustily	opustit	k5eAaPmAgInP	opustit
katolickou	katolický	k2eAgFnSc4d1	katolická
církev	církev	k1gFnSc4	církev
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
zvrátit	zvrátit	k5eAaPmF	zvrátit
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
napsal	napsat	k5eAaBmAgMnS	napsat
množství	množství	k1gNnSc4	množství
důležitých	důležitý	k2eAgInPc2d1	důležitý
dokumentů	dokument	k1gInPc2	dokument
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
podle	podle	k7c2	podle
mnohých	mnohý	k2eAgMnPc2d1	mnohý
pozorovatelů	pozorovatel	k1gMnPc2	pozorovatel
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
dlouhotrvající	dlouhotrvající	k2eAgInSc4d1	dlouhotrvající
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
církev	církev	k1gFnSc4	církev
a	a	k8xC	a
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
jako	jako	k9	jako
biskup	biskup	k1gMnSc1	biskup
<g/>
,	,	kIx,	,
Wojtyła	Wojtyła	k1gMnSc1	Wojtyła
publikoval	publikovat	k5eAaBmAgMnS	publikovat
vlivnou	vlivný	k2eAgFnSc4d1	vlivná
knihu	kniha	k1gFnSc4	kniha
Láska	láska	k1gFnSc1	láska
a	a	k8xC	a
zodpovědnost	zodpovědnost	k1gFnSc1	zodpovědnost
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
tradičního	tradiční	k2eAgNnSc2d1	tradiční
učení	učení	k1gNnSc2	učení
církve	církev	k1gFnSc2	církev
o	o	k7c6	o
sexu	sex	k1gInSc6	sex
a	a	k8xC	a
manželství	manželství	k1gNnSc1	manželství
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
filozofa	filozof	k1gMnSc2	filozof
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
při	při	k7c6	při
formulování	formulování	k1gNnSc6	formulování
encykliky	encyklika	k1gFnSc2	encyklika
papeže	papež	k1gMnSc2	papež
Pavla	Pavel	k1gMnSc2	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Humanae	Humanae	k1gFnSc1	Humanae
Vitae	Vitae	k1gFnSc1	Vitae
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zaobírá	zaobírat	k5eAaImIp3nS	zaobírat
stejnými	stejný	k2eAgFnPc7d1	stejná
otázkami	otázka	k1gFnPc7	otázka
a	a	k8xC	a
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
interrupce	interrupce	k1gFnPc4	interrupce
a	a	k8xC	a
umělou	umělý	k2eAgFnSc4d1	umělá
antikoncepci	antikoncepce	k1gFnSc4	antikoncepce
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějšími	důležitý	k2eAgInPc7d3	nejdůležitější
písemnými	písemný	k2eAgInPc7d1	písemný
dokumenty	dokument	k1gInPc7	dokument
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
papežové	papež	k1gMnPc1	papež
obracejí	obracet	k5eAaImIp3nP	obracet
na	na	k7c4	na
církev	církev	k1gFnSc4	církev
a	a	k8xC	a
věřící	věřící	k1gFnSc4	věřící
jsou	být	k5eAaImIp3nP	být
encykliky	encyklika	k1gFnPc1	encyklika
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
encykliky	encyklika	k1gFnPc1	encyklika
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
byly	být	k5eAaImAgFnP	být
zaměřené	zaměřený	k2eAgFnPc1d1	zaměřená
na	na	k7c4	na
trojjediného	trojjediný	k2eAgMnSc4d1	trojjediný
Boha	bůh	k1gMnSc4	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Úplně	úplně	k6eAd1	úplně
první	první	k4xOgFnSc1	první
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Encyklika	encyklika	k1gFnSc1	encyklika
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
svého	svůj	k3xOyFgInSc2	svůj
pontifikátu	pontifikát	k1gInSc2	pontifikát
obrací	obracet	k5eAaImIp3nS	obracet
na	na	k7c4	na
ctihodné	ctihodný	k2eAgMnPc4d1	ctihodný
bratry	bratr	k1gMnPc4	bratr
v	v	k7c6	v
biskupské	biskupský	k2eAgFnSc6d1	biskupská
službě	služba	k1gFnSc6	služba
<g/>
,	,	kIx,	,
na	na	k7c4	na
kněze	kněz	k1gMnPc4	kněz
a	a	k8xC	a
na	na	k7c4	na
řeholní	řeholní	k2eAgFnPc4d1	řeholní
rodiny	rodina	k1gFnPc4	rodina
<g/>
,	,	kIx,	,
na	na	k7c4	na
syny	syn	k1gMnPc4	syn
a	a	k8xC	a
dcery	dcera	k1gFnPc4	dcera
Církve	církev	k1gFnSc2	církev
a	a	k8xC	a
na	na	k7c4	na
všechny	všechen	k3xTgMnPc4	všechen
lidi	člověk	k1gMnPc4	člověk
dobré	dobrý	k2eAgFnSc2d1	dobrá
vůle	vůle	k1gFnSc2	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gNnSc7	její
ústředním	ústřední	k2eAgNnSc7d1	ústřední
tématem	téma	k1gNnSc7	téma
byl	být	k5eAaImAgMnS	být
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
<g/>
,	,	kIx,	,
Vykupitel	vykupitel	k1gMnSc1	vykupitel
(	(	kIx(	(
<g/>
Redemptor	Redemptor	k1gMnSc1	Redemptor
Hominis	Hominis	k1gFnSc2	Hominis
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
celého	celý	k2eAgNnSc2d1	celé
svého	své	k1gNnSc2	své
pontifikátu	pontifikát	k1gInSc2	pontifikát
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
zaměření	zaměření	k1gNnSc6	zaměření
na	na	k7c4	na
Boha	bůh	k1gMnSc4	bůh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
encyklice	encyklika	k1gFnSc6	encyklika
O	o	k7c6	o
některých	některý	k3yIgFnPc6	některý
základních	základní	k2eAgFnPc6d1	základní
otázkách	otázka	k1gFnPc6	otázka
morálního	morální	k2eAgNnSc2d1	morální
učení	učení	k1gNnSc3	učení
Církve	církev	k1gFnSc2	církev
(	(	kIx(	(
<g/>
Veritatis	Veritatis	k1gFnSc1	Veritatis
Splendor	Splendor	k1gMnSc1	Splendor
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejdůležitější	důležitý	k2eAgFnSc3d3	nejdůležitější
papežské	papežský	k2eAgFnSc3d1	Papežská
encyklice	encyklika	k1gFnSc3	encyklika
o	o	k7c6	o
morálce	morálka	k1gFnSc6	morálka
<g/>
,	,	kIx,	,
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
závislost	závislost	k1gFnSc4	závislost
člověka	člověk	k1gMnSc2	člověk
na	na	k7c6	na
Bohu	bůh	k1gMnSc6	bůh
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc6	jeho
zákonech	zákon	k1gInPc6	zákon
a	a	k8xC	a
závislost	závislost	k1gFnSc4	závislost
svobody	svoboda	k1gFnSc2	svoboda
na	na	k7c6	na
pravdě	pravda	k1gFnSc6	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Varoval	varovat	k5eAaImAgMnS	varovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
odevzdávající	odevzdávající	k2eAgMnSc1d1	odevzdávající
se	se	k3xPyFc4	se
relativismu	relativismus	k1gInSc2	relativismus
a	a	k8xC	a
skepticismu	skepticismus	k1gInSc2	skepticismus
se	se	k3xPyFc4	se
uchyluje	uchylovat	k5eAaImIp3nS	uchylovat
k	k	k7c3	k
hledání	hledání	k1gNnSc3	hledání
iluze	iluze	k1gFnSc2	iluze
svobody	svoboda	k1gFnSc2	svoboda
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
pravdy	pravda	k1gFnSc2	pravda
jako	jako	k8xS	jako
takové	takový	k3xDgFnSc2	takový
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dílech	díl	k1gInPc6	díl
také	také	k9	také
hodně	hodně	k6eAd1	hodně
věnoval	věnovat	k5eAaImAgInS	věnovat
dělníkům	dělník	k1gMnPc3	dělník
a	a	k8xC	a
sociální	sociální	k2eAgFnSc3d1	sociální
nauce	nauka	k1gFnSc3	nauka
církve	církev	k1gFnSc2	církev
-	-	kIx~	-
této	tento	k3xDgFnSc3	tento
problematice	problematika	k1gFnSc3	problematika
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
encyklikách	encyklika	k1gFnPc6	encyklika
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
Vatikán	Vatikán	k1gInSc4	Vatikán
vyzdvihl	vyzdvihnout	k5eAaPmAgMnS	vyzdvihnout
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
publikováním	publikování	k1gNnSc7	publikování
Přehledu	přehled	k1gInSc2	přehled
o	o	k7c6	o
sociální	sociální	k2eAgFnSc6d1	sociální
nauce	nauka	k1gFnSc6	nauka
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
encyklikách	encyklika	k1gFnPc6	encyklika
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
též	též	k9	též
hovořil	hovořit	k5eAaImAgMnS	hovořit
o	o	k7c6	o
důstojnosti	důstojnost	k1gFnSc6	důstojnost
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
důležitosti	důležitost	k1gFnSc2	důležitost
rodiny	rodina	k1gFnSc2	rodina
pro	pro	k7c4	pro
budoucnost	budoucnost	k1gFnSc4	budoucnost
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
důležité	důležitý	k2eAgFnPc1d1	důležitá
encykliky	encyklika	k1gFnPc1	encyklika
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
jsou	být	k5eAaImIp3nP	být
Encyklika	encyklika	k1gFnSc1	encyklika
o	o	k7c6	o
hodnotě	hodnota	k1gFnSc6	hodnota
a	a	k8xC	a
nenarušitelnosti	nenarušitelnost	k1gFnSc6	nenarušitelnost
lidského	lidský	k2eAgInSc2d1	lidský
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
Evangelium	evangelium	k1gNnSc1	evangelium
Vitae	Vita	k1gInSc2	Vita
<g/>
)	)	kIx)	)
a	a	k8xC	a
Encyklika	encyklika	k1gFnSc1	encyklika
o	o	k7c6	o
vztazích	vztah	k1gInPc6	vztah
mezi	mezi	k7c7	mezi
vírou	víra	k1gFnSc7	víra
a	a	k8xC	a
rozumem	rozum	k1gInSc7	rozum
(	(	kIx(	(
<g/>
Fides	Fides	k1gMnSc1	Fides
et	et	k?	et
Ratio	Ratio	k1gMnSc1	Ratio
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
hlavním	hlavní	k2eAgInSc6d1	hlavní
plánu	plán	k1gInSc6	plán
pro	pro	k7c4	pro
nové	nový	k2eAgNnSc4d1	nové
milénium	milénium	k1gNnSc4	milénium
<g/>
,	,	kIx,	,
vzpomínaném	vzpomínaný	k2eAgMnSc6d1	vzpomínaný
v	v	k7c6	v
apoštolském	apoštolský	k2eAgInSc6d1	apoštolský
listě	list	k1gInSc6	list
Na	na	k7c6	na
prahu	práh	k1gInSc6	práh
nového	nový	k2eAgNnSc2d1	nové
milénia	milénium	k1gNnSc2	milénium
<g/>
,	,	kIx,	,
Plán	plán	k1gInSc1	plán
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
časy	čas	k1gInPc4	čas
<g/>
,	,	kIx,	,
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
důležitost	důležitost	k1gFnSc4	důležitost
"	"	kIx"	"
<g/>
nového	nový	k2eAgInSc2d1	nový
začátku	začátek	k1gInSc2	začátek
od	od	k7c2	od
Krista	Kristus	k1gMnSc2	Kristus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Píše	psát	k5eAaImIp3nS	psát
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ne	ne	k9	ne
<g/>
,	,	kIx,	,
nezachrání	zachránit	k5eNaPmIp3nP	zachránit
nás	my	k3xPp1nPc4	my
slavné	slavný	k2eAgInPc1d1	slavný
návody	návod	k1gInPc1	návod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Osoba	osoba	k1gFnSc1	osoba
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tedy	tedy	k8xC	tedy
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
prioritou	priorita	k1gFnSc7	priorita
pro	pro	k7c4	pro
církev	církev	k1gFnSc4	církev
je	být	k5eAaImIp3nS	být
svatost	svatost	k1gFnSc1	svatost
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Všichni	všechen	k3xTgMnPc1	všechen
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
...	...	k?	...
jsou	být	k5eAaImIp3nP	být
povoláni	povolat	k5eAaPmNgMnP	povolat
k	k	k7c3	k
plnosti	plnost	k1gFnSc3	plnost
křesťanského	křesťanský	k2eAgInSc2d1	křesťanský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Také	také	k9	také
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
zásadní	zásadní	k2eAgInSc4d1	zásadní
význam	význam	k1gInSc4	význam
evangelia	evangelium	k1gNnSc2	evangelium
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
požadavky	požadavek	k1gInPc1	požadavek
by	by	kYmCp3nP	by
neměly	mít	k5eNaImAgInP	mít
být	být	k5eAaImF	být
oslabovány	oslabovat	k5eAaImNgInP	oslabovat
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Cvičení	cvičení	k1gNnSc1	cvičení
ve	v	k7c6	v
svatosti	svatost	k1gFnSc6	svatost
si	se	k3xPyFc3	se
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
křesťanský	křesťanský	k2eAgInSc1d1	křesťanský
život	život	k1gInSc1	život
vyznačující	vyznačující	k2eAgFnSc2d1	vyznačující
se	se	k3xPyFc4	se
především	především	k9	především
uměním	umění	k1gNnSc7	umění
modlitby	modlitba	k1gFnSc2	modlitba
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jeho	jeho	k3xOp3gFnSc1	jeho
poslední	poslední	k2eAgFnSc1d1	poslední
encyklika	encyklika	k1gFnSc1	encyklika
je	být	k5eAaImIp3nS	být
Encyklika	encyklika	k1gFnSc1	encyklika
o	o	k7c4	o
Eucharistii	eucharistie	k1gFnSc4	eucharistie
a	a	k8xC	a
jejím	její	k3xOp3gInSc6	její
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
Církvi	církev	k1gFnSc3	církev
(	(	kIx(	(
<g/>
Ecclesia	Ecclesia	k1gFnSc1	Ecclesia
de	de	k?	de
Eucharistia	Eucharistia	k1gFnSc1	Eucharistia
-	-	kIx~	-
17	[number]	k4	17
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
svaté	svatý	k2eAgFnSc6d1	svatá
Eucharistii	eucharistie	k1gFnSc6	eucharistie
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
celé	celý	k2eAgNnSc4d1	celé
církevní	církevní	k2eAgNnSc4d1	církevní
spirituální	spirituální	k2eAgNnSc4d1	spirituální
bohatství	bohatství	k1gNnSc4	bohatství
<g/>
:	:	kIx,	:
samotného	samotný	k2eAgMnSc2d1	samotný
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dále	daleko	k6eAd2	daleko
na	na	k7c6	na
základě	základ	k1gInSc6	základ
svého	svůj	k3xOyFgInSc2	svůj
hlavního	hlavní	k2eAgInSc2d1	hlavní
plánu	plán	k1gInSc2	plán
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
potřebu	potřeba	k1gFnSc4	potřeba
"	"	kIx"	"
<g/>
znovu	znovu	k6eAd1	znovu
zapálit	zapálit	k5eAaPmF	zapálit
úžas	úžas	k1gInSc4	úžas
<g/>
"	"	kIx"	"
z	z	k7c2	z
Eucharistie	eucharistie	k1gFnSc2	eucharistie
a	a	k8xC	a
"	"	kIx"	"
<g/>
rozjímat	rozjímat	k5eAaImF	rozjímat
před	před	k7c7	před
tváří	tvář	k1gFnSc7	tvář
Krista	Kristus	k1gMnSc2	Kristus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
považovali	považovat	k5eAaImAgMnP	považovat
jeho	jeho	k3xOp3gFnSc4	jeho
osobu	osoba	k1gFnSc4	osoba
za	za	k7c4	za
brzdu	brzda	k1gFnSc4	brzda
pokrokových	pokrokový	k2eAgFnPc2d1	pokroková
snah	snaha	k1gFnPc2	snaha
Druhého	druhý	k4xOgInSc2	druhý
vatikánského	vatikánský	k2eAgInSc2d1	vatikánský
koncilu	koncil	k1gInSc2	koncil
<g/>
;	;	kIx,	;
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
nich	on	k3xPp3gInPc2	on
vlajkovou	vlajkový	k2eAgFnSc4d1	vlajková
lodí	loď	k1gFnSc7	loď
pro	pro	k7c4	pro
konzervativce	konzervativec	k1gMnPc4	konzervativec
v	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
církvi	církev	k1gFnSc6	církev
<g/>
.	.	kIx.	.
</s>
<s>
Pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
neochvějné	neochvějný	k2eAgFnSc6d1	neochvějná
opozici	opozice	k1gFnSc6	opozice
vůči	vůči	k7c3	vůči
antikoncepci	antikoncepce	k1gFnSc3	antikoncepce
<g/>
,	,	kIx,	,
potratům	potrat	k1gInPc3	potrat
a	a	k8xC	a
homosexualitě	homosexualita	k1gFnSc6	homosexualita
<g/>
.	.	kIx.	.
</s>
<s>
Kontroverzním	kontroverzní	k2eAgInSc7d1	kontroverzní
bodem	bod	k1gInSc7	bod
pontifikátu	pontifikát	k1gInSc2	pontifikát
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
nich	on	k3xPp3gFnPc2	on
i	i	k9	i
jeho	jeho	k3xOp3gInSc4	jeho
list	list	k1gInSc4	list
všem	všecek	k3xTgMnPc3	všecek
biskupům	biskup	k1gMnPc3	biskup
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
popsal	popsat	k5eAaPmAgMnS	popsat
homosexualitu	homosexualita	k1gFnSc4	homosexualita
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
sklon	sklon	k1gInSc1	sklon
směřující	směřující	k2eAgInSc1d1	směřující
k	k	k7c3	k
intrinzickému	intrinzický	k2eAgNnSc3d1	intrinzický
morálnímu	morální	k2eAgNnSc3d1	morální
zlu	zlo	k1gNnSc3	zlo
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
objektivní	objektivní	k2eAgFnSc4d1	objektivní
(	(	kIx(	(
<g/>
zdravotní	zdravotní	k2eAgFnSc4d1	zdravotní
<g/>
)	)	kIx)	)
poruchu	porucha	k1gFnSc4	porucha
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
knize	kniha	k1gFnSc6	kniha
Paměť	paměť	k1gFnSc1	paměť
a	a	k8xC	a
identita	identita	k1gFnSc1	identita
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tlak	tlak	k1gInSc1	tlak
na	na	k7c4	na
homosexuální	homosexuální	k2eAgNnSc4d1	homosexuální
manželství	manželství	k1gNnSc4	manželství
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
částí	část	k1gFnSc7	část
"	"	kIx"	"
<g/>
nové	nový	k2eAgFnSc2d1	nová
ideologie	ideologie	k1gFnSc2	ideologie
zla	zlo	k1gNnSc2	zlo
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
pošlapat	pošlapat	k5eAaPmF	pošlapat
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
proti	proti	k7c3	proti
rodině	rodina	k1gFnSc3	rodina
a	a	k8xC	a
člověku	člověk	k1gMnSc3	člověk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
pontifikátu	pontifikát	k1gInSc2	pontifikát
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
vykonal	vykonat	k5eAaPmAgMnS	vykonat
víc	hodně	k6eAd2	hodně
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
cest	cesta	k1gFnPc2	cesta
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
100	[number]	k4	100
<g/>
)	)	kIx)	)
než	než	k8xS	než
všichni	všechen	k3xTgMnPc1	všechen
jeho	jeho	k3xOp3gMnPc1	jeho
předchůdci	předchůdce	k1gMnPc1	předchůdce
dohromady	dohromady	k6eAd1	dohromady
(	(	kIx(	(
<g/>
Prahu	Praha	k1gFnSc4	Praha
navštívil	navštívit	k5eAaPmAgMnS	navštívit
poprvé	poprvé	k6eAd1	poprvé
jako	jako	k9	jako
papež	papež	k1gMnSc1	papež
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sloužil	sloužit	k5eAaImAgMnS	sloužit
pontifikální	pontifikální	k2eAgFnSc4d1	pontifikální
mši	mše	k1gFnSc4	mše
na	na	k7c6	na
zcela	zcela	k6eAd1	zcela
zaplněné	zaplněný	k2eAgFnSc6d1	zaplněná
letenské	letenský	k2eAgFnSc6d1	Letenská
pláni	pláň	k1gFnSc6	pláň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
součtu	součet	k1gInSc6	součet
měřily	měřit	k5eAaImAgFnP	měřit
jeho	jeho	k3xOp3gFnPc1	jeho
cesty	cesta	k1gFnPc1	cesta
víc	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
1	[number]	k4	1
167	[number]	k4	167
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
jeho	jeho	k3xOp3gFnPc1	jeho
cesty	cesta	k1gFnPc1	cesta
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
například	například	k6eAd1	například
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
vedly	vést	k5eAaImAgFnP	vést
na	na	k7c6	na
místa	místo	k1gNnPc1	místo
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
už	už	k6eAd1	už
předtím	předtím	k6eAd1	předtím
navštívil	navštívit	k5eAaPmAgMnS	navštívit
papež	papež	k1gMnSc1	papež
Pavel	Pavel	k1gMnSc1	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
jiných	jiný	k1gMnPc2	jiný
vedlo	vést	k5eAaImAgNnS	vést
na	na	k7c4	na
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
žádný	žádný	k3yNgMnSc1	žádný
papež	papež	k1gMnSc1	papež
předtím	předtím	k6eAd1	předtím
nenavštívil	navštívit	k5eNaPmAgMnS	navštívit
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
prvním	první	k4xOgMnSc7	první
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
funkci	funkce	k1gFnSc6	funkce
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Spojené	spojený	k2eAgNnSc4d1	spojené
království	království	k1gNnSc4	království
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
královnou	královna	k1gFnSc7	královna
Alžbětou	Alžběta	k1gFnSc7	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
představitelkou	představitelka	k1gFnSc7	představitelka
anglikánské	anglikánský	k2eAgFnSc2d1	anglikánská
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
cesta	cesta	k1gFnSc1	cesta
byla	být	k5eAaImAgFnS	být
skoro	skoro	k6eAd1	skoro
zrušena	zrušit	k5eAaPmNgFnS	zrušit
kvůli	kvůli	k7c3	kvůli
válce	válka	k1gFnSc3	válka
o	o	k7c4	o
Falklandy	Falklanda	k1gFnPc4	Falklanda
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
které	který	k3yIgFnSc3	který
v	v	k7c6	v
době	doba	k1gFnSc6	doba
návštěvy	návštěva	k1gFnSc2	návštěva
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dramatickém	dramatický	k2eAgInSc6d1	dramatický
symbolickém	symbolický	k2eAgInSc6d1	symbolický
gestu	gest	k1gInSc6	gest
poklekl	pokleknout	k5eAaPmAgMnS	pokleknout
v	v	k7c6	v
modlitbě	modlitba	k1gFnSc6	modlitba
vedle	vedle	k7c2	vedle
canterburského	canterburský	k2eAgMnSc2d1	canterburský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
Roberta	Robert	k1gMnSc2	Robert
Runcia	Runcius	k1gMnSc2	Runcius
v	v	k7c6	v
Canterburské	Canterburský	k2eAgFnSc6d1	Canterburská
katedrále	katedrála	k1gFnSc6	katedrála
<g/>
,	,	kIx,	,
založené	založený	k2eAgNnSc1d1	založené
Augustýnem	Augustýn	k1gMnSc7	Augustýn
Canterburským	Canterburský	k2eAgMnSc7d1	Canterburský
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svých	svůj	k3xOyFgFnPc2	svůj
cest	cesta	k1gFnPc2	cesta
mnohokrát	mnohokrát	k6eAd1	mnohokrát
ukázal	ukázat	k5eAaPmAgMnS	ukázat
svoji	svůj	k3xOyFgFnSc4	svůj
oddanost	oddanost	k1gFnSc4	oddanost
Panně	Panna	k1gFnSc3	Panna
Marii	Maria	k1gFnSc3	Maria
<g/>
,	,	kIx,	,
když	když	k8xS	když
navštívil	navštívit	k5eAaPmAgMnS	navštívit
mnohé	mnohé	k1gNnSc4	mnohé
jí	on	k3xPp3gFnSc2	on
zasvěcené	zasvěcený	k2eAgInPc4d1	zasvěcený
chrámy	chrám	k1gInPc1	chrám
jako	jako	k8xC	jako
Knock	Knock	k1gInSc1	Knock
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
Fatima	Fatima	k1gFnSc1	Fatima
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
<g/>
,	,	kIx,	,
Guadalupe	Guadalup	k1gInSc5	Guadalup
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
a	a	k8xC	a
Lurdy	Lurd	k1gInPc4	Lurd
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc4	jeho
veřejná	veřejný	k2eAgNnPc4d1	veřejné
vystoupení	vystoupení	k1gNnPc4	vystoupení
přitahovala	přitahovat	k5eAaImAgFnS	přitahovat
masy	masa	k1gFnSc2	masa
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
ve	v	k7c4	v
Phoenix	Phoenix	k1gInSc4	Phoenix
Parku	park	k1gInSc2	park
v	v	k7c6	v
Dublinu	Dublin	k1gInSc6	Dublin
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
sešlo	sejít	k5eAaPmAgNnS	sejít
milion	milion	k4xCgInSc1	milion
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
celé	celý	k2eAgFnSc2d1	celá
populace	populace	k1gFnSc2	populace
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
se	se	k3xPyFc4	se
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Portoriko	Portoriko	k1gNnSc4	Portoriko
<g/>
.	.	kIx.	.
</s>
<s>
Speciálně	speciálně	k6eAd1	speciálně
kvůli	kvůli	k7c3	kvůli
němu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgFnP	být
vztyčeny	vztyčen	k2eAgFnPc1d1	vztyčena
zástavy	zástava	k1gFnPc1	zástava
na	na	k7c6	na
Mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
letišti	letiště	k1gNnSc6	letiště
Luise	Luisa	k1gFnSc3	Luisa
Munoza	Munoza	k1gFnSc1	Munoza
Marina	Marina	k1gFnSc1	Marina
v	v	k7c6	v
San	San	k1gMnSc6	San
Juanu	Juan	k1gMnSc6	Juan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
guvernérem	guvernér	k1gMnSc7	guvernér
Rafaelem	Rafael	k1gMnSc7	Rafael
Hernandezem	Hernandez	k1gInSc7	Hernandez
Colonem	colon	k1gNnSc7	colon
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Las	laso	k1gNnPc2	laso
Americas	Americasa	k1gFnPc2	Americasa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1990	[number]	k4	1990
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
papeže	papež	k1gMnSc2	papež
navštívil	navštívit	k5eAaPmAgMnS	navštívit
postkomunistické	postkomunistický	k2eAgNnSc4d1	postkomunistické
Československo	Československo	k1gNnSc4	Československo
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
Prahu	Praha	k1gFnSc4	Praha
<g/>
,	,	kIx,	,
Velehrad	Velehrad	k1gInSc4	Velehrad
a	a	k8xC	a
Bratislavu	Bratislava	k1gFnSc4	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jeho	jeho	k3xOp3gFnSc2	jeho
návštěvy	návštěva	k1gFnSc2	návštěva
Filipín	Filipíny	k1gFnPc2	Filipíny
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Manily	Manila	k1gFnSc2	Manila
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1995	[number]	k4	1995
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
spáchán	spáchat	k5eAaPmNgInS	spáchat
atentát	atentát	k1gInSc1	atentát
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
operace	operace	k1gFnSc1	operace
Bojinka	Bojinka	k1gFnSc1	Bojinka
<g/>
,	,	kIx,	,
masového	masový	k2eAgInSc2d1	masový
teroristického	teroristický	k2eAgInSc2d1	teroristický
útoku	útok	k1gInSc2	útok
naplánovaného	naplánovaný	k2eAgNnSc2d1	naplánované
členy	člen	k1gMnPc7	člen
Al-Kaidy	Al-Kaid	k1gInPc1	Al-Kaid
Ramzi	Ramze	k1gFnSc4	Ramze
Jusefem	Jusef	k1gInSc7	Jusef
a	a	k8xC	a
Chalídem	Chalíd	k1gMnSc7	Chalíd
Šejkem	šejk	k1gMnSc7	šejk
Mohammedem	Mohammed	k1gMnSc7	Mohammed
<g/>
.	.	kIx.	.
</s>
<s>
Sebevražedný	sebevražedný	k2eAgMnSc1d1	sebevražedný
atentátník	atentátník	k1gMnSc1	atentátník
převlečený	převlečený	k2eAgMnSc1d1	převlečený
za	za	k7c4	za
kněze	kněz	k1gMnPc4	kněz
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
podle	podle	k7c2	podle
plánu	plán	k1gInSc2	plán
odpálit	odpálit	k5eAaPmF	odpálit
v	v	k7c6	v
momentě	moment	k1gInSc6	moment
<g/>
,	,	kIx,	,
když	když	k8xS	když
bude	být	k5eAaImBp3nS	být
okolo	okolo	k6eAd1	okolo
projíždět	projíždět	k5eAaImF	projíždět
auto	auto	k1gNnSc4	auto
s	s	k7c7	s
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gInSc4	on
výbuch	výbuch	k1gInSc4	výbuch
usmrtil	usmrtit	k5eAaPmAgMnS	usmrtit
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
před	před	k7c7	před
plánovaným	plánovaný	k2eAgInSc7d1	plánovaný
útokem	útok	k1gInSc7	útok
se	se	k3xPyFc4	se
vyšetřovatelé	vyšetřovatel	k1gMnPc1	vyšetřovatel
vedení	vedený	k2eAgMnPc1d1	vedený
Aidou	Aida	k1gFnSc7	Aida
Fariscalem	Fariscal	k1gMnSc7	Fariscal
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
Jusefova	Jusefův	k2eAgInSc2d1	Jusefův
notebooku	notebook	k1gInSc2	notebook
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
našli	najít	k5eAaPmAgMnP	najít
plány	plán	k1gInPc4	plán
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
teroristický	teroristický	k2eAgInSc4d1	teroristický
útok	útok	k1gInSc4	útok
<g/>
.	.	kIx.	.
</s>
<s>
Jusef	Jusef	k1gInSc1	Jusef
byl	být	k5eAaImAgInS	být
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
zatčen	zatknout	k5eAaPmNgMnS	zatknout
v	v	k7c6	v
Pákistánu	Pákistán	k1gInSc6	Pákistán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Chalid	Chalid	k1gInSc1	Chalid
Šejk	šejk	k1gMnSc1	šejk
Muhammad	Muhammad	k1gInSc1	Muhammad
byl	být	k5eAaImAgMnS	být
zadržen	zadržet	k5eAaPmNgMnS	zadržet
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
filipínské	filipínský	k2eAgFnSc2d1	filipínská
cesty	cesta	k1gFnSc2	cesta
papež	papež	k1gMnSc1	papež
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1995	[number]	k4	1995
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
před	před	k7c7	před
shromážděním	shromáždění	k1gNnSc7	shromáždění
asi	asi	k9	asi
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
Luneta	luneta	k1gFnSc1	luneta
Park	park	k1gInSc1	park
v	v	k7c6	v
Manile	Manila	k1gFnSc6	Manila
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
navštívil	navštívit	k5eAaPmAgMnS	navštívit
i	i	k9	i
Olomouc	Olomouc	k1gFnSc4	Olomouc
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
při	při	k7c6	při
bohoslužbě	bohoslužba	k1gFnSc6	bohoslužba
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
asi	asi	k9	asi
stotisíc	stotisit	k5eAaImSgFnS	stotisit
věřících	věřící	k2eAgMnPc2d1	věřící
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
v	v	k7c6	v
Neředíně	Neředín	k1gInSc6	Neředín
svatořečil	svatořečit	k5eAaBmAgMnS	svatořečit
blahoslavenou	blahoslavený	k2eAgFnSc4d1	blahoslavená
Zdislavu	Zdislava	k1gFnSc4	Zdislava
z	z	k7c2	z
Lemberka	Lemberka	k1gFnSc1	Lemberka
a	a	k8xC	a
blahoslaveného	blahoslavený	k2eAgMnSc4d1	blahoslavený
Jana	Jan	k1gMnSc4	Jan
Sarkandera	Sarkander	k1gMnSc4	Sarkander
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
umučen	umučit	k5eAaPmNgMnS	umučit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1995	[number]	k4	1995
navštívil	navštívit	k5eAaPmAgMnS	navštívit
poutní	poutní	k2eAgNnSc4d1	poutní
místo	místo	k1gNnSc4	místo
Svatý	svatý	k2eAgMnSc1d1	svatý
Kopeček	Kopeček	k1gMnSc1	Kopeček
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
na	na	k7c6	na
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
mládeží	mládež	k1gFnSc7	mládež
Olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
a	a	k8xC	a
Pražské	pražský	k2eAgFnSc2d1	Pražská
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
(	(	kIx(	(
<g/>
v	v	k7c6	v
poutním	poutní	k2eAgInSc6d1	poutní
chrámu	chrám	k1gInSc6	chrám
poblíž	poblíž	k7c2	poblíž
monumentálního	monumentální	k2eAgInSc2d1	monumentální
barokního	barokní	k2eAgInSc2d1	barokní
jižního	jižní	k2eAgInSc2d1	jižní
oltáře	oltář	k1gInSc2	oltář
s	s	k7c7	s
ostatky	ostatek	k1gInPc7	ostatek
sv.	sv.	kA	sv.
Viktora	Viktor	k1gMnSc2	Viktor
<g/>
,	,	kIx,	,
na	na	k7c6	na
pilíři	pilíř	k1gInSc6	pilíř
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
neděle	neděle	k1gFnSc2	neděle
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2015	[number]	k4	2015
umístěn	umístěn	k2eAgInSc1d1	umístěn
relikviář	relikviář	k1gInSc1	relikviář
s	s	k7c7	s
ostatky	ostatek	k1gInPc7	ostatek
-	-	kIx~	-
krví-	krví-	k?	krví-
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
20	[number]	k4	20
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
jeho	jeho	k3xOp3gFnSc2	jeho
návštěvy	návštěva	k1gFnSc2	návštěva
této	tento	k3xDgFnSc2	tento
baziliky	bazilika	k1gFnSc2	bazilika
<g/>
,	,	kIx,	,
věnovaný	věnovaný	k2eAgInSc1d1	věnovaný
osobně	osobně	k6eAd1	osobně
Krakovským	krakovský	k2eAgMnSc7d1	krakovský
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
Stanislawem	Stanislaw	k1gMnSc7	Stanislaw
kardinálem	kardinál	k1gMnSc7	kardinál
Dziwiszem	Dziwisz	k1gMnSc7	Dziwisz
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
Svatého	svatý	k2eAgMnSc4d1	svatý
Otce	otec	k1gMnSc4	otec
na	na	k7c6	na
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
cestě	cesta	k1gFnSc6	cesta
také	také	k9	také
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
poutní	poutní	k2eAgNnSc4d1	poutní
místo	místo	k1gNnSc4	místo
navštívil	navštívit	k5eAaPmAgInS	navštívit
již	již	k6eAd1	již
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
po	po	k7c6	po
účasti	účast	k1gFnSc6	účast
na	na	k7c6	na
pohřbu	pohřeb	k1gInSc6	pohřeb
Štěpána	Štěpán	k1gMnSc2	Štěpán
kardinála	kardinál	k1gMnSc2	kardinál
Trochty	Trochty	k?	Trochty
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
jako	jako	k8xS	jako
Krakovský	krakovský	k2eAgInSc1d1	krakovský
arcibiskup-	arcibiskup-	k?	arcibiskup-
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Rumunsko	Rumunsko	k1gNnSc4	Rumunsko
a	a	k8xC	a
setkal	setkat	k5eAaPmAgMnS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
místními	místní	k2eAgMnPc7d1	místní
představiteli	představitel	k1gMnPc7	představitel
ortodoxní	ortodoxní	k2eAgFnSc2d1	ortodoxní
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
prvním	první	k4xOgMnSc7	první
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
navštívil	navštívit	k5eAaPmAgMnS	navštívit
stát	stát	k5eAaImF	stát
s	s	k7c7	s
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
převážně	převážně	k6eAd1	převážně
se	se	k3xPyFc4	se
hlásícím	hlásící	k2eAgFnPc3d1	hlásící
k	k	k7c3	k
ortodoxní	ortodoxní	k2eAgFnSc3d1	ortodoxní
církvi	církev	k1gFnSc3	církev
od	od	k7c2	od
časů	čas	k1gInPc2	čas
Velkého	velký	k2eAgNnSc2d1	velké
schizmatu	schizma	k1gNnSc2	schizma
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
1054	[number]	k4	1054
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
i	i	k9	i
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
početných	početný	k2eAgFnPc2d1	početná
návštěv	návštěva	k1gFnPc2	návštěva
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
tentokrát	tentokrát	k6eAd1	tentokrát
celebroval	celebrovat	k5eAaImAgMnS	celebrovat
mši	mše	k1gFnSc4	mše
v	v	k7c6	v
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc2	tento
mše	mše	k1gFnSc2	mše
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
104	[number]	k4	104
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
největší	veliký	k2eAgNnSc1d3	veliký
shromáždění	shromáždění	k1gNnSc1	shromáždění
uvnitř	uvnitř	k7c2	uvnitř
budovy	budova	k1gFnSc2	budova
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
katolickým	katolický	k2eAgMnSc7d1	katolický
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Egypt	Egypt	k1gInSc4	Egypt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
koptským	koptský	k2eAgMnSc7d1	koptský
papežem	papež	k1gMnSc7	papež
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2001	[number]	k4	2001
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
cestu	cesta	k1gFnSc4	cesta
po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
po	po	k7c6	po
Středozemí	středozemí	k1gNnSc6	středozemí
<g/>
.	.	kIx.	.
</s>
<s>
Cestoval	cestovat	k5eAaImAgMnS	cestovat
z	z	k7c2	z
Řecka	Řecko	k1gNnSc2	Řecko
do	do	k7c2	do
Sýrie	Sýrie	k1gFnSc2	Sýrie
a	a	k8xC	a
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Malta	Malta	k1gFnSc1	Malta
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
cesty	cesta	k1gFnSc2	cesta
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
římskokatolickým	římskokatolický	k2eAgMnSc7d1	římskokatolický
papežem	papež	k1gMnSc7	papež
po	po	k7c6	po
tisíci	tisíc	k4xCgInSc6	tisíc
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Řecko	Řecko	k1gNnSc4	Řecko
<g/>
,	,	kIx,	,
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
prvním	první	k4xOgInSc6	první
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
islámské	islámský	k2eAgFnSc2d1	islámská
mešity	mešita	k1gFnSc2	mešita
v	v	k7c6	v
Damašku	Damašek	k1gInSc6	Damašek
<g/>
.	.	kIx.	.
</s>
<s>
Navštívil	navštívit	k5eAaPmAgInS	navštívit
mešitu	mešita	k1gFnSc4	mešita
Umajjovců	Umajjovec	k1gInPc2	Umajjovec
<g/>
,	,	kIx,	,
o	o	k7c6	o
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
pochovaný	pochovaný	k2eAgMnSc1d1	pochovaný
i	i	k9	i
Jan	Jan	k1gMnSc1	Jan
Křtitel	křtitel	k1gMnSc1	křtitel
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
velmi	velmi	k6eAd1	velmi
intenzivně	intenzivně	k6eAd1	intenzivně
cestoval	cestovat	k5eAaImAgMnS	cestovat
a	a	k8xC	a
přicházel	přicházet	k5eAaImAgMnS	přicházet
do	do	k7c2	do
styku	styk	k1gInSc2	styk
s	s	k7c7	s
různými	různý	k2eAgNnPc7d1	různé
jinými	jiný	k2eAgNnPc7d1	jiné
náboženstvími	náboženství	k1gNnPc7	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
snažil	snažit	k5eAaImAgMnS	snažit
najít	najít	k5eAaPmF	najít
společný	společný	k2eAgInSc4d1	společný
základ	základ	k1gInSc4	základ
ať	ať	k8xS	ať
už	už	k6eAd1	už
v	v	k7c6	v
doktrínách	doktrína	k1gFnPc6	doktrína
anebo	anebo	k8xC	anebo
dogmatech	dogma	k1gNnPc6	dogma
<g/>
.	.	kIx.	.
</s>
<s>
Zapsal	zapsat	k5eAaPmAgInS	zapsat
se	se	k3xPyFc4	se
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
svými	svůj	k3xOyFgInPc7	svůj
kontakty	kontakt	k1gInPc7	kontakt
s	s	k7c7	s
Izraelem	Izrael	k1gInSc7	Izrael
<g/>
,	,	kIx,	,
modlitbou	modlitba	k1gFnSc7	modlitba
u	u	k7c2	u
Zdi	zeď	k1gFnSc2	zeď
nářků	nářek	k1gInPc2	nářek
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
římskokatolickým	římskokatolický	k2eAgMnSc7d1	římskokatolický
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
navštívil	navštívit	k5eAaPmAgMnS	navštívit
islámskou	islámský	k2eAgFnSc4d1	islámská
mešitu	mešita	k1gFnSc4	mešita
<g/>
.	.	kIx.	.
</s>
<s>
Dalajlama	dalajlama	k1gMnSc1	dalajlama
<g/>
,	,	kIx,	,
duchovní	duchovní	k2eAgMnSc1d1	duchovní
vůdce	vůdce	k1gMnSc1	vůdce
Tibeťanů	Tibeťan	k1gMnPc2	Tibeťan
<g/>
,	,	kIx,	,
navštívil	navštívit	k5eAaPmAgMnS	navštívit
papeže	papež	k1gMnSc4	papež
8	[number]	k4	8
<g/>
krát	krát	k6eAd1	krát
<g/>
,	,	kIx,	,
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
kterýkoliv	kterýkoliv	k3yIgMnSc1	kterýkoliv
jiný	jiný	k2eAgMnSc1d1	jiný
hodnostář	hodnostář	k1gMnSc1	hodnostář
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
a	a	k8xC	a
dalajlama	dalajlama	k1gMnSc1	dalajlama
často	často	k6eAd1	často
sdíleli	sdílet	k5eAaImAgMnP	sdílet
podobné	podobný	k2eAgInPc4d1	podobný
názory	názor	k1gInPc4	názor
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
oba	dva	k4xCgMnPc1	dva
vzešli	vzejít	k5eAaPmAgMnP	vzejít
z	z	k7c2	z
lidí	člověk	k1gMnPc2	člověk
trpících	trpící	k2eAgFnPc2d1	trpící
pod	pod	k7c7	pod
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
nadvládou	nadvláda	k1gFnSc7	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
i	i	k8xC	i
dalajlama	dalajlama	k1gMnSc1	dalajlama
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
osobám	osoba	k1gFnPc3	osoba
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nesou	nést	k5eAaImIp3nP	nést
titul	titul	k1gInSc4	titul
"	"	kIx"	"
<g/>
Jeho	jeho	k3xOp3gFnSc1	jeho
svatost	svatost	k1gFnSc1	svatost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Vztah	vztah	k1gInSc1	vztah
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
k	k	k7c3	k
Židům	Žid	k1gMnPc3	Žid
se	se	k3xPyFc4	se
během	běh	k1gInSc7	běh
pontifikátu	pontifikát	k1gInSc2	pontifikát
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
zlepšil	zlepšit	k5eAaPmAgMnS	zlepšit
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
hovořil	hovořit	k5eAaImAgMnS	hovořit
o	o	k7c6	o
vztahu	vztah	k1gInSc6	vztah
církví	církev	k1gFnPc2	církev
k	k	k7c3	k
Židům	Žid	k1gMnPc3	Žid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
navštívil	navštívit	k5eAaPmAgMnS	navštívit
koncentrační	koncentrační	k2eAgInSc4d1	koncentrační
tábor	tábor	k1gInSc4	tábor
Osvětim	Osvětim	k1gFnSc4	Osvětim
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
novodobý	novodobý	k2eAgMnSc1d1	novodobý
papež	papež	k1gMnSc1	papež
navštívil	navštívit	k5eAaPmAgMnS	navštívit
synagogu	synagoga	k1gFnSc4	synagoga
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
synagogu	synagoga	k1gFnSc4	synagoga
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2000	[number]	k4	2000
papež	papež	k1gMnSc1	papež
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
navštívil	navštívit	k5eAaPmAgMnS	navštívit
památník	památník	k1gInSc4	památník
holokaustu	holokaust	k1gInSc2	holokaust
Jad	Jad	k1gFnSc2	Jad
vašem	váš	k3xOp2gNnSc6	váš
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
a	a	k8xC	a
dotkl	dotknout	k5eAaPmAgMnS	dotknout
se	se	k3xPyFc4	se
toho	ten	k3xDgNnSc2	ten
pro	pro	k7c4	pro
Židy	Žid	k1gMnPc4	Žid
nejposvátnějšího	posvátný	k2eAgNnSc2d3	nejposvátnější
<g/>
,	,	kIx,	,
Zdi	zeď	k1gFnPc1	zeď
nářků	nářek	k1gInPc2	nářek
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2003	[number]	k4	2003
Anti-Defamation	Anti-Defamation	k1gInSc1	Anti-Defamation
League	Leagu	k1gFnSc2	Leagu
(	(	kIx(	(
<g/>
ADL	ADL	kA	ADL
<g/>
)	)	kIx)	)
pogratulovala	pogratulovat	k5eAaPmAgFnS	pogratulovat
papeži	papež	k1gMnSc3	papež
k	k	k7c3	k
vstupu	vstup	k1gInSc3	vstup
do	do	k7c2	do
25	[number]	k4	25
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
jeho	jeho	k3xOp3gNnPc2	jeho
papežství	papežství	k1gNnPc2	papežství
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2005	[number]	k4	2005
se	se	k3xPyFc4	se
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
posledním	poslední	k2eAgNnSc6d1	poslední
veřejném	veřejný	k2eAgNnSc6d1	veřejné
setkání	setkání	k1gNnSc6	setkání
sešel	sejít	k5eAaPmAgMnS	sejít
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
141	[number]	k4	141
židovských	židovský	k2eAgMnPc2d1	židovský
představitelů	představitel	k1gMnPc2	představitel
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Clementine	Clementin	k1gInSc5	Clementin
Hall	Hall	k1gInSc4	Hall
Apoštolského	apoštolský	k2eAgInSc2d1	apoštolský
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mu	on	k3xPp3gMnSc3	on
poděkovali	poděkovat	k5eAaPmAgMnP	poděkovat
za	za	k7c4	za
všechno	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
udělal	udělat	k5eAaPmAgMnS	udělat
pro	pro	k7c4	pro
židovskou	židovský	k2eAgFnSc4d1	židovská
komunitu	komunita	k1gFnSc4	komunita
a	a	k8xC	a
pro	pro	k7c4	pro
Izrael	Izrael	k1gInSc4	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Gary	Gara	k1gFnPc1	Gara
Krupp	Krupp	k1gMnSc1	Krupp
<g/>
,	,	kIx,	,
sedmý	sedmý	k4xOgMnSc1	sedmý
žid	žid	k1gMnSc1	žid
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
pasoval	pasovat	k5eAaBmAgMnS	pasovat
na	na	k7c4	na
rytíře	rytíř	k1gMnSc4	rytíř
Řádu	řád	k1gInSc2	řád
sv.	sv.	kA	sv.
Řehoře	Řehoř	k1gMnSc2	Řehoř
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
,	,	kIx,	,
osobně	osobně	k6eAd1	osobně
poděkoval	poděkovat	k5eAaPmAgMnS	poděkovat
papežovi	papež	k1gMnSc3	papež
za	za	k7c4	za
podporu	podpora	k1gFnSc4	podpora
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2005	[number]	k4	2005
ADL	ADL	kA	ADL
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
započal	započnout	k5eAaPmAgInS	započnout
revoluci	revoluce	k1gFnSc4	revoluce
v	v	k7c6	v
katolicko-židovských	katolicko-židovský	k2eAgFnPc6d1	katolicko-židovský
<g />
.	.	kIx.	.
</s>
<s>
vztazích	vztah	k1gInPc6	vztah
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
27	[number]	k4	27
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
více	hodně	k6eAd2	hodně
změn	změna	k1gFnPc2	změna
než	než	k8xS	než
za	za	k7c4	za
2000	[number]	k4	2000
let	léto	k1gNnPc2	léto
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
I	i	k9	i
když	když	k8xS	když
množství	množství	k1gNnSc1	množství
témat	téma	k1gNnPc2	téma
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
Židy	Žid	k1gMnPc4	Žid
a	a	k8xC	a
Vatikán	Vatikán	k1gInSc4	Vatikán
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
za	za	k7c2	za
posledních	poslední	k2eAgInPc2d1	poslední
čtyřicet	čtyřicet	k4xCc4	čtyřicet
roků	rok	k1gInPc2	rok
výrazně	výrazně	k6eAd1	výrazně
snížilo	snížit	k5eAaPmAgNnS	snížit
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
existují	existovat	k5eAaImIp3nP	existovat
témata	téma	k1gNnPc1	téma
pro	pro	k7c4	pro
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
diskusi	diskuse	k1gFnSc4	diskuse
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k8xS	jak
v	v	k7c6	v
doktrinálních	doktrinální	k2eAgFnPc6d1	doktrinální
otázkách	otázka	k1gFnPc6	otázka
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
postoje	postoj	k1gInSc2	postoj
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
měsíci	měsíc	k1gInSc6	měsíc
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
vydána	vydán	k2eAgFnSc1d1	vydána
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
poštovní	poštovní	k2eAgFnSc1d1	poštovní
známka	známka	k1gFnSc1	známka
o	o	k7c6	o
nominální	nominální	k2eAgFnSc6d1	nominální
hodnotě	hodnota	k1gFnSc6	hodnota
3,8	[number]	k4	3,8
šekelů	šekel	k1gInPc2	šekel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1999	[number]	k4	1999
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Rumunsko	Rumunsko	k1gNnSc4	Rumunsko
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
Jeho	jeho	k3xOp3gFnSc2	jeho
Blaženosti	blaženost	k1gFnSc2	blaženost
Teoctista	Teoctista	k1gMnSc1	Teoctista
<g/>
,	,	kIx,	,
patriarchy	patriarcha	k1gMnSc2	patriarcha
rumunské	rumunský	k2eAgFnSc2d1	rumunská
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
Velkého	velký	k2eAgNnSc2d1	velké
schizmatu	schizma	k1gNnSc2	schizma
(	(	kIx(	(
<g/>
události	událost	k1gFnSc2	událost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1054	[number]	k4	1054
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
církev	církev	k1gFnSc1	církev
na	na	k7c4	na
východní	východní	k2eAgFnSc4d1	východní
pravoslavnou	pravoslavný	k2eAgFnSc4d1	pravoslavná
a	a	k8xC	a
západní	západní	k2eAgFnSc4d1	západní
římskokatolickou	římskokatolický	k2eAgFnSc4d1	Římskokatolická
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
papež	papež	k1gMnSc1	papež
navštívil	navštívit	k5eAaPmAgMnS	navštívit
stát	stát	k5eAaImF	stát
s	s	k7c7	s
převažující	převažující	k2eAgFnSc7d1	převažující
částí	část	k1gFnSc7	část
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
hlásícího	hlásící	k2eAgInSc2d1	hlásící
se	se	k3xPyFc4	se
k	k	k7c3	k
východní	východní	k2eAgFnSc3d1	východní
pravoslavné	pravoslavný	k2eAgFnSc3d1	pravoslavná
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Papeže	Papež	k1gMnPc4	Papež
při	při	k7c6	při
příchodu	příchod	k1gInSc6	příchod
přivítal	přivítat	k5eAaPmAgMnS	přivítat
patriarcha	patriarcha	k1gMnSc1	patriarcha
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
Emilem	Emil	k1gMnSc7	Emil
Constantinescem	Constantinesce	k1gMnSc7	Constantinesce
<g/>
.	.	kIx.	.
</s>
<s>
Patriarcha	patriarcha	k1gMnSc1	patriarcha
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
druhé	druhý	k4xOgNnSc1	druhý
milénium	milénium	k1gNnSc1	milénium
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
historie	historie	k1gFnSc2	historie
začalo	začít	k5eAaPmAgNnS	začít
bolestivou	bolestivý	k2eAgFnSc4d1	bolestivá
ranou	raný	k2eAgFnSc4d1	raná
jednotě	jednota	k1gFnSc6	jednota
církvi	církev	k1gFnSc6	církev
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
tohoto	tento	k3xDgNnSc2	tento
milénia	milénium	k1gNnSc2	milénium
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
reálnou	reálný	k2eAgFnSc4d1	reálná
snahu	snaha	k1gFnSc4	snaha
tuto	tento	k3xDgFnSc4	tento
jednotu	jednota	k1gFnSc4	jednota
obnovit	obnovit	k5eAaPmF	obnovit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
papež	papež	k1gMnSc1	papež
i	i	k8xC	i
patriarcha	patriarcha	k1gMnSc1	patriarcha
navštívili	navštívit	k5eAaPmAgMnP	navštívit
společně	společně	k6eAd1	společně
bohoslužby	bohoslužba	k1gFnPc4	bohoslužba
celebrované	celebrovaný	k2eAgFnPc4d1	celebrovaná
tím	ten	k3xDgNnSc7	ten
druhým	druhý	k4xOgNnSc7	druhý
(	(	kIx(	(
<g/>
pravoslavnou	pravoslavný	k2eAgFnSc4d1	pravoslavná
liturgii	liturgie	k1gFnSc4	liturgie
a	a	k8xC	a
katolickou	katolický	k2eAgFnSc4d1	katolická
mši	mše	k1gFnSc4	mše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dav	Dav	k1gInSc4	Dav
stovek	stovka	k1gFnPc2	stovka
tisíců	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
navštívil	navštívit	k5eAaPmAgMnS	navštívit
tyto	tento	k3xDgFnPc4	tento
bohoslužby	bohoslužba	k1gFnPc4	bohoslužba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
na	na	k7c6	na
otevřeném	otevřený	k2eAgNnSc6d1	otevřené
prostranství	prostranství	k1gNnSc6	prostranství
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
řekl	říct	k5eAaPmAgMnS	říct
shromážděným	shromážděný	k2eAgMnSc7d1	shromážděný
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jsem	být	k5eAaImIp1nS	být
tu	tu	k6eAd1	tu
mezi	mezi	k7c7	mezi
vámi	vy	k3xPp2nPc7	vy
s	s	k7c7	s
touhou	touha	k1gFnSc7	touha
po	po	k7c6	po
skutečné	skutečný	k2eAgFnSc6d1	skutečná
jednotě	jednota	k1gFnSc6	jednota
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
dávno	dávno	k6eAd1	dávno
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
bylo	být	k5eAaImAgNnS	být
nemyslitelné	myslitelný	k2eNgNnSc1d1	nemyslitelné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
římský	římský	k2eAgInSc1d1	římský
biskup	biskup	k1gInSc1	biskup
mohl	moct	k5eAaImAgInS	moct
navštívit	navštívit	k5eAaPmF	navštívit
své	svůj	k3xOyFgMnPc4	svůj
rumunské	rumunský	k2eAgMnPc4d1	rumunský
bratry	bratr	k1gMnPc4	bratr
a	a	k8xC	a
sestry	sestra	k1gFnPc4	sestra
ve	v	k7c6	v
víře	víra	k1gFnSc6	víra
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
zimě	zima	k1gFnSc6	zima
utrpení	utrpení	k1gNnSc2	utrpení
a	a	k8xC	a
útlaku	útlak	k1gInSc2	útlak
si	se	k3xPyFc3	se
konečně	konečně	k6eAd1	konečně
můžeme	moct	k5eAaImIp1nP	moct
vyměnit	vyměnit	k5eAaPmF	vyměnit
polibky	polibek	k1gInPc4	polibek
míru	mír	k1gInSc2	mír
a	a	k8xC	a
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
modlit	modlit	k5eAaImF	modlit
k	k	k7c3	k
Pánu	pán	k1gMnSc3	pán
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Velká	velký	k2eAgFnSc1d1	velká
skupina	skupina	k1gFnSc1	skupina
rumunských	rumunský	k2eAgMnPc2d1	rumunský
pravoslavných	pravoslavný	k2eAgMnPc2d1	pravoslavný
věřících	věřící	k1gMnPc2	věřící
ukázala	ukázat	k5eAaPmAgFnS	ukázat
podporu	podpora	k1gFnSc4	podpora
myšlence	myšlenka	k1gFnSc3	myšlenka
znovusjednocení	znovusjednocení	k1gNnSc6	znovusjednocení
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
papežem	papež	k1gMnSc7	papež
po	po	k7c6	po
1291	[number]	k4	1291
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Řecko	Řecko	k1gNnSc4	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Návštěva	návštěva	k1gFnSc1	návštěva
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
kontroverze	kontroverze	k1gFnPc4	kontroverze
a	a	k8xC	a
papež	papež	k1gMnSc1	papež
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
protesty	protest	k1gInPc7	protest
a	a	k8xC	a
ignorováním	ignorování	k1gNnSc7	ignorování
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
čelných	čelný	k2eAgMnPc2d1	čelný
představitelů	představitel	k1gMnPc2	představitel
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
ho	on	k3xPp3gNnSc4	on
nepřišel	přijít	k5eNaPmAgMnS	přijít
přivítat	přivítat	k5eAaPmF	přivítat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Aténách	Atény	k1gFnPc6	Atény
se	se	k3xPyFc4	se
papež	papež	k1gMnSc1	papež
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
Christodulem	Christodul	k1gMnSc7	Christodul
<g/>
,	,	kIx,	,
hlavou	hlava	k1gFnSc7	hlava
řecké	řecký	k2eAgFnSc2d1	řecká
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
soukromém	soukromý	k2eAgNnSc6d1	soukromé
třicetiminutovém	třicetiminutový	k2eAgNnSc6d1	třicetiminutové
setkání	setkání	k1gNnSc6	setkání
oba	dva	k4xCgMnPc1	dva
dva	dva	k4xCgMnPc1	dva
veřejně	veřejně	k6eAd1	veřejně
promluvili	promluvit	k5eAaPmAgMnP	promluvit
<g/>
.	.	kIx.	.
</s>
<s>
Christodoulos	Christodoulos	k1gMnSc1	Christodoulos
předčítal	předčítat	k5eAaImAgMnS	předčítat
list	list	k1gInSc4	list
"	"	kIx"	"
<g/>
13	[number]	k4	13
prohřešků	prohřešek	k1gInPc2	prohřešek
<g/>
"	"	kIx"	"
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
proti	proti	k7c3	proti
pravoslavné	pravoslavný	k2eAgFnSc3d1	pravoslavná
církvi	církev	k1gFnSc3	církev
od	od	k7c2	od
Velkého	velký	k2eAgNnSc2d1	velké
schizmatu	schizma	k1gNnSc2	schizma
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
drancování	drancování	k1gNnSc2	drancování
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
křižáky	křižák	k1gInPc4	křižák
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1204	[number]	k4	1204
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Až	až	k9	až
dosud	dosud	k6eAd1	dosud
jsme	být	k5eAaImIp1nP	být
neslyšeli	slyšet	k5eNaImAgMnP	slyšet
ani	ani	k8xC	ani
jedno	jeden	k4xCgNnSc1	jeden
slovo	slovo	k1gNnSc1	slovo
omluvy	omluva	k1gFnSc2	omluva
<g/>
"	"	kIx"	"
za	za	k7c4	za
"	"	kIx"	"
<g/>
nepříčetné	příčetný	k2eNgInPc4d1	nepříčetný
křižáky	křižák	k1gInPc4	křižák
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Za	za	k7c4	za
případy	případ	k1gInPc4	případ
minulé	minulý	k2eAgFnSc2d1	minulá
a	a	k8xC	a
současné	současný	k2eAgFnSc2d1	současná
<g/>
,	,	kIx,	,
když	když	k8xS	když
synové	syn	k1gMnPc1	syn
a	a	k8xC	a
dcery	dcera	k1gFnPc1	dcera
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
páchali	páchat	k5eAaImAgMnP	páchat
hříchy	hřích	k1gInPc4	hřích
na	na	k7c6	na
svých	svůj	k3xOyFgMnPc6	svůj
bratrech	bratr	k1gMnPc6	bratr
a	a	k8xC	a
sestrách	sestra	k1gFnPc6	sestra
z	z	k7c2	z
pravoslavných	pravoslavný	k2eAgFnPc2d1	pravoslavná
církví	církev	k1gFnPc2	církev
<g/>
,	,	kIx,	,
doufáme	doufat	k5eAaImIp1nP	doufat
v	v	k7c4	v
Boží	boží	k2eAgNnSc4d1	boží
odpuštění	odpuštění	k1gNnSc4	odpuštění
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
za	za	k7c4	za
což	což	k3yQnSc4	což
mu	on	k3xPp3gMnSc3	on
Christodoulos	Christodoulos	k1gMnSc1	Christodoulos
okamžitě	okamžitě	k6eAd1	okamžitě
zatleskal	zatleskat	k5eAaPmAgMnS	zatleskat
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
též	též	k9	též
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyplenění	vyplenění	k1gNnSc1	vyplenění
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
katolíky	katolík	k1gMnPc4	katolík
zdrojem	zdroj	k1gInSc7	zdroj
"	"	kIx"	"
<g/>
hluboké	hluboký	k2eAgFnPc4d1	hluboká
lítosti	lítost	k1gFnPc4	lítost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Christodoulos	Christodoulos	k1gInSc4	Christodoulos
setkali	setkat	k5eAaPmAgMnP	setkat
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
svatý	svatý	k2eAgMnSc1d1	svatý
Pavel	Pavel	k1gMnSc1	Pavel
udělil	udělit	k5eAaPmAgMnS	udělit
požehnání	požehnání	k1gNnSc4	požehnání
aténským	aténský	k2eAgMnSc7d1	aténský
křesťanům	křesťan	k1gMnPc3	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Vydali	vydat	k5eAaPmAgMnP	vydat
společnou	společný	k2eAgFnSc4d1	společná
deklaraci	deklarace	k1gFnSc4	deklarace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
říká	říkat	k5eAaImIp3nS	říkat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Musíme	muset	k5eAaImIp1nP	muset
udělat	udělat	k5eAaPmF	udělat
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
silách	síla	k1gFnPc6	síla
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
společné	společný	k2eAgInPc1d1	společný
křesťanské	křesťanský	k2eAgInPc1d1	křesťanský
kořeny	kořen	k1gInPc1	kořen
a	a	k8xC	a
duše	duše	k1gFnPc1	duše
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
zachovány	zachován	k2eAgFnPc1d1	zachována
<g/>
...	...	k?	...
Odsuzujeme	odsuzovat	k5eAaImIp1nP	odsuzovat
všechny	všechen	k3xTgInPc4	všechen
projevy	projev	k1gInPc4	projev
násilí	násilí	k1gNnSc2	násilí
a	a	k8xC	a
fanatismu	fanatismus	k1gInSc2	fanatismus
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Oba	dva	k4xCgMnPc1	dva
dva	dva	k4xCgMnPc4	dva
představitelé	představitel	k1gMnPc1	představitel
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
společně	společně	k6eAd1	společně
pomodlili	pomodlit	k5eAaPmAgMnP	pomodlit
k	k	k7c3	k
Pánu	pán	k1gMnSc3	pán
<g/>
,	,	kIx,	,
prolomivše	prolomit	k5eAaPmDgFnP	prolomit
tak	tak	k9	tak
pravoslavné	pravoslavný	k2eAgNnSc1d1	pravoslavné
tabu	tabu	k1gNnSc1	tabu
modlení	modlení	k1gNnSc2	modlení
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
katolíky	katolík	k1gMnPc7	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
se	se	k3xPyFc4	se
během	během	k7c2	během
návštěvy	návštěva	k1gFnSc2	návštěva
vyhnul	vyhnout	k5eAaPmAgInS	vyhnout
zmínce	zmínka	k1gFnSc3	zmínka
o	o	k7c6	o
Kypru	Kypr	k1gInSc6	Kypr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
zdrojem	zdroj	k1gInSc7	zdroj
napětí	napětí	k1gNnSc2	napětí
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
dvěma	dva	k4xCgFnPc7	dva
vírami	víra	k1gFnPc7	víra
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
navštívil	navštívit	k5eAaPmAgMnS	navštívit
i	i	k9	i
další	další	k2eAgInSc4d1	další
stát	stát	k1gInSc4	stát
s	s	k7c7	s
početným	početný	k2eAgNnSc7d1	početné
zastoupením	zastoupení	k1gNnSc7	zastoupení
pravoslavných	pravoslavný	k2eAgMnPc2d1	pravoslavný
věřících	věřící	k1gMnPc2	věřící
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
chladnému	chladný	k2eAgNnSc3d1	chladné
přivítání	přivítání	k1gNnSc3	přivítání
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
ukončení	ukončení	k1gNnSc1	ukončení
schizmatu	schizma	k1gNnSc2	schizma
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
nejvroucnějších	vroucný	k2eAgNnPc2d3	nejvroucnější
přání	přání	k1gNnPc2	přání
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
vztahů	vztah	k1gInPc2	vztah
se	se	k3xPyFc4	se
srbskou	srbský	k2eAgFnSc7d1	Srbská
pravoslavnou	pravoslavný	k2eAgFnSc7d1	pravoslavná
církví	církev	k1gFnSc7	církev
<g/>
,	,	kIx,	,
papež	papež	k1gMnSc1	papež
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
nemohl	moct	k5eNaImAgInS	moct
vyhnout	vyhnout	k5eAaPmF	vyhnout
kontroverzní	kontroverzní	k2eAgInSc1d1	kontroverzní
náklonnosti	náklonnost	k1gFnSc3	náklonnost
chorvatského	chorvatský	k2eAgInSc2d1	chorvatský
katolického	katolický	k2eAgInSc2d1	katolický
kléru	klér	k1gInSc2	klér
k	k	k7c3	k
ustašovskému	ustašovský	k2eAgInSc3d1	ustašovský
režimu	režim	k1gInSc3	režim
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
blahořečil	blahořečit	k5eAaImAgInS	blahořečit
Aloysiuse	Aloysiuse	k1gFnSc2	Aloysiuse
Stepinaca	Stepinacus	k1gMnSc2	Stepinacus
<g/>
,	,	kIx,	,
chorvatského	chorvatský	k2eAgMnSc2d1	chorvatský
záhřebského	záhřebský	k2eAgMnSc2d1	záhřebský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
ve	v	k7c6	v
válečných	válečný	k2eAgInPc6d1	válečný
časech	čas	k1gInPc6	čas
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
vnímán	vnímat	k5eAaImNgInS	vnímat
negativně	negativně	k6eAd1	negativně
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
věřili	věřit	k5eAaImAgMnP	věřit
v	v	k7c4	v
jeho	jeho	k3xOp3gFnSc4	jeho
aktivní	aktivní	k2eAgFnSc4d1	aktivní
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
ustašovým	ustašův	k2eAgInSc7d1	ustašův
fašistickým	fašistický	k2eAgInSc7d1	fašistický
režimem	režim	k1gInSc7	režim
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2003	[number]	k4	2003
navštívil	navštívit	k5eAaPmAgMnS	navštívit
město	město	k1gNnSc4	město
Banja	banjo	k1gNnSc2	banjo
Luka	luka	k1gNnPc1	luka
v	v	k7c6	v
Bosně	Bosna	k1gFnSc6	Bosna
a	a	k8xC	a
Hercegovině	Hercegovina	k1gFnSc6	Hercegovina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgMnSc6	který
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
1995	[number]	k4	1995
bývali	bývat	k5eAaImAgMnP	bývat
mnozí	mnohý	k2eAgMnPc1d1	mnohý
katolíky	katolík	k1gMnPc7	katolík
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
většinově	většinově	k6eAd1	většinově
pravoslavné	pravoslavný	k2eAgNnSc1d1	pravoslavné
<g/>
.	.	kIx.	.
</s>
<s>
Sloužil	sloužit	k5eAaImAgMnS	sloužit
mši	mše	k1gFnSc4	mše
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
Petričevac	Petričevac	k1gFnSc4	Petričevac
<g/>
,	,	kIx,	,
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
kontroverzní	kontroverzní	k2eAgFnSc2d1	kontroverzní
a	a	k8xC	a
krizové	krizový	k2eAgFnSc2d1	krizová
<g/>
.	.	kIx.	.
</s>
<s>
Katolíci	katolík	k1gMnPc1	katolík
v	v	k7c6	v
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
(	(	kIx(	(
<g/>
nejméně	málo	k6eAd3	málo
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
)	)	kIx)	)
doufali	doufat	k5eAaImAgMnP	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
papež	papež	k1gMnSc1	papež
navštíví	navštívit	k5eAaPmIp3nS	navštívit
i	i	k9	i
jejich	jejich	k3xOp3gFnSc4	jejich
zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
to	ten	k3xDgNnSc4	ten
měl	mít	k5eAaImAgMnS	mít
také	také	k9	také
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
<g/>
.	.	kIx.	.
</s>
<s>
Odpor	odpor	k1gInSc1	odpor
ruské	ruský	k2eAgFnSc2d1	ruská
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
běloruského	běloruský	k2eAgMnSc2d1	běloruský
prezidenta	prezident	k1gMnSc2	prezident
Alexandra	Alexandr	k1gMnSc2	Alexandr
Lukašenka	Lukašenka	k1gFnSc1	Lukašenka
mu	on	k3xPp3gInSc3	on
to	ten	k3xDgNnSc1	ten
však	však	k9	však
neumožnil	umožnit	k5eNaPmAgInS	umožnit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
pontifikátu	pontifikát	k1gInSc2	pontifikát
často	často	k6eAd1	často
říkal	říkat	k5eAaImAgMnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc7	jeho
velkým	velký	k2eAgInSc7d1	velký
snem	sen	k1gInSc7	sen
je	být	k5eAaImIp3nS	být
navštívit	navštívit	k5eAaPmF	navštívit
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Vykonal	vykonat	k5eAaPmAgMnS	vykonat
mnoho	mnoho	k4c4	mnoho
pokusů	pokus	k1gInPc2	pokus
vyřešit	vyřešit	k5eAaPmF	vyřešit
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
během	během	k7c2	během
století	století	k1gNnSc2	století
nahromadily	nahromadit	k5eAaPmAgFnP	nahromadit
mezi	mezi	k7c7	mezi
římskokatolickou	římskokatolický	k2eAgFnSc7d1	Římskokatolická
církví	církev	k1gFnSc7	církev
a	a	k8xC	a
Ruskou	ruský	k2eAgFnSc7d1	ruská
pravoslavnou	pravoslavný	k2eAgFnSc7d1	pravoslavná
církví	církev	k1gFnSc7	církev
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2004	[number]	k4	2004
vrátil	vrátit	k5eAaPmAgMnS	vrátit
ruské	ruský	k2eAgFnSc3d1	ruská
církvi	církev	k1gFnSc3	církev
kazaňskou	kazaňský	k2eAgFnSc4d1	Kazaňská
ikonu	ikona	k1gFnSc4	ikona
Matky	matka	k1gFnSc2	matka
boží	boží	k2eAgFnSc2d1	boží
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
nesdílela	sdílet	k5eNaImAgFnS	sdílet
jeho	jeho	k3xOp3gNnSc4	jeho
nadšení	nadšení	k1gNnSc4	nadšení
dávajíc	dávat	k5eAaImSgFnS	dávat
prohlášení	prohlášení	k1gNnPc4	prohlášení
jako	jako	k9	jako
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Otázka	otázka	k1gFnSc1	otázka
návštěvy	návštěva	k1gFnSc2	návštěva
papeže	papež	k1gMnSc2	papež
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
není	být	k5eNaImIp3nS	být
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
problémy	problém	k1gInPc7	problém
mezi	mezi	k7c7	mezi
církvemi	církev	k1gFnPc7	církev
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
nereálné	reálný	k2eNgNnSc1d1	nereálné
vyřešit	vyřešit	k5eAaPmF	vyřešit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
vrácením	vrácení	k1gNnSc7	vrácení
mnohých	mnohý	k2eAgInPc2d1	mnohý
svatých	svatý	k2eAgInPc2d1	svatý
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
ukradeny	ukraden	k2eAgFnPc4d1	ukradena
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Vsevolod	Vsevolod	k1gInSc1	Vsevolod
Chaplin	Chaplin	k1gInSc1	Chaplin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
procestoval	procestovat	k5eAaPmAgMnS	procestovat
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
navštívit	navštívit	k5eAaPmF	navštívit
Rusko	Rusko	k1gNnSc4	Rusko
mu	on	k3xPp3gMnSc3	on
nebylo	být	k5eNaImAgNnS	být
souzeno	soudit	k5eAaImNgNnS	soudit
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
to	ten	k3xDgNnSc4	ten
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
největších	veliký	k2eAgInPc2d3	veliký
neúspěchů	neúspěch	k1gInPc2	neúspěch
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezident	prezident	k1gMnSc1	prezident
Vladimir	Vladimir	k1gMnSc1	Vladimir
Putin	putin	k2eAgMnSc1d1	putin
se	se	k3xPyFc4	se
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
papežova	papežův	k2eAgInSc2d1	papežův
pohřbu	pohřeb	k1gInSc2	pohřeb
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mu	on	k3xPp3gNnSc3	on
patriarcha	patriarcha	k1gMnSc1	patriarcha
Alexij	Alexij	k1gFnPc2	Alexij
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
hlava	hlava	k1gFnSc1	hlava
Ruské	ruský	k2eAgFnSc2d1	ruská
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nešel	jít	k5eNaImAgMnS	jít
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1992	[number]	k4	1992
se	se	k3xPyFc4	se
omluvil	omluvit	k5eAaPmAgMnS	omluvit
za	za	k7c4	za
pronásledování	pronásledování	k1gNnSc4	pronásledování
italského	italský	k2eAgMnSc2d1	italský
filozofa	filozof	k1gMnSc2	filozof
a	a	k8xC	a
vědce	vědec	k1gMnSc2	vědec
Galilea	Galilea	k1gFnSc1	Galilea
Galilei	Galilei	k1gNnPc4	Galilei
v	v	k7c6	v
soudním	soudní	k2eAgInSc6d1	soudní
procesu	proces	k1gInSc6	proces
vedeném	vedený	k2eAgInSc6d1	vedený
římskokatolickou	římskokatolický	k2eAgFnSc4d1	Římskokatolická
církví	církev	k1gFnPc2	církev
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1633	[number]	k4	1633
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1993	[number]	k4	1993
se	se	k3xPyFc4	se
omluvil	omluvit	k5eAaPmAgMnS	omluvit
za	za	k7c4	za
účast	účast	k1gFnSc4	účast
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
na	na	k7c6	na
obchodu	obchod	k1gInSc6	obchod
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1995	[number]	k4	1995
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
prosil	prosít	k5eAaPmAgMnS	prosít
o	o	k7c4	o
odpuštění	odpuštění	k1gNnSc4	odpuštění
za	za	k7c4	za
úlohu	úloha	k1gFnSc4	úloha
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
církev	církev	k1gFnSc1	církev
sehrála	sehrát	k5eAaPmAgFnS	sehrát
v	v	k7c6	v
náboženské	náboženský	k2eAgFnSc6d1	náboženská
válce	válka	k1gFnSc6	válka
proti	proti	k7c3	proti
reformaci	reformace	k1gFnSc3	reformace
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1995	[number]	k4	1995
poslal	poslat	k5eAaPmAgInS	poslat
list	list	k1gInSc1	list
"	"	kIx"	"
<g/>
všem	všecek	k3xTgFnPc3	všecek
ženám	žena	k1gFnPc3	žena
<g/>
"	"	kIx"	"
s	s	k7c7	s
omluvou	omluva	k1gFnSc7	omluva
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
církev	církev	k1gFnSc1	církev
byla	být	k5eAaImAgFnS	být
proti	proti	k7c3	proti
právům	právo	k1gNnPc3	právo
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
za	za	k7c4	za
historické	historický	k2eAgNnSc4d1	historické
znevažovaní	znevažovaný	k2eAgMnPc1d1	znevažovaný
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1998	[number]	k4	1998
se	se	k3xPyFc4	se
omluvil	omluvit	k5eAaPmAgMnS	omluvit
za	za	k7c4	za
nečinnost	nečinnost	k1gFnSc4	nečinnost
a	a	k8xC	a
mlčení	mlčení	k1gNnSc4	mlčení
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
během	během	k7c2	během
holokaustu	holokaust	k1gInSc2	holokaust
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1999	[number]	k4	1999
se	se	k3xPyFc4	se
omluvil	omluvit	k5eAaPmAgMnS	omluvit
za	za	k7c4	za
popravu	poprava	k1gFnSc4	poprava
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1415	[number]	k4	1415
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
veřejné	veřejný	k2eAgFnSc2d1	veřejná
omluvy	omluva	k1gFnSc2	omluva
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2000	[number]	k4	2000
poprosil	poprosit	k5eAaPmAgInS	poprosit
o	o	k7c4	o
odpuštění	odpuštění	k1gNnSc4	odpuštění
za	za	k7c4	za
hříchy	hřích	k1gInPc4	hřích
katolíků	katolík	k1gMnPc2	katolík
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
,	,	kIx,	,
za	za	k7c4	za
násilí	násilí	k1gNnSc4	násilí
"	"	kIx"	"
<g/>
na	na	k7c6	na
právech	právo	k1gNnPc6	právo
národnostních	národnostní	k2eAgFnPc2d1	národnostní
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
a	a	k8xC	a
za	za	k7c4	za
opovrhovaní	opovrhovaný	k2eAgMnPc1d1	opovrhovaný
jejich	jejich	k3xOp3gNnSc1	jejich
kulturou	kultura	k1gFnSc7	kultura
a	a	k8xC	a
náboženskými	náboženský	k2eAgFnPc7d1	náboženská
tradicemi	tradice	k1gFnPc7	tradice
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2001	[number]	k4	2001
se	se	k3xPyFc4	se
omluvil	omluvit	k5eAaPmAgMnS	omluvit
patriarchovi	patriarcha	k1gMnSc3	patriarcha
konstantinopolskému	konstantinopolský	k2eAgMnSc3d1	konstantinopolský
za	za	k7c4	za
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
křižáckou	křižácký	k2eAgFnSc4d1	křižácká
výpravu	výprava	k1gFnSc4	výprava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1204	[number]	k4	1204
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2001	[number]	k4	2001
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
internet	internet	k1gInSc4	internet
omluvil	omluvit	k5eAaPmAgMnS	omluvit
za	za	k7c4	za
hrubé	hrubý	k2eAgNnSc4d1	hrubé
zacházení	zacházení	k1gNnSc4	zacházení
misionářů	misionář	k1gMnPc2	misionář
s	s	k7c7	s
domorodým	domorodý	k2eAgNnSc7d1	domorodé
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
Pacifiku	Pacifik	k1gInSc6	Pacifik
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
doktrín	doktrína	k1gFnPc2	doktrína
a	a	k8xC	a
věcí	věc	k1gFnPc2	věc
souvisejících	související	k2eAgFnPc2d1	související
s	s	k7c7	s
porodností	porodnost	k1gFnSc7	porodnost
a	a	k8xC	a
postavením	postavení	k1gNnSc7	postavení
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
považovaný	považovaný	k2eAgMnSc1d1	považovaný
za	za	k7c4	za
konzervativního	konzervativní	k2eAgMnSc4d1	konzervativní
<g/>
.	.	kIx.	.
</s>
<s>
Listy	lista	k1gFnPc4	lista
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
lidské	lidský	k2eAgFnSc2d1	lidská
sexuality	sexualita	k1gFnSc2	sexualita
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgFnPc4d1	nazývaná
Teologie	teologie	k1gFnPc4	teologie
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
rozšířením	rozšíření	k1gNnSc7	rozšíření
meditací	meditace	k1gFnSc7	meditace
o	o	k7c4	o
přirozené	přirozený	k2eAgFnPc4d1	přirozená
mužnosti	mužnost	k1gFnPc4	mužnost
a	a	k8xC	a
ženskosti	ženskost	k1gFnPc4	ženskost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
lásce	láska	k1gFnSc3	láska
a	a	k8xC	a
k	k	k7c3	k
sexu	sex	k1gInSc3	sex
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
učení	učení	k1gNnPc1	učení
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
zásadní	zásadní	k2eAgInSc4d1	zásadní
vývoj	vývoj	k1gInSc4	vývoj
v	v	k7c6	v
katolickém	katolický	k2eAgNnSc6d1	katolické
učení	učení	k1gNnSc6	učení
o	o	k7c6	o
sexu	sex	k1gInSc6	sex
<g/>
.	.	kIx.	.
</s>
<s>
Ohledně	ohledně	k7c2	ohledně
interrupcí	interrupce	k1gFnPc2	interrupce
papež	papež	k1gMnSc1	papež
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Stále	stále	k6eAd1	stále
existuje	existovat	k5eAaImIp3nS	existovat
legální	legální	k2eAgNnSc1d1	legální
zabití	zabití	k1gNnSc1	zabití
lidské	lidský	k2eAgFnSc2d1	lidská
bytosti	bytost	k1gFnSc2	bytost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ještě	ještě	k6eAd1	ještě
není	být	k5eNaImIp3nS	být
narozená	narozený	k2eAgFnSc1d1	narozená
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
v	v	k7c6	v
dnešních	dnešní	k2eAgInPc6d1	dnešní
časech	čas	k1gInPc6	čas
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c4	o
zabití	zabití	k1gNnSc4	zabití
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
umožněné	umožněný	k2eAgNnSc1d1	umožněné
demokraticky	demokraticky	k6eAd1	demokraticky
zvoleným	zvolený	k2eAgInSc7d1	zvolený
parlamentem	parlament	k1gInSc7	parlament
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
považovaný	považovaný	k2eAgInSc1d1	považovaný
za	za	k7c4	za
pokrokový	pokrokový	k2eAgInSc4d1	pokrokový
výdobytek	výdobytek	k1gInSc4	výdobytek
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
celého	celý	k2eAgNnSc2d1	celé
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Byl	být	k5eAaImAgMnS	být
kritický	kritický	k2eAgMnSc1d1	kritický
k	k	k7c3	k
liberální	liberální	k2eAgFnSc3d1	liberální
teologii	teologie	k1gFnSc3	teologie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spíše	spíše	k9	spíše
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
liberální	liberální	k2eAgFnSc4d1	liberální
politiku	politika	k1gFnSc4	politika
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
duchovní	duchovní	k2eAgFnSc2d1	duchovní
liberalizace	liberalizace	k1gFnSc2	liberalizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
v	v	k7c6	v
encyklice	encyklika	k1gFnSc6	encyklika
Evangelium	evangelium	k1gNnSc1	evangelium
Vitae	Vitae	k1gNnSc2	Vitae
(	(	kIx(	(
<g/>
Evangelium	evangelium	k1gNnSc1	evangelium
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
znovu	znovu	k6eAd1	znovu
prosadil	prosadit	k5eAaPmAgMnS	prosadit
za	za	k7c4	za
hlavní	hlavní	k2eAgFnSc4d1	hlavní
hodnotu	hodnota	k1gFnSc4	hodnota
církve	církev	k1gFnSc2	církev
lidský	lidský	k2eAgInSc4d1	lidský
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
ji	on	k3xPp3gFnSc4	on
též	též	k6eAd1	též
o	o	k7c4	o
odsouzení	odsouzení	k1gNnSc4	odsouzení
interrupce	interrupce	k1gFnSc2	interrupce
<g/>
,	,	kIx,	,
eutanazie	eutanazie	k1gFnSc2	eutanazie
a	a	k8xC	a
o	o	k7c4	o
všechna	všechen	k3xTgNnPc4	všechen
použití	použití	k1gNnPc4	použití
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
označil	označit	k5eAaPmAgMnS	označit
jako	jako	k8xS	jako
součásti	součást	k1gFnSc2	součást
"	"	kIx"	"
<g/>
kultury	kultura	k1gFnSc2	kultura
smrti	smrt	k1gFnSc2	smrt
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pronikla	proniknout	k5eAaPmAgFnS	proniknout
do	do	k7c2	do
moderního	moderní	k2eAgInSc2d1	moderní
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
postoje	postoj	k1gInPc1	postoj
k	k	k7c3	k
válce	válka	k1gFnSc3	válka
<g/>
,	,	kIx,	,
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
k	k	k7c3	k
odpuštění	odpuštění	k1gNnSc3	odpuštění
dluhů	dluh	k1gInPc2	dluh
pro	pro	k7c4	pro
rozvojové	rozvojový	k2eAgFnPc4d1	rozvojová
země	zem	k1gFnPc4	zem
a	a	k8xC	a
k	k	k7c3	k
chudobě	chudoba	k1gFnSc3	chudoba
jsou	být	k5eAaImIp3nP	být
označované	označovaný	k2eAgInPc1d1	označovaný
jako	jako	k8xC	jako
liberální	liberální	k2eAgInPc1d1	liberální
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
těžké	těžký	k2eAgNnSc1d1	těžké
označit	označit	k5eAaPmF	označit
náboženské	náboženský	k2eAgMnPc4d1	náboženský
vůdce	vůdce	k1gMnPc4	vůdce
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
konzervativní	konzervativní	k2eAgMnPc4d1	konzervativní
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
liberální	liberální	k2eAgFnSc3d1	liberální
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
kontroloval	kontrolovat	k5eAaImAgInS	kontrolovat
jeho	jeho	k3xOp3gFnSc4	jeho
rodnou	rodný	k2eAgFnSc4d1	rodná
krajinu	krajina	k1gFnSc4	krajina
Polsko	Polsko	k1gNnSc4	Polsko
<g/>
,	,	kIx,	,
i	i	k8xC	i
zbytek	zbytek	k1gInSc1	zbytek
Východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
tvrdý	tvrdý	k2eAgMnSc1d1	tvrdý
kritik	kritik	k1gMnSc1	kritik
komunismu	komunismus	k1gInSc2	komunismus
a	a	k8xC	a
vyzýval	vyzývat	k5eAaImAgMnS	vyzývat
ke	k	k7c3	k
spolupráci	spolupráce	k1gFnSc3	spolupráce
v	v	k7c6	v
boji	boj	k1gInSc6	boj
za	za	k7c4	za
změnu	změna	k1gFnSc4	změna
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
polskému	polský	k2eAgNnSc3d1	polské
hnutí	hnutí	k1gNnSc3	hnutí
Solidarita	solidarita	k1gFnSc1	solidarita
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgMnSc1d1	sovětský
vůdce	vůdce	k1gMnSc1	vůdce
Michail	Michail	k1gMnSc1	Michail
Gorbačov	Gorbačovo	k1gNnPc2	Gorbačovo
jednou	jednou	k6eAd1	jednou
řekl	říct	k5eAaPmAgInS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
pád	pád	k1gInSc1	pád
železné	železný	k2eAgFnSc2d1	železná
opony	opona	k1gFnSc2	opona
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
bez	bez	k7c2	bez
Jana	Jan	k1gMnSc4	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
nemožný	možný	k2eNgMnSc1d1	nemožný
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
pohled	pohled	k1gInSc4	pohled
sdíleli	sdílet	k5eAaImAgMnP	sdílet
mnozí	mnohý	k2eAgMnPc1d1	mnohý
lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
postkomunistických	postkomunistický	k2eAgInPc6d1	postkomunistický
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
<g/>
,	,	kIx,	,
i	i	k9	i
hrdinové	hrdina	k1gMnPc1	hrdina
zodpovědní	zodpovědný	k2eAgMnPc1d1	zodpovědný
za	za	k7c4	za
ukončení	ukončení	k1gNnSc4	ukončení
komunistického	komunistický	k2eAgInSc2d1	komunistický
útlaku	útlak	k1gInSc2	útlak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
papež	papež	k1gMnSc1	papež
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
i	i	k9	i
některé	některý	k3yIgFnPc1	některý
extrémní	extrémní	k2eAgFnPc1d1	extrémní
verze	verze	k1gFnPc1	verze
korporátního	korporátní	k2eAgInSc2d1	korporátní
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
veřejně	veřejně	k6eAd1	veřejně
podpořil	podpořit	k5eAaPmAgMnS	podpořit
kampaň	kampaň	k1gFnSc4	kampaň
za	za	k7c4	za
odpuštění	odpuštění	k1gNnSc4	odpuštění
afrického	africký	k2eAgInSc2d1	africký
dluhu	dluh	k1gInSc2	dluh
vedenou	vedený	k2eAgFnSc4d1	vedená
irskými	irský	k2eAgFnPc7d1	irská
rockovými	rockový	k2eAgFnPc7d1	rocková
hvězdami	hvězda	k1gFnPc7	hvězda
Bobem	bob	k1gInSc7	bob
Geldofem	Geldof	k1gInSc7	Geldof
a	a	k8xC	a
Bonem	bon	k1gInSc7	bon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
čase	čas	k1gInSc6	čas
bylo	být	k5eAaImAgNnS	být
zveřejněné	zveřejněný	k2eAgNnSc1d1	zveřejněné
<g/>
,	,	kIx,	,
že	že	k8xS	že
nahrávaní	nahrávaný	k2eAgMnPc1d1	nahrávaný
písniček	písnička	k1gFnPc2	písnička
skupiny	skupina	k1gFnSc2	skupina
U2	U2	k1gFnSc2	U2
bylo	být	k5eAaImAgNnS	být
neustále	neustále	k6eAd1	neustále
přerušované	přerušovaný	k2eAgInPc4d1	přerušovaný
papežovými	papežův	k2eAgInPc7d1	papežův
telefonáty	telefonát	k1gInPc7	telefonát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
stal	stát	k5eAaPmAgMnS	stát
prominentním	prominentní	k2eAgMnSc7d1	prominentní
kritikem	kritik	k1gMnSc7	kritik
americké	americký	k2eAgFnSc2d1	americká
invaze	invaze	k1gFnSc2	invaze
do	do	k7c2	do
Iráku	Irák	k1gInSc2	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Poslal	poslat	k5eAaPmAgMnS	poslat
svého	svůj	k3xOyFgMnSc2	svůj
"	"	kIx"	"
<g/>
ministra	ministr	k1gMnSc2	ministr
míru	mír	k1gInSc2	mír
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kardinála	kardinál	k1gMnSc4	kardinál
Pia	Pius	k1gMnSc4	Pius
Laghiho	Laghi	k1gMnSc4	Laghi
na	na	k7c4	na
rozhovory	rozhovor	k1gInPc4	rozhovor
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
Georgem	Georg	k1gMnSc7	Georg
Bushem	Bush	k1gMnSc7	Bush
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
svůj	svůj	k3xOyFgInSc4	svůj
odpor	odpor	k1gInSc4	odpor
vůči	vůči	k7c3	vůči
této	tento	k3xDgFnSc3	tento
válce	válka	k1gFnSc3	válka
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
konflikt	konflikt	k1gInSc1	konflikt
musí	muset	k5eAaImIp3nS	muset
vést	vést	k5eAaImF	vést
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
vyřešit	vyřešit	k5eAaPmF	vyřešit
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
konflikt	konflikt	k1gInSc4	konflikt
diplomatickou	diplomatický	k2eAgFnSc7d1	diplomatická
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednostranná	jednostranný	k2eAgFnSc1d1	jednostranná
agrese	agrese	k1gFnSc1	agrese
je	být	k5eAaImIp3nS	být
zločin	zločin	k1gInSc4	zločin
vůči	vůči	k7c3	vůči
míru	mír	k1gInSc3	mír
a	a	k8xC	a
porušení	porušení	k1gNnSc4	porušení
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vyjednáváních	vyjednávání	k1gNnPc6	vyjednávání
s	s	k7c7	s
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
o	o	k7c6	o
nové	nový	k2eAgFnSc6d1	nová
ústavě	ústava	k1gFnSc6	ústava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
a	a	k8xC	a
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vatikánským	vatikánský	k2eAgMnSc7d1	vatikánský
představitelům	představitel	k1gMnPc3	představitel
nepodařilo	podařit	k5eNaPmAgNnS	podařit
prosadit	prosadit	k5eAaPmF	prosadit
zmínku	zmínka	k1gFnSc4	zmínka
o	o	k7c6	o
"	"	kIx"	"
<g/>
křesťanském	křesťanský	k2eAgNnSc6d1	křesťanské
dědictví	dědictví	k1gNnSc6	dědictví
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
byl	být	k5eAaImAgInS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
cílů	cíl	k1gInPc2	cíl
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
i	i	k9	i
papež	papež	k1gMnSc1	papež
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
byl	být	k5eAaImAgMnS	být
též	též	k9	též
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
kritiků	kritik	k1gMnPc2	kritik
homosexuálních	homosexuální	k2eAgNnPc2d1	homosexuální
manželství	manželství	k1gNnPc2	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
poslední	poslední	k2eAgFnSc6d1	poslední
knize	kniha	k1gFnSc6	kniha
Paměť	paměť	k1gFnSc1	paměť
a	a	k8xC	a
identita	identita	k1gFnSc1	identita
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
popisuje	popisovat	k5eAaImIp3nS	popisovat
nátlak	nátlak	k1gInSc4	nátlak
na	na	k7c4	na
Evropský	evropský	k2eAgInSc4d1	evropský
parlament	parlament	k1gInSc4	parlament
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
povolil	povolit	k5eAaPmAgInS	povolit
homosexuální	homosexuální	k2eAgNnPc4d1	homosexuální
manželství	manželství	k1gNnPc4	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Agentura	agentura	k1gFnSc1	agentura
Reuters	Reutersa	k1gFnPc2	Reutersa
citovala	citovat	k5eAaBmAgFnS	citovat
papeže	papež	k1gMnSc4	papež
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
legitimní	legitimní	k2eAgMnSc1d1	legitimní
a	a	k8xC	a
nevyhnutné	vyhnutný	k2eNgNnSc1d1	nevyhnutné
zeptat	zeptat	k5eAaPmF	zeptat
se	se	k3xPyFc4	se
sebe	sebe	k3xPyFc4	sebe
samého	samý	k3xTgNnSc2	samý
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
část	část	k1gFnSc1	část
nové	nový	k2eAgFnSc2d1	nová
ďábelské	ďábelský	k2eAgFnSc2d1	ďábelská
ideologie	ideologie	k1gFnSc2	ideologie
<g/>
,	,	kIx,	,
možná	možná	k9	možná
více	hodně	k6eAd2	hodně
vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
a	a	k8xC	a
skryté	skrytý	k2eAgFnPc1d1	skrytá
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zkouší	zkoušet	k5eAaImIp3nS	zkoušet
postavit	postavit	k5eAaPmF	postavit
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
proti	proti	k7c3	proti
rodině	rodina	k1gFnSc3	rodina
a	a	k8xC	a
proti	proti	k7c3	proti
člověku	člověk	k1gMnSc3	člověk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Papež	Papež	k1gMnSc1	Papež
též	též	k9	též
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
transsexualitu	transsexualita	k1gFnSc4	transsexualita
a	a	k8xC	a
změnu	změna	k1gFnSc4	změna
pohlaví	pohlaví	k1gNnSc2	pohlaví
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Kongregace	kongregace	k1gFnSc1	kongregace
pro	pro	k7c4	pro
nauku	nauka	k1gFnSc4	nauka
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
předsedal	předsedat	k5eAaImAgMnS	předsedat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
označila	označit	k5eAaPmAgFnS	označit
za	za	k7c4	za
lidi	člověk	k1gMnPc4	člověk
s	s	k7c7	s
"	"	kIx"	"
<g/>
mentálními	mentální	k2eAgFnPc7d1	mentální
patologiemi	patologie	k1gFnPc7	patologie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
velké	velký	k2eAgFnSc3d1	velká
popularitě	popularita	k1gFnSc3	popularita
si	se	k3xPyFc3	se
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
i	i	k9	i
mnoho	mnoho	k6eAd1	mnoho
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
<g/>
,	,	kIx,	,
že	že	k8xS	že
díky	díky	k7c3	díky
opozici	opozice	k1gFnSc3	opozice
vůči	vůči	k7c3	vůči
komunismu	komunismus	k1gInSc3	komunismus
podporoval	podporovat	k5eAaImAgMnS	podporovat
pravicové	pravicový	k2eAgMnPc4d1	pravicový
diktátory	diktátor	k1gMnPc4	diktátor
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
příležitostně	příležitostně	k6eAd1	příležitostně
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
diktátory	diktátor	k1gMnPc7	diktátor
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
s	s	k7c7	s
Augestem	Augest	k1gMnSc7	Augest
Pinochetem	Pinochet	k1gMnSc7	Pinochet
z	z	k7c2	z
Chile	Chile	k1gNnSc2	Chile
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
toto	tento	k3xDgNnSc4	tento
setkání	setkání	k1gNnSc4	setkání
interpretovali	interpretovat	k5eAaBmAgMnP	interpretovat
jako	jako	k8xS	jako
podporu	podpora	k1gFnSc4	podpora
Pinochetovi	Pinochetův	k2eAgMnPc1d1	Pinochetův
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jiných	jiný	k1gMnPc2	jiný
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
při	při	k7c6	při
setkání	setkání	k1gNnSc6	setkání
žádal	žádat	k5eAaImAgMnS	žádat
od	od	k7c2	od
Pinocheta	Pinocheto	k1gNnSc2	Pinocheto
obnovu	obnova	k1gFnSc4	obnova
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
setkání	setkání	k1gNnSc6	setkání
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
vmanévrován	vmanévrován	k2eAgInSc1d1	vmanévrován
Pinochetovým	Pinochetový	k2eAgNnSc7d1	Pinochetový
okolím	okolí	k1gNnSc7	okolí
do	do	k7c2	do
společné	společný	k2eAgFnSc2d1	společná
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
kritika	kritika	k1gFnSc1	kritika
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
údajně	údajně	k6eAd1	údajně
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
s	s	k7c7	s
kardinálem	kardinál	k1gMnSc7	kardinál
Piem	Pius	k1gMnSc7	Pius
Laghim	Laghim	k1gInSc4	Laghim
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
podle	podle	k7c2	podle
kritiků	kritik	k1gMnPc2	kritik
podporoval	podporovat	k5eAaImAgMnS	podporovat
"	"	kIx"	"
<g/>
špinavou	špinavý	k2eAgFnSc4d1	špinavá
válku	válka	k1gFnSc4	válka
<g/>
"	"	kIx"	"
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
byl	být	k5eAaImAgMnS	být
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
za	za	k7c4	za
podporu	podpora	k1gFnSc4	podpora
společnosti	společnost	k1gFnSc2	společnost
Opus	opus	k1gInSc1	opus
Dei	Dei	k1gFnSc4	Dei
a	a	k8xC	a
svatořečení	svatořečení	k1gNnSc4	svatořečení
jeho	jeho	k3xOp3gMnSc2	jeho
zakladatele	zakladatel	k1gMnSc2	zakladatel
Josemaríi	Josemarí	k1gFnSc2	Josemarí
Escrivy	Escriva	k1gFnSc2	Escriva
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
kritici	kritik	k1gMnPc1	kritik
praví	pravý	k2eAgMnPc1d1	pravý
<g/>
,	,	kIx,	,
že	že	k8xS	že
Opus	opus	k1gInSc1	opus
Dei	Dei	k1gFnSc2	Dei
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
kult	kult	k1gInSc1	kult
operující	operující	k2eAgInSc1d1	operující
uvnitř	uvnitř	k7c2	uvnitř
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
viděl	vidět	k5eAaImAgInS	vidět
tuto	tento	k3xDgFnSc4	tento
organizaci	organizace	k1gFnSc4	organizace
jako	jako	k8xC	jako
součást	součást	k1gFnSc4	součást
širšího	široký	k2eAgInSc2d2	širší
návratu	návrat	k1gInSc2	návrat
církve	církev	k1gFnSc2	církev
k	k	k7c3	k
základním	základní	k2eAgInPc3d1	základní
křesťanským	křesťanský	k2eAgInPc3d1	křesťanský
principům	princip	k1gInPc3	princip
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Escrivy	Escriva	k1gFnSc2	Escriva
i	i	k9	i
několik	několik	k4yIc4	několik
dalších	další	k2eAgNnPc2d1	další
svatořečení	svatořečení	k1gNnPc2	svatořečení
a	a	k8xC	a
blahořečení	blahořečení	k1gNnSc2	blahořečení
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
kritické	kritický	k2eAgInPc4d1	kritický
ohlasy	ohlas	k1gInPc4	ohlas
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dotyční	dotyčný	k2eAgMnPc1d1	dotyčný
údajně	údajně	k6eAd1	údajně
podporovali	podporovat	k5eAaImAgMnP	podporovat
fašistické	fašistický	k2eAgFnPc4d1	fašistická
politické	politický	k2eAgFnPc4d1	politická
strany	strana	k1gFnPc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Papežovi	Papežův	k2eAgMnPc1d1	Papežův
zastánci	zastánce	k1gMnPc1	zastánce
oponovali	oponovat	k5eAaImAgMnP	oponovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgNnPc1	tento
tvrzení	tvrzení	k1gNnPc1	tvrzení
jsou	být	k5eAaImIp3nP	být
nepravdivá	pravdivý	k2eNgNnPc1d1	nepravdivé
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
kritika	kritika	k1gFnSc1	kritika
se	se	k3xPyFc4	se
zaměřovala	zaměřovat	k5eAaImAgFnS	zaměřovat
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
přesvědčení	přesvědčení	k1gNnSc6	přesvědčení
<g/>
.	.	kIx.	.
</s>
<s>
Obzvlášť	obzvlášť	k6eAd1	obzvlášť
kritizováno	kritizovat	k5eAaImNgNnS	kritizovat
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gFnSc4	jeho
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
o	o	k7c6	o
úlohách	úloha	k1gFnPc6	úloha
pohlaví	pohlaví	k1gNnSc2	pohlaví
a	a	k8xC	a
sexuality	sexualita	k1gFnSc2	sexualita
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
feministky	feministka	k1gFnPc1	feministka
kritizovaly	kritizovat	k5eAaImAgFnP	kritizovat
jeho	jeho	k3xOp3gInSc4	jeho
postoj	postoj	k1gInSc4	postoj
o	o	k7c4	o
postavení	postavení	k1gNnSc4	postavení
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
aktivisté	aktivista	k1gMnPc1	aktivista
za	za	k7c2	za
práva	právo	k1gNnSc2	právo
homosexuálů	homosexuál	k1gMnPc2	homosexuál
nesouhlasili	souhlasit	k5eNaImAgMnP	souhlasit
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
formulací	formulace	k1gFnSc7	formulace
církevní	církevní	k2eAgFnPc1d1	církevní
pozice	pozice	k1gFnPc1	pozice
<g/>
,	,	kIx,	,
že	že	k8xS	že
homosexuální	homosexuální	k2eAgFnPc1d1	homosexuální
touhy	touha	k1gFnPc1	touha
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
postižené	postižený	k2eAgFnPc1d1	postižená
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
obzvlášť	obzvlášť	k6eAd1	obzvlášť
s	s	k7c7	s
nesouhlasem	nesouhlas	k1gInSc7	nesouhlas
se	se	k3xPyFc4	se
sňatky	sňatek	k1gInPc1	sňatek
osob	osoba	k1gFnPc2	osoba
stejného	stejný	k2eAgNnSc2d1	stejné
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
mnohé	mnohé	k1gNnSc4	mnohé
byl	být	k5eAaImAgInS	být
mimořádně	mimořádně	k6eAd1	mimořádně
kontroverzní	kontroverzní	k2eAgInSc1d1	kontroverzní
jeho	jeho	k3xOp3gInSc1	jeho
postoj	postoj	k1gInSc1	postoj
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
proti	proti	k7c3	proti
početí	početí	k1gNnSc3	početí
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
podporoval	podporovat	k5eAaImAgMnS	podporovat
tradiční	tradiční	k2eAgNnSc4d1	tradiční
katolické	katolický	k2eAgNnSc4d1	katolické
učení	učení	k1gNnSc4	učení
a	a	k8xC	a
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
neoddělitelnou	oddělitelný	k2eNgFnSc7d1	neoddělitelná
součástí	součást	k1gFnSc7	součást
sexu	sex	k1gInSc2	sex
je	být	k5eAaImIp3nS	být
rozměr	rozměr	k1gInSc1	rozměr
plodnosti	plodnost	k1gFnSc2	plodnost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
argumentoval	argumentovat	k5eAaImAgMnS	argumentovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
používání	používání	k1gNnSc1	používání
umělých	umělý	k2eAgInPc2d1	umělý
antikoncepčních	antikoncepční	k2eAgInPc2d1	antikoncepční
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
plodnost	plodnost	k1gFnSc1	plodnost
ze	z	k7c2	z
sexu	sex	k1gInSc2	sex
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
a	a	k8xC	a
redukují	redukovat	k5eAaBmIp3nP	redukovat
ho	on	k3xPp3gInSc4	on
jen	jen	k9	jen
na	na	k7c4	na
tělesný	tělesný	k2eAgInSc4d1	tělesný
požitek	požitek	k1gInSc4	požitek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nemorálním	morální	k2eNgInSc7d1	nemorální
činem	čin	k1gInSc7	čin
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
postojem	postoj	k1gInSc7	postoj
nesouhlasilo	souhlasit	k5eNaImAgNnS	souhlasit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokonce	dokonce	k9	dokonce
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
souhlasili	souhlasit	k5eAaImAgMnP	souhlasit
<g/>
,	,	kIx,	,
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nepraktické	praktický	k2eNgNnSc1d1	nepraktické
zavrhovaní	zavrhovaný	k2eAgMnPc1d1	zavrhovaný
používání	používání	k1gNnSc6	používání
kondomů	kondom	k1gInPc2	kondom
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
sexuálně	sexuálně	k6eAd1	sexuálně
přenosné	přenosný	k2eAgFnSc2d1	přenosná
choroby	choroba	k1gFnSc2	choroba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
AIDS	AIDS	kA	AIDS
<g/>
.	.	kIx.	.
</s>
<s>
Oddělené	oddělený	k2eAgNnSc1d1	oddělené
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
příbuzné	příbuzný	k2eAgNnSc1d1	příbuzné
tvrzení	tvrzení	k1gNnSc1	tvrzení
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
administrativa	administrativa	k1gFnSc1	administrativa
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
rozšiřovala	rozšiřovat	k5eAaImAgFnS	rozšiřovat
nepodložené	podložený	k2eNgNnSc4d1	nepodložené
tvrzení	tvrzení	k1gNnSc4	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
kondomy	kondom	k1gInPc1	kondom
nebrání	bránit	k5eNaImIp3nS	bránit
šíření	šíření	k1gNnSc4	šíření
HIV	HIV	kA	HIV
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
kritici	kritik	k1gMnPc1	kritik
ho	on	k3xPp3gNnSc4	on
obviňovali	obviňovat	k5eAaImAgMnP	obviňovat
z	z	k7c2	z
rozšiřovaní	rozšiřovaný	k2eAgMnPc1d1	rozšiřovaný
epidemie	epidemie	k1gFnSc1	epidemie
AIDS	AIDS	kA	AIDS
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
i	i	k9	i
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
jeho	jeho	k3xOp3gMnPc1	jeho
podporovatelé	podporovatel	k1gMnPc1	podporovatel
zase	zase	k9	zase
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
papežovo	papežův	k2eAgNnSc1d1	papežovo
zdůrazňování	zdůrazňování	k1gNnSc1	zdůrazňování
abstinence	abstinence	k1gFnSc2	abstinence
a	a	k8xC	a
věrnosti	věrnost	k1gFnSc2	věrnost
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
AIDS	AIDS	kA	AIDS
velmi	velmi	k6eAd1	velmi
účinné	účinný	k2eAgInPc4d1	účinný
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
například	například	k6eAd1	například
v	v	k7c6	v
Ugandě	Uganda	k1gFnSc6	Uganda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
vedl	vést	k5eAaImAgInS	vést
kampaň	kampaň	k1gFnSc4	kampaň
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgMnS	být
někdy	někdy	k6eAd1	někdy
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
pro	pro	k7c4	pro
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
vedl	vést	k5eAaImAgMnS	vést
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
vyčítáno	vyčítán	k2eAgNnSc1d1	vyčítáno
<g/>
,	,	kIx,	,
že	že	k8xS	že
nereagoval	reagovat	k5eNaBmAgMnS	reagovat
dostatečně	dostatečně	k6eAd1	dostatečně
rychle	rychle	k6eAd1	rychle
při	při	k7c6	při
skandálu	skandál	k1gInSc6	skandál
sexuálního	sexuální	k2eAgInSc2d1	sexuální
zneužívaní	zneužívaný	k2eAgMnPc1d1	zneužívaný
v	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
církvi	církev	k1gFnSc6	církev
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k9	též
byl	být	k5eAaImAgInS	být
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
za	za	k7c4	za
centralizaci	centralizace	k1gFnSc4	centralizace
moci	moct	k5eAaImF	moct
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Vatikánu	Vatikán	k1gInSc2	Vatikán
po	po	k7c6	po
předchozí	předchozí	k2eAgFnSc6d1	předchozí
decentralizaci	decentralizace	k1gFnSc6	decentralizace
papeže	papež	k1gMnSc2	papež
Jana	Jan	k1gMnSc2	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
takový	takový	k3xDgInSc1	takový
byl	být	k5eAaImAgInS	být
označován	označovat	k5eAaImNgInS	označovat
některými	některý	k3yIgFnPc7	některý
za	za	k7c4	za
striktního	striktní	k2eAgMnSc4d1	striktní
autokrata	autokrat	k1gMnSc4	autokrat
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
neakceptoval	akceptovat	k5eNaBmAgMnS	akceptovat
opozici	opozice	k1gFnSc4	opozice
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
hlavní	hlavní	k2eAgInSc1d1	hlavní
příklad	příklad	k1gInSc1	příklad
této	tento	k3xDgFnSc2	tento
kritiky	kritika	k1gFnSc2	kritika
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
exkomunikace	exkomunikace	k1gFnSc1	exkomunikace
otce	otec	k1gMnSc2	otec
Tissa	Tiss	k1gMnSc2	Tiss
Balasuriya	Balasuriyus	k1gMnSc2	Balasuriyus
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
veškeré	veškerý	k3xTgFnSc2	veškerý
kritiky	kritika	k1gFnSc2	kritika
vedené	vedený	k2eAgFnSc2d1	vedená
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
požadavku	požadavek	k1gInSc2	požadavek
přiblížit	přiblížit	k5eAaPmF	přiblížit
církev	církev	k1gFnSc4	církev
současnému	současný	k2eAgInSc3d1	současný
světu	svět	k1gInSc3	svět
<g/>
,	,	kIx,	,
katoličtí	katolický	k2eAgMnPc1d1	katolický
tradicionalisté	tradicionalista	k1gMnPc1	tradicionalista
se	se	k3xPyFc4	se
vehementně	vehementně	k6eAd1	vehementně
dožadovali	dožadovat	k5eAaImAgMnP	dožadovat
návratu	návrat	k1gInSc2	návrat
k	k	k7c3	k
Tridentské	tridentský	k2eAgFnSc3d1	tridentská
liturgii	liturgie	k1gFnSc3	liturgie
a	a	k8xC	a
popření	popření	k1gNnSc3	popření
reforem	reforma	k1gFnPc2	reforma
zavedených	zavedený	k2eAgFnPc2d1	zavedená
Druhým	druhý	k4xOgInSc7	druhý
vatikánským	vatikánský	k2eAgInSc7d1	vatikánský
koncilem	koncil	k1gInSc7	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
přešli	přejít	k5eAaPmAgMnP	přejít
na	na	k7c4	na
pozice	pozice	k1gFnPc4	pozice
sedevakantismu	sedevakantismus	k1gInSc2	sedevakantismus
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
další	další	k2eAgMnPc1d1	další
setrvali	setrvat	k5eAaPmAgMnP	setrvat
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ostře	ostro	k6eAd1	ostro
ho	on	k3xPp3gMnSc4	on
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
dost	dost	k6eAd1	dost
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
<g/>
.	.	kIx.	.
</s>
<s>
Konzervativci	konzervativec	k1gMnPc1	konzervativec
mu	on	k3xPp3gInSc3	on
také	také	k9	také
často	často	k6eAd1	často
vyčítají	vyčítat	k5eAaImIp3nP	vyčítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
modlil	modlit	k5eAaImAgMnS	modlit
s	s	k7c7	s
představiteli	představitel	k1gMnPc7	představitel
nekřesťanských	křesťanský	k2eNgNnPc2d1	nekřesťanské
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
,	,	kIx,	,
navštívil	navštívit	k5eAaPmAgMnS	navštívit
mešitu	mešita	k1gFnSc4	mešita
<g/>
,	,	kIx,	,
políbil	políbit	k5eAaPmAgInS	políbit
Korán	korán	k2eAgInSc4d1	korán
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vlastně	vlastně	k9	vlastně
projevil	projevit	k5eAaPmAgInS	projevit
úctu	úcta	k1gFnSc4	úcta
cizím	cizí	k2eAgNnPc3d1	cizí
božstvům	božstvo	k1gNnPc3	božstvo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
nejsou	být	k5eNaImIp3nP	být
totožná	totožný	k2eAgNnPc1d1	totožné
s	s	k7c7	s
Bohem	bůh	k1gMnSc7	bůh
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
vlastně	vlastně	k9	vlastně
provinil	provinit	k5eAaPmAgInS	provinit
proti	proti	k7c3	proti
1	[number]	k4	1
<g/>
.	.	kIx.	.
přikázání	přikázání	k1gNnPc2	přikázání
desatera	desatero	k1gNnSc2	desatero
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
vyčítá	vyčítat	k5eAaImIp3nS	vyčítat
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
mezináboženských	mezináboženský	k2eAgNnPc6d1	mezináboženské
setkáních	setkání	k1gNnPc6	setkání
v	v	k7c6	v
Assisi	Assise	k1gFnSc6	Assise
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
to	ten	k3xDgNnSc4	ten
dokonce	dokonce	k9	dokonce
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
apostázi	apostáze	k1gFnSc4	apostáze
(	(	kIx(	(
<g/>
odpad	odpad	k1gInSc4	odpad
<g/>
)	)	kIx)	)
od	od	k7c2	od
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Janu	Jan	k1gMnSc3	Jan
Pavlu	Pavel	k1gMnSc3	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
vyčítán	vyčítat	k5eAaImNgInS	vyčítat
jeho	on	k3xPp3gInSc4	on
apatický	apatický	k2eAgInSc4d1	apatický
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
aférám	aféra	k1gFnPc3	aféra
pohlavního	pohlavní	k2eAgNnSc2d1	pohlavní
zneužívání	zneužívání	k1gNnSc2	zneužívání
dětí	dítě	k1gFnPc2	dítě
katolickými	katolický	k2eAgMnPc7d1	katolický
kněžími	kněz	k1gMnPc7	kněz
(	(	kIx(	(
<g/>
především	především	k9	především
v	v	k7c6	v
anglosaských	anglosaský	k2eAgFnPc6d1	anglosaská
zemích	zem	k1gFnPc6	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
ale	ale	k8xC	ale
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
měl	mít	k5eAaImAgInS	mít
jen	jen	k9	jen
malou	malý	k2eAgFnSc4d1	malá
možnost	možnost	k1gFnSc4	možnost
do	do	k7c2	do
těchto	tento	k3xDgFnPc2	tento
záležitostí	záležitost	k1gFnPc2	záležitost
zasahovat	zasahovat	k5eAaImF	zasahovat
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgFnP	být
ututlány	ututlat	k5eAaPmNgFnP	ututlat
již	již	k6eAd1	již
v	v	k7c6	v
samotných	samotný	k2eAgFnPc6d1	samotná
diecézích	diecéze	k1gFnPc6	diecéze
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
k	k	k7c3	k
těmto	tento	k3xDgFnPc3	tento
událostem	událost	k1gFnPc3	událost
docházelo	docházet	k5eAaImAgNnS	docházet
<g/>
,	,	kIx,	,
a	a	k8xC	a
papež	papež	k1gMnSc1	papež
sám	sám	k3xTgMnSc1	sám
o	o	k7c6	o
nich	on	k3xPp3gMnPc6	on
nebyl	být	k5eNaImAgMnS	být
nikým	nikdo	k3yNnSc7	nikdo
informován	informovat	k5eAaBmNgMnS	informovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
konkrétních	konkrétní	k2eAgInPc2d1	konkrétní
případů	případ	k1gInPc2	případ
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
nejvíce	nejvíce	k6eAd1	nejvíce
vyčítán	vyčítán	k2eAgInSc1d1	vyčítán
případ	případ	k1gInSc1	případ
Marciala	Marcial	k1gMnSc2	Marcial
Maciela	Maciel	k1gMnSc2	Maciel
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
Svatý	svatý	k2eAgInSc1d1	svatý
stolec	stolec	k1gInSc1	stolec
dlouho	dlouho	k6eAd1	dlouho
podporoval	podporovat	k5eAaImAgInS	podporovat
navzdory	navzdory	k7c3	navzdory
všem	všecek	k3xTgNnPc3	všecek
závažným	závažný	k2eAgNnPc3d1	závažné
obviněním	obvinění	k1gNnPc3	obvinění
a	a	k8xC	a
podezřením	podezřeň	k1gFnPc3	podezřeň
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
tedy	tedy	k9	tedy
nakonec	nakonec	k6eAd1	nakonec
odsouhlasil	odsouhlasit	k5eAaPmAgMnS	odsouhlasit
jeho	jeho	k3xOp3gNnSc4	jeho
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
apoštolské	apoštolský	k2eAgFnPc1d1	apoštolská
konstituce	konstituce	k1gFnPc1	konstituce
všeobecného	všeobecný	k2eAgInSc2d1	všeobecný
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
konstituce	konstituce	k1gFnSc2	konstituce
zřizující	zřizující	k2eAgFnSc2d1	zřizující
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Ecclesia	Ecclesium	k1gNnPc1	Ecclesium
in	in	k?	in
Urbe	Urb	k1gMnSc2	Urb
o	o	k7c6	o
nové	nový	k2eAgFnSc6d1	nová
organizaci	organizace	k1gFnSc6	organizace
římského	římský	k2eAgInSc2d1	římský
vikariátu	vikariát	k1gInSc2	vikariát
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1998	[number]	k4	1998
Universi	Universe	k1gFnSc6	Universe
Dominici	Dominice	k1gFnSc3	Dominice
Gregis	Gregis	k1gFnSc3	Gregis
o	o	k7c4	o
vakanci	vakance	k1gFnSc4	vakance
Apoštolského	apoštolský	k2eAgInSc2d1	apoštolský
stolce	stolec	k1gInSc2	stolec
a	a	k8xC	a
volbě	volba	k1gFnSc6	volba
římského	římský	k2eAgMnSc2d1	římský
velekněze	velekněz	k1gMnSc2	velekněz
-	-	kIx~	-
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1996	[number]	k4	1996
Fidei	Fide	k1gFnSc6	Fide
Depositum	depositum	k1gNnSc4	depositum
pro	pro	k7c4	pro
publikaci	publikace	k1gFnSc4	publikace
Katechismu	katechismus	k1gInSc2	katechismus
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
-	-	kIx~	-
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1992	[number]	k4	1992
Codex	Codex	k1gInSc1	Codex
Canonum	Canonum	k1gInSc4	Canonum
Ecclesiarum	Ecclesiarum	k1gNnSc4	Ecclesiarum
Orientalium	Orientalium	k1gNnSc4	Orientalium
promulgující	promulgující	k2eAgFnSc2d1	promulgující
<g />
.	.	kIx.	.
</s>
<s>
kodex	kodex	k1gInSc1	kodex
východního	východní	k2eAgNnSc2d1	východní
církevního	církevní	k2eAgNnSc2d1	církevní
práva	právo	k1gNnSc2	právo
-	-	kIx~	-
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1990	[number]	k4	1990
Ex	ex	k6eAd1	ex
Corde	Cord	k1gInSc5	Cord
Ecclesiae	Ecclesia	k1gMnPc4	Ecclesia
o	o	k7c6	o
katolických	katolický	k2eAgFnPc6d1	katolická
univerzitách	univerzita	k1gFnPc6	univerzita
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1990	[number]	k4	1990
Pastor	pastor	k1gMnSc1	pastor
Bonus	bonus	k1gInSc1	bonus
o	o	k7c6	o
římské	římský	k2eAgFnSc6d1	římská
kurii	kurie	k1gFnSc6	kurie
-	-	kIx~	-
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1988	[number]	k4	1988
Spirituali	Spirituali	k1gFnPc2	Spirituali
militum	militum	k1gNnSc4	militum
curae	curae	k1gNnSc2	curae
o	o	k7c6	o
duchovní	duchovní	k2eAgFnSc6d1	duchovní
vojenské	vojenský	k2eAgFnSc6d1	vojenská
péči	péče	k1gFnSc6	péče
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1986	[number]	k4	1986
Divinus	Divinus	k1gMnSc1	Divinus
Perfectionis	Perfectionis	k1gFnSc4	Perfectionis
Magister	magistra	k1gFnPc2	magistra
o	o	k7c6	o
<g />
.	.	kIx.	.
</s>
<s>
nové	nový	k2eAgFnSc3d1	nová
legislativě	legislativa	k1gFnSc3	legislativa
při	při	k7c6	při
beatifikaci	beatifikace	k1gFnSc6	beatifikace
a	a	k8xC	a
kanonizaci	kanonizace	k1gFnSc6	kanonizace
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1983	[number]	k4	1983
Sacrae	Sacrae	k1gNnPc2	Sacrae
Disciplinae	Disciplinae	k1gFnPc2	Disciplinae
Leges	Legesa	k1gFnPc2	Legesa
promulgující	promulgující	k2eAgInSc1d1	promulgující
nový	nový	k2eAgInSc1d1	nový
Kodex	kodex	k1gInSc1	kodex
kanonického	kanonický	k2eAgNnSc2d1	kanonické
práva	právo	k1gNnSc2	právo
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1983	[number]	k4	1983
Magnum	Magnum	k1gInSc1	Magnum
Matrimonii	matrimonium	k1gNnPc7	matrimonium
Sacramentum	Sacramentum	k1gNnSc4	Sacramentum
o	o	k7c6	o
definitivní	definitivní	k2eAgFnSc6d1	definitivní
podobě	podoba	k1gFnSc6	podoba
Papežského	papežský	k2eAgInSc2d1	papežský
institutu	institut	k1gInSc2	institut
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
o	o	k7c6	o
rodině	rodina	k1gFnSc6	rodina
a	a	k8xC	a
manželství	manželství	k1gNnSc6	manželství
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1982	[number]	k4	1982
Scripturarum	Scripturarum	k1gInSc1	Scripturarum
Thesaurus	thesaurus	k1gInSc4	thesaurus
promulgující	promulgující	k2eAgFnSc7d1	promulgující
novou	nova	k1gFnSc7	nova
<g />
.	.	kIx.	.
</s>
<s>
Vulgátu	Vulgáta	k1gFnSc4	Vulgáta
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1979	[number]	k4	1979
Sapientia	Sapientius	k1gMnSc2	Sapientius
Christiana	Christian	k1gMnSc2	Christian
o	o	k7c6	o
církevních	církevní	k2eAgFnPc6d1	církevní
univerzitách	univerzita	k1gFnPc6	univerzita
a	a	k8xC	a
fakultách	fakulta	k1gFnPc6	fakulta
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1979	[number]	k4	1979
Redemptor	Redemptor	k1gInSc4	Redemptor
hominis	hominis	k1gFnSc2	hominis
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1979	[number]	k4	1979
Dives	Dives	k1gMnSc1	Dives
in	in	k?	in
misericordia	misericordium	k1gNnSc2	misericordium
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1980	[number]	k4	1980
Laborem	Labor	k1gInSc7	Labor
exercens	exercens	k1gInSc1	exercens
-	-	kIx~	-
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1981	[number]	k4	1981
Slavorum	Slavorum	k1gInSc1	Slavorum
apostoli	apostoli	k6eAd1	apostoli
-	-	kIx~	-
Věnováno	věnován	k2eAgNnSc1d1	věnováno
sv.	sv.	kA	sv.
<g />
.	.	kIx.	.
</s>
<s>
Cyrilu	Cyril	k1gMnSc3	Cyril
a	a	k8xC	a
Metodějovi	Metoděj	k1gMnSc3	Metoděj
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1985	[number]	k4	1985
Dominum	Dominum	k1gInSc1	Dominum
et	et	k?	et
Vivificantem	Vivificant	k1gInSc7	Vivificant
-	-	kIx~	-
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1986	[number]	k4	1986
Redemptoris	Redemptoris	k1gFnSc1	Redemptoris
mater	mater	k1gFnSc1	mater
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1987	[number]	k4	1987
Sollicitudo	Sollicitudo	k1gNnSc4	Sollicitudo
rei	rei	k?	rei
socialis	socialis	k1gInSc1	socialis
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1987	[number]	k4	1987
Redemptoris	Redemptoris	k1gFnPc2	Redemptoris
missio	missio	k6eAd1	missio
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1990	[number]	k4	1990
Centesimus	Centesimus	k1gMnSc1	Centesimus
annus	annus	k1gMnSc1	annus
-	-	kIx~	-
K	K	kA	K
100	[number]	k4	100
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
encykliky	encyklika	k1gFnSc2	encyklika
papeže	papež	k1gMnSc2	papež
Lva	Lev	k1gMnSc2	Lev
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
</s>
<s>
Rerum	Rerum	k1gInSc1	Rerum
novarum	novarum	k1gInSc1	novarum
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1991	[number]	k4	1991
Veritatis	Veritatis	k1gFnSc2	Veritatis
splendor	splendora	k1gFnPc2	splendora
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1993	[number]	k4	1993
Evangelium	evangelium	k1gNnSc4	evangelium
vitae	vitae	k1gNnSc2	vitae
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1995	[number]	k4	1995
Ut	Ut	k1gMnSc1	Ut
unum	unum	k1gMnSc1	unum
sint	sint	k1gMnSc1	sint
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1995	[number]	k4	1995
Fides	Fides	k1gMnSc1	Fides
et	et	k?	et
ratio	ratio	k1gMnSc1	ratio
-	-	kIx~	-
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1998	[number]	k4	1998
Ecclesia	Ecclesium	k1gNnSc2	Ecclesium
de	de	k?	de
Eucharistia	Eucharistium	k1gNnSc2	Eucharistium
-	-	kIx~	-
O	o	k7c6	o
eucharistii	eucharistie	k1gFnSc6	eucharistie
a	a	k8xC	a
jejím	její	k3xOp3gInSc6	její
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
<g/>
|	|	kIx~	|
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
]]	]]	k?	]]
2003	[number]	k4	2003
Dar	dar	k1gInSc4	dar
a	a	k8xC	a
tajemství	tajemství	k1gNnSc1	tajemství
<g/>
:	:	kIx,	:
k	k	k7c3	k
padesátému	padesátý	k4xOgInSc3	padesátý
výročí	výročí	k1gNnSc3	výročí
mého	můj	k3xOp1gNnSc2	můj
kněžství	kněžství	k1gNnSc2	kněžství
Nebojte	bát	k5eNaImRp2nP	bát
se	se	k3xPyFc4	se
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
:	:	kIx,	:
dialog	dialog	k1gInSc4	dialog
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Pavlem	Pavel	k1gMnSc7	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Nebojme	bát	k5eNaImRp1nP	bát
se	se	k3xPyFc4	se
pravdy	pravda	k1gFnSc2	pravda
<g/>
:	:	kIx,	:
nedostatky	nedostatek	k1gInPc1	nedostatek
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
provinění	provinění	k1gNnSc2	provinění
církve	církev	k1gFnSc2	církev
Paměť	paměť	k1gFnSc1	paměť
a	a	k8xC	a
identita	identita	k1gFnSc1	identita
<g/>
:	:	kIx,	:
rozhovory	rozhovor	k1gInPc1	rozhovor
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
dvou	dva	k4xCgNnPc2	dva
tisíciletí	tisíciletí	k1gNnPc2	tisíciletí
Překročit	překročit	k5eAaPmF	překročit
práh	práh	k1gInSc4	práh
naděje	naděje	k1gFnSc2	naděje
(	(	kIx(	(
<g/>
kniha	kniha	k1gFnSc1	kniha
odpovědí	odpověď	k1gFnPc2	odpověď
na	na	k7c4	na
otázky	otázka	k1gFnPc4	otázka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
kladl	klást	k5eAaImAgMnS	klást
Vittorio	Vittorio	k1gMnSc1	Vittorio
Messori	Messor	k1gFnSc2	Messor
<g/>
)	)	kIx)	)
Římský	římský	k2eAgInSc1d1	římský
Triptych	triptych	k1gInSc1	triptych
-	-	kIx~	-
Meditace	meditace	k1gFnSc1	meditace
Teologie	teologie	k1gFnSc2	teologie
těla	tělo	k1gNnSc2	tělo
-	-	kIx~	-
soubor	soubor	k1gInSc1	soubor
katechezí	katecheze	k1gFnSc7	katecheze
Vstaňte	vstát	k5eAaPmRp2nP	vstát
<g/>
,	,	kIx,	,
pojďme	jít	k5eAaImRp1nP	jít
<g/>
!	!	kIx.	!
</s>
<s>
Prameny	pramen	k1gInPc1	pramen
a	a	k8xC	a
ruce	ruka	k1gFnPc1	ruka
-	-	kIx~	-
sbírka	sbírka	k1gFnSc1	sbírka
básní	báseň	k1gFnPc2	báseň
</s>
