<s>
První	první	k4xOgInSc1	první
patent	patent	k1gInSc1	patent
na	na	k7c4	na
nábojovou	nábojový	k2eAgFnSc4d1	nábojová
převodovku	převodovka	k1gFnSc4	převodovka
s	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
převody	převod	k1gInPc7	převod
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gMnSc1	jeho
výrobce	výrobce	k1gMnSc1	výrobce
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
.	.	kIx.	.
</s>
