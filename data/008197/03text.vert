<p>
<s>
Nábojová	nábojový	k2eAgFnSc1d1	nábojová
převodovka	převodovka	k1gFnSc1	převodovka
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
obvykle	obvykle	k6eAd1	obvykle
planetové	planetový	k2eAgFnPc4d1	planetová
převodovky	převodovka	k1gFnPc4	převodovka
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
pro	pro	k7c4	pro
variátor	variátor	k1gInSc4	variátor
<g/>
)	)	kIx)	)
zapouzdřené	zapouzdřený	k2eAgFnPc4d1	zapouzdřená
v	v	k7c6	v
nábojích	náboj	k1gInPc6	náboj
jízdních	jízdní	k2eAgFnPc2d1	jízdní
kol	kola	k1gFnPc2	kola
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
termín	termín	k1gInSc1	termín
zapouzdřená	zapouzdřený	k2eAgFnSc1d1	zapouzdřená
převodovka	převodovka	k1gFnSc1	převodovka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
obsluhy	obsluha	k1gFnSc2	obsluha
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
manuální	manuální	k2eAgFnSc4d1	manuální
převodovku	převodovka	k1gFnSc4	převodovka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výhody	výhoda	k1gFnPc1	výhoda
a	a	k8xC	a
nevýhody	nevýhoda	k1gFnPc1	nevýhoda
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
základní	základní	k2eAgFnPc4d1	základní
výhody	výhoda	k1gFnPc4	výhoda
zapouzdřených	zapouzdřený	k2eAgFnPc2d1	zapouzdřená
převodovek	převodovka	k1gFnPc2	převodovka
patří	patřit	k5eAaImIp3nS	patřit
právě	právě	k9	právě
zapouzdřenost	zapouzdřenost	k1gFnSc4	zapouzdřenost
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
které	který	k3yRgFnSc3	který
jsou	být	k5eAaImIp3nP	být
choulostivé	choulostivý	k2eAgFnSc2d1	choulostivá
části	část	k1gFnSc2	část
systému	systém	k1gInSc2	systém
chráněny	chráněn	k2eAgFnPc4d1	chráněna
před	před	k7c7	před
nečistotami	nečistota	k1gFnPc7	nečistota
<g/>
,	,	kIx,	,
deštěm	dešť	k1gInSc7	dešť
<g/>
,	,	kIx,	,
i	i	k9	i
před	před	k7c7	před
mechanickým	mechanický	k2eAgNnSc7d1	mechanické
poškozením	poškození	k1gNnSc7	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
používat	používat	k5eAaImF	používat
i	i	k9	i
silnější	silný	k2eAgInSc4d2	silnější
a	a	k8xC	a
odolnější	odolný	k2eAgInSc4d2	odolnější
řetěz	řetěz	k1gInSc4	řetěz
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
systémů	systém	k1gInPc2	systém
s	s	k7c7	s
přesmykači	přesmykač	k1gMnPc7	přesmykač
není	být	k5eNaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
k	k	k7c3	k
zadnímu	zadní	k2eAgNnSc3d1	zadní
kolu	kolo	k1gNnSc3	kolo
vešlo	vejít	k5eAaPmAgNnS	vejít
mnoho	mnoho	k4c4	mnoho
koleček	kolečko	k1gNnPc2	kolečko
o	o	k7c6	o
tloušťce	tloušťka	k1gFnSc6	tloušťka
řetězu	řetěz	k1gInSc2	řetěz
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
převodů	převod	k1gInPc2	převod
použito	použít	k5eAaPmNgNnS	použít
jen	jen	k9	jen
nábojové	nábojový	k2eAgFnSc2d1	nábojová
převodovky	převodovka	k1gFnSc2	převodovka
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
řetěz	řetěz	k1gInSc1	řetěz
vyšší	vysoký	k2eAgFnSc1d2	vyšší
životnost	životnost	k1gFnSc1	životnost
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
kroucení	kroucení	k1gNnSc3	kroucení
při	při	k7c6	při
přehazování	přehazování	k1gNnSc6	přehazování
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
údržba	údržba	k1gFnSc1	údržba
nábojové	nábojový	k2eAgFnSc2d1	nábojová
převodovky	převodovka	k1gFnSc2	převodovka
méně	málo	k6eAd2	málo
náročná	náročný	k2eAgFnSc1d1	náročná
na	na	k7c4	na
prostředky	prostředek	k1gInPc4	prostředek
i	i	k9	i
na	na	k7c4	na
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
půjčovny	půjčovna	k1gFnSc2	půjčovna
městských	městský	k2eAgFnPc2d1	městská
kol	kola	k1gFnPc2	kola
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Vélib	Vélib	k1gInSc1	Vélib
<g/>
'	'	kIx"	'
nebo	nebo	k8xC	nebo
Vélo	Vélo	k6eAd1	Vélo
<g/>
'	'	kIx"	'
<g/>
v	v	k7c6	v
používají	používat	k5eAaImIp3nP	používat
kola	kolo	k1gNnPc4	kolo
s	s	k7c7	s
nábojovými	nábojový	k2eAgFnPc7d1	nábojová
převodovkami	převodovka	k1gFnPc7	převodovka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
městského	městský	k2eAgInSc2d1	městský
provozu	provoz	k1gInSc2	provoz
je	být	k5eAaImIp3nS	být
další	další	k2eAgFnSc7d1	další
výhodou	výhoda	k1gFnSc7	výhoda
také	také	k9	také
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
řetěz	řetěz	k1gInSc1	řetěz
nepohybující	pohybující	k2eNgFnSc2d1	nepohybující
se	se	k3xPyFc4	se
do	do	k7c2	do
stran	strana	k1gFnPc2	strana
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
snadno	snadno	k6eAd1	snadno
zakrytovat	zakrytovat	k5eAaImF	zakrytovat
a	a	k8xC	a
tak	tak	k6eAd1	tak
chránit	chránit	k5eAaImF	chránit
oblečení	oblečení	k1gNnSc4	oblečení
městského	městský	k2eAgMnSc2d1	městský
cyklisty	cyklista	k1gMnSc2	cyklista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zásadní	zásadní	k2eAgFnSc7d1	zásadní
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
nábojových	nábojový	k2eAgFnPc2d1	nábojová
převodovek	převodovka	k1gFnPc2	převodovka
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
vysoká	vysoký	k2eAgFnSc1d1	vysoká
pořizovací	pořizovací	k2eAgFnSc1d1	pořizovací
cena	cena	k1gFnSc1	cena
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
varianty	varianta	k1gFnPc4	varianta
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
převody	převod	k1gInPc7	převod
mívají	mívat	k5eAaImIp3nP	mívat
horší	zlý	k2eAgFnSc1d2	horší
účinnost	účinnost	k1gFnSc1	účinnost
než	než	k8xS	než
adekvátní	adekvátní	k2eAgInSc1d1	adekvátní
dobře	dobře	k6eAd1	dobře
promazaný	promazaný	k2eAgInSc1d1	promazaný
a	a	k8xC	a
udržovaný	udržovaný	k2eAgInSc1d1	udržovaný
přesmykačový	přesmykačový	k2eAgInSc1d1	přesmykačový
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
o	o	k7c4	o
něco	něco	k3yInSc4	něco
těžší	těžký	k2eAgMnSc1d2	těžší
než	než	k8xS	než
přesmykačový	přesmykačový	k2eAgInSc1d1	přesmykačový
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
patent	patent	k1gInSc1	patent
na	na	k7c4	na
nábojovou	nábojový	k2eAgFnSc4d1	nábojová
převodovku	převodovka	k1gFnSc4	převodovka
s	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
převody	převod	k1gInPc7	převod
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gMnSc1	jeho
výrobce	výrobce	k1gMnSc1	výrobce
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k1gInSc4	William
Reilly	Reilla	k1gFnSc2	Reilla
si	se	k3xPyFc3	se
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
patentoval	patentovat	k5eAaBmAgInS	patentovat
podobný	podobný	k2eAgInSc1d1	podobný
dvoupřevodový	dvoupřevodový	k2eAgInSc1d1	dvoupřevodový
princip	princip	k1gInSc1	princip
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
ho	on	k3xPp3gNnSc4	on
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
'	'	kIx"	'
<g/>
The	The	k1gFnSc4	The
Hub	houba	k1gFnPc2	houba
<g/>
'	'	kIx"	'
začal	začít	k5eAaPmAgInS	začít
prodávat	prodávat	k5eAaImF	prodávat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
amerického	americký	k2eAgMnSc2d1	americký
předchůdce	předchůdce	k1gMnSc2	předchůdce
uspěl	uspět	k5eAaPmAgMnS	uspět
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
navázal	navázat	k5eAaPmAgInS	navázat
návrhem	návrh	k1gInSc7	návrh
převodovky	převodovka	k1gFnSc2	převodovka
s	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
převody	převod	k1gInPc7	převod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
různých	různý	k2eAgInPc2d1	různý
převodů	převod	k1gInPc2	převod
práv	právo	k1gNnPc2	právo
ji	on	k3xPp3gFnSc4	on
začala	začít	k5eAaPmAgFnS	začít
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
vyrábět	vyrábět	k5eAaImF	vyrábět
firma	firma	k1gFnSc1	firma
Sturmey	Sturmea	k1gFnSc2	Sturmea
Archer	Archra	k1gFnPc2	Archra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kontinentální	kontinentální	k2eAgFnSc6d1	kontinentální
Evropě	Evropa	k1gFnSc6	Evropa
začala	začít	k5eAaPmAgNnP	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
nábojové	nábojový	k2eAgFnPc4d1	nábojová
převodovky	převodovka	k1gFnPc4	převodovka
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
německá	německý	k2eAgFnSc1d1	německá
firma	firma	k1gFnSc1	firma
Fichtel	Fichtela	k1gFnPc2	Fichtela
und	und	k?	und
Sachs	Sachsa	k1gFnPc2	Sachsa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
už	už	k6eAd1	už
celkem	celkem	k6eAd1	celkem
14	[number]	k4	14
různých	různý	k2eAgInPc2d1	různý
modelů	model	k1gInPc2	model
nábojových	nábojový	k2eAgFnPc2d1	nábojová
převodovek	převodovka	k1gFnPc2	převodovka
s	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
převody	převod	k1gInPc7	převod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
pouze	pouze	k6eAd1	pouze
dvou	dva	k4xCgNnPc2	dva
<g/>
,	,	kIx,	,
tří	tři	k4xCgNnPc2	tři
a	a	k8xC	a
pěti	pět	k4xCc3	pět
převodové	převodový	k2eAgInPc1d1	převodový
náboje	náboj	k1gInPc1	náboj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
se	s	k7c7	s
zvyšující	zvyšující	k2eAgFnSc7d1	zvyšující
se	se	k3xPyFc4	se
poptávkou	poptávka	k1gFnSc7	poptávka
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
i	i	k9	i
nabídka	nabídka	k1gFnSc1	nabídka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
zakoupit	zakoupit	k5eAaPmF	zakoupit
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Sturmey-Archer	Sturmey-Archra	k1gFnPc2	Sturmey-Archra
tří	tři	k4xCgFnPc2	tři
<g/>
,	,	kIx,	,
pěti	pět	k4xCc2	pět
a	a	k8xC	a
osmi	osm	k4xCc2	osm
převodové	převodový	k2eAgNnSc4d1	převodové
převodovky	převodovka	k1gFnPc4	převodovka
<g/>
,	,	kIx,	,
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
SRAM	SRAM	kA	SRAM
(	(	kIx(	(
<g/>
která	který	k3yRgFnSc1	který
podědila	podědit	k5eAaPmAgFnS	podědit
znalosti	znalost	k1gFnPc4	znalost
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Fichtel	Fichtel	k1gInSc1	Fichtel
und	und	k?	und
Sachs	Sachs	k1gInSc1	Sachs
<g/>
)	)	kIx)	)
tří	tři	k4xCgFnPc2	tři
<g/>
,	,	kIx,	,
pěti	pět	k4xCc2	pět
<g/>
,	,	kIx,	,
sedmi	sedm	k4xCc2	sedm
a	a	k8xC	a
devítipřevodové	devítipřevodový	k2eAgFnPc4d1	devítipřevodový
převodovky	převodovka	k1gFnPc4	převodovka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Firma	firma	k1gFnSc1	firma
Rohloff	Rohloff	k1gInSc1	Rohloff
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
dokonce	dokonce	k9	dokonce
čtrnáctipřevodovou	čtrnáctipřevodový	k2eAgFnSc4d1	čtrnáctipřevodový
převodovku	převodovka	k1gFnSc4	převodovka
Rohloff	Rohloff	k1gMnSc1	Rohloff
Speedhub	Speedhub	k1gMnSc1	Speedhub
a	a	k8xC	a
firma	firma	k1gFnSc1	firma
NuVinci	NuVinec	k1gMnPc1	NuVinec
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
variátor	variátor	k1gInSc1	variátor
s	s	k7c7	s
plynulou	plynulý	k2eAgFnSc7d1	plynulá
změnou	změna	k1gFnSc7	změna
převodu	převod	k1gInSc2	převod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Novým	nový	k2eAgNnSc7d1	nové
uplatněním	uplatnění	k1gNnSc7	uplatnění
pro	pro	k7c4	pro
nábojové	nábojový	k2eAgFnPc4d1	nábojová
převodovky	převodovka	k1gFnPc4	převodovka
se	se	k3xPyFc4	se
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
stala	stát	k5eAaPmAgFnS	stát
skládací	skládací	k2eAgNnSc4d1	skládací
kola	kolo	k1gNnPc4	kolo
<g/>
,	,	kIx,	,
firmy	firma	k1gFnPc4	firma
Dahon	Dahona	k1gFnPc2	Dahona
i	i	k8xC	i
Brompton	Brompton	k1gInSc1	Brompton
Bicycle	Bicycle	k1gFnSc1	Bicycle
své	svůj	k3xOyFgInPc4	svůj
lepší	dobrý	k2eAgInPc4d2	lepší
modely	model	k1gInPc4	model
nabízejí	nabízet	k5eAaImIp3nP	nabízet
s	s	k7c7	s
nábojovými	nábojový	k2eAgFnPc7d1	nábojová
převodovkami	převodovka	k1gFnPc7	převodovka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
nábojová	nábojový	k2eAgFnSc1d1	nábojová
převodovka	převodovka	k1gFnSc1	převodovka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
NuVinci	NuVinec	k1gMnPc1	NuVinec
–	–	k?	–
revoluce	revoluce	k1gFnSc1	revoluce
v	v	k7c6	v
řazeníV	řazeníV	k?	řazeníV
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Hub	houba	k1gFnPc2	houba
gear	geara	k1gFnPc2	geara
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
