<p>
<s>
Rýžové	rýžový	k2eAgNnSc1d1	rýžové
mléko	mléko	k1gNnSc1	mléko
je	být	k5eAaImIp3nS	být
rostlinné	rostlinný	k2eAgNnSc4d1	rostlinné
mléko	mléko	k1gNnSc4	mléko
vyrobené	vyrobený	k2eAgNnSc4d1	vyrobené
z	z	k7c2	z
rýže	rýže	k1gFnSc2	rýže
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
z	z	k7c2	z
hnědé	hnědý	k2eAgFnSc2d1	hnědá
rýže	rýže	k1gFnSc2	rýže
a	a	k8xC	a
většinou	většina	k1gFnSc7	většina
je	být	k5eAaImIp3nS	být
nasládlé	nasládlý	k2eAgNnSc1d1	nasládlé
<g/>
.	.	kIx.	.
</s>
<s>
Sladkost	sladkost	k1gFnSc1	sladkost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
variantách	varianta	k1gFnPc6	varianta
rýžového	rýžový	k2eAgNnSc2d1	rýžové
mléka	mléko	k1gNnSc2	mléko
způsobena	způsoben	k2eAgFnSc1d1	způsobena
díky	díky	k7c3	díky
přirozenému	přirozený	k2eAgInSc3d1	přirozený
enzymatickému	enzymatický	k2eAgInSc3d1	enzymatický
procesu	proces	k1gInSc3	proces
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
štěpí	štěpit	k5eAaImIp3nS	štěpit
sacharidy	sacharid	k1gInPc4	sacharid
na	na	k7c4	na
cukry	cukr	k1gInPc4	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
rýžová	rýžový	k2eAgNnPc1d1	rýžové
mléka	mléko	k1gNnPc1	mléko
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
nicméně	nicméně	k8xC	nicméně
oslazena	oslazen	k2eAgNnPc4d1	oslazeno
cukrovým	cukrový	k2eAgInSc7d1	cukrový
sirupem	sirup	k1gInSc7	sirup
nebo	nebo	k8xC	nebo
jinými	jiný	k2eAgNnPc7d1	jiné
sladidly	sladidlo	k1gNnPc7	sladidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Srovnání	srovnání	k1gNnSc1	srovnání
s	s	k7c7	s
kravským	kravský	k2eAgNnSc7d1	kravské
mlékem	mléko	k1gNnSc7	mléko
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
kravským	kravský	k2eAgNnSc7d1	kravské
mlékem	mléko	k1gNnSc7	mléko
má	mít	k5eAaImIp3nS	mít
rýžové	rýžový	k2eAgNnSc4d1	rýžové
mléko	mléko	k1gNnSc4	mléko
více	hodně	k6eAd2	hodně
sacharidů	sacharid	k1gInPc2	sacharid
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
tak	tak	k6eAd1	tak
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
vápníku	vápník	k1gInSc2	vápník
<g/>
,	,	kIx,	,
bílkovin	bílkovina	k1gFnPc2	bílkovina
a	a	k8xC	a
žádný	žádný	k3yNgInSc4	žádný
cholesterol	cholesterol	k1gInSc4	cholesterol
ani	ani	k8xC	ani
laktózu	laktóza	k1gFnSc4	laktóza
<g/>
.	.	kIx.	.
</s>
<s>
Komerční	komerční	k2eAgFnSc2d1	komerční
značky	značka	k1gFnSc2	značka
častokrát	častokrát	k6eAd1	častokrát
do	do	k7c2	do
rýžového	rýžový	k2eAgNnSc2d1	rýžové
mléka	mléko	k1gNnSc2	mléko
přidávají	přidávat	k5eAaImIp3nP	přidávat
vitamíny	vitamín	k1gInPc4	vitamín
a	a	k8xC	a
minerály	minerál	k1gInPc4	minerál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rýžové	rýžový	k2eAgNnSc4d1	rýžové
mléko	mléko	k1gNnSc4	mléko
častokrát	častokrát	k6eAd1	častokrát
konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
intolerancí	intolerance	k1gFnSc7	intolerance
laktózy	laktóza	k1gFnSc2	laktóza
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
alergičtí	alergický	k2eAgMnPc1d1	alergický
na	na	k7c4	na
sóju	sója	k1gFnSc4	sója
nebo	nebo	k8xC	nebo
mléko	mléko	k1gNnSc4	mléko
nebo	nebo	k8xC	nebo
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
fenylketonurii	fenylketonurie	k1gFnSc4	fenylketonurie
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jej	on	k3xPp3gMnSc4	on
častokrát	častokrát	k6eAd1	častokrát
konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
vegani	vegan	k1gMnPc1	vegan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Rice	Ric	k1gInSc2	Ric
milk	milk	k6eAd1	milk
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
