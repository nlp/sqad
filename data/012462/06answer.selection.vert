<s>
Robert	Robert	k1gMnSc1	Robert
Andrews	Andrewsa	k1gFnPc2	Andrewsa
Millikan	Millikan	k1gMnSc1	Millikan
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1868	[number]	k4	1868
–	–	k?	–
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
primárně	primárně	k6eAd1	primárně
za	za	k7c4	za
výzkum	výzkum	k1gInSc4	výzkum
elementárního	elementární	k2eAgInSc2d1	elementární
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
a	a	k8xC	a
fotoelektrického	fotoelektrický	k2eAgInSc2d1	fotoelektrický
jevu	jev	k1gInSc2	jev
<g/>
.	.	kIx.	.
</s>
