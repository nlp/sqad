<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Andrews	Andrewsa	k1gFnPc2	Andrewsa
Millikan	Millikan	k1gMnSc1	Millikan
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1868	[number]	k4	1868
–	–	k?	–
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
primárně	primárně	k6eAd1	primárně
za	za	k7c4	za
výzkum	výzkum	k1gInSc4	výzkum
elementárního	elementární	k2eAgInSc2d1	elementární
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
a	a	k8xC	a
fotoelektrického	fotoelektrický	k2eAgInSc2d1	fotoelektrický
jevu	jev	k1gInSc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Millikan	Millikan	k1gMnSc1	Millikan
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
fyziku	fyzika	k1gFnSc4	fyzika
na	na	k7c6	na
Columbijské	Columbijský	k2eAgFnSc6d1	Columbijská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
získal	získat	k5eAaPmAgMnS	získat
doktorát	doktorát	k1gInSc4	doktorát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1910	[number]	k4	1910
–	–	k?	–
1921	[number]	k4	1921
byl	být	k5eAaImAgInS	být
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ředitelem	ředitel	k1gMnSc7	ředitel
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
laboratoře	laboratoř	k1gFnSc2	laboratoř
v	v	k7c6	v
Pasadeně	Pasadena	k1gFnSc6	Pasadena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Millikan	Millikan	k1gInSc1	Millikan
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
mechanikou	mechanika	k1gFnSc7	mechanika
<g/>
,	,	kIx,	,
molekulární	molekulární	k2eAgFnSc7d1	molekulární
fyzikou	fyzika	k1gFnSc7	fyzika
<g/>
,	,	kIx,	,
termofyzikou	termofyzika	k1gFnSc7	termofyzika
<g/>
,	,	kIx,	,
elektřinou	elektřina	k1gFnSc7	elektřina
<g/>
,	,	kIx,	,
akustikou	akustika	k1gFnSc7	akustika
a	a	k8xC	a
optikou	optika	k1gFnSc7	optika
<g/>
,	,	kIx,	,
měřením	měření	k1gNnSc7	měření
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
elektronu	elektron	k1gInSc2	elektron
<g/>
,	,	kIx,	,
přímým	přímý	k2eAgInSc7d1	přímý
určení	určení	k1gNnSc4	určení
kvanta	kvantum	k1gNnSc2	kvantum
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
studiem	studio	k1gNnSc7	studio
Brownova	Brownův	k2eAgInSc2d1	Brownův
pohybu	pohyb	k1gInSc2	pohyb
v	v	k7c6	v
plynech	plyn	k1gInPc6	plyn
<g/>
,	,	kIx,	,
expanzí	expanze	k1gFnPc2	expanze
ultrafialového	ultrafialový	k2eAgNnSc2d1	ultrafialové
spektra	spektrum	k1gNnSc2	spektrum
a	a	k8xC	a
pohybovými	pohybový	k2eAgInPc7d1	pohybový
zákony	zákon	k1gInPc7	zákon
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dopadají	dopadat	k5eAaImIp3nP	dopadat
z	z	k7c2	z
atmosféry	atmosféra	k1gFnSc2	atmosféra
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Millikan	Millikan	k1gInSc1	Millikan
byl	být	k5eAaImAgInS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
3	[number]	k4	3
syny	syn	k1gMnPc7	syn
a	a	k8xC	a
v	v	k7c6	v
soukromém	soukromý	k2eAgInSc6d1	soukromý
životě	život	k1gInSc6	život
byl	být	k5eAaImAgInS	být
nadšeným	nadšený	k2eAgMnSc7d1	nadšený
tenisovým	tenisový	k2eAgMnSc7d1	tenisový
hráčem	hráč	k1gMnSc7	hráč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Měření	měření	k1gNnPc1	měření
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
elektronu	elektron	k1gInSc2	elektron
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
změřil	změřit	k5eAaPmAgInS	změřit
Millikan	Millikan	k1gInSc1	Millikan
experimentálně	experimentálně	k6eAd1	experimentálně
velikost	velikost	k1gFnSc4	velikost
elementárního	elementární	k2eAgInSc2d1	elementární
náboje	náboj	k1gInSc2	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pokus	pokus	k1gInSc1	pokus
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
kterými	který	k3yQgFnPc7	který
působí	působit	k5eAaImIp3nP	působit
elektrostatické	elektrostatický	k2eAgFnPc1d1	elektrostatická
a	a	k8xC	a
gravitační	gravitační	k2eAgFnPc1d1	gravitační
pole	pole	k1gFnPc1	pole
na	na	k7c4	na
malé	malý	k2eAgFnPc4d1	malá
nabité	nabitý	k2eAgFnPc4d1	nabitá
kapičky	kapička	k1gFnPc4	kapička
oleje	olej	k1gInSc2	olej
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
desky	deska	k1gFnPc4	deska
kondenzátoru	kondenzátor	k1gInSc2	kondenzátor
(	(	kIx(	(
<g/>
dvě	dva	k4xCgFnPc1	dva
vodorovné	vodorovný	k2eAgFnPc1d1	vodorovná
desky	deska	k1gFnPc1	deska
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
kterými	který	k3yRgInPc7	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
elektrické	elektrický	k2eAgNnSc4d1	elektrické
pole	pole	k1gNnSc4	pole
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
vstřikovány	vstřikován	k2eAgFnPc1d1	vstřikována
olejové	olejový	k2eAgFnPc1d1	olejová
kapičky	kapička	k1gFnPc1	kapička
<g/>
.	.	kIx.	.
</s>
<s>
Mikroskopem	mikroskop	k1gInSc7	mikroskop
byl	být	k5eAaImAgInS	být
sledován	sledovat	k5eAaImNgInS	sledovat
jejich	jejich	k3xOp3gInSc1	jejich
vertikální	vertikální	k2eAgInSc1d1	vertikální
pohyb	pohyb	k1gInSc1	pohyb
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
elektrického	elektrický	k2eAgNnSc2d1	elektrické
pole	pole	k1gNnSc2	pole
a	a	k8xC	a
bez	bez	k7c2	bez
něho	on	k3xPp3gMnSc2	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Měření	měření	k1gNnPc1	měření
Planckovy	Planckův	k2eAgFnSc2d1	Planckova
konstanty	konstanta	k1gFnSc2	konstanta
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
přibližná	přibližný	k2eAgFnSc1d1	přibližná
hodnota	hodnota	k1gFnSc1	hodnota
Planckovy	Planckův	k2eAgFnSc2d1	Planckova
konstanty	konstanta	k1gFnSc2	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
až	až	k9	až
v	v	k7c6	v
letech	let	k1gInPc6	let
1913	[number]	k4	1913
–	–	k?	–
1916	[number]	k4	1916
v	v	k7c6	v
pokusu	pokus	k1gInSc6	pokus
navrženém	navržený	k2eAgInSc6d1	navržený
Robertem	Robert	k1gMnSc7	Robert
Millikanem	Millikan	k1gMnSc7	Millikan
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
přesnému	přesný	k2eAgNnSc3d1	přesné
určení	určení	k1gNnSc3	určení
<g/>
.	.	kIx.	.
</s>
<s>
Millikan	Millikan	k1gInSc1	Millikan
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
využil	využít	k5eAaPmAgInS	využít
fotoelektrický	fotoelektrický	k2eAgInSc4d1	fotoelektrický
jev	jev	k1gInSc4	jev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Millikan	Millikan	k1gMnSc1	Millikan
dokonale	dokonale	k6eAd1	dokonale
vyčistil	vyčistit	k5eAaPmAgMnS	vyčistit
povrch	povrch	k1gInSc4	povrch
fotocitlivého	fotocitlivý	k2eAgInSc2d1	fotocitlivý
materiálu	materiál	k1gInSc2	materiál
pomocí	pomocí	k7c2	pomocí
rotujícího	rotující	k2eAgInSc2d1	rotující
nože	nůž	k1gInSc2	nůž
ve	v	k7c6	v
vakuové	vakuový	k2eAgFnSc6d1	vakuová
komoře	komora	k1gFnSc6	komora
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
dopadající	dopadající	k2eAgNnSc1d1	dopadající
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
materiálu	materiál	k1gInSc2	materiál
vyráželo	vyrážet	k5eAaImAgNnS	vyrážet
elektrony	elektron	k1gInPc4	elektron
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc4	jejichž
kinetickou	kinetický	k2eAgFnSc4d1	kinetická
energii	energie	k1gFnSc4	energie
určil	určit	k5eAaPmAgInS	určit
pomocí	pomocí	k7c2	pomocí
elektrického	elektrický	k2eAgInSc2d1	elektrický
potenciálu	potenciál	k1gInSc2	potenciál
nutného	nutný	k2eAgNnSc2d1	nutné
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
zastavení	zastavení	k1gNnSc3	zastavení
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výpočtu	výpočet	k1gInSc6	výpočet
využil	využít	k5eAaPmAgMnS	využít
hodnotu	hodnota	k1gFnSc4	hodnota
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
sám	sám	k3xTgMnSc1	sám
experimentálně	experimentálně	k6eAd1	experimentálně
změřil	změřit	k5eAaPmAgMnS	změřit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
v	v	k7c6	v
experimentu	experiment	k1gInSc6	experiment
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
nabitých	nabitý	k2eAgFnPc2d1	nabitá
olejových	olejový	k2eAgFnPc2d1	olejová
kapek	kapka	k1gFnPc2	kapka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Einsteinovy	Einsteinův	k2eAgFnSc2d1	Einsteinova
rovnice	rovnice	k1gFnSc2	rovnice
popisující	popisující	k2eAgInSc1d1	popisující
fotoelektrický	fotoelektrický	k2eAgInSc1d1	fotoelektrický
jev	jev	k1gInSc1	jev
potom	potom	k6eAd1	potom
určil	určit	k5eAaPmAgInS	určit
Planckovu	Planckův	k2eAgFnSc4d1	Planckova
konstantu	konstanta	k1gFnSc4	konstanta
h	h	k?	h
=	=	kIx~	=
6,57	[number]	k4	6,57
×	×	k?	×
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
34	[number]	k4	34
J	J	kA	J
<g/>
·	·	k?	·
<g/>
s	s	k7c7	s
s	s	k7c7	s
tehdy	tehdy	k6eAd1	tehdy
vynikající	vynikající	k2eAgFnSc7d1	vynikající
přesností	přesnost	k1gFnSc7	přesnost
0,5	[number]	k4	0,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
SODOMKA	SODOMKA	kA	SODOMKA
L.	L.	kA	L.
<g/>
,	,	kIx,	,
SODOMKOVÁ	SODOMKOVÁ	kA	SODOMKOVÁ
M.	M.	kA	M.
<g/>
,	,	kIx,	,
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SET	set	k1gInSc1	set
OUT	OUT	kA	OUT
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-902058-5-2	[number]	k4	80-902058-5-2
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Robert	Robert	k1gMnSc1	Robert
Andrews	Andrewsa	k1gFnPc2	Andrewsa
Millikan	Millikana	k1gFnPc2	Millikana
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Robert	Robert	k1gMnSc1	Robert
Andrews	Andrews	k1gInSc1	Andrews
Millikan	Millikana	k1gFnPc2	Millikana
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
nobelprize	nobelprize	k1gFnSc1	nobelprize
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
