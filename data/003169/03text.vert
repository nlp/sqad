<s>
Čaj	čaj	k1gInSc1	čaj
(	(	kIx(	(
<g/>
z	z	k7c2	z
výslovnosti	výslovnost	k1gFnSc2	výslovnost
čínského	čínský	k2eAgMnSc4d1	čínský
茶	茶	k?	茶
-	-	kIx~	-
čcha	čcha	k1gMnSc1	čcha
v	v	k7c6	v
mandarínském	mandarínský	k2eAgInSc6d1	mandarínský
a	a	k8xC	a
kantonském	kantonský	k2eAgInSc6d1	kantonský
dialektu	dialekt	k1gInSc6	dialekt
<g/>
;	;	kIx,	;
v	v	k7c6	v
dialektu	dialekt	k1gInSc6	dialekt
min	mina	k1gFnPc2	mina
významného	významný	k2eAgInSc2d1	významný
přístavu	přístav	k1gInSc2	přístav
Sia-men	Siaen	k1gInSc1	Sia-men
čteno	číst	k5eAaImNgNnS	číst
te	te	k?	te
-	-	kIx~	-
odtud	odtud	k6eAd1	odtud
pojmenování	pojmenování	k1gNnSc1	pojmenování
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kulturní	kulturní	k2eAgInSc1d1	kulturní
nápoj	nápoj	k1gInSc1	nápoj
<g/>
,	,	kIx,	,
připravovaný	připravovaný	k2eAgInSc1d1	připravovaný
obvykle	obvykle	k6eAd1	obvykle
louhováním	louhování	k1gNnSc7	louhování
lístků	lístek	k1gInPc2	lístek
rostliny	rostlina	k1gFnSc2	rostlina
čajovníku	čajovník	k1gInSc2	čajovník
v	v	k7c6	v
horké	horký	k2eAgFnSc6d1	horká
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
samotné	samotný	k2eAgInPc4d1	samotný
lístky	lístek	k1gInPc4	lístek
se	se	k3xPyFc4	se
také	také	k9	také
užívá	užívat	k5eAaImIp3nS	užívat
název	název	k1gInSc1	název
čaj	čaj	k1gInSc1	čaj
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
čaj	čaj	k1gInSc1	čaj
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
užívá	užívat	k5eAaImIp3nS	užívat
i	i	k9	i
pro	pro	k7c4	pro
jiné	jiný	k2eAgInPc4d1	jiný
nálevy	nálev	k1gInPc4	nálev
-	-	kIx~	-
pro	pro	k7c4	pro
zápary	zápar	k1gInPc4	zápar
a	a	k8xC	a
odvary	odvar	k1gInPc4	odvar
různých	různý	k2eAgFnPc2d1	různá
jiných	jiný	k2eAgFnPc2d1	jiná
rostlin	rostlina	k1gFnPc2	rostlina
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
tzv.	tzv.	kA	tzv.
ovocné	ovocný	k2eAgInPc4d1	ovocný
čaje	čaj	k1gInPc4	čaj
<g/>
,	,	kIx,	,
bylinné	bylinný	k2eAgInPc4d1	bylinný
čaje	čaj	k1gInPc4	čaj
nebo	nebo	k8xC	nebo
šípkový	šípkový	k2eAgInSc4d1	šípkový
čaj	čaj	k1gInSc4	čaj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
nečaje	nečaj	k1gInPc1	nečaj
<g/>
.	.	kIx.	.
</s>
<s>
Čaj	čaj	k1gInSc1	čaj
má	mít	k5eAaImIp3nS	mít
vedle	vedle	k7c2	vedle
povzbuzujících	povzbuzující	k2eAgInPc2d1	povzbuzující
účinků	účinek	k1gInPc2	účinek
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
díky	díky	k7c3	díky
látkám	látka	k1gFnPc3	látka
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
obsaženým	obsažený	k2eAgMnSc7d1	obsažený
také	také	k6eAd1	také
blahodárný	blahodárný	k2eAgInSc4d1	blahodárný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
organismus	organismus	k1gInSc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Čaj	čaj	k1gInSc1	čaj
vzniká	vznikat	k5eAaImIp3nS	vznikat
zpracováním	zpracování	k1gNnSc7	zpracování
listů	list	k1gInPc2	list
čajovníku	čajovník	k1gInSc2	čajovník
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
botanického	botanický	k2eAgNnSc2d1	botanické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
čajovník	čajovník	k1gInSc1	čajovník
stálezelený	stálezelený	k2eAgInSc1d1	stálezelený
keř	keř	k1gInSc1	keř
<g/>
,	,	kIx,	,
dorůstající	dorůstající	k2eAgInSc1d1	dorůstající
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
výšky	výška	k1gFnSc2	výška
5	[number]	k4	5
až	až	k9	až
15	[number]	k4	15
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
až	až	k9	až
kolem	kolem	k7c2	kolem
třiceti	třicet	k4xCc2	třicet
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
plantážích	plantáž	k1gFnPc6	plantáž
jsou	být	k5eAaImIp3nP	být
keře	keř	k1gInPc1	keř
udržovány	udržován	k2eAgInPc1d1	udržován
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
kolem	kolem	k6eAd1	kolem
jednoho	jeden	k4xCgInSc2	jeden
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
to	ten	k3xDgNnSc1	ten
pohodlnější	pohodlný	k2eAgInSc1d2	pohodlnější
sběr	sběr	k1gInSc1	sběr
čajových	čajový	k2eAgInPc2d1	čajový
lístků	lístek	k1gInPc2	lístek
a	a	k8xC	a
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
úrodu	úroda	k1gFnSc4	úroda
<g/>
.	.	kIx.	.
</s>
<s>
Přirozeně	přirozeně	k6eAd1	přirozeně
čajovník	čajovník	k1gInSc1	čajovník
rostl	růst	k5eAaImAgInS	růst
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
při	při	k7c6	při
hranicích	hranice	k1gFnPc6	hranice
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
rostlina	rostlina	k1gFnSc1	rostlina
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
s	s	k7c7	s
příhodnými	příhodný	k2eAgFnPc7d1	příhodná
podmínkami	podmínka	k1gFnPc7	podmínka
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
známé	známý	k2eAgInPc4d1	známý
čaj	čaj	k1gInSc4	čaj
produkující	produkující	k2eAgFnSc2d1	produkující
země	zem	k1gFnSc2	zem
patří	patřit	k5eAaImIp3nS	patřit
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Pákistán	Pákistán	k1gInSc1	Pákistán
<g/>
,	,	kIx,	,
Írán	Írán	k1gInSc1	Írán
<g/>
,	,	kIx,	,
Srí	Srí	k1gFnSc1	Srí
Lanka	lanko	k1gNnSc2	lanko
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
Indonésie	Indonésie	k1gFnSc1	Indonésie
<g/>
,	,	kIx,	,
Nepál	Nepál	k1gInSc1	Nepál
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Argentina	Argentina	k1gFnSc1	Argentina
a	a	k8xC	a
Keňa	Keňa	k1gFnSc1	Keňa
<g/>
.	.	kIx.	.
</s>
<s>
Nejlépe	dobře	k6eAd3	dobře
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
daří	dařit	k5eAaImIp3nS	dařit
v	v	k7c6	v
monzunovém	monzunový	k2eAgNnSc6d1	monzunové
klimatu	klima	k1gNnSc6	klima
v	v	k7c6	v
subtropickém	subtropický	k2eAgInSc6d1	subtropický
a	a	k8xC	a
tropickém	tropický	k2eAgInSc6d1	tropický
pásu	pás	k1gInSc6	pás
<g/>
,	,	kIx,	,
roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
kyselých	kyselý	k2eAgFnPc6d1	kyselá
půdách	půda	k1gFnPc6	půda
<g/>
,	,	kIx,	,
od	od	k7c2	od
hladiny	hladina	k1gFnSc2	hladina
moře	moře	k1gNnSc2	moře
až	až	k9	až
po	po	k7c4	po
nadmořské	nadmořský	k2eAgFnPc4d1	nadmořská
výšky	výška	k1gFnPc4	výška
kolem	kolem	k7c2	kolem
2500	[number]	k4	2500
m.	m.	k?	m.
</s>
<s>
Listy	list	k1gInPc1	list
čajovníku	čajovník	k1gInSc2	čajovník
jsou	být	k5eAaImIp3nP	být
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
od	od	k7c2	od
tří	tři	k4xCgInPc2	tři
do	do	k7c2	do
pětadvaceti	pětadvacet	k4xCc2	pětadvacet
centimetrů	centimetr	k1gInPc2	centimetr
a	a	k8xC	a
široké	široký	k2eAgFnPc1d1	široká
mezi	mezi	k7c7	mezi
jedním	jeden	k4xCgNnSc7	jeden
a	a	k8xC	a
deseti	deset	k4xCc7	deset
centimetry	centimetr	k1gInPc7	centimetr
<g/>
,	,	kIx,	,
dospělé	dospělý	k2eAgInPc1d1	dospělý
listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
tlusté	tlustý	k2eAgFnPc1d1	tlustá
<g/>
,	,	kIx,	,
hladké	hladký	k2eAgFnPc1d1	hladká
a	a	k8xC	a
kožnaté	kožnatý	k2eAgFnPc1d1	kožnatá
<g/>
,	,	kIx,	,
s	s	k7c7	s
krátkým	krátký	k2eAgInSc7d1	krátký
řapíkem	řapík	k1gInSc7	řapík
<g/>
.	.	kIx.	.
</s>
<s>
Čajové	čajový	k2eAgInPc1d1	čajový
lístky	lístek	k1gInPc1	lístek
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
sbírají	sbírat	k5eAaImIp3nP	sbírat
několikrát	několikrát	k6eAd1	několikrát
do	do	k7c2	do
roka	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
prakticky	prakticky	k6eAd1	prakticky
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Lístky	lístek	k1gInPc1	lístek
se	se	k3xPyFc4	se
sbírají	sbírat	k5eAaImIp3nP	sbírat
podle	podle	k7c2	podle
takzvaných	takzvaný	k2eAgFnPc2d1	takzvaná
sběrových	sběrový	k2eAgFnPc2d1	sběrová
formulí	formule	k1gFnPc2	formule
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
určují	určovat	k5eAaImIp3nP	určovat
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
část	část	k1gFnSc1	část
výhonku	výhonek	k1gInSc2	výhonek
uštípnout	uštípnout	k5eAaPmF	uštípnout
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
jsou	být	k5eAaImIp3nP	být
cennější	cenný	k2eAgInPc1d2	cennější
mladší	mladý	k2eAgInPc1d2	mladší
menší	malý	k2eAgInPc1d2	menší
lístky	lístek	k1gInPc1	lístek
u	u	k7c2	u
špičky	špička	k1gFnSc2	špička
výhonku	výhonek	k1gInSc2	výhonek
a	a	k8xC	a
pupen	pupen	k1gInSc4	pupen
<g/>
.	.	kIx.	.
</s>
<s>
Čaj	čaj	k1gInSc1	čaj
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kofein	kofein	k1gInSc1	kofein
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
také	také	k9	také
jako	jako	k9	jako
tein	tein	k1gInSc4	tein
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
pil	pít	k5eAaImAgInS	pít
jako	jako	k9	jako
lék	lék	k1gInSc1	lék
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
zpracování	zpracování	k1gNnSc1	zpracování
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
faktorem	faktor	k1gInSc7	faktor
je	být	k5eAaImIp3nS	být
stupeň	stupeň	k1gInSc1	stupeň
oxidace	oxidace	k1gFnSc2	oxidace
(	(	kIx(	(
<g/>
z	z	k7c2	z
historických	historický	k2eAgInPc2d1	historický
důvodů	důvod	k1gInPc2	důvod
chybně	chybně	k6eAd1	chybně
označována	označován	k2eAgFnSc1d1	označována
jako	jako	k8xS	jako
fermentace	fermentace	k1gFnSc1	fermentace
<g/>
,	,	kIx,	,
nejde	jít	k5eNaImIp3nS	jít
zde	zde	k6eAd1	zde
však	však	k9	však
o	o	k7c6	o
působení	působení	k1gNnSc6	působení
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
oxidace	oxidace	k1gFnSc2	oxidace
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pozměnění	pozměnění	k1gNnSc4	pozměnění
některých	některý	k3yIgFnPc2	některý
molekul	molekula	k1gFnPc2	molekula
listu	list	k1gInSc2	list
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
chuťové	chuťový	k2eAgFnPc4d1	chuťová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
čaje	čaj	k1gInSc2	čaj
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
zhnědnutí	zhnědnutí	k1gNnSc1	zhnědnutí
<g/>
.	.	kIx.	.
</s>
<s>
Oxidace	oxidace	k1gFnSc1	oxidace
probíhá	probíhat	k5eAaImIp3nS	probíhat
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
obsah	obsah	k1gInSc1	obsah
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
lístku	lístek	k1gInSc6	lístek
neklesne	klesnout	k5eNaPmIp3nS	klesnout
na	na	k7c4	na
určitou	určitý	k2eAgFnSc4d1	určitá
mez	mez	k1gFnSc4	mez
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
i	i	k9	i
pojem	pojem	k1gInSc1	pojem
tradiční	tradiční	k2eAgFnSc2d1	tradiční
fermentace	fermentace	k1gFnSc2	fermentace
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
k	k	k7c3	k
určitému	určitý	k2eAgInSc3d1	určitý
druhu	druh	k1gInSc3	druh
čajů	čaj	k1gInPc2	čaj
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
k	k	k7c3	k
tajemnému	tajemný	k2eAgInSc3d1	tajemný
čaji	čaj	k1gInSc3	čaj
Pchu-er	Pchua	k1gFnPc2	Pchu-ra
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgNnSc2	jenž
dochází	docházet	k5eAaImIp3nS	docházet
nejdříve	dříve	k6eAd3	dříve
k	k	k7c3	k
oxidaci	oxidace	k1gFnSc3	oxidace
a	a	k8xC	a
poté	poté	k6eAd1	poté
k	k	k7c3	k
fermentaci	fermentace	k1gFnSc3	fermentace
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
čajů	čaj	k1gInPc2	čaj
je	být	k5eAaImIp3nS	být
poté	poté	k6eAd1	poté
možné	možný	k2eAgNnSc1d1	možné
nechat	nechat	k5eAaPmF	nechat
zrát	zrát	k5eAaImF	zrát
(	(	kIx(	(
<g/>
až	až	k6eAd1	až
desítky	desítka	k1gFnPc4	desítka
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čaje	čaj	k1gInPc1	čaj
rozdělujeme	rozdělovat	k5eAaImIp1nP	rozdělovat
dle	dle	k7c2	dle
způsobu	způsob	k1gInSc2	způsob
zpracování	zpracování	k1gNnSc4	zpracování
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
základních	základní	k2eAgFnPc2d1	základní
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
čaje	čaj	k1gInSc2	čaj
bílé	bílý	k2eAgNnSc1d1	bílé
(	(	kIx(	(
<g/>
bai	bai	k?	bai
cha	cha	k0	cha
<g/>
)	)	kIx)	)
-	-	kIx~	-
dochází	docházet	k5eAaImIp3nS	docházet
zde	zde	k6eAd1	zde
k	k	k7c3	k
jemné	jemný	k2eAgFnSc3d1	jemná
oxidaci	oxidace	k1gFnSc3	oxidace
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
samovolnou	samovolný	k2eAgFnSc4d1	samovolná
oxidaci	oxidace	k1gFnSc4	oxidace
probíhající	probíhající	k2eAgFnSc4d1	probíhající
během	během	k7c2	během
"	"	kIx"	"
<g/>
zavadání	zavadání	k1gNnSc2	zavadání
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čaj	čaj	k1gInSc1	čaj
je	být	k5eAaImIp3nS	být
následně	následně	k6eAd1	následně
sušen	sušen	k2eAgInSc1d1	sušen
<g/>
.	.	kIx.	.
</s>
<s>
Lístky	lístek	k1gInPc1	lístek
jsou	být	k5eAaImIp3nP	být
lehce	lehko	k6eAd1	lehko
nahnědlé	nahnědlý	k2eAgMnPc4d1	nahnědlý
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
oxidace	oxidace	k1gFnSc1	oxidace
<g/>
,	,	kIx,	,
části	část	k1gFnPc1	část
listu	list	k1gInSc2	list
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
zelené	zelený	k2eAgInPc1d1	zelený
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
takto	takto	k6eAd1	takto
zpracovávají	zpracovávat	k5eAaImIp3nP	zpracovávat
mladé	mladý	k2eAgInPc4d1	mladý
lístky	lístek	k1gInPc4	lístek
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
keře	keř	k1gInPc1	keř
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
zastíněné	zastíněný	k2eAgInPc1d1	zastíněný
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
omezilo	omezit	k5eAaPmAgNnS	omezit
množství	množství	k1gNnSc1	množství
chlorofylu	chlorofyl	k1gInSc2	chlorofyl
v	v	k7c6	v
listech	list	k1gInPc6	list
<g/>
.	.	kIx.	.
čaje	čaj	k1gInPc1	čaj
zelené	zelený	k2eAgInPc1d1	zelený
(	(	kIx(	(
<g/>
lu	lu	k?	lu
cha	cha	k0	cha
<g/>
)	)	kIx)	)
-	-	kIx~	-
prochází	procházet	k5eAaImIp3nS	procházet
minimální	minimální	k2eAgFnSc7d1	minimální
oxidací	oxidace	k1gFnSc7	oxidace
v	v	k7c6	v
době	doba	k1gFnSc6	doba
mezi	mezi	k7c7	mezi
utržením	utržení	k1gNnSc7	utržení
a	a	k8xC	a
zpracováním	zpracování	k1gNnSc7	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
Enzymy	enzym	k1gInPc1	enzym
způsobující	způsobující	k2eAgFnSc4d1	způsobující
oxidaci	oxidace	k1gFnSc4	oxidace
jsou	být	k5eAaImIp3nP	být
zničeny	zničit	k5eAaPmNgFnP	zničit
teplem	teplo	k1gNnSc7	teplo
<g/>
:	:	kIx,	:
užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
sušení	sušení	k1gNnSc4	sušení
na	na	k7c6	na
pánvích	pánev	k1gFnPc6	pánev
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
napařování	napařování	k1gNnSc1	napařování
<g/>
.	.	kIx.	.
</s>
<s>
Zpracování	zpracování	k1gNnSc1	zpracování
je	být	k5eAaImIp3nS	být
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
jeden	jeden	k4xCgMnSc1	jeden
až	až	k9	až
dva	dva	k4xCgInPc1	dva
dny	den	k1gInPc1	den
od	od	k7c2	od
sběru	sběr	k1gInSc2	sběr
<g/>
.	.	kIx.	.
</s>
<s>
Lístky	lístek	k1gInPc4	lístek
si	se	k3xPyFc3	se
uchovávají	uchovávat	k5eAaImIp3nP	uchovávat
svěží	svěží	k2eAgFnSc4d1	svěží
zeleň	zeleň	k1gFnSc4	zeleň
<g/>
.	.	kIx.	.
čaje	čaj	k1gInSc2	čaj
polozelené	polozelený	k2eAgNnSc1d1	polozelené
(	(	kIx(	(
<g/>
též	též	k9	též
oolongy	oolong	k1gInPc4	oolong
<g/>
,	,	kIx,	,
modrozelené	modrozelený	k2eAgNnSc4d1	modrozelené
<g/>
,	,	kIx,	,
žlutozelené	žlutozelený	k2eAgNnSc4d1	žlutozelené
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
modré	modrý	k2eAgFnPc1d1	modrá
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
烏	烏	k?	烏
<g/>
)	)	kIx)	)
-	-	kIx~	-
lístky	lístek	k1gInPc1	lístek
procházejí	procházet	k5eAaImIp3nP	procházet
oxidací	oxidace	k1gFnSc7	oxidace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
fázích	fág	k1gInPc6	fág
přerušena	přerušen	k2eAgFnSc1d1	přerušena
(	(	kIx(	(
<g/>
přechod	přechod	k1gInSc1	přechod
mezi	mezi	k7c7	mezi
zelenými	zelená	k1gFnPc7	zelená
a	a	k8xC	a
červenými	červená	k1gFnPc7	červená
či	či	k8xC	či
černými	černý	k2eAgInPc7d1	černý
čaji	čaj	k1gInPc7	čaj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
lístky	lístek	k1gInPc1	lístek
jsou	být	k5eAaImIp3nP	být
hnědé	hnědý	k2eAgFnPc1d1	hnědá
<g/>
,	,	kIx,	,
s	s	k7c7	s
lehkými	lehký	k2eAgInPc7d1	lehký
zeleným	zelený	k2eAgInSc7d1	zelený
odstínem	odstín	k1gInSc7	odstín
<g/>
.	.	kIx.	.
</s>
<s>
Sběr	sběr	k1gInSc1	sběr
lístků	lístek	k1gInPc2	lístek
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
výrobu	výroba	k1gFnSc4	výroba
musí	muset	k5eAaImIp3nP	muset
proběhnout	proběhnout	k5eAaPmF	proběhnout
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
jejich	jejich	k3xOp3gFnSc2	jejich
plné	plný	k2eAgFnSc2d1	plná
zralosti	zralost	k1gFnSc2	zralost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
čajový	čajový	k2eAgInSc1d1	čajový
nálev	nálev	k1gInSc1	nálev
získal	získat	k5eAaPmAgInS	získat
svou	svůj	k3xOyFgFnSc4	svůj
pověstnou	pověstný	k2eAgFnSc4d1	pověstná
vyváženou	vyvážený	k2eAgFnSc4d1	vyvážená
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
čaje	čaj	k1gInSc2	čaj
černé	černý	k2eAgNnSc1d1	černé
(	(	kIx(	(
<g/>
též	též	k9	též
červené	červený	k2eAgInPc4d1	červený
čaje	čaj	k1gInPc4	čaj
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
camelia	camelius	k1gMnSc4	camelius
sinnensis	sinnensis	k1gFnSc1	sinnensis
-	-	kIx~	-
čínský	čínský	k2eAgInSc1d1	čínský
čajovník	čajovník	k1gInSc1	čajovník
<g/>
)	)	kIx)	)
-	-	kIx~	-
lístky	lístek	k1gInPc1	lístek
se	se	k3xPyFc4	se
mechanicky	mechanicky	k6eAd1	mechanicky
naruší	narušit	k5eAaPmIp3nS	narušit
<g/>
,	,	kIx,	,
oxidace	oxidace	k1gFnSc1	oxidace
probíhá	probíhat	k5eAaImIp3nS	probíhat
za	za	k7c4	za
teploty	teplota	k1gFnPc4	teplota
okolo	okolo	k7c2	okolo
30	[number]	k4	30
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
velice	velice	k6eAd1	velice
vysoké	vysoký	k2eAgFnSc2d1	vysoká
vlhkosti	vlhkost	k1gFnSc2	vlhkost
<g/>
.	.	kIx.	.
</s>
<s>
Získáváme	získávat	k5eAaImIp1nP	získávat
lístek	lístek	k1gInSc4	lístek
na	na	k7c4	na
plně	plně	k6eAd1	plně
oxidovaný	oxidovaný	k2eAgInSc4d1	oxidovaný
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
na	na	k7c4	na
pohled	pohled	k1gInSc4	pohled
hnědý	hnědý	k2eAgInSc4d1	hnědý
až	až	k6eAd1	až
černý	černý	k2eAgInSc4d1	černý
<g/>
.	.	kIx.	.
</s>
<s>
Zpracování	zpracování	k1gNnSc1	zpracování
trvá	trvat	k5eAaImIp3nS	trvat
týden	týden	k1gInSc4	týden
i	i	k9	i
déle	dlouho	k6eAd2	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
výrazný	výrazný	k2eAgInSc1d1	výrazný
rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
mezi	mezi	k7c7	mezi
ortodoxním	ortodoxní	k2eAgNnSc7d1	ortodoxní
zpracováním	zpracování	k1gNnSc7	zpracování
a	a	k8xC	a
plně	plně	k6eAd1	plně
průmyslovou	průmyslový	k2eAgFnSc7d1	průmyslová
výrobou	výroba	k1gFnSc7	výroba
metodou	metoda	k1gFnSc7	metoda
CTC	CTC	kA	CTC
(	(	kIx(	(
<g/>
Crush	Crush	k1gMnSc1	Crush
<g/>
,	,	kIx,	,
Tear	Tear	k1gMnSc1	Tear
<g/>
,	,	kIx,	,
Curl	Curl	k1gMnSc1	Curl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
nadrcený	nadrcený	k2eAgInSc1d1	nadrcený
substrát	substrát	k1gInSc1	substrát
vhodný	vhodný	k2eAgInSc1d1	vhodný
do	do	k7c2	do
čajových	čajový	k2eAgInPc2d1	čajový
pytlíků	pytlík	k1gInPc2	pytlík
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
a	a	k8xC	a
"	"	kIx"	"
<g/>
západní	západní	k2eAgFnSc6d1	západní
civilizaci	civilizace	k1gFnSc6	civilizace
<g/>
"	"	kIx"	"
obecně	obecně	k6eAd1	obecně
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
historicky	historicky	k6eAd1	historicky
nejoblíbenější	oblíbený	k2eAgNnSc1d3	nejoblíbenější
právě	právě	k9	právě
čaj	čaj	k1gInSc4	čaj
černý	černý	k2eAgInSc4d1	černý
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
exotické	exotický	k2eAgInPc4d1	exotický
"	"	kIx"	"
<g/>
čaje	čaj	k1gInPc4	čaj
<g/>
"	"	kIx"	"
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
jihoamerické	jihoamerický	k2eAgNnSc4d1	jihoamerické
maté	maté	k1gNnSc4	maté
nebo	nebo	k8xC	nebo
lapacho	lapacho	k6eAd1	lapacho
<g/>
,	,	kIx,	,
jihoafrický	jihoafrický	k2eAgInSc4d1	jihoafrický
rooibos	rooibos	k1gInSc4	rooibos
a	a	k8xC	a
honeybush	honeybush	k1gInSc4	honeybush
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgInPc4d1	jiný
bylinné	bylinný	k2eAgInPc4d1	bylinný
čaje	čaj	k1gInPc4	čaj
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
čaj	čaj	k1gInSc1	čaj
zde	zde	k6eAd1	zde
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
přesné	přesný	k2eAgNnSc1d1	přesné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
tyto	tyt	k2eAgNnSc1d1	tyto
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
bylinné	bylinný	k2eAgInPc4d1	bylinný
čaje	čaj	k1gInPc4	čaj
<g/>
"	"	kIx"	"
nevyrábí	vyrábět	k5eNaImIp3nS	vyrábět
z	z	k7c2	z
čajovníku	čajovník	k1gInSc2	čajovník
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
u	u	k7c2	u
ručně	ručně	k6eAd1	ručně
zpracovaných	zpracovaný	k2eAgInPc2d1	zpracovaný
čajů	čaj	k1gInPc2	čaj
existuje	existovat	k5eAaImIp3nS	existovat
množství	množství	k1gNnSc1	množství
dalších	další	k2eAgFnPc2d1	další
nuancí	nuance	k1gFnPc2	nuance
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ve	v	k7c6	v
způsobu	způsob	k1gInSc6	způsob
svinování	svinování	k1gNnSc2	svinování
lístků	lístek	k1gInPc2	lístek
(	(	kIx(	(
<g/>
asi	asi	k9	asi
nejznámějším	známý	k2eAgInSc7d3	nejznámější
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
svinutí	svinutí	k1gNnSc1	svinutí
do	do	k7c2	do
pevných	pevný	k2eAgFnPc2d1	pevná
kuliček	kulička	k1gFnPc2	kulička
-	-	kIx~	-
perel	perla	k1gFnPc2	perla
-	-	kIx~	-
např.	např.	kA	např.
u	u	k7c2	u
čaje	čaj	k1gInSc2	čaj
"	"	kIx"	"
<g/>
Gunpowder	Gunpowder	k1gInSc1	Gunpowder
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
způsobů	způsob	k1gInPc2	způsob
zpracování	zpracování	k1gNnSc2	zpracování
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
do	do	k7c2	do
uvedených	uvedený	k2eAgFnPc2d1	uvedená
kategorií	kategorie	k1gFnPc2	kategorie
příliš	příliš	k6eAd1	příliš
nezapadají	zapadat	k5eNaImIp3nP	zapadat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těch	ten	k3xDgFnPc2	ten
známějších	známý	k2eAgFnPc2d2	známější
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
Pu-erh	Purh	k1gInSc4	Pu-erh
(	(	kIx(	(
<g/>
tmavý	tmavý	k2eAgInSc4d1	tmavý
čaj	čaj	k1gInSc4	čaj
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
hei	hei	k?	hei
cha	cha	k0	cha
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Popisuje	popisovat	k5eAaImIp3nS	popisovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
"	"	kIx"	"
<g/>
dvakrát	dvakrát	k6eAd1	dvakrát
fermentovaný	fermentovaný	k2eAgInSc1d1	fermentovaný
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
u	u	k7c2	u
"	"	kIx"	"
<g/>
druhé	druhý	k4xOgFnSc2	druhý
fermentace	fermentace	k1gFnSc2	fermentace
<g/>
"	"	kIx"	"
už	už	k6eAd1	už
opravdu	opravdu	k6eAd1	opravdu
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
působení	působení	k1gNnSc4	působení
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
až	až	k9	až
několikaletým	několikaletý	k2eAgNnSc7d1	několikaleté
zráním	zrání	k1gNnSc7	zrání
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
Žlutý	žlutý	k2eAgInSc4d1	žlutý
čaj	čaj	k1gInSc4	čaj
(	(	kIx(	(
<g/>
黄	黄	k?	黄
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
u	u	k7c2	u
zeleného	zelený	k2eAgInSc2d1	zelený
čaje	čaj	k1gInSc2	čaj
jsou	být	k5eAaImIp3nP	být
enzymy	enzym	k1gInPc1	enzym
způsobující	způsobující	k2eAgInPc1d1	způsobující
oxidaci	oxidace	k1gFnSc4	oxidace
zničeny	zničit	k5eAaPmNgFnP	zničit
teplem	teplo	k1gNnSc7	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
však	však	k9	však
nesuší	sušit	k5eNaImIp3nS	sušit
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
pozvolna	pozvolna	k6eAd1	pozvolna
v	v	k7c6	v
silných	silný	k2eAgFnPc6d1	silná
vrstvách	vrstva	k1gFnPc6	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Kukicha	Kukich	k1gMnSc4	Kukich
<g/>
:	:	kIx,	:
Řapíkový	řapíkový	k2eAgInSc1d1	řapíkový
čaj	čaj	k1gInSc1	čaj
<g/>
,	,	kIx,	,
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
produkt	produkt	k1gInSc1	produkt
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
japonského	japonský	k2eAgInSc2d1	japonský
práškového	práškový	k2eAgInSc2d1	práškový
čaje	čaj	k1gInSc2	čaj
Matcha	Matcha	k1gMnSc1	Matcha
<g/>
.	.	kIx.	.
</s>
<s>
Hojicha	Hojich	k1gMnSc4	Hojich
<g/>
:	:	kIx,	:
Japonský	japonský	k2eAgInSc1d1	japonský
čajový	čajový	k2eAgInSc1d1	čajový
produkt	produkt	k1gInSc1	produkt
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
pražením	pražení	k1gNnSc7	pražení
řapíků	řapík	k1gInPc2	řapík
<g/>
.	.	kIx.	.
</s>
<s>
Lapsang	Lapsang	k1gMnSc1	Lapsang
souchong	souchong	k1gMnSc1	souchong
(	(	kIx(	(
<g/>
Zheng	Zheng	k1gMnSc1	Zheng
shang	shang	k1gMnSc1	shang
xiao	xiao	k1gMnSc1	xiao
zhong	zhong	k1gMnSc1	zhong
nebo	nebo	k8xC	nebo
pravá	pravý	k2eAgFnSc1d1	pravá
malá	malý	k2eAgFnSc1d1	malá
horská	horský	k2eAgFnSc1d1	horská
odrůda	odrůda	k1gFnSc1	odrůda
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Černý	černý	k2eAgInSc1d1	černý
čaj	čaj	k1gInSc1	čaj
sušený	sušený	k2eAgInSc1d1	sušený
nad	nad	k7c7	nad
pálícím	pálící	k2eAgMnSc7d1	pálící
se	se	k3xPyFc4	se
borovým	borový	k2eAgNnSc7d1	borové
dřívím	dříví	k1gNnSc7	dříví
<g/>
,	,	kIx,	,
získává	získávat	k5eAaImIp3nS	získávat
specifické	specifický	k2eAgNnSc1d1	specifické
kouřové	kouřový	k2eAgNnSc1d1	kouřové
aroma	aroma	k1gNnSc1	aroma
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
U	u	k7c2	u
Lapsangů	Lapsang	k1gInPc2	Lapsang
pochybné	pochybný	k2eAgFnSc2d1	pochybná
kvality	kvalita	k1gFnSc2	kvalita
je	být	k5eAaImIp3nS	být
černý	černý	k2eAgInSc1d1	černý
čaj	čaj	k1gInSc1	čaj
zakouřen	zakouřen	k2eAgInSc1d1	zakouřen
dodatečně	dodatečně	k6eAd1	dodatečně
a	a	k8xC	a
aroma	aroma	k1gNnSc1	aroma
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
dehtové	dehtový	k2eAgNnSc1d1	dehtové
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Rize	Riga	k1gFnSc3	Riga
(	(	kIx(	(
<g/>
Çay	Çay	k1gFnSc1	Çay
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Čaj	čaj	k1gInSc1	čaj
z	z	k7c2	z
turecké	turecký	k2eAgFnSc2d1	turecká
provincie	provincie	k1gFnSc2	provincie
Rize	Riga	k1gFnSc3	Riga
<g/>
.	.	kIx.	.
</s>
<s>
Zvyk	zvyk	k1gInSc1	zvyk
pít	pít	k5eAaImF	pít
čaj	čaj	k1gInSc4	čaj
o	o	k7c6	o
páté	pátá	k1gFnSc6	pátá
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
v	v	k7c4	v
pět	pět	k4xCc4	pět
hodin	hodina	k1gFnPc2	hodina
odpoledne	odpoledne	k6eAd1	odpoledne
<g/>
)	)	kIx)	)
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
přisuzován	přisuzován	k2eAgInSc1d1	přisuzován
Anglii	Anglie	k1gFnSc3	Anglie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ho	on	k3xPp3gMnSc4	on
od	od	k7c2	od
Francouzů	Francouz	k1gMnPc2	Francouz
pouze	pouze	k6eAd1	pouze
převzala	převzít	k5eAaPmAgFnS	převzít
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jako	jako	k9	jako
čaje	čaj	k1gInPc4	čaj
o	o	k7c4	o
páté	pátá	k1gFnPc4	pátá
se	se	k3xPyFc4	se
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
za	za	k7c2	za
socialismu	socialismus	k1gInSc2	socialismus
označovala	označovat	k5eAaImAgFnS	označovat
setkávání	setkávání	k1gNnSc4	setkávání
mládeže	mládež	k1gFnSc2	mládež
(	(	kIx(	(
<g/>
hovorově	hovorově	k6eAd1	hovorově
mejdany	mejdan	k1gInPc1	mejdan
<g/>
,	,	kIx,	,
sleziny	slezina	k1gFnPc1	slezina
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nepil	pít	k5eNaImAgInS	pít
alkohol	alkohol	k1gInSc1	alkohol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
čaj	čaj	k1gInSc1	čaj
(	(	kIx(	(
<g/>
alkohol	alkohol	k1gInSc1	alkohol
se	se	k3xPyFc4	se
samozřejmě	samozřejmě	k6eAd1	samozřejmě
pil	pít	k5eAaImAgMnS	pít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oficiálně	oficiálně	k6eAd1	oficiálně
se	se	k3xPyFc4	se
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
nemluvilo	mluvit	k5eNaImAgNnS	mluvit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čaj	čaj	k1gInSc1	čaj
(	(	kIx(	(
<g/>
nápoj	nápoj	k1gInSc1	nápoj
<g/>
)	)	kIx)	)
Čajovna	čajovna	k1gFnSc1	čajovna
Čajový	čajový	k2eAgInSc1d1	čajový
sáček	sáček	k1gInSc4	sáček
Lisovaný	lisovaný	k2eAgInSc4d1	lisovaný
čaj	čaj	k1gInSc4	čaj
Kofein	kofein	k1gInSc4	kofein
Třísloviny	tříslovina	k1gFnSc2	tříslovina
Bílý	bílý	k2eAgInSc4d1	bílý
čaj	čaj	k1gInSc4	čaj
<g/>
:	:	kIx,	:
neoxidovaný	oxidovaný	k2eNgInSc4d1	neoxidovaný
Zelený	zelený	k2eAgInSc4d1	zelený
čaj	čaj	k1gInSc4	čaj
<g/>
:	:	kIx,	:
neoxidovaný	oxidovaný	k2eNgInSc4d1	neoxidovaný
Žlutý	žlutý	k2eAgInSc4d1	žlutý
čaj	čaj	k1gInSc4	čaj
<g/>
:	:	kIx,	:
neoxidovaný	oxidovaný	k2eNgInSc4d1	neoxidovaný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
do	do	k7c2	do
zeleného	zelené	k1gNnSc2	zelené
sušený	sušený	k2eAgInSc1d1	sušený
pomalu	pomalu	k6eAd1	pomalu
pod	pod	k7c7	pod
vrstvou	vrstva	k1gFnSc7	vrstva
lněné	lněný	k2eAgFnSc2d1	lněná
látky	látka	k1gFnSc2	látka
Polozelený	Polozelený	k2eAgInSc4d1	Polozelený
čaj	čaj	k1gInSc4	čaj
<g/>
:	:	kIx,	:
částečně	částečně	k6eAd1	částečně
oxidovaný	oxidovaný	k2eAgInSc1d1	oxidovaný
Černý	černý	k2eAgInSc1d1	černý
čaj	čaj	k1gInSc1	čaj
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
čaj	čaj	k1gInSc1	čaj
dokonale	dokonale	k6eAd1	dokonale
oxidovaný	oxidovaný	k2eAgInSc1d1	oxidovaný
Puerh	Puerh	k1gInSc1	Puerh
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
×	×	k?	×
oxidovaný	oxidovaný	k2eAgInSc4d1	oxidovaný
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
oxidovaný	oxidovaný	k2eAgMnSc1d1	oxidovaný
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
bakterií	bakterie	k1gFnSc7	bakterie
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
čaj	čaj	k1gInSc1	čaj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gFnSc1	téma
Čaj	čaj	k1gInSc1	čaj
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Slovníkové	slovníkový	k2eAgFnSc2d1	slovníková
heslo	heslo	k1gNnSc4	heslo
čaj	čaj	k1gInSc1	čaj
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
e-cajovna	eajovna	k1gFnSc1	e-cajovna
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
-	-	kIx~	-
Internetový	internetový	k2eAgInSc1d1	internetový
portál	portál	k1gInSc1	portál
o	o	k7c6	o
čaji	čaj	k1gInSc6	čaj
a	a	k8xC	a
čajovnách	čajovna	k1gFnPc6	čajovna
icajove-more	icajoveor	k1gInSc5	icajove-mor
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Čajový	čajový	k2eAgInSc1d1	čajový
magazín	magazín	k1gInSc1	magazín
<g/>
,	,	kIx,	,
mapa	mapa	k1gFnSc1	mapa
čajoven	čajovna	k1gFnPc2	čajovna
a	a	k8xC	a
obchodů	obchod	k1gInPc2	obchod
s	s	k7c7	s
čajem	čaj	k1gInSc7	čaj
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
co	co	k9	co
dělá	dělat	k5eAaImIp3nS	dělat
-	-	kIx~	-
Čaj	čaj	k1gInSc1	čaj
</s>
