<s>
Tylov	Tylov	k1gInSc1
</s>
<s>
Tylov	Tylov	k1gInSc4
CestaLokalita	CestaLokalita	k1gFnSc1
Charakter	charakter	k1gInSc1
</s>
<s>
malá	malý	k2eAgFnSc1d1
vesnice	vesnice	k1gFnSc1
Obec	obec	k1gFnSc1
</s>
<s>
Lomnice	Lomnice	k1gFnSc1
Okres	okres	k1gInSc1
</s>
<s>
Bruntál	Bruntál	k1gInSc1
Kraj	kraj	k1gInSc1
</s>
<s>
Moravskoslezský	moravskoslezský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Morava	Morava	k1gFnSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
53	#num#	k4
<g/>
′	′	k?
<g/>
44	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
25	#num#	k4
<g/>
′	′	k?
<g/>
41	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
73	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Katastrální	katastrální	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Tylov	Tylov	k1gInSc1
(	(	kIx(
<g/>
12	#num#	k4
km²	km²	k?
<g/>
)	)	kIx)
PSČ	PSČ	kA
</s>
<s>
793	#num#	k4
05	#num#	k4
Počet	počet	k1gInSc1
domů	domů	k6eAd1
</s>
<s>
40	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tylov	Tylov	k1gInSc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Kód	kód	k1gInSc4
části	část	k1gFnPc1
obce	obec	k1gFnSc2
</s>
<s>
86673	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tylov	Tylov	k1gInSc1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
též	též	k9
Ptejlov	Ptejlov	k1gInSc1
<g/>
,	,	kIx,
německy	německy	k6eAd1
Tillendorf	Tillendorf	k1gInSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
část	část	k1gFnSc1
obce	obec	k1gFnSc2
Lomnice	Lomnice	k1gFnSc2
v	v	k7c6
okrese	okres	k1gInSc6
Bruntál	Bruntál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tylov	Tylov	k1gInSc1
měl	mít	k5eAaImAgInS
ve	v	k7c6
znaku	znak	k1gInSc6
osmilistou	osmilistý	k2eAgFnSc4d1
růži	růže	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
Tylova	Tylův	k2eAgNnSc2d1
podle	podle	k7c2
sčítání	sčítání	k1gNnSc2
nebo	nebo	k8xC
jiných	jiný	k2eAgInPc2d1
úředních	úřední	k2eAgInPc2d1
záznamů	záznam	k1gInPc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
:	:	kIx,
</s>
<s>
Rok	rok	k1gInSc1
<g/>
1869188018901900191019211930195019611970198019912001	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
329348356352331353339	#num#	k4
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
<g/>
19317220013511881	#num#	k4
</s>
<s>
↑	↑	k?
z	z	k7c2
toho	ten	k3xDgNnSc2
<g/>
:	:	kIx,
12	#num#	k4
Čechoslováků	Čechoslovák	k1gMnPc2
<g/>
,	,	kIx,
327	#num#	k4
Němců	Němec	k1gMnPc2
<g/>
;	;	kIx,
335	#num#	k4
řím	řím	k?
<g/>
.	.	kIx.
kat	kat	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1	#num#	k4
evang	evanga	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2	#num#	k4
čsl	čsl	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Tylově	Tylův	k2eAgNnSc6d1
je	být	k5eAaImIp3nS
evidováno	evidovat	k5eAaImNgNnS
44	#num#	k4
adres	adresa	k1gFnPc2
<g/>
,	,	kIx,
vesměs	vesměs	k6eAd1
čísla	číslo	k1gNnPc1
popisná	popisný	k2eAgNnPc1d1
(	(	kIx(
<g/>
trvalé	trvalý	k2eAgInPc4d1
objekty	objekt	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
sčítání	sčítání	k1gNnSc6
lidu	lid	k1gInSc2
roku	rok	k1gInSc2
2001	#num#	k4
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
napočteno	napočíst	k5eAaPmNgNnS
41	#num#	k4
domů	dům	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
18	#num#	k4
trvale	trvale	k6eAd1
obydlených	obydlený	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Významné	významný	k2eAgFnPc1d1
osobnosti	osobnost	k1gFnPc1
</s>
<s>
V	v	k7c6
Tylově	Tylův	k2eAgNnSc6d1
se	s	k7c7
10	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1880	#num#	k4
narodil	narodit	k5eAaPmAgMnS
Gustav	Gustav	k1gMnSc1
Brauner	Brauner	k1gMnSc1
<g/>
,	,	kIx,
významný	významný	k2eAgMnSc1d1
malíř	malíř	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vystudování	vystudování	k1gNnSc6
malířské	malířský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
působil	působit	k5eAaImAgInS
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřel	zemřít	k5eAaPmAgMnS
3	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1966	#num#	k4
v	v	k7c6
Bavorsku	Bavorsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Firma	firma	k1gFnSc1
</s>
<s>
Chalupy	chalupa	k1gFnPc1
</s>
<s>
Bytovky	bytovka	k1gFnPc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
–	–	k?
1869	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HOSÁK	HOSÁK	kA
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historický	historický	k2eAgInSc1d1
místopis	místopis	k1gInSc1
země	zem	k1gFnSc2
Moravskoslezské	moravskoslezský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
1144	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
1225	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
605	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Seichterová	Seichterová	k1gFnSc1
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
:	:	kIx,
Razítka	razítko	k1gNnPc1
obcí	obec	k1gFnPc2
bývalého	bývalý	k2eAgNnSc2d1
sovineckého	sovinecký	k2eAgNnSc2d1
panství	panství	k1gNnSc2
do	do	k7c2
roku	rok	k1gInSc2
1951	#num#	k4
<g/>
,	,	kIx,
Střední	střední	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
33	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85807	#num#	k4
<g/>
-	-	kIx~
<g/>
52	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
98	#num#	k4
<g/>
-	-	kIx~
<g/>
113	#num#	k4
<g/>
↑	↑	k?
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
1869	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Balcar	Balcar	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
Havel	Havel	k1gMnSc1
<g/>
,	,	kIx,
Radek	Radek	k1gMnSc1
<g/>
;	;	kIx,
Křídlo	křídlo	k1gNnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
;	;	kIx,
Pavlíková	Pavlíková	k1gFnSc1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
;	;	kIx,
Růžková	Růžková	k1gFnSc1
<g/>
,	,	kIx,
Jiřina	Jiřina	k1gFnSc1
<g/>
;	;	kIx,
Šanda	Šanda	k1gMnSc1
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
<g/>
;	;	kIx,
Škrabal	Škrabal	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
svazky	svazek	k1gInPc4
(	(	kIx(
<g/>
760	#num#	k4
s.	s.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
250	#num#	k4
<g/>
-	-	kIx~
<g/>
1311	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
708	#num#	k4
<g/>
-	-	kIx~
<g/>
709	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Statistický	statistický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
v	v	k7c6
zemi	zem	k1gFnSc6
Moravskoslezské	moravskoslezský	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ministerstvo	ministerstvo	k1gNnSc1
vnitra	vnitro	k1gNnSc2
a	a	k8xC
Státní	státní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
statistický	statistický	k2eAgInSc1d1
<g/>
,	,	kIx,
1935	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
98	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Statistický	statistický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Růžková	Růžková	k1gFnSc1
<g/>
,	,	kIx,
Jiřina	Jiřina	k1gFnSc1
<g/>
;	;	kIx,
Morávková	Morávková	k1gFnSc1
<g/>
,	,	kIx,
Štěpánka	Štěpánka	k1gFnSc1
<g/>
;	;	kIx,
Škrabal	Škrabal	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
;	;	kIx,
Jungová	Jungová	k1gFnSc1
<g/>
,	,	kIx,
Galina	Galina	k1gFnSc1
<g/>
;	;	kIx,
Pavlíková	Pavlíková	k1gFnSc1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ottovo	Ottův	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
1360	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7360	#num#	k4
<g/>
-	-	kIx~
<g/>
287	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1078	#num#	k4
<g/>
-	-	kIx~
<g/>
1079	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1
vnitra	vnitro	k1gNnSc2
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adresy	adresa	k1gFnSc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-03-19	2010-03-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
PINKAVA	PINKAVA	k?
<g/>
,	,	kIx,
Viktor	Viktor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastivěda	vlastivěda	k1gFnSc1
moravská	moravský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místopis	místopis	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unčovský	Unčovský	k2eAgInSc4d1
a	a	k8xC
rýmařovský	rýmařovský	k2eAgInSc4d1
okres	okres	k1gInSc4
<g/>
..	..	k?
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Musejní	musejní	k2eAgInSc1d1
spolek	spolek	k1gInSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
1922	#num#	k4
<g/>
.	.	kIx.
393	#num#	k4
s.	s.	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Tylov	Tylovo	k1gNnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Části	část	k1gFnPc1
obce	obec	k1gFnSc2
Lomnice	Lomnice	k1gFnSc2
Části	část	k1gFnSc2
obce	obec	k1gFnSc2
</s>
<s>
LomniceTylov	LomniceTylov	k1gInSc1
</s>
