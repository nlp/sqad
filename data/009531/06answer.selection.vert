<s>
Mississippi	Mississippi	k1gFnSc1	Mississippi
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Mississippi	Mississippi	k1gFnSc7	Mississippi
River	Rivero	k1gNnPc2	Rivero
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
místních	místní	k2eAgMnPc2d1	místní
indiánů	indián	k1gMnPc2	indián
"	"	kIx"	"
<g/>
Velká	velký	k2eAgFnSc1d1	velká
řeka	řeka	k1gFnSc1	řeka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
řeka	řeka	k1gFnSc1	řeka
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
řek	řeka	k1gFnPc2	řeka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
protéká	protékat	k5eAaImIp3nS	protékat
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
americkými	americký	k2eAgMnPc7d1	americký
od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
a	a	k8xC	a
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
