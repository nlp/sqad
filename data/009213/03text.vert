<p>
<s>
Schwaz	Schwaz	k1gInSc1	Schwaz
je	být	k5eAaImIp3nS	být
okresní	okresní	k2eAgNnSc4d1	okresní
město	město	k1gNnSc4	město
okresu	okres	k1gInSc2	okres
Schwaz	Schwaz	k1gInSc4	Schwaz
v	v	k7c6	v
rakouské	rakouský	k2eAgFnSc6d1	rakouská
spolkové	spolkový	k2eAgFnSc6d1	spolková
zemi	zem	k1gFnSc6	zem
Tyrolsko	Tyrolsko	k1gNnSc1	Tyrolsko
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
14	[number]	k4	14
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Dle	dle	k7c2	dle
archeologických	archeologický	k2eAgInPc2d1	archeologický
nálezů	nález	k1gInPc2	nález
existovalo	existovat	k5eAaImAgNnS	existovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Schwazu	Schwaz	k1gInSc2	Schwaz
osídlení	osídlení	k1gNnSc2	osídlení
již	již	k6eAd1	již
v	v	k7c6	v
mladší	mladý	k2eAgFnSc3d2	mladší
době	doba	k1gFnSc3	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
(	(	kIx(	(
<g/>
4000	[number]	k4	4000
-	-	kIx~	-
2000	[number]	k4	2000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
bronzové	bronzový	k2eAgFnSc2d1	bronzová
byla	být	k5eAaImAgNnP	být
využívána	využívat	k5eAaPmNgNnP	využívat
ložiska	ložisko	k1gNnPc1	ložisko
měděných	měděný	k2eAgFnPc2d1	měděná
rud	ruda	k1gFnPc2	ruda
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
mezi	mezi	k7c7	mezi
Schwazem	Schwaz	k1gInSc7	Schwaz
a	a	k8xC	a
Kitzbühelem	Kitzbühel	k1gInSc7	Kitzbühel
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
bronzových	bronzový	k2eAgInPc2d1	bronzový
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Schwazu	Schwaz	k1gInSc6	Schwaz
z	z	k7c2	z
raného	raný	k2eAgInSc2d1	raný
středověku	středověk	k1gInSc2	středověk
jako	jako	k8xC	jako
o	o	k7c6	o
sídle	sídlo	k1gNnSc6	sídlo
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
Sû	Sû	k1gMnSc1	Sû
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
930	[number]	k4	930
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začal	začít	k5eAaPmAgMnS	začít
obrovský	obrovský	k2eAgInSc4d1	obrovský
rozvoj	rozvoj	k1gInSc4	rozvoj
města	město	k1gNnSc2	město
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
objevem	objev	k1gInSc7	objev
a	a	k8xC	a
těžbou	těžba	k1gFnSc7	těžba
stříbra	stříbro	k1gNnSc2	stříbro
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
lokalitě	lokalita	k1gFnSc6	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1420	[number]	k4	1420
byly	být	k5eAaImAgInP	být
otevřeny	otevřen	k2eAgInPc1d1	otevřen
stříbrné	stříbrný	k2eAgInPc1d1	stříbrný
doly	dol	k1gInPc1	dol
na	na	k7c4	na
Falkensteinu	Falkensteina	k1gFnSc4	Falkensteina
a	a	k8xC	a
nastal	nastat	k5eAaPmAgInS	nastat
příliv	příliv	k1gInSc1	příliv
horníků	horník	k1gMnPc2	horník
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
ze	z	k7c2	z
Saska	Sasko	k1gNnSc2	Sasko
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
stříbra	stříbro	k1gNnSc2	stříbro
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
těžila	těžit	k5eAaImAgFnS	těžit
i	i	k9	i
měď	měď	k1gFnSc1	měď
<g/>
,	,	kIx,	,
stříbro	stříbro	k1gNnSc1	stříbro
bylo	být	k5eAaImAgNnS	být
zdrojem	zdroj	k1gInSc7	zdroj
bohatství	bohatství	k1gNnSc2	bohatství
Habsburků	Habsburk	k1gInPc2	Habsburk
<g/>
,	,	kIx,	,
bankéřů	bankéř	k1gMnPc2	bankéř
a	a	k8xC	a
podnikatelů	podnikatel	k1gMnPc2	podnikatel
Fuggerů	Fugger	k1gMnPc2	Fugger
<g/>
,	,	kIx,	,
Paumgartnerů	Paumgartner	k1gMnPc2	Paumgartner
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
největšího	veliký	k2eAgInSc2d3	veliký
rozkvětu	rozkvět	k1gInSc2	rozkvět
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
Schwaz	Schwaz	k1gInSc1	Schwaz
nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
hornickým	hornický	k2eAgNnSc7d1	Hornické
střediskem	středisko	k1gNnSc7	středisko
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
20	[number]	k4	20
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
Vídni	Vídeň	k1gFnSc6	Vídeň
druhým	druhý	k4xOgNnSc7	druhý
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
v	v	k7c6	v
habsburské	habsburský	k2eAgFnSc6d1	habsburská
monarchii	monarchie	k1gFnSc6	monarchie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Bourg-de-Péage	Bourge-Péage	k1gFnSc1	Bourg-de-Péage
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
</s>
</p>
<p>
<s>
East	East	k2eAgInSc1d1	East
Grinstead	Grinstead	k1gInSc1	Grinstead
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
</s>
</p>
<p>
<s>
Mindelheim	Mindelheim	k1gInSc1	Mindelheim
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
</s>
</p>
<p>
<s>
Sant	Sant	k1gMnSc1	Sant
Feliu	Felius	k1gMnSc3	Felius
de	de	k?	de
Guíxols	Guíxols	k1gInSc1	Guíxols
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
</s>
</p>
<p>
<s>
Satu	Satu	k6eAd1	Satu
Mare	Mare	k1gFnSc1	Mare
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
</s>
</p>
<p>
<s>
Termeno	Termen	k2eAgNnSc1d1	Termeno
sulla	sulla	k6eAd1	sulla
strada	strada	k1gFnSc1	strada
del	del	k?	del
vino	vina	k1gFnSc5	vina
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc5	Itálie
</s>
</p>
<p>
<s>
Trento	Trento	k1gNnSc1	Trento
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
</s>
</p>
<p>
<s>
Verbania	Verbanium	k1gNnPc1	Verbanium
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Schwaz	Schwaz	k1gInSc1	Schwaz
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
a	a	k8xC	a
některé	některý	k3yIgFnPc4	některý
další	další	k2eAgFnPc4d1	další
informace	informace	k1gFnPc4	informace
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Schwaz	Schwaz	k1gInSc1	Schwaz
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Schwaz	Schwaz	k1gInSc1	Schwaz
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
města	město	k1gNnSc2	město
</s>
</p>
