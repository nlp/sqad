<s>
USS	USS	kA
New	New	k1gFnSc1
York	York	k1gInSc1
(	(	kIx(
<g/>
BB-	BB-	k1gFnSc1
<g/>
34	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Základní	základní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Typ	typ	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
dreadnought	dreadnought	k1gInSc1
</s>
<s>
Třída	třída	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
New	New	k?
York	York	k1gInSc1
</s>
<s>
Číslo	číslo	k1gNnSc1
trupu	trup	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
BB-34	BB-34	k4
</s>
<s>
Uživatelé	uživatel	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
US	US	kA
Navy	Navy	k?
</s>
<s>
Jméno	jméno	k1gNnSc1
podle	podle	k7c2
<g/>
:	:	kIx,
</s>
<s>
americký	americký	k2eAgInSc1d1
stát	stát	k1gInSc1
New	New	k1gFnPc2
York	York	k1gInSc1
</s>
<s>
Objednána	objednán	k2eAgFnSc1d1
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1911	#num#	k4
</s>
<s>
Zahájení	zahájení	k1gNnSc1
stavby	stavba	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1911	#num#	k4
</s>
<s>
Spuštěna	spuštěn	k2eAgNnPc1d1
na	na	k7c4
vodu	voda	k1gFnSc4
<g/>
:	:	kIx,
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1912	#num#	k4
</s>
<s>
Uvedena	uveden	k2eAgFnSc1d1
do	do	k7c2
služby	služba	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1914	#num#	k4
</s>
<s>
Osud	osud	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
potopena	potopen	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
cvičný	cvičný	k2eAgInSc1d1
cíl	cíl	k1gInSc1
<g/>
,	,	kIx,
1948	#num#	k4
</s>
<s>
Takticko-technická	takticko-technický	k2eAgNnPc1d1
data	datum	k1gNnPc1
</s>
<s>
Výtlak	výtlak	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
27	#num#	k4
433	#num#	k4
t	t	k?
(	(	kIx(
<g/>
standardní	standardní	k2eAgFnSc7d1
<g/>
)	)	kIx)
<g/>
28	#num#	k4
822	#num#	k4
t	t	k?
(	(	kIx(
<g/>
plný	plný	k2eAgInSc4d1
<g/>
)	)	kIx)
</s>
<s>
Délka	délka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
172,2	172,2	k4
m	m	kA
(	(	kIx(
<g/>
vodoryska	vodoryska	k1gFnSc1
<g/>
)	)	kIx)
<g/>
174,7	174,7	k4
m	m	kA
(	(	kIx(
<g/>
max	max	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Šířka	šířka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
29,02	29,02	k4
m	m	kA
</s>
<s>
Ponor	ponor	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
8,7	8,7	k4
m	m	kA
(	(	kIx(
<g/>
průměrný	průměrný	k2eAgMnSc1d1
<g/>
)	)	kIx)
9	#num#	k4
m	m	kA
(	(	kIx(
<g/>
max	max	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Pohon	pohon	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
14	#num#	k4
<g/>
×	×	k?
Babcock	Babcock	k1gInSc1
&	&	k?
Wilcox	Wilcox	k1gInSc1
<g/>
28	#num#	k4
100	#num#	k4
koní	kůň	k1gMnPc2
</s>
<s>
Palivo	palivo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
uhlí	uhlí	k1gNnSc1
(	(	kIx(
<g/>
později	pozdě	k6eAd2
nafta	nafta	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Rychlost	rychlost	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
21	#num#	k4
uzlů	uzel	k1gInPc2
(	(	kIx(
<g/>
39	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
</s>
<s>
Posádka	posádka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
042	#num#	k4
důstojníků	důstojník	k1gMnPc2
a	a	k8xC
námořníků	námořník	k1gMnPc2
</s>
<s>
Pancíř	pancíř	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
boky	boka	k1gFnPc1
<g/>
:	:	kIx,
254	#num#	k4
mm-	mm-	k?
<g/>
305	#num#	k4
mmpaluba	mmpaluba	k1gFnSc1
<g/>
:	:	kIx,
51	#num#	k4
mmbarbety	mmbarbeta	k1gFnPc1
<g/>
:	:	kIx,
254	#num#	k4
mm-	mm-	k?
<g/>
305	#num#	k4
mmvelitelská	mmvelitelský	k2eAgFnSc1d1
věž	věž	k1gFnSc1
<g/>
:	:	kIx,
102	#num#	k4
mm-	mm-	k?
<g/>
305	#num#	k4
mm	mm	kA
</s>
<s>
Výzbroj	výzbroj	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
(	(	kIx(
<g/>
1914	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
×	×	k?
356	#num#	k4
<g/>
mm	mm	kA
kanón	kanón	k1gInSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
×	×	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
21	#num#	k4
<g/>
×	×	k?
127	#num#	k4
<g/>
mm	mm	kA
kanón	kanón	k1gInSc1
(	(	kIx(
<g/>
21	#num#	k4
<g/>
×	×	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
×	×	k?
47	#num#	k4
<g/>
mm	mm	kA
kanón	kanón	k1gInSc4
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
4	#num#	k4
<g/>
×	×	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
×	×	k?
37	#num#	k4
<g/>
mm	mm	kA
kanón	kanón	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
×	×	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
×	×	k?
533	#num#	k4
<g/>
mm	mm	kA
torpédomet	torpédomet	k1gInSc1
<g/>
(	(	kIx(
<g/>
1926	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
<g/>
10	#num#	k4
<g/>
×	×	k?
356	#num#	k4
<g/>
mm	mm	kA
kanón	kanón	k1gInSc4
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
5	#num#	k4
<g/>
×	×	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
16	#num#	k4
<g/>
×	×	k?
127	#num#	k4
<g/>
mm	mm	kA
kanón	kanón	k1gInSc1
(	(	kIx(
<g/>
16	#num#	k4
<g/>
×	×	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
8	#num#	k4
<g/>
×	×	k?
76	#num#	k4
<g/>
mm	mm	kA
kanón	kanón	k1gInSc1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
×	×	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
1942	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
10	#num#	k4
<g/>
×	×	k?
356	#num#	k4
<g/>
mm	mm	kA
kanón	kanón	k1gInSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
×	×	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
6	#num#	k4
<g/>
×	×	k?
127	#num#	k4
<g/>
mm	mm	kA
kanón	kanón	k1gInSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
×	×	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
10	#num#	k4
<g/>
×	×	k?
76	#num#	k4
<g/>
mm	mm	kA
kanón	kanón	k1gInSc1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
×	×	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
24	#num#	k4
<g/>
×	×	k?
40	#num#	k4
<g/>
mm	mm	kA
kanón	kanón	k1gInSc1
Bofors	Bofors	k1gInSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
×	×	k?
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
42	#num#	k4
<g/>
×	×	k?
20	#num#	k4
<g/>
mm	mm	kA
kanón	kanón	k1gInSc1
Oerlikon	Oerlikon	k1gInSc1
(	(	kIx(
<g/>
42	#num#	k4
<g/>
×	×	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Letadla	letadlo	k1gNnPc1
<g/>
:	:	kIx,
</s>
<s>
(	(	kIx(
<g/>
1926	#num#	k4
<g/>
-	-	kIx~
<g/>
1942	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
×	×	k?
hydroplán	hydroplán	k1gInSc1
</s>
<s>
Operační	operační	k2eAgNnSc1d1
nasazení	nasazení	k1gNnSc1
</s>
<s>
Nasazení	nasazení	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
První	první	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válkaDruhá	válkaDruhý	k2eAgFnSc1d1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
USS	USS	kA
New	New	k1gFnSc1
York	York	k1gInSc1
(	(	kIx(
<g/>
BB-	BB-	k1gFnSc1
<g/>
34	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
dreadnought	dreadnought	k1gInSc1
Námořnictva	námořnictvo	k1gNnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
a	a	k8xC
vedoucí	vedoucí	k2eAgFnSc4d1
loď	loď	k1gFnSc4
své	svůj	k3xOyFgFnSc2
třídy	třída	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jméno	jméno	k1gNnSc1
nesla	nést	k5eAaImAgFnS
po	po	k7c6
státu	stát	k1gInSc6
New	New	k1gFnSc1
York	York	k1gInSc1
a	a	k8xC
byla	být	k5eAaImAgFnS
navržena	navrhnout	k5eAaPmNgFnS
jako	jako	k9
první	první	k4xOgFnSc1
loď	loď	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
nesla	nést	k5eAaImAgFnS
děla	dělo	k1gNnSc2
ráže	ráže	k1gFnSc2
356	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
z	z	k7c2
ní	on	k3xPp3gFnSc2
tehdy	tehdy	k6eAd1
dělalo	dělat	k5eAaImAgNnS
jednu	jeden	k4xCgFnSc4
z	z	k7c2
nejsilnějších	silný	k2eAgFnPc2d3
lodí	loď	k1gFnPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výzbroj	výzbroj	k1gFnSc1
(	(	kIx(
<g/>
1914	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Americká	americký	k2eAgFnSc1d1
bitevní	bitevní	k2eAgFnSc1d1
loď	loď	k1gFnSc1
New	New	k1gFnSc1
York	York	k1gInSc1
byla	být	k5eAaImAgFnS
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
dobu	doba	k1gFnSc4
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
vyzbrojena	vyzbrojit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc4d1
zbraňový	zbraňový	k2eAgInSc4d1
systém	systém	k1gInSc4
tvořilo	tvořit	k5eAaImAgNnS
5	#num#	k4
dvojhlavňových	dvojhlavňový	k2eAgFnPc2d1
střeleckých	střelecký	k2eAgFnPc2d1
věží	věž	k1gFnPc2
s	s	k7c7
děly	dělo	k1gNnPc7
ráže	ráže	k1gFnSc2
356	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šrapnely	šrapnel	k1gInPc1
vystřelené	vystřelený	k2eAgInPc1d1
z	z	k7c2
těchto	tento	k3xDgNnPc2
mocných	mocný	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
mohly	moct	k5eAaImAgInP
zasáhnout	zasáhnout	k5eAaPmF
cíl	cíl	k1gInSc4
až	až	k9
20	#num#	k4
km	km	kA
vzdálený	vzdálený	k2eAgMnSc1d1
a	a	k8xC
na	na	k7c6
místě	místo	k1gNnSc6
dopadu	dopad	k1gInSc2
byly	být	k5eAaImAgFnP
schopny	schopen	k2eAgFnPc1d1
udělat	udělat	k5eAaPmF
až	až	k9
3	#num#	k4
metrový	metrový	k2eAgInSc4d1
kráter	kráter	k1gInSc4
do	do	k7c2
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc4
hmotnost	hmotnost	k1gFnSc4
byla	být	k5eAaImAgFnS
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
600	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
21	#num#	k4
děl	dělo	k1gNnPc2
ráže	ráže	k1gFnSc1
127	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
měla	mít	k5eAaImAgFnS
dostřel	dostřel	k1gInSc4
16	#num#	k4
km	km	kA
a	a	k8xC
šrapnely	šrapnel	k1gInPc1
měly	mít	k5eAaImAgInP
hmotnost	hmotnost	k1gFnSc4
25	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
nesměly	smět	k5eNaImAgInP
chybět	chybět	k5eAaImF
4	#num#	k4
kanóny	kanón	k1gInPc1
QF	QF	kA
3	#num#	k4
<g/>
-pounder	-poundero	k1gNnPc2
ráže	ráže	k1gFnSc1
47	#num#	k4
mm	mm	kA
a	a	k8xC
2	#num#	k4
kanóny	kanón	k1gInPc1
QF	QF	kA
1	#num#	k4
<g/>
-pounder	-poundero	k1gNnPc2
ráže	ráže	k1gFnSc1
37	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
zde	zde	k6eAd1
byly	být	k5eAaImAgFnP
i	i	k9
4	#num#	k4
torpédomety	torpédomet	k1gInPc4
s	s	k7c7
torpédy	torpédo	k1gNnPc7
Mk	Mk	k1gMnPc2
3	#num#	k4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
byli	být	k5eAaImAgMnP
5	#num#	k4
metrů	metr	k1gInPc2
dlouhé	dlouhý	k2eAgNnSc1d1
<g/>
,	,	kIx,
na	na	k7c4
průměr	průměr	k1gInSc4
měřily	měřit	k5eAaImAgFnP
533	#num#	k4
mm	mm	kA
a	a	k8xC
ve	v	k7c6
vodě	voda	k1gFnSc6
dosahovali	dosahovat	k5eAaImAgMnP
maximální	maximální	k2eAgFnSc3d1
rychlosti	rychlost	k1gFnSc3
26	#num#	k4
uzlů	uzel	k1gInPc2
(	(	kIx(
<g/>
48	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Do	do	k7c2
služby	služba	k1gFnSc2
vstoupila	vstoupit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1914	#num#	k4
a	a	k8xC
byla	být	k5eAaImAgFnS
součástí	součást	k1gFnSc7
sil	síla	k1gFnPc2
amerického	americký	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
byly	být	k5eAaImAgFnP
vyslány	vyslat	k5eAaPmNgFnP
k	k	k7c3
posílení	posílení	k1gNnSc3
britské	britský	k2eAgFnSc2d1
Grand	grand	k1gMnSc1
Fleet	Fleet	k1gInSc1
v	v	k7c6
Severním	severní	k2eAgNnSc6d1
moři	moře	k1gNnSc6
těsně	těsně	k6eAd1
před	před	k7c7
koncem	konec	k1gInSc7
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
byla	být	k5eAaImAgFnS
účastníkem	účastník	k1gMnSc7
nejméně	málo	k6eAd3
dvou	dva	k4xCgInPc2
incidentů	incident	k1gInPc2
s	s	k7c7
německými	německý	k2eAgFnPc7d1
ponorkami	ponorka	k1gFnPc7
a	a	k8xC
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
jedinou	jediný	k2eAgFnSc4d1
americkou	americký	k2eAgFnSc4d1
loď	loď	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
potopila	potopit	k5eAaPmAgFnS
ve	v	k7c6
válce	válka	k1gFnSc6
ponorku	ponorka	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
během	během	k7c2
náhodné	náhodný	k2eAgFnSc2d1
srážky	srážka	k1gFnSc2
v	v	k7c6
říjnu	říjen	k1gInSc6
1918	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
válce	válka	k1gFnSc6
byla	být	k5eAaImAgFnS
poslána	poslat	k5eAaPmNgFnS
na	na	k7c4
sérii	série	k1gFnSc4
cvičení	cvičení	k1gNnSc2
a	a	k8xC
plavby	plavba	k1gFnSc2
v	v	k7c6
Atlantiku	Atlantik	k1gInSc6
i	i	k8xC
Tichomoří	Tichomoří	k1gNnSc6
a	a	k8xC
prodělala	prodělat	k5eAaPmAgFnS
několik	několik	k4yIc4
generálních	generální	k2eAgFnPc2d1
oprav	oprava	k1gFnPc2
pro	pro	k7c4
zvýšení	zvýšení	k1gNnSc4
její	její	k3xOp3gFnSc2
výzbroje	výzbroj	k1gFnSc2
<g/>
,	,	kIx,
ovládání	ovládání	k1gNnSc1
letadel	letadlo	k1gNnPc2
a	a	k8xC
pancéřování	pancéřování	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Modernizace	modernizace	k1gFnSc1
lodi	loď	k1gFnSc2
(	(	kIx(
<g/>
1926	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Po	po	k7c6
uzavření	uzavření	k1gNnSc6
Washingtonské	washingtonský	k2eAgFnSc2d1
dohody	dohoda	k1gFnSc2
musely	muset	k5eAaImAgInP
státy	stát	k1gInPc1
jako	jako	k8xC,k8xS
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
<g/>
,	,	kIx,
Japonsko	Japonsko	k1gNnSc1
<g/>
,	,	kIx,
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
a	a	k8xC
Itálie	Itálie	k1gFnSc1
přestat	přestat	k5eAaPmF
vyrábět	vyrábět	k5eAaImF
válečné	válečný	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
po	po	k7c4
dobu	doba	k1gFnSc4
následujících	následující	k2eAgInPc2d1
10	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spousta	spousta	k6eAd1
amerických	americký	k2eAgFnPc2d1
válečných	válečný	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
byly	být	k5eAaImAgInP
modernizovány	modernizován	k2eAgInPc1d1
místo	místo	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
stavěli	stavět	k5eAaImAgMnP
nové	nový	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitevní	bitevní	k2eAgFnPc4d1
lodi	loď	k1gFnPc4
New	New	k1gFnSc4
York	York	k1gInSc4
bylo	být	k5eAaImAgNnS
přidáno	přidat	k5eAaPmNgNnS
pár	pár	k4xCyI
cm	cm	kA
nového	nový	k2eAgInSc2d1
pancíře	pancíř	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgFnP
odstraněny	odstranit	k5eAaPmNgInP
torpédomety	torpédomet	k1gInPc1
<g/>
,	,	kIx,
5	#num#	k4
děl	dělo	k1gNnPc2
ráže	ráže	k1gFnSc1
127	#num#	k4
mm	mm	kA
a	a	k8xC
všechny	všechen	k3xTgFnPc4
děla	dít	k5eAaBmAgFnS,k5eAaImAgFnS
QF	QF	kA
3	#num#	k4
<g/>
-pounder	-poundero	k1gNnPc2
a	a	k8xC
QF	QF	kA
1	#num#	k4
<g/>
-pounder	-poundra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k7c2
nich	on	k3xPp3gMnPc2
bylo	být	k5eAaImAgNnS
přidáno	přidat	k5eAaPmNgNnS
8	#num#	k4
protiletadlových	protiletadlový	k2eAgInPc2d1
kanónů	kanón	k1gInPc2
Mk	Mk	k1gFnSc2
22	#num#	k4
ráže	ráže	k1gFnSc1
76	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
měly	mít	k5eAaImAgFnP
dostřel	dostřel	k1gInSc4
až	až	k6eAd1
13	#num#	k4
km	km	kA
a	a	k8xC
šrapnely	šrapnel	k1gInPc1
vážily	vážit	k5eAaImAgInP
něco	něco	k6eAd1
okolo	okolo	k7c2
10	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
byly	být	k5eAaImAgInP
přidány	přidat	k5eAaPmNgInP
3	#num#	k4
hydroplány	hydroplán	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Standardní	standardní	k2eAgInSc1d1
výtlak	výtlak	k1gInSc1
lodi	loď	k1gFnSc2
zůstal	zůstat	k5eAaPmAgInS
stejný	stejný	k2eAgInSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
plný	plný	k2eAgInSc1d1
výtlak	výtlak	k1gInSc1
ze	z	k7c2
zvýšil	zvýšit	k5eAaPmAgInS
na	na	k7c4
32	#num#	k4
514	#num#	k4
t.	t.	k?
I	i	k9
maximální	maximální	k2eAgInSc1d1
ponor	ponor	k1gInSc1
lodi	loď	k1gFnSc2
se	se	k3xPyFc4
zvětšil	zvětšit	k5eAaPmAgInS
na	na	k7c4
9,6	9,6	k4
m.	m.	k?
</s>
<s>
Výzbroj	výzbroj	k1gFnSc1
(	(	kIx(
<g/>
1942	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Roku	rok	k1gInSc2
1942	#num#	k4
byla	být	k5eAaImAgFnS
lodi	loď	k1gFnPc4
naposledy	naposledy	k6eAd1
změněna	změněn	k2eAgFnSc1d1
výzbroj	výzbroj	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
USS	USS	kA
New	New	k1gFnSc1
York	York	k1gInSc1
tedy	tedy	k8xC
byla	být	k5eAaImAgFnS
vyzbrojena	vyzbrojit	k5eAaPmNgFnS
5	#num#	k4
dvojhlavňovými	dvojhlavňový	k2eAgFnPc7d1
střeleckými	střelecký	k2eAgFnPc7d1
věžemi	věž	k1gFnPc7
s	s	k7c7
děly	dělo	k1gNnPc7
ráže	ráže	k1gFnSc2
356	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
6	#num#	k4
děly	dít	k5eAaBmAgFnP,k5eAaImAgFnP
ráže	ráže	k1gFnPc1
127	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
10	#num#	k4
protiletadlovými	protiletadlový	k2eAgInPc7d1
kanóny	kanón	k1gInPc7
Mk	Mk	k1gFnSc2
22	#num#	k4
s	s	k7c7
ráží	ráže	k1gFnSc7
76	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
6	#num#	k4
čtyřhlavňovými	čtyřhlavňový	k2eAgInPc7d1
protiletadlovými	protiletadlový	k2eAgInPc7d1
kanóny	kanón	k1gInPc7
Bofors	Boforsa	k1gFnPc2
s	s	k7c7
ráží	ráže	k1gFnSc7
40	#num#	k4
mm	mm	kA
a	a	k8xC
dostřelem	dostřel	k1gInSc7
až	až	k6eAd1
12	#num#	k4
500	#num#	k4
m	m	kA
a	a	k8xC
nakonec	nakonec	k6eAd1
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
42	#num#	k4
protiletadlových	protiletadlový	k2eAgInPc2d1
kanónů	kanón	k1gInPc2
Oerlikon	Oerlikon	k1gNnSc4
ráže	ráže	k1gFnSc2
20	#num#	k4
mm	mm	kA
a	a	k8xC
dostřelem	dostřel	k1gInSc7
více	hodně	k6eAd2
než	než	k8xS
6	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šrapnely	šrapnel	k1gInPc1
pro	pro	k7c4
tyto	tento	k3xDgInPc4
kanóny	kanón	k1gInPc4
vážily	vážit	k5eAaImAgInP
123	#num#	k4
g.	g.	k?
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
USS	USS	kA
New	New	k1gFnSc1
York	York	k1gInSc1
(	(	kIx(
<g/>
BB-	BB-	k1gFnSc1
<g/>
34	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
BB-34	BB-34	k1gMnSc1
USS	USS	kA
NEW	NEW	kA
YORK	York	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
navsource	navsourka	k1gFnSc3
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
angličtina	angličtina	k1gFnSc1
<g/>
)	)	kIx)
↑	↑	k?
USS	USS	kA
New	New	k1gMnSc1
York	York	k1gInSc1
(	(	kIx(
<g/>
Battleship	Battleship	k1gInSc1
#	#	kIx~
34	#num#	k4
<g/>
,	,	kIx,
later	later	k1gInSc1
BB-	BB-	k1gFnSc1
<g/>
34	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1914-1948	1914-1948	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
history	histor	k1gInPc1
<g/>
.	.	kIx.
<g/>
navy	navy	k?
<g/>
.	.	kIx.
<g/>
mil	míle	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
angličtina	angličtina	k1gFnSc1
<g/>
)	)	kIx)
↑	↑	k?
War	War	k1gFnPc4
I	I	kA
<g/>
/	/	kIx~
<g/>
II	II	kA
World	World	k1gMnSc1
<g/>
:	:	kIx,
USS	USS	kA
New	New	k1gFnSc1
York	York	k1gInSc1
(	(	kIx(
<g/>
BB-	BB-	k1gFnSc1
<g/>
34	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
greelane	greelanout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
čeština	čeština	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
JACKSON	JACKSON	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
válečných	válečný	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
NAŠE	náš	k3xOp1gNnSc1
VOJSKO	vojsko	k1gNnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
448	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
206	#num#	k4
<g/>
-	-	kIx~
<g/>
1199	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
amerických	americký	k2eAgFnPc2d1
bitevních	bitevní	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
USS	USS	kA
New	New	k1gFnPc3
York	York	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Americké	americký	k2eAgFnPc1d1
bitevní	bitevní	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
třídy	třída	k1gFnSc2
New	New	k1gFnSc2
York	York	k1gInSc1
</s>
<s>
New	New	k?
York	York	k1gInSc1
•	•	k?
Texas	Texas	k1gInSc1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
třída	třída	k1gFnSc1
Wyoming	Wyoming	k1gInSc1
•	•	k?
Nástupce	nástupce	k1gMnSc2
<g/>
:	:	kIx,
třída	třída	k1gFnSc1
Nevada	Nevada	k1gFnSc1
Seznam	seznam	k1gInSc4
amerických	americký	k2eAgFnPc2d1
bitevních	bitevní	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Loďstvo	loďstvo	k1gNnSc1
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
