<s>
Mortadela	mortadela	k1gFnSc1
(	(	kIx(
<g/>
film	film	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Mortadela	mortadela	k1gFnSc1
Sofie	Sofia	k1gFnSc2
LorenováZákladní	LorenováZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
La	la	k1gNnSc1
mortadella	mortadello	k1gNnSc2
Země	zem	k1gFnSc2
</s>
<s>
Francie	Francie	k1gFnSc1
FrancieItálie	FrancieItálie	k1gFnSc2
Itálie	Itálie	k1gFnSc2
Jazyky	jazyk	k1gInPc1
</s>
<s>
italština	italština	k1gFnSc1
<g/>
,	,	kIx,
angličtina	angličtina	k1gFnSc1
Délka	délka	k1gFnSc1
</s>
<s>
109	#num#	k4
min	mina	k1gFnPc2
Žánry	žánr	k1gInPc1
</s>
<s>
komedie	komedie	k1gFnSc1
<g/>
,	,	kIx,
satira	satira	k1gFnSc1
Námět	námět	k1gInSc1
</s>
<s>
Renato	Renata	k1gFnSc5
W.	W.	kA
Spera	Spera	k1gFnSc1
Scénář	scénář	k1gInSc1
</s>
<s>
Suso	Suso	k1gMnSc1
Cecchi	Cecchi	k1gNnSc2
D	D	kA
<g/>
'	'	kIx"
<g/>
Amico	Amico	k1gMnSc1
<g/>
,	,	kIx,
Mario	Mario	k1gMnSc1
Monicelli	Monicelle	k1gFnSc4
<g/>
,	,	kIx,
Ring	ring	k1gInSc4
Lardner	Lardnra	k1gFnPc2
Jr	Jr	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Režie	režie	k1gFnSc1
</s>
<s>
Mario	Mario	k1gMnSc1
Monicelli	Monicelle	k1gFnSc4
Obsazení	obsazení	k1gNnSc2
a	a	k8xC
filmový	filmový	k2eAgInSc1d1
štáb	štáb	k1gInSc1
Hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
</s>
<s>
Sophia	Sophia	k1gFnSc1
Loren	Lorna	k1gFnPc2
<g/>
:	:	kIx,
Maddalena	Maddalen	k2eAgFnSc1d1
Ciarrapico	Ciarrapico	k1gNnSc4
</s>
<s>
Gigi	Gigi	k6eAd1
Proietti	Proietti	k1gNnSc1
<g/>
:	:	kIx,
Michele	Michel	k1gMnSc5
Bruni	Bruň	k1gMnSc5
</s>
<s>
William	William	k6eAd1
Devane	Devan	k1gMnSc5
<g/>
:	:	kIx,
Jack	Jack	k1gMnSc1
Fenner	Fenner	k1gMnSc1
</s>
<s>
Beeson	Beeson	k1gMnSc1
Carroll	Carroll	k1gMnSc1
<g/>
:	:	kIx,
Dominic	Dominice	k1gFnPc2
Perlino	perlina	k1gFnSc5
</s>
<s>
Danny	Dann	k1gMnPc4
DeVito	DeVit	k2eAgNnSc1d1
<g/>
:	:	kIx,
Fred	Fred	k1gMnSc1
Mancuso	Mancusa	k1gFnSc5
</s>
<s>
Susan	Susan	k1gMnSc1
Sarandon	Sarandon	k1gMnSc1
<g/>
:	:	kIx,
Sally	Sall	k1gInPc1
</s>
<s>
Robert	Robert	k1gMnSc1
Glaudini	Glaudin	k2eAgMnPc1d1
<g/>
:	:	kIx,
Georgie	Georgie	k1gFnSc2
</s>
<s>
David	David	k1gMnSc1
Doyle	Doyle	k1gFnSc2
<g/>
:	:	kIx,
O	O	kA
<g/>
'	'	kIx"
<g/>
Henry	Henry	k1gMnSc1
</s>
<s>
William	William	k1gInSc1
J.	J.	kA
Daprato	Daprat	k2eAgNnSc1d1
<g/>
:	:	kIx,
Pasquale	Pasqual	k1gInSc5
</s>
<s>
Charles	Charles	k1gMnSc1
Bartlett	Bartlett	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
Claudio	Claudio	k1gNnSc1
Trionfi	Trionf	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
María	Marí	k2eAgFnSc1d1
Luisa	Luisa	k1gFnSc1
Sala	Sala	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Tommaso	Tommasa	k1gFnSc5
Bianco	bianco	k2eAgFnPc5d1
<g/>
:	:	kIx,
</s>
<s>
Carla	Carla	k6eAd1
Mancini	Mancin	k1gMnPc1
<g/>
:	:	kIx,
Produkce	produkce	k1gFnSc1
</s>
<s>
Carlo	Carlo	k1gNnSc1
Ponti	Ponti	k1gNnSc2
<g/>
,	,	kIx,
Compagnia	Compagnium	k1gNnSc2
Cinematografica	Cinematografic	k1gInSc2
Champion	Champion	k1gInSc1
<g/>
,	,	kIx,
distribuce	distribuce	k1gFnSc1
Dear	Deara	k1gFnPc2
International	International	k1gFnSc1
Hudba	hudba	k1gFnSc1
</s>
<s>
Ron	Ron	k1gMnSc1
<g/>
,	,	kIx,
Lucio	Lucio	k1gMnSc1
Dalla	Dalla	k1gMnSc1
Kamera	kamera	k1gFnSc1
</s>
<s>
Alfio	Alfio	k6eAd1
Contini	Contin	k2eAgMnPc1d1
Kostýmy	kostým	k1gInPc1
</s>
<s>
Albert	Albert	k1gMnSc1
Wolsky	Wolska	k1gFnSc2
Střih	střih	k1gInSc1
</s>
<s>
Ruggero	Ruggero	k1gNnSc1
Mastroianni	Mastroiann	k1gMnPc1
Výroba	výroba	k1gFnSc1
a	a	k8xC
distribuce	distribuce	k1gFnSc1
Premiéra	premiéra	k1gFnSc1
</s>
<s>
1971	#num#	k4
Mortadela	mortadela	k1gFnSc1
na	na	k7c4
ČSFD	ČSFD	kA
<g/>
,	,	kIx,
IMDbNěkterá	IMDbNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mortadela	mortadela	k1gFnSc1
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
La	la	k1gNnSc1
mortadella	mortadello	k1gNnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
francouzsko-italská	francouzsko-italský	k2eAgFnSc1d1
satirická	satirický	k2eAgFnSc1d1
komedie	komedie	k1gFnSc1
<g/>
,	,	kIx,
natočil	natočit	k5eAaBmAgMnS
ji	on	k3xPp3gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1971	#num#	k4
slavný	slavný	k2eAgMnSc1d1
italský	italský	k2eAgMnSc1d1
režisér	režisér	k1gMnSc1
Mario	Mario	k1gMnSc1
Monicelli	Monicell	k1gMnPc1
s	s	k7c7
hvězdným	hvězdný	k2eAgNnSc7d1
hereckým	herecký	k2eAgNnSc7d1
obsazením	obsazení	k1gNnSc7
-	-	kIx~
Sophie	Sophie	k1gFnSc1
Lorenová	Lorenová	k1gFnSc1
<g/>
,	,	kIx,
William	William	k1gInSc1
Devane	Devan	k1gMnSc5
<g/>
,	,	kIx,
Gigi	Gigi	k1gNnPc7
Proietti	Proietť	k1gFnSc2
a	a	k8xC
Danny	Danna	k1gMnSc2
DeVito	DeVit	k2eAgNnSc1d1
ve	v	k7c6
vedlejší	vedlejší	k2eAgFnSc6d1
roli	role	k1gFnSc6
politika	politik	k1gMnSc2
Freda	Fred	k1gMnSc2
Mancusa	Mancus	k1gMnSc2
<g/>
.	.	kIx.
jejímž	jejíž	k3xOyRp3gMnSc7
režisérem	režisér	k1gMnSc7
byl	být	k5eAaImAgMnS
Mario	Mario	k1gMnSc1
Monicelli	Monicelle	k1gFnSc4
a	a	k8xC
osobu	osoba	k1gFnSc4
hlavního	hlavní	k2eAgMnSc2d1
hrdiny	hrdina	k1gMnSc2
zahrála	zahrát	k5eAaPmAgFnS
Sofia	Sofia	k1gFnSc1
Lorenová	Lorenová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Situace	situace	k1gFnSc1
v	v	k7c6
kterých	který	k3yQgFnPc6,k3yRgFnPc6,k3yIgFnPc6
se	se	k3xPyFc4
hrdinka	hrdinka	k1gFnSc1
ocitne	ocitnout	k5eAaPmIp3nS
<g/>
,	,	kIx,
ji	on	k3xPp3gFnSc4
nakonec	nakonec	k6eAd1
přiměje	přimět	k5eAaPmIp3nS
přehodnotit	přehodnotit	k5eAaPmF
názor	názor	k1gInSc4
na	na	k7c4
skvělý	skvělý	k2eAgInSc4d1
život	život	k1gInSc4
v	v	k7c6
zemi	zem	k1gFnSc6
nekonečných	konečný	k2eNgFnPc2d1
možností	možnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
úpravě	úprava	k1gFnSc6
(	(	kIx(
<g/>
na	na	k7c4
95	#num#	k4
minut	minuta	k1gFnPc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
snímek	snímek	k1gInSc1
společností	společnost	k1gFnPc2
United	United	k1gInSc4
Artists	Artists	k1gInSc4
pod	pod	k7c7
titulem	titul	k1gInSc7
Lady	lady	k1gFnSc1
Liberty	Libert	k1gInPc1
(	(	kIx(
<g/>
námět	námět	k1gInSc1
Leonard	Leonarda	k1gFnPc2
Melfi	Melf	k1gFnSc2
<g/>
,	,	kIx,
Suso	Suso	k1gMnSc1
Cecchi	Cecch	k1gFnSc2
D	D	kA
<g/>
'	'	kIx"
<g/>
Amico	Amico	k1gMnSc1
<g/>
,	,	kIx,
Don	Don	k1gMnSc1
Carlos	Carlos	k1gMnSc1
Dunaway	Dunawaa	k1gFnSc2
<g/>
)	)	kIx)
v	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
uveden	uvést	k5eAaPmNgMnS
na	na	k7c4
trh	trh	k1gInSc4
v	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
NSR	NSR	kA
a	a	k8xC
dalších	další	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Film	film	k1gInSc1
je	být	k5eAaImIp3nS
ozvučený	ozvučený	k2eAgInSc1d1
a	a	k8xC
barevný	barevný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Zápletka	zápletka	k1gFnSc1
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1
konfliktem	konflikt	k1gInSc7
je	být	k5eAaImIp3nS
údajná	údajný	k2eAgFnSc1d1
byrokracie	byrokracie	k1gFnSc1
při	při	k7c6
celním	celní	k2eAgInSc6d1
procesu	proces	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
přechází	přecházet	k5eAaImIp3nS
v	v	k7c6
procitnutí	procitnutí	k1gNnSc6
do	do	k7c2
reality	realita	k1gFnSc2
v	v	k7c6
zemi	zem	k1gFnSc6
svobody	svoboda	k1gFnSc2
a	a	k8xC
blahobytu	blahobyt	k1gInSc2
na	na	k7c6
pozadí	pozadí	k1gNnSc6
milostného	milostný	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maddalena	Maddalen	k2eAgFnSc1d1
Ciarrapico	Ciarrapico	k6eAd1
(	(	kIx(
<g/>
Sophia	Sophia	k1gFnSc1
Loren	Lorna	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
Michele	Michela	k1gFnSc3
Bruni	Brueň	k1gFnSc3
(	(	kIx(
<g/>
Gigi	Gigi	k1gNnSc1
Proietti	Proietť	k1gFnSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
poznají	poznat	k5eAaPmIp3nP
a	a	k8xC
zamilují	zamilovat	k5eAaPmIp3nP
v	v	k7c6
Itálii	Itálie	k1gFnSc6
v	v	k7c6
šedesátých	šedesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
velmi	velmi	k6eAd1
rušivých	rušivý	k2eAgFnPc6d1
vsuvkách	vsuvka	k1gFnPc6
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
vzpomínky	vzpomínka	k1gFnPc1
<g/>
)	)	kIx)
objasněno	objasněn	k2eAgNnSc1d1
během	během	k7c2
první	první	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
filmu	film	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vsunuté	vsunutý	k2eAgFnPc4d1
sekvence	sekvence	k1gFnPc4
ukazují	ukazovat	k5eAaImIp3nP
jak	jak	k6eAd1
Maddalena	Maddalen	k2eAgNnPc1d1
a	a	k8xC
Michele	Michel	k1gInPc4
pracovali	pracovat	k5eAaImAgMnP
v	v	k7c6
továrně	továrna	k1gFnSc6
na	na	k7c4
maso	maso	k1gNnSc4
a	a	k8xC
jak	jak	k8xS,k8xC
jejich	jejich	k3xOp3gFnSc1
nezměrná	změrný	k2eNgFnSc1d1,k2eAgFnSc1d1
čistá	čistý	k2eAgFnSc1d1
láska	láska	k1gFnSc1
nemohla	moct	k5eNaImAgFnS
být	být	k5eAaImF
legalizována	legalizován	k2eAgFnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
on	on	k3xPp3gMnSc1
už	už	k6eAd1
je	být	k5eAaImIp3nS
ženatý	ženatý	k2eAgMnSc1d1
a	a	k8xC
Itálie	Itálie	k1gFnSc1
neuznává	uznávat	k5eNaImIp3nS
rozvod	rozvod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dohodnou	dohodnout	k5eAaPmIp3nP
se	se	k3xPyFc4
na	na	k7c6
emigraci	emigrace	k1gFnSc6
do	do	k7c2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Michele	Michel	k1gInSc2
odjíždí	odjíždět	k5eAaImIp3nS
a	a	k8xC
a	a	k8xC
nyní	nyní	k6eAd1
za	za	k7c7
ním	on	k3xPp3gMnSc7
přichází	přicházet	k5eAaImIp3nS
zamilovaná	zamilovaná	k1gFnSc1
Maddelena	Maddelen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přiváží	přivážit	k5eAaPmIp3nS,k5eAaImIp3nS
s	s	k7c7
sebou	se	k3xPyFc7
mortadelu	mortadela	k1gFnSc4
jako	jako	k8xS,k8xC
svatební	svatební	k2eAgInSc4d1
dar	dar	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celní	celní	k2eAgInPc1d1
předpisy	předpis	k1gInPc1
ale	ale	k9
neumožňují	umožňovat	k5eNaImIp3nP
dovážet	dovážet	k5eAaImF
maso	maso	k1gNnSc4
a	a	k8xC
masné	masný	k2eAgInPc4d1
výrobky	výrobek	k1gInPc4
do	do	k7c2
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takže	takže	k8xS
Maddelena	Maddelen	k2eAgFnSc1d1
zůstává	zůstávat	k5eAaImIp3nS
na	na	k7c6
letišti	letiště	k1gNnSc6
JFK	JFK	kA
a	a	k8xC
tvrdohlavě	tvrdohlavě	k6eAd1
se	se	k3xPyFc4
odmítá	odmítat	k5eAaImIp3nS
vzdát	vzdát	k5eAaPmF
salámu	salám	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Setkává	setkávat	k5eAaImIp3nS
se	se	k3xPyFc4
nejen	nejen	k6eAd1
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
láskou	láska	k1gFnSc7
<g/>
,	,	kIx,
Michelem	Michel	k1gInSc7
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
s	s	k7c7
nezkrotným	zkrotný	k2eNgMnSc7d1
novinářem	novinář	k1gMnSc7
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
ji	on	k3xPp3gFnSc4
háji	háj	k1gInSc6
avšak	avšak	k8xC
večer	večer	k6eAd1
stráví	strávit	k5eAaPmIp3nS
s	s	k7c7
milým	milý	k2eAgMnSc7d1
<g/>
,	,	kIx,
cílevědomým	cílevědomý	k2eAgMnSc7d1
a	a	k8xC
disciplinovaným	disciplinovaný	k2eAgInSc7d1
celníkem	celník	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Celou	celý	k2eAgFnSc4d1
záležitost	záležitost	k1gFnSc4
s	s	k7c7
mortadelou	mortadela	k1gFnSc7
se	se	k3xPyFc4
pokouší	pokoušet	k5eAaImIp3nS
„	„	k?
<g/>
smírně	smírně	k6eAd1
<g/>
“	“	k?
(	(	kIx(
<g/>
zničením	zničení	k1gNnSc7
mortadely	mortadela	k1gFnSc2
<g/>
)	)	kIx)
řešit	řešit	k5eAaImF
„	„	k?
<g/>
hodný	hodný	k2eAgInSc4d1
<g/>
“	“	k?
celník	celník	k1gInSc4
Dominic	Dominice	k1gFnPc2
Perlino	perlina	k1gFnSc5
Beeson	Beesona	k1gFnPc2
Carroll	Carroll	k1gInSc1
<g/>
,	,	kIx,
kterému	který	k3yIgInSc3,k3yRgInSc3,k3yQgInSc3
se	se	k3xPyFc4
Maddalena	Maddalen	k2eAgNnPc1d1
na	na	k7c4
první	první	k4xOgInSc4
pohled	pohled	k1gInSc4
zalíbila	zalíbit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
tvrdohlavá	tvrdohlavý	k2eAgFnSc1d1
Maddalena	Maddalen	k2eAgFnSc1d1
odmítá	odmítat	k5eAaImIp3nS
respektovat	respektovat	k5eAaImF
zákony	zákon	k1gInPc4
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
a	a	k8xC
vydat	vydat	k5eAaPmF
mortadelu	mortadela	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spor	spor	k1gInSc1
tedy	tedy	k9
pomáhá	pomáhat	k5eAaImIp3nS
ukončit	ukončit	k5eAaPmF
její	její	k3xOp3gMnSc1
snoubenec	snoubenec	k1gMnSc1
Michele	Michel	k1gInSc2
(	(	kIx(
<g/>
Maddalena	Maddalen	k2eAgFnSc1d1
<g/>
:	:	kIx,
Seznámili	seznámit	k5eAaPmAgMnP
jsme	být	k5eAaImIp1nP
se	se	k3xPyFc4
před	před	k7c7
třiceti	třicet	k4xCc7
lety	let	k1gInPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Michele	Michel	k1gInSc2
se	se	k3xPyFc4
před	před	k7c7
zraky	zrak	k1gInPc7
zklamaného	zklamaný	k2eAgNnSc2d1
Dominika	Dominik	k1gMnSc4
s	s	k7c7
Maddalenou	Maddalený	k2eAgFnSc7d1
vášnivě	vášnivě	k6eAd1
vítá	vítat	k5eAaImIp3nS
polibky	polibek	k1gInPc4
a	a	k8xC
nařídí	nařídit	k5eAaPmIp3nS
Maddaleně	Maddalena	k1gFnSc3
aby	aby	kYmCp3nS
okamžitě	okamžitě	k6eAd1
poslechla	poslechnout	k5eAaPmAgFnS
celníky	celník	k1gInPc4
a	a	k8xC
přestala	přestat	k5eAaPmAgFnS
hubovat	hubovat	k5eAaImF
na	na	k7c4
zákony	zákon	k1gInPc4
USA	USA	kA
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
hloupé	hloupý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Michele	Michel	k1gInSc2
totiž	totiž	k9
nedočkavě	dočkavě	k6eNd1,k6eAd1
čeká	čekat	k5eAaImIp3nS
na	na	k7c4
udělení	udělení	k1gNnSc4
amerického	americký	k2eAgNnSc2d1
občanství	občanství	k1gNnSc2
a	a	k8xC
snaží	snažit	k5eAaImIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
rozepře	rozepřít	k5eAaPmIp3nS
se	s	k7c7
státním	státní	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
řešit	řešit	k5eAaImF
omluvami	omluva	k1gFnPc7
<g/>
,	,	kIx,
úklonami	úklona	k1gFnPc7
a	a	k8xC
komandováním	komandování	k1gNnSc7
snoubenky	snoubenka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maddalena	Maddalen	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
vskutku	vskutku	k9
rozčarována	rozčarován	k2eAgFnSc1d1
a	a	k8xC
hned	hned	k6eAd1
vysvětluje	vysvětlovat	k5eAaImIp3nS
americkým	americký	k2eAgMnPc3d1
úřadům	úřada	k1gMnPc3
proč	proč	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Že	že	k8xS
Michele	Michel	k1gInPc1
<g/>
,	,	kIx,
tedy	tedy	k9
jak	jak	k8xS,k8xC
ho	on	k3xPp3gMnSc4
zná	znát	k5eAaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
revolucionář	revolucionář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Že	že	k8xS
se	se	k3xPyFc4
Michele	Michel	k1gInSc2
bral	brát	k5eAaImAgMnS
vždy	vždy	k6eAd1
za	za	k7c4
svůj	svůj	k3xOyFgInSc4
názor	názor	k1gInSc4
<g/>
,	,	kIx,
chodil	chodit	k5eAaImAgMnS
vždy	vždy	k6eAd1
v	v	k7c6
čele	čelo	k1gNnSc6
demonstrací	demonstrace	k1gFnPc2
a	a	k8xC
protestů	protest	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
krátkém	krátký	k2eAgInSc6d1
šotu	šot	k1gInSc6
vzpomínky	vzpomínka	k1gFnSc2
je	on	k3xPp3gInPc4
Michele	Michel	k1gInPc4
pod	pod	k7c7
obrovskými	obrovský	k2eAgInPc7d1
rudými	rudý	k2eAgInPc7d1
<g/>
,	,	kIx,
očividně	očividně	k6eAd1
komunistickými	komunistický	k2eAgInPc7d1
<g/>
,	,	kIx,
vlajkami	vlajka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Michele	Michel	k1gInSc2
se	se	k3xPyFc4
okamžitě	okamžitě	k6eAd1
vyděsí	vyděsit	k5eAaPmIp3nS
k	k	k7c3
smrti	smrt	k1gFnSc3
a	a	k8xC
omlouvá	omlouvat	k5eAaImIp3nS
její	její	k3xOp3gNnPc4
slova	slovo	k1gNnPc4
<g/>
,	,	kIx,
že	že	k8xS
Maddalena	Maddalen	k2eAgFnSc1d1
neumí	umět	k5eNaImIp3nS
anglicky	anglicky	k6eAd1
a	a	k8xC
plácá	plácat	k5eAaImIp3nS
nesmysly	nesmysl	k1gInPc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
má	mít	k5eAaImIp3nS
jistě	jistě	k9
důvod	důvod	k1gInSc1
právě	právě	k6eAd1
ve	v	k7c6
snaze	snaha	k1gFnSc6
neztratit	ztratit	k5eNaPmF
naději	naděje	k1gFnSc4
naději	naděje	k1gFnSc4
na	na	k7c4
občanství	občanství	k1gNnSc4
a	a	k8xC
v	v	k7c6
kontextu	kontext	k1gInSc6
doby	doba	k1gFnSc2
i	i	k8xC
nebýt	být	k5eNaImF
dokonce	dokonce	k9
podezříván	podezříván	k2eAgMnSc1d1
ze	z	k7c2
sympatií	sympatie	k1gFnPc2
ke	k	k7c3
komunistické	komunistický	k2eAgFnSc3d1
straně	strana	k1gFnSc3
a	a	k8xC
třeba	třeba	k6eAd1
prověřován	prověřovat	k5eAaImNgMnS
americkými	americký	k2eAgMnPc7d1
vlastenci	vlastenec	k1gMnPc7
a	a	k8xC
Výborem	výbor	k1gInSc7
pro	pro	k7c4
neamerickou	americký	k2eNgFnSc4d1
činnost	činnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
tvrdohlavou	tvrdohlavý	k2eAgFnSc4d1
Maddalenu	Maddalen	k2eAgFnSc4d1
naléhají	naléhat	k5eAaBmIp3nP,k5eAaImIp3nP
nyní	nyní	k6eAd1
úředníci	úředník	k1gMnPc1
<g/>
,	,	kIx,
Dominik	Dominik	k1gMnSc1
i	i	k9
její	její	k3xOp3gFnSc3
snoubenec	snoubenec	k1gMnSc1
Michele	Michel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
vřavy	vřava	k1gFnSc2
rozepře	rozepře	k1gFnSc2
se	se	k3xPyFc4
vřítí	vřítit	k5eAaPmIp3nS
rozjásaný	rozjásaný	k2eAgMnSc1d1
reportér	reportér	k1gMnSc1
Jack	Jack	k1gMnSc1
Fenner	Fenner	k1gMnSc1
William	William	k1gInSc4
Devane	Devan	k1gMnSc5
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
zřejmě	zřejmě	k6eAd1
vše	všechen	k3xTgNnSc4
vyslechl	vyslechnout	k5eAaPmAgMnS
slibuje	slibovat	k5eAaImIp3nS
Maddaleně	Maddalena	k1gFnSc3
pomoc	pomoc	k1gFnSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
spravedlivém	spravedlivý	k2eAgInSc6d1
svatém	svatý	k2eAgInSc6d1
boji	boj	k1gInSc6
hrdinsky	hrdinsky	k6eAd1
vytrvá	vytrvat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
si	se	k3xPyFc3
reportér	reportér	k1gMnSc1
půjčí	půjčit	k5eAaPmIp3nS
od	od	k7c2
Maddaleny	Maddalen	k2eAgInPc4d1
peníze	peníz	k1gInPc4
na	na	k7c4
telefon	telefon	k1gInSc4
a	a	k8xC
je	být	k5eAaImIp3nS
odvlečen	odvléct	k5eAaPmNgInS
s	s	k7c7
křikem	křik	k1gInSc7
o	o	k7c6
policejní	policejní	k2eAgFnSc6d1
brutalitě	brutalita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jeho	jeho	k3xOp3gInSc6
odchodu	odchod	k1gInSc6
dostane	dostat	k5eAaPmIp3nS
snoubenec	snoubenec	k1gMnSc1
Michele	Michel	k1gInSc2
od	od	k7c2
Maddaleny	Maddalen	k2eAgFnPc4d1
kvinde	kvinde	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úředníci	úředník	k1gMnPc1
rozhodnou	rozhodnout	k5eAaPmIp3nP
<g/>
,	,	kIx,
že	že	k8xS
Maddalenu	Maddalen	k2eAgFnSc4d1
pošlou	pošlý	k2eAgFnSc4d1
letecky	letecky	k6eAd1
šupem	šup	k1gInSc7
zpět	zpět	k6eAd1
do	do	k7c2
rodné	rodný	k2eAgFnSc2d1
Itálie	Itálie	k1gFnSc2
i	i	k8xC
s	s	k7c7
mortadelou	mortadela	k1gFnSc7
s	s	k7c7
čímž	což	k3yQnSc7,k3yRnSc7
Madelene	Madelen	k1gInSc5
nesouhlasí	souhlasit	k5eNaImIp3nS
a	a	k8xC
žádá	žádat	k5eAaImIp3nS
advokáta	advokát	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úředníkům	úředník	k1gMnPc3
jsou	být	k5eAaImIp3nP
její	její	k3xOp3gInPc1
protesty	protest	k1gInPc1
lhostejné	lhostejný	k2eAgInPc1d1
a	a	k8xC
je	být	k5eAaImIp3nS
tedy	tedy	k9
rozhodnuto	rozhodnout	k5eAaPmNgNnS
o	o	k7c6
deportaci	deportace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zůstane	zůstat	k5eAaPmIp3nS
tak	tak	k9
pouze	pouze	k6eAd1
otázka	otázka	k1gFnSc1
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
letenku	letenka	k1gFnSc4
zaplatí	zaplatit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
pokročilé	pokročilý	k2eAgFnSc3d1
hodině	hodina	k1gFnSc3
se	se	k3xPyFc4
však	však	k9
nepodaří	podařit	k5eNaPmIp3nS
kontaktovat	kontaktovat	k5eAaImF
italského	italský	k2eAgMnSc4d1
velvyslance	velvyslanec	k1gMnSc4
a	a	k8xC
deportace	deportace	k1gFnPc4
se	se	k3xPyFc4
odkládá	odkládat	k5eAaImIp3nS
na	na	k7c4
další	další	k2eAgInSc4d1
den	den	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
noci	noc	k1gFnSc6
osamělou	osamělý	k2eAgFnSc4d1
Maddalene	Maddalen	k1gInSc5
navštíví	navštívit	k5eAaPmIp3nP
Dominic	Dominice	k1gFnPc2
a	a	k8xC
povídají	povídat	k5eAaImIp3nP
si	se	k3xPyFc3
o	o	k7c6
jeho	jeho	k3xOp3gFnSc6
matce	matka	k1gFnSc6
a	a	k8xC
jejím	její	k3xOp3gInSc6
životě	život	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celý	celý	k2eAgInSc1d1
snímek	snímek	k1gInSc1
v	v	k7c6
této	tento	k3xDgFnSc6
chvíli	chvíle	k1gFnSc6
silně	silně	k6eAd1
připomíná	připomínat	k5eAaImIp3nS
romantický	romantický	k2eAgInSc1d1
film	film	k1gInSc1
Terminál	terminála	k1gFnPc2
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
průběhem	průběh	k1gInSc7
se	se	k3xPyFc4
však	však	k9
snímek	snímek	k1gInSc1
zásadně	zásadně	k6eAd1
liší	lišit	k5eAaImIp3nS
a	a	k8xC
zakončení	zakončení	k1gNnSc1
není	být	k5eNaImIp3nS
sladký	sladký	k2eAgInSc4d1
happyend	happyend	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maddalene	Maddalen	k1gInSc5
má	mít	k5eAaImIp3nS
hlad	hlad	k1gInSc4
a	a	k8xC
nakrojí	nakrojit	k5eAaPmIp3nP
svatební	svatební	k2eAgInSc4d1
dar	dar	k1gInSc4
<g/>
,	,	kIx,
mortadelu	mortadela	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nabídne	nabídnout	k5eAaPmIp3nS
i	i	k9
Dominicovi	Dominic	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
je	být	k5eAaImIp3nS
náhle	náhle	k6eAd1
osvícen	osvítit	k5eAaPmNgInS
snadnou	snadný	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
je	být	k5eAaImIp3nS
vyřešit	vyřešit	k5eAaPmF
spor	spor	k1gInSc4
a	a	k8xC
zve	zvát	k5eAaImIp3nS
k	k	k7c3
pochoutce	pochoutka	k1gFnSc6
další	další	k2eAgMnPc4d1
stolovníky	stolovník	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc1
den	den	k1gInSc1
však	však	k9
už	už	k6eAd1
celý	celý	k2eAgInSc4d1
New	New	k1gFnSc4
York	York	k1gInSc1
čte	číst	k5eAaImIp3nS
reportáž	reportáž	k1gFnSc4
o	o	k7c6
mortadele	mortadela	k1gFnSc6
a	a	k8xC
hrdinské	hrdinský	k2eAgFnSc3d1
Italce	Italka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
letiště	letiště	k1gNnSc4
se	se	k3xPyFc4
dostaví	dostavit	k5eAaPmIp3nS
ministr	ministr	k1gMnSc1
Fred	Fred	k1gMnSc1
Mancuso	Mancusa	k1gFnSc5
Danny	Dann	k1gMnPc7
DeVito	DeVit	k2eAgNnSc1d1
v	v	k7c4
celé	celý	k2eAgNnSc4d1
své	své	k1gNnSc4
velikosti	velikost	k1gFnSc2
<g/>
,	,	kIx,
se	s	k7c7
vší	všecek	k3xTgFnSc7
pompou	pompa	k1gFnSc7
<g/>
,	,	kIx,
armádou	armáda	k1gFnSc7
novinářů	novinář	k1gMnPc2
a	a	k8xC
kamer	kamera	k1gFnPc2
a	a	k8xC
slibuje	slibovat	k5eAaImIp3nS
svoji	svůj	k3xOyFgFnSc4
velkolepou	velkolepý	k2eAgFnSc4d1
a	a	k8xC
hlavně	hlavně	k9
hlasitou	hlasitý	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
se	se	k3xPyFc4
zjistí	zjistit	k5eAaPmIp3nS
že	že	k8xS
z	z	k7c2
mortadely	mortadela	k1gFnSc2
zůstalo	zůstat	k5eAaPmAgNnS
tak	tak	k9
na	na	k7c4
jeden	jeden	k4xCgInSc4
sendvič	sendvič	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministr	ministr	k1gMnSc1
proto	proto	k8xC
zklamaně	zklamaně	k6eAd1
zase	zase	k9
odchází	odcházet	k5eAaImIp3nS
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
Maddalena	Maddalen	k2eAgFnSc1d1
protestuje	protestovat	k5eAaBmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
princip	princip	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celníci	celník	k1gMnPc1
spokojeně	spokojeně	k6eAd1
naštvanou	naštvaný	k2eAgFnSc4d1
Maddalena	Maddalen	k2eAgFnSc1d1
pouští	poušť	k1gFnSc7
do	do	k7c2
USA	USA	kA
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
však	však	k9
nemá	mít	k5eNaImIp3nS
kam	kam	k6eAd1
jít	jít	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jede	jet	k5eAaImIp3nS
tedy	tedy	k9
s	s	k7c7
reportérem	reportér	k1gMnSc7
Fennerem	Fenner	k1gMnSc7
do	do	k7c2
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
ubohého	ubohý	k2eAgInSc2d1
bytu	byt	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
jej	on	k3xPp3gMnSc4
napadnou	napadnout	k5eAaPmIp3nP
tři	tři	k4xCgFnPc1
mlátičky	mlátička	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
na	na	k7c4
Fennera	Fenner	k1gMnSc4
pošle	poslat	k5eAaPmIp3nS
její	její	k3xOp3gNnSc1
snoubenec	snoubenec	k1gMnSc1
Michele	Michel	k1gInSc2
<g/>
,	,	kIx,
uražený	uražený	k2eAgMnSc1d1
jeho	jeho	k3xOp3gFnSc7
reportáží	reportáž	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fenner	Fenner	k1gInSc1
skončí	skončit	k5eAaPmIp3nS
v	v	k7c6
nemocnici	nemocnice	k1gFnSc6
se	s	k7c7
zlomeným	zlomený	k2eAgNnSc7d1
žebrem	žebro	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maddalena	Maddalen	k2eAgFnSc1d1
pak	pak	k6eAd1
na	na	k7c6
revanš	revanš	k?
navštíví	navštívit	k5eAaPmIp3nS
Micheleho	Michele	k1gMnSc4
restauraci	restaurace	k1gFnSc4
praští	praštit	k5eAaPmIp3nP,k5eAaImIp3nP
ho	on	k3xPp3gMnSc4
a	a	k8xC
také	také	k9
zdemoluje	zdemolovat	k5eAaPmIp3nS
jeho	jeho	k3xOp3gFnSc4
restauraci	restaurace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odvádí	odvádět	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
policie	policie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Michele	Michel	k1gInSc2
končí	končit	k5eAaImIp3nS
ve	v	k7c6
stejné	stejný	k2eAgFnSc6d1
nemocnici	nemocnice	k1gFnSc6
jako	jako	k9
Fenner	Fenner	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zamilovaný	zamilovaný	k1gMnSc1
Dominic	Dominice	k1gFnPc2
zaplatí	zaplatit	k5eAaPmIp3nS
Maddaleně	Maddalena	k1gFnSc3
kauci	kauce	k1gFnSc4
a	a	k8xC
v	v	k7c6
restauraci	restaurace	k1gFnSc6
jí	on	k3xPp3gFnSc2
ukazuje	ukazovat	k5eAaImIp3nS
jak	jak	k8xS,k8xC
žijí	žít	k5eAaImIp3nP
slušní	slušný	k2eAgMnPc1d1
Američané	Američan	k1gMnPc1
a	a	k8xC
vypráví	vyprávět	k5eAaImIp3nP
jak	jak	k6eAd1
byl	být	k5eAaImAgInS
pochválen	pochválit	k5eAaPmNgInS
nadřízeným	nadřízený	k1gMnSc7
a	a	k8xC
jak	jak	k6eAd1
chytře	chytro	k6eAd1
vyřešil	vyřešit	k5eAaPmAgMnS
problém	problém	k1gInSc4
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
Maddalena	Maddalen	k2eAgMnSc4d1
vyvolala	vyvolat	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dominic	Dominice	k1gFnPc2
slibuje	slibovat	k5eAaImIp3nS
ubytování	ubytování	k1gNnSc1
u	u	k7c2
něj	on	k3xPp3gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
matky	matka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maddalena	Maddalen	k2eAgFnSc1d1
ale	ale	k8xC
zmizí	zmizet	k5eAaPmIp3nS
a	a	k8xC
nakonec	nakonec	k6eAd1
končí	končit	k5eAaImIp3nS
v	v	k7c6
posteli	postel	k1gFnSc6
s	s	k7c7
beznadějným	beznadějný	k2eAgMnSc7d1
Fennerem	Fenner	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ráno	ráno	k6eAd1
přichází	přicházet	k5eAaImIp3nS
Fennerova	Fennerův	k2eAgFnSc1d1
manželka	manželka	k1gFnSc1
a	a	k8xC
nechává	nechávat	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
na	na	k7c6
starost	starost	k1gFnSc1
děti	dítě	k1gFnPc1
<g/>
,	,	kIx,
<g/>
(	(	kIx(
<g/>
Snad	snad	k9
nechceš	chtít	k5eNaImIp2nS
abych	aby	kYmCp1nS
je	on	k3xPp3gNnSc4
poslala	poslat	k5eAaPmAgFnS
do	do	k7c2
školy	škola	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
s	s	k7c7
čímž	což	k3yRnSc7,k3yQnSc7
jsou	být	k5eAaImIp3nP
spojeno	spojit	k5eAaPmNgNnS
několik	několik	k4yIc4
velice	velice	k6eAd1
hezkých	hezký	k2eAgInPc2d1
dialogů	dialog	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maddalena	Maddalen	k2eAgFnSc1d1
odchází	odcházet	k5eAaImIp3nS
<g/>
,	,	kIx,
před	před	k7c7
domem	dům	k1gInSc7
potkává	potkávat	k5eAaImIp3nS
Micheleho	Michele	k1gMnSc2
nadšeného	nadšený	k2eAgMnSc2d1
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
zase	zase	k9
vidí	vidět	k5eAaImIp3nS
a	a	k8xC
chce	chtít	k5eAaImIp3nS
jejich	jejich	k3xOp3gInSc4
vztah	vztah	k1gInSc4
srovnat	srovnat	k5eAaPmF
<g/>
,	,	kIx,
slibuje	slibovat	k5eAaImIp3nS
manželství	manželství	k1gNnSc1
a	a	k8xC
bohatství	bohatství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Micheleho	Michele	k1gMnSc2
zdevastované	zdevastovaný	k2eAgFnSc2d1
restaurace	restaurace	k1gFnSc2
se	se	k3xPyFc4
totiž	totiž	k9
hrnou	hrnout	k5eAaImIp3nP
zvědaví	zvědavý	k2eAgMnPc1d1
zákazníci	zákazník	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Michele	Michel	k1gInSc2
se	se	k3xPyFc4
chce	chtít	k5eAaImIp3nS
spřátelit	spřátelit	k5eAaPmF
i	i	k9
s	s	k7c7
Fennerem	Fenner	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Maddeleny	Maddelen	k2eAgFnPc1d1
bude	být	k5eAaImBp3nS
atrakce	atrakce	k1gFnSc1
a	a	k8xC
Fenner	Fenner	k1gMnSc1
bude	být	k5eAaImBp3nS
propagovat	propagovat	k5eAaImF
jeho	jeho	k3xOp3gFnSc4
prosperující	prosperující	k2eAgFnSc4d1
restauraci	restaurace	k1gFnSc4
v	v	k7c6
tisku	tisk	k1gInSc6
<g/>
,	,	kIx,
plánuje	plánovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maddalena	Maddalen	k2eAgFnSc1d1
řekne	říct	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ze	z	k7c2
svatby	svatba	k1gFnSc2
nic	nic	k3yNnSc1
nebude	být	k5eNaImBp3nS
a	a	k8xC
že	že	k8xS
se	se	k3xPyFc4
vyspala	vyspat	k5eAaPmAgFnS
s	s	k7c7
Fennerem	Fenner	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naštvaný	naštvaný	k2eAgInSc4d1
Michele	Michel	k1gInPc4
zbije	zbít	k5eAaPmIp3nS
zraněného	zraněný	k2eAgMnSc4d1
Fennera	Fenner	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maddalena	Maddalen	k2eAgFnSc1d1
uspává	uspávat	k5eAaImIp3nS
zuboženého	zubožený	k2eAgMnSc4d1
Fennera	Fenner	k1gMnSc4
zpěvem	zpěv	k1gInSc7
o	o	k7c6
rodné	rodný	k2eAgFnSc6d1
Itálii	Itálie	k1gFnSc6
a	a	k8xC
pak	pak	k6eAd1
odchází	odcházet	k5eAaImIp3nS
pryč	pryč	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Satirická	satirický	k2eAgFnSc1d1
komedie	komedie	k1gFnSc1
Mortadela	mortadela	k1gFnSc1
je	být	k5eAaImIp3nS
dnešními	dnešní	k2eAgFnPc7d1
diváky	divák	k1gMnPc4
hodnocena	hodnocen	k2eAgFnSc1d1
většinou	většinou	k6eAd1
jako	jako	k8xS,k8xC
pouze	pouze	k6eAd1
průměrný	průměrný	k2eAgMnSc1d1
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
někdy	někdy	k6eAd1
ale	ale	k8xC
dobrý	dobrý	k2eAgMnSc1d1
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
film	film	k1gInSc1
známého	známý	k2eAgMnSc2d1
režíséra	režísér	k1gMnSc2
Maria	Mario	k1gMnSc2
Monicelliho	Monicelli	k1gMnSc2
(	(	kIx(
<g/>
např.	např.	kA
Markýz	markýz	k1gMnSc1
del	del	k?
Grillo	Grilla	k1gMnSc5
<g/>
,	,	kIx,
Moji	můj	k3xOp1gMnPc1
přátelé	přítel	k1gMnPc1
<g/>
,	,	kIx,
Moji	můj	k3xOp1gMnPc1
přátelé	přítel	k1gMnPc1
II	II	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zajímavý	zajímavý	k2eAgInSc1d1
skvělým	skvělý	k2eAgInSc7d1
hereckým	herecký	k2eAgInSc7d1
výkonem	výkon	k1gInSc7
Sofie	Sofia	k1gFnSc2
Lorenové	Lorenové	k2eAgFnSc7d1
a	a	k8xC
epizodní	epizodní	k2eAgFnSc7d1
rolí	role	k1gFnSc7
Dannyho	Danny	k1gMnSc2
DeVito	DeVit	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scénář	scénář	k1gInSc1
připravila	připravit	k5eAaPmAgFnS
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
i	i	k9
Suso	Suso	k1gNnSc4
Cecchi	Cecch	k1gFnSc2
d	d	k?
<g/>
'	'	kIx"
<g/>
Amico	Amico	k1gMnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
se	se	k3xPyFc4
podílela	podílet	k5eAaImAgFnS
rovněž	rovněž	k9
na	na	k7c6
zpracování	zpracování	k1gNnSc6
vynikajících	vynikající	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
jako	jako	k8xC,k8xS
Cizinec	cizinec	k1gMnSc1
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
podle	podle	k7c2
A.	A.	kA
<g/>
Camuse	Camuse	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Rocco	Rocco	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
bratři	bratr	k1gMnPc1
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Gepard	gepard	k1gMnSc1
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
Zmýlená	zmýlená	k1gFnSc1
neplatí	platit	k5eNaImIp3nS
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
filmu	film	k1gInSc6
hraje	hrát	k5eAaImIp3nS
epizodní	epizodní	k2eAgFnSc4d1
roli	role	k1gFnSc4
také	také	k9
George	George	k1gFnSc4
Fisher	Fishra	k1gFnPc2
(	(	kIx(
<g/>
násilník	násilník	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Edward	Edward	k1gMnSc1
Herrmann	Herrmann	k1gMnSc1
(	(	kIx(
<g/>
policista	policista	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
Libertini	libertin	k1gMnPc1
(	(	kIx(
<g/>
Tim	Tim	k?
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1
starší	starý	k2eAgMnPc1d2
diváci	divák	k1gMnPc1
si	se	k3xPyFc3
na	na	k7c4
film	film	k1gInSc4
Mortadela	mortadela	k1gFnSc1
však	však	k9
i	i	k9
po	po	k7c6
letech	léto	k1gNnPc6
vzpomínají	vzpomínat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
oceňován	oceňován	k2eAgInSc4d1
český	český	k2eAgInSc4d1
dabing	dabing	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Film	film	k1gInSc1
mírně	mírně	k6eAd1
napadá	napadat	k5eAaImIp3nS,k5eAaBmIp3nS,k5eAaPmIp3nS
proklamovanou	proklamovaný	k2eAgFnSc4d1
svobodu	svoboda	k1gFnSc4
a	a	k8xC
prosperitu	prosperita	k1gFnSc4
v	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
několik	několik	k4yIc1
sekvencí	sekvence	k1gFnPc2
se	se	k3xPyFc4
také	také	k9
odehrává	odehrávat	k5eAaImIp3nS
přímo	přímo	k6eAd1
v	v	k7c6
ulicích	ulice	k1gFnPc6
New	New	k1gFnSc2
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
zřejmě	zřejmě	k6eAd1
v	v	k7c6
krátkém	krátký	k2eAgNnSc6d1
období	období	k1gNnSc6
po	po	k7c6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
vypadaly	vypadat	k5eAaPmAgInP,k5eAaImAgInP
stále	stále	k6eAd1
ještě	ještě	k6eAd1
velmi	velmi	k6eAd1
zpustošeně	zpustošeně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
o	o	k7c6
tom	ten	k3xDgNnSc6
však	však	k9
píše	psát	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
nechápe	chápat	k5eNaImIp3nS
režisérovo	režisérův	k2eAgNnSc4d1
zobrazení	zobrazení	k1gNnSc4
New	New	k1gFnSc2
Yorku	York	k1gInSc2
„	„	k?
<g/>
…	…	k?
grindingly	grindingla	k1gFnSc2
bleak	bleak	k1gMnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
settings	settings	k1gInSc1
in	in	k?
which	which	k1gInSc1
so	so	k?
much	moucha	k1gFnPc2
of	of	k?
the	the	k?
film	film	k1gInSc1
is	is	k?
set	set	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
No	no	k9
Daily	Daila	k1gFnPc1
News	Newsa	k1gFnPc2
reporter	reportra	k1gFnPc2
that	that	k1gInSc1
I	i	k8xC
know	know	k?
<g/>
,	,	kIx,
for	forum	k1gNnPc2
example	example	k6eAd1
<g/>
,	,	kIx,
lives	lives	k1gInSc1
in	in	k?
a	a	k8xC
condemned	condemned	k1gMnSc1
building	building	k1gInSc1
<g/>
..	..	k?
<g/>
“	“	k?
(	(	kIx(
…	…	k?
<g/>
že	že	k8xS
by	by	kYmCp3nS
byl	být	k5eAaImAgInS
New	New	k1gFnSc3
York	York	k1gInSc1
tak	tak	k6eAd1
nehostinný	hostinný	k2eNgInSc1d1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
je	být	k5eAaImIp3nS
ukázán	ukázat	k5eAaPmNgInS
ve	v	k7c6
filmu	film	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
žádný	žádný	k3yNgMnSc1
reportér	reportér	k1gMnSc1
Daily	Daila	k1gFnSc2
News	News	k1gInSc1
<g/>
,	,	kIx,
kterého	který	k3yIgInSc2,k3yQgInSc2,k3yRgInSc2
znám	znát	k5eAaImIp1nS
<g/>
,	,	kIx,
například	například	k6eAd1
nebydlí	bydlet	k5eNaImIp3nP
v	v	k7c6
neobyvatelné	obyvatelný	k2eNgFnSc6d1
budově	budova	k1gFnSc6
<g/>
.	.	kIx.
<g/>
)	)	kIx)
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
databáze	databáze	k1gFnSc1
imdb	imdb	k1gMnSc1
<g/>
↑	↑	k?
csfd	csfd	k1gMnSc1
<g/>
↑	↑	k?
fdb	fdb	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
ilmsdefrance	ilmsdefrance	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
↑	↑	k?
Movie	Movie	k1gFnSc1
Review	Review	k1gFnSc1
-	-	kIx~
Chato	chata	k1gFnSc5
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Land	Landa	k1gFnPc2
-	-	kIx~
Sophia	Sophia	k1gFnSc1
Loren	Lorna	k1gFnPc2
Leads	Leadsa	k1gFnPc2
Cast	Cast	k1gInSc1
of	of	k?
'	'	kIx"
<g/>
Lady	lady	k1gFnSc1
Liberty	Libert	k1gInPc4
<g/>
'	'	kIx"
<g/>
:	:	kIx,
<g/>
Ponti-Produced	Ponti-Produced	k1gInSc4
Farce	farka	k1gFnSc3
Opens	Opens	k1gInSc1
on	on	k3xPp3gMnSc1
Double	double	k2eAgMnSc1d1
Bill	Bill	k1gMnSc1
Posse	Posse	k1gFnSc2
Chases	Chases	k1gMnSc1
Bronson	Bronson	k1gMnSc1
in	in	k?
'	'	kIx"
<g/>
Chato	chata	k1gFnSc5
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Land	Landa	k1gFnPc2
<g/>
'	'	kIx"
-	-	kIx~
NYTimes	NYTimes	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
:	:	kIx,
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
ceskatelevize	ceskatelevize	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
305896435	#num#	k4
</s>
