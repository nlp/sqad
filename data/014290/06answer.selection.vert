<s>
Mortadela	mortadela	k1gFnSc1
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
La	la	k1gNnSc1
mortadella	mortadello	k1gNnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
francouzsko-italská	francouzsko-italský	k2eAgFnSc1d1
satirická	satirický	k2eAgFnSc1d1
komedie	komedie	k1gFnSc1
<g/>
,	,	kIx,
natočil	natočit	k5eAaBmAgMnS
ji	on	k3xPp3gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1971	#num#	k4
slavný	slavný	k2eAgMnSc1d1
italský	italský	k2eAgMnSc1d1
režisér	režisér	k1gMnSc1
Mario	Mario	k1gMnSc1
Monicelli	Monicell	k1gMnPc1
s	s	k7c7
hvězdným	hvězdný	k2eAgNnSc7d1
hereckým	herecký	k2eAgNnSc7d1
obsazením	obsazení	k1gNnSc7
-	-	kIx~
Sophie	Sophie	k1gFnSc1
Lorenová	Lorenová	k1gFnSc1
<g/>
,	,	kIx,
William	William	k1gInSc1
Devane	Devan	k1gMnSc5
<g/>
,	,	kIx,
Gigi	Gigi	k1gNnPc7
Proietti	Proietť	k1gFnSc2
a	a	k8xC
Danny	Danna	k1gMnSc2
DeVito	DeVit	k2eAgNnSc1d1
ve	v	k7c6
vedlejší	vedlejší	k2eAgFnSc6d1
roli	role	k1gFnSc6
politika	politik	k1gMnSc2
Freda	Fred	k1gMnSc2
Mancusa	Mancus	k1gMnSc2
<g/>
.	.	kIx.
jejímž	jejíž	k3xOyRp3gMnSc7
režisérem	režisér	k1gMnSc7
byl	být	k5eAaImAgMnS
Mario	Mario	k1gMnSc1
Monicelli	Monicelle	k1gFnSc4
a	a	k8xC
osobu	osoba	k1gFnSc4
hlavního	hlavní	k2eAgMnSc2d1
hrdiny	hrdina	k1gMnSc2
zahrála	zahrát	k5eAaPmAgFnS
Sofia	Sofia	k1gFnSc1
Lorenová	Lorenová	k1gFnSc1
<g/>
.	.	kIx.
</s>
