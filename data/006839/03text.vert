<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Rosa	Rosa	k1gMnSc1	Rosa
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
Hranice	hranice	k1gFnSc1	hranice
u	u	k7c2	u
Aše	Aš	k1gFnSc2	Aš
-	-	kIx~	-
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
československý	československý	k2eAgMnSc1d1	československý
atlet	atlet	k1gMnSc1	atlet
–	–	k?	–
překážkář	překážkář	k1gMnSc1	překážkář
<g/>
.	.	kIx.	.
</s>
<s>
Začínal	začínat	k5eAaImAgInS	začínat
ve	v	k7c6	v
Spartě	Sparta	k1gFnSc6	Sparta
Praha	Praha	k1gFnSc1	Praha
1965	[number]	k4	1965
<g/>
-	-	kIx~	-
<g/>
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
Dukla	Dukla	k1gFnSc1	Dukla
Praha	Praha	k1gFnSc1	Praha
1967	[number]	k4	1967
<g/>
-	-	kIx~	-
<g/>
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
Sparta	Sparta	k1gFnSc1	Sparta
Praha	Praha	k1gFnSc1	Praha
1969	[number]	k4	1969
<g/>
-	-	kIx~	-
<g/>
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Osobní	osobní	k2eAgInPc4d1	osobní
rekordy	rekord	k1gInPc4	rekord
<g/>
:	:	kIx,	:
110	[number]	k4	110
m	m	kA	m
př	př	kA	př
<g/>
.	.	kIx.	.
14,68	[number]	k4	14,68
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
14,1	[number]	k4	14,1
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
mezistátním	mezistátní	k2eAgNnSc6d1	mezistátní
utkání	utkání	k1gNnSc6	utkání
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Účastník	účastník	k1gMnSc1	účastník
EHJ	EHJ	kA	EHJ
1966	[number]	k4	1966
(	(	kIx(	(
<g/>
110	[number]	k4	110
m	m	kA	m
př	př	kA	př
<g/>
.7	.7	k4	.7
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
Evropských	evropský	k2eAgFnPc2d1	Evropská
juniorských	juniorský	k2eAgFnPc2d1	juniorská
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
předchůdce	předchůdce	k1gMnSc1	předchůdce
ME	ME	kA	ME
juniorů	junior	k1gMnPc2	junior
<g/>
)	)	kIx)	)
a	a	k8xC	a
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c4	na
7	[number]	k4	7
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
časem	časem	k6eAd1	časem
15.2	[number]	k4	15.2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
mistrovství	mistrovství	k1gNnSc1	mistrovství
ČSSR	ČSSR	kA	ČSSR
juniorů	junior	k1gMnPc2	junior
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
výkonem	výkon	k1gInSc7	výkon
14.5	[number]	k4	14.5
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
skončil	skončit	k5eAaPmAgMnS	skončit
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
v	v	k7c6	v
Jablonci	Jablonec	k1gInSc6	Jablonec
4	[number]	k4	4
<g/>
.	.	kIx.	.
časem	čas	k1gInSc7	čas
14.4	[number]	k4	14.4
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
byl	být	k5eAaImAgMnS	být
druhý	druhý	k4xOgMnSc1	druhý
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
v	v	k7c6	v
Jablonci	Jablonec	k1gInSc6	Jablonec
(	(	kIx(	(
<g/>
60	[number]	k4	60
<g/>
m	m	kA	m
př	př	kA	př
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
8.1	[number]	k4	8.1
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
skončil	skončit	k5eAaPmAgMnS	skončit
i	i	k8xC	i
na	na	k7c6	na
venkovním	venkovní	k2eAgNnSc6d1	venkovní
mistrovství	mistrovství	k1gNnSc6	mistrovství
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
časem	časem	k6eAd1	časem
14.1	[number]	k4	14.1
(	(	kIx(	(
<g/>
osobní	osobní	k2eAgInSc1d1	osobní
rekord	rekord	k1gInSc1	rekord
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
na	na	k7c6	na
utkání	utkání	k1gNnSc6	utkání
se	s	k7c7	s
Švédskem	Švédsko	k1gNnSc7	Švédsko
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
časem	časem	k6eAd1	časem
14.6	[number]	k4	14.6
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
byl	být	k5eAaImAgInS	být
5	[number]	k4	5
<g/>
.	.	kIx.	.
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
a	a	k8xC	a
venku	venku	k6eAd1	venku
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
(	(	kIx(	(
<g/>
14.3	[number]	k4	14.3
<g/>
)	)	kIx)	)
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
5	[number]	k4	5
<g/>
.	.	kIx.	.
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yInSc1	kdo
byl	být	k5eAaImAgMnS	být
kdo	kdo	k3yInSc1	kdo
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
atletice	atletika	k1gFnSc6	atletika
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
