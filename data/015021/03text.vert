<s>
Epikureismus	epikureismus	k1gInSc1
</s>
<s>
Epikúros	Epikúrosa	k1gFnPc2
</s>
<s>
Epikureismus	epikureismus	k1gInSc1
je	být	k5eAaImIp3nS
filozofický	filozofický	k2eAgInSc4d1
směr	směr	k1gInSc4
založený	založený	k2eAgInSc4d1
cca	cca	kA
v	v	k7c6
letech	let	k1gInPc6
310	#num#	k4
<g/>
–	–	k?
<g/>
306	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
na	na	k7c6
základě	základ	k1gInSc6
učení	učení	k1gNnSc2
starověkého	starověký	k2eAgMnSc2d1
řeckého	řecký	k2eAgMnSc2d1
filozofa	filozof	k1gMnSc2
Epikúra	Epikúr	k1gMnSc2
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
.	.	kIx.
st.	st.	kA
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
materialisty	materialista	k1gMnSc2
vycházejícího	vycházející	k2eAgMnSc2d1
z	z	k7c2
Démokritova	Démokritův	k2eAgInSc2d1
atomismu	atomismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Epikúrova	Epikúrův	k2eAgFnSc1d1
matka	matka	k1gFnSc1
byla	být	k5eAaImAgFnS
vědma	vědma	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
ho	on	k3xPp3gMnSc4
jako	jako	k9
chlapce	chlapec	k1gMnPc4
vodila	vodit	k5eAaImAgFnS
do	do	k7c2
domů	dům	k1gInPc2
nemocných	nemocný	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
dala	dát	k5eAaPmAgFnS
mu	on	k3xPp3gNnSc3
tak	tak	k9
poznat	poznat	k5eAaPmF
drastické	drastický	k2eAgInPc4d1
obrazy	obraz	k1gInPc4
lidské	lidský	k2eAgFnSc2d1
bídy	bída	k1gFnSc2
<g/>
,	,	kIx,
zostřené	zostřený	k2eAgNnSc1d1
temnotou	temnota	k1gFnSc7
pověr	pověra	k1gFnPc2
vyvěrajících	vyvěrající	k2eAgFnPc2d1
z	z	k7c2
lidového	lidový	k2eAgNnSc2d1
náboženství	náboženství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
bylo	být	k5eAaImAgNnS
v	v	k7c6
dospělosti	dospělost	k1gFnSc2
jedním	jeden	k4xCgInSc7
z	z	k7c2
filozofových	filozofův	k2eAgInPc2d1
hlavních	hlavní	k2eAgInPc2d1
plánů	plán	k1gInPc2
osvobození	osvobození	k1gNnSc2
člověka	člověk	k1gMnSc2
z	z	k7c2
tohoto	tento	k3xDgNnSc2
nedobrovolného	dobrovolný	k2eNgNnSc2d1
otroctví	otroctví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Epikúros	Epikúrosa	k1gFnPc2
založil	založit	k5eAaPmAgMnS
v	v	k7c6
Athénách	Athéna	k1gFnPc6
školu	škola	k1gFnSc4
<g/>
,	,	kIx,
Zahradu	zahrada	k1gFnSc4
Epikúrovu	Epikúrův	k2eAgFnSc4d1
<g/>
;	;	kIx,
proto	proto	k8xC
jsou	být	k5eAaImIp3nP
epikurejci	epikurejec	k1gMnPc1
někdy	někdy	k6eAd1
nazýváni	nazývat	k5eAaImNgMnP
„	„	k?
<g/>
zahradními	zahradní	k2eAgMnPc7d1
filozofy	filozof	k1gMnPc7
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Filozofie	filozofie	k1gFnSc1
</s>
<s>
Středem	středem	k7c2
Epikúrovy	Epikúrův	k2eAgFnSc2d1
filozofie	filozofie	k1gFnSc2
je	být	k5eAaImIp3nS
etika	etika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
úkolem	úkol	k1gInSc7
dalších	další	k2eAgInPc2d1
dvou	dva	k4xCgInPc2
filozofických	filozofický	k2eAgInPc2d1
oborů	obor	k1gInPc2
<g/>
,	,	kIx,
o	o	k7c6
nichž	jenž	k3xRgFnPc6
epikurejci	epikurejec	k1gMnPc1
pojednávali	pojednávat	k5eAaImAgMnP
<g/>
,	,	kIx,
tj.	tj.	kA
kanoniky	kanonika	k1gFnSc2
a	a	k8xC
fyziky	fyzika	k1gFnSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
zdůvodnění	zdůvodnění	k1gNnSc1
etických	etický	k2eAgFnPc2d1
zásad	zásada	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kanonika	Kanonika	k1gFnSc1
a	a	k8xC
fyzika	fyzika	k1gFnSc1
</s>
<s>
Epikúrova	Epikúrův	k2eAgFnSc1d1
kanonika	kanonika	k1gFnSc1
pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c6
kritériích	kritérion	k1gNnPc6
(	(	kIx(
<g/>
kánonech	kánon	k1gInPc6
<g/>
)	)	kIx)
pravdy	pravda	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Základem	základ	k1gInSc7
všeho	všecek	k3xTgNnSc2
poznání	poznání	k1gNnSc2
jsou	být	k5eAaImIp3nP
smysly	smysl	k1gInPc4
(	(	kIx(
<g/>
senzualismus	senzualismus	k1gInSc4
<g/>
)	)	kIx)
a	a	k8xC
ty	ten	k3xDgInPc4
jsou	být	k5eAaImIp3nP
prakticky	prakticky	k6eAd1
vždy	vždy	k6eAd1
spolehlivé	spolehlivý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Případná	případný	k2eAgFnSc1d1
chyba	chyba	k1gFnSc1
nepramení	pramenit	k5eNaImIp3nS
v	v	k7c6
nich	on	k3xPp3gMnPc6
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
našem	náš	k3xOp1gNnSc6
myšlení	myšlení	k1gNnSc6
<g/>
,	,	kIx,
úsudku	úsudek	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Smyslové	smyslový	k2eAgNnSc1d1
vnímání	vnímání	k1gNnSc1
je	být	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc7
bezprostřední	bezprostřední	k2eAgFnSc7d1
zřejmostí	zřejmost	k1gFnSc7
i	i	k8xC
jediným	jediný	k2eAgNnSc7d1
kritériem	kritérion	k1gNnSc7
pravdy	pravda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
fyzice	fyzika	k1gFnSc6
sledovali	sledovat	k5eAaImAgMnP
epikurejci	epikurejec	k1gMnPc1
praktický	praktický	k2eAgInSc4d1
cíl	cíl	k1gInSc4
<g/>
:	:	kIx,
poznání	poznání	k1gNnSc4
přírody	příroda	k1gFnSc2
a	a	k8xC
jejích	její	k3xOp3gInPc2
zákonů	zákon	k1gInPc2
má	mít	k5eAaImIp3nS
cenu	cena	k1gFnSc4
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
dovede	dovést	k5eAaPmIp3nS
zbavit	zbavit	k5eAaPmF
člověka	člověk	k1gMnSc4
hrůzy	hrůza	k1gFnSc2
před	před	k7c7
nadpřirozenem	nadpřirozeno	k1gNnSc7
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
bázně	bázeň	k1gFnPc1
před	před	k7c7
bohy	bůh	k1gMnPc7
a	a	k8xC
před	před	k7c7
smrtí	smrt	k1gFnSc7
<g/>
;	;	kIx,
tento	tento	k3xDgInSc1
strach	strach	k1gInSc1
nejvíce	nejvíce	k6eAd1,k6eAd3
ohrožuje	ohrožovat	k5eAaImIp3nS
klid	klid	k1gInSc1
a	a	k8xC
blaženost	blaženost	k1gFnSc1
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Epikúros	Epikúrosa	k1gFnPc2
navazoval	navazovat	k5eAaImAgInS
na	na	k7c4
Démokritův	Démokritův	k2eAgInSc4d1
atomismus	atomismus	k1gInSc4
a	a	k8xC
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
existují	existovat	k5eAaImIp3nP
jen	jen	k9
dvě	dva	k4xCgNnPc1
jsoucna	jsoucno	k1gNnPc1
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
lze	lze	k6eAd1
odvodit	odvodit	k5eAaPmF
vznik	vznik	k1gInSc4
všeho	všecek	k3xTgNnSc2
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
je	být	k5eAaImIp3nS
a	a	k8xC
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
děje	dít	k5eAaImIp3nS
<g/>
:	:	kIx,
atomy	atom	k1gInPc1
a	a	k8xC
prázdný	prázdný	k2eAgInSc1d1
<g/>
,	,	kIx,
nekonečný	konečný	k2eNgInSc1d1
prostor	prostor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
lidská	lidský	k2eAgFnSc1d1
duše	duše	k1gFnSc1
je	být	k5eAaImIp3nS
hmotná	hmotný	k2eAgFnSc1d1
a	a	k8xC
skládá	skládat	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
jemných	jemný	k2eAgInPc2d1
atomů	atom	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
nerozlučně	rozlučně	k6eNd1
spjata	spjat	k2eAgFnSc1d1
s	s	k7c7
tělem	tělo	k1gNnSc7
a	a	k8xC
v	v	k7c6
okamžiku	okamžik	k1gInSc6
smrti	smrt	k1gFnSc2
se	se	k3xPyFc4
její	její	k3xOp3gInPc1
atomy	atom	k1gInPc1
rozptylují	rozptylovat	k5eAaImIp3nP
do	do	k7c2
nekonečného	konečný	k2eNgInSc2d1
prostoru	prostor	k1gInSc2
<g/>
;	;	kIx,
posmrtný	posmrtný	k2eAgInSc1d1
život	život	k1gInSc1
neexistuje	existovat	k5eNaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Existenci	existence	k1gFnSc4
nesmrtelných	smrtelný	k2eNgMnPc2d1
bohů	bůh	k1gMnPc2
epikureismus	epikureismus	k1gInSc1
uznává	uznávat	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
na	na	k7c6
základě	základ	k1gInSc6
souhlasu	souhlas	k1gInSc2
všech	všecek	k3xTgInPc2
národů	národ	k1gInPc2
v	v	k7c6
této	tento	k3xDgFnSc6
otázce	otázka	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
bytosti	bytost	k1gFnPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnPc1
těla	tělo	k1gNnPc1
jsou	být	k5eAaImIp3nP
složena	složit	k5eAaPmNgNnP
z	z	k7c2
nejjemnějších	jemný	k2eAgInPc2d3
světelných	světelný	k2eAgInPc2d1
atomů	atom	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žijí	žít	k5eAaImIp3nP
blaženě	blaženě	k6eAd1
v	v	k7c6
ničím	ničí	k3xOyNgInSc6
nerušeném	rušený	k2eNgInSc6d1
klidu	klid	k1gInSc6
v	v	k7c6
prázdných	prázdný	k2eAgFnPc6d1
prostorách	prostora	k1gFnPc6
mezi	mezi	k7c7
jednotlivými	jednotlivý	k2eAgInPc7d1
světy	svět	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nijak	nijak	k6eAd1
nezasahují	zasahovat	k5eNaImIp3nP
do	do	k7c2
světového	světový	k2eAgNnSc2d1
dění	dění	k1gNnSc2
a	a	k8xC
do	do	k7c2
lidských	lidský	k2eAgInPc2d1
osudů	osud	k1gInPc2
<g/>
:	:	kIx,
ani	ani	k8xC
neodměňují	odměňovat	k5eNaImIp3nP
<g/>
,	,	kIx,
ani	ani	k8xC
netrestají	trestat	k5eNaImIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Epikurejská	epikurejský	k2eAgFnSc1d1
fyzika	fyzika	k1gFnSc1
tedy	tedy	k9
odstraňuje	odstraňovat	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
hlavní	hlavní	k2eAgFnPc4d1
překážky	překážka	k1gFnPc4
lidského	lidský	k2eAgNnSc2d1
blaha	blaho	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
strach	strach	k1gInSc1
z	z	k7c2
bohů	bůh	k1gMnPc2
<g/>
;	;	kIx,
</s>
<s>
strach	strach	k1gInSc1
ze	z	k7c2
smrti	smrt	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
nemusíme	muset	k5eNaImIp1nP
obávat	obávat	k5eAaImF
<g/>
,	,	kIx,
protože	protože	k8xS
smrt	smrt	k1gFnSc1
je	být	k5eAaImIp3nS
rozkladem	rozklad	k1gInSc7
duše	duše	k1gFnSc2
i	i	k8xC
těla	tělo	k1gNnSc2
a	a	k8xC
veškeré	veškerý	k3xTgNnSc4
vnímání	vnímání	k1gNnSc4
smrtí	smrtit	k5eAaImIp3nS
končí	končit	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Epikúros	Epikúrosa	k1gFnPc2
konstatuje	konstatovat	k5eAaBmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
když	když	k8xS
jsme	být	k5eAaImIp1nP
tu	tu	k6eAd1
my	my	k3xPp1nPc1
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
tu	ten	k3xDgFnSc4
smrt	smrt	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
když	když	k8xS
je	být	k5eAaImIp3nS
tu	ten	k3xDgFnSc4
smrt	smrt	k1gFnSc4
<g/>
,	,	kIx,
nejsme	být	k5eNaImIp1nP
tu	tu	k6eAd1
již	již	k9
my	my	k3xPp1nPc1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Etika	etika	k1gFnSc1
</s>
<s>
Ideálem	ideál	k1gInSc7
je	být	k5eAaImIp3nS
ataraxie	ataraxie	k1gFnSc1
</s>
<s>
Epikúrova	Epikúrův	k2eAgFnSc1d1
etika	etika	k1gFnSc1
ukazuje	ukazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
v	v	k7c6
čem	co	k3yInSc6,k3yQnSc6,k3yRnSc6
záleží	záležet	k5eAaImIp3nS
lidská	lidský	k2eAgFnSc1d1
blaženost	blaženost	k1gFnSc1
a	a	k8xC
jak	jak	k6eAd1
ji	on	k3xPp3gFnSc4
lze	lze	k6eAd1
dosáhnout	dosáhnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
této	tento	k3xDgFnSc2
etiky	etika	k1gFnSc2
je	být	k5eAaImIp3nS
počátkem	počátek	k1gInSc7
i	i	k8xC
cílem	cíl	k1gInSc7
blaženého	blažený	k2eAgInSc2d1
života	život	k1gInSc2
slast	slast	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snaha	snaha	k1gFnSc1
po	po	k7c6
slasti	slast	k1gFnSc6
a	a	k8xC
vyhýbání	vyhýbání	k1gNnSc6
se	se	k3xPyFc4
strastem	strast	k1gFnPc3
je	být	k5eAaImIp3nS
vlastní	vlastní	k2eAgInPc4d1
všem	všecek	k3xTgFnPc3
živým	živý	k2eAgFnPc3d1
bytostem	bytost	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Více	hodně	k6eAd2
než	než	k8xS
chvilkovou	chvilkový	k2eAgFnSc4d1
pozitivní	pozitivní	k2eAgFnSc4d1
rozkoš	rozkoš	k1gFnSc4
však	však	k8xC
Epikúros	Epikúrosa	k1gFnPc2
cení	cenit	k5eAaImIp3nS
rozkoš	rozkoš	k1gFnSc4
negativní	negativní	k2eAgFnSc4d1
<g/>
,	,	kIx,
bezbolestnost	bezbolestnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ideálem	ideál	k1gInSc7
je	být	k5eAaImIp3nS
trvalý	trvalý	k2eAgInSc1d1
stav	stav	k1gInSc1
blaženosti	blaženost	k1gFnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc7
hlavní	hlavní	k2eAgFnSc7d1
podmínkou	podmínka	k1gFnSc7
je	být	k5eAaImIp3nS
vedle	vedle	k7c2
tělesného	tělesný	k2eAgNnSc2d1
zdraví	zdraví	k1gNnSc2
vyrovnaný	vyrovnaný	k2eAgInSc4d1
stav	stav	k1gInSc4
lidského	lidský	k2eAgNnSc2d1
nitra	nitro	k1gNnSc2
<g/>
,	,	kIx,
úplný	úplný	k2eAgInSc1d1
duševní	duševní	k2eAgInSc1d1
klid	klid	k1gInSc1
(	(	kIx(
<g/>
ataraxie	ataraxie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
Epikúrově	Epikúrův	k2eAgInSc6d1
dopise	dopis	k1gInSc6
Menoikeovi	Menoikeus	k1gMnSc3
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
<g/>
:	:	kIx,
„	„	k?
<g/>
Proto	proto	k8xC
když	když	k8xS
říkáme	říkat	k5eAaImIp1nP
<g/>
,	,	kIx,
že	že	k8xS
slast	slast	k1gFnSc1
je	být	k5eAaImIp3nS
konečným	konečný	k2eAgInSc7d1
cílem	cíl	k1gInSc7
<g/>
,	,	kIx,
nemíníme	mínit	k5eNaImIp1nP
tím	ten	k3xDgNnSc7
slasti	slast	k1gFnPc1
prostopášníků	prostopášník	k1gMnPc2
a	a	k8xC
ty	ten	k3xDgInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
spočívají	spočívat	k5eAaImIp3nP
v	v	k7c6
požitcích	požitek	k1gInPc6
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
domnívají	domnívat	k5eAaImIp3nP
někteří	některý	k3yIgMnPc1
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
neznají	znát	k5eNaImIp3nP,k5eAaImIp3nP
naše	náš	k3xOp1gNnSc4
učení	učení	k1gNnSc4
nebo	nebo	k8xC
s	s	k7c7
ním	on	k3xPp3gInSc7
nesouhlasí	souhlasit	k5eNaImIp3nP
nebo	nebo	k8xC
si	se	k3xPyFc3
je	on	k3xPp3gInPc4
špatně	špatně	k6eAd1
vykládají	vykládat	k5eAaImIp3nP
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
stav	stav	k1gInSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
člověk	člověk	k1gMnSc1
necítí	cítit	k5eNaImIp3nS
bolest	bolest	k1gFnSc4
v	v	k7c6
těle	tělo	k1gNnSc6
a	a	k8xC
nemá	mít	k5eNaImIp3nS
neklid	neklid	k1gInSc4
v	v	k7c6
duši	duše	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neboť	neboť	k8xC
život	život	k1gInSc1
nečiní	činit	k5eNaImIp3nS
slastným	slastný	k2eAgNnSc7d1
ani	ani	k8xC
ustavičné	ustavičný	k2eAgFnPc4d1
pitky	pitka	k1gFnPc4
a	a	k8xC
radovánky	radovánka	k1gFnPc4
<g/>
,	,	kIx,
ani	ani	k8xC
požitky	požitek	k1gInPc1
s	s	k7c7
hochy	hoch	k1gMnPc7
a	a	k8xC
s	s	k7c7
dívkami	dívka	k1gFnPc7
<g/>
,	,	kIx,
ani	ani	k8xC
požívání	požívání	k1gNnSc1
ryb	ryba	k1gFnPc2
a	a	k8xC
jiných	jiný	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
nabízí	nabízet	k5eAaImIp3nS
bohatý	bohatý	k2eAgInSc4d1
stůl	stůl	k1gInSc4
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
střízlivý	střízlivý	k2eAgInSc4d1
rozumový	rozumový	k2eAgInSc4d1
úsudek	úsudek	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
vyhledává	vyhledávat	k5eAaImIp3nS
důvody	důvod	k1gInPc4
pro	pro	k7c4
každý	každý	k3xTgInSc4
akt	akt	k1gInSc4
volby	volba	k1gFnSc2
nebo	nebo	k8xC
odmítnutí	odmítnutí	k1gNnSc3
a	a	k8xC
zahání	zahánět	k5eAaImIp3nP
plané	planý	k2eAgFnPc1d1
domněnky	domněnka	k1gFnPc1
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
se	se	k3xPyFc4
rodí	rodit	k5eAaImIp3nS
největší	veliký	k2eAgInSc1d3
zmatek	zmatek	k1gInSc1
v	v	k7c6
lidských	lidský	k2eAgFnPc6d1
duších	duše	k1gFnPc6
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozumné	rozumný	k2eAgNnSc1d1
uspokojování	uspokojování	k1gNnSc1
žádostí	žádost	k1gFnPc2
<g/>
;	;	kIx,
„	„	k?
<g/>
i	i	k8xC
střídmost	střídmost	k1gFnSc1
má	mít	k5eAaImIp3nS
své	svůj	k3xOyFgFnPc4
meze	mez	k1gFnPc4
<g/>
"	"	kIx"
</s>
<s>
Umění	umění	k1gNnSc1
radovat	radovat	k5eAaImF
se	se	k3xPyFc4
ze	z	k7c2
života	život	k1gInSc2
bývá	bývat	k5eAaImIp3nS
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
jeden	jeden	k4xCgInSc4
z	z	k7c2
význačných	význačný	k2eAgInPc2d1
rysů	rys	k1gInPc2
epikureismu	epikureismus	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Asketismus	asketismus	k1gInSc1
byl	být	k5eAaImAgInS
epikurejcům	epikurejec	k1gMnPc3
cizí	cizí	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neváhali	váhat	k5eNaImAgMnP
prohlásit	prohlásit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
počátek	počátek	k1gInSc4
a	a	k8xC
kořen	kořen	k1gInSc4
všeho	všecek	k3xTgNnSc2
dobra	dobro	k1gNnSc2
je	být	k5eAaImIp3nS
slast	slast	k1gFnSc1
žaludku	žaludek	k1gInSc2
<g/>
;	;	kIx,
i	i	k9
moudrost	moudrost	k1gFnSc4
a	a	k8xC
vyšší	vysoký	k2eAgInPc4d2
projevy	projev	k1gInPc4
života	život	k1gInSc2
na	na	k7c6
ní	on	k3xPp3gFnSc2
závisí	záviset	k5eAaImIp3nP
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
„	„	k?
<g/>
Nesmíme	smět	k5eNaImIp1nP
činiti	činit	k5eAaImF
násilí	násilí	k1gNnSc2
své	svůj	k3xOyFgFnSc2
přirozenosti	přirozenost	k1gFnSc2
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
poslouchati	poslouchat	k5eAaImF
jí	jíst	k5eAaImIp3nS
<g/>
,	,	kIx,
poslechneme	poslechnout	k5eAaPmIp1nP
pak	pak	k6eAd1
jí	jíst	k5eAaImIp3nS
<g/>
,	,	kIx,
ukojíme	ukojit	k5eAaPmIp1nP
<g/>
-li	-li	k?
nezbytné	nezbytný	k2eAgFnPc4d1,k2eNgFnPc4d1
žádosti	žádost	k1gFnPc4
a	a	k8xC
přirozené	přirozený	k2eAgFnPc4d1
žádosti	žádost	k1gFnPc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
nejsou	být	k5eNaImIp3nP
škodlivé	škodlivý	k2eAgFnPc1d1
<g/>
...	...	k?
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozlišovali	rozlišovat	k5eAaImAgMnP
mezi	mezi	k7c7
rozkošemi	rozkoš	k1gFnPc7
přirozenými	přirozený	k2eAgFnPc7d1
a	a	k8xC
nutnými	nutný	k2eAgFnPc7d1
(	(	kIx(
<g/>
ukojení	ukojení	k1gNnPc1
hladu	hlad	k1gInSc2
a	a	k8xC
jiné	jiný	k2eAgFnPc1d1
nezbytné	zbytný	k2eNgFnPc1d1,k2eAgFnPc1d1
potřeby	potřeba	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přirozenými	přirozený	k2eAgInPc7d1
<g/>
,	,	kIx,
ale	ale	k8xC
ne	ne	k9
nutnými	nutný	k2eAgFnPc7d1
(	(	kIx(
<g/>
jako	jako	k9
jsou	být	k5eAaImIp3nP
lahůdky	lahůdka	k1gFnPc4
<g/>
)	)	kIx)
a	a	k8xC
rozkošemi	rozkoš	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
nejsou	být	k5eNaImIp3nP
ani	ani	k8xC
přirozené	přirozený	k2eAgNnSc1d1
<g/>
,	,	kIx,
ani	ani	k8xC
nutné	nutný	k2eAgNnSc1d1
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
záliba	záliba	k1gFnSc1
v	v	k7c6
přepychu	přepych	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doporučovali	doporučovat	k5eAaImAgMnP
vyhnout	vyhnout	k5eAaPmF
se	se	k3xPyFc4
nestřídmosti	nestřídmost	k1gFnSc2
a	a	k8xC
umělým	umělý	k2eAgFnPc3d1
rozkoším	rozkoš	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
nakonec	nakonec	k6eAd1
rodí	rodit	k5eAaImIp3nP
bolest	bolest	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
spokojit	spokojit	k5eAaPmF
se	se	k3xPyFc4
s	s	k7c7
přirozenými	přirozený	k2eAgFnPc7d1
slastmi	slast	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
nevedou	vést	k5eNaImIp3nP
k	k	k7c3
utrpení	utrpení	k1gNnSc3
těla	tělo	k1gNnSc2
a	a	k8xC
zmatku	zmatek	k1gInSc2
v	v	k7c6
duši	duše	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Epikúros	Epikúrosa	k1gFnPc2
napsal	napsat	k5eAaBmAgInS,k5eAaPmAgInS
<g/>
:	:	kIx,
„	„	k?
<g/>
Nezávislost	nezávislost	k1gFnSc4
na	na	k7c6
žádostech	žádost	k1gFnPc6
pokládáme	pokládat	k5eAaImIp1nP
za	za	k7c4
velké	velký	k2eAgNnSc4d1
dobro	dobro	k1gNnSc4
<g/>
,	,	kIx,
ne	ne	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
bychom	by	kYmCp1nP
chtěli	chtít	k5eAaImAgMnP
mít	mít	k5eAaImF
stále	stále	k6eAd1
málo	málo	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
proto	proto	k8xC
<g/>
,	,	kIx,
abychom	aby	kYmCp1nP
se	se	k3xPyFc4
<g/>
,	,	kIx,
jestliže	jestliže	k8xS
nemáme	mít	k5eNaImIp1nP
mnoho	mnoho	k6eAd1
<g/>
,	,	kIx,
dovedli	dovést	k5eAaPmAgMnP
málem	málem	k6eAd1
spokojit	spokojit	k5eAaPmF
<g/>
...	...	k?
(	(	kIx(
<g/>
...	...	k?
<g/>
)	)	kIx)
Jídla	jídlo	k1gNnPc4
s	s	k7c7
prostou	prostý	k2eAgFnSc7d1
chutí	chuť	k1gFnSc7
působí	působit	k5eAaImIp3nS
stejný	stejný	k2eAgInSc4d1
pocit	pocit	k1gInSc4
slasti	slast	k1gFnSc2
jako	jako	k8xS,k8xC
nákladná	nákladný	k2eAgFnSc1d1
hostina	hostina	k1gFnSc1
<g/>
,	,	kIx,
jestliže	jestliže	k8xS
člověka	člověk	k1gMnSc4
zbavují	zbavovat	k5eAaImIp3nP
veškeré	veškerý	k3xTgFnPc4
trýzně	trýzeň	k1gFnPc4
vzniklé	vzniklý	k2eAgFnPc4d1
z	z	k7c2
pocitu	pocit	k1gInSc2
nedostatku	nedostatek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chléb	chléb	k1gInSc4
a	a	k8xC
voda	voda	k1gFnSc1
vyvolávají	vyvolávat	k5eAaImIp3nP
nejvyšší	vysoký	k2eAgInSc4d3
pocit	pocit	k1gInSc4
slasti	slast	k1gFnSc2
<g/>
,	,	kIx,
jestliže	jestliže	k8xS
je	on	k3xPp3gFnPc4
člověk	člověk	k1gMnSc1
požívá	požívat	k5eAaImIp3nS
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
je	on	k3xPp3gNnSc4
potřebuje	potřebovat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Zároveň	zároveň	k6eAd1
však	však	k9
epikurejci	epikurejec	k1gMnPc1
upozorňovali	upozorňovat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
i	i	k8xC
střídmost	střídmost	k1gFnSc1
má	mít	k5eAaImIp3nS
své	svůj	k3xOyFgFnPc4
meze	mez	k1gFnPc4
<g/>
;	;	kIx,
kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
toho	ten	k3xDgNnSc2
nedbá	dbát	k5eNaImIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
na	na	k7c6
tom	ten	k3xDgNnSc6
podobně	podobně	k6eAd1
jako	jako	k9
ten	ten	k3xDgMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
chybuje	chybovat	k5eAaImIp3nS
nemírností	nemírnost	k1gFnSc7
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jednotlivec	jednotlivec	k1gMnSc1
a	a	k8xC
společnost	společnost	k1gFnSc1
<g/>
;	;	kIx,
chvála	chvála	k1gFnSc1
přátelství	přátelství	k1gNnSc2
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
stoiků	stoik	k1gMnPc2
Epikúros	Epikúrosa	k1gFnPc2
nedoporučoval	doporučovat	k5eNaImAgMnS
účast	účast	k1gFnSc4
na	na	k7c6
veřejném	veřejný	k2eAgInSc6d1
životě	život	k1gInSc6
<g/>
,	,	kIx,
neboť	neboť	k8xC
život	život	k1gInSc1
soukromý	soukromý	k2eAgInSc1d1
(	(	kIx(
<g/>
prožitý	prožitý	k2eAgInSc1d1
„	„	k?
<g/>
v	v	k7c6
skrytu	skryt	k1gInSc6
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
poskytuje	poskytovat	k5eAaImIp3nS
mnohem	mnohem	k6eAd1
větší	veliký	k2eAgFnPc4d2
záruky	záruka	k1gFnPc4
bezpečí	bezpečí	k1gNnSc2
<g/>
,	,	kIx,
klidu	klid	k1gInSc2
a	a	k8xC
nezávislosti	nezávislost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Dle	dle	k7c2
<g />
.	.	kIx.
</s>
<s hack="1">
epikurejců	epikurejec	k1gMnPc2
„	„	k?
<g/>
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
se	se	k3xPyFc4
vysvoboditi	vysvobodit	k5eAaPmF
z	z	k7c2
pout	pouto	k1gNnPc2
všedních	všední	k2eAgInPc2d1
zájmů	zájem	k1gInPc2
a	a	k8xC
veřejného	veřejný	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
tzv.	tzv.	kA
Hlavních	hlavní	k2eAgFnPc6d1
myšlenkách	myšlenka	k1gFnPc6
se	se	k3xPyFc4
praví	pravit	k5eAaBmIp3nS,k5eAaImIp3nS
<g/>
:	:	kIx,
„	„	k?
<g/>
...	...	k?
nejdokonaleji	dokonale	k6eAd3
bezpečnost	bezpečnost	k1gFnSc1
vzniká	vznikat	k5eAaImIp3nS
z	z	k7c2
tichého	tichý	k2eAgInSc2d1
života	život	k1gInSc2
a	a	k8xC
stranění	stranění	k1gNnSc2
se	se	k3xPyFc4
lidu	lid	k1gInSc3
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
2	#num#	k4
<g/>
]	]	kIx)
Snaha	snaha	k1gFnSc1
po	po	k7c6
klidu	klid	k1gInSc6
a	a	k8xC
trvalém	trvalý	k2eAgInSc6d1
stavu	stav	k1gInSc6
blaženosti	blaženost	k1gFnSc2
vedla	vést	k5eAaImAgFnS
Epikura	Epikur	k1gMnSc4
k	k	k7c3
názoru	názor	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
ani	ani	k8xC
rodina	rodina	k1gFnSc1
a	a	k8xC
výchova	výchova	k1gFnSc1
dětí	dítě	k1gFnPc2
není	být	k5eNaImIp3nS
žádoucí	žádoucí	k2eAgFnSc7d1
formou	forma	k1gFnSc7
života	život	k1gInSc2
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
„	„	k?
<g/>
mudrc	mudrc	k1gMnSc1
prý	prý	k9
se	se	k3xPyFc4
nebude	být	k5eNaImBp3nS
ani	ani	k9
ženit	ženit	k5eAaImF
<g/>
,	,	kIx,
ani	ani	k8xC
plodit	plodit	k5eAaImF
děti	dítě	k1gFnPc4
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
však	však	k9
sňatek	sňatek	k1gInSc1
vhodný	vhodný	k2eAgInSc1d1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
záleží	záležet	k5eAaImIp3nS
na	na	k7c6
životních	životní	k2eAgFnPc6d1
okolnostech	okolnost	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Epikurejci	epikurejec	k1gMnPc1
vysoce	vysoce	k6eAd1
oceňovali	oceňovat	k5eAaImAgMnP
přátelství	přátelství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc4
se	se	k3xPyFc4
sice	sice	k8xC
jako	jako	k9
všechny	všechen	k3xTgInPc4
vztahy	vztah	k1gInPc4
mezi	mezi	k7c7
lidmi	člověk	k1gMnPc7
zakládá	zakládat	k5eAaImIp3nS
na	na	k7c6
pohnutkách	pohnutka	k1gFnPc6
egoistických	egoistický	k2eAgFnPc6d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
je	být	k5eAaImIp3nS
nejdokonalejší	dokonalý	k2eAgFnSc7d3
formou	forma	k1gFnSc7
lidského	lidský	k2eAgNnSc2d1
společenství	společenství	k1gNnSc2
a	a	k8xC
nejvíce	nejvíce	k6eAd1,k6eAd3
přispívá	přispívat	k5eAaImIp3nS
k	k	k7c3
dosažení	dosažení	k1gNnSc3
životní	životní	k2eAgFnSc2d1
blaženosti	blaženost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
tzv.	tzv.	kA
Hlavních	hlavní	k2eAgFnPc6d1
myšlenkách	myšlenka	k1gFnPc6
se	se	k3xPyFc4
k	k	k7c3
tomu	ten	k3xDgNnSc3
uvádí	uvádět	k5eAaImIp3nS
<g/>
:	:	kIx,
„	„	k?
<g/>
Z	z	k7c2
toho	ten	k3xDgNnSc2
všeho	všecek	k3xTgNnSc2
<g/>
,	,	kIx,
čím	čí	k3xOyRgNnSc7,k3xOyQgNnSc7
moudrost	moudrost	k1gFnSc1
hledí	hledět	k5eAaImIp3nS
zabezpečiti	zabezpečit	k5eAaPmF
blaženost	blaženost	k1gFnSc4
celého	celý	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
daleko	daleko	k6eAd1
nejdůležitější	důležitý	k2eAgFnSc7d3
věcí	věc	k1gFnSc7
získání	získání	k1gNnSc2
přátelství	přátelství	k1gNnSc2
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Epikureismus	epikureismus	k1gInSc4
lze	lze	k6eAd1
charakterizovat	charakterizovat	k5eAaBmF
jako	jako	k8xC,k8xS
filozofii	filozofie	k1gFnSc4
životní	životní	k2eAgFnSc2d1
radosti	radost	k1gFnSc2
a	a	k8xC
vlídné	vlídný	k2eAgFnSc3d1
lidskosti	lidskost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
nacházel	nacházet	k5eAaImAgMnS
mnoho	mnoho	k4c4
oddaných	oddaný	k2eAgMnPc2d1
vyznavačů	vyznavač	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Epikurejská	epikurejský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
310	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
založil	založit	k5eAaPmAgMnS
Epikúros	Epikúrosa	k1gFnPc2
filozofickou	filozofický	k2eAgFnSc4d1
školu	škola	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
nejprve	nejprve	k6eAd1
v	v	k7c6
Mytiléně	Mytiléna	k1gFnSc6
<g/>
,	,	kIx,
později	pozdě	k6eAd2
v	v	k7c6
Lampsaku	Lampsak	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
306	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
škola	škola	k1gFnSc1
přesídlila	přesídlit	k5eAaPmAgFnS
do	do	k7c2
Athén	Athéna	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
pro	pro	k7c4
ni	on	k3xPp3gFnSc4
Epikúros	Epikúrosa	k1gFnPc2
zakoupil	zakoupit	k5eAaPmAgMnS
<g />
.	.	kIx.
</s>
<s hack="1">
dům	dům	k1gInSc1
se	s	k7c7
zahradou	zahrada	k1gFnSc7
<g/>
;	;	kIx,
po	po	k7c6
ní	on	k3xPp3gFnSc6
se	se	k3xPyFc4
jeho	jeho	k3xOp3gFnSc1
škola	škola	k1gFnSc1
nazývala	nazývat	k5eAaImAgFnS
Zahrada	zahrada	k1gFnSc1
(	(	kIx(
<g/>
řec.	řec.	k?
κ	κ	k?
<g/>
;	;	kIx,
Kê	Kê	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
Seneky	Seneka	k1gMnSc2
byl	být	k5eAaImAgInS
nad	nad	k7c7
jejím	její	k3xOp3gInSc7
vchodem	vchod	k1gInSc7
nápis	nápis	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Cizinče	cizinec	k1gMnSc5
<g/>
,	,	kIx,
u	u	k7c2
nás	my	k3xPp1nPc2
ti	ten	k3xDgMnPc1
bude	být	k5eAaImBp3nS
dobře	dobře	k6eAd1
<g/>
,	,	kIx,
u	u	k7c2
nás	my	k3xPp1nPc2
je	být	k5eAaImIp3nS
nejvyšším	vysoký	k2eAgNnSc7d3
dobrem	dobro	k1gNnSc7
rozkoš	rozkoš	k1gFnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
Byl	být	k5eAaImAgInS
to	ten	k3xDgNnSc1
jakýsi	jakýsi	k3yIgInSc1
spolek	spolek	k1gInSc1
přátel	přítel	k1gMnPc2
žijících	žijící	k2eAgMnPc2d1
podle	podle	k7c2
společných	společný	k2eAgFnPc2d1
zásad	zásada	k1gFnPc2
stranou	stranou	k6eAd1
občanského	občanský	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členové	člen	k1gMnPc1
se	se	k3xPyFc4
věnovali	věnovat	k5eAaImAgMnP,k5eAaPmAgMnP
četbě	četba	k1gFnSc3
<g/>
,	,	kIx,
rozboru	rozbor	k1gInSc6
a	a	k8xC
opisování	opisování	k1gNnSc6
Epikúrových	Epikúrův	k2eAgFnPc2d1
knih	kniha	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezi	mezi	k7c4
nejvýznamnější	významný	k2eAgMnPc4d3
členy	člen	k1gMnPc4
patřil	patřit	k5eAaImAgInS
Metrodóros	Metrodórosa	k1gFnPc2
z	z	k7c2
Lampsaku	Lampsak	k1gInSc2
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
početných	početný	k2eAgFnPc2d1
děl	dělo	k1gNnPc2
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgFnPc6,k3yQgFnPc6,k3yRgFnPc6
hájil	hájit	k5eAaImAgMnS
učení	učení	k1gNnSc2
svého	svůj	k3xOyFgMnSc2
mistra	mistr	k1gMnSc2
(	(	kIx(
<g/>
zachovaly	zachovat	k5eAaPmAgInP
se	se	k3xPyFc4
jen	jen	k9
zlomky	zlomek	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
spolku	spolek	k1gInSc2
měly	mít	k5eAaImAgFnP
přístup	přístup	k1gInSc4
i	i	k9
ženy	žena	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
z	z	k7c2
nich	on	k3xPp3gFnPc2
byla	být	k5eAaImAgFnS
vzdělaná	vzdělaný	k2eAgFnSc1d1
hetéra	hetéra	k1gFnSc1
Leontion	Leontion	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
si	se	k3xPyFc3
Metrodóros	Metrodórosa	k1gFnPc2
vzal	vzít	k5eAaPmAgInS
za	za	k7c4
souložnici	souložnice	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
době	doba	k1gFnSc6
Epikurovy	Epikurův	k2eAgFnSc2d1
smrti	smrt	k1gFnSc2
byl	být	k5eAaImAgMnS
Metrodóros	Metrodórosa	k1gFnPc2
již	již	k6eAd1
mrtev	mrtev	k2eAgInSc1d1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
se	se	k3xPyFc4
Epikurovým	Epikurův	k2eAgMnSc7d1
nástupcem	nástupce	k1gMnSc7
ve	v	k7c6
vedení	vedení	k1gNnSc6
školy	škola	k1gFnSc2
stal	stát	k5eAaPmAgInS
Hermarchos	Hermarchos	k1gInSc1
z	z	k7c2
Mytilény	Mytiléna	k1gFnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
pracích	práce	k1gFnPc6
polemizoval	polemizovat	k5eAaImAgInS
s	s	k7c7
učením	učení	k1gNnSc7
Pythagora	Pythagoras	k1gMnSc2
<g/>
,	,	kIx,
Platóna	Platón	k1gMnSc2
<g/>
,	,	kIx,
Aristotela	Aristoteles	k1gMnSc2
a	a	k8xC
Empedokla	Empedokla	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
Hermarchovi	Hermarch	k1gMnSc6
se	se	k3xPyFc4
v	v	k7c6
čele	čelo	k1gNnSc6
školy	škola	k1gFnSc2
vystřídalo	vystřídat	k5eAaPmAgNnS
několik	několik	k4yIc1
myslitelů	myslitel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velkého	velký	k2eAgInSc2d1
rozkvětu	rozkvět	k1gInSc2
a	a	k8xC
vysokého	vysoký	k2eAgInSc2d1
počtu	počet	k1gInSc2
žáků	žák	k1gMnPc2
dosáhla	dosáhnout	k5eAaPmAgFnS
škola	škola	k1gFnSc1
za	za	k7c4
řízení	řízení	k1gNnSc4
Apollodóra	Apollodóro	k1gNnSc2
v	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
polovině	polovina	k1gFnSc6
2	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
před	před	k7c7
n.	n.	k?
l.	l.	k?
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Epikureismus	epikureismus	k1gInSc1
se	se	k3xPyFc4
šířil	šířit	k5eAaImAgInS
po	po	k7c6
celé	celý	k2eAgFnSc6d1
středozemní	středozemní	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
a	a	k8xC
záhy	záhy	k6eAd1
se	se	k3xPyFc4
ujal	ujmout	k5eAaPmAgInS
v	v	k7c6
Antiochii	Antiochie	k1gFnSc6
a	a	k8xC
v	v	k7c6
Alexandrii	Alexandrie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
1	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
před	před	k7c7
n.	n.	k?
l.	l.	k?
stál	stát	k5eAaImAgInS
v	v	k7c6
čele	čelo	k1gNnSc6
školy	škola	k1gFnSc2
Zenón	Zenón	k1gMnSc1
ze	z	k7c2
Sidonu	Sidon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
žákem	žák	k1gMnSc7
byl	být	k5eAaImAgMnS
Filodémos	Filodémos	k1gMnSc1
z	z	k7c2
Gadar	Gadara	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
později	pozdě	k6eAd2
přesídlil	přesídlit	k5eAaPmAgInS
do	do	k7c2
Říma	Řím	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
v	v	k7c4
r.	r.	kA
80	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
založil	založit	k5eAaPmAgMnS
v	v	k7c6
Neapoli	Neapol	k1gFnSc6
epikurejskou	epikurejský	k2eAgFnSc4d1
školu	škola	k1gFnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
měla	mít	k5eAaImAgFnS
v	v	k7c6
římském	římský	k2eAgInSc6d1
duchovním	duchovní	k2eAgInSc6d1
životě	život	k1gInSc6
značný	značný	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
později	pozdě	k6eAd2
navštěvovali	navštěvovat	k5eAaImAgMnP
např.	např.	kA
Vergilius	Vergilius	k1gMnSc1
a	a	k8xC
Horatius	Horatius	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
3	#num#	k4
<g/>
]	]	kIx)
Cicero	Cicero	k1gMnSc1
v	v	k7c6
polovině	polovina	k1gFnSc6
1	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
horliví	horlivý	k2eAgMnPc1d1
stoupenci	stoupenec	k1gMnPc1
epikureismu	epikureismus	k1gInSc2
„	„	k?
<g/>
napsali	napsat	k5eAaBmAgMnP,k5eAaPmAgMnP
mnoho	mnoho	k4c4
knih	kniha	k1gFnPc2
a	a	k8xC
rozšířili	rozšířit	k5eAaPmAgMnP
své	svůj	k3xOyFgNnSc4
učení	učení	k1gNnSc4
po	po	k7c6
celé	celý	k2eAgFnSc6d1
Itálii	Itálie	k1gFnSc6
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Latinské	latinský	k2eAgInPc4d1
spisy	spis	k1gInPc4
psali	psát	k5eAaImAgMnP
v	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
např.	např.	kA
epikurejci	epikurejec	k1gMnPc1
Gaius	Gaius	k1gMnSc1
Amafinius	Amafinius	k1gMnSc1
a	a	k8xC
Rabirius	Rabirius	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
Nejvýznamnějším	významný	k2eAgMnSc7d3
římským	římský	k2eAgMnSc7d1
epikurejcem	epikurejec	k1gMnSc7
byl	být	k5eAaImAgMnS
Lucretius	Lucretius	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
vyložil	vyložit	k5eAaPmAgMnS
Epikúrovu	Epikúrův	k2eAgFnSc4d1
nauku	nauka	k1gFnSc4
v	v	k7c6
didaktické	didaktický	k2eAgFnSc6d1
básni	báseň	k1gFnSc6
O	o	k7c6
přírodě	příroda	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
sklonku	sklonek	k1gInSc6
římské	římský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
v	v	k7c6
prvé	prvý	k4xOgFnSc6
době	doba	k1gFnSc6
císařské	císařský	k2eAgFnSc2d1
byl	být	k5eAaImAgInS
epikureismus	epikureismus	k1gInSc1
životním	životní	k2eAgNnSc7d1
krédem	krédo	k1gNnSc7
četných	četný	k2eAgMnPc2d1
vzdělanců	vzdělanec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Nacházel	nacházet	k5eAaImAgMnS
vřelé	vřelý	k2eAgNnSc4d1
přijetí	přijetí	k1gNnSc4
zejména	zejména	k9
u	u	k7c2
těch	ten	k3xDgInPc2
společenských	společenský	k2eAgInPc2d1
kruhů	kruh	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
dávaly	dávat	k5eAaImAgFnP
přednost	přednost	k1gFnSc4
životu	život	k1gInSc2
v	v	k7c6
soukromí	soukromí	k1gNnSc6
a	a	k8xC
neměly	mít	k5eNaImAgInP
zájem	zájem	k1gInSc4
na	na	k7c6
boji	boj	k1gInSc6
o	o	k7c4
politickou	politický	k2eAgFnSc4d1
moc	moc	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
Ale	ale	k8xC
měl	mít	k5eAaImAgMnS
zastánce	zastánce	k1gMnSc4
i	i	k9
u	u	k7c2
některých	některý	k3yIgFnPc2
politicky	politicky	k6eAd1
vlivných	vlivný	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
kladný	kladný	k2eAgInSc4d1
postoj	postoj	k1gInSc4
k	k	k7c3
epikureismu	epikureismus	k1gInSc3
choval	chovat	k5eAaImAgMnS
snad	snad	k9
i	i	k9
Julius	Julius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seneca	Seneca	k1gMnSc1
souhlasně	souhlasně	k6eAd1
citoval	citovat	k5eAaBmAgMnS
mnohé	mnohé	k1gNnSc4
epikurejské	epikurejský	k2eAgFnSc2d1
mravní	mravní	k2eAgFnSc2d1
poučky	poučka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
200	#num#	k4
n.	n.	k?
l.	l.	k?
byl	být	k5eAaImAgInS
epikureismus	epikureismus	k1gInSc1
hlavním	hlavní	k2eAgMnSc7d1
soupeřem	soupeř	k1gMnSc7
stoicismu	stoicismus	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gInSc1
vliv	vliv	k1gInSc1
na	na	k7c4
literaturu	literatura	k1gFnSc4
a	a	k8xC
intelektuální	intelektuální	k2eAgNnSc4d1
myšlení	myšlení	k1gNnSc4
byl	být	k5eAaImAgInS
o	o	k7c6
mnoho	mnoho	k6eAd1
slabší	slabý	k2eAgFnSc6d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovlivnil	ovlivnit	k5eAaPmAgMnS
tvorbu	tvorba	k1gFnSc4
řecky	řecky	k6eAd1
píšícího	píšící	k2eAgMnSc2d1
satirika	satirik	k1gMnSc2
Lúkiana	Lúkian	k1gMnSc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
o	o	k7c6
tom	ten	k3xDgNnSc6
svědčí	svědčit	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc1
próza	próza	k1gFnSc1
Alexandr	Alexandr	k1gMnSc1
neboli	neboli	k8xC
Lžiprorok	lžiprorok	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
druhém	druhý	k4xOgInSc6
století	století	k1gNnSc6
n.	n.	k?
l.	l.	k?
dal	dát	k5eAaPmAgMnS
v	v	k7c6
Oinoandě	Oinoanda	k1gFnSc6
v	v	k7c6
Lýkii	Lýkie	k1gFnSc6
filozof	filozof	k1gMnSc1
Díogenés	Díogenésa	k1gFnPc2
vytesat	vytesat	k5eAaPmF
obrovský	obrovský	k2eAgInSc4d1
filozofický	filozofický	k2eAgInSc4d1
nápis	nápis	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
obsahoval	obsahovat	k5eAaImAgInS
výklad	výklad	k1gInSc4
Epikúrova	Epikúrův	k2eAgNnSc2d1
učení	učení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Úpadek	úpadek	k1gInSc1
antického	antický	k2eAgInSc2d1
epikureismu	epikureismus	k1gInSc2
je	být	k5eAaImIp3nS
spojen	spojit	k5eAaPmNgInS
s	s	k7c7
nástupem	nástup	k1gInSc7
novoplatonismu	novoplatonismus	k1gInSc2
a	a	k8xC
křesťanství	křesťanství	k1gNnSc2
ve	v	k7c6
3	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
n.	n.	k?
l.	l.	k?
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Postupně	postupně	k6eAd1
zanikl	zaniknout	k5eAaPmAgInS
především	především	k9
v	v	k7c6
důsledku	důsledek	k1gInSc6
triumfu	triumf	k1gInSc2
křesťanské	křesťanský	k2eAgFnSc2d1
církve	církev	k1gFnSc2
<g/>
,	,	kIx,
pro	pro	k7c4
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
bylo	být	k5eAaImAgNnS
zcela	zcela	k6eAd1
nepřijatelné	přijatelný	k2eNgNnSc1d1
zejména	zejména	k9
epikurejské	epikurejský	k2eAgNnSc1d1
učení	učení	k1gNnSc1
o	o	k7c6
bozích	bůh	k1gMnPc6
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
důraz	důraz	k1gInSc1
na	na	k7c4
smrtelnost	smrtelnost	k1gFnSc4
lidské	lidský	k2eAgFnSc2d1
duše	duše	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
oživení	oživení	k1gNnSc3
epikureismu	epikureismus	k1gInSc2
dochází	docházet	k5eAaImIp3nS
v	v	k7c6
období	období	k1gNnSc6
renesance	renesance	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnPc1
myšlenky	myšlenka	k1gFnPc1
epikurejců	epikurejec	k1gMnPc2
ovlivnily	ovlivnit	k5eAaPmAgFnP
tvorbu	tvorba	k1gFnSc4
Ronsarda	Ronsarda	k1gFnSc1
<g/>
,	,	kIx,
du	du	k?
Bellaye	Bellay	k1gMnSc2
a	a	k8xC
Michela	Michel	k1gMnSc2
de	de	k?
Montaigne	Montaign	k1gInSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
Proti	proti	k7c3
pomlouvačným	pomlouvačný	k2eAgInPc3d1
předsudkům	předsudek	k1gInPc3
a	a	k8xC
podceňování	podceňování	k1gNnSc3
Epikúra	Epikúr	k1gMnSc2
vystoupil	vystoupit	k5eAaPmAgMnS
v	v	k7c6
polovině	polovina	k1gFnSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
francouzský	francouzský	k2eAgMnSc1d1
matematik	matematik	k1gMnSc1
<g/>
,	,	kIx,
fyzik	fyzik	k1gMnSc1
a	a	k8xC
filozof	filozof	k1gMnSc1
Pierre	Pierr	k1gInSc5
Gassendi	Gassend	k1gMnPc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
Epikúrovi	Epikúr	k1gMnSc6
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
filozofii	filozofie	k1gFnSc4
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
tři	tři	k4xCgFnPc4
knihy	kniha	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velký	velký	k2eAgInSc1d1
význam	význam	k1gInSc1
měl	mít	k5eAaImAgInS
zejména	zejména	k9
spis	spis	k1gInSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
vyšel	vyjít	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1649	#num#	k4
pod	pod	k7c7
názvem	název	k1gInSc7
Animadversiones	Animadversiones	k1gInSc4
in	in	k?
librum	librum	k1gInSc1
X	X	kA
Diogenis	Diogenis	k1gFnSc2
Laertii	Laertie	k1gFnSc3
qui	qui	k?
est	est	k?
de	de	k?
vita	vit	k2eAgFnSc1d1
<g/>
,	,	kIx,
moribus	moribus	k1gInSc1
et	et	k?
placitis	placitis	k1gFnSc1
Epicuri	Epicur	k1gFnSc2
[	[	kIx(
<g/>
Úvahy	úvaha	k1gFnPc1
o	o	k7c6
desáté	desátý	k4xOgFnSc6
knize	kniha	k1gFnSc6
Diogena	Diogenes	k1gMnSc4
Láertia	Láertius	k1gMnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c6
životě	život	k1gInSc6
<g/>
,	,	kIx,
mravech	mrav	k1gInPc6
a	a	k8xC
názorech	názor	k1gInPc6
Epikúrových	Epikúrových	k2eAgFnPc1d1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
spisu	spis	k1gInSc6
je	být	k5eAaImIp3nS
otištěna	otištěn	k2eAgFnSc1d1
10	#num#	k4
<g/>
.	.	kIx.
kniha	kniha	k1gFnSc1
antických	antický	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
filozofie	filozofie	k1gFnSc2
od	od	k7c2
Diogena	Diogenes	k1gMnSc2
Laertia	Laertius	k1gMnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
cele	cele	k6eAd1
věnována	věnovat	k5eAaImNgFnS,k5eAaPmNgFnS
Epikúrovi	Epikúr	k1gMnSc3
a	a	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
nejdůležitější	důležitý	k2eAgInPc4d3
údaje	údaj	k1gInPc4
o	o	k7c6
jeho	jeho	k3xOp3gInSc6
životě	život	k1gInSc6
a	a	k8xC
filozofii	filozofie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
řeckému	řecký	k2eAgInSc3d1
originálu	originál	k1gInSc3
Gassendi	Gassend	k1gMnPc1
připojil	připojit	k5eAaPmAgInS
i	i	k9
překlad	překlad	k1gInSc1
do	do	k7c2
latiny	latina	k1gFnSc2
a	a	k8xC
obsáhlý	obsáhlý	k2eAgInSc4d1
komentář	komentář	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Byly	být	k5eAaImAgInP
to	ten	k3xDgNnSc1
právě	právě	k9
Gassendiho	Gassendi	k1gMnSc4
knihy	kniha	k1gFnSc2
o	o	k7c6
Epikúrově	Epikúrův	k2eAgFnSc6d1
filozofii	filozofie	k1gFnSc6
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
Isaac	Isaac	k1gInSc1
Newton	Newton	k1gMnSc1
poznal	poznat	k5eAaPmAgMnS
antické	antický	k2eAgNnSc4d1
učení	učení	k1gNnSc4
o	o	k7c6
atomech	atom	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
díla	dílo	k1gNnSc2
Newtonova	Newtonův	k2eAgFnSc1d1
se	se	k3xPyFc4
pak	pak	k6eAd1
s	s	k7c7
atomismem	atomismus	k1gInSc7
seznámil	seznámit	k5eAaPmAgMnS
John	John	k1gMnSc1
Dalton	Dalton	k1gInSc4
<g/>
,	,	kIx,
jehož	jenž	k3xRgNnSc4,k3xOyRp3gNnSc4
práce	práce	k1gFnPc1
přispěly	přispět	k5eAaPmAgFnP
k	k	k7c3
prosazení	prosazení	k1gNnSc3
teorie	teorie	k1gFnSc2
o	o	k7c6
atomech	atom	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
Epikurovy	Epikurův	k2eAgFnSc2d1
etiky	etika	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
dovolával	dovolávat	k5eAaImAgMnS
John	John	k1gMnSc1
Stuart	Stuarta	k1gFnPc2
Mill	Mill	k1gMnSc1
<g/>
,	,	kIx,
když	když	k8xS
formuloval	formulovat	k5eAaImAgMnS
svou	svůj	k3xOyFgFnSc4
morálku	morálka	k1gFnSc4
štěstí	štěstí	k1gNnSc2
(	(	kIx(
<g/>
utilitarismus	utilitarismus	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Představitelé	představitel	k1gMnPc1
</s>
<s>
Titus	Titus	k1gMnSc1
Lucretius	Lucretius	k1gMnSc1
Carus	Carus	k1gMnSc1
</s>
<s>
Quintus	Quintus	k1gMnSc1
Horatius	Horatius	k1gMnSc1
Flaccus	Flaccus	k1gMnSc1
</s>
<s>
Metrodóros	Metrodórosa	k1gFnPc2
z	z	k7c2
Lampsaku	Lampsak	k1gInSc2
(	(	kIx(
<g/>
mladší	mladý	k2eAgMnSc1d2
<g/>
)	)	kIx)
</s>
<s>
Zenon	Zenon	k1gInSc1
Sidónský	Sidónský	k2eAgInSc1d1
</s>
<s>
Filodemos	Filodemos	k1gInSc1
z	z	k7c2
Gadary	Gadara	k1gFnSc2
</s>
<s>
Díogenés	Díogenés	k6eAd1
z	z	k7c2
Oinoandy	Oinoanda	k1gFnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Epikúros	Epikúrosa	k1gFnPc2
částečně	částečně	k6eAd1
navazoval	navazovat	k5eAaImAgInS
na	na	k7c4
hédonismus	hédonismus	k1gInSc4
Sokratova	Sokratův	k2eAgMnSc2d1
žáka	žák	k1gMnSc2
Aristippa	Aristipp	k1gMnSc2
z	z	k7c2
Kyrény	Kyréna	k1gFnSc2
(	(	kIx(
<g/>
zakladatele	zakladatel	k1gMnSc2
kyrénské	kyrénský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
považoval	považovat	k5eAaImAgMnS
za	za	k7c4
cíl	cíl	k1gInSc4
života	život	k1gInSc2
dosahování	dosahování	k1gNnSc2
slasti	slast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Aristippa	Aristipp	k1gMnSc2
je	být	k5eAaImIp3nS
šťastný	šťastný	k2eAgInSc4d1
ten	ten	k3xDgMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
zakouší	zakoušet	k5eAaImIp3nS
nějakou	nějaký	k3yIgFnSc4
pozitivní	pozitivní	k2eAgFnSc4d1
slast	slast	k1gFnSc4
<g/>
,	,	kIx,
požitek	požitek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Epikúros	Epikúrosa	k1gFnPc2
však	však	k9
pojem	pojem	k1gInSc1
slasti	slast	k1gFnSc2
rozšířil	rozšířit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jeho	jeho	k3xOp3gNnSc6
pojetí	pojetí	k1gNnSc6
stačí	stačit	k5eAaBmIp3nS
k	k	k7c3
dosažení	dosažení	k1gNnSc3
blaženosti	blaženost	k1gFnSc2
nepřítomnost	nepřítomnost	k1gFnSc1
utrpení	utrpení	k1gNnSc4
<g/>
,	,	kIx,
nepřítomnost	nepřítomnost	k1gFnSc4
bolesti	bolest	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klid	klid	k1gInSc1
duše	duše	k1gFnSc2
a	a	k8xC
bezbolestné	bezbolestný	k2eAgNnSc1d1
tělo	tělo	k1gNnSc1
už	už	k6eAd1
samy	sám	k3xTgInPc1
o	o	k7c6
sobě	sebe	k3xPyFc6
naplňují	naplňovat	k5eAaImIp3nP
člověka	člověk	k1gMnSc4
pocitem	pocit	k1gInSc7
štěstí	štěstí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Epikúros	Epikúrosa	k1gFnPc2
měl	mít	k5eAaImAgInS
kritický	kritický	k2eAgInSc1d1
a	a	k8xC
skeptický	skeptický	k2eAgInSc1d1
vztah	vztah	k1gInSc1
k	k	k7c3
nevědomému	nevědomé	k1gNnSc3
davu	dav	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seneca	Seneca	k1gMnSc1
cituje	citovat	k5eAaBmIp3nS
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
výrok	výrok	k1gInSc4
<g/>
:	:	kIx,
„	„	k?
<g/>
Nikdy	nikdy	k6eAd1
jsem	být	k5eAaImIp1nS
se	se	k3xPyFc4
nechtěl	chtít	k5eNaImAgMnS
líbit	líbit	k5eAaImF
lidu	lid	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vždyť	vždyť	k8xC
co	co	k9
já	já	k3xPp1nSc1
dovedu	dovést	k5eAaPmIp1nS
<g/>
,	,	kIx,
lid	lid	k1gInSc1
neschvaluje	schvalovat	k5eNaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
lid	lid	k1gInSc1
schvaluje	schvalovat	k5eAaImIp3nS
<g/>
,	,	kIx,
nedovedu	dovést	k5eNaPmIp1nS
já	já	k3xPp1nSc1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
V	v	k7c6
Horatiově	Horatiův	k2eAgFnSc6d1
tvorbě	tvorba	k1gFnSc6
se	se	k3xPyFc4
projevuje	projevovat	k5eAaImIp3nS
snaha	snaha	k1gFnSc1
po	po	k7c6
hledání	hledání	k1gNnSc6
rozumného	rozumný	k2eAgInSc2d1
životního	životní	k2eAgInSc2d1
stylu	styl	k1gInSc2
v	v	k7c6
duchu	duch	k1gMnSc6
umírněného	umírněný	k2eAgInSc2d1
epikureismu	epikureismus	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
41	#num#	k4
<g/>
]	]	kIx)
Básník	básník	k1gMnSc1
se	se	k3xPyFc4
vyslovuje	vyslovovat	k5eAaImIp3nS
proti	proti	k7c3
vybičovaným	vybičovaný	k2eAgMnPc3d1
vášním	vášnit	k5eAaImIp1nS
a	a	k8xC
vypjaté	vypjatý	k2eAgFnSc3d1
rozkoši	rozkoš	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
básnické	básnický	k2eAgFnSc6d1
sbírce	sbírka	k1gFnSc6
Listy	lista	k1gFnSc2
nabádá	nabádat	k5eAaImIp3nS,k5eAaBmIp3nS
svého	svůj	k3xOyFgMnSc4
přítele	přítel	k1gMnSc4
Albia	Albius	k1gMnSc4
Tibulla	Tibull	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
podle	podle	k7c2
zásady	zásada	k1gFnSc2
epikurejské	epikurejský	k2eAgFnSc2d1
užíval	užívat	k5eAaImAgInS
života	život	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
báseň	báseň	k1gFnSc1
žertovně	žertovně	k6eAd1
zakončuje	zakončovat	k5eAaImIp3nS
veršem	verš	k1gInSc7
<g/>
:	:	kIx,
<g/>
„	„	k?
<g/>
Navštiv	navštívit	k5eAaPmRp2nS
mě	já	k3xPp1nSc2
<g/>
,	,	kIx,
chceš	chtít	k5eAaImIp2nS
<g/>
-li	-li	k?
se	se	k3xPyFc4
zasmát	zasmát	k5eAaPmF
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
pěstuji	pěstovat	k5eAaImIp1nS
tělo	tělo	k1gNnSc4
a	a	k8xC
zářímtukem	zářímtuk	k1gInSc7
jak	jak	k8xC,k8xS
prasátko	prasátko	k1gNnSc1
asi	asi	k9
<g/>
,	,	kIx,
však	však	k9
ze	z	k7c2
stáda	stádo	k1gNnSc2
Epikúrova	Epikúrov	k1gInSc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
GROH	GROH	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řečtí	řecký	k2eAgMnPc1d1
filosofové	filosof	k1gMnPc1
a	a	k8xC
mystici	mystik	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
Nákladem	náklad	k1gInSc7
Společnosti	společnost	k1gFnSc2
přátel	přítel	k1gMnPc2
antické	antický	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
.	.	kIx.
174	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
669181	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Kapitola	kapitola	k1gFnSc1
„	„	k?
<g/>
Filosofie	filosofie	k1gFnSc1
v	v	k7c6
období	období	k1gNnSc6
hellenistickém	hellenistický	k2eAgNnSc6d1
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
101	#num#	k4
<g/>
–	–	k?
<g/>
124	#num#	k4
<g/>
;	;	kIx,
autor	autor	k1gMnSc1
Otakar	Otakar	k1gMnSc1
Jiráni	Jirán	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
]	]	kIx)
S.	S.	kA
112	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
DUROZOI	DUROZOI	kA
<g/>
,	,	kIx,
Gérard	Gérard	k1gMnSc1
a	a	k8xC
ROUSSEL	ROUSSEL	kA
<g/>
,	,	kIx,
André	André	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filozofický	filozofický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
EWA	EWA	kA
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
,	,	kIx,
©	©	k?
<g/>
1994	#num#	k4
<g/>
.	.	kIx.
352	#num#	k4
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85764	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
70	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BONDY	bond	k1gInPc7
<g/>
,	,	kIx,
Egon	Egon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antická	antický	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Sdružení	sdružení	k1gNnSc1
na	na	k7c4
podporu	podpora	k1gFnSc4
vydávání	vydávání	k1gNnSc2
časopisů	časopis	k1gInPc2
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
190	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85239	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
125	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GROH	GROH	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řečtí	řecký	k2eAgMnPc1d1
filosofové	filosof	k1gMnPc1
a	a	k8xC
mystici	mystik	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
Nákladem	náklad	k1gInSc7
Společnosti	společnost	k1gFnSc2
přátel	přítel	k1gMnPc2
antické	antický	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
.	.	kIx.
174	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
669181	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Kapitola	kapitola	k1gFnSc1
„	„	k?
<g/>
Filosofie	filosofie	k1gFnSc1
v	v	k7c6
období	období	k1gNnSc6
hellenistickém	hellenistický	k2eAgNnSc6d1
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
101	#num#	k4
<g/>
–	–	k?
<g/>
124	#num#	k4
<g/>
;	;	kIx,
autor	autor	k1gMnSc1
Otakar	Otakar	k1gMnSc1
Jiráni	Jirán	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
]	]	kIx)
S.	S.	kA
113	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GROH	GROH	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řečtí	řecký	k2eAgMnPc1d1
filosofové	filosof	k1gMnPc1
a	a	k8xC
mystici	mystik	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
Nákladem	náklad	k1gInSc7
Společnosti	společnost	k1gFnSc2
přátel	přítel	k1gMnPc2
antické	antický	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
.	.	kIx.
174	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
669181	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Kapitola	kapitola	k1gFnSc1
„	„	k?
<g/>
Filosofie	filosofie	k1gFnSc1
v	v	k7c6
období	období	k1gNnSc6
hellenistickém	hellenistický	k2eAgNnSc6d1
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
101	#num#	k4
<g/>
–	–	k?
<g/>
124	#num#	k4
<g/>
;	;	kIx,
autor	autor	k1gMnSc1
Otakar	Otakar	k1gMnSc1
Jiráni	Jirán	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
]	]	kIx)
S.	S.	kA
114	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BONDY	bond	k1gInPc7
<g/>
,	,	kIx,
Egon	Egon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antická	antický	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Sdružení	sdružení	k1gNnSc1
na	na	k7c4
podporu	podpora	k1gFnSc4
vydávání	vydávání	k1gNnSc2
časopisů	časopis	k1gInPc2
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
190	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85239	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
125	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GROH	GROH	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řečtí	řecký	k2eAgMnPc1d1
filosofové	filosof	k1gMnPc1
a	a	k8xC
mystici	mystik	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
Nákladem	náklad	k1gInSc7
Společnosti	společnost	k1gFnSc2
přátel	přítel	k1gMnPc2
antické	antický	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
.	.	kIx.
174	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
669181	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Kapitola	kapitola	k1gFnSc1
„	„	k?
<g/>
Filosofie	filosofie	k1gFnSc1
v	v	k7c6
období	období	k1gNnSc6
hellenistickém	hellenistický	k2eAgNnSc6d1
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
101	#num#	k4
<g/>
–	–	k?
<g/>
124	#num#	k4
<g/>
;	;	kIx,
autor	autor	k1gMnSc1
Otakar	Otakar	k1gMnSc1
Jiráni	Jirán	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
]	]	kIx)
S.	S.	kA
114	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
DUROZOI	DUROZOI	kA
<g/>
,	,	kIx,
Gérard	Gérard	k1gMnSc1
a	a	k8xC
ROUSSEL	ROUSSEL	kA
<g/>
,	,	kIx,
André	André	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filozofický	filozofický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
EWA	EWA	kA
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
,	,	kIx,
©	©	k?
<g/>
1994	#num#	k4
<g/>
.	.	kIx.
352	#num#	k4
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85764	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
71	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Díogenés	Díogenés	k1gInSc1
Laertios	Laertios	k1gInSc1
<g/>
:	:	kIx,
Životy	život	k1gInPc1
<g/>
,	,	kIx,
názory	názor	k1gInPc1
a	a	k8xC
výroky	výrok	k1gInPc1
proslulých	proslulý	k2eAgMnPc2d1
filosofů	filosof	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Antonín	Antonín	k1gMnSc1
Kolář	Kolář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pelhřimov	Pelhřimov	k1gInSc1
<g/>
:	:	kIx,
Nová	nový	k2eAgFnSc1d1
tiskárna	tiskárna	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
473	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
901916	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Citovaný	citovaný	k2eAgInSc1d1
text	text	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
419	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Díogenés	Díogenés	k1gInSc1
Laertios	Laertios	k1gInSc1
<g/>
:	:	kIx,
Životy	život	k1gInPc1
<g/>
,	,	kIx,
názory	názor	k1gInPc1
a	a	k8xC
výroky	výrok	k1gInPc1
proslulých	proslulý	k2eAgMnPc2d1
filosofů	filosof	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Antonín	Antonín	k1gMnSc1
Kolář	Kolář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pelhřimov	Pelhřimov	k1gInSc1
<g/>
:	:	kIx,
Nová	nový	k2eAgFnSc1d1
tiskárna	tiskárna	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
473	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
901916	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
420	#num#	k4
a	a	k8xC
422	#num#	k4
<g/>
–	–	k?
<g/>
423	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GROH	GROH	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řečtí	řecký	k2eAgMnPc1d1
filosofové	filosof	k1gMnPc1
a	a	k8xC
mystici	mystik	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
Nákladem	náklad	k1gInSc7
Společnosti	společnost	k1gFnSc2
přátel	přítel	k1gMnPc2
antické	antický	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
.	.	kIx.
174	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
669181	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Kapitola	kapitola	k1gFnSc1
„	„	k?
<g/>
Filosofie	filosofie	k1gFnSc1
v	v	k7c6
období	období	k1gNnSc6
hellenistickém	hellenistický	k2eAgNnSc6d1
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
101	#num#	k4
<g/>
–	–	k?
<g/>
124	#num#	k4
<g/>
;	;	kIx,
autor	autor	k1gMnSc1
Otakar	Otakar	k1gMnSc1
Jiráni	Jirán	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
]	]	kIx)
S.	S.	kA
114	#num#	k4
<g/>
–	–	k?
<g/>
115	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
TRETERA	TRETERA	kA
<g/>
,	,	kIx,
Ivo	Ivo	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
filozofie	filozofie	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgNnSc1d1
pedagogické	pedagogický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
.	.	kIx.
139	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2929456	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
69	#num#	k4
a	a	k8xC
100	#num#	k4
<g/>
–	–	k?
<g/>
101	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Platón	platón	k1gInSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Píši	psát	k5eAaImIp1nS
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
příteli	přítel	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydání	vydání	k1gNnSc1
první	první	k4xOgFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
.	.	kIx.
400	#num#	k4
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antická	antický	k2eAgFnSc1d1
próza	próza	k1gFnSc1
<g/>
;	;	kIx,
svazek	svazek	k1gInSc1
šestý	šestý	k4xOgInSc1
<g/>
.	.	kIx.
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
154418	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Epikúrův	Epikúrův	k2eAgInSc1d1
dopis	dopis	k1gInSc1
Menoikeovi	Menoikeus	k1gMnSc3
v	v	k7c6
překladu	překlad	k1gInSc6
Bořivoje	Bořivoj	k1gMnSc2
Boreckého	borecký	k2eAgInSc2d1
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
27	#num#	k4
<g/>
–	–	k?
<g/>
31	#num#	k4
<g/>
;	;	kIx,
citovaný	citovaný	k2eAgInSc1d1
text	text	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
30	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
TRETERA	TRETERA	kA
<g/>
,	,	kIx,
Ivo	Ivo	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
filozofie	filozofie	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgNnSc1d1
pedagogické	pedagogický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
.	.	kIx.
139	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2929456	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
101	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Diogenés	Diogenés	k1gInSc1
Laertios	Laertios	k1gMnSc1
a	a	k8xC
Ludvíkovský	ludvíkovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc4
a	a	k8xC
učení	učení	k1gNnSc4
filosofa	filosof	k1gMnSc2
Epikura	Epikur	k1gMnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Rovnost	rovnost	k1gFnSc1
<g/>
,	,	kIx,
1952	#num#	k4
<g/>
.	.	kIx.
117	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
507093	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Z	z	k7c2
kapitoly	kapitola	k1gFnSc2
„	„	k?
<g/>
Jiné	jiný	k2eAgInPc4d1
zlomky	zlomek	k1gInPc4
etické	etický	k2eAgInPc4d1
<g/>
"	"	kIx"
<g/>
;	;	kIx,
citovaný	citovaný	k2eAgInSc1d1
text	text	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
82	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Diogenés	Diogenés	k1gInSc1
Laertios	Laertios	k1gMnSc1
a	a	k8xC
Ludvíkovský	ludvíkovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc4
a	a	k8xC
učení	učení	k1gNnSc4
filosofa	filosof	k1gMnSc2
Epikura	Epikur	k1gMnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Rovnost	rovnost	k1gFnSc1
<g/>
,	,	kIx,
1952	#num#	k4
<g/>
.	.	kIx.
117	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
507093	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Z	z	k7c2
kapitoly	kapitola	k1gFnSc2
„	„	k?
<g/>
Výroky	výrok	k1gInPc1
Epikura	Epikur	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gMnPc2
žáků	žák	k1gMnPc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
gnomologium	gnomologium	k1gNnSc1
vatikánské	vatikánský	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
<g/>
;	;	kIx,
citovaný	citovaný	k2eAgInSc1d1
text	text	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
76	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
↑	↑	k?
DUROZOI	DUROZOI	kA
<g/>
,	,	kIx,
Gérard	Gérard	k1gMnSc1
a	a	k8xC
ROUSSEL	ROUSSEL	kA
<g/>
,	,	kIx,
André	André	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filozofický	filozofický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
EWA	EWA	kA
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
,	,	kIx,
©	©	k?
<g/>
1994	#num#	k4
<g/>
.	.	kIx.
352	#num#	k4
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85764	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
71	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Platón	platón	k1gInSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Píši	psát	k5eAaImIp1nS
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
příteli	přítel	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydání	vydání	k1gNnSc1
první	první	k4xOgFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
.	.	kIx.
400	#num#	k4
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antická	antický	k2eAgFnSc1d1
próza	próza	k1gFnSc1
<g/>
;	;	kIx,
svazek	svazek	k1gInSc1
šestý	šestý	k4xOgInSc1
<g/>
.	.	kIx.
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
154418	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Epikúrův	Epikúrův	k2eAgInSc1d1
dopis	dopis	k1gInSc1
Menoikeovi	Menoikeus	k1gMnSc3
v	v	k7c6
překladu	překlad	k1gInSc6
Bořivoje	Bořivoj	k1gMnSc2
Boreckého	borecký	k2eAgInSc2d1
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
27	#num#	k4
<g/>
–	–	k?
<g/>
31	#num#	k4
<g/>
;	;	kIx,
citovaný	citovaný	k2eAgInSc1d1
text	text	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
29	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Diogenés	Diogenés	k1gInSc1
Laertios	Laertios	k1gMnSc1
a	a	k8xC
Ludvíkovský	ludvíkovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc4
a	a	k8xC
učení	učení	k1gNnSc4
filosofa	filosof	k1gMnSc2
Epikura	Epikur	k1gMnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Rovnost	rovnost	k1gFnSc1
<g/>
,	,	kIx,
1952	#num#	k4
<g/>
.	.	kIx.
117	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
507093	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Z	z	k7c2
kapitoly	kapitola	k1gFnSc2
„	„	k?
<g/>
Výroky	výrok	k1gInPc1
Epikura	Epikur	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gMnPc2
žáků	žák	k1gMnPc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
gnomologium	gnomologium	k1gNnSc1
vatikánské	vatikánský	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
<g/>
;	;	kIx,
citovaný	citovaný	k2eAgInSc1d1
text	text	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
80	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
↑	↑	k?
„	„	k?
<g/>
Prožij	prožít	k5eAaPmRp2nS
svůj	svůj	k3xOyFgInSc4
život	život	k1gInSc4
v	v	k7c6
skrytu	skryt	k1gInSc6
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
→	→	k?
Diogenés	Diogenés	k1gInSc1
Laertios	Laertios	k1gMnSc1
a	a	k8xC
Ludvíkovský	ludvíkovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc4
a	a	k8xC
učení	učení	k1gNnSc4
filosofa	filosof	k1gMnSc2
Epikura	Epikur	k1gMnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Rovnost	rovnost	k1gFnSc1
<g/>
,	,	kIx,
1952	#num#	k4
<g/>
.	.	kIx.
117	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
507093	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Z	z	k7c2
kapitoly	kapitola	k1gFnSc2
„	„	k?
<g/>
Jiné	jiný	k2eAgInPc4d1
zlomky	zlomek	k1gInPc4
etické	etický	k2eAgInPc4d1
<g/>
"	"	kIx"
<g/>
;	;	kIx,
citovaný	citovaný	k2eAgInSc1d1
text	text	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
84	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
↑	↑	k?
GROH	GROH	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řečtí	řecký	k2eAgMnPc1d1
filosofové	filosof	k1gMnPc1
a	a	k8xC
mystici	mystik	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
Nákladem	náklad	k1gInSc7
Společnosti	společnost	k1gFnSc2
přátel	přítel	k1gMnPc2
antické	antický	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
.	.	kIx.
174	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
669181	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Kapitola	kapitola	k1gFnSc1
„	„	k?
<g/>
Filosofie	filosofie	k1gFnSc1
v	v	k7c6
období	období	k1gNnSc6
hellenistickém	hellenistický	k2eAgNnSc6d1
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
101	#num#	k4
<g/>
–	–	k?
<g/>
124	#num#	k4
<g/>
;	;	kIx,
autor	autor	k1gMnSc1
Otakar	Otakar	k1gMnSc1
Jiráni	Jirán	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
]	]	kIx)
S.	S.	kA
115	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Diogenés	Diogenés	k1gInSc1
Laertios	Laertios	k1gMnSc1
a	a	k8xC
Ludvíkovský	ludvíkovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc4
a	a	k8xC
učení	učení	k1gNnSc4
filosofa	filosof	k1gMnSc2
Epikura	Epikur	k1gMnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Rovnost	rovnost	k1gFnSc1
<g/>
,	,	kIx,
1952	#num#	k4
<g/>
.	.	kIx.
117	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
507093	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Z	z	k7c2
kapitoly	kapitola	k1gFnSc2
„	„	k?
<g/>
Výroky	výrok	k1gInPc1
Epikura	Epikur	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gMnPc2
žáků	žák	k1gMnPc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
gnomologium	gnomologium	k1gNnSc1
vatikánské	vatikánský	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
<g/>
;	;	kIx,
citovaný	citovaný	k2eAgInSc1d1
text	text	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
79	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Díogenés	Díogenés	k1gInSc1
Laertios	Laertios	k1gInSc1
<g/>
:	:	kIx,
Životy	život	k1gInPc1
<g/>
,	,	kIx,
názory	názor	k1gInPc1
a	a	k8xC
výroky	výrok	k1gInPc1
proslulých	proslulý	k2eAgMnPc2d1
filosofů	filosof	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Antonín	Antonín	k1gMnSc1
Kolář	Kolář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pelhřimov	Pelhřimov	k1gInSc1
<g/>
:	:	kIx,
Nová	nový	k2eAgFnSc1d1
tiskárna	tiskárna	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
473	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
901916	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Citovaný	citovaný	k2eAgInSc1d1
text	text	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
425	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
Seneca	Seneca	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výbor	výbor	k1gInSc1
z	z	k7c2
listů	list	k1gInPc2
Luciliovi	Luciliův	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Bohumil	Bohumil	k1gMnSc1
Ryba	Ryba	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
.	.	kIx.
282	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
145619	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Citovaný	citovaný	k2eAgInSc1d1
text	text	k1gInSc1
je	být	k5eAaImIp3nS
uveden	uvést	k5eAaPmNgInS
v	v	k7c6
29	#num#	k4
<g/>
.	.	kIx.
listu	list	k1gInSc2
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
46	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
↑	↑	k?
GROH	GROH	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řečtí	řecký	k2eAgMnPc1d1
filosofové	filosof	k1gMnPc1
a	a	k8xC
mystici	mystik	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
Nákladem	náklad	k1gInSc7
Společnosti	společnost	k1gFnSc2
přátel	přítel	k1gMnPc2
antické	antický	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
.	.	kIx.
174	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
669181	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Kapitola	kapitola	k1gFnSc1
„	„	k?
<g/>
Filosofie	filosofie	k1gFnSc1
v	v	k7c6
období	období	k1gNnSc6
hellenistickém	hellenistický	k2eAgNnSc6d1
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
101	#num#	k4
<g/>
–	–	k?
<g/>
124	#num#	k4
<g/>
;	;	kIx,
autor	autor	k1gMnSc1
Otakar	Otakar	k1gMnSc1
Jiráni	Jirán	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
]	]	kIx)
S.	S.	kA
115	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Díogenés	Díogenés	k1gInSc1
Laertios	Laertios	k1gInSc1
<g/>
:	:	kIx,
Životy	život	k1gInPc1
<g/>
,	,	kIx,
názory	názor	k1gInPc1
a	a	k8xC
výroky	výrok	k1gInPc1
proslulých	proslulý	k2eAgMnPc2d1
filosofů	filosof	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Antonín	Antonín	k1gMnSc1
Kolář	Kolář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pelhřimov	Pelhřimov	k1gInSc1
<g/>
:	:	kIx,
Nová	nový	k2eAgFnSc1d1
tiskárna	tiskárna	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
473	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
901916	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Citovaný	citovaný	k2eAgInSc1d1
text	text	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
417	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
↑	↑	k?
„	„	k?
<g/>
Dokonalé	dokonalý	k2eAgNnSc4d1
přátelství	přátelství	k1gNnSc4
je	být	k5eAaImIp3nS
žádoucí	žádoucí	k2eAgMnSc1d1
samo	sám	k3xTgNnSc1
o	o	k7c6
sobě	sebe	k3xPyFc6
<g/>
,	,	kIx,
počáteční	počáteční	k2eAgMnSc1d1
jeho	jeho	k3xOp3gFnSc7
pohnutkou	pohnutka	k1gFnSc7
je	být	k5eAaImIp3nS
však	však	k9
prospěch	prospěch	k1gInSc4
<g/>
.	.	kIx.
<g/>
"	"	kIx"
→	→	k?
</s>
<s>
Diogenés	Diogenés	k6eAd1
Laertios	Laertios	k1gMnSc1
a	a	k8xC
Ludvíkovský	ludvíkovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc4
a	a	k8xC
učení	učení	k1gNnSc4
filosofa	filosof	k1gMnSc2
Epikura	Epikur	k1gMnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Rovnost	rovnost	k1gFnSc1
<g/>
,	,	kIx,
1952	#num#	k4
<g/>
.	.	kIx.
117	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
507093	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Z	z	k7c2
kapitoly	kapitola	k1gFnSc2
„	„	k?
<g/>
Výroky	výrok	k1gInPc1
Epikura	Epikur	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gMnPc2
žáků	žák	k1gMnPc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
gnomologium	gnomologium	k1gNnSc1
vatikánské	vatikánský	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
"	"	kIx"
<g/>
;	;	kIx,
citovaný	citovaný	k2eAgInSc1d1
text	text	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
76	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
↑	↑	k?
GROH	GROH	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řečtí	řecký	k2eAgMnPc1d1
filosofové	filosof	k1gMnPc1
a	a	k8xC
mystici	mystik	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
Nákladem	náklad	k1gInSc7
Společnosti	společnost	k1gFnSc2
přátel	přítel	k1gMnPc2
antické	antický	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
.	.	kIx.
174	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
669181	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Kapitola	kapitola	k1gFnSc1
„	„	k?
<g/>
Filosofie	filosofie	k1gFnSc1
v	v	k7c6
období	období	k1gNnSc6
hellenistickém	hellenistický	k2eAgNnSc6d1
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
101	#num#	k4
<g/>
–	–	k?
<g/>
124	#num#	k4
<g/>
;	;	kIx,
autor	autor	k1gMnSc1
Otakar	Otakar	k1gMnSc1
Jiráni	Jirán	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
]	]	kIx)
S.	S.	kA
115	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Diogenés	Diogenés	k1gInSc1
Laertios	Laertios	k1gMnSc1
a	a	k8xC
Ludvíkovský	ludvíkovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc4
a	a	k8xC
učení	učení	k1gNnSc4
filosofa	filosof	k1gMnSc2
Epikura	Epikur	k1gMnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Rovnost	rovnost	k1gFnSc1
<g/>
,	,	kIx,
1952	#num#	k4
<g/>
.	.	kIx.
117	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
507093	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Citovaný	citovaný	k2eAgInSc1d1
text	text	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
70	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
↑	↑	k?
LEGOWICZ	LEGOWICZ	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prehľad	Prehľad	k1gInSc1
dejín	dejín	k1gInSc4
filozofie	filozofie	k1gFnSc2
<g/>
:	:	kIx,
základy	základ	k1gInPc1
doxografie	doxografie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhé	druhý	k4xOgFnPc1
vydanie	vydanie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
Obzor	obzor	k1gInSc1
<g/>
,	,	kIx,
1973	#num#	k4
<g/>
.	.	kIx.
655	#num#	k4
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
111	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SVOBODA	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
Ludvík	Ludvík	k1gMnSc1
ed	ed	k?
<g/>
.	.	kIx.
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
antiky	antika	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1973	#num#	k4
<g/>
.	.	kIx.
744	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
127564	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
172	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Seneca	Seneca	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výbor	výbor	k1gInSc1
z	z	k7c2
listů	list	k1gInPc2
Luciliovi	Luciliův	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Bohumil	Bohumil	k1gMnSc1
Ryba	Ryba	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
.	.	kIx.
282	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
145619	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Z	z	k7c2
21	#num#	k4
<g/>
.	.	kIx.
listu	list	k1gInSc2
<g/>
;	;	kIx,
citovaný	citovaný	k2eAgInSc1d1
text	text	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
33	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
↑	↑	k?
LONG	LONG	kA
<g/>
,	,	kIx,
A.	A.	kA
A.	A.	kA
Hellénistická	Hellénistický	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
<g/>
:	:	kIx,
stoikové	stoik	k1gMnPc1
<g/>
,	,	kIx,
epikurejci	epikurejec	k1gMnPc1
<g/>
,	,	kIx,
skeptikové	skeptik	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Petr	Petr	k1gMnSc1
Kolev	Kolev	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
OIKOYMENH	OIKOYMENH	kA
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
341	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7298	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
77	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
30	#num#	k4
<g/>
–	–	k?
<g/>
31	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ŚWIDERKOVÁ	ŚWIDERKOVÁ	kA
<g/>
,	,	kIx,
Anna	Anna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvář	tvář	k1gFnSc1
helénistického	helénistický	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
:	:	kIx,
od	od	k7c2
Alexandra	Alexandr	k1gMnSc2
Velikého	veliký	k2eAgMnSc2d1
do	do	k7c2
císaře	císař	k1gMnSc4
Augusta	August	k1gMnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Panorama	panorama	k1gNnSc1
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
.	.	kIx.
401	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
7874	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
54	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Díogenés	Díogenés	k1gInSc1
Laertios	Laertios	k1gInSc1
<g/>
:	:	kIx,
Životy	život	k1gInPc1
<g/>
,	,	kIx,
názory	názor	k1gInPc1
a	a	k8xC
výroky	výrok	k1gInPc1
proslulých	proslulý	k2eAgMnPc2d1
filosofů	filosof	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Antonín	Antonín	k1gMnSc1
Kolář	Kolář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pelhřimov	Pelhřimov	k1gInSc1
<g/>
:	:	kIx,
Nová	nový	k2eAgFnSc1d1
tiskárna	tiskárna	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
473	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
901916	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
392	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Epikúros	Epikúrosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
šťastnom	šťastnom	k1gInSc4
živote	život	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Miloslav	Miloslav	k1gMnSc1
Okál	okál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
Pravda	pravda	k1gFnSc1
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
445	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
218	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
13	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Překlad	překlad	k1gInSc1
části	část	k1gFnSc2
sbírky	sbírka	k1gFnSc2
H.	H.	kA
Usenera	Usener	k1gMnSc2
„	„	k?
<g/>
Epicurea	Epicureus	k1gMnSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
dochované	dochovaný	k2eAgInPc1d1
texty	text	k1gInPc1
a	a	k8xC
zlomky	zlomek	k1gInPc1
z	z	k7c2
díla	dílo	k1gNnSc2
Epikura	Epikur	k1gMnSc2
i	i	k8xC
jeho	jeho	k3xOp3gMnPc2
žáků	žák	k1gMnPc2
<g/>
.	.	kIx.
<g/>
]	]	kIx)
S.	S.	kA
378	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ALEKSANDROV	ALEKSANDROV	kA
<g/>
,	,	kIx,
Georgij	Georgij	k1gMnSc1
Fedorovič	Fedorovič	k1gMnSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
filosofie	filosofie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	I	kA
<g/>
,	,	kIx,
Filosofie	filosofie	k1gFnSc1
antické	antický	k2eAgFnSc2d1
a	a	k8xC
feudální	feudální	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
autoris	autoris	k1gFnSc1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1950	#num#	k4
<g/>
.	.	kIx.
512	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
502555	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
294	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
LONG	LONG	kA
<g/>
,	,	kIx,
A.	A.	kA
A.	A.	kA
Hellénistická	Hellénistický	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
<g/>
:	:	kIx,
stoikové	stoik	k1gMnPc1
<g/>
,	,	kIx,
epikurejci	epikurejec	k1gMnPc1
<g/>
,	,	kIx,
skeptikové	skeptik	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Petr	Petr	k1gMnSc1
Kolev	Kolev	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
OIKOYMENH	OIKOYMENH	kA
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
341	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7298	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
77	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
32	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BAHNÍK	bahník	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovník	slovník	k1gInSc1
antické	antický	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1974	#num#	k4
<g/>
.	.	kIx.
717	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
160204	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
214	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Epikúros	Epikúrosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
šťastnom	šťastnom	k1gInSc4
živote	život	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Miloslav	Miloslav	k1gMnSc1
Okál	okál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
Pravda	pravda	k1gFnSc1
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
445	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
218	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
13	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Překlad	překlad	k1gInSc1
části	část	k1gFnSc2
sbírky	sbírka	k1gFnSc2
H.	H.	kA
Usenera	Usener	k1gMnSc2
„	„	k?
<g/>
Epicurea	Epicureus	k1gMnSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
dochované	dochovaný	k2eAgInPc1d1
texty	text	k1gInPc1
a	a	k8xC
zlomky	zlomek	k1gInPc1
z	z	k7c2
díla	dílo	k1gNnSc2
Epikura	Epikur	k1gMnSc2
i	i	k8xC
jeho	jeho	k3xOp3gMnPc2
žáků	žák	k1gMnPc2
<g/>
.	.	kIx.
<g/>
]	]	kIx)
S.	S.	kA
389	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
SVOBODA	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
Ludvík	Ludvík	k1gMnSc1
ed	ed	k?
<g/>
.	.	kIx.
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
antiky	antika	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1973	#num#	k4
<g/>
.	.	kIx.
744	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
127564	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
243	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
HORATIUS	Horatius	k1gMnSc1
<g/>
,	,	kIx,
Quintus	Quintus	k1gMnSc1
Flaccus	Flaccus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vavřín	vavřín	k1gInSc4
a	a	k8xC
réva	réva	k1gFnSc1
<g/>
:	:	kIx,
ódy	ód	k1gInPc1
<g/>
,	,	kIx,
epódy	epód	k1gInPc1
<g/>
,	,	kIx,
satiry	satira	k1gFnPc1
<g/>
,	,	kIx,
listy	list	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Jindřich	Jindřich	k1gMnSc1
Pokorný	Pokorný	k1gMnSc1
a	a	k8xC
Rudolf	Rudolf	k1gMnSc1
Mertlík	Mertlík	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předmluvu	předmluva	k1gFnSc4
napsala	napsat	k5eAaBmAgFnS,k5eAaPmAgFnS
Eva	Eva	k1gFnSc1
Kuťáková	Kuťáková	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
úplné	úplný	k2eAgFnSc2d1
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
.	.	kIx.
364	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
140078	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
HORATIUS	Horatius	k1gMnSc1
<g/>
,	,	kIx,
Quintus	Quintus	k1gMnSc1
Flaccus	Flaccus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vavřín	vavřín	k1gInSc4
a	a	k8xC
réva	réva	k1gFnSc1
<g/>
:	:	kIx,
ódy	ód	k1gInPc1
<g/>
,	,	kIx,
epódy	epód	k1gInPc1
<g/>
,	,	kIx,
satiry	satira	k1gFnPc1
<g/>
,	,	kIx,
listy	list	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Jindřich	Jindřich	k1gMnSc1
Pokorný	Pokorný	k1gMnSc1
a	a	k8xC
Rudolf	Rudolf	k1gMnSc1
Mertlík	Mertlík	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předmluvu	předmluva	k1gFnSc4
napsala	napsat	k5eAaBmAgFnS,k5eAaPmAgFnS
Eva	Eva	k1gFnSc1
Kuťáková	Kuťáková	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
úplné	úplný	k2eAgFnSc2d1
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
.	.	kIx.
364	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
140078	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
230	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CICERO	Cicero	k1gMnSc1
<g/>
,	,	kIx,
Marcus	Marcus	k1gMnSc1
Tullius	Tullius	k1gInSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuskulské	Tuskulský	k2eAgInPc1d1
hovory	hovor	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Václav	Václav	k1gMnSc1
Bahník	bahník	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
souborné	souborný	k2eAgFnSc2d1
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
.	.	kIx.
433	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
123492	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Citovaný	citovaný	k2eAgInSc1d1
text	text	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
165	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
↑	↑	k?
VIDMANOVÁ	VIDMANOVÁ	kA
<g/>
,	,	kIx,
Anežka	Anežka	k1gFnSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
a	a	k8xC
KUŤÁKOVÁ	KUŤÁKOVÁ	kA
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovník	slovník	k1gInSc4
latinských	latinský	k2eAgMnPc2d1
spisovatelů	spisovatel	k1gMnPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
.	.	kIx.
718	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
24629	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
87	#num#	k4
a	a	k8xC
516	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
VIDMANOVÁ	VIDMANOVÁ	kA
<g/>
,	,	kIx,
Anežka	Anežka	k1gFnSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
a	a	k8xC
KUŤÁKOVÁ	KUŤÁKOVÁ	kA
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovník	slovník	k1gInSc4
latinských	latinský	k2eAgMnPc2d1
spisovatelů	spisovatel	k1gMnPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
.	.	kIx.
718	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
24629	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
396	#num#	k4
<g/>
–	–	k?
<g/>
397	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GROH	GROH	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řečtí	řecký	k2eAgMnPc1d1
filosofové	filosof	k1gMnPc1
a	a	k8xC
mystici	mystik	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
Nákladem	náklad	k1gInSc7
Společnosti	společnost	k1gFnSc2
přátel	přítel	k1gMnPc2
antické	antický	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
.	.	kIx.
174	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
669181	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Kapitola	kapitola	k1gFnSc1
„	„	k?
<g/>
Filosofie	filosofie	k1gFnSc1
v	v	k7c6
období	období	k1gNnSc6
hellenistickém	hellenistický	k2eAgNnSc6d1
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
101	#num#	k4
<g/>
–	–	k?
<g/>
124	#num#	k4
<g/>
;	;	kIx,
autor	autor	k1gMnSc1
Otakar	Otakar	k1gMnSc1
Jiráni	Jirán	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
]	]	kIx)
S.	S.	kA
116	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SVOBODA	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
Ludvík	Ludvík	k1gMnSc1
ed	ed	k?
<g/>
.	.	kIx.
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
antiky	antika	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1973	#num#	k4
<g/>
.	.	kIx.
744	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
127564	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
173	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
LONG	LONG	kA
<g/>
,	,	kIx,
A.	A.	kA
A.	A.	kA
Hellénistická	Hellénistický	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
<g/>
:	:	kIx,
stoikové	stoik	k1gMnPc1
<g/>
,	,	kIx,
epikurejci	epikurejec	k1gMnPc1
<g/>
,	,	kIx,
skeptikové	skeptik	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Petr	Petr	k1gMnSc1
Kolev	Kolev	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
OIKOYMENH	OIKOYMENH	kA
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
341	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7298	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
77	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
32	#num#	k4
<g/>
–	–	k?
<g/>
33	#num#	k4
<g/>
↑	↑	k?
Lúkianos	Lúkianos	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
bozích	bůh	k1gMnPc6
a	a	k8xC
lidech	člověk	k1gMnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
.	.	kIx.
399	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
403259	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Próza	próza	k1gFnSc1
„	„	k?
<g/>
Alexandr	Alexandr	k1gMnSc1
neboli	neboli	k8xC
Lžiprorok	lžiprorok	k1gMnSc1
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
172	#num#	k4
<g/>
–	–	k?
<g/>
198	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
↑	↑	k?
LONG	LONG	kA
<g/>
,	,	kIx,
A.	A.	kA
A.	A.	kA
Hellénistická	Hellénistický	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
<g/>
:	:	kIx,
stoikové	stoik	k1gMnPc1
<g/>
,	,	kIx,
epikurejci	epikurejec	k1gMnPc1
<g/>
,	,	kIx,
skeptikové	skeptik	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Petr	Petr	k1gMnSc1
Kolev	Kolev	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
OIKOYMENH	OIKOYMENH	kA
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
341	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7298	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
77	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
284	#num#	k4
a	a	k8xC
33	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CETL	CETL	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průvodce	průvodce	k1gMnSc1
dějinami	dějiny	k1gFnPc7
evropského	evropský	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Panorama	panorama	k1gNnSc1
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
.	.	kIx.
634	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
27282	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Kapitola	kapitola	k1gFnSc1
„	„	k?
<g/>
Epikuros	Epikuros	k1gMnSc1
–	–	k?
dovršitel	dovršitel	k1gMnSc1
atomismu	atomismus	k1gInSc2
<g/>
"	"	kIx"
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
76	#num#	k4
<g/>
–	–	k?
<g/>
79	#num#	k4
<g/>
;	;	kIx,
autor	autor	k1gMnSc1
Radislav	Radislav	k1gMnSc1
Hošek	Hošek	k1gMnSc1
<g/>
.	.	kIx.
<g/>
]	]	kIx)
S.	S.	kA
79	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
LONG	LONG	kA
<g/>
,	,	kIx,
A.	A.	kA
A.	A.	kA
Hellénistická	Hellénistický	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
<g/>
:	:	kIx,
stoikové	stoik	k1gMnPc1
<g/>
,	,	kIx,
epikurejci	epikurejec	k1gMnPc1
<g/>
,	,	kIx,
skeptikové	skeptik	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Petr	Petr	k1gMnSc1
Kolev	Kolev	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
OIKOYMENH	OIKOYMENH	kA
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
341	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7298	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
77	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
285	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
DUROZOI	DUROZOI	kA
<g/>
,	,	kIx,
Gérard	Gérard	k1gMnSc1
a	a	k8xC
ROUSSEL	ROUSSEL	kA
<g/>
,	,	kIx,
André	André	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filozofický	filozofický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
EWA	EWA	kA
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
,	,	kIx,
©	©	k?
<g/>
1994	#num#	k4
<g/>
.	.	kIx.
352	#num#	k4
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85764	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
71	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SVOBODA	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
Ludvík	Ludvík	k1gMnSc1
ed	ed	k?
<g/>
.	.	kIx.
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
antiky	antika	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1973	#num#	k4
<g/>
.	.	kIx.
744	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
127564	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
173	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
DUROZOI	DUROZOI	kA
<g/>
,	,	kIx,
Gérard	Gérard	k1gMnSc1
a	a	k8xC
ROUSSEL	ROUSSEL	kA
<g/>
,	,	kIx,
André	André	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filozofický	filozofický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
EWA	EWA	kA
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
,	,	kIx,
©	©	k?
<g/>
1994	#num#	k4
<g/>
.	.	kIx.
352	#num#	k4
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85764	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
71	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Díogenés	Díogenés	k1gInSc1
Laertios	Laertios	k1gInSc1
<g/>
:	:	kIx,
Životy	život	k1gInPc1
<g/>
,	,	kIx,
názory	názor	k1gInPc1
a	a	k8xC
výroky	výrok	k1gInPc1
proslulých	proslulý	k2eAgMnPc2d1
filosofů	filosof	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Antonín	Antonín	k1gMnSc1
Kolář	Kolář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pelhřimov	Pelhřimov	k1gInSc1
<g/>
:	:	kIx,
Nová	nový	k2eAgFnSc1d1
tiskárna	tiskárna	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
473	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
901916	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
X.	X.	kA
kniha	kniha	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
385	#num#	k4
<g/>
–	–	k?
<g/>
427	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
</s>
<s>
Diogenés	Diogenés	k6eAd1
Laertios	Laertios	k1gMnSc1
a	a	k8xC
Ludvíkovský	ludvíkovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc4
a	a	k8xC
učení	učení	k1gNnSc4
filosofa	filosof	k1gMnSc2
Epikura	Epikur	k1gMnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Rovnost	rovnost	k1gFnSc1
<g/>
,	,	kIx,
1952	#num#	k4
<g/>
.	.	kIx.
117	#num#	k4
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
507093	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Epikúros	Epikúrosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
šťastnom	šťastnom	k1gInSc4
živote	život	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Miloslav	Miloslav	k1gMnSc1
Okál	okál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
Pravda	pravda	k1gFnSc1
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
445	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
218	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
13	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Překlad	překlad	k1gInSc1
části	část	k1gFnSc2
sbírky	sbírka	k1gFnSc2
H.	H.	kA
Usenera	Usener	k1gMnSc2
„	„	k?
<g/>
Epicurea	Epicureus	k1gMnSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
dochované	dochovaný	k2eAgInPc1d1
texty	text	k1gInPc1
a	a	k8xC
zlomky	zlomek	k1gInPc1
z	z	k7c2
díla	dílo	k1gNnSc2
Epikura	Epikur	k1gMnSc2
i	i	k8xC
jeho	jeho	k3xOp3gMnPc2
žáků	žák	k1gMnPc2
<g/>
.	.	kIx.
<g/>
]	]	kIx)
</s>
<s>
Filosofie	filosofie	k1gFnSc1
doby	doba	k1gFnSc2
hellenistické	hellenistický	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
Nákladem	náklad	k1gInSc7
Společnosti	společnost	k1gFnSc2
přátel	přítel	k1gMnPc2
antické	antický	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
1938	#num#	k4
<g/>
.	.	kIx.
161	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
749169	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Stať	stať	k1gFnSc1
„	„	k?
<g/>
Epikuros	Epikuros	k1gMnSc1
a	a	k8xC
epikureismus	epikureismus	k1gInSc1
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
85	#num#	k4
<g/>
–	–	k?
<g/>
127	#num#	k4
<g/>
;	;	kIx,
autor	autor	k1gMnSc1
Jaroslav	Jaroslav	k1gMnSc1
Ludvíkovský	ludvíkovský	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
]	]	kIx)
</s>
<s>
LONG	LONG	kA
<g/>
,	,	kIx,
A.	A.	kA
A.	A.	kA
Hellénistická	Hellénistický	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
<g/>
:	:	kIx,
stoikové	stoik	k1gMnPc1
<g/>
,	,	kIx,
epikurejci	epikurejec	k1gMnPc1
<g/>
,	,	kIx,
skeptikové	skeptik	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Petr	Petr	k1gMnSc1
Kolev	Kolev	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
OIKOYMENH	OIKOYMENH	kA
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
341	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7298	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
77	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Kapitola	kapitola	k1gFnSc1
„	„	k?
<g/>
Epikúros	Epikúrosa	k1gFnPc2
a	a	k8xC
epikureismus	epikureismus	k1gInSc1
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
29	#num#	k4
<g/>
–	–	k?
<g/>
101	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
</s>
<s>
Lucretius	Lucretius	k1gMnSc1
Carus	Carus	k1gMnSc1
<g/>
,	,	kIx,
Titus	Titus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
přírodě	příroda	k1gFnSc6
=	=	kIx~
De	De	k?
rerum	rerum	k1gInSc1
natura	natura	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Julie	Julie	k1gFnSc2
Nováková	Nováková	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1971	#num#	k4
<g/>
.	.	kIx.
272	#num#	k4
s.	s.	k?
Antická	antický	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
<g/>
;	;	kIx,
sv.	sv.	kA
12	#num#	k4
<g/>
.	.	kIx.
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
135830	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
NOVÁK	Novák	k1gMnSc1
<g/>
,	,	kIx,
Mirko	Mirko	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústřední	ústřední	k2eAgInSc1d1
problém	problém	k1gInSc1
materialismu	materialismus	k1gInSc2
Epikurova	Epikurův	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sborník	sborník	k1gInSc1
prací	prací	k2eAgFnSc2d1
Filozofické	filozofický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
brněnské	brněnský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
<g/>
,	,	kIx,
Řada	řada	k1gFnSc1
filozofická	filozofický	k2eAgFnSc1d1
<g/>
.	.	kIx.
1953	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgFnSc1wB
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
<g/>
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
51	#num#	k4
<g/>
–	–	k?
<g/>
66	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
naučný	naučný	k2eAgInSc1d1
<g/>
:	:	kIx,
illustrovaná	illustrovaný	k2eAgFnSc1d1
encyklopaedie	encyklopaedie	k1gFnSc1
obecných	obecný	k2eAgFnPc2d1
vědomostí	vědomost	k1gFnPc2
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
J.	J.	kA
Otto	Otto	k1gMnSc1
<g/>
,	,	kIx,
1894	#num#	k4
<g/>
.	.	kIx.
1039	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
277218	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Heslo	heslo	k1gNnSc4
„	„	k?
<g/>
Epikúros	Epikúrosa	k1gFnPc2
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
665	#num#	k4
<g/>
–	–	k?
<g/>
667	#num#	k4
a	a	k8xC
heslo	heslo	k1gNnSc1
„	„	k?
<g/>
Epikúrovci	Epikúrovec	k1gMnPc1
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
667	#num#	k4
<g/>
;	;	kIx,
obě	dva	k4xCgNnPc4
hesla	heslo	k1gNnPc4
napsal	napsat	k5eAaPmAgInS,k5eAaBmAgInS
František	František	k1gMnSc1
Čáda	Čáda	k1gMnSc1
(	(	kIx(
<g/>
1865	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
</s>
<s>
Platón	platón	k1gInSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Píši	psát	k5eAaImIp1nS
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
příteli	přítel	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydání	vydání	k1gNnSc1
první	první	k4xOgFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
.	.	kIx.
400	#num#	k4
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antická	antický	k2eAgFnSc1d1
próza	próza	k1gFnSc1
<g/>
;	;	kIx,
svazek	svazek	k1gInSc1
šestý	šestý	k4xOgInSc1
<g/>
.	.	kIx.
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
154418	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Epikúrův	Epikúrův	k2eAgInSc1d1
dopis	dopis	k1gInSc1
Menoikeovi	Menoikeus	k1gMnSc3
v	v	k7c6
překladu	překlad	k1gInSc6
Bořivoje	Bořivoj	k1gMnSc2
Boreckého	borecký	k2eAgInSc2d1
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
27	#num#	k4
<g/>
–	–	k?
<g/>
31	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
</s>
<s>
TATARKIEWICZ	TATARKIEWICZ	kA
<g/>
,	,	kIx,
Władysław	Władysław	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dejiny	Dejin	k2eAgFnPc4d1
estetiky	estetika	k1gFnPc4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Staroveká	Staroveká	k1gFnSc1
estetika	estetika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přeložil	přeložit	k5eAaPmAgMnS
Jozef	Jozef	k1gMnSc1
Marušiak	Marušiak	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Texty	text	k1gInPc1
antických	antický	k2eAgMnPc2d1
autorů	autor	k1gMnPc2
přeložil	přeložit	k5eAaPmAgMnS
Peter	Peter	k1gMnSc1
Kuklica	Kuklica	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Bratislave	Bratislav	k1gMnSc5
:	:	kIx,
Tatran	Tatran	k1gInSc1
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
.	.	kIx.
528	#num#	k4
s.	s.	k?
[	[	kIx(
<g/>
Kapitola	kapitola	k1gFnSc1
„	„	k?
<g/>
Estetika	estetika	k1gFnSc1
epikurovcov	epikurovcov	k1gInSc1
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
181	#num#	k4
<g/>
–	–	k?
<g/>
185	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Antická	antický	k2eAgFnSc1d1
řecká	řecký	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
Předsókratovci	Předsókratovec	k1gMnSc3
</s>
<s>
iónská	iónský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
(	(	kIx(
<g/>
milétská	milétský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
•	•	k?
Hérakleitos	Hérakleitos	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
pythagoreismus	pythagoreismus	k1gInSc1
•	•	k?
eleaté	eleata	k1gMnPc1
•	•	k?
pluralisté	pluralista	k1gMnPc1
•	•	k?
atomisté	atomista	k1gMnPc1
•	•	k?
sofisté	sofista	k1gMnPc1
</s>
<s>
Sókratovská	Sókratovský	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
</s>
<s>
platonismus	platonismus	k1gInSc1
(	(	kIx(
<g/>
Platón	platón	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
peripatetikové	peripatetik	k1gMnPc1
(	(	kIx(
<g/>
Aristotelés	Aristotelés	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
kynismus	kynismus	k1gInSc4
•	•	k?
kyrénaikové	kyrénaikový	k2eAgFnSc2d1
•	•	k?
megarská	megarský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
•	•	k?
élidská	élidský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
</s>
<s>
Helénistická	helénistický	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
</s>
<s>
epikureismus	epikureismus	k1gInSc1
•	•	k?
stoicismus	stoicismus	k1gInSc1
•	•	k?
pyrrhonismus	pyrrhonismus	k1gInSc1
(	(	kIx(
<g/>
Pyrrhón	Pyrrhón	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
neopythagoreismus	neopythagoreismus	k1gInSc1
•	•	k?
neoplatonismus	neoplatonismus	k1gInSc1
(	(	kIx(
<g/>
Plótínos	Plótínos	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
dějiny	dějiny	k1gFnPc1
západní	západní	k2eAgFnSc2d1
filosofie	filosofie	k1gFnSc2
•	•	k?
starověká	starověký	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
•	•	k?
starověké	starověký	k2eAgNnSc4d1
Řecko	Řecko	k1gNnSc4
•	•	k?
řecká	řecký	k2eAgFnSc1d1
mytologie	mytologie	k1gFnSc1
•	•	k?
patristika	patristika	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Filosofie	filosofie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4015033-1	4015033-1	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
2840	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
96001809	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
96001809	#num#	k4
</s>
