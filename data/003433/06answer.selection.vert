<s>
Mezi	mezi	k7c4	mezi
členy	člen	k1gMnPc4	člen
nadřádu	nadřád	k1gInSc2	nadřád
běžců	běžec	k1gMnPc2	běžec
dále	daleko	k6eAd2	daleko
patří	patřit	k5eAaImIp3nP	patřit
nanduové	nandu	k1gMnPc1	nandu
<g/>
,	,	kIx,	,
kasuárové	kasuár	k1gMnPc1	kasuár
<g/>
,	,	kIx,	,
pštrosi	pštros	k1gMnPc1	pštros
emu	emu	k1gMnSc1	emu
<g/>
,	,	kIx,	,
kiviové	kivi	k1gMnPc1	kivi
a	a	k8xC	a
již	již	k6eAd1	již
vyhynulí	vyhynulý	k2eAgMnPc1d1	vyhynulý
největší	veliký	k2eAgMnPc1d3	veliký
ptáci	pták	k1gMnPc1	pták
světa	svět	k1gInSc2	svět
-	-	kIx~	-
běžci	běžec	k1gMnPc1	běžec
rodu	rod	k1gInSc2	rod
Aepyornithes	Aepyornithes	k1gInSc1	Aepyornithes
<g/>
.	.	kIx.	.
</s>
