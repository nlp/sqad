<s>
Vikariát	vikariát	k1gInSc1
Plzeň-jih	Plzeň-jiha	k1gFnPc2
</s>
<s>
Vikariát	vikariát	k1gInSc1
Plzeň-jih	Plzeň-jih	k1gInSc1
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
z	z	k7c2
deseti	deset	k4xCc2
vikariátů	vikariát	k1gInPc2
Diecéze	diecéze	k1gFnSc2
plzeňské	plzeňské	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Členění	členění	k1gNnSc1
vikariátu	vikariát	k1gInSc2
</s>
<s>
Vikariát	vikariát	k1gInSc1
Plzeň-jih	Plzeň-jih	k1gInSc1
se	se	k3xPyFc4
člení	členit	k5eAaImIp3nS
na	na	k7c4
následujících	následující	k2eAgInPc2d1
osm	osm	k4xCc4
farností	farnost	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
FarnostSprávceSídloFarní	FarnostSprávceSídloFarní	k2eAgNnSc1d1
kostelFiliální	kostelFiliální	k2eAgNnSc1d1
kostelyDatum	kostelyDatum	k1gNnSc1
zřízení	zřízení	k1gNnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
BloviceP	BloviceP	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Piotr	Piotr	k1gMnSc1
Marek	Marek	k1gMnSc1
<g/>
,	,	kIx,
administrátorAmerická	administrátorAmerický	k2eAgFnSc1d1
53	#num#	k4
<g/>
,	,	kIx,
336	#num#	k4
01	#num#	k4
BloviceKostel	BloviceKostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
Evangelisty	evangelista	k1gMnSc2
(	(	kIx(
<g/>
Blovice	Blovice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Prokopa	Prokop	k1gMnSc2
(	(	kIx(
<g/>
Letiny	Letina	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Martina	Martin	k1gMnSc2
(	(	kIx(
<g/>
Neurazy	Neuraz	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
(	(	kIx(
<g/>
Seč	seč	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Mikuláše	Mikuláš	k1gMnSc2
(	(	kIx(
<g/>
Zdemyslice	Zdemyslice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Cyrila	Cyril	k1gMnSc2
a	a	k8xC
Metoděje	Metoděj	k1gMnSc2
(	(	kIx(
<g/>
Řenče	Řenče	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Václava	Václav	k1gMnSc2
(	(	kIx(
<g/>
Žinkovy	Žinkův	k2eAgFnSc2d1
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Václava	Václav	k1gMnSc2
(	(	kIx(
<g/>
Žďár	Žďár	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
DobřanyP	DobřanyP	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alvaro	Alvara	k1gFnSc5
Grammatica	Grammatica	k1gFnSc1
Th	Th	k1gMnSc1
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
,	,	kIx,
administrátorNáměstí	administrátorNáměstit	k5eAaPmIp3nS
TGM	TGM	kA
3	#num#	k4
<g/>
,	,	kIx,
334	#num#	k4
41	#num#	k4
DobřanyKostel	DobřanyKostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Mikuláše	Mikuláš	k1gMnSc2
(	(	kIx(
<g/>
Dobřany	Dobřany	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
Narození	narození	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
(	(	kIx(
<g/>
Chotěšov	Chotěšov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
HolýšovP	HolýšovP	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hubert	Hubert	k1gMnSc1
Hain	Hain	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
<g/>
,	,	kIx,
člen	člen	k1gMnSc1
společné	společný	k2eAgFnSc2d1
duchovní	duchovní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
farnosti	farnost	k1gFnPc1
StodNáměstí	StodNáměstí	k1gNnSc1
5	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1	#num#	k4
<g/>
,	,	kIx,
345	#num#	k4
62	#num#	k4
<g/>
,	,	kIx,
HolýšovKostel	HolýšovKostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Petra	Petr	k1gMnSc2
a	a	k8xC
Pavla	Pavel	k1gMnSc2
(	(	kIx(
<g/>
Holýšov	Holýšov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
PřešticeMons	PřešticeMonsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ThLic	ThLice	k1gInPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
ICLic	ICLic	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karel	Karel	k1gMnSc1
Plavec	plavec	k1gMnSc1
<g/>
,	,	kIx,
Th	Th	k1gMnSc1
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
,	,	kIx,
farář	farář	k1gMnSc1
a	a	k8xC
okrskový	okrskový	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
farnosti	farnost	k1gFnSc6
KolovečHlávkova	KolovečHlávkův	k2eAgFnSc1d1
30	#num#	k4
<g/>
,	,	kIx,
334	#num#	k4
01	#num#	k4
PřešticeKostel	PřešticeKostela	k1gFnPc2
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
(	(	kIx(
<g/>
Přeštice	Přeštice	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Matěje	Matěj	k1gMnSc2
(	(	kIx(
<g/>
Horšice	Horšic	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
Všech	všecek	k3xTgFnPc2
svatých	svatá	k1gFnPc2
(	(	kIx(
<g/>
Kbel	kbel	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Spálené	spálený	k2eAgFnSc2d1
PoříčíP	PoříčíP	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ThDr.	ThDr.	k1gMnSc1
Miroslav	Miroslav	k1gMnSc1
Vančo	Vančo	k1gMnSc1
<g/>
,	,	kIx,
PhD	PhD	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
administrátorNáměstí	administrátorNáměstí	k1gNnSc1
Svobody	svoboda	k1gFnSc2
139	#num#	k4
<g/>
,	,	kIx,
335	#num#	k4
61	#num#	k4
Spálené	spálený	k2eAgInPc1d1
PoříčíKostel	PoříčíKostel	k1gInSc1
sv.	sv.	kA
Mikuláše	Mikuláš	k1gMnSc2
(	(	kIx(
<g/>
Spálené	spálený	k2eAgNnSc1d1
Poříčí	Poříčí	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
(	(	kIx(
<g/>
Nové	Nové	k2eAgFnPc1d1
Mitrovice	Mitrovice	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
Křtitele	křtitel	k1gMnSc2
(	(	kIx(
<g/>
Čížkov	Čížkov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Michala	Michal	k1gMnSc2
(	(	kIx(
<g/>
Dožice	Dožic	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
(	(	kIx(
<g/>
Těnovice	Těnovice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Filipa	Filip	k1gMnSc2
a	a	k8xC
Jakuba	Jakub	k1gMnSc2
(	(	kIx(
<g/>
Číčov	Číčov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kaple	kaple	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Petra	Petr	k1gMnSc2
a	a	k8xC
Pavla	Pavel	k1gMnSc2
(	(	kIx(
<g/>
Lipnice	Lipnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Kaple	kaple	k1gFnSc1
Navštívení	navštívení	k1gNnPc2
Panny	Panna	k1gFnSc2
Marie	Marie	k1gFnSc1
(	(	kIx(
<g/>
Nechánice	Nechánice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Starý	Starý	k1gMnSc1
PlzenecP	PlzenecP	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petr	Petr	k1gMnSc1
Franta	Franta	k1gMnSc1
<g/>
,	,	kIx,
farářTřebízského	farářTřebízský	k2eAgNnSc2d1
náměstí	náměstí	k1gNnSc2
1	#num#	k4
<g/>
,	,	kIx,
332	#num#	k4
02	#num#	k4
Starý	starý	k2eAgInSc4d1
PlzenecKostel	PlzenecKostel	k1gInSc4
Narození	narození	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
(	(	kIx(
<g/>
Starý	Starý	k1gMnSc1
Plzenec	Plzenec	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Martina	Martin	k1gMnSc2
(	(	kIx(
<g/>
Chválenice	Chválenice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
Všech	všecek	k3xTgFnPc2
svatých	svatá	k1gFnPc2
(	(	kIx(
<g/>
Nezvěstice	Nezvěstika	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Vojtěcha	Vojtěch	k1gMnSc2
(	(	kIx(
<g/>
Šťáhlavy	Šťáhlava	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Vavřince	Vavřinec	k1gMnSc2
(	(	kIx(
<g/>
Žákava	Žákava	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Kaple	kaple	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Vojtěcha	Vojtěch	k1gMnSc2
(	(	kIx(
<g/>
Planiny	planina	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
StodP	StodP	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ThLic	ThLice	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krzystof	Krzystof	k1gMnSc1
Dedek	dedek	k1gMnSc1
<g/>
,	,	kIx,
moderátor	moderátor	k1gMnSc1
spol	spol	k1gInSc1
<g/>
.	.	kIx.
duchovní	duchovní	k2eAgInSc1d1
správyStodKostel	správyStodKostel	k1gInSc1
svaté	svatý	k2eAgFnSc2d1
Máří	Máří	k?
Magdaleny	Magdalena	k1gFnSc2
(	(	kIx(
<g/>
Stod	Stod	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Jiří	Jiří	k1gMnSc2
(	(	kIx(
<g/>
Hradec	Hradec	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
MerklínP	MerklínP	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pavel	Pavel	k1gMnSc1
Vodička	Vodička	k1gMnSc1
<g/>
,	,	kIx,
administrátorŠkolní	administrátorŠkolní	k2eAgMnSc1d1
13	#num#	k4
<g/>
,	,	kIx,
334	#num#	k4
52	#num#	k4
MerklínKostel	MerklínKostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Mikuláše	Mikuláš	k1gMnSc2
(	(	kIx(
<g/>
Merklín	Merklín	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Václava	Václav	k1gMnSc2
(	(	kIx(
<g/>
Dnešice	Dnešic	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Petra	Petr	k1gMnSc2
a	a	k8xC
Pavla	Pavel	k1gMnSc2
(	(	kIx(
<g/>
Dolní	dolní	k2eAgFnPc1d1
Lukavice	Lukavice	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Vikariát	vikariát	k1gInSc1
Plzeň-jih	Plzeň-jih	k1gMnSc1
Plzeňská	plzeňská	k1gFnSc1
diecéze	diecéze	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Vikariáty	vikariát	k1gInPc1
plzeňské	plzeňský	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
</s>
<s>
domažlický	domažlický	k2eAgInSc1d1
•	•	k?
chebský	chebský	k2eAgInSc1d1
•	•	k?
karlovarský	karlovarský	k2eAgInSc1d1
•	•	k?
klatovský	klatovský	k2eAgInSc1d1
•	•	k?
Plzeň-město	Plzeň-města	k1gMnSc5
•	•	k?
Plzeň-jih	Plzeň-jih	k1gInSc1
•	•	k?
Plzeň-sever	Plzeň-sever	k1gInSc1
•	•	k?
rokycanský	rokycanský	k2eAgInSc1d1
•	•	k?
sokolovský	sokolovský	k2eAgInSc1d1
•	•	k?
tachovský	tachovský	k2eAgInSc1d1
</s>
<s>
Farnosti	farnost	k1gFnPc1
vikariátu	vikariát	k1gInSc2
Plzeň-jih	Plzeň-jih	k1gInSc1
</s>
<s>
Blovice	Blovice	k1gFnSc1
•	•	k?
Dobřany	Dobřany	k1gInPc1
•	•	k?
Holýšov	Holýšov	k1gInSc1
•	•	k?
Merklín	Merklín	k1gInSc1
•	•	k?
Přeštice	Přeštice	k1gFnPc4
•	•	k?
Spálené	spálený	k2eAgNnSc1d1
Poříčí	Poříčí	k1gNnSc1
•	•	k?
Starý	starý	k2eAgMnSc1d1
Plzenec	Plzenec	k1gMnSc1
•	•	k?
Stod	Stod	k1gMnSc1
</s>
