<s>
Felice	Felice	k1gFnSc1	Felice
Bauerová	Bauerová	k1gFnSc1	Bauerová
<g/>
,	,	kIx,	,
nepřechýleně	přechýleně	k6eNd1	přechýleně
Felice	Felice	k1gFnSc1	Felice
Bauer	Bauer	k1gMnSc1	Bauer
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1887	[number]	k4	1887
v	v	k7c6	v
Neustadtu	Neustadt	k1gInSc6	Neustadt
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1960	[number]	k4	1960
v	v	k7c6	v
Rye	Rye	k1gFnSc6	Rye
u	u	k7c2	u
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
a	a	k8xC	a
snoubenka	snoubenka	k1gFnSc1	snoubenka
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
.	.	kIx.	.
</s>
<s>
Felice	Felice	k1gFnSc1	Felice
Bauerová	Bauerová	k1gFnSc1	Bauerová
byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
pojišťovacího	pojišťovací	k2eAgMnSc4d1	pojišťovací
agenta	agent	k1gMnSc4	agent
ze	z	k7c2	z
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
jí	on	k3xPp3gFnSc3	on
bylo	být	k5eAaImAgNnS	být
dvanáct	dvanáct	k4xCc4	dvanáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k9	jako
prokuristka	prokuristka	k1gFnSc1	prokuristka
u	u	k7c2	u
firmy	firma	k1gFnSc2	firma
Firma	firma	k1gFnSc1	firma
Carl	Carl	k1gMnSc1	Carl
Lindström	Lindström	k1gMnSc1	Lindström
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vzdálenou	vzdálený	k2eAgFnSc4d1	vzdálená
příbuznou	příbuzná	k1gFnSc4	příbuzná
Maxe	Max	k1gMnSc2	Max
Broda	Brod	k1gMnSc2	Brod
<g/>
,	,	kIx,	,
Kafkova	Kafkův	k2eAgMnSc2d1	Kafkův
přítele	přítel	k1gMnSc2	přítel
a	a	k8xC	a
právě	právě	k9	právě
u	u	k7c2	u
Brodových	brodový	k2eAgFnPc2d1	Brodová
se	se	k3xPyFc4	se
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1912	[number]	k4	1912
Felice	Felice	k1gFnSc2	Felice
a	a	k8xC	a
Kafka	Kafka	k1gMnSc1	Kafka
spolu	spolu	k6eAd1	spolu
prvně	prvně	k?	prvně
setkali	setkat	k5eAaPmAgMnP	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
večera	večer	k1gInSc2	večer
si	se	k3xPyFc3	se
vyměnili	vyměnit	k5eAaPmAgMnP	vyměnit
adresy	adresa	k1gFnPc4	adresa
a	a	k8xC	a
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
se	se	k3xPyFc4	se
podniknout	podniknout	k5eAaPmF	podniknout
společnou	společný	k2eAgFnSc4d1	společná
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Palestiny	Palestina	k1gFnSc2	Palestina
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
začala	začít	k5eAaPmAgFnS	začít
jejich	jejich	k3xOp3gFnSc1	jejich
korespondence	korespondence	k1gFnSc1	korespondence
i	i	k9	i
pětiletý	pětiletý	k2eAgInSc4d1	pětiletý
poměr	poměr	k1gInSc4	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Milostný	milostný	k2eAgInSc1d1	milostný
poměr	poměr	k1gInSc1	poměr
Felice	Felic	k1gMnSc2	Felic
Bauerové	Bauerové	k2eAgMnSc2d1	Bauerové
a	a	k8xC	a
Kafky	Kafka	k1gMnSc2	Kafka
trval	trvat	k5eAaImAgMnS	trvat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
rozporuplný	rozporuplný	k2eAgInSc1d1	rozporuplný
a	a	k8xC	a
proměnlivý	proměnlivý	k2eAgInSc1d1	proměnlivý
<g/>
.	.	kIx.	.
</s>
<s>
Komplikace	komplikace	k1gFnPc1	komplikace
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
vnášela	vnášet	k5eAaImAgFnS	vnášet
i	i	k9	i
Felicina	Felicin	k2eAgFnSc1d1	Felicin
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
Grete	Gret	k1gInSc5	Gret
Blochová	Blochový	k2eAgNnPc4d1	Blochový
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
byl	být	k5eAaImAgMnS	být
Kafka	Kafka	k1gMnSc1	Kafka
rovněž	rovněž	k9	rovněž
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
zamilovaný	zamilovaný	k2eAgInSc4d1	zamilovaný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
jejich	jejich	k3xOp3gInSc2	jejich
vztahu	vztah	k1gInSc2	vztah
si	se	k3xPyFc3	se
spolu	spolu	k6eAd1	spolu
vyměnili	vyměnit	k5eAaPmAgMnP	vyměnit
řadu	řada	k1gFnSc4	řada
dopisů	dopis	k1gInPc2	dopis
(	(	kIx(	(
<g/>
Kafka	Kafka	k1gMnSc1	Kafka
jich	on	k3xPp3gMnPc2	on
Felici	Felic	k1gMnPc7	Felic
poslal	poslat	k5eAaPmAgMnS	poslat
na	na	k7c4	na
pět	pět	k4xCc4	pět
set	set	k1gInSc4	set
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podnikli	podniknout	k5eAaPmAgMnP	podniknout
spolu	spolu	k6eAd1	spolu
několik	několik	k4yIc4	několik
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
dvakrát	dvakrát	k6eAd1	dvakrát
zasnoubeni	zasnouben	k2eAgMnPc1d1	zasnouben
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
a	a	k8xC	a
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Felice	Felice	k1gFnSc1	Felice
dozvěděla	dozvědět	k5eAaPmAgFnS	dozvědět
o	o	k7c6	o
Kafkově	Kafkův	k2eAgInSc6d1	Kafkův
současném	současný	k2eAgInSc6d1	současný
blízkém	blízký	k2eAgInSc6d1	blízký
vztahu	vztah	k1gInSc6	vztah
s	s	k7c7	s
Gretou	Greta	k1gFnSc7	Greta
<g/>
,	,	kIx,	,
žádala	žádat	k5eAaImAgFnS	žádat
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyčítavé	vyčítavý	k2eAgFnSc6d1	vyčítavá
hádce	hádka	k1gFnSc6	hádka
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
byla	být	k5eAaImAgFnS	být
přítomna	přítomen	k2eAgFnSc1d1	přítomna
i	i	k8xC	i
Felicina	Felicin	k2eAgFnSc1d1	Felicin
sestra	sestra	k1gFnSc1	sestra
Erna	Ern	k1gInSc2	Ern
a	a	k8xC	a
s	s	k7c7	s
Grete	Gret	k1gInSc5	Gret
<g/>
,	,	kIx,	,
zrušil	zrušit	k5eAaPmAgMnS	zrušit
Kafka	Kafka	k1gMnSc1	Kafka
své	svůj	k3xOyFgFnSc2	svůj
první	první	k4xOgFnSc2	první
zásnuby	zásnuba	k1gFnSc2	zásnuba
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jí	jíst	k5eAaImIp3nS	jíst
psal	psát	k5eAaImAgMnS	psát
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgInPc4	druhý
zásnuby	zásnub	k1gInPc4	zásnub
zrušil	zrušit	k5eAaPmAgInS	zrušit
<g/>
,	,	kIx,	,
obávaje	obávat	k5eAaImSgInS	obávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
svatba	svatba	k1gFnSc1	svatba
a	a	k8xC	a
manželský	manželský	k2eAgInSc1d1	manželský
život	život	k1gInSc1	život
by	by	kYmCp3nP	by
jej	on	k3xPp3gMnSc4	on
odváděly	odvádět	k5eAaImAgInP	odvádět
od	od	k7c2	od
literární	literární	k2eAgFnSc2d1	literární
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
u	u	k7c2	u
Kafky	Kafka	k1gMnSc2	Kafka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
propukla	propuknout	k5eAaPmAgFnS	propuknout
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
<g/>
,	,	kIx,	,
definitivně	definitivně	k6eAd1	definitivně
Felici	Felice	k1gFnSc4	Felice
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
se	se	k3xPyFc4	se
Felice	Felice	k1gFnSc1	Felice
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
berlínského	berlínský	k2eAgMnSc4d1	berlínský
bankovního	bankovní	k2eAgMnSc4d1	bankovní
prokuristu	prokurista	k1gMnSc4	prokurista
Moritze	Moritz	k1gMnSc4	Moritz
Marasse	Marass	k1gMnSc4	Marass
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
měla	mít	k5eAaImAgFnS	mít
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
Heinze	Heinze	k1gFnSc2	Heinze
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
Ursulu	Ursul	k1gInSc2	Ursul
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
až	až	k9	až
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
se	se	k3xPyFc4	se
odstěhovali	odstěhovat	k5eAaPmAgMnP	odstěhovat
nejdříve	dříve	k6eAd3	dříve
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Marasse	Marasse	k1gFnSc1	Marasse
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Felice	Felice	k1gFnSc1	Felice
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
finančním	finanční	k2eAgInSc7d1	finanční
nedostatkem	nedostatek	k1gInSc7	nedostatek
a	a	k8xC	a
nemocí	nemoc	k1gFnSc7	nemoc
donucena	donucen	k2eAgFnSc1d1	donucena
Kafkovy	Kafkův	k2eAgInPc4d1	Kafkův
dopisy	dopis	k1gInPc4	dopis
prodat	prodat	k5eAaPmF	prodat
nakladateli	nakladatel	k1gMnSc3	nakladatel
Schockenovi	Schocken	k1gMnSc3	Schocken
<g/>
.	.	kIx.	.
</s>
<s>
Žila	žít	k5eAaImAgFnS	žít
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
až	až	k6eAd1	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Dopisy	dopis	k1gInPc1	dopis
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
Kafka	Kafka	k1gMnSc1	Kafka
Felici	Felice	k1gFnSc4	Felice
poslal	poslat	k5eAaPmAgMnS	poslat
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
později	pozdě	k6eAd2	pozdě
knižně	knižně	k6eAd1	knižně
vydány	vydat	k5eAaPmNgInP	vydat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Dopisy	dopis	k1gInPc4	dopis
Felici	Felik	k1gMnPc1	Felik
<g/>
.	.	kIx.	.
</s>
