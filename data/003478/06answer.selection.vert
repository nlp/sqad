<s>
Svatba	svatba	k1gFnSc1
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
po	po	k7c6
téměř	téměř	k6eAd1
desetileté	desetiletý	k2eAgFnSc6d1
známosti	známost	k1gFnSc6
roku	rok	k1gInSc2
1842	[number]	k4
a	a	k8xC
manželé	manžel	k1gMnPc1
Erbenovi	Erbenův	k2eAgMnPc1d1
měli	mít	k5eAaImAgMnP
čtyři	čtyři	k4xCgFnPc4
děti	dítě	k1gFnPc4
<g/>
,	,	kIx,
Blaženu	Blažena	k1gFnSc4
(	(	kIx(
<g/>
1844	[number]	k4
<g/>
–	–	k?
<g/>
1933	[number]	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Ladislavu	Ladislava	k1gFnSc4
(	(	kIx(
<g/>
1846	[number]	k4
<g/>
–	–	k?
<g/>
1892	[number]	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jaromíra	Jaromíra	k1gFnSc1
(	(	kIx(
<g/>
1848	[number]	k4
<g/>
–	–	k?
<g/>
1849	[number]	k4
<g/>
)	)	kIx)
a	a	k8xC
Bohuslavu	Bohuslava	k1gFnSc4
(	(	kIx(
<g/>
1850	[number]	k4
<g/>
–	–	k?
<g/>
1924	[number]	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>