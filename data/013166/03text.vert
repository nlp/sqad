<p>
<s>
Jako	jako	k8xC	jako
temné	temný	k2eAgNnSc1d1	temné
století	století	k1gNnSc1	století
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
saeculum	saeculum	k1gInSc1	saeculum
obscurum	obscurum	k1gInSc1	obscurum
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
etapa	etapa	k1gFnSc1	etapa
papežství	papežství	k1gNnSc2	papežství
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
středověku	středověk	k1gInSc6	středověk
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
vymezená	vymezený	k2eAgFnSc1d1	vymezená
pontifikáty	pontifikát	k1gInPc4	pontifikát
Sergia	Sergium	k1gNnSc2	Sergium
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
Jana	Jan	k1gMnSc2	Jan
XII	XII	kA	XII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
904	[number]	k4	904
–	–	k?	–
964	[number]	k4	964
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Termín	termín	k1gInSc1	termín
saeculum	saeculum	k1gNnSc1	saeculum
obscurum	obscurum	k1gInSc1	obscurum
zřejmě	zřejmě	k6eAd1	zřejmě
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
použil	použít	k5eAaPmAgMnS	použít
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
vrchní	vrchní	k2eAgMnSc1d1	vrchní
vatikánský	vatikánský	k2eAgMnSc1d1	vatikánský
knihovník	knihovník	k1gMnSc1	knihovník
Caesar	Caesar	k1gMnSc1	Caesar
Baronius	Baronius	k1gMnSc1	Baronius
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
Annales	Annales	k1gInSc4	Annales
ecclesiastici	ecclesiastik	k1gMnPc1	ecclesiastik
(	(	kIx(	(
<g/>
1588	[number]	k4	1588
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Události	událost	k1gFnPc1	událost
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
zhroucení	zhroucení	k1gNnSc6	zhroucení
Franské	franský	k2eAgFnSc2d1	Franská
říše	říš	k1gFnSc2	říš
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Hadriána	Hadrián	k1gMnSc2	Hadrián
II	II	kA	II
<g/>
.	.	kIx.	.
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
i	i	k9	i
papežství	papežství	k1gNnSc1	papežství
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
mocnými	mocný	k2eAgInPc7d1	mocný
<g/>
.	.	kIx.	.
</s>
<s>
Papežové	Papež	k1gMnPc1	Papež
se	se	k3xPyFc4	se
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
stali	stát	k5eAaPmAgMnP	stát
objektem	objekt	k1gInSc7	objekt
manipulací	manipulace	k1gFnPc2	manipulace
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
římských	římský	k2eAgMnPc2d1	římský
aristokratů	aristokrat	k1gMnPc2	aristokrat
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc2	jejich
politických	politický	k2eAgInPc2d1	politický
záměrů	záměr	k1gInPc2	záměr
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
kontrolou	kontrola	k1gFnSc7	kontrola
papežského	papežský	k2eAgInSc2d1	papežský
stolce	stolec	k1gInSc2	stolec
snažili	snažit	k5eAaImAgMnP	snažit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
nadvlády	nadvláda	k1gFnSc2	nadvláda
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc7	jeho
příjmy	příjem	k1gInPc7	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
na	na	k7c4	na
30	[number]	k4	30
papežů	papež	k1gMnPc2	papež
a	a	k8xC	a
vzdoropapežů	vzdoropapež	k1gMnPc2	vzdoropapež
<g/>
.	.	kIx.	.
</s>
<s>
Papežství	papežství	k1gNnSc1	papežství
prožívalo	prožívat	k5eAaImAgNnS	prožívat
hluboký	hluboký	k2eAgInSc4d1	hluboký
úpadek	úpadek	k1gInSc4	úpadek
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
úřad	úřad	k1gInSc4	úřad
zastávali	zastávat	k5eAaImAgMnP	zastávat
muži	muž	k1gMnPc1	muž
i	i	k8xC	i
ženy	žena	k1gFnPc1	žena
nevalné	valný	k2eNgFnSc2d1	nevalná
pověsti	pověst	k1gFnSc2	pověst
i	i	k8xC	i
přímo	přímo	k6eAd1	přímo
zločinci	zločinec	k1gMnPc1	zločinec
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
také	také	k9	také
nazývá	nazývat	k5eAaImIp3nS	nazývat
pornokracií	pornokracie	k1gFnSc7	pornokracie
–	–	k?	–
vládou	vláda	k1gFnSc7	vláda
prostitutek	prostitutka	k1gFnPc2	prostitutka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
XII	XII	kA	XII
<g/>
.	.	kIx.	.
navázal	navázat	k5eAaPmAgInS	navázat
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
německým	německý	k2eAgMnSc7d1	německý
císařem	císař	k1gMnSc7	císař
Otou	Ota	k1gMnSc7	Ota
I.	I.	kA	I.
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
změnil	změnit	k5eAaPmAgMnS	změnit
ochránce	ochránce	k1gMnSc1	ochránce
franské	franský	k2eAgFnSc2d1	Franská
na	na	k7c4	na
německé	německý	k2eAgInPc4d1	německý
a	a	k8xC	a
svázal	svázat	k5eAaPmAgMnS	svázat
papežství	papežství	k1gNnSc4	papežství
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
říší	říš	k1gFnSc7	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Důsledky	důsledek	k1gInPc1	důsledek
==	==	k?	==
</s>
</p>
<p>
<s>
Úpadek	úpadek	k1gInSc1	úpadek
papežství	papežství	k1gNnPc2	papežství
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
uznává	uznávat	k5eAaImIp3nS	uznávat
jako	jako	k9	jako
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
významných	významný	k2eAgInPc2d1	významný
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
velkému	velký	k2eAgNnSc3d1	velké
schizmatu	schizma	k1gNnSc3	schizma
mezi	mezi	k7c7	mezi
pravoslavím	pravoslaví	k1gNnSc7	pravoslaví
a	a	k8xC	a
římským	římský	k2eAgNnSc7d1	římské
křesťanstvím	křesťanství	k1gNnSc7	křesťanství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příbuzná	příbuzný	k2eAgNnPc1d1	příbuzné
hesla	heslo	k1gNnPc1	heslo
==	==	k?	==
</s>
</p>
<p>
<s>
Pornokracie	pornokracie	k1gFnSc1	pornokracie
</s>
</p>
<p>
<s>
Teodora	Teodora	k1gFnSc1	Teodora
I.	I.	kA	I.
Tuskulská	Tuskulský	k2eAgFnSc1d1	Tuskulský
</s>
</p>
<p>
<s>
Marozia	Marozia	k1gFnSc1	Marozia
</s>
</p>
