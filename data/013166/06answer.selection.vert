<s>
Termín	termín	k1gInSc1	termín
saeculum	saeculum	k1gNnSc1	saeculum
obscurum	obscurum	k1gInSc1	obscurum
zřejmě	zřejmě	k6eAd1	zřejmě
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
použil	použít	k5eAaPmAgMnS	použít
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
vrchní	vrchní	k2eAgMnSc1d1	vrchní
vatikánský	vatikánský	k2eAgMnSc1d1	vatikánský
knihovník	knihovník	k1gMnSc1	knihovník
Caesar	Caesar	k1gMnSc1	Caesar
Baronius	Baronius	k1gMnSc1	Baronius
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
Annales	Annales	k1gInSc4	Annales
ecclesiastici	ecclesiastik	k1gMnPc1	ecclesiastik
(	(	kIx(	(
<g/>
1588	[number]	k4	1588
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
