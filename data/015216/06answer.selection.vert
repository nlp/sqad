<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
získala	získat	k5eAaPmAgFnS
doktorát	doktorát	k1gInSc4
v	v	k7c6
oboru	obor	k1gInSc6
ekologie	ekologie	k1gFnSc2
na	na	k7c6
Cornellově	Cornellův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
a	a	k8xC
titul	titul	k1gInSc4
B.	B.	kA
<g/>
S.	S.	kA
na	na	k7c4
Conservation	Conservation	k1gInSc4
of	of	k?
Natural	Natural	k?
Resources	Resources	k1gMnSc1
from	from	k1gMnSc1
Cornell	Cornell	k1gMnSc1
(	(	kIx(
<g/>
1955	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Získala	získat	k5eAaPmAgFnS
ocenění	ocenění	k1gNnSc4
World	Worlda	k1gFnPc2
Wildlife	Wildlif	k1gInSc5
Fund	fund	k1gInSc1
a	a	k8xC
klubu	klub	k1gInSc6
Explorers	Explorersa	k1gFnPc2
Club	club	k1gInSc4
za	za	k7c2
své	svůj	k3xOyFgFnSc2
průkopnické	průkopnický	k2eAgFnSc2d1
práce	práce	k1gFnSc2
v	v	k7c6
ekologii	ekologie	k1gFnSc6
divoké	divoký	k2eAgFnSc2d1
zvěře	zvěř	k1gFnSc2
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
a	a	k8xC
v	v	k7c6
Guatemale	Guatemala	k1gFnSc6
<g/>
.	.	kIx.
</s>