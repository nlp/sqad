<s>
Anne	Anne	k1gFnSc1
LaBastille	LaBastille	k1gFnSc2
</s>
<s>
Anne	Anne	k1gNnSc1
LaBastille	LaBastille	k1gFnSc2
Narození	narození	k1gNnSc2
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1935	#num#	k4
<g/>
Montclair	Montclaira	k1gFnPc2
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2011	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
75	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Plattsburgh	Plattsburgh	k1gInSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Colorado	Colorado	k1gNnSc1
State	status	k1gInSc5
UniversityCornellova	UniversityCornellův	k2eAgFnSc1d1
univerzitaCornell	univerzitaCornelnout	k5eAaPmAgInS
University	universita	k1gFnSc2
College	College	k1gInSc1
of	of	k?
Agriculture	Agricultur	k1gMnSc5
and	and	k?
Life	Life	k1gFnSc3
Sciences	Sciencesa	k1gFnPc2
Povolání	povolání	k1gNnSc2
</s>
<s>
spisovatelka	spisovatelka	k1gFnSc1
<g/>
,	,	kIx,
fotografka	fotografka	k1gFnSc1
a	a	k8xC
ekoložka	ekoložka	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Anne	Anne	k1gFnSc1
LaBastille	LaBastille	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Mlhavé	mlhavý	k2eAgNnSc1d1
ráno	ráno	k1gNnSc1
na	na	k7c6
jezeru	jezero	k1gNnSc6
Twitchell	Twitchell	k1gInSc1
<g/>
,	,	kIx,
říjen	říjen	k1gInSc1
1973	#num#	k4
<g/>
,	,	kIx,
projekt	projekt	k1gInSc1
Documerica	Documerica	k1gFnSc1
<g/>
,	,	kIx,
archiv	archiv	k1gInSc1
NARA	NARA	kA
</s>
<s>
Anne	Anne	k1gFnSc1
LaBastille	LaBastille	k1gFnSc1
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1935	#num#	k4
Montclair	Montclair	k1gInSc1
New	New	k1gFnSc2
Jersey	Jersea	k1gFnSc2
-	-	kIx~
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2011	#num#	k4
Plattsburgh	Plattsburgh	k1gMnSc1
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
byla	být	k5eAaImAgFnS
americká	americký	k2eAgFnSc1d1
spisovatelka	spisovatelka	k1gFnSc1
<g/>
,	,	kIx,
fotografka	fotografka	k1gFnSc1
a	a	k8xC
ekoložka	ekoložka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
autorem	autor	k1gMnSc7
více	hodně	k6eAd2
než	než	k8xS
deseti	deset	k4xCc2
knih	kniha	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
<g/>
:	:	kIx,
Woodswoman	Woodswoman	k1gMnSc1
(	(	kIx(
<g/>
více	hodně	k6eAd2
dílů	díl	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Beyond	Beyond	k1gMnSc1
Black	Black	k1gMnSc1
Bear	Bear	k1gMnSc1
Lake	Lake	k1gFnSc4
<g/>
,	,	kIx,
Assignment	Assignment	k1gInSc4
<g/>
:	:	kIx,
<g/>
Wildlife	Wildlif	k1gMnSc5
a	a	k8xC
Women	Womno	k1gNnPc2
of	of	k?
the	the	k?
Wilderness	Wilderness	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Napsala	napsat	k5eAaPmAgFnS,k5eAaBmAgFnS
více	hodně	k6eAd2
než	než	k8xS
150	#num#	k4
populárních	populární	k2eAgInPc2d1
článků	článek	k1gInPc2
a	a	k8xC
více	hodně	k6eAd2
než	než	k8xS
25	#num#	k4
vědeckých	vědecký	k2eAgFnPc2d1
prací	práce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
získala	získat	k5eAaPmAgFnS
doktorát	doktorát	k1gInSc4
v	v	k7c6
oboru	obor	k1gInSc6
ekologie	ekologie	k1gFnSc2
na	na	k7c6
Cornellově	Cornellův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
a	a	k8xC
titul	titul	k1gInSc4
B.	B.	kA
<g/>
S.	S.	kA
na	na	k7c4
Conservation	Conservation	k1gInSc4
of	of	k?
Natural	Natural	k?
Resources	Resources	k1gMnSc1
from	from	k1gMnSc1
Cornell	Cornell	k1gMnSc1
(	(	kIx(
<g/>
1955	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Získala	získat	k5eAaPmAgFnS
ocenění	ocenění	k1gNnSc4
World	Worlda	k1gFnPc2
Wildlife	Wildlif	k1gInSc5
Fund	fund	k1gInSc1
a	a	k8xC
klubu	klub	k1gInSc6
Explorers	Explorersa	k1gFnPc2
Club	club	k1gInSc4
za	za	k7c2
své	svůj	k3xOyFgFnSc2
průkopnické	průkopnický	k2eAgFnSc2d1
práce	práce	k1gFnSc2
v	v	k7c6
ekologii	ekologie	k1gFnSc6
divoké	divoký	k2eAgFnSc2d1
zvěře	zvěř	k1gFnSc2
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
a	a	k8xC
v	v	k7c6
Guatemale	Guatemala	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
dopisovatelkou	dopisovatelka	k1gFnSc7
pro	pro	k7c4
Sierra	Sierra	k1gFnSc1
Club	club	k1gInSc4
a	a	k8xC
National	National	k1gFnSc4
Geographic	Geographice	k1gFnPc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
přispívala	přispívat	k5eAaImAgFnS
do	do	k7c2
mnoha	mnoho	k4c2
jiných	jiný	k2eAgInPc2d1
časopisů	časopis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získala	získat	k5eAaPmAgFnS
licenci	licence	k1gFnSc4
státu	stát	k1gInSc2
New	New	k1gFnSc2
York	York	k1gInSc1
jako	jako	k8xC,k8xS
průvodce	průvodce	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
a	a	k8xC
nabízela	nabízet	k5eAaImAgFnS
průvodcovské	průvodcovský	k2eAgFnPc4d1
služby	služba	k1gFnPc4
pro	pro	k7c4
turisty	turist	k1gMnPc4
jedoucí	jedoucí	k2eAgInPc1d1
kánoí	kánoe	k1gFnSc7
po	po	k7c4
Adirondacks	Adirondacks	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
více	hodně	k6eAd2
než	než	k8xS
čtyřicet	čtyřicet	k4xCc4
let	léto	k1gNnPc2
uspořádala	uspořádat	k5eAaPmAgFnS
řadu	řada	k1gFnSc4
workshopů	workshop	k1gInPc2
a	a	k8xC
přednášek	přednáška	k1gFnPc2
a	a	k8xC
procaovala	procaovat	k5eAaImAgFnS,k5eAaPmAgFnS,k5eAaBmAgFnS
pro	pro	k7c4
mnoho	mnoho	k4c4
organizací	organizace	k1gFnPc2
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
přírody	příroda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cestovala	cestovat	k5eAaImAgFnS
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
a	a	k8xC
spolupracovala	spolupracovat	k5eAaImAgFnS
s	s	k7c7
mnoha	mnoho	k4c7
neziskovými	ziskový	k2eNgFnPc7d1
organizacemi	organizace	k1gFnPc7
s	s	k7c7
cílem	cíl	k1gInSc7
zmírnit	zmírnit	k5eAaPmF
ničivé	ničivý	k2eAgInPc4d1
účinky	účinek	k1gInPc4
kyselých	kyselý	k2eAgInPc2d1
dešťů	dešť	k1gInPc2
a	a	k8xC
znečištění	znečištění	k1gNnSc4
jezer	jezero	k1gNnPc2
a	a	k8xC
proti	proti	k7c3
likvidaci	likvidace	k1gFnSc3
volně	volně	k6eAd1
žijících	žijící	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
také	také	k6eAd1
známou	známý	k2eAgFnSc7d1
fotografkou	fotografka	k1gFnSc7
divoké	divoký	k2eAgFnSc2d1
zvěře	zvěř	k1gFnSc2
a	a	k8xC
její	její	k3xOp3gFnSc2
práce	práce	k1gFnSc2
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgFnP
v	v	k7c6
mnoha	mnoho	k4c6
publikacích	publikace	k1gFnPc6
přírodě	příroda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
se	se	k3xPyFc4
podílela	podílet	k5eAaImAgFnS
na	na	k7c6
projektu	projekt	k1gInSc6
DOCUMERICA	DOCUMERICA	kA
americké	americký	k2eAgFnSc2d1
agentury	agentura	k1gFnSc2
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
a	a	k8xC
dílo	dílo	k1gNnSc1
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS
se	se	k3xPyFc4
v	v	k7c4
Montclair	Montclair	k1gInSc4
v	v	k7c6
New	New	k1gFnSc6
Jersey	Jersea	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Zemřela	zemřít	k5eAaPmAgFnS
v	v	k7c6
Plattsburghu	Plattsburgh	k1gInSc6
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Projekt	projekt	k1gInSc1
Documerica	Documeric	k1gInSc2
</s>
<s>
Byla	být	k5eAaImAgFnS
součástí	součást	k1gFnSc7
projektu	projekt	k1gInSc2
DOCUMERICA	DOCUMERICA	kA
americké	americký	k2eAgFnSc2d1
agentury	agentura	k1gFnSc2
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
1971-77	1971-77	k4
vznikl	vzniknout	k5eAaPmAgInS
program	program	k1gInSc1
sponzorovaný	sponzorovaný	k2eAgInSc1d1
americkou	americký	k2eAgFnSc7d1
Agenturou	agentura	k1gFnSc7
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
(	(	kIx(
<g/>
EPA	EPA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
úkolem	úkol	k1gInSc7
bylo	být	k5eAaImAgNnS
pořídit	pořídit	k5eAaPmF
„	„	k?
<g/>
fotografický	fotografický	k2eAgInSc4d1
dokument	dokument	k1gInSc4
subjektů	subjekt	k1gInPc2
z	z	k7c2
hlediska	hledisko	k1gNnSc2
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
“	“	k?
na	na	k7c6
území	území	k1gNnSc6
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
EPA	EPA	kA
najala	najmout	k5eAaPmAgFnS
externí	externí	k2eAgInSc4d1
fotografy	fotograf	k1gMnPc4
k	k	k7c3
fotografování	fotografování	k1gNnSc3
objektů	objekt	k1gInPc2
jako	jako	k8xS,k8xC
například	například	k6eAd1
zeměpisné	zeměpisný	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
s	s	k7c7
environmentálními	environmentální	k2eAgInPc7d1
problémy	problém	k1gInPc7
<g/>
,	,	kIx,
EPA	EPA	kA
aktivity	aktivita	k1gFnSc2
a	a	k8xC
každodenní	každodenní	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správa	správa	k1gFnSc1
národních	národní	k2eAgInPc2d1
archivů	archiv	k1gInPc2
a	a	k8xC
záznamů	záznam	k1gInPc2
část	část	k1gFnSc1
tohoto	tento	k3xDgInSc2
katalogu	katalog	k1gInSc2
zdigitalizovala	zdigitalizovat	k5eAaBmAgFnS,k5eAaPmAgFnS,k5eAaImAgFnS
a	a	k8xC
lze	lze	k6eAd1
v	v	k7c6
něm	on	k3xPp3gMnSc6
najít	najít	k5eAaPmF
odkaz	odkaz	k1gInSc4
na	na	k7c4
370	#num#	k4
fotografií	fotografia	k1gFnPc2
autorky	autorka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
LaBastille	LaBastille	k1gInSc4
své	svůj	k3xOyFgFnSc2
fotografie	fotografia	k1gFnSc2
pořizovala	pořizovat	k5eAaImAgFnS
ve	v	k7c6
státě	stát	k1gInSc6
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zobrazovala	zobrazovat	k5eAaImAgNnP
různá	různý	k2eAgNnPc1d1
témata	téma	k1gNnPc1
včetně	včetně	k7c2
přírodních	přírodní	k2eAgFnPc2d1
krás	krása	k1gFnPc2
a	a	k8xC
volně	volně	k6eAd1
žijících	žijící	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
<g/>
,	,	kIx,
problémy	problém	k1gInPc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
růst	růst	k1gInSc4
měst	město	k1gNnPc2
a	a	k8xC
každodenní	každodenní	k2eAgInSc4d1
život	život	k1gInSc4
v	v	k7c6
malých	malý	k2eAgNnPc6d1
městech	město	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Stovky	stovka	k1gFnPc1
jejích	její	k3xOp3gInPc2
snímků	snímek	k1gInPc2
spadají	spadat	k5eAaPmIp3nP,k5eAaImIp3nP
do	do	k7c2
kategorie	kategorie	k1gFnSc2
public	publicum	k1gNnPc2
domain	domain	k1gMnSc1
a	a	k8xC
jsou	být	k5eAaImIp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
na	na	k7c6
úložišti	úložiště	k1gNnSc6
obrázků	obrázek	k1gInPc2
Wikimedia	Wikimedium	k1gNnSc2
Commons	Commonsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Anne	Ann	k1gInPc1
LaBastille	LaBastille	k1gFnSc1
dies	dies	k1gInSc1
at	at	k?
77	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naturalist	Naturalist	k1gMnSc1
inspired	inspired	k1gMnSc1
women	women	k1gInSc4
to	ten	k3xDgNnSc1
explore	explor	k1gInSc5
outdoors	outdoors	k6eAd1
Valerie	Valerie	k1gFnSc2
J.	J.	kA
Nelson	Nelson	k1gMnSc1
<g/>
,	,	kIx,
Los	los	k1gMnSc1
Angeles	Angeles	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
,	,	kIx,
July	Jula	k1gFnSc2
10	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
↑	↑	k?
Guide	Guid	k1gInSc5
to	ten	k3xDgNnSc1
the	the	k?
Anne	Ann	k1gInPc1
LaBastille	LaBastille	k1gFnSc1
Papers	Papers	k1gInSc1
<g/>
,1963	,1963	k4
<g/>
-	-	kIx~
<g/>
2000	#num#	k4
<g/>
,	,	kIx,
Biographical	Biographical	k1gFnSc1
Note	Note	k1gFnSc1
<g/>
,	,	kIx,
Division	Division	k1gInSc1
of	of	k?
Rare	Rare	k1gInSc1
and	and	k?
Manuscript	Manuscript	k2eAgInSc1d1
Collections	Collections	k1gInSc1
<g/>
,	,	kIx,
Cornell	Cornell	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
University	universita	k1gFnPc4
Library	Librara	k1gFnSc2
2004	#num#	k4
<g/>
,	,	kIx,
Retrieved	Retrieved	k1gInSc1
December	December	k1gInSc1
20	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
↑	↑	k?
Cornell	Cornell	k1gInSc1
News	News	k1gInSc1
<g/>
:	:	kIx,
Anne	Anne	k1gInSc1
Labastille	Labastille	k1gFnSc2
<g/>
,	,	kIx,
Press	Press	k1gInSc4
Release	Releasa	k1gFnSc3
April	April	k1gInSc1
22	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
,	,	kIx,
Retrieved	Retrieved	k1gInSc1
December	December	k1gInSc1
20	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
↑	↑	k?
Anne	Annus	k1gMnSc5
LaBastille	LaBastill	k1gMnSc5
<g/>
,	,	kIx,
Advocate	Advocat	k1gMnSc5
<g/>
,	,	kIx,
Author	Author	k1gInSc1
and	and	k?
‘	‘	k?
<g/>
Woodswoman	Woodswoman	k1gMnSc1
<g/>
’	’	k?
of	of	k?
Adirondacks	Adirondacks	k1gInSc1
<g/>
,	,	kIx,
Dies	Dies	k1gInSc1
at	at	k?
75	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dennis	Dennis	k1gInSc1
Hevesi	Hevese	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
City	City	k1gFnSc2
<g/>
,	,	kIx,
N.	N.	kA
<g/>
Y.	Y.	kA
<g/>
,	,	kIx,
Jul	Jula	k1gFnPc2
9	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Retrieved	Retrieved	k1gInSc1
12	#num#	k4
July	Jula	k1gFnSc2
2011	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Anne	Ann	k1gInSc2
LaBastille	LaBastille	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Documerica	Documerica	k1gMnSc1
Project	Project	k1gMnSc1
(	(	kIx(
<g/>
1971	#num#	k4
<g/>
-	-	kIx~
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gMnSc1
Archives	Archives	k1gMnSc1
<g/>
,	,	kIx,
1977	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Fotografie	fotografia	k1gFnPc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1156979935	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1685	#num#	k4
8069	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80002263	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
92012132	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80002263	#num#	k4
</s>
