<s>
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
nezávislém	závislý	k2eNgInSc6d1
státě	stát	k1gInSc6
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Mexiko	Mexiko	k1gNnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
mexické	mexický	k2eAgInPc1d1
Estados	Estadosa	k1gFnPc2
Unidos	Unidos	k1gMnSc1
Mexicanos	Mexicanos	k1gMnSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
HymnaHimno	HymnaHimno	k1gNnSc1
Nacional	Nacional	k1gFnSc2
Mexicano	Mexicana	k1gFnSc5
Geografie	geografie	k1gFnPc5
</s>
<s>
Poloha	poloha	k1gFnSc1
Mexika	Mexiko	k1gNnSc2
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Ciudad	Ciudad	k1gInSc1
de	de	k?
México	México	k1gNnSc1
Rozloha	rozloha	k1gFnSc1
</s>
<s>
1	#num#	k4
964	#num#	k4
375	#num#	k4
km²	km²	k?
(	(	kIx(
<g/>
13	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
)	)	kIx)
z	z	k7c2
toho	ten	k3xDgNnSc2
2,5	2,5	k4
%	%	kIx~
vodní	vodní	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
Nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
</s>
<s>
Citlaltépetl	Citlaltépést	k5eAaPmAgMnS
(	(	kIx(
<g/>
5636	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
−	−	k?
až	až	k9
−	−	k?
h	h	k?
Poloha	poloha	k1gFnSc1
</s>
<s>
23	#num#	k4
<g/>
°	°	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
102	#num#	k4
<g/>
°	°	k?
z.	z.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
Obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
128	#num#	k4
431	#num#	k4
683	#num#	k4
(	(	kIx(
<g/>
11	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
54,3	54,3	k4
ob.	ob.	k?
/	/	kIx~
km²	km²	k?
(	(	kIx(
<g/>
117	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
)	)	kIx)
HDI	HDI	kA
</s>
<s>
▲	▲	k?
0,779	0,779	k4
(	(	kIx(
<g/>
vysoký	vysoký	k2eAgMnSc1d1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
74	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
)	)	kIx)
Jazyk	jazyk	k1gInSc1
</s>
<s>
španělština	španělština	k1gFnSc1
Náboženství	náboženství	k1gNnSc2
</s>
<s>
katolické	katolický	k2eAgInPc1d1
89	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
protestantské	protestantský	k2eAgFnSc2d1
6	#num#	k4
%	%	kIx~
Státní	státní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
Státní	státní	k2eAgInSc1d1
zřízení	zřízení	k1gNnSc4
</s>
<s>
federativní	federativní	k2eAgFnSc1d1
prezidentská	prezidentský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1810	#num#	k4
(	(	kIx(
<g/>
uznána	uznán	k2eAgFnSc1d1
27	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1827	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
nezávislost	nezávislost	k1gFnSc4
na	na	k7c6
Španělsku	Španělsko	k1gNnSc6
<g/>
)	)	kIx)
Prezident	prezident	k1gMnSc1
</s>
<s>
Andrés	Andrés	k6eAd1
Manuel	Manuel	k1gMnSc1
López	López	k1gMnSc1
Obrador	Obrador	k1gMnSc1
Měna	měna	k1gFnSc1
</s>
<s>
Mexické	mexický	k2eAgNnSc1d1
peso	peso	k1gNnSc1
(	(	kIx(
<g/>
MXN	MXN	kA
<g/>
)	)	kIx)
HDP	HDP	kA
<g/>
/	/	kIx~
<g/>
obyv	obyv	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
PPP	PPP	kA
<g/>
)	)	kIx)
</s>
<s>
17	#num#	k4
269	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
USD	USD	kA
(	(	kIx(
<g/>
68	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
)	)	kIx)
Mezinárodní	mezinárodní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
ISO	ISO	kA
3166-1	3166-1	k4
</s>
<s>
484	#num#	k4
MEX	MEX	kA
MX	MX	kA
MPZ	MPZ	kA
</s>
<s>
MEX	MEX	kA
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
+52	+52	k4
Národní	národní	k2eAgInSc1d1
TLD	TLD	kA
</s>
<s>
<g/>
mx	mx	k?
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
<g/>
,	,	kIx,
plným	plný	k2eAgInSc7d1
názvem	název	k1gInSc7
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
mexické	mexický	k2eAgInPc1d1
<g/>
,	,	kIx,
španělsky	španělsky	k6eAd1
Estados	Estados	k1gInPc1
Unidos	Unidos	k2eAgInPc1d1
Mexicanos	Mexicanos	k2eAgInPc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
federativní	federativní	k2eAgFnSc1d1
republika	republika	k1gFnSc1
na	na	k7c6
americkém	americký	k2eAgInSc6d1
kontinentě	kontinent	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geograficky	geograficky	k6eAd1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
drtivou	drtivý	k2eAgFnSc7d1
většinou	většina	k1gFnSc7
území	území	k1gNnPc2
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
,	,	kIx,
geopoliticky	geopoliticky	k6eAd1
bývá	bývat	k5eAaImIp3nS
ale	ale	k9
často	často	k6eAd1
řazeno	řazen	k2eAgNnSc1d1
mezi	mezi	k7c4
státy	stát	k1gInPc4
Střední	střední	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
severu	sever	k1gInSc6
hraničí	hraničit	k5eAaImIp3nP
se	s	k7c7
Spojenými	spojený	k2eAgInPc7d1
státy	stát	k1gInPc7
<g/>
,	,	kIx,
na	na	k7c6
jihu	jih	k1gInSc6
s	s	k7c7
Guatemalou	Guatemala	k1gFnSc7
a	a	k8xC
Belize	Belize	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
západu	západ	k1gInSc2
jsou	být	k5eAaImIp3nP
břehy	břeh	k1gInPc1
Mexika	Mexiko	k1gNnSc2
omývány	omýván	k2eAgMnPc4d1
Tichým	tichý	k2eAgInSc7d1
oceánem	oceán	k1gInSc7
<g/>
,	,	kIx,
východní	východní	k2eAgNnSc1d1
pobřeží	pobřeží	k1gNnSc1
je	být	k5eAaImIp3nS
tvořeno	tvořit	k5eAaImNgNnS
Mexickým	mexický	k2eAgInSc7d1
zálivem	záliv	k1gInSc7
a	a	k8xC
na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
Karibským	karibský	k2eAgNnSc7d1
mořem	moře	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mexiko	Mexiko	k1gNnSc1
má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
1	#num#	k4
972	#num#	k4
550	#num#	k4
kilometrů	kilometr	k1gInPc2
čtverečních	čtvereční	k2eAgInPc2d1
a	a	k8xC
přibližně	přibližně	k6eAd1
128	#num#	k4
milionů	milion	k4xCgInPc2
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
z	z	k7c2
něj	on	k3xPp3gNnSc2
činí	činit	k5eAaImIp3nS
13	#num#	k4
<g/>
.	.	kIx.
největší	veliký	k2eAgFnSc6d3
zemi	zem	k1gFnSc6
na	na	k7c6
světě	svět	k1gInSc6
podle	podle	k7c2
rozlohy	rozloha	k1gFnSc2
a	a	k8xC
10	#num#	k4
<g/>
.	.	kIx.
nejlidnatější	lidnatý	k2eAgInSc1d3
stát	stát	k1gInSc1
planety	planeta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
rovněž	rovněž	k9
o	o	k7c4
největší	veliký	k2eAgFnSc4d3
španělsky	španělsky	k6eAd1
mluvící	mluvící	k2eAgFnSc4d1
zemi	zem	k1gFnSc4
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
federace	federace	k1gFnSc1
skládající	skládající	k2eAgFnSc1d1
se	se	k3xPyFc4
z	z	k7c2
31	#num#	k4
států	stát	k1gInPc2
a	a	k8xC
samostatné	samostatný	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
Ciudad	Ciudad	k1gInSc1
de	de	k?
México	México	k6eAd1
<g/>
,	,	kIx,
hlavního	hlavní	k2eAgNnSc2d1
a	a	k8xC
také	také	k9
největšího	veliký	k2eAgNnSc2d3
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
další	další	k2eAgNnPc4d1
významná	významný	k2eAgNnPc4d1
města	město	k1gNnPc4
patří	patřit	k5eAaImIp3nS
Guadalajara	Guadalajara	k1gFnSc1
<g/>
,	,	kIx,
Monterrey	Monterrey	k1gInPc1
<g/>
,	,	kIx,
Puebla	Puebla	k1gFnSc1
<g/>
,	,	kIx,
Toluca	Toluca	k1gFnSc1
<g/>
,	,	kIx,
Tijuana	Tijuana	k1gFnSc1
<g/>
,	,	kIx,
Ciudad	Ciudad	k1gInSc1
Juárez	Juáreza	k1gFnPc2
a	a	k8xC
León	Leóna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
Mexika	Mexiko	k1gNnSc2
jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
starobylé	starobylý	k2eAgFnPc1d1
<g/>
,	,	kIx,
rozvinuté	rozvinutý	k2eAgFnPc1d1
civilizace	civilizace	k1gFnPc1
se	se	k3xPyFc4
zde	zde	k6eAd1
nacházely	nacházet	k5eAaImAgInP
již	již	k6eAd1
tisíce	tisíc	k4xCgInPc4
let	léto	k1gNnPc2
před	před	k7c7
naším	náš	k3xOp1gInSc7
letopočtem	letopočet	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nejslavnějším	slavný	k2eAgMnPc3d3
patří	patřit	k5eAaImIp3nS
civilizace	civilizace	k1gFnSc1
mayská	mayský	k2eAgFnSc1d1
a	a	k8xC
aztécká	aztécký	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
proběhla	proběhnout	k5eAaPmAgFnS
španělská	španělský	k2eAgFnSc1d1
kolonizace	kolonizace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mexiko	Mexiko	k1gNnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
nezávislým	závislý	k2eNgInSc7d1
státem	stát	k1gInSc7
po	po	k7c6
úspěšné	úspěšný	k2eAgFnSc6d1
mexické	mexický	k2eAgFnSc6d1
válce	válka	k1gFnSc6
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1821	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
následovaly	následovat	k5eAaImAgFnP
velké	velký	k2eAgFnPc4d1
územní	územní	k2eAgFnPc4d1
ztráty	ztráta	k1gFnPc4
ve	v	k7c4
prospěch	prospěch	k1gInSc4
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
,	,	kIx,
na	na	k7c6
severu	sever	k1gInSc6
Mexika	Mexiko	k1gNnSc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
tyto	tento	k3xDgFnPc1
oblasti	oblast	k1gFnPc1
byly	být	k5eAaImAgFnP
obydleny	obydlet	k5eAaPmNgFnP
jen	jen	k6eAd1
velmi	velmi	k6eAd1
řídce	řídce	k6eAd1
<g/>
,	,	kIx,
centrum	centrum	k1gNnSc1
země	zem	k1gFnSc2
se	se	k3xPyFc4
vždy	vždy	k6eAd1
nacházelo	nacházet	k5eAaImAgNnS
na	na	k7c6
jihu	jih	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
politickým	politický	k2eAgInSc7d1
přeryvem	přeryv	k1gInSc7
byly	být	k5eAaImAgFnP
reformy	reforma	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
poskytovaly	poskytovat	k5eAaImAgFnP
ochranu	ochrana	k1gFnSc4
domorodým	domorodý	k2eAgFnPc3d1
komunitám	komunita	k1gFnPc3
a	a	k8xC
omezovaly	omezovat	k5eAaImAgInP
moc	moc	k6eAd1
armády	armáda	k1gFnSc2
a	a	k8xC
církve	církev	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
byly	být	k5eAaImAgInP
zakotveny	zakotvit	k5eAaPmNgInP
v	v	k7c6
ústavě	ústava	k1gFnSc6
z	z	k7c2
roku	rok	k1gInSc2
1857	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgFnPc1
vyvolaly	vyvolat	k5eAaPmAgFnP
francouzskou	francouzský	k2eAgFnSc4d1
intervenci	intervence	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cizí	cizí	k2eAgFnSc1d1
nadvláda	nadvláda	k1gFnSc1
byla	být	k5eAaImAgFnS
odvržena	odvrhnout	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1867	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deset	deset	k4xCc4
let	léto	k1gNnPc2
poté	poté	k6eAd1
moc	moc	k6eAd1
na	na	k7c4
tři	tři	k4xCgFnPc4
dekády	dekáda	k1gFnPc4
uzmul	uzmout	k5eAaPmAgMnS
diktátor	diktátor	k1gMnSc1
Porfirio	Porfirio	k1gMnSc1
Díaz	Díaz	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toho	ten	k3xDgNnSc2
svrhla	svrhnout	k5eAaPmAgFnS
mexická	mexický	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1910	#num#	k4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
je	být	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
základním	základní	k2eAgInSc7d1
pilířem	pilíř	k1gInSc7
mexické	mexický	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
identity	identita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Revoluční	revoluční	k2eAgFnPc1d1
peripetie	peripetie	k1gFnPc1
nakonec	nakonec	k6eAd1
vyústily	vyústit	k5eAaPmAgFnP
v	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
Institucionální	institucionální	k2eAgFnSc2d1
revoluční	revoluční	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
Mexiko	Mexiko	k1gNnSc4
ovládala	ovládat	k5eAaImAgFnS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
žebříčku	žebříček	k1gInSc6
indexu	index	k1gInSc2
lidského	lidský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
se	se	k3xPyFc4
Mexiko	Mexiko	k1gNnSc1
nachází	nacházet	k5eAaImIp3nS
až	až	k9
na	na	k7c4
76	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vede	vést	k5eAaImIp3nS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
na	na	k7c4
něj	on	k3xPp3gNnSc4
často	často	k6eAd1
pohlíženo	pohlížen	k2eAgNnSc4d1
jako	jako	k8xC,k8xS
na	na	k7c4
rozvojovou	rozvojový	k2eAgFnSc4d1
zemi	zem	k1gFnSc4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
jde	jít	k5eAaImIp3nS
o	o	k7c4
16	#num#	k4
<g/>
.	.	kIx.
největší	veliký	k2eAgFnSc4d3
ekonomiku	ekonomika	k1gFnSc4
na	na	k7c6
světě	svět	k1gInSc6
podle	podle	k7c2
nominálního	nominální	k2eAgNnSc2d1
HDP	HDP	kA
a	a	k8xC
11	#num#	k4
<g/>
.	.	kIx.
největší	veliký	k2eAgMnSc1d3
podle	podle	k7c2
HDP	HDP	kA
v	v	k7c6
paritě	parita	k1gFnSc6
kupní	kupní	k2eAgFnSc2d1
síly	síla	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mexiko	Mexiko	k1gNnSc1
bývá	bývat	k5eAaImIp3nS
tudíž	tudíž	k8xC
označováno	označovat	k5eAaImNgNnS
za	za	k7c4
regionální	regionální	k2eAgFnSc4d1
velmoc	velmoc	k1gFnSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
dokonce	dokonce	k9
potenciální	potenciální	k2eAgFnSc4d1
velmoc	velmoc	k1gFnSc4
světovou	světový	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největším	veliký	k2eAgMnSc7d3
ekonomickým	ekonomický	k2eAgMnSc7d1
partnerem	partner	k1gMnSc7
jsou	být	k5eAaImIp3nP
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
,	,	kIx,
s	s	k7c7
nimi	on	k3xPp3gInPc7
a	a	k8xC
Kanadou	Kanada	k1gFnSc7
Mexiko	Mexiko	k1gNnSc1
vytvořilo	vytvořit	k5eAaPmAgNnS
i	i	k9
zónu	zóna	k1gFnSc4
volného	volný	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
vztahy	vztah	k1gInPc4
s	s	k7c7
mocným	mocný	k2eAgMnSc7d1
severním	severní	k2eAgMnSc7d1
sousedem	soused	k1gMnSc7
jsou	být	k5eAaImIp3nP
často	často	k6eAd1
také	také	k9
konfliktní	konfliktní	k2eAgMnSc1d1
<g/>
,	,	kIx,
nejen	nejen	k6eAd1
historicky	historicky	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
kvůli	kvůli	k7c3
řadě	řada	k1gFnSc3
současných	současný	k2eAgInPc2d1
problémů	problém	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
nelegální	legální	k2eNgFnSc1d1
migrace	migrace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
největším	veliký	k2eAgInPc3d3
problémům	problém	k1gInPc3
mexické	mexický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
patří	patřit	k5eAaImIp3nS
velká	velký	k2eAgFnSc1d1
sociální	sociální	k2eAgFnSc1d1
nerovnost	nerovnost	k1gFnSc1
<g/>
,	,	kIx,
značná	značný	k2eAgFnSc1d1
míra	míra	k1gFnSc1
chudoby	chudoba	k1gFnSc2
a	a	k8xC
rozsáhlá	rozsáhlý	k2eAgFnSc1d1
kriminalita	kriminalita	k1gFnSc1
<g/>
,	,	kIx,
zejména	zejména	k9
drogová	drogový	k2eAgFnSc1d1
–	–	k?
od	od	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
vedl	vést	k5eAaImAgInS
konflikt	konflikt	k1gInSc1
mezi	mezi	k7c7
vládou	vláda	k1gFnSc7
a	a	k8xC
drogovými	drogový	k2eAgInPc7d1
kartely	kartel	k1gInPc7
ke	k	k7c3
smrti	smrt	k1gFnSc3
více	hodně	k6eAd2
než	než	k8xS
120	#num#	k4
000	#num#	k4
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Dějiny	dějiny	k1gFnPc1
Mexika	Mexiko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Pyramida	pyramida	k1gFnSc1
v	v	k7c6
předkolumbovském	předkolumbovský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Chichén	Chichén	k1gInSc1
Itzá	Itzé	k1gNnPc4
</s>
<s>
Hernán	Hernán	k2eAgInSc1d1
Cortés	Cortés	k1gInSc1
zatýká	zatýkat	k5eAaImIp3nS
posledního	poslední	k2eAgMnSc4d1
aztéckého	aztécký	k2eAgMnSc4d1
krále	král	k1gMnSc4
Moctezumu	Moctezum	k1gInSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
(	(	kIx(
<g/>
obraz	obraz	k1gInSc1
Jana	Jan	k1gMnSc2
Karla	Karel	k1gMnSc2
Donata	Donat	k1gMnSc2
Van	van	k1gInSc4
Beecqa	Beecqum	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Oblasti	oblast	k1gFnPc1
dnešního	dnešní	k2eAgNnSc2d1
Mexika	Mexiko	k1gNnSc2
byly	být	k5eAaImAgFnP
osídleny	osídlit	k5eAaPmNgFnP
člověkem	člověk	k1gMnSc7
podle	podle	k7c2
archeologických	archeologický	k2eAgInPc2d1
nálezů	nález	k1gInPc2
před	před	k7c7
asi	asi	k9
25	#num#	k4
000	#num#	k4
až	až	k9
30	#num#	k4
000	#num#	k4
lety	léto	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
roku	rok	k1gInSc2
2500	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
nejstarší	starý	k2eAgFnSc1d3
mexická	mexický	k2eAgFnSc1d1
keramika	keramika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
první	první	k4xOgFnSc4
mexickou	mexický	k2eAgFnSc4d1
civilizaci	civilizace	k1gFnSc4
vůbec	vůbec	k9
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
olmécká	olmécký	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olmékové	Olméek	k1gMnPc1
obývali	obývat	k5eAaImAgMnP
především	především	k6eAd1
území	území	k1gNnSc4
kolem	kolem	k7c2
dnešního	dnešní	k2eAgNnSc2d1
města	město	k1gNnSc2
Veracruz	Veracruza	k1gFnPc2
a	a	k8xC
v	v	k7c6
oblasti	oblast	k1gFnSc6
Tabasco	Tabasco	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
roku	rok	k1gInSc2
200	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
město	město	k1gNnSc1
Teotihuacán	Teotihuacán	k2eAgMnSc1d1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
plnilo	plnit	k5eAaImAgNnS
funkce	funkce	k1gFnPc4
náboženského	náboženský	k2eAgMnSc2d1
a	a	k8xC
kulturního	kulturní	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
pozdní	pozdní	k2eAgFnSc2d1
olmécké	olmécký	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
a	a	k8xC
patřilo	patřit	k5eAaImAgNnS
k	k	k7c3
nejvýznamnějším	významný	k2eAgNnPc3d3
městským	městský	k2eAgNnPc3d1
osídlením	osídlení	k1gNnPc3
v	v	k7c6
Amerikách	Amerika	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
8	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
mělo	mít	k5eAaImAgNnS
100	#num#	k4
až	až	k8xS
250	#num#	k4
tisíc	tisíc	k4xCgInSc4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Srdcem	srdce	k1gNnSc7
města	město	k1gNnSc2
byl	být	k5eAaImAgMnS
komplex	komplex	k1gInSc4
několika	několik	k4yIc2
pyramid	pyramida	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
byly	být	k5eAaImAgFnP
nejdůležitější	důležitý	k2eAgFnPc1d3
pyramidy	pyramida	k1gFnPc1
Slunce	slunce	k1gNnSc2
a	a	k8xC
Měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jižních	jižní	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
poloostrova	poloostrov	k1gInSc2
Yucatánu	Yucatán	k1gMnSc6
se	se	k3xPyFc4
kolem	kolem	k6eAd1
2	#num#	k4
<g/>
.	.	kIx.
tisíciletí	tisíciletí	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
objevila	objevit	k5eAaPmAgFnS
další	další	k2eAgFnSc1d1
civilizace	civilizace	k1gFnSc1
–	–	k?
Mayů	May	k1gMnPc2
<g/>
,	,	kIx,
všeobecně	všeobecně	k6eAd1
považována	považován	k2eAgFnSc1d1
za	za	k7c4
nejvyspělejší	vyspělý	k2eAgFnSc4d3
civilizaci	civilizace	k1gFnSc4
předkolumbovské	předkolumbovský	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
s	s	k7c7
vysokou	vysoký	k2eAgFnSc7d1
úrovní	úroveň	k1gFnSc7
matematiky	matematika	k1gFnSc2
<g/>
,	,	kIx,
kalendářem	kalendář	k1gInSc7
a	a	k8xC
logosylabickým	logosylabický	k2eAgNnSc7d1
písmem	písmo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
ze	z	k7c2
silných	silný	k2eAgFnPc2d1
etap	etapa	k1gFnPc2
této	tento	k3xDgFnSc2
civilizace	civilizace	k1gFnSc2
je	být	k5eAaImIp3nS
spjata	spjat	k2eAgFnSc1d1
s	s	k7c7
příchodem	příchod	k1gInSc7
Toltéků	Tolték	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Centrem	centrum	k1gNnSc7
toltécko-mayské	toltécko-mayská	k1gFnSc2
civilizace	civilizace	k1gFnSc2
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
město	město	k1gNnSc1
Chichén	Chichén	k1gInSc1
Itzá	Itzá	k1gFnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
známou	známý	k2eAgFnSc7d1
Kukulkánovou	Kukulkánův	k2eAgFnSc7d1
pyramidou	pyramida	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Konečně	konečně	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
dnešního	dnešní	k2eAgNnSc2d1
Mexika	Mexiko	k1gNnSc2
objevili	objevit	k5eAaPmAgMnP
na	na	k7c6
počátku	počátek	k1gInSc6
14	#num#	k4
<g/>
.	.	kIx.
století	stoletý	k2eAgMnPc1d1
Aztékové	Azték	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
kolem	kolem	k7c2
roku	rok	k1gInSc2
1325	#num#	k4
založili	založit	k5eAaPmAgMnP
město	město	k1gNnSc4
Tenochtitlán	Tenochtitlán	k2eAgInSc4d1
na	na	k7c6
ostrovech	ostrov	k1gInPc6
jezera	jezero	k1gNnSc2
Texcoco	Texcoco	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aztécká	aztécký	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
byla	být	k5eAaImAgFnS
charakterizována	charakterizovat	k5eAaBmNgFnS
důrazem	důraz	k1gInSc7
na	na	k7c6
roli	role	k1gFnSc6
náboženství	náboženství	k1gNnPc2
(	(	kIx(
<g/>
a	a	k8xC
kněží	kněz	k1gMnPc1
<g/>
)	)	kIx)
a	a	k8xC
silnou	silný	k2eAgFnSc7d1
orientací	orientace	k1gFnSc7
na	na	k7c4
válku	válka	k1gFnSc4
a	a	k8xC
expanzi	expanze	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
dva	dva	k4xCgInPc1
jevy	jev	k1gInPc1
byly	být	k5eAaImAgInP
spojené	spojený	k2eAgInPc4d1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
významným	významný	k2eAgMnSc7d1
aztéckým	aztécký	k2eAgMnSc7d1
bohem	bůh	k1gMnSc7
byl	být	k5eAaImAgMnS
Huitzilopochtli	Huitzilopochtle	k1gFnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc4
vyžadoval	vyžadovat	k5eAaImAgMnS
lidské	lidský	k2eAgFnPc4d1
oběti	oběť	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
příchodu	příchod	k1gInSc6
Evropanů	Evropan	k1gMnPc2
dobyl	dobýt	k5eAaPmAgMnS
mezi	mezi	k7c4
roky	rok	k1gInPc4
1519	#num#	k4
a	a	k8xC
1521	#num#	k4
španělský	španělský	k2eAgInSc4d1
conquistador	conquistador	k1gMnSc1
Hernán	Hernán	k2eAgInSc4d1
Cortés	Cortés	k1gInSc4
Aztéckou	aztécký	k2eAgFnSc4d1
říši	říše	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Záhy	záhy	k6eAd1
poté	poté	k6eAd1
obrátili	obrátit	k5eAaPmAgMnP
Španělé	Španěl	k1gMnPc1
pozornost	pozornost	k1gFnSc4
na	na	k7c4
ostatní	ostatní	k2eAgInPc4d1
indiánské	indiánský	k2eAgInPc4d1
státy	stát	k1gInPc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1530	#num#	k4
Španělé	Španěl	k1gMnPc1
ovládli	ovládnout	k5eAaPmAgMnP
dřívějšího	dřívější	k2eAgMnSc4d1
aztéckého	aztécký	k2eAgMnSc4d1
konkurenta	konkurent	k1gMnSc4
Taraskánské	Taraskánský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1523	#num#	k4
<g/>
–	–	k?
<g/>
1697	#num#	k4
dobyli	dobýt	k5eAaPmAgMnP
a	a	k8xC
rozvrátili	rozvrátit	k5eAaPmAgMnP
všechny	všechen	k3xTgInPc4
mayské	mayský	k2eAgInPc4d1
státy	stát	k1gInPc4
na	na	k7c6
jihu	jih	k1gInSc6
Mexika	Mexiko	k1gNnSc2
<g/>
,	,	kIx,
jakkoli	jakkoli	k8xS
vrchol	vrchol	k1gInSc4
měla	mít	k5eAaImAgFnS
mayská	mayský	k2eAgFnSc1d1
civilizace	civilizace	k1gFnSc1
už	už	k6eAd1
dávno	dávno	k6eAd1
za	za	k7c7
sebou	se	k3xPyFc7
<g/>
,	,	kIx,
konec	konec	k1gInSc1
jejího	její	k3xOp3gNnSc2
klasického	klasický	k2eAgNnSc2d1
období	období	k1gNnSc2
bývá	bývat	k5eAaImIp3nS
datován	datován	k2eAgInSc1d1
k	k	k7c3
roku	rok	k1gInSc3
950	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgFnSc1
generace	generace	k1gFnSc1
conquistadorů	conquistador	k1gMnPc2
<g/>
,	,	kIx,
často	často	k6eAd1
velmi	velmi	k6eAd1
násilných	násilný	k2eAgMnPc2d1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
postupně	postupně	k6eAd1
nahrazena	nahradit	k5eAaPmNgFnS
novým	nový	k2eAgInSc7d1
systémem	systém	k1gInSc7
koloniální	koloniální	k2eAgFnSc2d1
správy	správa	k1gFnSc2
v	v	k7c6
podobě	podoba	k1gFnSc6
místokrálovství	místokrálovství	k1gNnSc2
<g/>
,	,	kIx,
generálních	generální	k2eAgInPc2d1
kapitanátů	kapitanát	k1gInPc2
a	a	k8xC
audiencií	audiencie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
encomiendy	encomienda	k1gFnSc2
byl	být	k5eAaImAgInS
nahrazen	nahradit	k5eAaPmNgInS
repartiementem	repartiement	k1gInSc7
a	a	k8xC
konečně	konečně	k6eAd1
haciendou	hacienda	k1gFnSc7
<g/>
,	,	kIx,
orientovanou	orientovaný	k2eAgFnSc7d1
na	na	k7c6
produkci	produkce	k1gFnSc6
koloniálního	koloniální	k2eAgNnSc2d1
zboží	zboží	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1535	#num#	k4
založili	založit	k5eAaPmAgMnP
Španělé	Španěl	k1gMnPc1
svoji	svůj	k3xOyFgFnSc4
kolonii	kolonie	k1gFnSc4
Nové	Nové	k2eAgNnSc1d1
Španělsko	Španělsko	k1gNnSc1
s	s	k7c7
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
Ciudad	Ciudad	k1gInSc1
de	de	k?
México	México	k6eAd1
na	na	k7c6
místě	místo	k1gNnSc6
hlavního	hlavní	k2eAgNnSc2d1
aztéckého	aztécký	k2eAgNnSc2d1
města	město	k1gNnSc2
Tenochtitlán	Tenochtitlán	k2eAgInSc1d1
přímo	přímo	k6eAd1
na	na	k7c6
území	území	k1gNnSc6
vysušeného	vysušený	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katolická	katolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
silně	silně	k6eAd1
působila	působit	k5eAaImAgFnS
na	na	k7c4
domorodé	domorodý	k2eAgNnSc4d1
obyvatelstvo	obyvatelstvo	k1gNnSc4
a	a	k8xC
sehrávala	sehrávat	k5eAaImAgFnS
centrální	centrální	k2eAgFnSc4d1
roli	role	k1gFnSc4
v	v	k7c6
rodící	rodící	k2eAgFnSc6d1
se	se	k3xPyFc4
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koloniální	koloniální	k2eAgFnSc1d1
expanze	expanze	k1gFnSc1
pokračovala	pokračovat	k5eAaImAgFnS
během	během	k7c2
celého	celý	k2eAgNnSc2d1
16	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
především	především	k9
směrem	směr	k1gInSc7
na	na	k7c4
sever	sever	k1gInSc4
do	do	k7c2
oblastí	oblast	k1gFnPc2
dnešních	dnešní	k2eAgInPc2d1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
a	a	k8xC
dále	daleko	k6eAd2
na	na	k7c4
jih	jih	k1gInSc4
do	do	k7c2
Střední	střední	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koloniální	koloniální	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
charakterizovalo	charakterizovat	k5eAaBmAgNnS
rasově	rasově	k6eAd1
založené	založený	k2eAgNnSc1d1
a	a	k8xC
segregační	segregační	k2eAgNnSc1d1
rozdělení	rozdělení	k1gNnSc1
společnosti	společnost	k1gFnSc2
na	na	k7c4
Republiku	republika	k1gFnSc4
Španělů	Španěl	k1gMnPc2
a	a	k8xC
Republiku	republika	k1gFnSc4
indiánů	indián	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obou	dva	k4xCgFnPc6
platily	platit	k5eAaImAgInP
rozdílné	rozdílný	k2eAgInPc1d1
zákony	zákon	k1gInPc1
a	a	k8xC
Republika	republika	k1gFnSc1
indiánů	indián	k1gMnPc2
byla	být	k5eAaImAgFnS
ve	v	k7c6
všem	všecek	k3xTgNnSc6
podřízena	podřízen	k2eAgFnSc1d1
republice	republika	k1gFnSc6
Španělů	Španěl	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Miguel	Miguel	k1gMnSc1
Hidalgo	Hidalgo	k1gMnSc1
<g/>
,	,	kIx,
hrdina	hrdina	k1gMnSc1
války	válka	k1gFnSc2
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
</s>
<s>
V	v	k7c6
první	první	k4xOgFnSc6
fázi	fáze	k1gFnSc6
kolonizace	kolonizace	k1gFnSc2
se	se	k3xPyFc4
Španělé	Španěl	k1gMnPc1
soustředili	soustředit	k5eAaPmAgMnP
na	na	k7c4
těžbu	těžba	k1gFnSc4
drahých	drahý	k2eAgInPc2d1
kovů	kov	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naleziště	naleziště	k1gNnSc1
v	v	k7c6
Zacatecas	Zacatecas	k1gMnSc1
(	(	kIx(
<g/>
1548	#num#	k4
<g/>
)	)	kIx)
poskytovalo	poskytovat	k5eAaImAgNnS
Španělům	Španěl	k1gMnPc3
obrovské	obrovský	k2eAgNnSc4d1
množství	množství	k1gNnSc4
stříbra	stříbro	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
ochranu	ochrana	k1gFnSc4
bylo	být	k5eAaImAgNnS
nutné	nutný	k2eAgNnSc1d1
vynakládat	vynakládat	k5eAaImF
nemalé	malý	k2eNgNnSc4d1
úsilí	úsilí	k1gNnSc4
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
v	v	k7c6
oblasti	oblast	k1gFnSc6
žili	žít	k5eAaImAgMnP
nepřátelští	přátelský	k2eNgMnPc1d1
Chichimékové	Chichiméek	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
přepadali	přepadat	k5eAaImAgMnP
španělské	španělský	k2eAgInPc4d1
konvoje	konvoj	k1gInPc4
se	s	k7c7
stříbrem	stříbro	k1gNnSc7
a	a	k8xC
ohrožovali	ohrožovat	k5eAaImAgMnP
španělské	španělský	k2eAgInPc4d1
doly	dol	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
letech	let	k1gInPc6
1650	#num#	k4
<g/>
–	–	k?
<g/>
1750	#num#	k4
však	však	k8xC
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
úpadku	úpadek	k1gInSc3
těžby	těžba	k1gFnSc2
stříbra	stříbro	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
důsledku	důsledek	k1gInSc6
převahy	převaha	k1gFnSc2
konkurenční	konkurenční	k2eAgFnSc2d1
těžby	těžba	k1gFnSc2
v	v	k7c6
Peru	Peru	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
tím	ten	k3xDgMnSc7
přišel	přijít	k5eAaPmAgInS
v	v	k7c6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
celkový	celkový	k2eAgInSc4d1
úpadek	úpadek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
něj	on	k3xPp3gMnSc4
se	se	k3xPyFc4
koloniální	koloniální	k2eAgFnSc1d1
správa	správa	k1gFnSc1
snažila	snažit	k5eAaImAgFnS
v	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
reagovat	reagovat	k5eAaBmF
osvícenskými	osvícenský	k2eAgFnPc7d1
reformami	reforma	k1gFnPc7
–	–	k?
vznikly	vzniknout	k5eAaPmAgFnP
nové	nový	k2eAgFnPc1d1
správní	správní	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
nazvané	nazvaný	k2eAgFnSc2d1
intendencie	intendencie	k1gFnSc2
(	(	kIx(
<g/>
1786	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
intendantem	intendant	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
měl	mít	k5eAaImAgInS
šířit	šířit	k5eAaImF
technologické	technologický	k2eAgFnPc4d1
novinky	novinka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Španělsko	Španělsko	k1gNnSc1
však	však	k8xC
celkově	celkově	k6eAd1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
zaostávalo	zaostávat	k5eAaImAgNnS
a	a	k8xC
vyklízelo	vyklízet	k5eAaImAgNnS
své	svůj	k3xOyFgFnPc4
pozice	pozice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místní	místní	k2eAgFnSc2d1
mexické	mexický	k2eAgFnSc2d1
elity	elita	k1gFnSc2
se	se	k3xPyFc4
postupně	postupně	k6eAd1
přeorientovaly	přeorientovat	k5eAaPmAgFnP
na	na	k7c6
Francii	Francie	k1gFnSc6
a	a	k8xC
Británii	Británie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1810	#num#	k4
vznikla	vzniknout	k5eAaPmAgFnS
ve	v	k7c6
městě	město	k1gNnSc6
Queretáru	Queretár	k1gInSc2
spiklenecká	spiklenecký	k2eAgFnSc1d1
junta	junta	k1gFnSc1
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
připravit	připravit	k5eAaPmF
povstání	povstání	k1gNnSc1
proti	proti	k7c3
Španělům	Španěl	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
juntě	junta	k1gFnSc3
se	se	k3xPyFc4
mimo	mimo	k6eAd1
kreolských	kreolský	k2eAgMnPc2d1
politiků	politik	k1gMnPc2
a	a	k8xC
úředníků	úředník	k1gMnPc2
přidal	přidat	k5eAaPmAgMnS
také	také	k9
kněz	kněz	k1gMnSc1
Miguel	Miguel	k1gMnSc1
Hidalgo	Hidalgo	k1gMnSc1
y	y	k?
Costilla	Costilla	k1gMnSc1
<g/>
.	.	kIx.
16	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
1810	#num#	k4
vydal	vydat	k5eAaPmAgInS
manifest	manifest	k1gInSc1
nazvaný	nazvaný	k2eAgInSc1d1
Provolání	provolání	k1gNnSc4
z	z	k7c2
Dolores	Doloresa	k1gFnPc2
(	(	kIx(
<g/>
Grito	Grito	k1gNnSc1
de	de	k?
Dolores	Dolores	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jímž	jenž	k3xRgInSc7
vyhlásil	vyhlásit	k5eAaPmAgMnS
válku	válka	k1gFnSc4
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Hidalgo	Hidalgo	k1gMnSc1
byl	být	k5eAaImAgMnS
popraven	popravit	k5eAaPmNgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
místním	místní	k2eAgFnPc3d1
elitám	elita	k1gFnPc3
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
podařilo	podařit	k5eAaPmAgNnS
nadvládu	nadvláda	k1gFnSc4
Evropanů	Evropan	k1gMnPc2
setřást	setřást	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvažovalo	uvažovat	k5eAaImAgNnS
se	se	k3xPyFc4
sice	sice	k8xC
o	o	k7c4
možnosti	možnost	k1gFnPc4
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
do	do	k7c2
čela	čelo	k1gNnSc2
nově	nově	k6eAd1
vzniklé	vzniklý	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
postavil	postavit	k5eAaPmAgMnS
opět	opět	k6eAd1
španělský	španělský	k2eAgInSc4d1
rod	rod	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
generál	generál	k1gMnSc1
Agustín	Agustín	k1gMnSc1
de	de	k?
Iturbide	Iturbid	k1gInSc5
využil	využít	k5eAaPmAgInS
svoje	svůj	k3xOyFgNnSc4
postavení	postavení	k1gNnSc4
a	a	k8xC
podporu	podpora	k1gFnSc4
v	v	k7c6
armádě	armáda	k1gFnSc6
a	a	k8xC
prohlásil	prohlásit	k5eAaPmAgMnS
se	s	k7c7
prvním	první	k4xOgMnSc7
mexickým	mexický	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Vůči	vůči	k7c3
němu	on	k3xPp3gInSc3
se	se	k3xPyFc4
však	však	k9
zdvihla	zdvihnout	k5eAaPmAgFnS
silná	silný	k2eAgFnSc1d1
republikánská	republikánský	k2eAgFnSc1d1
opozice	opozice	k1gFnSc1
vedená	vedený	k2eAgFnSc1d1
Antonio	Antonio	k1gMnSc1
Lopézem	Lopéz	k1gMnSc7
de	de	k?
Santa	Santa	k1gFnSc1
Anna	Anna	k1gFnSc1
a	a	k8xC
Iturbe	Iturb	k1gInSc5
v	v	k7c6
roce	rok	k1gInSc6
1824	#num#	k4
skončil	skončit	k5eAaPmAgMnS
na	na	k7c6
popravišti	popraviště	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikla	vzniknout	k5eAaPmAgFnS
republika	republika	k1gFnSc1
a	a	k8xC
jejím	její	k3xOp3gNnSc7
prvním	první	k4xOgMnSc6
prezidentem	prezident	k1gMnSc7
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
Guadalupe	Guadalup	k1gInSc5
Victoria	Victorium	k1gNnPc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klíčovou	klíčový	k2eAgFnSc7d1
osobností	osobnost	k1gFnSc7
republiky	republika	k1gFnSc2
zůstal	zůstat	k5eAaPmAgInS
Lopéz	Lopéza	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Situace	situace	k1gFnSc1
republiky	republika	k1gFnSc2
však	však	k9
byla	být	k5eAaImAgFnS
těžká	těžký	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evropské	evropský	k2eAgFnSc6d1
mocnosti	mocnost	k1gFnSc6
ji	on	k3xPp3gFnSc4
neuznávaly	uznávat	k5eNaImAgInP
<g/>
,	,	kIx,
severní	severní	k2eAgFnSc6d1
a	a	k8xC
jižní	jižní	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
žádaly	žádat	k5eAaImAgInP
odtržení	odtržení	k1gNnSc4
<g/>
,	,	kIx,
jichž	jenž	k3xRgMnPc2
také	také	k6eAd1
dosahovaly	dosahovat	k5eAaImAgInP
(	(	kIx(
<g/>
jih	jih	k1gInSc1
již	již	k6eAd1
na	na	k7c6
počátku	počátek	k1gInSc6
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
vznikla	vzniknout	k5eAaPmAgFnS
takto	takto	k6eAd1
Federativní	federativní	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Střední	střední	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
Texas	Texas	k1gInSc1
vyhlásil	vyhlásit	k5eAaPmAgInS
nezávislost	nezávislost	k1gFnSc4
roku	rok	k1gInSc2
1836	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
puč	puč	k1gInSc4
střídal	střídat	k5eAaImAgMnS
puč	puč	k1gInSc4
–	–	k?
sám	sám	k3xTgMnSc1
Lopéz	Lopéza	k1gFnPc2
se	se	k3xPyFc4
do	do	k7c2
úřadu	úřad	k1gInSc2
desetkrát	desetkrát	k6eAd1
vrátil	vrátit	k5eAaPmAgMnS
<g/>
,	,	kIx,
často	často	k6eAd1
právě	právě	k6eAd1
pučem	puč	k1gInSc7
<g/>
,	,	kIx,
a	a	k8xC
vládl	vládnout	k5eAaImAgMnS
v	v	k7c6
zásadě	zásada	k1gFnSc6
jako	jako	k9
diktátor	diktátor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krize	krize	k1gFnSc1
vyvrcholila	vyvrcholit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1846	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vyhlásily	vyhlásit	k5eAaPmAgInP
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Mexiku	Mexiko	k1gNnSc6
válku	válka	k1gFnSc4
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
začlenění	začlenění	k1gNnSc1
severních	severní	k2eAgFnPc2d1
provincií	provincie	k1gFnPc2
Mexika	Mexiko	k1gNnSc2
do	do	k7c2
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
v	v	k7c6
tomto	tento	k3xDgInSc6
konfliktu	konflikt	k1gInSc6
(	(	kIx(
<g/>
mexicko-americká	mexicko-americký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
)	)	kIx)
nebylo	být	k5eNaImAgNnS
Mexiko	Mexiko	k1gNnSc1
úspěšné	úspěšný	k2eAgNnSc1d1
a	a	k8xC
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
důsledku	důsledek	k1gInSc6
ztratilo	ztratit	k5eAaPmAgNnS
velká	velký	k2eAgNnPc4d1
území	území	k1gNnSc4
na	na	k7c6
severu	sever	k1gInSc6
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
Horní	horní	k2eAgFnSc1d1
Kalifornie	Kalifornie	k1gFnSc1
<g/>
,	,	kIx,
Nové	Nové	k2eAgNnSc1d1
Mexiko	Mexiko	k1gNnSc1
a	a	k8xC
další	další	k2eAgNnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poprava	poprava	k1gFnSc1
císaře	císař	k1gMnSc2
Maxmiliána	Maxmilián	k1gMnSc2
na	na	k7c6
obraze	obraz	k1gInSc6
Édouarda	Édouard	k1gMnSc2
Maneta	Manet	k1gMnSc2
</s>
<s>
Porážka	porážka	k1gFnSc1
Mexika	Mexiko	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1847	#num#	k4
dala	dát	k5eAaPmAgFnS
vzejít	vzejít	k5eAaPmF
tzv.	tzv.	kA
druhé	druhý	k4xOgFnSc6
republice	republika	k1gFnSc6
a	a	k8xC
zejména	zejména	k9
posílila	posílit	k5eAaPmAgFnS
liberální	liberální	k2eAgFnSc4d1
opozici	opozice	k1gFnSc4
vůči	vůči	k7c3
lopézovskému	lopézovský	k2eAgInSc3d1
režimu	režim	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
liberálové	liberál	k1gMnPc1
dostali	dostat	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1855	#num#	k4
k	k	k7c3
vládě	vláda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc7
nová	nový	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1857	#num#	k4
však	však	k9
vyvolala	vyvolat	k5eAaPmAgFnS
velký	velký	k2eAgInSc4d1
odpor	odpor	k1gInSc4
v	v	k7c6
církvi	církev	k1gFnSc6
a	a	k8xC
vůbec	vůbec	k9
v	v	k7c6
konzervativním	konzervativní	k2eAgInSc6d1
táboře	tábor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odsoudil	odsoudit	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
i	i	k8xC
papež	papež	k1gMnSc1
Pius	Pius	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záhy	záhy	k6eAd1
vypukla	vypuknout	k5eAaPmAgFnS
tříletá	tříletý	k2eAgFnSc1d1
občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Republikány	republikán	k1gMnPc7
vedl	vést	k5eAaImAgMnS
Benito	Benit	k2eAgNnSc4d1
Juárez	Juárez	k1gMnSc1
<g/>
,	,	kIx,
první	první	k4xOgMnSc1
prezident	prezident	k1gMnSc1
v	v	k7c6
Americe	Amerika	k1gFnSc6
indiánského	indiánský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konzervativci	konzervativec	k1gMnPc1
se	se	k3xPyFc4
rozhodli	rozhodnout	k5eAaPmAgMnP
svrhnout	svrhnout	k5eAaPmF
republiku	republika	k1gFnSc4
a	a	k8xC
nahradit	nahradit	k5eAaPmF
ji	on	k3xPp3gFnSc4
konstituční	konstituční	k2eAgFnSc4d1
monarchií	monarchie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podporu	podpor	k1gInSc2
hledali	hledat	k5eAaImAgMnP
zvláště	zvláště	k6eAd1
na	na	k7c6
francouzském	francouzský	k2eAgInSc6d1
dvoře	dvůr	k1gInSc6
<g/>
,	,	kIx,
u	u	k7c2
Napoleona	Napoleon	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záminkou	záminka	k1gFnSc7
pro	pro	k7c4
intervenci	intervence	k1gFnSc4
Evropanů	Evropan	k1gMnPc2
byly	být	k5eAaImAgInP
mexické	mexický	k2eAgInPc1d1
dluhy	dluh	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Mexiku	Mexiko	k1gNnSc6
se	se	k3xPyFc4
vylodila	vylodit	k5eAaPmAgFnS
vojska	vojsko	k1gNnSc2
„	„	k?
<g/>
věřitelských	věřitelský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
“	“	k?
Velké	velká	k1gFnPc4
Británie	Británie	k1gFnSc2
<g/>
,	,	kIx,
Španělska	Španělsko	k1gNnSc2
a	a	k8xC
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
Britové	Brit	k1gMnPc1
a	a	k8xC
Španělé	Španěl	k1gMnPc1
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
stáhli	stáhnout	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzi	Francouz	k1gMnPc1
dobyli	dobýt	k5eAaPmAgMnP
většinu	většina	k1gFnSc4
území	území	k1gNnSc2
<g/>
,	,	kIx,
republiku	republika	k1gFnSc4
svrhli	svrhnout	k5eAaPmAgMnP
a	a	k8xC
našli	najít	k5eAaPmAgMnP
pro	pro	k7c4
Mexiko	Mexiko	k1gNnSc4
panovníka	panovník	k1gMnSc2
–	–	k?
bratra	bratr	k1gMnSc4
rakouského	rakouský	k2eAgMnSc4d1
císaře	císař	k1gMnSc4
Františka	František	k1gMnSc4
Josefa	Josef	k1gMnSc4
I.	I.	kA
arcivévodu	arcivévoda	k1gMnSc4
Ferdinanda	Ferdinand	k1gMnSc4
Maxmiliána	Maxmilián	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Francouzská	francouzský	k2eAgFnSc1d1
intervence	intervence	k1gFnSc1
v	v	k7c6
Mexiku	Mexiko	k1gNnSc6
se	se	k3xPyFc4
ale	ale	k9
nelíbila	líbit	k5eNaImAgFnS
Spojeným	spojený	k2eAgInPc3d1
státům	stát	k1gInPc3
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
porušení	porušení	k1gNnSc4
Monroeovy	Monroeův	k2eAgFnSc2d1
doktríny	doktrína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc4
tak	tak	k6eAd1
začaly	začít	k5eAaPmAgFnP
podporovat	podporovat	k5eAaImF
Juaréze	Juaréza	k1gFnSc3
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
uchytil	uchytit	k5eAaPmAgMnS
na	na	k7c6
severu	sever	k1gInSc6
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ferdinand	Ferdinand	k1gMnSc1
Maxmilián	Maxmilián	k1gMnSc1
nakonec	nakonec	k6eAd1
ztratil	ztratit	k5eAaPmAgMnS
přízeň	přízeň	k1gFnSc4
i	i	k8xC
Napoleona	Napoleon	k1gMnSc4
III	III	kA
<g/>
.	.	kIx.
kterého	který	k3yRgMnSc4,k3yQgMnSc4,k3yIgMnSc4
začala	začít	k5eAaPmAgFnS
trápit	trápit	k5eAaImF
rostoucí	rostoucí	k2eAgFnSc4d1
moc	moc	k1gFnSc4
Pruska	Prusko	k1gNnSc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
a	a	k8xC
z	z	k7c2
Mexika	Mexiko	k1gNnSc2
stáhl	stáhnout	k5eAaPmAgMnS
všechna	všechen	k3xTgNnPc4
svá	svůj	k3xOyFgNnPc4
vojska	vojsko	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osud	osud	k1gInSc1
císaře	císař	k1gMnSc4
byl	být	k5eAaImAgInS
tragický	tragický	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1867	#num#	k4
byl	být	k5eAaImAgMnS
v	v	k7c6
Queretáru	Queretár	k1gInSc6
zajat	zajat	k2eAgMnSc1d1
a	a	k8xC
popraven	popraven	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Roku	rok	k1gInSc2
1867	#num#	k4
tak	tak	k9
vznikla	vzniknout	k5eAaPmAgFnS
třetí	třetí	k4xOgFnSc1
republika	republika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpočátku	zpočátku	k6eAd1
byla	být	k5eAaImAgFnS
značně	značně	k6eAd1
liberální	liberální	k2eAgFnSc1d1
<g/>
,	,	kIx,
avšak	avšak	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1877	#num#	k4
se	s	k7c7
prezidentem	prezident	k1gMnSc7
stal	stát	k5eAaPmAgMnS
Porfirio	Porfirio	k1gMnSc1
Díaz	Díaz	k1gMnSc1
<g/>
,	,	kIx,
kterému	který	k3yQgMnSc3,k3yIgMnSc3,k3yRgMnSc3
<g/>
,	,	kIx,
ač	ač	k8xS
liberálovi	liberál	k1gMnSc3
<g/>
,	,	kIx,
se	se	k3xPyFc4
zdařilo	zdařit	k5eAaPmAgNnS
získat	získat	k5eAaPmF
podporu	podpora	k1gFnSc4
konzervativních	konzervativní	k2eAgMnPc2d1
zástupců	zástupce	k1gMnPc2
církve	církev	k1gFnSc2
<g/>
,	,	kIx,
velkostatkářů	velkostatkář	k1gMnPc2
<g/>
,	,	kIx,
armády	armáda	k1gFnSc2
a	a	k8xC
zahraničního	zahraniční	k2eAgNnSc2d1
(	(	kIx(
<g/>
především	především	k6eAd1
amerického	americký	k2eAgInSc2d1
<g/>
)	)	kIx)
kapitálu	kapitál	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
intenzivně	intenzivně	k6eAd1
investoval	investovat	k5eAaBmAgMnS
v	v	k7c6
Mexiku	Mexiko	k1gNnSc6
do	do	k7c2
těžby	těžba	k1gFnSc2
ropy	ropa	k1gFnSc2
a	a	k8xC
stavby	stavba	k1gFnSc2
železnic	železnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Církev	církev	k1gFnSc1
opět	opět	k6eAd1
získala	získat	k5eAaPmAgFnS
vliv	vliv	k1gInSc4
nad	nad	k7c7
mexickým	mexický	k2eAgNnSc7d1
školstvím	školství	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díaz	Díaz	k1gInSc1
vládl	vládnout	k5eAaImAgInS
nepřetržitě	přetržitě	k6eNd1
27	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
v	v	k7c6
zásadě	zásada	k1gFnSc6
diktátorským	diktátorský	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
politika	politika	k1gFnSc1
byla	být	k5eAaImAgFnS
ekonomicky	ekonomicky	k6eAd1
liberální	liberální	k2eAgFnSc1d1
<g/>
,	,	kIx,
politicky	politicky	k6eAd1
konzervativní	konzervativní	k2eAgFnSc1d1
a	a	k8xC
usilující	usilující	k2eAgFnSc1d1
o	o	k7c4
industrializaci	industrializace	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
éra	éra	k1gFnSc1
bývá	bývat	k5eAaImIp3nS
nazývána	nazývat	k5eAaImNgFnS
porfiriát	porfiriát	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
ale	ale	k9
proti	proti	k7c3
Díazovi	Díaz	k1gMnSc3
a	a	k8xC
jeho	jeho	k3xOp3gMnPc3
stoupencům	stoupenec	k1gMnPc3
zformovala	zformovat	k5eAaPmAgFnS
nová	nový	k2eAgFnSc1d1
liberální	liberální	k2eAgFnSc1d1
a	a	k8xC
postupně	postupně	k6eAd1
i	i	k9
socialistická	socialistický	k2eAgFnSc1d1
opozice	opozice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díaz	Díaz	k1gInSc1
veřejně	veřejně	k6eAd1
přislíbil	přislíbit	k5eAaPmAgInS
demokratické	demokratický	k2eAgFnPc4d1
volby	volba	k1gFnPc4
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yQgFnPc6,k3yRgFnPc6,k3yIgFnPc6
nebude	být	k5eNaImBp3nS
kandidovat	kandidovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slib	slib	k1gInSc4
však	však	k9
nesplnil	splnit	k5eNaPmAgInS
a	a	k8xC
favorita	favorit	k1gMnSc2
voleb	volba	k1gFnPc2
vykázal	vykázat	k5eAaPmAgInS
do	do	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
zapůsobilo	zapůsobit	k5eAaPmAgNnS
jako	jako	k9
rozbuška	rozbuška	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opozice	opozice	k1gFnSc1
vyhlásila	vyhlásit	k5eAaPmAgFnS
20	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1910	#num#	k4
povstání	povstání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
byla	být	k5eAaImAgFnS
zahájena	zahájit	k5eAaPmNgFnS
mexická	mexický	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
jejího	její	k3xOp3gNnSc2
čela	čelo	k1gNnSc2
se	se	k3xPyFc4
postavil	postavit	k5eAaPmAgInS
Francisco	Francisco	k6eAd1
Madero	Madero	k1gNnSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
bývá	bývat	k5eAaImIp3nS
označován	označovat	k5eAaImNgInS
za	za	k7c2
liberála	liberál	k1gMnSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
k	k	k7c3
jeho	jeho	k3xOp3gFnSc3
rétorice	rétorika	k1gFnSc3
patřil	patřit	k5eAaImAgInS
i	i	k9
boj	boj	k1gInSc1
proti	proti	k7c3
zahraničnímu	zahraniční	k2eAgInSc3d1
kapitálu	kapitál	k1gInSc3
a	a	k8xC
pozemková	pozemkový	k2eAgFnSc1d1
reforma	reforma	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Revoluční	revoluční	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
poměrně	poměrně	k6eAd1
rychle	rychle	k6eAd1
a	a	k8xC
snadno	snadno	k6eAd1
zvítězily	zvítězit	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
1911	#num#	k4
byl	být	k5eAaImAgInS
Madero	Madero	k1gNnSc4
zvolen	zvolit	k5eAaPmNgMnS
novým	nový	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Bojovníci	bojovník	k1gMnPc1
Mexické	mexický	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
</s>
<s>
Madero	Madero	k1gNnSc1
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgMnS
v	v	k7c6
čele	čelo	k1gNnSc6
země	zem	k1gFnSc2
realizovat	realizovat	k5eAaBmF
pozemkovou	pozemkový	k2eAgFnSc4d1
reformu	reforma	k1gFnSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
čelil	čelit	k5eAaImAgInS
kritice	kritika	k1gFnSc3
zprava	zprava	k6eAd1
i	i	k9
zleva	zleva	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
ho	on	k3xPp3gMnSc4
smetl	smetnout	k5eAaPmAgInS
pravicový	pravicový	k2eAgInSc1d1
vojenský	vojenský	k2eAgInSc1d1
převrat	převrat	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začátkem	začátkem	k7c2
roku	rok	k1gInSc2
1913	#num#	k4
byl	být	k5eAaImAgInS
zavražděn	zavraždit	k5eAaPmNgMnS
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
na	na	k7c4
příkaz	příkaz	k1gInSc4
vojáka	voják	k1gMnSc2
a	a	k8xC
konzervativního	konzervativní	k2eAgMnSc2d1
politika	politik	k1gMnSc2
Victoriano	Victoriana	k1gFnSc5
Huerty	Huerta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
také	také	k9
postavil	postavit	k5eAaPmAgMnS
do	do	k7c2
čela	čelo	k1gNnSc2
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpor	odpor	k1gInSc1
proti	proti	k7c3
Huertovi	Huert	k1gMnSc3
je	být	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
za	za	k7c4
druhou	druhý	k4xOgFnSc4
fázi	fáze	k1gFnSc4
revoluce	revoluce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opozice	opozice	k1gFnSc1
měla	mít	k5eAaImAgFnS
tentokrát	tentokrát	k6eAd1
více	hodně	k6eAd2
center	centrum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednomu	jeden	k4xCgMnSc3
dominoval	dominovat	k5eAaImAgMnS
guvernér	guvernér	k1gMnSc1
státu	stát	k1gInSc2
Coahuila	Coahuila	k1gFnSc1
Venustiano	Venustiana	k1gFnSc5
Carranza	Carranz	k1gMnSc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
Konstitucionální	konstitucionální	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dalšímu	další	k2eAgMnSc3d1
voják	voják	k1gMnSc1
Álvaro	Álvara	k1gFnSc5
Obregón	Obregón	k1gInSc1
a	a	k8xC
levicovým	levicový	k2eAgFnPc3d1
skupinám	skupina	k1gFnPc3
velel	velet	k5eAaImAgInS
Pancho	Pancha	k1gFnSc5
Villa	Villa	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc4d1
centrum	centrum	k1gNnSc1
bylo	být	k5eAaImAgNnS
na	na	k7c6
jihu	jih	k1gInSc6
země	zem	k1gFnSc2
<g/>
,	,	kIx,
zde	zde	k6eAd1
rolnické	rolnický	k2eAgMnPc4d1
povstalce	povstalec	k1gMnPc4
vedl	vést	k5eAaImAgInS
Emiliano	Emiliana	k1gFnSc5
Zapata	Zapat	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Huerta	Huerta	k1gFnSc1
měl	mít	k5eAaImAgInS
podporu	podpora	k1gFnSc4
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
v	v	k7c6
roce	rok	k1gInSc6
1914	#num#	k4
v	v	k7c4
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
prospěch	prospěch	k1gInSc4
vojensky	vojensky	k6eAd1
intervenovaly	intervenovat	k5eAaImAgInP
(	(	kIx(
<g/>
okupace	okupace	k1gFnSc2
Veracruzu	Veracruz	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Vměšování	vměšování	k1gNnSc1
Američanů	Američan	k1gMnPc2
vzbudilo	vzbudit	k5eAaPmAgNnS
nevoli	nevole	k1gFnSc4
v	v	k7c6
celé	celý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
a	a	k8xC
spíše	spíše	k9
sjednotilo	sjednotit	k5eAaPmAgNnS
protihuertovskou	protihuertovský	k2eAgFnSc4d1
opozici	opozice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Huerta	Huerta	k1gFnSc1
tlaku	tlak	k1gInSc2
povstalců	povstalec	k1gMnPc2
odolával	odolávat	k5eAaImAgMnS
do	do	k7c2
roku	rok	k1gInSc2
1914	#num#	k4
<g/>
,	,	kIx,
poté	poté	k6eAd1
prchl	prchnout	k5eAaPmAgMnS
do	do	k7c2
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c7
protihuertovskou	protihuertovský	k2eAgFnSc7d1
opozicí	opozice	k1gFnSc7
však	však	k9
ihned	ihned	k6eAd1
začaly	začít	k5eAaPmAgInP
ozbrojené	ozbrojený	k2eAgInPc1d1
střety	střet	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zapata	Zapata	k1gFnSc1
uzavřel	uzavřít	k5eAaPmAgInS
spojenectví	spojenectví	k1gNnSc4
s	s	k7c7
Villou	Villa	k1gMnSc7
a	a	k8xC
zahájil	zahájit	k5eAaPmAgMnS
boj	boj	k1gInSc4
proti	proti	k7c3
Carranzovi	Carranz	k1gMnSc3
s	s	k7c7
Obregónem	Obregón	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Levice	levice	k1gFnSc1
ovládla	ovládnout	k5eAaPmAgFnS
načas	načas	k6eAd1
i	i	k9
Ciudad	Ciudad	k1gInSc1
de	de	k?
México	México	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Carranza	Carranza	k1gFnSc1
se	se	k3xPyFc4
však	však	k9
spojil	spojit	k5eAaPmAgMnS
s	s	k7c7
dělnickým	dělnický	k2eAgNnSc7d1
hnutím	hnutí	k1gNnSc7
a	a	k8xC
odbory	odbor	k1gInPc7
(	(	kIx(
<g/>
tzv.	tzv.	kA
rudými	rudý	k2eAgInPc7d1
pluky	pluk	k1gInPc7
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1916	#num#	k4
byli	být	k5eAaImAgMnP
Zapata	Zapat	k1gMnSc4
s	s	k7c7
Villou	Villa	k1gFnSc7
poraženi	poražen	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1917	#num#	k4
pak	pak	k6eAd1
byla	být	k5eAaImAgNnP
ve	v	k7c6
městě	město	k1gNnSc6
Santiago	Santiago	k1gNnSc1
de	de	k?
Querétaro	Querétara	k1gFnSc5
přijata	přijmout	k5eAaPmNgFnS
nová	nový	k2eAgFnSc1d1
Ústava	ústava	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
část	část	k1gFnSc1
půdy	půda	k1gFnSc2
předala	předat	k5eAaPmAgFnS
malým	malý	k2eAgMnPc3d1
rolníkům	rolník	k1gMnPc3
<g/>
,	,	kIx,
dala	dát	k5eAaPmAgNnP
sociální	sociální	k2eAgNnPc1d1
práva	právo	k1gNnPc1
zejména	zejména	k9
dělníkům	dělník	k1gMnPc3
(	(	kIx(
<g/>
osmihodinová	osmihodinový	k2eAgFnSc1d1
pracovní	pracovní	k2eAgFnSc1d1
doba	doba	k1gFnSc1
atp.	atp.	kA
<g/>
)	)	kIx)
a	a	k8xC
zakazovala	zakazovat	k5eAaImAgFnS
cizincům	cizinec	k1gMnPc3
nabývat	nabývat	k5eAaImF
v	v	k7c6
Mexiku	Mexiko	k1gNnSc6
majetek	majetek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Poté	poté	k6eAd1
však	však	k9
začaly	začít	k5eAaPmAgInP
spory	spor	k1gInPc1
i	i	k9
mezi	mezi	k7c7
Carranzou	Carranza	k1gFnSc7
a	a	k8xC
Obregónem	Obregón	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Carranza	Carranza	k1gFnSc1
byl	být	k5eAaImAgInS
zvolen	zvolit	k5eAaPmNgInS
prezidentem	prezident	k1gMnSc7
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Obregón	Obregón	k1gMnSc1
se	se	k3xPyFc4
cítil	cítit	k5eAaImAgMnS
upozaděný	upozaděný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
měli	mít	k5eAaImAgMnP
proti	proti	k7c3
sobě	se	k3xPyFc3
kandidovat	kandidovat	k5eAaImF
na	na	k7c4
prezidenta	prezident	k1gMnSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
Obregón	Obregón	k1gInSc1
ještě	ještě	k6eAd1
před	před	k7c7
volbami	volba	k1gFnPc7
(	(	kIx(
<g/>
údajně	údajně	k6eAd1
kvůli	kvůli	k7c3
obavám	obava	k1gFnPc3
z	z	k7c2
volebních	volební	k2eAgFnPc2d1
machinací	machinace	k1gFnPc2
<g/>
)	)	kIx)
provedl	provést	k5eAaPmAgInS
vojenský	vojenský	k2eAgInSc1d1
převrat	převrat	k1gInSc1
<g/>
,	,	kIx,
během	během	k7c2
nějž	jenž	k3xRgInSc2
byl	být	k5eAaImAgInS
Carranza	Carranz	k1gMnSc2
obregonovci	obregonovec	k1gMnPc1
zavražděn	zavraždit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obregón	Obregón	k1gMnSc1
se	se	k3xPyFc4
ujal	ujmout	k5eAaPmAgMnS
prezidentského	prezidentský	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svým	svůj	k3xOyFgInSc7
ekonomickým	ekonomický	k2eAgInSc7d1
nacionalismem	nacionalismus	k1gInSc7
výrazně	výrazně	k6eAd1
zhoršil	zhoršit	k5eAaPmAgMnS
vztahy	vztah	k1gInPc4
s	s	k7c7
USA	USA	kA
<g/>
,	,	kIx,
avšak	avšak	k8xC
ty	ten	k3xDgInPc1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
kolem	kolem	k7c2
roku	rok	k1gInSc2
1923	#num#	k4
podařilo	podařit	k5eAaPmAgNnS
urovnat	urovnat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trvale	trvale	k6eAd1
zlé	zlý	k2eAgInPc1d1
byly	být	k5eAaImAgInP
naopak	naopak	k6eAd1
vztahy	vztah	k1gInPc1
s	s	k7c7
Vatikánem	Vatikán	k1gInSc7
<g/>
,	,	kIx,
kterému	který	k3yRgInSc3,k3yQgInSc3,k3yIgInSc3
se	se	k3xPyFc4
nelíbilo	líbit	k5eNaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
Ústava	ústava	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1917	#num#	k4
provedla	provést	k5eAaPmAgFnS
jednostrannou	jednostranný	k2eAgFnSc4d1
odluku	odluka	k1gFnSc4
státu	stát	k1gInSc2
a	a	k8xC
církve	církev	k1gFnSc2
<g/>
,	,	kIx,
omezila	omezit	k5eAaPmAgFnS
přístup	přístup	k1gInSc4
katolické	katolický	k2eAgFnSc2d1
církve	církev	k1gFnSc2
do	do	k7c2
škol	škola	k1gFnPc2
a	a	k8xC
také	také	k9
k	k	k7c3
majetku	majetek	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vatikán	Vatikán	k1gInSc1
svou	svůj	k3xOyFgFnSc4
kritiku	kritika	k1gFnSc4
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
USA	USA	kA
<g/>
,	,	kIx,
stále	stále	k6eAd1
zesiloval	zesilovat	k5eAaImAgMnS
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vedlo	vést	k5eAaImAgNnS
ke	k	k7c3
vzniku	vznik	k1gInSc3
hnutí	hnutí	k1gNnSc2
radikálních	radikální	k2eAgMnPc2d1
katolíků	katolík	k1gMnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
na	na	k7c6
venkově	venkov	k1gInSc6
(	(	kIx(
<g/>
tzv.	tzv.	kA
cristeros	cristerosa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Povstání	povstání	k1gNnPc2
kristerů	krister	k1gInPc2
mimořádně	mimořádně	k6eAd1
zesílilo	zesílit	k5eAaPmAgNnS
<g/>
,	,	kIx,
když	když	k8xS
roku	rok	k1gInSc2
1924	#num#	k4
vystřídal	vystřídat	k5eAaPmAgInS
Obregóna	Obregón	k1gMnSc4
v	v	k7c6
prezidentském	prezidentský	k2eAgInSc6d1
úřadě	úřad	k1gInSc6
další	další	k2eAgMnSc1d1
voják	voják	k1gMnSc1
Plutarco	Plutarco	k1gMnSc1
Elías	Elías	k1gMnSc1
Calles	Calles	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
nasadil	nasadit	k5eAaPmAgMnS
ještě	ještě	k6eAd1
ostřejší	ostrý	k2eAgInSc4d2
kurz	kurz	k1gInSc4
proti	proti	k7c3
katolické	katolický	k2eAgFnSc3d1
církvi	církev	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1927	#num#	k4
již	již	k9
odpor	odpor	k1gInSc1
cristeros	cristerosa	k1gFnPc2
přerostl	přerůst	k5eAaPmAgInS
v	v	k7c4
regulérní	regulérní	k2eAgFnSc4d1
občanskou	občanský	k2eAgFnSc4d1
válku	válka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obregón	Obregón	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1928	#num#	k4
znovu	znovu	k6eAd1
kandidoval	kandidovat	k5eAaImAgMnS
na	na	k7c4
prezidenta	prezident	k1gMnSc4
<g/>
,	,	kIx,
ve	v	k7c6
volbách	volba	k1gFnPc6
vyhrál	vyhrát	k5eAaPmAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
než	než	k8xS
mohl	moct	k5eAaImAgMnS
do	do	k7c2
úřadu	úřad	k1gInSc2
nastoupit	nastoupit	k5eAaPmF
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
zavražděn	zavraždit	k5eAaPmNgMnS
katolickým	katolický	k2eAgMnSc7d1
radikálem	radikál	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Lázaro	Lázara	k1gFnSc5
Cárdenas	Cárdenas	k1gMnSc1
určil	určit	k5eAaPmAgInS
podobu	podoba	k1gFnSc4
mexického	mexický	k2eAgInSc2d1
politického	politický	k2eAgInSc2d1
systému	systém	k1gInSc2
na	na	k7c4
desítky	desítka	k1gFnPc4
let	léto	k1gNnPc2
</s>
<s>
Calles	Calles	k1gInSc1
založil	založit	k5eAaPmAgInS
roku	rok	k1gInSc2
1929	#num#	k4
Národní	národní	k2eAgFnSc4d1
revoluční	revoluční	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
(	(	kIx(
<g/>
Partido	Partida	k1gFnSc5
Nacional	Nacional	k1gMnSc5
Revolucionario	Revolucionaria	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gNnSc7
prostřednictvím	prostřednictví	k1gNnSc7
ovládal	ovládat	k5eAaImAgInS
Mexiko	Mexiko	k1gNnSc4
až	až	k6eAd1
do	do	k7c2
poloviny	polovina	k1gFnSc2
30	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
,	,	kIx,
byť	byť	k8xS
prezidenty	prezident	k1gMnPc7
bylo	být	k5eAaImAgNnS
několik	několik	k4yIc1
jiných	jiný	k2eAgMnPc2d1
politiků	politik	k1gMnPc2
<g/>
,	,	kIx,
povětšinou	povětšinou	k6eAd1
Callesových	Callesový	k2eAgFnPc2d1
loutek	loutka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1934	#num#	k4
Calles	Callesa	k1gFnPc2
do	do	k7c2
prezidentského	prezidentský	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
dosadil	dosadit	k5eAaPmAgInS
Lázaro	Lázara	k1gFnSc5
Cárdenase	Cárdenas	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
však	však	k9
Callesovi	Calles	k1gMnSc3
rychle	rychle	k6eAd1
přerostl	přerůst	k5eAaPmAgMnS
přes	přes	k7c4
hlavu	hlava	k1gFnSc4
<g/>
,	,	kIx,
zbavil	zbavit	k5eAaPmAgMnS
ho	on	k3xPp3gInSc2
vlivu	vliv	k1gInSc2
a	a	k8xC
nakonec	nakonec	k6eAd1
přinutil	přinutit	k5eAaPmAgMnS
k	k	k7c3
exilu	exil	k1gInSc3
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1938	#num#	k4
Cárdenas	Cárdenas	k1gMnSc1
přejmenoval	přejmenovat	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
stranu	strana	k1gFnSc4
na	na	k7c4
Mexickou	mexický	k2eAgFnSc4d1
revoluční	revoluční	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
(	(	kIx(
<g/>
Partido	Partida	k1gFnSc5
Revolucionario	Revolucionaria	k1gFnSc5
Mexicano	Mexicana	k1gFnSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cárdenas	Cárdenas	k1gMnSc1
také	také	k9
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
nastoluje	nastolovat	k5eAaImIp3nS
tzv.	tzv.	kA
„	„	k?
<g/>
revoluční	revoluční	k2eAgInSc4d1
režim	režim	k1gInSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
měla	mít	k5eAaImAgFnS
revoluce	revoluce	k1gFnSc1
„	„	k?
<g/>
definitivně	definitivně	k6eAd1
zvítězit	zvítězit	k5eAaPmF
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
výsledek	výsledek	k1gInSc1
revoluce	revoluce	k1gFnSc2
<g/>
,	,	kIx,
tedy	tedy	k9
režim	režim	k1gInSc1
založený	založený	k2eAgInSc1d1
na	na	k7c6
etatismu	etatismus	k1gInSc6
<g/>
,	,	kIx,
nacionalismu	nacionalismus	k1gInSc6
<g/>
,	,	kIx,
korporativismu	korporativismus	k1gInSc6
<g/>
,	,	kIx,
vládě	vláda	k1gFnSc6
jedné	jeden	k4xCgFnSc2
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
silném	silný	k2eAgInSc6d1
modernizačním	modernizační	k2eAgInSc6d1
étosu	étos	k1gInSc6
<g/>
,	,	kIx,
silné	silný	k2eAgFnSc6d1
roli	role	k1gFnSc6
odborů	odbor	k1gInPc2
a	a	k8xC
určitých	určitý	k2eAgInPc6d1
vztazích	vztah	k1gInPc6
ke	k	k7c3
komunismu	komunismus	k1gInSc3
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
Cárdenasův	Cárdenasův	k2eAgInSc1d1
obdiv	obdiv	k1gInSc4
k	k	k7c3
Trockému	Trocký	k1gMnSc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
trval	trvat	k5eAaImAgInS
v	v	k7c6
Mexiku	Mexiko	k1gNnSc6
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Systém	systém	k1gInSc1
jedné	jeden	k4xCgFnSc2
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
nazývané	nazývaný	k2eAgFnSc6d1
Institucionální	institucionální	k2eAgFnSc1d1
revoluční	revoluční	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
nebyl	být	k5eNaImAgMnS
demokratický	demokratický	k2eAgMnSc1d1
<g/>
,	,	kIx,
avšak	avšak	k8xC
připsal	připsat	k5eAaPmAgInS
si	se	k3xPyFc3
řadu	řada	k1gFnSc4
úspěchů	úspěch	k1gInPc2
<g/>
:	:	kIx,
odklidil	odklidit	k5eAaPmAgInS
z	z	k7c2
politiky	politika	k1gFnSc2
vliv	vliv	k1gInSc4
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
v	v	k7c6
mnoha	mnoho	k4c6
jiných	jiný	k2eAgFnPc6d1
latinoamerických	latinoamerický	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
nepovedlo	povést	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Země	země	k1gFnSc1
byla	být	k5eAaImAgFnS
silně	silně	k6eAd1
modernizována	modernizovat	k5eAaBmNgFnS
a	a	k8xC
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
zaznamenala	zaznamenat	k5eAaPmAgFnS
ekonomický	ekonomický	k2eAgInSc4d1
boom	boom	k1gInSc4
<g/>
,	,	kIx,
o	o	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
se	se	k3xPyFc4
začalo	začít	k5eAaPmAgNnS
hovořit	hovořit	k5eAaImF
jako	jako	k9
o	o	k7c6
mexickém	mexický	k2eAgInSc6d1
zázraku	zázrak	k1gInSc6
<g/>
,	,	kIx,
jakkoli	jakkoli	k8xS
mu	on	k3xPp3gMnSc3
pomáhal	pomáhat	k5eAaImAgInS
„	„	k?
<g/>
doping	doping	k1gInSc1
<g/>
“	“	k?
v	v	k7c6
podobě	podoba	k1gFnSc6
bohatých	bohatý	k2eAgNnPc2d1
ložisek	ložisko	k1gNnPc2
ropy	ropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
ale	ale	k8xC
rostla	růst	k5eAaImAgFnS
nespokojenost	nespokojenost	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
ze	z	k7c2
všech	všecek	k3xTgFnPc2
stran	strana	k1gFnPc2
–	–	k?
liberálům	liberál	k1gMnPc3
se	se	k3xPyFc4
nelíbilo	líbit	k5eNaImAgNnS
státní	státní	k2eAgNnSc1d1
vlastnictví	vlastnictví	k1gNnSc1
všech	všecek	k3xTgMnPc2
klíčových	klíčový	k2eAgMnPc2d1
podniků	podnik	k1gInPc2
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
radikálním	radikální	k2eAgMnPc3d1
demokratům	demokrat	k1gMnPc3
málo	málo	k6eAd1
demokracie	demokracie	k1gFnSc2
(	(	kIx(
<g/>
dali	dát	k5eAaPmAgMnP
o	o	k7c4
sobě	se	k3xPyFc3
vědět	vědět	k5eAaImF
prvně	prvně	k?
při	při	k7c6
studentských	studentský	k2eAgInPc6d1
nepokojích	nepokoj	k1gInPc6
roku	rok	k1gInSc2
1968	#num#	k4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
poznamenaly	poznamenat	k5eAaPmAgFnP
i	i	k9
toho	ten	k3xDgInSc2
roku	rok	k1gInSc2
pořádanou	pořádaný	k2eAgFnSc4d1
olympiádu	olympiáda	k1gFnSc4
<g/>
)	)	kIx)
a	a	k8xC
nespokojenost	nespokojenost	k1gFnSc4
rostla	růst	k5eAaImAgFnS
i	i	k9
na	na	k7c4
„	„	k?
<g/>
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc3
barikády	barikáda	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
u	u	k7c2
levicových	levicový	k2eAgMnPc2d1
radikálů	radikál	k1gMnPc2
na	na	k7c6
venkově	venkov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Podpis	podpis	k1gInSc1
dohody	dohoda	k1gFnSc2
NAFTA	nafta	k1gFnSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stojící	stojící	k2eAgFnSc1d1
zleva	zleva	k6eAd1
<g/>
:	:	kIx,
Carlos	Carlos	k1gMnSc1
Salinas	Salinas	k1gMnSc1
<g/>
,	,	kIx,
George	George	k1gFnSc1
Bush	Bush	k1gMnSc1
a	a	k8xC
Brian	Brian	k1gMnSc1
Mulroney	Mulronea	k1gFnSc2
</s>
<s>
Zvláště	zvláště	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
ekonomický	ekonomický	k2eAgInSc1d1
zázrak	zázrak	k1gInSc1
skončil	skončit	k5eAaPmAgInS
<g/>
,	,	kIx,
už	už	k6eAd1
kolem	kolem	k7c2
roku	rok	k1gInSc2
1970	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mexiko	Mexiko	k1gNnSc1
se	se	k3xPyFc4
nejprve	nejprve	k6eAd1
masivně	masivně	k6eAd1
zadlužilo	zadlužit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
tvrdě	tvrdě	k6eAd1
devalvovalo	devalvovat	k5eAaBmAgNnS
měnu	měna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krize	krize	k1gFnSc1
vyvrcholila	vyvrcholit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Prezident	prezident	k1gMnSc1
Miguel	Miguel	k1gMnSc1
de	de	k?
La	la	k1gNnPc2
Madrid	Madrid	k1gInSc1
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgInS
zlepšit	zlepšit	k5eAaPmF
hospodářskou	hospodářský	k2eAgFnSc4d1
situaci	situace	k1gFnSc4
země	zem	k1gFnSc2
například	například	k6eAd1
přílivem	příliv	k1gInSc7
nových	nový	k2eAgFnPc2d1
investic	investice	k1gFnPc2
a	a	k8xC
drastickým	drastický	k2eAgNnSc7d1
škrtáním	škrtání	k1gNnSc7
státních	státní	k2eAgInPc2d1
výdajů	výdaj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neoliberální	neoliberální	k2eAgInPc1d1
trendy	trend	k1gInPc1
<g/>
,	,	kIx,
tehdy	tehdy	k6eAd1
velmi	velmi	k6eAd1
módní	módní	k2eAgFnSc1d1
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
,	,	kIx,
posílil	posílit	k5eAaPmAgMnS
prezident	prezident	k1gMnSc1
Carlos	Carlos	k1gMnSc1
Salinas	Salinas	k1gMnSc1
de	de	k?
Gortari	Gortar	k1gFnSc2
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
–	–	k?
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přistoupil	přistoupit	k5eAaPmAgMnS
k	k	k7c3
privatizaci	privatizace	k1gFnSc3
a	a	k8xC
deregulaci	deregulace	k1gFnSc3
mexického	mexický	k2eAgNnSc2d1
hospodářství	hospodářství	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
Vrcholem	vrchol	k1gInSc7
Salinasovy	Salinasův	k2eAgFnSc2d1
hospodářské	hospodářský	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
bylo	být	k5eAaImAgNnS
přistoupení	přistoupení	k1gNnSc4
Mexika	Mexiko	k1gNnSc2
k	k	k7c3
Severoamerické	severoamerický	k2eAgFnSc3d1
dohodě	dohoda	k1gFnSc3
o	o	k7c6
volném	volný	k2eAgInSc6d1
obchodu	obchod	k1gInSc6
(	(	kIx(
<g/>
NAFTA	nafta	k1gFnSc1
<g/>
)	)	kIx)
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podpis	podpis	k1gInSc4
smlouvy	smlouva	k1gFnSc2
NAFTA	nafta	k1gFnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
bezprostředním	bezprostřední	k2eAgInSc7d1
motivem	motiv	k1gInSc7
levicového	levicový	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
ve	v	k7c6
státě	stát	k1gInSc6
Chiapas	Chiapasa	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
Zapatistická	Zapatistický	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
národního	národní	k2eAgNnSc2d1
osvobození	osvobození	k1gNnSc2
(	(	kIx(
<g/>
Ejército	Ejércit	k2eAgNnSc1d1
Zapatista	zapatista	k1gMnSc1
de	de	k?
Liberación	Liberación	k1gMnSc1
Nacional	Nacional	k1gMnSc1
<g/>
;	;	kIx,
EZLN	EZLN	kA
<g/>
)	)	kIx)
vyhlásila	vyhlásit	k5eAaPmAgFnS
válku	válka	k1gFnSc4
mexickému	mexický	k2eAgInSc3d1
státu	stát	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Zapatisté	zapatista	k1gMnPc1
si	se	k3xPyFc3
nakonec	nakonec	k6eAd1
vymohli	vymoct	k5eAaPmAgMnP
vznik	vznik	k1gInSc4
vlastní	vlastní	k2eAgFnSc2d1
autonomní	autonomní	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
,	,	kIx,
mexická	mexický	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
rezignovala	rezignovat	k5eAaBmAgFnS
na	na	k7c4
vojenské	vojenský	k2eAgNnSc4d1
řešení	řešení	k1gNnSc4
a	a	k8xC
přijala	přijmout	k5eAaPmAgFnS
tento	tento	k3xDgInSc4
kompromis	kompromis	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Vládnoucí	vládnoucí	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
za	za	k7c2
vlády	vláda	k1gFnSc2
prezidenta	prezident	k1gMnSc2
Ernesta	Ernest	k1gMnSc2
Zedilly	Zedilla	k1gMnSc2
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
musela	muset	k5eAaImAgFnS
udělat	udělat	k5eAaPmF
kompromisy	kompromis	k1gInPc4
i	i	k9
jinde	jinde	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
systém	systém	k1gInSc1
demokratizovala	demokratizovat	k5eAaBmAgFnS
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
byl	být	k5eAaImAgMnS
zvolen	zvolen	k2eAgMnSc1d1
první	první	k4xOgMnSc1
prezident	prezident	k1gMnSc1
z	z	k7c2
opoziční	opoziční	k2eAgFnSc2d1
strany	strana	k1gFnSc2
od	od	k7c2
roku	rok	k1gInSc2
1930	#num#	k4
–	–	k?
Vicente	Vicent	k1gInSc5
Fox	fox	k1gInSc1
Quesada	Quesada	k1gFnSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
z	z	k7c2
konzervativní	konzervativní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
PAN	Pan	k1gMnSc1
(	(	kIx(
<g/>
Partido	Partida	k1gFnSc5
de	de	k?
Acción	Acción	k1gMnSc1
Nacional	Nacional	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fox	fox	k1gInSc1
se	se	k3xPyFc4
silně	silně	k6eAd1
orientoval	orientovat	k5eAaBmAgMnS
na	na	k7c4
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
<g/>
,	,	kIx,
přesto	přesto	k8xC
i	i	k9
on	on	k3xPp3gMnSc1
čelil	čelit	k5eAaImAgMnS
rostoucí	rostoucí	k2eAgFnSc2d1
nespokojenosti	nespokojenost	k1gFnSc2
Američanů	Američan	k1gMnPc2
s	s	k7c7
nelegální	legální	k2eNgFnSc7d1
migrací	migrace	k1gFnSc7
Mexičanů	Mexičan	k1gMnPc2
do	do	k7c2
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
problém	problém	k1gInSc1
není	být	k5eNaImIp3nS
vyřešen	vyřešit	k5eAaPmNgInS
dosud	dosud	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Mexiku	Mexiko	k1gNnSc6
dramaticky	dramaticky	k6eAd1
zintenzívnila	zintenzívnit	k5eAaPmAgFnS
drogová	drogový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
mezi	mezi	k7c7
státem	stát	k1gInSc7
a	a	k8xC
drogovými	drogový	k2eAgInPc7d1
kartely	kartel	k1gInPc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
si	se	k3xPyFc3
vyžádala	vyžádat	k5eAaPmAgFnS
už	už	k6eAd1
téměř	téměř	k6eAd1
140	#num#	k4
000	#num#	k4
obětí	oběť	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2020	#num#	k4
vstoupila	vstoupit	k5eAaPmAgFnS
v	v	k7c4
platnost	platnost	k1gFnSc4
nová	nový	k2eAgFnSc1d1
Dohoda	dohoda	k1gFnSc1
USA-Mexiko-Kanada	USA-Mexiko-Kanada	k1gFnSc1
(	(	kIx(
<g/>
USMCA	USMCA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
nahradila	nahradit	k5eAaPmAgFnS
úmluvu	úmluva	k1gFnSc4
NAFTA	nafta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
mexické	mexický	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
americká	americký	k2eAgFnSc1d1
země	země	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severu	sever	k1gInSc6
hraničí	hraničit	k5eAaImIp3nS
s	s	k7c7
USA	USA	kA
a	a	k8xC
na	na	k7c6
jihu	jih	k1gInSc6
s	s	k7c7
Guatemalou	Guatemala	k1gFnSc7
a	a	k8xC
Belize	Belize	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
Mexiku	Mexiko	k1gNnSc3
patří	patřit	k5eAaImIp3nS
také	také	k9
několik	několik	k4yIc1
ostrovů	ostrov	k1gInPc2
ležících	ležící	k2eAgInPc2d1
na	na	k7c6
západě	západ	k1gInSc6
v	v	k7c6
Tichém	tichý	k2eAgInSc6d1
oceánu	oceán	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
na	na	k7c6
východě	východ	k1gInSc6
okolo	okolo	k7c2
pobřeží	pobřeží	k1gNnSc2
v	v	k7c6
Mexickém	mexický	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
<g/>
,	,	kIx,
či	či	k8xC
Karibském	karibský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejnižším	nízký	k2eAgInSc7d3
bodem	bod	k1gInSc7
je	být	k5eAaImIp3nS
proláklina	proláklina	k1gFnSc1
nedaleko	nedaleko	k7c2
Mexicali	Mexicali	k1gFnSc2
(	(	kIx(
<g/>
–	–	k?
<g/>
10	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Povrch	povrch	k1gInSc1
</s>
<s>
Družicový	družicový	k2eAgInSc1d1
snímek	snímek	k1gInSc1
Mexika	Mexiko	k1gNnSc2
</s>
<s>
Pico	Pico	k6eAd1
de	de	k?
Orizaba	Orizaba	k1gFnSc1
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnSc7d3
horou	hora	k1gFnSc7
Mexika	Mexiko	k1gNnSc2
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
leží	ležet	k5eAaImIp3nS
na	na	k7c6
západní	západní	k2eAgFnSc6d1
polokouli	polokoule	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
části	část	k1gFnSc6
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severu	sever	k1gInSc6
sousedí	sousedit	k5eAaImIp3nS
s	s	k7c7
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hranici	hranice	k1gFnSc4
v	v	k7c6
délce	délka	k1gFnSc6
2597	#num#	k4
km	km	kA
tvoří	tvořit	k5eAaImIp3nS
částečně	částečně	k6eAd1
řeka	řeka	k1gFnSc1
Rio	Rio	k1gMnSc5
Grande	grand	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jihu	jih	k1gInSc6
hraničí	hraničit	k5eAaImIp3nS
Mexiko	Mexiko	k1gNnSc1
s	s	k7c7
Guatemalou	Guatemala	k1gFnSc7
(	(	kIx(
<g/>
962	#num#	k4
km	km	kA
<g/>
)	)	kIx)
a	a	k8xC
Belize	Belize	k1gFnSc1
(	(	kIx(
<g/>
176	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1
délka	délka	k1gFnSc1
mexického	mexický	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
je	být	k5eAaImIp3nS
9219	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
západě	západ	k1gInSc6
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
tichomořské	tichomořský	k2eAgNnSc1d1
pobřeží	pobřeží	k1gNnSc1
<g/>
,	,	kIx,
na	na	k7c6
východě	východ	k1gInSc6
atlantské	atlantský	k2eAgNnSc4d1
pobřeží	pobřeží	k1gNnSc4
<g/>
,	,	kIx,
část	část	k1gFnSc4
východního	východní	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
oblévá	oblévat	k5eAaImIp3nS
Mexický	mexický	k2eAgInSc1d1
záliv	záliv	k1gInSc1
a	a	k8xC
část	část	k1gFnSc1
Karibské	karibský	k2eAgNnSc4d1
moře	moře	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jihu	jih	k1gInSc6
se	se	k3xPyFc4
při	při	k7c6
pobřeží	pobřeží	k1gNnSc6
táhne	táhnout	k5eAaImIp3nS
úzký	úzký	k2eAgInSc1d1
Středoamerický	středoamerický	k2eAgInSc1d1
příkop	příkop	k1gInSc1
dosahující	dosahující	k2eAgInSc4d1
hloubek	hloubek	k1gInSc4
přes	přes	k7c4
6000	#num#	k4
m.	m.	k?
</s>
<s>
Rozloha	rozloha	k1gFnSc1
mexického	mexický	k2eAgNnSc2d1
území	území	k1gNnSc2
je	být	k5eAaImIp3nS
téměř	téměř	k6eAd1
2	#num#	k4
milióny	milión	k4xCgInPc4
kilometrů	kilometr	k1gInPc2
čtverečních	čtvereční	k2eAgInPc2d1
<g/>
,	,	kIx,
po	po	k7c6
Brazílii	Brazílie	k1gFnSc6
a	a	k8xC
Argentině	Argentina	k1gFnSc6
je	být	k5eAaImIp3nS
Mexiko	Mexiko	k1gNnSc1
třetí	třetí	k4xOgFnSc7
největší	veliký	k2eAgFnSc7d3
zemí	zem	k1gFnSc7
Latinské	latinský	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
největší	veliký	k2eAgInPc4d3
mexické	mexický	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
patří	patřit	k5eAaImIp3nS
Tiburón	Tiburón	k1gInSc1
<g/>
,	,	kIx,
Ángel	Ángel	k1gInSc1
de	de	k?
la	la	k1gNnSc1
Guarda	Guardo	k1gNnSc2
<g/>
,	,	kIx,
Cozumel	Cozumela	k1gFnPc2
<g/>
,	,	kIx,
Cedros	Cedrosa	k1gFnPc2
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Mexické	mexický	k2eAgNnSc1d1
území	území	k1gNnSc1
se	se	k3xPyFc4
horopisně	horopisně	k6eAd1
z	z	k7c2
největší	veliký	k2eAgFnSc2d3
části	část	k1gFnSc2
přimyká	přimykat	k5eAaImIp3nS
k	k	k7c3
severoamerické	severoamerický	k2eAgFnSc3d1
Kordillerské	kordillerský	k2eAgFnSc3d1
soustavě	soustava	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
na	na	k7c6
něm	on	k3xPp3gInSc6
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
rozsáhlou	rozsáhlý	k2eAgFnSc4d1
vysočinu	vysočina	k1gFnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
se	se	k3xPyFc4
zdvihá	zdvihat	k5eAaImIp3nS
postupně	postupně	k6eAd1
k	k	k7c3
jihu	jih	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gNnSc7
jádrem	jádro	k1gNnSc7
je	být	k5eAaImIp3nS
náhorní	náhorní	k2eAgFnSc1d1
plošina	plošina	k1gFnSc1
„	„	k?
<g/>
Mesa	Mesa	k1gMnSc1
central	centrat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
“	“	k?
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc7,k3yQgFnSc7,k3yIgFnSc7
prostupují	prostupovat	k5eAaImIp3nP
horské	horský	k2eAgInPc1d1
hřbety	hřbet	k1gInPc1
a	a	k8xC
kotliny	kotlina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mexická	mexický	k2eAgFnSc1d1
vysočina	vysočina	k1gFnSc1
je	být	k5eAaImIp3nS
při	při	k7c6
obou	dva	k4xCgInPc6
oceánech	oceán	k1gInPc6
ohraničena	ohraničit	k5eAaPmNgFnS
horskými	horský	k2eAgFnPc7d1
pásmy	pásmo	k1gNnPc7
pohoří	pohoří	k1gNnSc3
Sierra	Sierra	k1gFnSc1
Madre	Madr	k1gInSc5
Oriental	Oriental	k1gMnSc1
a	a	k8xC
Sierra	Sierra	k1gFnSc1
Madre	Madr	k1gInSc5
Occidental	Occidental	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jihu	jih	k1gInSc6
se	se	k3xPyFc4
mexická	mexický	k2eAgFnSc1d1
vysočina	vysočina	k1gFnSc1
nazývá	nazývat	k5eAaImIp3nS
Anahuácká	Anahuácký	k2eAgFnSc1d1
vysočina	vysočina	k1gFnSc1
a	a	k8xC
zvedá	zvedat	k5eAaImIp3nS
se	se	k3xPyFc4
do	do	k7c2
výše	výše	k1gFnSc2,k1gFnSc2wB
2000	#num#	k4
m.	m.	k?
Nejdůležitější	důležitý	k2eAgFnSc7d3
částí	část	k1gFnSc7
jsou	být	k5eAaImIp3nP
dvě	dva	k4xCgFnPc4
kotliny	kotlina	k1gFnPc4
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
je	být	k5eAaImIp3nS
velká	velký	k2eAgFnSc1d1
koncentrace	koncentrace	k1gFnSc1
obyvatelstva	obyvatelstvo	k1gNnSc2
–	–	k?
Mexické	mexický	k2eAgFnSc2d1
a	a	k8xC
Tolucké	Tolucké	k2eAgNnSc2d1
údolí	údolí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
úrodné	úrodný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
jihu	jih	k1gInSc6
Mexické	mexický	k2eAgFnSc2d1
vysočiny	vysočina	k1gFnSc2
vystupuje	vystupovat	k5eAaImIp3nS
významné	významný	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
s	s	k7c7
několika	několik	k4yIc7
menšími	malý	k2eAgNnPc7d2
pohořími	pohoří	k1gNnPc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
tvoří	tvořit	k5eAaImIp3nS
jihomexické	jihomexický	k2eAgNnSc4d1
vulkanické	vulkanický	k2eAgNnSc4d1
pásmo	pásmo	k1gNnSc4
Cordillera	Cordiller	k1gMnSc2
Neovolcánica	Neovolcánicus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyššími	vysoký	k2eAgFnPc7d3
horami	hora	k1gFnPc7
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
Pico	Pico	k6eAd1
de	de	k?
Orizaba	Orizaba	k1gFnSc1
(	(	kIx(
<g/>
nebo	nebo	k8xC
též	též	k9
Citlaltépetl	Citlaltépetl	k1gFnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
5636	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Popocatépetl	Popocatépetl	k1gMnSc1
(	(	kIx(
<g/>
5452	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Iztaccíhuatl	Iztaccíhuatl	k1gMnSc1
(	(	kIx(
<g/>
5280	#num#	k4
m	m	kA
<g/>
)	)	kIx)
a	a	k8xC
Colima	Colima	k1gFnSc1
(	(	kIx(
<g/>
4340	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc4
území	území	k1gNnPc2
bývá	bývat	k5eAaImIp3nS
postihována	postihovat	k5eAaImNgFnS
zemětřeseními	zemětřesení	k1gNnPc7
tektonického	tektonický	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
sopky	sopka	k1gFnPc1
se	se	k3xPyFc4
často	často	k6eAd1
projevují	projevovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Ještě	ještě	k9
jižněji	jižně	k6eAd2
leží	ležet	k5eAaImIp3nS
pohoří	pohoří	k1gNnSc1
Sierra	Sierra	k1gFnSc1
Madre	Madr	k1gInSc5
del	del	k?
Sur	Sur	k1gMnSc5
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
ohraničeno	ohraničit	k5eAaPmNgNnS
na	na	k7c6
jihu	jih	k1gInSc6
Tichým	Tichý	k1gMnSc7
oceánem	oceán	k1gInSc7
<g/>
,	,	kIx,
na	na	k7c6
východě	východ	k1gInSc6
Tehuantepeckou	Tehuantepecká	k1gFnSc4
šíjí	šíj	k1gFnPc2
a	a	k8xC
na	na	k7c6
severu	sever	k1gInSc2
Neovulcanickým	Neovulcanický	k2eAgNnPc3d1
pohořím	pohoří	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
východ	východ	k1gInSc4
od	od	k7c2
Tehuantepecké	Tehuantepecký	k2eAgFnSc2d1
šíje	šíj	k1gFnSc2
se	se	k3xPyFc4
zvedá	zvedat	k5eAaImIp3nS
pohoří	pohoří	k1gNnSc1
Sierra	Sierra	k1gFnSc1
Madre	Madr	k1gInSc5
de	de	k?
Chiapas	Chiapasa	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
přechází	přecházet	k5eAaImIp3nP
do	do	k7c2
Tabaské	Tabaský	k2eAgFnSc2d1
nížiny	nížina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
navazuje	navazovat	k5eAaImIp3nS
na	na	k7c4
rozsáhlou	rozsáhlý	k2eAgFnSc4d1
suchou	suchý	k2eAgFnSc4d1
a	a	k8xC
zkrasovělou	zkrasovělý	k2eAgFnSc4d1
vápencovou	vápencový	k2eAgFnSc4d1
tabuli	tabule	k1gFnSc4
Yucatánského	Yucatánský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
severozápadě	severozápad	k1gInSc6
tvoří	tvořit	k5eAaImIp3nP
část	část	k1gFnSc4
Mexika	Mexiko	k1gNnSc2
Kalifornský	kalifornský	k2eAgInSc1d1
poloostrov	poloostrov	k1gInSc1
dlouhý	dlouhý	k2eAgInSc1d1
1250	#num#	k4
km	km	kA
a	a	k8xC
Kalifornský	kalifornský	k2eAgInSc4d1
záliv	záliv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgFnSc7d3
horou	hora	k1gFnSc7
poloostrova	poloostrov	k1gInSc2
je	být	k5eAaImIp3nS
El	Ela	k1gFnPc2
Picacho	Picacha	k1gFnSc5
del	del	k?
Diablo	Diablo	k1gFnSc4
–	–	k?
3095	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
500	#num#	k4
km	km	kA
od	od	k7c2
pobřeží	pobřeží	k1gNnSc2
na	na	k7c4
západ	západ	k1gInSc4
leží	ležet	k5eAaImIp3nP
vulkanické	vulkanický	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
Revillagigedo	Revillagigedo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Vodstvo	vodstvo	k1gNnSc1
</s>
<s>
Kaňon	kaňon	k1gInSc4
Sumidero	Sumidero	k1gNnSc1
na	na	k7c6
řece	řeka	k1gFnSc6
Grijalva	Grijalvo	k1gNnSc2
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
má	mít	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
málo	málo	k4c4
řek	řeka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgFnSc7d3
řekou	řeka	k1gFnSc7
je	být	k5eAaImIp3nS
pohraniční	pohraniční	k2eAgFnSc1d1
Río	Río	k1gFnSc1
Bravo	bravo	k1gMnSc1
del	del	k?
Norte	Nort	k1gInSc5
(	(	kIx(
<g/>
v	v	k7c6
USA	USA	kA
zvaná	zvaný	k2eAgFnSc1d1
Rio	Rio	k1gFnSc1
Grande	grand	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dlouhá	dlouhý	k2eAgFnSc1d1
3023	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
přítoků	přítok	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
přijímá	přijímat	k5eAaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
významnější	významný	k2eAgFnSc1d2
jen	jen	k9
řeka	řeka	k1gFnSc1
Conchos	Conchosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc1
větších	veliký	k2eAgFnPc2d2
řek	řeka	k1gFnPc2
je	být	k5eAaImIp3nS
na	na	k7c6
atlantském	atlantský	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
bohatší	bohatý	k2eAgFnPc1d2
na	na	k7c4
srážky	srážka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdůležitější	důležitý	k2eAgInSc1d3
z	z	k7c2
nich	on	k3xPp3gInPc2
je	být	k5eAaImIp3nS
Panuco	Panuco	k1gNnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
ústí	ústit	k5eAaImIp3nS
do	do	k7c2
Mexického	mexický	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západní	západní	k2eAgInSc4d1
pobřeží	pobřeží	k1gNnSc1
má	mít	k5eAaImIp3nS
četné	četný	k2eAgFnPc4d1
kratší	krátký	k2eAgFnPc4d2
řeky	řeka	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
prudce	prudko	k6eAd1
spadají	spadat	k5eAaPmIp3nP,k5eAaImIp3nP
z	z	k7c2
pobřežních	pobřežní	k2eAgNnPc2d1
horských	horský	k2eAgNnPc2d1
pásem	pásmo	k1gNnPc2
<g/>
,	,	kIx,
a	a	k8xC
mají	mít	k5eAaImIp3nP
proto	proto	k8xC
značný	značný	k2eAgInSc4d1
energetický	energetický	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
Yaqui	Yaqu	k1gFnPc1
<g/>
,	,	kIx,
Fuerte	Fuert	k1gMnSc5
<g/>
,	,	kIx,
Río	Río	k1gMnSc5
Grande	grand	k1gMnSc5
de	de	k?
Santiago	Santiago	k1gNnSc1
(	(	kIx(
<g/>
800	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
protéká	protékat	k5eAaImIp3nS
největším	veliký	k2eAgNnSc7d3
mexickým	mexický	k2eAgNnSc7d1
jezerem	jezero	k1gNnSc7
Lago	Lago	k1gMnSc1
de	de	k?
Chapala	Chapal	k1gMnSc4
(	(	kIx(
<g/>
1	#num#	k4
116	#num#	k4
km²	km²	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
na	na	k7c6
severu	sever	k1gInSc6
řeka	řeka	k1gFnSc1
Balsas	Balsas	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největšími	veliký	k2eAgFnPc7d3
řekami	řeka	k1gFnPc7
mexického	mexický	k2eAgInSc2d1
jihu	jih	k1gInSc2
jsou	být	k5eAaImIp3nP
Usumacinta	Usumacinto	k1gNnPc4
(	(	kIx(
<g/>
1000	#num#	k4
km	km	kA
<g/>
)	)	kIx)
a	a	k8xC
Grijalva	Grijalva	k1gFnSc1
(	(	kIx(
<g/>
velké	velký	k2eAgFnSc2d1
přehrady	přehrada	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Usumacinta	Usumacint	k1gInSc2
tvoří	tvořit	k5eAaImIp3nS
část	část	k1gFnSc1
hranice	hranice	k1gFnSc1
s	s	k7c7
Guatemalou	Guatemala	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východní	východní	k2eAgInSc4d1
pobřeží	pobřeží	k1gNnSc1
Yucatánského	Yucatánský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
lemuje	lemovat	k5eAaImIp3nS
Mezoamerický	Mezoamerický	k2eAgInSc4d1
korálový	korálový	k2eAgInSc4d1
útes	útes	k1gInSc4
<g/>
,	,	kIx,
druhý	druhý	k4xOgInSc4
největší	veliký	k2eAgInSc4d3
korálový	korálový	k2eAgInSc4d1
útes	útes	k1gInSc4
na	na	k7c4
Zemi	zem	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Podnebí	podnebí	k1gNnSc1
</s>
<s>
Sever	sever	k1gInSc1
Mexika	Mexiko	k1gNnSc2
leží	ležet	k5eAaImIp3nS
v	v	k7c6
subtropickém	subtropický	k2eAgInSc6d1
pásu	pás	k1gInSc6
<g/>
,	,	kIx,
jih	jih	k1gInSc1
v	v	k7c6
tropickém	tropický	k2eAgInSc6d1
pásu	pás	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
ale	ale	k9
týká	týkat	k5eAaImIp3nS
jen	jen	k6eAd1
nížin	nížina	k1gFnPc2
(	(	kIx(
<g/>
převážně	převážně	k6eAd1
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podnebí	podnebí	k1gNnSc1
jinak	jinak	k6eAd1
závisí	záviset	k5eAaImIp3nS
zejména	zejména	k9
na	na	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Země	země	k1gFnSc1
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
čtyři	čtyři	k4xCgNnPc4
výšková	výškový	k2eAgNnPc4d1
podnebná	podnebný	k2eAgNnPc4d1
pásma	pásmo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Horká	Horká	k1gFnSc1
země	země	k1gFnSc1
(	(	kIx(
<g/>
tierra	tierra	k1gFnSc1
caliente	calient	k1gInSc5
<g/>
)	)	kIx)
do	do	k7c2
výšky	výška	k1gFnSc2
800	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
má	mít	k5eAaImIp3nS
průměrnou	průměrný	k2eAgFnSc4d1
roční	roční	k2eAgFnSc4d1
teplotu	teplota	k1gFnSc4
přes	přes	k7c4
23	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Mírná	mírný	k2eAgFnSc1d1
země	země	k1gFnSc1
(	(	kIx(
<g/>
tierra	tierra	k1gFnSc1
templada	templada	k1gFnSc1
<g/>
)	)	kIx)
do	do	k7c2
1700	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
měla	mít	k5eAaImAgFnS
průměrnou	průměrný	k2eAgFnSc4d1
teplotu	teplota	k1gFnSc4
od	od	k7c2
17	#num#	k4
°	°	k?
<g/>
C	C	kA
do	do	k7c2
23	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Chladnou	chladný	k2eAgFnSc4d1
zemi	zem	k1gFnSc4
(	(	kIx(
<g/>
tierra	tierra	k6eAd1
fría	fría	k6eAd1
<g/>
)	)	kIx)
do	do	k7c2
2500	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
s	s	k7c7
teplotou	teplota	k1gFnSc7
méně	málo	k6eAd2
než	než	k8xS
17	#num#	k4
°	°	k?
<g/>
C	C	kA
střídá	střídat	k5eAaImIp3nS
pásmo	pásmo	k1gNnSc4
věčného	věčný	k2eAgInSc2d1
sněhu	sníh	k1gInSc2
(	(	kIx(
<g/>
tierra	tierra	k1gFnSc1
helada	helada	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
jihu	jih	k1gInSc6
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
rok	rok	k1gInSc1
na	na	k7c4
období	období	k1gNnSc4
sucha	sucho	k1gNnSc2
a	a	k8xC
dešťů	dešť	k1gInPc2
–	–	k?
ty	ten	k3xDgInPc1
dosahují	dosahovat	k5eAaImIp3nP
maxima	maximum	k1gNnPc4
v	v	k7c6
letních	letní	k2eAgInPc6d1
měsících	měsíc	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
východním	východní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
závisí	záviset	k5eAaImIp3nS
srážková	srážkový	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
v	v	k7c6
létě	léto	k1gNnSc6
na	na	k7c6
pasátu	pasát	k1gInSc6
vanoucím	vanoucí	k2eAgInSc6d1
z	z	k7c2
Atlantiku	Atlantik	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
zimě	zima	k1gFnSc6
na	na	k7c6
větru	vítr	k1gInSc6
ze	z	k7c2
severu	sever	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pacifické	pacifický	k2eAgNnSc1d1
pobřeží	pobřeží	k1gNnSc1
je	být	k5eAaImIp3nS
sušší	suchý	k2eAgNnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláště	zvláště	k6eAd1
Kalifornský	kalifornský	k2eAgInSc1d1
poloostrov	poloostrov	k1gInSc1
a	a	k8xC
severní	severní	k2eAgFnSc1d1
část	část	k1gFnSc1
západního	západní	k2eAgNnSc2d1
Mexika	Mexiko	k1gNnSc2
má	mít	k5eAaImIp3nS
srážek	srážka	k1gFnPc2
málo	málo	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Flóra	Flóra	k1gFnSc1
</s>
<s>
Na	na	k7c6
jihu	jih	k1gInSc6
a	a	k8xC
východě	východ	k1gInSc6
se	se	k3xPyFc4
setkáváme	setkávat	k5eAaImIp1nP
s	s	k7c7
tropickou	tropický	k2eAgFnSc7d1
flórou	flóra	k1gFnSc7
v	v	k7c6
typickém	typický	k2eAgInSc6d1
deštném	deštný	k2eAgInSc6d1
lese	les	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Různé	různý	k2eAgInPc1d1
druhy	druh	k1gInPc1
palem	palma	k1gFnPc2
propletených	propletený	k2eAgFnPc2d1
liánami	liána	k1gFnPc7
<g/>
,	,	kIx,
oživeny	oživen	k2eAgInPc1d1
pestře	pestro	k6eAd1
zbarvenými	zbarvený	k2eAgFnPc7d1
orchidejemi	orchidea	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pěstuje	pěstovat	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
palma	palma	k1gFnSc1
olejná	olejný	k2eAgFnSc1d1
<g/>
,	,	kIx,
cukrová	cukrový	k2eAgFnSc1d1
třtina	třtina	k1gFnSc1
<g/>
,	,	kIx,
sisal	sisal	k1gInSc1
<g/>
,	,	kIx,
kokosovník	kokosovník	k1gInSc1
<g/>
,	,	kIx,
kakaovník	kakaovník	k1gInSc1
<g/>
,	,	kIx,
káva	káva	k1gFnSc1
a	a	k8xC
kukuřice	kukuřice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
hranicí	hranice	k1gFnSc7
věčného	věčný	k2eAgInSc2d1
ledu	led	k1gInSc2
na	na	k7c6
vrcholcích	vrcholek	k1gInPc6
hor	hora	k1gFnPc2
nalézáme	nalézat	k5eAaImIp1nP
naopak	naopak	k6eAd1
severoamerickou	severoamerický	k2eAgFnSc4d1
holoarktickou	holoarktický	k2eAgFnSc4d1
flóru	flóra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
povrch	povrch	k1gInSc4
Mexika	Mexiko	k1gNnSc2
není	být	k5eNaImIp3nS
příliš	příliš	k6eAd1
zalesněn	zalesněn	k2eAgMnSc1d1
<g/>
,	,	kIx,
druhů	druh	k1gInPc2
stromů	strom	k1gInPc2
zde	zde	k6eAd1
roste	růst	k5eAaImIp3nS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvlášť	zvlášť	k6eAd1
četně	četně	k6eAd1
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
cedr	cedr	k1gInSc1
<g/>
,	,	kIx,
dub	dub	k1gInSc1
a	a	k8xC
borovice	borovice	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c6
vyšších	vysoký	k2eAgFnPc6d2
polohách	poloha	k1gFnPc6
jedle	jedle	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářský	hospodářský	k2eAgInSc1d1
význam	význam	k1gInSc1
má	mít	k5eAaImIp3nS
i	i	k9
dřevo	dřevo	k1gNnSc1
tropických	tropický	k2eAgInPc2d1
stromů	strom	k1gInPc2
<g/>
,	,	kIx,
užívané	užívaný	k2eAgInPc1d1
v	v	k7c6
nábytkářském	nábytkářský	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
(	(	kIx(
<g/>
mahagon	mahagon	k1gInSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
k	k	k7c3
barvení	barvení	k1gNnSc3
kampešek	kampeška	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charakteristickou	charakteristický	k2eAgFnSc7d1
rostlinou	rostlina	k1gFnSc7
celého	celý	k2eAgNnSc2d1
Mexika	Mexiko	k1gNnSc2
je	být	k5eAaImIp3nS
kaktus	kaktus	k1gInSc1
<g/>
,	,	kIx,
vyskytující	vyskytující	k2eAgInPc1d1
se	se	k3xPyFc4
ve	v	k7c6
stovkách	stovka	k1gFnPc6
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
horských	horský	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
Mexika	Mexiko	k1gNnSc2
se	se	k3xPyFc4
pěstuje	pěstovat	k5eAaImIp3nS
cukrová	cukrový	k2eAgFnSc1d1
třtina	třtina	k1gFnSc1
<g/>
,	,	kIx,
káva	káva	k1gFnSc1
<g/>
,	,	kIx,
pšenice	pšenice	k1gFnSc1
a	a	k8xC
proso	proso	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severněji	severně	k6eAd2
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
pastviny	pastvina	k1gFnPc1
a	a	k8xC
louky	louka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Mexiku	Mexiko	k1gNnSc6
se	se	k3xPyFc4
pěstuje	pěstovat	k5eAaImIp3nS
také	také	k9
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
tropického	tropický	k2eAgNnSc2d1
ovoce	ovoce	k1gNnSc2
a	a	k8xC
různé	různý	k2eAgFnSc2d1
zeleniny	zelenina	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
chilli	chilli	k1gNnSc4
a	a	k8xC
rajčata	rajče	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zcela	zcela	k6eAd1
na	na	k7c6
severu	sever	k1gInSc6
země	zem	k1gFnSc2
se	se	k3xPyFc4
daří	dařit	k5eAaImIp3nS
bavlníku	bavlník	k1gInSc2
<g/>
,	,	kIx,
zejména	zejména	k9
při	při	k7c6
ústí	ústí	k1gNnSc6
řek	řeka	k1gFnPc2
Colorado	Colorado	k1gNnSc1
a	a	k8xC
Rio	Rio	k1gFnSc1
Grande	grand	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s>
Fauna	fauna	k1gFnSc1
</s>
<s>
Stojící	stojící	k2eAgMnSc1d1
jaguár	jaguár	k1gMnSc1
</s>
<s>
Zvířena	zvířena	k1gFnSc1
čítá	čítat	k5eAaImIp3nS
tisíce	tisíc	k4xCgInPc4
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
většinu	většina	k1gFnSc4
tvoří	tvořit	k5eAaImIp3nP
hmyz	hmyz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velice	velice	k6eAd1
nepříjemní	příjemný	k2eNgMnPc1d1
jsou	být	k5eAaImIp3nP
moskyti	moskyt	k1gMnPc1
<g/>
,	,	kIx,
přenášející	přenášející	k2eAgFnSc4d1
malárii	malárie	k1gFnSc4
a	a	k8xC
žlutou	žlutý	k2eAgFnSc4d1
zimnici	zimnice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Mexiku	Mexiko	k1gNnSc6
nacházíme	nacházet	k5eAaImIp1nP
zvířata	zvíře	k1gNnPc4
<g/>
,	,	kIx,
žijící	žijící	k2eAgFnSc4d1
v	v	k7c6
obou	dva	k4xCgFnPc6
částech	část	k1gFnPc6
amerického	americký	k2eAgInSc2d1
kontinentu	kontinent	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
pocházejí	pocházet	k5eAaImIp3nP
medvědi	medvěd	k1gMnPc1
<g/>
,	,	kIx,
vydry	vydra	k1gFnPc1
<g/>
,	,	kIx,
jeleni	jelen	k1gMnPc1
a	a	k8xC
jelencii	jelencie	k1gFnSc4
<g/>
,	,	kIx,
z	z	k7c2
Jižní	jižní	k2eAgFnSc2d1
pak	pak	k6eAd1
jaguár	jaguár	k1gMnSc1
<g/>
,	,	kIx,
pásovec	pásovec	k1gMnSc1
<g/>
,	,	kIx,
mravenečník	mravenečník	k1gMnSc1
a	a	k8xC
množství	množství	k1gNnSc1
opic	opice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žijí	žít	k5eAaImIp3nP
zde	zde	k6eAd1
i	i	k9
dikobrazové	dikobraz	k1gMnPc1
<g/>
,	,	kIx,
různí	různý	k2eAgMnPc1d1
hlodavci	hlodavec	k1gMnPc1
a	a	k8xC
mnoho	mnoho	k4c1
dalších	další	k2eAgMnPc2d1
savců	savec	k1gMnPc2
jako	jako	k8xC,k8xS
například	například	k6eAd1
tapír	tapír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
zvířat	zvíře	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yQgNnPc4,k3yRgNnPc4,k3yIgNnPc4
dovezli	dovézt	k5eAaPmAgMnP
do	do	k7c2
Ameriky	Amerika	k1gFnSc2
až	až	k9
Evropané	Evropan	k1gMnPc1
<g/>
,	,	kIx,
najdeme	najít	k5eAaPmIp1nP
ve	v	k7c6
volné	volný	k2eAgFnSc6d1
přírodě	příroda	k1gFnSc6
divoké	divoký	k2eAgMnPc4d1
potomky	potomek	k1gMnPc4
španělských	španělský	k2eAgInPc2d1
koní	kůň	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
se	se	k3xPyFc4
v	v	k7c6
Mexiku	Mexiko	k1gNnSc6
vyskytuje	vyskytovat	k5eAaImIp3nS
mnoho	mnoho	k4c1
obojživelníků	obojživelník	k1gMnPc2
a	a	k8xC
plazů	plaz	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgFnSc7d3
nebezpečí	nebezpeč	k1gFnSc7wB
představují	představovat	k5eAaImIp3nP
četní	četný	k2eAgMnPc1d1
a	a	k8xC
prudce	prudko	k6eAd1
jedovatí	jedovatý	k2eAgMnPc1d1
chřestýši	chřestýš	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
ptačí	ptačí	k2eAgFnSc2d1
říše	říš	k1gFnSc2
můžeme	moct	k5eAaImIp1nP
jmenovat	jmenovat	k5eAaImF,k5eAaBmF
zase	zase	k9
řadu	řada	k1gFnSc4
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
od	od	k7c2
maličkých	maličký	k2eAgMnPc2d1
kolibříků	kolibřík	k1gMnPc2
až	až	k9
po	po	k7c4
supy	sup	k1gMnPc4
a	a	k8xC
orly	orel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejznámějším	známý	k2eAgMnSc7d3
mexickým	mexický	k2eAgMnSc7d1
ptákem	pták	k1gMnSc7
je	být	k5eAaImIp3nS
však	však	k9
patrně	patrně	k6eAd1
krocan	krocan	k1gMnSc1
<g/>
,	,	kIx,
divoký	divoký	k2eAgMnSc1d1
předek	předek	k1gMnSc1
dnes	dnes	k6eAd1
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
rozšířeného	rozšířený	k2eAgMnSc2d1
krocana	krocan	k1gMnSc2
domácího	domácí	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Střed	střed	k1gInSc1
země	zem	k1gFnSc2
se	se	k3xPyFc4
vyznačuje	vyznačovat	k5eAaImIp3nS
zejména	zejména	k9
chovem	chov	k1gInSc7
skotu	skot	k1gInSc2
<g/>
,	,	kIx,
prasat	prase	k1gNnPc2
<g/>
,	,	kIx,
koní	kůň	k1gMnPc2
<g/>
,	,	kIx,
koz	koza	k1gFnPc2
a	a	k8xC
ovcí	ovce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
pobřeží	pobřeží	k1gNnSc6
s	s	k7c7
Mexickým	mexický	k2eAgInSc7d1
zálivem	záliv	k1gInSc7
se	se	k3xPyFc4
loví	lovit	k5eAaImIp3nP
zejména	zejména	k9
krabi	krab	k1gMnPc1
a	a	k8xC
langusty	langusta	k1gFnPc1
<g/>
,	,	kIx,
ve	v	k7c6
vodách	voda	k1gFnPc6
Tichého	Tichého	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
krabi	krab	k1gMnPc1
a	a	k8xC
ústřice	ústřice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vodách	voda	k1gFnPc6
severní	severní	k2eAgFnSc2d1
části	část	k1gFnSc2
Kalifornského	kalifornský	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
žije	žít	k5eAaImIp3nS
kriticky	kriticky	k6eAd1
ohrožená	ohrožený	k2eAgFnSc1d1
sviňucha	sviňucha	k1gFnSc1
kalifornská	kalifornský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
V	v	k7c6
Mexiku	Mexiko	k1gNnSc6
funguje	fungovat	k5eAaImIp3nS
rozsáhlá	rozsáhlý	k2eAgFnSc1d1
síť	síť	k1gFnSc1
různých	různý	k2eAgNnPc2d1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
spravuje	spravovat	k5eAaImIp3nS
státní	státní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Národní	národní	k2eAgFnSc2d1
komise	komise	k1gFnSc2
pro	pro	k7c4
přírodní	přírodní	k2eAgNnPc4d1
chráněná	chráněný	k2eAgNnPc4d1
území	území	k1gNnPc4
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
Comisión	Comisión	k1gMnSc1
nacional	nacionat	k5eAaImAgMnS,k5eAaPmAgMnS
de	de	k?
áreas	áreas	k1gMnSc1
naturales	naturales	k1gMnSc1
protegidas	protegidas	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
září	září	k1gNnSc6
2011	#num#	k4
existovalo	existovat	k5eAaImAgNnS
celkem	celek	k1gInSc7
174	#num#	k4
území	území	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
zařazena	zařadit	k5eAaPmNgFnS
pod	pod	k7c4
jistý	jistý	k2eAgInSc4d1
stupeň	stupeň	k1gInSc4
ochrany	ochrana	k1gFnSc2
(	(	kIx(
<g/>
41	#num#	k4
biosférických	biosférický	k2eAgFnPc2d1
rezervací	rezervace	k1gFnPc2
<g/>
,	,	kIx,
67	#num#	k4
národních	národní	k2eAgInPc2d1
parků	park	k1gInPc2
<g/>
,	,	kIx,
5	#num#	k4
přírodních	přírodní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
<g/>
,	,	kIx,
8	#num#	k4
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
přírodních	přírodní	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
,	,	kIx,
35	#num#	k4
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
fauny	fauna	k1gFnSc2
a	a	k8xC
flory	flora	k1gFnSc2
a	a	k8xC
18	#num#	k4
tzv.	tzv.	kA
útočišť	útočiště	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Souhrnná	souhrnný	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
všech	všecek	k3xTgNnPc2
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
dosahovala	dosahovat	k5eAaImAgFnS
253	#num#	k4
848	#num#	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pět	pět	k4xCc1
z	z	k7c2
těchto	tento	k3xDgNnPc2
území	území	k1gNnPc2
figuruje	figurovat	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
na	na	k7c6
seznamu	seznam	k1gInSc6
světového	světový	k2eAgNnSc2d1
přírodního	přírodní	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	Unesco	k1gNnSc1
(	(	kIx(
<g/>
El	Ela	k1gFnPc2
Vizcaíno	Vizcaína	k1gFnSc5
<g/>
,	,	kIx,
Sian	Sian	k1gMnSc1
Ka	Ka	k1gMnSc1
<g/>
'	'	kIx"
<g/>
an	an	k?
<g/>
,	,	kIx,
ostrovy	ostrov	k1gInPc1
a	a	k8xC
chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
Kalifornského	kalifornský	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
<g/>
,	,	kIx,
biosférická	biosférický	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
monarchy	monarcha	k1gMnSc2
stěhovavého	stěhovavý	k2eAgMnSc2d1
a	a	k8xC
biosférická	biosférický	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
El	Ela	k1gFnPc2
Pinacate	Pinacat	k1gInSc5
a	a	k8xC
Gran	Grana	k1gFnPc2
Desierto	Desierta	k1gFnSc5
de	de	k?
Altar	Altar	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Politický	politický	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Uspořádání	uspořádání	k1gNnSc1
státu	stát	k1gInSc2
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1
mexický	mexický	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Enrique	Enriqu	k1gInSc2
Peñ	Peñ	k1gNnSc2
Nieto	Nieto	k1gNnSc1
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
mexické	mexický	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
federativní	federativní	k2eAgFnSc7d1
republikou	republika	k1gFnSc7
s	s	k7c7
ústavou	ústava	k1gFnSc7
z	z	k7c2
roku	rok	k1gInSc2
1917	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yQgFnSc6,k3yRgFnSc6,k3yIgFnSc6
je	být	k5eAaImIp3nS
zakotveno	zakotvit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
volební	volební	k2eAgNnSc4d1
právo	právo	k1gNnSc4
mají	mít	k5eAaImIp3nP
všichni	všechen	k3xTgMnPc1
dospělí	dospělý	k2eAgMnPc1d1
občané	občan	k1gMnPc1
starší	starý	k2eAgMnPc1d2
18	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mexiko	Mexiko	k1gNnSc1
má	mít	k5eAaImIp3nS
tzv.	tzv.	kA
prezidentský	prezidentský	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavou	hlava	k1gFnSc7
státu	stát	k1gInSc2
i	i	k8xC
vlády	vláda	k1gFnSc2
je	být	k5eAaImIp3nS
prezident	prezident	k1gMnSc1
(	(	kIx(
<g/>
presidente	president	k1gMnSc5
de	de	k?
México	México	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
je	být	k5eAaImIp3nS
volen	volit	k5eAaImNgInS
lidem	lid	k1gInSc7
při	při	k7c6
spolkových	spolkový	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
na	na	k7c4
jediné	jediný	k2eAgNnSc4d1
šestileté	šestiletý	k2eAgNnSc4d1
období	období	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezident	prezident	k1gMnSc1
soustřeďuje	soustřeďovat	k5eAaImIp3nS
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
rukou	ruka	k1gFnPc6
veškerou	veškerý	k3xTgFnSc4
exekutivní	exekutivní	k2eAgFnSc1d1
moc	moc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Zákonodárným	zákonodárný	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
je	být	k5eAaImIp3nS
dvoukomorový	dvoukomorový	k2eAgInSc1d1
Národní	národní	k2eAgInSc1d1
kongres	kongres	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
500	#num#	k4
členů	člen	k1gMnPc2
poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
jsou	být	k5eAaImIp3nP
3	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
voleny	volen	k2eAgMnPc4d1
přímo	přímo	k6eAd1
a	a	k8xC
1	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
podle	podle	k7c2
počtu	počet	k1gInSc2
hlasů	hlas	k1gInPc2
získaných	získaný	k2eAgInPc2d1
jednotlivými	jednotlivý	k2eAgFnPc7d1
stranami	strana	k1gFnPc7
<g/>
,	,	kIx,
všichni	všechen	k3xTgMnPc1
na	na	k7c6
3	#num#	k4
roky	rok	k1gInPc4
<g/>
.	.	kIx.
64	#num#	k4
senátorů	senátor	k1gMnPc2
je	být	k5eAaImIp3nS
voleno	volit	k5eAaImNgNnS
na	na	k7c4
6	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
po	po	k7c6
dvou	dva	k4xCgNnPc6
z	z	k7c2
každého	každý	k3xTgInSc2
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc1
stát	stát	k1gInSc4
si	se	k3xPyFc3
volí	volit	k5eAaImIp3nP
vlastního	vlastní	k2eAgMnSc4d1
guvernéra	guvernér	k1gMnSc4
a	a	k8xC
zákonodárný	zákonodárný	k2eAgInSc4d1
sbor	sbor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
Ciudad	Ciudad	k1gInSc1
de	de	k?
México	México	k6eAd1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
s	s	k7c7
okolím	okolí	k1gNnSc7
<g/>
,	,	kIx,
guvernéra	guvernér	k1gMnSc4
jmenuje	jmenovat	k5eAaBmIp3nS,k5eAaImIp3nS
prezident	prezident	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
je	být	k5eAaImIp3nS
mexickým	mexický	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
Enrique	Enriqu	k1gFnSc2
Peñ	Peñ	k1gNnSc2
Nieto	Niet	k2eAgNnSc1d1
(	(	kIx(
<g/>
*	*	kIx~
20	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1966	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
je	být	k5eAaImIp3nS
členem	člen	k1gMnSc7
tradiční	tradiční	k2eAgFnSc2d1
Revoluční	revoluční	k2eAgFnSc2d1
instituční	instituční	k2eAgFnSc2d1
strany	strana	k1gFnSc2
(	(	kIx(
<g/>
Partido	Partida	k1gFnSc5
Revolucionario	Revolucionario	k1gNnSc4
Institucional	Institucional	k1gFnSc3
<g/>
,	,	kIx,
PRI	PRI	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nieto	Nieto	k1gNnSc1
nemohl	moct	k5eNaImAgInS
podle	podle	k7c2
mexické	mexický	k2eAgFnSc2d1
ústavy	ústava	k1gFnSc2
ve	v	k7c6
spolkových	spolkový	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
2018	#num#	k4
znovu	znovu	k6eAd1
kandidovat	kandidovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novou	nový	k2eAgFnSc7d1
hlavou	hlava	k1gFnSc7
státu	stát	k1gInSc2
byl	být	k5eAaImAgMnS
dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2018	#num#	k4
zvolen	zvolen	k2eAgInSc1d1
Andrés	Andrés	k1gInSc1
Manuel	Manuel	k1gMnSc1
López	López	k1gMnSc1
Obrador	Obrador	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
13	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1953	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
úřadu	úřad	k1gInSc2
ujme	ujmout	k5eAaPmIp3nS
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
López	López	k1gMnSc1
Obrador	Obrador	k1gMnSc1
kandidoval	kandidovat	k5eAaImAgMnS
za	za	k7c4
širokou	široký	k2eAgFnSc4d1
volební	volební	k2eAgFnSc4d1
koalici	koalice	k1gFnSc4
<g/>
;	;	kIx,
sám	sám	k3xTgMnSc1
je	být	k5eAaImIp3nS
vedoucím	vedoucí	k2eAgMnSc7d1
činitelem	činitel	k1gMnSc7
Hnutí	hnutí	k1gNnSc2
národní	národní	k2eAgFnSc2d1
obrody	obroda	k1gFnSc2
(	(	kIx(
<g/>
Movimiento	Movimiento	k1gNnSc1
Regeneración	Regeneración	k1gMnSc1
Nacional	Nacional	k1gMnSc1
<g/>
,	,	kIx,
MRN	MRN	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Prezident	prezident	k1gMnSc1
Andrés	Andrésa	k1gFnPc2
Manuel	Manuel	k1gMnSc1
López	López	k1gMnSc1
Obrador	Obrador	k1gMnSc1
24	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2020	#num#	k4
</s>
<s>
Zahraniční	zahraniční	k2eAgFnSc1d1
politika	politika	k1gFnSc1
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
je	být	k5eAaImIp3nS
členem	člen	k1gInSc7
mnoha	mnoho	k4c2
mezinárodních	mezinárodní	k2eAgFnPc2d1
celosvětových	celosvětový	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
jako	jako	k9
OSN	OSN	kA
<g/>
,	,	kIx,
OECD	OECD	kA
<g/>
,	,	kIx,
G	G	kA
<g/>
20	#num#	k4
<g/>
,	,	kIx,
APEC	APEC	kA
<g/>
,	,	kIx,
Interpol	interpol	k1gInSc1
<g/>
,	,	kIx,
UNESCO	UNESCO	kA
atd.	atd.	kA
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
je	být	k5eAaImIp3nS
zapojené	zapojený	k2eAgNnSc1d1
do	do	k7c2
několika	několik	k4yIc2
organizací	organizace	k1gFnPc2
působících	působící	k2eAgFnPc2d1
pouze	pouze	k6eAd1
na	na	k7c6
americkém	americký	k2eAgInSc6d1
kontinentu	kontinent	k1gInSc6
(	(	kIx(
<g/>
např.	např.	kA
USMCA	USMCA	kA
<g/>
,	,	kIx,
Organizace	organizace	k1gFnSc1
amerických	americký	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
Společenství	společenství	k1gNnSc1
latinskoamerických	latinskoamerický	k2eAgInPc2d1
a	a	k8xC
karibských	karibský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
Latinskoamerické	latinskoamerický	k2eAgNnSc1d1
integrační	integrační	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
<g/>
,	,	kIx,
Pacifická	pacifický	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
<g/>
,	,	kIx,
Sdružení	sdružení	k1gNnSc1
karibských	karibský	k2eAgInPc2d1
států	stát	k1gInPc2
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vztahy	vztah	k1gInPc1
s	s	k7c7
Československem	Československo	k1gNnSc7
a	a	k8xC
Českou	český	k2eAgFnSc7d1
republikou	republika	k1gFnSc7
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Česko-mexické	česko-mexický	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
navázalo	navázat	k5eAaPmAgNnS
diplomatické	diplomatický	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
s	s	k7c7
Československem	Československo	k1gNnSc7
v	v	k7c6
roce	rok	k1gInSc6
1922	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Diplomatické	diplomatický	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
byly	být	k5eAaImAgInP
nakrátko	nakrátko	k6eAd1
přerušeny	přerušit	k5eAaPmNgInP
mezi	mezi	k7c7
roky	rok	k1gInPc7
1939	#num#	k4
a	a	k8xC
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1936	#num#	k4
prezident	prezident	k1gMnSc1
Lázaro	Lázara	k1gFnSc5
Cárdenas	Cárdenasa	k1gFnPc2
pojmenoval	pojmenovat	k5eAaPmAgMnS
tehdy	tehdy	k6eAd1
neznámou	známý	k2eNgFnSc4d1
ulici	ulice	k1gFnSc4
na	na	k7c6
okraji	okraj	k1gInSc6
Ciudad	Ciudad	k1gInSc1
de	de	k?
México	México	k6eAd1
jménem	jméno	k1gNnSc7
Tomáše	Tomáš	k1gMnSc2
Garrigue	Garrigu	k1gMnSc2
Masaryka	Masaryk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současný	současný	k2eAgInSc1d1
název	název	k1gInSc1
této	tento	k3xDgFnSc2
třídy	třída	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nejluxusnějších	luxusní	k2eAgFnPc2d3
nákupních	nákupní	k2eAgFnPc2d1
ulic	ulice	k1gFnPc2
v	v	k7c6
mexickém	mexický	k2eAgNnSc6d1
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
Avenida	Avenida	k1gFnSc1
Presidente	president	k1gMnSc5
Masaryk	Masaryk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
zde	zde	k6eAd1
byl	být	k5eAaImAgInS
na	na	k7c6
prestižním	prestižní	k2eAgNnSc6d1
místě	místo	k1gNnSc6
postaven	postaven	k2eAgInSc4d1
pomník	pomník	k1gInSc4
TGM	TGM	kA
<g/>
,	,	kIx,
financovaný	financovaný	k2eAgInSc4d1
do	do	k7c2
značné	značný	k2eAgFnSc2d1
míry	míra	k1gFnSc2
českými	český	k2eAgMnPc7d1
krajany	krajan	k1gMnPc7
v	v	k7c6
Mexiku	Mexiko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
Avenida	Avenida	k1gFnSc1
Presidente	president	k1gMnSc5
Masaryk	Masaryk	k1gMnSc1
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nejznámějších	známý	k2eAgFnPc2d3
ulic	ulice	k1gFnPc2
ve	v	k7c6
městě	město	k1gNnSc6
a	a	k8xC
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
nejdražší	drahý	k2eAgFnPc4d3
obchodní	obchodní	k2eAgFnPc4d1
třídy	třída	k1gFnPc4
v	v	k7c6
celé	celý	k2eAgFnSc6d1
Latinské	latinský	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
měla	mít	k5eAaImAgFnS
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
své	svůj	k3xOyFgNnSc4
velvyslanectví	velvyslanectví	k1gNnSc4
v	v	k7c4
Ciudad	Ciudad	k1gInSc4
de	de	k?
México	México	k6eAd1
a	a	k8xC
honorární	honorární	k2eAgInPc1d1
konzuláty	konzulát	k1gInPc1
v	v	k7c6
Guadalajaře	Guadalajara	k1gFnSc6
<g/>
,	,	kIx,
Tijuaně	Tijuana	k1gFnSc6
a	a	k8xC
Monterrey	Monterrea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mexiko	Mexiko	k1gNnSc1
má	mít	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
ambasádu	ambasáda	k1gFnSc4
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Územní	územní	k2eAgNnSc1d1
rozdělení	rozdělení	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Státy	stát	k1gInPc1
Mexika	Mexiko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Stát	stát	k1gInSc1
je	být	k5eAaImIp3nS
rozdělen	rozdělit	k5eAaPmNgInS
na	na	k7c4
31	#num#	k4
spolkových	spolkový	k2eAgInPc2d1
států	stát	k1gInPc2
a	a	k8xC
distrikt	distrikt	k1gInSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
názvy	název	k1gInPc4
států	stát	k1gInPc2
a	a	k8xC
měst	město	k1gNnPc2
nesou	nést	k5eAaImIp3nP
jména	jméno	k1gNnPc4
historických	historický	k2eAgFnPc2d1
osobností	osobnost	k1gFnPc2
Mexika	Mexiko	k1gNnSc2
<g/>
,	,	kIx,
jiné	jiný	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
indiánského	indiánský	k2eAgMnSc4d1
původu	původa	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aguascalientes	Aguascalientes	k1gInSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Baja	Baja	k1gFnSc1
California	Californium	k1gNnSc2
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Baja	Baja	k1gFnSc1
California	Californium	k1gNnSc2
Sur	Sur	k1gFnSc2
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Campeche	Campech	k1gInSc2
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chiapas	Chiapas	k1gInSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chihuahua	Chihuahu	k1gInSc2
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Coahuila	Coahuila	k1gFnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Colima	Colima	k1gNnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Durango	Durango	k1gNnSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Guanajuato	Guanajuat	k2eAgNnSc4d1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Guerrero	Guerrero	k1gNnSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hidalgo	Hidalgo	k1gMnSc1
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jalisco	Jalisco	k1gNnSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Michoacán	Michoacán	k2eAgInSc1d1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morelos	Morelos	k1gInSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nayarit	Nayarit	k1gInSc1
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nuevo	Nuevo	k1gNnSc1
León	Leóna	k1gFnPc2
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oaxaca	Oaxac	k1gInSc2
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Puebla	Puebla	k1gFnSc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Querétaro	Querétara	k1gFnSc5
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Quintana	Quintana	k1gFnSc1
Roo	Roo	k1gFnSc1
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
San	San	k1gFnSc1
Luis	Luisa	k1gFnPc2
Potosí	Potosý	k2eAgMnPc5d1
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sinaloa	Sinalo	k1gInSc2
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sonora	sonora	k1gFnSc1
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tabasco	Tabasco	k1gNnSc1
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tamaulipas	Tamaulipas	k1gInSc1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tlaxcala	Tlaxcala	k1gFnSc1
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veracruz	Veracruz	k1gInSc1
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Yucatán	Yucatán	k1gMnSc1
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zacatecas	Zacatecas	k1gInSc1
</s>
<s>
Ciudad	Ciudad	k1gInSc1
de	de	k?
México	México	k6eAd1
</s>
<s>
Státní	státní	k2eAgInPc1d1
symboly	symbol	k1gInPc1
</s>
<s>
Vlajka	vlajka	k1gFnSc1
země	zem	k1gFnSc2
je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
třemi	tři	k4xCgInPc7
svislými	svislý	k2eAgInPc7d1
pruhy	pruh	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zleva	zleva	k6eAd1
zeleným	zelené	k1gNnSc7
(	(	kIx(
<g/>
nezávislost	nezávislost	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bílým	bílý	k1gMnSc7
(	(	kIx(
<g/>
čistota	čistota	k1gFnSc1
náboženství	náboženství	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
červeným	červená	k1gFnPc3
(	(	kIx(
<g/>
jednota	jednota	k1gFnSc1
spojení	spojení	k1gNnSc2
míšenecké	míšenecký	k2eAgFnSc2d1
<g/>
,	,	kIx,
indiánské	indiánský	k2eAgFnSc2d1
a	a	k8xC
španělské	španělský	k2eAgFnSc2d1
krve	krev	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uprostřed	uprostřed	k6eAd1
je	být	k5eAaImIp3nS
státní	státní	k2eAgInSc1d1
znak	znak	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1823	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znak	znak	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
hnědě	hnědě	k6eAd1
zbarvený	zbarvený	k2eAgMnSc1d1
orel	orel	k1gMnSc1
stojící	stojící	k2eAgMnSc1d1
na	na	k7c6
nopálovém	nopálový	k2eAgInSc6d1
kaktusu	kaktus	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
roste	růst	k5eAaImIp3nS
z	z	k7c2
vody	voda	k1gFnSc2
jezera	jezero	k1gNnSc2
<g/>
;	;	kIx,
orel	orel	k1gMnSc1
drží	držet	k5eAaImIp3nS
v	v	k7c6
zobáku	zobák	k1gInSc6
zeleného	zelený	k2eAgMnSc2d1
hada	had	k1gMnSc2
<g/>
,	,	kIx,
pod	pod	k7c7
ním	on	k3xPp3gInSc7
jsou	být	k5eAaImIp3nP
do	do	k7c2
půlkruhu	půlkruh	k1gInSc2
prohnuté	prohnutý	k2eAgFnSc2d1
ratolesti	ratolest	k1gFnSc2
dubu	dub	k1gInSc2
a	a	k8xC
vavřínu	vavřín	k1gInSc2
svázané	svázaný	k2eAgNnSc4d1
stužkou	stužka	k1gFnSc7
v	v	k7c6
národních	národní	k2eAgFnPc6d1
barvách	barva	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znak	znak	k1gInSc1
ilustruje	ilustrovat	k5eAaBmIp3nS
starou	starý	k2eAgFnSc4d1
aztéckou	aztécký	k2eAgFnSc4d1
legendu	legenda	k1gFnSc4
<g/>
,	,	kIx,
podle	podle	k7c2
níž	jenž	k3xRgFnSc2
putující	putující	k2eAgInSc1d1
indiánský	indiánský	k2eAgInSc1d1
lid	lid	k1gInSc1
měl	mít	k5eAaImAgInS
nalézt	nalézt	k5eAaBmF,k5eAaPmF
nový	nový	k2eAgInSc1d1
domov	domov	k1gInSc1
na	na	k7c6
ostrově	ostrov	k1gInSc6
uprostřed	uprostřed	k7c2
jezera	jezero	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
uvidí	uvidět	k5eAaPmIp3nS
tuto	tento	k3xDgFnSc4
scénu	scéna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
místě	místo	k1gNnSc6
na	na	k7c6
jezeře	jezero	k1gNnSc6
Texcoco	Texcoco	k6eAd1
byl	být	k5eAaImAgMnS
roku	rok	k1gInSc2
1325	#num#	k4
založen	založen	k2eAgInSc1d1
Tenochtitlán	Tenochtitlán	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jeho	jeho	k3xOp3gFnPc6
troskách	troska	k1gFnPc6
dnes	dnes	k6eAd1
stojí	stát	k5eAaImIp3nS
dnešní	dnešní	k2eAgNnSc1d1
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Mexika	Mexiko	k1gNnSc2
<g/>
,	,	kIx,
Ciudad	Ciudad	k1gInSc1
de	de	k?
Méxiko	Méxika	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Ekonomika	ekonomika	k1gFnSc1
Mexika	Mexiko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Ciudad	Ciudad	k1gInSc1
de	de	k?
México	México	k6eAd1
je	být	k5eAaImIp3nS
ekonomickým	ekonomický	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
země	zem	k1gFnSc2
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
je	být	k5eAaImIp3nS
třetí	třetí	k4xOgFnSc7
největší	veliký	k2eAgFnSc7d3
ekonomikou	ekonomika	k1gFnSc7
na	na	k7c6
americkém	americký	k2eAgInSc6d1
kontinentu	kontinent	k1gInSc6
(	(	kIx(
<g/>
po	po	k7c6
USA	USA	kA
a	a	k8xC
Brazílii	Brazílie	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahrnuje	zahrnovat	k5eAaImIp3nS
kombinaci	kombinace	k1gFnSc4
služeb	služba	k1gFnPc2
<g/>
,	,	kIx,
průmyslu	průmysl	k1gInSc2
a	a	k8xC
zemědělské	zemědělský	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
založená	založený	k2eAgFnSc1d1
především	především	k9
na	na	k7c6
exportu	export	k1gInSc6
surovin	surovina	k1gFnPc2
a	a	k8xC
výrobků	výrobek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlády	vláda	k1gFnPc1
Mexika	Mexiko	k1gNnSc2
umožnily	umožnit	k5eAaPmAgFnP
na	na	k7c6
konci	konec	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
vyšší	vysoký	k2eAgFnSc4d2
konkurenci	konkurence	k1gFnSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
mořských	mořský	k2eAgInPc2d1
přístavů	přístav	k1gInPc2
<g/>
,	,	kIx,
telekomunikačních	telekomunikační	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
<g/>
,	,	kIx,
výrobě	výroba	k1gFnSc3
elektrické	elektrický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
<g/>
,	,	kIx,
distribuci	distribuce	k1gFnSc4
zemního	zemní	k2eAgInSc2d1
plynu	plyn	k1gInSc2
za	za	k7c7
účelem	účel	k1gInSc7
modernizace	modernizace	k1gFnSc2
infrastruktury	infrastruktura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahraniční	zahraniční	k2eAgInSc1d1
obchod	obchod	k1gInSc1
Mexika	Mexiko	k1gNnSc2
je	být	k5eAaImIp3nS
založen	založit	k5eAaPmNgInS
na	na	k7c6
řadě	řada	k1gFnSc6
dohod	dohoda	k1gFnPc2
o	o	k7c6
volném	volný	k2eAgInSc6d1
obchodu	obchod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdůležitější	důležitý	k2eAgInSc1d3
z	z	k7c2
nich	on	k3xPp3gInPc2
je	být	k5eAaImIp3nS
Dohoda	dohoda	k1gFnSc1
USA-Mexiko-Kanada	USA-Mexiko-Kanada	k1gFnSc1
<g/>
,	,	kIx,
dále	daleko	k6eAd2
pak	pak	k6eAd1
dohody	dohoda	k1gFnPc1
s	s	k7c7
Evropskou	evropský	k2eAgFnSc7d1
unií	unie	k1gFnSc7
<g/>
,	,	kIx,
Japonskem	Japonsko	k1gNnSc7
a	a	k8xC
několika	několik	k4yIc7
státy	stát	k1gInPc7
Střední	střední	k2eAgFnSc2d1
a	a	k8xC
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
USA	USA	kA
a	a	k8xC
Kanady	Kanada	k1gFnSc2
směřovalo	směřovat	k5eAaImAgNnS
např.	např.	kA
v	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
čtvrtletí	čtvrtletí	k1gNnSc4
2014	#num#	k4
celých	celý	k2eAgInPc2d1
80	#num#	k4
<g/>
%	%	kIx~
mexického	mexický	k2eAgInSc2d1
exportu	export	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
byl	být	k5eAaImAgInS
odhadovaný	odhadovaný	k2eAgInSc1d1
hrubý	hrubý	k2eAgInSc1d1
domácí	domácí	k2eAgInSc1d1
produkt	produkt	k1gInSc1
podle	podle	k7c2
parity	parita	k1gFnSc2
kupní	kupní	k2eAgFnSc2d1
síly	síla	k1gFnSc2
15	#num#	k4
600	#num#	k4
$	$	kIx~
za	za	k7c4
rok	rok	k1gInSc4
na	na	k7c4
osobu	osoba	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
zemědělství	zemědělství	k1gNnSc6
pracovalo	pracovat	k5eAaImAgNnS
13	#num#	k4
<g/>
%	%	kIx~
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
průmyslu	průmysl	k1gInSc6
24	#num#	k4
<g/>
%	%	kIx~
a	a	k8xC
v	v	k7c6
sektoru	sektor	k1gInSc6
služeb	služba	k1gFnPc2
62	#num#	k4
<g/>
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýznamnější	významný	k2eAgFnSc1d3
exportní	exportní	k2eAgNnSc4d1
zboží	zboží	k1gNnSc4
je	být	k5eAaImIp3nS
surová	surový	k2eAgFnSc1d1
ropa	ropa	k1gFnSc1
<g/>
,	,	kIx,
motorová	motorový	k2eAgNnPc4d1
vozidla	vozidlo	k1gNnPc4
a	a	k8xC
jejich	jejich	k3xOp3gFnPc1
součásti	součást	k1gFnPc1
<g/>
,	,	kIx,
elektronika	elektronika	k1gFnSc1
a	a	k8xC
další	další	k2eAgNnSc1d1
průmyslové	průmyslový	k2eAgNnSc1d1
zboží	zboží	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Nerostné	nerostný	k2eAgNnSc4d1
bohatství	bohatství	k1gNnSc4
</s>
<s>
ropná	ropný	k2eAgFnSc1d1
plošina	plošina	k1gFnSc1
u	u	k7c2
pobřeží	pobřeží	k1gNnSc2
Tabasca	Tabasc	k1gInSc2
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
oplývá	oplývat	k5eAaImIp3nS
značným	značný	k2eAgNnSc7d1
přírodním	přírodní	k2eAgNnSc7d1
bohatstvím	bohatství	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
koloniální	koloniální	k2eAgFnSc6d1
době	doba	k1gFnSc6
bylo	být	k5eAaImAgNnS
ceněno	cenit	k5eAaImNgNnS
zvlášť	zvlášť	k6eAd1
stříbro	stříbro	k1gNnSc1
a	a	k8xC
zlato	zlato	k1gNnSc1
<g/>
,	,	kIx,
těžilo	těžit	k5eAaImAgNnS
se	se	k3xPyFc4
i	i	k9
olovo	olovo	k1gNnSc1
<g/>
,	,	kIx,
zinek	zinek	k1gInSc1
<g/>
,	,	kIx,
měď	měď	k1gFnSc1
a	a	k8xC
železo	železo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stříbrné	stříbrný	k2eAgFnPc4d1
rudy	ruda	k1gFnPc4
našli	najít	k5eAaPmAgMnP
geologové	geolog	k1gMnPc1
na	na	k7c6
celém	celý	k2eAgNnSc6d1
území	území	k1gNnSc6
Mexika	Mexiko	k1gNnSc2
<g/>
,	,	kIx,
bohatými	bohatý	k2eAgInPc7d1
doly	dol	k1gInPc7
vynikají	vynikat	k5eAaImIp3nP
zejména	zejména	k9
státy	stát	k1gInPc1
Zacatecas	Zacatecasa	k1gFnPc2
<g/>
,	,	kIx,
Durango	Durango	k1gMnSc1
<g/>
,	,	kIx,
Hidalgo	Hidalgo	k1gMnSc1
<g/>
,	,	kIx,
Guanajuato	Guanajuat	k2eAgNnSc1d1
a	a	k8xC
Chihuahua	Chihuahua	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
tvoří	tvořit	k5eAaImIp3nS
mexický	mexický	k2eAgInSc1d1
podíl	podíl	k1gInSc1
na	na	k7c6
světové	světový	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
víc	hodně	k6eAd2
než	než	k8xS
20	#num#	k4
%	%	kIx~
a	a	k8xC
Mexiko	Mexiko	k1gNnSc1
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
největším	veliký	k2eAgMnSc7d3
producentem	producent	k1gMnSc7
tohoto	tento	k3xDgInSc2
kovu	kov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Menší	malý	k2eAgInSc1d2
význam	význam	k1gInSc1
má	mít	k5eAaImIp3nS
těžba	těžba	k1gFnSc1
zlata	zlato	k1gNnSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc4
naleziště	naleziště	k1gNnSc4
jsou	být	k5eAaImIp3nP
už	už	k6eAd1
částečně	částečně	k6eAd1
vyčerpána	vyčerpán	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nacházejí	nacházet	k5eAaImIp3nP
se	se	k3xPyFc4
hlavně	hlavně	k9
ve	v	k7c6
státech	stát	k1gInPc6
Durango	Durango	k6eAd1
<g/>
,	,	kIx,
Sonora	sonora	k1gFnSc1
<g/>
,	,	kIx,
Chihuahua	Chihuahua	k1gFnSc1
a	a	k8xC
Querétaro	Querétara	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
ostatních	ostatní	k2eAgFnPc2d1
kovových	kovový	k2eAgFnPc2d1
rud	ruda	k1gFnPc2
má	mít	k5eAaImIp3nS
značnou	značný	k2eAgFnSc4d1
důležitost	důležitost	k1gFnSc4
ruda	rudo	k1gNnSc2
zinková	zinkový	k2eAgFnSc1d1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc2
největší	veliký	k2eAgFnSc2d3
pánve	pánev	k1gFnSc2
můžeme	moct	k5eAaImIp1nP
najít	najít	k5eAaPmF
ve	v	k7c6
stejných	stejný	k2eAgInPc6d1
státech	stát	k1gInPc6
jako	jako	k8xS,k8xC
doly	dol	k1gInPc4
na	na	k7c4
stříbro	stříbro	k1gNnSc4
a	a	k8xC
zlato	zlato	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Totéž	týž	k3xTgNnSc1
platí	platit	k5eAaImIp3nS
pro	pro	k7c4
olovnaté	olovnatý	k2eAgFnPc4d1
a	a	k8xC
měděné	měděný	k2eAgFnPc4d1
rudy	ruda	k1gFnPc4
<g/>
,	,	kIx,
známé	známý	k2eAgMnPc4d1
vysokým	vysoký	k2eAgInSc7d1
obsahem	obsah	k1gInSc7
kovů	kov	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
poslední	poslední	k2eAgFnSc6d1
době	doba	k1gFnSc6
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
intenzivnímu	intenzivní	k2eAgInSc3d1
rozvoji	rozvoj	k1gInSc3
těžby	těžba	k1gFnSc2
železných	železný	k2eAgFnPc2d1
rud	ruda	k1gFnPc2
v	v	k7c6
nově	nově	k6eAd1
objevených	objevený	k2eAgNnPc6d1
ložiscích	ložisko	k1gNnPc6
na	na	k7c6
západě	západ	k1gInSc6
země	zem	k1gFnSc2
(	(	kIx(
<g/>
Cerro	Cerro	k1gNnSc1
del	del	k?
Mercado	Mercada	k1gFnSc5
<g/>
,	,	kIx,
El	Ela	k1gFnPc2
Mamen	Mamna	k1gFnPc2
<g/>
,	,	kIx,
La	la	k1gNnSc4
Perla	perla	k1gFnSc1
a	a	k8xC
El	Ela	k1gFnPc2
Joven	Jovna	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruda	ruda	k1gFnSc1
tu	tu	k6eAd1
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
obsahuje	obsahovat	k5eAaImIp3nS
až	až	k9
60	#num#	k4
%	%	kIx~
železa	železo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1959	#num#	k4
objevili	objevit	k5eAaPmAgMnP
v	v	k7c6
Mexiku	Mexiko	k1gNnSc6
i	i	k8xC
bohatá	bohatý	k2eAgNnPc4d1
naleziště	naleziště	k1gNnSc4
uranu	uran	k1gInSc2
<g/>
,	,	kIx,
významná	významný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
i	i	k9
těžba	těžba	k1gFnSc1
síry	síra	k1gFnSc2
<g/>
,	,	kIx,
grafitu	grafit	k1gInSc2
<g/>
,	,	kIx,
rtuti	rtuť	k1gFnSc2
<g/>
,	,	kIx,
cínu	cín	k1gInSc2
a	a	k8xC
antimonu	antimon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
se	se	k3xPyFc4
těží	těžet	k5eAaImIp3nS
uhlí	uhlí	k1gNnSc2
<g/>
,	,	kIx,
molybden	molybden	k1gInSc1
<g/>
,	,	kIx,
wolfram	wolfram	k1gInSc1
<g/>
,	,	kIx,
kadmium	kadmium	k1gNnSc1
a	a	k8xC
řada	řada	k1gFnSc1
dalších	další	k2eAgInPc2d1
nerostů	nerost	k1gInPc2
a	a	k8xC
rud	ruda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3
nerostné	nerostný	k2eAgNnSc1d1
bohatství	bohatství	k1gNnSc1
představuje	představovat	k5eAaImIp3nS
z	z	k7c2
dnešního	dnešní	k2eAgInSc2d1
pohledu	pohled	k1gInSc2
ropa	ropa	k1gFnSc1
a	a	k8xC
zemní	zemní	k2eAgInSc1d1
plyn	plyn	k1gInSc1
<g/>
,	,	kIx,
zejména	zejména	k9
při	při	k7c6
východním	východní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
<g/>
,	,	kIx,
na	na	k7c6
jejichž	jejichž	k3xOyRp3gFnSc6
těžbě	těžba	k1gFnSc6
závisí	záviset	k5eAaImIp3nS
příjem	příjem	k1gInSc4
deviz	deviza	k1gFnPc2
i	i	k8xC
rychle	rychle	k6eAd1
se	se	k3xPyFc4
rozvíjející	rozvíjející	k2eAgInSc1d1
chemický	chemický	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1938	#num#	k4
kontroluje	kontrolovat	k5eAaImIp3nS
všechny	všechen	k3xTgInPc1
vrty	vrt	k1gInPc1
<g/>
,	,	kIx,
soustředěné	soustředěný	k2eAgInPc1d1
zejména	zejména	k9
na	na	k7c6
atlantském	atlantský	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
včetně	včetně	k7c2
pobřežního	pobřežní	k2eAgInSc2d1
šelfu	šelf	k1gInSc2
<g/>
,	,	kIx,
státní	státní	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
PEMEX	PEMEX	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podíl	podíl	k1gInSc1
Mexika	Mexiko	k1gNnSc2
na	na	k7c6
světovém	světový	k2eAgInSc6d1
obchodu	obchod	k1gInSc6
s	s	k7c7
ropou	ropa	k1gFnSc7
představuje	představovat	k5eAaImIp3nS
asi	asi	k9
6	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stále	stále	k6eAd1
větší	veliký	k2eAgInSc1d2
význam	význam	k1gInSc1
má	mít	k5eAaImIp3nS
zemní	zemní	k2eAgInSc1d1
plyn	plyn	k1gInSc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
téměř	téměř	k6eAd1
celou	celý	k2eAgFnSc4d1
produkci	produkce	k1gFnSc4
odebírají	odebírat	k5eAaImIp3nP
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
a	a	k8xC
Kanada	Kanada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíc	hodně	k6eAd3,k6eAd1
plynu	plyn	k1gInSc2
se	se	k3xPyFc4
těží	těžet	k5eAaImIp3nS
kolem	kolem	k7c2
měst	město	k1gNnPc2
Reynosa	Reynosa	k1gFnSc1
a	a	k8xC
Poza	Poza	k?
Rica	Rica	k1gMnSc1
ve	v	k7c6
státě	stát	k1gInSc6
Tabasco	Tabasco	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Průmysl	průmysl	k1gInSc1
</s>
<s>
Převažuje	převažovat	k5eAaImIp3nS
průmysl	průmysl	k1gInSc4
chemický	chemický	k2eAgInSc4d1
<g/>
,	,	kIx,
strojírenský	strojírenský	k2eAgInSc4d1
<g/>
,	,	kIx,
potravinářský	potravinářský	k2eAgInSc4d1
a	a	k8xC
textilní	textilní	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemický	chemický	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
sídlí	sídlet	k5eAaImIp3nS
zejména	zejména	k9
ve	v	k7c6
městech	město	k1gNnPc6
Coatzacoalcos	Coatzacoalcosa	k1gFnPc2
a	a	k8xC
Tampico	Tampico	k6eAd1
na	na	k7c6
východě	východ	k1gInSc6
<g/>
,	,	kIx,
Salina	salina	k1gFnSc1
Cruz	Cruza	k1gFnPc2
na	na	k7c6
jihu	jih	k1gInSc6
a	a	k8xC
Salamanca	Salamanca	k1gMnSc1
ve	v	k7c6
středozemí	středozemí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strojírenským	strojírenský	k2eAgInSc7d1
průmyslem	průmysl	k1gInSc7
se	se	k3xPyFc4
vyznačují	vyznačovat	k5eAaImIp3nP
Veracruz	Veracruz	k1gMnSc1
na	na	k7c6
východě	východ	k1gInSc6
a	a	k8xC
Puebla	Puebla	k1gFnSc6
ve	v	k7c6
středozemí	středozemí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Města	město	k1gNnPc1
jako	jako	k8xS,k8xC
Mexicali	Mexicali	k1gFnPc1
<g/>
,	,	kIx,
Ciudad	Ciudad	k1gInSc1
Juárez	Juárez	k1gInSc1
<g/>
,	,	kIx,
Chihuahua	Chihuahua	k1gMnSc1
<g/>
,	,	kIx,
Torreón	Torreón	k1gMnSc1
<g/>
,	,	kIx,
León	León	k1gMnSc1
se	se	k3xPyFc4
vyznačují	vyznačovat	k5eAaImIp3nP
potravinářstvím	potravinářství	k1gNnSc7
<g/>
,	,	kIx,
textilním	textilní	k2eAgInSc7d1
průmyslem	průmysl	k1gInSc7
a	a	k8xC
hutnictvím	hutnictví	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Monclova	Monclův	k2eAgMnSc2d1
na	na	k7c6
severu	sever	k1gInSc6
a	a	k8xC
Veracruz	Veracruz	k1gInSc4
jsou	být	k5eAaImIp3nP
typická	typický	k2eAgNnPc1d1
hutní	hutní	k2eAgNnPc1d1
města	město	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takovým	takový	k3xDgNnSc7
textilním	textilní	k2eAgNnSc7d1
městem	město	k1gNnSc7
je	být	k5eAaImIp3nS
Mérida	Mérida	k1gFnSc1
na	na	k7c6
severu	sever	k1gInSc6
poloostrova	poloostrov	k1gInSc2
Yucatán	Yucatán	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Turistika	turistika	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
bylo	být	k5eAaImAgNnS
Mexiko	Mexiko	k1gNnSc1
6	#num#	k4
<g/>
.	.	kIx.
nejnavštěvovanější	navštěvovaný	k2eAgFnSc1d3
zemí	zem	k1gFnSc7
světa	svět	k1gInSc2
a	a	k8xC
mělo	mít	k5eAaImAgNnS
15	#num#	k4
<g/>
.	.	kIx.
nejvyšší	vysoký	k2eAgInSc1d3
příjem	příjem	k1gInSc1
z	z	k7c2
turistiky	turistika	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
řadí	řadit	k5eAaImIp3nS
Mexiko	Mexiko	k1gNnSc4
na	na	k7c4
první	první	k4xOgNnSc4
místo	místo	k1gNnSc4
v	v	k7c6
Latinské	latinský	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Naprostá	naprostý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
turistů	turist	k1gMnPc2
přijíždí	přijíždět	k5eAaImIp3nS
do	do	k7c2
Mexika	Mexiko	k1gNnSc2
ze	z	k7c2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
a	a	k8xC
Kanady	Kanada	k1gFnSc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
následují	následovat	k5eAaImIp3nP
turisté	turist	k1gMnPc1
evropští	evropský	k2eAgMnPc1d1
a	a	k8xC
asijští	asijský	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
málo	málo	k4c1
turistů	turist	k1gMnPc2
přijíždí	přijíždět	k5eAaImIp3nS
z	z	k7c2
jiných	jiný	k2eAgFnPc2d1
latinskoamerických	latinskoamerický	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
se	se	k3xPyFc4
Mexiko	Mexiko	k1gNnSc1
umístilo	umístit	k5eAaPmAgNnS
celosvětově	celosvětově	k6eAd1
na	na	k7c4
22	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
v	v	k7c6
žebříčku	žebříček	k1gInSc6
turistické	turistický	k2eAgFnSc2d1
konkurenceschopnosti	konkurenceschopnost	k1gFnSc2
(	(	kIx(
<g/>
Travel	Travel	k1gInSc1
and	and	k?
Tourism	Tourism	k1gInSc1
Competitiveness	Competitiveness	k1gInSc1
Report	report	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
Hustota	hustota	k1gFnSc1
osídlení	osídlení	k1gNnSc2
Mexika	Mexiko	k1gNnSc2
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
Mexika	Mexiko	k1gNnSc2
se	se	k3xPyFc4
zvýšil	zvýšit	k5eAaPmAgInS
ze	z	k7c2
14,3	14,3	k4
milionů	milion	k4xCgInPc2
v	v	k7c6
roce	rok	k1gInSc6
1921	#num#	k4
na	na	k7c4
přibližně	přibližně	k6eAd1
124,5	124,5	k4
milionu	milion	k4xCgInSc2
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Poměr	poměr	k1gInSc1
venkovského	venkovský	k2eAgNnSc2d1
a	a	k8xC
městského	městský	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
činí	činit	k5eAaImIp3nS
30	#num#	k4
<g/>
:	:	kIx,
<g/>
70	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrná	průměrný	k2eAgFnSc1d1
hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
je	být	k5eAaImIp3nS
57	#num#	k4
obyvatel	obyvatel	k1gMnPc2
na	na	k7c4
1	#num#	k4
km²	km²	k?
<g/>
,	,	kIx,
největší	veliký	k2eAgFnSc1d3
hustota	hustota	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
<g/>
,	,	kIx,
nejmenší	malý	k2eAgMnSc1d3
pak	pak	k6eAd1
ve	v	k7c6
státě	stát	k1gInSc6
Baja	Baj	k1gInSc2
California	Californium	k1gNnSc2
Sur	Sur	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Metropolitní	metropolitní	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Valle	Valle	k1gFnSc2
de	de	k?
México	México	k1gMnSc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gMnSc1
centrem	centrum	k1gNnSc7
je	být	k5eAaImIp3nS
Ciudad	Ciudad	k1gInSc1
de	de	k?
México	México	k6eAd1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
domovem	domov	k1gInSc7
pro	pro	k7c4
více	hodně	k6eAd2
jak	jak	k8xC,k8xS
20	#num#	k4
miliónů	milión	k4xCgInPc2
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Mesticové	mestic	k1gMnPc1
<g/>
,	,	kIx,
tedy	tedy	k9
míšenci	míšenec	k1gMnPc1
Indiánů	Indián	k1gMnPc2
a	a	k8xC
bělochů	běloch	k1gMnPc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
zastoupeni	zastoupit	k5eAaPmNgMnP
60	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
Indiáni	Indián	k1gMnPc1
30	#num#	k4
%	%	kIx~
(	(	kIx(
<g/>
asi	asi	k9
35,35	35,35	k4
milionu	milion	k4xCgInSc2
<g/>
)	)	kIx)
a	a	k8xC
běloši	běloch	k1gMnPc1
(	(	kIx(
<g/>
potomci	potomek	k1gMnPc1
Španělů	Španěl	k1gMnPc2
zvaní	zvaný	k2eAgMnPc1d1
kreolové	kreol	k1gMnPc1
<g/>
)	)	kIx)
9	#num#	k4
%	%	kIx~
(	(	kIx(
<g/>
11,205	11,205	k4
milionu	milion	k4xCgInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Indiánů	Indián	k1gMnPc2
je	být	k5eAaImIp3nS
nejvíce	hodně	k6eAd3,k6eAd1
<g/>
:	:	kIx,
Nahuů	Nahu	k1gMnPc2
(	(	kIx(
<g/>
potomci	potomek	k1gMnPc1
Aztéků	Azték	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Mayů	May	k1gMnPc2
a	a	k8xC
Zapoteců	Zapotece	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejméně	málo	k6eAd3
<g/>
:	:	kIx,
Ixcateců	Ixcatece	k1gMnPc2
a	a	k8xC
Kiliwaů	Kiliwa	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
jsou	být	k5eAaImIp3nP
na	na	k7c6
pokraji	pokraj	k1gInSc6
vymření	vymření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
89	#num#	k4
%	%	kIx~
katolíků	katolík	k1gMnPc2
(	(	kIx(
<g/>
vč.	vč.	k?
východních	východní	k2eAgInPc2d1
<g/>
)	)	kIx)
a	a	k8xC
6	#num#	k4
%	%	kIx~
protestantů	protestant	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
země	zem	k1gFnSc2
přicházejí	přicházet	k5eAaImIp3nP
lidé	člověk	k1gMnPc1
především	především	k6eAd1
z	z	k7c2
Guatemaly	Guatemala	k1gFnSc2
a	a	k8xC
Hondurasu	Honduras	k1gInSc2
<g/>
,	,	kIx,
hlavně	hlavně	k9
za	za	k7c7
prací	práce	k1gFnSc7
a	a	k8xC
lepšími	dobrý	k2eAgFnPc7d2
životními	životní	k2eAgFnPc7d1
podmínkami	podmínka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mexičané	Mexičan	k1gMnPc1
a	a	k8xC
vůbec	vůbec	k9
obyvatelé	obyvatel	k1gMnPc1
celé	celý	k2eAgFnSc2d1
Střední	střední	k2eAgFnSc2d1
a	a	k8xC
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
už	už	k6eAd1
od	od	k7c2
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
mohutně	mohutně	k6eAd1
migrují	migrovat	k5eAaImIp3nP
do	do	k7c2
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
Města	město	k1gNnSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
měst	město	k1gNnPc2
v	v	k7c6
Mexiku	Mexiko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
20	#num#	k4
největších	veliký	k2eAgNnPc2d3
měst	město	k1gNnPc2
v	v	k7c6
Mexiku	Mexiko	k1gNnSc6
<g/>
:	:	kIx,
</s>
<s>
město	město	k1gNnSc1
</s>
<s>
stát	stát	k1gInSc1
</s>
<s>
obyv	obyv	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
město	město	k1gNnSc1
</s>
<s>
stát	stát	k1gInSc1
</s>
<s>
obyv	obyv	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Cd	cd	kA
<g/>
.	.	kIx.
de	de	k?
MéxicoGuadalajaraMonterrey	MéxicoGuadalajaraMonterrea	k1gFnSc2
</s>
<s>
1	#num#	k4
<g/>
Ciudad	Ciudad	k1gInSc1
de	de	k?
MéxicoDF	MéxicoDF	k1gFnSc2
<g/>
19	#num#	k4
231	#num#	k4
82911	#num#	k4
<g/>
Santiago	Santiago	k1gNnSc4
de	de	k?
QuerétaroQro	QuerétaroQro	k1gNnSc1
<g/>
918	#num#	k4
100	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
GuadalajaraJal	GuadalajaraJal	k1gInSc1
<g/>
4	#num#	k4
095	#num#	k4
85312	#num#	k4
<g/>
MéridaYuc	MéridaYuc	k1gFnSc1
<g/>
897	#num#	k4
740	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
MonterreyNL	MonterreyNL	k1gFnSc1
<g/>
3	#num#	k4
664	#num#	k4
33113	#num#	k4
<g/>
MexicaliBC	MexicaliBC	k1gFnSc1
<g/>
855	#num#	k4
962	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
PueblaPue	PueblaPue	k1gFnSc1
<g/>
2	#num#	k4
109	#num#	k4
0	#num#	k4
<g/>
4914	#num#	k4
<g/>
AguascalientesAgs	AguascalientesAgs	k1gInSc1
<g/>
805	#num#	k4
666	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
TolucaMéx	TolucaMéx	k1gInSc1
<g/>
1	#num#	k4
610	#num#	k4
78615	#num#	k4
<g/>
TampicoTamps	TampicoTamps	k1gInSc1
<g/>
803	#num#	k4
196	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
TijuanaBC	TijuanaBC	k1gFnSc1
<g/>
1	#num#	k4
483	#num#	k4
99216	#num#	k4
<g/>
CuliacánSin	CuliacánSin	k2eAgInSc4d1
<g/>
793	#num#	k4
730	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
LeónGto	LeónGto	k1gNnSc1
<g/>
1	#num#	k4
425	#num#	k4
21017	#num#	k4
<g/>
CuernavacaMor	CuernavacaMor	k1gInSc1
<g/>
787	#num#	k4
556	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
Ciudad	Ciudad	k1gInSc1
JuárezChih	JuárezChih	k1gInSc1
<g/>
1	#num#	k4
313	#num#	k4
33818	#num#	k4
<g/>
AcapulcoGro	AcapulcoGro	k1gNnSc4
<g/>
786	#num#	k4
830	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
TorreónCoah	TorreónCoah	k1gInSc1
<g/>
1	#num#	k4
110	#num#	k4
89019	#num#	k4
<g/>
ChihuahuaChih	ChihuahuaChih	k1gInSc1
<g/>
784	#num#	k4
882	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
San	San	k1gFnSc1
Luis	Luisa	k1gFnPc2
PotosíSLP	PotosíSLP	k1gFnSc4
<g/>
957	#num#	k4
75320	#num#	k4
<g/>
MoreliaMich	MoreliaMich	k1gInSc1
<g/>
735	#num#	k4
624	#num#	k4
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jazyky	jazyk	k1gInPc1
</s>
<s>
Indiánské	indiánský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
</s>
<s>
Úřední	úřední	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
Mexika	Mexiko	k1gNnSc2
je	být	k5eAaImIp3nS
španělština	španělština	k1gFnSc1
s	s	k7c7
totožnou	totožný	k2eAgFnSc7d1
gramatikou	gramatika	k1gFnSc7
jako	jako	k8xS,k8xC
ve	v	k7c6
Španělsku	Španělsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Latinskoamerické	latinskoamerický	k2eAgFnPc4d1
španělštiny	španělština	k1gFnPc4
ovšem	ovšem	k9
mají	mít	k5eAaImIp3nP
svá	svůj	k3xOyFgNnPc4
specifika	specifikon	k1gNnPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
mezi	mezi	k7c4
nejvýraznější	výrazný	k2eAgNnSc4d3
patří	patřit	k5eAaImIp3nS
nepoužívání	nepoužívání	k1gNnSc4
2	#num#	k4
<g/>
.	.	kIx.
<g/>
osoby	osoba	k1gFnPc1
množného	množný	k2eAgNnSc2d1
čísla	číslo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Mexická	mexický	k2eAgFnSc1d1
španělština	španělština	k1gFnSc1
má	mít	k5eAaImIp3nS
slovník	slovník	k1gInSc4
obohacen	obohacen	k2eAgInSc4d1
o	o	k7c4
řadu	řada	k1gFnSc4
slov	slovo	k1gNnPc2
převzatých	převzatý	k2eAgNnPc2d1
z	z	k7c2
indiánských	indiánský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
,	,	kIx,
kterých	který	k3yIgInPc2,k3yQgInPc2,k3yRgInPc2
je	být	k5eAaImIp3nS
v	v	k7c6
Mexiku	Mexiko	k1gNnSc6
celkem	celkem	k6eAd1
62	#num#	k4
viz	vidět	k5eAaImRp2nS
.	.	kIx.
</s>
<s desamb="1">
Nejrozšířenější	rozšířený	k2eAgFnSc1d3
je	být	k5eAaImIp3nS
aztéčtina	aztéčtina	k1gFnSc1
–	–	k?
nahuatl	nahuatnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
po	po	k7c6
ní	on	k3xPp3gFnSc6
pak	pak	k6eAd1
mayština	mayština	k1gFnSc1
<g/>
,	,	kIx,
mixtéčtina	mixtéčtina	k1gFnSc1
a	a	k8xC
zapotečtina	zapotečtina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aztécký	aztécký	k2eAgMnSc1d1
nahuatl	nahuatnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
v	v	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
a	a	k8xC
okolí	okolí	k1gNnSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
snadno	snadno	k6eAd1
rozeznatelný	rozeznatelný	k2eAgInSc1d1
příponami	přípona	k1gFnPc7
-tl	-tl	k?
(	(	kIx(
<g/>
známá	známý	k2eAgFnSc1d1
sopka	sopka	k1gFnSc1
Popocatépetl	Popocatépetl	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Některá	některý	k3yIgNnPc1
slova	slovo	k1gNnPc1
z	z	k7c2
aztéčtiny	aztéčtina	k1gFnSc2
byla	být	k5eAaImAgFnS
převzata	převzat	k2eAgFnSc1d1
i	i	k9
do	do	k7c2
češtiny	čeština	k1gFnSc2
<g/>
,	,	kIx,
např.	např.	kA
„	„	k?
<g/>
šokolatl	šokolatnout	k5eAaPmAgMnS
<g/>
“	“	k?
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
u	u	k7c2
nás	my	k3xPp1nPc4
naše	náš	k3xOp1gFnSc1
známá	známý	k2eAgFnSc1d1
čokoláda	čokoláda	k1gFnSc1
<g/>
,	,	kIx,
papričky	paprička	k1gFnPc1
„	„	k?
<g/>
chilli	chilli	k1gNnSc1
<g/>
“	“	k?
se	se	k3xPyFc4
řeknou	říct	k5eAaPmIp3nP
také	také	k9
tak	tak	k6eAd1
<g/>
,	,	kIx,
ovšem	ovšem	k9
sousloví	sousloví	k1gNnSc6
„	„	k?
<g/>
tomatová	tomatový	k2eAgFnSc1d1
<g/>
“	“	k?
omáčka	omáčka	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
aztéckého	aztécký	k2eAgNnSc2d1
„	„	k?
<g/>
tomatl	tomatnout	k5eAaPmAgMnS
<g/>
“	“	k?
pro	pro	k7c4
rajče	rajče	k1gNnSc4
<g/>
)	)	kIx)
již	již	k6eAd1
Češi	Čech	k1gMnPc1
používají	používat	k5eAaImIp3nP
méně	málo	k6eAd2
častěji	často	k6eAd2
než	než	k8xS
Němci	Němec	k1gMnPc1
nebo	nebo	k8xC
Angličané	Angličan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Náboženství	náboženství	k1gNnSc1
</s>
<s>
Panna	Panna	k1gFnSc1
Marie	Maria	k1gFnSc2
Guadalupská	Guadalupský	k2eAgFnSc1d1
obklopená	obklopený	k2eAgFnSc1d1
svatozáří	svatozář	k1gFnPc2
</s>
<s>
Život	život	k1gInSc1
indiánských	indiánský	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
Mexika	Mexiko	k1gNnSc2
v	v	k7c6
předkolumbovském	předkolumbovský	k2eAgNnSc6d1
období	období	k1gNnSc6
byl	být	k5eAaImAgInS
prostoupen	prostoupit	k5eAaPmNgInS
uctíváním	uctívání	k1gNnSc7
obrovského	obrovský	k2eAgInSc2d1
panteonu	panteon	k1gInSc2
bohů	bůh	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jejich	jejich	k3xOp3gFnSc6
počest	počest	k1gFnSc6
se	se	k3xPyFc4
budovaly	budovat	k5eAaImAgFnP
rozsáhlé	rozsáhlý	k2eAgFnPc1d1
stavby	stavba	k1gFnPc1
a	a	k8xC
kultovní	kultovní	k2eAgNnPc1d1
centra	centrum	k1gNnPc1
<g/>
,	,	kIx,
obyvatelstvo	obyvatelstvo	k1gNnSc1
vydržovalo	vydržovat	k5eAaImAgNnS
početnou	početný	k2eAgFnSc4d1
vrstvu	vrstva	k1gFnSc4
kněží	kněz	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
byli	být	k5eAaImAgMnP
prostředníky	prostředník	k1gInPc4
mezi	mezi	k7c7
prostým	prostý	k2eAgInSc7d1
lidem	lid	k1gInSc7
a	a	k8xC
bohy	bůh	k1gMnPc4
<g/>
,	,	kIx,
putovalo	putovat	k5eAaImAgNnS
do	do	k7c2
vzdálených	vzdálený	k2eAgNnPc2d1
posvátných	posvátný	k2eAgNnPc2d1
míst	místo	k1gNnPc2
a	a	k8xC
pořádalo	pořádat	k5eAaImAgNnS
náboženské	náboženský	k2eAgFnPc4d1
slavnosti	slavnost	k1gFnPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnPc2
součástí	součást	k1gFnPc2
bývaly	bývat	k5eAaImAgFnP
i	i	k9
lidské	lidský	k2eAgFnPc1d1
oběti	oběť	k1gFnPc1
na	na	k7c4
usmíření	usmíření	k1gNnPc4
bohů	bůh	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
conquisty	conquista	k1gFnPc4
a	a	k8xC
v	v	k7c6
koloniálním	koloniální	k2eAgNnSc6d1
období	období	k1gNnSc6
dobyvatelé	dobyvatel	k1gMnPc1
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
i	i	k9
násilím	násilí	k1gNnSc7
<g/>
)	)	kIx)
obraceli	obracet	k5eAaImAgMnP
Indiány	Indián	k1gMnPc4
na	na	k7c4
katolickou	katolický	k2eAgFnSc4d1
víru	víra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c7
tímto	tento	k3xDgInSc7
účelem	účel	k1gInSc7
sem	sem	k6eAd1
ze	z	k7c2
Španělska	Španělsko	k1gNnSc2
a	a	k8xC
později	pozdě	k6eAd2
z	z	k7c2
dalších	další	k2eAgFnPc2d1
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
přicházeli	přicházet	k5eAaImAgMnP
kněží	kněz	k1gMnPc1
a	a	k8xC
mniši	mnich	k1gMnPc1
z	z	k7c2
různých	různý	k2eAgInPc2d1
náboženských	náboženský	k2eAgInPc2d1
řádů	řád	k1gInPc2
–	–	k?
františkáni	františkán	k1gMnPc1
<g/>
,	,	kIx,
dominikáni	dominikán	k1gMnPc1
<g/>
,	,	kIx,
augustiniáni	augustinián	k1gMnPc1
a	a	k8xC
nakonec	nakonec	k6eAd1
jezuité	jezuita	k1gMnPc1
–	–	k?
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
měli	mít	k5eAaImAgMnP
Indiány	Indián	k1gMnPc4
vzdělat	vzdělat	k5eAaPmF
v	v	k7c6
základech	základ	k1gInPc6
víry	víra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Každoročně	každoročně	k6eAd1
se	se	k3xPyFc4
konají	konat	k5eAaImIp3nP
oslavy	oslava	k1gFnPc1
kultu	kult	k1gInSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Guadalupské	Guadalupský	k2eAgFnSc2d1
<g/>
,	,	kIx,
patronky	patronka	k1gFnPc4
Mexika	Mexiko	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
vrcholí	vrcholit	k5eAaImIp3nS
12	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c4
den	den	k1gInSc4
jejího	její	k3xOp3gNnSc2
prvního	první	k4xOgNnSc2
údajného	údajný	k2eAgNnSc2d1
zjevení	zjevení	k1gNnSc2
v	v	k7c6
Mexiku	Mexiko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
je	být	k5eAaImIp3nS
katolická	katolický	k2eAgFnSc1d1
země	země	k1gFnSc1
<g/>
;	;	kIx,
zcela	zcela	k6eAd1
v	v	k7c6
ní	on	k3xPp3gFnSc6
převažují	převažovat	k5eAaImIp3nP
římští	římský	k2eAgMnPc1d1
katolíci	katolík	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
katolictví	katolictví	k1gNnSc3
včetně	včetně	k7c2
východního	východní	k2eAgNnSc2d1
se	se	k3xPyFc4
hlasí	hlasý	k2eAgMnPc1d1
89	#num#	k4
<g/>
–	–	k?
<g/>
90	#num#	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zemi	zem	k1gFnSc6
působí	působit	k5eAaImIp3nS
také	také	k9
následující	následující	k2eAgInPc4d1
tzv.	tzv.	kA
východní	východní	k2eAgFnSc2d1
katolické	katolický	k2eAgFnSc2d1
církve	církev	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
<g/>
)	)	kIx)
Arménští	arménský	k2eAgMnPc1d1
katolíci	katolík	k1gMnPc1
–	–	k?
Apoštolský	apoštolský	k2eAgInSc4d1
exarchát	exarchát	k1gInSc4
pro	pro	k7c4
Latinskou	latinský	k2eAgFnSc4d1
Ameriku	Amerika	k1gFnSc4
a	a	k8xC
Mexiko	Mexiko	k1gNnSc4
(	(	kIx(
<g/>
mimo	mimo	k7c4
Argentinu	Argentina	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
dvě	dva	k4xCgFnPc1
farnosti	farnost	k1gFnPc1
<g/>
,	,	kIx,
počet	počet	k1gInSc1
stagnuje	stagnovat	k5eAaImIp3nS
<g/>
:	:	kIx,
roku	rok	k1gInSc2
2015	#num#	k4
–	–	k?
12	#num#	k4
000	#num#	k4
věřících	věřící	k2eAgMnPc2d1
–	–	k?
stejně	stejně	k6eAd1
jako	jako	k9
r.	r.	kA
2014	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
)	)	kIx)
Maronité	Maronitý	k2eAgNnSc1d1
<g/>
:	:	kIx,
Eparchie	eparchie	k1gFnSc1
Nuestra	Nuestra	k1gFnSc1
Señ	Señ	k1gFnSc1
(	(	kIx(
<g/>
Naše	náš	k3xOp1gFnSc1
Paní	paní	k1gFnSc1
<g/>
,	,	kIx,
tj.	tj.	kA
Panna	Panna	k1gFnSc1
Maria	Maria	k1gFnSc1
<g/>
)	)	kIx)
de	de	k?
los	los	k1gInSc1
Martires	Martires	k1gInSc1
de	de	k?
Libano	Libana	k1gFnSc5
(	(	kIx(
<g/>
libanonských	libanonský	k2eAgMnPc2d1
mučedníků	mučedník	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vznikla	vzniknout	k5eAaPmAgFnS
1995	#num#	k4
<g/>
,	,	kIx,
roku	rok	k1gInSc2
2015	#num#	k4
–	–	k?
156	#num#	k4
000	#num#	k4
věřících	věřící	k1gFnPc2
–	–	k?
počet	počet	k1gInSc1
narůstá	narůstat	k5eAaImIp3nS
<g/>
:	:	kIx,
2014	#num#	k4
–	–	k?
154	#num#	k4
400	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
–	–	k?
150	#num#	k4
800	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
–	–	k?
150	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maronitou	Maronita	k1gFnSc7
je	být	k5eAaImIp3nS
i	i	k9
velkopodnikatel	velkopodnikatel	k1gMnSc1
a	a	k8xC
miliardář	miliardář	k1gMnSc1
Carlos	Carlos	k1gMnSc1
Slim	Slim	k1gMnSc1
Helú	Helú	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
)	)	kIx)
Řeckokatolíci	Řeckokatolík	k1gMnPc1
–	–	k?
melchité	melchitý	k2eAgNnSc1d1
<g/>
:	:	kIx,
Eparchie	eparchie	k1gFnSc1
Nuestra	Nuestra	k1gFnSc1
Señ	Señ	k1gFnSc1
(	(	kIx(
<g/>
Naše	náš	k3xOp1gFnSc1
Paní	paní	k1gFnSc1
<g/>
,	,	kIx,
tj.	tj.	kA
Panna	Panna	k1gFnSc1
Maria	Maria	k1gFnSc1
<g/>
)	)	kIx)
del	del	k?
Paraíso	Paraísa	k1gFnSc5
<g/>
:	:	kIx,
2015	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
–	–	k?
4	#num#	k4
700	#num#	k4
věřících	věřící	k2eAgMnPc2d1
<g/>
,	,	kIx,
2000	#num#	k4
–	–	k?
2	#num#	k4
500	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
–	–	k?
2	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Komunity	komunita	k1gFnPc1
jiných	jiný	k2eAgNnPc2d1
náboženství	náboženství	k1gNnPc2
jsou	být	k5eAaImIp3nP
méně	málo	k6eAd2
početné	početný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
k	k	k7c3
nim	on	k3xPp3gInPc3
protestanti	protestant	k1gMnPc1
(	(	kIx(
<g/>
asi	asi	k9
6	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
židé	žid	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
domorodého	domorodý	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
(	(	kIx(
<g/>
cca	cca	kA
30	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
praktikuje	praktikovat	k5eAaImIp3nS
kombinaci	kombinace	k1gFnSc4
svého	svůj	k3xOyFgNnSc2
původního	původní	k2eAgNnSc2d1
náboženství	náboženství	k1gNnSc2
s	s	k7c7
katolicismem	katolicismus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Mexiku	Mexiko	k1gNnSc6
žije	žít	k5eAaImIp3nS
také	také	k9
přes	přes	k7c4
80	#num#	k4
000	#num#	k4
Mennonitů	Mennonit	k1gInPc2
německo-holandského	německo-holandský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
v	v	k7c6
zemi	zem	k1gFnSc6
usadili	usadit	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mexiko	Mexiko	k1gNnSc4
několikrát	několikrát	k6eAd1
navštívil	navštívit	k5eAaPmAgMnS
i	i	k9
zesnulý	zesnulý	k1gMnSc1
papež	papež	k1gMnSc1
Jan	Jan	k1gMnSc1
Pavel	Pavel	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Kultura	kultura	k1gFnSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Carlos	Carlos	k1gMnSc1
Fuentes	Fuentes	k1gMnSc1
</s>
<s>
Básník	básník	k1gMnSc1
Octavio	Octavio	k1gMnSc1
Paz	Paz	k1gMnSc1
získal	získat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
Nobelovu	Nobelův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
za	za	k7c4
literaturu	literatura	k1gFnSc4
<g/>
,	,	kIx,
dosud	dosud	k6eAd1
jedinou	jediný	k2eAgFnSc7d1
pro	pro	k7c4
mexickou	mexický	k2eAgFnSc4d1
literaturu	literatura	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
nejvýznamnějším	významný	k2eAgMnPc3d3
mexickým	mexický	k2eAgMnPc3d1
prozaikům	prozaik	k1gMnPc3
patřil	patřit	k5eAaImAgInS
Carlos	Carlos	k1gInSc1
Fuentes	Fuentes	k1gInSc1
(	(	kIx(
<g/>
jakkoli	jakkoli	k8xS
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
v	v	k7c6
Panamě	Panama	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
získal	získat	k5eAaPmAgMnS
Cervantesovu	Cervantesův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
<g/>
,	,	kIx,
nejprestižnější	prestižní	k2eAgNnSc4d3
ocenění	ocenění	k1gNnSc4
pro	pro	k7c4
španělskojazyčné	španělskojazyčný	k2eAgMnPc4d1
autory	autor	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nejvýznamnějším	významný	k2eAgMnPc3d3
mexickým	mexický	k2eAgMnPc3d1
básníkům	básník	k1gMnPc3
2	#num#	k4
<g/>
.	.	kIx.
poloviny	polovina	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
patřil	patřit	k5eAaImAgMnS
José	Josí	k1gMnPc4
Emilio	Emilio	k1gMnSc1
Pacheco	Pacheco	k1gMnSc1
<g/>
,	,	kIx,
ten	ten	k3xDgMnSc1
Cervantesovu	Cervantesův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
získal	získat	k5eAaPmAgMnS
roku	rok	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
Prozaik	prozaik	k1gMnSc1
Sergio	Sergio	k1gMnSc1
Pitol	Pitol	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
roku	rok	k1gInSc2
2013	#num#	k4
publicistka	publicistka	k1gFnSc1
Elena	Elena	k1gFnSc1
Poniatowska	Poniatowska	k1gFnSc1
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
básník	básník	k1gMnSc1
Fernando	Fernanda	k1gFnSc5
del	del	k?
Paso	Paso	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
zakladatelům	zakladatel	k1gMnPc3
mexického	mexický	k2eAgNnSc2d1
básnictví	básnictví	k1gNnSc2
patřila	patřit	k5eAaImAgFnS
řeholnice	řeholnice	k1gFnSc1
Juana	Juan	k1gMnSc2
Inés	Inés	k1gInSc1
de	de	k?
la	la	k1gNnSc2
Cruz	Cruza	k1gFnPc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gNnSc1
dílo	dílo	k1gNnSc1
je	být	k5eAaImIp3nS
řazeno	řadit	k5eAaImNgNnS
k	k	k7c3
baroku	barok	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgMnSc7
z	z	k7c2
objevitelů	objevitel	k1gMnPc2
tzv.	tzv.	kA
magického	magický	k2eAgInSc2d1
realismu	realismus	k1gInSc2
<g/>
,	,	kIx,
jímž	jenž	k3xRgNnSc7
se	se	k3xPyFc4
proslavila	proslavit	k5eAaPmAgFnS
latinskoamerická	latinskoamerický	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
jako	jako	k8xS,k8xC
taková	takový	k3xDgFnSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
Juan	Juan	k1gMnSc1
Rulfo	Rulfo	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
Téma	téma	k1gNnSc1
střetu	střet	k1gInSc2
původní	původní	k2eAgFnSc2d1
a	a	k8xC
bílé	bílý	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
v	v	k7c6
Mexiku	Mexiko	k1gNnSc6
bylo	být	k5eAaImAgNnS
typickým	typický	k2eAgInPc3d1
pro	pro	k7c4
básnířku	básnířka	k1gFnSc4
i	i	k8xC
prozaičku	prozaička	k1gFnSc4
Rosariu	rosarium	k1gNnSc3
Castellanos	Castellanos	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mariano	Mariana	k1gFnSc5
Azuela	Azuela	k1gFnSc1
proslul	proslout	k5eAaPmAgInS
romány	román	k1gInPc7
z	z	k7c2
období	období	k1gNnSc2
Mexické	mexický	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
roku	rok	k1gInSc2
1910	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Především	především	k6eAd1
svou	svůj	k3xOyFgFnSc7
románovou	románový	k2eAgFnSc7d1
prvotinou	prvotina	k1gFnSc7
Como	Como	k6eAd1
agua	agu	k2eAgFnSc1d1
para	para	k1gFnSc1
chocolate	chocolat	k1gInSc5
na	na	k7c4
sebe	sebe	k3xPyFc4
upozornila	upozornit	k5eAaPmAgFnS
Laura	Laura	k1gFnSc1
Esquivelová	Esquivelová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Mexiku	Mexiko	k1gNnSc6
se	se	k3xPyFc4
odehrávala	odehrávat	k5eAaImAgFnS
též	též	k9
díla	dílo	k1gNnSc2
spisovatele	spisovatel	k1gMnSc2
s	s	k7c7
nejasnou	jasný	k2eNgFnSc7d1
identitou	identita	k1gFnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
psal	psát	k5eAaImAgInS
pod	pod	k7c7
pseudonymem	pseudonym	k1gInSc7
B.	B.	kA
Traven	Traven	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výtvarné	výtvarný	k2eAgNnSc1d1
umění	umění	k1gNnSc1
</s>
<s>
Jeden	jeden	k4xCgInSc1
z	z	k7c2
autoportrétů	autoportrét	k1gInPc2
Fridy	Frida	k1gFnSc2
Kahlo	Kahlo	k1gNnSc1
</s>
<s>
Muzeum	muzeum	k1gNnSc1
Soumaya	Soumay	k1gInSc2
</s>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3
mexickým	mexický	k2eAgMnSc7d1
výtvarným	výtvarný	k2eAgMnSc7d1
umělcem	umělec	k1gMnSc7
je	být	k5eAaImIp3nS
malířka	malířka	k1gFnSc1
Frida	Frida	k1gFnSc1
Kahlo	Kahlo	k1gNnSc4
<g/>
,	,	kIx,
jistě	jistě	k9
i	i	k9
díky	díky	k7c3
slavnému	slavný	k2eAgInSc3d1
biografickému	biografický	k2eAgInSc3d1
snímku	snímek	k1gInSc3
se	s	k7c7
Selmou	Selma	k1gFnSc7
Hayekovou	Hayekův	k2eAgFnSc7d1
v	v	k7c6
hlavní	hlavní	k2eAgFnSc6d1
roli	role	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gNnSc1
muzeum	muzeum	k1gNnSc1
v	v	k7c4
Ciudad	Ciudad	k1gInSc4
de	de	k?
México	México	k6eAd1
(	(	kIx(
<g/>
Museo	Museo	k6eAd1
Frida	Frido	k1gNnSc2
Kahlo	Kahlo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tzv.	tzv.	kA
Modrý	modrý	k2eAgInSc1d1
dům	dům	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgMnS
i	i	k9
jejím	její	k3xOp3gInSc7
domem	dům	k1gInSc7
rodným	rodný	k2eAgInSc7d1
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
patří	patřit	k5eAaImIp3nS
k	k	k7c3
nejvýznamnějším	významný	k2eAgFnPc3d3
mexickým	mexický	k2eAgFnPc3d1
kulturním	kulturní	k2eAgFnPc3d1
institucím	instituce	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malířem	malíř	k1gMnSc7
byl	být	k5eAaImAgMnS
i	i	k9
Fridin	Fridin	k2eAgMnSc1d1
manžel	manžel	k1gMnSc1
Diego	Diego	k1gMnSc1
Rivera	Rivera	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Patřil	patřit	k5eAaImAgMnS
k	k	k7c3
zakladatelům	zakladatel	k1gMnPc3
mexického	mexický	k2eAgInSc2d1
muralismu	muralismus	k1gInSc2
<g/>
,	,	kIx,
tedy	tedy	k9
obnovy	obnova	k1gFnSc2
tradice	tradice	k1gFnSc2
nástěnných	nástěnný	k2eAgFnPc2d1
maleb	malba	k1gFnPc2
<g/>
,	,	kIx,
často	často	k6eAd1
na	na	k7c4
stěny	stěna	k1gFnPc4
moderních	moderní	k2eAgFnPc2d1
budov	budova	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
Dalšími	další	k2eAgMnPc7d1
výraznými	výrazný	k2eAgMnPc7d1
muralisty	muralista	k1gMnPc7
byli	být	k5eAaImAgMnP
David	David	k1gMnSc1
Alfaro	Alfara	k1gFnSc5
Siqueiros	Siqueirosa	k1gFnPc2
<g/>
,	,	kIx,
José	Josá	k1gFnSc2
Clemente	Clement	k1gInSc5
Orozco	Orozco	k1gMnSc1
či	či	k8xC
Juan	Juan	k1gMnSc1
O	O	kA
<g/>
'	'	kIx"
<g/>
Gorman	Gorman	k1gMnSc1
<g/>
,	,	kIx,
též	též	k9
významný	významný	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Remedios	Remediosa	k1gFnPc2
Varová	Varová	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
do	do	k7c2
Mexika	Mexiko	k1gNnSc2
utekla	utéct	k5eAaPmAgFnS
z	z	k7c2
Evropy	Evropa	k1gFnSc2
před	před	k7c7
politickou	politický	k2eAgFnSc7d1
perzekucí	perzekuce	k1gFnSc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
přihlásila	přihlásit	k5eAaPmAgFnS
k	k	k7c3
surrealismu	surrealismus	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobný	podobný	k2eAgInSc1d1
osud	osud	k1gInSc1
i	i	k8xC
zaměření	zaměření	k1gNnSc2
díla	dílo	k1gNnSc2
měla	mít	k5eAaImAgFnS
Leonora	Leonora	k1gFnSc1
Carringtonová	Carringtonový	k2eAgFnSc1d1
<g/>
,	,	kIx,
obě	dva	k4xCgFnPc4
malířky	malířka	k1gFnPc4
si	se	k3xPyFc3
byli	být	k5eAaImAgMnP
ostatně	ostatně	k6eAd1
velmi	velmi	k6eAd1
blízké	blízký	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
surrealismu	surrealismus	k1gInSc3
neměl	mít	k5eNaImAgMnS
díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
figurativní	figurativní	k2eAgFnSc3d1
abstrakci	abstrakce	k1gFnSc3
daleko	daleko	k6eAd1
ani	ani	k8xC
Rufino	Rufin	k2eAgNnSc4d1
Tamayo	Tamayo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Nejvýznamnějšími	významný	k2eAgFnPc7d3
galeriemi	galerie	k1gFnPc7
a	a	k8xC
institucemi	instituce	k1gFnPc7
věnujícími	věnující	k2eAgFnPc7d1
se	se	k3xPyFc4
sběru	sběr	k1gInSc2
umění	umění	k1gNnSc2
jsou	být	k5eAaImIp3nP
Muzeum	muzeum	k1gNnSc4
Anahuacalli	Anahuacalle	k1gFnSc3
<g/>
,	,	kIx,
Muzeum	muzeum	k1gNnSc4
Soumaya	Soumaya	k1gFnSc1
a	a	k8xC
Palác	palác	k1gInSc1
umění	umění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anahuacalli	Anahuacalle	k1gFnSc6
se	se	k3xPyFc4
specializuje	specializovat	k5eAaBmIp3nS
na	na	k7c4
umění	umění	k1gNnSc4
předkolumbovských	předkolumbovský	k2eAgFnPc2d1
kultur	kultura	k1gFnPc2
<g/>
,	,	kIx,
jádro	jádro	k1gNnSc4
sbírky	sbírka	k1gFnSc2
shromáždil	shromáždit	k5eAaPmAgMnS
Diego	Diego	k1gMnSc1
Rivera	River	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muzeum	muzeum	k1gNnSc1
Soumaya	Soumay	k1gInSc2
založil	založit	k5eAaPmAgMnS
podnikatel	podnikatel	k1gMnSc1
Carlos	Carlos	k1gMnSc1
Slim	Slim	k1gMnSc1
Helú	Helú	k1gMnSc1
<g/>
,	,	kIx,
svého	svůj	k3xOyFgInSc2
času	čas	k1gInSc2
nejbohatší	bohatý	k2eAgMnSc1d3
člověk	člověk	k1gMnSc1
planety	planeta	k1gFnSc2
<g/>
,	,	kIx,
kterému	který	k3yQgInSc3,k3yIgInSc3,k3yRgInSc3
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
zakoupit	zakoupit	k5eAaPmF
mj.	mj.	kA
řadu	řada	k1gFnSc4
soch	socha	k1gFnPc2
Augusta	Augusta	k1gMnSc1
Rodina	rodina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Originální	originální	k2eAgFnSc4d1
budovu	budova	k1gFnSc4
muzeu	muzeum	k1gNnSc6
navrhl	navrhnout	k5eAaPmAgMnS
Slimův	Slimův	k2eAgMnSc1d1
zeť	zeť	k1gMnSc1
Fernando	Fernanda	k1gFnSc5
Romero	Romero	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palác	palác	k1gInSc1
umění	umění	k1gNnSc2
je	být	k5eAaImIp3nS
proti	proti	k7c3
tomu	ten	k3xDgNnSc3
státní	státní	k2eAgMnPc1d1
institucí	instituce	k1gFnSc7
a	a	k8xC
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
něm	on	k3xPp3gInSc6
krom	krom	k7c2
muzeí	muzeum	k1gNnPc2
a	a	k8xC
galerií	galerie	k1gFnPc2
i	i	k8xC
státní	státní	k2eAgFnSc1d1
opera	opera	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Architekt	architekt	k1gMnSc1
Luis	Luisa	k1gFnPc2
Barragán	Barragán	k1gMnSc1
získal	získat	k5eAaPmAgMnS
roku	rok	k1gInSc2
1980	#num#	k4
prestižní	prestižní	k2eAgFnSc4d1
Pritzkerovu	Pritzkerův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
<g/>
,	,	kIx,
zvanou	zvaný	k2eAgFnSc4d1
též	též	k9
„	„	k?
<g/>
Nobelova	Nobelův	k2eAgFnSc1d1
cena	cena	k1gFnSc1
za	za	k7c4
architekturu	architektura	k1gFnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Film	film	k1gInSc1
</s>
<s>
Mexický	mexický	k2eAgMnSc1d1
herec	herec	k1gMnSc1
Gael	Gael	k1gMnSc1
García	García	k1gMnSc1
Bernal	Bernal	k1gMnSc1
</s>
<s>
Počátkem	počátkem	k7c2
30	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
začíná	začínat	k5eAaImIp3nS
točit	točit	k5eAaImF
v	v	k7c6
Mexiku	Mexiko	k1gNnSc6
zvukový	zvukový	k2eAgInSc4d1
film	film	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základy	základ	k1gInPc7
mexického	mexický	k2eAgNnSc2d1
filmového	filmový	k2eAgNnSc2d1
umění	umění	k1gNnSc2
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
pokládal	pokládat	k5eAaImAgMnS
ruský	ruský	k2eAgMnSc1d1
režisér	režisér	k1gMnSc1
Sergej	Sergej	k1gMnSc1
Michajlovič	Michajlovič	k1gMnSc1
Ejzenštejn	Ejzenštejn	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
zde	zde	k6eAd1
natočil	natočit	k5eAaBmAgMnS
snímek	snímek	k1gInSc4
Que	Que	k1gMnSc1
viva	viva	k1gMnSc1
México	México	k1gMnSc1
(	(	kIx(
<g/>
Ať	ať	k9
žije	žít	k5eAaImIp3nS
Mexiko	Mexiko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
40	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
50	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnSc2
jsou	být	k5eAaImIp3nP
nazývána	nazývat	k5eAaImNgFnS
zlatou	zlatý	k2eAgFnSc7d1
érou	éra	k1gFnSc7
mexického	mexický	k2eAgInSc2d1
filmu	film	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významným	významný	k2eAgNnSc7d1
režisérem	režisér	k1gMnSc7
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
byl	být	k5eAaImAgMnS
Emilio	Emilio	k1gMnSc1
Fernández	Fernández	k1gMnSc1
<g/>
,	,	kIx,
mimochodem	mimochodem	k9
model	model	k1gInSc1
pro	pro	k7c4
slavnou	slavný	k2eAgFnSc4d1
sošku	soška	k1gFnSc4
Oscara	Oscar	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
Kameramanem	kameraman	k1gMnSc7
pak	pak	k8xC
Gabriel	Gabriel	k1gMnSc1
Figueroa	Figueroa	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
v	v	k7c6
Mexiku	Mexiko	k1gNnSc6
žil	žít	k5eAaImAgMnS
a	a	k8xC
tvořil	tvořit	k5eAaImAgInS
také	také	k9
slavný	slavný	k2eAgMnSc1d1
španělský	španělský	k2eAgMnSc1d1
režisér	režisér	k1gMnSc1
Luis	Luisa	k1gFnPc2
Buñ	Buñ	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
kvalita	kvalita	k1gFnSc1
mexického	mexický	k2eAgInSc2d1
filmu	film	k1gInSc2
znovu	znovu	k6eAd1
prudce	prudko	k6eAd1
zvedla	zvednout	k5eAaPmAgFnS
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
klíčovými	klíčový	k2eAgFnPc7d1
osobnostmi	osobnost	k1gFnPc7
tohoto	tento	k3xDgNnSc2
obrození	obrození	k1gNnSc2
jsou	být	k5eAaImIp3nP
Guillermo	Guillerma	k1gFnSc5
del	del	k?
Toro	Toro	k?
<g/>
,	,	kIx,
Alejandro	Alejandra	k1gFnSc5
González	Gonzáleza	k1gFnPc2
Iñ	Iñ	k1gInSc2
a	a	k8xC
Alfonso	Alfonso	k1gMnSc1
Cuarón	Cuarón	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc7
mexickým	mexický	k2eAgMnSc7d1
filmařem	filmař	k1gMnSc7
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
dostal	dostat	k5eAaPmAgMnS
Oscara	Oscar	k1gMnSc4
za	za	k7c4
režii	režie	k1gFnSc4
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
roku	rok	k1gInSc2
2013	#num#	k4
za	za	k7c4
snímek	snímek	k1gInSc4
Gravitace	gravitace	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
dalších	další	k2eAgNnPc6d1
dvou	dva	k4xCgNnPc6
letech	léto	k1gNnPc6
si	se	k3xPyFc3
Oscara	Oscar	k1gMnSc4
za	za	k7c4
režii	režie	k1gFnSc4
odnesl	odnést	k5eAaPmAgMnS
Iñ	Iñ	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
del	del	k?
Torro	Torro	k1gNnSc4
a	a	k8xC
rok	rok	k1gInSc4
poté	poté	k6eAd1
znovu	znovu	k6eAd1
Cuarón	Cuarón	k1gInSc4
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
vyvrcholila	vyvrcholit	k5eAaPmAgFnS
„	„	k?
<g/>
mexická	mexický	k2eAgFnSc1d1
oscarová	oscarový	k2eAgFnSc1d1
záplava	záplava	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
Emmanuel	Emmanuel	k1gMnSc1
Lubezki	Lubezk	k1gFnSc2
dostal	dostat	k5eAaPmAgMnS
za	za	k7c4
Gravitaci	gravitace	k1gFnSc4
Oscara	Oscar	k1gMnSc2
za	za	k7c4
kameru	kamera	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Guillermo	Guillerma	k1gFnSc5
Navarro	Navarra	k1gFnSc5
ho	on	k3xPp3gMnSc4
získal	získat	k5eAaPmAgInS
za	za	k7c4
výtvarné	výtvarný	k2eAgNnSc4d1
pojetí	pojetí	k1gNnSc4
snímku	snímek	k1gInSc2
Faunův	Faunův	k2eAgInSc4d1
labyrint	labyrint	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
Američan	Američan	k1gMnSc1
Robert	Robert	k1gMnSc1
Rodriguez	Rodriguez	k1gMnSc1
točí	točit	k5eAaImIp3nS
většinu	většina	k1gFnSc4
svých	svůj	k3xOyFgInPc2
filmů	film	k1gInPc2
v	v	k7c6
mexické	mexický	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Carlos	Carlos	k1gMnSc1
Reygadas	Reygadas	k1gMnSc1
je	být	k5eAaImIp3nS
představitelem	představitel	k1gMnSc7
artového	artový	k2eAgInSc2d1
filmu	film	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
důležitým	důležitý	k2eAgMnPc3d1
režisérům	režisér	k1gMnPc3
současnosti	současnost	k1gFnSc2
patří	patřit	k5eAaImIp3nS
mj.	mj.	kA
Arturo	Artura	k1gFnSc5
Ripstein	Ripstein	k1gMnSc1
<g/>
,	,	kIx,
Alfonso	Alfonso	k1gMnSc1
Arau	Araus	k1gInSc2
<g/>
,	,	kIx,
Amat	Amat	k1gMnSc1
Escalante	Escalant	k1gMnSc5
a	a	k8xC
Jorge	Jorge	k1gNnSc3
Fons	Fonsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
nejznámějším	známý	k2eAgMnPc3d3
mexickým	mexický	k2eAgMnPc3d1
hercům	herec	k1gMnPc3
současnosti	současnost	k1gFnSc2
patří	patřit	k5eAaImIp3nS
Gael	Gael	k1gMnSc1
García	García	k1gMnSc1
Bernal	Bernal	k1gMnSc1
či	či	k8xC
Diego	Diego	k1gMnSc1
Luna	luna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
éře	éra	k1gFnSc6
němého	němý	k2eAgInSc2d1
filmu	film	k1gInSc2
to	ten	k3xDgNnSc1
byl	být	k5eAaImAgInS
Ramon	Ramona	k1gFnPc2
Novarro	Novarro	k1gNnSc1
<g/>
,	,	kIx,
ve	v	k7c6
40	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
Pedro	Pedro	k1gNnSc4
Armendáriz	Armendáriz	k1gMnSc1
<g/>
,	,	kIx,
Dolores	Dolores	k1gMnSc1
del	del	k?
Río	Río	k1gMnSc1
a	a	k8xC
María	María	k1gFnSc1
Félixová	Félixový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ricardo	Ricardo	k1gNnSc1
Montalbán	Montalbán	k2eAgMnSc1d1
se	se	k3xPyFc4
v	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
letech	let	k1gInPc6
proslavil	proslavit	k5eAaPmAgInS
rolí	role	k1gFnSc7
ve	v	k7c4
sci-fi	sci-fi	k1gFnSc4
sérii	série	k1gFnSc4
Planeta	planeta	k1gFnSc1
opic	opice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
slaví	slavit	k5eAaImIp3nS
mexická	mexický	k2eAgFnSc1d1
televizní	televizní	k2eAgFnSc1d1
tvorba	tvorba	k1gFnSc1
celosvětový	celosvětový	k2eAgInSc1d1
úspěch	úspěch	k1gInSc4
se	s	k7c7
svými	svůj	k3xOyFgFnPc7
telenovelami	telenovela	k1gFnPc7
(	(	kIx(
<g/>
Maria	Maria	k1gFnSc1
Mercedes	mercedes	k1gInSc1
<g/>
,	,	kIx,
Rosalinda	Rosalinda	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proslavila	proslavit	k5eAaPmAgFnS
se	se	k3xPyFc4
v	v	k7c6
nich	on	k3xPp3gInPc6
například	například	k6eAd1
herečka	herečka	k1gFnSc1
a	a	k8xC
zpěvačka	zpěvačka	k1gFnSc1
Thalía	Thalía	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Hudba	hudba	k1gFnSc1
</s>
<s>
Klasická	klasický	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
</s>
<s>
K	k	k7c3
nejslavnějším	slavný	k2eAgMnPc3d3
mexickým	mexický	k2eAgMnPc3d1
hudebním	hudební	k2eAgMnPc3d1
skladatelům	skladatel	k1gMnPc3
patří	patřit	k5eAaImIp3nP
Carlos	Carlos	k1gMnSc1
Chávez	Chávez	k1gMnSc1
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
známé	známá	k1gFnSc2
Sinfonía	Sinfonía	k1gMnSc1
india	indium	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
byly	být	k5eAaImAgFnP
využity	využít	k5eAaPmNgInP
i	i	k9
tradiční	tradiční	k2eAgInPc1d1
indiánské	indiánský	k2eAgInPc1d1
nástroje	nástroj	k1gInPc1
a	a	k8xC
hudební	hudební	k2eAgInPc1d1
postupy	postup	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
Chávez	Chávez	k1gInSc1
byl	být	k5eAaImAgInS
rovněž	rovněž	k9
zakladatelem	zakladatel	k1gMnSc7
mexického	mexický	k2eAgInSc2d1
Národního	národní	k2eAgInSc2d1
symfonického	symfonický	k2eAgInSc2d1
orchestru	orchestr	k1gInSc2
(	(	kIx(
<g/>
Orquesta	Orquesta	k1gMnSc1
Sinfónica	Sinfónica	k1gMnSc1
Nacional	Nacional	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc4
mexickou	mexický	k2eAgFnSc4d1
operu	opera	k1gFnSc4
s	s	k7c7
názvem	název	k1gInSc7
La	la	k1gNnSc2
Parténope	Parténop	k1gInSc5
složil	složit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1711	#num#	k4
Manuel	Manuel	k1gMnSc1
de	de	k?
Zumaya	Zumaya	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
výrazným	výrazný	k2eAgMnPc3d1
skladatelům	skladatel	k1gMnPc3
vážné	vážný	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
patřil	patřit	k5eAaImAgMnS
Manuel	Manuel	k1gMnSc1
María	María	k1gMnSc1
Ponce	Ponce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodní	mezinárodní	k2eAgFnSc4d1
reputaci	reputace	k1gFnSc4
získal	získat	k5eAaPmAgMnS
i	i	k9
houslista	houslista	k1gMnSc1
Silvestre	Silvestr	k1gInSc5
Revueltas	Revueltas	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
rovněž	rovněž	k9
skládal	skládat	k5eAaImAgMnS
filmovou	filmový	k2eAgFnSc4d1
hudbu	hudba	k1gFnSc4
pro	pro	k7c4
vznikající	vznikající	k2eAgFnSc4d1
mexickou	mexický	k2eAgFnSc4d1
kinematografii	kinematografie	k1gFnSc4
ve	v	k7c6
30	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
40	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proslulým	proslulý	k2eAgNnSc7d1
klavíristou	klavírista	k1gMnSc7
byl	být	k5eAaImAgInS
Consuelo	Consuela	k1gFnSc5
Velázquez	Velázquez	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
operní	operní	k2eAgMnPc1d1
pěvci	pěvec	k1gMnPc1
se	se	k3xPyFc4
prosadili	prosadit	k5eAaPmAgMnP
Ramón	Ramón	k1gMnSc1
Vargas	Vargas	k1gMnSc1
a	a	k8xC
Rolando	Rolanda	k1gFnSc5
Villazón	Villazón	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Populární	populární	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
</s>
<s>
Carlos	Carlos	k1gMnSc1
Santana	Santan	k1gMnSc2
</s>
<s>
Ke	k	k7c3
známým	známý	k2eAgMnPc3d1
zpěvákům	zpěvák	k1gMnPc3
populární	populární	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
patří	patřit	k5eAaImIp3nS
například	například	k6eAd1
Jorge	Jorge	k1gInSc4
Negrete	Negre	k1gNnSc2
<g/>
,	,	kIx,
Pedro	Pedra	k1gMnSc5
Infante	infant	k1gMnSc5
<g/>
,	,	kIx,
Vicente	Vicent	k1gMnSc5
Fernández	Fernández	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
Alejandro	Alejandra	k1gFnSc5
Fernández	Fernández	k1gMnSc1
<g/>
,	,	kIx,
nejznámější	známý	k2eAgMnPc1d3
představitelé	představitel	k1gMnPc1
stylu	styl	k1gInSc2
zvaného	zvaný	k2eAgInSc2d1
canción	canción	k1gInSc1
ranchera	rancher	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
lze	lze	k6eAd1
považovat	považovat	k5eAaImF
za	za	k7c4
nejsilnější	silný	k2eAgFnSc4d3
větev	větev	k1gFnSc4
mexického	mexický	k2eAgInSc2d1
folku	folk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Představitelem	představitel	k1gMnSc7
bolera	bolero	k1gNnSc2
byl	být	k5eAaImAgMnS
Agustín	Agustín	k1gMnSc1
Lara	Lara	k1gMnSc1
a	a	k8xC
Armando	Armanda	k1gFnSc5
Manzanero	Manzanera	k1gFnSc5
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc2
skladby	skladba	k1gFnSc2
nazpívali	nazpívat	k5eAaBmAgMnP,k5eAaPmAgMnP
Frank	frank	k1gInSc4
Sinatra	Sinatrum	k1gNnSc2
<g/>
,	,	kIx,
Tony	Tony	k1gMnSc1
Bennett	Bennett	k1gMnSc1
či	či	k8xC
Elvis	Elvis	k1gMnSc1
Presley	Preslea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
představitele	představitel	k1gMnPc4
klasického	klasický	k2eAgInSc2d1
popu	pop	k1gInSc2
„	„	k?
<g/>
západního	západní	k2eAgInSc2d1
střihu	střih	k1gInSc2
<g/>
“	“	k?
lze	lze	k6eAd1
považovat	považovat	k5eAaImF
skupinu	skupina	k1gFnSc4
RBD	RBD	kA
<g/>
.	.	kIx.
</s>
<s>
Populární	populární	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
zpěvačky	zpěvačka	k1gFnPc1
Daniela	Daniel	k1gMnSc2
Romo	Romo	k1gNnSc4
<g/>
,	,	kIx,
Thalía	Thalía	k1gFnSc1
<g/>
,	,	kIx,
Paulina	Paulin	k2eAgFnSc1d1
Rubio	Rubio	k6eAd1
<g/>
,	,	kIx,
Maite	Mait	k1gInSc5
Perroni	Perroň	k1gFnSc6
<g/>
,	,	kIx,
Belinda	Belinda	k1gFnSc1
Peregrín	Peregrín	k1gInSc1
<g/>
,	,	kIx,
Lila	lila	k2eAgInSc1d1
Downs	Downs	k1gInSc1
<g/>
,	,	kIx,
Dulce	Dulce	k1gFnSc1
María	María	k1gFnSc1
<g/>
,	,	kIx,
Anahí	Anahí	k1gNnSc1
a	a	k8xC
Lucero	Lucero	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známými	známý	k2eAgMnPc7d1
zpěváky	zpěvák	k1gMnPc7
populárních	populární	k2eAgFnPc2d1
písní	píseň	k1gFnPc2
jsou	být	k5eAaImIp3nP
Juan	Juan	k1gMnSc1
Gabriel	Gabriel	k1gMnSc1
a	a	k8xC
José	Josý	k2eAgFnPc4d1
José	Josá	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typické	typický	k2eAgMnPc4d1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
zejména	zejména	k9
zpěvačky	zpěvačka	k1gFnPc1
začínají	začínat	k5eAaImIp3nP
svou	svůj	k3xOyFgFnSc4
kariéru	kariéra	k1gFnSc4
jako	jako	k8xS,k8xC
herečky	herečka	k1gFnPc4
telenovel	telenovela	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
někteří	některý	k3yIgMnPc1
představitelé	představitel	k1gMnPc1
americké	americký	k2eAgFnSc2d1
pop-music	pop-music	k1gFnSc2
<g/>
,	,	kIx,
zvané	zvaný	k2eAgFnSc2d1
Latin	Latin	k1gMnSc1
pop	pop	k1gMnSc1
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
mexický	mexický	k2eAgInSc4d1
původ	původ	k1gInSc4
<g/>
,	,	kIx,
byť	byť	k8xS
se	se	k3xPyFc4
třeba	třeba	k6eAd1
narodili	narodit	k5eAaPmAgMnP
již	již	k6eAd1
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
je	být	k5eAaImIp3nS
skupina	skupina	k1gFnSc1
Ha	ha	kA
<g/>
*	*	kIx~
<g/>
Ash	Ash	k1gMnSc1
nebo	nebo	k8xC
zpěvačka	zpěvačka	k1gFnSc1
Selena	selen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Mexiku	Mexiko	k1gNnSc6
se	se	k3xPyFc4
naopak	naopak	k6eAd1
usadil	usadit	k5eAaPmAgMnS
kubánský	kubánský	k2eAgMnSc1d1
mambo	mamba	k1gFnSc5
hudebník	hudebník	k1gMnSc1
Pérez	Pérez	k1gInSc1
Prado	Prado	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Za	za	k7c4
největší	veliký	k2eAgFnSc4d3
hvězdu	hvězda	k1gFnSc4
mexického	mexický	k2eAgInSc2d1
rocku	rock	k1gInSc2
platí	platit	k5eAaImIp3nS
Carlos	Carlos	k1gMnSc1
Santana	Santana	k1gFnSc1
<g/>
,	,	kIx,
často	často	k6eAd1
označovaný	označovaný	k2eAgMnSc1d1
za	za	k7c4
nejlepšího	dobrý	k2eAgMnSc4d3
kytaristu	kytarista	k1gMnSc4
světa	svět	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
proslul	proslout	k5eAaPmAgMnS
svým	svůj	k3xOyFgNnPc3
jedenáctiminutovým	jedenáctiminutový	k2eAgNnPc3d1
vystoupením	vystoupení	k1gNnPc3
na	na	k7c6
festivalu	festival	k1gInSc6
ve	v	k7c6
Woodstocku	Woodstock	k1gInSc6
roku	rok	k1gInSc2
1969	#num#	k4
a	a	k8xC
silně	silně	k6eAd1
poté	poté	k6eAd1
ovlivnil	ovlivnit	k5eAaPmAgInS
americkou	americký	k2eAgFnSc4d1
a	a	k8xC
vůbec	vůbec	k9
světovou	světový	k2eAgFnSc4d1
rockovou	rockový	k2eAgFnSc4d1
hudbu	hudba	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
Ke	k	k7c3
známým	známý	k2eAgFnPc3d1
mexickým	mexický	k2eAgFnPc3d1
rockovým	rockový	k2eAgFnPc3d1
skupinám	skupina	k1gFnPc3
patří	patřit	k5eAaImIp3nS
rovněž	rovněž	k9
Maná	Maná	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
představitelky	představitelka	k1gFnSc2
folk-rocku	folk-rock	k1gInSc2
bývají	bývat	k5eAaImIp3nP
označovány	označovat	k5eAaImNgFnP
Natalia	Natalium	k1gNnPc1
Lafourcade	Lafourcad	k1gInSc5
a	a	k8xC
Julieta	Juliet	k1gMnSc4
Venegas	Venegas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
se	se	k3xPyFc4
v	v	k7c6
žánru	žánr	k1gInSc6
hip	hip	k0
hop	hop	k0
prosadila	prosadit	k5eAaPmAgFnS
skupina	skupina	k1gFnSc1
Control	Controla	k1gFnPc2
Machete	Mache	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oboru	obor	k1gInSc6
heavy	heava	k1gFnSc2
metal	metal	k1gInSc1
je	být	k5eAaImIp3nS
známa	znám	k2eAgFnSc1d1
například	například	k6eAd1
skupina	skupina	k1gFnSc1
Brujeria	Brujerium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
jazzový	jazzový	k2eAgMnSc1d1
hudebník	hudebník	k1gMnSc1
vynikl	vyniknout	k5eAaPmAgMnS
bubeník	bubeník	k1gMnSc1
Antonio	Antonio	k1gMnSc1
Sánchez	Sánchez	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Mariachi	Mariachi	k6eAd1
</s>
<s>
Skupina	skupina	k1gFnSc1
mariachi	mariach	k1gFnSc2
při	při	k7c6
XIII	XIII	kA
<g/>
.	.	kIx.
festivalu	festival	k1gInSc2
v	v	k7c6
Guadalajaře	Guadalajara	k1gFnSc6
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2006	#num#	k4
</s>
<s>
Za	za	k7c2
specificky	specificky	k6eAd1
mexické	mexický	k2eAgFnSc2d1
hudební	hudební	k2eAgFnSc2d1
formace	formace	k1gFnSc2
lze	lze	k6eAd1
označit	označit	k5eAaPmF
tzv.	tzv.	kA
mariachi	mariachi	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
hudební	hudební	k2eAgInSc1d1
jev	jev	k1gInSc1
pochází	pocházet	k5eAaImIp3nS
původně	původně	k6eAd1
ze	z	k7c2
spolkového	spolkový	k2eAgInSc2d1
státu	stát	k1gInSc2
Jalisco	Jalisco	k6eAd1
<g/>
,	,	kIx,
postupně	postupně	k6eAd1
se	se	k3xPyFc4
však	však	k9
regionálně	regionálně	k6eAd1
rozlišil	rozlišit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
světě	svět	k1gInSc6
je	být	k5eAaImIp3nS
tento	tento	k3xDgInSc4
styl	styl	k1gInSc4
lidové	lidový	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
považován	považován	k2eAgInSc1d1
za	za	k7c4
typicky	typicky	k6eAd1
mexický	mexický	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
byla	být	k5eAaImAgFnS
hudba	hudba	k1gFnSc1
souborů	soubor	k1gInPc2
mariachi	mariachi	k6eAd1
zařazena	zařazen	k2eAgFnSc1d1
do	do	k7c2
seznamu	seznam	k1gInSc2
Mistrovských	mistrovský	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
ústního	ústní	k2eAgNnSc2d1
a	a	k8xC
nehmotného	hmotný	k2eNgNnSc2d1
dědictví	dědictví	k1gNnSc2
lidstva	lidstvo	k1gNnSc2
organizace	organizace	k1gFnSc2
UNESCO	UNESCO	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
Počet	počet	k1gInSc1
hudebníků	hudebník	k1gMnPc2
v	v	k7c6
souboru	soubor	k1gInSc6
je	být	k5eAaImIp3nS
většinou	většinou	k6eAd1
7	#num#	k4
až	až	k9
12	#num#	k4
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
až	až	k9
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
následující	následující	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
<g/>
:	:	kIx,
kytary	kytara	k1gFnPc4
<g/>
,	,	kIx,
mexické	mexický	k2eAgFnPc4d1
vihuely	vihuela	k1gFnPc4
<g/>
,	,	kIx,
guitarrón	guitarrón	k1gInSc4
<g/>
,	,	kIx,
housle	housle	k1gFnPc4
<g/>
,	,	kIx,
trumpety	trumpeta	k1gFnPc4
<g/>
,	,	kIx,
harfa	harfa	k1gFnSc1
a	a	k8xC
někdy	někdy	k6eAd1
také	také	k9
maraca	maraca	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
skupinám	skupina	k1gFnPc3
patří	patřit	k5eAaImIp3nS
též	též	k9
zpěváci	zpěvák	k1gMnPc1
<g/>
,	,	kIx,
avšak	avšak	k8xC
zpěv	zpěv	k1gInSc4
písní	píseň	k1gFnPc2
mohou	moct	k5eAaImIp3nP
přebírat	přebírat	k5eAaImF
instrumentalisté	instrumentalista	k1gMnPc1
<g/>
,	,	kIx,
buď	buď	k8xC
sborově	sborově	k6eAd1
nebo	nebo	k8xC
jako	jako	k9
solisté	solista	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
mariachi	mariach	k1gFnSc2
</s>
<s>
Lidová	lidový	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
</s>
<s>
Významné	významný	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
především	především	k9
hrnčířství	hrnčířství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
textilní	textilní	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
rozšířena	rozšířit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
zejména	zejména	k9
vlna	vlna	k1gFnSc1
a	a	k8xC
bavlna	bavlna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
henequenu	henequen	k1gInSc2
a	a	k8xC
ixtle	ixtle	k1gFnSc2
(	(	kIx(
<g/>
agáve	agáve	k1gFnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
vyrábí	vyrábět	k5eAaImIp3nS
brašny	brašna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
rákosí	rákosí	k1gNnSc2
se	se	k3xPyFc4
vyrábějí	vyrábět	k5eAaImIp3nP
různé	různý	k2eAgFnPc4d1
krytiny	krytina	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
charros	charrosa	k1gFnPc2
(	(	kIx(
<g/>
klobouky	klobouk	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
běžné	běžný	k2eAgFnPc4d1
lidové	lidový	k2eAgFnPc4d1
slavnosti	slavnost	k1gFnPc4
patří	patřit	k5eAaImIp3nP
býčí	býčí	k2eAgInPc1d1
a	a	k8xC
kohoutí	kohoutí	k2eAgInPc1d1
zápasy	zápas	k1gInPc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
udržení	udržení	k1gNnSc1
se	se	k3xPyFc4
na	na	k7c6
nezkrocených	zkrocený	k2eNgMnPc6d1
koních	kůň	k1gMnPc6
tzv.	tzv.	kA
charros	charrosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
převažují	převažovat	k5eAaImIp3nP
různé	různý	k2eAgFnPc1d1
indiánské	indiánský	k2eAgFnPc1d1
a	a	k8xC
křesťanské	křesťanský	k2eAgFnPc1d1
slavnosti	slavnost	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mexičané	Mexičan	k1gMnPc1
rádi	rád	k2eAgMnPc1d1
oslavují	oslavovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oslava	oslava	k1gFnSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
fiesta	fiesta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sejde	sejít	k5eAaPmIp3nS
se	se	k3xPyFc4
nejméně	málo	k6eAd3
dvacet	dvacet	k4xCc4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
příbuzných	příbuzný	k1gMnPc2
<g/>
,	,	kIx,
přátel	přítel	k1gMnPc2
nebo	nebo	k8xC
známých	známý	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
počtu	počet	k1gInSc3
lidí	člověk	k1gMnPc2
se	se	k3xPyFc4
fiesty	fiesta	k1gFnPc1
odehrávají	odehrávat	k5eAaImIp3nP
často	často	k6eAd1
venku	venku	k6eAd1
<g/>
,	,	kIx,
na	na	k7c6
dvorku	dvorek	k1gInSc6
nebo	nebo	k8xC
v	v	k7c6
zahradě	zahrada	k1gFnSc6
domu	dům	k1gInSc2
<g/>
,	,	kIx,
večer	večer	k6eAd1
pak	pak	k6eAd1
při	při	k7c6
světle	světlo	k1gNnSc6
lampionů	lampion	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
děti	dítě	k1gFnPc4
je	být	k5eAaImIp3nS
připravena	připravit	k5eAaPmNgFnS
piňata	piňata	k1gFnSc1
<g/>
,	,	kIx,
keramická	keramický	k2eAgFnSc1d1
nádoba	nádoba	k1gFnSc1
obalená	obalený	k2eAgFnSc1d1
staniolem	staniol	k1gInSc7
<g/>
,	,	kIx,
barevným	barevný	k2eAgInSc7d1
papírem	papír	k1gInSc7
a	a	k8xC
papírovými	papírový	k2eAgFnPc7d1
třásněmi	třáseň	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvnitř	uvnitř	k6eAd1
jsou	být	k5eAaImIp3nP
bonbóny	bonbón	k1gInPc4
<g/>
,	,	kIx,
drobné	drobný	k2eAgNnSc4d1
ovoce	ovoce	k1gNnSc4
<g/>
,	,	kIx,
ořechy	ořech	k1gInPc1
<g/>
,	,	kIx,
malé	malý	k2eAgFnPc1d1
hračky	hračka	k1gFnPc1
a	a	k8xC
papírové	papírový	k2eAgInPc1d1
vločky	vloček	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Piñ	Piñ	k1gFnSc1
visí	vise	k1gFnPc2
na	na	k7c6
dlouhém	dlouhý	k2eAgInSc6d1
provazu	provaz	k1gInSc6
a	a	k8xC
dva	dva	k4xCgMnPc1
z	z	k7c2
účastníků	účastník	k1gMnPc2
oslavy	oslava	k1gFnSc2
tahají	tahat	k5eAaImIp3nP
za	za	k7c2
konce	konec	k1gInSc2
provazu	provaz	k1gInSc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
piňata	piňata	k1gFnSc1
byla	být	k5eAaImAgFnS
na	na	k7c4
dosah	dosah	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
ve	v	k7c6
stálém	stálý	k2eAgInSc6d1
pohybu	pohyb	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děti	dítě	k1gFnPc1
dostanou	dostat	k5eAaPmIp3nP
hůl	hůl	k1gFnSc4
a	a	k8xC
jejich	jejich	k3xOp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
nádobu	nádoba	k1gFnSc4
zasáhnout	zasáhnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všichni	všechen	k3xTgMnPc1
přítomní	přítomný	k1gMnPc1
sledují	sledovat	k5eAaImIp3nP
s	s	k7c7
povyražením	povyražení	k1gNnSc7
marné	marný	k2eAgFnSc2d1
i	i	k8xC
úspěšné	úspěšný	k2eAgInPc4d1
pokusy	pokus	k1gInPc4
a	a	k8xC
bouřlivě	bouřlivě	k6eAd1
povzbuzují	povzbuzovat	k5eAaImIp3nP
tradičním	tradiční	k2eAgInSc7d1
popěvkem	popěvek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
přijdou	přijít	k5eAaPmIp3nP
na	na	k7c4
řadu	řada	k1gFnSc4
dospělí	dospělý	k2eAgMnPc1d1
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
mít	mít	k5eAaImF
převázané	převázaný	k2eAgFnPc4d1
oči	oko	k1gNnPc4
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
snaha	snaha	k1gFnSc1
zasáhnout	zasáhnout	k5eAaPmF
piňatu	piňata	k1gFnSc4
je	být	k5eAaImIp3nS
doprovázena	doprovázet	k5eAaImNgFnS
všeobecným	všeobecný	k2eAgNnSc7d1
veselím	veselí	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
fiestě	fiesta	k1gFnSc3
patří	patřit	k5eAaImIp3nS
hodně	hodně	k6eAd1
hudby	hudba	k1gFnSc2
a	a	k8xC
samozřejmě	samozřejmě	k6eAd1
tanec	tanec	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
fiesta	fiesta	k1gFnSc1
oslavou	oslava	k1gFnSc7
v	v	k7c6
kruhu	kruh	k1gInSc6
příbuzných	příbuzný	k1gMnPc2
<g/>
,	,	kIx,
pak	pak	k6eAd1
feria	feria	k1gFnSc1
bývá	bývat	k5eAaImIp3nS
výroční	výroční	k2eAgFnSc7d1
slavností	slavnost	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
zasahuje	zasahovat	k5eAaImIp3nS
celé	celý	k2eAgNnSc4d1
město	město	k1gNnSc4
a	a	k8xC
okolí	okolí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
vlastně	vlastně	k9
výroční	výroční	k2eAgInSc1d1
trh	trh	k1gInSc1
a	a	k8xC
výstava	výstava	k1gFnSc1
té	ten	k3xDgFnSc2
výrobní	výrobní	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
v	v	k7c6
dané	daný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
převažuje	převažovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takže	takže	k8xS
ve	v	k7c6
městě	město	k1gNnSc6
Taxco	Taxco	k6eAd1
<g/>
,	,	kIx,
známém	známý	k2eAgNnSc6d1
výrobou	výroba	k1gFnSc7
stříbrných	stříbrný	k2eAgMnPc2d1
předmětů	předmět	k1gInPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
koná	konat	k5eAaImIp3nS
feria	ferium	k1gNnSc2
stříbra	stříbro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinde	jinde	k6eAd1
je	být	k5eAaImIp3nS
feria	ferius	k1gMnSc4
výrobků	výrobek	k1gInPc2
z	z	k7c2
kůže	kůže	k1gFnSc2
<g/>
,	,	kIx,
jinde	jinde	k6eAd1
feria	feria	k1gFnSc1
květin	květina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Můžete	moct	k5eAaImIp2nP
navštívit	navštívit	k5eAaPmF
dílny	dílna	k1gFnPc4
zručných	zručný	k2eAgMnPc2d1
řemeslníků	řemeslník	k1gMnPc2
a	a	k8xC
pozorovat	pozorovat	k5eAaImF
jejich	jejich	k3xOp3gFnSc4
práci	práce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Večer	večer	k6eAd1
na	na	k7c6
náměstí	náměstí	k1gNnSc6
vystupují	vystupovat	k5eAaImIp3nP
hudebníci	hudebník	k1gMnPc1
a	a	k8xC
tanečníci	tanečník	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvuky	zvuk	k1gInPc7
mariachis	mariachis	k1gFnPc2
<g/>
,	,	kIx,
lidové	lidový	k2eAgFnPc1d1
písně	píseň	k1gFnPc1
a	a	k8xC
temperamentní	temperamentní	k2eAgInSc1d1
děj	děj	k1gInSc1
na	na	k7c6
pódiu	pódium	k1gNnSc6
rozehřeje	rozehřát	k5eAaPmIp3nS
postávající	postávající	k2eAgMnPc4d1
diváky	divák	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
podupávají	podupávat	k5eAaImIp3nP
a	a	k8xC
přizvukují	přizvukovat	k5eAaImIp3nP
do	do	k7c2
taktu	takt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
poledních	polední	k2eAgFnPc6d1
hodinách	hodina	k1gFnPc6
začíná	začínat	k5eAaImIp3nS
být	být	k5eAaImF
velké	velký	k2eAgNnSc1d1
vedro	vedro	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
odpoledne	odpoledne	k6eAd1
stává	stávat	k5eAaImIp3nS
úmorným	úmorný	k2eAgMnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
tak	tak	k6eAd1
Mexičané	Mexičan	k1gMnPc1
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
Španělé	Španěl	k1gMnPc1
a	a	k8xC
obyvatelé	obyvatel	k1gMnPc1
Latinské	latinský	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
tráví	trávit	k5eAaImIp3nP
siestu	siesta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Siesta	siesta	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
již	již	k6eAd1
v	v	k7c6
době	doba	k1gFnSc6
těžkých	těžký	k2eAgFnPc2d1
ručních	ruční	k2eAgFnPc2d1
prací	práce	k1gFnPc2
na	na	k7c6
polích	pole	k1gNnPc6
nebo	nebo	k8xC
stavbách	stavba	k1gFnPc6
<g/>
,	,	kIx,
protože	protože	k8xS
práce	práce	k1gFnSc1
pod	pod	k7c7
žhavým	žhavý	k2eAgNnSc7d1
sluncem	slunce	k1gNnSc7
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
vyčerpávající	vyčerpávající	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
tak	tak	k6eAd1
odpolední	odpolední	k2eAgInSc4d1
spánek	spánek	k1gInSc4
a	a	k8xC
odpočinek	odpočinek	k1gInSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
siesta	siesta	k1gFnSc1
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
znakem	znak	k1gInSc7
lenosti	lenost	k1gFnSc2
nebo	nebo	k8xC
přejedení	přejedení	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
téměř	téměř	k6eAd1
nevyhnutelností	nevyhnutelnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
můžete	moct	k5eAaImIp2nP
<g/>
,	,	kIx,
doplňte	doplnit	k5eAaPmRp2nP
prosím	prosit	k5eAaImIp1nS
snímek	snímek	k1gInSc4
ještě	ještě	k9
jedné	jeden	k4xCgFnSc2
skupiny	skupina	k1gFnSc2
mariachi	mariachi	k6eAd1
<g/>
,	,	kIx,
dvě	dva	k4xCgFnPc4
jsou	být	k5eAaImIp3nP
málo	málo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děkuji	děkovat	k5eAaImIp1nS
<g/>
,	,	kIx,
Zhovnajsem	Zhovnajso	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Kuchyně	kuchyně	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Mexická	mexický	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Tacos	Tacos	k1gInSc1
<g/>
,	,	kIx,
typické	typický	k2eAgNnSc1d1
mexické	mexický	k2eAgNnSc1d1
jídlo	jídlo	k1gNnSc1
</s>
<s>
Typickými	typický	k2eAgFnPc7d1
surovinami	surovina	k1gFnPc7
mexické	mexický	k2eAgFnSc2d1
kuchyně	kuchyně	k1gFnSc2
jsou	být	k5eAaImIp3nP
fazole	fazole	k1gFnSc1
<g/>
,	,	kIx,
kukuřice	kukuřice	k1gFnSc1
a	a	k8xC
chilli	chilli	k1gNnSc1
papričky	paprička	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgInSc1d1
způsob	způsob	k1gInSc1
využití	využití	k1gNnSc2
kukuřice	kukuřice	k1gFnSc2
v	v	k7c6
Mexiku	Mexiko	k1gNnSc6
je	být	k5eAaImIp3nS
příprava	příprava	k1gFnSc1
tortill	tortilla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mají	mít	k5eAaImIp3nP
řadu	řada	k1gFnSc4
využití	využití	k1gNnSc2
například	například	k6eAd1
k	k	k7c3
přípravě	příprava	k1gFnSc3
tacos	tacos	k1gInSc1
<g/>
,	,	kIx,
burritos	burritos	k1gInSc1
<g/>
,	,	kIx,
quesadill	quesadill	k1gInSc1
<g/>
,	,	kIx,
chimichingy	chimiching	k1gInPc1
a	a	k8xC
dalších	další	k2eAgNnPc2d1
typických	typický	k2eAgNnPc2d1
jídel	jídlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důležitou	důležitý	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
mexické	mexický	k2eAgFnSc2d1
kuchyně	kuchyně	k1gFnSc2
jsou	být	k5eAaImIp3nP
omáčky	omáčka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
zelená	zelený	k2eAgFnSc1d1
omáčka	omáčka	k1gFnSc1
(	(	kIx(
<g/>
salsa	salsa	k1gFnSc1
verde	verde	k6eAd1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
připravená	připravený	k2eAgNnPc1d1
ze	z	k7c2
zelených	zelený	k2eAgNnPc2d1
rajčat	rajče	k1gNnPc2
a	a	k8xC
chilli	chilli	k1gNnPc2
papriček	paprička	k1gFnPc2
a	a	k8xC
má	mít	k5eAaImIp3nS
konzistenci	konzistence	k1gFnSc4
pasty	pasta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
také	také	k9
červená	červený	k2eAgFnSc1d1
omáčka	omáčka	k1gFnSc1
(	(	kIx(
<g/>
salsa	salsa	k1gFnSc1
roja	roja	k1gFnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
rajčat	rajče	k1gNnPc2
<g/>
,	,	kIx,
česneku	česnek	k1gInSc2
a	a	k8xC
koriandrové	koriandrový	k2eAgFnSc2d1
natě	nať	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tradičním	tradiční	k2eAgNnPc3d1
mexickým	mexický	k2eAgNnPc3d1
jídlům	jídlo	k1gNnPc3
patří	patřit	k5eAaImIp3nS
i	i	k9
tamales	tamales	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
tradice	tradice	k1gFnSc1
sahá	sahat	k5eAaImIp3nS
až	až	k9
do	do	k7c2
předkolumbovské	předkolumbovský	k2eAgFnSc2d1
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tamales	Tamalesa	k1gFnPc2
se	se	k3xPyFc4
připravují	připravovat	k5eAaImIp3nP
z	z	k7c2
kukuřičného	kukuřičný	k2eAgNnSc2d1
těsta	těsto	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
připravuje	připravovat	k5eAaImIp3nS
tradiční	tradiční	k2eAgFnSc7d1
metodou	metoda	k1gFnSc7
nazvanou	nazvaný	k2eAgFnSc7d1
nixtamalizace	nixtamalizace	k1gFnPc4
(	(	kIx(
<g/>
slovo	slovo	k1gNnSc1
je	být	k5eAaImIp3nS
přejato	přejmout	k5eAaPmNgNnS
z	z	k7c2
jazyka	jazyk	k1gInSc2
Aztéků	Azték	k1gMnPc2
nahuatl	nahuatnout	k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nixtamalizace	Nixtamalizace	k1gFnSc1
znamená	znamenat	k5eAaImIp3nS
specifickou	specifický	k2eAgFnSc4d1
přípravu	příprava	k1gFnSc4
mouky	mouka	k1gFnSc2
z	z	k7c2
kukuřice	kukuřice	k1gFnSc2
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
celé	celý	k2eAgNnSc1d1
zrno	zrno	k1gNnSc1
nejprve	nejprve	k6eAd1
namočí	namočit	k5eAaPmIp3nS
a	a	k8xC
potom	potom	k6eAd1
vaří	vařit	k5eAaImIp3nP
v	v	k7c6
alkalické	alkalický	k2eAgFnSc6d1
směsi	směs	k1gFnSc6
většinou	většinou	k6eAd1
ve	v	k7c6
vápenaté	vápenatý	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
a	a	k8xC
až	až	k9
poté	poté	k6eAd1
se	se	k3xPyFc4
oloupe	oloupat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
připravené	připravený	k2eAgNnSc1d1
zrno	zrno	k1gNnSc1
se	se	k3xPyFc4
následně	následně	k6eAd1
vymyje	vymýt	k5eAaPmIp3nS
a	a	k8xC
později	pozdě	k6eAd2
umele	umlít	k5eAaPmIp3nS
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
vznikne	vzniknout	k5eAaPmIp3nS
masa	maso	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
se	se	k3xPyFc4
potom	potom	k6eAd1
nechá	nechat	k5eAaPmIp3nS
usušit	usušit	k5eAaPmF
a	a	k8xC
dále	daleko	k6eAd2
používá	používat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinak	jinak	k6eAd1
je	být	k5eAaImIp3nS
mexická	mexický	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
je	být	k5eAaImIp3nS
charakteristická	charakteristický	k2eAgFnSc1d1
regionální	regionální	k2eAgFnSc7d1
růzností	různost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
podstatě	podstata	k1gFnSc6
každý	každý	k3xTgInSc4
region	region	k1gInSc4
Mexika	Mexiko	k1gNnSc2
má	mít	k5eAaImIp3nS
svá	svůj	k3xOyFgNnPc4
typická	typický	k2eAgNnPc4d1
jídla	jídlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
region	region	k1gInSc1
Oaxaca	Oaxacum	k1gNnSc2
je	být	k5eAaImIp3nS
typický	typický	k2eAgInSc1d1
přípravou	příprava	k1gFnSc7
omáček	omáčka	k1gFnPc2
mole	mol	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polévka	polévka	k1gFnSc1
pozole	pozole	k1gFnSc1
charakterizuje	charakterizovat	k5eAaBmIp3nS
oblasti	oblast	k1gFnPc4
Jalisca	Jaliscum	k1gNnSc2
nebo	nebo	k8xC
Sinaloi	Sinaloi	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Carnitas	Carnitas	k1gInSc1
<g/>
,	,	kIx,
druh	druh	k1gInSc1
dušeného	dušený	k2eAgNnSc2d1
nebo	nebo	k8xC
pečeného	pečený	k2eAgNnSc2d1
vepřového	vepřový	k2eAgNnSc2d1
masa	maso	k1gNnSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
typické	typický	k2eAgInPc1d1
pro	pro	k7c4
střední	střední	k2eAgNnSc4d1
Mexiko	Mexiko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oblasti	oblast	k1gFnSc6
Coahuila	Coahuil	k1gMnSc2
je	být	k5eAaImIp3nS
populární	populární	k2eAgNnSc1d1
připravovat	připravovat	k5eAaImF
různá	různý	k2eAgNnPc4d1
jídla	jídlo	k1gNnPc4
z	z	k7c2
kozího	kozí	k2eAgNnSc2d1
masa	maso	k1gNnSc2
(	(	kIx(
<g/>
cabritas	cabritas	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
nachos	nachos	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Sonoře	sonora	k1gFnSc6
a	a	k8xC
Chihuahua	Chihuahua	k1gFnSc1
jde	jít	k5eAaImIp3nS
o	o	k7c4
typickou	typický	k2eAgFnSc4d1
polévku	polévka	k1gFnSc4
zvanou	zvaný	k2eAgFnSc4d1
menudo	menudo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
2010	#num#	k4
byla	být	k5eAaImAgFnS
tradiční	tradiční	k2eAgFnSc1d1
mexická	mexický	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
zařazena	zařadit	k5eAaPmNgFnS
na	na	k7c4
seznam	seznam	k1gInSc4
nehmotných	hmotný	k2eNgFnPc2d1
památek	památka	k1gFnPc2
Světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc3
UNESCO	Unesco	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sport	sport	k1gInSc1
</s>
<s>
Rafael	Rafael	k1gMnSc1
Márquez	Márquez	k1gMnSc1
v	v	k7c6
dresu	dres	k1gInSc6
národního	národní	k2eAgInSc2d1
týmu	tým	k1gInSc2
</s>
<s>
Zahajovací	zahajovací	k2eAgInSc1d1
ceremoniál	ceremoniál	k1gInSc1
OH	OH	kA
1968	#num#	k4
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
vybojovalo	vybojovat	k5eAaPmAgNnS
třináct	třináct	k4xCc4
zlatých	zlatá	k1gFnPc2
olympijských	olympijský	k2eAgFnPc2d1
medaili	medaile	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvě	dva	k4xCgFnPc1
má	mít	k5eAaImIp3nS
jezdec	jezdec	k1gInSc4
na	na	k7c6
koni	kůň	k1gMnSc6
Humberto	Humberta	k1gFnSc5
Mariles	Mariles	k1gMnSc1
(	(	kIx(
<g/>
1948	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
po	po	k7c6
jedné	jeden	k4xCgFnSc3
pak	pak	k9
skokan	skokan	k1gMnSc1
do	do	k7c2
vody	voda	k1gFnSc2
Joaquín	Joaquín	k1gMnSc1
Capilla	Capilla	k1gMnSc1
(	(	kIx(
<g/>
1956	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
boxeři	boxer	k1gMnPc1
Antonio	Antonio	k1gMnSc1
Roldán	Roldán	k2eAgMnSc1d1
a	a	k8xC
Ricardo	Ricardo	k1gNnSc1
Delgado	Delgada	k1gFnSc5
(	(	kIx(
<g/>
oba	dva	k4xCgMnPc1
1968	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
plavec	plavec	k1gMnSc1
Felipe	Felip	k1gInSc5
Muñ	Muñ	k1gMnSc1
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
<g/>
,	,	kIx,
chodci	chodec	k1gMnPc1
Daniel	Daniel	k1gMnSc1
Bautista	Bautista	k1gMnSc1
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Ernesto	Ernesta	k1gMnSc5
Canto	canto	k1gNnSc1
a	a	k8xC
Raúl	Raúl	k1gMnSc1
González	González	k1gMnSc1
(	(	kIx(
<g/>
oba	dva	k4xCgMnPc1
1984	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vzpěračka	vzpěračka	k1gFnSc1
Soraya	Soraya	k1gFnSc1
Jiménezová	Jiménezová	k1gFnSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
taekwondisté	taekwondista	k1gMnPc1
Guillermo	Guillerma	k1gFnSc5
Pérez	Pérez	k1gInSc1
a	a	k8xC
María	Marí	k2eAgFnSc1d1
Espinozová	Espinozová	k1gFnSc1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
oba	dva	k4xCgInPc4
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
Zatím	zatím	k6eAd1
poslední	poslední	k2eAgNnSc4d1
zlato	zlato	k1gNnSc4
vybojoval	vybojovat	k5eAaPmAgInS
fotbalový	fotbalový	k2eAgInSc1d1
tým	tým	k1gInSc1
na	na	k7c6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
v	v	k7c6
Londýně	Londýn	k1gInSc6
roku	rok	k1gInSc2
2012	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
Mexičtí	mexický	k2eAgMnPc1d1
sportovci	sportovec	k1gMnPc1
reprezentují	reprezentovat	k5eAaImIp3nP
svoji	svůj	k3xOyFgFnSc4
zemi	zem	k1gFnSc4
nejen	nejen	k6eAd1
na	na	k7c6
Olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
na	na	k7c6
regionálních	regionální	k2eAgFnPc6d1
Středoamerických	středoamerický	k2eAgFnPc6d1
a	a	k8xC
karibských	karibský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
či	či	k8xC
Panamerických	panamerický	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Mexická	mexický	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
se	se	k3xPyFc4
účastní	účastnit	k5eAaImIp3nS
Zlatého	zlatý	k2eAgInSc2d1
poháru	pohár	k1gInSc2
CONCACAF	CONCACAF	kA
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
desetkrát	desetkrát	k6eAd1
vyhrála	vyhrát	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1993	#num#	k4
hraje	hrát	k5eAaImIp3nS
občas	občas	k6eAd1
též	též	k9
soutěž	soutěž	k1gFnSc1
Copa	Copa	k1gFnSc1
América	América	k1gFnSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
mistrovství	mistrovství	k1gNnSc1
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
(	(	kIx(
<g/>
dvakrát	dvakrát	k6eAd1
byla	být	k5eAaImAgFnS
ve	v	k7c6
finále	finále	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
světovém	světový	k2eAgInSc6d1
šampionátu	šampionát	k1gInSc6
se	se	k3xPyFc4
Mexičané	Mexičan	k1gMnPc1
dostali	dostat	k5eAaPmAgMnP
nejdále	daleko	k6eAd3
do	do	k7c2
čtvrtfinále	čtvrtfinále	k1gNnSc2
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
vyhráli	vyhrát	k5eAaPmAgMnP
Konfederační	konfederační	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
evropských	evropský	k2eAgFnPc6d1
soutěžích	soutěž	k1gFnPc6
se	se	k3xPyFc4
prosadili	prosadit	k5eAaPmAgMnP
hráči	hráč	k1gMnPc1
jako	jako	k8xS,k8xC
Javier	Javier	k1gMnSc1
Hernández	Hernández	k1gMnSc1
<g/>
,	,	kIx,
Rafael	Rafael	k1gMnSc1
Márquez	Márquez	k1gMnSc1
či	či	k8xC
Hugo	Hugo	k1gMnSc1
Sánchez	Sánchez	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
některých	některý	k3yIgInPc6
regionech	region	k1gInPc6
Mexika	Mexiko	k1gNnSc2
je	být	k5eAaImIp3nS
populární	populární	k2eAgInSc1d1
baseball	baseball	k1gInSc1
<g/>
,	,	kIx,
někteří	některý	k3yIgMnPc1
Mexičané	Mexičan	k1gMnPc1
se	se	k3xPyFc4
prosadili	prosadit	k5eAaPmAgMnP
i	i	k9
v	v	k7c6
americké	americký	k2eAgFnSc6d1
Major	major	k1gMnSc1
League	Leagu	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
Fernando	Fernanda	k1gFnSc5
Valenzuela	Valenzuel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
americké	americký	k2eAgFnSc6d1
National	National	k1gFnSc6
Football	Football	k1gMnSc1
League	League	k1gNnSc2
prosadil	prosadit	k5eAaPmAgMnS
mexický	mexický	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
amerického	americký	k2eAgInSc2d1
fotbalu	fotbal	k1gInSc2
Raul	Raul	k1gMnSc1
Allegre	Allegr	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Basketbalista	basketbalista	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Raga	Raga	k1gMnSc1
<g/>
,	,	kIx,
zvaný	zvaný	k2eAgInSc1d1
„	„	k?
<g/>
Létající	létající	k2eAgMnSc1d1
Mexičan	Mexičan	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
vyhrál	vyhrát	k5eAaPmAgMnS
s	s	k7c7
italským	italský	k2eAgInSc7d1
klubem	klub	k1gInSc7
Pallacanestro	Pallacanestro	k1gNnSc1
Varese	Varese	k1gFnSc2
třikrát	třikrát	k6eAd1
Euroligu	Eurolig	k1gInSc2
a	a	k8xC
jako	jako	k8xC,k8xS
jediný	jediný	k2eAgMnSc1d1
Mexičan	Mexičan	k1gMnSc1
byl	být	k5eAaImAgMnS
uveden	uvést	k5eAaPmNgMnS
do	do	k7c2
Síně	síň	k1gFnSc2
slávy	sláva	k1gFnSc2
Mezinárodní	mezinárodní	k2eAgFnSc2d1
basketbalové	basketbalový	k2eAgFnSc2d1
federace	federace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
Tenista	tenista	k1gMnSc1
Rafael	Rafael	k1gMnSc1
Osuna	Osun	k1gMnSc4
byl	být	k5eAaImAgInS
světovou	světový	k2eAgFnSc7d1
jedničkou	jednička	k1gFnSc7
a	a	k8xC
v	v	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
roce	rok	k1gInSc6
1963	#num#	k4
vyhrál	vyhrát	k5eAaPmAgMnS
Wimbledon	Wimbledon	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
Závodní	závodní	k2eAgInSc1d1
automobilový	automobilový	k2eAgInSc1d1
jezdec	jezdec	k1gInSc1
Pedro	Pedro	k1gNnSc1
Rodríguez	Rodríguez	k1gInSc1
de	de	k?
la	la	k1gNnSc2
Vega	Vega	k1gFnSc1
vyhrál	vyhrát	k5eAaPmAgInS
vytrvalostní	vytrvalostní	k2eAgInSc1d1
závod	závod	k1gInSc1
24	#num#	k4
hodin	hodina	k1gFnPc2
Le	Le	k1gFnPc2
Mans	Mans	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
Lorena	Loren	k2eAgFnSc1d1
Ochoaová	Ochoaová	k1gFnSc1
byla	být	k5eAaImAgFnS
golfovou	golfový	k2eAgFnSc7d1
světovou	světový	k2eAgFnSc7d1
jedničkou	jednička	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
Ana	Ana	k1gFnSc1
Guevaraová	Guevaraová	k1gFnSc1
je	být	k5eAaImIp3nS
mistryní	mistryně	k1gFnSc7
světa	svět	k1gInSc2
v	v	k7c6
atletice	atletika	k1gFnSc6
v	v	k7c6
běhu	běh	k1gInSc6
na	na	k7c4
400	#num#	k4
metrů	metr	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
se	se	k3xPyFc4
v	v	k7c6
minulosti	minulost	k1gFnSc6
již	již	k6eAd1
několikrát	několikrát	k6eAd1
stalo	stát	k5eAaPmAgNnS
hostitelskou	hostitelský	k2eAgFnSc7d1
zemí	zem	k1gFnSc7
mezinárodních	mezinárodní	k2eAgFnPc2d1
sportovních	sportovní	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konaly	konat	k5eAaImAgInP
se	se	k3xPyFc4
zde	zde	k6eAd1
letní	letní	k2eAgFnSc2d1
olympijské	olympijský	k2eAgFnSc2d1
hry	hra	k1gFnSc2
1968	#num#	k4
<g/>
,	,	kIx,
mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
1970	#num#	k4
a	a	k8xC
mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdejší	zdejší	k2eAgInPc1d1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Mexika	Mexiko	k1gNnSc2
byla	být	k5eAaImAgFnS
po	po	k7c4
16	#num#	k4
let	léto	k1gNnPc2
součástí	součást	k1gFnSc7
mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
Formule	formule	k1gFnSc2
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
ročníku	ročník	k1gInSc2
2004	#num#	k4
je	být	k5eAaImIp3nS
zdejší	zdejší	k2eAgFnSc1d1
Mexická	mexický	k2eAgFnSc1d1
rallye	rallye	k1gFnSc1
součástí	součást	k1gFnSc7
Mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
v	v	k7c6
rallye	rallye	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Tradičním	tradiční	k2eAgInSc7d1
národním	národní	k2eAgInSc7d1
sportem	sport	k1gInSc7
<g/>
,	,	kIx,
s	s	k7c7
kořeny	kořen	k1gInPc7
v	v	k7c4
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
charreada	charreada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
blízko	blízko	k6eAd1
k	k	k7c3
americkému	americký	k2eAgNnSc3d1
rodeu	rodeo	k1gNnSc3
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
z	z	k7c2
ní	on	k3xPp3gFnSc2
zřejmě	zřejmě	k6eAd1
pochází	pocházet	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
Často	často	k6eAd1
je	být	k5eAaImIp3nS
provozována	provozován	k2eAgFnSc1d1
společně	společně	k6eAd1
s	s	k7c7
býčími	býčí	k2eAgInPc7d1
zápasy	zápas	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
zemi	zem	k1gFnSc6
také	také	k9
populární	populární	k2eAgFnSc1d1
<g/>
,	,	kIx,
vzhledem	vzhledem	k7c3
ke	k	k7c3
španělskému	španělský	k2eAgNnSc3d1
koloniálnímu	koloniální	k2eAgNnSc3d1
dědictví	dědictví	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobným	podobný	k2eAgNnSc7d1
dědictvím	dědictví	k1gNnSc7
je	být	k5eAaImIp3nS
baskická	baskický	k2eAgFnSc1d1
pelota	pelota	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
původní	původní	k2eAgMnPc1d1
indiánští	indiánský	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
provozovali	provozovat	k5eAaImAgMnP
míčovou	míčový	k2eAgFnSc4d1
hru	hra	k1gFnSc4
zvanou	zvaný	k2eAgFnSc4d1
tlachtli	tlachtle	k1gFnSc4
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc4
pravidla	pravidlo	k1gNnPc1
se	se	k3xPyFc4
zcela	zcela	k6eAd1
nedochovala	dochovat	k5eNaPmAgFnS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nicméně	nicméně	k8xC
považována	považován	k2eAgFnSc1d1
za	za	k7c4
nejstarší	starý	k2eAgFnSc4d3
míčovou	míčový	k2eAgFnSc4d1
hru	hra	k1gFnSc4
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
Navazuje	navazovat	k5eAaImIp3nS
na	na	k7c6
ní	on	k3xPp3gFnSc6
patrně	patrně	k6eAd1
například	například	k6eAd1
hra	hra	k1gFnSc1
ulama	ulama	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
hrají	hrát	k5eAaImIp3nP
dodnes	dodnes	k6eAd1
některé	některý	k3yIgFnPc1
indiánské	indiánský	k2eAgFnPc1d1
komunity	komunita	k1gFnPc1
v	v	k7c6
mexickém	mexický	k2eAgInSc6d1
státě	stát	k1gInSc6
Sinaloa	Sinalo	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
méně	málo	k6eAd2
známých	známý	k2eAgInPc2d1
sportů	sport	k1gInPc2
je	být	k5eAaImIp3nS
v	v	k7c6
Mexiku	Mexiko	k1gNnSc6
populární	populární	k2eAgMnSc1d1
též	též	k9
pólo	pólo	k1gNnSc1
(	(	kIx(
<g/>
Carlos	Carlos	k1gInSc1
Gracida	Gracid	k1gMnSc2
patří	patřit	k5eAaImIp3nS
k	k	k7c3
nejlepším	dobrý	k2eAgMnPc3d3
pólistům	pólista	k1gMnPc3
historie	historie	k1gFnSc2
<g/>
)	)	kIx)
či	či	k8xC
raketbal	raketbal	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Věda	věda	k1gFnSc1
a	a	k8xC
školství	školství	k1gNnSc1
</s>
<s>
Mario	Mario	k1gMnSc1
Molina	molino	k1gNnSc2
<g/>
,	,	kIx,
nositel	nositel	k1gMnSc1
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
</s>
<s>
V	v	k7c6
Mexiku	Mexiko	k1gNnSc6
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
nositel	nositel	k1gMnSc1
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
za	za	k7c4
chemii	chemie	k1gFnSc4
Mario	Mario	k1gMnSc1
J.	J.	kA
Molina	molino	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostal	dostat	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
roku	rok	k1gInSc2
1995	#num#	k4
za	za	k7c4
na	na	k7c4
odhalení	odhalení	k1gNnSc4
příčin	příčina	k1gFnPc2
vzniku	vznik	k1gInSc2
ozonové	ozonový	k2eAgFnSc2d1
díry	díra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Programátor	programátor	k1gMnSc1
Miguel	Miguel	k1gMnSc1
de	de	k?
Icaza	Icaz	k1gMnSc4
stál	stát	k5eAaImAgInS
u	u	k7c2
zrodu	zrod	k1gInSc2
projektů	projekt	k1gInPc2
GNOME	GNOME	kA
a	a	k8xC
Mono	mono	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Mexiku	Mexiko	k1gNnSc6
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
také	také	k9
izraelský	izraelský	k2eAgMnSc1d1
teoretický	teoretický	k2eAgMnSc1d1
fyzik	fyzik	k1gMnSc1
Ja	Ja	k?
<g/>
'	'	kIx"
<g/>
akov	akov	k1gMnSc1
Bekenstein	Bekenstein	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významným	významný	k2eAgMnSc7d1
odborníkem	odborník	k1gMnSc7
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
je	být	k5eAaImIp3nS
i	i	k9
Miguel	Miguel	k1gMnSc1
Alcubierre	Alcubierr	k1gInSc5
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
navrhl	navrhnout	k5eAaPmAgInS
Alcubierrův	Alcubierrův	k2eAgInSc1d1
pohon	pohon	k1gInSc1
(	(	kIx(
<g/>
Warp	Warp	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tedy	tedy	k8xC
hypotetický	hypotetický	k2eAgInSc1d1
pohon	pohon	k1gInSc1
kosmických	kosmický	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
by	by	kYmCp3nS
jim	on	k3xPp3gMnPc3
měl	mít	k5eAaImAgMnS
být	být	k5eAaImF
schopen	schopen	k2eAgMnSc1d1
udělit	udělit	k5eAaPmF
rychlost	rychlost	k1gFnSc4
vyšší	vysoký	k2eAgFnSc1d2
než	než	k8xS
je	být	k5eAaImIp3nS
rychlost	rychlost	k1gFnSc4
světla	světlo	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
za	za	k7c2
pomoci	pomoc	k1gFnSc2
ohýbání	ohýbání	k1gNnSc2
prostoru	prostor	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
gravitací	gravitace	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
prvním	první	k4xOgMnPc3
významným	významný	k2eAgMnPc3d1
mexickým	mexický	k2eAgMnPc3d1
vědcům	vědec	k1gMnPc3
patřil	patřit	k5eAaImAgInS
Andrés	Andrés	k1gInSc1
Manuel	Manuel	k1gMnSc1
del	del	k?
Río	Río	k1gFnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
roku	rok	k1gInSc3
1801	#num#	k4
objevil	objevit	k5eAaPmAgInS
vanad	vanad	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
Významným	významný	k2eAgMnSc7d1
chemikem	chemik	k1gMnSc7
byl	být	k5eAaImAgMnS
i	i	k9
Henry	Henry	k1gMnSc1
Eyring	Eyring	k1gInSc1
či	či	k8xC
Luis	Luisa	k1gFnPc2
Ernesto	Ernesta	k1gMnSc5
Miramontes	Miramontes	k1gInSc4
<g/>
,	,	kIx,
vynálezce	vynálezce	k1gMnSc4
jedné	jeden	k4xCgFnSc2
z	z	k7c2
prvních	první	k4xOgFnPc2
antikoncepčních	antikoncepční	k2eAgFnPc2d1
pilulek	pilulka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Školní	školní	k2eAgNnSc1d1
vzdělání	vzdělání	k1gNnSc1
se	se	k3xPyFc4
uskutečňuje	uskutečňovat	k5eAaImIp3nS
v	v	k7c6
základním	základní	k2eAgMnSc6d1
<g/>
,	,	kIx,
středním	střední	k2eAgInSc6d1
(	(	kIx(
<g/>
nižším	nízký	k2eAgInSc6d2
a	a	k8xC
vyšším	vysoký	k2eAgMnSc6d2
<g/>
)	)	kIx)
a	a	k8xC
vysokém	vysoký	k2eAgInSc6d1
stupni	stupeň	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc1d1
šestileté	šestiletý	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
je	být	k5eAaImIp3nS
povinné	povinný	k2eAgNnSc1d1
a	a	k8xC
bezplatné	bezplatný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nižší	nízký	k2eAgFnSc1d2
střední	střední	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
představují	představovat	k5eAaImIp3nP
tříleté	tříletý	k2eAgFnPc4d1
školy	škola	k1gFnPc4
2	#num#	k4
<g/>
.	.	kIx.
stupně	stupeň	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
ukončení	ukončení	k1gNnSc6
tohoto	tento	k3xDgNnSc2
vzdělání	vzdělání	k1gNnSc2
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
získat	získat	k5eAaPmF
vyšší	vysoký	k2eAgNnSc4d2
střední	střední	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
na	na	k7c6
tříletých	tříletý	k2eAgFnPc6d1
nebo	nebo	k8xC
čtyřletých	čtyřletý	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgFnSc7d3
formou	forma	k1gFnSc7
vzdělání	vzdělání	k1gNnSc2
jsou	být	k5eAaImIp3nP
vysoké	vysoký	k2eAgFnPc1d1
školy	škola	k1gFnPc1
univerzitního	univerzitní	k2eAgInSc2d1
nebo	nebo	k8xC
technického	technický	k2eAgInSc2d1
směru	směr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
patří	patřit	k5eAaImIp3nS
k	k	k7c3
zemím	zem	k1gFnPc3
s	s	k7c7
vysokou	vysoký	k2eAgFnSc7d1
gramotností	gramotnost	k1gFnSc7
a	a	k8xC
vyspělým	vyspělý	k2eAgInSc7d1
školským	školský	k2eAgInSc7d1
systémem	systém	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mexické	mexický	k2eAgNnSc1d1
školství	školství	k1gNnSc1
má	mít	k5eAaImIp3nS
širokou	široký	k2eAgFnSc4d1
síť	síť	k1gFnSc4
základních	základní	k2eAgFnPc2d1
škol	škola	k1gFnPc2
a	a	k8xC
světově	světově	k6eAd1
uznávané	uznávaný	k2eAgNnSc4d1
vysoké	vysoký	k2eAgNnSc4d1
školství	školství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Národní	národní	k2eAgFnSc6d1
autonomní	autonomní	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
(	(	kIx(
<g/>
UNAM	UNAM	kA
<g/>
)	)	kIx)
studuje	studovat	k5eAaImIp3nS
přes	přes	k7c4
400	#num#	k4
000	#num#	k4
studentů	student	k1gMnPc2
<g/>
,	,	kIx,
po	po	k7c6
celých	celý	k2eAgInPc6d1
spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Chybí	chybit	k5eAaPmIp3nP,k5eAaImIp3nP
učitelé	učitel	k1gMnPc1
se	s	k7c7
znalostí	znalost	k1gFnSc7
indiánských	indiánský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
2	#num#	k4
milióny	milión	k4xCgInPc1
Indiánů	Indián	k1gMnPc2
ovládá	ovládat	k5eAaImIp3nS
jen	jen	k9
svoji	svůj	k3xOyFgFnSc4
rodnou	rodný	k2eAgFnSc4d1
řeč	řeč	k1gFnSc4
<g/>
,	,	kIx,
další	další	k2eAgFnSc4d1
2	#num#	k4
milióny	milión	k4xCgInPc7
k	k	k7c3
tomu	ten	k3xDgNnSc3
hovoří	hovořit	k5eAaImIp3nS
částečně	částečně	k6eAd1
španělsky	španělsky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šanci	šance	k1gFnSc4
studovat	studovat	k5eAaImF
na	na	k7c6
nějaké	nějaký	k3yIgFnSc6
z	z	k7c2
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
v	v	k7c6
zemi	zem	k1gFnSc6
má	mít	k5eAaImIp3nS
ročně	ročně	k6eAd1
asi	asi	k9
1,5	1,5	k4
miliónů	milión	k4xCgInPc2
mladých	mladý	k2eAgMnPc2d1
Mexičanů	Mexičan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Světová	světový	k2eAgFnSc1d1
banka	banka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
GDP	GDP	kA
per	pero	k1gNnPc2
capita	capitum	k1gNnSc2
<g/>
,	,	kIx,
PPP	PPP	kA
(	(	kIx(
<g/>
current	current	k1gMnSc1
international	internationat	k5eAaImAgMnS,k5eAaPmAgMnS
$	$	kIx~
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Teotihuacán	Teotihuacán	k2eAgInSc1d1
-	-	kIx~
starověké	starověký	k2eAgNnSc1d1
posvátné	posvátný	k2eAgNnSc1d1
místo	místo	k1gNnSc1
-	-	kIx~
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.novinky.cz	www.novinky.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
POLIŠENSKÝ	POLIŠENSKÝ	kA
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
Latinské	latinský	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Svoboda	svoboda	k1gFnSc1
829	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gFnSc1
<g/>
:	:	kIx,
VWeaGAAACAAJ	VWeaGAAACAAJ	k1gFnSc1
<g/>
.	.	kIx.
↑	↑	k?
LNĚNIČKA	LNĚNIČKA	kA
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mexiko	Mexiko	k1gNnSc1
<g/>
:	:	kIx,
Tajemný	tajemný	k2eAgInSc1d1
odkaz	odkaz	k1gInSc1
mayské	mayský	k2eAgFnSc2d1
civilizace	civilizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
HedvabnaStezka	HedvabnaStezka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-04-23	2014-04-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Mexický	mexický	k2eAgInSc4d1
sedmý	sedmý	k4xOgInSc4
div	div	k1gInSc4
světa	svět	k1gInSc2
je	být	k5eAaImIp3nS
jako	jako	k9
matrjoška	matrjoška	k1gFnSc1
<g/>
,	,	kIx,
obsahuje	obsahovat	k5eAaImIp3nS
další	další	k2eAgInPc4d1
pyramidy	pyramid	k1gInPc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-11-17	2016-11-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
DVOŘÁKOVÁ	Dvořáková	k1gFnSc1
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejkrutější	krutý	k2eAgInPc4d3
aztécké	aztécký	k2eAgInPc4d1
rituály	rituál	k1gInPc4
<g/>
:	:	kIx,
Umíralo	umírat	k5eAaImAgNnS
při	pře	k1gFnSc4
nich	on	k3xPp3gMnPc2
40	#num#	k4
lidí	člověk	k1gMnPc2
denně	denně	k6eAd1
<g/>
!	!	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Epochaplus	Epochaplus	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Před	před	k7c7
500	#num#	k4
lety	let	k1gInPc7
dosáhl	dosáhnout	k5eAaPmAgInS
Cortés	Cortés	k1gInSc1
břehů	břeh	k1gInPc2
říše	říš	k1gFnSc2
Aztéků	Azték	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobýval	dobývat	k5eAaImAgMnS
ji	on	k3xPp3gFnSc4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
,	,	kIx,
domorodcům	domorodec	k1gMnPc3
‚	‚	k?
<g/>
přivezl	přivézt	k5eAaPmAgMnS
<g/>
‘	‘	k?
neštovice	neštovice	k1gFnPc1
|	|	kIx~
Zajímavosti	zajímavost	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-03-04	2019-03-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KRUPKA	Krupka	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k8xC,k8xS
zanikla	zaniknout	k5eAaPmAgFnS
starověká	starověký	k2eAgFnSc1d1
mayská	mayský	k2eAgFnSc1d1
civilizace	civilizace	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Vědci	vědec	k1gMnPc1
přišli	přijít	k5eAaPmAgMnP
s	s	k7c7
překvapivou	překvapivý	k2eAgFnSc7d1
teorií	teorie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Encomienda	Encomienda	k1gFnSc1
<g/>
,	,	kIx,
Hacienda	hacienda	k1gFnSc1
<g/>
,	,	kIx,
Latifundio	Latifundio	k1gNnSc1
<g/>
.	.	kIx.
is	is	k?
<g/>
.	.	kIx.
<g/>
mendelu	mendel	k1gInSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SCHMAL	SCHMAL	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
P.	P.	kA
Who	Who	k1gMnSc1
Were	Wer	k1gFnSc2
the	the	k?
Chichimecas	Chichimecas	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
IndigenousMexico	IndigenousMexico	k1gNnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
,	,	kIx,
2020-05-18	2020-05-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Mexiko	Mexiko	k1gNnSc1
slaví	slavit	k5eAaImIp3nS
dvousté	dvoustý	k2eAgNnSc1d1
výročí	výročí	k1gNnSc1
nezávislosti	nezávislost	k1gFnSc2
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ANNA	Anna	k1gFnSc1
<g/>
,	,	kIx,
Timothy	Timotha	k1gMnSc2
E.	E.	kA
The	The	k1gFnSc2
Rule	rula	k1gFnSc3
of	of	k?
Agustin	Agustin	k1gInSc1
de	de	k?
Iturbide	Iturbid	k1gInSc5
<g/>
:	:	kIx,
A	a	k9
Reappraisal	Reappraisal	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gMnSc1
of	of	k?
Latin	Latin	k1gMnSc1
American	American	k1gMnSc1
Studies	Studies	k1gMnSc1
<g/>
.	.	kIx.
1985	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
17	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
79	#num#	k4
<g/>
–	–	k?
<g/>
110	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
22	#num#	k4
<g/>
-	-	kIx~
<g/>
216	#num#	k4
<g/>
X.	X.	kA
↑	↑	k?
Před	před	k7c7
170	#num#	k4
lety	léto	k1gNnPc7
americký	americký	k2eAgInSc4d1
Kongres	kongres	k1gInSc4
vyhlásil	vyhlásit	k5eAaPmAgMnS
válku	válka	k1gFnSc4
Mexiku	Mexiko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reflex	reflex	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Mexické	mexický	k2eAgNnSc4d1
dobrodružství	dobrodružství	k1gNnSc4
Maxmiliána	Maxmilián	k1gMnSc2
Habsburského	habsburský	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Maxmilián	Maxmilián	k1gMnSc1
I.	I.	kA
Mexický	mexický	k2eAgMnSc1d1
<g/>
:	:	kIx,
císař	císař	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
skončil	skončit	k5eAaPmAgInS
před	před	k7c7
popravčí	popravčí	k2eAgFnSc7d1
četou	četa	k1gFnSc7
<g/>
.	.	kIx.
www.securitymagazin.cz	www.securitymagazin.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BRYAN	BRYAN	kA
<g/>
,	,	kIx,
Anthony	Anthona	k1gFnSc2
T.	T.	kA
Political	Political	k1gMnSc1
Power	Power	k1gMnSc1
In	In	k1gMnSc1
Porfirio	Porfirio	k1gMnSc1
Diaz	Diaz	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Mexico	Mexico	k6eAd1
<g/>
:	:	kIx,
A	a	k9
Review	Review	k1gFnSc1
and	and	k?
Commentary	Commentara	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Historian	Historian	k1gMnSc1
<g/>
.	.	kIx.
1976	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
38	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
648	#num#	k4
<g/>
–	–	k?
<g/>
668	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
18	#num#	k4
<g/>
-	-	kIx~
<g/>
2370	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Sto	sto	k4xCgNnSc4
let	léto	k1gNnPc2
od	od	k7c2
mexické	mexický	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plus	plus	k1gInSc1
-	-	kIx~
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-12-01	2010-12-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
QUIRK	QUIRK	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
E.	E.	kA
<g/>
;	;	kIx,
QUIRK	QUIRK	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
E.	E.	kA
An	An	k1gMnSc1
Affair	Affair	k1gMnSc1
of	of	k?
Honor	Honor	k1gMnSc1
<g/>
:	:	kIx,
Woodrow	Woodrow	k1gMnSc1
Wilson	Wilson	k1gMnSc1
and	and	k?
the	the	k?
Occupation	Occupation	k1gInSc1
of	of	k?
Veracruz	Veracruz	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
W.	W.	kA
W.	W.	kA
Norton	Norton	k1gInSc1
&	&	k?
Company	Compana	k1gFnSc2
196	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
393	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
390	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gFnSc1
<g/>
:	:	kIx,
2	#num#	k4
<g/>
AHjuuV	AHjuuV	k1gFnSc1
<g/>
6	#num#	k4
<g/>
nYEC	nYEC	k?
<g/>
.	.	kIx.
↑	↑	k?
TEITELBAUM	TEITELBAUM	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
M.	M.	kA
Woodrow	Woodrow	k1gMnSc1
Wilson	Wilson	k1gMnSc1
and	and	k?
<g />
.	.	kIx.
</s>
<s hack="1">
the	the	k?
Mexican	Mexican	k1gInSc1
Revolution	Revolution	k1gInSc1
<g/>
,	,	kIx,
1913	#num#	k4
<g/>
-	-	kIx~
<g/>
1916	#num#	k4
<g/>
:	:	kIx,
A	a	k8xC
History	Histor	k1gInPc1
of	of	k?
United	United	k1gInSc1
States-Mexican	States-Mexican	k1gMnSc1
Relations	Relationsa	k1gFnPc2
from	from	k1gMnSc1
the	the	k?
Murder	Murder	k1gInSc1
of	of	k?
Madero	Madero	k1gNnSc1
Until	Until	k1gMnSc1
Villa	Villa	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Provocation	Provocation	k1gInSc1
Across	Across	k1gInSc1
the	the	k?
Border	Border	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Exposition	Exposition	k1gInSc1
Press	Press	k1gInSc1
454	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gFnSc1
<g/>
:	:	kIx,
C_J	C_J	k1gFnSc1
<g/>
1	#num#	k4
<g/>
AAAAMAAJ	AAAAMAAJ	kA
<g/>
.	.	kIx.
↑	↑	k?
Cristero	Cristero	k1gNnSc4
Rebellion	Rebellion	k?
|	|	kIx~
Encyclopedia	Encyclopedium	k1gNnSc2
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
www.encyclopedia.com	www.encyclopedia.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BECKER	BECKER	kA
<g/>
,	,	kIx,
Marjorie	Marjorie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Black	Black	k1gMnSc1
and	and	k?
White	Whit	k1gInSc5
and	and	k?
Color	Color	k1gInSc1
<g/>
:	:	kIx,
Cardenismo	Cardenisma	k1gFnSc5
and	and	k?
the	the	k?
Search	Search	k1gMnSc1
for	forum	k1gNnPc2
a	a	k8xC
Campesino	Campesin	k2eAgNnSc1d1
Ideology	ideolog	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Comparative	Comparativ	k1gInSc5
Studies	Studies	k1gInSc4
in	in	k?
Society	societa	k1gFnSc2
and	and	k?
History	Histor	k1gMnPc7
<g/>
.	.	kIx.
1987	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
29	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
453	#num#	k4
<g/>
–	–	k?
<g/>
465	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
4175	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
O	O	kA
<g/>
'	'	kIx"
<g/>
BRIEN	BRIEN	kA
<g/>
,	,	kIx,
Elizabeth	Elizabeth	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Revolutionary	Revolutionar	k1gMnPc4
Ideology	ideolog	k1gMnPc4
and	and	k?
Political	Political	k1gFnSc1
Destiny	Destina	k1gFnSc2
in	in	k?
Mexico	Mexico	k1gMnSc1
<g/>
,	,	kIx,
1928	#num#	k4
<g/>
–	–	k?
<g/>
1934	#num#	k4
<g/>
:	:	kIx,
Lázaro	Lázara	k1gFnSc5
Cárdenas	Cárdenas	k1gInSc1
and	and	k?
Adalberto	Adalberta	k1gFnSc5
Tejeda	Tejed	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hispanic	Hispanice	k1gFnPc2
American	American	k1gMnSc1
Historical	Historical	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
98	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
763	#num#	k4
<g/>
–	–	k?
<g/>
765	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
18	#num#	k4
<g/>
-	-	kIx~
<g/>
2168	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.121	10.121	k4
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
182168	#num#	k4
<g/>
-	-	kIx~
<g/>
7160754	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CLINE	CLINE	kA
<g/>
,	,	kIx,
William	William	k1gInSc1
R.	R.	kA
Mexico	Mexico	k6eAd1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Crisis	Crisis	k1gInSc1
<g/>
,	,	kIx,
the	the	k?
World	World	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Peril	Perila	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Foreign	Foreign	k1gInSc1
Policy	Polica	k1gFnSc2
<g/>
.	.	kIx.
1982	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
49	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
107	#num#	k4
<g/>
–	–	k?
<g/>
118	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
15	#num#	k4
<g/>
-	-	kIx~
<g/>
7228	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.230	10.230	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
1148490	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
PASTOR	pastor	k1gMnSc1
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
A.	A.	kA
Post-Revolutionary	Post-Revolutionar	k1gInPc1
Mexico	Mexico	k1gNnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Salinas	Salinas	k1gMnSc1
Opening	Opening	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gMnSc1
of	of	k?
Interamerican	Interamerican	k1gMnSc1
Studies	Studies	k1gMnSc1
and	and	k?
World	World	k1gInSc1
Affairs	Affairs	k1gInSc1
<g/>
.	.	kIx.
1990	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
32	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
–	–	k?
<g/>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
22	#num#	k4
<g/>
-	-	kIx~
<g/>
1937	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.230	10.230	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
166087	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
PEČÍNKA	pečínka	k1gFnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
Guevary	Guevar	k1gInPc1
k	k	k7c3
zapatistům	zapatista	k1gMnPc3
<g/>
:	:	kIx,
Přehled	přehled	k1gInSc1
<g/>
,	,	kIx,
složení	složení	k1gNnSc1
a	a	k8xC
činnost	činnost	k1gFnSc1
gerilových	gerilový	k2eAgNnPc2d1
hnutí	hnutí	k1gNnPc2
Latinské	latinský	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Doplněk	doplněk	k1gInSc1
263	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85765	#num#	k4
<g/>
-	-	kIx~
<g/>
96	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gFnSc1
<g/>
:	:	kIx,
baGAAAAACAAJ	baGAAAAACAAJ	k?
<g/>
.	.	kIx.
↑	↑	k?
An	An	k1gMnSc1
unexpected	unexpected	k1gMnSc1
place	plac	k1gInSc6
for	forum	k1gNnPc2
lessons	lessons	k6eAd1
to	ten	k3xDgNnSc4
fight	fight	k5eAaPmF
Mexico	Mexico	k6eAd1
<g/>
’	’	k?
<g/>
s	s	k7c7
mafia	mafium	k1gNnPc1
<g/>
:	:	kIx,
Italy	Ital	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reuters	Reutersa	k1gFnPc2
<g/>
,	,	kIx,
2014-12-04	2014-12-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Nová	nový	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
o	o	k7c6
volném	volný	k2eAgInSc6d1
obchodu	obchod	k1gInSc6
mezi	mezi	k7c7
USA	USA	kA
<g/>
,	,	kIx,
Kanadou	Kanada	k1gFnSc7
a	a	k8xC
Mexikem	Mexiko	k1gNnSc7
začne	začít	k5eAaPmIp3nS
platit	platit	k5eAaImF
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
-	-	kIx~
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.novinky.cz	www.novinky.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
informace	informace	k1gFnPc1
z	z	k7c2
oficiálních	oficiální	k2eAgFnPc2d1
stránek	stránka	k1gFnPc2
Národní	národní	k2eAgFnSc2d1
komise	komise	k1gFnSc2
pro	pro	k7c4
přírodní	přírodní	k2eAgNnPc4d1
chráněná	chráněný	k2eAgNnPc4d1
území	území	k1gNnSc4
Archivováno	archivovat	k5eAaBmNgNnS
26	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
BILATERALNI	BILATERALNI	kA
VZTAHY	vztah	k1gInPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ambasáda	ambasáda	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
mexických	mexický	k2eAgInPc2d1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Krajané	krajan	k1gMnPc1
v	v	k7c6
Mexiku	Mexiko	k1gNnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ambasáda	ambasáda	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
mexických	mexický	k2eAgInPc6d1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Balanza	Balanza	k1gFnSc1
comercial	comercial	k1gMnSc1
de	de	k?
mercancías	mercancías	k1gMnSc1
de	de	k?
México	México	k1gMnSc1
-	-	kIx~
Información	Información	k1gMnSc1
revisada	revisada	k1gFnSc1
enero-marzo	enero-marza	k1gFnSc5
<g/>
,	,	kIx,
2014	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mexický	mexický	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
The	The	k1gFnSc1
world	world	k1gMnSc1
Factbook	Factbook	k1gInSc1
-	-	kIx~
Mexico	Mexico	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CIA	CIA	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
UNWTO	UNWTO	kA
Tourism	Tourism	k1gInSc1
Highlights	Highlights	k1gInSc1
<g/>
:	:	kIx,
2018	#num#	k4
Edition	Edition	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9789284419029	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.181	10.181	k4
<g/>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
9789284419029	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SECTUR	SECTUR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turismo	Turisma	k1gFnSc5
de	de	k?
internación	internación	k1gInSc1
2001	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
,	,	kIx,
Visitantes	Visitantes	k1gInSc1
internacionales	internacionales	k1gInSc1
hacia	hacia	k1gFnSc1
México	México	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Secretaría	Secretaría	k1gFnSc1
de	de	k?
Turismo	Turisma	k1gFnSc5
(	(	kIx(
<g/>
SECTUR	SECTUR	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
10	#num#	k4
June	jun	k1gMnSc5
2008	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Spanish	Spanish	k1gInSc1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
pp	pp	k?
<g/>
.	.	kIx.
5	#num#	k4
<g/>
↑	↑	k?
The	The	k1gMnSc1
Travel	Travel	k1gMnSc1
&	&	k?
Tourism	Tourism	k1gInSc1
Competitiveness	Competitiveness	k1gInSc1
Report	report	k1gInSc1
2017	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
World	World	k1gMnSc1
Economic	Economic	k1gMnSc1
Forum	forum	k1gNnSc1
<g/>
,	,	kIx,
April	April	k1gInSc1
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Robert	Robert	k1gMnSc1
McCaa	McCaa	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Missing	Missing	k1gInSc1
millions	millions	k1gInSc4
<g/>
:	:	kIx,
the	the	k?
human	human	k1gMnSc1
cost	cost	k1gMnSc1
of	of	k?
the	the	k?
Mexican	Mexican	k1gInSc1
Revolution	Revolution	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
University	universita	k1gFnSc2
of	of	k?
Minnesota	Minnesota	k1gFnSc1
Population	Population	k1gInSc1
Center	centrum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Consulta	Consulta	k1gMnSc1
de	de	k?
datos	datos	k1gMnSc1
del	del	k?
Conteo	Conteo	k1gMnSc1
2005	#num#	k4
<g/>
↑	↑	k?
FOTO	foto	k1gNnSc1
<g/>
:	:	kIx,
Podívejte	podívat	k5eAaImRp2nP,k5eAaPmRp2nP
se	se	k3xPyFc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
žijí	žít	k5eAaImIp3nP
mennonité	mennonitý	k2eAgInPc1d1
v	v	k7c6
Mexiku	Mexiko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zemřel	zemřít	k5eAaPmAgMnS
spisovatel	spisovatel	k1gMnSc1
Octavio	Octavio	k1gMnSc1
Paz	Paz	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
(	(	kIx(
<g/>
iHNed	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1998-04-21	1998-04-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Carlos	Carlos	k1gMnSc1
Fuentes	Fuentes	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PWF	PWF	kA
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Festival	festival	k1gInSc4
spisovatelů	spisovatel	k1gMnPc2
Praha	Praha	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
cz	cz	k?
<g/>
)	)	kIx)
↑	↑	k?
Mexican	Mexican	k1gInSc1
author	author	k1gMnSc1
Jose	Jose	k1gFnPc2
Emilio	Emilio	k1gMnSc1
Pacheco	Pacheco	k1gMnSc1
wins	winsa	k1gFnPc2
coveted	coveted	k1gMnSc1
Cervantes	Cervantes	k1gMnSc1
Prize	Prize	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
France	Franc	k1gMnSc2
24	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-11-30	2009-11-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
RIEBOVÁ	RIEBOVÁ	kA
<g/>
,	,	kIx,
Markéta	Markéta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pitol	Pitol	k1gInSc1
<g/>
,	,	kIx,
Sergio	Sergio	k1gMnSc1
(	(	kIx(
<g/>
Premio	Premio	k1gMnSc1
Cervantes	Cervantes	k1gMnSc1
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
www.iliteratura.cz	www.iliteratura.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Mexická	mexický	k2eAgFnSc1d1
autorka	autorka	k1gFnSc1
Elena	Elena	k1gFnSc1
Poniatowská	Poniatowská	k1gFnSc1
si	se	k3xPyFc3
přebrala	přebrat	k5eAaPmAgFnS
Cervantesovu	Cervantesův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blesk	blesk	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Fernando	Fernanda	k1gFnSc5
del	del	k?
Paso	Paso	k6eAd1
oživuje	oživovat	k5eAaImIp3nS
historii	historie	k1gFnSc4
jako	jako	k8xS,k8xC
Cervantes	Cervantes	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získal	získat	k5eAaPmAgMnS
jeho	jeho	k3xOp3gFnSc4
cenu	cena	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
-	-	kIx~
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
13	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
PAULÍK	PAULÍK	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rulfo	Rulfo	k1gMnSc1
<g/>
,	,	kIx,
Juan	Juan	k1gMnSc1
<g/>
.	.	kIx.
www.iliteratura.cz	www.iliteratura.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Marek	Marek	k1gMnSc1
Toman	Toman	k1gMnSc1
<g/>
:	:	kIx,
Rozpaky	rozpak	k1gInPc1
nad	nad	k7c7
Travenem	Traven	k1gInSc7
-	-	kIx~
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.novinky.cz	www.novinky.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Milostné	milostný	k2eAgFnSc2d1
aféry	aféra	k1gFnSc2
mexické	mexický	k2eAgFnSc2d1
malířky	malířka	k1gFnSc2
Fridy	Frida	k1gFnSc2
Kahlo	Kahlo	k1gNnSc1
-	-	kIx~
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.novinky.cz	www.novinky.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Mexiko	Mexiko	k1gNnSc1
<g/>
:	:	kIx,
Revoluce	revoluce	k1gFnSc1
v	v	k7c6
umění	umění	k1gNnSc6
<g/>
,	,	kIx,
1910	#num#	k4
<g/>
–	–	k?
<g/>
1940	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vltava	Vltava	k1gFnSc1
-	-	kIx~
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-09-04	2013-09-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Biography	Biographa	k1gFnSc2
<g/>
:	:	kIx,
Luis	Luisa	k1gFnPc2
Barragán	Barragán	k1gInSc1
|	|	kIx~
The	The	k1gMnSc1
Pritzker	Pritzker	k1gMnSc1
Architecture	Architectur	k1gMnSc5
Prize	Prize	k1gFnSc1
<g/>
.	.	kIx.
www.pritzkerprize.com	www.pritzkerprize.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Oscar	Oscar	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
real	reala	k1gFnPc2
name	name	k1gNnSc1
is	is	k?
Emilio	Emilio	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
World	World	k1gMnSc1
from	from	k1gMnSc1
PRX	PRX	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Alfonso	Alfonso	k1gMnSc1
Cuaron	Cuaron	k1gMnSc1
wins	wins	k6eAd1
best	best	k2eAgMnSc1d1
director	director	k1gMnSc1
Oscar	Oscar	k1gMnSc1
for	forum	k1gNnPc2
'	'	kIx"
<g/>
Gravity	Gravita	k1gFnSc2
<g/>
'	'	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reuters	Reuters	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
A	a	k8xC
new	new	k?
wave	wave	k1gInSc1
of	of	k?
Mexican	Mexican	k1gInSc1
filmmakers	filmmakers	k1gInSc1
succeeds	succeeds	k1gInSc4
internationally	internationalla	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
AP	ap	kA
NEWS	NEWS	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-03-22	2018-03-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
How	How	k1gMnSc1
Mexican	Mexican	k1gMnSc1
directors	directors	k6eAd1
came	camat	k5eAaPmIp3nS
to	ten	k3xDgNnSc1
dominate	dominat	k1gInSc5
the	the	k?
Oscars	Oscars	k1gInSc1
<g/>
.	.	kIx.
www.newstatesman.com	www.newstatesman.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SAAVEDRA	SAAVEDRA	kA
<g/>
,	,	kIx,
Leonora	Leonora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Carlos	Carlos	k1gMnSc1
Chávez	Chávez	k1gMnSc1
and	and	k?
His	his	k1gNnPc2
World	World	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Princeton	Princeton	k1gInSc4
University	universita	k1gFnSc2
Press	Press	k1gInSc1
385	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
4008	#num#	k4
<g/>
-	-	kIx~
<g/>
7420	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gFnSc1
<g/>
:	:	kIx,
xvT	xvT	k?
<g/>
8	#num#	k4
<g/>
CAAAQBAJ	CAAAQBAJ	kA
<g/>
.	.	kIx.
↑	↑	k?
Pérez	Pérez	k1gInSc1
Prado	Prado	k1gNnSc1
|	|	kIx~
The	The	k1gFnSc1
Legends	Legends	k1gInSc1
|	|	kIx~
Latin	Latin	k1gMnSc1
Music	Music	k1gMnSc1
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
PBS	PBS	kA
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Do	do	k7c2
Prahy	Praha	k1gFnSc2
míří	mířit	k5eAaImIp3nP
Santana	Santan	k1gMnSc4
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nejlepších	dobrý	k2eAgMnPc2d3
kytaristů	kytarista	k1gMnPc2
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
-	-	kIx~
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.novinky.cz	www.novinky.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KONRÁD	Konrád	k1gMnSc1
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kytara	kytara	k1gFnSc1
se	se	k3xPyFc4
mi	já	k3xPp1nSc3
proměnila	proměnit	k5eAaPmAgFnS
v	v	k7c4
hada	had	k1gMnSc4
<g/>
,	,	kIx,
vzpomínal	vzpomínat	k5eAaImAgMnS
Santana	Santan	k1gMnSc4
na	na	k7c4
festival	festival	k1gInSc4
Woodstock	Woodstocka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teď	teď	k6eAd1
kapelu	kapela	k1gFnSc4
obnovil	obnovit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
(	(	kIx(
<g/>
iHNed	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-04-29	2016-04-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
UNESCO	Unesco	k1gNnSc1
<g/>
,	,	kIx,
Reprezentativní	reprezentativní	k2eAgFnSc1d1
listina	listina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
UNESCO	Unesco	k1gNnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
,	,	kIx,
RL	RL	kA
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
575	#num#	k4
<g/>
,	,	kIx,
staženo	stažen	k2eAgNnSc4d1
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2011	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Traditional	Traditional	k1gMnSc1
Mexican	Mexican	k1gMnSc1
cuisine	cuisinout	k5eAaPmIp3nS
-	-	kIx~
ancestral	ancestrat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
,	,	kIx,
ongoing	ongoing	k1gInSc1
community	communita	k1gFnSc2
culture	cultur	k1gMnSc5
<g/>
,	,	kIx,
the	the	k?
Michoacán	Michoacán	k2eAgInSc1d1
paradigm	paradigm	k1gInSc1
<g/>
.	.	kIx.
ich	ich	k?
<g/>
.	.	kIx.
<g/>
unesco	unesco	k1gMnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Olympijská	olympijský	k2eAgFnSc1d1
vítězka	vítězka	k1gFnSc1
ze	z	k7c2
Sydney	Sydney	k1gNnSc2
vzpěračka	vzpěračka	k1gFnSc1
Jiménezová	Jiménezová	k1gFnSc1
zemřela	zemřít	k5eAaPmAgFnS
na	na	k7c4
infarkt	infarkt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
jí	on	k3xPp3gFnSc3
35	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
www.sport.cz	www.sport.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Perez	Perez	k1gMnSc1
of	of	k?
Mexico	Mexico	k1gMnSc1
wins	wins	k6eAd1
men	men	k?
<g/>
'	'	kIx"
<g/>
s	s	k7c7
58	#num#	k4
<g/>
kg	kg	kA
taekwondo	taekwondo	k1gNnSc1
gold	gold	k1gInSc1
<g/>
.	.	kIx.
www.chinadaily.com.cn	www.chinadaily.com.cn	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Olympijskou	olympijský	k2eAgFnSc7d1
víťazkou	víťazka	k1gFnSc7
nad	nad	k7c4
67	#num#	k4
kg	kg	kA
Mexičanka	Mexičanka	k1gFnSc1
Espinozová	Espinozová	k1gFnSc1
<g/>
.	.	kIx.
sport	sport	k1gInSc1
<g/>
.	.	kIx.
<g/>
sme	sme	k?
<g/>
.	.	kIx.
<g/>
sk	sk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Rychlý	rychlý	k2eAgInSc4d1
gól	gól	k1gInSc4
srazil	srazit	k5eAaPmAgMnS
favorita	favorit	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olympijský	olympijský	k2eAgInSc4d1
fotbal	fotbal	k1gInSc4
ovládli	ovládnout	k5eAaPmAgMnP
Mexičané	Mexičan	k1gMnPc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-08-11	2012-08-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
FIBA	FIBA	kA
<g/>
.	.	kIx.
<g/>
basketball	basketball	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
FIBA	FIBA	kA
<g/>
.	.	kIx.
<g/>
basketball	basketball	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Rafael	Rafael	k1gMnSc1
Osuna	Osun	k1gMnSc2
-	-	kIx~
International	International	k1gMnSc1
Tennis	Tennis	k1gFnSc2
Hall	Hall	k1gMnSc1
of	of	k?
Fame	Fame	k1gInSc1
<g/>
.	.	kIx.
www.tennisfame.com	www.tennisfame.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
A	a	k8xC
50	#num#	k4
añ	añ	k1gMnSc1
del	del	k?
triunfo	triunfo	k1gMnSc1
histórico	histórico	k1gNnSc1
de	de	k?
Pedro	Pedro	k1gNnSc1
Rodríguez	Rodríguez	k1gMnSc1
en	en	k?
Le	Le	k1gFnSc2
Mans	Mansa	k1gFnPc2
<g/>
.	.	kIx.
lat.	lat.	k?
<g/>
motorsport	motorsport	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Světová	světový	k2eAgFnSc1d1
golfová	golfový	k2eAgFnSc1d1
jednička	jednička	k1gFnSc1
Ochoaová	Ochoaová	k1gFnSc1
udělala	udělat	k5eAaPmAgFnS
tečku	tečka	k1gFnSc4
za	za	k7c7
úspěšnou	úspěšný	k2eAgFnSc7d1
kariérou	kariéra	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT	ČT	kA
sport	sport	k1gInSc1
-	-	kIx~
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Šampiónka	šampiónka	k1gFnSc1
Guevaraová	Guevaraová	k1gFnSc1
končí	končit	k5eAaImIp3nS
s	s	k7c7
atletikou	atletika	k1gFnSc7
<g/>
,	,	kIx,
prý	prý	k9
kvůli	kvůli	k7c3
korupci	korupce	k1gFnSc3
<g/>
.	.	kIx.
www.sport.cz	www.sport.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Charreada	Charreada	k1gFnSc1
|	|	kIx~
Encyclopedia	Encyclopedium	k1gNnSc2
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
www.encyclopedia.com	www.encyclopedia.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Nejstarší	starý	k2eAgFnSc1d3
míčová	míčový	k2eAgFnSc1d1
hra	hra	k1gFnSc1
na	na	k7c6
světě	svět	k1gInSc6
<g/>
?	?	kIx.
</s>
<s desamb="1">
Tlachtli	Tlachtle	k1gFnSc6
se	se	k3xPyFc4
hrála	hrát	k5eAaImAgFnS
na	na	k7c4
život	život	k1gInSc4
a	a	k8xC
na	na	k7c4
smrt	smrt	k1gFnSc4
|	|	kIx~
EuroZprávy	EuroZpráva	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
eurozpravy	eurozprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
What	What	k1gInSc1
is	is	k?
the	the	k?
Alcubierre	Alcubierr	k1gInSc5
"	"	kIx"
<g/>
warp	warp	k1gInSc1
<g/>
"	"	kIx"
drive	drive	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
phys	physa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Andrés	Andrés	k1gInSc1
Manuel	Manuel	k1gMnSc1
del	del	k?
Río	Río	k1gFnSc2
<g/>
,	,	kIx,
el	ela	k1gFnPc2
verdadero	verdadero	k1gNnSc1
descubridor	descubridor	k1gMnSc1
del	del	k?
vanadio	vanadio	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Agencia	Agencia	k1gFnSc1
SINC	SINC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
The	The	k1gFnSc1
contraceptive	contraceptiv	k1gInSc5
pill	pilnout	k5eAaPmAgMnS
<g/>
:	:	kIx,
a	a	k8xC
Mexican	Mexican	k1gMnSc1
invention	invention	k1gInSc1
<g/>
.	.	kIx.
www.mexicanist.com	www.mexicanist.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Mexiko-	Mexiko-	k?
<g/>
200	#num#	k4
let	léto	k1gNnPc2
nezávislosti	nezávislost	k1gFnSc2
<g/>
..	..	k?
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Hingarová	Hingarová	k1gFnSc1
-	-	kIx~
Květinová	květinový	k2eAgFnSc1d1
-	-	kIx~
Eichlová	Eichlová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Červený	červený	k2eAgInSc1d1
Kostelec	Kostelec	k1gInSc1
<g/>
:	:	kIx,
P.	P.	kA
Mervart	Mervart	k1gInSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
485	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87378	#num#	k4
<g/>
-	-	kIx~
<g/>
48	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KAŠPAR	Kašpar	k1gMnSc1
<g/>
,	,	kIx,
Oldřich	Oldřich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
Mexika	Mexiko	k1gNnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
389	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
269	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
OPATRNÝ	opatrný	k2eAgMnSc1d1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mexiko	Mexiko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
185	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
SLAVICKÝ	SLAVICKÝ	kA
<g/>
,	,	kIx,
Stanislav	Stanislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scénické	scénický	k2eAgFnPc4d1
tradice	tradice	k1gFnPc4
Latinské	latinský	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
KANT	Kant	k1gMnSc1
–	–	k?
Karel	Karel	k1gMnSc1
Kerlický	Kerlický	k2eAgMnSc1d1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
136	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7437	#num#	k4
<g/>
-	-	kIx~
<g/>
117	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1
intervence	intervence	k1gFnSc1
v	v	k7c6
Mexiku	Mexiko	k1gNnSc6
</s>
<s>
Hispanoamerika	Hispanoamerika	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Mexiko	Mexiko	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnPc1
Mexiko	Mexiko	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Mexiko	Mexiko	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
na	na	k7c4
OpenStreetMap	OpenStreetMap	k1gInSc4
</s>
<s>
Téma	téma	k1gNnSc1
Mexiko	Mexiko	k1gNnSc4
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Mexico	Mexico	k1gNnSc1
-	-	kIx~
Amnesty	Amnest	k1gInPc1
International	International	k1gFnSc1
Report	report	k1gInSc1
2011	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amnesty	Amnest	k1gInPc4
International	International	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Mexico	Mexico	k1gNnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Freedom	Freedom	k1gInSc1
House	house	k1gNnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
-	-	kIx~
o	o	k7c6
českých	český	k2eAgMnPc6d1
krajanech	krajan	k1gMnPc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerstvo	ministerstvo	k1gNnSc1
zahraničních	zahraniční	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
2002-05-16	2002-05-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1
Stiftung	Stiftung	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BTI	BTI	kA
2010	#num#	k4
—	—	k?
Mexico	Mexico	k6eAd1
Country	country	k2eAgInSc4d1
Report	report	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gütersloh	Gütersloh	k1gMnSc1
<g/>
:	:	kIx,
Bertelsmann	Bertelsmann	k1gMnSc1
Stiftung	Stiftung	k1gMnSc1
<g/>
,	,	kIx,
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Bureau	Bureau	k6eAd1
of	of	k?
Western	western	k1gInSc1
Hemispehere	Hemispeher	k1gInSc5
Affairs	Affairs	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Background	Background	k1gInSc1
Note	Note	k1gInSc4
<g/>
:	:	kIx,
Mexiko	Mexiko	k1gNnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Department	department	k1gInSc1
of	of	k?
State	status	k1gInSc5
<g/>
,	,	kIx,
2010-12-14	2010-12-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
CIA	CIA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
World	World	k1gMnSc1
Factbook	Factbook	k1gInSc1
-	-	kIx~
Mexico	Mexico	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnSc1
<g/>
.	.	kIx.
2011-07-05	2011-07-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Library	Librar	k1gInPc1
of	of	k?
Congress	Congress	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Country	country	k2eAgInSc1d1
Profile	profil	k1gInSc5
<g/>
:	:	kIx,
Mexico	Mexico	k1gNnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-07-11	2008-07-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1
úřad	úřad	k1gInSc1
ČR	ČR	kA
v	v	k7c6
Mexiku	Mexiko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souhrnná	souhrnný	k2eAgFnSc1d1
teritoriální	teritoriální	k2eAgFnSc1d1
informace	informace	k1gFnSc1
<g/>
:	:	kIx,
Mexiko	Mexiko	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Businessinfo	Businessinfo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2011-04-01	2011-04-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
BERNSTEIN	BERNSTEIN	kA
<g/>
,	,	kIx,
Marvin	Marvina	k1gFnPc2
David	David	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mexico	Mexico	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyclopaedia	Encyclopaedium	k1gNnSc2
Britannica	Britannicum	k1gNnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gNnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc4
<g/>
}	}	kIx)
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
mexické	mexický	k2eAgFnSc2d1
–	–	k?
Estados	Estados	k1gMnSc1
Unidos	Unidos	k1gMnSc1
Mexicanos	Mexicanos	k1gMnSc1
–	–	k?
(	(	kIx(
<g/>
MEX	MEX	kA
<g/>
)	)	kIx)
Státy	stát	k1gInPc1
Mexika	Mexiko	k1gNnSc2
</s>
<s>
Aguascalientes	Aguascalientes	k1gMnSc1
•	•	k?
Baja	Baja	k1gMnSc1
California	Californium	k1gNnSc2
•	•	k?
Baja	Baja	k1gMnSc1
California	Californium	k1gNnSc2
Sur	Sur	k1gMnSc1
•	•	k?
Campeche	Campeche	k1gInSc1
•	•	k?
Chiapas	Chiapas	k1gInSc1
•	•	k?
Chihuahua	Chihuahuus	k1gMnSc2
•	•	k?
Coahuila	Coahuil	k1gMnSc2
•	•	k?
Colima	Colim	k1gMnSc2
•	•	k?
Durango	Durango	k1gMnSc1
•	•	k?
Guanajuato	Guanajuat	k2eAgNnSc1d1
•	•	k?
Guerrero	Guerrero	k1gNnSc1
•	•	k?
Hidalgo	Hidalgo	k1gMnSc1
•	•	k?
Jalisco	Jalisco	k1gMnSc1
•	•	k?
México	México	k1gMnSc1
•	•	k?
Michoacán	Michoacán	k2eAgInSc1d1
•	•	k?
Morelos	Morelos	k1gInSc1
•	•	k?
Nayarit	Nayarit	k1gInSc1
•	•	k?
Nuevo	Nuevo	k1gNnSc1
León	León	k1gMnSc1
•	•	k?
Oaxaca	Oaxaca	k1gMnSc1
•	•	k?
Puebla	Puebla	k1gMnSc1
•	•	k?
Querétaro	Querétara	k1gFnSc5
•	•	k?
Quintana	Quintan	k1gMnSc2
Roo	Roo	k1gMnSc2
•	•	k?
San	San	k1gFnSc2
Luis	Luisa	k1gFnPc2
Potosí	Potosý	k2eAgMnPc1d1
•	•	k?
Sinaloa	Sinalo	k2eAgFnSc1d1
•	•	k?
Sonora	sonora	k1gFnSc1
•	•	k?
Tabasco	Tabasco	k1gMnSc1
•	•	k?
Tamaulipas	Tamaulipas	k1gMnSc1
•	•	k?
Tlaxcala	Tlaxcala	k1gMnSc1
•	•	k?
Veracruz	Veracruz	k1gMnSc1
•	•	k?
Yucatán	Yucatán	k1gMnSc1
•	•	k?
Zacatecas	Zacatecas	k1gMnSc1
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Ciudad	Ciudad	k1gInSc1
de	de	k?
México	México	k6eAd1
</s>
<s>
Státy	stát	k1gInPc1
a	a	k8xC
území	území	k1gNnPc1
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
a	a	k8xC
Střední	střední	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
Nezávislé	závislý	k2eNgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Antigua	Antigua	k1gFnSc1
a	a	k8xC
Barbuda	Barbuda	k1gFnSc1
•	•	k?
Bahamy	Bahamy	k1gFnPc4
•	•	k?
Barbados	Barbados	k1gMnSc1
•	•	k?
Belize	Belize	k1gFnSc2
•	•	k?
Dominika	Dominik	k1gMnSc2
•	•	k?
Dominikánská	dominikánský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Grenada	Grenada	k1gFnSc1
•	•	k?
Guatemala	Guatemala	k1gFnSc1
•	•	k?
Haiti	Haiti	k1gNnSc4
•	•	k?
Honduras	Honduras	k1gInSc1
•	•	k?
Jamajka	Jamajka	k1gFnSc1
•	•	k?
Kanada	Kanada	k1gFnSc1
•	•	k?
Kostarika	Kostarika	k1gFnSc1
•	•	k?
Kuba	Kuba	k1gFnSc1
•	•	k?
Mexiko	Mexiko	k1gNnSc1
•	•	k?
Nikaragua	Nikaragua	k1gFnSc1
•	•	k?
Panama	Panama	k1gFnSc1
•	•	k?
Salvador	Salvador	k1gInSc4
•	•	k?
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgFnSc2d1
•	•	k?
Svatý	svatý	k1gMnSc1
Kryštof	Kryštof	k1gMnSc1
a	a	k8xC
Nevis	viset	k5eNaImRp2nS
•	•	k?
Svatá	svatý	k2eAgFnSc1d1
Lucie	Lucie	k1gFnSc1
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Vincenc	Vincenc	k1gMnSc1
a	a	k8xC
Grenadiny	grenadina	k1gFnPc1
•	•	k?
Trinidad	Trinidad	k1gInSc1
a	a	k8xC
Tobago	Tobago	k1gNnSc1
Součásti	součást	k1gFnSc2
suverénních	suverénní	k2eAgInPc2d1
států	stát	k1gInPc2
</s>
<s>
Francie	Francie	k1gFnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Guadeloupe	Guadeloupe	k1gFnSc1
•	•	k?
Martinik	Martinik	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Kolumbie	Kolumbie	k1gFnSc1
(	(	kIx(
<g/>
San	San	k1gFnSc1
Andrés	Andrés	k1gInSc1
a	a	k8xC
Providencia	Providencia	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Nizozemsko	Nizozemsko	k1gNnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Bonaire	Bonair	k1gMnSc5
•	•	k?
Saba	Sab	k1gInSc2
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Eustach	Eustach	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Venezuela	Venezuela	k1gFnSc1
(	(	kIx(
<g/>
Federální	federální	k2eAgFnSc1d1
dependence	dependence	k1gFnSc1
•	•	k?
Nueva	Nueva	k1gFnSc1
Esparta	esparto	k1gNnSc2
<g/>
)	)	kIx)
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
kolonie	kolonie	k1gFnSc2
azávislá	azávislý	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Dánské	dánský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Grónsko	Grónsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
Francie	Francie	k1gFnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Clippertonův	Clippertonův	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Bartoloměj	Bartoloměj	k1gMnSc1
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Martin	Martin	k1gMnSc1
•	•	k?
Saint	Saint	k1gMnSc1
Pierre	Pierr	k1gInSc5
a	a	k8xC
Miquelon	Miquelon	k1gInSc1
<g/>
)	)	kIx)
<g/>
Nizozemské	nizozemský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Aruba	Aruba	k1gFnSc1
•	•	k?
Curaçao	curaçao	k1gNnSc1
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Martin	Martin	k1gMnSc1
<g/>
)	)	kIx)
<g/>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Anguilla	Anguilla	k1gFnSc1
•	•	k?
Bermudy	Bermudy	k1gFnPc4
•	•	k?
Britské	britský	k2eAgNnSc1d1
Panenské	panenský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
Kajmanské	Kajmanský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
Montserrat	Montserrat	k1gInSc1
•	•	k?
Turks	Turksa	k1gFnPc2
a	a	k8xC
Caicos	Caicosa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Americké	americký	k2eAgInPc4d1
Panenské	panenský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
•	•	k?
Navassa	Navassa	k1gFnSc1
•	•	k?
Portoriko	Portoriko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Státy	stát	k1gInPc1
a	a	k8xC
území	území	k1gNnSc1
s	s	k7c7
členstvím	členství	k1gNnSc7
v	v	k7c6
APEC	APEC	kA
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
•	•	k?
Brunej	Brunej	k1gFnSc1
•	•	k?
Čína	Čína	k1gFnSc1
•	•	k?
Filipíny	Filipíny	k1gFnPc4
•	•	k?
Hongkong	Hongkong	k1gInSc1
•	•	k?
Chile	Chile	k1gNnSc2
•	•	k?
Indonésie	Indonésie	k1gFnSc1
•	•	k?
Japonsko	Japonsko	k1gNnSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Kanada	Kanada	k1gFnSc1
•	•	k?
Malajsie	Malajsie	k1gFnSc1
•	•	k?
Mexiko	Mexiko	k1gNnSc1
•	•	k?
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
•	•	k?
Papua	Papuum	k1gNnSc2
Nová	nový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
•	•	k?
Peru	prát	k5eAaImIp1nS
•	•	k?
Rusko	Rusko	k1gNnSc4
•	•	k?
Singapur	Singapur	k1gInSc1
•	•	k?
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgInPc4d1
•	•	k?
Thajsko	Thajsko	k1gNnSc1
•	•	k?
Tchaj-wan	Tchaj-wan	k1gInSc1
•	•	k?
Vietnam	Vietnam	k1gInSc1
</s>
<s>
G20	G20	k4
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Argentina	Argentina	k1gFnSc1
•	•	k?
Brazílie	Brazílie	k1gFnSc2
•	•	k?
Kanada	Kanada	k1gFnSc1
•	•	k?
Mexiko	Mexiko	k1gNnSc1
•	•	k?
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgFnSc2d1
•	•	k?
Indonésie	Indonésie	k1gFnSc2
•	•	k?
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
•	•	k?
Indie	Indie	k1gFnSc2
•	•	k?
Čína	Čína	k1gFnSc1
•	•	k?
Japonsko	Japonsko	k1gNnSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Turecko	Turecko	k1gNnSc1
•	•	k?
Evropská	evropský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
•	•	k?
Francie	Francie	k1gFnSc2
•	•	k?
Německo	Německo	k1gNnSc1
•	•	k?
Itálie	Itálie	k1gFnSc2
•	•	k?
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Austrálie	Austrálie	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Střední	střední	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
a	a	k8xC
Karibik	Karibik	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
128515	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4039058-5	4039058-5	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2359	#num#	k4
3927	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
81013960	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
154775570	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
81013960	#num#	k4
</s>
