<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
moře	moře	k1gNnSc1	moře
Tethys	Tethys	k1gInSc1	Tethys
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
sahalo	sahat	k5eAaImAgNnS	sahat
od	od	k7c2	od
nynějšího	nynější	k2eAgInSc2d1	nynější
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
přes	přes	k7c4	přes
Španělsko	Španělsko	k1gNnSc4	Španělsko
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
,	,	kIx,	,
pohoří	pohořet	k5eAaPmIp3nP	pohořet
Himálaj	Himálaj	k1gFnSc4	Himálaj
až	až	k9	až
na	na	k7c4	na
Malajský	malajský	k2eAgInSc4d1	malajský
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
.	.	kIx.	.
</s>
