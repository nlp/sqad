<s>
Tethys	Tethys	k6eAd1	Tethys
bylo	být	k5eAaImAgNnS	být
prehistorické	prehistorický	k2eAgNnSc1d1	prehistorické
moře	moře	k1gNnSc1	moře
rovnoběžkového	rovnoběžkový	k2eAgInSc2d1	rovnoběžkový
směru	směr	k1gInSc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
druhohorách	druhohory	k1gFnPc6	druhohory
(	(	kIx(	(
<g/>
Mezozoikum	mezozoikum	k1gNnSc1	mezozoikum
<g/>
)	)	kIx)	)
v	v	k7c6	v
Triasu	trias	k1gInSc6	trias
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
250	[number]	k4	250
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Kontinent	kontinent	k1gInSc1	kontinent
Pangea	Pangea	k1gFnSc1	Pangea
se	se	k3xPyFc4	se
zhruba	zhruba	k6eAd1	zhruba
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
středu	střed	k1gInSc6	střed
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
na	na	k7c4	na
severní	severní	k2eAgFnPc4d1	severní
(	(	kIx(	(
<g/>
Laurasie	Laurasie	k1gFnPc4	Laurasie
<g/>
)	)	kIx)	)
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
(	(	kIx(	(
<g/>
Gondwana	Gondwana	k1gFnSc1	Gondwana
<g/>
)	)	kIx)	)
část	část	k1gFnSc1	část
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
moře	moře	k1gNnSc1	moře
Tethys	Tethys	k1gInSc1	Tethys
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
sahalo	sahat	k5eAaImAgNnS	sahat
od	od	k7c2	od
nynějšího	nynější	k2eAgInSc2d1	nynější
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
přes	přes	k7c4	přes
Španělsko	Španělsko	k1gNnSc4	Španělsko
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
,	,	kIx,	,
pohoří	pohořet	k5eAaPmIp3nP	pohořet
Himálaj	Himálaj	k1gFnSc4	Himálaj
až	až	k9	až
na	na	k7c4	na
Malajský	malajský	k2eAgInSc4d1	malajský
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Představovalo	představovat	k5eAaImAgNnS	představovat
teplou	teplý	k2eAgFnSc4d1	teplá
sedimentační	sedimentační	k2eAgFnSc4d1	sedimentační
oblast	oblast	k1gFnSc4	oblast
a	a	k8xC	a
docházelo	docházet	k5eAaImAgNnS	docházet
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
postupně	postupně	k6eAd1	postupně
k	k	k7c3	k
četným	četný	k2eAgInPc3d1	četný
vrásnivým	vrásnivý	k2eAgInPc3d1	vrásnivý
pohybům	pohyb	k1gInPc3	pohyb
a	a	k8xC	a
paleogeografickým	paleogeografický	k2eAgFnPc3d1	paleogeografická
změnám	změna	k1gFnPc3	změna
<g/>
.	.	kIx.	.
</s>
