<s>
Jahodník	jahodník	k1gInSc1	jahodník
trávnice	trávnice	k1gFnSc2	trávnice
(	(	kIx(	(
<g/>
Fragaria	Fragarium	k1gNnSc2	Fragarium
viridis	viridis	k1gFnSc2	viridis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
<g/>
,	,	kIx,	,
asi	asi	k9	asi
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
cm	cm	kA	cm
vysoká	vysoký	k2eAgFnSc1d1	vysoká
bylina	bylina	k1gFnSc1	bylina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
růžovitých	růžovitý	k2eAgMnPc2d1	růžovitý
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jahodníkem	jahodník	k1gInSc7	jahodník
obecným	obecný	k2eAgInSc7d1	obecný
(	(	kIx(	(
<g/>
Fragaria	Fragarium	k1gNnSc2	Fragarium
vesca	vesc	k1gInSc2	vesc
<g/>
)	)	kIx)	)
a	a	k8xC	a
jahodníkem	jahodník	k1gInSc7	jahodník
truskavcem	truskavec	k1gInSc7	truskavec
(	(	kIx(	(
<g/>
Fragaria	Fragarium	k1gNnSc2	Fragarium
moschata	moschata	k1gFnSc1	moschata
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
druhů	druh	k1gInPc2	druh
jahodníku	jahodník	k1gInSc2	jahodník
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
zaměňován	zaměňován	k2eAgInSc1d1	zaměňován
s	s	k7c7	s
jahodníkem	jahodník	k1gInSc7	jahodník
obecným	obecný	k2eAgInSc7d1	obecný
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
svým	svůj	k3xOyFgInSc7	svůj
drobnějším	drobný	k2eAgInSc7d2	drobnější
vzrůstem	vzrůst	k1gInSc7	vzrůst
a	a	k8xC	a
nápadně	nápadně	k6eAd1	nápadně
malým	malý	k2eAgInSc7d1	malý
prostředním	prostřední	k2eAgInSc7d1	prostřední
zubem	zub	k1gInSc7	zub
na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
lístku	lístek	k1gInSc6	lístek
<g/>
.	.	kIx.	.
</s>
<s>
Kvete	kvést	k5eAaImIp3nS	kvést
na	na	k7c6	na
konci	konec	k1gInSc6	konec
května	květen	k1gInSc2	květen
až	až	k9	až
začátkem	začátkem	k7c2	začátkem
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Oddenek	oddenek	k1gInSc1	oddenek
je	být	k5eAaImIp3nS	být
krátký	krátký	k2eAgInSc1d1	krátký
<g/>
,	,	kIx,	,
šikmý	šikmý	k2eAgInSc1d1	šikmý
<g/>
,	,	kIx,	,
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
počtem	počet	k1gInSc7	počet
niťovitých	niťovitý	k2eAgInPc2d1	niťovitý
kořenů	kořen	k1gInPc2	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
lodyha	lodyha	k1gFnSc1	lodyha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
kryta	krýt	k5eAaImNgFnS	krýt
žláznatými	žláznatý	k2eAgInPc7d1	žláznatý
a	a	k8xC	a
krycími	krycí	k2eAgInPc7d1	krycí
chlupy	chlup	k1gInPc7	chlup
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
polovině	polovina	k1gFnSc6	polovina
odstálé	odstálý	k2eAgFnSc6d1	odstálá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
polovině	polovina	k1gFnSc6	polovina
se	se	k3xPyFc4	se
lodyha	lodyha	k1gFnSc1	lodyha
větví	větev	k1gFnPc2	větev
a	a	k8xC	a
v	v	k7c6	v
přízemní	přízemní	k2eAgFnSc6d1	přízemní
části	část	k1gFnSc6	část
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
listovou	listový	k2eAgFnSc4d1	listová
růžici	růžice	k1gFnSc4	růžice
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
v	v	k7c6	v
přízemní	přízemní	k2eAgFnSc6d1	přízemní
růžici	růžice	k1gFnSc6	růžice
jsou	být	k5eAaImIp3nP	být
řapíkaté	řapíkatý	k2eAgFnPc1d1	řapíkatá
<g/>
,	,	kIx,	,
trojčetné	trojčetný	k2eAgFnPc1d1	trojčetná
a	a	k8xC	a
z	z	k7c2	z
jejich	jejich	k3xOp3gFnPc2	jejich
paždí	paždě	k1gFnPc2	paždě
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
šlahouny	šlahoun	k1gInPc1	šlahoun
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
krátké	krátký	k2eAgFnPc1d1	krátká
nebo	nebo	k8xC	nebo
zcela	zcela	k6eAd1	zcela
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Řapík	řapík	k1gInSc1	řapík
i	i	k8xC	i
lístky	lístek	k1gInPc1	lístek
listů	list	k1gInPc2	list
jsou	být	k5eAaImIp3nP	být
kryty	krýt	k5eAaImNgInP	krýt
velmi	velmi	k6eAd1	velmi
hustě	hustě	k6eAd1	hustě
chlupy	chlup	k1gInPc1	chlup
<g/>
.	.	kIx.	.
</s>
<s>
Lístky	lístek	k1gInPc1	lístek
jsou	být	k5eAaImIp3nP	být
pilovitě	pilovitě	k6eAd1	pilovitě
zubaté	zubatý	k2eAgFnPc1d1	zubatá
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
zuby	zub	k1gInPc1	zub
jsou	být	k5eAaImIp3nP	být
srpovitě	srpovitě	k6eAd1	srpovitě
protažené	protažený	k2eAgInPc1d1	protažený
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
směřují	směřovat	k5eAaImIp3nP	směřovat
dopředu	dopředu	k6eAd1	dopředu
a	a	k8xC	a
na	na	k7c6	na
koncích	konec	k1gInPc6	konec
nabývají	nabývat	k5eAaImIp3nP	nabývat
červené	červený	k2eAgFnPc1d1	červená
barvy	barva	k1gFnPc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
Prostřední	prostřední	k2eAgInSc1d1	prostřední
lístek	lístek	k1gInSc1	lístek
je	být	k5eAaImIp3nS	být
řapíkatý	řapíkatý	k2eAgInSc1d1	řapíkatý
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
prostřední	prostřední	k2eAgInSc1d1	prostřední
zub	zub	k1gInSc1	zub
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
menší	malý	k2eAgInPc4d2	menší
než	než	k8xS	než
ostatní	ostatní	k2eAgInPc4d1	ostatní
zuby	zub	k1gInPc4	zub
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
druhů	druh	k1gInPc2	druh
jahodníku	jahodník	k1gInSc2	jahodník
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
těchto	tento	k3xDgInPc2	tento
lístků	lístek	k1gInPc2	lístek
je	být	k5eAaImIp3nS	být
trávově	trávově	k6eAd1	trávově
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
.	.	kIx.	.
</s>
<s>
Palisty	palist	k1gInPc1	palist
jsou	být	k5eAaImIp3nP	být
úzké	úzký	k2eAgFnPc1d1	úzká
<g/>
,	,	kIx,	,
kopinaté	kopinatý	k2eAgFnPc1d1	kopinatá
<g/>
,	,	kIx,	,
světlé	světlý	k2eAgFnPc1d1	světlá
až	až	k8xS	až
tmavohnědé	tmavohnědý	k2eAgFnPc1d1	tmavohnědá
barvy	barva	k1gFnPc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
svrchní	svrchní	k2eAgFnSc1d1	svrchní
strana	strana	k1gFnSc1	strana
je	být	k5eAaImIp3nS	být
lysá	lysý	k2eAgFnSc1d1	Lysá
<g/>
,	,	kIx,	,
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
straně	strana	k1gFnSc6	strana
jsou	být	k5eAaImIp3nP	být
kryty	kryt	k2eAgInPc1d1	kryt
chlupy	chlup	k1gInPc1	chlup
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
pětičetné	pětičetný	k2eAgFnPc4d1	pětičetná
a	a	k8xC	a
oboupohlavné	oboupohlavný	k2eAgFnPc4d1	oboupohlavná
v	v	k7c6	v
chudém	chudý	k2eAgInSc6d1	chudý
vrcholíku	vrcholík	k1gInSc6	vrcholík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
jahodníkem	jahodník	k1gInSc7	jahodník
obecným	obecný	k2eAgInSc7d1	obecný
(	(	kIx(	(
<g/>
Fragaria	Fragarium	k1gNnSc2	Fragarium
vesca	vesc	k1gInSc2	vesc
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
výrazně	výrazně	k6eAd1	výrazně
větší	veliký	k2eAgFnPc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Kališní	kališní	k2eAgInPc1d1	kališní
lístky	lístek	k1gInPc1	lístek
jsou	být	k5eAaImIp3nP	být
trojhranné	trojhranný	k2eAgInPc1d1	trojhranný
<g/>
,	,	kIx,	,
na	na	k7c6	na
líci	líc	k1gFnSc6	líc
chlupaté	chlupatý	k2eAgFnSc6d1	chlupatá
a	a	k8xC	a
na	na	k7c6	na
rubu	rub	k1gInSc6	rub
lysé	lysý	k2eAgNnSc1d1	lysé
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
zrání	zrání	k1gNnSc2	zrání
nabývají	nabývat	k5eAaImIp3nP	nabývat
tmavočervené	tmavočervený	k2eAgFnPc1d1	tmavočervená
barvy	barva	k1gFnPc1	barva
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
přitisknuté	přitisknutý	k2eAgFnPc1d1	přitisknutá
k	k	k7c3	k
souplodí	souplodí	k1gNnSc3	souplodí
<g/>
.	.	kIx.	.
</s>
<s>
Korunní	korunní	k2eAgInPc1d1	korunní
lístky	lístek	k1gInPc1	lístek
jsou	být	k5eAaImIp3nP	být
okrouhlé	okrouhlý	k2eAgInPc1d1	okrouhlý
<g/>
,	,	kIx,	,
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
nažloutlé	nažloutlý	k2eAgNnSc1d1	nažloutlé
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
bílou	bílý	k2eAgFnSc4d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Jahoda	jahoda	k1gFnSc1	jahoda
není	být	k5eNaImIp3nS	být
jednotlivým	jednotlivý	k2eAgInSc7d1	jednotlivý
plodem	plod	k1gInSc7	plod
jahodníku	jahodník	k1gInSc2	jahodník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
souplodí	souplodí	k1gNnSc6	souplodí
nažek	nažka	k1gFnPc2	nažka
zanořených	zanořený	k2eAgFnPc2d1	zanořená
do	do	k7c2	do
povrchu	povrch	k1gInSc2	povrch
zdužnatělého	zdužnatělý	k2eAgNnSc2d1	zdužnatělé
květního	květní	k2eAgNnSc2d1	květní
lůžka	lůžko	k1gNnSc2	lůžko
<g/>
.	.	kIx.	.
</s>
<s>
Nažky	nažka	k1gFnPc1	nažka
jsou	být	k5eAaImIp3nP	být
drobné	drobný	k2eAgFnPc1d1	drobná
<g/>
,	,	kIx,	,
lesklé	lesklý	k2eAgFnPc1d1	lesklá
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
stejnoměrně	stejnoměrně	k6eAd1	stejnoměrně
rozloženy	rozložit	k5eAaPmNgInP	rozložit
po	po	k7c6	po
povrchu	povrch	k1gInSc6	povrch
jahody	jahoda	k1gFnSc2	jahoda
<g/>
.	.	kIx.	.
</s>
<s>
Souplodí	souplodí	k1gNnSc1	souplodí
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
těžko	těžko	k6eAd1	těžko
oddělitelné	oddělitelný	k2eAgInPc1d1	oddělitelný
od	od	k7c2	od
kalichu	kalich	k1gInSc2	kalich
<g/>
.	.	kIx.	.
<g/>
Jahoda	jahoda	k1gFnSc1	jahoda
je	být	k5eAaImIp3nS	být
hruškovitého	hruškovitý	k2eAgInSc2d1	hruškovitý
tvaru	tvar	k1gInSc2	tvar
a	a	k8xC	a
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
zhruba	zhruba	k6eAd1	zhruba
0,8	[number]	k4	0,8
<g/>
–	–	k?	–
<g/>
1,2	[number]	k4	1,2
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
krček	krček	k1gInSc1	krček
bílé	bílý	k2eAgFnSc2d1	bílá
až	až	k8xS	až
narůžovělé	narůžovělý	k2eAgFnSc2d1	narůžovělá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
vápencových	vápencový	k2eAgInPc6d1	vápencový
a	a	k8xC	a
neutrálních	neutrální	k2eAgInPc6d1	neutrální
podkladech	podklad	k1gInPc6	podklad
<g/>
,	,	kIx,	,
na	na	k7c6	na
suchých	suchý	k2eAgFnPc6d1	suchá
půdách	půda	k1gFnPc6	půda
<g/>
,	,	kIx,	,
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
slunných	slunný	k2eAgFnPc2d1	slunná
či	či	k8xC	či
mírně	mírně	k6eAd1	mírně
zastíněných	zastíněný	k2eAgInPc2d1	zastíněný
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
na	na	k7c6	na
lesních	lesní	k2eAgFnPc6d1	lesní
pastvinách	pastvina	k1gFnPc6	pastvina
<g/>
,	,	kIx,	,
na	na	k7c6	na
výsluních	výsluň	k1gFnPc6	výsluň
křovinatých	křovinatý	k2eAgFnPc2d1	křovinatá
strání	stráň	k1gFnPc2	stráň
nebo	nebo	k8xC	nebo
na	na	k7c6	na
travnatých	travnatý	k2eAgInPc6d1	travnatý
porostech	porost	k1gInPc6	porost
<g/>
.	.	kIx.	.
</s>
<s>
Snáší	snášet	k5eAaImIp3nP	snášet
i	i	k9	i
silnější	silný	k2eAgInPc1d2	silnější
výkyvy	výkyv	k1gInPc1	výkyv
teplot	teplota	k1gFnPc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
téměř	téměř	k6eAd1	téměř
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
až	až	k9	až
po	po	k7c6	po
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
jej	on	k3xPp3gMnSc4	on
můžeme	moct	k5eAaImIp1nP	moct
nalézt	nalézt	k5eAaPmF	nalézt
například	například	k6eAd1	například
v	v	k7c6	v
travinách	travina	k1gFnPc6	travina
při	při	k7c6	při
toku	tok	k1gInSc6	tok
Labe	Labe	k1gNnSc2	Labe
nebo	nebo	k8xC	nebo
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Žďáru	Žďár	k1gInSc2	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
se	se	k3xPyFc4	se
nepohlavně	pohlavně	k6eNd1	pohlavně
(	(	kIx(	(
<g/>
vegetativně	vegetativně	k6eAd1	vegetativně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
šlahouny	šlahoun	k1gInPc7	šlahoun
vyrůstajícími	vyrůstající	k2eAgMnPc7d1	vyrůstající
z	z	k7c2	z
paždí	paždí	k1gNnSc2	paždí
listů	list	k1gInPc2	list
v	v	k7c6	v
přízemní	přízemní	k2eAgFnSc6d1	přízemní
růžici	růžice	k1gFnSc6	růžice
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
odnože	odnož	k1gInPc1	odnož
zakořeňují	zakořeňovat	k5eAaImIp3nP	zakořeňovat
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
nové	nový	k2eAgFnPc4d1	nová
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Jahodník	jahodník	k1gInSc1	jahodník
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
rozmnožovat	rozmnožovat	k5eAaImF	rozmnožovat
i	i	k9	i
pohlavně	pohlavně	k6eAd1	pohlavně
(	(	kIx(	(
<g/>
generativně	generativně	k6eAd1	generativně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
semeny	semeno	k1gNnPc7	semeno
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgInSc2	tento
způsobu	způsob	k1gInSc2	způsob
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
především	především	k9	především
při	při	k7c6	při
záměrném	záměrný	k2eAgNnSc6d1	záměrné
pěstování	pěstování	k1gNnSc6	pěstování
<g/>
.	.	kIx.	.
</s>
<s>
Planě	planě	k6eAd1	planě
rostoucí	rostoucí	k2eAgInSc1d1	rostoucí
jahodník	jahodník	k1gInSc1	jahodník
trávnice	trávnice	k1gFnSc2	trávnice
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jahodníkem	jahodník	k1gInSc7	jahodník
obecným	obecný	k2eAgInSc7d1	obecný
a	a	k8xC	a
jahodníkem	jahodník	k1gInSc7	jahodník
truskavcem	truskavec	k1gInSc7	truskavec
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
součástí	součást	k1gFnSc7	součást
jídelníčku	jídelníček	k1gInSc2	jídelníček
primitivního	primitivní	k2eAgMnSc4d1	primitivní
člověka	člověk	k1gMnSc4	člověk
už	už	k6eAd1	už
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kamenné	kamenný	k2eAgInPc1d1	kamenný
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
nálezy	nález	k1gInPc1	nález
ve	v	k7c6	v
švýcarských	švýcarský	k2eAgFnPc6d1	švýcarská
kolových	kolový	k2eAgFnPc6d1	kolová
stavbách	stavba	k1gFnPc6	stavba
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
písemné	písemný	k2eAgFnPc1d1	písemná
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
jahodníku	jahodník	k1gInSc6	jahodník
se	se	k3xPyFc4	se
datují	datovat	k5eAaImIp3nP	datovat
do	do	k7c2	do
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rozšíření	rozšíření	k1gNnSc4	rozšíření
a	a	k8xC	a
využívání	využívání	k1gNnSc4	využívání
jahod	jahoda	k1gFnPc2	jahoda
psal	psát	k5eAaImAgInS	psát
například	například	k6eAd1	například
Plinius	Plinius	k1gInSc1	Plinius
starší	starý	k2eAgInSc1d2	starší
nebo	nebo	k8xC	nebo
Hippokratés	Hippokratés	k1gInSc1	Hippokratés
<g/>
.	.	kIx.	.
</s>
<s>
Ovidius	Ovidius	k1gMnSc1	Ovidius
a	a	k8xC	a
Vergilius	Vergilius	k1gMnSc1	Vergilius
označili	označit	k5eAaPmAgMnP	označit
jahody	jahoda	k1gFnPc4	jahoda
za	za	k7c4	za
dar	dar	k1gInSc4	dar
Matky	matka	k1gFnSc2	matka
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jahodník	jahodník	k1gInSc1	jahodník
trávnice	trávnice	k1gFnSc2	trávnice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Taxon	taxon	k1gInSc4	taxon
Fragaria	Fragarium	k1gNnSc2	Fragarium
viridis	viridis	k1gFnSc2	viridis
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
Biolib	Bioliba	k1gFnPc2	Bioliba
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Rostliny	rostlina	k1gFnSc2	rostlina
<g/>
.	.	kIx.	.
<g/>
prirodou	prirodý	k2eAgFnSc4d1	prirodý
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Jahody	jahoda	k1gFnSc2	jahoda
<g/>
.	.	kIx.	.
<g/>
unas	unas	k1gInSc1	unas
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Wikispecies	Wikispecies	k1gInSc1	Wikispecies
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
