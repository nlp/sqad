<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jahodníkem	jahodník	k1gInSc7	jahodník
obecným	obecný	k2eAgInSc7d1	obecný
(	(	kIx(	(
<g/>
Fragaria	Fragarium	k1gNnSc2	Fragarium
vesca	vesc	k1gInSc2	vesc
<g/>
)	)	kIx)	)
a	a	k8xC	a
jahodníkem	jahodník	k1gInSc7	jahodník
truskavcem	truskavec	k1gInSc7	truskavec
(	(	kIx(	(
<g/>
Fragaria	Fragarium	k1gNnSc2	Fragarium
moschata	moschata	k1gFnSc1	moschata
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
druhů	druh	k1gInPc2	druh
jahodníku	jahodník	k1gInSc2	jahodník
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
