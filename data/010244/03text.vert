<p>
<s>
Hymna	hymna	k1gFnSc1	hymna
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
SSSR	SSSR	kA	SSSR
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
oficiální	oficiální	k2eAgInSc4d1	oficiální
název	název	k1gInSc4	název
hymny	hymna	k1gFnSc2	hymna
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
neexistujícího	existující	k2eNgInSc2d1	neexistující
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
používána	používat	k5eAaImNgFnS	používat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
do	do	k7c2	do
zániku	zánik	k1gInSc2	zánik
tohoto	tento	k3xDgInSc2	tento
státu	stát	k1gInSc2	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Před	před	k7c7	před
touto	tento	k3xDgFnSc7	tento
hymnou	hymna	k1gFnSc7	hymna
byla	být	k5eAaImAgFnS	být
oficiální	oficiální	k2eAgFnSc7d1	oficiální
hymnou	hymna	k1gFnSc7	hymna
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
Internacionála	Internacionála	k1gFnSc1	Internacionála
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
píseň	píseň	k1gFnSc1	píseň
byla	být	k5eAaImAgFnS	být
svázána	svázat	k5eAaPmNgFnS	svázat
s	s	k7c7	s
dělnickou	dělnický	k2eAgFnSc7d1	Dělnická
revolucí	revoluce	k1gFnSc7	revoluce
a	a	k8xC	a
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
Stalin	Stalin	k1gMnSc1	Stalin
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
konečného	konečný	k2eAgInSc2d1	konečný
cíle	cíl	k1gInSc2	cíl
dělníků	dělník	k1gMnPc2	dělník
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
úspěšně	úspěšně	k6eAd1	úspěšně
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
se	se	k3xPyFc4	se
hymnu	hymna	k1gFnSc4	hymna
změnit	změnit	k5eAaPmF	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Realizaci	realizace	k1gFnSc3	realizace
změny	změna	k1gFnSc2	změna
pozdržela	pozdržet	k5eAaPmAgFnS	pozdržet
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
a	a	k8xC	a
hudba	hudba	k1gFnSc1	hudba
tak	tak	k6eAd1	tak
vznikaly	vznikat	k5eAaImAgFnP	vznikat
za	za	k7c2	za
Stalinova	Stalinův	k2eAgInSc2d1	Stalinův
důkladného	důkladný	k2eAgInSc2d1	důkladný
dozoru	dozor	k1gInSc2	dozor
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
soutěže	soutěž	k1gFnPc1	soutěž
o	o	k7c4	o
sestavení	sestavení	k1gNnSc4	sestavení
prvního	první	k4xOgInSc2	první
návrhu	návrh	k1gInSc2	návrh
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
spisovatelů	spisovatel	k1gMnPc2	spisovatel
a	a	k8xC	a
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Autoři	autor	k1gMnPc1	autor
slov	slovo	k1gNnPc2	slovo
vítězného	vítězný	k2eAgInSc2d1	vítězný
návrhu	návrh	k1gInSc2	návrh
jsou	být	k5eAaImIp3nP	být
váleční	váleční	k2eAgMnPc1d1	váleční
reportéři	reportér	k1gMnPc1	reportér
kapitán	kapitán	k1gMnSc1	kapitán
Sergej	Sergej	k1gMnSc1	Sergej
Vladimirovič	Vladimirovič	k1gMnSc1	Vladimirovič
Michalkov	Michalkov	k1gInSc1	Michalkov
(	(	kIx(	(
<g/>
také	také	k9	také
populární	populární	k2eAgMnSc1d1	populární
autor	autor	k1gMnSc1	autor
dětské	dětský	k2eAgFnSc2d1	dětská
literatury	literatura	k1gFnSc2	literatura
<g/>
)	)	kIx)	)
a	a	k8xC	a
major	major	k1gMnSc1	major
Gabriel	Gabriel	k1gMnSc1	Gabriel
Ureklian	Ureklian	k1gMnSc1	Ureklian
(	(	kIx(	(
<g/>
Armén	Armén	k1gMnSc1	Armén
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
používal	používat	k5eAaImAgInS	používat
pseudonym	pseudonym	k1gInSc4	pseudonym
El-Registán	El-Registán	k2eAgInSc4d1	El-Registán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
melodie	melodie	k1gFnSc2	melodie
je	být	k5eAaImIp3nS	být
Alexandr	Alexandr	k1gMnSc1	Alexandr
Vasiljevič	Vasiljevič	k1gMnSc1	Vasiljevič
Alexandrov	Alexandrovo	k1gNnPc2	Alexandrovo
<g/>
.	.	kIx.	.
</s>
<s>
Prvního	první	k4xOgNnSc2	první
představení	představení	k1gNnSc2	představení
se	se	k3xPyFc4	se
nová	nový	k2eAgFnSc1d1	nová
hymna	hymna	k1gFnSc1	hymna
dočkala	dočkat	k5eAaPmAgFnS	dočkat
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
byla	být	k5eAaImAgFnS	být
za	za	k7c2	za
hymnu	hymnus	k1gInSc2	hymnus
prohlášena	prohlášen	k2eAgFnSc1d1	prohlášena
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Melodie	melodie	k1gFnSc1	melodie
hymny	hymna	k1gFnSc2	hymna
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
1938	[number]	k4	1938
<g/>
-	-	kIx~	-
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
používána	používán	k2eAgFnSc1d1	používána
jako	jako	k8xC	jako
hymna	hymna	k1gFnSc1	hymna
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
svazové	svazový	k2eAgFnPc1d1	svazová
republiky	republika	k1gFnPc1	republika
během	během	k7c2	během
následujících	následující	k2eAgNnPc2d1	následující
let	léto	k1gNnPc2	léto
začaly	začít	k5eAaPmAgFnP	začít
používat	používat	k5eAaImF	používat
svoje	svůj	k3xOyFgInPc4	svůj
národní	národní	k2eAgInPc4d1	národní
hymny	hymnus	k1gInPc4	hymnus
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
byla	být	k5eAaImAgFnS	být
Ruská	ruský	k2eAgFnSc1d1	ruská
federace	federace	k1gFnSc1	federace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nikdy	nikdy	k6eAd1	nikdy
národní	národní	k2eAgFnSc4d1	národní
hymnu	hymna	k1gFnSc4	hymna
neměla	mít	k5eNaImAgFnS	mít
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
její	její	k3xOp3gInSc4	její
návrh	návrh	k1gInSc4	návrh
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
složil	složit	k5eAaPmAgMnS	složit
Dmitrij	Dmitrij	k1gMnSc1	Dmitrij
Šostakovič	Šostakovič	k1gMnSc1	Šostakovič
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Stalin	Stalin	k1gMnSc1	Stalin
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
hymna	hymna	k1gFnSc1	hymna
změněna	změněn	k2eAgFnSc1d1	změněna
(	(	kIx(	(
<g/>
Stalinovo	Stalinův	k2eAgNnSc1d1	Stalinovo
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
výslovně	výslovně	k6eAd1	výslovně
v	v	k7c6	v
textu	text	k1gInSc6	text
zmíněno	zmíněn	k2eAgNnSc1d1	zmíněno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
byla	být	k5eAaImAgFnS	být
hymna	hymna	k1gFnSc1	hymna
hrána	hrát	k5eAaImNgFnS	hrát
beze	beze	k7c2	beze
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgInS	být
text	text	k1gInSc1	text
Michalkovem	Michalkov	k1gInSc7	Michalkov
slabě	slabě	k6eAd1	slabě
pozměněn	pozměněn	k2eAgInSc4d1	pozměněn
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Stalinovi	Stalin	k1gMnSc3	Stalin
byla	být	k5eAaImAgFnS	být
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Text	text	k1gInSc1	text
a	a	k8xC	a
překlad	překlad	k1gInSc1	překlad
hymny	hymna	k1gFnSc2	hymna
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Umělecký	umělecký	k2eAgInSc1d1	umělecký
překlad	překlad	k1gInSc1	překlad
===	===	k?	===
</s>
</p>
<p>
<s>
Umělecký	umělecký	k2eAgInSc1d1	umělecký
překlad	překlad	k1gInSc1	překlad
<g/>
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Taufer	Taufer	k1gMnSc1	Taufer
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kol	kol	k7c2	kol
Rusi	Rus	k1gFnSc2	Rus
veliké	veliký	k2eAgFnPc1d1	veliká
teď	teď	k6eAd1	teď
republik	republika	k1gFnPc2	republika
volných	volný	k2eAgNnPc2d1	volné
</s>
</p>
<p>
<s>
zní	znět	k5eAaImIp3nS	znět
jednoty	jednota	k1gFnPc4	jednota
na	na	k7c4	na
věky	věk	k1gInPc4	věk
svobodný	svobodný	k2eAgInSc1d1	svobodný
hlas	hlas	k1gInSc1	hlas
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
to	ten	k3xDgNnSc4	ten
z	z	k7c2	z
národů	národ	k1gInPc2	národ
vůle	vůle	k1gFnSc2	vůle
je	být	k5eAaImIp3nS	být
svaz	svaz	k1gInSc1	svaz
nerozborný	rozborný	k2eNgInSc1d1	nerozborný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ať	ať	k9	ať
žije	žít	k5eAaImIp3nS	žít
náš	náš	k3xOp1gInSc1	náš
mohutný	mohutný	k2eAgInSc1d1	mohutný
Sovětský	sovětský	k2eAgInSc1d1	sovětský
Svaz	svaz	k1gInSc1	svaz
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
Sláva	Sláva	k1gFnSc1	Sláva
ti	ty	k3xPp2nSc3	ty
otčino	otčina	k1gFnSc5	otčina
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc4	ten
naše	náš	k3xOp1gFnSc1	náš
svobodná	svobodný	k2eAgFnSc1d1	svobodná
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
družby	družba	k1gFnPc1	družba
národů	národ	k1gInPc2	národ
opora	opora	k1gFnSc1	opora
tkví	tkvět	k5eAaImIp3nS	tkvět
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
Ať	ať	k9	ať
prapor	prapor	k1gInSc1	prapor
sovětů	sovět	k1gInPc2	sovět
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
prapor	prapor	k1gInSc4	prapor
lidový	lidový	k2eAgInSc4d1	lidový
</s>
</p>
<p>
<s>
nás	my	k3xPp1nPc4	my
od	od	k7c2	od
vítězství	vítězství	k1gNnPc2	vítězství
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kdys	kdys	k6eAd1	kdys
volnost	volnost	k1gFnSc1	volnost
jak	jak	k6eAd1	jak
slunce	slunce	k1gNnSc2	slunce
nám	my	k3xPp1nPc3	my
zářila	zářit	k5eAaImAgFnS	zářit
v	v	k7c6	v
dáli	dál	k1gFnSc6	dál
</s>
</p>
<p>
<s>
k	k	k7c3	k
ní	on	k3xPp3gFnSc7	on
veliký	veliký	k2eAgInSc1d1	veliký
Lenin	Lenin	k2eAgInSc1d1	Lenin
nás	my	k3xPp1nPc2	my
bouřemi	bouř	k1gFnPc7	bouř
veď	vést	k5eAaImRp2nS	vést
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
a	a	k8xC	a
věrnosti	věrnost	k1gFnSc3	věrnost
lidu	lid	k1gInSc2	lid
vždy	vždy	k6eAd1	vždy
nás	my	k3xPp1nPc2	my
učil	učít	k5eAaPmAgMnS	učít
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
nás	my	k3xPp1nPc4	my
učil	učit	k5eAaImAgMnS	učit
jít	jít	k5eAaImF	jít
v	v	k7c4	v
práci	práce	k1gFnSc4	práce
i	i	k8xC	i
hrdinství	hrdinství	k1gNnSc4	hrdinství
vpřed	vpřed	k6eAd1	vpřed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sláva	Sláva	k1gFnSc1	Sláva
ti	ty	k3xPp2nSc3	ty
otčino	otčina	k1gFnSc5	otčina
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc4	ten
naše	náš	k3xOp1gFnSc1	náš
svobodná	svobodný	k2eAgFnSc1d1	svobodná
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
štěstí	štěstí	k1gNnSc4	štěstí
národů	národ	k1gInPc2	národ
opora	opora	k1gFnSc1	opora
tkví	tkvět	k5eAaImIp3nS	tkvět
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
Ať	ať	k9	ať
prapor	prapor	k1gInSc1	prapor
sovětů	sovět	k1gInPc2	sovět
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
prapor	prapor	k1gInSc4	prapor
lidový	lidový	k2eAgInSc4d1	lidový
</s>
</p>
<p>
<s>
nás	my	k3xPp1nPc4	my
od	od	k7c2	od
vítězství	vítězství	k1gNnPc2	vítězství
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
verzi	verze	k1gFnSc6	verze
hymny	hymna	k1gFnSc2	hymna
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
se	se	k3xPyFc4	se
v	v	k7c6	v
refrénu	refrén	k1gInSc6	refrén
objevuje	objevovat	k5eAaImIp3nS	objevovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Slavsja	Slavsja	k1gFnSc1	Slavsja
<g/>
,	,	kIx,	,
Otěčestvo	Otěčestvo	k1gNnSc1	Otěčestvo
naše	náš	k3xOp1gInPc4	náš
svobodnoje	svobodnoj	k1gInPc4	svobodnoj
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Družby	družba	k1gFnPc1	družba
narodov	narodov	k1gInSc1	narodov
naďožnyj	naďožnyj	k1gMnSc1	naďožnyj
oplot	oplota	k1gFnPc2	oplota
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
Znamja	Znamja	k6eAd1	Znamja
sovjetskaje	sovjetskat	k5eAaImSgMnS	sovjetskat
<g/>
,	,	kIx,	,
znamja	znamja	k6eAd1	znamja
narodnaje	narodnat	k5eAaImSgMnS	narodnat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pusť	pustit	k5eAaPmRp2nS	pustit
od	od	k7c2	od
pobědy	poběda	k1gFnSc2	poběda
k	k	k7c3	k
pobědě	poběda	k1gFnSc3	poběda
veďot	veďot	k1gInSc1	veďot
<g/>
.	.	kIx.	.
<g/>
Tomu	ten	k3xDgNnSc3	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
umělecký	umělecký	k2eAgInSc1d1	umělecký
překlad	překlad	k1gInSc1	překlad
Jiřího	Jiří	k1gMnSc2	Jiří
Taufera	Taufer	k1gMnSc2	Taufer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
používá	používat	k5eAaImIp3nS	používat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
jako	jako	k8xC	jako
svou	svůj	k3xOyFgFnSc4	svůj
hymnu	hymna	k1gFnSc4	hymna
píseň	píseň	k1gFnSc4	píseň
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
melodie	melodie	k1gFnSc1	melodie
je	být	k5eAaImIp3nS	být
shodná	shodný	k2eAgFnSc1d1	shodná
s	s	k7c7	s
původní	původní	k2eAgFnSc7d1	původní
hymnou	hymna	k1gFnSc7	hymna
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Slova	slovo	k1gNnPc1	slovo
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
odlišná	odlišný	k2eAgFnSc1d1	odlišná
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
vlastenecky	vlastenecky	k6eAd1	vlastenecky
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
a	a	k8xC	a
oproštěná	oproštěný	k2eAgFnSc1d1	oproštěná
od	od	k7c2	od
komunistických	komunistický	k2eAgFnPc2d1	komunistická
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc7	jejich
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
Sergej	Sergej	k1gMnSc1	Sergej
Vladimirovič	Vladimirovič	k1gMnSc1	Vladimirovič
Michalkov	Michalkov	k1gInSc4	Michalkov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
</s>
</p>
<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
</s>
</p>
<p>
<s>
Hymna	hymna	k1gFnSc1	hymna
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
hymny	hymna	k1gFnSc2	hymna
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
</s>
</p>
<p>
<s>
MP3	MP3	k4	MP3
</s>
</p>
<p>
<s>
Midi	Midi	k6eAd1	Midi
</s>
</p>
<p>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
ruské	ruský	k2eAgFnSc2d1	ruská
hymny	hymna	k1gFnSc2	hymna
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
a	a	k8xC	a
anglicky	anglicky	k6eAd1	anglicky
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
nahrávek	nahrávka	k1gFnPc2	nahrávka
ruské	ruský	k2eAgFnSc2d1	ruská
a	a	k8xC	a
sovětské	sovětský	k2eAgFnSc2d1	sovětská
hymny	hymna	k1gFnSc2	hymna
</s>
</p>
