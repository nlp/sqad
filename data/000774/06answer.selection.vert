<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
se	se	k3xPyFc4	se
cyrilice	cyrilice	k1gFnSc1	cyrilice
používala	používat	k5eAaImAgFnS	používat
i	i	k9	i
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
také	také	k6eAd1	také
v	v	k7c6	v
Moldavsku	Moldavsko	k1gNnSc6	Moldavsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
pak	pak	k6eAd1	pak
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
latinkou	latinka	k1gFnSc7	latinka
<g/>
.	.	kIx.	.
</s>
