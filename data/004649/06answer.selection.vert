<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
podala	podat	k5eAaPmAgFnS	podat
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
EU	EU	kA	EU
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1996	[number]	k4	1996
<g/>
;	;	kIx,	;
vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
EU	EU	kA	EU
byl	být	k5eAaImAgInS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
priorit	priorita	k1gFnPc2	priorita
české	český	k2eAgFnSc2d1	Česká
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
s	s	k7c7	s
významnými	významný	k2eAgInPc7d1	významný
dopady	dopad	k1gInPc7	dopad
také	také	k9	také
na	na	k7c4	na
domácí	domácí	k2eAgFnSc4d1	domácí
politickou	politický	k2eAgFnSc4d1	politická
a	a	k8xC	a
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
situaci	situace	k1gFnSc4	situace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
nutnost	nutnost	k1gFnSc4	nutnost
postupné	postupný	k2eAgFnSc2d1	postupná
implementace	implementace	k1gFnSc2	implementace
acquis	acquis	k1gFnSc2	acquis
communautaire	communautair	k1gInSc5	communautair
do	do	k7c2	do
českého	český	k2eAgInSc2d1	český
právního	právní	k2eAgInSc2d1	právní
řádu	řád	k1gInSc2	řád
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
