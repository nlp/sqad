<s>
Papuchalk	Papuchalk	k1gInSc1
severní	severní	k2eAgFnSc2d1
</s>
<s>
Papuchalk	Papuchalk	k6eAd1
severní	severní	k2eAgMnSc1d1
Dospělý	dospělý	k2eAgMnSc1d1
pták	pták	k1gMnSc1
ve	v	k7c6
svatebním	svatební	k2eAgInSc6d1
šatě	šat	k1gInSc6
<g/>
,	,	kIx,
Skotsko	Skotsko	k1gNnSc1
Stupeň	stupeň	k1gInSc1
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
zranitelný	zranitelný	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
obratlovci	obratlovec	k1gMnPc1
(	(	kIx(
<g/>
Vertebrata	Vertebrat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
ptáci	pták	k1gMnPc1
(	(	kIx(
<g/>
Aves	Aves	k1gInSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
dlouhokřídlí	dlouhokřídlý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Charadriiformes	Charadriiformes	k1gMnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
alkovití	alkovití	k1gMnPc1
(	(	kIx(
<g/>
Alcidae	Alcidae	k1gInSc1
<g/>
)	)	kIx)
Podčeleď	podčeleď	k1gFnSc1
</s>
<s>
alky	alka	k1gFnPc1
(	(	kIx(
<g/>
Alcinae	Alcinae	k1gInSc1
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
papuchalk	papuchalk	k1gInSc1
(	(	kIx(
<g/>
Fratercula	Fratercula	k1gFnSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Fratercula	Fratercula	k1gFnSc1
arctica	arctica	k1gFnSc1
<g/>
(	(	kIx(
<g/>
Linné	Linné	k1gNnSc1
<g/>
,	,	kIx,
1758	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
papuchalka	papuchalka	k1gFnSc1
severníhooranžově	severníhooranžově	k6eAd1
<g/>
:	:	kIx,
hnízdiště	hnízdiště	k1gNnSc4
žlutě	žlutě	k6eAd1
<g/>
:	:	kIx,
zimoviště	zimoviště	k1gNnSc1
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
papuchalka	papuchalka	k1gFnSc1
severníhooranžově	severníhooranžově	k6eAd1
<g/>
:	:	kIx,
hnízdiště	hnízdiště	k1gNnSc4
žlutě	žlutě	k6eAd1
<g/>
:	:	kIx,
zimoviště	zimoviště	k1gNnSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Papuchalk	Papuchalk	k6eAd1
s	s	k7c7
kořistí	kořist	k1gFnSc7
</s>
<s>
Papuchalk	Papuchalk	k6eAd1
severní	severní	k2eAgFnSc1d1
(	(	kIx(
<g/>
Fratercula	Fratercula	k1gFnSc1
arctica	arctica	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
též	též	k9
pod	pod	k7c7
názvy	název	k1gInPc7
papuchalk	papuchalk	k6eAd1
ploskozobý	ploskozobý	k2eAgMnSc1d1
nebo	nebo	k8xC
bělobradý	bělobradý	k2eAgMnSc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
malý	malý	k2eAgMnSc1d1
mořský	mořský	k2eAgMnSc1d1
pták	pták	k1gMnSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
alkovitých	alkovití	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
svému	svůj	k3xOyFgInSc3
výrazně	výrazně	k6eAd1
zbarvenému	zbarvený	k2eAgInSc3d1
velkému	velký	k2eAgInSc3d1
zobáku	zobák	k1gInSc3
je	být	k5eAaImIp3nS
nazýván	nazývat	k5eAaImNgInS
též	též	k9
mořským	mořský	k2eAgMnSc7d1
papouškem	papoušek	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Papuchalk	Papuchalk	k1gMnSc1
severní	severní	k2eAgMnSc1d1
dorůstá	dorůstat	k5eAaImIp3nS
délky	délka	k1gFnSc2
26	#num#	k4
<g/>
–	–	k?
<g/>
29	#num#	k4
cm	cm	kA
a	a	k8xC
v	v	k7c6
rozpětí	rozpětí	k1gNnSc6
křídel	křídlo	k1gNnPc2
měří	měřit	k5eAaImIp3nS
47	#num#	k4
<g/>
–	–	k?
<g/>
63	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Samci	samec	k1gMnPc1
jsou	být	k5eAaImIp3nP
zpravidla	zpravidla	k6eAd1
větší	velký	k2eAgFnPc1d2
než	než	k8xS
samice	samice	k1gFnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
zbarvením	zbarvení	k1gNnSc7
se	se	k3xPyFc4
nijak	nijak	k6eAd1
viditelně	viditelně	k6eAd1
neliší	lišit	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svrchu	svrch	k1gInSc2
je	být	k5eAaImIp3nS
převážně	převážně	k6eAd1
černý	černý	k2eAgInSc1d1
a	a	k8xC
ze	z	k7c2
spodní	spodní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
bílý	bílý	k1gMnSc1
s	s	k7c7
šedými	šedý	k2eAgFnPc7d1
až	až	k8xS
bílými	bílý	k2eAgFnPc7d1
tvářemi	tvář	k1gFnPc7
a	a	k8xC
oranžovočervenými	oranžovočervený	k2eAgFnPc7d1
končetinami	končetina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zobák	zobák	k1gInSc1
je	být	k5eAaImIp3nS
velký	velký	k2eAgInSc1d1
3	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
trojúhelníkového	trojúhelníkový	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
<g/>
,	,	kIx,
během	během	k7c2
hnízdního	hnízdní	k2eAgNnSc2d1
období	období	k1gNnSc2
jasně	jasně	k6eAd1
oranžový	oranžový	k2eAgInSc1d1
<g/>
,	,	kIx,
modrý	modrý	k2eAgInSc1d1
a	a	k8xC
žlutý	žlutý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Papuchalk	Papuchalk	k1gInSc1
bělobradý	bělobradý	k2eAgInSc1d1
se	se	k3xPyFc4
velmi	velmi	k6eAd1
podobá	podobat	k5eAaImIp3nS
papuchalku	papuchalka	k1gFnSc4
černobradému	černobradý	k2eAgInSc3d1
(	(	kIx(
<g/>
Fratercula	Fraterculum	k1gNnSc2
corniculata	cornicule	k1gNnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
ním	on	k3xPp3gMnSc7
má	mít	k5eAaImIp3nS
odlišně	odlišně	k6eAd1
zbarvený	zbarvený	k2eAgInSc4d1
zobák	zobák	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
severní	severní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
Faerských	Faerský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
<g/>
,	,	kIx,
Islandu	Island	k1gInSc2
a	a	k8xC
na	na	k7c6
východě	východ	k1gInSc6
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
severně	severně	k6eAd1
přitom	přitom	k6eAd1
zasahuje	zasahovat	k5eAaImIp3nS
až	až	k9
po	po	k7c6
oblasti	oblast	k1gFnSc6
uvnitř	uvnitř	k7c2
Severního	severní	k2eAgInSc2d1
polárního	polární	k2eAgInSc2d1
kruhu	kruh	k1gInSc2
<g/>
,	,	kIx,
jižně	jižně	k6eAd1
pak	pak	k6eAd1
po	po	k7c6
Francii	Francie	k1gFnSc6
a	a	k8xC
Maine	Main	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zimní	zimní	k2eAgInPc1d1
měsíce	měsíc	k1gInPc1
tráví	trávit	k5eAaImIp3nP
na	na	k7c6
otevřeném	otevřený	k2eAgNnSc6d1
moři	moře	k1gNnSc6
daleko	daleko	k6eAd1
od	od	k7c2
pevniny	pevnina	k1gFnSc2
-	-	kIx~
v	v	k7c6
Evropě	Evropa	k1gFnSc6
během	během	k7c2
tohoto	tento	k3xDgNnSc2
období	období	k1gNnSc2
jižně	jižně	k6eAd1
zasahuje	zasahovat	k5eAaImIp3nS
až	až	k9
po	po	k7c4
Středozemní	středozemní	k2eAgNnSc4d1
moře	moře	k1gNnSc4
a	a	k8xC
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
zase	zase	k9
po	po	k7c4
Severní	severní	k2eAgFnSc4d1
Karolínu	Karolína	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Způsob	způsob	k1gInSc1
života	život	k1gInSc2
</s>
<s>
Potrava	potrava	k1gFnSc1
</s>
<s>
Za	za	k7c7
potravou	potrava	k1gFnSc7
se	se	k3xPyFc4
potápí	potápět	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
až	až	k9
do	do	k7c2
hloubky	hloubka	k1gFnSc2
70	#num#	k4
m	m	kA
<g/>
;	;	kIx,
pod	pod	k7c7
vodou	voda	k1gFnSc7
dokáže	dokázat	k5eAaPmIp3nS
vydržet	vydržet	k5eAaPmF
až	až	k9
5	#num#	k4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
častěji	často	k6eAd2
se	se	k3xPyFc4
potápí	potápět	k5eAaImIp3nS
do	do	k7c2
menších	malý	k2eAgFnPc2d2
hloubek	hloubka	k1gFnPc2
a	a	k8xC
pouze	pouze	k6eAd1
krátce	krátce	k6eAd1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
plavání	plavání	k1gNnSc3
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
svých	svůj	k3xOyFgFnPc2
silných	silný	k2eAgFnPc2d1
křídel	křídlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Končetiny	končetina	k1gFnPc1
s	s	k7c7
plovacími	plovací	k2eAgFnPc7d1
blánami	blána	k1gFnPc7
mu	on	k3xPp3gMnSc3
pak	pak	k6eAd1
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
kormidlo	kormidlo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
hnízdního	hnízdní	k2eAgNnSc2d1
období	období	k1gNnSc2
se	se	k3xPyFc4
přitom	přitom	k6eAd1
při	při	k7c6
hledání	hledání	k1gNnSc6
kořisti	kořist	k1gFnSc2
může	moct	k5eAaImIp3nS
od	od	k7c2
hnízdiště	hnízdiště	k1gNnSc2
vzdálit	vzdálit	k5eAaPmF
až	až	k9
100	#num#	k4
km	km	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Živí	živit	k5eAaImIp3nS
se	s	k7c7
především	především	k6eAd1
malými	malý	k2eAgInPc7d1
druhy	druh	k1gInPc7
ryb	ryba	k1gFnPc2
<g/>
,	,	kIx,
jakými	jaký	k3yRgInPc7,k3yIgInPc7,k3yQgInPc7
jsou	být	k5eAaImIp3nP
např.	např.	kA
sledi	sleď	k1gMnPc1
<g/>
,	,	kIx,
šproti	šprot	k1gMnPc1
nebo	nebo	k8xC
smáčky	smáčka	k1gFnPc1
<g/>
,	,	kIx,
požírá	požírat	k5eAaImIp3nS
též	též	k9
zooplankton	zooplankton	k1gInSc1
<g/>
,	,	kIx,
různé	různý	k2eAgMnPc4d1
korýše	korýš	k1gMnPc4
a	a	k8xC
měkkýše	měkkýš	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
přitom	přitom	k6eAd1
zvláštně	zvláštně	k6eAd1
přizpůsobenou	přizpůsobený	k2eAgFnSc4d1
horní	horní	k2eAgFnSc4d1
čelist	čelist	k1gFnSc4
zobáku	zobák	k1gInSc2
a	a	k8xC
jazyk	jazyk	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
má	mít	k5eAaImIp3nS
poseté	posetý	k2eAgNnSc1d1
spoustou	spousta	k1gFnSc7
drobných	drobný	k2eAgInPc2d1
trnů	trn	k1gInPc2
umožňujících	umožňující	k2eAgNnPc2d1
držení	držení	k1gNnPc2
i	i	k8xC
více	hodně	k6eAd2
než	než	k8xS
30	#num#	k4
rybek	rybka	k1gFnPc2
najednou	najednou	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hnízdění	hnízdění	k1gNnSc1
</s>
<s>
Fratercula	Fratercula	k1gFnSc1
arctica	arctica	k1gFnSc1
arctica	arctica	k1gFnSc1
</s>
<s>
Papuchalci	Papuchalec	k1gMnPc1
bělobradí	bělobradý	k2eAgMnPc1d1
pohlavně	pohlavně	k6eAd1
dospívají	dospívat	k5eAaImIp3nP
ve	v	k7c6
věku	věk	k1gInSc6
4	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
monogamní	monogamní	k2eAgMnPc1d1
a	a	k8xC
hnízdí	hnízdit	k5eAaImIp3nP
v	v	k7c6
koloniích	kolonie	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Využívají	využívat	k5eAaImIp3nP,k5eAaPmIp3nP
přitom	přitom	k6eAd1
převážně	převážně	k6eAd1
nor	nora	k1gFnPc2
vyhloubených	vyhloubený	k2eAgFnPc2d1
na	na	k7c6
travnatých	travnatý	k2eAgInPc6d1
svazích	svah	k1gInPc6
<g/>
,	,	kIx,
ale	ale	k8xC
hnízdí	hnízdit	k5eAaImIp3nS
též	též	k9
mezi	mezi	k7c7
skalami	skála	k1gFnPc7
nebo	nebo	k8xC
balvany	balvan	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
úpravě	úprava	k1gFnSc6
budoucího	budoucí	k2eAgNnSc2d1
hnízda	hnízdo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
později	pozdě	k6eAd2
vystýlají	vystýlat	k5eAaImIp3nP
trávou	tráva	k1gFnSc7
<g/>
,	,	kIx,
peřím	peří	k1gNnSc7
nebo	nebo	k8xC
mořskými	mořský	k2eAgFnPc7d1
řasami	řasa	k1gFnPc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
podílí	podílet	k5eAaImIp3nS
především	především	k9
samec	samec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
každé	každý	k3xTgFnSc6
snůšce	snůška	k1gFnSc6
je	být	k5eAaImIp3nS
jen	jen	k9
jedno	jeden	k4xCgNnSc1
vejce	vejce	k1gNnSc1
<g/>
,	,	kIx,
na	na	k7c6
jehož	jehož	k3xOyRp3gFnSc6
39	#num#	k4
<g/>
–	–	k?
<g/>
45	#num#	k4
dnů	den	k1gInPc2
dlouhé	dlouhý	k2eAgFnSc3d1
inkubaci	inkubace	k1gFnSc3
se	se	k3xPyFc4
podílí	podílet	k5eAaImIp3nS
oba	dva	k4xCgMnPc4
rodiče	rodič	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mláďata	mládě	k1gNnPc1
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
opeřena	opeřit	k5eAaPmNgFnS
asi	asi	k9
ve	v	k7c6
věku	věk	k1gInSc6
49	#num#	k4
dnů	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
věku	věk	k1gInSc6
opouští	opouštět	k5eAaImIp3nS
hnízdo	hnízdo	k1gNnSc1
a	a	k8xC
hned	hned	k6eAd1
se	se	k3xPyFc4
vydávají	vydávat	k5eAaPmIp3nP,k5eAaImIp3nP
do	do	k7c2
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Používání	používání	k1gNnSc1
nástrojů	nástroj	k1gInPc2
</s>
<s>
U	u	k7c2
papuchalků	papuchalka	k1gMnPc2
bylo	být	k5eAaImAgNnS
pozorováno	pozorovat	k5eAaImNgNnS
i	i	k9
používání	používání	k1gNnSc1
nástrojů	nástroj	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
větviček	větvička	k1gFnPc2
<g/>
,	,	kIx,
kterými	který	k3yRgMnPc7,k3yQgMnPc7,k3yIgMnPc7
se	se	k3xPyFc4
škrábali	škrábat	k5eAaImAgMnP
na	na	k7c6
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	on	k3xPp3gFnPc4
zřejmě	zřejmě	k6eAd1
trápili	trápit	k5eAaImAgMnP
paraziti	parazit	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
pozorování	pozorování	k1gNnSc3
došlo	dojít	k5eAaPmAgNnS
na	na	k7c6
dvou	dva	k4xCgNnPc6
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
dělilo	dělit	k5eAaImAgNnS
1700	#num#	k4
km	km	kA
-	-	kIx~
na	na	k7c6
ostrově	ostrov	k1gInSc6
Skomer	Skomra	k1gFnPc2
(	(	kIx(
<g/>
Wales	Wales	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
Grimsey	Grimse	k2eAgFnPc1d1
(	(	kIx(
<g/>
Island	Island	k1gInSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Predátoři	predátor	k1gMnPc1
</s>
<s>
Mezi	mezi	k7c4
predátory	predátor	k1gMnPc4
papuchalka	papuchalka	k1gFnSc1
bělobradého	bělobradý	k2eAgInSc2d1
patří	patřit	k5eAaImIp3nS
především	především	k9
racek	racek	k1gMnSc1
mořský	mořský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Larus	Larus	k1gMnSc1
marinus	marinus	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
chaluha	chaluha	k1gFnSc1
velká	velká	k1gFnSc1
(	(	kIx(
<g/>
Stercorarius	Stercorarius	k1gMnSc1
skua	skua	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
krysy	krysa	k1gFnPc1
<g/>
,	,	kIx,
kočky	kočka	k1gFnPc1
<g/>
,	,	kIx,
psi	pes	k1gMnPc1
nebo	nebo	k8xC
lišky	liška	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Menší	malý	k2eAgMnPc1d2
rackové	racek	k1gMnPc1
<g/>
,	,	kIx,
jakým	jaký	k3yQgMnSc7,k3yRgMnSc7,k3yIgMnSc7
je	být	k5eAaImIp3nS
např.	např.	kA
racek	racek	k1gMnSc1
stříbřitý	stříbřitý	k2eAgMnSc1d1
(	(	kIx(
<g/>
L.	L.	kA
argentatus	argentatus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pak	pak	k6eAd1
často	často	k6eAd1
plení	plenit	k5eAaImIp3nP
hnízda	hnízdo	k1gNnPc1
a	a	k8xC
dospělým	dospělý	k2eAgMnPc3d1
ptákům	pták	k1gMnPc3
kradou	krást	k5eAaImIp3nP
i	i	k8xC
již	již	k6eAd1
ulovenou	ulovený	k2eAgFnSc4d1
kořist	kořist	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Početnost	početnost	k1gFnSc1
</s>
<s>
Podle	podle	k7c2
Mezinárodního	mezinárodní	k2eAgInSc2d1
svazu	svaz	k1gInSc2
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
(	(	kIx(
<g/>
IUCN	IUCN	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
papuchalků	papuchalek	k1gInPc2
12-14	12-14	k4
milionů	milion	k4xCgInPc2
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Populace	populace	k1gFnSc1
těchto	tento	k3xDgMnPc2
ptáků	pták	k1gMnPc2
byla	být	k5eAaImAgFnS
lovem	lov	k1gInSc7
a	a	k8xC
vybíráním	vybírání	k1gNnSc7
hnízd	hnízdo	k1gNnPc2
během	během	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
výrazně	výrazně	k6eAd1
redukována	redukovat	k5eAaBmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
jsou	být	k5eAaImIp3nP
papuchalkové	papuchalková	k1gFnPc1
v	v	k7c6
některých	některý	k3yIgFnPc6
oblastech	oblast	k1gFnPc6
(	(	kIx(
<g/>
na	na	k7c6
Islandu	Island	k1gInSc6
a	a	k8xC
Faerských	Faerský	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
<g/>
)	)	kIx)
lovným	lovný	k2eAgInSc7d1
druhem	druh	k1gInSc7
dodnes	dodnes	k6eAd1
<g/>
,	,	kIx,
mnohem	mnohem	k6eAd1
vážněji	vážně	k6eAd2
je	být	k5eAaImIp3nS
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
ohrožují	ohrožovat	k5eAaImIp3nP
především	především	k6eAd1
noví	nový	k2eAgMnPc1d1
<g/>
,	,	kIx,
zavlečení	zavlečený	k2eAgMnPc1d1
predátoři	predátor	k1gMnPc1
(	(	kIx(
<g/>
kočky	kočka	k1gFnPc1
<g/>
,	,	kIx,
psi	pes	k1gMnPc1
<g/>
,	,	kIx,
krysy	krysa	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
znečišťování	znečišťování	k1gNnSc4
moří	mořit	k5eAaImIp3nP
jedovatými	jedovatý	k2eAgFnPc7d1
látkami	látka	k1gFnPc7
<g/>
,	,	kIx,
rybolov	rybolov	k1gInSc4
a	a	k8xC
klimatické	klimatický	k2eAgFnPc4d1
změny	změna	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
vedou	vést	k5eAaImIp3nP
k	k	k7c3
výraznému	výrazný	k2eAgInSc3d1
nedostatku	nedostatek	k1gInSc3
potravy	potrava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2021.1	2021.1	k4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
KHOLOVÁ	Kholová	k1gFnSc1
<g/>
,	,	kIx,
Helena	Helena	k1gFnSc1
(	(	kIx(
<g/>
autorka	autorka	k1gFnSc1
českého	český	k2eAgInSc2d1
překladu	překlad	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ptáci	pták	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Euromedia	Euromedium	k1gNnPc1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
k.	k.	k?
s.	s.	k?
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9788024222356	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
241	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
The	The	k1gFnSc1
Birds	Birds	k1gInSc1
of	of	k?
the	the	k?
Western	western	k1gInSc1
Palearctic	Palearctice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
OUP	OUP	kA
<g/>
.	.	kIx.
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
854099	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
DUNN	DUNN	kA
<g/>
,	,	kIx,
Euan	Euan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Puffins	Puffins	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Bloomsbury	Bloomsbur	k1gInPc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
4729	#num#	k4
<g/>
-	-	kIx~
<g/>
6520	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Lilliendahl	Lilliendahl	k1gMnSc1
<g/>
,	,	kIx,
K.	K.	kA
<g/>
;	;	kIx,
Solmundsson	Solmundsson	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
;	;	kIx,
Gudmundsson	Gudmundsson	k1gMnSc1
<g/>
,	,	kIx,
G.A.	G.A.	k1gMnSc1
&	&	k?
Taylor	Taylor	k1gMnSc1
<g/>
,	,	kIx,
L.	L.	kA
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Can	Can	k1gMnSc1
surveillance	surveillance	k1gFnSc2
radar	radar	k1gInSc1
be	be	k?
used	used	k1gInSc1
to	ten	k3xDgNnSc1
monitor	monitor	k1gInSc1
the	the	k?
foraging	foraging	k1gInSc1
distribution	distribution	k1gInSc1
of	of	k?
colonially	colonialla	k1gFnSc2
breeding	breeding	k1gInSc4
alcids	alcids	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Condor	Condor	k1gInSc1
105	#num#	k4
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
145	#num#	k4
<g/>
–	–	k?
<g/>
150	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Redakce	redakce	k1gFnSc2
David	David	k1gMnSc1
Burnie	Burnie	k1gFnSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
<g/>
;	;	kIx,
překlad	překlad	k1gInSc1
Jiří	Jiří	k1gMnSc1
Šmaha	Šmaha	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Euromedia	Euromedium	k1gNnPc1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
k.	k.	k?
s.	s.	k?
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
8024208628	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
303	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
STREET	STREET	kA
<g/>
,	,	kIx,
R.	R.	kA
<g/>
;	;	kIx,
EMILY	Emil	k1gMnPc4
<g/>
,	,	kIx,
A.	A.	kA
Fratercula	Fratercula	k1gFnSc1
arctica	arctica	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Animal	animal	k1gMnSc1
Diversity	Diversit	k1gInPc1
Web	web	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ehrlich	Ehrlich	k1gInSc1
<g/>
,	,	kIx,
P.	P.	kA
<g/>
;	;	kIx,
Dobkin	Dobkin	k1gMnSc1
<g/>
,	,	kIx,
D.	D.	kA
&	&	k?
Wheye	Wheye	k1gInSc1
<g/>
,	,	kIx,
D.	D.	kA
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Atlantic	Atlantice	k1gFnPc2
Puffin	Puffina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Birder	Birder	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Handbook	handbook	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
Field	Field	k1gInSc1
Guide	Guid	k1gInSc5
to	ten	k3xDgNnSc4
The	The	k1gFnSc1
Natural	Natural	k?
History	Histor	k1gInPc1
of	of	k?
North	North	k1gInSc1
American	American	k1gMnSc1
Birds	Birds	k1gInSc1
<g/>
:	:	kIx,
207	#num#	k4
<g/>
,	,	kIx,
209	#num#	k4
<g/>
-	-	kIx~
<g/>
214	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
FAYET	FAYET	kA
<g/>
,	,	kIx,
Annette	Annett	k1gInSc5
L.	L.	kA
<g/>
;	;	kIx,
HANSEN	HANSEN	kA
<g/>
,	,	kIx,
Erpur	Erpur	k1gMnSc1
Snæ	Snæ	k1gMnSc1
<g/>
;	;	kIx,
BIRO	BIRO	kA
<g/>
,	,	kIx,
Dora	Dora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evidence	evidence	k1gFnSc1
of	of	k?
tool	tool	k1gInSc1
use	usus	k1gInSc5
in	in	k?
a	a	k8xC
seabird	seabird	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proceedings	Proceedings	k1gInSc1
of	of	k?
the	the	k?
National	National	k1gMnSc1
Academy	Academa	k1gFnSc2
of	of	k?
Sciences	Sciences	k1gMnSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
201918060	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
27	#num#	k4
<g/>
-	-	kIx~
<g/>
8424	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.107	10.107	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
pnas	pnas	k6eAd1
<g/>
.1918060117	.1918060117	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
IUCN	IUCN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fratercula	Fratercula	k1gFnSc1
arctica	arctica	k1gFnSc1
<g/>
:	:	kIx,
BirdLife	BirdLif	k1gMnSc5
International	International	k1gMnSc5
<g/>
:	:	kIx,
The	The	k1gMnPc2
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2018	#num#	k4
<g/>
:	:	kIx,
e.T	e.T	k?
<g/>
22694927	#num#	k4
<g/>
A	a	k8xC
<g/>
132581443	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.230	10.230	k4
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
iucn	iucna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
.2018	.2018	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
rlts	rltsa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
t	t	k?
<g/>
22694927	#num#	k4
<g/>
a	a	k8xC
<g/>
132581443	#num#	k4
<g/>
.	.	kIx.
<g/>
en	en	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Type	typ	k1gInSc5
<g/>
:	:	kIx,
dataset	dataset	k1gInSc4
</s>
<s>
DOI	DOI	kA
<g/>
:	:	kIx,
10.230	10.230	k4
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
IUCN	IUCN	kA
<g/>
.	.	kIx.
<g/>
UK	UK	kA
<g/>
.2018	.2018	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
RLTS	RLTS	kA
<g/>
.	.	kIx.
<g/>
T	T	kA
<g/>
22694927	#num#	k4
<g/>
A	a	k9
<g/>
132581443	#num#	k4
<g/>
.	.	kIx.
<g/>
en	en	k?
<g/>
.	.	kIx.
↑	↑	k?
Mitchell	Mitchell	k1gInSc1
<g/>
,	,	kIx,
P.	P.	kA
<g/>
I.	I.	kA
<g/>
;	;	kIx,
Newton	Newton	k1gMnSc1
<g/>
,	,	kIx,
S.	S.	kA
<g/>
F.	F.	kA
<g/>
;	;	kIx,
Ratcliffe	Ratcliff	k1gMnSc5
<g/>
,	,	kIx,
N.	N.	kA
<g/>
;	;	kIx,
Dunn	Dunn	k1gMnSc1
<g/>
,	,	kIx,
T.E.	T.E.	k1gMnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Seabird	Seabird	k1gInSc1
Populations	Populations	k1gInSc1
of	of	k?
Britain	Britain	k1gInSc1
and	and	k?
Ireland	Ireland	k1gInSc1
<g/>
:	:	kIx,
Results	Results	k1gInSc1
of	of	k?
the	the	k?
Seabird	Seabird	k1gMnSc1
2000	#num#	k4
Census	census	k1gInSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
T.	T.	kA
&	&	k?
A.D.	A.D.	k1gMnSc1
Poyser	Poyser	k1gMnSc1
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0-7136-6901-2	0-7136-6901-2	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
papuchalk	papuchalk	k1gMnSc1
severní	severní	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
papuchalk	papuchalk	k1gMnSc1
severní	severní	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Fratercula	Fratercul	k1gMnSc2
arctica	arcticus	k1gMnSc2
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Ptáci	pták	k1gMnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4263986-4	4263986-4	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85009203	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85009203	#num#	k4
</s>
