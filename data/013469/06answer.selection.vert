<s>
Za	za	k7c7
potravou	potrava	k1gFnSc7
se	se	k3xPyFc4
potápí	potápět	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
až	až	k9
do	do	k7c2
hloubky	hloubka	k1gFnSc2
70	#num#	k4
m	m	kA
<g/>
;	;	kIx,
pod	pod	k7c7
vodou	voda	k1gFnSc7
dokáže	dokázat	k5eAaPmIp3nS
vydržet	vydržet	k5eAaPmF
až	až	k9
5	#num#	k4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
častěji	často	k6eAd2
se	se	k3xPyFc4
potápí	potápět	k5eAaImIp3nS
do	do	k7c2
menších	malý	k2eAgFnPc2d2
hloubek	hloubka	k1gFnPc2
a	a	k8xC
pouze	pouze	k6eAd1
krátce	krátce	k6eAd1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>