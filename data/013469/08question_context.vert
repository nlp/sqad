<s>
Papuchalk	Papuchalk	k1gMnSc1
severní	severní	k2eAgMnSc1d1
dorůstá	dorůstat	k5eAaImIp3nS
délky	délka	k1gFnSc2
26	#num#	k4
<g/>
–	–	k?
<g/>
29	#num#	k4
cm	cm	kA
a	a	k8xC
v	v	k7c6
rozpětí	rozpětí	k1gNnSc6
křídel	křídlo	k1gNnPc2
měří	měřit	k5eAaImIp3nS
47	#num#	k4
<g/>
–	–	k?
<g/>
63	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Samci	samec	k1gMnPc1
jsou	být	k5eAaImIp3nP
zpravidla	zpravidla	k6eAd1
větší	velký	k2eAgFnPc1d2
než	než	k8xS
samice	samice	k1gFnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
zbarvením	zbarvení	k1gNnSc7
se	se	k3xPyFc4
nijak	nijak	k6eAd1
viditelně	viditelně	k6eAd1
neliší	lišit	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>