<p>
<s>
Jeremy	Jerem	k1gInPc1	Jerem
Clarkson	Clarksona	k1gFnPc2	Clarksona
(	(	kIx(	(
<g/>
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Jeremy	Jerema	k1gFnSc2	Jerema
Charles	Charles	k1gMnSc1	Charles
Robert	Robert	k1gMnSc1	Robert
Clarkson	Clarkson	k1gMnSc1	Clarkson
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
anglický	anglický	k2eAgMnSc1d1	anglický
hlasatel	hlasatel	k1gMnSc1	hlasatel
<g/>
,	,	kIx,	,
žurnalista	žurnalista	k1gMnSc1	žurnalista
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
motorismus	motorismus	k1gInSc4	motorismus
<g/>
.	.	kIx.	.
</s>
<s>
Znám	znám	k2eAgMnSc1d1	znám
je	být	k5eAaImIp3nS	být
především	především	k9	především
jako	jako	k9	jako
bývalý	bývalý	k2eAgMnSc1d1	bývalý
moderátor	moderátor	k1gMnSc1	moderátor
pořadu	pořad	k1gInSc2	pořad
Top	topit	k5eAaImRp2nS	topit
Gear	Gear	k1gMnSc1	Gear
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
moderoval	moderovat	k5eAaBmAgMnS	moderovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Richardem	Richard	k1gMnSc7	Richard
Hammondem	Hammond	k1gMnSc7	Hammond
a	a	k8xC	a
Jamesem	James	k1gMnSc7	James
Mayem	May	k1gMnSc7	May
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
píše	psát	k5eAaImIp3nS	psát
články	článek	k1gInPc4	článek
pro	pro	k7c4	pro
The	The	k1gFnSc4	The
Sunday	Sundaa	k1gFnSc2	Sundaa
Times	Times	k1gInSc1	Times
a	a	k8xC	a
The	The	k1gFnSc1	The
Sun	Sun	kA	Sun
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
povědomí	povědomí	k1gNnSc2	povědomí
diváků	divák	k1gMnPc2	divák
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
jako	jako	k9	jako
moderátor	moderátor	k1gMnSc1	moderátor
již	již	k6eAd1	již
v	v	k7c6	v
původním	původní	k2eAgNnSc6d1	původní
(	(	kIx(	(
<g/>
starém	staré	k1gNnSc6	staré
<g/>
)	)	kIx)	)
Top	topit	k5eAaImRp2nS	topit
Gearu	Geara	k1gFnSc4	Geara
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
stávat	stávat	k5eAaImF	stávat
veřejně	veřejně	k6eAd1	veřejně
uznávanou	uznávaný	k2eAgFnSc7d1	uznávaná
osobností	osobnost	k1gFnSc7	osobnost
<g/>
,	,	kIx,	,
pravidelně	pravidelně	k6eAd1	pravidelně
se	se	k3xPyFc4	se
objevoval	objevovat	k5eAaImAgInS	objevovat
na	na	k7c6	na
britských	britský	k2eAgFnPc6d1	britská
stanicích	stanice	k1gFnPc6	stanice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
moderoval	moderovat	k5eAaBmAgMnS	moderovat
vlastní	vlastní	k2eAgInPc4d1	vlastní
pořady	pořad	k1gInPc4	pořad
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
zván	zvát	k5eAaImNgMnS	zvát
jako	jako	k8xC	jako
host	host	k1gMnSc1	host
do	do	k7c2	do
jiných	jiný	k2eAgInPc2d1	jiný
pořadů	pořad	k1gInPc2	pořad
<g/>
.	.	kIx.	.
</s>
<s>
Clarkson	Clarkson	k1gInSc1	Clarkson
není	být	k5eNaImIp3nS	být
pouze	pouze	k6eAd1	pouze
producentem	producent	k1gMnSc7	producent
pořadů	pořad	k1gInPc2	pořad
o	o	k7c6	o
motorismu	motorismus	k1gInSc6	motorismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
a	a	k8xC	a
strojírenství	strojírenství	k1gNnSc6	strojírenství
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
uváděl	uvádět	k5eAaImAgInS	uvádět
i	i	k9	i
vlastní	vlastní	k2eAgNnSc4d1	vlastní
show	show	k1gNnSc4	show
Clarkson	Clarksona	k1gFnPc2	Clarksona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
humorný	humorný	k2eAgInSc1d1	humorný
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
politicky	politicky	k6eAd1	politicky
nekorektní	korektní	k2eNgInSc4d1	nekorektní
styl	styl	k1gInSc4	styl
psaní	psaní	k1gNnSc2	psaní
a	a	k8xC	a
moderování	moderování	k1gNnSc2	moderování
často	často	k6eAd1	často
vzbuzuje	vzbuzovat	k5eAaImIp3nS	vzbuzovat
u	u	k7c2	u
veřejnosti	veřejnost	k1gFnSc2	veřejnost
rozporuplné	rozporuplný	k2eAgFnSc2d1	rozporuplná
reakce	reakce	k1gFnSc2	reakce
a	a	k8xC	a
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
médii	médium	k1gNnPc7	médium
<g/>
,	,	kIx,	,
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
různými	různý	k2eAgFnPc7d1	různá
skupinami	skupina	k1gFnPc7	skupina
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
veřejností	veřejnost	k1gFnSc7	veřejnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Clarkson	Clarkson	k1gMnSc1	Clarkson
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Doncasteru	Doncaster	k1gInSc6	Doncaster
cestovnímu	cestovní	k2eAgMnSc3d1	cestovní
prodavači	prodavač	k1gMnSc3	prodavač
Edwardu	Edward	k1gMnSc3	Edward
</s>
</p>
<p>
<s>
Grenvillovi	Grenvill	k1gMnSc3	Grenvill
Clarksonovi	Clarkson	k1gMnSc3	Clarkson
a	a	k8xC	a
učitelce	učitelka	k1gFnSc3	učitelka
Shirley	Shirle	k2eAgFnPc1d1	Shirle
Gabrielle	Gabrielle	k1gFnPc1	Gabrielle
Ward	Warda	k1gFnPc2	Warda
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
</s>
</p>
<p>
<s>
prodávali	prodávat	k5eAaImAgMnP	prodávat
návleky	návlek	k1gInPc4	návlek
na	na	k7c4	na
konvice	konvice	k1gFnPc4	konvice
<g/>
,	,	kIx,	,
dali	dát	k5eAaPmAgMnP	dát
syna	syn	k1gMnSc4	syn
na	na	k7c4	na
soukromou	soukromý	k2eAgFnSc4d1	soukromá
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nevěděli	vědět	k5eNaImAgMnP	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
</s>
</p>
<p>
<s>
zaplatí	zaplatit	k5eAaPmIp3nS	zaplatit
všechny	všechen	k3xTgInPc4	všechen
výdaje	výdaj	k1gInPc4	výdaj
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Clarksonovi	Clarkson	k1gMnSc3	Clarkson
bylo	být	k5eAaImAgNnS	být
13	[number]	k4	13
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
</s>
</p>
<p>
<s>
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
dva	dva	k4xCgMnPc4	dva
medvídky	medvídek	k1gMnPc4	medvídek
Paddington	Paddington	k1gInSc4	Paddington
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
dárek	dárek	k1gInSc1	dárek
pro	pro	k7c4	pro
svoje	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
tak	tak	k9	tak
</s>
</p>
<p>
<s>
populárními	populární	k2eAgInPc7d1	populární
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gFnPc4	on
matka	matka	k1gFnSc1	matka
začala	začít	k5eAaPmAgFnS	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
ve	v	k7c6	v
větším	veliký	k2eAgMnSc6d2	veliký
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
tak	tak	k6eAd1	tak
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
její	její	k3xOp3gMnSc1	její
syn	syn	k1gMnSc1	syn
mohl	moct	k5eAaImAgMnS	moct
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
Hill	Hill	k1gMnSc1	Hill
House	house	k1gNnSc4	house
School	Schoola	k1gFnPc2	Schoola
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Repton	Repton	k1gInSc1	Repton
School	Schoola	k1gFnPc2	Schoola
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
Adrianem	Adrian	k1gMnSc7	Adrian
Neweyem	Newey	k1gMnSc7	Newey
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgMnS	stát
konstruktérem	konstruktér	k1gMnSc7	konstruktér
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Repton	Repton	k1gInSc4	Repton
</s>
</p>
<p>
<s>
School	School	k1gInSc1	School
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
vyhozen	vyhodit	k5eAaPmNgInS	vyhodit
za	za	k7c4	za
"	"	kIx"	"
<g/>
pití	pití	k1gNnSc4	pití
<g/>
,	,	kIx,	,
kouření	kouření	k1gNnSc4	kouření
a	a	k8xC	a
darebáctví	darebáctví	k1gNnSc4	darebáctví
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Clarksonovou	Clarksonová	k1gFnSc4	Clarksonová
první	první	k4xOgFnSc6	první
ženou	žena	k1gFnSc7	žena
byla	být	k5eAaImAgFnS	být
Alexandra	Alexandra	k1gFnSc1	Alexandra
James	James	k1gMnSc1	James
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
Hall	Hall	k1gInSc1	Hall
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
</s>
</p>
<p>
<s>
květnu	květen	k1gInSc6	květen
1993	[number]	k4	1993
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgInS	vzít
svoji	svůj	k3xOyFgFnSc4	svůj
manažerku	manažerka	k1gFnSc4	manažerka
Frances	Frances	k1gMnSc1	Frances
Cain	Cain	k1gMnSc1	Cain
(	(	kIx(	(
<g/>
dcera	dcera	k1gFnSc1	dcera
držitele	držitel	k1gMnSc2	držitel
Viktoriina	Viktoriin	k2eAgInSc2d1	Viktoriin
</s>
</p>
<p>
<s>
kříže	kříž	k1gInPc1	kříž
Roberta	Robert	k1gMnSc2	Robert
Henryho	Henry	k1gMnSc2	Henry
Caina	Cain	k1gMnSc2	Cain
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
žili	žít	k5eAaImAgMnP	žít
v	v	k7c4	v
Chipping	Chipping	k1gInSc4	Chipping
Noronu	Noron	k1gInSc2	Noron
<g/>
,	,	kIx,	,
v	v	k7c6	v
Cotswoldsu	Cotswolds	k1gInSc6	Cotswolds
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
společně	společně	k6eAd1	společně
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
třemi	tři	k4xCgFnPc7	tři
dětmi	dítě	k1gFnPc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jeremy	Jerema	k1gFnPc4	Jerema
je	být	k5eAaImIp3nS	být
také	také	k9	také
členem	člen	k1gInSc7	člen
Chipping	Chipping	k1gInSc1	Chipping
Norton	Norton	k1gInSc1	Norton
set	set	k1gInSc4	set
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
pro	pro	k7c4	pro
kupování	kupování	k1gNnSc4	kupování
aut	auto	k1gNnPc2	auto
jako	jako	k8xC	jako
dárků	dárek	k1gInPc2	dárek
<g/>
,	,	kIx,	,
k	k	k7c3	k
Vánocům	Vánoce	k1gFnPc3	Vánoce
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
koupil	koupit	k5eAaPmAgMnS	koupit
své	svůj	k3xOyFgNnSc4	svůj
druhé	druhý	k4xOgNnSc4	druhý
ženě	žena	k1gFnSc3	žena
Mercedes-Benz	Mercedes-Benz	k1gInSc1	Mercedes-Benz
600	[number]	k4	600
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2010	[number]	k4	2010
si	se	k3xPyFc3	se
Clarkson	Clarkson	k1gMnSc1	Clarkson
nechal	nechat	k5eAaPmAgMnS	nechat
udělit	udělit	k5eAaPmF	udělit
soudní	soudní	k2eAgInSc4d1	soudní
příkaz	příkaz	k1gInSc4	příkaz
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gFnSc3	jeho
první	první	k4xOgFnSc3	první
manželce	manželka	k1gFnSc3	manželka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
tím	ten	k3xDgNnSc7	ten
zajistil	zajistit	k5eAaPmAgMnS	zajistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebude	být	k5eNaImBp3nS	být
publikovat	publikovat	k5eAaBmF	publikovat
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
tvrzení	tvrzení	k1gNnSc2	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc1	jejich
milostný	milostný	k2eAgInSc1d1	milostný
poměr	poměr	k1gInSc1	poměr
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
i	i	k9	i
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
druhém	druhý	k4xOgNnSc6	druhý
manželství	manželství	k1gNnSc6	manželství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2011	[number]	k4	2011
příkaz	příkaz	k1gInSc1	příkaz
dobrovolně	dobrovolně	k6eAd1	dobrovolně
zrušil	zrušit	k5eAaPmAgInS	zrušit
s	s	k7c7	s
komentářem	komentář	k1gInSc7	komentář
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Soudní	soudní	k2eAgInPc1d1	soudní
příkazy	příkaz	k1gInPc1	příkaz
nefungují	fungovat	k5eNaImIp3nP	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
Můžete	moct	k5eAaImIp2nP	moct
přijít	přijít	k5eAaPmF	přijít
se	s	k7c7	s
soudním	soudní	k2eAgInSc7d1	soudní
příkazem	příkaz	k1gInSc7	příkaz
proti	proti	k7c3	proti
někomu	někdo	k3yInSc3	někdo
nebo	nebo	k8xC	nebo
</s>
</p>
<p>
<s>
nějaké	nějaký	k3yIgFnSc3	nějaký
organizaci	organizace	k1gFnSc3	organizace
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
okamžitě	okamžitě	k6eAd1	okamžitě
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
novinky	novinka	k1gFnPc4	novinka
o	o	k7c6	o
příkazu	příkaz	k1gInSc6	příkaz
a	a	k8xC	a
příběhy	příběh	k1gInPc4	příběh
za	za	k7c4	za
</s>
</p>
<p>
<s>
příkazem	příkaz	k1gInSc7	příkaz
jsou	být	k5eAaImIp3nP	být
volně	volně	k6eAd1	volně
<g/>
,	,	kIx,	,
legálně	legálně	k6eAd1	legálně
šířeny	šířen	k2eAgFnPc1d1	šířena
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
a	a	k8xC	a
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
bezvýznamné	bezvýznamný	k2eAgNnSc1d1	bezvýznamné
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
epizodě	epizoda	k1gFnSc6	epizoda
BBC	BBC	kA	BBC
seriálu	seriál	k1gInSc2	seriál
Who	Who	k1gFnSc2	Who
Do	do	k7c2	do
You	You	k1gFnSc2	You
Think	Think	k1gMnSc1	Think
You	You	k1gMnSc1	You
Are	ar	k1gInSc5	ar
<g/>
?	?	kIx.	?
</s>
<s>
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Clarskon	Clarskon	k1gInSc1	Clarskon
zjišťoval	zjišťovat	k5eAaImAgInS	zjišťovat
svůj	svůj	k3xOyFgInSc4	svůj
rodokmen	rodokmen	k1gInSc4	rodokmen
<g/>
,	,	kIx,	,
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
</s>
</p>
<p>
<s>
pra-pra-pra-pradědeček	prarararadědeček	k1gInSc4	pra-pra-pra-pradědeček
byl	být	k5eAaImAgMnS	být
John	John	k1gMnSc1	John
Kilner	Kilner	k1gMnSc1	Kilner
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
Kilnerovu	Kilnerův	k2eAgFnSc4d1	Kilnerův
sklenici	sklenice	k1gFnSc4	sklenice
(	(	kIx(	(
<g/>
skleněná	skleněný	k2eAgFnSc1d1	skleněná
nádoba	nádoba	k1gFnSc1	nádoba
se	s	k7c7	s
silikonovým	silikonový	k2eAgNnSc7d1	silikonové
těsněním	těsnění	k1gNnSc7	těsnění
víčka	víčko	k1gNnSc2	víčko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Psaní	psaní	k1gNnSc1	psaní
==	==	k?	==
</s>
</p>
<p>
<s>
Clarksonova	Clarksonův	k2eAgFnSc1d1	Clarksonova
první	první	k4xOgFnSc1	první
práce	práce	k1gFnSc1	práce
byla	být	k5eAaImAgFnS	být
cestovní	cestovní	k2eAgFnSc1d1	cestovní
prodavač	prodavač	k1gMnSc1	prodavač
pro	pro	k7c4	pro
živnost	živnost	k1gFnSc4	živnost
</s>
</p>
<p>
<s>
jeho	jeho	k3xOp3gMnPc2	jeho
rodičů	rodič	k1gMnPc2	rodič
–	–	k?	–
prodával	prodávat	k5eAaImAgInS	prodávat
hračky	hračka	k1gFnSc2	hračka
Paddington	Paddington	k1gInSc1	Paddington
Bear	Bear	k1gInSc1	Bear
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
žurnalistou	žurnalista	k1gMnSc7	žurnalista
u	u	k7c2	u
</s>
</p>
<p>
<s>
Rotherham	Rotherham	k1gInSc1	Rotherham
Advertiser	Advertisra	k1gFnPc2	Advertisra
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
také	také	k9	také
psal	psát	k5eAaImAgInS	psát
pro	pro	k7c4	pro
Rochdale	Rochdala	k1gFnSc3	Rochdala
Observer	Observer	k1gInSc1	Observer
<g/>
,	,	kIx,	,
Wolverhampton	Wolverhampton	k1gInSc1	Wolverhampton
</s>
</p>
<p>
<s>
Express	express	k1gInSc1	express
and	and	k?	and
Star	Star	kA	Star
<g/>
,	,	kIx,	,
Lincolnshire	Lincolnshir	k1gMnSc5	Lincolnshir
Life	Lifus	k1gMnSc5	Lifus
a	a	k8xC	a
Associated	Associated	k1gMnSc1	Associated
Kent	Kent	k2eAgInSc4d1	Kent
Newspapers	Newspapers	k1gInSc4	Newspapers
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
založil	založit	k5eAaPmAgInS	založit
Motoring	Motoring	k1gInSc1	Motoring
Press	Press	k1gInSc4	Press
Agency	Agenca	k1gFnSc2	Agenca
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
společně	společně	k6eAd1	společně
s	s	k7c7	s
Jonathanem	Jonathan	k1gMnSc7	Jonathan
Gillem	Gill	k1gMnSc7	Gill
(	(	kIx(	(
<g/>
specialistou	specialista	k1gMnSc7	specialista
na	na	k7c4	na
motorismus	motorismus	k1gInSc4	motorismus
<g/>
)	)	kIx)	)
dělali	dělat	k5eAaImAgMnP	dělat
silniční	silniční	k2eAgFnSc4d1	silniční
</s>
</p>
<p>
<s>
testy	test	k1gInPc1	test
vozidel	vozidlo	k1gNnPc2	vozidlo
pro	pro	k7c4	pro
místní	místní	k2eAgFnPc4d1	místní
noviny	novina	k1gFnPc4	novina
a	a	k8xC	a
automobilové	automobilový	k2eAgInPc4d1	automobilový
magazíny	magazín	k1gInPc4	magazín
<g/>
.	.	kIx.	.
</s>
<s>
Posílal	posílat	k5eAaImAgInS	posílat
také	také	k9	také
texty	text	k1gInPc4	text
do	do	k7c2	do
</s>
</p>
<p>
<s>
publikací	publikace	k1gFnPc2	publikace
jako	jako	k8xS	jako
Performance	performance	k1gFnSc2	performance
Car	car	k1gMnSc1	car
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelně	pravidelně	k6eAd1	pravidelně
psával	psávat	k5eAaImAgMnS	psávat
do	do	k7c2	do
magazínu	magazín	k1gInSc2	magazín
Top	topit	k5eAaImRp2nS	topit
Gear	Gear	k1gInSc1	Gear
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Clarkson	Clarkson	k1gInSc1	Clarkson
psal	psát	k5eAaImAgInS	psát
také	také	k9	také
pravidelně	pravidelně	k6eAd1	pravidelně
články	článek	k1gInPc1	článek
do	do	k7c2	do
bulvárních	bulvární	k2eAgFnPc2d1	bulvární
novin	novina	k1gFnPc2	novina
The	The	k1gMnPc2	The
Sun	Sun	kA	Sun
a	a	k8xC	a
také	také	k9	také
do	do	k7c2	do
novin	novina	k1gFnPc2	novina
The	The	k1gFnSc2	The
Sunday	Sundaa	k1gFnPc1	Sundaa
Times	Times	k1gInSc4	Times
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
články	článek	k1gInPc1	článek
z	z	k7c2	z
The	The	k1gFnSc2	The
Times	Timesa	k1gFnPc2	Timesa
jsou	být	k5eAaImIp3nP	být
znovu	znovu	k6eAd1	znovu
publikovány	publikovat	k5eAaBmNgInP	publikovat
v	v	k7c6	v
The	The	k1gFnSc6	The
Weekend	weekend	k1gInSc1	weekend
Australian	Australiany	k1gInPc2	Australiany
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
píše	psát	k5eAaImIp3nS	psát
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
Wheels	Wheels	k1gInSc4	Wheels
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
kola	kolo	k1gNnPc4	kolo
<g/>
)	)	kIx)	)
sekci	sekce	k1gFnSc4	sekce
</s>
</p>
<p>
<s>
do	do	k7c2	do
Toronto	Toronto	k1gNnSc1	Toronto
Star	Star	kA	Star
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Clarkson	Clarkson	k1gMnSc1	Clarkson
napsal	napsat	k5eAaPmAgMnS	napsat
i	i	k9	i
několik	několik	k4yIc4	několik
humorných	humorný	k2eAgFnPc2d1	humorná
knih	kniha	k1gFnPc2	kniha
nejen	nejen	k6eAd1	nejen
o	o	k7c6	o
autech	aut	k1gInPc6	aut
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
jeho	jeho	k3xOp3gFnPc2	jeho
knih	kniha	k1gFnPc2	kniha
jsou	být	k5eAaImIp3nP	být
sbírky	sbírka	k1gFnPc1	sbírka
jeho	jeho	k3xOp3gInPc2	jeho
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
napsal	napsat	k5eAaPmAgInS	napsat
do	do	k7c2	do
The	The	k1gFnSc2	The
Sunday	Sundaa	k1gFnSc2	Sundaa
Times	Timesa	k1gFnPc2	Timesa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
</s>
</p>
<p>
<s>
knihách	kniha	k1gFnPc6	kniha
a	a	k8xC	a
článcích	článek	k1gInPc6	článek
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
řízení	řízení	k1gNnSc3	řízení
aut	auto	k1gNnPc2	auto
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
svůj	svůj	k3xOyFgInSc4	svůj
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
svět	svět	k1gInSc4	svět
jako	jako	k8xS	jako
např.	např.	kA	např.
v	v	k7c4	v
Jeremy	Jerema	k1gFnPc4	Jerema
Clarkson	Clarksona	k1gFnPc2	Clarksona
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Motorworld	Motorworld	k1gInSc1	Motorworld
<g/>
,	,	kIx,	,
Jeremy	Jerem	k1gInPc1	Jerem
Clakson	Claksona	k1gFnPc2	Claksona
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Car	car	k1gMnSc1	car
Years	Years	k1gInSc4	Years
</s>
</p>
<p>
<s>
and	and	k?	and
Jeremy	Jerema	k1gFnPc1	Jerema
Clarkson	Clarksona	k1gFnPc2	Clarksona
Meets	Meetsa	k1gFnPc2	Meetsa
the	the	k?	the
Neighbours	Neighboursa	k1gFnPc2	Neighboursa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Televize	televize	k1gFnSc1	televize
==	==	k?	==
</s>
</p>
<p>
<s>
Claksonova	Claksonův	k2eAgFnSc1d1	Claksonův
první	první	k4xOgFnSc1	první
hlavní	hlavní	k2eAgFnSc1d1	hlavní
televizní	televizní	k2eAgFnSc1d1	televizní
role	role	k1gFnSc1	role
byla	být	k5eAaImAgFnS	být
role	role	k1gFnPc4	role
moderátora	moderátor	k1gMnSc2	moderátor
</s>
</p>
<p>
<s>
v	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
motoristickém	motoristický	k2eAgInSc6d1	motoristický
programu	program	k1gInSc6	program
Top	topit	k5eAaImRp2nS	topit
Gear	Geara	k1gFnPc2	Geara
od	od	k7c2	od
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1988	[number]	k4	1988
do	do	k7c2	do
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
ve	v	k7c6	v
"	"	kIx"	"
<g/>
starém	starý	k2eAgInSc6d1	starý
Top	topit	k5eAaImRp2nS	topit
Gearu	Gear	k1gMnSc6	Gear
<g/>
"	"	kIx"	"
a	a	k8xC	a
poté	poté	k6eAd1	poté
opět	opět	k6eAd1	opět
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
natáčet	natáčet	k5eAaImF	natáčet
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
nový	nový	k2eAgInSc4d1	nový
Top	topit	k5eAaImRp2nS	topit
Gear	Gear	k1gInSc1	Gear
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Jamesem	James	k1gMnSc7	James
Mayem	May	k1gMnSc7	May
a	a	k8xC	a
Richardem	Richard	k1gMnSc7	Richard
Hammondem	Hammond	k1gMnSc7	Hammond
moderovali	moderovat	k5eAaBmAgMnP	moderovat
</s>
</p>
<p>
<s>
nejvíce	nejvíce	k6eAd1	nejvíce
sledovanou	sledovaný	k2eAgFnSc4d1	sledovaná
show	show	k1gFnSc4	show
na	na	k7c4	na
BBC	BBC	kA	BBC
Two	Two	k1gFnSc4	Two
<g/>
,	,	kIx,	,
vysílanou	vysílaný	k2eAgFnSc4d1	vysílaná
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
100	[number]	k4	100
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k6eAd1	také
uváděl	uvádět	k5eAaImAgMnS	uvádět
první	první	k4xOgFnSc4	první
sérii	série	k1gFnSc4	série
britské	britský	k2eAgFnSc2d1	britská
verze	verze	k1gFnSc2	verze
Robot	robot	k1gMnSc1	robot
Wars	Wars	k1gInSc1	Wars
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
talk	talk	k1gInSc1	talk
show	show	k1gNnSc1	show
Clarkson	Clarksona	k1gFnPc2	Clarksona
<g/>
,	,	kIx,	,
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
27	[number]	k4	27
půlhodinových	půlhodinový	k2eAgFnPc2d1	půlhodinová
epizod	epizoda	k1gFnPc2	epizoda
natáčených	natáčený	k2eAgFnPc2d1	natáčená
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
</s>
</p>
<p>
<s>
mezi	mezi	k7c7	mezi
listopadem	listopad	k1gInSc7	listopad
1998	[number]	k4	1998
a	a	k8xC	a
prosincem	prosinec	k1gInSc7	prosinec
2000	[number]	k4	2000
a	a	k8xC	a
objevovali	objevovat	k5eAaImAgMnP	objevovat
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
interview	interview	k1gNnSc4	interview
s	s	k7c7	s
hudebníky	hudebník	k1gMnPc7	hudebník
<g/>
,	,	kIx,	,
politiky	politik	k1gMnPc7	politik
a	a	k8xC	a
televizními	televizní	k2eAgFnPc7d1	televizní
osobnostmi	osobnost	k1gFnPc7	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Clarkson	Clarkson	k1gMnSc1	Clarkson
také	také	k9	také
moderoval	moderovat	k5eAaBmAgMnS	moderovat
dokumenty	dokument	k1gInPc7	dokument
o	o	k7c6	o
nemotoristických	motoristický	k2eNgNnPc6d1	motoristický
tématech	téma	k1gNnPc6	téma
jako	jako	k8xS	jako
historie	historie	k1gFnSc1	historie
a	a	k8xC	a
inženýrství	inženýrství	k1gNnSc1	inženýrství
ačkoli	ačkoli	k8xS	ačkoli
</s>
</p>
<p>
<s>
natáčení	natáčení	k1gNnSc1	natáčení
motoristických	motoristický	k2eAgFnPc2d1	motoristická
show	show	k1gFnPc2	show
a	a	k8xC	a
videí	video	k1gNnPc2	video
stále	stále	k6eAd1	stále
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
často	často	k6eAd1	často
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
v	v	k7c6	v
televizních	televizní	k2eAgFnPc6d1	televizní
show	show	k1gFnPc6	show
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
ve	v	k7c4	v
veselé	veselý	k2eAgNnSc4d1	veselé
komediální	komediální	k2eAgNnSc4d1	komediální
show	show	k1gNnSc4	show
Room	Rooma	k1gFnPc2	Rooma
101	[number]	k4	101
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgFnPc4	který
hosté	host	k1gMnPc1	host
nominují	nominovat	k5eAaBmIp3nP	nominovat
</s>
</p>
<p>
<s>
věci	věc	k1gFnPc1	věc
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
životě	život	k1gInSc6	život
nesnáší	snášet	k5eNaImIp3nS	snášet
<g/>
.	.	kIx.	.
</s>
<s>
Nominoval	nominovat	k5eAaBmAgMnS	nominovat
karavany	karavana	k1gFnPc4	karavana
<g/>
,	,	kIx,	,
mouchy	moucha	k1gFnPc4	moucha
<g/>
,	,	kIx,	,
sitkom	sitkom	k1gInSc1	sitkom
Last	Last	k1gInSc1	Last
of	of	k?	of
the	the	k?	the
Summer	Summer	k1gInSc4	Summer
Wine	Win	k1gFnSc2	Win
<g/>
,	,	kIx,	,
mentalitu	mentalita	k1gFnSc4	mentalita
golfových	golfový	k2eAgInPc2d1	golfový
klubů	klub	k1gInPc2	klub
a	a	k8xC	a
vegetariány	vegetarián	k1gMnPc7	vegetarián
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
</s>
</p>
<p>
<s>
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c4	v
talk	talk	k1gInSc4	talk
show	show	k1gFnSc1	show
Parkinson	Parkinsona	k1gFnPc2	Parkinsona
a	a	k8xC	a
Friday	Fridaa	k1gFnSc2	Fridaa
Night	Night	k1gMnSc1	Night
with	with	k1gMnSc1	with
Jonathan	Jonathan	k1gMnSc1	Jonathan
Ross	Ross	k1gInSc4	Ross
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
National	National	k1gFnSc2	National
Television	Television	k1gInSc1	Television
Adward	Adward	k1gInSc1	Adward
<g/>
,	,	kIx,	,
speciální	speciální	k2eAgInSc1d1	speciální
</s>
</p>
<p>
<s>
uznání	uznání	k1gNnSc1	uznání
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
odhadováno	odhadován	k2eAgNnSc1d1	odhadováno
<g/>
,	,	kIx,	,
že	že	k8xS	že
vydělává	vydělávat	k5eAaImIp3nS	vydělávat
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
liber	libra	k1gFnPc2	libra
ročně	ročně	k6eAd1	ročně
za	za	k7c4	za
moderování	moderování	k1gNnSc4	moderování
Top	topit	k5eAaImRp2nS	topit
Gearu	Gear	k1gInSc2	Gear
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
1,7	[number]	k4	1,7
milionu	milion	k4xCgInSc2	milion
liber	libra	k1gFnPc2	libra
z	z	k7c2	z
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
DVD	DVD	kA	DVD
a	a	k8xC	a
</s>
</p>
<p>
<s>
novinových	novinový	k2eAgInPc2d1	novinový
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Jeremy	Jerema	k1gFnPc4	Jerema
je	být	k5eAaImIp3nS	být
velkým	velký	k2eAgMnSc7d1	velký
fanouškem	fanoušek	k1gMnSc7	fanoušek
seriálu	seriál	k1gInSc2	seriál
Star	Star	kA	Star
Trek	Trek	k1gMnSc1	Trek
<g/>
.	.	kIx.	.
</s>
<s>
Nosem	nos	k1gInSc7	nos
umí	umět	k5eAaImIp3nS	umět
napodobit	napodobit	k5eAaPmF	napodobit
zvuk	zvuk	k1gInSc4	zvuk
otevírajících	otevírající	k2eAgFnPc2d1	otevírající
se	se	k3xPyFc4	se
dveří	dveře	k1gFnPc2	dveře
na	na	k7c4	na
lodi	loď	k1gFnPc4	loď
Enterprise	Enterprise	k1gFnSc2	Enterprise
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Jeremy	Jerema	k1gFnPc4	Jerema
nikdy	nikdy	k6eAd1	nikdy
v	v	k7c6	v
životě	život	k1gInSc6	život
(	(	kIx(	(
<g/>
ani	ani	k8xC	ani
jako	jako	k9	jako
malý	malý	k2eAgInSc4d1	malý
<g/>
)	)	kIx)	)
nejel	jet	k5eNaImAgMnS	jet
veřejnou	veřejný	k2eAgFnSc7d1	veřejná
dopravou	doprava	k1gFnSc7	doprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
sérii	série	k1gFnSc6	série
nového	nový	k2eAgInSc2d1	nový
Top	topit	k5eAaImRp2nS	topit
Gearu	Gear	k1gInSc2	Gear
však	však	k9	však
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
osmi	osm	k4xCc6	osm
letech	léto	k1gNnPc6	léto
jel	jet	k5eAaImAgMnS	jet
autobusem	autobus	k1gInSc7	autobus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trvalé	trvalý	k2eAgNnSc1d1	trvalé
bydliště	bydliště	k1gNnSc1	bydliště
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Man	mana	k1gFnPc2	mana
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c6	na
silnicích	silnice	k1gFnPc6	silnice
mimo	mimo	k7c4	mimo
obce	obec	k1gFnPc4	obec
není	být	k5eNaImIp3nS	být
rychlostní	rychlostní	k2eAgInSc1d1	rychlostní
limit	limit	k1gInSc1	limit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeremy	Jerema	k1gFnPc1	Jerema
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Top	topit	k5eAaImRp2nS	topit
Gear	Gear	k1gInSc1	Gear
hodně	hodně	k6eAd1	hodně
uráží	urážet	k5eAaImIp3nS	urážet
fotbalové	fotbalový	k2eAgInPc4d1	fotbalový
kluby	klub	k1gInPc4	klub
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
na	na	k7c4	na
Chelsea	Chelseum	k1gNnPc4	Chelseum
FC	FC	kA	FC
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
fandí	fandit	k5eAaImIp3nP	fandit
a	a	k8xC	a
veřejně	veřejně	k6eAd1	veřejně
to	ten	k3xDgNnSc4	ten
i	i	k9	i
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Clarkson	Clarkson	k1gMnSc1	Clarkson
je	být	k5eAaImIp3nS	být
velkým	velký	k2eAgMnSc7d1	velký
fanouškem	fanoušek	k1gMnSc7	fanoušek
rockové	rockový	k2eAgFnSc2d1	rocková
skupiny	skupina	k1gFnSc2	skupina
Genesis	Genesis	k1gFnSc1	Genesis
a	a	k8xC	a
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
i	i	k9	i
jejich	jejich	k3xOp3gInSc2	jejich
koncertu	koncert	k1gInSc2	koncert
na	na	k7c6	na
Twickenhamském	Twickenhamský	k2eAgInSc6d1	Twickenhamský
stadionu	stadion	k1gInSc6	stadion
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
láskou	láska	k1gFnSc7	láska
k	k	k7c3	k
Genesis	Genesis	k1gFnSc1	Genesis
si	se	k3xPyFc3	se
také	také	k9	také
tropí	tropit	k5eAaImIp3nP	tropit
žerty	žert	k1gInPc4	žert
z	z	k7c2	z
Richarda	Richard	k1gMnSc2	Richard
Hammonda	Hammond	k1gMnSc2	Hammond
<g/>
,	,	kIx,	,
moderátora	moderátor	k1gMnSc2	moderátor
Top	topit	k5eAaImRp2nS	topit
Gearu	Gear	k1gMnSc6	Gear
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
skupinu	skupina	k1gFnSc4	skupina
nenávidí	návidět	k5eNaImIp3nS	návidět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeremy	Jerem	k1gInPc4	Jerem
Clarkson	Clarksona	k1gFnPc2	Clarksona
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
Top	topit	k5eAaImRp2nS	topit
Gearu	Geara	k1gFnSc4	Geara
vyhozen	vyhodit	k5eAaPmNgMnS	vyhodit
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
že	že	k8xS	že
uhodil	uhodit	k5eAaPmAgInS	uhodit
scénaristu	scénarista	k1gMnSc4	scénarista
protože	protože	k8xS	protože
mu	on	k3xPp3gMnSc3	on
přinesl	přinést	k5eAaPmAgMnS	přinést
</s>
</p>
<p>
<s>
studené	studený	k2eAgNnSc1d1	studené
jídlo	jídlo	k1gNnSc1	jídlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Top	topit	k5eAaImRp2nS	topit
Gear	Gear	k1gInSc1	Gear
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
kolegové	kolega	k1gMnPc1	kolega
ze	z	k7c2	z
světově	světově	k6eAd1	světově
nejúspěšnějšího	úspěšný	k2eAgInSc2d3	nejúspěšnější
motoristického	motoristický	k2eAgInSc2d1	motoristický
pořadu	pořad	k1gInSc2	pořad
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Richard	Richard	k1gMnSc1	Richard
Hammond	Hammond	k1gMnSc1	Hammond
a	a	k8xC	a
James	James	k1gMnSc1	James
May	May	k1gMnSc1	May
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
závodí	závodit	k5eAaImIp3nS	závodit
Clarkson	Clarkson	k1gInSc1	Clarkson
proti	proti	k7c3	proti
svým	svůj	k3xOyFgMnPc3	svůj
kolegům	kolega	k1gMnPc3	kolega
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
autě	auto	k1gNnSc6	auto
(	(	kIx(	(
<g/>
oni	onen	k3xDgMnPc1	onen
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
snaží	snažit	k5eAaImIp3nS	snažit
pokořit	pokořit	k5eAaPmF	pokořit
různými	různý	k2eAgInPc7d1	různý
dopravními	dopravní	k2eAgInPc7d1	dopravní
prostředky	prostředek	k1gInPc7	prostředek
<g/>
)	)	kIx)	)
a	a	k8xC	a
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
vyrovnanou	vyrovnaný	k2eAgFnSc4d1	vyrovnaná
bitvu	bitva	k1gFnSc4	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
také	také	k9	také
o	o	k7c4	o
'	'	kIx"	'
<g/>
vynálezce	vynálezce	k1gMnSc4	vynálezce
<g/>
'	'	kIx"	'
Stiga	Stig	k1gMnSc4	Stig
–	–	k?	–
závodního	závodní	k1gMnSc4	závodní
jezdce	jezdec	k1gMnSc4	jezdec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byli	být	k5eAaImAgMnP	být
Clarkson	Clarkson	k1gNnSc4	Clarkson
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
kolega	kolega	k1gMnSc1	kolega
z	z	k7c2	z
Top	topit	k5eAaImRp2nS	topit
Gearu	Gear	k1gInSc2	Gear
</s>
</p>
<p>
<s>
-	-	kIx~	-
James	James	k1gMnSc1	James
May	May	k1gMnSc1	May
první	první	k4xOgMnPc1	první
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
severního	severní	k2eAgInSc2d1	severní
magnetického	magnetický	k2eAgInSc2d1	magnetický
pólu	pól	k1gInSc2	pól
za	za	k7c7	za
volantem	volant	k1gInSc7	volant
automobilu	automobil	k1gInSc2	automobil
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
vysíláno	vysílat	k5eAaImNgNnS	vysílat
ve	v	k7c6	v
speciálním	speciální	k2eAgInSc6d1	speciální
dílu	díl	k1gInSc6	díl
Top	topit	k5eAaImRp2nS	topit
Gearu	Gear	k1gInSc3	Gear
<g/>
:	:	kIx,	:
Polární	polární	k2eAgInSc1d1	polární
speciál	speciál	k1gInSc1	speciál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
Clarkson	Clarkson	k1gInSc1	Clarkson
po	po	k7c6	po
rozmíšce	rozmíška	k1gFnSc6	rozmíška
s	s	k7c7	s
producentem	producent	k1gMnSc7	producent
pořadu	pořad	k1gInSc2	pořad
suspendován	suspendován	k2eAgInSc4d1	suspendován
z	z	k7c2	z
mateřské	mateřský	k2eAgFnSc2d1	mateřská
BBC	BBC	kA	BBC
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
oznámila	oznámit	k5eAaPmAgFnS	oznámit
své	svůj	k3xOyFgNnSc4	svůj
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
neodvysílat	odvysílat	k5eNaPmF	odvysílat
zbylé	zbylý	k2eAgFnPc4d1	zbylá
epizody	epizoda	k1gFnPc4	epizoda
aktuálního	aktuální	k2eAgInSc2d1	aktuální
ročníku	ročník	k1gInSc2	ročník
Top	topit	k5eAaImRp2nS	topit
Gearu	Gear	k1gInSc6	Gear
a	a	k8xC	a
nad	nad	k7c7	nad
budoucností	budoucnost	k1gFnSc7	budoucnost
kultovního	kultovní	k2eAgInSc2d1	kultovní
motoristického	motoristický	k2eAgInSc2d1	motoristický
magazínu	magazín	k1gInSc2	magazín
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
vznáší	vznášet	k5eAaImIp3nS	vznášet
otazník	otazník	k1gInSc1	otazník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
25.3	[number]	k4	25.3
<g/>
.2015	.2015	k4	.2015
BBC	BBC	kA	BBC
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
neprodlouží	prodloužit	k5eNaPmIp3nS	prodloužit
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Clarksonem	Clarkson	k1gInSc7	Clarkson
.	.	kIx.	.
<g/>
Prozatím	prozatím	k6eAd1	prozatím
není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
ani	ani	k8xC	ani
pokračování	pokračování	k1gNnSc1	pokračování
Top	topit	k5eAaImRp2nS	topit
Gearu	Geara	k1gFnSc4	Geara
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
formátu	formát	k1gInSc6	formát
ani	ani	k8xC	ani
jeho	on	k3xPp3gMnSc2	on
nástupce	nástupce	k1gMnSc2	nástupce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
17.6	[number]	k4	17.6
<g/>
.2015	.2015	k4	.2015
BBC	BBC	kA	BBC
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nástupcem	nástupce	k1gMnSc7	nástupce
Jeremyho	Jeremy	k1gMnSc2	Jeremy
Clarksona	Clarkson	k1gMnSc2	Clarkson
bude	být	k5eAaImBp3nS	být
Chris	Chris	k1gFnSc3	Chris
Evans	Evans	k1gInSc1	Evans
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
stanicí	stanice	k1gFnSc7	stanice
podepsal	podepsat	k5eAaPmAgMnS	podepsat
tříletou	tříletý	k2eAgFnSc4d1	tříletá
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
oznámili	oznámit	k5eAaPmAgMnP	oznámit
svůj	svůj	k3xOyFgInSc4	svůj
odchod	odchod	k1gInSc4	odchod
z	z	k7c2	z
pořadu	pořad	k1gInSc2	pořad
i	i	k8xC	i
James	James	k1gMnSc1	James
May	May	k1gMnSc1	May
a	a	k8xC	a
Richard	Richard	k1gMnSc1	Richard
Hammond	Hammond	k1gMnSc1	Hammond
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeremy	Jerema	k1gFnPc1	Jerema
Clarskon	Clarskona	k1gFnPc2	Clarskona
nyní	nyní	k6eAd1	nyní
natáčí	natáčet	k5eAaImIp3nS	natáčet
pořad	pořad	k1gInSc1	pořad
podobného	podobný	k2eAgInSc2d1	podobný
stylu	styl	k1gInSc2	styl
jako	jako	k8xS	jako
Top	topit	k5eAaImRp2nS	topit
Gear	Gear	k1gInSc1	Gear
(	(	kIx(	(
<g/>
nově	nově	k6eAd1	nově
The	The	k1gMnSc1	The
Grand	grand	k1gMnSc1	grand
Tour	Tour	k1gMnSc1	Tour
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
epizoda	epizoda	k1gFnSc1	epizoda
byla	být	k5eAaImAgFnS	být
vysílána	vysílat	k5eAaImNgFnS	vysílat
18	[number]	k4	18
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
v	v	k7c6	v
česku	česko	k1gNnSc6	česko
službou	služba	k1gFnSc7	služba
Amazon	amazona	k1gFnPc2	amazona
Prime	prim	k1gInSc5	prim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Jeremy	Jerema	k1gFnSc2	Jerema
Clarkson	Clarksona	k1gFnPc2	Clarksona
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jeremy	Jerema	k1gFnSc2	Jerema
Clarkson	Clarkson	k1gInSc1	Clarkson
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Jeremy	Jerema	k1gFnSc2	Jerema
Clarkson	Clarksona	k1gFnPc2	Clarksona
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
České	český	k2eAgFnPc1d1	Česká
stránky	stránka	k1gFnPc1	stránka
s	s	k7c7	s
obsahy	obsah	k1gInPc7	obsah
epizod	epizoda	k1gFnPc2	epizoda
</s>
</p>
