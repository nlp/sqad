<p>
<s>
Open	Open	k1gInSc1	Open
University	universita	k1gFnSc2	universita
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
světová	světový	k2eAgFnSc1d1	světová
distanční	distanční	k2eAgFnSc1d1	distanční
(	(	kIx(	(
<g/>
multikanálová	multikanálový	k2eAgFnSc1d1	multikanálová
<g/>
)	)	kIx)	)
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
studuje	studovat	k5eAaImIp3nS	studovat
asi	asi	k9	asi
200	[number]	k4	200
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
50	[number]	k4	50
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
a	a	k8xC	a
studijní	studijní	k2eAgInSc1d1	studijní
model	model	k1gInSc1	model
==	==	k?	==
</s>
</p>
<p>
<s>
Open	Open	k1gNnSc1	Open
University	universita	k1gFnSc2	universita
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
na	na	k7c4	na
zakázku	zakázka	k1gFnSc4	zakázka
britské	britský	k2eAgFnSc2d1	britská
vlády	vláda	k1gFnSc2	vláda
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zpřístupnit	zpřístupnit	k5eAaPmF	zpřístupnit
špičkové	špičkový	k2eAgNnSc4d1	špičkové
vzdělání	vzdělání	k1gNnSc4	vzdělání
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
pracovní	pracovní	k2eAgInPc1d1	pracovní
povinnosti	povinnost	k1gFnPc4	povinnost
nebo	nebo	k8xC	nebo
životní	životní	k2eAgFnPc4d1	životní
situace	situace	k1gFnPc4	situace
neumožňují	umožňovat	k5eNaImIp3nP	umožňovat
pravidelně	pravidelně	k6eAd1	pravidelně
dojíždět	dojíždět	k5eAaImF	dojíždět
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
(	(	kIx(	(
<g/>
dálkové	dálkový	k2eAgNnSc1d1	dálkové
studium	studium	k1gNnSc1	studium
nebylo	být	k5eNaImAgNnS	být
pokládáno	pokládat	k5eAaImNgNnS	pokládat
za	za	k7c4	za
prakticky	prakticky	k6eAd1	prakticky
použitelnou	použitelný	k2eAgFnSc4d1	použitelná
alternativu	alternativa	k1gFnSc4	alternativa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
studenti	student	k1gMnPc1	student
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
stabilizoval	stabilizovat	k5eAaBmAgInS	stabilizovat
systém	systém	k1gInSc1	systém
výuky	výuka	k1gFnSc2	výuka
a	a	k8xC	a
rostl	růst	k5eAaImAgInS	růst
počet	počet	k1gInSc4	počet
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Mohutným	mohutný	k2eAgInSc7d1	mohutný
impulzem	impulz	k1gInSc7	impulz
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
rozvoji	rozvoj	k1gInSc3	rozvoj
byl	být	k5eAaImAgInS	být
nástup	nástup	k1gInSc4	nástup
internetu	internet	k1gInSc2	internet
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k9	tak
systém	systém	k1gInSc1	systém
výuky	výuka	k1gFnSc2	výuka
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
hrají	hrát	k5eAaImIp3nP	hrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
cvičebnice	cvičebnice	k1gFnPc1	cvičebnice
speciálně	speciálně	k6eAd1	speciálně
psané	psaný	k2eAgFnPc1d1	psaná
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
studijní	studijní	k2eAgInSc4d1	studijní
styl	styl	k1gInSc4	styl
</s>
</p>
<p>
<s>
osobnost	osobnost	k1gFnSc1	osobnost
tutora	tutor	k1gMnSc2	tutor
<g/>
,	,	kIx,	,
jakéhosi	jakýsi	k3yIgMnSc2	jakýsi
osobního	osobní	k2eAgMnSc2d1	osobní
kouče	kouč	k1gMnSc2	kouč
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
přidělen	přidělit	k5eAaPmNgInS	přidělit
každému	každý	k3xTgMnSc3	každý
studentovi	student	k1gMnSc3	student
</s>
</p>
<p>
<s>
on-line	onin	k1gInSc5	on-lin
aktivity	aktivita	k1gFnPc4	aktivita
</s>
</p>
<p>
<s>
řízená	řízený	k2eAgFnSc1d1	řízená
interakce	interakce	k1gFnSc1	interakce
mezi	mezi	k7c7	mezi
spolužáky	spolužák	k1gMnPc7	spolužák
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
rovněž	rovněž	k9	rovněž
on-line	onin	k1gInSc5	on-lin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
na	na	k7c4	na
Open	Open	k1gInSc4	Open
University	universita	k1gFnSc2	universita
vyučují	vyučovat	k5eAaImIp3nP	vyučovat
prakticky	prakticky	k6eAd1	prakticky
všechny	všechen	k3xTgInPc1	všechen
obory	obor	k1gInPc1	obor
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
medicíny	medicína	k1gFnSc2	medicína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
vydaném	vydaný	k2eAgInSc6d1	vydaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
předstihla	předstihnout	k5eAaPmAgFnS	předstihnout
úroveň	úroveň	k1gFnSc1	úroveň
výuky	výuka	k1gFnSc2	výuka
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
příčku	příčka	k1gFnSc4	příčka
denní	denní	k2eAgNnSc1d1	denní
studium	studium	k1gNnSc1	studium
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
<g/>
.	.	kIx.	.
</s>
<s>
Škola	škola	k1gFnSc1	škola
také	také	k9	také
pravidelně	pravidelně	k6eAd1	pravidelně
vítězí	vítězit	k5eAaImIp3nS	vítězit
v	v	k7c6	v
žebříčcích	žebříček	k1gInPc6	žebříček
spokojenosti	spokojenost	k1gFnSc2	spokojenost
britských	britský	k2eAgMnPc2d1	britský
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
roste	růst	k5eAaImIp3nS	růst
podíl	podíl	k1gInSc1	podíl
studentů	student	k1gMnPc2	student
žijících	žijící	k2eAgMnPc2d1	žijící
mimo	mimo	k7c4	mimo
Velkou	velký	k2eAgFnSc4d1	velká
Británii	Británie	k1gFnSc4	Británie
–	–	k?	–
roste	růst	k5eAaImIp3nS	růst
zejména	zejména	k9	zejména
počet	počet	k1gInSc1	počet
studentů	student	k1gMnPc2	student
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Japonsku	Japonsko	k1gNnSc6	Japonsko
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
tuto	tento	k3xDgFnSc4	tento
globální	globální	k2eAgFnSc4d1	globální
expanzi	expanze	k1gFnSc4	expanze
podpořila	podpořit	k5eAaPmAgFnS	podpořit
<g/>
,	,	kIx,	,
zakládá	zakládat	k5eAaImIp3nS	zakládat
Open	Open	k1gMnSc1	Open
University	universita	k1gFnSc2	universita
lokální	lokální	k2eAgNnPc4d1	lokální
podpůrná	podpůrný	k2eAgNnPc4d1	podpůrné
centra	centrum	k1gNnPc4	centrum
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
USA	USA	kA	USA
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejaktivnější	aktivní	k2eAgFnSc1d3	nejaktivnější
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
manažerská	manažerský	k2eAgFnSc1d1	manažerská
fakulta	fakulta	k1gFnSc1	fakulta
–	–	k?	–
OU	ou	k0	ou
Business	business	k1gInSc1	business
School	Schoola	k1gFnPc2	Schoola
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
postupně	postupně	k6eAd1	postupně
stala	stát	k5eAaPmAgFnS	stát
největší	veliký	k2eAgFnSc7d3	veliký
manažerskou	manažerský	k2eAgFnSc7d1	manažerská
školou	škola	k1gFnSc7	škola
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
některých	některý	k3yIgFnPc6	některý
dalších	další	k2eAgFnPc6d1	další
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
největším	veliký	k2eAgMnSc7d3	veliký
světovým	světový	k2eAgMnSc7d1	světový
vydavatelem	vydavatel	k1gMnSc7	vydavatel
titulu	titul	k1gInSc2	titul
MBA	MBA	kA	MBA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přítomnost	přítomnost	k1gFnSc1	přítomnost
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
aktivně	aktivně	k6eAd1	aktivně
přítomna	přítomen	k2eAgFnSc1d1	přítomna
manažerská	manažerský	k2eAgFnSc1d1	manažerská
fakulta	fakulta	k1gFnSc1	fakulta
The	The	k1gFnSc2	The
Open	Opena	k1gFnPc2	Opena
University	universita	k1gFnSc2	universita
–	–	k?	–
OU	ou	k0	ou
Business	business	k1gInSc1	business
School	School	k1gInSc1	School
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Open	Opena	k1gFnPc2	Opena
University	universita	k1gFnSc2	universita
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
www.open.ac.uk	www.open.ac.uk	k6eAd1	www.open.ac.uk
</s>
</p>
<p>
<s>
openmanagement	openmanagement	k1gInSc1	openmanagement
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
