<s>
Potrava	potrava	k1gFnSc1	potrava
surikaty	surikata	k1gFnSc2	surikata
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
hmyzu	hmyz	k1gInSc2	hmyz
(	(	kIx(	(
<g/>
termiti	termit	k1gMnPc1	termit
<g/>
,	,	kIx,	,
<g/>
kobylky	kobylka	k1gFnPc1	kobylka
nebo	nebo	k8xC	nebo
saranče	saranče	k1gFnPc1	saranče
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pavouků	pavouk	k1gMnPc2	pavouk
a	a	k8xC	a
ze	z	k7c2	z
štírů	štír	k1gMnPc2	štír
(	(	kIx(	(
<g/>
surikaty	surikata	k1gFnSc2	surikata
jsou	být	k5eAaImIp3nP	být
imunní	imunní	k2eAgMnPc1d1	imunní
vůči	vůči	k7c3	vůči
jejich	jejich	k3xOp3gNnSc3	jejich
jedu	jet	k5eAaImIp1nS	jet
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
z	z	k7c2	z
menších	malý	k2eAgMnPc2d2	menší
obratlovců	obratlovec	k1gMnPc2	obratlovec
(	(	kIx(	(
<g/>
hadi	had	k1gMnPc1	had
<g/>
,	,	kIx,	,
ještěrky	ještěrka	k1gFnPc1	ještěrka
nebo	nebo	k8xC	nebo
hlodavci	hlodavec	k1gMnPc1	hlodavec
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
také	také	k9	také
však	však	k9	však
z	z	k7c2	z
kořenů	kořen	k1gInPc2	kořen
nebo	nebo	k8xC	nebo
z	z	k7c2	z
cibulek	cibulka	k1gFnPc2	cibulka
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
