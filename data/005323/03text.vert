<s>
Surikata	surikata	k1gFnSc1	surikata
(	(	kIx(	(
<g/>
také	také	k9	také
hrabačka	hrabačka	k1gFnSc1	hrabačka
surikata	surikata	k1gFnSc1	surikata
<g/>
,	,	kIx,	,
promyka	promyka	k1gFnSc1	promyka
surikata	surikata	k1gFnSc1	surikata
<g/>
,	,	kIx,	,
Suricata	Suricat	k2eAgFnSc1d1	Suricata
suricatta	suricatta	k1gFnSc1	suricatta
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
denní	denní	k2eAgFnSc1d1	denní
pospolitě	pospolitě	k6eAd1	pospolitě
žijící	žijící	k2eAgFnSc1d1	žijící
promykovitá	promykovitý	k2eAgFnSc1d1	promykovitý
šelma	šelma	k1gFnSc1	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Surikata	surikata	k1gFnSc1	surikata
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
častým	častý	k2eAgMnSc7d1	častý
chovancem	chovanec	k1gMnSc7	chovanec
zoo	zoo	k1gFnSc2	zoo
<g/>
.	.	kIx.	.
</s>
<s>
Surikata	surikata	k1gFnSc1	surikata
obývá	obývat	k5eAaImIp3nS	obývat
skalnaté	skalnatý	k2eAgFnSc3d1	skalnatá
i	i	k8xC	i
písčité	písčitý	k2eAgFnSc3d1	písčitá
oblasti	oblast	k1gFnSc3	oblast
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
600	[number]	k4	600
až	až	k9	až
975	[number]	k4	975
g	g	kA	g
Délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
25	[number]	k4	25
až	až	k9	až
35	[number]	k4	35
cm	cm	kA	cm
Délka	délka	k1gFnSc1	délka
ocasu	ocas	k1gInSc2	ocas
17	[number]	k4	17
až	až	k9	až
25	[number]	k4	25
cm	cm	kA	cm
Výška	výška	k1gFnSc1	výška
až	až	k9	až
35	[number]	k4	35
cm	cm	kA	cm
Na	na	k7c6	na
obličeji	obličej	k1gInSc6	obličej
a	a	k8xC	a
na	na	k7c6	na
spodu	spod	k1gInSc6	spod
těla	tělo	k1gNnSc2	tělo
jsou	být	k5eAaImIp3nP	být
surikaty	surikata	k1gFnPc1	surikata
hnědé	hnědý	k2eAgFnPc1d1	hnědá
<g/>
,	,	kIx,	,
svrchu	svrchu	k6eAd1	svrchu
stříbrnohnědé	stříbrnohnědý	k2eAgFnPc1d1	stříbrnohnědý
<g/>
,	,	kIx,	,
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
mají	mít	k5eAaImIp3nP	mít
osm	osm	k4xCc1	osm
tmavších	tmavý	k2eAgInPc2d2	tmavší
pruhů	pruh	k1gInPc2	pruh
<g/>
,	,	kIx,	,
tmavé	tmavý	k2eAgInPc1d1	tmavý
kroužky	kroužek	k1gInPc1	kroužek
kolem	kolem	k7c2	kolem
očí	oko	k1gNnPc2	oko
a	a	k8xC	a
tmavou	tmavý	k2eAgFnSc4d1	tmavá
špičku	špička	k1gFnSc4	špička
štíhlého	štíhlý	k2eAgInSc2d1	štíhlý
ocasu	ocas	k1gInSc2	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Surikata	surikata	k1gFnSc1	surikata
používá	používat	k5eAaImIp3nS	používat
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
drápy	dráp	k1gInPc4	dráp
k	k	k7c3	k
vyhrabávání	vyhrabávání	k1gNnSc3	vyhrabávání
doupěte	doupě	k1gNnSc2	doupě
a	a	k8xC	a
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
postaví	postavit	k5eAaPmIp3nS	postavit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
tlapky	tlapka	k1gFnPc4	tlapka
<g/>
,	,	kIx,	,
podepírá	podepírat	k5eAaImIp3nS	podepírat
se	se	k3xPyFc4	se
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
ocasem	ocas	k1gInSc7	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Potrava	potrava	k1gFnSc1	potrava
surikaty	surikata	k1gFnSc2	surikata
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
hmyzu	hmyz	k1gInSc2	hmyz
(	(	kIx(	(
<g/>
termiti	termit	k1gMnPc1	termit
<g/>
,	,	kIx,	,
<g/>
kobylky	kobylka	k1gFnPc1	kobylka
nebo	nebo	k8xC	nebo
saranče	saranče	k1gFnPc1	saranče
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pavouků	pavouk	k1gMnPc2	pavouk
a	a	k8xC	a
ze	z	k7c2	z
štírů	štír	k1gMnPc2	štír
(	(	kIx(	(
<g/>
surikaty	surikata	k1gFnSc2	surikata
jsou	být	k5eAaImIp3nP	být
imunní	imunní	k2eAgMnPc1d1	imunní
vůči	vůči	k7c3	vůči
jejich	jejich	k3xOp3gNnSc3	jejich
jedu	jet	k5eAaImIp1nS	jet
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
z	z	k7c2	z
menších	malý	k2eAgMnPc2d2	menší
obratlovců	obratlovec	k1gMnPc2	obratlovec
(	(	kIx(	(
<g/>
hadi	had	k1gMnPc1	had
<g/>
,	,	kIx,	,
ještěrky	ještěrka	k1gFnPc1	ještěrka
nebo	nebo	k8xC	nebo
hlodavci	hlodavec	k1gMnPc1	hlodavec
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
také	také	k9	také
však	však	k9	však
z	z	k7c2	z
kořenů	kořen	k1gInPc2	kořen
nebo	nebo	k8xC	nebo
z	z	k7c2	z
cibulek	cibulka	k1gFnPc2	cibulka
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Surikaty	surikata	k1gFnPc1	surikata
loví	lovit	k5eAaImIp3nP	lovit
malé	malý	k2eAgMnPc4d1	malý
savce	savec	k1gMnPc4	savec
<g/>
,	,	kIx,	,
plazy	plaz	k1gMnPc4	plaz
<g/>
,	,	kIx,	,
malé	malý	k2eAgMnPc4d1	malý
ptáky	pták	k1gMnPc4	pták
a	a	k8xC	a
pochutnají	pochutnat	k5eAaPmIp3nP	pochutnat
si	se	k3xPyFc3	se
i	i	k9	i
na	na	k7c6	na
hmyzu	hmyz	k1gInSc6	hmyz
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
sarančatech	saranče	k1gNnPc6	saranče
<g/>
.	.	kIx.	.
</s>
<s>
Březost	březost	k1gFnSc1	březost
trvá	trvat	k5eAaImIp3nS	trvat
11	[number]	k4	11
týdnů	týden	k1gInPc2	týden
a	a	k8xC	a
v	v	k7c6	v
doupěti	doupě	k1gNnSc6	doupě
se	se	k3xPyFc4	se
ve	v	k7c6	v
zvláštní	zvláštní	k2eAgFnSc6d1	zvláštní
komoře	komora	k1gFnSc6	komora
vyložené	vyložený	k2eAgFnSc6d1	vyložená
trávou	tráva	k1gFnSc7	tráva
rodí	rodit	k5eAaImIp3nS	rodit
2	[number]	k4	2
až	až	k9	až
5	[number]	k4	5
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
slepá	slepý	k2eAgFnSc1d1	slepá
<g/>
,	,	kIx,	,
začínají	začínat	k5eAaImIp3nP	začínat
vidět	vidět	k5eAaImF	vidět
až	až	k9	až
od	od	k7c2	od
10	[number]	k4	10
-	-	kIx~	-
14	[number]	k4	14
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Dominantní	dominantní	k2eAgFnSc1d1	dominantní
samice	samice	k1gFnSc1	samice
brzy	brzy	k6eAd1	brzy
opět	opět	k6eAd1	opět
zabřezne	zabřeznout	k5eAaPmIp3nS	zabřeznout
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
o	o	k7c4	o
její	její	k3xOp3gNnPc4	její
dřívější	dřívější	k2eAgNnPc4d1	dřívější
mláďata	mládě	k1gNnPc4	mládě
musí	muset	k5eAaImIp3nP	muset
starat	starat	k5eAaImF	starat
ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
čtyři	čtyři	k4xCgInPc1	čtyři
týdny	týden	k1gInPc1	týden
žijí	žít	k5eAaImIp3nP	žít
mláďata	mládě	k1gNnPc4	mládě
v	v	k7c6	v
noře	nora	k1gFnSc6	nora
a	a	k8xC	a
živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
mateřským	mateřský	k2eAgNnSc7d1	mateřské
mlékem	mléko	k1gNnSc7	mléko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
však	však	k9	však
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
jen	jen	k9	jen
od	od	k7c2	od
jejich	jejich	k3xOp3gFnSc2	jejich
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
od	od	k7c2	od
jiné	jiný	k2eAgFnSc2d1	jiná
samičky	samička	k1gFnSc2	samička
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
mléko	mléko	k1gNnSc1	mléko
tvoří	tvořit	k5eAaImIp3nS	tvořit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
zranitelná	zranitelný	k2eAgNnPc1d1	zranitelné
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
mláďata	mládě	k1gNnPc1	mládě
objeví	objevit	k5eAaPmIp3nP	objevit
konkurenční	konkurenční	k2eAgFnSc1d1	konkurenční
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
zabije	zabít	k5eAaPmIp3nS	zabít
je	on	k3xPp3gNnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k8xS	co
mláďata	mládě	k1gNnPc1	mládě
vyjdou	vyjít	k5eAaPmIp3nP	vyjít
poprvé	poprvé	k6eAd1	poprvé
z	z	k7c2	z
nory	nora	k1gFnSc2	nora
<g/>
,	,	kIx,	,
jdou	jít	k5eAaImIp3nP	jít
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
na	na	k7c4	na
první	první	k4xOgInSc4	první
lov	lov	k1gInSc4	lov
<g/>
.	.	kIx.	.
</s>
<s>
Surikaty	surikata	k1gFnPc1	surikata
tvoří	tvořit	k5eAaImIp3nP	tvořit
kolonie	kolonie	k1gFnPc1	kolonie
až	až	k9	až
o	o	k7c6	o
třiceti	třicet	k4xCc6	třicet
členech	člen	k1gMnPc6	člen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
podzemní	podzemní	k2eAgFnSc6d1	podzemní
soustavě	soustava	k1gFnSc6	soustava
nor	nora	k1gFnPc2	nora
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dříve	dříve	k6eAd2	dříve
vyhrabaly	vyhrabat	k5eAaPmAgFnP	vyhrabat
zemní	zemní	k2eAgFnPc1d1	zemní
veverky	veverka	k1gFnPc1	veverka
<g/>
.	.	kIx.	.
</s>
<s>
Kolonii	kolonie	k1gFnSc4	kolonie
vévodí	vévodit	k5eAaImIp3nS	vévodit
dominantní	dominantní	k2eAgFnSc1d1	dominantní
pár	pár	k4xCyI	pár
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jediný	jediný	k2eAgMnSc1d1	jediný
se	se	k3xPyFc4	se
smí	smět	k5eAaImIp3nS	smět
rozmnožovat	rozmnožovat	k5eAaImF	rozmnožovat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
přece	přece	k9	přece
jen	jen	k9	jen
zabřezne	zabřeznout	k5eAaPmIp3nS	zabřeznout
nějaká	nějaký	k3yIgFnSc1	nějaký
jiná	jiný	k2eAgFnSc1d1	jiná
samice	samice	k1gFnSc1	samice
<g/>
,	,	kIx,	,
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
dominantním	dominantní	k2eAgInSc6d1	dominantní
páru	pár	k1gInSc6	pár
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
a	a	k8xC	a
kolik	kolik	k4yIc1	kolik
mláďat	mládě	k1gNnPc2	mládě
přežije	přežít	k5eAaPmIp3nS	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Dominantní	dominantní	k2eAgFnSc1d1	dominantní
samička	samička	k1gFnSc1	samička
může	moct	k5eAaImIp3nS	moct
zabít	zabít	k5eAaPmF	zabít
mláďata	mládě	k1gNnPc4	mládě
jiné	jiný	k2eAgFnSc2d1	jiná
samičky	samička	k1gFnSc2	samička
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zabezpečila	zabezpečit	k5eAaPmAgFnS	zabezpečit
přežití	přežití	k1gNnSc4	přežití
vlastních	vlastní	k2eAgNnPc2d1	vlastní
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
však	však	k9	však
dominantní	dominantní	k2eAgFnSc1d1	dominantní
samička	samička	k1gFnSc1	samička
vyžene	vyhnat	k5eAaPmIp3nS	vyhnat
jinou	jiný	k2eAgFnSc4d1	jiná
březí	březí	k2eAgFnSc4d1	březí
samičku	samička	k1gFnSc4	samička
<g/>
.	.	kIx.	.
</s>
<s>
Osamělá	osamělý	k2eAgFnSc1d1	osamělá
vyhnaná	vyhnaný	k2eAgFnSc1d1	vyhnaná
samička	samička	k1gFnSc1	samička
nemá	mít	k5eNaImIp3nS	mít
velké	velký	k2eAgFnPc4d1	velká
šance	šance	k1gFnPc4	šance
na	na	k7c4	na
přežití	přežití	k1gNnSc4	přežití
<g/>
.	.	kIx.	.
</s>
<s>
Buď	buď	k8xC	buď
o	o	k7c4	o
mláďata	mládě	k1gNnPc4	mládě
přijde	přijít	k5eAaPmIp3nS	přijít
nebo	nebo	k8xC	nebo
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
opustí	opustit	k5eAaPmIp3nS	opustit
vlastní	vlastní	k2eAgNnPc4d1	vlastní
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
o	o	k7c6	o
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
sama	sám	k3xTgFnSc1	sám
postarat	postarat	k5eAaPmF	postarat
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnPc4	její
mláďata	mládě	k1gNnPc4	mládě
také	také	k6eAd1	také
můžou	můžou	k?	můžou
zabít	zabít	k5eAaPmF	zabít
surikaty	surikata	k1gFnPc4	surikata
z	z	k7c2	z
jiné	jiný	k2eAgFnSc2d1	jiná
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
sama	sám	k3xTgMnSc4	sám
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaImF	stát
obětí	oběť	k1gFnSc7	oběť
dravce	dravec	k1gMnSc2	dravec
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
i	i	k9	i
zahynout	zahynout	k5eAaPmF	zahynout
na	na	k7c4	na
následky	následek	k1gInPc4	následek
podvýživy	podvýživa	k1gFnSc2	podvýživa
a	a	k8xC	a
podchlazení	podchlazení	k1gNnSc2	podchlazení
<g/>
.	.	kIx.	.
</s>
<s>
Samička	samička	k1gFnSc1	samička
bez	bez	k7c2	bez
mláďat	mládě	k1gNnPc2	mládě
má	mít	k5eAaImIp3nS	mít
šanci	šance	k1gFnSc4	šance
připojit	připojit	k5eAaPmF	připojit
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
původní	původní	k2eAgFnSc3d1	původní
rodině	rodina	k1gFnSc3	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
spojí	spojit	k5eAaPmIp3nP	spojit
s	s	k7c7	s
osamělými	osamělý	k2eAgInPc7d1	osamělý
samci	samec	k1gInPc7	samec
a	a	k8xC	a
založí	založit	k5eAaPmIp3nP	založit
novou	nový	k2eAgFnSc4d1	nová
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
ráno	ráno	k6eAd1	ráno
vylézají	vylézat	k5eAaImIp3nP	vylézat
surikaty	surikata	k1gFnPc1	surikata
z	z	k7c2	z
nor	nora	k1gFnPc2	nora
<g/>
,	,	kIx,	,
posedají	posedat	k5eAaImIp3nP	posedat
a	a	k8xC	a
sluní	slunit	k5eAaImIp3nP	slunit
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
klesá	klesat	k5eAaImIp3nS	klesat
teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
poušti	poušť	k1gFnSc6	poušť
k	k	k7c3	k
nule	nula	k1gFnSc3	nula
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Suchá	suchý	k2eAgFnSc1d1	suchá
savana	savana	k1gFnSc1	savana
<g/>
,	,	kIx,	,
polopouště	polopoušť	k1gFnPc1	polopoušť
a	a	k8xC	a
pouště	poušť	k1gFnPc1	poušť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
většina	většina	k1gFnSc1	většina
členů	člen	k1gMnPc2	člen
kolonie	kolonie	k1gFnSc2	kolonie
hledá	hledat	k5eAaImIp3nS	hledat
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
jedinci	jedinec	k1gMnPc1	jedinec
z	z	k7c2	z
kolonie	kolonie	k1gFnSc2	kolonie
jsou	být	k5eAaImIp3nP	být
pověřeni	pověřit	k5eAaPmNgMnP	pověřit
hlídkováním	hlídkování	k1gNnSc7	hlídkování
proti	proti	k7c3	proti
jejich	jejich	k3xOp3gMnPc3	jejich
predátorům	predátor	k1gMnPc3	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Surikata	surikata	k1gFnSc1	surikata
dobře	dobře	k6eAd1	dobře
vidí	vidět	k5eAaImIp3nS	vidět
do	do	k7c2	do
dálky	dálka	k1gFnSc2	dálka
<g/>
,	,	kIx,	,
hůře	zle	k6eAd2	zle
už	už	k9	už
na	na	k7c4	na
blízko	blízko	k6eAd1	blízko
<g/>
.	.	kIx.	.
</s>
<s>
Hlídky	hlídka	k1gFnPc4	hlídka
stojí	stát	k5eAaImIp3nS	stát
vzpřímené	vzpřímený	k2eAgNnSc1d1	vzpřímené
na	na	k7c6	na
vyvýšeninách	vyvýšenina	k1gFnPc6	vyvýšenina
nebo	nebo	k8xC	nebo
keřích	keř	k1gInPc6	keř
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
vydávají	vydávat	k5eAaPmIp3nP	vydávat
varovné	varovný	k2eAgNnSc4d1	varovné
pištění	pištění	k1gNnSc4	pištění
nebo	nebo	k8xC	nebo
kvokání	kvokání	k1gNnSc4	kvokání
<g/>
.	.	kIx.	.
</s>
<s>
Ostré	ostrý	k2eAgInPc1d1	ostrý
štěkavé	štěkavý	k2eAgInPc1d1	štěkavý
a	a	k8xC	a
vrčivé	vrčivý	k2eAgInPc1d1	vrčivý
zvuky	zvuk	k1gInPc1	zvuk
jsou	být	k5eAaImIp3nP	být
naléhavější	naléhavý	k2eAgInPc1d2	naléhavější
<g/>
,	,	kIx,	,
oznamují	oznamovat	k5eAaImIp3nP	oznamovat
větší	veliký	k2eAgNnSc4d2	veliký
ohrožení	ohrožení	k1gNnSc4	ohrožení
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
surikaty	surikata	k1gFnPc1	surikata
ihned	ihned	k6eAd1	ihned
ukryjí	ukrýt	k5eAaPmIp3nP	ukrýt
do	do	k7c2	do
nor	nora	k1gFnPc2	nora
<g/>
.	.	kIx.	.
</s>
