<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
forma	forma	k1gFnSc1	forma
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
vlády	vláda	k1gFnSc2	vláda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
neomezené	omezený	k2eNgFnSc2d1	neomezená
moci	moc	k1gFnSc2	moc
jednotlivec	jednotlivec	k1gMnSc1	jednotlivec
<g/>
?	?	kIx.	?
</s>
