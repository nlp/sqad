<s>
Monokracie	Monokracie	k1gFnSc1	Monokracie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
monos	monos	k1gInSc1	monos
–	–	k?	–
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
kratein	kratein	k1gMnSc1	kratein
–	–	k?	–
vládnout	vládnout	k5eAaImF	vládnout
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
jedinovláda	jedinovláda	k1gFnSc1	jedinovláda
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
samovláda	samovláda	k1gFnSc1	samovláda
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
překládá	překládat	k5eAaImIp3nS	překládat
pojem	pojem	k1gInSc1	pojem
autokracie	autokracie	k1gFnSc2	autokracie
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
však	však	k9	však
vytvářeno	vytvářen	k2eAgNnSc4d1	vytvářeno
rovnítko	rovnítko	k1gNnSc4	rovnítko
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
dvěma	dva	k4xCgInPc7	dva
pojmy	pojem	k1gInPc7	pojem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
forma	forma	k1gFnSc1	forma
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
vlády	vláda	k1gFnSc2	vláda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
neomezené	omezený	k2eNgFnSc2d1	neomezená
moci	moc	k1gFnSc2	moc
jednotlivec	jednotlivec	k1gMnSc1	jednotlivec
<g/>
.	.	kIx.	.
</s>
