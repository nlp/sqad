<s>
Bystrc	Bystrc	k1gFnSc1	Bystrc
(	(	kIx(	(
<g/>
ženský	ženský	k2eAgInSc1d1	ženský
rod	rod	k1gInSc1	rod
<g/>
,	,	kIx,	,
v	v	k7c6	v
místním	místní	k2eAgInSc6d1	místní
úzu	úzus	k1gInSc6	úzus
někdy	někdy	k6eAd1	někdy
mužský	mužský	k2eAgInSc1d1	mužský
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Bisterz	Bisterz	k1gInSc1	Bisterz
<g/>
,	,	kIx,	,
v	v	k7c6	v
hantecu	hantecus	k1gInSc6	hantecus
Bástr	Bástr	k1gInSc1	Bástr
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
historická	historický	k2eAgFnSc1d1	historická
obec	obec	k1gFnSc1	obec
<g/>
,	,	kIx,	,
městská	městský	k2eAgFnSc1d1	městská
čtvrť	čtvrť	k1gFnSc1	čtvrť
a	a	k8xC	a
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Brno-Bystrc	Brno-Bystrc	k1gFnSc1	Brno-Bystrc
také	také	k9	také
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
rozloha	rozloha	k1gFnSc1	rozloha
2724,16	[number]	k4	2724,16
hektarů	hektar	k1gInPc2	hektar
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
činí	činit	k5eAaImIp3nS	činit
vůbec	vůbec	k9	vůbec
největší	veliký	k2eAgFnSc4d3	veliký
městskou	městský	k2eAgFnSc4d1	městská
část	část	k1gFnSc4	část
i	i	k8xC	i
katastrální	katastrální	k2eAgNnSc4d1	katastrální
území	území	k1gNnSc4	území
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
této	tento	k3xDgFnSc2	tento
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
řada	řada	k1gFnSc1	řada
brněnských	brněnský	k2eAgInPc2d1	brněnský
rekreačních	rekreační	k2eAgInPc2d1	rekreační
objektů	objekt	k1gInPc2	objekt
-	-	kIx~	-
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
přehrada	přehrada	k1gFnSc1	přehrada
<g/>
,	,	kIx,	,
Zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
Brno	Brno	k1gNnSc1	Brno
situovaná	situovaný	k2eAgFnSc1d1	situovaná
na	na	k7c6	na
Mniší	mniší	k2eAgFnSc6d1	mniší
hoře	hora	k1gFnSc6	hora
<g/>
,	,	kIx,	,
Údolí	údolí	k1gNnSc6	údolí
oddechu	oddech	k1gInSc2	oddech
<g/>
,	,	kIx,	,
rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
Podkomorské	podkomorský	k2eAgInPc1d1	podkomorský
lesy	les	k1gInPc1	les
s	s	k7c7	s
areálem	areál	k1gInSc7	areál
Pohádky	pohádka	k1gFnSc2	pohádka
máje	máj	k1gInSc2	máj
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Helenčiny	Helenčin	k2eAgFnSc2d1	Helenčina
a	a	k8xC	a
Ríšovy	Ríšův	k2eAgFnSc2d1	Ríšova
studánky	studánka	k1gFnSc2	studánka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hrad	hrad	k1gInSc4	hrad
Veveří	veveří	k2eAgInSc4d1	veveří
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
Veveří	veveří	k2eAgInSc1d1	veveří
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
Brněnské	brněnský	k2eAgFnSc2d1	brněnská
přehrady	přehrada	k1gFnSc2	přehrada
však	však	k9	však
původně	původně	k6eAd1	původně
součástí	součást	k1gFnSc7	součást
Bystrce	Bystrc	k1gFnSc2	Bystrc
nebyly	být	k5eNaImAgFnP	být
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
Veveří	veveří	k2eAgInSc1d1	veveří
byl	být	k5eAaImAgInS	být
dlouho	dlouho	k6eAd1	dlouho
součástí	součást	k1gFnSc7	součást
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Veverská	Veverský	k2eAgFnSc1d1	Veverská
Bítýška	Bítýška	k1gFnSc1	Bítýška
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
Brněnské	brněnský	k2eAgFnSc2d1	brněnská
přehrady	přehrada	k1gFnSc2	přehrada
náležela	náležet	k5eAaImAgFnS	náležet
především	především	k9	především
do	do	k7c2	do
původního	původní	k2eAgInSc2d1	původní
katastru	katastr	k1gInSc2	katastr
obce	obec	k1gFnSc2	obec
Kníničky	Knínička	k1gFnSc2	Knínička
<g/>
,	,	kIx,	,
přehradou	přehrada	k1gFnSc7	přehrada
zatopené	zatopený	k2eAgFnPc1d1	zatopená
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
je	být	k5eAaImIp3nS	být
Bystrc	Bystrc	k1gFnSc1	Bystrc
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
brněnských	brněnský	k2eAgNnPc2d1	brněnské
sídlišť	sídliště	k1gNnPc2	sídliště
a	a	k8xC	a
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
bystrcké	bystrcký	k2eAgFnSc2d1	bystrcká
části	část	k1gFnSc2	část
Brněnské	brněnský	k2eAgFnSc2d1	brněnská
přehrady	přehrada	k1gFnSc2	přehrada
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
koná	konat	k5eAaImIp3nS	konat
významná	významný	k2eAgFnSc1d1	významná
několikadenní	několikadenní	k2eAgFnSc1d1	několikadenní
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
soutěžní	soutěžní	k2eAgFnSc1d1	soutěžní
přehlídka	přehlídka	k1gFnSc1	přehlídka
ohňostrojů	ohňostroj	k1gInPc2	ohňostroj
Ignis	Ignis	k1gFnSc1	Ignis
Brunensis	Brunensis	k1gFnSc1	Brunensis
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
tisíce	tisíc	k4xCgInPc4	tisíc
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
dochází	docházet	k5eAaImIp3nP	docházet
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
na	na	k7c6	na
území	území	k1gNnSc6	území
Brna	Brno	k1gNnSc2	Brno
ke	k	k7c3	k
znatelným	znatelný	k2eAgInPc3d1	znatelný
problémům	problém	k1gInPc3	problém
v	v	k7c6	v
plynulosti	plynulost	k1gFnSc6	plynulost
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
Bystrc	Bystrc	k1gFnSc1	Bystrc
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
různých	různý	k2eAgFnPc2d1	různá
částí	část	k1gFnPc2	část
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
Bystrc	Bystrc	k1gFnSc1	Bystrc
I	i	k9	i
se	s	k7c7	s
Starou	starý	k2eAgFnSc7d1	stará
Bystrcí	Bystrc	k1gFnSc7	Bystrc
představuje	představovat	k5eAaImIp3nS	představovat
původní	původní	k2eAgFnSc4d1	původní
ves	ves	k1gFnSc4	ves
okolo	okolo	k7c2	okolo
kostela	kostel	k1gInSc2	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
a	a	k8xC	a
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Evangelisty	evangelista	k1gMnSc2	evangelista
při	při	k7c6	při
řece	řeka	k1gFnSc6	řeka
Svratce	Svratka	k1gFnSc6	Svratka
a	a	k8xC	a
nejstarší	starý	k2eAgNnSc4d3	nejstarší
panelové	panelový	k2eAgNnSc4d1	panelové
sídliště	sídliště	k1gNnSc4	sídliště
až	až	k9	až
po	po	k7c4	po
tzv.	tzv.	kA	tzv.
Německou	německý	k2eAgFnSc4d1	německá
nebo	nebo	k8xC	nebo
Hitlerovu	Hitlerův	k2eAgFnSc4d1	Hitlerova
dálnici	dálnice	k1gFnSc4	dálnice
(	(	kIx(	(
<g/>
víceproudovou	víceproudový	k2eAgFnSc4d1	víceproudový
silniční	silniční	k2eAgFnSc4d1	silniční
komunikaci	komunikace	k1gFnSc4	komunikace
protínající	protínající	k2eAgFnSc4d1	protínající
Bystrc	Bystrc	k1gFnSc4	Bystrc
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Bystrc	Bystrc	k1gFnSc1	Bystrc
II	II	kA	II
je	být	k5eAaImIp3nS	být
novější	nový	k2eAgNnSc4d2	novější
panelové	panelový	k2eAgNnSc4d1	panelové
sídliště	sídliště	k1gNnSc4	sídliště
západně	západně	k6eAd1	západně
od	od	k7c2	od
Německé	německý	k2eAgFnSc2d1	německá
dálnice	dálnice	k1gFnSc2	dálnice
až	až	k9	až
po	po	k7c4	po
tramvajovou	tramvajový	k2eAgFnSc4d1	tramvajová
smyčku	smyčka	k1gFnSc4	smyčka
Bystrc-Ečerova	Bystrc-Ečerův	k2eAgFnSc1d1	Bystrc-Ečerův
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
začala	začít	k5eAaPmAgFnS	začít
výstavba	výstavba	k1gFnSc1	výstavba
sídliště	sídliště	k1gNnSc2	sídliště
Kamechy	Kamech	k1gInPc1	Kamech
Bystrc	Bystrc	k1gFnSc1	Bystrc
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
finální	finální	k2eAgFnSc6d1	finální
podobě	podoba	k1gFnSc6	podoba
pokrývat	pokrývat	k5eAaImF	pokrývat
prostor	prostor	k1gInSc4	prostor
za	za	k7c7	za
panelovými	panelový	k2eAgInPc7d1	panelový
domy	dům	k1gInPc7	dům
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Teyschlova	Teyschlův	k2eAgFnSc1d1	Teyschlova
<g/>
)	)	kIx)	)
vystavěnými	vystavěný	k2eAgInPc7d1	vystavěný
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Žebětínu	Žebětín	k1gInSc3	Žebětín
<g/>
.	.	kIx.	.
</s>
<s>
Radnice	radnice	k1gFnSc1	radnice
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
kostela	kostel	k1gInSc2	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
a	a	k8xC	a
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Evangelisty	evangelista	k1gMnSc2	evangelista
při	při	k7c6	při
řece	řeka	k1gFnSc6	řeka
Svratce	Svratka	k1gFnSc6	Svratka
ve	v	k7c6	v
Staré	Staré	k2eAgFnSc6d1	Staré
Bystrci	Bystrc	k1gFnSc6	Bystrc
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c4	o
vinařskou	vinařský	k2eAgFnSc4d1	vinařská
obec	obec	k1gFnSc4	obec
ve	v	k7c6	v
Velkopavlovické	Velkopavlovický	k2eAgFnSc6d1	Velkopavlovická
vinařské	vinařský	k2eAgFnSc6d1	vinařská
podoblasti	podoblast	k1gFnSc6	podoblast
(	(	kIx(	(
<g/>
viniční	viniční	k2eAgFnSc2d1	viniční
tratě	trať	k1gFnSc2	trať
Díly	díl	k1gInPc1	díl
<g/>
,	,	kIx,	,
Kamenice	Kamenice	k1gFnPc1	Kamenice
<g/>
,	,	kIx,	,
Pod	pod	k7c7	pod
Mniší	mniší	k2eAgFnSc7d1	mniší
horou	hora	k1gFnSc7	hora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
senátních	senátní	k2eAgFnPc2d1	senátní
voleb	volba	k1gFnPc2	volba
je	být	k5eAaImIp3nS	být
území	území	k1gNnSc1	území
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-Bystrc	Brno-Bystrc	k1gFnSc1	Brno-Bystrc
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
do	do	k7c2	do
volebního	volební	k2eAgInSc2d1	volební
obvodu	obvod	k1gInSc2	obvod
číslo	číslo	k1gNnSc1	číslo
55	[number]	k4	55
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
polovina	polovina	k1gFnSc1	polovina
znaku	znak	k1gInSc2	znak
se	se	k3xPyFc4	se
shoduje	shodovat	k5eAaImIp3nS	shodovat
se	s	k7c7	s
znakem	znak	k1gInSc7	znak
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
(	(	kIx(	(
<g/>
bílý	bílý	k2eAgInSc1d1	bílý
a	a	k8xC	a
červený	červený	k2eAgInSc1d1	červený
pruh	pruh	k1gInSc1	pruh
<g/>
)	)	kIx)	)
další	další	k2eAgInSc1d1	další
bílý	bílý	k2eAgInSc1d1	bílý
pruh	pruh	k1gInSc1	pruh
je	být	k5eAaImIp3nS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
výplní	výplň	k1gFnSc7	výplň
nad	nad	k7c7	nad
modrou	modrý	k2eAgFnSc7d1	modrá
hladinou	hladina	k1gFnSc7	hladina
vzduté	vzdutý	k2eAgFnSc2d1	vzdutá
vodní	vodní	k2eAgFnSc2d1	vodní
plochy	plocha	k1gFnSc2	plocha
-	-	kIx~	-
posledního	poslední	k2eAgNnSc2d1	poslední
modrého	modré	k1gNnSc2	modré
pruhu	pruh	k1gInSc2	pruh
u	u	k7c2	u
dna	dno	k1gNnSc2	dno
znaku	znak	k1gInSc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Prameny	pramen	k1gInPc1	pramen
doložená	doložená	k1gFnSc1	doložená
historie	historie	k1gFnSc2	historie
Bystrce	Bystrc	k1gFnSc2	Bystrc
a	a	k8xC	a
původ	původ	k1gInSc4	původ
tohoto	tento	k3xDgNnSc2	tento
místního	místní	k2eAgNnSc2d1	místní
jména	jméno	k1gNnSc2	jméno
začíná	začínat	k5eAaImIp3nS	začínat
rokem	rok	k1gInSc7	rok
1373	[number]	k4	1373
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
moravský	moravský	k2eAgMnSc1d1	moravský
markrabě	markrabě	k1gMnSc1	markrabě
Jan	Jan	k1gMnSc1	Jan
Jindřich	Jindřich	k1gMnSc1	Jindřich
daroval	darovat	k5eAaPmAgMnS	darovat
Bystrc	Bystrc	k1gFnSc4	Bystrc
augustiniánskému	augustiniánský	k2eAgInSc3d1	augustiniánský
klášteru	klášter	k1gInSc3	klášter
u	u	k7c2	u
sv.	sv.	kA	sv.
Tomáše	Tomáš	k1gMnSc2	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
Bystrc	Bystrc	k1gFnSc1	Bystrc
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
panství	panství	k1gNnSc2	panství
hradu	hrad	k1gInSc2	hrad
Veveří	veveří	k2eAgNnSc1d1	veveří
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
zeměpanského	zeměpanský	k2eAgInSc2d1	zeměpanský
<g/>
,	,	kIx,	,
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
různými	různý	k2eAgMnPc7d1	různý
majiteli	majitel	k1gMnPc7	majitel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
se	se	k3xPyFc4	se
Bystrc	Bystrc	k1gFnSc1	Bystrc
vykoupila	vykoupit	k5eAaPmAgFnS	vykoupit
z	z	k7c2	z
povinností	povinnost	k1gFnPc2	povinnost
vůči	vůči	k7c3	vůči
veverské	veverský	k2eAgFnSc3d1	veverská
vrchnosti	vrchnost	k1gFnSc3	vrchnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
Protektorátu	protektorát	k1gInSc6	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
území	území	k1gNnSc6	území
Bystrce	Bystrc	k1gFnSc2	Bystrc
vybudován	vybudován	k2eAgInSc1d1	vybudován
nedokončený	dokončený	k2eNgInSc1d1	nedokončený
úsek	úsek	k1gInSc1	úsek
nedokončené	dokončený	k2eNgFnSc2d1	nedokončená
Exteritoriální	exteritoriální	k2eAgFnSc2d1	exteritoriální
dálnice	dálnice	k1gFnSc2	dálnice
Vídeň-Vratislav	Vídeň-Vratislava	k1gFnPc2	Vídeň-Vratislava
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
osvobození	osvobození	k1gNnSc3	osvobození
Bystrce	Bystrc	k1gFnSc2	Bystrc
došlo	dojít	k5eAaPmAgNnS	dojít
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yQnSc4	což
upomíná	upomínat	k5eAaImIp3nS	upomínat
název	název	k1gInSc1	název
dejšího	dejší	k2eAgNnSc2d1	dejší
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
výstavby	výstavba	k1gFnSc2	výstavba
přehrady	přehrada	k1gFnSc2	přehrada
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
skály	skála	k1gFnSc2	skála
nad	nad	k7c7	nad
lagunou	laguna	k1gFnSc7	laguna
na	na	k7c6	na
Kozí	kozí	k2eAgFnSc6d1	kozí
horce	horka	k1gFnSc6	horka
navržena	navržen	k2eAgFnSc1d1	navržena
vilová	vilový	k2eAgFnSc1d1	vilová
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
její	její	k3xOp3gFnSc3	její
realizaci	realizace	k1gFnSc3	realizace
ale	ale	k8xC	ale
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Domy	dům	k1gInPc4	dům
tam	tam	k6eAd1	tam
navrhovali	navrhovat	k5eAaImAgMnP	navrhovat
většinou	většinou	k6eAd1	většinou
uznávaní	uznávaný	k2eAgMnPc1d1	uznávaný
brněnští	brněnský	k2eAgMnPc1d1	brněnský
moderní	moderní	k2eAgMnPc1d1	moderní
architekti	architekt	k1gMnPc1	architekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
slavný	slavný	k2eAgMnSc1d1	slavný
architekt	architekt	k1gMnSc1	architekt
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Fuchs	Fuchs	k1gMnSc1	Fuchs
o	o	k7c4	o
něco	něco	k3yInSc4	něco
dále	daleko	k6eAd2	daleko
rekreační	rekreační	k2eAgFnSc4d1	rekreační
vilku	vilka	k1gFnSc4	vilka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
o	o	k7c4	o
rok	rok	k1gInSc4	rok
a	a	k8xC	a
půl	půl	k1xP	půl
později	pozdě	k6eAd2	pozdě
postavena	postaven	k2eAgFnSc1d1	postavena
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1957	[number]	k4	1957
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
krajského	krajský	k2eAgInSc2d1	krajský
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
k	k	k7c3	k
Brnu	Brno	k1gNnSc3	Brno
připojena	připojit	k5eAaPmNgFnS	připojit
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
oblast	oblast	k1gFnSc1	oblast
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Brněnské	brněnský	k2eAgFnSc2d1	brněnská
přehrady	přehrada	k1gFnSc2	přehrada
zahrnující	zahrnující	k2eAgFnSc2d1	zahrnující
vedle	vedle	k7c2	vedle
většiny	většina	k1gFnSc2	většina
katastru	katastr	k1gInSc2	katastr
Bystrce	Bystrc	k1gFnSc2	Bystrc
také	také	k9	také
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
katastru	katastr	k1gInSc2	katastr
obce	obec	k1gFnSc2	obec
Kníničky	Knínička	k1gFnSc2	Knínička
<g/>
,	,	kIx,	,
malou	malý	k2eAgFnSc4d1	malá
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
katastru	katastr	k1gInSc2	katastr
obce	obec	k1gFnSc2	obec
Veverská	Veverský	k2eAgFnSc1d1	Veverská
Bítýška	Bítýška	k1gFnSc1	Bítýška
<g/>
,	,	kIx,	,
jihozápadní	jihozápadní	k2eAgFnSc1d1	jihozápadní
část	část	k1gFnSc1	část
katastru	katastr	k1gInSc2	katastr
obce	obec	k1gFnSc2	obec
Rozdrojovice	Rozdrojovice	k1gFnSc1	Rozdrojovice
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
katastru	katastr	k1gInSc2	katastr
obce	obec	k1gFnSc2	obec
Moravské	moravský	k2eAgFnSc2d1	Moravská
Knínice	Knínice	k1gFnSc2	Knínice
a	a	k8xC	a
jižní	jižní	k2eAgInSc1d1	jižní
cíp	cíp	k1gInSc1	cíp
katastru	katastr	k1gInSc2	katastr
obce	obec	k1gFnSc2	obec
Chudčice	Chudčice	k1gFnSc2	Chudčice
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
spravováno	spravovat	k5eAaImNgNnS	spravovat
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
městského	městský	k2eAgInSc2d1	městský
obvodu	obvod	k1gInSc2	obvod
Brno	Brno	k1gNnSc4	Brno
VII	VII	kA	VII
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1960	[number]	k4	1960
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
Brnu	Brno	k1gNnSc3	Brno
připojena	připojen	k2eAgFnSc1d1	připojena
i	i	k8xC	i
samotná	samotný	k2eAgFnSc1d1	samotná
Bystrc	Bystrc	k1gFnSc1	Bystrc
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
až	až	k9	až
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1976	[number]	k4	1976
tvořila	tvořit	k5eAaImAgFnS	tvořit
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sousedními	sousední	k2eAgFnPc7d1	sousední
Kníničkami	Knínička	k1gFnPc7	Knínička
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
výše	vysoce	k6eAd2	vysoce
připojenými	připojený	k2eAgFnPc7d1	připojená
částmi	část	k1gFnPc7	část
katastrů	katastr	k1gInPc2	katastr
sousedních	sousední	k2eAgFnPc2d1	sousední
obcí	obec	k1gFnPc2	obec
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Brna	Brno	k1gNnSc2	Brno
existovala	existovat	k5eAaImAgNnP	existovat
nově	nově	k6eAd1	nově
zřízená	zřízený	k2eAgNnPc1d1	zřízené
katastrální	katastrální	k2eAgNnPc1d1	katastrální
území	území	k1gNnPc1	území
Chudčice	Chudčice	k1gFnSc2	Chudčice
I	I	kA	I
<g/>
,	,	kIx,	,
Moravské	moravský	k2eAgFnSc2d1	Moravská
Knínice	Knínice	k1gFnSc2	Knínice
I	i	k9	i
<g/>
,	,	kIx,	,
Rozdrojovice	Rozdrojovice	k1gFnSc1	Rozdrojovice
I	i	k9	i
a	a	k8xC	a
Veverská	Veverský	k2eAgFnSc1d1	Veverská
Bítýška	Bítýška	k1gFnSc1	Bítýška
I	i	k9	i
<g/>
)	)	kIx)	)
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
městskou	městský	k2eAgFnSc4d1	městská
část	část	k1gFnSc4	část
Bystrc	Bystrc	k1gFnSc1	Bystrc
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
místním	místní	k2eAgInSc7d1	místní
národním	národní	k2eAgInSc7d1	národní
výborem	výbor	k1gInSc7	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
od	od	k7c2	od
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1971	[number]	k4	1971
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1972	[number]	k4	1972
nazývala	nazývat	k5eAaImAgFnS	nazývat
Brno	Brno	k1gNnSc4	Brno
VII-Bystrc	VII-Bystrc	k1gInSc1	VII-Bystrc
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1972	[number]	k4	1972
již	již	k6eAd1	již
jen	jen	k9	jen
Brno-Bystrc	Brno-Bystrc	k1gFnSc1	Brno-Bystrc
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgFnPc4d1	současná
katastrální	katastrální	k2eAgFnPc4d1	katastrální
hranice	hranice	k1gFnPc4	hranice
získala	získat	k5eAaPmAgFnS	získat
Bystrc	Bystrc	k1gFnSc1	Bystrc
při	při	k7c6	při
radikální	radikální	k2eAgFnSc6d1	radikální
katastrální	katastrální	k2eAgFnSc6d1	katastrální
reformě	reforma	k1gFnSc6	reforma
Brna	Brno	k1gNnSc2	Brno
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
upravený	upravený	k2eAgInSc4d1	upravený
katastr	katastr	k1gInSc4	katastr
Bystrce	Bystrc	k1gFnSc2	Bystrc
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
vedle	vedle	k7c2	vedle
téměř	téměř	k6eAd1	téměř
celého	celý	k2eAgInSc2d1	celý
původního	původní	k2eAgInSc2d1	původní
bystrckého	bystrcký	k2eAgInSc2d1	bystrcký
katastru	katastr	k1gInSc2	katastr
také	také	k9	také
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
původního	původní	k2eAgNnSc2d1	původní
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Kníničky	Knínička	k1gFnSc2	Knínička
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc4d1	celé
zrušené	zrušený	k2eAgNnSc4d1	zrušené
katastrální	katastrální	k2eAgNnSc4d1	katastrální
území	území	k1gNnSc4	území
Veverská	Veverský	k2eAgFnSc1d1	Veverská
Bítýška	Bítýška	k1gFnSc1	Bítýška
I	i	k9	i
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
zrušeného	zrušený	k2eAgNnSc2d1	zrušené
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Chudčice	Chudčice	k1gFnSc2	Chudčice
I.	I.	kA	I.
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
území	území	k1gNnSc6	území
Bystrce	Bystrc	k1gFnSc2	Bystrc
využita	využít	k5eAaPmNgFnS	využít
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
výše	výše	k1gFnSc2	výše
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
nedokončeného	dokončený	k2eNgInSc2d1	nedokončený
úseku	úsek	k1gInSc2	úsek
exteritoriální	exteritoriální	k2eAgFnSc2d1	exteritoriální
dálnice	dálnice	k1gFnSc2	dálnice
pro	pro	k7c4	pro
vybudování	vybudování	k1gNnSc4	vybudování
nové	nový	k2eAgFnSc2d1	nová
částečně	částečně	k6eAd1	částečně
víceproudé	víceproudý	k2eAgFnSc2d1	víceproudá
silnice	silnice	k1gFnSc2	silnice
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
Bystrcí	Bystrc	k1gFnSc7	Bystrc
prochází	procházet	k5eAaImIp3nS	procházet
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
15	[number]	k4	15
<g/>
.	.	kIx.	.
červenci	červenec	k1gInSc3	červenec
1976	[number]	k4	1976
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
místní	místní	k2eAgInSc1d1	místní
národní	národní	k2eAgInSc1d1	národní
výbor	výbor	k1gInSc1	výbor
v	v	k7c6	v
Bystrci	Bystrc	k1gFnSc6	Bystrc
<g/>
,	,	kIx,	,
a	a	k8xC	a
Bystrc	Bystrc	k1gFnSc1	Bystrc
i	i	k8xC	i
Kníničky	Knínička	k1gFnPc1	Knínička
byly	být	k5eAaImAgFnP	být
připojeny	připojit	k5eAaPmNgFnP	připojit
k	k	k7c3	k
městskému	městský	k2eAgInSc3d1	městský
obvodu	obvod	k1gInSc3	obvod
Brno	Brno	k1gNnSc4	Brno
II	II	kA	II
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
rámci	rámec	k1gInSc6	rámec
setrvaly	setrvat	k5eAaPmAgInP	setrvat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
obyvatelé	obyvatel	k1gMnPc1	obyvatel
obou	dva	k4xCgFnPc2	dva
čtvrtí	čtvrt	k1gFnPc2	čtvrt
vymohli	vymoct	k5eAaPmAgMnP	vymoct
vytvoření	vytvoření	k1gNnSc4	vytvoření
novodobých	novodobý	k2eAgFnPc2d1	novodobá
samosprávných	samosprávný	k2eAgFnPc2d1	samosprávná
městských	městský	k2eAgFnPc2d1	městská
části	část	k1gFnSc2	část
Brno-Bystrc	Brno-Bystrc	k1gFnSc4	Brno-Bystrc
a	a	k8xC	a
Brno-Kníničky	Brno-Knínička	k1gFnPc4	Brno-Knínička
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
dni	den	k1gInSc3	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2015	[number]	k4	2015
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
nová	nový	k2eAgFnSc1d1	nová
katastrální	katastrální	k2eAgFnSc1d1	katastrální
a	a	k8xC	a
správní	správní	k2eAgFnSc1d1	správní
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
Bystrcí	Bystrc	k1gFnSc7	Bystrc
a	a	k8xC	a
Žebětínem	Žebětín	k1gInSc7	Žebětín
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
Kamechy	Kamech	k1gInPc4	Kamech
<g/>
.	.	kIx.	.
</s>
<s>
Bystrc	Bystrc	k1gFnSc1	Bystrc
si	se	k3xPyFc3	se
i	i	k9	i
přes	přes	k7c4	přes
trvalý	trvalý	k2eAgInSc4d1	trvalý
rozvoj	rozvoj	k1gInSc4	rozvoj
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
charakter	charakter	k1gInSc4	charakter
klidné	klidný	k2eAgFnSc2d1	klidná
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brna	Brno	k1gNnSc2	Brno
s	s	k7c7	s
přírodním	přírodní	k2eAgNnSc7d1	přírodní
okolím	okolí	k1gNnSc7	okolí
obytné	obytný	k2eAgFnSc2d1	obytná
zástavby	zástavba	k1gFnSc2	zástavba
<g/>
,	,	kIx,	,
dostatkem	dostatek	k1gInSc7	dostatek
zeleně	zeleň	k1gFnSc2	zeleň
a	a	k8xC	a
příležitostí	příležitost	k1gFnPc2	příležitost
k	k	k7c3	k
rekreaci	rekreace	k1gFnSc3	rekreace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bystrci	Bystrc	k1gFnSc6	Bystrc
funguje	fungovat	k5eAaImIp3nS	fungovat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k4c4	málo
společností	společnost	k1gFnPc2	společnost
a	a	k8xC	a
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
vytvářely	vytvářet	k5eAaImAgFnP	vytvářet
pracovní	pracovní	k2eAgNnSc4d1	pracovní
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
91	[number]	k4	91
%	%	kIx~	%
zaměstnaných	zaměstnaný	k2eAgMnPc2d1	zaměstnaný
obyvatel	obyvatel	k1gMnPc2	obyvatel
Bystrce	Bystrc	k1gFnSc2	Bystrc
vyjíždí	vyjíždět	k5eAaImIp3nS	vyjíždět
za	za	k7c7	za
prací	práce	k1gFnSc7	práce
mimo	mimo	k7c4	mimo
městskou	městský	k2eAgFnSc4d1	městská
část	část	k1gFnSc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
věkové	věkový	k2eAgFnSc3d1	věková
struktuře	struktura	k1gFnSc3	struktura
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
a	a	k8xC	a
nedostatku	nedostatek	k1gInSc2	nedostatek
pracovních	pracovní	k2eAgFnPc2d1	pracovní
příležitostí	příležitost	k1gFnPc2	příležitost
funguje	fungovat	k5eAaImIp3nS	fungovat
Bystrc	Bystrc	k1gFnSc4	Bystrc
jako	jako	k8xS	jako
zázemí	zázemí	k1gNnSc4	zázemí
pro	pro	k7c4	pro
pracovní	pracovní	k2eAgFnSc4d1	pracovní
sílu	síla	k1gFnSc4	síla
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Jednoznačně	jednoznačně	k6eAd1	jednoznačně
dominantní	dominantní	k2eAgFnSc1d1	dominantní
funkcí	funkce	k1gFnSc7	funkce
MČ	MČ	kA	MČ
je	být	k5eAaImIp3nS	být
funkce	funkce	k1gFnSc1	funkce
obytná	obytný	k2eAgFnSc1d1	obytná
a	a	k8xC	a
trh	trh	k1gInSc1	trh
práce	práce	k1gFnSc2	práce
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
marginální	marginální	k2eAgNnSc1d1	marginální
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bystrci	Bystrc	k1gFnSc6	Bystrc
fungují	fungovat	k5eAaImIp3nP	fungovat
sportovní	sportovní	k2eAgInPc1d1	sportovní
kluby	klub	k1gInPc1	klub
(	(	kIx(	(
<g/>
FC	FC	kA	FC
Dosta	Dosta	k1gFnSc1	Dosta
Bystrc-Kníničky	Bystrc-Knínička	k1gFnSc2	Bystrc-Knínička
<g/>
,	,	kIx,	,
Rugby	rugby	k1gNnSc1	rugby
Club	club	k1gInSc1	club
Bystrc	Bystrc	k1gFnSc1	Bystrc
<g/>
,	,	kIx,	,
šachový	šachový	k2eAgInSc4d1	šachový
klub	klub	k1gInSc4	klub
Bystrc	Bystrc	k1gFnSc1	Bystrc
Oilers	Oilers	k1gInSc1	Oilers
<g/>
,	,	kIx,	,
veslařský	veslařský	k2eAgInSc1d1	veslařský
klub	klub	k1gInSc1	klub
Draci	drak	k1gMnPc1	drak
Bystrc	Bystrc	k1gFnSc4	Bystrc
<g/>
)	)	kIx)	)
a	a	k8xC	a
zájmové	zájmový	k2eAgFnSc2d1	zájmová
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Bystrce	Bystrc	k1gFnSc2	Bystrc
mají	mít	k5eAaImIp3nP	mít
možnost	možnost	k1gFnSc4	možnost
využívat	využívat	k5eAaImF	využívat
poměrně	poměrně	k6eAd1	poměrně
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
sportovní	sportovní	k2eAgFnSc4d1	sportovní
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
v	v	k7c6	v
Bystrci	Bystrc	k1gFnSc6	Bystrc
nachází	nacházet	k5eAaImIp3nS	nacházet
17	[number]	k4	17
sportovních	sportovní	k2eAgNnPc2d1	sportovní
hřišť	hřiště	k1gNnPc2	hřiště
<g/>
,	,	kIx,	,
lezecká	lezecký	k2eAgFnSc1d1	lezecká
stěna	stěna	k1gFnSc1	stěna
(	(	kIx(	(
<g/>
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Vondrákova	Vondrákův	k2eAgFnSc1d1	Vondrákova
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skateboardový	skateboardový	k2eAgMnSc1d1	skateboardový
(	(	kIx(	(
<g/>
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Vejrostova	Vejrostův	k2eAgFnSc1d1	Vejrostova
<g/>
)	)	kIx)	)
a	a	k8xC	a
dirt	dirt	k2eAgInSc1d1	dirt
areál	areál	k1gInSc1	areál
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
kulturní	kulturní	k2eAgInSc1d1	kulturní
život	život	k1gInSc1	život
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
ve	v	k7c6	v
Společenském	společenský	k2eAgNnSc6d1	společenské
centru	centrum	k1gNnSc6	centrum
na	na	k7c6	na
Odbojářské	odbojářský	k2eAgFnSc6d1	Odbojářská
ulici	ulice	k1gFnSc6	ulice
(	(	kIx(	(
<g/>
koncerty	koncert	k1gInPc1	koncert
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgNnPc1d1	divadelní
představení	představení	k1gNnPc1	představení
<g/>
,	,	kIx,	,
výstavy	výstava	k1gFnPc1	výstava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
pobočce	pobočka	k1gFnSc6	pobočka
Mahenovy	Mahenův	k2eAgFnSc2d1	Mahenova
knihovny	knihovna	k1gFnSc2	knihovna
(	(	kIx(	(
<g/>
výstavy	výstava	k1gFnPc1	výstava
<g/>
,	,	kIx,	,
vernisáže	vernisáž	k1gFnPc1	vernisáž
<g/>
,	,	kIx,	,
přednášky	přednáška	k1gFnPc1	přednáška
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Veveří	veveří	k2eAgNnSc1d1	veveří
(	(	kIx(	(
<g/>
koncerty	koncert	k1gInPc1	koncert
<g/>
,	,	kIx,	,
zábavné	zábavný	k2eAgFnPc1d1	zábavná
akce	akce	k1gFnPc1	akce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kulturní	kulturní	k2eAgInSc1d1	kulturní
odbor	odbor	k1gInSc1	odbor
úřadu	úřad	k1gInSc2	úřad
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
pořádá	pořádat	k5eAaImIp3nS	pořádat
hody	hod	k1gInPc4	hod
a	a	k8xC	a
vánoční	vánoční	k2eAgInPc4d1	vánoční
trhy	trh	k1gInPc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bystrci	Bystrc	k1gFnSc6	Bystrc
neexistuje	existovat	k5eNaImIp3nS	existovat
lokální	lokální	k2eAgFnSc1d1	lokální
kabelová	kabelový	k2eAgFnSc1d1	kabelová
televize	televize	k1gFnSc1	televize
ani	ani	k8xC	ani
místní	místní	k2eAgNnSc1d1	místní
rádio	rádio	k1gNnSc1	rádio
<g/>
;	;	kIx,	;
nevychází	vycházet	k5eNaImIp3nS	vycházet
žádný	žádný	k3yNgInSc1	žádný
nezávislý	závislý	k2eNgInSc1d1	nezávislý
místní	místní	k2eAgInSc1d1	místní
papírový	papírový	k2eAgInSc1d1	papírový
tisk	tisk	k1gInSc1	tisk
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
událostech	událost	k1gFnPc6	událost
informuje	informovat	k5eAaBmIp3nS	informovat
internetový	internetový	k2eAgInSc1d1	internetový
magazín	magazín	k1gInSc1	magazín
Bystrčník	Bystrčník	k1gInSc1	Bystrčník
(	(	kIx(	(
<g/>
vydává	vydávat	k5eAaImIp3nS	vydávat
občanské	občanský	k2eAgNnSc1d1	občanské
sdružení	sdružení	k1gNnSc1	sdružení
Horní	horní	k2eAgNnSc1d1	horní
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
)	)	kIx)	)
a	a	k8xC	a
radniční	radniční	k2eAgNnSc1d1	radniční
periodikum	periodikum	k1gNnSc1	periodikum
Bystrcké	bystrcký	k2eAgFnSc2d1	bystrcká
noviny	novina	k1gFnSc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Dění	dění	k1gNnSc1	dění
v	v	k7c6	v
Bystrci	Bystrc	k1gFnSc6	Bystrc
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
mediálně	mediálně	k6eAd1	mediálně
mapováno	mapovat	k5eAaImNgNnS	mapovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
širšího	široký	k2eAgInSc2d2	širší
celku	celek	k1gInSc2	celek
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Brněnským	brněnský	k2eAgInSc7d1	brněnský
deníkem	deník	k1gInSc7	deník
Rovnost	rovnost	k1gFnSc1	rovnost
(	(	kIx(	(
<g/>
vydává	vydávat	k5eAaImIp3nS	vydávat
Vltava-Labe-Press	Vltava-Labe-Press	k1gInSc1	Vltava-Labe-Press
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bystrc	Bystrc	k1gFnSc1	Bystrc
je	být	k5eAaImIp3nS	být
kompletně	kompletně	k6eAd1	kompletně
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
kabelovou	kabelový	k2eAgFnSc7d1	kabelová
televizí	televize	k1gFnSc7	televize
UPC	UPC	kA	UPC
a	a	k8xC	a
NETBOX	NETBOX	kA	NETBOX
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgNnPc4d1	hlavní
témata	téma	k1gNnPc4	téma
současné	současný	k2eAgFnSc2d1	současná
Bystrce	Bystrc	k1gFnSc2	Bystrc
patří	patřit	k5eAaImIp3nS	patřit
spor	spor	k1gInSc1	spor
kolem	kolem	k7c2	kolem
dostavby	dostavba	k1gFnSc2	dostavba
dálnice	dálnice	k1gFnSc2	dálnice
D	D	kA	D
<g/>
43	[number]	k4	43
<g/>
,	,	kIx,	,
čistota	čistota	k1gFnSc1	čistota
brněnské	brněnský	k2eAgFnSc2d1	brněnská
přehrady	přehrada	k1gFnSc2	přehrada
a	a	k8xC	a
kontroverze	kontroverze	k1gFnSc2	kontroverze
kolem	kolem	k7c2	kolem
výstavby	výstavba	k1gFnSc2	výstavba
v	v	k7c6	v
citlivých	citlivý	k2eAgFnPc6d1	citlivá
lokalitách	lokalita	k1gFnPc6	lokalita
Staré	Staré	k2eAgFnSc2d1	Staré
Bystrce	Bystrc	k1gFnSc2	Bystrc
a	a	k8xC	a
Horního	horní	k2eAgNnSc2d1	horní
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
,	,	kIx,	,
výstavba	výstavba	k1gFnSc1	výstavba
nových	nový	k2eAgInPc2d1	nový
sídelních	sídelní	k2eAgInPc2d1	sídelní
celků	celek	k1gInPc2	celek
Kamechy	Kamech	k1gInPc1	Kamech
a	a	k8xC	a
Panorama	panorama	k1gNnSc1	panorama
nad	nad	k7c7	nad
přehradou	přehrada	k1gFnSc7	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
sídliště	sídliště	k1gNnSc2	sídliště
Kamechy	Kamech	k1gInPc4	Kamech
se	se	k3xPyFc4	se
debatuje	debatovat	k5eAaImIp3nS	debatovat
o	o	k7c4	o
prodloužení	prodloužení	k1gNnSc4	prodloužení
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
linky	linka	k1gFnSc2	linka
<g/>
.	.	kIx.	.
</s>
<s>
Frekventovaným	frekventovaný	k2eAgNnSc7d1	frekventované
lokálním	lokální	k2eAgNnSc7d1	lokální
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
čistota	čistota	k1gFnSc1	čistota
veřejných	veřejný	k2eAgFnPc2d1	veřejná
ploch	plocha	k1gFnPc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
ostré	ostrý	k2eAgInPc4d1	ostrý
konflikty	konflikt	k1gInPc4	konflikt
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
aktuální	aktuální	k2eAgInSc1d1	aktuální
nedostatek	nedostatek	k1gInSc1	nedostatek
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
mateřských	mateřský	k2eAgFnPc6d1	mateřská
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Diskutován	diskutován	k2eAgMnSc1d1	diskutován
bývá	bývat	k5eAaImIp3nS	bývat
nefungující	fungující	k2eNgInSc1d1	nefungující
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
částečně	částečně	k6eAd1	částečně
fungující	fungující	k2eAgInSc1d1	fungující
komunitní	komunitní	k2eAgInSc1d1	komunitní
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
částečný	částečný	k2eAgInSc1d1	částečný
důvod	důvod	k1gInSc1	důvod
bývá	bývat	k5eAaImIp3nS	bývat
spatřován	spatřovat	k5eAaImNgInS	spatřovat
v	v	k7c6	v
nedostatečné	dostatečný	k2eNgFnSc6d1	nedostatečná
infrastruktuře	infrastruktura	k1gFnSc6	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
sídlištích	sídliště	k1gNnPc6	sídliště
vznikají	vznikat	k5eAaImIp3nP	vznikat
spory	spora	k1gFnPc1	spora
kolem	kolem	k7c2	kolem
hlučných	hlučný	k2eAgFnPc2d1	hlučná
či	či	k8xC	či
jinak	jinak	k6eAd1	jinak
obtěžujících	obtěžující	k2eAgFnPc2d1	obtěžující
komerčních	komerční	k2eAgFnPc2d1	komerční
či	či	k8xC	či
občanských	občanský	k2eAgFnPc2d1	občanská
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Diskuze	diskuze	k1gFnPc1	diskuze
se	se	k3xPyFc4	se
vedou	vést	k5eAaImIp3nP	vést
nad	nad	k7c7	nad
financováním	financování	k1gNnSc7	financování
a	a	k8xC	a
smyslem	smysl	k1gInSc7	smysl
festivalu	festival	k1gInSc2	festival
Ignis	Ignis	k1gFnSc2	Ignis
Brunensis	Brunensis	k1gFnPc2	Brunensis
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
nespokojenost	nespokojenost	k1gFnSc1	nespokojenost
se	s	k7c7	s
stavem	stav	k1gInSc7	stav
brněnské	brněnský	k2eAgFnSc2d1	brněnská
zoologické	zoologický	k2eAgFnSc2d1	zoologická
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
rozepře	rozepře	k1gFnSc1	rozepře
mezi	mezi	k7c7	mezi
příznivci	příznivec	k1gMnPc7	příznivec
a	a	k8xC	a
odpůrci	odpůrce	k1gMnPc7	odpůrce
zamýšlené	zamýšlený	k2eAgFnSc2d1	zamýšlená
lanové	lanový	k2eAgFnSc2d1	lanová
dráhy	dráha	k1gFnSc2	dráha
na	na	k7c6	na
Brněnské	brněnský	k2eAgFnSc6d1	brněnská
přehradě	přehrada	k1gFnSc6	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bystrci	Bystrc	k1gFnSc6	Bystrc
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
neexistuje	existovat	k5eNaImIp3nS	existovat
veřejné	veřejný	k2eAgNnSc4d1	veřejné
pohřebiště	pohřebiště	k1gNnSc4	pohřebiště
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
diskutuje	diskutovat	k5eAaImIp3nS	diskutovat
o	o	k7c6	o
možnostech	možnost	k1gFnPc6	možnost
jeho	jeho	k3xOp3gNnSc2	jeho
zřízení	zřízení	k1gNnSc2	zřízení
či	či	k8xC	či
obnovení	obnovení	k1gNnSc2	obnovení
<g/>
.	.	kIx.	.
</s>
<s>
Diskuze	diskuze	k1gFnPc1	diskuze
se	se	k3xPyFc4	se
vedou	vést	k5eAaImIp3nP	vést
kolem	kolem	k7c2	kolem
plánu	plán	k1gInSc2	plán
na	na	k7c4	na
přímé	přímý	k2eAgNnSc4d1	přímé
propojení	propojení	k1gNnSc4	propojení
Bystrce	Bystrc	k1gFnSc2	Bystrc
a	a	k8xC	a
Medlánek	Medlánka	k1gFnPc2	Medlánka
komunikací	komunikace	k1gFnPc2	komunikace
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
rozšíření	rozšíření	k1gNnSc4	rozšíření
stávající	stávající	k2eAgFnSc2d1	stávající
panelové	panelový	k2eAgFnSc2d1	panelová
silnice	silnice	k1gFnSc2	silnice
spojující	spojující	k2eAgFnPc1d1	spojující
obě	dva	k4xCgFnPc1	dva
městské	městský	k2eAgFnPc1d1	městská
části	část	k1gFnPc1	část
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	Tom	k1gMnSc3	Tom
se	se	k3xPyFc4	se
však	však	k9	však
brání	bránit	k5eAaImIp3nS	bránit
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
Brno-Komín	Brno-Komína	k1gFnPc2	Brno-Komína
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
jejíž	jejíž	k3xOyRp3gNnSc4	jejíž
území	území	k1gNnSc4	území
část	část	k1gFnSc1	část
komunikace	komunikace	k1gFnSc2	komunikace
vede	vést	k5eAaImIp3nS	vést
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2012	[number]	k4	2012
a	a	k8xC	a
2013	[number]	k4	2013
sužoval	sužovat	k5eAaImAgMnS	sužovat
obyvatele	obyvatel	k1gMnPc4	obyvatel
Bystrce	Bystrc	k1gFnSc2	Bystrc
hojný	hojný	k2eAgInSc1d1	hojný
výskyt	výskyt	k1gInSc1	výskyt
ploštice	ploštice	k1gFnSc2	ploštice
vroubenky	vroubenka	k1gFnSc2	vroubenka
americké	americký	k2eAgFnSc2d1	americká
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobý	dlouhodobý	k2eAgMnSc1d1	dlouhodobý
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
průzkum	průzkum	k1gInSc1	průzkum
spokojenosti	spokojenost	k1gFnSc2	spokojenost
obyvatel	obyvatel	k1gMnPc2	obyvatel
ukázal	ukázat	k5eAaPmAgMnS	ukázat
nadprůměrnou	nadprůměrný	k2eAgFnSc7d1	nadprůměrná
a	a	k8xC	a
s	s	k7c7	s
časem	čas	k1gInSc7	čas
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
spokojenost	spokojenost	k1gFnSc4	spokojenost
obyvatel	obyvatel	k1gMnPc2	obyvatel
Bystrce	Bystrc	k1gFnSc2	Bystrc
II	II	kA	II
s	s	k7c7	s
životními	životní	k2eAgFnPc7d1	životní
podmínkami	podmínka	k1gFnPc7	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Nespokojenost	nespokojenost	k1gFnSc1	nespokojenost
s	s	k7c7	s
přidělováním	přidělování	k1gNnSc7	přidělování
financí	finance	k1gFnPc2	finance
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
s	s	k7c7	s
oklešťování	oklešťování	k1gNnSc6	oklešťování
pravomocí	pravomoc	k1gFnPc2	pravomoc
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
magistrátu	magistrát	k1gInSc2	magistrát
vedla	vést	k5eAaImAgFnS	vést
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
k	k	k7c3	k
návrhu	návrh	k1gInSc3	návrh
posoudit	posoudit	k5eAaPmF	posoudit
výhody	výhoda	k1gFnPc4	výhoda
a	a	k8xC	a
negativa	negativum	k1gNnPc4	negativum
fungování	fungování	k1gNnSc2	fungování
Bystrce	Bystrc	k1gFnSc2	Bystrc
jako	jako	k8xC	jako
samostatné	samostatný	k2eAgFnSc2d1	samostatná
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
interpertováno	interpertovat	k5eAaImNgNnS	interpertovat
tiskem	tisk	k1gInSc7	tisk
jako	jako	k8xS	jako
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
odtržení	odtržení	k1gNnSc4	odtržení
od	od	k7c2	od
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Změnu	změna	k1gFnSc4	změna
statutu	statut	k1gInSc2	statut
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
přesunu	přesun	k1gInSc3	přesun
pravomocí	pravomoc	k1gFnPc2	pravomoc
na	na	k7c6	na
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
na	na	k7c6	na
základě	základ	k1gInSc6	základ
principu	princip	k1gInSc2	princip
subsidiarity	subsidiarita	k1gFnSc2	subsidiarita
a	a	k8xC	a
ke	k	k7c3	k
spravedlivějšími	spravedlivý	k2eAgFnPc7d2	spravedlivější
rozdělování	rozdělování	k1gNnSc4	rozdělování
investic	investice	k1gFnPc2	investice
do	do	k7c2	do
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
místní	místní	k2eAgNnSc4d1	místní
politické	politický	k2eAgNnSc4d1	politické
uskupení	uskupení	k1gNnSc4	uskupení
Bystrčáci	Bystrčák	k1gMnPc1	Bystrčák
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Koláček	Koláček	k1gMnSc1	Koláček
(	(	kIx(	(
<g/>
*	*	kIx~	*
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
-	-	kIx~	-
jezuitský	jezuitský	k2eAgMnSc1d1	jezuitský
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
,	,	kIx,	,
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
vedoucí	vedoucí	k1gMnSc1	vedoucí
České	český	k2eAgFnSc2d1	Česká
sekce	sekce	k1gFnSc2	sekce
Radia	radio	k1gNnSc2	radio
Vatikán	Vatikán	k1gInSc1	Vatikán
Jakub	Jakub	k1gMnSc1	Jakub
Obrovský	obrovský	k2eAgMnSc1d1	obrovský
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
Bystrc	Bystrc	k1gFnSc1	Bystrc
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
-	-	kIx~	-
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
-	-	kIx~	-
sochař	sochař	k1gMnSc1	sochař
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Vincenc	Vincenc	k1gMnSc1	Vincenc
Šťastný	Šťastný	k1gMnSc1	Šťastný
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1885	[number]	k4	1885
<g/>
,	,	kIx,	,
Bystrc	Bystrc	k1gFnSc1	Bystrc
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
-	-	kIx~	-
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
Brno-Tuřany	Brno-Tuřan	k1gMnPc4	Brno-Tuřan
<g/>
)	)	kIx)	)
-	-	kIx~	-
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
klavírní	klavírní	k2eAgMnSc1d1	klavírní
virtuos	virtuos	k1gMnSc1	virtuos
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
Lubomír	Lubomír	k1gMnSc1	Lubomír
Zajíček	Zajíček	k1gMnSc1	Zajíček
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
Brno-Bystrc	Brno-Bystrc	k1gFnSc1	Brno-Bystrc
<g/>
)	)	kIx)	)
-	-	kIx~	-
československý	československý	k2eAgMnSc1d1	československý
volejbalista	volejbalista	k1gMnSc1	volejbalista
<g/>
,	,	kIx,	,
reprezentant	reprezentant	k1gMnSc1	reprezentant
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
bronzového	bronzový	k2eAgInSc2d1	bronzový
týmu	tým	k1gInSc2	tým
na	na	k7c4	na
LOH	LOH	kA	LOH
1968	[number]	k4	1968
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
"	"	kIx"	"
<g/>
Rudy	Rudy	k1gMnSc1	Rudy
<g/>
"	"	kIx"	"
Kovanda	Kovanda	k1gMnSc1	Kovanda
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
-	-	kIx~	-
bavič	bavič	k1gMnSc1	bavič
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
sportovec	sportovec	k1gMnSc1	sportovec
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Přibyl	Přibyl	k1gMnSc1	Přibyl
(	(	kIx(	(
<g/>
*	*	kIx~	*
březen	březen	k1gInSc1	březen
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
Brno-Bystrc	Brno-Bystrc	k1gFnSc1	Brno-Bystrc
<g/>
)	)	kIx)	)
-	-	kIx~	-
publicista	publicista	k1gMnSc1	publicista
a	a	k8xC	a
žurnalista	žurnalista	k1gMnSc1	žurnalista
Miroslav	Miroslav	k1gMnSc1	Miroslav
Fišmeister	Fišmeister	k1gMnSc1	Fišmeister
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
-	-	kIx~	-
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
amatérský	amatérský	k2eAgMnSc1d1	amatérský
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Regina	Regina	k1gFnSc1	Regina
Agia	Agia	k1gFnSc1	Agia
Holásková	Holásková	k1gFnSc1	Holásková
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
-	-	kIx~	-
návrhářka	návrhářka	k1gFnSc1	návrhářka
oděvů	oděv	k1gInPc2	oděv
<g/>
,	,	kIx,	,
tanečnice	tanečnice	k1gFnPc1	tanečnice
a	a	k8xC	a
malířka	malířka	k1gFnSc1	malířka
Miroslava	Miroslava	k1gFnSc1	Miroslava
Knapková	Knapková	k1gFnSc1	Knapková
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
Brno-Bystrc	Brno-Bystrc	k1gFnSc1	Brno-Bystrc
<g/>
)	)	kIx)	)
-	-	kIx~	-
česká	český	k2eAgFnSc1d1	Česká
veslařka	veslařka	k1gFnSc1	veslařka
<g/>
-	-	kIx~	-
<g/>
skifařka	skifařka	k1gFnSc1	skifařka
a	a	k8xC	a
olympijská	olympijský	k2eAgFnSc1d1	olympijská
vítězka	vítězka	k1gFnSc1	vítězka
Josef	Josef	k1gMnSc1	Josef
Hron	Hron	k1gMnSc1	Hron
(	(	kIx(	(
<g/>
*	*	kIx~	*
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
Pelhřimov	Pelhřimov	k1gInSc1	Pelhřimov
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
bývalý	bývalý	k2eAgMnSc1d1	bývalý
český	český	k2eAgMnSc1d1	český
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
brankář	brankář	k1gMnSc1	brankář
a	a	k8xC	a
nynější	nynější	k2eAgMnSc1d1	nynější
trenér	trenér	k1gMnSc1	trenér
Vnislav	Vnislava	k1gFnPc2	Vnislava
Fruvirt	Fruvirta	k1gFnPc2	Fruvirta
(	(	kIx(	(
<g/>
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
-	-	kIx~	-
římskokatolický	římskokatolický	k2eAgMnSc1d1	římskokatolický
kněz	kněz	k1gMnSc1	kněz
brněnské	brněnský	k2eAgFnSc2d1	brněnská
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
,	,	kIx,	,
politický	politický	k2eAgMnSc1d1	politický
vězeň	vězeň	k1gMnSc1	vězeň
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
kaplan	kaplan	k1gMnSc1	kaplan
Jeho	jeho	k3xOp3gFnPc4	jeho
Svatosti	svatost	k1gFnPc4	svatost
a	a	k8xC	a
emeritní	emeritní	k2eAgMnSc1d1	emeritní
farář	farář	k1gMnSc1	farář
farnosti	farnost	k1gFnSc2	farnost
Brno-Bystrc	Brno-Bystrc	k1gInSc1	Brno-Bystrc
Tomáš	Tomáš	k1gMnSc1	Tomáš
Ruprecht	Ruprecht	k1gMnSc1	Ruprecht
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
-	-	kIx~	-
První	první	k4xOgMnSc1	první
vítěz	vítěz	k1gMnSc1	vítěz
ankety	anketa	k1gFnSc2	anketa
Bystrčák	Bystrčák	k1gInSc1	Bystrčák
roku	rok	k1gInSc2	rok
V	v	k7c6	v
Bystrci	Bystrc	k1gFnSc6	Bystrc
působí	působit	k5eAaImIp3nS	působit
několik	několik	k4yIc1	několik
sdružení	sdružení	k1gNnPc2	sdružení
aktivně	aktivně	k6eAd1	aktivně
se	se	k3xPyFc4	se
podílejících	podílející	k2eAgFnPc2d1	podílející
na	na	k7c6	na
rozvoji	rozvoj	k1gInSc6	rozvoj
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Sdružení	sdružení	k1gNnSc1	sdružení
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
svých	svůj	k3xOyFgFnPc2	svůj
aktivit	aktivita	k1gFnPc2	aktivita
zapojují	zapojovat	k5eAaImIp3nP	zapojovat
do	do	k7c2	do
občanského	občanský	k2eAgInSc2d1	občanský
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
rozhodování	rozhodování	k1gNnSc4	rozhodování
veřejné	veřejný	k2eAgFnSc2d1	veřejná
správy	správa	k1gFnSc2	správa
a	a	k8xC	a
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
se	se	k3xPyFc4	se
k	k	k7c3	k
aktuálním	aktuální	k2eAgNnPc3d1	aktuální
společenským	společenský	k2eAgNnPc3d1	společenské
tématům	téma	k1gNnPc3	téma
<g/>
.	.	kIx.	.
</s>
<s>
Občanské	občanský	k2eAgNnSc1d1	občanské
sdružení	sdružení	k1gNnSc1	sdružení
Horní	horní	k2eAgNnSc1d1	horní
náměstí	náměstí	k1gNnSc1	náměstí
-	-	kIx~	-
Ochrana	ochrana	k1gFnSc1	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
MČ	MČ	kA	MČ
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
Bystrc	Bystrc	k1gFnSc1	Bystrc
a	a	k8xC	a
v	v	k7c6	v
přilehlých	přilehlý	k2eAgFnPc6d1	přilehlá
lokalitách	lokalita	k1gFnPc6	lokalita
před	před	k7c7	před
veškerými	veškerý	k3xTgMnPc7	veškerý
<g/>
,	,	kIx,	,
nevhodnými	vhodný	k2eNgInPc7d1	nevhodný
podnikatelskými	podnikatelský	k2eAgInPc7d1	podnikatelský
záměry	záměr	k1gInPc7	záměr
<g/>
,	,	kIx,	,
investičními	investiční	k2eAgFnPc7d1	investiční
akcemi	akce	k1gFnPc7	akce
a	a	k8xC	a
stavbami	stavba	k1gFnPc7	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Sdružení	sdružení	k1gNnSc4	sdružení
zformovaly	zformovat	k5eAaPmAgInP	zformovat
protesty	protest	k1gInPc1	protest
bystrckých	bystrcký	k2eAgMnPc2d1	bystrcký
obyvatel	obyvatel	k1gMnPc2	obyvatel
proti	proti	k7c3	proti
stavbě	stavba	k1gFnSc3	stavba
tzv.	tzv.	kA	tzv.
polyfunkčního	polyfunkční	k2eAgNnSc2d1	polyfunkční
centra	centrum	k1gNnSc2	centrum
na	na	k7c6	na
Horním	horní	k2eAgNnSc6d1	horní
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Bystrci	Bystrc	k1gFnSc6	Bystrc
<g/>
.	.	kIx.	.
</s>
<s>
Občané	občan	k1gMnPc1	občan
Brna	Brno	k1gNnSc2	Brno
proti	proti	k7c3	proti
stavbě	stavba	k1gFnSc3	stavba
rychlostní	rychlostní	k2eAgFnSc2d1	rychlostní
komunikace	komunikace	k1gFnSc2	komunikace
R43	R43	k1gFnSc2	R43
v	v	k7c6	v
trase	trasa	k1gFnSc6	trasa
Kuřim-Troubsko	Kuřim-Troubsko	k1gNnSc1	Kuřim-Troubsko
-	-	kIx~	-
Ochrana	ochrana	k1gFnSc1	ochrana
zájmů	zájem	k1gInPc2	zájem
občanů	občan	k1gMnPc2	občan
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
realizace	realizace	k1gFnSc1	realizace
výstavby	výstavba	k1gFnSc2	výstavba
dálnice	dálnice	k1gFnSc1	dálnice
D43	D43	k1gFnSc1	D43
Ochránci	ochránce	k1gMnSc3	ochránce
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Brněnské	brněnský	k2eAgFnSc2d1	brněnská
přehrady	přehrada	k1gFnSc2	přehrada
-	-	kIx~	-
Ochrana	ochrana	k1gFnSc1	ochrana
zájmů	zájem	k1gInPc2	zájem
občanů	občan	k1gMnPc2	občan
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
realizace	realizace	k1gFnSc1	realizace
výstavby	výstavba	k1gFnSc2	výstavba
dálnice	dálnice	k1gFnSc1	dálnice
D43	D43	k1gFnSc2	D43
Občanské	občanský	k2eAgNnSc4d1	občanské
sdružení	sdružení	k1gNnSc4	sdružení
Stará	starý	k2eAgFnSc1d1	stará
Bystrc	Bystrc	k1gFnSc1	Bystrc
-	-	kIx~	-
Chaloupky	chaloupka	k1gFnSc2	chaloupka
-	-	kIx~	-
Snahou	snaha	k1gFnSc7	snaha
sdružení	sdružení	k1gNnPc2	sdružení
je	být	k5eAaImIp3nS	být
ochrana	ochrana	k1gFnSc1	ochrana
genia	genius	k1gMnSc2	genius
loci	loc	k1gFnSc2	loc
staré	starý	k2eAgFnSc2d1	stará
Bystrce	Bystrc	k1gFnSc2	Bystrc
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
bránit	bránit	k5eAaImF	bránit
nezaměnitelnou	zaměnitelný	k2eNgFnSc4d1	nezaměnitelná
atmosféru	atmosféra	k1gFnSc4	atmosféra
této	tento	k3xDgFnSc2	tento
části	část	k1gFnSc2	část
Bystrce	Bystrc	k1gFnSc2	Bystrc
proti	proti	k7c3	proti
nerozumným	rozumný	k2eNgInPc3d1	nerozumný
urbanistickým	urbanistický	k2eAgInPc3d1	urbanistický
výpadům	výpad	k1gInPc3	výpad
<g/>
.	.	kIx.	.
</s>
<s>
VŠEM	všecek	k3xTgMnPc3	všecek
<g/>
,	,	kIx,	,
o.	o.	k?	o.
s.	s.	k?	s.
-	-	kIx~	-
Sdružení	sdružení	k1gNnSc1	sdružení
je	být	k5eAaImIp3nS	být
dobrovolným	dobrovolný	k2eAgMnSc7d1	dobrovolný
<g/>
,	,	kIx,	,
neziskovým	ziskový	k2eNgMnSc7d1	neziskový
<g/>
,	,	kIx,	,
nevládním	vládní	k2eNgNnSc7d1	nevládní
sdružením	sdružení	k1gNnSc7	sdružení
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
sesnaží	sesnažit	k5eAaPmIp3nP	sesnažit
připravovat	připravovat	k5eAaImF	připravovat
a	a	k8xC	a
realizovat	realizovat	k5eAaBmF	realizovat
projekty	projekt	k1gInPc4	projekt
a	a	k8xC	a
akce	akce	k1gFnPc4	akce
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mládež	mládež	k1gFnSc4	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
rodičů	rodič	k1gMnPc2	rodič
a	a	k8xC	a
přátel	přítel	k1gMnPc2	přítel
ZŠ	ZŠ	kA	ZŠ
Heyrovského	Heyrovský	k2eAgInSc2d1	Heyrovský
32	[number]	k4	32
<g/>
,	,	kIx,	,
o.	o.	k?	o.
s.	s.	k?	s.
-	-	kIx~	-
Občanské	občanský	k2eAgNnSc1d1	občanské
sdružení	sdružení	k1gNnSc1	sdružení
Klub	klub	k1gInSc4	klub
rodičů	rodič	k1gMnPc2	rodič
a	a	k8xC	a
přátel	přítel	k1gMnPc2	přítel
ZŠ	ZŠ	kA	ZŠ
Heyrovského	Heyrovský	k2eAgInSc2d1	Heyrovský
32	[number]	k4	32
<g/>
,	,	kIx,	,
o.	o.	k?	o.
s.	s.	k?	s.
spojuje	spojovat	k5eAaImIp3nS	spojovat
všechny	všechen	k3xTgMnPc4	všechen
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
chtějí	chtít	k5eAaImIp3nP	chtít
společně	společně	k6eAd1	společně
podporovat	podporovat	k5eAaImF	podporovat
děti	dítě	k1gFnPc4	dítě
ze	z	k7c2	z
Základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
na	na	k7c4	na
ulici	ulice	k1gFnSc4	ulice
Heyrovského	Heyrovský	k2eAgNnSc2d1	Heyrovský
32	[number]	k4	32
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
přispívat	přispívat	k5eAaImF	přispívat
k	k	k7c3	k
dobrým	dobrý	k2eAgInPc3d1	dobrý
vztahům	vztah	k1gInPc3	vztah
mezi	mezi	k7c7	mezi
rodiči	rodič	k1gMnPc7	rodič
<g/>
,	,	kIx,	,
školou	škola	k1gFnSc7	škola
a	a	k8xC	a
veřejností	veřejnost	k1gFnSc7	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
bez	bez	k7c2	bez
výhrad	výhrada	k1gFnPc2	výhrada
<g/>
,	,	kIx,	,
o.s.	o.s.	k?	o.s.
-	-	kIx~	-
Občanské	občanský	k2eAgNnSc4d1	občanské
sdružení	sdružení	k1gNnSc4	sdružení
Hrad	hrad	k1gInSc1	hrad
bez	bez	k7c2	bez
výhrad	výhrada	k1gFnPc2	výhrada
<g/>
,	,	kIx,	,
o.s.	o.s.	k?	o.s.
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
jako	jako	k8xC	jako
platforma	platforma	k1gFnSc1	platforma
zastřešující	zastřešující	k2eAgMnPc4d1	zastřešující
jednotlivce	jednotlivec	k1gMnPc4	jednotlivec
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
aktivity	aktivita	k1gFnPc4	aktivita
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
obnovit	obnovit	k5eAaPmF	obnovit
Státní	státní	k2eAgInSc1d1	státní
hrad	hrad	k1gInSc1	hrad
Veveří	veveří	k2eAgFnSc2d1	veveří
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc2	jeho
okolí	okolí	k1gNnSc2	okolí
jako	jako	k9	jako
kulturní	kulturní	k2eAgNnSc4d1	kulturní
a	a	k8xC	a
vzdělávací	vzdělávací	k2eAgNnSc4d1	vzdělávací
centrum	centrum	k1gNnSc4	centrum
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bystrci	Bystrc	k1gFnSc6	Bystrc
roste	růst	k5eAaImIp3nS	růst
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejstarší	starý	k2eAgInSc1d3	nejstarší
strom	strom	k1gInSc1	strom
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
asi	asi	k9	asi
400	[number]	k4	400
let	léto	k1gNnPc2	léto
stará	starý	k2eAgFnSc1d1	stará
lípa	lípa	k1gFnSc1	lípa
srdčitá	srdčitý	k2eAgFnSc1d1	srdčitá
První	první	k4xOgInPc1	první
oficiálně	oficiálně	k6eAd1	oficiálně
povolené	povolený	k2eAgInPc1d1	povolený
závody	závod	k1gInPc1	závod
suchozemských	suchozemský	k2eAgFnPc2d1	suchozemská
želv	želva	k1gFnPc2	želva
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
byly	být	k5eAaImAgInP	být
uspořádány	uspořádat	k5eAaPmNgInP	uspořádat
v	v	k7c6	v
Bystrci	Bystrc	k1gFnSc6	Bystrc
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2012	[number]	k4	2012
Zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
se	se	k3xPyFc4	se
čtyři	čtyři	k4xCgNnPc1	čtyři
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
šestiletá	šestiletý	k2eAgFnSc1d1	šestiletá
samice	samice	k1gFnSc1	samice
želvy	želva	k1gFnSc2	želva
zelenavé	zelenavý	k2eAgFnSc2d1	zelenavá
Wendy	Wenda	k1gFnSc2	Wenda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Bystrci	Bystrc	k1gFnSc6	Bystrc
překonán	překonán	k2eAgInSc1d1	překonán
rekord	rekord	k1gInSc1	rekord
v	v	k7c6	v
rychlokreslení	rychlokreslení	k1gNnSc6	rychlokreslení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
I.	I.	kA	I.
ročníku	ročník	k1gInSc6	ročník
soutěžně	soutěžně	k6eAd1	soutěžně
-	-	kIx~	-
zábavného	zábavný	k2eAgNnSc2d1	zábavné
odpoledne	odpoledne	k1gNnSc2	odpoledne
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
i	i	k9	i
dospělé	dospělý	k2eAgNnSc1d1	dospělé
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
bystrckému	bystrcký	k2eAgMnSc3d1	bystrcký
občanovi	občan	k1gMnSc3	občan
<g/>
,	,	kIx,	,
karikaturistovi	karikaturista	k1gMnSc3	karikaturista
Lubomíru	Lubomír	k1gMnSc3	Lubomír
Vaňkovi	Vaněk	k1gMnSc3	Vaněk
nakreslit	nakreslit	k5eAaPmF	nakreslit
100	[number]	k4	100
dětských	dětský	k2eAgInPc2d1	dětský
portrétů	portrét	k1gInPc2	portrét
na	na	k7c4	na
vlastnoručně	vlastnoručně	k6eAd1	vlastnoručně
vyrobené	vyrobený	k2eAgFnPc4d1	vyrobená
papírové	papírový	k2eAgFnPc4d1	papírová
večerníčkovské	večerníčkovský	k2eAgFnPc4d1	večerníčkovská
čepice	čepice	k1gFnPc4	čepice
v	v	k7c6	v
rekordně	rekordně	k6eAd1	rekordně
krátkém	krátký	k2eAgInSc6d1	krátký
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
stanovený	stanovený	k2eAgInSc4d1	stanovený
limit	limit	k1gInSc4	limit
jedné	jeden	k4xCgFnSc2	jeden
hodiny	hodina	k1gFnSc2	hodina
na	na	k7c4	na
zhotovení	zhotovení	k1gNnSc4	zhotovení
kreseb	kresba	k1gFnPc2	kresba
byl	být	k5eAaImAgMnS	být
karikaturistou	karikaturista	k1gMnSc7	karikaturista
zkrácen	zkrátit	k5eAaPmNgMnS	zkrátit
o	o	k7c4	o
téměř	téměř	k6eAd1	téměř
třináct	třináct	k4xCc4	třináct
minut	minuta	k1gFnPc2	minuta
(	(	kIx(	(
<g/>
výsledný	výsledný	k2eAgInSc1d1	výsledný
čas	čas	k1gInSc1	čas
47	[number]	k4	47
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
11	[number]	k4	11
sekund	sekunda	k1gFnPc2	sekunda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
