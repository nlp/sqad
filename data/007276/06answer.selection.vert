<s>
Delirium	delirium	k1gNnSc1	delirium
tremens	tremensa	k1gFnPc2	tremensa
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
"	"	kIx"	"
<g/>
třesoucí	třesoucí	k2eAgNnSc1d1	třesoucí
šílenství	šílenství	k1gNnSc1	šílenství
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
přeloženo	přeložen	k2eAgNnSc1d1	přeloženo
"	"	kIx"	"
<g/>
šílenství	šílenství	k1gNnSc1	šílenství
s	s	k7c7	s
třesem	třes	k1gInSc7	třes
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
též	též	k9	též
alkoholové	alkoholový	k2eAgNnSc1d1	alkoholové
delirium	delirium	k1gNnSc1	delirium
je	být	k5eAaImIp3nS	být
život	život	k1gInSc4	život
ohrožující	ohrožující	k2eAgInSc4d1	ohrožující
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
u	u	k7c2	u
alkoholiků	alkoholik	k1gMnPc2	alkoholik
při	při	k7c6	při
náhlém	náhlý	k2eAgNnSc6d1	náhlé
přerušení	přerušení	k1gNnSc6	přerušení
užívání	užívání	k1gNnSc2	užívání
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
