<s>
Delirium	delirium	k1gNnSc1	delirium
tremens	tremensa	k1gFnPc2	tremensa
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
"	"	kIx"	"
<g/>
třesoucí	třesoucí	k2eAgNnSc1d1	třesoucí
šílenství	šílenství	k1gNnSc1	šílenství
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
přeloženo	přeložen	k2eAgNnSc1d1	přeloženo
"	"	kIx"	"
<g/>
šílenství	šílenství	k1gNnSc1	šílenství
s	s	k7c7	s
třesem	třes	k1gInSc7	třes
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
též	též	k9	též
alkoholové	alkoholový	k2eAgNnSc1d1	alkoholové
delirium	delirium	k1gNnSc1	delirium
je	být	k5eAaImIp3nS	být
život	život	k1gInSc4	život
ohrožující	ohrožující	k2eAgInSc4d1	ohrožující
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
u	u	k7c2	u
alkoholiků	alkoholik	k1gMnPc2	alkoholik
při	při	k7c6	při
náhlém	náhlý	k2eAgNnSc6d1	náhlé
přerušení	přerušení	k1gNnSc6	přerušení
užívání	užívání	k1gNnSc2	užívání
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejtěžším	těžký	k2eAgNnSc7d3	nejtěžší
stádiem	stádium	k1gNnSc7	stádium
abstinenčního	abstinenční	k2eAgInSc2d1	abstinenční
syndromu	syndrom	k1gInSc2	syndrom
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
<g/>
%	%	kIx~	%
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
delirium	delirium	k1gNnSc1	delirium
je	být	k5eAaImIp3nS	být
též	též	k9	též
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xS	jako
alkoholická	alkoholický	k2eAgFnSc1d1	alkoholická
psychóza	psychóza	k1gFnSc1	psychóza
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
obdobné	obdobný	k2eAgInPc1d1	obdobný
příznaky	příznak	k1gInPc1	příznak
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
u	u	k7c2	u
rychlého	rychlý	k2eAgNnSc2d1	rychlé
vysazení	vysazení	k1gNnSc2	vysazení
benzodiazepinů	benzodiazepin	k1gInPc2	benzodiazepin
a	a	k8xC	a
barbiturátů	barbiturát	k1gInPc2	barbiturát
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc1	průběh
je	být	k5eAaImIp3nS	být
individuální	individuální	k2eAgMnSc1d1	individuální
<g/>
,	,	kIx,	,
předchází	předcházet	k5eAaImIp3nS	předcházet
mu	on	k3xPp3gMnSc3	on
predelirantní	predelirantní	k2eAgInSc1d1	predelirantní
stav	stav	k1gInSc4	stav
charakterizovaný	charakterizovaný	k2eAgInSc4d1	charakterizovaný
pocením	pocení	k1gNnSc7	pocení
<g/>
,	,	kIx,	,
nevolností	nevolnost	k1gFnSc7	nevolnost
<g/>
,	,	kIx,	,
zrychlenou	zrychlený	k2eAgFnSc7d1	zrychlená
srdeční	srdeční	k2eAgFnSc7d1	srdeční
činností	činnost	k1gFnSc7	činnost
<g/>
,	,	kIx,	,
nespavostí	nespavost	k1gFnSc7	nespavost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nezasáhne	zasáhnout	k5eNaPmIp3nS	zasáhnout
včas	včas	k6eAd1	včas
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přechodu	přechod	k1gInSc3	přechod
do	do	k7c2	do
deliria	delirium	k1gNnSc2	delirium
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
deliria	delirium	k1gNnSc2	delirium
pacient	pacient	k1gMnSc1	pacient
mohutně	mohutně	k6eAd1	mohutně
halucinuje	halucinovat	k5eAaImIp3nS	halucinovat
–	–	k?	–
může	moct	k5eAaImIp3nS	moct
vidět	vidět	k5eAaImF	vidět
různá	různý	k2eAgNnPc4d1	různé
malá	malý	k2eAgNnPc4d1	malé
zvířata	zvíře	k1gNnPc4	zvíře
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
mikrozoopsie	mikrozoopsie	k1gFnSc2	mikrozoopsie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
zažité	zažitý	k2eAgNnSc1d1	zažité
<g/>
,	,	kIx,	,
zautomatizované	zautomatizovaný	k2eAgInPc1d1	zautomatizovaný
pohyby	pohyb	k1gInPc1	pohyb
(	(	kIx(	(
<g/>
např.	např.	kA	např.
řidiči	řidič	k1gMnPc1	řidič
z	z	k7c2	z
povolání	povolání	k1gNnSc2	povolání
řídí	řídit	k5eAaImIp3nS	řídit
auto	auto	k1gNnSc1	auto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostavuje	dostavovat	k5eAaImIp3nS	dostavovat
se	se	k3xPyFc4	se
hrubý	hrubý	k2eAgInSc1d1	hrubý
tremor	tremor	k1gInSc1	tremor
(	(	kIx(	(
<g/>
třes	třes	k1gInSc1	třes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čase	čas	k1gInSc6	čas
a	a	k8xC	a
prostoru	prostor	k1gInSc2	prostor
je	být	k5eAaImIp3nS	být
nepřesně	přesně	k6eNd1	přesně
orientován	orientován	k2eAgInSc1d1	orientován
či	či	k8xC	či
vůbec	vůbec	k9	vůbec
neorientován	orientován	k2eNgInSc1d1	orientován
<g/>
,	,	kIx,	,
výrazná	výrazný	k2eAgFnSc1d1	výrazná
je	být	k5eAaImIp3nS	být
porucha	porucha	k1gFnSc1	porucha
bezprostřední	bezprostřední	k2eAgFnSc2d1	bezprostřední
paměti	paměť	k1gFnSc2	paměť
a	a	k8xC	a
porucha	porucha	k1gFnSc1	porucha
konsolidace	konsolidace	k1gFnSc2	konsolidace
paměťových	paměťový	k2eAgFnPc2d1	paměťová
stop	stopa	k1gFnPc2	stopa
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgInSc1d1	schopen
si	se	k3xPyFc3	se
zapamatovat	zapamatovat	k5eAaPmF	zapamatovat
nové	nový	k2eAgInPc4d1	nový
údaje	údaj	k1gInPc4	údaj
a	a	k8xC	a
události	událost	k1gFnPc4	událost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
poruchám	porucha	k1gFnPc3	porucha
psychomotoriky	psychomotorika	k1gFnSc2	psychomotorika
–	–	k?	–
pacient	pacient	k1gMnSc1	pacient
je	být	k5eAaImIp3nS	být
zvýšeně	zvýšeně	k6eAd1	zvýšeně
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
,	,	kIx,	,
neklidný	klidný	k2eNgMnSc1d1	neklidný
<g/>
,	,	kIx,	,
agresivní	agresivní	k2eAgMnSc1d1	agresivní
či	či	k8xC	či
naopak	naopak	k6eAd1	naopak
zcela	zcela	k6eAd1	zcela
klidný	klidný	k2eAgInSc1d1	klidný
<g/>
,	,	kIx,	,
utlumený	utlumený	k2eAgInSc1d1	utlumený
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
celkem	celkem	k6eAd1	celkem
rychle	rychle	k6eAd1	rychle
měnit	měnit	k5eAaImF	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Nastávají	nastávat	k5eAaImIp3nP	nastávat
poruchy	porucha	k1gFnPc1	porucha
emocí	emoce	k1gFnPc2	emoce
–	–	k?	–
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nadnesená	nadnesený	k2eAgFnSc1d1	nadnesená
nálada	nálada	k1gFnSc1	nálada
<g/>
,	,	kIx,	,
euforie	euforie	k1gFnSc1	euforie
či	či	k8xC	či
naopak	naopak	k6eAd1	naopak
úzkost	úzkost	k1gFnSc1	úzkost
či	či	k8xC	či
agrese	agrese	k1gFnSc1	agrese
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
přítomné	přítomný	k2eAgInPc1d1	přítomný
bludy	blud	k1gInPc1	blud
–	–	k?	–
např.	např.	kA	např.
pacient	pacient	k1gMnSc1	pacient
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
ostatní	ostatní	k2eAgMnPc1d1	ostatní
usilují	usilovat	k5eAaImIp3nP	usilovat
o	o	k7c4	o
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
objevit	objevit	k5eAaPmF	objevit
epileptický	epileptický	k2eAgInSc4d1	epileptický
záchvat	záchvat	k1gInSc4	záchvat
–	–	k?	–
křeče	křeč	k1gFnSc2	křeč
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ohrožení	ohrožení	k1gNnSc3	ohrožení
života	život	k1gInSc2	život
dochází	docházet	k5eAaImIp3nS	docházet
nejčastěji	často	k6eAd3	často
srdečním	srdeční	k2eAgMnSc7d1	srdeční
(	(	kIx(	(
<g/>
arytmie	arytmie	k1gFnSc2	arytmie
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
dechovým	dechový	k2eAgNnSc7d1	dechové
selháním	selhání	k1gNnSc7	selhání
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
může	moct	k5eAaImIp3nS	moct
trvat	trvat	k5eAaImF	trvat
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
hlubokým	hluboký	k2eAgInSc7d1	hluboký
spánkem	spánek	k1gInSc7	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
proběhlý	proběhlý	k2eAgInSc4d1	proběhlý
stav	stav	k1gInSc4	stav
je	být	k5eAaImIp3nS	být
amnézie	amnézie	k1gFnSc1	amnézie
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nespecifičnosti	nespecifičnost	k1gFnSc3	nespecifičnost
těchto	tento	k3xDgInPc2	tento
příznaků	příznak	k1gInPc2	příznak
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
i	i	k9	i
u	u	k7c2	u
prokázaných	prokázaný	k2eAgMnPc2d1	prokázaný
alkoholiků	alkoholik	k1gMnPc2	alkoholik
vyloučit	vyloučit	k5eAaPmF	vyloučit
jiné	jiný	k2eAgFnPc4d1	jiná
příčiny	příčina	k1gFnPc4	příčina
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
sníženou	snížený	k2eAgFnSc4d1	snížená
hladinu	hladina	k1gFnSc4	hladina
cukru	cukr	k1gInSc2	cukr
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
,	,	kIx,	,
poškození	poškození	k1gNnSc4	poškození
mozku	mozek	k1gInSc2	mozek
úrazem	úraz	k1gInSc7	úraz
nebo	nebo	k8xC	nebo
infekcí	infekce	k1gFnSc7	infekce
a	a	k8xC	a
další	další	k2eAgNnSc1d1	další
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Prvotní	prvotní	k2eAgFnSc7d1	prvotní
příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
návyk	návyk	k1gInSc1	návyk
na	na	k7c4	na
alkohol	alkohol	k1gInSc4	alkohol
<g/>
,	,	kIx,	,
bezprostředním	bezprostřední	k2eAgInSc7d1	bezprostřední
startérem	startér	k1gInSc7	startér
deliria	delirium	k1gNnSc2	delirium
je	být	k5eAaImIp3nS	být
přerušení	přerušení	k1gNnSc1	přerušení
užívání	užívání	k1gNnSc2	užívání
alkoholu	alkohol	k1gInSc2	alkohol
–	–	k?	–
typicky	typicky	k6eAd1	typicky
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
pacient	pacient	k1gMnSc1	pacient
v	v	k7c6	v
opilosti	opilost	k1gFnSc6	opilost
zraní	zranit	k5eAaPmIp3nS	zranit
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
hospitalizován	hospitalizován	k2eAgMnSc1d1	hospitalizován
<g/>
.	.	kIx.	.
</s>
<s>
Alkohol	alkohol	k1gInSc1	alkohol
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
přenos	přenos	k1gInSc4	přenos
nervových	nervový	k2eAgInPc2d1	nervový
signálů	signál	k1gInPc2	signál
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
ovlivněním	ovlivnění	k1gNnSc7	ovlivnění
metabolismu	metabolismus	k1gInSc2	metabolismus
kyseliny	kyselina	k1gFnSc2	kyselina
gamaaminomáselné	gamaaminomáselný	k2eAgFnSc2d1	gamaaminomáselný
–	–	k?	–
GABA	GABA	kA	GABA
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
neurotransmiterů	neurotransmiter	k1gInPc2	neurotransmiter
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vysazení	vysazení	k1gNnSc6	vysazení
alkoholu	alkohol	k1gInSc2	alkohol
chybí	chybět	k5eAaImIp3nS	chybět
jeho	jeho	k3xOp3gInSc4	jeho
tlumivý	tlumivý	k2eAgInSc4d1	tlumivý
účinek	účinek	k1gInSc4	účinek
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
zvýšenému	zvýšený	k2eAgNnSc3d1	zvýšené
nervovému	nervový	k2eAgNnSc3d1	nervové
dráždění	dráždění	k1gNnSc3	dráždění
<g/>
,	,	kIx,	,
neklidu	neklid	k1gInSc2	neklid
<g/>
,	,	kIx,	,
zrychlení	zrychlení	k1gNnSc1	zrychlení
srdeční	srdeční	k2eAgFnSc2d1	srdeční
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
křečím	křeč	k1gFnPc3	křeč
a	a	k8xC	a
nemožnosti	nemožnost	k1gFnSc3	nemožnost
usnout	usnout	k5eAaPmF	usnout
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
život	život	k1gInSc4	život
ohrožující	ohrožující	k2eAgInSc1d1	ohrožující
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
pacienti	pacient	k1gMnPc1	pacient
bývají	bývat	k5eAaImIp3nP	bývat
často	často	k6eAd1	často
hospitalizováni	hospitalizovat	k5eAaBmNgMnP	hospitalizovat
na	na	k7c6	na
jednotkách	jednotka	k1gFnPc6	jednotka
intenzivní	intenzivní	k2eAgFnSc2d1	intenzivní
péče	péče	k1gFnSc2	péče
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
intenzivní	intenzivní	k2eAgFnSc2d1	intenzivní
péče	péče	k1gFnSc2	péče
byla	být	k5eAaImAgFnS	být
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvojnásobná	dvojnásobný	k2eAgFnSc1d1	dvojnásobná
<g/>
.	.	kIx.	.
</s>
<s>
Lékem	lék	k1gInSc7	lék
první	první	k4xOgFnSc2	první
volby	volba	k1gFnSc2	volba
tohoto	tento	k3xDgInSc2	tento
stavu	stav	k1gInSc2	stav
je	být	k5eAaImIp3nS	být
Clomethiazol	Clomethiazol	k1gInSc1	Clomethiazol
(	(	kIx(	(
<g/>
firemní	firemní	k2eAgInSc1d1	firemní
název	název	k1gInSc1	název
Heminevrin	Heminevrin	k1gInSc1	Heminevrin
<g/>
)	)	kIx)	)
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
dávkách	dávka	k1gFnPc6	dávka
–	–	k?	–
až	až	k9	až
16	[number]	k4	16
tablet	tableta	k1gFnPc2	tableta
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
ten	ten	k3xDgInSc4	ten
nelze	lze	k6eNd1	lze
podávat	podávat	k5eAaImF	podávat
–	–	k?	–
např.	např.	kA	např.
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
odmítání	odmítání	k1gNnSc2	odmítání
cokoliv	cokoliv	k3yInSc4	cokoliv
spolknout	spolknout	k5eAaPmF	spolknout
(	(	kIx(	(
<g/>
v	v	k7c6	v
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
dostupný	dostupný	k2eAgInSc1d1	dostupný
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
kapslí	kapsle	k1gFnPc2	kapsle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
další	další	k2eAgInPc1d1	další
nitrožilně	nitrožilně	k6eAd1	nitrožilně
podávané	podávaný	k2eAgInPc1d1	podávaný
preparáty	preparát	k1gInPc1	preparát
<g/>
:	:	kIx,	:
Benzodiazepiny	benzodiazepin	k1gInPc1	benzodiazepin
ve	v	k7c6	v
vysokých	vysoký	k2eAgFnPc6d1	vysoká
dávkách	dávka	k1gFnPc6	dávka
(	(	kIx(	(
<g/>
až	až	k9	až
80	[number]	k4	80
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
den	den	k1gInSc1	den
<g/>
)	)	kIx)	)
Tiaprid	Tiaprid	k1gInSc1	Tiaprid
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
atypické	atypický	k2eAgNnSc1d1	atypické
antipsychotikum	antipsychotikum	k1gNnSc1	antipsychotikum
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
těchto	tento	k3xDgMnPc2	tento
pacientů	pacient	k1gMnPc2	pacient
je	být	k5eAaImIp3nS	být
přítomná	přítomný	k2eAgFnSc1d1	přítomná
minerálová	minerálový	k2eAgFnSc1d1	minerálová
dysbalance	dysbalance	k1gFnSc1	dysbalance
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
nízká	nízký	k2eAgFnSc1d1	nízká
hladina	hladina	k1gFnSc1	hladina
draslíku	draslík	k1gInSc2	draslík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
deficit	deficit	k1gInSc1	deficit
vitamínů	vitamín	k1gInPc2	vitamín
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
–	–	k?	–
zejména	zejména	k9	zejména
thiamin	thiamin	k1gInSc4	thiamin
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
tyto	tento	k3xDgFnPc4	tento
látky	látka	k1gFnPc4	látka
dodávat	dodávat	k5eAaImF	dodávat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
perorálně	perorálně	k6eAd1	perorálně
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
pacient	pacient	k1gMnSc1	pacient
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
či	či	k8xC	či
ochoten	ochoten	k2eAgMnSc1d1	ochoten
polykat	polykat	k5eAaImF	polykat
<g/>
,	,	kIx,	,
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
infúzí	infúze	k1gFnPc2	infúze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nižší	nízký	k2eAgFnSc2d2	nižší
hladiny	hladina	k1gFnSc2	hladina
záchvatového	záchvatový	k2eAgInSc2d1	záchvatový
prahu	práh	k1gInSc2	práh
se	se	k3xPyFc4	se
k	k	k7c3	k
prevenci	prevence	k1gFnSc3	prevence
epileptického	epileptický	k2eAgInSc2d1	epileptický
záchvatu	záchvat	k1gInSc2	záchvat
podává	podávat	k5eAaImIp3nS	podávat
hořčík	hořčík	k1gInSc1	hořčík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vysokého	vysoký	k2eAgInSc2d1	vysoký
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
pulzu	pulz	k1gInSc2	pulz
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
nutné	nutný	k2eAgNnSc1d1	nutné
podávat	podávat	k5eAaImF	podávat
antihypertenziva	antihypertenziv	k1gMnSc4	antihypertenziv
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
poruchy	porucha	k1gFnPc4	porucha
krevního	krevní	k2eAgInSc2d1	krevní
cukru	cukr	k1gInSc2	cukr
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
dávají	dávat	k5eAaImIp3nP	dávat
infúze	infúze	k1gFnPc1	infúze
s	s	k7c7	s
glukózou	glukóza	k1gFnSc7	glukóza
<g/>
.	.	kIx.	.
</s>
<s>
Hrozí	hrozit	k5eAaImIp3nS	hrozit
riziko	riziko	k1gNnSc1	riziko
smrtelných	smrtelný	k2eAgFnPc2d1	smrtelná
komplikací	komplikace	k1gFnPc2	komplikace
vyplývajících	vyplývající	k2eAgFnPc2d1	vyplývající
z	z	k7c2	z
nasedajících	nasedající	k2eAgFnPc2d1	nasedající
tělesných	tělesný	k2eAgFnPc2d1	tělesná
komplikací	komplikace	k1gFnPc2	komplikace
typických	typický	k2eAgFnPc2d1	typická
pro	pro	k7c4	pro
chronickým	chronický	k2eAgInSc7d1	chronický
alkoholizmem	alkoholizmus	k1gInSc7	alkoholizmus
poškozený	poškozený	k1gMnSc1	poškozený
a	a	k8xC	a
oslabený	oslabený	k2eAgInSc1d1	oslabený
organizmus	organizmus	k1gInSc1	organizmus
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
zápal	zápal	k1gInSc1	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
selhání	selhání	k1gNnSc1	selhání
jater	játra	k1gNnPc2	játra
<g/>
,	,	kIx,	,
ruptura	ruptura	k1gFnSc1	ruptura
(	(	kIx(	(
<g/>
trhliny	trhlina	k1gFnSc2	trhlina
<g/>
)	)	kIx)	)
jícnových	jícnový	k2eAgInPc2d1	jícnový
varixů	varix	k1gInPc2	varix
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
</s>
