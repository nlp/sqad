<s>
Barista	Barista	k1gMnSc1	Barista
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
přípravu	příprava	k1gFnSc4	příprava
a	a	k8xC	a
výrobu	výroba	k1gFnSc4	výroba
kvalitní	kvalitní	k2eAgFnSc2d1	kvalitní
kávy	káva	k1gFnSc2	káva
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
espressa	espressa	k1gFnSc1	espressa
a	a	k8xC	a
nápojů	nápoj	k1gInPc2	nápoj
založených	založený	k2eAgInPc2d1	založený
na	na	k7c6	na
espressu	espress	k1gInSc6	espress
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
schopnosti	schopnost	k1gFnPc4	schopnost
porovnávají	porovnávat	k5eAaImIp3nP	porovnávat
baristé	barista	k1gMnPc1	barista
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
mezinárodních	mezinárodní	k2eAgInPc6d1	mezinárodní
i	i	k8xC	i
národních	národní	k2eAgNnPc6d1	národní
kláních	klání	k1gNnPc6	klání
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholnou	vrcholný	k2eAgFnSc7d1	vrcholná
akcí	akce	k1gFnSc7	akce
je	být	k5eAaImIp3nS	být
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
baristů	barista	k1gMnPc2	barista
(	(	kIx(	(
<g/>
World	World	k1gMnSc1	World
Barista	Barista	k1gMnSc1	Barista
Championship	Championship	k1gMnSc1	Championship
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
postupuje	postupovat	k5eAaImIp3nS	postupovat
vítěz	vítěz	k1gMnSc1	vítěz
národního	národní	k2eAgNnSc2d1	národní
mistrovství	mistrovství	k1gNnSc2	mistrovství
každé	každý	k3xTgFnSc2	každý
členské	členský	k2eAgFnSc2d1	členská
země	zem	k1gFnSc2	zem
kávové	kávový	k2eAgFnSc2d1	kávová
asociace	asociace	k1gFnSc2	asociace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
soutěže	soutěž	k1gFnSc2	soutěž
baristů	barista	k1gMnPc2	barista
založili	založit	k5eAaPmAgMnP	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
Roberto	Roberta	k1gFnSc5	Roberta
Trevisan	Trevisan	k1gInSc1	Trevisan
a	a	k8xC	a
Štěpánka	Štěpánka	k1gFnSc1	Štěpánka
Havrlíková	Havrlíková	k1gFnSc1	Havrlíková
<g/>
.	.	kIx.	.
</s>
<s>
Káva	káva	k1gFnSc1	káva
Mistr	mistr	k1gMnSc1	mistr
kávy	káva	k1gFnSc2	káva
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
barista	barist	k1gMnSc2	barist
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Video	video	k1gNnSc1	video
s	s	k7c7	s
ukázkou	ukázka	k1gFnSc7	ukázka
baristických	baristický	k2eAgInPc2d1	baristický
kousků	kousek	k1gInPc2	kousek
(	(	kIx(	(
<g/>
youtube	youtubat	k5eAaPmIp3nS	youtubat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kto	Kto	k?	Kto
je	být	k5eAaImIp3nS	být
barista	barista	k1gMnSc1	barista
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
Hotelier	Hotelier	k1gInSc1	Hotelier
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
