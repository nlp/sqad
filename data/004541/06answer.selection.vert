<s>
Barista	Barista	k1gMnSc1	Barista
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
přípravu	příprava	k1gFnSc4	příprava
a	a	k8xC	a
výrobu	výroba	k1gFnSc4	výroba
kvalitní	kvalitní	k2eAgFnSc2d1	kvalitní
kávy	káva	k1gFnSc2	káva
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
espressa	espressa	k1gFnSc1	espressa
a	a	k8xC	a
nápojů	nápoj	k1gInPc2	nápoj
založených	založený	k2eAgInPc2d1	založený
na	na	k7c6	na
espressu	espress	k1gInSc6	espress
<g/>
.	.	kIx.	.
</s>
