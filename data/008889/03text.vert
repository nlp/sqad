<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Švédska	Švédsko	k1gNnSc2	Švédsko
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
modrým	modrý	k2eAgInSc7d1	modrý
listem	list	k1gInSc7	list
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
8	[number]	k4	8
se	s	k7c7	s
žlutým	žlutý	k2eAgInSc7d1	žlutý
<g/>
,	,	kIx,	,
skandinávským	skandinávský	k2eAgInSc7d1	skandinávský
křížem	kříž	k1gInSc7	kříž
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc2	jenž
ramena	rameno	k1gNnSc2	rameno
mají	mít	k5eAaImIp3nP	mít
šířku	šířka	k1gFnSc4	šířka
stanovenou	stanovený	k2eAgFnSc4d1	stanovená
na	na	k7c4	na
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
šířky	šířka	k1gFnSc2	šířka
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
roku	rok	k1gInSc2	rok
1521	[number]	k4	1521
byla	být	k5eAaImAgFnS	být
inspirovaná	inspirovaný	k2eAgFnSc1d1	inspirovaná
nejstarší	starý	k2eAgFnSc1d3	nejstarší
vlajkou	vlajka	k1gFnSc7	vlajka
světa	svět	k1gInSc2	svět
–	–	k?	–
dánskou	dánský	k2eAgFnSc7d1	dánská
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
barva	barva	k1gFnSc1	barva
vlajky	vlajka	k1gFnSc2	vlajka
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
dle	dle	k7c2	dle
výkladu	výklad	k1gInSc2	výklad
slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
pak	pak	k6eAd1	pak
moře	moře	k1gNnSc4	moře
i	i	k8xC	i
nebe	nebe	k1gNnSc4	nebe
<g/>
.	.	kIx.	.
<g/>
Rozměry	rozměr	k1gInPc1	rozměr
polí	pole	k1gNnPc2	pole
různých	různý	k2eAgFnPc2d1	různá
barev	barva	k1gFnPc2	barva
švédské	švédský	k2eAgFnSc2d1	švédská
vlajky	vlajka	k1gFnSc2	vlajka
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
vertikálně	vertikálně	k6eAd1	vertikálně
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
a	a	k8xC	a
horizontálně	horizontálně	k6eAd1	horizontálně
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Rozměry	rozměra	k1gFnPc1	rozměra
polí	pole	k1gFnPc2	pole
válečné	válečný	k2eAgFnSc2d1	válečná
vlajky	vlajka	k1gFnSc2	vlajka
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
vertikálně	vertikálně	k6eAd1	vertikálně
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
a	a	k8xC	a
horizontálně	horizontálně	k6eAd1	horizontálně
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
švédský	švédský	k2eAgInSc1d1	švédský
stát	stát	k1gInSc1	stát
(	(	kIx(	(
<g/>
Švédské	švédský	k2eAgNnSc1d1	švédské
království	království	k1gNnSc1	království
<g/>
)	)	kIx)	)
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1157	[number]	k4	1157
obsadil	obsadit	k5eAaPmAgMnS	obsadit
Erik	Erik	k1gMnSc1	Erik
IX	IX	kA	IX
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgInSc1d1	svatý
jihozápad	jihozápad	k1gInSc1	jihozápad
Finska	Finsko	k1gNnSc2	Finsko
a	a	k8xC	a
křížovou	křížový	k2eAgFnSc7d1	křížová
výpravou	výprava	k1gFnSc7	výprava
chtěl	chtít	k5eAaImAgMnS	chtít
pohanské	pohanský	k2eAgFnSc2d1	pohanská
Finy	Fina	k1gFnSc2	Fina
obrátit	obrátit	k5eAaPmF	obrátit
na	na	k7c4	na
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pochází	pocházet	k5eAaImIp3nS	pocházet
legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
první	první	k4xOgFnSc2	první
švédské	švédský	k2eAgFnSc2d1	švédská
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Erik	Erik	k1gMnSc1	Erik
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
po	po	k7c6	po
modlitbě	modlitba	k1gFnSc3	modlitba
vzhlédl	vzhlédnout	k5eAaPmAgMnS	vzhlédnout
k	k	k7c3	k
nebi	nebe	k1gNnSc3	nebe
a	a	k8xC	a
spatřil	spatřit	k5eAaPmAgMnS	spatřit
mezi	mezi	k7c7	mezi
paprsky	paprsek	k1gInPc7	paprsek
slunce	slunce	k1gNnSc2	slunce
žlutý	žlutý	k2eAgInSc4d1	žlutý
kříž	kříž	k1gInSc4	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
kříž	kříž	k1gInSc1	kříž
použil	použít	k5eAaPmAgInS	použít
na	na	k7c6	na
modrých	modrý	k2eAgNnPc6d1	modré
polních	polní	k2eAgNnPc6d1	polní
znameních	znamení	k1gNnPc6	znamení
<g/>
.	.	kIx.	.
</s>
<s>
Doložena	doložen	k2eAgFnSc1d1	doložena
je	být	k5eAaImIp3nS	být
však	však	k9	však
tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
až	až	k9	až
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
inspirována	inspirovat	k5eAaBmNgFnS	inspirovat
nejstarší	starý	k2eAgFnSc7d3	nejstarší
státní	státní	k2eAgFnSc7d1	státní
vlajkou	vlajka	k1gFnSc7	vlajka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
vlajkou	vlajka	k1gFnSc7	vlajka
dánskou	dánský	k2eAgFnSc7d1	dánská
a	a	k8xC	a
tehdejším	tehdejší	k2eAgInSc7d1	tehdejší
modrým	modrý	k2eAgInSc7d1	modrý
švédským	švédský	k2eAgInSc7d1	švédský
znakem	znak	k1gInSc7	znak
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1336	[number]	k4	1336
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgNnSc6	který
zlatý	zlatý	k2eAgInSc1d1	zlatý
kříž	kříž	k1gInSc4	kříž
od	od	k7c2	od
sebe	se	k3xPyFc2	se
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
čtyři	čtyři	k4xCgNnPc4	čtyři
modrá	modrý	k2eAgNnPc4d1	modré
pole	pole	k1gNnPc4	pole
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
dle	dle	k7c2	dle
tradice	tradice	k1gFnSc2	tradice
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1521	[number]	k4	1521
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
Dánům	Dán	k1gMnPc3	Dán
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
vedl	vést	k5eAaImAgMnS	vést
Gustav	Gustav	k1gMnSc1	Gustav
I.	I.	kA	I.
Vasa	Vasa	k1gMnSc1	Vasa
-	-	kIx~	-
regent	regent	k1gMnSc1	regent
a	a	k8xC	a
pozdější	pozdní	k2eAgMnSc1d2	pozdější
švédský	švédský	k2eAgMnSc1d1	švédský
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
králové	král	k1gMnPc1	král
(	(	kIx(	(
<g/>
Erik	Erik	k1gMnSc1	Erik
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
</s>
<s>
Švédský	švédský	k2eAgMnSc1d1	švédský
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
populární	populární	k2eAgFnSc4d1	populární
vlajku	vlajka	k1gFnSc4	vlajka
ponechali	ponechat	k5eAaPmAgMnP	ponechat
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
III	III	kA	III
<g/>
.	.	kIx.	.
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
žlutý	žlutý	k2eAgInSc1d1	žlutý
kříž	kříž	k1gInSc1	kříž
navěky	navěky	k6eAd1	navěky
užíván	užívat	k5eAaImNgInS	užívat
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
švédských	švédský	k2eAgFnPc6d1	švédská
vlajkách	vlajka	k1gFnPc6	vlajka
<g/>
,	,	kIx,	,
zástavách	zástava	k1gFnPc6	zástava
a	a	k8xC	a
standartách	standarta	k1gFnPc6	standarta
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Gustava	Gustav	k1gMnSc2	Gustav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Adolfa	Adolf	k1gMnSc2	Adolf
se	se	k3xPyFc4	se
vlajka	vlajka	k1gFnSc1	vlajka
začala	začít	k5eAaPmAgFnS	začít
běžně	běžně	k6eAd1	běžně
užívat	užívat	k5eAaImF	užívat
i	i	k9	i
na	na	k7c6	na
lodích	loď	k1gFnPc6	loď
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	let	k1gInPc6	let
1397	[number]	k4	1397
<g/>
–	–	k?	–
<g/>
1523	[number]	k4	1523
existovala	existovat	k5eAaImAgFnS	existovat
personální	personální	k2eAgFnSc1d1	personální
unie	unie	k1gFnSc1	unie
dánského	dánský	k2eAgMnSc2d1	dánský
<g/>
,	,	kIx,	,
norského	norský	k2eAgNnSc2d1	norské
a	a	k8xC	a
švédského	švédský	k2eAgNnSc2d1	švédské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Vlajku	vlajka	k1gFnSc4	vlajka
Kalmarské	Kalmarský	k2eAgFnSc2d1	Kalmarská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
též	též	k9	též
Vlajka	vlajka	k1gFnSc1	vlajka
severu	sever	k1gInSc2	sever
zavedl	zavést	k5eAaPmAgInS	zavést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1430	[number]	k4	1430
král	král	k1gMnSc1	král
Erik	Erik	k1gMnSc1	Erik
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Pomořanský	pomořanský	k2eAgInSc1d1	pomořanský
pro	pro	k7c4	pro
slavnostní	slavnostní	k2eAgFnPc4d1	slavnostní
příležitosti	příležitost	k1gFnPc4	příležitost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
napoleonských	napoleonský	k2eAgFnPc6d1	napoleonská
válkách	válka	k1gFnPc6	válka
a	a	k8xC	a
Vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
kongresu	kongres	k1gInSc6	kongres
přišlo	přijít	k5eAaPmAgNnS	přijít
Švédsko	Švédsko	k1gNnSc1	Švédsko
o	o	k7c4	o
Finsko	Finsko	k1gNnSc4	Finsko
<g/>
,	,	kIx,	,
Švédské	švédský	k2eAgInPc4d1	švédský
Pomořany	Pomořany	k1gInPc4	Pomořany
ale	ale	k9	ale
získalo	získat	k5eAaPmAgNnS	získat
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1814	[number]	k4	1814
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Švédsko-norská	švédskoorský	k2eAgFnSc1d1	švédsko-norská
unie	unie	k1gFnSc1	unie
a	a	k8xC	a
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1815	[number]	k4	1815
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Vlajku	vlajka	k1gFnSc4	vlajka
tvořila	tvořit	k5eAaImAgFnS	tvořit
předchozí	předchozí	k2eAgFnSc1d1	předchozí
(	(	kIx(	(
<g/>
současná	současný	k2eAgFnSc1d1	současná
<g/>
)	)	kIx)	)
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
v	v	k7c6	v
červeném	červený	k2eAgInSc6d1	červený
kantonu	kanton	k1gInSc6	kanton
v	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
rohu	roh	k1gInSc6	roh
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
umístěn	umístit	k5eAaPmNgInS	umístit
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
úhlopříčný	úhlopříčný	k2eAgInSc1d1	úhlopříčný
kříž	kříž	k1gInSc1	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
zdroje	zdroj	k1gInSc2	zdroj
mají	mít	k5eAaImIp3nP	mít
vyobrazené	vyobrazený	k2eAgFnPc1d1	vyobrazená
vlajky	vlajka	k1gFnPc1	vlajka
místo	místo	k7c2	místo
kantonu	kanton	k1gInSc2	kanton
karé	karé	k1gNnSc2	karé
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1844	[number]	k4	1844
se	se	k3xPyFc4	se
vlajka	vlajka	k1gFnSc1	vlajka
změnila	změnit	k5eAaPmAgFnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Vlajku	vlajka	k1gFnSc4	vlajka
opět	opět	k6eAd1	opět
tvořila	tvořit	k5eAaImAgFnS	tvořit
původní	původní	k2eAgFnSc1d1	původní
(	(	kIx(	(
<g/>
současná	současný	k2eAgFnSc1d1	současná
<g/>
)	)	kIx)	)
švédská	švédský	k2eAgFnSc1d1	švédská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
v	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
kantonu	kanton	k1gInSc6	kanton
byl	být	k5eAaImAgInS	být
však	však	k9	však
unionistický	unionistický	k2eAgInSc1d1	unionistický
emblém	emblém	k1gInSc1	emblém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
úhlopříčně	úhlopříčně	k6eAd1	úhlopříčně
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
čtyři	čtyři	k4xCgNnPc4	čtyři
pole	pole	k1gNnPc4	pole
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterých	který	k3yRgInPc6	který
byly	být	k5eAaImAgFnP	být
výseky	výseka	k1gFnPc4	výseka
ze	z	k7c2	z
švédské	švédský	k2eAgFnSc2d1	švédská
a	a	k8xC	a
norské	norský	k2eAgFnSc2d1	norská
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
po	po	k7c6	po
referendu	referendum	k1gNnSc6	referendum
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
,	,	kIx,	,
Švédsko-norská	švédskoorský	k2eAgFnSc1d1	švédsko-norská
unie	unie	k1gFnSc1	unie
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
a	a	k8xC	a
dosavadní	dosavadní	k2eAgFnSc1d1	dosavadní
vlajka	vlajka	k1gFnSc1	vlajka
pozbyla	pozbýt	k5eAaPmAgFnS	pozbýt
platnosti	platnost	k1gFnPc4	platnost
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1906	[number]	k4	1906
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
zavedena	zaveden	k2eAgFnSc1d1	zavedena
modrá	modrý	k2eAgFnSc1d1	modrá
vlajka	vlajka	k1gFnSc1	vlajka
se	s	k7c7	s
žlutým	žlutý	k2eAgInSc7d1	žlutý
křížem	kříž	k1gInSc7	kříž
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
platná	platný	k2eAgNnPc4d1	platné
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
změnil	změnit	k5eAaPmAgInS	změnit
odstín	odstín	k1gInSc1	odstín
modré	modrý	k2eAgFnSc2d1	modrá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnPc4	vlajka
švédských	švédský	k2eAgInPc2d1	švédský
krajů	kraj	k1gInPc2	kraj
==	==	k?	==
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
Švédska	Švédsko	k1gNnSc2	Švédsko
se	se	k3xPyFc4	se
správně	správně	k6eAd1	správně
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
21	[number]	k4	21
krajů	kraj	k1gInPc2	kraj
(	(	kIx(	(
<g/>
län	län	k?	län
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
kraje	kraj	k1gInPc1	kraj
mají	mít	k5eAaImIp3nP	mít
svou	svůj	k3xOyFgFnSc4	svůj
vlajku	vlajka	k1gFnSc4	vlajka
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Vlajky	vlajka	k1gFnPc1	vlajka
krajů	kraj	k1gInPc2	kraj
jsou	být	k5eAaImIp3nP	být
odvozeny	odvodit	k5eAaPmNgFnP	odvodit
od	od	k7c2	od
nových	nový	k2eAgInPc2d1	nový
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
složením	složení	k1gNnSc7	složení
znaků	znak	k1gInPc2	znak
původních	původní	k2eAgFnPc2d1	původní
<g/>
,	,	kIx,	,
tradičních	tradiční	k2eAgFnPc2d1	tradiční
provincií	provincie	k1gFnPc2	provincie
(	(	kIx(	(
<g/>
landskap	landskap	k1gInSc1	landskap
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Den	den	k1gInSc4	den
švédské	švédský	k2eAgFnSc2d1	švédská
vlajky	vlajka	k1gFnSc2	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
Švédská	švédský	k2eAgFnSc1d1	švédská
vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
velkou	velký	k2eAgFnSc4d1	velká
oblibu	obliba	k1gFnSc4	obliba
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
Švédů	Švéd	k1gMnPc2	Švéd
chována	chovat	k5eAaImNgFnS	chovat
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
úctě	úcta	k1gFnSc6	úcta
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
<g/>
,	,	kIx,	,
den	den	k1gInSc1	den
korunovace	korunovace	k1gFnSc2	korunovace
Gustava	Gustav	k1gMnSc2	Gustav
I.	I.	kA	I.
Vasy	Vasa	k1gMnSc2	Vasa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
každoročně	každoročně	k6eAd1	každoročně
slaví	slavit	k5eAaImIp3nP	slavit
jako	jako	k8xC	jako
Den	den	k1gInSc4	den
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Švédska	Švédsko	k1gNnSc2	Švédsko
</s>
</p>
<p>
<s>
Švédská	švédský	k2eAgFnSc1d1	švédská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Švédska	Švédsko	k1gNnSc2	Švédsko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Švédská	švédský	k2eAgFnSc1d1	švédská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
