<s>
Jiří	Jiří	k1gMnSc1	Jiří
Wimmer	Wimmer	k1gMnSc1	Wimmer
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1943	[number]	k4	1943
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2001	[number]	k4	2001
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
komik	komik	k1gMnSc1	komik
a	a	k8xC	a
bavič	bavič	k1gMnSc1	bavič
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
kreslíř	kreslíř	k1gMnSc1	kreslíř
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Wimmer	Wimmer	k1gMnSc1	Wimmer
<g/>
,	,	kIx,	,
alias	alias	k9	alias
"	"	kIx"	"
<g/>
Klobouk	klobouk	k1gInSc1	klobouk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
mnohostranně	mnohostranně	k6eAd1	mnohostranně
nadaný	nadaný	k2eAgMnSc1d1	nadaný
muž	muž	k1gMnSc1	muž
s	s	k7c7	s
výrazným	výrazný	k2eAgInSc7d1	výrazný
komediálním	komediální	k2eAgInSc7d1	komediální
talentem	talent	k1gInSc7	talent
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
psal	psát	k5eAaImAgInS	psát
básně	báseň	k1gFnPc4	báseň
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
zpíval	zpívat	k5eAaImAgMnS	zpívat
<g/>
,	,	kIx,	,
kreslil	kreslit	k5eAaImAgMnS	kreslit
anekdoty	anekdota	k1gFnSc2	anekdota
a	a	k8xC	a
vymýšlel	vymýšlet	k5eAaImAgInS	vymýšlet
vtipy	vtip	k1gInPc4	vtip
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
výborný	výborný	k2eAgInSc4d1	výborný
hudební	hudební	k2eAgInSc4d1	hudební
sluch	sluch	k1gInSc4	sluch
a	a	k8xC	a
rád	rád	k6eAd1	rád
sportoval	sportovat	k5eAaImAgMnS	sportovat
<g/>
,	,	kIx,	,
miloval	milovat	k5eAaImAgMnS	milovat
tenis	tenis	k1gInSc4	tenis
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolutoriu	absolutorium	k1gNnSc6	absolutorium
DAMU	DAMU	kA	DAMU
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gMnSc7	jeho
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
spolužákem	spolužák	k1gMnSc7	spolužák
a	a	k8xC	a
kamarádem	kamarád	k1gMnSc7	kamarád
byl	být	k5eAaImAgMnS	být
herec	herec	k1gMnSc1	herec
Vladimír	Vladimír	k1gMnSc1	Vladimír
Pucholt	Pucholt	k1gMnSc1	Pucholt
<g/>
)	)	kIx)	)
získal	získat	k5eAaPmAgMnS	získat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
divadelní	divadelní	k2eAgNnSc4d1	divadelní
angažmá	angažmá	k1gNnSc4	angažmá
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
Petra	Petr	k1gMnSc2	Petr
Bezruče	Bezruč	k1gFnSc2	Bezruč
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1965	[number]	k4	1965
až	až	k9	až
1970	[number]	k4	1970
a	a	k8xC	a
kde	kde	k6eAd1	kde
sehrál	sehrát	k5eAaPmAgInS	sehrát
nadprůměrně	nadprůměrně	k6eAd1	nadprůměrně
mnoho	mnoho	k6eAd1	mnoho
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
vážných	vážný	k2eAgFnPc2d1	vážná
divadelních	divadelní	k2eAgFnPc2d1	divadelní
rolí	role	k1gFnPc2	role
<g/>
.	.	kIx.	.
</s>
<s>
Souběžně	souběžně	k6eAd1	souběžně
hrál	hrát	k5eAaImAgInS	hrát
i	i	k9	i
v	v	k7c6	v
někdejším	někdejší	k2eAgNnSc6d1	někdejší
ostravském	ostravský	k2eAgNnSc6d1	ostravské
divadle	divadlo	k1gNnSc6	divadlo
malých	malý	k2eAgFnPc2d1	malá
forem	forma	k1gFnPc2	forma
Waterloo	Waterloo	k1gNnSc2	Waterloo
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
ostravském	ostravský	k2eAgNnSc6d1	ostravské
Státním	státní	k2eAgNnSc6d1	státní
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
přešel	přejít	k5eAaPmAgInS	přejít
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
společně	společně	k6eAd1	společně
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Valou	Vala	k1gMnSc7	Vala
uváděl	uvádět	k5eAaImAgMnS	uvádět
koncerty	koncert	k1gInPc4	koncert
skupiny	skupina	k1gFnSc2	skupina
Rangers	Rangers	k1gInSc1	Rangers
-	-	kIx~	-
Plavci	plavec	k1gMnPc1	plavec
<g/>
.	.	kIx.	.
</s>
<s>
Vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
i	i	k9	i
v	v	k7c6	v
libereckém	liberecký	k2eAgNnSc6d1	liberecké
a	a	k8xC	a
pražském	pražský	k2eAgNnSc6d1	Pražské
Divadle	divadlo	k1gNnSc6	divadlo
Ypsilon	ypsilon	k1gNnSc2	ypsilon
<g/>
,	,	kIx,	,
v	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
Prozatímním	prozatímní	k2eAgNnSc6d1	prozatímní
divadle	divadlo	k1gNnSc6	divadlo
Františka	František	k1gMnSc2	František
Ringo	Ringo	k1gMnSc1	Ringo
Čecha	Čech	k1gMnSc2	Čech
a	a	k8xC	a
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
Jiřího	Jiří	k1gMnSc2	Jiří
Grossmanna	Grossmann	k1gMnSc2	Grossmann
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
komickou	komický	k2eAgFnSc4d1	komická
dvojici	dvojice	k1gFnSc4	dvojice
se	s	k7c7	s
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Karlem	Karel	k1gMnSc7	Karel
Černochem	Černoch	k1gMnSc7	Černoch
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
jak	jak	k6eAd1	jak
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
estrádách	estráda	k1gFnPc6	estráda
tak	tak	k9	tak
i	i	k9	i
v	v	k7c6	v
Československé	československý	k2eAgFnSc6d1	Československá
televizi	televize	k1gFnSc6	televize
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Ring	ring	k1gInSc4	ring
volný	volný	k2eAgInSc1d1	volný
a	a	k8xC	a
Možná	možná	k9	možná
přijde	přijít	k5eAaPmIp3nS	přijít
i	i	k9	i
kouzelník	kouzelník	k1gMnSc1	kouzelník
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
také	také	k9	také
ve	v	k7c6	v
Směšném	směšný	k2eAgNnSc6d1	směšné
divadle	divadlo	k1gNnSc6	divadlo
Luďka	Luděk	k1gMnSc2	Luděk
Soboty	Sobota	k1gMnSc2	Sobota
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
velký	velký	k2eAgMnSc1d1	velký
kamarád	kamarád	k1gMnSc1	kamarád
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
dvakrát	dvakrát	k6eAd1	dvakrát
ženat	ženat	k2eAgInSc1d1	ženat
a	a	k8xC	a
dvakrát	dvakrát	k6eAd1	dvakrát
rozvedený	rozvedený	k2eAgInSc1d1	rozvedený
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
trpěl	trpět	k5eAaImAgMnS	trpět
silnými	silný	k2eAgFnPc7d1	silná
depresemi	deprese	k1gFnPc7	deprese
a	a	k8xC	a
psychickými	psychický	k2eAgInPc7d1	psychický
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
třikrát	třikrát	k6eAd1	třikrát
se	se	k3xPyFc4	se
neúspěšně	úspěšně	k6eNd1	úspěšně
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
několikrát	několikrát	k6eAd1	několikrát
se	se	k3xPyFc4	se
léčil	léčit	k5eAaImAgMnS	léčit
z	z	k7c2	z
alkoholismu	alkoholismus	k1gInSc2	alkoholismus
v	v	k7c6	v
protialkoholní	protialkoholní	k2eAgFnSc6d1	protialkoholní
léčebně	léčebna	k1gFnSc6	léčebna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
měl	mít	k5eAaImAgMnS	mít
těžkou	těžký	k2eAgFnSc4d1	těžká
autonehodu	autonehoda	k1gFnSc4	autonehoda
<g/>
,	,	kIx,	,
usnul	usnout	k5eAaPmAgMnS	usnout
a	a	k8xC	a
naboural	nabourat	k5eAaPmAgMnS	nabourat
do	do	k7c2	do
stromu	strom	k1gInSc2	strom
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Zemřel	zemřít	k5eAaPmAgInS	zemřít
tragicky	tragicky	k6eAd1	tragicky
sražen	srazit	k5eAaPmNgInS	srazit
autobusem	autobus	k1gInSc7	autobus
<g/>
,	,	kIx,	,
když	když	k8xS	když
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
alkoholu	alkohol	k1gInSc2	alkohol
přelézal	přelézat	k5eAaImAgInS	přelézat
zábradlí	zábradlí	k1gNnSc4	zábradlí
na	na	k7c6	na
Vítězném	vítězný	k2eAgNnSc6d1	vítězné
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Praze-Dejvicích	Praze-Dejvice	k1gFnPc6	Praze-Dejvice
<g/>
.	.	kIx.	.
1974	[number]	k4	1974
Jáchyme	Jáchym	k1gMnSc5	Jáchym
<g/>
,	,	kIx,	,
hoď	hodit	k5eAaPmRp2nS	hodit
ho	on	k3xPp3gMnSc4	on
do	do	k7c2	do
stroje	stroj	k1gInSc2	stroj
<g/>
!	!	kIx.	!
</s>
<s>
1977	[number]	k4	1977
Jen	jen	k6eAd1	jen
ho	on	k3xPp3gInSc4	on
nechte	nechat	k5eAaPmRp2nP	nechat
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nP	bát
1980	[number]	k4	1980
Sešlost	sešlost	k1gFnSc4	sešlost
(	(	kIx(	(
<g/>
TV	TV	kA	TV
pořad	pořad	k1gInSc1	pořad
<g/>
)	)	kIx)	)
1982	[number]	k4	1982
Možná	možná	k9	možná
přijde	přijít	k5eAaPmIp3nS	přijít
i	i	k9	i
kouzelník	kouzelník	k1gMnSc1	kouzelník
(	(	kIx(	(
<g/>
TV	TV	kA	TV
pořad	pořad	k1gInSc1	pořad
<g/>
)	)	kIx)	)
1983	[number]	k4	1983
Jára	Jára	k1gMnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
<g/>
,	,	kIx,	,
spící	spící	k2eAgFnSc1d1	spící
1985	[number]	k4	1985
O	o	k7c6	o
človíčkovi	človíček	k1gMnSc6	človíček
(	(	kIx(	(
<g/>
kreslený	kreslený	k2eAgMnSc1d1	kreslený
TV	TV	kA	TV
seriál	seriál	k1gInSc4	seriál
<g/>
)	)	kIx)	)
dabing	dabing	k1gInSc1	dabing
Seznam	seznam	k1gInSc1	seznam
děl	dít	k5eAaBmAgMnS	dít
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Jiří	Jiří	k1gMnSc1	Jiří
Wimmer	Wimmer	k1gMnSc1	Wimmer
Jiří	Jiří	k1gMnSc1	Jiří
Wimmer	Wimmer	k1gMnSc1	Wimmer
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
Jiří	Jiří	k1gMnSc1	Jiří
Wimmer	Wimmer	k1gMnSc1	Wimmer
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Jiří	Jiří	k1gMnSc1	Jiří
Wimmer	Wimmer	k1gMnSc1	Wimmer
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
Jiří	Jiří	k1gMnSc1	Jiří
Wimmer	Wimmer	k1gMnSc1	Wimmer
<g/>
:	:	kIx,	:
Vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
na	na	k7c4	na
démona	démon	k1gMnSc4	démon
na	na	k7c4	na
instinkt	instinkt	k1gInSc4	instinkt
<g/>
.	.	kIx.	.
<g/>
tyden	tyden	k?	tyden
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Ecce	Ecce	k1gInSc1	Ecce
Homo	Homo	k1gMnSc1	Homo
-	-	kIx~	-
Jiří	Jiří	k1gMnSc1	Jiří
Wimmer	Wimmer	k1gMnSc1	Wimmer
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
2011-01-25	[number]	k4	2011-01-25
Jiří	Jiří	k1gMnSc1	Jiří
Wimmer	Wimmer	k1gMnSc1	Wimmer
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
