<s>
První	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
obci	obec	k1gFnSc6
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1213	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
král	král	k1gMnSc1
Přemysl	Přemysl	k1gMnSc1
Otakar	Otakar	k1gMnSc1
I.	I.	kA
daroval	darovat	k5eAaPmAgMnS
vsi	ves	k1gFnSc3
Provodov	Provodov	k1gInSc1
a	a	k8xC
Nesvačily	Nesvačily	k1gFnPc1
klášteru	klášter	k1gInSc3
benediktinů	benediktin	k1gMnPc2
v	v	k7c6
Břevnově	Břevnov	k1gInSc6
a	a	k8xC
podřídil	podřídit	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
propropboštství	propropboštství	k1gNnSc1
v	v	k7c6
Polici	police	k1gFnSc6
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>