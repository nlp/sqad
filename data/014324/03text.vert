<s>
Provodov-Šonov	Provodov-Šonov	k1gInSc1
</s>
<s>
Provodov-Šonov	Provodov-Šonov	k1gInSc1
Obecní	obecní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
Provodov	Provodov	k1gInSc1
-	-	kIx~
Šonov	Šonov	k1gInSc1
</s>
<s>
znakvlajka	znakvlajka	k1gFnSc1
Lokalita	lokalita	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
obec	obec	k1gFnSc1
LAU	LAU	kA
2	#num#	k4
(	(	kIx(
<g/>
obec	obec	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
CZ0523	CZ0523	k4
574350	#num#	k4
Pověřená	pověřený	k2eAgFnSc1d1
obec	obec	k1gFnSc1
a	a	k8xC
obec	obec	k1gFnSc1
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
</s>
<s>
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
nad	nad	k7c7
Metují	Metuje	k1gFnSc7
Okres	okres	k1gInSc1
(	(	kIx(
<g/>
LAU	LAU	kA
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Náchod	Náchod	k1gInSc1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
523	#num#	k4
<g/>
)	)	kIx)
Kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
NUTS	NUTS	kA
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Královéhradecký	královéhradecký	k2eAgMnSc1d1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
52	#num#	k4
<g/>
)	)	kIx)
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Čechy	Čechy	k1gFnPc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
23	#num#	k4
<g/>
′	′	k?
<g/>
14	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
6	#num#	k4
<g/>
′	′	k?
<g/>
29	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
1	#num#	k4
231	#num#	k4
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rozloha	rozloha	k1gFnSc1
</s>
<s>
16,17	16,17	k4
km²	km²	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
297	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
PSČ	PSČ	kA
</s>
<s>
549	#num#	k4
08	#num#	k4
Počet	počet	k1gInSc1
částí	část	k1gFnPc2
obce	obec	k1gFnSc2
</s>
<s>
5	#num#	k4
Počet	počet	k1gInSc1
k.	k.	k?
ú.	ú.	k?
</s>
<s>
5	#num#	k4
Počet	počet	k1gInSc1
ZSJ	ZSJ	kA
</s>
<s>
7	#num#	k4
Kontakt	kontakt	k1gInSc1
Adresa	adresa	k1gFnSc1
obecního	obecní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
</s>
<s>
Šonov	Šonov	k1gInSc1
u	u	k7c2
Nového	Nového	k2eAgNnSc2d1
Města	město	k1gNnSc2
nad	nad	k7c7
Metují	Metuje	k1gFnSc7
13454908	#num#	k4
Provodov-Šonov	Provodov-Šonovo	k1gNnPc2
podatelna@provodovsonov.cz	podatelna@provodovsonov.cz	k1gMnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Ing.	ing.	kA
Josef	Josef	k1gMnSc1
Kulek	kulka	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
:	:	kIx,
www.provodovsonov.cz	www.provodovsonov.cz	k1gInSc1
</s>
<s>
Úřední	úřední	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
:	:	kIx,
typo	typa	k1gFnSc5
</s>
<s>
Provodov-Šonov	Provodov-Šonov	k1gInSc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Obec	obec	k1gFnSc1
Provodov-Šonov	Provodov-Šonov	k1gInSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Prowodow-Schonow	Prowodow-Schonow	k1gFnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
okrese	okres	k1gInSc6
Náchod	Náchod	k1gInSc1
<g/>
,	,	kIx,
kraj	kraj	k1gInSc1
Královéhradecký	královéhradecký	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
přibližně	přibližně	k6eAd1
1	#num#	k4
200	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
obci	obec	k1gFnSc6
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1213	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
král	král	k1gMnSc1
Přemysl	Přemysl	k1gMnSc1
Otakar	Otakar	k1gMnSc1
I.	I.	kA
daroval	darovat	k5eAaPmAgMnS
vsi	ves	k1gFnSc3
Provodov	Provodov	k1gInSc1
a	a	k8xC
Nesvačily	Nesvačily	k1gFnPc1
klášteru	klášter	k1gInSc3
benediktinů	benediktin	k1gMnPc2
v	v	k7c6
Břevnově	Břevnov	k1gInSc6
a	a	k8xC
podřídil	podřídit	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
propropboštství	propropboštství	k1gNnSc1
v	v	k7c6
Polici	police	k1gFnSc6
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
vsi	ves	k1gFnSc2
se	se	k3xPyFc4
odvozuje	odvozovat	k5eAaImIp3nS
od	od	k7c2
vlastního	vlastní	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
Provod	provod	k1gInSc1
<g/>
/	/	kIx~
<g/>
Prowod	Prowod	k1gInSc1
<g/>
,	,	kIx,
původně	původně	k6eAd1
to	ten	k3xDgNnSc4
byl	být	k5eAaImAgInS
Provodův	Provodův	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Utrakvistické	utrakvistický	k2eAgFnPc1d1
období	období	k1gNnSc4
církve	církev	k1gFnSc2
dokládá	dokládat	k5eAaImIp3nS
renesanční	renesanční	k2eAgInSc4d1
cínový	cínový	k2eAgInSc4d1
kalich	kalich	k1gInSc4
<g/>
,	,	kIx,
předaný	předaný	k2eAgInSc4d1
do	do	k7c2
sbírky	sbírka	k1gFnSc2
Regionálního	regionální	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
v	v	k7c6
Náchodě	Náchod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Části	část	k1gFnPc1
obce	obec	k1gFnSc2
</s>
<s>
Kleny	klen	k1gInPc1
(	(	kIx(
<g/>
k.	k.	k?
ú.	ú.	k?
Kleny	klen	k2eAgFnPc4d1
<g/>
)	)	kIx)
</s>
<s>
Provodov	Provodov	k1gInSc1
(	(	kIx(
<g/>
k.	k.	k?
ú.	ú.	k?
Provodov	Provodov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Šeřeč	Šeřeč	k1gInSc1
(	(	kIx(
<g/>
k.	k.	k?
ú.	ú.	k?
Šeřeč	Šeřeč	k1gInSc1
a	a	k8xC
Domkov	Domkov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Šonov	Šonov	k1gInSc1
u	u	k7c2
Nového	Nového	k2eAgNnSc2d1
Města	město	k1gNnSc2
nad	nad	k7c7
Metují	Metuje	k1gFnSc7
(	(	kIx(
<g/>
k.	k.	k?
ú.	ú.	k?
Šonov	Šonov	k1gInSc1
u	u	k7c2
Nového	Nového	k2eAgNnSc2d1
Města	město	k1gNnSc2
nad	nad	k7c7
Metují	Metuje	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
Václavice	Václavice	k1gFnSc1
(	(	kIx(
<g/>
k.	k.	k?
ú.	ú.	k?
Provodov	Provodov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
:	:	kIx,
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
obcích	obec	k1gFnPc6
-	-	kIx~
k	k	k7c3
1.1	1.1	k4
<g/>
.2020	.2020	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Codex	Codex	k1gInSc1
diplomaticus	diplomaticus	k1gMnSc1
necnon	necnona	k1gFnPc2
epistolaris	epistolaris	k1gInSc4
regní	regní	k2eAgFnSc2d1
Bohemiae	Bohemia	k1gFnSc2
II	II	kA
<g/>
,	,	kIx,
s.	s.	k?
400	#num#	k4
<g/>
↑	↑	k?
Antonín	Antonín	k1gMnSc1
Profous	Profous	k1gMnSc1
<g/>
,	,	kIx,
Místní	místní	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
III	III	kA
<g/>
,	,	kIx,
s.	s.	k?
495	#num#	k4
<g/>
,	,	kIx,
dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Provodov-Šonov	Provodov-Šonovo	k1gNnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Provodov-Šonov	Provodov-Šonov	k1gInSc1
v	v	k7c6
Registru	registrum	k1gNnSc6
územní	územní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
<g/>
,	,	kIx,
adres	adresa	k1gFnPc2
a	a	k8xC
nemovitostí	nemovitost	k1gFnPc2
(	(	kIx(
<g/>
RÚIAN	RÚIAN	kA
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Části	část	k1gFnSc2
obce	obec	k1gFnSc2
Provodov-Šonov	Provodov-Šonov	k1gInSc1
Části	část	k1gFnSc2
obce	obec	k1gFnSc2
</s>
<s>
KlenyProvodovŠeřečŠonov	KlenyProvodovŠeřečŠonov	k1gInSc1
u	u	k7c2
Nového	Nového	k2eAgNnSc2d1
Města	město	k1gNnSc2
nad	nad	k7c4
MetujíVáclavice	MetujíVáclavice	k1gFnPc4
Ostatní	ostatní	k2eAgFnPc4d1
</s>
<s>
Dobenín	Dobenín	k1gMnSc1
</s>
<s>
Města	město	k1gNnPc1
<g/>
,	,	kIx,
městyse	městys	k1gInPc1
a	a	k8xC
obce	obec	k1gFnPc1
okresu	okres	k1gInSc2
Náchod	Náchod	k1gInSc1
</s>
<s>
Adršpach	Adršpach	k1gInSc1
•	•	k?
Bezděkov	Bezděkov	k1gInSc1
nad	nad	k7c7
Metují	Metuje	k1gFnSc7
•	•	k?
Bohuslavice	Bohuslavice	k1gFnPc4
•	•	k?
Borová	borový	k2eAgFnSc1d1
•	•	k?
Božanov	Božanov	k1gInSc1
•	•	k?
Broumov	Broumov	k1gInSc1
•	•	k?
Brzice	Brzice	k1gFnSc2
•	•	k?
Bukovice	Bukovice	k1gFnSc2
•	•	k?
Černčice	Černčice	k1gFnSc2
•	•	k?
Červená	červený	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
•	•	k?
Červený	červený	k2eAgInSc1d1
Kostelec	Kostelec	k1gInSc1
•	•	k?
Česká	český	k2eAgFnSc1d1
Čermná	Čermný	k2eAgFnSc1d1
•	•	k?
Česká	český	k2eAgFnSc1d1
Metuje	Metuje	k1gFnSc1
•	•	k?
Česká	český	k2eAgFnSc1d1
Skalice	Skalice	k1gFnSc1
•	•	k?
Dolany	Dolany	k1gInPc1
•	•	k?
Dolní	dolní	k2eAgFnSc1d1
Radechová	Radechová	k1gFnSc1
•	•	k?
Hejtmánkovice	Hejtmánkovice	k1gFnSc2
•	•	k?
Heřmanice	Heřmanice	k1gFnSc2
•	•	k?
Heřmánkovice	Heřmánkovice	k1gFnSc2
•	•	k?
Horní	horní	k2eAgFnSc1d1
Radechová	Radechová	k1gFnSc1
•	•	k?
Hořenice	Hořenice	k1gFnSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Hořičky	Hořička	k1gFnSc2
•	•	k?
Hronov	Hronov	k1gInSc1
•	•	k?
Hynčice	Hynčice	k1gFnSc2
•	•	k?
Chvalkovice	Chvalkovice	k1gFnSc2
•	•	k?
Jaroměř	Jaroměř	k1gFnSc4
•	•	k?
Jasenná	Jasenný	k2eAgFnSc1d1
•	•	k?
Jestřebí	Jestřebí	k1gNnSc6
•	•	k?
Jetřichov	Jetřichov	k1gInSc1
•	•	k?
Kramolna	Kramolna	k1gFnSc1
•	•	k?
Křinice	Křinice	k1gFnSc1
•	•	k?
Lhota	Lhota	k1gFnSc1
pod	pod	k7c7
Hořičkami	Hořička	k1gFnPc7
•	•	k?
Libchyně	Libchyně	k1gFnPc4
•	•	k?
Litoboř	Litoboř	k1gFnSc1
•	•	k?
Machov	Machov	k1gInSc1
•	•	k?
Martínkovice	Martínkovice	k1gFnSc1
•	•	k?
Mezilečí	Mezileč	k1gFnPc2
•	•	k?
Mezilesí	mezilesí	k1gNnSc4
•	•	k?
Meziměstí	meziměstí	k1gNnSc2
•	•	k?
Nahořany	Nahořan	k1gMnPc4
•	•	k?
Náchod	Náchod	k1gInSc1
•	•	k?
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
nad	nad	k7c7
Metují	Metuje	k1gFnSc7
•	•	k?
Nový	nový	k2eAgInSc1d1
Hrádek	hrádek	k1gInSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Nový	nový	k2eAgInSc1d1
Ples	ples	k1gInSc1
•	•	k?
Otovice	Otovice	k1gFnSc2
•	•	k?
Police	police	k1gFnSc2
nad	nad	k7c7
Metují	Metuje	k1gFnSc7
•	•	k?
Provodov-Šonov	Provodov-Šonov	k1gInSc4
•	•	k?
Přibyslav	Přibyslava	k1gFnPc2
•	•	k?
Rasošky	rasoška	k1gFnSc2
•	•	k?
Rožnov	Rožnov	k1gInSc1
•	•	k?
Rychnovek	Rychnovka	k1gFnPc2
•	•	k?
Říkov	Říkov	k1gInSc1
•	•	k?
Sendraž	Sendraž	k1gFnSc1
•	•	k?
Slatina	slatina	k1gFnSc1
nad	nad	k7c7
Úpou	Úpa	k1gFnSc7
•	•	k?
Slavětín	Slavětína	k1gFnPc2
nad	nad	k7c7
Metují	Metuje	k1gFnSc7
•	•	k?
Slavoňov	Slavoňov	k1gInSc4
•	•	k?
Stárkov	Stárkov	k1gInSc1
•	•	k?
Studnice	studnice	k1gFnSc2
•	•	k?
Suchý	suchý	k2eAgInSc1d1
Důl	důl	k1gInSc1
•	•	k?
Šestajovice	Šestajovice	k1gFnSc2
•	•	k?
Šonov	Šonov	k1gInSc1
•	•	k?
Teplice	teplice	k1gFnSc1
nad	nad	k7c7
Metují	Metuje	k1gFnSc7
•	•	k?
Velichovky	Velichovka	k1gFnSc2
•	•	k?
Velká	velká	k1gFnSc1
Jesenice	Jesenice	k1gFnSc2
•	•	k?
Velké	velký	k2eAgFnSc2d1
Petrovice	Petrovice	k1gFnSc2
•	•	k?
Velké	velký	k2eAgNnSc1d1
Poříčí	Poříčí	k1gNnSc1
•	•	k?
Velký	velký	k2eAgInSc1d1
Třebešov	Třebešov	k1gInSc1
•	•	k?
Vernéřovice	Vernéřovice	k1gFnSc2
•	•	k?
Vestec	Vestec	k1gInSc1
•	•	k?
Vlkov	Vlkov	k1gInSc1
•	•	k?
Vršovka	Vršovka	k1gFnSc1
•	•	k?
Vysoká	vysoká	k1gFnSc1
Srbská	srbský	k2eAgFnSc1d1
•	•	k?
Vysokov	Vysokov	k1gInSc1
•	•	k?
Zábrodí	Zábrodí	k1gNnSc2
•	•	k?
Zaloňov	Zaloňov	k1gInSc1
•	•	k?
Žďár	Žďár	k1gInSc1
nad	nad	k7c7
Metují	Metuje	k1gFnSc7
•	•	k?
Žďárky	Žďárka	k1gFnSc2
•	•	k?
Žernov	žernov	k1gInSc1
legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
město	město	k1gNnSc1
<g/>
,	,	kIx,
městys	městys	k1gInSc1
.	.	kIx.
</s>
