<s>
Karamel	karamel	k1gInSc1	karamel
je	být	k5eAaImIp3nS	být
potravina	potravina	k1gFnSc1	potravina
oranžové	oranžový	k2eAgFnSc2d1	oranžová
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
sladké	sladký	k2eAgFnSc2d1	sladká
chuti	chuť	k1gFnSc2	chuť
s	s	k7c7	s
nádechem	nádech	k1gInSc7	nádech
jakoby	jakoby	k8xS	jakoby
připálení	připálení	k1gNnSc1	připálení
či	či	k8xC	či
opečení	opečení	k1gNnSc1	opečení
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
karamelizací	karamelizace	k1gFnSc7	karamelizace
cukru	cukr	k1gInSc2	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Karamel	karamel	k1gInSc1	karamel
bývá	bývat	k5eAaImIp3nS	bývat
používán	používat	k5eAaImNgInS	používat
k	k	k7c3	k
dochucování	dochucování	k1gNnSc3	dochucování
bonbonů	bonbon	k1gInPc2	bonbon
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
i	i	k9	i
nealkoholických	alkoholický	k2eNgInPc2d1	nealkoholický
nápojů	nápoj	k1gInPc2	nápoj
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Coca-Coly	cocaola	k1gFnSc2	coca-cola
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
běžně	běžně	k6eAd1	běžně
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xS	jako
potravinářské	potravinářský	k2eAgNnSc1d1	potravinářské
barvivo	barvivo	k1gNnSc1	barvivo
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
E	E	kA	E
kódem	kód	k1gInSc7	kód
E	E	kA	E
<g/>
150	[number]	k4	150
<g/>
)	)	kIx)	)
i	i	k9	i
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
potravinářských	potravinářský	k2eAgInPc6d1	potravinářský
výrobcích	výrobek	k1gInPc6	výrobek
(	(	kIx(	(
<g/>
piškoty	piškot	k1gInPc1	piškot
<g/>
,	,	kIx,	,
sušenky	sušenka	k1gFnPc1	sušenka
<g/>
,	,	kIx,	,
omáčky	omáčka	k1gFnPc1	omáčka
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Karamel	karamel	k1gInSc1	karamel
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
z	z	k7c2	z
cukru	cukr	k1gInSc2	cukr
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
pomalým	pomalý	k2eAgNnSc7d1	pomalé
zahřátím	zahřátí	k1gNnSc7	zahřátí
na	na	k7c4	na
asi	asi	k9	asi
170	[number]	k4	170
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
konkrétní	konkrétní	k2eAgFnSc1d1	konkrétní
teplota	teplota	k1gFnSc1	teplota
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
cukru	cukr	k1gInSc2	cukr
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
karamelizace	karamelizace	k1gFnSc1	karamelizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tání	tání	k1gNnSc2	tání
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgInSc3	který
dochází	docházet	k5eAaImIp3nS	docházet
u	u	k7c2	u
krystalů	krystal	k1gInPc2	krystal
cukru	cukr	k1gInSc2	cukr
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnSc1	jejich
teplota	teplota	k1gFnSc1	teplota
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
této	tento	k3xDgFnSc3	tento
teplotě	teplota	k1gFnSc3	teplota
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
molekuly	molekula	k1gFnPc1	molekula
cukru	cukr	k1gInSc2	cukr
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
na	na	k7c4	na
jiné	jiný	k2eAgMnPc4d1	jiný
<g/>
,	,	kIx,	,
nestálé	stálý	k2eNgFnPc1d1	nestálá
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
pak	pak	k6eAd1	pak
dávají	dávat	k5eAaImIp3nP	dávat
karamelu	karamela	k1gFnSc4	karamela
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
karamel	karamela	k1gFnPc2	karamela
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Recept	recept	k1gInSc4	recept
na	na	k7c4	na
přípravu	příprava	k1gFnSc4	příprava
karamelu	karamel	k1gInSc2	karamel
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Karamel	karamela	k1gFnPc2	karamela
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
