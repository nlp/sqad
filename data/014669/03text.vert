<s>
Konstancie	Konstancie	k1gFnSc1
Normandská	normandský	k2eAgFnSc1d1
</s>
<s>
Konstancie	Konstancie	k1gFnSc1
Bretaňskábretaňská	Bretaňskábretaňský	k2eAgFnSc1d1
vévodkyně	vévodkyně	k1gFnSc1
Manžel	manžel	k1gMnSc1
</s>
<s>
Alan	Alan	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bretaňský	bretaňský	k2eAgInSc4d1
Narození	narození	k1gNnSc1
</s>
<s>
1057	#num#	k4
<g/>
/	/	kIx~
<g/>
1061	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1090	#num#	k4
Dynastie	dynastie	k1gFnSc2
</s>
<s>
Normanská	normanský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
Otec	otec	k1gMnSc1
</s>
<s>
Vilém	Vilém	k1gMnSc1
I.	I.	kA
Dobyvatel	dobyvatel	k1gMnSc1
Matka	matka	k1gFnSc1
</s>
<s>
Matylda	Matylda	k1gFnSc1
Flanderská	flanderský	k2eAgFnSc1d1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Konstancie	Konstancie	k1gFnSc1
Normanská	normanský	k2eAgFnSc1d1
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
Constance	Constanec	k1gMnSc2
de	de	k?
Normandie	Normandie	k1gFnSc2
<g/>
,	,	kIx,
bretonsky	bretonsky	k6eAd1
Konstanza	Konstanza	k1gFnSc1
<g/>
,	,	kIx,
1057	#num#	k4
<g/>
/	/	kIx~
<g/>
1061	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
-	-	kIx~
13	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1090	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
byla	být	k5eAaImAgFnS
bretaňskou	bretaňský	k2eAgFnSc7d1
vévodkyní	vévodkyně	k1gFnSc7
a	a	k8xC
jednou	jeden	k4xCgFnSc7
z	z	k7c2
dcer	dcera	k1gFnPc2
Viléma	Vilém	k1gMnSc2
Dobyvatele	dobyvatel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Úmluva	úmluva	k1gFnSc1
o	o	k7c6
manželském	manželský	k2eAgInSc6d1
svazku	svazek	k1gInSc6
byla	být	k5eAaImAgFnS
vyústěním	vyústění	k1gNnSc7
mírového	mírový	k2eAgNnSc2d1
ujednání	ujednání	k1gNnSc2
uzavřeného	uzavřený	k2eAgNnSc2d1
mezi	mezi	k7c7
Vilémem	Vilém	k1gMnSc7
Dobyvatelem	dobyvatel	k1gMnSc7
a	a	k8xC
bretaňským	bretaňský	k2eAgMnSc7d1
vévodou	vévoda	k1gMnSc7
Alanem	Alan	k1gMnSc7
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svatba	svatba	k1gFnSc1
bretaňského	bretaňský	k2eAgMnSc2d1
vévody	vévoda	k1gMnSc2
a	a	k8xC
dcery	dcera	k1gFnSc2
anglického	anglický	k2eAgMnSc2d1
krále	král	k1gMnSc2
se	se	k3xPyFc4
konala	konat	k5eAaImAgNnP
v	v	k7c4
Bayeux	Bayeux	k1gInSc4
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
let	léto	k1gNnPc2
1086	#num#	k4
<g/>
-	-	kIx~
<g/>
1088	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konstancie	Konstancie	k1gFnSc1
zemřela	zemřít	k5eAaPmAgFnS
bezdětná	bezdětný	k2eAgFnSc1d1
v	v	k7c6
srpnu	srpen	k1gInSc6
1090	#num#	k4
<g/>
,	,	kIx,
kronikář	kronikář	k1gMnSc1
Vilém	Vilém	k1gMnSc1
z	z	k7c2
Malmesbury	Malmesbura	k1gFnSc2
hovořil	hovořit	k5eAaImAgMnS
dokonce	dokonce	k9
o	o	k7c6
jedu	jed	k1gInSc6
jako	jako	k8xC,k8xS
důvodu	důvod	k1gInSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
pohřbena	pohřbít	k5eAaPmNgFnS
v	v	k7c6
kostele	kostel	k1gInSc6
sv.	sv.	kA
Melánie	Melánie	k1gFnPc1
poblíž	poblíž	k7c2
Redonu	Redon	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Předkové	předek	k1gMnPc1
</s>
<s>
Richard	Richard	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Normandský	normandský	k2eAgInSc1d1
</s>
<s>
Robert	Robert	k1gMnSc1
I.	I.	kA
Normandský	normandský	k2eAgInSc4d1
</s>
<s>
Judita	Judita	k1gFnSc1
Bretaňská	bretaňský	k2eAgFnSc1d1
</s>
<s>
Vilém	Vilém	k1gMnSc1
Dobyvatel	dobyvatel	k1gMnSc1
</s>
<s>
Fulbert	Fulbert	k1gInSc1
z	z	k7c2
Falaise	Falaise	k1gFnSc2
</s>
<s>
Herleva	Herleva	k6eAd1
</s>
<s>
Konstancie	Konstancie	k1gFnSc1
Normandská	normandský	k2eAgFnSc1d1
</s>
<s>
Balduin	Balduin	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flanderský	flanderský	k2eAgInSc1d1
</s>
<s>
Balduin	Balduin	k1gInSc1
V.	V.	kA
Flanderský	flanderský	k2eAgInSc1d1
</s>
<s>
Ogive	Ogivat	k5eAaPmIp3nS
Lucemburská	lucemburský	k2eAgFnSc1d1
</s>
<s>
Matylda	Matylda	k1gFnSc1
Flanderská	flanderský	k2eAgFnSc1d1
</s>
<s>
Robert	Robert	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzský	francouzský	k2eAgInSc1d1
</s>
<s>
Adéla	Adéla	k1gFnSc1
Francouzská	francouzský	k2eAgFnSc1d1
</s>
<s>
Konstancie	Konstancie	k1gFnSc1
z	z	k7c2
Arles	Arlesa	k1gFnPc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Konstancie	Konstancie	k1gFnSc1
Normandská	normandský	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
1	#num#	k4
2	#num#	k4
www	www	k?
<g/>
.	.	kIx.
<g/>
fmg	fmg	k?
<g/>
.	.	kIx.
<g/>
a	a	k8xC
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Středověk	středověk	k1gInSc1
</s>
