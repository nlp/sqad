<s>
Boris	Boris	k1gMnSc1	Boris
Leonidovič	Leonidovič	k1gMnSc1	Leonidovič
Pasternak	Pasternak	k1gMnSc1	Pasternak
(	(	kIx(	(
<g/>
Б	Б	k?	Б
Л	Л	k?	Л
П	П	k?	П
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ruský	ruský	k2eAgMnSc1d1	ruský
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
