<s>
Boris	Boris	k1gMnSc1	Boris
Leonidovič	Leonidovič	k1gMnSc1	Leonidovič
Pasternak	Pasternak	k1gMnSc1	Pasternak
(	(	kIx(	(
<g/>
Б	Б	k?	Б
Л	Л	k?	Л
П	П	k?	П
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ruský	ruský	k2eAgMnSc1d1	ruský
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
umělecké	umělecký	k2eAgFnSc6d1	umělecká
rodině	rodina	k1gFnSc6	rodina
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Rosalia	Rosalia	k1gFnSc1	Rosalia
Isidorovna	Isidorovna	k1gFnSc1	Isidorovna
Pasternaková	Pasternakový	k2eAgFnSc1d1	Pasternakový
(	(	kIx(	(
<g/>
rozená	rozený	k2eAgFnSc1d1	rozená
Kaufmanová	Kaufmanová	k1gFnSc1	Kaufmanová
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
klavíristka	klavíristka	k1gFnSc1	klavíristka
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
Leonid	Leonida	k1gFnPc2	Leonida
Osipovič	Osipovič	k1gMnSc1	Osipovič
Pasternak	Pasternak	k1gMnSc1	Pasternak
akademický	akademický	k2eAgMnSc1d1	akademický
umělec	umělec	k1gMnSc1	umělec
na	na	k7c6	na
Petěrburské	petěrburský	k2eAgFnSc6d1	Petěrburská
akademii	akademie	k1gFnSc6	akademie
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Boris	Boris	k1gMnSc1	Boris
Pasternak	Pasternak	k1gMnSc1	Pasternak
měl	mít	k5eAaImAgMnS	mít
dvě	dva	k4xCgFnPc4	dva
sestry	sestra	k1gFnPc4	sestra
a	a	k8xC	a
bratra	bratr	k1gMnSc4	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Častými	častý	k2eAgMnPc7d1	častý
hosty	host	k1gMnPc7	host
Pasternakových	Pasternakový	k2eAgMnPc2d1	Pasternakový
rodičů	rodič	k1gMnPc2	rodič
byli	být	k5eAaImAgMnP	být
různí	různý	k2eAgMnPc1d1	různý
ruští	ruský	k2eAgMnPc1d1	ruský
umělci	umělec	k1gMnPc1	umělec
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
ty	ten	k3xDgInPc4	ten
nejznámější	známý	k2eAgMnSc1d3	nejznámější
patřil	patřit	k5eAaImAgMnS	patřit
například	například	k6eAd1	například
Lev	Lev	k1gMnSc1	Lev
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
skončil	skončit	k5eAaPmAgInS	skončit
gymnázium	gymnázium	k1gNnSc4	gymnázium
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
práv	právo	k1gNnPc2	právo
na	na	k7c6	na
Moskevské	moskevský	k2eAgFnSc6d1	Moskevská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
roku	rok	k1gInSc6	rok
1912	[number]	k4	1912
studoval	studovat	k5eAaImAgMnS	studovat
krátce	krátce	k6eAd1	krátce
i	i	k9	i
v	v	k7c6	v
Marburgu	Marburg	k1gInSc6	Marburg
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
založil	založit	k5eAaPmAgMnS	založit
společně	společně	k6eAd1	společně
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Lyrika	lyrika	k1gFnSc1	lyrika
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
také	také	k9	také
otiskl	otisknout	k5eAaPmAgMnS	otisknout
několik	několik	k4yIc4	několik
svých	svůj	k3xOyFgFnPc2	svůj
básní	báseň	k1gFnPc2	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Pasternak	Pasternak	k6eAd1	Pasternak
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
zakládajících	zakládající	k2eAgMnPc2d1	zakládající
členů	člen	k1gMnPc2	člen
futuristické	futuristický	k2eAgFnSc2d1	futuristická
skupiny	skupina	k1gFnSc2	skupina
Centrifuga	centrifuga	k1gFnSc1	centrifuga
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
tvorbu	tvorba	k1gFnSc4	tvorba
mělo	mít	k5eAaImAgNnS	mít
především	především	k6eAd1	především
dobré	dobrý	k2eAgNnSc4d1	dobré
rodinné	rodinný	k2eAgNnSc4d1	rodinné
zázemí	zázemí	k1gNnSc4	zázemí
a	a	k8xC	a
možnost	možnost	k1gFnSc4	možnost
setkávat	setkávat	k5eAaImF	setkávat
se	se	k3xPyFc4	se
s	s	k7c7	s
významnými	významný	k2eAgFnPc7d1	významná
osobnostmi	osobnost	k1gFnPc7	osobnost
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
zapůsobil	zapůsobit	k5eAaPmAgMnS	zapůsobit
R.	R.	kA	R.
M.	M.	kA	M.
Rilke	Rilke	k1gFnPc2	Rilke
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
román	román	k1gInSc4	román
Doktor	doktor	k1gMnSc1	doktor
Živago	Živago	k1gMnSc1	Živago
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
psal	psát	k5eAaImAgMnS	psát
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
ze	z	k7c2	z
svazu	svaz	k1gInSc2	svaz
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
hrozilo	hrozit	k5eAaImAgNnS	hrozit
mu	on	k3xPp3gMnSc3	on
odebrání	odebrání	k1gNnSc4	odebrání
sovětského	sovětský	k2eAgNnSc2d1	sovětské
občanství	občanství	k1gNnSc2	občanství
a	a	k8xC	a
vyhnání	vyhnání	k1gNnSc2	vyhnání
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
obranu	obrana	k1gFnSc4	obrana
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
světová	světový	k2eAgFnSc1d1	světová
inteligence	inteligence	k1gFnSc1	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
směl	smět	k5eAaImAgInS	smět
román	román	k1gInSc1	román
vyjít	vyjít	k5eAaPmF	vyjít
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
-	-	kIx~	-
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
bylo	být	k5eAaImAgNnS	být
dílo	dílo	k1gNnSc1	dílo
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
kritizováno	kritizovat	k5eAaImNgNnS	kritizovat
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
dokonce	dokonce	k9	dokonce
i	i	k9	i
s	s	k7c7	s
osobně	osobně	k6eAd1	osobně
motivovaným	motivovaný	k2eAgInSc7d1	motivovaný
odporem	odpor	k1gInSc7	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
sovětských	sovětský	k2eAgInPc2d1	sovětský
orgánů	orgán	k1gInPc2	orgán
zřekl	zřeknout	k5eAaPmAgMnS	zřeknout
potom	potom	k6eAd1	potom
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
ji	on	k3xPp3gFnSc4	on
telefonicky	telefonicky	k6eAd1	telefonicky
přijal	přijmout	k5eAaPmAgInS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Cenu	cena	k1gFnSc4	cena
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
přebral	přebrat	k5eAaPmAgMnS	přebrat
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Jevgenij	Jevgenij	k1gMnSc1	Jevgenij
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
básnické	básnický	k2eAgFnSc2d1	básnická
a	a	k8xC	a
prozaické	prozaický	k2eAgFnSc2d1	prozaická
tvorby	tvorba	k1gFnSc2	tvorba
se	se	k3xPyFc4	se
Pasternak	Pasternak	k1gMnSc1	Pasternak
věnoval	věnovat	k5eAaImAgMnS	věnovat
také	také	k9	také
překladům	překlad	k1gInPc3	překlad
Shakespearových	Shakespearových	k2eAgFnPc2d1	Shakespearových
tragédií	tragédie	k1gFnPc2	tragédie
a	a	k8xC	a
gruzínských	gruzínský	k2eAgMnPc2d1	gruzínský
básníků	básník	k1gMnPc2	básník
<g/>
.	.	kIx.	.
</s>
<s>
Pohřben	pohřben	k2eAgMnSc1d1	pohřben
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
Moskvy	Moskva	k1gFnSc2	Moskva
v	v	k7c6	v
spisovatelském	spisovatelský	k2eAgNnSc6d1	spisovatelské
městečku	městečko	k1gNnSc6	městečko
–	–	k?	–
Peredělkině	Peredělkina	k1gFnSc6	Peredělkina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	on	k3xPp3gInSc4	on
pohřeb	pohřeb	k1gInSc4	pohřeb
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
demonstrací	demonstrace	k1gFnSc7	demonstrace
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
úcty	úcta	k1gFnSc2	úcta
k	k	k7c3	k
svobodné	svobodný	k2eAgFnSc3d1	svobodná
tvorbě	tvorba	k1gFnSc3	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Blíženec	blíženec	k1gMnSc1	blíženec
v	v	k7c6	v
oblacích	oblak	k1gInPc6	oblak
<g/>
:	:	kIx,	:
Verše	verš	k1gInPc1	verš
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
Nad	nad	k7c7	nad
bariérami	bariéra	k1gFnPc7	bariéra
–	–	k?	–
druhá	druhý	k4xOgFnSc1	druhý
kniha	kniha	k1gFnSc1	kniha
veršů	verš	k1gInPc2	verš
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
Sestra	sestra	k1gFnSc1	sestra
má	mít	k5eAaImIp3nS	mít
-	-	kIx~	-
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Léto	léto	k1gNnSc1	léto
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
Témata	téma	k1gNnPc4	téma
a	a	k8xC	a
variace	variace	k1gFnPc4	variace
<g/>
:	:	kIx,	:
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
kniha	kniha	k1gFnSc1	kniha
básní	báseň	k1gFnPc2	báseň
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
Druhé	druhý	k4xOgNnSc1	druhý
zrození	zrození	k1gNnSc1	zrození
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
V	v	k7c6	v
ranních	ranní	k2eAgInPc6d1	ranní
vlacích	vlak	k1gInPc6	vlak
<g/>
:	:	kIx,	:
Nové	Nové	k2eAgFnPc1d1	Nové
básně	báseň	k1gFnPc1	báseň
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
Když	když	k8xS	když
se	se	k3xPyFc4	se
vyčasí	vyčasit	k5eAaPmIp3nS	vyčasit
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
–	–	k?	–
<g/>
1959	[number]	k4	1959
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
Rok	rok	k1gInSc1	rok
devatenáct	devatenáct	k4xCc4	devatenáct
set	sto	k4xCgNnPc2	sto
pět	pět	k4xCc4	pět
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
–	–	k?	–
<g/>
26	[number]	k4	26
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poéma	poéma	k1gFnSc1	poéma
Vznešená	vznešený	k2eAgFnSc1d1	vznešená
nemoc	nemoc	k1gFnSc1	nemoc
Poručík	poručík	k1gMnSc1	poručík
Šmidt	Šmidt	k1gMnSc1	Šmidt
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
–	–	k?	–
<g/>
27	[number]	k4	27
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poéma	poéma	k1gFnSc1	poéma
Spektorskij	Spektorskij	k1gFnSc1	Spektorskij
Lyrika	lyrika	k1gFnSc1	lyrika
Rok	rok	k1gInSc4	rok
devatenáctsetpět	devatenáctsetpět	k5eAaImF	devatenáctsetpět
Modrý	modrý	k2eAgMnSc1d1	modrý
host	host	k1gMnSc1	host
Světlohra	Světlohra	k1gFnSc1	Světlohra
Hvězdný	hvězdný	k2eAgInSc1d1	hvězdný
déšť	déšť	k1gInSc1	déšť
Život	život	k1gInSc1	život
můj	můj	k1gMnSc1	můj
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
bratr	bratr	k1gMnSc1	bratr
Druhé	druhý	k4xOgNnSc1	druhý
zrození	zrození	k1gNnSc1	zrození
Básně	báseň	k1gFnSc2	báseň
doktora	doktor	k1gMnSc2	doktor
Živaga	Živag	k1gMnSc2	Živag
(	(	kIx(	(
<g/>
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
N.	N.	kA	N.
<g/>
Y.	Y.	kA	Y.
<g/>
)	)	kIx)	)
Můj	můj	k3xOp1gInSc4	můj
život	život	k1gInSc4	život
Malá	malý	k2eAgFnSc1d1	malá
Luversová	Luversová	k1gFnSc1	Luversová
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
Vzdušné	vzdušný	k2eAgFnSc2d1	vzdušná
tratě	trať	k1gFnSc2	trať
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
Glejt	glejt	k1gInSc1	glejt
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
Doktor	doktor	k1gMnSc1	doktor
Živago	Živago	k1gMnSc1	Živago
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
–	–	k?	–
román	román	k1gInSc1	román
o	o	k7c6	o
osudu	osud	k1gInSc6	osud
ruské	ruský	k2eAgFnSc2d1	ruská
<g />
.	.	kIx.	.
</s>
<s>
inteligence	inteligence	k1gFnSc1	inteligence
za	za	k7c2	za
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
vydáno	vydat	k5eAaPmNgNnS	vydat
r.	r.	kA	r.
1958	[number]	k4	1958
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
zfilmováno	zfilmován	k2eAgNnSc1d1	zfilmováno
Seznam	seznam	k1gInSc4	seznam
ruských	ruský	k2eAgMnPc2d1	ruský
spisovatelů	spisovatel	k1gMnPc2	spisovatel
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Boris	Boris	k1gMnSc1	Boris
Leonidovič	Leonidovič	k1gMnSc1	Leonidovič
Pasternak	Pasternak	k1gMnSc1	Pasternak
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Boris	Boris	k1gMnSc1	Boris
Leonidovič	Leonidovič	k1gMnSc1	Leonidovič
Pasternak	Pasternak	k1gMnSc1	Pasternak
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Boris	Boris	k1gMnSc1	Boris
Leonidovič	Leonidovič	k1gMnSc1	Leonidovič
Pasternak	Pasternak	k1gMnSc1	Pasternak
</s>
