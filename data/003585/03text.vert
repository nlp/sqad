<s>
Adresář	adresář	k1gInSc1	adresář
(	(	kIx(	(
<g/>
také	také	k9	také
složka	složka	k1gFnSc1	složka
nebo	nebo	k8xC	nebo
direktorář	direktorář	k1gInSc1	direktorář
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
organizační	organizační	k2eAgFnSc1d1	organizační
jednotka	jednotka	k1gFnSc1	jednotka
v	v	k7c6	v
souborovém	souborový	k2eAgInSc6d1	souborový
systému	systém	k1gInSc6	systém
na	na	k7c6	na
datovém	datový	k2eAgNnSc6d1	datové
médiu	médium	k1gNnSc6	médium
<g/>
.	.	kIx.	.
</s>
<s>
Adresář	adresář	k1gInSc1	adresář
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
na	na	k7c6	na
disku	disk	k1gInSc6	disk
dokumenty	dokument	k1gInPc1	dokument
(	(	kIx(	(
<g/>
soubory	soubor	k1gInPc1	soubor
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
složky	složka	k1gFnPc1	složka
(	(	kIx(	(
<g/>
podadresáře	podadresář	k1gInPc1	podadresář
<g/>
)	)	kIx)	)
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
je	on	k3xPp3gInPc4	on
uživatel	uživatel	k1gMnSc1	uživatel
mohl	moct	k5eAaImAgMnS	moct
logicky	logicky	k6eAd1	logicky
uspořádat	uspořádat	k5eAaPmF	uspořádat
<g/>
.	.	kIx.	.
</s>
<s>
Adresáře	adresář	k1gInPc1	adresář
tvoří	tvořit	k5eAaImIp3nP	tvořit
na	na	k7c6	na
disku	disk	k1gInSc6	disk
stromovou	stromový	k2eAgFnSc4d1	stromová
strukturu	struktura	k1gFnSc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
jednoznačnosti	jednoznačnost	k1gFnSc3	jednoznačnost
nemohou	moct	k5eNaImIp3nP	moct
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
adresáři	adresář	k1gInSc6	adresář
existovat	existovat	k5eAaImF	existovat
dvě	dva	k4xCgFnPc4	dva
položky	položka	k1gFnPc4	položka
se	s	k7c7	s
shodným	shodný	k2eAgNnSc7d1	shodné
jménem	jméno	k1gNnSc7	jméno
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
nerozlišuje	rozlišovat	k5eNaImIp3nS	rozlišovat
mezi	mezi	k7c7	mezi
souborem	soubor	k1gInSc7	soubor
a	a	k8xC	a
adresářem	adresář	k1gInSc7	adresář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
souborové	souborový	k2eAgInPc1d1	souborový
systémy	systém	k1gInPc1	systém
neumožňují	umožňovat	k5eNaImIp3nP	umožňovat
vznik	vznik	k1gInSc4	vznik
podadresářů	podadresář	k1gInPc2	podadresář
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
jen	jen	k6eAd1	jen
kořenový	kořenový	k2eAgInSc4d1	kořenový
adresář	adresář	k1gInSc4	adresář
(	(	kIx(	(
<g/>
např.	např.	kA	např.
CP	CP	kA	CP
<g/>
/	/	kIx~	/
<g/>
M	M	kA	M
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
souborové	souborový	k2eAgInPc1d1	souborový
systémy	systém	k1gInPc1	systém
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
vznik	vznik	k1gInSc1	vznik
neorientovaných	orientovaný	k2eNgInPc2d1	neorientovaný
cyklů	cyklus	k1gInPc2	cyklus
(	(	kIx(	(
<g/>
smyček	smyčka	k1gFnPc2	smyčka
<g/>
)	)	kIx)	)
pomocí	pomocí	k7c2	pomocí
symbolických	symbolický	k2eAgInPc2d1	symbolický
odkazů	odkaz	k1gInPc2	odkaz
<g/>
.	.	kIx.	.
</s>
<s>
Pevné	pevný	k2eAgInPc1d1	pevný
odkazy	odkaz	k1gInPc1	odkaz
nejsou	být	k5eNaImIp3nP	být
obvyklé	obvyklý	k2eAgInPc1d1	obvyklý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
evidovat	evidovat	k5eAaImF	evidovat
další	další	k2eAgFnPc4d1	další
doplňující	doplňující	k2eAgFnPc4d1	doplňující
informace	informace	k1gFnPc4	informace
(	(	kIx(	(
<g/>
zpětné	zpětný	k2eAgInPc4d1	zpětný
odkazy	odkaz	k1gInPc4	odkaz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kořenový	kořenový	k2eAgInSc1d1	kořenový
adresář	adresář	k1gInSc1	adresář
má	mít	k5eAaImIp3nS	mít
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
stromové	stromový	k2eAgFnSc6d1	stromová
struktuře	struktura	k1gFnSc3	struktura
souborovém	souborový	k2eAgInSc6d1	souborový
systému	systém	k1gInSc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
adresář	adresář	k1gInSc1	adresář
v	v	k7c6	v
adresářové	adresářový	k2eAgFnSc6d1	adresářová
hierarchii	hierarchie	k1gFnSc6	hierarchie
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc1	všechen
další	další	k2eAgInPc1d1	další
adresáře	adresář	k1gInPc1	adresář
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
souborovém	souborový	k2eAgInSc6d1	souborový
systému	systém	k1gInSc6	systém
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gInSc3	jeho
podadresáři	podadresář	k1gInSc3	podadresář
<g/>
.	.	kIx.	.
v	v	k7c6	v
unixových	unixový	k2eAgInPc6d1	unixový
systémech	systém	k1gInPc6	systém
se	se	k3xPyFc4	se
kořenový	kořenový	k2eAgInSc4d1	kořenový
adresář	adresář	k1gInSc4	adresář
označuje	označovat	k5eAaImIp3nS	označovat
znakem	znak	k1gInSc7	znak
lomítko	lomítko	k1gNnSc1	lomítko
(	(	kIx(	(
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
společný	společný	k2eAgInSc1d1	společný
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc4	všechen
připojená	připojený	k2eAgNnPc4d1	připojené
média	médium	k1gNnPc4	médium
v	v	k7c6	v
operačních	operační	k2eAgInPc6d1	operační
systémech	systém	k1gInPc6	systém
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
(	(	kIx(	(
<g/>
též	též	k9	též
<g />
.	.	kIx.	.
</s>
<s>
DOS	DOS	kA	DOS
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
každý	každý	k3xTgInSc4	každý
svazek	svazek	k1gInSc4	svazek
(	(	kIx(	(
<g/>
logická	logický	k2eAgFnSc1d1	logická
disková	diskový	k2eAgFnSc1d1	disková
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
)	)	kIx)	)
svůj	svůj	k3xOyFgInSc4	svůj
kořenový	kořenový	k2eAgInSc4d1	kořenový
adresář	adresář	k1gInSc4	adresář
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
označení	označení	k1gNnSc1	označení
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
označení	označení	k1gNnSc2	označení
jednotky	jednotka	k1gFnSc2	jednotka
(	(	kIx(	(
<g/>
písmeno	písmeno	k1gNnSc1	písmeno
latinky	latinka	k1gFnSc2	latinka
a	a	k8xC	a
dvojtečka	dvojtečka	k1gFnSc1	dvojtečka
<g/>
)	)	kIx)	)
a	a	k8xC	a
zpětného	zpětný	k2eAgNnSc2d1	zpětné
lomítka	lomítko	k1gNnSc2	lomítko
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
C	C	kA	C
<g/>
:	:	kIx,	:
<g/>
\	\	kIx~	\
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Cesta	cesta	k1gFnSc1	cesta
(	(	kIx(	(
<g/>
informatika	informatika	k1gFnSc1	informatika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
identifikaci	identifikace	k1gFnSc6	identifikace
adresáře	adresář	k1gInSc2	adresář
nebo	nebo	k8xC	nebo
souboru	soubor	k1gInSc2	soubor
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgInSc7	který
má	mít	k5eAaImIp3nS	mít
program	program	k1gInSc1	program
(	(	kIx(	(
<g/>
uživatel	uživatel	k1gMnSc1	uživatel
<g/>
)	)	kIx)	)
pracovat	pracovat	k5eAaImF	pracovat
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
tak	tak	k6eAd1	tak
zvaná	zvaný	k2eAgFnSc1d1	zvaná
cesta	cesta	k1gFnSc1	cesta
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
path	path	k1gInSc1	path
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
zápis	zápis	k1gInSc1	zápis
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
absolutní	absolutní	k2eAgInSc1d1	absolutní
(	(	kIx(	(
<g/>
úplný	úplný	k2eAgInSc1d1	úplný
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
relativní	relativní	k2eAgFnSc1d1	relativní
(	(	kIx(	(
<g/>
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
se	se	k3xPyFc4	se
k	k	k7c3	k
aktuálnímu	aktuální	k2eAgInSc3d1	aktuální
pracovnímu	pracovní	k2eAgInSc3d1	pracovní
adresáři	adresář	k1gInSc3	adresář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Soubor	soubor	k1gInSc1	soubor
Souborový	souborový	k2eAgInSc4d1	souborový
systém	systém	k1gInSc4	systém
Filesystem	Filesyst	k1gInSc7	Filesyst
Hierarchy	hierarcha	k1gMnSc2	hierarcha
Standard	standard	k1gInSc1	standard
–	–	k?	–
standard	standard	k1gInSc4	standard
adresářové	adresářový	k2eAgFnSc2d1	adresářová
struktury	struktura	k1gFnSc2	struktura
v	v	k7c6	v
unixových	unixový	k2eAgInPc6d1	unixový
systémech	systém	k1gInPc6	systém
V	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
významu	význam	k1gInSc6	význam
slovo	slovo	k1gNnSc1	slovo
adresář	adresář	k1gInSc1	adresář
označuje	označovat	k5eAaImIp3nS	označovat
seznam	seznam	k1gInSc4	seznam
poštovních	poštovní	k2eAgFnPc2d1	poštovní
doručovacích	doručovací	k2eAgFnPc2d1	doručovací
adres	adresa	k1gFnPc2	adresa
<g/>
.	.	kIx.	.
</s>
