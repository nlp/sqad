<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
jižní	jižní	k2eAgFnSc1d1	jižní
polovina	polovina	k1gFnSc1	polovina
včetně	včetně	k7c2	včetně
rozhledny	rozhledna	k1gFnSc2	rozhledna
náleží	náležet	k5eAaImIp3nS	náležet
katastrálnímu	katastrální	k2eAgNnSc3d1	katastrální
území	území	k1gNnSc3	území
Horní	horní	k2eAgInSc4d1	horní
Maxov	Maxov	k1gInSc4	Maxov
(	(	kIx(	(
<g/>
část	část	k1gFnSc4	část
města	město	k1gNnSc2	město
Lučany	Lučan	k1gMnPc4	Lučan
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc1d1	severní
polovina	polovina	k1gFnSc1	polovina
pak	pak	k6eAd1	pak
k.	k.	k?	k.
ú.	ú.	k?	ú.
Karlov	Karlov	k1gInSc1	Karlov
u	u	k7c2	u
Josefova	Josefův	k2eAgInSc2d1	Josefův
Dolu	dol	k1gInSc2	dol
(	(	kIx(	(
<g/>
část	část	k1gFnSc4	část
obce	obec	k1gFnSc2	obec
Josefův	Josefův	k2eAgInSc4d1	Josefův
Důl	důl	k1gInSc4	důl
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
rozhraní	rozhraní	k1gNnSc1	rozhraní
probíhá	probíhat	k5eAaImIp3nS	probíhat
přes	přes	k7c4	přes
vrchol	vrchol	k1gInSc4	vrchol
<g/>
.	.	kIx.	.
</s>
