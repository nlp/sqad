<p>
<s>
Slovanka	Slovanka	k1gFnSc1	Slovanka
je	být	k5eAaImIp3nS	být
820	[number]	k4	820
m	m	kA	m
vysoký	vysoký	k2eAgInSc1d1	vysoký
zalesněný	zalesněný	k2eAgInSc1d1	zalesněný
vrch	vrch	k1gInSc1	vrch
v	v	k7c6	v
Maxovském	Maxovský	k2eAgInSc6d1	Maxovský
hřebeni	hřeben	k1gInSc6	hřeben
Jizerských	jizerský	k2eAgFnPc2d1	Jizerská
hor	hora	k1gFnPc2	hora
nad	nad	k7c7	nad
Hraběticemi	Hrabětice	k1gFnPc7	Hrabětice
nedaleko	nedaleko	k7c2	nedaleko
lyžařského	lyžařský	k2eAgNnSc2d1	lyžařské
střediska	středisko	k1gNnSc2	středisko
Severák	severák	k1gInSc1	severák
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
jižní	jižní	k2eAgFnSc1d1	jižní
polovina	polovina	k1gFnSc1	polovina
včetně	včetně	k7c2	včetně
rozhledny	rozhledna	k1gFnSc2	rozhledna
náleží	náležet	k5eAaImIp3nS	náležet
katastrálnímu	katastrální	k2eAgNnSc3d1	katastrální
území	území	k1gNnSc3	území
Horní	horní	k2eAgInSc4d1	horní
Maxov	Maxov	k1gInSc4	Maxov
(	(	kIx(	(
<g/>
část	část	k1gFnSc4	část
města	město	k1gNnSc2	město
Lučany	Lučan	k1gMnPc4	Lučan
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc1d1	severní
polovina	polovina	k1gFnSc1	polovina
pak	pak	k6eAd1	pak
k.	k.	k?	k.
ú.	ú.	k?	ú.
Karlov	Karlov	k1gInSc1	Karlov
u	u	k7c2	u
Josefova	Josefův	k2eAgInSc2d1	Josefův
Dolu	dol	k1gInSc2	dol
(	(	kIx(	(
<g/>
část	část	k1gFnSc4	část
obce	obec	k1gFnSc2	obec
Josefův	Josefův	k2eAgInSc4d1	Josefův
Důl	důl	k1gInSc4	důl
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
rozhraní	rozhraní	k1gNnSc1	rozhraní
probíhá	probíhat	k5eAaImIp3nS	probíhat
přes	přes	k7c4	přes
vrchol	vrchol	k1gInSc4	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Kopec	kopec	k1gInSc1	kopec
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Seibtův	Seibtův	k2eAgInSc4d1	Seibtův
vrch	vrch	k1gInSc4	vrch
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Seibthübel	Seibthübela	k1gFnPc2	Seibthübela
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
svého	svůj	k3xOyFgMnSc4	svůj
majitele	majitel	k1gMnSc4	majitel
<g/>
,	,	kIx,	,
jméno	jméno	k1gNnSc4	jméno
Slovanka	Slovanka	k1gFnSc1	Slovanka
nese	nést	k5eAaImIp3nS	nést
po	po	k7c6	po
podnikové	podnikový	k2eAgFnSc6d1	podniková
chatě	chata	k1gFnSc6	chata
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
vrcholu	vrchol	k1gInSc6	vrchol
stávala	stávat	k5eAaImAgFnS	stávat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
hory	hora	k1gFnSc2	hora
nejstarší	starý	k2eAgFnSc1d3	nejstarší
železná	železný	k2eAgFnSc1d1	železná
rozhledna	rozhledna	k1gFnSc1	rozhledna
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
výšce	výška	k1gFnSc6	výška
14	[number]	k4	14
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozhledna	rozhledna	k1gFnSc1	rozhledna
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
se	se	k3xPyFc4	se
jablonecká	jablonecký	k2eAgFnSc1d1	Jablonecká
<g/>
,	,	kIx,	,
janovská	janovský	k2eAgFnSc1d1	janovská
a	a	k8xC	a
hornokamenická	hornokamenický	k2eAgFnSc1d1	hornokamenický
sekce	sekce	k1gFnSc1	sekce
Horského	Horského	k2eAgInSc2d1	Horského
spolku	spolek	k1gInSc2	spolek
dohodly	dohodnout	k5eAaPmAgFnP	dohodnout
na	na	k7c6	na
výstavbě	výstavba	k1gFnSc6	výstavba
rozhledny	rozhledna	k1gFnSc2	rozhledna
na	na	k7c6	na
Seibtově	Seibtův	k2eAgInSc6d1	Seibtův
vrchu	vrch	k1gInSc6	vrch
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
stavební	stavební	k2eAgFnSc4d1	stavební
parcelu	parcela	k1gFnSc4	parcela
jim	on	k3xPp3gMnPc3	on
majitel	majitel	k1gMnSc1	majitel
vrchu	vrch	k1gInSc2	vrch
<g/>
,	,	kIx,	,
pan	pan	k1gMnSc1	pan
Seibt	Seibt	k1gMnSc1	Seibt
<g/>
,	,	kIx,	,
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
bezplatně	bezplatně	k6eAd1	bezplatně
<g/>
,	,	kIx,	,
zbývalo	zbývat	k5eAaImAgNnS	zbývat
jen	jen	k6eAd1	jen
vybrat	vybrat	k5eAaPmF	vybrat
vhodný	vhodný	k2eAgInSc4d1	vhodný
projekt	projekt	k1gInSc4	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
návrh	návrh	k1gInSc1	návrh
zakoupit	zakoupit	k5eAaPmF	zakoupit
od	od	k7c2	od
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
firmy	firma	k1gFnSc2	firma
Waagner	Waagner	k1gMnSc1	Waagner
jedenáctimetrovou	jedenáctimetrový	k2eAgFnSc4d1	jedenáctimetrová
litinovou	litinový	k2eAgFnSc4d1	litinová
konstrukci	konstrukce	k1gFnSc4	konstrukce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
vystavena	vystavit	k5eAaPmNgFnS	vystavit
na	na	k7c6	na
výstavě	výstava	k1gFnSc6	výstava
pořádané	pořádaný	k2eAgFnSc6d1	pořádaná
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jediný	jediný	k2eAgMnSc1d1	jediný
zedník	zedník	k1gMnSc1	zedník
postavil	postavit	k5eAaPmAgMnS	postavit
během	během	k7c2	během
několika	několik	k4yIc2	několik
dní	den	k1gInPc2	den
3	[number]	k4	3
m	m	kA	m
vysokou	vysoký	k2eAgFnSc4d1	vysoká
podezdívku	podezdívka	k1gFnSc4	podezdívka
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgMnPc4	který
další	další	k2eAgMnPc1d1	další
tři	tři	k4xCgMnPc1	tři
dělníci	dělník	k1gMnPc1	dělník
sestavili	sestavit	k5eAaPmAgMnP	sestavit
dovezenou	dovezený	k2eAgFnSc4d1	dovezená
11	[number]	k4	11
m	m	kA	m
vysokou	vysoký	k2eAgFnSc4d1	vysoká
a	a	k8xC	a
5	[number]	k4	5
t	t	k?	t
těžkou	těžký	k2eAgFnSc4d1	těžká
konstrukci	konstrukce	k1gFnSc4	konstrukce
rozhledny	rozhledna	k1gFnSc2	rozhledna
během	během	k7c2	během
pouhých	pouhý	k2eAgInPc2d1	pouhý
17	[number]	k4	17
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
otevření	otevření	k1gNnSc1	otevření
rozhledny	rozhledna	k1gFnSc2	rozhledna
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
14	[number]	k4	14
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1887	[number]	k4	1887
a	a	k8xC	a
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc2	on
na	na	k7c4	na
5000	[number]	k4	5000
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterých	který	k3yIgInPc2	který
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
dobrovolném	dobrovolný	k2eAgNnSc6d1	dobrovolné
vstupném	vstupné	k1gNnSc6	vstupné
vybráno	vybrat	k5eAaPmNgNnS	vybrat
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
činila	činit	k5eAaImAgFnS	činit
cena	cena	k1gFnSc1	cena
celé	celý	k2eAgFnSc2d1	celá
stavby	stavba	k1gFnSc2	stavba
včetně	včetně	k7c2	včetně
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
vyhořela	vyhořet	k5eAaPmAgFnS	vyhořet
původní	původní	k2eAgFnSc1d1	původní
turistická	turistický	k2eAgFnSc1d1	turistická
chata	chata	k1gFnSc1	chata
<g/>
,	,	kIx,	,
nová	nový	k2eAgFnSc1d1	nová
chata	chata	k1gFnSc1	chata
"	"	kIx"	"
<g/>
Slovanka	Slovanka	k1gFnSc1	Slovanka
<g/>
"	"	kIx"	"
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
rozhledna	rozhledna	k1gFnSc1	rozhledna
udržovaná	udržovaný	k2eAgFnSc1d1	udržovaná
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
však	však	k9	však
byl	být	k5eAaImAgInS	být
vstup	vstup	k1gInSc4	vstup
jen	jen	k9	jen
na	na	k7c4	na
vlastní	vlastní	k2eAgNnSc4d1	vlastní
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Rozhledna	rozhledna	k1gFnSc1	rozhledna
chátrala	chátrat	k5eAaImAgFnS	chátrat
<g/>
,	,	kIx,	,
až	až	k8xS	až
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
stala	stát	k5eAaPmAgFnS	stát
nepřístupnou	přístupný	k2eNgFnSc4d1	nepřístupná
<g/>
.	.	kIx.	.
</s>
<s>
Záchranu	záchrana	k1gFnSc4	záchrana
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
znamenalo	znamenat	k5eAaImAgNnS	znamenat
vyhlášení	vyhlášení	k1gNnSc1	vyhlášení
technickou	technický	k2eAgFnSc7d1	technická
památkou	památka	k1gFnSc7	památka
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
a	a	k8xC	a
založení	založení	k1gNnSc4	založení
občanského	občanský	k2eAgNnSc2d1	občanské
sdružení	sdružení	k1gNnSc2	sdružení
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
záchranu	záchrana	k1gFnSc4	záchrana
9	[number]	k4	9
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
finančním	finanční	k2eAgInPc3d1	finanční
příspěvkům	příspěvek	k1gInPc3	příspěvek
od	od	k7c2	od
různých	různý	k2eAgMnPc2d1	různý
dárců	dárce	k1gMnPc2	dárce
byla	být	k5eAaImAgFnS	být
rozhledna	rozhledna	k1gFnSc1	rozhledna
restaurována	restaurován	k2eAgFnSc1d1	restaurována
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
znovu	znovu	k6eAd1	znovu
otevřena	otevřít	k5eAaPmNgFnS	otevřít
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
přišla	přijít	k5eAaPmAgFnS	přijít
na	na	k7c4	na
550	[number]	k4	550
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Slovanka	Slovanka	k1gFnSc1	Slovanka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Rozhledna	rozhledna	k1gFnSc1	rozhledna
na	na	k7c4	na
Rozhledny	rozhledna	k1gFnPc4	rozhledna
Jizerských	jizerský	k2eAgFnPc2d1	Jizerská
hor	hora	k1gFnPc2	hora
</s>
</p>
<p>
<s>
Občanské	občanský	k2eAgNnSc1d1	občanské
sdružení	sdružení	k1gNnSc1	sdružení
pro	pro	k7c4	pro
obnovu	obnova	k1gFnSc4	obnova
rozhledny	rozhledna	k1gFnSc2	rozhledna
Slovanka	Slovanka	k1gFnSc1	Slovanka
</s>
</p>
<p>
<s>
Slovanka	Slovanka	k1gFnSc1	Slovanka
na	na	k7c4	na
Rozhledny	rozhledna	k1gFnPc4	rozhledna
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
</s>
</p>
<p>
<s>
Znovuotevření	znovuotevření	k1gNnSc4	znovuotevření
rozhledny	rozhledna	k1gFnSc2	rozhledna
Slovanka	Slovanka	k1gFnSc1	Slovanka
na	na	k7c6	na
serveru	server	k1gInSc6	server
Jizerské	jizerský	k2eAgFnSc2d1	Jizerská
hory	hora	k1gFnSc2	hora
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
rozhleden	rozhledna	k1gFnPc2	rozhledna
v	v	k7c6	v
Jizerských	jizerský	k2eAgFnPc6d1	Jizerská
horách	hora	k1gFnPc6	hora
dle	dle	k7c2	dle
roku	rok	k1gInSc2	rok
otevření	otevření	k1gNnSc2	otevření
</s>
</p>
<p>
<s>
Rozhledna	rozhledna	k1gFnSc1	rozhledna
Slovanka	Slovanka	k1gFnSc1	Slovanka
na	na	k7c6	na
WIKIROZHLEDNY	WIKIROZHLEDNY	kA	WIKIROZHLEDNY
<g/>
.	.	kIx.	.
<g/>
CZ	CZ	kA	CZ
</s>
</p>
