<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
má	mít	k5eAaImIp3nS	mít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
28	[number]	k4	28
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
500	[number]	k4	500
miliónů	milión	k4xCgInPc2	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
;	;	kIx,	;
lidnatější	lidnatý	k2eAgInPc1d2	lidnatější
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
1	[number]	k4	1
306	[number]	k4	306
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
a	a	k8xC	a
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
1	[number]	k4	1
080	[number]	k4	080
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
