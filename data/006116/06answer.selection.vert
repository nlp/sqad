<s>
Zavedly	zavést	k5eAaPmAgFnP	zavést
ji	on	k3xPp3gFnSc4	on
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
agentura	agentura	k1gFnSc1	agentura
pro	pro	k7c4	pro
atomovou	atomový	k2eAgFnSc4d1	atomová
energii	energie	k1gFnSc4	energie
(	(	kIx(	(
<g/>
česká	český	k2eAgFnSc1d1	Česká
zkratka	zkratka	k1gFnSc1	zkratka
MAAE	MAAE	kA	MAAE
<g/>
,	,	kIx,	,
anglická	anglický	k2eAgFnSc1d1	anglická
zkratka	zkratka	k1gFnSc1	zkratka
IAEA	IAEA	kA	IAEA
<g/>
)	)	kIx)	)
a	a	k8xC	a
Agentura	agentura	k1gFnSc1	agentura
pro	pro	k7c4	pro
jadernou	jaderný	k2eAgFnSc4d1	jaderná
energii	energie	k1gFnSc4	energie
OECD	OECD	kA	OECD
(	(	kIx(	(
<g/>
OECD	OECD	kA	OECD
<g/>
/	/	kIx~	/
<g/>
NEA	NEA	kA	NEA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
