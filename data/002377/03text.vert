<s>
Tridentský	tridentský	k2eAgInSc1d1	tridentský
koncil	koncil	k1gInSc1	koncil
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Tridentinum	Tridentinum	k1gInSc1	Tridentinum
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
19	[number]	k4	19
<g/>
.	.	kIx.	.
ekumenický	ekumenický	k2eAgInSc1d1	ekumenický
koncil	koncil	k1gInSc1	koncil
uznaný	uznaný	k2eAgInSc1d1	uznaný
katolickou	katolický	k2eAgFnSc7d1	katolická
církví	církev	k1gFnSc7	církev
<g/>
.	.	kIx.	.
</s>
<s>
Svolal	svolat	k5eAaPmAgMnS	svolat
jej	on	k3xPp3gMnSc4	on
papež	papež	k1gMnSc1	papež
Pavel	Pavel	k1gMnSc1	Pavel
III	III	kA	III
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
1545	[number]	k4	1545
a	a	k8xC	a
zasedal	zasedat	k5eAaImAgInS	zasedat
v	v	k7c6	v
italském	italský	k2eAgMnSc6d1	italský
Tridentu	Trident	k1gMnSc6	Trident
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
Trento	Trent	k2eAgNnSc1d1	Trento
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Tridentum	Tridentum	k1gNnSc1	Tridentum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Účastnilo	účastnit	k5eAaImAgNnS	účastnit
se	se	k3xPyFc4	se
jej	on	k3xPp3gNnSc2	on
asi	asi	k9	asi
255	[number]	k4	255
biskupů	biskup	k1gMnPc2	biskup
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
reagovali	reagovat	k5eAaBmAgMnP	reagovat
na	na	k7c4	na
vznik	vznik	k1gInSc4	vznik
protestantství	protestantství	k1gNnSc2	protestantství
a	a	k8xC	a
položili	položit	k5eAaPmAgMnP	položit
základ	základ	k1gInSc4	základ
katolické	katolický	k2eAgFnSc2d1	katolická
reformace	reformace	k1gFnSc2	reformace
<g/>
.	.	kIx.	.
</s>
<s>
Koncil	koncil	k1gInSc1	koncil
schválil	schválit	k5eAaPmAgInS	schválit
16	[number]	k4	16
dogmatických	dogmatický	k2eAgInPc2d1	dogmatický
dekretů	dekret	k1gInPc2	dekret
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
věnují	věnovat	k5eAaPmIp3nP	věnovat
mnoha	mnoho	k4c2	mnoho
aspektům	aspekt	k1gInPc3	aspekt
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Koncil	koncil	k1gInSc1	koncil
zasedal	zasedat	k5eAaImAgInS	zasedat
v	v	k7c6	v
období	období	k1gNnSc6	období
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosincem	prosinec	k1gInSc7	prosinec
1545	[number]	k4	1545
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosincem	prosinec	k1gInSc7	prosinec
1563	[number]	k4	1563
<g/>
.	.	kIx.	.
</s>
<s>
Předsedali	předsedat	k5eAaImAgMnP	předsedat
mu	on	k3xPp3gMnSc3	on
postupně	postupně	k6eAd1	postupně
tří	tři	k4xCgFnPc2	tři
papežové	papež	k1gMnPc1	papež
-	-	kIx~	-
Pavel	Pavel	k1gMnSc1	Pavel
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Julius	Julius	k1gMnSc1	Julius
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
Pius	Pius	k1gMnSc1	Pius
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
průběh	průběh	k1gInSc1	průběh
se	se	k3xPyFc4	se
členil	členit	k5eAaImAgInS	členit
do	do	k7c2	do
tří	tři	k4xCgNnPc2	tři
období	období	k1gNnPc2	období
(	(	kIx(	(
<g/>
1545	[number]	k4	1545
<g/>
-	-	kIx~	-
<g/>
1549	[number]	k4	1549
<g/>
,	,	kIx,	,
1551-1552	[number]	k4	1551-1552
a	a	k8xC	a
1562	[number]	k4	1562
<g/>
-	-	kIx~	-
<g/>
1563	[number]	k4	1563
<g/>
)	)	kIx)	)
a	a	k8xC	a
25	[number]	k4	25
zasedání	zasedání	k1gNnSc2	zasedání
<g/>
.	.	kIx.	.
</s>
<s>
Koncil	koncil	k1gInSc1	koncil
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
reformaci	reformace	k1gFnSc4	reformace
a	a	k8xC	a
proti	proti	k7c3	proti
ní	on	k3xPp3gFnSc3	on
vymezil	vymezit	k5eAaPmAgInS	vymezit
katolickou	katolický	k2eAgFnSc4d1	katolická
nauku	nauka	k1gFnSc4	nauka
o	o	k7c6	o
spáse	spása	k1gFnSc6	spása
a	a	k8xC	a
ospravedlnění	ospravedlnění	k1gNnSc6	ospravedlnění
<g/>
,	,	kIx,	,
svátostech	svátost	k1gFnPc6	svátost
a	a	k8xC	a
biblickém	biblický	k2eAgInSc6d1	biblický
kánonu	kánon	k1gInSc6	kánon
<g/>
.	.	kIx.	.
</s>
<s>
Zasadil	zasadit	k5eAaPmAgMnS	zasadit
se	se	k3xPyFc4	se
o	o	k7c4	o
sjednocení	sjednocení	k1gNnSc4	sjednocení
liturgie	liturgie	k1gFnSc2	liturgie
v	v	k7c6	v
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
a	a	k8xC	a
omezil	omezit	k5eAaPmAgMnS	omezit
rozdílné	rozdílný	k2eAgFnSc2d1	rozdílná
místní	místní	k2eAgFnSc2d1	místní
praxe	praxe	k1gFnSc2	praxe
<g/>
;	;	kIx,	;
tím	ten	k3xDgNnSc7	ten
byla	být	k5eAaImAgFnS	být
fixována	fixován	k2eAgFnSc1d1	fixována
podoba	podoba	k1gFnSc1	podoba
tzv.	tzv.	kA	tzv.
tridentské	tridentský	k2eAgFnSc2d1	tridentská
mše	mše	k1gFnSc2	mše
<g/>
,	,	kIx,	,
výhradní	výhradní	k2eAgFnSc2d1	výhradní
formy	forma	k1gFnSc2	forma
římského	římský	k2eAgInSc2d1	římský
ritu	rit	k1gInSc2	rit
až	až	k9	až
do	do	k7c2	do
vydání	vydání	k1gNnSc2	vydání
misálu	misál	k1gInSc2	misál
Pavla	Pavel	k1gMnSc2	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Cíle	cíl	k1gInPc1	cíl
koncilu	koncil	k1gInSc2	koncil
byly	být	k5eAaImAgInP	být
dva	dva	k4xCgInPc1	dva
<g/>
:	:	kIx,	:
</s>
<s>
Reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
vznik	vznik	k1gInSc4	vznik
protestantství	protestantství	k1gNnSc2	protestantství
a	a	k8xC	a
definovat	definovat	k5eAaBmF	definovat
nauku	nauka	k1gFnSc4	nauka
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
v	v	k7c6	v
diskutovaných	diskutovaný	k2eAgFnPc6d1	diskutovaná
otázkách	otázka	k1gFnPc6	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
se	se	k3xPyFc4	se
zasazoval	zasazovat	k5eAaImAgMnS	zasazovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
protestanti	protestant	k1gMnPc1	protestant
měli	mít	k5eAaImAgMnP	mít
na	na	k7c6	na
koncilu	koncil	k1gInSc6	koncil
svůj	svůj	k3xOyFgInSc4	svůj
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgMnS	být
sám	sám	k3xTgMnSc1	sám
katolíkem	katolík	k1gMnSc7	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
papeži	papež	k1gMnSc3	papež
se	se	k3xPyFc4	se
také	také	k9	také
snažil	snažit	k5eAaImAgMnS	snažit
o	o	k7c6	o
znovusjednocení	znovusjednocení	k1gNnSc6	znovusjednocení
víry	víra	k1gFnSc2	víra
v	v	k7c6	v
německých	německý	k2eAgFnPc6d1	německá
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
nejlépe	dobře	k6eAd3	dobře
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
jeho	jeho	k3xOp3gFnPc3	jeho
potřebám	potřeba	k1gFnPc3	potřeba
centralizace	centralizace	k1gFnSc2	centralizace
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhého	druhý	k4xOgNnSc2	druhý
zasedání	zasedání	k1gNnSc2	zasedání
koncilu	koncil	k1gInSc2	koncil
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1551	[number]	k4	1551
<g/>
-	-	kIx~	-
<g/>
1552	[number]	k4	1552
byli	být	k5eAaImAgMnP	být
protestanti	protestant	k1gMnPc1	protestant
dvakrát	dvakrát	k6eAd1	dvakrát
pozváni	pozvat	k5eAaPmNgMnP	pozvat
a	a	k8xC	a
koncil	koncil	k1gInSc4	koncil
vydal	vydat	k5eAaPmAgMnS	vydat
záruky	záruka	k1gFnPc4	záruka
pro	pro	k7c4	pro
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
reformátorských	reformátorský	k2eAgMnPc2d1	reformátorský
vyslanců	vyslanec	k1gMnPc2	vyslanec
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
jim	on	k3xPp3gMnPc3	on
nabídnuto	nabídnut	k2eAgNnSc1d1	nabídnuto
právo	právo	k1gNnSc1	právo
diskutovat	diskutovat	k5eAaImF	diskutovat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
upřeno	upřen	k2eAgNnSc1d1	upřeno
hlasování	hlasování	k1gNnSc1	hlasování
<g/>
.	.	kIx.	.
</s>
<s>
Philipp	Philipp	k1gMnSc1	Philipp
Melanchthon	Melanchthon	k1gMnSc1	Melanchthon
a	a	k8xC	a
Johann	Johann	k1gMnSc1	Johann
Brenz	Brenz	k1gMnSc1	Brenz
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
německými	německý	k2eAgMnPc7d1	německý
lutherány	lutherán	k2eAgFnPc1d1	lutherán
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
na	na	k7c4	na
koncil	koncil	k1gInSc4	koncil
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgNnPc7	svůj
vyznáními	vyznání	k1gNnPc7	vyznání
(	(	kIx(	(
<g/>
Melanchthon	Melanchthon	k1gInSc1	Melanchthon
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgNnSc7	svůj
irenickým	irenický	k2eAgNnSc7d1	irenický
vyznáním	vyznání	k1gNnSc7	vyznání
známým	známý	k2eAgNnSc7d1	známé
jako	jako	k8xC	jako
Confessio	Confessio	k6eAd1	Confessio
Saxonica	Saxonic	k2eAgFnSc1d1	Saxonic
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
upření	upření	k1gNnSc1	upření
práva	právo	k1gNnSc2	právo
hlasovat	hlasovat	k5eAaImF	hlasovat
a	a	k8xC	a
průběžný	průběžný	k2eAgInSc4d1	průběžný
úspěch	úspěch	k1gInSc4	úspěch
vojenské	vojenský	k2eAgFnSc2d1	vojenská
kampaně	kampaň	k1gFnSc2	kampaň
proti	proti	k7c3	proti
císaři	císař	k1gMnSc3	císař
Karlu	Karel	k1gMnSc3	Karel
V.	V.	kA	V.
je	on	k3xPp3gNnSc4	on
přimělo	přimět	k5eAaPmAgNnS	přimět
ukončit	ukončit	k5eAaPmF	ukončit
protestantskou	protestantský	k2eAgFnSc4d1	protestantská
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
koncilem	koncil	k1gInSc7	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Připravit	připravit	k5eAaPmF	připravit
reformu	reforma	k1gFnSc4	reforma
církevní	církevní	k2eAgFnSc2d1	církevní
discipliny	disciplina	k1gFnSc2	disciplina
a	a	k8xC	a
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
cíl	cíl	k1gInSc1	cíl
byl	být	k5eAaImAgInS	být
důvodem	důvod	k1gInSc7	důvod
svolání	svolání	k1gNnSc2	svolání
i	i	k8xC	i
předešlého	předešlý	k2eAgNnSc2d1	předešlé
<g/>
,	,	kIx,	,
Pátého	pátý	k4xOgInSc2	pátý
lateránského	lateránský	k2eAgInSc2d1	lateránský
koncilu	koncil	k1gInSc2	koncil
<g/>
,	,	kIx,	,
kterému	který	k3yIgInSc3	který
předsedali	předsedat	k5eAaImAgMnP	předsedat
Julius	Julius	k1gMnSc1	Julius
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Lev	Lev	k1gMnSc1	Lev
X.	X.	kA	X.
<g/>
,	,	kIx,	,
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
úspěchu	úspěch	k1gInSc2	úspěch
reformace	reformace	k1gFnSc2	reformace
<g/>
.	.	kIx.	.
</s>
<s>
Koncil	koncil	k1gInSc1	koncil
zasedal	zasedat	k5eAaImAgInS	zasedat
v	v	k7c6	v
25	[number]	k4	25
zasedáních	zasedání	k1gNnPc6	zasedání
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
celá	celý	k2eAgFnSc1d1	celá
polovina	polovina	k1gFnSc1	polovina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
věnována	věnovat	k5eAaPmNgFnS	věnovat
slavnostním	slavnostní	k2eAgNnSc7d1	slavnostní
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
část	část	k1gFnSc1	část
práce	práce	k1gFnSc2	práce
byla	být	k5eAaImAgFnS	být
vykonána	vykonat	k5eAaPmNgFnS	vykonat
ve	v	k7c6	v
výborech	výbor	k1gInPc6	výbor
a	a	k8xC	a
kongregacích	kongregace	k1gFnPc6	kongregace
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
práci	práce	k1gFnSc4	práce
koordinoval	koordinovat	k5eAaBmAgInS	koordinovat
papežský	papežský	k2eAgInSc1d1	papežský
legát	legát	k1gInSc1	legát
<g/>
.	.	kIx.	.
</s>
<s>
Koncil	koncil	k1gInSc1	koncil
učinil	učinit	k5eAaPmAgInS	učinit
přítrž	přítrž	k1gFnSc4	přítrž
zneužívání	zneužívání	k1gNnSc2	zneužívání
církevní	církevní	k2eAgFnSc2d1	církevní
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
zavedl	zavést	k5eAaPmAgInS	zavést
disciplinární	disciplinární	k2eAgFnSc4d1	disciplinární
reformu	reforma	k1gFnSc4	reforma
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
i	i	k9	i
odpustky	odpustek	k1gInPc7	odpustek
<g/>
,	,	kIx,	,
stav	stav	k1gInSc1	stav
řádů	řád	k1gInPc2	řád
<g/>
,	,	kIx,	,
vzdělání	vzdělání	k1gNnSc1	vzdělání
kléru	klér	k1gInSc2	klér
<g/>
,	,	kIx,	,
rezidence	rezidence	k1gFnSc2	rezidence
biskupů	biskup	k1gMnPc2	biskup
a	a	k8xC	a
zákaz	zákaz	k1gInSc1	zákaz
soubojů	souboj	k1gInPc2	souboj
<g/>
.	.	kIx.	.
</s>
<s>
Doktrinální	doktrinální	k2eAgNnSc4d1	doktrinální
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
koncilu	koncil	k1gInSc2	koncil
jsou	být	k5eAaImIp3nP	být
rozdělena	rozdělit	k5eAaPmNgNnP	rozdělit
do	do	k7c2	do
dekretů	dekret	k1gInPc2	dekret
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
pozitivní	pozitivní	k2eAgNnSc4d1	pozitivní
vyjádření	vyjádření	k1gNnSc4	vyjádření
dogmat	dogma	k1gNnPc2	dogma
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
kratší	krátký	k2eAgInPc1d2	kratší
kánony	kánon	k1gInPc1	kánon
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zavrhují	zavrhovat	k5eAaImIp3nP	zavrhovat
(	(	kIx(	(
<g/>
anathematizují	anathematizovat	k5eAaBmIp3nP	anathematizovat
<g/>
)	)	kIx)	)
protestantský	protestantský	k2eAgInSc1d1	protestantský
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
sporné	sporný	k2eAgFnPc4d1	sporná
otázky	otázka	k1gFnPc4	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
vyjádření	vyjádření	k1gNnPc1	vyjádření
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
strohá	strohý	k2eAgNnPc4d1	strohé
a	a	k8xC	a
přesná	přesný	k2eAgNnPc4d1	přesné
<g/>
.	.	kIx.	.
</s>
<s>
Protestantské	protestantský	k2eAgFnPc4d1	protestantská
pozice	pozice	k1gFnPc4	pozice
však	však	k9	však
často	často	k6eAd1	často
dokumenty	dokument	k1gInPc1	dokument
zachycují	zachycovat	k5eAaImIp3nP	zachycovat
ve	v	k7c6	v
vyhrocené	vyhrocený	k2eAgFnSc6d1	vyhrocená
podobě	podoba	k1gFnSc6	podoba
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
směšovány	směšován	k2eAgInPc1d1	směšován
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
herezemi	hereze	k1gFnPc7	hereze
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zavrhují	zavrhovat	k5eAaImIp3nP	zavrhovat
i	i	k9	i
protestanti	protestant	k1gMnPc1	protestant
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
znovustvrzení	znovustvrzení	k1gNnSc6	znovustvrzení
Nicejsko-konstantinopolského	Nicejskoonstantinopolský	k2eAgNnSc2d1	Nicejsko-konstantinopolské
vyznání	vyznání	k1gNnSc2	vyznání
víry	víra	k1gFnSc2	víra
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
zasedání	zasedání	k1gNnSc6	zasedání
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
dekret	dekret	k1gInSc1	dekret
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
zasedání	zasedání	k1gNnSc2	zasedání
<g/>
)	)	kIx)	)
stvrzující	stvrzující	k2eAgFnSc1d1	stvrzující
<g/>
,	,	kIx,	,
že	že	k8xS	že
deuterokanonické	deuterokanonický	k2eAgFnPc1d1	deuterokanonická
knihy	kniha	k1gFnPc1	kniha
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
úrovni	úroveň	k1gFnSc6	úroveň
jako	jako	k9	jako
ostatní	ostatní	k2eAgFnPc1d1	ostatní
knihy	kniha	k1gFnPc1	kniha
biblického	biblický	k2eAgInSc2d1	biblický
kánonu	kánon	k1gInSc2	kánon
(	(	kIx(	(
<g/>
proti	proti	k7c3	proti
Lutherovu	Lutherův	k2eAgNnSc3d1	Lutherovo
zúžení	zúžení	k1gNnSc3	zúžení
biblického	biblický	k2eAgInSc2d1	biblický
kánonu	kánon	k1gInSc2	kánon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tentýž	týž	k3xTgInSc4	týž
dekret	dekret	k1gInSc4	dekret
staví	stavit	k5eAaPmIp3nS	stavit
do	do	k7c2	do
vztahu	vztah	k1gInSc2	vztah
k	k	k7c3	k
Písmu	písmo	k1gNnSc3	písmo
tradici	tradice	k1gFnSc4	tradice
jako	jako	k8xC	jako
měřítko	měřítko	k1gNnSc4	měřítko
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Latinská	latinský	k2eAgFnSc1d1	Latinská
Vulgáta	Vulgáta	k1gFnSc1	Vulgáta
je	být	k5eAaImIp3nS	být
označena	označit	k5eAaPmNgFnS	označit
za	za	k7c4	za
autoritativní	autoritativní	k2eAgInSc4d1	autoritativní
text	text	k1gInSc4	text
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
teologii	teologie	k1gFnSc6	teologie
<g/>
.	.	kIx.	.
</s>
<s>
Ospravedlnění	ospravedlnění	k1gNnSc1	ospravedlnění
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
zasedání	zasedání	k1gNnSc6	zasedání
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
koncilu	koncil	k1gInSc2	koncil
je	být	k5eAaImIp3nS	být
nabídnuto	nabídnout	k5eAaPmNgNnS	nabídnout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
dobrých	dobrý	k2eAgInPc2d1	dobrý
skutků	skutek	k1gInPc2	skutek
proti	proti	k7c3	proti
luteránské	luteránský	k2eAgFnSc3d1	luteránská
nauce	nauka	k1gFnSc3	nauka
"	"	kIx"	"
<g/>
sola	sola	k6eAd1	sola
fide	fidat	k5eAaPmIp3nS	fidat
<g/>
"	"	kIx"	"
a	a	k8xC	a
popisuje	popisovat	k5eAaImIp3nS	popisovat
víru	víra	k1gFnSc4	víra
jako	jako	k8xS	jako
postoj	postoj	k1gInSc4	postoj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
života	život	k1gInSc2	život
člověka	člověk	k1gMnSc2	člověk
postupně	postupně	k6eAd1	postupně
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
odmítnuta	odmítnout	k5eAaPmNgFnS	odmítnout
také	také	k9	také
teze	teze	k1gFnSc1	teze
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
milosti	milost	k1gFnSc2	milost
zcela	zcela	k6eAd1	zcela
pasivní	pasivní	k2eAgInSc1d1	pasivní
a	a	k8xC	a
nečinný	činný	k2eNgInSc1d1	nečinný
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
péči	péče	k1gFnSc4	péče
věnuje	věnovat	k5eAaImIp3nS	věnovat
koncil	koncil	k1gInSc1	koncil
svátostem	svátost	k1gFnPc3	svátost
<g/>
.	.	kIx.	.
</s>
<s>
Koncil	koncil	k1gInSc1	koncil
stvrzuje	stvrzovat	k5eAaImIp3nS	stvrzovat
svátostnou	svátostný	k2eAgFnSc4d1	svátostná
povahu	povaha	k1gFnSc4	povaha
sedmi	sedm	k4xCc2	sedm
svátostí	svátost	k1gFnPc2	svátost
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
eucharistii	eucharistie	k1gFnSc4	eucharistie
za	za	k7c4	za
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
skutečné	skutečný	k2eAgFnSc6d1	skutečná
Kristově	Kristův	k2eAgFnSc6d1	Kristova
oběti	oběť	k1gFnSc6	oběť
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
chleba	chléb	k1gInSc2	chléb
a	a	k8xC	a
víno	víno	k1gNnSc4	víno
proměněno	proměněn	k2eAgNnSc4d1	proměněno
v	v	k7c4	v
tělo	tělo	k1gNnSc4	tělo
a	a	k8xC	a
krev	krev	k1gFnSc4	krev
Krista	Kristus	k1gMnSc2	Kristus
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
22	[number]	k4	22
<g/>
.	.	kIx.	.
zasedání	zasedání	k1gNnSc6	zasedání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Koncil	koncil	k1gInSc1	koncil
užívá	užívat	k5eAaImIp3nS	užívat
pojmu	pojem	k1gInSc3	pojem
transsubstanciace	transsubstanciace	k1gFnSc2	transsubstanciace
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
aristotelské	aristotelský	k2eAgFnSc2d1	aristotelská
hylemorfické	hylemorfický	k2eAgFnSc2d1	hylemorfický
teorie	teorie	k1gFnSc2	teorie
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
dogmatickou	dogmatický	k2eAgFnSc4d1	dogmatická
závaznost	závaznost	k1gFnSc4	závaznost
<g/>
.	.	kIx.	.
</s>
<s>
Dekret	dekret	k1gInSc1	dekret
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kristus	Kristus	k1gMnSc1	Kristus
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
vere	vere	k1gFnSc1	vere
<g/>
,	,	kIx,	,
realiter	realiter	k1gMnSc1	realiter
<g/>
,	,	kIx,	,
substantialiter	substantialiter	k1gMnSc1	substantialiter
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
opravdu	opravdu	k6eAd1	opravdu
<g/>
,	,	kIx,	,
skutečně	skutečně	k6eAd1	skutečně
<g/>
,	,	kIx,	,
podstatně	podstatně	k6eAd1	podstatně
<g/>
)	)	kIx)	)
přítomen	přítomen	k2eAgInSc1d1	přítomen
v	v	k7c6	v
konsekrovaných	konsekrovaný	k2eAgFnPc6d1	konsekrovaná
způsobách	způsoba	k1gFnPc6	způsoba
<g/>
.	.	kIx.	.
</s>
<s>
Kristova	Kristův	k2eAgFnSc1d1	Kristova
oběť	oběť	k1gFnSc1	oběť
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
při	při	k7c6	při
mši	mše	k1gFnSc6	mše
vztažena	vztáhnout	k5eAaPmNgFnS	vztáhnout
na	na	k7c4	na
živé	živý	k2eAgMnPc4d1	živý
i	i	k9	i
zemřelé	zemřelý	k2eAgNnSc1d1	zemřelé
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
zpřítomňování	zpřítomňování	k1gNnSc1	zpřítomňování
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Ježíšova	Ježíšův	k2eAgInSc2d1	Ježíšův
příkazu	příkaz	k1gInSc2	příkaz
apoštolům	apoštol	k1gMnPc3	apoštol
"	"	kIx"	"
<g/>
To	to	k9	to
čiňte	činit	k5eAaImRp2nP	činit
na	na	k7c4	na
mou	můj	k3xOp1gFnSc4	můj
památku	památka	k1gFnSc4	památka
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
čímž	což	k3yRnSc7	což
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
zároveň	zároveň	k6eAd1	zároveň
svátost	svátost	k1gFnSc4	svátost
kněžství	kněžství	k1gNnSc2	kněžství
<g/>
.	.	kIx.	.
</s>
<s>
Koncil	koncil	k1gInSc1	koncil
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
praxi	praxe	k1gFnSc4	praxe
přijímání	přijímání	k1gNnSc2	přijímání
pod	pod	k7c7	pod
jednou	jeden	k4xCgFnSc7	jeden
způsobou	způsoba	k1gFnSc7	způsoba
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
zasedání	zasedání	k1gNnSc2	zasedání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
s	s	k7c7	s
určitými	určitý	k2eAgFnPc7d1	určitá
regionálními	regionální	k2eAgFnPc7d1	regionální
výjimkami	výjimka	k1gFnPc7	výjimka
<g/>
,	,	kIx,	,
a	a	k8xC	a
předal	předat	k5eAaPmAgMnS	předat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
otázce	otázka	k1gFnSc6	otázka
poslední	poslední	k2eAgNnSc4d1	poslední
slovo	slovo	k1gNnSc4	slovo
papeži	papež	k1gMnSc3	papež
<g/>
.	.	kIx.	.
</s>
<s>
Svěcení	svěcení	k1gNnSc1	svěcení
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
zasedání	zasedání	k1gNnSc6	zasedání
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
koncilu	koncil	k1gInSc2	koncil
a	a	k8xC	a
učení	učení	k1gNnSc2	učení
církve	církev	k1gFnSc2	církev
vtiskuje	vtiskovat	k5eAaImIp3nS	vtiskovat
nezrušitelné	zrušitelný	k2eNgNnSc1d1	nezrušitelné
znamení	znamení	k1gNnSc1	znamení
(	(	kIx(	(
<g/>
character	character	k1gInSc1	character
<g/>
)	)	kIx)	)
do	do	k7c2	do
duše	duše	k1gFnSc2	duše
<g/>
.	.	kIx.	.
</s>
<s>
Kněžství	kněžství	k1gNnSc1	kněžství
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
kněžství	kněžství	k1gNnSc1	kněžství
starozákonní	starozákonní	k2eAgNnSc1d1	starozákonní
<g/>
.	.	kIx.	.
</s>
<s>
Souhlas	souhlas	k1gInSc1	souhlas
Božího	boží	k2eAgInSc2d1	boží
lidu	lid	k1gInSc2	lid
není	být	k5eNaImIp3nS	být
nezbytný	zbytný	k2eNgInSc1d1	zbytný
k	k	k7c3	k
platnosti	platnost	k1gFnSc3	platnost
svátostí	svátost	k1gFnPc2	svátost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dekretu	dekret	k1gInSc6	dekret
o	o	k7c6	o
manželství	manželství	k1gNnSc6	manželství
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
zasedání	zasedání	k1gNnSc2	zasedání
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
hodnota	hodnota	k1gFnSc1	hodnota
celibátního	celibátní	k2eAgInSc2d1	celibátní
způsobu	způsob	k1gInSc2	způsob
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
zavrhuje	zavrhovat	k5eAaImIp3nS	zavrhovat
se	se	k3xPyFc4	se
konkubinát	konkubinát	k1gInSc1	konkubinát
<g/>
.	.	kIx.	.
</s>
<s>
Platnost	platnost	k1gFnSc1	platnost
manželství	manželství	k1gNnSc2	manželství
je	být	k5eAaImIp3nS	být
církevně-právně	církevněrávně	k6eAd1	církevně-právně
vázána	vázat	k5eAaImNgFnS	vázat
na	na	k7c4	na
výměnu	výměna	k1gFnSc4	výměna
souhlasu	souhlas	k1gInSc2	souhlas
před	před	k7c7	před
knězem	kněz	k1gMnSc7	kněz
a	a	k8xC	a
dvěma	dva	k4xCgInPc7	dva
svědky	svědek	k1gMnPc7	svědek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
rozvodu	rozvod	k1gInSc2	rozvod
není	být	k5eNaImIp3nS	být
nevinné	vinný	k2eNgFnSc3d1	nevinná
straně	strana	k1gFnSc3	strana
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
sezdat	sezdat	k5eAaPmF	sezdat
se	se	k3xPyFc4	se
za	za	k7c2	za
života	život	k1gInSc2	život
manžela	manžel	k1gMnSc2	manžel
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
ten	ten	k3xDgMnSc1	ten
dopustil	dopustit	k5eAaPmAgMnS	dopustit
cizoložství	cizoložství	k1gNnSc4	cizoložství
<g/>
.	.	kIx.	.
</s>
<s>
Koncil	koncil	k1gInSc1	koncil
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
i	i	k9	i
dalšími	další	k2eAgFnPc7d1	další
konkrétními	konkrétní	k2eAgFnPc7d1	konkrétní
praxemi	praxe	k1gFnPc7	praxe
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
<g/>
,	,	kIx,	,
naukou	nauka	k1gFnSc7	nauka
o	o	k7c6	o
očistci	očistec	k1gInSc6	očistec
<g/>
,	,	kIx,	,
svatých	svatá	k1gFnPc6	svatá
<g/>
,	,	kIx,	,
uctívání	uctívání	k1gNnSc6	uctívání
ostatků	ostatek	k1gInPc2	ostatek
<g/>
,	,	kIx,	,
o	o	k7c6	o
odpustcích	odpustek	k1gInPc6	odpustek
<g/>
.	.	kIx.	.
</s>
<s>
Koncil	koncil	k1gInSc1	koncil
dává	dávat	k5eAaImIp3nS	dávat
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
zvlášť	zvlášť	k6eAd1	zvlášť
zřízené	zřízený	k2eAgFnSc3d1	zřízená
komisi	komise	k1gFnSc3	komise
připravit	připravit	k5eAaPmF	připravit
seznam	seznam	k1gInSc4	seznam
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
knih	kniha	k1gFnPc2	kniha
(	(	kIx(	(
<g/>
Index	index	k1gInSc1	index
Librorum	Librorum	k1gNnSc1	Librorum
Prohibitorum	Prohibitorum	k1gInSc1	Prohibitorum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
tato	tento	k3xDgFnSc1	tento
komise	komise	k1gFnSc1	komise
index	index	k1gInSc4	index
svěřuje	svěřovat	k5eAaImIp3nS	svěřovat
do	do	k7c2	do
správy	správa	k1gFnSc2	správa
papeži	papež	k1gMnSc3	papež
<g/>
.	.	kIx.	.
</s>
<s>
Papeži	Papež	k1gMnSc3	Papež
je	být	k5eAaImIp3nS	být
postoupena	postoupen	k2eAgFnSc1d1	postoupena
i	i	k8xC	i
příprava	příprava	k1gFnSc1	příprava
katechismu	katechismus	k1gInSc2	katechismus
a	a	k8xC	a
nové	nový	k2eAgNnSc4d1	nové
vydání	vydání	k1gNnSc4	vydání
breviáře	breviář	k1gInSc2	breviář
a	a	k8xC	a
misálu	misál	k1gInSc2	misál
<g/>
.	.	kIx.	.
</s>
<s>
Ustanovení	ustanovení	k1gNnSc1	ustanovení
koncilu	koncil	k1gInSc2	koncil
schválil	schválit	k5eAaPmAgInS	schválit
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1564	[number]	k4	1564
papež	papež	k1gMnSc1	papež
Pius	Pius	k1gMnSc1	Pius
IV	IV	kA	IV
<g/>
.	.	kIx.	.
bulou	bula	k1gFnSc7	bula
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zavazuje	zavazovat	k5eAaImIp3nS	zavazovat
všechny	všechen	k3xTgInPc4	všechen
členy	člen	k1gInPc4	člen
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
uposlechnout	uposlechnout	k5eAaPmF	uposlechnout
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
koncilu	koncil	k1gInSc2	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svolání	svolání	k1gNnSc6	svolání
koncilu	koncil	k1gInSc2	koncil
za	za	k7c2	za
podmínek	podmínka	k1gFnPc2	podmínka
nevýhodných	výhodný	k2eNgInPc2d1	nevýhodný
pro	pro	k7c4	pro
protestanty	protestant	k1gMnPc4	protestant
reagoval	reagovat	k5eAaBmAgMnS	reagovat
již	již	k6eAd1	již
r.	r.	kA	r.
1545	[number]	k4	1545
Martin	Martina	k1gFnPc2	Martina
Luther	Luthra	k1gFnPc2	Luthra
pamfletem	pamflet	k1gInSc7	pamflet
Wider	Wider	k1gMnSc1	Wider
das	das	k?	das
Papsttum	Papsttum	k1gNnSc4	Papsttum
zu	zu	k?	zu
Rom	Rom	k1gMnSc1	Rom
<g/>
,	,	kIx,	,
vom	vom	k?	vom
Teufel	Teufel	k1gMnSc1	Teufel
gestiftet	gestiftet	k1gMnSc1	gestiftet
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Proti	proti	k7c3	proti
římskému	římský	k2eAgNnSc3d1	římské
papežství	papežství	k1gNnSc3	papežství
založenému	založený	k2eAgInSc3d1	založený
od	od	k7c2	od
ďábla	ďábel	k1gMnSc2	ďábel
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Soustavný	soustavný	k2eAgInSc1d1	soustavný
kritický	kritický	k2eAgInSc1d1	kritický
rozbor	rozbor	k1gInSc1	rozbor
dekretů	dekret	k1gInPc2	dekret
Tridentského	tridentský	k2eAgInSc2d1	tridentský
koncilu	koncil	k1gInSc2	koncil
z	z	k7c2	z
luteránských	luteránský	k2eAgFnPc2d1	luteránská
pozic	pozice	k1gFnPc2	pozice
podal	podat	k5eAaPmAgInS	podat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1566-72	[number]	k4	1566-72
teolog	teolog	k1gMnSc1	teolog
Martin	Martin	k1gMnSc1	Martin
Chemnitz	Chemnitz	k1gMnSc1	Chemnitz
ve	v	k7c6	v
čtyřsvazkovém	čtyřsvazkový	k2eAgNnSc6d1	čtyřsvazkové
díle	dílo	k1gNnSc6	dílo
Examen	examen	k1gInSc1	examen
Concilii	Concilie	k1gFnSc3	Concilie
Tridentini	Tridentin	k2eAgMnPc1d1	Tridentin
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Zkoumání	zkoumání	k1gNnSc1	zkoumání
Tridentského	tridentský	k2eAgInSc2d1	tridentský
sněmu	sněm	k1gInSc2	sněm
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Concilium	Concilium	k1gNnSc1	Concilium
Tridentinum	Tridentinum	k1gInSc1	Tridentinum
<g/>
.	.	kIx.	.
</s>
<s>
Diariorum	Diariorum	k1gNnSc1	Diariorum
<g/>
,	,	kIx,	,
actorum	actorum	k1gNnSc1	actorum
<g/>
,	,	kIx,	,
epistularum	epistularum	k1gNnSc1	epistularum
<g/>
,	,	kIx,	,
tractatuum	tractatuum	k1gInSc1	tractatuum
nova	nova	k1gFnSc1	nova
collectio	collectio	k1gMnSc1	collectio
<g/>
,	,	kIx,	,
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Görres-Gesellschaft	Görres-Gesellschaft	k1gMnSc1	Görres-Gesellschaft
<g/>
,	,	kIx,	,
21	[number]	k4	21
sv.	sv.	kA	sv.
<g/>
,	,	kIx,	,
Freiburg	Freiburg	k1gInSc1	Freiburg
1901	[number]	k4	1901
<g/>
-	-	kIx~	-
<g/>
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Šusta	Šusta	k1gMnSc1	Šusta
Josef	Josef	k1gMnSc1	Josef
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Die	Die	k1gMnSc1	Die
Römische	Römisch	k1gFnSc2	Römisch
Curie	Curie	k1gMnSc1	Curie
und	und	k?	und
das	das	k?	das
Konzil	Konzil	k1gFnSc1	Konzil
von	von	k1gInSc1	von
Trient	Trient	k1gMnSc1	Trient
unter	unter	k1gMnSc1	unter
Pius	Pius	k1gMnSc1	Pius
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Actenstücke	Actenstücke	k1gFnSc1	Actenstücke
zur	zur	k?	zur
Geschichte	Geschicht	k1gInSc5	Geschicht
des	des	k1gNnSc7	des
Conzils	Conzils	k1gInSc1	Conzils
von	von	k1gInSc1	von
Trient	Trient	k1gInSc1	Trient
<g/>
,	,	kIx,	,
I-IV	I-IV	k1gMnSc1	I-IV
<g/>
,	,	kIx,	,
Wien	Wien	k1gMnSc1	Wien
<g/>
,	,	kIx,	,
Alfred	Alfred	k1gMnSc1	Alfred
Hölder	Hölder	k1gMnSc1	Hölder
1904	[number]	k4	1904
<g/>
-	-	kIx~	-
<g/>
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
</s>
<s>
Georg	Georg	k1gMnSc1	Georg
Schreiber	Schreiber	k1gMnSc1	Schreiber
<g/>
:	:	kIx,	:
Das	Das	k1gMnSc1	Das
Weltkonzil	Weltkonzil	k1gFnSc2	Weltkonzil
von	von	k1gInSc1	von
Trient	Trient	k1gInSc1	Trient
<g/>
.	.	kIx.	.
</s>
<s>
Sein	Seina	k1gFnPc2	Seina
Werden	Werdna	k1gFnPc2	Werdna
und	und	k?	und
Wirken	Wirkna	k1gFnPc2	Wirkna
<g/>
.	.	kIx.	.
2	[number]	k4	2
Bände	Bänd	k1gInSc5	Bänd
<g/>
.	.	kIx.	.
</s>
<s>
Freiburg	Freiburg	k1gInSc1	Freiburg
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
<s>
Hubert	Hubert	k1gMnSc1	Hubert
Jedin	Jedin	k1gMnSc1	Jedin
<g/>
:	:	kIx,	:
Geschichte	Geschicht	k1gInSc5	Geschicht
des	des	k1gNnSc7	des
Konzils	Konzils	k1gInSc1	Konzils
von	von	k1gInSc1	von
Trient	Trient	k1gInSc1	Trient
<g/>
.	.	kIx.	.
4	[number]	k4	4
Bände	Bänd	k1gInSc5	Bänd
<g/>
.	.	kIx.	.
</s>
<s>
Freiburg	Freiburg	k1gInSc1	Freiburg
im	im	k?	im
Breisgau	Breisgaus	k1gInSc2	Breisgaus
1949	[number]	k4	1949
<g/>
-	-	kIx~	-
<g/>
1975	[number]	k4	1975
(	(	kIx(	(
<g/>
I	I	kA	I
<g/>
:	:	kIx,	:
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
:	:	kIx,	:
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
:	:	kIx,	:
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
IV	IV	kA	IV
<g/>
:	:	kIx,	:
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Remigius	Remigius	k1gMnSc1	Remigius
Bäumer	Bäumer	k1gMnSc1	Bäumer
(	(	kIx(	(
<g/>
Hrsg	Hrsg	k1gMnSc1	Hrsg
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Concilium	Concilium	k1gNnSc1	Concilium
Tridentinum	Tridentinum	k1gNnSc1	Tridentinum
<g/>
.	.	kIx.	.
</s>
<s>
Wege	Wege	k6eAd1	Wege
der	drát	k5eAaImRp2nS	drát
Forschung	Forschung	k1gInSc4	Forschung
313	[number]	k4	313
<g/>
.	.	kIx.	.
</s>
<s>
Darmstadt	Darmstadt	k1gInSc1	Darmstadt
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Paolo	Paolo	k1gNnSc1	Paolo
Prodi	Prod	k1gMnPc1	Prod
(	(	kIx(	(
<g/>
Hrsg	Hrsg	k1gMnSc1	Hrsg
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Das	Das	k1gMnSc1	Das
Konzil	Konzil	k1gFnSc2	Konzil
von	von	k1gInSc1	von
Trient	Trient	k1gMnSc1	Trient
und	und	k?	und
die	die	k?	die
Moderne	Modern	k1gInSc5	Modern
<g/>
.	.	kIx.	.
</s>
<s>
Schriften	Schriften	k2eAgInSc1d1	Schriften
des	des	k1gNnSc4	des
Italienisch-Deutschen	Italienisch-Deutschen	k2eAgInSc1d1	Italienisch-Deutschen
Historischen	Historischen	k2eAgInSc1d1	Historischen
Instituts	Instituts	k1gInSc1	Instituts
in	in	k?	in
Trient	Trient	k1gInSc1	Trient
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
Berlin	berlina	k1gFnPc2	berlina
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Kavka	Kavka	k1gMnSc1	Kavka
František	František	k1gMnSc1	František
-	-	kIx~	-
Skýbová	Skýbový	k2eAgFnSc1d1	Skýbový
Anna	Anna	k1gFnSc1	Anna
<g/>
,	,	kIx,	,
Husitský	husitský	k2eAgInSc1d1	husitský
epilog	epilog	k1gInSc1	epilog
na	na	k7c6	na
koncilu	koncil	k1gInSc6	koncil
Tridentském	tridentský	k2eAgInSc6d1	tridentský
a	a	k8xC	a
původní	původní	k2eAgFnSc2d1	původní
koncepce	koncepce	k1gFnSc2	koncepce
habsburské	habsburský	k2eAgFnSc2d1	habsburská
rekatolizace	rekatolizace	k1gFnSc2	rekatolizace
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc4	počátek
obnoveného	obnovený	k2eAgNnSc2d1	obnovené
pražského	pražský	k2eAgNnSc2d1	Pražské
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
1561	[number]	k4	1561
<g/>
-	-	kIx~	-
<g/>
1580	[number]	k4	1580
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
UK	UK	kA	UK
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Šusta	Šusta	k1gMnSc1	Šusta
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
Pius	Pius	k1gMnSc1	Pius
IV	IV	kA	IV
<g/>
.	.	kIx.	.
před	před	k7c7	před
pontifikátem	pontifikát	k1gInSc7	pontifikát
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
pontifikátu	pontifikát	k1gInSc2	pontifikát
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
koncilů	koncil	k1gInPc2	koncil
a	a	k8xC	a
synod	synod	k1gInSc4	synod
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tridentský	tridentský	k2eAgInSc4d1	tridentský
koncil	koncil	k1gInSc4	koncil
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Tridentský	tridentský	k2eAgInSc4d1	tridentský
koncil	koncil	k1gInSc4	koncil
ve	v	k7c6	v
Vlastenském	vlastenský	k2eAgInSc6d1	vlastenský
slovníku	slovník	k1gInSc6	slovník
historickém	historický	k2eAgInSc6d1	historický
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
