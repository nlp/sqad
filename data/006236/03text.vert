<s>
Veveří	veveří	k2eAgFnSc1d1	veveří
je	být	k5eAaImIp3nS	být
městská	městský	k2eAgFnSc1d1	městská
čtvrť	čtvrť	k1gFnSc1	čtvrť
severně	severně	k6eAd1	severně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
1,98	[number]	k4	1,98
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
Veveří	veveří	k2eAgNnSc4d1	veveří
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
Brnu	Brno	k1gNnSc6	Brno
připojeno	připojit	k5eAaPmNgNnS	připojit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
<g/>
.	.	kIx.	.
</s>
<s>
Veveří	veveří	k2eAgNnSc1d1	veveří
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
součástí	součást	k1gFnSc7	součást
samosprávné	samosprávný	k2eAgFnSc2d1	samosprávná
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-střed	Brnotřed	k1gMnSc1	Brno-střed
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přes	přes	k7c4	přes
19	[number]	k4	19
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrť	čtvrť	k1gFnSc1	čtvrť
sousedí	sousedit	k5eAaImIp3nS	sousedit
přímo	přímo	k6eAd1	přímo
s	s	k7c7	s
historickým	historický	k2eAgNnSc7d1	historické
jádrem	jádro	k1gNnSc7	jádro
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
proto	proto	k8xC	proto
má	mít	k5eAaImIp3nS	mít
výrazně	výrazně	k6eAd1	výrazně
městský	městský	k2eAgInSc1d1	městský
charakter	charakter	k1gInSc1	charakter
s	s	k7c7	s
několika	několik	k4yIc7	několik
dopravně	dopravně	k6eAd1	dopravně
vysoce	vysoce	k6eAd1	vysoce
vytíženými	vytížený	k2eAgFnPc7d1	vytížená
ulicemi	ulice	k1gFnPc7	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Zástavbu	zástavba	k1gFnSc4	zástavba
čtvrtě	čtvrt	k1gFnSc2	čtvrt
tvoří	tvořit	k5eAaImIp3nP	tvořit
mnohapatrové	mnohapatrový	k2eAgInPc1d1	mnohapatrový
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
historické	historický	k2eAgInPc4d1	historický
domy	dům	k1gInPc4	dům
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
reprezentativních	reprezentativní	k2eAgInPc2d1	reprezentativní
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
honosné	honosný	k2eAgInPc1d1	honosný
secesní	secesní	k2eAgInPc1d1	secesní
nájemní	nájemní	k2eAgInPc1d1	nájemní
domy	dům	k1gInPc1	dům
na	na	k7c6	na
Konečného	Konečného	k2eAgNnSc6d1	Konečného
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
dominuje	dominovat	k5eAaImIp3nS	dominovat
domovní	domovní	k2eAgInSc4d1	domovní
blok	blok	k1gInSc4	blok
Tivoli	Tivole	k1gFnSc4	Tivole
na	na	k7c6	na
nároží	nároží	k1gNnSc6	nároží
Konečného	Konečného	k2eAgNnSc2d1	Konečného
náměstí	náměstí	k1gNnSc2	náměstí
a	a	k8xC	a
Jiráskovy	Jiráskův	k2eAgFnSc2d1	Jiráskova
ulice	ulice	k1gFnSc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Dominantou	dominanta	k1gFnSc7	dominanta
celé	celý	k2eAgFnSc2d1	celá
čtvrti	čtvrt	k1gFnSc2	čtvrt
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Brněnská	brněnský	k2eAgNnPc4d1	brněnské
Trojčata	trojče	k1gNnPc4	trojče
<g/>
"	"	kIx"	"
na	na	k7c6	na
Šumavské	šumavský	k2eAgFnSc6d1	Šumavská
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
patří	patřit	k5eAaImIp3nS	patřit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k8xC	i
Kraví	kraví	k2eAgFnSc1d1	kraví
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
Hvězdárna	hvězdárna	k1gFnSc1	hvězdárna
a	a	k8xC	a
planetárium	planetárium	k1gNnSc1	planetárium
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Středem	střed	k1gInSc7	střed
Veveří	veveří	k2eAgFnSc1d1	veveří
prochází	procházet	k5eAaImIp3nS	procházet
třída	třída	k1gFnSc1	třída
Veveří	veveří	k2eAgFnSc1d1	veveří
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
získala	získat	k5eAaPmAgFnS	získat
čtvrť	čtvrť	k1gFnSc1	čtvrť
název	název	k1gInSc1	název
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
třídou	třída	k1gFnSc7	třída
je	být	k5eAaImIp3nS	být
ulice	ulice	k1gFnSc1	ulice
Kounicova	Kounicův	k2eAgFnSc1d1	Kounicova
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
lemují	lemovat	k5eAaImIp3nP	lemovat
významné	významný	k2eAgFnPc1d1	významná
městské	městský	k2eAgFnPc1d1	městská
budovy	budova	k1gFnPc1	budova
(	(	kIx(	(
<g/>
střední	střední	k2eAgFnPc1d1	střední
i	i	k8xC	i
vysoké	vysoký	k2eAgFnPc1d1	vysoká
školy	škola	k1gFnPc1	škola
<g/>
,	,	kIx,	,
areál	areál	k1gInSc1	areál
Sokola	Sokol	k1gMnSc4	Sokol
<g/>
,	,	kIx,	,
pošta	pošta	k1gFnSc1	pošta
<g/>
,	,	kIx,	,
hotely	hotel	k1gInPc1	hotel
<g/>
,	,	kIx,	,
finanční	finanční	k2eAgInPc1d1	finanční
ústavy	ústav	k1gInPc1	ústav
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
významnou	významný	k2eAgFnSc7d1	významná
třídou	třída	k1gFnSc7	třída
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Černým	Černý	k1gMnSc7	Černý
Polem	polem	k6eAd1	polem
Lidická	lidický	k2eAgFnSc1d1	Lidická
a	a	k8xC	a
s	s	k7c7	s
Ponavou	Ponava	k1gFnSc7	Ponava
Štefánikova	Štefánikův	k2eAgNnSc2d1	Štefánikovo
<g/>
.	.	kIx.	.
</s>
<s>
Dopravní	dopravní	k2eAgInPc1d1	dopravní
uzly	uzel	k1gInPc1	uzel
Konečného	Konečného	k2eAgNnSc2d1	Konečného
náměstí	náměstí	k1gNnSc2	náměstí
a	a	k8xC	a
Pionýrská	pionýrský	k2eAgFnSc1d1	Pionýrská
spojuje	spojovat	k5eAaImIp3nS	spojovat
ulice	ulice	k1gFnSc1	ulice
Kotlářská	kotlářský	k2eAgFnSc1d1	Kotlářská
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
významnými	významný	k2eAgFnPc7d1	významná
ulicemi	ulice	k1gFnPc7	ulice
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
Úvoz	úvoz	k1gInSc1	úvoz
nebo	nebo	k8xC	nebo
Údolní	údolní	k2eAgInSc1d1	údolní
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významná	významný	k2eAgNnPc4d1	významné
náměstí	náměstí	k1gNnPc4	náměstí
patří	patřit	k5eAaImIp3nS	patřit
Obilní	obilní	k2eAgInSc1d1	obilní
trh	trh	k1gInSc1	trh
<g/>
,	,	kIx,	,
Janáčkovo	Janáčkův	k2eAgNnSc1d1	Janáčkovo
náměstí	náměstí	k1gNnSc1	náměstí
či	či	k8xC	či
Konečného	Konečného	k2eAgNnSc1d1	Konečného
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
se	se	k3xPyFc4	se
na	na	k7c6	na
ulicích	ulice	k1gFnPc6	ulice
Veveří	veveří	k2eAgFnSc2d1	veveří
a	a	k8xC	a
Šumavská	šumavský	k2eAgFnSc1d1	Šumavská
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
několik	několik	k4yIc4	několik
budov	budova	k1gFnPc2	budova
Vysokého	vysoký	k2eAgNnSc2d1	vysoké
učení	učení	k1gNnSc2	učení
technického	technický	k2eAgNnSc2d1	technické
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
60	[number]	k4	60
<g/>
metrové	metrový	k2eAgFnSc2d1	metrová
výškové	výškový	k2eAgFnSc2d1	výšková
budovy	budova	k1gFnSc2	budova
Kancelářského	kancelářský	k2eAgInSc2d1	kancelářský
komplexu	komplex	k1gInSc2	komplex
Šumavská	šumavský	k2eAgNnPc1d1	Šumavské
tvoří	tvořit	k5eAaImIp3nP	tvořit
takřka	takřka	k6eAd1	takřka
dominantu	dominanta	k1gFnSc4	dominanta
čtvrti	čtvrt	k1gFnSc2	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sousední	sousední	k2eAgFnSc6d1	sousední
Akademické	akademický	k2eAgFnSc6d1	akademická
ulici	ulice	k1gFnSc6	ulice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
budova	budova	k1gFnSc1	budova
právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Kounicova	Kounicův	k2eAgFnSc1d1	Kounicova
Univerzita	univerzita	k1gFnSc1	univerzita
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
Ústav	ústav	k1gInSc1	ústav
operačně	operačně	k6eAd1	operačně
taktických	taktický	k2eAgFnPc2d1	taktická
studií	studie	k1gFnPc2	studie
a	a	k8xC	a
Moravská	moravský	k2eAgFnSc1d1	Moravská
zemská	zemský	k2eAgFnSc1d1	zemská
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
samé	samý	k3xTgFnSc6	samý
jižní	jižní	k2eAgFnSc6d1	jižní
hranici	hranice	k1gFnSc6	hranice
čtvrti	čtvrt	k1gFnSc2	čtvrt
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
Žerotínově	Žerotínův	k2eAgNnSc6d1	Žerotínovo
náměstí	náměstí	k1gNnSc6	náměstí
reprezentativní	reprezentativní	k2eAgFnSc1d1	reprezentativní
budova	budova	k1gFnSc1	budova
Nového	Nového	k2eAgInSc2d1	Nového
zemského	zemský	k2eAgInSc2d1	zemský
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
dnes	dnes	k6eAd1	dnes
sídlí	sídlet	k5eAaImIp3nS	sídlet
krajský	krajský	k2eAgInSc1d1	krajský
úřad	úřad	k1gInSc1	úřad
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
významnými	významný	k2eAgFnPc7d1	významná
stavbami	stavba	k1gFnPc7	stavba
je	být	k5eAaImIp3nS	být
hvězdárna	hvězdárna	k1gFnSc1	hvězdárna
a	a	k8xC	a
planetárium	planetárium	k1gNnSc1	planetárium
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
Kraví	kraví	k2eAgFnSc1d1	kraví
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
koupaliště	koupaliště	k1gNnSc4	koupaliště
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Obilním	obilní	k2eAgInSc6d1	obilní
trhu	trh	k1gInSc6	trh
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
porodnice	porodnice	k1gFnSc1	porodnice
Fakultní	fakultní	k2eAgFnSc2d1	fakultní
nemocnice	nemocnice	k1gFnSc2	nemocnice
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
území	území	k1gNnSc2	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
čtvrti	čtvrt	k1gFnSc2	čtvrt
Veveří	veveří	k2eAgFnSc2d1	veveří
bylo	být	k5eAaImAgNnS	být
připojeno	připojit	k5eAaPmNgNnS	připojit
k	k	k7c3	k
Brnu	Brno	k1gNnSc3	Brno
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1850	[number]	k4	1850
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
tehdejších	tehdejší	k2eAgNnPc2d1	tehdejší
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
Křížová	Křížová	k1gFnSc1	Křížová
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Nová	nový	k2eAgFnSc1d1	nová
Ulice	ulice	k1gFnSc1	ulice
<g/>
,	,	kIx,	,
Švábka	švábka	k1gFnSc1	švábka
a	a	k8xC	a
Malá	malý	k2eAgFnSc1d1	malá
Nová	nový	k2eAgFnSc1d1	nová
Ulice	ulice	k1gFnSc1	ulice
<g/>
;	;	kIx,	;
menší	malý	k2eAgFnSc1d2	menší
část	část	k1gFnSc1	část
území	území	k1gNnSc2	území
na	na	k7c6	na
severu	sever	k1gInSc6	sever
byla	být	k5eAaImAgNnP	být
získána	získat	k5eAaPmNgNnP	získat
od	od	k7c2	od
Žabovřesk	Žabovřesky	k1gFnPc2	Žabovřesky
až	až	k9	až
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
<s>
Zástavba	zástavba	k1gFnSc1	zástavba
dnešní	dnešní	k2eAgFnSc2d1	dnešní
čtvrti	čtvrt	k1gFnSc2	čtvrt
vznikala	vznikat	k5eAaImAgFnS	vznikat
postupně	postupně	k6eAd1	postupně
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
během	během	k7c2	během
třetí	třetí	k4xOgFnSc2	třetí
třetiny	třetina	k1gFnSc2	třetina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nejstarší	starý	k2eAgFnSc1d3	nejstarší
zástavba	zástavba	k1gFnSc1	zástavba
ulic	ulice	k1gFnPc2	ulice
Lidické	lidický	k2eAgFnSc2d1	Lidická
a	a	k8xC	a
Údolní	údolní	k2eAgFnSc1d1	údolní
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
již	již	k6eAd1	již
během	během	k7c2	během
středověku	středověk	k1gInSc2	středověk
a	a	k8xC	a
zástavba	zástavba	k1gFnSc1	zástavba
nejjižnější	jižní	k2eAgFnSc2d3	nejjižnější
části	část	k1gFnSc2	část
ulice	ulice	k1gFnSc2	ulice
Veveří	veveří	k2eAgFnSc2d1	veveří
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nejjižnější	jižní	k2eAgFnSc1d3	nejjižnější
část	část	k1gFnSc1	část
moderního	moderní	k2eAgInSc2d1	moderní
katastru	katastr	k1gInSc2	katastr
Veveří	veveří	k2eAgNnSc1d1	veveří
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Údolní	údolní	k2eAgFnSc2d1	údolní
ulice	ulice	k1gFnSc2	ulice
původně	původně	k6eAd1	původně
tvořila	tvořit	k5eAaImAgFnS	tvořit
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
bývalé	bývalý	k2eAgFnSc2d1	bývalá
vesnice	vesnice	k1gFnSc2	vesnice
Švábka	švábka	k1gFnSc1	švábka
<g/>
,	,	kIx,	,
několik	několik	k4yIc1	několik
bloků	blok	k1gInPc2	blok
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
nejjižnější	jižní	k2eAgFnSc2d3	nejjižnější
části	část	k1gFnSc2	část
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Veveří	veveří	k2eAgFnSc2d1	veveří
ulice	ulice	k1gFnSc2	ulice
zase	zase	k9	zase
patřilo	patřit	k5eAaImAgNnS	patřit
do	do	k7c2	do
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Malá	malý	k2eAgFnSc1d1	malá
Nová	nový	k2eAgFnSc1d1	nová
Ulice	ulice	k1gFnSc1	ulice
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
čtvrti	čtvrt	k1gFnSc2	čtvrt
byl	být	k5eAaImAgInS	být
hranicí	hranice	k1gFnSc7	hranice
probíhající	probíhající	k2eAgFnSc7d1	probíhající
Veveří	veveří	k2eAgFnSc7d1	veveří
ulicí	ulice	k1gFnSc7	ulice
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
mezi	mezi	k7c4	mezi
katastrální	katastrální	k2eAgNnSc4d1	katastrální
území	území	k1gNnSc4	území
Křížová	Křížová	k1gFnSc1	Křížová
(	(	kIx(	(
<g/>
jehož	jehož	k3xOyRp3gFnSc7	jehož
součástí	součást	k1gFnSc7	součást
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
i	i	k9	i
původně	původně	k6eAd1	původně
žabovřeské	žabovřeský	k2eAgInPc4d1	žabovřeský
pozemky	pozemek	k1gInPc4	pozemek
<g/>
,	,	kIx,	,
připojené	připojený	k2eAgInPc4d1	připojený
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
<g/>
)	)	kIx)	)
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
Nová	nový	k2eAgFnSc1d1	nová
Ulice	ulice	k1gFnSc1	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
30	[number]	k4	30
<g/>
.	.	kIx.	.
a	a	k8xC	a
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
celá	celý	k2eAgFnSc1d1	celá
zástavba	zástavba	k1gFnSc1	zástavba
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Malá	malý	k2eAgFnSc1d1	malá
Nová	nový	k2eAgFnSc1d1	nová
Ulice	ulice	k1gFnSc1	ulice
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
úzkým	úzký	k2eAgInSc7d1	úzký
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
severněji	severně	k6eAd2	severně
přiléhajícím	přiléhající	k2eAgInSc7d1	přiléhající
pruhem	pruh	k1gInSc7	pruh
zástavby	zástavba	k1gFnSc2	zástavba
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
katastrálním	katastrální	k2eAgNnSc7d1	katastrální
územím	území	k1gNnSc7	území
Švábská	švábský	k2eAgNnPc1d1	švábské
(	(	kIx(	(
<g/>
nepatrně	nepatrně	k6eAd1	nepatrně
přejmenované	přejmenovaný	k2eAgNnSc4d1	přejmenované
katastrální	katastrální	k2eAgNnSc4d1	katastrální
území	území	k1gNnSc4	území
Švábky	švábka	k1gFnSc2	švábka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
částí	část	k1gFnSc7	část
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Křížová	Křížová	k1gFnSc1	Křížová
vymezenou	vymezený	k2eAgFnSc4d1	vymezená
na	na	k7c6	na
severu	sever	k1gInSc6	sever
dnešní	dnešní	k2eAgFnSc2d1	dnešní
ulicí	ulice	k1gFnSc7	ulice
Gorkého	Gorkého	k2eAgMnSc2d1	Gorkého
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
ulicí	ulice	k1gFnSc7	ulice
Úvoz	úvoz	k1gInSc1	úvoz
<g/>
,	,	kIx,	,
připojeno	připojen	k2eAgNnSc4d1	připojeno
ke	k	k7c3	k
katastrálnímu	katastrální	k2eAgNnSc3d1	katastrální
území	území	k1gNnSc3	území
Špilberk	Špilberk	k1gInSc1	Špilberk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
čtvrtě	čtvrt	k1gFnSc2	čtvrt
na	na	k7c6	na
nejvyšším	vysoký	k2eAgInSc6d3	Nejvyšší
bodě	bod	k1gInSc6	bod
Kraví	kraví	k2eAgFnSc2d1	kraví
hory	hora	k1gFnSc2	hora
stavba	stavba	k1gFnSc1	stavba
dvou	dva	k4xCgFnPc2	dva
pozorovatelen	pozorovatelna	k1gFnPc2	pozorovatelna
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jednu	jeden	k4xCgFnSc4	jeden
dodnes	dodnes	k6eAd1	dodnes
využívá	využívat	k5eAaPmIp3nS	využívat
Hvězdárna	hvězdárna	k1gFnSc1	hvězdárna
a	a	k8xC	a
planetárium	planetárium	k1gNnSc1	planetárium
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
druhou	druhý	k4xOgFnSc4	druhý
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
připojila	připojit	k5eAaPmAgFnS	připojit
i	i	k9	i
menší	malý	k2eAgFnSc1d2	menší
budova	budova	k1gFnSc1	budova
planetária	planetárium	k1gNnSc2	planetárium
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
instituce	instituce	k1gFnSc1	instituce
dostala	dostat	k5eAaPmAgFnS	dostat
r.	r.	kA	r.
1973	[number]	k4	1973
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
500	[number]	k4	500
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
narození	narození	k1gNnSc2	narození
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
Koperníka	Koperník	k1gMnSc2	Koperník
jméno	jméno	k1gNnSc4	jméno
Hvězdárna	hvězdárna	k1gFnSc1	hvězdárna
a	a	k8xC	a
planetárium	planetárium	k1gNnSc1	planetárium
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
Koperníka	Koperník	k1gMnSc2	Koperník
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
byl	být	k5eAaImAgInS	být
položen	položit	k5eAaPmNgInS	položit
základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
k	k	k7c3	k
nové	nový	k2eAgFnSc3d1	nová
budově	budova	k1gFnSc3	budova
planetária	planetárium	k1gNnSc2	planetárium
(	(	kIx(	(
<g/>
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
s	s	k7c7	s
kupolí	kupole	k1gFnSc7	kupole
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
17,5	[number]	k4	17,5
m	m	kA	m
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
přestěhovalo	přestěhovat	k5eAaPmAgNnS	přestěhovat
těžiště	těžiště	k1gNnSc1	těžiště
konání	konání	k1gNnSc2	konání
nejen	nejen	k6eAd1	nejen
vzdělávacích	vzdělávací	k2eAgInPc2d1	vzdělávací
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
rozličných	rozličný	k2eAgFnPc2d1	rozličná
kulturních	kulturní	k2eAgFnPc2d1	kulturní
a	a	k8xC	a
výstavních	výstavní	k2eAgFnPc2d1	výstavní
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
straně	strana	k1gFnSc6	strana
blíže	blíž	k1gFnSc2	blíž
k	k	k7c3	k
městu	město	k1gNnSc3	město
bylo	být	k5eAaImAgNnS	být
r.	r.	kA	r.
1975	[number]	k4	1975
dokončen	dokončen	k2eAgInSc1d1	dokončen
svažitý	svažitý	k2eAgInSc1d1	svažitý
Areál	areál	k1gInSc1	areál
zdraví	zdravit	k5eAaImIp3nS	zdravit
TJ	tj	kA	tj
Moravská	moravský	k2eAgFnSc1d1	Moravská
Slávia	Slávia	k1gFnSc1	Slávia
(	(	kIx(	(
<g/>
projekt	projekt	k1gInSc1	projekt
M.	M.	kA	M.
Kramoliš	Kramoliš	k1gInSc1	Kramoliš
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
saunou	sauna	k1gFnSc7	sauna
<g/>
,	,	kIx,	,
bazénem	bazén	k1gInSc7	bazén
a	a	k8xC	a
slunečními	sluneční	k2eAgFnPc7d1	sluneční
loukami	louka	k1gFnPc7	louka
s	s	k7c7	s
nezaměnitelnými	zaměnitelný	k2eNgInPc7d1	nezaměnitelný
výhledy	výhled	k1gInPc7	výhled
na	na	k7c4	na
Brno	Brno	k1gNnSc4	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
na	na	k7c4	na
zamýšlené	zamýšlený	k2eAgNnSc4d1	zamýšlené
dobudování	dobudování	k1gNnSc4	dobudování
areálu	areál	k1gInSc2	areál
<g/>
:	:	kIx,	:
další	další	k2eAgInSc1d1	další
bazén	bazén	k1gInSc1	bazén
a	a	k8xC	a
"	"	kIx"	"
<g/>
oprášená	oprášený	k2eAgFnSc1d1	oprášená
<g/>
"	"	kIx"	"
myšlenka	myšlenka	k1gFnSc1	myšlenka
krytého	krytý	k2eAgInSc2d1	krytý
bazénu	bazén	k1gInSc2	bazén
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
areálem	areál	k1gInSc7	areál
bylo	být	k5eAaImAgNnS	být
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
Středisko	středisko	k1gNnSc1	středisko
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
léčivých	léčivý	k2eAgFnPc2d1	léčivá
rostlin	rostlina	k1gFnPc2	rostlina
při	při	k7c6	při
UJEP	UJEP	kA	UJEP
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
samostatné	samostatný	k2eAgNnSc1d1	samostatné
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Veveří	veveří	k2eAgNnSc1d1	veveří
při	při	k7c6	při
radikální	radikální	k2eAgFnSc6d1	radikální
druhé	druhý	k4xOgFnSc6	druhý
katastrální	katastrální	k2eAgFnSc6d1	katastrální
reformě	reforma	k1gFnSc6	reforma
Brna	Brno	k1gNnSc2	Brno
koncem	koncem	k7c2	koncem
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1979	[number]	k4	1979
pak	pak	k6eAd1	pak
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
byly	být	k5eAaImAgFnP	být
připojeny	připojit	k5eAaPmNgFnP	připojit
také	také	k9	také
západní	západní	k2eAgFnPc1d1	západní
části	část	k1gFnPc1	část
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
Lužánky	Lužánka	k1gFnSc2	Lužánka
a	a	k8xC	a
Ponava	Ponava	k1gFnSc1	Ponava
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Ponavy	Ponava	k1gFnSc2	Ponava
bloky	blok	k1gInPc1	blok
mezi	mezi	k7c7	mezi
ulicemi	ulice	k1gFnPc7	ulice
Kounicovou	Kounicový	k2eAgFnSc7d1	Kounicová
a	a	k8xC	a
Klatovskou	klatovský	k2eAgFnSc7d1	Klatovská
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
náleželo	náležet	k5eAaImAgNnS	náležet
celé	celý	k2eAgNnSc1d1	celé
moderní	moderní	k2eAgNnSc1d1	moderní
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
Veveří	veveří	k2eAgNnSc1d1	veveří
včetně	včetně	k7c2	včetně
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
později	pozdě	k6eAd2	pozdě
připojených	připojený	k2eAgFnPc2d1	připojená
částí	část	k1gFnPc2	část
katastrálních	katastrální	k2eAgFnPc2d1	katastrální
území	území	k1gNnPc2	území
Lužánky	Lužánka	k1gFnSc2	Lužánka
a	a	k8xC	a
Ponava	Ponava	k1gFnSc1	Ponava
k	k	k7c3	k
Městskému	městský	k2eAgInSc3d1	městský
obvodu	obvod	k1gInSc3	obvod
Brno	Brno	k1gNnSc4	Brno
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
je	být	k5eAaImIp3nS	být
celé	celý	k2eAgNnSc4d1	celé
katastrální	katastrální	k2eAgNnSc4d1	katastrální
území	území	k1gNnSc4	území
Veveří	veveří	k2eAgFnSc7d1	veveří
součástí	součást	k1gFnSc7	součást
samosprávné	samosprávný	k2eAgFnSc2d1	samosprávná
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-střed	Brnotřed	k1gMnSc1	Brno-střed
<g/>
.	.	kIx.	.
</s>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Pešek	Pešek	k1gMnSc1	Pešek
</s>
