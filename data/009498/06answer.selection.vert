<s>
Měsíc	měsíc	k1gInSc1	měsíc
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
synchronní	synchronní	k2eAgFnSc6d1	synchronní
rotaci	rotace	k1gFnSc6	rotace
se	s	k7c7	s
Zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedna	jeden	k4xCgFnSc1	jeden
strana	strana	k1gFnSc1	strana
Měsíce	měsíc	k1gInSc2	měsíc
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
přivrácená	přivrácený	k2eAgFnSc1d1	přivrácená
strana	strana	k1gFnSc1	strana
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
obrácená	obrácený	k2eAgFnSc1d1	obrácená
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
