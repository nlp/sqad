<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Aragonská	aragonský	k2eAgFnSc1d1	Aragonská
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1485	[number]	k4	1485
Alcalá	Alcalý	k2eAgFnSc1d1	Alcalá
de	de	k?	de
Henares	Henares	k1gInSc1	Henares
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1536	[number]	k4	1536
Huntingdonshire	Huntingdonshir	k1gInSc5	Huntingdonshir
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
anglická	anglický	k2eAgFnSc1d1	anglická
královna	královna	k1gFnSc1	královna
a	a	k8xC	a
první	první	k4xOgFnSc1	první
manželka	manželka	k1gFnSc1	manželka
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
</s>
<s>
Tudora	tudor	k1gMnSc4	tudor
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
katolických	katolický	k2eAgMnPc2d1	katolický
králů	král	k1gMnPc2	král
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Aragonského	aragonský	k2eAgMnSc2d1	aragonský
a	a	k8xC	a
Isabely	Isabela	k1gFnPc1	Isabela
I.	I.	kA	I.
Kastilské	kastilský	k2eAgFnPc1d1	Kastilská
a	a	k8xC	a
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
matce	matka	k1gFnSc6	matka
také	také	k9	také
prapravnučka	prapravnučka	k1gFnSc1	prapravnučka
anglického	anglický	k2eAgMnSc2d1	anglický
krále	král	k1gMnSc2	král
Eduarda	Eduard	k1gMnSc2	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
prapradědečkem	prapradědeček	k1gMnSc7	prapradědeček
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Aragonské	aragonský	k2eAgFnSc2d1	Aragonská
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Gentu	Gent	k1gInSc2	Gent
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
synů	syn	k1gMnPc2	syn
Eduarda	Eduard	k1gMnSc2	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gNnSc7	její
životním	životní	k2eAgNnSc7d1	životní
heslem	heslo	k1gNnSc7	heslo
bylo	být	k5eAaImAgNnS	být
"	"	kIx"	"
<g/>
skromnost	skromnost	k1gFnSc1	skromnost
a	a	k8xC	a
oddanost	oddanost	k1gFnSc1	oddanost
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Humble	Humble	k1gMnSc1	Humble
and	and	k?	and
Loyal	Loyal	k1gMnSc1	Loyal
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
arcibiskupském	arcibiskupský	k2eAgInSc6d1	arcibiskupský
paláci	palác	k1gInSc6	palác
v	v	k7c4	v
Alcalá	Alcalý	k2eAgNnPc4d1	Alcalý
de	de	k?	de
Henares	Henaresa	k1gFnPc2	Henaresa
u	u	k7c2	u
Madridu	Madrid	k1gInSc2	Madrid
jako	jako	k9	jako
nejmladší	mladý	k2eAgFnSc1d3	nejmladší
z	z	k7c2	z
pěti	pět	k4xCc2	pět
dětí	dítě	k1gFnPc2	dítě
katolických	katolický	k2eAgMnPc2d1	katolický
králů	král	k1gMnPc2	král
Španělska	Španělsko	k1gNnSc2	Španělsko
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Aragonského	aragonský	k2eAgMnSc2d1	aragonský
a	a	k8xC	a
Isabely	Isabela	k1gFnPc4	Isabela
I.	I.	kA	I.
Kastilské	kastilský	k2eAgFnPc4d1	Kastilská
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1489	[number]	k4	1489
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
smlouva	smlouva	k1gFnSc1	smlouva
mezi	mezi	k7c7	mezi
anglickým	anglický	k2eAgInSc7d1	anglický
a	a	k8xC	a
španělským	španělský	k2eAgInSc7d1	španělský
dvorem	dvůr	k1gInSc7	dvůr
o	o	k7c6	o
sňatku	sňatek	k1gInSc6	sňatek
Kateřiny	Kateřina	k1gFnSc2	Kateřina
s	s	k7c7	s
tehdy	tehdy	k6eAd1	tehdy
teprve	teprve	k6eAd1	teprve
dvouletým	dvouletý	k2eAgMnSc7d1	dvouletý
následníkem	následník	k1gMnSc7	následník
trůnu	trůn	k1gInSc2	trůn
Arturem	Artur	k1gMnSc7	Artur
Tudorem	tudor	k1gInSc7	tudor
<g/>
,	,	kIx,	,
synem	syn	k1gMnSc7	syn
krále	král	k1gMnSc2	král
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
1501	[number]	k4	1501
Kateřina	Kateřina	k1gFnSc1	Kateřina
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnSc2d1	bílá
pleti	pleť	k1gFnSc2	pleť
<g/>
,	,	kIx,	,
plavovlasá	plavovlasý	k2eAgFnSc1d1	plavovlasá
a	a	k8xC	a
modrooká	modrooký	k2eAgFnSc1d1	modrooká
jako	jako	k8xS	jako
všichni	všechen	k3xTgMnPc1	všechen
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Trastámarů	Trastámar	k1gInPc2	Trastámar
pocházející	pocházející	k2eAgInPc1d1	pocházející
od	od	k7c2	od
Kateřiny	Kateřina	k1gFnSc2	Kateřina
z	z	k7c2	z
Lancesteru	Lancester	k1gInSc2	Lancester
<g/>
,	,	kIx,	,
opustila	opustit	k5eAaPmAgFnS	opustit
Španělsko	Španělsko	k1gNnSc4	Španělsko
a	a	k8xC	a
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
cestu	cesta	k1gFnSc4	cesta
za	za	k7c7	za
svým	svůj	k3xOyFgMnSc7	svůj
budoucím	budoucí	k2eAgMnSc7d1	budoucí
manželem	manžel	k1gMnSc7	manžel
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1501	[number]	k4	1501
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
konala	konat	k5eAaImAgFnS	konat
svatba	svatba	k1gFnSc1	svatba
mezi	mezi	k7c7	mezi
patnáctiletou	patnáctiletý	k2eAgFnSc7d1	patnáctiletá
španělskou	španělský	k2eAgFnSc7d1	španělská
infantkou	infantka	k1gFnSc7	infantka
a	a	k8xC	a
o	o	k7c6	o
rok	rok	k1gInSc1	rok
mladším	mladý	k2eAgMnSc7d2	mladší
následníkem	následník	k1gMnSc7	následník
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
manželé	manžel	k1gMnPc1	manžel
pobývali	pobývat	k5eAaImAgMnP	pobývat
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Ludlow	Ludlow	k1gFnSc2	Ludlow
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
Arturovi	Artur	k1gMnSc3	Artur
jako	jako	k8xS	jako
princi	princ	k1gMnSc3	princ
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
náležel	náležet	k5eAaImAgInS	náležet
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
oba	dva	k4xCgMnPc1	dva
mladí	mladý	k2eAgMnPc1d1	mladý
manželé	manžel	k1gMnPc1	manžel
těžce	těžce	k6eAd1	těžce
onemocněli	onemocnět	k5eAaPmAgMnP	onemocnět
horečnatým	horečnatý	k2eAgNnSc7d1	horečnaté
onemocněním	onemocnění	k1gNnSc7	onemocnění
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
hemoragickou	hemoragický	k2eAgFnSc7d1	hemoragická
horečkou	horečka	k1gFnSc7	horečka
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
dobře	dobře	k6eAd1	dobře
živená	živený	k2eAgFnSc1d1	živená
Kateřina	Kateřina	k1gFnSc1	Kateřina
nemoc	nemoc	k1gFnSc1	nemoc
přestála	přestát	k5eAaPmAgFnS	přestát
<g/>
,	,	kIx,	,
její	její	k3xOp3gNnSc1	její
drobný	drobný	k2eAgMnSc1d1	drobný
manžel	manžel	k1gMnSc1	manžel
jí	jíst	k5eAaImIp3nS	jíst
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
a	a	k8xC	a
Kateřina	Kateřina	k1gFnSc1	Kateřina
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
po	po	k7c6	po
čtyřech	čtyři	k4xCgInPc6	čtyři
měsících	měsíc	k1gInPc6	měsíc
manželství	manželství	k1gNnSc1	manželství
vdovou	vdova	k1gFnSc7	vdova
<g/>
.	.	kIx.	.
</s>
<s>
Kateřinin	Kateřinin	k2eAgMnSc1d1	Kateřinin
otec	otec	k1gMnSc1	otec
dosud	dosud	k6eAd1	dosud
nevyplatil	vyplatit	k5eNaPmAgMnS	vyplatit
Jindřichovi	Jindřich	k1gMnSc3	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Tudorovi	Tudorův	k2eAgMnPc1d1	Tudorův
věno	věno	k1gNnSc1	věno
své	svůj	k3xOyFgFnSc2	svůj
dcery	dcera	k1gFnSc2	dcera
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
<g/>
,	,	kIx,	,
když	když	k8xS	když
Kateřina	Kateřina	k1gFnSc1	Kateřina
ovdověla	ovdovět	k5eAaPmAgFnS	ovdovět
<g/>
,	,	kIx,	,
odmítal	odmítat	k5eAaImAgMnS	odmítat
je	být	k5eAaImIp3nS	být
vyplatit	vyplatit	k5eAaPmF	vyplatit
tím	ten	k3xDgNnSc7	ten
spíš	spát	k5eAaImIp2nS	spát
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
nechtěl	chtít	k5eNaImAgMnS	chtít
věna	věno	k1gNnPc4	věno
vzdát	vzdát	k5eAaPmF	vzdát
a	a	k8xC	a
Kateřina	Kateřina	k1gFnSc1	Kateřina
musela	muset	k5eAaImAgFnS	muset
zůstat	zůstat	k5eAaPmF	zůstat
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
princezna	princezna	k1gFnSc1	princezna
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
chvíle	chvíle	k1gFnSc2	chvíle
čekala	čekat	k5eAaImAgFnS	čekat
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
doroste	dorůst	k5eAaPmIp3nS	dorůst
druhý	druhý	k4xOgMnSc1	druhý
následník	následník	k1gMnSc1	následník
trůnu	trůn	k1gInSc2	trůn
<g/>
,	,	kIx,	,
o	o	k7c4	o
šest	šest	k4xCc4	šest
let	let	k1gInSc4	let
mladší	mladý	k2eAgMnSc1d2	mladší
Jindřich	Jindřich	k1gMnSc1	Jindřich
a	a	k8xC	a
ona	onen	k3xDgFnSc1	onen
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
moci	moct	k5eAaImF	moct
provdat	provdat	k5eAaPmF	provdat
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
ale	ale	k9	ale
pro	pro	k7c4	pro
Kateřinu	Kateřina	k1gFnSc4	Kateřina
nevyvíjela	vyvíjet	k5eNaImAgFnS	vyvíjet
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
zemřela	zemřít	k5eAaPmAgFnS	zemřít
její	její	k3xOp3gFnSc4	její
matka	matka	k1gFnSc1	matka
Isabela	Isabela	k1gFnSc1	Isabela
Kastilská	kastilský	k2eAgFnSc1d1	Kastilská
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
najít	najít	k5eAaPmF	najít
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
vhodnější	vhodný	k2eAgFnSc4d2	vhodnější
nevěstu	nevěsta	k1gFnSc4	nevěsta
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
sňatek	sňatek	k1gInSc1	sňatek
s	s	k7c7	s
Kateřinou	Kateřina	k1gFnSc7	Kateřina
už	už	k6eAd1	už
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nezdál	zdát	k5eNaImAgInS	zdát
tak	tak	k6eAd1	tak
výhodný	výhodný	k2eAgInSc1d1	výhodný
<g/>
.	.	kIx.	.
</s>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
se	se	k3xPyFc4	se
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
nepříjemné	příjemný	k2eNgFnSc3d1	nepříjemná
situaci	situace	k1gFnSc3	situace
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
tchán	tchán	k1gMnSc1	tchán
Jindřich	Jindřich	k1gMnSc1	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Tudor	tudor	k1gInSc4	tudor
jí	jíst	k5eAaImIp3nS	jíst
sdělil	sdělit	k5eAaPmAgMnS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
chvíle	chvíle	k1gFnSc2	chvíle
nemá	mít	k5eNaImIp3nS	mít
vůči	vůči	k7c3	vůči
ní	on	k3xPp3gFnSc3	on
žádné	žádný	k3yNgInPc4	žádný
závazky	závazek	k1gInPc4	závazek
a	a	k8xC	a
přestal	přestat	k5eAaPmAgMnS	přestat
jí	jíst	k5eAaImIp3nS	jíst
poskytovat	poskytovat	k5eAaImF	poskytovat
finanční	finanční	k2eAgFnSc4d1	finanční
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čase	čas	k1gInSc6	čas
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
dodávat	dodávat	k5eAaImF	dodávat
na	na	k7c4	na
její	její	k3xOp3gInSc4	její
malý	malý	k2eAgInSc4d1	malý
soukromý	soukromý	k2eAgInSc4d1	soukromý
dvůr	dvůr	k1gInSc4	dvůr
také	také	k9	také
jídlo	jídlo	k1gNnSc4	jídlo
a	a	k8xC	a
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
na	na	k7c4	na
vydržování	vydržování	k1gNnSc4	vydržování
služebnictva	služebnictvo	k1gNnSc2	služebnictvo
<g/>
.	.	kIx.	.
</s>
<s>
Zoufalá	zoufalý	k2eAgFnSc1d1	zoufalá
Kateřina	Kateřina	k1gFnSc1	Kateřina
prosila	prosít	k5eAaPmAgFnS	prosít
v	v	k7c6	v
dopisech	dopis	k1gInPc6	dopis
svého	svůj	k1gMnSc4	svůj
otce	otec	k1gMnSc4	otec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jí	on	k3xPp3gFnSc3	on
finančně	finančně	k6eAd1	finančně
vypomohl	vypomoct	k5eAaPmAgMnS	vypomoct
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
období	období	k1gNnSc6	období
nejistoty	nejistota	k1gFnSc2	nejistota
se	se	k3xPyFc4	se
konečně	konečně	k6eAd1	konečně
situace	situace	k1gFnSc1	situace
obrátila	obrátit	k5eAaPmAgFnS	obrátit
v	v	k7c4	v
Kateřinin	Kateřinin	k2eAgInSc4d1	Kateřinin
prospěch	prospěch	k1gInSc4	prospěch
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
král	král	k1gMnSc1	král
Jindřich	Jindřich	k1gMnSc1	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Tudor	tudor	k1gInSc1	tudor
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1509	[number]	k4	1509
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Trůnu	trůn	k1gInSc3	trůn
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Jindřich	Jindřich	k1gMnSc1	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
pevně	pevně	k6eAd1	pevně
rozhodnut	rozhodnout	k5eAaPmNgMnS	rozhodnout
vzít	vzít	k5eAaPmF	vzít
si	se	k3xPyFc3	se
za	za	k7c4	za
manželku	manželka	k1gFnSc4	manželka
právě	právě	k9	právě
Kateřinu	Kateřina	k1gFnSc4	Kateřina
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
již	již	k6eAd1	již
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
v	v	k7c4	v
se	se	k3xPyFc4	se
zrovna	zrovna	k6eAd1	zrovna
v	v	k7c6	v
Evorpě	Evorpa	k1gFnSc6	Evorpa
nenacházela	nacházet	k5eNaImAgFnS	nacházet
jiná	jiný	k2eAgFnSc1d1	jiná
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
urozená	urozený	k2eAgFnSc1d1	urozená
nevěsta	nevěsta	k1gFnSc1	nevěsta
v	v	k7c6	v
podobném	podobný	k2eAgInSc6d1	podobný
věku	věk	k1gInSc6	věk
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
sňatek	sňatek	k1gInSc4	sňatek
byl	být	k5eAaImAgMnS	být
však	však	k9	však
potřeba	potřeba	k6eAd1	potřeba
papežova	papežův	k2eAgInSc2d1	papežův
dispensu	dispens	k1gInSc2	dispens
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
si	se	k3xPyFc3	se
Kateřina	Kateřina	k1gFnSc1	Kateřina
měla	mít	k5eAaImAgFnS	mít
vzít	vzít	k5eAaPmF	vzít
bratra	bratr	k1gMnSc4	bratr
svého	svůj	k3xOyFgMnSc4	svůj
zemřelého	zemřelý	k1gMnSc4	zemřelý
manžela	manžel	k1gMnSc4	manžel
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
nepřípustné	přípustný	k2eNgNnSc1d1	nepřípustné
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Julius	Julius	k1gMnSc1	Julius
II	II	kA	II
<g/>
.	.	kIx.	.
dispens	dispens	k1gFnSc1	dispens
udělil	udělit	k5eAaPmAgInS	udělit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
budoucí	budoucí	k2eAgFnSc1d1	budoucí
nevěsta	nevěsta	k1gFnSc1	nevěsta
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
předchozí	předchozí	k2eAgNnSc1d1	předchozí
manželství	manželství	k1gNnSc1	manželství
nebylo	být	k5eNaImAgNnS	být
naplněno	naplnit	k5eAaPmNgNnS	naplnit
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
Kateřinu	Kateřina	k1gFnSc4	Kateřina
za	za	k7c4	za
manželku	manželka	k1gFnSc4	manželka
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1509	[number]	k4	1509
<g/>
.	.	kIx.	.
</s>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
už	už	k6eAd1	už
jako	jako	k8xS	jako
princezna	princezna	k1gFnSc1	princezna
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
poddaní	poddaný	k1gMnPc1	poddaný
s	s	k7c7	s
mladou	mladá	k1gFnSc7	mladá
královnu	královna	k1gFnSc4	královna
soucítili	soucítit	k5eAaImAgMnP	soucítit
i	i	k9	i
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
dalším	další	k2eAgNnSc6d1	další
utrpení	utrpení	k1gNnSc6	utrpení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1510	[number]	k4	1510
porodila	porodit	k5eAaPmAgFnS	porodit
mrtvé	mrtvý	k2eAgNnSc4d1	mrtvé
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ani	ani	k8xC	ani
ne	ne	k9	ne
dvouměsíční	dvouměsíční	k2eAgMnSc1d1	dvouměsíční
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
narozený	narozený	k2eAgMnSc1d1	narozený
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1511	[number]	k4	1511
<g/>
,	,	kIx,	,
princ	princ	k1gMnSc1	princ
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
.	.	kIx.	.
</s>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
následně	následně	k6eAd1	následně
prodělala	prodělat	k5eAaPmAgFnS	prodělat
ještě	ještě	k9	ještě
několik	několik	k4yIc4	několik
potratů	potrat	k1gInPc2	potrat
a	a	k8xC	a
porod	porod	k1gInSc1	porod
dalšího	další	k2eAgMnSc2d1	další
mrtvého	mrtvý	k2eAgMnSc2d1	mrtvý
syna	syn	k1gMnSc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1516	[number]	k4	1516
nakonec	nakonec	k9	nakonec
přivedla	přivést	k5eAaPmAgFnS	přivést
na	na	k7c4	na
svět	svět	k1gInSc4	svět
dceru	dcera	k1gFnSc4	dcera
Marii	Maria	k1gFnSc4	Maria
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgFnSc4d2	pozdější
královnu	královna	k1gFnSc4	královna
Marii	Maria	k1gFnSc3	Maria
I.	I.	kA	I.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1518	[number]	k4	1518
však	však	k9	však
Kateřina	Kateřina	k1gFnSc1	Kateřina
opět	opět	k6eAd1	opět
potratila	potratit	k5eAaPmAgFnS	potratit
<g/>
.	.	kIx.	.
</s>
<s>
Manželství	manželství	k1gNnSc1	manželství
bylo	být	k5eAaImAgNnS	být
šťastné	šťastný	k2eAgNnSc1d1	šťastné
pro	pro	k7c4	pro
oba	dva	k4xCgMnPc1	dva
dva	dva	k4xCgMnPc1	dva
(	(	kIx(	(
<g/>
s	s	k7c7	s
občasnými	občasný	k2eAgFnPc7d1	občasná
-	-	kIx~	-
nedůležitými	důležitý	k2eNgFnPc7d1	nedůležitá
-	-	kIx~	-
královými	králův	k2eAgFnPc7d1	králova
nevěrami	nevěra	k1gFnPc7	nevěra
<g/>
)	)	kIx)	)
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
král	král	k1gMnSc1	král
nezačal	začít	k5eNaPmAgMnS	začít
vážně	vážně	k6eAd1	vážně
zabývat	zabývat	k5eAaImF	zabývat
problémem	problém	k1gInSc7	problém
nutnosti	nutnost	k1gFnSc2	nutnost
mít	mít	k5eAaImF	mít
mužského	mužský	k2eAgMnSc4d1	mužský
potomka	potomek	k1gMnSc4	potomek
<g/>
,	,	kIx,	,
následníka	následník	k1gMnSc4	následník
trůnu	trůn	k1gInSc2	trůn
<g/>
,	,	kIx,	,
a	a	k8xC	a
zřejmým	zřejmý	k2eAgInSc7d1	zřejmý
koncem	konec	k1gInSc7	konec
královniny	královnin	k2eAgFnSc2d1	Královnina
plodnosti	plodnost	k1gFnSc2	plodnost
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
Kateřiny	Kateřina	k1gFnSc2	Kateřina
se	se	k3xPyFc4	se
již	již	k6eAd1	již
dědice	dědic	k1gMnPc4	dědic
nedočká	dočkat	k5eNaPmIp3nS	dočkat
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
vdovy	vdova	k1gFnSc2	vdova
po	po	k7c6	po
bratrovi	bratr	k1gMnSc6	bratr
zbavit	zbavit	k5eAaPmF	zbavit
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
se	se	k3xPyFc4	se
však	však	k9	však
postavil	postavit	k5eAaPmAgMnS	postavit
za	za	k7c4	za
Kateřinu	Kateřina	k1gFnSc4	Kateřina
a	a	k8xC	a
proti	proti	k7c3	proti
rozvodu	rozvod	k1gInSc3	rozvod
<g/>
.	.	kIx.	.
</s>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
bojovat	bojovat	k5eAaImF	bojovat
za	za	k7c4	za
zachování	zachování	k1gNnSc4	zachování
svého	svůj	k3xOyFgNnSc2	svůj
manželství	manželství	k1gNnSc2	manželství
všemi	všecek	k3xTgInPc7	všecek
prostředky	prostředek	k1gInPc7	prostředek
a	a	k8xC	a
pomoc	pomoc	k1gFnSc1	pomoc
hledala	hledat	k5eAaImAgFnS	hledat
u	u	k7c2	u
svého	svůj	k3xOyFgMnSc2	svůj
synovce	synovec	k1gMnSc2	synovec
<g/>
,	,	kIx,	,
císaře	císař	k1gMnSc2	císař
a	a	k8xC	a
španělského	španělský	k2eAgMnSc2d1	španělský
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
V.	V.	kA	V.
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nejmocnějším	mocný	k2eAgMnSc7d3	nejmocnější
panovníkem	panovník	k1gMnSc7	panovník
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
také	také	k9	také
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
tlak	tlak	k1gInSc4	tlak
na	na	k7c4	na
papeže	papež	k1gMnSc4	papež
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
rozvod	rozvod	k1gInSc1	rozvod
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
nepovolil	povolit	k5eNaPmAgMnS	povolit
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
silný	silný	k2eAgInSc1d1	silný
tlak	tlak	k1gInSc1	tlak
byl	být	k5eAaImAgInS	být
však	však	k9	však
na	na	k7c4	na
papeže	papež	k1gMnSc4	papež
činěn	činěn	k2eAgInSc1d1	činěn
i	i	k8xC	i
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
anglického	anglický	k2eAgMnSc2d1	anglický
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
si	se	k3xPyFc3	se
Kateřinu	Kateřina	k1gFnSc4	Kateřina
bral	brát	k5eAaImAgMnS	brát
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
již	již	k9	již
pannou	panna	k1gFnSc7	panna
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gNnSc1	její
první	první	k4xOgNnSc1	první
manželství	manželství	k1gNnSc1	manželství
s	s	k7c7	s
Arturem	Artur	k1gMnSc7	Artur
Tudorem	tudor	k1gInSc7	tudor
bylo	být	k5eAaImAgNnS	být
naplněno	naplněn	k2eAgNnSc1d1	naplněno
a	a	k8xC	a
druhé	druhý	k4xOgNnSc1	druhý
manželství	manželství	k1gNnSc1	manželství
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
neplatné	platný	k2eNgNnSc1d1	neplatné
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1533	[number]	k4	1533
se	se	k3xPyFc4	se
Jindřich	Jindřich	k1gMnSc1	Jindřich
tajně	tajně	k6eAd1	tajně
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
Boleynovou	Boleynová	k1gFnSc7	Boleynová
<g/>
.	.	kIx.	.
</s>
<s>
Sňatek	sňatek	k1gInSc1	sňatek
s	s	k7c7	s
Kateřinou	Kateřina	k1gFnSc7	Kateřina
zrušil	zrušit	k5eAaPmAgInS	zrušit
parlament	parlament	k1gInSc1	parlament
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1533	[number]	k4	1533
a	a	k8xC	a
následně	následně	k6eAd1	následně
(	(	kIx(	(
<g/>
údajně	údajně	k6eAd1	údajně
pod	pod	k7c7	pod
nátlakem	nátlak	k1gInSc7	nátlak
<g/>
)	)	kIx)	)
i	i	k8xC	i
anglický	anglický	k2eAgInSc1d1	anglický
klér	klér	k1gInSc1	klér
-	-	kIx~	-
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
papeže	papež	k1gMnSc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Dcera	dcera	k1gFnSc1	dcera
Marie	Maria	k1gFnSc2	Maria
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
nemanželskou	manželský	k2eNgFnSc7d1	nemanželská
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1533	[number]	k4	1533
Anna	Anna	k1gFnSc1	Anna
Boleynová	Boleynová	k1gFnSc1	Boleynová
Jindřichovi	Jindřich	k1gMnSc3	Jindřich
porodila	porodit	k5eAaPmAgFnS	porodit
dceru	dcera	k1gFnSc4	dcera
Alžbětu	Alžběta	k1gFnSc4	Alžběta
a	a	k8xC	a
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1534	[number]	k4	1534
papež	papež	k1gMnSc1	papež
Klement	Klement	k1gMnSc1	Klement
VII	VII	kA	VII
<g/>
.	.	kIx.	.
schválil	schválit	k5eAaPmAgInS	schválit
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
rozvod	rozvod	k1gInSc1	rozvod
s	s	k7c7	s
Kateřinou	Kateřina	k1gFnSc7	Kateřina
<g/>
.	.	kIx.	.
</s>
<s>
Panovníkův	panovníkův	k2eAgInSc1d1	panovníkův
rozkol	rozkol	k1gInSc1	rozkol
s	s	k7c7	s
římsko-katolickou	římskoatolický	k2eAgFnSc7d1	římsko-katolická
církví	církev	k1gFnSc7	církev
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
položil	položit	k5eAaPmAgInS	položit
základ	základ	k1gInSc4	základ
budoucím	budoucí	k2eAgMnPc3d1	budoucí
náboženským	náboženský	k2eAgMnPc3d1	náboženský
nepokojům	nepokoj	k1gInPc3	nepokoj
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
už	už	k6eAd1	už
neodvratný	odvratný	k2eNgInSc1d1	neodvratný
<g/>
.	.	kIx.	.
</s>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
prožila	prožít	k5eAaPmAgFnS	prožít
nezáviděníhodný	nezáviděníhodný	k2eAgInSc4d1	nezáviděníhodný
život	život	k1gInSc4	život
plný	plný	k2eAgInSc4d1	plný
útrap	útrap	k1gInSc4	útrap
a	a	k8xC	a
nespravedlnosti	nespravedlnost	k1gFnPc4	nespravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
bojovala	bojovat	k5eAaImAgFnS	bojovat
o	o	k7c6	o
zajištění	zajištění	k1gNnSc6	zajištění
práv	právo	k1gNnPc2	právo
své	svůj	k3xOyFgFnSc2	svůj
dcery	dcera	k1gFnSc2	dcera
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Anglická	anglický	k2eAgFnSc1d1	anglická
královna	královna	k1gFnSc1	královna
Kateřina	Kateřina	k1gFnSc1	Kateřina
zemřela	zemřít	k5eAaPmAgFnS	zemřít
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1536	[number]	k4	1536
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Kimbolton	Kimbolton	k1gInSc1	Kimbolton
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
se	se	k3xPyFc4	se
pohřbu	pohřeb	k1gInSc2	pohřeb
vdovy	vdova	k1gFnSc2	vdova
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
<g/>
.	.	kIx.	.
</s>
<s>
Syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
Eduarda	Eduard	k1gMnSc2	Eduard
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
král	král	k1gMnSc1	král
dočkal	dočkat	k5eAaPmAgMnS	dočkat
ze	z	k7c2	z
svazku	svazek	k1gInSc2	svazek
s	s	k7c7	s
Janou	Jana	k1gFnSc7	Jana
Seymourovou	Seymourův	k2eAgFnSc7d1	Seymourova
<g/>
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
však	však	k9	však
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
šestnácti	šestnáct	k4xCc6	šestnáct
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
pak	pak	k6eAd1	pak
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
Kateřinina	Kateřinin	k2eAgFnSc1d1	Kateřinina
dcera	dcera	k1gFnSc1	dcera
Marie	Marie	k1gFnSc1	Marie
jako	jako	k8xS	jako
anglická	anglický	k2eAgFnSc1d1	anglická
královna	královna	k1gFnSc1	královna
Marie	Maria	k1gFnSc2	Maria
I.	I.	kA	I.
syn	syn	k1gMnSc1	syn
(	(	kIx(	(
<g/>
1510	[number]	k4	1510
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
<g/>
;	;	kIx,	;
Jindřich	Jindřich	k1gMnSc1	Jindřich
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1511	[number]	k4	1511
-	-	kIx~	-
zemřel	zemřít	k5eAaPmAgMnS	zemřít
pouhých	pouhý	k2eAgNnPc2d1	pouhé
52	[number]	k4	52
dní	den	k1gInPc2	den
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
princ	princ	k1gMnSc1	princ
waleský	waleský	k2eAgMnSc1d1	waleský
a	a	k8xC	a
vévoda	vévoda	k1gMnSc1	vévoda
cornwallský	cornwallský	k2eAgMnSc1d1	cornwallský
<g/>
<g />
.	.	kIx.	.
</s>
<s>
;	;	kIx,	;
potrat	potrat	k1gInSc4	potrat
(	(	kIx(	(
<g/>
1513	[number]	k4	1513
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Jindřich	Jindřich	k1gMnSc1	Jindřich
(	(	kIx(	(
<g/>
prosinec	prosinec	k1gInSc1	prosinec
1514	[number]	k4	1514
-	-	kIx~	-
prosinec	prosinec	k1gInSc1	prosinec
1514	[number]	k4	1514
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
měsíc	měsíc	k1gInSc4	měsíc
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
princ	princ	k1gMnSc1	princ
waleský	waleský	k2eAgMnSc1d1	waleský
a	a	k8xC	a
vévoda	vévoda	k1gMnSc1	vévoda
cornwallský	cornwallský	k2eAgMnSc1d1	cornwallský
<g/>
;	;	kIx,	;
Marie	Marie	k1gFnSc1	Marie
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
únor	únor	k1gInSc1	únor
1516	[number]	k4	1516
-	-	kIx~	-
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1558	[number]	k4	1558
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anglická	anglický	k2eAgFnSc1d1	anglická
královna	královna	k1gFnSc1	královna
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1553	[number]	k4	1553
<g/>
-	-	kIx~	-
<g/>
1558	[number]	k4	1558
<g/>
,	,	kIx,	,
~	~	kIx~	~
Filip	Filip	k1gMnSc1	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Španělský	španělský	k2eAgMnSc1d1	španělský
;	;	kIx,	;
potrat	potrat	k1gInSc1	potrat
(	(	kIx(	(
<g/>
1518	[number]	k4	1518
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Když	když	k8xS	když
Kateřina	Kateřina	k1gFnSc1	Kateřina
zemřela	zemřít	k5eAaPmAgFnS	zemřít
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
ostatky	ostatek	k1gInPc1	ostatek
byly	být	k5eAaImAgInP	být
balzamovány	balzamovat	k5eAaImNgInP	balzamovat
<g/>
,	,	kIx,	,
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gNnSc1	její
srdce	srdce	k1gNnSc1	srdce
bylo	být	k5eAaImAgNnS	být
zčernalé	zčernalý	k2eAgNnSc1d1	zčernalé
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
čas	čas	k1gInSc1	čas
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
příčina	příčina	k1gFnSc1	příčina
její	její	k3xOp3gFnSc2	její
smrti	smrt	k1gFnSc2	smrt
byla	být	k5eAaImAgFnS	být
otrava	otrava	k1gFnSc1	otrava
(	(	kIx(	(
<g/>
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
Anny	Anna	k1gFnSc2	Anna
Boleynové	Boleynová	k1gFnSc2	Boleynová
či	či	k8xC	či
samotného	samotný	k2eAgMnSc2d1	samotný
krále	král	k1gMnSc2	král
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgMnPc1d1	dnešní
experti	expert	k1gMnPc1	expert
se	se	k3xPyFc4	se
však	však	k9	však
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
rakovina	rakovina	k1gFnSc1	rakovina
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
příliš	příliš	k6eAd1	příliš
nevědělo	vědět	k5eNaImAgNnS	vědět
<g/>
;	;	kIx,	;
ostatně	ostatně	k6eAd1	ostatně
o	o	k7c4	o
23	[number]	k4	23
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
Kateřinina	Kateřinin	k2eAgFnSc1d1	Kateřinina
dcera	dcera	k1gFnSc1	dcera
Marie	Maria	k1gFnSc2	Maria
patrně	patrně	k6eAd1	patrně
rovněž	rovněž	k9	rovněž
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známý	k2eAgFnSc1d1	známá
je	být	k5eAaImIp3nS	být
pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jindřich	Jindřich	k1gMnSc1	Jindřich
Kateřinu	Kateřina	k1gFnSc4	Kateřina
před	před	k7c7	před
svatbou	svatba	k1gFnSc7	svatba
vyzpovídal	vyzpovídat	k5eAaPmAgMnS	vyzpovídat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
měl	mít	k5eAaImAgMnS	mít
učinit	učinit	k5eAaImF	učinit
bez	bez	k7c2	bez
jejího	její	k3xOp3gNnSc2	její
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozvodu	rozvod	k1gInSc6	rozvod
byla	být	k5eAaImAgFnS	být
titulována	titulován	k2eAgFnSc1d1	titulována
princeznou	princezna	k1gFnSc7	princezna
vdovou	vdova	k1gFnSc7	vdova
waleskou	waleský	k2eAgFnSc4d1	Waleská
Její	její	k3xOp3gInSc1	její
hrob	hrob	k1gInSc1	hrob
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
v	v	k7c6	v
Peterboroughu	Peterborough	k1gInSc6	Peterborough
(	(	kIx(	(
<g/>
středovýchodní	středovýchodní	k2eAgFnSc2d1	středovýchodní
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Monumentální	monumentální	k2eAgInSc1d1	monumentální
náhrobek	náhrobek	k1gInSc1	náhrobek
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
teprve	teprve	k6eAd1	teprve
ve	v	k7c6	v
viktoriánském	viktoriánský	k2eAgNnSc6d1	viktoriánské
období	období	k1gNnSc6	období
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
oprav	oprava	k1gFnPc2	oprava
katedrály	katedrála	k1gFnSc2	katedrála
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
za	za	k7c4	za
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
sebrané	sebraný	k2eAgInPc4d1	sebraný
od	od	k7c2	od
jejích	její	k3xOp3gFnPc2	její
jmenovkyň	jmenovkyně	k1gFnPc2	jmenovkyně
katolické	katolický	k2eAgFnSc2d1	katolická
víry	víra	k1gFnSc2	víra
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
,	,	kIx,	,
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Osobní	osobní	k2eAgMnSc1d1	osobní
kaplan	kaplan	k1gMnSc1	kaplan
byl	být	k5eAaImAgMnS	být
Tomáš	Tomáš	k1gMnSc1	Tomáš
Abell	Abell	k1gMnSc1	Abell
Osobní	osobní	k2eAgMnSc1d1	osobní
lékař	lékař	k1gMnSc1	lékař
byl	být	k5eAaImAgMnS	být
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
de	de	k?	de
Victoria	Victorium	k1gNnSc2	Victorium
HOLT	HOLT	k?	HOLT
Victoria	Victorium	k1gNnSc2	Victorium
<g/>
,	,	kIx,	,
Panenská	panenský	k2eAgFnSc1d1	panenská
vdova	vdova	k1gFnSc1	vdova
<g/>
,	,	kIx,	,
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Baronet	baroneta	k1gFnPc2	baroneta
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7214-013-2	[number]	k4	80-7214-013-2
RIVAL	rival	k1gMnSc1	rival
Paul	Paul	k1gMnSc1	Paul
<g/>
,	,	kIx,	,
Šest	šest	k4xCc1	šest
žen	žena	k1gFnPc2	žena
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Motto	motto	k1gNnSc4	motto
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7246-090-0	[number]	k4	80-7246-090-0
FRASER	FRASER	kA	FRASER
Antonia	Antonio	k1gMnSc2	Antonio
<g/>
,	,	kIx,	,
Šest	šest	k4xCc1	šest
žen	žena	k1gFnPc2	žena
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Domino	domino	k1gNnSc1	domino
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7303-257-0	[number]	k4	80-7303-257-0
WEIR	WEIR	kA	WEIR
Alison	Alisona	k1gFnPc2	Alisona
<g/>
,	,	kIx,	,
Šest	šest	k4xCc4	šest
žen	žena	k1gFnPc2	žena
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VIII	VIII	kA	VIII
<g/>
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
vydavatelství	vydavatelství	k1gNnSc3	vydavatelství
Melantrich	Melantricha	k1gFnPc2	Melantricha
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7023-285-4	[number]	k4	80-7023-285-4
ERICKSON	ERICKSON	kA	ERICKSON
Carolly	Carolla	k1gFnSc2	Carolla
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
Veliký	veliký	k2eAgMnSc1d1	veliký
,	,	kIx,	,
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Domino	domino	k1gNnSc1	domino
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86128-41-5	[number]	k4	80-86128-41-5
ERICKSON	ERICKSON	kA	ERICKSON
Carolly	Carolla	k1gFnSc2	Carolla
<g/>
,	,	kIx,	,
Krvavá	krvavý	k2eAgFnSc1d1	krvavá
Marie	Marie	k1gFnSc1	Marie
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Domino	domino	k1gNnSc1	domino
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7303-099-3	[number]	k4	80-7303-099-3
ERICKSON	ERICKSON	kA	ERICKSON
Carolly	Carolla	k1gFnSc2	Carolla
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
Boleynová	Boleynová	k1gFnSc1	Boleynová
<g/>
,	,	kIx,	,
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Domino	domino	k1gNnSc1	domino
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7303-194-9	[number]	k4	80-7303-194-9
Jindřich	Jindřich	k1gMnSc1	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
orig	orig	k1gInSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
Henry	Henry	k1gMnSc1	Henry
VIII	VIII	kA	VIII
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Historický	historický	k2eAgInSc1d1	historický
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
250	[number]	k4	250
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
Tudorovci	Tudorovec	k1gMnPc1	Tudorovec
-	-	kIx~	-
seriál	seriál	k1gInSc1	seriál
-	-	kIx~	-
koprodukční	koprodukční	k2eAgFnSc1d1	koprodukční
2007	[number]	k4	2007
Králova	Králův	k2eAgFnSc1d1	Králova
přízeň	přízeň	k1gFnSc1	přízeň
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Other	Other	k1gMnSc1	Other
Boleyn	Boleyn	k1gMnSc1	Boleyn
Girl	girl	k1gFnSc1	girl
<g/>
)	)	kIx)	)
-	-	kIx~	-
britský	britský	k2eAgInSc1d1	britský
film	film	k1gInSc1	film
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
románu	román	k1gInSc2	román
Philippy	Philippa	k1gFnSc2	Philippa
Gregory	Gregor	k1gMnPc4	Gregor
</s>
