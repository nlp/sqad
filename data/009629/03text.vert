<p>
<s>
Nachal	Nachal	k1gMnSc1	Nachal
Gdora	Gdora	k1gMnSc1	Gdora
nebo	nebo	k8xC	nebo
Nachal	Nachal	k1gMnSc1	Nachal
Gedora	Gedor	k1gMnSc2	Gedor
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
נ	נ	k?	נ
ג	ג	k?	ג
<g/>
,	,	kIx,	,
arabsky	arabsky	k6eAd1	arabsky
<g/>
:	:	kIx,	:
Fura	Fura	k1gFnSc1	Fura
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
krátké	krátký	k2eAgNnSc4d1	krátké
vádí	vádí	k1gNnSc4	vádí
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
Zebulunském	Zebulunský	k2eAgNnSc6d1	Zebulunský
údolí	údolí	k1gNnSc6	údolí
mezi	mezi	k7c7	mezi
městy	město	k1gNnPc7	město
Kirjat	Kirjat	k2eAgMnSc1d1	Kirjat
Bialik	Bialik	k1gMnSc1	Bialik
a	a	k8xC	a
Kirjat	Kirjat	k2eAgMnSc1d1	Kirjat
Ata	Ata	k1gMnSc1	Ata
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
odvodňuje	odvodňovat	k5eAaImIp3nS	odvodňovat
nížinatou	nížinatý	k2eAgFnSc4d1	nížinatá
jižní	jižní	k2eAgFnSc4d1	jižní
část	část	k1gFnSc4	část
tohoto	tento	k3xDgNnSc2	tento
údolí	údolí	k1gNnSc2	údolí
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
jihozápadu	jihozápad	k1gInSc3	jihozápad
<g/>
.	.	kIx.	.
</s>
<s>
Míjí	míjet	k5eAaImIp3nS	míjet
východní	východní	k2eAgInSc4d1	východní
okraj	okraj	k1gInSc4	okraj
zástavby	zástavba	k1gFnSc2	zástavba
Kirjat	Kirjat	k2eAgMnSc1d1	Kirjat
Bialik	Bialik	k1gMnSc1	Bialik
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c6	na
protějším	protější	k2eAgInSc6d1	protější
břehu	břeh	k1gInSc6	břeh
je	být	k5eAaImIp3nS	být
lemuje	lemovat	k5eAaImIp3nS	lemovat
volná	volný	k2eAgFnSc1d1	volná
zemědělsky	zemědělsky	k6eAd1	zemědělsky
využívaná	využívaný	k2eAgFnSc1d1	využívaná
krajina	krajina	k1gFnSc1	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
Haifského	haifský	k2eAgInSc2d1	haifský
přístavu	přístav	k1gInSc2	přístav
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ústí	ústit	k5eAaImIp3nS	ústit
zprava	zprava	k6eAd1	zprava
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Kišon	Kišon	k1gInSc1	Kišon
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
jejím	její	k3xOp3gInSc7	její
vtokem	vtok	k1gInSc7	vtok
do	do	k7c2	do
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
fragmentech	fragment	k1gInPc6	fragment
volné	volný	k2eAgFnSc2d1	volná
krajiny	krajina	k1gFnSc2	krajina
se	se	k3xPyFc4	se
podél	podél	k7c2	podél
toku	tok	k1gInSc2	tok
Nachal	Nachal	k1gMnSc1	Nachal
Gdora	Gdora	k1gMnSc1	Gdora
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
původní	původní	k2eAgFnSc1d1	původní
vegetace	vegetace	k1gFnSc1	vegetace
i	i	k8xC	i
drobná	drobný	k2eAgFnSc1d1	drobná
zvířena	zvířena	k1gFnSc1	zvířena
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
hnízdících	hnízdící	k2eAgMnPc2d1	hnízdící
ptáků	pták	k1gMnPc2	pták
jako	jako	k8xC	jako
slípka	slípka	k1gFnSc1	slípka
zelenonohá	zelenonohý	k2eAgFnSc1d1	zelenonohá
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
blízkosti	blízkost	k1gFnSc3	blízkost
hustě	hustě	k6eAd1	hustě
zalidněných	zalidněný	k2eAgFnPc2d1	zalidněná
oblastí	oblast	k1gFnPc2	oblast
aglomerace	aglomerace	k1gFnSc2	aglomerace
Haify	Haifa	k1gFnSc2	Haifa
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
potýká	potýkat	k5eAaImIp3nS	potýkat
se	s	k7c7	s
silným	silný	k2eAgNnSc7d1	silné
znečištěním	znečištění	k1gNnSc7	znečištění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2010	[number]	k4	2010
došlo	dojít	k5eAaPmAgNnS	dojít
při	při	k7c6	při
stavebních	stavební	k2eAgFnPc6d1	stavební
pracích	práce	k1gFnPc6	práce
poblíž	poblíž	k6eAd1	poblíž
Nachal	Nachal	k1gMnSc1	Nachal
Gdora	Gdora	k1gMnSc1	Gdora
k	k	k7c3	k
narušení	narušení	k1gNnSc3	narušení
hlavního	hlavní	k2eAgNnSc2d1	hlavní
kanalizačního	kanalizační	k2eAgNnSc2d1	kanalizační
potrubí	potrubí	k1gNnSc2	potrubí
a	a	k8xC	a
kontaminován	kontaminován	k2eAgMnSc1d1	kontaminován
byl	být	k5eAaImAgInS	být
i	i	k9	i
tok	tok	k1gInSc1	tok
řeky	řeka	k1gFnSc2	řeka
Kišon	Kišona	k1gFnPc2	Kišona
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přitom	přitom	k6eAd1	přitom
krátce	krátce	k6eAd1	krátce
předtím	předtím	k6eAd1	předtím
začínala	začínat	k5eAaImAgFnS	začínat
jevit	jevit	k5eAaImF	jevit
známky	známka	k1gFnPc4	známka
obnovy	obnova	k1gFnSc2	obnova
ekosystému	ekosystém	k1gInSc2	ekosystém
<g/>
,	,	kIx,	,
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
narušovaného	narušovaný	k2eAgInSc2d1	narušovaný
nečistotami	nečistota	k1gFnPc7	nečistota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
נ	נ	k?	נ
ג	ג	k?	ג
na	na	k7c6	na
hebrejské	hebrejský	k2eAgFnSc6d1	hebrejská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Krajot	Krajot	k1gMnSc1	Krajot
</s>
</p>
