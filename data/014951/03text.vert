<s>
Imaginární	imaginární	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
Jako	jako	k8xS,k8xC
imaginární	imaginární	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
matematice	matematika	k1gFnSc6
označuje	označovat	k5eAaImIp3nS
číslo	číslo	k1gNnSc1
značené	značený	k2eAgFnSc2d1
i	i	k8xC
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
též	též	k9
j	j	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
rozšiřuje	rozšiřovat	k5eAaImIp3nS
obor	obor	k1gInSc4
reálných	reálný	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
</s>
<s>
R	R	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathbb	mathbb	k1gMnSc1
{	{	kIx(
<g/>
R	R	kA
<g/>
}	}	kIx)
}	}	kIx)
</s>
<s>
na	na	k7c4
obor	obor	k1gInSc4
čísel	číslo	k1gNnPc2
komplexních	komplexní	k2eAgNnPc2d1
</s>
<s>
C	C	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathbb	mathbb	k1gMnSc1
{	{	kIx(
<g/>
C	C	kA
<g/>
}	}	kIx)
}	}	kIx)
</s>
<s>
Po	po	k7c6
tomto	tento	k3xDgInSc6
rozšíření	rozšíření	k1gNnSc6
existuje	existovat	k5eAaImIp3nS
řešení	řešení	k1gNnSc1
libovolné	libovolný	k2eAgFnSc2d1
polynomiální	polynomiální	k2eAgFnSc2d1
rovnice	rovnice	k1gFnSc2
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
)	)	kIx)
=	=	kIx~
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
reálných	reálný	k2eAgNnPc6d1
číslech	číslo	k1gNnPc6
některé	některý	k3yIgNnSc1
takové	takový	k3xDgFnPc1
rovnice	rovnice	k1gFnPc1
řešení	řešení	k1gNnSc2
nemají	mít	k5eNaImIp3nP
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
např.	např.	kA
rovnice	rovnice	k1gFnSc1
x²	x²	k?
+	+	kIx~
1	#num#	k4
=	=	kIx~
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
k	k	k7c3
množině	množina	k1gFnSc3
reálných	reálný	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
přidán	přidán	k2eAgInSc4d1
nový	nový	k2eAgInSc4d1
prvek	prvek	k1gInSc4
i	i	k9
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
tuto	tento	k3xDgFnSc4
rovnici	rovnice	k1gFnSc4
řeší	řešit	k5eAaImIp3nS
<g/>
,	,	kIx,
algebraickým	algebraický	k2eAgInSc7d1
uzávěrem	uzávěr	k1gInSc7
takto	takto	k6eAd1
vzniklé	vzniklý	k2eAgFnSc2d1
množiny	množina	k1gFnSc2
je	být	k5eAaImIp3nS
právě	právě	k6eAd1
množina	množina	k1gFnSc1
komplexních	komplexní	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgInPc6,k3yIgInPc6,k3yQgInPc6
má	mít	k5eAaImIp3nS
řešení	řešení	k1gNnPc1
už	už	k6eAd1
každá	každý	k3xTgFnSc1
polynomiální	polynomiální	k2eAgFnSc1d1
rovnice	rovnice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
oboru	obor	k1gInSc6
elektrotechniky	elektrotechnika	k1gFnSc2
je	být	k5eAaImIp3nS
často	často	k6eAd1
imaginární	imaginární	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
označována	označovat	k5eAaImNgFnS
jako	jako	k9
j	j	k?
místo	místo	k6eAd1
i	i	k9
<g/>
,	,	kIx,
protože	protože	k8xS
i	i	k9
se	se	k3xPyFc4
běžně	běžně	k6eAd1
používá	používat	k5eAaImIp3nS
pro	pro	k7c4
označení	označení	k1gNnSc4
okamžité	okamžitý	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
elektrického	elektrický	k2eAgInSc2d1
proudu	proud	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Definice	definice	k1gFnSc1
</s>
<s>
Podle	podle	k7c2
definice	definice	k1gFnSc2
imaginární	imaginární	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
i	i	k9
je	být	k5eAaImIp3nS
řešením	řešení	k1gNnSc7
rovnice	rovnice	k1gFnSc2
</s>
<s>
x	x	k?
<g/>
2	#num#	k4
=	=	kIx~
−	−	k?
</s>
<s>
Operace	operace	k1gFnSc1
s	s	k7c7
reálnými	reálný	k2eAgNnPc7d1
čísly	číslo	k1gNnPc7
lze	lze	k6eAd1
rozšířit	rozšířit	k5eAaPmF
na	na	k7c4
imaginární	imaginární	k2eAgNnSc4d1
a	a	k8xC
komplexní	komplexní	k2eAgNnSc4d1
čísla	číslo	k1gNnPc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
při	při	k7c6
manipulaci	manipulace	k1gFnSc6
s	s	k7c7
výrazem	výraz	k1gInSc7
zacházíme	zacházet	k5eAaImIp1nP
s	s	k7c7
i	i	k8xC
jako	jako	k9
s	s	k7c7
neznámou	známý	k2eNgFnSc7d1
veličinou	veličina	k1gFnSc7
a	a	k8xC
použijeme	použít	k5eAaPmIp1nP
tuto	tento	k3xDgFnSc4
definici	definice	k1gFnSc4
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
abychom	aby	kYmCp1nP
nahradili	nahradit	k5eAaPmAgMnP
všechny	všechen	k3xTgInPc4
výskyty	výskyt	k1gInPc4
i	i	k9
<g/>
2	#num#	k4
číslem	číslo	k1gNnSc7
−	−	k?
<g/>
.	.	kIx.
</s>
<s>
i	i	k9
a	a	k8xC
−	−	k?
<g/>
i	i	k8xC
</s>
<s>
Výše	vysoce	k6eAd2
uvedená	uvedený	k2eAgFnSc1d1
rovnice	rovnice	k1gFnSc1
má	mít	k5eAaImIp3nS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
dvě	dva	k4xCgNnPc4
různá	různý	k2eAgNnPc4d1
řešení	řešení	k1gNnPc4
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
jsou	být	k5eAaImIp3nP
aditivně	aditivně	k6eAd1
inverzní	inverzní	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesněji	přesně	k6eAd2
<g/>
,	,	kIx,
pokud	pokud	k8xS
řekneme	říct	k5eAaPmIp1nP
<g/>
,	,	kIx,
že	že	k8xS
řešením	řešení	k1gNnSc7
rovnice	rovnice	k1gFnSc1
je	být	k5eAaImIp3nS
i	i	k9
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
také	také	k9
řešením	řešení	k1gNnSc7
této	tento	k3xDgFnSc2
rovnice	rovnice	k1gFnSc2
−	−	k?
<g/>
i	i	k9
(	(	kIx(
<g/>
≠	≠	k?
i	i	k9
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
výše	vysoce	k6eAd2
uvedená	uvedený	k2eAgFnSc1d1
rovnice	rovnice	k1gFnSc1
je	být	k5eAaImIp3nS
jedinou	jediný	k2eAgFnSc7d1
definicí	definice	k1gFnSc7
i	i	k9
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zřejmé	zřejmý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
tato	tento	k3xDgFnSc1
definice	definice	k1gFnSc1
je	být	k5eAaImIp3nS
nejednoznačná	jednoznačný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
nejednoznačnost	nejednoznačnost	k1gFnSc4
odstraníme	odstranit	k5eAaPmIp1nP
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
vybereme	vybrat	k5eAaPmIp1nP
a	a	k8xC
zafixujeme	zafixovat	k5eAaPmIp1nP
jako	jako	k9
řešení	řešení	k1gNnSc4
výše	výše	k1gFnSc2,k1gFnSc2wB
uvedené	uvedený	k2eAgFnSc2d1
rovnice	rovnice	k1gFnSc2
„	„	k?
<g/>
pozitivní	pozitivní	k2eAgInSc4d1
i	i	k8xC
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Upozornění	upozornění	k1gNnSc1
</s>
<s>
Imaginární	imaginární	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
se	se	k3xPyFc4
někdy	někdy	k6eAd1
zapisuje	zapisovat	k5eAaImIp3nS
jako	jako	k9
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	kA
{	{	kIx(
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
}}}	}}}	kIx)
</s>
<s>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
dát	dát	k5eAaPmF
pozor	pozor	k1gInSc4
při	při	k7c6
manipulaci	manipulace	k1gFnSc6
s	s	k7c7
těmito	tento	k3xDgFnPc7
odmocninami	odmocnina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
aplikaci	aplikace	k1gFnSc6
pravidel	pravidlo	k1gNnPc2
platících	platící	k2eAgMnPc2d1
pro	pro	k7c4
odmocniny	odmocnina	k1gFnPc4
z	z	k7c2
kladných	kladný	k2eAgNnPc2d1
reálných	reálný	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
na	na	k7c4
celý	celý	k2eAgInSc4d1
obor	obor	k1gInSc4
reálných	reálný	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
můžeme	moct	k5eAaImIp1nP
dostat	dostat	k5eAaPmF
špatný	špatný	k2eAgInSc4d1
výsledek	výsledek	k1gInSc4
:	:	kIx,
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
i	i	k9
</s>
<s>
⋅	⋅	k?
</s>
<s>
i	i	k9
</s>
<s>
=	=	kIx~
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
(	(	kIx(
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
⋅	⋅	k?
</s>
<s>
(	(	kIx(
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
-1	-1	k4
<g/>
=	=	kIx~
<g/>
i	i	k8xC
<g/>
\	\	kIx~
<g/>
cdot	cdot	k2eAgMnSc1d1
i	i	k9
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k5eAaPmF
{	{	kIx(
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
cdot	cdot	k5eAaPmF
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k5eAaPmF
{	{	kIx(
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k5eAaPmF
{	{	kIx(
<g/>
(	(	kIx(
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
cdot	cdot	k5eAaPmF
(	(	kIx(
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
Kalkulační	kalkulační	k2eAgNnSc1d1
pravidlo	pravidlo	k1gNnSc1
</s>
<s>
a	a	k8xC
</s>
<s>
⋅	⋅	k?
</s>
<s>
b	b	k?
</s>
<s>
=	=	kIx~
</s>
<s>
a	a	k8xC
</s>
<s>
⋅	⋅	k?
</s>
<s>
b	b	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
a	a	k8xC
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
cdot	cdot	k5eAaPmF
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
b	b	k?
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
a	a	k8xC
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
b	b	k?
<g/>
}}}	}}}	k?
</s>
<s>
je	být	k5eAaImIp3nS
v	v	k7c6
oboru	obor	k1gInSc6
reálných	reálný	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
platné	platný	k2eAgInPc1d1
<g/>
,	,	kIx,
pokud	pokud	k8xS
a	a	k8xC
≥	≥	k?
0	#num#	k4
nebo	nebo	k8xC
b	b	k?
≥	≥	k?
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nemůžeme	moct	k5eNaImIp1nP
ho	on	k3xPp3gMnSc4
tedy	tedy	k9
použít	použít	k5eAaPmF
<g/>
,	,	kIx,
pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
obě	dva	k4xCgNnPc1
čísla	číslo	k1gNnPc1
záporná	záporný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP
ho	on	k3xPp3gInSc4
však	však	k9
použít	použít	k5eAaPmF
pro	pro	k7c4
výpočet	výpočet	k1gInSc4
odmocniny	odmocnina	k1gFnSc2
ze	z	k7c2
záporného	záporný	k2eAgNnSc2d1
čísla	číslo	k1gNnSc2
<g/>
,	,	kIx,
např.	např.	kA
druhou	druhý	k4xOgFnSc4
odmocninu	odmocnina	k1gFnSc4
z	z	k7c2
čísla	číslo	k1gNnSc2
-4	-4	k4
vypočteme	vypočíst	k5eAaPmIp1nP
jako	jako	k9
<g/>
:	:	kIx,
</s>
<s>
−	−	k?
</s>
<s>
4	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
(	(	kIx(
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
i	i	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k5eAaPmF
{	{	kIx(
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
\	\	kIx~
<g/>
cdot	cdot	k5eAaPmF
(	(	kIx(
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
cdot	cdot	k5eAaPmF
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k5eAaPmF
{	{	kIx(
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
2	#num#	k4
<g/>
i	i	k8xC
<g/>
}	}	kIx)
</s>
<s>
Abychom	aby	kYmCp1nP
se	se	k3xPyFc4
vyhnuli	vyhnout	k5eAaPmAgMnP
chybám	chyba	k1gFnPc3
při	při	k7c6
manipulaci	manipulace	k1gFnSc6
s	s	k7c7
komplexními	komplexní	k2eAgNnPc7d1
čísly	číslo	k1gNnPc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
lépe	dobře	k6eAd2
nepoužívat	používat	k5eNaImF
záporná	záporný	k2eAgNnPc4d1
čísla	číslo	k1gNnPc4
pod	pod	k7c7
odmocninou	odmocnina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Mocniny	mocnina	k1gFnPc1
i	i	k9
</s>
<s>
Mocniny	mocnina	k1gFnPc1
i	i	k8xC
se	se	k3xPyFc4
cyklicky	cyklicky	k6eAd1
opakují	opakovat	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
i	i	k9
</s>
<s>
0	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
i	i	k9
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
i	i	k9
</s>
<s>
1	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
i	i	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
i	i	k9
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
i	i	k9
<g/>
}	}	kIx)
</s>
<s>
i	i	k9
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
i	i	k9
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
i	i	k9
</s>
<s>
3	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
−	−	k?
</s>
<s>
i	i	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
i	i	k9
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
-i	-i	k?
<g/>
}	}	kIx)
</s>
<s>
i	i	k9
</s>
<s>
4	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
i	i	k9
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
i	i	k9
</s>
<s>
5	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
i	i	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
i	i	k9
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
5	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
i	i	k9
<g/>
}	}	kIx)
</s>
<s>
i	i	k9
</s>
<s>
6	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
i	i	k9
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
6	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
To	ten	k3xDgNnSc1
lze	lze	k6eAd1
vyjádřit	vyjádřit	k5eAaPmF
matematickým	matematický	k2eAgInSc7d1
vzorcem	vzorec	k1gInSc7
<g/>
,	,	kIx,
kde	kde	k9
n	n	k0
je	být	k5eAaImIp3nS
libovolné	libovolný	k2eAgNnSc4d1
celé	celý	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
<g/>
:	:	kIx,
</s>
<s>
i	i	k9
</s>
<s>
4	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
i	i	k9
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
i	i	k9
</s>
<s>
4	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
i	i	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
i	i	k9
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
i	i	k9
<g/>
}	}	kIx)
</s>
<s>
i	i	k9
</s>
<s>
4	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
i	i	k9
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
i	i	k9
</s>
<s>
4	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
3	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
−	−	k?
</s>
<s>
i	i	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
i	i	k9
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
-i	-i	k?
<g/>
}	}	kIx)
</s>
<s>
i	i	k9
a	a	k8xC
Eulerův	Eulerův	k2eAgInSc4d1
vzorec	vzorec	k1gInSc4
</s>
<s>
Vezmeme	vzít	k5eAaPmIp1nP
Eulerův	Eulerův	k2eAgInSc4d1
vzorec	vzorec	k1gInSc4
</s>
<s>
e	e	k0
</s>
<s>
i	i	k9
</s>
<s>
x	x	k?
</s>
<s>
=	=	kIx~
</s>
<s>
cos	cos	k3yInSc1
</s>
<s>
x	x	k?
</s>
<s>
+	+	kIx~
</s>
<s>
i	i	k9
</s>
<s>
sin	sin	kA
</s>
<s>
x	x	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
e	e	k0
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
ix	ix	k?
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
cos	cos	kA
x	x	k?
<g/>
+	+	kIx~
<g/>
i	i	k9
<g/>
\	\	kIx~
<g/>
sin	sin	kA
x	x	k?
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
a	a	k8xC
po	po	k7c6
dosazení	dosazení	k1gNnSc6
</s>
<s>
π	π	k?
</s>
<s>
/	/	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
/	/	kIx~
<g/>
2	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
za	za	k7c4
</s>
<s>
x	x	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
x	x	k?
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
dostaneme	dostat	k5eAaPmIp1nP
</s>
<s>
e	e	k0
</s>
<s>
i	i	k9
</s>
<s>
π	π	k?
</s>
<s>
/	/	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
i	i	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
e	e	k0
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
i	i	k8xC
<g/>
\	\	kIx~
<g/>
pi	pi	k0
/	/	kIx~
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
i	i	k9
<g/>
}	}	kIx)
</s>
<s>
Jestliže	jestliže	k8xS
obě	dva	k4xCgFnPc4
strany	strana	k1gFnPc4
umocníme	umocnit	k5eAaPmIp1nP
na	na	k7c6
i	i	k9
<g/>
,	,	kIx,
a	a	k8xC
využijeme	využít	k5eAaPmIp1nP
</s>
<s>
i	i	k9
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
i	i	k9
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
získáme	získat	k5eAaPmIp1nP
následující	následující	k2eAgFnSc4d1
rovnost	rovnost	k1gFnSc4
<g/>
:	:	kIx,
</s>
<s>
i	i	k9
</s>
<s>
i	i	k9
</s>
<s>
=	=	kIx~
</s>
<s>
e	e	k0
</s>
<s>
−	−	k?
</s>
<s>
π	π	k?
</s>
<s>
/	/	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
0,207	0,207	k4
</s>
<s>
8795763	#num#	k4
</s>
<s>
…	…	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
i	i	k9
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
i	i	k9
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
e	e	k0
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
-	-	kIx~
<g/>
\	\	kIx~
<g/>
pi	pi	k0
/	/	kIx~
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
{	{	kIx(
<g/>
,	,	kIx,
<g/>
}	}	kIx)
<g/>
2078795763	#num#	k4
<g/>
\	\	kIx~
<g/>
dots	dots	k6eAd1
}	}	kIx)
</s>
<s>
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
je	být	k5eAaImIp3nS
snadné	snadný	k2eAgNnSc1d1
určit	určit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
</s>
<s>
i	i	k9
</s>
<s>
i	i	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
i	i	k9
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
i	i	k9
<g/>
}}	}}	k?
</s>
<s>
má	mít	k5eAaImIp3nS
nekonečný	konečný	k2eNgInSc1d1
počet	počet	k1gInSc1
řešení	řešení	k1gNnPc2
ve	v	k7c6
tvaru	tvar	k1gInSc6
</s>
<s>
i	i	k9
</s>
<s>
i	i	k9
</s>
<s>
=	=	kIx~
</s>
<s>
e	e	k0
</s>
<s>
−	−	k?
</s>
<s>
π	π	k?
</s>
<s>
/	/	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
N	N	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
i	i	k9
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
i	i	k9
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
e	e	k0
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
-	-	kIx~
<g/>
\	\	kIx~
<g/>
pi	pi	k0
/	/	kIx~
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
2	#num#	k4
<g/>
\	\	kIx~
<g/>
pi	pi	k0
N	N	kA
<g/>
}}	}}	k?
</s>
<s>
Z	z	k7c2
výše	výše	k1gFnSc2,k1gFnSc2wB
uvedené	uvedený	k2eAgFnSc2d1
identity	identita	k1gFnSc2
</s>
<s>
e	e	k0
</s>
<s>
i	i	k9
</s>
<s>
π	π	k?
</s>
<s>
/	/	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
i	i	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
e	e	k0
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
i	i	k8xC
<g/>
\	\	kIx~
<g/>
pi	pi	k0
/	/	kIx~
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
i	i	k9
<g/>
}	}	kIx)
</s>
<s>
lze	lze	k6eAd1
odvodit	odvodit	k5eAaPmF
Eulerovu	Eulerův	k2eAgFnSc4d1
identitu	identita	k1gFnSc4
</s>
<s>
e	e	k0
</s>
<s>
i	i	k9
</s>
<s>
π	π	k?
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
e	e	k0
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
i	i	k8xC
<g/>
\	\	kIx~
<g/>
pi	pi	k0
}	}	kIx)
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
</s>
<s>
V	v	k7c6
Gaussově	Gaussův	k2eAgFnSc6d1
rovině	rovina	k1gFnSc6
imaginární	imaginární	k2eAgFnSc4d1
jednotku	jednotka	k1gFnSc4
představuje	představovat	k5eAaImIp3nS
číslo	číslo	k1gNnSc1
[	[	kIx(
<g/>
0	#num#	k4
<g/>
;	;	kIx,
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Každé	každý	k3xTgNnSc4
komplexní	komplexní	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
lze	lze	k6eAd1
zapsat	zapsat	k5eAaPmF
(	(	kIx(
<g/>
v	v	k7c6
tzv.	tzv.	kA
algebraickém	algebraický	k2eAgInSc6d1
tvaru	tvar	k1gInSc6
<g/>
)	)	kIx)
ve	v	k7c6
tvaru	tvar	k1gInSc6
a	a	k8xC
+	+	kIx~
ib	ib	k?
<g/>
,	,	kIx,
kde	kde	k6eAd1
a	a	k8xC
a	a	k8xC
b	b	k?
jsou	být	k5eAaImIp3nP
reálná	reálný	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Komplexní	komplexní	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
</s>
<s>
Komplexní	komplexní	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
Hyperkomplexní	Hyperkomplexní	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Imaginární	imaginární	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
v	v	k7c6
encyklopedii	encyklopedie	k1gFnSc6
MathWorld	MathWorlda	k1gFnPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Matematika	matematika	k1gFnSc1
</s>
