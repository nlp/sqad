<s>
Nejrozšířenější	rozšířený	k2eAgMnPc1d3	nejrozšířenější
světově	světově	k6eAd1	světově
hranou	hrana	k1gFnSc7	hrana
RPG	RPG	kA	RPG
hrou	hra	k1gFnSc7	hra
na	na	k7c4	na
hrdiny	hrdina	k1gMnPc4	hrdina
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
Dungeons	Dungeons	k1gInSc1	Dungeons
&	&	k?	&
Dragons	Dragons	k1gInSc1	Dragons
<g/>
,	,	kIx,	,
v	v	k7c6	v
aktuální	aktuální	k2eAgFnSc6d1	aktuální
edici	edice	k1gFnSc6	edice
<g/>
:	:	kIx,	:
Dungeons	Dungeons	k1gInSc1	Dungeons
&	&	k?	&
Dragons	Dragons	k1gInSc1	Dragons
4	[number]	k4	4
<g/>
th	th	k?	th
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
původní	původní	k2eAgFnSc1d1	původní
trojici	trojice	k1gFnSc4	trojice
povolání	povolání	k1gNnSc2	povolání
(	(	kIx(	(
<g/>
válečník	válečník	k1gMnSc1	válečník
<g/>
/	/	kIx~	/
<g/>
lapka	lapka	k1gMnSc1	lapka
<g/>
/	/	kIx~	/
<g/>
kouzelník	kouzelník	k1gMnSc1	kouzelník
<g/>
/	/	kIx~	/
<g/>
klerik	klerik	k1gMnSc1	klerik
<g/>
)	)	kIx)	)
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
o	o	k7c4	o
spoustu	spousta	k1gFnSc4	spousta
dalších	další	k2eAgInPc2d1	další
a	a	k8xC	a
nabízí	nabízet	k5eAaImIp3nS	nabízet
přepracování	přepracování	k1gNnSc4	přepracování
a	a	k8xC	a
doplnění	doplnění	k1gNnSc4	doplnění
herních	herní	k2eAgInPc2d1	herní
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
