<s>
Objektově	objektově	k6eAd1
orientované	orientovaný	k2eAgNnSc1d1
programování	programování	k1gNnSc1
(	(	kIx(
<g/>
zkracováno	zkracován	k2eAgNnSc1d1
na	na	k7c6
OOP	OOP	kA
<g/>
,	,	kIx,
z	z	k7c2
anglického	anglický	k2eAgInSc2d1
Object-oriented	Object-oriented	k1gMnSc1
programming	programming	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
informatice	informatika	k1gFnSc6
specifické	specifický	k2eAgNnSc1d1
programovací	programovací	k2eAgNnSc1d1
paradigma	paradigma	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
ho	on	k3xPp3gMnSc4
odlišilo	odlišit	k5eAaPmAgNnS
od	od	k7c2
původního	původní	k2eAgInSc2d1
imperativního	imperativní	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>