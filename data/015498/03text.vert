<s>
Objektově	objektově	k6eAd1
orientované	orientovaný	k2eAgNnSc4d1
programování	programování	k1gNnSc4
</s>
<s>
Programovací	programovací	k2eAgNnPc1d1
paradigmata	paradigma	k1gNnPc1
</s>
<s>
Aspektově	aspektově	k6eAd1
orientované	orientovaný	k2eAgInPc1d1
</s>
<s>
Deklarativní	deklarativní	k2eAgInPc4d1
(	(	kIx(
<g/>
FunkcionálníLogické	FunkcionálníLogický	k2eAgInPc4d1
<g/>
)	)	kIx)
</s>
<s>
Function-level	Function-level	k1gMnSc1
</s>
<s>
Generické	generický	k2eAgNnSc1d1
</s>
<s>
Imperativní	imperativní	k2eAgFnSc1d1
</s>
<s>
Metaprogramování	Metaprogramování	k1gNnSc1
</s>
<s>
Paralelní	paralelní	k2eAgFnSc1d1
</s>
<s>
Programování	programování	k1gNnSc1
ve	v	k7c6
velkém	velký	k2eAgInSc6d1
</s>
<s>
Strukturované	strukturovaný	k2eAgNnSc1d1
(	(	kIx(
<g/>
Objektově	objektově	k6eAd1
orientovanéRekurzivní	orientovanéRekurzivnět	k5eAaPmIp3nS
<g/>
)	)	kIx)
</s>
<s>
z	z	k7c2
•	•	k?
d	d	k?
•	•	k?
e	e	k0
</s>
<s>
Objektově	objektově	k6eAd1
orientované	orientovaný	k2eAgNnSc1d1
programování	programování	k1gNnSc1
(	(	kIx(
<g/>
zkracováno	zkracován	k2eAgNnSc1d1
na	na	k7c6
OOP	OOP	kA
<g/>
,	,	kIx,
z	z	k7c2
anglického	anglický	k2eAgInSc2d1
Object-oriented	Object-oriented	k1gMnSc1
programming	programming	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
informatice	informatika	k1gFnSc6
specifické	specifický	k2eAgNnSc1d1
programovací	programovací	k2eAgNnSc1d1
paradigma	paradigma	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
ho	on	k3xPp3gMnSc4
odlišilo	odlišit	k5eAaPmAgNnS
od	od	k7c2
původního	původní	k2eAgInSc2d1
imperativního	imperativní	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výkonný	výkonný	k2eAgInSc1d1
kód	kód	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
objektovém	objektový	k2eAgInSc6d1
programování	programování	k1gNnSc2
přidružen	přidružit	k5eAaPmNgMnS
k	k	k7c3
datům	datum	k1gNnPc3
(	(	kIx(
<g/>
metody	metoda	k1gFnPc1
jsou	být	k5eAaImIp3nP
zapouzdřeny	zapouzdřit	k5eAaPmNgFnP
v	v	k7c6
objektech	objekt	k1gInPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
umožňuje	umožňovat	k5eAaImIp3nS
snadnější	snadný	k2eAgInSc1d2
přenos	přenos	k1gInSc1
kódu	kód	k1gInSc2
mezi	mezi	k7c7
různými	různý	k2eAgInPc7d1
projekty	projekt	k1gInPc7
(	(	kIx(
<g/>
abstrakce	abstrakce	k1gFnSc1
a	a	k8xC
zapouzdření	zapouzdření	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Propojení	propojení	k1gNnSc1
umožnilo	umožnit	k5eAaPmAgNnS
zavést	zavést	k5eAaPmF
dědičnost	dědičnost	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
kvůli	kvůli	k7c3
zjednodušení	zjednodušení	k1gNnSc3
si	se	k3xPyFc3
vyžádalo	vyžádat	k5eAaPmAgNnS
zavedení	zavedení	k1gNnSc1
polymorfismu	polymorfismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Koncepce	koncepce	k1gFnSc1
</s>
<s>
Objekty	objekt	k1gInPc1
–	–	k?
jednotlivé	jednotlivý	k2eAgInPc1d1
prvky	prvek	k1gInPc1
modelované	modelovaný	k2eAgFnSc2d1
reality	realita	k1gFnSc2
(	(	kIx(
<g/>
jak	jak	k8xC,k8xS
data	datum	k1gNnPc4
<g/>
,	,	kIx,
tak	tak	k6eAd1
související	související	k2eAgFnSc4d1
funkčnost	funkčnost	k1gFnSc4
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
v	v	k7c6
programu	program	k1gInSc6
seskupeny	seskupen	k2eAgInPc4d1
do	do	k7c2
entit	entita	k1gFnPc2
<g/>
,	,	kIx,
nazývaných	nazývaný	k2eAgInPc2d1
objekty	objekt	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objekty	objekt	k1gInPc1
si	se	k3xPyFc3
pamatují	pamatovat	k5eAaImIp3nP
svůj	svůj	k3xOyFgInSc4
stav	stav	k1gInSc4
a	a	k8xC
navenek	navenek	k6eAd1
poskytují	poskytovat	k5eAaImIp3nP
operace	operace	k1gFnPc4
(	(	kIx(
<g/>
přístupné	přístupný	k2eAgFnPc4d1
jako	jako	k8xC,k8xS
metody	metoda	k1gFnPc4
pro	pro	k7c4
volání	volání	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Abstrakce	abstrakce	k1gFnSc1
–	–	k?
programátor	programátor	k1gInSc1
<g/>
,	,	kIx,
potažmo	potažmo	k6eAd1
program	program	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
abstrahovat	abstrahovat	k5eAaBmF
od	od	k7c2
některých	některý	k3yIgInPc2
detailů	detail	k1gInPc2
práce	práce	k1gFnSc2
jednotlivých	jednotlivý	k2eAgInPc2d1
objektů	objekt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc1
objekt	objekt	k1gInSc4
pracuje	pracovat	k5eAaImIp3nS
jako	jako	k9
černá	černý	k2eAgFnSc1d1
skříňka	skříňka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
dokáže	dokázat	k5eAaPmIp3nS
provádět	provádět	k5eAaImF
určené	určený	k2eAgFnPc4d1
činnosti	činnost	k1gFnPc4
a	a	k8xC
komunikovat	komunikovat	k5eAaImF
s	s	k7c7
okolím	okolí	k1gNnSc7
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nS
vyžadovala	vyžadovat	k5eAaImAgFnS
znalost	znalost	k1gFnSc1
způsobu	způsob	k1gInSc2
<g/>
,	,	kIx,
kterým	který	k3yQgNnSc7,k3yIgNnSc7,k3yRgNnSc7
vnitřně	vnitřně	k6eAd1
pracuje	pracovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Zapouzdření	zapouzdření	k1gNnSc1
–	–	k?
zaručuje	zaručovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
objekt	objekt	k1gInSc1
nemůže	moct	k5eNaImIp3nS
přímo	přímo	k6eAd1
přistupovat	přistupovat	k5eAaImF
k	k	k7c3
„	„	k?
<g/>
vnitřnostem	vnitřnost	k1gFnPc3
<g/>
“	“	k?
jiných	jiný	k2eAgInPc2d1
objektů	objekt	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
by	by	kYmCp3nS
mohlo	moct	k5eAaImAgNnS
vést	vést	k5eAaImF
k	k	k7c3
nekonzistenci	nekonzistence	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc4
objekt	objekt	k1gInSc1
navenek	navenek	k6eAd1
zpřístupňuje	zpřístupňovat	k5eAaImIp3nS
rozhraní	rozhraní	k1gNnSc4
<g/>
,	,	kIx,
pomocí	pomocí	k7c2
kterého	který	k3yRgMnSc4,k3yIgMnSc4,k3yQgMnSc4
(	(	kIx(
<g/>
a	a	k8xC
nijak	nijak	k6eAd1
jinak	jinak	k6eAd1
<g/>
)	)	kIx)
se	s	k7c7
s	s	k7c7
objektem	objekt	k1gInSc7
pracuje	pracovat	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
k	k	k7c3
tomu	ten	k3xDgNnSc3
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
<g/>
:	:	kIx,
modifikátory	modifikátor	k1gInPc7
přístupu	přístup	k1gInSc2
<g/>
,	,	kIx,
jmenný	jmenný	k2eAgInSc4d1
prostor	prostor	k1gInSc4
<g/>
...	...	k?
</s>
<s>
Kompozice	kompozice	k1gFnSc1
–	–	k?
Objekt	objekt	k1gInSc1
může	moct	k5eAaImIp3nS
obsahovat	obsahovat	k5eAaImF
jiné	jiný	k2eAgInPc4d1
objekty	objekt	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Delegování	delegování	k1gNnSc4
–	–	k?
Objekt	objekt	k1gInSc1
může	moct	k5eAaImIp3nS
využívat	využívat	k5eAaPmF,k5eAaImF
služeb	služba	k1gFnPc2
jiných	jiný	k2eAgInPc2d1
objektů	objekt	k1gInPc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
je	on	k3xPp3gMnPc4
požádá	požádat	k5eAaPmIp3nS
o	o	k7c4
provedení	provedení	k1gNnSc4
operace	operace	k1gFnPc4
<g/>
,	,	kIx,
ty	ten	k3xDgFnPc4
tedy	tedy	k9
pro	pro	k7c4
okolí	okolí	k1gNnSc4
vystavují	vystavovat	k5eAaImIp3nP
své	svůj	k3xOyFgFnPc4
služby	služba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Dědičnost	dědičnost	k1gFnSc1
–	–	k?
objekty	objekt	k1gInPc1
jsou	být	k5eAaImIp3nP
organizovány	organizovat	k5eAaBmNgInP
stromovým	stromový	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
objekty	objekt	k1gInPc4
nějakého	nějaký	k3yIgInSc2
druhu	druh	k1gInSc2
mohou	moct	k5eAaImIp3nP
dědit	dědit	k5eAaImF
z	z	k7c2
jiného	jiný	k2eAgInSc2d1
druhu	druh	k1gInSc2
objektů	objekt	k1gInPc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
přebírají	přebírat	k5eAaImIp3nP
jejich	jejich	k3xOp3gNnPc4
schopnosti	schopnost	k1gFnPc4
<g/>
,	,	kIx,
ke	k	k7c3
kterým	který	k3yQgFnPc3,k3yIgFnPc3,k3yRgFnPc3
pouze	pouze	k6eAd1
přidávají	přidávat	k5eAaImIp3nP
svoje	svůj	k3xOyFgNnSc4
vlastní	vlastní	k2eAgNnSc4d1
rozšíření	rozšíření	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
myšlenka	myšlenka	k1gFnSc1
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
implementuje	implementovat	k5eAaImIp3nS
pomocí	pomocí	k7c2
rozdělení	rozdělení	k1gNnSc2
objektů	objekt	k1gInPc2
do	do	k7c2
tříd	třída	k1gFnPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
každý	každý	k3xTgInSc4
objekt	objekt	k1gInSc4
je	být	k5eAaImIp3nS
instancí	instance	k1gFnSc7
nějaké	nějaký	k3yIgFnSc2
třídy	třída	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
třída	třída	k1gFnSc1
pak	pak	k6eAd1
může	moct	k5eAaImIp3nS
dědit	dědit	k5eAaImF
od	od	k7c2
jiné	jiný	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
(	(	kIx(
<g/>
v	v	k7c6
některých	některý	k3yIgInPc6
programovacích	programovací	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
i	i	k9
z	z	k7c2
několika	několik	k4yIc2
jiných	jiný	k2eAgFnPc2d1
tříd	třída	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Polymorfismus	polymorfismus	k1gInSc1
–	–	k?
odkazovaný	odkazovaný	k2eAgInSc1d1
objekt	objekt	k1gInSc1
se	se	k3xPyFc4
chová	chovat	k5eAaImIp3nS
podle	podle	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
jaké	jaký	k3yQgFnSc2,k3yRgFnSc2,k3yIgFnSc2
třídy	třída	k1gFnSc2
je	být	k5eAaImIp3nS
instancí	instance	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozná	poznat	k5eAaPmIp3nS
se	se	k3xPyFc4
tak	tak	k9
<g/>
,	,	kIx,
že	že	k8xS
několik	několik	k4yIc1
objektů	objekt	k1gInPc2
poskytuje	poskytovat	k5eAaImIp3nS
stejné	stejný	k2eAgNnSc4d1
rozhraní	rozhraní	k1gNnSc4
<g/>
,	,	kIx,
pracuje	pracovat	k5eAaImIp3nS
se	se	k3xPyFc4
s	s	k7c7
nimi	on	k3xPp3gMnPc7
navenek	navenek	k6eAd1
stejným	stejný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
,	,	kIx,
ale	ale	k8xC
jejich	jejich	k3xOp3gNnPc1
konkrétní	konkrétní	k2eAgNnPc1d1
chování	chování	k1gNnPc1
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
podle	podle	k7c2
implementace	implementace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
polymorfismu	polymorfismus	k1gInSc2
podmíněného	podmíněný	k2eAgInSc2d1
dědičností	dědičnost	k1gFnSc7
to	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c4
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
očekávána	očekáván	k2eAgFnSc1d1
instance	instance	k1gFnSc1
nějaké	nějaký	k3yIgFnSc2
třídy	třída	k1gFnSc2
<g/>
,	,	kIx,
můžeme	moct	k5eAaImIp1nP
dosadit	dosadit	k5eAaPmF
i	i	k9
instanci	instance	k1gFnSc4
libovolné	libovolný	k2eAgFnSc2d1
její	její	k3xOp3gNnPc4
podtřídy	podtřída	k1gFnPc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
rozhraní	rozhraní	k1gNnSc1
třídy	třída	k1gFnSc2
je	být	k5eAaImIp3nS
podmnožinou	podmnožina	k1gFnSc7
rozhraní	rozhraní	k1gNnSc2
podtřídy	podtřída	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
polymorfismu	polymorfismus	k1gInSc2
nepodmíněného	podmíněný	k2eNgInSc2d1
dědičností	dědičnost	k1gFnSc7
je	být	k5eAaImIp3nS
dostačující	dostačující	k2eAgNnSc1d1
<g/>
,	,	kIx,
jestliže	jestliže	k8xS
se	se	k3xPyFc4
rozhraní	rozhraní	k1gNnSc1
(	(	kIx(
<g/>
nebo	nebo	k8xC
jejich	jejich	k3xOp3gFnPc4
požadované	požadovaný	k2eAgFnPc4d1
části	část	k1gFnPc4
<g/>
)	)	kIx)
u	u	k7c2
různých	různý	k2eAgFnPc2d1
tříd	třída	k1gFnPc2
shodují	shodovat	k5eAaImIp3nP
<g/>
,	,	kIx,
pak	pak	k6eAd1
jsou	být	k5eAaImIp3nP
vzájemně	vzájemně	k6eAd1
polymorfní	polymorfní	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Některé	některý	k3yIgFnPc1
z	z	k7c2
těchto	tento	k3xDgFnPc2
vlastností	vlastnost	k1gFnPc2
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
OOP	OOP	kA
unikátní	unikátní	k2eAgFnPc1d1
<g/>
,	,	kIx,
jiné	jiný	k2eAgFnPc1d1
(	(	kIx(
<g/>
např.	např.	kA
abstrakce	abstrakce	k1gFnSc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
běžnou	běžný	k2eAgFnSc7d1
vlastností	vlastnost	k1gFnSc7
i	i	k8xC
jiných	jiný	k2eAgFnPc2d1
vývojových	vývojový	k2eAgFnPc2d1
metodik	metodika	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
OOP	OOP	kA
je	být	k5eAaImIp3nS
někdy	někdy	k6eAd1
označováno	označovat	k5eAaImNgNnS
jako	jako	k8xS,k8xC
programátorské	programátorský	k2eAgNnSc1d1
paradigma	paradigma	k1gNnSc1
<g/>
,	,	kIx,
neboť	neboť	k8xC
popisuje	popisovat	k5eAaImIp3nS
nejen	nejen	k6eAd1
způsob	způsob	k1gInSc4
vývoje	vývoj	k1gInSc2
a	a	k8xC
zápisu	zápis	k1gInSc2
programu	program	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
způsob	způsob	k1gInSc1
<g/>
,	,	kIx,
jakým	jaký	k3yIgFnPc3,k3yQgFnPc3,k3yRgFnPc3
návrhář	návrhář	k1gMnSc1
programu	program	k1gInSc2
o	o	k7c6
problému	problém	k1gInSc6
přemýšlí	přemýšlet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Vztah	vztah	k1gInSc1
k	k	k7c3
principům	princip	k1gInPc3
a	a	k8xC
fungování	fungování	k1gNnSc3
reálného	reálný	k2eAgInSc2d1
světa	svět	k1gInSc2
</s>
<s>
Základním	základní	k2eAgNnSc7d1
paradigmatem	paradigma	k1gNnSc7
OOP	OOP	kA
je	být	k5eAaImIp3nS
snaha	snaha	k1gFnSc1
modelovat	modelovat	k5eAaImF
při	při	k7c6
řešení	řešení	k1gNnSc6
úloh	úloha	k1gFnPc2
principy	princip	k1gInPc4
reálného	reálný	k2eAgInSc2d1
světa	svět	k1gInSc2
v	v	k7c6
počítači	počítač	k1gInSc6
pokud	pokud	k8xS
možno	možno	k6eAd1
jedna	jeden	k4xCgFnSc1
ku	k	k7c3
jedné	jeden	k4xCgFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
praktickém	praktický	k2eAgInSc6d1
životě	život	k1gInSc6
otevíráme	otevírat	k5eAaImIp1nP
dveře	dveře	k1gFnPc1
pořád	pořád	k6eAd1
stejně	stejně	k6eAd1
<g/>
,	,	kIx,
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
zda	zda	k8xS
jsou	být	k5eAaImIp3nP
dřevěné	dřevěný	k2eAgInPc1d1
nebo	nebo	k8xC
laminované	laminovaný	k2eAgInPc1d1
<g/>
,	,	kIx,
zda	zda	k8xS
mají	mít	k5eAaImIp3nP
kukátko	kukátko	k1gNnSc4
<g/>
,	,	kIx,
bezpečnostní	bezpečnostní	k2eAgFnSc4d1
vložku	vložka	k1gFnSc4
nebo	nebo	k8xC
řetízek	řetízek	k1gInSc4
navíc	navíc	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
tak	tak	k6eAd1
se	se	k3xPyFc4
můžeme	moct	k5eAaImIp1nP
dívat	dívat	k5eAaImF
na	na	k7c4
televizi	televize	k1gFnSc4
<g/>
,	,	kIx,
přepínat	přepínat	k5eAaImF
programy	program	k1gInPc4
a	a	k8xC
docela	docela	k6eAd1
dobře	dobře	k6eAd1
ji	on	k3xPp3gFnSc4
ovládat	ovládat	k5eAaImF
<g/>
,	,	kIx,
přesto	přesto	k8xC
že	že	k8xS
nevíme	vědět	k5eNaImIp1nP
vůbec	vůbec	k9
nic	nic	k3yNnSc1
o	o	k7c6
principech	princip	k1gInPc6
jejího	její	k3xOp3gNnSc2
fungování	fungování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analogicky	analogicky	k6eAd1
při	při	k7c6
vývoji	vývoj	k1gInSc6
složitých	složitý	k2eAgInPc2d1
informačních	informační	k2eAgInPc2d1
systémů	systém	k1gInPc2
mohou	moct	k5eAaImIp3nP
vývojáři	vývojář	k1gMnPc1
používat	používat	k5eAaImF
již	již	k6eAd1
vytvořené	vytvořený	k2eAgInPc4d1
komponenty	komponent	k1gInPc4
<g/>
,	,	kIx,
podle	podle	k7c2
potřeby	potřeba	k1gFnSc2
si	se	k3xPyFc3
je	být	k5eAaImIp3nS
trochu	trochu	k6eAd1
upravit	upravit	k5eAaPmF
nebo	nebo	k8xC
je	být	k5eAaImIp3nS
používat	používat	k5eAaImF
jako	jako	k9
stavebnici	stavebnice	k1gFnSc4
pro	pro	k7c4
sestavování	sestavování	k1gNnSc4
důmyslnějších	důmyslný	k2eAgInPc2d2
a	a	k8xC
složitějších	složitý	k2eAgInPc2d2
objektů	objekt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Programovací	programovací	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
</s>
<s>
Existuje	existovat	k5eAaImIp3nS
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
programovacích	programovací	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
umožňujících	umožňující	k2eAgInPc2d1
objektově	objektově	k6eAd1
orientované	orientovaný	k2eAgNnSc4d1
programování	programování	k1gNnSc4
<g/>
,	,	kIx,
např.	např.	kA
Perl	perl	k1gInSc1
<g/>
,	,	kIx,
Smalltalk	Smalltalk	k1gMnSc1
<g/>
,	,	kIx,
Java	Java	k1gMnSc1
<g/>
,	,	kIx,
C	C	kA
<g/>
++	++	k?
<g/>
,	,	kIx,
Object	Object	k2eAgInSc1d1
Pascal	pascal	k1gInSc1
<g/>
,	,	kIx,
C	C	kA
<g/>
#	#	kIx~
<g/>
,	,	kIx,
Visual	Visual	k1gMnSc1
Basic	Basic	kA
.	.	kIx.
<g/>
NET	NET	kA
<g/>
,	,	kIx,
Lisp	Lisp	k1gMnSc1
<g/>
,	,	kIx,
PHP	PHP	kA
<g/>
,	,	kIx,
Python	Python	k1gMnSc1
<g/>
,	,	kIx,
Ruby	rub	k1gInPc1
<g/>
,	,	kIx,
Go	Go	k1gFnPc1
<g/>
,	,	kIx,
Rust	Rust	k1gInSc1
<g/>
,	,	kIx,
D	D	kA
<g/>
…	…	k?
</s>
<s>
Tyto	tento	k3xDgInPc1
jazyky	jazyk	k1gInPc1
můžeme	moct	k5eAaImIp1nP
rozčlenit	rozčlenit	k5eAaPmF
do	do	k7c2
mnoha	mnoho	k4c2
skupin	skupina	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
např.	např.	kA
</s>
<s>
Tzv.	tzv.	kA
čistě	čistě	k6eAd1
objektové	objektový	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
nazývané	nazývaný	k2eAgInPc1d1
objektové	objektový	k2eAgInPc1d1
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgInPc6,k3yIgInPc6,k3yQgInPc6
výpočet	výpočet	k1gInSc1
probíhá	probíhat	k5eAaImIp3nS
výhradně	výhradně	k6eAd1
interakcí	interakce	k1gFnSc7
objektů	objekt	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
vzájemným	vzájemný	k2eAgNnSc7d1
zasíláním	zasílání	k1gNnSc7
zpráv	zpráva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sem	sem	k6eAd1
patří	patřit	k5eAaImIp3nS
např.	např.	kA
Smalltalk	Smalltalk	k1gInSc4
a	a	k8xC
Ruby	rub	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Tzv.	tzv.	kA
hybridní	hybridní	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
také	také	k9
objektově	objektově	k6eAd1
orientované	orientovaný	k2eAgNnSc1d1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
vytvořeny	vytvořit	k5eAaPmNgFnP
na	na	k7c6
imperativním	imperativní	k2eAgNnSc6d1
programování	programování	k1gNnSc6
a	a	k8xC
obvykle	obvykle	k6eAd1
pouze	pouze	k6eAd1
částečně	částečně	k6eAd1
implementují	implementovat	k5eAaImIp3nP
vlastnosti	vlastnost	k1gFnPc4
objektového	objektový	k2eAgNnSc2d1
programování	programování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
takové	takový	k3xDgMnPc4
jazyky	jazyk	k1gMnPc4
patří	patřit	k5eAaImIp3nS
např.	např.	kA
C	C	kA
<g/>
++	++	k?
<g/>
,	,	kIx,
Go	Go	k1gMnSc1
<g/>
,	,	kIx,
Rust	Rust	k1gMnSc1
a	a	k8xC
D.	D.	kA
</s>
<s>
Objektová	objektový	k2eAgNnPc1d1
rozšíření	rozšíření	k1gNnPc1
původně	původně	k6eAd1
neobjektových	objektový	k2eNgMnPc2d1
jazyků	jazyk	k1gInPc2
<g/>
,	,	kIx,
např.	např.	kA
Object	Object	k1gMnSc1
Pascal	Pascal	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
vznikl	vzniknout	k5eAaPmAgInS
jako	jako	k9
rozšíření	rozšíření	k1gNnSc4
neobjektového	objektový	k2eNgInSc2d1
jazyka	jazyk	k1gInSc2
Pascal	pascal	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Teorie	teorie	k1gFnSc1
objektů	objekt	k1gInPc2
(	(	kIx(
<g/>
object	object	k1gInSc1
theory	theora	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Kromě	kromě	k7c2
vlastních	vlastní	k2eAgFnPc2d1
implementací	implementace	k1gFnPc2
objektů	objekt	k1gInPc2
v	v	k7c6
nějakém	nějaký	k3yIgInSc6
programovacím	programovací	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
<g/>
,	,	kIx,
existuje	existovat	k5eAaImIp3nS
i	i	k9
obecná	obecný	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
se	se	k3xPyFc4
objekty	objekt	k1gInPc1
a	a	k8xC
jejich	jejich	k3xOp3gFnPc7
vlastnostmi	vlastnost	k1gFnPc7
zabývá	zabývat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Třída	třída	k1gFnSc1
je	být	k5eAaImIp3nS
základním	základní	k2eAgInSc7d1
obecným	obecný	k2eAgInSc7d1
pojmem	pojem	k1gInSc7
klasifikace	klasifikace	k1gFnSc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
při	při	k7c6
návrhu	návrh	k1gInSc6
uspořádávat	uspořádávat	k5eAaImF
informace	informace	k1gFnPc4
do	do	k7c2
smysluplné	smysluplný	k2eAgFnSc2d1
entity	entita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základním	základní	k2eAgInSc7d1
pojmem	pojem	k1gInSc7
je	být	k5eAaImIp3nS
objekt	objekt	k1gInSc1
<g/>
,	,	kIx,
instance	instance	k1gFnSc1
třídy	třída	k1gFnSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
konkrétní	konkrétní	k2eAgInSc4d1
případ	případ	k1gInSc4
realizace	realizace	k1gFnSc2
předpisu	předpis	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objekt	objekt	k1gInSc1
si	se	k3xPyFc3
„	„	k?
<g/>
pamatuje	pamatovat	k5eAaImIp3nS
<g/>
“	“	k?
svůj	svůj	k3xOyFgInSc4
stav	stav	k1gInSc4
(	(	kIx(
<g/>
v	v	k7c6
podobě	podoba	k1gFnSc6
dat	datum	k1gNnPc2
čili	čili	k8xC
atributů	atribut	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
zveřejněním	zveřejnění	k1gNnSc7
některých	některý	k3yIgFnPc2
svých	svůj	k3xOyFgFnPc2
operací	operace	k1gFnPc2
(	(	kIx(
<g/>
nazývaných	nazývaný	k2eAgFnPc2d1
metody	metoda	k1gFnPc1
<g/>
)	)	kIx)
poskytuje	poskytovat	k5eAaImIp3nS
rozhraní	rozhraní	k1gNnSc1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
s	s	k7c7
ním	on	k3xPp3gInSc7
pracovat	pracovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
používání	používání	k1gNnSc6
objektu	objekt	k1gInSc2
nás	my	k3xPp1nPc4
zajímá	zajímat	k5eAaImIp3nS
<g/>
,	,	kIx,
jaké	jaký	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
operace	operace	k1gFnPc4
(	(	kIx(
<g/>
služby	služba	k1gFnPc4
<g/>
)	)	kIx)
poskytuje	poskytovat	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
ne	ne	k9
<g/>
,	,	kIx,
jakým	jaký	k3yIgInSc7,k3yQgInSc7,k3yRgInSc7
způsobem	způsob	k1gInSc7
to	ten	k3xDgNnSc4
provádí	provádět	k5eAaImIp3nS
–	–	k?
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
princip	princip	k1gInSc1
zapouzdření	zapouzdření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jestli	jestli	k8xS
to	ten	k3xDgNnSc1
provádí	provádět	k5eAaImIp3nS
sám	sám	k3xTgInSc4
nebo	nebo	k8xC
využije	využít	k5eAaPmIp3nS
služeb	služba	k1gFnPc2
jiných	jiný	k2eAgInPc2d1
objektů	objekt	k1gInPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
celkem	celek	k1gInSc7
jedno	jeden	k4xCgNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastní	vlastní	k2eAgFnSc4d1
implementaci	implementace	k1gFnSc4
pak	pak	k6eAd1
můžeme	moct	k5eAaImIp1nP
změnit	změnit	k5eAaPmF
(	(	kIx(
<g/>
např.	např.	kA
zefektivnit	zefektivnit	k5eAaPmF
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
dotklo	dotknout	k5eAaPmAgNnS
všech	všecek	k3xTgMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
objekt	objekt	k1gInSc4
používají	používat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Abstrakce	abstrakce	k1gFnSc1
objektu	objekt	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
v	v	k7c6
architektuře	architektura	k1gFnSc6
programu	program	k1gInSc2
podchycuje	podchycovat	k5eAaImIp3nS
na	na	k7c6
obecné	obecný	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
podstatu	podstata	k1gFnSc4
všech	všecek	k3xTgInPc2
objektů	objekt	k1gInPc2
podobného	podobný	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
třída	třída	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třída	třída	k1gFnSc1
je	být	k5eAaImIp3nS
předpis	předpis	k1gInSc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
vyrobit	vyrobit	k5eAaPmF
objekt	objekt	k1gInSc4
daného	daný	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
sousedka	sousedka	k1gFnSc1
(	(	kIx(
<g/>
chápejme	chápat	k5eAaImRp1nP
ji	on	k3xPp3gFnSc4
jako	jako	k8xC,k8xS
objekt	objekt	k1gInSc4
<g/>
)	)	kIx)
má	mít	k5eAaImIp3nS
nějaké	nějaký	k3yIgNnSc4
jméno	jméno	k1gNnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nějak	nějak	k6eAd1
vysoká	vysoký	k2eAgFnSc1d1
<g/>
,	,	kIx,
umí	umět	k5eAaImIp3nS
chodit	chodit	k5eAaImF
a	a	k8xC
umí	umět	k5eAaImIp3nS
mluvit	mluvit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Totéž	týž	k3xTgNnSc1
platí	platit	k5eAaImIp3nS
i	i	k9
pro	pro	k7c4
mne	já	k3xPp1nSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohu	moct	k5eAaImIp1nS
tedy	tedy	k9
při	při	k7c6
modelování	modelování	k1gNnSc6
těchto	tento	k3xDgInPc2
dvou	dva	k4xCgInPc2
objektů	objekt	k1gInPc2
<g/>
,	,	kIx,
sousedky	sousedka	k1gFnSc2
a	a	k8xC
mě	já	k3xPp1nSc4
<g/>
,	,	kIx,
abstrahovat	abstrahovat	k5eAaBmF
od	od	k7c2
nepodstatných	podstatný	k2eNgFnPc2d1
dílčích	dílčí	k2eAgFnPc2d1
odlišností	odlišnost	k1gFnPc2
a	a	k8xC
díky	díky	k7c3
této	tento	k3xDgFnSc3
abstrakci	abstrakce	k1gFnSc3
vytvořit	vytvořit	k5eAaPmF
obecnou	obecný	k2eAgFnSc4d1
třídu	třída	k1gFnSc4
Člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
atributy	atribut	k1gInPc4
jméno	jméno	k1gNnSc1
a	a	k8xC
příjmení	příjmení	k1gNnSc1
(	(	kIx(
<g/>
obojí	oboj	k1gFnPc2
je	být	k5eAaImIp3nS
nějaký	nějaký	k3yIgInSc1
řetězec	řetězec	k1gInSc1
znaků	znak	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
metody	metoda	k1gFnPc4
chodit	chodit	k5eAaImF
a	a	k8xC
mluvit	mluvit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Dědičnost	dědičnost	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Dědičnost	dědičnost	k1gFnSc1
(	(	kIx(
<g/>
objektově	objektově	k6eAd1
orientované	orientovaný	k2eAgNnSc4d1
programování	programování	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
vytváření	vytváření	k1gNnSc6
objektů	objekt	k1gInPc2
pro	pro	k7c4
chod	chod	k1gInSc4
programu	program	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
budou	být	k5eAaImBp3nP
odpovídat	odpovídat	k5eAaImF
zmíněným	zmíněný	k2eAgNnSc7d1
dvěma	dva	k4xCgInPc3
reálným	reálný	k2eAgInPc3d1
objektům	objekt	k1gInPc3
<g/>
,	,	kIx,
je	on	k3xPp3gInPc4
třeba	třeba	k6eAd1
vytvořit	vytvořit	k5eAaPmF
dvě	dva	k4xCgFnPc4
různé	různý	k2eAgFnPc4d1
instance	instance	k1gFnPc4
třídy	třída	k1gFnSc2
Člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
pro	pro	k7c4
každý	každý	k3xTgInSc4
modelovaný	modelovaný	k2eAgInSc4d1
reálný	reálný	k2eAgInSc4d1
objekt	objekt	k1gInSc4
jednu	jeden	k4xCgFnSc4
instanci	instance	k1gFnSc4
(	(	kIx(
<g/>
tedy	tedy	k9
jedna	jeden	k4xCgFnSc1
instance	instance	k1gFnSc1
já	já	k3xPp1nSc1
a	a	k8xC
jedna	jeden	k4xCgFnSc1
sousedka	sousedka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodnoty	hodnota	k1gFnSc2
atributů	atribut	k1gInPc2
jméno	jméno	k1gNnSc1
a	a	k8xC
příjmení	příjmení	k1gNnSc1
se	se	k3xPyFc4
pochopitelně	pochopitelně	k6eAd1
v	v	k7c6
našem	náš	k3xOp1gInSc6
případě	případ	k1gInSc6
budou	být	k5eAaImBp3nP
lišit	lišit	k5eAaImF
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
obě	dva	k4xCgFnPc1
instance	instance	k1gFnPc1
jsou	být	k5eAaImIp3nP
schopny	schopen	k2eAgFnPc1d1
chodit	chodit	k5eAaImF
a	a	k8xC
mluvit	mluvit	k5eAaImF
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
jsme	být	k5eAaImIp1nP
tyto	tento	k3xDgFnPc4
schopnosti	schopnost	k1gFnPc4
obecně	obecně	k6eAd1
přiřadili	přiřadit	k5eAaPmAgMnP
třídě	třída	k1gFnSc6
Člověk	člověk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Představme	představit	k5eAaPmRp1nP
si	se	k3xPyFc3
dále	daleko	k6eAd2
<g/>
,	,	kIx,
že	že	k8xS
můj	můj	k3xOp1gMnSc1
soused	soused	k1gMnSc1
je	být	k5eAaImIp3nS
svářeč	svářeč	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
moje	můj	k3xOp1gFnSc1
sousedka	sousedka	k1gFnSc1
má	mít	k5eAaImIp3nS
jméno	jméno	k1gNnSc4
<g/>
,	,	kIx,
příjmení	příjmení	k1gNnSc4
a	a	k8xC
umí	umět	k5eAaImIp3nS
chodit	chodit	k5eAaImF
<g/>
,	,	kIx,
mluvit	mluvit	k5eAaImF
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
ovšem	ovšem	k9
umí	umět	k5eAaImIp3nS
svářet	svářet	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
modelování	modelování	k1gNnSc6
můžeme	moct	k5eAaImIp1nP
využít	využít	k5eAaPmF
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
svářeč	svářeč	k1gMnSc1
má	mít	k5eAaImIp3nS
všechny	všechen	k3xTgFnPc4
vlastnosti	vlastnost	k1gFnPc4
třídy	třída	k1gFnSc2
Člověk	člověk	k1gMnSc1
a	a	k8xC
něco	něco	k3yInSc1
navíc	navíc	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytvořme	vytvořit	k5eAaPmRp1nP
třídu	třída	k1gFnSc4
Svářeč	svářeč	k1gMnSc1
jako	jako	k9
potomka	potomek	k1gMnSc4
třídy	třída	k1gFnSc2
Člověk	člověk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třída	třída	k1gFnSc1
svářeč	svářeč	k1gMnSc1
tímto	tento	k3xDgInSc7
dědí	dědit	k5eAaImIp3nS
všechny	všechen	k3xTgInPc1
atributy	atribut	k1gInPc1
i	i	k8xC
metody	metoda	k1gFnPc1
třídy	třída	k1gFnSc2
Člověk	člověk	k1gMnSc1
(	(	kIx(
<g/>
nemusíme	muset	k5eNaImIp1nP
je	on	k3xPp3gFnPc4
v	v	k7c6
kódu	kód	k1gInSc6
znovu	znovu	k6eAd1
psát	psát	k5eAaImF
a	a	k8xC
budeme	být	k5eAaImBp1nP
je	on	k3xPp3gInPc4
upravovat	upravovat	k5eAaImF
na	na	k7c6
jediném	jediný	k2eAgNnSc6d1
místě	místo	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
metodu	metoda	k1gFnSc4
svařit	svařit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Metoda	metoda	k1gFnSc1
svařit	svařit	k5eAaPmF
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
například	například	k6eAd1
dva	dva	k4xCgInPc4
parametry	parametr	k1gInPc4
(	(	kIx(
<g/>
argumenty	argument	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
prvníKusKovu	prvníKusKovo	k1gNnSc6
a	a	k8xC
druhýKusKovu	druhýKusKovo	k1gNnSc6
<g/>
,	,	kIx,
a	a	k8xC
návratovou	návratový	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
jedenKusKovu	jedenKusKov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vím	vědět	k5eAaImIp1nS
<g/>
,	,	kIx,
že	že	k8xS
když	když	k8xS
dám	dát	k5eAaPmIp1nS
sousedovi	soused	k1gMnSc3
svářeči	svářeč	k1gMnSc3
dva	dva	k4xCgInPc4
kusy	kus	k1gInPc4
kovu	kov	k1gInSc2
a	a	k8xC
požádám	požádat	k5eAaPmIp1nS
o	o	k7c6
jejich	jejich	k3xOp3gNnSc6
svaření	svaření	k1gNnSc6
<g/>
,	,	kIx,
dostanu	dostat	k5eAaPmIp1nS
je	být	k5eAaImIp3nS
zpátky	zpátky	k6eAd1
jako	jako	k8xS,k8xC
jeden	jeden	k4xCgInSc4
kus	kus	k1gInSc4
kovu	kov	k1gInSc2
a	a	k8xC
nemusím	muset	k5eNaImIp1nS
se	se	k3xPyFc4
starat	starat	k5eAaImF
o	o	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
to	ten	k3xDgNnSc1
dokázal	dokázat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
to	ten	k3xDgNnSc1
bude	být	k5eAaImBp3nS
fungovat	fungovat	k5eAaImF
v	v	k7c6
programu	program	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objekty	objekt	k1gInPc1
si	se	k3xPyFc3
mezi	mezi	k7c7
sebou	se	k3xPyFc7
mohou	moct	k5eAaImIp3nP
posílat	posílat	k5eAaImF
zprávy	zpráva	k1gFnPc4
<g/>
,	,	kIx,
v	v	k7c6
našem	náš	k3xOp1gInSc6
případě	případ	k1gInSc6
zpráva	zpráva	k1gFnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jeden	jeden	k4xCgInSc1
objekt	objekt	k1gInSc1
spustí	spustit	k5eAaPmIp3nS
metodu	metoda	k1gFnSc4
svařit	svařit	k5eAaPmF
jiného	jiný	k2eAgInSc2d1
objektu	objekt	k1gInSc2
a	a	k8xC
využije	využít	k5eAaPmIp3nS
vrácenou	vrácený	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
(	(	kIx(
<g/>
odpověď	odpověď	k1gFnSc4
na	na	k7c4
zprávu	zpráva	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomuto	tento	k3xDgInSc3
způsobu	způsob	k1gInSc3
komunikace	komunikace	k1gFnSc2
mezi	mezi	k7c7
objekty	objekt	k1gInPc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
skrývá	skrývat	k5eAaImIp3nS
konkrétní	konkrétní	k2eAgFnSc4d1
implementaci	implementace	k1gFnSc4
a	a	k8xC
stará	starat	k5eAaImIp3nS
se	se	k3xPyFc4
jen	jen	k9
o	o	k7c4
zprávy	zpráva	k1gFnPc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
v	v	k7c6
objektově	objektově	k6eAd1
orientovaném	orientovaný	k2eAgNnSc6d1
programování	programování	k1gNnSc6
říká	říkat	k5eAaImIp3nS
zapouzdření	zapouzdření	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Představme	představit	k5eAaPmRp1nP
si	se	k3xPyFc3
nyní	nyní	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
potřebujeme	potřebovat	k5eAaImIp1nP
vytvořit	vytvořit	k5eAaPmF
komplexnější	komplexní	k2eAgInSc4d2
model	model	k1gInSc4
světa	svět	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
se	se	k3xPyFc4
já	já	k3xPp1nSc1
od	od	k7c2
své	svůj	k3xOyFgFnSc2
sousedky	sousedka	k1gFnSc2
začnu	začít	k5eAaPmIp1nS
lišit	lišit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Já	já	k3xPp1nSc1
umím	umět	k5eAaImIp1nS
programovat	programovat	k5eAaImF
a	a	k8xC
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
umí	umět	k5eAaImIp3nS
účtovat	účtovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
nám	my	k3xPp1nPc3
zůstaly	zůstat	k5eAaPmAgFnP
různé	různý	k2eAgFnPc4d1
společné	společný	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
<g/>
,	,	kIx,
ponechme	ponechet	k5eAaPmRp1nP,k5eAaBmRp1nP,k5eAaImRp1nP
původní	původní	k2eAgFnSc4d1
třídu	třída	k1gFnSc4
Člověk	člověk	k1gMnSc1
a	a	k8xC
vytvořme	vytvořit	k5eAaPmRp1nP
dvě	dva	k4xCgFnPc4
třídy	třída	k1gFnSc2
Programátor	programátor	k1gInSc1
a	a	k8xC
Účetní	účetní	k1gFnPc1
<g/>
,	,	kIx,
obě	dva	k4xCgFnPc1
odděděné	odděděný	k2eAgFnPc1d1
od	od	k7c2
třídy	třída	k1gFnSc2
Člověk	člověk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
však	však	k9
v	v	k7c6
modelu	model	k1gInSc6
světa	svět	k1gInSc2
nebude	být	k5eNaImBp3nS
existovat	existovat	k5eAaImF
žádná	žádný	k3yNgFnSc1
instance	instance	k1gFnSc1
třídy	třída	k1gFnSc2
Člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
budou	být	k5eAaImBp3nP
existovat	existovat	k5eAaImF
jen	jen	k9
instance	instance	k1gFnPc4
potomků	potomek	k1gMnPc2
třídy	třída	k1gFnSc2
Člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
tedy	tedy	k9
tříd	třída	k1gFnPc2
od	od	k7c2
Člověka	člověk	k1gMnSc2
odděděných	odděděný	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
skutečně	skutečně	k6eAd1
chceme	chtít	k5eAaImIp1nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
v	v	k7c6
našem	náš	k3xOp1gInSc6
modelu	model	k1gInSc6
existovali	existovat	k5eAaImAgMnP
pouze	pouze	k6eAd1
svářeči	svářeč	k1gMnPc1
<g/>
,	,	kIx,
programátoři	programátor	k1gMnPc1
a	a	k8xC
účetní	účetní	k1gMnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
žádní	žádný	k3yNgMnPc1
obecní	obecní	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
by	by	kYmCp3nP
nespadali	spadat	k5eNaPmAgMnP,k5eNaImAgMnP
ani	ani	k8xC
do	do	k7c2
jedné	jeden	k4xCgFnSc2
z	z	k7c2
těchto	tento	k3xDgFnPc2
skupin	skupina	k1gFnPc2
<g/>
,	,	kIx,
lze	lze	k6eAd1
označit	označit	k5eAaPmF
třídu	třída	k1gFnSc4
Člověk	člověk	k1gMnSc1
jako	jako	k8xS,k8xC
abstraktní	abstraktní	k2eAgFnSc4d1
třídu	třída	k1gFnSc4
<g/>
,	,	kIx,
tedy	tedy	k9
takovou	takový	k3xDgFnSc4
<g/>
,	,	kIx,
od	od	k7c2
níž	jenž	k3xRgFnSc2
nelze	lze	k6eNd1
tvořit	tvořit	k5eAaImF
žádné	žádný	k3yNgFnPc4
instance	instance	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Metody	metoda	k1gFnPc1
</s>
<s>
Děděním	dědění	k1gNnSc7
lze	lze	k6eAd1
nejen	nejen	k6eAd1
rozšiřovat	rozšiřovat	k5eAaImF
repertoár	repertoár	k1gInSc4
metod	metoda	k1gFnPc2
o	o	k7c4
další	další	k2eAgNnSc4d1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
„	„	k?
<g/>
měnit	měnit	k5eAaImF
<g/>
“	“	k?
dosavadní	dosavadní	k2eAgInPc4d1
zděděné	zděděný	k2eAgInPc4d1
<g/>
:	:	kIx,
Například	například	k6eAd1
třída	třída	k1gFnSc1
Starý_člověk	Starý_člověka	k1gFnPc2
může	moct	k5eAaImIp3nS
překrýt	překrýt	k5eAaPmF
(	(	kIx(
<g/>
angl.	angl.	k?
overriding	overriding	k1gInSc4
<g/>
)	)	kIx)
původní	původní	k2eAgFnSc4d1
metodu	metoda	k1gFnSc4
chodit	chodit	k5eAaImF
<g/>
(	(	kIx(
<g/>
)	)	kIx)
poděděnou	poděděný	k2eAgFnSc4d1
od	od	k7c2
Člověka	člověk	k1gMnSc2
svou	svůj	k3xOyFgFnSc7
vlastní	vlastní	k2eAgFnSc7d1
implementací	implementace	k1gFnSc7
a	a	k8xC
chodit	chodit	k5eAaImF
<g/>
(	(	kIx(
<g/>
)	)	kIx)
bude	být	k5eAaImBp3nS
s	s	k7c7
použitím	použití	k1gNnSc7
hole	hole	k1gFnSc2
<g/>
:	:	kIx,
Sám	sám	k3xTgMnSc1
už	už	k6eAd1
tedy	tedy	k9
jinak	jinak	k6eAd1
než	než	k8xS
o	o	k7c6
holi	hole	k1gFnSc6
nechodí	chodit	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přitom	přitom	k6eAd1
si	se	k3xPyFc3
stále	stále	k6eAd1
<g/>
,	,	kIx,
např.	např.	kA
pomocí	pomocí	k7c2
syntaktické	syntaktický	k2eAgFnSc2d1
konstrukce	konstrukce	k1gFnSc2
zvané	zvaný	k2eAgFnSc2d1
tečková	tečkový	k2eAgFnSc1d1
konvence	konvence	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c4
původní	původní	k2eAgFnSc4d1
metodu	metoda	k1gFnSc4
Člověk	člověk	k1gMnSc1
<g/>
.	.	kIx.
<g/>
chodit	chodit	k5eAaImF
<g/>
(	(	kIx(
<g/>
)	)	kIx)
může	moct	k5eAaImIp3nS
vzpomenout	vzpomenout	k5eAaPmF
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
přístup	přístup	k1gInSc4
k	k	k7c3
metodám	metoda	k1gFnPc3
svých	svůj	k3xOyFgMnPc2
předků	předek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Jiný	jiný	k2eAgInSc1d1
způsob	způsob	k1gInSc1
<g/>
,	,	kIx,
jak	jak	k6eAd1
upravit	upravit	k5eAaPmF
nebo	nebo	k8xC
rozšířit	rozšířit	k5eAaPmF
možnosti	možnost	k1gFnPc4
(	(	kIx(
<g/>
nejen	nejen	k6eAd1
zděděné	zděděný	k2eAgFnPc1d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
přetěžování	přetěžování	k1gNnSc1
metody	metoda	k1gFnSc2
(	(	kIx(
<g/>
angl.	angl.	k?
method	method	k1gInSc1
overloading	overloading	k1gInSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Například	například	k6eAd1
Akrobat	akrobat	k1gMnSc1
je	být	k5eAaImIp3nS
Člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
umí	umět	k5eAaImIp3nS
nejen	nejen	k6eAd1
normálně	normálně	k6eAd1
chodit	chodit	k5eAaImF
<g/>
(	(	kIx(
<g/>
)	)	kIx)
po	po	k7c6
nohou	noha	k1gFnPc6
<g/>
,	,	kIx,
ale	ale	k8xC
navíc	navíc	k6eAd1
si	se	k3xPyFc3
doplnil	doplnit	k5eAaPmAgMnS
také	také	k9
schopnost	schopnost	k1gFnSc4
volanou	volaný	k2eAgFnSc4d1
chodit	chodit	k5eAaImF
<g/>
(	(	kIx(
<g/>
po_rukou	po_ruka	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
na	na	k7c6
volbě	volba	k1gFnSc6
Akrobata	akrobat	k1gMnSc2
<g/>
,	,	kIx,
jaký	jaký	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
vstup	vstup	k1gInSc4
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
metodu	metoda	k1gFnSc4
použije	použít	k5eAaPmIp3nS
<g/>
,	,	kIx,
a	a	k8xC
podle	podle	k7c2
toho	ten	k3xDgNnSc2
se	se	k3xPyFc4
pak	pak	k6eAd1
zavolá	zavolat	k5eAaPmIp3nS
právě	právě	k9
ta	ten	k3xDgFnSc1
jediná	jediný	k2eAgFnSc1d1
odpovídající	odpovídající	k2eAgFnSc1d1
ze	z	k7c2
všech	všecek	k3xTgFnPc2
přítomných	přítomný	k2eAgFnPc2d1
implementací	implementace	k1gFnPc2
této	tento	k3xDgFnSc2
metody	metoda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozdíl	rozdíl	k1gInSc1
zde	zde	k6eAd1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
buď	buď	k8xC
v	v	k7c6
počtu	počet	k1gInSc6
předaných	předaný	k2eAgInPc2d1
parametrů	parametr	k1gInPc2
volané	volaný	k2eAgFnSc3d1
metodě	metoda	k1gFnSc3
<g/>
,	,	kIx,
a	a	k8xC
pak	pak	k6eAd1
je	být	k5eAaImIp3nS
rozdíl	rozdíl	k1gInSc4
<g/>
,	,	kIx,
jestli	jestli	k8xS
se	se	k3xPyFc4
vozíme	vozit	k5eAaImIp1nP
autem	auto	k1gNnSc7
jezdit	jezdit	k5eAaImF
<g/>
(	(	kIx(
<g/>
Vozidlo	vozidlo	k1gNnSc1
auto	auto	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
jestli	jestli	k8xS
jedeme	jet	k5eAaImIp1nP
autobusem	autobus	k1gInSc7
po	po	k7c6
lince	linka	k1gFnSc6
183	#num#	k4
jezdit	jezdit	k5eAaImF
<g/>
(	(	kIx(
<g/>
Vozidlo	vozidlo	k1gNnSc4
autobus	autobus	k1gInSc1
<g/>
,	,	kIx,
Linka	linka	k1gFnSc1
číslo	číslo	k1gNnSc4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
jde	jít	k5eAaImIp3nS
o	o	k7c4
dvě	dva	k4xCgFnPc4
různé	různý	k2eAgFnPc4d1
přetížené	přetížený	k2eAgFnPc4d1
metody	metoda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anebo	anebo	k8xC
zda	zda	k8xS
pro	pro	k7c4
řízení	řízení	k1gNnSc4
škodovky	škodovka	k1gFnSc2
voláme	volat	k5eAaImIp1nP
metodu	metoda	k1gFnSc4
řídit	řídit	k5eAaImF
<g/>
(	(	kIx(
<g/>
škoda	škoda	k1gFnSc1
<g/>
)	)	kIx)
sice	sice	k8xC
se	s	k7c7
stejným	stejný	k2eAgInSc7d1
počtem	počet	k1gInSc7
vstupů	vstup	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
předáváme	předávat	k5eAaImIp1nP
různé	různý	k2eAgInPc4d1
typy	typ	k1gInPc4
pro	pro	k7c4
metody	metoda	k1gFnPc4
<g/>
:	:	kIx,
řídit	řídit	k5eAaImF
<g/>
(	(	kIx(
<g/>
Automobil	automobil	k1gInSc1
název	název	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
řídit	řídit	k5eAaImF
<g/>
(	(	kIx(
<g/>
Firma	firma	k1gFnSc1
název	název	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
to	ten	k3xDgNnSc4
jsou	být	k5eAaImIp3nP
dvě	dva	k4xCgFnPc1
různé	různý	k2eAgFnPc1d1
přetížené	přetížený	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
<g/>
,	,	kIx,
obě	dva	k4xCgFnPc1
Člověkovi	člověk	k1gMnSc3
dostupná	dostupný	k2eAgNnPc4d1
zároveň	zároveň	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Naopak	naopak	k6eAd1
<g/>
,	,	kIx,
obě	dva	k4xCgFnPc1
volání	volání	k1gNnSc1
jezdit	jezdit	k5eAaImF
<g/>
(	(	kIx(
<g/>
auto	auto	k1gNnSc4
<g/>
)	)	kIx)
a	a	k8xC
jezdit	jezdit	k5eAaImF
<g/>
(	(	kIx(
<g/>
motorka	motorka	k1gFnSc1
<g/>
)	)	kIx)
použijí	použít	k5eAaPmIp3nP
stejnou	stejný	k2eAgFnSc4d1
metodu	metoda	k1gFnSc4
jezdit	jezdit	k5eAaImF
<g/>
(	(	kIx(
<g/>
Vozidlo	vozidlo	k1gNnSc1
vozidlo	vozidlo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nejde	jít	k5eNaImIp3nS
o	o	k7c6
přetížení	přetížení	k1gNnSc6
metody	metoda	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
v	v	k7c6
obou	dva	k4xCgInPc6
případech	případ	k1gInPc6
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
stejný	stejný	k2eAgInSc4d1
počet	počet	k1gInSc4
vstupů	vstup	k1gInPc2
a	a	k8xC
i	i	k9
stejný	stejný	k2eAgInSc1d1
typ	typ	k1gInSc1
vstupu	vstup	k1gInSc2
Vozidlo	vozidlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ne	ne	k9
že	že	k8xS
by	by	kYmCp3nS
takové	takový	k3xDgNnSc4
ježdění	ježdění	k1gNnSc4
nebyl	být	k5eNaImAgInS
rozdíl	rozdíl	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
ten	ten	k3xDgMnSc1
už	už	k6eAd1
se	se	k3xPyFc4
při	při	k7c6
takto	takto	k6eAd1
postavené	postavený	k2eAgFnSc6d1
třídě	třída	k1gFnSc6
musí	muset	k5eAaImIp3nS
řešit	řešit	k5eAaImF
uvnitř	uvnitř	k7c2
metody	metoda	k1gFnSc2
<g/>
,	,	kIx,
rozvětvením	rozvětvení	k1gNnSc7
běhu	běh	k1gInSc2
programu	program	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Prvním	první	k4xOgInSc7
stupněm	stupeň	k1gInSc7
řešení	řešení	k1gNnPc2
je	být	k5eAaImIp3nS
například	například	k6eAd1
do	do	k7c2
objektu	objekt	k1gInSc2
zavést	zavést	k5eAaPmF
dědění	dědění	k1gNnSc4
<g/>
:	:	kIx,
Prohlásit	prohlásit	k5eAaPmF
Vozidlo	vozidlo	k1gNnSc4
za	za	k7c4
abstraktní	abstraktní	k2eAgFnSc4d1
třídu	třída	k1gFnSc4
<g/>
,	,	kIx,
její	její	k3xOp3gFnSc4
metodu	metoda	k1gFnSc4
jezdit	jezdit	k5eAaImF
<g/>
(	(	kIx(
<g/>
)	)	kIx)
také	také	k9
za	za	k7c4
abstraktní	abstraktní	k2eAgNnSc4d1
<g/>
,	,	kIx,
a	a	k8xC
do	do	k7c2
odděděných	odděděný	k2eAgFnPc2d1
metod	metoda	k1gFnPc2
jezdit	jezdit	k5eAaImF
<g/>
(	(	kIx(
<g/>
)	)	kIx)
potomků	potomek	k1gMnPc2
Automobil	automobil	k1gInSc4
a	a	k8xC
Motocykl	motocykl	k1gInSc4
přenést	přenést	k5eAaPmF
jen	jen	k9
příslušné	příslušný	k2eAgFnPc4d1
větve	větev	k1gFnPc4
kódu	kód	k1gInSc2
<g/>
:	:	kIx,
Jednou	jednou	k9
z	z	k7c2
výhod	výhoda	k1gFnPc2
dědění	dědění	k1gNnSc2
je	být	k5eAaImIp3nS
právě	právě	k9
eliminace	eliminace	k1gFnSc1
složitých	složitý	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
pro	pro	k7c4
větvení	větvení	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1
možnost	možnost	k1gFnSc1
je	být	k5eAaImIp3nS
opravdu	opravdu	k6eAd1
přetížení	přetížení	k1gNnSc1
metody	metoda	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
původní	původní	k2eAgFnSc1d1
obecná	obecný	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
jezdit	jezdit	k5eAaImF
<g/>
(	(	kIx(
<g/>
Vozidlo	vozidlo	k1gNnSc1
vozidlo	vozidlo	k1gNnSc1
<g/>
)	)	kIx)
Člověku	člověk	k1gMnSc3
ponechá	ponechat	k5eAaPmIp3nS
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
ale	ale	k8xC
lze	lze	k6eAd1
dopisovat	dopisovat	k5eAaImF
specifičtější	specifický	k2eAgFnPc4d2
metody	metoda	k1gFnPc4
jezdit	jezdit	k5eAaImF
<g/>
(	(	kIx(
<g/>
Autem	auto	k1gNnSc7
auto	auto	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
jezdit	jezdit	k5eAaImF
<g/>
(	(	kIx(
<g/>
Na_motorce	Na_motorka	k1gFnSc6
motorka	motorka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jestli	jestli	k8xS
jsou	být	k5eAaImIp3nP
všechny	všechen	k3xTgFnPc1
tyto	tento	k3xDgFnPc1
tři	tři	k4xCgFnPc1
metody	metoda	k1gFnPc1
přítomny	přítomen	k2eAgFnPc1d1
už	už	k6eAd1
přímo	přímo	k6eAd1
ve	v	k7c6
třídě	třída	k1gFnSc6
Člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
přibudou	přibýt	k5eAaPmIp3nP
další	další	k2eAgInPc4d1
až	až	k6eAd1
v	v	k7c6
odděděné	odděděný	k2eAgFnSc6d1
třídě	třída	k1gFnSc6
je	být	k5eAaImIp3nS
jedno	jeden	k4xCgNnSc1
<g/>
:	:	kIx,
řidič	řidič	k1gMnSc1
je	on	k3xPp3gInPc4
vidí	vidět	k5eAaImIp3nS
všechny	všechen	k3xTgMnPc4
<g/>
,	,	kIx,
z	z	k7c2
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
pohledu	pohled	k1gInSc2
je	být	k5eAaImIp3nS
metoda	metoda	k1gFnSc1
přetížená	přetížený	k2eAgFnSc1d1
<g/>
,	,	kIx,
tedy	tedy	k8xC
si	se	k3xPyFc3
může	moct	k5eAaImIp3nS
vybírat	vybírat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Někdy	někdy	k6eAd1
se	se	k3xPyFc4
rodičovská	rodičovský	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
<g/>
,	,	kIx,
třeba	třeba	k6eAd1
chodit	chodit	k5eAaImF
<g/>
(	(	kIx(
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nejen	nejen	k6eAd1
přetěžuje	přetěžovat	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
navíc	navíc	k6eAd1
ještě	ještě	k9
zcela	zcela	k6eAd1
úmyslně	úmyslně	k6eAd1
překrývá	překrývat	k5eAaImIp3nS
<g/>
:	:	kIx,
Třeba	třeba	k8wRxS
zmiňovaný	zmiňovaný	k2eAgMnSc1d1
Akrobat	akrobat	k1gMnSc1
se	se	k3xPyFc4
naučil	naučit	k5eAaPmAgMnS
chodit	chodit	k5eAaImF
po	po	k7c6
laně	lano	k1gNnSc6
<g/>
,	,	kIx,
proto	proto	k8xC
je	být	k5eAaImIp3nS
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
žádoucí	žádoucí	k2eAgNnPc1d1
při	při	k7c6
volání	volání	k1gNnSc6
metody	metoda	k1gFnSc2
chodit	chodit	k5eAaImF
<g/>
(	(	kIx(
<g/>
)	)	kIx)
nejdříve	dříve	k6eAd3
zkusit	zkusit	k5eAaPmF
svou	svůj	k3xOyFgFnSc4
novou	nový	k2eAgFnSc4d1
schopnost	schopnost	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
až	až	k9
pak	pak	k6eAd1
použít	použít	k5eAaPmF
obecnější	obecní	k2eAgMnPc1d2
chodit	chodit	k5eAaImF
<g/>
(	(	kIx(
<g/>
)	)	kIx)
od	od	k7c2
předka	předek	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdříve	dříve	k6eAd3
se	se	k3xPyFc4
ověří	ověřit	k5eAaPmIp3nS
<g/>
,	,	kIx,
zda	zda	k8xS
Akrobat	akrobat	k1gMnSc1
je_na_laně	je_na_laně	k6eAd1
<g/>
,	,	kIx,
podle	podle	k7c2
této	tento	k3xDgFnSc2
podmínky	podmínka	k1gFnSc2
se	se	k3xPyFc4
buď	buď	k8xC
interně	interně	k6eAd1
zavolá	zavolat	k5eAaPmIp3nS
chodit	chodit	k5eAaImF
<g/>
(	(	kIx(
<g/>
po_laně	po_laně	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
se	se	k3xPyFc4
výslovně	výslovně	k6eAd1
zavolá	zavolat	k5eAaPmIp3nS
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
zděděná	zděděný	k2eAgFnSc1d1
ale	ale	k8xC
právě	právě	k6eAd1
zakrývaná	zakrývaný	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
parent	parent	k1gMnSc1
<g/>
.	.	kIx.
<g/>
chodit	chodit	k5eAaImF
<g/>
(	(	kIx(
<g/>
)	)	kIx)
od	od	k7c2
předka	předek	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Akrobat	akrobat	k1gMnSc1
nemusí	muset	k5eNaImIp3nS
být	být	k5eAaImF
odděděn	odděděn	k2eAgInSc4d1
od	od	k7c2
Člověka	člověk	k1gMnSc2
ale	ale	k8xC
třeba	třeba	k6eAd1
od	od	k7c2
Atléta	Atléto	k1gNnSc2
a	a	k8xC
ten	ten	k3xDgMnSc1
teprve	teprve	k6eAd1
od	od	k7c2
Člověka	člověk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potom	potom	k6eAd1
když	když	k8xS
Akrobat	akrobat	k1gMnSc1
volá	volat	k5eAaImIp3nS
metodu	metoda	k1gFnSc4
předka	předek	k1gMnSc4
parent	parent	k1gMnSc1
<g/>
.	.	kIx.
<g/>
chodit	chodit	k5eAaImF
<g/>
(	(	kIx(
<g/>
)	)	kIx)
nemusí	muset	k5eNaImIp3nS
to	ten	k3xDgNnSc1
znamenat	znamenat	k5eAaImF
chodit	chodit	k5eAaImF
po	po	k7c6
zemi	zem	k1gFnSc6
jako	jako	k8xC,k8xS
u	u	k7c2
Člověka	člověk	k1gMnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
třeba	třeba	k6eAd1
chodit	chodit	k5eAaImF
po	po	k7c6
kladině	kladina	k1gFnSc6
<g/>
:	:	kIx,
Metody	metoda	k1gFnPc1
předků	předek	k1gMnPc2
se	se	k3xPyFc4
volají	volat	k5eAaImIp3nP
po	po	k7c6
jednom	jeden	k4xCgMnSc6
zpětně	zpětně	k6eAd1
ve	v	k7c6
sledu	sled	k1gInSc6
dědění	dědění	k1gNnSc2
<g/>
,	,	kIx,
například	například	k6eAd1
Akrobat	akrobat	k1gMnSc1
<g/>
.	.	kIx.
<g/>
chodit	chodit	k5eAaImF
<g/>
(	(	kIx(
<g/>
)	)	kIx)
po	po	k7c6
laně	lano	k1gNnSc6
<g/>
,	,	kIx,
ak	ak	k?
neplatí	platit	k5eNaImIp3nS
že	že	k8xS
je_na_laně	je_na_laně	k6eAd1
zavolá	zavolat	k5eAaPmIp3nS
Atlét	Atlét	k1gInSc1
<g/>
.	.	kIx.
<g/>
chodit	chodit	k5eAaImF
<g/>
(	(	kIx(
<g/>
)	)	kIx)
po	po	k7c6
žíněnce	žíněnka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ak	Ak	k1gMnSc2
je_na_žíněnce	je_na_žíněnec	k1gMnSc2
na	na	k7c4
volání	volání	k1gNnSc4
samotné	samotný	k2eAgFnSc2d1
Člověk	člověk	k1gMnSc1
<g/>
.	.	kIx.
<g/>
chodit	chodit	k5eAaImF
<g/>
(	(	kIx(
<g/>
)	)	kIx)
po	po	k7c6
zemi	zem	k1gFnSc6
se	se	k3xPyFc4
nedostane	dostat	k5eNaPmIp3nS
<g/>
,	,	kIx,
protože	protože	k8xS
byla	být	k5eAaImAgFnS
splněna	splnit	k5eAaPmNgFnS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
podmínek	podmínka	k1gFnPc2
v	v	k7c6
překrytých	překrytý	k2eAgFnPc6d1
metodách	metoda	k1gFnPc6
ve	v	k7c6
sledu	sled	k1gInSc6
dědění	dědění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Obecně	obecně	k6eAd1
se	se	k3xPyFc4
při	při	k7c6
překrývání	překrývání	k1gNnSc6
může	moct	k5eAaImIp3nS
ověřovat	ověřovat	k5eAaImF
nějaká	nějaký	k3yIgFnSc1
zadaná	zadaný	k2eAgFnSc1d1
podmínka	podmínka	k1gFnSc1
<g/>
,	,	kIx,
zde	zde	k6eAd1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
před	před	k7c7
zavoláním	zavolání	k1gNnSc7
metody	metoda	k1gFnSc2
předka	předek	k1gMnSc2
zařídit	zařídit	k5eAaPmF
inicializaci	inicializace	k1gFnSc4
<g/>
,	,	kIx,
vrácení	vrácení	k1gNnSc4
prostředků	prostředek	k1gInPc2
systému	systém	k1gInSc2
apod.	apod.	kA
Podobná	podobný	k2eAgFnSc1d1
konstrukce	konstrukce	k1gFnSc1
je	být	k5eAaImIp3nS
podstatná	podstatný	k2eAgFnSc1d1
pro	pro	k7c4
destruktor	destruktor	k1gInSc4
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
i	i	k9
s	s	k7c7
konstruktory	konstruktor	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Rozhraní	rozhraní	k1gNnSc1
objektů	objekt	k1gInPc2
</s>
<s>
Zjistíme	zjistit	k5eAaPmIp1nP
<g/>
,	,	kIx,
že	že	k8xS
programátor	programátor	k1gMnSc1
umí	umět	k5eAaImIp3nS
psát	psát	k5eAaImF
na	na	k7c6
počítači	počítač	k1gInSc6
a	a	k8xC
účetní	účetní	k1gFnSc6
také	také	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Intuitivně	intuitivně	k6eAd1
cítíme	cítit	k5eAaImIp1nP
<g/>
,	,	kIx,
že	že	k8xS
nebudou	být	k5eNaImBp3nP
mít	mít	k5eAaImF
mnoho	mnoho	k4c4
dalších	další	k2eAgFnPc2d1
společných	společný	k2eAgFnPc2d1
schopností	schopnost	k1gFnPc2
a	a	k8xC
navíc	navíc	k6eAd1
tuto	tento	k3xDgFnSc4
dovednost	dovednost	k1gFnSc4
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
napříč	napříč	k7c7
povoláními	povolání	k1gNnPc7
leckdo	leckdo	k3yInSc1
<g/>
,	,	kIx,
proto	proto	k8xC
nemá	mít	k5eNaImIp3nS
smysl	smysl	k1gInSc4
tvořit	tvořit	k5eAaImF
třídu	třída	k1gFnSc4
na	na	k7c4
způsob	způsob	k1gInSc4
ČlovekPracujícíSPočítačem	ČlovekPracujícíSPočítač	k1gInSc7
a	a	k8xC
od	od	k7c2
ní	on	k3xPp3gFnSc2
dědit	dědit	k5eAaImF
Programátora	programátor	k1gMnSc4
a	a	k8xC
Účetní	účetní	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
výhodnější	výhodný	k2eAgNnSc1d2
například	například	k6eAd1
vytvořit	vytvořit	k5eAaPmF
rozhraní	rozhraní	k1gNnSc3
SchopenPsátNaPočítači	SchopenPsátNaPočítač	k1gMnPc1
s	s	k7c7
požadavkem	požadavek	k1gInSc7
na	na	k7c4
metodu	metoda	k1gFnSc4
napišNaPočítači	napišNaPočítač	k1gMnPc1
<g/>
(	(	kIx(
<g/>
)	)	kIx)
a	a	k8xC
upravit	upravit	k5eAaPmF
třídy	třída	k1gFnSc2
Programátor	programátor	k1gInSc1
a	a	k8xC
Účetní	účetní	k1gFnSc1
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
toto	tento	k3xDgNnSc4
rozhraní	rozhraní	k1gNnSc4
implementovaly	implementovat	k5eAaImAgInP
<g/>
,	,	kIx,
tedy	tedy	k8xC
předepsanou	předepsaný	k2eAgFnSc4d1
metodu	metoda	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
každý	každý	k3xTgInSc4
po	po	k7c6
svém	svůj	k3xOyFgInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
definici	definice	k1gFnSc6
rozhraní	rozhraní	k1gNnSc2
nemůže	moct	k5eNaImIp3nS
být	být	k5eAaImF
obsažen	obsažen	k2eAgInSc1d1
kód	kód	k1gInSc1
(	(	kIx(
<g/>
implementace	implementace	k1gFnPc1
<g/>
)	)	kIx)
dané	daný	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
všechny	všechen	k3xTgFnPc1
třídy	třída	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
toto	tento	k3xDgNnSc4
rozhraní	rozhraní	k1gNnSc4
implementují	implementovat	k5eAaImIp3nP
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
schopny	schopen	k2eAgInPc1d1
se	se	k3xPyFc4
s	s	k7c7
příkazem	příkaz	k1gInSc7
napišNaPočítači	napišNaPočítač	k1gMnPc1
<g/>
(	(	kIx(
<g/>
)	)	kIx)
nějak	nějak	k6eAd1
vypořádat	vypořádat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Obdobně	obdobně	k6eAd1
i	i	k9
již	již	k6eAd1
zmiňovaná	zmiňovaný	k2eAgFnSc1d1
abstraktní	abstraktní	k2eAgFnSc1d1
třída	třída	k1gFnSc1
může	moct	k5eAaImIp3nS
předepisovat	předepisovat	k5eAaImF
doimplementování	doimplementování	k1gNnSc4
metod	metoda	k1gFnPc2
<g/>
,	,	kIx,
pro	pro	k7c4
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
sama	sám	k3xTgFnSc1
nemá	mít	k5eNaImIp3nS
vlastní	vlastní	k2eAgInSc4d1
kód	kód	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
jen	jen	k9
předpis	předpis	k1gInSc1
abstraktní	abstraktní	k2eAgFnSc2d1
metody	metoda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnSc7d1
důležitou	důležitý	k2eAgFnSc7d1
vlastností	vlastnost	k1gFnSc7
objektů	objekt	k1gInPc2
je	být	k5eAaImIp3nS
polymorfismus	polymorfismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejná	stejný	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
poslána	poslat	k5eAaPmNgFnS
od	od	k7c2
různých	různý	k2eAgFnPc2d1
nebo	nebo	k8xC
různým	různý	k2eAgInPc3d1
objektům	objekt	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
rozlišit	rozlišit	k5eAaPmF
<g/>
,	,	kIx,
zda	zda	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
polymorfismus	polymorfismus	k1gInSc4
</s>
<s>
nad	nad	k7c7
dvěma	dva	k4xCgInPc7
různými	různý	k2eAgInPc7d1
objekty	objekt	k1gInPc7
stejné	stejný	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
<g/>
,	,	kIx,
</s>
<s>
nad	nad	k7c7
dvěma	dva	k4xCgInPc7
různými	různý	k2eAgInPc7d1
stavy	stav	k1gInPc7
téhož	týž	k3xTgInSc2
objektu	objekt	k1gInSc2
<g/>
,	,	kIx,
</s>
<s>
anebo	anebo	k8xC
zda	zda	k8xS
zprávu	zpráva	k1gFnSc4
posílají	posílat	k5eAaImIp3nP
různé	různý	k2eAgInPc1d1
objekty	objekt	k1gInPc1
různých	různý	k2eAgFnPc2d1
tříd	třída	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Zapouzdření	zapouzdření	k1gNnSc1
</s>
<s>
Zapouzdření	zapouzdření	k1gNnSc1
v	v	k7c6
objektech	objekt	k1gInPc6
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
obsahu	obsah	k1gInSc3
objektu	objekt	k1gInSc2
se	se	k3xPyFc4
nedostane	dostat	k5eNaPmIp3nS
nikdo	nikdo	k3yNnSc1
jiný	jiný	k2eAgMnSc1d1
<g/>
,	,	kIx,
než	než	k8xS
sám	sám	k3xTgMnSc1
vlastník	vlastník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navenek	navenek	k6eAd1
se	se	k3xPyFc4
objekt	objekt	k1gInSc1
projeví	projevit	k5eAaPmIp3nS
jen	jen	k9
svým	svůj	k3xOyFgNnSc7
rozhraním	rozhraní	k1gNnSc7
(	(	kIx(
<g/>
operacemi	operace	k1gFnPc7
<g/>
,	,	kIx,
metodami	metoda	k1gFnPc7
<g/>
)	)	kIx)
a	a	k8xC
komunikačním	komunikační	k2eAgInSc7d1
protokolem	protokol	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Představte	představit	k5eAaPmRp2nP
si	se	k3xPyFc3
sekretářku	sekretářka	k1gFnSc4
a	a	k8xC
ředitele	ředitel	k1gMnSc4
jako	jako	k8xC,k8xS
dva	dva	k4xCgInPc4
objekty	objekt	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sekretářka	sekretářka	k1gFnSc1
umí	umět	k5eAaImIp3nS
rozhraní	rozhraní	k1gNnSc4
SchopenPsátNaPočítači	SchopenPsátNaPočítač	k1gMnPc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
např.	např.	kA
operace	operace	k1gFnSc1
napsatDopis	napsatDopis	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
ředitel	ředitel	k1gMnSc1
ho	on	k3xPp3gNnSc4
nemá	mít	k5eNaImIp3nS
(	(	kIx(
<g/>
není	být	k5eNaImIp3nS
to	ten	k3xDgNnSc1
jeho	jeho	k3xOp3gFnSc4
starost	starost	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
operaci	operace	k1gFnSc3
napsatDopis	napsatDopis	k1gInSc4
<g/>
,	,	kIx,
protože	protože	k8xS
může	moct	k5eAaImIp3nS
požadavek	požadavek	k1gInSc4
předat	předat	k5eAaPmF
jinému	jiný	k2eAgInSc3d1
objektu	objekt	k1gInSc2
–	–	k?
sekretářce	sekretářka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Udržování	udržování	k1gNnSc1
odkazů	odkaz	k1gInPc2
na	na	k7c4
jiné	jiný	k2eAgInPc4d1
objekty	objekt	k1gInPc4
se	se	k3xPyFc4
říká	říkat	k5eAaImIp3nS
skládání	skládání	k1gNnSc1
objektů	objekt	k1gInPc2
a	a	k8xC
využívání	využívání	k1gNnSc2
jejich	jejich	k3xOp3gFnPc2
služeb	služba	k1gFnPc2
delegování	delegování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Skládání	skládání	k1gNnSc1
objektů	objekt	k1gInPc2
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nejdůležitějších	důležitý	k2eAgFnPc2d3
vlastností	vlastnost	k1gFnPc2
objektového	objektový	k2eAgInSc2d1
datového	datový	k2eAgInSc2d1
modelu	model	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
o	o	k7c4
dost	dost	k6eAd1
důležitější	důležitý	k2eAgFnSc4d2
než	než	k8xS
dědičnost	dědičnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
dědičnost	dědičnost	k1gFnSc1
lze	lze	k6eAd1
v	v	k7c6
OOP	OOP	kA
realizovat	realizovat	k5eAaBmF
pomocí	pomocí	k7c2
skládání	skládání	k1gNnSc2
objektů	objekt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Atomizace	atomizace	k1gFnSc1
metod	metoda	k1gFnPc2
</s>
<s>
A	a	k9
konečně	konečně	k6eAd1
zodpovědnost	zodpovědnost	k1gFnSc4
objektu	objekt	k1gInSc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
objekt	objekt	k1gInSc1
umí	umět	k5eAaImIp3nS
<g/>
,	,	kIx,
o	o	k7c4
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
se	se	k3xPyFc4
stará	starat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objekt	objekt	k1gInSc1
by	by	kYmCp3nS
neměl	mít	k5eNaImAgInS
umět	umět	k5eAaImF
sám	sám	k3xTgInSc4
příliš	příliš	k6eAd1
mnoho	mnoho	k6eAd1
a	a	k8xC
nemusí	muset	k5eNaImIp3nS
to	ten	k3xDgNnSc4
dělat	dělat	k5eAaImF
sám	sám	k3xTgMnSc1
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
skládání	skládání	k1gNnSc4
objektů	objekt	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znakem	znak	k1gInSc7
kvalitního	kvalitní	k2eAgInSc2d1
návrhu	návrh	k1gInSc2
softwaru	software	k1gInSc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
třídy	třída	k1gFnPc1
v	v	k7c6
projektu	projekt	k1gInSc6
obsahují	obsahovat	k5eAaImIp3nP
pokud	pokud	k8xS
možno	možno	k6eAd1
atomické	atomický	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
řeší	řešit	k5eAaImIp3nP
pouze	pouze	k6eAd1
jeden	jeden	k4xCgInSc4
konkrétní	konkrétní	k2eAgInSc4d1
problém	problém	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Aspektově	aspektově	k6eAd1
orientované	orientovaný	k2eAgNnSc4d1
programování	programování	k1gNnSc4
</s>
<s>
Objektově-relační	Objektově-relační	k2eAgNnSc1d1
mapování	mapování	k1gNnSc1
</s>
<s>
Objektová	objektový	k2eAgFnSc1d1
databáze	databáze	k1gFnSc1
</s>
<s>
CORBA	CORBA	kA
</s>
<s>
DCOM	DCOM	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
objektově	objektově	k6eAd1
orientované	orientovaný	k2eAgNnSc4d1
programování	programování	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Objektově	objektově	k6eAd1
orientované	orientovaný	k2eAgNnSc4d1
programování	programování	k1gNnSc4
(	(	kIx(
<g/>
OOP	OOP	kA
<g/>
)	)	kIx)
v	v	k7c6
PHP	PHP	kA
</s>
<s>
PHP	PHP	kA
je	být	k5eAaImIp3nS
věda	věda	k1gFnSc1
-	-	kIx~
Objektově	objektově	k6eAd1
orientované	orientovaný	k2eAgNnSc4d1
programování	programování	k1gNnSc4
nejen	nejen	k6eAd1
v	v	k7c6
PHP	PHP	kA
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4233947-9	4233947-9	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
12473	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
87007503	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
87007503	#num#	k4
</s>
