<s>
Urychlovač	urychlovač	k1gInSc1	urychlovač
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
jen	jen	k9	jen
urychlovač	urychlovač	k1gInSc1	urychlovač
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
technické	technický	k2eAgNnSc1d1	technické
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
používané	používaný	k2eAgNnSc1d1	používané
pro	pro	k7c4	pro
dodání	dodání	k1gNnSc4	dodání
kinetické	kinetický	k2eAgFnSc2d1	kinetická
energie	energie	k1gFnSc2	energie
nabitým	nabitý	k2eAgFnPc3d1	nabitá
částicím	částice	k1gFnPc3	částice
<g/>
.	.	kIx.	.
</s>
