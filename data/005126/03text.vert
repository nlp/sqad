<s>
Urychlovač	urychlovač	k1gInSc1	urychlovač
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
jen	jen	k9	jen
urychlovač	urychlovač	k1gInSc1	urychlovač
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
technické	technický	k2eAgNnSc1d1	technické
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
používané	používaný	k2eAgNnSc1d1	používané
pro	pro	k7c4	pro
dodání	dodání	k1gNnSc4	dodání
kinetické	kinetický	k2eAgFnSc2d1	kinetická
energie	energie	k1gFnSc2	energie
nabitým	nabitý	k2eAgFnPc3d1	nabitá
částicím	částice	k1gFnPc3	částice
<g/>
.	.	kIx.	.
</s>
<s>
Nabité	nabitý	k2eAgFnPc1d1	nabitá
částice	částice	k1gFnPc1	částice
(	(	kIx(	(
<g/>
ionty	ion	k1gInPc1	ion
nebo	nebo	k8xC	nebo
elektrony	elektron	k1gInPc1	elektron
či	či	k8xC	či
pozitrony	pozitron	k1gInPc1	pozitron
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
urychlovači	urychlovač	k1gInSc6	urychlovač
jednou	jednou	k6eAd1	jednou
nebo	nebo	k8xC	nebo
opakovaně	opakovaně	k6eAd1	opakovaně
urychleny	urychlen	k2eAgInPc1d1	urychlen
rozdílem	rozdíl	k1gInSc7	rozdíl
potenciálů	potenciál	k1gInPc2	potenciál
elektrického	elektrický	k2eAgNnSc2d1	elektrické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Urychlovače	urychlovač	k1gInPc1	urychlovač
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
výzkumu	výzkum	k1gInSc3	výzkum
elementárních	elementární	k2eAgFnPc2d1	elementární
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
technické	technický	k2eAgFnSc6d1	technická
praxi	praxe	k1gFnSc6	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc1	dva
základní	základní	k2eAgInPc1d1	základní
typy	typ	k1gInPc1	typ
urychlovačů	urychlovač	k1gInPc2	urychlovač
<g/>
:	:	kIx,	:
lineární	lineární	k2eAgInSc1d1	lineární
a	a	k8xC	a
kruhový	kruhový	k2eAgInSc1d1	kruhový
<g/>
.	.	kIx.	.
</s>
<s>
Urychlovač	urychlovač	k1gInSc1	urychlovač
částic	částice	k1gFnPc2	částice
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
čelní	čelní	k2eAgFnPc4d1	čelní
srážky	srážka	k1gFnPc4	srážka
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
svazky	svazek	k1gInPc7	svazek
částic	částice	k1gFnPc2	částice
stejného	stejný	k2eAgInSc2d1	stejný
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
protony	proton	k1gInPc1	proton
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
různými	různý	k2eAgInPc7d1	různý
typy	typ	k1gInPc7	typ
iontů	ion	k1gInPc2	ion
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
iontů	ion	k1gInPc2	ion
olova	olovo	k1gNnSc2	olovo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
srážce	srážka	k1gFnSc6	srážka
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
částice	částice	k1gFnPc1	částice
rozptýlí	rozptýlit	k5eAaPmIp3nP	rozptýlit
a	a	k8xC	a
když	když	k8xS	když
mají	mít	k5eAaImIp3nP	mít
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
vznikají	vznikat	k5eAaImIp3nP	vznikat
přitom	přitom	k6eAd1	přitom
další	další	k2eAgFnPc1d1	další
částice	částice	k1gFnPc1	částice
(	(	kIx(	(
<g/>
produkty	produkt	k1gInPc1	produkt
srážky	srážka	k1gFnSc2	srážka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zaznamenání	zaznamenání	k1gNnSc4	zaznamenání
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
při	při	k7c6	při
srážce	srážka	k1gFnSc6	srážka
částic	částice	k1gFnPc2	částice
slouží	sloužit	k5eAaImIp3nS	sloužit
detektor	detektor	k1gInSc1	detektor
ionizujícího	ionizující	k2eAgNnSc2d1	ionizující
záření	záření	k1gNnSc2	záření
(	(	kIx(	(
<g/>
částicový	částicový	k2eAgInSc1d1	částicový
detektor	detektor	k1gInSc1	detektor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Částicové	částicový	k2eAgInPc1d1	částicový
urychlovače	urychlovač	k1gInPc1	urychlovač
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
ke	k	k7c3	k
zkoumání	zkoumání	k1gNnSc3	zkoumání
složení	složení	k1gNnSc2	složení
hmoty	hmota	k1gFnSc2	hmota
okolo	okolo	k7c2	okolo
nás	my	k3xPp1nPc2	my
–	–	k?	–
atomů	atom	k1gInPc2	atom
<g/>
,	,	kIx,	,
elementárních	elementární	k2eAgFnPc2d1	elementární
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
kvarků	kvark	k1gInPc2	kvark
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
typ	typ	k1gInSc4	typ
urychlovače	urychlovač	k1gInSc2	urychlovač
lze	lze	k6eAd1	lze
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
považovat	považovat	k5eAaImF	považovat
i	i	k9	i
klasickou	klasický	k2eAgFnSc7d1	klasická
(	(	kIx(	(
<g/>
CRT	CRT	kA	CRT
<g/>
)	)	kIx)	)
televizní	televizní	k2eAgFnSc4d1	televizní
obrazovku	obrazovka	k1gFnSc4	obrazovka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
urychlovače	urychlovač	k1gInPc1	urychlovač
začaly	začít	k5eAaPmAgInP	začít
být	být	k5eAaImF	být
vyvíjeny	vyvíjet	k5eAaImNgInP	vyvíjet
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následujícího	následující	k2eAgNnSc2d1	následující
desetiletí	desetiletí	k1gNnSc2	desetiletí
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgInP	objevit
základní	základní	k2eAgInPc1d1	základní
principy	princip	k1gInPc1	princip
a	a	k8xC	a
postaveny	postaven	k2eAgFnPc1d1	postavena
podle	podle	k7c2	podle
nich	on	k3xPp3gInPc2	on
první	první	k4xOgInPc1	první
urychlovače	urychlovač	k1gInPc1	urychlovač
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc1	dva
základní	základní	k2eAgInPc1d1	základní
typy	typ	k1gInPc1	typ
urychlovačů	urychlovač	k1gInPc2	urychlovač
<g/>
:	:	kIx,	:
Lineární	lineární	k2eAgInSc1d1	lineární
urychlovač	urychlovač	k1gInSc1	urychlovač
Kruhový	kruhový	k2eAgInSc4d1	kruhový
urychlovač	urychlovač	k1gInSc4	urychlovač
Lineární	lineární	k2eAgInSc1d1	lineární
urychlovač	urychlovač	k1gInSc1	urychlovač
využívá	využívat	k5eAaPmIp3nS	využívat
k	k	k7c3	k
urychlení	urychlení	k1gNnSc3	urychlení
pouze	pouze	k6eAd1	pouze
elektrického	elektrický	k2eAgNnSc2d1	elektrické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
pole	pole	k1gNnSc1	pole
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
statické	statický	k2eAgNnSc1d1	statické
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
měnit	měnit	k5eAaImF	měnit
(	(	kIx(	(
<g/>
a	a	k8xC	a
částice	částice	k1gFnSc1	částice
je	být	k5eAaImIp3nS	být
urychlována	urychlovat	k5eAaImNgFnS	urychlovat
během	během	k7c2	během
letu	let	k1gInSc2	let
urychlovačem	urychlovač	k1gInSc7	urychlovač
na	na	k7c6	na
několika	několik	k4yIc6	několik
místech	místo	k1gNnPc6	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
konstrukce	konstrukce	k1gFnSc1	konstrukce
<g/>
,	,	kIx,	,
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
rozměry	rozměra	k1gFnSc2	rozměra
a	a	k8xC	a
potřeba	potřeba	k1gFnSc1	potřeba
vysokého	vysoký	k2eAgNnSc2d1	vysoké
napětí	napětí	k1gNnSc2	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
příkladem	příklad	k1gInSc7	příklad
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
je	být	k5eAaImIp3nS	být
iontová	iontový	k2eAgFnSc1d1	iontová
trubice	trubice	k1gFnSc1	trubice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kruhovém	kruhový	k2eAgInSc6d1	kruhový
urychlovači	urychlovač	k1gInSc6	urychlovač
je	být	k5eAaImIp3nS	být
dráha	dráha	k1gFnSc1	dráha
urychlovaných	urychlovaný	k2eAgFnPc2d1	urychlovaná
částic	částice	k1gFnPc2	částice
zakřivena	zakřivit	k5eAaPmNgFnS	zakřivit
magnetickým	magnetický	k2eAgNnSc7d1	magnetické
polem	pole	k1gNnSc7	pole
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
bez	bez	k7c2	bez
dalšího	další	k2eAgNnSc2d1	další
urychlování	urychlování	k1gNnSc2	urychlování
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
kruhová	kruhový	k2eAgFnSc1d1	kruhová
<g/>
.	.	kIx.	.
</s>
<s>
Částice	částice	k1gFnPc1	částice
jsou	být	k5eAaImIp3nP	být
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
u	u	k7c2	u
lineárního	lineární	k2eAgInSc2d1	lineární
urychlovače	urychlovač	k1gInSc2	urychlovač
urychlovány	urychlovat	k5eAaImNgFnP	urychlovat
elektrickým	elektrický	k2eAgNnSc7d1	elektrické
polem	pole	k1gNnSc7	pole
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
je	být	k5eAaImIp3nS	být
cyklotron	cyklotron	k1gInSc1	cyklotron
<g/>
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
po	po	k7c6	po
zakřivené	zakřivený	k2eAgFnSc6d1	zakřivená
dráze	dráha	k1gFnSc6	dráha
není	být	k5eNaImIp3nS	být
jen	jen	k9	jen
technickou	technický	k2eAgFnSc7d1	technická
komplikací	komplikace	k1gFnSc7	komplikace
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
spočívá	spočívat	k5eAaImIp3nS	spočívat
zejména	zejména	k9	zejména
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
částice	částice	k1gFnPc1	částice
pohybující	pohybující	k2eAgFnPc1d1	pohybující
se	se	k3xPyFc4	se
po	po	k7c6	po
kruhové	kruhový	k2eAgFnSc6d1	kruhová
dráze	dráha	k1gFnSc6	dráha
mají	mít	k5eAaImIp3nP	mít
velké	velký	k2eAgNnSc1d1	velké
zrychlení	zrychlení	k1gNnSc1	zrychlení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
a	a	k8xC	a
teorie	teorie	k1gFnSc2	teorie
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
pole	pole	k1gNnSc2	pole
částice	částice	k1gFnSc2	částice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
nějaké	nějaký	k3yIgNnSc4	nějaký
zrychlení	zrychlení	k1gNnSc4	zrychlení
kolmé	kolmý	k2eAgNnSc4d1	kolmé
na	na	k7c4	na
směr	směr	k1gInSc4	směr
jeho	jeho	k3xOp3gInSc2	jeho
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
záření	záření	k1gNnSc1	záření
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kruhovém	kruhový	k2eAgInSc6d1	kruhový
urychlovači	urychlovač	k1gInSc6	urychlovač
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
energie	energie	k1gFnSc1	energie
dodávaná	dodávaný	k2eAgFnSc1d1	dodávaná
částicím	částice	k1gFnPc3	částice
elektrickým	elektrický	k2eAgFnPc3d1	elektrická
polem	polem	k6eAd1	polem
snižovaná	snižovaný	k2eAgFnSc1d1	snižovaná
vlastním	vlastní	k2eAgNnSc7d1	vlastní
vyzařováním	vyzařování	k1gNnSc7	vyzařování
těchto	tento	k3xDgFnPc2	tento
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vyzařovaní	vyzařovaný	k2eAgMnPc1d1	vyzařovaný
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
snížit	snížit	k5eAaPmF	snížit
zvětšením	zvětšení	k1gNnSc7	zvětšení
poloměru	poloměr	k1gInSc2	poloměr
dráhy	dráha	k1gFnSc2	dráha
(	(	kIx(	(
<g/>
dostředivá	dostředivý	k2eAgFnSc1d1	dostředivá
síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
nepřímo	přímo	k6eNd1	přímo
úměrná	úměrná	k1gFnSc1	úměrná
poloměru	poloměr	k1gInSc2	poloměr
dráhy	dráha	k1gFnSc2	dráha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
na	na	k7c6	na
dosahovaní	dosahovaný	k2eAgMnPc1d1	dosahovaný
vyšších	vysoký	k2eAgFnPc2d2	vyšší
energií	energie	k1gFnPc2	energie
kruhové	kruhový	k2eAgInPc1d1	kruhový
urychlovače	urychlovač	k1gInPc1	urychlovač
stále	stále	k6eAd1	stále
větších	veliký	k2eAgInPc2d2	veliký
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
největší	veliký	k2eAgMnSc1d3	veliký
je	být	k5eAaImIp3nS	být
LHC	LHC	kA	LHC
<g/>
.	.	kIx.	.
</s>
<s>
CERN-Evropská	CERN-Evropský	k2eAgFnSc1d1	CERN-Evropský
organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
jaderný	jaderný	k2eAgInSc4d1	jaderný
výzkum	výzkum	k1gInSc4	výzkum
Synchrotron	synchrotron	k1gInSc1	synchrotron
Large	Large	k1gFnSc1	Large
Hadron	Hadron	k1gMnSc1	Hadron
Collider	Collider	k1gInSc1	Collider
(	(	kIx(	(
<g/>
Velký	velký	k2eAgInSc1d1	velký
hadronový	hadronový	k2eAgInSc1d1	hadronový
urychlovač	urychlovač	k1gInSc1	urychlovač
<g/>
)	)	kIx)	)
</s>
