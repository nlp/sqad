<p>
<s>
Mahákášjapa	Mahákášjapa	k1gFnSc1	Mahákášjapa
(	(	kIx(	(
<g/>
sanskrt	sanskrt	k1gInSc1	sanskrt
<g/>
;	;	kIx,	;
v	v	k7c6	v
páli	pál	k1gFnSc6	pál
Mahákássapa	Mahákássap	k1gMnSc2	Mahákássap
<g/>
,	,	kIx,	,
čínsky	čínsky	k6eAd1	čínsky
摩	摩	k?	摩
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Kášjapa	Kášjap	k1gMnSc4	Kášjap
Veliký	veliký	k2eAgInSc1d1	veliký
nebo	nebo	k8xC	nebo
Velký	velký	k2eAgInSc1d1	velký
Kášjapa	Kášjapa	k1gFnSc1	Kášjapa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
jen	jen	k9	jen
Kášjapa	Kášjap	k1gMnSc4	Kášjap
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
předních	přední	k2eAgMnPc2d1	přední
žáků	žák	k1gMnPc2	žák
Buddhy	Buddha	k1gMnSc2	Buddha
Šákjamuniho	Šákjamuni	k1gMnSc2	Šákjamuni
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zenu	zenus	k1gInSc6	zenus
je	být	k5eAaImIp3nS	být
Mahákášjapa	Mahákášjap	k1gMnSc4	Mahákášjap
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c2	za
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
patriarchu	patriarcha	k1gMnSc4	patriarcha
<g/>
,	,	kIx,	,
v	v	k7c6	v
čínských	čínský	k2eAgInPc6d1	čínský
klášterech	klášter	k1gInPc6	klášter
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
socha	socha	k1gFnSc1	socha
nachází	nacházet	k5eAaImIp3nS	nacházet
po	po	k7c6	po
Buddhově	Buddhův	k2eAgFnSc6d1	Buddhova
pravici	pravice	k1gFnSc6	pravice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Za	za	k7c2	za
života	život	k1gInSc2	život
Buddhy	Buddha	k1gMnSc2	Buddha
===	===	k?	===
</s>
</p>
<p>
<s>
Mahákášjapa	Mahákášjap	k1gMnSc4	Mahákášjap
prý	prý	k9	prý
žil	žít	k5eAaImAgInS	žít
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
chotí	choť	k1gFnSc7	choť
dvanáct	dvanáct	k4xCc4	dvanáct
let	let	k1gInSc4	let
asketickým	asketický	k2eAgInSc7d1	asketický
životem	život	k1gInSc7	život
a	a	k8xC	a
v	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
cudnosti	cudnost	k1gFnSc6	cudnost
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Buddhou	Buddha	k1gMnSc7	Buddha
<g/>
,	,	kIx,	,
okamžitě	okamžitě	k6eAd1	okamžitě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
žákem	žák	k1gMnSc7	žák
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
šíření	šíření	k1gNnSc6	šíření
nauky	nauka	k1gFnSc2	nauka
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
znám	znám	k2eAgMnSc1d1	znám
především	především	k9	především
svou	svůj	k3xOyFgFnSc7	svůj
přísnou	přísný	k2eAgFnSc7d1	přísná
askezí	askeze	k1gFnSc7	askeze
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
Buddha	Buddha	k1gMnSc1	Buddha
Šákjamuni	Šákjamueň	k1gFnSc3	Šákjamueň
mnohdy	mnohdy	k6eAd1	mnohdy
nabízel	nabízet	k5eAaImAgInS	nabízet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
někdy	někdy	k6eAd1	někdy
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
askeze	askeze	k1gFnSc2	askeze
upustil	upustit	k5eAaPmAgMnS	upustit
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgMnS	přijmout
pozvání	pozvání	k1gNnSc4	pozvání
do	do	k7c2	do
domu	dům	k1gInSc2	dům
některého	některý	k3yIgMnSc2	některý
stoupence	stoupenec	k1gMnSc2	stoupenec
z	z	k7c2	z
laické	laický	k2eAgFnSc2d1	laická
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
Mahákášjapa	Mahákášjapa	k1gFnSc1	Mahákášjapa
vždy	vždy	k6eAd1	vždy
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
Buddha	Buddha	k1gMnSc1	Buddha
umřel	umřít	k5eAaPmAgMnS	umřít
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
tělo	tělo	k1gNnSc4	tělo
položené	položený	k2eAgNnSc4d1	položené
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
zapálit	zapálit	k5eAaPmF	zapálit
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
dorazil	dorazit	k5eAaPmAgMnS	dorazit
i	i	k9	i
Kášjapa	Kášjap	k1gMnSc4	Kášjap
<g/>
,	,	kIx,	,
hranice	hranice	k1gFnSc1	hranice
vzplála	vzplát	k5eAaPmAgFnS	vzplát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgInSc4	první
buddhistický	buddhistický	k2eAgInSc4d1	buddhistický
koncil	koncil	k1gInSc4	koncil
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Buddhově	Buddhův	k2eAgFnSc6d1	Buddhova
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
Mahákášjapa	Mahákášjapa	k1gFnSc1	Mahákášjapa
ujal	ujmout	k5eAaPmAgInS	ujmout
vedení	vedení	k1gNnSc4	vedení
sanghy	sangha	k1gFnSc2	sangha
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zastáncem	zastánce	k1gMnSc7	zastánce
přísného	přísný	k2eAgNnSc2d1	přísné
dodržování	dodržování	k1gNnSc2	dodržování
vinajanových	vinajanův	k2eAgFnPc2d1	vinajanův
zásad	zásada	k1gFnPc2	zásada
<g/>
.	.	kIx.	.
</s>
<s>
Necelý	celý	k2eNgInSc4d1	necelý
rok	rok	k1gInSc4	rok
po	po	k7c6	po
Buddhově	Buddhův	k2eAgFnSc6d1	Buddhova
smrti	smrt	k1gFnSc6	smrt
svolal	svolat	k5eAaPmAgInS	svolat
první	první	k4xOgInSc4	první
buddhistický	buddhistický	k2eAgInSc4d1	buddhistický
koncil	koncil	k1gInSc4	koncil
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
nebyl	být	k5eNaImAgMnS	být
spokojen	spokojen	k2eAgMnSc1d1	spokojen
s	s	k7c7	s
dodržováním	dodržování	k1gNnSc7	dodržování
řádových	řádový	k2eAgNnPc2d1	řádové
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
co	co	k9	co
se	se	k3xPyFc4	se
týše	týše	k1gFnSc1	týše
mnichů	mnich	k1gMnPc2	mnich
ve	v	k7c6	v
Vaišálí	Vaišálý	k2eAgMnPc5d1	Vaišálý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
koncilu	koncil	k1gInSc6	koncil
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
roztržce	roztržka	k1gFnSc3	roztržka
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gNnSc7	on
a	a	k8xC	a
Ánandou	Ánanda	k1gFnSc7	Ánanda
<g/>
.	.	kIx.	.
</s>
<s>
Mahákášjapa	Mahákášjap	k1gMnSc4	Mahákášjap
vyčítal	vyčítat	k5eAaImAgMnS	vyčítat
Ánandovi	Ánand	k1gMnSc3	Ánand
<g/>
,	,	kIx,	,
že	že	k8xS	že
doposud	doposud	k6eAd1	doposud
nedosáhl	dosáhnout	k5eNaPmAgMnS	dosáhnout
stavu	stav	k1gInSc2	stav
arhata	arhat	k1gMnSc2	arhat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
koncilu	koncil	k1gInSc2	koncil
účastnili	účastnit	k5eAaImAgMnP	účastnit
prakticky	prakticky	k6eAd1	prakticky
samí	samý	k3xTgMnPc1	samý
arhati	arhat	k1gMnPc1	arhat
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
mu	on	k3xPp3gMnSc3	on
vyčítal	vyčítat	k5eAaImAgMnS	vyčítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
řádu	řád	k1gInSc2	řád
mnišek	mniška	k1gFnPc2	mniška
<g/>
,	,	kIx,	,
když	když	k8xS	když
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
prosil	prosít	k5eAaPmAgInS	prosít
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
matce	matka	k1gFnSc3	matka
samotného	samotný	k2eAgMnSc4d1	samotný
Buddhu	Buddha	k1gMnSc4	Buddha
<g/>
.	.	kIx.	.
</s>
<s>
Ánanda	Ánand	k1gMnSc4	Ánand
dále	daleko	k6eAd2	daleko
nechtěl	chtít	k5eNaImAgMnS	chtít
po	po	k7c6	po
Buddhovi	Buddha	k1gMnSc6	Buddha
Šákjamunim	Šákjamunim	k1gMnSc1	Šákjamunim
přesné	přesný	k2eAgFnPc4d1	přesná
instrukce	instrukce	k1gFnPc4	instrukce
ohledně	ohledně	k6eAd1	ohledně
vinajapitaky	vinajapitak	k1gInPc1	vinajapitak
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
to	ten	k3xDgNnSc1	ten
za	za	k7c4	za
následek	následek	k1gInSc4	následek
pozdější	pozdní	k2eAgNnSc4d2	pozdější
nedodržování	nedodržování	k1gNnSc4	nedodržování
řádových	řádový	k2eAgNnPc2d1	řádové
pravidel	pravidlo	k1gNnPc2	pravidlo
a	a	k8xC	a
štěpení	štěpení	k1gNnSc1	štěpení
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
věcí	věc	k1gFnSc7	věc
se	se	k3xPyFc4	se
Ánanda	Ánanda	k1gFnSc1	Ánanda
provinil	provinit	k5eAaPmAgInS	provinit
v	v	k7c6	v
Kášjapových	Kášjapův	k2eAgNnPc6d1	Kášjapův
očích	oko	k1gNnPc6	oko
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepožádal	požádat	k5eNaPmAgInS	požádat
Buddhu	Buddha	k1gMnSc4	Buddha
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
prodloužil	prodloužit	k5eAaPmAgMnS	prodloužit
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
koncilu	koncil	k1gInSc6	koncil
se	se	k3xPyFc4	se
Mahákášjapa	Mahákášjapa	k1gFnSc1	Mahákášjapa
dotazoval	dotazovat	k5eAaImAgMnS	dotazovat
Ánandy	Ánand	k1gMnPc4	Ánand
na	na	k7c4	na
Buddhovy	Buddhův	k2eAgFnPc4d1	Buddhova
promluvy	promluva	k1gFnPc4	promluva
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
první	první	k4xOgInSc1	první
oddíl	oddíl	k1gInSc1	oddíl
Tipitaky	Tipitak	k1gInPc1	Tipitak
(	(	kIx(	(
<g/>
sa	sa	k?	sa
<g/>
.	.	kIx.	.
</s>
<s>
Tripitaka	Tripitak	k1gMnSc4	Tripitak
<g/>
)	)	kIx)	)
Suttapitaka	Suttapitak	k1gMnSc4	Suttapitak
(	(	kIx(	(
<g/>
sa	sa	k?	sa
<g/>
.	.	kIx.	.
</s>
<s>
Sútrapitaka	Sútrapitak	k1gMnSc4	Sútrapitak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Systematicky	systematicky	k6eAd1	systematicky
kladl	klást	k5eAaImAgMnS	klást
otázky	otázka	k1gFnPc4	otázka
také	také	k9	také
mnichu	mnich	k1gMnSc3	mnich
Upálimu	Upálim	k1gMnSc3	Upálim
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
odpovědí	odpověď	k1gFnPc2	odpověď
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
Tripitaky	Tripitak	k1gInPc4	Tripitak
<g/>
,	,	kIx,	,
Vinajapitaka	Vinajapitak	k1gMnSc4	Vinajapitak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Čekání	čekání	k1gNnSc1	čekání
na	na	k7c4	na
Maitréju	Maitréju	k1gFnSc4	Maitréju
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
byl	být	k5eAaImAgMnS	být
Mahákášjapa	Mahákášjap	k1gMnSc4	Mahákášjap
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
hlubokého	hluboký	k2eAgNnSc2d1	hluboké
samádhi	samádhi	k1gNnSc2	samádhi
do	do	k7c2	do
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
očekává	očekávat	k5eAaImIp3nS	očekávat
příchod	příchod	k1gInSc1	příchod
buddhy	buddha	k1gMnSc2	buddha
příštího	příští	k2eAgInSc2d1	příští
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
Maitréji	Maitréje	k1gFnSc4	Maitréje
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
tam	tam	k6eAd1	tam
Šákjamuniho	Šákjamuni	k1gMnSc2	Šákjamuni
roucho	roucho	k1gNnSc4	roucho
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
předá	předat	k5eAaPmIp3nS	předat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
FILIPSKÝ	FILIPSKÝ	kA	FILIPSKÝ
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
LIŠČÁK	LIŠČÁK	kA	LIŠČÁK
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
;	;	kIx,	;
HEROLDOVÁ	HEROLDOVÁ	kA	HEROLDOVÁ
<g/>
,	,	kIx,	,
Helena	Helena	k1gFnSc1	Helena
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Lexikon	lexikon	k1gInSc1	lexikon
východní	východní	k2eAgFnSc2d1	východní
moudrosti	moudrost	k1gFnSc2	moudrost
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Votobia	Votobia	k1gFnSc1	Votobia
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7198	[number]	k4	7198
<g/>
-	-	kIx~	-
<g/>
168	[number]	k4	168
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LOPEZ	LOPEZ	kA	LOPEZ
<g/>
,	,	kIx,	,
Donald	Donald	k1gMnSc1	Donald
S.	S.	kA	S.
Příběh	příběh	k1gInSc1	příběh
buddhismu	buddhismus	k1gInSc2	buddhismus
<g/>
:	:	kIx,	:
průvodce	průvodce	k1gMnSc1	průvodce
dějinami	dějiny	k1gFnPc7	dějiny
buddhismu	buddhismus	k1gInSc2	buddhismus
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
učením	učení	k1gNnSc7	učení
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Barrister	Barrister	k1gMnSc1	Barrister
&	&	k?	&
Principal	Principal	k1gMnSc1	Principal
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86598	[number]	k4	86598
<g/>
-	-	kIx~	-
<g/>
54	[number]	k4	54
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MILTNER	MILTNER	kA	MILTNER
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
buddhismu	buddhismus	k1gInSc2	buddhismus
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
410	[number]	k4	410
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WERNER	Werner	k1gMnSc1	Werner
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Náboženské	náboženský	k2eAgFnPc1d1	náboženská
tradice	tradice	k1gFnPc1	tradice
Asie	Asie	k1gFnSc2	Asie
<g/>
:	:	kIx,	:
od	od	k7c2	od
Indie	Indie	k1gFnSc2	Indie
po	po	k7c4	po
Japonsko	Japonsko	k1gNnSc4	Japonsko
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
210	[number]	k4	210
<g/>
-	-	kIx~	-
<g/>
2978	[number]	k4	2978
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZOTZ	ZOTZ	kA	ZOTZ
<g/>
,	,	kIx,	,
Volker	Volker	k1gMnSc1	Volker
<g/>
.	.	kIx.	.
</s>
<s>
Buddha	Buddha	k1gMnSc1	Buddha
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Votobia	Votobia	k1gFnSc1	Votobia
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85885	[number]	k4	85885
<g/>
-	-	kIx~	-
<g/>
72	[number]	k4	72
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
buddhistický	buddhistický	k2eAgInSc1d1	buddhistický
koncil	koncil	k1gInSc1	koncil
</s>
</p>
