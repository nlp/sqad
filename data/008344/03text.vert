<p>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1503	[number]	k4	1503
Alcalá	Alcalý	k2eAgFnSc1d1	Alcalá
de	de	k?	de
Henares	Henares	k1gInSc1	Henares
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1564	[number]	k4	1564
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
římskoněmecký	římskoněmecký	k2eAgMnSc1d1	římskoněmecký
král	král	k1gMnSc1	král
(	(	kIx(	(
<g/>
od	od	k7c2	od
1531	[number]	k4	1531
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
(	(	kIx(	(
<g/>
od	od	k7c2	od
1556	[number]	k4	1556
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1526	[number]	k4	1526
<g/>
)	)	kIx)	)
a	a	k8xC	a
rakouský	rakouský	k2eAgMnSc1d1	rakouský
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
rodiči	rodič	k1gMnPc7	rodič
byli	být	k5eAaImAgMnP	být
Habsburk	Habsburk	k1gMnSc1	Habsburk
Filip	Filip	k1gMnSc1	Filip
I.	I.	kA	I.
Sličný	sličný	k2eAgMnSc1d1	sličný
a	a	k8xC	a
španělská	španělský	k2eAgFnSc1d1	španělská
infantka	infantka	k1gFnSc1	infantka
Jana	Jana	k1gFnSc1	Jana
Šílená	šílený	k2eAgFnSc1d1	šílená
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starší	k1gMnSc1	starší
bratr	bratr	k1gMnSc1	bratr
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
roku	rok	k1gInSc2	rok
1516	[number]	k4	1516
španělským	španělský	k2eAgMnSc7d1	španělský
králem	král	k1gMnSc7	král
a	a	k8xC	a
1519	[number]	k4	1519
římským	římský	k2eAgMnSc7d1	římský
císařem	císař	k1gMnSc7	císař
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinandovou	Ferdinandův	k2eAgFnSc7d1	Ferdinandova
manželkou	manželka	k1gFnSc7	manželka
byla	být	k5eAaImAgFnS	být
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
uherská	uherský	k2eAgFnSc1d1	uherská
princezna	princezna	k1gFnSc1	princezna
Anna	Anna	k1gFnSc1	Anna
Jagellonská	jagellonský	k2eAgFnSc1d1	Jagellonská
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
spolu	spolu	k6eAd1	spolu
patnáct	patnáct	k4xCc4	patnáct
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
Ferdinandova	Ferdinandův	k2eAgMnSc2d1	Ferdinandův
nástupce	nástupce	k1gMnSc2	nástupce
Maxmiliána	Maxmilián	k1gMnSc2	Maxmilián
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dětství	dětství	k1gNnSc4	dětství
==	==	k?	==
</s>
</p>
<p>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1503	[number]	k4	1503
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Alcalá	Alcalý	k2eAgFnSc1d1	Alcalá
de	de	k?	de
Henares	Henares	k1gInSc4	Henares
40	[number]	k4	40
km	km	kA	km
od	od	k7c2	od
Madridu	Madrid	k1gInSc2	Madrid
jako	jako	k8xS	jako
druhorozený	druhorozený	k2eAgMnSc1d1	druhorozený
syn	syn	k1gMnSc1	syn
kastilského	kastilský	k2eAgMnSc2d1	kastilský
spolukrále	spolukrál	k1gMnSc2	spolukrál
a	a	k8xC	a
burgundského	burgundský	k2eAgMnSc2d1	burgundský
vévody	vévoda	k1gMnSc2	vévoda
Filipa	Filip	k1gMnSc2	Filip
I.	I.	kA	I.
Sličného	sličný	k2eAgNnSc2d1	sličné
a	a	k8xC	a
kastilské	kastilský	k2eAgFnPc1d1	Kastilská
královny	královna	k1gFnPc1	královna
Johany	Johana	k1gFnSc2	Johana
(	(	kIx(	(
<g/>
Jany	Jana	k1gFnSc2	Jana
<g/>
)	)	kIx)	)
Šílené	šílený	k2eAgFnPc1d1	šílená
<g/>
.	.	kIx.	.
</s>
<s>
Dětství	dětství	k1gNnSc4	dětství
prožil	prožít	k5eAaPmAgMnS	prožít
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1506	[number]	k4	1506
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Ferdinandův	Ferdinandův	k2eAgMnSc1d1	Ferdinandův
otec	otec	k1gMnSc1	otec
Filip	Filip	k1gMnSc1	Filip
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jeho	jeho	k3xOp3gFnSc2	jeho
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
jedinou	jediný	k2eAgFnSc7d1	jediná
dědičkou	dědička	k1gFnSc7	dědička
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
vládců	vládce	k1gMnPc2	vládce
nedávno	nedávno	k6eAd1	nedávno
sjednocených	sjednocený	k2eAgFnPc2d1	sjednocená
španělských	španělský	k2eAgFnPc2d1	španělská
království	království	k1gNnPc2	království
Kastilie	Kastilie	k1gFnSc2	Kastilie
a	a	k8xC	a
Aragonie	Aragonie	k1gFnSc2	Aragonie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
projevovala	projevovat	k5eAaImAgFnS	projevovat
duševní	duševní	k2eAgFnSc1d1	duševní
choroba	choroba	k1gFnSc1	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
aragonský	aragonský	k2eAgMnSc1d1	aragonský
král	král	k1gMnSc1	král
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
shledal	shledat	k5eAaPmAgInS	shledat
neschopnou	schopný	k2eNgFnSc4d1	neschopná
vlády	vláda	k1gFnPc4	vláda
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
izolovat	izolovat	k5eAaBmF	izolovat
<g/>
.	.	kIx.	.
</s>
<s>
Postaral	postarat	k5eAaPmAgMnS	postarat
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
o	o	k7c4	o
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
a	a	k8xC	a
výchovu	výchova	k1gFnSc4	výchova
jejích	její	k3xOp3gFnPc2	její
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
veden	vést	k5eAaImNgMnS	vést
k	k	k7c3	k
přísně	přísně	k6eAd1	přísně
katolickému	katolický	k2eAgInSc3d1	katolický
životu	život	k1gInSc3	život
a	a	k8xC	a
k	k	k7c3	k
respektování	respektování	k1gNnSc3	respektování
habsburských	habsburský	k2eAgFnPc2d1	habsburská
rodových	rodový	k2eAgFnPc2d1	rodová
idejí	idea	k1gFnPc2	idea
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
myšlenkami	myšlenka	k1gFnPc7	myšlenka
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
renesančních	renesanční	k2eAgMnPc2d1	renesanční
učenců	učenec	k1gMnPc2	učenec
Erasma	Erasm	k1gMnSc2	Erasm
Rotterdamského	rotterdamský	k2eAgMnSc2d1	rotterdamský
měl	mít	k5eAaImAgInS	mít
tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
největší	veliký	k2eAgInSc4d3	veliký
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
formování	formování	k1gNnSc4	formování
Ferdinandovy	Ferdinandův	k2eAgFnSc2d1	Ferdinandova
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1516	[number]	k4	1516
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Aragonský	aragonský	k2eAgInSc1d1	aragonský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
životě	život	k1gInSc6	život
třináctiletého	třináctiletý	k2eAgMnSc2d1	třináctiletý
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
to	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
zásadní	zásadní	k2eAgInSc4d1	zásadní
zvrat	zvrat	k1gInSc4	zvrat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příštím	příští	k2eAgInSc6d1	příští
roce	rok	k1gInSc6	rok
přijel	přijet	k5eAaPmAgMnS	přijet
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
jeho	jeho	k3xOp3gMnSc4	jeho
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
Karel	Karel	k1gMnSc1	Karel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dosud	dosud	k6eAd1	dosud
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c6	v
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Bratři	bratr	k1gMnPc1	bratr
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
setkali	setkat	k5eAaPmAgMnP	setkat
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
neoplýval	oplývat	k5eNaImAgInS	oplývat
vzájemnou	vzájemný	k2eAgFnSc7d1	vzájemná
sympatií	sympatie	k1gFnSc7	sympatie
a	a	k8xC	a
důvěrou	důvěra	k1gFnSc7	důvěra
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
postupovali	postupovat	k5eAaImAgMnP	postupovat
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
při	při	k7c6	při
prosazování	prosazování	k1gNnSc6	prosazování
společných	společný	k2eAgInPc2d1	společný
zájmů	zájem	k1gInPc2	zájem
habsburského	habsburský	k2eAgInSc2d1	habsburský
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
vleklých	vleklý	k2eAgNnPc6d1	vleklé
jednáních	jednání	k1gNnPc6	jednání
přijat	přijat	k2eAgMnSc1d1	přijat
jako	jako	k8xS	jako
španělský	španělský	k2eAgMnSc1d1	španělský
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
rozpustil	rozpustit	k5eAaPmAgMnS	rozpustit
Ferdinandův	Ferdinandův	k2eAgInSc4d1	Ferdinandův
dvůr	dvůr	k1gInSc4	dvůr
a	a	k8xC	a
poslal	poslat	k5eAaPmAgMnS	poslat
mladšího	mladý	k2eAgMnSc4d2	mladší
bratra	bratr	k1gMnSc4	bratr
k	k	k7c3	k
tetě	teta	k1gFnSc3	teta
Markétě	Markéta	k1gFnSc3	Markéta
do	do	k7c2	do
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rakouský	rakouský	k2eAgMnSc1d1	rakouský
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1519	[number]	k4	1519
zemřel	zemřít	k5eAaPmAgMnS	zemřít
druhý	druhý	k4xOgMnSc1	druhý
Ferdinandův	Ferdinandův	k2eAgMnSc1d1	Ferdinandův
děd	děd	k1gMnSc1	děd
<g/>
,	,	kIx,	,
císař	císař	k1gMnSc1	císař
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
I.	I.	kA	I.
Habsburský	habsburský	k2eAgInSc4d1	habsburský
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
jediným	jediný	k2eAgMnSc7d1	jediný
vládcem	vládce	k1gMnSc7	vládce
v	v	k7c6	v
rakouských	rakouský	k2eAgFnPc6d1	rakouská
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
nebylo	být	k5eNaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
jednotným	jednotný	k2eAgInSc7d1	jednotný
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
samostatná	samostatný	k2eAgNnPc4d1	samostatné
knížectví	knížectví	k1gNnPc4	knížectví
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
spojovala	spojovat	k5eAaImAgFnS	spojovat
vláda	vláda	k1gFnSc1	vláda
příslušníků	příslušník	k1gMnPc2	příslušník
rodu	rod	k1gInSc2	rod
Habsburků	Habsburk	k1gMnPc2	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
závěti	závěť	k1gFnSc6	závěť
nestanovil	stanovit	k5eNaPmAgInS	stanovit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
tyto	tento	k3xDgFnPc1	tento
země	zem	k1gFnPc1	zem
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
nebyl	být	k5eNaImAgInS	být
ustanoven	ustanoven	k2eAgInSc1d1	ustanoven
princip	princip	k1gInSc1	princip
primogenitury	primogenitura	k1gFnSc2	primogenitura
<g/>
.	.	kIx.	.
</s>
<s>
Nastala	nastat	k5eAaPmAgNnP	nastat
tedy	tedy	k9	tedy
jednání	jednání	k1gNnPc1	jednání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1520	[number]	k4	1520
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
získal	získat	k5eAaPmAgMnS	získat
wormskou	wormský	k2eAgFnSc7d1	wormský
smlouvou	smlouva	k1gFnSc7	smlouva
Horní	horní	k2eAgFnPc1d1	horní
a	a	k8xC	a
Dolní	dolní	k2eAgInPc1d1	dolní
Rakousy	Rakousy	k1gInPc1	Rakousy
<g/>
,	,	kIx,	,
Korutany	Korutany	k1gInPc1	Korutany
<g/>
,	,	kIx,	,
Štýrsko	Štýrsko	k1gNnSc1	Štýrsko
a	a	k8xC	a
Kraňsko	Kraňsko	k1gNnSc1	Kraňsko
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
bohatého	bohatý	k2eAgNnSc2d1	bohaté
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
<g/>
,	,	kIx,	,
připadla	připadnout	k5eAaPmAgFnS	připadnout
Karlovi	Karel	k1gMnSc3	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
stal	stát	k5eAaPmAgMnS	stát
králem	král	k1gMnSc7	král
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
(	(	kIx(	(
<g/>
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1530	[number]	k4	1530
císařem	císař	k1gMnSc7	císař
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
plně	plně	k6eAd1	plně
věnovat	věnovat	k5eAaPmF	věnovat
vládě	vláda	k1gFnSc3	vláda
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
a	a	k8xC	a
v	v	k7c6	v
Říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
svým	svůj	k3xOyFgMnPc3	svůj
místodržícím	místodržící	k1gMnPc3	místodržící
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
rakouských	rakouský	k2eAgFnPc6d1	rakouská
zemích	zem	k1gFnPc6	zem
<g/>
;	;	kIx,	;
plnou	plný	k2eAgFnSc4d1	plná
vládu	vláda	k1gFnSc4	vláda
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
Karel	Karel	k1gMnSc1	Karel
přenechal	přenechat	k5eAaPmAgInS	přenechat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1522	[number]	k4	1522
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
položen	položen	k2eAgInSc4d1	položen
základ	základ	k1gInSc4	základ
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
rodu	rod	k1gInSc2	rod
Habsburků	Habsburk	k1gMnPc2	Habsburk
na	na	k7c4	na
starší	starý	k2eAgFnSc4d2	starší
španělskou	španělský	k2eAgFnSc4d1	španělská
větev	větev	k1gFnSc4	větev
(	(	kIx(	(
<g/>
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
<g/>
)	)	kIx)	)
a	a	k8xC	a
mladší	mladý	k2eAgFnSc4d2	mladší
rakouskou	rakouský	k2eAgFnSc4d1	rakouská
větev	větev	k1gFnSc4	větev
(	(	kIx(	(
<g/>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
opět	opět	k6eAd1	opět
jediným	jediný	k2eAgMnSc7d1	jediný
vládcem	vládce	k1gMnSc7	vládce
rakouských	rakouský	k2eAgFnPc2d1	rakouská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
dorazil	dorazit	k5eAaPmAgMnS	dorazit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1521	[number]	k4	1521
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
nesetkával	setkávat	k5eNaImAgMnS	setkávat
se	se	k3xPyFc4	se
zpočátku	zpočátku	k6eAd1	zpočátku
se	s	k7c7	s
sympatiemi	sympatie	k1gFnPc7	sympatie
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vnímán	vnímat	k5eAaImNgMnS	vnímat
jako	jako	k9	jako
cizinec	cizinec	k1gMnSc1	cizinec
(	(	kIx(	(
<g/>
Španěl	Španěl	k1gMnSc1	Španěl
<g/>
)	)	kIx)	)
a	a	k8xC	a
neuměl	umět	k5eNaImAgMnS	umět
německy	německy	k6eAd1	německy
<g/>
.	.	kIx.	.
</s>
<s>
Šlechta	šlechta	k1gFnSc1	šlechta
navíc	navíc	k6eAd1	navíc
využila	využít	k5eAaPmAgFnS	využít
dvouletého	dvouletý	k2eAgNnSc2d1	dvouleté
bezvládí	bezvládí	k1gNnSc2	bezvládí
k	k	k7c3	k
posílení	posílení	k1gNnSc3	posílení
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Ambiciózní	ambiciózní	k2eAgMnSc1d1	ambiciózní
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
se	se	k3xPyFc4	se
však	však	k9	však
ujal	ujmout	k5eAaPmAgInS	ujmout
vlády	vláda	k1gFnSc2	vláda
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
odhodláním	odhodlání	k1gNnSc7	odhodlání
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
musel	muset	k5eAaImAgInS	muset
čelit	čelit	k5eAaImF	čelit
vzpouře	vzpoura	k1gFnSc3	vzpoura
stavovské	stavovský	k2eAgFnSc2d1	stavovská
opozice	opozice	k1gFnSc2	opozice
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
podařilo	podařit	k5eAaPmAgNnS	podařit
potlačit	potlačit	k5eAaPmF	potlačit
pomocí	pomocí	k7c2	pomocí
represí	represe	k1gFnPc2	represe
<g/>
,	,	kIx,	,
namířených	namířený	k2eAgFnPc2d1	namířená
především	především	k6eAd1	především
proti	proti	k7c3	proti
městům	město	k1gNnPc3	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1522	[number]	k4	1522
pak	pak	k6eAd1	pak
byli	být	k5eAaImAgMnP	být
odsouzeni	odsoudit	k5eAaPmNgMnP	odsoudit
a	a	k8xC	a
popraveni	popraven	k2eAgMnPc1d1	popraven
vůdci	vůdce	k1gMnPc1	vůdce
vzpoury	vzpoura	k1gFnSc2	vzpoura
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
krvavý	krvavý	k2eAgInSc4d1	krvavý
soud	soud	k1gInSc4	soud
<g/>
"	"	kIx"	"
ve	v	k7c6	v
Vídeňském	vídeňský	k2eAgNnSc6d1	Vídeňské
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rázným	rázný	k2eAgInSc7d1	rázný
postupem	postup	k1gInSc7	postup
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
upevnil	upevnit	k5eAaPmAgMnS	upevnit
svoji	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
nastoupení	nastoupení	k1gNnSc6	nastoupení
vlády	vláda	k1gFnSc2	vláda
musel	muset	k5eAaImAgInS	muset
zahájit	zahájit	k5eAaPmF	zahájit
hospodářské	hospodářský	k2eAgFnPc4d1	hospodářská
reformy	reforma	k1gFnPc4	reforma
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
slíbil	slíbit	k5eAaPmAgMnS	slíbit
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
I.	I.	kA	I.
kvůli	kvůli	k7c3	kvůli
nutnosti	nutnost	k1gFnSc3	nutnost
řešení	řešení	k1gNnSc2	řešení
vážné	vážný	k2eAgFnSc2d1	vážná
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
provedl	provést	k5eAaPmAgMnS	provést
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
peněžní	peněžní	k2eAgFnSc4d1	peněžní
reformu	reforma	k1gFnSc4	reforma
a	a	k8xC	a
zavedl	zavést	k5eAaPmAgInS	zavést
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
měnu	měna	k1gFnSc4	měna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1524	[number]	k4	1524
<g/>
–	–	k?	–
<g/>
1525	[number]	k4	1525
probíhalo	probíhat	k5eAaImAgNnS	probíhat
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
části	část	k1gFnSc6	část
říše	říš	k1gFnSc2	říš
selské	selské	k1gNnSc4	selské
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Zasáhlo	zasáhnout	k5eAaPmAgNnS	zasáhnout
i	i	k9	i
některé	některý	k3yIgFnPc4	některý
části	část	k1gFnPc4	část
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
šlechty	šlechta	k1gFnSc2	šlechta
se	se	k3xPyFc4	se
Ferdinandovi	Ferdinand	k1gMnSc3	Ferdinand
podařilo	podařit	k5eAaPmAgNnS	podařit
povstání	povstání	k1gNnSc4	povstání
ve	v	k7c6	v
Štýrsku	Štýrsko	k1gNnSc6	Štýrsko
potlačit	potlačit	k5eAaPmF	potlačit
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tyrolsku	Tyrolsko	k1gNnSc6	Tyrolsko
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
postupovat	postupovat	k5eAaImF	postupovat
více	hodně	k6eAd2	hodně
diplomaticky	diplomaticky	k6eAd1	diplomaticky
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
díky	díky	k7c3	díky
ústupkům	ústupek	k1gInPc3	ústupek
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
zklidnění	zklidnění	k1gNnSc4	zklidnění
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinandovi	Ferdinand	k1gMnSc3	Ferdinand
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
podařilo	podařit	k5eAaPmAgNnS	podařit
udržet	udržet	k5eAaPmF	udržet
jednotnost	jednotnost	k1gFnSc4	jednotnost
rakouských	rakouský	k2eAgFnPc2d1	rakouská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tvůrce	tvůrce	k1gMnSc2	tvůrce
habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1515	[number]	k4	1515
jeho	jeho	k3xOp3gFnSc3	jeho
děd	děd	k1gMnSc1	děd
–	–	k?	–
císař	císař	k1gMnSc1	císař
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
I.	I.	kA	I.
a	a	k8xC	a
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
sňatkovou	sňatkový	k2eAgFnSc4d1	sňatková
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
Maxmiliánových	Maxmiliánův	k2eAgMnPc2d1	Maxmiliánův
vnuků	vnuk	k1gMnPc2	vnuk
(	(	kIx(	(
<g/>
až	až	k9	až
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
určeno	určit	k5eAaPmNgNnS	určit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
<g/>
)	)	kIx)	)
měl	mít	k5eAaImAgMnS	mít
stát	stát	k5eAaPmF	stát
manželem	manžel	k1gMnSc7	manžel
Vladislavovy	Vladislavův	k2eAgFnSc2d1	Vladislavova
dcery	dcera	k1gFnSc2	dcera
Anny	Anna	k1gFnSc2	Anna
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
Vladislavův	Vladislavův	k2eAgMnSc1d1	Vladislavův
syn	syn	k1gMnSc1	syn
Ludvík	Ludvík	k1gMnSc1	Ludvík
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
oženit	oženit	k5eAaPmF	oženit
s	s	k7c7	s
Ferdinandovou	Ferdinandův	k2eAgFnSc7d1	Ferdinandova
mladší	mladý	k2eAgFnSc7d2	mladší
sestrou	sestra	k1gFnSc7	sestra
Marií	Maria	k1gFnPc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Svatba	svatba	k1gFnSc1	svatba
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
a	a	k8xC	a
Anny	Anna	k1gFnSc2	Anna
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
o	o	k7c4	o
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1521	[number]	k4	1521
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důležitost	důležitost	k1gFnSc1	důležitost
sňatku	sňatek	k1gInSc2	sňatek
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Moháče	Moháč	k1gInSc2	Moháč
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1526	[number]	k4	1526
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zahynul	zahynout	k5eAaPmAgMnS	zahynout
Annin	Annin	k2eAgMnSc1d1	Annin
bratr	bratr	k1gMnSc1	bratr
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
zanechal	zanechat	k5eAaPmAgMnS	zanechat
potomka	potomek	k1gMnSc4	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
coby	coby	k?	coby
manžel	manžel	k1gMnSc1	manžel
jeho	jeho	k3xOp3gFnSc2	jeho
sestry	sestra	k1gFnSc2	sestra
začal	začít	k5eAaPmAgInS	začít
usilovat	usilovat	k5eAaImF	usilovat
o	o	k7c4	o
český	český	k2eAgInSc4d1	český
a	a	k8xC	a
uherský	uherský	k2eAgInSc4d1	uherský
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Česká	český	k2eAgFnSc1d1	Česká
koruna	koruna	k1gFnSc1	koruna
==	==	k?	==
</s>
</p>
<p>
<s>
Dalo	dát	k5eAaPmAgNnS	dát
se	se	k3xPyFc4	se
očekávat	očekávat	k5eAaImF	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
kandidáty	kandidát	k1gMnPc7	kandidát
pro	pro	k7c4	pro
českou	český	k2eAgFnSc4d1	Česká
korunu	koruna	k1gFnSc4	koruna
bude	být	k5eAaImBp3nS	být
figurovat	figurovat	k5eAaImF	figurovat
i	i	k9	i
jméno	jméno	k1gNnSc1	jméno
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Habsburského	habsburský	k2eAgMnSc2d1	habsburský
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
pokusí	pokusit	k5eAaPmIp3nS	pokusit
uplatnit	uplatnit	k5eAaPmF	uplatnit
dědičné	dědičný	k2eAgInPc4d1	dědičný
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
ovozované	ovozovaný	k2eAgInPc4d1	ovozovaný
ze	z	k7c2	z
sňatku	sňatek	k1gInSc2	sňatek
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
Jagellonskou	jagellonský	k2eAgFnSc7d1	Jagellonská
<g/>
.	.	kIx.	.
</s>
<s>
Zájemců	zájemce	k1gMnPc2	zájemce
o	o	k7c4	o
českou	český	k2eAgFnSc4d1	Česká
korunu	koruna	k1gFnSc4	koruna
bylo	být	k5eAaImAgNnS	být
ovšem	ovšem	k9	ovšem
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
polského	polský	k2eAgMnSc2d1	polský
krále	král	k1gMnSc2	král
a	a	k8xC	a
litevského	litevský	k2eAgMnSc2d1	litevský
knížete	kníže	k1gMnSc2	kníže
Zikmunda	Zikmund	k1gMnSc2	Zikmund
I.	I.	kA	I.
Starého	Starý	k1gMnSc2	Starý
<g/>
,	,	kIx,	,
nejbližšího	blízký	k2eAgMnSc2d3	nejbližší
mužského	mužský	k2eAgMnSc2d1	mužský
příbuzného	příbuzný	k1gMnSc2	příbuzný
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Jagellonského	jagellonský	k2eAgMnSc2d1	jagellonský
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
pozdějších	pozdní	k2eAgInPc6d2	pozdější
písemných	písemný	k2eAgInPc6d1	písemný
pramenech	pramen	k1gInPc6	pramen
objevují	objevovat	k5eAaImIp3nP	objevovat
dokonce	dokonce	k9	dokonce
i	i	k9	i
jména	jméno	k1gNnSc2	jméno
předních	přední	k2eAgMnPc2d1	přední
českých	český	k2eAgMnPc2d1	český
politiků	politik	k1gMnPc2	politik
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
:	:	kIx,	:
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Lva	Lev	k1gMnSc2	Lev
z	z	k7c2	z
Rožmitálu	Rožmitál	k1gInSc2	Rožmitál
a	a	k8xC	a
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
<g/>
.	.	kIx.	.
</s>
<s>
Jednoznačně	jednoznačně	k6eAd1	jednoznačně
nejsilnější	silný	k2eAgFnSc4d3	nejsilnější
pozici	pozice	k1gFnSc4	pozice
však	však	k9	však
měl	mít	k5eAaImAgMnS	mít
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
část	část	k1gFnSc1	část
šlechty	šlechta	k1gFnSc2	šlechta
odmítala	odmítat	k5eAaImAgFnS	odmítat
uznat	uznat	k5eAaPmF	uznat
dědické	dědický	k2eAgInPc4d1	dědický
nároky	nárok	k1gInPc4	nárok
Ferdinandovy	Ferdinandův	k2eAgFnSc2d1	Ferdinandova
manželky	manželka	k1gFnSc2	manželka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zemskému	zemský	k2eAgInSc3d1	zemský
sněmu	sněm	k1gInSc3	sněm
náleželo	náležet	k5eAaImAgNnS	náležet
právo	právo	k1gNnSc1	právo
volby	volba	k1gFnSc2	volba
panovníka	panovník	k1gMnSc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Zemské	zemský	k2eAgInPc1d1	zemský
stavy	stav	k1gInPc1	stav
si	se	k3xPyFc3	se
také	také	k6eAd1	také
ještě	ještě	k9	ještě
od	od	k7c2	od
Vladislava	Vladislav	k1gMnSc2	Vladislav
Jagellonského	jagellonský	k2eAgInSc2d1	jagellonský
vymínily	vymínit	k5eAaPmAgInP	vymínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sňatek	sňatek	k1gInSc1	sňatek
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
a	a	k8xC	a
Anny	Anna	k1gFnSc2	Anna
musí	muset	k5eAaImIp3nS	muset
potvrdit	potvrdit	k5eAaPmF	potvrdit
zemský	zemský	k2eAgInSc1d1	zemský
sněm	sněm	k1gInSc1	sněm
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
mít	mít	k5eAaImF	mít
poslední	poslední	k2eAgNnSc4d1	poslední
slovo	slovo	k1gNnSc4	slovo
při	při	k7c6	při
výběru	výběr	k1gInSc6	výběr
budoucího	budoucí	k2eAgMnSc2d1	budoucí
možného	možný	k2eAgMnSc2d1	možný
vládce	vládce	k1gMnSc2	vládce
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
se	se	k3xPyFc4	se
tuto	tento	k3xDgFnSc4	tento
kličku	klička	k1gFnSc4	klička
pokusil	pokusit	k5eAaPmAgMnS	pokusit
obejít	obejít	k5eAaPmF	obejít
několika	několik	k4yIc2	několik
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
žádal	žádat	k5eAaImAgMnS	žádat
Karla	Karel	k1gMnSc4	Karel
V.	V.	kA	V.
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
udělil	udělit	k5eAaPmAgMnS	udělit
české	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
v	v	k7c4	v
léno	léno	k1gNnSc4	léno
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ale	ale	k8xC	ale
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
udělat	udělat	k5eAaPmF	udělat
nemohl	moct	k5eNaImAgMnS	moct
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
ucházet	ucházet	k5eAaImF	ucházet
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dědického	dědický	k2eAgInSc2d1	dědický
principu	princip	k1gInSc2	princip
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
ale	ale	k8xC	ale
musel	muset	k5eAaImAgMnS	muset
přistoupit	přistoupit	k5eAaPmF	přistoupit
na	na	k7c4	na
svobodnou	svobodný	k2eAgFnSc4d1	svobodná
volbu	volba	k1gFnSc4	volba
nového	nový	k2eAgMnSc2d1	nový
panovníka	panovník	k1gMnSc2	panovník
na	na	k7c6	na
mimořádném	mimořádný	k2eAgInSc6d1	mimořádný
sněmu	sněm	k1gInSc6	sněm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
roku	rok	k1gInSc2	rok
1526	[number]	k4	1526
byl	být	k5eAaImAgMnS	být
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
skupiny	skupina	k1gFnSc2	skupina
šlechticů	šlechtic	k1gMnPc2	šlechtic
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
čele	čelo	k1gNnSc6	čelo
stál	stát	k5eAaImAgMnS	stát
Adam	Adam	k1gMnSc1	Adam
I.	I.	kA	I.
z	z	k7c2	z
Hradce	Hradec	k1gInSc2	Hradec
<g/>
,	,	kIx,	,
zvolen	zvolit	k5eAaPmNgMnS	zvolit
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
především	především	k9	především
kvůli	kvůli	k7c3	kvůli
zoufalému	zoufalý	k2eAgInSc3d1	zoufalý
stavu	stav	k1gInSc3	stav
státní	státní	k2eAgFnSc2d1	státní
pokladny	pokladna	k1gFnSc2	pokladna
(	(	kIx(	(
<g/>
české	český	k2eAgInPc1d1	český
stavy	stav	k1gInPc1	stav
evidentně	evidentně	k6eAd1	evidentně
nevěděly	vědět	k5eNaImAgInP	vědět
a	a	k8xC	a
asi	asi	k9	asi
ani	ani	k8xC	ani
nemohly	moct	k5eNaImAgFnP	moct
vědět	vědět	k5eAaImF	vědět
o	o	k7c6	o
špatném	špatný	k2eAgInSc6d1	špatný
stavu	stav	k1gInSc6	stav
Ferdinandovy	Ferdinandův	k2eAgFnSc2d1	Ferdinandova
pokladny	pokladna	k1gFnSc2	pokladna
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Určitý	určitý	k2eAgInSc4d1	určitý
vliv	vliv	k1gInSc4	vliv
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
prestiž	prestiž	k1gFnSc1	prestiž
habsburského	habsburský	k2eAgInSc2d1	habsburský
rodu	rod	k1gInSc2	rod
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc3d2	vyšší
šlechtě	šlechta	k1gFnSc3	šlechta
byl	být	k5eAaImAgInS	být
určitě	určitě	k6eAd1	určitě
sympatický	sympatický	k2eAgInSc4d1	sympatický
Ferdinandův	Ferdinandův	k2eAgInSc4d1	Ferdinandův
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
městskému	městský	k2eAgInSc3d1	městský
stavu	stav	k1gInSc3	stav
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
panstva	panstvo	k1gNnSc2	panstvo
byla	být	k5eAaImAgFnS	být
také	také	k9	také
nespokojena	spokojen	k2eNgFnSc1d1	nespokojena
s	s	k7c7	s
dosavadní	dosavadní	k2eAgFnSc7d1	dosavadní
slabou	slabý	k2eAgFnSc7d1	slabá
královskou	královský	k2eAgFnSc7d1	královská
mocí	moc	k1gFnSc7	moc
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
opomenout	opomenout	k5eAaPmF	opomenout
tureckou	turecký	k2eAgFnSc4d1	turecká
hrozbu	hrozba	k1gFnSc4	hrozba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
teď	teď	k6eAd1	teď
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Moháče	Moháč	k1gInSc2	Moháč
vyvstala	vyvstat	k5eAaPmAgFnS	vyvstat
jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
reálná	reálný	k2eAgFnSc1d1	reálná
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
volby	volba	k1gFnSc2	volba
byl	být	k5eAaImAgInS	být
Ferdinandův	Ferdinandův	k2eAgInSc4d1	Ferdinandův
slib	slib	k1gInSc4	slib
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
dodržovat	dodržovat	k5eAaImF	dodržovat
kompaktáta	kompaktáta	k1gNnPc4	kompaktáta
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
respektovat	respektovat	k5eAaImF	respektovat
svobodu	svoboda	k1gFnSc4	svoboda
českých	český	k2eAgInPc2d1	český
stavů	stav	k1gInPc2	stav
a	a	k8xC	a
přenese	přenést	k5eAaPmIp3nS	přenést
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
byl	být	k5eAaImAgMnS	být
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
korunován	korunován	k2eAgMnSc1d1	korunován
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
roku	rok	k1gInSc2	rok
1527	[number]	k4	1527
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
inauguračních	inaugurační	k2eAgInPc2d1	inaugurační
slibů	slib	k1gInPc2	slib
ovšem	ovšem	k9	ovšem
nakonec	nakonec	k6eAd1	nakonec
nesplnil	splnit	k5eNaPmAgMnS	splnit
téměř	téměř	k6eAd1	téměř
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Uhersku	Uhersko	k1gNnSc6	Uhersko
byl	být	k5eAaImAgInS	být
Ferdinandovým	Ferdinandův	k2eAgMnSc7d1	Ferdinandův
soupeřem	soupeř	k1gMnSc7	soupeř
sedmihradský	sedmihradský	k2eAgMnSc1d1	sedmihradský
vévoda	vévoda	k1gMnSc1	vévoda
Jan	Jan	k1gMnSc1	Jan
Zápolský	Zápolský	k2eAgMnSc1d1	Zápolský
<g/>
,	,	kIx,	,
kterého	který	k3yIgNnSc2	který
většina	většina	k1gFnSc1	většina
uherských	uherský	k2eAgInPc2d1	uherský
stavů	stav	k1gInPc2	stav
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1526	[number]	k4	1526
zvolila	zvolit	k5eAaPmAgFnS	zvolit
svým	svůj	k3xOyFgMnSc7	svůj
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Uherským	uherský	k2eAgMnSc7d1	uherský
králem	král	k1gMnSc7	král
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
stal	stát	k5eAaPmAgInS	stát
i	i	k9	i
Habsburk	Habsburk	k1gInSc1	Habsburk
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgInS	nechat
hrstkou	hrstka	k1gFnSc7	hrstka
svých	svůj	k3xOyFgMnPc2	svůj
přívrženců	přívrženec	k1gMnPc2	přívrženec
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
zvolit	zvolit	k5eAaPmF	zvolit
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
zdaleka	zdaleka	k6eAd1	zdaleka
však	však	k9	však
neměl	mít	k5eNaImAgInS	mít
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
takovou	takový	k3xDgFnSc4	takový
podporu	podpora	k1gFnSc4	podpora
jako	jako	k8xC	jako
Zápolský	Zápolský	k2eAgInSc4d1	Zápolský
<g/>
.	.	kIx.	.
<g/>
Zápolský	Zápolský	k2eAgMnSc1d1	Zápolský
byl	být	k5eAaImAgMnS	být
podporován	podporovat	k5eAaImNgMnS	podporovat
Osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
říší	říš	k1gFnSc7	říš
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pronikala	pronikat	k5eAaImAgFnS	pronikat
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Turci	Turek	k1gMnPc1	Turek
dvakrát	dvakrát	k6eAd1	dvakrát
(	(	kIx(	(
<g/>
v	v	k7c6	v
r.	r.	kA	r.
1529	[number]	k4	1529
a	a	k8xC	a
1532	[number]	k4	1532
<g/>
)	)	kIx)	)
ohrozili	ohrozit	k5eAaPmAgMnP	ohrozit
Vídeň	Vídeň	k1gFnSc4	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
si	se	k3xPyFc3	se
ovšem	ovšem	k9	ovšem
kromě	kromě	k7c2	kromě
titulu	titul	k1gInSc2	titul
uherského	uherský	k2eAgMnSc2d1	uherský
krále	král	k1gMnSc2	král
udržel	udržet	k5eAaPmAgInS	udržet
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
Uhersku	Uhersko	k1gNnSc6	Uhersko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
a	a	k8xC	a
na	na	k7c6	na
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
==	==	k?	==
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1530	[number]	k4	1530
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1531	[number]	k4	1531
byl	být	k5eAaImAgMnS	být
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
zvolen	zvolit	k5eAaPmNgMnS	zvolit
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
také	také	k9	také
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
korunovaci	korunovace	k1gFnSc3	korunovace
došlo	dojít	k5eAaPmAgNnS	dojít
jen	jen	k9	jen
o	o	k7c4	o
šest	šest	k4xCc4	šest
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
Cáchách	Cáchy	k1gFnPc6	Cáchy
<g/>
.	.	kIx.	.
</s>
<s>
Císařem	Císař	k1gMnSc7	Císař
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1556	[number]	k4	1556
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Ferdinandovu	Ferdinandův	k2eAgNnSc3d1	Ferdinandovo
povýšení	povýšení	k1gNnSc3	povýšení
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
hodnosti	hodnost	k1gFnSc2	hodnost
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
letech	léto	k1gNnPc6	léto
Karlova	Karlův	k2eAgNnSc2d1	Karlovo
váhání	váhání	k1gNnSc2	váhání
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
ponechat	ponechat	k5eAaPmF	ponechat
římský	římský	k2eAgInSc4d1	římský
a	a	k8xC	a
španělský	španělský	k2eAgInSc4d1	španělský
trůn	trůn	k1gInSc4	trůn
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
svých	svůj	k3xOyFgMnPc2	svůj
potomků	potomek	k1gMnPc2	potomek
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
rozdělit	rozdělit	k5eAaPmF	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
tak	tak	k9	tak
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
stal	stát	k5eAaPmAgMnS	stát
úplným	úplný	k2eAgMnSc7d1	úplný
nástupcem	nástupce	k1gMnSc7	nástupce
svého	svůj	k3xOyFgMnSc2	svůj
staršího	starý	k2eAgMnSc2d2	starší
bratra	bratr	k1gMnSc2	bratr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
středověku	středověk	k1gInSc2	středověk
a	a	k8xC	a
novověku	novověk	k1gInSc2	novověk
se	se	k3xPyFc4	se
z	z	k7c2	z
Habsburků	Habsburk	k1gMnPc2	Habsburk
stala	stát	k5eAaPmAgFnS	stát
především	především	k9	především
díky	díky	k7c3	díky
promyšlené	promyšlený	k2eAgFnSc3d1	promyšlená
sňatkové	sňatkový	k2eAgFnSc3d1	sňatková
politice	politika	k1gFnSc3	politika
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
také	také	k9	také
nesplněným	splněný	k2eNgInPc3d1	nesplněný
slibům	slib	k1gInPc3	slib
<g/>
)	)	kIx)	)
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejmocnějších	mocný	k2eAgFnPc2d3	nejmocnější
panovnických	panovnický	k2eAgFnPc2d1	panovnická
dynastií	dynastie	k1gFnPc2	dynastie
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
usiloval	usilovat	k5eAaImAgMnS	usilovat
za	za	k7c4	za
své	svůj	k3xOyFgFnPc4	svůj
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
vlády	vláda	k1gFnPc4	vláda
o	o	k7c4	o
pevné	pevný	k2eAgNnSc4d1	pevné
spojení	spojení	k1gNnSc4	spojení
svých	svůj	k3xOyFgFnPc2	svůj
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
centrální	centrální	k2eAgNnSc4d1	centrální
řízení	řízení	k1gNnSc4	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
protiváhu	protiváha	k1gFnSc4	protiváha
omezení	omezení	k1gNnSc3	omezení
českých	český	k2eAgInPc2d1	český
a	a	k8xC	a
uherských	uherský	k2eAgInPc2d1	uherský
zemských	zemský	k2eAgInPc2d1	zemský
úřadů	úřad	k1gInPc2	úřad
založil	založit	k5eAaPmAgMnS	založit
roku	rok	k1gInSc2	rok
1527	[number]	k4	1527
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
centrální	centrální	k2eAgInPc1d1	centrální
úřady	úřad	k1gInPc1	úřad
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Hofstaatsordnung	Hofstaatsordnung	k1gInSc1	Hofstaatsordnung
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Habsburská	habsburský	k2eAgFnSc1d1	habsburská
monarchie	monarchie	k1gFnSc1	monarchie
<g/>
,	,	kIx,	,
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
spojením	spojení	k1gNnSc7	spojení
rakouských	rakouský	k2eAgFnPc2d1	rakouská
habsburských	habsburský	k2eAgFnPc2d1	habsburská
držav	država	k1gFnPc2	država
<g/>
,	,	kIx,	,
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
Uherska	Uhersko	k1gNnSc2	Uhersko
<g/>
,	,	kIx,	,
přečkala	přečkat	k5eAaPmAgFnS	přečkat
všechny	všechen	k3xTgInPc4	všechen
otřesy	otřes	k1gInPc4	otřes
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
jako	jako	k8xC	jako
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
==	==	k?	==
</s>
</p>
<p>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
byl	být	k5eAaImAgMnS	být
zastáncem	zastánce	k1gMnSc7	zastánce
absolutismu	absolutismus	k1gInSc2	absolutismus
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgInS	být
trend	trend	k1gInSc4	trend
rozšiřující	rozšiřující	k2eAgInSc4d1	rozšiřující
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
logicky	logicky	k6eAd1	logicky
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
střetu	střet	k1gInSc2	střet
s	s	k7c7	s
českým	český	k2eAgInSc7d1	český
stavovským	stavovský	k2eAgInSc7d1	stavovský
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Habsburkové	Habsburk	k1gMnPc1	Habsburk
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
politice	politika	k1gFnSc6	politika
prosazovali	prosazovat	k5eAaImAgMnP	prosazovat
katolické	katolický	k2eAgNnSc4d1	katolické
vyznání	vyznání	k1gNnSc4	vyznání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
se	se	k3xPyFc4	se
hlásila	hlásit	k5eAaImAgFnS	hlásit
k	k	k7c3	k
utrakvismu	utrakvismus	k1gInSc2	utrakvismus
<g/>
,	,	kIx,	,
luterské	luterský	k2eAgFnSc3d1	luterská
augsburské	augsburský	k2eAgFnSc3d1	augsburská
konfesi	konfese	k1gFnSc3	konfese
a	a	k8xC	a
k	k	k7c3	k
Jednotě	jednota	k1gFnSc3	jednota
bratrské	bratrský	k2eAgFnSc3d1	bratrská
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
nakonec	nakonec	k6eAd1	nakonec
městům	město	k1gNnPc3	město
omezoval	omezovat	k5eAaImAgInS	omezovat
privilegia	privilegium	k1gNnPc4	privilegium
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
vyžadoval	vyžadovat	k5eAaImAgInS	vyžadovat
příliš	příliš	k6eAd1	příliš
vysoké	vysoký	k2eAgFnPc4d1	vysoká
daně	daň	k1gFnPc4	daň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1546	[number]	k4	1546
<g/>
–	–	k?	–
<g/>
1547	[number]	k4	1547
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
protihabsburskému	protihabsburský	k2eAgInSc3d1	protihabsburský
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Svaté	svatý	k2eAgFnSc6d1	svatá
říši	říš	k1gFnSc6	říš
římské	římský	k2eAgFnSc6d1	římská
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
probíhala	probíhat	k5eAaImAgFnS	probíhat
šmalkaldská	šmalkaldský	k2eAgFnSc1d1	šmalkaldská
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
katolíky	katolík	k1gMnPc7	katolík
a	a	k8xC	a
luterány	luterán	k1gMnPc7	luterán
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
požádal	požádat	k5eAaPmAgMnS	požádat
bratra	bratr	k1gMnSc4	bratr
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
svolal	svolat	k5eAaPmAgMnS	svolat
zemskou	zemský	k2eAgFnSc4d1	zemská
hotovost	hotovost	k1gFnSc4	hotovost
k	k	k7c3	k
tažení	tažení	k1gNnSc3	tažení
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
šlechtici	šlechtic	k1gMnPc1	šlechtic
však	však	k9	však
vypověděli	vypovědět	k5eAaPmAgMnP	vypovědět
králi	král	k1gMnSc3	král
poslušnost	poslušnost	k1gFnSc4	poslušnost
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
se	se	k3xPyFc4	se
měšťansko-šlechtická	měšťansko-šlechtický	k2eAgFnSc1d1	měšťansko-šlechtický
opozice	opozice	k1gFnSc1	opozice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ale	ale	k9	ale
byla	být	k5eAaImAgFnS	být
potlačena	potlačen	k2eAgFnSc1d1	potlačena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
odebral	odebrat	k5eAaPmAgMnS	odebrat
městům	město	k1gNnPc3	město
politická	politický	k2eAgNnPc4d1	politické
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
musela	muset	k5eAaImAgFnS	muset
odevzdat	odevzdat	k5eAaPmF	odevzdat
všechny	všechen	k3xTgFnPc4	všechen
střelné	střelný	k2eAgFnPc4d1	střelná
zbraně	zbraň	k1gFnPc4	zbraň
a	a	k8xC	a
pozemkový	pozemkový	k2eAgInSc4d1	pozemkový
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Projevem	projev	k1gInSc7	projev
vzestupu	vzestup	k1gInSc2	vzestup
panovnické	panovnický	k2eAgFnSc2d1	panovnická
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
ignorance	ignorance	k1gFnSc2	ignorance
českých	český	k2eAgInPc2d1	český
stavů	stav	k1gInPc2	stav
bylo	být	k5eAaImAgNnS	být
podřízení	podřízení	k1gNnSc1	podřízení
zemské	zemský	k2eAgFnSc2d1	zemská
správy	správa	k1gFnSc2	správa
nově	nově	k6eAd1	nově
jmenovanému	jmenovaný	k2eAgMnSc3d1	jmenovaný
místodržiteli	místodržitel	k1gMnSc3	místodržitel
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Ferdinandův	Ferdinandův	k2eAgMnSc1d1	Ferdinandův
druhorozený	druhorozený	k2eAgMnSc1d1	druhorozený
syn	syn	k1gMnSc1	syn
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Tyrolský	tyrolský	k2eAgInSc1d1	tyrolský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
Ferdinandovy	Ferdinandův	k2eAgFnSc2d1	Ferdinandova
vlády	vláda	k1gFnSc2	vláda
také	také	k9	také
přicházejí	přicházet	k5eAaImIp3nP	přicházet
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rekatolizace	rekatolizace	k1gFnSc2	rekatolizace
jezuité	jezuita	k1gMnPc1	jezuita
(	(	kIx(	(
<g/>
1556	[number]	k4	1556
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
nechal	nechat	k5eAaPmAgMnS	nechat
obsadit	obsadit	k5eAaPmF	obsadit
úřad	úřad	k1gInSc4	úřad
pražského	pražský	k2eAgMnSc2d1	pražský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
(	(	kIx(	(
<g/>
1561	[number]	k4	1561
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
si	se	k3xPyFc3	se
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
uvědomoval	uvědomovat	k5eAaImAgMnS	uvědomovat
důležitost	důležitost	k1gFnSc4	důležitost
smíru	smír	k1gInSc2	smír
mezi	mezi	k7c7	mezi
katolíky	katolík	k1gMnPc7	katolík
a	a	k8xC	a
staroutrakvisty	staroutrakvista	k1gMnPc7	staroutrakvista
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1564	[number]	k4	1564
požádal	požádat	k5eAaPmAgInS	požádat
Tridentský	tridentský	k2eAgInSc1d1	tridentský
koncil	koncil	k1gInSc1	koncil
o	o	k7c4	o
udělení	udělení	k1gNnSc4	udělení
výjimky	výjimka	k1gFnSc2	výjimka
pro	pro	k7c4	pro
České	český	k2eAgFnPc4d1	Česká
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
povolení	povolení	k1gNnSc4	povolení
přijímání	přijímání	k1gNnSc2	přijímání
podobojí	podobojí	k6eAd1	podobojí
nekatolíkům	nekatolík	k1gMnPc3	nekatolík
<g/>
.	.	kIx.	.
</s>
<s>
Koncil	koncil	k1gInSc1	koncil
přenechal	přenechat	k5eAaPmAgInS	přenechat
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
papeží	papeží	k2eAgFnSc2d1	papeží
Piu	Pius	k1gMnSc3	Pius
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
výjimku	výjimka	k1gFnSc4	výjimka
roku	rok	k1gInSc2	rok
1564	[number]	k4	1564
povolil	povolit	k5eAaPmAgMnS	povolit
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
problematickou	problematický	k2eAgFnSc4d1	problematická
cenu	cena	k1gFnSc4	cena
mělo	mít	k5eAaImAgNnS	mít
toto	tento	k3xDgNnSc4	tento
žalostně	žalostně	k6eAd1	žalostně
opožděné	opožděný	k2eAgNnSc4d1	opožděné
stvrzení	stvrzení	k1gNnSc4	stvrzení
jedné	jeden	k4xCgFnSc2	jeden
části	část	k1gFnSc2	část
kompaktát	kompaktáta	k1gNnPc2	kompaktáta
<g/>
,	,	kIx,	,
dosvědčuje	dosvědčovat	k5eAaImIp3nS	dosvědčovat
nejlépe	dobře	k6eAd3	dobře
žádost	žádost	k1gFnSc4	žádost
stavů	stav	k1gInPc2	stav
na	na	k7c6	na
sněmu	sněm	k1gInSc6	sněm
roku	rok	k1gInSc2	rok
1567	[number]	k4	1567
o	o	k7c4	o
vypuštění	vypuštění	k1gNnSc4	vypuštění
kompaktát	kompaktáta	k1gNnPc2	kompaktáta
z	z	k7c2	z
privilegií	privilegium	k1gNnPc2	privilegium
zemských	zemský	k2eAgNnPc2d1	zemské
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
také	také	k6eAd1	také
bylo	být	k5eAaImAgNnS	být
vyhověno	vyhověn	k2eAgNnSc1d1	vyhověno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rakousko-česko-uherské	Rakousko-českoherský	k2eAgNnSc1d1	Rakousko-česko-uherský
soustátí	soustátí	k1gNnSc1	soustátí
původně	původně	k6eAd1	původně
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
jen	jen	k9	jen
jako	jako	k9	jako
personální	personální	k2eAgFnSc2d1	personální
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Postupný	postupný	k2eAgInSc1d1	postupný
přechod	přechod	k1gInSc1	přechod
v	v	k7c4	v
centralizovaný	centralizovaný	k2eAgInSc4d1	centralizovaný
absolutistický	absolutistický	k2eAgInSc4d1	absolutistický
stát	stát	k1gInSc4	stát
i	i	k9	i
přes	přes	k7c4	přes
velké	velký	k2eAgNnSc4d1	velké
úsilí	úsilí	k1gNnSc4	úsilí
některých	některý	k3yIgMnPc2	některý
Habsburků	Habsburk	k1gMnPc2	Habsburk
umožnila	umožnit	k5eAaPmAgFnS	umožnit
až	až	k9	až
porážka	porážka	k1gFnSc1	porážka
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1620	[number]	k4	1620
a	a	k8xC	a
přijetí	přijetí	k1gNnSc4	přijetí
tzv.	tzv.	kA	tzv.
Obnoveného	obnovený	k2eAgNnSc2d1	obnovené
zřízení	zřízení	k1gNnSc2	zřízení
zemského	zemský	k2eAgInSc2d1	zemský
(	(	kIx(	(
<g/>
1627	[number]	k4	1627
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
zemřel	zemřít	k5eAaPmAgMnS	zemřít
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
roku	rok	k1gInSc2	rok
1564	[number]	k4	1564
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
vedle	vedle	k7c2	vedle
své	svůj	k3xOyFgFnSc2	svůj
manželky	manželka	k1gFnSc2	manželka
Anny	Anna	k1gFnSc2	Anna
Jagellonské	jagellonský	k2eAgFnSc2d1	Jagellonská
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
země	zem	k1gFnPc4	zem
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
mezi	mezi	k7c7	mezi
syny	syn	k1gMnPc7	syn
Maxmiliána	Maxmilián	k1gMnSc2	Maxmilián
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Země	zem	k1gFnPc1	zem
koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnPc1d1	Česká
<g/>
,	,	kIx,	,
Uhersko	Uhersko	k1gNnSc1	Uhersko
<g/>
,	,	kIx,	,
Rakousy	Rakousy	k1gInPc1	Rakousy
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Tyrolského	tyrolský	k2eAgMnSc2d1	tyrolský
(	(	kIx(	(
<g/>
Tyrolsko	Tyrolsko	k1gNnSc1	Tyrolsko
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
přední	přední	k2eAgFnPc4d1	přední
země	zem	k1gFnPc4	zem
<g/>
)	)	kIx)	)
a	a	k8xC	a
Karla	Karel	k1gMnSc2	Karel
Štýrského	štýrský	k2eAgMnSc2d1	štýrský
(	(	kIx(	(
<g/>
Štýrsko	Štýrsko	k1gNnSc1	Štýrsko
<g/>
,	,	kIx,	,
Korutany	Korutany	k1gInPc1	Korutany
<g/>
,	,	kIx,	,
Kraňsko	Kraňsko	k1gNnSc1	Kraňsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potomci	potomek	k1gMnPc5	potomek
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Annou	Anna	k1gFnSc7	Anna
Jagellonskou	jagellonský	k2eAgFnSc7d1	Jagellonská
(	(	kIx(	(
<g/>
1503	[number]	k4	1503
<g/>
–	–	k?	–
<g/>
1547	[number]	k4	1547
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zemřela	zemřít	k5eAaPmAgFnS	zemřít
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
nejmladší	mladý	k2eAgFnSc2d3	nejmladší
dcery	dcera	k1gFnSc2	dcera
Johany	Johana	k1gFnSc2	Johana
na	na	k7c4	na
horečku	horečka	k1gFnSc4	horečka
omladnic	omladnice	k1gFnPc2	omladnice
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
15	[number]	k4	15
dětí	dítě	k1gFnPc2	dítě
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
(	(	kIx(	(
<g/>
1526	[number]	k4	1526
<g/>
–	–	k?	–
<g/>
1545	[number]	k4	1545
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polská	polský	k2eAgFnSc1d1	polská
královna	královna	k1gFnSc1	královna
∞	∞	k?	∞
1543	[number]	k4	1543
král	král	k1gMnSc1	král
Zikmund	Zikmund	k1gMnSc1	Zikmund
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
August	August	k1gMnSc1	August
</s>
</p>
<p>
<s>
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1527	[number]	k4	1527
<g/>
–	–	k?	–
<g/>
1576	[number]	k4	1576
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
císař	císař	k1gMnSc1	císař
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
a	a	k8xC	a
král	král	k1gMnSc1	král
uherský	uherský	k2eAgMnSc1d1	uherský
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
<g/>
,	,	kIx,	,
...	...	k?	...
∞	∞	k?	∞
1548	[number]	k4	1548
infantka	infantka	k1gFnSc1	infantka
Marie	Maria	k1gFnSc2	Maria
Španělská	španělský	k2eAgFnSc1d1	španělská
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
V.	V.	kA	V.
</s>
</p>
<p>
<s>
Anna	Anna	k1gFnSc1	Anna
(	(	kIx(	(
<g/>
1528	[number]	k4	1528
<g/>
–	–	k?	–
<g/>
1590	[number]	k4	1590
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bavorská	bavorský	k2eAgFnSc1d1	bavorská
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
∞	∞	k?	∞
1546	[number]	k4	1546
vévoda	vévoda	k1gMnSc1	vévoda
Albrecht	Albrecht	k1gMnSc1	Albrecht
V.	V.	kA	V.
</s>
</p>
<p>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1529	[number]	k4	1529
<g/>
–	–	k?	–
<g/>
1595	[number]	k4	1595
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakousko-tyrolský	rakouskoyrolský	k2eAgMnSc1d1	rakousko-tyrolský
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
<g/>
∞	∞	k?	∞
1557	[number]	k4	1557
Filipína	Filipína	k1gFnSc1	Filipína
Welserová	Welserová	k1gFnSc1	Welserová
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
kupce	kupec	k1gMnSc4	kupec
Františka	František	k1gMnSc4	František
Antona	Anton	k1gMnSc4	Anton
Welsera	Welser	k1gMnSc4	Welser
</s>
</p>
<p>
<s>
∞	∞	k?	∞
1582	[number]	k4	1582
princezna	princezna	k1gFnSc1	princezna
Anna	Anna	k1gFnSc1	Anna
Kateřina	Kateřina	k1gFnSc1	Kateřina
Gonzagová	Gonzagový	k2eAgFnSc1d1	Gonzagový
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
vévody	vévoda	k1gMnSc2	vévoda
Viléma	Vilém	k1gMnSc2	Vilém
I.	I.	kA	I.
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Ferdinandova	Ferdinandův	k2eAgFnSc1d1	Ferdinandova
neteř	neteř	k1gFnSc1	neteř
<g/>
)	)	kIx)	)
<g/>
Marie	Marie	k1gFnSc1	Marie
(	(	kIx(	(
<g/>
1531	[number]	k4	1531
<g/>
–	–	k?	–
<g/>
1581	[number]	k4	1581
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
z	z	k7c2	z
Jülichu-Kleve-Berg	Jülichu-Kleve-Berg	k1gMnSc1	Jülichu-Kleve-Berg
∞	∞	k?	∞
1546	[number]	k4	1546
vévoda	vévoda	k1gMnSc1	vévoda
Vilém	Vilém	k1gMnSc1	Vilém
Bohatý	bohatý	k2eAgMnSc1d1	bohatý
</s>
</p>
<p>
<s>
Magdalena	Magdalena	k1gFnSc1	Magdalena
(	(	kIx(	(
<g/>
1532	[number]	k4	1532
<g/>
–	–	k?	–
<g/>
1590	[number]	k4	1590
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zakladatelka	zakladatelka	k1gFnSc1	zakladatelka
(	(	kIx(	(
<g/>
1563	[number]	k4	1563
<g/>
)	)	kIx)	)
a	a	k8xC	a
představená	představená	k1gFnSc1	představená
ústavu	ústav	k1gInSc2	ústav
šlechtičen	šlechtična	k1gFnPc2	šlechtična
v	v	k7c6	v
tyrolském	tyrolský	k2eAgInSc6d1	tyrolský
Hallu	Hall	k1gInSc6	Hall
</s>
</p>
<p>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
(	(	kIx(	(
<g/>
1533	[number]	k4	1533
<g/>
–	–	k?	–
<g/>
1572	[number]	k4	1572
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
z	z	k7c2	z
Mantovi-Montferratu	Mantovi-Montferrat	k1gInSc2	Mantovi-Montferrat
a	a	k8xC	a
pozdější	pozdní	k2eAgFnSc1d2	pozdější
polská	polský	k2eAgFnSc1d1	polská
královna	královna	k1gFnSc1	královna
<g/>
∞	∞	k?	∞
1549	[number]	k4	1549
vévoda	vévoda	k1gMnSc1	vévoda
František	František	k1gMnSc1	František
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Gonzaga	Gonzaga	k1gFnSc1	Gonzaga
</s>
</p>
<p>
<s>
∞	∞	k?	∞
1553	[number]	k4	1553
polský	polský	k2eAgMnSc1d1	polský
král	král	k1gMnSc1	král
Zikmund	Zikmund	k1gMnSc1	Zikmund
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
AugustEleonora	AugustEleonora	k1gFnSc1	AugustEleonora
(	(	kIx(	(
<g/>
1534	[number]	k4	1534
<g/>
–	–	k?	–
<g/>
1594	[number]	k4	1594
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
z	z	k7c2	z
Mantovy-Montferratu	Mantovy-Montferrat	k1gInSc2	Mantovy-Montferrat
∞	∞	k?	∞
1549	[number]	k4	1549
vévoda	vévoda	k1gMnSc1	vévoda
Vilém	Vilém	k1gMnSc1	Vilém
I.	I.	kA	I.
Gonzaga	Gonzaga	k1gFnSc1	Gonzaga
</s>
</p>
<p>
<s>
Markéta	Markéta	k1gFnSc1	Markéta
(	(	kIx(	(
<g/>
1536	[number]	k4	1536
<g/>
–	–	k?	–
<g/>
1566	[number]	k4	1566
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1563	[number]	k4	1563
jeptiška	jeptiška	k1gFnSc1	jeptiška
v	v	k7c6	v
Hallu	Hall	k1gInSc6	Hall
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
(	(	kIx(	(
<g/>
1538	[number]	k4	1538
<g/>
–	–	k?	–
<g/>
1539	[number]	k4	1539
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
</s>
</p>
<p>
<s>
Barbora	Barbora	k1gFnSc1	Barbora
(	(	kIx(	(
<g/>
1539	[number]	k4	1539
<g/>
–	–	k?	–
<g/>
1572	[number]	k4	1572
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
z	z	k7c2	z
Ferrary	Ferrara	k1gFnSc2	Ferrara
–	–	k?	–
1565	[number]	k4	1565
vévoda	vévoda	k1gMnSc1	vévoda
Alfons	Alfons	k1gMnSc1	Alfons
II	II	kA	II
<g/>
.	.	kIx.	.
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Este	Est	k1gMnSc5	Est
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1540	[number]	k4	1540
<g/>
–	–	k?	–
<g/>
1590	[number]	k4	1590
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
štýrský	štýrský	k2eAgMnSc1d1	štýrský
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
∞	∞	k?	∞
1571	[number]	k4	1571
princezna	princezna	k1gFnSc1	princezna
Marie	Marie	k1gFnSc1	Marie
Anna	Anna	k1gFnSc1	Anna
Bavorská	bavorský	k2eAgFnSc1d1	bavorská
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
vévody	vévoda	k1gMnSc2	vévoda
Albrechta	Albrecht	k1gMnSc2	Albrecht
V.	V.	kA	V.
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Karlova	Karlův	k2eAgFnSc1d1	Karlova
neteř	neteř	k1gFnSc1	neteř
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Uršula	Uršula	k1gFnSc1	Uršula
(	(	kIx(	(
<g/>
1541	[number]	k4	1541
<g/>
–	–	k?	–
<g/>
1543	[number]	k4	1543
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakouská	rakouský	k2eAgFnSc1d1	rakouská
arcivévodkyně	arcivévodkyně	k1gFnSc1	arcivévodkyně
</s>
</p>
<p>
<s>
Helena	Helena	k1gFnSc1	Helena
(	(	kIx(	(
<g/>
1543	[number]	k4	1543
<g/>
–	–	k?	–
<g/>
1574	[number]	k4	1574
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeptiška	jeptiška	k1gFnSc1	jeptiška
v	v	k7c6	v
Hallu	Hall	k1gInSc6	Hall
</s>
</p>
<p>
<s>
Johana	Johana	k1gFnSc1	Johana
(	(	kIx(	(
<g/>
1547	[number]	k4	1547
<g/>
–	–	k?	–
<g/>
1578	[number]	k4	1578
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
toskánská	toskánský	k2eAgFnSc1d1	Toskánská
velkovévodkyně	velkovévodkyně	k1gFnSc1	velkovévodkyně
∞	∞	k?	∞
1565	[number]	k4	1565
velkovévoda	velkovévoda	k1gMnSc1	velkovévoda
František	František	k1gMnSc1	František
I.	I.	kA	I.
Medicejský	Medicejský	k2eAgInSc5d1	Medicejský
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc1	vývod
z	z	k7c2	z
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
:	:	kIx,	:
16	[number]	k4	16
<g/>
.	.	kIx.	.
sešit	sešit	k1gInSc1	sešit
:	:	kIx,	:
Ep	Ep	k1gMnSc1	Ep
<g/>
–	–	k?	–
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
;	;	kIx,	;
Historický	historický	k2eAgInSc1d1	historický
ústav	ústav	k1gInSc1	ústav
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
136	[number]	k4	136
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978-80-200-2292-9	[number]	k4	978-80-200-2292-9
(	(	kIx(	(
<g/>
Academia	academia	k1gFnSc1	academia
<g/>
)	)	kIx)	)
;	;	kIx,	;
ISBN	ISBN	kA	ISBN
978-80-7286-215-3	[number]	k4	978-80-7286-215-3
(	(	kIx(	(
<g/>
Historický	historický	k2eAgInSc1d1	historický
ústav	ústav	k1gInSc1	ústav
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
119	[number]	k4	119
<g/>
–	–	k?	–
<g/>
122	[number]	k4	122
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČECHURA	Čechura	k1gMnSc1	Čechura
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
v	v	k7c6	v
letech	let	k1gInPc6	let
1526	[number]	k4	1526
<g/>
–	–	k?	–
<g/>
1583	[number]	k4	1583
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
Habsburkové	Habsburk	k1gMnPc1	Habsburk
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trůně	trůn	k1gInSc6	trůn
I.	I.	kA	I.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
324	[number]	k4	324
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
385	[number]	k4	385
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČORNEJOVÁ	ČORNEJOVÁ	kA	ČORNEJOVÁ
<g/>
,	,	kIx,	,
Ivana	Ivana	k1gFnSc1	Ivana
<g/>
;	;	kIx,	;
RAK	rak	k1gMnSc1	rak
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
VLNAS	VLNAS	kA	VLNAS
<g/>
,	,	kIx,	,
Vít	Vít	k1gMnSc1	Vít
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
tvých	tvůj	k3xOp2gNnPc2	tvůj
křídel	křídlo	k1gNnPc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Habsburkové	Habsburk	k1gMnPc1	Habsburk
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grafoprint-Neubert	Grafoprint-Neubert	k1gMnSc1	Grafoprint-Neubert
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
289	[number]	k4	289
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85785	[number]	k4	85785
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
EVANS	EVANS	kA	EVANS
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
J.	J.	kA	J.
W.	W.	kA	W.
Vznik	vznik	k1gInSc1	vznik
habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
1550	[number]	k4	1550
<g/>
–	–	k?	–
<g/>
1700	[number]	k4	1700
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Lenka	Lenka	k1gFnSc1	Lenka
Kolářová	Kolářová	k1gFnSc1	Kolářová
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
593	[number]	k4	593
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
463	[number]	k4	463
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FICHTNER	FICHTNER	kA	FICHTNER
<g/>
,	,	kIx,	,
Paula	Paula	k1gFnSc1	Paula
Sutter	Sutter	k1gInSc1	Sutter
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
Wider	Wider	k1gMnSc1	Wider
Türken	Türken	k2eAgMnSc1d1	Türken
und	und	k?	und
Glaubensspaltung	Glaubensspaltung	k1gMnSc1	Glaubensspaltung
<g/>
.	.	kIx.	.
</s>
<s>
Graz	Graz	k1gMnSc1	Graz
;	;	kIx,	;
Wien	Wien	k1gMnSc1	Wien
;	;	kIx,	;
Köln	Köln	k1gMnSc1	Köln
<g/>
:	:	kIx,	:
Verlag	Verlag	k1gInSc1	Verlag
Styria	Styrium	k1gNnSc2	Styrium
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
303	[number]	k4	303
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
222	[number]	k4	222
<g/>
-	-	kIx~	-
<g/>
11670	[number]	k4	11670
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HAMANNOVÁ	HAMANNOVÁ	kA	HAMANNOVÁ
<g/>
,	,	kIx,	,
Brigitte	Brigitte	k1gFnSc1	Brigitte
<g/>
.	.	kIx.	.
</s>
<s>
Habsburkové	Habsburk	k1gMnPc1	Habsburk
:	:	kIx,	:
životopisná	životopisný	k2eAgFnSc1d1	životopisná
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Milada	Milada	k1gFnSc1	Milada
a	a	k8xC	a
Milan	Milan	k1gMnSc1	Milan
Kouřimských	kouřimský	k2eAgMnPc2d1	kouřimský
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
403	[number]	k4	403
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7243	[number]	k4	7243
<g/>
-	-	kIx~	-
<g/>
455	[number]	k4	455
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
91	[number]	k4	91
<g/>
–	–	k?	–
<g/>
94	[number]	k4	94
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JANÁČEK	Janáček	k1gMnSc1	Janáček
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dějiny	dějiny	k1gFnPc1	dějiny
I.	I.	kA	I.
Doba	doba	k1gFnSc1	doba
předbělohorská	předbělohorský	k2eAgFnSc1d1	předbělohorská
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
I	i	k9	i
<g/>
,	,	kIx,	,
díl	díl	k1gInSc4	díl
I.	I.	kA	I.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
281	[number]	k4	281
s.	s.	k?	s.
</s>
</p>
<p>
<s>
JANÁČEK	Janáček	k1gMnSc1	Janáček
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dějiny	dějiny	k1gFnPc1	dějiny
I.	I.	kA	I.
Doba	doba	k1gFnSc1	doba
předbělohorská	předbělohorský	k2eAgFnSc1d1	předbělohorská
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
I	i	k9	i
<g/>
,	,	kIx,	,
díl	díl	k1gInSc1	díl
II	II	kA	II
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
359	[number]	k4	359
s.	s.	k?	s.
</s>
</p>
<p>
<s>
KOHLER	KOHLER	kA	KOHLER
<g/>
,	,	kIx,	,
Alfred	Alfred	k1gMnSc1	Alfred
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1503	[number]	k4	1503
<g/>
–	–	k?	–
<g/>
1564	[number]	k4	1564
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kníže	kníže	k1gMnSc1	kníže
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Karel	Karel	k1gMnSc1	Karel
Pacovský	Pacovský	k1gMnSc1	Pacovský
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
:	:	kIx,	:
Veduta	veduta	k1gFnSc1	veduta
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
332	[number]	k4	332
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
88030	[number]	k4	88030
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
LAUBACH	LAUBACH	kA	LAUBACH
<g/>
,	,	kIx,	,
Ernst	Ernst	k1gMnSc1	Ernst
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
als	als	k?	als
Kaiser	Kaiser	k1gMnSc1	Kaiser
<g/>
.	.	kIx.	.
</s>
<s>
Politik	politik	k1gMnSc1	politik
und	und	k?	und
Herrscherauffassung	Herrscherauffassung	k1gInSc1	Herrscherauffassung
des	des	k1gNnSc1	des
Nachfolgers	Nachfolgersa	k1gFnPc2	Nachfolgersa
Karls	Karlsa	k1gFnPc2	Karlsa
V.	V.	kA	V.
Münster	Münster	k1gMnSc1	Münster
<g/>
:	:	kIx,	:
Aschendorff	Aschendorff	k1gMnSc1	Aschendorff
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
783	[number]	k4	783
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
402	[number]	k4	402
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5165	[number]	k4	5165
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
PEŠEK	Pešek	k1gMnSc1	Pešek
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
In	In	k1gMnSc1	In
<g/>
:	:	kIx,	:
RYANTOVÁ	RYANTOVÁ	kA	RYANTOVÁ
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
;	;	kIx,	;
VOREL	Vorel	k1gMnSc1	Vorel
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
králové	král	k1gMnPc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
940	[number]	k4	940
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
303	[number]	k4	303
<g/>
–	–	k?	–
<g/>
319	[number]	k4	319
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SÍMÁNYI	SÍMÁNYI	kA	SÍMÁNYI
<g/>
,	,	kIx,	,
Tibor	Tibor	k1gMnSc1	Tibor
<g/>
.	.	kIx.	.
</s>
<s>
Er	Er	k?	Er
schuf	schuf	k1gInSc1	schuf
das	das	k?	das
Reich	Reich	k?	Reich
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
von	von	k1gInSc4	von
Habsburg	Habsburg	k1gInSc1	Habsburg
<g/>
.	.	kIx.	.
</s>
<s>
Wien	Wien	k1gNnSc1	Wien
;	;	kIx,	;
München	München	k2eAgMnSc1d1	München
<g/>
:	:	kIx,	:
Amalthea	Amalthea	k1gMnSc1	Amalthea
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
316	[number]	k4	316
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
85002	[number]	k4	85002
<g/>
-	-	kIx~	-
<g/>
224	[number]	k4	224
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
VEBER	VEBER	kA	VEBER
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
doplněné	doplněný	k2eAgInPc4d1	doplněný
a	a	k8xC	a
aktualizované	aktualizovaný	k2eAgInPc4d1	aktualizovaný
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
239	[number]	k4	239
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
212	[number]	k4	212
<g/>
–	–	k?	–
<g/>
237	[number]	k4	237
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VOREL	Vorel	k1gMnSc1	Vorel
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
VII	VII	kA	VII
<g/>
.	.	kIx.	.
1526	[number]	k4	1526
<g/>
–	–	k?	–
<g/>
1618	[number]	k4	1618
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
672	[number]	k4	672
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
648	[number]	k4	648
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WINKELBAUER	WINKELBAUER	kA	WINKELBAUER
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
<g/>
.	.	kIx.	.
</s>
<s>
Österreichische	Österreichischus	k1gMnSc5	Österreichischus
Geschichte	Geschicht	k1gMnSc5	Geschicht
1522	[number]	k4	1522
<g/>
–	–	k?	–
<g/>
1699	[number]	k4	1699
:	:	kIx,	:
Ständefreiheit	Ständefreiheit	k1gInSc1	Ständefreiheit
und	und	k?	und
Fürstenmacht	Fürstenmacht	k1gInSc1	Fürstenmacht
;	;	kIx,	;
Länder	Länder	k1gInSc1	Länder
und	und	k?	und
Untertanen	Untertanen	k1gInSc1	Untertanen
des	des	k1gNnSc1	des
Hauses	Hauses	k1gInSc1	Hauses
Habsburg	Habsburg	k1gMnSc1	Habsburg
im	im	k?	im
konfessionellen	konfessionellen	k2eAgMnSc1d1	konfessionellen
Zeitalter	Zeitalter	k1gMnSc1	Zeitalter
<g/>
.	.	kIx.	.
</s>
<s>
Teil	Teil	k1gInSc1	Teil
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Wien	Wien	k1gMnSc1	Wien
<g/>
:	:	kIx,	:
Ueberreuter	Ueberreuter	k1gMnSc1	Ueberreuter
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
621	[number]	k4	621
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
8000	[number]	k4	8000
<g/>
-	-	kIx~	-
<g/>
3528	[number]	k4	3528
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
WINKELBAUER	WINKELBAUER	kA	WINKELBAUER
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
<g/>
.	.	kIx.	.
</s>
<s>
Österreichische	Österreichischus	k1gMnSc5	Österreichischus
Geschichte	Geschicht	k1gMnSc5	Geschicht
1522	[number]	k4	1522
<g/>
–	–	k?	–
<g/>
1699	[number]	k4	1699
:	:	kIx,	:
Ständefreiheit	Ständefreiheit	k1gInSc1	Ständefreiheit
und	und	k?	und
Fürstenmacht	Fürstenmacht	k1gInSc1	Fürstenmacht
;	;	kIx,	;
Länder	Länder	k1gInSc1	Länder
und	und	k?	und
Untertanen	Untertanen	k1gInSc1	Untertanen
des	des	k1gNnSc1	des
Hauses	Hauses	k1gInSc1	Hauses
Habsburg	Habsburg	k1gMnSc1	Habsburg
im	im	k?	im
konfessionellen	konfessionellen	k2eAgMnSc1d1	konfessionellen
Zeitalter	Zeitalter	k1gMnSc1	Zeitalter
<g/>
.	.	kIx.	.
</s>
<s>
Teil	Teil	k1gInSc1	Teil
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Wien	Wien	k1gMnSc1	Wien
<g/>
:	:	kIx,	:
Ueberreuter	Ueberreuter	k1gMnSc1	Ueberreuter
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
567	[number]	k4	567
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
8000	[number]	k4	8000
<g/>
-	-	kIx~	-
<g/>
3987	[number]	k4	3987
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ZIEGERHOFER	ZIEGERHOFER	kA	ZIEGERHOFER
<g/>
,	,	kIx,	,
Anita	Anito	k1gNnSc2	Anito
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
und	und	k?	und
die	die	k?	die
steirischen	steirischen	k1gInSc1	steirischen
Stände	Ständ	k1gInSc5	Ständ
<g/>
.	.	kIx.	.
</s>
<s>
Dargestellt	Dargestellt	k2eAgInSc1d1	Dargestellt
anhand	anhand	k1gInSc1	anhand
der	drát	k5eAaImRp2nS	drát
Landtage	Landtage	k1gNnSc4	Landtage
von	von	k1gInSc1	von
1542	[number]	k4	1542
bis	bis	k?	bis
1556	[number]	k4	1556
<g/>
.	.	kIx.	.
</s>
<s>
Graz	Graz	k1gInSc1	Graz
<g/>
:	:	kIx,	:
dbv	dbv	k?	dbv
Verlag	Verlag	k1gInSc1	Verlag
für	für	k?	für
die	die	k?	die
technische	technischat	k5eAaPmIp3nS	technischat
Universität	Universität	k1gInSc1	Universität
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
207	[number]	k4	207
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
7041	[number]	k4	7041
<g/>
-	-	kIx~	-
<g/>
9062	[number]	k4	9062
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Země	země	k1gFnSc1	země
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
</s>
</p>
<p>
<s>
Rada	rada	k1gFnSc1	rada
nad	nad	k7c7	nad
apelacemi	apelace	k1gFnPc7	apelace
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
Habsburský	habsburský	k2eAgInSc4d1	habsburský
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc4	dílo
Odpor	odpor	k1gInSc4	odpor
stavův	stavův	k2eAgInSc4d1	stavův
českých	český	k2eAgFnPc2d1	Česká
proti	proti	k7c3	proti
Ferdinandovi	Ferdinand	k1gMnSc3	Ferdinand
I.	I.	kA	I.
l.	l.	k?	l.
1547	[number]	k4	1547
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
Habsburský	habsburský	k2eAgInSc5d1	habsburský
</s>
</p>
<p>
<s>
Nástup	nástup	k1gInSc1	nástup
Habsburků	Habsburk	k1gMnPc2	Habsburk
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
</s>
</p>
<p>
<s>
Osudová	osudový	k2eAgFnSc1d1	osudová
volba	volba	k1gFnSc1	volba
(	(	kIx(	(
<g/>
Bílá	bílý	k2eAgNnPc1d1	bílé
místa	místo	k1gNnPc1	místo
–	–	k?	–
dokument	dokument	k1gInSc1	dokument
TV	TV	kA	TV
<g/>
)	)	kIx)	)
</s>
</p>
