<p>
<s>
16	[number]	k4	16
<g/>
.	.	kIx.	.
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
volejbale	volejbal	k1gInSc6	volejbal
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Účastníci	účastník	k1gMnPc1	účastník
==	==	k?	==
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
se	se	k3xPyFc4	se
závěrečného	závěrečný	k2eAgInSc2d1	závěrečný
turnaje	turnaj	k1gInSc2	turnaj
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
24	[number]	k4	24
mužstev	mužstvo	k1gNnPc2	mužstvo
<g/>
,	,	kIx,	,
rozdělených	rozdělený	k2eAgInPc2d1	rozdělený
do	do	k7c2	do
4	[number]	k4	4
skupin	skupina	k1gFnPc2	skupina
po	po	k7c6	po
šesti	šest	k4xCc6	šest
týmech	tým	k1gInPc6	tým
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc1	první
čtyři	čtyři	k4xCgNnPc1	čtyři
mužstva	mužstvo	k1gNnPc1	mužstvo
z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
kvalifikovalo	kvalifikovat	k5eAaBmAgNnS	kvalifikovat
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
turnaje	turnaj	k1gInSc2	turnaj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
jen	jen	k9	jen
dvě	dva	k4xCgFnPc1	dva
skupiny	skupina	k1gFnPc1	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Japonsko	Japonsko	k1gNnSc1	Japonsko
se	se	k3xPyFc4	se
do	do	k7c2	do
kvalifikovalo	kvalifikovat	k5eAaBmAgNnS	kvalifikovat
jako	jako	k8xS	jako
hostitel	hostitel	k1gMnSc1	hostitel
a	a	k8xC	a
Brazílie	Brazílie	k1gFnSc1	Brazílie
jako	jako	k8xS	jako
úřadující	úřadující	k2eAgMnSc1d1	úřadující
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k1gNnSc1	ostatní
mužstva	mužstvo	k1gNnSc2	mužstvo
se	se	k3xPyFc4	se
kvalifikovala	kvalifikovat	k5eAaBmAgFnS	kvalifikovat
z	z	k7c2	z
kontinentálních	kontinentální	k2eAgInPc2d1	kontinentální
turnajů	turnaj	k1gInPc2	turnaj
(	(	kIx(	(
<g/>
9	[number]	k4	9
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
4	[number]	k4	4
ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
2	[number]	k4	2
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
2	[number]	k4	2
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
5	[number]	k4	5
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Oceánie	Oceánie	k1gFnSc2	Oceánie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Místa	místo	k1gNnSc2	místo
zápasů	zápas	k1gInPc2	zápas
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Výsledky	výsledek	k1gInPc1	výsledek
a	a	k8xC	a
tabulky	tabulka	k1gFnPc1	tabulka
==	==	k?	==
</s>
</p>
<p>
<s>
Časové	časový	k2eAgFnPc1d1	časová
udaje	udaj	k1gFnPc1	udaj
podle	podle	k7c2	podle
místního	místní	k2eAgInSc2d1	místní
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
UTC	UTC	kA	UTC
+	+	kIx~	+
9	[number]	k4	9
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Základní	základní	k2eAgFnPc1d1	základní
skupiny	skupina	k1gFnPc1	skupina
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Skupina	skupina	k1gFnSc1	skupina
A	a	k9	a
(	(	kIx(	(
<g/>
Saitama	Saitama	k1gFnSc1	Saitama
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Skupina	skupina	k1gFnSc1	skupina
B	B	kA	B
(	(	kIx(	(
<g/>
Fukuoka	Fukuoka	k1gMnSc1	Fukuoka
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Skupina	skupina	k1gFnSc1	skupina
C	C	kA	C
(	(	kIx(	(
<g/>
Nagano	Nagano	k1gNnSc1	Nagano
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Skupina	skupina	k1gFnSc1	skupina
D	D	kA	D
(	(	kIx(	(
<g/>
Sendai	Sendae	k1gFnSc4	Sendae
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Osmifinále	osmifinále	k1gNnPc3	osmifinále
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Skupina	skupina	k1gFnSc1	skupina
E	E	kA	E
(	(	kIx(	(
<g/>
Sendai	Sendae	k1gFnSc4	Sendae
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Skupina	skupina	k1gFnSc1	skupina
F	F	kA	F
(	(	kIx(	(
<g/>
Hirošima	Hirošima	k1gFnSc1	Hirošima
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Play	play	k0	play
off	off	k?	off
(	(	kIx(	(
<g/>
Tokio	Tokio	k1gNnSc1	Tokio
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Semifinále	semifinále	k1gNnPc3	semifinále
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Finále	finále	k1gNnSc1	finále
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
O	o	k7c4	o
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
O	o	k7c4	o
5	[number]	k4	5
až	až	k9	až
8	[number]	k4	8
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
O	o	k7c4	o
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
O	o	k7c4	o
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
O	o	k7c4	o
9	[number]	k4	9
až	až	k9	až
12	[number]	k4	12
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
O	o	k7c4	o
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
O	o	k7c4	o
11	[number]	k4	11
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc1	přehled
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
hráčů	hráč	k1gMnPc2	hráč
==	==	k?	==
</s>
</p>
<p>
<s>
Nejužitečnější	užitečný	k2eAgMnSc1d3	nejužitečnější
hráč	hráč	k1gMnSc1	hráč
<g/>
:	:	kIx,	:
Gilberto	Gilberta	k1gFnSc5	Gilberta
Godoy	Godo	k2eAgInPc1d1	Godo
Filho	Fil	k1gMnSc4	Fil
</s>
</p>
<p>
<s>
Nejvíce	hodně	k6eAd3	hodně
bodující	bodující	k2eAgMnSc1d1	bodující
hráč	hráč	k1gMnSc1	hráč
<g/>
:	:	kIx,	:
Héctor	Héctor	k1gMnSc1	Héctor
Soto	Soto	k1gMnSc1	Soto
</s>
</p>
<p>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
útočník	útočník	k1gMnSc1	útočník
<g/>
:	:	kIx,	:
Dante	Dante	k1gMnSc1	Dante
Amaral	Amaral	k1gMnSc1	Amaral
</s>
</p>
<p>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
blokař	blokař	k1gMnSc1	blokař
<g/>
:	:	kIx,	:
Alexej	Alexej	k1gMnSc1	Alexej
Kulešov	Kulešov	k1gInSc4	Kulešov
</s>
</p>
<p>
<s>
Nejlépe	dobře	k6eAd3	dobře
podávající	podávající	k2eAgMnSc1d1	podávající
hráč	hráč	k1gMnSc1	hráč
<g/>
:	:	kIx,	:
Matey	Mate	k2eAgFnPc1d1	Mate
Kaziyski	Kaziysk	k1gFnPc1	Kaziysk
</s>
</p>
<p>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
nahrávač	nahrávač	k1gMnSc1	nahrávač
<g/>
:	:	kIx,	:
Paweł	Paweł	k1gMnSc1	Paweł
Zagumny	Zagumna	k1gFnSc2	Zagumna
</s>
</p>
<p>
<s>
Nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
Libero	Libero	k1gNnSc1	Libero
<g/>
:	:	kIx,	:
Alexej	Alexej	k1gMnSc1	Alexej
Verbov	Verbov	k1gInSc4	Verbov
</s>
</p>
<p>
<s>
==	==	k?	==
Mistři	mistr	k1gMnPc1	mistr
světa	svět	k1gInSc2	svět
==	==	k?	==
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Brazílie	Brazílie	k1gFnSc1	Brazílie
</s>
</p>
<p>
<s>
Trenér	trenér	k1gMnSc1	trenér
<g/>
:	:	kIx,	:
Bernardo	Bernarda	k1gMnSc5	Bernarda
Rezende	Rezend	k1gMnSc5	Rezend
(	(	kIx(	(
<g/>
Bernardinho	Bernardin	k1gMnSc2	Bernardin
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Konečné	Konečné	k2eAgNnSc1d1	Konečné
pořadí	pořadí	k1gNnSc1	pořadí
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
volejbale	volejbal	k1gInSc6	volejbal
mužů	muž	k1gMnPc2	muž
2006	[number]	k4	2006
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
