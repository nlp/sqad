<s>
Pik	pik	k1gInSc1
Vit	vit	k2eAgInSc1d1
</s>
<s>
п	п	k?
в	в	k?
Pohled	pohled	k1gInSc1
na	na	k7c4
ledovec	ledovec	k1gInSc4
Inylchek	Inylchek	k6eAd1
z	z	k7c2
vrcholu	vrchol	k1gInSc2
</s>
<s>
Vrchol	vrchol	k1gInSc1
</s>
<s>
4215	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Poloha	poloha	k1gFnSc1
Světadíl	světadíl	k1gInSc1
</s>
<s>
Asie	Asie	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Kyrgyzstán	Kyrgyzstán	k2eAgInSc1d1
Kyrgyzstán	Kyrgyzstán	k1gInSc1
Pohoří	pohořet	k5eAaPmIp3nS
</s>
<s>
Ťan-šan	Ťan-šan	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
42	#num#	k4
<g/>
°	°	k?
<g/>
14	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
79	#num#	k4
<g/>
°	°	k?
<g/>
34	#num#	k4
<g/>
′	′	k?
<g/>
43	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Pik	pik	k1gInSc1
Vit	vit	k2eAgInSc1d1
</s>
<s>
Typ	typ	k1gInSc1
</s>
<s>
horský	horský	k2eAgInSc4d1
štít	štít	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pik	Pik	k1gInSc1
Vit	vit	k1gInSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
п	п	k?
в	в	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vrchol	vrchol	k1gInSc1
ležící	ležící	k2eAgInSc1d1
v	v	k7c6
4215	#num#	k4
m	m	kA
n.	n.	kA
m.	m.	kA
v	v	k7c6
jihovýchodním	jihovýchodní	k2eAgInSc6d1
Kyrgyzstánu	Kyrgyzstán	k1gInSc6
v	v	k7c6
pohoří	pohoří	k1gNnSc6
Ťan-šan	Ťan-šan	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Leží	ležet	k5eAaImIp3nS
nedaleko	nedaleko	k7c2
sedla	sedlo	k1gNnSc2
Tjuz	Tjuz	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1
spojuje	spojovat	k5eAaImIp3nS
údolí	údolí	k1gNnSc4
ledovce	ledovec	k1gInSc2
Inylchek	Inylchek	k1gInSc1
a	a	k8xC
údolí	údolí	k1gNnSc4
řeky	řeka	k1gFnSc2
Ťuz	Ťuz	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přístup	přístup	k1gInSc1
</s>
<s>
Z	z	k7c2
jihu	jih	k1gInSc2
se	se	k3xPyFc4
na	na	k7c4
vrchol	vrchol	k1gInSc4
jde	jít	k5eAaImIp3nS
od	od	k7c2
řeky	řeka	k1gFnSc2
Tjuz	Tjuz	k1gInSc1
<g/>
,	,	kIx,
výstup	výstup	k1gInSc1
je	být	k5eAaImIp3nS
nejdříve	dříve	k6eAd3
korytem	koryto	k1gNnSc7
potok	potok	k1gInSc1
až	až	k8xS
posléze	posléze	k6eAd1
přejde	přejít	k5eAaPmIp3nS
na	na	k7c4
pěšinu	pěšina	k1gFnSc4
a	a	k8xC
v	v	k7c6
závěru	závěr	k1gInSc6
se	se	k3xPyFc4
jde	jít	k5eAaImIp3nS
sutí	suť	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cca	cca	kA
500	#num#	k4
metrů	metr	k1gInPc2
pod	pod	k7c7
vrcholem	vrchol	k1gInSc7
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
pěkná	pěkný	k2eAgFnSc1d1
plošina	plošina	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
lze	lze	k6eAd1
stanovat	stanovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
vrchol	vrchol	k1gInSc4
ze	z	k7c2
sedla	sedlo	k1gNnSc2
Tjuz	Tjuza	k1gFnPc2
</s>
<s>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Ze	z	k7c2
severu	sever	k1gInSc2
od	od	k7c2
ledovce	ledovec	k1gInSc2
je	být	k5eAaImIp3nS
výstup	výstup	k1gInSc1
poměrně	poměrně	k6eAd1
prudký	prudký	k2eAgInSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
lze	lze	k6eAd1
zvládnout	zvládnout	k5eAaPmF
bez	bez	k7c2
horolezeckého	horolezecký	k2eAgNnSc2d1
vybavení	vybavení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Pik	pik	k1gInSc1
Vit	vit	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PeakVisor	PeakVisor	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BASSYOVÁ	BASSYOVÁ	kA
<g/>
,	,	kIx,
AuthorAnežka	AuthorAnežka	k1gFnSc1
Rozálie	Rozálie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kyrgyzstán	Kyrgyzstán	k1gInSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
část	část	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
Trek	Trek	k1gInSc4
k	k	k7c3
ledovci	ledovec	k1gInSc3
Inylček	Inylčka	k1gFnPc2
–	–	k?
dokončení	dokončení	k1gNnSc4
dobrodružství	dobrodružství	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hanibal	Hanibal	k1gMnSc1
blog	blog	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-08-29	2018-08-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SVIRDA	SVIRDA	kA
<g/>
.	.	kIx.
#	#	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
výzva	výzva	k1gFnSc1
–	–	k?
Kyrgyzstán	Kyrgyzstán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trek	Treka	k1gFnPc2
k	k	k7c3
ledovci	ledovec	k1gInSc3
Inylček	Inylčko	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Chan	Chan	k1gInSc1
Tengri	Tengr	k1gFnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
</s>
