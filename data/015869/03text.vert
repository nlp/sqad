<s>
Gejza	Gejza	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uherský	uherský	k2eAgInSc1d1
</s>
<s>
Gejza	Gejza	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uherský	uherský	k2eAgInSc1d1
</s>
<s>
Král	Král	k1gMnSc1
uherský	uherský	k2eAgMnSc1d1
</s>
<s>
Gejza	Gejza	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
na	na	k7c6
dobové	dobový	k2eAgFnSc6d1
miniatuře	miniatura	k1gFnSc6
(	(	kIx(
<g/>
Chronicon	Chronicon	k1gInSc1
Pictum	Pictum	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Doba	doba	k1gFnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
1141	#num#	k4
-	-	kIx~
1162	#num#	k4
</s>
<s>
Korunovace	korunovace	k1gFnSc1
</s>
<s>
1141	#num#	k4
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
1130	#num#	k4
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1162	#num#	k4
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Béla	Béla	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uherský	uherský	k2eAgInSc1d1
</s>
<s>
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Štěpán	Štěpán	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uherský	uherský	k2eAgInSc1d1
</s>
<s>
Manželka	manželka	k1gFnSc1
</s>
<s>
Eufrozina	Eufrozin	k2eAgFnSc1d1
Kyjevská	kyjevský	k2eAgFnSc1d1
</s>
<s>
Dynastie	dynastie	k1gFnSc1
</s>
<s>
Arpádovci	Arpádovec	k1gMnPc1
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
Béla	Béla	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uherský	uherský	k2eAgInSc1d1
</s>
<s>
Matka	matka	k1gFnSc1
</s>
<s>
Helena	Helena	k1gFnSc1
Srbská	srbský	k2eAgFnSc1d1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Gejza	Gejza	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uherský	uherský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1130	#num#	k4
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1162	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
uherským	uherský	k2eAgMnSc7d1
králem	král	k1gMnSc7
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
letech	let	k1gInPc6
1141	#num#	k4
–	–	k?
1162	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
rodu	rod	k1gInSc2
Arpádovců	Arpádovec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
synem	syn	k1gMnSc7
uherského	uherský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Bély	Béla	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
Heleny	Helena	k1gFnSc2
<g/>
,	,	kIx,
dcery	dcera	k1gFnSc2
srbského	srbský	k2eAgMnSc2d1
velikého	veliký	k2eAgMnSc2d1
župana	župan	k1gMnSc2
Uroše	Uroš	k1gMnSc2
I.	I.	kA
Korunovaný	korunovaný	k2eAgInSc1d1
za	za	k7c4
uherského	uherský	k2eAgMnSc4d1
krále	král	k1gMnSc4
byl	být	k5eAaImAgMnS
16	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1141	#num#	k4
<g/>
,	,	kIx,
již	již	k6eAd1
3	#num#	k4
dny	den	k1gInPc4
po	po	k7c6
otcově	otcův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dva	dva	k4xCgMnPc4
z	z	k7c2
jeho	jeho	k3xOp3gMnPc2
synů	syn	k1gMnPc2
se	se	k3xPyFc4
také	také	k9
stali	stát	k5eAaPmAgMnP
uherskými	uherský	k2eAgMnPc7d1
králi	král	k1gMnPc7
<g/>
;	;	kIx,
skrze	skrze	k?
dvě	dva	k4xCgFnPc4
své	svůj	k3xOyFgFnSc2
dcery	dcera	k1gFnSc2
je	být	k5eAaImIp3nS
prapředkem	prapředek	k1gMnSc7
rodu	rod	k1gInSc2
Wittelsbachů	Wittelsbach	k1gInPc2
a	a	k8xC
všech	všecek	k3xTgFnPc2
linií	linie	k1gFnPc2
Wettinů	Wettin	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Začátek	začátek	k1gInSc1
vlády	vláda	k1gFnSc2
</s>
<s>
Jelikož	jelikož	k8xS
měl	mít	k5eAaImAgInS
pouze	pouze	k6eAd1
11	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
správu	správa	k1gFnSc4
země	zem	k1gFnSc2
převzal	převzít	k5eAaPmAgMnS
bratr	bratr	k1gMnSc1
jeho	jeho	k3xOp3gFnSc2
matky	matka	k1gFnSc2
Beloš	Beloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začátek	začátek	k1gInSc4
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
byl	být	k5eAaImAgInS
poznamenán	poznamenat	k5eAaPmNgInS
zhoršujícími	zhoršující	k2eAgFnPc7d1
se	s	k7c7
vztahy	vztah	k1gInPc7
s	s	k7c7
římskoněmeckou	římskoněmecký	k2eAgFnSc7d1
říší	říš	k1gFnSc7
<g/>
,	,	kIx,
když	když	k8xS
byly	být	k5eAaImAgInP
zrušeny	zrušen	k2eAgInPc1d1
zásnuby	zásnub	k1gInPc1
mezi	mezi	k7c7
Gejzovou	Gejzový	k2eAgFnSc7d1
sestrou	sestra	k1gFnSc7
Žofií	Žofie	k1gFnSc7
a	a	k8xC
následníkem	následník	k1gMnSc7
německého	německý	k2eAgInSc2d1
trůnu	trůn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzájemné	vzájemný	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
se	se	k3xPyFc4
zhoršily	zhoršit	k5eAaPmAgInP
i	i	k9
s	s	k7c7
Čechami	Čechy	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
scéně	scéna	k1gFnSc6
se	se	k3xPyFc4
také	také	k9
objevil	objevit	k5eAaPmAgMnS
syn	syn	k1gMnSc1
dřívějšího	dřívější	k2eAgMnSc2d1
krále	král	k1gMnSc2
Kolomana	Koloman	k1gMnSc2
Boris	Boris	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římskoněmecký	římskoněmecký	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Konrád	Konrád	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
mu	on	k3xPp3gMnSc3
dovolil	dovolit	k5eAaPmAgMnS
naverbovat	naverbovat	k5eAaPmF
si	se	k3xPyFc3
žoldáky	žoldák	k1gMnPc4
na	na	k7c6
jeho	jeho	k3xOp3gNnPc6
územích	území	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dubnu	duben	k1gInSc6
1146	#num#	k4
Boris	Boris	k1gMnSc1
vpadl	vpadnout	k5eAaPmAgMnS
na	na	k7c6
území	území	k1gNnSc6
Uherska	Uhersko	k1gNnSc2
a	a	k8xC
dobyl	dobýt	k5eAaPmAgInS
Bratislavu	Bratislava	k1gFnSc4
<g/>
,	,	kIx,
jenže	jenže	k8xC
Gejzovi	Gejz	k1gMnSc3
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
okupanty	okupant	k1gMnPc4
podplatit	podplatit	k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
odtáhli	odtáhnout	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útok	útok	k1gInSc1
se	se	k3xPyFc4
však	však	k9
po	po	k7c6
půlroce	půlrok	k1gInSc6
zopakoval	zopakovat	k5eAaPmAgMnS
<g/>
,	,	kIx,
jenže	jenže	k8xC
tentokrát	tentokrát	k6eAd1
byly	být	k5eAaImAgFnP
v	v	k7c6
přímém	přímý	k2eAgNnSc6d1
setkání	setkání	k1gNnSc6
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Litavy	Litava	k1gFnSc2
oddíly	oddíl	k1gInPc1
Jindřicha	Jindřich	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
rakouského	rakouský	k2eAgMnSc2d1
markraběte	markrabě	k1gMnSc2
a	a	k8xC
Borisova	Borisův	k2eAgMnSc2d1
podporovatele	podporovatel	k1gMnSc2
<g/>
,	,	kIx,
poraženy	poražen	k2eAgInPc1d1
uherskými	uherský	k2eAgInPc7d1
vojsky	vojsky	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
přes	přes	k7c4
Uhersko	Uhersko	k1gNnSc4
procházely	procházet	k5eAaImAgInP
německé	německý	k2eAgInPc1d1
oddíly	oddíl	k1gInPc1
na	na	k7c4
Druhou	druhý	k4xOgFnSc4
křižáckou	křižácký	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
tomto	tento	k3xDgNnSc6
vojsku	vojsko	k1gNnSc6
do	do	k7c2
Uher	Uhry	k1gFnPc2
dorazili	dorazit	k5eAaPmAgMnP
i	i	k9
vojáci	voják	k1gMnPc1
francouzského	francouzský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Ludvíka	Ludvík	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
spřátelení	spřátelení	k1gNnSc3
tohoto	tento	k3xDgInSc2
krále	král	k1gMnSc2
s	s	k7c7
Gejzou	Gejza	k1gFnSc7
a	a	k8xC
Ludvík	Ludvík	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
kmotrem	kmotr	k1gMnSc7
Gejzova	Gejzův	k2eAgMnSc4d1
syna	syn	k1gMnSc4
Štěpána	Štěpán	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
tyto	tento	k3xDgInPc1
přechody	přechod	k1gInPc1
proběhly	proběhnout	k5eAaPmAgInP
bez	bez	k7c2
problémů	problém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Zahraniční	zahraniční	k2eAgFnSc1d1
politika	politika	k1gFnSc1
</s>
<s>
Manželstvím	manželství	k1gNnSc7
se	s	k7c7
sestrou	sestra	k1gFnSc7
velkoknížete	velkokníže	k1gNnSc2wR
Kyjevské	kyjevský	k2eAgFnSc2d1
Rusi	Rus	k1gFnSc2
byl	být	k5eAaImAgInS
zatažen	zatáhnout	k5eAaPmNgInS
do	do	k7c2
mocenských	mocenský	k2eAgInPc2d1
sporů	spor	k1gInPc2
v	v	k7c6
této	tento	k3xDgFnSc6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc4
výpravu	výprava	k1gFnSc4
do	do	k7c2
Kyjevské	kyjevský	k2eAgFnSc2d1
rusi	rus	k1gMnPc1
provedl	provést	k5eAaPmAgInS
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1148	#num#	k4
a	a	k8xC
další	další	k2eAgInSc4d1
následující	následující	k2eAgInSc4d1
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
skončily	skončit	k5eAaPmAgInP
bez	bez	k7c2
výraznějších	výrazný	k2eAgInPc2d2
úspěchů	úspěch	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1150	#num#	k4
opět	opět	k6eAd1
vtrhl	vtrhnout	k5eAaPmAgInS
na	na	k7c4
Kyjevskou	kyjevský	k2eAgFnSc4d1
Rus	Rus	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
královští	královský	k2eAgMnPc1d1
rádcové	rádce	k1gMnPc1
byli	být	k5eAaImAgMnP
koupeni	koupit	k5eAaPmNgMnP
haličský	haličský	k2eAgMnSc1d1
knížetem	kníže	k1gMnSc7
a	a	k8xC
přiměli	přimět	k5eAaPmAgMnP
uherského	uherský	k2eAgMnSc4d1
krále	král	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
domů	domů	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInPc1d1
<g/>
,	,	kIx,
šestou	šestý	k4xOgFnSc4
výpravu	výprava	k1gFnSc4
podnikl	podniknout	k5eAaPmAgMnS
Gejza	Gejz	k1gMnSc4
v	v	k7c6
roce	rok	k1gInSc6
1152	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Porazil	porazit	k5eAaPmAgMnS
haličského	haličský	k2eAgMnSc4d1
knížete	kníže	k1gMnSc4
Vladimirka	Vladimirek	k1gMnSc2
Volodareviče	Volodarevič	k1gMnSc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
mu	on	k3xPp3gMnSc3
odplatil	odplatit	k5eAaPmAgMnS
hanebnou	hanebný	k2eAgFnSc4d1
porážku	porážka	k1gFnSc4
z	z	k7c2
předchozího	předchozí	k2eAgInSc2d1
roku	rok	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
pomocí	pomocí	k7c2
podplácení	podplácení	k1gNnSc2
opět	opět	k6eAd1
postaral	postarat	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Gejza	Gejz	k1gMnSc4
nemohl	moct	k5eNaImAgInS
vyhrát	vyhrát	k5eAaPmF
definitivně	definitivně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Srbové	Srb	k1gMnPc1
<g/>
,	,	kIx,
kterým	který	k3yRgMnPc3,k3yIgMnPc3,k3yQgMnPc3
Gejza	Gejza	k1gFnSc1
pomáhal	pomáhat	k5eAaImAgInS
v	v	k7c6
odboji	odboj	k1gInSc6
proti	proti	k7c3
Byzanci	Byzanc	k1gFnSc3
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
byzantským	byzantský	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
Manuelem	Manuel	k1gMnSc7
I.	I.	kA
poražení	poražení	k1gNnSc2
a	a	k8xC
císař	císař	k1gMnSc1
se	se	k3xPyFc4
vydal	vydat	k5eAaPmAgMnS
proti	proti	k7c3
Uhersku	Uhersko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsadil	obsadit	k5eAaPmAgMnS
pevnost	pevnost	k1gFnSc4
Zemun	Zemun	k1gMnSc1
a	a	k8xC
Kolomanův	Kolomanův	k2eAgMnSc1d1
syn	syn	k1gMnSc1
Boris	Boris	k1gMnSc1
pustošil	pustošit	k5eAaImAgMnS
uherské	uherský	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gejza	Gejza	k1gFnSc1
se	se	k3xPyFc4
vydal	vydat	k5eAaPmAgMnS
za	za	k7c7
Borisem	Boris	k1gMnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
ten	ten	k3xDgMnSc1
utekl	utéct	k5eAaPmAgMnS
do	do	k7c2
Byzance	Byzanc	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
nakonec	nakonec	k6eAd1
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1153	#num#	k4
a	a	k8xC
také	také	k9
o	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
se	se	k3xPyFc4
Gejza	Gejz	k1gMnSc2
pokusil	pokusit	k5eAaPmAgMnS
uzavřít	uzavřít	k5eAaPmF
spojenectví	spojenectví	k1gNnSc4
s	s	k7c7
jihoitalskými	jihoitalský	k2eAgMnPc7d1
Normany	Norman	k1gMnPc7
proti	proti	k7c3
Byzanci	Byzanc	k1gFnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
neúspěšně	úspěšně	k6eNd1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
Gejza	Gejza	k1gFnSc1
raději	rád	k6eAd2
s	s	k7c7
byzantským	byzantský	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
uzavřel	uzavřít	k5eAaPmAgMnS
mír	mír	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
podporoval	podporovat	k5eAaImAgInS
císařova	císařův	k2eAgMnSc4d1
bratra	bratr	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
chtěl	chtít	k5eAaImAgMnS
uchvátit	uchvátit	k5eAaPmF
korunu	koruna	k1gFnSc4
pro	pro	k7c4
sebe	sebe	k3xPyFc4
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1154	#num#	k4
podnikl	podniknout	k5eAaPmAgMnS
poměrně	poměrně	k6eAd1
úspěšnou	úspěšný	k2eAgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
na	na	k7c6
území	území	k1gNnSc6
Byzantské	byzantský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Císař	Císař	k1gMnSc1
Manuel	Manuel	k1gMnSc1
se	se	k3xPyFc4
mu	on	k3xPp3gNnSc3
odplatil	odplatit	k5eAaPmAgMnS
vojenskou	vojenský	k2eAgFnSc7d1
výpravou	výprava	k1gFnSc7
do	do	k7c2
Uher	Uhry	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
Gejza	Gejza	k1gFnSc1
raději	rád	k6eAd2
roce	rok	k1gInSc6
1155	#num#	k4
uzavřel	uzavřít	k5eAaPmAgInS
mír	mír	k1gInSc1
na	na	k7c4
5	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgNnSc7d1
nebezpečím	nebezpečí	k1gNnSc7
pro	pro	k7c4
zemi	zem	k1gFnSc4
byl	být	k5eAaImAgMnS
římskoněmecký	římskoněmecký	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
Barbarossa	Barbarossa	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
netajil	tajit	k5eNaImAgMnS
mocenskými	mocenský	k2eAgInPc7d1
zájmy	zájem	k1gInPc7
na	na	k7c4
úkor	úkor	k1gInSc4
Uherska	Uhersko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
povýšil	povýšit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1156	#num#	k4
rakouské	rakouský	k2eAgFnSc2d1
markrabství	markrabství	k1gNnSc4
na	na	k7c4
vévodství	vévodství	k1gNnSc4
a	a	k8xC
vévoda	vévoda	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jasomirgott	Jasomirgott	k1gMnSc1
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
připraven	připravit	k5eAaPmNgInS
na	na	k7c4
boj	boj	k1gInSc4
proti	proti	k7c3
Uhersku	Uhersko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Dynastické	dynastický	k2eAgInPc1d1
spory	spor	k1gInPc1
a	a	k8xC
konec	konec	k1gInSc1
vlády	vláda	k1gFnSc2
</s>
<s>
Velmoži	velmož	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
nebyli	být	k5eNaImAgMnP
spokojeni	spokojen	k2eAgMnPc1d1
s	s	k7c7
Gejzovou	Gejzová	k1gFnSc7
vládou	vláda	k1gFnSc7
<g/>
,	,	kIx,
začali	začít	k5eAaPmAgMnP
organizovat	organizovat	k5eAaBmF
spiknutí	spiknutý	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
jejich	jejich	k3xOp3gNnSc2
čela	čelo	k1gNnSc2
se	se	k3xPyFc4
postavil	postavit	k5eAaPmAgMnS
Gejzův	Gejzův	k2eAgMnSc1d1
mladší	mladý	k2eAgMnSc1d2
bratr	bratr	k1gMnSc1
Štěpán	Štěpán	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spiknutí	spiknutí	k1gNnSc1
však	však	k9
bylo	být	k5eAaImAgNnS
odhaleno	odhalit	k5eAaPmNgNnS
a	a	k8xC
Štěpán	Štěpán	k1gMnSc1
byl	být	k5eAaImAgMnS
nucen	nutit	k5eAaImNgMnS
utéct	utéct	k5eAaPmF
do	do	k7c2
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jenže	jenže	k8xC
německý	německý	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Štěpánovi	Štěpán	k1gMnSc3
odmítl	odmítnout	k5eAaPmAgMnS
pomoct	pomoct	k5eAaPmF
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Byzance	Byzanc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uhersko	Uhersko	k1gNnSc1
-	-	kIx~
německé	německý	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
se	se	k3xPyFc4
však	však	k9
zlepšily	zlepšit	k5eAaPmAgFnP
<g/>
,	,	kIx,
protože	protože	k8xS
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
hledal	hledat	k5eAaImAgMnS
spojence	spojenec	k1gMnPc4
proti	proti	k7c3
Byzantské	byzantský	k2eAgNnSc4d1
říší	říš	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
se	se	k3xPyFc4
papežem	papež	k1gMnSc7
stal	stát	k5eAaPmAgMnS
proticísařsky	proticísařsky	k6eAd1
zaměřený	zaměřený	k2eAgMnSc1d1
Alexandr	Alexandr	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
avšak	avšak	k8xC
císařovi	císařův	k2eAgMnPc1d1
přívrženci	přívrženec	k1gMnPc1
si	se	k3xPyFc3
zvolili	zvolit	k5eAaPmAgMnP
vzdoropapeže	vzdoropapež	k1gMnSc4
Viktora	Viktor	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evropa	Evropa	k1gFnSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
rozdělila	rozdělit	k5eAaPmAgFnS
na	na	k7c4
dva	dva	k4xCgInPc4
tábory	tábor	k1gInPc4
a	a	k8xC
Gejza	Gejz	k1gMnSc2
podpořil	podpořit	k5eAaPmAgMnS
Alexandra	Alexandr	k1gMnSc4
III	III	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
zpečetil	zpečetit	k5eAaPmAgMnS
konkordát	konkordát	k1gInSc4
mezi	mezi	k7c7
Uherskem	Uhersko	k1gNnSc7
a	a	k8xC
papežskou	papežský	k2eAgFnSc7d1
kurií	kurie	k1gFnSc7
uzavřený	uzavřený	k2eAgInSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1161	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Králův	Králův	k2eAgMnSc1d1
nespokojený	spokojený	k2eNgMnSc1d1
bratr	bratr	k1gMnSc1
Štefan	Štefan	k1gMnSc1
se	se	k3xPyFc4
opět	opět	k6eAd1
pokusil	pokusit	k5eAaPmAgMnS
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
stranu	strana	k1gFnSc4
získat	získat	k5eAaPmF
císaře	císař	k1gMnSc4
Fridricha	Fridrich	k1gMnSc4
I.	I.	kA
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
znovu	znovu	k6eAd1
nepodařilo	podařit	k5eNaPmAgNnS
a	a	k8xC
znovu	znovu	k6eAd1
utekl	utéct	k5eAaPmAgMnS
do	do	k7c2
Byzance	Byzanc	k1gFnSc2
<g/>
,	,	kIx,
následovaný	následovaný	k2eAgInSc1d1
bratrem	bratr	k1gMnSc7
Ladislavem	Ladislav	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gejza	Gejza	k1gFnSc1
určil	určit	k5eAaPmAgInS
za	za	k7c4
následníka	následník	k1gMnSc4
trůnu	trůn	k1gInSc2
svého	svůj	k3xOyFgMnSc2
syna	syn	k1gMnSc2
Štěpána	Štěpán	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
již	již	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1152	#num#	k4
vládl	vládnout	k5eAaImAgMnS
jako	jako	k9
spolukrál	spolukrál	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
Gejzově	Gejzův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
tak	tak	k9
Štěpán	Štěpán	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
na	na	k7c4
trůn	trůn	k1gInSc4
nastoupil	nastoupit	k5eAaPmAgMnS
<g/>
,	,	kIx,
jenže	jenže	k8xC
čekaly	čekat	k5eAaImAgFnP
ho	on	k3xPp3gMnSc4
mocenské	mocenský	k2eAgFnPc1d1
zápasy	zápas	k1gInPc7
se	s	k7c7
strýci	strýc	k1gMnPc7
Štěpánem	Štěpán	k1gMnSc7
a	a	k8xC
Ladislavem	Ladislav	k1gMnSc7
podporovanými	podporovaný	k2eAgFnPc7d1
Byzantskou	byzantský	k2eAgFnSc7d1
říší	říš	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Rodina	rodina	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1146	#num#	k4
si	se	k3xPyFc3
vzal	vzít	k5eAaPmAgMnS
za	za	k7c4
manželku	manželka	k1gFnSc4
Eufrozinu	Eufrozin	k1gInSc2
z	z	k7c2
rodu	rod	k1gInSc2
Rurikovců	Rurikovec	k1gMnPc2
<g/>
,	,	kIx,
dceru	dcera	k1gFnSc4
kyjevského	kyjevský	k2eAgNnSc2d1
velkoknížete	velkokníže	k1gNnSc2wR
Mstislava	Mstislava	k1gFnSc1
I.	I.	kA
Spolu	spolu	k6eAd1
měli	mít	k5eAaImAgMnP
tyto	tento	k3xDgFnPc4
děti	dítě	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
Štěpán	Štěpán	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1147	#num#	k4
–	–	k?
4	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
1172	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
uherský	uherský	k2eAgMnSc1d1
král	král	k1gMnSc1
1162	#num#	k4
<g/>
–	–	k?
<g/>
1172	#num#	k4
</s>
<s>
Béla	Béla	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1148	#num#	k4
–	–	k?
23	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
1196	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
uherský	uherský	k2eAgMnSc1d1
král	král	k1gMnSc1
1172	#num#	k4
<g/>
–	–	k?
<g/>
1196	#num#	k4
<g/>
,	,	kIx,
pokračovatel	pokračovatel	k1gMnSc1
rodové	rodový	k2eAgFnSc2d1
linie	linie	k1gFnSc2
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Uherská	uherský	k2eAgFnSc1d1
(	(	kIx(
<g/>
asi	asi	k9
1149	#num#	k4
–	–	k?
po	po	k7c4
r.	r.	kA
1189	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
česká	český	k2eAgFnSc1d1
kněžna	kněžna	k1gFnSc1
jako	jako	k8xS,k8xC
manželka	manželka	k1gFnSc1
knížete	kníže	k1gMnSc2
Bedřicha	Bedřich	k1gMnSc2
<g/>
,	,	kIx,
skrze	skrze	k?
dceru	dcera	k1gFnSc4
Ludmilu	Ludmila	k1gFnSc4
předek	předek	k1gInSc4
rodu	rod	k1gInSc2
Wittelsbachů	Wittelsbach	k1gMnPc2
</s>
<s>
princ	princ	k1gMnSc1
Gejza	Gejz	k1gMnSc2
Uherský	uherský	k2eAgInSc1d1
(	(	kIx(
<g/>
asi	asi	k9
1150	#num#	k4
–	–	k?
před	před	k7c7
r.	r.	kA
1210	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Helena	Helena	k1gFnSc1
Uherská	uherský	k2eAgFnSc1d1
(	(	kIx(
<g/>
asi	asi	k9
1158	#num#	k4
–	–	k?
25	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1199	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
manželka	manželka	k1gFnSc1
rakouského	rakouský	k2eAgMnSc2d1
vévody	vévoda	k1gMnSc2
Leopolda	Leopold	k1gMnSc2
V.	V.	kA
Babenberského	babenberský	k2eAgInSc2d1
<g/>
,	,	kIx,
skrze	skrze	k?
vnučku	vnučka	k1gFnSc4
Konstancii	Konstancie	k1gFnSc4
předek	předek	k1gInSc4
rodu	rod	k1gInSc2
Wettinů	Wettin	k1gMnPc2
</s>
<s>
„	„	k?
</s>
<s>
Kraloval	kralovat	k5eAaImAgMnS
dvadcet	dvadcet	k5eAaImF,k5eAaPmF
let	let	k1gInSc4
<g/>
,	,	kIx,
tři	tři	k4xCgInPc4
měsíce	měsíc	k1gInPc4
a	a	k8xC
jedenáct	jedenáct	k4xCc4
dní	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zplodil	zplodit	k5eAaPmAgMnS
čtyři	čtyři	k4xCgMnPc4
syny	syn	k1gMnPc4
<g/>
:	:	kIx,
Štěpána	Štěpán	k1gMnSc4
<g/>
,	,	kIx,
Bélu	Béla	k1gMnSc4
<g/>
,	,	kIx,
Arpáda	Arpád	k1gMnSc4
a	a	k8xC
Gejzu	Gejza	k1gFnSc4
<g/>
...	...	k?
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Dubnická	dubnický	k2eAgFnSc1d1
kronika	kronika	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Gejza	Gejz	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Uhorsko	Uhorsko	k1gNnSc1
<g/>
)	)	kIx)
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
www.britannica.com	www.britannica.com	k1gInSc1
<g/>
↑	↑	k?
Kronika	kronika	k1gFnSc1
uhorských	uhorský	k2eAgNnPc2d1
kráľov	kráľovo	k1gNnPc2
zvaná	zvaný	k2eAgFnSc1d1
Dubnická	dubnický	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Július	Július	k1gMnSc1
Sopko	sopka	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budmerice	Budmerika	k1gFnSc6
<g/>
:	:	kIx,
Vydavateľstvo	Vydavateľstvo	k1gNnSc1
Rak	rak	k1gMnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
239	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85501	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
89	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
KRISTÓ	KRISTÓ	kA
<g/>
,	,	kIx,
Gyula	Gyulo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Die	Die	k1gFnSc1
Arpaden-Dynastie	Arpaden-Dynastie	k1gFnSc1
:	:	kIx,
die	die	k?
Geschichte	Geschicht	k1gInSc5
Ungarns	Ungarns	k1gInSc4
von	von	k1gInSc1
895	#num#	k4
bis	bis	k?
1301	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budapest	Budapest	k1gInSc1
<g/>
:	:	kIx,
Corvina	Corvina	k1gFnSc1
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
310	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
9631338576	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Segeš	Segeš	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Kniha	kniha	k1gFnSc1
kráľov	kráľov	k1gInSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Bratislava	Bratislava	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-10-01091-X	80-10-01091-X	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Gejza	Gejz	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uherský	uherský	k2eAgInSc4d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Béla	Béla	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Uherský	uherský	k2eAgMnSc1d1
král	král	k1gMnSc1
Gejza	Gejza	k1gFnSc1
II	II	kA
<g/>
.1141	.1141	k4
-	-	kIx~
1162	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Štěpán	Štěpán	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Béla	Béla	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Chorvatský	chorvatský	k2eAgMnSc1d1
a	a	k8xC
dalmatský	dalmatský	k2eAgMnSc1d1
král	král	k1gMnSc1
Gejza	Gejz	k1gMnSc4
I	i	k9
<g/>
.1141	.1141	k4
-	-	kIx~
1162	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Štěpán	Štěpán	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Uherští	uherský	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
Mýtičtí	mýtický	k2eAgMnPc1d1
Arpádovci	Arpádovec	k1gMnPc1
<g/>
(	(	kIx(
<g/>
do	do	k7c2
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Levedi	Leved	k1gMnPc1
•	•	k?
Almoš	Almoš	k1gMnSc1
Arpádovci	Arpádovec	k1gMnPc1
<g/>
(	(	kIx(
<g/>
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
–	–	k?
<g/>
1038	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Arpád	Arpád	k1gMnSc1
(	(	kIx(
<g/>
asi	asi	k9
895	#num#	k4
<g/>
–	–	k?
<g/>
asi	asi	k9
907	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Zoltán	Zoltán	k1gMnSc1
(	(	kIx(
<g/>
náčelník	náčelník	k1gMnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
asi	asi	k9
907	#num#	k4
<g/>
–	–	k?
<g/>
asi	asi	k9
950	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Fajsz	Fajsz	k1gInSc1
(	(	kIx(
<g/>
asi	asi	k9
950	#num#	k4
<g/>
–	–	k?
<g/>
955	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Takšoň	Takšoň	k1gFnSc1
(	(	kIx(
<g/>
955	#num#	k4
<g/>
–	–	k?
<g/>
asi	asi	k9
970	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Gejza	Gejza	k1gFnSc1
(	(	kIx(
<g/>
asi	asi	k9
970	#num#	k4
<g/>
–	–	k?
<g/>
997	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Štěpán	Štěpán	k1gMnSc1
(	(	kIx(
<g/>
997	#num#	k4
<g/>
–	–	k?
<g/>
1038	#num#	k4
<g/>
)	)	kIx)
Nedynastičtí	dynastický	k2eNgMnPc1d1
<g/>
(	(	kIx(
<g/>
1038	#num#	k4
<g/>
-	-	kIx~
<g/>
1046	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Petr	Petr	k1gMnSc1
Orseolo	Orseola	k1gFnSc5
(	(	kIx(
<g/>
1038	#num#	k4
<g/>
–	–	k?
<g/>
1041	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Samuel	Samuel	k1gMnSc1
Aba	Aba	k1gMnSc1
(	(	kIx(
<g/>
1041	#num#	k4
<g/>
–	–	k?
<g/>
1044	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Petr	Petr	k1gMnSc1
Orseolo	Orseola	k1gFnSc5
(	(	kIx(
<g/>
1044	#num#	k4
<g/>
–	–	k?
<g/>
1046	#num#	k4
<g/>
)	)	kIx)
Arpádovci	Arpádovec	k1gMnPc1
<g/>
(	(	kIx(
<g/>
1046	#num#	k4
<g/>
-	-	kIx~
<g/>
1301	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1046	#num#	k4
<g/>
–	–	k?
<g/>
1060	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Béla	Béla	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1060	#num#	k4
<g/>
–	–	k?
<g/>
1063	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Šalamoun	Šalamoun	k1gMnSc1
(	(	kIx(
<g/>
1063	#num#	k4
<g/>
–	–	k?
<g/>
1074	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Gejza	Gejza	k1gFnSc1
I.	I.	kA
(	(	kIx(
<g/>
1074	#num#	k4
<g/>
–	–	k?
<g/>
1077	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ladislav	Ladislav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
1077	#num#	k4
<g/>
–	–	k?
<g/>
1095	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Koloman	Koloman	k1gMnSc1
(	(	kIx(
<g/>
1095	#num#	k4
<g/>
–	–	k?
<g/>
1116	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Štěpán	Štěpán	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1116	#num#	k4
<g/>
-	-	kIx~
<g/>
1131	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Béla	Béla	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1131	#num#	k4
<g/>
–	–	k?
<g/>
1141	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Gejza	Gejza	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1141	#num#	k4
<g/>
–	–	k?
<g/>
1162	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ladislav	Ladislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
¹	¹	k?
(	(	kIx(
<g/>
1162	#num#	k4
<g/>
-	-	kIx~
<g/>
1163	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Štěpán	Štěpán	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1162	#num#	k4
<g/>
–	–	k?
<g/>
1172	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Štěpán	Štěpán	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uherský¹	Uherský¹	k1gFnSc1
(	(	kIx(
<g/>
1163	#num#	k4
<g/>
–	–	k?
<g/>
1165	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Béla	Béla	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1172	#num#	k4
<g/>
–	–	k?
<g/>
1196	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Emerich	Emerich	k1gMnSc1
Uherský	uherský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1196	#num#	k4
<g/>
–	–	k?
<g/>
1204	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ladislav	Ladislav	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1204	#num#	k4
<g/>
-	-	kIx~
<g/>
1205	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ondřej	Ondřej	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1205	#num#	k4
<g/>
-	-	kIx~
<g/>
1235	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Béla	Béla	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1235	#num#	k4
<g/>
-	-	kIx~
<g/>
1270	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Štěpán	Štěpán	k1gMnSc1
V.	V.	kA
(	(	kIx(
<g/>
1270	#num#	k4
<g/>
-	-	kIx~
<g/>
1272	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ladislav	Ladislav	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kumán	Kumán	k1gInSc1
(	(	kIx(
<g/>
1272	#num#	k4
<g/>
-	-	kIx~
<g/>
1290	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ondřej	Ondřej	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1290	#num#	k4
<g/>
-	-	kIx~
<g/>
1301	#num#	k4
<g/>
)	)	kIx)
Boje	boj	k1gInPc1
o	o	k7c4
trůn	trůn	k1gInSc4
<g/>
(	(	kIx(
<g/>
1301	#num#	k4
<g/>
–	–	k?
<g/>
1308	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
V.²	V.²	k1gMnSc1
(	(	kIx(
<g/>
1301	#num#	k4
<g/>
-	-	kIx~
<g/>
1305	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Béla	Béla	k1gMnSc1
V.³	V.³	k1gMnSc1
(	(	kIx(
<g/>
1305	#num#	k4
<g/>
–	–	k?
<g/>
1308	#num#	k4
<g/>
)	)	kIx)
Anjouovci	Anjouovec	k1gInSc6
<g/>
(	(	kIx(
<g/>
1308	#num#	k4
<g/>
–	–	k?
<g/>
1387	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Karel	Karel	k1gMnSc1
I.	I.	kA
Robert	Robert	k1gMnSc1
(	(	kIx(
<g/>
1308	#num#	k4
<g/>
–	–	k?
<g/>
1346	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ludvík	Ludvík	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1346	#num#	k4
<g/>
–	–	k?
<g/>
1382	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Marie	Marie	k1gFnSc1
Uherská⁴	Uherská⁴	k1gFnSc1
(	(	kIx(
<g/>
1382	#num#	k4
<g/>
–	–	k?
<g/>
1395	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Karel	Karel	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
¹	¹	k?
(	(	kIx(
<g/>
1385	#num#	k4
<g/>
–	–	k?
<g/>
1386	#num#	k4
<g/>
)	)	kIx)
Různé	různý	k2eAgInPc1d1
rody	rod	k1gInPc1
<g/>
(	(	kIx(
<g/>
1387	#num#	k4
<g/>
–	–	k?
<g/>
1526	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zikmund	Zikmund	k1gMnSc1
Lucemburský	lucemburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1387	#num#	k4
<g/>
–	–	k?
<g/>
1437	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Albrecht	Albrecht	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1437	#num#	k4
<g/>
–	–	k?
<g/>
1439	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Alžběta	Alžběta	k1gFnSc1
Lucemburská⁵	Lucemburská⁵	k1gFnSc1
(	(	kIx(
<g/>
1439	#num#	k4
<g/>
–	–	k?
<g/>
1440	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vladislav	Vladislav	k1gMnSc1
I.	I.	kA
Jagellonec	Jagellonec	k1gMnSc1
(	(	kIx(
<g/>
1440	#num#	k4
<g/>
–	–	k?
<g/>
1444	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Pohrobek	pohrobek	k1gMnSc1
(	(	kIx(
<g/>
1444	#num#	k4
<g/>
–	–	k?
<g/>
1457	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Matyáš	Matyáš	k1gMnSc1
Korvín	Korvín	k1gMnSc1
(	(	kIx(
<g/>
1458	#num#	k4
<g/>
–	–	k?
<g/>
1490	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vladislav	Vladislav	k1gMnSc1
Jagellonský	jagellonský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1490	#num#	k4
<g/>
–	–	k?
<g/>
1516	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ludvík	Ludvík	k1gMnSc1
Jagellonský	jagellonský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1516	#num#	k4
<g/>
–	–	k?
<g/>
1526	#num#	k4
<g/>
)	)	kIx)
Habsburkové	Habsburk	k1gMnPc1
<g/>
(	(	kIx(
<g/>
1526	#num#	k4
<g/>
–	–	k?
<g/>
1780	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1526	#num#	k4
<g/>
–	–	k?
<g/>
1564	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Maxmilián	Maxmilián	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1564	#num#	k4
<g/>
–	–	k?
<g/>
1576	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Rudolf	Rudolf	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1576	#num#	k4
<g/>
–	–	k?
<g/>
1608	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Matyáš	Matyáš	k1gMnSc1
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1608	#num#	k4
<g/>
–	–	k?
<g/>
1619	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1619	#num#	k4
<g/>
–	–	k?
<g/>
1637	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1637	#num#	k4
<g/>
–	–	k?
<g/>
1657	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburský	habsburský	k2eAgInSc1d1
•	•	k?
Leopold	Leopolda	k1gFnPc2
I.	I.	kA
(	(	kIx(
<g/>
1657	#num#	k4
<g/>
–	–	k?
<g/>
1705	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Josef	Josef	k1gMnSc1
I.	I.	kA
Habsburský	habsburský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1705	#num#	k4
<g/>
–	–	k?
<g/>
1711	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Karel	Karel	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1711	#num#	k4
<g/>
–	–	k?
<g/>
1740	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
(	(	kIx(
<g/>
1740	#num#	k4
<g/>
–	–	k?
<g/>
1780	#num#	k4
<g/>
)	)	kIx)
Habsburko-Lotrinkové	Habsburko-Lotrinkový	k2eAgFnSc2d1
<g/>
(	(	kIx(
<g/>
1780	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Josef	Josef	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1780	#num#	k4
<g/>
–	–	k?
<g/>
1790	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Leopold	Leopolda	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1790	#num#	k4
<g/>
–	–	k?
<g/>
1792	#num#	k4
<g/>
)	)	kIx)
•	•	k?
František	František	k1gMnSc1
I.	I.	kA
Rakouský	rakouský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1792	#num#	k4
<g/>
–	–	k?
<g/>
1835	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivý	dobrotivý	k2eAgMnSc1d1
(	(	kIx(
<g/>
1835	#num#	k4
<g/>
–	–	k?
<g/>
1848	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1848	#num#	k4
<g/>
–	–	k?
<g/>
1916	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Karel	Karel	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
1916	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
¹	¹	k?
vzdorokrál	vzdorokrál	k1gMnSc1
<g/>
,	,	kIx,
²	²	k?
Přemyslovec	Přemyslovec	k1gMnSc1
,	,	kIx,
³	³	k?
Wittelsbach	Wittelsbach	k1gInSc1
<g/>
,	,	kIx,
⁴	⁴	k?
od	od	k7c2
roku	rok	k1gInSc2
1387	#num#	k4
spoluvládkyní	spoluvládkyně	k1gFnPc2
Zikmunda	Zikmund	k1gMnSc2
Lucemburského	lucemburský	k2eAgMnSc2d1
<g/>
,	,	kIx,
⁵	⁵	k?
polooficiální	polooficiální	k2eAgMnPc4d1
vládce	vládce	k1gMnPc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
136974570	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
81230325	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Středověk	středověk	k1gInSc1
</s>
