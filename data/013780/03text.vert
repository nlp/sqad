<s>
Tulkas	Tulkas	k1gMnSc1
</s>
<s>
Tulkas	Tulkas	k1gMnSc1
je	být	k5eAaImIp3nS
postava	postava	k1gFnSc1
z	z	k7c2
fiktivního	fiktivní	k2eAgInSc2d1
světa	svět	k1gInSc2
spisovatele	spisovatel	k1gMnSc2
J.	J.	kA
R.	R.	kA
R.	R.	kA
Tolkiena	Tolkien	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
z	z	k7c2
Valar	Valar	k1gMnPc2
–	–	k?
nejmocnějších	mocný	k2eAgMnPc2d3
duchů	duch	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
vstoupili	vstoupit	k5eAaPmAgMnP
na	na	k7c6
počátku	počátek	k1gInSc6
času	čas	k1gInSc2
do	do	k7c2
Ardy	Arda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tulkas	Tulkas	k1gMnSc1
je	být	k5eAaImIp3nS
popisován	popisován	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
nejsilnější	silný	k2eAgMnSc1d3
bojovník	bojovník	k1gMnSc1
mezi	mezi	k7c4
Valar	Valar	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
manželkou	manželka	k1gFnSc7
je	být	k5eAaImIp3nS
Nessa	Nessa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Zmínky	zmínka	k1gFnPc1
o	o	k7c6
Tulkasovi	Tulkas	k1gMnSc6
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
v	v	k7c6
Tolkienově	Tolkienův	k2eAgInSc6d1
Silmarillionu	Silmarillion	k1gInSc6
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
v	v	k7c6
elfských	elfský	k2eAgInPc6d1
zpěvech	zpěv	k1gInPc6
Valaquenta	Valaquenta	k1gFnSc1
a	a	k8xC
Quenta	Quenta	k1gFnSc1
silmarillion	silmarillion	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Tulkas	Tulkas	k1gMnSc1
byl	být	k5eAaImAgMnS
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
moc	moc	k1gFnSc4
přezdíván	přezdíván	k2eAgInSc4d1
Silný	silný	k2eAgInSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Byl	být	k5eAaImAgMnS
znám	znám	k2eAgMnSc1d1
též	též	k9
jako	jako	k9
Astaldo	Astaldo	k1gNnSc1
čili	čili	k8xC
Udatný	udatný	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Ačkoliv	ačkoliv	k8xS
byl	být	k5eAaImAgMnS
Tulkas	Tulkas	k1gMnSc1
největším	veliký	k2eAgMnSc7d3
válečníkem	válečník	k1gMnSc7
mezi	mezi	k7c4
Valar	Valar	k1gInSc4
<g/>
,	,	kIx,
nepoužíval	používat	k5eNaImAgMnS
žádné	žádný	k3yNgFnPc4
zbraně	zbraň	k1gFnPc4
a	a	k8xC
vždy	vždy	k6eAd1
bojoval	bojovat	k5eAaImAgMnS
holýma	holý	k2eAgFnPc7d1
rukama	ruka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepoužíval	používat	k5eNaImAgMnS
ani	ani	k9
koně	kůň	k1gMnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
byl	být	k5eAaImAgInS
„	„	k?
<g/>
rychlejší	rychlý	k2eAgInSc4d2
než	než	k8xS
cokoliv	cokoliv	k3yInSc4
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
má	mít	k5eAaImIp3nS
nohy	noha	k1gFnPc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Liboval	libovat	k5eAaImAgMnS
si	se	k3xPyFc3
v	v	k7c6
boji	boj	k1gInSc6
a	a	k8xC
v	v	k7c6
měření	měření	k1gNnSc6
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
hře	hra	k1gFnSc6
i	i	k8xC
ve	v	k7c6
válce	válka	k1gFnSc6
se	se	k3xPyFc4
stále	stále	k6eAd1
smál	smát	k5eAaImAgMnS
a	a	k8xC
to	ten	k3xDgNnSc1
dokonce	dokonce	k9
i	i	k9
v	v	k7c6
souboji	souboj	k1gInSc6
s	s	k7c7
Melkorem	Melkor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
Tulkasově	Tulkasův	k2eAgFnSc6d1
síle	síla	k1gFnSc6
svědčil	svědčit	k5eAaImAgInS
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
během	během	k7c2
válek	válka	k1gFnPc2
Valar	Valar	k1gInSc4
s	s	k7c7
Melkorem	Melkor	k1gInSc7
se	se	k3xPyFc4
Tulkas	Tulkas	k1gMnSc1
s	s	k7c7
Temným	temný	k2eAgMnSc7d1
pánem	pán	k1gMnSc7
opakovaně	opakovaně	k6eAd1
utkal	utkat	k5eAaPmAgMnS
a	a	k8xC
vždy	vždy	k6eAd1
zvítězil	zvítězit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tulkas	Tulkas	k1gInSc1
byl	být	k5eAaImAgInS
vyrovnaný	vyrovnaný	k2eAgInSc1d1
<g/>
,	,	kIx,
nerozhněval	rozhněvat	k5eNaPmAgMnS
se	se	k3xPyFc4
snadno	snadno	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
obtížně	obtížně	k6eAd1
však	však	k9
zapomínal	zapomínat	k5eAaImAgMnS
nebo	nebo	k8xC
odpouštěl	odpouštět	k5eAaImAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Byl	být	k5eAaImAgInS
popisován	popisovat	k5eAaImNgInS
jako	jako	k8xC,k8xS
zardělý	zardělý	k2eAgMnSc1d1
muž	muž	k1gMnSc1
se	s	k7c7
zlatými	zlatý	k2eAgInPc7d1
vlasy	vlas	k1gInPc7
a	a	k8xC
vousy	vous	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Role	role	k1gFnSc1
v	v	k7c6
příběhu	příběh	k1gInSc6
</s>
<s>
Tulkas	Tulkas	k1gMnSc1
vstoupil	vstoupit	k5eAaPmAgMnS
do	do	k7c2
Ardy	Arda	k1gFnSc2
jako	jako	k8xC,k8xS
poslední	poslední	k2eAgInPc4d1
z	z	k7c2
Valar	Valara	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
tak	tak	k9
během	během	k7c2
první	první	k4xOgFnSc2
války	válka	k1gFnSc2
mezi	mezi	k7c4
Valar	Valar	k1gInSc4
a	a	k8xC
Temným	temný	k2eAgMnSc7d1
pánem	pán	k1gMnSc7
Melkorem	Melkor	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
Tulkasovu	Tulkasův	k2eAgInSc3d1
příchodu	příchod	k1gInSc3
byl	být	k5eAaImAgMnS
Melkor	Melkor	k1gMnSc1
poprvé	poprvé	k6eAd1
zahnán	zahnat	k5eAaPmNgMnS
z	z	k7c2
Ardy	Arda	k1gFnSc2
do	do	k7c2
vnější	vnější	k2eAgFnSc2d1
temnoty	temnota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tulkas	Tulkas	k1gInSc1
si	se	k3xPyFc3
za	za	k7c4
to	ten	k3xDgNnSc4
vysloužil	vysloužit	k5eAaPmAgInS
Melkorovu	Melkorův	k2eAgFnSc4d1
věčnou	věčný	k2eAgFnSc4d1
nenávist	nenávist	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k8xS
Valar	Valar	k1gMnSc1
vyhnali	vyhnat	k5eAaPmAgMnP
Melkora	Melkora	k1gFnSc1
<g/>
,	,	kIx,
sloužila	sloužit	k5eAaImAgFnS
Tulkasova	Tulkasův	k2eAgFnSc1d1
síla	síla	k1gFnSc1
k	k	k7c3
uklidnění	uklidnění	k1gNnSc3
rozbouřené	rozbouřený	k2eAgFnSc2d1
Ardy	Arda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dokončení	dokončení	k1gNnSc6
díla	dílo	k1gNnSc2
se	se	k3xPyFc4
Valar	Valar	k1gInSc1
shromáždili	shromáždit	k5eAaPmAgMnP
na	na	k7c4
Almarenu	Almarena	k1gFnSc4
a	a	k8xC
zde	zde	k6eAd1
se	se	k3xPyFc4
Tulkas	Tulkas	k1gMnSc1
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Oromëho	Oromëha	k1gFnSc5
sestrou	sestra	k1gFnSc7
Nessou	Nessa	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
Tulkas	Tulkas	k1gMnSc1
po	po	k7c6
hostině	hostina	k1gFnSc6
usnul	usnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
překročil	překročit	k5eAaPmAgMnS
Melkor	Melkor	k1gMnSc1
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
služebníky	služebník	k1gMnPc7
Hradby	hradba	k1gFnSc2
Noci	noc	k1gFnSc2
a	a	k8xC
na	na	k7c6
severu	sever	k1gInSc6
Středozemě	Středozem	k1gFnSc2
vybudoval	vybudovat	k5eAaPmAgInS
své	svůj	k3xOyFgNnSc4
sídlo	sídlo	k1gNnSc4
Utumno	Utumno	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
probuzení	probuzení	k1gNnSc6
elfů	elf	k1gMnPc2
se	se	k3xPyFc4
Tulkas	Tulkas	k1gInSc1
účastnil	účastnit	k5eAaImAgInS
bitvy	bitva	k1gFnSc2
mocností	mocnost	k1gFnSc7
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
Valar	Valar	k1gInSc1
zničili	zničit	k5eAaPmAgMnP
Utumno	Utumno	k6eAd1
a	a	k8xC
zajali	zajmout	k5eAaPmAgMnP
Melkora	Melkor	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tulkas	Tulkasa	k1gFnPc2
tehdy	tehdy	k6eAd1
Melkora	Melkora	k1gFnSc1
přemohl	přemoct	k5eAaPmAgInS
v	v	k7c6
osobním	osobní	k2eAgInSc6d1
souboji	souboj	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Tolkien	Tolkien	k1gInSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
Ronald	Ronald	k1gMnSc1
Reuel	Reuel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Silmarillion	Silmarillion	k1gInSc1
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Tolkien	Tolkien	k1gInSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Str	str	kA
<g/>
.	.	kIx.
27.1	27.1	k4
2	#num#	k4
Tolkien	Tolkina	k1gFnPc2
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Str	str	kA
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Tolkien	Tolkien	k1gInSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Str	str	kA
<g/>
.	.	kIx.
53	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Tolkien	Tolkien	k1gInSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Str	str	kA
<g/>
.	.	kIx.
22	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Tolkien	Tolkien	k1gInSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Str	str	kA
<g/>
.	.	kIx.
27	#num#	k4
-	-	kIx~
28	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Tolkien	Tolkien	k1gInSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Str	str	kA
<g/>
.	.	kIx.
40	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
TOLKIEN	TOLKIEN	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
Ronald	Ronald	k1gMnSc1
Reuel	Reuel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Silmarillion	Silmarillion	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
999	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Valar	Valar	k1gMnSc1
</s>
<s>
Morgoth	Morgoth	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Ainur	Ainur	k1gMnSc1
ve	v	k7c6
Středozemi	Středozem	k1gFnSc6
Ainulindalë	Ainulindalë	k1gFnSc1
(	(	kIx(
<g/>
Hudba	hudba	k1gFnSc1
Ainur	Ainura	k1gFnPc2
<g/>
)	)	kIx)
Páni	pan	k1gMnPc1
Valar	Valara	k1gFnPc2
</s>
<s>
Manwë	Manwë	k?
•	•	k?
Ulmo	Ulmo	k1gMnSc1
•	•	k?
Aulë	Aulë	k1gMnSc1
•	•	k?
Oromë	Oromë	k1gMnSc1
•	•	k?
Mandos	Mandos	k1gMnSc1
(	(	kIx(
<g/>
Námo	Námo	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Lórien	Lórien	k1gInSc1
(	(	kIx(
<g/>
Irmo	Irma	k1gFnSc5
<g/>
)	)	kIx)
•	•	k?
Tulkas	Tulkas	k1gMnSc1
Královny	královna	k1gFnSc2
Valar	Valar	k1gMnSc1
(	(	kIx(
<g/>
Valier	Valier	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Varda	Varda	k1gFnSc1
•	•	k?
Yavanna	Yavann	k1gMnSc2
•	•	k?
Nienna	Nienn	k1gMnSc2
•	•	k?
Estë	Estë	k1gMnSc2
•	•	k?
Vairë	Vairë	k1gMnSc2
•	•	k?
Vána	ván	k2eAgMnSc4d1
•	•	k?
Nessa	Ness	k1gMnSc4
Nepřítel	nepřítel	k1gMnSc1
</s>
<s>
Morgoth	Morgoth	k1gMnSc1
(	(	kIx(
<g/>
Melkor	Melkor	k1gMnSc1
<g/>
)	)	kIx)
Maiar	Maiar	k1gMnSc1
</s>
<s>
Eönwë	Eönwë	k?
•	•	k?
Ilmarë	Ilmarë	k1gMnSc1
•	•	k?
Ossë	Ossë	k1gMnSc1
•	•	k?
Uinen	Uinen	k1gInSc1
•	•	k?
Salmar	Salmar	k1gInSc1
•	•	k?
Sauron	Sauron	k1gInSc1
•	•	k?
Melian	Melian	k1gInSc1
•	•	k?
Arien	Arien	k1gInSc1
•	•	k?
Tilion	Tilion	k1gInSc1
•	•	k?
Balrogové	Balrogový	k2eAgInPc1d1
(	(	kIx(
<g/>
Gothmog	Gothmog	k1gInSc1
<g/>
,	,	kIx,
Durinova	Durinův	k2eAgFnSc1d1
zhouba	zhouba	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Curumo	Curuma	k1gFnSc5
(	(	kIx(
<g/>
Saruman	Saruman	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Olórin	Olórin	k1gInSc1
(	(	kIx(
<g/>
Gandalf	Gandalf	k1gInSc1
nebo	nebo	k8xC
Mithrandir	Mithrandir	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Aiwendil	Aiwendil	k1gFnSc1
(	(	kIx(
<g/>
Radagast	Radagast	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Alatar	Alatar	k1gInSc1
a	a	k8xC
Pallando	Pallanda	k1gFnSc5
(	(	kIx(
<g/>
Modří	modrý	k2eAgMnPc1d1
čarodějové	čaroděj	k1gMnPc1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Tolkien	Tolkien	k2eAgInSc1d1
</s>
