<s>
Nikl	Nikl	k1gMnSc1	Nikl
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Ni	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
Niccolum	Niccolum	k1gInSc1	Niccolum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
feromagnetický	feromagnetický	k2eAgInSc1d1	feromagnetický
<g/>
,	,	kIx,	,
kujný	kujný	k2eAgInSc1d1	kujný
a	a	k8xC	a
tažný	tažný	k2eAgInSc1d1	tažný
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
různých	různý	k2eAgFnPc2d1	různá
slitin	slitina	k1gFnPc2	slitina
a	a	k8xC	a
k	k	k7c3	k
povrchové	povrchový	k2eAgFnSc3d1	povrchová
ochraně	ochrana	k1gFnSc3	ochrana
jiných	jiný	k2eAgInPc2d1	jiný
kovů	kov	k1gInPc2	kov
před	před	k7c7	před
korozí	koroze	k1gFnSc7	koroze
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
toxicitě	toxicita	k1gFnSc3	toxicita
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc4	jeho
praktické	praktický	k2eAgNnSc4d1	praktické
využití	využití	k1gNnSc4	využití
postupně	postupně	k6eAd1	postupně
omezováno	omezovat	k5eAaImNgNnS	omezovat
<g/>
.	.	kIx.	.
</s>
<s>
Typický	typický	k2eAgInSc1d1	typický
kovový	kovový	k2eAgInSc1d1	kovový
ferromagnetický	ferromagnetický	k2eAgInSc1d1	ferromagnetický
prvek	prvek	k1gInSc1	prvek
stříbrobílý	stříbrobílý	k2eAgInSc1d1	stříbrobílý
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
lesklý	lesklý	k2eAgInSc1d1	lesklý
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
<s>
Nikl	Nikl	k1gMnSc1	Nikl
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
výborně	výborně	k6eAd1	výborně
leštit	leštit	k5eAaImF	leštit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
tažný	tažný	k2eAgInSc1d1	tažný
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
kovat	kovat	k5eAaImF	kovat
<g/>
,	,	kIx,	,
svářet	svářet	k5eAaImF	svářet
a	a	k8xC	a
válcovat	válcovat	k5eAaImF	válcovat
na	na	k7c4	na
plech	plech	k1gInSc4	plech
nebo	nebo	k8xC	nebo
vytahovat	vytahovat	k5eAaImF	vytahovat
v	v	k7c4	v
dráty	drát	k1gInPc4	drát
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
přechodné	přechodný	k2eAgInPc4d1	přechodný
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
valenční	valenční	k2eAgInPc1d1	valenční
elektrony	elektron	k1gInPc1	elektron
v	v	k7c6	v
d-sféře	dféra	k1gFnSc6	d-sféra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
především	především	k9	především
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
Ni	on	k3xPp3gFnSc4	on
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
sloučeniny	sloučenina	k1gFnPc1	sloučenina
Ni	on	k3xPp3gFnSc4	on
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
látky	látka	k1gFnPc1	látka
obsahující	obsahující	k2eAgFnPc1d1	obsahující
Ni	on	k3xPp3gFnSc4	on
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
jsou	být	k5eAaImIp3nP	být
nestálé	stálý	k2eNgInPc1d1	nestálý
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
silně	silně	k6eAd1	silně
oxidačně	oxidačně	k6eAd1	oxidačně
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zředěných	zředěný	k2eAgFnPc6d1	zředěná
minerálních	minerální	k2eAgFnPc6d1	minerální
kyselinách	kyselina	k1gFnPc6	kyselina
se	se	k3xPyFc4	se
nikl	nikl	k1gInSc1	nikl
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hůře	zle	k6eAd2	zle
než	než	k8xS	než
železo	železo	k1gNnSc4	železo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
koncentrovaných	koncentrovaný	k2eAgFnPc6d1	koncentrovaná
kyselinách	kyselina	k1gFnPc6	kyselina
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
ještě	ještě	k9	ještě
o	o	k7c4	o
něco	něco	k3yInSc4	něco
hůře	zle	k6eAd2	zle
a	a	k8xC	a
koncentrovanou	koncentrovaný	k2eAgFnSc7d1	koncentrovaná
kyselinou	kyselina	k1gFnSc7	kyselina
dusičnou	dusičný	k2eAgFnSc4d1	dusičná
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
pasivuje	pasivovat	k5eAaBmIp3nS	pasivovat
<g/>
.	.	kIx.	.
</s>
<s>
Nepůsobí	působit	k5eNaImIp3nS	působit
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
suché	suchý	k2eAgInPc1d1	suchý
halogenovodíky	halogenovodík	k1gInPc4	halogenovodík
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
normální	normální	k2eAgFnSc2d1	normální
teploty	teplota	k1gFnSc2	teplota
je	být	k5eAaImIp3nS	být
vůči	vůči	k7c3	vůči
působení	působení	k1gNnSc3	působení
vzduchu	vzduch	k1gInSc2	vzduch
i	i	k8xC	i
vody	voda	k1gFnSc2	voda
nikl	niknout	k5eAaImAgInS	niknout
poměrně	poměrně	k6eAd1	poměrně
stálý	stálý	k2eAgInSc1d1	stálý
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
často	často	k6eAd1	často
k	k	k7c3	k
povrchové	povrchový	k2eAgFnSc3d1	povrchová
ochraně	ochrana	k1gFnSc3	ochrana
jiných	jiný	k2eAgInPc2d1	jiný
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jemně	jemně	k6eAd1	jemně
rozptýleném	rozptýlený	k2eAgInSc6d1	rozptýlený
stavu	stav	k1gInSc6	stav
je	být	k5eAaImIp3nS	být
nikl	nikl	k1gInSc1	nikl
pyroforický	pyroforický	k2eAgInSc1d1	pyroforický
tj.	tj.	kA	tj.
samozápalný	samozápalný	k2eAgInSc1d1	samozápalný
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zahřívání	zahřívání	k1gNnSc6	zahřívání
v	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
kyslíku	kyslík	k1gInSc6	kyslík
shoří	shořet	k5eAaPmIp3nS	shořet
nikl	nikl	k1gInSc1	nikl
za	za	k7c4	za
jiskření	jiskření	k1gNnSc4	jiskření
a	a	k8xC	a
i	i	k9	i
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
prvky	prvek	k1gInPc7	prvek
se	se	k3xPyFc4	se
za	za	k7c2	za
vyšší	vysoký	k2eAgFnSc2d2	vyšší
teploty	teplota	k1gFnSc2	teplota
slučuje	slučovat	k5eAaImIp3nS	slučovat
(	(	kIx(	(
<g/>
chlor	chlor	k1gInSc1	chlor
<g/>
,	,	kIx,	,
brom	brom	k1gInSc1	brom
<g/>
,	,	kIx,	,
fosfor	fosfor	k1gInSc1	fosfor
<g/>
,	,	kIx,	,
arsen	arsen	k1gInSc1	arsen
<g/>
,	,	kIx,	,
antimon	antimon	k1gInSc1	antimon
<g/>
,	,	kIx,	,
hliník	hliník	k1gInSc1	hliník
<g/>
,	,	kIx,	,
bor	bor	k1gInSc1	bor
<g/>
,	,	kIx,	,
křemík	křemík	k1gInSc1	křemík
<g/>
,	,	kIx,	,
síra	síra	k1gFnSc1	síra
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Je	být	k5eAaImIp3nS	být
také	také	k9	také
značně	značně	k6eAd1	značně
stálý	stálý	k2eAgMnSc1d1	stálý
vůči	vůči	k7c3	vůči
působení	působení	k1gNnSc3	působení
alkálií	alkálie	k1gFnPc2	alkálie
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
zařízení	zařízení	k1gNnSc2	zařízení
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
alkalickými	alkalický	k2eAgInPc7d1	alkalický
hydroxidy	hydroxid	k1gInPc7	hydroxid
<g/>
.	.	kIx.	.
</s>
<s>
Kovový	kovový	k2eAgInSc1d1	kovový
nikl	nikl	k1gInSc1	nikl
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
při	při	k7c6	při
mírném	mírný	k2eAgInSc6d1	mírný
žáru	žár	k1gInSc6	žár
amoniak	amoniak	k1gInSc4	amoniak
na	na	k7c4	na
dusík	dusík	k1gInSc4	dusík
a	a	k8xC	a
vodík	vodík	k1gInSc4	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Nikl	Nikl	k1gMnSc1	Nikl
má	mít	k5eAaImIp3nS	mít
schopnost	schopnost	k1gFnSc4	schopnost
pohlcovat	pohlcovat	k5eAaImF	pohlcovat
velká	velký	k2eAgNnPc4d1	velké
množství	množství	k1gNnPc4	množství
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
za	za	k7c2	za
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
houbovitý	houbovitý	k2eAgInSc1d1	houbovitý
nikl	nikl	k1gInSc1	nikl
využívá	využívat	k5eAaImIp3nS	využívat
jako	jako	k9	jako
katalyzátor	katalyzátor	k1gInSc1	katalyzátor
při	při	k7c6	při
hydrogenacích	hydrogenace	k1gFnPc6	hydrogenace
<g/>
.	.	kIx.	.
</s>
<s>
Předměty	předmět	k1gInPc1	předmět
ze	z	k7c2	z
slitin	slitina	k1gFnPc2	slitina
niklu	nikl	k1gInSc2	nikl
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
nalézt	nalézt	k5eAaBmF	nalézt
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
stáří	stáří	k1gNnSc1	stáří
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nikl	nikl	k1gInSc1	nikl
byl	být	k5eAaImAgInS	být
objeven	objeven	k2eAgInSc1d1	objeven
roku	rok	k1gInSc2	rok
1751	[number]	k4	1751
německým	německý	k2eAgMnSc7d1	německý
chemikem	chemik	k1gMnSc7	chemik
baronem	baron	k1gMnSc7	baron
Axelem	Axel	k1gMnSc7	Axel
Frederikem	Frederik	k1gMnSc7	Frederik
Cronstedtem	Cronstedt	k1gMnSc7	Cronstedt
při	při	k7c6	při
pokusech	pokus	k1gInPc6	pokus
o	o	k7c4	o
izolaci	izolace	k1gFnSc4	izolace
mědi	měď	k1gFnSc2	měď
z	z	k7c2	z
rudy	ruda	k1gFnSc2	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
prvek	prvek	k1gInSc1	prvek
pojmenoval	pojmenovat	k5eAaPmAgInS	pojmenovat
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gInSc2	jeho
výskytu	výskyt	k1gInSc2	výskyt
v	v	k7c6	v
rudě	ruda	k1gFnSc6	ruda
nikelinu	nikelin	k1gInSc2	nikelin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hornické	hornický	k2eAgFnSc6d1	hornická
mluvě	mluva	k1gFnSc6	mluva
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
slovo	slovo	k1gNnSc1	slovo
nikl	niknout	k5eAaImAgInS	niknout
hanlivým	hanlivý	k2eAgInSc7d1	hanlivý
výrazem	výraz	k1gInSc7	výraz
pro	pro	k7c4	pro
rudu	ruda	k1gFnSc4	ruda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
horníci	horník	k1gMnPc1	horník
očekávali	očekávat	k5eAaImAgMnP	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
obsahovat	obsahovat	k5eAaImF	obsahovat
měď	měď	k1gFnSc4	měď
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
zpracovávání	zpracovávání	k1gNnSc6	zpracovávání
odolávala	odolávat	k5eAaImAgFnS	odolávat
veškerému	veškerý	k3xTgMnSc3	veškerý
úsilí	úsilí	k1gNnSc1	úsilí
při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
získávání	získávání	k1gNnSc6	získávání
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
po	po	k7c6	po
objevu	objev	k1gInSc6	objev
niklu	nikl	k1gInSc2	nikl
zastávali	zastávat	k5eAaImAgMnP	zastávat
někteří	některý	k3yIgMnPc1	některý
chemici	chemik	k1gMnPc1	chemik
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikelin	nikelin	k1gInSc1	nikelin
je	být	k5eAaImIp3nS	být
měděná	měděný	k2eAgFnSc1d1	měděná
ruda	ruda	k1gFnSc1	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
Torbern	Torbern	k1gMnSc1	Torbern
Bergman	Bergman	k1gMnSc1	Bergman
roku	rok	k1gInSc2	rok
1775	[number]	k4	1775
popsal	popsat	k5eAaPmAgMnS	popsat
přesněji	přesně	k6eAd2	přesně
povahu	povaha	k1gFnSc4	povaha
niklu	nikl	k1gInSc2	nikl
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc1	jeho
podobnost	podobnost	k1gFnSc1	podobnost
s	s	k7c7	s
železem	železo	k1gNnSc7	železo
<g/>
)	)	kIx)	)
a	a	k8xC	a
připravil	připravit	k5eAaPmAgInS	připravit
nikl	nikl	k1gInSc1	nikl
v	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
relativně	relativně	k6eAd1	relativně
lehký	lehký	k2eAgInSc1d1	lehký
prvek	prvek	k1gInSc1	prvek
je	být	k5eAaImIp3nS	být
nikl	nikl	k1gInSc4	nikl
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
poměrně	poměrně	k6eAd1	poměrně
hojně	hojně	k6eAd1	hojně
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
jeho	jeho	k3xOp3gInSc1	jeho
průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
činí	činit	k5eAaImIp3nS	činit
kolem	kolem	k7c2	kolem
100	[number]	k4	100
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
,	,	kIx,	,
t.j.	t.j.	k?	t.j.
asi	asi	k9	asi
100	[number]	k4	100
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
parts	partsa	k1gFnPc2	partsa
per	pero	k1gNnPc2	pero
milion	milion	k4xCgInSc4	milion
=	=	kIx~	=
počet	počet	k1gInSc4	počet
částic	částice	k1gFnPc2	částice
na	na	k7c4	na
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
částic	částice	k1gFnPc2	částice
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
výskytu	výskyt	k1gInSc6	výskyt
přechodných	přechodný	k2eAgInPc2d1	přechodný
prvků	prvek	k1gInPc2	prvek
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
5,4	[number]	k4	5,4
mikrogramu	mikrogram	k1gInSc2	mikrogram
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
litru	litr	k1gInSc6	litr
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
niklu	nikl	k1gInSc2	nikl
přibližně	přibližně	k6eAd1	přibližně
700	[number]	k4	700
000	[number]	k4	000
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ryzím	ryzí	k2eAgInSc7d1	ryzí
niklem	nikl	k1gInSc7	nikl
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
setkáme	setkat	k5eAaPmIp1nP	setkat
pouze	pouze	k6eAd1	pouze
vzácně	vzácně	k6eAd1	vzácně
ve	v	k7c6	v
slitině	slitina	k1gFnSc6	slitina
s	s	k7c7	s
železem	železo	k1gNnSc7	železo
v	v	k7c6	v
železných	železný	k2eAgInPc6d1	železný
meteoritech	meteorit	k1gInPc6	meteorit
<g/>
,	,	kIx,	,
dopadajících	dopadající	k2eAgInPc2d1	dopadající
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
z	z	k7c2	z
kosmického	kosmický	k2eAgInSc2d1	kosmický
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Geologové	geolog	k1gMnPc1	geolog
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
niklu	nikl	k1gInSc2	nikl
přítomného	přítomný	k2eAgInSc2d1	přítomný
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
je	být	k5eAaImIp3nS	být
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jejího	její	k3xOp3gInSc2	její
středu	střed	k1gInSc2	střed
-	-	kIx~	-
v	v	k7c6	v
zemském	zemský	k2eAgNnSc6d1	zemské
jádře	jádro	k1gNnSc6	jádro
-	-	kIx~	-
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
z	z	k7c2	z
analogie	analogie	k1gFnSc2	analogie
s	s	k7c7	s
meteority	meteorit	k1gInPc7	meteorit
<g/>
.	.	kIx.	.
</s>
<s>
Nikl	Nikl	k1gMnSc1	Nikl
má	mít	k5eAaImIp3nS	mít
afinitu	afinita	k1gFnSc4	afinita
ke	k	k7c3	k
kyslíku	kyslík	k1gInSc3	kyslík
i	i	k9	i
k	k	k7c3	k
síře	síra	k1gFnSc3	síra
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
arzenu	arzen	k1gInSc2	arzen
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
tedy	tedy	k9	tedy
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
ložisek	ložisko	k1gNnPc2	ložisko
<g/>
.	.	kIx.	.
</s>
<s>
Jednak	jednak	k8xC	jednak
primární	primární	k2eAgFnSc1d1	primární
<g/>
,	,	kIx,	,
sulfidická	sulfidický	k2eAgFnSc1d1	sulfidická
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
sirnících	sirník	k1gInPc6	sirník
a	a	k8xC	a
arzenidech	arzenid	k1gInPc6	arzenid
-	-	kIx~	-
sulfid	sulfid	k1gInSc1	sulfid
nikelnato-železitý	nikelnato-železitý	k2eAgInSc1d1	nikelnato-železitý
-	-	kIx~	-
pentlandit	pentlandit	k1gInSc1	pentlandit
(	(	kIx(	(
<g/>
Ni	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
Fe	Fe	k1gMnSc1	Fe
<g/>
)	)	kIx)	)
<g/>
9	[number]	k4	9
<g/>
S	s	k7c7	s
<g/>
8	[number]	k4	8
millerit	millerit	k1gInSc4	millerit
NiS	Nisa	k1gFnPc2	Nisa
<g/>
,	,	kIx,	,
nikelin	nikelin	k1gInSc1	nikelin
NiAs	NiAs	k1gInSc1	NiAs
<g/>
,	,	kIx,	,
breithauptit	breithauptit	k1gInSc1	breithauptit
NiSb	NiSb	k1gInSc1	NiSb
<g/>
,	,	kIx,	,
chloantit	chloantit	k1gInSc1	chloantit
NiAs	NiAs	k1gInSc1	NiAs
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
gersdorfit	gersdorfit	k1gMnSc1	gersdorfit
NiAsS	NiAsS	k1gMnSc1	NiAsS
<g/>
,	,	kIx,	,
smaltin	smaltin	k1gInSc1	smaltin
(	(	kIx(	(
<g/>
Ni	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
Co	co	k3yInSc1	co
<g/>
,	,	kIx,	,
Fe	Fe	k1gMnSc1	Fe
<g/>
)	)	kIx)	)
<g/>
As	as	k1gInSc1	as
<g/>
2	[number]	k4	2
a	a	k8xC	a
ullmanit	ullmanit	k1gInSc1	ullmanit
NiSbS	NiSbS	k1gFnSc2	NiSbS
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
sekundární	sekundární	k2eAgFnSc1d1	sekundární
<g/>
,	,	kIx,	,
kyslíkatá	kyslíkatý	k2eAgFnSc1d1	kyslíkatá
<g/>
,	,	kIx,	,
lateritická	lateritický	k2eAgFnSc1d1	lateritický
<g/>
,	,	kIx,	,
vznikající	vznikající	k2eAgFnSc1d1	vznikající
zvětráváním	zvětrávání	k1gNnSc7	zvětrávání
olivínu	olivín	k1gInSc2	olivín
z	z	k7c2	z
ultrabazických	ultrabazický	k2eAgFnPc2d1	ultrabazický
hornin	hornina	k1gFnPc2	hornina
zemského	zemský	k2eAgInSc2d1	zemský
pláště	plášť	k1gInSc2	plášť
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Vrstvy	vrstva	k1gFnPc1	vrstva
zvětralin	zvětralina	k1gFnPc2	zvětralina
<g/>
,	,	kIx,	,
nabohacené	nabohacený	k2eAgNnSc1d1	nabohacený
železem	železo	k1gNnSc7	železo
<g/>
,	,	kIx,	,
hliníkem	hliník	k1gInSc7	hliník
a	a	k8xC	a
ochuzené	ochuzený	k2eAgNnSc1d1	ochuzené
o	o	k7c4	o
křemík	křemík	k1gInSc4	křemík
a	a	k8xC	a
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
laterity	laterit	k1gInPc1	laterit
a	a	k8xC	a
vznikají	vznikat	k5eAaImIp3nP	vznikat
lateritickým	lateritický	k2eAgNnSc7d1	lateritický
zvětráváním	zvětrávání	k1gNnSc7	zvětrávání
<g/>
.	.	kIx.	.
</s>
<s>
Niklonosné	Niklonosný	k2eAgInPc1d1	Niklonosný
laterity	laterit	k1gInPc1	laterit
se	se	k3xPyFc4	se
typicky	typicky	k6eAd1	typicky
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
zvětralinách	zvětralina	k1gFnPc6	zvětralina
hadcových	hadcový	k2eAgFnPc2d1	hadcová
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byly	být	k5eAaImAgFnP	být
původně	původně	k6eAd1	původně
olivinické	olivinický	k2eAgFnPc1d1	olivinický
horniny	hornina	k1gFnPc1	hornina
ze	z	k7c2	z
zemského	zemský	k2eAgInSc2d1	zemský
pláště	plášť	k1gInSc2	plášť
<g/>
,	,	kIx,	,
z	z	k7c2	z
hloubek	hloubka	k1gFnPc2	hloubka
okolo	okolo	k7c2	okolo
50	[number]	k4	50
km	km	kA	km
a	a	k8xC	a
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Olivín	olivín	k1gInSc1	olivín
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
nestálý	stálý	k2eNgInSc4d1	nestálý
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
zvětrává	zvětrávat	k5eAaImIp3nS	zvětrávat
<g/>
.	.	kIx.	.
</s>
<s>
Nikl	Nikl	k1gMnSc1	Nikl
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
byl	být	k5eAaImAgMnS	být
původně	původně	k6eAd1	původně
vázán	vázat	k5eAaImNgMnS	vázat
jako	jako	k8xC	jako
příměs	příměs	k1gFnSc1	příměs
a	a	k8xC	a
zvětráváním	zvětrávání	k1gNnSc7	zvětrávání
přešel	přejít	k5eAaPmAgInS	přejít
do	do	k7c2	do
hydrosilikátů	hydrosilikát	k1gInPc2	hydrosilikát
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
garnieritu	garnierit	k1gInSc3	garnierit
(	(	kIx(	(
<g/>
Ni	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
Mg	mg	kA	mg
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
Si	se	k3xPyFc3	se
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
5	[number]	k4	5
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
současně	současně	k6eAd1	současně
těženým	těžený	k2eAgNnSc7d1	těžené
nalezištěm	naleziště	k1gNnSc7	naleziště
niklových	niklový	k2eAgFnPc2d1	niklová
rud	ruda	k1gFnPc2	ruda
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
pochází	pocházet	k5eAaImIp3nS	pocházet
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
světové	světový	k2eAgFnSc2d1	světová
produkce	produkce	k1gFnSc2	produkce
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kanadské	kanadský	k2eAgFnPc4d1	kanadská
Sudbury	Sudbura	k1gFnPc4	Sudbura
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
bylo	být	k5eAaImAgNnS	být
objeveno	objeven	k2eAgNnSc1d1	objeveno
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
při	při	k7c6	při
výstavbě	výstavba	k1gFnSc6	výstavba
trati	trať	k1gFnSc2	trať
pro	pro	k7c4	pro
Kanadskou	kanadský	k2eAgFnSc4d1	kanadská
pacifickou	pacifický	k2eAgFnSc4d1	Pacifická
železnici	železnice	k1gFnSc4	železnice
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Ontario	Ontario	k1gNnSc1	Ontario
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
původem	původ	k1gInSc7	původ
těchto	tento	k3xDgFnPc2	tento
rud	ruda	k1gFnPc2	ruda
je	být	k5eAaImIp3nS	být
obrovský	obrovský	k2eAgInSc1d1	obrovský
meteorický	meteorický	k2eAgInSc1d1	meteorický
zásah	zásah	k1gInSc1	zásah
Země	zem	k1gFnSc2	zem
v	v	k7c6	v
dávných	dávný	k2eAgFnPc6d1	dávná
geologických	geologický	k2eAgFnPc6d1	geologická
dobách	doba	k1gFnPc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
oblasti	oblast	k1gFnPc1	oblast
s	s	k7c7	s
bohatým	bohatý	k2eAgInSc7d1	bohatý
výskytem	výskyt	k1gInSc7	výskyt
niklových	niklový	k2eAgFnPc2d1	niklová
rud	ruda	k1gFnPc2	ruda
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
Rusko	Rusko	k1gNnSc4	Rusko
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
okolí	okolí	k1gNnSc6	okolí
sibiřského	sibiřský	k2eAgNnSc2d1	sibiřské
města	město	k1gNnSc2	město
Norilsk	Norilsk	k1gInSc1	Norilsk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
Kaledonie	Kaledonie	k1gFnSc1	Kaledonie
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Kuba	Kuba	k1gFnSc1	Kuba
a	a	k8xC	a
Indonésie	Indonésie	k1gFnSc1	Indonésie
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnPc1d3	nejdůležitější
rudy	ruda	k1gFnPc1	ruda
niklu	nikl	k1gInSc2	nikl
jsou	být	k5eAaImIp3nP	být
novokaledonský	novokaledonský	k2eAgInSc4d1	novokaledonský
garnierit	garnierit	k1gInSc4	garnierit
(	(	kIx(	(
<g/>
Ni	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
Mg	mg	kA	mg
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
Si	se	k3xPyFc3	se
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
5	[number]	k4	5
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
a	a	k8xC	a
kanadský	kanadský	k2eAgMnSc1d1	kanadský
pyrrhotin	pyrrhotina	k1gFnPc2	pyrrhotina
s	s	k7c7	s
příměsí	příměs	k1gFnSc7	příměs
pentlanditu	pentlandit	k1gInSc2	pentlandit
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
průměrně	průměrně	k6eAd1	průměrně
3	[number]	k4	3
%	%	kIx~	%
niklu	nikl	k1gInSc2	nikl
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
obou	dva	k4xCgFnPc6	dva
výrobách	výroba	k1gFnPc6	výroba
probíhá	probíhat	k5eAaImIp3nS	probíhat
získávání	získávání	k1gNnSc1	získávání
niklu	nikl	k1gInSc2	nikl
přes	přes	k7c4	přes
tyto	tento	k3xDgInPc4	tento
dva	dva	k4xCgInPc4	dva
kroky	krok	k1gInPc4	krok
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
7	[number]	k4	7
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c6	o
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
6	[number]	k4	6
:	:	kIx,	:
N	N	kA	N
i	i	k8xC	i
O	O	kA	O
+	+	kIx~	+
4	[number]	k4	4
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gInSc1	mathsf
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
Ni_	Ni_	k1gMnSc1	Ni_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
S_	S_	k1gFnSc2	S_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
7	[number]	k4	7
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
O_	O_	k1gMnSc1	O_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
6	[number]	k4	6
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
NiO	NiO	k1gMnSc1	NiO
<g/>
+	+	kIx~	+
<g/>
4	[number]	k4	4
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
SO_	SO_	k1gMnSc1	SO_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
Sulfid	sulfid	k1gInSc1	sulfid
niklitý	niklitý	k2eAgInSc1d1	niklitý
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
oxidu	oxid	k1gInSc2	oxid
nikelnatého	nikelnatý	k2eAgInSc2d1	nikelnatý
<g />
.	.	kIx.	.
</s>
<s hack="1">
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
siřičitého	siřičitý	k2eAgInSc2d1	siřičitý
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
N	N	kA	N
i	i	k8xC	i
O	O	kA	O
+	+	kIx~	+
C	C	kA	C
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
N	N	kA	N
i	i	k9	i
+	+	kIx~	+
C	C	kA	C
O	O	kA	O
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gMnSc1	mathsf
{	{	kIx(	{
<g/>
NiO	NiO	k1gMnSc1	NiO
<g/>
+	+	kIx~	+
<g/>
C	C	kA	C
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
Ni	on	k3xPp3gFnSc4	on
<g/>
+	+	kIx~	+
<g/>
CO	co	k8xS	co
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Oxid	oxid	k1gInSc1	oxid
nikelnatý	nikelnatý	k2eAgInSc1d1	nikelnatý
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
uhlíkem	uhlík	k1gInSc7	uhlík
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
niklu	nikl	k1gInSc2	nikl
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
uhlenatého	uhlenatý	k2eAgInSc2d1	uhlenatý
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
niklu	nikl	k1gInSc2	nikl
z	z	k7c2	z
garnieritu	garnierit	k1gInSc2	garnierit
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
mimořádná	mimořádný	k2eAgFnSc1d1	mimořádná
afinita	afinita	k1gFnSc1	afinita
niklu	nikl	k1gInSc2	nikl
k	k	k7c3	k
síře	síra	k1gFnSc3	síra
<g/>
.	.	kIx.	.
</s>
<s>
Ruda	ruda	k1gFnSc1	ruda
se	se	k3xPyFc4	se
taví	tavit	k5eAaImIp3nS	tavit
se	s	k7c7	s
sloučeninami	sloučenina	k1gFnPc7	sloučenina
snadno	snadno	k6eAd1	snadno
odštěpujícími	odštěpující	k2eAgFnPc7d1	odštěpující
síru	síra	k1gFnSc4	síra
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
vzniká	vznikat	k5eAaImIp3nS	vznikat
Ni	on	k3xPp3gFnSc4	on
<g/>
3	[number]	k4	3
<g/>
S	s	k7c7	s
<g/>
2	[number]	k4	2
a	a	k8xC	a
nečistoty	nečistota	k1gFnPc1	nečistota
přechází	přecházet	k5eAaImIp3nP	přecházet
jako	jako	k9	jako
křemičitany	křemičitan	k1gInPc1	křemičitan
do	do	k7c2	do
strusky	struska	k1gFnSc2	struska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
konvertoru	konvertor	k1gInSc6	konvertor
se	se	k3xPyFc4	se
částečným	částečný	k2eAgNnSc7d1	částečné
vypražením	vypražení	k1gNnSc7	vypražení
<g/>
,	,	kIx,	,
opakovaným	opakovaný	k2eAgNnSc7d1	opakované
tavením	tavení	k1gNnSc7	tavení
s	s	k7c7	s
přísadou	přísada	k1gFnSc7	přísada
křemene	křemen	k1gInSc2	křemen
odstraní	odstranit	k5eAaPmIp3nS	odstranit
železo	železo	k1gNnSc4	železo
a	a	k8xC	a
zbude	zbýt	k5eAaPmIp3nS	zbýt
tak	tak	k6eAd1	tak
čistý	čistý	k2eAgInSc1d1	čistý
Ni	on	k3xPp3gFnSc4	on
<g/>
3	[number]	k4	3
<g/>
S	s	k7c7	s
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Následným	následný	k2eAgNnSc7d1	následné
pražením	pražení	k1gNnSc7	pražení
se	se	k3xPyFc4	se
z	z	k7c2	z
sulfidu	sulfid	k1gInSc2	sulfid
získá	získat	k5eAaPmIp3nS	získat
oxid	oxid	k1gInSc1	oxid
nikelnatý	nikelnatý	k2eAgInSc1d1	nikelnatý
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
oxidu	oxid	k1gInSc3	oxid
nikelnatému	nikelnatý	k2eAgInSc3d1	nikelnatý
se	se	k3xPyFc4	se
přidá	přidat	k5eAaPmIp3nS	přidat
dřevěné	dřevěný	k2eAgNnSc1d1	dřevěné
uhlí	uhlí	k1gNnSc1	uhlí
a	a	k8xC	a
směs	směs	k1gFnSc1	směs
se	se	k3xPyFc4	se
žíhá	žíhat	k5eAaImIp3nS	žíhat
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
získá	získat	k5eAaPmIp3nS	získat
práškový	práškový	k2eAgInSc1d1	práškový
nikl	nikl	k1gInSc1	nikl
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
k	k	k7c3	k
oxidu	oxid	k1gInSc2	oxid
nikelnatému	nikelnatý	k2eAgNnSc3d1	nikelnatý
a	a	k8xC	a
dřevěnému	dřevěný	k2eAgNnSc3d1	dřevěné
uhlí	uhlí	k1gNnSc3	uhlí
přidá	přidat	k5eAaPmIp3nS	přidat
ještě	ještě	k9	ještě
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
mouka	mouka	k1gFnSc1	mouka
(	(	kIx(	(
<g/>
jako	jako	k9	jako
pojidla	pojidlo	k1gNnSc2	pojidlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
se	se	k3xPyFc4	se
vytvarují	vytvarovat	k5eAaPmIp3nP	vytvarovat
krychle	krychle	k1gFnPc1	krychle
a	a	k8xC	a
při	při	k7c6	při
žíhání	žíhání	k1gNnSc6	žíhání
vzniká	vznikat	k5eAaImIp3nS	vznikat
nikl	nikl	k1gInSc1	nikl
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
krychlí	krychle	k1gFnPc2	krychle
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
niklu	nikl	k1gInSc2	nikl
z	z	k7c2	z
pyrrhotinu	pyrrhotina	k1gFnSc4	pyrrhotina
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
pražením	pražení	k1gNnSc7	pražení
snižuje	snižovat	k5eAaImIp3nS	snižovat
obsah	obsah	k1gInSc1	obsah
síry	síra	k1gFnSc2	síra
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
rudě	ruda	k1gFnSc6	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
vysokému	vysoký	k2eAgInSc3d1	vysoký
obsahu	obsah	k1gInSc3	obsah
mědi	měď	k1gFnSc2	měď
v	v	k7c6	v
rudě	ruda	k1gFnSc6	ruda
se	se	k3xPyFc4	se
získá	získat	k5eAaPmIp3nS	získat
směs	směs	k1gFnSc1	směs
sulfidu	sulfid	k1gInSc2	sulfid
niklu	nikl	k1gInSc2	nikl
a	a	k8xC	a
mědi	měď	k1gFnSc2	měď
<g/>
.	.	kIx.	.
</s>
<s>
Redukcí	redukce	k1gFnSc7	redukce
této	tento	k3xDgFnSc2	tento
směsi	směs	k1gFnSc2	směs
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
získat	získat	k5eAaPmF	získat
slitina	slitina	k1gFnSc1	slitina
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
niklu	nikl	k1gInSc2	nikl
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
slitina	slitina	k1gFnSc1	slitina
nemá	mít	k5eNaImIp3nS	mít
praktický	praktický	k2eAgInSc4d1	praktický
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
sulfid	sulfid	k1gInSc4	sulfid
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
niklu	nikl	k1gInSc2	nikl
od	od	k7c2	od
sebe	se	k3xPyFc2	se
oddělit	oddělit	k5eAaPmF	oddělit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
oxfordským	oxfordský	k2eAgInSc7d1	oxfordský
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Sulfidy	sulfid	k1gInPc1	sulfid
niklu	nikl	k1gInSc2	nikl
a	a	k8xC	a
mědi	měď	k1gFnSc2	měď
se	se	k3xPyFc4	se
taví	tavit	k5eAaImIp3nS	tavit
v	v	k7c6	v
šachtové	šachtový	k2eAgFnSc6d1	šachtová
peci	pec	k1gFnSc6	pec
s	s	k7c7	s
hydrogensíranem	hydrogensíran	k1gInSc7	hydrogensíran
sodným	sodný	k2eAgInSc7d1	sodný
a	a	k8xC	a
koksem	koks	k1gInSc7	koks
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tavení	tavení	k1gNnSc6	tavení
se	se	k3xPyFc4	se
sulfid	sulfid	k1gInSc1	sulfid
niklu	nikl	k1gInSc2	nikl
usazuje	usazovat	k5eAaImIp3nS	usazovat
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
sulfid	sulfid	k1gInSc1	sulfid
mědi	měď	k1gFnSc2	měď
se	se	k3xPyFc4	se
drží	držet	k5eAaImIp3nS	držet
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
taveniny	tavenina	k1gFnSc2	tavenina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vychladnutí	vychladnutí	k1gNnSc6	vychladnutí
se	se	k3xPyFc4	se
oddělí	oddělit	k5eAaPmIp3nS	oddělit
horní	horní	k2eAgFnSc1d1	horní
vrstva	vrstva	k1gFnSc1	vrstva
od	od	k7c2	od
spodní	spodní	k2eAgFnSc2d1	spodní
a	a	k8xC	a
odstraní	odstranit	k5eAaPmIp3nS	odstranit
se	se	k3xPyFc4	se
další	další	k2eAgFnPc1d1	další
nečistoty	nečistota	k1gFnPc1	nečistota
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pražení	pražení	k1gNnSc6	pražení
s	s	k7c7	s
koksem	koks	k1gInSc7	koks
se	se	k3xPyFc4	se
získá	získat	k5eAaPmIp3nS	získat
surový	surový	k2eAgInSc1d1	surový
nikl	nikl	k1gInSc1	nikl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
95	[number]	k4	95
%	%	kIx~	%
niklu	nikl	k1gInSc2	nikl
a	a	k8xC	a
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
%	%	kIx~	%
mědi	měď	k1gFnSc2	měď
<g/>
.	.	kIx.	.
</s>
<s>
Surový	surový	k2eAgInSc1d1	surový
nikl	nikl	k1gInSc1	nikl
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
elektrolyticky	elektrolyticky	k6eAd1	elektrolyticky
rafinuje	rafinovat	k5eAaImIp3nS	rafinovat
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
na	na	k7c4	na
čistý	čistý	k2eAgInSc4d1	čistý
nikl	nikl	k1gInSc4	nikl
karbonylovým	karbonylový	k2eAgInSc7d1	karbonylový
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Karbonylový	karbonylový	k2eAgInSc1d1	karbonylový
způsob	způsob	k1gInSc1	způsob
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
přípravě	příprava	k1gFnSc6	příprava
tetrakarbonylu	tetrakarbonyl	k1gInSc2	tetrakarbonyl
niklu	nikl	k1gInSc2	nikl
Ni	on	k3xPp3gFnSc4	on
<g/>
(	(	kIx(	(
<g/>
CO	co	k8xS	co
<g/>
)	)	kIx)	)
<g/>
4	[number]	k4	4
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc6	jeho
následném	následný	k2eAgInSc6d1	následný
rozkladu	rozklad	k1gInSc6	rozklad
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
výrobě	výroba	k1gFnSc6	výroba
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vycházet	vycházet	k5eAaImF	vycházet
ze	z	k7c2	z
surového	surový	k2eAgInSc2d1	surový
niklu	nikl	k1gInSc2	nikl
získaného	získaný	k2eAgInSc2d1	získaný
oxfordským	oxfordský	k2eAgInSc7d1	oxfordský
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
50	[number]	k4	50
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
působením	působení	k1gNnSc7	působení
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgInSc2d1	uhelnatý
za	za	k7c2	za
obyčejného	obyčejný	k2eAgInSc2d1	obyčejný
tlaku	tlak	k1gInSc2	tlak
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
Mondův	Mondův	k2eAgInSc1d1	Mondův
proces	proces	k1gInSc1	proces
<g/>
.	.	kIx.	.
</s>
<s>
Karbonyl	karbonyl	k1gInSc1	karbonyl
niklu	nikl	k1gInSc2	nikl
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
získat	získat	k5eAaPmF	získat
přímo	přímo	k6eAd1	přímo
ze	z	k7c2	z
sulfidu	sulfid	k1gInSc2	sulfid
niklu	nikl	k1gInSc2	nikl
působením	působení	k1gNnSc7	působení
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgInSc2d1	uhelnatý
při	při	k7c6	při
tlaku	tlak	k1gInSc6	tlak
200	[number]	k4	200
atmosfér	atmosféra	k1gFnPc2	atmosféra
a	a	k8xC	a
teplotě	teplota	k1gFnSc6	teplota
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
250	[number]	k4	250
°	°	k?	°
<g/>
C.	C.	kA	C.
Rozklad	rozklad	k1gInSc1	rozklad
karbonylu	karbonyl	k1gInSc2	karbonyl
probíhá	probíhat	k5eAaImIp3nS	probíhat
za	za	k7c4	za
teploty	teplota	k1gFnPc4	teplota
200	[number]	k4	200
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
normálním	normální	k2eAgInSc6d1	normální
tlaku	tlak	k1gInSc6	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
získá	získat	k5eAaPmIp3nS	získat
velmi	velmi	k6eAd1	velmi
čistý	čistý	k2eAgInSc1d1	čistý
nikl	nikl	k1gInSc1	nikl
99,95	[number]	k4	99,95
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přečišťování	přečišťování	k1gNnSc3	přečišťování
niklu	nikl	k1gInSc2	nikl
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
elektrolytická	elektrolytický	k2eAgFnSc1d1	elektrolytická
rafinace	rafinace	k1gFnSc1	rafinace
<g/>
.	.	kIx.	.
</s>
<s>
Hlavně	hlavně	k9	hlavně
u	u	k7c2	u
surového	surový	k2eAgInSc2d1	surový
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
platinu	platina	k1gFnSc4	platina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
z	z	k7c2	z
anodového	anodový	k2eAgInSc2d1	anodový
kalu	kal	k1gInSc2	kal
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
přitom	přitom	k6eAd1	přitom
odpadá	odpadat	k5eAaImIp3nS	odpadat
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
platina	platina	k1gFnSc1	platina
a	a	k8xC	a
kovy	kov	k1gInPc1	kov
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ji	on	k3xPp3gFnSc4	on
doprovází	doprovázet	k5eAaImIp3nP	doprovázet
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
získány	získán	k2eAgInPc1d1	získán
<g/>
.	.	kIx.	.
</s>
<s>
Nikl	nikl	k1gInSc1	nikl
získaný	získaný	k2eAgInSc1d1	získaný
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
99,99	[number]	k4	99,99
%	%	kIx~	%
čistý	čistý	k2eAgInSc1d1	čistý
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
poměrně	poměrně	k6eAd1	poměrně
velmi	velmi	k6eAd1	velmi
dobré	dobrý	k2eAgFnSc3d1	dobrá
stálosti	stálost	k1gFnSc3	stálost
kovového	kovový	k2eAgInSc2d1	kovový
niklu	nikl	k1gInSc2	nikl
vůči	vůči	k7c3	vůči
atmosférickým	atmosférický	k2eAgInPc3d1	atmosférický
vlivům	vliv	k1gInPc3	vliv
i	i	k8xC	i
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
často	často	k6eAd1	často
nanáší	nanášet	k5eAaImIp3nS	nanášet
velmi	velmi	k6eAd1	velmi
tenká	tenký	k2eAgFnSc1d1	tenká
niklová	niklový	k2eAgFnSc1d1	niklová
vrstva	vrstva	k1gFnSc1	vrstva
na	na	k7c4	na
povrchy	povrch	k1gInPc4	povrch
méně	málo	k6eAd2	málo
odolných	odolný	k2eAgInPc2d1	odolný
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Nanášení	nanášení	k1gNnSc1	nanášení
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
elektrolyticky	elektrolyticky	k6eAd1	elektrolyticky
obvykle	obvykle	k6eAd1	obvykle
z	z	k7c2	z
alkalického	alkalický	k2eAgNnSc2d1	alkalické
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
nikl	nikl	k1gInSc1	nikl
přítomen	přítomen	k2eAgInSc1d1	přítomen
jako	jako	k8xC	jako
kyanidový	kyanidový	k2eAgInSc1d1	kyanidový
komplex	komplex	k1gInSc1	komplex
a	a	k8xC	a
na	na	k7c4	na
pokovovaný	pokovovaný	k2eAgInSc4d1	pokovovaný
předmět	předmět	k1gInSc4	předmět
je	být	k5eAaImIp3nS	být
vložen	vložit	k5eAaPmNgInS	vložit
záporný	záporný	k2eAgInSc1d1	záporný
elektrický	elektrický	k2eAgInSc1d1	elektrický
potenciál	potenciál	k1gInSc1	potenciál
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
tedy	tedy	k9	tedy
jako	jako	k9	jako
katoda	katoda	k1gFnSc1	katoda
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
takto	takto	k6eAd1	takto
upravují	upravovat	k5eAaImIp3nP	upravovat
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
pracovní	pracovní	k2eAgInPc4d1	pracovní
nástroje	nástroj	k1gInPc4	nástroj
jako	jako	k8xS	jako
šroubováky	šroubovák	k1gInPc4	šroubovák
nebo	nebo	k8xC	nebo
klíče	klíč	k1gInPc4	klíč
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
některé	některý	k3yIgInPc1	některý
chirurgické	chirurgický	k2eAgInPc1d1	chirurgický
nástroje	nástroj	k1gInPc1	nástroj
a	a	k8xC	a
pomůcky	pomůcka	k1gFnPc1	pomůcka
se	se	k3xPyFc4	se
niklují	niklovat	k5eAaImIp3nP	niklovat
<g/>
.	.	kIx.	.
</s>
<s>
Značné	značný	k2eAgFnPc1d1	značná
odolnosti	odolnost	k1gFnPc1	odolnost
kovového	kovový	k2eAgInSc2d1	kovový
niklu	nikl	k1gInSc2	nikl
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
chemického	chemický	k2eAgNnSc2d1	chemické
nádobí	nádobí	k1gNnSc2	nádobí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
vystavit	vystavit	k5eAaPmF	vystavit
účinkům	účinek	k1gInPc3	účinek
alkalických	alkalický	k2eAgFnPc2d1	alkalická
tavenin	tavenina	k1gFnPc2	tavenina
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
hydroxid	hydroxid	k1gInSc1	hydroxid
sodný	sodný	k2eAgInSc1d1	sodný
nebo	nebo	k8xC	nebo
uhličitan	uhličitan	k1gInSc1	uhličitan
draselný	draselný	k2eAgInSc1d1	draselný
bez	bez	k7c2	bez
výraznějšího	výrazný	k2eAgNnSc2d2	výraznější
poškození	poškození	k1gNnSc2	poškození
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kyselém	kyselý	k2eAgNnSc6d1	kyselé
prostředí	prostředí	k1gNnSc6	prostředí
je	být	k5eAaImIp3nS	být
však	však	k9	však
nutno	nutno	k6eAd1	nutno
použít	použít	k5eAaPmF	použít
mnohem	mnohem	k6eAd1	mnohem
dražších	drahý	k2eAgInPc2d2	dražší
kelímků	kelímek	k1gInPc2	kelímek
z	z	k7c2	z
platiny	platina	k1gFnSc2	platina
nebo	nebo	k8xC	nebo
slitin	slitina	k1gFnPc2	slitina
platiny	platina	k1gFnSc2	platina
s	s	k7c7	s
rhodiem	rhodium	k1gNnSc7	rhodium
nebo	nebo	k8xC	nebo
iridiem	iridium	k1gNnSc7	iridium
<g/>
.	.	kIx.	.
</s>
<s>
Ocelářský	ocelářský	k2eAgInSc1d1	ocelářský
průmysl	průmysl	k1gInSc1	průmysl
je	být	k5eAaImIp3nS	být
rozhodně	rozhodně	k6eAd1	rozhodně
největším	veliký	k2eAgMnSc7d3	veliký
světovým	světový	k2eAgMnSc7d1	světový
spotřebitelem	spotřebitel	k1gMnSc7	spotřebitel
niklu	nikl	k1gInSc2	nikl
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	s	k7c7	s
železem	železo	k1gNnSc7	železo
<g/>
,	,	kIx,	,
chromem	chrom	k1gInSc7	chrom
a	a	k8xC	a
manganem	mangan	k1gInSc7	mangan
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
základní	základní	k2eAgInPc4d1	základní
kovy	kov	k1gInPc4	kov
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
pro	pro	k7c4	pro
legování	legování	k1gNnSc4	legování
ocelí	ocel	k1gFnPc2	ocel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
mít	mít	k5eAaImF	mít
na	na	k7c6	na
zřeteli	zřetel	k1gInSc6	zřetel
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
tisíce	tisíc	k4xCgInPc4	tisíc
typů	typ	k1gInPc2	typ
ocelí	ocel	k1gFnPc2	ocel
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
liší	lišit	k5eAaImIp3nP	lišit
svým	svůj	k3xOyFgNnSc7	svůj
složením	složení	k1gNnSc7	složení
<g/>
,	,	kIx,	,
způsobem	způsob	k1gInSc7	způsob
zpracování	zpracování	k1gNnSc2	zpracování
a	a	k8xC	a
následně	následně	k6eAd1	následně
pak	pak	k6eAd1	pak
svými	svůj	k3xOyFgFnPc7	svůj
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tvrdost	tvrdost	k1gFnSc1	tvrdost
<g/>
,	,	kIx,	,
pevnost	pevnost	k1gFnSc1	pevnost
<g/>
,	,	kIx,	,
kujnost	kujnost	k1gFnSc1	kujnost
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
odolnost	odolnost	k1gFnSc1	odolnost
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řadě	řada	k1gFnSc6	řada
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
výše	výše	k1gFnSc2	výše
uvedených	uvedený	k2eAgInPc2d1	uvedený
prvků	prvek	k1gInPc2	prvek
přítomno	přítomen	k2eAgNnSc1d1	přítomno
i	i	k8xC	i
menší	malý	k2eAgNnSc1d2	menší
množství	množství	k1gNnSc1	množství
dalších	další	k2eAgInPc2d1	další
kovů	kov	k1gInPc2	kov
(	(	kIx(	(
<g/>
molybden	molybden	k1gInSc1	molybden
<g/>
,	,	kIx,	,
wolfram	wolfram	k1gInSc1	wolfram
<g/>
,	,	kIx,	,
kobalt	kobalt	k1gInSc1	kobalt
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nikl	Nikl	k1gMnSc1	Nikl
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
velmi	velmi	k6eAd1	velmi
odolných	odolný	k2eAgFnPc2d1	odolná
slitin	slitina	k1gFnPc2	slitina
jako	jako	k8xS	jako
například	například	k6eAd1	například
Monelův	Monelův	k2eAgInSc1d1	Monelův
kov	kov	k1gInSc1	kov
o	o	k7c6	o
složení	složení	k1gNnSc6	složení
68	[number]	k4	68
%	%	kIx~	%
Ni	on	k3xPp3gFnSc4	on
a	a	k8xC	a
32	[number]	k4	32
%	%	kIx~	%
Cu	Cu	k1gFnSc2	Cu
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
také	také	k9	také
uvádí	uvádět	k5eAaImIp3nS	uvádět
alternativní	alternativní	k2eAgInSc4d1	alternativní
poměr	poměr	k1gInSc4	poměr
67	[number]	k4	67
<g/>
:	:	kIx,	:
<g/>
28	[number]	k4	28
nebo	nebo	k8xC	nebo
65	[number]	k4	65
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stopami	stopa	k1gFnPc7	stopa
manganu	mangan	k1gInSc2	mangan
a	a	k8xC	a
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
lodních	lodní	k2eAgInPc2d1	lodní
šroubů	šroub	k1gInPc2	šroub
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
kuchyňského	kuchyňský	k2eAgNnSc2d1	kuchyňské
vybavení	vybavení	k1gNnSc2	vybavení
<g/>
.	.	kIx.	.
</s>
<s>
Slitiny	slitina	k1gFnPc1	slitina
Alnico	Alnico	k6eAd1	Alnico
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
kobaltu	kobalt	k1gInSc2	kobalt
<g/>
,	,	kIx,	,
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
hliníku	hliník	k1gInSc2	hliník
a	a	k8xC	a
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
velmi	velmi	k6eAd1	velmi
silných	silný	k2eAgInPc2d1	silný
permanentních	permanentní	k2eAgInPc2d1	permanentní
magnetů	magnet	k1gInPc2	magnet
<g/>
.	.	kIx.	.
</s>
<s>
Nikl	Nikl	k1gMnSc1	Nikl
patří	patřit	k5eAaImIp3nS	patřit
již	již	k6eAd1	již
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
mezi	mezi	k7c4	mezi
mincovní	mincovní	k2eAgInPc4d1	mincovní
kovy	kov	k1gInPc4	kov
<g/>
,	,	kIx,	,
používané	používaný	k2eAgFnSc2d1	používaná
k	k	k7c3	k
ražení	ražení	k1gNnSc3	ražení
mincí	mince	k1gFnPc2	mince
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
slitinách	slitina	k1gFnPc6	slitina
s	s	k7c7	s
mědí	měď	k1gFnSc7	měď
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
se	se	k3xPyFc4	se
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
slitin	slitina	k1gFnPc2	slitina
razily	razit	k5eAaImAgFnP	razit
především	především	k9	především
mince	mince	k1gFnPc1	mince
o	o	k7c6	o
nominální	nominální	k2eAgFnSc6d1	nominální
hodnotě	hodnota	k1gFnSc6	hodnota
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
a	a	k8xC	a
5	[number]	k4	5
Kčs	Kčs	kA	Kčs
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
ČR	ČR	kA	ČR
jsou	být	k5eAaImIp3nP	být
mince	mince	k1gFnSc2	mince
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
a	a	k8xC	a
5	[number]	k4	5
Kč	Kč	kA	Kč
pouze	pouze	k6eAd1	pouze
niklem	nikl	k1gInSc7	nikl
povrchově	povrchově	k6eAd1	povrchově
upravené	upravený	k2eAgInPc1d1	upravený
(	(	kIx(	(
<g/>
ražené	ražený	k2eAgInPc1d1	ražený
z	z	k7c2	z
oceli	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
povrchová	povrchový	k2eAgFnSc1d1	povrchová
vrstva	vrstva	k1gFnSc1	vrstva
niklová	niklový	k2eAgFnSc1d1	niklová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
Kanadě	Kanada	k1gFnSc6	Kanada
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
minci	mince	k1gFnSc4	mince
o	o	k7c6	o
hodnotě	hodnota	k1gFnSc6	hodnota
5	[number]	k4	5
centů	cent	k1gInPc2	cent
používá	používat	k5eAaImIp3nS	používat
označení	označení	k1gNnSc1	označení
nickel	nickela	k1gFnPc2	nickela
<g/>
,	,	kIx,	,
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
překládané	překládaný	k2eAgFnSc2d1	překládaná
jako	jako	k8xC	jako
niklák	niklák	k?	niklák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
týká	týkat	k5eAaImIp3nS	týkat
mincí	mince	k1gFnSc7	mince
s	s	k7c7	s
nominální	nominální	k2eAgFnSc7d1	nominální
hodnotou	hodnota	k1gFnSc7	hodnota
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
eura	euro	k1gNnSc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
mince	mince	k1gFnPc4	mince
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
ze	z	k7c2	z
slitiny	slitina	k1gFnSc2	slitina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
nové	nový	k2eAgNnSc4d1	nové
stříbro	stříbro	k1gNnSc4	stříbro
neboli	neboli	k8xC	neboli
argentan	argentan	k1gInSc4	argentan
či	či	k8xC	či
nejčastěji	často	k6eAd3	často
alpaka	alpaka	k1gFnSc1	alpaka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
slitina	slitina	k1gFnSc1	slitina
se	se	k3xPyFc4	se
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
dostala	dostat	k5eAaPmAgFnS	dostat
z	z	k7c2	z
Činy	čina	k1gFnSc2	čina
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mince	mince	k1gFnSc1	mince
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
začaly	začít	k5eAaPmAgFnP	začít
razit	razit	k5eAaImF	razit
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
až	až	k8xS	až
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Alpaka	alpaka	k1gFnSc1	alpaka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
%	%	kIx~	%
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
40	[number]	k4	40
<g/>
-	-	kIx~	-
<g/>
70	[number]	k4	70
%	%	kIx~	%
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
40	[number]	k4	40
%	%	kIx~	%
zinku	zinek	k1gInSc2	zinek
<g/>
.	.	kIx.	.
</s>
<s>
Slitina	slitina	k1gFnSc1	slitina
je	být	k5eAaImIp3nS	být
stříbrobílá	stříbrobílý	k2eAgFnSc1d1	stříbrobílá
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
odolná	odolný	k2eAgFnSc1d1	odolná
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
dobře	dobře	k6eAd1	dobře
leštit	leštit	k5eAaImF	leštit
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
místo	místo	k1gNnSc1	místo
patří	patřit	k5eAaImIp3nS	patřit
slitinám	slitina	k1gFnPc3	slitina
niklu	nikl	k1gInSc2	nikl
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
šperků	šperk	k1gInPc2	šperk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
poměrně	poměrně	k6eAd1	poměrně
populární	populární	k2eAgNnSc1d1	populární
bílé	bílý	k2eAgNnSc1d1	bílé
zlato	zlato	k1gNnSc1	zlato
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
právě	právě	k9	právě
slitinou	slitina	k1gFnSc7	slitina
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
zinku	zinek	k1gInSc2	zinek
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
těchto	tento	k3xDgInPc2	tento
materiálů	materiál	k1gInPc2	materiál
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
řada	řada	k1gFnSc1	řada
lidí	člověk	k1gMnPc2	člověk
trpí	trpět	k5eAaImIp3nS	trpět
alergií	alergie	k1gFnSc7	alergie
na	na	k7c4	na
slitiny	slitina	k1gFnPc4	slitina
niklu	nikl	k1gInSc2	nikl
a	a	k8xC	a
nemůže	moct	k5eNaImIp3nS	moct
šperky	šperk	k1gInPc4	šperk
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
slitin	slitina	k1gFnPc2	slitina
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
nosit	nosit	k5eAaImF	nosit
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
slitina	slitina	k1gFnSc1	slitina
niklu	nikl	k1gInSc2	nikl
a	a	k8xC	a
stříbra	stříbro	k1gNnSc2	stříbro
slouží	sloužit	k5eAaImIp3nS	sloužit
často	často	k6eAd1	často
jako	jako	k9	jako
materiál	materiál	k1gInSc4	materiál
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
elektrických	elektrický	k2eAgInPc2d1	elektrický
kontaktů	kontakt	k1gInPc2	kontakt
v	v	k7c6	v
silně	silně	k6eAd1	silně
namáhaných	namáhaný	k2eAgInPc6d1	namáhaný
silnoproudých	silnoproudý	k2eAgInPc6d1	silnoproudý
spínačích	spínač	k1gInPc6	spínač
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
musí	muset	k5eAaImIp3nP	muset
vykazovat	vykazovat	k5eAaImF	vykazovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
úroveň	úroveň	k1gFnSc4	úroveň
spolehlivosti	spolehlivost	k1gFnSc2	spolehlivost
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
směs	směs	k1gFnSc4	směs
o	o	k7c6	o
složení	složení	k1gNnSc6	složení
přibližně	přibližně	k6eAd1	přibližně
90	[number]	k4	90
%	%	kIx~	%
Ag	Ag	k1gFnSc2	Ag
+	+	kIx~	+
10	[number]	k4	10
%	%	kIx~	%
Ni	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
oba	dva	k4xCgInPc1	dva
kovy	kov	k1gInPc1	kov
se	se	k3xPyFc4	se
při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
poměru	poměr	k1gInSc6	poměr
v	v	k7c6	v
tavenině	tavenina	k1gFnSc6	tavenina
nemísí	mísit	k5eNaImIp3nS	mísit
<g/>
,	,	kIx,	,
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
slitina	slitina	k1gFnSc1	slitina
poměrně	poměrně	k6eAd1	poměrně
komplikovaným	komplikovaný	k2eAgNnSc7d1	komplikované
spékáním	spékání	k1gNnSc7	spékání
práškového	práškový	k2eAgInSc2d1	práškový
materiálu	materiál	k1gInSc2	materiál
za	za	k7c2	za
vysokých	vysoký	k2eAgFnPc2d1	vysoká
teplot	teplota	k1gFnPc2	teplota
a	a	k8xC	a
tlaků	tlak	k1gInPc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Výslednému	výsledný	k2eAgInSc3d1	výsledný
materiálu	materiál	k1gInSc3	materiál
potom	potom	k6eAd1	potom
stříbro	stříbro	k1gNnSc1	stříbro
dodává	dodávat	k5eAaImIp3nS	dodávat
vynikající	vynikající	k2eAgFnSc4d1	vynikající
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
vodivost	vodivost	k1gFnSc4	vodivost
a	a	k8xC	a
nikl	niknout	k5eAaImAgMnS	niknout
zase	zase	k9	zase
výhodné	výhodný	k2eAgFnPc4d1	výhodná
mechanické	mechanický	k2eAgFnPc4d1	mechanická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
-	-	kIx~	-
tvrdost	tvrdost	k1gFnSc1	tvrdost
a	a	k8xC	a
odolnost	odolnost	k1gFnSc1	odolnost
proti	proti	k7c3	proti
otěru	otěr	k1gInSc3	otěr
<g/>
.	.	kIx.	.
</s>
<s>
Nikl	Nikl	k1gMnSc1	Nikl
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
také	také	k9	také
ve	v	k7c6	v
slitinách	slitina	k1gFnPc6	slitina
s	s	k7c7	s
tvarovou	tvarový	k2eAgFnSc7d1	tvarová
pamětí	paměť	k1gFnSc7	paměť
jako	jako	k8xC	jako
slitina	slitina	k1gFnSc1	slitina
NiTi	nit	k1gFnSc2	nit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
významným	významný	k2eAgFnPc3d1	významná
slitinám	slitina	k1gFnPc3	slitina
niklu	nikl	k1gInSc2	nikl
patří	patřit	k5eAaImIp3nP	patřit
konstantan	konstantan	k1gInSc4	konstantan
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
slitina	slitina	k1gFnSc1	slitina
40	[number]	k4	40
%	%	kIx~	%
niklu	nikl	k1gInSc2	nikl
a	a	k8xC	a
60	[number]	k4	60
%	%	kIx~	%
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
konstantní	konstantní	k2eAgInSc4d1	konstantní
velký	velký	k2eAgInSc4d1	velký
elektrický	elektrický	k2eAgInSc4d1	elektrický
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Nikelin	nikelin	k1gInSc1	nikelin
je	být	k5eAaImIp3nS	být
slitina	slitina	k1gFnSc1	slitina
31	[number]	k4	31
%	%	kIx~	%
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
56	[number]	k4	56
%	%	kIx~	%
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
13	[number]	k4	13
%	%	kIx~	%
zinku	zinek	k1gInSc2	zinek
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
velký	velký	k2eAgInSc4d1	velký
konstantní	konstantní	k2eAgInSc4d1	konstantní
elektrický	elektrický	k2eAgInSc4d1	elektrický
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Manganin	manganin	k1gInSc1	manganin
je	být	k5eAaImIp3nS	být
slitina	slitina	k1gFnSc1	slitina
4	[number]	k4	4
%	%	kIx~	%
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
12	[number]	k4	12
%	%	kIx~	%
manganu	mangan	k1gInSc2	mangan
a	a	k8xC	a
84	[number]	k4	84
%	%	kIx~	%
chromu	chrom	k1gInSc2	chrom
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
na	na	k7c4	na
zhotovování	zhotovování	k1gNnSc4	zhotovování
přesných	přesný	k2eAgInPc2d1	přesný
elektrických	elektrický	k2eAgInPc2d1	elektrický
odporů	odpor	k1gInPc2	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Chromnikl	chromnikl	k1gInSc1	chromnikl
neboli	neboli	k8xC	neboli
nichrom	nichrom	k1gInSc1	nichrom
je	být	k5eAaImIp3nS	být
slitina	slitina	k1gFnSc1	slitina
60	[number]	k4	60
%	%	kIx~	%
niklu	nikl	k1gInSc2	nikl
a	a	k8xC	a
40	[number]	k4	40
%	%	kIx~	%
chromu	chrom	k1gInSc2	chrom
a	a	k8xC	a
využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
na	na	k7c4	na
vinutí	vinutí	k1gNnSc4	vinutí
elektrických	elektrický	k2eAgFnPc2d1	elektrická
pecí	pec	k1gFnPc2	pec
<g/>
.	.	kIx.	.
</s>
<s>
Značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
celosvětově	celosvětově	k6eAd1	celosvětově
vyrobeného	vyrobený	k2eAgInSc2d1	vyrobený
niklu	nikl	k1gInSc2	nikl
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jako	jako	k8xS	jako
surovina	surovina	k1gFnSc1	surovina
pro	pro	k7c4	pro
elektrické	elektrický	k2eAgInPc4d1	elektrický
články	článek	k1gInPc4	článek
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
mnohonásobného	mnohonásobný	k2eAgNnSc2d1	mnohonásobné
dobíjení	dobíjení	k1gNnSc2	dobíjení
<g/>
.	.	kIx.	.
</s>
<s>
Nikl-hydridové	Niklydridový	k2eAgFnPc1d1	Nikl-hydridový
baterie	baterie	k1gFnPc1	baterie
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
zdroj	zdroj	k1gInSc4	zdroj
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
<g/>
,	,	kIx,	,
přenosných	přenosný	k2eAgFnPc2d1	přenosná
svítilen	svítilna	k1gFnPc2	svítilna
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zdroje	zdroj	k1gInPc4	zdroj
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
elektrickou	elektrický	k2eAgFnSc7d1	elektrická
kapacitou	kapacita	k1gFnSc7	kapacita
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
spíše	spíše	k9	spíše
nikl	nikl	k1gInSc4	nikl
-	-	kIx~	-
kadmiové	kadmiový	k2eAgInPc4d1	kadmiový
galvanické	galvanický	k2eAgInPc4d1	galvanický
elektrické	elektrický	k2eAgInPc4d1	elektrický
články	článek	k1gInPc4	článek
typu	typ	k1gInSc2	typ
NiCd	NiCdo	k1gNnPc2	NiCdo
<g/>
.	.	kIx.	.
</s>
<s>
Vykazují	vykazovat	k5eAaImIp3nP	vykazovat
velmi	velmi	k6eAd1	velmi
dobré	dobrý	k2eAgFnPc1d1	dobrá
elektrické	elektrický	k2eAgFnPc1d1	elektrická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
(	(	kIx(	(
<g/>
kapacita	kapacita	k1gFnSc1	kapacita
x	x	k?	x
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
)	)	kIx)	)
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
i	i	k9	i
zpětně	zpětně	k6eAd1	zpětně
dobíjet	dobíjet	k5eAaImF	dobíjet
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
často	často	k6eAd1	často
jako	jako	k8xS	jako
zdroj	zdroj	k1gInSc4	zdroj
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
v	v	k7c6	v
automobilech	automobil	k1gInPc6	automobil
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
dopravních	dopravní	k2eAgInPc6d1	dopravní
prostředcích	prostředek	k1gInPc6	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
klasických	klasický	k2eAgInPc2d1	klasický
olověných	olověný	k2eAgInPc2d1	olověný
akumulátorů	akumulátor	k1gInPc2	akumulátor
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
jako	jako	k9	jako
elektrolyt	elektrolyt	k1gInSc1	elektrolyt
používá	používat	k5eAaImIp3nS	používat
roztok	roztok	k1gInSc4	roztok
alkalického	alkalický	k2eAgInSc2d1	alkalický
hydroxidu	hydroxid	k1gInSc2	hydroxid
<g/>
.	.	kIx.	.
</s>
<s>
Reakci	reakce	k1gFnSc4	reakce
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k8xC	jako
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
N	N	kA	N
i	i	k8xC	i
O	O	kA	O
(	(	kIx(	(
O	o	k7c6	o
H	H	kA	H
)	)	kIx)	)
+	+	kIx~	+
C	C	kA	C
d	d	k?	d
+	+	kIx~	+
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
:	:	kIx,	:
⇌	⇌	k?	⇌
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
N	N	kA	N
i	i	k8xC	i
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
O	o	k7c6	o
H	H	kA	H
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
C	C	kA	C
d	d	k?	d
(	(	kIx(	(
O	o	k7c6	o
H	H	kA	H
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gInSc1	mathsf
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
NiO	NiO	k1gMnSc1	NiO
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
+	+	kIx~	+
<g/>
Cd	cd	kA	cd
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O	o	k7c4	o
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
rightleftharpoons	rightleftharpoons	k1gInSc1	rightleftharpoons
\	\	kIx~	\
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
Ni	on	k3xPp3gFnSc4	on
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
Cd	cd	kA	cd
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
Alkalický	alkalický	k2eAgInSc1d1	alkalický
hydroxid	hydroxid	k1gInSc1	hydroxid
nikelnatý	nikelnatý	k2eAgInSc1d1	nikelnatý
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
kadmiem	kadmium	k1gNnSc7	kadmium
a	a	k8xC	a
vodou	voda	k1gFnSc7	voda
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
hydroxidu	hydroxid	k1gInSc2	hydroxid
nikelnatého	nikelnatý	k2eAgInSc2d1	nikelnatý
a	a	k8xC	a
hydroxidu	hydroxid	k1gInSc2	hydroxid
kademnatého	kademnatý	k2eAgInSc2d1	kademnatý
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
prokázané	prokázaný	k2eAgFnSc3d1	prokázaná
toxicitě	toxicita	k1gFnSc3	toxicita
kadmia	kadmium	k1gNnSc2	kadmium
se	se	k3xPyFc4	se
však	však	k9	však
výroba	výroba	k1gFnSc1	výroba
těchto	tento	k3xDgFnPc2	tento
baterií	baterie	k1gFnPc2	baterie
postupně	postupně	k6eAd1	postupně
omezuje	omezovat	k5eAaImIp3nS	omezovat
<g/>
.	.	kIx.	.
</s>
<s>
Jemně	jemně	k6eAd1	jemně
rozptýlený	rozptýlený	k2eAgInSc1d1	rozptýlený
elementární	elementární	k2eAgInSc1d1	elementární
nikl	nikl	k1gInSc1	nikl
–	–	k?	–
Raneyův	Raneyův	k2eAgInSc1d1	Raneyův
nikl	nikl	k1gInSc1	nikl
–	–	k?	–
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
účinným	účinný	k2eAgInSc7d1	účinný
hydrogenačním	hydrogenační	k2eAgInSc7d1	hydrogenační
katalyzátorem	katalyzátor	k1gInSc7	katalyzátor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
působí	působit	k5eAaImIp3nS	působit
reakci	reakce	k1gFnSc4	reakce
dvojné	dvojný	k2eAgFnSc2d1	dvojná
vazby	vazba	k1gFnSc2	vazba
mezi	mezi	k7c7	mezi
uhlíkovými	uhlíkový	k2eAgInPc7d1	uhlíkový
atomy	atom	k1gInPc7	atom
s	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
vazby	vazba	k1gFnSc2	vazba
jednoduché	jednoduchý	k2eAgFnSc2d1	jednoduchá
<g/>
.	.	kIx.	.
</s>
<s>
Schematicky	schematicky	k6eAd1	schematicky
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gMnSc1	mathsf
{	{	kIx(	{
<g/>
R_	R_	k1gMnSc1	R_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
C	C	kA	C
<g/>
{	{	kIx(	{
<g/>
=	=	kIx~	=
<g/>
}	}	kIx)	}
<g/>
CR_	CR_	k1gMnSc1	CR_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
\	\	kIx~	\
HR_	HR_	k1gMnSc1	HR_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
C	C	kA	C
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}	}	kIx)	}
<g/>
CR_	CR_	k1gMnSc1	CR_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
H	H	kA	H
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Této	tento	k3xDgFnSc2	tento
reakce	reakce	k1gFnSc2	reakce
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
v	v	k7c4	v
potravinářství	potravinářství	k1gNnSc4	potravinářství
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
ztužených	ztužený	k2eAgInPc2d1	ztužený
tuků	tuk	k1gInPc2	tuk
z	z	k7c2	z
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
olejů	olej	k1gInPc2	olej
<g/>
.	.	kIx.	.
</s>
<s>
Běžné	běžný	k2eAgInPc1d1	běžný
rostlinné	rostlinný	k2eAgInPc1d1	rostlinný
oleje	olej	k1gInPc1	olej
jsou	být	k5eAaImIp3nP	být
chemicky	chemicky	k6eAd1	chemicky
estery	ester	k1gInPc1	ester
nenasycených	nasycený	k2eNgFnPc2d1	nenasycená
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
s	s	k7c7	s
několika	několik	k4yIc2	několik
dvojnými	dvojný	k2eAgFnPc7d1	dvojná
vazbami	vazba	k1gFnPc7	vazba
v	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Převedením	převedení	k1gNnSc7	převedení
části	část	k1gFnSc2	část
těchto	tento	k3xDgFnPc2	tento
dvojných	dvojný	k2eAgFnPc2d1	dvojná
vazeb	vazba	k1gFnPc2	vazba
na	na	k7c4	na
vazby	vazba	k1gFnPc4	vazba
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
vzniká	vznikat	k5eAaImIp3nS	vznikat
rostlinný	rostlinný	k2eAgInSc1d1	rostlinný
tuk	tuk	k1gInSc1	tuk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
normální	normální	k2eAgFnPc4d1	normální
teploty	teplota	k1gFnPc4	teplota
tuhou	tuhý	k2eAgFnSc4d1	tuhá
konzistenci	konzistence	k1gFnSc4	konzistence
<g/>
.	.	kIx.	.
</s>
<s>
Nikl	nikl	k1gInSc4	nikl
tvoří	tvořit	k5eAaImIp3nP	tvořit
sloučeniny	sloučenina	k1gFnPc1	sloučenina
v	v	k7c6	v
oxidačních	oxidační	k2eAgInPc6d1	oxidační
stavech	stav	k1gInPc6	stav
od	od	k7c2	od
Ni-	Ni-	k1gFnSc2	Ni-
<g/>
1	[number]	k4	1
do	do	k7c2	do
Ni	on	k3xPp3gFnSc4	on
<g/>
+	+	kIx~	+
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
záporných	záporný	k2eAgInPc6d1	záporný
stavech	stav	k1gInPc6	stav
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
organokovové	organokovový	k2eAgFnPc4d1	organokovová
sloučeniny	sloučenina	k1gFnPc4	sloučenina
a	a	k8xC	a
v	v	k7c6	v
kladných	kladný	k2eAgInPc6d1	kladný
je	být	k5eAaImIp3nS	být
nejstabilnější	stabilní	k2eAgFnSc1d3	nejstabilnější
Ni	on	k3xPp3gFnSc4	on
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
a	a	k8xC	a
vyšší	vysoký	k2eAgInPc1d2	vyšší
stavy	stav	k1gInPc1	stav
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
i	i	k8xC	i
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
stabilních	stabilní	k2eAgFnPc6d1	stabilní
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
nikl	nikl	k1gInSc1	nikl
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
převážně	převážně	k6eAd1	převážně
jako	jako	k8xS	jako
kladně	kladně	k6eAd1	kladně
dvojmocný	dvojmocný	k2eAgInSc1d1	dvojmocný
Ni	on	k3xPp3gFnSc4	on
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Nikelnaté	nikelnatý	k2eAgFnPc1d1	nikelnatý
soli	sůl	k1gFnPc1	sůl
běžných	běžný	k2eAgFnPc2d1	běžná
anorganických	anorganický	k2eAgFnPc2d1	anorganická
kyselin	kyselina	k1gFnPc2	kyselina
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
hydratované	hydratovaný	k2eAgFnSc6d1	hydratovaná
podobě	podoba	k1gFnSc6	podoba
zelené	zelený	k2eAgFnSc2d1	zelená
krystalické	krystalický	k2eAgFnSc2d1	krystalická
látky	látka	k1gFnSc2	látka
dobře	dobře	k6eAd1	dobře
rozpustné	rozpustný	k2eAgNnSc1d1	rozpustné
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
jinak	jinak	k6eAd1	jinak
zbarveny	zbarven	k2eAgFnPc1d1	zbarvena
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
špatně	špatně	k6eAd1	špatně
rozpustný	rozpustný	k2eAgInSc4d1	rozpustný
uhličitan	uhličitan	k1gInSc4	uhličitan
nikelnatý	nikelnatý	k2eAgInSc4d1	nikelnatý
NiCO	NiCO	k1gFnSc7	NiCO
<g/>
3	[number]	k4	3
a	a	k8xC	a
černý	černý	k2eAgInSc1d1	černý
silně	silně	k6eAd1	silně
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
sulfid	sulfid	k1gInSc1	sulfid
nikelnatý	nikelnatý	k2eAgInSc1d1	nikelnatý
NiS	Nisa	k1gFnPc2	Nisa
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgInPc1d2	vyšší
oxidační	oxidační	k2eAgInPc1d1	oxidační
stavy	stav	k1gInPc1	stav
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
sloučeniny	sloučenina	k1gFnPc1	sloučenina
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
i	i	k8xC	i
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
<g/>
.	.	kIx.	.
</s>
<s>
Stabilní	stabilní	k2eAgInPc1d1	stabilní
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
inertních	inertní	k2eAgFnPc6d1	inertní
atmosférách	atmosféra	k1gFnPc6	atmosféra
a	a	k8xC	a
proto	proto	k8xC	proto
nemají	mít	k5eNaImIp3nP	mít
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
nikelnatý	nikelnatý	k2eAgInSc1d1	nikelnatý
NiO	NiO	k1gFnSc4	NiO
je	být	k5eAaImIp3nS	být
zelený	zelený	k2eAgInSc1d1	zelený
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
hydroxidech	hydroxid	k1gInPc6	hydroxid
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
snadno	snadno	k6eAd1	snadno
rozpustný	rozpustný	k2eAgInSc1d1	rozpustný
v	v	k7c6	v
kyselinách	kyselina	k1gFnPc6	kyselina
na	na	k7c4	na
nikelnaté	nikelnatý	k2eAgFnPc4d1	nikelnatý
soli	sůl	k1gFnPc4	sůl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
nerost	nerost	k1gInSc4	nerost
bunsenit	bunsenit	k5eAaImF	bunsenit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
keramickém	keramický	k2eAgInSc6d1	keramický
průmyslu	průmysl	k1gInSc6	průmysl
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
barvení	barvení	k1gNnSc3	barvení
na	na	k7c4	na
šedo	šedo	k1gNnSc4	šedo
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
žíháním	žíhání	k1gNnSc7	žíhání
hydroxidu	hydroxid	k1gInSc2	hydroxid
nikelnatého	nikelnatý	k2eAgInSc2d1	nikelnatý
nebo	nebo	k8xC	nebo
uhličitanu	uhličitan	k1gInSc2	uhličitan
nikelnatého	nikelnatý	k2eAgInSc2d1	nikelnatý
<g/>
.	.	kIx.	.
</s>
<s>
Hydroxid	hydroxid	k1gInSc1	hydroxid
nikelnatý	nikelnatý	k2eAgInSc1d1	nikelnatý
Ni	on	k3xPp3gFnSc4	on
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
jablkově	jablkově	k6eAd1	jablkově
zelená	zelený	k2eAgFnSc1d1	zelená
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
hydroxidech	hydroxid	k1gInPc6	hydroxid
<g/>
,	,	kIx,	,
rozpustná	rozpustný	k2eAgNnPc1d1	rozpustné
v	v	k7c6	v
kyselinách	kyselina	k1gFnPc6	kyselina
a	a	k8xC	a
amoniakálních	amoniakální	k2eAgInPc6d1	amoniakální
roztocích	roztok	k1gInPc6	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
srážením	srážení	k1gNnSc7	srážení
roztoků	roztok	k1gInPc2	roztok
nikelnaté	nikelnatý	k2eAgFnSc2d1	nikelnatý
soli	sůl	k1gFnSc2	sůl
roztokem	roztok	k1gInSc7	roztok
alkalického	alkalický	k2eAgInSc2d1	alkalický
hydroxidu	hydroxid	k1gInSc2	hydroxid
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
niklitý	niklitý	k2eAgInSc1d1	niklitý
Ni	on	k3xPp3gFnSc4	on
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
šedý	šedý	k2eAgInSc1d1	šedý
až	až	k8xS	až
černý	černý	k2eAgInSc1d1	černý
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
rozpustný	rozpustný	k2eAgInSc1d1	rozpustný
v	v	k7c6	v
kyselinách	kyselina	k1gFnPc6	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnPc1d1	chlorovodíková
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nP	rozpouštět
za	za	k7c2	za
vývoje	vývoj	k1gInSc2	vývoj
chloru	chlor	k1gInSc2	chlor
a	a	k8xC	a
vzniku	vznik	k1gInSc2	vznik
nikelnaté	nikelnatý	k2eAgFnSc2d1	nikelnatý
soli	sůl	k1gFnSc2	sůl
<g/>
,	,	kIx,	,
v	v	k7c6	v
kyslíkatých	kyslíkatý	k2eAgFnPc6d1	kyslíkatá
kyselinách	kyselina	k1gFnPc6	kyselina
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
kyslíku	kyslík	k1gInSc2	kyslík
a	a	k8xC	a
nikelnaté	nikelnatý	k2eAgFnSc2d1	nikelnatý
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
niklitý	niklitý	k2eAgMnSc1d1	niklitý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
opatrnou	opatrný	k2eAgFnSc7d1	opatrná
oxidací	oxidace	k1gFnSc7	oxidace
uhličitanu	uhličitan	k1gInSc2	uhličitan
nikelnatého	nikelnatý	k2eAgInSc2d1	nikelnatý
nebo	nebo	k8xC	nebo
dusičnanu	dusičnan	k1gInSc2	dusičnan
nikelnatého	nikelnatý	k2eAgInSc2d1	nikelnatý
za	za	k7c2	za
teploty	teplota	k1gFnSc2	teplota
300	[number]	k4	300
°	°	k?	°
<g/>
C.	C.	kA	C.
Sulfid	sulfid	k1gInSc4	sulfid
nikelnatý	nikelnatý	k2eAgInSc1d1	nikelnatý
NiS	Nisa	k1gFnPc2	Nisa
je	být	k5eAaImIp3nS	být
černý	černý	k2eAgInSc1d1	černý
prášek	prášek	k1gInSc1	prášek
velmi	velmi	k6eAd1	velmi
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
hydroxidech	hydroxid	k1gInPc6	hydroxid
<g/>
,	,	kIx,	,
v	v	k7c6	v
čerstvém	čerstvý	k2eAgInSc6d1	čerstvý
stavu	stav	k1gInSc6	stav
rozpustný	rozpustný	k2eAgInSc4d1	rozpustný
v	v	k7c6	v
kyselinách	kyselina	k1gFnPc6	kyselina
<g/>
,	,	kIx,	,
po	po	k7c6	po
odstátí	odstátý	k2eAgMnPc1d1	odstátý
nerozpustný	rozpustný	k2eNgInSc5d1	nerozpustný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
nerost	nerost	k1gInSc1	nerost
millerit	millerit	k1gInSc1	millerit
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
srážením	srážení	k1gNnSc7	srážení
roztoků	roztok	k1gInPc2	roztok
nikelnatých	nikelnatý	k2eAgMnPc2d1	nikelnatý
solí	solit	k5eAaImIp3nP	solit
alkalickým	alkalický	k2eAgInSc7d1	alkalický
sulfidemem	sulfidem	k1gInSc7	sulfidem
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
nikelnatý	nikelnatý	k2eAgInSc4d1	nikelnatý
NiCl	NiCl	k1gInSc4	NiCl
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
zlatožlutá	zlatožlutý	k2eAgFnSc1d1	zlatožlutá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
v	v	k7c6	v
hydratované	hydratovaný	k2eAgFnSc6d1	hydratovaná
podobě	podoba	k1gFnSc6	podoba
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zelená	zelený	k2eAgFnSc1d1	zelená
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
lihu	líh	k1gInSc6	líh
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
alkalickými	alkalický	k2eAgInPc7d1	alkalický
chloridy	chlorid	k1gInPc7	chlorid
tvoří	tvořit	k5eAaImIp3nP	tvořit
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
podvojné	podvojný	k2eAgFnSc2d1	podvojná
sloučeniny	sloučenina	k1gFnSc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Působením	působení	k1gNnSc7	působení
fluoru	fluor	k1gInSc2	fluor
na	na	k7c4	na
roztok	roztok	k1gInSc4	roztok
chloridu	chlorid	k1gInSc2	chlorid
nikelnatého	nikelnatý	k2eAgInSc2d1	nikelnatý
a	a	k8xC	a
chloridu	chlorid	k1gInSc2	chlorid
draselného	draselný	k2eAgNnSc2d1	draselné
lze	lze	k6eAd1	lze
připravit	připravit	k5eAaPmF	připravit
sloučeniny	sloučenina	k1gFnSc2	sloučenina
K	k	k7c3	k
<g/>
3	[number]	k4	3
<g/>
[	[	kIx(	[
<g/>
NiIIIF	NiIIIF	k1gFnSc1	NiIIIF
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
a	a	k8xC	a
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
[	[	kIx(	[
<g/>
NiIVF	NiIVF	k1gFnSc1	NiIVF
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
spalováním	spalování	k1gNnSc7	spalování
niklu	nikl	k1gInSc2	nikl
v	v	k7c6	v
proudu	proud	k1gInSc6	proud
chloru	chlor	k1gInSc2	chlor
<g/>
.	.	kIx.	.
</s>
<s>
Bromid	bromid	k1gInSc1	bromid
nikelnatý	nikelnatý	k2eAgInSc4d1	nikelnatý
NiBr	NiBr	k1gInSc4	NiBr
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
hydratované	hydratovaný	k2eAgFnSc6d1	hydratovaná
podobě	podoba	k1gFnSc6	podoba
zelená	zelený	k2eAgFnSc1d1	zelená
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
spalováním	spalování	k1gNnSc7	spalování
niklu	nikl	k1gInSc2	nikl
v	v	k7c6	v
bromu	brom	k1gInSc6	brom
<g/>
.	.	kIx.	.
</s>
<s>
Jodid	jodid	k1gInSc4	jodid
nikelnatý	nikelnatý	k2eAgInSc4d1	nikelnatý
NiI	NiI	k1gFnSc7	NiI
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
hydratované	hydratovaný	k2eAgFnSc6d1	hydratovaná
podobě	podoba	k1gFnSc6	podoba
zelená	zelený	k2eAgFnSc1d1	zelená
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
spalováním	spalování	k1gNnSc7	spalování
niklu	nikl	k1gInSc2	nikl
v	v	k7c6	v
jodu	jod	k1gInSc6	jod
<g/>
.	.	kIx.	.
</s>
<s>
Fluorid	fluorid	k1gInSc4	fluorid
nikelnatý	nikelnatý	k2eAgInSc4d1	nikelnatý
NiF	NiF	k1gFnSc7	NiF
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
světle	světle	k6eAd1	světle
hnědý	hnědý	k2eAgInSc1d1	hnědý
až	až	k8xS	až
zelený	zelený	k2eAgInSc1d1	zelený
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
v	v	k7c6	v
hydratované	hydratovaný	k2eAgFnSc6d1	hydratovaná
podobě	podoba	k1gFnSc6	podoba
bledě	bledě	k6eAd1	bledě
zelený	zelený	k2eAgInSc1d1	zelený
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
nerozpustný	rozpustný	k2eNgMnSc1d1	nerozpustný
v	v	k7c6	v
lihu	líh	k1gInSc6	líh
a	a	k8xC	a
etheru	ether	k1gInSc6	ether
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
tvoří	tvořit	k5eAaImIp3nP	tvořit
podvojné	podvojný	k2eAgFnPc1d1	podvojná
soli	sůl	k1gFnPc1	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Fluorid	fluorid	k1gInSc1	fluorid
nikelnatý	nikelnatý	k2eAgMnSc1d1	nikelnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
zahříváním	zahřívání	k1gNnSc7	zahřívání
podvojného	podvojný	k2eAgInSc2d1	podvojný
fluoridu	fluorid	k1gInSc2	fluorid
nikelnato-amonného	nikelnatomonný	k2eAgInSc2d1	nikelnato-amonný
nebo	nebo	k8xC	nebo
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
hydroxidu	hydroxid	k1gInSc2	hydroxid
nikelnatého	nikelnatý	k2eAgInSc2d1	nikelnatý
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
fluorovodíkové	fluorovodíkový	k2eAgFnSc6d1	fluorovodíková
<g/>
.	.	kIx.	.
</s>
<s>
Kyanid	kyanid	k1gInSc1	kyanid
nikelnatý	nikelnatý	k2eAgInSc1d1	nikelnatý
Ni	on	k3xPp3gFnSc4	on
<g/>
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
hnědožlutý	hnědožlutý	k2eAgInSc4d1	hnědožlutý
prášek	prášek	k1gInSc4	prášek
<g/>
,	,	kIx,	,
v	v	k7c6	v
hydratovaném	hydratovaný	k2eAgInSc6d1	hydratovaný
stavu	stav	k1gInSc6	stav
jablkově	jablkově	k6eAd1	jablkově
zelená	zelený	k2eAgFnSc1d1	zelená
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
tvoří	tvořit	k5eAaImIp3nP	tvořit
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
kyanonikelnatano	kyanonikelnatana	k1gFnSc5	kyanonikelnatana
<g/>
.	.	kIx.	.
</s>
<s>
Kyanid	kyanid	k1gInSc1	kyanid
nikelnatý	nikelnatý	k2eAgMnSc1d1	nikelnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
srážením	srážení	k1gNnSc7	srážení
nikelnaté	nikelnatý	k2eAgFnSc2d1	nikelnatý
soli	sůl	k1gFnSc2	sůl
roztokem	roztok	k1gInSc7	roztok
soli	sůl	k1gFnSc2	sůl
alkalického	alkalický	k2eAgInSc2d1	alkalický
kyanidu	kyanid	k1gInSc2	kyanid
<g/>
.	.	kIx.	.
</s>
<s>
Rhodanid	Rhodanid	k1gInSc1	Rhodanid
nikelnatý	nikelnatý	k2eAgInSc1d1	nikelnatý
Ni	on	k3xPp3gFnSc4	on
<g/>
(	(	kIx(	(
<g/>
SCN	SCN	kA	SCN
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
žlutohnědá	žlutohnědý	k2eAgFnSc1d1	žlutohnědá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
na	na	k7c4	na
zelený	zelený	k2eAgInSc4d1	zelený
roztok	roztok	k1gInSc4	roztok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
tvoří	tvořit	k5eAaImIp3nP	tvořit
podvojné	podvojný	k2eAgFnPc1d1	podvojná
sloučeniny	sloučenina	k1gFnPc1	sloučenina
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
rhodanonikelnatany	rhodanonikelnatan	k1gInPc1	rhodanonikelnatan
<g/>
.	.	kIx.	.
</s>
<s>
Rhodanid	Rhodanid	k1gInSc1	Rhodanid
nikelnatý	nikelnatý	k2eAgMnSc1d1	nikelnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
uhličitanu	uhličitan	k1gInSc2	uhličitan
nikelnatého	nikelnatý	k2eAgInSc2d1	nikelnatý
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
rhodanovodíkové	rhodanovodíková	k1gFnSc2	rhodanovodíková
nebo	nebo	k8xC	nebo
reakcí	reakce	k1gFnSc7	reakce
síranu	síran	k1gInSc2	síran
nikelnatého	nikelnatý	k2eAgInSc2d1	nikelnatý
s	s	k7c7	s
rhodanidem	rhodanid	k1gInSc7	rhodanid
barnatým	barnatý	k2eAgInSc7d1	barnatý
<g/>
.	.	kIx.	.
</s>
<s>
Dusičnan	dusičnan	k1gInSc1	dusičnan
nikelnatý	nikelnatý	k2eAgInSc1d1	nikelnatý
Ni	on	k3xPp3gFnSc4	on
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
hydratovaném	hydratovaný	k2eAgInSc6d1	hydratovaný
stavu	stav	k1gInSc6	stav
smaragdově	smaragdově	k6eAd1	smaragdově
zelená	zelený	k2eAgFnSc1d1	zelená
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
v	v	k7c6	v
keramickém	keramický	k2eAgInSc6d1	keramický
průmyslu	průmysl	k1gInSc6	průmysl
k	k	k7c3	k
barvení	barvení	k1gNnSc3	barvení
na	na	k7c4	na
hnědo	hnědo	k1gNnSc4	hnědo
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
hydroxidu	hydroxid	k1gInSc2	hydroxid
nikelnatého	nikelnatý	k2eAgInSc2d1	nikelnatý
nebo	nebo	k8xC	nebo
uhličitanu	uhličitan	k1gInSc2	uhličitan
nikelnatého	nikelnatý	k2eAgInSc2d1	nikelnatý
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
dusičné	dusičný	k2eAgFnSc6d1	dusičná
<g/>
.	.	kIx.	.
</s>
<s>
Dusitan	dusitan	k1gInSc1	dusitan
nikelnatý	nikelnatý	k2eAgInSc1d1	nikelnatý
Ni	on	k3xPp3gFnSc4	on
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
hydratovaném	hydratovaný	k2eAgInSc6d1	hydratovaný
stavu	stav	k1gInSc6	stav
červenožlutá	červenožlutý	k2eAgFnSc1d1	červenožlutá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
na	na	k7c4	na
zelený	zelený	k2eAgInSc4d1	zelený
roztok	roztok	k1gInSc4	roztok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
tvoří	tvořit	k5eAaImIp3nP	tvořit
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
nitronikelnatany	nitronikelnatan	k1gInPc1	nitronikelnatan
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
reakcí	reakce	k1gFnSc7	reakce
dusitanu	dusitan	k1gInSc2	dusitan
barnatého	barnatý	k2eAgInSc2d1	barnatý
se	s	k7c7	s
síranem	síran	k1gInSc7	síran
nikelnatým	nikelnatý	k2eAgInSc7d1	nikelnatý
<g/>
.	.	kIx.	.
</s>
<s>
Síran	síran	k1gInSc1	síran
nikelnatý	nikelnatý	k2eAgInSc1d1	nikelnatý
NiSO	Nisa	k1gFnSc5	Nisa
<g/>
4	[number]	k4	4
se	se	k3xPyFc4	se
v	v	k7c6	v
hydratované	hydratovaný	k2eAgFnSc6d1	hydratovaná
podobě	podoba	k1gFnSc6	podoba
hexahydrátu	hexahydrát	k1gInSc2	hexahydrát
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
modifikacích	modifikace	k1gFnPc6	modifikace
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
je	být	k5eAaImIp3nS	být
stálá	stálý	k2eAgFnSc1d1	stálá
mezi	mezi	k7c4	mezi
31,5	[number]	k4	31,5
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
53,3	[number]	k4	53,3
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
modrozelenou	modrozelený	k2eAgFnSc4d1	modrozelená
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
je	být	k5eAaImIp3nS	být
stálá	stálý	k2eAgFnSc1d1	stálá
nad	nad	k7c4	nad
53,3	[number]	k4	53,3
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
obyčejné	obyčejný	k2eAgFnPc4d1	obyčejná
teploty	teplota	k1gFnPc4	teplota
krystaluje	krystalovat	k5eAaImIp3nS	krystalovat
heptahydrát	heptahydrát	k1gInSc4	heptahydrát
v	v	k7c6	v
smaragdově	smaragdově	k6eAd1	smaragdově
zelených	zelený	k2eAgInPc6d1	zelený
krystalech	krystal	k1gInPc6	krystal
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
nikelnatá	nikelnatý	k2eAgFnSc1d1	nikelnatý
skalice	skalice	k1gFnSc1	skalice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
nerost	nerost	k1gInSc4	nerost
morenosit	morenosit	k5eAaPmF	morenosit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
tvoří	tvořit	k5eAaImIp3nP	tvořit
se	se	k3xPyFc4	se
sírany	síran	k1gInPc7	síran
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
podvojné	podvojný	k2eAgFnSc2d1	podvojná
sloučeniny	sloučenina	k1gFnSc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Síran	síran	k1gInSc1	síran
nikelnatý	nikelnatý	k2eAgMnSc1d1	nikelnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
oxidu	oxid	k1gInSc2	oxid
nikelnatého	nikelnatý	k2eAgInSc2d1	nikelnatý
nebo	nebo	k8xC	nebo
uhličitanu	uhličitan	k1gInSc2	uhličitan
nikelnatého	nikelnatý	k2eAgInSc2d1	nikelnatý
ve	v	k7c6	v
zředěné	zředěný	k2eAgFnSc6d1	zředěná
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgFnSc6d1	sírová
<g/>
.	.	kIx.	.
</s>
<s>
Uhličitan	uhličitan	k1gInSc4	uhličitan
nikelnatý	nikelnatý	k2eAgInSc4d1	nikelnatý
NiCO	NiCO	k1gFnSc7	NiCO
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
světle	světle	k6eAd1	světle
zelená	zelený	k2eAgFnSc1d1	zelená
jemně	jemně	k6eAd1	jemně
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
z	z	k7c2	z
roztoků	roztok	k1gInPc2	roztok
získává	získávat	k5eAaImIp3nS	získávat
zásaditý	zásaditý	k2eAgInSc1d1	zásaditý
uhličitan	uhličitan	k1gInSc1	uhličitan
nikelnatý	nikelnatý	k2eAgInSc1d1	nikelnatý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
lze	lze	k6eAd1	lze
s	s	k7c7	s
uhličitany	uhličitan	k1gInPc7	uhličitan
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
získat	získat	k5eAaPmF	získat
podvojné	podvojný	k2eAgFnPc4d1	podvojná
sloučeniny	sloučenina	k1gFnPc4	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Uhličitan	uhličitan	k1gInSc1	uhličitan
nikelnatý	nikelnatý	k2eAgMnSc1d1	nikelnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
srážením	srážení	k1gNnSc7	srážení
roztoků	roztok	k1gInPc2	roztok
nikelnatých	nikelnatý	k2eAgMnPc2d1	nikelnatý
solí	solit	k5eAaImIp3nP	solit
roztokem	roztok	k1gInSc7	roztok
alkalického	alkalický	k2eAgInSc2d1	alkalický
hydrogenuhličitanu	hydrogenuhličitan	k1gInSc2	hydrogenuhličitan
<g/>
.	.	kIx.	.
</s>
<s>
Komplexy	komplex	k1gInPc1	komplex
NiIV	NiIV	k1gFnSc2	NiIV
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
výjimečně	výjimečně	k6eAd1	výjimečně
stabilní	stabilní	k2eAgFnSc1d1	stabilní
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
a	a	k8xC	a
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
všechny	všechen	k3xTgFnPc1	všechen
bez	bez	k7c2	bez
výjimky	výjimka	k1gFnSc2	výjimka
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oxidačním	oxidační	k2eAgInSc6d1	oxidační
stavu	stav	k1gInSc6	stav
NiIII	NiIII	k1gFnSc2	NiIII
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgInPc1	některý
komplexy	komplex	k1gInPc1	komplex
stabilní	stabilní	k2eAgInPc1d1	stabilní
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
hned	hned	k6eAd1	hned
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
fialová	fialový	k2eAgFnSc1d1	fialová
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
K	k	k7c3	k
<g/>
3	[number]	k4	3
<g/>
[	[	kIx(	[
<g/>
NiF	NiF	k1gFnSc1	NiF
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
vodu	voda	k1gFnSc4	voda
za	za	k7c2	za
vývoje	vývoj	k1gInSc2	vývoj
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Získává	získávat	k5eAaImIp3nS	získávat
se	s	k7c7	s
fluorací	fluorace	k1gFnSc7	fluorace
směsi	směs	k1gFnSc2	směs
chloridu	chlorid	k1gInSc2	chlorid
draselného	draselný	k2eAgInSc2d1	draselný
a	a	k8xC	a
nikelnatého	nikelnatý	k2eAgInSc2d1	nikelnatý
za	za	k7c2	za
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
oktaedrických	oktaedrický	k2eAgInPc2d1	oktaedrický
komplexů	komplex	k1gInPc2	komplex
existují	existovat	k5eAaImIp3nP	existovat
s	s	k7c7	s
komplexy	komplex	k1gInPc7	komplex
s	s	k7c7	s
koordinačním	koordinační	k2eAgNnSc7d1	koordinační
číslem	číslo	k1gNnSc7	číslo
5	[number]	k4	5
<g/>
,	,	kIx,	,
např.	např.	kA	např.
černý	černý	k2eAgInSc1d1	černý
trigonálně	trigonálně	k6eAd1	trigonálně
bipyramidální	bipyramidální	k2eAgInSc1d1	bipyramidální
[	[	kIx(	[
<g/>
NiBr	NiBr	k1gInSc1	NiBr
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
PEt	PEt	k1gFnSc1	PEt
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
Nikelnaté	nikelnatý	k2eAgFnPc1d1	nikelnatý
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
jsou	být	k5eAaImIp3nP	být
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
odolné	odolný	k2eAgFnPc1d1	odolná
vůči	vůči	k7c3	vůči
oxidaci	oxidace	k1gFnSc3	oxidace
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
soli	sůl	k1gFnPc4	sůl
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
anionty	anion	k1gInPc7	anion
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vodném	vodný	k2eAgInSc6d1	vodný
roztoku	roztok	k1gInSc6	roztok
je	být	k5eAaImIp3nS	být
běžný	běžný	k2eAgInSc1d1	běžný
zelený	zelený	k2eAgInSc1d1	zelený
hexaaquanikelnatý	hexaaquanikelnatý	k2eAgInSc1d1	hexaaquanikelnatý
kation	kation	k1gInSc1	kation
[	[	kIx(	[
<g/>
Ni	on	k3xPp3gFnSc4	on
<g/>
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
NiII	NiII	k?	NiII
překračuje	překračovat	k5eAaImIp3nS	překračovat
koordinační	koordinační	k2eAgNnSc1d1	koordinační
číslo	číslo	k1gNnSc1	číslo
6	[number]	k4	6
pouze	pouze	k6eAd1	pouze
vzácně	vzácně	k6eAd1	vzácně
<g/>
,	,	kIx,	,
základní	základní	k2eAgNnPc1d1	základní
uspořádání	uspořádání	k1gNnPc1	uspořádání
jsou	být	k5eAaImIp3nP	být
oktaedrická	oktaedrický	k2eAgNnPc1d1	oktaedrický
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
a	a	k8xC	a
čtvercová	čtvercový	k2eAgFnSc1d1	čtvercová
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
časté	častý	k2eAgFnPc1d1	častá
konformace	konformace	k1gFnPc1	konformace
jsou	být	k5eAaImIp3nP	být
trigonálně	trigonálně	k6eAd1	trigonálně
bipyramidální	bipyramidální	k2eAgMnPc1d1	bipyramidální
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čtvercově	čtvercově	k6eAd1	čtvercově
pyramidální	pyramidální	k2eAgFnSc1d1	pyramidální
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
a	a	k8xC	a
tetraedrické	tetraedrický	k2eAgNnSc1d1	tetraedrický
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
nikl	niknout	k5eAaImAgMnS	niknout
možnost	možnost	k1gFnSc4	možnost
vybrat	vybrat	k5eAaPmF	vybrat
si	se	k3xPyFc3	se
mezi	mezi	k7c7	mezi
tetraedrem	tetraedr	k1gInSc7	tetraedr
a	a	k8xC	a
čtvercem	čtverec	k1gInSc7	čtverec
<g/>
,	,	kIx,	,
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
díky	díky	k7c3	díky
stabilizační	stabilizační	k2eAgFnSc3d1	stabilizační
energii	energie	k1gFnSc3	energie
ligandového	ligandový	k2eAgNnSc2d1	ligandový
pole	pole	k1gNnSc2	pole
v	v	k7c6	v
nízkospinových	nízkospinův	k2eAgInPc6d1	nízkospinův
komplexech	komplex	k1gInPc6	komplex
čtvercové	čtvercový	k2eAgFnSc2d1	čtvercová
ligandové	ligandový	k2eAgFnSc2d1	ligandový
pole	pole	k1gFnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vysokospinových	vysokospinův	k2eAgInPc6d1	vysokospinův
komplexech	komplex	k1gInPc6	komplex
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
ligandu	ligand	k1gInSc2	ligand
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
ligandu	ligand	k1gInSc2	ligand
s	s	k7c7	s
velkými	velký	k2eAgFnPc7d1	velká
molekulami	molekula	k1gFnPc7	molekula
nebo	nebo	k8xC	nebo
atomy	atom	k1gInPc7	atom
vznikají	vznikat	k5eAaImIp3nP	vznikat
čtvercové	čtvercový	k2eAgNnSc4d1	čtvercové
ligandové	ligandový	k2eAgNnSc4d1	ligandový
pole	pole	k1gNnSc4	pole
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
ligandů	ligand	k1gInPc2	ligand
s	s	k7c7	s
malými	malý	k2eAgInPc7d1	malý
atomy	atom	k1gInPc7	atom
nebo	nebo	k8xC	nebo
molekulami	molekula	k1gFnPc7	molekula
vzniká	vznikat	k5eAaImIp3nS	vznikat
tetraedrické	tetraedrický	k2eAgNnSc1d1	tetraedrický
ligandové	ligandový	k2eAgNnSc1d1	ligandový
uspořádání	uspořádání	k1gNnSc1	uspořádání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
velkého	velký	k2eAgInSc2d1	velký
nadbytku	nadbytek	k1gInSc2	nadbytek
komplexních	komplexní	k2eAgFnPc2d1	komplexní
částic	částice	k1gFnPc2	částice
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
je	být	k5eAaImIp3nS	být
nikl	nikl	k1gInSc1	nikl
schopen	schopen	k2eAgInSc1d1	schopen
uplatnit	uplatnit	k5eAaPmF	uplatnit
koordinační	koordinační	k2eAgNnSc4d1	koordinační
číslo	číslo	k1gNnSc4	číslo
5	[number]	k4	5
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
také	také	k9	také
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
velikosti	velikost	k1gFnSc6	velikost
ligandu	ligand	k1gInSc2	ligand
<g/>
.	.	kIx.	.
</s>
<s>
Rozměrné	rozměrný	k2eAgFnPc1d1	rozměrná
ligandy	liganda	k1gFnPc1	liganda
se	se	k3xPyFc4	se
váží	vážit	k5eAaImIp3nP	vážit
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
čtvercové	čtvercový	k2eAgFnSc2d1	čtvercová
pyramidy	pyramida	k1gFnSc2	pyramida
a	a	k8xC	a
malé	malá	k1gFnSc2	malá
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
trigonální	trigonální	k2eAgFnSc2d1	trigonální
bipyramidy	bipyramida	k1gFnSc2	bipyramida
<g/>
.	.	kIx.	.
</s>
<s>
Nikl	Nikl	k1gMnSc1	Nikl
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
přednostně	přednostně	k6eAd1	přednostně
komplexní	komplexní	k2eAgFnPc4d1	komplexní
sloučeniny	sloučenina	k1gFnPc4	sloučenina
s	s	k7c7	s
dusíkatými	dusíkatý	k2eAgInPc7d1	dusíkatý
ligandy	ligand	k1gInPc7	ligand
<g/>
,	,	kIx,	,
např.	např.	kA	např.
NH	NH	kA	NH
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
en	en	k?	en
(	(	kIx(	(
<g/>
ethylendiamin	ethylendiamin	k1gInSc1	ethylendiamin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bipy	bipy	k1gInPc1	bipy
(	(	kIx(	(
<g/>
bipyridyl	bipyridyl	k1gInSc1	bipyridyl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
phen	phen	k1gInSc1	phen
(	(	kIx(	(
<g/>
fenanthrolin	fenanthrolin	k1gInSc1	fenanthrolin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
NCS-	NCS-	k1gFnSc4	NCS-
a	a	k8xC	a
NO	no	k9	no
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Amoniakáty	Amoniakáta	k1gFnPc1	Amoniakáta
mají	mít	k5eAaImIp3nP	mít
při	při	k7c6	při
plném	plný	k2eAgNnSc6d1	plné
nahrazení	nahrazení	k1gNnSc6	nahrazení
molekul	molekula	k1gFnPc2	molekula
vody	voda	k1gFnSc2	voda
v	v	k7c4	v
nikelnaté	nikelnatý	k2eAgFnPc4d1	nikelnatý
soli	sůl	k1gFnPc4	sůl
na	na	k7c4	na
hexaamminnikelnatý	hexaamminnikelnatý	k2eAgInSc4d1	hexaamminnikelnatý
kation	kation	k1gInSc4	kation
[	[	kIx(	[
<g/>
Ni	on	k3xPp3gFnSc4	on
<g/>
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
obvykle	obvykle	k6eAd1	obvykle
fialovou	fialový	k2eAgFnSc4d1	fialová
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nenahradí	nahradit	k5eNaPmIp3nP	nahradit
všechny	všechen	k3xTgFnPc1	všechen
molekuly	molekula	k1gFnPc1	molekula
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
má	mít	k5eAaImIp3nS	mít
látka	látka	k1gFnSc1	látka
obvykle	obvykle	k6eAd1	obvykle
modrou	modrý	k2eAgFnSc7d1	modrá
až	až	k8xS	až
tmavě	tmavě	k6eAd1	tmavě
modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Amoniakáty	Amoniakáta	k1gFnPc1	Amoniakáta
se	se	k3xPyFc4	se
připravují	připravovat	k5eAaImIp3nP	připravovat
převáděním	převádění	k1gNnSc7	převádění
amoniaku	amoniak	k1gInSc2	amoniak
přes	přes	k7c4	přes
bezvodé	bezvodý	k2eAgFnPc4d1	bezvodá
nikelnaté	nikelnatý	k2eAgFnPc4d1	nikelnatý
soli	sůl	k1gFnPc4	sůl
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
k	k	k7c3	k
hydratovaným	hydratovaný	k2eAgFnPc3d1	hydratovaná
solím	sůl	k1gFnPc3	sůl
přidává	přidávat	k5eAaImIp3nS	přidávat
roztok	roztok	k1gInSc4	roztok
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
.	.	kIx.	.
</s>
<s>
Roztok	roztok	k1gInSc1	roztok
amoniakátů	amoniakát	k1gInPc2	amoniakát
je	být	k5eAaImIp3nS	být
modrý	modrý	k2eAgInSc1d1	modrý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
částečné	částečný	k2eAgFnSc3d1	částečná
výměně	výměna	k1gFnSc3	výměna
molekul	molekula	k1gFnPc2	molekula
amoniaku	amoniak	k1gInSc2	amoniak
za	za	k7c2	za
molekuly	molekula	k1gFnSc2	molekula
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Čtvercové	čtvercový	k2eAgFnPc1d1	čtvercová
konformace	konformace	k1gFnPc1	konformace
tvoří	tvořit	k5eAaImIp3nP	tvořit
komplexy	komplex	k1gInPc4	komplex
[	[	kIx(	[
<g/>
NiX	NiX	k1gFnSc1	NiX
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
tetrakyanonikelnatany	tetrakyanonikelnatan	k1gInPc1	tetrakyanonikelnatan
jsou	být	k5eAaImIp3nP	být
zlatožluté	zlatožlutý	k2eAgInPc1d1	zlatožlutý
<g/>
,	,	kIx,	,
tetrarhodanonikelnatany	tetrarhodanonikelnatan	k1gInPc1	tetrarhodanonikelnatan
světle	světle	k6eAd1	světle
zelené	zelený	k2eAgInPc1d1	zelený
(	(	kIx(	(
<g/>
oktaedrické	oktaedrický	k2eAgInPc1d1	oktaedrický
[	[	kIx(	[
<g/>
Ni	on	k3xPp3gFnSc4	on
<g/>
(	(	kIx(	(
<g/>
SCN	SCN	kA	SCN
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
modré	modrý	k2eAgFnPc1d1	modrá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tetrachloro-	tetrachloro-	k?	tetrachloro-
<g/>
,	,	kIx,	,
tetrabromo-	tetrabromo-	k?	tetrabromo-
a	a	k8xC	a
tetrajodonikelnatany	tetrajodonikelnatan	k1gInPc1	tetrajodonikelnatan
jsou	být	k5eAaImIp3nP	být
modré	modrý	k2eAgFnPc1d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
zástupcem	zástupce	k1gMnSc7	zástupce
je	být	k5eAaImIp3nS	být
tetrakarbonyl	tetrakarbonyl	k1gInSc4	tetrakarbonyl
niklu	nikl	k1gInSc2	nikl
[	[	kIx(	[
<g/>
Ni	on	k3xPp3gFnSc4	on
<g/>
(	(	kIx(	(
<g/>
CO	co	k8xS	co
<g/>
)	)	kIx)	)
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
těkavá	těkavý	k2eAgFnSc1d1	těkavá
kapalina	kapalina	k1gFnSc1	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
sloučenina	sloučenina	k1gFnSc1	sloučenina
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
toxická	toxický	k2eAgFnSc1d1	toxická
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
oxid	oxid	k1gInSc1	oxid
uhelnatý	uhelnatý	k2eAgInSc1d1	uhelnatý
CO	co	k6eAd1	co
<g/>
,	,	kIx,	,
nutný	nutný	k2eAgInSc1d1	nutný
pro	pro	k7c4	pro
její	její	k3xOp3gFnSc4	její
výrobu	výroba	k1gFnSc4	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Šťavelan	šťavelan	k1gInSc4	šťavelan
nikelnatý	nikelnatý	k2eAgInSc4d1	nikelnatý
NiC	nic	k6eAd1	nic
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
nazelenalá	nazelenalý	k2eAgFnSc1d1	nazelenalá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
v	v	k7c6	v
kyselinách	kyselina	k1gFnPc6	kyselina
a	a	k8xC	a
amoniakálním	amoniakální	k2eAgInSc6d1	amoniakální
roztoku	roztok	k1gInSc6	roztok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
tvoří	tvořit	k5eAaImIp3nP	tvořit
podvojné	podvojný	k2eAgFnPc1d1	podvojná
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Šťavelan	šťavelan	k1gInSc1	šťavelan
nikelnatý	nikelnatý	k2eAgInSc1d1	nikelnatý
se	s	k7c7	s
připravuje	připravovat	k5eAaImIp3nS	připravovat
hydroxidu	hydroxid	k1gInSc2	hydroxid
nikelnatého	nikelnatý	k2eAgInSc2d1	nikelnatý
nebo	nebo	k8xC	nebo
uhličitanu	uhličitan	k1gInSc2	uhličitan
nikelnatého	nikelnatý	k2eAgInSc2d1	nikelnatý
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
šťavelové	šťavelový	k2eAgFnSc6d1	šťavelová
<g/>
.	.	kIx.	.
</s>
<s>
Octan	octan	k1gInSc1	octan
nikelnatý	nikelnatý	k2eAgInSc1d1	nikelnatý
Ni	on	k3xPp3gFnSc4	on
<g/>
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
COO	COO	kA	COO
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
jablkově	jablkově	k6eAd1	jablkově
zelená	zelený	k2eAgFnSc1d1	zelená
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
v	v	k7c6	v
lihu	líh	k1gInSc6	líh
<g/>
.	.	kIx.	.
</s>
<s>
Vodný	vodný	k2eAgInSc1d1	vodný
roztok	roztok	k1gInSc1	roztok
má	mít	k5eAaImIp3nS	mít
sladkou	sladký	k2eAgFnSc4d1	sladká
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
hydroxidu	hydroxid	k1gInSc2	hydroxid
nikelnatého	nikelnatý	k2eAgInSc2d1	nikelnatý
v	v	k7c6	v
chladné	chladný	k2eAgFnSc6d1	chladná
kyselině	kyselina	k1gFnSc6	kyselina
octové	octový	k2eAgFnSc6d1	octová
<g/>
.	.	kIx.	.
</s>
<s>
Nikl	Nikl	k1gMnSc1	Nikl
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
několik	několik	k4yIc4	několik
málo	málo	k4c4	málo
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
zdravotní	zdravotní	k2eAgInSc4d1	zdravotní
stav	stav	k1gInSc4	stav
lidského	lidský	k2eAgInSc2d1	lidský
organizmu	organizmus	k1gInSc2	organizmus
je	být	k5eAaImIp3nS	být
jednoznačně	jednoznačně	k6eAd1	jednoznačně
negativní	negativní	k2eAgMnSc1d1	negativní
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
být	být	k5eAaImF	být
kuriózní	kuriózní	k2eAgInSc4d1	kuriózní
např.	např.	kA	např.
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
chemicky	chemicky	k6eAd1	chemicky
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc1d1	podobný
kobaltu	kobalt	k1gInSc3	kobalt
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
nezbytnou	nezbytný	k2eAgFnSc7d1	nezbytná
součástí	součást	k1gFnSc7	součást
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
pro	pro	k7c4	pro
správný	správný	k2eAgInSc4d1	správný
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
zdravotní	zdravotní	k2eAgInSc4d1	zdravotní
stav	stav	k1gInSc4	stav
lidského	lidský	k2eAgInSc2d1	lidský
organizmu	organizmus	k1gInSc2	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
velkých	velký	k2eAgFnPc6d1	velká
anebo	anebo	k8xC	anebo
pravidelně	pravidelně	k6eAd1	pravidelně
zvýšených	zvýšený	k2eAgFnPc6d1	zvýšená
dávkách	dávka	k1gFnPc6	dávka
niklu	nikl	k1gInSc2	nikl
se	se	k3xPyFc4	se
silně	silně	k6eAd1	silně
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc1	riziko
vzniku	vznik	k1gInSc2	vznik
rakoviny	rakovina	k1gFnSc2	rakovina
a	a	k8xC	a
nikl	nikl	k1gInSc1	nikl
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
řazen	řadit	k5eAaImNgInS	řadit
i	i	k9	i
mezi	mezi	k7c4	mezi
teratogeny	teratogen	k1gInPc4	teratogen
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
látky	látka	k1gFnPc1	látka
schopné	schopný	k2eAgMnPc4d1	schopný
negativním	negativní	k2eAgInSc7d1	negativní
způsobem	způsob	k1gInSc7	způsob
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
vývoj	vývoj	k1gInSc4	vývoj
lidského	lidský	k2eAgInSc2d1	lidský
plodu	plod	k1gInSc2	plod
<g/>
.	.	kIx.	.
</s>
<s>
Ohrožení	ohrožení	k1gNnSc1	ohrožení
takovými	takový	k3xDgNnPc7	takový
dávkami	dávka	k1gFnPc7	dávka
niklu	nikl	k1gInSc2	nikl
však	však	k9	však
hrozí	hrozit	k5eAaImIp3nS	hrozit
pouze	pouze	k6eAd1	pouze
pracovníkům	pracovník	k1gMnPc3	pracovník
metalurgických	metalurgický	k2eAgInPc2d1	metalurgický
provozů	provoz	k1gInPc2	provoz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
zpracováním	zpracování	k1gNnSc7	zpracování
tohoto	tento	k3xDgInSc2	tento
kovu	kov	k1gInSc2	kov
a	a	k8xC	a
nedodržují	dodržovat	k5eNaImIp3nP	dodržovat
základní	základní	k2eAgNnPc4d1	základní
pravidla	pravidlo	k1gNnPc4	pravidlo
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
životě	život	k1gInSc6	život
se	se	k3xPyFc4	se
však	však	k9	však
poměrně	poměrně	k6eAd1	poměrně
často	často	k6eAd1	často
setkáváme	setkávat	k5eAaImIp1nP	setkávat
s	s	k7c7	s
kožní	kožní	k2eAgFnSc7d1	kožní
alergií	alergie	k1gFnSc7	alergie
na	na	k7c4	na
nikl	nikl	k1gInSc4	nikl
<g/>
.	.	kIx.	.
</s>
<s>
Projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
u	u	k7c2	u
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
a	a	k8xC	a
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
ji	on	k3xPp3gFnSc4	on
nejprve	nejprve	k6eAd1	nejprve
zarudnutí	zarudnutí	k1gNnSc3	zarudnutí
kůže	kůže	k1gFnSc2	kůže
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
až	až	k9	až
vznik	vznik	k1gInSc4	vznik
kožních	kožní	k2eAgInPc2d1	kožní
ekzémů	ekzém	k1gInPc2	ekzém
při	při	k7c6	při
trvalém	trvalý	k2eAgInSc6d1	trvalý
styku	styk	k1gInSc6	styk
s	s	k7c7	s
předměty	předmět	k1gInPc7	předmět
z	z	k7c2	z
niklu	nikl	k1gInSc2	nikl
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
nebezpečné	bezpečný	k2eNgFnPc1d1	nebezpečná
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
náušnice	náušnice	k1gFnSc2	náušnice
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ušní	ušní	k2eAgInSc1d1	ušní
lalůček	lalůček	k1gInSc1	lalůček
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
velice	velice	k6eAd1	velice
citlivé	citlivý	k2eAgFnSc6d1	citlivá
části	část	k1gFnSc6	část
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
alergické	alergický	k2eAgNnSc4d1	alergické
působení	působení	k1gNnSc4	působení
zde	zde	k6eAd1	zde
může	moct	k5eAaImIp3nS	moct
nabývat	nabývat	k5eAaImF	nabývat
dramatičtějších	dramatický	k2eAgInPc2d2	dramatičtější
rozměrů	rozměr	k1gInPc2	rozměr
-	-	kIx~	-
otoky	otok	k1gInPc1	otok
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
astmatické	astmatický	k2eAgInPc4d1	astmatický
záchvaty	záchvat	k1gInPc4	záchvat
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
výše	výše	k1gFnSc2	výše
uvedených	uvedený	k2eAgMnPc2d1	uvedený
alergiků	alergik	k1gMnPc2	alergik
však	však	k9	však
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
podstatně	podstatně	k6eAd1	podstatně
méně	málo	k6eAd2	málo
početná	početný	k2eAgFnSc1d1	početná
skupina	skupina	k1gFnSc1	skupina
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
trpí	trpět	k5eAaImIp3nP	trpět
alergií	alergie	k1gFnSc7	alergie
na	na	k7c4	na
nikl	nikl	k1gInSc4	nikl
v	v	k7c6	v
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
<g/>
.	.	kIx.	.
</s>
<s>
Těm	ten	k3xDgMnPc3	ten
pak	pak	k6eAd1	pak
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
alergické	alergický	k2eAgFnSc2d1	alergická
reakce	reakce	k1gFnSc2	reakce
např.	např.	kA	např.
i	i	k9	i
placení	placený	k2eAgMnPc1d1	placený
mincemi	mince	k1gFnPc7	mince
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
niklu	nikl	k1gInSc2	nikl
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
diskuze	diskuze	k1gFnSc2	diskuze
o	o	k7c6	o
budoucnosti	budoucnost	k1gFnSc6	budoucnost
euromincí	eurominký	k2eAgMnPc1d1	eurominký
o	o	k7c6	o
hodnotě	hodnota	k1gFnSc6	hodnota
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
eura	euro	k1gNnSc2	euro
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
nikl	niknout	k5eAaImAgMnS	niknout
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
nikl	niknout	k5eAaImAgInS	niknout
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
nikl	nikl	k1gInSc1	nikl
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Nickel	Nickel	k1gMnSc1	Nickel
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Periodic	Periodic	k1gMnSc1	Periodic
Table	tablo	k1gNnSc6	tablo
of	of	k?	of
Videos	Videos	k1gInSc1	Videos
<g/>
,	,	kIx,	,
University	universita	k1gFnPc1	universita
of	of	k?	of
Nottingham	Nottingham	k1gInSc1	Nottingham
<g/>
)	)	kIx)	)
</s>
