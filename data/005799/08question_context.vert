<s>
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
Universitas	Universitas	k1gInSc1
Masarykiana	Masarykiana	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1960	[number]	k4
<g/>
–	–	k?
<g/>
1990	[number]	k4
Univerzita	univerzita	k1gFnSc1
Jana	Jan	k1gMnSc2
Evangelisty	evangelista	k1gMnSc2
Purkyně	Purkyně	k1gFnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
česká	český	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
</s>