<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Heyrovský	Heyrovský	k2eAgMnSc1d1	Heyrovský
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1890	[number]	k4	1890
Praha-Staré	Praha-Starý	k2eAgNnSc4d1	Praha-Staré
Město	město	k1gNnSc4	město
–	–	k?	–
27.	[number]	k4	27.
března	březen	k1gInSc2	březen
1967	[number]	k4	1967
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Smíchov	Smíchov	k1gInSc1	Smíchov
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
fyzikální	fyzikální	k2eAgMnSc1d1	fyzikální
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
objevitel	objevitel	k1gMnSc1	objevitel
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
polarografie	polarografie	k1gFnSc2	polarografie
a	a	k8xC	a
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
.	.	kIx.
</s>
</p>
<p>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Karlově	Karlův	k2eAgFnSc6d1	Karlova
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
University	universita	k1gFnPc4	universita
College	College	k1gFnPc2	College
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
fyzikální	fyzikální	k2eAgFnSc7d1	fyzikální
chemií	chemie	k1gFnSc7	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
bakaláře	bakalář	k1gMnSc2	bakalář
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
postgraduálního	postgraduální	k2eAgNnSc2d1	postgraduální
studia	studio	k1gNnSc2	studio
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c6	o
elektrochemii	elektrochemie	k1gFnSc6	elektrochemie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k9	jako
zdravotník	zdravotník	k1gMnSc1	zdravotník
v	v	k7c6	v
Innsbrucku	Innsbruck	k1gInSc6	Innsbruck
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
působil	působit	k5eAaImAgMnS	působit
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Karlově	Karlův	k2eAgFnSc6d1	Karlova
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
stal	stát	k5eAaPmAgMnS	stát
profesorem	profesor	k1gMnSc7	profesor
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
objevil	objevit	k5eAaPmAgMnS	objevit
polarografii	polarografie	k1gFnSc4	polarografie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
metoda	metoda	k1gFnSc1	metoda
používající	používající	k2eAgNnSc1d1	používající
měření	měření	k1gNnSc1	měření
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
rtuťovou	rtuťový	k2eAgFnSc7d1	rtuťová
kapkou	kapka	k1gFnSc7	kapka
a	a	k8xC	a
roztokem	roztok	k1gInSc7	roztok
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgNnSc2	jenž
rtuť	rtuť	k1gFnSc1	rtuť
odkapává	odkapávat	k5eAaImIp3nS	odkapávat
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
získat	získat	k5eAaPmF	získat
cenné	cenný	k2eAgFnPc4d1	cenná
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
druhu	druh	k1gInSc6	druh
a	a	k8xC	a
množství	množství	k1gNnSc1	množství
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
roztok	roztok	k1gInSc1	roztok
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
při	při	k7c6	při
chemické	chemický	k2eAgFnSc6d1	chemická
analýze	analýza	k1gFnSc6	analýza
i	i	k8xC	i
při	při	k7c6	při
základním	základní	k2eAgInSc6d1	základní
fyzikálně-chemickém	fyzikálněhemický	k2eAgInSc6d1	fyzikálně-chemický
výzkumu	výzkum	k1gInSc6	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
urychlení	urychlení	k1gNnSc4	urychlení
zaznamenávání	zaznamenávání	k1gNnPc2	zaznamenávání
hodnot	hodnota	k1gFnPc2	hodnota
s	s	k7c7	s
japonským	japonský	k2eAgMnSc7d1	japonský
vědcem	vědec	k1gMnSc7	vědec
Šikatou	Šikata	k1gFnSc7	Šikata
zkonstruovali	zkonstruovat	k5eAaPmAgMnP	zkonstruovat
polarograf	polarograf	k1gInSc4	polarograf
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tyto	tento	k3xDgFnPc4	tento
křivky	křivka	k1gFnPc4	křivka
automaticky	automaticky	k6eAd1	automaticky
zaznamenával	zaznamenávat	k5eAaImAgMnS	zaznamenávat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
objev	objev	k1gInSc4	objev
a	a	k8xC	a
rozpracování	rozpracování	k1gNnSc4	rozpracování
analytické	analytický	k2eAgFnSc2d1	analytická
polarografické	polarografický	k2eAgFnSc2d1	polarografická
metody	metoda	k1gFnSc2	metoda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
nominován	nominován	k2eAgMnSc1d1	nominován
již	již	k6eAd1	již
poosmnácté	poosmnácté	k4xO	poosmnácté
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
v	v	k7c6	v
Protektorátu	protektorát	k1gInSc6	protektorát
byly	být	k5eAaImAgFnP	být
univerzity	univerzita	k1gFnPc1	univerzita
zavřené	zavřený	k2eAgFnPc1d1	zavřená
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
dále	daleko	k6eAd2	daleko
vědecky	vědecky	k6eAd1	vědecky
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgInS	být
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
povolení	povolení	k1gNnSc4	povolení
vědeckého	vědecký	k2eAgInSc2d1	vědecký
výzkumu	výzkum	k1gInSc2	výzkum
obviněn	obvinit	k5eAaPmNgInS	obvinit
z	z	k7c2	z
kolaborace	kolaborace	k1gFnSc2	kolaborace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
ovšem	ovšem	k9	ovšem
nebyla	být	k5eNaImAgFnS	být
nikdy	nikdy	k6eAd1	nikdy
prokázána	prokázat	k5eAaPmNgFnS	prokázat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
spoluzakládal	spoluzakládat	k5eAaImAgInS	spoluzakládat
Polarografický	polarografický	k2eAgInSc1d1	polarografický
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
čele	čelo	k1gNnSc6	čelo
stál	stát	k5eAaImAgInS	stát
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
odešel	odejít	k5eAaPmAgMnS	odejít
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
do	do	k7c2	do
penze	penze	k1gFnSc2	penze
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
ve	v	k7c6	v
smíchovském	smíchovský	k2eAgNnSc6d1	Smíchovské
sanatoriu	sanatorium	k1gNnSc6	sanatorium
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
<g/>
,	,	kIx,	,
rozenou	rozený	k2eAgFnSc7d1	rozená
Kořánovou	Kořánová	k1gFnSc7	Kořánová
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1903	[number]	k4	1903
–	–	k?	–
27.	[number]	k4	27.
října	říjen	k1gInSc2	říjen
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
Jitku	Jitka	k1gFnSc4	Jitka
Černou	Černá	k1gFnSc4	Černá
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1929	[number]	k4	1929
–	–	k?	–
17.	[number]	k4	17.
dubna	duben	k1gInSc2	duben
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
a	a	k8xC	a
Michaela	Michaela	k1gFnSc1	Michaela
Heyrovského	Heyrovský	k2eAgInSc2d1	Heyrovský
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1932	[number]	k4	1932
–	–	k?	–
12.	[number]	k4	12.
dubna	duben	k1gInSc2	duben
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
se	se	k3xPyFc4	se
věnovali	věnovat	k5eAaImAgMnP	věnovat
vědecké	vědecký	k2eAgFnSc3d1	vědecká
činnosti	činnost	k1gFnSc3	činnost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Mládí	mládí	k1gNnSc2	mládí
a	a	k8xC	a
studia	studio	k1gNnSc2	studio
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
Kláry	Klára	k1gFnSc2	Klára
(	(	kIx(	(
<g/>
rozená	rozený	k2eAgFnSc1d1	rozená
Hanlová	Hanlová	k1gFnSc1	Hanlová
z	z	k7c2	z
Kirchtreu	Kirchtreus	k1gInSc2	Kirchtreus
<g/>
)	)	kIx)	)
a	a	k8xC	a
Leopolda	Leopolda	k1gFnSc1	Leopolda
Heyrovských	Heyrovský	k2eAgFnPc2d1	Heyrovská
jako	jako	k8xS	jako
čtvrté	čtvrtý	k4xOgFnPc1	čtvrtý
z	z	k7c2	z
pěti	pět	k4xCc2	pět
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
tři	tři	k4xCgFnPc4	tři
sestry	sestra	k1gFnPc4	sestra
<g/>
,	,	kIx,	,
Kláru	Klára	k1gFnSc4	Klára
<g/>
,	,	kIx,	,
provdanou	provdaný	k2eAgFnSc4d1	provdaná
za	za	k7c4	za
Arnošta	Arnošt	k1gMnSc4	Arnošt
Hofbauera	Hofbauer	k1gMnSc4	Hofbauer
<g/>
,	,	kIx,	,
Marii	Maria	k1gFnSc4	Maria
a	a	k8xC	a
Helenu	Helena	k1gFnSc4	Helena
a	a	k8xC	a
bratra	bratr	k1gMnSc4	bratr
Leopolda	Leopold	k1gMnSc4	Leopold
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
profesorem	profesor	k1gMnSc7	profesor
římského	římský	k2eAgNnSc2d1	římské
práva	právo	k1gNnSc2	právo
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Karlově	Karlův	k2eAgFnSc6d1	Karlova
a	a	k8xC	a
také	také	k6eAd1	také
její	její	k3xOp3gMnSc1	její
rektor	rektor	k1gMnSc1	rektor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c4	na
Akademické	akademický	k2eAgNnSc4d1	akademické
gymnázium	gymnázium	k1gNnSc4	gymnázium
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
stejné	stejný	k2eAgFnSc2d1	stejná
třídy	třída	k1gFnSc2	třída
přistoupil	přistoupit	k5eAaPmAgMnS	přistoupit
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
.	.	kIx.	.
</s>
<s>
Odmaturoval	odmaturovat	k5eAaPmAgMnS	odmaturovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
zapsal	zapsat	k5eAaPmAgMnS	zapsat
na	na	k7c4	na
Karlovu	Karlův	k2eAgFnSc4d1	Karlova
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
přednášky	přednáška	k1gFnPc4	přednáška
chemie	chemie	k1gFnSc2	chemie
<g/>
,	,	kIx,	,
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
matematiky	matematika	k1gFnSc2	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
zde	zde	k6eAd1	zde
dva	dva	k4xCgInPc4	dva
semestry	semestr	k1gInPc4	semestr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
odešel	odejít	k5eAaPmAgMnS	odejít
studovat	studovat	k5eAaImF	studovat
na	na	k7c4	na
University	universita	k1gFnPc4	universita
College	College	k1gFnPc2	College
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byl	být	k5eAaImAgInS	být
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
fyzikální	fyzikální	k2eAgFnSc4d1	fyzikální
chemii	chemie	k1gFnSc4	chemie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
v	v	k7c6	v
Rakousku-Uhersku	Rakousku-Uhersek	k1gInSc6	Rakousku-Uhersek
na	na	k7c6	na
nízké	nízký	k2eAgFnSc6d1	nízká
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
University	universita	k1gFnPc4	universita
College	College	k1gFnSc7	College
tehdy	tehdy	k6eAd1	tehdy
přednášel	přednášet	k5eAaImAgMnS	přednášet
například	například	k6eAd1	například
Sir	sir	k1gMnSc1	sir
William	William	k1gInSc4	William
Ramsay	Ramsaa	k1gFnSc2	Ramsaa
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
studiu	studio	k1gNnSc3	studio
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
experimentovat	experimentovat	k5eAaImF	experimentovat
co	co	k3yRnSc4	co
nejjednodušeji	jednoduše	k6eAd3	jednoduše
a	a	k8xC	a
laboratorní	laboratorní	k2eAgInSc4d1	laboratorní
deník	deník	k1gInSc4	deník
si	se	k3xPyFc3	se
vedl	vést	k5eAaImAgMnS	vést
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
bakaláře	bakalář	k1gMnSc2	bakalář
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
Bachelor	Bachelor	k1gInSc1	Bachelor
of	of	k?	of
Science	Science	k1gFnSc1	Science
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
k	k	k7c3	k
postgraduálnímu	postgraduální	k2eAgNnSc3d1	postgraduální
studiu	studio	k1gNnSc3	studio
u	u	k7c2	u
profesora	profesor	k1gMnSc2	profesor
Fredericka	Fredericko	k1gNnSc2	Fredericko
G.	G.	kA	G.
Donnana	Donnan	k1gMnSc4	Donnan
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
při	při	k7c6	při
přednáškách	přednáška	k1gFnPc6	přednáška
dělal	dělat	k5eAaImAgMnS	dělat
demonstrátora	demonstrátor	k1gMnSc4	demonstrátor
(	(	kIx(	(
<g/>
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
začínajícího	začínající	k2eAgMnSc4d1	začínající
asistenta	asistent	k1gMnSc4	asistent
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
profesorem	profesor	k1gMnSc7	profesor
Donnanem	Donnan	k1gMnSc7	Donnan
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c4	o
elektrochemii	elektrochemie	k1gFnSc4	elektrochemie
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
prázdninách	prázdniny	k1gFnPc6	prázdniny
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
domů	domů	k6eAd1	domů
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kvůli	kvůli	k7c3	kvůli
první	první	k4xOgFnSc3	první
světové	světový	k2eAgFnSc3d1	světová
válce	válka	k1gFnSc3	válka
musel	muset	k5eAaImAgMnS	muset
zůstat	zůstat	k5eAaPmF	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
profesora	profesor	k1gMnSc2	profesor
Štěrby-Böhma	Štěrby-Böhm	k1gMnSc2	Štěrby-Böhm
v	v	k7c6	v
Chemickém	chemický	k2eAgInSc6d1	chemický
ústavu	ústav	k1gInSc6	ústav
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1915	[number]	k4	1915
narukoval	narukovat	k5eAaPmAgMnS	narukovat
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tělesné	tělesný	k2eAgFnSc3d1	tělesná
slabosti	slabost	k1gFnSc3	slabost
jej	on	k3xPp3gMnSc4	on
přidělili	přidělit	k5eAaPmAgMnP	přidělit
k	k	k7c3	k
zdravotnímu	zdravotní	k2eAgInSc3d1	zdravotní
oddílu	oddíl	k1gInSc3	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Sloužil	sloužit	k5eAaImAgInS	sloužit
u	u	k7c2	u
28.	[number]	k4	28.
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
plukovní	plukovní	k2eAgFnSc4d1	plukovní
nemocnici	nemocnice	k1gFnSc4	nemocnice
v	v	k7c6	v
Táboře	Tábor	k1gInSc6	Tábor
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
lékárně	lékárna	k1gFnSc6	lékárna
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
lékárenský	lékárenský	k2eAgMnSc1d1	lékárenský
chemik	chemik	k1gMnSc1	chemik
a	a	k8xC	a
na	na	k7c6	na
rentgenovém	rentgenový	k2eAgNnSc6d1	rentgenové
oddělení	oddělení	k1gNnSc6	oddělení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
se	se	k3xPyFc4	se
s	s	k7c7	s
plukem	pluk	k1gInSc7	pluk
přesunul	přesunout	k5eAaPmAgMnS	přesunout
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Innsbrucku	Innsbruck	k1gInSc2	Innsbruck
<g/>
,	,	kIx,	,
Igls	Igls	k1gInSc1	Igls
<g/>
.	.	kIx.	.
</s>
<s>
Tady	tady	k6eAd1	tady
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c4	na
dizertační	dizertační	k2eAgFnSc4d1	dizertační
práci	práce	k1gFnSc4	práce
o	o	k7c6	o
elektroafinitě	elektroafinita	k1gFnSc6	elektroafinita
hliníku	hliník	k1gInSc2	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Onemocněl	onemocnět	k5eAaPmAgInS	onemocnět
však	však	k9	však
zánětem	zánět	k1gInSc7	zánět
středního	střední	k2eAgNnSc2d1	střední
ucha	ucho	k1gNnSc2	ucho
a	a	k8xC	a
odjel	odjet	k5eAaPmAgMnS	odjet
se	se	k3xPyFc4	se
léčit	léčit	k5eAaImF	léčit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Práce	práce	k1gFnPc1	práce
===	===	k?	===
</s>
</p>
<p>
<s>
Rigorózní	rigorózní	k2eAgFnSc4d1	rigorózní
zkoušku	zkouška	k1gFnSc4	zkouška
složil	složit	k5eAaPmAgMnS	složit
27.	[number]	k4	27.
června	červen	k1gInSc2	červen
1918	[number]	k4	1918
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
26.	[number]	k4	26.
září	září	k1gNnSc2	září
byl	být	k5eAaImAgMnS	být
promován	promovat	k5eAaBmNgMnS	promovat
doktorem	doktor	k1gMnSc7	doktor
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
oponenty	oponent	k1gMnPc7	oponent
byl	být	k5eAaImAgMnS	být
i	i	k9	i
profesor	profesor	k1gMnSc1	profesor
Bohumil	Bohumil	k1gMnSc1	Bohumil
Kučera	Kučera	k1gMnSc1	Kučera
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
Heyrovského	Heyrovský	k2eAgMnSc4d1	Heyrovský
upozornil	upozornit	k5eAaPmAgMnS	upozornit
na	na	k7c4	na
obecné	obecný	k2eAgNnSc4d1	obecné
otázky	otázka	k1gFnPc4	otázka
elektrokapilarity	elektrokapilarita	k1gFnSc2	elektrokapilarita
a	a	k8xC	a
na	na	k7c4	na
neshody	neshoda	k1gFnPc4	neshoda
hodnot	hodnota	k1gFnPc2	hodnota
povrchového	povrchový	k2eAgNnSc2d1	povrchové
napětí	napětí	k1gNnSc2	napětí
měřeného	měřený	k2eAgInSc2d1	měřený
jeho	jeho	k3xOp3gFnSc7	jeho
metodou	metoda	k1gFnSc7	metoda
a	a	k8xC	a
klasickou	klasický	k2eAgFnSc7d1	klasická
metodou	metoda	k1gFnSc7	metoda
francouzského	francouzský	k2eAgMnSc2d1	francouzský
fyzika	fyzik	k1gMnSc2	fyzik
Gabriela	Gabriel	k1gMnSc2	Gabriel
Lippmana	Lippman	k1gMnSc2	Lippman
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
zkoumání	zkoumání	k1gNnSc3	zkoumání
Kučerových	Kučerových	k2eAgInPc2d1	Kučerových
výsledků	výsledek	k1gInPc2	výsledek
měření	měření	k1gNnSc2	měření
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
objevu	objev	k1gInSc3	objev
polarografie	polarografie	k1gFnSc2	polarografie
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
podzimního	podzimní	k2eAgInSc2d1	podzimní
semestru	semestr	k1gInSc2	semestr
1918	[number]	k4	1918
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
asistent	asistent	k1gMnSc1	asistent
profesora	profesor	k1gMnSc2	profesor
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Braunera	Brauner	k1gMnSc2	Brauner
na	na	k7c6	na
Ústavu	ústav	k1gInSc6	ústav
analytické	analytický	k2eAgFnSc2d1	analytická
chemie	chemie	k1gFnSc2	chemie
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
studiu	studio	k1gNnSc3	studio
ionů	ion	k1gInPc2	ion
a	a	k8xC	a
sloučenin	sloučenina	k1gFnPc2	sloučenina
hliníku	hliník	k1gInSc2	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgInS	napsat
o	o	k7c6	o
tomto	tento	k3xDgNnSc6	tento
tématu	téma	k1gNnSc6	téma
šest	šest	k4xCc1	šest
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
publikací	publikace	k1gFnPc2	publikace
<g/>
,	,	kIx,	,
tři	tři	k4xCgFnPc1	tři
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
vyšly	vyjít	k5eAaPmAgFnP	vyjít
v	v	k7c6	v
renomovaném	renomovaný	k2eAgInSc6d1	renomovaný
časopise	časopis	k1gInSc6	časopis
Journal	Journal	k1gFnSc2	Journal
of	of	k?	of
the	the	k?	the
Chemical	Chemical	k1gFnSc2	Chemical
Society	societa	k1gFnSc2	societa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1920	[number]	k4	1920
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgNnP	být
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
prodloužena	prodloužen	k2eAgFnSc1d1	prodloužena
smlouva	smlouva	k1gFnSc1	smlouva
na	na	k7c4	na
post	post	k1gInSc4	post
Braunerova	Braunerův	k2eAgMnSc2d1	Braunerův
asistenta	asistent	k1gMnSc2	asistent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
přednášet	přednášet	k5eAaImF	přednášet
nový	nový	k2eAgInSc4d1	nový
obor	obor	k1gInSc4	obor
<g/>
,	,	kIx,	,
fyzikální	fyzikální	k2eAgFnSc4d1	fyzikální
chemii	chemie	k1gFnSc4	chemie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1920	[number]	k4	1920
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
docentem	docent	k1gMnSc7	docent
<g/>
.	.	kIx.	.
</s>
<s>
Habilitační	habilitační	k2eAgFnSc4d1	habilitační
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
názvem	název	k1gInSc7	název
Kyselina	kyselina	k1gFnSc1	kyselina
hlinitá	hlinitý	k2eAgFnSc1d1	hlinitá
<g/>
.	.	kIx.	.
</s>
<s>
Příspěvek	příspěvek	k1gInSc1	příspěvek
k	k	k7c3	k
teorii	teorie	k1gFnSc3	teorie
amfoterních	amfoterní	k2eAgInPc2d1	amfoterní
elektrolytů	elektrolyt	k1gInPc2	elektrolyt
<g/>
,	,	kIx,	,
tvořily	tvořit	k5eAaImAgInP	tvořit
výsledky	výsledek	k1gInPc1	výsledek
jeho	on	k3xPp3gNnSc2	on
bádání	bádání	k1gNnSc2	bádání
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1913	[number]	k4	1913
až	až	k6eAd1	až
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
získal	získat	k5eAaPmAgMnS	získat
za	za	k7c4	za
stejnou	stejný	k2eAgFnSc4d1	stejná
práci	práce	k1gFnSc4	práce
titul	titul	k1gInSc4	titul
doktora	doktor	k1gMnSc2	doktor
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
DSc	DSc	k1gFnSc1	DSc
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
na	na	k7c6	na
londýnské	londýnský	k2eAgFnSc6d1	londýnská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1922	[number]	k4	1922
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
mimořádným	mimořádný	k2eAgMnSc7d1	mimořádný
a	a	k8xC	a
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1926	[number]	k4	1926
řádným	řádný	k2eAgMnSc7d1	řádný
profesorem	profesor	k1gMnSc7	profesor
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
chemie	chemie	k1gFnSc2	chemie
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Karlově	Karlův	k2eAgFnSc6d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
šestiměsíční	šestiměsíční	k2eAgFnSc4d1	šestiměsíční
stáž	stáž	k1gFnSc4	stáž
na	na	k7c6	na
Sorbonně	Sorbonna	k1gFnSc6	Sorbonna
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	let	k1gInPc6	let
1922	[number]	k4	1922
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
řídil	řídit	k5eAaImAgInS	řídit
Ústav	ústav	k1gInSc1	ústav
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
chemie	chemie	k1gFnSc2	chemie
Přírodovědecké	přírodovědecký	k2eAgFnSc2d1	Přírodovědecká
fakulty	fakulta	k1gFnSc2	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
založil	založit	k5eAaPmAgInS	založit
společně	společně	k6eAd1	společně
s	s	k7c7	s
Emilem	Emil	k1gMnSc7	Emil
Votočkem	Votoček	k1gMnSc7	Votoček
časopis	časopis	k1gInSc4	časopis
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
vycházely	vycházet	k5eAaImAgFnP	vycházet
československé	československý	k2eAgInPc4d1	československý
vědecké	vědecký	k2eAgInPc4d1	vědecký
články	článek	k1gInPc4	článek
ve	v	k7c6	v
světových	světový	k2eAgInPc6d1	světový
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
časopisu	časopis	k1gInSc2	časopis
byl	být	k5eAaImAgInS	být
dvojjazyčný	dvojjazyčný	k2eAgInSc1d1	dvojjazyčný
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Collection	Collection	k1gInSc1	Collection
of	of	k?	of
Czechoslovak	Czechoslovak	k1gInSc1	Czechoslovak
Chemical	Chemical	k1gMnSc1	Chemical
Communications	Communications	k1gInSc1	Communications
a	a	k8xC	a
francouzsky	francouzsky	k6eAd1	francouzsky
Collection	Collection	k1gInSc1	Collection
des	des	k1gNnSc1	des
travaux	travaux	k1gInSc1	travaux
chimiques	chimiques	k1gMnSc1	chimiques
Tchécoslovaques	Tchécoslovaques	k1gMnSc1	Tchécoslovaques
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
sérii	série	k1gFnSc4	série
přednášek	přednáška	k1gFnPc2	přednáška
v	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Objev	objevit	k5eAaPmRp2nS	objevit
polarografie	polarografie	k1gFnSc2	polarografie
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
napadlo	napadnout	k5eAaPmAgNnS	napadnout
Heyrovského	Heyrovský	k2eAgInSc2d1	Heyrovský
měřit	měřit	k5eAaImF	měřit
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
kapkovou	kapkový	k2eAgFnSc7d1	kapková
elektrodou	elektroda	k1gFnSc7	elektroda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
závislosti	závislost	k1gFnPc1	závislost
(	(	kIx(	(
<g/>
polarografické	polarografický	k2eAgFnPc1d1	polarografická
křivky	křivka	k1gFnPc1	křivka
<g/>
)	)	kIx)	)
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
10.	[number]	k4	10.
února	únor	k1gInSc2	únor
1922	[number]	k4	1922
při	při	k7c6	při
elektrolýze	elektrolýza	k1gFnSc6	elektrolýza
roztoku	roztok	k1gInSc2	roztok
chloridu	chlorid	k1gInSc2	chlorid
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
mezi	mezi	k7c4	mezi
kapkovou	kapkový	k2eAgFnSc4d1	kapková
elektrodu	elektroda	k1gFnSc4	elektroda
a	a	k8xC	a
potenciometr	potenciometr	k1gInSc4	potenciometr
zařadil	zařadit	k5eAaPmAgMnS	zařadit
galvanometr	galvanometr	k1gInSc4	galvanometr
<g/>
.	.	kIx.	.
</s>
<s>
Metoda	metoda	k1gFnSc1	metoda
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
při	při	k7c6	při
zkoumání	zkoumání	k1gNnSc6	zkoumání
elektrolýzy	elektrolýza	k1gFnSc2	elektrolýza
se	s	k7c7	s
rtuťovou	rtuťový	k2eAgFnSc7d1	rtuťová
kapkovou	kapkový	k2eAgFnSc7d1	kapková
katodou	katoda	k1gFnSc7	katoda
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
byly	být	k5eAaImAgFnP	být
při	při	k7c6	při
objevu	objev	k1gInSc6	objev
z	z	k7c2	z
roztoku	roztok	k1gInSc2	roztok
deponovány	deponován	k2eAgInPc1d1	deponován
alkalické	alkalický	k2eAgInPc1d1	alkalický
kovy	kov	k1gInPc1	kov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnovém	říjnový	k2eAgNnSc6d1	říjnové
čísle	číslo	k1gNnSc6	číslo
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
časopisu	časopis	k1gInSc2	časopis
Chemické	chemický	k2eAgFnSc2d1	chemická
listy	lista	k1gFnSc2	lista
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
článek	článek	k1gInSc1	článek
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
objevu	objev	k1gInSc6	objev
nazvaný	nazvaný	k2eAgInSc4d1	nazvaný
Elektrolýza	elektrolýza	k1gFnSc1	elektrolýza
s	s	k7c7	s
rtuťovou	rtuťový	k2eAgFnSc7d1	rtuťová
kapkovou	kapkový	k2eAgFnSc7d1	kapková
metodou	metoda	k1gFnSc7	metoda
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
objevu	objev	k1gInSc6	objev
polarografie	polarografie	k1gFnSc2	polarografie
přednesl	přednést	k5eAaPmAgMnS	přednést
přednášku	přednáška	k1gFnSc4	přednáška
na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
Faradayovy	Faradayův	k2eAgFnSc2d1	Faradayova
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zaujalo	zaujmout	k5eAaPmAgNnS	zaujmout
japonského	japonský	k2eAgMnSc2d1	japonský
vědeckého	vědecký	k2eAgMnSc2d1	vědecký
pracovníka	pracovník	k1gMnSc2	pracovník
Masuzó	Masuzó	k1gFnSc2	Masuzó
Šikatu	Šikat	k1gInSc2	Šikat
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
přijel	přijet	k5eAaPmAgMnS	přijet
s	s	k7c7	s
metodou	metoda	k1gFnSc7	metoda
seznámit	seznámit	k5eAaPmF	seznámit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
japonským	japonský	k2eAgMnSc7d1	japonský
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
sestavil	sestavit	k5eAaPmAgInS	sestavit
originální	originální	k2eAgInSc1d1	originální
přístroj	přístroj	k1gInSc1	přístroj
pro	pro	k7c4	pro
automatický	automatický	k2eAgInSc4d1	automatický
záznam	záznam	k1gInSc4	záznam
křivek	křivka	k1gFnPc2	křivka
závislosti	závislost	k1gFnSc2	závislost
intenzity	intenzita	k1gFnSc2	intenzita
proudu	proud	k1gInSc2	proud
v	v	k7c6	v
napětí	napětí	k1gNnSc6	napětí
<g/>
,	,	kIx,	,
přístroj	přístroj	k1gInSc4	přístroj
nazvali	nazvat	k5eAaBmAgMnP	nazvat
polarograf	polarograf	k1gInSc4	polarograf
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
vybral	vybrat	k5eAaPmAgInS	vybrat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Šikatou	Šikata	k1gFnSc7	Šikata
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
vkládání	vkládání	k1gNnSc6	vkládání
elektrického	elektrický	k2eAgNnSc2d1	elektrické
napětí	napětí	k1gNnSc2	napětí
se	se	k3xPyFc4	se
kapková	kapkový	k2eAgFnSc1d1	kapková
elektroda	elektroda	k1gFnSc1	elektroda
elektrochemicky	elektrochemicky	k6eAd1	elektrochemicky
polarizuje	polarizovat	k5eAaImIp3nS	polarizovat
<g/>
.	.	kIx.	.
</s>
<s>
Sestavili	sestavit	k5eAaPmAgMnP	sestavit
ho	on	k3xPp3gMnSc4	on
pro	pro	k7c4	pro
urychlení	urychlení	k1gNnSc4	urychlení
ručního	ruční	k2eAgInSc2d1	ruční
záznamu	záznam	k1gInSc2	záznam
polarizačních	polarizační	k2eAgFnPc2d1	polarizační
křivek	křivka	k1gFnPc2	křivka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
návrhu	návrh	k1gInSc3	návrh
jej	on	k3xPp3gMnSc4	on
vyrobili	vyrobit	k5eAaPmAgMnP	vyrobit
mechanici	mechanik	k1gMnPc1	mechanik
fyzikálního	fyzikální	k2eAgInSc2d1	fyzikální
ústavu	ústav	k1gInSc2	ústav
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
Innemann	Innemann	k1gMnSc1	Innemann
a	a	k8xC	a
Peták	Peták	k1gMnSc1	Peták
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
se	se	k3xPyFc4	se
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
začal	začít	k5eAaPmAgInS	začít
polarograf	polarograf	k1gInSc1	polarograf
vyrábět	vyrábět	k5eAaImF	vyrábět
komerčně	komerčně	k6eAd1	komerčně
<g/>
,	,	kIx,	,
od	od	k7c2	od
30.	[number]	k4	30.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
do	do	k7c2	do
výroby	výroba	k1gFnSc2	výroba
zapojily	zapojit	k5eAaPmAgInP	zapojit
i	i	k9	i
zahraniční	zahraniční	k2eAgFnPc1d1	zahraniční
firmy	firma	k1gFnPc1	firma
<g/>
.	.	kIx.	.
</s>
<s>
Heyrovský	Heyrovský	k2eAgInSc1d1	Heyrovský
polarografii	polarografie	k1gFnSc6	polarografie
vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
a	a	k8xC	a
nasměroval	nasměrovat	k5eAaPmAgInS	nasměrovat
i	i	k9	i
pro	pro	k7c4	pro
praktické	praktický	k2eAgNnSc4d1	praktické
využití	využití	k1gNnSc4	využití
v	v	k7c6	v
chemické	chemický	k2eAgFnSc6d1	chemická
analýze	analýza	k1gFnSc6	analýza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
údajná	údajný	k2eAgFnSc1d1	údajná
kolaborace	kolaborace	k1gFnSc1	kolaborace
===	===	k?	===
</s>
</p>
<p>
<s>
Díky	dík	k1gInPc1	dík
své	svůj	k3xOyFgFnSc2	svůj
známosti	známost	k1gFnSc2	známost
s	s	k7c7	s
profesorem	profesor	k1gMnSc7	profesor
Johannem	Johann	k1gMnSc7	Johann
Böhmem	Böhm	k1gInSc7	Böhm
<g/>
,	,	kIx,	,
kterému	který	k3yRgNnSc3	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
pomohl	pomoct	k5eAaPmAgMnS	pomoct
přestoupit	přestoupit	k5eAaPmF	přestoupit
z	z	k7c2	z
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
na	na	k7c4	na
pražskou	pražský	k2eAgFnSc4d1	Pražská
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
v	v	k7c6	v
laboratořích	laboratoř	k1gFnPc6	laboratoř
chemického	chemický	k2eAgInSc2d1	chemický
ústavu	ústav	k1gInSc2	ústav
pracovat	pracovat	k5eAaImF	pracovat
i	i	k9	i
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
když	když	k8xS	když
byly	být	k5eAaImAgFnP	být
vysoké	vysoký	k2eAgFnPc1d1	vysoká
školy	škola	k1gFnPc1	škola
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
v	v	k7c6	v
polarografii	polarografie	k1gFnSc6	polarografie
zavedl	zavést	k5eAaPmAgInS	zavést
použití	použití	k1gNnSc4	použití
osciloskopu	osciloskop	k1gInSc2	osciloskop
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Forejtem	Forejt	k1gMnSc7	Forejt
tak	tak	k8xS	tak
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
oscilografickou	oscilografický	k2eAgFnSc4d1	oscilografický
polarografii	polarografie	k1gFnSc4	polarografie
střídavým	střídavý	k2eAgInSc7d1	střídavý
proudem	proud	k1gInSc7	proud
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
možnost	možnost	k1gFnSc1	možnost
vědecké	vědecký	k2eAgFnSc2d1	vědecká
práce	práce	k1gFnSc2	práce
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
vnímána	vnímat	k5eAaImNgFnS	vnímat
částí	část	k1gFnSc7	část
české	český	k2eAgFnSc2d1	Česká
veřejnosti	veřejnost	k1gFnSc2	veřejnost
jako	jako	k8xC	jako
kolaborace	kolaborace	k1gFnSc2	kolaborace
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
15.	[number]	k4	15.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
mu	on	k3xPp3gNnSc3	on
kolegové	kolega	k1gMnPc1	kolega
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
členy	člen	k1gInPc4	člen
revolučních	revoluční	k2eAgFnPc2d1	revoluční
gard	garda	k1gFnPc2	garda
<g/>
,	,	kIx,	,
zakázali	zakázat	k5eAaPmAgMnP	zakázat
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
fyzikálně-chemického	fyzikálněhemický	k2eAgInSc2d1	fyzikálně-chemický
ústavu	ústav	k1gInSc2	ústav
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
osudu	osud	k1gInSc6	osud
mělo	mít	k5eAaImAgNnS	mít
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
vedení	vedení	k1gNnSc1	vedení
Přírodovědecké	přírodovědecký	k2eAgFnSc2d1	Přírodovědecká
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
21.	[number]	k4	21.
června	červen	k1gInSc2	červen
nařídilo	nařídit	k5eAaPmAgNnS	nařídit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Heyrovský	Heyrovský	k2eAgMnSc1d1	Heyrovský
odevzdal	odevzdat	k5eAaPmAgMnS	odevzdat
klíče	klíč	k1gInPc4	klíč
od	od	k7c2	od
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
případu	případ	k1gInSc3	případ
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
vyšetřovací	vyšetřovací	k2eAgFnSc1d1	vyšetřovací
komise	komise	k1gFnSc1	komise
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
případ	případ	k1gInSc4	případ
ukončila	ukončit	k5eAaPmAgFnS	ukončit
až	až	k9	až
koncem	koncem	k7c2	koncem
ledna	leden	k1gInSc2	leden
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
stranu	strana	k1gFnSc4	strana
se	se	k3xPyFc4	se
postavili	postavit	k5eAaPmAgMnP	postavit
mnozí	mnohý	k2eAgMnPc1d1	mnohý
kolegové	kolega	k1gMnPc1	kolega
i	i	k8xC	i
žáci	žák	k1gMnPc1	žák
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
mu	on	k3xPp3gMnSc3	on
přitížilo	přitížit	k5eAaPmAgNnS	přitížit
členství	členství	k1gNnSc4	členství
ve	v	k7c6	v
Svazu	svaz	k1gInSc6	svaz
pro	pro	k7c4	pro
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
Němci	Němec	k1gMnPc7	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Argumentoval	argumentovat	k5eAaImAgMnS	argumentovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jiných	jiný	k2eAgFnPc6d1	jiná
organizacích	organizace	k1gFnPc6	organizace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Americký	americký	k2eAgInSc1d1	americký
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
English	English	k1gInSc1	English
Grammar	Grammara	k1gFnPc2	Grammara
School	School	k1gInSc1	School
<g/>
,	,	kIx,	,
British	British	k1gInSc1	British
Society	societa	k1gFnSc2	societa
<g/>
)	)	kIx)	)
a	a	k8xC	a
musel	muset	k5eAaImAgMnS	muset
tak	tak	k6eAd1	tak
prokazovat	prokazovat	k5eAaImF	prokazovat
"	"	kIx"	"
<g/>
objektivitu	objektivita	k1gFnSc4	objektivita
v	v	k7c6	v
kulturní	kulturní	k2eAgFnSc6d1	kulturní
spolupráci	spolupráce	k1gFnSc6	spolupráce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kauzou	kauza	k1gFnSc7	kauza
se	se	k3xPyFc4	se
zabývala	zabývat	k5eAaImAgFnS	zabývat
i	i	k9	i
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
skončila	skončit	k5eAaPmAgFnS	skončit
ji	on	k3xPp3gFnSc4	on
až	až	k6eAd1	až
11.	[number]	k4	11.
dubna	duben	k1gInSc2	duben
1946	[number]	k4	1946
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
požadovala	požadovat	k5eAaImAgFnS	požadovat
omluvu	omluva	k1gFnSc4	omluva
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
Heyrovský	Heyrovský	k2eAgMnSc1d1	Heyrovský
nejprve	nejprve	k6eAd1	nejprve
odmítal	odmítat	k5eAaImAgMnS	odmítat
a	a	k8xC	a
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1946	[number]	k4	1946
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
vzdal	vzdát	k5eAaPmAgMnS	vzdát
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
ČAVU	ČAVU	kA	ČAVU
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
po	po	k7c4	po
naléhání	naléhání	k1gNnSc4	naléhání
kolegů	kolega	k1gMnPc2	kolega
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
kompromisu	kompromis	k1gInSc3	kompromis
<g/>
.	.	kIx.	.
</s>
<s>
Heyrovský	Heyrovský	k2eAgInSc1d1	Heyrovský
rezignaci	rezignace	k1gFnSc3	rezignace
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
a	a	k8xC	a
16.	[number]	k4	16.
ledna	leden	k1gInSc2	leden
1947	[number]	k4	1947
akademie	akademie	k1gFnSc1	akademie
přijala	přijmout	k5eAaPmAgFnS	přijmout
jeho	on	k3xPp3gInSc4	on
dopis	dopis	k1gInSc4	dopis
s	s	k7c7	s
textem	text	k1gInSc7	text
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1950	[number]	k4	1950
se	se	k3xPyFc4	se
Heyrovský	Heyrovský	k2eAgInSc1d1	Heyrovský
zabýval	zabývat	k5eAaImAgInS	zabývat
oscilografickým	oscilografický	k2eAgInSc7d1	oscilografický
výzkumem	výzkum	k1gInSc7	výzkum
reverzibility	reverzibilita	k1gFnSc2	reverzibilita
dějů	děj	k1gInPc2	děj
na	na	k7c6	na
rtuťové	rtuťový	k2eAgFnSc6d1	rtuťová
kapilární	kapilární	k2eAgFnSc6d1	kapilární
elektrodě	elektroda	k1gFnSc6	elektroda
<g/>
,	,	kIx,	,
diferenciální	diferenciální	k2eAgFnSc7d1	diferenciální
polarografií	polarografie	k1gFnSc7	polarografie
s	s	k7c7	s
tryskající	tryskající	k2eAgFnSc7d1	tryskající
elektrodou	elektroda	k1gFnSc7	elektroda
a	a	k8xC	a
studiem	studio	k1gNnSc7	studio
kapacitních	kapacitní	k2eAgInPc2d1	kapacitní
jevů	jev	k1gInPc2	jev
vyvolaných	vyvolaný	k2eAgFnPc2d1	vyvolaná
adsorpcí	adsorpce	k1gFnPc2	adsorpce
různých	různý	k2eAgFnPc2d1	různá
látek	látka	k1gFnPc2	látka
na	na	k7c6	na
rtuťových	rtuťový	k2eAgFnPc6d1	rtuťová
elektrodách	elektroda	k1gFnPc6	elektroda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1951	[number]	k4	1951
<g/>
–	–	k?	–
<g/>
1957	[number]	k4	1957
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
místopředseda	místopředseda	k1gMnSc1	místopředseda
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
unie	unie	k1gFnSc2	unie
pro	pro	k7c4	pro
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
prezidentem	prezident	k1gMnSc7	prezident
a	a	k8xC	a
prvním	první	k4xOgInSc7	první
čestným	čestný	k2eAgInSc7d1	čestný
členem	člen	k1gInSc7	člen
Polarografické	polarografický	k2eAgFnSc2d1	polarografická
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS	pracovat
rovněž	rovněž	k9	rovněž
ve	v	k7c6	v
vládní	vládní	k2eAgFnSc6d1	vládní
komisi	komise	k1gFnSc6	komise
pro	pro	k7c4	pro
zřízení	zřízení	k1gNnSc4	zřízení
Československé	československý	k2eAgFnSc2d1	Československá
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Polarografický	polarografický	k2eAgInSc1d1	polarografický
ústav	ústav	k1gInSc1	ústav
===	===	k?	===
</s>
</p>
<p>
<s>
Samostatný	samostatný	k2eAgInSc1d1	samostatný
Polarografický	polarografický	k2eAgInSc1d1	polarografický
ústav	ústav	k1gInSc1	ústav
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
1.	[number]	k4	1.
dubna	duben	k1gInSc2	duben
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
čele	čelo	k1gNnSc6	čelo
stál	stát	k5eAaImAgMnS	stát
Heyrovský	Heyrovský	k2eAgMnSc1d1	Heyrovský
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
přejmenován	přejmenovat	k5eAaPmNgInS	přejmenovat
na	na	k7c4	na
Polarografický	polarografický	k2eAgInSc4d1	polarografický
ústav	ústav	k1gInSc4	ústav
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Heyrovského	Heyrovský	k2eAgNnSc2d1	Heyrovský
ČSAV	ČSAV	kA	ČSAV
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc1	název
Ústav	ústav	k1gInSc1	ústav
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
chemie	chemie	k1gFnSc2	chemie
a	a	k8xC	a
elektrochemie	elektrochemie	k1gFnSc2	elektrochemie
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Heyrovského	Heyrovský	k2eAgMnSc2d1	Heyrovský
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
čela	čelo	k1gNnSc2	čelo
ústavu	ústav	k1gInSc2	ústav
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
věnoval	věnovat	k5eAaPmAgMnS	věnovat
oscilografickému	oscilografický	k2eAgInSc3d1	oscilografický
výzkumu	výzkum	k1gInSc3	výzkum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
smrt	smrt	k1gFnSc4	smrt
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
40.	[number]	k4	40.
let	léto	k1gNnPc2	léto
jej	on	k3xPp3gMnSc4	on
provázely	provázet	k5eAaImAgInP	provázet
drobné	drobný	k2eAgInPc1d1	drobný
zdravotní	zdravotní	k2eAgInPc1d1	zdravotní
problémy	problém	k1gInPc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
měl	mít	k5eAaImAgInS	mít
slabý	slabý	k2eAgInSc1d1	slabý
záchvat	záchvat	k1gInSc1	záchvat
mozkové	mozkový	k2eAgFnSc2d1	mozková
mrtvice	mrtvice	k1gFnSc2	mrtvice
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
však	však	k9	však
nezanechal	zanechat	k5eNaPmAgMnS	zanechat
trvalé	trvalý	k2eAgInPc4d1	trvalý
následky	následek	k1gInPc4	následek
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
před	před	k7c7	před
udělením	udělení	k1gNnSc7	udělení
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
se	se	k3xPyFc4	se
léčil	léčit	k5eAaImAgMnS	léčit
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
,	,	kIx,	,
provázely	provázet	k5eAaImAgInP	provázet
jej	on	k3xPp3gInSc4	on
nepravidelnosti	nepravidelnost	k1gFnSc2	nepravidelnost
v	v	k7c6	v
krevním	krevní	k2eAgInSc6d1	krevní
oběhu	oběh	k1gInSc6	oběh
<g/>
,	,	kIx,	,
těžký	těžký	k2eAgInSc1d1	těžký
zánět	zánět	k1gInSc1	zánět
žil	žíla	k1gFnPc2	žíla
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
komplikace	komplikace	k1gFnPc1	komplikace
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
komplikace	komplikace	k1gFnPc1	komplikace
byly	být	k5eAaImAgFnP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
způsobeny	způsoben	k2eAgInPc1d1	způsoben
častou	častý	k2eAgFnSc7d1	častá
prací	práce	k1gFnSc7	práce
s	s	k7c7	s
rtutí	rtuť	k1gFnSc7	rtuť
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
Velikonoční	velikonoční	k2eAgNnSc4d1	velikonoční
pondělí	pondělí	k1gNnSc4	pondělí
27.	[number]	k4	27.
března	březen	k1gInSc2	březen
1967	[number]	k4	1967
ve	v	k7c6	v
státním	státní	k2eAgNnSc6d1	státní
sanatoriu	sanatorium	k1gNnSc6	sanatorium
na	na	k7c6	na
Smíchově	Smíchov	k1gInSc6	Smíchov
<g/>
.	.	kIx.	.
</s>
<s>
Pohřben	pohřben	k2eAgMnSc1d1	pohřben
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
Vyšehradském	vyšehradský	k2eAgInSc6d1	vyšehradský
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Publikace	publikace	k1gFnSc1	publikace
==	==	k?	==
</s>
</p>
<p>
<s>
Heyrovský	Heyrovský	k2eAgMnSc1d1	Heyrovský
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
cca	cca	kA	cca
130	[number]	k4	130
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
odborných	odborný	k2eAgInPc2d1	odborný
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
publikace	publikace	k1gFnPc4	publikace
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Použití	použití	k1gNnSc1	použití
polarografické	polarografický	k2eAgFnSc2d1	polarografická
metody	metoda	k1gFnSc2	metoda
v	v	k7c6	v
praktické	praktický	k2eAgFnSc6d1	praktická
chemii	chemie	k1gFnSc6	chemie
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Polarographie	Polarographie	k1gFnSc1	Polarographie
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
praktickej	praktickej	k?	praktickej
polarografie	polarografie	k1gFnSc2	polarografie
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
s	s	k7c7	s
P.	P.	kA	P.
Zumanem	Zuman	k1gMnSc7	Zuman
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Oscilografická	oscilografický	k2eAgFnSc1d1	oscilografický
polarografie	polarografie	k1gFnSc1	polarografie
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
s	s	k7c7	s
J.	J.	kA	J.
Forejtem	Forejt	k1gMnSc7	Forejt
<g/>
)	)	kIx)	)
a	a	k8xC	a
Základy	základ	k1gInPc1	základ
polarografie	polarografie	k1gFnSc2	polarografie
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
s	s	k7c7	s
J.	J.	kA	J.
Kůtou	Kůta	k1gFnSc7	Kůta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Učebnice	učebnice	k1gFnSc1	učebnice
Základy	základ	k1gInPc4	základ
polarografie	polarografie	k1gFnSc1	polarografie
byla	být	k5eAaImAgFnS	být
přeložena	přeložit	k5eAaPmNgFnS	přeložit
do	do	k7c2	do
několika	několik	k4yIc2	několik
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Rodina	rodina	k1gFnSc1	rodina
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1926	[number]	k4	1926
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Kořánovou	Kořánová	k1gFnSc7	Kořánová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
potkali	potkat	k5eAaPmAgMnP	potkat
v	v	k7c6	v
Táboře	Tábor	k1gInSc6	Tábor
během	během	k7c2	během
jeho	jeho	k3xOp3gFnSc2	jeho
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
matky	matka	k1gFnSc2	matka
Marie	Maria	k1gFnSc2	Maria
Kořánové	Kořánové	k2eAgMnSc1d1	Kořánové
Vladimír	Vladimír	k1gMnSc1	Vladimír
Kořán	kořán	k1gInSc4	kořán
byl	být	k5eAaImAgMnS	být
bratrancem	bratranec	k1gMnSc7	bratranec
Leopolda	Leopold	k1gMnSc2	Leopold
Heyrovského	Heyrovský	k2eAgMnSc2d1	Heyrovský
a	a	k8xC	a
ředitelem	ředitel	k1gMnSc7	ředitel
pivovaru	pivovar	k1gInSc2	pivovar
Budvar	budvar	k1gInSc1	budvar
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
šla	jít	k5eAaImAgFnS	jít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
studovat	studovat	k5eAaImF	studovat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
Filosofickou	filosofický	k2eAgFnSc4d1	filosofická
fakultu	fakulta	k1gFnSc4	fakulta
a	a	k8xC	a
rodinu	rodina	k1gFnSc4	rodina
Heyrovských	Heyrovský	k2eAgInPc2d1	Heyrovský
navštěvovala	navštěvovat	k5eAaImAgNnP	navštěvovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Jitka	Jitka	k1gFnSc1	Jitka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
chemii	chemie	k1gFnSc4	chemie
na	na	k7c4	na
PřF	PřF	k1gFnSc4	PřF
UK	UK	kA	UK
a	a	k8xC	a
poté	poté	k6eAd1	poté
pracovala	pracovat	k5eAaImAgFnS	pracovat
v	v	k7c6	v
potravinářském	potravinářský	k2eAgInSc6d1	potravinářský
výzkumu	výzkum	k1gInSc6	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Michael	Michael	k1gMnSc1	Michael
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dostal	dostat	k5eAaPmAgMnS	dostat
jméno	jméno	k1gNnSc4	jméno
po	po	k7c4	po
Heyrovského	Heyrovský	k2eAgMnSc4d1	Heyrovský
vzoru	vzor	k1gInSc6	vzor
–	–	k?	–
Michaelu	Michael	k1gMnSc3	Michael
Faradayovi	Faradaya	k1gMnSc3	Faradaya
<g/>
.	.	kIx.	.
</s>
<s>
Michael	Michael	k1gMnSc1	Michael
Heyrovský	Heyrovský	k2eAgMnSc1d1	Heyrovský
rovněž	rovněž	k9	rovněž
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
chemii	chemie	k1gFnSc4	chemie
na	na	k7c4	na
PřF	PřF	k1gFnSc4	PřF
UK	UK	kA	UK
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Cambridgi	Cambridge	k1gFnSc6	Cambridge
u	u	k7c2	u
laureáta	laureát	k1gMnSc2	laureát
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
Ronalda	Ronald	k1gMnSc2	Ronald
Norrishe	Norrish	k1gMnSc2	Norrish
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k8xS	jako
vědecký	vědecký	k2eAgMnSc1d1	vědecký
pracovník	pracovník	k1gMnSc1	pracovník
na	na	k7c4	na
Polarografický	polarografický	k2eAgInSc4d1	polarografický
ústav	ústav	k1gInSc4	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Manželka	manželka	k1gFnSc1	manželka
Marie	Marie	k1gFnSc1	Marie
později	pozdě	k6eAd2	pozdě
pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k9	jako
Heyrovského	Heyrovský	k2eAgMnSc4d1	Heyrovský
sekretářka	sekretářek	k1gMnSc4	sekretářek
v	v	k7c6	v
Polarografickém	polarografický	k2eAgInSc6d1	polarografický
ústavu	ústav	k1gInSc6	ústav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
i	i	k9	i
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
získal	získat	k5eAaPmAgInS	získat
Heyrovský	Heyrovský	k2eAgInSc1d1	Heyrovský
četná	četný	k2eAgNnPc4d1	četné
ocenění	ocenění	k1gNnPc4	ocenění
<g/>
,	,	kIx,	,
čestná	čestný	k2eAgNnPc4d1	čestné
členství	členství	k1gNnPc4	členství
ve	v	k7c6	v
vědeckých	vědecký	k2eAgFnPc6d1	vědecká
institucích	instituce	k1gFnPc6	instituce
či	či	k8xC	či
čestné	čestný	k2eAgInPc1d1	čestný
doktoráty	doktorát	k1gInPc1	doktorát
několika	několik	k4yIc2	několik
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
obdržel	obdržet	k5eAaPmAgMnS	obdržet
státní	státní	k2eAgFnSc4d1	státní
cenu	cena	k1gFnSc4	cena
I.	I.	kA	I.
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1955	[number]	k4	1955
a	a	k8xC	a
1960	[number]	k4	1960
získal	získat	k5eAaPmAgInS	získat
Řád	řád	k1gInSc1	řád
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
obdržel	obdržet	k5eAaPmAgMnS	obdržet
zlatou	zlatý	k2eAgFnSc4d1	zlatá
čestnou	čestný	k2eAgFnSc4d1	čestná
plaketu	plaketa	k1gFnSc4	plaketa
Za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
o	o	k7c4	o
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
lidstvo	lidstvo	k1gNnSc4	lidstvo
od	od	k7c2	od
ČSAV	ČSAV	kA	ČSAV
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
odhalena	odhalen	k2eAgFnSc1d1	odhalena
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
v	v	k7c6	v
Kařezu	Kařez	k1gInSc6	Kařez
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
často	často	k6eAd1	často
pobýval	pobývat	k5eAaImAgMnS	pobývat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
o	o	k7c6	o
Heyrovském	Heyrovský	k2eAgInSc6d1	Heyrovský
a	a	k8xC	a
polarografii	polarografie	k1gFnSc6	polarografie
natočil	natočit	k5eAaBmAgInS	natočit
Československý	československý	k2eAgInSc1d1	československý
státní	státní	k2eAgInSc1d1	státní
film	film	k1gInSc1	film
krátký	krátký	k2eAgInSc1d1	krátký
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
neozvučený	ozvučený	k2eNgInSc4d1	neozvučený
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
planetka	planetka	k1gFnSc1	planetka
3069	[number]	k4	3069
Heyrovský	Heyrovský	k2eAgInSc1d1	Heyrovský
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
pak	pak	k6eAd1	pak
kráter	kráter	k1gInSc1	kráter
Heyrovsky	Heyrovsky	k1gFnSc2	Heyrovsky
na	na	k7c6	na
odvrácené	odvrácený	k2eAgFnSc6d1	odvrácená
straně	strana	k1gFnSc6	strana
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
<g/>
Několik	několik	k4yIc4	několik
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
mu	on	k3xPp3gMnSc3	on
udělilo	udělit	k5eAaPmAgNnS	udělit
čestný	čestný	k2eAgInSc4d1	čestný
doktorát	doktorát	k1gInSc4	doktorát
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
<g/>
,	,	kIx,	,
Varšavská	varšavský	k2eAgFnSc1d1	Varšavská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Technická	technický	k2eAgFnSc1d1	technická
univerzita	univerzita	k1gFnSc1	univerzita
Drážďany	Drážďany	k1gInPc4	Drážďany
<g/>
,	,	kIx,	,
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Univerzita	univerzita	k1gFnSc1	univerzita
Johanna	Johann	k1gMnSc2	Johann
Wolfganga	Wolfgang	k1gMnSc2	Wolfgang
Goetheho	Goethe	k1gMnSc2	Goethe
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
a	a	k8xC	a
Université	Universita	k1gMnPc1	Universita
of	of	k?	of
Provence	Provence	k1gFnSc1	Provence
Aix-Marseille	Aix-Marseille	k1gFnSc1	Aix-Marseille
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
jej	on	k3xPp3gMnSc4	on
jmenovaly	jmenovat	k5eAaBmAgInP	jmenovat
čestným	čestný	k2eAgInSc7d1	čestný
členem	člen	k1gInSc7	člen
(	(	kIx(	(
<g/>
Americká	americký	k2eAgFnSc1d1	americká
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
Německá	německý	k2eAgFnSc1d1	německá
přírodovědecká	přírodovědecký	k2eAgFnSc1d1	Přírodovědecká
akademie	akademie	k1gFnSc1	akademie
Leopoldiny	Leopoldina	k1gFnSc2	Leopoldina
<g/>
,	,	kIx,	,
Royal	Royal	k1gMnSc1	Royal
Society	societa	k1gFnSc2	societa
a	a	k8xC	a
německé	německý	k2eAgFnSc2d1	německá
<g/>
,	,	kIx,	,
maďarské	maďarský	k2eAgFnSc2d1	maďarská
<g/>
,	,	kIx,	,
polské	polský	k2eAgFnSc2d1	polská
<g/>
,	,	kIx,	,
dánské	dánský	k2eAgFnSc2d1	dánská
<g/>
,	,	kIx,	,
sovětské	sovětský	k2eAgFnSc2d1	sovětská
a	a	k8xC	a
indické	indický	k2eAgFnSc2d1	indická
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čestným	čestný	k2eAgMnSc7d1	čestný
členem	člen	k1gMnSc7	člen
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
také	také	k9	také
v	v	k7c6	v
chemických	chemický	k2eAgFnPc6d1	chemická
společnostech	společnost	k1gFnPc6	společnost
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
Velké	velký	k2eAgFnSc3d1	velká
Británii	Británie	k1gFnSc3	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
též	též	k9	též
členem	člen	k1gMnSc7	člen
Královské	královský	k2eAgFnSc2d1	královská
české	český	k2eAgFnSc2d1	Česká
společnosti	společnost	k1gFnSc2	společnost
nauk	nauka	k1gFnPc2	nauka
<g/>
,	,	kIx,	,
České	český	k2eAgFnSc2d1	Česká
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
Československé	československý	k2eAgFnSc2d1	Československá
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
badatelské	badatelský	k2eAgFnSc2d1	badatelská
a	a	k8xC	a
akademikem	akademik	k1gMnSc7	akademik
ČSAV	ČSAV	kA	ČSAV
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
úmrtí	úmrtí	k1gNnSc6	úmrtí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
se	se	k3xPyFc4	se
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
začaly	začít	k5eAaPmAgFnP	začít
pořádat	pořádat	k5eAaImF	pořádat
debaty	debata	k1gFnPc4	debata
o	o	k7c6	o
elektrochemii	elektrochemie	k1gFnSc6	elektrochemie
s	s	k7c7	s
názvem	název	k1gInSc7	název
Heyrovský	Heyrovský	k2eAgInSc4d1	Heyrovský
Discussions	Discussions	k1gInSc4	Discussions
on	on	k3xPp3gMnSc1	on
Electrochemistry	Electrochemistr	k1gMnPc7	Electrochemistr
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Heyrovského	Heyrovský	k2eAgInSc2d1	Heyrovský
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
čestná	čestný	k2eAgFnSc1d1	čestná
plaketa	plaketa	k1gFnSc1	plaketa
ČSAV	ČSAV	kA	ČSAV
Za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
o	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
chemických	chemický	k2eAgFnPc2d1	chemická
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c4	na
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
byl	být	k5eAaImAgInS	být
nominován	nominovat	k5eAaBmNgInS	nominovat
celkově	celkově	k6eAd1	celkově
osmnáctkrát	osmnáctkrát	k6eAd1	osmnáctkrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oboru	obor	k1gInSc6	obor
chemie	chemie	k1gFnSc2	chemie
čtrnáctkrát	čtrnáctkrát	k6eAd1	čtrnáctkrát
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
<g/>
–	–	k?	–
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
jednou	jednou	k6eAd1	jednou
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
medicínu	medicína	k1gFnSc4	medicína
třikrát	třikrát	k6eAd1	třikrát
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
převzal	převzít	k5eAaPmAgMnS	převzít
10.	[number]	k4	10.
prosince	prosinec	k1gInSc2	prosinec
1959	[number]	k4	1959
za	za	k7c4	za
objev	objev	k1gInSc4	objev
a	a	k8xC	a
rozpracování	rozpracování	k1gNnSc4	rozpracování
analytické	analytický	k2eAgNnSc4d1	analytické
polarografické	polarografický	k2eAgFnPc4d1	polarografická
metody	metoda	k1gFnPc4	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tu	tu	k6eAd1	tu
jej	on	k3xPp3gInSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
předchozí	předchozí	k2eAgMnPc1d1	předchozí
laureáti	laureát	k1gMnPc1	laureát
Archer	Archra	k1gFnPc2	Archra
John	John	k1gMnSc1	John
Porter	porter	k1gInSc4	porter
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
Chandrasekhara	Chandrasekhar	k1gMnSc2	Chandrasekhar
Venkata	Venkat	k1gMnSc2	Venkat
Raman	Raman	k1gMnSc1	Raman
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
významní	významný	k2eAgMnPc1d1	významný
vědci	vědec	k1gMnPc1	vědec
<g/>
,	,	kIx,	,
nominovali	nominovat	k5eAaBmAgMnP	nominovat
jej	on	k3xPp3gMnSc4	on
také	také	k6eAd1	také
chemici	chemik	k1gMnPc1	chemik
ze	z	k7c2	z
slovenských	slovenský	k2eAgFnPc2d1	slovenská
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tradiční	tradiční	k2eAgFnSc6d1	tradiční
přednášce	přednáška	k1gFnSc6	přednáška
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
předání	předání	k1gNnSc2	předání
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
hovořil	hovořit	k5eAaImAgMnS	hovořit
o	o	k7c6	o
směrech	směr	k1gInPc6	směr
polarografického	polarografický	k2eAgInSc2d1	polarografický
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Výstava	výstava	k1gFnSc1	výstava
Příběh	příběh	k1gInSc1	příběh
kapky	kapka	k1gFnSc2	kapka
==	==	k?	==
</s>
</p>
<p>
<s>
Putovní	putovní	k2eAgFnSc1d1	putovní
výstava	výstava	k1gFnSc1	výstava
Příběh	příběh	k1gInSc4	příběh
kapky	kapka	k1gFnSc2	kapka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
jako	jako	k8xS	jako
připomínka	připomínka	k1gFnSc1	připomínka
50.	[number]	k4	50.
výročí	výročí	k1gNnSc4	výročí
udělení	udělení	k1gNnSc2	udělení
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
Jaroslavu	Jaroslav	k1gMnSc3	Jaroslav
Heyrovskému	Heyrovský	k2eAgMnSc3d1	Heyrovský
<g/>
.	.	kIx.	.
</s>
<s>
Navštívit	navštívit	k5eAaPmF	navštívit
ji	on	k3xPp3gFnSc4	on
mohli	moct	k5eAaImAgMnP	moct
lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
(	(	kIx(	(
<g/>
2x	[number]	k4	2x
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Brně	Brno	k1gNnSc6	Brno
(	(	kIx(	(
<g/>
2x	[number]	k4	2x
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
5x	[number]	k4	5x
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
,	,	kIx,	,
Jihlavě	Jihlava	k1gFnSc6	Jihlava
<g/>
,	,	kIx,	,
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
Děčíně	Děčín	k1gInSc6	Děčín
<g/>
,	,	kIx,	,
Teplicích	Teplice	k1gFnPc6	Teplice
<g/>
,	,	kIx,	,
Žďáru	Žďár	k1gInSc6	Žďár
n	n	k0	n
<g/>
.	.	kIx.	.
<g/>
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
,	,	kIx,	,
Železném	železný	k2eAgInSc6d1	železný
Brodu	Brod	k1gInSc6	Brod
<g/>
,	,	kIx,	,
Vítkově	Vítkov	k1gInSc6	Vítkov
<g/>
,	,	kIx,	,
Prostějově	Prostějov	k1gInSc6	Prostějov
<g/>
,	,	kIx,	,
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
<g/>
,	,	kIx,	,
Zlíně	Zlín	k1gInSc6	Zlín
a	a	k8xC	a
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
.	.	kIx.	.
</s>
<s>
Výstavu	výstava	k1gFnSc4	výstava
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
2016	[number]	k4	2016
zhlédlo	zhlédnout	k5eAaPmAgNnS	zhlédnout
22 500	[number]	k4	22 500
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
.	.	kIx.	.
</s>
<s>
Výstava	výstava	k1gFnSc1	výstava
si	se	k3xPyFc3	se
klade	klást	k5eAaImIp3nS	klást
si	se	k3xPyFc3	se
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
přiblížit	přiblížit	k5eAaPmF	přiblížit
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
osobnost	osobnost	k1gFnSc4	osobnost
Heyrovského	Heyrovský	k2eAgMnSc2d1	Heyrovský
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc4	jeho
objev	objev	k1gInSc4	objev
<g/>
,	,	kIx,	,
polarografii	polarografie	k1gFnSc4	polarografie
<g/>
.	.	kIx.	.
</s>
<s>
Autory	autor	k1gMnPc7	autor
expozice	expozice	k1gFnSc2	expozice
jsou	být	k5eAaImIp3nP	být
vědci	vědec	k1gMnPc1	vědec
z	z	k7c2	z
Ústavu	ústav	k1gInSc2	ústav
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
chemie	chemie	k1gFnSc2	chemie
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Heyrovského	Heyrovský	k2eAgMnSc2d1	Heyrovský
AV	AV	kA	AV
ČR	ČR	kA	ČR
Dr.	dr.	kA	dr.
Květoslava	Květoslava	k1gFnSc1	Květoslava
Stejskalová	Stejskalová	k1gFnSc1	Stejskalová
<g/>
,	,	kIx,	,
Dr.	dr.	kA	dr.
Michael	Michael	k1gMnSc1	Michael
Heyrovský	Heyrovský	k2eAgMnSc1d1	Heyrovský
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Heyrovského	Heyrovský	k2eAgMnSc2d1	Heyrovský
<g/>
,	,	kIx,	,
a	a	k8xC	a
prof.	prof.	kA	prof.
Robert	Robert	k1gMnSc1	Robert
Kalvoda	Kalvoda	k1gMnSc1	Kalvoda
<g/>
.	.	kIx.	.
</s>
<s>
Expozici	expozice	k1gFnSc4	expozice
tvoří	tvořit	k5eAaImIp3nS	tvořit
12	[number]	k4	12
plakátů	plakát	k1gInPc2	plakát
formátů	formát	k1gInPc2	formát
A0	A0	k1gFnPc7	A0
a	a	k8xC	a
A1	A1	k1gFnPc7	A1
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgNnPc6	který
jsou	být	k5eAaImIp3nP	být
umístěny	umístěn	k2eAgFnPc4d1	umístěna
fotografie	fotografia	k1gFnPc4	fotografia
života	život	k1gInSc2	život
a	a	k8xC	a
vědecké	vědecký	k2eAgFnSc2d1	vědecká
práce	práce	k1gFnSc2	práce
Heyrovského	Heyrovský	k2eAgInSc2d1	Heyrovský
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
polarografické	polarografický	k2eAgInPc4d1	polarografický
přístroje	přístroj	k1gInPc4	přístroj
<g/>
,	,	kIx,	,
skleněné	skleněný	k2eAgFnPc4d1	skleněná
polarografické	polarografický	k2eAgFnPc4d1	polarografická
nádobky	nádobka	k1gFnPc4	nádobka
<g/>
,	,	kIx,	,
ukázky	ukázka	k1gFnPc4	ukázka
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
publikací	publikace	k1gFnPc2	publikace
o	o	k7c4	o
polarografii	polarografie	k1gFnSc4	polarografie
a	a	k8xC	a
diapozitivy	diapozitiv	k1gInPc4	diapozitiv
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výstavní	výstavní	k2eAgFnSc6d1	výstavní
místnosti	místnost	k1gFnSc6	místnost
jsou	být	k5eAaImIp3nP	být
promítány	promítán	k2eAgInPc1d1	promítán
popularizační	popularizační	k2eAgInPc1d1	popularizační
filmy	film	k1gInPc1	film
o	o	k7c6	o
Heyrovském	Heyrovský	k2eAgInSc6d1	Heyrovský
a	a	k8xC	a
výzkumu	výzkum	k1gInSc6	výzkum
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
polarografie	polarografie	k1gFnSc2	polarografie
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
50.	[number]	k4	50.
a	a	k8xC	a
60.	[number]	k4	60.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Nadační	nadační	k2eAgInSc1d1	nadační
fond	fond	k1gInSc1	fond
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Heyrovského	Heyrovský	k2eAgMnSc2d1	Heyrovský
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Nadační	nadační	k2eAgInSc1d1	nadační
fond	fond	k1gInSc1	fond
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Heyrovského	Heyrovský	k2eAgMnSc2d1	Heyrovský
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Heyrovský	Heyrovský	k2eAgInSc1d1	Heyrovský
Endowment	Endowment	k1gInSc1	Endowment
Fund	fund	k1gInSc1	fund
<g/>
)	)	kIx)	)
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
10.	[number]	k4	10.
září	září	k1gNnSc6	září
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
zakladatele	zakladatel	k1gMnPc4	zakladatel
patřily	patřit	k5eAaImAgFnP	patřit
Heyrovského	Heyrovský	k2eAgInSc2d1	Heyrovský
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
Mgr.	Mgr.	kA	Mgr.
Jitka	Jitka	k1gFnSc1	Jitka
Černá	Černá	k1gFnSc1	Černá
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Heyrovská	Heyrovský	k2eAgFnSc1d1	Heyrovská
a	a	k8xC	a
Dr.	dr.	kA	dr.
Michael	Michael	k1gMnSc1	Michael
Heyrovský	Heyrovský	k2eAgMnSc1d1	Heyrovský
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
náplní	náplň	k1gFnSc7	náplň
fondu	fond	k1gInSc2	fond
je	být	k5eAaImIp3nS	být
pomoc	pomoc	k1gFnSc4	pomoc
při	při	k7c6	při
vyhledávání	vyhledávání	k1gNnSc6	vyhledávání
nadaných	nadaný	k2eAgMnPc2d1	nadaný
středoškoláků	středoškolák	k1gMnPc2	středoškolák
a	a	k8xC	a
podpora	podpora	k1gFnSc1	podpora
při	při	k7c6	při
jejich	jejich	k3xOp3gInSc6	jejich
odborném	odborný	k2eAgInSc6d1	odborný
i	i	k8xC	i
osobním	osobní	k2eAgInSc6d1	osobní
rozvoji	rozvoj	k1gInSc6	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gFnPc4	jeho
další	další	k2eAgFnPc4d1	další
aktivity	aktivita	k1gFnPc4	aktivita
patří	patřit	k5eAaImIp3nS	patřit
propagace	propagace	k1gFnSc1	propagace
odkazu	odkaz	k1gInSc2	odkaz
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Heyrovského	Heyrovský	k2eAgMnSc2d1	Heyrovský
<g/>
.	.	kIx.	.
</s>
<s>
Uděluje	udělovat	k5eAaImIp3nS	udělovat
Cenu	cena	k1gFnSc4	cena
Nadačního	nadační	k2eAgInSc2d1	nadační
fondu	fond	k1gInSc2	fond
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Heyrovského	Heyrovský	k2eAgMnSc2d1	Heyrovský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Instituce	instituce	k1gFnSc2	instituce
nesoucí	nesoucí	k2eAgMnSc1d1	nesoucí
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
==	==	k?	==
</s>
</p>
<p>
<s>
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Heyrovského	Heyrovský	k2eAgMnSc2d1	Heyrovský
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
13	[number]	k4	13
</s>
</p>
<p>
<s>
Střední	střední	k2eAgFnSc1d1	střední
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
škola	škola	k1gFnSc1	škola
chemická	chemický	k2eAgFnSc1d1	chemická
akademika	akademik	k1gMnSc4	akademik
Heyrovského	Heyrovský	k2eAgMnSc4d1	Heyrovský
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
chemie	chemie	k1gFnSc2	chemie
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Heyrovského	Heyrovský	k2eAgMnSc2d1	Heyrovský
AV	AV	kA	AV
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
ZŠ	ZŠ	kA	ZŠ
Heyrovského	Heyrovský	k2eAgInSc2d1	Heyrovský
v	v	k7c6	v
Brně-Bystrci	Brně-Bystrec	k1gInSc6	Brně-Bystrec
(	(	kIx(	(
<g/>
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
podle	podle	k7c2	podle
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc2	jenž
sídlí	sídlet	k5eAaImIp3nP	sídlet
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ZŠ	ZŠ	kA	ZŠ
Heyrovského	Heyrovský	k2eAgInSc2d1	Heyrovský
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
(	(	kIx(	(
<g/>
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
podle	podle	k7c2	podle
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc2	jenž
sídlí	sídlet	k5eAaImIp3nP	sídlet
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ZŠ	ZŠ	kA	ZŠ
Heyrovského	Heyrovský	k2eAgInSc2d1	Heyrovský
v	v	k7c6	v
Chomutově	Chomutov	k1gInSc6	Chomutov
(	(	kIx(	(
<g/>
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
podle	podle	k7c2	podle
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc2	jenž
sídlí	sídlet	k5eAaImIp3nP	sídlet
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BENEŠOVÁ	Benešová	k1gFnSc1	Benešová
<g/>
,	,	kIx,	,
Olga	Olga	k1gFnSc1	Olga
<g/>
.	.	kIx.	.
</s>
<s>
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
:	:	kIx,	:
historie	historie	k1gFnSc1	historie
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
nadace	nadace	k1gFnSc2	nadace
:	:	kIx,	:
laureáti	laureát	k1gMnPc1	laureát
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
1901	[number]	k4	1901
<g/>
–	–	k?	–
<g/>
1996	[number]	k4	1996
:	:	kIx,	:
čeští	český	k2eAgMnPc1d1	český
laureáti	laureát	k1gMnPc1	laureát
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Heyrovský	Heyrovský	k2eAgMnSc1d1	Heyrovský
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Psychiatrické	psychiatrický	k2eAgNnSc1d1	psychiatrické
centrum	centrum	k1gNnSc1	centrum
<g/>
,	,	kIx,	,
1996.	[number]	k4	1996.
91	[number]	k4	91
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-85121-88-3	[number]	k4	80-85121-88-3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HEYROVSKÝ	HEYROVSKÝ	kA	HEYROVSKÝ
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Heyrovský	Heyrovský	k2eAgMnSc1d1	Heyrovský
on	on	k3xPp3gMnSc1	on
Polarography	Polarograph	k1gInPc5	Polarograph
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Union	union	k1gInSc1	union
of	of	k?	of
Czechoslovak	Czechoslovak	k1gInSc1	Czechoslovak
Mathematicians	Mathematicians	k1gInSc1	Mathematicians
and	and	k?	and
Physicists	Physicists	k1gInSc1	Physicists
<g/>
,	,	kIx,	,
1990.	[number]	k4	1990.
91	[number]	k4	91
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7015-132-3	[number]	k4	80-7015-132-3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JINDRA	Jindra	k1gMnSc1	Jindra
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Heyrovský	Heyrovský	k2eAgMnSc1d1	Heyrovský
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
(	(	kIx(	(
<g/>
Johann	Johann	k1gMnSc1	Johann
<g/>
)	)	kIx)	)
Böhm	Böhm	k1gMnSc1	Böhm
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgInPc1d1	chemický
listy	list	k1gInPc1	list
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
103	[number]	k4	103
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
.	.	kIx.	.
894-897	[number]	k4	894-897
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1213-7103	[number]	k4	1213-7103
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JINDRA	Jindra	k1gMnSc1	Jindra
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
a	a	k8xC	a
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
a	a	k8xC	a
chemii	chemie	k1gFnSc4	chemie
1901	[number]	k4	1901
<g/>
–	–	k?	–
<g/>
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
39	[number]	k4	39
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
.	.	kIx.	.
1-24	[number]	k4	1-24
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0300-4414	[number]	k4	0300-4414
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KALVODA	Kalvoda	k1gMnSc1	Kalvoda
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
Hrst	hrst	k1gFnSc1	hrst
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
na	na	k7c4	na
pana	pan	k1gMnSc4	pan
profesora	profesor	k1gMnSc4	profesor
Heyrovského	Heyrovský	k2eAgMnSc4d1	Heyrovský
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
polarografii	polarografie	k1gFnSc4	polarografie
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgInPc1d1	chemický
listy	list	k1gInPc1	list
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
103	[number]	k4	103
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
.	.	kIx.	.
880-888	[number]	k4	880-888
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1213-7103	[number]	k4	1213-7103
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KORYTA	koryto	k1gNnPc1	koryto
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Heyrovský	Heyrovský	k2eAgMnSc1d1	Heyrovský
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1990.	[number]	k4	1990.
168	[number]	k4	168
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7023-058-4	[number]	k4	80-7023-058-4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osobnosti	osobnost	k1gFnPc1	osobnost
-	-	kIx~	-
Česko	Česko	k1gNnSc1	Česko
:	:	kIx,	:
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2008.	[number]	k4	2008.
823	[number]	k4	823
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-7360-796-8	[number]	k4	978-80-7360-796-8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
221.	[number]	k4	221.
</s>
</p>
<p>
<s>
POSPÍŠIL	Pospíšil	k1gMnSc1	Pospíšil
<g/>
,	,	kIx,	,
Lubomír	Lubomír	k1gMnSc1	Lubomír
<g/>
;	;	kIx,	;
HROMADOVÁ	Hromadová	k1gFnSc1	Hromadová
<g/>
,	,	kIx,	,
Magdaléna	Magdaléna	k1gFnSc1	Magdaléna
<g/>
.	.	kIx.	.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Heyrovský	Heyrovský	k2eAgMnSc1d1	Heyrovský
-	-	kIx~	-
50.	[number]	k4	50.
výročí	výročí	k1gNnSc2	výročí
udělení	udělení	k1gNnSc2	udělení
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgInPc1d1	chemický
listy	list	k1gInPc1	list
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
103	[number]	k4	103
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
.	.	kIx.	.
900-901	[number]	k4	900-901
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1213-7103	[number]	k4	1213-7103
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SKRAMLÍK	SKRAMLÍK	kA	SKRAMLÍK
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Heyrovský	Heyrovský	k2eAgMnSc1d1	Heyrovský
byl	být	k5eAaImAgMnS	být
první	první	k4xOgInPc4	první
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc1	svět
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
.	.	kIx.	.
44-45	[number]	k4	44-45
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1214-9365	[number]	k4	1214-9365
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOMEŠ	Tomeš	k1gMnSc1	Tomeš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
XX	XX	kA	XX
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
:	:	kIx,	:
I.	I.	kA	I.
díl	díl	k1gInSc1	díl
:	:	kIx,	:
A	a	k9	a
<g/>
–	–	k?	–
<g/>
J.	J.	kA	J.
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
;	;	kIx,	;
Petr	Petr	k1gMnSc1	Petr
Meissner	Meissner	k1gMnSc1	Meissner
<g/>
,	,	kIx,	,
1999.	[number]	k4	1999.
634	[number]	k4	634
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7185-245-7	[number]	k4	80-7185-245-7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
455	[number]	k4	455
<g/>
–	–	k?	–
<g/>
456	[number]	k4	456
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Heyrovský	Heyrovský	k2eAgMnSc1d1	Heyrovský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Heyrovský	Heyrovský	k2eAgMnSc1d1	Heyrovský
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Heyrovský	Heyrovský	k2eAgMnSc1d1	Heyrovský
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Stránka	stránka	k1gFnSc1	stránka
komise	komise	k1gFnSc2	komise
Nobelových	Nobelových	k2eAgFnPc2d1	Nobelových
cen	cena	k1gFnPc2	cena
o	o	k7c4	o
Jaroslavu	Jaroslava	k1gFnSc4	Jaroslava
Heyrovském	Heyrovský	k2eAgNnSc6d1	Heyrovský
</s>
</p>
<p>
<s>
Výstava	výstava	k1gFnSc1	výstava
Příběh	příběh	k1gInSc1	příběh
kapky	kapka	k1gFnSc2	kapka
na	na	k7c6	na
webu	web	k1gInSc6	web
Ústavu	ústav	k1gInSc2	ústav
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
chemie	chemie	k1gFnSc2	chemie
J.	J.	kA	J.
Heyrovského	Heyrovský	k2eAgMnSc2d1	Heyrovský
AV	AV	kA	AV
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Nadační	nadační	k2eAgInSc1d1	nadační
fond	fond	k1gInSc1	fond
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Heyrovského	Heyrovský	k2eAgMnSc2d1	Heyrovský
</s>
</p>
<p>
<s>
Příběh	příběh	k1gInSc1	příběh
kapky	kapka	k1gFnSc2	kapka
Video	video	k1gNnSc1	video
o	o	k7c4	o
polarografii	polarografie	k1gFnSc4	polarografie
a	a	k8xC	a
Jaroslavu	Jaroslava	k1gFnSc4	Jaroslava
Heyrovském	Heyrovský	k2eAgInSc6d1	Heyrovský
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
PORT	porta	k1gFnPc2	porta
<g/>
,	,	kIx,	,
2.	[number]	k4	2.
12.	[number]	k4	12.
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Heyrovský	Heyrovský	k2eAgMnSc1d1	Heyrovský
a	a	k8xC	a
příběh	příběh	k1gInSc1	příběh
rtuťové	rtuťový	k2eAgFnSc2d1	rtuťová
kapky	kapka	k1gFnSc2	kapka
Zvukový	zvukový	k2eAgInSc1d1	zvukový
záznam	záznam	k1gInSc1	záznam
z	z	k7c2	z
pořadu	pořad	k1gInSc2	pořad
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Planetárium	planetárium	k1gNnSc1	planetárium
<g/>
,	,	kIx,	,
19.	[number]	k4	19.
11.	[number]	k4	11.
2011	[number]	k4	2011
</s>
</p>
<p>
<s>
Rozhovor	rozhovor	k1gInSc1	rozhovor
se	s	k7c7	s
synem	syn	k1gMnSc7	syn
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Heyrovského	Heyrovský	k2eAgInSc2d1	Heyrovský
Michaelem	Michael	k1gMnSc7	Michael
Heyrovským	Heyrovský	k2eAgMnSc7d1	Heyrovský
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Vstupte	vstoupit	k5eAaPmRp2nP	vstoupit
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
19.	[number]	k4	19.
11.	[number]	k4	11.
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
Pořad	pořad	k1gInSc1	pořad
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Všechny	všechen	k3xTgFnPc1	všechen
věci	věc	k1gFnPc1	věc
na	na	k7c6	na
světě	svět	k1gInSc6	svět
nejsou	být	k5eNaImIp3nP	být
krásné	krásný	k2eAgFnPc1d1	krásná
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
množství	množství	k1gNnSc1	množství
unikátních	unikátní	k2eAgFnPc2d1	unikátní
ukázek	ukázka	k1gFnPc2	ukázka
s	s	k7c7	s
hlasem	hlas	k1gInSc7	hlas
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Heyrovského	Heyrovský	k2eAgMnSc2d1	Heyrovský
včetně	včetně	k7c2	včetně
reportáže	reportáž	k1gFnSc2	reportáž
švédského	švédský	k2eAgInSc2d1	švédský
rozhlasu	rozhlas	k1gInSc2	rozhlas
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
z	z	k7c2	z
udělení	udělení	k1gNnSc2	udělení
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
</s>
</p>
