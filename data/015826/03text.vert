<s>
Ronit	ronit	k5eAaImF
Tiroš	Tiroš	k1gMnSc1
</s>
<s>
Ronit	ronit	k5eAaImF
Tirošר	Tirošר	k1gFnSc4
ת	ת	k?
Ronit	ronit	k5eAaImF
Tiroš	Tiroš	k1gMnSc1
na	na	k7c6
snímku	snímek	k1gInSc6
z	z	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
Kadima	Kadima	k1gFnSc1
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1953	#num#	k4
(	(	kIx(
<g/>
67	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Tel	tel	kA
Aviv	Aviv	k1gInSc1
<g/>
,	,	kIx,
Izrael	Izrael	k1gInSc1
Kneset	Kneseta	k1gFnPc2
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Telavivská	telavivský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Profese	profese	k1gFnSc1
</s>
<s>
politička	politička	k1gFnSc1
a	a	k8xC
vychovatelka	vychovatelka	k1gFnSc1
Commons	Commonsa	k1gFnPc2
</s>
<s>
Ronit	ronit	k5eAaImF
Tirosh	Tirosh	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ronit	ronit	k5eAaImF
Tiroš	Tiroš	k1gMnSc1
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
:	:	kIx,
ר	ר	k?
ת	ת	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
izraelská	izraelský	k2eAgFnSc1d1
politička	politička	k1gFnSc1
a	a	k8xC
bývalá	bývalý	k2eAgFnSc1d1
poslankyně	poslankyně	k1gFnSc1
Knesetu	Kneset	k1gInSc2
za	za	k7c4
stranu	strana	k1gFnSc4
Kadima	Kadimum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS
se	s	k7c7
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1953	#num#	k4
v	v	k7c6
Tel	tel	kA
Avivu	Aviv	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysokoškolské	vysokoškolský	k2eAgNnSc1d1
vzdělání	vzdělání	k1gNnSc1
bakalářského	bakalářský	k2eAgInSc2d1
typu	typ	k1gInSc2
v	v	k7c6
oboru	obor	k1gInSc6
arabská	arabský	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
a	a	k8xC
filozofie	filozofie	k1gFnSc1
získala	získat	k5eAaPmAgFnS
roku	rok	k1gInSc2
1977	#num#	k4
na	na	k7c6
Telavivské	telavivský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následoval	následovat	k5eAaImAgInS
magisterský	magisterský	k2eAgInSc1d1
titul	titul	k1gInSc1
ze	z	k7c2
vzdělávacího	vzdělávací	k2eAgInSc2d1
managementu	management	k1gInSc2
na	na	k7c6
téže	týž	k3xTgFnSc6,k3xDgFnSc6
škole	škola	k1gFnSc6
roku	rok	k1gInSc2
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
studuje	studovat	k5eAaImIp3nS
na	na	k7c6
Telavivské	telavivský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
doktorandském	doktorandský	k2eAgNnSc6d1
studiu	studio	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Žije	žít	k5eAaImIp3nS
v	v	k7c4
Ramat	Ramat	k1gInSc4
Ganu	Ganus	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
vdaná	vdaný	k2eAgFnSc1d1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
tři	tři	k4xCgFnPc4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sloužila	sloužit	k5eAaImAgFnS
v	v	k7c6
izraelské	izraelský	k2eAgFnSc6d1
armádě	armáda	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
dosáhla	dosáhnout	k5eAaPmAgFnS
hodnosti	hodnost	k1gFnPc4
seržantky	seržantka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hovoří	hovořit	k5eAaImIp3nS
hebrejsky	hebrejsky	k6eAd1
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
a	a	k8xC
arabsky	arabsky	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Politická	politický	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1988	#num#	k4
<g/>
–	–	k?
<g/>
1996	#num#	k4
byla	být	k5eAaImAgFnS
ředitelkou	ředitelka	k1gFnSc7
9	#num#	k4
<g/>
.	.	kIx.
střední	střední	k2eAgFnSc2d1
školy	škola	k1gFnSc2
v	v	k7c6
Tel	tel	kA
Avivu	Aviv	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
předtím	předtím	k6eAd1
působila	působit	k5eAaImAgFnS
jako	jako	k8xC,k8xS
učitelka	učitelka	k1gFnSc1
arabštiny	arabština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získala	získat	k5eAaPmAgFnS
tu	tu	k6eAd1
roku	rok	k1gInSc2
1992	#num#	k4
ocenění	ocenění	k1gNnSc4
za	za	k7c2
vynikajícího	vynikající	k2eAgMnSc2d1
ředitele	ředitel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1997	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
působila	působit	k5eAaImAgFnS
na	na	k7c6
telavivské	telavivský	k2eAgFnSc6d1
radnici	radnice	k1gFnSc6
jako	jako	k8xS,k8xC
ředitelka	ředitelka	k1gFnSc1
vzdělávacího	vzdělávací	k2eAgInSc2d1
odboru	odbor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
2000	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
Osobností	osobnost	k1gFnSc7
roku	rok	k1gInSc2
ve	v	k7c6
školství	školství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2001	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
pracovala	pracovat	k5eAaImAgFnS
na	na	k7c6
vysoké	vysoký	k2eAgFnSc6d1
úřední	úřední	k2eAgFnSc6d1
pozici	pozice	k1gFnSc6
na	na	k7c6
ministerstvu	ministerstvo	k1gNnSc6
školství	školství	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Do	do	k7c2
Knesetu	Kneset	k1gInSc2
nastoupila	nastoupit	k5eAaPmAgFnS
po	po	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgMnPc6,k3yQgMnPc6,k3yIgMnPc6
kandidovala	kandidovat	k5eAaImAgFnS
za	za	k7c4
stranu	strana	k1gFnSc4
Kadima	Kadimum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
volebním	volební	k2eAgNnSc6d1
období	období	k1gNnSc6
2006	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
byla	být	k5eAaImAgFnS
členkou	členka	k1gFnSc7
výboru	výbor	k1gInSc2
pro	pro	k7c4
televizi	televize	k1gFnSc4
a	a	k8xC
rozhlas	rozhlas	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
podvýboru	podvýbor	k1gInSc2
pro	pro	k7c4
boj	boj	k1gInSc4
s	s	k7c7
dopravní	dopravní	k2eAgFnSc7d1
nehodovostí	nehodovost	k1gFnSc7
<g/>
,	,	kIx,
zasedala	zasedat	k5eAaImAgFnS
ve	v	k7c6
výboru	výbor	k1gInSc6
pro	pro	k7c4
drogy	droga	k1gFnPc4
<g/>
,	,	kIx,
pro	pro	k7c4
otázky	otázka	k1gFnPc4
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
sociálních	sociální	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
a	a	k8xC
zdravotnictví	zdravotnictví	k1gNnSc2
nebo	nebo	k8xC
pro	pro	k7c4
vzdělávání	vzdělávání	k1gNnSc4
<g/>
,	,	kIx,
kulturu	kultura	k1gFnSc4
a	a	k8xC
sport	sport	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Silné	silný	k2eAgInPc1d1
zastoupení	zastoupení	k1gNnSc4
v	v	k7c6
parlamentních	parlamentní	k2eAgInPc6d1
výborech	výbor	k1gInPc6
si	se	k3xPyFc3
podržela	podržet	k5eAaPmAgFnS
i	i	k8xC
po	po	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opět	opět	k6eAd1
zasedla	zasednout	k5eAaPmAgFnS
ve	v	k7c6
výborech	výbor	k1gInPc6
pro	pro	k7c4
vzdělávání	vzdělávání	k1gNnSc4
<g/>
,	,	kIx,
kulturu	kultura	k1gFnSc4
a	a	k8xC
sport	sport	k1gInSc4
a	a	k8xC
pro	pro	k7c4
televizi	televize	k1gFnSc4
a	a	k8xC
rozhlas	rozhlas	k1gInSc4
<g/>
,	,	kIx,
dále	daleko	k6eAd2
ve	v	k7c6
finančním	finanční	k2eAgInSc6d1
výboru	výbor	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Ronit	ronit	k5eAaImF
Tiroš	Tiroš	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kneset	Kneset	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Kneset	Kneset	k1gInSc1
–	–	k?
Ronit	ronit	k5eAaImF
Tiroš	Tiroš	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Členové	člen	k1gMnPc1
osmnáctého	osmnáctý	k4xOgInSc2
Knesetu	Kneset	k1gInSc2
zvoleného	zvolený	k2eAgInSc2d1
roku	rok	k1gInSc2
2009	#num#	k4
Kadima	Kadimum	k1gNnSc2
</s>
<s>
Livni	Livn	k1gMnPc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Celner	Celner	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Mofaz	Mofaz	k1gInSc1
</s>
<s>
Icik	Icik	k6eAd1
</s>
<s>
Bar-On	Bar-On	k1gMnSc1
</s>
<s>
Šítrit	Šítrit	k1gInSc1
(	(	kIx(
<g/>
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
ha-Tnu	ha-Tn	k1gInSc2
<g/>
'	'	kIx"
<g/>
a	a	k8xC
<g/>
)	)	kIx)
</s>
<s>
Avraham	Avraham	k6eAd1
</s>
<s>
Dichter	Dichter	k1gInSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Dabbah	Dabbah	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Solodkin	Solodkin	k1gMnSc1
</s>
<s>
Jo	jo	k9
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
Chason	Chason	k1gInSc1
(	(	kIx(
<g/>
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
ha-Tnu	ha-Tn	k1gInSc2
<g/>
'	'	kIx"
<g/>
a	a	k8xC
<g/>
)	)	kIx)
</s>
<s>
Ezra	Ezra	k1gFnSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Hasson	Hasson	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Edri	Edri	k6eAd1
</s>
<s>
Aflalo	Aflat	k5eAaPmAgNnS,k5eAaBmAgNnS,k5eAaImAgNnS
(	(	kIx(
<g/>
pak	pak	k6eAd1
Du	Du	k?
<g/>
'	'	kIx"
<g/>
an	an	k?
<g/>
)	)	kIx)
</s>
<s>
Bielski	Bielski	k6eAd1
</s>
<s>
Tiroš	Tiroš	k1gMnSc1
</s>
<s>
Šaj	Šaj	k?
</s>
<s>
Mola	molo	k1gNnPc1
(	(	kIx(
<g/>
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
ha-Tnu	ha-Tn	k1gInSc2
<g/>
'	'	kIx"
<g/>
a	a	k8xC
<g/>
)	)	kIx)
</s>
<s>
Tiviajev	Tiviajet	k5eAaPmDgInS
(	(	kIx(
<g/>
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
ha-Tnu	ha-Tn	k1gInSc2
<g/>
'	'	kIx"
<g/>
a	a	k8xC
<g/>
)	)	kIx)
</s>
<s>
Wahabi	Wahabi	k1gNnSc1
(	(	kIx(
<g/>
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
ha-Tnu	ha-Tn	k1gInSc2
<g/>
'	'	kIx"
<g/>
a	a	k8xC
<g/>
)	)	kIx)
</s>
<s>
Adato	Adato	k1gNnSc4
(	(	kIx(
<g/>
přešla	přejít	k5eAaPmAgFnS
do	do	k7c2
ha-Tnu	ha-Tn	k1gInSc2
<g/>
'	'	kIx"
<g/>
a	a	k8xC
<g/>
)	)	kIx)
</s>
<s>
Plesner	Plesner	k1gMnSc1
</s>
<s>
Chermeš	Chermat	k5eAaPmIp2nS,k5eAaBmIp2nS
</s>
<s>
Jisra	Jisra	k1gFnSc1
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
Chason	Chason	k1gInSc1
</s>
<s>
Bibi	Bibi	k6eAd1
</s>
<s>
Šneler	Šneler	k1gMnSc1
</s>
<s>
Zu	Zu	k?
<g/>
'	'	kIx"
<g/>
arec	arec	k1gFnSc1
(	(	kIx(
<g/>
přešla	přejít	k5eAaPmAgFnS
do	do	k7c2
ha-Tnu	ha-Tn	k1gInSc2
<g/>
'	'	kIx"
<g/>
a	a	k8xC
<g/>
)	)	kIx)
</s>
<s>
Ramon	Ramona	k1gFnPc2
(	(	kIx(
<g/>
pak	pak	k6eAd1
Šamalov-Berkovič	Šamalov-Berkovič	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hanegbi	Hanegbi	k1gNnSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Abesadze	Abesadze	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Boim	Boim	k1gInSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Avital	Avital	k1gMnSc1
<g/>
)	)	kIx)
Likud	Likud	k1gMnSc1
</s>
<s>
Netanjahu	Netanjaha	k1gFnSc4
</s>
<s>
Sa	Sa	k?
<g/>
'	'	kIx"
<g/>
ar	ar	k1gInSc1
</s>
<s>
Erdan	Erdan	k1gMnSc1
</s>
<s>
Rivlin	Rivlin	k1gInSc1
</s>
<s>
Begin	Begin	k1gMnSc1
</s>
<s>
Kachlon	Kachlon	k1gInSc1
</s>
<s>
Šalom	šalom	k0
</s>
<s>
Ja	Ja	k?
<g/>
'	'	kIx"
<g/>
alon	alon	k1gMnSc1
</s>
<s>
Steinitz	Steinitz	k1gMnSc1
</s>
<s>
Nas	Nas	k?
</s>
<s>
Jisra	Jisra	k1gFnSc1
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
Kac	Kac	k1gMnPc2
</s>
<s>
Edelstein	Edelstein	k1gMnSc1
</s>
<s>
Livnat	Livnat	k5eAaPmF,k5eAaImF
</s>
<s>
Chajim	Chajim	k1gMnSc1
Kac	Kac	k1gMnSc1
</s>
<s>
Peled	Peled	k1gInSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Adamso	Adamsa	k1gFnSc5
<g/>
)	)	kIx)
</s>
<s>
Ejtan	Ejtan	k1gMnSc1
</s>
<s>
Meridor	Meridor	k1gMnSc1
</s>
<s>
Chotovely	Chotovela	k1gFnPc1
</s>
<s>
Gamli	Gamle	k1gFnSc3
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
</s>
<s>
Elkin	Elkin	k1gMnSc1
</s>
<s>
Levin	Levin	k1gMnSc1
</s>
<s>
Pinjan	Pinjan	k1gMnSc1
</s>
<s>
Qará	Qarat	k5eAaImIp3nS,k5eAaPmIp3nS,k5eAaBmIp3nS
</s>
<s>
Danon	Danon	k1gMnSc1
</s>
<s>
Šama	šama	k1gFnSc1
</s>
<s>
Akunis	Akunis	k1gFnSc1
</s>
<s>
Regev	Regev	k1gFnSc1
Jisra	Jisro	k1gNnSc2
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
bejtenu	bejten	k1gInSc2
</s>
<s>
Lieberman	Lieberman	k1gMnSc1
</s>
<s>
Landau	Landau	k6eAd1
</s>
<s>
Misežnikov	Misežnikov	k1gInSc1
</s>
<s>
Aharonovič	Aharonovič	k1gMnSc1
</s>
<s>
Landver	Landver	k1gMnSc1
</s>
<s>
Levy	Levy	k?
</s>
<s>
Ajalon	Ajalon	k1gInSc1
</s>
<s>
Rotem	Rotem	k6eAd1
</s>
<s>
Micha	Micha	k1gFnSc1
<g/>
'	'	kIx"
<g/>
eli	eli	k?
</s>
<s>
Kirschenbaum	Kirschenbaum	k1gInSc1
</s>
<s>
Ilatov	Ilatov	k1gInSc1
</s>
<s>
Amar	Amar	k1gMnSc1
</s>
<s>
Matalon	Matalon	k1gInSc1
</s>
<s>
Šemtov	Šemtov	k1gInSc1
</s>
<s>
Miller	Miller	k1gMnSc1
Izr	Izr	k1gMnSc1
<g/>
.	.	kIx.
str	str	kA
<g/>
.	.	kIx.
práce	práce	k1gFnSc1
</s>
<s>
Barak	Barak	k1gMnSc1
(	(	kIx(
<g/>
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
Acma	Acm	k1gInSc2
<g/>
'	'	kIx"
<g/>
ut	ut	k?
<g/>
)	)	kIx)
</s>
<s>
Herzog	Herzog	k1gMnSc1
</s>
<s>
Braverman	Braverman	k1gMnSc1
</s>
<s>
Jachimovič	Jachimovič	k1gMnSc1
</s>
<s>
Vilna	Vilna	k1gFnSc1
<g/>
'	'	kIx"
<g/>
i	i	k9
(	(	kIx(
<g/>
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
Acma	Acm	k1gInSc2
<g/>
'	'	kIx"
<g/>
ut	ut	k?
<g/>
,	,	kIx,
pak	pak	k6eAd1
Šanán	Šanán	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kabel	kabel	k1gInSc1
</s>
<s>
Ben	Ben	k1gInSc1
Eliezer	Eliezra	k1gFnPc2
</s>
<s>
Perec	Perec	k1gInSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Marci	Marek	k1gMnPc1
<g/>
'	'	kIx"
<g/>
ano	ano	k9
<g/>
)	)	kIx)
</s>
<s>
Ben	Ben	k1gInSc1
Simon	Simona	k1gFnPc2
</s>
<s>
Simchon	Simchon	k1gInSc1
(	(	kIx(
<g/>
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
Acma	Acm	k1gInSc2
<g/>
'	'	kIx"
<g/>
ut	ut	k?
<g/>
)	)	kIx)
</s>
<s>
Noked	Noked	k1gInSc1
(	(	kIx(
<g/>
přešla	přejít	k5eAaPmAgFnS
do	do	k7c2
Acma	Acm	k1gInSc2
<g/>
'	'	kIx"
<g/>
ut	ut	k?
<g/>
)	)	kIx)
</s>
<s>
Pines-Paz	Pines-Paz	k1gInSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Wilf	Wilf	k1gInSc4
<g/>
,	,	kIx,
přešla	přejít	k5eAaPmAgFnS
do	do	k7c2
Acma	Acm	k1gInSc2
<g/>
'	'	kIx"
<g/>
ut	ut	k?
<g/>
)	)	kIx)
</s>
<s>
Tamir	Tamir	k1gInSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Madžádala	Madžádala	k1gMnSc2
<g/>
)	)	kIx)
Šas	Šas	k1gMnSc2
</s>
<s>
Jišaj	Jišaj	k1gFnSc1
</s>
<s>
Atias	Atias	k1gMnSc1
</s>
<s>
Jicchak	Jicchak	k6eAd1
Kohen	Kohen	k2eAgInSc1d1
</s>
<s>
Amnon	Amnon	k1gNnSc1
Kohen	Kohna	k1gFnPc2
</s>
<s>
Nahari	Nahari	k6eAd1
</s>
<s>
Margi	Margi	k6eAd1
</s>
<s>
Azulaj	Azulaj	k1gFnSc1
</s>
<s>
Vaknin	Vaknin	k1gInSc1
</s>
<s>
Ze	z	k7c2
<g/>
'	'	kIx"
<g/>
ev	ev	k?
</s>
<s>
Amsalem	Amsal	k1gInSc7
(	(	kIx(
<g/>
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
Am	Am	k1gMnPc2
šalem	šal	k1gInSc7
<g/>
)	)	kIx)
</s>
<s>
Michaeli	Michael	k1gMnSc5
Sjednoc	Sjednoc	k1gInSc1
<g/>
.	.	kIx.
judaismus	judaismus	k1gInSc1
Tóry	tóra	k1gFnSc2
</s>
<s>
Litzman	Litzman	k1gMnSc1
</s>
<s>
Gafni	Gafn	k1gMnPc1
</s>
<s>
Poruš	porušit	k5eAaPmRp2nS
(	(	kIx(
<g/>
pak	pak	k6eAd1
Eichler	Eichler	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Maklev	Maklet	k5eAaPmDgInS
</s>
<s>
Mozes	Mozes	k1gInSc1
Sjednoc	Sjednoc	k1gInSc1
<g/>
.	.	kIx.
arab	arab	k1gMnSc1
<g/>
.	.	kIx.
kandid	kandid	k1gInSc1
<g/>
.	.	kIx.
<g/>
-Ta	-Ta	k?
<g/>
'	'	kIx"
<g/>
al	ala	k1gFnPc2
</s>
<s>
Sarsúr	Sarsúr	k1gMnSc1
</s>
<s>
Tíbí	Tíbí	k6eAd1
</s>
<s>
as-Sána	as-Sána	k1gFnSc1
(	(	kIx(
<g/>
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
Arab	Arab	k1gMnSc1
<g/>
.	.	kIx.
dem	dem	k?
<g/>
.	.	kIx.
str	str	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Ghnaim	Ghnaim	k6eAd1
Národní	národní	k2eAgFnSc1d1
jednota	jednota	k1gFnSc1
</s>
<s>
Kac	Kac	k?
</s>
<s>
Ari	Ari	k?
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
</s>
<s>
Eldad	Eldad	k1gInSc1
(	(	kIx(
<g/>
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
Ocma	Ocm	k1gInSc2
le-Jisra	le-Jisr	k1gInSc2
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Ben	Ben	k1gInSc1
Ari	Ari	k1gMnPc2
(	(	kIx(
<g/>
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
Ocma	Ocm	k1gInSc2
le-Jisra	le-Jisr	k1gInSc2
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
<g/>
)	)	kIx)
Chadaš	Chadaš	k1gInSc1
</s>
<s>
Baraka	Barak	k1gMnSc4
</s>
<s>
Sweid	Sweid	k1gInSc1
</s>
<s>
Chenin	Chenin	k1gInSc1
</s>
<s>
Aghbáríja	Aghbáríja	k1gMnSc1
Nové	Nová	k1gFnSc2
hnutí-Merec	hnutí-Merec	k1gMnSc1
</s>
<s>
Oron	Oron	k1gNnSc1
(	(	kIx(
<g/>
pak	pak	k6eAd1
Gal-On	Gal-On	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Gil	Gil	k?
<g/>
'	'	kIx"
<g/>
on	on	k3xPp3gMnSc1
</s>
<s>
Horowitz	Horowitz	k1gInSc1
Židovský	židovský	k2eAgInSc4d1
domov	domov	k1gInSc4
</s>
<s>
Hershkowitz	Hershkowitz	k1gMnSc1
</s>
<s>
Orlev	Orlev	k1gFnSc1
</s>
<s>
Orbach	Orbach	k1gInSc1
Balad	balada	k1gFnPc2
</s>
<s>
Zahalka	Zahalka	k1gFnSc1
</s>
<s>
Nafa	Nafa	k6eAd1
</s>
<s>
Zuabí	Zuabit	k5eAaPmIp3nS
</s>
<s>
Členové	člen	k1gMnPc1
sedmnáctého	sedmnáctý	k4xOgInSc2
Knesetu	Kneset	k1gInSc2
zvoleného	zvolený	k2eAgInSc2d1
roku	rok	k1gInSc2
2006	#num#	k4
Kadima	Kadimum	k1gNnSc2
</s>
<s>
Olmert	Olmert	k1gMnSc1
</s>
<s>
Livni	Livn	k1gMnPc1
</s>
<s>
Šítrit	Šítrit	k1gInSc1
</s>
<s>
Dichter	Dichter	k1gMnSc1
</s>
<s>
Solodkin	Solodkin	k1gMnSc1
</s>
<s>
Ramon	Ramona	k1gFnPc2
</s>
<s>
Mofaz	Mofaz	k1gInSc1
</s>
<s>
Hanegbi	Hanegbi	k6eAd1
</s>
<s>
Hirschson	Hirschson	k1gMnSc1
</s>
<s>
Ezra	Ezra	k6eAd1
</s>
<s>
Bar-On	Bar-On	k1gMnSc1
</s>
<s>
Icik	Icik	k6eAd1
</s>
<s>
Boim	Boim	k6eAd1
</s>
<s>
Edri	Edri	k6eAd1
</s>
<s>
Elkin	Elkin	k1gMnSc1
</s>
<s>
Wahabi	Wahabi	k6eAd1
</s>
<s>
Avraham	Avraham	k6eAd1
</s>
<s>
Ben	Ben	k1gInSc1
Sason	Sasona	k1gFnPc2
</s>
<s>
Aflalo	Aflat	k5eAaBmAgNnS,k5eAaPmAgNnS,k5eAaImAgNnS
</s>
<s>
Tal	Tal	k?
</s>
<s>
Tiroš	Tiroš	k1gMnSc1
</s>
<s>
Šneler	Šneler	k1gMnSc1
</s>
<s>
Nudelman	Nudelman	k1gMnSc1
</s>
<s>
Dotan	Dotan	k1gMnSc1
</s>
<s>
Chason	Chason	k1gMnSc1
</s>
<s>
Chermeš	Chermat	k5eAaBmIp2nS,k5eAaPmIp2nS
(	(	kIx(
<g/>
původně	původně	k6eAd1
Reichman	Reichman	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Ben	Ben	k1gInSc1
Jisra	Jisr	k1gInSc2
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
(	(	kIx(
<g/>
původně	původně	k6eAd1
Peres	Peres	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Plesner	Plesner	k1gInSc1
(	(	kIx(
<g/>
původně	původně	k6eAd1
Breznic	Breznice	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Mola	molo	k1gNnPc1
(	(	kIx(
<g/>
původně	původně	k6eAd1
Jicchaki	Jicchaki	k1gNnSc1
<g/>
)	)	kIx)
Izraelská	izraelský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
práce-Mejmad	práce-Mejmad	k6eAd1
</s>
<s>
Perec	Perec	k1gMnSc1
</s>
<s>
Herzog	Herzog	k1gMnSc1
</s>
<s>
Pines-Paz	Pines-Paz	k1gInSc1
</s>
<s>
Braverman	Braverman	k1gMnSc1
</s>
<s>
Tamir	Tamir	k1gMnSc1
</s>
<s>
Ajalon	Ajalon	k1gInSc1
</s>
<s>
Kabel	kabel	k1gInSc1
</s>
<s>
Ben	Ben	k1gInSc1
Eliezer	Eliezra	k1gFnPc2
</s>
<s>
Jachimovič	Jachimovič	k1gMnSc1
</s>
<s>
Melchi	Melchi	k1gNnSc1
<g/>
'	'	kIx"
<g/>
or	or	k?
</s>
<s>
Vilna	Vilna	k1gFnSc1
<g/>
'	'	kIx"
<g/>
i	i	k9
</s>
<s>
Avital	Avital	k1gMnSc1
</s>
<s>
Chilu	Chilat	k5eAaPmIp1nS
</s>
<s>
Simchon	Simchon	k1gMnSc1
</s>
<s>
Noked	Noked	k1gMnSc1
</s>
<s>
Marci	Marek	k1gMnPc1
<g/>
'	'	kIx"
<g/>
ano	ano	k9
</s>
<s>
Madžádala	Madžádat	k5eAaImAgFnS,k5eAaPmAgFnS
</s>
<s>
Šanán	Šanán	k1gInSc1
(	(	kIx(
<g/>
původně	původně	k6eAd1
Sne	sen	k1gInSc5
<g/>
)	)	kIx)
</s>
<s>
Litinecki	Litinecki	k1gNnSc1
(	(	kIx(
<g/>
původně	původně	k6eAd1
Jatom	Jatom	k1gInSc1
<g/>
)	)	kIx)
Šas	Šas	k1gFnSc1
</s>
<s>
Jišaj	Jišaj	k1gFnSc1
</s>
<s>
Jicchak	Jicchak	k6eAd1
Kohen	Kohen	k2eAgInSc1d1
</s>
<s>
Amnon	Amnon	k1gNnSc1
Kohen	Kohna	k1gFnPc2
</s>
<s>
Nahari	Nahari	k6eAd1
</s>
<s>
Atias	Atias	k1gMnSc1
</s>
<s>
Azulaj	Azulaj	k1gFnSc1
</s>
<s>
Vaknin	Vaknin	k1gInSc1
</s>
<s>
Ze	z	k7c2
<g/>
'	'	kIx"
<g/>
ev	ev	k?
</s>
<s>
Margi	Margi	k6eAd1
</s>
<s>
Amsalem	Amsal	k1gInSc7
</s>
<s>
Michaeli	Michael	k1gMnSc5
</s>
<s>
Bahajna	Bahajna	k1gFnSc1
(	(	kIx(
<g/>
původně	původně	k6eAd1
Benizri	Benizri	k1gNnSc1
<g/>
)	)	kIx)
Likud	Likud	k1gInSc1
</s>
<s>
Netanjahu	Netanjaha	k1gFnSc4
</s>
<s>
Šalom	šalom	k0
</s>
<s>
Kachlon	Kachlon	k1gInSc1
</s>
<s>
Erdan	Erdan	k1gMnSc1
</s>
<s>
Sa	Sa	k?
<g/>
'	'	kIx"
<g/>
ar	ar	k1gInSc1
</s>
<s>
Ejtan	Ejtan	k1gMnSc1
</s>
<s>
Rivlin	Rivlin	k1gInSc1
</s>
<s>
Steinitz	Steinitz	k1gMnSc1
</s>
<s>
Livnat	Livnat	k5eAaImF,k5eAaPmF
</s>
<s>
Jisra	Jisra	k1gFnSc1
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
Kac	Kac	k1gMnPc2
</s>
<s>
Chajim	Chajim	k1gMnSc1
Kac	Kac	k1gMnSc1
(	(	kIx(
<g/>
původně	původně	k6eAd1
Šaransky	Šaransky	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
Edelstein	Edelstein	k1gInSc1
(	(	kIx(
<g/>
původně	původně	k6eAd1
Nave	Nav	k1gMnSc2
<g/>
)	)	kIx)
Jisra	Jisro	k1gNnSc2
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
bejtenu	bejten	k1gInSc2
</s>
<s>
Lieberman	Lieberman	k1gMnSc1
</s>
<s>
Chason	Chason	k1gMnSc1
</s>
<s>
Šagal	Šagal	k1gMnSc1
</s>
<s>
Tartman	Tartman	k1gMnSc1
</s>
<s>
Misežnikov	Misežnikov	k1gInSc1
</s>
<s>
Landver	Landver	k1gMnSc1
</s>
<s>
Aharonovič	Aharonovič	k1gMnSc1
</s>
<s>
Ilatov	Ilatov	k1gInSc1
</s>
<s>
Miller	Miller	k1gMnSc1
</s>
<s>
Šemtov	Šemtov	k1gInSc1
</s>
<s>
Rotem	Rotem	k1gInSc1
(	(	kIx(
<g/>
původně	původně	k6eAd1
Štern	Štern	k1gNnSc1
<g/>
)	)	kIx)
Národní	národní	k2eAgFnSc1d1
jednota-Národní	jednota-Národní	k2eAgFnSc1d1
náboženská	náboženský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
Elon	Elon	k1gMnSc1
</s>
<s>
Orlev	Orlev	k1gFnSc1
</s>
<s>
Hendel	Hendlo	k1gNnPc2
</s>
<s>
Ejtam	Ejtam	k6eAd1
</s>
<s>
Slomjanski	Slomjanski	k6eAd1
</s>
<s>
Levy	Levy	k?
</s>
<s>
Gabaj	Gabaj	k1gFnSc1
</s>
<s>
Eldad	Eldad	k6eAd1
</s>
<s>
Ari	Ari	k?
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
Gil	Gil	k1gMnPc2
</s>
<s>
Ejtan	Ejtan	k1gMnSc1
</s>
<s>
Ben	Ben	k1gInSc1
Jezri	Jezr	k1gFnSc2
</s>
<s>
Šaroni	Šaron	k1gMnPc1
</s>
<s>
Ziv	Ziv	k?
</s>
<s>
Galanti	Galant	k1gMnPc1
</s>
<s>
Glazer	Glazer	k1gInSc1
(	(	kIx(
<g/>
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
ha-Derech	ha-Der	k1gInPc6
ha-tova	ha-tův	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Marom	Marom	k1gInSc1
Šalev	Šalev	k1gFnSc4
Sjednocený	sjednocený	k2eAgInSc1d1
judaismus	judaismus	k1gInSc1
Tóry	tóra	k1gFnSc2
</s>
<s>
Litzman	Litzman	k1gMnSc1
</s>
<s>
Poruš	porušit	k5eAaPmRp2nS
</s>
<s>
Gafni	Gafn	k1gMnPc1
</s>
<s>
Halpert	Halpert	k1gMnSc1
</s>
<s>
Maklev	Maklet	k5eAaPmDgInS
(	(	kIx(
<g/>
původně	původně	k6eAd1
Kohen	Kohen	k2eAgInSc1d1
<g/>
)	)	kIx)
</s>
<s>
Pollack	Pollack	k1gInSc1
(	(	kIx(
<g/>
původně	původně	k6eAd1
Ravic	Ravice	k1gInPc2
<g/>
)	)	kIx)
Merec-Jachad	Merec-Jachad	k1gInSc1
</s>
<s>
Oron	Oron	k1gMnSc1
</s>
<s>
Kohen	Kohen	k1gInSc1
</s>
<s>
Gal-On	Gal-On	k1gMnSc1
</s>
<s>
Vilan	Vilan	k1gMnSc1
</s>
<s>
Greenfeld	Greenfeld	k1gInSc1
(	(	kIx(
<g/>
původně	původně	k6eAd1
Bejlin	Bejlin	k2eAgInSc1d1
<g/>
)	)	kIx)
Sjednocená	sjednocený	k2eAgFnSc1d1
arabská	arabský	k2eAgFnSc1d1
kandidátka	kandidátka	k1gFnSc1
</s>
<s>
Sarsúr	Sarsúr	k1gMnSc1
</s>
<s>
Tíbí	Tíbí	k6eAd1
</s>
<s>
as-Sána	as-Sána	k1gFnSc1
</s>
<s>
Zakur	Zakur	k1gMnSc1
Chadaš	Chadaš	k1gMnSc1
</s>
<s>
Baraka	Barak	k1gMnSc4
</s>
<s>
Sweid	Sweid	k1gInSc1
</s>
<s>
Chenin	Chenin	k1gInSc1
Balad	balada	k1gFnPc2
</s>
<s>
Zahalka	Zahalka	k1gFnSc1
</s>
<s>
Táhá	táhat	k5eAaImIp3nS
</s>
<s>
Nafa	Nafa	k1gFnSc1
(	(	kIx(
<g/>
původně	původně	k6eAd1
Bišára	Bišár	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
308262172	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Izrael	Izrael	k1gInSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Politika	politika	k1gFnSc1
</s>
