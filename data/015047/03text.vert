<s>
Rychlost	rychlost	k1gFnSc1
zvuku	zvuk	k1gInSc2
</s>
<s>
Rychlost	rychlost	k1gFnSc1
zvuku	zvuk	k1gInSc2
je	být	k5eAaImIp3nS
rychlost	rychlost	k1gFnSc1
<g/>
,	,	kIx,
jakou	jaký	k3yRgFnSc7,k3yIgFnSc7,k3yQgFnSc7
se	se	k3xPyFc4
zvukové	zvukový	k2eAgFnSc2d1
vlny	vlna	k1gFnSc2
šíří	šířit	k5eAaImIp3nP
prostředím	prostředí	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
se	se	k3xPyFc4
tímto	tento	k3xDgInSc7
pojmem	pojem	k1gInSc7
myslí	myslet	k5eAaImIp3nS
rychlost	rychlost	k1gFnSc1
zvuku	zvuk	k1gInSc2
ve	v	k7c6
vzduchu	vzduch	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
atmosférických	atmosférický	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInSc4d3
vliv	vliv	k1gInSc4
na	na	k7c4
její	její	k3xOp3gFnSc4
hodnotu	hodnota	k1gFnSc4
má	mít	k5eAaImIp3nS
teplota	teplota	k1gFnSc1
vzduchu	vzduch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
teplotě	teplota	k1gFnSc6
20	#num#	k4
°	°	k?
<g/>
C	C	kA
je	být	k5eAaImIp3nS
rychlost	rychlost	k1gFnSc1
zvuku	zvuk	k1gInSc2
v	v	k7c6
suchém	suchý	k2eAgInSc6d1
vzduchu	vzduch	k1gInSc6
343	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
,	,	kIx,
tj.	tj.	kA
1235	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
</s>
<s>
Rychlost	rychlost	k1gFnSc1
zvuku	zvuk	k1gInSc2
v	v	k7c6
ideálním	ideální	k2eAgInSc6d1
plynu	plyn	k1gInSc6
</s>
<s>
V	v	k7c6
ideálním	ideální	k2eAgInSc6d1
plynu	plyn	k1gInSc6
pro	pro	k7c4
rychlost	rychlost	k1gFnSc4
zvuku	zvuk	k1gInSc2
platí	platit	k5eAaImIp3nS
vzorec	vzorec	k1gInSc1
</s>
<s>
c	c	k0
</s>
<s>
=	=	kIx~
</s>
<s>
κ	κ	k?
</s>
<s>
p	p	k?
</s>
<s>
0	#num#	k4
</s>
<s>
ρ	ρ	k?
</s>
<s>
0	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
γ	γ	k?
</s>
<s>
t	t	k?
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
c	c	k0
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k5eAaPmF
{	{	kIx(
<g/>
\	\	kIx~
<g/>
kappa	kappa	k1gNnSc1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
p_	p_	k?
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
rho	rho	k?
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
}}}}}	}}}}}	k?
<g/>
\	\	kIx~
<g/>
left	left	k1gInSc1
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
+	+	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
gamma	gamma	k1gFnSc1
t	t	k?
<g/>
\	\	kIx~
<g/>
right	right	k1gMnSc1
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
</s>
<s>
kde	kde	k6eAd1
</s>
<s>
p	p	k?
</s>
<s>
0	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
p_	p_	k?
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
je	být	k5eAaImIp3nS
tlak	tlak	k1gInSc1
plynu	plyn	k1gInSc2
při	při	k7c6
teplotě	teplota	k1gFnSc6
0	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
</s>
<s>
ρ	ρ	k?
</s>
<s>
0	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
rho	rho	k?
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
příslušná	příslušný	k2eAgFnSc1d1
hustota	hustota	k1gFnSc1
a	a	k8xC
</s>
<s>
γ	γ	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
gamma	gammum	k1gNnSc2
}	}	kIx)
</s>
<s>
teplotní	teplotní	k2eAgFnSc4d1
rozpínavost	rozpínavost	k1gFnSc4
pro	pro	k7c4
daný	daný	k2eAgInSc4d1
plyn	plyn	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
měření	měření	k1gNnSc2
rychlosti	rychlost	k1gFnSc2
zvuku	zvuk	k1gInSc2
</s>
<s>
První	první	k4xOgMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgMnS
změřit	změřit	k5eAaPmF
rychlost	rychlost	k1gFnSc4
zvuku	zvuk	k1gInSc2
ve	v	k7c6
vzduchu	vzduch	k1gInSc6
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
Marin	Marina	k1gFnPc2
Mersenne	Mersenn	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
pokusech	pokus	k1gInPc6
s	s	k7c7
kanónem	kanón	k1gInSc7
naměřil	naměřit	k5eAaBmAgInS
rychlost	rychlost	k1gFnSc4
428	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s.	s.	k?
Rychlost	rychlost	k1gFnSc1
zvuku	zvuk	k1gInSc2
ve	v	k7c6
vodě	voda	k1gFnSc6
poprvé	poprvé	k6eAd1
přesně	přesně	k6eAd1
měřili	měřit	k5eAaImAgMnP
Jean-Daniel	Jean-Daniel	k1gMnSc1
Colladon	Colladon	k1gMnSc1
a	a	k8xC
Charles	Charles	k1gMnSc1
Sturm	Sturm	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
ženevském	ženevský	k2eAgNnSc6d1
jezeře	jezero	k1gNnSc6
postavili	postavit	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1827	#num#	k4
dvě	dva	k4xCgFnPc4
loďky	loďka	k1gFnPc1
do	do	k7c2
vzdálenosti	vzdálenost	k1gFnSc2
13487	#num#	k4
m.	m.	k?
Speciální	speciální	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
zároveň	zároveň	k6eAd1
uhodilo	uhodit	k5eAaPmAgNnS
do	do	k7c2
zvonu	zvon	k1gInSc2
<g/>
,	,	kIx,
ponořeného	ponořený	k2eAgInSc2d1
do	do	k7c2
vody	voda	k1gFnSc2
a	a	k8xC
odpálilo	odpálit	k5eAaPmAgNnS
nálož	nálož	k1gFnSc4
střelného	střelný	k2eAgInSc2d1
prachu	prach	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozorovatel	pozorovatel	k1gMnSc1
na	na	k7c6
druhé	druhý	k4xOgFnSc6
loďce	loďka	k1gFnSc6
naměřil	naměřit	k5eAaBmAgMnS
rozdíl	rozdíl	k1gInSc4
mezi	mezi	k7c7
akustickým	akustický	k2eAgInSc7d1
a	a	k8xC
optickým	optický	k2eAgInSc7d1
signálem	signál	k1gInSc7
9,4	9,4	k4
s	s	k7c7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
odpovídá	odpovídat	k5eAaImIp3nS
1435	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s.	s.	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rychlosti	rychlost	k1gFnPc1
zvuku	zvuk	k1gInSc2
v	v	k7c6
některých	některý	k3yIgFnPc6
látkách	látka	k1gFnPc6
</s>
<s>
Látka	látka	k1gFnSc1
</s>
<s>
Rychlost	rychlost	k1gFnSc1
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
</s>
<s>
Rychlost	rychlost	k1gFnSc1
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
Oxid	oxid	k1gInSc1
uhličitý	uhličitý	k2eAgInSc1d1
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
259932	#num#	k4
</s>
<s>
Kyslík	kyslík	k1gInSc1
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
3161138	#num#	k4
</s>
<s>
Suchý	suchý	k2eAgInSc1d1
vzduch	vzduch	k1gInSc1
(	(	kIx(
<g/>
0	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
3311193	#num#	k4
</s>
<s>
Suchý	suchý	k2eAgInSc1d1
vzduch	vzduch	k1gInSc1
(	(	kIx(
<g/>
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
3431235	#num#	k4
</s>
<s>
Suchý	suchý	k2eAgInSc1d1
vzduch	vzduch	k1gInSc1
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
3461247	#num#	k4
</s>
<s>
Helium	helium	k1gNnSc1
(	(	kIx(
<g/>
0	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
9703492	#num#	k4
</s>
<s>
Vodík	vodík	k1gInSc1
(	(	kIx(
<g/>
0	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
12704572	#num#	k4
</s>
<s>
Rtuť	rtuť	k1gFnSc1
(	(	kIx(
<g/>
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
14005040	#num#	k4
</s>
<s>
Destilovaná	destilovaný	k2eAgFnSc1d1
voda	voda	k1gFnSc1
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
14975389	#num#	k4
</s>
<s>
Mořská	mořský	k2eAgFnSc1d1
voda	voda	k1gFnSc1
(	(	kIx(
<g/>
13	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
15005400	#num#	k4
</s>
<s>
Led	led	k1gInSc1
(	(	kIx(
<g/>
−	−	k?
<g/>
4	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
325011700	#num#	k4
</s>
<s>
Stříbro	stříbro	k1gNnSc1
(	(	kIx(
<g/>
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
2700	#num#	k4
/	/	kIx~
37009720	#num#	k4
/	/	kIx~
13320	#num#	k4
</s>
<s>
Měď	měď	k1gFnSc1
(	(	kIx(
<g/>
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
3500	#num#	k4
/	/	kIx~
472012600	#num#	k4
/	/	kIx~
16992	#num#	k4
</s>
<s>
Ocel	ocel	k1gFnSc1
(	(	kIx(
<g/>
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
5000	#num#	k4
/	/	kIx~
600018000	#num#	k4
/	/	kIx~
21600	#num#	k4
</s>
<s>
Sklo	sklo	k1gNnSc1
(	(	kIx(
<g/>
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
520018720	#num#	k4
</s>
<s>
Hliník	hliník	k1gInSc1
(	(	kIx(
<g/>
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
5200	#num#	k4
/	/	kIx~
640018720	#num#	k4
/	/	kIx~
23040	#num#	k4
</s>
<s>
V	v	k7c6
neohraničených	ohraničený	k2eNgFnPc6d1
pevných	pevný	k2eAgFnPc6d1
látkách	látka	k1gFnPc6
se	se	k3xPyFc4
obecně	obecně	k6eAd1
šíří	šířit	k5eAaImIp3nS
zvuk	zvuk	k1gInSc4
ve	v	k7c6
třech	tři	k4xCgFnPc6
vlnách	vlna	k1gFnPc6
-	-	kIx~
jedné	jeden	k4xCgFnSc2
podélné	podélný	k2eAgFnSc2d1
a	a	k8xC
dvou	dva	k4xCgFnPc2
příčných	příčný	k2eAgFnPc2d1
(	(	kIx(
<g/>
v	v	k7c6
izotropních	izotropní	k2eAgFnPc6d1
látkách	látka	k1gFnPc6
jsou	být	k5eAaImIp3nP
ty	ten	k3xDgFnPc1
dvě	dva	k4xCgFnPc1
příčné	příčný	k2eAgFnPc1d1
vlny	vlna	k1gFnPc1
degenerované	degenerovaný	k2eAgFnPc1d1
-	-	kIx~
tedy	tedy	k8xC
se	se	k3xPyFc4
šíří	šířit	k5eAaImIp3nS
stejně	stejně	k6eAd1
rychle	rychle	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejrychlejší	rychlý	k2eAgFnSc1d3
je	být	k5eAaImIp3nS
podélná	podélný	k2eAgFnSc1d1
vlna	vlna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ohraničených	ohraničený	k2eAgFnPc6d1
pevných	pevný	k2eAgFnPc6d1
látkách	látka	k1gFnPc6
se	se	k3xPyFc4
zvuk	zvuk	k1gInSc1
šíří	šířit	k5eAaImIp3nS
pomaleji	pomale	k6eAd2
(	(	kIx(
<g/>
protože	protože	k8xS
"	"	kIx"
<g/>
drhne	drhnout	k5eAaImIp3nS
o	o	k7c4
stěnu	stěna	k1gFnSc4
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
pevných	pevný	k2eAgFnPc2d1
látek	látka	k1gFnPc2
záleží	záležet	k5eAaImIp3nS
měření	měření	k1gNnSc1
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
jestli	jestli	k8xS
se	se	k3xPyFc4
měří	měřit	k5eAaImIp3nS
podélné	podélný	k2eAgNnSc4d1
vlnění	vlnění	k1gNnSc4
v	v	k7c6
kompaktní	kompaktní	k2eAgFnSc6d1
hmotě	hmota	k1gFnSc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
příčné	příčný	k2eAgNnSc4d1
vlnění	vlnění	k1gNnSc4
na	na	k7c6
tyči	tyč	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kompaktní	kompaktní	k2eAgFnSc6d1
hmotě	hmota	k1gFnSc6
je	být	k5eAaImIp3nS
rychlost	rychlost	k1gFnSc1
vyšší	vysoký	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Limitem	limit	k1gInSc7
by	by	kYmCp3nS
mohlo	moct	k5eAaImAgNnS
být	být	k5eAaImF
36	#num#	k4
000	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s.	s.	k?
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rychlost	rychlost	k1gFnSc1
zvuku	zvuk	k1gInSc2
ve	v	k7c6
vzduchu	vzduch	k1gInSc6
</s>
<s>
Přibližná	přibližný	k2eAgFnSc1d1
(	(	kIx(
<g/>
červeně	červeně	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
skutečná	skutečný	k2eAgFnSc1d1
(	(	kIx(
<g/>
zeleně	zeleně	k6eAd1
<g/>
)	)	kIx)
závislost	závislost	k1gFnSc1
rychlosti	rychlost	k1gFnSc2
zvuku	zvuk	k1gInSc2
na	na	k7c6
teplotě	teplota	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlevo	vlevo	k6eAd1
uvedený	uvedený	k2eAgInSc1d1
lineární	lineární	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
lze	lze	k6eAd1
proto	proto	k8xC
použít	použít	k5eAaPmF
jen	jen	k9
v	v	k7c6
okolí	okolí	k1gNnSc6
několika	několik	k4yIc2
desítek	desítka	k1gFnPc2
stupňů	stupeň	k1gInPc2
od	od	k7c2
teploty	teplota	k1gFnSc2
0	#num#	k4
<g/>
°	°	k?
<g/>
C	C	kA
</s>
<s>
Ze	z	k7c2
vzorce	vzorec	k1gInSc2
pro	pro	k7c4
rychlost	rychlost	k1gFnSc4
zvuku	zvuk	k1gInSc2
v	v	k7c6
ideálním	ideální	k2eAgInSc6d1
plynu	plyn	k1gInSc6
vyplývá	vyplývat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
rychlost	rychlost	k1gFnSc4
zvuku	zvuk	k1gInSc2
v	v	k7c6
suchém	suchý	k2eAgInSc6d1
vzduchu	vzduch	k1gInSc6
platí	platit	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
následující	následující	k2eAgInSc1d1
vztah	vztah	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
c	c	k0
</s>
<s>
=	=	kIx~
</s>
<s>
(	(	kIx(
</s>
<s>
331	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
57	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
0,607	0,607	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
t	t	k?
</s>
<s>
)	)	kIx)
</s>
<s>
ms	ms	k?
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
c	c	k0
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
left	left	k1gInSc1
<g/>
(	(	kIx(
<g/>
331	#num#	k4
<g/>
{	{	kIx(
<g/>
,	,	kIx,
<g/>
}	}	kIx)
<g/>
57	#num#	k4
<g/>
+	+	kIx~
<g/>
0	#num#	k4
<g/>
{	{	kIx(
<g/>
,	,	kIx,
<g/>
}	}	kIx)
<g/>
607	#num#	k4
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
t	t	k?
<g/>
\	\	kIx~
<g/>
right	right	k1gMnSc1
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
;	;	kIx,
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
text	text	k1gInSc1
<g/>
{	{	kIx(
<g/>
ms	ms	k?
<g/>
}}	}}	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
t	t	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
t	t	k?
<g/>
}	}	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
teplota	teplota	k1gFnSc1
ve	v	k7c6
stupních	stupeň	k1gInPc6
Celsia	Celsius	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Rychlost	rychlost	k1gFnSc1
zvuku	zvuk	k1gInSc2
tedy	tedy	k9
závisí	záviset	k5eAaImIp3nS
jen	jen	k9
na	na	k7c6
teplotě	teplota	k1gFnSc6
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
na	na	k7c6
tlaku	tlak	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
zjednodušený	zjednodušený	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
je	být	k5eAaImIp3nS
se	s	k7c7
zanedbatelnou	zanedbatelný	k2eAgFnSc7d1
chybou	chyba	k1gFnSc7
použitelný	použitelný	k2eAgInSc1d1
přibližně	přibližně	k6eAd1
v	v	k7c6
rozsahu	rozsah	k1gInSc6
od	od	k7c2
−	−	k?
°	°	k?
<g/>
C	C	kA
do	do	k7c2
100	#num#	k4
°	°	k?
<g/>
C.	C.	kA
</s>
<s>
Následující	následující	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
udává	udávat	k5eAaImIp3nS
přibližné	přibližný	k2eAgFnSc2d1
rychlosti	rychlost	k1gFnSc2
zvuku	zvuk	k1gInSc2
v	v	k7c6
různých	různý	k2eAgFnPc6d1
nadmořských	nadmořský	k2eAgFnPc6d1
výškách	výška	k1gFnPc6
<g/>
:	:	kIx,
</s>
<s>
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
Teplota	teplota	k1gFnSc1
vzduchu	vzduch	k1gInSc2
</s>
<s>
Rychlost	rychlost	k1gFnSc1
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
</s>
<s>
Rychlost	rychlost	k1gFnSc1
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
Hladina	hladina	k1gFnSc1
moře	moře	k1gNnSc1
<g/>
0	#num#	k4
<g/>
15	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
3401224	#num#	k4
</s>
<s>
11	#num#	k4
000	#num#	k4
m	m	kA
–	–	k?
20	#num#	k4
000	#num#	k4
m	m	kA
<g/>
−	−	k?
<g/>
57	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
2951062	#num#	k4
</s>
<s>
29	#num#	k4
000	#num#	k4
m	m	kA
<g/>
−	−	k?
<g/>
48	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
3011084	#num#	k4
</s>
<s>
Šíření	šíření	k1gNnSc4
zvuku	zvuk	k1gInSc2
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
</s>
<s>
K	k	k7c3
šíření	šíření	k1gNnSc3
zvuku	zvuk	k1gInSc2
je	být	k5eAaImIp3nS
potřeba	potřeba	k1gFnSc1
nějakého	nějaký	k3yIgNnSc2
látkového	látkový	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
takové	takový	k3xDgNnSc4
prostředí	prostředí	k1gNnSc4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
jsou	být	k5eAaImIp3nP
nějaké	nějaký	k3yIgFnPc1
částice	částice	k1gFnPc1
–	–	k?
například	například	k6eAd1
částice	částice	k1gFnPc1
plynů	plyn	k1gInPc2
ve	v	k7c6
vzduchu	vzduch	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
zvuk	zvuk	k1gInSc1
nešíří	šířit	k5eNaImIp3nS
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
v	v	k7c6
ideálním	ideální	k2eAgInSc6d1
případě	případ	k1gInSc6
neobsahuje	obsahovat	k5eNaImIp3nS
žádné	žádný	k3yNgNnSc1
částice	částice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Rudolf	Rudolf	k1gMnSc1
Faukner	Faukner	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Moderní	moderní	k2eAgFnSc1d1
fysika	fysika	k1gFnSc1
(	(	kIx(
<g/>
1947	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
https://phys.org/news/2020-10-scientists-upper-limit.html	https://phys.org/news/2020-10-scientists-upper-limit.html	k1gInSc1
-	-	kIx~
Scientists	Scientists	k1gInSc1
find	find	k?
upper	upper	k1gInSc1
limit	limita	k1gFnPc2
for	forum	k1gNnPc2
the	the	k?
speed	speed	k1gMnSc1
of	of	k?
sound	sound	k1gMnSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Aerodynamický	aerodynamický	k2eAgInSc1d1
třesk	třesk	k1gInSc1
–	–	k?
jev	jev	k1gInSc1
nastávající	nastávající	k1gFnSc2
v	v	k7c6
okamžiku	okamžik	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
rychlost	rychlost	k1gFnSc1
zvuku	zvuk	k1gInSc2
a	a	k8xC
rychlost	rychlost	k1gFnSc4
zdroje	zdroj	k1gInSc2
zvuku	zvuk	k1gInSc2
rovnají	rovnat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Machovo	Machův	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
rychlost	rychlost	k1gFnSc1
zvuku	zvuk	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4179365-1	4179365-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85125374	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85125374	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fyzika	fyzika	k1gFnSc1
</s>
