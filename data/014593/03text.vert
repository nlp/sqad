<s>
Spojka	spojka	k1gFnSc1
(	(	kIx(
<g/>
slovní	slovní	k2eAgInSc1d1
druh	druh	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
nepokrývá	pokrývat	k5eNaImIp3nS
téma	téma	k1gNnSc4
dostatečně	dostatečně	k6eAd1
z	z	k7c2
celosvětového	celosvětový	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Pokuste	pokusit	k5eAaPmRp2nP
se	se	k3xPyFc4
<g/>
,	,	kIx,
prosíme	prosit	k5eAaImIp1nP
<g/>
,	,	kIx,
článek	článek	k1gInSc4
vylepšit	vylepšit	k5eAaPmF
a	a	k8xC
doplnit	doplnit	k5eAaPmF
nebo	nebo	k8xC
k	k	k7c3
možnému	možný	k2eAgNnSc3d1
zdokonalení	zdokonalení	k1gNnSc3
přispět	přispět	k5eAaPmF
v	v	k7c6
diskusi	diskuse	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Spojka	spojka	k1gFnSc1
(	(	kIx(
<g/>
konjunkce	konjunkce	k1gFnSc1
<g/>
;	;	kIx,
lat.	lat.	kA
conjunctio	conjunctio	k1gNnSc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
neohebný	ohebný	k2eNgInSc1d1
slovní	slovní	k2eAgInSc1d1
druh	druh	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
spojuje	spojovat	k5eAaImIp3nS
větné	větný	k2eAgInPc4d1
členy	člen	k1gInPc4
nebo	nebo	k8xC
věty	věta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojky	spojka	k1gFnPc1
vyjadřují	vyjadřovat	k5eAaImIp3nP
mluvnický	mluvnický	k2eAgInSc4d1
a	a	k8xC
významový	významový	k2eAgInSc4d1
poměr	poměr	k1gInSc4
vět	věta	k1gFnPc2
nebo	nebo	k8xC
členů	člen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozlišují	rozlišovat	k5eAaImIp3nP
se	se	k3xPyFc4
spojky	spojka	k1gFnPc1
souřadicí	souřadicí	k2eAgFnSc2d1
a	a	k8xC
podřadicí	podřadicí	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Spojka	spojka	k1gFnSc1
souřadicí	souřadicí	k2eAgFnSc1d1
(	(	kIx(
<g/>
parataktická	parataktický	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Spojka	spojka	k1gFnSc1
souřadicí	souřadicí	k2eAgFnSc1d1
spojuje	spojovat	k5eAaImIp3nS
dva	dva	k4xCgInPc4
větné	větný	k2eAgInPc4d1
členy	člen	k1gInPc4
nebo	nebo	k8xC
věty	věta	k1gFnPc4
v	v	k7c6
souvětí	souvětí	k1gNnSc6
v	v	k7c6
souřadném	souřadný	k2eAgInSc6d1
poměru	poměr	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Příkladem	příklad	k1gInSc7
souřadicích	souřadicí	k2eAgFnPc2d1
spojek	spojka	k1gFnPc2
v	v	k7c6
češtině	čeština	k1gFnSc6
jsou	být	k5eAaImIp3nP
a	a	k8xC
<g/>
,	,	kIx,
i	i	k8xC
<g/>
,	,	kIx,
či	či	k8xC
nebo	nebo	k8xC
nebo	nebo	k8xC
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujícím	následující	k2eAgInSc6d1
výčtu	výčet	k1gInSc6
jsou	být	k5eAaImIp3nP
kromě	kromě	k7c2
spojek	spojka	k1gFnPc2
uvedeny	uvést	k5eAaPmNgInP
i	i	k8xC
spojovací	spojovací	k2eAgInPc1d1
výrazy	výraz	k1gInPc1
složené	složený	k2eAgInPc1d1
buď	buď	k8xC
z	z	k7c2
více	hodně	k6eAd2
spojek	spojka	k1gFnPc2
(	(	kIx(
<g/>
ale	ale	k8xC
i	i	k9
<g/>
)	)	kIx)
anebo	anebo	k8xC
doprovázeny	doprovázet	k5eAaImNgInP
příslovci	příslovce	k1gNnSc6
(	(	kIx(
<g/>
jednak	jednak	k8xC
(	(	kIx(
<g/>
a	a	k8xC
<g/>
)	)	kIx)
jednak	jednak	k8xC
<g/>
;	;	kIx,
jednak	jednak	k8xC
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spojky	spojka	k1gFnSc2
slučovací	slučovací	k2eAgFnSc2d1
–	–	k?
a	a	k8xC
<g/>
,	,	kIx,
i	i	k8xC
<g/>
,	,	kIx,
ani	ani	k8xC
<g/>
,	,	kIx,
nebo	nebo	k8xC
<g/>
,	,	kIx,
či	či	k8xC
<g/>
,	,	kIx,
přímo	přímo	k6eAd1
<g/>
,	,	kIx,
nadto	nadto	k6eAd1
<g/>
,	,	kIx,
ani-ani	ani-aň	k1gFnSc5
<g/>
,	,	kIx,
jak-tak	jak-tak	k1gMnSc1
<g/>
,	,	kIx,
hned-hned	hned-hned	k1gMnSc1
<g/>
,	,	kIx,
jednak-jednak	jednak-jednak	k1gMnSc1
<g/>
,	,	kIx,
zčásti-zčásti	zčásti-zčást	k1gFnPc1
<g/>
,	,	kIx,
dílem-dílem	dílem-díl	k1gInSc7
</s>
<s>
Spojky	spojka	k1gFnSc2
odporovací	odporovací	k2eAgFnSc2d1
–	–	k?
ale	ale	k9
<g/>
,	,	kIx,
avšak	avšak	k8xC
<g/>
,	,	kIx,
však	však	k9
<g/>
,	,	kIx,
leč	leč	k8xS,k8xC
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
<g/>
,	,	kIx,
jenomže	jenomže	k8xC
<g/>
,	,	kIx,
jenže	jenže	k8xC
<g/>
,	,	kIx,
sice-ale	sice-al	k1gMnSc5
<g/>
,	,	kIx,
jistě-ale	jistě-ale	k6eAd1
</s>
<s>
Spojky	spojka	k1gFnSc2
stupňovací	stupňovací	k2eAgFnSc2d1
–	–	k?
i	i	k9
<g/>
,	,	kIx,
ba	ba	k9
<g/>
,	,	kIx,
ba	ba	k9
i	i	k9
<g/>
,	,	kIx,
ba	ba	k9
ani	ani	k8xC
<g/>
,	,	kIx,
nadto	nadto	k6eAd1
<g/>
,	,	kIx,
dokonce	dokonce	k9
<g/>
,	,	kIx,
nejen-ale	jen-ale	k6eNd1
i	i	k9
<g/>
,	,	kIx,
nejen-nýbrž	jen-nýbrž	k6eNd1
i	i	k9
</s>
<s>
Spojky	spojka	k1gFnSc2
vylučovací	vylučovací	k2eAgFnSc2d1
–	–	k?
nebo	nebo	k8xC
<g/>
,	,	kIx,
aneb	aneb	k?
<g/>
,	,	kIx,
buď	buď	k8xC
-	-	kIx~
nebo	nebo	k8xC
<g/>
,	,	kIx,
buď	buď	k8xC
-	-	kIx~
anebo	anebo	k8xC
</s>
<s>
Spojky	spojka	k1gFnSc2
vysvětlovací	vysvětlovací	k2eAgFnSc2d1
–	–	k?
totiž	totiž	k9
<g/>
,	,	kIx,
vždyť	vždyť	k9
</s>
<s>
Spojky	spojka	k1gFnSc2
příčinné	příčinný	k2eAgFnSc2d1
–	–	k?
neboť	neboť	k8xC
<g/>
,	,	kIx,
vždyť	vždyť	k9
<g/>
,	,	kIx,
totiž	totiž	k9
<g/>
,	,	kIx,
však	však	k9
také	také	k9
</s>
<s>
Spojky	spojka	k1gFnSc2
důsledkové	důsledkový	k2eAgFnSc2d1
–	–	k?
proto	proto	k8xC
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
<g/>
,	,	kIx,
tudíž	tudíž	k8xC
<g/>
,	,	kIx,
a	a	k8xC
tudíž	tudíž	k8xC
<g/>
,	,	kIx,
tedy	tedy	k8xC
</s>
<s>
Spojka	spojka	k1gFnSc1
podřadicí	podřadicí	k2eAgFnSc1d1
(	(	kIx(
<g/>
hypotaktická	hypotaktický	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Spojky	spojka	k1gFnPc1
podřadicí	podřadicí	k2eAgFnPc1d1
připojují	připojovat	k5eAaImIp3nP
vedlejší	vedlejší	k2eAgFnPc1d1
věty	věta	k1gFnPc1
v	v	k7c6
souvětí	souvětí	k1gNnSc6
k	k	k7c3
větě	věta	k1gFnSc3
hlavní	hlavní	k2eAgFnSc2d1
v	v	k7c6
poměru	poměr	k1gInSc6
podřadném	podřadný	k2eAgInSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojky	spojka	k1gFnSc2
podřadicí	podřadicí	k2eAgFnSc2d1
<g/>
:	:	kIx,
aby	aby	k9
<g/>
,	,	kIx,
jakmile	jakmile	k8xS
<g/>
,	,	kIx,
až	až	k9
<g/>
,	,	kIx,
než	než	k8xS
<g/>
,	,	kIx,
nežli	nežli	k8xS
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
<g/>
,	,	kIx,
když	když	k8xS
<g/>
,	,	kIx,
kdyby	kdyby	kYmCp3nS
<g/>
,	,	kIx,
pokud	pokud	k8xS
<g/>
,	,	kIx,
protože	protože	k8xS
<g/>
,	,	kIx,
poněvadž	poněvadž	k8xS
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
<g/>
,	,	kIx,
jestliže	jestliže	k8xS
<g/>
,	,	kIx,
-li	-li	k?
<g/>
,	,	kIx,
přestože	přestože	k8xS
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
<g/>
,	,	kIx,
třebaže	třebaže	k8xS
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
<g/>
,	,	kIx,
ač	ač	k8xS
<g/>
,	,	kIx,
že	že	k8xS
<g/>
,	,	kIx,
sotva	sotva	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
podřadicích	podřadicí	k2eAgFnPc2d1
spojek	spojka	k1gFnPc2
se	se	k3xPyFc4
ke	k	k7c3
spojování	spojování	k1gNnSc3
větných	větný	k2eAgInPc2d1
členů	člen	k1gInPc2
užívají	užívat	k5eAaImIp3nP
téměř	téměř	k6eAd1
výhradně	výhradně	k6eAd1
spojky	spojka	k1gFnPc1
přípustkové	přípustkový	k2eAgFnPc1d1
<g/>
:	:	kIx,
statečný	statečný	k2eAgInSc1d1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
pošetilý	pošetilý	k2eAgInSc4d1
plán	plán	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
seznam	seznam	k1gInSc1
latinských	latinský	k2eAgInPc2d1
gramatických	gramatický	k2eAgInPc2d1
pojmů	pojem	k1gInPc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
GRUET	GRUET	kA
ŠKRABALOVÁ	Škrabalová	k1gFnSc1
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koordinační	koordinační	k2eAgFnSc1d1
spojka	spojka	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
encyklopedický	encyklopedický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
češtiny	čeština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7422	#num#	k4
<g/>
-	-	kIx~
<g/>
480	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
spojka	spojka	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Kategorie	kategorie	k1gFnPc1
Spojky	spojka	k1gFnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Slovní	slovní	k2eAgInPc1d1
druhy	druh	k1gInPc1
</s>
<s>
podstatné	podstatný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
•	•	k?
přídavné	přídavný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
•	•	k?
zájmeno	zájmeno	k1gNnSc4
•	•	k?
číslovka	číslovka	k1gFnSc1
•	•	k?
sloveso	sloveso	k1gNnSc4
•	•	k?
příslovce	příslovce	k1gNnSc2
•	•	k?
předložka	předložka	k1gFnSc1
•	•	k?
spojka	spojka	k1gFnSc1
•	•	k?
částice	částice	k1gFnSc1
•	•	k?
citoslovce	citoslovce	k1gNnSc1
jméno	jméno	k1gNnSc4
</s>
<s>
obecné	obecný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
vlastní	vlastní	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
bionymum	bionymum	k1gInSc1
</s>
<s>
antroponymum	antroponymum	k1gNnSc1
(	(	kIx(
<g/>
rodné	rodný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
/	/	kIx~
křestní	křestní	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
•	•	k?
hypokoristikon	hypokoristikon	k1gNnSc1
•	•	k?
příjmí	příjmí	k?
•	•	k?
příjmení	příjmení	k1gNnSc2
•	•	k?
přezdívka	přezdívka	k1gFnSc1
•	•	k?
jméno	jméno	k1gNnSc1
po	po	k7c6
chalupě	chalupa	k1gFnSc6
•	•	k?
fiktonymum	fiktonymum	k1gInSc1
(	(	kIx(
<g/>
pseudonym	pseudonym	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
obyvatelské	obyvatelský	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
•	•	k?
rodinné	rodinný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
•	•	k?
rodové	rodový	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
•	•	k?
etnonymum	etnonymum	k1gNnSc1
•	•	k?
theonymum	theonymum	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
zoonymum	zoonymum	k1gInSc1
•	•	k?
fytonymum	fytonymum	k1gInSc1
abionymum	abionymum	k1gInSc1
</s>
<s>
toponymum	toponymum	k1gNnSc1
(	(	kIx(
<g/>
choronymum	choronymum	k1gInSc1
•	•	k?
oikonymum	oikonymum	k1gInSc1
•	•	k?
anoikonymum	anoikonymum	k1gInSc1
–	–	k?
urbanonymum	urbanonymum	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
kosmonymum	kosmonymum	k1gInSc1
/	/	kIx~
astronymum	astronymum	k1gInSc1
•	•	k?
chrématonymum	chrématonymum	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
exonymum	exonymum	k1gInSc1
•	•	k?
endonymum	endonymum	k1gInSc1
•	•	k?
cizí	cizit	k5eAaImIp3nS
jméno	jméno	k1gNnSc4
•	•	k?
standardizované	standardizovaný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
PSH	PSH	kA
<g/>
:	:	kIx,
6860	#num#	k4
</s>
