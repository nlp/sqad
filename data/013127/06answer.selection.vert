<s>
Tělesné	tělesný	k2eAgInPc1d1	tělesný
pocity	pocit	k1gInPc1	pocit
nejsou	být	k5eNaImIp3nP	být
emocemi	emoce	k1gFnPc7	emoce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
úzký	úzký	k2eAgInSc1d1	úzký
vztah	vztah	k1gInSc1	vztah
–	–	k?	–
určitý	určitý	k2eAgInSc1d1	určitý
tělesný	tělesný	k2eAgInSc1d1	tělesný
pocit	pocit	k1gInSc1	pocit
zpravidla	zpravidla	k6eAd1	zpravidla
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
vyvolání	vyvolání	k1gNnSc3	vyvolání
některých	některý	k3yIgFnPc2	některý
emocí	emoce	k1gFnPc2	emoce
<g/>
.	.	kIx.	.
</s>
