<p>
<s>
Brian	Brian	k1gMnSc1	Brian
Johnson	Johnson	k1gMnSc1	Johnson
(	(	kIx(	(
<g/>
*	*	kIx~	*
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
Dunston	Dunston	k1gInSc1	Dunston
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
textař	textař	k1gMnSc1	textař
známý	známý	k2eAgMnSc1d1	známý
především	především	k6eAd1	především
svým	svůj	k3xOyFgNnSc7	svůj
působením	působení	k1gNnSc7	působení
v	v	k7c6	v
australské	australský	k2eAgFnSc6d1	australská
hard	hard	k6eAd1	hard
rockové	rockový	k2eAgFnSc3d1	rocková
kapele	kapela	k1gFnSc3	kapela
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
založil	založit	k5eAaPmAgMnS	založit
Johnson	Johnson	k1gMnSc1	Johnson
glam	glam	k1gMnSc1	glam
rockovou	rockový	k2eAgFnSc4d1	rocková
kapelu	kapela	k1gFnSc4	kapela
Geordie	Geordie	k1gFnSc2	Geordie
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jejím	její	k3xOp3gMnSc7	její
zpěvákem	zpěvák	k1gMnSc7	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
se	se	k3xPyFc4	se
však	však	k9	však
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
několika	několik	k4yIc2	několik
úspěšných	úspěšný	k2eAgNnPc2d1	úspěšné
alb	album	k1gNnPc2	album
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
byla	být	k5eAaImAgFnS	být
kapela	kapela	k1gFnSc1	kapela
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
obnovena	obnoven	k2eAgFnSc1d1	obnovena
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
obnovení	obnovení	k1gNnSc6	obnovení
byl	být	k5eAaImAgMnS	být
Johnson	Johnson	k1gMnSc1	Johnson
osloven	oslovit	k5eAaPmNgMnS	oslovit
s	s	k7c7	s
nabídkou	nabídka	k1gFnSc7	nabídka
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgInS	mít
nahradit	nahradit	k5eAaPmF	nahradit
zesnulého	zesnulý	k1gMnSc2	zesnulý
Bona	bona	k1gFnSc1	bona
Scotta	Scotta	k1gFnSc1	Scotta
<g/>
.	.	kIx.	.
</s>
<s>
Johnson	Johnson	k1gMnSc1	Johnson
nabídku	nabídka	k1gFnSc4	nabídka
přijal	přijmout	k5eAaPmAgMnS	přijmout
a	a	k8xC	a
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
ještě	ještě	k9	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
vydal	vydat	k5eAaPmAgMnS	vydat
album	album	k1gNnSc4	album
Back	Back	k1gMnSc1	Back
in	in	k?	in
Black	Black	k1gInSc1	Black
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
komerčně	komerčně	k6eAd1	komerčně
vůbec	vůbec	k9	vůbec
nejúspěšnějších	úspěšný	k2eAgFnPc2d3	nejúspěšnější
alb	alba	k1gFnPc2	alba
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
<g/>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
začal	začít	k5eAaPmAgMnS	začít
mít	mít	k5eAaImF	mít
problémy	problém	k1gInPc4	problém
se	s	k7c7	s
sluchem	sluch	k1gInSc7	sluch
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
čemuž	což	k3yRnSc3	což
se	se	k3xPyFc4	se
skupinou	skupina	k1gFnSc7	skupina
přestal	přestat	k5eAaPmAgMnS	přestat
vystupovat	vystupovat	k5eAaImF	vystupovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
ho	on	k3xPp3gInSc2	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
zpěvák	zpěvák	k1gMnSc1	zpěvák
Axl	Axl	k1gMnSc1	Axl
Rose	Rose	k1gMnSc1	Rose
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
S	s	k7c7	s
Geordie	Geordie	k1gFnSc5	Geordie
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
S	s	k7c7	s
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
