<s>
Pasterace	pasterace	k1gFnSc1	pasterace
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
pasterizace	pasterizace	k1gFnSc1	pasterizace
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
metod	metoda	k1gFnPc2	metoda
konzervace	konzervace	k1gFnSc2	konzervace
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
francouzský	francouzský	k2eAgMnSc1d1	francouzský
vědec	vědec	k1gMnSc1	vědec
Louis	Louis	k1gMnSc1	Louis
Pasteur	Pasteur	k1gMnSc1	Pasteur
<g/>
.	.	kIx.	.
</s>
