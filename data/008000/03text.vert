<s>
Pasterace	pasterace	k1gFnSc1	pasterace
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
pasterizace	pasterizace	k1gFnSc1	pasterizace
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
metod	metoda	k1gFnPc2	metoda
konzervace	konzervace	k1gFnSc2	konzervace
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
francouzský	francouzský	k2eAgMnSc1d1	francouzský
vědec	vědec	k1gMnSc1	vědec
Louis	Louis	k1gMnSc1	Louis
Pasteur	Pasteur	k1gMnSc1	Pasteur
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
pasterace	pasterace	k1gFnSc1	pasterace
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
francouzského	francouzský	k2eAgNnSc2d1	francouzské
válečného	válečný	k2eAgNnSc2d1	válečné
loďstva	loďstvo	k1gNnSc2	loďstvo
pro	pro	k7c4	pro
zamezení	zamezení	k1gNnSc4	zamezení
octovatění	octovatění	k1gNnSc2	octovatění
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
ujala	ujmout	k5eAaPmAgFnS	ujmout
i	i	k9	i
u	u	k7c2	u
dalších	další	k2eAgFnPc2d1	další
komodit	komodita	k1gFnPc2	komodita
<g/>
.	.	kIx.	.
</s>
<s>
Podstatou	podstata	k1gFnSc7	podstata
pasterace	pasterace	k1gFnSc2	pasterace
je	být	k5eAaImIp3nS	být
krátkodobé	krátkodobý	k2eAgNnSc1d1	krátkodobé
zvýšení	zvýšení	k1gNnSc1	zvýšení
teploty	teplota	k1gFnPc1	teplota
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
způsobí	způsobit	k5eAaPmIp3nP	způsobit
zničení	zničení	k1gNnSc3	zničení
nesporulujících	sporulující	k2eNgMnPc2d1	sporulující
patogenních	patogenní	k2eAgMnPc2d1	patogenní
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
převaření	převaření	k1gNnSc2	převaření
výrazněji	výrazně	k6eAd2	výrazně
nemění	měnit	k5eNaImIp3nS	měnit
kvalitu	kvalita	k1gFnSc4	kvalita
potraviny	potravina	k1gFnSc2	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pasteraci	pasterace	k1gFnSc6	pasterace
nedochází	docházet	k5eNaImIp3nS	docházet
ke	k	k7c3	k
sterilaci	sterilace	k1gFnSc3	sterilace
(	(	kIx(	(
<g/>
nesprávně	správně	k6eNd1	správně
sterilizaci	sterilizace	k1gFnSc3	sterilizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Působení	působení	k1gNnSc1	působení
tepla	teplo	k1gNnSc2	teplo
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
přizpůsobeno	přizpůsobit	k5eAaPmNgNnS	přizpůsobit
individuální	individuální	k2eAgFnSc3d1	individuální
potravině	potravina	k1gFnSc3	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Čím	čí	k3xOyRgNnSc7	čí
vyšší	vysoký	k2eAgFnSc1d2	vyšší
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
kratší	krátký	k2eAgInPc4d2	kratší
je	být	k5eAaImIp3nS	být
čas	čas	k1gInSc4	čas
potřebný	potřebný	k2eAgInSc4d1	potřebný
k	k	k7c3	k
usmrcení	usmrcení	k1gNnSc3	usmrcení
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yInSc7	co
vyšší	vysoký	k2eAgMnSc1d2	vyšší
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
potřebný	potřebný	k2eAgInSc1d1	potřebný
čas	čas	k1gInSc1	čas
delší	dlouhý	k2eAgInSc1d2	delší
<g/>
.	.	kIx.	.
</s>
<s>
Teplotní	teplotní	k2eAgFnSc1d1	teplotní
odolnost	odolnost	k1gFnSc1	odolnost
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
růstová	růstový	k2eAgFnSc1d1	růstová
fáze	fáze	k1gFnSc1	fáze
vegetativní	vegetativní	k2eAgFnSc2d1	vegetativní
buňky	buňka	k1gFnSc2	buňka
nebo	nebo	k8xC	nebo
spory	spor	k1gInPc4	spor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vlastnosti	vlastnost	k1gFnPc4	vlastnost
prostředí	prostředí	k1gNnSc2	prostředí
(	(	kIx(	(
<g/>
vlhkost	vlhkost	k1gFnSc1	vlhkost
<g/>
,	,	kIx,	,
kyselost	kyselost	k1gFnSc1	kyselost
(	(	kIx(	(
<g/>
pH	ph	kA	ph
<g/>
)	)	kIx)	)
a	a	k8xC	a
koncentrace	koncentrace	k1gFnSc1	koncentrace
soli	sůl	k1gFnSc2	sůl
odolnost	odolnost	k1gFnSc1	odolnost
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
,	,	kIx,	,
přítomnost	přítomnost	k1gFnSc1	přítomnost
bílkovin	bílkovina	k1gFnPc2	bílkovina
a	a	k8xC	a
tuků	tuk	k1gInPc2	tuk
odolnost	odolnost	k1gFnSc4	odolnost
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
mžikový	mžikový	k2eAgInSc1d1	mžikový
čas	čas	k1gInSc1	čas
Nízká	nízký	k2eAgFnSc1d1	nízká
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
čas	čas	k1gInSc1	čas
Ultravysoká	Ultravysoký	k2eAgFnSc1d1	Ultravysoký
teplota	teplota	k1gFnSc1	teplota
(	(	kIx(	(
<g/>
UHT	UHT	kA	UHT
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
nesprávně	správně	k6eNd1	správně
UHT-pasterace	UHTasterace	k1gFnSc1	UHT-pasterace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sterilací	sterilace	k1gFnSc7	sterilace
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
nepřežijí	přežít	k5eNaPmIp3nP	přežít
žádné	žádný	k3yNgFnPc4	žádný
bakterie	bakterie	k1gFnPc4	bakterie
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
jejich	jejich	k3xOp3gInPc1	jejich
spory	spor	k1gInPc1	spor
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
zahřátí	zahřátí	k1gNnSc2	zahřátí
na	na	k7c4	na
135	[number]	k4	135
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jen	jen	k9	jen
1-2	[number]	k4	1-2
sekundy	sekunda	k1gFnPc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
např.	např.	kA	např.
u	u	k7c2	u
mléka	mléko	k1gNnSc2	mléko
<g/>
,	,	kIx,	,
džusů	džus	k1gInPc2	džus
<g/>
,	,	kIx,	,
polévek	polévka	k1gFnPc2	polévka
atd.	atd.	kA	atd.
Pasterace	pasterace	k1gFnSc1	pasterace
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
složek	složka	k1gFnPc2	složka
technologického	technologický	k2eAgInSc2d1	technologický
postupu	postup	k1gInSc2	postup
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
a	a	k8xC	a
zpracování	zpracování	k1gNnSc6	zpracování
mléka	mléko	k1gNnSc2	mléko
a	a	k8xC	a
mléčných	mléčný	k2eAgInPc2d1	mléčný
výrobků	výrobek	k1gInPc2	výrobek
<g/>
,	,	kIx,	,
některých	některý	k3yIgInPc2	některý
masných	masný	k2eAgInPc2d1	masný
výrobků	výrobek	k1gInPc2	výrobek
<g/>
,	,	kIx,	,
vaječných	vaječný	k2eAgInPc2d1	vaječný
výrobků	výrobek	k1gInPc2	výrobek
a	a	k8xC	a
různých	různý	k2eAgInPc2d1	různý
alkoholických	alkoholický	k2eAgInPc2d1	alkoholický
nápojů	nápoj	k1gInPc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Přesná	přesný	k2eAgFnSc1d1	přesná
teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
doba	doba	k1gFnSc1	doba
pasterace	pasterace	k1gFnSc1	pasterace
u	u	k7c2	u
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
výrobku	výrobek	k1gInSc2	výrobek
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
příslušné	příslušný	k2eAgFnSc6d1	příslušná
vyhlášce	vyhláška	k1gFnSc6	vyhláška
a	a	k8xC	a
zejména	zejména	k9	zejména
na	na	k7c6	na
způsobu	způsob	k1gInSc6	způsob
technologie	technologie	k1gFnSc2	technologie
potravinářské	potravinářský	k2eAgFnSc2d1	potravinářská
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Klasická	klasický	k2eAgFnSc1d1	klasická
pasterace	pasterace	k1gFnSc1	pasterace
obvykle	obvykle	k6eAd1	obvykle
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
ohřátí	ohřátý	k2eAgMnPc1d1	ohřátý
tekutiny	tekutina	k1gFnSc2	tekutina
na	na	k7c4	na
60	[number]	k4	60
-	-	kIx~	-
75	[number]	k4	75
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
její	její	k3xOp3gInPc4	její
udržování	udržování	k1gNnSc4	udržování
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
30-120	[number]	k4	30-120
minut	minuta	k1gFnPc2	minuta
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
zpracovávaného	zpracovávaný	k2eAgInSc2d1	zpracovávaný
subjektu	subjekt	k1gInSc2	subjekt
-	-	kIx~	-
například	například	k6eAd1	například
u	u	k7c2	u
piva	pivo	k1gNnSc2	pivo
a	a	k8xC	a
vína	víno	k1gNnSc2	víno
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
teplota	teplota	k1gFnSc1	teplota
52,7	[number]	k4	52,7
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
mléko	mléko	k1gNnSc1	mléko
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
61,6	[number]	k4	61,6
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
likvidace	likvidace	k1gFnSc1	likvidace
patogenních	patogenní	k2eAgMnPc2d1	patogenní
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zvýšení	zvýšení	k1gNnSc4	zvýšení
trvanlivosti	trvanlivost	k1gFnSc2	trvanlivost
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zabránění	zabránění	k1gNnSc4	zabránění
šíření	šíření	k1gNnSc2	šíření
nemocí	nemoc	k1gFnPc2	nemoc
těmito	tento	k3xDgFnPc7	tento
potravinami	potravina	k1gFnPc7	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
byla	být	k5eAaImAgFnS	být
přenášena	přenášet	k5eAaImNgFnS	přenášet
mlékem	mléko	k1gNnSc7	mléko
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
nepasterizovalo	pasterizovat	k5eNaBmAgNnS	pasterizovat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
veškeré	veškerý	k3xTgNnSc1	veškerý
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
jde	jít	k5eAaImIp3nS	jít
do	do	k7c2	do
obchodů	obchod	k1gInPc2	obchod
<g/>
,	,	kIx,	,
pasterováno	pasterován	k2eAgNnSc4d1	pasterován
(	(	kIx(	(
<g/>
ať	ať	k8xS	ať
už	už	k6eAd1	už
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
pijeme	pít	k5eAaImIp1nP	pít
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
mléčné	mléčný	k2eAgInPc1d1	mléčný
výrobky	výrobek	k1gInPc1	výrobek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
používá	používat	k5eAaImIp3nS	používat
metody	metoda	k1gFnPc4	metoda
UHT	UHT	kA	UHT
(	(	kIx(	(
<g/>
Ultra	ultra	k2eAgInSc1d1	ultra
High	High	k1gInSc1	High
Temperature	Temperatur	k1gMnSc5	Temperatur
<g/>
)	)	kIx)	)
s	s	k7c7	s
následným	následný	k2eAgNnSc7d1	následné
aseptickým	aseptický	k2eAgNnSc7d1	aseptické
balením	balení	k1gNnSc7	balení
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sice	sice	k8xC	sice
trochu	trochu	k6eAd1	trochu
mění	měnit	k5eAaImIp3nS	měnit
chemické	chemický	k2eAgFnPc4d1	chemická
a	a	k8xC	a
senzorické	senzorický	k2eAgFnPc4d1	senzorická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
produktu	produkt	k1gInSc2	produkt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zato	zato	k6eAd1	zato
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
delší	dlouhý	k2eAgFnPc4d2	delší
trvanlivosti	trvanlivost	k1gFnPc4	trvanlivost
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
krabicová	krabicový	k2eAgNnPc4d1	krabicové
<g/>
"	"	kIx"	"
mléka	mléko	k1gNnPc4	mléko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
platné	platný	k2eAgFnSc2d1	platná
legislativy	legislativa	k1gFnSc2	legislativa
(	(	kIx(	(
<g/>
Nařízení	nařízení	k1gNnSc2	nařízení
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
Rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
ES	ES	kA	ES
<g/>
)	)	kIx)	)
č.	č.	k?	č.
853	[number]	k4	853
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
Nařízení	nařízení	k1gNnSc2	nařízení
Komise	komise	k1gFnSc2	komise
(	(	kIx(	(
<g/>
ES	ES	kA	ES
<g/>
)	)	kIx)	)
č.	č.	k?	č.
1662	[number]	k4	1662
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pasterace	pasterace	k1gFnSc1	pasterace
mléka	mléko	k1gNnSc2	mléko
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
několika	několik	k4yIc7	několik
způsoby	způsob	k1gInPc7	způsob
<g/>
:	:	kIx,	:
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
nízká	nízký	k2eAgFnSc1d1	nízká
–	–	k?	–
61,6	[number]	k4	61,6
<g />
.	.	kIx.	.
</s>
<s>
°	°	k?	°
<g/>
C	C	kA	C
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
šetrná	šetrný	k2eAgFnSc1d1	šetrná
krátkodobá	krátkodobý	k2eAgFnSc1d1	krátkodobá
–	–	k?	–
71,5	[number]	k4	71,5
<g/>
°	°	k?	°
<g/>
-	-	kIx~	-
<g/>
74	[number]	k4	74
°	°	k?	°
<g/>
C	C	kA	C
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
sekund	sekunda	k1gFnPc2	sekunda
vysoká	vysoký	k2eAgFnSc1d1	vysoká
mžiková	mžikový	k2eAgFnSc1d1	Mžiková
–	–	k?	–
85	[number]	k4	85
°	°	k?	°
<g/>
C	C	kA	C
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
sekund	sekunda	k1gFnPc2	sekunda
legislativní	legislativní	k2eAgInPc1d1	legislativní
72	[number]	k4	72
°	°	k?	°
<g/>
C	C	kA	C
15	[number]	k4	15
sekund	sekunda	k1gFnPc2	sekunda
Další	další	k2eAgNnSc4d1	další
ošetření	ošetření	k1gNnSc4	ošetření
mléka	mléko	k1gNnSc2	mléko
<g/>
:	:	kIx,	:
ESL	ESL	kA	ESL
–	–	k?	–
mléko	mléko	k1gNnSc1	mléko
s	s	k7c7	s
prodlouženou	prodloužený	k2eAgFnSc7d1	prodloužená
trvanlivostí	trvanlivost	k1gFnSc7	trvanlivost
(	(	kIx(	(
<g/>
kombinovaná	kombinovaný	k2eAgFnSc1d1	kombinovaná
metoda	metoda	k1gFnSc1	metoda
do	do	k7c2	do
100	[number]	k4	100
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sterilace	sterilace	k1gFnSc1	sterilace
–	–	k?	–
zahřátí	zahřátí	k1gNnSc1	zahřátí
mléka	mléko	k1gNnSc2	mléko
30	[number]	k4	30
<g/>
minut	minuta	k1gFnPc2	minuta
na	na	k7c4	na
100	[number]	k4	100
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
uzavřených	uzavřený	k2eAgFnPc6d1	uzavřená
nádobkách	nádobka	k1gFnPc6	nádobka
<g/>
,	,	kIx,	,
UHT	UHT	kA	UHT
–	–	k?	–
135-150	[number]	k4	135-150
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c4	na
zlomek	zlomek	k1gInSc4	zlomek
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Pasterace	pasterace	k1gFnSc1	pasterace
udrží	udržet	k5eAaPmIp3nS	udržet
mléko	mléko	k1gNnSc4	mléko
trvanlivé	trvanlivý	k2eAgNnSc4d1	trvanlivé
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
ESL	ESL	kA	ESL
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
sterilace	sterilace	k1gFnSc2	sterilace
a	a	k8xC	a
UHT	UHT	kA	UHT
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
pasterace	pasterace	k1gFnSc2	pasterace
mléka	mléko	k1gNnSc2	mléko
je	být	k5eAaImIp3nS	být
zachovaní	zachovaný	k2eAgMnPc1d1	zachovaný
bakterií	bakterie	k1gFnPc2	bakterie
mléčného	mléčný	k2eAgNnSc2d1	mléčné
kysání	kysání	k1gNnSc2	kysání
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
spor	spora	k1gFnPc2	spora
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
termostabilní	termostabilní	k2eAgFnPc1d1	termostabilní
<g/>
.	.	kIx.	.
</s>
<s>
UHT	UHT	kA	UHT
a	a	k8xC	a
sterilací	sterilace	k1gFnSc7	sterilace
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
bakterie	bakterie	k1gFnPc1	bakterie
i	i	k8xC	i
jejich	jejich	k3xOp3gInPc1	jejich
spory	spor	k1gInPc1	spor
ničí	ničit	k5eAaImIp3nP	ničit
a	a	k8xC	a
mléko	mléko	k1gNnSc1	mléko
po	po	k7c6	po
otevření	otevření	k1gNnSc6	otevření
při	při	k7c6	při
pokojové	pokojový	k2eAgFnSc6d1	pokojová
teplotě	teplota	k1gFnSc6	teplota
nezkysá	zkysat	k5eNaPmIp3nS	zkysat
jako	jako	k9	jako
pasterované	pasterovaný	k2eAgNnSc1d1	pasterované
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
shnije	shnít	k5eAaPmIp3nS	shnít
vlivem	vliv	k1gInSc7	vliv
divokých	divoký	k2eAgNnPc2d1	divoké
bakterií	bakterium	k1gNnPc2	bakterium
a	a	k8xC	a
plísní	plíseň	k1gFnPc2	plíseň
získaných	získaný	k2eAgFnPc2d1	získaná
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Pasterace	pasterace	k1gFnSc1	pasterace
zmrzliny	zmrzlina	k1gFnSc2	zmrzlina
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
provádět	provádět	k5eAaImF	provádět
dvojím	dvojí	k4xRgInSc7	dvojí
způsobem	způsob	k1gInSc7	způsob
<g/>
:	:	kIx,	:
Studenou	studený	k2eAgFnSc7d1	studená
cestou	cesta	k1gFnSc7	cesta
Teplou	Teplá	k1gFnSc7	Teplá
cestou	cesta	k1gFnSc7	cesta
(	(	kIx(	(
<g/>
náročnější	náročný	k2eAgMnSc1d2	náročnější
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvalitnější	kvalitní	k2eAgInSc1d2	kvalitnější
<g/>
)	)	kIx)	)
Zatímco	zatímco	k8xS	zatímco
studená	studený	k2eAgFnSc1d1	studená
cesta	cesta	k1gFnSc1	cesta
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
obejít	obejít	k5eAaPmF	obejít
použitím	použití	k1gNnSc7	použití
speciálních	speciální	k2eAgFnPc2d1	speciální
surovin	surovina	k1gFnPc2	surovina
<g/>
,	,	kIx,	,
teplá	teplý	k2eAgFnSc1d1	teplá
cesta	cesta	k1gFnSc1	cesta
je	být	k5eAaImIp3nS	být
technologicky	technologicky	k6eAd1	technologicky
i	i	k9	i
časově	časově	k6eAd1	časově
náročný	náročný	k2eAgInSc1d1	náročný
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yRgNnSc2	který
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
dodržet	dodržet	k5eAaPmF	dodržet
patřičné	patřičný	k2eAgFnPc4d1	patřičná
technologické	technologický	k2eAgFnPc4d1	technologická
procedury	procedura	k1gFnPc4	procedura
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
provozů	provoz	k1gInPc2	provoz
(	(	kIx(	(
<g/>
cukrárny	cukrárna	k1gFnSc2	cukrárna
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kvalitní	kvalitní	k2eAgFnSc2d1	kvalitní
pasterace	pasterace	k1gFnSc2	pasterace
teplou	teplý	k2eAgFnSc7d1	teplá
cestou	cesta	k1gFnSc7	cesta
navíc	navíc	k6eAd1	navíc
podmíněna	podmínit	k5eAaPmNgFnS	podmínit
vlastněním	vlastnění	k1gNnSc7	vlastnění
tří	tři	k4xCgInPc2	tři
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
:	:	kIx,	:
pasterizátoru	pasterizátor	k1gInSc2	pasterizátor
<g/>
,	,	kIx,	,
dozrávače	dozrávač	k1gInSc2	dozrávač
a	a	k8xC	a
výrobníku	výrobník	k1gInSc2	výrobník
zmrzliny	zmrzlina	k1gFnSc2	zmrzlina
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
pasterace	pasterace	k1gFnSc2	pasterace
piva	pivo	k1gNnSc2	pivo
je	být	k5eAaImIp3nS	být
ošetření	ošetření	k1gNnSc4	ošetření
a	a	k8xC	a
zvýšení	zvýšení	k1gNnSc4	zvýšení
jeho	jeho	k3xOp3gFnSc2	jeho
biologické	biologický	k2eAgFnSc2d1	biologická
trvanlivosti	trvanlivost	k1gFnSc2	trvanlivost
<g/>
.	.	kIx.	.
<g/>
Pivo	pivo	k1gNnSc1	pivo
je	být	k5eAaImIp3nS	být
pasterováno	pasterován	k2eAgNnSc1d1	pasterován
dvěma	dva	k4xCgInPc7	dva
způsoby	způsob	k1gInPc7	způsob
dle	dle	k7c2	dle
typu	typ	k1gInSc2	typ
balení	balení	k1gNnSc2	balení
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
sudy	sud	k1gInPc4	sud
a	a	k8xC	a
umělohmotné	umělohmotný	k2eAgFnPc4d1	umělohmotná
láhve	láhev	k1gFnPc4	láhev
se	se	k3xPyFc4	se
upravuje	upravovat	k5eAaImIp3nS	upravovat
předem	předem	k6eAd1	předem
tzv.	tzv.	kA	tzv.
bleskovou	bleskový	k2eAgFnSc7d1	blesková
pasterací	pasterace	k1gFnSc7	pasterace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
tekutina	tekutina	k1gFnSc1	tekutina
zahřeje	zahřát	k5eAaPmIp3nS	zahřát
na	na	k7c4	na
30	[number]	k4	30
až	až	k9	až
60	[number]	k4	60
sekund	sekunda	k1gFnPc2	sekunda
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
72	[number]	k4	72
°	°	k?	°
<g/>
C.	C.	kA	C.
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
metodě	metoda	k1gFnSc6	metoda
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
maximálně	maximálně	k6eAd1	maximálně
snížit	snížit	k5eAaPmF	snížit
v	v	k7c6	v
pivu	pivo	k1gNnSc6	pivo
obsah	obsah	k1gInSc4	obsah
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
při	při	k7c6	při
vysoké	vysoký	k2eAgFnSc6d1	vysoká
teplotě	teplota	k1gFnSc6	teplota
nepříznivě	příznivě	k6eNd1	příznivě
mění	měnit	k5eAaImIp3nP	měnit
vlastnosti	vlastnost	k1gFnPc4	vlastnost
nápoje	nápoj	k1gInSc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Plechovky	plechovka	k1gFnPc1	plechovka
a	a	k8xC	a
skleněné	skleněný	k2eAgFnPc1d1	skleněná
láhve	láhev	k1gFnPc1	láhev
se	se	k3xPyFc4	se
upravují	upravovat	k5eAaImIp3nP	upravovat
buď	buď	k8xC	buď
to	ten	k3xDgNnSc4	ten
tzv.	tzv.	kA	tzv.
tunelovou	tunelový	k2eAgFnSc7d1	tunelová
pasterací	pasterace	k1gFnSc7	pasterace
nebo	nebo	k8xC	nebo
Průtokovou	průtokový	k2eAgFnSc7d1	průtoková
pasterací	pasterace	k1gFnSc7	pasterace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průchodu	průchod	k1gInSc6	průchod
lahví	lahev	k1gFnPc2	lahev
tunelovým	tunelový	k2eAgInSc7d1	tunelový
pastérem	pastér	k1gInSc7	pastér
jsou	být	k5eAaImIp3nP	být
lahve	lahev	k1gFnPc1	lahev
sprchovány	sprchovat	k5eAaImNgFnP	sprchovat
vodou	voda	k1gFnSc7	voda
různou	různý	k2eAgFnSc7d1	různá
teplotou	teplota	k1gFnSc7	teplota
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
ohřály	ohřát	k5eAaPmAgFnP	ohřát
na	na	k7c4	na
požadovanou	požadovaný	k2eAgFnSc4d1	požadovaná
pasterační	pasterační	k2eAgFnSc4d1	pasterační
teplotu	teplota	k1gFnSc4	teplota
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
61	[number]	k4	61
<g/>
-	-	kIx~	-
<g/>
62	[number]	k4	62
<g/>
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
následně	následně	k6eAd1	následně
pak	pak	k6eAd1	pak
postupně	postupně	k6eAd1	postupně
zchlazovány	zchlazovat	k5eAaImNgInP	zchlazovat
na	na	k7c4	na
výstupní	výstupní	k2eAgFnSc4d1	výstupní
teplotu	teplota	k1gFnSc4	teplota
cca	cca	kA	cca
25-30	[number]	k4	25-30
°	°	k?	°
<g/>
C.	C.	kA	C.
Celková	celkový	k2eAgFnSc1d1	celková
doba	doba	k1gFnSc1	doba
průchodu	průchod	k1gInSc2	průchod
lahví	lahev	k1gFnPc2	lahev
trvá	trvat	k5eAaImIp3nS	trvat
40-60	[number]	k4	40-60
min	mina	k1gFnPc2	mina
(	(	kIx(	(
<g/>
U	u	k7c2	u
plechovek	plechovka	k1gFnPc2	plechovka
je	být	k5eAaImIp3nS	být
kratší	krátký	k2eAgFnSc1d2	kratší
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
průtokovém	průtokový	k2eAgInSc6d1	průtokový
pastéru	pastér	k1gInSc6	pastér
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
pasterační	pasterační	k2eAgFnPc1d1	pasterační
teploty	teplota	k1gFnPc1	teplota
70	[number]	k4	70
<g/>
-	-	kIx~	-
<g/>
74	[number]	k4	74
<g/>
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
již	již	k6eAd1	již
krátké	krátký	k2eAgNnSc4d1	krátké
prodloužení	prodloužení	k1gNnSc4	prodloužení
vede	vést	k5eAaImIp3nS	vést
rychle	rychle	k6eAd1	rychle
k	k	k7c3	k
přepasterovaní	přepasterovaný	k2eAgMnPc1d1	přepasterovaný
s	s	k7c7	s
negativními	negativní	k2eAgInPc7d1	negativní
důsledky	důsledek	k1gInPc7	důsledek
na	na	k7c4	na
chuť	chuť	k1gFnSc4	chuť
a	a	k8xC	a
vůni	vůně	k1gFnSc4	vůně
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
pasterace	pasterace	k1gFnSc1	pasterace
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
filtrací	filtrace	k1gFnSc7	filtrace
přes	přes	k7c4	přes
polypropylenovou	polypropylenový	k2eAgFnSc4d1	polypropylenová
membránu	membrána	k1gFnSc4	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Epidemie	epidemie	k1gFnSc1	epidemie
klíšťové	klíšťový	k2eAgFnSc2d1	klíšťová
encefalitidy	encefalitida	k1gFnSc2	encefalitida
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
asi	asi	k9	asi
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
Slovensku	Slovensko	k1gNnSc6	Slovensko
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
na	na	k7c6	na
Rožňavě	Rožňava	k1gFnSc6	Rožňava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
přenášela	přenášet	k5eAaImAgFnS	přenášet
mlékem	mléko	k1gNnSc7	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
byly	být	k5eAaImAgInP	být
dva	dva	k4xCgInPc1	dva
faktory	faktor	k1gInPc1	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
byl	být	k5eAaImAgInS	být
u	u	k7c2	u
zemědělců	zemědělec	k1gMnPc2	zemědělec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ředili	ředit	k5eAaImAgMnP	ředit
kravské	kravský	k2eAgNnSc4d1	kravské
mléko	mléko	k1gNnSc4	mléko
levnějším	levný	k2eAgFnPc3d2	levnější
kozím	kozí	k2eAgInPc3d1	kozí
(	(	kIx(	(
<g/>
do	do	k7c2	do
kterého	který	k3yRgNnSc2	který
virus	virus	k1gInSc1	virus
klíšťové	klíšťový	k2eAgFnSc2d1	klíšťová
encefalitidy	encefalitida	k1gFnSc2	encefalitida
snadno	snadno	k6eAd1	snadno
prochází	procházet	k5eAaImIp3nS	procházet
<g/>
)	)	kIx)	)
a	a	k8xC	a
jako	jako	k9	jako
čistě	čistě	k6eAd1	čistě
kravské	kravský	k2eAgNnSc1d1	kravské
je	být	k5eAaImIp3nS	být
prodávali	prodávat	k5eAaImAgMnP	prodávat
mlékárně	mlékárna	k1gFnSc6	mlékárna
<g/>
.	.	kIx.	.
</s>
<s>
Normálně	normálně	k6eAd1	normálně
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nic	nic	k3yNnSc1	nic
nestalo	stát	k5eNaPmAgNnS	stát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pasterace	pasterace	k1gFnSc1	pasterace
by	by	kYmCp3nS	by
viry	vir	k1gInPc7	vir
spolehlivě	spolehlivě	k6eAd1	spolehlivě
zničila	zničit	k5eAaPmAgFnS	zničit
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
v	v	k7c6	v
mlékárně	mlékárna	k1gFnSc6	mlékárna
se	se	k3xPyFc4	se
porouchalo	porouchat	k5eAaPmAgNnS	porouchat
pasterační	pasterační	k2eAgNnSc1d1	pasterační
zařízení	zařízení	k1gNnSc1	zařízení
(	(	kIx(	(
<g/>
paster	paster	k1gInSc1	paster
<g/>
)	)	kIx)	)
a	a	k8xC	a
podnik	podnik	k1gInSc1	podnik
to	ten	k3xDgNnSc4	ten
"	"	kIx"	"
<g/>
vyřešil	vyřešit	k5eAaPmAgInS	vyřešit
<g/>
"	"	kIx"	"
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
prodával	prodávat	k5eAaImAgMnS	prodávat
mléko	mléko	k1gNnSc4	mléko
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
produkty	produkt	k1gInPc1	produkt
nepasterované	pasterovaný	k2eNgInPc1d1	nepasterovaný
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnělo	onemocnět	k5eAaPmAgNnS	onemocnět
na	na	k7c4	na
500	[number]	k4	500
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
této	tento	k3xDgFnSc2	tento
nemoci	nemoc	k1gFnSc2	nemoc
byl	být	k5eAaImAgInS	být
zaregistrován	zaregistrovat	k5eAaPmNgInS	zaregistrovat
i	i	k9	i
přenos	přenos	k1gInSc1	přenos
přes	přes	k7c4	přes
ovčí	ovčí	k2eAgInSc4d1	ovčí
sýr	sýr	k1gInSc4	sýr
vyráběný	vyráběný	k2eAgInSc4d1	vyráběný
z	z	k7c2	z
nepasterizovaného	pasterizovaný	k2eNgNnSc2d1	nepasterizované
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
