<p>
<s>
Zoot	Zoot	k2eAgMnSc1d1	Zoot
Horn	Horn	k1gMnSc1	Horn
Rollo	Rollo	k1gNnSc4	Rollo
(	(	kIx(	(
<g/>
pravé	pravý	k2eAgNnSc4d1	pravé
jméno	jméno	k1gNnSc4	jméno
Bill	Bill	k1gMnSc1	Bill
Harkleroad	Harkleroad	k1gInSc1	Harkleroad
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
Captain	Captain	k1gMnSc1	Captain
Beefheart	Beefhearta	k1gFnPc2	Beefhearta
and	and	k?	and
The	The	k1gMnSc1	The
Magic	Magic	k1gMnSc1	Magic
Band	banda	k1gFnPc2	banda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
62	[number]	k4	62
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
kytaristou	kytarista	k1gMnSc7	kytarista
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
podle	podle	k7c2	podle
časopisu	časopis	k1gInSc2	časopis
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
