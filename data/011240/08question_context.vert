<s>
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaImF	stát
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
karibské	karibský	k2eAgFnSc2d1	karibská
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
jediná	jediný	k2eAgFnSc1d1	jediná
země	země	k1gFnSc1	země
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
u	u	k7c2	u
obou	dva	k4xCgInPc2	dva
oceánů	oceán	k1gInPc2	oceán
<g/>
,	,	kIx,	,
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Karibiku	Karibik	k1gInSc2	Karibik
u	u	k7c2	u
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
pobřeží	pobřeží	k1gNnSc6	pobřeží
leží	ležet	k5eAaImIp3nS	ležet
<g/>
.	.	kIx.	.
</s>
