<s>
Během	během	k7c2	během
glam	glam	k6eAd1	glam
metalové	metalový	k2eAgFnSc2d1	metalová
éry	éra	k1gFnSc2	éra
vydali	vydat	k5eAaPmAgMnP	vydat
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
album	album	k1gNnSc4	album
Turbo	turba	k1gFnSc5	turba
a	a	k8xC	a
aby	aby	kYmCp3nP	aby
udrželi	udržet	k5eAaPmAgMnP	udržet
krok	krok	k1gInSc4	krok
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
<g/>
,	,	kIx,	,
akceptovali	akceptovat	k5eAaBmAgMnP	akceptovat
pestřejší	pestrý	k2eAgInSc4d2	pestřejší
a	a	k8xC	a
barevnější	barevný	k2eAgInSc4d2	barevnější
image	image	k1gInSc4	image
a	a	k8xC	a
přidáním	přidání	k1gNnSc7	přidání
syntezátorů	syntezátor	k1gInPc2	syntezátor
dali	dát	k5eAaPmAgMnP	dát
své	svůj	k3xOyFgFnSc6	svůj
hudbě	hudba	k1gFnSc6	hudba
větší	veliký	k2eAgInSc4d2	veliký
pocit	pocit	k1gInSc4	pocit
opojení	opojení	k1gNnSc2	opojení
<g/>
.	.	kIx.	.
</s>
