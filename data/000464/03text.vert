<p>
<s>
Kréta	Kréta	k1gFnSc1	Kréta
(	(	kIx(	(
<g/>
též	též	k9	též
Candia	Candium	k1gNnPc1	Candium
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
Κ	Κ	k?	Κ
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Crete	Cret	k1gMnSc5	Cret
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
řecký	řecký	k2eAgInSc4d1	řecký
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
pátý	pátý	k4xOgInSc1	pátý
největší	veliký	k2eAgInSc1d3	veliký
ostrov	ostrov	k1gInSc1	ostrov
ve	v	k7c6	v
Středozemním	středozemní	k2eAgNnSc6d1	středozemní
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
99	[number]	k4	99
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
čtyři	čtyři	k4xCgFnPc4	čtyři
řecké	řecký	k2eAgFnPc4d1	řecká
regionální	regionální	k2eAgFnPc4d1	regionální
jednotky	jednotka	k1gFnPc4	jednotka
–	–	k?	–
Iraklio	Iraklio	k1gNnSc1	Iraklio
<g/>
,	,	kIx,	,
Chania	Chanium	k1gNnPc1	Chanium
<g/>
,	,	kIx,	,
Rethymno	Rethymno	k1gNnSc1	Rethymno
a	a	k8xC	a
Lasithi	Lasithi	k1gNnSc1	Lasithi
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
starořeckých	starořecký	k2eAgInPc2d1	starořecký
mýtů	mýtus	k1gInPc2	mýtus
je	být	k5eAaImIp3nS	být
Kréta	Kréta	k1gFnSc1	Kréta
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
místo	místo	k1gNnSc4	místo
zrození	zrození	k1gNnSc2	zrození
Dia	Dia	k1gMnSc2	Dia
–	–	k?	–
vládce	vládce	k1gMnSc2	vládce
olympských	olympský	k2eAgMnPc2d1	olympský
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
Iraklio	Iraklio	k6eAd1	Iraklio
<g/>
,	,	kIx,	,
dalšími	další	k2eAgFnPc7d1	další
významnějšími	významný	k2eAgFnPc7d2	významnější
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
Chania	Chanium	k1gNnPc1	Chanium
<g/>
,	,	kIx,	,
Rethymno	Rethymno	k6eAd1	Rethymno
a	a	k8xC	a
Ágios	Ágios	k1gMnSc1	Ágios
Nikólaos	Nikólaos	k1gMnSc1	Nikólaos
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
zároveň	zároveň	k6eAd1	zároveň
představují	představovat	k5eAaImIp3nP	představovat
centra	centrum	k1gNnPc1	centrum
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
prefektur	prefektura	k1gFnPc2	prefektura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Geografické	geografický	k2eAgInPc1d1	geografický
údaje	údaj	k1gInPc1	údaj
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
ostrova	ostrov	k1gInSc2	ostrov
činí	činit	k5eAaImIp3nS	činit
8261	[number]	k4	8261
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
severu	sever	k1gInSc2	sever
ho	on	k3xPp3gMnSc4	on
omývá	omývat	k5eAaImIp3nS	omývat
Krétské	krétský	k2eAgNnSc4d1	krétské
moře	moře	k1gNnSc4	moře
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgFnSc2d1	jižní
část	část	k1gFnSc4	část
Egejského	egejský	k2eAgNnSc2d1	Egejské
moře	moře	k1gNnSc2	moře
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
Libyjské	libyjský	k2eAgNnSc4d1	Libyjské
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Kréta	Kréta	k1gFnSc1	Kréta
je	být	k5eAaImIp3nS	být
hornatý	hornatý	k2eAgInSc4d1	hornatý
ostrov	ostrov	k1gInSc4	ostrov
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Psiloritis	Psiloritis	k1gFnSc1	Psiloritis
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Idi	Idi	k1gMnSc2	Idi
Ori	Ori	k1gMnSc2	Ori
má	mít	k5eAaImIp3nS	mít
2456	[number]	k4	2456
m	m	kA	m
n	n	k0	n
<g/>
.	.	kIx.	.
m	m	kA	m
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Páhnes	Páhnesa	k1gFnPc2	Páhnesa
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Levka	Levek	k1gMnSc2	Levek
Ori	Ori	k1gMnSc2	Ori
má	mít	k5eAaImIp3nS	mít
2453	[number]	k4	2453
m	m	kA	m
n	n	k0	n
<g/>
.	.	kIx.	.
m	m	kA	m
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgNnSc1d1	severní
pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
pozvolné	pozvolný	k2eAgNnSc1d1	pozvolné
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
však	však	k9	však
je	být	k5eAaImIp3nS	být
strmé	strmý	k2eAgNnSc1d1	strmé
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
tam	tam	k6eAd1	tam
útesy	útes	k1gInPc4	útes
<g/>
.	.	kIx.	.
</s>
<s>
Krétu	Kréta	k1gFnSc4	Kréta
tvoří	tvořit	k5eAaImIp3nS	tvořit
vápencová	vápencový	k2eAgFnSc1d1	vápencová
kra	kra	k1gFnSc1	kra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
při	při	k7c6	při
stejném	stejný	k2eAgNnSc6d1	stejné
vrásnění	vrásnění	k1gNnSc6	vrásnění
jako	jako	k9	jako
Balkánské	balkánský	k2eAgNnSc4d1	balkánské
pohoří	pohoří	k1gNnSc4	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nS	tvořit
Mesarská	Mesarský	k2eAgFnSc1d1	Mesarský
nížina	nížina	k1gFnSc1	nížina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Klima	klima	k1gNnSc1	klima
===	===	k?	===
</s>
</p>
<p>
<s>
Počasí	počasí	k1gNnSc1	počasí
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Kréta	Kréta	k1gFnSc1	Kréta
je	být	k5eAaImIp3nS	být
typicky	typicky	k6eAd1	typicky
středozemní	středozemní	k2eAgFnSc1d1	středozemní
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejteplejším	teplý	k2eAgMnPc3d3	nejteplejší
z	z	k7c2	z
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc2	jeho
ostrovů	ostrov	k1gInPc2	ostrov
i	i	k8xC	i
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Léta	léto	k1gNnPc1	léto
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
horká	horký	k2eAgFnSc1d1	horká
a	a	k8xC	a
suchá	suchý	k2eAgFnSc1d1	suchá
<g/>
,	,	kIx,	,
zimy	zima	k1gFnPc1	zima
mírné	mírný	k2eAgFnPc1d1	mírná
se	s	k7c7	s
sněhem	sníh	k1gInSc7	sníh
v	v	k7c6	v
horských	horský	k2eAgFnPc6d1	horská
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
deštěm	dešť	k1gInSc7	dešť
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
tu	tu	k6eAd1	tu
spadne	spadnout	k5eAaPmIp3nS	spadnout
pouze	pouze	k6eAd1	pouze
500	[number]	k4	500
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Teploty	teplota	k1gFnPc1	teplota
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
klesnou	klesnout	k5eAaPmIp3nP	klesnout
pod	pod	k7c7	pod
10	[number]	k4	10
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
horách	hora	k1gFnPc6	hora
může	moct	k5eAaImIp3nS	moct
ležet	ležet	k5eAaImF	ležet
sníh	sníh	k1gInSc4	sníh
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
a	a	k8xC	a
v	v	k7c6	v
jarních	jarní	k2eAgInPc6d1	jarní
měsících	měsíc	k1gInPc6	měsíc
je	být	k5eAaImIp3nS	být
Kréta	Kréta	k1gFnSc1	Kréta
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
vysokým	vysoký	k2eAgFnPc3d1	vysoká
teplotám	teplota	k1gFnPc3	teplota
<g/>
,	,	kIx,	,
dostatku	dostatek	k1gInSc3	dostatek
srážek	srážka	k1gFnPc2	srážka
a	a	k8xC	a
vysoké	vysoký	k2eAgFnSc2d1	vysoká
vlhkosti	vlhkost	k1gFnSc2	vlhkost
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejzelenějších	zelený	k2eAgInPc2d3	nejzelenější
ostrovů	ostrov	k1gInPc2	ostrov
ve	v	k7c6	v
Středozemním	středozemní	k2eAgNnSc6d1	středozemní
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Hornatost	hornatost	k1gFnSc1	hornatost
Kréty	Kréta	k1gFnSc2	Kréta
dává	dávat	k5eAaImIp3nS	dávat
jejímu	její	k3xOp3gNnSc3	její
počasí	počasí	k1gNnSc3	počasí
nevyzpytatelný	vyzpytatelný	k2eNgInSc4d1	nevyzpytatelný
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
změny	změna	k1gFnPc1	změna
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
náhlé	náhlý	k2eAgNnSc1d1	náhlé
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
slunečno	slunečno	k6eAd1	slunečno
a	a	k8xC	a
jasno	jasno	k6eAd1	jasno
s	s	k7c7	s
vysokými	vysoký	k2eAgFnPc7d1	vysoká
teplotami	teplota	k1gFnPc7	teplota
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
činí	činit	k5eAaImIp3nP	činit
snesitelné	snesitelný	k2eAgMnPc4d1	snesitelný
jen	jen	k6eAd1	jen
severní	severní	k2eAgInPc1d1	severní
vítr	vítr	k1gInSc1	vítr
Meltemi	Melte	k1gFnPc7	Melte
<g/>
,	,	kIx,	,
také	také	k9	také
nazýván	nazýván	k2eAgInSc4d1	nazýván
Etesian	Etesian	k1gInSc4	Etesian
<g/>
.	.	kIx.	.
</s>
<s>
Teploty	teplota	k1gFnPc1	teplota
mají	mít	k5eAaImIp3nP	mít
své	své	k1gNnSc4	své
maxima	maximum	k1gNnSc2	maximum
mezi	mezi	k7c4	mezi
35	[number]	k4	35
a	a	k8xC	a
40	[number]	k4	40
°	°	k?	°
<g/>
C	C	kA	C
ve	v	k7c6	v
dne	den	k1gInSc2	den
a	a	k8xC	a
mezi	mezi	k7c7	mezi
25	[number]	k4	25
a	a	k8xC	a
30	[number]	k4	30
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podzim	podzim	k1gInSc1	podzim
je	být	k5eAaImIp3nS	být
nejmírnější	mírný	k2eAgMnSc1d3	nejmírnější
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
ročních	roční	k2eAgNnPc2d1	roční
období	období	k1gNnPc2	období
<g/>
,	,	kIx,	,
teploty	teplota	k1gFnPc1	teplota
jsou	být	k5eAaImIp3nP	být
vyšší	vysoký	k2eAgFnPc1d2	vyšší
než	než	k8xS	než
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
nesnesitelné	snesitelný	k2eNgNnSc1d1	nesnesitelné
horko	horko	k1gNnSc1	horko
jako	jako	k8xS	jako
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
slunce	slunce	k1gNnSc1	slunce
svítí	svítit	k5eAaImIp3nS	svítit
ještě	ještě	k9	ještě
poměrně	poměrně	k6eAd1	poměrně
dlouho	dlouho	k6eAd1	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Déšť	déšť	k1gInSc1	déšť
hrozí	hrozit	k5eAaImIp3nS	hrozit
již	již	k6eAd1	již
od	od	k7c2	od
půlky	půlka	k1gFnSc2	půlka
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
spíše	spíše	k9	spíše
až	až	k6eAd1	až
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
<g/>
Díky	díky	k7c3	díky
výšce	výška	k1gFnSc3	výška
pohoří	pohoří	k1gNnSc2	pohoří
<g/>
,	,	kIx,	,
až	až	k9	až
2453	[number]	k4	2453
m	m	kA	m
<g/>
.	.	kIx.	.
n	n	k0	n
<g/>
.	.	kIx.	.
m	m	kA	m
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
bariéru	bariéra	k1gFnSc4	bariéra
mezi	mezi	k7c7	mezi
jižní	jižní	k2eAgFnSc7d1	jižní
a	a	k8xC	a
severní	severní	k2eAgFnSc7d1	severní
Krétou	Kréta	k1gFnSc7	Kréta
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
částech	část	k1gFnPc6	část
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jiné	jiný	k2eAgFnPc1d1	jiná
povětrnostní	povětrnostní	k2eAgFnPc1d1	povětrnostní
a	a	k8xC	a
teplotní	teplotní	k2eAgFnPc1d1	teplotní
podmínky	podmínka	k1gFnPc1	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Turista	turista	k1gMnSc1	turista
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
těchto	tento	k3xDgInPc2	tento
rozdílů	rozdíl	k1gInPc2	rozdíl
vědom	vědom	k2eAgInSc1d1	vědom
<g/>
.	.	kIx.	.
</s>
<s>
Lépe	dobře	k6eAd2	dobře
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
bude	být	k5eAaImBp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
vybrat	vybrat	k5eAaPmF	vybrat
oblast	oblast	k1gFnSc4	oblast
Kréty	Kréta	k1gFnSc2	Kréta
pro	pro	k7c4	pro
ubytování	ubytování	k1gNnSc4	ubytování
i	i	k8xC	i
návštěvu	návštěva	k1gFnSc4	návštěva
z	z	k7c2	z
dostupných	dostupný	k2eAgFnPc2d1	dostupná
turistických	turistický	k2eAgFnPc2d1	turistická
lokací	lokace	k1gFnPc2	lokace
<g/>
.	.	kIx.	.
</s>
<s>
Teplotní	teplotní	k2eAgInPc1d1	teplotní
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
severní	severní	k2eAgFnSc7d1	severní
a	a	k8xC	a
teplejší	teplý	k2eAgFnSc7d2	teplejší
jižní	jižní	k2eAgFnSc7d1	jižní
Krétou	Kréta	k1gFnSc7	Kréta
jsou	být	k5eAaImIp3nP	být
asi	asi	k9	asi
3	[number]	k4	3
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
teplota	teplota	k1gFnSc1	teplota
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
části	část	k1gFnPc1	část
ostrova	ostrov	k1gInSc2	ostrov
Kréta	Kréta	k1gFnSc1	Kréta
mají	mít	k5eAaImIp3nP	mít
ale	ale	k9	ale
i	i	k9	i
další	další	k2eAgInPc4d1	další
rozdíly	rozdíl	k1gInPc4	rozdíl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Východ	východ	k1gInSc4	východ
====	====	k?	====
</s>
</p>
<p>
<s>
Východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
Kréty	Kréta	k1gFnSc2	Kréta
(	(	kIx(	(
<g/>
oblast	oblast	k1gFnSc1	oblast
okolo	okolo	k7c2	okolo
městečka	městečko	k1gNnSc2	městečko
Sitia	Sitium	k1gNnSc2	Sitium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
sušší	suchý	k2eAgFnSc7d2	sušší
s	s	k7c7	s
menším	malý	k2eAgNnSc7d2	menší
množstvím	množství	k1gNnSc7	množství
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Krajina	Krajina	k1gFnSc1	Krajina
je	být	k5eAaImIp3nS	být
vyprahlá	vyprahlý	k2eAgNnPc1d1	vyprahlé
<g/>
,	,	kIx,	,
některá	některý	k3yIgNnPc1	některý
území	území	k1gNnPc1	území
mají	mít	k5eAaImIp3nP	mít
jen	jen	k9	jen
řídký	řídký	k2eAgInSc4d1	řídký
porost	porost	k1gInSc4	porost
nebo	nebo	k8xC	nebo
připomínají	připomínat	k5eAaImIp3nP	připomínat
poušť	poušť	k1gFnSc4	poušť
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Karoumbes	Karoumbes	k1gInSc1	Karoumbes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
celkově	celkově	k6eAd1	celkově
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
koryt	koryto	k1gNnPc2	koryto
potoků	potok	k1gInPc2	potok
a	a	k8xC	a
říček	říčka	k1gFnPc2	říčka
je	být	k5eAaImIp3nS	být
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
vyschlých	vyschlý	k2eAgInPc2d1	vyschlý
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
jen	jen	k9	jen
jako	jako	k9	jako
cesty	cesta	k1gFnPc1	cesta
ke	k	k7c3	k
skrytým	skrytý	k2eAgFnPc3d1	skrytá
krásám	krása	k1gFnPc3	krása
pobřeží	pobřeží	k1gNnSc2	pobřeží
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
kaňon	kaňon	k1gInSc1	kaňon
Hohlakies	Hohlakiesa	k1gFnPc2	Hohlakiesa
nebo	nebo	k8xC	nebo
Údolí	údolí	k1gNnPc2	údolí
mrtvých	mrtvý	k1gMnPc2	mrtvý
u	u	k7c2	u
Kató	Kató	k1gFnPc2	Kató
Zakros	Zakrosa	k1gFnPc2	Zakrosa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Západ	západ	k1gInSc1	západ
====	====	k?	====
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
okolo	okolo	k7c2	okolo
města	město	k1gNnSc2	město
Chania	Chanium	k1gNnSc2	Chanium
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
Kréty	Kréta	k1gFnSc2	Kréta
je	být	k5eAaImIp3nS	být
typických	typický	k2eAgInPc6d1	typický
více	hodně	k6eAd2	hodně
srážek	srážka	k1gFnPc2	srážka
a	a	k8xC	a
vlhkosti	vlhkost	k1gFnSc2	vlhkost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
zelenější	zelený	k2eAgFnSc1d2	zelenější
než	než	k8xS	než
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
teploty	teplota	k1gFnPc1	teplota
jsou	být	k5eAaImIp3nP	být
o	o	k7c4	o
trochu	trocha	k1gFnSc4	trocha
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
potoky	potok	k1gInPc1	potok
a	a	k8xC	a
říčky	říčka	k1gFnPc1	říčka
jsou	být	k5eAaImIp3nP	být
zavodněné	zavodněný	k2eAgFnPc1d1	zavodněná
celoročně	celoročně	k6eAd1	celoročně
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
podporuje	podporovat	k5eAaImIp3nS	podporovat
větší	veliký	k2eAgFnSc4d2	veliký
hustotu	hustota	k1gFnSc4	hustota
keřů	keř	k1gInPc2	keř
a	a	k8xC	a
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Jižní	jižní	k2eAgNnSc4d1	jižní
pobřeží	pobřeží	k1gNnSc4	pobřeží
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
jižního	jižní	k2eAgNnSc2d1	jižní
pobřeží	pobřeží	k1gNnSc2	pobřeží
bývají	bývat	k5eAaImIp3nP	bývat
teploty	teplota	k1gFnPc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
však	však	k9	však
mnohem	mnohem	k6eAd1	mnohem
hlubší	hluboký	k2eAgMnSc1d2	hlubší
a	a	k8xC	a
rychleji	rychle	k6eAd2	rychle
se	se	k3xPyFc4	se
svažuje	svažovat	k5eAaImIp3nS	svažovat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
bývá	bývat	k5eAaImIp3nS	bývat
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
až	až	k8xS	až
dva	dva	k4xCgInPc4	dva
stupně	stupeň	k1gInPc4	stupeň
studenější	studený	k2eAgInSc1d2	studenější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Severní	severní	k2eAgNnSc4d1	severní
pobřeží	pobřeží	k1gNnSc4	pobřeží
====	====	k?	====
</s>
</p>
<p>
<s>
Na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pobřeží	pobřeží	k1gNnSc6	pobřeží
jsou	být	k5eAaImIp3nP	být
teploty	teplota	k1gFnPc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
o	o	k7c4	o
trochu	trocha	k1gFnSc4	trocha
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
pobřeží	pobřeží	k1gNnSc1	pobřeží
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
svažuje	svažovat	k5eAaImIp3nS	svažovat
postupně	postupně	k6eAd1	postupně
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
vody	voda	k1gFnSc2	voda
vyšší	vysoký	k2eAgFnSc1d2	vyšší
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
celkově	celkově	k6eAd1	celkově
lepší	dobrý	k2eAgFnPc4d2	lepší
podmínky	podmínka	k1gFnPc4	podmínka
ke	k	k7c3	k
koupání	koupání	k1gNnSc3	koupání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Hory	hora	k1gFnSc2	hora
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
horách	hora	k1gFnPc6	hora
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
chladněji	chladně	k6eAd2	chladně
než	než	k8xS	než
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
nebo	nebo	k8xC	nebo
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
teplota	teplota	k1gFnSc1	teplota
okolo	okolo	k7c2	okolo
15	[number]	k4	15
–	–	k?	–
20	[number]	k4	20
°	°	k?	°
<g/>
C.	C.	kA	C.
Všude	všude	k6eAd1	všude
je	být	k5eAaImIp3nS	být
sucho	sucho	k1gNnSc1	sucho
a	a	k8xC	a
prameny	pramen	k1gInPc1	pramen
a	a	k8xC	a
říčky	říčka	k1gFnPc1	říčka
jsou	být	k5eAaImIp3nP	být
bez	bez	k7c2	bez
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Horské	Horské	k2eAgFnPc1d1	Horské
oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
úplně	úplně	k6eAd1	úplně
vyprahlé	vyprahlý	k2eAgFnPc1d1	vyprahlá
a	a	k8xC	a
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
polohách	poloha	k1gFnPc6	poloha
bez	bez	k7c2	bez
vegetace	vegetace	k1gFnSc2	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
na	na	k7c6	na
horách	hora	k1gFnPc6	hora
sněží	sněžit	k5eAaImIp3nS	sněžit
a	a	k8xC	a
v	v	k7c6	v
horních	horní	k2eAgFnPc6d1	horní
partiích	partie	k1gFnPc6	partie
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Lefka	Lefka	k1gMnSc1	Lefka
Ori	Ori	k1gMnSc1	Ori
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Bílé	bílý	k2eAgFnPc1d1	bílá
hory	hora	k1gFnPc1	hora
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Psiloritis	Psiloritis	k1gFnSc1	Psiloritis
nebo	nebo	k8xC	nebo
Ida	Ida	k1gFnSc1	Ida
se	se	k3xPyFc4	se
sníh	sníh	k1gInSc1	sníh
drží	držet	k5eAaImIp3nS	držet
často	často	k6eAd1	často
až	až	k9	až
do	do	k7c2	do
května	květen	k1gInSc2	květen
či	či	k8xC	či
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Fauna	fauna	k1gFnSc1	fauna
a	a	k8xC	a
flóra	flóra	k1gFnSc1	flóra
===	===	k?	===
</s>
</p>
<p>
<s>
Původní	původní	k2eAgInSc1d1	původní
porost	porost	k1gInSc1	porost
Kréty	Kréta	k1gFnSc2	Kréta
tvořily	tvořit	k5eAaImAgInP	tvořit
cypřišové	cypřišový	k2eAgInPc1d1	cypřišový
a	a	k8xC	a
cedrové	cedrový	k2eAgInPc1d1	cedrový
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
počet	počet	k1gInSc1	počet
však	však	k9	však
značně	značně	k6eAd1	značně
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
kvůli	kvůli	k7c3	kvůli
hospodářství	hospodářství	k1gNnSc3	hospodářství
a	a	k8xC	a
kácení	kácení	k1gNnSc3	kácení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
oblastech	oblast	k1gFnPc6	oblast
roste	růst	k5eAaImIp3nS	růst
nízká	nízký	k2eAgFnSc1d1	nízká
borovice	borovice	k1gFnSc1	borovice
halepská	halepský	k2eAgFnSc1d1	halepská
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
horami	hora	k1gFnPc7	hora
a	a	k8xC	a
přímořskou	přímořský	k2eAgFnSc7d1	přímořská
rovinou	rovina	k1gFnSc7	rovina
houštiny	houština	k1gFnSc2	houština
tvořené	tvořený	k2eAgFnSc2d1	tvořená
například	například	k6eAd1	například
vřesovcem	vřesovec	k1gInSc7	vřesovec
<g/>
,	,	kIx,	,
zakrslými	zakrslý	k2eAgInPc7d1	zakrslý
duby	dub	k1gInPc7	dub
nebo	nebo	k8xC	nebo
myrtou	myrta	k1gFnSc7	myrta
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
porosty	porost	k1gInPc1	porost
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
macchie	macchie	k1gFnSc1	macchie
nebo	nebo	k8xC	nebo
garrigues	garrigues	k1gInSc1	garrigues
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Krétu	Kréta	k1gFnSc4	Kréta
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgInPc4d1	typický
olivovníky	olivovník	k1gInPc4	olivovník
<g/>
,	,	kIx,	,
kterých	který	k3yRgInPc2	který
tu	tu	k6eAd1	tu
roste	růst	k5eAaImIp3nS	růst
kolem	kolem	k7c2	kolem
25	[number]	k4	25
miliónů	milión	k4xCgInPc2	milión
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
je	být	k5eAaImIp3nS	být
ostrov	ostrov	k1gInSc1	ostrov
přímo	přímo	k6eAd1	přímo
zasypán	zasypat	k5eAaPmNgInS	zasypat
květinami	květina	k1gFnPc7	květina
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
vlčím	vlčí	k2eAgInSc7d1	vlčí
mákem	mák	k1gInSc7	mák
<g/>
,	,	kIx,	,
kopretinami	kopretina	k1gFnPc7	kopretina
<g/>
,	,	kIx,	,
divokými	divoký	k2eAgInPc7d1	divoký
tulipány	tulipán	k1gInPc7	tulipán
nebo	nebo	k8xC	nebo
orchidejemi	orchidea	k1gFnPc7	orchidea
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
tu	tu	k6eAd1	tu
roste	růst	k5eAaImIp3nS	růst
přes	přes	k7c4	přes
2000	[number]	k4	2000
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
bylin	bylina	k1gFnPc2	bylina
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
160	[number]	k4	160
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
nikde	nikde	k6eAd1	nikde
jinde	jinde	k6eAd1	jinde
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bylin	bylina	k1gFnPc2	bylina
zde	zde	k6eAd1	zde
roste	růst	k5eAaImIp3nS	růst
např.	např.	kA	např.
levandule	levandule	k1gFnSc1	levandule
<g/>
,	,	kIx,	,
majoránka	majoránka	k1gFnSc1	majoránka
<g/>
,	,	kIx,	,
oregano	oregano	k1gNnSc1	oregano
<g/>
,	,	kIx,	,
rozmarýn	rozmarýn	k1gInSc1	rozmarýn
<g/>
,	,	kIx,	,
tymián	tymián	k1gInSc1	tymián
<g/>
,	,	kIx,	,
šalvěj	šalvěj	k1gInSc1	šalvěj
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
ovocné	ovocný	k2eAgInPc1d1	ovocný
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
druhů	druh	k1gInPc2	druh
zeleniny	zelenina	k1gFnSc2	zelenina
a	a	k8xC	a
květin	květina	k1gFnPc2	květina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
suchých	suchý	k2eAgNnPc6d1	suché
místech	místo	k1gNnPc6	místo
rostou	růst	k5eAaImIp3nP	růst
agáve	agáve	k1gFnPc1	agáve
<g/>
,	,	kIx,	,
rohovníky	rohovník	k1gInPc1	rohovník
a	a	k8xC	a
fíkové	fíkový	k2eAgInPc1d1	fíkový
kaktusy	kaktus	k1gInPc1	kaktus
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
skrze	skrze	k?	skrze
rostlinstvo	rostlinstvo	k1gNnSc1	rostlinstvo
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
velká	velký	k2eAgFnSc1d1	velká
pestrost	pestrost	k1gFnSc1	pestrost
Kréty	Kréta	k1gFnSc2	Kréta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
vynikne	vyniknout	k5eAaPmIp3nS	vyniknout
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
volně	volně	k6eAd1	volně
dostupných	dostupný	k2eAgFnPc2d1	dostupná
fotek	fotka	k1gFnPc2	fotka
<g/>
.	.	kIx.	.
<g/>
Fauna	fauna	k1gFnSc1	fauna
svou	svůj	k3xOyFgFnSc7	svůj
rozmanitosti	rozmanitost	k1gFnSc6	rozmanitost
za	za	k7c7	za
flórou	flóra	k1gFnSc7	flóra
zaostává	zaostávat	k5eAaImIp3nS	zaostávat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
žije	žít	k5eAaImIp3nS	žít
jen	jen	k9	jen
jeden	jeden	k4xCgInSc1	jeden
druh	druh	k1gInSc1	druh
vysoké	vysoký	k2eAgFnSc2d1	vysoká
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
,	,	kIx,	,
divoká	divoký	k2eAgFnSc1d1	divoká
koza	koza	k1gFnSc1	koza
kri-kri	krir	k1gFnSc2	kri-kr
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
menší	malý	k2eAgFnSc2d2	menší
zvěře	zvěř	k1gFnSc2	zvěř
zde	zde	k6eAd1	zde
pobývají	pobývat	k5eAaImIp3nP	pobývat
jezevci	jezevec	k1gMnPc1	jezevec
<g/>
,	,	kIx,	,
lasičky	lasička	k1gFnPc1	lasička
a	a	k8xC	a
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
slyšet	slyšet	k5eAaImF	slyšet
cvrkot	cvrkot	k1gInSc1	cvrkot
cikád	cikáda	k1gFnPc2	cikáda
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
Krétě	Kréta	k1gFnSc6	Kréta
žil	žít	k5eAaImAgInS	žít
trpasličí	trpasličí	k2eAgInSc1d1	trpasličí
druh	druh	k1gInSc1	druh
slona	slon	k1gMnSc2	slon
Palaeoloxodon	Palaeoloxodon	k1gInSc4	Palaeoloxodon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
velký	velký	k2eAgInSc4d1	velký
jako	jako	k8xS	jako
největší	veliký	k2eAgInSc4d3	veliký
známý	známý	k2eAgInSc4d1	známý
druh	druh	k1gInSc4	druh
psa	pes	k1gMnSc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Trpasličí	trpasličí	k2eAgMnPc1d1	trpasličí
sloni	slon	k1gMnPc1	slon
na	na	k7c6	na
Krétě	Kréta	k1gFnSc6	Kréta
vyhynuli	vyhynout	k5eAaPmAgMnP	vyhynout
asi	asi	k9	asi
před	před	k7c7	před
10	[number]	k4	10
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
neolitu	neolit	k1gInSc2	neolit
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
v	v	k7c6	v
3.	[number]	k4	3.
a	a	k8xC	a
2.	[number]	k4	2.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Kréta	Kréta	k1gFnSc1	Kréta
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
center	centrum	k1gNnPc2	centrum
velmi	velmi	k6eAd1	velmi
vyspělé	vyspělý	k2eAgFnPc1d1	vyspělá
mínojské	mínojský	k2eAgFnPc1d1	mínojská
civilizace	civilizace	k1gFnPc1	civilizace
<g/>
,	,	kIx,	,
nazvané	nazvaný	k2eAgInPc1d1	nazvaný
podle	podle	k7c2	podle
krále	král	k1gMnSc2	král
Mínóa	Mínóus	k1gMnSc2	Mínóus
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
po	po	k7c6	po
sobě	se	k3xPyFc3	se
zanechala	zanechat	k5eAaPmAgFnS	zanechat
ruiny	ruina	k1gFnSc2	ruina
svých	svůj	k3xOyFgInPc2	svůj
paláců	palác	k1gInPc2	palác
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
největší	veliký	k2eAgInSc1d3	veliký
je	být	k5eAaImIp3nS	být
Knóssos	Knóssos	k1gInSc1	Knóssos
<g/>
.	.	kIx.	.
</s>
<s>
Znalost	znalost	k1gFnSc1	znalost
výroby	výroba	k1gFnSc2	výroba
bronzu	bronz	k1gInSc2	bronz
<g/>
,	,	kIx,	,
rozvoj	rozvoj	k1gInSc1	rozvoj
hrnčířství	hrnčířství	k1gNnSc2	hrnčířství
a	a	k8xC	a
kovotepectví	kovotepectví	k1gNnSc2	kovotepectví
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgInPc1d1	obchodní
styky	styk	k1gInPc1	styk
způsobily	způsobit	k5eAaPmAgInP	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mínojská	mínojský	k2eAgFnSc1d1	mínojská
civilizace	civilizace	k1gFnSc1	civilizace
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
ve	v	k7c4	v
významnou	významný	k2eAgFnSc4d1	významná
námořní	námořní	k2eAgFnSc4d1	námořní
velmoc	velmoc	k1gFnSc4	velmoc
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
písmo	písmo	k1gNnSc1	písmo
minojců	minojce	k1gMnPc2	minojce
zatím	zatím	k6eAd1	zatím
nebylo	být	k5eNaImAgNnS	být
zcela	zcela	k6eAd1	zcela
rozluštěno	rozluštěn	k2eAgNnSc1d1	rozluštěno
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1470	[number]	k4	1470
až	až	k9	až
1450	[number]	k4	1450
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
říše	říše	k1gFnSc1	říše
náhle	náhle	k6eAd1	náhle
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
se	se	k3xPyFc4	se
o	o	k7c6	o
následcích	následek	k1gInPc6	následek
erupce	erupce	k1gFnSc2	erupce
sopky	sopka	k1gFnSc2	sopka
Thera	Ther	k1gInSc2	Ther
(	(	kIx(	(
<g/>
Santorini	Santorin	k1gMnPc1	Santorin
<g/>
)	)	kIx)	)
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1550	[number]	k4	1550
př.	př.	kA	př.
<g/>
n	n	k0	n
<g/>
.	.	kIx.	.
<g/>
l	l	kA	l
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
silném	silný	k2eAgNnSc6d1	silné
zemětřesení	zemětřesení	k1gNnSc6	zemětřesení
v	v	k7c6	v
uvedených	uvedený	k2eAgNnPc6d1	uvedené
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
této	tento	k3xDgFnSc2	tento
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
na	na	k7c6	na
Krétě	Kréta	k1gFnSc6	Kréta
žila	žít	k5eAaImAgFnS	žít
přes	přes	k7c4	přes
1500	[number]	k4	1500
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ostrova	ostrov	k1gInSc2	ostrov
postupně	postupně	k6eAd1	postupně
zmocňovaly	zmocňovat	k5eAaImAgFnP	zmocňovat
jiné	jiný	k2eAgFnPc1d1	jiná
kultury	kultura	k1gFnPc1	kultura
-	-	kIx~	-
mykénská	mykénský	k2eAgNnPc1d1	mykénské
<g/>
,	,	kIx,	,
dórská	dórský	k2eAgNnPc1d1	dórské
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
67	[number]	k4	67
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
ostrov	ostrov	k1gInSc4	ostrov
Římané	Říman	k1gMnPc1	Říman
-	-	kIx~	-
<g/>
Římská	římský	k2eAgFnSc1d1	římská
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
za	za	k7c7	za
níž	jenž	k3xRgFnSc7	jenž
ostrov	ostrov	k1gInSc1	ostrov
poměrně	poměrně	k6eAd1	poměrně
prosperoval	prosperovat	k5eAaImAgInS	prosperovat
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c6	na
periferii	periferie	k1gFnSc6	periferie
dění	dění	k1gNnSc2	dění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
395	[number]	k4	395
<g/>
–	–	k?	–
<g/>
823	[number]	k4	823
a	a	k8xC	a
964	[number]	k4	964
<g/>
–	–	k?	–
<g/>
1204	[number]	k4	1204
vládla	vládnout	k5eAaImAgFnS	vládnout
ostrovu	ostrov	k1gInSc3	ostrov
říše	říš	k1gFnSc2	říš
Východořímská	východořímský	k2eAgNnPc1d1	Východořímské
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Byzantská	byzantský	k2eAgFnSc1d1	byzantská
<g/>
,	,	kIx,	,
v	v	k7c6	v
mezidobí	mezidobí	k1gNnSc6	mezidobí
roků	rok	k1gInPc2	rok
823	[number]	k4	823
<g/>
–	–	k?	–
<g/>
961	[number]	k4	961
ostrov	ostrov	k1gInSc4	ostrov
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
Arabové	Arab	k1gMnPc1	Arab
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1204	[number]	k4	1204
město	město	k1gNnSc4	město
spravovala	spravovat	k5eAaImAgFnS	spravovat
Benátská	benátský	k2eAgFnSc1d1	Benátská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zde	zde	k6eAd1	zde
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
svou	svůj	k3xOyFgFnSc4	svůj
kolonii	kolonie	k1gFnSc4	kolonie
Království	království	k1gNnSc4	království
Kandia	Kandium	k1gNnSc2	Kandium
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
vláda	vláda	k1gFnSc1	vláda
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1669	[number]	k4	1669
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
po	po	k7c6	po
dvaadvacetiletém	dvaadvacetiletý	k2eAgNnSc6d1	Dvaadvacetileté
obléhání	obléhání	k1gNnSc6	obléhání
dobyla	dobýt	k5eAaPmAgFnS	dobýt
Iraklio	Iraklio	k6eAd1	Iraklio
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19.	[number]	k4	19.
století	století	k1gNnSc2	století
probíhal	probíhat	k5eAaImAgInS	probíhat
boj	boj	k1gInSc1	boj
za	za	k7c4	za
připojení	připojení	k1gNnSc4	připojení
Kréty	Kréta	k1gFnSc2	Kréta
k	k	k7c3	k
Řeckému	řecký	k2eAgNnSc3d1	řecké
království	království	k1gNnSc3	království
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
dostal	dostat	k5eAaPmAgMnS	dostat
ostrov	ostrov	k1gInSc4	ostrov
díky	díky	k7c3	díky
intervenci	intervence	k1gFnSc3	intervence
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
vlastní	vlastní	k2eAgFnSc4d1	vlastní
samosprávu	samospráva	k1gFnSc4	samospráva
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
klesl	klesnout	k5eAaPmAgInS	klesnout
podíl	podíl	k1gInSc1	podíl
muslimů	muslim	k1gMnPc2	muslim
z	z	k7c2	z
asi	asi	k9	asi
45	[number]	k4	45
%	%	kIx~	%
na	na	k7c4	na
11	[number]	k4	11
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
byl	být	k5eAaImAgInS	být
ostrov	ostrov	k1gInSc1	ostrov
oficiálně	oficiálně	k6eAd1	oficiálně
postoupen	postoupen	k2eAgInSc1d1	postoupen
Řecku	Řecko	k1gNnSc6	Řecko
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
smlouvy	smlouva	k1gFnSc2	smlouva
z	z	k7c2	z
Lausanne	Lausanne	k1gNnSc2	Lausanne
byl	být	k5eAaImAgInS	být
i	i	k9	i
zbytek	zbytek	k1gInSc1	zbytek
muslimské	muslimský	k2eAgFnSc2d1	muslimská
populace	populace	k1gFnSc2	populace
repatriován	repatriovat	k5eAaBmNgMnS	repatriovat
do	do	k7c2	do
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
to	ten	k3xDgNnSc1	ten
de	de	k?	de
facto	facto	k1gNnSc1	facto
byli	být	k5eAaImAgMnP	být
místní	místní	k2eAgMnPc1d1	místní
islamizovaní	islamizovaný	k2eAgMnPc1d1	islamizovaný
Kréťané	Kréťan	k1gMnPc1	Kréťan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
byl	být	k5eAaImAgInS	být
ostrov	ostrov	k1gInSc1	ostrov
obsazen	obsazen	k2eAgInSc1d1	obsazen
nacisty	nacista	k1gMnPc7	nacista
při	při	k7c6	při
první	první	k4xOgFnSc6	první
parašutistické	parašutistický	k2eAgFnSc6d1	parašutistická
výsadkové	výsadkový	k2eAgFnSc6d1	výsadková
vojenské	vojenský	k2eAgFnSc6d1	vojenská
akci	akce	k1gFnSc6	akce
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nacisté	nacista	k1gMnPc1	nacista
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
vzdušné	vzdušný	k2eAgFnSc6d1	vzdušná
operaci	operace	k1gFnSc6	operace
utrpěli	utrpět	k5eAaPmAgMnP	utrpět
poměrně	poměrně	k6eAd1	poměrně
značné	značný	k2eAgFnPc4d1	značná
ztráty	ztráta	k1gFnPc4	ztráta
asi	asi	k9	asi
4000	[number]	k4	4000
<g/>
–	–	k?	–
<g/>
7000	[number]	k4	7000
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
byl	být	k5eAaImAgInS	být
ostrov	ostrov	k1gInSc1	ostrov
navrácen	navrátit	k5eAaPmNgInS	navrátit
Řecku	Řecko	k1gNnSc3	Řecko
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejosobitějších	osobitý	k2eAgFnPc2d3	nejosobitější
oblastí	oblast	k1gFnPc2	oblast
Řecka	Řecko	k1gNnSc2	Řecko
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
dialektem	dialekt	k1gInSc7	dialekt
a	a	k8xC	a
uvědoměním	uvědomění	k1gNnSc7	uvědomění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Památky	památka	k1gFnPc1	památka
a	a	k8xC	a
turistické	turistický	k2eAgInPc1d1	turistický
cíle	cíl	k1gInPc1	cíl
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Samaria	samarium	k1gNnSc2	samarium
–	–	k?	–
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
soutěska	soutěska	k1gFnSc1	soutěska
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
</s>
</p>
<p>
<s>
Gortyn	Gortyn	k1gInSc1	Gortyn
–	–	k?	–
první	první	k4xOgMnSc1	první
evropský	evropský	k2eAgInSc1d1	evropský
zákoník	zákoník	k1gInSc1	zákoník
vytesaný	vytesaný	k2eAgInSc1d1	vytesaný
do	do	k7c2	do
kamene	kámen	k1gInSc2	kámen
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Gortynský	Gortynský	k1gMnSc1	Gortynský
zákoník	zákoník	k1gMnSc1	zákoník
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Spinalonga	Spinalonga	k1gFnSc1	Spinalonga
–	–	k?	–
ostrovní	ostrovní	k2eAgFnSc4d1	ostrovní
pevnost	pevnost	k1gFnSc4	pevnost
chránící	chránící	k2eAgInSc4d1	chránící
přístav	přístav	k1gInSc4	přístav
Elounda	Elounda	k1gFnSc1	Elounda
</s>
</p>
<p>
<s>
Rethymno	Rethymno	k6eAd1	Rethymno
–	–	k?	–
přístav	přístav	k1gInSc1	přístav
v	v	k7c6	v
benátském	benátský	k2eAgInSc6d1	benátský
slohu	sloh	k1gInSc6	sloh
</s>
</p>
<p>
<s>
Vai	Vai	k?	Vai
–	–	k?	–
jediná	jediný	k2eAgFnSc1d1	jediná
palmová	palmový	k2eAgFnSc1d1	Palmová
pláž	pláž	k1gFnSc1	pláž
na	na	k7c6	na
Krétě	Kréta	k1gFnSc6	Kréta
</s>
</p>
<p>
<s>
Ierapetra	Ierapetra	k1gFnSc1	Ierapetra
–	–	k?	–
největší	veliký	k2eAgFnSc1d3	veliký
osada	osada	k1gFnSc1	osada
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
</s>
</p>
<p>
<s>
Agia	Agi	k2eAgFnSc1d1	Agia
Marina	Marina	k1gFnSc1	Marina
<g/>
,	,	kIx,	,
Platanias	Platanias	k1gMnSc1	Platanias
–	–	k?	–
nejznámější	známý	k2eAgMnSc1d3	nejznámější
tur	tur	k1gMnSc1	tur
<g/>
.	.	kIx.	.
oblasti	oblast	k1gFnSc6	oblast
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Mínojské	Mínojský	k2eAgInPc1d1	Mínojský
paláce	palác	k1gInPc1	palác
===	===	k?	===
</s>
</p>
<p>
<s>
Knóssos	Knóssos	k1gInSc1	Knóssos
–	–	k?	–
hlavní	hlavní	k2eAgInSc1d1	hlavní
a	a	k8xC	a
největší	veliký	k2eAgInSc1d3	veliký
palác	palác	k1gInSc1	palác
se	s	k7c7	s
1400	[number]	k4	1400
místnostmi	místnost	k1gFnPc7	místnost
</s>
</p>
<p>
<s>
Faistos	Faistos	k1gInSc1	Faistos
–	–	k?	–
nejstarší	starý	k2eAgInSc1d3	nejstarší
palác	palác	k1gInSc1	palác
</s>
</p>
<p>
<s>
Kató	Kató	k?	Kató
Zakros	Zakrosa	k1gFnPc2	Zakrosa
</s>
</p>
<p>
<s>
Tilisos	Tilisos	k1gMnSc1	Tilisos
</s>
</p>
<p>
<s>
Malia	Malia	k1gFnSc1	Malia
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Letiště	letiště	k1gNnSc2	letiště
==	==	k?	==
</s>
</p>
<p>
<s>
Kréta	Kréta	k1gFnSc1	Kréta
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgNnPc4	dva
mezinárodní	mezinárodní	k2eAgNnPc4d1	mezinárodní
letiště	letiště	k1gNnPc4	letiště
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Letiště	letiště	k1gNnSc1	letiště
Nikos	Nikos	k1gMnSc1	Nikos
Kazantzakis	Kazantzakis	k1gMnSc1	Kazantzakis
(	(	kIx(	(
<g/>
kód	kód	k1gInSc1	kód
IATA	IATA	kA	IATA
<g/>
:	:	kIx,	:
HER	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
kód	kód	k1gInSc4	kód
ICAO	ICAO	kA	ICAO
<g/>
:	:	kIx,	:
LGIR	LGIR	kA	LGIR
<g/>
)	)	kIx)	)
nedaleko	nedaleko	k7c2	nedaleko
města	město	k1gNnSc2	město
Iraklion	Iraklion	k1gInSc1	Iraklion
</s>
</p>
<p>
<s>
Letiště	letiště	k1gNnSc1	letiště
Daskalogiannis	Daskalogiannis	k1gFnPc2	Daskalogiannis
(	(	kIx(	(
<g/>
kód	kód	k1gInSc4	kód
IATA	IATA	kA	IATA
<g/>
:	:	kIx,	:
CHQ	CHQ	kA	CHQ
<g/>
,	,	kIx,	,
kód	kód	k1gInSc4	kód
ICAO	ICAO	kA	ICAO
<g/>
:	:	kIx,	:
LGSA	LGSA	kA	LGSA
<g/>
)	)	kIx)	)
nedaleko	nedaleko	k7c2	nedaleko
města	město	k1gNnSc2	město
Chania	Chanium	k1gNnSc2	Chanium
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
DIBELKOVÁ	DIBELKOVÁ	kA	DIBELKOVÁ
<g/>
,	,	kIx,	,
Irena	Irena	k1gFnSc1	Irena
<g/>
;	;	kIx,	;
ZÍKOVÁ	Zíková	k1gFnSc1	Zíková
<g/>
,	,	kIx,	,
Marcela	Marcela	k1gFnSc1	Marcela
<g/>
.	.	kIx.	.
</s>
<s>
Kréta	Kréta	k1gFnSc1	Kréta
<g/>
:	:	kIx,	:
nejen	nejen	k6eAd1	nejen
po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
mínojské	mínojský	k2eAgFnSc2d1	mínojská
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
.	.	kIx.	.
194-201	[number]	k4	194-201
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210-6097	[number]	k4	1210-6097
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Mykénská	mykénský	k2eAgFnSc1d1	mykénská
civilizace	civilizace	k1gFnSc1	civilizace
</s>
</p>
<p>
<s>
Invaze	invaze	k1gFnSc1	invaze
na	na	k7c4	na
Krétu	Kréta	k1gFnSc4	Kréta
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kréta	Kréta	k1gFnSc1	Kréta
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Kréta	Kréta	k1gFnSc1	Kréta
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
kreta.rovnou.cz	kreta.rovnou.cz	k1gInSc1	kreta.rovnou.cz
<g/>
,	,	kIx,	,
popisy	popis	k1gInPc1	popis
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
míst	místo	k1gNnPc2	místo
na	na	k7c6	na
Krétě	Kréta	k1gFnSc6	Kréta
a	a	k8xC	a
všeobecné	všeobecný	k2eAgFnPc4d1	všeobecná
informace	informace	k1gFnPc4	informace
</s>
</p>
<p>
<s>
nakrete.cz	nakrete.cza	k1gFnPc2	nakrete.cza
<g/>
,	,	kIx,	,
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
tradice	tradice	k1gFnSc2	tradice
<g/>
,	,	kIx,	,
pláže	pláž	k1gFnSc2	pláž
<g/>
,	,	kIx,	,
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
místa	místo	k1gNnSc2	místo
a	a	k8xC	a
spousta	spousta	k1gFnSc1	spousta
dalších	další	k2eAgFnPc2d1	další
užitečných	užitečný	k2eAgFnPc2d1	užitečná
informací	informace	k1gFnPc2	informace
</s>
</p>
<p>
<s>
Kréta	Kréta	k1gFnSc1	Kréta
-	-	kIx~	-
informace	informace	k1gFnSc1	informace
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
o	o	k7c6	o
Krétě	Kréta	k1gFnSc6	Kréta
</s>
</p>
<p>
<s>
Kréta	Kréta	k1gFnSc1	Kréta
-	-	kIx~	-
Historie	historie	k1gFnSc1	historie
<g/>
,	,	kIx,	,
města	město	k1gNnPc1	město
<g/>
,	,	kIx,	,
vesničky	vesnička	k1gFnPc1	vesnička
<g/>
,	,	kIx,	,
pláže	pláž	k1gFnPc1	pláž
<g/>
,	,	kIx,	,
kláštery	klášter	k1gInPc1	klášter
<g/>
,	,	kIx,	,
tipy	tip	k1gInPc1	tip
na	na	k7c4	na
výlety	výlet	k1gInPc4	výlet
<g/>
.	.	kIx.	.
</s>
</p>
