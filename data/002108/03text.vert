<s>
Madrid	Madrid	k1gInSc1	Madrid
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
a	a	k8xC	a
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
3,2	[number]	k4	3,2
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
v	v	k7c6	v
metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
oblasti	oblast	k1gFnSc6	oblast
žije	žít	k5eAaImIp3nS	žít
kolem	kolem	k7c2	kolem
6,3	[number]	k4	6,3
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
třetí	třetí	k4xOgNnSc4	třetí
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
,	,	kIx,	,
po	po	k7c6	po
Londýně	Londýn	k1gInSc6	Londýn
a	a	k8xC	a
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
a	a	k8xC	a
madridská	madridský	k2eAgFnSc1d1	Madridská
metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc7	třetí
největší	veliký	k2eAgFnSc7d3	veliký
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
,	,	kIx,	,
po	po	k7c6	po
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
celkovou	celkový	k2eAgFnSc4d1	celková
rozlohu	rozloha	k1gFnSc4	rozloha
604,3	[number]	k4	604,3
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Manzanares	Manzanaresa	k1gFnPc2	Manzanaresa
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
jak	jak	k6eAd1	jak
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Madridského	madridský	k2eAgNnSc2d1	madridské
autonomního	autonomní	k2eAgNnSc2d1	autonomní
společenství	společenství	k1gNnSc2	společenství
(	(	kIx(	(
<g/>
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
vlastního	vlastní	k2eAgInSc2d1	vlastní
Madridu	Madrid	k1gInSc2	Madrid
<g/>
,	,	kIx,	,
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
aglomerace	aglomerace	k1gFnSc2	aglomerace
a	a	k8xC	a
okolních	okolní	k2eAgNnPc2d1	okolní
měst	město	k1gNnPc2	město
a	a	k8xC	a
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
toto	tento	k3xDgNnSc1	tento
společenství	společenství	k1gNnSc1	společenství
sousedí	sousedit	k5eAaImIp3nS	sousedit
se	s	k7c7	s
společenstvími	společenství	k1gNnPc7	společenství
Kastilie	Kastilie	k1gFnSc2	Kastilie
a	a	k8xC	a
León	León	k1gMnSc1	León
a	a	k8xC	a
Kastilie-La	Kastilie-La	k1gMnSc1	Kastilie-La
Mancha	Mancha	k1gMnSc1	Mancha
<g/>
.	.	kIx.	.
</s>
<s>
Jakožto	jakožto	k8xS	jakožto
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc1	sídlo
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
sídlo	sídlo	k1gNnSc1	sídlo
španělského	španělský	k2eAgMnSc2d1	španělský
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Madrid	Madrid	k1gInSc1	Madrid
zároveň	zároveň	k6eAd1	zároveň
politickým	politický	k2eAgNnSc7d1	politické
<g/>
,	,	kIx,	,
ekonomickým	ekonomický	k2eAgNnSc7d1	ekonomické
a	a	k8xC	a
kulturním	kulturní	k2eAgNnSc7d1	kulturní
centrem	centrum	k1gNnSc7	centrum
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Současnou	současný	k2eAgFnSc7d1	současná
starostkou	starostka	k1gFnSc7	starostka
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
Manuela	Manuela	k1gFnSc1	Manuela
Carmena	Carmen	k2eAgFnSc1d1	Carmena
z	z	k7c2	z
Ahora	Ahor	k1gInSc2	Ahor
Madrid	Madrid	k1gInSc1	Madrid
<g/>
.	.	kIx.	.
</s>
<s>
Madridská	madridský	k2eAgFnSc1d1	Madridská
aglomerace	aglomerace	k1gFnSc1	aglomerace
má	mít	k5eAaImIp3nS	mít
třetí	třetí	k4xOgFnSc4	třetí
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
HDP	HDP	kA	HDP
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
<g/>
,	,	kIx,	,
vzdělávání	vzdělávání	k1gNnSc6	vzdělávání
<g/>
,	,	kIx,	,
médiích	médium	k1gNnPc6	médium
<g/>
,	,	kIx,	,
módě	móda	k1gFnSc6	móda
<g/>
,	,	kIx,	,
vědě	věda	k1gFnSc6	věda
<g/>
,	,	kIx,	,
kultuře	kultura	k1gFnSc6	kultura
i	i	k8xC	i
umění	umění	k1gNnSc6	umění
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
všechno	všechen	k3xTgNnSc1	všechen
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Madrid	Madrid	k1gInSc1	Madrid
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
globálních	globální	k2eAgNnPc2d1	globální
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
ekonomické	ekonomický	k2eAgFnSc3d1	ekonomická
produkci	produkce	k1gFnSc3	produkce
<g/>
,	,	kIx,	,
vysoké	vysoký	k2eAgFnSc3d1	vysoká
životní	životní	k2eAgFnSc3d1	životní
úrovni	úroveň	k1gFnSc3	úroveň
a	a	k8xC	a
šíří	šíř	k1gFnSc7	šíř
trhu	trh	k1gInSc2	trh
je	být	k5eAaImIp3nS	být
Madrid	Madrid	k1gInSc1	Madrid
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
největší	veliký	k2eAgNnSc4d3	veliký
finanční	finanční	k2eAgNnSc4d1	finanční
centrum	centrum	k1gNnSc4	centrum
jižní	jižní	k2eAgFnSc2d1	jižní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
;	;	kIx,	;
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
vedení	vedení	k1gNnSc2	vedení
mnoha	mnoho	k4c2	mnoho
významných	významný	k2eAgFnPc2d1	významná
španělských	španělský	k2eAgFnPc2d1	španělská
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Telefónica	Telefónicum	k1gNnPc1	Telefónicum
<g/>
,	,	kIx,	,
Iberia	Iberium	k1gNnPc1	Iberium
nebo	nebo	k8xC	nebo
Repsol	Repsol	k1gInSc1	Repsol
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
žebříčku	žebříček	k1gInSc2	žebříček
časopisu	časopis	k1gInSc2	časopis
Monocle	Monocle	k1gFnSc2	Monocle
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
je	být	k5eAaImIp3nS	být
Madrid	Madrid	k1gInSc1	Madrid
sedmnáctým	sedmnáctý	k4xOgInSc7	sedmnáctý
nejvhodnějším	vhodný	k2eAgNnSc7d3	nejvhodnější
městem	město	k1gNnSc7	město
pro	pro	k7c4	pro
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
sídlí	sídlet	k5eAaImIp3nP	sídlet
vrchní	vrchní	k1gMnPc1	vrchní
vedení	vedení	k1gNnSc2	vedení
mnoha	mnoho	k4c2	mnoho
organizací	organizace	k1gFnPc2	organizace
<g/>
:	:	kIx,	:
Světová	světový	k2eAgFnSc1d1	světová
organizace	organizace	k1gFnSc1	organizace
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
(	(	kIx(	(
<g/>
UNWTO	UNWTO	kA	UNWTO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spadá	spadat	k5eAaImIp3nS	spadat
pod	pod	k7c7	pod
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
Organizace	organizace	k1gFnSc1	organizace
ibero-amerických	iberomerický	k2eAgInPc2d1	ibero-americký
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
OEI	OEI	kA	OEI
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
PIOB	PIOB	kA	PIOB
či	či	k8xC	či
SEGIB	SEGIB	kA	SEGIB
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
také	také	k9	také
centrem	centrum	k1gNnSc7	centrum
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
regulátorů	regulátor	k1gInPc2	regulátor
španělského	španělský	k2eAgInSc2d1	španělský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
:	:	kIx,	:
vedení	vedení	k1gNnSc1	vedení
Španělské	španělský	k2eAgFnSc2d1	španělská
královské	královský	k2eAgFnSc2d1	královská
akademie	akademie	k1gFnSc2	akademie
(	(	kIx(	(
<g/>
RAE	RAE	kA	RAE
<g/>
)	)	kIx)	)
či	či	k8xC	či
Instituto	Institut	k2eAgNnSc1d1	Instituto
Cervantes	Cervantes	k1gInSc1	Cervantes
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
Madrid	Madrid	k1gInSc1	Madrid
disponuje	disponovat	k5eAaBmIp3nS	disponovat
moderní	moderní	k2eAgFnSc7d1	moderní
infrastrukturou	infrastruktura	k1gFnSc7	infrastruktura
<g/>
,	,	kIx,	,
mnoho	mnoho	k6eAd1	mnoho
jeho	jeho	k3xOp3gFnPc2	jeho
historických	historický	k2eAgFnPc2d1	historická
čtvrtí	čtvrt	k1gFnPc2	čtvrt
a	a	k8xC	a
ulic	ulice	k1gFnPc2	ulice
si	se	k3xPyFc3	se
stále	stále	k6eAd1	stále
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
svůj	svůj	k3xOyFgInSc4	svůj
původní	původní	k2eAgInSc4d1	původní
vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
specifickou	specifický	k2eAgFnSc4d1	specifická
náladu	nálada	k1gFnSc4	nálada
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
přední	přední	k2eAgFnPc4d1	přední
madridské	madridský	k2eAgFnPc4d1	Madridská
pamětihodnosti	pamětihodnost	k1gFnPc4	pamětihodnost
patří	patřit	k5eAaImIp3nS	patřit
Královský	královský	k2eAgInSc1d1	královský
palác	palác	k1gInSc1	palác
(	(	kIx(	(
<g/>
Palacio	Palacio	k6eAd1	Palacio
Real	Real	k1gInSc1	Real
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Královské	královský	k2eAgNnSc1d1	královské
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
park	park	k1gInSc1	park
Buen	Buena	k1gFnPc2	Buena
Retiro	Retiro	k1gNnSc1	Retiro
<g/>
,	,	kIx,	,
budova	budova	k1gFnSc1	budova
Národní	národní	k2eAgFnSc2d1	národní
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
,	,	kIx,	,
řada	řada	k1gFnSc1	řada
galerií	galerie	k1gFnPc2	galerie
a	a	k8xC	a
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
Museo	Museo	k6eAd1	Museo
del	del	k?	del
Prado	Prado	k1gNnSc4	Prado
<g/>
,	,	kIx,	,
Museo	Museo	k6eAd1	Museo
Reina	Rein	k2eAgFnSc1d1	Reina
Sofía	Sofía	k1gFnSc1	Sofía
či	či	k8xC	či
muzeum	muzeum	k1gNnSc1	muzeum
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
Museo	Museo	k1gMnSc1	Museo
Thyssen-Bornemisza	Thyssen-Bornemisz	k1gMnSc2	Thyssen-Bornemisz
<g/>
.	.	kIx.	.
</s>
<s>
Symbolem	symbol	k1gInSc7	symbol
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
palác	palác	k1gInSc4	palác
a	a	k8xC	a
fontána	fontána	k1gFnSc1	fontána
na	na	k7c4	na
náměstí	náměstí	k1gNnSc4	náměstí
Plaza	plaz	k1gMnSc2	plaz
Cibeles	Cibeles	k1gInSc4	Cibeles
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
rovněž	rovněž	k9	rovněž
sídlí	sídlet	k5eAaImIp3nP	sídlet
dva	dva	k4xCgInPc1	dva
světoznámé	světoznámý	k2eAgInPc1d1	světoznámý
fotbalové	fotbalový	k2eAgInPc1d1	fotbalový
kluby	klub	k1gInPc1	klub
Real	Real	k1gInSc4	Real
Madrid	Madrid	k1gInSc1	Madrid
a	a	k8xC	a
Atlético	Atlético	k1gNnSc1	Atlético
Madrid	Madrid	k1gInSc1	Madrid
<g/>
.	.	kIx.	.
</s>
<s>
Heslo	heslo	k1gNnSc1	heslo
města	město	k1gNnSc2	město
zní	znět	k5eAaImIp3nS	znět
španělsky	španělsky	k6eAd1	španělsky
"	"	kIx"	"
<g/>
Fui	Fui	k1gMnSc5	Fui
sobre	sobr	k1gMnSc5	sobr
agua	aguum	k1gNnPc1	aguum
edificada	edificada	k1gFnSc1	edificada
<g/>
,	,	kIx,	,
mis	mísit	k5eAaImRp2nS	mísit
muros	muros	k1gInSc1	muros
de	de	k?	de
fuego	fuego	k6eAd1	fuego
son	son	k1gInSc4	son
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
-	-	kIx~	-
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
Byl	být	k5eAaImAgInS	být
jsem	být	k5eAaImIp1nS	být
na	na	k7c6	na
vodě	voda	k1gFnSc6	voda
zbudován	zbudován	k2eAgInSc1d1	zbudován
<g/>
,	,	kIx,	,
mé	můj	k3xOp1gFnPc4	můj
hradby	hradba	k1gFnPc4	hradba
z	z	k7c2	z
ohně	oheň	k1gInSc2	oheň
jsou	být	k5eAaImIp3nP	být
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Město	město	k1gNnSc1	město
Madrid	Madrid	k1gInSc1	Madrid
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
geografickém	geografický	k2eAgInSc6d1	geografický
středu	střed	k1gInSc6	střed
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
souřadnice	souřadnice	k1gFnSc1	souřadnice
40	[number]	k4	40
°	°	k?	°
26	[number]	k4	26
'	'	kIx"	'
s.	s.	k?	s.
š.	š.	k?	š.
3	[number]	k4	3
<g/>
°	°	k?	°
41	[number]	k4	41
<g/>
'	'	kIx"	'
z.	z.	k?	z.
d.	d.	k?	d.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
průměrná	průměrný	k2eAgFnSc1d1	průměrná
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
je	být	k5eAaImIp3nS	být
667	[number]	k4	667
m.	m.	k?	m.
Madridské	madridský	k2eAgNnSc1d1	madridské
klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
kontinentální-středomořské	kontinentálnítředomořský	k2eAgNnSc1d1	kontinentální-středomořský
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
městskými	městský	k2eAgFnPc7d1	městská
podmínkami	podmínka	k1gFnPc7	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Zimy	zima	k1gFnPc1	zima
jsou	být	k5eAaImIp3nP	být
chladné	chladný	k2eAgFnPc1d1	chladná
<g/>
,	,	kIx,	,
s	s	k7c7	s
teplotami	teplota	k1gFnPc7	teplota
pod	pod	k7c4	pod
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
časté	častý	k2eAgInPc1d1	častý
mrazy	mráz	k1gInPc1	mráz
a	a	k8xC	a
občas	občas	k6eAd1	občas
sněží	sněžit	k5eAaImIp3nS	sněžit
<g/>
.	.	kIx.	.
</s>
<s>
Léta	léto	k1gNnPc1	léto
jsou	být	k5eAaImIp3nP	být
horká	horký	k2eAgFnSc1d1	horká
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
kolem	kolem	k7c2	kolem
24	[number]	k4	24
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
a	a	k8xC	a
srpnu	srpen	k1gInSc6	srpen
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
maximy	maximum	k1gNnPc7	maximum
nad	nad	k7c4	nad
35	[number]	k4	35
°	°	k?	°
<g/>
C.	C.	kA	C.
Roční	roční	k2eAgInSc1d1	roční
teplotní	teplotní	k2eAgInSc1d1	teplotní
rozsah	rozsah	k1gInSc1	rozsah
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgMnSc1d1	vysoký
(	(	kIx(	(
<g/>
19	[number]	k4	19
stupňů	stupeň	k1gInPc2	stupeň
<g/>
)	)	kIx)	)
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
velké	velký	k2eAgFnSc2d1	velká
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
od	od	k7c2	od
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
600	[number]	k4	600
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
400	[number]	k4	400
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
širokými	široký	k2eAgFnPc7d1	široká
třídami	třída	k1gFnPc7	třída
se	s	k7c7	s
zelenými	zelený	k2eAgInPc7d1	zelený
pruhy	pruh	k1gInPc7	pruh
<g/>
,	,	kIx,	,
velkými	velký	k2eAgFnPc7d1	velká
kruhovými	kruhový	k2eAgFnPc7d1	kruhová
náměstími	náměstí	k1gNnPc7	náměstí
a	a	k8xC	a
velkým	velký	k2eAgInSc7d1	velký
počtem	počet	k1gInSc7	počet
pompézních	pompézní	k2eAgFnPc2d1	pompézní
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
renesančního	renesanční	k2eAgInSc2d1	renesanční
stylu	styl	k1gInSc2	styl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zcela	zcela	k6eAd1	zcela
hypermoderně	hypermoderně	k6eAd1	hypermoderně
vypadajících	vypadající	k2eAgInPc2d1	vypadající
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
historického	historický	k2eAgNnSc2d1	historické
centra	centrum	k1gNnSc2	centrum
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
královský	královský	k2eAgInSc1d1	královský
palác	palác	k1gInSc1	palác
Palacio	Palacio	k1gNnSc1	Palacio
Real	Real	k1gInSc1	Real
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
852	[number]	k4	852
zde	zde	k6eAd1	zde
arabský	arabský	k2eAgMnSc1d1	arabský
emír	emír	k1gMnSc1	emír
Muhammad	Muhammad	k1gInSc1	Muhammad
I.	I.	kA	I.
nechal	nechat	k5eAaPmAgInS	nechat
postavit	postavit	k5eAaPmF	postavit
tzv.	tzv.	kA	tzv.
Alcázar	Alcázar	k1gInSc4	Alcázar
-	-	kIx~	-
pevnostní	pevnostní	k2eAgInSc4d1	pevnostní
palác	palác	k1gInSc4	palác
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
Madžrít	Madžrít	k1gMnSc1	Madžrít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
reconquistě	reconquist	k1gInSc6	reconquist
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
používali	používat	k5eAaImAgMnP	používat
palác	palác	k1gInSc4	palác
křesťanští	křesťanský	k2eAgMnPc1d1	křesťanský
panovníci	panovník	k1gMnPc1	panovník
jako	jako	k8xS	jako
lovecký	lovecký	k2eAgInSc4d1	lovecký
zámek	zámek	k1gInSc4	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
jména	jméno	k1gNnSc2	jméno
Madžrít	Madžrít	k1gFnSc1	Madžrít
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
jméno	jméno	k1gNnSc1	jméno
města	město	k1gNnSc2	město
Madrid	Madrid	k1gInSc1	Madrid
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1561	[number]	k4	1561
se	se	k3xPyFc4	se
Madrid	Madrid	k1gInSc1	Madrid
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
maurský	maurský	k2eAgInSc1d1	maurský
palác	palác	k1gInSc1	palác
sloužil	sloužit	k5eAaImAgInS	sloužit
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1734	[number]	k4	1734
jako	jako	k8xS	jako
královské	královský	k2eAgNnSc4d1	královské
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
vyhoření	vyhoření	k1gNnSc6	vyhoření
byla	být	k5eAaImAgFnS	být
stavba	stavba	k1gFnSc1	stavba
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
novým	nový	k2eAgInSc7d1	nový
zámkem	zámek	k1gInSc7	zámek
Palacio	Palacio	k1gMnSc1	Palacio
real	real	k1gMnSc1	real
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
desetkrát	desetkrát	k6eAd1	desetkrát
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
londýnský	londýnský	k2eAgInSc4d1	londýnský
Buckinghamský	buckinghamský	k2eAgInSc4d1	buckinghamský
palác	palác	k1gInSc4	palác
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
jeho	jeho	k3xOp3gInPc2	jeho
sálů	sál	k1gInPc2	sál
s	s	k7c7	s
nástropními	nástropní	k2eAgFnPc7d1	nástropní
freskami	freska	k1gFnPc7	freska
<g/>
,	,	kIx,	,
zlaceným	zlacený	k2eAgNnSc7d1	zlacené
štukováním	štukování	k1gNnSc7	štukování
a	a	k8xC	a
lustry	lustrum	k1gNnPc7	lustrum
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
téměř	téměř	k6eAd1	téměř
100	[number]	k4	100
kg	kg	kA	kg
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
být	být	k5eAaImF	být
až	až	k6eAd1	až
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
skvostům	skvost	k1gInPc3	skvost
paláce	palác	k1gInSc2	palác
patří	patřit	k5eAaImIp3nP	patřit
sály	sál	k1gInPc1	sál
Trůnní	trůnní	k2eAgInPc1d1	trůnní
<g/>
,	,	kIx,	,
Gaspariniho	Gasparini	k1gMnSc4	Gasparini
a	a	k8xC	a
Porcelánový	porcelánový	k2eAgInSc4d1	porcelánový
<g/>
.	.	kIx.	.
</s>
<s>
Španělský	španělský	k2eAgMnSc1d1	španělský
král	král	k1gMnSc1	král
používá	používat	k5eAaImIp3nS	používat
tuto	tento	k3xDgFnSc4	tento
monumentální	monumentální	k2eAgFnSc4d1	monumentální
stavbu	stavba	k1gFnSc4	stavba
již	již	k6eAd1	již
jen	jen	k9	jen
k	k	k7c3	k
reprezentačním	reprezentační	k2eAgInPc3d1	reprezentační
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Královský	královský	k2eAgInSc1d1	královský
palác	palác	k1gInSc1	palác
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejmonumentálnějším	monumentální	k2eAgFnPc3d3	nejmonumentálnější
a	a	k8xC	a
nejhezčím	hezký	k2eAgFnPc3d3	nejhezčí
stavbám	stavba	k1gFnPc3	stavba
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Palác	palác	k1gInSc1	palác
je	být	k5eAaImIp3nS	být
obklopen	obklopit	k5eAaPmNgInS	obklopit
parky	park	k1gInPc7	park
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
Casa	Casa	k1gFnSc1	Casa
de	de	k?	de
Campo	Campa	k1gFnSc5	Campa
<g/>
,	,	kIx,	,
Jardin	Jardin	k2eAgInSc1d1	Jardin
de	de	k?	de
Sabatini	Sabatin	k2eAgMnPc1d1	Sabatin
nebo	nebo	k8xC	nebo
park	park	k1gInSc1	park
atrakcí	atrakce	k1gFnPc2	atrakce
Parque	Parqu	k1gFnSc2	Parqu
de	de	k?	de
Atracciones	Atracciones	k1gMnSc1	Atracciones
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
paláci	palác	k1gInSc3	palác
stojí	stát	k5eAaImIp3nS	stát
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
kostelů	kostel	k1gInPc2	kostel
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
katedrála	katedrála	k1gFnSc1	katedrála
Virgen	Virgen	k1gInSc1	Virgen
de	de	k?	de
la	la	k1gNnSc2	la
Almudena	Almuden	k1gMnSc2	Almuden
<g/>
.	.	kIx.	.
</s>
<s>
Blízko	blízko	k7c2	blízko
centra	centrum	k1gNnSc2	centrum
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
obrazárna	obrazárna	k1gFnSc1	obrazárna
Museo	Museo	k1gNnSc1	Museo
del	del	k?	del
Prado	Prado	k1gNnSc1	Prado
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
nejen	nejen	k6eAd1	nejen
možné	možný	k2eAgNnSc1d1	možné
zhlédnout	zhlédnout	k5eAaPmF	zhlédnout
některé	některý	k3yIgMnPc4	některý
habsburské	habsburský	k2eAgMnPc4d1	habsburský
panovníky	panovník	k1gMnPc4	panovník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
jedinečnou	jedinečný	k2eAgFnSc4d1	jedinečná
sbírku	sbírka	k1gFnSc4	sbírka
šesti	šest	k4xCc2	šest
tisíc	tisíc	k4xCgInSc1	tisíc
malířských	malířský	k2eAgNnPc2d1	malířské
děl	dělo	k1gNnPc2	dělo
mistrů	mistr	k1gMnPc2	mistr
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Plaza	plaz	k1gMnSc4	plaz
de	de	k?	de
Españ	Españ	k1gMnSc1	Españ
je	být	k5eAaImIp3nS	být
obdélníkové	obdélníkový	k2eAgNnSc4d1	obdélníkové
náměstí	náměstí	k1gNnSc4	náměstí
s	s	k7c7	s
parčíkem	parčík	k1gInSc7	parčík
a	a	k8xC	a
pomníkem	pomník	k1gInSc7	pomník
Miguela	Miguel	k1gMnSc2	Miguel
de	de	k?	de
Cervantese	Cervantese	k1gFnSc2	Cervantese
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc2	jeho
románových	románový	k2eAgFnPc2d1	románová
postav	postava	k1gFnPc2	postava
Dona	Don	k1gMnSc4	Don
Quijota	Quijot	k1gMnSc4	Quijot
a	a	k8xC	a
Sancho	Sancha	k1gFnSc5	Sancha
Panzy	Panza	k1gFnSc2	Panza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parku	park	k1gInSc6	park
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
různé	různý	k2eAgFnPc1d1	různá
vodní	vodní	k2eAgFnPc1d1	vodní
plochy	plocha	k1gFnPc1	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
náměstí	náměstí	k1gNnSc4	náměstí
Plaza	plaz	k1gMnSc2	plaz
Mayor	Mayor	k1gInSc4	Mayor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejkrásnějším	krásný	k2eAgNnPc3d3	nejkrásnější
náměstím	náměstí	k1gNnPc3	náměstí
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
<g/>
,	,	kIx,	,
přihlížela	přihlížet	k5eAaImAgFnS	přihlížet
dříve	dříve	k6eAd2	dříve
vrchnost	vrchnost	k1gFnSc4	vrchnost
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
balkónu	balkón	k1gInSc2	balkón
popravám	poprava	k1gFnPc3	poprava
<g/>
,	,	kIx,	,
býčím	býčí	k2eAgInPc3d1	býčí
zápasům	zápas	k1gInPc3	zápas
a	a	k8xC	a
upalovaní	upalovaný	k2eAgMnPc1d1	upalovaný
kacířů	kacíř	k1gMnPc2	kacíř
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
symbolů	symbol	k1gInPc2	symbol
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
socha	socha	k1gFnSc1	socha
El	Ela	k1gFnPc2	Ela
oso	osa	k1gFnSc5	osa
y	y	k?	y
el	ela	k1gFnPc2	ela
Madroñ	Madroñ	k1gFnSc1	Madroñ
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Puerta	Puert	k1gMnSc2	Puert
del	del	k?	del
Sol	sol	k1gInSc1	sol
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
socha	socha	k1gFnSc1	socha
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgInSc4d1	pocházející
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
z	z	k7c2	z
dílny	dílna	k1gFnSc2	dílna
sochaře	sochař	k1gMnSc2	sochař
Antonia	Antonio	k1gMnSc2	Antonio
Navarra	Navarra	k1gFnSc1	Navarra
Santafé	Santafé	k1gNnPc1	Santafé
<g/>
,	,	kIx,	,
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
medvěda	medvěd	k1gMnSc4	medvěd
pojídajícího	pojídající	k2eAgInSc2d1	pojídající
plody	plod	k1gInPc4	plod
planiky	planik	k1gInPc4	planik
-	-	kIx~	-
rostliny	rostlina	k1gFnPc4	rostlina
podobné	podobný	k2eAgFnPc4d1	podobná
jahodníku	jahodník	k1gInSc3	jahodník
<g/>
.	.	kIx.	.
</s>
<s>
Populární	populární	k2eAgFnSc1d1	populární
socha	socha	k1gFnSc1	socha
tak	tak	k9	tak
reflektuje	reflektovat	k5eAaImIp3nS	reflektovat
znak	znak	k1gInSc1	znak
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
právě	právě	k9	právě
obraz	obraz	k1gInSc1	obraz
medvěda	medvěd	k1gMnSc2	medvěd
pojídajícího	pojídající	k2eAgInSc2d1	pojídající
planiku	planik	k1gInSc2	planik
na	na	k7c6	na
modrém	modrý	k2eAgNnSc6d1	modré
hvězdnatém	hvězdnatý	k2eAgNnSc6d1	hvězdnaté
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
museu	museum	k1gNnSc6	museum
Museo	Museo	k1gMnSc1	Museo
Arqueológico	Arqueológico	k1gMnSc1	Arqueológico
Nacional	Nacional	k1gMnSc1	Nacional
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
zhlédnout	zhlédnout	k5eAaPmF	zhlédnout
velký	velký	k2eAgInSc4d1	velký
počet	počet	k1gInSc4	počet
exponátů	exponát	k1gInPc2	exponát
od	od	k7c2	od
začátků	začátek	k1gInPc2	začátek
lidstva	lidstvo	k1gNnSc2	lidstvo
přes	přes	k7c4	přes
dobu	doba	k1gFnSc4	doba
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
<g/>
,	,	kIx,	,
staré	starý	k2eAgMnPc4d1	starý
Egypťany	Egypťan	k1gMnPc4	Egypťan
<g/>
,	,	kIx,	,
iberské	iberský	k2eAgInPc4d1	iberský
národy	národ	k1gInPc7	národ
<g/>
,	,	kIx,	,
Řeky	Řek	k1gMnPc7	Řek
a	a	k8xC	a
Římany	Říman	k1gMnPc7	Říman
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c4	po
středověk	středověk	k1gInSc4	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
je	být	k5eAaImIp3nS	být
hlavně	hlavně	k9	hlavně
egyptská	egyptský	k2eAgFnSc1d1	egyptská
mumie	mumie	k1gFnSc1	mumie
kněžky	kněžka	k1gFnSc2	kněžka
v	v	k7c6	v
sarkofágu	sarkofág	k1gInSc6	sarkofág
<g/>
,	,	kIx,	,
socha	socha	k1gFnSc1	socha
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Dáma	dáma	k1gFnSc1	dáma
z	z	k7c2	z
Elche	Elch	k1gInSc2	Elch
a	a	k8xC	a
také	také	k9	také
kopie	kopie	k1gFnPc1	kopie
celé	celý	k2eAgFnSc2d1	celá
jeskyně	jeskyně	k1gFnSc2	jeskyně
Altamira	Altamira	k1gFnSc1	Altamira
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
přístupná	přístupný	k2eAgFnSc1d1	přístupná
ze	z	k7c2	z
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
zajímavou	zajímavý	k2eAgFnSc7d1	zajímavá
připomínkou	připomínka	k1gFnSc7	připomínka
straověkých	straověký	k2eAgFnPc2d1	straověký
civilizací	civilizace	k1gFnPc2	civilizace
je	být	k5eAaImIp3nS	být
egyptský	egyptský	k2eAgInSc1d1	egyptský
chrám	chrám	k1gInSc1	chrám
Debod	Debod	k1gInSc1	Debod
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Parque	Parque	k1gFnSc6	Parque
del	del	k?	del
Oeste	Oest	k1gInSc5	Oest
nedaleko	nedaleko	k7c2	nedaleko
Královského	královský	k2eAgInSc2d1	královský
paláce	palác	k1gInSc2	palác
<g/>
.	.	kIx.	.
</s>
<s>
Madrid	Madrid	k1gInSc1	Madrid
je	být	k5eAaImIp3nS	být
administartivně	administartivně	k6eAd1	administartivně
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
21	[number]	k4	21
obvodů	obvod	k1gInPc2	obvod
(	(	kIx(	(
<g/>
distritos	distritos	k1gInSc1	distritos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
128	[number]	k4	128
čtvrtí	čtvrt	k1gFnPc2	čtvrt
(	(	kIx(	(
<g/>
barrios	barrios	k1gInSc1	barrios
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Centro	Centro	k1gNnSc1	Centro
<g/>
:	:	kIx,	:
Palacio	Palacio	k1gMnSc1	Palacio
<g/>
,	,	kIx,	,
Embajadores	Embajadores	k1gMnSc1	Embajadores
<g/>
,	,	kIx,	,
Cortes	Cortes	k1gMnSc1	Cortes
<g/>
,	,	kIx,	,
Justicia	Justicia	k1gFnSc1	Justicia
<g/>
,	,	kIx,	,
Universidad	Universidad	k1gInSc1	Universidad
<g/>
,	,	kIx,	,
Sol	sol	k1gInSc1	sol
<g/>
.	.	kIx.	.
</s>
<s>
Arganzuela	Arganzuela	k1gFnSc1	Arganzuela
<g/>
:	:	kIx,	:
Imperial	Imperial	k1gInSc1	Imperial
<g/>
,	,	kIx,	,
Acacias	Acacias	k1gInSc1	Acacias
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Chopera	Chopero	k1gNnSc2	Chopero
<g/>
,	,	kIx,	,
Legazpi	Legazpi	k1gNnSc2	Legazpi
<g/>
,	,	kIx,	,
Delicias	Delicias	k1gMnSc1	Delicias
<g/>
,	,	kIx,	,
Palos	Palos	k1gMnSc1	Palos
de	de	k?	de
Moguer	Moguer	k1gMnSc1	Moguer
<g/>
,	,	kIx,	,
Atocha	Atocha	k1gMnSc1	Atocha
<g/>
.	.	kIx.	.
</s>
<s>
Retiro	Retiro	k1gNnSc1	Retiro
<g/>
:	:	kIx,	:
Pacífico	Pacífico	k1gMnSc1	Pacífico
<g/>
,	,	kIx,	,
Adelfas	Adelfas	k1gMnSc1	Adelfas
<g/>
,	,	kIx,	,
Estrella	Estrella	k1gMnSc1	Estrella
<g/>
,	,	kIx,	,
Ibiza	Ibiza	k1gFnSc1	Ibiza
<g/>
,	,	kIx,	,
Jerónimos	Jerónimos	k1gMnSc1	Jerónimos
<g/>
,	,	kIx,	,
Niñ	Niñ	k1gMnSc1	Niñ
Jesús	Jesús	k1gInSc1	Jesús
<g/>
.	.	kIx.	.
</s>
<s>
Salamanca	Salamanca	k1gMnSc1	Salamanca
<g/>
:	:	kIx,	:
Recoletos	Recoletos	k1gMnSc1	Recoletos
<g/>
,	,	kIx,	,
Goya	Goya	k1gMnSc1	Goya
<g/>
,	,	kIx,	,
Parque	Parque	k1gFnSc1	Parque
de	de	k?	de
las	laso	k1gNnPc2	laso
Avenidas	Avenidasa	k1gFnPc2	Avenidasa
<g/>
,	,	kIx,	,
Fuente	Fuent	k1gInSc5	Fuent
del	del	k?	del
Berro	Berra	k1gMnSc5	Berra
<g/>
,	,	kIx,	,
Guindalera	Guindaler	k1gMnSc4	Guindaler
<g/>
,	,	kIx,	,
Lista	lista	k1gFnSc1	lista
<g/>
,	,	kIx,	,
Castellana	Castellana	k1gFnSc1	Castellana
<g/>
.	.	kIx.	.
</s>
<s>
Chamartín	Chamartín	k1gMnSc1	Chamartín
<g/>
:	:	kIx,	:
El	Ela	k1gFnPc2	Ela
Viso	viso	k1gNnSc1	viso
<g/>
,	,	kIx,	,
Prosperidad	Prosperidad	k1gInSc1	Prosperidad
<g/>
,	,	kIx,	,
Ciudad	Ciudad	k1gInSc1	Ciudad
Jardín	Jardín	k1gInSc1	Jardín
<g/>
,	,	kIx,	,
Hispanoamérica	Hispanoamérica	k1gFnSc1	Hispanoamérica
<g/>
,	,	kIx,	,
Nueva	Nueva	k1gFnSc1	Nueva
Españ	Españ	k1gFnSc1	Españ
<g/>
,	,	kIx,	,
Castilla	Castilla	k1gFnSc1	Castilla
<g/>
.	.	kIx.	.
</s>
<s>
Tetuán	Tetuán	k1gMnSc1	Tetuán
<g/>
:	:	kIx,	:
Bellas	Bellas	k1gMnSc1	Bellas
Vistas	Vistas	k1gMnSc1	Vistas
<g/>
,	,	kIx,	,
Cuatro	Cuatro	k1gNnSc1	Cuatro
Caminos	Caminosa	k1gFnPc2	Caminosa
<g/>
,	,	kIx,	,
Castillejos	Castillejosa	k1gFnPc2	Castillejosa
<g/>
,	,	kIx,	,
Almenara	Almenara	k1gFnSc1	Almenara
<g/>
,	,	kIx,	,
Valdeacederas	Valdeacederasa	k1gFnPc2	Valdeacederasa
<g/>
,	,	kIx,	,
Berruguete	Berrugue	k1gNnSc2	Berrugue
<g/>
.	.	kIx.	.
</s>
<s>
Chamberí	Chamberí	k1gNnSc1	Chamberí
<g/>
:	:	kIx,	:
Gaztambide	Gaztambid	k1gMnSc5	Gaztambid
<g/>
,	,	kIx,	,
Arapiles	Arapiles	k1gMnSc1	Arapiles
<g/>
,	,	kIx,	,
Trafalgar	Trafalgar	k1gMnSc1	Trafalgar
<g/>
,	,	kIx,	,
Almagro	Almagra	k1gFnSc5	Almagra
<g/>
,	,	kIx,	,
Vallehermoso	Vallehermosa	k1gFnSc5	Vallehermosa
<g/>
,	,	kIx,	,
Ríos	Ríos	k1gInSc4	Ríos
Rosas	Rosasa	k1gFnPc2	Rosasa
<g/>
.	.	kIx.	.
</s>
<s>
Fuencarral-El	Fuencarral-El	k1gInSc1	Fuencarral-El
Pardo	Pardo	k1gNnSc1	Pardo
<g/>
:	:	kIx,	:
El	Ela	k1gFnPc2	Ela
Pardo	Pardo	k1gNnSc1	Pardo
<g/>
,	,	kIx,	,
Fuentelarreina	Fuentelarreina	k1gMnSc1	Fuentelarreina
<g/>
,	,	kIx,	,
Peñ	Peñ	k1gMnSc1	Peñ
<g/>
,	,	kIx,	,
Barrio	Barrio	k1gMnSc1	Barrio
del	del	k?	del
Pilar	Pilar	k1gInSc1	Pilar
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Paz	Paz	k1gFnPc2	Paz
<g/>
,	,	kIx,	,
Valverde	Valverd	k1gMnSc5	Valverd
<g/>
,	,	kIx,	,
Mirasierra	Mirasierro	k1gNnSc2	Mirasierro
<g/>
,	,	kIx,	,
El	Ela	k1gFnPc2	Ela
Goloso	Golosa	k1gFnSc5	Golosa
<g/>
.	.	kIx.	.
</s>
<s>
Moncloa-Aravaca	Moncloa-Aravaca	k1gFnSc1	Moncloa-Aravaca
<g/>
:	:	kIx,	:
Casa	Casa	k1gFnSc1	Casa
de	de	k?	de
Campo	Campa	k1gFnSc5	Campa
<g/>
,	,	kIx,	,
Argüelles	Argüelles	k1gMnSc1	Argüelles
<g/>
,	,	kIx,	,
Ciudad	Ciudad	k1gInSc1	Ciudad
Universitaria	Universitarium	k1gNnSc2	Universitarium
<g/>
,	,	kIx,	,
Valdezarza	Valdezarza	k1gFnSc1	Valdezarza
<g/>
,	,	kIx,	,
Valdemarín	Valdemarína	k1gFnPc2	Valdemarína
<g/>
,	,	kIx,	,
El	Ela	k1gFnPc2	Ela
Plantío	Plantío	k6eAd1	Plantío
<g/>
,	,	kIx,	,
Aravaca	Aravaca	k1gFnSc1	Aravaca
<g/>
.	.	kIx.	.
</s>
<s>
Latina	latina	k1gFnSc1	latina
<g/>
:	:	kIx,	:
Los	los	k1gInSc1	los
Cármenes	Cármenes	k1gInSc1	Cármenes
<g/>
,	,	kIx,	,
Puerta	Puerta	k1gFnSc1	Puerta
del	del	k?	del
Ángel	Ángel	k1gInSc1	Ángel
<g/>
,	,	kIx,	,
Lucero	Lucero	k1gNnSc1	Lucero
<g/>
,	,	kIx,	,
Aluche	Aluche	k1gNnSc1	Aluche
<g/>
,	,	kIx,	,
Las	laso	k1gNnPc2	laso
Águilas	Águilasa	k1gFnPc2	Águilasa
<g/>
,	,	kIx,	,
Campamento	Campamento	k1gNnSc1	Campamento
<g/>
,	,	kIx,	,
Cuatro	Cuatro	k1gNnSc1	Cuatro
Vientos	Vientosa	k1gFnPc2	Vientosa
<g/>
.	.	kIx.	.
</s>
<s>
Carabanchel	Carabanchel	k1gMnSc1	Carabanchel
<g/>
:	:	kIx,	:
Comillas	Comillas	k1gMnSc1	Comillas
<g/>
,	,	kIx,	,
Opañ	Opañ	k1gMnSc1	Opañ
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
Isidro	Isidra	k1gFnSc5	Isidra
<g/>
,	,	kIx,	,
Vista	vista	k2eAgMnSc5d1	vista
Alegre	Alegr	k1gMnSc5	Alegr
<g/>
,	,	kIx,	,
Puerta	Puerta	k1gFnSc1	Puerta
Bonita	bonita	k1gFnSc1	bonita
<g/>
,	,	kIx,	,
Buenavista	Buenavista	k1gMnSc1	Buenavista
<g/>
,	,	kIx,	,
Abrantes	Abrantes	k1gMnSc1	Abrantes
<g/>
.	.	kIx.	.
</s>
<s>
Usera	Usera	k1gMnSc1	Usera
<g/>
:	:	kIx,	:
Orcasitas	Orcasitas	k1gMnSc1	Orcasitas
<g/>
,	,	kIx,	,
Orcasur	Orcasur	k1gMnSc1	Orcasur
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
Fermín	Fermín	k1gMnSc1	Fermín
<g/>
,	,	kIx,	,
Almendrales	Almendrales	k1gMnSc1	Almendrales
<g/>
,	,	kIx,	,
Moscardó	Moscardó	k1gMnSc1	Moscardó
<g/>
,	,	kIx,	,
Zofío	Zofío	k1gMnSc1	Zofío
<g/>
,	,	kIx,	,
Pradolongo	Pradolongo	k1gMnSc1	Pradolongo
<g/>
.	.	kIx.	.
</s>
<s>
Puente	Puent	k1gMnSc5	Puent
de	de	k?	de
Vallecas	Vallecas	k1gMnSc1	Vallecas
<g/>
:	:	kIx,	:
Entrevías	Entrevías	k1gMnSc1	Entrevías
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
Diego	Diego	k1gMnSc1	Diego
<g/>
,	,	kIx,	,
Palomeras	Palomeras	k1gMnSc1	Palomeras
Bajas	Bajas	k1gMnSc1	Bajas
<g/>
,	,	kIx,	,
Palomeras	Palomeras	k1gMnSc1	Palomeras
Sureste	Surest	k1gInSc5	Surest
<g/>
,	,	kIx,	,
Portazgo	Portazgo	k1gNnSc1	Portazgo
<g/>
,	,	kIx,	,
Numancia	Numancia	k1gFnSc1	Numancia
<g/>
.	.	kIx.	.
</s>
<s>
Moratalaz	Moratalaz	k1gInSc1	Moratalaz
<g/>
:	:	kIx,	:
Pavones	Pavones	k1gMnSc1	Pavones
<g/>
,	,	kIx,	,
Horcajo	Horcajo	k1gMnSc1	Horcajo
<g/>
,	,	kIx,	,
Marroquina	Marroquina	k1gFnSc1	Marroquina
<g/>
,	,	kIx,	,
Media	medium	k1gNnPc1	medium
Legua	Legua	k1gFnSc1	Legua
<g/>
,	,	kIx,	,
Fontarrón	Fontarrón	k1gInSc1	Fontarrón
<g/>
,	,	kIx,	,
Vinateros	Vinaterosa	k1gFnPc2	Vinaterosa
<g/>
.	.	kIx.	.
</s>
<s>
Ciudad	Ciudad	k1gInSc1	Ciudad
Lineal	Lineal	k1gInSc1	Lineal
<g/>
:	:	kIx,	:
Ventas	Ventas	k1gInSc1	Ventas
<g/>
,	,	kIx,	,
Pueblo	Pueblo	k1gFnPc1	Pueblo
Nuevo	Nuevo	k1gNnSc1	Nuevo
<g/>
,	,	kIx,	,
Quintana	Quintana	k1gFnSc1	Quintana
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Concepción	Concepción	k1gMnSc1	Concepción
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
Pascual	Pascual	k1gMnSc1	Pascual
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
Juan	Juan	k1gMnSc1	Juan
Bautista	Bautista	k1gMnSc1	Bautista
<g/>
,	,	kIx,	,
Colina	Colina	k1gMnSc1	Colina
<g/>
,	,	kIx,	,
Atalaya	Atalaya	k1gMnSc1	Atalaya
<g/>
,	,	kIx,	,
Costillares	Costillares	k1gMnSc1	Costillares
<g/>
.	.	kIx.	.
</s>
<s>
Hortaleza	Hortaleza	k1gFnSc1	Hortaleza
<g/>
:	:	kIx,	:
Palomas	Palomas	k1gMnSc1	Palomas
<g/>
,	,	kIx,	,
Valdefuentes	Valdefuentes	k1gMnSc1	Valdefuentes
<g/>
,	,	kIx,	,
Canillas	Canillas	k1gMnSc1	Canillas
<g/>
,	,	kIx,	,
Pinar	Pinar	k1gMnSc1	Pinar
del	del	k?	del
Rey	Rea	k1gFnSc2	Rea
<g/>
,	,	kIx,	,
Apóstol	Apóstol	k1gInSc4	Apóstol
Santiago	Santiago	k1gNnSc4	Santiago
<g/>
,	,	kIx,	,
Piovera	Piovera	k1gFnSc1	Piovera
<g/>
.	.	kIx.	.
</s>
<s>
Villaverde	Villaverde	k6eAd1	Villaverde
<g/>
:	:	kIx,	:
San	San	k1gFnSc1	San
Andrés	Andrés	k1gInSc1	Andrés
<g/>
,	,	kIx,	,
San	San	k1gFnSc1	San
Cristóbal	Cristóbal	k1gInSc1	Cristóbal
<g/>
,	,	kIx,	,
Butarque	Butarque	k1gInSc1	Butarque
<g/>
,	,	kIx,	,
Los	los	k1gMnSc1	los
Rosales	Rosales	k1gMnSc1	Rosales
<g/>
,	,	kIx,	,
Los	los	k1gMnSc1	los
Ángeles	Ángeles	k1gMnSc1	Ángeles
<g/>
.	.	kIx.	.
</s>
<s>
Villa	Villa	k1gMnSc1	Villa
de	de	k?	de
Vallecas	Vallecas	k1gMnSc1	Vallecas
<g/>
:	:	kIx,	:
Casco	Casco	k1gMnSc1	Casco
Histórico	Histórico	k1gMnSc1	Histórico
de	de	k?	de
Vallecas	Vallecas	k1gMnSc1	Vallecas
<g/>
,	,	kIx,	,
Santa	Santa	k1gMnSc1	Santa
Eugenia	Eugenium	k1gNnSc2	Eugenium
<g/>
.	.	kIx.	.
</s>
<s>
Vicálvaro	Vicálvara	k1gFnSc5	Vicálvara
<g/>
:	:	kIx,	:
Casco	Casco	k1gMnSc1	Casco
Histórico	Histórico	k1gMnSc1	Histórico
de	de	k?	de
Vicálvaro	Vicálvara	k1gFnSc5	Vicálvara
<g/>
,	,	kIx,	,
Ambroz	Ambroz	k1gMnSc1	Ambroz
<g/>
.	.	kIx.	.
</s>
<s>
San	San	k?	San
Blas	Blas	k1gInSc1	Blas
<g/>
:	:	kIx,	:
Simancas	Simancas	k1gInSc1	Simancas
<g/>
,	,	kIx,	,
Hellín	Hellín	k1gInSc1	Hellín
<g/>
,	,	kIx,	,
Amposta	Amposta	k1gMnSc1	Amposta
<g/>
,	,	kIx,	,
Arcos	Arcos	k1gMnSc1	Arcos
<g/>
,	,	kIx,	,
Rosas	Rosas	k1gMnSc1	Rosas
<g/>
,	,	kIx,	,
Rejas	Rejas	k1gMnSc1	Rejas
<g/>
,	,	kIx,	,
Canillejas	Canillejas	k1gMnSc1	Canillejas
<g/>
,	,	kIx,	,
Salvador	Salvador	k1gMnSc1	Salvador
<g/>
.	.	kIx.	.
</s>
<s>
Barajas	Barajas	k1gMnSc1	Barajas
<g/>
:	:	kIx,	:
Alameda	Alameda	k1gMnSc1	Alameda
de	de	k?	de
Osuna	Osuna	k1gFnSc1	Osuna
<g/>
,	,	kIx,	,
Aeropuerto	Aeropuerta	k1gFnSc5	Aeropuerta
<g/>
,	,	kIx,	,
Casco	Casco	k1gMnSc1	Casco
Histórico	Histórico	k1gMnSc1	Histórico
de	de	k?	de
Barajas	Barajas	k1gMnSc1	Barajas
<g/>
,	,	kIx,	,
Timón	Timón	k1gMnSc1	Timón
<g/>
,	,	kIx,	,
Corralejos	Corralejos	k1gMnSc1	Corralejos
<g/>
.	.	kIx.	.
</s>
<s>
Madrid	Madrid	k1gInSc1	Madrid
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc4	město
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
počtem	počet	k1gInSc7	počet
stromů	strom	k1gInPc2	strom
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
druhé	druhý	k4xOgNnSc1	druhý
nejdelší	dlouhý	k2eAgNnSc1d3	nejdelší
stromořadí	stromořadí	k1gNnSc1	stromořadí
na	na	k7c6	na
světě	svět	k1gInSc6	svět
po	po	k7c6	po
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stromořadí	stromořadí	k1gNnSc6	stromořadí
je	být	k5eAaImIp3nS	být
248	[number]	k4	248
000	[number]	k4	000
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
se	se	k3xPyFc4	se
zeleň	zeleň	k1gFnSc1	zeleň
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
o	o	k7c4	o
16	[number]	k4	16
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
tvoří	tvořit	k5eAaImIp3nS	tvořit
zeleň	zeleň	k1gFnSc4	zeleň
8,2	[number]	k4	8,2
%	%	kIx~	%
Madridu	Madrid	k1gInSc2	Madrid
<g/>
.	.	kIx.	.
</s>
<s>
Madridské	madridský	k2eAgFnPc1d1	Madridská
ulice	ulice	k1gFnPc1	ulice
jsou	být	k5eAaImIp3nP	být
učiněným	učiněný	k2eAgNnSc7d1	učiněné
muzeem	muzeum	k1gNnSc7	muzeum
venkovního	venkovní	k2eAgNnSc2d1	venkovní
sochařství	sochařství	k1gNnSc2	sochařství
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
částí	část	k1gFnPc2	část
parku	park	k1gInSc2	park
Buen	Buen	k1gMnSc1	Buen
Retiro	Retiro	k1gNnSc4	Retiro
je	být	k5eAaImIp3nS	být
krásnou	krásný	k2eAgFnSc7d1	krásná
sochařskou	sochařský	k2eAgFnSc7d1	sochařská
scenérií	scenérie	k1gFnSc7	scenérie
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
díky	díky	k7c3	díky
památníku	památník	k1gInSc3	památník
Alfonse	Alfons	k1gMnSc2	Alfons
XII	XII	kA	XII
<g/>
.	.	kIx.	.
a	a	k8xC	a
soše	socha	k1gFnSc3	socha
Fuente	Fuent	k1gInSc5	Fuent
del	del	k?	del
Ángel	Ángel	k1gInSc1	Ángel
Caído	Caído	k1gNnSc1	Caído
Real	Real	k1gInSc1	Real
Madrid	Madrid	k1gInSc1	Madrid
Atlético	Atlético	k1gNnSc1	Atlético
Madrid	Madrid	k1gInSc1	Madrid
Getafe	Getaf	k1gInSc5	Getaf
CF	CF	kA	CF
Rayo	Raya	k1gFnSc5	Raya
Vallecano	Vallecana	k1gFnSc5	Vallecana
Real	Real	k1gInSc4	Real
Madrid	Madrid	k1gInSc1	Madrid
Castilla	Castilla	k1gFnSc1	Castilla
Real	Real	k1gInSc4	Real
Madrid	Madrid	k1gInSc1	Madrid
Baloncesto	Baloncesta	k1gMnSc5	Baloncesta
CB	CB	kA	CB
Estudiantes	Estudiantes	k1gMnSc1	Estudiantes
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc4	seznam
madridských	madridský	k2eAgInPc2d1	madridský
rodáků	rodák	k1gMnPc2	rodák
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Madrid	Madrid	k1gInSc1	Madrid
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gFnSc1	téma
Madrid	Madrid	k1gInSc1	Madrid
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Slovníkové	slovníkový	k2eAgFnSc2d1	slovníková
heslo	heslo	k1gNnSc4	heslo
Madrid	Madrid	k1gInSc1	Madrid
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
města	město	k1gNnSc2	město
Madrid	Madrid	k1gInSc1	Madrid
</s>
