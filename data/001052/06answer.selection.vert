<s>
Třeboň	Třeboň	k1gFnSc1	Třeboň
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Wittingau	Wittingaa	k1gFnSc4	Wittingaa
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
Wittingův	Wittingův	k2eAgInSc4d1	Wittingův
či	či	k8xC	či
Vítkův	Vítkův	k2eAgInSc4d1	Vítkův
Luh	luh	k1gInSc4	luh
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Trebonis	Trebonis	k1gFnSc1	Trebonis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
Hradec	Hradec	k1gInSc1	Hradec
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
22	[number]	k4	22
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
,	,	kIx,	,
v	v	k7c6	v
Třeboňské	třeboňský	k2eAgFnSc6d1	Třeboňská
pánvi	pánev	k1gFnSc6	pánev
na	na	k7c6	na
Zlaté	zlatý	k2eAgFnSc6d1	zlatá
stoce	stoka	k1gFnSc6	stoka
mezi	mezi	k7c7	mezi
rybníky	rybník	k1gInPc1	rybník
Svět	svět	k1gInSc1	svět
a	a	k8xC	a
Rožmberk	Rožmberk	k1gInSc1	Rožmberk
<g/>
.	.	kIx.	.
</s>
