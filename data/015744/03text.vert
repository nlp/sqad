<s>
Státní	státní	k2eAgFnSc1d1
bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Státní	státní	k2eAgFnSc1d1
bezpečnost	bezpečnost	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1945	#num#	k4
Zánik	zánik	k1gInSc1
</s>
<s>
1990	#num#	k4
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Bartolomějská	bartolomějský	k2eAgFnSc1d1
14	#num#	k4
<g/>
,	,	kIx,
Československo	Československo	k1gNnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Někdejší	někdejší	k2eAgNnSc1d1
sídlo	sídlo	k1gNnSc1
StB	StB	k1gFnSc2
v	v	k7c6
pražské	pražský	k2eAgFnSc6d1
Bartolomějské	bartolomějský	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
,	,	kIx,
tzv.	tzv.	kA
kachlíkárna	kachlíkárna	k1gFnSc1
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
StB	StB	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
STB	STB	kA
<g/>
.	.	kIx.
</s>
<s>
Znak	znak	k1gInSc1
<g/>
,	,	kIx,
jímž	jenž	k3xRgNnSc7
se	se	k3xPyFc4
mohli	moct	k5eAaImAgMnP
prokazovat	prokazovat	k5eAaImF
příslušníci	příslušník	k1gMnPc1
StB	StB	k1gFnSc4
<g/>
,	,	kIx,
měl	mít	k5eAaImAgInS
průměr	průměr	k1gInSc1
4	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s>
Státní	státní	k2eAgFnSc1d1
bezpečnost	bezpečnost	k1gFnSc1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
StB	StB	k1gFnSc1
<g/>
,	,	kIx,
hovorově	hovorově	k6eAd1
estébáci	estébáci	k?
<g/>
,	,	kIx,
slovensky	slovensky	k6eAd1
Štátna	Štátna	k1gFnSc1
bezpečnosť	bezpečnostit	k5eAaPmRp2nS
<g/>
,	,	kIx,
ŠtB	ŠtB	k1gMnSc5
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
československá	československý	k2eAgFnSc1d1
politická	politický	k2eAgFnSc1d1
policie	policie	k1gFnSc1
<g/>
,	,	kIx,
zpravodajská	zpravodajský	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
ihned	ihned	k6eAd1
od	od	k7c2
svého	svůj	k3xOyFgInSc2
vzniku	vznik	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
pod	pod	k7c7
kontrolou	kontrola	k1gFnSc7
KSČ	KSČ	kA
<g/>
,	,	kIx,
díky	díky	k7c3
obsazení	obsazení	k1gNnSc3
Ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
ministrem	ministr	k1gMnSc7
Václavem	Václav	k1gMnSc7
Noskem	Nosek	k1gMnSc7
<g/>
,	,	kIx,
a	a	k8xC
sloužila	sloužit	k5eAaImAgFnS
k	k	k7c3
ovládnutí	ovládnutí	k1gNnSc3
mocenských	mocenský	k2eAgFnPc2d1
pozic	pozice	k1gFnPc2
a	a	k8xC
likvidaci	likvidace	k1gFnSc4
protivníků	protivník	k1gMnPc2
komunistického	komunistický	k2eAgInSc2d1
režimu	režim	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikla	vzniknout	k5eAaPmAgFnS
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1945	#num#	k4
jako	jako	k9
jedna	jeden	k4xCgFnSc1
z	z	k7c2
neuniformovaných	uniformovaný	k2eNgFnPc2d1
složek	složka	k1gFnPc2
Sboru	sbor	k1gInSc2
národní	národní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
(	(	kIx(
<g/>
SNB	SNB	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
byla	být	k5eAaImAgFnS
jedním	jeden	k4xCgInSc7
z	z	k7c2
hlavních	hlavní	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
komunistického	komunistický	k2eAgInSc2d1
teroru	teror	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zanikla	zaniknout	k5eAaPmAgFnS
rozkazem	rozkaz	k1gInSc7
ministra	ministr	k1gMnSc2
vnitra	vnitro	k1gNnSc2
Richarda	Richard	k1gMnSc2
Sachera	Sacher	k1gMnSc2
15	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1990	#num#	k4
a	a	k8xC
některé	některý	k3yIgFnPc4
její	její	k3xOp3gFnPc4
úlohy	úloha	k1gFnPc4
převzal	převzít	k5eAaPmAgInS
nově	nově	k6eAd1
vzniklý	vzniklý	k2eAgInSc1d1
Úřad	úřad	k1gInSc1
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
ústavy	ústava	k1gFnSc2
a	a	k8xC
demokracie	demokracie	k1gFnSc2
FMV	FMV	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Struktura	struktura	k1gFnSc1
a	a	k8xC
normy	norma	k1gFnPc1
</s>
<s>
Práva	právo	k1gNnSc2
a	a	k8xC
povinnosti	povinnost	k1gFnSc2
StB	StB	k1gFnSc2
stanovil	stanovit	k5eAaPmAgInS
zákon	zákon	k1gInSc1
č.	č.	k?
149	#num#	k4
<g/>
/	/	kIx~
<g/>
1947	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
zároveň	zároveň	k6eAd1
předpokládal	předpokládat	k5eAaImAgInS
její	její	k3xOp3gNnSc4
budoucí	budoucí	k2eAgNnSc4d1
sloučení	sloučení	k1gNnSc4
s	s	k7c7
kriminálními	kriminální	k2eAgFnPc7d1
složkami	složka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
prosinci	prosinec	k1gInSc3
1947	#num#	k4
k	k	k7c3
ní	on	k3xPp3gFnSc6
byly	být	k5eAaImAgFnP
přičleněny	přičleněn	k2eAgFnPc1d1
zpravodajské	zpravodajský	k2eAgFnPc1d1
složky	složka	k1gFnPc1
a	a	k8xC
novelizovaný	novelizovaný	k2eAgInSc1d1
zákon	zákon	k1gInSc1
o	o	k7c6
národní	národní	k2eAgFnSc6d1
bezpečnosti	bezpečnost	k1gFnSc6
286	#num#	k4
<g/>
/	/	kIx~
<g/>
1948	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
ještě	ještě	k6eAd1
rozšířil	rozšířit	k5eAaPmAgInS
její	její	k3xOp3gFnPc4
pravomoci	pravomoc	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
údobí	údobí	k1gNnSc1
již	již	k6eAd1
bylo	být	k5eAaImAgNnS
jejím	její	k3xOp3gInSc7
úkolem	úkol	k1gInSc7
identifikovat	identifikovat	k5eAaBmF
<g/>
,	,	kIx,
vyšetřovat	vyšetřovat	k5eAaImF
a	a	k8xC
zpracovávat	zpracovávat	k5eAaImF
(	(	kIx(
<g/>
případně	případně	k6eAd1
i	i	k9
likvidovat	likvidovat	k5eAaBmF
<g/>
)	)	kIx)
oponenty	oponent	k1gMnPc7
KSČ	KSČ	kA
a	a	k8xC
komunistického	komunistický	k2eAgInSc2d1
režimu	režim	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Využívané	využívaný	k2eAgInPc1d1
objekty	objekt	k1gInPc1
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1
sídlem	sídlo	k1gNnSc7
StB	StB	k1gFnSc2
byla	být	k5eAaImAgFnS
budova	budova	k1gFnSc1
v	v	k7c6
pražské	pražský	k2eAgFnSc6d1
Bartolomějské	bartolomějský	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
(	(	kIx(
<g/>
slangově	slangově	k6eAd1
tzv.	tzv.	kA
kachlíkárna	kachlíkárna	k1gFnSc1
<g/>
;	;	kIx,
dnes	dnes	k6eAd1
objekt	objekt	k1gInSc4
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
Policie	policie	k1gFnSc1
ČR	ČR	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
snad	snad	k9
nejbrutálnější	brutální	k2eAgFnSc1d3
vyšetřovna	vyšetřovna	k1gFnSc1
nechvalně	chvalně	k6eNd1
proslul	proslout	k5eAaPmAgInS
tzv.	tzv.	kA
hradčanský	hradčanský	k2eAgInSc4d1
Domeček	domeček	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
StB	StB	k1gFnSc1
užívala	užívat	k5eAaImAgFnS
mnohé	mnohý	k2eAgInPc4d1
objekty	objekt	k1gInPc4
také	také	k9
konspiračně	konspiračně	k6eAd1
<g/>
,	,	kIx,
tak	tak	k9
např.	např.	kA
část	část	k1gFnSc1
břevnovského	břevnovský	k2eAgInSc2d1
kláštera	klášter	k1gInSc2
(	(	kIx(
<g/>
malý	malý	k2eAgInSc1d1
nebo	nebo	k8xC
také	také	k9
Sartoriův	Sartoriův	k2eAgInSc1d1
konvent	konvent	k1gInSc1
<g/>
)	)	kIx)
využívala	využívat	k5eAaPmAgFnS,k5eAaImAgFnS
pod	pod	k7c7
názvem	název	k1gInSc7
„	„	k?
<g/>
Montážní	montážní	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Činnost	činnost	k1gFnSc1
</s>
<s>
Maskovaný	maskovaný	k2eAgInSc1d1
fotoaparát	fotoaparát	k1gInSc1
určený	určený	k2eAgInSc1d1
pro	pro	k7c4
fotografování	fotografování	k1gNnSc4
v	v	k7c6
noci	noc	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přístroj	přístroj	k1gInSc1
sovětské	sovětský	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
</s>
<s>
Parník	parník	k1gInSc1
<g/>
,	,	kIx,
zařízení	zařízení	k1gNnSc1
užívané	užívaný	k2eAgFnSc2d1
StB	StB	k1gFnSc2
pro	pro	k7c4
rozlepování	rozlepování	k1gNnSc4
tajně	tajně	k6eAd1
cenzurovaných	cenzurovaný	k2eAgInPc2d1
dopisů	dopis	k1gInPc2
</s>
<s>
Činnost	činnost	k1gFnSc1
před	před	k7c7
rokem	rok	k1gInSc7
1948	#num#	k4
</s>
<s>
Známy	znám	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
takovéto	takovýto	k3xDgFnPc1
dehonestační	dehonestační	k2eAgFnPc1d1
akce	akce	k1gFnPc1
okolo	okolo	k7c2
Sergeje	Sergej	k1gMnSc2
Ingra	Ingr	k1gMnSc2
<g/>
,	,	kIx,
Vladimíra	Vladimír	k1gMnSc2
Krajiny	Krajina	k1gFnSc2
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Prokopa	Prokop	k1gMnSc2
Drtiny	drtina	k1gFnSc2
a	a	k8xC
Petra	Petr	k1gMnSc2
Zenkla	Zenkla	k1gMnSc2
(	(	kIx(
<g/>
neúspěšné	úspěšný	k2eNgFnPc1d1
<g/>
)	)	kIx)
a	a	k8xC
představitelů	představitel	k1gMnPc2
Demokratické	demokratický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
(	(	kIx(
<g/>
Ján	Ján	k1gMnSc1
Kempný	Kempný	k2eAgMnSc1d1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
Bugár	Bugár	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
uspěly	uspět	k5eAaPmAgFnP
a	a	k8xC
oba	dva	k4xCgMnPc1
poslanci	poslanec	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
nechali	nechat	k5eAaPmAgMnP
vydat	vydat	k5eAaPmF
trestnímu	trestní	k2eAgNnSc3d1
stíhání	stíhání	k1gNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
před	před	k7c7
soudem	soud	k1gInSc7
očistili	očistit	k5eAaPmAgMnP
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
na	na	k7c6
základě	základ	k1gInSc6
podvržených	podvržený	k2eAgInPc2d1
důkazů	důkaz	k1gInPc2
odsouzeni	odsouzet	k5eAaImNgMnP,k5eAaPmNgMnP
do	do	k7c2
vězení	vězení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Vyšetřovací	vyšetřovací	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
</s>
<s>
Již	již	k6eAd1
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
používala	používat	k5eAaImAgFnS
StB	StB	k1gFnSc1
nezákonné	zákonný	k2eNgFnSc2d1
metody	metoda	k1gFnSc2
jako	jako	k9
bylo	být	k5eAaImAgNnS
mučení	mučení	k1gNnSc1
<g/>
,	,	kIx,
vynucování	vynucování	k1gNnSc1
přiznání	přiznání	k1gNnSc1
a	a	k8xC
aplikování	aplikování	k1gNnSc1
drog	droga	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
únoru	únor	k1gInSc6
1948	#num#	k4
se	se	k3xPyFc4
tyto	tento	k3xDgFnPc1
praktiky	praktika	k1gFnPc1
staly	stát	k5eAaPmAgFnP
naprosto	naprosto	k6eAd1
běžnými	běžný	k2eAgFnPc7d1
a	a	k8xC
obecnými	obecný	k2eAgFnPc7d1
<g/>
,	,	kIx,
notně	notně	k6eAd1
podporovány	podporovat	k5eAaImNgInP
sovětskými	sovětský	k2eAgFnPc7d1
školiteli	školitel	k1gMnSc6
a	a	k8xC
pozorovateli	pozorovatel	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
hojně	hojně	k6eAd1
používány	používat	k5eAaImNgInP
zejména	zejména	k9
při	při	k7c6
přípravě	příprava	k1gFnSc6
vykonstruovaných	vykonstruovaný	k2eAgInPc2d1
procesů	proces	k1gInPc2
proti	proti	k7c3
politickým	politický	k2eAgMnPc3d1
a	a	k8xC
ideovým	ideový	k2eAgMnPc3d1
odpůrcům	odpůrce	k1gMnPc3
komunismu	komunismus	k1gInSc2
či	či	k8xC
nespolehlivým	spolehlivý	k2eNgMnPc3d1
občanům	občan	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Případy	případ	k1gInPc7
úmrtí	úmrtí	k1gNnSc2
nebo	nebo	k8xC
doživotního	doživotní	k2eAgNnSc2d1
zmrzačení	zmrzačení	k1gNnSc2
vyslýchaných	vyslýchaný	k2eAgInPc2d1
byly	být	k5eAaImAgInP
tolerovány	tolerován	k2eAgInPc4d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
dokonce	dokonce	k9
považovány	považován	k2eAgInPc1d1
přímo	přímo	k6eAd1
za	za	k7c4
žádoucí	žádoucí	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
výslechu	výslech	k1gInSc6
StB	StB	k1gMnSc4
zemřeli	zemřít	k5eAaPmAgMnP
například	například	k6eAd1
filozof	filozof	k1gMnSc1
Jan	Jan	k1gMnSc1
Patočka	Patočka	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Lukas	Lukas	k1gMnSc1
či	či	k8xC
kněz	kněz	k1gMnSc1
Josef	Josef	k1gMnSc1
Toufar	Toufar	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
StB	StB	k1gMnSc1
též	též	k9
s	s	k7c7
nejvyšší	vysoký	k2eAgFnSc7d3
pravděpodobností	pravděpodobnost	k1gFnSc7
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
prováděla	provádět	k5eAaImAgFnS
vraždy	vražda	k1gFnPc4
nepohodlných	pohodlný	k2eNgFnPc2d1
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
(	(	kIx(
<g/>
např.	např.	kA
Přemysl	Přemysl	k1gMnSc1
Coufal	Coufal	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
Švanda	Švanda	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Akce	akce	k1gFnSc1
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
</s>
<s>
Agenti	agent	k1gMnPc1
StB	StB	k1gFnPc2
v	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
podnikali	podnikat	k5eAaImAgMnP
ilegální	ilegální	k2eAgFnPc4d1
akce	akce	k1gFnPc4
i	i	k9
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
například	například	k6eAd1
únos	únos	k1gInSc1
posledního	poslední	k2eAgMnSc2d1
předúnorového	předúnorový	k2eAgMnSc2d1
předsedy	předseda	k1gMnSc2
ČSSD	ČSSD	kA
Bohumila	Bohumil	k1gMnSc2
Laušmana	Laušman	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
emigroval	emigrovat	k5eAaBmAgMnS
<g/>
,	,	kIx,
z	z	k7c2
Rakouska	Rakousko	k1gNnSc2
zpět	zpět	k6eAd1
do	do	k7c2
Československa	Československo	k1gNnSc2
(	(	kIx(
<g/>
1953	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hodnocení	hodnocení	k1gNnSc1
StB	StB	k1gFnSc2
</s>
<s>
Průkazka	průkazka	k1gFnSc1
člena	člen	k1gMnSc2
StB	StB	k1gMnSc2
(	(	kIx(
<g/>
vydaná	vydaný	k2eAgFnSc1d1
roku	rok	k1gInSc2
1945	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podle	podle	k7c2
řady	řada	k1gFnSc2
autorů	autor	k1gMnPc2
<g/>
,	,	kIx,
odborníků	odborník	k1gMnPc2
i	i	k8xC
vyšetřovatelů	vyšetřovatel	k1gMnPc2
zločinů	zločin	k1gInPc2
StB	StB	k1gMnSc1
(	(	kIx(
<g/>
např.	např.	kA
Pillerova	Pillerův	k2eAgFnSc1d1
komise	komise	k1gFnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
Kratochvil	kratochvíle	k1gFnPc2
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Kaplan	Kaplan	k1gMnSc1
<g/>
)	)	kIx)
byli	být	k5eAaImAgMnP
vyšetřovatelé	vyšetřovatel	k1gMnPc1
StB	StB	k1gFnSc2
(	(	kIx(
<g/>
zejména	zejména	k9
v	v	k7c6
období	období	k1gNnSc6
největšího	veliký	k2eAgInSc2d3
teroru	teror	k1gInSc2
<g/>
)	)	kIx)
v	v	k7c6
drtivé	drtivý	k2eAgFnSc6d1
většině	většina	k1gFnSc6
lidé	člověk	k1gMnPc1
bez	bez	k7c2
zkušeností	zkušenost	k1gFnPc2
a	a	k8xC
schopností	schopnost	k1gFnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
neuznávali	uznávat	k5eNaImAgMnP
a	a	k8xC
nebo	nebo	k8xC
neznali	neznat	k5eAaImAgMnP,k5eNaImAgMnP
zákony	zákon	k1gInPc4
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
a	a	k8xC
jedinou	jediný	k2eAgFnSc7d1
autoritou	autorita	k1gFnSc7
pro	pro	k7c4
ně	on	k3xPp3gNnSc4
byla	být	k5eAaImAgFnS
strana	strana	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gNnPc4
přání	přání	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedostatky	nedostatek	k1gInPc7
důkazů	důkaz	k1gInPc2
i	i	k8xC
vlastních	vlastní	k2eAgFnPc2d1
schopností	schopnost	k1gFnPc2
nahrazovali	nahrazovat	k5eAaImAgMnP
krutými	krutý	k2eAgInPc7d1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
metodami	metoda	k1gFnPc7
výslechů	výslech	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
autoři	autor	k1gMnPc1
srovnávají	srovnávat	k5eAaImIp3nP
StB	StB	k1gFnPc4
s	s	k7c7
gestapem	gestapo	k1gNnSc7
(	(	kIx(
<g/>
např.	např.	kA
Antonín	Antonín	k1gMnSc1
Kratochvíl	Kratochvíl	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
Škutina	Škutina	k1gFnSc1
<g/>
,	,	kIx,
Artur	Artur	k1gMnSc1
London	London	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
někteří	některý	k3yIgMnPc1
pak	pak	k6eAd1
podotýkají	podotýkat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
gestapu	gestapo	k1gNnSc6
šlo	jít	k5eAaImAgNnS
více	hodně	k6eAd2
o	o	k7c4
„	„	k?
<g/>
skutečné	skutečný	k2eAgMnPc4d1
viníky	viník	k1gMnPc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Podobně	podobně	k6eAd1
hovořil	hovořit	k5eAaImAgMnS
i	i	k9
bývalý	bývalý	k2eAgMnSc1d1
příslušník	příslušník	k1gMnSc1
StB	StB	k1gMnSc1
z	z	k7c2
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
Miloš	Miloš	k1gMnSc1
Kocman	Kocman	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
bývalého	bývalý	k2eAgMnSc2d1
disidenta	disident	k1gMnSc2
Petra	Petr	k1gMnSc2
Placáka	Placák	k1gMnSc2
se	s	k7c7
příslušníky	příslušník	k1gMnPc7
StB	StB	k1gFnSc2
nestávali	stávat	k5eNaImAgMnP
lidé	člověk	k1gMnPc1
z	z	k7c2
ideologických	ideologický	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
toužili	toužit	k5eAaImAgMnP
po	po	k7c6
moci	moc	k1gFnSc6
nebo	nebo	k8xC
kariéře	kariéra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
vstoupili	vstoupit	k5eAaPmAgMnP
do	do	k7c2
KSČ	KSČ	kA
<g/>
,	,	kIx,
udělali	udělat	k5eAaPmAgMnP
to	ten	k3xDgNnSc4
kvůli	kvůli	k7c3
kariérnímu	kariérní	k2eAgInSc3d1
postupu	postup	k1gInSc3
či	či	k8xC
získání	získání	k1gNnSc3
dalších	další	k2eAgFnPc2d1
výhod	výhoda	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bývalí	bývalý	k2eAgMnPc1d1
příslušníci	příslušník	k1gMnPc1
StB	StB	k1gFnSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
sloužili	sloužit	k5eAaImAgMnP
u	u	k7c2
policie	policie	k1gFnSc2
i	i	k9
po	po	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
ke	k	k7c3
svým	svůj	k3xOyFgInPc3
normálním	normální	k2eAgInPc3d1
důchodům	důchod	k1gInPc3
pobírají	pobírat	k5eAaImIp3nP
i	i	k9
pravidelné	pravidelný	k2eAgFnPc1d1
měsíční	měsíční	k2eAgFnPc1d1
renty	renta	k1gFnPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
výše	výše	k1gFnSc1
začíná	začínat	k5eAaImIp3nS
na	na	k7c6
deseti	deset	k4xCc6
tisících	tisíc	k4xCgInPc6,k4xOgInPc6
korunách	koruna	k1gFnPc6
<g/>
,	,	kIx,
ovšem	ovšem	k9
výjimkou	výjimka	k1gFnSc7
nejsou	být	k5eNaImIp3nP
ani	ani	k8xC
mnohem	mnohem	k6eAd1
vyšší	vysoký	k2eAgFnPc4d2
částky	částka	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Seznamy	seznam	k1gInPc1
příslušníků	příslušník	k1gMnPc2
a	a	k8xC
spolupracovníků	spolupracovník	k1gMnPc2
StB	StB	k1gFnSc2
</s>
<s>
Svazky	svazek	k1gInPc1
StB	StB	k1gFnSc2
očima	oko	k1gNnPc7
Sylvy	Sylva	k1gFnSc2
Chňapkové	Chňapková	k1gFnSc2
</s>
<s>
Po	po	k7c6
listopadu	listopad	k1gInSc6
1989	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
veřejnosti	veřejnost	k1gFnSc6
objevily	objevit	k5eAaPmAgFnP
první	první	k4xOgFnPc1
<g/>
,	,	kIx,
neúplné	úplný	k2eNgFnPc1d1
<g/>
,	,	kIx,
seznamy	seznam	k1gInPc1
příslušníků	příslušník	k1gMnPc2
a	a	k8xC
spolupracovníků	spolupracovník	k1gMnPc2
StB	StB	k1gFnSc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
„	„	k?
<g/>
Cibulkovy	Cibulkův	k2eAgInPc4d1
seznamy	seznam	k1gInPc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reakce	reakce	k1gFnSc1
některých	některý	k3yIgMnPc2
z	z	k7c2
těch	ten	k3xDgMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
byli	být	k5eAaImAgMnP
v	v	k7c6
seznamech	seznam	k1gInPc6
uvedeni	uvést	k5eAaPmNgMnP
<g/>
,	,	kIx,
vyšly	vyjít	k5eAaPmAgInP
v	v	k7c6
knize	kniha	k1gFnSc6
Osočení	osočení	k1gNnSc1
(	(	kIx(
<g/>
Toronto	Toronto	k1gNnSc1
1993	#num#	k4
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
někteří	některý	k3yIgMnPc1
tvrdí	tvrdit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
do	do	k7c2
Cibulkových	Cibulkových	k2eAgInPc2d1
seznamů	seznam	k1gInPc2
dostali	dostat	k5eAaPmAgMnP
neprávem	neprávo	k1gNnSc7
<g/>
,	,	kIx,
jiní	jiný	k2eAgMnPc1d1
vysvětlují	vysvětlovat	k5eAaImIp3nP
okolnosti	okolnost	k1gFnPc1
vzniku	vznik	k1gInSc2
spolupráce	spolupráce	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
seznamech	seznam	k1gInPc6
byl	být	k5eAaImAgMnS
například	například	k6eAd1
uveden	uveden	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
a	a	k8xC
režisér	režisér	k1gMnSc1
Ivan	Ivan	k1gMnSc1
Vyskočil	Vyskočil	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
odmítl	odmítnout	k5eAaPmAgMnS
podepsat	podepsat	k5eAaPmF
Antichartu	anticharta	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
proti	proti	k7c3
uvedení	uvedení	k1gNnSc3
v	v	k7c6
seznamech	seznam	k1gInPc6
jako	jako	k8xC,k8xS
spolupracovník	spolupracovník	k1gMnSc1
StB	StB	k1gMnSc1
se	se	k3xPyFc4
úspěšně	úspěšně	k6eAd1
bránil	bránit	k5eAaImAgMnS
soudní	soudní	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
O	o	k7c6
více	hodně	k6eAd2
než	než	k8xS
desetiletí	desetiletí	k1gNnPc4
později	pozdě	k6eAd2
<g/>
,	,	kIx,
v	v	k7c6
březnu	březen	k1gInSc6
2003	#num#	k4
<g/>
,	,	kIx,
zveřejnilo	zveřejnit	k5eAaPmAgNnS
Ministerstvo	ministerstvo	k1gNnSc1
vnitra	vnitro	k1gNnSc2
další	další	k2eAgMnPc1d1
<g/>
,	,	kIx,
také	také	k9
neúplné	úplný	k2eNgInPc4d1
seznamy	seznam	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc4
jsou	být	k5eAaImIp3nP
(	(	kIx(
<g/>
zejména	zejména	k9
pokud	pokud	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
spolupracovníky	spolupracovník	k1gMnPc4
<g/>
)	)	kIx)
předmětem	předmět	k1gInSc7
soudních	soudní	k2eAgInPc2d1
sporů	spor	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
mají	mít	k5eAaImIp3nP
dosáhnout	dosáhnout	k5eAaPmF
vymazání	vymazání	k1gNnSc4
záznamů	záznam	k1gInPc2
těch	ten	k3xDgMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
cítí	cítit	k5eAaImIp3nP
být	být	k5eAaImF
zaneseni	zanést	k5eAaPmNgMnP
neoprávněně	oprávněně	k6eNd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc1d1
problém	problém	k1gInSc1
těchto	tento	k3xDgInPc2
seznamů	seznam	k1gInPc2
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
až	až	k9
na	na	k7c4
nepatrné	nepatrný	k2eAgFnPc4d1,k2eNgFnPc4d1
výjimky	výjimka	k1gFnPc4
se	se	k3xPyFc4
zakládají	zakládat	k5eAaImIp3nP
pouze	pouze	k6eAd1
na	na	k7c6
archivních	archivní	k2eAgInPc6d1
svazcích	svazek	k1gInPc6
-	-	kIx~
tj.	tj.	kA
obsahují	obsahovat	k5eAaImIp3nP
pouze	pouze	k6eAd1
jména	jméno	k1gNnSc2
příslušníků	příslušník	k1gMnPc2
a	a	k8xC
spolupracovníků	spolupracovník	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
ukončili	ukončit	k5eAaPmAgMnP
spolupráci	spolupráce	k1gFnSc4
nebo	nebo	k8xC
pracovní	pracovní	k2eAgInSc4d1
poměr	poměr	k1gInSc4
před	před	k7c7
rokem	rok	k1gInSc7
1989	#num#	k4
a	a	k8xC
jejichž	jejichž	k3xOyRp3gInPc1
svazky	svazek	k1gInPc1
byly	být	k5eAaImAgInP
tudíž	tudíž	k8xC
v	v	k7c6
době	doba	k1gFnSc6
mocenských	mocenský	k2eAgFnPc2d1
změn	změna	k1gFnPc2
uloženy	uložen	k2eAgFnPc4d1
v	v	k7c6
archivu	archiv	k1gInSc6
a	a	k8xC
nemohly	moct	k5eNaImAgFnP
být	být	k5eAaImF
proto	proto	k8xC
zavčas	zavčas	k6eAd1
zničeny	zničen	k2eAgInPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
únoru	únor	k1gInSc6
2007	#num#	k4
informoval	informovat	k5eAaBmAgMnS
ministr	ministr	k1gMnSc1
vnitra	vnitro	k1gNnSc2
Ivan	Ivan	k1gMnSc1
Langer	Langer	k1gMnSc1
o	o	k7c6
projektu	projekt	k1gInSc6
Otevřená	otevřený	k2eAgFnSc1d1
minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
„	„	k?
<g/>
co	co	k9
nejúplnější	úplný	k2eAgNnSc4d3
otevření	otevření	k1gNnSc4
a	a	k8xC
přiblížení	přiblížení	k1gNnSc4
archivních	archivní	k2eAgInPc2d1
fondů	fond	k1gInPc2
všem	všecek	k3xTgMnPc3
občanům	občan	k1gMnPc3
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
<g/>
Realizuji	realizovat	k5eAaBmIp1nS
projekt	projekt	k1gInSc1
Otevřená	otevřený	k2eAgFnSc1d1
minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
jehož	jehož	k3xOyRp3gInSc6
rámci	rámec	k1gInSc6
chci	chtít	k5eAaImIp1nS
maximálně	maximálně	k6eAd1
zpřístupnit	zpřístupnit	k5eAaPmF
archivy	archiv	k1gInPc1
bývalé	bývalý	k2eAgFnSc2d1
StB	StB	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zřídil	zřídit	k5eAaPmAgMnS
jsem	být	k5eAaImIp1nS
Archiv	archiv	k1gInSc4
bezpečnostních	bezpečnostní	k2eAgFnPc2d1
složek	složka	k1gFnPc2
a	a	k8xC
noví	nový	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
tam	tam	k6eAd1
přišli	přijít	k5eAaPmAgMnP
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
mě	já	k3xPp1nSc4
nadějí	nadát	k5eAaBmIp3nP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
podaří	podařit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgInSc7
krokem	krok	k1gInSc7
bylo	být	k5eAaImAgNnS
propuštění	propuštění	k1gNnSc1
8	#num#	k4
bývalých	bývalý	k2eAgMnPc2d1
pracovníků	pracovník	k1gMnPc2
StB	StB	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
tam	tam	k6eAd1
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
pracovali	pracovat	k5eAaImAgMnP
<g/>
.	.	kIx.
<g/>
“	“	k?
(	(	kIx(
<g/>
Ivan	Ivan	k1gMnSc1
Langer	Langer	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
<g/>
K	k	k7c3
úplnému	úplný	k2eAgNnSc3d1
odtajnění	odtajnění	k1gNnSc3
by	by	kYmCp3nP
mohlo	moct	k5eAaImAgNnS
dojít	dojít	k5eAaPmF
do	do	k7c2
deseti	deset	k4xCc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
“	“	k?
(	(	kIx(
<g/>
Pavel	Pavel	k1gMnSc1
Žáček	Žáček	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Struktura	struktura	k1gFnSc1
StB	StB	k1gFnSc2
</s>
<s>
Svědectví	svědectví	k1gNnPc1
o	o	k7c6
přístupu	přístup	k1gInSc6
k	k	k7c3
podezřelému	podezřelý	k2eAgInSc3d1
ve	v	k7c6
vyšetřovací	vyšetřovací	k2eAgFnSc6d1
vazbě	vazba	k1gFnSc6
<g/>
:	:	kIx,
"	"	kIx"
<g/>
...	...	k?
manželka	manželka	k1gFnSc1
se	se	k3xPyFc4
má	mít	k5eAaImIp3nS
připravit	připravit	k5eAaPmF
na	na	k7c6
vystěhování	vystěhování	k1gNnSc6
<g/>
,	,	kIx,
neboť	neboť	k8xC
Miroslav	Miroslav	k1gMnSc1
Sýkora	Sýkora	k1gMnSc1
„	„	k?
<g/>
dostane	dostat	k5eAaPmIp3nS
provaz	provaz	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Struktura	struktura	k1gFnSc1
Státní	státní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
průběhu	průběh	k1gInSc6
její	její	k3xOp3gFnSc2
existence	existence	k1gFnSc2
mnohokrát	mnohokrát	k6eAd1
měnila	měnit	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
reorganizaci	reorganizace	k1gFnSc6
ze	z	k7c2
dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1974	#num#	k4
měla	mít	k5eAaImAgFnS
následující	následující	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I.	I.	kA
správa	správa	k1gFnSc1
FMV	FMV	kA
(	(	kIx(
<g/>
SNB	SNB	kA
<g/>
)	)	kIx)
–	–	k?
Hlavní	hlavní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
rozvědky	rozvědka	k1gFnSc2
</s>
<s>
II	II	kA
<g/>
.	.	kIx.
správa	správa	k1gFnSc1
FMV	FMV	kA
(	(	kIx(
<g/>
SNB	SNB	kA
<g/>
)	)	kIx)
–	–	k?
Hlavní	hlavní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
kontrarozvědky	kontrarozvědka	k1gFnSc2
pro	pro	k7c4
boj	boj	k1gInSc4
proti	proti	k7c3
vnějšímu	vnější	k2eAgMnSc3d1
nepříteli	nepřítel	k1gMnSc3
(	(	kIx(
<g/>
ochrana	ochrana	k1gFnSc1
proti	proti	k7c3
zahraničním	zahraniční	k2eAgMnPc3d1
špionům	špion	k1gMnPc3
<g/>
)	)	kIx)
</s>
<s>
III	III	kA
<g/>
.	.	kIx.
správa	správa	k1gFnSc1
FMV	FMV	kA
(	(	kIx(
<g/>
SNB	SNB	kA
<g/>
)	)	kIx)
–	–	k?
Hlavní	hlavní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
vojenské	vojenský	k2eAgFnSc2d1
kontrarozvědky	kontrarozvědka	k1gFnSc2
</s>
<s>
IV	IV	kA
<g/>
.	.	kIx.
správa	správa	k1gFnSc1
FMV	FMV	kA
(	(	kIx(
<g/>
SNB	SNB	kA
<g/>
)	)	kIx)
–	–	k?
Správa	správa	k1gFnSc1
sledování	sledování	k1gNnSc2
</s>
<s>
V.	V.	kA
správa	správa	k1gFnSc1
FMV	FMV	kA
(	(	kIx(
<g/>
SNB	SNB	kA
<g/>
)	)	kIx)
–	–	k?
Správa	správa	k1gFnSc1
ochrany	ochrana	k1gFnSc2
stranických	stranický	k2eAgInPc2d1
a	a	k8xC
ústavních	ústavní	k2eAgInPc2d1
činitelů	činitel	k1gInPc2
</s>
<s>
VI	VI	kA
<g/>
.	.	kIx.
správa	správa	k1gFnSc1
FMV	FMV	kA
(	(	kIx(
<g/>
SNB	SNB	kA
<g/>
)	)	kIx)
–	–	k?
Správa	správa	k1gFnSc1
(	(	kIx(
<g/>
výkonné	výkonný	k2eAgFnSc2d1
<g/>
)	)	kIx)
zpravodajské	zpravodajský	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
</s>
<s>
X.	X.	kA
správa	správa	k1gFnSc1
FMV	FMV	kA
(	(	kIx(
<g/>
SNB	SNB	kA
<g/>
)	)	kIx)
–	–	k?
Správa	správa	k1gFnSc1
kontrarozvědky	kontrarozvědka	k1gFnSc2
pro	pro	k7c4
boj	boj	k1gInSc4
proti	proti	k7c3
vnitřnímu	vnitřní	k2eAgMnSc3d1
nepříteli	nepřítel	k1gMnSc3
(	(	kIx(
<g/>
disidenti	disident	k1gMnPc1
<g/>
,	,	kIx,
chartisté	chartista	k1gMnPc1
<g/>
,	,	kIx,
kněží	kněz	k1gMnPc1
<g/>
,	,	kIx,
mládež	mládež	k1gFnSc4
apod.	apod.	kA
<g/>
)	)	kIx)
</s>
<s>
XI	XI	kA
<g/>
.	.	kIx.
správa	správa	k1gFnSc1
FMV	FMV	kA
(	(	kIx(
<g/>
SNB	SNB	kA
<g/>
)	)	kIx)
–	–	k?
Správa	správa	k1gFnSc1
kontrarozvědky	kontrarozvědka	k1gFnSc2
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
ekonomiky	ekonomika	k1gFnSc2
</s>
<s>
XII	XII	kA
<g/>
.	.	kIx.
správa	správa	k1gFnSc1
FMV	FMV	kA
(	(	kIx(
<g/>
SNB	SNB	kA
<g/>
)	)	kIx)
–	–	k?
Správa	správa	k1gFnSc1
kontrarozvědky	kontrarozvědka	k1gFnSc2
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
</s>
<s>
XIII	XIII	kA
<g/>
.	.	kIx.
správa	správa	k1gFnSc1
FMV	FMV	kA
(	(	kIx(
<g/>
SNB	SNB	kA
<g/>
)	)	kIx)
–	–	k?
Oborové	oborový	k2eAgFnPc4d1
výpočetní	výpočetní	k2eAgFnPc4d1
a	a	k8xC
informační	informační	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
Státní	státní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
</s>
<s>
XIV	XIV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
FMV	FMV	kA
(	(	kIx(
<g/>
SNB	SNB	kA
<g/>
)	)	kIx)
–	–	k?
Správa	správa	k1gFnSc1
kontrarozvědky	kontrarozvědka	k1gFnSc2
pro	pro	k7c4
boj	boj	k1gInSc4
proti	proti	k7c3
mimořádným	mimořádný	k2eAgFnPc3d1
a	a	k8xC
zvláštním	zvláštní	k2eAgFnPc3d1
formám	forma	k1gFnPc3
trestné	trestný	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
</s>
<s>
Správa	správa	k1gFnSc1
vyšetřování	vyšetřování	k1gNnSc2
Státní	státní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
</s>
<s>
Odbor	odbor	k1gInSc1
vyšetřování	vyšetřování	k1gNnSc2
Státní	státní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
ve	v	k7c6
vojenské	vojenský	k2eAgFnSc6d1
kontrarozvědce	kontrarozvědka	k1gFnSc6
</s>
<s>
Správa	správa	k1gFnSc1
pasů	pas	k1gInPc2
a	a	k8xC
víz	vízo	k1gNnPc2
</s>
<s>
Statisticko	Statisticko	k6eAd1
evidenční	evidenční	k2eAgInSc4d1
odbor	odbor	k1gInSc4
</s>
<s>
IV	IV	kA
<g/>
.	.	kIx.
odbor	odbor	k1gInSc1
FMV	FMV	kA
(	(	kIx(
<g/>
SNB	SNB	kA
<g/>
)	)	kIx)
–	–	k?
Odbor	odbor	k1gInSc4
zahraničního	zahraniční	k2eAgInSc2d1
tisku	tisk	k1gInSc2
</s>
<s>
Odbor	odbor	k1gInSc1
vyšetřování	vyšetřování	k1gNnSc2
Státní	státní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
ve	v	k7c6
vojenské	vojenský	k2eAgFnSc6d1
kontrarozvědce	kontrarozvědka	k1gFnSc6
byl	být	k5eAaImAgInS
zrušen	zrušit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
<g/>
,	,	kIx,
XIII	XIII	kA
<g/>
.	.	kIx.
správa	správa	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Statisticko	Statisticko	k1gNnSc4
evidenční	evidenční	k2eAgInSc1d1
odbor	odbor	k1gInSc1
byl	být	k5eAaImAgInS
od	od	k7c2
roku	rok	k1gInSc2
1981	#num#	k4
přiřazen	přiřazen	k2eAgInSc4d1
pod	pod	k7c4
XIII	XIII	kA
<g/>
.	.	kIx.
správu	správa	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
V	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
byla	být	k5eAaImAgFnS
zrušena	zrušit	k5eAaPmNgFnS
XIV	XIV	kA
<g/>
.	.	kIx.
správa	správa	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gInPc1
úkoly	úkol	k1gInPc1
byly	být	k5eAaImAgInP
rozděleny	rozdělit	k5eAaPmNgInP
mezi	mezi	k7c4
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
XI	XI	kA
<g/>
.	.	kIx.
správu	správa	k1gFnSc4
a	a	k8xC
Odbor	odbor	k1gInSc4
zvláštního	zvláštní	k2eAgNnSc2d1
určení	určení	k1gNnSc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
"	"	kIx"
<g/>
červené	červený	k2eAgInPc4d1
barety	baret	k1gInPc4
<g/>
"	"	kIx"
proslulé	proslulý	k2eAgNnSc4d1
brutalitou	brutalita	k1gFnSc7
během	během	k7c2
demonstrace	demonstrace	k1gFnSc2
17	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1989	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
poslední	poslední	k2eAgFnSc3d1
reorganizaci	reorganizace	k1gFnSc3
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
srpnu	srpen	k1gInSc3
1988	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
X.	X.	kA
a	a	k8xC
XI	XI	kA
<g/>
.	.	kIx.
správa	správa	k1gFnSc1
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
do	do	k7c2
jednoho	jeden	k4xCgInSc2
útvaru	útvar	k1gInSc2
s	s	k7c7
názvem	název	k1gInSc7
II	II	kA
<g/>
.	.	kIx.
správa	správa	k1gFnSc1
SNB	SNB	kA
–	–	k?
Hlavní	hlavní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
kontrarozvědky	kontrarozvědka	k1gFnSc2
SNB	SNB	kA
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Karla	Karel	k1gMnSc2
Vykypěla	vykypět	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Terminologie	terminologie	k1gFnSc1
StB	StB	k1gFnSc2
</s>
<s>
Terminologie	terminologie	k1gFnSc1
spolupracovníků	spolupracovník	k1gMnPc2
(	(	kIx(
<g/>
výběr	výběr	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
zřejmě	zřejmě	k6eAd1
obsahuje	obsahovat	k5eAaImIp3nS
výsledky	výsledek	k1gInPc4
vlastního	vlastní	k2eAgInSc2d1
výzkumu	výzkum	k1gInSc2
nebo	nebo	k8xC
neověřená	ověřený	k2eNgNnPc1d1
tvrzení	tvrzení	k1gNnPc1
<g/>
.	.	kIx.
<g/>
Můžete	moct	k5eAaImIp2nP
pomoci	pomoct	k5eAaPmF
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
přidáte	přidat	k5eAaPmIp2nP
reference	reference	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podívejte	podívat	k5eAaPmRp2nP,k5eAaImRp2nP
se	se	k3xPyFc4
na	na	k7c4
diskusní	diskusní	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
uvedeny	uveden	k2eAgInPc1d1
další	další	k2eAgInPc1d1
detaily	detail	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
vkladatele	vkladatel	k1gMnPc4
šablony	šablona	k1gFnSc2
<g/>
:	:	kIx,
Na	na	k7c6
diskusní	diskusní	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
zdůvodněte	zdůvodnit	k5eAaPmRp2nP
vložení	vložení	k1gNnSc4
šablony	šablona	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Zdroje	zdroj	k1gInPc1
<g/>
:	:	kIx,
Zkratky	zkratka	k1gFnPc1
v	v	k7c6
protokolech	protokol	k1gInPc6
VKR	VKR	kA
na	na	k7c6
stránkách	stránka	k1gFnPc6
ÚSTR	ÚSTR	kA
<g/>
,	,	kIx,
Zkratky	zkratka	k1gFnSc2
a	a	k8xC
terminologie	terminologie	k1gFnSc2
na	na	k7c4
svazky	svazek	k1gInPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
Vysvětlivky	vysvětlivka	k1gFnPc1
<g/>
,	,	kIx,
vybrané	vybraný	k2eAgFnPc1d1
zkratky	zkratka	k1gFnPc1
a	a	k8xC
časté	častý	k2eAgInPc1d1
dotazy	dotaz	k1gInPc1
na	na	k7c4
svazky	svazek	k1gInPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
Žáček	Žáček	k1gMnSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Ostrá	ostrý	k2eAgFnSc1d1
zbraň	zbraň	k1gFnSc1
<g/>
“	“	k?
Státní	státní	k2eAgFnSc3d1
bezpečnosti	bezpečnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Kategorie	kategorie	k1gFnSc1
důvěrník	důvěrník	k1gMnSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kandidát	kandidát	k1gMnSc1
tajné	tajný	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
(	(	kIx(
<g/>
KTS	KTS	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kandidát	kandidát	k1gMnSc1
agenta	agent	k1gMnSc2
(	(	kIx(
<g/>
KA	KA	kA
<g/>
)	)	kIx)
či	či	k8xC
kandidát	kandidát	k1gMnSc1
informátora	informátor	k1gMnSc2
(	(	kIx(
<g/>
KI	KI	kA
<g/>
)	)	kIx)
nejsou	být	k5eNaImIp3nP
na	na	k7c6
základě	základ	k1gInSc6
nálezu	nález	k1gInSc2
Ústavního	ústavní	k2eAgInSc2d1
soudu	soud	k1gInSc2
kategoriemi	kategorie	k1gFnPc7
vědomé	vědomý	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
ve	v	k7c6
smyslu	smysl	k1gInSc6
zákona	zákon	k1gInSc2
č.	č.	k?
451	#num#	k4
<g/>
/	/	kIx~
<g/>
1991	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
tzv.	tzv.	kA
lustrační	lustrační	k2eAgInSc1d1
zákon	zákon	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tajný	tajný	k1gMnSc1
spolupracovník	spolupracovník	k1gMnSc1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
TS	ts	k0
<g/>
)	)	kIx)
<g/>
:	:	kIx,
označení	označení	k1gNnSc1
pro	pro	k7c4
čsl	čsl	kA
<g/>
.	.	kIx.
i	i	k8xC
cizí	cizí	k2eAgMnPc4d1
státní	státní	k2eAgMnPc4d1
příslušníky	příslušník	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
byli	být	k5eAaImAgMnP
získáni	získat	k5eAaPmNgMnP
k	k	k7c3
vědomé	vědomý	k2eAgFnSc3d1
spolupráci	spolupráce	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvody	důvod	k1gInPc4
<g/>
:	:	kIx,
Hmotné	hmotný	k2eAgFnPc4d1
výhody	výhoda	k1gFnPc4
(	(	kIx(
<g/>
kupř	kupř	kA
<g/>
:	:	kIx,
poukázky	poukázka	k1gFnSc2
do	do	k7c2
obchodních	obchodní	k2eAgInPc2d1
domů	dům	k1gInPc2
<g/>
,	,	kIx,
finance	finance	k1gFnPc1
na	na	k7c4
výdaje	výdaj	k1gInPc4
(	(	kIx(
<g/>
oblečení	oblečení	k1gNnSc1
<g/>
,	,	kIx,
západní	západní	k2eAgInSc1d1
alkohol	alkohol	k1gInSc1
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
lepší	dobrý	k2eAgNnSc1d2
pracovní	pracovní	k2eAgNnSc1d1
zařazení	zařazení	k1gNnSc1
<g/>
,	,	kIx,
přístup	přístup	k1gInSc1
k	k	k7c3
zahraničním	zahraniční	k2eAgFnPc3d1
cestám	cesta	k1gFnPc3
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ideové	ideový	k2eAgInPc1d1
(	(	kIx(
<g/>
boj	boj	k1gInSc1
za	za	k7c4
<g />
.	.	kIx.
</s>
<s hack="1">
socialismus	socialismus	k1gInSc1
<g/>
,	,	kIx,
mírové	mírový	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
atd.	atd.	kA
důvody	důvod	k1gInPc1
spolupráce	spolupráce	k1gFnSc2
většinou	většinou	k6eAd1
u	u	k7c2
cizinců	cizinec	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
psychický	psychický	k2eAgInSc4d1
nátlak-kompromitující	nátlak-kompromitující	k2eAgInSc4d1
materiály	materiál	k1gInPc4
<g/>
,	,	kIx,
demoralizace	demoralizace	k1gFnPc4
(	(	kIx(
<g/>
dehonestace	dehonestace	k1gFnPc4
<g/>
)	)	kIx)
jedince	jedinec	k1gMnSc2
<g/>
,	,	kIx,
vydírání	vydírání	k1gNnSc1
atd.	atd.	kA
StB	StB	k1gFnSc2
používala	používat	k5eAaImAgFnS
tajné	tajný	k2eAgFnPc4d1
spolupracovníky	spolupracovník	k1gMnPc4
u	u	k7c2
všech	všecek	k3xTgInPc2
svých	svůj	k3xOyFgInPc2
útvarů	útvar	k1gInPc2
<g/>
:	:	kIx,
rozvědka	rozvědka	k1gFnSc1
<g/>
,	,	kIx,
kontrarozvědka	kontrarozvědka	k1gFnSc1
<g/>
,	,	kIx,
ochranka	ochranka	k1gFnSc1
<g/>
,	,	kIx,
útvary	útvar	k1gInPc1
sledování	sledování	k1gNnSc2
a	a	k8xC
zpravodajské	zpravodajský	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
rozvědky	rozvědka	k1gFnSc2
existovaly	existovat	k5eAaImAgFnP
kategorie	kategorie	k1gFnPc1
TS	ts	k0
jako	jako	k8xC,k8xS
agent	agent	k1gMnSc1
(	(	kIx(
<g/>
A	A	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
důvěrný	důvěrný	k2eAgInSc4d1
styk	styk	k1gInSc4
(	(	kIx(
<g/>
DS	DS	kA
<g/>
)	)	kIx)
a	a	k8xC
ideospolupracovník	ideospolupracovník	k1gMnSc1
(	(	kIx(
<g/>
IS	IS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolupráce	spolupráce	k1gFnSc1
končila	končit	k5eAaImAgFnS
nejčastěji	často	k6eAd3
ze	z	k7c2
zdravotních	zdravotní	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
či	či	k8xC
smrtí	smrt	k1gFnSc7
spolupracovníka	spolupracovník	k1gMnSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnPc2
emigrací	emigrace	k1gFnPc2
<g/>
,	,	kIx,
sesazením	sesazení	k1gNnSc7
na	na	k7c6
nižší	nízký	k2eAgFnSc6d2
pozici	pozice	k1gFnSc6
atd.	atd.	kA
většinou	většinou	k6eAd1
podle	podle	k7c2
kvality	kvalita	k1gFnSc2
jeho	jeho	k3xOp3gFnSc2
spolupráce	spolupráce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Informátor	informátor	k1gMnSc1
<g/>
:	:	kIx,
spolupracovník	spolupracovník	k1gMnSc1
StB	StB	k1gMnSc2
menšího	malý	k2eAgInSc2d2
významu	význam	k1gInSc2
<g/>
,	,	kIx,
dodával	dodávat	k5eAaImAgMnS
informace	informace	k1gFnPc4
nepravidelně	pravidelně	k6eNd1
a	a	k8xC
spolupráce	spolupráce	k1gFnSc1
byla	být	k5eAaImAgFnS
často	často	k6eAd1
nevědomá	vědomý	k2eNgFnSc1d1
(	(	kIx(
<g/>
resp.	resp.	kA
nebyl	být	k5eNaImAgInS
si	se	k3xPyFc3
vědom	vědom	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
informace	informace	k1gFnSc1
předává	předávat	k5eAaImIp3nS
pracovníkovi	pracovník	k1gMnSc3
StB	StB	k1gMnSc3
<g/>
,	,	kIx,
takový	takový	k3xDgMnSc1
informátor	informátor	k1gMnSc1
poté	poté	k6eAd1
ujednal	ujednat	k5eAaPmAgMnS
tedy	tedy	k9
tzv.	tzv.	kA
„	„	k?
<g/>
kontakt	kontakt	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
Informátor	informátor	k1gMnSc1
jednal	jednat	k5eAaImAgMnS
z	z	k7c2
různých	různý	k2eAgFnPc2d1
pohnutek	pohnutka	k1gFnPc2
a	a	k8xC
byl	být	k5eAaImAgInS
u	u	k7c2
StB	StB	k1gFnPc2
veden	vést	k5eAaImNgInS
pod	pod	k7c7
speciálním	speciální	k2eAgNnSc7d1
krycím	krycí	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Informátor	informátor	k1gMnSc1
byl	být	k5eAaImAgMnS
označován	označovat	k5eAaImNgMnS
také	také	k9
jako	jako	k9
"	"	kIx"
<g/>
zdroj	zdroj	k1gInSc1
<g/>
"	"	kIx"
či	či	k8xC
"	"	kIx"
<g/>
pramen	pramen	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Informátoři	informátor	k1gMnPc1
byli	být	k5eAaImAgMnP
často	často	k6eAd1
aktivní	aktivní	k2eAgInSc4d1
ve	v	k7c6
vojenském	vojenský	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
a	a	k8xC
v	v	k7c6
různých	různý	k2eAgFnPc6d1
komunistických	komunistický	k2eAgFnPc6d1
soustavách	soustava	k1gFnPc6
(	(	kIx(
<g/>
SSM	SSM	kA
<g/>
,	,	kIx,
Svazarm	Svazarm	k1gInSc1
<g/>
,	,	kIx,
KSČ	KSČ	kA
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neměl	mít	k5eNaImAgMnS
žádné	žádný	k3yNgInPc4
úkoly	úkol	k1gInPc4
<g/>
,	,	kIx,
žádné	žádný	k3yNgNnSc1
vedení	vedení	k1gNnSc1
a	a	k8xC
ke	k	k7c3
spolupráci	spolupráce	k1gFnSc3
se	se	k3xPyFc4
informátor	informátor	k1gMnSc1
oficiálně	oficiálně	k6eAd1
nezavázal	zavázat	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Ideový	ideový	k2eAgMnSc1d1
spolupracovník	spolupracovník	k1gMnSc1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
IS	IS	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Zcela	zcela	k6eAd1
nejnižší	nízký	k2eAgFnSc2d3
kategorie	kategorie	k1gFnSc2
tajných	tajný	k2eAgMnPc2d1
spolupracovníků	spolupracovník	k1gMnPc2
StB	StB	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
IS	IS	kA
působil	působit	k5eAaImAgMnS
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
,	,	kIx,
kam	kam	k6eAd1
mohl	moct	k5eAaImAgInS
oficiálně	oficiálně	k6eAd1
zajíždět	zajíždět	k5eAaImF
<g/>
,	,	kIx,
pracovat	pracovat	k5eAaImF
atd.	atd.	kA
Mnohdy	mnohdy	k6eAd1
byl	být	k5eAaImAgInS
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
výjezd	výjezd	k1gInSc4
za	za	k7c2
hranice	hranice	k1gFnSc2
právě	právě	k6eAd1
umožněn	umožnit	k5eAaPmNgInS
ziskem	zisk	k1gInSc7
funkce	funkce	k1gFnSc2
IS	IS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
IS	IS	kA
podávali	podávat	k5eAaImAgMnP
pouze	pouze	k6eAd1
zprávy	zpráva	k1gFnPc4
o	o	k7c6
aktivitách	aktivita	k1gFnPc6
svých	svůj	k3xOyFgMnPc2
kolegů	kolega	k1gMnPc2
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
(	(	kIx(
<g/>
různé	různý	k2eAgFnSc2d1
kvality	kvalita	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
informace	informace	k1gFnPc4
ze	z	k7c2
svých	svůj	k3xOyFgNnPc2
zahraničních	zahraniční	k2eAgNnPc2d1
pracovišť	pracoviště	k1gNnPc2
či	či	k8xC
z	z	k7c2
ústavů	ústav	k1gInPc2
a	a	k8xC
škol	škola	k1gFnPc2
(	(	kIx(
<g/>
např.	např.	kA
<g/>
:	:	kIx,
stážisté	stážista	k1gMnPc1
na	na	k7c6
výzkumných	výzkumný	k2eAgInPc6d1
či	či	k8xC
postgraduálních	postgraduální	k2eAgNnPc6d1
stipendiích	stipendium	k1gNnPc6
atd.	atd.	kA
<g/>
)	)	kIx)
motivy	motiv	k1gInPc1
byly	být	k5eAaImAgInP
u	u	k7c2
většiny	většina	k1gFnSc2
pouze	pouze	k6eAd1
ideové	ideový	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
StB	StB	k1gFnSc2
byl	být	k5eAaImAgInS
IS	IS	kA
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
svazek	svazek	k1gInSc1
označen	označit	k5eAaPmNgInS
číslem	číslo	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
začínalo	začínat	k5eAaImAgNnS
č.	č.	k?
8	#num#	k4
či	či	k8xC
88	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kandidát	kandidát	k1gMnSc1
agenta	agent	k1gMnSc2
(	(	kIx(
<g/>
tajného	tajný	k2eAgMnSc2d1
spolupracovníka	spolupracovník	k1gMnSc2
<g/>
)	)	kIx)
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
KA	KA	kA
<g/>
)	)	kIx)
a	a	k8xC
kandidát	kandidát	k1gMnSc1
tajné	tajný	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
KTS	KTS	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
jedince	jedinec	k1gMnSc4
<g/>
,	,	kIx,
kterého	který	k3yQgMnSc4,k3yIgMnSc4,k3yRgMnSc4
chtěla	chtít	k5eAaImAgFnS
StB	StB	k1gFnSc1
získat	získat	k5eAaPmF
za	za	k7c4
tajného	tajný	k1gMnSc4
spolupracovníka	spolupracovník	k1gMnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
spolupracovníkem	spolupracovník	k1gMnSc7
nebyl	být	k5eNaImAgMnS
a	a	k8xC
ke	k	k7c3
spolupráci	spolupráce	k1gFnSc3
se	se	k3xPyFc4
nijak	nijak	k6eAd1
nezavázal	zavázat	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
KTS	KTS	kA
byl	být	k5eAaImAgInS
ale	ale	k8xC
veden	vést	k5eAaImNgInS
osobní	osobní	k2eAgInSc1d1
spis	spis	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
zahrnoval	zahrnovat	k5eAaImAgInS
krycí	krycí	k2eAgNnPc4d1
jméno	jméno	k1gNnSc4
<g/>
,	,	kIx,
registrační	registrační	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
atd.	atd.	kA
Ve	v	k7c6
spisu	spis	k1gInSc6
bylo	být	k5eAaImAgNnS
uvedeno	uvést	k5eAaPmNgNnS
také	také	k6eAd1
zda	zda	k8xS
je	být	k5eAaImIp3nS
vhodný	vhodný	k2eAgInSc1d1
na	na	k7c4
určitou	určitý	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
<g/>
,	,	kIx,
pro	pro	k7c4
jaký	jaký	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
obor	obor	k1gInSc4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
vložena	vložit	k5eAaPmNgFnS
dokumentace	dokumentace	k1gFnSc1
různých	různý	k2eAgFnPc2d1
prověrek	prověrka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kandidát	kandidát	k1gMnSc1
většinou	většinou	k6eAd1
o	o	k7c6
své	svůj	k3xOyFgFnSc6
evidenci	evidence	k1gFnSc6
jako	jako	k8xC,k8xS
KTS	KTS	kA
nevěděl	vědět	k5eNaImAgMnS
<g/>
,	,	kIx,
a	a	k8xC
pokud	pokud	k8xS
se	se	k3xPyFc4
mu	on	k3xPp3gInSc3
příslušníci	příslušník	k1gMnPc1
StB	StB	k1gMnSc4
nepředstavili	představit	k5eNaPmAgMnP
<g/>
,	,	kIx,
nemusel	muset	k5eNaImAgMnS
ani	ani	k9
tušit	tušit	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
jedná	jednat	k5eAaImIp3nS
s	s	k7c7
rozvědkou	rozvědka	k1gFnSc7
StB	StB	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
30	#num#	k4
<g/>
.	.	kIx.
červnu	červen	k1gInSc3
1989	#num#	k4
bylo	být	k5eAaImAgNnS
evidováno	evidovat	k5eAaImNgNnS
3816	#num#	k4
spisů	spis	k1gInPc2
KTS	KTS	kA
<g/>
,	,	kIx,
ještě	ještě	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
jich	on	k3xPp3gMnPc2
bylo	být	k5eAaImAgNnS
ukončeno	ukončit	k5eAaPmNgNnS
1251	#num#	k4
a	a	k8xC
411	#num#	k4
získáno	získán	k2eAgNnSc4d1
ke	k	k7c3
spolupráci	spolupráce	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
roku	rok	k1gInSc2
1989	#num#	k4
byla	být	k5eAaImAgFnS
většina	většina	k1gFnSc1
svazku	svazek	k1gInSc2
KTS	KTS	kA
zničena	zničit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
složka	složka	k1gFnSc1
KTS	KTS	kA
nebyla	být	k5eNaImAgFnS
převedena	převést	k5eAaPmNgFnS
na	na	k7c4
svazek	svazek	k1gInSc4
tajného	tajný	k2eAgMnSc2d1
spolupracovníka	spolupracovník	k1gMnSc2
<g/>
,	,	kIx,
k	k	k7c3
tajné	tajný	k2eAgFnSc3d1
spolupráci	spolupráce	k1gFnSc3
nedošlo	dojít	k5eNaPmAgNnS
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
z	z	k7c2
tohoto	tento	k3xDgNnSc2
pravidla	pravidlo	k1gNnSc2
existují	existovat	k5eAaImIp3nP
odchylky	odchylka	k1gFnPc4
(	(	kIx(
<g/>
vizte	vidět	k5eAaImRp2nP
spis	spis	k1gInSc4
ÉTER	éter	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
KTS	KTS	kA
příslušníci	příslušník	k1gMnPc1
StB	StB	k1gFnSc4
představili	představit	k5eAaPmAgMnP
<g/>
,	,	kIx,
mohl	moct	k5eAaImAgMnS
další	další	k2eAgInSc4d1
styk	styk	k1gInSc4
s	s	k7c7
nimi	on	k3xPp3gMnPc7
přerušit	přerušit	k5eAaPmF
či	či	k8xC
v	v	k7c6
něm	on	k3xPp3gInSc6
dále	daleko	k6eAd2
pokračovat	pokračovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Důvěrný	důvěrný	k2eAgInSc1d1
styk	styk	k1gInSc1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
DS	DS	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
spolupracovník	spolupracovník	k1gMnSc1
nižší	nízký	k2eAgFnSc2d2
kategorie	kategorie	k1gFnSc2
než	než	k8xS
agent	agent	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolupráce	spolupráce	k1gFnSc1
nebyla	být	k5eNaImAgFnS
utajována	utajovat	k5eAaImNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpravidla	zpravidla	k6eAd1
cizí	cizit	k5eAaImIp3nS
státní	státní	k2eAgMnSc1d1
příslušník	příslušník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
DS	DS	kA
byl	být	k5eAaImAgInS
ochoten	ochoten	k2eAgInSc1d1
předávat	předávat	k5eAaImF
citlivé	citlivý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
(	(	kIx(
<g/>
špionáž	špionáž	k1gFnSc1
<g/>
)	)	kIx)
ale	ale	k8xC
nezavázal	zavázat	k5eNaPmAgMnS
se	se	k3xPyFc4
přímo	přímo	k6eAd1
ke	k	k7c3
spolupráci	spolupráce	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
především	především	k9
o	o	k7c4
diplomaty	diplomat	k1gInPc4
(	(	kIx(
<g/>
předávání	předávání	k1gNnSc1
informací	informace	k1gFnPc2
z	z	k7c2
různých	různý	k2eAgInPc2d1
registrů	registr	k1gInPc2
atd.	atd.	kA
<g/>
)	)	kIx)
či	či	k8xC
obchodníky	obchodník	k1gMnPc4
(	(	kIx(
<g/>
průmyslová	průmyslový	k2eAgFnSc1d1
špionáž	špionáž	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
DS	DS	kA
nemusel	muset	k5eNaImAgInS
být	být	k5eAaImF
utajován	utajovat	k5eAaImNgInS
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
měl	mít	k5eAaImAgInS
legální	legální	k2eAgInPc4d1
styky	styk	k1gInPc4
s	s	k7c7
ČSSR	ČSSR	kA
jako	jako	k8xC,k8xS
„	„	k?
<g/>
diplomat	diplomat	k1gMnSc1
<g/>
“	“	k?
či	či	k8xC
„	„	k?
<g/>
obchodní	obchodní	k2eAgMnSc1d1
zástupce	zástupce	k1gMnSc1
<g/>
“	“	k?
atd.	atd.	kA
Důvody	důvod	k1gInPc1
činnosti	činnost	k1gFnSc2
<g/>
:	:	kIx,
Materiální	materiální	k2eAgFnSc1d1
odměna	odměna	k1gFnSc1
<g/>
,	,	kIx,
zahrnovala	zahrnovat	k5eAaImAgFnS
zisk	zisk	k1gInSc4
věcných	věcný	k2eAgInPc2d1
darů	dar	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
<g/>
:	:	kIx,
broušené	broušený	k2eAgNnSc4d1
sklo	sklo	k1gNnSc4
<g/>
,	,	kIx,
lovecké	lovecký	k2eAgFnPc1d1
zbraně	zbraň	k1gFnPc1
apod.	apod.	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
<g/>
,	,	kIx,
protislužba	protislužba	k1gFnSc1
(	(	kIx(
<g/>
např.	např.	kA
osobní	osobní	k2eAgInSc1d1
profit	profit	k1gInSc1
při	při	k7c6
obchodu	obchod	k1gInSc6
s	s	k7c7
ČSSR	ČSSR	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ideové	ideový	k2eAgInPc1d1
důvody	důvod	k1gInPc1
(	(	kIx(
<g/>
boj	boj	k1gInSc1
za	za	k7c4
mír	mír	k1gInSc4
<g/>
,	,	kIx,
socialismus	socialismus	k1gInSc4
<g/>
,	,	kIx,
proti	proti	k7c3
kapitalistickému	kapitalistický	k2eAgInSc3d1
systému	systém	k1gInSc3
vykořisťování	vykořisťování	k1gNnSc2
a	a	k8xC
imperialismu	imperialismus	k1gInSc2
<g/>
,	,	kIx,
monopolům	monopol	k1gInPc3
atd.	atd.	kA
<g/>
)	)	kIx)
jiné	jiný	k2eAgFnSc2d1
(	(	kIx(
<g/>
finanční	finanční	k2eAgFnSc2d1
<g/>
,	,	kIx,
osobní	osobní	k2eAgMnSc1d1
atd.	atd.	kA
<g/>
)	)	kIx)
</s>
<s>
Důvěrník	důvěrník	k1gMnSc1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
D	D	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
ke	k	k7c3
spolupráci	spolupráce	k1gFnSc3
s	s	k7c7
StB	StB	k1gFnPc7
se	se	k3xPyFc4
přímo	přímo	k6eAd1
nezavázal	zavázat	k5eNaPmAgInS
<g/>
,	,	kIx,
neměl	mít	k5eNaImAgInS
oficiálně	oficiálně	k6eAd1
krycí	krycí	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
(	(	kIx(
<g/>
tedy	tedy	k8xC
neznal	znát	k5eNaImAgInS,k5eAaImAgInS
své	svůj	k3xOyFgNnSc4
krycí	krycí	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
<g/>
,	,	kIx,
pod	pod	k7c7
kterým	který	k3yQgMnSc7,k3yRgMnSc7,k3yIgMnSc7
byl	být	k5eAaImAgMnS
evidován	evidován	k2eAgInSc4d1
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
svazek	svazek	k1gInSc4
u	u	k7c2
rozvědky	rozvědka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Především	především	k6eAd1
předával	předávat	k5eAaImAgMnS
informace	informace	k1gFnPc4
z	z	k7c2
různých	různý	k2eAgFnPc2d1
databází	databáze	k1gFnPc2
a	a	k8xC
registrů	registr	k1gInPc2
<g/>
,	,	kIx,
ke	k	k7c3
kterým	který	k3yRgMnPc3,k3yQgMnPc3,k3yIgMnPc3
měl	mít	k5eAaImAgInS
přístup	přístup	k1gInSc4
v	v	k7c6
zaměstnání	zaměstnání	k1gNnSc6
či	či	k8xC
jiné	jiný	k2eAgFnSc3d1
činnosti	činnost	k1gFnSc3
(	(	kIx(
<g/>
SSM	SSM	kA
<g/>
,	,	kIx,
Svazarm	Svazarm	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Upozorňoval	upozorňovat	k5eAaImAgMnS
na	na	k7c4
protispolečenské	protispolečenský	k2eAgMnPc4d1
jedince	jedinec	k1gMnPc4
a	a	k8xC
chování	chování	k1gNnSc4
<g/>
,	,	kIx,
případy	případ	k1gInPc4
konspirace	konspirace	k1gFnSc2
a	a	k8xC
špionáže	špionáž	k1gFnSc2
v	v	k7c6
průmyslových	průmyslový	k2eAgInPc6d1
podnicích	podnik	k1gInPc6
či	či	k8xC
divadlech	divadlo	k1gNnPc6
atd.	atd.	kA
<g/>
,	,	kIx,
staral	starat	k5eAaImAgMnS
se	se	k3xPyFc4
o	o	k7c4
sledování	sledování	k1gNnSc4
či	či	k8xC
fungování	fungování	k1gNnSc4
kupř	kupř	kA
<g/>
.	.	kIx.
konspiračních	konspirační	k2eAgInPc2d1
bytů	byt	k1gInPc2
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
konspirační	konspirační	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
<g/>
,	,	kIx,
měl	mít	k5eAaImAgInS
také	také	k9
na	na	k7c4
starosti	starost	k1gFnPc4
různé	různý	k2eAgFnSc2d1
protikomunistické	protikomunistický	k2eAgFnSc2d1
provokace	provokace	k1gFnSc2
atd.	atd.	kA
"	"	kIx"
<g/>
Déčko	déčko	k1gNnSc1
<g/>
"	"	kIx"
byl	být	k5eAaImAgMnS
buď	buď	k8xC
jedinec	jedinec	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
měl	mít	k5eAaImAgMnS
později	pozdě	k6eAd2
stát	stát	k5eAaPmF,k5eAaImF
spolupracovníkem	spolupracovník	k1gMnSc7
StB	StB	k1gFnSc2
na	na	k7c4
vyšší	vysoký	k2eAgFnSc4d2
pozici	pozice	k1gFnSc4
(	(	kIx(
<g/>
kupř	kupř	kA
<g/>
.	.	kIx.
Agent	agent	k1gMnSc1
A	A	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
byl	být	k5eAaImAgMnS
v	v	k7c6
pozici	pozice	k1gFnSc6
D	D	kA
sledován	sledovat	k5eAaImNgInS
a	a	k8xC
"	"	kIx"
<g/>
testován	testován	k2eAgMnSc1d1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiným	jiný	k2eAgInSc7d1
případem	případ	k1gInSc7
byl	být	k5eAaImAgInS
"	"	kIx"
<g/>
D	D	kA
<g/>
"	"	kIx"
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
působil	působit	k5eAaImAgMnS
dříve	dříve	k6eAd2
na	na	k7c4
vyšší	vysoký	k2eAgFnSc4d2
pozici	pozice	k1gFnSc4
(	(	kIx(
<g/>
např.	např.	kA
<g/>
:	:	kIx,
bývalý	bývalý	k2eAgMnSc1d1
Agent	agent	k1gMnSc1
A	A	kA
či	či	k8xC
F	F	kA
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gInSc1
post	post	k1gInSc1
byl	být	k5eAaImAgInS
statutárně	statutárně	k6eAd1
změněn	změnit	k5eAaPmNgInS
na	na	k7c6
D	D	kA
z	z	k7c2
důvodů	důvod	k1gInPc2
změny	změna	k1gFnSc2
jeho	jeho	k3xOp3gFnSc2
pozice	pozice	k1gFnSc2
v	v	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
socialistické	socialistický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
(	(	kIx(
<g/>
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
členem	člen	k1gInSc7
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
poradcem	poradce	k1gMnSc7
<g/>
,	,	kIx,
vyšším	vysoký	k2eAgMnSc7d2
svazovým	svazový	k2eAgMnSc7d1
funkcionářem	funkcionář	k1gMnSc7
SSM	SSM	kA
<g/>
,	,	kIx,
Svazarm	Svazarm	k1gInSc1
<g/>
,	,	kIx,
zisk	zisk	k1gInSc1
vyšší	vysoký	k2eAgFnSc2d2
pozice	pozice	k1gFnSc2
v	v	k7c6
KSČ	KSČ	kA
<g/>
,	,	kIx,
diplomatická	diplomatický	k2eAgFnSc1d1
mise	mise	k1gFnSc1
atd.	atd.	kA
<g/>
)	)	kIx)
"	"	kIx"
<g/>
Déčko	déčko	k1gNnSc1
<g/>
"	"	kIx"
o	o	k7c6
svém	svůj	k3xOyFgInSc6
styku	styk	k1gInSc6
s	s	k7c7
StB	StB	k1gFnSc7
povětšinou	povětšinou	k6eAd1
vědělo	vědět	k5eAaImAgNnS
<g/>
,	,	kIx,
objevily	objevit	k5eAaPmAgInP
se	se	k3xPyFc4
i	i	k9
případy	případ	k1gInPc7
nevědomého	vědomý	k2eNgNnSc2d1
vedení	vedení	k1gNnSc2
jako	jako	k8xC,k8xS
"	"	kIx"
<g/>
D	D	kA
<g/>
"	"	kIx"
ve	v	k7c6
spisu	spis	k1gInSc6
StB	StB	k1gMnPc1
(	(	kIx(
<g/>
normalizační	normalizační	k2eAgMnPc1d1
herci	herec	k1gMnPc1
a	a	k8xC
sportovci	sportovec	k1gMnPc1
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Agent	agent	k1gMnSc1
A	A	kA
(	(	kIx(
<g/>
uváděno	uvádět	k5eAaImNgNnS
také	také	k9
jako	jako	k9
„	„	k?
<g/>
A	a	k8xC
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
:	:	kIx,
tajný	tajný	k2eAgMnSc1d1
spolupracovník	spolupracovník	k1gMnSc1
StB	StB	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
ke	k	k7c3
spolupráci	spolupráce	k1gFnSc3
písemně	písemně	k6eAd1
nebo	nebo	k8xC
ústně	ústně	k6eAd1
zavázal	zavázat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Agent	agent	k1gMnSc1
A	a	k9
udržoval	udržovat	k5eAaImAgMnS
komunikaci	komunikace	k1gFnSc4
(	(	kIx(
<g/>
konspirativní	konspirativní	k2eAgInSc1d1
styk	styk	k1gInSc1
<g/>
)	)	kIx)
s	s	k7c7
řídícím	řídící	k2eAgMnSc7d1
důstojníkem	důstojník	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plnil	plnit	k5eAaImAgInS
rozličné	rozličný	k2eAgInPc4d1
úkoly	úkol	k1gInPc4
specifikované	specifikovaný	k2eAgInPc4d1
podle	podle	k7c2
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
zařazení	zařazení	k1gNnSc2
do	do	k7c2
určitých	určitý	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
,	,	kIx,
např.	např.	kA
<g/>
:	:	kIx,
„	„	k?
<g/>
Volná	volný	k2eAgFnSc1d1
mládež	mládež	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
skupiny	skupina	k1gFnPc1
podle	podle	k7c2
schopností	schopnost	k1gFnPc2
<g/>
,	,	kIx,
zaměstnání	zaměstnání	k1gNnSc2
<g/>
,	,	kIx,
podle	podle	k7c2
charakteru	charakter	k1gInSc2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
oboru	obor	k1gInSc2
<g/>
,	,	kIx,
jazykových	jazykový	k2eAgFnPc2d1
dovedností	dovednost	k1gFnPc2
<g/>
,	,	kIx,
talentu	talent	k1gInSc2
atd.	atd.	kA
Úkoly	úkol	k1gInPc1
byly	být	k5eAaImAgInP
různého	různý	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
<g/>
,	,	kIx,
převážně	převážně	k6eAd1
však	však	k9
informační	informační	k2eAgFnSc4d1
<g/>
,	,	kIx,
dokumentační	dokumentační	k2eAgFnSc4d1
či	či	k8xC
naopak	naopak	k6eAd1
dezinformační	dezinformační	k2eAgFnPc1d1
a	a	k8xC
rozkladné	rozkladný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
často	často	k6eAd1
honorován	honorovat	k5eAaBmNgInS
<g/>
,	,	kIx,
ale	ale	k8xC
pracoval	pracovat	k5eAaImAgMnS
i	i	k9
zcela	zcela	k6eAd1
zdarma	zdarma	k6eAd1
s	s	k7c7
vidinou	vidina	k1gFnSc7
jisté	jistý	k2eAgFnSc2d1
výhodnosti	výhodnost	k1gFnSc2
spolupráce	spolupráce	k1gFnSc2
v	v	k7c6
budoucnosti	budoucnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1
termíny	termín	k1gInPc1
</s>
<s>
Nepřátelská	přátelský	k2eNgFnSc1d1
osoba	osoba	k1gFnSc1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
NO	no	k9
<g/>
)	)	kIx)
<g/>
:	:	kIx,
jako	jako	k8xC,k8xS
Nepřátelská	přátelský	k2eNgFnSc1d1
osoba	osoba	k1gFnSc1
byl	být	k5eAaImAgMnS
označen	označen	k2eAgMnSc1d1
jedinec	jedinec	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
byl	být	k5eAaImAgMnS
„	„	k?
<g/>
odpůrcem	odpůrce	k1gMnSc7
komunismu	komunismus	k1gInSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
ale	ale	k8xC
odpůrcem	odpůrce	k1gMnSc7
komunismu	komunismus	k1gInSc2
z	z	k7c2
hlediska	hledisko	k1gNnSc2
komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
a	a	k8xC
sovětských	sovětský	k2eAgMnPc2d1
komunistů	komunista	k1gMnPc2
(	(	kIx(
<g/>
po	po	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
„	„	k?
<g/>
NO	no	k9
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
“	“	k?
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
příslušníkem	příslušník	k1gMnSc7
např.	např.	kA
<g/>
:	:	kIx,
zakázané	zakázaný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
státem	stát	k1gInSc7
povolené	povolený	k2eAgFnSc2d1
církve	církev	k1gFnSc2
<g/>
,	,	kIx,
výstředním	výstřední	k2eAgMnSc7d1
prozápadním	prozápadní	k2eAgMnSc7d1
avantgardním	avantgardní	k2eAgMnSc7d1
umělcem	umělec	k1gMnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
socialistickým	socialistický	k2eAgMnSc7d1
realistou	realista	k1gMnSc7
(	(	kIx(
<g/>
kupř	kupř	kA
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Pavel	Pavel	k1gMnSc1
Kohout	Kohout	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
změnil	změnit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
světonázor	světonázor	k1gInSc4
atd.	atd.	kA
Důležitou	důležitý	k2eAgFnSc7d1
skupinou	skupina	k1gFnSc7
byli	být	k5eAaImAgMnP
„	„	k?
<g/>
nedůvěryhodní	důvěryhodný	k2eNgMnPc1d1
<g/>
“	“	k?
jedinci	jedinec	k1gMnPc1
socialistického	socialistický	k2eAgInSc2d1
„	„	k?
<g/>
establišmentu	establišment	k1gInSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
kupř	kupř	kA
<g/>
.	.	kIx.
vysoce	vysoce	k6eAd1
postavený	postavený	k2eAgMnSc1d1
člen	člen	k1gMnSc1
KSČ	KSČ	kA
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
zdiskreditoval	zdiskreditovat	k5eAaPmAgInS
během	během	k7c2
roku	rok	k1gInSc2
1968	#num#	k4
se	se	k3xPyFc4
ocitl	ocitnout	k5eAaPmAgInS
kvůli	kvůli	k7c3
své	svůj	k3xOyFgFnSc2
"	"	kIx"
<g/>
nedůvěryhodnosti	nedůvěryhodnost	k1gFnSc2
<g/>
"	"	kIx"
na	na	k7c6
seznamu	seznam	k1gInSc6
NO	no	k9
atd.	atd.	kA
</s>
<s>
Fond	fond	k1gInSc1
zvláštního	zvláštní	k2eAgNnSc2d1
uložení	uložení	k1gNnSc2
(	(	kIx(
<g/>
zvaný	zvaný	k2eAgInSc1d1
i	i	k8xC
Fond	fond	k1gInSc1
Z	z	k7c2
-	-	kIx~
pozor	pozor	k1gInSc1
na	na	k7c4
možnou	možný	k2eAgFnSc4d1
záměnu	záměna	k1gFnSc4
s	s	k7c7
fondem	fond	k1gInSc7
Z	z	k7c2
Studijního	studijní	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
MV	MV	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
část	část	k1gFnSc4
operativního	operativní	k2eAgInSc2d1
archivu	archiv	k1gInSc2
StB	StB	k1gMnPc2
s	s	k7c7
režimovým	režimový	k2eAgInSc7d1
přístupem	přístup	k1gInSc7
<g/>
,	,	kIx,
do	do	k7c2
níž	jenž	k3xRgFnSc2
byly	být	k5eAaImAgFnP
ukládány	ukládat	k5eAaImNgFnP
některé	některý	k3yIgInPc4
"	"	kIx"
<g/>
citlivější	citlivý	k2eAgInPc4d2
<g/>
"	"	kIx"
osobní	osobní	k2eAgInPc4d1
svazky	svazek	k1gInPc4
a	a	k8xC
vyšetřovací	vyšetřovací	k2eAgInPc4d1
spisy	spis	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známým	známá	k1gFnPc3
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
fond	fond	k1gInSc1
stal	stát	k5eAaPmAgInS
na	na	k7c6
jaře	jaro	k1gNnSc6
1990	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
do	do	k7c2
něj	on	k3xPp3gMnSc2
z	z	k7c2
příkazu	příkaz	k1gInSc2
ministra	ministr	k1gMnSc2
Sachera	Sacher	k1gMnSc2
byly	být	k5eAaImAgFnP
nesystematicky	systematicky	k6eNd1
převedeny	převést	k5eAaPmNgInP
svazky	svazek	k1gInPc1
vedené	vedený	k2eAgFnSc2d1
k	k	k7c3
nově	nově	k6eAd1
kooptovaným	kooptovaný	k2eAgMnPc3d1
členům	člen	k1gMnPc3
Federálního	federální	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
<g/>
,	,	kIx,
členům	člen	k1gInPc3
vlád	vláda	k1gFnPc2
a	a	k8xC
dalších	další	k2eAgMnPc2d1
ústavních	ústavní	k2eAgMnPc2d1
činitelů	činitel	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
spolu	spolu	k6eAd1
se	se	k3xPyFc4
zásahy	zásah	k1gInPc7
do	do	k7c2
centrální	centrální	k2eAgFnSc2d1
evidence	evidence	k1gFnSc2
zavdalo	zavdat	k5eAaPmAgNnS
příčinu	příčina	k1gFnSc4
k	k	k7c3
mnoha	mnoho	k4c3
„	„	k?
<g/>
smělým	smělý	k2eAgNnSc7d1
tvrzením	tvrzení	k1gNnSc7
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rezident	rezident	k1gMnSc1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
R	R	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Má	mít	k5eAaImIp3nS
více	hodně	k6eAd2
významů	význam	k1gInPc2
<g/>
,	,	kIx,
existoval	existovat	k5eAaImAgMnS
<g/>
:	:	kIx,
1	#num#	k4
<g/>
)	)	kIx)
Rezident	rezident	k1gMnSc1
rozvědky	rozvědka	k1gFnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
vedoucím	vedoucí	k1gMnSc7
rozvědné	rozvědný	k2eAgFnSc2d1
rezidentury	rezidentura	k1gFnSc2
(	(	kIx(
<g/>
sítě	síť	k1gFnSc2
<g/>
)	)	kIx)
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
pracoval	pracovat	k5eAaImAgMnS
pod	pod	k7c7
různým	různý	k2eAgNnSc7d1
diplomatickým	diplomatický	k2eAgNnSc7d1
krytím	krytí	k1gNnSc7
<g/>
,	,	kIx,
kupř	kupř	kA
<g/>
.	.	kIx.
<g/>
:	:	kIx,
na	na	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
čsl	čsl	kA
<g/>
.	.	kIx.
ambasádě	ambasáda	k1gFnSc6
či	či	k8xC
jiném	jiný	k2eAgInSc6d1
zastupitelském	zastupitelský	k2eAgInSc6d1
úřadě	úřad	k1gInSc6
(	(	kIx(
<g/>
zahraniční	zahraniční	k2eAgInSc4d1
export	export	k1gInSc4
<g/>
,	,	kIx,
filiálka	filiálka	k1gFnSc1
<g/>
,	,	kIx,
rozhlas	rozhlas	k1gInSc1
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
2	#num#	k4
<g/>
)	)	kIx)
Rezident	rezident	k1gMnSc1
kontrarozvědky	kontrarozvědka	k1gFnSc2
(	(	kIx(
<g/>
II	II	kA
<g/>
.	.	kIx.
správa	správa	k1gFnSc1
StB	StB	k1gFnSc1
<g/>
)	)	kIx)
-	-	kIx~
byl	být	k5eAaImAgInS
to	ten	k3xDgNnSc1
spolehlivý	spolehlivý	k2eAgInSc1d1
a	a	k8xC
prověřený	prověřený	k2eAgInSc1d1
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
zkušený	zkušený	k2eAgMnSc1d1
tajný	tajný	k2eAgMnSc1d1
spolupracovník	spolupracovník	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
řídil	řídit	k5eAaImAgMnS
jiné	jiný	k2eAgMnPc4d1
spolupracovníky	spolupracovník	k1gMnPc4
(	(	kIx(
<g/>
ale	ale	k8xC
pracoval	pracovat	k5eAaImAgMnS
pod	pod	k7c7
vedením	vedení	k1gNnSc7
svého	svůj	k3xOyFgMnSc2
důstojníka	důstojník	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
R	R	kA
organizoval	organizovat	k5eAaBmAgInS
a	a	k8xC
řídil	řídit	k5eAaImAgInS
svěřené	svěřený	k2eAgInPc4d1
agenty	agens	k1gInPc4
<g/>
,	,	kIx,
stanovoval	stanovovat	k5eAaImAgMnS
úkoly	úkol	k1gInPc4
<g/>
,	,	kIx,
strategie	strategie	k1gFnPc4
<g/>
,	,	kIx,
dosažení	dosažení	k1gNnPc4
cílů	cíl	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sbíral	sbírat	k5eAaImAgMnS
a	a	k8xC
vyhodnocoval	vyhodnocovat	k5eAaImAgMnS
informace	informace	k1gFnPc4
a	a	k8xC
výsledky	výsledek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rezidenti	rezident	k1gMnPc1
kontaminovali	kontaminovat	k5eAaBmAgMnP
průmysl	průmysl	k1gInSc4
<g/>
,	,	kIx,
média	médium	k1gNnPc4
<g/>
,	,	kIx,
školy	škola	k1gFnPc4
<g/>
,	,	kIx,
kulturu	kultura	k1gFnSc4
<g/>
.	.	kIx.
„	„	k?
<g/>
R	R	kA
<g/>
“	“	k?
byli	být	k5eAaImAgMnP
vytvářeni	vytvářet	k5eAaImNgMnP,k5eAaPmNgMnP
ze	z	k7c2
zkušených	zkušený	k2eAgMnPc2d1
agentů	agent	k1gMnPc2
<g/>
,	,	kIx,
tvořili	tvořit	k5eAaImAgMnP
je	on	k3xPp3gNnSc4
často	často	k6eAd1
zaměstnanci	zaměstnanec	k1gMnPc7
ministerstev	ministerstvo	k1gNnPc2
vnitra	vnitro	k1gNnSc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
pracovali	pracovat	k5eAaImAgMnP
na	na	k7c6
civilních	civilní	k2eAgNnPc6d1
pracovištích	pracoviště	k1gNnPc6
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
byli	být	k5eAaImAgMnP
v	v	k7c6
policejním	policejní	k2eAgInSc6d1
důchodu	důchod	k1gInSc6
<g/>
.	.	kIx.
„	„	k?
<g/>
R	R	kA
<g/>
“	“	k?
měl	mít	k5eAaImAgInS
pravidelné	pravidelný	k2eAgFnPc4d1
odměny	odměna	k1gFnPc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
pravidelný	pravidelný	k2eAgInSc1d1
příjem	příjem	k1gInSc1
za	za	k7c4
své	svůj	k3xOyFgFnPc4
služby	služba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Diplomatické	diplomatický	k2eAgNnSc1d1
krytí	krytí	k1gNnSc1
<g/>
:	:	kIx,
Termín	termín	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
užívala	užívat	k5eAaImAgFnS
I.	I.	kA
správa	správa	k1gFnSc1
StB	StB	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Agent	agent	k1gMnSc1
byl	být	k5eAaImAgMnS
vyslán	vyslat	k5eAaPmNgMnS
rozvědkou	rozvědka	k1gFnSc7
do	do	k7c2
zahraničí	zahraničí	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
oficiálně	oficiálně	k6eAd1
pracoval	pracovat	k5eAaImAgMnS
např.	např.	kA
<g/>
:	:	kIx,
v	v	k7c6
týmu	tým	k1gInSc6
na	na	k7c4
čs	čs	kA
<g/>
.	.	kIx.
ambasádě	ambasáda	k1gFnSc3
<g/>
,	,	kIx,
v	v	k7c6
podniku	podnik	k1gInSc6
zabývajícím	zabývající	k2eAgMnSc7d1
se	se	k3xPyFc4
exportem	export	k1gInSc7
<g/>
,	,	kIx,
kulturním	kulturní	k2eAgNnSc6d1
centru	centrum	k1gNnSc6
<g/>
,	,	kIx,
turistické	turistický	k2eAgFnSc3d1
agentuře	agentura	k1gFnSc3
<g/>
,	,	kIx,
finanční	finanční	k2eAgFnSc3d1
či	či	k8xC
vzdělávací	vzdělávací	k2eAgFnSc3d1
instituci	instituce	k1gFnSc3
atd.	atd.	kA
</s>
<s>
Dublér	Dublér	k1gMnSc1
(	(	kIx(
<g/>
agent	agent	k1gMnSc1
dublér	dublér	k1gMnSc1
<g/>
,	,	kIx,
zkratka	zkratka	k1gFnSc1
D	D	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Také	také	k9
„	„	k?
<g/>
double	double	k1gInSc1
agent	agent	k1gMnSc1
<g/>
“	“	k?
byl	být	k5eAaImAgMnS
typ	typ	k1gInSc4
agenta	agent	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
pracoval	pracovat	k5eAaImAgMnS
pro	pro	k7c4
více	hodně	k6eAd2
zpravodajských	zpravodajský	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
(	(	kIx(
<g/>
většinou	většinou	k6eAd1
2	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
to	ten	k3xDgNnSc1
buď	buď	k8xC
pro	pro	k7c4
obě	dva	k4xCgFnPc4
strany	strana	k1gFnPc4
současně	současně	k6eAd1
<g/>
,	,	kIx,
či	či	k8xC
po	po	k7c4
jedno	jeden	k4xCgNnSc4
období	období	k1gNnSc4
pouze	pouze	k6eAd1
pro	pro	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
nich	on	k3xPp3gFnPc2
<g/>
.	.	kIx.
„	„	k?
<g/>
D	D	kA
<g/>
“	“	k?
používal	používat	k5eAaImAgMnS
často	často	k6eAd1
dezinformace	dezinformace	k1gFnSc2
<g/>
,	,	kIx,
tedy	tedy	k9
zkreslené	zkreslený	k2eAgFnPc1d1
a	a	k8xC
matoucí	matoucí	k2eAgFnPc1d1
informace	informace	k1gFnPc1
na	na	k7c6
základě	základ	k1gInSc6
pravdivých	pravdivý	k2eAgInPc2d1
faktů	fakt	k1gInPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nebyl	být	k5eNaImAgInS
odhalen	odhalen	k2eAgInSc1d1
uvnitř	uvnitř	k7c2
"	"	kIx"
<g/>
domovské	domovský	k2eAgFnSc2d1
<g/>
"	"	kIx"
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvalitní	kvalitní	k2eAgInSc1d1
„	„	k?
<g/>
D	D	kA
<g/>
“	“	k?
měl	mít	k5eAaImAgMnS
pro	pro	k7c4
příslušnou	příslušný	k2eAgFnSc4d1
rozvědku	rozvědka	k1gFnSc4
vysokou	vysoký	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
nabízení	nabízení	k1gNnSc3
si	se	k3xPyFc3
falešných	falešný	k2eAgMnPc2d1
„	„	k?
<g/>
D	D	kA
<g/>
“	“	k?
rozvědkami	rozvědka	k1gFnPc7
navzájem	navzájem	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odhalený	odhalený	k2eAgMnSc1d1
„	„	k?
<g/>
D	D	kA
<g/>
“	“	k?
již	již	k6eAd1
ztratil	ztratit	k5eAaPmAgInS
jakýkoliv	jakýkoliv	k3yIgInSc4
vliv	vliv	k1gInSc4
a	a	k8xC
význam	význam	k1gInSc4
a	a	k8xC
nemohl	moct	k5eNaImAgMnS
být	být	k5eAaImF
nasazen	nasadit	k5eAaPmNgMnS
do	do	k7c2
další	další	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odhalený	odhalený	k2eAgMnSc1d1
„	„	k?
<g/>
D	D	kA
<g/>
“	“	k?
buď	buď	k8xC
emigroval	emigrovat	k5eAaBmAgMnS
do	do	k7c2
země	zem	k1gFnSc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
dodával	dodávat	k5eAaImAgMnS
informace	informace	k1gFnPc4
(	(	kIx(
<g/>
kupř	kupř	kA
<g/>
.	.	kIx.
do	do	k7c2
SSSR	SSSR	kA
<g/>
,	,	kIx,
či	či	k8xC
naopak	naopak	k6eAd1
do	do	k7c2
kapitalistické	kapitalistický	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
či	či	k8xC
byl	být	k5eAaImAgInS
odsouzen	odsouzet	k5eAaImNgInS,k5eAaPmNgInS
za	za	k7c4
„	„	k?
<g/>
vlastizradu	vlastizrada	k1gFnSc4
<g/>
“	“	k?
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
domovské	domovský	k2eAgFnSc6d1
síti	síť	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvody	důvod	k1gInPc1
činnosti	činnost	k1gFnSc2
<g/>
:	:	kIx,
materiální	materiální	k2eAgNnSc4d1
(	(	kIx(
<g/>
zahraniční	zahraniční	k2eAgNnSc4d1
zboží	zboží	k1gNnSc4
<g/>
,	,	kIx,
jiné	jiný	k2eAgFnPc4d1
výhody	výhoda	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
finanční	finanční	k2eAgFnSc3d1
<g/>
,	,	kIx,
ideové	ideový	k2eAgFnSc3d1
nebo	nebo	k8xC
ryze	ryze	k6eAd1
osobní	osobní	k2eAgInPc1d1
(	(	kIx(
<g/>
např.	např.	kA
<g/>
:	:	kIx,
msta	msta	k1gFnSc1
za	za	k7c4
úmrtí	úmrtí	k1gNnSc4
dítěte	dítě	k1gNnSc2
vinou	vinou	k7c2
sovětských	sovětský	k2eAgMnPc2d1
lékařů	lékař	k1gMnPc2
v	v	k7c6
socialistické	socialistický	k2eAgFnSc6d1
nemocnici	nemocnice	k1gFnSc6
atd.	atd.	kA
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Historie	historie	k1gFnSc1
vzniku	vznik	k1gInSc2
<g/>
,	,	kIx,
Bezpečnostní	bezpečnostní	k2eAgFnSc1d1
informační	informační	k2eAgFnSc1d1
služba	služba	k1gFnSc1
ČR	ČR	kA
-	-	kIx~
Kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
jsme	být	k5eAaImIp1nP
Archivováno	archivován	k2eAgNnSc4d1
7	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
149	#num#	k4
<g/>
/	/	kIx~
<g/>
1947	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
Archivováno	archivován	k2eAgNnSc1d1
18	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BUBEN	buben	k1gInSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
M.	M.	kA
Encyklopedie	encyklopedie	k1gFnSc1
řádů	řád	k1gInPc2
<g/>
,	,	kIx,
kongregací	kongregace	k1gFnPc2
a	a	k8xC
řeholních	řeholní	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
katolické	katolický	k2eAgFnSc2d1
církve	církev	k1gFnSc2
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
II	II	kA
<g/>
,	,	kIx,
svazek	svazek	k1gInSc4
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
55	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KROUPA	Kroupa	k1gMnSc1
<g/>
,	,	kIx,
Mikuláš	Mikuláš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příběhy	příběh	k1gInPc1
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
:	:	kIx,
Komunistu	komunista	k1gMnSc4
mučilo	mučit	k5eAaImAgNnS
gestapo	gestapo	k1gNnSc1
<g/>
,	,	kIx,
zkušenosti	zkušenost	k1gFnPc4
využil	využít	k5eAaPmAgMnS
u	u	k7c2
StB	StB	k1gFnSc2
<g/>
.	.	kIx.
iDnes	iDnes	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-01-25	2014-01-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
</s>
<s>
PLACÁK	placák	k1gInSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
normalizačních	normalizační	k2eAgMnPc6d1
důstojnících	důstojník	k1gMnPc6
StB	StB	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
podobali	podobat	k5eAaImAgMnP
svým	svůj	k3xOyFgMnPc3
předchůdcům	předchůdce	k1gMnPc3
z	z	k7c2
padesátých	padesátý	k4xOgNnPc2
let	léto	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
http://domaci.ihned.cz/c1-45491630-zadne-vyhody-necekam-rika-prevadec-poslali-ho-do-uranovych-dolu	http://domaci.ihned.cz/c1-45491630-zadne-vyhody-necekam-rika-prevadec-poslali-ho-do-uranovych-dol	k1gInSc2
<g/>
↑	↑	k?
Osočení	osočení	k1gNnSc1
:	:	kIx,
pravdivé	pravdivý	k2eAgInPc1d1
příběhy	příběh	k1gInPc1
lidí	člověk	k1gMnPc2
z	z	k7c2
"	"	kIx"
<g/>
Cibulkova	Cibulkův	k2eAgInSc2d1
seznamu	seznam	k1gInSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
:	:	kIx,
Host	host	k1gMnSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
417	#num#	k4
s.	s.	k?
<g/>
↑	↑	k?
Kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
umělci	umělec	k1gMnPc1
odmítli	odmítnout	k5eAaPmAgMnP
podepsat	podepsat	k5eAaPmF
Antichartu	anticharta	k1gFnSc4
a	a	k8xC
jak	jak	k6eAd1
dopadli	dopadnout	k5eAaPmAgMnP
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Extrastory	Extrastor	k1gInPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
27	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Langer	Langer	k1gMnSc1
<g/>
:	:	kIx,
Dědictví	dědictví	k1gNnSc1
StB	StB	k1gFnSc2
je	být	k5eAaImIp3nS
hrozné	hrozný	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
zvládnout	zvládnout	k5eAaPmF
to	ten	k3xDgNnSc4
musíme	muset	k5eAaImIp1nP
rozhovor	rozhovor	k1gInSc1
online	onlinout	k5eAaPmIp3nS
<g/>
,	,	kIx,
iHNed	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
31.1	31.1	k4
<g/>
.2007	.2007	k4
1	#num#	k4
2	#num#	k4
Přístup	přístup	k1gInSc1
do	do	k7c2
archivů	archiv	k1gInPc2
StB	StB	k1gMnPc2
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
jednodušší	jednoduchý	k2eAgMnSc1d2
<g/>
,	,	kIx,
iHNed	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
5.2	5.2	k4
<g/>
.2007	.2007	k4
↑	↑	k?
Kilometry	kilometr	k1gInPc1
spisů	spis	k1gInPc2
StB	StB	k1gFnSc4
budou	být	k5eAaImBp3nP
na	na	k7c6
webu	web	k1gInSc6
<g/>
,	,	kIx,
slíbil	slíbit	k5eAaPmAgMnS
Langer	Langer	k1gMnSc1
<g/>
,	,	kIx,
iHNed	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
14.2	14.2	k4
<g/>
.20071	.20071	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Struktura	struktura	k1gFnSc1
StB	StB	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Policie	policie	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
Struktura	struktura	k1gFnSc1
StB	StB	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://www.abscr.cz/cs/vyhledavani-archivni-pomucky	http://www.abscr.cz/cs/vyhledavani-archivni-pomucka	k1gFnSc2
<g/>
↑	↑	k?
http://www.abscr.cz/cs/svazky-konspiracnich-a-propujcenych-bytu-svazky-radu-7	http://www.abscr.cz/cs/svazky-konspiracnich-a-propujcenych-bytu-svazky-radu-7	k4
<g/>
↑	↑	k?
Evidence	evidence	k1gFnSc2
agentů	agens	k1gInPc2
F	F	kA
byla	být	k5eAaImAgFnS
odstraněna	odstranit	k5eAaPmNgFnS
již	již	k6eAd1
na	na	k7c6
konci	konec	k1gInSc6
listopadu	listopad	k1gInSc2
1989	#num#	k4
<g/>
,	,	kIx,
viz	vidět	k5eAaImRp2nS
<g/>
:	:	kIx,
</s>
<s>
http://www.cibulka.com/nnoviny/nn1999/nn0199/obsah/43.htm	http://www.cibulka.com/nnoviny/nn1999/nn0199/obsah/43.htm	k6eAd1
</s>
<s>
https://www.parlamentnilisty.cz/arena/nazory-a-petice/Zdenek-Slanina-Tri-malo-znama-F-z-kalnych-vod-provozu-StB-Agenti-F-fiktivni-verbovky-falsovani-fondu-563637	https://www.parlamentnilisty.cz/arena/nazory-a-petice/Zdenek-Slanina-Tri-malo-znama-F-z-kalnych-vod-provozu-StB-Agenti-F-fiktivni-verbovky-falsovani-fondu-563637	k4
<g/>
↑	↑	k?
Paměť	paměť	k1gFnSc4
a	a	k8xC
dějiny	dějiny	k1gFnPc4
s.	s.	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
-	-	kIx~
https://www.ustrcr.cz/data/pdf/pamet-dejiny/pad1504/003-019.pdf	https://www.ustrcr.cz/data/pdf/pamet-dejiny/pad1504/003-019.pdf	k1gMnSc1
<g/>
↑	↑	k?
Jan	Jan	k1gMnSc1
Frolík	Frolík	k1gMnSc1
<g/>
:	:	kIx,
Zvláštní	zvláštní	k2eAgInSc1d1
fond	fond	k1gInSc1
operativního	operativní	k2eAgInSc2d1
archivu	archiv	k1gInSc2
StB	StB	k1gFnSc1
<g/>
,	,	kIx,
Sborník	sborník	k1gInSc1
Archivu	archiv	k1gInSc2
MV	MV	kA
2	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Antonín	Antonín	k1gMnSc1
Kratochvil	kratochvíle	k1gFnPc2
<g/>
:	:	kIx,
Žaluji	žalovat	k5eAaImIp1nS
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalinská	stalinský	k2eAgFnSc1d1
justice	justice	k1gFnSc1
v	v	k7c6
Československu	Československo	k1gNnSc6
<g/>
,	,	kIx,
Mnichov	Mnichov	k1gInSc4
1973	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1990	#num#	k4
<g/>
;	;	kIx,
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrátit	vrátit	k5eAaPmF
slovo	slovo	k1gNnSc1
umlčeným	umlčený	k2eAgFnPc3d1
<g/>
,	,	kIx,
Haarlem	Haarl	k1gMnSc7
1975	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1990	#num#	k4
<g/>
;	;	kIx,
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cestou	cesta	k1gFnSc7
k	k	k7c3
Sionu	Siono	k1gNnSc3
<g/>
,	,	kIx,
Haarlem	Haarl	k1gMnSc7
1977	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1990	#num#	k4
</s>
<s>
Tomek	Tomek	k1gMnSc1
<g/>
,	,	kIx,
Prokop	Prokop	k1gMnSc1
<g/>
:	:	kIx,
Estébáckou	estébácký	k2eAgFnSc7d1
Prahou	Praha	k1gFnSc7
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Academia	academia	k1gFnSc1
2013	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-200-2290-5	978-80-200-2290-5	k4
</s>
<s>
Koudelka	Koudelka	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgFnSc1d1
bezpečnost	bezpečnost	k1gFnSc1
1954	#num#	k4
<g/>
-	-	kIx~
<g/>
1968	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Základní	základní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1993	#num#	k4
<g/>
,	,	kIx,
Sešity	sešit	k1gInPc1
Ústavu	ústav	k1gInSc2
pro	pro	k7c4
soudobé	soudobý	k2eAgFnPc4d1
dějiny	dějiny	k1gFnPc4
AV	AV	kA
ČR	ČR	kA
13	#num#	k4
</s>
<s>
Osočení	osočení	k1gNnSc1
:	:	kIx,
pravdivé	pravdivý	k2eAgInPc1d1
příběhy	příběh	k1gInPc1
lidí	člověk	k1gMnPc2
z	z	k7c2
"	"	kIx"
<g/>
Cibulkova	Cibulkův	k2eAgInSc2d1
seznamu	seznam	k1gInSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
:	:	kIx,
Host	host	k1gMnSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
417	#num#	k4
s.	s.	k?
Ukázka	ukázka	k1gFnSc1
z	z	k7c2
doslovu	doslov	k1gInSc2
Josefa	Josef	k1gMnSc2
Škvoreckého	Škvorecký	k2eAgInSc2d1
je	být	k5eAaImIp3nS
dostupná	dostupný	k2eAgFnSc1d1
také	také	k6eAd1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Gula	gula	k1gFnSc1
<g/>
,	,	kIx,
Marián	Marián	k1gMnSc1
<g/>
:	:	kIx,
Vývoj	vývoj	k1gInSc1
typů	typ	k1gInPc2
spolupracovníků	spolupracovník	k1gMnPc2
kontrarozvědky	kontrarozvědka	k1gFnSc2
StB	StB	k1gFnSc1
ve	v	k7c6
směrnicích	směrnice	k1gFnPc6
pro	pro	k7c4
agenturní	agenturní	k2eAgFnSc4d1
práci	práce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Securitas	Securitasa	k1gFnPc2
imperii	imperie	k1gFnSc4
1	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
6-17	6-17	k4
</s>
<s>
Placák	placák	k1gInSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
:	:	kIx,
StB	StB	k1gMnSc1
a	a	k8xC
"	"	kIx"
<g/>
protizákonné	protizákonný	k2eAgFnPc4d1
písemnosti	písemnost	k1gFnPc4
<g/>
"	"	kIx"
v	v	k7c6
osmdesátých	osmdesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Securitas	Securitasa	k1gFnPc2
imperii	imperie	k1gFnSc4
1	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
32-59	32-59	k4
</s>
<s>
Žáček	Žáček	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
:	:	kIx,
Iluze	iluze	k1gFnSc1
a	a	k8xC
realita	realita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokus	pokus	k1gInSc1
o	o	k7c4
reformu	reforma	k1gFnSc4
StB	StB	k1gFnSc2
v	v	k7c6
letech	léto	k1gNnPc6
1987	#num#	k4
<g/>
-	-	kIx~
<g/>
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Securitas	Securitasa	k1gFnPc2
imperii	imperie	k1gFnSc4
3	#num#	k4
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
7-210	7-210	k4
</s>
<s>
Cuhra	Cuhra	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
:	:	kIx,
Katolická	katolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
přelomu	přelom	k1gInSc2
80	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
v	v	k7c6
diplomových	diplomový	k2eAgFnPc6d1
pracích	práce	k1gFnPc6
příslušníků	příslušník	k1gMnPc2
StB	StB	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Securitas	Securitasa	k1gFnPc2
imperii	imperie	k1gFnSc4
5	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
3-145	3-145	k4
</s>
<s>
Gruntorád	Gruntoráda	k1gFnPc2
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgFnSc1d1
bezpečnost	bezpečnost	k1gFnSc1
proti	proti	k7c3
Chartě	charta	k1gFnSc3
77	#num#	k4
-	-	kIx~
Akce	akce	k1gFnSc1
Izolace	izolace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Securitas	Securitasa	k1gFnPc2
imperii	imperie	k1gFnSc4
5	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
173-190	173-190	k4
</s>
<s>
Málek	Málek	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
:	:	kIx,
Metody	metoda	k1gFnPc1
Státní	státní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
a	a	k8xC
likvidace	likvidace	k1gFnSc2
třetího	třetí	k4xOgInSc2
odboje	odboj	k1gInSc2
v	v	k7c6
letech	léto	k1gNnPc6
1948	#num#	k4
<g/>
-	-	kIx~
<g/>
1953	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Za	za	k7c4
svobodu	svoboda	k1gFnSc4
a	a	k8xC
demokracii	demokracie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpor	odpor	k1gInSc1
proti	proti	k7c3
komunistické	komunistický	k2eAgFnSc3d1
moci	moc	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Karolinum	Karolinum	k1gNnSc1
1999	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
60-84	60-84	k4
</s>
<s>
Žáček	Žáček	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
:	:	kIx,
Všední	všednět	k5eAaImIp3nS
den	den	k1gInSc4
represivního	represivní	k2eAgInSc2d1
aparátu	aparát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Za	za	k7c4
svobodu	svoboda	k1gFnSc4
a	a	k8xC
demokracii	demokracie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpor	odpor	k1gInSc1
proti	proti	k7c3
komunistické	komunistický	k2eAgFnSc3d1
moci	moc	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Karolinum	Karolinum	k1gNnSc1
1999	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
156-166	156-166	k4
</s>
<s>
Engelmann	Engelmann	k1gMnSc1
<g/>
,	,	kIx,
Roger	Roger	k1gMnSc1
<g/>
:	:	kIx,
Funkční	funkční	k2eAgFnSc1d1
proměna	proměna	k1gFnSc1
Státní	státní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soudobé	soudobý	k2eAgFnPc4d1
dějiny	dějiny	k1gFnPc4
7	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
122-137	122-137	k4
</s>
<s>
Žáček	Žáček	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
:	:	kIx,
Přísně	přísně	k6eAd1
tajné	tajný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Státní	státní	k2eAgFnSc1d1
bezpečnost	bezpečnost	k1gFnSc1
za	za	k7c2
normalizace	normalizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vybrané	vybraný	k2eAgFnPc1d1
směrnice	směrnice	k1gFnPc1
a	a	k8xC
metodické	metodický	k2eAgInPc1d1
pokyny	pokyn	k1gInPc1
politické	politický	k2eAgFnSc2d1
policie	policie	k1gFnSc2
z	z	k7c2
let	léto	k1gNnPc2
1978	#num#	k4
<g/>
-	-	kIx~
<g/>
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Votobia	Votobia	k1gFnSc1
2001	#num#	k4
</s>
<s>
Bašta	Bašta	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
:	:	kIx,
Kontrarozvědná	kontrarozvědný	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
StB	StB	k1gMnPc2
proti	proti	k7c3
vnějšímu	vnější	k2eAgMnSc3d1
nepříteli	nepřítel	k1gMnSc3
v	v	k7c6
diplomových	diplomový	k2eAgFnPc6d1
pracích	práce	k1gFnPc6
absolventů	absolvent	k1gMnPc2
VŠ	vš	k0
SNB	SNB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Securitas	Securitasa	k1gFnPc2
imperii	imperie	k1gFnSc4
9	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
58-257	58-257	k4
</s>
<s>
Frolík	Frolík	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
:	:	kIx,
Ještě	ještě	k6eAd1
k	k	k7c3
nástinu	nástin	k1gInSc3
organizačního	organizační	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
státobezpečnostních	státobezpečnostní	k2eAgFnPc2d1
složek	složka	k1gFnPc2
Sboru	sbor	k1gInSc2
národní	národní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
v	v	k7c6
letech	léto	k1gNnPc6
1948	#num#	k4
<g/>
-	-	kIx~
<g/>
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sborník	sborník	k1gInSc1
archivních	archivní	k2eAgFnPc2d1
prací	práce	k1gFnPc2
52	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
371-520	371-520	k4
</s>
<s>
Povolný	Povolný	k1gMnSc1
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
<g/>
:	:	kIx,
Operativní	operativní	k2eAgFnSc1d1
technika	technika	k1gFnSc1
a	a	k8xC
StB	StB	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Securitas	Securitasa	k1gFnPc2
imperii	imperie	k1gFnSc4
10	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
73-145	73-145	k4
</s>
<s>
Křen	křen	k1gInSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
:	:	kIx,
Dokumenty	dokument	k1gInPc1
StB	StB	k1gFnSc2
jako	jako	k8xS,k8xC
pramen	pramen	k1gInSc4
poznání	poznání	k1gNnSc2
minulosti	minulost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soudobé	soudobý	k2eAgFnPc4d1
dějiny	dějiny	k1gFnPc4
12	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
3	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
708-733	708-733	k4
</s>
<s>
kolektiv	kolektiv	k1gInSc1
autorů	autor	k1gMnPc2
<g/>
:	:	kIx,
Oči	oko	k1gNnPc1
a	a	k8xC
uši	ucho	k1gNnPc1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sedm	sedm	k4xCc4
pohledů	pohled	k1gInPc2
do	do	k7c2
života	život	k1gInSc2
StB	StB	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šenov	Šenov	k1gInSc1
u	u	k7c2
Ostravy	Ostrava	k1gFnSc2
<g/>
,	,	kIx,
Tillia	Tillia	k1gFnSc1
2005	#num#	k4
</s>
<s>
Urbánek	Urbánek	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
:	:	kIx,
Správa	správa	k1gFnSc1
sledování	sledování	k1gNnSc2
Ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
v	v	k7c6
letech	léto	k1gNnPc6
1948	#num#	k4
<g/>
-	-	kIx~
<g/>
89	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Stručný	stručný	k2eAgInSc1d1
nástin	nástin	k1gInSc1
organizačního	organizační	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sborník	sborník	k1gInSc1
Archivu	archiv	k1gInSc2
Ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
3	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
173-232	173-232	k4
</s>
<s>
Koutek	koutek	k1gInSc1
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
:	:	kIx,
X.	X.	kA
správa	správa	k1gFnSc1
SNB	SNB	kA
-	-	kIx~
útvar	útvar	k1gInSc1
Státní	státní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
určený	určený	k2eAgInSc1d1
pro	pro	k7c4
boj	boj	k1gInSc4
proti	proti	k7c3
tzv.	tzv.	kA
vnitřnímu	vnitřní	k2eAgInSc3d1
nepříteli	nepřítel	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Securitas	Securitasa	k1gFnPc2
imperii	imperie	k1gFnSc4
14	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
354-410	354-410	k4
</s>
<s>
Pacner	Pacner	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
:	:	kIx,
Československo	Československo	k1gNnSc1
ve	v	k7c6
zvláštních	zvláštní	k2eAgFnPc6d1
službách	služba	k1gFnPc6
<g/>
:	:	kIx,
pohledy	pohled	k1gInPc7
do	do	k7c2
historie	historie	k1gFnSc2
československých	československý	k2eAgFnPc2d1
výzvědných	výzvědný	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
1914	#num#	k4
<g/>
-	-	kIx~
<g/>
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Themis	Themis	k1gFnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
-	-	kIx~
<g/>
2002	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Pečenka	pečenka	k1gFnSc1
<g/>
,	,	kIx,
Marek	Marek	k1gMnSc1
<g/>
:	:	kIx,
Encyklopedie	encyklopedie	k1gFnSc1
špionáže	špionáž	k1gFnSc2
<g/>
:	:	kIx,
ze	z	k7c2
zákulisí	zákulisí	k1gNnSc2
tajných	tajný	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
Státní	státní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
,	,	kIx,
430	#num#	k4
s.	s.	k?
Dostupná	dostupný	k2eAgFnSc1d1
také	také	k6eAd1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Sobek	Sobek	k1gMnSc1
<g/>
,	,	kIx,
Věroslav	Věroslav	k1gMnSc1
<g/>
:	:	kIx,
Pomohli	pomoct	k5eAaPmAgMnP
jsme	být	k5eAaImIp1nP
sestřelit	sestřelit	k5eAaPmF
Powerse	Powerse	k1gFnPc4
<g/>
:	:	kIx,
tajemství	tajemství	k1gNnSc4
Vědeckotechnické	vědeckotechnický	k2eAgFnSc2d1
rozvědky	rozvědka	k1gFnSc2
ČSSR	ČSSR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Futura	futurum	k1gNnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
162	#num#	k4
s.	s.	k?
</s>
<s>
Bárta	Bárta	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
;	;	kIx,
Kalous	Kalous	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
;	;	kIx,
Povolný	Povolný	k1gMnSc1
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
<g/>
;	;	kIx,
Sivoš	Sivoš	k1gMnSc1
<g/>
,	,	kIx,
Jerguš	Jerguš	k1gMnSc1
<g/>
;	;	kIx,
Žáček	Žáček	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
:	:	kIx,
Biografický	biografický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
náčelníků	náčelník	k1gMnPc2
operativních	operativní	k2eAgFnPc2d1
správ	správa	k1gFnPc2
Státní	státní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
v	v	k7c6
letech	léto	k1gNnPc6
1953	#num#	k4
<g/>
-	-	kIx~
<g/>
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
2690	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
:	:	kIx,
Odboj	odboj	k1gInSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
provokace	provokace	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Provokace	provokace	k1gFnSc1
StB	StB	k1gFnSc2
prováděné	prováděný	k2eAgFnSc2d1
proti	proti	k7c3
třetímu	třetí	k4xOgInSc3
odboji	odboj	k1gInSc3
v	v	k7c6
rámci	rámec	k1gInSc6
akce	akce	k1gFnSc2
"	"	kIx"
<g/>
Skaut	skaut	k1gMnSc1
<g/>
"	"	kIx"
na	na	k7c6
příkladu	příklad	k1gInSc6
skupiny	skupina	k1gFnSc2
"	"	kIx"
<g/>
Za	za	k7c4
svobodu	svoboda	k1gFnSc4
<g/>
"	"	kIx"
v	v	k7c6
letech	léto	k1gNnPc6
1948	#num#	k4
<g/>
-	-	kIx~
<g/>
1951	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-80-200-3084-9	978-80-200-3084-9	k4
</s>
<s>
Petrilák	Petrilák	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
,	,	kIx,
Longaretti	Longaretť	k1gFnPc1
Kraenski	Kraenski	k1gNnSc1
<g/>
,	,	kIx,
Mauro	Mauro	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rudá	rudý	k2eAgFnSc1d1
samba	samba	k1gFnSc1
<g/>
:	:	kIx,
Činnost	činnost	k1gFnSc1
StB	StB	k1gFnSc2
v	v	k7c6
Brazílii	Brazílie	k1gFnSc6
v	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-80-200-3088-7	978-80-200-3088-7	k4
</s>
<s>
Tomek	Tomek	k1gMnSc1
<g/>
,	,	kIx,
Prokop	Prokop	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Estébáckou	estébácký	k2eAgFnSc7d1
Prahou	Praha	k1gFnSc7
<g/>
:	:	kIx,
Průvodce	průvodce	k1gMnSc1
po	po	k7c6
pražských	pražský	k2eAgNnPc6d1
sídlech	sídlo	k1gNnPc6
Státní	státní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhé	druhý	k4xOgFnPc4
<g/>
,	,	kIx,
rozšířené	rozšířený	k2eAgNnSc4d1
vydání	vydání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-80-200-3098-6	978-80-200-3098-6	k4
</s>
<s>
Kudrna	Kudrna	k1gMnSc1
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
<g/>
,	,	kIx,
Stárek	Stárek	k1gMnSc1
Čuňas	Čuňas	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kniha	kniha	k1gFnSc1
v	v	k7c6
barvě	barva	k1gFnSc6
krve	krev	k1gFnSc2
<g/>
:	:	kIx,
Násilí	násilí	k1gNnSc2
komunistického	komunistický	k2eAgInSc2d1
režimu	režim	k1gInSc2
vůči	vůči	k7c3
undergroundu	underground	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
Ústav	ústav	k1gInSc1
pro	pro	k7c4
studium	studium	k1gNnSc4
totalitních	totalitní	k2eAgInPc2d1
režimů	režim	k1gInPc2
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
3207	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Domeček	domeček	k1gInSc1
</s>
<s>
Tajná	tajný	k2eAgFnSc1d1
policie	policie	k1gFnSc1
</s>
<s>
KGB	KGB	kA
</s>
<s>
Stasi	stase	k1gFnSc4
</s>
<s>
Zpravodajská	zpravodajský	k2eAgFnSc1d1
správa	správa	k1gFnSc1
Generálního	generální	k2eAgInSc2d1
štábu	štáb	k1gInSc2
ČSLA	ČSLA	kA
</s>
<s>
Vojenská	vojenský	k2eAgFnSc1d1
kontrarozvědka	kontrarozvědka	k1gFnSc1
MV	MV	kA
ČSSR	ČSSR	kA
</s>
<s>
Pohraniční	pohraniční	k2eAgFnSc1d1
stráž	stráž	k1gFnSc1
</s>
<s>
Úřad	úřad	k1gInSc1
dokumentace	dokumentace	k1gFnSc2
a	a	k8xC
vyšetřování	vyšetřování	k1gNnSc2
zločinů	zločin	k1gInPc2
komunismu	komunismus	k1gInSc2
</s>
<s>
Ústav	ústav	k1gInSc1
pro	pro	k7c4
studium	studium	k1gNnSc4
totalitních	totalitní	k2eAgInPc2d1
režimů	režim	k1gInPc2
</s>
<s>
Úřad	úřad	k1gInSc1
pro	pro	k7c4
zpřístupňování	zpřístupňování	k1gNnSc4
dokumentů	dokument	k1gInPc2
MO	MO	kA
</s>
<s>
Ústav	ústav	k1gInSc1
paměti	paměť	k1gFnSc2
národa	národ	k1gInSc2
</s>
<s>
Otevřená	otevřený	k2eAgFnSc1d1
minulost	minulost	k1gFnSc1
</s>
<s>
Alojz	Alojz	k1gMnSc1
Lorenc	Lorenc	k1gMnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Köcher	Köchra	k1gFnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Státní	státní	k2eAgFnSc1d1
bezpečnost	bezpečnost	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1
heslo	heslo	k1gNnSc4
Státní	státní	k2eAgFnSc1d1
bezpečnost	bezpečnost	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
StB	StB	k1gFnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Téma	téma	k1gNnSc1
Státní	státní	k2eAgFnSc4d1
bezpečnost	bezpečnost	k1gFnSc4
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Státní	státní	k2eAgFnSc1d1
bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Zákon	zákon	k1gInSc1
č.	č.	k?
286	#num#	k4
<g/>
/	/	kIx~
<g/>
1948	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
národní	národní	k2eAgFnSc6d1
bezpečnosti	bezpečnost	k1gFnSc6
na	na	k7c6
webu	web	k1gInSc6
Zákony	zákon	k1gInPc1
pro	pro	k7c4
lidi	člověk	k1gMnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Struktura	struktura	k1gFnSc1
StB	StB	k1gFnSc2
na	na	k7c6
webu	web	k1gInSc6
Policie	policie	k1gFnSc2
ČR	ČR	kA
</s>
<s>
Úřad	úřad	k1gInSc1
vyšetřování	vyšetřování	k1gNnSc2
zločinů	zločin	k1gInPc2
komunismu	komunismus	k1gInSc2
včetně	včetně	k7c2
sborníků	sborník	k1gInPc2
Securitas	Securitas	k1gInSc4
Imperii	imperie	k1gFnSc3
a	a	k8xC
archivu	archiv	k1gInSc3
</s>
<s>
Archiv	archiv	k1gInSc1
bezpečnostních	bezpečnostní	k2eAgFnPc2d1
složek	složka	k1gFnPc2
</s>
<s>
Vyhledávání	vyhledávání	k1gNnSc1
v	v	k7c6
registračních	registrační	k2eAgInPc6d1
a	a	k8xC
archivních	archivní	k2eAgInPc6d1
protokolech	protokol	k1gInPc6
svazků	svazek	k1gInPc2
StB	StB	k1gMnPc1
</s>
<s>
Státní	státní	k2eAgFnSc1d1
bezpečnost	bezpečnost	k1gFnSc1
na	na	k7c6
webu	web	k1gInSc6
iBadatelna	iBadatelna	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Černí	černit	k5eAaImIp3nS
a	a	k8xC
černější	černý	k2eAgNnSc1d2
<g/>
,	,	kIx,
dokument	dokument	k1gInSc1
České	český	k2eAgFnSc2d1
televize	televize	k1gFnSc2
z	z	k7c2
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2007	#num#	k4
o	o	k7c4
pronikání	pronikání	k1gNnSc4
StB	StB	k1gFnSc2
do	do	k7c2
katolické	katolický	k2eAgFnSc2d1
církve	církev	k1gFnSc2
</s>
<s>
Tak	tak	k6eAd1
fungoval	fungovat	k5eAaImAgInS
meč	meč	k1gInSc1
a	a	k8xC
štít	štít	k1gInSc1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
Proč	proč	k6eAd1
stačilo	stačit	k5eAaBmAgNnS
patnáct	patnáct	k4xCc1
tisíc	tisíc	k4xCgInPc2
příslušníků	příslušník	k1gMnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
a	a	k8xC
Vyšetřovací	vyšetřovací	k2eAgFnSc1d1
vazba	vazba	k1gFnSc1
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
v	v	k7c6
provozu	provoz	k1gInSc6
(	(	kIx(
<g/>
rozhovor	rozhovor	k1gInSc1
s	s	k7c7
Pavlem	Pavel	k1gMnSc7
Žáčkem	Žáček	k1gMnSc7
<g/>
)	)	kIx)
na	na	k7c6
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
2	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2008	#num#	k4
</s>
<s>
Rub	rub	k1gInSc1
a	a	k8xC
líc	líc	k1gInSc1
nejen	nejen	k6eAd1
Svobodné	svobodný	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
pořad	pořad	k1gInSc1
Českého	český	k2eAgInSc2d1
rozhlasu	rozhlas	k1gInSc2
Plus	plus	k1gInSc1
z	z	k7c2
25	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
2014	#num#	k4
věnovaný	věnovaný	k2eAgInSc1d1
osobnosti	osobnost	k1gFnSc2
redaktora	redaktor	k1gMnSc4
Josefa	Josef	k1gMnSc4
Pejskara	Pejskar	k1gMnSc4
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
sledování	sledování	k1gNnSc1
Státní	státní	k2eAgNnSc1d1
bezpečností	bezpečnost	k1gFnSc7
</s>
<s>
Zpráva	zpráva	k1gFnSc1
o	o	k7c6
organizované	organizovaný	k2eAgFnSc6d1
normalizaci	normalizace	k1gFnSc6
<g/>
,	,	kIx,
pořad	pořad	k1gInSc1
Českého	český	k2eAgInSc2d1
rozhlasu	rozhlas	k1gInSc2
Vltava	Vltava	k1gFnSc1
z	z	k7c2
25	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2017	#num#	k4
o	o	k7c6
formách	forma	k1gFnPc6
odposlechu	odposlech	k1gInSc2
a	a	k8xC
sledování	sledování	k1gNnSc1
spisovatelů	spisovatel	k1gMnPc2
v	v	k7c6
polovině	polovina	k1gFnSc6
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
rozhovoru	rozhovor	k1gInSc2
s	s	k7c7
historikem	historik	k1gMnSc7
o	o	k7c4
práci	práce	k1gFnSc4
Státní	státní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20020322223	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
124802577	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Komunistický	komunistický	k2eAgInSc1d1
režim	režim	k1gInSc1
v	v	k7c6
Československu	Československo	k1gNnSc6
</s>
