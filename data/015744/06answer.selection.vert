<s>
Státní	státní	k2eAgFnSc1d1
bezpečnost	bezpečnost	k1gFnSc1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
StB	StB	k1gFnSc1
<g/>
,	,	kIx,
hovorově	hovorově	k6eAd1
estébáci	estébáci	k?
<g/>
,	,	kIx,
slovensky	slovensky	k6eAd1
Štátna	Štátna	k1gFnSc1
bezpečnosť	bezpečnostit	k5eAaPmRp2nS
<g/>
,	,	kIx,
ŠtB	ŠtB	k1gMnSc5
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
československá	československý	k2eAgFnSc1d1
politická	politický	k2eAgFnSc1d1
policie	policie	k1gFnSc1
<g/>
,	,	kIx,
zpravodajská	zpravodajský	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
ihned	ihned	k6eAd1
od	od	k7c2
svého	svůj	k3xOyFgInSc2
vzniku	vznik	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
pod	pod	k7c7
kontrolou	kontrola	k1gFnSc7
KSČ	KSČ	kA
<g/>
,	,	kIx,
díky	díky	k7c3
obsazení	obsazení	k1gNnSc3
Ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
ministrem	ministr	k1gMnSc7
Václavem	Václav	k1gMnSc7
Noskem	Nosek	k1gMnSc7
<g/>
,	,	kIx,
a	a	k8xC
sloužila	sloužit	k5eAaImAgFnS
k	k	k7c3
ovládnutí	ovládnutí	k1gNnSc3
mocenských	mocenský	k2eAgFnPc2d1
pozic	pozice	k1gFnPc2
a	a	k8xC
likvidaci	likvidace	k1gFnSc4
protivníků	protivník	k1gMnPc2
komunistického	komunistický	k2eAgInSc2d1
režimu	režim	k1gInSc2
<g/>
.	.	kIx.
</s>