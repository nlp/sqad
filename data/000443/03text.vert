<s>
Svátek	svátek	k1gInSc4	svátek
svatého	svatý	k2eAgMnSc2d1	svatý
Valentýna	Valentýn	k1gMnSc2	Valentýn
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
také	také	k9	také
Valentýn	Valentýn	k1gMnSc1	Valentýn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nP	slavit
v	v	k7c6	v
anglosaských	anglosaský	k2eAgFnPc6d1	anglosaská
zemích	zem	k1gFnPc6	zem
každoročně	každoročně	k6eAd1	každoročně
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
jako	jako	k8xS	jako
svátek	svátek	k1gInSc4	svátek
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
náklonnosti	náklonnost	k1gFnSc2	náklonnost
mezi	mezi	k7c7	mezi
intimními	intimní	k2eAgMnPc7d1	intimní
partnery	partner	k1gMnPc7	partner
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
posílají	posílat	k5eAaImIp3nP	posílat
dárky	dárek	k1gInPc4	dárek
<g/>
,	,	kIx,	,
květiny	květina	k1gFnPc4	květina
<g/>
,	,	kIx,	,
cukrovinky	cukrovinka	k1gFnPc4	cukrovinka
a	a	k8xC	a
pohlednice	pohlednice	k1gFnPc4	pohlednice
s	s	k7c7	s
tematikou	tematika	k1gFnSc7	tematika
stylizovaného	stylizovaný	k2eAgNnSc2d1	stylizované
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
symbolu	symbol	k1gInSc3	symbol
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
svátek	svátek	k1gInSc1	svátek
šíří	šířit	k5eAaImIp3nS	šířit
i	i	k9	i
v	v	k7c6	v
kontinentální	kontinentální	k2eAgFnSc6d1	kontinentální
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
z	z	k7c2	z
komerčních	komerční	k2eAgInPc2d1	komerční
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Svátek	svátek	k1gInSc4	svátek
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
svátku	svátek	k1gInSc2	svátek
Lupercalia	Lupercalium	k1gNnSc2	Lupercalium
slaveném	slavený	k2eAgInSc6d1	slavený
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
tohoto	tento	k3xDgInSc2	tento
dne	den	k1gInSc2	den
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
"	"	kIx"	"
<g/>
urny	urna	k1gFnSc2	urna
lásky	láska	k1gFnSc2	láska
<g/>
"	"	kIx"	"
vloženy	vložen	k2eAgInPc1d1	vložen
lístečky	lísteček	k1gInPc1	lísteček
se	s	k7c7	s
jmény	jméno	k1gNnPc7	jméno
dívek	dívka	k1gFnPc2	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
mladý	mladý	k2eAgMnSc1d1	mladý
muž	muž	k1gMnSc1	muž
potom	potom	k8xC	potom
tahal	tahat	k5eAaImAgInS	tahat
lísteček	lísteček	k1gInSc1	lísteček
a	a	k8xC	a
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc4	jejíž
jméno	jméno	k1gNnSc4	jméno
si	se	k3xPyFc3	se
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
stát	stát	k5eAaPmF	stát
jeho	jeho	k3xOp3gFnSc4	jeho
"	"	kIx"	"
<g/>
miláčkem	miláček	k1gMnSc7	miláček
<g/>
"	"	kIx"	"
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Legenda	legenda	k1gFnSc1	legenda
také	také	k9	také
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
den	den	k1gInSc1	den
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xC	jako
Den	den	k1gInSc1	den
svatého	svatý	k2eAgMnSc2d1	svatý
Valentýna	Valentýn	k1gMnSc2	Valentýn
až	až	k9	až
díky	díky	k7c3	díky
knězi	kněz	k1gMnSc3	kněz
Valentýnovi	Valentýn	k1gMnSc3	Valentýn
<g/>
.	.	kIx.	.
</s>
<s>
Claudius	Claudius	k1gMnSc1	Claudius
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
vládce	vládce	k1gMnSc4	vládce
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
zakazoval	zakazovat	k5eAaImAgMnS	zakazovat
svým	svůj	k3xOyFgMnPc3	svůj
vojákům	voják	k1gMnPc3	voják
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
ženili	ženit	k5eAaImAgMnP	ženit
nebo	nebo	k8xC	nebo
jen	jen	k6eAd1	jen
zasnubovali	zasnubovat	k5eAaImAgMnP	zasnubovat
<g/>
.	.	kIx.	.
</s>
<s>
Bál	bát	k5eAaImAgInS	bát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
chtěli	chtít	k5eAaImAgMnP	chtít
zůstat	zůstat	k5eAaPmF	zůstat
doma	doma	k6eAd1	doma
u	u	k7c2	u
svých	svůj	k3xOyFgFnPc2	svůj
rodin	rodina	k1gFnPc2	rodina
a	a	k8xC	a
nešli	jít	k5eNaImAgMnP	jít
do	do	k7c2	do
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Valentýn	Valentýn	k1gMnSc1	Valentýn
vzdoroval	vzdorovat	k5eAaImAgMnS	vzdorovat
vládci	vládce	k1gMnSc3	vládce
a	a	k8xC	a
tajně	tajně	k6eAd1	tajně
oddával	oddávat	k5eAaImAgMnS	oddávat
mladé	mladý	k2eAgInPc4d1	mladý
páry	pár	k1gInPc4	pár
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
popraven	popravit	k5eAaPmNgInS	popravit
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
.	.	kIx.	.
</s>
<s>
Svátek	svátek	k1gInSc1	svátek
Luprecalia	Luprecalium	k1gNnSc2	Luprecalium
splynul	splynout	k5eAaPmAgInS	splynout
s	s	k7c7	s
oslavami	oslava	k1gFnPc7	oslava
mučednictví	mučednictví	k1gNnSc2	mučednictví
Svatého	svatý	k2eAgMnSc2d1	svatý
Valentýna	Valentýn	k1gMnSc2	Valentýn
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
romantický	romantický	k2eAgInSc1d1	romantický
svátek	svátek	k1gInSc1	svátek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
slaven	slaven	k2eAgMnSc1d1	slaven
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Valentýnu	Valentýn	k1gMnSc3	Valentýn
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
zvyků	zvyk	k1gInPc2	zvyk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
pomalu	pomalu	k6eAd1	pomalu
pronikají	pronikat	k5eAaImIp3nP	pronikat
i	i	k9	i
do	do	k7c2	do
českého	český	k2eAgNnSc2d1	české
prostředí	prostředí	k1gNnSc2	prostředí
-	-	kIx~	-
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
obchodníků	obchodník	k1gMnPc2	obchodník
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
společnou	společný	k2eAgFnSc4d1	společná
oslavu	oslava	k1gFnSc4	oslava
svátku	svátek	k1gInSc2	svátek
zamilovanými	zamilovaná	k1gFnPc7	zamilovaná
páry	pára	k1gFnSc2	pára
<g/>
,	,	kIx,	,
darování	darování	k1gNnSc1	darování
valentynských	valentynský	k2eAgNnPc2d1	valentynský
přáníček	přáníčko	k1gNnPc2	přáníčko
-	-	kIx~	-
vyznání	vyznání	k1gNnSc4	vyznání
lásky	láska	k1gFnSc2	láska
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Valentýnky	Valentýnka	k1gFnSc2	Valentýnka
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
uzamykání	uzamykání	k1gNnSc1	uzamykání
zámečků	zámeček	k1gInPc2	zámeček
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
původně	původně	k6eAd1	původně
s	s	k7c7	s
Valentýnem	Valentýn	k1gMnSc7	Valentýn
ale	ale	k8xC	ale
téměř	téměř	k6eAd1	téměř
nesouvisí	souviset	k5eNaImIp3nS	souviset
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
bez	bez	k7c2	bez
zajímavosti	zajímavost	k1gFnSc2	zajímavost
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
období	období	k1gNnSc2	období
Valentýna	Valentýn	k1gMnSc2	Valentýn
každoročně	každoročně	k6eAd1	každoročně
zvedá	zvedat	k5eAaImIp3nS	zvedat
poptávku	poptávka	k1gFnSc4	poptávka
po	po	k7c6	po
visacích	visací	k2eAgInPc6d1	visací
zámcích	zámek	k1gInPc6	zámek
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yQnSc4	což
reagují	reagovat	k5eAaBmIp3nP	reagovat
také	také	k9	také
výrobci	výrobce	k1gMnPc1	výrobce
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nabízí	nabízet	k5eAaImIp3nP	nabízet
visací	visací	k2eAgInPc4d1	visací
zámečky	zámeček	k1gInPc4	zámeček
s	s	k7c7	s
valentýnskými	valentýnský	k2eAgInPc7d1	valentýnský
motivy	motiv	k1gInPc7	motiv
<g/>
.	.	kIx.	.
</s>
