<p>
<s>
Smrt	smrt	k1gFnSc1	smrt
obchodního	obchodní	k2eAgMnSc2d1	obchodní
cestujícího	cestující	k1gMnSc2	cestující
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Death	Death	k1gInSc1	Death
of	of	k?	of
a	a	k8xC	a
Salesman	Salesman	k1gMnSc1	Salesman
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
drama	drama	k1gNnSc4	drama
o	o	k7c6	o
dvou	dva	k4xCgNnPc6	dva
dějstvích	dějství	k1gNnPc6	dějství
od	od	k7c2	od
Arthura	Arthur	k1gMnSc2	Arthur
Millera	Miller	k1gMnSc2	Miller
a	a	k8xC	a
premiéra	premiér	k1gMnSc2	premiér
hry	hra	k1gFnSc2	hra
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
byla	být	k5eAaImAgFnS	být
oceněna	ocenit	k5eAaPmNgFnS	ocenit
Pulitzerovou	Pulitzerův	k2eAgFnSc7d1	Pulitzerova
cenou	cena	k1gFnSc7	cena
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Luba	Luba	k1gMnSc1	Luba
a	a	k8xC	a
Rudolf	Rudolf	k1gMnSc1	Rudolf
Pellarovi	Pellar	k1gMnSc3	Pellar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
se	se	k3xPyFc4	se
střídá	střídat	k5eAaImIp3nS	střídat
několik	několik	k4yIc1	několik
časových	časový	k2eAgFnPc2d1	časová
linií	linie	k1gFnPc2	linie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
proplétají	proplétat	k5eAaImIp3nP	proplétat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgNnPc4	první
dějství	dějství	k1gNnPc4	dějství
===	===	k?	===
</s>
</p>
<p>
<s>
Willy	Willa	k1gFnPc1	Willa
Loman	Lomany	k1gInPc2	Lomany
se	se	k3xPyFc4	se
navrací	navracet	k5eAaImIp3nS	navracet
domů	dům	k1gInPc2	dům
do	do	k7c2	do
Brooklynu	Brooklyn	k1gInSc2	Brooklyn
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
znechucený	znechucený	k2eAgMnSc1d1	znechucený
z	z	k7c2	z
neúspěšných	úspěšný	k2eNgFnPc2d1	neúspěšná
obchodních	obchodní	k2eAgFnPc2d1	obchodní
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Setkává	setkávat	k5eAaImIp3nS	setkávat
se	se	k3xPyFc4	se
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Lindou	Linda	k1gFnSc7	Linda
a	a	k8xC	a
svými	svůj	k3xOyFgMnPc7	svůj
dospělými	dospělý	k2eAgMnPc7d1	dospělý
syny	syn	k1gMnPc7	syn
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starší	k1gMnSc1	starší
<g/>
,	,	kIx,	,
34	[number]	k4	34
<g/>
letý	letý	k2eAgInSc1d1	letý
Biff	Biff	k1gInSc1	Biff
se	se	k3xPyFc4	se
právě	právě	k6eAd1	právě
vrátil	vrátit	k5eAaPmAgMnS	vrátit
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
nemá	mít	k5eNaImIp3nS	mít
trvalé	trvalý	k2eAgNnSc4d1	trvalé
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
<g/>
,	,	kIx,	,
neustále	neustále	k6eAd1	neustále
střídá	střídat	k5eAaImIp3nS	střídat
bydliště	bydliště	k1gNnSc4	bydliště
<g/>
,	,	kIx,	,
netouží	toužit	k5eNaImIp3nP	toužit
po	po	k7c6	po
kariéře	kariéra	k1gFnSc6	kariéra
obchodníka	obchodník	k1gMnSc2	obchodník
<g/>
.	.	kIx.	.
</s>
<s>
Rád	rád	k2eAgMnSc1d1	rád
by	by	kYmCp3nS	by
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
farmě	farma	k1gFnSc6	farma
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Happy	Happa	k1gFnPc1	Happa
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc1	jeho
32	[number]	k4	32
<g/>
letý	letý	k2eAgMnSc1d1	letý
mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Biffa	Biff	k1gMnSc2	Biff
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
byt	byt	k1gInSc4	byt
a	a	k8xC	a
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
být	být	k5eAaImF	být
sice	sice	k8xC	sice
spokojenější	spokojený	k2eAgMnSc1d2	spokojenější
než	než	k8xS	než
Biff	Biff	k1gInSc1	Biff
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
sní	snít	k5eAaImIp3nS	snít
o	o	k7c6	o
lepší	dobrý	k2eAgFnSc6d2	lepší
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Biff	Biff	k1gMnSc1	Biff
není	být	k5eNaImIp3nS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
střídá	střídat	k5eAaImIp3nS	střídat
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
děje	děj	k1gInSc2	děj
jsou	být	k5eAaImIp3nP	být
vloženy	vložen	k2eAgFnPc1d1	vložena
"	"	kIx"	"
<g/>
flashbacky	flashbacka	k1gFnPc1	flashbacka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
náhledy	náhled	k1gInPc4	náhled
do	do	k7c2	do
Willyho	Willy	k1gMnSc2	Willy
minulosti	minulost	k1gFnSc2	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Biff	Biff	k1gInSc1	Biff
dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgInS	být
významným	významný	k2eAgMnSc7d1	významný
hráčem	hráč	k1gMnSc7	hráč
ragby	ragby	k1gNnSc2	ragby
a	a	k8xC	a
Willy	Willa	k1gFnSc2	Willa
na	na	k7c4	na
něj	on	k3xPp3gInSc4	on
hrdý	hrdý	k2eAgInSc4d1	hrdý
<g/>
.	.	kIx.	.
</s>
<s>
Přehlížel	přehlížet	k5eAaImAgInS	přehlížet
tedy	tedy	k9	tedy
Biffovy	Biffův	k2eAgInPc4d1	Biffův
špatné	špatný	k2eAgInPc4d1	špatný
výsledky	výsledek	k1gInPc4	výsledek
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
i	i	k8xC	i
arogantní	arogantní	k2eAgNnSc1d1	arogantní
jednání	jednání	k1gNnSc1	jednání
se	s	k7c7	s
spolužáky	spolužák	k1gMnPc7	spolužák
i	i	k8xC	i
bratrancem	bratranec	k1gMnSc7	bratranec
Bernardem	Bernard	k1gMnSc7	Bernard
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
právník	právník	k1gMnSc1	právník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Biff	Biff	k1gInSc1	Biff
neuspěl	uspět	k5eNaPmAgInS	uspět
u	u	k7c2	u
závěrečných	závěrečný	k2eAgFnPc2d1	závěrečná
zkoušek	zkouška	k1gFnPc2	zkouška
<g/>
,	,	kIx,	,
odjel	odjet	k5eAaPmAgMnS	odjet
za	za	k7c7	za
otcem	otec	k1gMnSc7	otec
do	do	k7c2	do
Bostonu	Boston	k1gInSc2	Boston
<g/>
,	,	kIx,	,
a	a	k8xC	a
zastihl	zastihnout	k5eAaPmAgInS	zastihnout
ho	on	k3xPp3gMnSc4	on
s	s	k7c7	s
milenkou	milenka	k1gFnSc7	milenka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
příhodě	příhoda	k1gFnSc6	příhoda
přestal	přestat	k5eAaPmAgMnS	přestat
Biff	Biff	k1gMnSc1	Biff
Willyho	Willy	k1gMnSc4	Willy
obdivovat	obdivovat	k5eAaImF	obdivovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Druhé	druhý	k4xOgNnSc1	druhý
dějství	dějství	k1gNnSc1	dějství
===	===	k?	===
</s>
</p>
<p>
<s>
Willy	Willa	k1gMnSc2	Willa
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
už	už	k6eAd1	už
svým	svůj	k3xOyFgNnSc7	svůj
cestováním	cestování	k1gNnSc7	cestování
a	a	k8xC	a
prodejem	prodej	k1gInSc7	prodej
téměř	téměř	k6eAd1	téměř
nevydělává	vydělávat	k5eNaImIp3nS	vydělávat
<g/>
,	,	kIx,	,
požádá	požádat	k5eAaPmIp3nS	požádat
ředitele	ředitel	k1gMnSc2	ředitel
Howarda	Howard	k1gMnSc4	Howard
Wagnera	Wagner	k1gMnSc2	Wagner
o	o	k7c4	o
jinou	jiný	k2eAgFnSc4d1	jiná
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
ho	on	k3xPp3gNnSc4	on
ale	ale	k9	ale
propustí	propustit	k5eAaPmIp3nS	propustit
<g/>
.	.	kIx.	.
</s>
<s>
Willy	Willa	k1gFnPc4	Willa
navštíví	navštívit	k5eAaPmIp3nS	navštívit
svého	svůj	k3xOyFgMnSc2	svůj
souseda	soused	k1gMnSc2	soused
Charleyho	Charley	k1gMnSc2	Charley
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
<g/>
,	,	kIx,	,
Willy	Will	k1gInPc4	Will
z	z	k7c2	z
hrdosti	hrdost	k1gFnSc2	hrdost
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
Biff	Biff	k1gMnSc1	Biff
chtěl	chtít	k5eAaImAgMnS	chtít
rozjet	rozjet	k5eAaPmF	rozjet
velký	velký	k2eAgInSc4d1	velký
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
se	se	k3xPyFc4	se
schází	scházet	k5eAaImIp3nS	scházet
Biff	Biff	k1gInSc1	Biff
<g/>
,	,	kIx,	,
Happy	Happ	k1gInPc1	Happ
a	a	k8xC	a
Willy	Will	k1gInPc1	Will
na	na	k7c4	na
večeři	večeře	k1gFnSc4	večeře
a	a	k8xC	a
vše	všechen	k3xTgNnSc4	všechen
oslavit	oslavit	k5eAaPmF	oslavit
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
bratři	bratr	k1gMnPc1	bratr
se	se	k3xPyFc4	se
pohádají	pohádat	k5eAaPmIp3nP	pohádat
<g/>
.	.	kIx.	.
</s>
<s>
Happy	Happa	k1gFnSc2	Happa
se	se	k3xPyFc4	se
seznámí	seznámit	k5eAaPmIp3nS	seznámit
s	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
krásnými	krásný	k2eAgFnPc7d1	krásná
dívkami	dívka	k1gFnPc7	dívka
a	a	k8xC	a
s	s	k7c7	s
Biffem	Biff	k1gInSc7	Biff
odcházejí	odcházet	k5eAaImIp3nP	odcházet
<g/>
,	,	kIx,	,
otce	otec	k1gMnSc4	otec
opouštějí	opouštět	k5eAaImIp3nP	opouštět
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
vracejí	vracet	k5eAaImIp3nP	vracet
oba	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
Linda	Linda	k1gFnSc1	Linda
je	být	k5eAaImIp3nS	být
nešťastná	šťastný	k2eNgFnSc1d1	nešťastná
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
odešli	odejít	k5eAaPmAgMnP	odejít
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jejich	jejich	k3xOp3gMnSc1	jejich
otec	otec	k1gMnSc1	otec
blouznil	blouznit	k5eAaImAgMnS	blouznit
v	v	k7c6	v
restauraci	restaurace	k1gFnSc6	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Nechce	chtít	k5eNaImIp3nS	chtít
je	on	k3xPp3gNnSc4	on
vidět	vidět	k5eAaImF	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Biff	Biff	k1gMnSc1	Biff
však	však	k9	však
si	se	k3xPyFc3	se
chce	chtít	k5eAaImIp3nS	chtít
promluvit	promluvit	k5eAaPmF	promluvit
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
<g/>
.	.	kIx.	.
</s>
<s>
Willy	Willa	k1gFnPc1	Willa
sadí	sadit	k5eAaPmIp3nP	sadit
na	na	k7c6	na
dvorku	dvorek	k1gInSc6	dvorek
zeleninu	zelenina	k1gFnSc4	zelenina
a	a	k8xC	a
opět	opět	k6eAd1	opět
blouzní	blouznit	k5eAaImIp3nS	blouznit
<g/>
.	.	kIx.	.
</s>
<s>
Biff	Biff	k1gInSc1	Biff
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
omluvit	omluvit	k5eAaPmF	omluvit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Willy	Will	k1gInPc1	Will
je	být	k5eAaImIp3nS	být
rozzuřen	rozzuřen	k2eAgInSc1d1	rozzuřen
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
hádají	hádat	k5eAaImIp3nP	hádat
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
když	když	k8xS	když
Biff	Biff	k1gMnSc1	Biff
začíná	začínat	k5eAaImIp3nS	začínat
plakat	plakat	k5eAaImF	plakat
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Willy	Willa	k1gFnSc2	Willa
nabude	nabýt	k5eAaPmIp3nS	nabýt
pocitu	pocit	k1gInSc2	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
Biff	Biff	k1gInSc1	Biff
má	mít	k5eAaImIp3nS	mít
přece	přece	k9	přece
jen	jen	k9	jen
rád	rád	k2eAgMnSc1d1	rád
<g/>
.	.	kIx.	.
</s>
<s>
Biff	Biff	k1gInSc1	Biff
se	se	k3xPyFc4	se
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Willy	Willa	k1gFnPc4	Willa
chce	chtít	k5eAaImIp3nS	chtít
spáchat	spáchat	k5eAaPmF	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
rodina	rodina	k1gFnSc1	rodina
získala	získat	k5eAaPmAgFnS	získat
dvacet	dvacet	k4xCc4	dvacet
tisíc	tisíc	k4xCgInPc2	tisíc
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
pojistky	pojistka	k1gFnSc2	pojistka
<g/>
.	.	kIx.	.
</s>
<s>
Zjeví	zjevit	k5eAaPmIp3nS	zjevit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
Ben	Ben	k1gInSc1	Ben
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mu	on	k3xPp3gMnSc3	on
sebevraždu	sebevražda	k1gFnSc4	sebevražda
schválí	schválit	k5eAaPmIp3nS	schválit
<g/>
.	.	kIx.	.
</s>
<s>
Willy	Willa	k1gFnPc4	Willa
spáchá	spáchat	k5eAaPmIp3nS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
v	v	k7c6	v
autě	auto	k1gNnSc6	auto
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
končí	končit	k5eAaImIp3nS	končit
Willyho	Willy	k1gMnSc4	Willy
pohřbem	pohřeb	k1gInSc7	pohřeb
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
přichází	přicházet	k5eAaImIp3nS	přicházet
jen	jen	k9	jen
nejbližší	blízký	k2eAgMnPc1d3	nejbližší
příbuzní	příbuzný	k1gMnPc1	příbuzný
<g/>
,	,	kIx,	,
Willyho	Willyha	k1gFnSc5	Willyha
zákazníci	zákazník	k1gMnPc1	zákazník
a	a	k8xC	a
přátelé	přítel	k1gMnPc1	přítel
nepřicházejí	přicházet	k5eNaImIp3nP	přicházet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmové	filmový	k2eAgFnSc2d1	filmová
adaptace	adaptace	k1gFnSc2	adaptace
==	==	k?	==
</s>
</p>
<p>
<s>
1951	[number]	k4	1951
Smrt	smrt	k1gFnSc1	smrt
obchodního	obchodní	k2eAgMnSc2d1	obchodní
cestujícího	cestující	k1gMnSc2	cestující
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
oceněn	ocenit	k5eAaPmNgInS	ocenit
čtyřmi	čtyři	k4xCgInPc7	čtyři
Zlatými	zlatý	k2eAgInPc7d1	zlatý
glóby	glóbus	k1gInPc7	glóbus
(	(	kIx(	(
<g/>
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
drama	drama	k1gNnSc1	drama
<g/>
)	)	kIx)	)
–	–	k?	–
Fredric	Fredric	k1gMnSc1	Fredric
March	March	k1gMnSc1	March
<g/>
,	,	kIx,	,
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
režie	režie	k1gFnSc1	režie
–	–	k?	–
László	László	k1gFnSc2	László
Benedek	Benedka	k1gFnPc2	Benedka
<g/>
,	,	kIx,	,
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
černobílá	černobílý	k2eAgFnSc1d1	černobílá
kamera	kamera	k1gFnSc1	kamera
–	–	k?	–
Franz	Franz	k1gMnSc1	Franz
Planer	Planer	k1gMnSc1	Planer
<g/>
,	,	kIx,	,
Objev	objev	k1gInSc1	objev
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
herec	herec	k1gMnSc1	herec
<g/>
)	)	kIx)	)
–	–	k?	–
Kevin	Kevin	k1gMnSc1	Kevin
McCarthy	McCartha	k1gFnSc2	McCartha
<g/>
)	)	kIx)	)
a	a	k8xC	a
Zlatým	zlatý	k2eAgMnSc7d1	zlatý
lvem	lev	k1gMnSc7	lev
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
László	László	k1gFnSc1	László
Benedek	Benedka	k1gFnPc2	Benedka
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Fredric	Fredric	k1gMnSc1	Fredric
March	March	k1gMnSc1	March
<g/>
,	,	kIx,	,
Mildred	Mildred	k1gMnSc1	Mildred
Dunnock	Dunnock	k1gMnSc1	Dunnock
</s>
</p>
<p>
<s>
1966	[number]	k4	1966
Smrt	smrt	k1gFnSc1	smrt
obchodního	obchodní	k2eAgMnSc2d1	obchodní
cestujícího	cestující	k1gMnSc2	cestující
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
americký	americký	k2eAgMnSc1d1	americký
TV	TV	kA	TV
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Alex	Alex	k1gMnSc1	Alex
Segal	Segal	k1gMnSc1	Segal
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
George	George	k1gFnSc1	George
Segal	Segal	k1gInSc1	Segal
<g/>
,	,	kIx,	,
Gene	gen	k1gInSc5	gen
Wilder	Wildra	k1gFnPc2	Wildra
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
Smrt	smrt	k1gFnSc1	smrt
obchodního	obchodní	k2eAgMnSc2d1	obchodní
cestujícího	cestující	k1gMnSc2	cestující
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
americko	americko	k6eAd1	americko
německý	německý	k2eAgInSc1d1	německý
TV	TV	kA	TV
film	film	k1gInSc1	film
oceněn	ocenit	k5eAaPmNgInS	ocenit
Zlatým	zlatý	k2eAgInSc7d1	zlatý
glóbem	glóbus	k1gInSc7	glóbus
(	(	kIx(	(
<g/>
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
herec	herec	k1gMnSc1	herec
v	v	k7c6	v
TV	TV	kA	TV
filmu	film	k1gInSc2	film
–	–	k?	–
Dustin	Dustin	k1gMnSc1	Dustin
Hoffman	Hoffman	k1gMnSc1	Hoffman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Volker	Volker	k1gMnSc1	Volker
Schlöndorff	Schlöndorff	k1gMnSc1	Schlöndorff
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Dustin	Dustin	k2eAgMnSc1d1	Dustin
Hoffman	Hoffman	k1gMnSc1	Hoffman
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Malkovich	Malkovich	k1gMnSc1	Malkovich
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
Smrt	smrt	k1gFnSc1	smrt
obchodního	obchodní	k2eAgMnSc2d1	obchodní
cestujícího	cestující	k1gMnSc2	cestující
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
americký	americký	k2eAgMnSc1d1	americký
TV	TV	kA	TV
film	film	k1gInSc4	film
oceněn	oceněn	k2eAgInSc1d1	oceněn
Zlatým	zlatý	k2eAgInSc7d1	zlatý
glóbem	glóbus	k1gInSc7	glóbus
(	(	kIx(	(
<g/>
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
herec	herec	k1gMnSc1	herec
v	v	k7c6	v
TV	TV	kA	TV
filmu	film	k1gInSc2	film
–	–	k?	–
Brian	Brian	k1gMnSc1	Brian
Dennehy	Denneha	k1gFnSc2	Denneha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Kirk	Kirk	k1gInSc1	Kirk
Browning	browning	k1gInSc1	browning
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Brian	Brian	k1gInSc1	Brian
Dennehy	Denneha	k1gFnSc2	Denneha
<g/>
,	,	kIx,	,
Ron	Ron	k1gMnSc1	Ron
Eldard	Eldard	k1gMnSc1	Eldard
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Smrt	smrt	k1gFnSc1	smrt
obchodního	obchodní	k2eAgMnSc2d1	obchodní
cestujícího	cestující	k1gMnSc2	cestující
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
</s>
</p>
