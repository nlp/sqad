<s>
Rohatec	rohatec	k1gMnSc1
(	(	kIx(
<g/>
rod	rod	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c6
rodu	rod	k1gInSc6
Glaucium	Glaucium	k1gNnSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
makovitých	makovití	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možná	možná	k6eAd1
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
rohatec	rohatec	k1gMnSc1
(	(	kIx(
<g/>
Ceratopteris	Ceratopteris	k1gFnSc1
<g/>
)	)	kIx)
-	-	kIx~
dřívější	dřívější	k2eAgInSc1d1
název	název	k1gInSc1
vodní	vodní	k2eAgFnSc2d1
kapradiny	kapradina	k1gFnSc2
obšírák	obšírák	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Rohatec	rohatec	k1gMnSc1
Rohatec	rohatec	k1gMnSc1
žlutý	žlutý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Glaucium	Glaucium	k1gNnSc1
flavum	flavum	k1gNnSc1
<g/>
)	)	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Plantae	Plantae	k1gFnSc1
<g/>
)	)	kIx)
Podříše	podříše	k1gFnSc1
</s>
<s>
cévnaté	cévnatý	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Tracheobionta	Tracheobionta	k1gFnSc1
<g/>
)	)	kIx)
Oddělení	oddělení	k1gNnSc1
</s>
<s>
krytosemenné	krytosemenný	k2eAgNnSc1d1
(	(	kIx(
<g/>
Magnoliophyta	Magnoliophyta	k1gFnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
vyšší	vysoký	k2eAgFnPc1d2
dvouděložné	dvouděložná	k1gFnPc1
(	(	kIx(
<g/>
Rosopsida	Rosopsida	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
pryskyřníkotvaré	pryskyřníkotvarý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Ranunculales	Ranunculales	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
makovité	makovitý	k2eAgInPc4d1
(	(	kIx(
<g/>
Papaveraceae	Papaveracea	k1gInPc4
<g/>
)	)	kIx)
Rod	rod	k1gInSc4
</s>
<s>
rohatec	rohatec	k1gMnSc1
(	(	kIx(
<g/>
Glaucium	Glaucium	k1gNnSc1
<g/>
)	)	kIx)
<g/>
Mill	Mill	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1913	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rohatec	rohatec	k1gMnSc1
růžkatý	růžkatý	k2eAgMnSc1d1
na	na	k7c4
ilustraci	ilustrace	k1gFnSc4
z	z	k7c2
roku	rok	k1gInSc2
1885	#num#	k4
</s>
<s>
Rohatec	rohatec	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
Glaucium	Glaucium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
rod	rod	k1gInSc4
rostlin	rostlina	k1gFnPc2
z	z	k7c2
čeledi	čeleď	k1gFnSc2
makovité	makovitý	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
jednoleté	jednoletý	k2eAgFnPc1d1
až	až	k8xS
vytrvalé	vytrvalý	k2eAgFnPc1d1
<g/>
,	,	kIx,
ojíněné	ojíněný	k2eAgFnPc1d1
byliny	bylina	k1gFnPc1
s	s	k7c7
tuhými	tuhý	k2eAgInPc7d1
listy	list	k1gInPc7
a	a	k8xC
nápadnými	nápadný	k2eAgInPc7d1
žlutými	žlutý	k2eAgInPc7d1
<g/>
,	,	kIx,
oranžovými	oranžový	k2eAgInPc7d1
nebo	nebo	k8xC
červenými	červený	k2eAgInPc7d1
květy	květ	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rod	rod	k1gInSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
asi	asi	k9
25	#num#	k4
druhů	druh	k1gInPc2
a	a	k8xC
je	být	k5eAaImIp3nS
rozšířen	rozšířit	k5eAaPmNgInS
od	od	k7c2
Kanárských	kanárský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
po	po	k7c6
Střední	střední	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
a	a	k8xC
severozápadní	severozápadní	k2eAgFnSc4d1
Čínu	Čína	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
se	se	k3xPyFc4
jako	jako	k9
archeofyt	archeofyt	k1gInSc1
vyskytuje	vyskytovat	k5eAaImIp3nS
rohatec	rohatec	k1gMnSc1
růžkatý	růžkatý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc1
druhy	druh	k1gInPc1
jsou	být	k5eAaImIp3nP
pěstovány	pěstovat	k5eAaImNgInP
jako	jako	k8xS,k8xC
letničky	letnička	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Rohatce	rohatec	k1gMnPc4
jsou	být	k5eAaImIp3nP
jednoleté	jednoletý	k2eAgFnPc1d1
<g/>
,	,	kIx,
dvouleté	dvouletý	k2eAgFnPc1d1
nebo	nebo	k8xC
vytrvalé	vytrvalý	k2eAgFnPc1d1
<g/>
,	,	kIx,
ojíněné	ojíněný	k2eAgFnPc1d1
byliny	bylina	k1gFnPc1
s	s	k7c7
kožovitými	kožovitý	k2eAgInPc7d1
listy	list	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahují	obsahovat	k5eAaImIp3nP
žlutou	žlutý	k2eAgFnSc4d1
šťávu	šťáva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stonek	stonek	k1gInSc1
je	být	k5eAaImIp3nS
přímý	přímý	k2eAgInSc4d1
nebo	nebo	k8xC
vystoupavý	vystoupavý	k2eAgInSc4d1
<g/>
,	,	kIx,
lysý	lysý	k2eAgInSc4d1
nebo	nebo	k8xC
vlnatý	vlnatý	k2eAgInSc4d1
<g/>
,	,	kIx,
olistěný	olistěný	k2eAgInSc4d1
a	a	k8xC
někdy	někdy	k6eAd1
na	na	k7c6
bázi	báze	k1gFnSc6
dřevnatějící	dřevnatějící	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Listy	list	k1gInPc4
v	v	k7c6
přízemní	přízemní	k2eAgFnSc6d1
růžici	růžice	k1gFnSc6
jsou	být	k5eAaImIp3nP
četné	četný	k2eAgFnPc1d1
<g/>
,	,	kIx,
řapíkaté	řapíkatý	k2eAgFnPc1d1
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
se	se	k3xPyFc4
zpeřeně	zpeřeně	k6eAd1
členěnou	členěný	k2eAgFnSc7d1
čepelí	čepel	k1gFnSc7
<g/>
,	,	kIx,
na	na	k7c6
okraji	okraj	k1gInSc6
zubaté	zubatý	k2eAgFnSc2d1
nebo	nebo	k8xC
vroubkované	vroubkovaný	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lodyžní	lodyžní	k2eAgInPc1d1
listy	list	k1gInPc1
jsou	být	k5eAaImIp3nP
přisedlé	přisedlý	k2eAgInPc1d1
<g/>
,	,	kIx,
střídavé	střídavý	k2eAgInPc1d1
<g/>
,	,	kIx,
střelovitě	střelovitě	k6eAd1
srdčitou	srdčitý	k2eAgFnSc7d1
bází	báze	k1gFnSc7
objímavé	objímavý	k2eAgFnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Květy	Květa	k1gFnPc1
jsou	být	k5eAaImIp3nP
velké	velký	k2eAgFnPc1d1
<g/>
,	,	kIx,
jednotlivé	jednotlivý	k2eAgFnPc1d1
<g/>
,	,	kIx,
úžlabní	úžlabní	k2eAgFnPc1d1
nebo	nebo	k8xC
vrcholové	vrcholový	k2eAgFnPc1d1
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
dlouze	dlouho	k6eAd1
stopkaté	stopkatý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kalich	kalich	k1gInSc1
je	být	k5eAaImIp3nS
opadavý	opadavý	k2eAgInSc1d1
<g/>
,	,	kIx,
tvořený	tvořený	k2eAgInSc1d1
2	#num#	k4
volnými	volný	k2eAgInPc7d1
plátky	plátek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koruna	koruna	k1gFnSc1
je	být	k5eAaImIp3nS
žlutá	žlutý	k2eAgFnSc1d1
<g/>
,	,	kIx,
oranžová	oranžový	k2eAgFnSc1d1
nebo	nebo	k8xC
červená	červený	k2eAgFnSc1d1
<g/>
,	,	kIx,
složená	složený	k2eAgFnSc1d1
ze	z	k7c2
4	#num#	k4
plátků	plátek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyčinek	tyčinka	k1gFnPc2
je	být	k5eAaImIp3nS
mnoho	mnoho	k4c1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Semeník	semeník	k1gInSc1
je	být	k5eAaImIp3nS
srostlý	srostlý	k2eAgMnSc1d1
ze	z	k7c2
2	#num#	k4
plodolistů	plodolist	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
2	#num#	k4
komůrky	komůrka	k1gFnSc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
přepážka	přepážka	k1gFnSc1
vzniká	vznikat	k5eAaImIp3nS
druhotně	druhotně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blizna	blizna	k1gFnSc1
je	být	k5eAaImIp3nS
téměř	téměř	k6eAd1
přisedlá	přisedlý	k2eAgFnSc1d1
<g/>
,	,	kIx,
dvoulaločná	dvoulaločný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plodem	plod	k1gInSc7
je	být	k5eAaImIp3nS
úzká	úzký	k2eAgFnSc1d1
<g/>
,	,	kIx,
nezploštělá	zploštělý	k2eNgFnSc1d1
tobolka	tobolka	k1gFnSc1
pukající	pukající	k2eAgFnSc1d1
od	od	k7c2
vrcholu	vrchol	k1gInSc2
2	#num#	k4
chlopněmi	chlopeň	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
mnoho	mnoho	k4c1
tmavě	tmavě	k6eAd1
hnědých	hnědý	k2eAgNnPc2d1
semen	semeno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Semena	semeno	k1gNnPc1
nemají	mít	k5eNaImIp3nP
míšek	míšek	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rohatec	rohatec	k1gMnSc1
žlutý	žlutý	k2eAgMnSc1d1
<g/>
,	,	kIx,
forma	forma	k1gFnSc1
aurantiacum	aurantiacum	k1gInSc1
</s>
<s>
Květ	květ	k1gInSc1
rohatce	rohatec	k1gMnSc2
Glaucium	Glaucium	k1gNnSc4
oxylobum	oxylobum	k1gNnSc5
</s>
<s>
Suché	Suché	k2eAgFnSc2d1
tobolky	tobolka	k1gFnSc2
rohatce	rohatec	k1gMnSc2
žlutého	žlutý	k2eAgMnSc2d1
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
</s>
<s>
Rod	rod	k1gInSc1
rohatec	rohatec	k1gMnSc1
obsahuje	obsahovat	k5eAaImIp3nS
asi	asi	k9
25	#num#	k4
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
pocházejících	pocházející	k2eAgFnPc2d1
z	z	k7c2
Evropy	Evropa	k1gFnSc2
a	a	k8xC
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
se	se	k3xPyFc4
v	v	k7c6
nejteplejších	teplý	k2eAgFnPc6d3
oblastech	oblast	k1gFnPc6
vyskytuje	vyskytovat	k5eAaImIp3nS
jako	jako	k9
archeofyt	archeofyt	k1gInSc1
rohatec	rohatec	k1gMnSc1
růžkatý	růžkatý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Glaucium	Glaucium	k1gNnSc1
corniculatum	corniculatum	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzácně	vzácně	k6eAd1
zplaňuje	zplaňovat	k5eAaImIp3nS
pěstovaný	pěstovaný	k2eAgMnSc1d1
rohatec	rohatec	k1gMnSc1
žlutý	žlutý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Glaucium	Glaucium	k1gNnSc1
flavum	flavum	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
tyto	tento	k3xDgInPc1
druhy	druh	k1gInPc1
jsou	být	k5eAaImIp3nP
rozšířeny	rozšířit	k5eAaPmNgInP
zejména	zejména	k9
ve	v	k7c6
Středomoří	středomoří	k1gNnSc6
a	a	k8xC
jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
jediní	jediný	k2eAgMnPc1d1
evropští	evropský	k2eAgMnPc1d1
zástupci	zástupce	k1gMnPc1
rodu	rod	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Areál	areál	k1gInSc1
přirozeného	přirozený	k2eAgNnSc2d1
rozšíření	rozšíření	k1gNnSc2
rodu	rod	k1gInSc2
rohatec	rohatec	k1gMnSc1
sahá	sahat	k5eAaImIp3nS
od	od	k7c2
Kanárských	kanárský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
po	po	k7c6
Střední	střední	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
a	a	k8xC
severozápadní	severozápadní	k2eAgFnSc4d1
Čínu	Čína	k1gFnSc4
(	(	kIx(
<g/>
Sin-ťiang	Sin-ťiang	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zástupci	zástupce	k1gMnPc1
</s>
<s>
rohatec	rohatec	k1gMnSc1
růžkatý	růžkatý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Glaucium	Glaucium	k1gNnSc1
corniculatum	corniculatum	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
rohatec	rohatec	k1gMnSc1
žlutý	žlutý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Glaucium	Glaucium	k1gNnSc1
flavum	flavum	k1gNnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Význam	význam	k1gInSc1
</s>
<s>
Některé	některý	k3yIgInPc1
druhy	druh	k1gInPc1
jsou	být	k5eAaImIp3nP
pěstovány	pěstován	k2eAgFnPc4d1
jako	jako	k8xC,k8xS
okrasné	okrasný	k2eAgFnPc4d1
letničky	letnička	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
SLAVÍK	Slavík	k1gMnSc1
<g/>
,	,	kIx,
Bohumil	Bohumil	k1gMnSc1
(	(	kIx(
<g/>
editor	editor	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Květena	květena	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
1	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
643	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SKALICKÁ	Skalická	k1gFnSc1
<g/>
,	,	kIx,
Anna	Anna	k1gFnSc1
<g/>
;	;	kIx,
VĚTVIČKA	větvička	k1gFnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
;	;	kIx,
ZELENÝ	Zelený	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Botanický	botanický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
rodových	rodový	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
cévnatých	cévnatý	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Aventinum	Aventinum	k1gNnSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7442	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
31	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KIGER	KIGER	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
W.	W.	kA
Flora	Flora	k1gMnSc1
of	of	k?
North	North	k1gMnSc1
America	America	k1gMnSc1
<g/>
:	:	kIx,
Glaucium	Glaucium	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
ZHANG	ZHANG	kA
<g/>
,	,	kIx,
Mingli	Mingle	k1gFnSc4
<g/>
;	;	kIx,
GREY-WILSON	GREY-WILSON	k1gMnPc2
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flora	Flora	k1gFnSc1
of	of	k?
China	China	k1gFnSc1
<g/>
:	:	kIx,
Glaucium	Glaucium	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Flora	Flora	k1gFnSc1
Europaea	Europaea	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Royal	Royal	k1gInSc1
Botanic	Botanice	k1gFnPc2
Garden	Gardno	k1gNnPc2
Edinburgh	Edinburgh	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
rohatec	rohatec	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc4
Glaucium	Glaucium	k1gNnSc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Rostliny	rostlina	k1gFnPc1
</s>
