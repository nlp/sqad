<s>
Platónská	platónský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
</s>
<s>
Starověké	starověký	k2eAgFnPc1d1
Athény	Athéna	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akademie	akademie	k1gFnSc1
je	být	k5eAaImIp3nS
při	při	k7c6
horním	horní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
mapy	mapa	k1gFnSc2
</s>
<s>
Vykopávky	vykopávka	k1gFnPc1
v	v	k7c4
Akadimia	Akadimium	k1gNnPc4
Platonos	Platonosa	k1gFnPc2
v	v	k7c6
Athénách	Athéna	k1gFnPc6
</s>
<s>
Platónova	Platónův	k2eAgFnSc1d1
Akademie	akademie	k1gFnSc1
na	na	k7c6
mozaice	mozaika	k1gFnSc6
z	z	k7c2
Pompejí	Pompeje	k1gFnPc2
</s>
<s>
Platónská	platónský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
(	(	kIx(
<g/>
řec.	řec.	k?
Ά	Ά	k?
<g/>
,	,	kIx,
lat.	lat.	k?
Academia	academia	k1gFnSc1
Platonica	Platonica	k1gFnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
slavná	slavný	k2eAgFnSc1d1
starověká	starověký	k2eAgFnSc1d1
filosofická	filosofický	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
,	,	kIx,
založená	založený	k2eAgFnSc1d1
Platónem	platón	k1gInSc7
asi	asi	k9
roku	rok	k1gInSc2
387	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
v	v	k7c6
Athénách	Athéna	k1gFnPc6
a	a	k8xC
definitivně	definitivně	k6eAd1
zrušená	zrušený	k2eAgFnSc1d1
roku	rok	k1gInSc2
529	#num#	k4
císařem	císař	k1gMnSc7
Justiniánem	Justinián	k1gMnSc7
I.	I.	kA
Název	název	k1gInSc4
později	pozdě	k6eAd2
převzala	převzít	k5eAaPmAgFnS
skupina	skupina	k1gFnSc1
humanistů	humanista	k1gMnPc2
ve	v	k7c6
Florencii	Florencie	k1gFnSc6
<g/>
,	,	kIx,
založená	založený	k2eAgFnSc1d1
kolem	kolem	k7c2
roku	rok	k1gInSc2
1450	#num#	k4
Marsiliem	Marsilium	k1gNnSc7
Ficinem	Ficin	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
athénské	athénský	k2eAgFnSc2d1
Akademie	akademie	k1gFnSc2
se	se	k3xPyFc4
odvozují	odvozovat	k5eAaImIp3nP
také	také	k9
názvy	název	k1gInPc1
různých	různý	k2eAgFnPc2d1
učených	učený	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
<g/>
,	,	kIx,
škol	škola	k1gFnPc2
a	a	k8xC
vědeckých	vědecký	k2eAgFnPc2d1
institucí	instituce	k1gFnPc2
v	v	k7c6
novověku	novověk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Akademie	akademie	k1gFnSc1
v	v	k7c6
Athénách	Athéna	k1gFnPc6
</s>
<s>
Někdy	někdy	k6eAd1
po	po	k7c6
roce	rok	k1gInSc6
387	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
Platón	platón	k1gInSc1
vrátil	vrátit	k5eAaPmAgInS
z	z	k7c2
první	první	k4xOgFnSc2
cesty	cesta	k1gFnSc2
na	na	k7c6
Sicílii	Sicílie	k1gFnSc6
<g/>
,	,	kIx,
koupil	koupit	k5eAaPmAgMnS
z	z	k7c2
darů	dar	k1gInPc2
přátel	přítel	k1gMnPc2
pozemek	pozemka	k1gFnPc2
asi	asi	k9
2,5	2,5	k4
km	km	kA
na	na	k7c4
SSZ	SSZ	kA
od	od	k7c2
středu	střed	k1gInSc2
historických	historický	k2eAgFnPc2d1
Athén	Athéna	k1gFnPc2
a	a	k8xC
asi	asi	k9
1,5	1,5	k4
km	km	kA
od	od	k7c2
hradeb	hradba	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
založil	založit	k5eAaPmAgMnS
školu	škola	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
blízkosti	blízkost	k1gFnSc6
byl	být	k5eAaImAgInS
starý	starý	k2eAgInSc1d1
posvátný	posvátný	k2eAgInSc1d1
háj	háj	k1gInSc1
olivovníků	olivovník	k1gInPc2
<g/>
,	,	kIx,
zasvěcený	zasvěcený	k2eAgMnSc1d1
Athéně	Athéna	k1gFnSc3
a	a	k8xC
Dioskurům	Dioskur	k1gMnPc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
konaly	konat	k5eAaImAgFnP
různé	různý	k2eAgFnPc1d1
slavnosti	slavnost	k1gFnPc1
a	a	k8xC
obřady	obřad	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
mytologického	mytologický	k2eAgMnSc2d1
hrdiny	hrdina	k1gMnSc2
Akadéma	Akadém	k1gMnSc2
(	(	kIx(
<g/>
Hekadéma	Hekadéma	k1gFnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
nazýval	nazývat	k5eAaImAgMnS
Akademeia	Akademeia	k1gFnSc1
a	a	k8xC
název	název	k1gInSc1
se	se	k3xPyFc4
přenesl	přenést	k5eAaPmAgInS
na	na	k7c4
Platónovu	Platónův	k2eAgFnSc4d1
školu	škola	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Nebyla	být	k5eNaImAgFnS
to	ten	k3xDgNnSc1
ovšem	ovšem	k9
škola	škola	k1gFnSc1
v	v	k7c6
dnešním	dnešní	k2eAgInSc6d1
slova	slovo	k1gNnSc2
smyslu	smysl	k1gInSc6
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
neveřejné	veřejný	k2eNgNnSc4d1
společenství	společenství	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
diskutovalo	diskutovat	k5eAaImAgNnS
a	a	k8xC
bádalo	bádat	k5eAaImAgNnS
<g/>
,	,	kIx,
snad	snad	k9
podle	podle	k7c2
Pythagorejského	pythagorejský	k2eAgInSc2d1
vzoru	vzor	k1gInSc2
<g/>
,	,	kIx,
s	s	k7c7
nímž	jenž	k3xRgMnSc7
se	se	k3xPyFc4
Platón	Platón	k1gMnSc1
setkal	setkat	k5eAaPmAgMnS
v	v	k7c6
Itálii	Itálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
účastníky	účastník	k1gMnPc4
se	se	k3xPyFc4
rozlišovali	rozlišovat	k5eAaImAgMnP
„	„	k?
<g/>
starší	starší	k1gMnSc1
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
mladší	mladý	k2eAgFnSc2d2
<g/>
“	“	k?
<g/>
,	,	kIx,
za	za	k7c4
účast	účast	k1gFnSc4
se	se	k3xPyFc4
neplatilo	platit	k5eNaImAgNnS
a	a	k8xC
velký	velký	k2eAgInSc1d1
důraz	důraz	k1gInSc1
se	se	k3xPyFc4
kladl	klást	k5eAaImAgInS
na	na	k7c4
znalost	znalost	k1gFnSc4
matematiky	matematika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
pozdější	pozdní	k2eAgFnSc2d2
pověsti	pověst	k1gFnSc2
byl	být	k5eAaImAgInS
prý	prý	k9
nad	nad	k7c7
vchodem	vchod	k1gInSc7
nápis	nápis	k1gInSc1
„	„	k?
<g/>
Bez	bez	k7c2
znalosti	znalost	k1gFnSc2
geometrie	geometrie	k1gFnSc2
sem	sem	k6eAd1
nikdo	nikdo	k3yNnSc1
nevstupuj	vstupovat	k5eNaImRp2nS
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diogenés	Diogenés	k1gInSc1
Laertios	Laertios	k1gInSc4
jmenuje	jmenovat	k5eAaBmIp3nS,k5eAaImIp3nS
mezi	mezi	k7c7
akademiky	akademik	k1gMnPc7
i	i	k8xC
dvě	dva	k4xCgFnPc1
ženy	žena	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
athénské	athénský	k2eAgFnSc2d1
Akademie	akademie	k1gFnSc2
</s>
<s>
Diogenés	Diogenés	k6eAd1
Laertios	Laertios	k1gInSc1
dělí	dělit	k5eAaImIp3nS
historii	historie	k1gFnSc4
Akademie	akademie	k1gFnSc2
na	na	k7c4
starou	starý	k2eAgFnSc4d1
<g/>
,	,	kIx,
střední	střední	k2eAgFnSc4d1
a	a	k8xC
novou	nový	k2eAgFnSc4d1
<g/>
,	,	kIx,
Sextus	Sextus	k1gInSc1
Empiricus	Empiricus	k1gInSc1
rozeznává	rozeznávat	k5eAaImIp3nS
pět	pět	k4xCc4
období	období	k1gNnPc2
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
Cicero	Cicero	k1gMnSc1
jen	jen	k6eAd1
dvě	dva	k4xCgFnPc4
<g/>
:	:	kIx,
starou	starý	k2eAgFnSc7d1
a	a	k8xC
novou	nový	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scholarchy	Scholarcha	k1gFnPc4
staré	starý	k2eAgFnSc2d1
Akademie	akademie	k1gFnSc2
byli	být	k5eAaImAgMnP
po	po	k7c6
Platónovi	Platónův	k2eAgMnPc1d1
Speusippos	Speusipposa	k1gFnPc2
(	(	kIx(
<g/>
347	#num#	k4
<g/>
-	-	kIx~
<g/>
339	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Xenokratés	Xenokratés	k1gInSc1
(	(	kIx(
<g/>
339	#num#	k4
<g/>
-	-	kIx~
<g/>
314	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Polemon	Polemon	k1gInSc1
a	a	k8xC
Kratés	Kratés	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
významné	významný	k2eAgInPc4d1
další	další	k2eAgInPc4d1
členy	člen	k1gInPc4
patřili	patřit	k5eAaImAgMnP
Aristotelés	Aristotelés	k1gInSc4
<g/>
,	,	kIx,
Herakleidés	Herakleidés	k1gInSc4
<g/>
,	,	kIx,
Eudoxos	Eudoxos	k1gInSc4
a	a	k8xC
Krantór	Krantór	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Střední	střední	k2eAgFnSc1d1
Akademie	akademie	k1gFnSc1
začíná	začínat	k5eAaImIp3nS
Arkesiláem	Arkesiláem	k1gInSc1
(	(	kIx(
<g/>
266	#num#	k4
<g/>
-	-	kIx~
<g/>
241	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
za	za	k7c2
něhož	jenž	k3xRgInSc2
se	se	k3xPyFc4
Akademie	akademie	k1gFnSc1
orientovala	orientovat	k5eAaBmAgFnS
na	na	k7c4
skepticismus	skepticismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novou	Nová	k1gFnSc4
či	či	k8xC
Třetí	třetí	k4xOgFnSc4
Akademii	akademie	k1gFnSc4
založil	založit	k5eAaPmAgMnS
rovněž	rovněž	k9
skeptický	skeptický	k2eAgInSc4d1
Karneadés	Karneadés	k1gInSc4
(	(	kIx(
<g/>
155	#num#	k4
<g/>
-	-	kIx~
<g/>
129	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
90	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
zde	zde	k6eAd1
Antiochos	Antiochos	k1gInSc1
z	z	k7c2
Askalonu	Askalon	k1gInSc2
začal	začít	k5eAaPmAgInS
učit	učit	k5eAaImF
stoicismus	stoicismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
první	první	k4xOgFnSc2
války	válka	k1gFnSc2
s	s	k7c7
králem	král	k1gMnSc7
Mithridatem	Mithridat	k1gMnSc7
Pontským	pontský	k2eAgMnSc7d1
roku	rok	k1gInSc2
88	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
scholarcha	scholarcha	k1gMnSc1
Filón	Filón	k1gMnSc1
z	z	k7c2
Larissy	Larissa	k1gFnSc2
uprchl	uprchnout	k5eAaPmAgMnS
do	do	k7c2
Říma	Řím	k1gInSc2
a	a	k8xC
roku	rok	k1gInSc2
86	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
při	při	k7c6
obléhání	obléhání	k1gNnSc6
Athén	Athéna	k1gFnPc2
místo	místo	k6eAd1
zpustošil	zpustošit	k5eAaPmAgMnS
římský	římský	k2eAgMnSc1d1
vojevůdce	vojevůdce	k1gMnSc1
Lucius	Lucius	k1gMnSc1
Cornelius	Cornelius	k1gMnSc1
Sulla	Sulla	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akademie	akademie	k1gFnSc1
se	se	k3xPyFc4
sice	sice	k8xC
obnovila	obnovit	k5eAaPmAgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
patrně	patrně	k6eAd1
na	na	k7c6
jiných	jiný	k2eAgNnPc6d1
místech	místo	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cicero	Cicero	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
zde	zde	k6eAd1
studoval	studovat	k5eAaImAgMnS
v	v	k7c6
letech	léto	k1gNnPc6
79	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
scházeli	scházet	k5eAaImAgMnP
v	v	k7c6
nějakém	nějaký	k3yIgNnSc6
gymnáziu	gymnázium	k1gNnSc6
a	a	k8xC
místo	místo	k7c2
staré	starý	k2eAgFnSc2d1
Akademie	akademie	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
navštívil	navštívit	k5eAaPmAgMnS
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
pusté	pustý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Platonovo	Platonův	k2eAgNnSc1d1
učení	učení	k1gNnSc1
v	v	k7c6
Athénách	Athéna	k1gFnPc6
pokračovalo	pokračovat	k5eAaImAgNnS
<g/>
,	,	kIx,
ale	ale	k8xC
až	až	k9
kolem	kolem	k7c2
roku	rok	k1gInSc2
410	#num#	k4
byla	být	k5eAaImAgFnS
Akademie	akademie	k1gFnSc1
oficiálně	oficiálně	k6eAd1
obnovena	obnovit	k5eAaPmNgFnS
novoplatoniky	novoplatonik	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
pokládali	pokládat	k5eAaImAgMnP
za	za	k7c4
dědice	dědic	k1gMnPc4
(	(	kIx(
<g/>
diadochoi	diadocho	k1gMnPc7
<g/>
)	)	kIx)
Platónovy	Platónův	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
roku	rok	k1gInSc2
430	#num#	k4
zde	zde	k6eAd1
učili	učít	k5eAaPmAgMnP,k5eAaImAgMnP
Plutarchos	Plutarchos	k1gInSc4
z	z	k7c2
Athén	Athéna	k1gFnPc2
a	a	k8xC
Syrianos	Syrianosa	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gMnSc7
žákem	žák	k1gMnSc7
Proklem	Prokl	k1gInSc7
novoplatónská	novoplatónský	k2eAgFnSc1d1
Akademie	akademie	k1gFnSc1
dosáhla	dosáhnout	k5eAaPmAgFnS
patrně	patrně	k6eAd1
vrcholu	vrchol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
historika	historik	k1gMnSc2
Agathia	Agathius	k1gMnSc2
pocházela	pocházet	k5eAaImAgFnS
většina	většina	k1gFnSc1
čelných	čelný	k2eAgMnPc2d1
akademiků	akademik	k1gMnPc2
ze	z	k7c2
Sýrie	Sýrie	k1gFnSc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
Simplikios	Simplikios	k1gMnSc1
a	a	k8xC
poslední	poslední	k2eAgMnSc1d1
scholarcha	scholarcha	k1gMnSc1
Damaskios	Damaskios	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
roku	rok	k1gInSc2
529	#num#	k4
Justinián	Justinián	k1gMnSc1
I.	I.	kA
Akademii	akademie	k1gFnSc4
zrušil	zrušit	k5eAaPmAgInS
<g/>
,	,	kIx,
uprchl	uprchnout	k5eAaPmAgInS
prý	prý	k9
Damaskios	Damaskios	k1gInSc1
k	k	k7c3
Peršanům	Peršan	k1gMnPc3
<g/>
,	,	kIx,
ale	ale	k8xC
roku	rok	k1gInSc2
532	#num#	k4
se	se	k3xPyFc4
mohl	moct	k5eAaImAgInS
vrátit	vrátit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
Simplikiovi	Simplikiův	k2eAgMnPc1d1
a	a	k8xC
Damaskiovi	Damaskiův	k2eAgMnPc1d1
žáci	žák	k1gMnPc1
však	však	k9
mohli	moct	k5eAaImAgMnP
v	v	k7c6
novoplatónské	novoplatónský	k2eAgFnSc6d1
tradici	tradice	k1gFnSc6
pokračovat	pokračovat	k5eAaImF
<g/>
,	,	kIx,
v	v	k7c6
7	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
v	v	k7c6
Gundišapúru	Gundišapúro	k1gNnSc6
a	a	k8xC
v	v	k7c6
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
stopy	stopa	k1gFnPc1
platoniků	platonik	k1gMnPc2
znovu	znovu	k6eAd1
objevují	objevovat	k5eAaImIp3nP
v	v	k7c6
Bagdádu	Bagdád	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Humanistická	humanistický	k2eAgFnSc1d1
Akademie	akademie	k1gFnSc1
ve	v	k7c6
Florencii	Florencie	k1gFnSc6
</s>
<s>
Kolem	kolem	k7c2
roku	rok	k1gInSc2
1430	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
začali	začít	k5eAaPmAgMnP
Konstantinopol	Konstantinopol	k1gInSc4
bezprostředně	bezprostředně	k6eAd1
ohrožovat	ohrožovat	k5eAaImF
Turci	Turek	k1gMnPc1
<g/>
,	,	kIx,
začal	začít	k5eAaPmAgMnS
císař	císař	k1gMnSc1
i	i	k8xC
tamní	tamní	k2eAgMnPc1d1
učenci	učenec	k1gMnPc1
hledat	hledat	k5eAaImF
kontakty	kontakt	k1gInPc4
v	v	k7c6
Itálii	Itálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1439	#num#	k4
byla	být	k5eAaImAgFnS
na	na	k7c6
koncilu	koncil	k1gInSc6
ve	v	k7c6
Florencii	Florencie	k1gFnSc6
velká	velký	k2eAgFnSc1d1
delegace	delegace	k1gFnSc1
byzantských	byzantský	k2eAgMnPc2d1
učenců	učenec	k1gMnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
pokusila	pokusit	k5eAaPmAgFnS
o	o	k7c6
sjednocení	sjednocení	k1gNnSc6
s	s	k7c7
Římem	Řím	k1gInSc7
a	a	k8xC
s	s	k7c7
papežem	papež	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uzavřená	uzavřený	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
však	však	k9
neměla	mít	k5eNaImAgFnS
dlouhé	dlouhý	k2eAgNnSc4d1
trvání	trvání	k1gNnSc4
a	a	k8xC
roku	rok	k1gInSc2
1453	#num#	k4
Konstantinopol	Konstantinopol	k1gInSc4
padla	padnout	k5eAaPmAgFnS,k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řada	řada	k1gFnSc1
byzantských	byzantský	k2eAgMnPc2d1
učenců	učenec	k1gMnPc2
v	v	k7c6
Itálii	Itálie	k1gFnSc6
zůstala	zůstat	k5eAaPmAgFnS
a	a	k8xC
Georgios	Georgios	k1gMnSc1
Gemistos	Gemistos	k1gMnSc1
Plethon	Plethon	k1gMnSc1
přivezl	přivézt	k5eAaPmAgMnS
dosud	dosud	k6eAd1
neznámé	známý	k2eNgInPc4d1
rukopisy	rukopis	k1gInPc4
díla	dílo	k1gNnSc2
Platónova	Platónův	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
zájem	zájem	k1gInSc1
o	o	k7c4
platonismus	platonismus	k1gInSc4
a	a	k8xC
novoplatonismus	novoplatonismus	k1gInSc4
podporovali	podporovat	k5eAaImAgMnP
florentští	florentský	k2eAgMnPc1d1
Mediceové	Mediceus	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Pod	pod	k7c7
vedením	vedení	k1gNnSc7
Marsilia	Marsilius	k1gMnSc2
Ficina	Ficin	k1gMnSc2
<g/>
,	,	kIx,
vynikajícího	vynikající	k2eAgMnSc2d1
překladatele	překladatel	k1gMnSc2
a	a	k8xC
propagátora	propagátor	k1gMnSc2
Platónovy	Platónův	k2eAgFnSc2d1
filosofie	filosofie	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
tak	tak	k6eAd1
vytvořila	vytvořit	k5eAaPmAgFnS
neformální	formální	k2eNgFnSc1d1
skupina	skupina	k1gFnSc1
učenců	učenec	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
studovali	studovat	k5eAaImAgMnP
a	a	k8xC
diskutovali	diskutovat	k5eAaImAgMnP
o	o	k7c4
filosofii	filosofie	k1gFnSc4
a	a	k8xC
klasických	klasický	k2eAgMnPc6d1
autorech	autor	k1gMnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vliv	vliv	k1gInSc1
jejich	jejich	k3xOp3gNnSc2
modernizovaného	modernizovaný	k2eAgNnSc2d1
a	a	k8xC
křesťansky	křesťansky	k6eAd1
laděného	laděný	k2eAgInSc2d1
platonismu	platonismus	k1gInSc2
na	na	k7c4
myšlení	myšlení	k1gNnSc4
italské	italský	k2eAgFnSc2d1
renesance	renesance	k1gFnSc2
byl	být	k5eAaImAgInS
hluboký	hluboký	k2eAgInSc1d1
a	a	k8xC
dodnes	dodnes	k6eAd1
přežívá	přežívat	k5eAaImIp3nS
v	v	k7c6
obecně	obecně	k6eAd1
známém	známý	k2eAgInSc6d1
pojmu	pojem	k1gInSc6
„	„	k?
<g/>
platonická	platonický	k2eAgFnSc1d1
láska	láska	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotný	samotný	k2eAgInSc1d1
Marsilio	Marsilio	k6eAd1
Ficino	Ficino	k1gNnSc4
nepokládal	pokládat	k5eNaImAgInS
Akademii	akademie	k1gFnSc4
za	za	k7c4
pouhou	pouhý	k2eAgFnSc4d1
školu	škola	k1gFnSc4
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
za	za	k7c4
společenství	společenství	k1gNnSc4
přátel	přítel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Ačkoli	ačkoli	k8xS
tato	tento	k3xDgFnSc1
skupina	skupina	k1gFnSc1
nikdy	nikdy	k6eAd1
neměla	mít	k5eNaImAgFnS
organizovanou	organizovaný	k2eAgFnSc4d1
podobu	podoba	k1gFnSc4
<g/>
,	,	kIx,
její	její	k3xOp3gMnPc1
členové	člen	k1gMnPc1
se	se	k3xPyFc4
pokládali	pokládat	k5eAaImAgMnP
za	za	k7c4
obnovení	obnovení	k1gNnSc4
starověké	starověký	k2eAgFnSc2d1
Akademie	akademie	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
založil	založit	k5eAaPmAgMnS
Platón	Platón	k1gMnSc1
v	v	k7c6
Athénách	Athéna	k1gFnPc6
Členové	člen	k1gMnPc1
Platónské	platónský	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
byli	být	k5eAaImAgMnP
většinou	většinou	k6eAd1
ve	v	k7c6
styku	styk	k1gInSc6
se	s	k7c7
dvorem	dvůr	k1gInSc7
Cosima	Cosim	k1gMnSc2
a	a	k8xC
Lorenza	Lorenza	k?
z	z	k7c2
rodu	rod	k1gInSc2
Medici	medik	k1gMnPc1
a	a	k8xC
mezi	mezi	k7c4
nejdůležitější	důležitý	k2eAgInPc4d3
z	z	k7c2
nich	on	k3xPp3gInPc2
náleželi	náležet	k5eAaImAgMnP
Angelo	Angela	k1gFnSc5
Poliziano	Poliziana	k1gFnSc5
<g/>
,	,	kIx,
význačný	význačný	k2eAgMnSc1d1
básník	básník	k1gMnSc1
a	a	k8xC
znalec	znalec	k1gMnSc1
antiky	antika	k1gFnSc2
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
poezie	poezie	k1gFnSc2
a	a	k8xC
řečnictví	řečnictví	k1gNnSc2
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
ve	v	k7c6
Florencii	Florencie	k1gFnSc6
<g/>
,	,	kIx,
Cristofero	Cristofero	k1gNnSc1
Landino	Landin	k2eAgNnSc1d1
a	a	k8xC
učenci	učenec	k1gMnPc1
a	a	k8xC
filosofové	filosof	k1gMnPc1
Pico	Pico	k6eAd1
della	della	k6eAd1
Mirandola	Mirandola	k1gFnSc1
a	a	k8xC
Gentile	Gentil	k1gMnSc5
de	de	k?
Becchi	Becch	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1
antiky	antika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Akademia	Akademia	k1gFnSc1
1973	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc4
Akadémie	Akadémie	k1gFnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
42	#num#	k4
<g/>
n.	n.	k?
</s>
<s>
HIBBERT	HIBBERT	kA
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Vzestup	vzestup	k1gInSc4
a	a	k8xC
pád	pád	k1gInSc4
rodu	rod	k1gInSc2
Medici	medik	k1gMnPc1
<g/>
,	,	kIx,
kap	kap	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	V	kA
a	a	k8xC
kap	kap	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IX	IX	kA
<g/>
,	,	kIx,
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KRISTELLER	KRISTELLER	kA
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
Oskar	Oskar	k1gMnSc1
<g/>
:	:	kIx,
Osm	osm	k4xCc1
filosofů	filosof	k1gMnPc2
italské	italský	k2eAgFnSc2d1
renesance	renesance	k1gFnSc2
<g/>
,	,	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PICO	PICO	kA
DELLA	DELLA	kA
MIRANDOLA	MIRANDOLA	kA
<g/>
,	,	kIx,
Giovanni	Giovaneň	k1gFnSc6
<g/>
:	:	kIx,
O	o	k7c6
důstojnosti	důstojnost	k1gFnSc6
člověka	člověk	k1gMnSc2
<g/>
,	,	kIx,
Oikoymenh	Oikoymenh	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Platónská	platónský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Akadémie	Akadémie	k1gFnSc2
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1035537052	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
301913189	#num#	k4
</s>
