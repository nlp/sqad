<s>
Platónská	platónský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
(	(	kIx(
<g/>
řec.	řec.	k?
Ά	Ά	k?
<g/>
,	,	kIx,
lat.	lat.	k?
Academia	academia	k1gFnSc1
Platonica	Platonica	k1gFnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
slavná	slavný	k2eAgFnSc1d1
starověká	starověký	k2eAgFnSc1d1
filosofická	filosofický	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
,	,	kIx,
založená	založený	k2eAgFnSc1d1
Platónem	platón	k1gInSc7
asi	asi	k9
roku	rok	k1gInSc2
387	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
v	v	k7c6
Athénách	Athéna	k1gFnPc6
a	a	k8xC
definitivně	definitivně	k6eAd1
zrušená	zrušený	k2eAgFnSc1d1
roku	rok	k1gInSc2
529	#num#	k4
císařem	císař	k1gMnSc7
Justiniánem	Justinián	k1gMnSc7
I.	I.	kA
Název	název	k1gInSc4
později	pozdě	k6eAd2
převzala	převzít	k5eAaPmAgFnS
skupina	skupina	k1gFnSc1
humanistů	humanista	k1gMnPc2
ve	v	k7c6
Florencii	Florencie	k1gFnSc6
<g/>
,	,	kIx,
založená	založený	k2eAgFnSc1d1
kolem	kolem	k7c2
roku	rok	k1gInSc2
1450	#num#	k4
Marsiliem	Marsilium	k1gNnSc7
Ficinem	Ficin	k1gMnSc7
<g/>
.	.	kIx.
</s>