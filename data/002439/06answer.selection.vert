<s>
Konrád	Konrád	k1gMnSc1	Konrád
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
kníže	kníže	k1gMnSc1	kníže
znojemského	znojemský	k2eAgInSc2d1	znojemský
údělu	úděl	k1gInSc2	úděl
Konrád	Konrád	k1gMnSc1	Konrád
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
matkou	matka	k1gFnSc7	matka
Marie	Marie	k1gFnSc1	Marie
Srbská	srbský	k2eAgFnSc1d1	Srbská
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
srbského	srbský	k2eAgMnSc2d1	srbský
župana	župan	k1gMnSc2	župan
Uroše	Uroš	k1gInSc2	Uroš
Bílého	bílý	k2eAgInSc2d1	bílý
<g/>
.	.	kIx.	.
</s>
