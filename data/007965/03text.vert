<s>
Latte	Latte	k5eAaPmIp2nP	Latte
macchiato	macchiato	k6eAd1	macchiato
[	[	kIx(	[
<g/>
late	lat	k1gInSc5	lat
makijáto	makijáto	k1gNnSc4	makijáto
<g/>
]	]	kIx)	]
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
jazyce	jazyk	k1gInSc6	jazyk
znamená	znamenat	k5eAaImIp3nS	znamenat
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
umazané	umazaný	k2eAgInPc4d1	umazaný
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
či	či	k8xC	či
"	"	kIx"	"
<g/>
skvrnité	skvrnitý	k2eAgNnSc1d1	skvrnité
mléko	mléko	k1gNnSc1	mléko
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
horké	horký	k2eAgNnSc1d1	horké
mléko	mléko	k1gNnSc1	mléko
s	s	k7c7	s
vrstvou	vrstva	k1gFnSc7	vrstva
espressa	espressa	k1gFnSc1	espressa
(	(	kIx(	(
<g/>
espresso	espressa	k1gFnSc5	espressa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
doplněné	doplněný	k2eAgInPc1d1	doplněný
mléčnou	mléčný	k2eAgFnSc7d1	mléčná
pěnou	pěna	k1gFnSc7	pěna
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
dávných	dávný	k2eAgFnPc2d1	dávná
dob	doba	k1gFnPc2	doba
jsou	být	k5eAaImIp3nP	být
Italové	Ital	k1gMnPc1	Ital
známí	známý	k1gMnPc1	známý
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
rádi	rád	k2eAgMnPc1d1	rád
kávu	káva	k1gFnSc4	káva
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
se	se	k3xPyFc4	se
každodenní	každodenní	k2eAgInPc1d1	každodenní
dýchánky	dýchánek	k1gInPc1	dýchánek
v	v	k7c6	v
rytmu	rytmus	k1gInSc6	rytmus
praženého	pražený	k2eAgInSc2d1	pražený
nápoje	nápoj	k1gInSc2	nápoj
neobešly	obešt	k5eNaBmAgFnP	obešt
bez	bez	k7c2	bez
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
občas	občas	k6eAd1	občas
byly	být	k5eAaImAgInP	být
přítomné	přítomný	k2eAgInPc1d1	přítomný
i	i	k8xC	i
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
zpravidla	zpravidla	k6eAd1	zpravidla
žadonily	žadonit	k5eAaImAgInP	žadonit
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
i	i	k8xC	i
jim	on	k3xPp3gMnPc3	on
dostalo	dostat	k5eAaPmAgNnS	dostat
kávy	káva	k1gFnSc2	káva
<g/>
,	,	kIx,	,
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
"	"	kIx"	"
<g/>
Latte	Latt	k1gMnSc5	Latt
macchiato	macchiat	k2eAgNnSc1d1	macchiato
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
=	=	kIx~	=
"	"	kIx"	"
<g/>
mléko	mléko	k1gNnSc1	mléko
se	s	k7c7	s
skvrnou	skvrna	k1gFnSc7	skvrna
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
-	-	kIx~	-
příprava	příprava	k1gFnSc1	příprava
byla	být	k5eAaImAgFnS	být
naprosto	naprosto	k6eAd1	naprosto
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
<g/>
,	,	kIx,	,
horké	horký	k2eAgNnSc1d1	horké
mléko	mléko	k1gNnSc1	mléko
bylo	být	k5eAaImAgNnS	být
doplněno	doplnit	k5eAaPmNgNnS	doplnit
o	o	k7c4	o
nepatrné	patrný	k2eNgNnSc4d1	nepatrné
množství	množství	k1gNnSc4	množství
kávy	káva	k1gFnSc2	káva
<g/>
,	,	kIx,	,
jen	jen	k9	jen
pro	pro	k7c4	pro
zabarvení	zabarvení	k1gNnSc4	zabarvení
<g/>
.	.	kIx.	.
</s>
<s>
Spokojenost	spokojenost	k1gFnSc1	spokojenost
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc1	dítě
se	se	k3xPyFc4	se
honosily	honosit	k5eAaImAgFnP	honosit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pijou	pít	k5eAaImIp3nP	pít
kávu	káva	k1gFnSc4	káva
a	a	k8xC	a
rodiče	rodič	k1gMnPc1	rodič
svůj	svůj	k3xOyFgInSc4	svůj
zasloužený	zasloužený	k2eAgInSc4d1	zasloužený
klid	klid	k1gInSc4	klid
k	k	k7c3	k
odpočinku	odpočinek	k1gInSc3	odpočinek
u	u	k7c2	u
svého	svůj	k3xOyFgNnSc2	svůj
praženého	pražený	k2eAgNnSc2d1	pražené
potěšení	potěšení	k1gNnSc2	potěšení
<g/>
.	.	kIx.	.
</s>
<s>
Prvotní	prvotní	k2eAgFnSc1d1	prvotní
vizáž	vizáž	k1gFnSc1	vizáž
však	však	k9	však
byla	být	k5eAaImAgFnS	být
jiná	jiný	k2eAgFnSc1d1	jiná
než	než	k8xS	než
dnes	dnes	k6eAd1	dnes
-	-	kIx~	-
mléko	mléko	k1gNnSc1	mléko
bez	bez	k7c2	bez
mléčné	mléčný	k2eAgFnSc2d1	mléčná
pěny	pěna	k1gFnSc2	pěna
<g/>
,	,	kIx,	,
lehce	lehko	k6eAd1	lehko
zabarvené	zabarvený	k2eAgFnSc2d1	zabarvená
kávou	káva	k1gFnSc7	káva
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Kom	kdo	k3yInSc6	kdo
<g/>
)	)	kIx)	)
Latte	Latt	k1gInSc5	Latt
macchiato	macchiat	k2eAgNnSc4d1	macchiato
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
z	z	k7c2	z
našlehaného	našlehaný	k2eAgNnSc2d1	našlehané
mléka	mléko	k1gNnSc2	mléko
a	a	k8xC	a
espressa	espressa	k1gFnSc1	espressa
<g/>
.	.	kIx.	.
</s>
<s>
Suroviny	surovina	k1gFnPc1	surovina
jsou	být	k5eAaImIp3nP	být
stejné	stejný	k2eAgFnPc1d1	stejná
jako	jako	k8xS	jako
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
cappuccina	cappuccino	k1gNnSc2	cappuccino
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
způsob	způsob	k1gInSc1	způsob
přípravy	příprava	k1gFnSc2	příprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nerezové	rezový	k2eNgFnSc6d1	nerezová
konvičce	konvička	k1gFnSc6	konvička
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
páry	pár	k1gInPc1	pár
našleháme	našlehat	k5eAaPmIp1nP	našlehat
studené	studený	k2eAgNnSc4d1	studené
mléko	mléko	k1gNnSc4	mléko
(	(	kIx(	(
<g/>
cca	cca	kA	cca
0,3	[number]	k4	0,3
litru	litr	k1gInSc2	litr
dle	dle	k7c2	dle
objemu	objem	k1gInSc2	objem
sklenice	sklenice	k1gFnSc2	sklenice
<g/>
)	)	kIx)	)
na	na	k7c4	na
mléčnou	mléčný	k2eAgFnSc4d1	mléčná
pěnu	pěna	k1gFnSc4	pěna
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
mléka	mléko	k1gNnSc2	mléko
po	po	k7c4	po
našlehání	našlehání	k1gNnSc4	našlehání
by	by	kYmCp3nS	by
neměla	mít	k5eNaImAgFnS	mít
přesáhnout	přesáhnout	k5eAaPmF	přesáhnout
70	[number]	k4	70
<g/>
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
jakmile	jakmile	k8xS	jakmile
začne	začít	k5eAaPmIp3nS	začít
být	být	k5eAaImF	být
konvička	konvička	k1gFnSc1	konvička
horká	horký	k2eAgFnSc1d1	horká
na	na	k7c4	na
dotek	dotek	k1gInSc4	dotek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pěna	pěna	k1gFnSc1	pěna
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
jemná	jemný	k2eAgFnSc1d1	jemná
<g/>
,	,	kIx,	,
velké	velký	k2eAgFnPc1d1	velká
bubliny	bublina	k1gFnPc4	bublina
odstraníme	odstranit	k5eAaPmIp1nP	odstranit
poklepáním	poklepání	k1gNnSc7	poklepání
konvičky	konvička	k1gFnSc2	konvička
o	o	k7c4	o
stůl	stůl	k1gInSc4	stůl
<g/>
.	.	kIx.	.
</s>
<s>
Mléko	mléko	k1gNnSc1	mléko
s	s	k7c7	s
pěnou	pěna	k1gFnSc7	pěna
přelijeme	přelít	k5eAaPmIp1nP	přelít
do	do	k7c2	do
sklenice	sklenice	k1gFnSc2	sklenice
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pěna	pěna	k1gFnSc1	pěna
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
kousek	kousek	k1gInSc4	kousek
pod	pod	k7c4	pod
okraj	okraj	k1gInSc4	okraj
sklenice	sklenice	k1gFnSc2	sklenice
-	-	kIx~	-
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
střední	střední	k2eAgFnSc1d1	střední
nebo	nebo	k8xC	nebo
vysoká	vysoký	k2eAgFnSc1d1	vysoká
úzká	úzký	k2eAgFnSc1d1	úzká
sklenice	sklenice	k1gFnSc1	sklenice
(	(	kIx(	(
<g/>
mléko	mléko	k1gNnSc1	mléko
můžeme	moct	k5eAaImIp1nP	moct
pro	pro	k7c4	pro
zjednodušení	zjednodušení	k1gNnSc4	zjednodušení
šlehat	šlehat	k5eAaImF	šlehat
přímo	přímo	k6eAd1	přímo
ve	v	k7c6	v
sklenici	sklenice	k1gFnSc6	sklenice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
musíme	muset	k5eAaImIp1nP	muset
dávat	dávat	k5eAaImF	dávat
větší	veliký	k2eAgInSc4d2	veliký
pozor	pozor	k1gInSc4	pozor
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
mléka	mléko	k1gNnSc2	mléko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
další	další	k2eAgFnSc2d1	další
malé	malý	k2eAgFnSc2d1	malá
nahřáté	nahřátý	k2eAgFnSc2d1	nahřátá
konvičky	konvička	k1gFnSc2	konvička
vyextrahujeme	vyextrahovat	k5eAaPmIp1nP	vyextrahovat
espresso	espressa	k1gFnSc5	espressa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
poté	poté	k6eAd1	poté
pomalu	pomalu	k6eAd1	pomalu
nalijeme	nalít	k5eAaPmIp1nP	nalít
do	do	k7c2	do
sklenice	sklenice	k1gFnSc2	sklenice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sklenici	sklenice	k1gFnSc6	sklenice
se	se	k3xPyFc4	se
vrstvy	vrstva	k1gFnPc1	vrstva
oddělí	oddělit	k5eAaPmIp3nP	oddělit
a	a	k8xC	a
uvidíme	uvidět	k5eAaPmIp1nP	uvidět
dole	dole	k6eAd1	dole
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
ním	on	k3xPp3gMnSc7	on
mléko	mléko	k1gNnSc1	mléko
s	s	k7c7	s
espressem	espress	k1gInSc7	espress
a	a	k8xC	a
nahoře	nahoře	k6eAd1	nahoře
mléčnou	mléčný	k2eAgFnSc4d1	mléčná
pěnu	pěna	k1gFnSc4	pěna
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
přístroj	přístroj	k1gInSc4	přístroj
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
espresso	espressa	k1gFnSc5	espressa
extrahovat	extrahovat	k5eAaBmF	extrahovat
do	do	k7c2	do
sklenice	sklenice	k1gFnSc2	sklenice
přímo	přímo	k6eAd1	přímo
<g/>
.	.	kIx.	.
</s>
<s>
Nalévání	nalévání	k1gNnSc1	nalévání
espressa	espress	k1gMnSc2	espress
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
na	na	k7c6	na
pěně	pěna	k1gFnSc6	pěna
skvrnu	skvrna	k1gFnSc4	skvrna
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
tomuto	tento	k3xDgInSc3	tento
nápoji	nápoj	k1gInSc3	nápoj
říká	říkat	k5eAaImIp3nS	říkat
latte	latte	k5eAaPmIp2nP	latte
machhiato	machhiat	k2eAgNnSc1d1	machhiat
=	=	kIx~	=
skvrnité	skvrnitý	k2eAgNnSc1d1	skvrnité
mléko	mléko	k1gNnSc1	mléko
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
využitím	využití	k1gNnSc7	využití
tyčinky	tyčinka	k1gFnSc2	tyčinka
nebo	nebo	k8xC	nebo
špejle	špejle	k1gFnSc2	špejle
můžeme	moct	k5eAaImIp1nP	moct
dotvořit	dotvořit	k5eAaPmF	dotvořit
skvrnu	skvrna	k1gFnSc4	skvrna
do	do	k7c2	do
zajímavých	zajímavý	k2eAgInPc2d1	zajímavý
tvarů	tvar	k1gInPc2	tvar
<g/>
,	,	kIx,	,
tomuto	tento	k3xDgNnSc3	tento
umění	umění	k1gNnSc3	umění
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
latte	latit	k5eAaPmRp2nP	latit
art	art	k?	art
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
cappuccina	cappuccino	k1gNnSc2	cappuccino
<g/>
,	,	kIx,	,
latte	latte	k5eAaPmIp2nP	latte
machhiato	machhiato	k6eAd1	machhiato
se	se	k3xPyFc4	se
nezdobí	zdobit	k5eNaImIp3nS	zdobit
skořicí	skořice	k1gFnSc7	skořice
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ta	ten	k3xDgFnSc1	ten
přebíjí	přebíjet	k5eAaImIp3nS	přebíjet
chuť	chuť	k1gFnSc4	chuť
kávy	káva	k1gFnSc2	káva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
přání	přání	k1gNnPc1	přání
je	on	k3xPp3gNnSc4	on
latte	latte	k5eAaPmIp2nP	latte
macchiato	macchiato	k6eAd1	macchiato
možné	možný	k2eAgNnSc1d1	možné
dozdobit	dozdobit	k5eAaPmF	dozdobit
kakaovým	kakaový	k2eAgInSc7d1	kakaový
sirupem	sirup	k1gInSc7	sirup
nebo	nebo	k8xC	nebo
kakaem	kakao	k1gNnSc7	kakao
<g/>
.	.	kIx.	.
</s>
<s>
Skořici	skořice	k1gFnSc4	skořice
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
výslovné	výslovný	k2eAgNnSc4d1	výslovné
přání	přání	k1gNnSc4	přání
zákazníka	zákazník	k1gMnSc2	zákazník
<g/>
.	.	kIx.	.
</s>
<s>
Espresso	Espressa	k1gFnSc5	Espressa
Cappuccino	Cappuccina	k1gFnSc5	Cappuccina
-	-	kIx~	-
espresso	espressa	k1gFnSc5	espressa
s	s	k7c7	s
mlékem	mléko	k1gNnSc7	mléko
a	a	k8xC	a
mléčnou	mléčný	k2eAgFnSc7d1	mléčná
pěnou	pěna	k1gFnSc7	pěna
Káva	káva	k1gFnSc1	káva
Barista	Barista	k1gMnSc1	Barista
</s>
