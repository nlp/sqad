<s>
Latte	Latte	k5eAaPmIp2nP	Latte
macchiato	macchiato	k6eAd1	macchiato
[	[	kIx(	[
<g/>
late	lat	k1gInSc5	lat
makijáto	makijáto	k1gNnSc4	makijáto
<g/>
]	]	kIx)	]
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
jazyce	jazyk	k1gInSc6	jazyk
znamená	znamenat	k5eAaImIp3nS	znamenat
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
umazané	umazaný	k2eAgInPc4d1	umazaný
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
či	či	k8xC	či
"	"	kIx"	"
<g/>
skvrnité	skvrnitý	k2eAgNnSc1d1	skvrnité
mléko	mléko	k1gNnSc1	mléko
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
horké	horký	k2eAgNnSc1d1	horké
mléko	mléko	k1gNnSc1	mléko
s	s	k7c7	s
vrstvou	vrstva	k1gFnSc7	vrstva
espressa	espressa	k1gFnSc1	espressa
(	(	kIx(	(
<g/>
espresso	espressa	k1gFnSc5	espressa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
doplněné	doplněný	k2eAgInPc1d1	doplněný
mléčnou	mléčný	k2eAgFnSc7d1	mléčná
pěnou	pěna	k1gFnSc7	pěna
<g/>
.	.	kIx.	.
</s>
