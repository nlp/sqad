<s>
PhDr.	PhDr.	kA	PhDr.
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Horský	Horský	k1gMnSc1	Horský
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1929	[number]	k4	1929
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
-	-	kIx~	-
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1988	[number]	k4	1988
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
astronom	astronom	k1gMnSc1	astronom
a	a	k8xC	a
historik	historik	k1gMnSc1	historik
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
filozofii	filozofie	k1gFnSc4	filozofie
a	a	k8xC	a
hudební	hudební	k2eAgFnSc4d1	hudební
vědu	věda	k1gFnSc4	věda
na	na	k7c6	na
filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
astronomii	astronomie	k1gFnSc4	astronomie
a	a	k8xC	a
matematiku	matematika	k1gFnSc4	matematika
na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
přírodovědecké	přírodovědecký	k2eAgFnSc6d1	Přírodovědecká
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
studiem	studio	k1gNnSc7	studio
historických	historický	k2eAgInPc2d1	historický
vědeckých	vědecký	k2eAgInPc2d1	vědecký
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
humanitních	humanitní	k2eAgFnPc2d1	humanitní
a	a	k8xC	a
exaktních	exaktní	k2eAgFnPc2d1	exaktní
věd	věda	k1gFnPc2	věda
určilo	určit	k5eAaPmAgNnS	určit
celou	celý	k2eAgFnSc4d1	celá
jeho	jeho	k3xOp3gFnSc4	jeho
další	další	k2eAgFnSc4d1	další
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
1956	[number]	k4	1956
<g/>
-	-	kIx~	-
<g/>
1970	[number]	k4	1970
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
Historickém	historický	k2eAgInSc6d1	historický
ústavu	ústav	k1gInSc6	ústav
ČSAV	ČSAV	kA	ČSAV
(	(	kIx(	(
<g/>
Ústav	ústav	k1gInSc1	ústav
čsl	čsl	kA	čsl
<g/>
.	.	kIx.	.
a	a	k8xC	a
světových	světový	k2eAgFnPc2d1	světová
dějin	dějiny	k1gFnPc2	dějiny
ČSAV	ČSAV	kA	ČSAV
<g/>
)	)	kIx)	)
1970	[number]	k4	1970
<g/>
-	-	kIx~	-
<g/>
1973	[number]	k4	1973
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
Astronomické	astronomický	k2eAgFnSc6d1	astronomická
společnosti	společnost	k1gFnSc6	společnost
při	při	k7c6	při
ČSAV	ČSAV	kA	ČSAV
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
byl	být	k5eAaImAgMnS	být
pracovníkem	pracovník	k1gMnSc7	pracovník
Astronomického	astronomický	k2eAgInSc2d1	astronomický
ústavu	ústav	k1gInSc2	ústav
ČSAV	ČSAV	kA	ČSAV
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
se	se	k3xPyFc4	se
soustředil	soustředit	k5eAaPmAgMnS	soustředit
na	na	k7c4	na
dějiny	dějiny	k1gFnPc4	dějiny
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
se	se	k3xPyFc4	se
obracel	obracet	k5eAaImAgMnS	obracet
k	k	k7c3	k
dílu	dílo	k1gNnSc3	dílo
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
Koperníka	Koperník	k1gMnSc2	Koperník
a	a	k8xC	a
k	k	k7c3	k
astronomii	astronomie	k1gFnSc3	astronomie
"	"	kIx"	"
<g/>
rudolfínské	rudolfínský	k2eAgFnSc2d1	rudolfínská
epochy	epocha	k1gFnSc2	epocha
<g/>
"	"	kIx"	"
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
zastoupenou	zastoupený	k2eAgFnSc7d1	zastoupená
osobnostmi	osobnost	k1gFnPc7	osobnost
jako	jako	k9	jako
Johannes	Johannes	k1gMnSc1	Johannes
Kepler	Kepler	k1gMnSc1	Kepler
<g/>
,	,	kIx,	,
Tycho	Tyc	k1gMnSc2	Tyc
Brahe	Brah	k1gMnSc2	Brah
a	a	k8xC	a
Tadeáš	Tadeáš	k1gMnSc1	Tadeáš
Hájek	Hájek	k1gMnSc1	Hájek
z	z	k7c2	z
Hájku	hájek	k1gInSc2	hájek
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
aplikoval	aplikovat	k5eAaBmAgInS	aplikovat
archeoastronomický	archeoastronomický	k2eAgInSc1d1	archeoastronomický
výzkum	výzkum	k1gInSc1	výzkum
svým	svůj	k3xOyFgInSc7	svůj
výkladem	výklad	k1gInSc7	výklad
"	"	kIx"	"
<g/>
makotřaské	makotřaský	k2eAgFnPc4d1	makotřaský
lokality	lokalita	k1gFnPc4	lokalita
<g/>
"	"	kIx"	"
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
4	[number]	k4	4
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
vyložil	vyložit	k5eAaPmAgMnS	vyložit
"	"	kIx"	"
<g/>
kosmologickou	kosmologický	k2eAgFnSc4d1	kosmologická
symboliku	symbolika	k1gFnSc4	symbolika
<g/>
"	"	kIx"	"
gotické	gotický	k2eAgFnPc1d1	gotická
architektury	architektura	k1gFnPc1	architektura
Karlova	Karlův	k2eAgInSc2d1	Karlův
mostu	most	k1gInSc2	most
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
cenným	cenný	k2eAgInPc3d1	cenný
výsledkům	výsledek	k1gInPc3	výsledek
jeho	jeho	k3xOp3gFnSc2	jeho
práce	práce	k1gFnSc2	práce
patří	patřit	k5eAaImIp3nS	patřit
rovněž	rovněž	k9	rovněž
nová	nový	k2eAgFnSc1d1	nová
datace	datace	k1gFnSc1	datace
staroměstského	staroměstský	k2eAgInSc2d1	staroměstský
orloje	orloj	k1gInSc2	orloj
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
nové	nový	k2eAgFnPc1d1	nová
atribuce	atribuce	k1gFnPc1	atribuce
vědeckých	vědecký	k2eAgInPc2d1	vědecký
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
dalších	další	k2eAgFnPc2d1	další
prací	práce	k1gFnPc2	práce
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
komentovaný	komentovaný	k2eAgInSc4d1	komentovaný
překlad	překlad	k1gInSc4	překlad
základního	základní	k2eAgNnSc2d1	základní
Koperníkova	Koperníkův	k2eAgNnSc2d1	Koperníkovo
díla	dílo	k1gNnSc2	dílo
"	"	kIx"	"
<g/>
De	De	k?	De
Revolutionibus	Revolutionibus	k1gInSc1	Revolutionibus
Orbium	Orbium	k1gNnSc1	Orbium
Coelestium	Coelestium	k1gNnSc1	Coelestium
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dosud	dosud	k6eAd1	dosud
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
)	)	kIx)	)
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
nedokončeno	dokončit	k5eNaPmNgNnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Horský	Horský	k1gMnSc1	Horský
Stanislav	Stanislav	k1gMnSc1	Stanislav
Marušák	Marušák	k1gMnSc1	Marušák
<g/>
:	:	kIx,	:
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Horský	Horský	k1gMnSc1	Horský
ČVUT	ČVUT	kA	ČVUT
<g/>
:	:	kIx,	:
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Horský	Horský	k1gMnSc1	Horský
Česká	český	k2eAgFnSc1d1	Česká
astronomická	astronomický	k2eAgFnSc1d1	astronomická
společnost	společnost	k1gFnSc1	společnost
<g/>
:	:	kIx,	:
Vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
na	na	k7c4	na
Zdeňka	Zdeněk	k1gMnSc4	Zdeněk
Horského	Horský	k1gMnSc4	Horský
</s>
