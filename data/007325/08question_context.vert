<s>
Český	český	k2eAgInSc1d1	český
lev	lev	k1gInSc1	lev
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
filmová	filmový	k2eAgFnSc1d1	filmová
cena	cena	k1gFnSc1	cena
udělovaná	udělovaný	k2eAgFnSc1d1	udělovaná
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Ocenění	ocenění	k1gNnSc1	ocenění
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
křišťálová	křišťálový	k2eAgFnSc1d1	Křišťálová
soška	soška	k1gFnSc1	soška
lva	lev	k1gInSc2	lev
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
udělováno	udělován	k2eAgNnSc1d1	udělováno
v	v	k7c6	v
několika	několik	k4yIc2	několik
kategoriích	kategorie	k1gFnPc6	kategorie
<g/>
.	.	kIx.	.
</s>
