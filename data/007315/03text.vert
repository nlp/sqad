<s>
Johannes	Johannes	k1gMnSc1	Johannes
Gensfleisch	Gensfleisch	k1gMnSc1	Gensfleisch
<g/>
,	,	kIx,	,
řečený	řečený	k2eAgMnSc1d1	řečený
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Johannes	Johannes	k1gMnSc1	Johannes
Gensfleisch	Gensfleisch	k1gMnSc1	Gensfleisch
zur	zur	k?	zur
Laden	ladno	k1gNnPc2	ladno
zum	zum	k?	zum
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
(	(	kIx(	(
<g/>
1397	[number]	k4	1397
<g/>
/	/	kIx~	/
<g/>
1400	[number]	k4	1400
<g/>
,	,	kIx,	,
Mohuč	Mohuč	k1gFnSc1	Mohuč
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1468	[number]	k4	1468
<g/>
,	,	kIx,	,
Mohuč	Mohuč	k1gFnSc1	Mohuč
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
vynálezce	vynálezce	k1gMnSc1	vynálezce
technologie	technologie	k1gFnSc2	technologie
mechanického	mechanický	k2eAgInSc2d1	mechanický
knihtisku	knihtisk	k1gInSc2	knihtisk
pomocí	pomocí	k7c2	pomocí
sestavovatelných	sestavovatelný	k2eAgFnPc2d1	sestavovatelný
liter	litera	k1gFnPc2	litera
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
rozšíření	rozšíření	k1gNnSc2	rozšíření
jeho	jeho	k3xOp3gInPc2	jeho
objevů	objev	k1gInPc2	objev
byla	být	k5eAaImAgFnS	být
masová	masový	k2eAgFnSc1d1	masová
produkce	produkce	k1gFnSc1	produkce
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
přelom	přelom	k1gInSc1	přelom
v	v	k7c6	v
možnostech	možnost	k1gFnPc6	možnost
šíření	šíření	k1gNnSc2	šíření
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
vynález	vynález	k1gInSc1	vynález
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
bez	bez	k7c2	bez
znalostí	znalost	k1gFnPc2	znalost
tiskových	tiskový	k2eAgFnPc2d1	tisková
technologií	technologie	k1gFnPc2	technologie
Dálného	dálný	k2eAgInSc2d1	dálný
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
mohučské	mohučský	k2eAgFnSc6d1	mohučská
patricijské	patricijský	k2eAgFnSc6d1	patricijská
rodině	rodina	k1gFnSc6	rodina
Gensfleischů	Gensfleischa	k1gMnPc2	Gensfleischa
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgNnSc4	svůj
známější	známý	k2eAgNnSc4d2	známější
příjmení	příjmení	k1gNnSc4	příjmení
převzal	převzít	k5eAaPmAgInS	převzít
z	z	k7c2	z
názvu	název	k1gInSc2	název
rodového	rodový	k2eAgNnSc2d1	rodové
sídla	sídlo	k1gNnSc2	sídlo
–	–	k?	–
dvora	dvůr	k1gInSc2	dvůr
Zum	Zum	k1gMnSc1	Zum
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
<g/>
.	.	kIx.	.
</s>
<s>
Pověst	pověst	k1gFnSc1	pověst
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
kutnohorském	kutnohorský	k2eAgInSc6d1	kutnohorský
původu	původ	k1gInSc6	původ
(	(	kIx(	(
<g/>
Kuttenberg	Kuttenberg	k1gMnSc1	Kuttenberg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
reálný	reálný	k2eAgInSc4d1	reálný
základ	základ	k1gInSc4	základ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
snad	snad	k9	snad
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Erfurtu	Erfurt	k1gInSc6	Erfurt
(	(	kIx(	(
<g/>
1419	[number]	k4	1419
<g/>
–	–	k?	–
<g/>
1420	[number]	k4	1420
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1428	[number]	k4	1428
opustil	opustit	k5eAaPmAgMnS	opustit
Mohuč	Mohuč	k1gFnSc4	Mohuč
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
sociálním	sociální	k2eAgInPc3d1	sociální
nepokojům	nepokoj	k1gInPc3	nepokoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1434	[number]	k4	1434
<g/>
–	–	k?	–
<g/>
1444	[number]	k4	1444
je	být	k5eAaImIp3nS	být
doložen	doložen	k2eAgInSc4d1	doložen
jeho	on	k3xPp3gInSc4	on
pobyt	pobyt	k1gInSc4	pobyt
ve	v	k7c6	v
Štrasburku	Štrasburk	k1gInSc6	Štrasburk
<g/>
.	.	kIx.	.
</s>
<s>
Gutenberg	Gutenberg	k1gInSc1	Gutenberg
byl	být	k5eAaImAgInS	být
brusičem	brusič	k1gInSc7	brusič
drahokamů	drahokam	k1gInPc2	drahokam
a	a	k8xC	a
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
zlatníkem	zlatník	k1gInSc7	zlatník
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
zkušenost	zkušenost	k1gFnSc1	zkušenost
s	s	k7c7	s
puncovními	puncovní	k2eAgNnPc7d1	puncovní
razidly	razidlo	k1gNnPc7	razidlo
a	a	k8xC	a
pečetními	pečetní	k2eAgInPc7d1	pečetní
typáři	typář	k1gInPc7	typář
ho	on	k3xPp3gMnSc4	on
přivedla	přivést	k5eAaPmAgFnS	přivést
k	k	k7c3	k
myšlence	myšlenka	k1gFnSc3	myšlenka
odlévání	odlévání	k1gNnSc6	odlévání
písmen	písmeno	k1gNnPc2	písmeno
z	z	k7c2	z
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
prvním	první	k4xOgInPc3	první
pokusům	pokus	k1gInPc3	pokus
přistoupil	přistoupit	k5eAaPmAgMnS	přistoupit
už	už	k6eAd1	už
koncem	koncem	k7c2	koncem
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zda	zda	k8xS	zda
k	k	k7c3	k
prvním	první	k4xOgInPc3	první
pokusům	pokus	k1gInPc3	pokus
s	s	k7c7	s
knihtiskem	knihtisk	k1gInSc7	knihtisk
došlo	dojít	k5eAaPmAgNnS	dojít
ještě	ještě	k9	ještě
ve	v	k7c6	v
Štrasburku	Štrasburk	k1gInSc6	Štrasburk
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Mohuči	Mohuč	k1gFnSc6	Mohuč
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1448	[number]	k4	1448
vrátil	vrátit	k5eAaPmAgInS	vrátit
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
dochované	dochovaný	k2eAgInPc1d1	dochovaný
tisky	tisk	k1gInPc1	tisk
totiž	totiž	k9	totiž
pocházejí	pocházet	k5eAaImIp3nP	pocházet
až	až	k9	až
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
jeho	on	k3xPp3gNnSc2	on
mohučského	mohučský	k2eAgNnSc2d1	mohučské
působení	působení	k1gNnSc2	působení
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
datovaným	datovaný	k2eAgInSc7d1	datovaný
tiskem	tisk	k1gInSc7	tisk
Johanna	Johanen	k2eAgFnSc1d1	Johanna
Gutenberga	Gutenberga	k1gFnSc1	Gutenberga
je	být	k5eAaImIp3nS	být
jednolistový	jednolistový	k2eAgInSc4d1	jednolistový
kalendář	kalendář	k1gInSc4	kalendář
Almanach	almanach	k1gInSc1	almanach
auf	auf	k?	auf
das	das	k?	das
Jahr	Jahr	k1gInSc1	Jahr
1448	[number]	k4	1448
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
z	z	k7c2	z
povahy	povaha	k1gFnSc2	povaha
svého	svůj	k3xOyFgInSc2	svůj
obsahu	obsah	k1gInSc2	obsah
vyšel	vyjít	k5eAaPmAgInS	vyjít
nejpozději	pozdě	k6eAd3	pozdě
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1448	[number]	k4	1448
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
nejstarších	starý	k2eAgInPc6d3	nejstarší
prvotiscích	prvotisk	k1gInPc6	prvotisk
je	být	k5eAaImIp3nS	být
vedeno	vést	k5eAaImNgNnS	vést
mnoho	mnoho	k4c1	mnoho
sporů	spor	k1gInPc2	spor
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
často	často	k6eAd1	často
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
torzech	torzo	k1gNnPc6	torzo
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
datování	datování	k1gNnSc2	datování
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1450	[number]	k4	1450
začal	začít	k5eAaPmAgInS	začít
Gutenberg	Gutenberg	k1gInSc1	Gutenberg
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
nejznámějším	známý	k2eAgInSc6d3	nejznámější
díle	díl	k1gInSc6	díl
–	–	k?	–
tištěné	tištěný	k2eAgFnSc6d1	tištěná
bibli	bible	k1gFnSc6	bible
(	(	kIx(	(
<g/>
42	[number]	k4	42
<g/>
řádková	řádkový	k2eAgFnSc1d1	řádková
bible	bible	k1gFnSc1	bible
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Gutenbergova	Gutenbergův	k2eAgFnSc1d1	Gutenbergova
bible	bible	k1gFnSc1	bible
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prostředky	prostředek	k1gInPc1	prostředek
k	k	k7c3	k
vybavení	vybavení	k1gNnSc3	vybavení
dílny	dílna	k1gFnSc2	dílna
si	se	k3xPyFc3	se
opakovaně	opakovaně	k6eAd1	opakovaně
půjčil	půjčit	k5eAaPmAgMnS	půjčit
u	u	k7c2	u
bohatého	bohatý	k2eAgMnSc2d1	bohatý
mohučského	mohučský	k2eAgMnSc2d1	mohučský
měšťana	měšťan	k1gMnSc2	měšťan
Johanna	Johann	k1gMnSc4	Johann
Fusta	Fust	k1gMnSc4	Fust
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
chápal	chápat	k5eAaImAgMnS	chápat
Fusta	Fusta	k1gFnSc1	Fusta
jako	jako	k8xC	jako
pouhého	pouhý	k2eAgMnSc2d1	pouhý
věřitele	věřitel	k1gMnSc2	věřitel
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
spoluvlastníka	spoluvlastník	k1gMnSc4	spoluvlastník
podniku	podnik	k1gInSc2	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Zisky	zisk	k1gInPc1	zisk
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
bible	bible	k1gFnSc2	bible
Gutenberg	Gutenberg	k1gInSc1	Gutenberg
investoval	investovat	k5eAaBmAgInS	investovat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
podniku	podnik	k1gInSc2	podnik
a	a	k8xC	a
Fust	Fust	k1gMnSc1	Fust
ho	on	k3xPp3gMnSc4	on
proto	proto	k6eAd1	proto
zažaloval	zažalovat	k5eAaPmAgMnS	zažalovat
pro	pro	k7c4	pro
nesplácení	nesplácení	k1gNnSc4	nesplácení
dluhu	dluh	k1gInSc2	dluh
<g/>
.	.	kIx.	.
</s>
<s>
Gutenberg	Gutenberg	k1gInSc1	Gutenberg
byl	být	k5eAaImAgInS	být
odsouzen	odsouzen	k2eAgMnSc1d1	odsouzen
a	a	k8xC	a
k	k	k7c3	k
umoření	umoření	k1gNnSc3	umoření
dluhu	dluh	k1gInSc2	dluh
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
tiskárna	tiskárna	k1gFnSc1	tiskárna
zabavena	zabavit	k5eAaPmNgFnS	zabavit
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
majitel	majitel	k1gMnSc1	majitel
Johann	Johann	k1gMnSc1	Johann
Fust	Fust	k1gMnSc1	Fust
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
jejím	její	k3xOp3gMnPc3	její
vedoucím	vedoucí	k1gMnPc3	vedoucí
Gutenbergova	Gutenbergův	k2eAgMnSc2d1	Gutenbergův
tovaryše	tovaryš	k1gMnSc2	tovaryš
Petra	Petr	k1gMnSc2	Petr
Schöffera	Schöffera	k1gFnSc1	Schöffera
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
zbylých	zbylý	k2eAgMnPc2d1	zbylý
učňů	učeň	k1gMnPc2	učeň
odešla	odejít	k5eAaPmAgFnS	odejít
provozovat	provozovat	k5eAaImF	provozovat
živnost	živnost	k1gFnSc1	živnost
do	do	k7c2	do
jiných	jiný	k2eAgNnPc2d1	jiné
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Knihtisk	knihtisk	k1gInSc1	knihtisk
přestal	přestat	k5eAaPmAgInS	přestat
být	být	k5eAaImF	být
tajemstvím	tajemství	k1gNnSc7	tajemství
jediné	jediný	k2eAgFnSc2d1	jediná
firmy	firma	k1gFnSc2	firma
a	a	k8xC	a
šířil	šířit	k5eAaImAgMnS	šířit
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Gutenberg	Gutenberg	k1gInSc1	Gutenberg
byl	být	k5eAaImAgInS	být
naopak	naopak	k6eAd1	naopak
ožebračen	ožebračit	k5eAaPmNgInS	ožebračit
<g/>
.	.	kIx.	.
</s>
<s>
Sklonek	sklonek	k1gInSc1	sklonek
života	život	k1gInSc2	život
prožil	prožít	k5eAaPmAgInS	prožít
v	v	k7c6	v
chudobě	chudoba	k1gFnSc6	chudoba
<g/>
,	,	kIx,	,
knihtisku	knihtisk	k1gInSc6	knihtisk
se	s	k7c7	s
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
už	už	k6eAd1	už
nevěnoval	věnovat	k5eNaImAgMnS	věnovat
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
jej	on	k3xPp3gMnSc4	on
zaopatřil	zaopatřit	k5eAaPmAgInS	zaopatřit
mohučský	mohučský	k2eAgMnSc1d1	mohučský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Adolf	Adolf	k1gMnSc1	Adolf
Nasavský	Nasavský	k2eAgMnSc1d1	Nasavský
přijetím	přijetí	k1gNnSc7	přijetí
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
dvoru	dvůr	k1gInSc3	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1468	[number]	k4	1468
Johannes	Johannes	k1gMnSc1	Johannes
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
hrob	hrob	k1gInSc1	hrob
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
konce	konec	k1gInSc2	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
neznámý	známý	k2eNgMnSc1d1	neznámý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
r.	r.	kA	r.
1577	[number]	k4	1577
jezuité	jezuita	k1gMnPc1	jezuita
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přebrali	přebrat	k5eAaPmAgMnP	přebrat
františkánský	františkánský	k2eAgInSc4d1	františkánský
kostel	kostel	k1gInSc4	kostel
<g/>
,	,	kIx,	,
nechali	nechat	k5eAaPmAgMnP	nechat
odstranit	odstranit	k5eAaPmF	odstranit
všechny	všechen	k3xTgInPc4	všechen
nápisy	nápis	k1gInPc4	nápis
a	a	k8xC	a
ozdoby	ozdoba	k1gFnPc4	ozdoba
z	z	k7c2	z
náhrobních	náhrobní	k2eAgInPc2d1	náhrobní
kamenů	kámen	k1gInPc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Knihtisk	knihtisk	k1gInSc1	knihtisk
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
Gutenbergem	Gutenberg	k1gMnSc7	Gutenberg
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
rozšířen	rozšířen	k2eAgMnSc1d1	rozšířen
pouze	pouze	k6eAd1	pouze
blokový	blokový	k2eAgInSc1d1	blokový
tisk	tisk	k1gInSc1	tisk
(	(	kIx(	(
<g/>
deskotisk	deskotisk	k1gInSc1	deskotisk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stránka	stránka	k1gFnSc1	stránka
včetně	včetně	k7c2	včetně
písma	písmo	k1gNnSc2	písmo
i	i	k8xC	i
obrazu	obraz	k1gInSc2	obraz
kompletně	kompletně	k6eAd1	kompletně
odlila	odlít	k5eAaPmAgFnS	odlít
<g/>
,	,	kIx,	,
či	či	k8xC	či
vyřezala	vyřezat	k5eAaPmAgFnS	vyřezat
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
pak	pak	k6eAd1	pak
vytiskla	vytisknout	k5eAaPmAgFnS	vytisknout
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
tištěnou	tištěný	k2eAgFnSc4d1	tištěná
stránku	stránka	k1gFnSc4	stránka
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
tedy	tedy	k9	tedy
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
značně	značně	k6eAd1	značně
zdlouhavé	zdlouhavý	k2eAgNnSc1d1	zdlouhavé
a	a	k8xC	a
nákladné	nákladný	k2eAgNnSc1d1	nákladné
<g/>
.	.	kIx.	.
</s>
<s>
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
přišel	přijít	k5eAaPmAgMnS	přijít
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
sestavit	sestavit	k5eAaPmF	sestavit
stranu	strana	k1gFnSc4	strana
z	z	k7c2	z
písmových	písmový	k2eAgFnPc2d1	písmová
kuželek	kuželka	k1gFnPc2	kuželka
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
matric	matrice	k1gFnPc2	matrice
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
znaků	znak	k1gInPc2	znak
(	(	kIx(	(
<g/>
písmen	písmeno	k1gNnPc2	písmeno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
opětovně	opětovně	k6eAd1	opětovně
přeskupit	přeskupit	k5eAaPmF	přeskupit
a	a	k8xC	a
použít	použít	k5eAaPmF	použít
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
což	což	k9	což
celý	celý	k2eAgInSc4d1	celý
tisk	tisk	k1gInSc4	tisk
podstatně	podstatně	k6eAd1	podstatně
zefektivnilo	zefektivnit	k5eAaPmAgNnS	zefektivnit
a	a	k8xC	a
urychlilo	urychlit	k5eAaPmAgNnS	urychlit
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
sestavování	sestavování	k1gNnSc1	sestavování
podkladů	podklad	k1gInPc2	podklad
pro	pro	k7c4	pro
tisk	tisk	k1gInSc4	tisk
z	z	k7c2	z
dřevořezů	dřevořez	k1gInPc2	dřevořez
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
znaků	znak	k1gInPc2	znak
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
známé	známý	k2eAgNnSc1d1	známé
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
za	za	k7c4	za
vynálezce	vynálezce	k1gMnSc4	vynálezce
tohoto	tento	k3xDgInSc2	tento
principu	princip	k1gInSc2	princip
řada	řada	k1gFnSc1	řada
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Nizozemec	Nizozemec	k1gMnSc1	Nizozemec
Laurens	Laurensa	k1gFnPc2	Laurensa
Janszoon	Janszoon	k1gMnSc1	Janszoon
Coster	Coster	k1gMnSc1	Coster
(	(	kIx(	(
<g/>
Haarlem	Haarlo	k1gNnSc7	Haarlo
<g/>
,	,	kIx,	,
asi	asi	k9	asi
1370	[number]	k4	1370
<g/>
–	–	k?	–
<g/>
1440	[number]	k4	1440
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
též	též	k9	též
Donat	Donat	k2eAgInSc1d1	Donat
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
ale	ale	k9	ale
právě	právě	k9	právě
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
<g/>
.	.	kIx.	.
</s>
<s>
Gutenberg	Gutenberg	k1gInSc1	Gutenberg
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
své	svůj	k3xOyFgFnSc2	svůj
práce	práce	k1gFnSc2	práce
tiskl	tisknout	k5eAaImAgInS	tisknout
kalendáře	kalendář	k1gInPc4	kalendář
a	a	k8xC	a
drobné	drobný	k2eAgFnPc4d1	drobná
knižky	knižka	k1gFnPc4	knižka
(	(	kIx(	(
<g/>
do	do	k7c2	do
28	[number]	k4	28
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
učebnice	učebnice	k1gFnPc4	učebnice
latiny	latina	k1gFnSc2	latina
zvané	zvaný	k2eAgFnPc1d1	zvaná
Donatus	Donatus	k1gInSc4	Donatus
a	a	k8xC	a
především	především	k9	především
odpustky	odpustek	k1gInPc1	odpustek
jako	jako	k8xS	jako
zdroj	zdroj	k1gInSc1	zdroj
běžného	běžný	k2eAgNnSc2d1	běžné
živobytí	živobytí	k1gNnSc2	živobytí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
získání	získání	k1gNnSc6	získání
zkušeností	zkušenost	k1gFnPc2	zkušenost
začal	začít	k5eAaPmAgInS	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
i	i	k9	i
krásně	krásně	k6eAd1	krásně
vypravená	vypravený	k2eAgNnPc1d1	vypravené
a	a	k8xC	a
náročná	náročný	k2eAgNnPc1d1	náročné
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
především	především	k9	především
svou	svůj	k3xOyFgFnSc4	svůj
slavnou	slavný	k2eAgFnSc4d1	slavná
bibli	bible	k1gFnSc4	bible
(	(	kIx(	(
<g/>
1452	[number]	k4	1452
<g/>
–	–	k?	–
<g/>
1455	[number]	k4	1455
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c6	na
pergamenu	pergamen	k1gInSc6	pergamen
i	i	k8xC	i
na	na	k7c6	na
papíře	papír	k1gInSc6	papír
dvoudílnou	dvoudílný	k2eAgFnSc4d1	dvoudílná
42	[number]	k4	42
<g/>
řádkovou	řádkový	k2eAgFnSc4d1	řádková
bibli	bible	k1gFnSc4	bible
<g/>
,	,	kIx,	,
648	[number]	k4	648
+	+	kIx~	+
634	[number]	k4	634
stránek	stránka	k1gFnPc2	stránka
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
jednosvazkouvou	jednosvazkouvý	k2eAgFnSc4d1	jednosvazkouvý
bibli	bible	k1gFnSc4	bible
ve	v	k7c6	v
fólovém	fólový	k2eAgInSc6d1	fólový
formátu	formát	k1gInSc6	formát
<g/>
,	,	kIx,	,
1768	[number]	k4	1768
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
vynález	vynález	k1gInSc4	vynález
uměl	umět	k5eAaImAgMnS	umět
i	i	k9	i
prakticky	prakticky	k6eAd1	prakticky
využít	využít	k5eAaPmF	využít
<g/>
:	:	kIx,	:
zlepšil	zlepšit	k5eAaPmAgMnS	zlepšit
tiskařský	tiskařský	k2eAgInSc4d1	tiskařský
lis	lis	k1gInSc4	lis
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
pramenů	pramen	k1gInPc2	pramen
použil	použít	k5eAaPmAgInS	použít
upravený	upravený	k2eAgInSc1d1	upravený
vinařský	vinařský	k2eAgInSc1d1	vinařský
lis	lis	k1gInSc1	lis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
shromáždil	shromáždit	k5eAaPmAgInS	shromáždit
tehdy	tehdy	k6eAd1	tehdy
dostupné	dostupný	k2eAgFnPc4d1	dostupná
znalosti	znalost	k1gFnPc4	znalost
o	o	k7c6	o
slitinách	slitina	k1gFnPc6	slitina
a	a	k8xC	a
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
použil	použít	k5eAaPmAgMnS	použít
slitinu	slitina	k1gFnSc4	slitina
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
drobnými	drobný	k2eAgFnPc7d1	drobná
úpravami	úprava	k1gFnPc7	úprava
při	při	k7c6	při
tzv.	tzv.	kA	tzv.
horné	horný	k1gMnPc4	horný
sazbě	sazba	k1gFnSc6	sazba
stále	stále	k6eAd1	stále
používána	používán	k2eAgFnSc1d1	používána
a	a	k8xC	a
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
liteřina	liteřina	k1gFnSc1	liteřina
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
novou	nový	k2eAgFnSc4d1	nová
tiskařskou	tiskařský	k2eAgFnSc4d1	tiskařská
barvu	barva	k1gFnSc4	barva
ze	z	k7c2	z
suspenze	suspenze	k1gFnSc2	suspenze
sazí	saze	k1gFnPc2	saze
ve	v	k7c6	v
lněné	lněný	k2eAgFnSc6d1	lněná
fermeži	fermež	k1gFnSc6	fermež
fermeže	fermež	k1gFnSc2	fermež
(	(	kIx(	(
<g/>
tiskařskou	tiskařský	k2eAgFnSc4d1	tiskařská
čerň	čerň	k1gFnSc4	čerň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gutenbergův	Gutenbergův	k2eAgInSc1d1	Gutenbergův
vynález	vynález	k1gInSc1	vynález
umožnil	umožnit	k5eAaPmAgInS	umožnit
tisknout	tisknout	k5eAaImF	tisknout
množství	množství	k1gNnSc4	množství
celých	celý	k2eAgFnPc2d1	celá
knih	kniha	k1gFnPc2	kniha
ve	v	k7c6	v
značných	značný	k2eAgFnPc6d1	značná
sériích	série	k1gFnPc6	série
<g/>
.	.	kIx.	.
</s>
<s>
Vynález	vynález	k1gInSc1	vynález
tisku	tisk	k1gInSc2	tisk
pomocí	pomocí	k7c2	pomocí
pohyblivých	pohyblivý	k2eAgFnPc2d1	pohyblivá
liter	litera	k1gFnPc2	litera
způsobil	způsobit	k5eAaPmAgInS	způsobit
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
informační	informační	k2eAgFnSc4d1	informační
explozi	exploze	k1gFnSc4	exploze
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
století	století	k1gNnPc2	století
dříve	dříve	k6eAd2	dříve
byla	být	k5eAaImAgFnS	být
technologie	technologie	k1gFnSc1	technologie
knihtisku	knihtisk	k1gInSc2	knihtisk
vynalezena	vynaleznout	k5eAaPmNgFnS	vynaleznout
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tisk	tisk	k1gInSc4	tisk
dokumentů	dokument	k1gInPc2	dokument
z	z	k7c2	z
dřevořezových	dřevořezový	k2eAgFnPc2d1	dřevořezový
desek	deska	k1gFnPc2	deska
a	a	k8xC	a
následně	následně	k6eAd1	následně
od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
pomocí	pomocí	k7c2	pomocí
pohyblivých	pohyblivý	k2eAgNnPc2d1	pohyblivé
písmen	písmeno	k1gNnPc2	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
dochovaný	dochovaný	k2eAgInSc1d1	dochovaný
tisk	tisk	k1gInSc1	tisk
pomocí	pomocí	k7c2	pomocí
pohyblivých	pohyblivý	k2eAgNnPc2d1	pohyblivé
kovových	kovový	k2eAgNnPc2d1	kovové
písmen	písmeno	k1gNnPc2	písmeno
se	se	k3xPyFc4	se
dochoval	dochovat	k5eAaPmAgInS	dochovat
v	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
Čikči	Čikč	k1gInSc6	Čikč
byla	být	k5eAaImAgFnS	být
vytištěná	vytištěný	k2eAgFnSc1d1	vytištěná
v	v	k7c6	v
období	období	k1gNnSc6	období
dynastie	dynastie	k1gFnSc2	dynastie
Korjo	Korjo	k6eAd1	Korjo
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
1377	[number]	k4	1377
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
jej	on	k3xPp3gNnSc2	on
ve	v	k7c4	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
používal	používat	k5eAaImAgMnS	používat
islámský	islámský	k2eAgInSc4d1	islámský
svět	svět	k1gInSc4	svět
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
orientální	orientální	k2eAgFnSc1d1	orientální
technologie	technologie	k1gFnSc1	technologie
knihtisku	knihtisk	k1gInSc2	knihtisk
nepoužívala	používat	k5eNaImAgFnS	používat
tiskařský	tiskařský	k2eAgInSc4d1	tiskařský
lis	lis	k1gInSc4	lis
<g/>
,	,	kIx,	,
nedosahovala	dosahovat	k5eNaImAgFnS	dosahovat
příliš	příliš	k6eAd1	příliš
vysoké	vysoký	k2eAgFnPc4d1	vysoká
rychlosti	rychlost	k1gFnPc4	rychlost
tisku	tisk	k1gInSc2	tisk
a	a	k8xC	a
nedoznala	doznat	k5eNaPmAgFnS	doznat
proto	proto	k8xC	proto
výraznějšího	výrazný	k2eAgNnSc2d2	výraznější
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Gutenbergova	Gutenbergův	k2eAgFnSc1d1	Gutenbergova
bible	bible	k1gFnSc1	bible
<g/>
.	.	kIx.	.
</s>
<s>
Tisk	tisk	k1gInSc1	tisk
42	[number]	k4	42
<g/>
řádkové	řádkový	k2eAgFnSc2d1	řádková
bible	bible	k1gFnSc2	bible
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
se	s	k7c7	s
stranami	strana	k1gFnPc7	strana
o	o	k7c6	o
42	[number]	k4	42
řádcích	řádek	k1gInPc6	řádek
textu	text	k1gInSc2	text
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
nejslavnějším	slavný	k2eAgInSc7d3	nejslavnější
Gutenbergovým	Gutenbergův	k2eAgInSc7d1	Gutenbergův
podnikem	podnik	k1gInSc7	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Probíhal	probíhat	k5eAaImAgMnS	probíhat
buď	buď	k8xC	buď
na	na	k7c4	na
pergamen	pergamen	k1gInSc4	pergamen
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
papír	papír	k1gInSc1	papír
<g/>
.	.	kIx.	.
</s>
<s>
Dvousloupcový	dvousloupcový	k2eAgInSc1d1	dvousloupcový
text	text	k1gInSc1	text
napodoboval	napodobovat	k5eAaImAgInS	napodobovat
jak	jak	k6eAd1	jak
rozvržení	rozvržení	k1gNnSc4	rozvržení
<g/>
,	,	kIx,	,
tak	tak	k9	tak
písmo	písmo	k1gNnSc1	písmo
rukopisů	rukopis	k1gInPc2	rukopis
<g/>
.	.	kIx.	.
</s>
<s>
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
zvládal	zvládat	k5eAaImAgMnS	zvládat
tisk	tisk	k1gInSc4	tisk
dvěma	dva	k4xCgFnPc7	dva
barvami	barva	k1gFnPc7	barva
–	–	k?	–
červenou	červená	k1gFnSc4	červená
a	a	k8xC	a
černou	černá	k1gFnSc4	černá
<g/>
,	,	kIx,	,
ilustrace	ilustrace	k1gFnPc4	ilustrace
a	a	k8xC	a
iniciály	iniciála	k1gFnPc4	iniciála
doplnil	doplnit	k5eAaPmAgMnS	doplnit
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
tisku	tisk	k1gInSc2	tisk
iluminátor	iluminátor	k1gInSc1	iluminátor
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
36	[number]	k4	36
papírových	papírový	k2eAgInPc2d1	papírový
a	a	k8xC	a
12	[number]	k4	12
pergamenových	pergamenový	k2eAgInPc2d1	pergamenový
úplných	úplný	k2eAgInPc2d1	úplný
výtisků	výtisk	k1gInPc2	výtisk
a	a	k8xC	a
mimo	mimo	k7c4	mimo
ně	on	k3xPp3gFnPc4	on
ještě	ještě	k9	ještě
množství	množství	k1gNnSc1	množství
zlomků	zlomek	k1gInPc2	zlomek
<g/>
.	.	kIx.	.
</s>
