<s>
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
(	(	kIx(	(
<g/>
*	*	kIx~	*
28.	[number]	k4	28.
září	zářit	k5eAaImIp3nS	zářit
1944	[number]	k4	1944
Kolín	Kolín	k1gInSc1	Kolín
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
ekonom	ekonom	k1gMnSc1	ekonom
<g/>
,	,	kIx,	,
prognostik	prognostik	k1gMnSc1	prognostik
a	a	k8xC	a
třetí	třetí	k4xOgMnSc1	třetí
prezident	prezident	k1gMnSc1	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Úřadu	úřad	k1gInSc2	úřad
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
ujal	ujmout	k5eAaPmAgMnS	ujmout
složením	složení	k1gNnSc7	složení
slibu	slib	k1gInSc2	slib
8.	[number]	k4	8.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
a	a	k8xC	a
druhé	druhý	k4xOgNnSc4	druhý
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
zahájil	zahájit	k5eAaPmAgInS	zahájit
přesně	přesně	k6eAd1	přesně
o	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
předseda	předseda	k1gMnSc1	předseda
České	český	k2eAgFnSc2d1	Česká
strany	strana	k1gFnSc2	strana
sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgFnPc1d1	demokratická
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
předsedy	předseda	k1gMnSc2	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
následující	následující	k2eAgInPc4d1	následující
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
sociálnědemokratické	sociálnědemokratický	k2eAgFnSc2d1	sociálnědemokratická
vlády	vláda	k1gFnSc2	vláda
menšinového	menšinový	k2eAgInSc2d1	menšinový
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
existence	existence	k1gFnSc1	existence
byla	být	k5eAaImAgFnS	být
umožněna	umožnit	k5eAaPmNgFnS	umožnit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
opoziční	opoziční	k2eAgFnSc2d1	opoziční
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
"	"	kIx"	"
s	s	k7c7	s
Občanskou	občanský	k2eAgFnSc7d1	občanská
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
neúspěšné	úspěšný	k2eNgFnSc6d1	neúspěšná
kandidatuře	kandidatura	k1gFnSc6	kandidatura
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
z	z	k7c2	z
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2010	[number]	k4	2010
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
nově	nově	k6eAd1	nově
založené	založený	k2eAgFnSc2d1	založená
Strany	strana	k1gFnSc2	strana
Práv	právo	k1gNnPc2	právo
Občanů	občan	k1gMnPc2	občan
ZEMANOVCI	zemanovec	k1gMnPc7	zemanovec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
funkci	funkce	k1gFnSc6	funkce
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
po	po	k7c6	po
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
se	se	k3xPyFc4	se
strana	strana	k1gFnSc1	strana
nedostala	dostat	k5eNaPmAgFnS	dostat
do	do	k7c2	do
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
přímé	přímý	k2eAgFnSc6d1	přímá
volbě	volba	k1gFnSc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
třetím	třetí	k4xOgInSc7	třetí
prezidentem	prezident	k1gMnSc7	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
historicky	historicky	k6eAd1	historicky
prvním	první	k4xOgMnSc7	první
českým	český	k2eAgMnSc7d1	český
prezidentem	prezident	k1gMnSc7	prezident
zvoleným	zvolený	k2eAgInSc7d1	zvolený
přímou	přímý	k2eAgFnSc7d1	přímá
volbou	volba	k1gFnSc7	volba
<g/>
.	.	kIx.	.
</s>

