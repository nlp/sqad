<s>
V	v	k7c6	v
mluvené	mluvený	k2eAgFnSc6d1	mluvená
řeči	řeč	k1gFnSc6	řeč
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
spojka	spojka	k1gFnSc1	spojka
"	"	kIx"	"
<g/>
nebo	nebo	k8xC	nebo
<g/>
"	"	kIx"	"
jako	jako	k8xC	jako
vylučovací	vylučovací	k2eAgMnPc1d1	vylučovací
"	"	kIx"	"
<g/>
buď	buď	k8xC	buď
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tomuto	tento	k3xDgInSc3	tento
vylučovacímu	vylučovací	k2eAgInSc3d1	vylučovací
případu	případ	k1gInSc3	případ
však	však	k9	však
v	v	k7c6	v
logice	logika	k1gFnSc6	logika
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
exkluzivní	exkluzivní	k2eAgFnPc4d1	exkluzivní
disjunkce	disjunkce	k1gFnPc4	disjunkce
<g/>
,	,	kIx,	,
např	např	kA	např
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Venku	venku	k6eAd1	venku
je	být	k5eAaImIp3nS	být
mokro	mokro	k6eAd1	mokro
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
sucho	sucho	k1gNnSc1	sucho
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Nemohou	moct	k5eNaImIp3nP	moct
tedy	tedy	k9	tedy
nastat	nastat	k5eAaPmF	nastat
oba	dva	k4xCgInPc4	dva
případy	případ	k1gInPc4	případ
zároveň	zároveň	k6eAd1	zároveň
<g/>
.	.	kIx.	.
</s>
