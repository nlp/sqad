<s>
Ostřice	ostřice	k1gFnSc1	ostřice
krkonošská	krkonošský	k2eAgFnSc1d1	Krkonošská
(	(	kIx(	(
<g/>
Carex	Carex	k1gInSc1	Carex
derelicta	derelict	k1gInSc2	derelict
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
jednoděložné	jednoděložný	k2eAgFnSc2d1	jednoděložná
rostliny	rostlina	k1gFnSc2	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
šáchorovité	šáchorovitý	k2eAgFnSc2d1	šáchorovitá
(	(	kIx(	(
<g/>
Cyperaceae	Cyperacea	k1gFnSc2	Cyperacea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
složitého	složitý	k2eAgInSc2d1	složitý
taxonomického	taxonomický	k2eAgInSc2d1	taxonomický
komplexu	komplex	k1gInSc2	komplex
Carex	Carex	k1gInSc1	Carex
flava	flava	k1gFnSc1	flava
agg	agg	k?	agg
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
ostřice	ostřice	k1gFnSc2	ostřice
pozdní	pozdní	k2eAgInPc1d1	pozdní
se	se	k3xPyFc4	se
v	v	k7c6	v
ČR	ČR	kA	ČR
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ještě	ještě	k9	ještě
ostřice	ostřice	k1gFnSc1	ostřice
rusá	rusý	k2eAgFnSc1d1	rusá
(	(	kIx(	(
<g/>
Carex	Carex	k1gInSc1	Carex
flava	flava	k1gFnSc1	flava
s.	s.	k?	s.
str	str	kA	str
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ostřice	ostřice	k1gFnSc1	ostřice
šupinoplodá	šupinoplodat	k5eAaPmIp3nS	šupinoplodat
(	(	kIx(	(
<g/>
Carex	Carex	k1gInSc1	Carex
lepidocarpa	lepidocarp	k1gMnSc2	lepidocarp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ostřice	ostřice	k1gFnSc1	ostřice
skloněná	skloněný	k2eAgFnSc1d1	skloněná
(	(	kIx(	(
<g/>
Carex	Carex	k1gInSc1	Carex
demissa	demissa	k1gFnSc1	demissa
<g/>
)	)	kIx)	)
a	a	k8xC	a
ostřice	ostřice	k1gFnPc1	ostřice
pozdní	pozdní	k2eAgFnPc1d1	pozdní
(	(	kIx(	(
<g/>
Carex	Carex	k1gInSc1	Carex
viridula	viridulum	k1gNnSc2	viridulum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
endemit	endemit	k1gInSc4	endemit
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
a	a	k8xC	a
druh	druh	k1gInSc1	druh
byl	být	k5eAaImAgInS	být
řádně	řádně	k6eAd1	řádně
popsán	popsat	k5eAaPmNgInS	popsat
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Rostlin	rostlina	k1gFnPc2	rostlina
si	se	k3xPyFc3	se
povšiml	povšimnout	k5eAaPmAgMnS	povšimnout
už	už	k9	už
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
český	český	k2eAgMnSc1d1	český
botanik	botanik	k1gMnSc1	botanik
Josef	Josef	k1gMnSc1	Josef
Holub	Holub	k1gMnSc1	Holub
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
pak	pak	k6eAd1	pak
uváděna	uvádět	k5eAaImNgFnS	uvádět
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
pod	pod	k7c7	pod
provizorním	provizorní	k2eAgNnSc7d1	provizorní
jménem	jméno	k1gNnSc7	jméno
C.	C.	kA	C.
viridula	viridula	k1gFnSc1	viridula
Michx	Michx	k1gInSc1	Michx
<g/>
.	.	kIx.	.
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
pseudoscandinavica	pseudoscandinavica	k6eAd1	pseudoscandinavica
(	(	kIx(	(
<g/>
jak	jak	k6eAd1	jak
uvádí	uvádět	k5eAaImIp3nS	uvádět
i	i	k8xC	i
Kubát	Kubát	k1gMnSc1	Kubát
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
C.	C.	kA	C.
oederi	oederi	k1gNnSc1	oederi
Retz	Retz	k1gMnSc1	Retz
<g/>
.	.	kIx.	.
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
pseudoscandinavica	pseudoscandinavica	k1gMnSc1	pseudoscandinavica
<g/>
.	.	kIx.	.
</s>
<s>
Dostál	Dostál	k1gMnSc1	Dostál
1989	[number]	k4	1989
ji	on	k3xPp3gFnSc4	on
pak	pak	k6eAd1	pak
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
Carex	Carex	k1gInSc4	Carex
serotina	serotina	k1gFnSc1	serotina
Mérat	Mérat	k1gInSc1	Mérat
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
pseudoscandinavica	pseudoscandinavica	k1gFnSc1	pseudoscandinavica
Řádně	řádně	k6eAd1	řádně
popsána	popsat	k5eAaPmNgFnS	popsat
byla	být	k5eAaImAgFnS	být
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
Jitkou	Jitka	k1gFnSc7	Jitka
Štěpánkovou	Štěpánkův	k2eAgFnSc7d1	Štěpánkova
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
rostlinu	rostlina	k1gFnSc4	rostlina
dosahující	dosahující	k2eAgFnSc2d1	dosahující
výšky	výška	k1gFnSc2	výška
nejčastěji	často	k6eAd3	často
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
<g/>
,	,	kIx,	,
trsnatá	trsnatý	k2eAgFnSc1d1	trsnatá
<g/>
,	,	kIx,	,
s	s	k7c7	s
krátkým	krátký	k2eAgInSc7d1	krátký
oddenkem	oddenek	k1gInSc7	oddenek
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
střídavé	střídavý	k2eAgInPc1d1	střídavý
<g/>
,	,	kIx,	,
přisedlé	přisedlý	k2eAgInPc1d1	přisedlý
<g/>
,	,	kIx,	,
s	s	k7c7	s
listovými	listový	k2eAgFnPc7d1	listová
pochvami	pochva	k1gFnPc7	pochva
<g/>
.	.	kIx.	.
</s>
<s>
Čepele	čepel	k1gFnPc1	čepel
jsou	být	k5eAaImIp3nP	být
asi	asi	k9	asi
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2,2	[number]	k4	2,2
mm	mm	kA	mm
široké	široký	k2eAgFnPc1d1	široká
<g/>
,	,	kIx,	,
žlábkovité	žlábkovitý	k2eAgFnPc1d1	žlábkovitá
<g/>
.	.	kIx.	.
</s>
<s>
Bazální	bazální	k2eAgFnPc1d1	bazální
pochvy	pochva	k1gFnPc1	pochva
jsou	být	k5eAaImIp3nP	být
bledě	bledě	k6eAd1	bledě
hnědé	hnědý	k2eAgFnPc1d1	hnědá
a	a	k8xC	a
nerozpadavé	rozpadavý	k2eNgFnPc1d1	rozpadavý
<g/>
.	.	kIx.	.
</s>
<s>
Ostřice	ostřice	k1gFnSc1	ostřice
krkonošská	krkonošský	k2eAgFnSc1d1	Krkonošská
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
různoklasé	různoklasý	k2eAgFnPc4d1	různoklasý
ostřice	ostřice	k1gFnPc4	ostřice
<g/>
,	,	kIx,	,
nahoře	nahoře	k6eAd1	nahoře
jsou	být	k5eAaImIp3nP	být
klásky	klásek	k1gInPc1	klásek
samčí	samčí	k2eAgInPc1d1	samčí
<g/>
,	,	kIx,	,
dole	dole	k6eAd1	dole
samičí	samičí	k2eAgFnSc1d1	samičí
<g/>
.	.	kIx.	.
</s>
<s>
Samčí	samčí	k2eAgInSc1d1	samčí
vrcholový	vrcholový	k2eAgInSc1d1	vrcholový
klásek	klásek	k1gInSc1	klásek
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
jeden	jeden	k4xCgInSc1	jeden
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přisedlý	přisedlý	k2eAgMnSc1d1	přisedlý
nebo	nebo	k8xC	nebo
krátce	krátce	k6eAd1	krátce
krátce	krátce	k6eAd1	krátce
stopkatý	stopkatý	k2eAgInSc1d1	stopkatý
<g/>
.	.	kIx.	.
</s>
<s>
Samičích	samičí	k2eAgInPc2d1	samičí
klásků	klásek	k1gInPc2	klásek
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
zřídka	zřídka	k6eAd1	zřídka
až	až	k9	až
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dolní	dolní	k2eAgInSc1d1	dolní
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
oddálený	oddálený	k2eAgInSc1d1	oddálený
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
za	za	k7c2	za
zralosti	zralost	k1gFnSc2	zralost
asi	asi	k9	asi
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
mm	mm	kA	mm
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
mm	mm	kA	mm
široké	široký	k2eAgNnSc1d1	široké
<g/>
.	.	kIx.	.
</s>
<s>
Dolní	dolní	k2eAgInSc1d1	dolní
listen	listen	k1gInSc1	listen
je	být	k5eAaImIp3nS	být
pochvatý	pochvatý	k2eAgMnSc1d1	pochvatý
a	a	k8xC	a
delší	dlouhý	k2eAgMnSc1d2	delší
než	než	k8xS	než
květenství	květenství	k1gNnSc1	květenství
<g/>
.	.	kIx.	.
</s>
<s>
Okvětí	okvětí	k1gNnSc1	okvětí
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samčích	samčí	k2eAgInPc6d1	samčí
květech	květ	k1gInPc6	květ
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
3	[number]	k4	3
tyčinky	tyčinka	k1gFnSc2	tyčinka
<g/>
.	.	kIx.	.
</s>
<s>
Blizny	blizna	k1gFnPc1	blizna
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
mošnička	mošnička	k1gFnSc1	mošnička
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
2,5	[number]	k4	2,5
mm	mm	kA	mm
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
zelená	zelený	k2eAgFnSc1d1	zelená
až	až	k9	až
žlutě	žlutě	k6eAd1	žlutě
nahnědlá	nahnědlý	k2eAgFnSc1d1	nahnědlá
<g/>
,	,	kIx,	,
za	za	k7c4	za
zralosti	zralost	k1gFnPc4	zralost
našedle	našedle	k6eAd1	našedle
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
je	být	k5eAaImIp3nS	být
dvouklaný	dvouklaný	k2eAgInSc1d1	dvouklaný
zobánek	zobánek	k1gInSc1	zobánek
<g/>
,	,	kIx,	,
asi	asi	k9	asi
0,4	[number]	k4	0,4
<g/>
–	–	k?	–
<g/>
0,7	[number]	k4	0,7
mm	mm	kA	mm
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
<g/>
,	,	kIx,	,
na	na	k7c4	na
mošničku	mošnička	k1gFnSc4	mošnička
nasedá	nasedat	k5eAaImIp3nS	nasedat
přímo	přímo	k6eAd1	přímo
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
lomený	lomený	k2eAgMnSc1d1	lomený
(	(	kIx(	(
<g/>
ohnutý	ohnutý	k2eAgMnSc1d1	ohnutý
dolů	dolů	k6eAd1	dolů
ani	ani	k8xC	ani
u	u	k7c2	u
spodních	spodní	k2eAgFnPc2d1	spodní
mošniček	mošnička	k1gFnPc2	mošnička
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nažka	nažka	k1gFnSc1	nažka
kompletně	kompletně	k6eAd1	kompletně
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
mošničku	mošnička	k1gFnSc4	mošnička
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
mošnička	mošnička	k1gFnSc1	mošnička
je	být	k5eAaImIp3nS	být
podepřená	podepřený	k2eAgFnSc1d1	podepřená
plevou	pleva	k1gFnSc7	pleva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
1,5	[number]	k4	1,5
<g/>
–	–	k?	–
<g/>
2,5	[number]	k4	2,5
mm	mm	kA	mm
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
nebo	nebo	k8xC	nebo
delší	dlouhý	k2eAgFnSc1d2	delší
než	než	k8xS	než
mošnička	mošnička	k1gFnSc1	mošnička
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
krátce	krátce	k6eAd1	krátce
osinatá	osinatý	k2eAgFnSc1d1	osinatá
<g/>
,	,	kIx,	,
hnědá	hnědat	k5eAaImIp3nS	hnědat
se	se	k3xPyFc4	se
světlejším	světlý	k2eAgInSc7d2	světlejší
středem	střed	k1gInSc7	střed
<g/>
,	,	kIx,	,
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
někdy	někdy	k6eAd1	někdy
tence	tenko	k6eAd1	tenko
blanitá	blanitý	k2eAgFnSc1d1	blanitá
Počet	počet	k1gInSc1	počet
chromozómů	chromozóm	k1gInPc2	chromozóm
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
35	[number]	k4	35
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
jen	jen	k9	jen
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
kotelné	kotelný	k2eAgFnSc2d1	kotelný
jámy	jáma	k1gFnSc2	jáma
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
endemitem	endemit	k1gInSc7	endemit
české	český	k2eAgFnSc2d1	Česká
části	část	k1gFnSc2	část
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
Evropě	Evropa	k1gFnSc6	Evropa
roste	růst	k5eAaImIp3nS	růst
příbuzný	příbuzný	k2eAgInSc4d1	příbuzný
druh	druh	k1gInSc4	druh
Carex	Carex	k1gInSc1	Carex
scandinavica	scandinavic	k1gInSc2	scandinavic
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
izolované	izolovaný	k2eAgFnSc2d1	izolovaná
populace	populace	k1gFnSc2	populace
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
(	(	kIx(	(
<g/>
glaciální	glaciální	k2eAgInSc1d1	glaciální
relikt	relikt	k1gInSc1	relikt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
křížení	křížení	k1gNnSc3	křížení
s	s	k7c7	s
dalším	další	k2eAgMnSc7d1	další
druhem	druh	k1gMnSc7	druh
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Carex	Carex	k1gInSc1	Carex
demissa	demissa	k1gFnSc1	demissa
<g/>
.	.	kIx.	.
</s>
