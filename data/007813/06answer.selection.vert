<s>
Ostřice	ostřice	k1gFnSc1	ostřice
krkonošská	krkonošský	k2eAgFnSc1d1	Krkonošská
(	(	kIx(	(
<g/>
Carex	Carex	k1gInSc1	Carex
derelicta	derelict	k1gInSc2	derelict
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
jednoděložné	jednoděložný	k2eAgFnSc2d1	jednoděložná
rostliny	rostlina	k1gFnSc2	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
šáchorovité	šáchorovitý	k2eAgFnSc2d1	šáchorovitá
(	(	kIx(	(
<g/>
Cyperaceae	Cyperacea	k1gFnSc2	Cyperacea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
