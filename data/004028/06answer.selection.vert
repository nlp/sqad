<s>
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
The	The	k1gMnSc1	The
Lord	lord	k1gMnSc1	lord
of	of	k?	of
the	the	k?	the
Rings	Rings	k1gInSc1	Rings
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
epický	epický	k2eAgInSc4d1	epický
román	román	k1gInSc4	román
žánru	žánr	k1gInSc2	žánr
hrdinská	hrdinský	k2eAgFnSc1d1	hrdinská
fantasy	fantas	k1gInPc4	fantas
od	od	k7c2	od
Johna	John	k1gMnSc2	John
Ronalda	Ronald	k1gMnSc2	Ronald
Reuela	Reuel	k1gMnSc2	Reuel
Tolkiena	Tolkien	k1gMnSc2	Tolkien
<g/>
,	,	kIx,	,
napsaný	napsaný	k2eAgInSc4d1	napsaný
s	s	k7c7	s
přestávkami	přestávka	k1gFnPc7	přestávka
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1937	[number]	k4	1937
<g/>
-	-	kIx~	-
<g/>
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
