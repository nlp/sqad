<s>
Socialismus	socialismus	k1gInSc1	socialismus
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
socialis	socialis	k1gFnPc4	socialis
družný	družný	k2eAgInSc1d1	družný
<g/>
,	,	kIx,	,
společenský	společenský	k2eAgInSc1d1	společenský
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
myšlenka	myšlenka	k1gFnSc1	myšlenka
a	a	k8xC	a
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
liberálně	liberálně	k6eAd1	liberálně
kapitalistický	kapitalistický	k2eAgInSc4d1	kapitalistický
soukromovlastnický	soukromovlastnický	k2eAgInSc4d1	soukromovlastnický
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
a	a	k8xC	a
společenský	společenský	k2eAgInSc4d1	společenský
řád	řád	k1gInSc4	řád
nahradit	nahradit	k5eAaPmF	nahradit
systémem	systém	k1gInSc7	systém
založeným	založený	k2eAgInSc7d1	založený
na	na	k7c6	na
společném	společný	k2eAgNnSc6d1	společné
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
socialismu	socialismus	k1gInSc2	socialismus
je	být	k5eAaImIp3nS	být
dosažení	dosažení	k1gNnSc4	dosažení
ideálů	ideál	k1gInPc2	ideál
rovnosti	rovnost	k1gFnSc2	rovnost
<g/>
,	,	kIx,	,
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
a	a	k8xC	a
mezilidské	mezilidský	k2eAgFnSc2d1	mezilidská
solidarity	solidarita	k1gFnSc2	solidarita
cestou	cestou	k7c2	cestou
sociální	sociální	k2eAgFnSc2d1	sociální
reformy	reforma	k1gFnSc2	reforma
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
reformní	reformní	k2eAgInSc1d1	reformní
socialismus	socialismus	k1gInSc1	socialismus
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
revolucí	revoluce	k1gFnSc7	revoluce
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
revoluční	revoluční	k2eAgInSc1d1	revoluční
socialismus	socialismus	k1gInSc1	socialismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Socialismus	socialismus	k1gInSc1	socialismus
si	se	k3xPyFc3	se
obecně	obecně	k6eAd1	obecně
klade	klást	k5eAaImIp3nS	klást
tyto	tento	k3xDgInPc4	tento
cíle	cíl	k1gInPc4	cíl
<g/>
:	:	kIx,	:
změna	změna	k1gFnSc1	změna
majetkových	majetkový	k2eAgInPc2d1	majetkový
a	a	k8xC	a
právních	právní	k2eAgInPc2d1	právní
vztahů	vztah	k1gInPc2	vztah
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
změna	změna	k1gFnSc1	změna
struktury	struktura	k1gFnSc2	struktura
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
,	,	kIx,	,
boj	boj	k1gInSc1	boj
proti	proti	k7c3	proti
výsadám	výsada	k1gFnPc3	výsada
ve	v	k7c6	v
vzdělání	vzdělání	k1gNnSc6	vzdělání
a	a	k8xC	a
změna	změna	k1gFnSc1	změna
státního	státní	k2eAgNnSc2d1	státní
zřízení	zřízení	k1gNnSc2	zřízení
<g/>
.	.	kIx.	.
</s>
<s>
Socialismus	socialismus	k1gInSc1	socialismus
je	být	k5eAaImIp3nS	být
ucelený	ucelený	k2eAgInSc4d1	ucelený
myšlenkový	myšlenkový	k2eAgInSc4d1	myšlenkový
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jako	jako	k9	jako
reakce	reakce	k1gFnPc4	reakce
na	na	k7c4	na
industrializaci	industrializace	k1gFnSc4	industrializace
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
termín	termín	k1gInSc1	termín
podléhá	podléhat	k5eAaImIp3nS	podléhat
době	doba	k1gFnSc3	doba
a	a	k8xC	a
místu	místo	k1gNnSc3	místo
užití	užití	k1gNnSc2	užití
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
může	moct	k5eAaImIp3nS	moct
znamenat	znamenat	k5eAaImF	znamenat
velmi	velmi	k6eAd1	velmi
odlišné	odlišný	k2eAgInPc4d1	odlišný
postupy	postup	k1gInPc4	postup
<g/>
.	.	kIx.	.
</s>
<s>
Oxfordský	oxfordský	k2eAgInSc1d1	oxfordský
slovník	slovník	k1gInSc1	slovník
světové	světový	k2eAgFnSc2d1	světová
politiky	politika	k1gFnSc2	politika
ho	on	k3xPp3gMnSc4	on
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
sociálně-demokratický	sociálněemokratický	k2eAgInSc4d1	sociálně-demokratický
<g/>
,	,	kIx,	,
marxistický	marxistický	k2eAgInSc4d1	marxistický
a	a	k8xC	a
utopický	utopický	k2eAgInSc4d1	utopický
(	(	kIx(	(
<g/>
komunistický	komunistický	k2eAgMnSc1d1	komunistický
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
jde	jít	k5eAaImIp3nS	jít
zaprvé	zaprvé	k4xO	zaprvé
o	o	k7c4	o
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
systém	systém	k1gInSc4	systém
charakterizovaný	charakterizovaný	k2eAgInSc1d1	charakterizovaný
společným	společný	k2eAgNnSc7d1	společné
vlastnictvím	vlastnictví	k1gNnSc7	vlastnictví
výrobních	výrobní	k2eAgInPc2d1	výrobní
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
kolektivismem	kolektivismus	k1gInSc7	kolektivismus
<g/>
,	,	kIx,	,
intervencionismem	intervencionismus	k1gInSc7	intervencionismus
a	a	k8xC	a
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
reálně	reálně	k6eAd1	reálně
socialistických	socialistický	k2eAgInPc6d1	socialistický
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
vycházejících	vycházející	k2eAgFnPc2d1	vycházející
z	z	k7c2	z
totalitního	totalitní	k2eAgNnSc2d1	totalitní
pojetí	pojetí	k1gNnSc2	pojetí
řízení	řízení	k1gNnSc2	řízení
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
,	,	kIx,	,
i	i	k9	i
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
demokratických	demokratický	k2eAgFnPc6d1	demokratická
zemích	zem	k1gFnPc6	zem
určitou	určitý	k2eAgFnSc7d1	určitá
mírou	míra	k1gFnSc7	míra
plánovaného	plánovaný	k2eAgNnSc2d1	plánované
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ekonomickém	ekonomický	k2eAgInSc6d1	ekonomický
smyslu	smysl	k1gInSc6	smysl
je	být	k5eAaImIp3nS	být
protiváhou	protiváha	k1gFnSc7	protiváha
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
<g/>
.	.	kIx.	.
</s>
<s>
Zadruhé	zadruhé	k4xO	zadruhé
o	o	k7c4	o
politickou	politický	k2eAgFnSc4d1	politická
ideologii	ideologie	k1gFnSc4	ideologie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
liberalismem	liberalismus	k1gInSc7	liberalismus
a	a	k8xC	a
konzervatismem	konzervatismus	k1gInSc7	konzervatismus
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
ideologií	ideologie	k1gFnPc2	ideologie
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
poté	poté	k6eAd1	poté
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
různé	různý	k2eAgInPc4d1	různý
levicové	levicový	k2eAgInPc4d1	levicový
myšlenkové	myšlenkový	k2eAgInPc4d1	myšlenkový
směry	směr	k1gInPc4	směr
jako	jako	k8xC	jako
Demokratický	demokratický	k2eAgInSc1d1	demokratický
socialismus	socialismus	k1gInSc1	socialismus
<g/>
,	,	kIx,	,
Křesťanský	křesťanský	k2eAgInSc1d1	křesťanský
socialismus	socialismus	k1gInSc1	socialismus
<g/>
,	,	kIx,	,
Komunální	komunální	k2eAgInSc1d1	komunální
socialismus	socialismus	k1gInSc1	socialismus
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc1d1	národní
socialismus	socialismus	k1gInSc1	socialismus
nebo	nebo	k8xC	nebo
Sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
slova	slovo	k1gNnSc2	slovo
socialismus	socialismus	k1gInSc1	socialismus
je	být	k5eAaImIp3nS	být
datován	datovat	k5eAaImNgInS	datovat
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
spojován	spojovat	k5eAaImNgInS	spojovat
se	s	k7c7	s
jmény	jméno	k1gNnPc7	jméno
myslitelů	myslitel	k1gMnPc2	myslitel
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byly	být	k5eAaImAgFnP	být
Pierre	Pierr	k1gInSc5	Pierr
Leroux	Leroux	k1gInSc1	Leroux
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Roch	roch	k0	roch
Louis	Louis	k1gMnSc1	Louis
Reybaud	Reybaud	k1gMnSc1	Reybaud
a	a	k8xC	a
Robert	Robert	k1gMnSc1	Robert
Owen	Owen	k1gMnSc1	Owen
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
autora	autor	k1gMnSc2	autor
termínu	termín	k1gInSc2	termín
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
Henri	Henr	k1gFnSc3	Henr
de	de	k?	de
Saint-Simon	Saint-Simon	k1gInSc1	Saint-Simon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ho	on	k3xPp3gMnSc4	on
měl	mít	k5eAaImAgInS	mít
použít	použít	k5eAaPmF	použít
jako	jako	k9	jako
protiváhu	protiváha	k1gFnSc4	protiváha
k	k	k7c3	k
tehdejšímu	tehdejší	k2eAgNnSc3d1	tehdejší
pojetí	pojetí	k1gNnSc3	pojetí
liberalismu	liberalismus	k1gInSc2	liberalismus
<g/>
.	.	kIx.	.
</s>
<s>
Kořeny	kořen	k1gInPc1	kořen
socialismu	socialismus	k1gInSc2	socialismus
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
mnohem	mnohem	k6eAd1	mnohem
starší	starý	k2eAgInPc1d2	starší
<g/>
,	,	kIx,	,
jisté	jistý	k2eAgInPc1d1	jistý
náznaky	náznak	k1gInPc1	náznak
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
již	již	k6eAd1	již
v	v	k7c6	v
Antice	antika	k1gFnSc6	antika
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
u	u	k7c2	u
Peršana	Peršan	k1gMnSc2	Peršan
jménem	jméno	k1gNnSc7	jméno
Mazdak	Mazdak	k1gMnSc1	Mazdak
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověkém	starověký	k2eAgNnSc6d1	starověké
Řecku	Řecko	k1gNnSc6	Řecko
poté	poté	k6eAd1	poté
v	v	k7c6	v
části	část	k1gFnSc6	část
učení	učení	k1gNnSc2	učení
filozofů	filozof	k1gMnPc2	filozof
Platóna	Platón	k1gMnSc2	Platón
a	a	k8xC	a
Aristotela	Aristoteles	k1gMnSc2	Aristoteles
<g/>
.	.	kIx.	.
</s>
<s>
Platón	Platón	k1gMnSc1	Platón
například	například	k6eAd1	například
věřil	věřit	k5eAaImAgMnS	věřit
ve	v	k7c4	v
společné	společný	k2eAgNnSc4d1	společné
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
Platón	Platón	k1gMnSc1	Platón
psal	psát	k5eAaImAgMnS	psát
své	svůj	k3xOyFgInPc4	svůj
pohledy	pohled	k1gInPc4	pohled
jako	jako	k8xC	jako
ideje	idea	k1gFnPc4	idea
ideálního	ideální	k2eAgInSc2d1	ideální
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stejném	stejný	k2eAgMnSc6d1	stejný
duchu	duch	k1gMnSc6	duch
v	v	k7c6	v
období	období	k1gNnSc6	období
raného	raný	k2eAgInSc2d1	raný
novověku	novověk	k1gInSc2	novověk
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pojmenovaní	pojmenovaný	k2eAgMnPc1d1	pojmenovaný
<g/>
,	,	kIx,	,
utopičtí	utopický	k2eAgMnPc1d1	utopický
socialisté	socialist	k1gMnPc1	socialist
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
byl	být	k5eAaImAgInS	být
angličan	angličan	k1gMnSc1	angličan
Thomas	Thomas	k1gMnSc1	Thomas
More	mor	k1gInSc5	mor
<g/>
,	,	kIx,	,
renesanční	renesanční	k2eAgMnSc1d1	renesanční
humanista	humanista	k1gMnSc1	humanista
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
sepsal	sepsat	k5eAaPmAgMnS	sepsat
dílo	dílo	k1gNnSc4	dílo
Utopie	utopie	k1gFnSc2	utopie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
základní	základní	k2eAgFnSc4d1	základní
buňku	buňka	k1gFnSc4	buňka
svobodného	svobodný	k2eAgInSc2d1	svobodný
státu	stát	k1gInSc2	stát
považoval	považovat	k5eAaImAgMnS	považovat
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
ideálním	ideální	k2eAgNnSc6d1	ideální
pojetí	pojetí	k1gNnSc6	pojetí
práce	práce	k1gFnSc2	práce
trvá	trvat	k5eAaImIp3nS	trvat
pouze	pouze	k6eAd1	pouze
šest	šest	k4xCc4	šest
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
zbylý	zbylý	k2eAgInSc1d1	zbylý
čas	čas	k1gInSc1	čas
je	být	k5eAaImIp3nS	být
věnován	věnovat	k5eAaPmNgInS	věnovat
vědám	věda	k1gFnPc3	věda
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
ho	on	k3xPp3gMnSc4	on
následoval	následovat	k5eAaImAgMnS	následovat
Ital	Ital	k1gMnSc1	Ital
Tommaso	Tommasa	k1gFnSc5	Tommasa
Campanella	Campanella	k1gMnSc1	Campanella
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
"	"	kIx"	"
<g/>
Sluneční	sluneční	k2eAgInSc1d1	sluneční
stát	stát	k1gInSc1	stát
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1602	[number]	k4	1602
<g/>
)	)	kIx)	)
zveřejňuje	zveřejňovat	k5eAaImIp3nS	zveřejňovat
utopii	utopie	k1gFnSc4	utopie
o	o	k7c4	o
ideální	ideální	k2eAgFnPc4d1	ideální
společnosti	společnost	k1gFnPc4	společnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
neexistuje	existovat	k5eNaImIp3nS	existovat
soukromé	soukromý	k2eAgNnSc4d1	soukromé
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
společná	společný	k2eAgFnSc1d1	společná
práce	práce	k1gFnSc1	práce
je	být	k5eAaImIp3nS	být
zárukou	záruka	k1gFnSc7	záruka
hojnosti	hojnost	k1gFnSc2	hojnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
existuje	existovat	k5eAaImIp3nS	existovat
přísná	přísný	k2eAgFnSc1d1	přísná
reglementace	reglementace	k1gFnSc1	reglementace
způsobu	způsob	k1gInSc2	způsob
života	život	k1gInSc2	život
a	a	k8xC	a
autokratická	autokratický	k2eAgFnSc1d1	autokratická
vláda	vláda	k1gFnSc1	vláda
kněží	kněz	k1gMnPc2	kněz
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
teokratická	teokratický	k2eAgFnSc1d1	teokratická
<g/>
.	.	kIx.	.
</s>
<s>
Prvky	prvek	k1gInPc1	prvek
utopického	utopický	k2eAgInSc2d1	utopický
socialismu	socialismus	k1gInSc2	socialismus
lze	lze	k6eAd1	lze
ovšem	ovšem	k9	ovšem
najít	najít	k5eAaPmF	najít
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
u	u	k7c2	u
středověkých	středověký	k2eAgMnPc2d1	středověký
"	"	kIx"	"
<g/>
kacířů	kacíř	k1gMnPc2	kacíř
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
bogomilové	bogomil	k1gMnPc1	bogomil
<g/>
,	,	kIx,	,
valdenští	valdenští	k1gMnPc1	valdenští
<g/>
,	,	kIx,	,
sestry	sestra	k1gFnPc1	sestra
svobodného	svobodný	k2eAgMnSc2d1	svobodný
ducha	duch	k1gMnSc2	duch
<g/>
,	,	kIx,	,
bekyně	bekyně	k1gFnSc2	bekyně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
programech	program	k1gInPc6	program
některých	některý	k3yIgNnPc2	některý
rolnických	rolnický	k2eAgNnPc2d1	rolnické
povstání	povstání	k1gNnPc2	povstání
za	za	k7c2	za
feudalismu	feudalismus	k1gInSc2	feudalismus
<g/>
,	,	kIx,	,
v	v	k7c6	v
husitství	husitství	k1gNnSc6	husitství
(	(	kIx(	(
<g/>
táborité	táborita	k1gMnPc1	táborita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
Thomase	Thomas	k1gMnSc2	Thomas
Münzera	Münzer	k1gMnSc2	Münzer
(	(	kIx(	(
<g/>
vůdce	vůdce	k1gMnSc2	vůdce
selské	selský	k2eAgFnSc2d1	selská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vyzýval	vyzývat	k5eAaImAgMnS	vyzývat
povstavší	povstavší	k2eAgMnPc4d1	povstavší
rolníky	rolník	k1gMnPc4	rolník
k	k	k7c3	k
nastolení	nastolení	k1gNnSc3	nastolení
"	"	kIx"	"
<g/>
království	království	k1gNnSc3	království
božího	boží	k2eAgNnSc2d1	boží
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Revoluční	revoluční	k2eAgInSc1d1	revoluční
směr	směr	k1gInSc1	směr
utopického	utopický	k2eAgInSc2d1	utopický
socialismu	socialismus	k1gInSc2	socialismus
potom	potom	k6eAd1	potom
vyvrcholil	vyvrcholit	k5eAaPmAgInS	vyvrcholit
v	v	k7c6	v
babeufismu	babeufismus	k1gInSc6	babeufismus
<g/>
,	,	kIx,	,
pojmenovaném	pojmenovaný	k2eAgInSc6d1	pojmenovaný
jménem	jméno	k1gNnSc7	jméno
jeho	on	k3xPp3gMnSc2	on
vůdce	vůdce	k1gMnSc2	vůdce
a	a	k8xC	a
nejdůležitějšího	důležitý	k2eAgMnSc2d3	nejdůležitější
teoretika	teoretik	k1gMnSc2	teoretik
Graccha	Gracch	k1gMnSc2	Gracch
Babeufa	Babeuf	k1gMnSc2	Babeuf
-	-	kIx~	-
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
revoluční	revoluční	k2eAgNnSc4d1	revoluční
hnutí	hnutí	k1gNnSc4	hnutí
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
usilující	usilující	k2eAgFnPc1d1	usilující
o	o	k7c4	o
"	"	kIx"	"
<g/>
Republiku	republika	k1gFnSc4	republika
rovných	rovný	k2eAgFnPc2d1	rovná
<g/>
"	"	kIx"	"
-	-	kIx~	-
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
<g/>
,	,	kIx,	,
z	z	k7c2	z
jednoho	jeden	k4xCgNnSc2	jeden
centra	centrum	k1gNnSc2	centrum
řízenou	řízený	k2eAgFnSc4d1	řízená
celonárodní	celonárodní	k2eAgFnSc4d1	celonárodní
komunu	komuna	k1gFnSc4	komuna
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1796	[number]	k4	1796
hnutí	hnutí	k1gNnSc1	hnutí
zorganizovalo	zorganizovat	k5eAaPmAgNnS	zorganizovat
"	"	kIx"	"
<g/>
Spiknutí	spiknutí	k1gNnSc1	spiknutí
rovných	rovný	k2eAgFnPc2d1	rovná
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
odhaleno	odhalen	k2eAgNnSc1d1	odhaleno
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
moderním	moderní	k2eAgMnSc7d1	moderní
obhájcem	obhájce	k1gMnSc7	obhájce
socialismu	socialismus	k1gInSc2	socialismus
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
hrabě	hrabě	k1gMnSc1	hrabě
Henri	Henr	k1gFnSc2	Henr
de	de	k?	de
Saint-Simon	Saint-Simon	k1gMnSc1	Saint-Simon
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
navíc	navíc	k6eAd1	navíc
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
formu	forma	k1gFnSc4	forma
vlády	vláda	k1gFnSc2	vláda
zvanou	zvaný	k2eAgFnSc4d1	zvaná
meritokracie	meritokracie	k1gFnPc4	meritokracie
a	a	k8xC	a
kladl	klást	k5eAaImAgInS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
byrokracii	byrokracie	k1gFnSc4	byrokracie
a	a	k8xC	a
industrializaci	industrializace	k1gFnSc4	industrializace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
tradice	tradice	k1gFnPc4	tradice
dále	daleko	k6eAd2	daleko
navázali	navázat	k5eAaPmAgMnP	navázat
socialisté	socialist	k1gMnPc1	socialist
Charles	Charles	k1gMnSc1	Charles
Fourier	Fourier	k1gMnSc1	Fourier
a	a	k8xC	a
Robert	Robert	k1gMnSc1	Robert
Owen	Owen	k1gMnSc1	Owen
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Owen	Owen	k1gMnSc1	Owen
si	se	k3xPyFc3	se
představoval	představovat	k5eAaImAgMnS	představovat
budoucí	budoucí	k2eAgFnSc4d1	budoucí
beztřídní	beztřídní	k2eAgFnSc4d1	beztřídní
společnost	společnost	k1gFnSc4	společnost
jako	jako	k8xS	jako
svobodnou	svobodný	k2eAgFnSc4d1	svobodná
federaci	federace	k1gFnSc4	federace
samosprávných	samosprávný	k2eAgFnPc2d1	samosprávná
komun	komuna	k1gFnPc2	komuna
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
neúspěšně	úspěšně	k6eNd1	úspěšně
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
zorganizovat	zorganizovat	k5eAaPmF	zorganizovat
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
Pierre-Joseph	Pierre-Joseph	k1gMnSc1	Pierre-Joseph
Proudhon	Proudhon	k1gMnSc1	Proudhon
<g/>
,	,	kIx,	,
Louis	Louis	k1gMnSc1	Louis
Blanc	Blanc	k1gMnSc1	Blanc
nebo	nebo	k8xC	nebo
Charles	Charles	k1gMnSc1	Charles
Hall	Hall	k1gMnSc1	Hall
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
předchůdcům	předchůdce	k1gMnPc3	předchůdce
socialismu	socialismus	k1gInSc2	socialismus
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
řadí	řadit	k5eAaImIp3nS	řadit
Jean	Jean	k1gMnSc1	Jean
Jacques	Jacques	k1gMnSc1	Jacques
Rousseau	Rousseau	k1gMnSc1	Rousseau
a	a	k8xC	a
frakce	frakce	k1gFnSc1	frakce
zběsilých	zběsilý	k2eAgFnPc2d1	zběsilá
velké	velký	k2eAgFnSc2d1	velká
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
také	také	k9	také
přinesla	přinést	k5eAaPmAgFnS	přinést
nový	nový	k2eAgInSc4d1	nový
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
revolucí	revoluce	k1gFnPc2	revoluce
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
socialistická	socialistický	k2eAgFnSc1d1	socialistická
ideologie	ideologie	k1gFnSc1	ideologie
vytvářela	vytvářet	k5eAaImAgFnS	vytvářet
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
vědecký	vědecký	k2eAgInSc4d1	vědecký
socialismus	socialismus	k1gInSc4	socialismus
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
starých	starý	k2eAgMnPc2d1	starý
anglických	anglický	k2eAgMnPc2d1	anglický
a	a	k8xC	a
francouzských	francouzský	k2eAgMnPc2d1	francouzský
socialistů	socialist	k1gMnPc2	socialist
<g/>
.	.	kIx.	.
</s>
<s>
Pierre-Joseph	Pierre-Joseph	k1gMnSc1	Pierre-Joseph
Proudhon	Proudhon	k1gMnSc1	Proudhon
považoval	považovat	k5eAaImAgMnS	považovat
svou	svůj	k3xOyFgFnSc4	svůj
podobu	podoba	k1gFnSc4	podoba
socialismu	socialismus	k1gInSc2	socialismus
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
mutualismus	mutualismus	k1gInSc1	mutualismus
<g/>
)	)	kIx)	)
za	za	k7c4	za
vědecký	vědecký	k2eAgInSc4d1	vědecký
socialismus	socialismus	k1gInSc4	socialismus
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vědeckému	vědecký	k2eAgInSc3d1	vědecký
socialismu	socialismus	k1gInSc3	socialismus
se	se	k3xPyFc4	se
hlásili	hlásit	k5eAaImAgMnP	hlásit
také	také	k9	také
svým	svůj	k3xOyFgNnSc7	svůj
učením	učení	k1gNnSc7	učení
Karl	Karl	k1gMnSc1	Karl
Marx	Marx	k1gMnSc1	Marx
a	a	k8xC	a
Friedrich	Friedrich	k1gMnSc1	Friedrich
Engels	Engels	k1gMnSc1	Engels
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
materialisticky	materialisticky	k6eAd1	materialisticky
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
teorie	teorie	k1gFnSc1	teorie
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
marxistickou	marxistický	k2eAgFnSc7d1	marxistická
periodizací	periodizace	k1gFnSc7	periodizace
dějin	dějiny	k1gFnPc2	dějiny
předpokládala	předpokládat	k5eAaImAgFnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dělnická	dělnický	k2eAgFnSc1d1	Dělnická
třída	třída	k1gFnSc1	třída
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
třídního	třídní	k2eAgInSc2d1	třídní
boje	boj	k1gInSc2	boj
<g/>
,	,	kIx,	,
převezme	převzít	k5eAaPmIp3nS	převzít
moc	moc	k6eAd1	moc
v	v	k7c6	v
socialistické	socialistický	k2eAgFnSc6d1	socialistická
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
zapříčiní	zapříčinit	k5eAaPmIp3nS	zapříčinit
nerovnováha	nerovnováha	k1gFnSc1	nerovnováha
mezi	mezi	k7c7	mezi
výrobními	výrobní	k2eAgFnPc7d1	výrobní
silami	síla	k1gFnPc7	síla
a	a	k8xC	a
výrobními	výrobní	k2eAgInPc7d1	výrobní
vztahy	vztah	k1gInPc7	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
marxismus	marxismus	k1gInSc4	marxismus
stály	stát	k5eAaImAgFnP	stát
teorie	teorie	k1gFnPc1	teorie
družstevnictví	družstevnictví	k1gNnSc2	družstevnictví
(	(	kIx(	(
<g/>
navržené	navržený	k2eAgFnSc2d1	navržená
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
Lassallem	Lassall	k1gMnSc7	Lassall
<g/>
)	)	kIx)	)
či	či	k8xC	či
anarchistická	anarchistický	k2eAgFnSc1d1	anarchistická
filosofie	filosofie	k1gFnSc1	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
prvky	prvek	k1gInPc1	prvek
ovšem	ovšem	k9	ovšem
socialismus	socialismus	k1gInSc1	socialismus
také	také	k9	také
silně	silně	k6eAd1	silně
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
socialistických	socialistický	k2eAgNnPc2d1	socialistické
hnutí	hnutí	k1gNnPc2	hnutí
začala	začít	k5eAaPmAgFnS	začít
již	již	k6eAd1	již
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vznikem	vznik	k1gInSc7	vznik
Svazu	svaz	k1gInSc2	svaz
spravedlivých	spravedlivý	k2eAgFnPc2d1	spravedlivá
(	(	kIx(	(
<g/>
následně	následně	k6eAd1	následně
Svazu	svaz	k1gInSc3	svaz
komunistů	komunista	k1gMnPc2	komunista
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
význam	význam	k1gInSc4	význam
měla	mít	k5eAaImAgFnS	mít
Internacionála	Internacionála	k1gFnSc1	Internacionála
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
ke	k	k7c3	k
sjednocení	sjednocení	k1gNnSc3	sjednocení
marxistů	marxista	k1gMnPc2	marxista
a	a	k8xC	a
zastánců	zastánce	k1gMnPc2	zastánce
družstevnictví	družstevnictví	k1gNnSc2	družstevnictví
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
sociálnědemokratická	sociálnědemokratický	k2eAgFnSc1d1	sociálnědemokratická
strana	strana	k1gFnSc1	strana
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
Gothajským	gothajský	k2eAgInSc7d1	gothajský
programem	program	k1gInSc7	program
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
začíná	začínat	k5eAaImIp3nS	začínat
další	další	k2eAgNnSc1d1	další
štěpení	štěpení	k1gNnSc1	štěpení
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
.	.	kIx.	.
ortodoxní	ortodoxní	k2eAgInSc1d1	ortodoxní
marxismus	marxismus	k1gInSc1	marxismus
<g/>
:	:	kIx,	:
Karl	Karl	k1gMnSc1	Karl
Kautsky	Kautsky	k1gFnSc2	Kautsky
revizionismus	revizionismus	k1gInSc1	revizionismus
<g/>
:	:	kIx,	:
Eduard	Eduard	k1gMnSc1	Eduard
Bernstein	Bernstein	k1gMnSc1	Bernstein
<g/>
.	.	kIx.	.
</s>
<s>
Poukazoval	poukazovat	k5eAaImAgMnS	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
a	a	k8xC	a
politické	politický	k2eAgFnPc1d1	politická
předpovědi	předpověď	k1gFnPc1	předpověď
marxismu	marxismus	k1gInSc2	marxismus
se	se	k3xPyFc4	se
nepotvrdily	potvrdit	k5eNaPmAgFnP	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Požadoval	požadovat	k5eAaImAgMnS	požadovat
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
cíle	cíl	k1gInPc4	cíl
socialismu	socialismus	k1gInSc2	socialismus
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
revoluční	revoluční	k2eAgInSc1d1	revoluční
(	(	kIx(	(
<g/>
marxismus	marxismus	k1gInSc1	marxismus
<g/>
)	)	kIx)	)
demokratický	demokratický	k2eAgInSc1d1	demokratický
<g/>
:	:	kIx,	:
Rosa	Rosa	k1gFnSc1	Rosa
Luxemburgová	Luxemburgový	k2eAgFnSc1d1	Luxemburgová
komunistický	komunistický	k2eAgMnSc1d1	komunistický
<g/>
:	:	kIx,	:
Vladimir	Vladimir	k1gMnSc1	Vladimir
Iljič	Iljič	k1gMnSc1	Iljič
Lenin	Lenin	k1gMnSc1	Lenin
<g/>
.	.	kIx.	.
</s>
<s>
Revoluci	revoluce	k1gFnSc4	revoluce
musí	muset	k5eAaImIp3nS	muset
zorganizovat	zorganizovat	k5eAaPmF	zorganizovat
malá	malý	k2eAgFnSc1d1	malá
hrstka	hrstka	k1gFnSc1	hrstka
předvoje	předvoj	k1gInSc2	předvoj
dělnické	dělnický	k2eAgFnSc2d1	Dělnická
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
došlo	dojít	k5eAaPmAgNnS	dojít
kvůli	kvůli	k7c3	kvůli
přetrvávajícím	přetrvávající	k2eAgInPc3d1	přetrvávající
rozporům	rozpor	k1gInPc3	rozpor
k	k	k7c3	k
faktickému	faktický	k2eAgNnSc3d1	faktické
rozštěpení	rozštěpení	k1gNnSc3	rozštěpení
socialistického	socialistický	k2eAgNnSc2d1	socialistické
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
radikální	radikální	k2eAgFnSc1d1	radikální
socialistická	socialistický	k2eAgFnSc1d1	socialistická
platforma	platforma	k1gFnSc1	platforma
dala	dát	k5eAaPmAgFnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
komunistickým	komunistický	k2eAgFnPc3d1	komunistická
stranám	strana	k1gFnPc3	strana
<g/>
,	,	kIx,	,
z	z	k7c2	z
umírněného	umírněný	k2eAgInSc2d1	umírněný
sociálnědemokratického	sociálnědemokratický	k2eAgInSc2d1	sociálnědemokratický
směru	směr	k1gInSc2	směr
se	se	k3xPyFc4	se
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
strany	strana	k1gFnPc1	strana
blízké	blízký	k2eAgFnSc6d1	blízká
levostředové	levostředový	k2eAgFnSc3d1	levostředová
orientaci	orientace	k1gFnSc3	orientace
<g/>
.	.	kIx.	.
</s>
<s>
Raný	raný	k2eAgInSc1d1	raný
socialismus	socialismus	k1gInSc1	socialismus
-	-	kIx~	-
družstevní	družstevní	k2eAgMnPc1d1	družstevní
(	(	kIx(	(
<g/>
svépomocná	svépomocný	k2eAgNnPc1d1	svépomocné
družstva	družstvo	k1gNnPc1	družstvo
<g/>
)	)	kIx)	)
a	a	k8xC	a
utopický	utopický	k2eAgInSc4d1	utopický
socialismus	socialismus	k1gInSc4	socialismus
Marxismus	marxismus	k1gInSc1	marxismus
Leninismus	leninismus	k1gInSc1	leninismus
Stalinismus	stalinismus	k1gInSc1	stalinismus
Trockismus	trockismus	k1gInSc1	trockismus
Maoismus	maoismus	k1gInSc1	maoismus
Reformní	reformní	k2eAgInSc1d1	reformní
socialismus	socialismus	k1gInSc4	socialismus
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
Revizionismus	revizionismus	k1gInSc1	revizionismus
<g/>
,	,	kIx,	,
Sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
,	,	kIx,	,
Fabiánský	fabiánský	k2eAgInSc1d1	fabiánský
socialismus	socialismus	k1gInSc1	socialismus
(	(	kIx(	(
<g/>
Fabiánská	fabiánský	k2eAgFnSc1d1	Fabiánská
společnost	společnost	k1gFnSc1	společnost
<g/>
)	)	kIx)	)
Křesťanský	křesťanský	k2eAgInSc1d1	křesťanský
socialismus	socialismus	k1gInSc1	socialismus
Národní	národní	k2eAgInSc1d1	národní
socialismus	socialismus	k1gInSc1	socialismus
(	(	kIx(	(
<g/>
demokratický	demokratický	k2eAgMnSc1d1	demokratický
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
Česká	český	k2eAgFnSc1d1	Česká
<g />
.	.	kIx.	.
</s>
<s>
strana	strana	k1gFnSc1	strana
národně	národně	k6eAd1	národně
sociální	sociální	k2eAgFnSc1d1	sociální
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
strana	strana	k1gFnSc1	strana
národně	národně	k6eAd1	národně
socialistická	socialistický	k2eAgFnSc1d1	socialistická
Africký	africký	k2eAgInSc1d1	africký
socialismus	socialismus	k1gInSc1	socialismus
<g/>
,	,	kIx,	,
Arabský	arabský	k2eAgInSc1d1	arabský
socialismus	socialismus	k1gInSc1	socialismus
<g/>
,	,	kIx,	,
Guevarismus	Guevarismus	k1gInSc1	Guevarismus
<g/>
,	,	kIx,	,
Castrismus	Castrismus	k1gInSc1	Castrismus
Anarchismus	anarchismus	k1gInSc1	anarchismus
Neomarxismus	Neomarxismus	k1gInSc1	Neomarxismus
(	(	kIx(	(
<g/>
Frankfurtská	frankfurtský	k2eAgFnSc1d1	Frankfurtská
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
Skupina	skupina	k1gFnSc1	skupina
Praxis	Praxis	k1gFnSc1	Praxis
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Nová	nový	k2eAgFnSc1d1	nová
levice	levice	k1gFnSc1	levice
Socialismus	socialismus	k1gInSc1	socialismus
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Nacismus	nacismus	k1gInSc1	nacismus
<g/>
.	.	kIx.	.
</s>
<s>
Nacismus	nacismus	k1gInSc1	nacismus
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
řazen	řadit	k5eAaImNgMnS	řadit
mezi	mezi	k7c4	mezi
ultrapravicové	ultrapravicový	k2eAgInPc4d1	ultrapravicový
směry	směr	k1gInPc4	směr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zastával	zastávat	k5eAaImAgMnS	zastávat
například	například	k6eAd1	například
i	i	k9	i
Hitlerův	Hitlerův	k2eAgMnSc1d1	Hitlerův
současník	současník	k1gMnSc1	současník
Konrád	Konrád	k1gMnSc1	Konrád
Heiden	Heidna	k1gFnPc2	Heidna
<g/>
,	,	kIx,	,
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc3	století
ho	on	k3xPp3gNnSc2	on
však	však	k9	však
někteří	některý	k3yIgMnPc1	některý
neoliberálové	neoliberál	k1gMnPc1	neoliberál
začali	začít	k5eAaPmAgMnP	začít
řadit	řadit	k5eAaImF	řadit
na	na	k7c4	na
levici	levice	k1gFnSc4	levice
jako	jako	k8xS	jako
radikální	radikální	k2eAgNnSc4d1	radikální
vyústění	vyústění	k1gNnSc4	vyústění
socialismu	socialismus	k1gInSc2	socialismus
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
jeho	on	k3xPp3gInSc4	on
komunitarismus	komunitarismus	k1gInSc4	komunitarismus
a	a	k8xC	a
intervencionismus	intervencionismus	k1gInSc4	intervencionismus
<g/>
.	.	kIx.	.
</s>
<s>
Prvně	Prvně	k?	Prvně
tak	tak	k6eAd1	tak
ale	ale	k8xC	ale
učinil	učinit	k5eAaPmAgMnS	učinit
obhájce	obhájce	k1gMnSc4	obhájce
klasického	klasický	k2eAgInSc2d1	klasický
liberalismu	liberalismus	k1gInSc2	liberalismus
Friedrich	Friedrich	k1gMnSc1	Friedrich
August	August	k1gMnSc1	August
von	von	k1gInSc4	von
Hayek	Hayek	k6eAd1	Hayek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
slovo	slovo	k1gNnSc4	slovo
socialismus	socialismus	k1gInSc4	socialismus
široce	široko	k6eAd1	široko
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
používáno	používat	k5eAaImNgNnS	používat
různě	různě	k6eAd1	různě
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
a	a	k8xC	a
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
dobách	doba	k1gFnPc6	doba
jak	jak	k8xS	jak
jedinci	jedinec	k1gMnPc7	jedinec
a	a	k8xC	a
skupinami	skupina	k1gFnPc7	skupina
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
socialisty	socialist	k1gMnPc4	socialist
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jejich	jejich	k3xOp3gMnPc4	jejich
oponenty	oponent	k1gMnPc4	oponent
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
skupinami	skupina	k1gFnPc7	skupina
socialistů	socialist	k1gMnPc2	socialist
velké	velká	k1gFnSc2	velká
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgMnPc4	všechen
se	se	k3xPyFc4	se
shodují	shodovat	k5eAaImIp3nP	shodovat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgInPc4	svůj
kořeny	kořen	k1gInPc4	kořen
v	v	k7c6	v
zápasech	zápas	k1gInPc6	zápas
dělníků	dělník	k1gMnPc2	dělník
v	v	k7c6	v
devatenáctém	devatenáctý	k4xOgInSc6	devatenáctý
a	a	k8xC	a
dvacátém	dvacátý	k4xOgInSc6	dvacátý
století	století	k1gNnSc6	století
a	a	k8xC	a
že	že	k8xS	že
jednají	jednat	k5eAaImIp3nP	jednat
podle	podle	k7c2	podle
principů	princip	k1gInPc2	princip
solidarity	solidarita	k1gFnSc2	solidarita
a	a	k8xC	a
obhajují	obhajovat	k5eAaImIp3nP	obhajovat
sociálně-spravedlivou	sociálněpravedlivý	k2eAgFnSc4d1	sociálně-spravedlivý
rovnostářskou	rovnostářský	k2eAgFnSc4d1	rovnostářská
společnost	společnost	k1gFnSc4	společnost
s	s	k7c7	s
centrálně	centrálně	k6eAd1	centrálně
řízenou	řízený	k2eAgFnSc7d1	řízená
ekonomikou	ekonomika	k1gFnSc7	ekonomika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
upřednostňováním	upřednostňování	k1gNnSc7	upřednostňování
kolektivních	kolektivní	k2eAgNnPc2d1	kolektivní
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
zájmů	zájem	k1gInPc2	zájem
před	před	k7c7	před
individuálními	individuální	k2eAgInPc7d1	individuální
a	a	k8xC	a
společenským	společenský	k2eAgNnSc7d1	společenské
pojetím	pojetí	k1gNnSc7	pojetí
práce	práce	k1gFnSc2	práce
-	-	kIx~	-
socialisté	socialist	k1gMnPc1	socialist
usilují	usilovat	k5eAaImIp3nP	usilovat
o	o	k7c4	o
zespolečenštění	zespolečenštění	k1gNnSc4	zespolečenštění
výrobních	výrobní	k2eAgInPc2d1	výrobní
prostředků	prostředek	k1gInPc2	prostředek
a	a	k8xC	a
likvidaci	likvidace	k1gFnSc3	likvidace
společenských	společenský	k2eAgFnPc2d1	společenská
tříd	třída	k1gFnPc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
jejich	jejich	k3xOp3gInSc2	jejich
názoru	názor	k1gInSc2	názor
bude	být	k5eAaImBp3nS	být
takový	takový	k3xDgInSc1	takový
model	model	k1gInSc1	model
řízení	řízení	k1gNnSc2	řízení
společnosti	společnost	k1gFnSc2	společnost
přinášet	přinášet	k5eAaImF	přinášet
prospěch	prospěch	k1gInSc1	prospěch
všem	všecek	k3xTgMnPc3	všecek
bez	bez	k7c2	bez
rozdílu	rozdíl	k1gInSc2	rozdíl
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
jen	jen	k9	jen
malé	malý	k2eAgFnSc6d1	malá
skupině	skupina	k1gFnSc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
marxismu-leninismu	marxismueninismus	k1gInSc2	marxismu-leninismus
je	být	k5eAaImIp3nS	být
socialismus	socialismus	k1gInSc4	socialismus
první	první	k4xOgFnSc2	první
fáze	fáze	k1gFnSc2	fáze
komunistické	komunistický	k2eAgFnSc2d1	komunistická
společenské	společenský	k2eAgFnSc2d1	společenská
formace	formace	k1gFnSc2	formace
<g/>
;	;	kIx,	;
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
<g/>
,	,	kIx,	,
když	když	k8xS	když
socialistická	socialistický	k2eAgFnSc1d1	socialistická
revoluce	revoluce	k1gFnSc1	revoluce
porazí	porazit	k5eAaPmIp3nS	porazit
kapitalismus	kapitalismus	k1gInSc4	kapitalismus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vybudování	vybudování	k1gNnSc6	vybudování
beztřídní	beztřídní	k2eAgFnSc2d1	beztřídní
společnosti	společnost	k1gFnSc2	společnost
nastane	nastat	k5eAaPmIp3nS	nastat
druhá	druhý	k4xOgFnSc1	druhý
fáze	fáze	k1gFnSc1	fáze
komunistické	komunistický	k2eAgFnSc2d1	komunistická
společenské	společenský	k2eAgFnSc2d1	společenská
formace	formace	k1gFnSc2	formace
<g/>
.	.	kIx.	.
</s>
<s>
Socialismus	socialismus	k1gInSc1	socialismus
je	být	k5eAaImIp3nS	být
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
rovnostářské	rovnostářský	k2eAgFnSc2d1	rovnostářská
společnosti	společnost	k1gFnSc2	společnost
na	na	k7c6	na
základě	základ	k1gInSc6	základ
společného	společný	k2eAgInSc2d1	společný
(	(	kIx(	(
<g/>
družstevního	družstevní	k2eAgInSc2d1	družstevní
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
společenského	společenský	k2eAgNnSc2d1	společenské
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
centrálního	centrální	k2eAgNnSc2d1	centrální
řízení	řízení	k1gNnSc2	řízení
se	s	k7c7	s
zachováním	zachování	k1gNnSc7	zachování
soukromého	soukromý	k2eAgNnSc2d1	soukromé
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pojetí	pojetí	k1gNnSc6	pojetí
Karla	Karel	k1gMnSc4	Karel
Marxe	Marx	k1gMnSc4	Marx
byl	být	k5eAaImAgInS	být
kapitalismus	kapitalismus	k1gInSc1	kapitalismus
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
soukromém	soukromý	k2eAgNnSc6d1	soukromé
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
komunismus	komunismus	k1gInSc1	komunismus
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
opírat	opírat	k5eAaImF	opírat
o	o	k7c4	o
společenské	společenský	k2eAgNnSc4d1	společenské
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
Skutečným	skutečný	k2eAgInSc7d1	skutečný
účelem	účel	k1gInSc7	účel
socialismu	socialismus	k1gInSc2	socialismus
je	být	k5eAaImIp3nS	být
překonat	překonat	k5eAaPmF	překonat
predátorskou	predátorský	k2eAgFnSc4d1	predátorská
fázi	fáze	k1gFnSc4	fáze
lidského	lidský	k2eAgInSc2d1	lidský
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
Socialismus	socialismus	k1gInSc4	socialismus
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
ideologií	ideologie	k1gFnPc2	ideologie
nejrozsáhlejší	rozsáhlý	k2eAgFnSc4d3	nejrozsáhlejší
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
teorií	teorie	k1gFnPc2	teorie
a	a	k8xC	a
tradic	tradice	k1gFnPc2	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
společném	společný	k2eAgNnSc6d1	společné
ideologickém	ideologický	k2eAgNnSc6d1	ideologické
dědictví	dědictví	k1gNnSc6	dědictví
komunistických	komunistický	k2eAgMnPc2d1	komunistický
revolucionářů	revolucionář	k1gMnPc2	revolucionář
<g/>
,	,	kIx,	,
afrických	africký	k2eAgMnPc2d1	africký
nacionalistů	nacionalista	k1gMnPc2	nacionalista
<g/>
,	,	kIx,	,
západních	západní	k2eAgMnPc2d1	západní
sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
i	i	k8xC	i
některých	některý	k3yIgMnPc2	některý
fašistů	fašista	k1gMnPc2	fašista
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
nacionálních	nacionální	k2eAgMnPc2d1	nacionální
socialistů	socialist	k1gMnPc2	socialist
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
lépe	dobře	k6eAd2	dobře
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
"	"	kIx"	"
<g/>
socialismech	socialismus	k1gInPc6	socialismus
<g/>
"	"	kIx"	"
než	než	k8xS	než
o	o	k7c6	o
"	"	kIx"	"
<g/>
socialismu	socialismus	k1gInSc6	socialismus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
nic	nic	k3yNnSc1	nic
neobvyklého	obvyklý	k2eNgNnSc2d1	neobvyklé
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
o	o	k7c4	o
"	"	kIx"	"
<g/>
pravý	pravý	k2eAgInSc4d1	pravý
<g/>
"	"	kIx"	"
socialismus	socialismus	k1gInSc1	socialismus
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc4	tento
tradice	tradice	k1gFnPc4	tradice
a	a	k8xC	a
proudy	proud	k1gInPc4	proud
socialismu	socialismus	k1gInSc2	socialismus
k	k	k7c3	k
sobě	se	k3xPyFc3	se
chovaly	chovat	k5eAaImAgInP	chovat
mnohem	mnohem	k6eAd1	mnohem
nepřátelštěji	přátelsky	k6eNd2	přátelsky
<g/>
,	,	kIx,	,
než	než	k8xS	než
k	k	k7c3	k
jiným	jiný	k2eAgFnPc3d1	jiná
ideologiím	ideologie	k1gFnPc3	ideologie
<g/>
.	.	kIx.	.
</s>
<s>
Andrew	Andrew	k?	Andrew
Heywood	Heywood	k1gInSc1	Heywood
<g/>
:	:	kIx,	:
Politické	politický	k2eAgFnSc2d1	politická
ideologie	ideologie	k1gFnSc2	ideologie
Můj	můj	k3xOp1gInSc1	můj
socialism	socialism	k1gInSc1	socialism
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
jednoduše	jednoduše	k6eAd1	jednoduše
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
bližnímu	bližní	k1gMnSc3	bližní
<g/>
,	,	kIx,	,
humanita	humanita	k1gFnSc1	humanita
<g/>
.	.	kIx.	.
</s>
<s>
Přeji	přát	k5eAaImIp1nS	přát
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nebylo	být	k5eNaImAgNnS	být
bídy	bída	k1gFnSc2	bída
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
všichni	všechen	k3xTgMnPc1	všechen
lidé	člověk	k1gMnPc1	člověk
slušně	slušně	k6eAd1	slušně
žili	žít	k5eAaImAgMnP	žít
prací	práce	k1gFnSc7	práce
a	a	k8xC	a
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
každý	každý	k3xTgMnSc1	každý
měl	mít	k5eAaImAgMnS	mít
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
dost	dost	k6eAd1	dost
místa	místo	k1gNnPc1	místo
<g/>
,	,	kIx,	,
elbow-room	elbowoom	k1gInSc1	elbow-room
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
říkají	říkat	k5eAaImIp3nP	říkat
Amerikáni	Amerikán	k1gMnPc1	Amerikán
<g/>
.	.	kIx.	.
</s>
<s>
Humanita	humanita	k1gFnSc1	humanita
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
bývalá	bývalý	k2eAgFnSc1d1	bývalá
filantropie	filantropie	k1gFnSc1	filantropie
<g/>
;	;	kIx,	;
filantropie	filantropie	k1gFnSc1	filantropie
jenom	jenom	k9	jenom
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
tu	tu	k6eAd1	tu
a	a	k8xC	a
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
humanita	humanita	k1gFnSc1	humanita
hledí	hledět	k5eAaImIp3nS	hledět
opravit	opravit	k5eAaPmF	opravit
poměry	poměra	k1gFnPc4	poměra
zákonem	zákon	k1gInSc7	zákon
a	a	k8xC	a
řádem	řád	k1gInSc7	řád
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
toto	tento	k3xDgNnSc4	tento
socialismus	socialismus	k1gInSc1	socialismus
tož	tož	k9	tož
dobrá	dobrý	k2eAgFnSc1d1	dobrá
<g/>
.	.	kIx.	.
</s>
<s>
TGM	TGM	kA	TGM
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
...	...	k?	...
neblahé	blahý	k2eNgNnSc1d1	neblahé
dědictví	dědictví	k1gNnSc1	dědictví
selektivních	selektivní	k2eAgInPc2d1	selektivní
a	a	k8xC	a
tendenčních	tendenční	k2eAgInPc2d1	tendenční
přístupů	přístup	k1gInPc2	přístup
předlistopadové	předlistopadový	k2eAgFnSc2d1	předlistopadová
éry	éra	k1gFnSc2	éra
-	-	kIx~	-
vnesly	vnést	k5eAaPmAgInP	vnést
do	do	k7c2	do
výuky	výuka	k1gFnSc2	výuka
dějepisu	dějepis	k1gInSc2	dějepis
spíše	spíše	k9	spíše
než	než	k8xS	než
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
korektní	korektní	k2eAgFnSc4d1	korektní
informaci	informace	k1gFnSc4	informace
postmodernistické	postmodernistický	k2eAgInPc4d1	postmodernistický
relativizující	relativizující	k2eAgInPc4d1	relativizující
přístupy	přístup	k1gInPc4	přístup
a	a	k8xC	a
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
plynoucí	plynoucí	k2eAgFnSc1d1	plynoucí
hodnotové	hodnotový	k2eAgNnSc4d1	hodnotové
zmatení	zmatení	k1gNnSc4	zmatení
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
i	i	k9	i
dnes	dnes	k6eAd1	dnes
jsme	být	k5eAaImIp1nP	být
často	často	k6eAd1	často
svědky	svědek	k1gMnPc7	svědek
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnozí	mnohý	k2eAgMnPc1d1	mnohý
příslušníci	příslušník	k1gMnPc1	příslušník
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
generací	generace	k1gFnPc2	generace
mimoděčně	mimoděčně	k6eAd1	mimoděčně
směšují	směšovat	k5eAaImIp3nP	směšovat
pojmy	pojem	k1gInPc1	pojem
socialismus	socialismus	k1gInSc1	socialismus
<g/>
,	,	kIx,	,
komunismus	komunismus	k1gInSc1	komunismus
a	a	k8xC	a
stalinismus	stalinismus	k1gInSc1	stalinismus
<g/>
,	,	kIx,	,
boj	boj	k1gInSc1	boj
za	za	k7c4	za
sociální	sociální	k2eAgNnPc4d1	sociální
a	a	k8xC	a
politická	politický	k2eAgNnPc4d1	politické
práva	právo	k1gNnPc4	právo
dělnictva	dělnictvo	k1gNnSc2	dělnictvo
s	s	k7c7	s
úsilím	úsilí	k1gNnSc7	úsilí
o	o	k7c4	o
nastolení	nastolení	k1gNnSc4	nastolení
"	"	kIx"	"
<g/>
diktatury	diktatura	k1gFnSc2	diktatura
proletariátu	proletariát	k1gInSc2	proletariát
<g/>
"	"	kIx"	"
...	...	k?	...
Josef	Josef	k1gMnSc1	Josef
Tomeš	Tomeš	k1gMnSc1	Tomeš
<g/>
:	:	kIx,	:
Historie	historie	k1gFnSc1	historie
ČSSD	ČSSD	kA	ČSSD
Kapitalismu	kapitalismus	k1gInSc2	kapitalismus
je	být	k5eAaImIp3nS	být
vlastní	vlastní	k2eAgFnSc1d1	vlastní
jedna	jeden	k4xCgFnSc1	jeden
vada	vada	k1gFnSc1	vada
<g/>
:	:	kIx,	:
nerovnoměrné	rovnoměrný	k2eNgNnSc1d1	nerovnoměrné
rozdělení	rozdělení	k1gNnSc1	rozdělení
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
.	.	kIx.	.
</s>
<s>
Socialismu	socialismus	k1gInSc2	socialismus
je	být	k5eAaImIp3nS	být
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
jedna	jeden	k4xCgFnSc1	jeden
ctnost	ctnost	k1gFnSc1	ctnost
<g/>
:	:	kIx,	:
rovnoměrné	rovnoměrný	k2eAgNnSc1d1	rovnoměrné
rozdělení	rozdělení	k1gNnSc1	rozdělení
bídy	bída	k1gFnSc2	bída
<g/>
.	.	kIx.	.
</s>
<s>
Winston	Winston	k1gInSc1	Winston
Churchill	Churchill	k1gInSc1	Churchill
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
</s>
