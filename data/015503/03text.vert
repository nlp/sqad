<s>
Julius	Julius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
</s>
<s>
Julius	Julius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
červenec	červenec	k1gInSc1
100	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
Řím	Řím	k1gInSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
44	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
55	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Pompeiovo	Pompeiův	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnSc1
</s>
<s>
bodná	bodný	k2eAgFnSc1d1
rána	rána	k1gFnSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Caesarův	Caesarův	k2eAgInSc1d1
chrám	chrám	k1gInSc1
Bydliště	bydliště	k1gNnSc2
</s>
<s>
Řím	Řím	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
řečník	řečník	k1gMnSc1
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
memoárů	memoáry	k1gInPc2
<g/>
,	,	kIx,
vládce	vládce	k1gMnSc1
<g/>
,	,	kIx,
starořímský	starořímský	k2eAgMnSc1d1
historik	historik	k1gMnSc1
<g/>
,	,	kIx,
básník	básník	k1gMnSc1
<g/>
,	,	kIx,
Ancient	Ancient	k1gMnSc1
Roman	Roman	k1gMnSc1
priest	priest	k1gMnSc1
<g/>
,	,	kIx,
starořímský	starořímský	k2eAgMnSc1d1
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
starořímský	starořímský	k2eAgMnSc1d1
voják	voják	k1gMnSc1
<g/>
,	,	kIx,
politik	politik	k1gMnSc1
a	a	k8xC
vojevůdce	vojevůdce	k1gMnSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
TriumfApoteóza	TriumfApoteóza	k1gFnSc1
Politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
Populárové	populár	k1gMnPc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Cornelia	Cornelia	k1gFnSc1
Cinna	Cinn	k1gInSc2
minor	minor	k2eAgInSc2d1
(	(	kIx(
<g/>
84	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
–	–	k?
<g/>
68	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
Pompeia	Pompeius	k1gMnSc2
Sulla	Sull	k1gMnSc2
(	(	kIx(
<g/>
67	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
–	–	k?
<g/>
61	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
Calpurnia	Calpurnium	k1gNnPc1
Pisonis	Pisonis	k1gFnPc2
(	(	kIx(
<g/>
59	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
–	–	k?
<g/>
44	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
Partner	partner	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Kleopatra	Kleopatra	k1gFnSc1
VII	VII	kA
<g/>
.	.	kIx.
<g/>
SemproniaLolliaPomponiaClodia	SemproniaLolliaPomponiaClodium	k1gNnPc1
Děti	dítě	k1gFnPc1
</s>
<s>
Julia	Julius	k1gMnSc4
CaesarisPtolemaios	CaesarisPtolemaiosa	k1gFnPc2
XV	XV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
KaisarionAugustus	KaisarionAugustus	k1gInSc1
Rodiče	rodič	k1gMnPc1
</s>
<s>
Gaius	Gaius	k1gMnSc1
Julius	Julius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
a	a	k8xC
Aurelia	Aurelia	k1gFnSc1
Cotta	Cotta	k1gFnSc1
Rod	rod	k1gInSc4
</s>
<s>
Julii	Julie	k1gFnSc3
Caesares	Caesaresa	k1gFnPc2
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Julia	Julius	k1gMnSc2
Major	major	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Julia	Julius	k1gMnSc2
Caesaris	Caesaris	k1gFnPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
sourozenci	sourozenec	k1gMnPc1
<g/>
)	)	kIx)
<g/>
Pompeius	Pompeius	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Pompeia	Pompeia	k1gFnSc1
(	(	kIx(
<g/>
vnuk	vnuk	k1gMnSc1
či	či	k8xC
vnučka	vnučka	k1gFnSc1
<g/>
)	)	kIx)
Funkce	funkce	k1gFnSc1
</s>
<s>
Flamen	Flamen	k2eAgInSc1d1
Dialis	Dialis	k1gInSc1
(	(	kIx(
<g/>
84	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
–	–	k?
<g/>
81	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
Pontifikát	pontifikát	k1gInSc1
(	(	kIx(
<g/>
73	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
–	–	k?
<g/>
63	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
quaestor	quaestor	k1gMnSc1
(	(	kIx(
<g/>
69	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
curule	curule	k1gFnPc4
aedile	aedile	k6eAd1
(	(	kIx(
<g/>
65	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
Pontifex	pontifex	k1gMnSc1
maximus	maximus	k1gMnSc1
(	(	kIx(
<g/>
63	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
–	–	k?
<g/>
44	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Gaius	Gaius	k1gMnSc1
Julius	Julius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
100	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
–	–	k?
15	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
44	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
vojevůdce	vojevůdce	k1gMnSc1
a	a	k8xC
politik	politik	k1gMnSc1
a	a	k8xC
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nejmocnějších	mocný	k2eAgMnPc2d3
mužů	muž	k1gMnPc2
antické	antický	k2eAgFnSc2d1
historie	historie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sehrál	sehrát	k5eAaPmAgMnS
klíčovou	klíčový	k2eAgFnSc4d1
roli	role	k1gFnSc4
v	v	k7c6
procesu	proces	k1gInSc6
zániku	zánik	k1gInSc2
římské	římský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
její	její	k3xOp3gFnSc2
transformace	transformace	k1gFnSc2
v	v	k7c6
císařství	císařství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
jména	jméno	k1gNnSc2
pochází	pocházet	k5eAaImIp3nS
titul	titul	k1gInSc4
caesar	caesara	k1gFnPc2
a	a	k8xC
odvozené	odvozený	k2eAgInPc4d1
císař	císař	k1gMnSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
car	car	k1gMnSc1
<g/>
,	,	kIx,
německy	německy	k6eAd1
Kaiser	Kaiser	k1gMnSc1
<g/>
,	,	kIx,
dánsky	dánsky	k6eAd1
kejser	kejser	k1gInSc1
<g/>
,	,	kIx,
polsky	polsky	k6eAd1
cesarz	cesarz	k1gMnSc1
aj.	aj.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterým	který	k3yIgInSc7,k3yQgInSc7,k3yRgInSc7
ale	ale	k9
sám	sám	k3xTgMnSc1
Caesar	Caesar	k1gMnSc1
nebyl	být	k5eNaImAgMnS
<g/>
;	;	kIx,
prvním	první	k4xOgMnSc6
římským	římský	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
až	až	k9
jeho	jeho	k3xOp3gMnSc1
prasynovec	prasynovec	k1gMnSc1
a	a	k8xC
adoptivní	adoptivní	k2eAgMnSc1d1
syn	syn	k1gMnSc1
Augustus	Augustus	k1gMnSc1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
vládnoucí	vládnoucí	k2eAgFnSc1d1
v	v	k7c6
letech	let	k1gInPc6
27	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
–	–	k?
14	#num#	k4
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Starořímská	starořímský	k2eAgFnSc1d1
výslovnost	výslovnost	k1gFnSc1
byla	být	k5eAaImAgFnS
[	[	kIx(
<g/>
kaisar	kaisar	k1gMnSc1
<g/>
]	]	kIx)
<g/>
;	;	kIx,
dnešní	dnešní	k2eAgFnSc1d1
výslovnost	výslovnost	k1gFnSc1
[	[	kIx(
<g/>
cézar	cézar	k1gInSc1
<g/>
]	]	kIx)
pochází	pocházet	k5eAaImIp3nS
až	až	k9
z	z	k7c2
pozdní	pozdní	k2eAgFnSc2d1
antiky	antika	k1gFnSc2
či	či	k8xC
raného	raný	k2eAgInSc2d1
středověku	středověk	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
</s>
<s>
Jako	jako	k8xS,k8xC
stoupenec	stoupenec	k1gMnSc1
politické	politický	k2eAgFnSc2d1
frakce	frakce	k1gFnSc2
populárů	populár	k1gMnPc2
vytvořil	vytvořit	k5eAaPmAgInS
spolu	spolu	k6eAd1
s	s	k7c7
Marcem	Marce	k1gNnSc7
Liciniem	Licinium	k1gNnSc7
Crassem	Crass	k1gMnSc7
a	a	k8xC
Gnaeem	Gnaeus	k1gMnSc7
Pompeiem	Pompeius	k1gMnSc7
Magnem	Magn	k1gMnSc7
tzv.	tzv.	kA
první	první	k4xOgInSc4
triumvirát	triumvirát	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
po	po	k7c4
několik	několik	k4yIc4
let	léto	k1gNnPc2
dominoval	dominovat	k5eAaImAgInS
římské	římský	k2eAgFnSc3d1
politice	politika	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesarovy	Caesarův	k2eAgInPc1d1
výboje	výboj	k1gInPc1
v	v	k7c6
Galii	Galie	k1gFnSc6
rozšířily	rozšířit	k5eAaPmAgFnP
římskou	římska	k1gFnSc7
moc	moc	k6eAd1
hluboko	hluboko	k6eAd1
na	na	k7c4
sever	sever	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesar	Caesar	k1gMnSc1
dále	daleko	k6eAd2
jako	jako	k8xC,k8xS
první	první	k4xOgMnSc1
Říman	Říman	k1gMnSc1
pronikl	proniknout	k5eAaPmAgMnS
do	do	k7c2
Británie	Británie	k1gFnSc2
a	a	k8xC
za	za	k7c4
řeku	řeka	k1gFnSc4
Rýn	rýna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozpad	rozpad	k1gInSc1
triumvirátu	triumvirát	k1gInSc2
po	po	k7c6
smrti	smrt	k1gFnSc6
Crassa	Crass	k1gMnSc2
přivedl	přivést	k5eAaPmAgMnS
Caesara	Caesar	k1gMnSc4
do	do	k7c2
konfliktu	konflikt	k1gInSc2
s	s	k7c7
Pompeiem	Pompeius	k1gMnSc7
podporovaným	podporovaný	k2eAgMnSc7d1
římským	římský	k2eAgInSc7d1
senátem	senát	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překročením	překročení	k1gNnSc7
řeky	řeka	k1gFnSc2
Rubikonu	Rubikon	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
49	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
zahájil	zahájit	k5eAaPmAgMnS
Caesar	Caesar	k1gMnSc1
občanskou	občanský	k2eAgFnSc4d1
válku	válka	k1gFnSc4
<g/>
,	,	kIx,
na	na	k7c6
jejímž	jejíž	k3xOyRp3gInSc6
konci	konec	k1gInSc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
jediným	jediný	k2eAgMnSc7d1
a	a	k8xC
nesporným	sporný	k2eNgMnSc7d1
vládcem	vládce	k1gMnSc7
celého	celý	k2eAgInSc2d1
římského	římský	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
uchopení	uchopení	k1gNnSc6
moci	moc	k1gFnSc2
zahájil	zahájit	k5eAaPmAgMnS
rozsáhlé	rozsáhlý	k2eAgFnPc4d1
reformy	reforma	k1gFnPc4
římské	římský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
a	a	k8xC
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
doživotním	doživotní	k2eAgMnSc7d1
diktátorem	diktátor	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
však	však	k9
podnítil	podnítit	k5eAaPmAgMnS
Marca	Marcus	k1gMnSc4
Junia	Junius	k1gMnSc4
Bruta	Brut	k1gMnSc4
a	a	k8xC
řadu	řada	k1gFnSc4
dalších	další	k2eAgMnPc2d1
římských	římský	k2eAgMnPc2d1
senátorů	senátor	k1gMnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
ho	on	k3xPp3gMnSc4
o	o	k7c6
březnových	březnový	k2eAgFnPc6d1
idách	idy	k1gFnPc6
roku	rok	k1gInSc2
44	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
v	v	k7c6
senátu	senát	k1gInSc6
zavraždili	zavraždit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strůjci	strůjce	k1gMnPc1
tohoto	tento	k3xDgInSc2
činu	čin	k1gInSc2
doufali	doufat	k5eAaImAgMnP
v	v	k7c6
obnovení	obnovení	k1gNnSc6
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
akce	akce	k1gFnSc1
ale	ale	k8xC
vedla	vést	k5eAaImAgFnS
pouze	pouze	k6eAd1
k	k	k7c3
rozpoutání	rozpoutání	k1gNnSc3
nového	nový	k2eAgNnSc2d1
kola	kolo	k1gNnSc2
občanských	občanský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
něhož	jenž	k3xRgInSc2
vyšel	vyjít	k5eAaPmAgMnS
jako	jako	k9
vítěz	vítěz	k1gMnSc1
Caesarův	Caesarův	k2eAgMnSc1d1
adoptivní	adoptivní	k2eAgMnSc1d1
syn	syn	k1gMnSc1
Octavianus	Octavianus	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
poté	poté	k6eAd1
nastolil	nastolit	k5eAaPmAgMnS
římské	římský	k2eAgNnSc4d1
císařství	císařství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
42	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
byl	být	k5eAaImAgMnS
Caesar	Caesar	k1gMnSc1
římským	římský	k2eAgInSc7d1
senátem	senát	k1gInSc7
oficiálně	oficiálně	k6eAd1
prohlášen	prohlásit	k5eAaPmNgMnS
za	za	k7c4
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
římských	římský	k2eAgMnPc2d1
bohů	bůh	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Mnohé	mnohé	k1gNnSc1
z	z	k7c2
Caesarova	Caesarův	k2eAgInSc2d1
života	život	k1gInSc2
je	být	k5eAaImIp3nS
nám	my	k3xPp1nPc3
známo	znám	k2eAgNnSc1d1
z	z	k7c2
jeho	jeho	k3xOp3gMnPc2
vlastních	vlastní	k2eAgMnPc2d1
Zápisků	zápisek	k1gInPc2
(	(	kIx(
<g/>
Commentarii	Commentarie	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
sepsal	sepsat	k5eAaPmAgInS
ze	z	k7c2
svých	svůj	k3xOyFgNnPc2
vojenských	vojenský	k2eAgNnPc2d1
tažení	tažení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
další	další	k2eAgInPc4d1
soudobé	soudobý	k2eAgInPc4d1
prameny	pramen	k1gInPc4
náleží	náležet	k5eAaImIp3nP
také	také	k9
dochované	dochovaný	k2eAgInPc1d1
dopisy	dopis	k1gInPc1
a	a	k8xC
projevy	projev	k1gInPc1
Caesarova	Caesarův	k2eAgMnSc2d1
politického	politický	k2eAgMnSc2d1
soupeře	soupeř	k1gMnSc2
Cicerona	Cicero	k1gMnSc2
<g/>
,	,	kIx,
historické	historický	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
Sallustia	Sallustia	k1gFnSc1
či	či	k8xC
Cattulovy	Cattulův	k2eAgFnPc1d1
básně	báseň	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnPc4d1
podrobnosti	podrobnost	k1gFnPc4
o	o	k7c6
Caesarově	Caesarův	k2eAgInSc6d1
životě	život	k1gInSc6
byly	být	k5eAaImAgInP
zachyceny	zachycen	k2eAgInPc1d1
pozdějšími	pozdní	k2eAgMnPc7d2
historiky	historik	k1gMnPc7
–	–	k?
Appiánem	Appián	k1gInSc7
<g/>
,	,	kIx,
Suetoniem	Suetonium	k1gNnSc7
<g/>
,	,	kIx,
Plútarchem	Plútarch	k1gInSc7
<g/>
,	,	kIx,
Cassiem	Cassius	k1gMnSc7
Dionem	Dion	k1gMnSc7
a	a	k8xC
Strabónem	Strabón	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Neklidné	klidný	k2eNgNnSc4d1
mládí	mládí	k1gNnSc4
</s>
<s>
Caesar	Caesar	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
100	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
(	(	kIx(
<g/>
případně	případně	k6eAd1
o	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
dříve	dříve	k6eAd2
<g/>
)	)	kIx)
v	v	k7c6
patricijské	patricijský	k2eAgFnSc6d1
rodině	rodina	k1gFnSc6
náležející	náležející	k2eAgFnSc6d1
k	k	k7c3
rodu	rod	k1gInSc3
Juliů	Julius	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
odvozoval	odvozovat	k5eAaImAgMnS
svůj	svůj	k3xOyFgInSc4
původ	původ	k1gInSc4
od	od	k7c2
Jula	Jula	k1gFnSc1
<g/>
,	,	kIx,
syna	syn	k1gMnSc2
trójského	trójský	k2eAgMnSc2d1
hrdiny	hrdina	k1gMnSc2
Aenea	Aeneas	k1gMnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
byl	být	k5eAaImAgMnS
synem	syn	k1gMnSc7
bohyně	bohyně	k1gFnSc2
Venuše	Venuše	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navzdory	navzdory	k6eAd1
svému	svůj	k3xOyFgMnSc3
aristokratickému	aristokratický	k2eAgMnSc3d1
a	a	k8xC
patricijskému	patricijský	k2eAgInSc3d1
původu	původ	k1gInSc3
nepatřila	patřit	k5eNaImAgFnS
caesarská	caesarský	k2eAgFnSc1d1
frakce	frakce	k1gFnSc1
Juliů	Julius	k1gMnPc2
mezi	mezi	k7c4
zvláště	zvláště	k6eAd1
vlivné	vlivný	k2eAgFnPc4d1
rodiny	rodina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesarův	Caesarův	k2eAgMnSc1d1
otec	otec	k1gMnSc1
<g/>
,	,	kIx,
Gaius	Gaius	k1gMnSc1
Julius	Julius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
starší	starší	k1gMnSc1
<g/>
,	,	kIx,
dosáhl	dosáhnout	k5eAaPmAgMnS
pouze	pouze	k6eAd1
hodnosti	hodnost	k1gFnPc4
praetora	praetor	k1gMnSc2
<g/>
,	,	kIx,
třebaže	třebaže	k8xS
jeho	jeho	k3xOp3gMnSc7
švagrem	švagr	k1gMnSc7
byl	být	k5eAaImAgMnS
Gaius	Gaius	k1gMnSc1
Marius	Marius	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnPc7
matka	matka	k1gFnSc1
<g/>
,	,	kIx,
Aurelia	Aurelia	k1gFnSc1
Cotta	Cotta	k1gFnSc1
<g/>
,	,	kIx,
pocházela	pocházet	k5eAaImAgFnS
z	z	k7c2
mocnější	mocný	k2eAgFnSc2d2
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
dala	dát	k5eAaPmAgFnS
republice	republika	k1gFnSc6
řadu	řada	k1gFnSc4
konzulů	konzul	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesar	Caesar	k1gMnSc1
měl	mít	k5eAaImAgMnS
dvě	dva	k4xCgFnPc4
sestry	sestra	k1gFnPc4
<g/>
,	,	kIx,
obě	dva	k4xCgFnPc4
pojmenované	pojmenovaný	k2eAgFnSc2d1
Julia	Julius	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
jeho	jeho	k3xOp3gInSc6
dětství	dětství	k1gNnSc1
toho	ten	k3xDgMnSc4
není	být	k5eNaImIp3nS
mnoho	mnoho	k6eAd1
známo	znám	k2eAgNnSc1d1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
počáteční	počáteční	k2eAgFnPc1d1
kapitoly	kapitola	k1gFnPc1
jeho	jeho	k3xOp3gInPc2
životopisů	životopis	k1gInPc2
od	od	k7c2
Suetonia	Suetonium	k1gNnSc2
a	a	k8xC
Plútarcha	Plútarcha	k1gFnSc1
byly	být	k5eAaImAgFnP
ztraceny	ztratit	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s>
Doba	doba	k1gFnSc1
Caesarova	Caesarův	k2eAgNnSc2d1
vyrůstání	vyrůstání	k1gNnSc2
byla	být	k5eAaImAgFnS
obdobím	období	k1gNnSc7
zmatků	zmatek	k1gInPc2
v	v	k7c6
římských	římský	k2eAgFnPc6d1
dějinách	dějiny	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
91	#num#	k4
až	až	k9
88	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
probíhala	probíhat	k5eAaImAgFnS
spojenecká	spojenecký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
mezi	mezi	k7c7
Římem	Řím	k1gInSc7
a	a	k8xC
jeho	jeho	k3xOp3gMnPc7
italskými	italský	k2eAgMnPc7d1
spojenci	spojenec	k1gMnPc7
žádajícími	žádající	k2eAgFnPc7d1
římské	římský	k2eAgNnSc4d1
občanství	občanství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takřka	takřka	k6eAd1
současně	současně	k6eAd1
ohrožoval	ohrožovat	k5eAaImAgMnS
pontský	pontský	k2eAgMnSc1d1
král	král	k1gMnSc1
Mithridatés	Mithridatésa	k1gFnPc2
římské	římský	k2eAgFnSc2d1
provincie	provincie	k1gFnSc2
na	na	k7c6
východě	východ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římská	římský	k2eAgFnSc1d1
politická	politický	k2eAgFnSc1d1
scéna	scéna	k1gFnSc1
byla	být	k5eAaImAgFnS
rozpolcena	rozpolcen	k2eAgFnSc1d1
mezi	mezi	k7c4
znepřátelené	znepřátelený	k2eAgFnPc4d1
frakce	frakce	k1gFnPc4
„	„	k?
<g/>
šlechtických	šlechtický	k2eAgInPc2d1
<g/>
“	“	k?
optimátů	optimát	k1gMnPc2
(	(	kIx(
<g/>
optimates	optimates	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
„	„	k?
<g/>
lidových	lidový	k2eAgInPc2d1
<g/>
“	“	k?
populárů	populár	k1gInPc2
(	(	kIx(
<g/>
populares	populares	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
Caesarův	Caesarův	k2eAgMnSc1d1
strýc	strýc	k1gMnSc1
Marius	Marius	k1gMnSc1
byl	být	k5eAaImAgMnS
vůdcem	vůdce	k1gMnSc7
populárů	populár	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lucius	Lucius	k1gMnSc1
Cornelius	Cornelius	k1gMnSc1
Sulla	Sulla	k1gMnSc1
<g/>
,	,	kIx,
někdejší	někdejší	k2eAgMnSc1d1
Mariův	Mariův	k2eAgMnSc1d1
legát	legát	k1gMnSc1
z	z	k7c2
války	válka	k1gFnSc2
proti	proti	k7c3
Jugurthovi	Jugurth	k1gMnSc3
<g/>
,	,	kIx,
stál	stát	k5eAaImAgMnS
v	v	k7c6
čele	čelo	k1gNnSc6
optimátů	optimát	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
se	se	k3xPyFc4
vyznamenali	vyznamenat	k5eAaPmAgMnP
během	během	k7c2
spojenecké	spojenecký	k2eAgFnSc2d1
války	válka	k1gFnSc2
a	a	k8xC
oba	dva	k4xCgMnPc1
nyní	nyní	k6eAd1
žádali	žádat	k5eAaImAgMnP
velení	velení	k1gNnSc4
ve	v	k7c6
válce	válka	k1gFnSc6
proti	proti	k7c3
Mithridatovi	Mithridat	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
sice	sice	k8xC
přiřčeno	přiřknout	k5eAaPmNgNnS
Sullovi	Sullův	k2eAgMnPc1d1
<g/>
,	,	kIx,
když	když	k8xS
ale	ale	k8xC
opustil	opustit	k5eAaPmAgInS
Řím	Řím	k1gInSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
ujal	ujmout	k5eAaPmAgMnS
velení	velení	k1gNnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
na	na	k7c4
jeho	jeho	k3xOp3gNnSc4
místo	místo	k1gNnSc4
dosazen	dosazen	k2eAgInSc1d1
Marius	Marius	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nato	nato	k6eAd1
Sulla	Sulla	k1gMnSc1
vytáhl	vytáhnout	k5eAaPmAgMnS
s	s	k7c7
legiemi	legie	k1gFnPc7
proti	proti	k7c3
Římu	Řím	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marius	Marius	k1gMnSc1
byl	být	k5eAaImAgMnS
přinucen	přinutit	k5eAaPmNgMnS
odejít	odejít	k5eAaPmF
do	do	k7c2
vyhnanství	vyhnanství	k1gNnSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
po	po	k7c6
Sullově	Sullův	k2eAgInSc6d1
odchodu	odchod	k1gInSc6
na	na	k7c4
východ	východ	k1gInSc4
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
zpět	zpět	k6eAd1
a	a	k8xC
spolu	spolu	k6eAd1
se	se	k3xPyFc4
svým	svůj	k3xOyFgMnSc7
spojencem	spojenec	k1gMnSc7
Luciem	Lucius	k1gMnSc7
Corneliem	Cornelium	k1gNnSc7
Cinnou	Cinna	k1gFnSc7
se	se	k3xPyFc4
zmocnil	zmocnit	k5eAaPmAgInS
vlády	vláda	k1gFnSc2
v	v	k7c6
Římě	Řím	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sulla	Sulla	k1gMnSc1
byl	být	k5eAaImAgMnS
prohlášen	prohlásit	k5eAaPmNgMnS
za	za	k7c4
veřejného	veřejný	k2eAgMnSc4d1
nepřítele	nepřítel	k1gMnSc4
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
stoupenci	stoupenec	k1gMnPc1
byli	být	k5eAaImAgMnP
zmasakrováni	zmasakrovat	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marius	Marius	k1gInSc1
již	již	k6eAd1
krátce	krátce	k6eAd1
poté	poté	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
86	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
zemřel	zemřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
populárové	populár	k1gMnPc1
přesto	přesto	k8xC
nadále	nadále	k6eAd1
zůstávali	zůstávat	k5eAaImAgMnP
u	u	k7c2
moci	moc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
85	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Caesarův	Caesarův	k2eAgMnSc1d1
otec	otec	k1gMnSc1
náhle	náhle	k6eAd1
zemřel	zemřít	k5eAaPmAgMnS
a	a	k8xC
Caesar	Caesar	k1gMnSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
ve	v	k7c6
věku	věk	k1gInSc6
šestnácti	šestnáct	k4xCc2
let	léto	k1gNnPc2
stal	stát	k5eAaPmAgMnS
hlavou	hlava	k1gFnSc7
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujícím	následující	k2eAgInSc6d1
roce	rok	k1gInSc6
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Cinnovou	Cinnový	k2eAgFnSc7d1
dcerou	dcera	k1gFnSc7
Cornelií	Cornelie	k1gFnPc2
<g/>
,	,	kIx,
když	když	k8xS
ještě	ještě	k9
předtím	předtím	k6eAd1
zrušil	zrušit	k5eAaPmAgInS
zasnoubení	zasnoubení	k1gNnSc4
s	s	k7c7
Cossutií	Cossutie	k1gFnSc7
<g/>
,	,	kIx,
dívkou	dívka	k1gFnSc7
z	z	k7c2
bohaté	bohatý	k2eAgFnSc2d1
jezdecké	jezdecký	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
byl	být	k5eAaImAgInS
určen	určit	k5eAaPmNgInS
za	za	k7c4
Jovova	Jovův	k2eAgMnSc4d1
velekněze	velekněz	k1gMnSc4
(	(	kIx(
<g/>
flamen	flamen	k2eAgInSc1d1
Dialis	Dialis	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesarův	Caesarův	k2eAgInSc1d1
předchůdce	předchůdce	k1gMnSc1
padl	padnout	k5eAaPmAgInS,k5eAaImAgInS
za	za	k7c4
oběť	oběť	k1gFnSc4
Mariovým	Mariův	k2eAgMnPc3d1
stoupencům	stoupenec	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
Sulla	Sulla	k1gMnSc1
přiměl	přimět	k5eAaPmAgMnS
Mithridata	Mithridat	k1gMnSc4
k	k	k7c3
uzavření	uzavření	k1gNnSc3
míru	mír	k1gInSc2
<g/>
,	,	kIx,
obrátil	obrátit	k5eAaPmAgMnS
se	se	k3xPyFc4
proti	proti	k7c3
populárům	populár	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
rychlém	rychlý	k2eAgNnSc6d1
tažení	tažení	k1gNnSc6
Itálií	Itálie	k1gFnPc2
porazil	porazit	k5eAaPmAgInS
v	v	k7c6
listopadu	listopad	k1gInSc6
roku	rok	k1gInSc2
82	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
mariovce	mariovec	k1gInSc2
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Collinské	Collinský	k2eAgFnSc2d1
brány	brána	k1gFnSc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
ukončil	ukončit	k5eAaPmAgMnS
jejich	jejich	k3xOp3gFnSc4
vládu	vláda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
se	se	k3xPyFc4
obdařil	obdařit	k5eAaPmAgMnS
starodávnou	starodávný	k2eAgFnSc7d1
funkcí	funkce	k1gFnSc7
diktátora	diktátor	k1gMnSc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
tradičního	tradiční	k2eAgNnSc2d1
šestiměsíčního	šestiměsíční	k2eAgNnSc2d1
funkčního	funkční	k2eAgNnSc2d1
období	období	k1gNnSc2
nebylo	být	k5eNaImAgNnS
Sullově	Sullův	k2eAgFnSc3d1
diktatuře	diktatura	k1gFnSc3
stanoveno	stanovit	k5eAaPmNgNnS
žádné	žádný	k3yNgNnSc1
časové	časový	k2eAgNnSc1d1
omezení	omezení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovaly	následovat	k5eAaImAgFnP
krvavé	krvavý	k2eAgFnPc1d1
proskripce	proskripce	k1gFnPc1
(	(	kIx(
<g/>
seznamy	seznam	k1gInPc1
nepřátel	nepřítel	k1gMnPc2
státu	stát	k1gInSc2
hodných	hodný	k2eAgInPc2d1
trestu	trest	k1gInSc3
smrti	smrt	k1gFnSc2
<g/>
)	)	kIx)
namířené	namířený	k2eAgFnSc2d1
proti	proti	k7c3
Sullovým	Sullův	k2eAgMnPc3d1
politickým	politický	k2eAgMnPc3d1
nepřátelům	nepřítel	k1gMnPc3
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
překonaly	překonat	k5eAaPmAgFnP
dokonce	dokonce	k9
i	i	k9
Mariovy	Mariův	k2eAgFnPc4d1
čistky	čistka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mariovy	Mariův	k2eAgFnPc1d1
sochy	socha	k1gFnPc1
a	a	k8xC
obrazy	obraz	k1gInPc1
byly	být	k5eAaImAgInP
zničeny	zničen	k2eAgInPc4d1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInPc1
ostatky	ostatek	k1gInPc1
byly	být	k5eAaImAgInP
vytaženy	vytáhnout	k5eAaPmNgInP
z	z	k7c2
hrobu	hrob	k1gInSc2
a	a	k8xC
vhozeny	vhozen	k2eAgFnPc1d1
do	do	k7c2
Tiberu	Tiber	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesar	Caesar	k1gMnSc1
<g/>
,	,	kIx,
jakožto	jakožto	k8xS
Mariův	Mariův	k2eAgMnSc1d1
synovec	synovec	k1gMnSc1
a	a	k8xC
zeť	zeť	k1gMnSc1
Cinny	Cinna	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgInS
zabit	zabít	k5eAaPmNgMnS
již	již	k9
dříve	dříve	k6eAd2
během	během	k7c2
vzpoury	vzpoura	k1gFnSc2
vlastních	vlastní	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
nyní	nyní	k6eAd1
nacházel	nacházet	k5eAaImAgMnS
ve	v	k7c6
smrtelném	smrtelný	k2eAgNnSc6d1
ohrožení	ohrožení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
kněžského	kněžský	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
byl	být	k5eAaImAgMnS
zbaven	zbaven	k2eAgMnSc1d1
také	také	k9
svého	svůj	k3xOyFgNnSc2
dědictví	dědictví	k1gNnSc2
a	a	k8xC
věna	věno	k1gNnSc2
své	svůj	k3xOyFgFnSc2
manželky	manželka	k1gFnSc2
<g/>
,	,	kIx,
přesto	přesto	k8xC
se	se	k3xPyFc4
odmítl	odmítnout	k5eAaPmAgMnS
dát	dát	k5eAaPmF
rozvést	rozvést	k5eAaPmF
s	s	k7c7
Cornelií	Cornelie	k1gFnSc7
<g/>
,	,	kIx,
načež	načež	k6eAd1
byl	být	k5eAaImAgMnS
nucen	nutit	k5eAaImNgMnS
uchýlit	uchýlit	k5eAaPmF
se	se	k3xPyFc4
do	do	k7c2
ústraní	ústraní	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
zásah	zásah	k1gInSc4
ze	z	k7c2
strany	strana	k1gFnSc2
rodiny	rodina	k1gFnSc2
jeho	jeho	k3xOp3gFnSc2
matky	matka	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
podporovala	podporovat	k5eAaImAgFnS
Sullu	Sulla	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
panenských	panenský	k2eAgFnPc2d1
kněžek	kněžka	k1gFnPc2
vestálek	vestálka	k1gFnPc2
mu	on	k3xPp3gMnSc3
zachránil	zachránit	k5eAaPmAgInS
život	život	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sulla	Sulla	k1gFnSc1
mu	on	k3xPp3gMnSc3
údajně	údajně	k6eAd1
jen	jen	k6eAd1
velmi	velmi	k6eAd1
neochotně	ochotně	k6eNd1
daroval	darovat	k5eAaPmAgMnS
milost	milost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
té	ten	k3xDgFnSc6
příležitosti	příležitost	k1gFnSc6
prý	prý	k9
jeho	jeho	k3xOp3gFnSc4
přímluvce	přímluvce	k1gMnSc1
varoval	varovat	k5eAaImAgMnS
<g/>
,	,	kIx,
když	když	k8xS
prozíravě	prozíravě	k6eAd1
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Vždyť	vždyť	k9
v	v	k7c6
Caesarovi	Caesar	k1gMnSc6
je	být	k5eAaImIp3nS
skryto	skrýt	k5eAaPmNgNnS
mnoho	mnoho	k4c1
Mariů	Mario	k1gMnPc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
</s>
<s>
Počátky	počátek	k1gInPc1
kariéry	kariéra	k1gFnSc2
</s>
<s>
Caesar	Caesar	k1gMnSc1
se	se	k3xPyFc4
však	však	k9
nevrátil	vrátit	k5eNaPmAgMnS
do	do	k7c2
Říma	Řím	k1gInSc2
<g/>
,	,	kIx,
místo	místo	k7c2
toho	ten	k3xDgNnSc2
se	se	k3xPyFc4
připojil	připojit	k5eAaPmAgMnS
k	k	k7c3
vojsku	vojsko	k1gNnSc3
v	v	k7c6
Asii	Asie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
obléhání	obléhání	k1gNnSc2
města	město	k1gNnSc2
Mytilény	Mytiléna	k1gFnSc2
byl	být	k5eAaImAgMnS
Caesar	Caesar	k1gMnSc1
vyslán	vyslat	k5eAaPmNgMnS
do	do	k7c2
Bithýnie	Bithýnie	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
požádal	požádat	k5eAaPmAgMnS
místního	místní	k2eAgMnSc4d1
krále	král	k1gMnSc4
Níkoméda	Níkoméd	k1gMnSc4
o	o	k7c4
podporu	podpora	k1gFnSc4
jeho	jeho	k3xOp3gFnSc2
flotily	flotila	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Níkomédově	Níkomédův	k2eAgInSc6d1
dvoře	dvůr	k1gInSc6
strávil	strávit	k5eAaPmAgMnS
poté	poté	k6eAd1
hodně	hodně	k6eAd1
času	čas	k1gInSc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
zavdal	zavdat	k5eAaPmAgMnS
příčinu	příčina	k1gFnSc4
ke	k	k7c3
vzniku	vznik	k1gInSc3
klepům	klep	k1gInPc3
o	o	k7c6
homosexuálním	homosexuální	k2eAgNnSc6d1
vztahu	vztah	k1gInSc6
s	s	k7c7
králem	král	k1gMnSc7
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
ho	on	k3xPp3gMnSc2
měly	mít	k5eAaImAgFnP
pronásledovat	pronásledovat	k5eAaImF
až	až	k9
do	do	k7c2
konce	konec	k1gInSc2
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
při	při	k7c6
závěrečném	závěrečný	k2eAgInSc6d1
útoku	útok	k1gInSc6
na	na	k7c4
Mytilénu	Mytiléna	k1gFnSc4
si	se	k3xPyFc3
Caesar	Caesar	k1gMnSc1
počínal	počínat	k5eAaImAgMnS
natolik	natolik	k6eAd1
udatně	udatně	k6eAd1
a	a	k8xC
odvážně	odvážně	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
vysloužil	vysloužit	k5eAaPmAgMnS
občanskou	občanský	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
(	(	kIx(
<g/>
corona	corona	k1gFnSc1
civica	civica	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
vyznamenání	vyznamenání	k1gNnSc1
náleželo	náležet	k5eAaImAgNnS
tomu	ten	k3xDgMnSc3
Římanovi	Říman	k1gMnSc3
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
v	v	k7c6
bitvě	bitva	k1gFnSc6
zachránil	zachránit	k5eAaPmAgMnS
život	život	k1gInSc4
svému	svůj	k3xOyFgMnSc3
spoluobčanovi	spoluobčan	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
se	se	k3xPyFc4
zúčastnil	zúčastnit	k5eAaPmAgMnS
rovněž	rovněž	k9
tažení	tažení	k1gNnSc4
proti	proti	k7c3
pirátům	pirát	k1gMnPc3
v	v	k7c6
Kilíkii	Kilíkie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
dvou	dva	k4xCgNnPc6
letech	léto	k1gNnPc6
samovlády	samovláda	k1gFnSc2
se	se	k3xPyFc4
Sulla	Sulla	k1gMnSc1
vzdal	vzdát	k5eAaPmAgMnS
úřadu	úřad	k1gInSc3
diktátora	diktátor	k1gMnSc2
a	a	k8xC
obnovil	obnovit	k5eAaPmAgMnS
republikánské	republikánský	k2eAgNnSc4d1
uspořádání	uspořádání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokonce	dokonce	k9
propustil	propustit	k5eAaPmAgInS
i	i	k9
své	svůj	k3xOyFgMnPc4
liktory	liktor	k1gMnPc4
a	a	k8xC
zcela	zcela	k6eAd1
nestřežen	střežen	k2eNgMnSc1d1
se	se	k3xPyFc4
procházel	procházet	k5eAaImAgMnS
po	po	k7c6
fóru	fórum	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
zemřel	zemřít	k5eAaPmAgMnS
a	a	k8xC
byl	být	k5eAaImAgMnS
mu	on	k3xPp3gMnSc3
vystrojen	vystrojen	k2eAgInSc4d1
státní	státní	k2eAgInSc4d1
pohřeb	pohřeb	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
Caesar	Caesar	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
78	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
doslechl	doslechnout	k5eAaPmAgMnS
o	o	k7c6
Sullově	Sullův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
<g/>
,	,	kIx,
cítil	cítit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
už	už	k6eAd1
nic	nic	k3yNnSc1
nebrání	bránit	k5eNaImIp3nS
jeho	jeho	k3xOp3gInSc3
bezpečnému	bezpečný	k2eAgInSc3d1
návratu	návrat	k1gInSc3
do	do	k7c2
Říma	Řím	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
důvodu	důvod	k1gInSc2
nedostatku	nedostatek	k1gInSc2
prostředků	prostředek	k1gInPc2
si	se	k3xPyFc3
pořídil	pořídit	k5eAaPmAgMnS
skromný	skromný	k2eAgInSc4d1
dům	dům	k1gInSc4
ve	v	k7c6
čtvrti	čtvrt	k1gFnSc6
Subura	Subura	k1gFnSc1
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc7
obývali	obývat	k5eAaImAgMnP
méně	málo	k6eAd2
majetní	majetný	k2eAgMnPc1d1
Římané	Říman	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc1
příchod	příchod	k1gInSc1
se	se	k3xPyFc4
časově	časově	k6eAd1
kryl	krýt	k5eAaImAgMnS
s	s	k7c7
pokusem	pokus	k1gInSc7
Marca	Marcus	k1gMnSc2
Aemilia	Aemilius	k1gMnSc2
Lepida	Lepid	k1gMnSc2
o	o	k7c4
převrat	převrat	k1gInSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
Caesar	Caesar	k1gMnSc1
se	se	k3xPyFc4
této	tento	k3xDgFnSc2
akce	akce	k1gFnSc2
neúčastnil	účastnit	k5eNaImAgMnS
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
postrádal	postrádat	k5eAaImAgMnS
důvěru	důvěra	k1gFnSc4
v	v	k7c4
Lepidovo	Lepidův	k2eAgNnSc4d1
vedení	vedení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Raději	rád	k6eAd2
se	se	k3xPyFc4
vydal	vydat	k5eAaPmAgInS
na	na	k7c4
dráhu	dráha	k1gFnSc4
veřejného	veřejný	k2eAgMnSc2d1
obhájce	obhájce	k1gMnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
vznesl	vznést	k5eAaPmAgMnS
neúspěšnou	úspěšný	k2eNgFnSc4d1
žalobu	žaloba	k1gFnSc4
proti	proti	k7c3
Sullovu	Sullův	k2eAgMnSc3d1
příznivci	příznivec	k1gMnSc3
Gnaeu	Gnae	k1gMnSc3
Corneliu	Cornelius	k1gMnSc3
Dolabellovi	Dolabell	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
spor	spor	k1gInSc1
prohrál	prohrát	k5eAaPmAgInS
<g/>
,	,	kIx,
vešel	vejít	k5eAaPmAgInS
ve	v	k7c4
známost	známost	k1gFnSc4
pro	pro	k7c4
své	svůj	k3xOyFgNnSc4
výjimečné	výjimečný	k2eAgFnPc1d1
řečnické	řečnický	k2eAgFnPc1d1
schopnosti	schopnost	k1gFnPc1
a	a	k8xC
rovněž	rovněž	k9
kvůli	kvůli	k7c3
nelítostnému	lítostný	k2eNgNnSc3d1
stíhání	stíhání	k1gNnSc3
bývalých	bývalý	k2eAgMnPc2d1
správců	správce	k1gMnPc2
provincií	provincie	k1gFnPc2
smutně	smutně	k6eAd1
proslulých	proslulý	k2eAgInPc2d1
svým	svůj	k3xOyFgNnSc7
vydíráním	vydírání	k1gNnSc7
a	a	k8xC
zkorumpovaností	zkorumpovanost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokonce	dokonce	k9
samotný	samotný	k2eAgMnSc1d1
Marcus	Marcus	k1gMnSc1
Tullius	Tullius	k1gMnSc1
Cicero	Cicero	k1gMnSc1
<g/>
,	,	kIx,
největší	veliký	k2eAgMnSc1d3
tehdejší	tehdejší	k2eAgMnSc1d1
rétor	rétor	k1gMnSc1
<g/>
,	,	kIx,
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Najde	najít	k5eAaPmIp3nS
se	se	k3xPyFc4
někdo	někdo	k3yInSc1
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
ovládá	ovládat	k5eAaImIp3nS
lépe	dobře	k6eAd2
než	než	k8xS
Caesar	Caesar	k1gMnSc1
schopnost	schopnost	k1gFnSc4
mluvit	mluvit	k5eAaImF
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
Aby	aby	kYmCp3nS
ještě	ještě	k6eAd1
zdokonalil	zdokonalit	k5eAaPmAgMnS
své	svůj	k3xOyFgNnSc4
řečnické	řečnický	k2eAgNnSc4d1
umění	umění	k1gNnSc4
<g/>
,	,	kIx,
odcestoval	odcestovat	k5eAaPmAgMnS
Caesar	Caesar	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
75	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
na	na	k7c4
Rhodos	Rhodos	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
studoval	studovat	k5eAaImAgMnS
filosofii	filosofie	k1gFnSc4
a	a	k8xC
rétoriku	rétorika	k1gFnSc4
u	u	k7c2
slavného	slavný	k2eAgMnSc2d1
učitele	učitel	k1gMnSc2
Apollónia	Apollónium	k1gNnSc2
Molóna	Molón	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
dříve	dříve	k6eAd2
vyučoval	vyučovat	k5eAaImAgMnS
i	i	k9
Cicerona	Cicero	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
plavby	plavba	k1gFnSc2
Egejským	egejský	k2eAgNnSc7d1
mořem	moře	k1gNnSc7
byl	být	k5eAaImAgMnS
však	však	k8xC
Caesar	Caesar	k1gMnSc1
unesen	unést	k5eAaPmNgMnS
kilikickými	kilikický	k2eAgMnPc7d1
piráty	pirát	k1gMnPc7
a	a	k8xC
následně	následně	k6eAd1
byl	být	k5eAaImAgMnS
jimi	on	k3xPp3gNnPc7
držen	držet	k5eAaImNgMnS
jako	jako	k9
vězeň	vězeň	k1gMnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
Dodekanéského	Dodekanéský	k2eAgNnSc2d1
souostroví	souostroví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajetí	zajetí	k1gNnSc1
ho	on	k3xPp3gMnSc4
ale	ale	k8xC
nijak	nijak	k6eAd1
nepřipravilo	připravit	k5eNaPmAgNnS
o	o	k7c4
jeho	jeho	k3xOp3gFnSc4
nezdolné	zdolný	k2eNgNnSc1d1
sebevědomí	sebevědomí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
za	za	k7c4
něho	on	k3xPp3gMnSc4
piráti	pirát	k1gMnPc1
požadovali	požadovat	k5eAaImAgMnP
výkupné	výkupné	k1gNnSc4
dvacet	dvacet	k4xCc4
talentů	talent	k1gInPc2
zlata	zlato	k1gNnSc2
<g/>
,	,	kIx,
naléhal	naléhat	k5eAaBmAgMnS,k5eAaImAgMnS
na	na	k7c4
ně	on	k3xPp3gNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
si	se	k3xPyFc3
řekli	říct	k5eAaPmAgMnP
o	o	k7c4
padesát	padesát	k4xCc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
propuštění	propuštění	k1gNnSc6
na	na	k7c4
svobodu	svoboda	k1gFnSc4
shromáždil	shromáždit	k5eAaPmAgMnS
Caesar	Caesar	k1gMnSc1
loďstvo	loďstvo	k1gNnSc1
<g/>
,	,	kIx,
s	s	k7c7
kterým	který	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
piráty	pirát	k1gMnPc4
pronásledoval	pronásledovat	k5eAaImAgMnS
a	a	k8xC
nakonec	nakonec	k6eAd1
dopadl	dopadnout	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správce	správka	k1gFnSc3
provincie	provincie	k1gFnSc2
Asie	Asie	k1gFnSc2
v	v	k7c6
Pergamu	Pergamos	k1gInSc6
je	být	k5eAaImIp3nS
odmítl	odmítnout	k5eAaPmAgMnS
popravit	popravit	k5eAaPmF
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
Caesar	Caesar	k1gMnSc1
žádal	žádat	k5eAaImAgMnS
<g/>
,	,	kIx,
a	a	k8xC
dal	dát	k5eAaPmAgMnS
přednost	přednost	k1gFnSc4
jejich	jejich	k3xOp3gFnSc3
prodeji	prodej	k1gFnSc3
do	do	k7c2
otroctví	otroctví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesar	Caesar	k1gMnSc1
se	se	k3xPyFc4
ale	ale	k9
pro	pro	k7c4
piráty	pirát	k1gMnPc4
vrátil	vrátit	k5eAaPmAgMnS
a	a	k8xC
na	na	k7c4
vlastní	vlastní	k2eAgFnSc4d1
odpovědnost	odpovědnost	k1gFnSc4
je	on	k3xPp3gMnPc4
nechal	nechat	k5eAaPmAgMnS
ukřižovat	ukřižovat	k5eAaPmF
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
jim	on	k3xPp3gMnPc3
ostatně	ostatně	k6eAd1
slíbil	slíbit	k5eAaPmAgMnS
během	během	k7c2
svého	svůj	k3xOyFgNnSc2
zajetí	zajetí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomuto	tento	k3xDgInSc3
jeho	jeho	k3xOp3gInSc3
slibu	slib	k1gInSc3
se	se	k3xPyFc4
tehdy	tehdy	k6eAd1
piráti	pirát	k1gMnPc1
vysmáli	vysmát	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
pokračoval	pokračovat	k5eAaImAgInS
na	na	k7c4
Rhodos	Rhodos	k1gInSc4
<g/>
,	,	kIx,
brzy	brzy	k6eAd1
však	však	k9
byl	být	k5eAaImAgMnS
odvolán	odvolat	k5eAaPmNgMnS
zpět	zpět	k6eAd1
do	do	k7c2
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Římě	Řím	k1gInSc6
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
vojenským	vojenský	k2eAgMnSc7d1
tribunem	tribun	k1gMnSc7
a	a	k8xC
učinil	učinit	k5eAaPmAgMnS,k5eAaImAgMnS
tak	tak	k6eAd1
první	první	k4xOgInSc4
krok	krok	k1gInSc4
své	svůj	k3xOyFgFnSc2
politické	politický	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
(	(	kIx(
<g/>
cursus	cursus	k1gInSc1
honorum	honorum	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
zuřila	zuřit	k5eAaImAgFnS
v	v	k7c6
Itálii	Itálie	k1gFnSc6
válka	válka	k1gFnSc1
se	s	k7c7
Spartakem	Spartak	k1gInSc7
(	(	kIx(
<g/>
73	#num#	k4
až	až	k9
71	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
ale	ale	k9
známo	znám	k2eAgNnSc1d1
jakou	jaký	k3yRgFnSc7,k3yQgFnSc7,k3yIgFnSc7
<g/>
,	,	kIx,
pokud	pokud	k8xS
vůbec	vůbec	k9
nějakou	nějaký	k3yIgFnSc4
<g/>
,	,	kIx,
úlohu	úloha	k1gFnSc4
v	v	k7c6
ní	on	k3xPp3gFnSc6
Caesar	Caesar	k1gMnSc1
sehrál	sehrát	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
quaestorem	quaestor	k1gMnSc7
pro	pro	k7c4
rok	rok	k1gInSc4
69	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
a	a	k8xC
v	v	k7c6
tomto	tento	k3xDgInSc6
roce	rok	k1gInSc6
pronesl	pronést	k5eAaPmAgMnS
smuteční	smuteční	k2eAgFnSc4d1
řeč	řeč	k1gFnSc4
za	za	k7c4
svoji	svůj	k3xOyFgFnSc4
tetu	teta	k1gFnSc4
Julii	Julie	k1gFnSc4
<g/>
,	,	kIx,
vdovu	vdova	k1gFnSc4
po	po	k7c6
Mariovi	Mario	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
smutečním	smuteční	k2eAgNnSc6d1
procesí	procesí	k1gNnSc6
byly	být	k5eAaImAgInP
neseny	nést	k5eAaImNgInP
zakázané	zakázaný	k2eAgInPc1d1
Mariovy	Mariův	k2eAgInPc1d1
obrazy	obraz	k1gInPc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
byla	být	k5eAaImAgFnS
od	od	k7c2
Sullova	Sullův	k2eAgInSc2d1
nástupu	nástup	k1gInSc2
věc	věc	k1gFnSc1
vskutku	vskutku	k9
nevídaná	vídaný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
manželka	manželka	k1gFnSc1
Cornelia	Cornelia	k1gFnSc1
zemřela	zemřít	k5eAaPmAgFnS
v	v	k7c6
témže	týž	k3xTgInSc6
roce	rok	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesar	Caesar	k1gMnSc1
pak	pak	k9
odešel	odejít	k5eAaPmAgMnS
sloužit	sloužit	k5eAaImF
do	do	k7c2
Hispánie	Hispánie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Suetonia	Suetonium	k1gNnSc2
spatřil	spatřit	k5eAaPmAgMnS
v	v	k7c6
Gadesu	Gades	k1gInSc6
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgInSc4d1
Cádiz	Cádiz	k1gInSc4
<g/>
)	)	kIx)
sochu	socha	k1gFnSc4
Alexandra	Alexandr	k1gMnSc2
Velikého	veliký	k2eAgMnSc2d1
<g/>
,	,	kIx,
přitom	přitom	k6eAd1
si	se	k3xPyFc3
prý	prý	k9
posteskl	postesknout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
dosáhl	dosáhnout	k5eAaPmAgMnS
věku	věk	k1gInSc3
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Alexandrovi	Alexandrův	k2eAgMnPc1d1
už	už	k6eAd1
ležel	ležet	k5eAaImAgMnS
u	u	k7c2
nohou	noha	k1gFnPc2
celý	celý	k2eAgInSc4d1
svět	svět	k1gInSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
on	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
dosud	dosud	k6eAd1
nic	nic	k3yNnSc1
nedokázal	dokázat	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
návratu	návrat	k1gInSc6
z	z	k7c2
Hispánie	Hispánie	k1gFnSc2
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Pompeiou	Pompeia	k1gFnSc7
<g/>
,	,	kIx,
Sullovou	Sullův	k2eAgFnSc7d1
vnučkou	vnučka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zvolení	zvolení	k1gNnSc6
aedilem	aedil	k1gInSc7
nechal	nechat	k5eAaPmAgInS
obnovit	obnovit	k5eAaPmF
pomníky	pomník	k1gInPc4
Mariových	Mariův	k2eAgFnPc2d1
vítězství	vítězství	k1gNnSc4
–	–	k?
šlo	jít	k5eAaImAgNnS
o	o	k7c4
velice	velice	k6eAd1
kontroverzní	kontroverzní	k2eAgInSc4d1
čin	čin	k1gInSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
moc	moc	k6eAd1
v	v	k7c6
Římě	Řím	k1gInSc6
stále	stále	k6eAd1
drželi	držet	k5eAaImAgMnP
někdejší	někdejší	k2eAgMnPc1d1
Sullovi	Sullův	k2eAgMnPc1d1
straníci	straník	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
vznesl	vznést	k5eAaPmAgMnS
několik	několik	k4yIc4
žalob	žaloba	k1gFnPc2
proti	proti	k7c3
občanům	občan	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
obohatili	obohatit	k5eAaPmAgMnP
na	na	k7c6
majetku	majetek	k1gInSc6
proskribovaných	proskribovaný	k2eAgFnPc2d1
a	a	k8xC
vynaložil	vynaložit	k5eAaPmAgMnS
velkou	velký	k2eAgFnSc4d1
část	část	k1gFnSc4
peněz	peníze	k1gInPc2
půjčených	půjčený	k2eAgInPc2d1
převážně	převážně	k6eAd1
od	od	k7c2
Crassa	Crass	k1gMnSc2
na	na	k7c4
veřejné	veřejný	k2eAgFnPc4d1
práce	práce	k1gFnPc4
a	a	k8xC
hry	hra	k1gFnPc4
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
zastínil	zastínit	k5eAaPmAgMnS
svého	svůj	k3xOyFgMnSc4
kolegu	kolega	k1gMnSc4
v	v	k7c6
úřadě	úřad	k1gInSc6
Marca	Marcus	k1gMnSc2
Calpurnia	Calpurnium	k1gNnSc2
Bibula	Bibul	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Caesar	Caesar	k1gMnSc1
se	se	k3xPyFc4
dere	drát	k5eAaImIp3nS
do	do	k7c2
popředí	popředí	k1gNnSc2
</s>
<s>
Rok	rok	k1gInSc1
63	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
byl	být	k5eAaImAgMnS
pro	pro	k7c4
Caesara	Caesar	k1gMnSc4
rokem	rok	k1gInSc7
plným	plný	k2eAgNnSc7d1
událostí	událost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejprve	nejprve	k6eAd1
přesvědčil	přesvědčit	k5eAaPmAgMnS
tribuna	tribun	k1gMnSc2
lidu	lid	k1gInSc2
Tita	Titus	k1gMnSc4
Labiena	Labien	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
obžaloval	obžalovat	k5eAaPmAgInS
Gaia	Gai	k2eAgNnPc4d1
Rabiria	Rabirium	k1gNnPc4
<g/>
,	,	kIx,
senátora	senátor	k1gMnSc4
z	z	k7c2
řad	řada	k1gFnPc2
optimátů	optimát	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
politické	politický	k2eAgFnSc2d1
vraždy	vražda	k1gFnSc2
tribuna	tribun	k1gMnSc2
Lucia	Lucius	k1gMnSc2
Appuleia	Appuleius	k1gMnSc2
Saturnina	Saturnin	k1gMnSc2
<g/>
,	,	kIx,
a	a	k8xC
aby	aby	kYmCp3nS
jeho	on	k3xPp3gMnSc4,k3xOp3gMnSc4
samotného	samotný	k2eAgMnSc4d1
určil	určit	k5eAaPmAgInS
za	za	k7c4
jednoho	jeden	k4xCgMnSc4
ze	z	k7c2
dvou	dva	k4xCgMnPc2
soudců	soudce	k1gMnPc2
v	v	k7c6
řízení	řízení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rabiria	Rabirium	k1gNnSc2
obhajovali	obhajovat	k5eAaImAgMnP
Cicero	Cicero	k1gMnSc1
a	a	k8xC
Quintus	Quintus	k1gMnSc1
Hortensius	Hortensius	k1gMnSc1
<g/>
,	,	kIx,
přesto	přesto	k8xC
byl	být	k5eAaImAgMnS
shledán	shledat	k5eAaPmNgMnS
vinným	vinný	k1gMnSc7
z	z	k7c2
velezrady	velezrada	k1gFnSc2
(	(	kIx(
<g/>
perduellio	perduellio	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
Rabirius	Rabirius	k1gInSc1
uplatňoval	uplatňovat	k5eAaImAgInS
své	svůj	k3xOyFgNnSc4
právo	právo	k1gNnSc4
na	na	k7c6
odvolání	odvolání	k1gNnSc6
se	se	k3xPyFc4
k	k	k7c3
lidu	lid	k1gInSc3
<g/>
,	,	kIx,
praetor	praetor	k1gMnSc1
Quintus	Quintus	k1gMnSc1
Caecilius	Caecilius	k1gMnSc1
Metellus	Metellus	k1gMnSc1
Celer	celer	k1gInSc4
dal	dát	k5eAaPmAgMnS
přerušit	přerušit	k5eAaPmF
shromáždění	shromáždění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
Labienus	Labienus	k1gMnSc1
mohl	moct	k5eAaImAgMnS
řízení	řízení	k1gNnSc4
odročit	odročit	k5eAaPmF
na	na	k7c4
další	další	k2eAgNnSc4d1
zasedání	zasedání	k1gNnSc4
<g/>
,	,	kIx,
neučinil	učinit	k5eNaPmAgMnS
tak	tak	k6eAd1
a	a	k8xC
Caesar	Caesar	k1gMnSc1
dosáhl	dosáhnout	k5eAaPmAgMnS
svého	svůj	k3xOyFgInSc2
cíle	cíl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Labienus	Labienus	k1gMnSc1
zůstal	zůstat	k5eAaPmAgMnS
důležitým	důležitý	k2eAgMnSc7d1
spojencem	spojenec	k1gMnSc7
Caesara	Caesar	k1gMnSc2
po	po	k7c4
celé	celý	k2eAgNnSc4d1
další	další	k2eAgNnSc4d1
desetiletí	desetiletí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedlouho	dlouho	k6eNd1
poté	poté	k6eAd1
byl	být	k5eAaImAgMnS
Caesar	Caesar	k1gMnSc1
zvolen	zvolit	k5eAaPmNgMnS
do	do	k7c2
úřadu	úřad	k1gInSc2
nejvyššího	vysoký	k2eAgMnSc2d3
velekněze	velekněz	k1gMnSc2
(	(	kIx(
<g/>
pontifex	pontifex	k1gMnSc1
maximus	maximus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystoupil	vystoupit	k5eAaPmAgMnS
přitom	přitom	k6eAd1
proti	proti	k7c3
dvěma	dva	k4xCgMnPc3
mocným	mocný	k2eAgMnPc3d1
optimátům	optimát	k1gMnPc3
a	a	k8xC
někdejším	někdejší	k2eAgMnPc3d1
konzulům	konzul	k1gMnPc3
<g/>
:	:	kIx,
Quintu	Quint	k1gMnSc3
Lutatiu	Lutatius	k1gMnSc3
Catulovi	Catul	k1gMnSc3
a	a	k8xC
Publiu	Publius	k1gMnSc3
Serviliu	Servilius	k1gMnSc3
Isauricovi	Isauric	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
strany	strana	k1gFnPc1
se	se	k3xPyFc4
vzájemně	vzájemně	k6eAd1
obviňovaly	obviňovat	k5eAaImAgInP
z	z	k7c2
uplácení	uplácení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ráno	ráno	k6eAd1
v	v	k7c4
den	den	k1gInSc4
voleb	volba	k1gFnPc2
řekl	říct	k5eAaPmAgMnS
prý	prý	k9
Caesar	Caesar	k1gMnSc1
své	svůj	k3xOyFgFnSc3
matce	matka	k1gFnSc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
buď	buď	k8xC
vrátí	vrátit	k5eAaPmIp3nS
jako	jako	k9
pontifex	pontifex	k1gMnSc1
maximus	maximus	k1gMnSc1
anebo	anebo	k8xC
vůbec	vůbec	k9
ne	ne	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
neúspěchu	neúspěch	k1gInSc2
by	by	kYmCp3nS
byl	být	k5eAaImAgInS
totiž	totiž	k9
kvůli	kvůli	k7c3
svým	svůj	k3xOyFgInPc3
enormním	enormní	k2eAgInPc3d1
dluhům	dluh	k1gInPc3
nucen	nutit	k5eAaImNgMnS
odejít	odejít	k5eAaPmF
bojovat	bojovat	k5eAaImF
do	do	k7c2
provincií	provincie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
celkem	celkem	k6eAd1
jasně	jasně	k6eAd1
zvítězil	zvítězit	k5eAaPmAgMnS
<g/>
,	,	kIx,
i	i	k8xC
přes	přes	k7c4
lepší	dobrý	k2eAgNnSc4d2
postavení	postavení	k1gNnSc4
a	a	k8xC
větší	veliký	k2eAgFnPc4d2
zkušenosti	zkušenost	k1gFnPc4
svých	svůj	k3xOyFgMnPc2
oponentů	oponent	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Caesarova	Caesarův	k2eAgFnSc1d1
busta	busta	k1gFnSc1
ve	v	k7c6
vídeňském	vídeňský	k2eAgInSc6d1
Kunsthistorisches	Kunsthistorisches	k1gInSc1
Museum	museum	k1gNnSc1
</s>
<s>
Když	když	k8xS
Cicero	Cicero	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgMnS
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
konzulem	konzul	k1gMnSc7
<g/>
,	,	kIx,
odhalil	odhalit	k5eAaPmAgMnS
Catilinovo	Catilinův	k2eAgNnSc4d1
spiknutí	spiknutí	k1gNnSc4
<g/>
,	,	kIx,
Catulus	Catulus	k1gMnSc1
a	a	k8xC
mnozí	mnohý	k2eAgMnPc1d1
jiní	jiný	k2eAgMnPc1d1
nařkli	nařknout	k5eAaPmAgMnP
Caesara	Caesar	k1gMnSc4
ze	z	k7c2
zapojení	zapojení	k1gNnSc2
do	do	k7c2
komplotu	komplot	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesar	Caesar	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
byl	být	k5eAaImAgInS
právě	právě	k6eAd1
zvolen	zvolen	k2eAgInSc1d1
praetorem	praetor	k1gInSc7
na	na	k7c4
další	další	k2eAgInSc4d1
rok	rok	k1gInSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
zúčastnil	zúčastnit	k5eAaPmAgInS
debaty	debata	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
senátoři	senátor	k1gMnPc1
zabývali	zabývat	k5eAaImAgMnP
způsobem	způsob	k1gInSc7
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
vypořádat	vypořádat	k5eAaPmF
se	s	k7c7
spiklenci	spiklenec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Probíhající	probíhající	k2eAgFnSc6d1
diskuzi	diskuze	k1gFnSc6
však	však	k9
nevěnoval	věnovat	k5eNaImAgMnS
zvláštní	zvláštní	k2eAgFnSc4d1
pozornost	pozornost	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
četl	číst	k5eAaImAgMnS
milostný	milostný	k2eAgInSc4d1
dopis	dopis	k1gInSc4
od	od	k7c2
Catonovy	Catonův	k2eAgFnSc2d1
sestry	sestra	k1gFnSc2
Servilie	Servilie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marcus	Marcus	k1gMnSc1
Porcius	Porcius	k1gMnSc1
Cato	Cato	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Caesarovým	Caesarův	k2eAgMnSc7d1
nesmiřitelným	smiřitelný	k2eNgMnSc7d1
odpůrcem	odpůrce	k1gMnSc7
<g/>
,	,	kIx,
ho	on	k3xPp3gMnSc4
přitom	přitom	k6eAd1
obvinil	obvinit	k5eAaPmAgMnS
z	z	k7c2
udržování	udržování	k1gNnSc2
korespondence	korespondence	k1gFnSc2
se	s	k7c7
spiklenci	spiklenec	k1gMnPc7
a	a	k8xC
žádal	žádat	k5eAaImAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
inkriminované	inkriminovaný	k2eAgInPc1d1
dopisy	dopis	k1gInPc1
byly	být	k5eAaImAgInP
nahlas	nahlas	k6eAd1
předčítány	předčítán	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesar	Caesar	k1gMnSc1
snadno	snadno	k6eAd1
odolal	odolat	k5eAaPmAgMnS
jeho	jeho	k3xOp3gInPc3
útokům	útok	k1gInPc3
a	a	k8xC
přesvědčivě	přesvědčivě	k6eAd1
se	se	k3xPyFc4
zasazoval	zasazovat	k5eAaImAgMnS
proti	proti	k7c3
trestu	trest	k1gInSc3
smrti	smrt	k1gFnSc2
pro	pro	k7c4
spiklence	spiklenec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Catonova	Catonův	k2eAgFnSc1d1
řeč	řeč	k1gFnSc1
se	se	k3xPyFc4
ale	ale	k9
ukázala	ukázat	k5eAaPmAgFnS
být	být	k5eAaImF
rozhodující	rozhodující	k2eAgFnSc1d1
a	a	k8xC
Catilina	Catilina	k1gFnSc1
i	i	k8xC
jeho	jeho	k3xOp3gMnPc1
stoupenci	stoupenec	k1gMnPc1
byli	být	k5eAaImAgMnP
popraveni	popravit	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
své	svůj	k3xOyFgFnSc2
praetury	praetura	k1gFnSc2
byl	být	k5eAaImAgMnS
Caesar	Caesar	k1gMnSc1
podporován	podporovat	k5eAaImNgMnS
tribunem	tribun	k1gMnSc7
Caeciliem	Caecilium	k1gNnSc7
Metellem	Metell	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
však	však	k9
předkládali	předkládat	k5eAaImAgMnP
natolik	natolik	k6eAd1
kontroverzní	kontroverzní	k2eAgInPc4d1
návrhy	návrh	k1gInPc4
<g/>
,	,	kIx,
až	až	k9
byli	být	k5eAaImAgMnP
senátem	senát	k1gInSc7
zbaveni	zbavit	k5eAaPmNgMnP
úřadů	úřad	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
srocení	srocení	k1gNnSc4
lidu	lid	k1gInSc2
na	na	k7c4
podporu	podpora	k1gFnSc4
obou	dva	k4xCgFnPc2
přimělo	přimět	k5eAaPmAgNnS
senát	senát	k1gInSc1
k	k	k7c3
jejich	jejich	k3xOp3gNnSc3
opětnému	opětný	k2eAgNnSc3d1
dosazení	dosazení	k1gNnSc3
do	do	k7c2
úřadu	úřad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
byla	být	k5eAaImAgFnS
zřízena	zřízen	k2eAgFnSc1d1
komise	komise	k1gFnSc1
k	k	k7c3
vyšetření	vyšetření	k1gNnSc3
Catilinova	Catilinův	k2eAgNnSc2d1
spiknutí	spiknutí	k1gNnSc2
a	a	k8xC
Caesarovi	Caesarův	k2eAgMnPc1d1
byla	být	k5eAaImAgFnS
opět	opět	k6eAd1
předhazována	předhazován	k2eAgFnSc1d1
spoluúčast	spoluúčast	k1gFnSc1
na	na	k7c6
celém	celý	k2eAgInSc6d1
podniku	podnik	k1gInSc6
<g/>
,	,	kIx,
znovu	znovu	k6eAd1
mu	on	k3xPp3gNnSc3
ale	ale	k9
nebylo	být	k5eNaImAgNnS
nic	nic	k6eAd1
prokázáno	prokázat	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
se	se	k3xPyFc4
v	v	k7c6
Caesarově	Caesarův	k2eAgInSc6d1
domě	dům	k1gInSc6
konal	konat	k5eAaImAgInS
festival	festival	k1gInSc1
Bona	bona	k1gFnSc1
Dea	Dea	k1gMnSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
Dobré	dobrý	k2eAgFnSc2d1
bohyně	bohyně	k1gFnSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
se	se	k3xPyFc4
nesměl	smět	k5eNaImAgInS
účastnit	účastnit	k5eAaImF
žádný	žádný	k3yNgMnSc1
muž	muž	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mladý	mladý	k1gMnSc1
patricius	patricius	k1gMnSc1
jménem	jméno	k1gNnSc7
Publius	Publius	k1gInSc1
Clodius	Clodius	k1gInSc1
Pulcher	Pulchra	k1gFnPc2
sem	sem	k6eAd1
přesto	přesto	k8xC
v	v	k7c6
převlečení	převlečení	k1gNnSc6
za	za	k7c4
ženu	žena	k1gFnSc4
pronikl	proniknout	k5eAaPmAgInS
<g/>
,	,	kIx,
patrně	patrně	k6eAd1
s	s	k7c7
úmyslem	úmysl	k1gInSc7
svést	svést	k5eAaPmF
Caesarovu	Caesarův	k2eAgFnSc4d1
manželku	manželka	k1gFnSc4
Pompeiu	Pompeium	k1gNnSc3
<g/>
,	,	kIx,
načež	načež	k6eAd1
byl	být	k5eAaImAgMnS
chycen	chytit	k5eAaPmNgMnS
a	a	k8xC
obviněn	obvinit	k5eAaPmNgMnS
z	z	k7c2
rouhání	rouhání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesar	Caesar	k1gMnSc1
během	během	k7c2
následného	následný	k2eAgInSc2d1
soudního	soudní	k2eAgInSc2d1
procesu	proces	k1gInSc2
nepředložil	předložit	k5eNaPmAgInS
jediný	jediný	k2eAgInSc1d1
důkaz	důkaz	k1gInSc1
proti	proti	k7c3
Clodiovi	Clodius	k1gMnSc3
<g/>
,	,	kIx,
protože	protože	k8xS
si	se	k3xPyFc3
nechtěl	chtít	k5eNaImAgMnS
znepřátelit	znepřátelit	k5eAaPmF
jednu	jeden	k4xCgFnSc4
z	z	k7c2
nejmocnějších	mocný	k2eAgFnPc2d3
patricijských	patricijský	k2eAgFnPc2d1
rodin	rodina	k1gFnPc2
v	v	k7c6
Římě	Řím	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Clodius	Clodius	k1gInSc1
byl	být	k5eAaImAgInS
díky	díky	k7c3
bezuzdnému	bezuzdný	k2eAgNnSc3d1
uplácení	uplácení	k1gNnSc3
a	a	k8xC
zastrašování	zastrašování	k1gNnSc4
svědků	svědek	k1gMnPc2
i	i	k8xC
soudců	soudce	k1gMnPc2
posléze	posléze	k6eAd1
osvobozen	osvobodit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
Caesar	Caesar	k1gMnSc1
se	se	k3xPyFc4
s	s	k7c7
Pompeiou	Pompeia	k1gMnSc7
záhy	záhy	k6eAd1
rozvedl	rozvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
neboť	neboť	k8xC
jak	jak	k6eAd1
řekl	říct	k5eAaPmAgMnS
„	„	k?
<g/>
na	na	k7c6
mé	můj	k3xOp1gFnSc6
ženě	žena	k1gFnSc6
nesmí	smět	k5eNaImIp3nS
ulpět	ulpět	k5eAaPmF
nejmenší	malý	k2eAgNnSc4d3
podezření	podezření	k1gNnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
skončení	skončení	k1gNnSc6
praetury	praetura	k1gFnSc2
byl	být	k5eAaImAgMnS
Caesar	Caesar	k1gMnSc1
ustanoven	ustanovit	k5eAaPmNgMnS
správcem	správce	k1gMnSc7
provincie	provincie	k1gFnSc2
Hispania	Hispanium	k1gNnSc2
Ulterior	Ulteriora	k1gFnPc2
(	(	kIx(
<g/>
„	„	k?
<g/>
Vzdálenější	vzdálený	k2eAgFnSc2d2
Hispánie	Hispánie	k1gFnSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
avšak	avšak	k8xC
stále	stále	k6eAd1
byl	být	k5eAaImAgInS
velmi	velmi	k6eAd1
zadlužený	zadlužený	k2eAgInSc1d1
a	a	k8xC
předtím	předtím	k6eAd1
<g/>
,	,	kIx,
než	než	k8xS
mohl	moct	k5eAaImAgMnS
odejít	odejít	k5eAaPmF
<g/>
,	,	kIx,
musel	muset	k5eAaImAgMnS
vyhovět	vyhovět	k5eAaPmF
svým	svůj	k3xOyFgMnPc3
věřitelům	věřitel	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obrátil	obrátit	k5eAaPmAgMnS
se	se	k3xPyFc4
tudíž	tudíž	k8xC
na	na	k7c4
Marca	Marcum	k1gNnPc4
Licinia	Licinium	k1gNnSc2
Crassa	Crass	k1gMnSc2
<g/>
,	,	kIx,
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
nejbohatších	bohatý	k2eAgMnPc2d3
mužů	muž	k1gMnPc2
Říma	Řím	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
oplátku	oplátka	k1gFnSc4
za	za	k7c4
Caesarovu	Caesarův	k2eAgFnSc4d1
politickou	politický	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
proti	proti	k7c3
Pompeiovi	Pompeius	k1gMnSc3
<g/>
,	,	kIx,
zaplatil	zaplatit	k5eAaPmAgMnS
Crassus	Crassus	k1gMnSc1
část	část	k1gFnSc1
z	z	k7c2
Caesarových	Caesarových	k2eAgInPc2d1
dluhů	dluh	k1gInPc2
a	a	k8xC
za	za	k7c4
zbylé	zbylý	k2eAgInPc4d1
se	se	k3xPyFc4
zaručil	zaručit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
musel	muset	k5eAaImAgMnS
Caesar	Caesar	k1gMnSc1
odejít	odejít	k5eAaPmF
do	do	k7c2
provincie	provincie	k1gFnSc2
ještě	ještě	k6eAd1
před	před	k7c7
uplynutím	uplynutí	k1gNnSc7
svého	svůj	k3xOyFgNnSc2
funkčního	funkční	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
mu	on	k3xPp3gNnSc3
hrozila	hrozit	k5eAaImAgFnS
žaloba	žaloba	k1gFnSc1
pro	pro	k7c4
neplacení	neplacení	k1gNnSc4
dluhů	dluh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Hispánii	Hispánie	k1gFnSc6
si	se	k3xPyFc3
Caesar	Caesar	k1gMnSc1
počínal	počínat	k5eAaImAgMnS
velice	velice	k6eAd1
zdatně	zdatně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc1
agresivní	agresivní	k2eAgNnPc1d1
válečná	válečný	k2eAgNnPc1d1
tažení	tažení	k1gNnPc1
proti	proti	k7c3
Iberům	Iber	k1gMnPc3
na	na	k7c6
severu	sever	k1gInSc6
dnešního	dnešní	k2eAgNnSc2d1
Portugalska	Portugalsko	k1gNnSc2
posílily	posílit	k5eAaPmAgInP
jeho	jeho	k3xOp3gFnPc4
renomé	renomé	k1gNnSc2
schopného	schopný	k2eAgMnSc4d1
velitele	velitel	k1gMnSc4
a	a	k8xC
získaná	získaný	k2eAgFnSc1d1
kořist	kořist	k1gFnSc1
mu	on	k3xPp3gMnSc3
umožnila	umožnit	k5eAaPmAgFnS
uhradit	uhradit	k5eAaPmF
alespoň	alespoň	k9
část	část	k1gFnSc4
ze	z	k7c2
svých	svůj	k3xOyFgInPc2
vysokých	vysoký	k2eAgInPc2d1
dluhů	dluh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vítězství	vítězství	k1gNnSc6
nad	nad	k7c7
Lusitány	Lusitán	k1gMnPc7
a	a	k8xC
Callaici	Callaic	k1gMnPc7
byl	být	k5eAaImAgMnS
navíc	navíc	k6eAd1
svými	svůj	k3xOyFgMnPc7
vojáky	voják	k1gMnPc4
pozdraven	pozdraven	k2eAgInSc4d1
jako	jako	k8xC,k8xS
imperátor	imperátor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Tím	ten	k3xDgNnSc7
mu	on	k3xPp3gNnSc3
po	po	k7c6
návratu	návrat	k1gInSc6
do	do	k7c2
Říma	Řím	k1gInSc2
vznikl	vzniknout	k5eAaPmAgInS
nárok	nárok	k1gInSc1
na	na	k7c4
oslavu	oslava	k1gFnSc4
triumfu	triumf	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
ale	ale	k8xC
velmi	velmi	k6eAd1
usiloval	usilovat	k5eAaImAgMnS
o	o	k7c4
získání	získání	k1gNnSc4
konzulátu	konzulát	k1gInSc2
<g/>
,	,	kIx,
nejvyššího	vysoký	k2eAgInSc2d3
republikánského	republikánský	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
(	(	kIx(
<g/>
magistratus	magistratus	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
by	by	kYmCp3nS
chtěl	chtít	k5eAaImAgMnS
oslavit	oslavit	k5eAaPmF
triumf	triumf	k1gInSc4
<g/>
,	,	kIx,
musel	muset	k5eAaImAgInS
by	by	kYmCp3nP
zůstat	zůstat	k5eAaPmF
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
vojáky	voják	k1gMnPc7
mimo	mimo	k7c4
město	město	k1gNnSc4
až	až	k9
do	do	k7c2
zahájení	zahájení	k1gNnSc2
slavnosti	slavnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jestliže	jestliže	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
ale	ale	k9
chtěl	chtít	k5eAaImAgMnS
zúčastnit	zúčastnit	k5eAaPmF
voleb	volba	k1gFnPc2
<g/>
,	,	kIx,
musel	muset	k5eAaImAgMnS
by	by	kYmCp3nS
složit	složit	k5eAaPmF
velení	velení	k1gNnSc1
a	a	k8xC
vstoupit	vstoupit	k5eAaPmF
do	do	k7c2
města	město	k1gNnSc2
jako	jako	k8xC,k8xS
soukromá	soukromý	k2eAgFnSc1d1
osoba	osoba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obojí	oboj	k1gFnPc2
současně	současně	k6eAd1
bylo	být	k5eAaImAgNnS
neproveditelné	proveditelný	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Požádal	požádat	k5eAaPmAgInS
proto	proto	k8xC
senát	senát	k1gInSc1
o	o	k7c4
povolení	povolení	k1gNnSc4
kandidovat	kandidovat	k5eAaImF
v	v	k7c6
nepřítomnosti	nepřítomnost	k1gFnSc6
(	(	kIx(
<g/>
in	in	k?
absentia	absentia	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tomu	ten	k3xDgMnSc3
však	však	k9
zabránil	zabránit	k5eAaPmAgInS
Cato	Cato	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tváří	tvář	k1gFnPc2
v	v	k7c4
tvář	tvář	k1gFnSc4
výběru	výběr	k1gInSc2
mezi	mezi	k7c7
triumfem	triumf	k1gInSc7
a	a	k8xC
konzulátem	konzulát	k1gInSc7
dal	dát	k5eAaPmAgMnS
Caesar	Caesar	k1gMnSc1
nakonec	nakonec	k6eAd1
přednost	přednost	k1gFnSc4
konzulátu	konzulát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Konzul	konzul	k1gMnSc1
a	a	k8xC
triumvir	triumvir	k1gMnSc1
</s>
<s>
Vlastní	vlastní	k2eAgFnSc1d1
volba	volba	k1gFnSc1
se	se	k3xPyFc4
odehrávala	odehrávat	k5eAaImAgFnS
za	za	k7c2
velmi	velmi	k6eAd1
nedůstojných	důstojný	k2eNgFnPc2d1
podmínek	podmínka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kandidáti	kandidát	k1gMnPc1
byli	být	k5eAaImAgMnP
celkem	celkem	k6eAd1
tři	tři	k4xCgMnPc1
<g/>
:	:	kIx,
Caesar	Caesar	k1gMnSc1
<g/>
,	,	kIx,
Lucius	Lucius	k1gMnSc1
Lucceius	Lucceius	k1gMnSc1
a	a	k8xC
Marcus	Marcus	k1gMnSc1
Calpurnius	Calpurnius	k1gMnSc1
Bibulus	Bibulus	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
o	o	k7c4
několik	několik	k4yIc4
let	léto	k1gNnPc2
dříve	dříve	k6eAd2
zastával	zastávat	k5eAaImAgInS
spolu	spolu	k6eAd1
s	s	k7c7
Caesarem	Caesar	k1gMnSc7
úřad	úřad	k1gInSc4
aedila	aedít	k5eAaPmAgFnS,k5eAaImAgFnS,k5eAaBmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesar	Caesar	k1gMnSc1
se	se	k3xPyFc4
ucházel	ucházet	k5eAaImAgMnS
o	o	k7c4
podporu	podpora	k1gFnSc4
Cicerona	Cicero	k1gMnSc2
a	a	k8xC
uzavřel	uzavřít	k5eAaPmAgMnS
spojenectví	spojenectví	k1gNnSc4
s	s	k7c7
bohatým	bohatý	k2eAgMnSc7d1
Lucceiem	Lucceius	k1gMnSc7
<g/>
,	,	kIx,
ovšem	ovšem	k9
Cato	Cato	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
jinak	jinak	k6eAd1
těšil	těšit	k5eAaImAgInS
reputaci	reputace	k1gFnSc4
nezkorumpovaného	zkorumpovaný	k2eNgMnSc2d1
a	a	k8xC
poctivého	poctivý	k2eAgMnSc2d1
muže	muž	k1gMnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
uchýlil	uchýlit	k5eAaPmAgMnS
k	k	k7c3
uplácení	uplácení	k1gNnSc3
ve	v	k7c4
prospěch	prospěch	k1gInSc4
konzervativního	konzervativní	k2eAgNnSc2d1
Bibula	Bibulum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
takto	takto	k6eAd1
oboustranně	oboustranně	k6eAd1
zmanipulovaných	zmanipulovaný	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
vzešli	vzejít	k5eAaPmAgMnP
jako	jako	k9
konzulové	konzul	k1gMnPc1
pro	pro	k7c4
rok	rok	k1gInSc4
59	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Caesar	Caesar	k1gMnSc1
a	a	k8xC
Bibulus	Bibulus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Caesar	Caesar	k1gMnSc1
byl	být	k5eAaImAgMnS
již	již	k6eAd1
z	z	k7c2
dřívějška	dřívějšek	k1gInSc2
Crassovým	Crassův	k2eAgMnSc7d1
dlužníkem	dlužník	k1gMnSc7
<g/>
,	,	kIx,
to	ten	k3xDgNnSc4
mu	on	k3xPp3gMnSc3
ale	ale	k9
nebránilo	bránit	k5eNaImAgNnS
ve	v	k7c6
vyjednávání	vyjednávání	k1gNnSc6
s	s	k7c7
Pompeiem	Pompeius	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
v	v	k7c6
senátu	senát	k1gInSc6
neúspěšně	úspěšně	k6eNd1
prosazoval	prosazovat	k5eAaImAgMnS
schválení	schválení	k1gNnSc4
jím	jíst	k5eAaImIp1nS
ustaveného	ustavený	k2eAgNnSc2d1
uspořádání	uspořádání	k1gNnSc2
východních	východní	k2eAgFnPc2d1
provincií	provincie	k1gFnPc2
a	a	k8xC
přidělení	přidělení	k1gNnSc4
půdy	půda	k1gFnSc2
svým	svůj	k3xOyFgMnPc3
veteránům	veterán	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pompeius	Pompeius	k1gInSc4
a	a	k8xC
Crassus	Crassus	k1gInSc4
se	se	k3xPyFc4
nacházeli	nacházet	k5eAaImAgMnP
ve	v	k7c6
sporu	spor	k1gInSc6
už	už	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
70	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
společně	společně	k6eAd1
vykonávali	vykonávat	k5eAaImAgMnP
konzulát	konzulát	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesar	Caesar	k1gMnSc1
si	se	k3xPyFc3
byl	být	k5eAaImAgMnS
vědom	vědom	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
se	se	k3xPyFc4
dohodne	dohodnout	k5eAaPmIp3nS
s	s	k7c7
jedním	jeden	k4xCgInSc7
<g/>
,	,	kIx,
ztratí	ztratit	k5eAaPmIp3nS
podporu	podpora	k1gFnSc4
druhého	druhý	k4xOgNnSc2
<g/>
,	,	kIx,
pročež	pročež	k6eAd1
usiloval	usilovat	k5eAaImAgMnS
o	o	k7c6
usmíření	usmíření	k1gNnSc6
obou	dva	k4xCgMnPc2
rivalů	rival	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakmile	jakmile	k8xS
se	se	k3xPyFc4
tito	tento	k3xDgMnPc1
tři	tři	k4xCgMnPc1
muži	muž	k1gMnPc1
spojili	spojit	k5eAaPmAgMnP
<g/>
,	,	kIx,
disponovali	disponovat	k5eAaBmAgMnP
dostatkem	dostatek	k1gInSc7
finančních	finanční	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
a	a	k8xC
politického	politický	k2eAgInSc2d1
vlivu	vliv	k1gInSc2
k	k	k7c3
zajištění	zajištění	k1gNnSc3
úplné	úplný	k2eAgFnSc2d1
kontroly	kontrola	k1gFnSc2
nad	nad	k7c7
veřejnými	veřejný	k2eAgFnPc7d1
záležitostmi	záležitost	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInSc1
neformální	formální	k2eNgFnPc1d1
aliance	aliance	k1gFnSc1
<g/>
,	,	kIx,
známá	známý	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
první	první	k4xOgInSc1
triumvirát	triumvirát	k1gInSc1
(	(	kIx(
<g/>
vláda	vláda	k1gFnSc1
tří	tři	k4xCgMnPc2
mužů	muž	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
ještě	ještě	k9
upevněna	upevnit	k5eAaPmNgFnS
svatbou	svatba	k1gFnSc7
Pompeia	Pompeius	k1gMnSc2
s	s	k7c7
Caesarovou	Caesarův	k2eAgFnSc7d1
dcerou	dcera	k1gFnSc7
Julií	Julie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k9
Caesar	Caesar	k1gMnSc1
se	se	k3xPyFc4
opět	opět	k6eAd1
oženil	oženit	k5eAaPmAgMnS
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
s	s	k7c7
Calpurnií	Calpurnie	k1gFnSc7
<g/>
,	,	kIx,
dcerou	dcera	k1gFnSc7
Lucia	Lucius	k1gMnSc2
Calpurnia	Calpurnium	k1gNnSc2
Pisona	Pison	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
konzulem	konzul	k1gMnSc7
v	v	k7c6
následujícím	následující	k2eAgInSc6d1
roce	rok	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Veřejnost	veřejnost	k1gFnSc1
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
dozvěděla	dozvědět	k5eAaPmAgFnS
o	o	k7c6
vzniku	vznik	k1gInSc6
triumvirátu	triumvirát	k1gInSc2
<g/>
,	,	kIx,
když	když	k8xS
Caesar	Caesar	k1gMnSc1
podal	podat	k5eAaPmAgMnS
návrh	návrh	k1gInSc4
zákona	zákon	k1gInSc2
o	o	k7c4
přerozdělení	přerozdělení	k1gNnSc4
půdy	půda	k1gFnSc2
chudým	chudý	k1gMnPc3
občanům	občan	k1gMnPc3
<g/>
,	,	kIx,
načež	načež	k6eAd1
jak	jak	k8xC,k8xS
Pompeius	Pompeius	k1gInSc4
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k8xC
Crassus	Crassus	k1gInSc4
vyjádřili	vyjádřit	k5eAaPmAgMnP
s	s	k7c7
tímto	tento	k3xDgInSc7
návrhem	návrh	k1gInSc7
souhlas	souhlas	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pompeius	Pompeius	k1gMnSc1
poté	poté	k6eAd1
vyslal	vyslat	k5eAaPmAgMnS
své	svůj	k3xOyFgMnPc4
vojáky	voják	k1gMnPc4
do	do	k7c2
Říma	Řím	k1gInSc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
byli	být	k5eAaImAgMnP
političtí	politický	k2eAgMnPc1d1
oponenti	oponent	k1gMnPc1
triumvirů	triumvir	k1gMnPc2
dokonale	dokonale	k6eAd1
překvapeni	překvapit	k5eAaPmNgMnP
a	a	k8xC
zastrašeni	zastrašit	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibulus	Bibulus	k1gInSc1
v	v	k7c6
reakci	reakce	k1gFnSc6
nato	nato	k6eAd1
prohlásil	prohlásit	k5eAaPmAgMnS
znamení	znamení	k1gNnSc4
za	za	k7c4
nepříznivá	příznivý	k2eNgNnPc4d1
a	a	k8xC
zákon	zákon	k1gInSc4
tudíž	tudíž	k8xC
za	za	k7c4
neplatný	platný	k2eNgInSc4d1
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
ale	ale	k8xC
vyhnán	vyhnat	k5eAaPmNgMnS
z	z	k7c2
fóra	fórum	k1gNnSc2
Caesarovými	Caesarův	k2eAgMnPc7d1
přívrženci	přívrženec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fasces	Fasces	k1gInSc1
jeho	jeho	k3xOp3gMnPc2
liktorů	liktor	k1gMnPc2
byly	být	k5eAaImAgFnP
rozlámány	rozlámán	k2eAgFnPc1d1
<g/>
,	,	kIx,
dva	dva	k4xCgMnPc1
tribunové	tribun	k1gMnPc1
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
doprovodu	doprovod	k1gInSc6
byli	být	k5eAaImAgMnP
zraněni	zranit	k5eAaPmNgMnP
a	a	k8xC
Bibulus	Bibulus	k1gInSc4
sám	sám	k3xTgInSc4
byl	být	k5eAaImAgInS
zasažen	zasáhnout	k5eAaPmNgInS
hozenými	hozený	k2eAgInPc7d1
exkrementy	exkrement	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
strachu	strach	k1gInSc2
o	o	k7c4
život	život	k1gInSc4
se	se	k3xPyFc4
na	na	k7c4
zbytek	zbytek	k1gInSc4
funkčního	funkční	k2eAgNnSc2d1
období	období	k1gNnSc2
stáhl	stáhnout	k5eAaPmAgMnS
do	do	k7c2
svého	svůj	k3xOyFgInSc2
domu	dům	k1gInSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
příležitostně	příležitostně	k6eAd1
vydával	vydávat	k5eAaImAgInS,k5eAaPmAgInS
proklamace	proklamace	k1gFnSc2
o	o	k7c6
špatných	špatný	k2eAgNnPc6d1
znameních	znamení	k1gNnPc6
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
chtěl	chtít	k5eAaImAgMnS
Caesarovi	Caesar	k1gMnSc3
zabránit	zabránit	k5eAaPmF
ve	v	k7c6
vykonávání	vykonávání	k1gNnSc6
úřadu	úřad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
obstrukce	obstrukce	k1gFnPc1
se	se	k3xPyFc4
ale	ale	k9
ukázaly	ukázat	k5eAaPmAgFnP
být	být	k5eAaImF
zcela	zcela	k6eAd1
neúčinné	účinný	k2eNgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římští	římský	k2eAgMnPc1d1
satirikové	satirik	k1gMnPc1
potom	potom	k6eAd1
už	už	k6eAd1
navždy	navždy	k6eAd1
referovali	referovat	k5eAaBmAgMnP
o	o	k7c6
tomto	tento	k3xDgInSc6
roce	rok	k1gInSc6
jako	jako	k8xS,k8xC
o	o	k7c6
„	„	k?
<g/>
roce	rok	k1gInSc6
konzulátu	konzulát	k1gInSc2
Julia	Julius	k1gMnSc2
a	a	k8xC
Caesara	Caesar	k1gMnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
byli	být	k5eAaImAgMnP
Caesar	Caesar	k1gMnSc1
a	a	k8xC
Bibulus	Bibulus	k1gMnSc1
zvoleni	zvolen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pokusila	pokusit	k5eAaPmAgFnS
se	se	k3xPyFc4
aristokracie	aristokracie	k1gFnSc1
omezit	omezit	k5eAaPmF
budoucí	budoucí	k2eAgFnSc4d1
Caesarovu	Caesarův	k2eAgFnSc4d1
prokonzul	prokonzul	k1gMnSc1
moc	moc	k1gFnSc1
(	(	kIx(
<g/>
následující	následující	k2eAgFnSc1d1
po	po	k7c6
skončení	skončení	k1gNnSc6
konzulátu	konzulát	k1gInSc2
<g/>
)	)	kIx)
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
mu	on	k3xPp3gMnSc3
místo	místo	k1gNnSc1
správy	správa	k1gFnSc2
nějaké	nějaký	k3yIgFnSc2
provincie	provincie	k1gFnSc2
<g/>
,	,	kIx,
přidělila	přidělit	k5eAaPmAgFnS
na	na	k7c4
starost	starost	k1gFnSc4
dohled	dohled	k1gInSc1
nad	nad	k7c7
lesy	les	k1gInPc7
a	a	k8xC
pastvinami	pastvina	k1gFnPc7
Itálie	Itálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
pomocí	pomoc	k1gFnSc7
Pisona	Pisona	k1gFnSc1
a	a	k8xC
Pompeia	Pompeia	k1gFnSc1
Caesar	Caesar	k1gMnSc1
toto	tento	k3xDgNnSc4
usnesení	usnesení	k1gNnSc4
zvrátil	zvrátit	k5eAaPmAgInS
a	a	k8xC
byl	být	k5eAaImAgInS
ustaven	ustavit	k5eAaPmNgInS
za	za	k7c4
místodržitele	místodržitel	k1gMnPc4
Předalpské	předalpský	k2eAgFnSc2d1
Galie	Galie	k1gFnSc2
(	(	kIx(
<g/>
severní	severní	k2eAgFnSc2d1
Itálie	Itálie	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
Ilýrie	Ilýrie	k1gFnSc1
(	(	kIx(
<g/>
západní	západní	k2eAgInSc1d1
Balkán	Balkán	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
obdržel	obdržet	k5eAaPmAgInS
také	také	k9
správu	správa	k1gFnSc4
Zaalpské	zaalpský	k2eAgFnSc2d1
Galie	Galie	k1gFnSc2
(	(	kIx(
<g/>
jižní	jižní	k2eAgFnSc2d1
Francie	Francie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
získal	získat	k5eAaPmAgMnS
velení	velení	k1gNnSc4
nad	nad	k7c7
čtyřmi	čtyři	k4xCgFnPc7
legiemi	legie	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Délka	délka	k1gFnSc1
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
úřadu	úřad	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
tím	ten	k3xDgNnSc7
i	i	k8xC
doba	doba	k1gFnSc1
jeho	jeho	k3xOp3gFnSc2
imunity	imunita	k1gFnSc2
před	před	k7c7
soudním	soudní	k2eAgNnSc7d1
stíháním	stíhání	k1gNnSc7
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
stanovena	stanovit	k5eAaPmNgFnS
na	na	k7c4
pět	pět	k4xCc4
let	léto	k1gNnPc2
místo	místo	k7c2
obvyklého	obvyklý	k2eAgInSc2d1
jednoho	jeden	k4xCgInSc2
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
skončení	skončení	k1gNnSc6
konzulátu	konzulát	k1gInSc2
Caesar	Caesar	k1gMnSc1
těsně	těsně	k6eAd1
unikl	uniknout	k5eAaPmAgMnS
obžalobě	obžaloba	k1gFnSc3
kvůli	kvůli	k7c3
nezákonnostem	nezákonnost	k1gFnPc3
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
se	se	k3xPyFc4
děly	dít	k5eAaBmAgFnP,k5eAaImAgFnP
v	v	k7c6
době	doba	k1gFnSc6
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
úřadování	úřadování	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
ihned	ihned	k6eAd1
se	se	k3xPyFc4
odebral	odebrat	k5eAaPmAgInS
do	do	k7c2
svěřených	svěřený	k2eAgFnPc2d1
provincií	provincie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Prokonzulem	prokonzul	k1gMnSc7
v	v	k7c6
Galii	Galie	k1gFnSc6
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Galské	galský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Caesarova	Caesarův	k2eAgNnPc1d1
tažení	tažení	k1gNnPc1
v	v	k7c6
Galii	Galie	k1gFnSc6
</s>
<s>
Prokonzulát	Prokonzulát	k1gInSc1
v	v	k7c6
Galii	Galie	k1gFnSc6
znamenal	znamenat	k5eAaImAgInS
pro	pro	k7c4
Caesara	Caesar	k1gMnSc4
vynikající	vynikající	k2eAgFnSc4d1
možnost	možnost	k1gFnSc4
k	k	k7c3
dalšímu	další	k2eAgInSc3d1
mocenskému	mocenský	k2eAgInSc3d1
vzestupu	vzestup	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesar	Caesar	k1gMnSc1
byl	být	k5eAaImAgMnS
stále	stále	k6eAd1
enormně	enormně	k6eAd1
zadlužený	zadlužený	k2eAgInSc1d1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
správcovství	správcovství	k1gNnSc1
provincie	provincie	k1gFnSc2
představovalo	představovat	k5eAaImAgNnS
výtečný	výtečný	k2eAgInSc4d1
způsob	způsob	k1gInSc4
jak	jak	k6eAd1
se	se	k3xPyFc4
domoci	domoct	k5eAaPmF
majetku	majetek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesarovi	Caesarův	k2eAgMnPc5d1
podléhaly	podléhat	k5eAaImAgFnP
čtyři	čtyři	k4xCgFnPc4
legie	legie	k1gFnPc4
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
dvě	dva	k4xCgFnPc1
z	z	k7c2
jeho	jeho	k3xOp3gFnPc2
provincií	provincie	k1gFnPc2
<g/>
,	,	kIx,
Ilýrie	Ilýrie	k1gFnSc1
a	a	k8xC
Gallia	Gallia	k1gFnSc1
Narbonensis	Narbonensis	k1gFnSc1
<g/>
,	,	kIx,
hraničily	hraničit	k5eAaImAgFnP
s	s	k7c7
dosud	dosud	k6eAd1
nepodrobenými	podrobený	k2eNgNnPc7d1
teritorii	teritorium	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedinečná	jedinečný	k2eAgFnSc1d1
příležitost	příležitost	k1gFnSc1
k	k	k7c3
válce	válka	k1gFnSc3
se	se	k3xPyFc4
naskýtala	naskýtat	k5eAaImAgFnS
v	v	k7c6
Galii	Galie	k1gFnSc6
<g/>
,	,	kIx,
mezi	mezi	k7c7
jejímiž	jejíž	k3xOyRp3gInPc7
neklidnými	klidný	k2eNgInPc7d1
a	a	k8xC
znesvářenými	znesvářený	k2eAgInPc7d1
kmeny	kmen	k1gInPc7
probíhaly	probíhat	k5eAaImAgFnP
již	již	k9
po	po	k7c4
mnoho	mnoho	k4c4
let	léto	k1gNnPc2
boje	boj	k1gInSc2
<g/>
,	,	kIx,
kterými	který	k3yIgMnPc7,k3yQgMnPc7,k3yRgMnPc7
se	se	k3xPyFc4
Galové	Galové	k2eAgMnPc1d1
vzájemně	vzájemně	k6eAd1
oslabovali	oslabovat	k5eAaImAgMnP
a	a	k8xC
připravovali	připravovat	k5eAaImAgMnP
tak	tak	k9
půdu	půda	k1gFnSc4
pro	pro	k7c4
své	svůj	k3xOyFgNnSc4
podrobení	podrobení	k1gNnSc4
cizí	cizí	k2eAgFnSc7d1
mocností	mocnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Poměry	poměr	k1gInPc1
v	v	k7c6
zemi	zem	k1gFnSc6
byly	být	k5eAaImAgFnP
napjaté	napjatý	k2eAgFnPc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
římští	římský	k2eAgMnPc1d1
spojenci	spojenec	k1gMnPc1
Aeduové	Aeduová	k1gFnSc2
podlehli	podlehnout	k5eAaPmAgMnP
svým	svůj	k3xOyFgMnPc3
nepřátelům	nepřítel	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
si	se	k3xPyFc3
na	na	k7c4
pomoc	pomoc	k1gFnSc4
přizvali	přizvat	k5eAaPmAgMnP
germánské	germánský	k2eAgInPc4d1
Svéby	Svéb	k1gInPc4
<g/>
,	,	kIx,
vedené	vedený	k2eAgNnSc1d1
náčelníkem	náčelník	k1gMnSc7
Ariovistem	Ariovist	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svébové	Svébová	k1gFnSc6
se	se	k3xPyFc4
následně	následně	k6eAd1
usadili	usadit	k5eAaPmAgMnP
v	v	k7c6
dobyté	dobytý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
Haeduů	Haedu	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezitím	mezitím	k6eAd1
také	také	k9
Helvéciové	Helvécius	k1gMnPc1
se	se	k3xPyFc4
chystali	chystat	k5eAaImAgMnP
proniknout	proniknout	k5eAaPmF
z	z	k7c2
území	území	k1gNnSc2
dnešního	dnešní	k2eAgNnSc2d1
Švýcarska	Švýcarsko	k1gNnSc2
hlouběji	hluboko	k6eAd2
do	do	k7c2
Galie	Galie	k1gFnSc2
a	a	k8xC
vyhledat	vyhledat	k5eAaPmF
si	se	k3xPyFc3
zde	zde	k6eAd1
nová	nový	k2eAgNnPc4d1
sídla	sídlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesar	Caesar	k1gMnSc1
okamžitě	okamžitě	k6eAd1
odvedl	odvést	k5eAaPmAgMnS
dvě	dva	k4xCgFnPc4
nové	nový	k2eAgFnPc4d1
legie	legie	k1gFnPc4
a	a	k8xC
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Bibracte	Bibract	k1gInSc5
Helvéty	Helvét	k1gMnPc7
v	v	k7c6
roce	rok	k1gInSc6
58	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
drtivě	drtivě	k6eAd1
porazil	porazit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
přeživší	přeživší	k2eAgInPc4d1
zbytky	zbytek	k1gInPc4
odeslal	odeslat	k5eAaPmAgMnS
zpět	zpět	k6eAd1
do	do	k7c2
jejich	jejich	k3xOp3gFnSc2
někdejší	někdejší	k2eAgFnSc2d1
sídelní	sídelní	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
měla	mít	k5eAaImAgFnS
fungovat	fungovat	k5eAaImF
jako	jako	k9
nárazník	nárazník	k1gInSc4
před	před	k7c7
pronikajícími	pronikající	k2eAgMnPc7d1
Germány	Germán	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potom	potom	k6eAd1
vytáhl	vytáhnout	k5eAaPmAgInS
směrem	směr	k1gInSc7
na	na	k7c4
sever	sever	k1gInSc4
do	do	k7c2
dnešního	dnešní	k2eAgNnSc2d1
Alsaska	Alsasko	k1gNnSc2
proti	proti	k7c3
Ariovistovi	Ariovista	k1gMnSc3
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc4,k3yIgMnSc4,k3yQgMnSc4
i	i	k9
s	s	k7c7
jeho	jeho	k3xOp3gMnPc7
Germány	Germán	k1gMnPc7
vypudil	vypudit	k5eAaPmAgMnS
zpět	zpět	k6eAd1
za	za	k7c4
Rýn	Rýn	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgNnSc4
vítězné	vítězný	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
nechal	nechat	k5eAaPmAgInS
přezimovat	přezimovat	k5eAaBmF
na	na	k7c6
území	území	k1gNnSc6
Sekvánů	Sekván	k1gMnPc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
dal	dát	k5eAaPmAgMnS
jasně	jasně	k6eAd1
najevo	najevo	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gInPc1
zásahy	zásah	k1gInPc1
v	v	k7c6
oblastech	oblast	k1gFnPc6
severně	severně	k6eAd1
od	od	k7c2
Narbonensis	Narbonensis	k1gFnSc2
nebyly	být	k5eNaImAgInP
pouze	pouze	k6eAd1
dočasného	dočasný	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
druhém	druhý	k4xOgInSc6
roce	rok	k1gInSc6
si	se	k3xPyFc3
Caesar	Caesar	k1gMnSc1
podrobil	podrobit	k5eAaPmAgMnS
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Galii	Galie	k1gFnSc6
Belgy	Belg	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
byli	být	k5eAaImAgMnP
považováni	považován	k2eAgMnPc1d1
za	za	k7c4
nejudatnější	udatný	k2eAgInSc4d3
lid	lid	k1gInSc4
mezi	mezi	k7c7
všemi	všecek	k3xTgInPc7
galskými	galský	k2eAgInPc7d1
kmeny	kmen	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
56	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
porazil	porazit	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
legát	legát	k1gMnSc1
Decimus	Decimus	k1gMnSc1
Junius	Junius	k1gMnSc1
Brutus	Brutus	k1gMnSc1
povstalé	povstalý	k2eAgFnSc2d1
Venety	Veneta	k1gFnSc2
v	v	k7c6
Armorice	Armorika	k1gFnSc6
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc1d1
Bretaň	Bretaň	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
Caesar	Caesar	k1gMnSc1
přivedl	přivést	k5eAaPmAgMnS
pod	pod	k7c4
svoji	svůj	k3xOyFgFnSc4
kontrolu	kontrola	k1gFnSc4
velkou	velký	k2eAgFnSc4d1
část	část	k1gFnSc4
západní	západní	k2eAgFnSc2d1
Galie	Galie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
zimní	zimní	k2eAgFnSc2d1
přestávky	přestávka	k1gFnSc2
v	v	k7c6
bojích	boj	k1gInPc6
se	se	k3xPyFc4
Caesar	Caesar	k1gMnSc1
setkal	setkat	k5eAaPmAgMnS
v	v	k7c6
Ravenně	Ravenně	k1gFnSc6
s	s	k7c7
Pompeiem	Pompeius	k1gMnSc7
a	a	k8xC
Crassem	Crass	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
jejich	jejich	k3xOp3gNnSc2
jednání	jednání	k1gNnSc2
byla	být	k5eAaImAgFnS
dohoda	dohoda	k1gFnSc1
o	o	k7c6
prodloužení	prodloužení	k1gNnSc6
Caesarova	Caesarův	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
o	o	k7c4
dalších	další	k2eAgInPc2d1
pět	pět	k4xCc4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Crassus	Crassus	k1gInSc1
a	a	k8xC
Pompeius	Pompeius	k1gInSc1
se	se	k3xPyFc4
měli	mít	k5eAaImAgMnP
stát	stát	k5eAaPmF,k5eAaImF
konzuly	konzul	k1gMnPc4
pro	pro	k7c4
následující	následující	k2eAgInSc4d1
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesarovi	Caesarův	k2eAgMnPc5d1
tak	tak	k9
byl	být	k5eAaImAgInS
poskytnut	poskytnut	k2eAgInSc1d1
dostatek	dostatek	k1gInSc1
času	čas	k1gInSc2
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
si	se	k3xPyFc3
podmanil	podmanit	k5eAaPmAgMnS
všechny	všechen	k3xTgInPc4
dosud	dosud	k6eAd1
svobodné	svobodný	k2eAgInPc4d1
keltské	keltský	k2eAgInPc4d1
kraje	kraj	k1gInPc4
rozkládající	rozkládající	k2eAgInPc4d1
se	se	k3xPyFc4
mezi	mezi	k7c7
Pyrenejemi	Pyreneje	k1gFnPc7
a	a	k8xC
řekou	řeka	k1gFnSc7
Rýnem	Rýn	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
situace	situace	k1gFnSc2
byla	být	k5eAaImAgFnS
značně	značně	k6eAd1
ulehčena	ulehčit	k5eAaPmNgFnS
nejednotností	nejednotnost	k1gFnSc7
Galů	Gal	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
na	na	k7c4
něho	on	k3xPp3gMnSc4
leckdy	leckdy	k6eAd1
neváhali	váhat	k5eNaImAgMnP
obrátit	obrátit	k5eAaPmF
se	s	k7c7
žádostí	žádost	k1gFnSc7
o	o	k7c4
pomoc	pomoc	k1gFnSc4
proti	proti	k7c3
svým	svůj	k3xOyFgMnPc3
sousedům	soused	k1gMnPc3
<g/>
,	,	kIx,
jen	jen	k9
aby	aby	kYmCp3nP
se	se	k3xPyFc4
sami	sám	k3xTgMnPc1
nestali	stát	k5eNaPmAgMnP
obětí	oběť	k1gFnSc7
ambiciózního	ambiciózní	k2eAgMnSc2d1
prokonzula	prokonzul	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Kvůli	kvůli	k7c3
vpádu	vpád	k1gInSc3
germánských	germánský	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
Usipetů	Usipet	k1gInPc2
a	a	k8xC
Tenkterů	Tenkter	k1gInPc2
do	do	k7c2
Galie	Galie	k1gFnSc2
považoval	považovat	k5eAaImAgMnS
Caesar	Caesar	k1gMnSc1
za	za	k7c4
nezbytné	nezbytný	k2eAgNnSc4d1,k2eNgNnSc4d1
překročit	překročit	k5eAaPmF
v	v	k7c6
roce	rok	k1gInSc6
55	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Rýn	Rýn	k1gInSc4
a	a	k8xC
podniknout	podniknout	k5eAaPmF
trestnou	trestný	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
do	do	k7c2
Germánie	Germánie	k1gFnSc2
(	(	kIx(
<g/>
což	což	k3yRnSc1,k3yQnSc1
zopakoval	zopakovat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
53	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taktéž	Taktéž	k?
se	se	k3xPyFc4
vypravil	vypravit	k5eAaPmAgInS
do	do	k7c2
Británie	Británie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
ale	ale	k9
příliš	příliš	k6eAd1
nezdržel	zdržet	k5eNaPmAgMnS
<g/>
,	,	kIx,
poněvadž	poněvadž	k8xS
jeho	jeho	k3xOp3gFnSc1
flotila	flotila	k1gFnSc1
byla	být	k5eAaImAgFnS
zničena	zničit	k5eAaPmNgFnS
bouří	bouř	k1gFnSc7
a	a	k8xC
již	již	k6eAd1
se	se	k3xPyFc4
blížila	blížit	k5eAaImAgFnS
zima	zima	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
54	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
provedl	provést	k5eAaPmAgMnS
Caesar	Caesar	k1gMnSc1
další	další	k2eAgMnSc1d1
vpád	vpád	k1gInSc4
na	na	k7c4
ostrov	ostrov	k1gInSc4
<g/>
,	,	kIx,
během	během	k7c2
něhož	jenž	k3xRgInSc2
postoupil	postoupit	k5eAaPmAgInS
až	až	k6eAd1
k	k	k7c3
řece	řeka	k1gFnSc3
Thamesus	Thamesus	k1gInSc1
(	(	kIx(
<g/>
Temže	Temže	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
formálním	formální	k2eAgNnSc6d1
ujištění	ujištění	k1gNnSc6
britanských	britanský	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
o	o	k7c6
jejich	jejich	k3xOp3gNnSc6
poddanství	poddanství	k1gNnSc6
a	a	k8xC
loajalitě	loajalita	k1gFnSc6
vůči	vůči	k7c3
Římu	Řím	k1gInSc3
se	se	k3xPyFc4
ale	ale	k9
opět	opět	k6eAd1
stáhl	stáhnout	k5eAaPmAgMnS
do	do	k7c2
Galie	Galie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
výpravy	výprava	k1gFnPc1
vyvolaly	vyvolat	k5eAaPmAgFnP
v	v	k7c6
Římě	Řím	k1gInSc6
a	a	k8xC
především	především	k6eAd1
v	v	k7c6
samotném	samotný	k2eAgInSc6d1
senátu	senát	k1gInSc6
senzaci	senzace	k1gFnSc3
<g/>
:	:	kIx,
Caesar	Caesar	k1gMnSc1
jako	jako	k8xS,k8xC
první	první	k4xOgMnSc1
římský	římský	k2eAgMnSc1d1
vojevůdce	vojevůdce	k1gMnSc1
vedl	vést	k5eAaImAgMnS
vojenské	vojenský	k2eAgFnPc4d1
výpravy	výprava	k1gFnPc4
do	do	k7c2
těchto	tento	k3xDgFnPc2
pro	pro	k7c4
Římany	Říman	k1gMnPc4
dosud	dosud	k6eAd1
neznámých	známý	k2eNgFnPc2d1
končin	končina	k1gFnPc2
<g/>
,	,	kIx,
od	od	k7c2
trvalého	trvalý	k2eAgNnSc2d1
obsazení	obsazení	k1gNnSc2
Germánie	Germánie	k1gFnSc2
a	a	k8xC
Británie	Británie	k1gFnSc2
však	však	k9
upustil	upustit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každopádně	každopádně	k6eAd1
stanovení	stanovení	k1gNnSc3
hranice	hranice	k1gFnSc2
římského	římský	k2eAgInSc2d1
záboru	zábor	k1gInSc2
na	na	k7c6
Rýně	Rýn	k1gInSc6
bylo	být	k5eAaImAgNnS
jedno	jeden	k4xCgNnSc1
z	z	k7c2
nejvýznamnějších	významný	k2eAgNnPc2d3
Caesarových	Caesarových	k2eAgNnPc2d1
rozhodnutí	rozhodnutí	k1gNnPc2
z	z	k7c2
hlediska	hledisko	k1gNnSc2
evropských	evropský	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Důvodem	důvod	k1gInSc7
k	k	k7c3
tomu	ten	k3xDgNnSc3
byl	být	k5eAaImAgMnS
i	i	k8xC
šířící	šířící	k2eAgMnSc1d1
se	se	k3xPyFc4
neklid	neklid	k1gInSc1
mezi	mezi	k7c7
teprve	teprve	k6eAd1
nedávno	nedávno	k6eAd1
podrobenými	podrobený	k2eAgInPc7d1
galskými	galský	k2eAgInPc7d1
kmeny	kmen	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
slabé	slabý	k2eAgFnSc3d1
úrodě	úroda	k1gFnSc3
v	v	k7c6
roce	rok	k1gInSc6
54	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
povstaly	povstat	k5eAaPmAgFnP
proti	proti	k7c3
Římanům	Říman	k1gMnPc3
kmeny	kmen	k1gInPc7
Eburonů	Eburon	k1gMnPc2
<g/>
,	,	kIx,
Treverů	Trever	k1gMnPc2
a	a	k8xC
Nerviů	Nervi	k1gMnPc2
<g/>
,	,	kIx,
do	do	k7c2
jejichž	jejichž	k3xOyRp3gNnSc2
čela	čelo	k1gNnSc2
se	se	k3xPyFc4
postavil	postavit	k5eAaPmAgMnS
náčelník	náčelník	k1gMnSc1
Ambiorix	Ambiorix	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpočátku	zpočátku	k6eAd1
sice	sice	k8xC
dosáhly	dosáhnout	k5eAaPmAgFnP
několika	několik	k4yIc2
menších	malý	k2eAgInPc2d2
úspěchů	úspěch	k1gInPc2
<g/>
,	,	kIx,
nakonec	nakonec	k6eAd1
byly	být	k5eAaImAgFnP
ale	ale	k9
definitivně	definitivně	k6eAd1
zbaveny	zbavit	k5eAaPmNgFnP
svobody	svoboda	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větším	veliký	k2eAgNnSc7d2
nebezpečím	nebezpečí	k1gNnSc7
pro	pro	k7c4
římskou	římský	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
v	v	k7c6
Galii	Galie	k1gFnSc6
se	se	k3xPyFc4
ukázalo	ukázat	k5eAaPmAgNnS
být	být	k5eAaImF
povstání	povstání	k1gNnSc1
Vercingetoriga	Vercingetorig	k1gMnSc2
<g/>
,	,	kIx,
náčelníka	náčelník	k1gMnSc2
kmene	kmen	k1gInSc2
Arvernů	Arvern	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
52	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Tomu	ten	k3xDgInSc3
se	se	k3xPyFc4
jako	jako	k8xC,k8xS
prvnímu	první	k4xOgNnSc3
podařilo	podařit	k5eAaPmAgNnS
sjednotit	sjednotit	k5eAaPmF
všechny	všechen	k3xTgMnPc4
Galy	Gal	k1gMnPc4
a	a	k8xC
Caesarovi	Caesarův	k2eAgMnPc1d1
se	se	k3xPyFc4
tak	tak	k9
v	v	k7c6
něm	on	k3xPp3gMnSc6
objevil	objevit	k5eAaPmAgMnS
první	první	k4xOgMnSc1
skutečně	skutečně	k6eAd1
rovnocenný	rovnocenný	k2eAgMnSc1d1
nepřítel	nepřítel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
taktika	taktika	k1gFnSc1
spálené	spálený	k2eAgFnSc2d1
země	zem	k1gFnSc2
přivodila	přivodit	k5eAaPmAgFnS,k5eAaBmAgFnS
Caesarovým	Caesarův	k2eAgFnPc3d1
legiím	legie	k1gFnPc3
vážné	vážná	k1gFnSc2
problémy	problém	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vercingetorix	Vercingetorix	k1gInSc1
nejprve	nejprve	k6eAd1
odřízl	odříznout	k5eAaPmAgInS
Caesara	Caesar	k1gMnSc4
od	od	k7c2
jeho	jeho	k3xOp3gFnPc2
zásobovacích	zásobovací	k2eAgFnPc2d1
linií	linie	k1gFnPc2
a	a	k8xC
poté	poté	k6eAd1
ho	on	k3xPp3gMnSc4
odrazil	odrazit	k5eAaPmAgMnS
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Gergovie	Gergovie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
tomto	tento	k3xDgNnSc6
vítězství	vítězství	k1gNnSc6
ale	ale	k8xC
Vercingentorix	Vercingentorix	k1gInSc1
nerozvážně	rozvážně	k6eNd1
upustil	upustit	k5eAaPmAgInS
od	od	k7c2
dosavadní	dosavadní	k2eAgFnSc2d1
opatrné	opatrný	k2eAgFnSc2d1
defenzivní	defenzivní	k2eAgFnSc2d1
taktiky	taktika	k1gFnSc2
a	a	k8xC
pln	pln	k2eAgMnSc1d1
sebedůvěry	sebedůvěra	k1gFnPc4
napadl	napadnout	k5eAaPmAgMnS
Caesarovo	Caesarův	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
nedostatečně	dostatečně	k6eNd1
vycvičená	vycvičený	k2eAgFnSc1d1
jízda	jízda	k1gFnSc1
byla	být	k5eAaImAgFnS
poražena	porazit	k5eAaPmNgFnS
<g/>
,	,	kIx,
načež	načež	k6eAd1
se	se	k3xPyFc4
Vercingetorix	Vercingetorix	k1gInSc1
stáhl	stáhnout	k5eAaPmAgInS
do	do	k7c2
Alesie	Alesie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesar	Caesar	k1gMnSc1
začal	začít	k5eAaPmAgMnS
toto	tento	k3xDgNnSc4
oppidum	oppidum	k1gNnSc4
bez	bez	k7c2
otálení	otálení	k1gNnSc2
obklopovat	obklopovat	k5eAaImF
třicet	třicet	k4xCc4
pět	pět	k4xCc4
kilometrů	kilometr	k1gInPc2
dlouhou	dlouhý	k2eAgFnSc4d1
obléhací	obléhací	k2eAgFnSc4d1
zdí	zeď	k1gFnSc7
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
se	se	k3xPyFc4
z	z	k7c2
nitra	nitro	k1gNnSc2
Galie	Galie	k1gFnSc1
vydalo	vydat	k5eAaPmAgNnS
Vercingetorigovi	Vercingetorig	k1gMnSc3
na	na	k7c4
pomoc	pomoc	k1gFnSc4
ohromné	ohromný	k2eAgNnSc1d1
osvobozovací	osvobozovací	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následné	následný	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
o	o	k7c6
Alesii	Alesie	k1gFnSc6
se	se	k3xPyFc4
Římané	Říman	k1gMnPc1
s	s	k7c7
pomocí	pomoc	k1gFnSc7
Germánské	germánský	k2eAgFnSc2d1
jízdy	jízda	k1gFnSc2
dokázali	dokázat	k5eAaPmAgMnP
navzdory	navzdory	k7c3
nesmírné	smírný	k2eNgFnSc3d1
převaze	převaha	k1gFnSc3
Galů	Gal	k1gMnPc2
ubránit	ubránit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
kapitulaci	kapitulace	k1gFnSc6
Vercingetoriga	Vercingetorig	k1gMnSc2
byl	být	k5eAaImAgInS
galský	galský	k2eAgInSc1d1
odpor	odpor	k1gInSc1
definitivně	definitivně	k6eAd1
zlomen	zlomit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
šest	šest	k4xCc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
v	v	k7c6
roce	rok	k1gInSc6
46	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
byl	být	k5eAaImAgInS
krátce	krátce	k6eAd1
po	po	k7c6
skončení	skončení	k1gNnSc6
Caesarova	Caesarův	k2eAgInSc2d1
triumfu	triumf	k1gInSc2
Vercingetorix	Vercingetorix	k1gInSc1
v	v	k7c6
Římě	Řím	k1gInSc6
popraven	popravit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
dalším	další	k2eAgInSc6d1
roce	rok	k1gInSc6
musel	muset	k5eAaImAgMnS
Caesar	Caesar	k1gMnSc1
čelit	čelit	k5eAaImF
ještě	ještě	k6eAd1
ojedinělým	ojedinělý	k2eAgFnPc3d1
lokálním	lokální	k2eAgFnPc3d1
vzpourám	vzpoura	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
potlačil	potlačit	k5eAaPmAgInS
se	s	k7c7
značnou	značný	k2eAgFnSc7d1
brutalitou	brutalita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nezměrnou	změrný	k2eNgFnSc4d1,k2eAgFnSc4d1
válečnou	válečný	k2eAgFnSc4d1
kořist	kořist	k1gFnSc4
a	a	k8xC
tribut	tribut	k1gInSc4
placený	placený	k2eAgInSc1d1
poraženými	poražený	k2eAgInPc7d1
kmeny	kmen	k1gInPc7
využil	využít	k5eAaPmAgMnS
k	k	k7c3
financování	financování	k1gNnSc3
vlastní	vlastní	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
na	na	k7c6
konci	konec	k1gInSc6
války	válka	k1gFnSc2
čítala	čítat	k5eAaImAgFnS
třináct	třináct	k4xCc4
legií	legie	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
politického	politický	k2eAgInSc2d1
boje	boj	k1gInSc2
v	v	k7c6
Římě	Řím	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
tohoto	tento	k3xDgInSc2
okamžiku	okamžik	k1gInSc2
se	se	k3xPyFc4
již	již	k9
Caesar	Caesar	k1gMnSc1
nemusel	muset	k5eNaImAgMnS
obávat	obávat	k5eAaImF
věřitelů	věřitel	k1gMnPc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
v	v	k7c4
Galii	Galie	k1gFnSc4
nabyl	nabýt	k5eAaPmAgInS
(	(	kIx(
<g/>
nebo	nebo	k8xC
spíše	spíše	k9
uloupil	uloupit	k5eAaPmAgMnS
<g/>
)	)	kIx)
ohromné	ohromný	k2eAgNnSc1d1
bohatství	bohatství	k1gNnSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
z	z	k7c2
něho	on	k3xPp3gMnSc2
učinilo	učinit	k5eAaImAgNnS,k5eAaPmAgNnS
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
nejzámožnějších	zámožný	k2eAgMnPc2d3
římských	římský	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
Caesarem	Caesar	k1gMnSc7
sloužil	sloužit	k5eAaImAgInS
v	v	k7c6
řadách	řada	k1gFnPc6
jeho	jeho	k3xOp3gMnPc2
legátů	legát	k1gMnPc2
jeho	jeho	k3xOp3gFnSc4
bratranec	bratranec	k1gMnSc1
Lucius	Lucius	k1gMnSc1
Iulius	Iulius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
<g/>
,	,	kIx,
dále	daleko	k6eAd2
Marcus	Marcus	k1gMnSc1
Antonius	Antonius	k1gMnSc1
<g/>
,	,	kIx,
Titus	Titus	k1gMnSc1
Labienus	Labienus	k1gMnSc1
<g/>
,	,	kIx,
Quintus	Quintus	k1gMnSc1
Tullius	Tullius	k1gMnSc1
Cicero	Cicero	k1gMnSc1
<g/>
,	,	kIx,
bratr	bratr	k1gMnSc1
Marca	Marcum	k1gNnSc2
Tullia	Tullius	k1gMnSc2
Cicerona	Cicero	k1gMnSc2
<g/>
,	,	kIx,
a	a	k8xC
Crassův	Crassův	k2eAgMnSc1d1
stejnojmenný	stejnojmenný	k2eAgMnSc1d1
syn	syn	k1gMnSc1
<g/>
,	,	kIx,
Publius	Publius	k1gMnSc1
Licinius	Licinius	k1gMnSc1
Crassus	Crassus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
Plútarcha	Plútarch	k1gMnSc2
bylo	být	k5eAaImAgNnS
výsledkem	výsledek	k1gInSc7
tažení	tažení	k1gNnPc2
800	#num#	k4
podmaněných	podmaněný	k2eAgNnPc2d1
galských	galský	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
,	,	kIx,
300	#num#	k4
podrobených	podrobený	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
<g/>
,	,	kIx,
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
poražených	poražený	k2eAgMnPc2d1
Galů	Gal	k1gMnPc2
bylo	být	k5eAaImAgNnS
prodáno	prodat	k5eAaPmNgNnS
do	do	k7c2
otroctví	otroctví	k1gNnSc2
a	a	k8xC
další	další	k2eAgMnPc1d1
zahynuli	zahynout	k5eAaPmAgMnP
v	v	k7c6
důsledku	důsledek	k1gInSc6
bojů	boj	k1gInPc2
a	a	k8xC
taktiky	taktika	k1gFnSc2
spálené	spálený	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antičtí	antický	k2eAgMnPc1d1
historikové	historik	k1gMnPc1
měli	mít	k5eAaImAgMnP
obvykle	obvykle	k6eAd1
tendenci	tendence	k1gFnSc4
zveličovat	zveličovat	k5eAaImF
všechny	všechen	k3xTgInPc4
početní	početní	k2eAgInPc4d1
údaje	údaj	k1gInPc4
<g/>
,	,	kIx,
ovšem	ovšem	k9
Caesarovy	Caesarův	k2eAgInPc4d1
výboje	výboj	k1gInPc4
v	v	k7c6
Galii	Galie	k1gFnSc6
byly	být	k5eAaImAgFnP
zřejmě	zřejmě	k6eAd1
největší	veliký	k2eAgFnSc7d3
vojenskou	vojenský	k2eAgFnSc7d1
operací	operace	k1gFnSc7
od	od	k7c2
dob	doba	k1gFnPc2
Alexandrova	Alexandrův	k2eAgNnSc2d1
tažení	tažení	k1gNnSc2
proti	proti	k7c3
perské	perský	k2eAgFnSc3d1
říši	říš	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
důsledky	důsledek	k1gInPc4
navíc	navíc	k6eAd1
přetrvaly	přetrvat	k5eAaPmAgInP
podstatně	podstatně	k6eAd1
déle	dlouho	k6eAd2
než	než	k8xS
v	v	k7c6
případě	případ	k1gInSc6
Alexandra	Alexandr	k1gMnSc2
<g/>
:	:	kIx,
Galie	Galie	k1gFnSc1
již	již	k6eAd1
nikdy	nikdy	k6eAd1
nezískala	získat	k5eNaPmAgFnS
nezávislost	nezávislost	k1gFnSc4
a	a	k8xC
věrně	věrně	k6eAd1
setrvávala	setrvávat	k5eAaImAgFnS
po	po	k7c6
boku	bok	k1gInSc6
Říma	Řím	k1gInSc2
až	až	k9
do	do	k7c2
zániku	zánik	k1gInSc2
římské	římský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgInPc4
vlastní	vlastní	k2eAgInSc4d1
poznatky	poznatek	k1gInPc4
z	z	k7c2
tažení	tažení	k1gNnSc2
zachytil	zachytit	k5eAaPmAgMnS
Caesar	Caesar	k1gMnSc1
v	v	k7c6
díle	dílo	k1gNnSc6
Commentarii	Commentarie	k1gFnSc3
de	de	k?
bello	bello	k1gNnSc1
Gallico	Gallico	k1gMnSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
Zápisky	zápiska	k1gFnSc2
o	o	k7c6
válce	válka	k1gFnSc6
galské	galský	k2eAgFnSc6d1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
písemnost	písemnost	k1gFnSc1
kromě	kromě	k7c2
průběhu	průběh	k1gInSc2
vojenských	vojenský	k2eAgFnPc2d1
operací	operace	k1gFnPc2
líčí	líčit	k5eAaImIp3nS
také	také	k9
mnoho	mnoho	k4c4
zajímavých	zajímavý	k2eAgInPc2d1
detailů	detail	k1gInPc2
o	o	k7c6
životě	život	k1gInSc6
a	a	k8xC
zvycích	zvyk	k1gInPc6
Galů	Gal	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gInSc7
hlavním	hlavní	k2eAgInSc7d1
účelem	účel	k1gInSc7
bylo	být	k5eAaImAgNnS
ale	ale	k9
především	především	k6eAd1
ospravedlnění	ospravedlnění	k1gNnSc1
Caesarových	Caesarových	k2eAgInPc2d1
agresivních	agresivní	k2eAgInPc2d1
a	a	k8xC
dobyvačných	dobyvačný	k2eAgInPc2d1
výbojů	výboj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesar	Caesar	k1gMnSc1
rovněž	rovněž	k9
jako	jako	k9
první	první	k4xOgFnSc7
poukázal	poukázat	k5eAaPmAgMnS
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
mezi	mezi	k7c7
Galy	Gal	k1gMnPc7
a	a	k8xC
Germány	Germán	k1gMnPc7
a	a	k8xC
vzájemně	vzájemně	k6eAd1
je	být	k5eAaImIp3nS
odlišil	odlišit	k5eAaPmAgMnS
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
za	za	k7c4
hranici	hranice	k1gFnSc4
mezi	mezi	k7c7
oběma	dva	k4xCgFnPc7
skupinami	skupina	k1gFnPc7
kmenů	kmen	k1gInPc2
stanovil	stanovit	k5eAaPmAgInS
Rýn	Rýn	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Germáni	Germán	k1gMnPc1
byli	být	k5eAaImAgMnP
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
pokládáni	pokládán	k2eAgMnPc1d1
pouze	pouze	k6eAd1
za	za	k7c4
jeden	jeden	k4xCgInSc4
z	z	k7c2
keltských	keltský	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Busta	busta	k1gFnSc1
v	v	k7c4
Arles	Arles	k1gInSc4
</s>
<s>
I	i	k9
přes	přes	k7c4
své	svůj	k3xOyFgInPc4
úspěchy	úspěch	k1gInPc4
a	a	k8xC
přínos	přínos	k1gInSc4
Římu	Řím	k1gInSc3
zůstával	zůstávat	k5eAaImAgMnS
Caesar	Caesar	k1gMnSc1
nadále	nadále	k6eAd1
značně	značně	k6eAd1
nepopulární	populární	k2eNgMnSc1d1
mezi	mezi	k7c7
konzervativními	konzervativní	k2eAgMnPc7d1
senátory	senátor	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
ho	on	k3xPp3gMnSc4
podezřívali	podezřívat	k5eAaImAgMnP
z	z	k7c2
touhy	touha	k1gFnSc2
po	po	k7c6
královské	královský	k2eAgFnSc6d1
vládě	vláda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
55	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
byli	být	k5eAaImAgMnP
Caesarovi	Caesarův	k2eAgMnPc1d1
spojenci	spojenec	k1gMnPc1
Pompeius	Pompeius	k1gMnSc1
a	a	k8xC
Crassus	Crassus	k1gMnSc1
zvoleni	zvolit	k5eAaPmNgMnP
za	za	k7c7
konzuly	konzul	k1gMnPc7
a	a	k8xC
Caesarovi	Caesar	k1gMnSc6
byl	být	k5eAaImAgMnS
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
ujednáním	ujednání	k1gNnSc7
triumvirů	triumvir	k1gMnPc2
prodloužen	prodloužen	k2eAgInSc4d1
prokonzulát	prokonzulát	k1gInSc4
o	o	k7c4
dalších	další	k2eAgNnPc2d1
pět	pět	k4xCc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
byl	být	k5eAaImAgInS
fakticky	fakticky	k6eAd1
poslední	poslední	k2eAgInSc1d1
moment	moment	k1gInSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
triumvirové	triumvir	k1gMnPc1
vystupovali	vystupovat	k5eAaImAgMnP
ve	v	k7c6
vzájemném	vzájemný	k2eAgInSc6d1
souladu	soulad	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
54	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
zemřela	zemřít	k5eAaPmAgFnS
během	během	k7c2
porodu	porod	k1gInSc2
Caesarova	Caesarův	k2eAgFnSc1d1
dcera	dcera	k1gFnSc1
Julia	Julius	k1gMnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
Pompeiovou	Pompeiový	k2eAgFnSc7d1
manželkou	manželka	k1gFnSc7
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
zanikl	zaniknout	k5eAaPmAgInS
vzájemný	vzájemný	k2eAgInSc1d1
příbuzenský	příbuzenský	k2eAgInSc1d1
svazek	svazek	k1gInSc1
mezi	mezi	k7c7
Pompeiem	Pompeius	k1gMnSc7
a	a	k8xC
Caesarem	Caesar	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rok	rok	k1gInSc1
poté	poté	k6eAd1
byl	být	k5eAaImAgInS
Crassus	Crassus	k1gInSc1
zabit	zabít	k5eAaPmNgInS
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Karrh	Karrha	k1gFnPc2
v	v	k7c6
boji	boj	k1gInSc6
proti	proti	k7c3
Parthům	Parth	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pompeius	Pompeius	k1gMnSc1
se	se	k3xPyFc4
nyní	nyní	k6eAd1
ucházel	ucházet	k5eAaImAgInS
o	o	k7c4
přízeň	přízeň	k1gFnSc4
a	a	k8xC
podporu	podpora	k1gFnSc4
optimátů	optimát	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesar	Caesar	k1gMnSc1
<g/>
,	,	kIx,
tehdy	tehdy	k6eAd1
plně	plně	k6eAd1
zaměstnaný	zaměstnaný	k2eAgInSc1d1
záležitostmi	záležitost	k1gFnPc7
v	v	k7c6
Galii	Galie	k1gFnSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgMnS
udržet	udržet	k5eAaPmF
spojenectví	spojenectví	k1gNnSc4
s	s	k7c7
Pompeiem	Pompeius	k1gMnSc7
a	a	k8xC
nabídl	nabídnout	k5eAaPmAgMnS
mu	on	k3xPp3gMnSc3
ruku	ruka	k1gFnSc4
jedné	jeden	k4xCgFnSc3
ze	z	k7c2
svých	svůj	k3xOyFgFnPc2
praneteří	praneteř	k1gFnPc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
Pompeius	Pompeius	k1gMnSc1
odmítl	odmítnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k7c2
toho	ten	k3xDgInSc2
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Cornelií	Cornelie	k1gFnSc7
Metellou	Metella	k1gFnSc7
<g/>
,	,	kIx,
dcerou	dcera	k1gFnSc7
Metella	Metell	k1gMnSc2
Scipiona	Scipion	k1gMnSc2
<g/>
,	,	kIx,
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
nejúhlavnějších	úhlavní	k2eAgMnPc2d3
Caesarových	Caesarových	k2eAgMnPc2d1
nepřátel	nepřítel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Senát	senát	k1gInSc1
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Pompeiem	Pompeius	k1gMnSc7
nařídil	nařídit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
50	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Caesarovi	Caesar	k1gMnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
Říma	Řím	k1gInSc2
a	a	k8xC
rozpustil	rozpustit	k5eAaPmAgMnS
svoji	svůj	k3xOyFgFnSc4
armádu	armáda	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
jeho	jeho	k3xOp3gNnSc4
funkční	funkční	k2eAgNnSc4d1
období	období	k1gNnSc4
jako	jako	k8xC,k8xS
prokonzula	prokonzul	k1gMnSc4
skončilo	skončit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Senát	senát	k1gInSc1
navíc	navíc	k6eAd1
Caesarovi	Caesar	k1gMnSc3
znovu	znovu	k6eAd1
zakázal	zakázat	k5eAaPmAgMnS
ucházet	ucházet	k5eAaImF
se	se	k3xPyFc4
in	in	k?
absentia	absentium	k1gNnSc2
o	o	k7c4
nový	nový	k2eAgInSc4d1
konzulát	konzulát	k1gInSc4
a	a	k8xC
rovněž	rovněž	k9
mu	on	k3xPp3gMnSc3
odmítl	odmítnout	k5eAaPmAgMnS
přidělit	přidělit	k5eAaPmF
novou	nový	k2eAgFnSc4d1
provincii	provincie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesar	Caesar	k1gMnSc1
si	se	k3xPyFc3
byl	být	k5eAaImAgMnS
vědom	vědom	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
jakmile	jakmile	k8xS
se	se	k3xPyFc4
jednou	jeden	k4xCgFnSc7
vzdá	vzdát	k5eAaPmIp3nS
svého	svůj	k3xOyFgNnSc2
vojska	vojsko	k1gNnSc2
a	a	k8xC
vstoupí	vstoupit	k5eAaPmIp3nP
do	do	k7c2
Říma	Řím	k1gInSc2
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
okamžitě	okamžitě	k6eAd1
obžalován	obžalovat	k5eAaPmNgMnS
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
politická	politický	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
skončí	skončit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
Pompeius	Pompeius	k1gMnSc1
v	v	k7c6
senátu	senát	k1gInSc6
obvinil	obvinit	k5eAaPmAgMnS
Caesara	Caesar	k1gMnSc4
z	z	k7c2
porušení	porušení	k1gNnSc2
kázně	kázeň	k1gFnSc2
a	a	k8xC
zrady	zrada	k1gFnSc2
<g/>
,	,	kIx,
překročil	překročit	k5eAaPmAgMnS
Caesar	Caesar	k1gMnSc1
10	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
roku	rok	k1gInSc2
49	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
s	s	k7c7
jedinou	jediný	k2eAgFnSc7d1
legií	legie	k1gFnSc7
řeku	řeka	k1gFnSc4
Rubikon	Rubikon	k1gInSc1
(	(	kIx(
<g/>
tehdejší	tehdejší	k2eAgFnSc1d1
hranice	hranice	k1gFnSc1
Itálie	Itálie	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
zahájil	zahájit	k5eAaPmAgMnS
tak	tak	k6eAd1
další	další	k2eAgFnSc4d1
fázi	fáze	k1gFnSc4
občanských	občanský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
překročení	překročení	k1gNnSc6
této	tento	k3xDgFnSc2
řeky	řeka	k1gFnSc2
měl	mít	k5eAaImAgMnS
Caesar	Caesar	k1gMnSc1
pronést	pronést	k5eAaPmF
slavný	slavný	k2eAgInSc4d1
výrok	výrok	k1gInSc4
„	„	k?
<g/>
alea	aleus	k1gMnSc2
iacta	iact	k1gMnSc2
est	est	k?
<g/>
“	“	k?
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
v	v	k7c6
překladu	překlad	k1gInSc6
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
„	„	k?
<g/>
kostky	kostka	k1gFnPc1
jsou	být	k5eAaImIp3nP
vrženy	vržen	k2eAgInPc1d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Optimáti	optimát	k1gMnPc1
<g/>
,	,	kIx,
včetně	včetně	k7c2
Catona	Caton	k1gMnSc2
mladšího	mladý	k2eAgMnSc2d2
a	a	k8xC
Metella	Metell	k1gMnSc2
Scipiona	Scipion	k1gMnSc2
<g/>
,	,	kIx,
uprchli	uprchnout	k5eAaPmAgMnP
z	z	k7c2
Říma	Řím	k1gInSc2
na	na	k7c4
jih	jih	k1gInSc4
<g/>
,	,	kIx,
postrádajíce	postrádat	k5eAaImSgMnP
důvěry	důvěra	k1gFnPc4
v	v	k7c6
nově	nova	k1gFnSc6
odvedené	odvedený	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
<g/>
,	,	kIx,
obzvláště	obzvláště	k6eAd1
když	když	k8xS
se	se	k3xPyFc4
města	město	k1gNnPc1
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Itálii	Itálie	k1gFnSc6
ochotně	ochotně	k6eAd1
vydávala	vydávat	k5eAaImAgFnS,k5eAaPmAgFnS
do	do	k7c2
Caesarových	Caesarových	k2eAgFnPc2d1
rukou	ruka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
senátní	senátní	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
početně	početně	k6eAd1
vysoce	vysoce	k6eAd1
převyšovalo	převyšovat	k5eAaImAgNnS
Caesarovo	Caesarův	k2eAgNnSc1d1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
stále	stále	k6eAd1
disponoval	disponovat	k5eAaBmAgInS
pouze	pouze	k6eAd1
svojí	svojit	k5eAaImIp3nS
XIII	XIII	kA
<g/>
.	.	kIx.
legií	legie	k1gFnPc2
<g/>
,	,	kIx,
Pompeius	Pompeius	k1gMnSc1
se	se	k3xPyFc4
neodvážil	odvážit	k5eNaPmAgMnS
svést	svést	k5eAaPmF
s	s	k7c7
ním	on	k3xPp3gInSc7
bitvu	bitva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesar	Caesar	k1gMnSc1
proto	proto	k8xC
pronásledoval	pronásledovat	k5eAaImAgInS
Pompeia	Pompeius	k1gMnSc4
do	do	k7c2
Brundisia	Brundisium	k1gNnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
doufal	doufat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Pompeia	Pompeia	k1gFnSc1
a	a	k8xC
senátory	senátor	k1gMnPc4
polapí	polapit	k5eAaPmIp3nP
ještě	ještě	k6eAd1
předtím	předtím	k6eAd1
<g/>
,	,	kIx,
než	než	k8xS
spolu	spolu	k6eAd1
s	s	k7c7
legiemi	legie	k1gFnPc7
uniknou	uniknout	k5eAaPmIp3nP
po	po	k7c6
moři	moře	k1gNnSc6
do	do	k7c2
Řecka	Řecko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
ale	ale	k8xC
Caesar	Caesar	k1gMnSc1
konečně	konečně	k6eAd1
obsadil	obsadit	k5eAaPmAgMnS
město	město	k1gNnSc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
již	již	k9
Pompeius	Pompeius	k1gInSc1
s	s	k7c7
většinou	většina	k1gFnSc7
vojska	vojsko	k1gNnSc2
pryč	pryč	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
ke	k	k7c3
stíhání	stíhání	k1gNnSc3
Pompeia	Pompeium	k1gNnSc2
mu	on	k3xPp3gMnSc3
chybělo	chybět	k5eAaImAgNnS
odpovídající	odpovídající	k2eAgNnSc4d1
námořnictvo	námořnictvo	k1gNnSc4
<g/>
,	,	kIx,
rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
Caesar	Caesar	k1gMnSc1
vytáhnout	vytáhnout	k5eAaPmF
proti	proti	k7c3
Pompeiovým	Pompeiový	k2eAgFnPc3d1
legiím	legie	k1gFnPc3
v	v	k7c6
Hispánii	Hispánie	k1gFnSc6
<g/>
,	,	kIx,
neboť	neboť	k8xC
jak	jak	k6eAd1
pravil	pravit	k5eAaBmAgMnS,k5eAaImAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Nejprve	nejprve	k6eAd1
budu	být	k5eAaImBp1nS
bojovat	bojovat	k5eAaImF
proti	proti	k7c3
vojsku	vojsko	k1gNnSc3
bez	bez	k7c2
vojevůdce	vojevůdce	k1gMnSc2
a	a	k8xC
potom	potom	k6eAd1
se	se	k3xPyFc4
obrátím	obrátit	k5eAaPmIp1nS
proti	proti	k7c3
vojevůdci	vojevůdce	k1gMnSc3
bez	bez	k7c2
vojska	vojsko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
Marca	Marcus	k1gMnSc4
Aemilia	Aemilius	k1gMnSc4
Lepida	Lepid	k1gMnSc4
ponechal	ponechat	k5eAaPmAgMnS
jako	jako	k8xS,k8xC
prefekta	prefekt	k1gMnSc2
v	v	k7c6
Římě	Řím	k1gInSc6
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
správu	správa	k1gFnSc4
zbytku	zbytek	k1gInSc2
Itálie	Itálie	k1gFnSc2
svěřil	svěřit	k5eAaPmAgInS
Marcu	Marc	k1gMnSc3
Antoniovi	Antonio	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
pozoruhodného	pozoruhodný	k2eAgNnSc2d1
<g/>
,	,	kIx,
pouhých	pouhý	k2eAgMnPc2d1
dvacet	dvacet	k4xCc1
sedm	sedm	k4xCc1
dní	den	k1gInPc2
trvajícího	trvající	k2eAgInSc2d1
pochodu	pochod	k1gInSc2
oblehl	oblehnout	k5eAaPmAgInS
město	město	k1gNnSc4
Massilii	Massilie	k1gFnSc4
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc1d1
Marseille	Marseille	k1gFnSc1
<g/>
)	)	kIx)
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
Galii	Galie	k1gFnSc6
a	a	k8xC
odsud	odsud	k6eAd1
pokračoval	pokračovat	k5eAaImAgInS
dále	daleko	k6eAd2
do	do	k7c2
Hispánie	Hispánie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
střetl	střetnout	k5eAaPmAgMnS
s	s	k7c7
pompeiovským	pompeiovský	k2eAgNnSc7d1
vojskem	vojsko	k1gNnSc7
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Ilerdy	Ilerda	k1gFnSc2
a	a	k8xC
přesvědčivě	přesvědčivě	k6eAd1
ho	on	k3xPp3gMnSc4
porazil	porazit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
příchodu	příchod	k1gInSc6
do	do	k7c2
Říma	Řím	k1gInSc2
byl	být	k5eAaImAgMnS
Caesar	Caesar	k1gMnSc1
ustaven	ustavit	k5eAaPmNgMnS
diktátorem	diktátor	k1gMnSc7
s	s	k7c7
Marcem	Marce	k1gMnSc7
Antoniem	Antonio	k1gMnSc7
jako	jako	k8xS,k8xC
velitelem	velitel	k1gMnSc7
jízdy	jízda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jedenácti	jedenáct	k4xCc2
dnech	den	k1gInPc6
odstoupil	odstoupit	k5eAaPmAgInS
z	z	k7c2
funkce	funkce	k1gFnSc2
a	a	k8xC
byl	být	k5eAaImAgMnS
podruhé	podruhé	k6eAd1
zvolen	zvolit	k5eAaPmNgMnS
konzulem	konzul	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Kleopatra	Kleopatra	k1gFnSc1
</s>
<s>
Následně	následně	k6eAd1
s	s	k7c7
15	#num#	k4
000	#num#	k4
muži	muž	k1gMnPc7
překročil	překročit	k5eAaPmAgMnS
Jaderské	jaderský	k2eAgNnSc4d1
moře	moře	k1gNnSc4
a	a	k8xC
vypravil	vypravit	k5eAaPmAgMnS
se	se	k3xPyFc4
vypudit	vypudit	k5eAaPmF
Pompeia	Pompeia	k1gFnSc1
z	z	k7c2
Řecka	Řecko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Dyrrhachia	Dyrrhachius	k1gMnSc2
se	se	k3xPyFc4
jen	jen	k9
se	s	k7c7
štěstím	štěstí	k1gNnSc7
vyhnul	vyhnout	k5eAaPmAgMnS
katastrofální	katastrofální	k2eAgFnSc3d1
porážce	porážka	k1gFnSc3
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
pompeiovcům	pompeiovec	k1gMnPc3
podařilo	podařit	k5eAaPmAgNnS
prolomit	prolomit	k5eAaPmF
jeho	jeho	k3xOp3gFnSc4
obranu	obrana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nato	nato	k6eAd1
se	se	k3xPyFc4
stáhl	stáhnout	k5eAaPmAgMnS
do	do	k7c2
Thesálie	Thesálie	k1gFnSc2
a	a	k8xC
při	při	k7c6
postupu	postup	k1gInSc6
se	se	k3xPyFc4
spojil	spojit	k5eAaPmAgMnS
s	s	k7c7
ostatními	ostatní	k2eAgFnPc7d1
částmi	část	k1gFnPc7
svého	svůj	k3xOyFgNnSc2
vojska	vojsko	k1gNnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
zatím	zatím	k6eAd1
dorazily	dorazit	k5eAaPmAgInP
z	z	k7c2
Itálie	Itálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pompeius	Pompeius	k1gInSc4
a	a	k8xC
Caesar	Caesar	k1gMnSc1
se	se	k3xPyFc4
poté	poté	k6eAd1
střetli	střetnout	k5eAaPmAgMnP
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Farsálu	Farsál	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
byl	být	k5eAaImAgInS
Pompeius	Pompeius	k1gInSc1
9	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
roku	rok	k1gInSc2
48	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
přesvědčivě	přesvědčivě	k6eAd1
poražen	porazit	k5eAaPmNgMnS
<g/>
,	,	kIx,
třebaže	třebaže	k8xS
měl	mít	k5eAaImAgMnS
nad	nad	k7c7
Caesarem	Caesar	k1gMnSc7
převahu	převah	k1gInSc2
v	v	k7c6
pěchotě	pěchota	k1gFnSc6
i	i	k8xC
jízdě	jízda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
Pompeiových	Pompeiový	k2eAgMnPc2d1
stoupenců	stoupenec	k1gMnPc2
a	a	k8xC
senátorů	senátor	k1gMnPc2
se	se	k3xPyFc4
rozprchlo	rozprchnout	k5eAaPmAgNnS
do	do	k7c2
různých	různý	k2eAgInPc2d1
koutů	kout	k1gInPc2
římského	římský	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
zde	zde	k6eAd1
podněcovali	podněcovat	k5eAaImAgMnP
další	další	k2eAgInSc4d1
odpor	odpor	k1gInSc4
proti	proti	k7c3
Caesarovi	Caesar	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k2eAgMnPc1d1
obdrželi	obdržet	k5eAaPmAgMnP
od	od	k7c2
Caesara	Caesar	k1gMnSc2
milost	milost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pompeius	Pompeius	k1gMnSc1
uprchl	uprchnout	k5eAaPmAgMnS
po	po	k7c6
své	svůj	k3xOyFgFnSc6
porážce	porážka	k1gFnSc6
do	do	k7c2
Egypta	Egypt	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
ale	ale	k9
na	na	k7c4
příkaz	příkaz	k1gInSc4
krále	král	k1gMnSc2
Ptolemaia	Ptolemaios	k1gMnSc2
XIII	XIII	kA
<g/>
.	.	kIx.
zákeřně	zákeřně	k6eAd1
zavražděn	zavraždit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s>
Caesar	Caesar	k1gMnSc1
pronásledoval	pronásledovat	k5eAaImAgMnS
Pompeia	Pompeius	k1gMnSc4
do	do	k7c2
Alexandrie	Alexandrie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
mu	on	k3xPp3gMnSc3
však	však	k9
byla	být	k5eAaImAgFnS
předána	předat	k5eAaPmNgFnS
pouze	pouze	k6eAd1
uťatá	uťatý	k2eAgFnSc1d1
hlava	hlava	k1gFnSc1
jeho	on	k3xPp3gMnSc2,k3xOp3gMnSc2
rivala	rival	k1gMnSc2
a	a	k8xC
někdejšího	někdejší	k2eAgMnSc4d1
spojence	spojenec	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
nechal	nechat	k5eAaPmAgMnS
Caesar	Caesar	k1gMnSc1
přepravit	přepravit	k5eAaPmF
a	a	k8xC
pohřbít	pohřbít	k5eAaPmF
Pompeiovy	Pompeiův	k2eAgInPc4d1
ostatky	ostatek	k1gInPc4
s	s	k7c7
veškerými	veškerý	k3xTgFnPc7
poctami	pocta	k1gFnPc7
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
chtěl	chtít	k5eAaImAgMnS
demonstrovat	demonstrovat	k5eAaBmF
svoji	svůj	k3xOyFgFnSc4
mírnost	mírnost	k1gFnSc4
k	k	k7c3
mrtvému	mrtvý	k2eAgMnSc3d1
nepříteli	nepřítel	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Alexandrii	Alexandrie	k1gFnSc6
se	se	k3xPyFc4
Caesar	Caesar	k1gMnSc1
zapletl	zaplést	k5eAaPmAgMnS
do	do	k7c2
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
mezi	mezi	k7c7
Ptolemaiem	Ptolemaios	k1gMnSc7
a	a	k8xC
jeho	jeho	k3xOp3gFnSc7
sestrou	sestra	k1gFnSc7
<g/>
,	,	kIx,
manželkou	manželka	k1gFnSc7
a	a	k8xC
spoluvládkyní	spoluvládkyně	k1gFnSc7
<g/>
,	,	kIx,
královnou	královna	k1gFnSc7
Kleopatrou	Kleopatra	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patrně	patrně	k6eAd1
kvůli	kvůli	k7c3
Ptolemaiově	Ptolemaiův	k2eAgFnSc3d1
roli	role	k1gFnSc3
v	v	k7c6
Pompeiově	Pompeiův	k2eAgNnSc6d1
zavraždění	zavraždění	k1gNnSc6
se	se	k3xPyFc4
Caesar	Caesar	k1gMnSc1
přidal	přidat	k5eAaPmAgMnS
na	na	k7c4
stranu	strana	k1gFnSc4
Kleopatry	Kleopatra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následné	následný	k2eAgFnSc6d1
„	„	k?
<g/>
alexandrijské	alexandrijský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
“	“	k?
byl	být	k5eAaImAgMnS
Caesar	Caesar	k1gMnSc1
nejprve	nejprve	k6eAd1
zatlačen	zatlačit	k5eAaPmNgMnS
do	do	k7c2
defenzívy	defenzíva	k1gFnSc2
a	a	k8xC
obklíčen	obklíčit	k5eAaPmNgMnS
ve	v	k7c6
městě	město	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
obležení	obležení	k1gNnSc2
lehla	lehnout	k5eAaPmAgFnS
popelem	popel	k1gInSc7
slavná	slavný	k2eAgFnSc1d1
alexandrijská	alexandrijský	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
obdržení	obdržení	k1gNnSc6
posil	posít	k5eAaPmAgMnS
Caesar	Caesar	k1gMnSc1
snadno	snadno	k6eAd1
zvítězil	zvítězit	k5eAaPmAgMnS
nad	nad	k7c7
královou	králův	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
a	a	k8xC
sám	sám	k3xTgMnSc1
Ptolemaios	Ptolemaios	k1gMnSc1
při	při	k7c6
útěku	útěk	k1gInSc6
utonul	utonout	k5eAaPmAgMnS
v	v	k7c6
řece	řeka	k1gFnSc6
Nilu	Nil	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesar	Caesar	k1gMnSc1
dosadil	dosadit	k5eAaPmAgMnS
Kleopatru	Kleopatra	k1gFnSc4
na	na	k7c4
egyptský	egyptský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
otcem	otec	k1gMnSc7
jejího	její	k3xOp3gMnSc2
syna	syn	k1gMnSc2
Ptolemaia	Ptolemaios	k1gMnSc2
XV	XV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesara	Caesar	k1gMnSc4
známého	známý	k1gMnSc4
jako	jako	k8xS,k8xC
Kaisarion	Kaisarion	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
několika	několik	k4yIc6
měsících	měsíc	k1gInPc6
strávených	strávený	k2eAgInPc2d1
v	v	k7c6
Egyptě	Egypt	k1gInSc6
u	u	k7c2
Kleopatry	Kleopatra	k1gFnSc2
se	se	k3xPyFc4
Caesar	Caesar	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
47	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
obrátil	obrátit	k5eAaPmAgMnS
do	do	k7c2
Malé	Malé	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
proti	proti	k7c3
pontskému	pontský	k2eAgMnSc3d1
králi	král	k1gMnSc3
Farnakovi	Farnak	k1gMnSc3
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
přepadl	přepadnout	k5eAaPmAgInS
tamější	tamější	k2eAgFnPc4d1
římské	římský	k2eAgFnPc4d1
provincie	provincie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
krátkém	krátký	k2eAgNnSc6d1
<g/>
,	,	kIx,
pětidenním	pětidenní	k2eAgNnSc6d1
tažení	tažení	k1gNnSc6
byl	být	k5eAaImAgInS
Farnakés	Farnakés	k1gInSc1
poražen	porazit	k5eAaPmNgInS
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Zely	zet	k5eAaImAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Senátu	senát	k1gInSc2
poslal	poslat	k5eAaPmAgMnS
poté	poté	k6eAd1
Caesar	Caesar	k1gMnSc1
stručný	stručný	k2eAgInSc4d1
komentář	komentář	k1gInSc4
svého	svůj	k3xOyFgNnSc2
vítězství	vítězství	k1gNnSc2
<g/>
:	:	kIx,
„	„	k?
<g/>
Veni	Veni	k1gNnSc1
<g/>
,	,	kIx,
vidi	vidi	kA
<g/>
,	,	kIx,
vici	vici	k6eAd1
<g/>
“	“	k?
(	(	kIx(
<g/>
„	„	k?
<g/>
Přišel	přijít	k5eAaPmAgMnS
jsem	být	k5eAaImIp1nS
<g/>
,	,	kIx,
viděl	vidět	k5eAaImAgMnS
jsem	být	k5eAaImIp1nS
<g/>
,	,	kIx,
zvítězil	zvítězit	k5eAaPmAgMnS
jsem	být	k5eAaImIp1nS
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odtud	odtud	k6eAd1
se	se	k3xPyFc4
vypravil	vypravit	k5eAaPmAgInS
do	do	k7c2
Afriky	Afrika	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
vypořádal	vypořádat	k5eAaPmAgInS
se	s	k7c7
zbývajícími	zbývající	k2eAgMnPc7d1
Pompeiovými	Pompeiový	k2eAgMnPc7d1
straníky	straník	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Thapsu	Thaps	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
46	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
rychle	rychle	k6eAd1
přemohl	přemoct	k5eAaPmAgMnS
republikánské	republikánský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
vedené	vedený	k2eAgNnSc1d1
Metellem	Metell	k1gMnSc7
Scipionem	Scipion	k1gInSc7
(	(	kIx(
<g/>
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
v	v	k7c6
bitvě	bitva	k1gFnSc6
padl	padnout	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
)	)	kIx)
a	a	k8xC
Catonem	Caton	k1gMnSc7
mladším	mladý	k2eAgMnSc7d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
této	tento	k3xDgFnSc6
vojenské	vojenský	k2eAgFnSc6d1
katastrofě	katastrofa	k1gFnSc6
spáchal	spáchat	k5eAaPmAgInS
Cato	Cato	k6eAd1
v	v	k7c6
Utice	Utika	k1gFnSc6
sebevraždu	sebevražda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Království	království	k1gNnSc1
Numidie	Numidie	k1gFnPc1
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
podporovalo	podporovat	k5eAaImAgNnS
pompeiovce	pompeiovec	k1gInPc4
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
Caesarem	Caesar	k1gMnSc7
přeměněno	přeměnit	k5eAaPmNgNnS
v	v	k7c4
římskou	římský	k2eAgFnSc4d1
provincii	provincie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
Pompeiovi	Pompeiův	k2eAgMnPc1d1
synové	syn	k1gMnPc1
<g/>
,	,	kIx,
Gnaeus	Gnaeus	k1gMnSc1
Pompeius	Pompeius	k1gMnSc1
a	a	k8xC
Sextus	Sextus	k1gMnSc1
Pompeius	Pompeius	k1gMnSc1
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
Titem	Tit	k1gMnSc7
Labienem	Labien	k1gMnSc7
<g/>
,	,	kIx,
bývalým	bývalý	k2eAgMnSc7d1
Caesarovým	Caesarův	k2eAgMnSc7d1
legátem	legát	k1gMnSc7
z	z	k7c2
dob	doba	k1gFnPc2
galských	galský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
<g/>
,	,	kIx,
unikli	uniknout	k5eAaPmAgMnP
do	do	k7c2
Hispánie	Hispánie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
krátkém	krátký	k2eAgInSc6d1
pobytu	pobyt	k1gInSc6
v	v	k7c6
Římě	Řím	k1gInSc6
se	se	k3xPyFc4
je	být	k5eAaImIp3nS
Caesar	Caesar	k1gMnSc1
vydal	vydat	k5eAaPmAgMnS
stíhat	stíhat	k5eAaImF
a	a	k8xC
v	v	k7c6
březnu	březen	k1gInSc6
roku	rok	k1gInSc2
45	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
je	být	k5eAaImIp3nS
porazil	porazit	k5eAaPmAgInS
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Mundy	Munda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Republikáni	republikán	k1gMnPc1
resp.	resp.	kA
pompeiovci	pompeiovec	k1gMnPc1
byli	být	k5eAaImAgMnP
definitivně	definitivně	k6eAd1
poraženi	poražen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pročež	pročež	k6eAd1
republika	republika	k1gFnSc1
fakticky	fakticky	k6eAd1
dospěla	dochvít	k5eAaPmAgFnS
ke	k	k7c3
svému	svůj	k3xOyFgInSc3
zániku	zánik	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Diktatura	diktatura	k1gFnSc1
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
45	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
se	se	k3xPyFc4
Caesar	Caesar	k1gMnSc1
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
Itálie	Itálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
předchozích	předchozí	k2eAgNnPc6d1
letech	léto	k1gNnPc6
byl	být	k5eAaImAgMnS
zvolen	zvolen	k2eAgMnSc1d1
nejprve	nejprve	k6eAd1
s	s	k7c7
Lepidem	Lepid	k1gInSc7
potřetí	potřetí	k4xO
a	a	k8xC
poté	poté	k6eAd1
bez	bez	k7c2
kolegy	kolega	k1gMnSc2
počtvrté	počtvrté	k4xO
konzulem	konzul	k1gMnSc7
(	(	kIx(
<g/>
ve	v	k7c6
druhém	druhý	k4xOgInSc6
případě	případ	k1gInSc6
na	na	k7c4
dobu	doba	k1gFnSc4
desíti	deset	k4xCc2
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
z	z	k7c2
prvních	první	k4xOgNnPc2
opatření	opatření	k1gNnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc4
učinil	učinit	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
sepsání	sepsání	k1gNnSc3
závěti	závěť	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
ustanovil	ustanovit	k5eAaPmAgMnS
Gaia	Gaia	k1gMnSc1
Octavia	octavia	k1gFnSc1
svým	svůj	k1gMnSc7
dědicem	dědic	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rozporu	rozpor	k1gInSc6
s	s	k7c7
očekáváním	očekávání	k1gNnSc7
nepřistoupil	přistoupit	k5eNaPmAgMnS
Caesar	Caesar	k1gMnSc1
k	k	k7c3
proskripcím	proskripce	k1gFnPc3
svých	svůj	k3xOyFgMnPc2
nepřátel	nepřítel	k1gMnPc2
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
udělil	udělit	k5eAaPmAgMnS
milost	milost	k1gFnSc4
každému	každý	k3xTgMnSc3
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
proti	proti	k7c3
němu	on	k3xPp3gMnSc3
postavil	postavit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
počest	počest	k1gFnSc4
Caesarových	Caesarových	k2eAgNnPc2d1
velkolepých	velkolepý	k2eAgNnPc2d1
vítězství	vítězství	k1gNnPc2
byly	být	k5eAaImAgFnP
uspořádány	uspořádat	k5eAaPmNgFnP
honosné	honosný	k2eAgFnPc1d1
hry	hra	k1gFnPc1
a	a	k8xC
jiné	jiný	k2eAgFnPc1d1
slavnosti	slavnost	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesarovi	Caesar	k1gMnSc3
bylo	být	k5eAaImAgNnS
přiznáno	přiznán	k2eAgNnSc1d1
právo	právo	k1gNnSc1
nosit	nosit	k5eAaImF
při	při	k7c6
všech	všecek	k3xTgFnPc6
veřejných	veřejný	k2eAgFnPc6d1
příležitostech	příležitost	k1gFnPc6
roucho	roucho	k1gNnSc1
triumfátora	triumfátor	k1gMnSc2
včetně	včetně	k7c2
purpurové	purpurový	k2eAgFnSc2d1
roby	roba	k1gFnSc2
(	(	kIx(
<g/>
do	do	k7c2
níž	jenž	k3xRgFnSc2
se	se	k3xPyFc4
odívali	odívat	k5eAaImAgMnP
římští	římský	k2eAgMnPc1d1
králové	král	k1gMnPc1
<g/>
)	)	kIx)
a	a	k8xC
vavřínového	vavřínový	k2eAgInSc2d1
věnce	věnec	k1gInSc2
<g/>
,	,	kIx,
jímž	jenž	k3xRgInSc7
chtěl	chtít	k5eAaImAgMnS
zakrýt	zakrýt	k5eAaPmF
svoji	svůj	k3xOyFgFnSc4
pleš	pleš	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
státní	státní	k2eAgInPc4d1
náklady	náklad	k1gInPc4
bylo	být	k5eAaImAgNnS
Caesarovi	Caesar	k1gMnSc3
postaveno	postaven	k2eAgNnSc1d1
v	v	k7c6
Římě	Řím	k1gInSc6
nádherné	nádherný	k2eAgNnSc1d1
sídlo	sídlo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Senát	senát	k1gInSc1
mu	on	k3xPp3gMnSc3
dále	daleko	k6eAd2
přiznal	přiznat	k5eAaPmAgMnS
titul	titul	k1gInSc4
doživotního	doživotní	k2eAgMnSc2d1
diktátora	diktátor	k1gMnSc2
(	(	kIx(
<g/>
dictator	dictator	k1gMnSc1
perpetuus	perpetuus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
jakýchkoli	jakýkoli	k3yIgNnPc6
náboženských	náboženský	k2eAgNnPc6d1
procesích	procesí	k1gNnPc6
byla	být	k5eAaImAgFnS
nesena	nesen	k2eAgFnSc1d1
Caesarova	Caesarův	k2eAgFnSc1d1
socha	socha	k1gFnSc1
ze	z	k7c2
slonoviny	slonovina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiná	jiný	k2eAgFnSc1d1
Caesarova	Caesarův	k2eAgFnSc1d1
socha	socha	k1gFnSc1
s	s	k7c7
nápisem	nápis	k1gInSc7
„	„	k?
<g/>
neporazitelnému	porazitelný	k2eNgMnSc3d1
bohu	bůh	k1gMnSc3
<g/>
“	“	k?
byla	být	k5eAaImAgFnS
umístěna	umístit	k5eAaPmNgFnS
v	v	k7c6
Quirinově	Quirinův	k2eAgInSc6d1
chrámu	chrám	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgMnSc7
se	se	k3xPyFc4
stavěl	stavět	k5eAaImAgMnS
na	na	k7c4
roveň	roveň	k1gFnSc4
nejen	nejen	k6eAd1
bohům	bůh	k1gMnPc3
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k8xC
římským	římský	k2eAgMnPc3d1
králům	král	k1gMnPc3
<g/>
,	,	kIx,
protože	protože	k8xS
v	v	k7c6
Quirinovi	Quirin	k1gMnSc6
byl	být	k5eAaImAgMnS
uctíván	uctíván	k2eAgMnSc1d1
zakladatel	zakladatel	k1gMnSc1
města	město	k1gNnSc2
–	–	k?
Romulus	Romulus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgFnSc1
socha	socha	k1gFnSc1
byla	být	k5eAaImAgFnS
vztyčena	vztyčit	k5eAaPmNgFnS
na	na	k7c6
Kapitolu	Kapitol	k1gInSc6
vedle	vedle	k7c2
soch	socha	k1gFnPc2
sedmi	sedm	k4xCc2
římských	římský	k2eAgMnPc2d1
králů	král	k1gMnPc2
a	a	k8xC
Lucia	Lucius	k1gMnSc4
Junia	Junius	k1gMnSc4
Bruta	Brut	k1gMnSc4
<g/>
,	,	kIx,
Římana	Říman	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
měl	mít	k5eAaImAgMnS
podle	podle	k7c2
legendy	legenda	k1gFnSc2
vyhnat	vyhnat	k5eAaPmF
Tarquinia	Tarquinium	k1gNnPc1
Superba	Superba	k1gMnSc1
<g/>
,	,	kIx,
posledního	poslední	k2eAgInSc2d1
z	z	k7c2
králů	král	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
tak	tak	k6eAd1
neslýchané	slýchaný	k2eNgNnSc4d1
jednání	jednání	k1gNnSc4
završil	završit	k5eAaPmAgMnS
Caesar	Caesar	k1gMnSc1
ražením	ražení	k1gNnSc7
peněz	peníze	k1gInPc2
s	s	k7c7
vlastním	vlastní	k2eAgInSc7d1
portrétem	portrét	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
v	v	k7c6
římských	římský	k2eAgFnPc6d1
dějinách	dějiny	k1gFnPc6
se	se	k3xPyFc4
tak	tak	k6eAd1
stalo	stát	k5eAaPmAgNnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgMnS
žijící	žijící	k2eAgMnSc1d1
Říman	Říman	k1gMnSc1
vyobrazován	vyobrazován	k2eAgMnSc1d1
na	na	k7c6
mincích	mince	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Caesar	Caesar	k1gMnSc1
byl	být	k5eAaImAgMnS
první	první	k4xOgInSc4
žijící	žijící	k2eAgInSc4d1
osobou	osoba	k1gFnSc7
vyobrazenou	vyobrazený	k2eAgFnSc7d1
na	na	k7c6
římských	římský	k2eAgFnPc6d1
mincích	mince	k1gFnPc6
</s>
<s>
V	v	k7c6
říjnu	říjen	k1gInSc6
45	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
se	se	k3xPyFc4
Caesar	Caesar	k1gMnSc1
vzdal	vzdát	k5eAaPmAgMnS
svého	svůj	k3xOyFgInSc2
čtvrtého	čtvrtý	k4xOgInSc2
konzulátu	konzulát	k1gInSc2
a	a	k8xC
místo	místo	k1gNnSc4
sebe	sebe	k3xPyFc4
určil	určit	k5eAaPmAgMnS
za	za	k7c7
konzuly	konzul	k1gMnPc7
Quinta	Quinta	k1gFnSc1
Fabia	fabia	k1gFnSc1
Maxima	Maxima	k1gFnSc1
a	a	k8xC
Gaia	Gaia	k1gFnSc1
Trebonia	Trebonium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
krokem	krok	k1gInSc7
popudil	popudit	k5eAaPmAgInS
senát	senát	k1gInSc1
<g/>
,	,	kIx,
neboť	neboť	k8xC
tak	tak	k6eAd1
úmyslně	úmyslně	k6eAd1
ignoroval	ignorovat	k5eAaImAgMnS
republikánský	republikánský	k2eAgInSc4d1
systém	systém	k1gInSc4
voleb	volba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
senátoři	senátor	k1gMnPc1
pokračovali	pokračovat	k5eAaImAgMnP
ve	v	k7c6
vzdávání	vzdávání	k1gNnSc6
poct	pocta	k1gFnPc2
Caesarovi	Caesar	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
postavení	postavení	k1gNnSc6
chrámu	chrám	k1gInSc2
bohyně	bohyně	k1gFnSc2
Libertas	Libertasa	k1gFnPc2
<g/>
,	,	kIx,
obdržel	obdržet	k5eAaPmAgMnS
Caesar	Caesar	k1gMnSc1
titul	titul	k1gInSc4
liberator	liberator	k1gInSc4
(	(	kIx(
<g/>
„	„	k?
<g/>
Osvoboditel	osvoboditel	k1gMnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesar	Caesar	k1gMnSc1
získal	získat	k5eAaPmAgMnS
také	také	k9
jako	jako	k9
jediný	jediný	k2eAgMnSc1d1
Říman	Říman	k1gMnSc1
bezprecedentní	bezprecedentní	k2eAgNnSc4d1
právo	právo	k1gNnSc4
vlastního	vlastní	k2eAgNnSc2d1
imperia	imperium	k1gNnSc2
<g/>
,	,	kIx,
takže	takže	k8xS
byl	být	k5eAaImAgInS
chráněn	chránit	k5eAaImNgInS
před	před	k7c7
jakoukoli	jakýkoli	k3yIgFnSc7
žalobou	žaloba	k1gFnSc7
a	a	k8xC
fakticky	fakticky	k6eAd1
stál	stát	k5eAaImAgMnS
nad	nad	k7c7
zákony	zákon	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
přiznáno	přiznán	k2eAgNnSc4d1
právo	právo	k1gNnSc4
dosazovat	dosazovat	k5eAaImF
polovinu	polovina	k1gFnSc4
všech	všecek	k3xTgInPc2
magistrátů	magistrát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měsíc	měsíc	k1gInSc1
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
narození	narození	k1gNnSc2
<g/>
,	,	kIx,
Quintilis	Quintilis	k1gFnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
přejmenován	přejmenovat	k5eAaPmNgInS
na	na	k7c6
Julius	Julius	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Datum	datum	k1gNnSc1
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
narození	narození	k1gNnSc2
bylo	být	k5eAaImAgNnS
uznáno	uznat	k5eAaPmNgNnS
za	za	k7c4
státní	státní	k2eAgInSc4d1
svátek	svátek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokonce	dokonce	k9
i	i	k9
Comitia	Comitia	k1gFnSc1
tributa	tribut	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
pojmenována	pojmenovat	k5eAaPmNgFnS
po	po	k7c6
něm.	něm.	k?
</s>
<s>
Kromě	kromě	k7c2
přijímání	přijímání	k1gNnSc2
různých	různý	k2eAgInPc2d1
titulů	titul	k1gInPc2
a	a	k8xC
poct	pocta	k1gFnPc2
však	však	k9
Caesar	Caesar	k1gMnSc1
prokázal	prokázat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
mu	on	k3xPp3gMnSc3
leží	ležet	k5eAaImIp3nS
na	na	k7c6
srdci	srdce	k1gNnSc6
rovněž	rovněž	k9
prospěch	prospěch	k1gInSc4
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Římě	Řím	k1gInSc6
sice	sice	k8xC
strávil	strávit	k5eAaPmAgMnS
jen	jen	k9
málo	málo	k1gNnSc4
času	čas	k1gInSc2
v	v	k7c6
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc7
vládcem	vládce	k1gMnSc7
<g/>
,	,	kIx,
přesto	přesto	k8xC
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
v	v	k7c6
tomto	tento	k3xDgNnSc6
krátkém	krátký	k2eAgNnSc6d1
období	období	k1gNnSc6
podařilo	podařit	k5eAaPmAgNnS
zavést	zavést	k5eAaPmF
množství	množství	k1gNnSc1
reforem	reforma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
před	před	k7c7
vítězným	vítězný	k2eAgNnSc7d1
završením	završení	k1gNnSc7
občanských	občanský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
vyvíjel	vyvíjet	k5eAaImAgMnS
Caesar	Caesar	k1gMnSc1
rozsáhlou	rozsáhlý	k2eAgFnSc4d1
zákonodárnou	zákonodárný	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kodifikací	kodifikace	k1gFnPc2
a	a	k8xC
přepracováním	přepracování	k1gNnSc7
zákonů	zákon	k1gInPc2
plánoval	plánovat	k5eAaImAgMnS
zásadně	zásadně	k6eAd1
reformovat	reformovat	k5eAaBmF
římský	římský	k2eAgInSc1d1
stát	stát	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zjednodušil	zjednodušit	k5eAaPmAgMnS
možnost	možnost	k1gFnSc4
nabytí	nabytí	k1gNnSc2
římského	římský	k2eAgNnSc2d1
občanství	občanství	k1gNnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc4
hojně	hojně	k6eAd1
uděloval	udělovat	k5eAaImAgMnS
galským	galský	k2eAgMnSc7d1
<g/>
,	,	kIx,
hispánským	hispánský	k2eAgMnSc7d1
a	a	k8xC
africkým	africký	k2eAgMnPc3d1
provinciálům	provinciál	k1gMnPc3
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
započal	započnout	k5eAaPmAgMnS
romanizaci	romanizace	k1gFnSc3
těchto	tento	k3xDgFnPc2
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
nechal	nechat	k5eAaPmAgInS
zvýšit	zvýšit	k5eAaPmF
počet	počet	k1gInSc1
senátorů	senátor	k1gMnPc2
ze	z	k7c2
600	#num#	k4
na	na	k7c4
900	#num#	k4
a	a	k8xC
vřadil	vřadit	k5eAaPmAgInS
mezi	mezi	k7c4
ně	on	k3xPp3gMnPc4
dokonce	dokonce	k9
některé	některý	k3yIgInPc4
galské	galský	k2eAgInPc4d1
náčelníky	náčelník	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
navýšil	navýšit	k5eAaPmAgMnS
počty	počet	k1gInPc4
většiny	většina	k1gFnSc2
magistrátů	magistrát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Caesar	Caesar	k1gMnSc1
dal	dát	k5eAaPmAgMnS
na	na	k7c6
území	území	k1gNnSc6
celého	celý	k2eAgInSc2d1
římského	římský	k2eAgInSc2d1
státu	stát	k1gInSc2
založit	založit	k5eAaPmF
dvacet	dvacet	k4xCc4
nových	nový	k2eAgFnPc2d1
kolonií	kolonie	k1gFnPc2
<g/>
,	,	kIx,
do	do	k7c2
nichž	jenž	k3xRgFnPc2
se	se	k3xPyFc4
mělo	mít	k5eAaImAgNnS
vystěhovat	vystěhovat	k5eAaPmF
obyvatelstvo	obyvatelstvo	k1gNnSc1
přelidněného	přelidněný	k2eAgInSc2d1
Říma	Řím	k1gInSc2
spolu	spolu	k6eAd1
s	s	k7c7
veterány	veterán	k1gMnPc7
<g/>
,	,	kIx,
jimž	jenž	k3xRgMnPc3
zaopatřil	zaopatřit	k5eAaPmAgMnS
půdu	půda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kartágo	Kartágo	k1gNnSc1
a	a	k8xC
Korint	Korint	k1gInSc1
<g/>
,	,	kIx,
města	město	k1gNnPc1
zničená	zničený	k2eAgNnPc1d1
Římany	Říman	k1gMnPc4
před	před	k7c7
více	hodně	k6eAd2
než	než	k8xS
sto	sto	k4xCgNnSc4
lety	léto	k1gNnPc7
<g/>
,	,	kIx,
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
obnovena	obnoven	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimoto	mimoto	k6eAd1
hodlal	hodlat	k5eAaImAgInS
vysušit	vysušit	k5eAaPmF
Pomptinské	Pomptinský	k2eAgInPc4d1
močály	močál	k1gInPc4
rozkládající	rozkládající	k2eAgInPc4d1
se	se	k3xPyFc4
nedaleko	nedaleko	k7c2
Říma	Řím	k1gInSc2
<g/>
,	,	kIx,
stavět	stavět	k5eAaImF
silnice	silnice	k1gFnPc4
a	a	k8xC
prokopat	prokopat	k5eAaPmF
Korintskou	korintský	k2eAgFnSc4d1
šíji	šíje	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Římě	Řím	k1gInSc6
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
byl	být	k5eAaImAgInS
dosud	dosud	k6eAd1
vcelku	vcelku	k6eAd1
prostým	prostý	k2eAgNnSc7d1
městem	město	k1gNnSc7
<g/>
,	,	kIx,
měly	mít	k5eAaImAgFnP
být	být	k5eAaImF
realizovány	realizovat	k5eAaBmNgFnP
ambiciózní	ambiciózní	k2eAgFnPc1d1
veřejné	veřejný	k2eAgFnPc1d1
stavby	stavba	k1gFnPc1
podle	podle	k7c2
helénistického	helénistický	k2eAgInSc2d1
vzoru	vzor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budova	budova	k1gFnSc1
senátu	senát	k1gInSc2
Curia	curium	k1gNnSc2
Hostilia	Hostilium	k1gNnSc2
byla	být	k5eAaImAgFnS
přestavěna	přestavět	k5eAaPmNgFnS
a	a	k8xC
od	od	k7c2
tohoto	tento	k3xDgInSc2
okamžiku	okamžik	k1gInSc2
byla	být	k5eAaImAgNnP
tudíž	tudíž	k8xC
nazývána	nazýván	k2eAgNnPc1d1
Curia	curium	k1gNnPc1
Iulia	Iulius	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
vybudováno	vybudovat	k5eAaPmNgNnS
Caesarovo	Caesarův	k2eAgNnSc1d1
fórum	fórum	k1gNnSc1
s	s	k7c7
chrámem	chrám	k1gInSc7
Venuše	Venuše	k1gFnSc2
Venetrix	Venetrix	k1gInSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
Venuše	Venuše	k1gFnSc2
rodičky	rodička	k1gFnSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomerium	Pomerium	k1gNnSc1
<g/>
,	,	kIx,
posvátné	posvátný	k2eAgFnPc1d1
hranice	hranice	k1gFnPc1
města	město	k1gNnSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
rozšířeno	rozšířit	k5eAaPmNgNnS
z	z	k7c2
důvodu	důvod	k1gInSc2
dalšího	další	k2eAgInSc2d1
předpokládaného	předpokládaný	k2eAgInSc2d1
růstu	růst	k1gInSc2
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
své	svůj	k3xOyFgFnSc6
reformní	reformní	k2eAgFnSc6d1
činnosti	činnost	k1gFnSc6
se	se	k3xPyFc4
Caesar	Caesar	k1gMnSc1
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
i	i	k9
řešení	řešení	k1gNnSc4
rozličných	rozličný	k2eAgInPc2d1
společenských	společenský	k2eAgInPc2d1
neduhů	neduh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Schválil	schválit	k5eAaPmAgMnS
zákon	zákon	k1gInSc4
zakazující	zakazující	k2eAgInSc4d1
římským	římský	k2eAgMnSc7d1
občanům	občan	k1gMnPc3
opustit	opustit	k5eAaPmF
Itálii	Itálie	k1gFnSc4
na	na	k7c4
dobu	doba	k1gFnSc4
delší	dlouhý	k2eAgFnSc4d2
než	než	k8xS
tři	tři	k4xCgInPc1
roky	rok	k1gInPc1
(	(	kIx(
<g/>
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
vojáků	voják	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taktéž	Taktéž	k?
prosadil	prosadit	k5eAaPmAgMnS
zákon	zákon	k1gInSc4
<g/>
,	,	kIx,
podle	podle	k7c2
něhož	jenž	k3xRgInSc2
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
příslušník	příslušník	k1gMnSc1
společenské	společenský	k2eAgFnSc2d1
elity	elita	k1gFnSc2
zranil	zranit	k5eAaPmAgMnS
nebo	nebo	k8xC
zabil	zabít	k5eAaPmAgInS
člena	člen	k1gMnSc4
nižší	nízký	k2eAgFnSc2d2
společenské	společenský	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
<g/>
,	,	kIx,
veškeré	veškerý	k3xTgNnSc4
bohatství	bohatství	k1gNnSc4
pachatele	pachatel	k1gMnSc2
bylo	být	k5eAaImAgNnS
zkonfiskováno	zkonfiskovat	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesar	Caesar	k1gMnSc1
dal	dát	k5eAaPmAgMnS
zrušit	zrušit	k5eAaPmF
čtvrtinu	čtvrtina	k1gFnSc4
všech	všecek	k3xTgInPc2
dluhů	dluh	k1gInPc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
upevnil	upevnit	k5eAaPmAgInS
svoji	svůj	k3xOyFgFnSc4
popularitu	popularita	k1gFnSc4
v	v	k7c6
řadách	řada	k1gFnPc6
prostého	prostý	k2eAgInSc2d1
lidu	lid	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k8xS,k8xC
pontifex	pontifex	k1gMnSc1
maximus	maximus	k1gInSc4
měl	mít	k5eAaImAgMnS
Caesar	Caesar	k1gMnSc1
spravovat	spravovat	k5eAaImF
záležitosti	záležitost	k1gFnPc4
počítání	počítání	k1gNnSc2
času	čas	k1gInSc2
a	a	k8xC
kalendáře	kalendář	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
radu	rada	k1gFnSc4
astronoma	astronom	k1gMnSc2
Sósigena	Sósigen	k1gMnSc2
z	z	k7c2
Alexandrie	Alexandrie	k1gFnSc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
zavedl	zavést	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
46	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
nový	nový	k2eAgInSc4d1
kalendář	kalendář	k1gInSc4
s	s	k7c7
365	#num#	k4
dny	den	k1gInPc7
a	a	k8xC
s	s	k7c7
každým	každý	k3xTgInSc7
čtvrtým	čtvrtý	k4xOgInSc7
rokem	rok	k1gInSc7
přestupným	přestupný	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
juliánský	juliánský	k2eAgInSc1d1
kalendář	kalendář	k1gInSc1
byl	být	k5eAaImAgInS
upraven	upravit	k5eAaPmNgInS
papežem	papež	k1gMnSc7
Řehořem	Řehoř	k1gMnSc7
XIII	XIII	kA
<g/>
.	.	kIx.
v	v	k7c6
roce	rok	k1gInSc6
1582	#num#	k4
a	a	k8xC
tím	ten	k3xDgNnSc7
vznikl	vzniknout	k5eAaPmAgInS
moderní	moderní	k2eAgInSc4d1
gregoriánský	gregoriánský	k2eAgInSc4d1
kalendář	kalendář	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Spiknutí	spiknutí	k1gNnSc1
a	a	k8xC
Caesarova	Caesarův	k2eAgFnSc1d1
smrt	smrt	k1gFnSc1
</s>
<s>
Busta	busta	k1gFnSc1
Julia	Julius	k1gMnSc2
Caesara	Caesar	k1gMnSc2
<g/>
,	,	kIx,
Antická	antický	k2eAgFnSc1d1
sbírka	sbírka	k1gFnSc1
v	v	k7c6
Berlíně	Berlín	k1gInSc6
</s>
<s>
Antičtí	antický	k2eAgMnPc1d1
historikové	historik	k1gMnPc1
popisují	popisovat	k5eAaImIp3nP
rostoucí	rostoucí	k2eAgNnSc1d1
napětí	napětí	k1gNnSc1
mezi	mezi	k7c7
Caesarem	Caesar	k1gMnSc7
a	a	k8xC
senátem	senát	k1gInSc7
pramenící	pramenící	k2eAgFnSc2d1
z	z	k7c2
Caesarových	Caesarových	k2eAgInPc2d1
možných	možný	k2eAgInPc2d1
nároků	nárok	k1gInPc2
na	na	k7c4
královský	královský	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otázka	otázka	k1gFnSc1
<g/>
,	,	kIx,
zda	zda	k8xS
Caesar	Caesar	k1gMnSc1
skutečně	skutečně	k6eAd1
usiloval	usilovat	k5eAaImAgMnS
o	o	k7c4
tento	tento	k3xDgInSc4
titul	titul	k1gInSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
se	se	k3xPyFc4
spokojil	spokojit	k5eAaPmAgMnS
s	s	k7c7
funkcí	funkce	k1gFnSc7
diktátora	diktátor	k1gMnSc2
<g/>
,	,	kIx,
zaměstnává	zaměstnávat	k5eAaImIp3nS
historiky	historik	k1gMnPc4
prakticky	prakticky	k6eAd1
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jisté	jistý	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Caesarovo	Caesarův	k2eAgNnSc1d1
postavení	postavení	k1gNnSc1
bylo	být	k5eAaImAgNnS
vskutku	vskutku	k9
královské	královský	k2eAgNnSc1d1
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
ale	ale	k8xC
nenalezl	nalézt	k5eNaBmAgMnS,k5eNaPmAgMnS
způsob	způsob	k1gInSc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
přimět	přimět	k5eAaPmF
Římany	Říman	k1gMnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
smířili	smířit	k5eAaPmAgMnP
s	s	k7c7
nastolením	nastolení	k1gNnSc7
monarchie	monarchie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláště	zvláště	k6eAd1
mezi	mezi	k7c7
konzervativními	konzervativní	k2eAgMnPc7d1
optimáty	optimát	k1gMnPc7
a	a	k8xC
stoupenci	stoupenec	k1gMnPc7
republiky	republika	k1gFnSc2
narůstal	narůstat	k5eAaImAgInS
neklid	neklid	k1gInSc4
a	a	k8xC
odpor	odpor	k1gInSc4
proti	proti	k7c3
Caesarovi	Caesar	k1gMnSc3
<g/>
,	,	kIx,
v	v	k7c6
čemž	což	k3yRnSc6,k3yQnSc6
lze	lze	k6eAd1
spatřovat	spatřovat	k5eAaImF
pohnutky	pohnutka	k1gFnPc4
a	a	k8xC
motivy	motiv	k1gInPc4
pro	pro	k7c4
Caesarovo	Caesarův	k2eAgNnSc4d1
pozdější	pozdní	k2eAgNnSc4d2
zavraždění	zavraždění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Suetonius	Suetonius	k1gMnSc1
popisuje	popisovat	k5eAaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
při	při	k7c6
Caesarově	Caesarův	k2eAgInSc6d1
návratu	návrat	k1gInSc6
někteří	některý	k3yIgMnPc1
lidé	člověk	k1gMnPc1
z	z	k7c2
davu	dav	k1gInSc2
vložili	vložit	k5eAaPmAgMnP
na	na	k7c4
nedalekou	daleký	k2eNgFnSc4d1
Caesarovu	Caesarův	k2eAgFnSc4d1
sochu	socha	k1gFnSc4
vavřínový	vavřínový	k2eAgInSc4d1
věnec	věnec	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tribunové	tribunový	k2eAgFnPc4d1
lidu	lid	k1gInSc2
Gaius	Gaius	k1gMnSc1
Epidius	Epidius	k1gMnSc1
Marcellus	Marcellus	k1gMnSc1
a	a	k8xC
Lucius	Lucius	k1gMnSc1
Caesetius	Caesetius	k1gMnSc1
Flavius	Flavius	k1gMnSc1
nařídili	nařídit	k5eAaPmAgMnP
věnec	věnec	k1gInSc4
odstranit	odstranit	k5eAaPmF
jako	jako	k9
symbol	symbol	k1gInSc1
boha	bůh	k1gMnSc2
Jova	Jova	k1gMnSc1
a	a	k8xC
království	království	k1gNnSc1
<g/>
,	,	kIx,
za	za	k7c4
což	což	k3yQnSc4,k3yRnSc4
Caesar	Caesar	k1gMnSc1
následně	následně	k6eAd1
oba	dva	k4xCgMnPc4
tribuny	tribun	k1gMnPc4
zbavil	zbavit	k5eAaPmAgMnS
jejich	jejich	k3xOp3gInSc2
úřadu	úřad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
tohoto	tento	k3xDgInSc2
momentu	moment	k1gInSc2
se	se	k3xPyFc4
Caesar	Caesar	k1gMnSc1
prý	prý	k9
již	již	k6eAd1
nebyl	být	k5eNaImAgInS
schopen	schopen	k2eAgInSc1d1
oprostit	oprostit	k5eAaPmF
od	od	k7c2
podezření	podezření	k1gNnSc2
<g/>
,	,	kIx,
že	že	k8xS
usiluje	usilovat	k5eAaImIp3nS
o	o	k7c4
královládu	královláda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
ještě	ještě	k6eAd1
přiživila	přiživit	k5eAaPmAgFnS
příhoda	příhoda	k1gFnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ho	on	k3xPp3gInSc4
dav	dav	k1gInSc4
tituloval	titulovat	k5eAaImAgMnS
jako	jako	k9
krále	král	k1gMnSc4
<g/>
,	,	kIx,
načež	načež	k6eAd1
Caesar	Caesar	k1gMnSc1
nepříliš	příliš	k6eNd1
přesvědčivě	přesvědčivě	k6eAd1
odpověděl	odpovědět	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Nejsem	být	k5eNaImIp1nS
král	král	k1gMnSc1
<g/>
,	,	kIx,
jsem	být	k5eAaImIp1nS
Caesar	Caesar	k1gMnSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
Při	při	k7c6
slavnosti	slavnost	k1gFnSc6
luperkálií	luperkálie	k1gFnPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Caesar	Caesar	k1gMnSc1
pronášel	pronášet	k5eAaImAgMnS
projev	projev	k1gInSc4
<g/>
,	,	kIx,
Marcus	Marcus	k1gInSc1
Antonius	Antonius	k1gInSc1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
opakovaně	opakovaně	k6eAd1
pokoušel	pokoušet	k5eAaImAgMnS
vložit	vložit	k5eAaPmF
na	na	k7c4
hlavu	hlava	k1gFnSc4
diadém	diadém	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Suetonius	Suetonius	k1gInSc1
dále	daleko	k6eAd2
podotýká	podotýkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Lucius	Lucius	k1gMnSc1
Cotta	Cotta	k1gMnSc1
vyzval	vyzvat	k5eAaPmAgMnS
senát	senát	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
učinil	učinit	k5eAaImAgMnS,k5eAaPmAgMnS
Caesara	Caesar	k1gMnSc4
králem	král	k1gMnSc7
<g/>
,	,	kIx,
protože	protože	k8xS
podle	podle	k7c2
proroctví	proroctví	k1gNnPc2
Sibyliných	Sibylin	k2eAgFnPc2d1
knih	kniha	k1gFnPc2
mohl	moct	k5eAaImAgMnS
pouze	pouze	k6eAd1
král	král	k1gMnSc1
porazit	porazit	k5eAaPmF
Parthy	Parth	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Senátoři	senátor	k1gMnPc1
udělili	udělit	k5eAaPmAgMnP
Caesarovi	Caesarův	k2eAgMnPc1d1
titul	titul	k1gInSc4
pater	patro	k1gNnPc2
patriae	patria	k1gMnSc2
(	(	kIx(
<g/>
„	„	k?
<g/>
otec	otec	k1gMnSc1
vlasti	vlast	k1gFnSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
a	a	k8xC
dále	daleko	k6eAd2
ho	on	k3xPp3gMnSc4
jmenovali	jmenovat	k5eAaImAgMnP,k5eAaBmAgMnP
diktátorem	diktátor	k1gMnSc7
na	na	k7c4
doživotí	doživotí	k1gNnSc4
(	(	kIx(
<g/>
dictator	dictator	k1gMnSc1
perpetuus	perpetuus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Cassia	Cassius	k1gMnSc2
Diona	Dion	k1gMnSc2
<g/>
,	,	kIx,
když	když	k8xS
v	v	k7c6
roce	rok	k1gInSc6
44	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
přišla	přijít	k5eAaPmAgFnS
k	k	k7c3
Caesarovi	Caesar	k1gMnSc3
ze	z	k7c2
senátu	senát	k1gInSc2
delegace	delegace	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ho	on	k3xPp3gMnSc4
informovala	informovat	k5eAaBmAgFnS
o	o	k7c4
udělení	udělení	k1gNnSc4
těchto	tento	k3xDgFnPc2
poct	pocta	k1gFnPc2
<g/>
,	,	kIx,
Caesar	Caesar	k1gMnSc1
je	být	k5eAaImIp3nS
prý	prý	k9
přijal	přijmout	k5eAaPmAgMnS
vsedě	vsedě	k6eAd1
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
se	se	k3xPyFc4
obtěžoval	obtěžovat	k5eAaImAgMnS
vstát	vstát	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	to	k9
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
prý	prý	k9
poslední	poslední	k2eAgFnSc1d1
kapka	kapka	k1gFnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
uražené	uražený	k2eAgMnPc4d1
senátory	senátor	k1gMnPc4
přiměla	přimět	k5eAaPmAgFnS
uvažovat	uvažovat	k5eAaImF
o	o	k7c6
jeho	jeho	k3xOp3gNnSc6
zavraždění	zavraždění	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesarovi	Caesarův	k2eAgMnPc1d1
příznivci	příznivec	k1gMnPc1
měli	mít	k5eAaImAgMnP
jeho	jeho	k3xOp3gNnSc4
selhání	selhání	k1gNnSc4
omlouvat	omlouvat	k5eAaImF
záchvatem	záchvat	k1gInSc7
průjmu	průjem	k1gInSc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
jeho	jeho	k3xOp3gMnPc1
odpůrci	odpůrce	k1gMnPc1
si	se	k3xPyFc3
to	ten	k3xDgNnSc4
vykládali	vykládat	k5eAaImAgMnP
jako	jako	k8xS,k8xC
projev	projev	k1gInSc4
neúcty	neúcta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Caesarovy	Caesarův	k2eAgFnPc1d1
plné	plný	k2eAgFnPc1d1
moci	moc	k1gFnPc1
vyvolávaly	vyvolávat	k5eAaImAgFnP
v	v	k7c6
mnoha	mnoho	k4c6
senátorech	senátor	k1gMnPc6
nedůvěru	nedůvěra	k1gFnSc4
<g/>
,	,	kIx,
závist	závist	k1gFnSc4
a	a	k8xC
především	především	k9
strach	strach	k1gInSc4
z	z	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
Caesar	Caesar	k1gMnSc1
nastolí	nastolit	k5eAaPmIp3nS
skutečnou	skutečný	k2eAgFnSc4d1
tyranii	tyranie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marcus	Marcus	k1gMnSc1
Junius	Junius	k1gMnSc1
Brutus	Brutus	k1gMnSc1
spolu	spolu	k6eAd1
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
přítelem	přítel	k1gMnSc7
a	a	k8xC
švagrem	švagr	k1gMnSc7
Gaiem	Gai	k1gMnSc7
Cassiem	Cassius	k1gMnSc7
a	a	k8xC
dalšími	další	k2eAgMnPc7d1
muži	muž	k1gMnPc7
<g/>
,	,	kIx,
nazývajícími	nazývající	k2eAgMnPc7d1
sami	sám	k3xTgMnPc1
sebe	sebe	k3xPyFc4
„	„	k?
<g/>
osvoboditeli	osvoboditel	k1gMnSc3
<g/>
“	“	k?
(	(	kIx(
<g/>
liberatores	liberatores	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
začali	začít	k5eAaPmAgMnP
proto	proto	k6eAd1
připravovat	připravovat	k5eAaImF
Caesarovo	Caesarův	k2eAgNnSc4d1
zavraždění	zavraždění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
spiklence	spiklenec	k1gMnPc4
náleželi	náležet	k5eAaImAgMnP
kromě	kromě	k7c2
optimátů	optimát	k1gMnPc2
také	také	k9
populárové	populár	k1gMnPc1
<g/>
,	,	kIx,
nepřátelé	nepřítel	k1gMnPc1
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
přátelé	přítel	k1gMnPc1
Caesara	Caesar	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkový	celkový	k2eAgInSc1d1
počet	počet	k1gInSc1
osob	osoba	k1gFnPc2
zapojených	zapojený	k2eAgFnPc2d1
do	do	k7c2
komplotu	komplot	k1gInSc2
čítal	čítat	k5eAaImAgInS
zřejmě	zřejmě	k6eAd1
kolem	kolem	k7c2
šedesáti	šedesát	k4xCc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dva	dva	k4xCgInPc4
dny	den	k1gInPc4
před	před	k7c7
činem	čin	k1gInSc7
se	se	k3xPyFc4
Cassius	Cassius	k1gMnSc1
setkal	setkat	k5eAaPmAgMnS
s	s	k7c7
některými	některý	k3yIgMnPc7
z	z	k7c2
nich	on	k3xPp3gMnPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
jim	on	k3xPp3gMnPc3
doporučil	doporučit	k5eAaPmAgInS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
v	v	k7c6
případě	případ	k1gInSc6
odhalení	odhalení	k1gNnSc2
obrátili	obrátit	k5eAaPmAgMnP
své	své	k1gNnSc4
nože	nůž	k1gInSc2
proti	proti	k7c3
sobě	se	k3xPyFc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
březnové	březnový	k2eAgFnPc4d1
idy	idy	k1gFnPc4
(	(	kIx(
<g/>
15	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
<g/>
)	)	kIx)
44	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
pozvala	pozvat	k5eAaPmAgFnS
skupina	skupina	k1gFnSc1
senátorů	senátor	k1gMnPc2
Caesara	Caesar	k1gMnSc2
na	na	k7c6
zasedání	zasedání	k1gNnSc6
senátu	senát	k1gInSc2
v	v	k7c6
Pompeiově	Pompeiův	k2eAgNnSc6d1
divadle	divadlo	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
měl	mít	k5eAaImAgMnS
vyslechnout	vyslechnout	k5eAaPmF
žádost	žádost	k1gFnSc4
dotazující	dotazující	k2eAgFnSc4d1
se	se	k3xPyFc4
ho	on	k3xPp3gNnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
vzdá	vzdát	k5eAaPmIp3nS
moci	moct	k5eAaImF
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
však	však	k9
bylo	být	k5eAaImAgNnS
jenom	jenom	k6eAd1
záminkou	záminka	k1gFnSc7
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
vylákání	vylákání	k1gNnSc3
<g/>
.	.	kIx.
14	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
prý	prý	k9
při	při	k7c6
návštěvě	návštěva	k1gFnSc6
velitele	velitel	k1gMnSc2
jízdy	jízda	k1gFnSc2
na	na	k7c4
otázku	otázka	k1gFnSc4
<g/>
,	,	kIx,
jaká	jaký	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
smrt	smrt	k1gFnSc1
je	být	k5eAaImIp3nS
nejlepší	dobrý	k2eAgFnSc1d3
<g/>
,	,	kIx,
Caesar	Caesar	k1gMnSc1
odpověděl	odpovědět	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
neočekávaná	očekávaný	k2eNgFnSc1d1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ráno	ráno	k6eAd1
15	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
postihla	postihnout	k5eAaPmAgFnS
Caesara	Caesar	k1gMnSc4
nevolnost	nevolnost	k1gFnSc4
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
čemuž	což	k3yQnSc3,k3yRnSc3
zamýšlel	zamýšlet	k5eAaImAgMnS
odložit	odložit	k5eAaPmF
zasedání	zasedání	k1gNnSc4
senátu	senát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
jeho	jeho	k3xOp3gFnSc3
manželce	manželka	k1gFnSc3
Calpurnii	Calpurnie	k1gFnSc3
se	se	k3xPyFc4
v	v	k7c6
noci	noc	k1gFnSc6
zdálo	zdát	k5eAaImAgNnS
o	o	k7c6
jeho	jeho	k3xOp3gNnSc6
zavraždění	zavraždění	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
haruspex	haruspex	k1gMnSc1
vyčetl	vyčíst	k5eAaPmAgMnS
z	z	k7c2
věštby	věštba	k1gFnSc2
velmi	velmi	k6eAd1
neblahá	blahý	k2eNgNnPc4d1
znamení	znamení	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Decimus	Decimus	k1gMnSc1
Junius	Junius	k1gMnSc1
Brutus	Brutus	k1gMnSc1
<g/>
,	,	kIx,
taktéž	taktéž	k?
zapletený	zapletený	k2eAgMnSc1d1
do	do	k7c2
spiknutí	spiknutí	k1gNnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
proto	proto	k8xC
vydal	vydat	k5eAaPmAgInS
k	k	k7c3
váhajícímu	váhající	k2eAgMnSc3d1
diktátorovi	diktátor	k1gMnSc3
a	a	k8xC
přesvědčil	přesvědčit	k5eAaPmAgMnS
ho	on	k3xPp3gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
jednání	jednání	k1gNnSc4
neodkládal	odkládat	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
se	se	k3xPyFc4
společně	společně	k6eAd1
Caesar	Caesar	k1gMnSc1
a	a	k8xC
Decimus	Decimus	k1gMnSc1
Brutus	Brutus	k1gMnSc1
odebrali	odebrat	k5eAaPmAgMnP
do	do	k7c2
divadla	divadlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
cesty	cesta	k1gFnSc2
obdržel	obdržet	k5eAaPmAgMnS
Caesar	Caesar	k1gMnSc1
dopis	dopis	k1gInSc4
s	s	k7c7
varováním	varování	k1gNnSc7
o	o	k7c6
atentátu	atentát	k1gInSc6
<g/>
,	,	kIx,
ten	ten	k3xDgInSc1
jej	on	k3xPp3gMnSc4
ale	ale	k8xC
odložil	odložit	k5eAaPmAgMnS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
ho	on	k3xPp3gMnSc4
přečte	přečíst	k5eAaPmIp3nS
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marcus	Marcus	k1gMnSc1
Antonius	Antonius	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
měl	mít	k5eAaImAgMnS
nejasné	jasný	k2eNgNnSc4d1
tušení	tušení	k1gNnSc4
o	o	k7c6
komplotu	komplot	k1gInSc6
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc4
nabyl	nabýt	k5eAaPmAgInS
předchozí	předchozí	k2eAgFnPc4d1
noci	noc	k1gFnPc4
po	po	k7c6
setkání	setkání	k1gNnSc6
s	s	k7c7
rozrušeným	rozrušený	k2eAgNnSc7d1
Serviliem	Servilium	k1gNnSc7
Cascou	Casca	k1gMnSc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
obával	obávat	k5eAaImAgInS
nejhoršího	zlý	k2eAgMnSc2d3
a	a	k8xC
chystal	chystat	k5eAaImAgMnS
se	se	k3xPyFc4
Caesara	Caesar	k1gMnSc2
zadržet	zadržet	k5eAaPmF
na	na	k7c6
schodech	schod	k1gInPc6
před	před	k7c7
Pompeiovým	Pompeiový	k2eAgNnSc7d1
divadlem	divadlo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
jeden	jeden	k4xCgMnSc1
ze	z	k7c2
spiklenců	spiklenec	k1gMnPc2
zde	zde	k6eAd1
s	s	k7c7
ním	on	k3xPp3gNnSc7
pod	pod	k7c7
jakousi	jakýsi	k3yIgFnSc7
záminkou	záminka	k1gFnSc7
zapředl	zapříst	k5eAaPmAgMnS
dlouhý	dlouhý	k2eAgInSc4d1
rozhovor	rozhovor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Smrt	smrt	k1gFnSc1
Caesara	Caesar	k1gMnSc2
od	od	k7c2
Vincenza	Vincenz	k1gMnSc2
Camucciniho	Camuccini	k1gMnSc2
</s>
<s>
V	v	k7c6
senátu	senát	k1gInSc6
se	se	k3xPyFc4
Lucius	Lucius	k1gMnSc1
Tillius	Tillius	k1gMnSc1
Cimber	Cimber	k1gMnSc1
obrátil	obrátit	k5eAaPmAgMnS
na	na	k7c4
Caesara	Caesar	k1gMnSc4
s	s	k7c7
prosbou	prosba	k1gFnSc7
o	o	k7c4
milost	milost	k1gFnSc4
pro	pro	k7c4
svého	svůj	k3xOyFgMnSc4
bratra	bratr	k1gMnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
ale	ale	k8xC
diktátor	diktátor	k1gMnSc1
odmítl	odmítnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
Cimber	Cimber	k1gMnSc1
začal	začít	k5eAaPmAgMnS
strhávat	strhávat	k5eAaImF
z	z	k7c2
Caesara	Caesar	k1gMnSc2
jeho	jeho	k3xOp3gNnSc1
togu	togu	k5eAaPmIp1nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spiklenci	spiklenec	k1gMnSc3
se	se	k3xPyFc4
po	po	k7c6
tomto	tento	k3xDgNnSc6
smluveném	smluvený	k2eAgNnSc6d1
znamení	znamení	k1gNnSc6
shlukli	shluknout	k5eAaPmAgMnP
kolem	kolem	k7c2
diktátora	diktátor	k1gMnSc2
<g/>
,	,	kIx,
načež	načež	k6eAd1
ho	on	k3xPp3gMnSc4
Casca	Casca	k1gMnSc1
říznul	říznout	k5eAaPmAgMnS
svojí	svůj	k3xOyFgFnSc7
dýkou	dýka	k1gFnSc7
do	do	k7c2
krku	krk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesar	Caesar	k1gMnSc1
se	se	k3xPyFc4
obrátil	obrátit	k5eAaPmAgMnS
proti	proti	k7c3
Cascovi	Casec	k1gMnSc3
a	a	k8xC
obořil	obořit	k5eAaPmAgInS
se	se	k3xPyFc4
na	na	k7c4
něho	on	k3xPp3gMnSc4
slovy	slovo	k1gNnPc7
<g/>
:	:	kIx,
„	„	k?
<g/>
Praničemný	praničemný	k2eAgMnSc1d1
Casco	Casco	k1gMnSc1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
to	ten	k3xDgNnSc4
děláš	dělat	k5eAaImIp2nS
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Vystrašený	vystrašený	k2eAgMnSc1d1
Casca	Casca	k1gMnSc1
zvolal	zvolat	k5eAaPmAgMnS
řecky	řecky	k6eAd1
ke	k	k7c3
svým	svůj	k3xOyFgInPc3
druhům	druh	k1gInPc3
o	o	k7c4
pomoc	pomoc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
krátké	krátký	k2eAgFnSc6d1
chvíli	chvíle	k1gFnSc6
se	se	k3xPyFc4
všichni	všechen	k3xTgMnPc1
spiklenci	spiklenec	k1gMnPc1
<g/>
,	,	kIx,
včetně	včetně	k7c2
Bruta	Brut	k1gInSc2
<g/>
,	,	kIx,
vrhli	vrhnout	k5eAaPmAgMnP,k5eAaImAgMnP
s	s	k7c7
obnaženými	obnažený	k2eAgInPc7d1
noži	nůž	k1gInPc7
na	na	k7c4
diktátora	diktátor	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesar	Caesar	k1gMnSc1
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgMnS
uniknout	uniknout	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
sevřen	sevřen	k2eAgInSc1d1
nepřáteli	nepřítel	k1gMnPc7
a	a	k8xC
oslepen	oslepit	k5eAaPmNgMnS
vlastní	vlastní	k2eAgFnSc7d1
krví	krev	k1gFnSc7
zakopl	zakopnout	k5eAaPmAgMnS
a	a	k8xC
upadl	upadnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spiklenci	spiklenec	k1gMnPc1
mu	on	k3xPp3gMnSc3
zasadili	zasadit	k5eAaPmAgMnP
údajně	údajně	k6eAd1
třiadvacet	třiadvacet	k4xCc4
ran	rána	k1gFnPc2
<g/>
,	,	kIx,
jimž	jenž	k3xRgFnPc3
Caesar	Caesar	k1gMnSc1
podlehl	podlehnout	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesarova	Caesarův	k2eAgNnPc1d1
poslední	poslední	k2eAgNnPc1d1
slova	slovo	k1gNnPc1
nejsou	být	k5eNaImIp3nP
známa	znám	k2eAgNnPc1d1
zcela	zcela	k6eAd1
s	s	k7c7
jistotou	jistota	k1gFnSc7
a	a	k8xC
nadále	nadále	k6eAd1
zůstávají	zůstávat	k5eAaImIp3nP
předmětem	předmět	k1gInSc7
zkoumání	zkoumání	k1gNnSc2
badatelů	badatel	k1gMnPc2
a	a	k8xC
historiků	historik	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Shakespearově	Shakespearův	k2eAgNnSc6d1
dramatu	drama	k1gNnSc6
Julius	Julius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
znějí	znět	k5eAaImIp3nP
jeho	jeho	k3xOp3gNnPc1
poslední	poslední	k2eAgNnPc1d1
slova	slovo	k1gNnPc1
Et	Et	k1gFnSc2
tu	tu	k6eAd1
<g/>
,	,	kIx,
Brute	Brut	k1gInSc5
<g/>
?	?	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
„	„	k?
<g/>
I	i	k8xC
ty	ty	k3xPp2nSc1
<g/>
,	,	kIx,
Brute	Brut	k1gMnSc5
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
však	však	k9
o	o	k7c4
pouhou	pouhý	k2eAgFnSc4d1
Shakespearovu	Shakespearův	k2eAgFnSc4d1
uměleckou	umělecký	k2eAgFnSc4d1
invenci	invence	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Suetonia	Suetonium	k1gNnSc2
pravil	pravit	k5eAaBmAgMnS,k5eAaImAgMnS
řecky	řecky	k6eAd1
Kai	Kai	k1gMnSc1
sí	sí	k?
<g/>
,	,	kIx,
teknón	teknón	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
„	„	k?
<g/>
I	i	k8xC
ty	ty	k3xPp2nSc1
<g/>
,	,	kIx,
můj	můj	k1gMnSc5
synu	syn	k1gMnSc5
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plútarchos	Plútarchos	k1gMnSc1
udává	udávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Caesar	Caesar	k1gMnSc1
neřekl	říct	k5eNaPmAgMnS
nic	nic	k3yNnSc1
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
když	když	k8xS
spatřil	spatřit	k5eAaPmAgMnS
mezi	mezi	k7c7
spiklenci	spiklenec	k1gMnPc7
Bruta	Bruto	k1gNnSc2
<g/>
,	,	kIx,
přehodil	přehodit	k5eAaPmAgInS
si	se	k3xPyFc3
tógu	tóga	k1gFnSc4
přes	přes	k7c4
hlavu	hlava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
dokončení	dokončení	k1gNnSc6
činu	čin	k1gInSc2
se	se	k3xPyFc4
Brutus	Brutus	k1gMnSc1
pokusil	pokusit	k5eAaPmAgMnS
promluvit	promluvit	k5eAaPmF
<g/>
,	,	kIx,
avšak	avšak	k8xC
vyděšení	vyděšený	k2eAgMnPc1d1
senátoři	senátor	k1gMnPc1
se	se	k3xPyFc4
ihned	ihned	k6eAd1
rozutekli	rozutéct	k5eAaPmAgMnP
na	na	k7c4
všechny	všechen	k3xTgFnPc4
strany	strana	k1gFnPc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Caesarovo	Caesarův	k2eAgNnSc1d1
zakrvácené	zakrvácený	k2eAgNnSc1d1
tělo	tělo	k1gNnSc1
leželo	ležet	k5eAaImAgNnS
u	u	k7c2
podstavce	podstavec	k1gInSc2
Pompeiovy	Pompeiův	k2eAgFnSc2d1
sochy	socha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spiklenci	spiklenec	k1gMnSc3
volající	volající	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
osvobodili	osvobodit	k5eAaPmAgMnP
Řím	Řím	k1gInSc4
<g/>
,	,	kIx,
vyběhli	vyběhnout	k5eAaPmAgMnP
na	na	k7c4
fórum	fórum	k1gNnSc4
a	a	k8xC
ukazovali	ukazovat	k5eAaImAgMnP
svoje	svůj	k3xOyFgFnPc4
zkrvavené	zkrvavený	k2eAgFnPc4d1
ruce	ruka	k1gFnPc4
ohromenému	ohromený	k2eAgInSc3d1
lidu	lid	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antonius	Antonius	k1gMnSc1
<g/>
,	,	kIx,
Lepidus	Lepidus	k1gMnSc1
a	a	k8xC
ostatní	ostatní	k2eAgMnPc1d1
senátoři	senátor	k1gMnPc1
se	se	k3xPyFc4
v	v	k7c6
panice	panika	k1gFnSc6
rozprchli	rozprchnout	k5eAaPmAgMnP
do	do	k7c2
bezpečí	bezpečí	k1gNnSc2
svých	svůj	k3xOyFgInPc2
domovů	domov	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
triumfující	triumfující	k2eAgMnPc1d1
„	„	k?
<g/>
osvoboditelé	osvoboditel	k1gMnPc1
<g/>
“	“	k?
neměli	mít	k5eNaImAgMnP
žádný	žádný	k3yNgInSc4
plán	plán	k1gInSc4
jak	jak	k6eAd1
postupovat	postupovat	k5eAaImF
po	po	k7c6
Caesarově	Caesarův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znejistělí	znejistělý	k2eAgMnPc1d1
nedostatkem	nedostatek	k1gInSc7
veřejné	veřejný	k2eAgFnSc2d1
podpory	podpora	k1gFnSc2
pro	pro	k7c4
svůj	svůj	k3xOyFgInSc4
čin	čin	k1gInSc4
se	se	k3xPyFc4
stáhli	stáhnout	k5eAaPmAgMnP
ve	v	k7c6
zmatku	zmatek	k1gInSc6
do	do	k7c2
chrámu	chrám	k1gInSc2
Jova	Jova	k1gMnSc1
Nejlepšího	dobrý	k2eAgMnSc2d3
a	a	k8xC
Největšího	veliký	k2eAgMnSc2d3
<g/>
,	,	kIx,
kde	kde	k6eAd1
vyčkávali	vyčkávat	k5eAaImAgMnP
dalšího	další	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k1gNnSc1
očekávané	očekávaný	k2eAgFnSc2d1
svobody	svoboda	k1gFnSc2
se	se	k3xPyFc4
Římanům	Říman	k1gMnPc3
dostalo	dostat	k5eAaPmAgNnS
dalších	další	k2eAgNnPc2d1
třinácti	třináct	k4xCc2
let	léto	k1gNnPc2
občanských	občanský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Následný	následný	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
</s>
<s>
Brutovo	Brutův	k2eAgNnSc4d1
rozhodnutí	rozhodnutí	k1gNnSc4
neodstranit	odstranit	k5eNaPmF
spolu	spolu	k6eAd1
s	s	k7c7
Caesarem	Caesar	k1gMnSc7
také	také	k9
Marca	Marcum	k1gNnSc2
Antonia	Antonio	k1gMnSc2
bylo	být	k5eAaImAgNnS
později	pozdě	k6eAd2
hodnoceno	hodnotit	k5eAaImNgNnS
jako	jako	k8xC,k8xS
osudový	osudový	k2eAgInSc1d1
omyl	omyl	k1gInSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
podrobil	podrobit	k5eAaPmAgMnS
kritice	kritika	k1gFnSc3
i	i	k8xC
Cicero	cicero	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marcus	Marcus	k1gMnSc1
Antonius	Antonius	k1gMnSc1
byl	být	k5eAaImAgMnS
nyní	nyní	k6eAd1
jako	jako	k8xS,k8xC
jediný	jediný	k2eAgMnSc1d1
konzul	konzul	k1gMnSc1
formálně	formálně	k6eAd1
v	v	k7c6
čele	čelo	k1gNnSc6
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejprve	nejprve	k6eAd1
získal	získat	k5eAaPmAgInS
podporu	podpora	k1gFnSc4
Lepida	Lepid	k1gMnSc2
<g/>
,	,	kIx,
kterému	který	k3yQgMnSc3,k3yIgMnSc3,k3yRgMnSc3
jako	jako	k9
veliteli	velitel	k1gMnPc7
jízdy	jízda	k1gFnSc2
podléhala	podléhat	k5eAaImAgNnP
Caesarova	Caesarův	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
mimo	mimo	k7c4
Řím	Řím	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedlouho	nedlouho	k1gNnSc1
po	po	k7c6
západu	západ	k1gInSc6
slunce	slunce	k1gNnSc1
si	se	k3xPyFc3
od	od	k7c2
Calpurnie	Calpurnie	k1gFnSc2
vymohl	vymoct	k5eAaPmAgMnS
Caesarovy	Caesarův	k2eAgFnPc4d1
písemnosti	písemnost	k1gFnPc4
a	a	k8xC
pokladnici	pokladnice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
si	se	k3xPyFc3
takto	takto	k6eAd1
zabezpečil	zabezpečit	k5eAaPmAgMnS
legie	legie	k1gFnPc4
a	a	k8xC
finance	finance	k1gFnPc4
<g/>
,	,	kIx,
ohlásil	ohlásit	k5eAaPmAgMnS
na	na	k7c4
další	další	k2eAgInSc4d1
den	den	k1gInSc4
zasedání	zasedání	k1gNnSc2
senátu	senát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
jeho	jeho	k3xOp3gNnPc2
jednání	jednání	k1gNnPc2
se	se	k3xPyFc4
domluvil	domluvit	k5eAaPmAgInS
s	s	k7c7
Caesarovými	Caesarův	k2eAgMnPc7d1
vrahy	vrah	k1gMnPc7
na	na	k7c6
kompromisu	kompromis	k1gInSc6
<g/>
,	,	kIx,
podle	podle	k7c2
něhož	jenž	k3xRgInSc2
se	se	k3xPyFc4
výměnou	výměna	k1gFnSc7
za	za	k7c4
amnestii	amnestie	k1gFnSc4
zavázali	zavázat	k5eAaPmAgMnP
respektovat	respektovat	k5eAaImF
Caesarovy	Caesarův	k2eAgInPc4d1
zákony	zákon	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
tentýž	týž	k3xTgInSc4
den	den	k1gInSc4
otevřel	otevřít	k5eAaPmAgMnS
Piso	Pisa	k1gFnSc5
závěť	závěť	k1gFnSc1
svého	svůj	k3xOyFgMnSc2
zetě	zeť	k1gMnSc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
Caesar	Caesar	k1gMnSc1
odkázal	odkázat	k5eAaPmAgMnS
své	svůj	k3xOyFgFnPc4
zahrady	zahrada	k1gFnPc4
římským	římský	k2eAgMnPc3d1
občanům	občan	k1gMnPc3
a	a	k8xC
každému	každý	k3xTgMnSc3
obyvateli	obyvatel	k1gMnSc3
města	město	k1gNnSc2
zanechal	zanechat	k5eAaPmAgMnS
část	část	k1gFnSc4
svých	svůj	k3xOyFgMnPc2
peněz	peníze	k1gInPc2
<g/>
.	.	kIx.
20	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
se	se	k3xPyFc4
na	na	k7c6
Foru	forum	k1gNnSc6
Romanu	Romana	k1gFnSc4
konal	konat	k5eAaImAgInS
Caesarův	Caesarův	k2eAgInSc1d1
pohřeb	pohřeb	k1gInSc1
<g/>
,	,	kIx,
během	během	k7c2
něhož	jenž	k3xRgInSc2
pronesl	pronést	k5eAaPmAgMnS
Antonius	Antonius	k1gMnSc1
krátkou	krátký	k2eAgFnSc4d1
smuteční	smuteční	k2eAgFnSc4d1
řeč	řeč	k1gFnSc4
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc3
vzbouřil	vzbouřit	k5eAaPmAgInS
přítomný	přítomný	k2eAgInSc1d1
lid	lid	k1gInSc1
roznícený	roznícený	k2eAgInSc1d1
pohledem	pohled	k1gInSc7
na	na	k7c4
zkrvavenou	zkrvavený	k2eAgFnSc4d1
Caesarovu	Caesarův	k2eAgFnSc4d1
tógu	tóga	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leckteří	leckterý	k3yIgMnPc1
vrazi	vrah	k1gMnPc1
padli	padnout	k5eAaPmAgMnP,k5eAaImAgMnP
té	ten	k3xDgFnSc6
noci	noc	k1gFnSc6
za	za	k7c4
oběť	oběť	k1gFnSc4
rozvášněnému	rozvášněný	k2eAgInSc3d1
davu	dav	k1gInSc3
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
ostatní	ostatní	k2eAgMnPc1d1
unikli	uniknout	k5eAaPmAgMnP
z	z	k7c2
města	město	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
chtěli	chtít	k5eAaImAgMnP
osvobodit	osvobodit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
bylo	být	k5eAaImAgNnS
Caesarovo	Caesarův	k2eAgNnSc1d1
tělo	tělo	k1gNnSc1
spáleno	spálit	k5eAaPmNgNnS
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
poté	poté	k6eAd1
vystavěn	vystavěn	k2eAgInSc1d1
chrám	chrám	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
byl	být	k5eAaImAgInS
uctíván	uctívat	k5eAaImNgMnS
jako	jako	k9
bůh	bůh	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
dubnu	duben	k1gInSc6
dorazil	dorazit	k5eAaPmAgMnS
do	do	k7c2
Říma	Řím	k1gInSc2
Caesarův	Caesarův	k2eAgMnSc1d1
prasynovec	prasynovec	k1gMnSc1
Gaius	Gaius	k1gMnSc1
Octavius	Octavius	k1gMnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc1
Caesar	Caesar	k1gMnSc1
ustavil	ustavit	k5eAaPmAgMnS
svým	svůj	k3xOyFgMnSc7
hlavním	hlavní	k2eAgMnSc7d1
dědicem	dědic	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
mnohem	mnohem	k6eAd1
závažnější	závažný	k2eAgFnSc1d2
byla	být	k5eAaImAgFnS
skutečnost	skutečnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
Caesar	Caesar	k1gMnSc1
Octavia	octavia	k1gFnSc1
také	také	k6eAd1
adoptoval	adoptovat	k5eAaPmAgMnS
za	za	k7c4
svého	svůj	k3xOyFgMnSc4
syna	syn	k1gMnSc4
<g/>
,	,	kIx,
pročež	pročež	k6eAd1
Octavius	Octavius	k1gMnSc1
přijal	přijmout	k5eAaPmAgMnS
jeho	jeho	k3xOp3gNnSc4
jméno	jméno	k1gNnSc4
a	a	k8xC
vystupoval	vystupovat	k5eAaImAgMnS
jako	jako	k9
Caesarův	Caesarův	k2eAgMnSc1d1
oprávněný	oprávněný	k2eAgMnSc1d1
nástupce	nástupce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antonius	Antonius	k1gInSc1
zřejmě	zřejmě	k6eAd1
sám	sám	k3xTgInSc4
usiloval	usilovat	k5eAaImAgMnS
o	o	k7c4
uchopení	uchopení	k1gNnSc4
moci	moc	k1gFnSc2
v	v	k7c6
Římě	Řím	k1gInSc6
a	a	k8xC
tohoto	tento	k3xDgMnSc4
osmnáctiletého	osmnáctiletý	k2eAgMnSc4d1
chlapce	chlapec	k1gMnSc4
si	se	k3xPyFc3
vzápětí	vzápětí	k6eAd1
znepřátelil	znepřátelit	k5eAaPmAgMnS
<g/>
,	,	kIx,
když	když	k8xS
mu	on	k3xPp3gMnSc3
odmítl	odmítnout	k5eAaPmAgMnS
vydat	vydat	k5eAaPmF
jeho	jeho	k3xOp3gNnSc4
dědictví	dědictví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
měsících	měsíc	k1gInPc6
po	po	k7c6
Caesarově	Caesarův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
přiřkli	přiřknout	k5eAaPmAgMnP
senátoři	senátor	k1gMnPc1
Brutovi	Brutův	k2eAgMnPc1d1
a	a	k8xC
Cassiovi	Cassiův	k2eAgMnPc1d1
správcovství	správcovství	k1gNnPc1
velkých	velký	k2eAgFnPc2d1
východních	východní	k2eAgFnPc2d1
provincií	provincie	k1gFnPc2
Makedonie	Makedonie	k1gFnSc2
a	a	k8xC
Sýrie	Sýrie	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
oplývaly	oplývat	k5eAaImAgFnP
četnými	četný	k2eAgFnPc7d1
legiemi	legie	k1gFnPc7
umístěnými	umístěný	k2eAgFnPc7d1
zde	zde	k6eAd1
kvůli	kvůli	k7c3
chystanému	chystaný	k2eAgNnSc3d1
tažení	tažení	k1gNnSc3
proti	proti	k7c3
Parthům	Parth	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antonius	Antonius	k1gInSc1
se	se	k3xPyFc4
je	být	k5eAaImIp3nS
pokusil	pokusit	k5eAaPmAgMnS
intrikami	intrika	k1gFnPc7
zbavit	zbavit	k5eAaPmF
těchto	tento	k3xDgFnPc2
provincií	provincie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozice	pozice	k1gFnSc1
vrahů	vrah	k1gMnPc2
v	v	k7c6
Římě	Řím	k1gInSc6
se	se	k3xPyFc4
tudíž	tudíž	k8xC
stala	stát	k5eAaPmAgFnS
natolik	natolik	k6eAd1
neudržitelnou	udržitelný	k2eNgFnSc4d1
<g/>
,	,	kIx,
že	že	k8xS
raději	rád	k6eAd2
opustili	opustit	k5eAaPmAgMnP
město	město	k1gNnSc4
a	a	k8xC
uchýlili	uchýlit	k5eAaPmAgMnP
se	se	k3xPyFc4
na	na	k7c4
východ	východ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Marcus	Marcus	k1gMnSc1
Antonius	Antonius	k1gMnSc1
a	a	k8xC
Octavianus	Octavianus	k1gMnSc1
na	na	k7c6
aureu	aureus	k1gInSc6
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
43	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
prohlásil	prohlásit	k5eAaPmAgMnS
senát	senát	k1gInSc4
Antonia	Antonio	k1gMnSc2
z	z	k7c2
podnětu	podnět	k1gInSc2
Cicerona	Cicero	k1gMnSc2
za	za	k7c2
nepřítele	nepřítel	k1gMnSc2
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antonius	Antonius	k1gInSc1
již	již	k6eAd1
předtím	předtím	k6eAd1
vyrazil	vyrazit	k5eAaPmAgMnS
na	na	k7c4
sever	sever	k1gInSc4
do	do	k7c2
Předalpské	předalpský	k2eAgFnSc2d1
Galie	Galie	k1gFnSc2
proti	proti	k7c3
tamějšímu	tamější	k2eAgMnSc3d1
místodržiteli	místodržitel	k1gMnSc3
Decimu	decima	k1gFnSc4
Brutovi	Brut	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Octavianus	Octavianus	k1gMnSc1
<g/>
,	,	kIx,
jemuž	jenž	k3xRgMnSc3
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
shromáždit	shromáždit	k5eAaPmF
několik	několik	k4yIc1
legií	legie	k1gFnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
spojil	spojit	k5eAaPmAgMnS
se	s	k7c7
senátory	senátor	k1gMnPc7
vedenými	vedený	k2eAgMnPc7d1
Ciceronem	Cicero	k1gMnSc7
<g/>
,	,	kIx,
načež	načež	k6eAd1
se	se	k3xPyFc4
s	s	k7c7
Antoniem	Antonio	k1gMnSc7
srazil	srazit	k5eAaPmAgMnS
ve	v	k7c6
dvou	dva	k4xCgFnPc6
bitvách	bitva	k1gFnPc6
u	u	k7c2
Mutiny	Mutina	k1gFnSc2
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc1d1
Modena	Modena	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
svém	svůj	k3xOyFgNnSc6
vítězství	vítězství	k1gNnSc6
se	se	k3xPyFc4
však	však	k9
obrátil	obrátit	k5eAaPmAgMnS
proti	proti	k7c3
senátu	senát	k1gInSc3
<g/>
,	,	kIx,
vpadl	vpadnout	k5eAaPmAgInS
do	do	k7c2
Itálie	Itálie	k1gFnSc2
a	a	k8xC
vojensky	vojensky	k6eAd1
se	se	k3xPyFc4
zmocnil	zmocnit	k5eAaPmAgMnS
Říma	Řím	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antoniův	Antoniův	k2eAgInSc4d1
kompromis	kompromis	k1gInSc4
prohlásil	prohlásit	k5eAaPmAgMnS
za	za	k7c4
nelegální	legální	k2eNgMnPc4d1
a	a	k8xC
Caesarovy	Caesarův	k2eAgMnPc4d1
vrahy	vrah	k1gMnPc4
postavil	postavit	k5eAaPmAgMnS
mimo	mimo	k7c4
zákon	zákon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
se	se	k3xPyFc4
zcela	zcela	k6eAd1
neočekávaně	očekávaně	k6eNd1
rozhodl	rozhodnout	k5eAaPmAgMnS
uzavřít	uzavřít	k5eAaPmF
mír	mír	k1gInSc4
s	s	k7c7
Antoniem	Antonio	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
si	se	k3xPyFc3
zabezpečil	zabezpečit	k5eAaPmAgMnS
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
Hispánií	Hispánie	k1gFnSc7
a	a	k8xC
Galií	Galie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
říjnu	říjen	k1gInSc6
43	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
se	se	k3xPyFc4
Octavianus	Octavianus	k1gInSc1
<g/>
,	,	kIx,
Marcus	Marcus	k1gMnSc1
Antonius	Antonius	k1gMnSc1
a	a	k8xC
Lepidus	Lepidus	k1gMnSc1
setkali	setkat	k5eAaPmAgMnP
u	u	k7c2
Bononie	Bononie	k1gFnSc2
(	(	kIx(
<g/>
Bologna	Bologna	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
vytvořili	vytvořit	k5eAaPmAgMnP
zde	zde	k6eAd1
druhý	druhý	k4xOgInSc4
triumvirát	triumvirát	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Shodným	shodný	k2eAgInSc7d1
cílem	cíl	k1gInSc7
triumvirů	triumvir	k1gMnPc2
byla	být	k5eAaImAgFnS
pomsta	pomsta	k1gFnSc1
Caesara	Caesar	k1gMnSc2
a	a	k8xC
potrestání	potrestání	k1gNnSc1
jeho	jeho	k3xOp3gMnPc2
vrahů	vrah	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nP
získali	získat	k5eAaPmAgMnP
prostředky	prostředek	k1gInPc4
a	a	k8xC
současně	současně	k6eAd1
se	se	k3xPyFc4
zbavili	zbavit	k5eAaPmAgMnP
svých	svůj	k3xOyFgMnPc2
nepřátel	nepřítel	k1gMnPc2
v	v	k7c6
Římě	Řím	k1gInSc6
<g/>
,	,	kIx,
vydali	vydat	k5eAaPmAgMnP
proskripční	proskripční	k2eAgFnPc4d1
listiny	listina	k1gFnPc4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
po	po	k7c6
vzoru	vzor	k1gInSc6
Sully	Sulla	k1gFnSc2
rozpoutali	rozpoutat	k5eAaPmAgMnP
teror	teror	k1gInSc4
<g/>
,	,	kIx,
při	při	k7c6
němž	jenž	k3xRgInSc6
ztratily	ztratit	k5eAaPmAgFnP
své	svůj	k3xOyFgInPc4
životy	život	k1gInPc4
stovky	stovka	k1gFnSc2
možná	možná	k9
tisíce	tisíc	k4xCgInPc1
předních	přední	k2eAgMnPc2d1
republikánsky	republikánsky	k6eAd1
smýšlejících	smýšlející	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
včetně	včetně	k7c2
Cicerona	Cicero	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krutost	krutost	k1gFnSc1
triumvirů	triumvir	k1gMnPc2
ostře	ostro	k6eAd1
kontrastovala	kontrastovat	k5eAaImAgFnS
s	s	k7c7
Caesarovou	Caesarův	k2eAgFnSc7d1
vlastní	vlastní	k2eAgFnSc7d1
politikou	politika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesar	Caesar	k1gMnSc1
byl	být	k5eAaImAgMnS
nedlouho	dlouho	k6eNd1
nato	nato	k6eAd1
senátem	senát	k1gInSc7
oficiálně	oficiálně	k6eAd1
prohlášen	prohlásit	k5eAaPmNgMnS
za	za	k7c4
boha	bůh	k1gMnSc4
jako	jako	k9
Divus	Divus	k1gMnSc1
Iulius	Iulius	k1gMnSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
božský	božský	k2eAgMnSc1d1
Julius	Julius	k1gMnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
létě	léto	k1gNnSc6
roku	rok	k1gInSc2
42	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
se	se	k3xPyFc4
Antonius	Antonius	k1gInSc1
a	a	k8xC
Octavianus	Octavianus	k1gInSc1
doprovázeni	doprovázet	k5eAaImNgMnP
mocným	mocný	k2eAgInSc7d1
vojskem	vojsko	k1gNnSc7
přeplavili	přeplavit	k5eAaPmAgMnP
do	do	k7c2
Řecka	Řecko	k1gNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
utkali	utkat	k5eAaPmAgMnP
s	s	k7c7
početně	početně	k6eAd1
slabší	slabý	k2eAgFnSc7d2
armádou	armáda	k1gFnSc7
Cassia	Cassium	k1gNnSc2
a	a	k8xC
Bruta	Bruto	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
rozhodujícímu	rozhodující	k2eAgInSc3d1
střetu	střet	k1gInSc3
došlo	dojít	k5eAaPmAgNnS
3	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
a	a	k8xC
opět	opět	k6eAd1
23	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
42	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Filipp	Filippy	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třebaže	třebaže	k8xS
v	v	k7c6
prvním	první	k4xOgInSc6
boji	boj	k1gInSc6
měli	mít	k5eAaImAgMnP
mírně	mírně	k6eAd1
navrch	navrch	k6eAd1
republikáni	republikán	k1gMnPc1
<g/>
,	,	kIx,
Cassius	Cassius	k1gMnSc1
špatně	špatně	k6eAd1
vyhodnotil	vyhodnotit	k5eAaPmAgMnS
vývoj	vývoj	k1gInSc4
a	a	k8xC
spáchal	spáchat	k5eAaPmAgInS
sebevraždu	sebevražda	k1gFnSc4
v	v	k7c6
přesvědčení	přesvědčení	k1gNnSc6
<g/>
,	,	kIx,
že	že	k8xS
bitva	bitva	k1gFnSc1
je	být	k5eAaImIp3nS
ztracena	ztratit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brutus	Brutus	k1gInSc1
poté	poté	k6eAd1
podlehl	podlehnout	k5eAaPmAgInS
spojeným	spojený	k2eAgInPc3d1
silám	síla	k1gFnPc3
triumvirů	triumvir	k1gMnPc2
a	a	k8xC
následoval	následovat	k5eAaImAgMnS
Cassiova	Cassiův	k2eAgInSc2d1
příkladu	příklad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
vzaly	vzít	k5eAaPmAgFnP
zasvé	zasvé	k6eAd1
poslední	poslední	k2eAgFnPc1d1
naděje	naděje	k1gFnPc1
republikánů	republikán	k1gMnPc2
<g/>
,	,	kIx,
načež	načež	k6eAd1
byli	být	k5eAaImAgMnP
všichni	všechen	k3xTgMnPc1
přeživší	přeživší	k2eAgMnPc1d1
vrazi	vrah	k1gMnPc1
brzy	brzy	k6eAd1
dopadeni	dopadnout	k5eAaPmNgMnP
a	a	k8xC
popraveni	popravit	k5eAaPmNgMnP
triumviry	triumvir	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
si	se	k3xPyFc3
rozdělili	rozdělit	k5eAaPmAgMnP
vládu	vláda	k1gFnSc4
nad	nad	k7c7
římským	římský	k2eAgInSc7d1
státem	stát	k1gInSc7
<g/>
,	,	kIx,
avšak	avšak	k8xC
bez	bez	k7c2
společného	společný	k2eAgInSc2d1
cíle	cíl	k1gInSc2
se	se	k3xPyFc4
mezi	mezi	k7c7
sebou	se	k3xPyFc7
rychle	rychle	k6eAd1
znesvářili	znesvářit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Octavianus	Octavianus	k1gInSc1
nejprve	nejprve	k6eAd1
eliminoval	eliminovat	k5eAaBmAgInS
Lepida	Lepid	k1gMnSc4
<g/>
,	,	kIx,
jemuž	jenž	k3xRgMnSc3
odňal	odnít	k5eAaPmAgInS
vojsko	vojsko	k1gNnSc4
na	na	k7c6
Sicílii	Sicílie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
v	v	k7c6
roce	rok	k1gInSc6
31	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
porazil	porazit	k5eAaPmAgMnS
v	v	k7c6
námořní	námořní	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
u	u	k7c2
Actia	Actius	k1gMnSc2
Marca	Marcus	k1gMnSc2
Antonia	Antonio	k1gMnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
se	se	k3xPyFc4
předtím	předtím	k6eAd1
v	v	k7c6
Egyptě	Egypt	k1gInSc6
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Caesarovou	Caesarová	k1gFnSc7
milenkou	milenka	k1gFnSc7
Kleopatrou	Kleopatra	k1gFnSc7
<g/>
,	,	kIx,
dosáhl	dosáhnout	k5eAaPmAgMnS
Octavianus	Octavianus	k1gMnSc1
samovlády	samovláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Caesara	Caesar	k1gMnSc2
se	se	k3xPyFc4
Octavianus	Octavianus	k1gMnSc1
obezřetně	obezřetně	k6eAd1
vyvaroval	vyvarovat	k5eAaPmAgMnS
byť	byť	k8xS
i	i	k9
jen	jen	k9
zdání	zdání	k1gNnSc1
vytvoření	vytvoření	k1gNnSc2
monarchie	monarchie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokonce	dokonce	k9
hovořil	hovořit	k5eAaImAgMnS
o	o	k7c4
„	„	k?
<g/>
obnovení	obnovení	k1gNnSc4
republiky	republika	k1gFnSc2
<g/>
“	“	k?
a	a	k8xC
skromně	skromně	k6eAd1
se	se	k3xPyFc4
nazýval	nazývat	k5eAaImAgInS
„	„	k?
<g/>
prvním	první	k4xOgMnSc6
občanem	občan	k1gMnSc7
<g/>
“	“	k?
(	(	kIx(
<g/>
princeps	princeps	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
ale	ale	k8xC
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
rukou	ruka	k1gFnPc6
pevně	pevně	k6eAd1
třímal	třímat	k5eAaImAgInS
veškerou	veškerý	k3xTgFnSc4
rozhodující	rozhodující	k2eAgFnSc4d1
moc	moc	k1gFnSc4
ve	v	k7c6
státě	stát	k1gInSc6
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
se	se	k3xPyFc4
opíral	opírat	k5eAaImAgMnS
o	o	k7c4
svoji	svůj	k3xOyFgFnSc4
nezměrnou	nezměrný	k2eAgFnSc4d1,k2eNgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
moc	moc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Senát	senát	k1gInSc4
mu	on	k3xPp3gMnSc3
dovolil	dovolit	k5eAaPmAgMnS
vykonávat	vykonávat	k5eAaImF
pravomoci	pravomoc	k1gFnPc4
různých	různý	k2eAgInPc2d1
magistrátů	magistrát	k1gInPc2
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
tyto	tento	k3xDgInPc4
úřady	úřad	k1gInPc4
nezastával	zastávat	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lednu	leden	k1gInSc6
27	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
přijal	přijmout	k5eAaPmAgMnS
Octavianus	Octavianus	k1gMnSc1
čestné	čestný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
Augustus	Augustus	k1gInSc4
(	(	kIx(
<g/>
„	„	k?
<g/>
Vznešený	vznešený	k2eAgInSc4d1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
mu	on	k3xPp3gMnSc3
udělili	udělit	k5eAaPmAgMnP
senátoři	senátor	k1gMnPc1
<g/>
,	,	kIx,
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
tak	tak	k6eAd1
skutečným	skutečný	k2eAgMnSc7d1
zakladatelem	zakladatel	k1gMnSc7
římského	římský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
s	s	k7c7
konečnou	konečný	k2eAgFnSc7d1
platností	platnost	k1gFnSc7
pohřbil	pohřbít	k5eAaPmAgInS
římskou	římský	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
doby	doba	k1gFnSc2
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
označované	označovaný	k2eAgFnPc1d1
jako	jako	k8xC,k8xS
zlatý	zlatý	k2eAgInSc1d1
nebo	nebo	k8xC
augustovský	augustovský	k2eAgInSc1d1
věk	věk	k1gInSc1
<g/>
,	,	kIx,
zažila	zažít	k5eAaPmAgFnS
římská	římský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
skutečný	skutečný	k2eAgInSc1d1
rozkvět	rozkvět	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Caesarova	Caesarův	k2eAgFnSc1d1
literární	literární	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
</s>
<s>
Gaius	Gaius	k1gMnSc1
Iulius	Iulius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
</s>
<s>
Caesar	Caesar	k1gMnSc1
byl	být	k5eAaImAgMnS
již	již	k6eAd1
za	za	k7c2
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
pokládán	pokládat	k5eAaImNgInS
za	za	k7c4
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
nejlepších	dobrý	k2eAgMnPc2d3
římských	římský	k2eAgMnPc2d1
řečníků	řečník	k1gMnPc2
a	a	k8xC
spisovatelů	spisovatel	k1gMnPc2
–	–	k?
dokonce	dokonce	k9
i	i	k9
Cicero	Cicero	k1gMnSc1
se	se	k3xPyFc4
vyjadřoval	vyjadřovat	k5eAaImAgMnS
pochvalně	pochvalně	k6eAd1
o	o	k7c6
Caesarových	Caesarových	k2eAgFnPc6d1
rétorských	rétorský	k2eAgFnPc6d1
schopnostech	schopnost	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
jeho	jeho	k3xOp3gMnSc4,k3xPp3gMnSc4
nejslavnější	slavný	k2eAgNnPc1d3
díla	dílo	k1gNnPc1
náleží	náležet	k5eAaImIp3nP
smuteční	smuteční	k2eAgFnSc4d1
řeč	řeč	k1gFnSc4
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
tetu	teta	k1gFnSc4
Julii	Julie	k1gFnSc4
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
Anticato	Anticat	k2eAgNnSc1d1
<g/>
,	,	kIx,
polemika	polemika	k1gFnSc1
sloužící	sloužící	k1gFnSc1
k	k	k7c3
očernění	očernění	k1gNnSc3
Catonovy	Catonův	k2eAgFnSc2d1
reputace	reputace	k1gFnSc2
<g/>
,	,	kIx,
jímž	jenž	k3xRgNnSc7
reagoval	reagovat	k5eAaBmAgInS
na	na	k7c4
Ciceronův	Ciceronův	k2eAgInSc4d1
oslavný	oslavný	k2eAgInSc4d1
spis	spis	k1gInSc4
Cato	Cato	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
napsal	napsat	k5eAaBmAgInS,k5eAaPmAgInS
báseň	báseň	k1gFnSc4
o	o	k7c6
cestě	cesta	k1gFnSc6
z	z	k7c2
Říma	Řím	k1gInSc2
do	do	k7c2
Hispánie	Hispánie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
práce	práce	k1gFnPc1
se	se	k3xPyFc4
do	do	k7c2
dnešních	dnešní	k2eAgInPc2d1
dnů	den	k1gInPc2
nezachovaly	zachovat	k5eNaPmAgInP
<g/>
,	,	kIx,
neboť	neboť	k8xC
při	při	k7c6
opisování	opisování	k1gNnSc6
antických	antický	k2eAgInPc2d1
rukopisů	rukopis	k1gInPc2
ve	v	k7c6
středověku	středověk	k1gInSc6
byly	být	k5eAaImAgInP
považovány	považován	k2eAgInPc1d1
za	za	k7c4
nedůležité	důležitý	k2eNgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
na	na	k7c6
počátku	počátek	k1gInSc6
2	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
byla	být	k5eAaImAgNnP
ztracená	ztracený	k2eAgNnPc1d1
jiná	jiný	k2eAgNnPc1d1
Caesarova	Caesarův	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
<g/>
:	:	kIx,
tragédie	tragédie	k1gFnSc1
Oidipus	Oidipus	k1gMnSc1
<g/>
,	,	kIx,
sbírka	sbírka	k1gFnSc1
rčení	rčení	k1gNnSc1
a	a	k8xC
báseň	báseň	k1gFnSc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc7
ústřední	ústřední	k2eAgFnSc7d1
postavou	postava	k1gFnSc7
byl	být	k5eAaImAgInS
Héraklés	Héraklés	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
tito	tento	k3xDgMnPc1
mýtičtí	mýtický	k2eAgMnPc1d1
hrdinové	hrdina	k1gMnPc1
trpěli	trpět	k5eAaImAgMnP
za	za	k7c4
činy	čin	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
vykonali	vykonat	k5eAaPmAgMnP
proti	proti	k7c3
své	svůj	k3xOyFgFnSc3
vůli	vůle	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgInSc2
lze	lze	k6eAd1
usuzovat	usuzovat	k5eAaImF
na	na	k7c4
Caesarův	Caesarův	k2eAgInSc4d1
skeptický	skeptický	k2eAgInSc4d1
postoj	postoj	k1gInSc4
k	k	k7c3
náboženství	náboženství	k1gNnSc3
<g/>
,	,	kIx,
přestože	přestože	k8xS
v	v	k7c6
mládí	mládí	k1gNnSc6
působil	působit	k5eAaImAgMnS
jako	jako	k8xC,k8xS
velekněz	velekněz	k1gMnSc1
římského	římský	k2eAgInSc2d1
státního	státní	k2eAgInSc2d1
kultu	kult	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Jediné	jediný	k2eAgInPc4d1
Caesarovy	Caesarův	k2eAgInPc4d1
spisy	spis	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
se	se	k3xPyFc4
nám	my	k3xPp1nPc3
dochovaly	dochovat	k5eAaPmAgFnP
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
jeho	jeho	k3xOp3gInPc1
fascinující	fascinující	k2eAgInPc1d1
Zápisky	zápisek	k1gInPc1
o	o	k7c6
válce	válka	k1gFnSc6
galské	galský	k2eAgFnSc6d1
(	(	kIx(
<g/>
Commentarii	Commentarie	k1gFnSc3
de	de	k?
bello	bello	k1gNnSc1
Gallico	Gallico	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
Zápisky	zápiska	k1gFnSc2
o	o	k7c6
válce	válka	k1gFnSc6
občanské	občanský	k2eAgFnSc6d1
(	(	kIx(
<g/>
Commentarii	Commentarie	k1gFnSc3
de	de	k?
bello	bello	k1gNnSc4
civili	civit	k5eAaBmAgMnP,k5eAaImAgMnP,k5eAaPmAgMnP
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvním	první	k4xOgInSc6
díle	dílo	k1gNnSc6
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
tvořilo	tvořit	k5eAaImAgNnS
podklad	podklad	k1gInSc4
pro	pro	k7c4
zprávy	zpráva	k1gFnPc4
o	o	k7c6
průběhu	průběh	k1gInSc6
boje	boj	k1gInSc2
posílané	posílaný	k2eAgFnSc2d1
do	do	k7c2
senátu	senát	k1gInSc2
<g/>
,	,	kIx,
sepsal	sepsat	k5eAaPmAgMnS
Caesar	Caesar	k1gMnSc1
svá	svůj	k3xOyFgNnPc4
vojenská	vojenský	k2eAgNnPc4d1
tažení	tažení	k1gNnPc4
v	v	k7c6
Galii	Galie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
celkem	celkem	k6eAd1
osm	osm	k4xCc4
knih	kniha	k1gFnPc2
pojednávajících	pojednávající	k2eAgFnPc2d1
o	o	k7c6
letech	léto	k1gNnPc6
58	#num#	k4
až	až	k9
50	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
autorem	autor	k1gMnSc7
poslední	poslední	k2eAgFnSc2d1
z	z	k7c2
nich	on	k3xPp3gFnPc2
byl	být	k5eAaImAgMnS
Caesarův	Caesarův	k2eAgMnSc1d1
poddůstojník	poddůstojník	k1gMnSc1
Aulus	Aulus	k1gMnSc1
Hirtius	Hirtius	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zápisky	zápisek	k1gInPc4
patří	patřit	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
pro	pro	k7c4
svoji	svůj	k3xOyFgFnSc4
jednoduchou	jednoduchý	k2eAgFnSc4d1
a	a	k8xC
snadno	snadno	k6eAd1
srozumitelnou	srozumitelný	k2eAgFnSc4d1
formu	forma	k1gFnSc4
(	(	kIx(
<g/>
v	v	k7c6
díle	dílo	k1gNnSc6
je	být	k5eAaImIp3nS
použito	použít	k5eAaPmNgNnS
jen	jen	k9
asi	asi	k9
1300	#num#	k4
slov	slovo	k1gNnPc2
<g/>
)	)	kIx)
k	k	k7c3
literatuře	literatura	k1gFnSc3
užívané	užívaný	k2eAgFnSc3d1
při	při	k7c6
výuce	výuka	k1gFnSc6
latiny	latina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesar	Caesar	k1gMnSc1
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
díle	díl	k1gInSc6
detailně	detailně	k6eAd1
popsal	popsat	k5eAaPmAgMnS
osm	osm	k4xCc1
let	léto	k1gNnPc2
bitev	bitva	k1gFnPc2
a	a	k8xC
válečných	válečný	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgMnPc6
jeho	jeho	k3xOp3gMnSc3
vojsko	vojsko	k1gNnSc1
zdecimovalo	zdecimovat	k5eAaPmAgNnS
galské	galský	k2eAgInPc4d1
kmeny	kmen	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
zaznamenal	zaznamenat	k5eAaPmAgInS
řadu	řada	k1gFnSc4
postřehů	postřeh	k1gInPc2
týkajících	týkající	k2eAgInPc2d1
se	se	k3xPyFc4
životních	životní	k2eAgInPc2d1
poměrů	poměr	k1gInPc2
a	a	k8xC
zvyků	zvyk	k1gInPc2
Galů	Gal	k1gMnPc2
<g/>
,	,	kIx,
Germanů	German	k1gMnPc2
a	a	k8xC
Britanů	Britan	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zhotovení	zhotovení	k1gNnSc1
a	a	k8xC
zveřejnění	zveřejnění	k1gNnSc1
Zápisků	zápisek	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
zřejmě	zřejmě	k6eAd1
nebyly	být	k5eNaImAgInP
vydávány	vydávat	k5eAaPmNgInP,k5eAaImNgInP
každoročně	každoročně	k6eAd1
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
teprve	teprve	k6eAd1
po	po	k7c6
skončení	skončení	k1gNnSc6
Caesarova	Caesarův	k2eAgInSc2d1
prokonzulátu	prokonzulát	k1gInSc2
v	v	k7c6
Galii	Galie	k1gFnSc6
jako	jako	k8xS,k8xC
celistvé	celistvý	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
<g/>
,	,	kIx,
sloužilo	sloužit	k5eAaImAgNnS
především	především	k9
k	k	k7c3
ospravedlnění	ospravedlnění	k1gNnSc3
Caesarových	Caesarových	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
a	a	k8xC
tím	ten	k3xDgNnSc7
i	i	k9
k	k	k7c3
obhajobě	obhajoba	k1gFnSc3
jeho	jeho	k3xOp3gFnPc2
bezohledných	bezohledný	k2eAgFnPc2d1
opatření	opatření	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
vystavena	vystavit	k5eAaPmNgFnS
silné	silný	k2eAgFnSc3d1
kritice	kritika	k1gFnSc3
leckterých	leckterý	k3yIgMnPc2
senátorů	senátor	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Tři	tři	k4xCgFnPc1
knihy	kniha	k1gFnPc1
o	o	k7c6
občanské	občanský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
zachycující	zachycující	k2eAgFnSc2d1
události	událost	k1gFnSc2
z	z	k7c2
let	léto	k1gNnPc2
49	#num#	k4
a	a	k8xC
48	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
avšak	avšak	k8xC
jsou	být	k5eAaImIp3nP
nedokončené	dokončený	k2eNgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesar	Caesar	k1gMnSc1
se	se	k3xPyFc4
v	v	k7c6
této	tento	k3xDgFnSc6
své	svůj	k3xOyFgFnSc6
práci	práce	k1gFnSc6
vykresluje	vykreslovat	k5eAaImIp3nS
jako	jako	k9
nedobrovolný	dobrovolný	k2eNgMnSc1d1
bojovník	bojovník	k1gMnSc1
v	v	k7c6
nevyhnutelném	vyhnutelný	k2eNgInSc6d1
konfliktu	konflikt	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
tažení	tažení	k1gNnSc6
v	v	k7c6
Alexandrii	Alexandrie	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
47	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
vyhotovil	vyhotovit	k5eAaPmAgInS
Hirtius	Hirtius	k1gInSc1
spis	spis	k1gInSc1
Bellum	Bellum	k1gInSc1
Alexandrinum	Alexandrinum	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
ještě	ještě	k9
další	další	k2eAgFnPc1d1
dvě	dva	k4xCgFnPc1
knihy	kniha	k1gFnPc1
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
byly	být	k5eAaImAgInP
připisovány	připisovat	k5eAaImNgInP
Caesarovi	Caesar	k1gMnSc3
<g/>
,	,	kIx,
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
je	být	k5eAaImIp3nS
ale	ale	k9
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
někdo	někdo	k3yInSc1
jiný	jiný	k2eAgMnSc1d1
<g/>
:	:	kIx,
Bellum	Bellum	k1gInSc1
Africanum	Africanum	k1gNnSc1
a	a	k8xC
Bellum	Bellum	k1gNnSc1
Hispaniense	Hispaniense	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgNnPc1
líčí	líčit	k5eAaImIp3nP
závěrečná	závěrečný	k2eAgNnPc1d1
střetnutí	střetnutí	k1gNnPc1
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
z	z	k7c2
let	léto	k1gNnPc2
46	#num#	k4
a	a	k8xC
45	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
Vojenské	vojenský	k2eAgNnSc1d1
umění	umění	k1gNnSc1
</s>
<s>
Vojenští	vojenský	k2eAgMnPc1d1
historikové	historik	k1gMnPc1
hodnotí	hodnotit	k5eAaImIp3nP
Caesara	Caesar	k1gMnSc2
jako	jako	k8xC,k8xS
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
nejzdatnějších	zdatný	k2eAgMnPc2d3
a	a	k8xC
nejschopnějších	schopný	k2eAgMnPc2d3
stratégů	stratég	k1gMnPc2
a	a	k8xC
taktiků	taktik	k1gMnPc2
v	v	k7c6
dějinách	dějiny	k1gFnPc6
vojenství	vojenství	k1gNnSc2
a	a	k8xC
řadí	řadit	k5eAaImIp3nS
ho	on	k3xPp3gMnSc4
po	po	k7c4
bok	bok	k1gInSc4
vojevůdců	vojevůdce	k1gMnPc2
<g/>
,	,	kIx,
jako	jako	k9
byli	být	k5eAaImAgMnP
Alexandr	Alexandr	k1gMnSc1
Veliký	veliký	k2eAgMnSc1d1
<g/>
,	,	kIx,
Hannibal	Hannibal	k1gInSc1
<g/>
,	,	kIx,
Čingischán	Čingischán	k1gMnSc1
nebo	nebo	k8xC
Napoleon	Napoleon	k1gMnSc1
Bonaparte	bonapart	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třebaže	třebaže	k8xS
utrpěl	utrpět	k5eAaPmAgMnS
i	i	k9
některé	některý	k3yIgFnPc4
porážky	porážka	k1gFnPc4
jako	jako	k8xC,k8xS
například	například	k6eAd1
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Gergovie	Gergovie	k1gFnSc2
během	během	k7c2
galských	galský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
a	a	k8xC
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Dyrrhachia	Dyrrhachium	k1gNnSc2
během	během	k7c2
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
Caesar	Caesar	k1gMnSc1
prokázal	prokázat	k5eAaPmAgMnS
taktickou	taktický	k2eAgFnSc4d1
genialitu	genialita	k1gFnSc4
svými	svůj	k3xOyFgInPc7
obdivuhodnými	obdivuhodný	k2eAgInPc7d1
výkony	výkon	k1gInPc7
při	při	k7c6
obléhání	obléhání	k1gNnSc6
Alesie	Alesie	k1gFnSc2
<g/>
,	,	kIx,
obrácením	obrácení	k1gNnSc7
na	na	k7c4
útěk	útěk	k1gInSc4
Pompeiova	Pompeiův	k2eAgNnSc2d1
početně	početně	k6eAd1
silnějšího	silný	k2eAgNnSc2d2
vojska	vojsko	k1gNnSc2
u	u	k7c2
Farsálu	Farsál	k1gInSc2
nebo	nebo	k8xC
úplným	úplný	k2eAgNnSc7d1
zničením	zničení	k1gNnSc7
Farnakova	Farnakův	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
u	u	k7c2
Zely	zet	k5eAaImAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesarova	Caesarův	k2eAgFnSc1d1
úspěšná	úspěšný	k2eAgFnSc1d1
tažení	tažení	k1gNnSc1
v	v	k7c6
jakémkoli	jakýkoli	k3yIgInSc6
terénu	terén	k1gInSc6
a	a	k8xC
za	za	k7c2
jakýchkoli	jakýkoli	k3yIgFnPc2
klimatických	klimatický	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
lze	lze	k6eAd1
přičíst	přičíst	k5eAaPmF
jeho	jeho	k3xOp3gFnSc3
přísné	přísný	k2eAgFnSc3d1
a	a	k8xC
tvrdé	tvrdý	k2eAgFnSc3d1
kázni	kázeň	k1gFnSc3
vyžadované	vyžadovaný	k2eAgFnSc2d1
jím	jíst	k5eAaImIp1nS
po	po	k7c6
svých	svůj	k3xOyFgMnPc6
legionářích	legionář	k1gMnPc6
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc1
obdiv	obdiv	k1gInSc1
<g/>
,	,	kIx,
oddanost	oddanost	k1gFnSc1
a	a	k8xC
úcta	úcta	k1gFnSc1
vůči	vůči	k7c3
němu	on	k3xPp3gNnSc3
byla	být	k5eAaImAgFnS
pověstná	pověstný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toho	ten	k3xDgMnSc4
dosáhl	dosáhnout	k5eAaPmAgInS
svým	svůj	k3xOyFgNnSc7
spravedlivým	spravedlivý	k2eAgNnSc7d1
zacházením	zacházení	k1gNnSc7
a	a	k8xC
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
neváhal	váhat	k5eNaImAgMnS
dávat	dávat	k5eAaImF
přednost	přednost	k1gFnSc4
vojákům	voják	k1gMnPc3
zkušeným	zkušený	k2eAgNnSc7d1
před	před	k7c7
urozenými	urozený	k2eAgMnPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vskutku	vskutku	k9
prvotřídní	prvotřídní	k2eAgFnSc4d1
Caesarovu	Caesarův	k2eAgFnSc4d1
pěchotu	pěchota	k1gFnSc4
a	a	k8xC
jízdu	jízda	k1gFnSc4
doplňovala	doplňovat	k5eAaImAgFnS
hrůzu	hrůza	k1gFnSc4
budící	budící	k2eAgFnSc1d1
těžká	těžký	k2eAgFnSc1d1
artilerie	artilerie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgInPc7d1
faktory	faktor	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
činily	činit	k5eAaImAgInP
jeho	jeho	k3xOp3gNnSc4
vojsko	vojsko	k1gNnSc4
tak	tak	k9
vysoce	vysoce	k6eAd1
efektivním	efektivní	k2eAgFnPc3d1
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
takřka	takřka	k6eAd1
dokonalé	dokonalý	k2eAgFnSc2d1
kastrametační	kastrametační	k2eAgFnSc2d1
a	a	k8xC
ženistické	ženistický	k2eAgFnSc2d1
schopnosti	schopnost	k1gFnSc2
jeho	jeho	k3xOp3gMnPc2
vojáků	voják	k1gMnPc2
a	a	k8xC
legendární	legendární	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
<g/>
,	,	kIx,
s	s	k7c7
níž	jenž	k3xRgFnSc7
se	se	k3xPyFc4
dokázali	dokázat	k5eAaPmAgMnP
přesouvat	přesouvat	k5eAaImF
a	a	k8xC
manévrovat	manévrovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesarova	Caesarův	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
byla	být	k5eAaImAgFnS
při	při	k7c6
pochodu	pochod	k1gInSc6
schopná	schopný	k2eAgFnSc1d1
urazit	urazit	k5eAaPmF
i	i	k9
čtyřicet	čtyřicet	k4xCc4
mil	míle	k1gFnPc2
denně	denně	k6eAd1
a	a	k8xC
touto	tento	k3xDgFnSc7
rychlostí	rychlost	k1gFnSc7
akcí	akce	k1gFnPc2
ohromovala	ohromovat	k5eAaImAgFnS
své	svůj	k3xOyFgMnPc4
nepřátele	nepřítel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
vojsko	vojsko	k1gNnSc1
čítalo	čítat	k5eAaImAgNnS
jen	jen	k9
málokdy	málokdy	k6eAd1
více	hodně	k6eAd2
než	než	k8xS
40	#num#	k4
000	#num#	k4
legionářů	legionář	k1gMnPc2
spolu	spolu	k6eAd1
s	s	k7c7
několika	několik	k4yIc7
tisíci	tisíc	k4xCgInPc7
jezdci	jezdec	k1gMnPc7
a	a	k8xC
specializovanými	specializovaný	k2eAgFnPc7d1
jednotkami	jednotka	k1gFnPc7
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
byli	být	k5eAaImAgMnP
ženisté	ženista	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Zápiscích	zápisek	k1gInPc6
o	o	k7c6
válce	válka	k1gFnSc6
galské	galský	k2eAgFnSc2d1
Caesar	Caesar	k1gMnSc1
popsal	popsat	k5eAaPmAgMnS
obléhání	obléhání	k1gNnSc1
jednoho	jeden	k4xCgNnSc2
galského	galský	k2eAgNnSc2d1
oppida	oppidum	k1gNnSc2
umístěného	umístěný	k2eAgNnSc2d1
na	na	k7c6
velmi	velmi	k6eAd1
vysoké	vysoký	k2eAgFnSc3d1
a	a	k8xC
příkré	příkrý	k2eAgFnSc3d1
náhorní	náhorní	k2eAgFnSc3d1
plošině	plošina	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
ženisté	ženista	k1gMnPc1
prorazili	prorazit	k5eAaPmAgMnP
tunel	tunel	k1gInSc4
v	v	k7c6
ohromné	ohromný	k2eAgFnSc6d1
skále	skála	k1gFnSc6
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
odklonili	odklonit	k5eAaPmAgMnP
pramen	pramen	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
zásoboval	zásobovat	k5eAaImAgMnS
galské	galský	k2eAgNnSc4d1
město	město	k1gNnSc4
vodou	voda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
<g/>
,	,	kIx,
odříznuté	odříznutý	k2eAgNnSc1d1
od	od	k7c2
svého	svůj	k3xOyFgInSc2
zdroje	zdroj	k1gInSc2
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
záhy	záhy	k6eAd1
kapitulovalo	kapitulovat	k5eAaBmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Caesarovo	Caesarův	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Podle	podle	k7c2
římského	římský	k2eAgMnSc2d1
spisovatele	spisovatel	k1gMnSc2
Plinia	Plinium	k1gNnSc2
staršího	starší	k1gMnSc2
bylo	být	k5eAaImAgNnS
jméno	jméno	k1gNnSc1
„	„	k?
<g/>
Caesar	Caesar	k1gMnSc1
<g/>
“	“	k?
odvozeno	odvozen	k2eAgNnSc1d1
z	z	k7c2
latinského	latinský	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
caedare	caedar	k1gMnSc5
(	(	kIx(
<g/>
„	„	k?
<g/>
vyříznout	vyříznout	k5eAaPmF
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
případně	případně	k6eAd1
caesus	caesus	k1gInSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
řez	řez	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větev	větev	k1gFnSc1
rodu	rod	k1gInSc2
Juliů	Julius	k1gMnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
nesla	nést	k5eAaImAgFnS
cognomen	cognomen	k2eAgInSc4d1
Caesar	Caesar	k1gMnSc1
měla	mít	k5eAaImAgFnS
prý	prý	k9
pocházet	pocházet	k5eAaImF
od	od	k7c2
muže	muž	k1gMnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgInS
císařským	císařský	k2eAgInSc7d1
řezem	řez	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historia	Historium	k1gNnSc2
Augusta	August	k1gMnSc4
podává	podávat	k5eAaImIp3nS
další	další	k2eAgFnPc4d1
tři	tři	k4xCgFnPc4
alternativní	alternativní	k2eAgFnPc4d1
možnosti	možnost	k1gFnPc4
vzniku	vznik	k1gInSc2
tohoto	tento	k3xDgNnSc2
jména	jméno	k1gNnSc2
<g/>
:	:	kIx,
první	první	k4xOgMnSc1
Caesar	Caesar	k1gMnSc1
buď	buď	k8xC
zabil	zabít	k5eAaPmAgMnS
v	v	k7c6
bitvě	bitva	k1gFnSc6
během	během	k7c2
punských	punský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
slona	slon	k1gMnSc2
(	(	kIx(
<g/>
caesai	caesai	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
oplýval	oplývat	k5eAaImAgInS
bujnou	bujný	k2eAgFnSc7d1
kšticí	kštice	k1gFnSc7
(	(	kIx(
<g/>
caesaris	caesaris	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
případně	případně	k6eAd1
měl	mít	k5eAaImAgInS
jasně	jasně	k6eAd1
šedé	šedý	k2eAgNnSc4d1
oči	oko	k1gNnPc4
(	(	kIx(
<g/>
oculis	oculis	k1gFnSc1
caesiis	caesiis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
první	první	k4xOgFnSc3
variantě	varianta	k1gFnSc3
se	se	k3xPyFc4
zřejmě	zřejmě	k6eAd1
klonil	klonit	k5eAaImAgMnS
i	i	k9
sám	sám	k3xTgMnSc1
Caesar	Caesar	k1gMnSc1
<g/>
,	,	kIx,
neboť	neboť	k8xC
během	během	k7c2
pobytu	pobyt	k1gInSc2
v	v	k7c6
Galii	Galie	k1gFnSc6
nechal	nechat	k5eAaPmAgMnS
razit	razit	k5eAaImF
mince	mince	k1gFnPc4
se	s	k7c7
svým	svůj	k3xOyFgNnSc7
jménem	jméno	k1gNnSc7
a	a	k8xC
vyobrazením	vyobrazení	k1gNnSc7
slona	slon	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
klasické	klasický	k2eAgFnSc6d1
latině	latina	k1gFnSc6
v	v	k7c6
době	doba	k1gFnSc6
Caesarova	Caesarův	k2eAgInSc2d1
života	život	k1gInSc2
bylo	být	k5eAaImAgNnS
písmeno	písmeno	k1gNnSc1
„	„	k?
<g/>
C	C	kA
<g/>
“	“	k?
vyslovováno	vyslovovat	k5eAaImNgNnS
obvykle	obvykle	k6eAd1
jako	jako	k8xS,k8xC
„	„	k?
<g/>
K	k	k7c3
<g/>
“	“	k?
<g/>
,	,	kIx,
spřežka	spřežka	k1gFnSc1
„	„	k?
<g/>
ae	ae	k?
<g/>
“	“	k?
jako	jako	k8xC,k8xS
„	„	k?
<g/>
ai	ai	k?
<g/>
“	“	k?
<g/>
,	,	kIx,
tedy	tedy	k9
nikoli	nikoli	k9
jako	jako	k9
„	„	k?
<g/>
é	é	k0
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgInSc2
plyne	plynout	k5eAaImIp3nS
následující	následující	k2eAgFnSc1d1
latinská	latinský	k2eAgFnSc1d1
výslovnost	výslovnost	k1gFnSc1
slova	slovo	k1gNnSc2
Caesar	Caesar	k1gMnSc1
<g/>
:	:	kIx,
kaisar	kaisar	k1gMnSc1
<g/>
,	,	kIx,
případně	případně	k6eAd1
také	také	k9
kaizar	kaizar	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dobách	doba	k1gFnPc6
pozdní	pozdní	k2eAgFnSc2d1
římské	římský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
byla	být	k5eAaImAgFnS
většina	většina	k1gFnSc1
historických	historický	k2eAgInPc2d1
spisů	spis	k1gInPc2
vyhotovena	vyhotoven	k2eAgFnSc1d1
v	v	k7c6
řečtině	řečtina	k1gFnSc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byl	být	k5eAaImAgInS
jazyk	jazyk	k1gInSc4
vzdělaných	vzdělaný	k2eAgMnPc2d1
a	a	k8xC
urozených	urozený	k2eAgFnPc2d1
římských	římský	k2eAgFnPc2d1
vrstev	vrstva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caesarovo	Caesarův	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
se	se	k3xPyFc4
v	v	k7c6
tomto	tento	k3xDgInSc6
jazyce	jazyk	k1gInSc6
psalo	psát	k5eAaImAgNnS
Κ	Κ	k?
(	(	kIx(
<g/>
[	[	kIx(
<g/>
kaísar	kaísar	k1gInSc1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
odpovídalo	odpovídat	k5eAaImAgNnS
tehdejší	tehdejší	k2eAgFnPc4d1
výslovnosti	výslovnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
této	tento	k3xDgFnSc2
klasické	klasický	k2eAgFnSc2d1
výslovnosti	výslovnost	k1gFnSc2
Caesarova	Caesarův	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
je	být	k5eAaImIp3nS
odvozen	odvodit	k5eAaPmNgInS
německý	německý	k2eAgInSc1d1
titul	titul	k1gInSc1
Kaiser	Kaisra	k1gFnPc2
(	(	kIx(
<g/>
[	[	kIx(
<g/>
kajzr	kajzr	k1gInSc1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naproti	naproti	k7c3
tomu	ten	k3xDgMnSc3
anglická	anglický	k2eAgFnSc1d1
výslovnost	výslovnost	k1gFnSc1
[	[	kIx(
<g/>
sízr	sízr	k1gInSc1
<g/>
]	]	kIx)
a	a	k8xC
stejně	stejně	k6eAd1
tak	tak	k9
česká	český	k2eAgFnSc1d1
[	[	kIx(
<g/>
cézar	cézar	k1gMnSc1
<g/>
]	]	kIx)
pocházejí	pocházet	k5eAaImIp3nP
ze	z	k7c2
středověké	středověký	k2eAgFnSc2d1
latiny	latina	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
Caesarovo	Caesarův	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
znělo	znět	k5eAaImAgNnS
[	[	kIx(
<g/>
čésar	čésar	k1gInSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Slovo	slovo	k1gNnSc1
Caesar	Caesar	k1gMnSc1
bylo	být	k5eAaImAgNnS
od	od	k7c2
dob	doba	k1gFnPc2
Augusta	August	k1gMnSc4
součástí	součást	k1gFnSc7
jména	jméno	k1gNnSc2
a	a	k8xC
titulatury	titulatura	k1gFnSc2
římských	římský	k2eAgMnPc2d1
císařů	císař	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
doby	doba	k1gFnSc2
vlády	vláda	k1gFnSc2
Hadriana	Hadriana	k1gFnSc1
náležel	náležet	k5eAaImAgInS
titul	titul	k1gInSc1
caesar	caesar	k1gInSc4
císařovu	císařův	k2eAgMnSc3d1
designovanému	designovaný	k2eAgMnSc3d1
nástupci	nástupce	k1gMnSc3
a	a	k8xC
v	v	k7c6
období	období	k1gNnSc6
tetrarchie	tetrarchie	k1gFnSc2
níže	nízce	k6eAd2
postaveným	postavený	k2eAgMnPc3d1
spolucísařům	spolucísař	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nepatrně	patrně	k6eNd1,k6eAd1
obměněné	obměněný	k2eAgFnSc6d1
formě	forma	k1gFnSc6
se	se	k3xPyFc4
slovo	slovo	k1gNnSc1
Caesar	Caesar	k1gMnSc1
vyskytuje	vyskytovat	k5eAaImIp3nS
i	i	k9
v	v	k7c6
mnoha	mnoho	k4c6
různých	různý	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
jako	jako	k8xS,k8xC
označení	označení	k1gNnSc1
pro	pro	k7c4
vládce	vládce	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
titul	titul	k1gInSc1
„	„	k?
<g/>
císař	císař	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
německý	německý	k2eAgMnSc1d1
„	„	k?
<g/>
Kaiser	Kaiser	k1gMnSc1
<g/>
“	“	k?
a	a	k8xC
ruský	ruský	k2eAgMnSc1d1
„	„	k?
<g/>
car	car	k1gMnSc1
<g/>
“	“	k?
jsou	být	k5eAaImIp3nP
odvozeny	odvodit	k5eAaPmNgFnP
z	z	k7c2
Caesarova	Caesarův	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Prameny	pramen	k1gInPc1
</s>
<s>
O	o	k7c6
Caesarově	Caesarův	k2eAgInSc6d1
životě	život	k1gInSc6
pojednává	pojednávat	k5eAaImIp3nS
nejobšírněji	obšírně	k6eAd3
Gaius	Gaius	k1gMnSc1
Suetonius	Suetonius	k1gMnSc1
Tranquillus	Tranquillus	k1gMnSc1
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
díle	díl	k1gInSc6
Životopisy	životopis	k1gInPc1
dvanácti	dvanáct	k4xCc2
císařů	císař	k1gMnPc2
(	(	kIx(
<g/>
De	De	k?
Vita	vit	k2eAgFnSc1d1
Caesarum	Caesarum	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc4
spisovatel	spisovatel	k1gMnSc1
dohlížel	dohlížet	k5eAaImAgMnS
na	na	k7c4
císařské	císařský	k2eAgInPc4d1
archivy	archiv	k1gInPc4
za	za	k7c4
Hadriana	Hadrian	k1gMnSc4
na	na	k7c6
počátku	počátek	k1gInSc6
2	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Suetonius	Suetonius	k1gInSc1
měl	mít	k5eAaImAgInS
tudíž	tudíž	k8xC
přístup	přístup	k1gInSc1
k	k	k7c3
výtečnému	výtečný	k2eAgInSc3d1
zdroji	zdroj	k1gInSc3
informací	informace	k1gFnPc2
<g/>
,	,	kIx,
s	s	k7c7
nimiž	jenž	k3xRgInPc7
ovšem	ovšem	k9
zacházel	zacházet	k5eAaImAgMnS
se	s	k7c7
značnou	značný	k2eAgFnSc7d1
zdrženlivostí	zdrženlivost	k1gFnSc7
a	a	k8xC
vyjadřoval	vyjadřovat	k5eAaImAgMnS
pochybnosti	pochybnost	k1gFnPc4
o	o	k7c6
mnohých	mnohý	k2eAgNnPc6d1
tvrzeních	tvrzení	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Suetonius	Suetonius	k1gMnSc1
se	se	k3xPyFc4
nesnažil	snažit	k5eNaImAgMnS
sepsat	sepsat	k5eAaPmF
prosté	prostý	k2eAgInPc1d1
životopisy	životopis	k1gInPc1
<g/>
,	,	kIx,
ale	ale	k8xC
usiloval	usilovat	k5eAaImAgInS
i	i	k9
o	o	k7c4
vystižení	vystižení	k1gNnSc4
určitých	určitý	k2eAgFnPc2d1
mravních	mravní	k2eAgFnPc2d1
zásad	zásada	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
něho	on	k3xPp3gInSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
určitý	určitý	k2eAgMnSc1d1
člověk	člověk	k1gMnSc1
disponoval	disponovat	k5eAaBmAgMnS
absolutní	absolutní	k2eAgFnSc7d1
svobodou	svoboda	k1gFnSc7
a	a	k8xC
mocí	moc	k1gFnSc7
římského	římský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
<g/>
,	,	kIx,
muselo	muset	k5eAaImAgNnS
se	se	k3xPyFc4
jednat	jednat	k5eAaImF
o	o	k7c4
jedince	jedinec	k1gMnPc4
vskutku	vskutku	k9
pevného	pevný	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
<g/>
,	,	kIx,
jestliže	jestliže	k8xS
si	se	k3xPyFc3
chtěl	chtít	k5eAaImAgMnS
udržet	udržet	k5eAaPmF
svoji	svůj	k3xOyFgFnSc4
čestnost	čestnost	k1gFnSc4
a	a	k8xC
bezúhonnost	bezúhonnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
biografie	biografie	k1gFnPc1
znázorňují	znázorňovat	k5eAaImIp3nP
ve	v	k7c6
velkém	velký	k2eAgNnSc6d1
měřítku	měřítko	k1gNnSc6
krutost	krutost	k1gFnSc1
a	a	k8xC
různé	různý	k2eAgFnPc1d1
sexuální	sexuální	k2eAgFnPc1d1
úchylky	úchylka	k1gFnPc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
z	z	k7c2
něho	on	k3xPp3gMnSc2
činí	činit	k5eAaImIp3nS
přirozeně	přirozeně	k6eAd1
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
nejpopulárnějších	populární	k2eAgMnPc2d3
antických	antický	k2eAgMnPc2d1
historiků	historik	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc1
portréty	portrét	k1gInPc1
císařů	císař	k1gMnPc2
jsou	být	k5eAaImIp3nP
proto	proto	k8xC
považovány	považován	k2eAgFnPc1d1
za	za	k7c4
příliš	příliš	k6eAd1
temné	temný	k2eAgInPc4d1
a	a	k8xC
negativní	negativní	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řecký	řecký	k2eAgInSc1d1
autor	autor	k1gMnSc1
Plútarchos	Plútarchos	k1gInSc1
z	z	k7c2
Chairóneie	Chairóneie	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgInS
jen	jen	k9
o	o	k7c4
několik	několik	k4yIc4
let	léto	k1gNnPc2
mladší	mladý	k2eAgFnSc1d2
než	než	k8xS
Suetonius	Suetonius	k1gInSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
taktéž	taktéž	k?
zabýval	zabývat	k5eAaImAgInS
Caesarem	Caesar	k1gMnSc7
a	a	k8xC
v	v	k7c6
Paralelních	paralelní	k2eAgFnPc6d1
biografiích	biografie	k1gFnPc6
(	(	kIx(
<g/>
Bioi	Bioi	k1gNnSc6
parallè	parallè	k1gFnSc2
<g/>
)	)	kIx)
ho	on	k3xPp3gMnSc4
porovnal	porovnat	k5eAaPmAgMnS
s	s	k7c7
Alexandrem	Alexandr	k1gMnSc7
Velikým	veliký	k2eAgMnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
srovnání	srovnání	k1gNnSc2
nejen	nejen	k6eAd1
těchto	tento	k3xDgFnPc2
dvou	dva	k4xCgFnPc2
postav	postava	k1gFnPc2
vyplývá	vyplývat	k5eAaImIp3nS
jednoznačné	jednoznačný	k2eAgNnSc4d1
zjištění	zjištění	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
Řekové	Řek	k1gMnPc1
a	a	k8xC
Římané	Říman	k1gMnPc1
měli	mít	k5eAaImAgMnP
více	hodně	k6eAd2
společného	společný	k2eAgMnSc4d1
<g/>
,	,	kIx,
než	než	k8xS
byli	být	k5eAaImAgMnP
schopni	schopen	k2eAgMnPc1d1
připustit	připustit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Suetoniova	Suetoniův	k2eAgFnSc1d1
i	i	k8xC
Plútarchova	Plútarchův	k2eAgFnSc1d1
biografie	biografie	k1gFnSc1
představují	představovat	k5eAaImIp3nP
pouhý	pouhý	k2eAgInSc4d1
náčrt	náčrt	k1gInSc4
Caesarova	Caesarův	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
nám	my	k3xPp1nPc3
usnadňuje	usnadňovat	k5eAaImIp3nS
zařadit	zařadit	k5eAaPmF
informace	informace	k1gFnPc4
ostatních	ostatní	k2eAgMnPc2d1
spisovatelů	spisovatel	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
důležité	důležitý	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
náleží	náležet	k5eAaImIp3nS
samotnému	samotný	k2eAgMnSc3d1
Caesarovi	Caesar	k1gMnSc3
a	a	k8xC
jeho	jeho	k3xOp3gFnSc3
tvorbě	tvorba	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
nelze	lze	k6eNd1
pominout	pominout	k5eAaPmF
Ciceronovu	Ciceronův	k2eAgFnSc4d1
korespondenci	korespondence	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
Listy	lista	k1gFnSc2
Atticovi	Atticův	k2eAgMnPc1d1
(	(	kIx(
<g/>
Epistulae	Epistulae	k1gNnPc4
ad	ad	k7c4
Atticum	Atticum	k1gNnSc4
<g/>
)	)	kIx)
nám	my	k3xPp1nPc3
velkou	velký	k2eAgFnSc7d1
měrou	míra	k1gFnSc7wR
napomáhají	napomáhat	k5eAaBmIp3nP,k5eAaImIp3nP
pochopit	pochopit	k5eAaPmF
politický	politický	k2eAgInSc4d1
život	život	k1gInSc4
v	v	k7c6
Římě	Řím	k1gInSc6
v	v	k7c6
polovině	polovina	k1gFnSc6
1	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Tyto	tento	k3xDgInPc4
dopisy	dopis	k1gInPc4
byly	být	k5eAaImAgInP
znovu	znovu	k6eAd1
objeveny	objevit	k5eAaPmNgInP
až	až	k9
za	za	k7c2
vlády	vláda	k1gFnSc2
císaře	císař	k1gMnSc4
Nerona	Nero	k1gMnSc4
zhruba	zhruba	k6eAd1
o	o	k7c4
sto	sto	k4xCgNnSc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
mnohé	mnohý	k2eAgNnSc1d1
nepatřičně	patřičně	k6eNd1
se	se	k3xPyFc4
vyjadřující	vyjadřující	k2eAgMnSc1d1
o	o	k7c6
Caesarovi	Caesar	k1gMnSc3
nebyly	být	k5eNaImAgInP
vůbec	vůbec	k9
zveřejněny	zveřejnit	k5eAaPmNgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejné	stejný	k2eAgFnSc6d1
selekci	selekce	k1gFnSc6
byly	být	k5eAaImAgInP
podrobeny	podrobit	k5eAaPmNgInP
také	také	k9
soubory	soubor	k1gInPc4
Ciceronových	Ciceronův	k2eAgInPc2d1
Listů	list	k1gInPc2
přátelům	přítel	k1gMnPc3
(	(	kIx(
<g/>
Epistulae	Epistulae	k1gFnSc1
ad	ad	k7c4
Familiares	Familiares	k1gInSc4
<g/>
)	)	kIx)
a	a	k8xC
Listů	list	k1gInPc2
Brutovi	Brutovi	k1gRnPc1
(	(	kIx(
<g/>
Epistulae	Epistulae	k1gFnSc1
ad	ad	k7c4
Brutum	Brutum	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velká	k1gFnSc6
množství	množství	k1gNnSc4
údajů	údaj	k1gInPc2
je	být	k5eAaImIp3nS
obsaženo	obsáhnout	k5eAaPmNgNnS
rovněž	rovněž	k9
v	v	k7c6
projevech	projev	k1gInPc6
Cicerona	Cicero	k1gMnSc2
<g/>
,	,	kIx,
obzvláště	obzvláště	k6eAd1
v	v	k7c6
řečech	řeč	k1gFnPc6
za	za	k7c4
Marcella	Marcello	k1gNnPc4
(	(	kIx(
<g/>
Pro	pro	k7c4
Marcello	Marcello	k1gNnSc4
<g/>
)	)	kIx)
a	a	k8xC
za	za	k7c4
Ligaria	Ligarium	k1gNnPc4
(	(	kIx(
<g/>
Pro	pro	k7c4
Ligario	Ligario	k1gNnSc4
<g/>
)	)	kIx)
a	a	k8xC
ve	v	k7c6
Filipikách	filipika	k1gFnPc6
(	(	kIx(
<g/>
Philippicae	Philippicae	k1gNnSc6
<g/>
)	)	kIx)
proti	proti	k7c3
Marcu	Marc	k1gMnSc3
Antoniovi	Antonio	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Nejvýznamnějším	významný	k2eAgInSc7d3
pramenem	pramen	k1gInSc7
o	o	k7c6
Caesarově	Caesarův	k2eAgNnSc6d1
vystupování	vystupování	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
63	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
je	být	k5eAaImIp3nS
Catilinovo	Catilinův	k2eAgNnSc1d1
spiknutí	spiknutí	k1gNnSc1
(	(	kIx(
<g/>
Bellum	Bellum	k1gInSc1
Catilinae	Catilina	k1gInSc2
<g/>
)	)	kIx)
od	od	k7c2
Caesarova	Caesarův	k2eAgMnSc2d1
přívržence	přívrženec	k1gMnSc2
Gaia	Gaius	k1gMnSc2
Sallustia	Sallustius	k1gMnSc2
Crispa	Crisp	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
je	být	k5eAaImIp3nS
taktéž	taktéž	k?
autorem	autor	k1gMnSc7
Listů	list	k1gInPc2
Caesarovi	Caesarovi	k1gRnPc1
(	(	kIx(
<g/>
Epistolae	Epistolae	k1gFnSc1
ad	ad	k7c4
Caesarem	Caesar	k1gMnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kniha	kniha	k1gFnSc1
o	o	k7c6
Caesarovi	Caesar	k1gMnSc6
od	od	k7c2
Augustova	Augustův	k2eAgMnSc2d1
současníka	současník	k1gMnSc2
<g/>
,	,	kIx,
historika	historik	k1gMnSc2
Tita	Titus	k1gMnSc2
Livia	Livius	k1gMnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
až	až	k9
na	na	k7c4
určité	určitý	k2eAgInPc4d1
fragmenty	fragment	k1gInPc4
nedochovala	dochovat	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
Plútarchos	Plútarchos	k1gMnSc1
při	pře	k1gFnSc4
psaní	psaní	k1gNnSc2
svého	svůj	k3xOyFgInSc2
Caesarova	Caesarův	k2eAgInSc2d1
životopisu	životopis	k1gInSc2
čerpal	čerpat	k5eAaImAgInS
právě	právě	k9
z	z	k7c2
této	tento	k3xDgFnSc2
práce	práce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řada	řada	k1gFnSc1
podrobností	podrobnost	k1gFnPc2
o	o	k7c6
Caesarovi	Caesar	k1gMnSc6
je	být	k5eAaImIp3nS
zmíněna	zmíněn	k2eAgFnSc1d1
i	i	k9
v	v	k7c6
Plútarchových	Plútarchův	k2eAgFnPc6d1
biografiích	biografie	k1gFnPc6
Catona	Catona	k1gFnSc1
mladšího	mladý	k2eAgMnSc2d2
<g/>
,	,	kIx,
Cicerona	Cicero	k1gMnSc2
<g/>
,	,	kIx,
Crassa	Crass	k1gMnSc2
<g/>
,	,	kIx,
Marca	Marcus	k1gMnSc2
Antonia	Antonio	k1gMnSc2
a	a	k8xC
Pompeia	Pompeius	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
2	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
působil	působit	k5eAaImAgMnS
řecký	řecký	k2eAgMnSc1d1
dějepisec	dějepisec	k1gMnSc1
Appiános	Appiános	k1gMnSc1
z	z	k7c2
Alexandrie	Alexandrie	k1gFnSc2
popisující	popisující	k2eAgInSc1d1
zánik	zánik	k1gInSc1
římské	římský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Livia	Livius	k1gMnSc2
vycházel	vycházet	k5eAaImAgMnS
na	na	k7c6
počátku	počátek	k1gInSc6
3	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Cassius	Cassius	k1gMnSc1
Dio	Dio	k1gMnSc1
<g/>
,	,	kIx,
jehož	jenž	k3xRgMnSc4,k3xOyRp3gMnSc4
dílo	dílo	k1gNnSc1
je	být	k5eAaImIp3nS
nepochybně	pochybně	k6eNd1
důležitým	důležitý	k2eAgInSc7d1
pramenem	pramen	k1gInSc7
zápasu	zápas	k1gInSc2
o	o	k7c4
Caesarovo	Caesarův	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
C.	C.	kA
Iulii	Iulius	k1gMnPc7
Caesaris	Caesaris	k1gFnSc4
quae	quae	k1gNnSc2
extant	extanta	k1gFnPc2
<g/>
,	,	kIx,
1678	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
Julius	Julius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
Gaius	Gaius	k1gMnSc1
Iulius	Iulius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Nikolaj	Nikolaj	k1gMnSc1
Petrovič	Petrovič	k1gMnSc1
Obnorskij	Obnorskij	k1gMnSc1
<g/>
:	:	kIx,
Ю	Ю	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Encyklopedický	encyklopedický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
Brockhaus-Jefron	Brockhaus-Jefron	k1gInSc1
<g/>
,	,	kIx,
svazek	svazek	k1gInSc1
XLI	XLI	kA
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
naučný	naučný	k2eAgInSc1d1
<g/>
↑	↑	k?
Lucie	Lucie	k1gFnSc1
Pultrová	Pultrová	k1gFnSc1
<g/>
,	,	kIx,
O	o	k7c6
výslovnosti	výslovnost	k1gFnSc6
klasické	klasický	k2eAgFnSc2d1
latiny	latina	k1gFnSc2
<g/>
,	,	kIx,
dostupné	dostupný	k2eAgFnPc1d1
on-line	on-lin	k1gInSc5
<g/>
,	,	kIx,
21.04	21.04	k4
<g/>
.2009	.2009	k4
<g/>
↑	↑	k?
RÜKL	RÜKL	kA
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atlas	Atlas	k1gInSc1
Měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Aventinum	Aventinum	k1gNnSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85277	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Arago	Arago	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
96	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Plútarchos	Plútarchos	k1gInSc4
<g/>
,	,	kIx,
Životopisy	životopis	k1gInPc4
slavných	slavný	k2eAgMnPc2d1
Řeků	Řek	k1gMnPc2
a	a	k8xC
Římanů	Říman	k1gMnPc2
II	II	kA
<g/>
,	,	kIx,
Gaius	Gaius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
<g/>
,	,	kIx,
63	#num#	k4
<g/>
↑	↑	k?
Plútarchos	Plútarchosa	k1gFnPc2
<g/>
,	,	kIx,
Životopisy	životopis	k1gInPc4
slavných	slavný	k2eAgMnPc2d1
Řeků	Řek	k1gMnPc2
a	a	k8xC
Římanů	Říman	k1gMnPc2
II	II	kA
<g/>
,	,	kIx,
Gaius	Gaius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
<g/>
,	,	kIx,
66	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
APPIÁNOS	APPIÁNOS	kA
<g/>
,	,	kIx,
Krize	krize	k1gFnSc1
římské	římský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-205-0060-X	80-205-0060-X	k4
</s>
<s>
BURIAN	Burian	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
,	,	kIx,
G.	G.	kA
J.	J.	kA
Caesar	Caesar	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
:	:	kIx,
Orbis	orbis	k1gInSc1
<g/>
,	,	kIx,
1963	#num#	k4
</s>
<s>
CAESAR	Caesar	k1gMnSc1
<g/>
,	,	kIx,
Gaius	Gaius	k1gMnSc1
Iulius	Iulius	k1gMnSc1
<g/>
,	,	kIx,
Válečné	válečný	k2eAgFnSc6d1
paměti	paměť	k1gFnSc6
<g/>
:	:	kIx,
o	o	k7c6
válce	válka	k1gFnSc6
gallské	gallský	k2eAgFnSc6d1
<g/>
,	,	kIx,
o	o	k7c6
válce	válka	k1gFnSc6
občanské	občanský	k2eAgFnSc6d1
<g/>
,	,	kIx,
alexandrijské	alexandrijský	k2eAgFnSc6d1
<g/>
,	,	kIx,
africké	africký	k2eAgFnSc6d1
a	a	k8xC
hispánské	hispánský	k2eAgFnSc6d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1972	#num#	k4
</s>
<s>
CAESAR	Caesar	k1gMnSc1
<g/>
,	,	kIx,
Gaius	Gaius	k1gMnSc1
Iulius	Iulius	k1gMnSc1
<g/>
,	,	kIx,
Zápisky	zápiska	k1gFnPc1
o	o	k7c6
válce	válka	k1gFnSc6
galské	galský	k2eAgFnSc6d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
:	:	kIx,
Naše	náš	k3xOp1gNnSc1
vojsko	vojsko	k1gNnSc1
<g/>
,	,	kIx,
1986	#num#	k4
</s>
<s>
CAESAR	Caesar	k1gMnSc1
<g/>
,	,	kIx,
Gaius	Gaius	k1gMnSc1
Iulius	Iulius	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zápisky	zápisek	k1gInPc4
o	o	k7c6
válce	válka	k1gFnSc6
gallské	gallský	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Josef	Josef	k1gMnSc1
V.	V.	kA
Víta	Víta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Alexander	Alexandra	k1gFnPc2
Storch	Storch	k1gInSc1
<g/>
,	,	kIx,
1882	#num#	k4
<g/>
.	.	kIx.
230	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
CANFORA	CANFORA	kA
<g/>
,	,	kIx,
Luciano	Luciana	k1gFnSc5
<g/>
,	,	kIx,
Gaius	Gaius	k1gMnSc1
Iulius	Iulius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
<g/>
:	:	kIx,
demokratický	demokratický	k2eAgMnSc1d1
diktátor	diktátor	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-80-7021-901-0	978-80-7021-901-0	k4
</s>
<s>
GRANT	grant	k1gInSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
,	,	kIx,
Dějiny	dějiny	k1gFnPc1
antického	antický	k2eAgInSc2d1
Říma	Řím	k1gInSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
:	:	kIx,
BB	BB	kA
<g/>
/	/	kIx~
<g/>
art	art	k?
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7341-930-0	80-7341-930-0	k4
</s>
<s>
GRANT	grant	k1gInSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
,	,	kIx,
Dvanáct	dvanáct	k4xCc1
Cézarů	Cézar	k1gMnPc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
:	:	kIx,
BB	BB	kA
art	art	k?
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-86070-60-3	80-86070-60-3	k4
</s>
<s>
HOLLAND	HOLLAND	kA
<g/>
,	,	kIx,
Tom	Tom	k1gMnSc1
<g/>
,	,	kIx,
Rubikon	Rubikon	k1gInSc1
<g/>
:	:	kIx,
triumf	triumf	k1gInSc1
a	a	k8xC
tragédie	tragédie	k1gFnSc1
římské	římský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
:	:	kIx,
Dokořán	dokořán	k6eAd1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-86569-86-1	80-86569-86-1	k4
</s>
<s>
MOMMSEN	MOMMSEN	kA
<g/>
,	,	kIx,
Theodor	Theodor	k1gMnSc1
<g/>
,	,	kIx,
Diktátoři	diktátor	k1gMnPc1
<g/>
,	,	kIx,
Olomouc	Olomouc	k1gFnSc4
:	:	kIx,
Votobia	Votobia	k1gFnSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7198-145-1	80-7198-145-1	k4
</s>
<s>
PLÚTARCHOS	PLÚTARCHOS	kA
<g/>
,	,	kIx,
Životopisy	životopis	k1gInPc4
slavných	slavný	k2eAgMnPc2d1
Řeků	Řek	k1gMnPc2
a	a	k8xC
Římanů	Říman	k1gMnPc2
II	II	kA
(	(	kIx(
<g/>
Gaius	Gaius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
:	:	kIx,
Arista	Arista	k1gMnSc1
<g/>
,	,	kIx,
Baset	Baset	k1gMnSc1
<g/>
,	,	kIx,
Maitrea	Maitrea	k1gMnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-86410-46-3	80-86410-46-3	k4
</s>
<s>
SUETONIUS	SUETONIUS	kA
<g/>
,	,	kIx,
Gaius	Gaius	k1gMnSc1
Tranquillus	Tranquillus	k1gMnSc1
<g/>
,	,	kIx,
Životopisy	životopis	k1gInPc1
dvanácti	dvanáct	k4xCc2
císařů	císař	k1gMnPc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-902300-5-9	80-902300-5-9	k4
</s>
<s>
VANDENBERG	VANDENBERG	kA
<g/>
,	,	kIx,
Philipp	Philipp	k1gMnSc1
<g/>
,	,	kIx,
Caesar	Caesar	k1gMnSc1
a	a	k8xC
Kleopatra	Kleopatra	k1gFnSc1
<g/>
,	,	kIx,
Frýdek-Místek	Frýdek-Místek	k1gInSc1
:	:	kIx,
Alpress	Alpress	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7362-106-1	80-7362-106-1	k4
</s>
<s>
ZAMAROVSKÝ	ZAMAROVSKÝ	kA
<g/>
,	,	kIx,
Vojtěch	Vojtěch	k1gMnSc1
<g/>
,	,	kIx,
Dějiny	dějiny	k1gFnPc1
psané	psaný	k2eAgNnSc1d1
Římem	Řím	k1gInSc7
<g/>
,	,	kIx,
Bratislava	Bratislava	k1gFnSc1
:	:	kIx,
Perfekt	perfektum	k1gNnPc2
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-8046-297-6	80-8046-297-6	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Caesar	Caesar	k1gMnSc1
</s>
<s>
Julius	Julius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
(	(	kIx(
<g/>
Shakespeare	Shakespeare	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Juliánský	juliánský	k2eAgInSc4d1
kalendář	kalendář	k1gInSc4
</s>
<s>
Caesarův	Caesarův	k2eAgInSc1d1
chrám	chrám	k1gInSc1
</s>
<s>
Caesarova	Caesarův	k2eAgFnSc1d1
šifra	šifra	k1gFnSc1
</s>
<s>
Caesarova	Caesarův	k2eAgFnSc1d1
kašna	kašna	k1gFnSc1
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Julius	Julius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Julius	Julius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
</s>
<s>
Osoba	osoba	k1gFnSc1
Gaius	Gaius	k1gMnSc1
Iulius	Iulius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Digitalizovaná	digitalizovaný	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
Gaia	Gaius	k1gMnSc2
Julia	Julius	k1gMnSc2
Caesara	Caesar	k1gMnSc2
v	v	k7c6
digitální	digitální	k2eAgFnSc6d1
knihovně	knihovna	k1gFnSc6
Kramerius	Kramerius	k1gMnSc1
NK	NK	kA
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s>
Gaius	Gaius	k1gMnSc1
Iulius	Iulius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
(	(	kIx(
<g/>
stránky	stránka	k1gFnPc4
Antika	antika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Caesarova	Caesarův	k2eAgFnSc1d1
diktatura	diktatura	k1gFnSc1
očima	oko	k1gNnPc7
antických	antický	k2eAgMnPc2d1
historiků	historik	k1gMnPc2
-	-	kIx~
Staré	Staré	k2eAgInPc1d1
národy	národ	k1gInPc1
|	|	kIx~
civilizace	civilizace	k1gFnSc1
</s>
<s>
Gaius	Gaius	k1gMnSc1
Julius	Julius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
(	(	kIx(
<g/>
Livius	Livius	k1gMnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
)	)	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Gaius	Gaius	k1gMnSc1
Julius	Julius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
(	(	kIx(
<g/>
100	#num#	k4
-	-	kIx~
44	#num#	k4
v.	v.	k?
Christus	Christus	k1gMnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Von	von	k1gInSc1
der	drát	k5eAaImRp2nS
Ermorderung	Ermorderung	k1gMnSc1
des	des	k1gNnSc2
Gaius	Gaius	k1gMnSc1
Iulius	Iulius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Gaius	Gaius	k1gMnSc1
Julius	Julius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
(	(	kIx(
<g/>
UNRV	UNRV	kA
History	Histor	k1gInPc4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Julius	Julius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
(	(	kIx(
<g/>
The	The	k1gMnSc1
Roman	Roman	k1gMnSc1
Empire	empir	k1gInSc5
<g/>
)	)	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
JULIUS	Julius	k1gMnSc1
CAESAR	Caesar	k1gMnSc1
<g/>
:	:	kIx,
HISTORICAL	HISTORICAL	kA
BACKGROUND	BACKGROUND	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
19981000434	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118518275	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2283	#num#	k4
4514	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79021400	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500077373	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
286265178	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79021400	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Politika	politika	k1gFnSc1
|	|	kIx~
Starověký	starověký	k2eAgInSc1d1
Řím	Řím	k1gInSc1
</s>
