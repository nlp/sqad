<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Juřička	Juřička	k1gMnSc1	Juřička
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1960	[number]	k4	1960
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
dabér	dabér	k1gMnSc1	dabér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
Státní	státní	k2eAgFnSc4d1	státní
konzervatoř	konzervatoř	k1gFnSc4	konzervatoř
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
studií	studie	k1gFnPc2	studie
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
členem	člen	k1gMnSc7	člen
souboru	soubor	k1gInSc2	soubor
Divadle	divadlo	k1gNnSc6	divadlo
Jiřího	Jiří	k1gMnSc2	Jiří
Wolkera	Wolker	k1gMnSc2	Wolker
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
televizi	televize	k1gFnSc6	televize
i	i	k8xC	i
ve	v	k7c6	v
filmu	film	k1gInSc6	film
debutoval	debutovat	k5eAaBmAgMnS	debutovat
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
hrál	hrát	k5eAaImAgMnS	hrát
např.	např.	kA	např.
v	v	k7c6	v
seriálech	seriál	k1gInPc6	seriál
Malý	malý	k2eAgInSc1d1	malý
pitaval	pitaval	k1gInSc1	pitaval
z	z	k7c2	z
velkého	velký	k2eAgNnSc2d1	velké
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Dynastie	dynastie	k1gFnSc2	dynastie
Nováků	Novák	k1gMnPc2	Novák
či	či	k8xC	či
Rodáci	rodák	k1gMnPc1	rodák
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc4d3	veliký
roli	role	k1gFnSc4	role
však	však	k9	však
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Sanitka	sanitka	k1gFnSc1	sanitka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ztvárnil	ztvárnit	k5eAaPmAgInS	ztvárnit
postavu	postava	k1gFnSc4	postava
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Jandery	Jandera	k1gMnSc2	Jandera
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
a	a	k8xC	a
ve	v	k7c6	v
filmu	film	k1gInSc6	film
objevuje	objevovat	k5eAaImIp3nS	objevovat
spíše	spíše	k9	spíše
příležitostně	příležitostně	k6eAd1	příležitostně
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgMnS	hrát
např.	např.	kA	např.
v	v	k7c6	v
pohádce	pohádka	k1gFnSc6	pohádka
Princezna	princezna	k1gFnSc1	princezna
ze	z	k7c2	z
mlejna	mlejn	k1gInSc2	mlejn
2	[number]	k4	2
nebo	nebo	k8xC	nebo
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Pojišťovna	pojišťovna	k1gFnSc1	pojišťovna
štěstí	štěstí	k1gNnSc2	štěstí
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
převážně	převážně	k6eAd1	převážně
dabingu	dabing	k1gInSc2	dabing
<g/>
,	,	kIx,	,
podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
desítkách	desítka	k1gFnPc6	desítka
filmů	film	k1gInPc2	film
a	a	k8xC	a
seriálů	seriál	k1gInPc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
větších	veliký	k2eAgFnPc2d2	veliký
rolí	role	k1gFnPc2	role
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
např.	např.	kA	např.
Eric	Eric	k1gInSc1	Eric
Weiss	Weiss	k1gMnSc1	Weiss
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Alias	alias	k9	alias
<g/>
,	,	kIx,	,
Radek	Radek	k1gMnSc1	Radek
Zelenka	Zelenka	k1gMnSc1	Zelenka
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
:	:	kIx,	:
Atlantida	Atlantida	k1gFnSc1	Atlantida
nebo	nebo	k8xC	nebo
Quark	Quark	k1gInSc1	Quark
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
:	:	kIx,	:
Stanice	stanice	k1gFnPc1	stanice
Deep	Deep	k1gInSc4	Deep
Space	Space	k1gFnSc2	Space
Nine	Nin	k1gInSc2	Nin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
dostal	dostat	k5eAaPmAgMnS	dostat
za	za	k7c4	za
postavu	postava	k1gFnSc4	postava
Ranceho	Rance	k1gMnSc2	Rance
otce	otec	k1gMnSc2	otec
v	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
Hvězda	hvězda	k1gFnSc1	hvězda
salónu	salón	k1gInSc2	salón
Cenu	cena	k1gFnSc4	cena
Františka	František	k1gMnSc4	František
Filipovského	Filipovský	k1gMnSc4	Filipovský
<g/>
.	.	kIx.	.
<g/>
Jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
disident	disident	k1gMnSc1	disident
František	František	k1gMnSc1	František
Juřička	Juřička	k1gMnSc1	Juřička
<g/>
,	,	kIx,	,
matkou	matka	k1gFnSc7	matka
herečka	herečka	k1gFnSc1	herečka
Marie	Marie	k1gFnSc1	Marie
Grafnetterová	Grafnetterová	k1gFnSc1	Grafnetterová
<g/>
.	.	kIx.	.
</s>
<s>
Sestra	sestra	k1gFnSc1	sestra
Lucie	Lucie	k1gFnSc2	Lucie
je	být	k5eAaImIp3nS	být
také	také	k9	také
herečkou	herečka	k1gFnSc7	herečka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tomáš	Tomáš	k1gMnSc1	Tomáš
Juřička	Juřička	k1gFnSc1	Juřička
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Juřička	Juřička	k1gMnSc1	Juřička
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
