<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
Státní	státní	k2eAgFnSc4d1	státní
konzervatoř	konzervatoř	k1gFnSc4	konzervatoř
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
studií	studie	k1gFnPc2	studie
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
členem	člen	k1gMnSc7	člen
souboru	soubor	k1gInSc2	soubor
Divadle	divadlo	k1gNnSc6	divadlo
Jiřího	Jiří	k1gMnSc2	Jiří
Wolkera	Wolker	k1gMnSc2	Wolker
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
