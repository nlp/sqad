<s>
Arabské	arabský	k2eAgFnPc1d1	arabská
číslice	číslice	k1gFnPc1	číslice
jsou	být	k5eAaImIp3nP	být
následující	následující	k2eAgMnSc1d1	následující
<g/>
:	:	kIx,	:
0	[number]	k4	0
1	[number]	k4	1
2	[number]	k4	2
3	[number]	k4	3
4	[number]	k4	4
5	[number]	k4	5
6	[number]	k4	6
7	[number]	k4	7
8	[number]	k4	8
9	[number]	k4	9
Římská	římský	k2eAgNnPc1d1	římské
čísla	číslo	k1gNnPc1	číslo
jsou	být	k5eAaImIp3nP	být
tvořena	tvořit	k5eAaImNgNnP	tvořit
pomocí	pomocí	k7c2	pomocí
těchto	tento	k3xDgNnPc2	tento
písmen	písmeno	k1gNnPc2	písmeno
latinky	latinka	k1gFnSc2	latinka
<g/>
:	:	kIx,	:
I	i	k9	i
V	v	k7c6	v
X	X	kA	X
L	L	kA	L
C	C	kA	C
D	D	kA	D
M	M	kA	M
Podle	podle	k7c2	podle
mluvčích	mluvčí	k1gFnPc2	mluvčí
Latinku	latinka	k1gFnSc4	latinka
používá	používat	k5eAaImIp3nS	používat
přes	přes	k7c4	přes
2,5	[number]	k4	2,5
miliardy	miliarda	k4xCgFnSc2	miliarda
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
