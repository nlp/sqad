<s>
Roger	Roger	k1gInSc1
Hanin	Hanin	k2eAgInSc1d1
</s>
<s>
Roger	Roger	k1gMnSc1
Hanin	Hanin	k2eAgMnSc1d1
Rodné	rodný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Roger	Roger	k1gInSc1
Paul	Paula	k1gFnPc2
Jacob	Jacoba	k1gFnPc2
Lévy	Léva	k1gMnSc2
Narození	narození	k1gNnSc2
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1925	#num#	k4
Alžír	Alžír	k1gInSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2015	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
89	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
15	#num#	k4
<g/>
.	.	kIx.
obvod	obvod	k1gInSc1
Národnost	národnost	k1gFnSc4
</s>
<s>
Židé	Žid	k1gMnPc1
Povolání	povolání	k1gNnSc2
</s>
<s>
herec	herec	k1gMnSc1
<g/>
,	,	kIx,
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
filmový	filmový	k2eAgMnSc1d1
režisér	režisér	k1gMnSc1
<g/>
,	,	kIx,
scenárista	scenárista	k1gMnSc1
<g/>
,	,	kIx,
filmový	filmový	k2eAgMnSc1d1
herec	herec	k1gMnSc1
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
a	a	k8xC
televizní	televizní	k2eAgMnSc1d1
herec	herec	k1gMnSc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Christine	Christin	k1gMnSc5
Gouze-Rénal	Gouze-Rénal	k1gMnSc5
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Národní	národní	k2eAgInSc1d1
řád	řád	k1gInSc1
za	za	k7c4
zásluhy	zásluha	k1gFnPc4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
databázi	databáze	k1gFnSc6
Národní	národní	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Roger	Roger	k1gInSc1
Hanin	Hanin	k2eAgInSc1d1
<g/>
,	,	kIx,
rodným	rodný	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Roger	Rogero	k1gNnPc2
Lévy	Léva	k1gFnSc2
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1925	#num#	k4
Alžír	Alžír	k1gInSc1
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
Francouzské	francouzský	k2eAgNnSc1d1
Alžírsko	Alžírsko	k1gNnSc1
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
Alžírsko	Alžírsko	k1gNnSc1
–	–	k?
11	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2015	#num#	k4
Paříž	Paříž	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
francouzský	francouzský	k2eAgMnSc1d1
herec	herec	k1gMnSc1
a	a	k8xC
režisér	režisér	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Českým	český	k2eAgInSc7d1
televizním	televizní	k2eAgInSc7d1
divákům	divák	k1gMnPc3
je	být	k5eAaImIp3nS
patrně	patrně	k6eAd1
ponejvíce	ponejvíce	k6eAd1
znám	znám	k2eAgMnSc1d1
jakožto	jakožto	k8xS
představitel	představitel	k1gMnSc1
hlavní	hlavní	k2eAgFnSc2d1
role	role	k1gFnSc2
ve	v	k7c6
francouzském	francouzský	k2eAgInSc6d1
kriminálním	kriminální	k2eAgInSc6d1
a	a	k8xC
detektivním	detektivní	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
Navarro	Navarra	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
francouzské	francouzský	k2eAgFnSc2d1
židovské	židovský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
žila	žít	k5eAaImAgFnS
v	v	k7c6
Alžíru	Alžír	k1gInSc6
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
dědeček	dědeček	k1gMnSc1
byl	být	k5eAaImAgMnS
rabín	rabín	k1gMnSc1
<g/>
,	,	kIx,
otec	otec	k1gMnSc1
telekomunikační	telekomunikační	k2eAgMnSc1d1
úředník	úředník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
po	po	k7c6
svatbě	svatba	k1gFnSc6
konvertoval	konvertovat	k5eAaBmAgMnS
na	na	k7c4
katolickou	katolický	k2eAgFnSc4d1
víru	víra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
a	a	k8xC
nacistické	nacistický	k2eAgFnSc2d1
okupace	okupace	k1gFnSc2
severní	severní	k2eAgFnSc2d1
Francie	Francie	k1gFnSc2
měl	mít	k5eAaImAgInS
problémy	problém	k1gInPc4
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
židovským	židovský	k2eAgInSc7d1
původem	původ	k1gInSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgMnS
vyloučen	vyloučit	k5eAaPmNgMnS
ze	z	k7c2
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
francouzská	francouzský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
ve	v	k7c4
Vichy	Vicha	k1gFnPc4
uplatňovala	uplatňovat	k5eAaImAgFnS
i	i	k9
ve	v	k7c6
školství	školství	k1gNnSc6
antisemitské	antisemitský	k2eAgInPc4d1
zákony	zákon	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1952	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
hrál	hrát	k5eAaImAgInS
aktivně	aktivně	k6eAd1
divadlo	divadlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
televizi	televize	k1gFnSc6
vystupoval	vystupovat	k5eAaImAgMnS
od	od	k7c2
roku	rok	k1gInSc2
1971	#num#	k4
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1973	#num#	k4
působil	působit	k5eAaImAgInS
i	i	k9
jako	jako	k9
režisér	režisér	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Sám	sám	k3xTgMnSc1
o	o	k7c6
sobě	se	k3xPyFc3
prohlašoval	prohlašovat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
celoživotní	celoživotní	k2eAgMnSc1d1
komunista	komunista	k1gMnSc1
i	i	k8xC
aktivní	aktivní	k2eAgMnSc1d1
odpůrce	odpůrce	k1gMnSc1
antisemitismu	antisemitismus	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc4,k3yRnSc4
potvrdil	potvrdit	k5eAaPmAgInS
také	také	k9
svojí	svůj	k3xOyFgFnSc7
soudní	soudní	k2eAgFnSc7d1
pří	pře	k1gFnSc7
s	s	k7c7
Jeanem-Marie	Jeanem-Marie	k1gFnSc2
Le	Le	k1gMnSc7
Penem	Pen	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2008	#num#	k4
oficiálně	oficiálně	k6eAd1
ukončil	ukončit	k5eAaPmAgInS
svoji	svůj	k3xOyFgFnSc4
hereckou	herecký	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
únoru	únor	k1gInSc6
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
Roger	Roger	k1gInSc4
Hanin	Hanin	k2eAgInSc4d1
na	na	k7c6
Filmovém	filmový	k2eAgInSc6d1
festivalu	festival	k1gInSc6
v	v	k7c6
Cannes	Cannes	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
Christine	Christin	k1gInSc5
Gouze-Rénal	Gouze-Rénal	k1gMnSc6
(	(	kIx(
<g/>
1914	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
filmová	filmový	k2eAgFnSc1d1
producentka	producentka	k1gFnSc1
a	a	k8xC
podnikatelka	podnikatelka	k1gFnSc1
<g/>
,	,	kIx,
sestra	sestra	k1gFnSc1
bývalé	bývalý	k2eAgFnSc2d1
první	první	k4xOgFnSc2
dámy	dáma	k1gFnSc2
Francie	Francie	k1gFnSc2
Danielle	Danielle	k1gFnSc2
Mitterrandové	Mitterrandový	k2eAgFnSc2d1
<g/>
,	,	kIx,
manželky	manželka	k1gFnSc2
Françoise	Françoise	k1gFnSc2
Mitterranda	Mitterranda	k1gFnSc1
(	(	kIx(
<g/>
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
jim	on	k3xPp3gMnPc3
byl	být	k5eAaImAgMnS
na	na	k7c6
jejich	jejich	k3xOp3gFnSc6
svatbě	svatba	k1gFnSc6
za	za	k7c4
svědka	svědek	k1gMnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3
role	role	k1gFnSc1
</s>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
se	se	k3xPyFc4
jeho	jeho	k3xOp3gMnSc1
švagr	švagr	k1gMnSc1
François	François	k1gFnSc2
Mitterrand	Mitterrand	k1gInSc1
stal	stát	k5eAaPmAgMnS
francouzským	francouzský	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
<g/>
,	,	kIx,
obdržel	obdržet	k5eAaPmAgMnS
svoji	svůj	k3xOyFgFnSc4
nejproslulejší	proslulý	k2eAgFnSc4d3
televizní	televizní	k2eAgFnSc4d1
roli	role	k1gFnSc4
komisaře	komisař	k1gMnSc2
Navarra	Navarra	k1gFnSc1
ve	v	k7c6
stejnojmenném	stejnojmenný	k2eAgInSc6d1
francouzském	francouzský	k2eAgInSc6d1
televizním	televizní	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
vysílán	vysílat	k5eAaImNgInS
na	na	k7c6
francouzském	francouzský	k2eAgInSc6d1
televizním	televizní	k2eAgInSc6d1
kanálu	kanál	k1gInSc6
TF1	TF1	k1gFnSc2
od	od	k7c2
října	říjen	k1gInSc2
1989	#num#	k4
do	do	k7c2
října	říjen	k1gInSc2
2008	#num#	k4
(	(	kIx(
<g/>
tedy	tedy	k9
po	po	k7c4
20	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vyznamenání	vyznamenání	k1gNnSc1
</s>
<s>
achir	achir	k1gInSc1
Národního	národní	k2eAgInSc2d1
řádu	řád	k1gInSc2
za	za	k7c4
zásluhy	zásluha	k1gFnPc4
–	–	k?
Alžírsko	Alžírsko	k1gNnSc1
<g/>
,	,	kIx,
27	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2000	#num#	k4
–	–	k?
udělil	udělit	k5eAaPmAgMnS
prezident	prezident	k1gMnSc1
Abdelazíz	Abdelazíz	k1gMnSc1
Buteflika	Buteflika	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Publikační	publikační	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
</s>
<s>
L	L	kA
<g/>
'	'	kIx"
<g/>
ours	ours	k1gInSc1
en	en	k?
lambeaux	lambeaux	k1gInSc1
<g/>
,	,	kIx,
1981	#num#	k4
</s>
<s>
Le	Le	k?
voyage	voyage	k1gInSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Arsè	Arsè	k1gMnSc5
<g/>
,	,	kIx,
1985	#num#	k4
</s>
<s>
Lettre	Lettr	k1gMnSc5
à	à	k?
un	un	k?
ami	ami	k?
mystérieux	mystérieux	k1gInSc1
(	(	kIx(
<g/>
François	François	k1gInSc1
Mitterrand	Mitterrand	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Ed	Ed	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Grasset	Grasseta	k1gFnPc2
<g/>
,	,	kIx,
2001	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřel	zemřít	k5eAaPmAgMnS
herec	herec	k1gMnSc1
Roger	Roger	k1gMnSc1
Hanin	Hanin	k2eAgMnSc1d1
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
komisař	komisař	k1gMnSc1
Navarro	Navarra	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-02-11	2015-02-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Journal	Journal	k1gMnSc1
Officiel	Officiel	k1gMnSc1
de	de	k?
la	la	k1gNnSc4
Republique	Republiqu	k1gFnSc2
Algerienne	Algerienn	k1gInSc5
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2000	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
73	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Roger	Roger	k1gMnSc1
Hanin	Hanin	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Rozhovor	rozhovor	k1gInSc1
s	s	k7c7
Rogerem	Roger	k1gMnSc7
Haninem	Hanin	k1gMnSc7
na	na	k7c6
stránkách	stránka	k1gFnPc6
Francouzský	francouzský	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Roger	Roger	k1gInSc1
Hanin	Hanin	k2eAgInSc1d1
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Roger	Roger	k1gInSc1
Hanin	Hanin	k2eAgInSc1d1
ve	v	k7c6
Filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
151607	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
130475955	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
7972	#num#	k4
2591	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
83224915	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
27065733	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
83224915	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
|	|	kIx~
Francie	Francie	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Umění	umění	k1gNnSc1
</s>
