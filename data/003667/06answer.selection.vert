<s>
Klima	klima	k1gNnSc1	klima
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
zeměpisné	zeměpisný	k2eAgFnSc6d1	zeměpisná
šířce	šířka	k1gFnSc6	šířka
<g/>
.	.	kIx.	.
</s>
