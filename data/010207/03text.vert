<p>
<s>
Remscheid	Remscheid	k1gInSc1	Remscheid
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc4	město
s	s	k7c7	s
postavením	postavení	k1gNnSc7	postavení
městského	městský	k2eAgInSc2d1	městský
okresu	okres	k1gInSc2	okres
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
spolkové	spolkový	k2eAgFnSc6d1	spolková
zemi	zem	k1gFnSc6	zem
Severní	severní	k2eAgNnSc4d1	severní
Porýní-Vestfálsko	Porýní-Vestfálsko	k1gNnSc4	Porýní-Vestfálsko
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
několika	několik	k4yIc7	několik
dalšími	další	k2eAgNnPc7d1	další
městy	město	k1gNnPc7	město
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
regionu	region	k1gInSc2	region
Bergisches	Bergisches	k1gMnSc1	Bergisches
Land	Land	k1gMnSc1	Land
a	a	k8xC	a
s	s	k7c7	s
městy	město	k1gNnPc7	město
Wuppertal	Wuppertal	k1gMnSc1	Wuppertal
a	a	k8xC	a
Solingen	Solingen	k1gInSc1	Solingen
tvoří	tvořit	k5eAaImIp3nS	tvořit
geografický	geografický	k2eAgInSc1d1	geografický
a	a	k8xC	a
kulturní	kulturní	k2eAgInSc1d1	kulturní
region	region	k1gInSc1	region
Bergisches	Bergisches	k1gMnSc1	Bergisches
Städtedreieck	Städtedreieck	k1gMnSc1	Städtedreieck
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
do	do	k7c2	do
rozsáhlého	rozsáhlý	k2eAgInSc2d1	rozsáhlý
Metropolitního	metropolitní	k2eAgInSc2d1	metropolitní
regionu	region	k1gInSc2	region
Porýní-Porúří	Porýní-Porúří	k1gNnSc2	Porýní-Porúří
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
přesáhl	přesáhnout	k5eAaPmAgInS	přesáhnout
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
hranici	hranice	k1gFnSc4	hranice
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
zařadilo	zařadit	k5eAaPmAgNnS	zařadit
mezi	mezi	k7c4	mezi
velkoměsta	velkoměsto	k1gNnPc4	velkoměsto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
téměř	téměř	k6eAd1	téměř
110	[number]	k4	110
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Remscheid	Remscheid	k1gInSc1	Remscheid
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
spolkové	spolkový	k2eAgFnSc2d1	spolková
země	zem	k1gFnSc2	zem
Severní	severní	k2eAgNnSc4d1	severní
Porýní-Vestfálsko	Porýní-Vestfálsko	k1gNnSc4	Porýní-Vestfálsko
nejmenším	malý	k2eAgNnSc7d3	nejmenší
velkoměstem	velkoměsto	k1gNnSc7	velkoměsto
s	s	k7c7	s
postavením	postavení	k1gNnSc7	postavení
městského	městský	k2eAgInSc2d1	městský
okresu	okres	k1gInSc2	okres
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
hutním	hutní	k2eAgInSc7d1	hutní
průmyslem	průmysl	k1gInSc7	průmysl
a	a	k8xC	a
zpracováním	zpracování	k1gNnSc7	zpracování
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Quimper	Quimper	k1gInSc1	Quimper
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Wansbeck	Wansbeck	k1gInSc1	Wansbeck
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	let	k1gInPc6	let
1978	[number]	k4	1978
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Prešov	Prešov	k1gInSc1	Prešov
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pirna	Pirna	k1gFnSc1	Pirna
<g/>
,	,	kIx,	,
Sasko	Sasko	k1gNnSc1	Sasko
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Šmalkaldy	Šmalkald	k1gInPc1	Šmalkald
<g/>
,	,	kIx,	,
Durynsko	Durynsko	k1gNnSc1	Durynsko
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Remscheid	Remscheida	k1gFnPc2	Remscheida
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Remscheid	Remscheida	k1gFnPc2	Remscheida
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
města	město	k1gNnSc2	město
</s>
</p>
