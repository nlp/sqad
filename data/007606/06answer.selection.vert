<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnSc3	jeho
bohaté	bohatý	k2eAgFnSc3d1	bohatá
historii	historie	k1gFnSc3	historie
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
nalézt	nalézt	k5eAaBmF	nalézt
stovky	stovka	k1gFnPc4	stovka
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
devíti	devět	k4xCc2	devět
národních	národní	k2eAgFnPc2d1	národní
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
a	a	k8xC	a
jedné	jeden	k4xCgFnSc2	jeden
památky	památka	k1gFnSc2	památka
zařazené	zařazený	k2eAgFnSc2d1	zařazená
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
Světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
