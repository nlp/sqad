<s>
Rudyard	Rudyard	k1gInSc1
Kipling	Kipling	k1gInSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgInS
v	v	k7c6
indické	indický	k2eAgFnSc6d1
Bombaji	Bombaj	k1gFnSc6
jako	jako	k9
syn	syn	k1gMnSc1
malíře	malíř	k1gMnSc2
pracujícího	pracující	k1gMnSc2
pro	pro	k7c4
britskou	britský	k2eAgFnSc4d1
koloniální	koloniální	k2eAgFnSc4d1
správu	správa	k1gFnSc4
<g/>
,	,	kIx,
dětství	dětství	k1gNnSc4
prožil	prožít	k5eAaPmAgMnS
v	v	k7c6
Indii	Indie	k1gFnSc6
<g/>
,	,	kIx,
kam	kam	k6eAd1
se	se	k3xPyFc4
po	po	k7c6
absolvování	absolvování	k1gNnSc6
internátní	internátní	k2eAgFnSc2d1
školy	škola	k1gFnSc2
v	v	k7c6
Anglii	Anglie	k1gFnSc6
roku	rok	k1gInSc2
1881	[number]	k4
opět	opět	k6eAd1
vrátil	vrátit	k5eAaPmAgMnS
<g/>
,	,	kIx,
protože	protože	k8xS
vojenská	vojenský	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
mu	on	k3xPp3gMnSc3
byla	být	k5eAaImAgFnS
uzavřena	uzavřen	k2eAgFnSc1d1
pro	pro	k7c4
silnou	silný	k2eAgFnSc4d1
krátkozrakost	krátkozrakost	k1gFnSc4
a	a	k8xC
studium	studium	k1gNnSc4
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
bylo	být	k5eAaImAgNnS
příliš	příliš	k6eAd1
nákladné	nákladný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>